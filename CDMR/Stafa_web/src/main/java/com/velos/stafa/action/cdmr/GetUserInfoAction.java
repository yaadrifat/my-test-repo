package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;

import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.VelosUtil;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.util.CDMRSqlConstants;



/**
 * @summary     AttachServiceSets
 * @description Attach Service Set to the line items
 * @version     1.0
 * @file        AttachServiceSets.java
 * @author      Lalit Chattar
 */
public class GetUserInfoAction extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	private String sEcho;
	private Integer iTotalRecords;
	private Integer iTotalDisplayRecords;
	private List<Map<String, String>> aaData;
	private String[] aColumnsSer = { "USER_FIRSTNAME", "USER_LASTNAME", "USER_EMAIL", "USER_FIRSTNAME || ' ' || USER_LASTNAME"};
	private String sOrderBy = null;
	private StringBuilder sWhere;
	
	
	public GetUserInfoAction(){
		cdmrController = new CDMRController();
		aaData = new ArrayList<Map<String,String>>();
	}
	
	
	public String getsEcho() {
		return sEcho;
	}

	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}
	public Integer getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(Integer iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public List<Map<String, String>> getAaData() {
		return aaData;
	}

	public void setAaData(List<Map<String, String>> aaData) {
		this.aaData = aaData;
	}

	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return loadAllUserForCDMR();
	}
	
	public String loadAllUserForCDMR() throws Exception{
		try {
			//List<String> userList = Arrays.asList(aColumnsSer);
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			String esec_DB = CDMRUtil.fetchPropertyValStr("ESEC_DB");
			int constant = Integer.parseInt(jsonObject.getString("iDisplayLength"));
			int end = Integer.parseInt(jsonObject.getString("iDisplayStart")) + Integer.parseInt(jsonObject.getString("iDisplayLength"));
			int start = Integer.parseInt(jsonObject.getString("iDisplayStart"))+1;
			
			sWhere = new StringBuilder();
			if ( jsonObject.getString("sSearch") != "" ){
				sWhere.append(" and (");
				for ( int i=0 ; i<aColumnsSer.length ; i++ )
				{
					
					sWhere.append("LOWER("+aColumnsSer[i]+") LIKE '%"+jsonObject.getString("sSearch").toLowerCase()+"%' OR ");
					
					
				}
				sWhere = sWhere.replace((sWhere.length()-3), sWhere.length(), "");
				sWhere.append(")");
			}
			
			if (jsonObject.has("iSortCol_0")){
				sOrderBy = " ORDER BY  ";
				for ( int i=0 ; i<jsonObject.getInt("iSortingCols") ; i++ )
				{
					if((jsonObject.getString("bSortable_" + jsonObject.getString("iSortCol_"+i))).equals("true")){
						String columnName = "";
						columnName = jsonObject.getString("mDataProp_" + jsonObject.getInt("iSortCol_0"));
						sOrderBy = sOrderBy + columnName + " " + (jsonObject.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
						//sOrderBy = sOrderBy + aColumnsSer[jsonObject.getInt("iSortCol_"+i)] + " " + (jsonObject.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
					}
				}
				
				sOrderBy = sOrderBy.substring(0, (sOrderBy.length()-2));
				if ( sOrderBy.equals(" ORDER BY") )
				{
					sOrderBy = "";
				}
				
			}
			
			
			String query = "select * from (Select PK_USERID, USER_EMAIL, (USER_FIRSTNAME ||' '|| USER_LASTNAME) as NAME,Row_Number() OVER (ORDER BY PK_USERID) MyRow, COUNT(*) OVER () AS TotalRows  from "+esec_DB+".SEC_USER where deletedflag = 0 "+sWhere + sOrderBy+") where MyRow between "+start+" and "+end;
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getUserDetail(jsonObject);
			this.processData(jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	
	/**
	 * @description Process data for displaying in datatable
	 * @return void
	 * @throws Exception
	 */
	public void processData(JSONObject jsonObject) throws Exception{
		List<Map> dataList = (List<Map>)jsonObject.get("userdetail");
		try{
			for (Map map : dataList) {
				Set keySet = map.keySet();
				Map<String, String> data = new HashMap<String, String>();
				String uname = map.get("NAME") == null?"":map.get("NAME").toString();
				data.put("check", "<input type=\"checkbox\" onclick=\"addInUserArray(this.value, '"+uname+"');\" value=\""+map.get("PK_USERID").toString()+"\" name=\"checkForUSer\"/>");
				for (Object key : keySet) {
					if(key.toString().equalsIgnoreCase("TOTALROWS")){
						this.iTotalDisplayRecords = Integer.parseInt(map.get("TOTALROWS").toString());
						this.iTotalRecords = Integer.parseInt(map.get("TOTALROWS").toString());
						continue;
					}
					if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
						data.put(key.toString(), "N/A");
					}else{
						data.put(key.toString(), map.get(key.toString()).toString());
					}
					
				}
				aaData.add(data);
			}
			if(aaData.isEmpty()){
				this.iTotalDisplayRecords = 0;
				this.iTotalRecords = 0;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		this.sEcho= jsonObject.getString("sEcho");
	}
	
}
