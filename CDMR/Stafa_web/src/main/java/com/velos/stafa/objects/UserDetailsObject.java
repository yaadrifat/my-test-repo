/**
 * 
 */
package com.velos.stafa.objects;

import java.io.Serializable;
import java.util.Map;

/**
 * @author akajeera
 *
 */
public class UserDetailsObject implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Long userId;
	private String loginName;
	private String firstName;
	private String lastName;
	private String roleRights;
	private Map moduleMap;
	private String lastLogindt;
	private Long organizationId;
	private String rightsXml;
	
	private String customerId;
	
	
	

	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getRightsXml() {
		return rightsXml;
	}
	public void setRightsXml(String rightsXml) {
		this.rightsXml = rightsXml;
	}
	public Long getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getRoleRights() {
		return roleRights;
	}
	public void setRoleRights(String roleRights) {
		this.roleRights = roleRights;
	}
	public Map getModuleMap() {
		return moduleMap;
	}
	public void setModuleMap(Map moduleMap) {
		this.moduleMap = moduleMap;
	}
	public String getLastLogindt() {
		return lastLogindt;
	}
	public void setLastLogindt(String lastLogindt) {
		this.lastLogindt = lastLogindt;
	}
	
	
}
