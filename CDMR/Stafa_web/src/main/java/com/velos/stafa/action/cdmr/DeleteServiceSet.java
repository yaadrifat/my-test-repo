package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.xwork.StringUtils;
import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;

import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.VelosUtil;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;

/**
 * @summary     LoadTreeGroupViewAction
 * @description Load Tree View in Charge Library
 * @version     1.0
 * @file        LoadTreeGroupViewAction.java
 * @author      Lalit Chattar
 */
public class DeleteServiceSet extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	/*
	 * Parameter getting from request
	 */
	 
	private Set idSet;
	 private Integer grpIdLength;
	/*
	 * Getter and Setter

	*/
	
	
	
	public DeleteServiceSet(){
		cdmrController = new CDMRController();
	}
	
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return SUCCESS;
	}
	
	public String deleteServiceSet() throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			String sersetid = jsonObj.getString("sersetId");
			jsonObj = CDMRUtil.fetchEventLibId(jsonObj);
			String eventlib_id= jsonObj.getString("EVENT_ID");
			this.updateCDMain(sersetid, jsonObj.getInt("user"));
			this.updateSSLink(sersetid);
			this.saveInObsSvcBfr(sersetid,eventlib_id);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		return SUCCESS;
	}
	
	public void updateCDMain(String sersetId, int user) throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			String query = "update CDM_MAIN set DELETEDFLAG = 1, LAST_MODIFIED_BY = "+user+", LAST_MODIFIED_DATE =  TO_TIMESTAMP('"+CDMRUtil.getCurrentTimeStamp()+"', 'DD-MON-RR HH.MI.SS') where PK_CDM_MAIN = " + sersetId ;
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.editServiceSet(jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	/**
	 * @description Get Service set link Information
	 * @return void
	 * @throws Exception
	 */
	public void updateSSLink(String sersetId) throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			String query = "update CDM_SSLINK set DELETEDFLAG = 1 where SS_ID = (select LSERVICE_CODE from CDM_MAIN where PK_CDM_MAIN = " + sersetId + ")" ;
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.editServiceSet(jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * @description Make entry in CDM_OBISVCBFR table
	 * @return void
	 * @throws Exception
	 */
	public void saveInObsSvcBfr(String sersetId,String eventlib_id) throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			String query = "select LSERVICE_CODE, SERVICE_DESC1 from CDM_MAIN where PK_CDM_MAIN = " + sersetId;
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromCdmMain(jsonObj);
			List<Map> data = (List<Map>)jsonObj.get("cdmmain");
			String ssid = data.get(0).get("LSERVICE_CODE").toString();
			String ssName = data.get(0).get("SERVICE_DESC1").toString();
			
			jsonObj.put("ssId", ssid);
			jsonObj.put("ssNameOrg", ssName);
			jsonObj.put("opCode", "D");
			jsonObj.put("eventLibid", eventlib_id);
			jsonObj = cdmrController.saveInObsSvcBfr(jsonObj);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
