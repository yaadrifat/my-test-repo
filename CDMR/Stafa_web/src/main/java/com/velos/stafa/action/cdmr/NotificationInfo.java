package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;

import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.VelosUtil;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.business.domain.cdmr.CodeList;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.util.CDMRSqlConstants;



/**
 * @summary     AttachServiceSets
 * @description Attach Service Set to the line items
 * @version     1.0
 * @file        AttachServiceSets.java
 * @author      Lalit Chattar
 */
public class NotificationInfo extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	private String sEcho;
	private Integer iTotalRecords;
	private Integer iTotalDisplayRecords;
	private List<Map<String, String>> aaData;
	HttpSession httpSession;
	
	private String[] aColumnsSer = { "FILE_DESC", "USER_NAME", "USER_EMAIL"};
	
	private String sOrderBy = null;
	private StringBuilder sWhere;
	
	
	public String getsEcho() {
		return sEcho;
	}

	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}
	public Integer getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(Integer iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public List<Map<String, String>> getAaData() {
		return aaData;
	}

	public void setAaData(List<Map<String, String>> aaData) {
		this.aaData = aaData;
	}
	
	
	public NotificationInfo(){
		cdmrController = new CDMRController();
		aaData = new ArrayList<Map<String,String>>();
	}
	

	public String saveNotificationInfo() throws Exception{
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			String pkID = jsonObject.getString("pkID");
			String fileStatus = jsonObject.getString("fileStatus");
			String[] pkArray = pkID.split(",");
			for (int i = 0; i < pkArray.length; i++) {
				if(checkForExistence(pkArray[i], fileStatus)){
					this.saveInNotifycnf(pkArray[i], fileStatus);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	public boolean checkForExistence(String userid, String fileStatus) throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			String query = "select * from CDM_NOTIFYCNF where DELETEDFLAG = 0 and USER_ID = " + userid + " and STATE_TO=" + fileStatus;
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromNotifycnf(jsonObj);
			List<Map> dataMap = (List<Map>)jsonObj.get("notifycnf");
			if(dataMap.size() > 0){
				return false;
			}else{
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return true;
	}
	

	public boolean saveInNotifycnf(String userid, String fileStatus) throws Exception{
		try {
			
			JSONObject jsonObj = new JSONObject();
			httpSession = request.getSession();
			String esec_DB = CDMRUtil.fetchPropertyValStr("ESEC_DB");
			List<CodeList> codelist = (List<CodeList>)httpSession.getAttribute("codelist");
			String query = "select USER_EMAIL, (USER_FIRSTNAME || ' ' || USER_LASTNAME) as NAME from "+esec_DB+".SEC_USER where PK_USERID = " + userid;
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromEsec(jsonObj);
			List<Map> dataMap = (List<Map>)jsonObj.get("esecdata");
			Map data = dataMap.get(0);
			if(!(data.get("USER_EMAIL") == null)){
				jsonObj.put("stateTo", fileStatus);
				jsonObj.put("userId", userid);
				jsonObj.put("userName", data.get("NAME").toString());
				jsonObj.put("deletedFalg", 0);
				jsonObj.put("userEmail", data.get("USER_EMAIL").toString());
				jsonObj.put("fileDesc", this.getStatus(fileStatus, codelist));
				cdmrController.saveNotificationInfo(jsonObj);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	public String getNotification() throws Exception {
		try {
			List<String> userList = Arrays.asList(aColumnsSer);
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			httpSession = request.getSession();
			List<CodeList> codelist = (List<CodeList>)httpSession.getAttribute("codelist");
			int constant = Integer.parseInt(jsonObj.getString("iDisplayLength"));
			int end = Integer.parseInt(jsonObj.getString("iDisplayStart")) + Integer.parseInt(jsonObj.getString("iDisplayLength"));
			int start = Integer.parseInt(jsonObj.getString("iDisplayStart"))+1;
			
			sWhere = new StringBuilder();
			if ( jsonObj.getString("sSearch") != "" ){
				sWhere.append(" and (");
				for ( int i=0 ; i<aColumnsSer.length ; i++ )
				{
					
					sWhere.append("LOWER("+aColumnsSer[i]+") LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
					
					
				}
				sWhere = sWhere.replace((sWhere.length()-3), sWhere.length(), "");
				sWhere.append(")");
			}
			
			if (jsonObj.has("iSortCol_0")){
				sOrderBy = " ORDER BY  ";
				for ( int i=0 ; i<jsonObj.getInt("iSortingCols") ; i++ )
				{
					if((jsonObj.getString("bSortable_" + jsonObj.getString("iSortCol_"+i))).equals("true")){
					//	sOrderBy = sOrderBy + aColumnsSer[jsonObj.getInt("iSortCol_"+i)] + " " + (jsonObj.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
						String columnName = "";
						columnName = jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0"));
						sOrderBy = sOrderBy + columnName + " " + (jsonObj.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
					}
				}
				
				sOrderBy = sOrderBy.substring(0, (sOrderBy.length()-2));
				if ( sOrderBy.equals(" ORDER BY") )
				{
					sOrderBy = "";
				}
				
			}
						
			String query="select * from(select PK_CDM_NOTIFYCNF, USER_EMAIL, USER_NAME, FILE_DESC, COUNT(*) OVER () AS TotalRows,Row_Number() OVER (ORDER BY PK_CDM_NOTIFYCNF) MyRow from CDM_NOTIFYCNF  left join vbar_codelst on CDM_NOTIFYCNF.state_to = vbar_codelst.PK_CODELST WHERE CDM_NOTIFYCNF.DELETEDFLAG = 0 and vbar_codelst.CODELST_TYPE='filestatus' "+sWhere + sOrderBy+") where MyRow between "+start+" and " + end;
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromNotifycnf(jsonObj);
			this.processData(jsonObj, codelist);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String getNotificationExport() throws Exception {
		try {
			List<String> userList = Arrays.asList(aColumnsSer);
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			httpSession = request.getSession();
			List<CodeList> codelist = (List<CodeList>)httpSession.getAttribute("codelist");
			int constant = Integer.parseInt(jsonObj.getString("iDisplayLength"));
			int end = Integer.parseInt(jsonObj.getString("iDisplayStart")) + Integer.parseInt(jsonObj.getString("iDisplayLength"));
			int start = Integer.parseInt(jsonObj.getString("iDisplayStart"))+1;
			
			sWhere = new StringBuilder();
			if ( jsonObj.getString("sSearch") != "" ){
				sWhere.append(" and (");
				for ( int i=0 ; i<aColumnsSer.length ; i++ )
				{
					
					sWhere.append("LOWER("+aColumnsSer[i]+") LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
					
					
				}
				sWhere = sWhere.replace((sWhere.length()-3), sWhere.length(), "");
				sWhere.append(")");
			}
			
			if (jsonObj.has("iSortCol_0")){
				sOrderBy = " ORDER BY  ";
				for ( int i=0 ; i<jsonObj.getInt("iSortingCols") ; i++ )
				{
					if((jsonObj.getString("bSortable_" + jsonObj.getString("iSortCol_"+i))).equals("true")){
					//	sOrderBy = sOrderBy + aColumnsSer[jsonObj.getInt("iSortCol_"+i)] + " " + (jsonObj.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
						String columnName = "";
						columnName = jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0"));
						sOrderBy = sOrderBy + columnName + " " + (jsonObj.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
					}
				}
				
				sOrderBy = sOrderBy.substring(0, (sOrderBy.length()-2));
				if ( sOrderBy.equals(" ORDER BY") )
				{
					sOrderBy = "";
				}
				
			}
						
			String query="select * from(select PK_CDM_NOTIFYCNF, USER_EMAIL, USER_NAME, FILE_DESC, COUNT(*) OVER () AS TotalRows,Row_Number() OVER (ORDER BY PK_CDM_NOTIFYCNF) MyRow from CDM_NOTIFYCNF left join vbar_codelst on CDM_NOTIFYCNF.state_to = vbar_codelst.PK_CODELST where CDM_NOTIFYCNF.DELETEDFLAG = 0 and vbar_codelst.CODELST_SUBTYP='exportstatus' "+sWhere + sOrderBy+") where MyRow between "+start+" and " + end;
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromNotifycnf(jsonObj);
			this.processData(jsonObj, codelist);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * @description Process data for displaying in datatable
	 * @return void
	 * @throws Exception
	 */
	public void processData(JSONObject jsonObject, List<CodeList> codeList) throws Exception{
		try{
			List<Map> dataList = (List<Map>)jsonObject.get("notifycnf");
			for (Map map : dataList) {
				Set keySet = map.keySet();
				Map<String, String> data = new HashMap<String, String>();
				for (Object key : keySet) {
					
					data.put("delete", "<img src=\"images/cdmr/delete.png\" name=\"deletenotiItem\" id=\"deletenotiItem\" onclick=\"deleteNotifiedUser("+map.get("PK_CDM_NOTIFYCNF").toString()+");\"/>");
					if(key.toString().equalsIgnoreCase("TOTALROWS") || key.toString().equalsIgnoreCase("PK_CDM_MAIN")){
						this.iTotalDisplayRecords = Integer.parseInt(map.get("TOTALROWS").toString());
						this.iTotalRecords = Integer.parseInt(map.get("TOTALROWS").toString());
						continue;
					}
					if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
						data.put(key.toString(), "N/A");
					}else{
						
						data.put(key.toString(), map.get(key.toString()).toString());
					
						
					}
					
				}
				aaData.add(data);
			}
			if(aaData.isEmpty()){
				this.iTotalDisplayRecords = 0;
				this.iTotalRecords = 0;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		this.sEcho= jsonObject.getString("sEcho");
	}
	
	public String getStatus(String status, List<CodeList> codeList) throws Exception{
		for (CodeList codeListObj : codeList) {
			if(codeListObj.getPkCodeList() == Long.parseLong(status)){
				return codeListObj.getCodeListDesc();
			}
		}
		return "";
	}
	
	
}
