package com.velos.stafa.action.cdmr;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.QuartzSchedulerListener;
import com.velos.stafa.controller.CDMRController;

/**
 * @summary     LockListenerexport
 * @description Activate with server start and set defaulr time to Schedular
 * @version     1.0
 * @file        LockListenerexport.java
 * @author      shikha srivastava
 */
public class LockListenerExport implements ServletContextListener {
    private QuartzSchedulerListener jobScheduler = null;
   
    
    /**
	 * @description Destroy Context
	 * @return void
	 * @throws Exception
	 */
    public void contextDestroyed(ServletContextEvent arg0) {
         
         if(jobScheduler != null){
              jobScheduler.shutDownScheduler();
         }
    }

    /**
	 * @description Intilize Context
	 * @return void
	 * @throws Exception
	 */
    public void contextInitialized(ServletContextEvent arg0) {
      
         try {
              jobScheduler = new QuartzSchedulerListener();
              jobScheduler.startSchedulerexport(this.getValueFromDBexport());
         } catch (Throwable e) {
                 e.printStackTrace();
         }
    }
    
   
    public String getValueFromDBexport() throws Exception{
    	try {
    		CDMRController cdmrController = new CDMRController();
			String query = "SELECT PARAM_VAL FROM VBAR_CONTROLPARAM WHERE PARAM_TYPE = 'EXPORT_SCHEDULER' and PARAM_SUBTYP = 'PST'";
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getSchedulerTime(jsonObj);
			List<Map> dataList = (List<Map>)jsonObj.get("scheduler");
			return dataList.get(0).get("PARAM_VAL").toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "";
    }
    
}

