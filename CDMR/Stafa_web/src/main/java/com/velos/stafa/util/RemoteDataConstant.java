package com.velos.stafa.util;

public interface RemoteDataConstant{
	

    public static final String BLUE_CROSS_HCPCS_CPT = "BLUE_CROSS_HCPCS_CPT";
	public static final String BLUE_CROSS_HCPCS_CPT_EFF_DATE = "BLUE_CROSS_HCPCS_CPT_EFF_DATE";
	public static final String MCAID_HCPCS_CPT = "MCAID_HCPCS_CPT";
	public static final String MCAID_HCPCS_CPT_EFF_DATE = "MCAID_HCPCS_CPT_EFF_DATE";
	public static final String MCARE_HCPCS_CPT = "MCARE_HCPCS_CPT";
	public static final String MCARE_HCPCS_CPT_EFF_DATE = "MCARE_HCPCS_CPT_EFF_DATE";
	public static final String PRS_PROC_CD = "PRS_PROC_CD";
	public static final String PRS_PROC_EFF_DATE = "PRS_PROC_EFF_DATE";
	public static final String REV_CLASS = "REV_CLASS";
	public static final String REV_CLASS_DESC = "REV_CLASS_DESC";
	public static final String SVC_CD_TYPE = "SVC_CD_TYPE";
	public static final String SVC_CD = "SVC_CD";
	public static final String SVC_CD_DESC = "SVC_CD_DESC";
	public static final String SVC_CD_GNRL_DESC = "SVC_CD_GNRL_DESC";
	public static final String SVC_GL_KEY = "SVC_GL_KEY";
	public static final String SVC_INACT_STS_DATE = "SVC_INACT_STS_DATE";
	public static final String SVC_PRICE = "SVC_PRICE";
	public static final String SVC_PRICE_EFF_DATE = "SVC_PRICE_EFF_DATE";
	
	
	
	public static final String BASE_FEE_AMT = "BASE_FEE_AMT";
	public static final String CPT_CD = "CPT_CD";
	public static final String EIW_START_DATE = "EIW_START_DATE";
	public static final String EIW_END_DATE = "EIW_END_DATE";
	public static final String MEDICARE_CD = "MEDICARE_CD";
	public static final String PROC_CD = "PROC_CD";
	public static final String PROC_DESC = "PROC_DESC";
	public static final String WELFARE_CD = "WELFARE_CD";
	public static final String CURT_INDCTR = "CURT_INDCTR";
}






