package com.velos.stafa.action.cdmr;

import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.CDMRSqlConstants;
/**
 * @summary     SchedularJobsExport
 * @description Execute Job According Schedular for exporting to CRMS
 * @version     1.0
 * @file        SchedularJobsExport.java
 * @author      Shikha Srivastav
 */


public class ProcessandExportData {
	private String fileStatusPk;
	private String lcode;
	private static CDMRController cdmrController;
	public ProcessandExportData() {
		cdmrController = new CDMRController();
				
	}
	static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	static Date date = new Date();
	//	FOR CALLING DATA EXPORT PROCEDURE
	synchronized public static void getAndExportData() throws Exception {
		try{
			ProcessandExportData exportData = new ProcessandExportData();
			JSONObject jsonObject = new JSONObject();
			String start_body = "CDM-R has started exporting data "+dateFormat.format(date)+". Please refrain from using CDM-R and/or CRMS while the export is in progress.";
			String end_body = "CDM-R has completed data export "+dateFormat.format(date)+". You may resume using CDM-R and/or CRMS.";
			if(exportData.getcountToexport()!=0){
				exportData.Exportnotifications("Starts",start_body);
				
				jsonObject = cdmrController.transferToCtms();	
				if(jsonObject.getString("CRMSDataTransferResult").equalsIgnoreCase("true")){
					
					exportData.Exportnotifications("Completes",end_body);
					
				}
				else{
					System.out.println("Error while data trnsfer-------------------------------");
					String error_body="There was following error while transfering data from CDM-R to CRMS : "+ "\n" +jsonObject.getString("CRMSDataTransferResult");
					
					exportData.Exportnotifications("Error while transfer",error_body);
					
				}
				
			}
			else{
				System.out.println("No changes found to export/update--------------------------------");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void Exportnotifications(String status,String body)throws Exception {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject=getDataFromCodeList(status);
			List<Map> codeList = (List<Map>) jsonObject.get("codelist");
			for (Map map : codeList) {
				this.fileStatusPk = map.get("PK_CODELST").toString();
					
				
			}
			jsonObject = this.getUsernameAndEmailAddress(this.fileStatusPk);
			JSONObject mailConfig = this.getMailConfiguration();
			List<Map> mailObjList = (List<Map>) jsonObject.get("notifycnf");
			
			if (mailObjList.size() != 0) {
				
				for (Map map : mailObjList) {
					this.insertNOtification(map.get("PK_CDM_NOTIFYCNF").toString(),map.get("USER_ID").toString(),body,dateFormat.format(date),map.get("USER_EMAIL").toString());
					List<String> message = this.getNotificationMessages(map.get("PK_CDM_NOTIFYCNF").toString());
					
					for (String msg : message) {
						
						String to = map.get("USER_EMAIL") == null ? null : map.get("USER_EMAIL").toString();
						String body1 = "Hello User" + "\n" + msg.toString();
						mailConfig.put("TO", to);
						mailConfig.put("BODY", body1);
						if (to != null)
							try{
							CDMRUtil.sendNotificationMailExport(mailConfig);
							}
						catch(Exception e){
							System.out.println(e.getMessage());
							System.out.println("mail not sent/issue in email server-----------------------------");
						}
						}
					this.updateNotification(map.get("PK_CDM_NOTIFYCNF").toString());
					
					}					
				}
			
			
		}
		catch (Exception e) {
			CDMRUtil.errorLogUtil(e, lcode, cdmrController,"ProcessandExportData::Exportnotifications", "1");
			e.printStackTrace();
			throw e;
		
		}
	}
	
	public JSONObject getUsernameAndEmailAddress(String pkCodeList)	throws Exception {
		JSONObject jsonObject = new JSONObject();
		try {
			String query = "select PK_CDM_NOTIFYCNF, USER_ID, USER_NAME, USER_EMAIL from CDM_NOTIFYCNF where DELETEDFLAG=0 and STATE_TO = "
					+ pkCodeList;
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getFromNotifycnf(jsonObject);
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,"ProcessandExportData::getUsernameAndEmailAddress", "1");
			e.printStackTrace();
		}
		return jsonObject;
		}
		
		public JSONObject getMailConfiguration() throws Exception {
		JSONObject jsonObject = new JSONObject();
		try {
			String query = "select PARAM_SUBTYP, PARAM_VAL from vbar_controlparam where PARAM_TYPE in ('CTMSEMAIL') and PARAM_SUBTYP in ('SMTP', 'FROM', 'PASSWORD', 'PORT', 'SUBJECT', 'SSL') and DELETEDFLAG = 0";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getMailConfigurationInfo(jsonObject);
			List<Map> data = (List<Map>) jsonObject.get("mailconfig");
			for (Map map : data) {
				jsonObject.put(map.get("PARAM_SUBTYP").toString(), map.get("PARAM_VAL").toString());
			}
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,"ProcessandExportData::getMailConfiguration", "1");
			e.printStackTrace();
		}
		return jsonObject;
		}
	
		
		
		public JSONObject insertNOtification(String fkcnf,String user_ID,String body, String sent_on,String user_mail){
			JSONObject jsonObject = new JSONObject();
			try{
				jsonObject.put("userId",user_ID);
				jsonObject.put("msgTxt", body);
			    jsonObject.put("sentOn",sent_on);
			    jsonObject.put("sentStatus","N");
			    jsonObject.put("pkCdmNotifycnf",fkcnf);
			    jsonObject.put("userEmail", user_mail);
			    jsonObject.put("deletedFalg",0);
			    cdmrController.saveNewNotification(jsonObject);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			return jsonObject;
		}
	
		public int getcountToexport() throws Exception{
			int count=0;
			try{
				JSONObject jsonObject = new JSONObject();
				
				/*String active_rule = CDMRUtil.fetchPropertyValStr("EXPORT_ACTIVATION_RULE");
				String query;
				if(active_rule.trim().equalsIgnoreCase("FLAG"))
				{
				query=CDMRSqlConstants.GET_CHG_COUNT_FLAG;
				}
				else
				{
					query=CDMRSqlConstants.GET_CHG_COUNT_DATE;
				}*/
				String query=CDMRSqlConstants.GET_CHG_COUNT_DATE;
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.getexportDatacount(jsonObject);	
				List<Map> dataList = (List<Map>)jsonObject.get("exportcount");
				for (Map map : dataList) {
					count = Integer.parseInt(map.get("COUNT(*)").toString());
				}
				
				return count+getcountToexportForUpdate();
			}
		 catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
			
		}	
		
		public JSONObject getDataFromCodeList(String status) throws Exception {
			JSONObject jsonObject= new JSONObject();
			try {
				String query = "select PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC from vbar_codelst " +
						"where CODELST_TYPE='transferstatus' and CODELST_SUBTYP='exportstatus' and CODELST_DESC='"+status+"'";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.getCodeList(jsonObject);
			} catch (Exception e) {
				CDMRUtil.errorLogUtil(e, "", cdmrController,"ProcessandExportData::getDataFromCodeList", "1");
				e.printStackTrace();
				throw e;
			}
			
			return jsonObject;
		}
		public List<String> getNotificationMessages(String pkNotifyCnf)	throws Exception {
			List<String> message = new ArrayList<String>();
			JSONObject jsonObject = new JSONObject();
			try {
			String query = "select MSG_TEXT from CDM_NOTIFICATION where FK_CDM_NOTIFYCNF = "
					+ pkNotifyCnf
					+ " and SENT_STATUS = 'N' and DELETEDFLAG = 0";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getNotificationMessages(jsonObject);
			List<Map> data = (List<Map>) jsonObject.get("notimesg");
			for (Map map : data) {
				message.add(map.get("MSG_TEXT").toString());
			}
			} 
			catch (Exception e) {
				CDMRUtil.errorLogUtil(e, "", cdmrController,
				"GetAndProcessRemoteData::getNotificationMessages", "1");
				e.printStackTrace();
			}

			return message;
		}
		public void updateNotification(String pkNotifyCnf) throws Exception {
			JSONObject jsonObject = new JSONObject();
			try {
				String query = "update CDM_NOTIFICATION set SENT_STATUS = 'Y' where FK_CDM_NOTIFYCNF = "
						+ pkNotifyCnf+ " and SENT_STATUS = 'N' and DELETEDFLAG = 0";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.updateNotification(jsonObject);
			} catch (Exception e) {
				CDMRUtil.errorLogUtil(e, "", cdmrController,
						"GetAndProcessRemoteData::updateNotification", "1");
				e.printStackTrace();
			}

		}
		public int getcountToexportForUpdate() throws Exception{
			int count=0;
			try{
				JSONObject jsonObject = new JSONObject();
				
				String query="select count(*) from CDM_OBISVCBFR ";//where SERVICE_NAME is null";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.getexportDatacount(jsonObject);	
				List<Map> dataList = (List<Map>)jsonObject.get("exportcount");
				for (Map map : dataList) {
					count = Integer.parseInt(map.get("COUNT(*)").toString());
				}
				return count;
			}
		 catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
			
		}	
		public JSONObject getTransferStatusData() throws Exception {
			JSONObject jsonObject= new JSONObject();
			try {
				String query = "select PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC from vbar_codelst " +
						"where CODELST_TYPE='transferstatus' and CODELST_SUBTYP='exportstatus'";
				jsonObject.put("query", query);
				jsonObject.put("SQLQuery", true);
				jsonObject = cdmrController.getCodeList(jsonObject);
			} catch (Exception e) {
				CDMRUtil.errorLogUtil(e, "", cdmrController,"ProcessandExportData::getDataFromCodeList", "1");
				e.printStackTrace();
				throw e;
			}
			
			return jsonObject;
		}
}