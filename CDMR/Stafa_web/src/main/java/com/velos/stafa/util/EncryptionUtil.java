package com.velos.stafa.util;



import java.io.IOException;
import java.security.Key;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import atg.taglib.json.util.JSONArray;
import atg.taglib.json.util.JSONObject;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;

import java.security.*;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.*;

/**
 * Servlet implementation class EncryptionServlet
 */
public class EncryptionUtil extends StafaBaseAction {
	private static final long serialVersionUID = 1L;
	private static final String ALGO = "AES";
	HttpSession session = null;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request =(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpServletResponse response =(HttpServletResponse)ac.get(ServletActionContext.HTTP_RESPONSE);
	public String getE() {
		return e;
	}

	public void setE(String e) {
		this.e = e;
	}

	public String getN() {
		return n;
	}

	public void setN(String n) {
		this.n = n;
	}

	public String getMaxdigits() {
		return maxdigits;
	}

	public void setMaxdigits(String maxdigits) {
		this.maxdigits = maxdigits;
	}

	public List<Object> reutnList = new ArrayList();
	public String e;
	public String n;
	public String maxdigits;
	public List getReutnList() {
		return reutnList;
	}

	public void setReutnList(List reutnList) {
		this.reutnList = reutnList;
	}

	/**
	 * Default constructor.
	 */
	public EncryptionUtil() {
		// TODO Auto-generated constructor stub
		session=request.getSession(true);
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	public String f_getEncryptKey() {
		try{
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
		if (request.getParameter("generateKeypair") != null) {

			JCryptionUtil jCryptionUtil = new JCryptionUtil();

			KeyPair keys = null;
			if (request.getSession().getAttribute("keys") == null) {
				keys = jCryptionUtil.generateKeypair(512);
				request.getSession().setAttribute("keys", keys);
			}else{
				keys = (KeyPair) request.getSession().getAttribute("keys");
			}

			StringBuffer output = new StringBuffer();

			String e = JCryptionUtil.getPublicKeyExponent(keys);
			String n = JCryptionUtil.getPublicKeyModulus(keys);
			String md = String.valueOf(JCryptionUtil.getMaxDigits(512));
			setE(e);
			setN(n);
			setMaxdigits(md);
			/*jsonObject.put("e", e);
			jsonObject.put("n", n);
			jsonObject.put("maxdigits", md);
			jsonArray.put(0,jsonObject);*/
			/*output.append("{\"e\":\"");
			output.append(e);
			output.append("\",\"n\":\"");
			output.append(n);
			output.append("\",\"maxdigits\":\"");
			output.append(md);
			output.append("\"}");

			output.toString();
			
			
			output.toString().replaceAll("\r", "").replaceAll("\n", "").trim();
			jsonObject.put("output", output.toString());
			
//			reutnList.add(jsonObject.get("output"));*/
			
		} else {
			//response.getOutputStream().print(String.valueOf(false));
		}
	}catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
	}
	return SUCCESS;
	}
	
	public static String encrypt(String Data) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(Data.getBytes());
        String encryptedValue = new BASE64Encoder().encode(encVal);
        return encryptedValue;
    }

    public static String decrypt(String encryptedData) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
        byte[] decValue = c.doFinal(decordedValue);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }
    private static Key generateKey() throws Exception {
    	byte[] keyValue = com.velos.stafa.util.CDMRConstant.SECRETE_KEY.getBytes();
        Key key = new SecretKeySpec(keyValue, ALGO);
        System.out.println(key);
        return key;
}
}
