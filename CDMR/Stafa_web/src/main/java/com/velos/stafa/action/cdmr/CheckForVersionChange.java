package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.VelosUtil;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;

/**
 * @summary     LoadTreeGroupViewAction
 * @description Load Tree View in Charge Library
 * @version     1.0
 * @file        LoadTreeGroupViewAction.java
 * @author      Lalit Chattar
 */
public class CheckForVersionChange extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	/*
	 * Parameter getting from request
	 */
	 
	private boolean checkCurrentVersion;

	/*
	 * Getter and Setter

	*/
	
	public boolean isCheckCurrentVersion() {
		return checkCurrentVersion;
	}

	public void setCheckCurrentVersion(boolean checkCurrentVersion) {
		this.checkCurrentVersion = checkCurrentVersion;
	}
	
	public CheckForVersionChange(){
		cdmrController = new CDMRController();
	}
	
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return SUCCESS;
	}
	
	public String checkForVersionChange() throws Exception{
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			String query = "select * from CDM_VERSIONDEF where IS_CURRENT = 1 and VERSION_ID = " + jsonObj.getString("versionid");
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getCDMVersionDetail(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("versioninfo");
			if(list.size() > 0){
				this.setCheckCurrentVersion(true);
			}else{
				this.setCheckCurrentVersion(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		return SUCCESS;
	}
	
	
}
