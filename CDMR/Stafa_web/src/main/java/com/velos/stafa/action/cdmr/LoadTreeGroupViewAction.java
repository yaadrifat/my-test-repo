package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.VelosUtil;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;

/**
 * @summary     LoadTreeGroupViewAction
 * @description Load Tree View in Charge Library
 * @version     1.0
 * @file        LoadTreeGroupViewAction.java
 * @author      Lalit Chattar
 */
public class LoadTreeGroupViewAction extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	/*
	 * Parameter getting from request
	 */
	 private int pkId;
	 private List<Map> dataList;

	/*
	 * Getter and Setter

	*/
	
	 public int getPkId() {
			return pkId;
	 }
	 public void setPkId(int pkId) {
			this.pkId = pkId;
	 }
	 public List<Map> getDataList() {
			return dataList;
	 }
	 public void setDataList(List<Map> dataList) {
		this.dataList = dataList;
	 }
	
	public LoadTreeGroupViewAction(){
		cdmrController = new CDMRController();
		dataList = new ArrayList<Map>();
	}
	
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return SUCCESS;
	}
	
	/**
	 * @description Load Tree View With Content
	 * @return String
	 * @throws Exception
	 */
	public String loadTreeGroupViewAction()throws Exception {
	
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			if(jsonObj.getString("root").equals("source")){
				String query = CDMRSqlConstants.GET_SUPER_PARENT_GROUP;
				jsonObj.put("query", query);
				jsonObj.put("SQLQuery", true);
				jsonObj = cdmrController.getFromCDMGroupCode(jsonObj);
				List<Map> objectList = (List<Map>)jsonObj.get("cdmgrpcode");
				for (Map map : objectList) {
					Map data = new HashMap();
					data.put("text", map.get("GRP_DISP_NAME").toString());
					data.put( "expanded", false);
					boolean checkChild = this.checkForChildren(map.get("PK_CDM_GRPCODE").toString(),jsonObj);
					if(checkChild){
						data.put("children", this.getChildrenNode(map.get("PK_CDM_GRPCODE").toString()));
					}
					//data.put( "hasChildren", checkChild);
					data.put("id", map.get("PK_CDM_GRPCODE").toString() + "grp");
					dataList.add(data);
					
				}
			}
			else{
				String query = "select PK_CDM_GRPCODE, GRP_DISP_NAME from CDM_GRPCODE where DELETEDFLAG =0 and PARENT_NODE_ID = " + jsonObj.getString("root").replace("grp", "");
				jsonObj.put("query", query);
				jsonObj.put("SQLQuery", true);
				jsonObj = cdmrController.getFromCDMGroupCode(jsonObj);
				List<Map> objectList = (List<Map>)jsonObj.get("cdmgrpcode");
				for (Map map : objectList) {
					Map data = new HashMap();
					data.put("text", map.get("GRP_DISP_NAME").toString());
					data.put( "expanded", false);
					boolean checkChild = this.checkForChildren(map.get("PK_CDM_GRPCODE").toString(),jsonObj);
					
					
					//data.put( "hasChildren", checkChild);
					data.put("id", map.get("PK_CDM_GRPCODE").toString());
					
					dataList.add(data);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	/**
	 * @description Check For Children for Parent Element
	 * @return Boolean
	 * @throws Exception
	 */
	public Boolean checkForChildren(String pkId, JSONObject jsonObj) throws Exception {
		try {
			String query = CDMRSqlConstants.GET_GROUPS + pkId;
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getFromCDMGroupCode(jsonObj);
			List<Map> objectList = (List<Map>)jsonObj.get("cdmgrpcode");
			if(objectList.isEmpty()){
				return false;
			}else{
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	
	/**
	 * @description Get Chiled Node
	 * @return void
	 * @throws Exception
	 */
	public List getChildrenNode(String pkId) throws Exception {
		JSONObject jsonObj = new JSONObject();
		String query = "select PK_CDM_GRPCODE, GRP_DISP_NAME from CDM_GRPCODE where DELETEDFLAG =0 and PARENT_NODE_ID = " + pkId;
		jsonObj.put("query", query);
		jsonObj.put("SQLQuery", true);
		jsonObj = cdmrController.getFromCDMGroupCode(jsonObj);
		List<Map> objectList = (List<Map>)jsonObj.get("cdmgrpcode");
		List dataList = new ArrayList();
		for (Map map : objectList) {
			Map chiledData = new HashMap();
			chiledData.put("text", map.get("GRP_DISP_NAME").toString());
			chiledData.put( "expanded", false);
			boolean checkChild = this.checkForChildren(map.get("PK_CDM_GRPCODE").toString(),jsonObj);
			if(checkChild){
				chiledData.put("children", this.getChildrenNode(map.get("PK_CDM_GRPCODE").toString()));
			}
			
			//data.put( "hasChildren", checkChild);
			chiledData.put("id", map.get("PK_CDM_GRPCODE").toString() + "grp");
			dataList.add(chiledData);
		}
		return dataList;
	}
	
	

}
