package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.business.domain.cdmr.CodeList;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;


/**
 * @summary     GetNextPossibleStateAction
 * @description Get Nest Possible state of file for staging
 * @version     1.0
 * @file        GetNextPossibleStateAction.java
 * @author      Lalit Chattar
 */
public class GetNextPossibleStateAction extends StafaBaseAction{
	
	/*
	 * Parameter getting from request
	 */
	private Integer fileeID;
	private List<String> filestatus;
	private List<String> value;
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpSession httpSession;
	
	/*
	 * getter and setter
	 */
	public Integer getFileeID() {
		return fileeID;
	}
	public void setFileeID(Integer fileeID) {
		this.fileeID = fileeID;
	}
	public List<String> getFilestatus() {
		return filestatus;
	}

	public void setFilestatus(List<String> filestatus) {
		this.filestatus = filestatus;
	}
	
	public List<String> getValue() {
		return value;
	}

	public void setValue(List<String> value) {
		this.value = value;
	}

	/*
	 * Constructor
	 */
	public GetNextPossibleStateAction(){
		cdmrController = new CDMRController();
		filestatus = new ArrayList<String>();
		value = new ArrayList<String>();
	}
	
	
	/**
	 * @description Default method of action for getting next status of file using state machine
	 * @return String
	 * @throws Exception
	 */
	public String execute() throws Exception{
		JSONObject jsonObj = new JSONObject();
		try{
			httpSession = request.getSession();
			List<CodeList> codelist = (List<CodeList>)httpSession.getAttribute("codelist");
			jsonObj = VelosUtil.constructJSON(request);
			String query = CDMRSqlConstants.SELECT_NEXT_POSSIBLE_STATUS + jsonObj.getString("fileID");
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getNextPossibleStatus(jsonObj);
			List<Map> dataMap = (List<Map>)jsonObj.get("possiblestatus");
			for (Map map : dataMap) {
				getFileStatus(map.get("NXT_POSS_STATE").toString(),codelist);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * @description Get file status
	 * @return void
	 * @throws Exception
	 */
	public void getFileStatus(String status, List<CodeList> codeList) throws Exception{
		if(status.equals("nac"))
			return;
		for (CodeList codeListObj : codeList) {
			if(codeListObj.getPkCodeList() == Long.parseLong(status)){
				this.filestatus.add(codeListObj.getCodeListDesc());
				this.value.add(codeListObj.getPkCodeList().toString());
			}
		}
	}
}