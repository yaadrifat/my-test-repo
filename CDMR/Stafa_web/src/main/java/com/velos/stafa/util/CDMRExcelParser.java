package com.velos.stafa.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.xwork.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.hibernate.classic.Session;

import atg.taglib.json.util.JSONArray;
import atg.taglib.json.util.JSONObject;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;
import com.velos.stafa.business.domain.cdmr.StagingMaps;
import com.velos.stafa.business.domain.cdmr.StagingTemp;
import com.velos.stafa.business.util.HibernateUtil;
import com.velos.stafa.business.util.ObjectTransfer;
import com.velos.stafa.business.util.VelosConstants;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.helper.VelosHelper;

/**
 * @summary     CDMRExcelParser
 * @description Utility Class for reading data from xls/xlsx file
 * 				and processing
 * @version     1.0
 * @file        CDMRExcelParser.java
 * @author      Shiv Murti Pal
 */
public class CDMRExcelParser{	
	
	private int indexNo;
	@SuppressWarnings("unchecked")
	
	public HashMap<String, HashMap<String,ArrayList<List>>> ReadAndParseExcel(JSONObject objCDMR,CDMRController cdmrController)
	{
		
		int noOfSheet = 0;
		int noofcols = 0;
		int noHeaderRows = 0;	
				
		FileInputStream fis = null; 
		HashMap<String, HashMap<String,ArrayList<List>>> excelSheetsData = new HashMap<String,HashMap<String,ArrayList<List>>>();
		
		try 
		{ 	
			JSONArray jsonArray = new JSONArray(objCDMR.getString("fileinfo"));
			noOfSheet = Integer.parseInt(((JSONObject)jsonArray.get(0)).getString("SHEET_NOS"));	
			noHeaderRows = Integer.parseInt(((JSONObject)jsonArray.get(0)).getString("TH_ROWS"));
			noofcols=Integer.parseInt(((JSONObject)jsonArray.get(0)).getString("LM_COLS"));				
								
			// Create a FileInputStream that will be use to read the excel file. 
			   
			fis = new FileInputStream(objCDMR.getString("final_file_name")); 
			
			// Create an excel workbook from the file system. 		
			Workbook workbook = WorkbookFactory.create(fis); 
			
			//
			Map cdmFileColType = new HashMap();
						
			List<StagingMaps> mapsObjList = (List<StagingMaps>) objCDMR.get("coltypemap");
		
			for (StagingMaps stagingMaps : mapsObjList) {
				
				cdmFileColType.put(stagingMaps.getCompSeqNum().toString(),
							stagingMaps.getMainCdmColType());
				}
			for(int sheetCount = 0; sheetCount < noOfSheet; sheetCount++)
			{
				Sheet sheet = workbook.getSheetAt(sheetCount); 
				String sheetName = sheet.getSheetName();
				ArrayList sheetData = new ArrayList(); 
				Iterator rows = sheet.rowIterator(); 
				while (rows.hasNext()) 
				{ 					
					Row row = (Row) rows.next(); 
					
					if(row.getRowNum() < noHeaderRows)
						continue;
					
					//changes for blank row	start	
					int firstCell = row.getFirstCellNum();
					int lastCell = row.getLastCellNum(); 
					int noofcels = lastCell-firstCell;
					boolean bBlankRow = IsBlankRow(row);//, firstCell, lastCell);
					//changes for blank row end
					
					Iterator cells = row.cellIterator(); 
					List<String> data = new ArrayList(); 
					
					if(bBlankRow == false)		
					{
						//int noOfCell = row.getLastCellNum();
						for(int cn=0; cn<noofcols; cn++) 
						{ 		
							Cell cell;
							String cellValue = "";													
							
							 if( row.getCell(cn ) == null)
				             {
				                 cell = row.createCell(cn);
				                 cellValue = "";
				                
				             } 
				             else if(row.getCell(cn).getCellType() == Cell.CELL_TYPE_BLANK)	 
				             {
				                 cell = row.getCell(cn);
				                 cellValue = "";
				                 
				             }     
				            else if(row.getCell(cn).getCellType() == Cell.CELL_TYPE_NUMERIC)	 
				             {
				            	if (HSSFDateUtil.isCellDateFormatted(row.getCell(cn))) {
				            		cell = row.getCell(cn);				                  
				                    cellValue = new SimpleDateFormat("dd-MMM-yyyy").format(cell.getDateCellValue());
					                           
				                }
				            	else
				            	{				            		
				            		cell = row.getCell(cn);
				            		cell.setCellType(Cell.CELL_TYPE_STRING);
				            		cellValue = cell.getStringCellValue();
				            		
				            	}
				                 
				             } 
				            else if(row.getCell(cn).getCellType() == Cell.CELL_TYPE_STRING)	 
				             {
				            	if (cdmFileColType.containsKey(Integer.toString(cn+1))){
				            		          		
				            		String filecdmCol = (cdmFileColType.get(Integer.toString(cn+1))) == null ? "" : cdmFileColType.get(Integer.toString(cn+1)).toString();

									/*if (filecdmCol.equalsIgnoreCase("TIMESTAMP")) {
				                     
										cell = row.getCell(cn);
										cellValue = cell.getStringCellValue();
										cell.setCellType(Cell.CELL_TYPE_NUMERIC);
						                SimpleDateFormat datetemp = new SimpleDateFormat("MM/dd/yyyy");
					            		Date cellVal  = datetemp.parse(cellValue);
					            		
					            		cell.setCellValue(cellVal);
					            		
					            		//binds the style you need to the cell.
										CellStyle dateCellStyle = workbook.createCellStyle();
										short df = workbook.createDataFormat().getFormat("MM/dd/yyyy");
										dateCellStyle.setDataFormat(df);
										cell.setCellStyle(dateCellStyle);		            				            		
										cellValue = new SimpleDateFormat("dd-MMM-yyyy").format(cell.getDateCellValue());
					            		
				
									}*/
				            		if (filecdmCol.equalsIgnoreCase("TIMESTAMP")) {
	                                    SimpleDateFormat srcformat = new SimpleDateFormat("MM/dd/yyyy");
	                                    SimpleDateFormat desformat = new SimpleDateFormat("dd-MMM-yyyy");
	                                    cell = row.getCell(cn);
	                                    String cellString = cell.getStringCellValue();
	                                    try {
	                                        Date date = srcformat.parse(cellString);
	                                       
	                                        cellValue = desformat.format(date);
	                                    }
	                                    catch (ParseException e) {
	                                        e.printStackTrace();
	                                    }
				            		}
	                                else
									{
										cell = row.getCell(cn);
						                cellValue = cell.getStringCellValue();
									}
				            			
				            	}
				            
				            	else
				            	{
				            		cell = row.getCell(cn);
					                cellValue = cell.getStringCellValue();
				            	}
				                 
				             } 
				            else if(row.getCell(cn).getCellType() == Cell.CELL_TYPE_BOOLEAN)	 
				             {
				                 cell = row.getCell(cn);
				                 cell.setCellType(Cell.CELL_TYPE_STRING);
				            	// cellValue = cell.getStringCellValue();
				                 cellValue = Boolean.toString(cell.getBooleanCellValue());
				                 
				             } 
							 		
							 
				            else if (row.getCell(cn).getCellType() == Cell.CELL_TYPE_FORMULA) {
				                // Re-run based on the formula type
				            	cell = row.getCell(cn);
				                
				                
				                if(cell.getCachedFormulaResultType() == Cell.CELL_TYPE_NUMERIC)	 
					             {
					                
				                	if (HSSFDateUtil.isCellDateFormatted(row.getCell(cn))) {
						                   
					                    cell = row.getCell(cn);	
					                    cellValue = new SimpleDateFormat("dd-MMM-yyyy").format(cell.getDateCellValue());
						            }
					            	else
					            	{
					            		cell = row.getCell(cn);
					            		cell.setCellType(Cell.CELL_TYPE_STRING);
					            		cellValue = cell.getStringCellValue();
					            	}
					                 
					                 
					             } 
					            else if(cell.getCachedFormulaResultType() == Cell.CELL_TYPE_STRING)	 
					             {
					                 cellValue = cell.getStringCellValue();
					             } 
					            else if(cell.getCachedFormulaResultType() == Cell.CELL_TYPE_BOOLEAN)	 
					             {
					            	cell = row.getCell(cn);
					                cell.setCellType(Cell.CELL_TYPE_STRING);
					            	cellValue = cell.getStringCellValue();
					             } 
				            }							
				             data.add(cellValue);             
						} 						
						sheetData.add(data); 
						
					}								
				} 
				

				ProcessXLS(objCDMR, sheetData, noHeaderRows,cdmrController);
						
				
			}
		} 
		catch (FileNotFoundException e) 
	    {
			//e.printStackTrace();
	    }
		catch (IOException e) 
		{ 
			e.printStackTrace(); 
		}
		
		
		catch (InvalidFormatException e) 
		{			
			e.printStackTrace();
		}		
		catch (NullPointerException e) 
		{ 
			e.printStackTrace();
		} 
		
		catch (IllegalArgumentException e) 
	    {
			e.printStackTrace();
	    }
		catch (Exception e) 
	    {
			e.printStackTrace();
	    }
		
		
		return excelSheetsData;
	}
	
	
	private void ProcessXLS(JSONObject objCDMR, List sheetData,  int noHeaderRows, CDMRController cdmrController) throws Exception 
	{ 
		Session session = null;
		try{
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			
		}catch (Exception e) {
			e.printStackTrace();
		}			
		int sheetSize = sheetData.size();	
		
		
			
		for (int rowCnt = 0; rowCnt < sheetSize; rowCnt++) 
		{ 			
			List list = (List) sheetData.get(rowCnt); 
			
			
			/*******************************************************************/
			JSONObject dataObject = new JSONObject();			
			int index = 1;
			
			for (int j = 0; j<(list.size()-2); j++){
					dataObject.put("dataCol" + index, list.get(j).toString());
						
				index++;
			}
							
			String cdmrStagingTempInfo = VelosConstants.CDMR_STAGING_TEMP;
			Object stagingTempObj = VelosHelper.getNewInstance(cdmrStagingTempInfo);
			stagingTempObj = ObjectTransfer.tranferJSONObjecttoObject(dataObject, stagingTempObj);
			StagingTemp stagingTemp = (StagingTemp)stagingTempObj;
			if(objCDMR.getString("eventLibID") != null){
				stagingTemp.setEventLibid(objCDMR.getString("eventLibID"));
			}
			session.save(stagingTemp);
		}
				
		session.getTransaction().commit();
			
		
	} 
	
	
	private boolean IsBlankRow(Row row)
	{
	     boolean isEmptyRow = true;
	     
	     if (row == null) 
	     {
	         return true;
	     }
	     if (row.getLastCellNum() <= 0) 
	     {
	         return true;
	     }
	     for(int cellNum = row.getFirstCellNum(); cellNum < row.getLastCellNum(); cellNum++)
	     {
	        Cell cell = row.getCell(cellNum);
	        if(cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK && StringUtils.isNotBlank(cell.toString()))
	        {
	        	isEmptyRow = false;
	        }    
	     }
	     return isEmptyRow;		   
	}
	
}