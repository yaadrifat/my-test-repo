/**
 * 
 */
package com.velos.stafa.action;

import java.io.StringReader;
import java.security.KeyPair;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.velos.stafa.controller.CDMRController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpUtils;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.struts2.ServletActionContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import atg.taglib.json.util.JSONArray;
import atg.taglib.json.util.JSONObject;

import com.opensymphony.xwork2.ActionContext;
import java.sql.Timestamp;
import com.velos.stafa.business.domain.usertracking.UserSession;
import com.velos.stafa.business.util.VelosConstants;
import com.velos.stafa.business.util.WebserviceUtil;
import com.velos.stafa.controller.CommonController;
import com.velos.stafa.objects.UserDetailsObject;
import com.velos.stafa.util.JCryptionUtil;
import com.velos.stafa.util.VelosStafaConstants;
import com.velos.stafa.util.VelosUtil;
//import com.velos.stafa.util.CDMRUtil;

/**
 * @author akajeera
 *
 */
public class LoginAction extends StafaBaseAction{
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	public String errorMessage = "";
	public String errorCode = "";
	public String statusCode;
	UserDetailsObject userObject = new UserDetailsObject(); 
	HttpSession session = null;
	public ArrayList returnList;
	public String getErrorMessage() {
		return errorMessage;
	}


	public LoginAction() {
		cdmrController = new CDMRController();
	}
	
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

//	public String forgotPassword()throws Exception{
//		JSONObject jsonObject = new JSONObject();
//		try {
//			jsonObject = VelosUtil.constructJSON(request);
//			jsonObject.put("query", com.velos.stafa.util.CDMRSqlConstants.GET_SUPER_ADMIN);
//			jsonObject.put("SQLQuery", true);
//			jsonObject = cdmrController.getSuperAdmin(jsonObject);
//			List<Map> list = (List<Map>)jsonObject.get("superadmin");
//			for (Map map : list) {
//				if(map.get("PARAM_SUBTYP").toString().equalsIgnoreCase("SUPER_ADMIN")){
//					jsonObject.put("userId", map.get("PARAM_VAL").toString());
//				}
//				if(map.get("PARAM_SUBTYP").toString().equalsIgnoreCase("ADMIN_EMAIL")){
//					jsonObject.put("adminEmail", map.get("PARAM_VAL").toString());
//				}
//			}
//			jsonObject.put("loginName", jsonObject.getString("from"));
//			jsonObject.put("userEmail", jsonObject.getString("notifyTo"));
//			jsonObject.put("sentStatus", "N");
//			jsonObject.put("msgTxt", "Dear Admin, User '"+jsonObject.getString("from")+"' requested to reset password.");
//			jsonObject.put("receivedOn", jsonObject.getString("createdOn"));
//			jsonObject.put("deletedFalg", 0);
//			cdmrController.saveForgotPasswordRequest(jsonObject);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return SUCCESS;
//	}
	
	public String forgotPassword()throws Exception{
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = VelosUtil.constructJSON(request);
//			jsonObject.put("query", com.velos.stafa.util.CDMRSqlConstants.GET_SUPER_ADMIN);
//			jsonObject.put("SQLQuery", true);
//			jsonObject = cdmrController.getSuperAdmin(jsonObject);
//			List<Map> list = (List<Map>)jsonObject.get("superadmin");
//			Map dataMap = list.get(0);
//			jsonObject.put("userId", dataMap.get("PARAM_VAL").toString());
//			jsonObject.put("msgTxt", "");
//			jsonObject.put("sentStatus", "N");
//			System.out.println(jsonObject);
			jsonObject.put(VelosStafaConstants.SUBJECT, VelosStafaConstants.RESET_PASSWORD);
			jsonObject.put("Domain", VelosStafaConstants.NOTIFICATION_DOMAIN);
			jsonObject.put("domainKey", VelosStafaConstants.NOTIFICATION_DOMAINKEY);
			CommonController.saveObject(jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String checkForUserExistance() throws Exception{
		ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
		String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
		if(eSecurityFlag.equalsIgnoreCase("true")){
				JSONObject jsonObject = new JSONObject();
				String esecurityURL = resource.getString(ESECURITY_URL);
			try {
				jsonObject = VelosUtil.constructJSON(request);
				
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:checkUserByLoginEmail>");
				xmlIp.append("<loginName>"+jsonObject.getString("from")+"</loginName>");
				xmlIp.append("<email>"+jsonObject.getString("notifyTo")+"</email>");
				xmlIp.append("</ser:checkUserByLoginEmail> </soapenv:Body></soapenv:Envelope>");
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				Pattern pattern = Pattern.compile("<result>(.+?)</result>");
				Matcher matcher = pattern.matcher(result.toString());
				if(matcher.find()){
					this.setStatusCode("true");
				}else{
					this.setStatusCode("false");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return SUCCESS;
	}
	
	public String fetchVersionDetails()throws Exception{
		JSONObject jsonObject = new JSONObject();
		try {
			
			String criteria = "select pkCodelst, description from CodeList where subType='"+VelosStafaConstants.APPLICATIONVERSION_PARENTCODE+"'";
						System.out.println("criteria  : "+criteria );
			jsonObject.put("criteria", criteria);	
			jsonObject.put(VelosStafaConstants.TYPE, VelosConstants.HQLSTR);
			jsonObject = CommonController.getObjectListByCriteria(jsonObject);
			returnList = (ArrayList)jsonObject.get("returnList");
			System.out.println("returnList "+returnList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String loadLoginDetails()throws Exception{
		System.out.println("LoginAction : validateUser Method Start ");
		

		try{
			ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
				
			session=request.getSession(true);
			KeyPair keys = (KeyPair) request.getSession().getAttribute("keys");
			System.out.println("THIS IS USER KEY>>>>>>>>>" + keys);
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			String decryptedUser = JCryptionUtil.decrypt(username, keys);
			String decryptedPassword = JCryptionUtil.decrypt(password, keys);
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			StringBuffer moduleRightsXml = new StringBuffer();
			if(eSecurityFlag.equalsIgnoreCase("true")){
				// eSecurity validation
				String esecurityURL = resource.getString(ESECURITY_URL);
				System.out.println("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:validateUser>");
				xmlIp.append("<user> <application>Stafa</application>");
				//xmlIp.append("<orgName>?</orgName>");
				xmlIp.append("<password>"+decryptedPassword+"</password>");
				xmlIp.append("<userName>"+decryptedUser+"</userName>");
				xmlIp.append("</user> </ser:validateUser> </soapenv:Body></soapenv:Envelope>");

				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				System.out.println("Result================ " + result);
				
				String strXmlValue = WebserviceUtil.getXmlValue(result, "result", "validUser");
				if(strXmlValue.equalsIgnoreCase("false")){
					//?BB errorMessage= WebserviceUtil.getXmlValue(result, "result", "errorCode") + ":"+WebserviceUtil.getXmlValue(result, "result", "errorMessage");
					errorMessage= WebserviceUtil.getXmlValue(result, "result", "errorMessage");
				}else{
					String statusCode = WebserviceUtil.getXmlValue(result, "result", "statusCode");
					System.out.println("statusCode->"+statusCode);
					if( statusCode.equalsIgnoreCase("FAIL")){
						errorCode = WebserviceUtil.getXmlValue(result, "result", "errorCode");
						System.out.println("errorCode->"+errorCode);
					}
					else{
						Long userId = new Long(WebserviceUtil.getXmlValue(result, "result", "userId"));
						userObject.setFirstName(WebserviceUtil.getXmlValue(result, "result", "firstName"));
						userObject.setLastName(WebserviceUtil.getXmlValue(result, "result", "lastName"));
						userObject.setUserId(userId);
						userObject.setLoginName(WebserviceUtil.getXmlValue(result, "result", "userName"));
						userObject.setCustomerId(WebserviceUtil.getXmlValue(result, "result", "customerId"));
						//userObject.setRoleRights(validateKeeper.getroleRights());
						userObject.setModuleMap(WebserviceUtil.getArrayToMap(result,"module","moduleName","userRights"));
						
						//userObject.setEsign(validateKeeper.getEsign());
						
						if(userObject.getModuleMap() != null){
							Iterator it = userObject.getModuleMap().entrySet().iterator();
							while (it.hasNext()) {
								Map.Entry pairs = (Map.Entry)it.next();
								//System.out.println(pairs.getKey() + " = " + pairs.getValue());
								/*moduleRightsXml.append("<moduleName>" +pairs.getKey()+"</moduleName>" );
								moduleRightsXml.append("<userRights>" +pairs.getValue()+"</userRights>" );
								*/
								moduleRightsXml.append("<" +pairs.getKey()+">"+pairs.getValue()+"</" +pairs.getKey()+">" );
								
							}
						}
						///code for maintaining user session done by Mohiuddin on 19/11/2013
						saveUserSession(userId,true);
					}
				}
			}else{
				// no security check
				userObject.setFirstName("Velos");
				userObject.setLastName("Test User");
				userObject.setUserId(new Long("0"));
				userObject.setLoginName("Testuser");
				//admin rights
				moduleRightsXml.append("<AdministratorView>RAMD</AdministratorView>");
				moduleRightsXml.append("<StafaMaintenance>RAMD</StafaMaintenance>");
				moduleRightsXml.append("<DataFieldConfig>RAMD</DataFieldConfig>");
				moduleRightsXml.append("<PanelConfig>RAMD</PanelConfig>");
				moduleRightsXml.append("<Qc>RAMD</Qc>");
				moduleRightsXml.append("<Protocol>RAMD</Protocol>");
				moduleRightsXml.append("<Immunophenotyping>RAMD</Immunophenotyping>");
				
				moduleRightsXml.append("<WidgetLibrary>RAMD</WidgetLibrary>");
				moduleRightsXml.append("<PageConfig>RAMD</PageConfig>");
				
				//E-Security Rights
				moduleRightsXml.append("<Security>RAMD</Security>");
				moduleRightsXml.append("<Organizations>RAMD</Organizations>");
				moduleRightsXml.append("<Groups>RAMD</Groups>");
				moduleRightsXml.append("<Users>RAMD</Users>");
				
				moduleRightsXml.append("<AccountManagement>RAMD</AccountManagement>");
				moduleRightsXml.append("<DataFieldLibrary>RAMD</DataFieldLibrary>");
				moduleRightsXml.append("<InventoryLibrary>RAMD</InventoryLibrary>");
				moduleRightsXml.append("<QCLibrary>RAMD</QCLibrary>");
				moduleRightsXml.append("<WidgetLibrary>RAMD</WidgetLibrary>");
				moduleRightsXml.append("<ProtocolLibrary>RAMD</ProtocolLibrary>");
				moduleRightsXml.append("<TaskLibrary>RAMD</TaskLibrary>");
				//Technician rights
				moduleRightsXml.append("<TechnicianView>RAMD</TechnicianView>");
				moduleRightsXml.append("<Home>RAMD</Home>");
				moduleRightsXml.append("<Acquisition>RAMD</Acquisition>");
				moduleRightsXml.append("<Accession>RAMD</Accession>");
				moduleRightsXml.append("<Production>RAMD</Production>");
				moduleRightsXml.append("<PreProduct>RAMD</PreProduct>");
				moduleRightsXml.append("<OvernightStorage>RAMD</OvernightStorage>");
				moduleRightsXml.append("<OriginalProduct>RAMD</OriginalProduct>");
				moduleRightsXml.append("<Processing>RAMD</Processing>");
				
				moduleRightsXml.append("<Cryoprep>RAMD</Cryoprep>");
				moduleRightsXml.append("<Storage>RAMD</Storage>");
				moduleRightsXml.append("<StoreProduct>RAMD</StoreProduct>");
				moduleRightsXml.append("<Transfer>RAMD</Transfer>");
				moduleRightsXml.append("<Retrieve>RAMD</Retrieve>");
				moduleRightsXml.append("<Release>RAMD</Release>");
				moduleRightsXml.append("<Documents>RAMD</Documents>");
				moduleRightsXml.append("<Thaw>RAMD</Thaw>");
				moduleRightsXml.append("<Wash>RAMD</Wash>");
				moduleRightsXml.append("<InfusionRelease>RAMD</InfusionRelease>");
				
				// Physician rights
				moduleRightsXml.append("<PhysicianView>RAMD</PhysicianView>");
				moduleRightsXml.append("<InfusionOrder>RAMD</InfusionOrder>");
				moduleRightsXml.append("<SelectDonor>RAMD</SelectDonor>");
				moduleRightsXml.append("<CollectionCycle>RAMD</CollectionCycle>");
				moduleRightsXml.append("<SelectProduct>RAMD</SelectProduct>");
				moduleRightsXml.append("<Signoff>RAMD</Signoff>");
				moduleRightsXml.append("<ProcedureNote>RAMD</ProcedureNote>");
				moduleRightsXml.append("<SupervisorView>RAMD</SupervisorView>");
				moduleRightsXml.append("<SupervisorHomePage>RAMD</SupervisorHomePage>");
				moduleRightsXml.append("<DonorList>RAMD</DonorList>");
				moduleRightsXml.append("<DonorDetails>RAMD</DonorDetails>");
				moduleRightsXml.append("<DonorAssignment>RAMD</DonorAssignment>");
				moduleRightsXml.append("<OtherTask>RAMD</OtherTask>");
				
				
				moduleRightsXml.append("<ProductHome>RAMD</ProductHome>");
				
				moduleRightsXml.append("<TaskManagement>RAMD</TaskManagement>");
				moduleRightsXml.append("<DailyAssignments>RAMD</DailyAssignments>");
				moduleRightsXml.append("<RecurringTask>RAMD</RecurringTask>");
				moduleRightsXml.append("<Handover>RAMD</Handover>");
				
				// NurseView
				moduleRightsXml.append("<NurseView>RAMD</NurseView>");
				moduleRightsXml.append("<HomePage>RAMD</HomePage>");
				moduleRightsXml.append("<Procedure>RAMD</Procedure>");
				moduleRightsXml.append("<Preparation>RAMD</Preparation>");
				moduleRightsXml.append("<Verification>RAMD</Verification>");
				moduleRightsXml.append("<Collection>RAMD</Collection>");
				moduleRightsXml.append("<PostCollection>RAMD</PostCollection>");
				moduleRightsXml.append("<Transport>RAMD</Transport>");
				moduleRightsXml.append("<Followup>RAMD</Followup>");
				
				// CIBMTR FORMS
				moduleRightsXml.append("<CIBMTRForms>RAMD</CIBMTRForms>");
				moduleRightsXml.append("<2400>RAMD</2400>");
				moduleRightsXml.append("<2006>RAMD</2006>");
				
				
				 moduleRightsXml.append("<SupervisorViewCTL>RAMD</SupervisorViewCTL>");
	                moduleRightsXml.append("<HomeCTL>RAMD</HomeCTL>");
	                moduleRightsXml.append("<ProductHomeCTL>RAMD</ProductHomeCTL>");
	                moduleRightsXml.append("<TaskManagementCTL>RAMD</TaskManagementCTL>");
	                moduleRightsXml.append("<DailyAssignmentCTL>RAMD</DailyAssignmentCTL>");
	                moduleRightsXml.append("<RecurringTaskCTL>RAMD</RecurringTaskCTL>");
	                moduleRightsXml.append("<ProductProcessingReview>RAMD</ProductProcessingReview>");
	                moduleRightsXml.append("<ManufacturingRecordClosure>RAMD</ManufacturingRecordClosure>");
	                moduleRightsXml.append("<SOPLibrary>RAMD</SOPLibrary>");


	                moduleRightsXml.append("<TechnicianViewCTL>RAMD</TechnicianViewCTL>");
	                moduleRightsXml.append("<PreProcessingCTL>RAMD</PreProcessingCTL>");
	             /*   moduleRightsXml.append("<Processing>RAMD</Processing>");*/
	                moduleRightsXml.append("<CryopreparationCTL>RAMD</CryopreparationCTL>");
	                /*moduleRightsXml.append("<LabThaw>RAMD</LabThaw>");*/
	                moduleRightsXml.append("<DocumentReview>RAMD</DocumentReview>");
	                moduleRightsXml.append("<retrieval>RAMD</retrieval>");
	                moduleRightsXml.append("<InfusionPreparation>RAMD</InfusionPreparation>");
	                moduleRightsXml.append("<BesideThaw>RAMD</BesideThaw>");
					moduleRightsXml.append("<infusionInformation>RAMD</infusionInformation>");
					moduleRightsXml.append("<Relocation>RAMD</Relocation>");
					moduleRightsXml.append("<Discard>RAMD</Discard>");
					moduleRightsXml.append("<InternalTransport>RAMD</InternalTransport>");
					moduleRightsXml.append("<ProductInventory>RAMD</ProductInventory>");
					moduleRightsXml.append("<TrackShipping>RAMD</TrackShipping>");

				
				Map rightsMap = new HashMap();
				rightsMap.put("NurseView", "RAM");
				userObject.setModuleMap(rightsMap);
				
			}
			
//?BB for widget lib
			
			moduleRightsXml.append("<WidgetLibrary>RAMD</WidgetLibrary>");
				userObject.setRightsXml(moduleRightsXml.toString());
				cleanSession();
				session.setAttribute(USER_OBJECT,userObject);
				
				//Session Initialization
				session.setAttribute("sessionInitial", "true");
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String logout()throws Exception{
		System.out.println("LogOut Start");
		try {
			session=request.getSession(false);
			saveUserSession(null,false);	
			
			if (session!=null){
				System.out.println("------------------LogOut-----------------");
				session.removeAttribute(USER_OBJECT);
				session.removeAttribute(USER_SESSION_OBJECT);
				//session.invalidate();
			}
			
			session = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("LogOut End");
		return SUCCESS;
	}

	public void cleanSession()throws Exception{
		if (session!=null){
			//session.removeAttribute(VelosP1VConstants.SESSION_CALLQUEUESEARCH);
			session.removeAttribute("sessionInitial");
		}	
	}
	
	private void saveUserSession(Long userId,Boolean flag){
		try{
			JSONObject jsonDataObject = new JSONObject();
	
			//JSONObject jsonUserSessionObject = (JSONObject) session.getAttribute(VelosStafaConstants.USER_SESSION_OBJECT);
			if(session.getAttribute(VelosStafaConstants.USER_SESSION_OBJECT)!=null)
			{	JSONObject jsonUserSessionObject = (JSONObject) session.getAttribute(VelosStafaConstants.USER_SESSION_OBJECT);
				jsonUserSessionObject.remove("successFlag");
				jsonUserSessionObject.put("successFlag", flag);
				jsonDataObject = CommonController.saveJSONObject(jsonUserSessionObject);
			}else{
				JSONObject userSessionData = constructUserSession(userId,flag);
				userSessionData.put("domain", VelosStafaConstants.DOMAIN_USERSESSION);
				userSessionData.put("domainKey", VelosStafaConstants.USERSESSION_DOMAINKEY);
				//userSessionData = VelosUtil.setUserDetails(userSessionData,request);
				System.out.println(" final data --------------------");
				System.out.println(userSessionData.toString());
				userSessionData = CommonController.saveJSONObject(userSessionData);
				session.setAttribute(VelosStafaConstants.USER_SESSION_OBJECT, userSessionData);
			}
			
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private JSONObject constructUserSession(Long userId,Boolean flag){
		JSONObject userSessionObject = new JSONObject();		
		try{
			userSessionObject.put("user",userId);
			userSessionObject.put("loginTime", new SimpleDateFormat("MM/dd/yyyy HH:mm").format(new Timestamp(new Date().getTime()).getDate()));
			userSessionObject.put("logoutTime",new SimpleDateFormat("MM/dd/yyyy HH:mm").format(new Timestamp(new Date().getTime()).getDate()));
			userSessionObject.put("successFlag", flag);			
		}catch (Exception e){
			e.printStackTrace();
		}
		return userSessionObject;
	}
		

	public ArrayList getReturnList() {
		return returnList;
	}


	public void setReturnList(ArrayList returnList) {
		this.returnList = returnList;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
//	public  JSONObject getMailConfiguration() throws Exception{
//		  JSONObject jsonObject = new JSONObject();
//			try {
//				String query = "select PARAM_SUBTYP, PARAM_VAL from vbar_controlparam where PARAM_TYPE in ('EMAIL') and PARAM_SUBTYP in ('SMTP', 'FROM', 'PASSWORD', 'PORT', 'SUBJECT', 'SSL') and DELETEDFLAG = 0";
//				jsonObject.put("query", query);
//				jsonObject.put("SQLQuery", true);
//				jsonObject = cdmrController.getMailConfigurationInfo(jsonObject);
//				List<Map> data = (List<Map>)jsonObject.get("mailconfig");
//				for (Map map : data) {
//					jsonObject.put(map.get("PARAM_SUBTYP").toString(), map.get("PARAM_VAL").toString());
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			return jsonObject;
//	  }	  
}
