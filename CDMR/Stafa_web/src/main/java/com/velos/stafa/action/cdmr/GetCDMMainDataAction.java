package com.velos.stafa.action.cdmr;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.velos.stafa.util.CDMRUtil;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.VelosUtil;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.util.AdvanceSearchUtil;



/**
 * @summary     GetCDMMainDataAction
 * @description load data in charge library datatable
 * @version     1.0
 * @file        GetCDMMainDataAction.java
 * @author      Lalit Chattar
 */
public class GetCDMMainDataAction extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	
	/*
	 * Parameter getting from request
	 */
	
	private String sEcho;
	private Integer iTotalRecords;
	private Integer iTotalDisplayRecords;
	private List<Map<String, String>> aaData;
	
	private int constant, end, start;
	private StringBuilder sWhere;
	
	private String[] dateArray;
	private String[] numberArray;
	private String[] aColumnsSer;
	private String sOrderBy = null;
	private String chrgType;

	private String modNameMain;
	private String modNameGrp;
	private String modNamedel;
    private String import_status;
    private String export_status;
    private String libName;
	/*
	 * Getter and Setter

	*/	
	public String getChrgType() {
		return chrgType;
	}

	public void setChrgType(String chrgType) {
		this.chrgType = chrgType;
	}

	public String getsEcho() {
		return sEcho;
	}

	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}
	public Integer getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(Integer iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public List<Map<String, String>> getAaData() {
		return aaData;
	}

	public void setAaData(List<Map<String, String>> aaData) {
		this.aaData = aaData;
	}

	public GetCDMMainDataAction(){
		cdmrController = new CDMRController();
		aaData = new ArrayList<Map<String,String>>();
	}
		
	
	public String getModNameMain() {
		return modNameMain;
	}

	public void setModNameMain(String modNameMain) {
		this.modNameMain = modNameMain;
	}

	public String getModNameGrp() {
		return modNameGrp;
	}

	public void setModNameGrp(String modNameGrp) {
		this.modNameGrp = modNameGrp;
	}

	public String getModNamedel() {
		return modNamedel;
	}

	public void setModNamedel(String modNamedel) {
		this.modNamedel = modNamedel;
	}
	

	public String getImport_status() {
		return import_status;
	}

	public void setImport_status(String importStatus) {
		import_status = importStatus;
	}

	public String getExport_status() {
		return export_status;
	}

	public void setExport_status(String exportStatus) {
		export_status = exportStatus;
	}
	
	public String getLibName() {
		return libName;
	}

	public void setLibName(String libName) {
		this.libName = libName;
	}

	/*
	 * Default method of action class
	 */
	public String execute() throws Exception {
		return SUCCESS;
	}
		
	/**
	 * @description load data from cdm main to charge library datatable
	 * @return String
	 * @throws Exception
	 */
	
	public String fetchCdmMainData()throws Exception {
		
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			
			
			aColumnsSer = CDMRUtil.fetchPropertyVal(jsonObj.getString("FILE_ID")+"_COLS");
			dateArray = CDMRUtil.fetchPropertyVal(jsonObj.getString("FILE_ID")+"_DATEARY");
			numberArray = CDMRUtil.fetchPropertyVal(jsonObj.getString("FILE_ID")+"_NUMARY");
						
			constant = Integer.parseInt(jsonObj.getString("iDisplayLength"));
			end = Integer.parseInt(jsonObj.getString("iDisplayStart")) + Integer.parseInt(jsonObj.getString("iDisplayLength"));
			start = Integer.parseInt(jsonObj.getString("iDisplayStart"))+1;
			
			if(jsonObj.getString("FILE_ID").equalsIgnoreCase("ALLLIB"))
			{
				jsonObj = fetchChgIndcBySeq(jsonObj);	//return  key:CHG_ID, Value:NULL 
				jsonObj = CDMRUtil.fetchEventLibId(jsonObj);	//return 	Key:EVENT_ID, Value: 1,2,3,4	
				jsonObj = CDMRUtil.getActiveInactiveRule(jsonObj);	//return Key:ACTDACTRULE, Value:DATE
				jsonObj = CDMRUtil.getMapVersion(jsonObj); 	//return Key:MAPSVERSION, Value:5
		        jsonObj = getColumnFromStagingMap(jsonObj); //return Key:columnsNames, Value: list
		        
		        String[] evntIdArry = jsonObj.getString("EVENT_ID").split(",");

		        StringBuilder query = new StringBuilder();
		        int count = 1 ;
		        for (String evntId: evntIdArry) {         
		        	jsonObj.put("EVENT_ID", evntId);
		        	
		        	//String libName = this.fetchLibraryName(jsonObj);
		        	libName = CDMRUtil.fetchPropertyValStr(this.fetchLibraryName(jsonObj) + "_LibName");
		        	
		        	this.chrgType = ("and EVENT_LIB_ID IN (" + jsonObj.getString("EVENT_ID") + ") ");
					jsonObj = CDMRUtil.getCurrentVersionAllLib(jsonObj);	//return Key:versionid
					
			        if ((jsonObj.get("preprvver") != null) && (!jsonObj.getString("preprvver").equals("null"))) {
			          jsonObj.put("versionid", jsonObj.getString("preprvver"));
			        }					
			        jsonObj = createQueryForDefaultListAllLib(jsonObj);
			        
			        if(evntIdArry.length > count){
			        	query.append(jsonObj.getString("query") +" union ");	
			        	count++;
			        }else{
			        	query.append(jsonObj.getString("query"));
			        }
			        
		        }     
		        jsonObj.put("query", query.toString());
				jsonObj = createFinalQueryForDefaultListAllLib(jsonObj);
				jsonObj = this.cdmrController.getMatchedData(jsonObj);
		        
		        processDataForList(jsonObj);
			}
			else
			{
			jsonObj = fetchChgIndcBySeq(jsonObj);
			jsonObj = CDMRUtil.fetchEventLibId(jsonObj);
			
			jsonObj = CDMRUtil.getActiveInactiveRule(jsonObj);			
			
			 if (jsonObj.getString("chgtype").equals("1"))
		      {
		        this.chrgType = (" and EVENT_LIB_ID = '" + jsonObj.getString("EVENT_ID") + "' ");
		        jsonObj = CDMRUtil.getCurrentVersion(jsonObj);
		        if ((jsonObj.get("preprvver") != null) && (!jsonObj.getString("preprvver").equals("null"))) {
		          jsonObj.put("versionid", jsonObj.getString("preprvver"));
		        }
		        jsonObj = CDMRUtil.getMapVersion(jsonObj);
		        jsonObj = getColumnFromStagingMap(jsonObj);
		        jsonObj = createQueryForDefaultList(jsonObj);
		        jsonObj = this.cdmrController.getMatchedData(jsonObj);
		        
		        processDataForList(jsonObj);
		      }
		      else
		      {
		        if (jsonObj.has("CHG_ID") == true) {
		          this.chrgType = ("and CHG_IND = '" + jsonObj.getString("CHG_ID") + "' " + " and EVENT_LIB_ID = '" + jsonObj.getString("EVENT_ID") + "' ");
		        } else {
		          this.chrgType = ("and EVENT_LIB_ID = '" + jsonObj.getString("EVENT_ID") + "' ");
		        }
		        jsonObj = CDMRUtil.getCurrentVersion(jsonObj);
		        if ((jsonObj.get("preprvver") != null) && (!jsonObj.getString("preprvver").equals("null"))) {
		          jsonObj.put("versionid", jsonObj.getString("preprvver"));
		        }
		        jsonObj = CDMRUtil.getMapVersion(jsonObj);
		        jsonObj = getColumnFromStagingMap(jsonObj);
		        jsonObj = createQueryForDefaultList(jsonObj);
		        jsonObj = this.cdmrController.getMatchedData(jsonObj);
		        
		        processDataForList(jsonObj);
		      }
			}
			
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	/*public String getCdmMainData()throws Exception {
	
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			
			aColumnsSer = getPropertyVal(jsonObj.getString("FILE_ID")+"_COLS");
			dateArray = getPropertyVal(jsonObj.getString("FILE_ID")+"_DATEARY");
			numberArray = getPropertyVal(jsonObj.getString("FILE_ID")+"_NUMARY");
			
			
			constant = Integer.parseInt(jsonObj.getString("iDisplayLength"));
			end = Integer.parseInt(jsonObj.getString("iDisplayStart")) + Integer.parseInt(jsonObj.getString("iDisplayLength"));
			start = Integer.parseInt(jsonObj.getString("iDisplayStart"))+1;
			
			
			if(jsonObj.getString("chgtype").equals("1")){
				jsonObj = this.getCurrentVersion(jsonObj);
				if(!(jsonObj.get("preprvver") == null || jsonObj.getString("preprvver").equals("null"))){
					jsonObj.put("versionid", jsonObj.getString("preprvver"));
				}
				jsonObj = this.getMapVersion(jsonObj);
				jsonObj = this.getColumnFromStagingMap(jsonObj);
				jsonObj = this.createQueryForDefaultList(jsonObj);
				jsonObj = cdmrController.getMatchedData(jsonObj);
				this.processDataForList(jsonObj);
			}else if(jsonObj.getString("chgtype").equals("2")){
				chrgType = "and CHG_IND = 'T' ";
				jsonObj = this.getCurrentVersion(jsonObj);
				if(!(jsonObj.get("preprvver") == null || jsonObj.getString("preprvver").equals("null"))){
					jsonObj.put("versionid", jsonObj.getString("preprvver"));
				}
				jsonObj = this.getMapVersion(jsonObj);
				jsonObj = this.getColumnFromStagingMap(jsonObj);
				jsonObj = this.createQueryForDefaultList(jsonObj);
				jsonObj = cdmrController.getMatchedData(jsonObj);
				this.processDataForList(jsonObj);
			}else if(jsonObj.getString("chgtype").equals("3")){
				chrgType = "and CHG_IND = 'P' ";
				jsonObj = this.getCurrentVersion(jsonObj);
				if(!(jsonObj.get("preprvver") == null || jsonObj.getString("preprvver").equals("null"))){
					jsonObj.put("versionid", jsonObj.getString("preprvver"));
				}
				jsonObj = this.getMapVersion(jsonObj);
				jsonObj = this.getColumnFromStagingMap(jsonObj);
				jsonObj = this.createQueryForDefaultList(jsonObj);
				jsonObj = cdmrController.getMatchedData(jsonObj);
				this.processDataForList(jsonObj);
			}
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}*/
	
	/**
	 * @description Get Latest version of Data in cdm main
	 * @return JSONObject
	 * @throws Exception
	 */
	/*public JSONObject getCurrentVersion(JSONObject jsonObj) throws Exception{
		try {
			String query = "select version_id from cdm_versiondef where is_current = 1 and FILE_ID = '"+jsonObj.getString("EVENT_ID")+"' ";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getCDMVersionDetail(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("versioninfo");
			Map dataMap = list.get(0);
			jsonObj.put("versionid", dataMap.get("VERSION_ID").toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObj;
	}
	*/
	
	/**
	 * @description Create dynamic query for default list view in charge library
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject createQueryForDefaultList(JSONObject jsonObj) throws Exception{
		try {
			
			List<String> dateList = Arrays.asList(dateArray);
			List<String> numberList = Arrays.asList(numberArray);
			String columnNames = "";
			List<Map> columnsNameMap = (List<Map>)jsonObj.get("columnsNames");
			for (Map map : columnsNameMap) {
				String type = map.get("MAINCDM_COLTYPE") == null? "":map.get("MAINCDM_COLTYPE").toString();
				if(type.equalsIgnoreCase("TIMESTAMP")){
					columnNames = columnNames + "TO_CHAR( " +map.get("EQMAINCDM_COL").toString() + ", 'dd-MON-YYYY') as " +map.get("EQMAINCDM_COL").toString()  + ",";
				}
				//for showing charge value up to two decimal places
				else if(type.equalsIgnoreCase("NUMERIC")){
					columnNames = columnNames + "TO_CHAR( " +map.get("EQMAINCDM_COL").toString() + ", '999G999G999G990D99') as " +map.get("EQMAINCDM_COL").toString()  + ",";					
				}
				else{
					columnNames = columnNames +map.get("EQMAINCDM_COL").toString()+ ",";
				}
				
			}
			columnNames = columnNames.substring(0, (columnNames.length()-1));
			
			sWhere = new StringBuilder();
			if ( jsonObj.getString("sSearch") != "" ){
				sWhere.append(" and (");
				for ( int i=0 ; i<aColumnsSer.length ; i++ )
				{
					if(aColumnsSer[i].contains("DATE") || aColumnsSer[i].contains("date")){
						sWhere.append("LOWER(TO_CHAR("+aColumnsSer[i]+", 'dd-MON-YYYY')) LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
					}else{
						sWhere.append("LOWER("+aColumnsSer[i]+") LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
					}
					
				}
				sWhere = sWhere.replace((sWhere.length()-3), sWhere.length(), "");
				sWhere.append(")");
			}
			
			if (jsonObj.has("iSortCol_0")){
				sOrderBy = " ORDER BY  ";
				for ( int i=0 ; i<jsonObj.getInt("iSortingCols") ; i++ )
				{
					if((jsonObj.getString("bSortable_" + jsonObj.getString("iSortCol_"+i))).equals("true")){
						
						String columnName = "";
						if(numberList.contains(jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0")))){
							columnName = "TO_NUMBER(" +jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0"))+ ")";
						}else{
							columnName = jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0"));
						}
						sOrderBy = sOrderBy + columnName + " " + (jsonObj.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
					}
				}
				
				sOrderBy = sOrderBy.substring(0, (sOrderBy.length()-2));
				if ( sOrderBy.equals(" ORDER BY") )
				{
					sOrderBy = sOrderBy + " PK_CDM_MAIN";
				}
				
			}
			
			
			StringBuilder query = new StringBuilder();
			String activerule = jsonObj.getString("ACTDACTRULE");
			if (activerule.equals("DATE"))
			{
			
				if(jsonObj.getString("chgstat").equals("1")){
					query.append("select TotalRows, PK_CDM_MAIN, "+columnNames+" from (SELECT t.*, Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(*) OVER () AS TotalRows FROM CDM_MAIN t where");
					query.append(" IS_THIS_SS = 0 " +chrgType+ "and EFF_FROM_DATE IS NOT NULL and DELETEDFLAG = 0 "+sWhere+" and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where (CUR_VERSION <= "+jsonObj.getString("versionid")+") group by LSERVICE_CODE, CHG_IND,EVENT_LIB_ID)AND ((TO_DATE((TO_CHAR(EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') > TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY') or TO_DATE((TO_CHAR(EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') is null)OR (TO_DATE((TO_CHAR(EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') <= TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY'))AND ((TO_DATE((TO_CHAR(EFF_FROM_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') > TO_DATE((TO_CHAR(EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY'))AND (TO_DATE((TO_CHAR(EFF_FROM_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') <= TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY')))) " +AdvanceSearchUtil.getNewAdvanceSearchCriteria(jsonObj)+" ) WHERE MyRow BETWEEN "+start+" AND "+end);
				}else if(jsonObj.getString("chgstat").equals("2")){
					query.append("select TotalRows, PK_CDM_MAIN, "+columnNames+" from (SELECT t.*, Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(*) OVER () AS TotalRows FROM CDM_MAIN t where");
					query.append(" IS_THIS_SS = 0 " +chrgType+ "and EFF_FROM_DATE IS NOT NULL and DELETEDFLAG = 0 "+sWhere+" and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where (CUR_VERSION <= "+jsonObj.getString("versionid")+") group by LSERVICE_CODE, CHG_IND,EVENT_LIB_ID)AND (TO_DATE((TO_CHAR(EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') <= TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY') AND TO_DATE((TO_CHAR(EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') IS NOT NULL AND TO_DATE((TO_CHAR(EFF_FROM_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') <= TO_DATE((TO_CHAR(EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY')OR (TO_DATE((TO_CHAR(EFF_FROM_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') >= TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY')))" + AdvanceSearchUtil.getNewAdvanceSearchCriteria(jsonObj)+" ) WHERE MyRow BETWEEN "+start+" AND "+end);
				}else{
					query.append("select TotalRows, PK_CDM_MAIN, "+columnNames+" from (SELECT t.*, Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(*) OVER () AS TotalRows FROM CDM_MAIN t where");
					query.append(" IS_THIS_SS = 0 " +chrgType+ "and EFF_FROM_DATE IS NOT NULL and DELETEDFLAG = 0 "+sWhere+" and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where (CUR_VERSION <= "+jsonObj.getString("versionid")+") group by LSERVICE_CODE, CHG_IND,EVENT_LIB_ID) " + AdvanceSearchUtil.getNewAdvanceSearchCriteria(jsonObj)+" ) WHERE MyRow BETWEEN "+start+" AND "+end);
				}
				
			}
			else
				if (activerule.equals("FLAG"))
				{
					if(jsonObj.getString("chgstat").equals("1")){
						query.append("select TotalRows, PK_CDM_MAIN, "+columnNames+" from (SELECT t.*, Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(*) OVER () AS TotalRows FROM CDM_MAIN t where");
						query.append(" IS_THIS_SS = 0 " +chrgType+ "and EFF_FROM_DATE IS NOT NULL and DELETEDFLAG = 0 "+sWhere+" and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where (CUR_VERSION <= "+jsonObj.getString("versionid")+") group by LSERVICE_CODE, CHG_IND,EVENT_LIB_ID)AND IS_ACTIVE_YN = 'Y' " + AdvanceSearchUtil.getNewAdvanceSearchCriteria(jsonObj)+" ) WHERE MyRow BETWEEN "+start+" AND "+end);
					}else if(jsonObj.getString("chgstat").equals("2")){
						query.append("select TotalRows, PK_CDM_MAIN, "+columnNames+" from (SELECT t.*, Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(*) OVER () AS TotalRows FROM CDM_MAIN t where");
						query.append(" IS_THIS_SS = 0 " +chrgType+ "and EFF_FROM_DATE IS NOT NULL and DELETEDFLAG = 0 "+sWhere+" and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where (CUR_VERSION <= "+jsonObj.getString("versionid")+") group by LSERVICE_CODE, CHG_IND,EVENT_LIB_ID)AND IS_ACTIVE_YN = 'N' " + AdvanceSearchUtil.getNewAdvanceSearchCriteria(jsonObj)+" ) WHERE MyRow BETWEEN "+start+" AND "+end);
					}else{
						query.append("select TotalRows, PK_CDM_MAIN, "+columnNames+" from (SELECT t.*, Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(*) OVER () AS TotalRows FROM CDM_MAIN t where");
						query.append(" IS_THIS_SS = 0 " +chrgType+ "and EFF_FROM_DATE IS NOT NULL and DELETEDFLAG = 0 "+sWhere+" and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where (CUR_VERSION <= "+jsonObj.getString("versionid")+") group by LSERVICE_CODE, CHG_IND,EVENT_LIB_ID) " + AdvanceSearchUtil.getNewAdvanceSearchCriteria(jsonObj)+" ) WHERE MyRow BETWEEN "+start+" AND "+end);
					}
				
				}
			
			jsonObj.put("query", query.toString());
			jsonObj.put("SQLQuery", true);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return jsonObj;
	}
	
	
	public String getAdvanceSearchCriteria(JSONObject jsonObj) throws Exception{
		String query = "";
		try {
			if(jsonObj.getBoolean("advanceSearch")){
				Iterator<?> keys = jsonObj.keys();
				
				while( keys.hasNext() ) {
				    String key = (String)keys.next();
				    if(key.contains("_AS") && !jsonObj.getString(key).equalsIgnoreCase("")){
				    	if(key.contains("DATE")){
				    		query = query + " AND " + key.replace("_AS", "") + " = '" +jsonObj.getString(key) + "'";
				    	}else{
				    		query = query + " AND " + key.replace("_AS", "") + " like '%" +jsonObj.getString(key) + "%'";
				    	}
				    	
				    }
				}
				
				return query;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return query;
	}
	
	/**
	 * @description Process data for displaying in datatable
	 * @return void
	 * @throws Exception
	 */
	public void processDataForList(JSONObject jsonObject) throws Exception{
		try{
			String activerule = jsonObject.getString("ACTDACTRULE");
			List<Map> dataList = (List<Map>)jsonObject.get("searched");
			for (Map map : dataList) {
				Set keySet = map.keySet();
				Map<String, String> data = new HashMap<String, String>();
				data.put("check", "<input type=\"checkbox\" onclick=\"addInArray(this.value);\" value=\""+map.get("PK_CDM_MAIN").toString()+"\" name=\"check\"/>");
				long days = 0;
				if(map.get("EFF_TO_DATE") != null){
					days = CDMRUtil.getDifferenceBetweenDate(map.get("EFF_TO_DATE").toString());
				}
				for (Object key : keySet) {
					
					if(key.toString().equalsIgnoreCase("TOTALROWS") || key.toString().equalsIgnoreCase("PK_CDM_MAIN")){
						this.iTotalDisplayRecords = Integer.parseInt(map.get("TOTALROWS").toString());
						this.iTotalRecords = Integer.parseInt(map.get("TOTALROWS").toString());
						continue;
					}
					if (activerule.equals("DATE"))
					{
						if(days >= 0 && map.get("EFF_TO_DATE") != null){
							long diffBetDate = CDMRUtil.getDifferenceBetweenTwoDates(map.get("EFF_FROM_DATE").toString(), map.get("EFF_TO_DATE").toString());
							long diffBBtwnFromAndPrest = CDMRUtil.getDifferenceBetweenDate(map.get("EFF_FROM_DATE").toString());
							if(diffBetDate > 0 && diffBBtwnFromAndPrest>=0){
								
								if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
									data.put(key.toString(), "N/A");
								}else{
									data.put(key.toString(),  map.get(key.toString()).toString());
								}
							}else{
								
								if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
									data.put(key.toString(), "<strike style='color:red'>N/A</strike>");
								}else{
									data.put(key.toString(), "<strike style='color:red'>" + map.get(key.toString()).toString() + "</strike>");
								}
							}
							
						}else{
							
							if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
								data.put(key.toString(), "N/A");
							}else{
								data.put(key.toString(),  map.get(key.toString()).toString());
							}
						}
					}
					else
					if (activerule.equals("FLAG"))
					{
						if(map.get("IS_ACTIVE_YN").toString().equalsIgnoreCase("N"))
						{		
							if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
								data.put(key.toString(), "<strike style='color:red'>N/A</strike>");
							}else{
								data.put(key.toString(), "<strike style='color:red'>" + map.get(key.toString()).toString() + "</strike>");
							}
						}
							
						else
						{
							
							if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
								data.put(key.toString(), "N/A");
							}else{
								data.put(key.toString(),  map.get(key.toString()).toString());
							}
						}
					
					}
				}
				
				aaData.add(data);
			
			}
			
			if(aaData.isEmpty()){
				this.iTotalDisplayRecords = 0;
				this.iTotalRecords = 0;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		this.sEcho= jsonObject.getString("sEcho");
	}
	

	/**
	 * @description Get Latest map version 
	 * @return JSONObject
	 * @throws Exception
	 */
	/*public JSONObject getMapVersion(JSONObject jsonObj) throws Exception{
		try {
			String vermap = CDMRUtil.fetchPropertyValStr(jsonObj.getString("FILE_ID")+"_MAPS");
			String query="select MAPSVERSION from CDM_MAPSVERSION where IS_ACTIVE=1 and DELETEDFLAG=0 and FILE_ID IN("+vermap+")";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getMapVersion(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("mapversion");		
			jsonObj.put("MAPSVERSION", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObj;
	}*/
	
	/**
	 * @description Get Columns from Staging Maps
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getColumnFromStagingMap(JSONObject jsonObj) throws Exception{
		try {
			
			List<Map> versionList = (List<Map>)jsonObj.get("MAPSVERSION");
			String versionInfo = "";
			for (Map map : versionList) {
				versionInfo = versionInfo + map.get("MAPSVERSION").toString() + ",";
			}
			versionInfo = versionInfo.substring(0, (versionInfo.length()-1));
			String query = "select distinct(EQMAINCDM_COL),MAINCDM_COLTYPE from CDM_STAGINGMAPS where FK_CDM_MAPSVERSION in ("+versionInfo+") and DISP_INCDM = 1";
			jsonObj.put("query", query);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getStagingMaps(jsonObj);
			List<Map> list = (List<Map>)jsonObj.get("mapversion");
			jsonObj.put("columnsNames", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObj;
	}
	
	
	/*public String[] fetchPropertyVal(String fileID) throws Exception{
		String[] strCols = null;
			
		Properties prop = new Properties();
		InputStream input = null;

		try {
			
			prop.load(this.getClass().getClassLoader().getResourceAsStream("/cdmr.properties"));
			strCols = prop.getProperty(fileID).split(",");
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return strCols;
	  }
	public String fetchPropertyValStr(String fileID) throws Exception{
		String strCols = null;
			
		Properties prop = new Properties();
		InputStream input = null;

		try {
			
			prop.load(this.getClass().getClassLoader().getResourceAsStream("/cdmr.properties"));
			strCols = prop.getProperty(fileID);
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return strCols;
	  }*/

     public String fetchModuleMain() throws Exception {
		
		
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			CDMRController cdmrController = new CDMRController();
			
			String query = "select CODELST_VALUE from VBAR_CODELST where CODELST_TYPE='MODNAME' and CODELST_DESC = 'CHG_LIB' and  CODELST_SUBTYP='"
					+ jsonObject.getString("FILE_ID") + "'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getModulename(jsonObject);
			List<Map> modulename = (List<Map>) jsonObject.get("module_name");
			modNameMain = modulename.get(0).get("CODELST_VALUE").toString();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";

	}
	public String fetchModuleGrp() throws Exception {
		
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			CDMRController cdmrController = new CDMRController();
			
			String query = "select CODELST_VALUE from VBAR_CODELST where CODELST_TYPE='MODNAME' and CODELST_DESC = 'CHG_LIBGRP' and  CODELST_SUBTYP='"
					+ jsonObject.getString("FILE_ID") + "'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getModulename(jsonObject);
			List<Map> modulename = (List<Map>) jsonObject.get("module_name");
			modNameGrp = modulename.get(0).get("CODELST_VALUE").toString();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";

	}
	public String fetchModuleEdit() throws Exception {
		
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			CDMRController cdmrController = new CDMRController();
			
			String query = "select CODELST_VALUE from VBAR_CODELST where CODELST_TYPE='MODNAME' and CODELST_DESC = 'CHG_LIBDEL' and  CODELST_SUBTYP='"
					+ jsonObject.getString("FILE_ID") + "'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getModulename(jsonObject);
			List<Map> modulename = (List<Map>) jsonObject.get("module_name");
			modNamedel = modulename.get(0).get("CODELST_VALUE").toString();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";

	}
	
	
	public String fetchChgtyp() throws Exception{
		try{
			StringBuffer buffer = new StringBuffer();
			
			CDMRController cdmrController = new CDMRController();
			JSONObject jsonObject = new JSONObject();
			String query = "select CODELST_DESC,CODELST_SEQ from VBAR_CODELST where CODELST_TYPE = 'CHG_TYPE' ORDER BY CODELST_SEQ";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getchgtype(jsonObject);
			List<Map> listObj = (List<Map>)jsonObject.get("chg_type");
			for (Map map : listObj) {
		
				buffer.append("<option value=\""+map.get("CODELST_SEQ").toString()+"\">"+map.get("CODELST_DESC").toString()+"</option>");
			}
			this.setChrgType(buffer.toString());
			
		}
				
		 catch (Exception e) {
				e.printStackTrace();
			}
		 return "SUCCESS";
	
	}
	
	public JSONObject fetchChgIndcBySeq(JSONObject jsonObject) throws Exception{
		try{
			
			CDMRController cdmrController = new CDMRController();
			String query = "select CODELST_SUBTYP from VBAR_CODELST where CODELST_TYPE = 'CHG_TYPE' and CODELST_SEQ = '"
				+jsonObject.getString("chgtype")+"'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getChgIndcBySeq(jsonObject);
			
			List<Map> chgidc = (List<Map>) jsonObject.get("chgseqind");			
			 if (chgidc.size() > 0) {
		        jsonObject.put("CHG_ID", ((Map)chgidc.get(0)).get("CODELST_SUBTYP").toString());
		      }
		}
				
		 catch (Exception e) {
				e.printStackTrace();
			}
		 return jsonObject;
	
	}
	public String getTransferstatus() throws Exception {
		try {
			import_status=CDMRUtil.ChkFileTransferStatus("'"+"IMPORT_DATA"+"'");
			export_status=CDMRUtil.ChkFileTransferStatus("'"+"CRMS_DATA_TRANSFER"+"'");			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";
	}
		
	/*public JSONObject fetchEventLibId(JSONObject jsonObject) throws Exception{
		try{			
			
			CDMRController cdmrController = new CDMRController();
			
			String query = "select CODELST_DESC from VBAR_CODELST where CODELST_TYPE = 'EVENT_LIB_ID' and CODELST_SUBTYP = '"
				+ jsonObject.getString("FILE_ID") + "'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.fetchEventLibId(jsonObject);
			
			List<Map> chgidc = (List<Map>) jsonObject.get("eventlid");
			jsonObject.put("EVENT_ID", chgidc.get(0).get("CODELST_DESC").toString());
			
		}
				
		 catch (Exception e) {
				e.printStackTrace();
			}
		 return jsonObject;
	
	}*/
	
	/**
	 * @description Create dynamic query for default list view in charge library
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject createQueryForDefaultListAllLib(JSONObject jsonObj) throws Exception{
		try {
			
			List<String> dateList = Arrays.asList(dateArray);
			List<String> numberList = Arrays.asList(numberArray);
			
			sWhere = new StringBuilder();
			if ( jsonObj.getString("sSearch") != "" ){
				sWhere.append(" and (");
				for ( int i=0 ; i<aColumnsSer.length ; i++ )
				{
					if(!aColumnsSer[i].equalsIgnoreCase("LIB_NAME")){					
						if(aColumnsSer[i].contains("DATE") || aColumnsSer[i].contains("date")){
							sWhere.append("LOWER(TO_CHAR("+aColumnsSer[i]+", 'dd-MON-YYYY')) LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
						}else{
							sWhere.append("LOWER("+aColumnsSer[i]+") LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
						}
					}
				}
				sWhere = sWhere.replace((sWhere.length()-3), sWhere.length(), "");
				sWhere.append(")");
			}
			
			if (jsonObj.has("iSortCol_0")){
				sOrderBy = " ORDER BY  ";
				for ( int i=0 ; i<jsonObj.getInt("iSortingCols") ; i++ )
				{
					if((jsonObj.getString("bSortable_" + jsonObj.getString("iSortCol_"+i))).equals("true")){
						
						String columnName = "";
						if(numberList.contains(jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0")))){
							columnName = "TO_NUMBER(" +jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0"))+ ")";
						}else{
							columnName = jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0"));
						}
						sOrderBy = sOrderBy + columnName + " " + (jsonObj.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
					}
				}
				
				sOrderBy = sOrderBy.substring(0, (sOrderBy.length()-2));
				if ( sOrderBy.equals(" ORDER BY") )
				{
					sOrderBy = sOrderBy + " PK_CDM_MAIN";
				}
				
			}
			
			
			StringBuilder query = new StringBuilder();
			String activerule = jsonObj.getString("ACTDACTRULE");
			if (activerule.equals("DATE"))
			{
			
				if(jsonObj.getString("chgstat").equals("1")){
					query.append("SELECT "+libName+" LIB_NAME, t.* FROM CDM_MAIN t where");
					query.append(" IS_THIS_SS = 0 " +chrgType+ "and EFF_FROM_DATE IS NOT NULL and DELETEDFLAG = 0 "+sWhere+" and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where (CUR_VERSION <= "+jsonObj.getString("versionid")+") group by LSERVICE_CODE, CHG_IND,EVENT_LIB_ID)AND ((TO_DATE((TO_CHAR(EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') > TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY') or TO_DATE((TO_CHAR(EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') is null)OR (TO_DATE((TO_CHAR(EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') <= TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY'))AND ((TO_DATE((TO_CHAR(EFF_FROM_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') > TO_DATE((TO_CHAR(EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY'))AND (TO_DATE((TO_CHAR(EFF_FROM_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') <= TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY')))) " +AdvanceSearchUtil.getNewAdvanceSearchCriteria(jsonObj));
				}else if(jsonObj.getString("chgstat").equals("2")){
					query.append("SELECT "+libName+" LIB_NAME, t.* FROM CDM_MAIN t where");
					query.append(" IS_THIS_SS = 0 " +chrgType+ "and EFF_FROM_DATE IS NOT NULL and DELETEDFLAG = 0 "+sWhere+" and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where (CUR_VERSION <= "+jsonObj.getString("versionid")+") group by LSERVICE_CODE, CHG_IND,EVENT_LIB_ID)AND (TO_DATE((TO_CHAR(EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') <= TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY') AND TO_DATE((TO_CHAR(EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') IS NOT NULL AND TO_DATE((TO_CHAR(EFF_FROM_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') <= TO_DATE((TO_CHAR(EFF_TO_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY')OR (TO_DATE((TO_CHAR(EFF_FROM_DATE, 'DD-MON-YYYY')),'DD-MON-YYYY') >= TO_DATE((TO_CHAR(Sysdate, 'DD-MON-YYYY')),'DD-MON-YYYY')))" + AdvanceSearchUtil.getNewAdvanceSearchCriteria(jsonObj));
				}else{
					query.append("SELECT "+libName+" LIB_NAME, t.* FROM CDM_MAIN t where");
					query.append(" IS_THIS_SS = 0 " +chrgType+ "and EFF_FROM_DATE IS NOT NULL and DELETEDFLAG = 0 "+sWhere+" and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where (CUR_VERSION <= "+jsonObj.getString("versionid")+") group by LSERVICE_CODE, CHG_IND,EVENT_LIB_ID) " + AdvanceSearchUtil.getNewAdvanceSearchCriteria(jsonObj));
				}
				
			}
			else
				if (activerule.equals("FLAG"))
				{
					if(jsonObj.getString("chgstat").equals("1")){
						query.append("SELECT t.* FROM CDM_MAIN t where");
						query.append(" IS_THIS_SS = 0 " +chrgType+ "and EFF_FROM_DATE IS NOT NULL and DELETEDFLAG = 0 "+sWhere+" and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where (CUR_VERSION <= "+jsonObj.getString("versionid")+") group by LSERVICE_CODE, CHG_IND,EVENT_LIB_ID)AND IS_ACTIVE_YN = 'Y' " + AdvanceSearchUtil.getNewAdvanceSearchCriteria(jsonObj));
					}else if(jsonObj.getString("chgstat").equals("2")){
						query.append("SELECT t.* FROM CDM_MAIN t where");
						query.append(" IS_THIS_SS = 0 " +chrgType+ "and EFF_FROM_DATE IS NOT NULL and DELETEDFLAG = 0 "+sWhere+" and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where (CUR_VERSION <= "+jsonObj.getString("versionid")+") group by LSERVICE_CODE, CHG_IND,EVENT_LIB_ID)AND IS_ACTIVE_YN = 'N' " + AdvanceSearchUtil.getNewAdvanceSearchCriteria(jsonObj));
					}else{
						query.append("SELECT t.* FROM CDM_MAIN t where");
						query.append(" IS_THIS_SS = 0 " +chrgType+ "and EFF_FROM_DATE IS NOT NULL and DELETEDFLAG = 0 "+sWhere+" and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where (CUR_VERSION <= "+jsonObj.getString("versionid")+") group by LSERVICE_CODE, CHG_IND,EVENT_LIB_ID) " + AdvanceSearchUtil.getNewAdvanceSearchCriteria(jsonObj));
					}
				
				}
			
			jsonObj.put("query", query.toString());
			jsonObj.put("SQLQuery", true);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return jsonObj;
	}
	
	/**
	 * @description Create dynamic query for default list view in charge library
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject createFinalQueryForDefaultListAllLib(JSONObject jsonObj) throws Exception{
		try {
			
			List<String> dateList = Arrays.asList(dateArray);
			List<String> numberList = Arrays.asList(numberArray);
			String columnNames = "";
			List<Map> columnsNameMap = (List<Map>)jsonObj.get("columnsNames");
			for (Map map : columnsNameMap) {
				String type = map.get("MAINCDM_COLTYPE") == null? "":map.get("MAINCDM_COLTYPE").toString();
				if(type.equalsIgnoreCase("TIMESTAMP")){
					columnNames = columnNames + "TO_CHAR( " +map.get("EQMAINCDM_COL").toString() + ", 'dd-MON-YYYY') as " +map.get("EQMAINCDM_COL").toString()  + ",";
				}
				//for showing charge value up to two decimal places
				else if(type.equalsIgnoreCase("NUMERIC")){
					columnNames = columnNames + "TO_CHAR( " +map.get("EQMAINCDM_COL").toString() + ", '999G999G999G990D99') as " +map.get("EQMAINCDM_COL").toString()  + ",";					
				}
				else{
					columnNames = columnNames +map.get("EQMAINCDM_COL").toString()+ ",";
				}
				
			}
			columnNames = columnNames.substring(0, (columnNames.length()-1));
						
			
			StringBuilder query = new StringBuilder();
			String activerule = jsonObj.getString("ACTDACTRULE");
			if (activerule.equals("DATE"))
			{
			
				if(jsonObj.getString("chgstat").equals("1")){
					query.append("SELECT Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(*) OVER () AS TotalRows, PK_CDM_MAIN, "+columnNames+" FROM ( " + jsonObj.getString("query")+")");
				}else if(jsonObj.getString("chgstat").equals("2")){
					query.append("SELECT Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(*) OVER () AS TotalRows, PK_CDM_MAIN, "+columnNames+" FROM ( " + jsonObj.getString("query")+")");
				}else{
					query.append("SELECT Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(*) OVER () AS TotalRows, PK_CDM_MAIN, "+columnNames+" FROM ( " + jsonObj.getString("query")+")");
				}
				
			}
			else
				if (activerule.equals("FLAG"))
				{
					if(jsonObj.getString("chgstat").equals("1")){
						query.append("SELECT Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(*) OVER () AS TotalRows, PK_CDM_MAIN, "+columnNames+" FROM ( " + jsonObj.getString("query")+")");
					}else if(jsonObj.getString("chgstat").equals("2")){
						query.append("SELECT Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(*) OVER () AS TotalRows, PK_CDM_MAIN, "+columnNames+" FROM ( " + jsonObj.getString("query")+")");
					}else{
						query.append("SELECT Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(*) OVER () AS TotalRows, PK_CDM_MAIN, "+columnNames+" FROM ( " + jsonObj.getString("query")+")");
					}
				
				}
			
			jsonObj.put("query", query.toString());
			jsonObj.put("SQLQuery", true);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return jsonObj;
	}
	
	public String fetchLibraryName(JSONObject jsonObject) throws Exception {
		
		try {
			
			CDMRController cdmrController = new CDMRController();
			
			String query = "select CODELST_SUBTYP from VBAR_CODELST where CODELST_TYPE='EVENT_LIB_ID' and CODELST_DESC='"
					+ jsonObject.getString("EVENT_ID") + "'";
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", true);
			jsonObject = cdmrController.getLibraryName(jsonObject);  
			
			List<Map> libName = (List<Map>) jsonObject.get("lib_name");
			jsonObject.put("lib_name", libName.get(0).get("CODELST_SUBTYP").toString());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject.getString("lib_name");

	}
	
	public String getCurrentLibName(){
		
		JSONObject jsonObj = new JSONObject();
		jsonObj = VelosUtil.constructJSON(request);
		
		try {			
			if(jsonObj.has("EVENT_ID")){				
				libName = CDMRUtil.fetchPropertyValStr(this.fetchLibraryName(jsonObj) + "_LibName");
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return SUCCESS;
	} 
	
}
