package com.velos.stafa.action.cdmr;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.VelosUtil;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;



/**
 * @summary     CDMRHomeAction
 * @description load home page after successful login
 * @version     1.0
 * @file        CDMRHomeAction.java
 * @author      Lalit Chattar
 */
public class CDMRHomeAction extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	HttpSession httpSession;
	
	/*
	 * Parameter getting from request
	 */
	private String user;
	private List lstTaskAction;
	private List lstTaskAsign;
	private String transferValue;
	private String terminateValue;
	private String selfAsignValue;
	private List lstNurseUsers;
	
	public CDMRHomeAction(){
		cdmrController = new CDMRController();
	}
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return loadCDMRHomePage();
	}
	
	/**
	 * @description load home page
	 * @return String
	 * @throws Exception
	 */
	public String loadCDMRHomePage()throws Exception {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject = VelosUtil.constructJSON(request);
			String query = CDMRSqlConstants.FETCH_CODELIST_DATA;
			jsonObject.put("query", query);
			jsonObject.put("SQLQuery", false);
			jsonObject = cdmrController.getCodeList(jsonObject);
			httpSession = request.getSession();
			httpSession.setAttribute("codelist", jsonObject.get("codelist"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	/*
	 * Getter and Setter
	 */
	public List getLstTaskAction() {
		return lstTaskAction;
	}

	public void setLstTaskAction(List lstTaskAction) {
		this.lstTaskAction = lstTaskAction;
	}

	

	public String getModel(){
		return SUCCESS;
	}
	

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public List getLstTaskAsign() {
		return lstTaskAsign;
	}

	public void setLstTaskAsign(List lstTaskAsign) {
		this.lstTaskAsign = lstTaskAsign;
	}

	public String getTransferValue() {
		return transferValue;
	}

	public String getTerminateValue() {
		return terminateValue;
	}

	public String getSelfAsignValue() {
		return selfAsignValue;
	}

	public void setTransferValue(String transferValue) {
		this.transferValue = transferValue;
	}

	public void setTerminateValue(String terminateValue) {
		this.terminateValue = terminateValue;
	}

	public void setSelfAsignValue(String selfAsignValue) {
		this.selfAsignValue = selfAsignValue;
	}

	public List getLstNurseUsers() {
		return lstNurseUsers;
	}

	public void setLstNurseUsers(List lstNurseUsers) {
		this.lstNurseUsers = lstNurseUsers;
	}
	
	
}
