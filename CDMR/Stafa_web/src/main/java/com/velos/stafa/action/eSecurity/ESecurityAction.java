/**
*
*/
package com.velos.stafa.action.eSecurity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONArray;
import atg.taglib.json.util.JSONObject;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.business.util.WebserviceUtil;
import com.velos.stafa.controller.CommonController;
import com.velos.stafa.objects.UserDetailsObject;
import com.velos.stafa.objects.eSecurity.EsecurityUserDetailsObject;
import com.velos.stafa.util.VelosStafaConstants;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.util.eSecurity.ESecurityConstants;
import com.velos.stafa.util.eSecurity.esecWebServiceUtil;
import com.velos.stafa.business.util.VelosConstants;

/**
 * @author vlakshmipriya
 *
 */
public class ESecurityAction extends StafaBaseAction{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final Log log = LogFactory.getLog(ESecurityAction.class);
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	public String errorMessage = "";
	EsecurityUserDetailsObject esecUserObject = new EsecurityUserDetailsObject(); 
	UserDetailsObject userObject = new UserDetailsObject();
	VelosUtil velosUtil = new VelosUtil();
	HttpSession session = null;
	public ArrayList returnList;
	public ArrayList roleList = new ArrayList();	
	public ArrayList firstNamelst = new ArrayList();
	public ArrayList lastNamelst = new ArrayList();
	public String resultJson;
	private List countryList;
	private List jobTypeList;
	private List timeZoneList;


	public List getTimeZoneList() {
		return timeZoneList;
	}
	public void setTimeZoneList(List timeZoneList) {
		this.timeZoneList = timeZoneList;
	}
	public String getResultJson() {
		return resultJson;
	}
	public void setResultJson(String resultJson) {
		this.resultJson = resultJson;
	}
	public ArrayList getRoleList() {
		return roleList;
	}
	public void setRoleList(ArrayList roleList) {
		this.roleList = roleList;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
	
	public String saveUserDetails()throws Exception{
		log.info("ESecurityAction : saveUserDetails Method Start ");
		String userId = request.getParameter("userId");
		String loginName = request.getParameter("login");
		String password = request.getParameter("password");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String phone = request.getParameter("phone");
		String addr1 = request.getParameter("addr1");
		String addr2 = request.getParameter("addr2");
		String city = request.getParameter("city");
		String emailId = request.getParameter("email");
		String state = request.getParameter("state");
		String country = request.getParameter("country");
		String zipcode = request.getParameter("postalCode");
		String userIsActive = request.getParameter("status")==null?"1":request.getParameter("status");
		String securityQuestion = request.getParameter("securityQue");
		String answer = request.getParameter("answer");
		String esign = request.getParameter("esign");
		String clientUserId = request.getParameter("clientUserId");
		String accExpDate = request.getParameter("accExpDate");
		String jobType=request.getParameter("jobType");
		String timeZone=request.getParameter("timeZone");
		String acctIsActive=request.getParameter("acctIsActive");
		log.debug("userId->"+userId+",loginName->"+loginName+",firstName->"+firstName+",lastName->"+lastName+",phone->"+phone);
		log.debug("addr1->"+addr1+",addr2->"+addr2+",city->"+city+",emailId->"+emailId+",state->"+state+",country->"+country+",zipcode->"+zipcode+",timeZone->"+timeZone);
		log.debug("userIsActive->"+userIsActive+"accExpDate"+accExpDate+",jobType"+jobType);
		try{
			ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
			session=request.getSession(false);
			log.debug(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			StringBuffer moduleRightsXml = new StringBuffer();
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:saveUsers>");
				xmlIp.append("<user> <application>esecurity</application>");
				xmlIp.append("<"+ESecurityConstants.userId+">"+userId+"</"+ESecurityConstants.userId+">");
				xmlIp.append("<"+ESecurityConstants.login+">"+loginName+"</"+ESecurityConstants.login+">");
				xmlIp.append("<"+ESecurityConstants.password+">"+password+"</"+ESecurityConstants.password+">");
				xmlIp.append("<"+ESecurityConstants.firstName+">"+firstName+"</"+ESecurityConstants.firstName+">");
				xmlIp.append("<"+ESecurityConstants.lastName+">"+lastName+"</"+ESecurityConstants.lastName+">");
				xmlIp.append("<"+ESecurityConstants.phone+">"+phone+"</"+ESecurityConstants.phone+">");
				xmlIp.append("<"+ESecurityConstants.addr1+">"+addr1+"</"+ESecurityConstants.addr1+">");
				xmlIp.append("<"+ESecurityConstants.addr2+">"+addr2+"</"+ESecurityConstants.addr2+">");
				xmlIp.append("<"+ESecurityConstants.city+">"+city+"</"+ESecurityConstants.city+">");
				xmlIp.append("<"+ESecurityConstants.email+">"+emailId+"</"+ESecurityConstants.email+">");
				xmlIp.append("<"+ESecurityConstants.state+">"+state+"</"+ESecurityConstants.state+">");
				xmlIp.append("<"+ESecurityConstants.country+">"+country+"</"+ESecurityConstants.country+">");
				xmlIp.append("<"+ESecurityConstants.postalCode+">"+zipcode+"</"+ESecurityConstants.postalCode+">");
				xmlIp.append("<"+ESecurityConstants.securityQue+">"+securityQuestion+"</"+ESecurityConstants.securityQue+">");
				xmlIp.append("<"+ESecurityConstants.answer+">"+answer+"</"+ESecurityConstants.answer+">");
				xmlIp.append("<"+ESecurityConstants.userIsActive+">"+userIsActive+"</"+ESecurityConstants.userIsActive+">");
				xmlIp.append("<"+ESecurityConstants.acctIsActive+">"+acctIsActive+"</"+ESecurityConstants.acctIsActive+">");
				xmlIp.append("<"+ESecurityConstants.clientUserId+">"+clientUserId+"</"+ESecurityConstants.clientUserId+">");
				xmlIp.append("<"+ESecurityConstants.accExpDate+">"+accExpDate+"</"+ESecurityConstants.accExpDate+">");
				xmlIp.append("<"+ESecurityConstants.esign+">"+esign+"</"+ESecurityConstants.esign+">");
				xmlIp.append("<"+ESecurityConstants.jobType+">"+jobType+"</"+ESecurityConstants.jobType+">");
				xmlIp.append("<"+ESecurityConstants.timeZone+">"+timeZone+"</"+ESecurityConstants.timeZone+">");
				xmlIp.append("</user> </ser:saveUsers> </soapenv:Body></soapenv:Envelope>");
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				log.debug("Result --->" + result);
				String strXmlValue = WebserviceUtil.getXmlValue(result, "result", "saveUsers");
				log.debug("strXmlValue --->" + strXmlValue);
			}
			esecUserObject.setRightsXml(moduleRightsXml.toString());
			session.setAttribute(ESecurityConstants.ESEC_USER_OBJECT,esecUserObject);
			log.info("ESecurityAction : saveUserDetails Method End ");
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String loadUserDetails()throws Exception{
		log.info("ESecurityAction : loadUserDetails Method Start ");
		ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
		session=request.getSession(false);
		userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
		String loginName = "";
		if(request.getParameter("userName") != null && !request.getParameter("userName").equalsIgnoreCase("") && !request.getParameter("userName").equalsIgnoreCase("undefined")){
			loginName = request.getParameter("userName");
		}
		else{
			loginName = userObject.getLoginName();
		}
		log.debug("loginName->"+loginName);
		try{
			log.debug(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:getUsers>");
				xmlIp.append("<"+ESecurityConstants.login+">"+loginName+"</"+ESecurityConstants.login+">");
				xmlIp.append(" <firstName/> <lastName/></ser:getUsers> </soapenv:Body></soapenv:Envelope>");
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				log.debug("Result --->" + result);
				esecUserObject = esecWebServiceUtil.getXmlValueObject(result, "result");
				JSONObject jsonObject = new JSONObject();
				JSONArray jsonArray = new JSONArray();
				
				String criteria = " isHide = 'N' and type='country' order by sequence,description";
				jsonObject.put("Domain", CODELST_DOMAIN);
				jsonObject.put("criteria", criteria);
				jsonArray.put(0,jsonObject);
				
				jsonObject = new JSONObject();
				criteria = " isHide = 'N' and type='jobtype' order by sequence,description";
				jsonObject.put("Domain", CODELST_DOMAIN);
				jsonObject.put("criteria", criteria);
				jsonArray.put(1,jsonObject);
				
				jsonObject = new JSONObject();
				criteria = " isHide = 'N' and type='timezone' order by sequence,description";
				jsonObject.put("Domain", CODELST_DOMAIN);
				jsonObject.put("criteria", criteria);
				jsonArray.put(2,jsonObject);
				
				System.out.println("$$$$$$$$$$$$$$$$" + jsonArray);
				List esecDropDownlst=(List)CommonController.getObjectList(jsonArray);
				System.out.println("$$$$$$$$$$$$$$" + esecDropDownlst);
				countryList=(List)esecDropDownlst.get(0);
				jobTypeList=(List)esecDropDownlst.get(1);
				timeZoneList=(List)esecDropDownlst.get(2);
			}
			
			log.info("ESecurityAction : loadUserDetails Method End ");
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String addEsecUser()throws Exception{
		try{
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			
			String criteria = " codeListHide = 'N' and codeListType='country' order by codeListSeq,codeListDesc";
			jsonObject.put("Domain", VelosConstants.CODELST_DOMAIN);
			jsonObject.put("criteria", criteria);
			jsonArray.put(0,jsonObject);
			
			jsonObject = new JSONObject();
			criteria = " codeListHide = 'N' and codeListType='jobtype' order by codeListSeq,codeListDesc";
			jsonObject.put("Domain", VelosConstants.CODELST_DOMAIN);
			jsonObject.put("criteria", criteria);
			jsonArray.put(1,jsonObject);
			
			jsonObject = new JSONObject();
			criteria = " codeListHide = 'N' and codeListType='timezone' order by codeListSeq,codeListDesc";
			jsonObject.put("Domain", VelosConstants.CODELST_DOMAIN);
			jsonObject.put("criteria", criteria);
			jsonArray.put(2,jsonObject);
			
			System.out.println("$$$$$$$$$$$$$$$$" + jsonArray);
			List esecDropDownlst=(List)CommonController.getObjectList(jsonArray);
			System.out.println("$$$$$$$$$$$$$$" + esecDropDownlst);
			countryList=(List)esecDropDownlst.get(0);
			System.out.println("*************" + countryList);
			jobTypeList=(List)esecDropDownlst.get(1);
			System.out.println("*************" + jobTypeList);
			timeZoneList=(List)esecDropDownlst.get(2);
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String passwordValidation(String oldPassword, String newPassword, String oldEsign, String newEsign)throws Exception{
		log.info("ESecurityAction : passwordValidation Method Start ");
		String strXmlValue = "";
		ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
		session=request.getSession(false);
		userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
		String loginName = userObject.getLoginName();
		log.debug("loginName->"+loginName);
		try{
			log.debug(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:checkAuthentication>");
				xmlIp.append("<user><application>Stafa</application>");
				xmlIp.append("<"+ESecurityConstants.login+">"+loginName+"</"+ESecurityConstants.login+">");
				xmlIp.append("<"+ESecurityConstants.password+">"+oldPassword+"</"+ESecurityConstants.password+">");
				xmlIp.append("<"+ESecurityConstants.newpassword+">"+newPassword+"</"+ESecurityConstants.newpassword+">");
				xmlIp.append("<"+ESecurityConstants.esign+">"+oldEsign+"</"+ESecurityConstants.esign+">");
				xmlIp.append("<"+ESecurityConstants.newesign+">"+newEsign+"</"+ESecurityConstants.newesign+">");
				xmlIp.append("</user></ser:checkAuthentication> </soapenv:Body></soapenv:Envelope>");
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				log.debug("Result --->" + result);
				strXmlValue = WebserviceUtil.getXmlValue(result, "result","statusCode");
			}
			log.info("ESecurityAction : passwordValidation Method End ");
		}catch(Exception e){
			e.printStackTrace();
		}
		return strXmlValue;
	}

	public String updateUserDetails()throws Exception{
		log.info("ESecurityAction : updateUserDetails Method Start ");
		String firstName = request.getParameter("firstName") == null ? "":request.getParameter("firstName");
		String lastName = request.getParameter("lastName") == null ? "":request.getParameter("lastName");
		String phone = request.getParameter("phone") == null ? "":request.getParameter("phone");
		String addr1 = request.getParameter("addr1") == null ? "":request.getParameter("addr1");
		String addr2 = request.getParameter("addr2") == null ? "":request.getParameter("addr2");
		String city = request.getParameter("city") == null ? "":request.getParameter("city");
		String emailId = request.getParameter("email") == null ? "":request.getParameter("email");
		String state = request.getParameter("state") == null ? "":request.getParameter("state");
		String country = request.getParameter("country") == null ? "":request.getParameter("country");
		String zipcode = request.getParameter("postalCode") == null ? "":request.getParameter("postalCode");
		String clientUserId = request.getParameter("clientUserId") == null ? "":request.getParameter("clientUserId");
		String password = "";
		String esign = "";
		String oldPassword = request.getParameter("oldPassword") == null ? "":request.getParameter("oldPassword");
		String newPassword = request.getParameter("newPassword") == null ? "":request.getParameter("newPassword");
		String oldEsign = request.getParameter("oldEsign") == null ? "":request.getParameter("oldEsign");
		String newEsign = request.getParameter("newEsign") == null ? "":request.getParameter("newEsign");
		String userIsActive = request.getParameter("status") == null ? "1":request.getParameter("status");
		String passwordDays = request.getParameter("passwordDays") == null ? "":request.getParameter("passwordDays");
		String accExpDate = request.getParameter("accExpDate")== null ? "":request.getParameter("accExpDate");
		String jobType = request.getParameter("jobType")==null ?"":request.getParameter("jobType");
		String timeZone = request.getParameter("timeZone")==null ?"":request.getParameter("timeZone");
		String acctIsActive=request.getParameter("acctIsActive")==null?"":request.getParameter("acctIsActive");
		log.debug("firstName->"+firstName+",lastName->"+lastName+",phone->"+phone);
		log.debug("addr1->"+addr1+",addr2->"+addr2+",city->"+city+",emailId->"+emailId+",state->"+state+",country->"+country+",zipcode->"+zipcode+",status"+userIsActive);
		try{
			ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
			session=request.getSession(false);		
			userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
			String loginName = userObject.getLoginName();
			Integer changePassword = request.getParameter("changePassword")== null ? 0 : Integer.parseInt(request.getParameter("changePassword"));
			Integer changeeSign = request.getParameter("changeeSign")== null ? 0 : Integer.parseInt(request.getParameter("changeeSign"));
			if((changePassword == 1) || (changeeSign == 1)){
				String result = passwordValidation(oldPassword,newPassword,oldEsign,newEsign);
				log.debug("result-->"+result);
				if(!result.equalsIgnoreCase("SUCCESS")){
					errorMessage = "Invaild Password/E-Sign";
					log.debug("errorMessage->"+errorMessage);
					return ERROR;
				}
				else{
					if(newPassword != null && !newPassword.equals("")){
						password = newPassword;
					}
					if(newEsign != null && !newEsign.equals("")){
						esign = newEsign;
					}
				}
			}
			log.debug(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			StringBuffer moduleRightsXml = new StringBuffer();
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:updateUser>");
				xmlIp.append("<user> <application>esecurity</application>");
				xmlIp.append("<"+ESecurityConstants.login+">"+loginName+"</"+ESecurityConstants.login+">");
				xmlIp.append("<"+ESecurityConstants.password+">"+password+"</"+ESecurityConstants.password+">");
				xmlIp.append("<"+ESecurityConstants.firstName+">"+firstName+"</"+ESecurityConstants.firstName+">");
				xmlIp.append("<"+ESecurityConstants.lastName+">"+lastName+"</"+ESecurityConstants.lastName+">");
				xmlIp.append("<"+ESecurityConstants.phone+">"+phone+"</"+ESecurityConstants.phone+">");
				xmlIp.append("<"+ESecurityConstants.addr1+">"+addr1+"</"+ESecurityConstants.addr1+">");
				xmlIp.append("<"+ESecurityConstants.addr2+">"+addr2+"</"+ESecurityConstants.addr2+">");
				xmlIp.append("<"+ESecurityConstants.city+">"+city+"</"+ESecurityConstants.city+">");
				xmlIp.append("<"+ESecurityConstants.email+">"+emailId+"</"+ESecurityConstants.email+">");
				xmlIp.append("<"+ESecurityConstants.state+">"+state+"</"+ESecurityConstants.state+">");
				xmlIp.append("<"+ESecurityConstants.country+">"+country+"</"+ESecurityConstants.country+">");
				xmlIp.append("<"+ESecurityConstants.postalCode+">"+zipcode+"</"+ESecurityConstants.postalCode+">");
				xmlIp.append("<"+ESecurityConstants.esign+">"+esign+"</"+ESecurityConstants.esign+">");
				xmlIp.append("<"+ESecurityConstants.passwordDays+">"+passwordDays+"</"+ESecurityConstants.passwordDays+">");
				xmlIp.append("<"+ESecurityConstants.clientUserId+">"+clientUserId+"</"+ESecurityConstants.clientUserId+">");
				xmlIp.append("<"+ESecurityConstants.accExpDate+">"+accExpDate+"</"+ESecurityConstants.accExpDate+">");
				xmlIp.append("<"+ESecurityConstants.userIsActive+">"+userIsActive+"</"+ESecurityConstants.userIsActive+">");
				xmlIp.append("<"+ESecurityConstants.acctIsActive+">"+acctIsActive+"</"+ESecurityConstants.acctIsActive+">");
				xmlIp.append("<"+ESecurityConstants.jobType+">"+jobType+"</"+ESecurityConstants.jobType+">");
				xmlIp.append("<"+ESecurityConstants.timeZone+">"+timeZone+"</"+ESecurityConstants.timeZone+">");
				xmlIp.append("</user> </ser:updateUser> </soapenv:Body></soapenv:Envelope>");
				log.debug("xmlIp Request"+xmlIp.toString());
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				log.debug("Result --->" + result);
				String strXmlValue = WebserviceUtil.getXmlValue(result, "result", "updateUser");
				log.debug("strXmlValue --->" + strXmlValue);
			}
			errorMessage = null;
			esecUserObject.setRightsXml(moduleRightsXml.toString());
			session.setAttribute(ESecurityConstants.ESEC_USER_OBJECT,esecUserObject);
			log.info("ESecurityAction : saveUserDetails Method End ");
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String loadModuleTreeDetails() throws Exception{
		log.info("ESecurityAction : loadModuleTreeDetails Method Start ");
		String userName = request.getParameter("userName") == null ? "":request.getParameter("userName");
		String nameType = request.getParameter("nameType") == null ? "":request.getParameter("nameType");
		log.debug("nameType--->"+nameType);
		try{
			ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
			session=request.getSession(false);		
			userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
			log.debug(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:getModuleTree>");
				xmlIp.append("<"+ESecurityConstants.loginOrRoleName+">"+userName+"</"+ESecurityConstants.loginOrRoleName+">");
				xmlIp.append("<"+ESecurityConstants.nameType+">"+nameType+"</"+ESecurityConstants.nameType+">");
				xmlIp.append("</ser:getModuleTree> </soapenv:Body></soapenv:Envelope>");
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				log.debug("Result --->" + result);
				roleList = esecWebServiceUtil.getXmlRoleValue(result, "result");
				log.debug("roleList --->" + roleList);
			}
			log.info("ESecurityAction : loadModuleTreeDetails Method End ");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String saveModuleRights() throws Exception{
		log.info("ESecurityAction : saveModuleRights Method Start ");
		JSONObject mainJSON = new JSONObject();
		Long maxlength = 0l;
		try{
			ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
			session=request.getSession(false);		
			userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
			log.debug(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			mainJSON = VelosUtil.constructJSON(request);
			String userName = mainJSON.getString("userName") == null ? "":mainJSON.getString("userName");
			String description = mainJSON.getString("description") == null ? "":mainJSON.getString("description");
			String transType = mainJSON.getString("transType") == null ? "":mainJSON.getString("transType");
			log.debug("mainJson->"+mainJSON);
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:saveModuleRights>");
				xmlIp.append("<"+ESecurityConstants.loginOrRoleName+">"+userName+"</"+ESecurityConstants.loginOrRoleName+">");
				xmlIp.append("<"+ESecurityConstants.description+">"+description+"</"+ESecurityConstants.description+">");
				xmlIp.append("<"+ESecurityConstants.type+">"+transType+"</"+ESecurityConstants.type+">");
				log.debug("transType-->"+transType);
				if(mainJSON.has("maxlength") && !mainJSON.getString("maxlength").equals("")){
					maxlength = mainJSON.getLong("maxlength");
				}
				for(int i = 0; i < maxlength; i++){
					xmlIp.append("<user>");
					if(mainJSON.has("moduleName-"+i+"")){
						xmlIp.append("<"+ESecurityConstants.moduleName+">"+mainJSON.getString("moduleName-"+i+"")+"</"+ESecurityConstants.moduleName+">");
					}
					String view = "",add = "", edit = "", delete = "",rights = "";
					if(mainJSON.has("view-"+i) && !mainJSON.getString("view-"+i).equals("")){
						view = mainJSON.getString("view-"+i);
					}
					if(mainJSON.has("add-"+i) && !mainJSON.getString("add-"+i).equals("")){
						add = mainJSON.getString("add-"+i);
					}
					if(mainJSON.has("edit-"+i) && !mainJSON.getString("edit-"+i).equals("")){
						edit = mainJSON.getString("edit-"+i);
					}
					if(mainJSON.has("delete-"+i) && !mainJSON.getString("delete-"+i).equals("")){
						delete = mainJSON.getString("delete-"+i);
					}
					rights = view + add + edit + delete; 
					log.debug("rights->"+rights);
					xmlIp.append("<"+ESecurityConstants.rights+">"+rights+"</"+ESecurityConstants.rights+"></user>");
				}
				xmlIp.append("</ser:saveModuleRights> </soapenv:Body></soapenv:Envelope>");
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				log.debug("Result --->" + result);
				roleList = esecWebServiceUtil.getXmlRoleValue(result, "result");
				log.debug("roleList --->" + roleList);
			}
			log.info("ESecurityAction : saveModuleRights Method End ");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String loadAllUser()throws Exception{
		log.info("ESecurityAction : loadAllUser Method Start ");
		ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
		session=request.getSession(false);
		userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
		String loginName = "%";
		try{
			log.debug(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:getUsers>");
				xmlIp.append("<"+ESecurityConstants.login+">"+loginName+"</"+ESecurityConstants.login+">");
				xmlIp.append(" <firstName/> <lastName/></ser:getUsers> </soapenv:Body></soapenv:Envelope>");
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				log.debug("Result --->" + result.toString());			
				roleList = esecWebServiceUtil.getUserValue(result, "user");
				log.debug("roleList --->" + roleList);
			}
			log.info("ESecurityAction : loadAllUser Method End ");
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String loadAllRoles()throws Exception{
		log.info("ESecurityAction : loadAllRoles Method Start ");
		ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
		session=request.getSession(false);
		userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
		try{
			log.debug(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:getAllRoles>");
				xmlIp.append("</ser:getAllRoles> </soapenv:Body></soapenv:Envelope>");
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				log.debug("Result --->" + result.toString());
				roleList = esecWebServiceUtil.getRoleValue(result, "result");
				log.debug("roleList --->" + roleList);
			}
			log.info("ESecurityAction : getAllRoles Method End ");
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String loadUserByRoleCode()throws Exception{
		log.info("ESecurityAction : loadUserByRoleCode Method Start ");
		String roleCode = request.getParameter("roleCode");
		ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
		session=request.getSession(false);
		log.debug("roleCode --->" + roleCode);
		userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
		try{
			log.debug(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:getUserByRoleCode>");
				xmlIp.append("<"+ESecurityConstants.roleCode+">"+roleCode+"</"+ESecurityConstants.roleCode+">");
				xmlIp.append("</ser:getUserByRoleCode> </soapenv:Body></soapenv:Envelope>");
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				log.debug("Result --->" + result);
				roleList = esecWebServiceUtil.getXmlValueForESec(result, ESecurityConstants.userName);
			}
			log.info("ESecurityAction : loadUserByRoleCode Method End ");
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String loadUserNotInRoleCode()throws Exception{
		log.info("ESecurityAction : loadUserNotInRoleCode Method Start ");
		String roleCode = request.getParameter("roleCode");
		ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
		session=request.getSession(false);
		log.debug("roleCode --->" + roleCode);
		userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
		try{
			log.debug(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:getUserNotInRoleCode>");
				xmlIp.append("<"+ESecurityConstants.roleCode+">"+roleCode+"</"+ESecurityConstants.roleCode+">");
				xmlIp.append("</ser:getUserNotInRoleCode> </soapenv:Body></soapenv:Envelope>");
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				log.debug("Result --->" + result);
				System.out.println("**********"+ result);
				roleList = esecWebServiceUtil.getXmlValueForESec(result, ESecurityConstants.userName);
				firstNamelst = esecWebServiceUtil.getXmlValueForESec(result, ESecurityConstants.firstName);
				lastNamelst = esecWebServiceUtil.getXmlValueForESec(result, ESecurityConstants.lastName);
			}
		
			log.info("ESecurityAction : loadUserNotInRoleCode Method End ");
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String saveRoleUser() throws Exception{
		log.info("ESecurityAction : saveRoleUser Method Start ");
		String roleName = request.getParameter("roleName");
		String description = request.getParameter("desc");
		String userNames = request.getParameter("userNames");
		log.debug("roleName->"+roleName+"description->"+description+"userNames->"+userNames);
		try{
			ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
			session=request.getSession(false);		
			userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
			log.debug(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:saveRoleUser>");
				xmlIp.append("<"+ESecurityConstants.roleCode+">"+roleName+"</"+ESecurityConstants.roleCode+">");
				xmlIp.append("<"+ESecurityConstants.description+">"+description+"</"+ESecurityConstants.description+">");
				String users[] = userNames.split(",");
				for(int i = 0; i < users.length; i++){
					xmlIp.append("<"+ESecurityConstants.userLoginName+">"+users[i]+"</"+ESecurityConstants.userLoginName+">");
				}
				xmlIp.append("</ser:saveRoleUser> </soapenv:Body></soapenv:Envelope>");
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				log.debug("Result --->" + result);
				roleList = esecWebServiceUtil.getXmlRoleValue(result, "result");
				log.debug("roleList --->" + roleList);
			}
			log.info("ESecurityAction : saveRoleUser Method End ");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String resetPassword() throws Exception{
		log.info("ESecurityAction : resetPassword Method Start ");
		String loginName = request.getParameter("loginName") == null ? "":request.getParameter("loginName");
		try{
			ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
			session=request.getSession(false);		
			userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
			log.debug(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:resetPassword>");
				xmlIp.append("<"+ESecurityConstants.login+">"+loginName+"</"+ESecurityConstants.login+">");
				xmlIp.append("</ser:resetPassword> </soapenv:Body></soapenv:Envelope>");
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				log.debug("Result --->" + result);
				roleList = esecWebServiceUtil.getXmlRoleValue(result, "result");
				log.debug("roleList --->" + roleList);
			}
			log.info("ESecurityAction : resetPassword Method End ");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String reseteSign() throws Exception{
		log.info("ESecurityAction : reseteSign Method Start ");
		String loginName = request.getParameter("loginName") == null ? "":request.getParameter("loginName");
		String strXmlValue = "";
		try{
			ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
			session=request.getSession(false);		
			userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
			log.debug(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:reseteSign>");
				xmlIp.append("<"+ESecurityConstants.login+">"+loginName+"</"+ESecurityConstants.login+">");
				xmlIp.append("</ser:reseteSign> </soapenv:Body></soapenv:Envelope>");
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				log.debug("Result --->" + result);
				strXmlValue = WebserviceUtil.getXmlValue(result, "result", "statusCode");
				log.debug("roleList --->" + roleList);
			}
			log.info("ESecurityAction : reseteSign Method End ");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return strXmlValue;
	}
	
	public String updateAuthentication()throws Exception{
		log.info("ESecurityAction : updateAuthentication Method Start ");
		ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
		session=request.getSession(false);
		userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
		JSONObject mainJSON = new JSONObject();
		log.debug("mainJSON->"+mainJSON);
		String strXmlValue = "";
		try{
			log.debug(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			mainJSON = VelosUtil.constructJSON(request);
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:updateAuthentication>");
				xmlIp.append("<user><application>Stafa</application>");
				xmlIp.append("<"+ESecurityConstants.login+">"+mainJSON.getString("loginName")+"</"+ESecurityConstants.login+">");
				xmlIp.append("<"+ESecurityConstants.password+">"+mainJSON.getString("oldPassword")+"</"+ESecurityConstants.password+">");
				xmlIp.append("<"+ESecurityConstants.newpassword+">"+mainJSON.getString("newPassword")+"</"+ESecurityConstants.newpassword+">");
				xmlIp.append("<"+ESecurityConstants.passwordDays+">"+mainJSON.getString("passwordDays")+"</"+ESecurityConstants.passwordDays+">");
				xmlIp.append("<"+ESecurityConstants.esign+"></"+ESecurityConstants.esign+">");
				xmlIp.append("<"+ESecurityConstants.newesign+"></"+ESecurityConstants.newesign+">");
				xmlIp.append("</user></ser:updateAuthentication> </soapenv:Body></soapenv:Envelope>");
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				strXmlValue = WebserviceUtil.getXmlValue(result, "result","statusCode");
				log.debug("Result --->" + result);
				if(!strXmlValue.equalsIgnoreCase("SUCCESS")){
					errorMessage = "Invaild Password/E-Sign";
					log.debug("errorMessage->"+errorMessage);
					return ERROR;
				}
			}
			log.info("ESecurityAction : updateAuthentication Method End ");
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String updateUser()throws Exception{
		log.info("ESecurityAction : updateUser Method Start ");
		String firstName = request.getParameter("firstName") == null ? "":request.getParameter("firstName");
		String lastName = request.getParameter("lastName") == null ? "":request.getParameter("lastName");
		String phone = request.getParameter("phone") == null ? "":request.getParameter("phone");
		String addr1 = request.getParameter("addr1") == null ? "":request.getParameter("addr1");
		String addr2 = request.getParameter("addr2") == null ? "":request.getParameter("addr2");
		String city = request.getParameter("city") == null ? "":request.getParameter("city");
		String emailId = request.getParameter("email") == null ? "":request.getParameter("email");
		String state = request.getParameter("state") == null ? "":request.getParameter("state");
		String country = request.getParameter("country") == null ? "":request.getParameter("country");
		String zipcode = request.getParameter("postalCode") == null ? "":request.getParameter("postalCode");
		String password = request.getParameter("password") == null ? "":request.getParameter("password");
		String esign = request.getParameter("esign") == null ? "":request.getParameter("esign");
		String loginName = request.getParameter("loginName") == null ? "":request.getParameter("loginName");
		String clientUserId = request.getParameter("clientUserId") == null ? "":request.getParameter("clientUserId");
		String userIsActive = request.getParameter("status") == null ? "1":request.getParameter("status");
		String accExpDate = request.getParameter("accExpDate") == null ? "":request.getParameter("accExpDate");
		String jobType=request.getParameter("jobType") == null ? "":request.getParameter("jobType");
		String timeZone=request.getParameter("timeZone") == null ? "":request.getParameter("timeZone");
		String acctIsActive=request.getParameter("acctIsActive") == null ? "":request.getParameter("acctIsActive");
		log.debug("firstName->"+firstName+",lastName->"+lastName+",phone->"+phone+",jobType"+jobType);
		log.debug("addr1->"+addr1+",addr2->"+addr2+",city->"+city+",emailId->"+emailId+",state->"+state+",country->"+country+",zipcode->"+zipcode+",timeZone->"+timeZone);
		try{
			ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
			session=request.getSession(false);		
			log.debug(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:updateUser>");
				xmlIp.append("<user> <application>esecurity</application>");
				xmlIp.append("<"+ESecurityConstants.login+">"+loginName+"</"+ESecurityConstants.login+">");
				xmlIp.append("<"+ESecurityConstants.password+">"+password+"</"+ESecurityConstants.password+">");
				xmlIp.append("<"+ESecurityConstants.firstName+">"+firstName+"</"+ESecurityConstants.firstName+">");
				xmlIp.append("<"+ESecurityConstants.lastName+">"+lastName+"</"+ESecurityConstants.lastName+">");
				xmlIp.append("<"+ESecurityConstants.phone+">"+phone+"</"+ESecurityConstants.phone+">");
				xmlIp.append("<"+ESecurityConstants.addr1+">"+addr1+"</"+ESecurityConstants.addr1+">");
				xmlIp.append("<"+ESecurityConstants.addr2+">"+addr2+"</"+ESecurityConstants.addr2+">");
				xmlIp.append("<"+ESecurityConstants.city+">"+city+"</"+ESecurityConstants.city+">");
				xmlIp.append("<"+ESecurityConstants.email+">"+emailId+"</"+ESecurityConstants.email+">");
				xmlIp.append("<"+ESecurityConstants.state+">"+state+"</"+ESecurityConstants.state+">");
				xmlIp.append("<"+ESecurityConstants.country+">"+country+"</"+ESecurityConstants.country+">");
				xmlIp.append("<"+ESecurityConstants.postalCode+">"+zipcode+"</"+ESecurityConstants.postalCode+">");
				xmlIp.append("<"+ESecurityConstants.esign+">"+esign+"</"+ESecurityConstants.esign+">");
				xmlIp.append("<"+ESecurityConstants.passwordDays+"></"+ESecurityConstants.passwordDays+">");
				xmlIp.append("<"+ESecurityConstants.clientUserId+">"+clientUserId+"</"+ESecurityConstants.clientUserId+">");
				xmlIp.append("<"+ESecurityConstants.accExpDate+">"+accExpDate+"</"+ESecurityConstants.accExpDate+">");
				xmlIp.append("<"+ESecurityConstants.userIsActive+">"+userIsActive+"</"+ESecurityConstants.userIsActive+">");
				xmlIp.append("<"+ESecurityConstants.acctIsActive+">"+acctIsActive+"</"+ESecurityConstants.acctIsActive+">");
				xmlIp.append("<"+ESecurityConstants.jobType+">"+jobType+"</"+ESecurityConstants.jobType+">");
				xmlIp.append("<"+ESecurityConstants.timeZone+">"+timeZone+"</"+ESecurityConstants.timeZone+">");
				xmlIp.append("</user> </ser:updateUser> </soapenv:Body></soapenv:Envelope>");
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				log.debug("Result --->" + result);
				String strXmlValue = WebserviceUtil.getXmlValue(result, "result", "updateUser");
				log.debug("strXmlValue --->" + strXmlValue);
			}
			errorMessage = null;
			session.setAttribute(ESecurityConstants.ESEC_USER_OBJECT,esecUserObject);
			log.info("ESecurityAction : updateUser Method End ");
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String resetUserAccount() throws Exception{
		log.info("ESecurityAction : resetUserAccount Method Start ");
		String loginName = request.getParameter("loginName") == null ? "":request.getParameter("loginName");
		String strXmlValue = "";
		try{
			ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
			session=request.getSession(false);		
			userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
			log.debug(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:resetUserAccount>");
				xmlIp.append("<"+ESecurityConstants.login+">"+loginName+"</"+ESecurityConstants.login+">");
				xmlIp.append("</ser:resetUserAccount> </soapenv:Body></soapenv:Envelope>");
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				log.debug("Result --->" + result);
				strXmlValue = WebserviceUtil.getXmlValue(result, "result", "statusCode");
				log.debug("roleList --->" + roleList);
			}
			log.info("ESecurityAction : resetUserAccount Method End ");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return strXmlValue;
	}
	public String savePolicy() throws Exception{
		log.info("ESecurityAction : savePolicy Method Start ");
		try{
			String jsonData = request.getParameter("jsonData");
			log.debug("jsonData" + jsonData);
			JSONObject jsonObject = new JSONObject(jsonData);
			if(jsonObject.has("policyData")){
				JSONArray policyArr =jsonObject.getJSONArray("policyData");
				ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
				session=request.getSession(false);		
				userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
				log.debug(resource.getKeys());
				String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
				if(eSecurityFlag.equalsIgnoreCase("true")){
					String esecurityURL = resource.getString(ESECURITY_URL);
					log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
					StringBuffer xmlIp = new StringBuffer();
					xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
					xmlIp.append("<soapenv:Header/><soapenv:Body><ser:savePolicyDetails>");
					for(int i=0;i<policyArr.length();i++){
						JSONObject policyData = policyArr.getJSONObject(i);
						xmlIp.append("<"+ESecurityConstants.policy+">");
						xmlIp.append("<"+ESecurityConstants.code+">"+policyData.getString("code")+"</"+ESecurityConstants.code+">");
						xmlIp.append("<"+ESecurityConstants.sequence+">"+policyData.getString("sequence")+"</"+ESecurityConstants.sequence+">");
						xmlIp.append("<"+ESecurityConstants.value+">"+policyData.getString("value")+"</"+ESecurityConstants.value+">");
						xmlIp.append("<"+ESecurityConstants.visible+">1</"+ESecurityConstants.visible+">");
						xmlIp.append("</"+ESecurityConstants.policy+">");
					}
					xmlIp.append("</ser:savePolicyDetails> </soapenv:Body></soapenv:Envelope>");
					StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
					log.debug("Result --->" + result);
					roleList = esecWebServiceUtil.getXmlRoleValue(result, "result");
					log.debug("roleList --->" + roleList);
				}
			}
			log.info("ESecurityAction : savePolicy Method End ");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String loadAllPolicyDetails()throws Exception{
		log.info("ESecurityAction : loadPolicyDetails Method Start ");
		ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
		session=request.getSession(false);
		userObject = (UserDetailsObject) session.getAttribute(USER_OBJECT);
		String loginName = "";
		if(request.getParameter("userName") != null && !request.getParameter("userName").equalsIgnoreCase("") && !request.getParameter("userName").equalsIgnoreCase("undefined")){
			loginName = request.getParameter("userName");
		}
		else{
			loginName = userObject.getLoginName();
		}
		log.debug("loginName->"+loginName);
		try{
			log.debug(resource.getKeys());
			String eSecurityFlag = resource.getString(ESECURITY_SERVICEFLAG);
			if(eSecurityFlag.equalsIgnoreCase("true")){
				String esecurityURL = resource.getString(ESECURITY_URL);
				log.debug("eSecurity URL :::::::::::::  " + esecurityURL);
				StringBuffer xmlIp = new StringBuffer();
				xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
				xmlIp.append("<soapenv:Header/><soapenv:Body><ser:getAllPolicyDetails />");
				xmlIp.append("</soapenv:Body></soapenv:Envelope>");
				StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
				log.debug("Result --->" + result);
				roleList = esecWebServiceUtil.getPolicyValueObject(result, "policy");
			}
			log.info("ESecurityAction : loadPolicyDetails Method End ");
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public EsecurityUserDetailsObject getEsecUserObject() {
		return esecUserObject;
	}
	public void setEsecUserObject(EsecurityUserDetailsObject esecUserObject) {
		this.esecUserObject = esecUserObject;
	}
	public UserDetailsObject getUserObject() {
		return userObject;
	}
	public void setUserObject(UserDetailsObject userObject) {
		this.userObject = userObject;
	}
	public List getCountryList() {
		return countryList;
	}
	public void setCountryList(List countryList) {
		this.countryList = countryList;
	}
	public List getJobTypeList() {
		return jobTypeList;
	}
	public void setJobTypeList(List jobTypeList) {
		this.jobTypeList = jobTypeList;
	}
	public ArrayList getLastNamelst() {
		return lastNamelst;
	}
	public void setLastNamelst(ArrayList lastNamelst) {
		this.lastNamelst = lastNamelst;
	}
	public ArrayList getFirstNamelst() {
		return firstNamelst;
	}
	public void setFirstNamelst(ArrayList firstNamelst) {
		this.firstNamelst = firstNamelst;
	}
	
	
	
}