package com.velos.stafa.action.cdmr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.velos.stafa.util.CDMRUtil;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.xwork.StringUtils;
import org.apache.struts2.ServletActionContext;

import atg.taglib.json.util.JSONObject;
import com.velos.stafa.util.VelosUtil;

import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;

/**
 * @summary     GetCdmSearchGrpDataAction
 * @description load data from cdm main to group view in charge library datatable
 * @version     1.0
 * @file        GetCdmSearchGrpDataAction.java
 * @author      Lalit Chattar
 */
public class GetCdmSearchGrpDataEditAction extends StafaBaseAction{
	
	/*
	 * Action Context and Request object
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	
	/*
	 * Parameter getting from request
	 */
	
	private String sEcho;
	private Integer iTotalRecords;
	private Integer iTotalDisplayRecords;
	private List<Map<String, String>> aaData;
	
	private Set idSet;
	private int constant, end, start;
	private StringBuilder sWhere;
	private String sOrderBy = null;
	private String pkGrpId;
	
	private String[] aColumnsSer;	
	private String[] dateArray;
	private String[] numberArray;
	
	
	/*
	 * Getter and Setter

	*/
	
	public String getsEcho() {
		return sEcho;
	}

	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}
	public Integer getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(Integer iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public List<Map<String, String>> getAaData() {
		return aaData;
	}

	public void setAaData(List<Map<String, String>> aaData) {
		this.aaData = aaData;
	}

	public GetCdmSearchGrpDataEditAction(){
		cdmrController = new CDMRController();
		aaData = new ArrayList<Map<String,String>>();
	}
	
	
	/*
	 * Default methos of action class
	 */
	public String execute() throws Exception {
		return SUCCESS;
	}
	
	/**
	 * @description Get Data on the Basis of Group
	 * @return String
	 * @throws Exception
	 */
	public String getCdmSearchGrpData()throws Exception {
	
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj = VelosUtil.constructJSON(request);
			
			aColumnsSer = CDMRUtil.fetchPropertyVal(jsonObj.getString("FILE_ID")+"_COLS");
			dateArray = CDMRUtil.fetchPropertyVal(jsonObj.getString("FILE_ID")+"_DATEARY");
			numberArray = CDMRUtil.fetchPropertyVal(jsonObj.getString("FILE_ID")+"_NUMARY");
			
			//jsonObj = CDMRUtil.fetchChgIndcBySeq(jsonObj);
			jsonObj = CDMRUtil.fetchEventLibId(jsonObj);
			
			jsonObj = CDMRUtil.getActiveInactiveRule(jsonObj);
			
			
			constant = Integer.parseInt(jsonObj.getString("iDisplayLength"));
			end = Integer.parseInt(jsonObj.getString("iDisplayStart")) + Integer.parseInt(jsonObj.getString("iDisplayLength"));
			start = Integer.parseInt(jsonObj.getString("iDisplayStart"))+1;
			
			if(!(jsonObj.has("grpID"))){
				jsonObj = CDMRUtil.getCurrentVersion(jsonObj);
				jsonObj = CDMRUtil.getMapVersion(jsonObj);
				jsonObj = this.getColumnFromStagingMap(jsonObj);
				jsonObj = this.createQueryForDefaultGroup(jsonObj);
				jsonObj = cdmrController.getMatchedData(jsonObj);
				this.processDataForList(jsonObj);
				return SUCCESS;
			}else{
				idSet = new HashSet();
				pkGrpId = jsonObj.getString("grpID").replace("grp", "");
				idSet.add(pkGrpId);
				jsonObj = CDMRUtil.getCurrentVersion(jsonObj);
				jsonObj = CDMRUtil.getMapVersion(jsonObj);
				jsonObj = this.getColumnFromStagingMap(jsonObj);
				jsonObj = this.createQueryForSearchGroup(jsonObj);
				jsonObj = cdmrController.getMatchedData(jsonObj);
				this.processDataForList(jsonObj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
		
	/**
	 * @description Get All level of Group
	 * @return Set
	 * @throws Exception
	 */
		public Set getAllGroupLevels(Set idSet) throws Exception{
			try {
				JSONObject jsonObj = new JSONObject();
				String query = CDMRSqlConstants.GET_GROUP_PARENT_ID+"("+StringUtils.join(idSet, ",")+")";
				jsonObj.put("query", query);
				jsonObj.put("SQLQuery", true);
				jsonObj = cdmrController.getGroups(jsonObj);
				List<Map> listData = (List<Map>)jsonObj.get("groupcode");
				for (Map map : listData) {
					idSet.add(map.get("PK_CDM_GRPCODE").toString());
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return idSet;
		}
		
		/**
		 * @description Get Current version of data 
		 * @return JSONObject
		 * @throws Exception
		 */
		/*public JSONObject getCurrentVersion(JSONObject jsonObj) throws Exception{
			try {				
				String query = "select version_id from cdm_versiondef where is_current = 1 and FILE_ID = '"+jsonObj.getString("EVENT_ID")+"' ";
				jsonObj.put("query", query);
				jsonObj.put("SQLQuery", true);
				jsonObj = cdmrController.getCDMVersionDetail(jsonObj);
				List<Map> list = (List<Map>)jsonObj.get("versioninfo");
				Map dataMap = list.get(0);
				jsonObj.put("versionid", dataMap.get("VERSION_ID").toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return jsonObj;
		}*/
	
		
		/**
		 * @description Create dynamic Query 
		 * @return JSONObject
		 * @throws Exception
		 */
		public JSONObject createQueryForDefaultGroup(JSONObject jsonObj) throws Exception{
			try {
				List<String> dateList = Arrays.asList(dateArray);
				List<String> numberList = Arrays.asList(numberArray);
				String columnNames = "";
				List<Map> columnsNameMap = (List<Map>)jsonObj.get("columnsNames");
				for (Map map : columnsNameMap) {
					String type = map.get("MAINCDM_COLTYPE") == null? "":map.get("MAINCDM_COLTYPE").toString();
					if(type.equalsIgnoreCase("TIMESTAMP")){
						columnNames = columnNames + "TO_CHAR( " +map.get("EQMAINCDM_COL").toString() + ", 'dd-MON-YYYY') as " +map.get("EQMAINCDM_COL").toString()  + ",";
					}else{
						columnNames = columnNames +map.get("EQMAINCDM_COL").toString()+ ",";
					}
					
				}
				columnNames = columnNames.substring(0, (columnNames.length()-1));
				
				
				sWhere = new StringBuilder();
				if ( jsonObj.getString("sSearch") != "" )
				{
					sWhere.append(" and (");
					for ( int i=0 ; i<aColumnsSer.length ; i++ )
					{
						if(aColumnsSer[i].contains("DATE") || aColumnsSer[i].contains("date")){
							sWhere.append("LOWER(TO_CHAR("+aColumnsSer[i]+", 'dd-MON-YYYY')) LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
						}else{
							sWhere.append("LOWER("+aColumnsSer[i]+") LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
						}
						//sWhere.append("LOWER("+aColumnsSer[i]+") LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
					}
					sWhere = sWhere.replace((sWhere.length()-3), sWhere.length(), "");
					sWhere.append(")");
				}
				
				if (jsonObj.has("iSortCol_0")){
					sOrderBy = " ORDER BY  ";
					for ( int i=0 ; i<jsonObj.getInt("iSortingCols") ; i++ )
					{
						if((jsonObj.getString("bSortable_" + jsonObj.getString("iSortCol_"+i))).equals("true")){
							//sOrderBy = sOrderBy + aColumns[jsonObj.getInt("iSortCol_"+i)] + " " + (jsonObj.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
							String columnName = "";
							
							 if(numberList.contains(jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0")))){
									columnName = "TO_NUMBER(" +jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0"))+ ")";
								}else{
									columnName = jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0"));
								}
							sOrderBy = sOrderBy + columnName + " " + (jsonObj.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
						}
					}
					
					sOrderBy = sOrderBy.substring(0, (sOrderBy.length()-2));
					if ( sOrderBy.equals(" ORDER BY") )
					{
						sOrderBy = sOrderBy + " PK_CDM_MAIN";
					}
					
				}
				
				StringBuilder query = new StringBuilder();
				query.append("select TotalRows, PK_CDM_MAIN, "+columnNames+" from (SELECT t.*, Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(*) OVER () AS TotalRows FROM CDM_MAIN t where");
				query.append(" IS_THIS_SS = 0 and DELETEDFLAG = 0 and EVENT_LIB_ID = '"+jsonObj.getString("EVENT_ID")+"'"+sWhere+" and EFF_FROM_DATE IS NOT NULL and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where (CUR_VERSION <= "+jsonObj.getString("versionid")+") and EVENT_LIB_ID = '"+jsonObj.getString("EVENT_ID")+"' group by LSERVICE_CODE, CHG_IND)) WHERE MyRow BETWEEN "+start+" AND "+end);
				
				jsonObj.put("query", query.toString());
				jsonObj.put("SQLQuery", true);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return jsonObj;
		}
		
		/**
		 * @description Create dynamic for searching group 
		 * @return JSONObject
		 * @throws Exception
		 */
		public JSONObject createQueryForSearchGroup(JSONObject jsonObj) throws Exception{
			try {
				List<String> dateList = Arrays.asList(dateArray);
				List<String> numberList = Arrays.asList(numberArray);
				String columnNames = "";
				List<Map> columnsNameMap = (List<Map>)jsonObj.get("columnsNames");
				for (Map map : columnsNameMap) {
					String type = map.get("MAINCDM_COLTYPE") == null? "":map.get("MAINCDM_COLTYPE").toString();
					if(type.equalsIgnoreCase("TIMESTAMP")){
						columnNames = columnNames + "TO_CHAR( " +map.get("EQMAINCDM_COL").toString() + ", 'dd-MON-YYYY') as " +map.get("EQMAINCDM_COL").toString()  + ",";
					}else{
						columnNames = columnNames +map.get("EQMAINCDM_COL").toString()+ ",";
					}
					
				}
				columnNames = columnNames.substring(0, (columnNames.length()-1));
				
				
				sWhere = new StringBuilder();
				if ( jsonObj.getString("sSearch") != "" )
				{
					sWhere.append(" and (");
					for ( int i=0 ; i<aColumnsSer.length ; i++ )
					{
						//sWhere.append(aColumnsSer[i]+" LIKE '%"+jsonObj.getString("sSearch")+"%' OR ");
						if(aColumnsSer[i].contains("DATE") || aColumnsSer[i].contains("date")){
							sWhere.append("LOWER(TO_CHAR("+aColumnsSer[i]+", 'dd-MON-YYYY')) LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
						}else{
							sWhere.append("LOWER("+aColumnsSer[i]+") LIKE '%"+jsonObj.getString("sSearch").toLowerCase()+"%' OR ");
						}
					}
					sWhere = sWhere.replace((sWhere.length()-3), sWhere.length(), "");
					sWhere.append(")");
				}
				
				if (jsonObj.has("iSortCol_0")){
					sOrderBy = " ORDER BY  ";
					for ( int i=0 ; i<jsonObj.getInt("iSortingCols") ; i++ )
					{
						if((jsonObj.getString("bSortable_" + jsonObj.getString("iSortCol_"+i))).equals("true")){
							
							String columnName = "";
						
							if(numberList.contains(jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0")))){
								columnName = "TO_NUMBER(" +jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0"))+ ")";
							}else{
								columnName = jsonObj.getString("mDataProp_" + jsonObj.getInt("iSortCol_0"));
							}
							sOrderBy = sOrderBy + columnName + " " + (jsonObj.getString("sSortDir_" + i).equals("asc")?"asc" : "desc") + ", ";
						}
					}
					
					sOrderBy = sOrderBy.substring(0, (sOrderBy.length()-2));
					if ( sOrderBy.equals(" ORDER BY") )
					{
						sOrderBy = sOrderBy + " PK_CDM_MAIN";
					}
					
				}
				
				
				StringBuilder query = new StringBuilder();
				query.append("select TotalRows, PK_CDM_MAIN, "+columnNames+" from (SELECT t.*, Row_Number() OVER ("+sOrderBy+") MyRow, COUNT(*) OVER () AS TotalRows FROM CDM_MAIN t where ");
				if(!(pkGrpId.equals(""))){
					query.append("FK_CDM_GRPCODE in (" + StringUtils.join(idSet, ",") + ") and ");
				}else{
					query.append("");
				}
				query.append(" IS_THIS_SS = 0 and EVENT_LIB_ID = '"+jsonObj.getString("EVENT_ID")+"'" +sWhere+" and DELETEDFLAG = 0 and EFF_FROM_DATE IS NOT NULL and PK_CDM_MAIN in (Select MAX(PK_CDM_MAIN) from cdm_main where (CUR_VERSION <= "+jsonObj.getString("versionid")+") and EVENT_LIB_ID = '"+jsonObj.getString("EVENT_ID")+"' group by LSERVICE_CODE, CHG_IND)) WHERE MyRow BETWEEN "+start+" AND "+end);
				
				jsonObj.put("query", query.toString());
				jsonObj.put("SQLQuery", true);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return jsonObj;
		}
		
		/**
		 * @description Process data
		 * @return void
		 * @throws Exception
		 */
		public void processDataForList(JSONObject jsonObject) throws Exception{
			try{
				List<Map> dataList = (List<Map>)jsonObject.get("searched");
				String activerule = jsonObject.getString("ACTDACTRULE");
				for (Map map : dataList) {
					Set keySet = map.keySet();
					Map<String, String> data = new HashMap<String, String>();
					data.put("check", "<input type=\"checkbox\" onclick=\"addInArray(this.value);\" value=\""+map.get("PK_CDM_MAIN").toString()+"\" name=\"check\"/>");
					
					data.put("deleteset", "<img src=\"images/cdmr/delete.png\" value=\""+map.get("PK_CDM_MAIN").toString()+"\" name=\"deletegrpItem\" id=\"deletegrpItem\" onclick=\"deleteServSec("+map.get("PK_CDM_MAIN").toString()+", this);\"/>");
					long days = 0;
					if(map.get("EFF_TO_DATE") != null){
						days = CDMRUtil.getDifferenceBetweenDate(map.get("EFF_TO_DATE").toString());
					}
					
					
					for (Object key : keySet) {
						
						if(key.toString().equalsIgnoreCase("TOTALROWS") || key.toString().equalsIgnoreCase("PK_CDM_MAIN")){
							this.iTotalDisplayRecords = Integer.parseInt(map.get("TOTALROWS").toString());
							this.iTotalRecords = Integer.parseInt(map.get("TOTALROWS").toString());
							continue;
						}
						
						
						if (activerule.equals("DATE"))
						{					
							if(days >= 0 && map.get("EFF_TO_DATE") != null){
								long diffBetDate = CDMRUtil.getDifferenceBetweenTwoDates(map.get("EFF_FROM_DATE").toString(), map.get("EFF_TO_DATE").toString());
								long diffBBtwnFromAndPrest = CDMRUtil.getDifferenceBetweenDate(map.get("EFF_FROM_DATE").toString());
								if(diffBetDate > 0 && diffBBtwnFromAndPrest>=0){
									if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
										data.put(key.toString(), "N/A");
									}else{
										data.put(key.toString(),  map.get(key.toString()).toString());
									}
								}else{
									if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
										data.put(key.toString(), "<strike style='color:red'>N/A</strike>");
									}else{
										data.put(key.toString(), "<strike style='color:red'>" + map.get(key.toString()).toString() + "</strike>");
									}
								}
							}else{
								if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
									data.put(key.toString(), "N/A");
								}else{
									data.put(key.toString(),  map.get(key.toString()).toString());
								}
							}
						}
						
						else
							if (activerule.equals("FLAG"))
							{
								if(map.get("IS_ACTIVE_YN").toString().equalsIgnoreCase("N"))
								{		
									if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
										data.put(key.toString(), "<strike style='color:red'>N/A</strike>");
									}else{
										data.put(key.toString(), "<strike style='color:red'>" + map.get(key.toString()).toString() + "</strike>");
									}
								}
									
								else
								{
									
									if(map.get(key.toString()) == null || map.get(key.toString()).toString().equals("")){
										data.put(key.toString(), "N/A");
									}else{
										data.put(key.toString(),  map.get(key.toString()).toString());
									}
								}
							
							}
						
						
					}
					aaData.add(data);
				}
				
				if(aaData.isEmpty()){
					this.iTotalDisplayRecords = 0;
					this.iTotalRecords = 0;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			this.sEcho= jsonObject.getString("sEcho");
		}
		
		/**
		 * @description Get Columns from Staging Maps
		 * @return JSONObject
		 * @throws Exception
		 */
		public JSONObject getColumnFromStagingMap(JSONObject jsonObj) throws Exception{
			try {
				
				List<Map> versionList = (List<Map>)jsonObj.get("MAPSVERSION");
				String versionInfo = "";
				for (Map map : versionList) {
					versionInfo = versionInfo + map.get("MAPSVERSION").toString() + ",";
				}
				versionInfo = versionInfo.substring(0, (versionInfo.length()-1));
				String query = "select distinct(EQMAINCDM_COL),MAINCDM_COLTYPE from CDM_STAGINGMAPS where FK_CDM_MAPSVERSION in ("+versionInfo+") and DISP_INCDM = 1";
				jsonObj.put("query", query);
				jsonObj.put("SQLQuery", true);
				jsonObj = cdmrController.getStagingMaps(jsonObj);
				List<Map> list = (List<Map>)jsonObj.get("mapversion");
				jsonObj.put("columnsNames", list);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return jsonObj;
		}
		
		/**
		 * @description Get Latest map version 
		 * @return JSONObject
		 * @throws Exception
		 */
					
		/*public JSONObject getMapVersion(JSONObject jsonObj) throws Exception{
			try {
				String vermap = CDMRUtil.fetchPropertyValStr(jsonObj.getString("FILE_ID")+"_MAPS");
				String query="select MAPSVERSION from CDM_MAPSVERSION where IS_ACTIVE=1 and DELETEDFLAG=0 and FILE_ID IN("+vermap+")";
				jsonObj.put("query", query);
				jsonObj.put("SQLQuery", true);
				jsonObj = cdmrController.getMapVersion(jsonObj);
				List<Map> list = (List<Map>)jsonObj.get("mapversion");		
				jsonObj.put("MAPSVERSION", list);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return jsonObj;
		}*/
	
}
