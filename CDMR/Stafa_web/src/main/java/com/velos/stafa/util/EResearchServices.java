package com.velos.stafa.util;

import java.util.ResourceBundle;

import com.velos.stafa.business.util.WebserviceUtil;


public class EResearchServices {
	private final String ERESEARCH_SERVICEURL="ERESEARCH_SERVICEURL";
	private final String ERESEARCH_USERNAME="ERESEARCH_USERNAME";
	private final String ERESEARCH_PASSWORD="ERESEARCH_PASSWORD";
	private final String ERESEARCH_ACCOUNTNUMBER="ERESEARCH_ACCOUNTNUMBER";

	public String ERESEARCH_URL;
	public String ERESEARCH_USER;
	public String ERESEARCH_PWD;
	public String ERESEARCH_ACCNO;
	private final String XMLHEADER = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.velos.com/\"><soapenv:Header/><soapenv:Body>";
	private final String XMLFOOTER = "</soapenv:Body></soapenv:Envelope> ";
	public EResearchServices(){
		ResourceBundle resource = ResourceBundle.getBundle(VelosStafaConstants.STAFA_PROPERTYFILE);
		ERESEARCH_URL=resource.getString(ERESEARCH_SERVICEURL);
		ERESEARCH_USER=resource.getString(ERESEARCH_USERNAME);
		ERESEARCH_PWD=resource.getString(ERESEARCH_PASSWORD);
		ERESEARCH_ACCNO=resource.getString(ERESEARCH_ACCOUNTNUMBER);
		
	}
	
	public StringBuffer saveStorage(String xmlParam){
		StringBuffer result = null;
		try{
					
					StringBuffer xmlIp = new StringBuffer();
					xmlIp.append(XMLHEADER);
					xmlIp.append(xmlParam);
					xmlIp.append(XMLFOOTER);
					result = WebserviceUtil.invokeService(ERESEARCH_URL, xmlIp);
					
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	public  StringBuffer getstorageDetails(String xmlParam){
		StringBuffer result = null;
		try{
			
			StringBuffer xmlIp = new StringBuffer();
			xmlIp.append(XMLHEADER);
			xmlIp.append(xmlParam);
			xmlIp.append(XMLFOOTER);
			result = WebserviceUtil.invokeService(ERESEARCH_URL, xmlIp);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	public  StringBuffer getStorages(){
		StringBuffer result = null;
		try{
			
			StringBuffer xmlIp = new StringBuffer();
			xmlIp.append(XMLHEADER);
			xmlIp.append("<ser:getStorages>");
			xmlIp.append("<user> <account>"+ERESEARCH_ACCNO +"</account>");
			xmlIp.append("<password>"+ERESEARCH_PWD+"</password>");
			xmlIp.append("<userName>"+ERESEARCH_USER+"</userName>");
			xmlIp.append("</user> </ser:getStorages> </soapenv:Body></soapenv:Envelope>");
			
			
			 result = WebserviceUtil.invokeService(ERESEARCH_URL, xmlIp);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	
	
	
	
	
}
