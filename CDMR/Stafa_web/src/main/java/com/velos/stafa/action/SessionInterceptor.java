/**
 * 
 */
package com.velos.stafa.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * @author akajeera
 *
 */
public class SessionInterceptor extends AbstractInterceptor {

	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	public static final Log log=LogFactory.getLog(SessionInterceptor.class);
	
	String notSessionMethod = ",loadCDMRHomePage,fetchVersionDetails,loadLoginDetails,logout,openDocumentReview,viewdocument,fetchBarcode,getEncryptKey,";
	@Override
	  public String intercept(ActionInvocation invocation) throws Exception {
		log.debug("SessionInterceptor.intercept() start");
		System.out.println("--SessionInterceptor.intercept() start");
		
		String methodName = invocation.getInvocationContext().getName();
		log.trace("Inside Intercept Method Start =================== >Method Name=" + methodName +"<");
		System.out.println("Inside Intercept Method Start =================== >Method Name=" + methodName +"<");
		
		if(notSessionMethod.indexOf(","+methodName+",") != -1){
			log.trace(" inside skip session ");
			System.out.println(" inside skip session ");
			return invocation.invoke();
		}
		Map<String,Object> sessionMap = invocation.getInvocationContext().getSession();
		System.out.println("Check Session " + sessionMap.isEmpty());
	    System.out.println("SessionInterceptor.intercept() end");
	    
	    log.trace("Check Session " + sessionMap.isEmpty());
	    log.debug("SessionInterceptor.intercept() end");
	    
	    
	    
	    if(sessionMap.isEmpty()){
	         return "sessionError";
	        // return "logout";
	    } 
	    if(methodName.equalsIgnoreCase("autoLogOof")){
	    	System.out.println(" session interceptor auto log off"  );
	    	System.out.println("This is autoLogOff SessionMap Onject"  +sessionMap);
	    	sessionMap.clear();
		}
	    return invocation.invoke();
	  }
	}
