package com.velos.stafa.util;

public interface CDMRConstant{
	
	public static final String SEPARATOR = "!@#$%";
	
	//CodeList related constant
	public static final Integer CODELST_PK = 0;
	public static final Integer CODELST_FK_ACCOUNT = 1;
	public static final Integer CODELST_TYPE = 2;
	public static final Integer CODELST_SUBTYP = 3;
	public static final Integer CODELST_DESC = 4;
	public static final Integer CODELST_HIDE = 5;
	public static final Integer CODELST_SEQ = 6;
	public static final Integer CODELST_MAINT = 7;
	public static final Integer CODELST_RID = 8;
	public static final Integer CODELST_CREATOR = 9;
	public static final Integer CODELST_LAST_MODIFIED_BY = 20;
	public static final Integer CODELST_LAST_MODIFIED_DATE = 11;
	public static final Integer CODELST_CREATED_ON = 12;
	public static final Integer CODELST_IP_ADD = 13;
	public static final Integer CODELST_CUSTOM_COL = 14;
	public static final Integer CODELST_CUSTOM_COL1 = 15;
	public static final Integer FK_CODELST_ORGID = 16;
	public static final Integer CODELST_DELETEDFLAG = 17;
	public static final Integer CODELST_PAGEURL = 18;
	public static final Integer CODELST_DEFAULTVALUE = 19;
	public static final Integer CODELST_MODULE = 20;
	public static final Integer CODELST_COMMENTS = 21;
	public static final Integer CODELST_VALUE = 22;
	public static final Integer FK_COPY_CODELST = 23;
	public static final Integer CODELST_APPLOCK = 24;
	public static final Integer CODELST_FIELDTYPE = 25;
	
	
	public static final String INST_CODE = "COMP0";
	public static final String SERVICE_CODE = "COMP1";
	public static final String REV_CLASS = "COMP2";
	public static final String REV_SUB_DEPT = "COMP3";
	public static final String REV_TEST_CODE = "COMP4";
	public static final String SERVICE_DESC1 = "COMP5";
	public static final String SERVICE_DESC2 = "COMP6";
	public static final String HCC_TECH_CHG = "COMP7";
	public static final String HCPCS_CODE = "COMP8";
	public static final String CUSTOM_CODE1 = "COMP9";
	public static final String CUSTOM_CODE2 = "COMP10";
	public static final String CUSTOM_CODE3 = "COMP11";
	public static final String PRS_CODE = "COMP12";
	public static final String REV_CODE = "COMP13";
	public static final String EFF_FROM_DATE = "COMP14";
	public static final String SPONSOR_REF1 = "COMP15";
	public static final String SERVICE_UNIT = "COMP16";
	public static final String UNIT_SIZE = "COMP17";
	public static final String CPT_CODE = "COMP18";
	public static final String PRS_PHY_CHG = "COMP19";
	public static final String CUSTOM_CHG1 = "COMP20";
	public static final String CUSTOM_CHG2 = "COMP21";
	public static final String CUSTOM_CHG3 = "COMP22";
	public static final String CUSTOM_CHG4 = "COMP23";
	public static final String IS_ACTIVATED = "COMP24";
	public static final String NOTES = "COMP25";
	public static final String FK_CDM_GRPCODE = "COMP26";
	public static final String CUR_STATUS = "COMP27";
	public static final String LSERVICE_CODE = "COMP28";
	public static final String MCAID_HCPCS_CPT = "COMP31";
	public static final String EFF_TO_DATE = "COMP39";
	public static final String CMS_HCPCS_CPT = "COMP33";
	public static final String BLUE_CROSS_HCPCS_CPT_EFF_DATE = "COMP30";
	public static final String MCAID_HCPCS_CPT_EFF_DATE = "COMP32";
	public static final String BLUE_CROSS_HCPCS_CPT = "COMP29";
	public static final String CUSTOM_DESC1 = "COMP36";
	public static final String CUSTOM_DATE1 = "COMP35";
	public static final String CMS_HCPCS_CPT_EFF_DATE = "COMP34";
	public static final String IS_ACTIVE = "COMP45";
	public static final String EVENT_LIB_ID = "COMP44";
	public static final String SECRETE_KEY = "TheBestSecretKey";
}