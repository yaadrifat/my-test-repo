package com.velos.stafa.action.cdmr;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;


import org.apache.struts2.ServletActionContext;
import atg.taglib.json.util.JSONObject;
import com.opensymphony.xwork2.ActionContext;
import com.velos.stafa.util.VelosUtil;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.action.StafaBaseAction;
import com.velos.stafa.util.CDMRSqlConstants;

/**
 * @summary     GetLatestFileID
 * @description Get latest file id
 * @version     1.0
 * @file        GetLatestFileID.java
 * @author      Lalit Chattar
 */
public class GetLatestFileID extends StafaBaseAction{
	
	/*
	 * Parameter getting from request
	 */
	
	private String fileID;
	/*
	 * Objects used for processing data
	 */
	private CDMRController cdmrController;
	ActionContext ac = ActionContext.getContext();
	HttpServletRequest request=(HttpServletRequest)ac.get(ServletActionContext.HTTP_REQUEST);
	
	/*
	 * getter and setter
	 */
	public String getFileID() {
		return fileID;
	}

	public void setFileID(String fileID) {
		this.fileID = fileID;
	}
	/*
	 * Constructor
	 */
	public GetLatestFileID(){
		cdmrController = new CDMRController();
	}
	
	
	/*
	 * Default Method of Action class
	 */
	public String execute() throws Exception{
		return SUCCESS;
	}
	
	
	/**
	 * @description Get latest file id
	 * @return String
	 * @throws Exception
	 */
	public String getLatestFielId() throws Exception{
		JSONObject jsonObj = new JSONObject();
		try{
			jsonObj = VelosUtil.constructJSON(request);
			jsonObj.put("query", CDMRSqlConstants.GET_LATEST_FILE_ID);
			jsonObj.put("SQLQuery", true);
			jsonObj = cdmrController.getCDMRFilesInfo(jsonObj);
			List<Map> listData = (List<Map>)jsonObj.get("filesinfo");
			this.setFileID(listData.get(0).get("MAXID").toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
}