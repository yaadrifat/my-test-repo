package com.velos.stafa.objects;

public class TaskObject extends BaseObject{
	
	private String applicationCode;
	private String assignUserIds;
	private String contextCode;
	private String createUserId;
	private String endDate;
	private String entityName;
	private String entityId;
	private String entityType;
	
	private String password;
	private String priority;
	private String sharedUserIds;
	private String startDate;
	private String taskDescription;
	private String taskName;
	private String taskstatus;
	private String userName;
	
	private String sharedUserNames;
	private String assignUserNames;
	
	
	
	
	public String getSharedUserNames() {
		return sharedUserNames;
	}
	public String getAssignUserNames() {
		return assignUserNames;
	}
	public void setSharedUserNames(String sharedUserNames) {
		this.sharedUserNames = sharedUserNames;
	}
	public void setAssignUserNames(String assignUserNames) {
		this.assignUserNames = assignUserNames;
	}
	public String getEntityId() {
		return entityId;
	}
	public String getEntityType() {
		return entityType;
	}
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	public String getApplicationCode() {
		return applicationCode;
	}
	public String getAssignUserIds() {
		return assignUserIds;
	}
	public String getContextCode() {
		return contextCode;
	}
	public String getCreateUserId() {
		return createUserId;
	}
	public String getEndDate() {
		return endDate;
	}
	public String getEntityName() {
		return entityName;
	}
	public String getPassword() {
		return password;
	}
	public String getPriority() {
		return priority;
	}
	public String getSharedUserIds() {
		return sharedUserIds;
	}
	public String getStartDate() {
		return startDate;
	}
	public String getTaskDescription() {
		return taskDescription;
	}
	public String getTaskName() {
		return taskName;
	}
	public String getTaskstatus() {
		return taskstatus;
	}
	public String getUserName() {
		return userName;
	}
	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}
	public void setAssignUserIds(String assignUserIds) {
		this.assignUserIds = assignUserIds;
	}
	public void setContextCode(String contextCode) {
		this.contextCode = contextCode;
	}
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public void setSharedUserIds(String sharedUserIds) {
		this.sharedUserIds = sharedUserIds;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public void setTaskstatus(String taskstatus) {
		this.taskstatus = taskstatus;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	

}
