package com.velos.stafa.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.commons.lang.xwork.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.velos.stafa.business.domain.CodeList;
//import com.velos.stafa.business.domain.Lab.LabTest;
import com.velos.stafa.helper.VelosHelper;


/**
* This class made for loading codelist value on start up.
* @author Mohiuddin Ali Ahmed 
* @version 1
*/
public class StafaConfig {
	public static final Log log=LogFactory.getLog(StafaConfig.class);
	public static ServletContext context = null;
    private static boolean initializeothers = false;
	private static Map<String,CodeList> codeListMapByTypeSubtype = null;
	private static Map<Long,CodeList> codeListMapByIds = null;
	//private static Map<Long,List<LabTest>> labs = new LinkedHashMap<Long,List<LabTest>>();
	
	public static void initializeOthers(ServletContext context) {
			  
		if(!initializeothers)
		{			
			initializeCodeList(context);			
			initializeothers = true;
		}
	}
	
	
	private static void initializeCodeList(ServletContext context)
	{
		Map<String,List<CodeList>> codeListMap = getCodeListValues();					
		context.setAttribute("codeListValues", codeListMap);
		context.setAttribute("codeListMapByTypeSubtype", codeListMapByTypeSubtype);
		context.setAttribute("codeListMapByIds", codeListMapByIds);
		setLabs();
		//context.setAttribute("labs", labs);
	}
	
	private static Map<String,List<CodeList>> getCodeListValues(){
    	List<CodeList> codeLists = null;
		List<CodeList> codeListVal = null;
		List<Object> list = null;
		CodeList codeList = null;
		CodeList cList = null;
		Map<String,List<CodeList>> codeListMap = new HashMap<String, List<CodeList>>();
//		codeListMapByTypeSubtype = new HashMap<String, CodeList>();
//		codeListMapByIds = new HashMap<Long, CodeList>();
//		String s = null;
//		try{
//			list = VelosHelper.getObjectList("CodeList", " isHide = 'N' order by sequence, description ");
//			//log.debug("list size"+codeLists.size());
//			for(Object c : list)
//			{
//				codeList = new CodeList();
//				cList = (CodeList)c;
//				codeListMapByIds.put(cList.getPkCodelst(), cList);
//				s = cList.getType()+"-"+cList.getSubType();
//				codeListMapByTypeSubtype.put(s, cList);
//				if(codeListMap.containsKey(cList.getType()))
//				{
//					codeListVal = codeListMap.get(cList.getType());
//					codeListVal.add(cList);
//				}else{
//					codeListVal = new ArrayList<CodeList>();
//					codeListVal.add(cList);
//					codeListMap.put(cList.getType(), codeListVal);
//				}
//			}
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
		return codeListMap;
	}
	
	private static void setLabs(){
//		List<Object> list = null;
//		Object[] arr = null;
//		try{
//			list = VelosHelper.getDataFromSQLQuery("SELECT G.FK_LABGROUP,L.PK_LABTEST,L.LABTEST_NAME,L.LABTEST_SHORTNAME,L.LABTEST_CPT,L.LABTEST_SEQ,L.LABTEST_HOWER" +
//					" FROM ER_LABTEST L JOIN ER_LABTESTGRP G ON G.FK_TESTID=L.PK_LABTEST ORDER BY l.labtest_seq",null);
//			if(list!=null){
//				for(Object o : list)
//				{
//					arr = (Object[])o;					
//					Long key = ((BigDecimal)arr[0]).longValue();
//					if(key!=null && labs.containsKey(key)){
//						List<LabTest> l = labs.get(key);
//						LabTest lT = convertObjectArrToLabTest(arr);
//						l.add(lT);
//						labs.put(key,l);
//					}else{
//						List<LabTest> l = new ArrayList<LabTest>();
//						LabTest lT = convertObjectArrToLabTest(arr);
//						l.add(lT);
//						labs.put(key,l);
//					}
//				}
//			}
//		}catch (Exception e) {
//			e.printStackTrace();
//		}		
	}
	
	/*private static LabTest convertObjectArrToLabTest(Object[] arr){
		LabTest lTest = new LabTest();
		lTest.setPkLabTest(arr[1]!=null?((BigDecimal)arr[1]).longValue():null);
		lTest.setLabTestName(arr[2]!=null && !StringUtils.isEmpty(arr[2].toString())?arr[2].toString():null);
		lTest.setLabTestShrtName(arr[3]!=null && !StringUtils.isEmpty(arr[3].toString())?arr[3].toString():null);
		lTest.setLabTestCpt(arr[4]!=null && !StringUtils.isEmpty(arr[4].toString())?arr[4].toString():null);
		lTest.setLabTestSeq(arr[4]!=null?((BigDecimal)arr[5]).longValue():null);
		lTest.setLabTestHower(arr[6]!=null && !StringUtils.isEmpty(arr[6].toString())?arr[6].toString():null);		
		return lTest;
	}*/
}
