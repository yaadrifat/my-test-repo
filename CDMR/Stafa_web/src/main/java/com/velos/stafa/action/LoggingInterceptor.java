/**
 * 
 */
package com.velos.stafa.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpUtils;

import org.apache.commons.lang.xwork.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.StrutsStatics;

import atg.taglib.json.util.JSONArray;
import atg.taglib.json.util.JSONObject;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import java.sql.Timestamp;
import com.velos.stafa.business.domain.usertracking.UserSession;
import com.velos.stafa.controller.CommonController;
import com.velos.stafa.objects.UserDetailsObject;
import com.velos.stafa.util.VelosStafaConstants;
import com.velos.stafa.util.VelosUtil;

/**
 * @author Mohiuddin
 *
 */
public class LoggingInterceptor extends AbstractInterceptor implements StrutsStatics {	
	
	
	
	public static final Log log=LogFactory.getLog(LoggingInterceptor.class);	
	
	@Override
	public String intercept(ActionInvocation invocation) throws Exception {
		final ActionContext context = invocation.getInvocationContext().getContext();
		HttpServletRequest request = (HttpServletRequest)context.get(HTTP_REQUEST);
		
		log.debug("LoggingInterceptor.intercept() start");
		System.out.println("--LoggingInterceptor.intercept() start");		
		HttpSession session = request.getSession(false);
		JSONObject userSession = null;
		if (session!=null && request.getParameter("module")!=null && request.getParameter("entityType")!=null 
				&& (request.getParameter("entityId")!=null || request.getParameter("entityIdentifier")!=null)
				&& request.getParameter("eventCode")!=null ){
			JSONObject jsonDataObject = new JSONObject();			
			userSession = (JSONObject) session.getAttribute(VelosStafaConstants.USER_SESSION_OBJECT);
			JSONObject requestObject = VelosUtil.constructJSON(request);
			System.out.println("requestObject " + requestObject);
			JSONObject userSessionLogData = constructUserSessionLog(requestObject,userSession,request);
			JSONArray domains = new JSONArray();
			JSONObject domainObject = new JSONObject();
			domainObject.put("domain", VelosStafaConstants.DOMAIN_USERSESSIONLOG);
			domainObject.put("domainKey", VelosStafaConstants.USERSESSIONLOG_DOMAINKEY);
			domainObject.put("data", userSessionLogData);
			domains.put(domainObject);
			//----------------
			jsonDataObject.put("domains", domains);
			jsonDataObject = VelosUtil.setUserDetails(jsonDataObject,request);
			System.out.println(" final data --------------------");
			System.out.println(jsonDataObject.toString());
			jsonDataObject = CommonController.saveJSONData(jsonDataObject);
		}
	   
	  return invocation.invoke();
	}
	
	private JSONObject constructUserSessionLog(JSONObject requestObject, JSONObject userObject, HttpServletRequest request){
		JSONObject userSessionObject = new JSONObject();
		HttpUtils utills = new HttpUtils();
		try{			
			userSessionObject.put("fkUserSession", userObject!=null && !StringUtils.isEmpty(userObject.getString("pkUserSession"))?userObject.getString("pkUserSession"):"");
			userSessionObject.put("uslUrl", utills.getRequestURL(request)+"?"+request.getQueryString());
			userSessionObject.put("uslAccessTime", new SimpleDateFormat("MM/dd/yyyy HH:mm").format(new Timestamp(new Date().getTime()).getDate()));
			userSessionObject.put("uslUrlParam",requestObject.toString());
			userSessionObject.put("uslModuleName", requestObject.get("module"));
			userSessionObject.put("uslEntityId", requestObject.get("entityId")!=null?requestObject.get("entityId"):"");
			userSessionObject.put("uslEntityType", requestObject.get("entityType"));
			userSessionObject.put("uslEntityIdentifier", requestObject.get("entityIdentifier")!=null?requestObject.get("entityIdentifier"):"");
			userSessionObject.put("uslEntityEventCode", requestObject.get("eventCode"));			
			
		}catch (Exception e){
			e.printStackTrace();
		}
		return userSessionObject;
	}
}