/**
 * 
 */
package com.velos.stafa.util.eSecurity;

/**
 * @author vlakshmipriya
 *
 */
public interface ESecurityConstants {
	public static final String userId="userUniqueId";
	public static final String firstName="firstName";
	public static final String lastName="lastName";
	public static final String phone="phone";
	public static final String email="email";
	public static final String jobType="jobType";
	public static final String timeZone="timeszone";
	public static final String addr1="addr1";
	public static final String addr2="addr2";
	public static final String city="city";
	public static final String state="state";
	public static final String postalCode="zipcode";
	public static final String country="country";
	public static final String userPrimaryOrg="userPrimaryOrg";
	public static final String defaultGroup="defaultGroup";
	public static final String login="loginName";
	public static final String password="password";
	public static final String esign="esign";
	public static final String securityQue="secQuestion";
	public static final String answer="secAnswer";
	public static final String status="acctIsActive";
	public static final String passwordExpDate = "passwordExpDate";
	public static final String ESEC_USER_OBJECT = "esecUserObject";
	public static final String newpassword="newPassword";
	public static final String newesign="newesign";
	public static final String loginOrRoleName = "LoginOrRoleName";
	public static final String nameType = "nameType";
	public static final String appRights = "appRights";
	public static final String moduleName = "moduleName";
	public static final String rights = "rights";
	public static final String treeSeq = "treeSeq";
	public static final String description = "description";
	public static final String type = "type";
	public static final String roleCode = "roleCode";
	public static final String userName = "userName";
	public static final String userLoginName = "userLoginName";
	public static final String passwordDays = "passwordDays";
	public static final String roleName = "roleName";
	public static final String roleDesc = "roleDesc";
	public static final String roleNames = "roleNames";
	public static final String clientUserId = "clientUserId";
	public static final String code = "code";
	public static final String policyDescription = "description";
	public static final String field = "field";
	public static final String sequence = "sequence";
	public static final String value = "value";
	public static final String comboValue = "comboValue";
	public static final String accExpDate="acctexpDate";
	public static final String userIsActive="userIsActive";
	public static final String acctIsActive="acctIsActive";
	public static final String policy="policy";
	public static final String visible="visible";
	
}