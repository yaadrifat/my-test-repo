<% String contextpath=request.getContextPath();

	String secServiceEnable = VelosStafaConstants.SECURITY_ENABLE_KEY;

	//System.out.println("secServiceEnable  " + secServiceEnable);
%>
	<%@ page contentType="text/html; charset=UTF-8"%>
	<%@ taglib prefix="s" uri="/struts-tags"%>
	<%@ page import="com.velos.stafa.util.VelosStafaConstants" %>
	<%@ page import="com.velos.stafa.objects.UserDetailsObject,java.util.Map,java.util.HashMap" %>
<script>

var cotextpath = "<%=contextpath%>";
var secServiceEnable = "<%=secServiceEnable%>";
</script>

<%

	String STAFA_ADMINISTRATOR = "Administrator Option";
	String STAFA_DATAFIELD = "DataField Config";
	String STAFA_PANEL = "Panel Config";
	String STAFA_TECHNICIAN = "Technician View";
	String STAFA_HOME = "Home";
	String STAFA_ACQUISITION = "Acquisition";
	String STAFA_ACCESSION = "Accession";
	String STAFA_PRODUCTION = "Production";
	String STAFA_PREPRODUCT ="Pre-Product";
	String STAFA_OVERNIGHTSTOR = "Overnight Storage";
	String STAFA_ORGPRODUCT = "Original Product";
	String STAFA_PROCESSING = "Processing";
	String STAFA_CRYOPREP="Cryoprep";
	String STAFA_STORAGE = "Storage";
	String STAFA_STOREPROD = "Store Product";
	String STAFA_TRANSFER = "Transfer";
	String STAFA_RETRIEVE="Retrieve";
	String STAFA_RELEASE = "Release";
	String STAFA_DOCUMENTS="Documents";
	String STAFA_THAW="Thaw";
	String STAFA_WASH="Wash";
	String STAFA_INFRELEASE="Infusion Release";
	String STAFA_PHYSICIAN="Physician View";
	String STAFA_SELPRODUCT="Select Product";
	String STAFA_SUPERVISOR="Supervisor";
	String STAFA_TASK="Task Management";
	
	
	Long userId = null;
	userInfo=(UserDetailsObject)session.getAttribute("userDetailsObject");
	if(userInfo!=null){
		moduleMap= userInfo.getModuleMap();
		userId = userInfo.getUserId();
	} 
	
%>
<%!
 Map moduleMap=new HashMap();
UserDetailsObject userInfo = null;
 char getModuleRight(String moduleName){
	 char modulerights='0';
     int moduleid=0;
	 String chk="";
	 if(moduleMap!=null && moduleMap.get(moduleName)!=null){
		 moduleid=Integer.parseInt((String)moduleMap.get(moduleName));
		 modulerights=userInfo.getRoleRights().charAt((moduleid-1));
 	}
 	return modulerights;
 }


  String checkAccessRights(String module){
	  System.out.println("module :  "+module);
	   char userRights = getModuleRight(module);
	   String accRights = "";
	    String readRights="13579BDF";
		String addRigths="2367ABEF";
		String modifyRights="4567CDEF";
		String deleteRights="89ABCDEF";
	   // read
	   if(readRights.indexOf(userRights) != -1 ){
	      accRights+="R";
	   }
	   // Add
	   if(addRigths.indexOf(userRights) != -1 ){
	      accRights+="A";
	   }

	   // modify
	   if(modifyRights.indexOf(userRights) != -1 ){
	      accRights+="M";
	   }

	   // delete
	   if(deleteRights.indexOf(userRights) != -1 ){
	      accRights+="D";
	   }
	   System.out.println("Rigths " + accRights);
	   return accRights;

   }

 %>
