(function ($) {
AjaxSolr.TopSearchWidget = AjaxSolr.AbstractFacetWidget.extend({
  afterRequest: function () {
	var selfobj = this;
	createTopSearchFilter(selfobj);
  }
});
})(jQuery);
