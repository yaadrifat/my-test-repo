
(function($) {
	AjaxSolr.MenuFacetWidget = AjaxSolr.AbstractFacetWidget
			.extend({
				afterRequest : function() {


					var object = this.manager.response.facet_counts.facet_fields[this.field];
					if($.isEmptyObject(object)){
						$(this.target).hide();
						var parent = $(this.target).parent();
						$(parent).find('.menuInfo').remove();
						$(parent).append('<div class="menuInfo">'+AjaxSolr.theme('no_items_found')+'</div>');
					}
					else{
						$(this.target).show();
						var parent = $(this.target).parent();
						$(parent).find(".menuInfo").remove();

						if (this.autocomplete == 'AlphabeticSearch') {
							loadMenuFilter(this);
							var data = this.manager.response;
						 	var facetname = this.field;
							fnDisplayResult(data, facetname, "", "0", $(this.target));
							var menuName = $(this.target).attr("id");
							$('#'+menuName+' .active').css({'font-weight':'bold',"font-size":"18px"});
							$('#'+menuName+' .active').addClass('addFontWeight');
						}
						else if (this.autocomplete == 'DateSearch') {
							var fieldId = $(this.target).attr("id");
							$(this.target).addClass("ulDateContentDiv");
							createDateFilter(fieldId, this);
							$('#ui-datepicker-div').css("display","none");
						}
						else if(this.autocomplete == 'NumberSearch'){
							var fieldId = $(this.target).attr("id");
							$(this.target).addClass("ulDateContentDiv");
							createNumberFilter(fieldId, this);
						}
						else{
							$(this.target).empty();
							var fieldId = $(this.target).attr("id");
							createFilter(fieldId, this);

							var data = this.manager.response;
						 	var facetname = this.field;
							fnDisplayResult(data, facetname, "", "0", $(this.target));
						}
					}
					var groupId = this.group;
					var fieldId = $(this.target).attr("id");
					var menuli = $('#'+fieldId).closest("li");
					var filterLen = $(menuli).find(".filterSection").length;
					if(filterLen > 1){
						 $(menuli).find(".filterSection:last").remove();
					}
					$('#'+groupId).append(menuli);
				}
			});

})(jQuery);
