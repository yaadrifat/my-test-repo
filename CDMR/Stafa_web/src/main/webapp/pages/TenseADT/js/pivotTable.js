function createPivotTable(){
	if(TotalPages == 0){
		 alert("Error: No Record(s) Found.");
		 return false;
	 }
	var liStr = "";
	$('.listSelection').each(function(i, aEl){
			var facetname = $(aEl).siblings("ul").find("#facetname").val();
			var facetText = $(aEl).find('#facetText').val();
			if(typeof(facetname) != 'undefined'){
				liStr += "<li class='pivotmenuli'><input type='hidden' value='"+facetname+"' />"+facetText+"</li>";	
			}
	});
	var pivotmenu = "<ul id='dragItem'>"+liStr+"</ul>";
	$('#pivotFacetnameDiv').html(pivotmenu);
	/*Applying drag and drop functionality*/
	$( "#dragItem li" ).draggable({ helper: 'clone'});
	$( "#dropItemRow" ).droppable({ 
			drop: function( event, ui ) {
				$(this).find('#centerDiv').addClass('pivotmenuliSelected').html($(ui.helper).html());
				var selecCol = $('#dropItemColumn :hidden').val();
				if(selecCol != undefined){
					getDataFromSolr();
				}
			}				
	});
	$( "#dropItemColumn" ).droppable({ 
			drop: function( event, ui ) {
				$(this).find('#centerDiv').addClass('pivotmenuliSelected').html($(ui.helper).html());
				var selecRow = $('#dropItemRow :hidden').val();
				if(selecRow != undefined){
					getDataFromSolr();
				}
			}				
	});
	 $( "#pivotDialogDiv").dialog({
			modal: true,
			title: "Summary Table",
			position: 'center',
			height: '600',
			width: '99%',
			resizable: false,
			buttons: {
	            "Close": function(){
	                $( this ).dialog( "close" );
	            },
	            "Export": function(){
	            	exportData();
	            },
	            "Print": function(){
	            	var tablestr = $('#solrpivotDiv table').html();
	            	if(tablestr){
	            		$('#printDiv').html("<table border='1' class='pivotPrintTable' cellpadding='0' cellspacing='0' >"+tablestr+"</table>");
		            	$('#printDiv td').attr("align","right");
		            	$('#printDiv .selcColumm').attr("align","left").css("font-weight","bold");
		            	$('#printDiv').print();
	            	}
	            	else{
	            		alert("Error: Please create a Summary table to Print.");
	            	}
	            }
	        },
			close: function( event, ui ) {
				$('#pivotDialogDiv').dialog('destroy');
				$('#solrpivotDiv').empty();
				$('#pivotFacetnameDiv').empty();
				$('#dropItemRow #centerDiv').html("<div class='draganddropText' >Drag &amp; Drop Row Here</div>");
				$('#dropItemColumn #centerDiv').html("<div class='draganddropText'>Drag &amp; Drop Column Here</div>");
				$('.pivotmenuliSelected').removeClass('pivotmenuliSelected');
			}
		});
}

function getDataFromSolr(){
	var selecRow = $('#dropItemRow :hidden').val();
	var selecRowtext = $('#dropItemRow div').text();
	var selecCol = $('#dropItemColumn :hidden').val();
	var selecColtext = $('#dropItemColumn div').text();
	var fq = topSearchObj.manager.store.values('fq');
	var fqStr = "";
	for(var i in fq){
		if(fqStr != ""){
			fqStr += "&";
		}
		fqStr += "fq="+fq[i];
	}
	var solrurl = ipvalue+urlObj[0].childNodes[0].nodeValue;
		solrurl = solrurl + "?q=*%3A*&facet=true&"+fqStr+"&facet.pivot="+selecRow+","+selecCol+"&rows=0&wt=json&json.wrf=?";
	 $.ajax({
		  url: solrurl,
		  dataType: 'json',
		  success: function(data){
			/*JSON FORMATTING FOR PIVOT*/
			var jsonstr = getJSONSTR(data);
			var parsedJson = $.parseJSON(jsonstr);
			var pivotJSONdata = {
			columns: [
			{ colvalue: "Column", coltext: "Column", header: "Column", sortbycol: "Column", groupbyrank: null, pivot: true, result: false },
			{ colvalue: "Row", coltext: "Row", header: selecRowtext, sortbycol: "Row", groupbyrank: 2, pivot: false, result: false },
			{ colvalue: "Count", coltext: "Count", header: "Count", sortbycol: "Count", groupbyrank: null, pivot: false, result: true}],
			rows: parsedJson
		    };
			/*APPLYING PIVOT*/
			$('#solrpivotDiv').pivot({
	            source: pivotJSONdata
	        });
			/*ADDING COLUMN HEADER*/
			var columnlen = $('#solrpivotDiv table.pivot tbody tr:first th').length;
			var columnRow = "<tr class='pivotFirstrow'><td>&nbsp;</td><td class='selcColumm' colspan="+(columnlen-1)+">"+selecColtext+"</td></tr>";
			$('#solrpivotDiv table.pivot tbody').prepend(columnRow);
		}
	});
}

function getJSONSTR(data){
	var jsonstr = "";
	/*JSON ITERARTION START*/
	var facetpivot = data.facet_counts.facet_pivot;
	for(var key in facetpivot){
		var facetObjArr = facetpivot[key];
		for(var j in facetObjArr){
			/*ROW JSON VALUE*/
			var rowvalue = facetObjArr[j].value;
			var pivotObjArr = facetObjArr[j].pivot;
			for(var k in pivotObjArr){
				/*COLUMN JSON VALUE*/
				var columnname = pivotObjArr[k].value;
				var columncount = pivotObjArr[k].count;
				if(jsonstr != ""){
					jsonstr += ",";
				}
				jsonstr += '{"Row":"'+rowvalue+'","Column":"'+columnname+'","Count":"'+columncount+'"}';
			}
		
		}
	}
	/*JSON ITERARTION END*/
	jsonstr = "["+jsonstr+"]";
	return jsonstr;
}

function exportData(){
	var rowObject = new Object();
	var rowlen = $('#solrpivotDiv table.pivot tr').length;
	if(rowlen == 0){
		alert("Error: Please create a Summary table to Export.");
		return false;
	}
	$('#solrpivotDiv table.pivot tr').each(function(i, el){
		 var rowname = "row_"+i;
		 var rowStr = "";
		 $(el).find("th").each(function(i, el){
			 if(rowStr != ""){
				 rowStr += "||";
			 }
			 rowStr += $(el).text();
		 });
		 $(el).find("td").each(function(i, el){
			 if(rowStr != ""){
				 rowStr += "||";
			 }
			 var text = $(el).text();
			 if($.trim(text)){
				 rowStr += $(el).text();
			 }
			 else{
				 rowStr += "null";
			 }
		 });
		 rowObject[rowname] = rowStr;
	});
	var rowQuery = "";
	var noOfRows = 0;
	for(var key in rowObject){
		if(rowQuery != ""){
			rowQuery += "&";
		}
		var rowValue = encodeURIComponent(rowObject[key]);
		rowQuery += (key+"="+rowValue);
		noOfRows ++;
	}
	var exporturl = ipvalue+urlObj[0].childNodes[0].nodeValue;
    exporturl =  exporturl.replace("/select","");
    var lastSlashPos = exporturl.lastIndexOf("/");
    exporturl = exporturl.substr(0,lastSlashPos);
	$.download(exporturl+"/pivot", rowQuery+"&noOfRows="+noOfRows);
}