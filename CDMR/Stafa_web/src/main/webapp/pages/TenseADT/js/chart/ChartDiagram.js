var chartType = "PieChart";
function createChart(){
	if(TotalPages == 0){
		 alert("Error: No Record(s) Found.");
		 return false;
	 }
	var liStr = "";
	$('.listSelection').each(function(i, aEl){
			var facetname = $(aEl).siblings("ul").find("#facetname").val();
			var facetText = $(aEl).find('#facetText').val();
			if(typeof(facetname) != 'undefined'){
				liStr += "<li class='chartmenuli'><input type='hidden' value='"+facetname+"' />"+facetText+"</li>";	
			}
	});
	var chartmenu = "<ul id='dragItem'>"+liStr+"</ul>";
	$('#chartFacetnameDiv').html(chartmenu);
	//$('#charttabs').tabs();
	/*Applying drag and drop functionality*/
	$( "#dragItem li" ).draggable({ helper: 'clone'});
	$( "#dropFieldForChart" ).droppable({ 
			drop: function( event, ui ) {
				$(this).find('#centerDiv').addClass('chartmenuliSelected').html($(ui.helper).html());
				getChartDataFromSolr();
			}				
	});
	 
	$( "#chartDialogDiv").dialog({
		modal: true,
		title: "Chart",
		position: 'center',
		height: '650',
		width: '90%',
		resizable: false,
		buttons: {
	          "Close": function(){
	               $( this ).dialog( "close" );
	           }
	    },
		close: function( event, ui ) {
			$('#chartDialogDiv').dialog('destroy');
			$('#chartFacetnameDiv').empty();
			$('.chartCanvas').hide();
			$('#dropFieldForChart').html("<div id='centerDiv'><div class='draganddropText'>Drag &amp; Drop a Field Here</div></div>");
			$('.chartmenuliSelected').removeClass('chartmenuliSelected');
		}
	});
}

function getChartDataFromSolr(){
	var fieldName = $('#dropFieldForChart :hidden').val();
	if(typeof(fieldName) != 'undefined'){
		var fq = topSearchObj.manager.store.values('fq');
		var fqStr = "";
		for(var i in fq){
			if(fqStr != ""){
				fqStr += ",";
			}
			fqStr += encodeURIComponent(fq[i]);
		}
		
		var url = getChartURL();
		$.ajax({
			url: url,
			dataType: 'jsonp',
			data:{
				domain: currentSelc,
				fieldName: fieldName,
				fq: fqStr,
				chartType: chartType
			},
			jsonpCallback: 'tenseCallback',
			success: function(data) {
				$('.chartCanvas').hide();
				var message = data.message;
				if(message == "error"){
					alert("Error: No Record(s) Found.");
				}
				else{
					if(chartType == "PieChart"){
						var jsonstr = data.datalst;
						new Chart(document.getElementById("pieChartcanvas").getContext("2d")).Pie(jsonstr);
						$('#pieChartcanvas').show();
					}
					else if(chartType == "BarChart"){
						var jsonstr = data.data;
						new Chart(document.getElementById("barChartcanvas").getContext("2d")).Bar(jsonstr);
						$('#barChartcanvas').show();
					}
					else if(chartType == "LineChart"){
						var jsonstr = data.data;
						new Chart(document.getElementById("lineChartcanvas").getContext("2d")).Line(jsonstr);
						$('#lineChartcanvas').show();
					}
				}
			} 
		});
	}
}

function changeChartType(el, chart){
	chartType = chart;
	$('#chartUL .activeChartli').removeClass("activeChartli");
	$(el).addClass("activeChartli");
	getChartDataFromSolr();
}

function getChartURL(){
	var saveurl = ipvalue+urlObj[0].childNodes[0].nodeValue;
	saveurl =  saveurl.replace("/select","");
    var lastSlashPos = saveurl.lastIndexOf("/");
    saveurl = saveurl.substr(0,lastSlashPos);
    var reqUrl = saveurl+"/chart";
    return reqUrl;
}