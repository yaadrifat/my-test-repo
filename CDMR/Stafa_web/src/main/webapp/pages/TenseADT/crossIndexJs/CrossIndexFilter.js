function createFreeSearch(elDiv){
	var elDivObj = $("#"+elDiv);
	var staticHtml = $('#menuFilter').html();
	$(elDivObj).find('.alphabetFilterUL').remove();
	
	var radioButtDiv = '&nbsp;&nbsp;<label><input type="radio" id="startWithBox" name="'+elDiv+'" />&nbsp;Starts With&nbsp;&nbsp;</label>'
    +'<label><input type="radio" id="containBox" name="'+elDiv+'" checked="true" />&nbsp;Contains</label>';
	
	$(elDivObj).append("<ul class='alphabetFilterUL' >"+staticHtml+"</ul>");
	$(elDivObj).find(".alpha-nav").hide();
	$(elDivObj).find('#resultcount').wrap('<div id="radioButtDiv"></div>');
	$(elDivObj).find('#radioButtDiv').prepend(radioButtDiv);
	var facetname = elDiv;
	$(elDivObj).find('#facetname').val(facetname);
 
	$(elDivObj).find('#search').click(function(){
		var txtBoxVal = $.trim($(elDivObj).find(":text").val());
		var olEle = $(elDivObj).find('#selectedOL');
		var checkboxSelec = $(olEle).find("li").length;
		if(typeof(checkboxSelec)!='undefined'&&checkboxSelec!=0){
			var selec = "";
			var isMultiple = false;
			$(olEle).find("li").each(function(i,el){
				if(selec!=""){
					selec = selec +" OR ";
					isMultiple = true;
				}
				var selVal = $(el).find(":hidden").prop("value");
				if(selVal.charAt((selVal.length-1)) == '\\')
				{ 
					selVal = selVal.replace(selVal.charAt(selVal.length-1), "\\\\");
				}
				selec = selec +'"'+selVal+'"';
			});
			if(isMultiple){
				selec = "("+selec+")";
			}
			var fq = elDiv+":"+selec;
			fqArr.push(fq);
			currPage = 1;
			loadLuceneData();
		}
		else if(txtBoxVal!=""){
			var boxSelec = $(elDivObj).find(':checked').attr("id");
			var tempVal = txtBoxVal;
			tempVal = tempVal.toLowerCase();
			tempVal = tempVal.replace(/%/g,"_percentage_");
			tempVal = encodeURIComponent(tempVal);
			var fq_splchar = "";
			if(boxSelec == "startWithBox"){
				fq_splchar = facetname+"_prefix:"+tempVal;
			}
			else{
				fq_splchar = facetname+"_splchars:("+tempVal+"* OR *"+tempVal+"*)";
			}
			fqArr.push(fq_splchar);
			currPage = 1;
			loadLuceneData();
		}
		else{
			$(elDivObj).find("#close").trigger("click");
		}
	});
	
	$(elDivObj).find(":text").keyup(function(eve){
		var txtVal = $.trim($(elDivObj).find(":text").val());
		if(txtVal==""){
			fnGetFacetResult(txtVal, $('.active'), facetname, "");
		}
		else{
			txtVal = txtVal.replace(/%/g,"_percentage_");
			txtVal = encodeURIComponent(txtVal);
			var boxSelec = $(elDivObj).find(':checked').attr("id");
			var searchQuery = "";
			if(boxSelec == "startWithBox"){
				searchQuery = "fq="+facetname+"_prefix:"+txtVal;
			}
			else{
				searchQuery = "fq="+facetname+"_splchars:("+txtVal+"* OR *"+txtVal+"*)";
			}
			fnGetFacetResult(txtVal, null, facetname, searchQuery);
		}
		
		if(eve.which == 13){
			$(elDivObj).find('#search').triggerHandler("click");
		}
	});
	
	$("input[name="+elDiv+"]").bind("click",function(eve){
		$(elDivObj).find(":text").triggerHandler("keyup");
	});
	
	$(elDivObj).find("#close").click(function(eve){
		 $(elDivObj).find("ul.alphabetFilterUL").attr("style","display:none;");
		 $('#resultTable').css({opacity:'1'});
	});
}

function createAlphabetFilter(elDiv){
	var elDivObj = $("#"+elDiv);
	var staticHtml = $('#menuFilter').html();
	$(elDivObj).find('.alphabetFilterUL').remove();
	$(elDivObj).append("<ul class='alphabetFilterUL' >"+staticHtml+"</ul>");
	var facetname = elDiv;
	$(elDivObj).find('#facetname').val(facetname);
	$.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase()); 
	if($.browser.chrome || $.browser.safari){
		$('.alpha-nav button').css('margin-left','-1px');
	}
	$(elDivObj).find('#search').click(function(){
		var txtBoxVal = $.trim($(elDivObj).find(":text").val());
		var olEle = $(elDivObj).find('#selectedOL');
		var checkboxSelec = $(olEle).find("li").length;
		if(typeof(checkboxSelec)!='undefined'&&checkboxSelec!=0){
			var selec = "";
			var isMultiple = false;
			$(olEle).find("li").each(function(i,el){
				if(selec!=""){
					selec = selec +" OR ";
					isMultiple = true;
				}
				var selVal = $(el).find(":hidden").prop("value");
				if(selVal.charAt((selVal.length-1)) == '\\')
				{ 
					selVal = selVal.replace(selVal.charAt(selVal.length-1), "\\\\");
				}
				selec = selec +'"'+selVal+'"';
			});
			if(isMultiple){
				selec = "("+selec+")";
			}
			var fq = elDiv+":"+selec;
			fqArr.push(fq);
			currPage = 1;
			loadLuceneData();
		}
		else if(txtBoxVal!=""){
			var tempVal = txtBoxVal;
			tempVal = tempVal.toLowerCase();
			var facetname = elDiv;
			var fq_splchar = facetname+"_splchars:("+tempVal+"* OR *"+tempVal+"*)";
			fqArr.push(fq_splchar);
			currPage = 1;
			loadLuceneData();
		}
		else{
			$(elDivObj).find("#close").trigger("click");
		}
	});
	
	$(elDivObj).find(":text").keyup(function(eve){
		var txtVal = $.trim($(elDivObj).find(":text").val());
		if(txtVal==""){
			var activeLetter = $(elDivObj).find(".active").text();
			if(activeLetter != 'All'){
				txtVal = activeLetter;
			}			
			fnGetFacetResult(txtVal, $('.active'), facetname, "");
		}
		else{
			fnGetFacetResult(txtVal, this, facetname, "");
		}
		
		if(eve.which == 13){
			$(elDivObj).find('#search').triggerHandler("click");
		}
	});
	
	$(elDivObj).find("#close").click(function(eve){
		 $(elDivObj).find("ul.alphabetFilterUL").attr("style","display:none;");
		 $('#resultTable').css({opacity:'1'});
	});
}

function fnGetFacetResult(value, el, facetname, searchQuery){
	if(facetname == ""){
		facetname = $(el).closest('#menuFilterContent').find('#facetname').val();
	}
	var pagerName = facetname+"_paginator";
	var pagerLen = $('#'+pagerName).length;
	if(pagerLen != 0){
		$('#'+facetname+" #selectedOL li").remove();
		$('#'+pagerName).pagination('destroy');
		$('#'+pagerName).remove();
	}
	fnGetFacetResultWithPagination(value, el, 0, facetname, searchQuery);
}

function fnGetFacetResultWithPagination(value, el, offset, facetname, searchQuery){
	
	var isButtonClick = $(el).is("button");
	$('#'+facetname+' .addFontWeight').css({'font-weight':'normal',"font-size":"14px"});
	
	if(isButtonClick){
		$(el).css({'font-weight':'bold',"font-size":"18px"});
		$(el).addClass('addFontWeight');
		$('#'+facetname+' .active').removeClass('active');
		$(el).addClass('active');
	}
	var tempVal = value.toLowerCase();
	var fq_splchar = facetname+"_splchars:("+tempVal+"* OR *"+tempVal+"*)";
	fq_splchar = fq_splchar.replace(/%/g,"_percentage_");
	var searchCriteria = "fq="+encodeURIComponent(fq_splchar);
	
	var fqStr = "";
	for(var i in fqArr){
		var fqText = fqArr[i];
		fqText = fqText.replace(/%/g,"_percentage_");
		fqText = encodeURIComponent(fqText);
		fqStr += "&fq="+fqText;
	}
	if(isButtonClick && value != ""){
		value = value.replace(/%/g,"_percentage_");
		value = encodeURIComponent(value);
		searchCriteria = "fq="+facetname+"_prefix:"+value;
	}
	else if(value == ""){
		searchCriteria = "";
	}
	if(searchQuery != ""){
		searchCriteria = searchQuery;
	}
	var facetData = searchCriteria+fqStr+"&facet.field="+facetname+"&offset="+offset;
	var faceturl = ipvalue+"/solrFacet";
	$.ajax({
		url: faceturl,
		dataType:"jsonp",
		jsonpCallback: 'tenseFacetCallback',
		data: facetData,
		success: function(data){
			 var facets = data.facets;
			 var facettotal = data.facets.facettotal;
			 var totalDocuments = data.facets.totalDocuments;
			 constructFacetResult(facets, facetname, value, el, facettotal, totalDocuments, searchQuery);
		}
	});
}

function constructFacetResult(facets, facetname, gvalue, el, facettotal, totalDocuments, searchQuery){
	var facetArr = facets[facetname];
	$('#'+facetname+" ul > li").remove();
	for(var i in facetArr){
		var value = facetArr[i];
		value = escapeHtml(value);
		var tempValue = value;
		var textLength = 45;
		if( value != null && value != "" && value.length > textLength)
		{
			tempValue = value.slice(0,textLength);
			tempValue = tempValue+"...";
		}
		 var countPos = value.indexOf("(");
		 var checkboxval = value.substr(0, countPos);
		 var checked  = "";
		 var olEle = $('#'+facetname).find('#selectedOL');
		 $(olEle).find("li").each(function(i, el){
				var elVal = $(el).find(":hidden").prop("value");
				if(checkboxval == elVal){
					checked = "checked";
					return false;
				}
		 });
		var chckboxel='<input type="checkbox" value="'+checkboxval+'" onclick="removeSelec(this)"  '+checked+' />';
		var output = $('<li>'+chckboxel+'<button type="button" class="selection" title="'+value+'"><span style="display:none">'+value+'</span><b>'+tempValue+'</b></button></li>').click(function(){
			var isChecked = $(this).find("input").prop("checked");
			if(isChecked){
				$(this).find("input").prop("checked", false);
			}
			else{
				$(this).find("input").prop("checked", true);
			}
			setSelectedFacetValues(this);
		});
		$('#'+facetname+" ul #menuFilterContent").after(output);
	}
	var lilen = $('#'+facetname+" ul li").length;
	var docCount = "";
	if(lilen != 0){
		$('#'+facetname+" ul #menuFilterContent").css({'margin-bottom':'0px'});
		docCount = "Current result set: "+totalDocuments+" documents";
	}
	else{
		$('#'+facetname+" ul #menuFilterContent").css({'margin-bottom':'20px'});
		docCount = "Current result set: 0 documents";
	}
	$('#'+facetname+" div#resultcount").html(docCount);
	var pagerName = facetname+"_paginator";
	var pagerLen = $('#'+pagerName).length;
	if(pagerLen == 0){
		$('#'+facetname+" ul").append("<div id='"+pagerName+"' class='facet_paginator' align='center'></div>");
		if(facettotal > 10){
			$('#'+pagerName).pagination({
		        items: facettotal,
		        itemsOnPage: 10,
		        cssStyle: 'light-theme',
		        displayedPages: 3,
		        currentPage: 1,
		        hrefTextPrefix: "#",
		        onPageClick: function(pageNumber){
		        	var offset = (pageNumber-1) * 10;
		        	fnGetFacetResultWithPagination(gvalue, el, offset, facetname, searchQuery);
		        } 
		    });
		}
	} 
}

function setSelectedFacetValues(el){
	var checkbox = $(el).find(":checkbox");
	var isChecked = $(checkbox).prop("checked");
	var elDivObj = $(el).closest('ul');
	var olEle = $(elDivObj).find('#selectedOL');
	var selecVal = $(checkbox).prop("value");
	if(isChecked){
		var liStr = "<li><input type='hidden' value='"+selecVal+"' /></li>";
		$(olEle).append(liStr);
	}
	else{
		$(olEle).find("li").each(function(i, el){
			var elVal = $(el).find(":hidden").prop("value");
			if(elVal == selecVal){
				$(el).remove();
			}
		});
	}
}