function createNumberRangeFilter(elDiv){
	var elDivObj = $("#"+elDiv);
	$(elDivObj).find('ul').remove();
    $(elDivObj).find('#facetname').remove();
	var facetname = elDiv;
	var fromBox = "numberFrom_"+elDiv;
	var toBox = "numberTo_"+elDiv;
	var valBox = "numberVal_"+elDiv;
	var labelName = $(elDivObj).text();
 	labelName = labelName.replace(/<br\/>/g, '');	
		 
var filterCons = '<ul><div  id="filterNumberDiv"><table><tr><td nowrap>&nbsp;<label><input class="verticalPosition" type="radio" id="numberRange" name="'+elDiv+'" value="numberRange" checked="checked" onClick=numberRange(this) ><b class="verticalPosition" >&nbsp;'+labelName+' Range</b></label></td></tr>'
	+'<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From&nbsp;<input type="text" size="10" id="'+fromBox+'" class="rangeTxtbox" >&nbsp;&nbsp;To&nbsp;'
 +'<input type="text" id="'+toBox+'"  size="10" class="rangeTxtbox" ></td></tr> <tr><td>&nbsp;<label><input class="verticalPosition"  type="radio" name="'+elDiv+'" id="number" value="number" onClick=numberRange(this) ><b class="verticalPosition" >&nbsp;'+labelName+'&nbsp;</b></label></td></tr>'
 +'<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="'+valBox+'" size="10" disabled="disabled" class="rangeTxtbox" >&nbsp;(exact)</td></tr>'
 +'</table><div><img id="search" src = "images/search_button.gif"></img>&nbsp;&nbsp;<img id="close" src = "images/close.gif"></img>'
 +'</div></div></ul>';
    
	$(elDivObj).append(filterCons);
	$(elDivObj).append("<input type='hidden' id='facetname' value='"+facetname+"' />");
	// Function Called to apply the masking to the textboxes
	numbermask(elDiv);
	$('#'+valBox).css('background-color','#D8D8D8');
	var numberFromObj = $(elDivObj).find("#"+fromBox);
	var numberToObj = $(elDivObj).find("#"+toBox);
	var numberValObj= $(elDivObj).find("#"+valBox);
	 
	$(elDivObj).find("#search").click(function(eve){
		 var numberFrom = $(numberFromObj).val();
		 var numberTo = $(numberToObj).val();
		 var numberVal=$(numberValObj).val();
		 var filedVal = "";
		 var boxSelec = $(elDivObj).find(':checked').attr("id");
		 if(boxSelec == 'numberRange'){
		 	if(numberFrom!=""&numberTo!=""){
					if(Number(numberFrom)>Number(numberTo)){
						 alert("Error: To value should be greater than From value.");
						 createFocus(toBox);
						 return false;
					 }
					filedVal = '['+ numberFrom +' TO '+ numberTo + ']';
			 }
			 else if(numberFrom==""&numberTo!=""){
				 filedVal = '[* TO '+numberTo+']';				 
			 }
			 else if(numberFrom!=""&numberTo==""){
				 filedVal = '['+numberFrom+' TO *]';				 
			 }
			 else{
				 alert("Error: Please enter the value(s) to search.");
				 createFocus(fromBox);
				 return false;
			 }
			  if(validateDecimal(elDiv, 1)){
			      return false;
			 }
			 var fq = elDiv+"_td:"+filedVal;
			 fqArr.push(fq);
		 }
		 if(boxSelec=='number'){
			 if(numberVal!=""){
				 filedVal = numberVal;
			 }
			 else{
				 alert("Error: Please enter the value to search.");
				 createFocus(valBox);
				 return false;
			 }
			 if(validateDecimal(elDiv, 0)){
			      return false;
			 }
			 var fq = elDiv+':"'+filedVal+'"';
			 fqArr.push(fq);
		 }
		 
		 $(elDivObj).find("ul").attr("style","display:none;");
		 if(filedVal!=""){
			 currPage = 1;
			 loadLuceneData();
		 }
	});
	
	$(elDivObj).find(":text").keyup(function(eve){
		if(eve.which == 13){
			$(elDivObj).find('#search').triggerHandler("click");
		}
	});
	
	$(elDivObj).find("#close").click(function(eve){
		 $(elDivObj).find("ul").attr("style","display:none;");
		 $('#resultTable').css({opacity:'1'});
	});
}

function createDateRangeFilter(elDiv){
	var elDivObj = $("#"+elDiv);
	$(elDivObj).find('ul').remove();
    $(elDivObj).find('#facetname').remove();
	var facetname = elDiv;
	var fromBox = elDiv+'_fromBox';
	var toBox = elDiv+'_toBox';
	var filterCons = '<ul><div  id="filterDateDiv"><table><tr>'
					 +'<td style="text-align:right">&nbsp;&nbsp;From&nbsp;</td><td style="text-align:left"><input type="text" id="'+fromBox+'" size="12" class="rangeTxtbox" /></td>'
					 +'</tr><tr><td style="text-align:right">&nbsp;&nbsp;To&nbsp;</td><td style="text-align:left"><input type="text" id="'+toBox+'" size="12" class="rangeTxtbox" />'
					 +'</td></tr></table><div><img id="search" src = "images/search_button.gif"></img>&nbsp;&nbsp;<img id="close" src = "images/close.gif"></img>'
					 +'</div></div></ul>';	
	$(elDivObj).append(filterCons);
	$(elDivObj).append("<input type='hidden' id='facetname' value='"+facetname+"' />");
	$(elDivObj).find(":text").dateMask({separator:'-', format:'ymd'});
	var fromDateObj = $(elDivObj).find('#'+fromBox);
	var toDateObj = $(elDivObj).find('#'+toBox);
	
	$(fromDateObj).datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "yy-mm-dd",
			yearRange: '1900:2100',
			onSelect: function( selectedDate ) {
				selectedDate = getUSformatedDate(selectedDate);
				$(toDateObj).val('');
				$(toDateObj).datepicker( "destroy" );
				$(toDateObj).datepicker({
					changeMonth: true,
					changeYear: true,
					dateFormat: "yy-mm-dd",
					yearRange: '1900:2100',
					minDate: new Date(selectedDate)
				});
			} 
	}); 
	
	$(toDateObj).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "yy-mm-dd",
		yearRange: '1900:2100'
	});
	 
	$(elDivObj).find("#search").click(function(eve){
		 var fromdate = $(fromDateObj).val();
		 var todate = $(toDateObj).val();
		 var filedVal = "";
		 if(fromdate!=""&todate!=""){
			 var fdate=new Date(getUSformatedDate(fromdate));
			 var tdate=new Date(getUSformatedDate(todate));
			 if(tdate < fdate){
				 alert("Error: To date should be greater than From Date.");
				 return false;
			 }
			 if(fromdate == todate){
				 filedVal = '"'+fromdate+'"';
			 }
			 else{
				 var solrFromdate = fromdate;
				 var solrTodate = todate;
				 filedVal = '['+solrFromdate+' TO '+solrTodate+']';
			 }
		 }
		 else if(fromdate==""&todate!=""){
			 var solrTodate = todate;
			 filedVal = '[* TO '+solrTodate+']';
		 }
		 else if(fromdate!=""&todate==""){
			 var solrFromdate = fromdate;
			 filedVal = '['+solrFromdate+' TO *]';
		 }
		 $(elDivObj).find("ul").attr("style","display:none;");
		 $(fromDateObj).val('');
		 $(toDateObj).val('');
		 if(filedVal!=""){
			 var fq = elDiv+':'+filedVal;
			 fqArr.push(fq);
			 currPage = 1;
			 loadLuceneData();
		 }
	});
	
	$(elDivObj).find(":text").keyup(function(eve){
		if(eve.which == 13){
			$(elDivObj).find('#search').triggerHandler("click");
		}
	});
	
	$(elDivObj).find("#close").click(function(eve){
		 $(elDivObj).find("ul").attr("style","display:none;");
		 $('#resultTable').css({opacity:'1'});
	});
}