<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="ADTIncludes.jsp" %> 

	<script type="text/javascript">
		$(function(){
			$('#mainContent').load("TenseADT/generalSearch.html",function(){
				loadLuceneSearchScreen();

				$('#query').live("focus",function(){
					$('.menuli').hide();
					$('.singleIndexMenuli').hide();
					$('#resultTable, #docs').css({opacity:'1'});
				});

				$('#facetUL img').live('click',function(){
					$('#resultTable').css({opacity:'1'});
				});

				$('#resultTable, #docs').live('click',function(){
					$(this).css({opacity:'1'});
					$('#topsearchResult').hide();
					$('.menuli, .singleIndexMenuli').hide();
				});
				
				$("#patient_lab").trigger("click");
				
			});

			
		});

		function loadLuceneSearchScreen(){
			$('#indexUL').html(indexliStr);
			$('#facetUL').html(facetliStr);
			$("#facetUL > li").hover(
					function(){
						//mousenter
						$('#topsearchResult').hide();
						$('.menuli').hide();
						$(this).find('ul:first').css({visibility: "visible",display: "inline"});
						$(this).find('ul:first').addClass("menuli");
						$('#resultTable').css({opacity:'0.3'});
					},function(){});

			 var tooltiplen = $('.menu_tooltip').length;
		     if(tooltiplen == 0){
		     	 $('.menuFacetTitleSpan').tooltip({
		                 track: true,
		                 delay: 0,
		                 showURL: false
		         });
		         $('.menuFacetTitleSpan').addClass('menu_tooltip');
		     }
			loadLuceneData();
			
		}
	</script>


	<div id="mainContent"></div>

