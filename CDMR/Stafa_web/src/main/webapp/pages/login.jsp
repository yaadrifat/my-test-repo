<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="jsp/common/includes.jsp"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8;" />
<meta content="text/html;charset=utf-8" http-equiv="Content-Type" />
<meta content="utf-8" http-equiv="encoding" />
<title>Velos CDM-R</title>


<link type="text/css" href="css/menu/velos_default.css" media="screen"
	rel="stylesheet" type="text/css" />
<link type="text/css" href="css/menu/velos_default.ultimate.css"
	media="screen" rel="stylesheet" type="text/css" />
<link type="text/css" href="css/menu/velos_dropdown.css" media="screen"
	rel="stylesheet" type="text/css" />

<!-- CDMR SPECIFIC CSS -->
<link rel="Stylesheet" type="text/css" href="css/vcdmr.css" />
<link rel="Stylesheet" type="text/css" href="css/cdmr/cdmr_main.css" />


<!-- CDMR SPECIFIC CSS END -->

<link rel="stylesheet" type="text/css" href="css/notes.css" />
<link rel="Stylesheet" type="text/css" href="css/jHtmlArea.css" />
<!-- for workflow -->
<link type="text/css" href="css/workFlow.css" rel="stylesheet" />

<!-- end of  -->
<link type="text/css" href="css/alert.css" rel="stylesheet" />
<link href="images/icons/favicon.ico" type="image/x-icon"
	rel="shortcut icon">
<link type="text/css" href="css/themes/grey.css" rel="stylesheet" />
<!-- 
<link type="text/css" href="css/ui/barcode_style.css" rel="stylesheet" />
 -->

<!-- Barcode Css/JavaScripts Start -->
<link type="text/css" href="css/barcode.css" media="screen"
	rel="stylesheet" />
<link type="text/css" href="css/barcode.css" media="print"
	rel="stylesheet" />
<!-- Barcode Css/JavaScripts End -->

<link type="text/css" href="css/main_style.css" rel="stylesheet" />
<link type="text/css" href="css/Velos_style.css" rel="stylesheet" />
<link type="text/css" href="css/fonts.css" rel="stylesheet" />
<link type="text/css" href="css/portlet.css" rel="stylesheet" />

<!--<link type="text/css" href="css/ui/base/jquery.ui.all.css" rel="stylesheet" />-->
<link type="text/css" href="css/cdmr/themes/base/jquery.ui.all.css"
	rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/demo_table.css"
	media="screen" />
<link rel="stylesheet" type="text/css" href="css/demo_page.css"
	media="screen" />
<link rel="stylesheet" type="text/css" href="css/ui/jquery-ui.css">
<!--<link href="css/tabs.css" rel="stylesheet" type="text/css" />
-->
<!--<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
-->
<!-- Menu.css -->
<link href="css/menu.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/calc_protocol.css" type="text/css" />
<link href="css/newlayout.css" rel="stylesheet" type="text/css">

<style type="text/css">
.copyright {
	font-size: 12px;
	font-weight: normal;
	margin-left: 550px;
}
</style>

<!--  html5 js  for  supporting IE8 broswer-->
<script type="text/javascript" src="js/jquery/modernizr.js"></script>
<script type="text/javascript" src="js/jquery/html5shiv.js"></script>
<script type="text/javascript" src="js/jquery/html5shiv-printshiv.js"></script>
<!-- html js ends -->
<script type="text/javascript" src="js/cdmr/jquery-1.10.2.js"></script>
<script type="text/javascript">
    var myJ = jQuery.noConflict(true);
</script>
<script type="text/javascript" src="js/jquery/jquery-1.6.3.js"></script>
<script type="text/javascript"
	src="js/jquery/jquery-ui-1.8.7.custom.min.js"></script>
<script type="text/javascript" src="js/jquery/jquery.ui.core.js"></script>
<script type="text/javascript" src="js/jquery/jquery.ui.widget.js"></script>
<script type="text/javascript" src="js/jquery/jquery.ui.mouse.js"></script>
<script type="text/javascript" src="js/jquery/jquery.ui.sortable.js"></script>
<script type="text/javascript" src="js/jquery/jquery.ui.resizable.js"></script>
<script type="text/javascript" src="js/jquery/jquery.ui.datepicker.js"></script>

<link rel="stylesheet" type="text/css"
	href="css/ui/base/jquery.ui.tabs.css" media="screen" />
<link type="text/css" href="css/jquery.jqplot.min.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery/graph/excanvas.js"></script>
<script type="text/javascript"
	src="js/jquery/graph/jquery.jqplot.min.js"></script>
<script class="include" type="text/javascript"
	src="js/jquery/graph/jqplot.barRenderer.min.js"></script>
<script class="include" type="text/javascript"
	src="js/jquery/graph/jqplot.categoryAxisRenderer.min.js"></script>
<script class="include" type="text/javascript"
	src="js/jquery/graph/jqplot.pointLabels.js"></script>
<!--<script class="include" type="text/javascript" src="js/jquery/jquery.livequery.js"></script>
-->

<script type="text/javascript" src="js/jquery/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/cdmr/alertify.min.js"></script>
<!--<script type="text/javascript" src="js/cdmr/datatable/js/jquery.dataTables.js"></script>-->



<script type="text/javascript" src="js/dateFormat.js"></script>
<script type="text/javascript" src="js/jquery/jquery.jclock.js"></script>
<script type="text/javascript"
	src="js/jquery/jquery-ui-timepicker-addon.js"></script>

<!-- For Alert success Msg -->
<script type="text/javascript" src="js/jquery/jquery.blockUI.js"></script>
<script type="text/javascript" src="js/cdmr/alertify.min.js"></script>
<link rel="stylesheet" href="css/cdmr/alertify.core.css" type="text/css">
<link rel="stylesheet" href="css/cdmr/alertify.default.css"
	type="text/css">
<!-- DataTable Column Manager Starts 
 
 
  <script type="text/javascript" src="js/jquery/ColVis.js"></script>-->
<link rel="Stylesheet" type="text/css" href="css/dataTable.extend.css" />

<script src="js/jquery/jquery.event.hover.js" type="text/javascript"></script>
<!-- for Vertical menuy bar -->
<script src="js/jquery/jquery.megamenu.js" type="text/javascript"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/commonScript.js"></script>
<script type="text/javascript" src="js/questions.js"></script>

<!-- Drop Down -->

<script type="text/javascript" src="js/jquery/jquery.uniform.js"></script>
<link rel="stylesheet" href="css/uniform.default.css" type="text/css"
	media="screen">
<!-- End  -->

<!-- autocomplete start -->

<!-- <link rel="stylesheet" type="text/css" href="css/jquery-ui.css" media="screen" /> -->
<script type="text/javascript" language="javascript"
	src="js/jquery/jquery.ui.autocomplete.autoSelect.js"></script>

<!-- autocomplete end -->

<!-- note Scripts and CSS starts-->


<!-- Graph start -->
<!--<link type="text/css" href="css/jquery.jqplot.css" rel="stylesheet" />

-->
<!-- Don't touch this! -->
<!--<script class="include" type="text/javascript" src="js/jquery/graph/jquery.jqplot.min.js"></script>-->
<%--     <script type="text/javascript" src="js/XRegExp.js"></script> --%>
<script type="text/javascript" src="js/jquery/graph/shCore.min.js"></script>






<!-- End Don't touch this! -->


<!-- End additional plugins -->
<!-- Graph End -->

<script type="text/javascript" src="js/jquery/jquery.tablednd.js"></script>
<!--Tree-->
<!--<link rel="stylesheet" href="css/jquery.treeview.css" type="text/css"/>-->
<script type="text/javascript" src="js/jquery/jquery.treeview.js"></script>

<!-- Column Manager plugins go here Start -->
<script class="include" language="javascript" type="text/javascript"
	src="js/jquery/columManager/jquery.cookie.js"></script>
<script class="include" language="javascript" type="text/javascript"
	src="TenseADT/js/jquery.columnmanager.js"></script>
<script class="include" language="javascript" type="text/javascript"
	src="TenseADT/js/jquery.clickmenu.js"></script>


<!--  <link rel="Stylesheet" type="text/css" href="css/columManager/columnManagerStyle.css" />-->
<!-- Column Manager plugins go here End -->
<!-- <link rel="Stylesheet" type="text/css" href="css/popup.css" />-->
<!-- Spinner plugins go here start -->
<script src="js/smartspinner.js" type="text/javascript"></script>
<link href="css/smartspinner.css" rel="stylesheet" type="text/css" />
<!-- Spinner plugins go here End  -->

<!-- Stroage css go here Starts -->
<link href="css/storage.css" rel="stylesheet" type="text/css" />
<!-- Stroage css go here End-->
<!-- Added for Tense ADT Search -->
<!--
 <link rel="stylesheet" type="text/css" href="TenseADT/css/tagsearch.css" media="screen"/>
 <link rel="stylesheet" type="text/css" href="TenseADT/menu/style.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="TenseADT/css/reset.css"  media="screen" />
 -->
<link rel="stylesheet" type="text/css" href="TenseADT/css/TenseADT.css"
	media="screen" />


<!-- masking plugins go here start -->
<script type="text/javascript"
	src="js/jquery/jquery.maskedinput-1.2.2.js"></script>
<!-- masking plugins go here ends -->

<!-- Column Filter Css/JavaScripts Start -->
<script class="include" language="javascript" type="text/javascript"
	src="js/jquery/columnfilter.js"></script>
<!-- Column Filter Css/JavaScripts End -->


<script type="text/javascript" src="js/jquery.layout.js"></script>
<script type="text/javascript" src="js/modalDialogHeightWidth.js"></script>
<!-- Global Messages Start -->
<script type="text/javascript" src="js/globalMessages.js"></script>
<!-- Global Messages End -->

<!-- ESecurity Start -->
<script type="text/javascript" src="jsp/esecurity/js/commonconstants.js"></script>
<script type="text/javascript" src="jsp/esecurity/js/eSecurityJS.js"></script>
<!-- ESecurity End -->

<!--  validation start -->
<script type="text/javascript" src="js/jquery/jquery.validate.js"></script>

<script type="text/javascript" src="js/velosValidation.js"></script>
<!-- validation end  -->
<!-- Tense script start -->
<script type="text/javascript" src="js/ADTSearch.js"></script>
<!-- Tense script end -->
<!--  Report test -->


<script type="text/javascript" src="search/lib/core/Core.js"></script>
<script type="text/javascript" src="search/lib/core/AbstractManager.js"></script>
<script type="text/javascript"
	src="search/lib/managers/Manager.jquery.js"></script>
<script type="text/javascript" src="search/lib/core/Parameter.js"></script>
<script type="text/javascript" src="search/lib/core/ParameterStore.js"></script>
<script type="text/javascript" src="search/lib/core/AbstractWidget.js"></script>
<script type="text/javascript" src="search/widgets/ResultWidget.js"></script>
<script type="text/javascript"
	src="search/lib/helpers/jquery/ajaxsolr.theme.js"></script>
<script type="text/javascript" src="search/js/theme.js"></script>

<script type="text/javascript" src="search/js/jquery.livequery.js"></script>
<script type="text/javascript"
	src="search/lib/widgets/jquery/PagerWidget.js"></script>
<script type="text/javascript"
	src="search/lib/core/AbstractFacetWidget.js"></script>
<script type="text/javascript" src="search/widgets/MenuListWidget.js"></script>
<script type="text/javascript"
	src="search/lib/core/AbstractTextWidget.js"></script>
<script type="text/javascript" src="search/widgets/ListFacetWidget.js"></script>
<script type="text/javascript" src="search/widgets/TagcloudWidget.js"></script>
<script type="text/javascript"
	src="search/widgets/CurrentSearchWidget.q.js"></script>

<!--<script type="text/javascript" src="search/ext/jquery.autocomplete.js"></script>-->
<script type="text/javascript"
	src="search/widgets/AutocompleteWidget.js"></script>

<script type="text/javascript" src="search/widgets/MenuFacetWidget.js"></script>
<script type='text/javascript' src='search/widgets/TopSearchWidget.js'></script>
<script type="text/javascript" src="search/widgets/CalendarWidget.js"></script>


<script type='text/javascript' src='search/js/filter.js'></script>
<script type='text/javascript' src='search/js/mask.js'></script>
<script type='text/javascript' src='search/js/jquery.alerts.js'></script>

<script type='text/javascript'
	src='search/js/jquery.menuautocomplete.js'></script>
<script type='text/javascript' src='search/js/menufilter.js'></script>
<script type='text/javascript' src='search/js/topsearchfilter.js'></script>


<script type="text/javascript" src="search/js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="js/jquery/jHtmlArea-0.7.0.js"></script>
<script type="text/javascript" src="js/jquery/jHtmlArea-0.7.0.min.js"></script>

<!--  end of Report Test -->
<!--  js for Timeout -->
<link type="text/css" href="css/jquery.countdown.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery/jquery.countdown.js"></script>
<script type="text/javascript" src="js/jquery/jquery.idletimeout.js"></script>
<script type="text/javascript" src="js/jquery/jquery.idletimer.js"></script>


<!--  -->
<script type="text/javascript" src="js/jquery/jquery.svelos.js"></script>
<%-- Ajax File Upload js --%>
<script type="text/javascript" src="js/ajaxfileupload.js"></script>

</head>

<%
String sessionid = request.getSession().getId();
//be careful overwriting: JSESSIONID may have been set with other flags
//response.setHeader("SET-COOKIE", "JSESSIONID=" + sessionid + "; HttpOnly");
%>

<script>

var outerLayout, middleLayout, innerLayout; 
//alert("contextPath " + contextpath);
$(document).ready(function() {
	  
	outerLayout = $('body').layout({ 
		center__paneSelector:	".outer-center" 
	,	west__paneSelector:		".outer-west" 
	,	east__paneSelector:		".outer-east" 
	,	west__size:				250
	,	east__size:				200 
	,	spacing_open:			10 // ALL panes
	,	spacing_closed:			10 // ALL panes
	,	north__spacing_open:	0
	,	south__spacing_open:	0
	,	center__onresize:		"middleLayout.resizeAll" 
	}); 

	middleLayout = $('div.outer-center').layout({ 
		center__paneSelector:	".middle-center" 
	,	west__paneSelector:		".middle-west" 
	,	east__paneSelector:		".middle-east" 
	,	west__size:				75
	,	east__size:				75 
	,	spacing_open:			12  // ALL panes
	,	spacing_closed:			12 // ALL panes
	,	center__onresize:		"innerLayout.resizeAll" 
	}); 

	innerLayout = $('div.middle-center').layout({ 
		center__paneSelector:	".inner-center" 
	,	west__paneSelector:		".inner-west" 
	,	east__paneSelector:		".inner-east" 
	,	west__size:				75 
	,	east__size:				75 
	,	north__size:			100 
	,	spacing_open:			8  // ALL panes
	,	spacing_closed:			8  // ALL panes
	,	west__spacing_closed:	12
	,	east__spacing_closed:	12
	}); 
		

    $('#header').load('html/common/login_header.html');
	$('#loginfooter').load('html/common/login_footer.html');
	removePane();
	$("#baner").removeClass("hidden");
	$('.progress-indicator').css( 'display', 'block' );
	//var url = "fetchVersionDetails";
	//var result = ajaxCallWithParam(url,"");
	var versionId = "Version 2.0 Build 18"; 
		
	//if(result.returnList!=null && result.returnList.length>0){
	//	versionId = result.returnList[0][1]
	//}
	$("#versionId").html(versionId);
	/* calcdiv();
	    $("innercenter").bind('resize', calcdiv); */
	$('.progress-indicator').css( 'display', 'none' );
	   // getKeys();
//	$('#outercenter').css({  margin: "40px", border: "1px solid ", height:"auto", width:"auto"});

});
var errorMessage = "";
var keys;
function getKeys() {
	
	$.jCryption.getKeys("getEncryptKey.action?generateKeypair=true", function(
			receivedKeys) {
		keys = receivedKeys;
		var errorMessage = isValidUser();
		if(errorMessage == ""){
			
			 addPane();
			 allWidgetHide();
			 $('#menu_nav').load('jsp/submenu.jsp',function(){
				 
			    $("#pagetitle").show();
			    if (checkSupervisorView()){		
				    
					//loadAction('loadSupHomePage.action');
			    	loadAction('loadCDMRHomePage.action');
				}else if(checknurseview()){
					
					//loadAction('loadNurseHomePage.action');
					loadAction('loadCDMRHomePage.action');
				}else if(checkRecipientTracker()){
					
			         //loadAction('loadRecipientTracker.action');
					loadAction('loadCDMRHomePage.action');
				}else{
					
					loadAction('loadCDMRHomePage.action');
				}
			 });
			 $('#header').hide();
			 
			 $('.disclaimer').hide();
			 $('#loginfooter').hide();
			 $('#footer').load('html/common/footer.html');
			 sessionCheck();
		}else if(errorMessage != 'PASSWORDEXPIRED'){
			//$("#pagetitle").hide();
			//$("#errormesdiv").css("color","red");
			//$("#errormesdiv").html(errorMessage);
			//$("#errorMessageDialog").dialog('open');
			$.blockUI({title:'<img style="cursor:auto;width:16px;height:16px;" src="images/cdmr/closeicon.png" onclick="closeErrorPopUp();"/>', theme:true, draggable:true, message: $('#errorMessageDialog'), 
				themedCSS: {width: '50%', left:'300px', top:'300px'} }); 
		}
	});
}

function closeErrorPopUp(){
	$.unblockUI(); 
}
   
var docsId = "0";
var docsType="";
var docsName="";
function openPopup(docId,type,fileName){
    docsId = docId;
    docsType = type;
    docsName = fileName;
    addNewPopup("Document Review","docsReview","openDocumentReview","800","800","");
}

function isValidUser(){
	var errorMessage = "";
	var data = "";
	var user = $("#username").val();
	var password = $("#password").val();
	$.jCryption.encrypt(user, keys, function(encrypted) {
		encryptedUser = encrypted;
		$.jCryption.encrypt(password, keys, function(encryptedPasswd) {
			encryptedPassword = encryptedPasswd;
			
			data = "username="+encryptedUser+"&password="+encryptedPassword;
			//alert("Data:::"+data);
			var username = $("#username").val();
			
			$('.progress-indicator').css( 'display', 'block' );
			 $.ajax({
			        type: "POST",
			        url: "loadLoginDetails.action",
			        data: data,
			        async:false,
			        success: function (result){
			        	
			        	//alert(result.errorMessage);
			        	//alert(result.errorCode);
			        	var errorCode = result.errorCode;
			        	
			        	if(errorCode == "PASSWORDEXPIRED"){
		            		errorMessage = result.errorCode;
		            		
			            	$('#login_box').load('jsp/esecurity/change_Password.jsp',function(){
			            		$('#loginName').val(username);
			            	});
			            }
			        	else{
			        		errorMessage = result.errorMessage;
			        	}
			        },
			        error: function (request, status, error) {
			        	alert("Error " + error);
			            alert(request.responseText);
			        }

				});
			 $('.progress-indicator').css( 'display', 'none' );
		});
	});
	//"userName="+encryptedUser+"&password="+encryptedPassword;
	//alert("Data:::"+data);
	
	 return errorMessage;
}



/**For Jboss*/


function validateuser(){
	getKeys();	
}

// for physician pages
var allPages = ['s1','s2','s3','s4','s5','s6'];

function gotoNextPage(){
	//alert(allPages);
	//alert($("#currentPage").val());
	//alert(allPages.length);
	
	var idx = jQuery.inArray($("#currentPage").val(), allPages);
	//alert(idx);
	idx = idx+1;
	//alert(idx);
	//alert(allPages[idx]);
	if(idx < 6){
		var url = 'html/physician/'+allPages[idx]+'.html';
	}
	loadPage(url);
}
function gotoPreviousPage(){
	//alert(allPages);
	//alert($("#currentPage").val());
	//alert(allPages.length);
	var idx = jQuery.inArray($("#currentPage").val(), allPages);
	//alert(idx);
	idx = idx-1;
	//alert(idx);
	if(idx>=0){
	var url = 'html/physician/'+allPages[idx]+'.html';
	loadPage(url);
	}
}
function cleanData(){
	$("#forgotuserName").val("");
	$("#email").val("");
	$("#errormesssagetd").html("");
}

$(function() {
	  $( "#forgotpasswddiv").dialog(
	   {autoOpen: false,
	   	title: 'Forgot Password ',
	    modal: true, width:width_550+modalWidthConstant, height:height_300+modalHeightConstant,
	    close : function(){
	    	cleanData();
			//jquery("#respondentDiv").dialog("destroy");
	      }
	    }
	   );
	 // jQuery("#respondentDiv").dialog("destroy");

	 }); 
	 function showpopup(){
		 $("#from").val("");
		 $("#notifyTo").val("");
		 $("#forgotpasswddiv").dialog({	
				title:"Forgot Password",
				width:550,
			 	height:modalHeightConstant + height_300,
			 	autoOpen: false,
				modal: true,
				close: function() {
		    		jQuery("#forgotpasswddiv").dialog("destroy");
				}
			 });

			$("#forgotpasswddiv").dialog("open");
	}

</script>

<body>

<div class="progress-indicator"><img
	src="images/icon_loading_75x75.gif" alt="" /></div>
<div id="ieprogressindicator" class="" style="display: none;">
<center><img src="images/icon_loading_75x75.gif" alt="" /></center>
</div>


<div id="sessionexpired" style="display: none;">Your Session Gets
Expired in <span id="exptime"></span></div>
<div id="modelPopup"></div>
<div id="modelPopup1"></div>
<div id="modeldialog">
<div id="dialogContent"></div>
<div id="labResults" style="display: none"></div>
</div>
<div id="preview" style="display: none"></div>
<span id="userDisplay" style="display: none"> </span>
<input type="hidden" id="userRole" name="userRole" value="" />
<input type="hidden" id="activity" name="activity" value="0" />
<span style="display: none"> <select id="hnextOption"
	name="hnextOption"></select></span>



<!-- Print Hidden DIV Starts-->
<div id="widgetcollect" title="Widgets" style="display: none;"></div>
<!-- Print Hidden DIV Ends -->
<!-- footer ends here-->
<!-- <div id="noteDialog" title="Note" style="display: none">
		<textarea id="dialogEditor" rows="10" style="resize:both;max-width:100%; min-width:400px;" ></textarea>
		<div align="center">
		<input type="text" size="5" value="" placeholder="e-Sign"/>
		<input type="button" value="Save" onClick="savingNotes('');"/>
		</div>
		<div id="existingNotes"></div>
    </div> -->
<div id="notesContainer"></div>
<div id="maintpopup" style="display: none" name="maintpopup">
<form action="#" id="maintenanceform"><input type="hidden"
	name="calList" id="calList" /> <input type="hidden"
	name="calListTempType1" id="calListTempType1" /> <input type="hidden"
	name="calListTempType2" id="calListTempType2" /> <input type="hidden"
	name="seqarray" id="seqarray" /> <input type="hidden" name="pkCodelst"
	id="pkCodelst" /> <input type="hidden" name="type" id="type"> <input
	type="hidden" name="isHide" id="isHide"> <!-- <input type="hidden" name="maintDeletedFlag" id="maintDeletedFlag" value="0"> -->
<input type="hidden" name="deletedFlag" id="deletedFlag" value="0">
<input type="hidden" name="maintainable" id="maintainable" value="0">
<input type="hidden" name="defaultValue" id="defaultValue" />
<table width="100%">
	<tr>
		<td><s:text name="stafa.maintenance.label.name" /></td>
		<td><input type="text" name="description" id="description"
			onKeyUp="javascript:loadAutoCodelstData('');" /></td>

		<td><s:text name="stafa.maintenance.label.code" /></td>
		<td><input type="text" name="subType" id="subType"
			onKeyUp="javascript:loadAutoCodelstData('subtype');" /></td>
	</tr>
	<tr>
		<td><s:text name="stafa.maintenance.label.value" /></td>
		<td><input type="text" name="codelstvalue" id="codelstvalue" /></td>

		<td><s:text name="stafa.maintenance.label.comments" /></td>
		<td><input type="text" name="comments" id="comments"></td>
	</tr>
	<tr>
		<td><s:text name="stafa.maintenance.label.default" /></td>
		<td><input type="checkbox" name="tempdefault" id="tempdefault"></td>
		<td><s:text name="stafa.maintenance.label.deactivate" /></td>
		<td><input type="checkbox" name="tempisHide" id="tempisHide"></td>
		<td align="left"><s:text name="stafa.label.delete" /></td>
		<td align="left"><input type="checkbox" name="tempisDelete"
			id="tempisDelete"></td>
	</tr>

	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4" align="center" id="messagetd">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4" align="center"><input type="button" value="New"
			onclick="javascript:clearvalue();" />&nbsp;&nbsp;<input
			type="button" value="Save" onclick="javascript:return saveCodelst();" />
		</td>
	</tr>

</table>


</form>
</div>
<!-- QC Codelst popup -->
<div id="addnewDialog" title="addNew" style="display: none">
<form action="#" id="reagentsNewForm"><input type="hidden"
	name="pkCodelst" id="reagentpkCodelst" /> <input type="hidden"
	name="type" id="reagenttype" value="reagentssuppl"> <input
	type="hidden" name="isHide" id="reagentisHide"> <input
	type="hidden" name="maintainable" id="reagentmaintainable" value="0">
<input type="hidden" name="defaultValue" id="reagentdefaultValue" />
<table border="0">
	<tr>
		<td>Name</td>
		<td><input type="text" name="description" id="reagentdescription" /></td>
	</tr>
	<tr>
		<td>Code</td>
		<td><input type="text" name="subType" id="reagentsubType"></td>
	</tr>
	<tr>
		<td>Default</td>
		<td><input type="checkbox" name="tempdefault"
			id="reagenttempdefault"></td>
	</tr>
	<tr>
		<td>DeActivate</td>
		<td><input type="checkbox" name="tempisHide"
			id="reagenttempisHide"></td>
	</tr>
	<tr>
		<td colspan="4" align="center">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4" align="center" id="reagentmessagetd">&nbsp;</td>
	</tr>
	<tr>
		<td align="right" colspan="4"><input type="button" value="New"
			onclick="javascript:clearvalue();" />&nbsp;&nbsp;<!-- <input type="button" value="New" onclick="javascript:newReagentSuppliers();">&nbsp;&nbsp; --><input
			type="button" value="Submit"
			onclick="javascript:return saveNewReagentSuppliers();" /></td>
	</tr>
</table>
</form>
</div>

<div class="outer-center">

<div class="middle-center">

<div class="inner-center centerlayout-marginheight" id="inner_center"
	style="overflow-x: auto; overflow-y: auto;"><header id="header">

</header> <section id="content_wrapper">

<div id="content_inner_new" class="content_inner_new">



<div id="storage"><input type="hidden" value="" id="storageinput" />
</div>
<div id="main"><!--Fremont Team: login page images; css in main_style.css-->
<div id="helix"><img src="images/others/img-header-inner.png"
	align="left" /></div>
<div id="emp"><img src="images/others/img-empowering.png"
	align="left" /></div>
<!-- App Name Code -->
<div id="appName"><s:property
	value="getText('vcdmr.label.appname')" /></div>

<!-- App Name Code -->
<div id="login_bar_wrapper">
<div id="login_box">
<div class="login_top_bg"></div>

<s:form action="#" id="frmLogin" onsubmit="return false;">
	<div class="login_middle_bg">
	<div class="username"><input type="text" id="username"
		name="username" value="" autocomplete="off" placeholder="Username" />
	</div>
	<div class="password"><input type="password" value=""
		autocomplete="off" name="password" id="password"
		placeholder="Password" autocomplete="off" /></div>
	<div id="errormesdiv"></div>
	<div class="signin" align="center">
	<button onClick="javascript:validateuser();">Sign in</button>
	</div>
	<div class="forget" align="center"><a href="#"
		onclick="showpopup();"><i>Forgot password?</i></a></div>
	<div class="version" align="center" id="versionId"></div>
	</div>
	<div class="login_bottom_bg"></div>
</s:form></div>
</div>
</div>
</div>

</section>

<div id="loginfooter" style="margin-top: 5%"></div>





</div>

<!-- <div id="baner" class="ui-layout-north hidden"  style="overflow:auto">
		<form id="loginForm"  onsubmit="return avoidFormSubmitting()">
			<table cellpadding="0" cellspacing="0" class="" align="" border="0" width="100%">
			<tr>
					<td><span id="pagename"  Class="titleText" style="float:left;">  </span> </td>
					<td> <span id="pagecontrols" style="float:right;"><input type="hidden" name="flowNextFlag" id="flowNextFlag" value=""/><input type="password" style="width:80px;" size="5" value="" id="eSign" name="eSign" placeholder="e-Sign"  autocomplete="off" />&nbsp;&nbsp;
						<input type="button" value="Save" onclick="verifyeSign('eSign','save')"/>  </span></td>
			</tr>
			<tr>
			       <td colspan="2"> <table id="tracker" cellpadding="0" cellspacing="0"><tr class="workFlowContainer"></tr></table></td>
			</tr>
			</table>
		</form>

		</div> -->
<div class="ui-layout-south"></div>

</div>

</div>

<div id="innerwest" class="outer-west" style="overflow: auto;"></div>
<div id="innereast" class="outer-east" style="overflow: auto;"></div>

<div class="ui-layout-north north-height ui-north-zindex"
	style="overflow: visible;">

<div id="menu_nav"></div>



</div>
<div class="ui-layout-south .north-south"><footer id="footer">
<div class="footer_wrapper_new">
<div class="footer_inner_new"></div>
</div>
</footer></div>

<!-- Layout Implemtation Ends Here -->

<div id="forgotpasswddiv" class="hidden"><s:form id="forgotform"
	method="post">
	<table>
		<s:hidden name="notifyId" />
		<tr>
			<td>User Name :</td>
			<td><input type="text" id="from" name="from" /></td>
		</tr>
		<tr>
			<td>Email :</td>
			<td><input type="text" id="notifyTo" name="notifyTo" /></td>
		</tr>
		<tr>
			<td colspan="2" align="center" id="errormesssagetd"></td>
		</tr>
		<tr>
			<td colspan="2" align="center">
			<button type="button" onclick="javascript:return forgotpassword();">Submit</button>
			</td>
		</tr>
	</table>
</s:form></div>

<div id="addqc" title="add qc" style="display: none">
<div id="qcResultInfo"></div>


</div>

<!-- Print Hidden DIV Starts-->
<div id='printDiv' class="hidden" style="height: 0px !important;"></div>
<div id='printBarcodeDiv' style="height: 0px;"></div>
<!-- Print Hidden DIV Ends -->
<div id="docsReview" style="display: none;"></div>
<div class="barcode_wrapper" id="barcodeTem" style="display: none;"></div>

<!-- Popup for Print Widgets Starts-->
<div id="widgetCollect" class="model" title="Widgets"
	style="display: none">
<form action="#" id="widgetcollectForm"><%-- <table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td  style="font-weight:bold">Follow-Up Verification</td><td><input type="checkbox" id='followupVerification' /></td>
			</tr>
			<tr>
				<td  style="font-weight:bold"><s:text name="stafa.label.donorcontactinformation"/></td><td><input type="checkbox" id='donorCtctInfo' /></td>
			</tr>
			<tr>
				<td  style="font-weight:bold"><s:text name="stafa.label.nextdaycollection"/></td><td><input type="checkbox" id='nextDayColl' /></td>
			</tr>
			<tr>
				<td  style="font-weight:bold"><s:text name="stafa.label.mobilizationorder"/></td><td><input type="checkbox" id='mobileOrder' /></td>
			</tr>
			<tr>
				<td  style="font-weight:bold"><s:text name="stafa.label.mobilizationinstruction"/></td><td><input type="checkbox" id='mobileInstruction' /></td>
			</tr>
			<tr>
				<td  style="font-weight:bold"><s:text name="stafa.label.donorinstruction"/></td><td><input type="checkbox" id='donorInstruction' /></td>
			</tr>
		</table>
		<div align="right" style="float:right;">
			<table cellpadding="0" cellspacing="0" class="" align="" border="0">
				<tr>
					<td><input type="button" onclick="printToDiv();" value="Print"/></td>
					<td><input type="button" onclick="closePopup();" value="Cancel"/></td>
				</tr>
			</table>
		</div> --%></form>
</div>
<!-- Popup for Print Widgets Ends	-->

<div id="addLabTest" class="model" title="addNew" style="display: none">
<form action="#" id="addLabTestForm">
<table cellpadding="0" cellspacing="0" border="0" id="addLabTable"
	class="display">
	<thead>
		<tr>
			<th><input type='checkbox'
				onclick="checkall('addLabTable',this)" />Check All</th>
			<th>Code</th>
			<th>Description</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
<div align='right' style='float: right;'>
<table cellpadding='0' cellspacing='0' class='' align='' border='0'>
	<tr>
		<td><input type='button' onclick='saveLabTest();' value='Save' /></td>
		<td><input type='button' onclick='closePopup()' value='Cancel' /></td>
	</tr>
</table>
</div>
</form>
</div>

<div id="barcodeprint" style="display: none; width: 100%;">
<div id=""
	style="height: 200px; width: 49%; float: left; background-color: white; border-radius: 10px; border: 1px solid black; margin-left: 0%">
<form id="barcodeReprintForm">
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><b>Product Description</b></td>
		<td><label id="rePrintProductDescription"></label></td>
	</tr>
	<tr>
		<td><b>Original Print Date</b></td>
		<td><label id="rePrintOrigPrintDate"></label></td>
	</tr>
	<tr>
		<td colspan="2"><b>Specify the Reason for Reprinting</b></td>
	</tr>
	<tr>
		<td colspan="2"><textarea id="reasonForRprint" class="" row="30"
			col="150" /></textarea></td>
	</tr>

</table>
</form>
</div>

<div id=""
	style="width: 49%; float: right; padding-right: 1%; background-color: white;">
<div id="barcodeMainDiv" class="barcode_wrapper"></div>
</div>
</div>

<div id="modalpopup"></div>

<div id="plasmaLabelVerification" style="display: none"></div>
<div id="labelverified" style="display: none;"></div>
<div id="verifyUserPopup" style="display: none">
<form>
<table style="width: 100%">
	<tr>
		<td>3-4 Id</td>
		<td><input type="text" name="custom3Id" id="custom3Id"
			placeholder="3-4 Id" /></td>
	</tr>
	<tr>
		<td>E-Sign</td>
		<td><input type="password" name="verifyesign" id="verifyesign"
			placeholder="e-Sign" autocomplete="off" /></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td style="float: right; padding-right: 20%"><input type="button"
			value="Verify" id="verifyESignBut"></td>
	</tr>
</table>
</form>
</div>

<div id="sessionout" class="" style="display: none" align="center">
<table width="100%">
	<tr>
		<td align="center"><span style=""><b>You will be
		logged out in</b> </span>&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td align="center"><b><span id="highlightCountdown"
			class="countdown" style=""></span>&nbsp;&nbsp;</b></td>
	</tr>
	<tr>
		<td><span id="" class="" style="cursor: pointer; float: left;"><b>To
		stay logged in, Please click ok</b></span><br>
		</td>
	</tr>
	<tr>
		<td align="center">
		<form>
		<button type="button" id="highlightButton" style="">OK</button>
		</form>
		</td>
	</tr>
</table>
</div>

<!-- -Dialog Error Message -->
<div id="errorMessageDialog" style="display: none;">
<p>We are sorry; You can not be logged on due to one of the
following reason:</p>
<ol>
	<li>You did not provide Username or Password.</li>
	<li>You have provided wrong username and password.</li>
	<li>Your account is not yet activated.</li>
	<li>Your account is expired. Please check with your account
	administrator.</li>
	<li>Application version does not match with current database
	version.</li>
</ol>
<input type="button" value="Ok" onclick="closeErrorPopUp();" style="float: right;">
</div>
<!-- Dialog Error Message -->
<!-- CDMR SPECIFIC JS -->
<script type="text/javascript">
/*$(document).ready(function(){
	$("#errorMessageDialog").dialog({
		autoOpen: false,
	    modal: true,
	    width:550,
	    buttons: {
	        Ok: function() {
	            $(this).dialog("close").remove();
	        }
	    },
	    close:function(){
			$(this).dialog('close').remove();
	    }
	});
});*/

</script>
<!-- CDMR SPECIFIC JS CLOSE-->
</body>

<%

String errorMessage = (String)session.getAttribute("sessionerror");
session.removeAttribute("sessionerror");

%>
<script>
var sessiontimeout = <s:text name="stafa.session.timeout"/>;

var errorMessage = "<%=errorMessage%>";

if(errorMessage != null && errorMessage=="sessionexpired"){
	$("#errormesdiv").html('<s:text name="stafa.session.expired" />');
}
var dataTableJson;
</script>