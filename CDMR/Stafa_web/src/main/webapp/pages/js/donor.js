
function showRecipientTable(donorobj,modtype){
	var donorType= $(donorobj).find('option:selected').text(); 
	//alert("donorType>" + donorType+"<");
	if(donorType=="Auto" || donorType=="AUTO"){
		//?BB $(".ui-dialog").find("#recipientTable").addClass("hidden");
		$("#tmpdonorType").val("1");
		bindAutoType(true);
		
		if(modtype=='ctl'){
			setAutoDonorDetails();
		}else{
		setAutoRecipientDetails();
		}
		//$("#recipientTable").addClass("hidden");
	}else{
		$(".ui-dialog").find("#recipientTable").removeClass("hidden");
		//$("#recipientTable").removeClass("hidden");
		$("#tmpdonorType").val("0");
		bindAutoType(false);
		if(modtype=='ctl'){
			resetDonorData();
		}else{
		resetRecipientData();
		}
		$("#recipientMRN").val("");
		
	}
}

function setAutoRecipientDetails(){
	$("#recipientPerson").val($("#donorPerson").val());
	$("#recipientLastName").val($("#donorLastName").val());
	$("#recipientFirstName").val($("#donorFirstName").val());
	$("#recipientDob").val($("#donorDob").val());
	$("#recipientMRN").val($("#donorMRN").val());
	$("#recipientSSN").val($("#donorSSN").val());
	$("#recipientabo").val($("#donorabo").val());
	$("#recipientWeight").val($("#donorWeight").val());
	$("#recipientAbs").val($("#donorAbs").val());
	$("#recipientMiddleName").val($("#donorMiddleName").val());
	//$("#donorUnitsHeight").val(var13);
	$("#recipientUnitsWeight").val($("#donorUnitsWeight").val());
	$.uniform.restore('select');
	$("select").uniform(); 	
}

function resetRecipientData(){
	//$("#recipientMRN").val("");
	$("#recipientSSN").val("");
	$("#recipientLastName").val("");
	$("#recipientFirstName").val("");
	$("#recipientMiddleName").val("");
	$("#recipientWeight").val("");
	$("#recipientDob").val("");
	clearDropDown('donordiagnosis');
	clearDropDown('recipientUnitsWeight');
	clearDropDown('transplantPhysician');
	clearDropDown('recipientAbs');
	clearDropDown('recipientabo');
}

function setAutoDonorDetails(){
	$("#donorPerson").val($("#recipientPerson").val());
	$("#donorLastName").val($("#recipientLastName").val());
	$("#donorFirstName").val($("#recipientFirstName").val());
	$("#donorDob").val($("#recipientDob").val());
	$("#donorMRN").val($("#recipientMRN").val());
	$("#donorSSN").val($("#recipientSSN").val());
	$("#donorabo").val($("#recipientabo").val());
	$("#donorWeight").val($("#recipientWeight").val());
	$("#donorAbs").val($("#recipientAbs").val());
	$("#donorMiddleName").val($("#recipientMiddleName").val());
	//$("#donorUnitsHeight").val();
	$("#donorUnitsWeight").val($("#recipientUnitsWeight").val());
	$.uniform.restore('select');
	$("select").uniform(); 	
}
function resetDonorData(){
	//$("#recipientMRN").val("");
	$("#donorSSN").val("");
	$("#donorLastName").val("");
	$("#donorFirstName").val("");
	$("#donorMiddleName").val("");
	$("#donorWeight").val("");
	$("#donorDob").val("");
	$("#donorHeight").val("");
	clearDropDown('donorUnitsHeight');
	clearDropDown('donorUnitsWeight');
	clearDropDown('physicianID');
	clearDropDown('donorAbs');
	clearDropDown('donorabo');
}




function bindAutoType(flag){
	if(flag){
		$("#donorMRN").bind("keyup",function(){
		  $("#recipientMRN").val($("#donorMRN").val());	
		});
		$("#recipientMRN").bind("keyup",function(){
			  $("#donorMRN").val($("#recipientMRN").val());	
			});
		$("#donorSSN").bind("keyup",function(){
			  $("#recipientSSN").val($("#donorSSN").val());	
		});
		$("#recipientSSN").bind("keyup",function(){
			  $("#donorSSN").val($("#recipientSSN").val());	
		});
		$("#donorLastName").bind("keyup",function(){
			  $("#recipientLastName").val($("#donorLastName").val());	
		});
		$("#recipientLastName").bind("keyup",function(){
			  $("#donorLastName").val($("#recipientLastName").val());	
		});
		
		
		$("#donorFirstName").bind("keyup",function(){
			  $("#recipientFirstName").val($("#donorFirstName").val());	
		});
		$("#recipientFirstName").bind("keyup",function(){
			  $("#donorFirstName").val($("#recipientFirstName").val());	
		})
		
		
		$("#donorWeight").bind("keyup",function(){
			  $("#recipientWeight").val($("#donorWeight").val());	
		});
		$("#recipientWeight").bind("keyup",function(){
			  $("#donorWeight").val($("#recipientWeight").val());	
		});
		
		$("#donorMiddleName").bind("keyup",function(){
			  $("#recipientMiddleName").val($("#donorMiddleName").val());	
		});
		$("#recipientMiddleName").bind("keyup",function(){
			  $("#donorMiddleName").val($("#recipientMiddleName").val());	
		});
		/*
		$("#donorHeight").bind("keyup",function(){
			  $("#recipientLastName").val($("#donorHeight").val());	
		});
		*/
		$("#donorDob").bind("keyup",function(){
			
			  $("#recipientDob").val($("#donorDob").val());	
		});
		$("#recipientDob").bind("keyup",function(){
			
			  $("#donorDob").val($("#recipientDob").val());	
		});
		
		$("#donorDob").bind("change",function(){
			
			  $("#recipientDob").val($("#donorDob").val());	
		});
		$("#recipientDob").bind("change",function(){
			
			  $("#donorDob").val($("#recipientDob").val());	
		});
		$("#donorAbs").bind("change",function(){
			
			  $("#recipientAbs").val($("#donorAbs").val());	
		});
		$("#recipientAbs").bind("change",function(){
			
			  $("#donorAbs").val($("#recipientAbs").val());	
		});
		
	}else{
		
			$("#donorMRN").unbind("keyup");
			$("#recipientMRN").unbind("keyup");
			$("#donorSSN").unbind("keyup")
			$("#recipientSSN").unbind("keyup")
			$("#donorLastName").unbind("keyup");
			$("#recipientLastName").unbind("keyup");
			$("#donorFirstName").unbind("keyup");
			$("#recipientFirstName").unbind("keyup");
			$("#donorMiddleName").unbind("keyup");
			$("#recipientMiddleName").unbind("keyup");
			$("#donorWeight").unbind("keyup");
			$("#donorWeight").unbind("keyup");
			$("#donorDob").unbind("change")
			$("#recipientDob").unbind("change");
			$("#donorDob").unbind("keyup")
			$("#recipientDob").unbind("keyup");
			$("#donorAbs").unbind("change");
			$("#recipientAbs").unbind("change");
			
	}
	
	
}


function changeABO(param){
	if($("#tmpdonorType").val() == 1){
		if( param=="donor" || param=="DONOR" ){
			$("#recipientabo").val($("#donorabo").val());
			  $.uniform.restore('#recipientabo');
				$("#recipientabo").uniform(); 		
		}else if ( param=="recipient" || param=="RECIPIENT"){
			$("#donorabo").val($("#recipientabo").val());
			  $.uniform.restore('#donorabo');
				$("#donorabo").uniform();
				}
	}
	
}
function changeABS(param){
	if($("#tmpdonorType").val() == 1){
		if( param=="donor" || param=="DONOR" ){
			$("#recipientAbs").val($("#donorAbs").val());
			  $.uniform.restore('#recipientAbs');
				$("#recipientAbs").uniform(); 		
		}else if ( param=="recipient" || param=="RECIPIENT"){
			$("#donorAbs").val($("#recipientAbs").val());
			  $.uniform.restore('#donorAbs');
				$("#donorAbs").uniform();
				}
				
	}else{
		if(param=='recipient'){
			 $.uniform.restore('#recipientAbs');
				$("#recipientAbs").uniform()
			}else{
				$.uniform.restore('#donorAbs');
				$("#donorAbs").uniform();
			}
		}
	}

function changeUnit(param){
	if($("#tmpdonorType").val() == 1){
		if( param=="donor" || param=="DONOR" ){
			$("#recipientUnitsWeight").val($("#donorUnitsWeight").val());
			  $.uniform.restore('#recipientUnitsWeight');
				$("#recipientUnitsWeight").uniform(); 		
		}else if ( param=="recipient" || param=="RECIPIENT"){
			$("#donorUnitsWeight").val($("#recipientUnitsWeight").val());
			  $.uniform.restore('#donorUnitsWeight');
				$("#donorUnitsWeight").uniform();
				}
	}
	
}


function changeGender(param){
	if($("#tmpdonorType").val() == 1){
		if( param=="donor" || param=="DONOR" ){
			$("#recipientGender").val($("#gender").val());
			  $.uniform.restore('#recipientGender');
				$("#recipientGender").uniform(); 		
		}else if ( param=="recipient" || param=="RECIPIENT"){
			$("#gender").val($("#recipientGender").val());
			  $.uniform.restore('#gender');
				$("#gender").uniform();
				}
				
	}/*else{
		if(param=='recipient'){
			 $.uniform.restore('#recipientAbs');
				$("#recipientAbs").uniform()
			}else{
				$.uniform.restore('#donorAbs');
				$("#donorAbs").uniform();
			}
		}*/
	}
$(document).ready(function(){
	$(".decimalTextBox").live("keypress" ,function (evt) {
		evt = (evt) ? evt : window.event;
	    var charCode = (evt.which) ? evt.which : evt.keyCode;
		 if (charCode ==8 ||charCode ==46|| (charCode >=48 && charCode <=57)) {
		    if ((pointPos = this.value.indexOf('.')) >= 0){
		    	 $(this).attr("maxLength", pointPos+4);
		    	 }else{
		    		 $(this).removeAttr("maxLength");
		    	 }
		 }else{
			 //$(this).val("");
			 //?BB return false;
		 }
	});
});

function autoCheck(){
	var donorType= $("#donorType option:selected").text();
	//alert("donor"+donorType);
			if(donorType!="AUTO" && donorType!="Auto" ){
				//alert("inside");
			var donorMrn=$("#donorMRN").val();
			var recipientMrn=$("#recipientMRN").val();
				if(donorMrn==recipientMrn){
					alert("Please Select the Product Type to Auto");
					return false;
			}
	}
			return true;
}