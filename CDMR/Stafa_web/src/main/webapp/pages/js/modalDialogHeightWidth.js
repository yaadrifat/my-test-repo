var modalHeightConstant;
var modalWidthConstant;

if (jQuery.browser.msie) {
	 modalHeightConstant = 250;
	 modalWidthConstant=0;
}else{
	modalHeightConstant = 0;
	modalWidthConstant=0;
}

	/** Height Specifications*/
	
	var height_50 = 50;
	var height_100 = 100;
	var height_150 = 150;
	var height_200 = 200;
	var height_250 = 250;
	var height_300 = 300;
	var height_350 = 350;
	var height_400 = 400;
	var height_450 = 450;
	var height_460 = 460;
	var height_475 = 475;
	var height_480 = 480;
	var height_500 = 500;
	var height_550 = 550;
	var height_580 = 580;
	var height_600 = 600;
	var height_620 = 620;
	var height_650 = 650;
	var height_700 = 700;
	var height_750 = 750;
	var height_800 = 800;
	var height_850 = 850;
	var height_900 = 900;
	var height_950 = 950;
	var height_1000 = 1000;
	var height_1050 = 1050;
	var height_1100 = 1100;
	var height_1150 = 1150;
	var height_1200 = 1200;
	var height_1250 = 1250;
	var height_1300 = 1300;
	var height_1350 = 1350;
	var height_1400 = 1400;
	var height_1450 = 1450;
	var height_1500 = 1500;
	
	
	/** Width Specifications*/

	var width_50 = 50;
	var width_100 = 100;
	var width_150 = 150;
	var width_200 = 200;
	var width_250 = 250;
	var width_300 = 300;
	var width_350 = 350;
	var width_400 = 400;
	var width_450 = 450;
	var width_460 = 460;
	var width_480 = 480;
	var width_500 = 500;
	var width_550 = 550;
	var width_580 = 580;
	var width_600 = 600;
	var width_650 = 650;
	var width_700 = 700;
	var width_750 = 750;
	var width_800 = 800;
	var width_850 = 850;
	var width_900 = 900;
	var width_950 = 950;
	var width_1000 = 1000;
	var width_1050 = 1050;
	var width_1100 = 1100;
	var width_1150 = 1150;
	var width_1200 = 1200;
	var width_1250 = 1250;
	var width_1300 = 1300;
	var width_1350 = 1350;
	var width_1400 = 1400;
	var width_1450 = 1450;
	var width_1500 = 1500;

