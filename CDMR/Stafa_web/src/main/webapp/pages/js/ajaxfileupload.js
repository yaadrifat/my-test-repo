// to be copy
/**file attachment start**/

// end of  new ajax copy 
jQuery.extend({
    

     createUploadIframe: function(id, uri){
             //create frame
             var frameId = 'jUploadFrame' + id;
             var iframeHtml = '<iframe id="' + frameId + '" name="' + frameId + '" style="position:absolute; top:-9999px; left:-9999px"';
             if(window.ActiveXObject)
             {
                 if(typeof uri== 'boolean'){
                     iframeHtml += ' src="' + 'javascript:false' + '"';

                 }
                 else if(typeof uri== 'string'){
                     iframeHtml += ' src="' + uri + '"';

                 }   
             }
             iframeHtml += ' />';
             jQuery(iframeHtml).appendTo(document.body);
				//alert("trace 1 createuploadframe ");
             return jQuery('#' + frameId).get(0);           
     },
     createUploadForm: function(id, fileElementId, data){
         //create form   
         var formId = 'jUploadForm' + id;
         var fileId = 'jUploadFile' + id;
         var form = jQuery('<form  action="" method="POST" name="' + formId + '" id="' + formId + '" enctype="multipart/form-data"></form>');   
         if(data){
             for(var i in data){
                 jQuery('<input type="hidden" name="' + i + '" value="' + data[i] + '" />').appendTo(form);
             }           
         }       
         var oldElement = jQuery('#' + fileElementId);
         var newElement = jQuery(oldElement).clone();
         jQuery(oldElement).attr('id', fileId);
         jQuery(oldElement).before(newElement);
         jQuery(oldElement).appendTo(form);


        
         //set attributes
         jQuery(form).css('position', 'absolute');
         jQuery(form).css('top', '-1200px');
         jQuery(form).css('left', '-1200px');
         jQuery(form).appendTo('body');   
        // alert("createuploadform");
         return form;
     },

     ajaxFileUpload: function(s) {
         // TODO introduce global settings, allowing the client to modify them for all requests, not only timeout
         //alert("ajaxfileupload");
         s = jQuery.extend({}, jQuery.ajaxSettings, s);
         var id = new Date().getTime();
         var rId = s.elementSeq;
         var form = jQuery.createUploadForm(id, s.fileElementId, (typeof(s.data)=='undefined'?false:s.data));
         var io = jQuery.createUploadIframe(id, s.secureuri);
         var frameId = 'jUploadFrame' + id;
         var formId = 'jUploadForm' + id;       
         // Watch for a new set of requests
         if ( s.global && ! jQuery.active++ )
         {
             jQuery.event.trigger( "ajaxStart" );
         }           
         var requestDone = false;
         // Create the request object
         var xml = {}  
         if ( s.global )
             jQuery.event.trigger("ajaxSend", [xml, s]);
         // Wait for a response to come back
         var uploadCallback = function(isTimeout)
         {           
             var io = document.getElementById(frameId);
             try
             {
              
                 //alert("io.contentWindow:::"+io.contentWindow+":::io.contentDocument::"+io.contentDocument);
                 if(io.contentWindow)
                 {
               	   //alert("ie " + io.contentWindow.document.body);
               	  //alert("inside io.contentWindow:::");
                      xml.responseText = io.contentWindow.document.body?io.contentWindow.document.body.innerHTML:null;
                      xml.responseXML= io.contentWindow.document.XMLDocument?io.contentWindow.document.XMLDocument:io.contentWindow.document;
                     // alert("xml.responseText:::"+xml.responseText);
                      //alert("xml.responseXML::"+xml.responseXML);
                      
                 }else if(io.contentDocument)
                 {
               	 // alert("other than ie");
               	 // alert("inside io.contentDocument:::");
                      xml.responseText = io.contentDocument.document.body?io.contentDocument.document.body.innerHTML:null;
                     xml.responseXML = io.contentDocument.document.XMLDocument?io.contentDocument.document.XMLDocument:io.contentDocument.document;
                    // alert("xml.responseText:::"+xml.responseText);
                    // alert("xml.responseXML::"+xml.responseXML);
                 }
            
                 //xml.responseText = $("#"+frameId).html();
             }catch(e)
             {
                 //alert("error in ajaxfileupload  " + e);
                 jQuery.handleError(s, xml, null, e);
             }
          //   alert("xml responseText " + xml.responseText);
           //  alert(" xml.responseXML " +  xml.responseXML);
             if ( xml || isTimeout == "timeout")
             {               
                 requestDone = true;
                 var status;
                 try {
                     status = isTimeout != "timeout" ? "success" : "error";
                     // Make sure that the request was successful or notmodified
                     if ( status != "error" )
                     {
                         // process the data (runs the xml through httpData regardless of callback)
                        // alert("XML:" + xml + " / " + s.dataType);
                         var data = jQuery.uploadHttpData(xml, s.dataType,s.elementSeq,id);   
                         // If a local callback was specified, fire it and pass it the data
                         if ( s.success )
                             s.success( data, status );
    
                         // Fire the global callback
                         if( s.global )
                             jQuery.event.trigger( "ajaxSuccess", [xml, s] );
                     } else
                         jQuery.handleError(s, xml, status);
                 } catch(e)
                 {
                     //alert("error in ajaxfileupload 2" + e);
                     status = "error";
                     jQuery.handleError(s, xml, status, e);
                 }

                 // The request was completed
                 if( s.global )
                     jQuery.event.trigger( "ajaxComplete", [xml, s] );

                 // Handle the global AJAX counter
                 if ( s.global && ! --jQuery.active )
                     jQuery.event.trigger( "ajaxStop" );

                 // Process result
                 if ( s.complete )
                     s.complete(xml, status);

                 jQuery(io).unbind()

                 setTimeout(function()
                                     {    try
                                         {
                                             jQuery(io).remove();
                                             jQuery(form).remove();   
                                            
                                         } catch(e)
                                         {
                                             //alert("error in ajaxfileupload 3 " + e);
                                             jQuery.handleError(s, xml, null, e);
                                         }                                   

                                     }, 100)

                 xml = null

             }
         }
         // Timeout checker
         if ( s.timeout > 0 )
         {
             setTimeout(function(){
                 // Check to see if the request is still happening
                 if( !requestDone ) uploadCallback( "timeout" );
             }, s.timeout);
         }
         try
         {

             var form = jQuery('#' + formId);
             jQuery(form).attr('action', s.url);
             jQuery(form).attr('method', 'POST');
             jQuery(form).attr('target', frameId);
             jQuery(form).attr('accept-charset', "UTF-8");
             if(form.encoding)
             {
                 jQuery(form).attr('encoding', 'multipart/form-data');
             }
             else
             {   
                 jQuery(form).attr('enctype', 'multipart/form-data');           
             }

             jQuery(form).submit();

         } catch(e)
         {       
             //alert("error in ajaxfileupload 4 " + e);
             jQuery.handleError(s, xml, null, e);
         }
        
         jQuery('#' + frameId).load(uploadCallback    );
         return {abort: function () {}};   

     },

     uploadHttpData: function( r, type,rowId,id) {
    	 if ($.browser.msie) {
	            var response = jsonDataCall("fetchTempFilePath","");
	            var fileData=response.returnMessage;
	            var fileUploadContentType = response.uploadContentType;
	            fileData=fileData.replace(/\\/g,"/");
	            //alert("fileData :"+fileData);
	            //alert("rowId :"+rowId);
	            //alert("uploadContentType :"+fileUploadContentType);
	            $("#filePathId"+rowId).val(fileData);
	            $("#fileUploadContentType"+rowId).val(fileUploadContentType);
	            $('#fileUploadImage'+rowId).css( 'display', 'none' );
	            //alert("==>"+$("#attach").val());
	            //alert("==>"+$("#jUploadFile" + id).val());
				var fileName = $("#jUploadFile" + id).val();
				var index = fileName.lastIndexOf("\\");
				if(index != -1){
				 fileName = fileName.substring(index+1);
				}
	           $('#fileUploadImage'+rowId).css( 'display', 'none' );
            	$("#fileDisplayName"+rowId).html(fileName);            
            	$("#fileName"+rowId).val(fileName);
          }
         var data = !type;
         data = type == "xml" || data ? r.responseXML : r.responseText;
       /*
         var dataparsed = r.responseText.split("{"); //added by Jude
       dataparsed = dataparsed[1].split("}"); //added by Jude

        data = type == "xml" || "{ " + dataparsed[0] + " }"; //added by Jude & BB
        
   //    alert(type + " data " + data);
         // If the type is "script", eval it in global context*/
         if ( type == "script" )
             jQuery.globalEval( data );
         // Get the JavaScript object, if JSON is used.
         if ( type == "json" )
             eval( "data = " + data );
         // evaluate scripts within html
         if ( type == "html" )
             $("<div>").html(data);

         return data;
     }
 });


// end of ajax copy
 function ajaxFileUpload1(fileOb,rowId){
     //starting setting some animation when the ajax starts and completes
	 var fileId = $(fileOb).attr("id");
	 var fileSize = GetFileSize(fileId)
	 if(fileSize>2){
		 alert("Upload file size limit is 2 MB only");
	 }else{
	     $(fileOb).hide();
	     $('#fileUploadImage'+rowId).css( 'display', 'block' );
	     $('#displayAttach'+rowId).css( 'display', 'none' );     
	     //$("#fileName1").html($(fileOb).val());
	     //alert("trace 1 -- ajaxfileupload1");
	     var fileId = $(fileOb).attr("id");
	     //alert(fileId);
	     //alert("rowId : "+rowId);
	     var uploadUrl = "inlineAddDocuments.action";
	     if ($.browser.msie) {
	    	 uploadUrl = "inlineAddDocumentsIE.action";
	     }
	     //alert("uploadUrl : "+uploadUrl);
	     $.ajaxFileUpload({
	             url:uploadUrl,
	             secureuri:false,
	             fileElementId:fileId,
	             elementSeq:rowId,
	             dataType: 'html',
	             //dataType: 'json',
	             success: function (data, status){
	                 //alert("success ");
	                 //alert("data "+data);
	            	 if (!($.browser.msie)) {
		                 data = data.replace("<pre>","");
		                 data = data.replace("</pre>","");
		                 //alert(data);
		                 var obj = jQuery.parseJSON(data);
		                 //alert(obj);
		                 //alert("returnMessage:  "+obj.returnMessage);
		                 //alert("obj.fileUploadContentType:  "+obj.uploadContentType);
		                 $("#filePathId"+rowId).val(obj.returnMessage);
		                 $("#fileUploadContentType"+rowId).val(obj.uploadContentType);
	            	 }
	
	             },
	             error: function (data, status, e){
	                 //alert("error ");
	                 //alert(data + " / " + status);
	                 $('#fileUploadImage'+rowId).css( 'display', 'none' );
	                 //$("#fileName1").html($(fileOb).val());
	                 alert("Error : "+e);
	             },
	             complete: function(data){
		            	 if (!($.browser.msie)) {
			            	 $('#fileUploadImage'+rowId).css( 'display', 'none' );
			                 $("#fileDisplayName"+rowId).html($(fileOb).val());            
			                 $("#fileName"+rowId).val($(fileOb).val());
		            	 }
	                 }
	         }
	     )
	   // alert("trace end")
	     return false;
	 }
 } 
