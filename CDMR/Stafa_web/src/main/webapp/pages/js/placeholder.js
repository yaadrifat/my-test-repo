//alert("js");
var keyEntryFlag = false;
var flag = true; 
var start, end, interval;

function getBarcodeScanData(event,obj){
    var productSearch = $("#fake_conformProductSearch").val();
	var url = "scanBarcode";
	var result = jsonDataCall(url,"fake_productSearch="+productSearch);
	//alert(result);
	var returnList=result.returnList;
	//alert("-"+result.returnList[0]);
	var listLength=result.returnList.length;
	//alert("listLength : "+listLength);
	if(listLength>0){
    $("#fake_conformProductSearch").val(result.returnList[0]);
    $("#conformProductSearch").val(result.returnList[0]);
    //$("#productSearch").val("BMT453");
    //$("#fake_productSearch").val("BMT453");
    getScanData(event,obj);
	}
}

$('.barcodeSearch').keydown(function() {
    start = new Date();
});
$(".barcodeSearch").keyup(function(e) {
	//alert("key up ");
	if(e.keyCode!=13){
		var data=$("#conformProductSearch").val();
		var datalen=data.length;
		if(datalen < 2){
			keyEntryFlag = false;
		}
		end = new Date();
	    interval= end.getTime() - start.getTime(); // interval in milliseconds
	    if(!keyEntryFlag && interval >100){
	    	keyEntryFlag = true;
		}
	  //  alert("trace 0")
	}else{
		//alert("trace 1" + e.keyCode);
	}
	 if((e.keyCode==13)&& ($("#conformProductSearch").val()!="")){
	//	 alert("trace" + flag + " / " + keyEntryFlag);
       e.preventDefault();
       $("#fake_conformProductSearch").val($("#conformProductSearch").val());
	//alert("flag"+flag);
		//alert("keyEntryFlag"+keyEntryFlag);
        if(flag && keyEntryFlag){
        	$("#button_wrapper div").animate({"margin-left":"30%"}, "Medium");
        	var apntxt="<input  class='scanboxSearch barcodeSearch' type='text' name='productSearch' id='productSearch' autocomplete='off'  onkeyup='validatedScanData(event,this);'><input  class='scanboxSearch' type='password' name='f_productSearch' id='fake_productSearch' autocomplete='off' onkeyup='validatedScanData(event,this);'style='display:none;'>";
            $("#button_wrapper div").append(apntxt);
            $("#productSearch").focus();
            $("#productSearch").focusout(function(){
                var pwdtxt=$("#productSearch").val();
                $("#productSearch").hide();
                $("#fake_productSearch").show();
                $("#fake_productSearch").val(pwdtxt);
               
            });
            $("#fake_productSearch").focus(function(){
            	$("#fake_productSearch").hide();
                $("#productSearch").show().focus();
            });
           // keyEntryFlag = false;
            flag=false;
         }
        /*else{
        	 
        	 try{
        		 keyEntryFlag = false;
                 getScanData(e,this);
             }
             catch(err){
            	 alert("error"+err);
             }
         }
         */
    }
    
    
       
    
    });


(function ($) {
    $.fn.delayKeyup = function(callback, ms){
        var timer = 0;
        $(this).keyup(function(){                   
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        });
        return $(this);
    };
})(jQuery);


$('#conformProductSearch').delayKeyup(function(OBJ){ 
	//alert("trace" + keyEntryFlag);
	if(!keyEntryFlag){
		var data =$("#conformProductSearch").val();
		if(data.length>1){
			$("#fake_conformProductSearch").val(data);
			var keyEvent = jQuery.Event("keypress");
			keyEvent.keyCode=$.ui.keyCode.ENTER;
			//getScanData(keyEvent,this);
			getBarcodeScanData(keyEvent,this);
		}
	}
	
}, 2000);







$("#conformProductSearch").blur(function(){
    $("#fake_productSearch").hide();
    $("#productSearch").show().focus();
   
    //$("#fake_productSearch").val(pwdtxt);
});
$("#fake_conformProductSearch").focus(function(){
    $("#fake_conformProductSearch").hide();
    $("#conformProductSearch").show().focus();
});

$("#conformProductSearch").focusout(function(){
    var pwdtxt=$("#conformProductSearch").val();
    if(pwdtxt!=""){
    $("#conformProductSearch").hide();
    $("#fake_conformProductSearch").show();
    $("#fake_conformProductSearch").val(pwdtxt);
    }
    });

function validatedScanData(event,obj){
    var productSearch=$("#productSearch").val();
   
    if(event.keyCode==13 && productSearch!=""){
    	
        var conformProductSearch=$("#conformProductSearch").val();
        if((conformProductSearch!=productSearch)){
        	$("#conformProductSearch").val("");
        	$("#productSearch").remove();
        		keyEntryFlag = true;
        	$("#fake_conformProductSearch").hide();
        	$("#conformProductSearch").show();
        	$("#button_wrapper div").css("margin-left","40%");
        	alert("Product ID Mismatch");
        	flag=true;
            return false;
        }else{
            $("#productSearch").remove();
            $("#button_wrapper div").css("margin-left","40%");
            flag=true;
            try{
                getScanData(event,this);
                   keyEntryFlag = true;
            }catch(error){}
        }
    }
}
(function( $ ){

    //detect the browser, only change the placeholder when IE 8 appears
    if ($.browser.msie  && parseInt($.browser.version, 10) == 8) {
          // for each placeholder on the page
          $('input[placeholder]').each(function(){
            $(this).css('color', 'grey');
            var input = $(this); 
            
            if(input.attr('placeholder')=='Password'){
           return false;
            }
            if(input.attr('placeholder')=='e-Sign'){ 
           
            return false;
            }else {
            	
                   input.val('');
            	   input.val(input.attr('placeholder'));
            }
            
            $(input).focus(function(){
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                    $(this).css('color', 'grey');
                 }
            });
            $(input).keypress(function () {
                $(this).css('color', 'black');
            });
            $(input).change(function () {
                $(this).css('color', 'black');
            });

            $(input).blur(function(){
                if (input.val() == '' || input.val() == input.attr('placeholder')) {
                    $(this).css('color', 'grey');
                    if(input.attr('placeholder')=='Password'){
                   return false;
                    }
                    if(input.attr('placeholder')=='e-Sign'){
                    return false;
                    }else{input.val("");
                    	   input.val(input.attr('placeholder'));
                    }
                }
            }).blur().parents('form').submit(function() {
                  $(this).find('[placeholder]').each(function() {
                        var input = $(this);
                        if (input.val() == input.attr('placeholder')) {
                          input.val('');
                        }
                      })
                    });
        });
    }
    })( jQuery );