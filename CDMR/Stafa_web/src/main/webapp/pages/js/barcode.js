
function getSetCollectionFacility(){
	var flag = false;

	var collectionFacility = "";
	var collectionFacilities = "";
	var facilityAddress = "";
	var collectionFda = "";
	
	if($("#processingFacilityName").is(":checked")){
		flag = true;
		collectionFacility = $("#collectionFacility").val();
		collectionFacilities = $("#collectionFacilities").val();
		facilityAddress = $("#facilityAddress").val();
		collectionFda = $("#collectionFda").val();
		
		
		$("#processingFacility").val(collectionFacility);
		$("#processingFacilities").val(collectionFacilities);
		$("#processingAddress").val(facilityAddress);
		$("#processingFda").val(collectionFda);
		}
	
	$("#processingFacility").attr("readonly",flag);
	$("#processingFacilities").attr("readonly",flag);
	$("#processingFda").attr("readonly",flag);
	
	
}


function printBarcode(){
	var eSignFlag = verifyeSignBarcode($(".ui-dialog #barcodeeSign"),'save');
	
}
function verifyeSignBarcode(eSignVal,mode){
		var eSignErroMsg = "Incorrect e-Signature. Please enter again."
		var eSignFlag=false;
		try{
			var eSign = $(".ui-dialog #barcodeeSign").val();
			if(eSign == "" || eSign == "e-Sign" ){
				alert(eSignErroMsg);
				return;
			}
			$(".ui-dialog #barcodeeSign").val("");
			var response = jsonDataCall("checkeSign","eSign="+eSign);
			if(response!=null){
				eSignFlag=response.eSignFlag;
				if(eSignFlag=='true'){
					if(mode=="popup" && savePopup()){
						// popup save
					}else if(mode=="save" && savePagesBarcode() ){
						// save
					}
				}else{
					alert(eSignErroMsg);
				}
				
			}else{
				alert(eSignErroMsg);
			}
			
		}catch(e){
			alert("Error : "+e);
		};
	}
function requireFields(lableTypeObj){
	var param ="";
	//alert($(lableTypeObj).find("option:selected").text())
	if($(lableTypeObj).find("option:selected").text()=="Full Label"){
		param ="{module:'APHERESIS',page:'FULL_LABEL'}";
	}else if($(lableTypeObj).find("option:selected").text()=="Collection Label"){
		param ="{module:'APHERESIS',page:'COLLECTION_LABEL'}";
		}
	$("#maindiv").find(".validate_required").each(function(){
		$(this).removeClass("validate_required");
	});
	$("sup").remove();
	$('.formError').remove();
	if(param!=""){
		markValidation(param);
	}
	
//	alert ("trace 1");
}

//Function name has been changed because when we open the Barcode Pop-up savePage() of the mainPage was overrode.
function savePagesBarcode(){
	//alert("save Pages");
	var productDin = $(".ui-dialog .datebox .day").html();
	var productDes = $("#mainfielddiv #productDescriptionSelect").val();
	var productStatus = "Preparation";
	var typeOfLabel = $("#barcodePrinting #barcodemodel").val();
	//var printDate = converDate(new Date());
	var printDate=$.datepicker.formatDate('M dd, yy', new Date());
	
	$("#productDin").val($.trim(productDin));
	$("#productDes").val(productDes);
	$("#productStatus").val(productStatus);
	$("#typeOfLabel").val(typeOfLabel);
	$("#printDate").val(printDate);
	//$("#productStatus").val("Preparation");
	 
	//var jsonData = "{barcodeLabel:'"+barcodeLbl+"',productDin:'"+productDin+"',productDes:'"+productDes+"',productStatus:'"+productStatus+"',typeOfLabel:'"+typeOfLabel+"'}";
	//url="savePrintedBarCode";
	//alert("before call");
	 $.ajax({
	        type: "POST",
	        url: "savePrintedBarCode.action",
	        async:false,
	        data:$("#tempBarcodeForm").serialize(),
	        success: function (result){
	        	//alert(" success" + result);
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	        }

		});
	 	//response = jsonDataCall(url,"jsonData="+jsonData);
	 	//alert("eSignFlag : "+eSignFlag);
	 
		 $("#printBarcodeDiv").html("");
		 $("#printBarcodeDiv").append($(".ui-dialog").find("#barcodeTem").clone());
		$('#printBarcodeDiv').printElement({printMode: 'popup'});
		//$('#printBarcodeDiv').printElement();
}

/*//alert("ones");
$("#productDescriptionInput").autocomplete({
    selectFirst: true,
    source: function(request, response) {
        var json;
        $.ajax({
            type:"POST",
            url: "loadProductDesc.action",
            dataType: "json",
            data: {productDesc:$('#productDescriptionInput').val(), maxRows: 10, values:request.term},
            success: function(result) {
                //alert("Success1 "+result.productDescriptionCodeList.length);
                var i=0;
                if(!result.productDescriptionCodeList.length){
                    result=[NoResultsLabel];
                    response(result);
                }else{
                	//alert("Success Else");
                    response( $.map( result.productDescriptionCodeList , function( item ) {
                         json=result.productDescriptionCodeList[i];
                         //alert("josn : "+json.productDescription);
                         
                         i++;
                        return {
                            label: json.productDescription,
                            value: json.productDescripCode,
                            id: json.productDescId
                        }
                    }));
                }
            }
        });
    },
    focus: function( event, ui ) { event.preventDefault(); 
    },
    select: function( event, ui ) {
    		//alert("select ");
            if (ui.item.label === NoResultsLabel) {
            	//alert("select If");
                event.preventDefault();
            }else{
                //alert("select Else");
                productDescCode=ui.item.id;
                productDescVal=ui.item.value;
                $("input[id=protocolId]").val(studyNumber);
                $("input[id=studyNumber]").val(studyNumber);
                $("input[id=protocolIdHidden]").val(protId);
                selectProtocol(protId);
            }
    },
    error: function(xmlhttp, status, error){
		 alert(error);
	 	return false; 
		}
}); 
*/
function getProductValue(productDiscCode){
	//alert($(productDiscCode).val());
	var productCode = $(productDiscCode).val();
	//productCode = "Cryopreserved HPC, APHERESIS|Citrate/XX/<=-120C|buffy coat enriched|10% DMSO|3rd Party Comp:Yes|Other Additives:Yes ";
 	//productCode = " Cryopreserved HPC, APHERESIS|Citrate+Heparin/XX/<=-120C|10% DMSO|Other Additives:Yes";
 	 //				  Cryopreserved HPC, CORD BLOOD|CPDA-1+10% DMSO+0.8% HES+1% dextran/XX/<-120C   
	if(productCode.indexOf('3rd Party Comp:Yes') > -1){
		$("#thirdPartyComponent").trigger('click');
		$("#thirdPartyComponent").attr("checked",true);
	}
	if(productCode.indexOf('Other Additives:Yes') > -1){
		$("#otherAdditives").trigger('click');
		$("#otherAdditives").attr("checked",true);
	} 

	var productDiscCode1 = productCode.split('|');
	//alert("productDiscCode1 :  "+productDiscCode1);
	//alert("Len "+productDiscCode1.length);
	if(productDiscCode1.length>0 && productDiscCode1[1]!=undefined){
		//alert("1->"+productDiscCode1[1]);
		var temp = productDiscCode1[1].split('/');
		if(temp.length>0 && temp[2]!="undefined"){
		//alert(temp[2]);
			var position = 1;
			var a = temp[2].substr(1, temp[2].length);
			var b = "";
			var storageTemprature = a.substr(0, position) + " " + b + a.substr(position);
			//alert("["+output+"]")
			$("#storageTemprature").val(storageTemprature);
			$.uniform.update('#storageTemprature');
		}
		if(temp.length>0 && $.trim(temp[0])!="undefined"){
			//alert("["+temp[0]+"]");
			$("#anticoagulantType").val(temp[0]);
			$.uniform.update('#anticoagulantType');
		}
	}
	//alert(productDiscCode1[0]);
	////alert(productDiscCode1[1]);
	//alert(productDiscCode1[2]);
	//alert(productDiscCode1[3]);
	if(productDiscCode1.length>0 && $.trim(productDiscCode1[2])!="undefined"){
		//alert("3----->|"+productDiscCode1[2]+"|");
		$("#cryoprotectant").val($.trim(productDiscCode1[2]));
		$.uniform.update('#cryoprotectant');
	}

	if(productDiscCode1.length>0 && $.trim(productDiscCode1[0])!="undefined"){
		//alert("0->"+productDiscCode1[0]);
		//$("#cryoprotectant").val(productDiscCode1[0]);
		//var classMod = productDiscCode1[0].split(", ");
		//str.substr(0,str.indexOf(' ')); // "72"
		//alert("|"+productDiscCode1[0]+"|");
		var classModifier = $.trim(productDiscCode1[0]);
		//alert("ind : "+classModifier.indexOf(' '))
		//alert("==>"+classModifier.substr(0,classModifier.indexOf(' '))); 
		//alert("-->"+classModifier.substr(classModifier.indexOf(' ')+1));
		var barClass=classModifier.substr(classModifier.indexOf(' ')+1);
		var modifi=classModifier.substr(0,classModifier.indexOf(' '));
		$("#modifier").val(classModifier.substr(0,classModifier.indexOf(' ')));
		//alert("|"+barClass+"|");
		$("#barcodeClass").val(barClass);
		$.uniform.update('#modifier');
		$.uniform.update('#barcodeClass');
	}
	
	/*$.uniform.restore('.ui-dialog select');
	$('.ui-dialog select').uniform();*/
	HideDropDowns();
}

function HideDropDowns(){
	$("#uniform-productDescriptionSelect").hide();
	$("#uniform-collectionFacilities").hide();
	$("#uniform-processingFacilities").hide();
	$("#uniform-donorRelationText").hide();
	$("#uniform-storageTempratureText").hide();
}

function getAddress(facilityId,addressId){
	//alert("s");
	//alert("Val : "+$("#collectionFacilities").val());
	
	var collectionFacility = $("#"+facilityId).val();
	var facilityAddress="";
	if(collectionFacility!=null && collectionFacility!=""){
		var collectionFacility = collectionFacility.replace('null', '');
		var collecFacility = collectionFacility.split("~-~");
		//alert("Len "+collecFacility.length);
		
		if(collecFacility.length>0 && collecFacility[1]!="undefined"){
			facilityAddress=collecFacility[1]+","+collecFacility[2]+","+collecFacility[3]+","+collecFacility[4]+".";
		}
		if(facilityAddress!=""){
			$("#"+addressId).html(facilityAddress);
		}

	}else{
		$("#"+addressId).html(facilityAddress);
	}
	getSetCollectionFacility();
}
function loadBarcodeInfo(proid,productRecipient,productDonor,specimenId){

	requireFields($("#barcodemodel"));
	
	//Auto Populate Barcode Info
	var url = "autoPopulateBarcodeInfo";
	//alert(proid);
	result = jsonDataCall(url,"productId="+proid+"&productRecipient="+productRecipient+"&productDonor="+productDonor+"&specimenId="+specimenId);
	
	var listLength=result.recipientDonorList.length;
	//alert(listLength);
	$.each(result.recipientDonorList, function(i,v) {
		//alert(v[0]);
		$(".ui-dialog #barcodeRecipientName").val(v[0]);
		//alert(v[0]);
		//$("#recipientId").html(v[1]);
		//$("#recipientDiagnosis").html(v[2]);
		//$("#recipientAboRh").html(v[3]);
		$(".ui-dialog #barcodeDonorName").val(v[4]);
		//$(".ui-dialog #barcodeDonorNo").val(v[5]);
		//$("#donorAboRh").html(v[6]);
		//$("#productId").html(v[7]);
		//$("#productType").html(v[9]);
		$(".ui-dialog #barcodeDonorDOB").val(v[12]);
		$(".ui-dialog #recipient").val(v[13]);
		$(".ui-dialog #donor").val(v[14]);
		$(".ui-dialog #barcodeRecipientMRN").val(v[15]);
		$(".ui-dialog #barcodeDonorNo").val(v[16]);
		$(".ui-dialog #barcodeRecipientDOB").val(v[17]);
	});
	$(".ui-dialog #sequenceId").val(result.sequenceId);
	$(".ui-dialog #sequenceNumber").val(result.sequenceId);
	$(".ui-dialog #specimenId").val(specimenId);
}



$(document).ready(function() {
		//readRequiredFields();
	$("#processingAddress").attr("readonly",true);
	$("#facilityAddress").attr("readonly",true);
		
	
		$(".ui-dialog #barcodeForm").validate({
		});
		//constructTableWithCriteria(true,barEntity,'archiveLabels');
		/*$("#productDescriptionSelect").change(function(){
			alert("product value "+$(this)val());
		});*/	
		/*$('.ui-dialog #productDescriptionSelect').val().change(function() {
			alert('Handler for .change() called.');
			});*/

			var date = new Date();
            var currentMonth = date.getMonth();
            var currentDate = date.getDate();
            var currentYear = date.getFullYear();

			
	  $( "#personDob" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	        changeYear: true, 
	        yearRange: "1900:"+currentYear,
	        maxDate: new Date(currentYear, currentMonth, currentDate)
		        });
	  		
	    $( "#accessionDate" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	        changeYear: true, yearRange: "1900:"+currentYear,maxDate: new Date(currentYear, currentMonth, currentDate)});
	    
	    $( "#collectionDateID" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	         changeYear: true, yearRange: "1900:"+currentYear});
        
	    $( "#productionDateID" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	        changeYear: true , yearRange: "1900:"+currentYear});
	    $( "#expirationDateID" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	        changeYear: true, yearRange: "1900:"+currentYear}); 
	    $( "#barcodeRecipientDOB" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	        changeYear: true, yearRange: "1900:"+currentYear,
	        maxDate: new Date(currentYear, currentMonth, currentDate)
        });
	    $( "#barcodeDonorDOB" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	        changeYear: true, yearRange: "1900:"+currentYear,maxDate: new Date(currentYear, currentMonth, currentDate)});
	   
	    $('#collectionTime').timepicker({});
	    $('#productionTime').timepicker({});
	    $('#expirationTime').timepicker({});
	    //for Autocomplete Product Description Code
	    /*var inputBoxName = "productDescriptionInput";
	    var selectBoxName = "productDescriptionSelect";*/
	    //autoCompleteCombo(inputBoxName,selectBoxName);

	  //for Autocomplete processingFacility
	    /*var inputBoxName = "processingFacility";
	    var selectBoxName = "processingFacilities";
	    autoCompleteCombo(inputBoxName,selectBoxName);

	  //for Autocomplete Collection Facility
	    var inputBoxName = "collectionFacility";
	    var selectBoxName = "collectionFacilities";
	    autoCompleteCombo(inputBoxName,selectBoxName);
*/	    
	    //$('#dinYear').spinit(13);
	    
	    var cYear = (new Date).getFullYear()+"";
	    var yr = cYear.substring(2,4);
	    //alert(yr); 
	    $('#dinYear').spinit({min:0,max:100,initValue:yr,height: 22 });
});

function mandatoryXmlParser(xml,lableTypeObj){
	try{
	//alert("==>"+xml);
	//($(xml).length);
	$(xml).find("page").each(function (i,el1) {
		//alert(i+"=="+$(this).find("code").text());
		if($(this).find("code").text()=="barcode"){
			//alert("2");
			//alert("=>"+ $(this).find("fileds").attr("id"));
			//alert($(lableTypeObj).val());
			$("input").nextAll('label').remove();
			$("select").nextAll('label').remove();
			
				$(el1).find("fileds").each(function (j,el2) {
					//alert("s"+j+"=="+j);
					//alert("==>In : "+$(el1).find("fileds").attr("id"));
						//("s2");
						if($(this).attr("id")=="fullLabel" && $(lableTypeObj).find("option:selected").text()=="Full Label"){
							//$('#barcodeForm').removeData('validator')
							$(el2).find("field").each(function(k,el3){
								var id = $(el3).attr("id");
								//alert("id : "+id)
								//id="collectionVolume";
								if($(el3).find("rule").text()==true || $(el3).find("rule").text()=='true'){
									$("#"+id).rules("add", {
										 required: true,
										 messages: {
										   required: "*",
										 }
									});
									}
									if($(el3).find("mandatory").text()==true || $(el3).find("mandatory").text()=='true'){
										//alert("mandatory")
											//$("#"+id).after("<label class='requiredField'>*</label>");
										$("#"+id).nextAll('label').remove();
										//$("#"+id).after("<label class='requiredField'>*</label>");
										$("#"+id).after("<label>*</label>");
									}
									
							});
						}else if($(this).attr("id")=="collectionLabel" && $(lableTypeObj).find("option:selected").text()=="Collection Label"){
							//$('#barcodeForm').removeData('validator')
							$(el2).find("field").each(function(k,el3){
								var id = $(el3).attr("id");
								//alert("id : "+id)
								//id="collectionVolume";
								
								if($(el3).find("rule").text()==true || $(el3).find("rule").text()=='true'){
									//alert("Rules : "+$(el3).find("rule").text());
									//alert("Id : "+id);
									$("#"+id).rules("add", {
										 required: true,
										 messages: {
										   required: "*",
									 	}
									});
								}

								if($(el3).find("mandatory").text()==true || $(el3).find("mandatory").text()=='true'){
									//alert("mandatory")
									    //$("input").after("<span>*</span>");
										$("#"+id).nextAll('label').remove();
										//$("#"+id).after("<label class='requiredField'>*</label>");
										$("#"+id).after("<label>*</label>");
								}
						});
							
					}
						
				
				});
		}
		
	});
	
	}catch(er){
		alert(er);
	}
	$("#barcodeForm").valid();
	//return true;
}
function readRequiredFields(lableTypeObj){
	removeValidation("barcodeForm");
	
	$.ajax({
        type: "GET",
        url: "xml/mandatory.xml",
        dataType: "xml",
        success: function(xml){
        	mandatoryXmlParser(xml,lableTypeObj);
        },
		complete: function( xhr, status )
		{
		  //alert( "COMPLETE.  You got:\n\n" + xhr.responseText ) ;
		  //alert("status"+status);
		  if( status == 'parsererror' )
		  {
			//alert( "There was a PARSERERROR.  Luckily, we know how to fix that.\n\n" +
			//   "The complete server response text was " + xhr.responseText ) ;

			xmlDoc = null;

			// Create the xml document from the responseText string.
			// This uses the w3schools method.
			// see also
			if (document.implementation.createDocument)
			{// code for Firefox, Mozilla, Opera, etc.
				//alert("firefox");
				var xmlhttp = new window.XMLHttpRequest();
				xmlhttp.open("GET", "aithmenu.xml", false);
				xmlhttp.send(null);
				xmlDoc = xmlhttp.responseXML.documentElement;
			}			
			else  if (window.ActiveXObject) // Internet Explorer
			{
			  xmlDoc=new ActiveXObject( "Microsoft.XMLDOM" ) ;
			  xmlDoc.async = "false" ;
			  xmlDoc.loadXML( xhr.responseText ) ;
			}  else if( window.DOMParser )
			{
			  parser=new DOMParser();
			  xmlDoc=parser.parseFromString( xhr.responseText,"text/xml" ) ;
			}
			else
			{
				alert('Your browser cannot handle this script');
			} 
			$( '# ' ).append( '<p>complete event/xmlDoc: ' + xmlDoc + '</p>' ) ;
			$( '#response' ).append( '<p>complete event/status: ' + status + '</p>' ) ;
			//alert(xmlDoc);
			//alert("---"+$(lableTypeObj).val());
			mandatoryXmlParser(xmlDoc,lableTypeObj) ;
		  }
		},

		error: function(xmlhttp, status, error){
				 //alert(error);
			 	return false; 
				}
    });
    return true;
}
function applytoall(){
	 
	var results=JSON.stringify(serializeFormObject($('#barcodeForm')));
	  $('#barcodetabs  ul li').each(function(i,v){
		  var allid=$(this).find('input').attr('id');
		  $("#"+allid).val(results);
	  });
	alert("Information applied to all Products");	
}

var prod_flag=false,init_data="";
var pro_array=new Array();
var activeTabJsonId="";
var activeTabId="";

function addTab(){
		var proid;
		var specimenId="";
		var productDonor="";
		var produtRecipient="";
		var plasmaId="";
		var plasmaName="";
		var k=0;
		var specId="";
		$('#productSearchTable tbody tr').each(function(i,v){
			
			var cbox=$(v).children("td:eq(0)").find(':checkbox').is(":checked");
			if(cbox){
					
				//proid=$(v).children("td:eq(4)").text();
				specimenId = $(v).children("td:eq(0)").find('#specimenId').val();
				productDonor = $(v).children("td:eq(0)").find('#productDonor').val();
				productRecipient = $(v).children("td:eq(0)").find('#pkRecipient').val();
				plasmaId = $(v).children("td:eq(0)").find('#plasmaId').val();
				plasmaName = $(v).children("td:eq(0)").find('#plasmaName').val();
				
				if(i==0){
					specId=specimenId;
					}
				proid=$(v).find("#hiddenpersonprId").val();
			 	if(prod_flag==false){	
				var results=JSON.stringify(serializeFormObject($('#barcodeForm')));
				init_data=results;
				if(k==0){
					activeTabJsonId = "prod_"+proid;
					activeTabId=proid;
					k++;
				}
				
				$("#barcodetabs").find("ul").append("<li id='li_"+proid+"'><input type='hidden' id='prod_"+proid+"'   value='"+results+"'><a href='#secondarydiv' id="+proid+"  onclick=loadjsons('"+proid+"','prod_"+proid+"','li_"+proid+"','"+specimenId+"','"+productRecipient+"','"+productDonor+"')>"+proid+"</a></li>");
				if(plasmaId!=null && plasmaId!='null' && plasmaName!=null && plasmaName!='null'){
					$("#barcodetabs").find("ul").append("<li id='li_"+plasmaName+"'><input type='hidden' id='prod_"+plasmaName+"'   value='"+results+"'><a href='#secondarydiv' id="+plasmaName+"  onclick=loadjsons('"+proid+"','prod_"+plasmaName+"','li_"+plasmaName+"','"+specimenId+"','"+productRecipient+"','"+productDonor+"')>"+plasmaName+"</a></li>");
				}
				
				//$("#barcodetabs").find("ul").append("<li id='li_"+proid+"'><input type='text' id='prod_"+proid+"'   value='"+results+"'><a href='#secondarydiv' id="+proid+"  onclick=loadjsons('"+proid+"','prod_"+proid+"','li_"+proid+"','"+specimenId+"','"+productRecipient+"','"+productDonor+"')>"+proid+"</a></li>");
			   pro_array[i]=proid;
				
			 }
			 else{
				 if(pro_array[i]!=proid && typeof(pro_array)!='undefined'){
					 $("#barcodetabs").find("ul").append("<li id='li_"+proid+"'><input type='hidden' id='prod_"+proid+"' value='"+init_data+"'><a href='#secondarydiv' id="+proid+"  onclick=loadjsons("+proid+",'prod_"+proid+"','li_"+proid+"','"+specimenId+"','"+productRecipient+"','"+productDonor+"') >"+proid+"</a></li>");
					 //$("#barcodetabs").find("ul").append("<li id='li_"+proid+"'><input type='text' id='prod_"+proid+"'   value='"+init_data+"'><a href='#secondarydiv' id="+proid+"  onclick=loadjsons("+proid+",'prod_"+proid+"','li_"+proid+"','"+specimenId+"','"+productRecipient+"','"+productDonor+"')>"+proid+"</a></li>");
					  $("#barcodetabs").tabs();
					// $("#li_"+proid).bind('click').addClass('ui-tabs-selected ui-state-active');
					 pro_array[i]=proid;
				 }
			 }
			}
			
		});  
			var cbox = false;
			$("#barcodetabs").css('display','none');
			
			$('#productSearchTable tbody tr').each(function(i,v){
			cbox=$(v).children("td:eq(0)").find(':checkbox').is(":checked");
			//alert
			if(cbox){
				
			    $("#protoggle").toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
			   // $("#protoggle").parents( ".portlet" ).find( ".portlet-content" ).toggle();
			    $("#protoggle").parents( ".portlet:first" ).find( "#proFind" ).toggle();
			   /*  $("#protoggle").find("span:first").find("ui-icon ui-icon-minusthick").trigger("click");*/
				$("#protoggle").find("span:first").removeClass("ui-icon ui-icon-minusthick").addClass("ui-icon ui-icon-plusthick");
				$("#barcodetabs").show();
			 	//$("#barcodetabs div:first").show();
			 	//$("#barcodetabs div:first").trigger("click");
			 //	$(".ui-dialog #pkSpecimen").val(specId);
			 	loadBarcodeInfo(activeTabId);
				return false;
			}
		});
		    if(cbox){
			$("#barcodetabs").tabs();
		 	$("#"+activeTabId).trigger("click");
			
			//alert("HideDropDowns()");
			HideDropDowns();
			prod_flag=true ;
		    }

}  
var reresult="";
function loadjsons(proid,hidenValueId,lititle,specimenId,productRecipient,productDonor){
	//alert("proid : "+proid+" / hidenValueId /"+hidenValueId+"/lititle : "+lititle);
	//loadjsons('0628A','prod_0628A','li_0628A','5681','5987','5986')
	//proid = 
	var reresult=JSON.stringify(serializeFormObject($('#barcodeForm')));

    $("#barcodeRecipient").val(productRecipient);
    $("#barcodeDonor").val(productDonor);
    $("#barcodeEntity").val(specimenId);
    
	var data1=$("#"+activeTabJsonId).val(reresult);
	//to Maintain active tab and previous tab
	activeTabJsonId = hidenValueId;
	activeTabId=proid;
	var data="";
	
	data=$("#"+hidenValueId).val();
	data = '{"pkSpecimen":"5681","productStatus":"Accession","aboSelectedText":"Bombay, RhD positive","donorRelationDisplayText":"","barcodemodel":"2","specimenId":"5681","barcodeClass":"HPC, APHERESIS","modifier":"Cryopreserved","manipulation":"B-cell reduced","cryoprotectant":"10% DMSO","collectionVolume":"777","productDescriptionInput":"Cryopreserved HPC, APHERESIS|Citrate+Heparin/XX/<=-120C|10% DMSO|Other Additives:Yes","productDescriptionSelect":"S1583","divisionCode":"00","anticoagulantType":"Citrate+Heparin","storageTemprature":"= -120C","storageTempratureText":"Store at -120 C or colder","intendedUse":"724","donationType":"4","aborh":"596","barcodeRecipientName":"MARK","recipient":"5987","barcodeRecipientDOB":"Jun 13, 1980","barcodeRecipientMRN":"DONOR155","barcodeDonorName":"MARK","donor":"5986","barcodeDonorDOB":"Jun 13, 1980","barcodeDonorNo":"DONOR155","donorRelation":"autologous","donorRelationText":"Donor/Recipient","__checkbox_thirdPartyComponent":"true","otherAdditives":"true","__checkbox_otherAdditives":"true","__checkbox_geneticallyModified":"true","__checkbox_biohazard":"true","__checkbox_autologousUse":"true","__checkbox_intendedRecipient":"true","__checkbox_doNotIrradiate":"true","__checkbox_doNotUseLeukocyteReduction":"true","__checkbox_attachedDocsDetails":"true","collectionFacility":"Oddzial Terenowy RCKiK Nv 56","collectionFacilities":"Z5321~-~Ciechanow~-~null~-~Poland~-~06-400","dinYear":"13","sequenceId":"12732","flagCharacter":"00","facilityAddress":"Ciechanow,,Poland,06-400.","collectionFda":"555555","processingFacilityName":"","processingFacility":"Oddzial Terenowy RCKiK Nv 56","processingFacilities":"Z5321~-~Ciechanow~-~null~-~Poland~-~06-400","processingAddress":"Ciechanow,,Poland,06-400.","processingFda":"555555","collectionDate":"","collectionTime":"","productionDate":"Oct 14, 2013","productionTime":"11:21","expirationDate":"Oct 08, 2013","expirationTime":"11:20"}';
	

	//alert(data);
	var result = jQuery.parseJSON(data);
	$('.ui-dialog #barcodeForm').loadJSON(result);
	$(".ui-dialog #pkSpecimen").val(specimenId)
	//alert($(".ui-dialog #pkSpecimen").val());
	//for Autocomplete Product Description Code
    var inputBoxName = "productDescriptionInput";
    var selectBoxName = "productDescriptionSelect";
    autoCompleteCombo(inputBoxName,selectBoxName);

  //for Autocomplete processingFacility
    var inputBoxName = "processingFacility";
    var selectBoxName = "processingFacilities";
    autoCompleteCombo(inputBoxName,selectBoxName);

  //for Autocomplete Collection Facility
    var inputBoxName = "collectionFacility";
    var selectBoxName = "collectionFacilities";
    autoCompleteCombo(inputBoxName,selectBoxName);
	
    loadBarcodeInfo(proid,productRecipient,productDonor,specimenId);
	
	$.uniform.restore('.ui-dialog select');
	$('.ui-dialog select').uniform();
	HideDropDowns();
	} 
var readFlag = false;
function fetchBarcodeTemplate(type){
	
		//readFlag = readRequiredFields($("#barcodemodel"));
		//readFlag = true;
		
		var divId = "maindiv";
		if(checkValidation(divId)){

			$("#aboSelectedText").val($(".ui-dialog #aboRh option:selected").text());
			//donorRelation,donorRelationText
			var donorRelation = $.trim($("#donorRelation").val());
			//alert("donorRelation : "+donorRelation);
		
			if(donorRelation!=""){
				$("#donorRelationText option:contains(" + donorRelation + ")").attr('selected', 'selected');
			}
			//alert("donorRelationText : "+$("#donorRelationText").val());
			
			var storageTemprature = $.trim($("#storageTemprature").val());
			
			if(storageTemprature!=""){
				$("#storageTempratureText option:contains(" + storageTemprature + ")").attr('selected', 'selected');
			}
			//alert("storageTemprature : "+$("#storageTempratureText").val());
			
			$("#intendedUseSelectedText").val($(".ui-dialog #intendedUse option:selected").text());
			
			$("#aboSelectedText").val($(".ui-dialog #aboRh option:selected").text());
	    	var url = "fetchBarcodeTemplate";
	    	var response = ajaxCall(url,"barcodeForm");
	   
	    		if(response.returnJsonStr!=null){
	    			if(type=="active"){
		    			//alert("active : "+active);
	    				//alert("html" + $("#barcodeTem").html());
	    				$("#barcodeTem").html(response.returnJsonStr);
	    				
	    			}else{
	    				//alert("rePrint  : ");
	    				$("#barcodeTem").append(response.returnJsonStr);
	    			}
	    			$("#productLabel").val(response.returnJsonStr);
	    			
	    			$("#barcodeTem").prepend('<div><input type="password" style="width:80px;" size="5" value="" id="barcodeeSign" name="barcodeeSign" placeholder="e-Sign"/><input type="button" class="cursor" id="printBut" value="Print" onclick="printBarcode();"/></div>');
	    			$("#modelPopup").html($("#barcodeTem").html());
	    			$("select").uniform();

	     			//alert("s22");
	     			
	     			/*$.ajax({
                            type: "POST",
                            data:response,
                            cache: false,
                            success: function (result, status, error){
                                     $("#modelPopup").html($("#barcodeTem").html());
                                     $("select").uniform();
                        },
                     error: function (request, status, error) {
                         alert("Error " + error);
                         alert(request.responseText);
                 }
                    });*/
	     			$("#modelPopup").dialog({
	        			width : 450,
	         			height : 480,
	         			autoOpen: false,
	        			modal: true,
	        			close: function() {
	         				jQuery("#modelPopup").dialog("destroy");
	        			}
	     			});
	     			
	    			$('#modelPopup').dialog('open');
	    			$("#modelPopup img").each(function(){
	    			       var timeStamp = (new Date()).getTime();
	    			       //alert("==>"+$(this).attr("src"));
	    			       if($(this).hasClass("ref")){
	    			       	$(this).attr("src", $(this).attr("src") +"&date="+ timeStamp);
	    			       }
	    			    });
    			    
	    			
	    			/*alert($('#dinBarcodeImg').attr("src"));
	    			alert($('#aborhImg').attr("src"));
	    			alert($('#productDescImg').attr("src"));
	    			alert($('#expireDateImg').attr("src"));
	    			alert($('#marrixImg').attr("src"));
					var ss = $('#expireDateImg').attr("src").replace("&","$");
	    			$('#dinBarcodeImg').attr("src",$('#dinBarcodeImg').attr("src"));
	    			$('#aborhImg').attr("src",$('#aborhImg').attr("src"));
	    			$('#productDescImg').attr("src",$('#productDescImg').attr("src"));
	    			$('#expireDateImg').attr("src",ss);*/
	    			//alert($('#marrixImg').attr("src");
	    			
	    			//$('#dinBarcodeImg').attr("src","")
	    			//alert("s22222--");
	    			//$('#modelPopup .ui-dialog-content').location.reload();
	    		}else{
	        	$("#barcodeTem").hide();
	    }
		//}
	}else{
		$("#protoggle").toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$("#protoggle").parents( ".portlet:first" ).find( "#proFind" ).toggle();
		$("#protoggle").find("span:first").removeClass("ui-icon ui-icon-minusthick").addClass("ui-icon ui-icon-plusthick");
		$("#archiveProductLabel").hide();
	}
		
}


