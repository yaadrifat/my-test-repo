var keyEntryFlag= false;
var flag = true; 
var start, end, interval;
$('.barcodeSearch').keydown(function() {
	//console.log("barcodeSearch");
	start = new Date();
});
$('.multiple').keydown(function() {
	//console.log("multiple ");
    start = new Date();
});
function performScanData(e,obj){
	if(e.keyCode!=13){
		var data=$(obj).val();
		var datalen=data.length;
	//	console.log("datalen "+datalen);
		if(datalen < 2){
			keyEntryFlag = false;
		}
		end = new Date();
	    interval= end.getTime() - start.getTime(); // interval in milliseconds
	//    console.log("keyEntryFlag "+keyEntryFlag);
	    if(!keyEntryFlag && interval >100){
	    	//console.log("Inside : interval----------> "+interval+", keyEntryFlag------> : "+keyEntryFlag );
	    	keyEntryFlag = true;
		}
	}else{
	}
	 if((e.keyCode==13)&& ($(obj).val()!="")){
		 // e.preventDefault();
		 e.returnValue = false;
       $(obj).find("#fake_conformProductSearch").val($(obj).val());
     //  alert("flag"+flag)
     //  alert("keyEntryFlag"+keyEntryFlag);
       if(flag && keyEntryFlag){
        	$(obj).parent().animate({"margin-left":"30%"}, "Medium");
        	var apntxt="<input  class='scanboxSearch barcodeSearch' type='text' name='productSearch' id='productSearch' autocomplete='off'  onkeyup='validatedScanData(event,this);'><input  class='scanboxSearch' type='password' name='f_productSearch' id='fake_productSearch' autocomplete='off' onkeyup='validatedScanData(event,this);'style='display:none;'>";
            $(obj).parent().append(apntxt);
            var pwdtxt=$(obj).val()
		    if(pwdtxt!=""){
		    	//alert("1");
		    	$(obj).hide();
		    $(obj).parent().find("#fake_conformProductSearch").show();
		    $(obj).parent().find("#fake_conformProductSearch").val(pwdtxt);
		    }
            $("#productSearch").focus();
            $(obj).parent().find("#productSearch").focusout(function(){
            	//alert("2");
            	if(flag){
            	var pwdtxt=$(obj).parent().find("#productSearch").val();
                $(obj).parent().find("#productSearch").hide();
                $(obj).parent().find("#fake_productSearch").show();
                $(obj).parent().find("#fake_productSearch").val(pwdtxt);
            	}
            });
            $(obj).parent().find("#fake_productSearch").focus(function(){
            	//alert("3");
            	$(obj).parent().find("#fake_productSearch").hide();
            	$(obj).parent().find("#productSearch").show().focus();
            });
            flag=false;
         }
        else{
        	 try{
        		//console.log(" before getScanData");
        		 keyEntryFlag = false;
        		 e.which = 13;
                 getScanData(e,this);
             }
             catch(err){
            	 alert("error"+err);
             }
         }
    }
/*	 $(obj).focusout(function(){
		 	
		    }
		    });*/
}
(function ($) {
	//console.log("0 timer : ");
    $.fn.delayKeyup = function(callback, ms){
    //	console.log("0.111 timer : "+ms);
        var timer = 0;
        $(this).keyup(function(){
        	//console.log("1 timer : " + timer);
            clearTimeout (timer);
            //console.log("2 timer : " + timer);
            timer = setTimeout(callback, ms);
            //console.log("after timer : " + timer);
        });
        return $(this);
    };
})(jQuery);
$('.barcodeSearch').delayKeyup(function(OBJ){ 
	
	//console.log("trace : " + keyEntryFlag);
	if(!keyEntryFlag){
		var data =$("#conformProductSearch").val();
		//console.log("data: " + data);
		if(data.length>1){
			$("#fake_conformProductSearch").val(data);
			var keyEvent = jQuery.Event("keyup");
			keyEvent.which = 13;
			//console.log("---Barcode Scanned---");
			getScanData(keyEvent,this);
		}
	}
	
}, 2000);

$('.recipientscan').delayKeyup(function(OBJ){ 
	
	if(!keyEntryFlag){
		var objid=$('.recipientscan').closest('div').prop("id");
		var data=$("#"+objid).first().find("input").val();
	//	console.log(data);
		if(data.length>1){
			$("#"+objid+" #fake_conformProductSearch").val(data);

			 
			var keyEvent = jQuery.Event("keyup");
			keyEvent.which = 13;
			//keyEvent.keyCode=$.ui.keyCode.ENTER;
			getScanData(keyEvent,this);
			
		}
	}
	
}, 2000);
$('.donorscan').delayKeyup(function(OBJ){ 
	if(!keyEntryFlag){
		var objid=$('.donorscan').closest('div').prop("id");
		var data=$("#"+objid).first().find("input").val();
		if(data.length>1){
			$("#divDonorScan #fake_conformProductSearch").val(data);
		//	console.log("data: in fkproduct" + $("#fake_conformProductSearch").val());
			/*var keyEvent = jQuery.Event("keypress");
			keyEvent.keyCode=$.ui.keyCode.ENTER;*/
			var keyEvent = jQuery.Event("keyup");
			keyEvent.which = 13;
			//console.log("---Barcode Scanned---");
			getScanData(keyEvent,this);
			
		}
	}
	
}, 2000);
$(".barcodeSearch").blur(function(){
	//alert(flag);
	if(flag){
    $(this).find("#fake_productSearch").hide();
    $("#productSearch").show().focus();
    }
});
function changeType(obj){
	$(obj).hide();
	$(obj).parent().find("#conformProductSearch").show();
}
function validatedScanData(event,obj){
    var productSearch=$(obj).val();
    if(event.keyCode==13 && productSearch!=""){
        var conformProductSearch=$(obj).parent().find("#conformProductSearch").val();
         if((conformProductSearch!=productSearch)){
        	 $(obj).parent().children(":first").val("");
         	$(obj).parent().children(":first").show();
         	 $(obj).parent().find("#fake_conformProductSearch").hide();
        	$("#productSearch").remove();
        		keyEntryFlag = true;
        		
        		$("#button_wrapper div").css("margin-left","40%");
        		alert("Product ID Mismatch");
        	flag=true;
            return false;
        }else{
            $("#productSearch").remove();
            $("#fake_productSearch").remove();
            $("#button_wrapper div").css("margin-left","40%");
            flag=true;
            try{
            	event.which=13;
                getScanData(event,this);
                   keyEntryFlag = true;
            }catch(error){}
        }
    }
}
