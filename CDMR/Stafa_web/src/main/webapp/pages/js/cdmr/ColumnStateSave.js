/* To save the Column Detail Status in database*/
function saveORUpdateColumnDetail(m,tableid){
	var tableobj=getModule(tableid);
	$.ajax({
		url:'saveColState.action',
		type:'post',
		 data: {
	            "colvalue":JSON.stringify(m),
	            "module":tableobj
	        }
	});	
}

/* To save the Column Order in database*/
function saveORUpdateColumnOrder(m,tableid){
	var tableobj=getModule(tableid);
	$.ajax({
		url:'saveColOrder.action',
		type:'post',
		 data: {
	            "colvalue":JSON.stringify(m),
	            "module":tableobj
	        }
	});	
}

/* All the tableID related to CDMR*/
var checkboxTab = ["maincdmData"]; 
var ubcheckboxTab = ["servLink","servicedata","grpLink","groupdata"];
var deletedTab = ["editGroupSectionTable","editServiceSetTable"];
var homeTab=["pendingDonorsTable"];
var stageTabTech=["pendingDonorsTableTech"];
var stageTabProf=["pendingDonorsTablepro"];
var cdmupdate=["pendingDonorsTableUpdate"];

/* Get the module name according to the tableID*/
function getModule(res){
	var dataTableObj;
	if( $.inArray(res, checkboxTab) > -1){
		dataTableObj="Charge Library";
	}
	else if($.inArray(res, ubcheckboxTab) > -1){
		dataTableObj="Charge Library Group";
	}
	else if($.inArray(res, deletedTab) > -1){
		dataTableObj="Charge Library Delete";
	}
	else if($.inArray(res, homeTab) > -1){
		dataTableObj="Home";
	}
	else if($.inArray(res, stageTabTech) > -1){
		dataTableObj="StagingT";
	}
	else if($.inArray(res, stageTabProf) > -1){
		dataTableObj="StagingP";
	}
	else if($.inArray(res, cdmupdate) > -1){
		dataTableObj="CDM Update";
	}
	return dataTableObj;
}
