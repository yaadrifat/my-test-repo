var selectedValues = [];
var savedfiltercolvals = [];
var fk = " ";
var pk = "";
var filtername_check;
var savebuttonclicked = 'false';
$.ajax( {
	url : "loadfilter.action",
	type : "post",
	data : null,
	success : function(response) {
		$("#savedfilter").html(response['filterNames']);
	}
});
// $(".datepicker_AS").attr('readonly', 'readonly');
$(".number_only").keypress(
		function(e) {
			if (e.which != 8 && e.which != 0 && e.which != 13
					&& (e.which < 46 || e.which > 57) || e.which == 47) {
				if ($(".number_only").val().length <= 1) {
					$(".number_only").val("");
				}
				return false;
			}
		});
$(".datepicker_AS").keypress(
		function(e) {
			if (e.which != 8 && e.which != 0 && e.which != 13
					&& (e.which < 46 || e.which > 57) || e.which == 47) {
				if ($(".datepicker_AS").val().length <= 1) {
					$(".datepicker_AS").val("");
				}
				return false;
			}
		});

$("#savedfilter").change(function() {
	$('#delfiltername').css('pointer-events', 'auto');// removeAttr('pointer-events');
		$('#editfilterset').css('pointer-events', 'auto')
	});


function showAddFilterform() {

	$.ajax({
		url:"getbannertext.action",
		type:"post",
		data:null,
		cache:false,
			success:function(response){
				
	      	  if(response.import_status=='1'&&response.export_status=='0'){
	      		$("#bannercontent").show();
	 				$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
				}
				else if(response.export_status=='1'&& response.import_status=='0'){
					$("#bannercontent").show();
					$("#bannercontent").html("Export Interface is in progress. System slowness may occur")
				}
				else if(response.import_status=='1' && response.export_status=='1'){
					$("#bannercontent").show();
					$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
					$("#bannercontent").append("<div style='background-color: yellow;'>Export Interface is in progress. System slowness may occur</div>");
				}
				else{
					$("#bannercontent").hide();
				}
	       }

	});
	var excludecols = [];
	$("#showfilters_area").empty();
	if ($('.listview li').length == 0 && $('.grpdata li').length == 0
			&& $('.setgrpdata li').length == 0) {
		$.ajax( {
			url : "getadvncesrchcols.action",
			type : "post",
			data : null,
			cache : false,
			success : function(response) {
				$("#searchfilter").html(response['advancesrchcols']);
				selectedValues.length = 0;
			}
		});
	} else {
		if ($('.listview li').length != 0) {
			$('.listview li').each(function() {
				excludecols.push($(this).attr('id'));
				$("#showfilters_area").html($('.listview').html());
			});
		}
		if ($('.grpdata li').length != 0) {
			$('.grpdata li').each(function() {
				excludecols.push($(this).attr('id'));
				$("#showfilters_area").html($('.grpdata').html());
			});
		}
		if ($('.setgrpdata li').length != 0) {
			$('.setgrpdata li').each(function() {
				excludecols.push($(this).attr('id'));
				$("#showfilters_area").html($('.setgrpdata').html());
			});
		}
		$.ajax( {
			url : "loadfiltersetcols.action",
			type : "post",
			dataType : "json",
			data : {
				"excludecols" : JSON.stringify(excludecols)
			},
			success : function(response) {
				$("#searchfilter").html(response['advancesrchcols']);
				excludecols.length = 0;
			}
		});
	}
	$("#advanceSearchnew").dialog( {
		title : '<label>Add Filters</label>',
		autoOpen : false,
		modal : true,
		draggable : true,
		resizable : false,
		dialogClass : 'advncesrchdialog',
		close : function() {
			$("#date_selectordialog").dialog("destroy");
			$("#charge_selector").dialog("destroy");
			$("#advanceSearchnew").dialog("destroy");
		}
	});
	$("#advanceSearchnew").dialog("open");
	$("#advanceSearchnew").css('overflow', 'hidden');
	$("#advanceSearchnew").resizable( {
		handles : 's,e'
	})
	return true;
}

function showSaveFilterpopup() {

	$.ajax({
		url:"getbannertext.action",
		type:"post",
		data:null,
		cache:false,
			success:function(response){
				
	      	  if(response.import_status=='1'&&response.export_status=='0'){
	      		$("#bannercontent").show();
	 				$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
				}
				else if(response.export_status=='1'&& response.import_status=='0'){
					$("#bannercontent").show();
					$("#bannercontent").html("Export Interface is in progress. System slowness may occur")
				}
				else if(response.import_status=='1' && response.export_status=='1'){
					$("#bannercontent").show();
					$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
					$("#bannercontent").append("<div style='background-color: yellow;'>Export Interface is in progress. System slowness may occur</div>");
				}
				else{
					$("#bannercontent").hide();
				}
	       }

	});
	$("#savedfilter").prop('selectedIndex', 0);
	savedfiltercolvals.length = 0;
	if ($('.listview li').length == 0 && $('.grpdata li').length == 0
			&& $('.setgrpdata li').length == 0) {
		selectedValues.length = 0;
	}
	$.ajax( {
		url : "loadfilter.action",
		type : "post",
		data : null,
		success : function(response) {
			$("#savedfilter").html(response['filterNames']);
		}
	});
	$("#advanceSearchsave").dialog( {
		title : '<label>Saved Filters</label>',
		autoOpen : false,
		modal : true,
		draggable : true,
		resizable : false,
		dialogClass : 'advncesrchdialogsave',
		close : function() {
			$("#advanceSearchsave").dialog("destroy");
		}
	});
	$("#advanceSearchsave").dialog("open");
	$("#advanceSearchsave").css('overflow', 'hidden');
	return true;
}
$("#searchfilter").change(function() {
	var type = $(this).find(':selected').attr('datatype');
	var optionvalue = $(this).val();
	$('#matchvalue').val("");
	$('#matchvalue').removeAttr('disabled');
	$("#matchvalue").removeAttr('onclick');
	if (type == "TIMESTAMP") {
		$("#matchvalue").attr('onclick', 'datepickermenu()');
	} else if (type == "NUMERIC") {
		$("#matchvalue").attr('onclick', 'chargeselectMenu()');
	}
});

$("#searchfiltersave").change(function() {
	var type = $(this).find(':selected').attr('datatype');
	var value = $(this).val();
	$('#enterval').val("");
	$('#enterval').removeAttr('disabled');
	$("#enterval").removeAttr('onclick');
	if (type == "TIMESTAMP") {
		$("#enterval").attr('onclick', 'datepickermenu()');
	} else if (type == "NUMERIC") {
		$("#enterval").attr('onclick', 'chargeselectMenu()');
	}
});

function ClicktoSaveandApply() {
	if ($("#showfilters_area li").length != 0) {
		$('#entertosave').val("");
		$("#show_initialy").hide();
		$("#showtosavefilter").show();
	} else {
		alertify.alert("Please select filter(s) to save.");
		return;
	}
}
function ClicktoCancel() {
	$("#show_initialy").show();
	$("#showtosavefilter").hide();
	$("#showfilters_area").empty();
	selectedValues.length = 0;
	$.ajax( {
		url : "getadvncesrchcols.action",
		type : "post",
		data : null,
		success : function(response) {
			$("#searchfilter").html(response['advancesrchcols']);
		}
	});
}

function SaveNamedFilter() {

	savebuttonclicked = 'true';
	var selectedvalsall = [];
	$("#show_initialy").show();
	$("#showtosavefilter").hide();
	var filterName = $('#entertosave').val();
	$("#showfilters_area li").each(function() {
		selectedvalsall.push($(this).text() + ":" + $(this).attr('id'));
	});
	if (filterName == "") {
		alertify.alert("Please enter filter set name.");
	} else {
		$
				.ajax( {
					url : "savefilter.action",
					async : false,
					type : "post",
					data : {
						filtrName : filterName,
						"FILE_ID" : $("#cdmtype").val()
					},
					success : function(response) {
						var fkid = response['fkid'];
						if (response.checkForExisting == false) {
							alertify
									.alert('Filter set "' + filterName + '" already exists.');
						} else {
							$('.progress-indicator').css('display', 'block');
							$.ajax( {
								url : "savedetails.action",
								type : "post",
								async : false,
								dataType : "json",
								data : {
									"col_vals" : JSON
											.stringify(selectedvalsall),
									"FKID" : fkid
								},
								success : function(response) {
									applyFilters();
									selectedvalsall.length = 0;
								}
							});

							$("#advanceSearchnew").dialog("destroy");
							alertify.alert("Created successfully.");
						}
					}
				});
	}
	filtername_check = "Create";
	$('.progress-indicator').css('display', 'none');
}

function datepickermenu() {
	$("#date_selectordialog").dialog( {
		title : '<label>Choose Date</label>',
		autoOpen : false,
		modal : true,
		dialogClass : 'specified_search_dialog',
		close : function() {
			$("#date_selectordialog").dialog("destroy");
		}
	});
	$("#date_selectordialog").dialog("open");
	return true;
}

function chargeselectMenu() {
	$("#charge_selector").dialog( {
		title : '<label>Choose Cost</label>',
		autoOpen : false,
		modal : true,
		dialogClass : 'specified_search_dialog',
		close : function() {
			$("#charge_selector").dialog("destroy");
		}
	});
	$("#charge_selector").dialog("open");
	return true;
}

$("input[name$='selectdate']").click(function() {
	var val = $(this).val();
	if (val == "exactoption") {
		$("#exactdate").show();
		$("#rangedate").hide();
		$("#fromdate").val("");
		$("#todate").val("");
	} else {
		$("#exactdatetext").val("");
		$("#exactdate").hide();
		$("#rangedate").show();
	}
});

$("input[name$='charge']").click(function() {
	var val = $(this).val();
	if (val == "exactcostoption") {
		$("#costrange").hide();
		$("#exactcost").show();
		$("#fromchg").val("");
		$("#tochg").val("");
	} else {
		$("#costrange").show();
		$("#exactcost").hide();
		$("#exactcosttext").val("");
	}
});

var savedfiltersetname;
function editFilter() {
	$("#advanceSearchsave").dialog("destroy");
	$("#savedfilterset_area").empty();
	savedfiltersetname = $('#savedfilter').find(":selected").text();
	var colval, colname, colnamecdm, res;
	var excludecols = [];
	/*
	 * if($('#savedfilter').find(":selected").text()=="Saved Filter Sets"){
	 * alertify.alert("Please select Filter set to edit."); return; }
	 */
	$("#advanceSearcheditform").dialog( {
		title : '<label>Edit Filters</label>',
		autoOpen : false,
		modal : true,
		draggable : true,
		dialogClass : 'advncesrchdialog',
		close : function() {
			$("#date_selectordialog").dialog("destroy");
			$("#charge_selector").dialog("destroy");
			$("#advanceSearcheditform").dialog("destroy");
		}
	});
	$("#advanceSearcheditform").dialog("open");
	$("#advanceSearcheditform").css('overflow', 'hidden');
	$("#advanceSearcheditform").resizable( {
		handles : 's,e'
	})
	$.ajax( {
		url : "applyfilter.action",
		type : "post",
		async : false,
		data : {
			"filtrName" : savedfiltersetname
		},
		success : function(response) {
			$("#savedval").val(savedfiltersetname);

			for (i = 0; i <= response.savedfiltersVal.length; i++) {
				pk = response.savedfiltersVal[i].PKMaint;
				fk = response.savedfiltersVal[i].FK;
				colnamecdm = response.savedfiltersVal[i].CDM_COLS_NAME;
				colval = response.savedfiltersVal[i].FILTER_VAL;
				colname = response.savedfiltersVal[i].colname;
				excludecols.push(colnamecdm);// used for loading cols of
												// filterset saved
		res = colname + " : " + colval;
		$("#savedfilterset_area").append(
				'<li class= "li_style" id ="' + colnamecdm + '">' + res + '   '
						+ '<a class = "remove_li"> X </a>' + '</li>');
	}
	;
}
	});
	if (excludecols != "") {
		$.ajax( {
			url : "loadfiltersetcols.action",
			type : "post",
			dataType : "json",
			data : {
				"excludecols" : JSON.stringify(excludecols)
			},
			success : function(response) {
				$("#searchfiltersave").html(response['advancesrchcols']);
				excludecols.length = 0;
			}
		});
	} else {
		$("#savedfilterset_area")
				.html(
						"<div style='color:#FF0000;'>No saved filters to display.</div>");
		$.ajax( {
			url : "getadvncesrchcols.action",
			type : "post",
			data : null,
			success : function(response) {
				$("#searchfiltersave").html(response['advancesrchcols']);
			}
		});
	}
}

$('#savedfilterset_area a.remove_li').live(
		'click',
		function myLiveEventHandler(event) {
			if (event.handled !== true) {
				$(this).parent().remove();
				var col = $(this).parent().text();
				var id_cdmcol = $(this).parent().attr('id');
				var removeItem = id_cdmcol
						+ ":"
						+ col.substring(col.indexOf(":") + 1,
								col.indexOf("X") - 1).trim();
				if ($('.listview li').length != 0
						|| $('.grpdata li').length != 0
						|| $('.setgrpdata li').length != 0) {
					selectedValues = jQuery.grep(selectedValues,
							function(value) {
								return value != removeItem;
							});
				}
				$.ajax( {
					url : "removeonefilter.action",
					type : "post",
					async : false,
					data : {
						"columnname" : col,
						"FKID" : fk,
						"cdmcol" : id_cdmcol
					},
					success : function(response) {
						if (response.datatype != null) {
							$('#searchfiltersave')
									.append(
											'<option value ="'
													+ response.optval
													+ '" datatype = "'
													+ response.datatype + '">'
													+ response.columnname
													+ '</option>');
						} else {
							$('#searchfiltersave')
									.append(
											'<option value ="'
													+ response.optval + '">'
													+ response.columnname
													+ '</option>');
						}
					}
				});
				event.handled = true;
			}
			return false;
		});

function saveUpdatedFilters() {
	filtername_check = "Edit";
	var changeFlag = true;
	var updatedvals = [];
	var updatedval = $("#savedval").val();
	if (updatedval.trim() == "") {
		alertify.alert("Please enter Filter set name.");
		return;
	}
	if ($("#savedfilterset_area li").length == 0) {
		alertify.alert("Please select filter(s) to save.");
		return;
	}
	$("#savedfilterset_area li").each(function() {
		updatedvals.push($(this).text() + ":" + $(this).attr('id'));
	});
	if (savedfiltersetname.trim() == updatedval.trim()) {
		changeFlag = false;
	}
	$
			.ajax( {
				url : "updatefiltername.action",
				type : "post",
				async : false,
				data : {
					"filtrName" : updatedval,
					"changeFlagstatus" : changeFlag,
					"FKID" : fk
				},
				success : function(response) {
					if (response.checkForExisting == false) {
						alertify
								.alert('Filter set "' + updatedval + '" already exists.');
						return;
					} else {
						$
								.ajax( {
									url : "saveditedfilter.action",
									type : "post",
									data : {
										"filtrName" : updatedval,
										"updated_val_array" : JSON
												.stringify(updatedvals),
										"FKID" : fk
									},
									success : function(response) {
										alertify
												.alert("Filter set updated successfully.");
										updateAndapply();
										updatedvals.length = 0;
									}
								});
					}
				}
			});
	// $('.progress-indicator').css( 'display', 'none' );
}
function cancelUpdate() {
	$("#advanceSearcheditform").dialog("destroy");
}
function delFilter() {
	var filtername = "";
	if ($('#advanceSearchsave').dialog('isOpen') === true) {
		filtername = $('#savedfilter').find(":selected").text();
	} else {
		filtername = $("#savedval").val();
	}
	/*
	 * if(filtername=="Saved Filter Sets"){ alertify.alert("Please select Filter
	 * set to delete."); return; }
	 */
	var r = alertify.confirm(
			"This action will delete Filter set. Do you want to continue?",
			function(r) {
				if (r) {
					$.ajax( {
						url : "deletefilter.action",
						type : "post",
						async : false,
						data : {
							"filtrName" : filtername
						},
						success : function(response) {
							$("#advanceSearchsave").dialog("destroy");
							$("#advanceSearcheditform").dialog("destroy");
							alertify.alert("Filter set deleted successfully.");
						}
					});
				} else {
				}
				return;
			});
}
function clearFilters() {
	$("#matchvalue").val("");
	$("#showfilters_area").empty();
	selectedValues.length = 0;
	$.ajax( {
		url : "getadvncesrchcols.action",
		type : "post",
		data : null,
		success : function(response) {
			$("#searchfilter").html(response['advancesrchcols']);
		}
	});
}
function clearFiltersandDelete() {
	$("#enterval").val("");
	$("#savedfilterset_area").empty();
	$.ajax( {
		url : "getadvncesrchcols.action",
		type : "post",
		data : null,
		success : function(response) {
			$("#searchfiltersave").html(response['advancesrchcols']);
		}
	});
}

function addDate() {
	var cdmkey = $('#searchfiltersave').val();
	var cdmkey1 = $('#searchfilter').val();
	var fromdate = $("#fromdate").val();
	var todate = $("#todate").val();
	var exactdate = $("#exactdatetext").val();
	var start = $('#fromdate').datepicker('getDate');
	var end = $('#todate').datepicker('getDate');
	var advncesrchfiltertext = $('#searchfilter').find(":selected").text();
	var adsrchtext = $('#searchfiltersave').find(":selected").text();
	if (exactdate.trim() == "" && fromdate.trim() == "" && todate.trim() == "") {
		alertify.alert("Please enter filter value");
		return;
	} else {
		if (fromdate != "" && todate != "") {
			if (start >= end) {
				alertify.alert("Please enter valid date range");
				return;
			} else {
				if ($('#advanceSearchnew').dialog('isOpen') === true) {
					$('#searchfilter').find(":selected").remove();
					$('#searchfilter').prop('selectedIndex', 0);
					$("#showfilters_area").append(
							'<li class= "li_style" id ="' + cdmkey1 + '">'
									+ advncesrchfiltertext + " : " + fromdate
									+ " To " + todate + '   '
									+ '<a class = "remove_li" > X </a>'
									+ '</li>');

				} else {
					$('#searchfiltersave').find(":selected").remove();
					$('#searchfiltersave').prop('selectedIndex', 0);
					$("#savedfilterset_area").append(
							'<li class= "li_style" id ="' + cdmkey + '">'
									+ adsrchtext + " : " + fromdate + " To "
									+ todate + '   '
									+ '<a class = "remove_li" > X </a>'
									+ '</li>');
				}
			}
		} else if (todate.trim() == "" && fromdate.trim() != "") {
			if ($('#advanceSearchnew').dialog('isOpen') === true) {
				$('#searchfilter').find(":selected").remove();
				$('#searchfilter').prop('selectedIndex', 0);
				$("#showfilters_area").append(
						'<li class= "li_style" id ="' + cdmkey1 + '">'
								+ advncesrchfiltertext + " : From " + fromdate
								+ '   ' + '<a class = "remove_li" > X </a>'
								+ '</li>');

			} else {
				$('#searchfiltersave').find(":selected").remove();
				$('#searchfiltersave').prop('selectedIndex', 0);
				$("#savedfilterset_area").append(
						'<li class= "li_style" id ="' + cdmkey + '">'
								+ adsrchtext + " : From " + fromdate + '   '
								+ '<a class = "remove_li" > X </a>' + '</li>');

			}

		} else if (fromdate.trim() == "" && todate.trim() != "") {
			if ($('#advanceSearchnew').dialog('isOpen') === true) {
				$('#searchfilter').find(":selected").remove();
				$('#searchfilter').prop('selectedIndex', 0);
				$("#showfilters_area").append(
						'<li class= "li_style" id ="' + cdmkey1 + '">'
								+ advncesrchfiltertext + " : Up to " + todate
								+ '   ' + '<a class = "remove_li" > X </a>'
								+ '</li>');

			} else {
				$('#searchfiltersave').find(":selected").remove();
				$('#searchfiltersave').prop('selectedIndex', 0);
				$("#savedfilterset_area").append(
						'<li class= "li_style" id ="' + cdmkey + '">'
								+ adsrchtext + " : Up to " + todate + '   '
								+ '<a class = "remove_li" > X </a>' + '</li>');
			}
		} else {
			if ($('#advanceSearchnew').dialog('isOpen') === true) {
				$('#searchfilter').find(":selected").remove();
				$('#searchfilter').prop('selectedIndex', 0);
				$("#showfilters_area").append(
						'<li class= "li_style" id ="' + cdmkey1 + '">'
								+ advncesrchfiltertext + " : " + exactdate
								+ '   ' + '<a class = "remove_li" > X </a>'
								+ '</li>');
			} else {
				$('#searchfiltersave').find(":selected").remove();
				$('#searchfiltersave').prop('selectedIndex', 0);
				$("#savedfilterset_area").append(
						'<li class= "li_style" id ="' + cdmkey + '">'
								+ adsrchtext + " : " + exactdate + '   '
								+ '<a class = "remove_li" > X </a>' + '</li>');
			}
		}
		$("#date_selectordialog").dialog("destroy");
	}
	$('#matchvalue').attr('disabled', 'true');
	$('#enterval').attr('disabled', 'true');
	$("#fromdate").val("");
	$("#todate").val("");
	$("#exactdatetext").val("");
}

function addCharge() {
	var cdmkey = $('#searchfiltersave').val();
	var cdmkey1 = $('#searchfilter').val();
	var advncesrchfiltertext = $('#searchfilter').find(":selected").text();
	var exactchg = $("#exactcosttext").val().trim();
	var minchg = $("#fromchg").val().trim();
	var maxchg = $("#tochg").val().trim();
	var adsrchtext = $('#searchfiltersave').find(":selected").text();
	if (exactchg == "" && minchg == "" && maxchg == "") {
		alertify.alert("Please enter filter value.");
		return;
	} else {
		if (minchg != "" && maxchg != "") {
			if (parseInt(minchg) >= parseInt(maxchg)) {
				alertify.alert("Please enter valid charge range.");
				return;
			} else {
				if ($('#advanceSearchnew').dialog('isOpen') === true) {
					$('#searchfilter').find(":selected").remove();
					$('#searchfilter').prop('selectedIndex', 0);
					$("#showfilters_area").append(
							'<li class= "li_style" id ="' + cdmkey1 + '">'
									+ advncesrchfiltertext + " : " + minchg
									+ " - " + maxchg + '   '
									+ '<a class = "remove_li" > X </a>'
									+ '</li>');
				} else {
					$('#searchfiltersave').find(":selected").remove();
					$('#searchfiltersave').prop('selectedIndex', 0);
					$("#savedfilterset_area").append(
							'<li class= "li_style" id ="' + cdmkey + '">'
									+ adsrchtext + " : " + minchg + " - "
									+ maxchg + '   '
									+ '<a class = "remove_li" > X </a>'
									+ '</li>');
				}
			}
		} else if (minchg == "" && maxchg != "") {
			if ($('#advanceSearchnew').dialog('isOpen') === true) {
				$('#searchfilter').find(":selected").remove();
				$('#searchfilter').prop('selectedIndex', 0);
				$("#showfilters_area").append(
						'<li class= "li_style" id ="' + cdmkey1 + '">'
								+ advncesrchfiltertext + " : Up to " + maxchg
								+ '   ' + '<a class = "remove_li" > X </a>'
								+ '</li>');
			} else {
				$('#searchfiltersave').find(":selected").remove();
				$('#searchfiltersave').prop('selectedIndex', 0);
				$("#savedfilterset_area").append(
						'<li class= "li_style" id ="' + cdmkey + '">'
								+ adsrchtext + " : Up to " + maxchg + '   '
								+ '<a class = "remove_li" > X </a>' + '</li>');
			}

		} else if (maxchg == "" && minchg != "") {
			if ($('#advanceSearchnew').dialog('isOpen') === true) {
				$('#searchfilter').find(":selected").remove();
				$('#searchfilter').prop('selectedIndex', 0);
				$("#showfilters_area").append(
						'<li class= "li_style" id ="' + cdmkey1 + '">'
								+ advncesrchfiltertext + " : From " + minchg
								+ '   ' + '<a class = "remove_li" > X </a>'
								+ '</li>');
			} else {
				$('#searchfiltersave').find(":selected").remove();
				$('#searchfiltersave').prop('selectedIndex', 0);
				$("#savedfilterset_area").append(
						'<li class= "li_style" id ="' + cdmkey + '">'
								+ adsrchtext + " : From " + minchg + '   '
								+ '<a class = "remove_li" > X </a>' + '</li>');
			}
		} else {
			if ($('#advanceSearchnew').dialog('isOpen') === true) {
				$('#searchfilter').find(":selected").remove();
				$('#searchfilter').prop('selectedIndex', 0);
				$("#showfilters_area").append(
						'<li class= "li_style" id ="' + cdmkey1 + '">'
								+ advncesrchfiltertext + " : " + exactchg
								+ '   ' + '<a class = "remove_li" > X </a>'
								+ '</li>');
			} else {
				$('#searchfiltersave').find(":selected").remove();
				$('#searchfiltersave').prop('selectedIndex', 0);
				$("#savedfilterset_area").append(
						'<li class= "li_style" id ="' + cdmkey + '">'
								+ adsrchtext + " : " + exactchg + '   '
								+ '<a class = "remove_li" > X </a>' + '</li>');
			}
		}
		$("#charge_selector").dialog("destroy");
	}
	$('#matchvalue').attr('disabled', 'true');
	$('#enterval').attr('disabled', 'true');
	$("#exactcosttext").val("");
	$("#fromchg").val("");
	$("#tochg").val("");
}

function showFilters() {
	var advncesrchfilterval, advncesrchfiltertext, val, res;
	$("#matchvalue").removeAttr('onclick');
	advncesrchfiltertext = $('#searchfilter').find(":selected").text();
	advncesrchfilterval = $('#searchfilter').val();
	val = $('#matchvalue').val();
	res = advncesrchfiltertext + " : " + val;
	if (advncesrchfiltertext == "Select Filter") {
		alertify.alert("Please select column name.");
		return;
	}
	if (val != "") {
		$('#searchfilter').find(":selected").remove();
	}
	$('#matchvalue').val("");
	$('#searchfilter').prop('selectedIndex', 0);
	if (val.trim().length == 0) {
		alertify.alert("Please enter filter value.");
	} else {
		if ($('#showfilters_area li').length == 0) {
			$("#showfilters_area").html(
					'<li class= "li_style" id ="' + advncesrchfilterval + '">'
							+ res + '   ' + '<a class = "remove_li"> X </a>'
							+ '</li>');
		} else {
			$("#showfilters_area").append(
					'<li class= "li_style" id ="' + advncesrchfilterval + '">'
							+ res + '   ' + '<a class = "remove_li" > X </a>'
							+ '</li>');
		}
	}
	$('#matchvalue').attr('disabled', 'true');
}

$('#showfilters_area a.remove_li').live(
		'click',
		function myLiveEventHandler(event) {
			if (event.handled !== true) {
				$(this).parent().remove();
				var col = $(this).parent().text();
				var id_cdmcol = $(this).parent().attr('id');
				var removeItem = id_cdmcol
						+ ":"
						+ col.substring(col.indexOf(":") + 1,
								col.indexOf("X") - 1).trim();
				if ($('.listview li').length != 0
						|| $('.grpdata li').length != 0
						|| $('.setgrpdata li').length != 0) {
					selectedValues = jQuery.grep(selectedValues,
							function(value) {
								return value != removeItem;
							});
				}
				$.ajax( {
					url : "removeonefilter.action",
					type : "post",
					async : false,
					data : {
						"columnname" : col,
						"cdmcol" : id_cdmcol
					},
					success : function(response) {
						if (response.datatype != null) {
							$('#searchfilter')
									.append(
											'<option value ="'
													+ response.optval
													+ '" datatype = "'
													+ response.datatype + '">'
													+ response.columnname
													+ '</option>');
						} else {
							$('#searchfilter')
									.append(
											'<option value ="'
													+ response.optval + '">'
													+ response.columnname
													+ '</option>');
						}
					}
				});
				event.handled = true;
			}
			return false;
		});
$('.grpdata a.remove_li')
		.live(
				'click',
				function myLiveEventHandler(event) {
					if (event.handled !== true) {
						filtername_check = "";
						$('.progress-indicator').css('display', 'block');
						$(this).closest('li').remove();
						var newkeys = [];
						var newkeyvals = [];
						var newcdmkeys = [];
						var col = $(this).parent().text();
						var id_cdmcol = $(this).parent().attr('id');
						$('.grpdata li')
								.each(
										function() {
											if ($.inArray($(this).attr('id'),
													newcdmkeys) == -1) {
												newcdmkeys.push($(this).attr(
														'id'));
											}
											if ($.inArray($(this).text()
													.replace("X", "").trim(),
													newkeyvals) == -1) {
												newkeyvals.push($(this).text()
														.replace("X", "")
														.trim());
											}
											if ($
													.inArray(
															$(this).attr('id')
																	+ ":"
																	+ $(this)
																			.text()
																			.substring(
																					$(
																							this)
																							.text()
																							.indexOf(
																									":") + 1,
																					$(
																							this)
																							.text()
																							.indexOf(
																									"X") - 1)
																			.trim(),
															newkeys) == -1) {
												newkeys
														.push($(this)
																.attr('id')
																+ ":"
																+ $(this)
																		.text()
																		.substring(
																				$(
																						this)
																						.text()
																						.indexOf(
																								":") + 1,
																				$(
																						this)
																						.text()
																						.indexOf(
																								"X") - 1)
																		.trim());
											}
										});
						$("#maincdmData").dataTable().fnDestroy();
						$("#maincdmData").empty();
						oTable = $("#maincdmData")
								.dataTable(
										{
											"bProcessing" : false,
											"bServerSide" : true,
											"sAjaxSource" : "getcdmsearchgrpdata.action",
											"fnServerParams" : function(aoData) {
												aoData.push( {
													"name" : "chgtype",
													"value" : $("#chgtype")
															.val()
												});
												aoData.push( {
													"name" : "preprvver",
													"value" : $("#preprvver")
															.val()
												});
												aoData.push( {
													"name" : "advanceSearch",
													"value" : true
												});
												aoData.push( {
													"name" : "chgstat",
													"value" : chgstatval
												});
												aoData.push( {
													"name" : "FILE_ID",
													"value" : $("#cdmtype")
															.val()
												});
												aoData.push( {
													"name" : "grpID",
													"value" : editServSecId
												}), aoData.push( {
													"name" : "filterValues",
													"value" : JSON
															.stringify(newkeys)
												});
											},
											"bJQueryUI" : true,
											"bPaginate" : true,
											'iDisplayLength' : 100,
											"bLengthChange" : true,
											"bDestroy" : true,
											"bFilter" : true,
											"bSort" : true,
											"sScrollY" : "230px",
											"sScrollX" : "100%",
											"sScrollXInner" : "100%",
											"sDom" : 'RC<"clear">frtlip',
											"sPaginationType" : "full_numbers",
											"oColVis" : {
												"bShowAll" : true,
												"sShowAll" : "Select All"
											},
											"aoColumns" : fetchColumnProp(chgmodulename),
											"fnDrawCallback" : function() {
												$('#allcb').prop('checked',
														false);
												$(
														'tbody tr td input[name="check"]')
														.each(
																function() {
																	if ($
																			.inArray(
																					$(
																							this)
																							.val(),
																					checkdAll) != -1) {
																		$(this)
																				.prop(
																						'checked',
																						true);
																		$(
																				'#allcb')
																				.prop(
																						'checked',
																						true);
																	}
																});
											}
										});
						$(
								'<select id="chgstat_dropdown" onchange="updateViewForChgStatAdvSrchSec(this.value);"><option value="1">Active </option><option value="2">Inactive </option><option value="3">Both </option></select>')
								.insertBefore('#maincdmData_filter label');
						$(
								'<a class="addFilter" href="#" onclick="showAddFilterform();">Add Filters</a>')
								.insertAfter('#maincdmData_filter');
						$(
								'<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>')
								.insertAfter('#maincdmData_filter');
						$(
								'<div class="sersec"><label id="showfilter_indt" >Search results for : </label><a href="#" onclick="clearAllFiltersSec();" class= "clearall_grp" style="padding-left:10px"> CLEAR ALL</a></div>')
								.insertAfter('#maincdmData_filter');
						$('<div class="grpdata"></div>').insertAfter(
								'#maincdmData_filter');
						$("#showfilters_area").empty();
						for (j = 0, i = 0; i < newkeyvals.length,
								j < newcdmkeys.length; i++, j++) {
							$('.grpdata').append(
									'<li class= "li_style" id ="'
											+ newcdmkeys[i] + '">'
											+ newkeyvals[i] + '   '
											+ '<a class = "remove_li"> X </a>'
											+ '</li>');
							$("#showfilters_area").append(
									'<li class= "li_style" id ="'
											+ newcdmkeys[i] + '">'
											+ newkeyvals[i] + '   '
											+ '<a class = "remove_li"> X </a>'
											+ '</li>');
						}
						$("#chgstat_dropdown").val(chgstatval);
						if ($('.grpdata li').length == 0) {
							clearAllFiltersSec();
						}
						$(".ColVis_MasterButton span").html("");
						initilizeCheckBox();
						$('.progress-indicator').css('display', 'none');
						event.handled = true;
					}
					return false;
				});

$('.setgrpdata a.remove_li')
		.live(
				'click',
				function myLiveEventHandler(event) {
					if (event.handled !== true) {
						filtername_check = "";
						$('.progress-indicator').css('display', 'block');
						var newkeys = [];
						var newkeyvals = [];
						var newcdmkeys = [];
						var col = $(this).parent().text();
						var id_cdmcol = $(this).parent().attr('id');
						$(this).closest('li').remove();
						$('.setgrpdata li')
								.each(
										function() {
											if ($.inArray($(this).attr('id'),
													newcdmkeys) == -1) {
												newcdmkeys.push($(this).attr(
														'id'));
											}
											if ($.inArray($(this).text()
													.replace("X", "").trim(),
													newkeyvals) == -1) {
												newkeyvals.push($(this).text()
														.replace("X", "")
														.trim());
											}
											if ($
													.inArray(
															$(this).attr('id')
																	+ ":"
																	+ $(this)
																			.text()
																			.substring(
																					$(
																							this)
																							.text()
																							.indexOf(
																									":") + 1,
																					$(
																							this)
																							.text()
																							.indexOf(
																									"X") - 1)
																			.trim(),
															newkeys) == -1) {
												newkeys
														.push($(this)
																.attr('id')
																+ ":"
																+ $(this)
																		.text()
																		.substring(
																				$(
																						this)
																						.text()
																						.indexOf(
																								":") + 1,
																				$(
																						this)
																						.text()
																						.indexOf(
																								"X") - 1)
																		.trim());
											}
										});
						$("#maincdmData").dataTable().fnDestroy();
						$("#maincdmData").empty();
						oTable = $("#maincdmData")
								.dataTable(
										{
											"bProcessing" : false,
											"bServerSide" : true,
											"sAjaxSource" : "getsslinkdata.action",
											"fnServerParams" : function(aoData) {
												aoData.push( {
													"name" : "ssID",
													"value" : serSetId
												}), aoData.push( {
													"name" : "chgtype",
													"value" : $("#chgtype")
															.val()
												});
												aoData.push( {
													"name" : "preprvver",
													"value" : $("#preprvver")
															.val()
												});
												aoData.push( {
													"name" : "advanceSearch",
													"value" : true
												});
												aoData.push( {
													"name" : "chgstat",
													"value" : chgstatval
												});
												aoData.push( {
													"name" : "FILE_ID",
													"value" : $("#cdmtype")
															.val()
												});
												aoData.push( {
													"name" : "filterValues",
													"value" : JSON
															.stringify(newkeys)
												});
											},
											"bJQueryUI" : true,
											"bPaginate" : true,
											'iDisplayLength' : 100,
											"bLengthChange" : true,
											"bDestroy" : true,
											"bFilter" : true,
											"bSort" : true,
											"sScrollY" : "230px",
											"sScrollX" : "100%",
											"sScrollXInner" : "100%",
											"sDom" : 'RC<"clear">frtlip',
											"sPaginationType" : "full_numbers",
											"oColVis" : {
												"bShowAll" : true,
												"sShowAll" : "Select All"
											},
											"aoColumns" : fetchColumnProp(chgmodulename),
											"fnDrawCallback" : function() {
												$('#allcb').prop('checked',
														false);
												$(
														'tbody tr td input[name="check"]')
														.each(
																function() {
																	if ($
																			.inArray(
																					$(
																							this)
																							.val(),
																					checkdAll) != -1) {
																		$(this)
																				.prop(
																						'checked',
																						true);
																		$(
																				'#allcb')
																				.prop(
																						'checked',
																						true);
																	}
																});
											}
										});
						$(
								'<select id="chgstat_dropdown" onchange="updateViewForChgStatAdvSrchSet(this.value);""><option value="1">Active  </option><option value="2">Inactive </option><option value="3">Both </option></select>')
								.insertBefore('#maincdmData_filter label');
						$(
								'<a class="addFilter" href="#" onclick="showAddFilterform();">Add Filters</a>')
								.insertAfter('#maincdmData_filter');
						$(
								'<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>')
								.insertAfter('#maincdmData_filter');
						$(
								'<div class="serset"><label id="showfilter_indt" >Search results for : </label><a href="#" onclick="clearAllFiltersSet();" class= "clearall_grp" style="padding-left:10px"> CLEAR ALL </a></div>')
								.insertAfter('#maincdmData_filter');
						$('<div class="setgrpdata"></div>').insertAfter(
								'#maincdmData_filter');
						$("#showfilters_area").empty();
						for (j = 0, i = 0; i < newkeyvals.length,
								j < newcdmkeys.length; i++, j++) {
							$('.setgrpdata').append(
									'<li class= "li_style" id ="'
											+ newcdmkeys[i] + '">'
											+ newkeyvals[i] + '   '
											+ '<a class = "remove_li"> X </a>'
											+ '</li>');
							$("#showfilters_area").append(
									'<li class= "li_style" id ="'
											+ newcdmkeys[i] + '">'
											+ newkeyvals[i] + '   '
											+ '<a class = "remove_li"> X </a>'
											+ '</li>');
						}
						$("#chgstat_dropdown").val(chgstatval);
						if ($('.setgrpdata li').length == 0) {
							clearAllFiltersSet();
						}
						$(".ColVis_MasterButton span").html("");
						initilizeCheckBox();
						$('.progress-indicator').css('display', 'none');
						event.handled = true;
					}
					return false;
				});

$('.listview a.remove_li')
		.live(
				'click',
				function myLiveEventHandler(event) {
					if (event.handled !== true) {
						filtername_check = "";
						$('.progress-indicator').css('display', 'block');
						var col = $(this).parent().text();
						var id_cdmcol = $(this).parent().attr('id');
						var new_keys = []
						var new_keyvals = [];
						var new_cdmkeys = [];
						var removeItem = id_cdmcol
								+ ":"
								+ col.substring(col.indexOf(":") + 1,
										col.indexOf("X") - 1).trim();
						selectedValues = jQuery.grep(selectedValues, function(
								value) {
							return value != removeItem;
						});
						$(this).closest('li').remove();
						$('.listview li')
								.each(
										function() {
											if ($.inArray($(this).attr('id'),
													new_cdmkeys) == -1) {
												new_cdmkeys.push($(this).attr(
														'id'));
											}
											if ($.inArray($(this).text()
													.replace("X", "").trim(),
													new_keyvals) == -1) {
												new_keyvals.push($(this).text()
														.replace("X", "")
														.trim());
											}
											if ($
													.inArray(
															$(this).attr('id')
																	+ ":"
																	+ $(this)
																			.text()
																			.substring(
																					$(
																							this)
																							.text()
																							.indexOf(
																									":") + 1,
																					$(
																							this)
																							.text()
																							.indexOf(
																									"X") - 1)
																			.trim(),
															new_keys) == -1) {
												new_keys
														.push($(this)
																.attr('id')
																+ ":"
																+ $(this)
																		.text()
																		.substring(
																				$(
																						this)
																						.text()
																						.indexOf(
																								":") + 1,
																				$(
																						this)
																						.text()
																						.indexOf(
																								"X") - 1)
																		.trim());
											}
										});
						$("#maincdmData").dataTable().fnDestroy();
						$("#maincdmData").empty();
						oTable = $("#maincdmData")
								.dataTable(
										{
											"bProcessing" : false,
											"bServerSide" : true,
											"sAjaxSource" : "getcdmsearchdata.action",
											"fnServerParams" : function(aoData) {
												aoData.push( {
													"name" : "grpID",
													"value" : editServSecId
												}), aoData.push( {
													"name" : "chgtype",
													"value" : $("#chgtype")
															.val()
												});
												aoData.push( {
													"name" : "preprvver",
													"value" : $("#preprvver")
															.val()
												});
												aoData.push( {
													"name" : "advanceSearch",
													"value" : true
												});
												aoData.push( {
													"name" : "chgstat",
													"value" : chgstatval
												});
												aoData.push( {
													"name" : "FILE_ID",
													"value" : $("#cdmtype")
															.val()
												});
												aoData
														.push( {
															"name" : "filterValues",
															"value" : JSON
																	.stringify(new_keys)
														});
											},
											"bJQueryUI" : true,
											"bPaginate" : true,
											'iDisplayLength' : 100,
											"bLengthChange" : true,
											"bDestroy" : true,
											"bFilter" : true,
											"bSort" : true,
											"sScrollY" : "400px",
											"sScrollX" : "100%",
											"sScrollXInner" : "100%",
											"sDom" : 'RC<"clear">frtlip',
											"sPaginationType" : "full_numbers",
											"oColVis" : {
												"bShowAll" : true,
												"sShowAll" : "Select All"
											},
											"aoColumns" : fetchColumnProp(chgmodulename),
											"fnDrawCallback" : function() {
												$('#allcb').prop('checked',
														false);
												$(
														'tbody tr td input[name="check"]')
														.each(
																function() {
																	if ($
																			.inArray(
																					$(
																							this)
																							.val(),
																					checkdAll) != -1) {
																		$(this)
																				.prop(
																						'checked',
																						true);
																		$(
																				'#allcb')
																				.prop(
																						'checked',
																						true);
																	}
																});
											}
										});
						$(
								'<select id="chgstat_dropdown" onchange="updateViewForChgStatAdvSrch(this.value);"><option value="1">Active </option><option value="2">Inactive </option><option value="3">Both </option></select>')
								.insertBefore('#maincdmData_filter label');
						$(
								'<a class="addFilter" href="#" onclick="showAddFilterform();">Add Filters</a>')
								.insertAfter('#maincdmData_filter');
						$(
								'<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>')
								.insertAfter('#maincdmData_filter');
						$(
								'<div class="datablock" ><label id="showfilter_indt" >Search results for : </label></div>')
								.insertAfter('div.ColVis');
						$('.datablock')
								.append(
										'<div id="clearall_div"><a href="#" onclick="clearAllFiltersList();" class= "clearall_list1" style="padding-left:10px"> CLEAR ALL </a></div>');
						$('<div class="listview"></div>').insertAfter(
								'div.ColVis');
						$("#showfilters_area").empty();
						for (j = 0, i = 0; i < new_keyvals.length,
								j < new_cdmkeys.length; i++, j++) {
							$('.listview').append(
									'<li class= "li_style" id ="'
											+ new_cdmkeys[i] + '">'
											+ new_keyvals[i] + '   '
											+ '<a class = "remove_li"> X </a>'
											+ '</li>');
							$("#showfilters_area").append(
									'<li class= "li_style" id ="'
											+ new_cdmkeys[i] + '">'
											+ new_keyvals[i] + '   '
											+ '<a class = "remove_li"> X </a>'
											+ '</li>');
						}
						$("#chgstat_dropdown").val(chgstatval);
						if ($('.listview li').length == 0) {
							clearAllFiltersList();
						}
						$(".ColVis_MasterButton span").html("");
						initilizeCheckBox();
						event.handled = true;
						$('.progress-indicator').css('display', 'none');
					}
					return false;
				});

function applyFilters() {

	filtername_check = "apply";
	var filterName = $('#entertosave').val();
	var excludecols = [];
	savedfiltercolvals.length = 0;
	if ($("#showfilters_area li").length == 0) {
		alertify.alert("Please select filter(s) to apply.");
		return;
	}
	$("#exactcosttext").val("");
	$("#fromchg").val("");
	$("#tochg").val("");
	$("#fromdate").val("");
	$("#todate").val("");
	$("#exactdatetext").val("");
	$('#matchvalue').val("");
	$("#showfilters_area li").each(
			function() {
				if ($.inArray($(this).attr('id')
						+ ":"
						+ $(this).text().substring(
								$(this).text().indexOf(":") + 1,
								$(this).text().indexOf("X") - 1).trim(),
						selectedValues) == -1) {
					selectedValues.push($(this).attr('id')
							+ ":"
							+ $(this).text().substring(
									$(this).text().indexOf(":") + 1,
									$(this).text().indexOf("X") - 1).trim());
				}
				excludecols.push($(this).attr('id'));
			});
	$('.progress-indicator').css('display', 'block');
	if (currentView == 'List') {
		$("#maincdmData").dataTable().fnDestroy();
		$("#maincdmData").empty();
		// oTable = $("#maincdmData").dataTable(advanceSearch);
		oTable = $("#maincdmData").dataTable( {
			"bProcessing" : false,
			"bServerSide" : true,
			"sAjaxSource" : "getcdmsearchdata.action",
			"fnServerParams" : function(aoData) {
				aoData.push( {
					"name" : "chgtype",
					"value" : $("#chgtype").val()
				});
				aoData.push( {
					"name" : "preprvver",
					"value" : $("#preprvver").val()
				});
				aoData.push( {
					"name" : "advanceSearch",
					"value" : true
				});
				aoData.push( {
					"name" : "chgstat",
					"value" : chgstatval
				});
				aoData.push( {
					"name" : "FILE_ID",
					"value" : $("#cdmtype").val()
				});
				aoData.push( {
					"name" : "filterValues",
					"value" : JSON.stringify(selectedValues)
				});
				aoData.push( {
					"name" : "savedfilters",
					"value" : JSON.stringify(savedfiltercolvals)
				});
			},
			"bJQueryUI" : true,
			"bPaginate" : true,
			'iDisplayLength' : 100,
			"bLengthChange" : true,
			"bDestroy" : true,
			"bFilter" : true,
			"bSort" : true,
			"sScrollY" : "230px",
			"sScrollX" : "100%",
			"sScrollXInner" : "100%",
			"sDom" : 'RC<"clear">frtlip',
			"sPaginationType" : "full_numbers",
			"oColVis" : {
				"bShowAll" : true,
				"sShowAll" : "Select All"
			},
			"aoColumns" : fetchColumnProp(chgmodulename),
			"fnDrawCallback" : function() {
				$('#allcb').prop('checked', false);
				$('tbody tr td input[name="check"]').each(function() {
					if ($.inArray($(this).val(), checkdAll) != -1) {
						$(this).prop('checked', true);
						$('#allcb').prop('checked', true);
					}
				});
			}
		});
		$(
				'<select id="chgstat_dropdown" onchange="updateViewForChgStatAdvSrch(this.value);"><option value="1">Active </option><option value="2">Inactive </option><option value="3">Both </option></select>')
				//.insertBefore('#maincdmData_filter label');
				.insertBefore('div.inner-addon input:first');
		$(
				'<a class="addFilter" href="#" onclick="showAddFilterform();">Add Filters</a>')
				.insertAfter('#maincdmData_filter');
		$(
				'<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>')
				.insertAfter('#maincdmData_filter');
		if ($('#showfilters_area li').length != 0
				&& $("#div.ColVis").not(":contains('Search results for')")) {
			if (savebuttonclicked == 'true') {
				$(
						'<div class="datablock" ><label id="showfilter_indt" >Search results for : ' + filterName + '</label></div>')
						.insertAfter('div.ColVis');
			} else {

				$(
						'<div class="datablock" ><label id="showfilter_indt" >Search results for : </label></div>')
						.insertAfter('div.ColVis');
			}
			$('.datablock')
					.append(
							'<div id="clearall_div"><a href="#" onclick="clearAllFiltersList();" class= "clearall_list1" style="padding-left:10px"> CLEAR ALL</a></div>');
			$(
					'<div class="listview">' + $("#showfilters_area").html() + '</div>')
					.insertAfter('div.ColVis');
		}
		$(".ColVis_MasterButton span").html("");
		initilizeCheckBox();
	} else if (currentView == 'ServiceSection') {
		advanceSearchForServiceSection();
	} else if (currentView == 'ServiceSet') {
		advanceSearchForServiceSet();
	}
	savebuttonclicked = 'false';

	$('.progress-indicator').css('display', 'none');
	$("#advanceSearchnew").dialog("destroy");
	$("#chgstat_dropdown").val(chgstatval);
}

function applySelectedfilterName() {

	filtername_check = "Saved";
	$("#savedfilterset_area").empty();
	selectedValues.length = 0;
	var res, colname, colval, colnamecdm;
	var filterName = $('#savedfilter').find(":selected").text();
	var excludecols = [];
	if (filterName == "Saved Filter Sets") {
		alertify.alert("Please select Filter set to apply.");
		return;
	}
	$('.progress-indicator').css('display', 'block');
	$.ajax( {
		url : "applyfilter.action",
		type : "post",
		async : false,
		data : {
			filtrName : filterName
		},
		success : function(response) {
			alertify.alert("Selected Filter set applied.");
			for (i = 0; i <= response.savedfiltersVal.length; i++) {
				fk = response.savedfiltersVal[i].FK;
				colnamecdm = response.savedfiltersVal[i].CDM_COLS_NAME;
				colval = response.savedfiltersVal[i].FILTER_VAL;
				colname = response.savedfiltersVal[i].colname;
				savedfiltercolvals.push(colnamecdm + ":" + colval);
				excludecols.push(colnamecdm);
				res = colname + " : " + colval;
				$("#savedfilterset_area").append(
						'<li class= "li_style" id ="' + colnamecdm + '">' + res
								+ '   ' + '<a class = "remove_li"> X </a>'
								+ '</li>');
				// $("#showfilters_area").append('<li class= "li_style" id
				// ="'+colnamecdm+'">'+res+' '+'<a class = "remove_li"> X
				// </a>'+'</li>');
			}
		}
	});
	if (currentView == 'List') {
		$("#maincdmData").dataTable().fnDestroy();
		$("#maincdmData").empty();
		oTable = $("#maincdmData").dataTable(advanceSearch);
		$(
				'<select id="chgstat_dropdown" onchange="updateViewForChgStatAdvSrch(this.value);"><option value="1">Active </option><option value="2">Inactive </option><option value="3">Both </option></select>')
				//.insertBefore('#maincdmData_filter label');
				.insertBefore('div.inner-addon input:first');
		$(
				'<a class="addFilter" href="#" onclick="showAddFilterform();">Add Filters</a>')
				.insertAfter('#maincdmData_filter');
		$(
				'<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>')
				.insertAfter('#maincdmData_filter');
		if ($("#savedfilterset_area li").length > 0) {
			$(
					'<div class="datablock" ><label id="showfilter_indt" >Search results for : ' + filterName + '</label></div>')
					.insertAfter('div.ColVis');
			$('.datablock')
					.append(
							'<div id="clearall_div"><a href="#" onclick="clearAllFiltersList();" class= "clearall_list1" style="padding-left:10px"> CLEAR ALL</a></div>');
			$(
					'<div class="listview">' + $("#savedfilterset_area").html() + '</div>')
					.insertAfter('div.ColVis');
		}
		$(".ColVis_MasterButton span").html("");
		initilizeCheckBox();
	} else if (currentView == 'ServiceSection') {
		advSrchForSerSecUpdate();
	} else if (currentView == 'ServiceSet') {
		advSrchForSerSetUpdate();
	}
	$("#advanceSearchsave").dialog("destroy");
	$('.progress-indicator').css('display', 'none');
	$("#chgstat_dropdown").val(chgstatval);
}

function updateAndapply() {
	var excludecols = [];
	var filterName = $("#savedval").val();
	selectedValues.length = 0;
	$("#savedfilterset_area li").each(
			function() {
				if ($.inArray($(this).attr('id')
						+ ":"
						+ $(this).text().substring(
								$(this).text().indexOf(":") + 1,
								$(this).text().indexOf("X") - 1).trim(),
						selectedValues) == -1) {
					selectedValues.push($(this).attr('id')
							+ ":"
							+ $(this).text().substring(
									$(this).text().indexOf(":") + 1,
									$(this).text().indexOf("X") - 1).trim());
				}
				excludecols.push($(this).attr('id'));
			});
	$('.progress-indicator').css('display', 'block');
	if (currentView == 'List') {
		$("#maincdmData").dataTable().fnDestroy();
		$("#maincdmData").empty();
		// oTable = $("#maincdmData").dataTable(advanceSearch);
		oTable = $("#maincdmData").dataTable( {
			"bProcessing" : false,
			"bServerSide" : true,
			"sAjaxSource" : "getcdmsearchdata.action",
			"fnServerParams" : function(aoData) {
				aoData.push( {
					"name" : "chgtype",
					"value" : $("#chgtype").val()
				});
				aoData.push( {
					"name" : "preprvver",
					"value" : $("#preprvver").val()
				});
				aoData.push( {
					"name" : "advanceSearch",
					"value" : true
				});
				aoData.push( {
					"name" : "chgstat",
					"value" : chgstatval
				});
				aoData.push( {
					"name" : "FILE_ID",
					"value" : $("#cdmtype").val()
				});
				aoData.push( {
					"name" : "filterValues",
					"value" : JSON.stringify(selectedValues)
				});
				aoData.push( {
					"name" : "savedfilters",
					"value" : JSON.stringify(savedfiltercolvals)
				});
			},
			"bJQueryUI" : true,
			"bPaginate" : true,
			'iDisplayLength' : 100,
			"bLengthChange" : true,
			"bDestroy" : true,
			"bFilter" : true,
			"bSort" : true,
			"sScrollY" : "230px",
			"sScrollX" : "100%",
			"sScrollXInner" : "100%",
			"sDom" : 'RC<"clear">frtlip',
			"sPaginationType" : "full_numbers",
			"oColVis" : {
				"bShowAll" : true,
				"sShowAll" : "Select All"
			},
			"aoColumns" : fetchColumnProp(chgmodulename),
			"fnDrawCallback" : function() {
				$('#allcb').prop('checked', false);
				$('tbody tr td input[name="check"]').each(function() {
					if ($.inArray($(this).val(), checkdAll) != -1) {
						$(this).prop('checked', true);
						$('#allcb').prop('checked', true);
					}
				});
			}
		});
		$(
				'<select id="chgstat_dropdown" onchange="updateViewForChgStatAdvSrch(this.value);"><option value="1">Active </option><option value="2">Inactive </option><option value="3">Both </option></select>')
				.insertBefore('#maincdmData_filter label');
		$(
				'<a class="addFilter" href="#" onclick="showAddFilterform();">Add Filters</a>')
				.insertAfter('#maincdmData_filter');
		$(
				'<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>')
				.insertAfter('#maincdmData_filter');
		if ($('#savedfilterset_area li').length != 0
				&& $("div.ColVis").not(":contains('Search results for')")) {
			$(
					'<div class="datablock" ><label id="showfilter_indt" >Search results for : ' + filterName + '</label></div>')
					.insertAfter('div.ColVis');
			$('.datablock')
					.append(
							'<div id="clearall_div"><a href="#" onclick="clearAllFiltersList();" class= "clearall_list1" style="padding-left:10px"> CLEAR ALL</a></div>');
			$(
					'<div class="listview">' + $("#savedfilterset_area").html() + '</div>')
					.insertAfter('div.ColVis');
			$("#showfilters_area").empty();
			$("#showfilters_area").append($("#savedfilterset_area").html());
		}
		$(".ColVis_MasterButton span").html("");
		initilizeCheckBox();
	} else if (currentView == 'ServiceSection') {
		advSrchForSerSecUpdate();
	} else if (currentView == 'ServiceSet') {
		advSrchForSerSetUpdate();
	}
	$('.progress-indicator').css('display', 'none');
	$("#advanceSearcheditform").dialog("destroy");
}

function clearAllFiltersList() {
	$('.progress-indicator').css('display', 'block');
	$("#maincdmData").dataTable().fnDestroy();
	$("#maincdmData").empty();
	drawDataTable();
	chgstatval = 1;
	$('.progress-indicator').css('display', 'none');
}
function clearAllFiltersSec() {
	$('.progress-indicator').css('display', 'block');
	loadGroupView();
	chgstatval = 1;
	$('.progress-indicator').css('display', 'none');
}
function clearAllFiltersSet() {
	$('.progress-indicator').css('display', 'block');
	loadServiceSetView();
	chgstatval = 1;
	$('.progress-indicator').css('display', 'none');
}

function showNewFilters() {
	var val, res, adsrchtext, adsrchval;
	$("#enterval").removeAttr('onclick');
	adsrchtext = $('#searchfiltersave').find(":selected").text();
	adsrchval = $('#searchfiltersave').val();
	val = $('#enterval').val();
	res = adsrchtext + " : " + val;
	$('#enterval').val("");
	if (adsrchtext == "Select Filter") {
		alertify.alert("Please select column name.");
		return;
	}
	if (val.trim() == "") {
		alertify.alert("Please enter filter value.");
		return;
	} else {
		$('#searchfiltersave').find(":selected").remove();
		$('#searchfiltersave').prop('selectedIndex', 0);
		if ($('#savedfilterset_area li').length == 0) {
			$("#savedfilterset_area")
					.html(
							'<li class= "li_style" id ="' + adsrchval + '">'
									+ res + '  '
									+ '<a class = "remove_li"> X </a>'
									+ '</li>');
		} else {
			$("#savedfilterset_area").append(
					'<li class= "li_style" id ="' + adsrchval + '">' + res
							+ '   ' + '<a class = "remove_li" > X </a>'
							+ '</li>');
		}
	}
	$('#enterval').attr('disabled', 'true');
}

function advSrchForSerSecUpdate() {
	$('.progress-indicator').css('display', 'block');
	var filterName;
	if (filtername_check == "Saved") {
		filterName = $('#savedfilter').find(":selected").text();
	} else if (filtername_check == "Edit") {
		filterName = $("#savedval").val();
	} else {
		filterName = "";
	}
	$("#maincdmData").dataTable().fnDestroy();
	$("#maincdmData").empty();
	oTable = $("#maincdmData").dataTable(
			{
				"bProcessing" : false,
				"bServerSide" : true,
				"sAjaxSource" : "getcdmsearchgrpdata.action",
				"fnServerParams" : function(aoData) {
					aoData.push( {
						"name" : "FILE_ID",
						"value" : $("#cdmtype").val()
					});
					aoData.push( {
						"name" : "grpID",
						"value" : editServSecId
					}), aoData.push( {
						"name" : "chgstat",
						"value" : chgstatval
					});
					aoData.push( {
						"name" : "advanceSearch",
						"value" : true
					});
					aoData.push( {
						"name" : "savedfilters",
						"value" : JSON.stringify(savedfiltercolvals)
					});
					aoData.push( {
						"name" : "filterValues",
						"value" : JSON.stringify(selectedValues)
					});
				},
				"bJQueryUI" : true,
				"bPaginate" : true,
				'iDisplayLength' : 100,
				"bLengthChange" : true,
				"bDestroy" : true,
				"bFilter" : true,
				"bSort" : true,
				"sScrollY" : "200px",
				"sScrollX" : "100%",
				"sScrollXInner" : "100%",
				"sDom" : 'RC<"clear">frtlip',
				"sPaginationType" : "full_numbers",
				"oColVis" : {
					"bShowAll" : true,
					"sShowAll" : "Select All"
				},
				"aoColumns" : fetchColumnProp(chgmodulename),
				"fnDrawCallback" : function() {
					$('#allcb').prop('checked', false);
					$('tbody tr td input[name="check"]').each(
							function() {
								var v = $(this).val().split(',');
								if (($.inArray(v[0], checkdAll) != -1)
										&& ($.inArray(v[1], ssLinkID) != -1)) {
									$(this).prop('checked', true);
									$('#allcb').prop('checked', true);
								}
							});
				}
			});
	$(
			'<select id="chgstat_dropdown" onchange="updateViewForChgStatAdvSrchSec(this.value);"><option value="1">Active </option><option value="2">Inactive </option><option value="3">Both </option></select>')
			.insertBefore('div.inner-addon input:first');//.insertBefore('#maincdmData_filter label');
	$(
			'<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>')
			.insertAfter('#maincdmData_filter');
	$(
			'<a href="#" onclick="showAddFilterform();" style ="float:left;font-weight:bold;margin-left:76%">Add Filters</a>')
			.insertAfter('#maincdmData_filter');
	if ($('#savedfilterset_area li').length != 0
			&& $("div.ColVis").not(":contains('Search results for')")) {
		$(
				'<div class="sersec"><label id="showfilter_indt" >Search results for : ' + filterName + '</label><a href="#" onclick="clearAllFiltersSec();" class= "clearall_grp" style="padding-left:10px"> CLEAR ALL</a></div>')
				.insertAfter('#maincdmData_filter');
		$('<div class="grpdata">' + $("#savedfilterset_area").html() + '</div>')
				.insertAfter('#maincdmData_filter');
	}
	$('.progress-indicator').css('display', 'none');
	$(".ColVis_MasterButton span").html("");
	initilizeCheckBox();
}

function advSrchForSerSetUpdate() {
	$('.progress-indicator').css('display', 'block');
	var filterName;
	if (filtername_check == "Saved") {
		filterName = $('#savedfilter').find(":selected").text();
	} else if (filtername_check == "Edit") {
		filterName = $("#savedval").val();
	} else {
		filterName = "";
	}
	$("#maincdmData").dataTable().fnDestroy();
	$("#maincdmData").empty();
	oTable = $("#maincdmData").dataTable(
			{
				"bProcessing" : false,
				"bServerSide" : true,
				"sAjaxSource" : "getsslinkdata.action",
				"fnServerParams" : function(aoData) {
					aoData.push( {
						"name" : "ssID",
						"value" : serSetId
					}), aoData.push( {
						"name" : "chgstat",
						"value" : chgstatval
					});
					aoData.push( {
						"name" : "advanceSearch",
						"value" : true
					});
					aoData.push( {
						"name" : "savedfilters",
						"value" : JSON.stringify(savedfiltercolvals)
					});
					aoData.push( {
						"name" : "FILE_ID",
						"value" : $("#cdmtype").val()
					});
					aoData.push( {
						"name" : "filterValues",
						"value" : JSON.stringify(selectedValues)
					});
				},
				"bJQueryUI" : true,
				"bPaginate" : true,
				'iDisplayLength' : 100,
				"bLengthChange" : true,
				"bDestroy" : true,
				"bFilter" : true,
				"bSort" : true,
				"sScrollY" : "200px",
				"sScrollX" : "100%",
				"sScrollXInner" : "100%",
				"sDom" : 'RC<"clear">frtlip',
				"sPaginationType" : "full_numbers",
				"oColVis" : {
					"bShowAll" : true,
					"sShowAll" : "Select All"
				},
				"aoColumns" : fetchColumnProp(chgmodulename),
				"fnDrawCallback" : function() {
					$('#allcb').prop('checked', false);
					$('tbody tr td input[name="check"]').each(
							function() {
								var v = $(this).val().split(',');
								if (($.inArray(v[0], checkdAll) != -1)
										&& ($.inArray(v[1], ssLinkID) != -1)) {
									$(this).prop('checked', true);
									$('#allcb').prop('checked', true);
								}
							});
				}
			});
	$(
			'<select id="chgstat_dropdown" onchange="updateViewForChgStatAdvSrchSet(this.value);"><option value="1">Active </option><option value="2">Inactive </option><option value="3">Both </option></select>')
			//.insertBefore('#maincdmData_filter label');
			.insertBefore('#maincdmData_filter label div.inner-addon input:first');
	$(
			'<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>')
			.insertAfter('#maincdmData_filter');
	$(
			'<a href="#" onclick="showAddFilterform();" style ="float:left;font-weight:bold;margin-left:76%">Add Filters</a>')
			.insertAfter('#maincdmData_filter');
	if ($('#savedfilterset_area li').length != 0
			&& $("div.ColVis").not(":contains('Search results for')")) {
		$(
				'<div class="serset"><label id="showfilter_indt" >Search results for : ' + filterName + '</label><a href="#" onclick="clearAllFiltersSet();" class= "clearall_grp" style="padding-left:10px"> CLEAR ALL</a></div>')
				.insertAfter('#maincdmData_filter');
		$(
				'<div class="setgrpdata">' + $("#savedfilterset_area").html() + '</div>')
				.insertAfter('#maincdmData_filter');
	}
	$('.progress-indicator').css('display', 'none');
	$(".ColVis_MasterButton span").html("");
	initilizeCheckBox();
}
function updateViewForChgStatAdvSrch(value) {
	$('.progress-indicator').css('display', 'block');
	chgstatval = value;
	checkdAll.length = 0;
	var newkeys = [];
	var newkeyvals = [];
	var newcdmkeys = [];
	var filterName;
	if (filtername_check == "Create") {
		filterName = $('#entertosave').val();
	} else if (filtername_check == "Saved") {

		filterName = $('#savedfilter').find(":selected").text();
	} else if (filtername_check == "Edit") {
		filterName = $("#savedval").val();
	} else {
		filterName = "";
	}
	$('.listview li').each(
			function() {
				if ($.inArray($(this).attr('id'), newcdmkeys) == -1) {
					newcdmkeys.push($(this).attr('id'));
				}
				if ($.inArray($(this).text().replace("X", "").trim(),
						newkeyvals) == -1) {
					newkeyvals.push($(this).text().replace("X", "").trim());
				}
				if ($.inArray($(this).attr('id')
						+ ":"
						+ $(this).text().substring(
								$(this).text().indexOf(":") + 1,
								$(this).text().indexOf("X") - 1).trim(),
						newkeys) == -1) {
					newkeys.push($(this).attr('id')
							+ ":"
							+ $(this).text().substring(
									$(this).text().indexOf(":") + 1,
									$(this).text().indexOf("X") - 1).trim());
				}
			});
	$("#maincdmData").dataTable().fnDestroy();
	$("#maincdmData").empty();
	oTable = $("#maincdmData").dataTable( {
		"bProcessing" : false,
		"bServerSide" : true,
		"sAjaxSource" : "getcdmsearchdata.action",
		"fnServerParams" : function(aoData) {
			aoData.push( {
				"name" : "chgtype",
				"value" : $("#chgtype").val()
			});
			aoData.push( {
				"name" : "preprvver",
				"value" : $("#preprvver").val()
			});
			aoData.push( {
				"name" : "advanceSearch",
				"value" : true
			});
			aoData.push( {
				"name" : "chgstat",
				"value" : chgstatval
			});
			aoData.push( {
				"name" : "FILE_ID",
				"value" : $("#cdmtype").val()
			});
			aoData.push( {
				"name" : "filterValues",
				"value" : JSON.stringify(newkeys)
			});
		},
		"bJQueryUI" : true,
		"bPaginate" : true,
		'iDisplayLength' : 100,
		"bLengthChange" : true,
		"bDestroy" : true,
		"bFilter" : true,
		"bSort" : true,
		"sScrollY" : "400px",
		"sScrollX" : "100%",
		"sScrollXInner" : "100%",
		"sDom" : 'RC<"clear">frtlip',
		"sPaginationType" : "full_numbers",
		"oColVis" : {
			"bShowAll" : true,
			"sShowAll" : "Select All"
		},
		"aoColumns" : fetchColumnProp(chgmodulename),
		"fnDrawCallback" : function() {
			$('#allcb').prop('checked', false);
			$('tbody tr td input[name="check"]').each(function() {
				if ($.inArray($(this).val(), checkdAll) != -1) {
					$(this).prop('checked', true);
					$('#allcb').prop('checked', true);
				}
			});
		}
	});
	$(
			'<select id="chgstat_dropdown" onchange="updateViewForChgStatAdvSrch(this.value);"><option value="1">Active </option><option value="2">Inactive </option><option value="3">Both </option></select>')
			.insertBefore('div.inner-addon input:first');//.insertBefore('#maincdmData_filter label');
	$(
			'<a class="addFilter" href="#" onclick="showAddFilterform();">Add Filters</a>')
			.insertAfter('#maincdmData_filter');
	$(
			'<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>')
			.insertAfter('#maincdmData_filter');
	$(
			'<div class="datablock" ><label id="showfilter_indt" >Search results for : ' + filterName + '</label></div>')
			.insertAfter('div.ColVis');
	$('.datablock')
			.append(
					'<div id="clearall_div"><a href="#" onclick="clearAllFiltersList();" class= "clearall_list1" style="padding-left:10px"> CLEAR ALL</a></div>');
	$('<div class="listview"></div>').insertAfter('div.ColVis');
	for (j = 0, i = 0; i < newkeyvals.length, j < newcdmkeys.length; i++, j++) {
		$('.listview').append(
				'<li class= "li_style" id ="' + newcdmkeys[i] + '">'
						+ newkeyvals[i] + '   '
						+ '<a class = "remove_li"> X </a>' + '</li>');
	}
	$('.progress-indicator').css('display', 'none');
	$(".ColVis_MasterButton span").html("");
	initilizeCheckBox();
	$("#chgstat_dropdown").val(chgstatval);
}

function updateViewForChgStatAdvSrchSet(value) {
	$('.progress-indicator').css('display', 'block');
	chgstatval = value;
	checkdAll.length = 0;
	var newkeys = [];
	var newkeyvals = [];
	var newcdmkeys = [];
	var filterName;
	if (filtername_check == "Create") {
		filterName = $('#entertosave').val();
	} else if (filtername_check == "Saved") {

		filterName = $('#savedfilter').find(":selected").text();
	} else if (filtername_check == "Edit") {
		filterName = $("#savedval").val();
	} else {
		filterName = "";
	}
	$('.setgrpdata li').each(
			function() {
				if ($.inArray($(this).attr('id'), newcdmkeys) == -1) {
					newcdmkeys.push($(this).attr('id'));
				}
				if ($.inArray($(this).text().replace("X", "").trim(),
						newkeyvals) == -1) {
					newkeyvals.push($(this).text().replace("X", "").trim());
				}
				if ($.inArray($(this).attr('id')
						+ ":"
						+ $(this).text().substring(
								$(this).text().indexOf(":") + 1,
								$(this).text().indexOf("X") - 1).trim(),
						newkeys) == -1) {
					newkeys.push($(this).attr('id')
							+ ":"
							+ $(this).text().substring(
									$(this).text().indexOf(":") + 1,
									$(this).text().indexOf("X") - 1).trim());
				}
			});
	$("#maincdmData").dataTable().fnDestroy();
	$("#maincdmData").empty();
	oTable = $("#maincdmData").dataTable( {
		"bProcessing" : false,
		"bServerSide" : true,
		"sAjaxSource" : "getsslinkdata.action",
		"fnServerParams" : function(aoData) {
			aoData.push( {
				"name" : "ssID",
				"value" : serSetId
			}), aoData.push( {
				"name" : "chgtype",
				"value" : $("#chgtype").val()
			});
			aoData.push( {
				"name" : "preprvver",
				"value" : $("#preprvver").val()
			});
			aoData.push( {
				"name" : "advanceSearch",
				"value" : true
			});
			aoData.push( {
				"name" : "chgstat",
				"value" : chgstatval
			});
			aoData.push( {
				"name" : "FILE_ID",
				"value" : $("#cdmtype").val()
			});
			aoData.push( {
				"name" : "filterValues",
				"value" : JSON.stringify(newkeys)
			});
		},
		"bJQueryUI" : true,
		"bPaginate" : true,
		'iDisplayLength' : 100,
		"bLengthChange" : true,
		"bDestroy" : true,
		"bFilter" : true,
		"bSort" : true,
		"sScrollY" : "200px",
		"sScrollX" : "100%",
		"sScrollXInner" : "100%",
		"sDom" : 'RC<"clear">frtlip',
		"sPaginationType" : "full_numbers",
		"oColVis" : {
			"bShowAll" : true,
			"sShowAll" : "Select All"
		},
		"aoColumns" : fetchColumnProp(chgmodulename),
		"fnDrawCallback" : function() {
			$('#allcb').prop('checked', false);
			$('tbody tr td input[name="check"]').each(function() {
				if ($.inArray($(this).val(), checkdAll) != -1) {
					$(this).prop('checked', true);
					$('#allcb').prop('checked', true);
				}
			});
		}
	});
	$(
			'<select id="chgstat_dropdown" onchange="updateViewForChgStatAdvSrchSet(this.value);"><option value="1">Active  </option><option value="2">Inactive </option><option value="3">Both </option></select>')
			.insertBefore('#maincdmData_filter label div.inner-addon input:first');//.insertBefore('#maincdmData_filter label');
	$(
			'<a class="addFilter" href="#" onclick="showAddFilterform();">Add Filters</a>')
			.insertAfter('#maincdmData_filter');
	$(
			'<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>')
			.insertAfter('#maincdmData_filter');
	$(
			'<div class="serset"><label id="showfilter_indt" >Search results for : ' + filterName + '</label><a href="#" onclick="clearAllFiltersSet();" class= "clearall_grp" style="padding-left:10px"> CLEAR ALL</a></div>')
			.insertAfter('#maincdmData_filter');
	$('<div class="setgrpdata"></div>').insertAfter('#maincdmData_filter');
	for (j = 0, i = 0; i < newkeyvals.length, j < newcdmkeys.length; i++, j++) {
		$('.setgrpdata').append(
				'<li class= "li_style" id ="' + newcdmkeys[i] + '">'
						+ newkeyvals[i] + '   '
						+ '<a class = "remove_li"> X </a>' + '</li>');
	}
	$('.progress-indicator').css('display', 'none');
	$(".ColVis_MasterButton span").html("");
	initilizeCheckBox();
	$("#chgstat_dropdown").val(chgstatval);
}

function updateViewForChgStatAdvSrchSec(value) {
	$('.progress-indicator').css('display', 'block');
	chgstatval = value;
	checkdAll.length = 0;
	var newkeys = [];
	var newkeyvals = [];
	var newcdmkeys = [];
	var filterName;
	if (filtername_check == "Create") {
		filterName = $('#entertosave').val();
	} else if (filtername_check == "Saved") {

		filterName = $('#savedfilter').find(":selected").text();
	} else if (filtername_check == "Edit") {
		filterName = $("#savedval").val();
	} else {
		filterName = "";
	}
	$('.grpdata li').each(
			function() {
				if ($.inArray($(this).attr('id'), newcdmkeys) == -1) {
					newcdmkeys.push($(this).attr('id'));
				}
				if ($.inArray($(this).text().replace("X", "").trim(),
						newkeyvals) == -1) {
					newkeyvals.push($(this).text().replace("X", "").trim());
				}
				if ($.inArray($(this).attr('id')
						+ ":"
						+ $(this).text().substring(
								$(this).text().indexOf(":") + 1,
								$(this).text().indexOf("X") - 1).trim(),
						newkeys) == -1) {
					newkeys.push($(this).attr('id')
							+ ":"
							+ $(this).text().substring(
									$(this).text().indexOf(":") + 1,
									$(this).text().indexOf("X") - 1).trim());
				}
			});
	$("#maincdmData").dataTable().fnDestroy();
	$("#maincdmData").empty();
	oTable = $("#maincdmData").dataTable( {
		"bProcessing" : false,
		"bServerSide" : true,
		"sAjaxSource" : "getcdmsearchgrpdata.action",
		"fnServerParams" : function(aoData) {
			aoData.push( {
				"name" : "grpID",
				"value" : editServSecId
			}), aoData.push( {
				"name" : "chgtype",
				"value" : $("#chgtype").val()
			});
			aoData.push( {
				"name" : "preprvver",
				"value" : $("#preprvver").val()
			});
			aoData.push( {
				"name" : "advanceSearch",
				"value" : true
			});
			aoData.push( {
				"name" : "chgstat",
				"value" : chgstatval
			});
			aoData.push( {
				"name" : "FILE_ID",
				"value" : $("#cdmtype").val()
			});
			aoData.push( {
				"name" : "filterValues",
				"value" : JSON.stringify(newkeys)
			});
		},
		"bJQueryUI" : true,
		"bPaginate" : true,
		'iDisplayLength' : 100,
		"bLengthChange" : true,
		"bDestroy" : true,
		"bFilter" : true,
		"bSort" : true,
		"sScrollY" : "200px",
		"sScrollX" : "100%",
		"sScrollXInner" : "100%",
		"sDom" : 'RC<"clear">frtlip',
		"sPaginationType" : "full_numbers",
		"oColVis" : {
			"bShowAll" : true,
			"sShowAll" : "Select All"
		},
		"aoColumns" : fetchColumnProp(chgmodulename),
		"fnDrawCallback" : function() {
			$('#allcb').prop('checked', false);
			$('tbody tr td input[name="check"]').each(function() {
				if ($.inArray($(this).val(), checkdAll) != -1) {
					$(this).prop('checked', true);
					$('#allcb').prop('checked', true);
				}
			});
		}
	});
	$(
			'<select id="chgstat_dropdown" onchange="updateViewForChgStatAdvSrchSec(this.value);" ><option value="1">Active </option><option value="2">Inactive </option><option value="3">Both </option></select>')
			.insertBefore('div.inner-addon input:first');//.insertBefore('#maincdmData_filter label');
	$(
			'<a class="addFilter" href="#" onclick="showAddFilterform();">Add Filters</a>')
			.insertAfter('#maincdmData_filter');
	$(
			'<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>')
			.insertAfter('#maincdmData_filter');
	$(
			'<div class="sersec"><label id="showfilter_indt" >Search results for : ' + filterName + '</label><a href="#" onclick="clearAllFiltersSec();" class= "clearall_grp" style="padding-left:10px"> CLEAR ALL</a></div>')
			.insertAfter('#maincdmData_filter');
	$('<div class="grpdata"></div>').insertAfter('#maincdmData_filter');
	for (j = 0, i = 0; i < newkeyvals.length, j < newcdmkeys.length; i++, j++) {
		$('.grpdata').append(
				'<li class= "li_style" id ="' + newcdmkeys[i] + '">'
						+ newkeyvals[i] + '   '
						+ '<a class = "remove_li"> X </a>' + '</li></div>');
	}
	$('.progress-indicator').css('display', 'none');
	$(".ColVis_MasterButton span").html("");
	initilizeCheckBox();
	$("#chgstat_dropdown").val(chgstatval);
}
