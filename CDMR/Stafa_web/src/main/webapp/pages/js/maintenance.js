var codelstparam = ["subType","description","pageUrl","isHide","maintainable","pkCodelst","type","defaultValue","codelstvalue"];
$(document).ready(function() {
	$("#divcon").hide();
});
jQuery.expr[':'].contains = function(a, i, m) { 
	  return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0; 
	};
	function fn_filtertext(maintIndex){
		$('#filterText'+maintIndex).keyup(function(eve){
			
			var val = $(this).val();

			if(val==""){
				$('li').show();
			}else{
				$('.filtercontent'+maintIndex+ ' li').hide();
				$('.filtercontent'+maintIndex+' li:first').show();
				$(".filtercontent"+maintIndex+" li:contains('"+val+"')").show();
			}
		});
	}
	
$(function() {
	
	
	$( ".column" ).sortable({
		connectWith: ".column",
	    cursor: 'move',
	    cancel:	".portlet-content"
			
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open');
	});

	/**For Notes**/
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='ui-addnew'></span>");

	$( ".portlet-header .ui-addnew" ).click(function() {
		
		showPopUp("open","addNewDialog","Add New Acquision","900","350")
	});
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open');
	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$( ".portlet" ).find( ".maintaddnew" ).append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;width:5px;\" class='ui-addnew'></span>");
	$( ".portlet .maintaddnew .ui-addnew" ).click(function() {clearvalue();
		$("#tempisDelete").removeAttr("checked");
		
		var tableId = $(this).parent().find("#callistTable").val();
		$("#calList").val(tableId);
		
		if(tableId=="table-1")
		{
			var callType = $("#calListTempType1").val();
			$("#type").val(callType);
		}
		else if(tableId=="table-2")
		{
			var callType = $("#calListTempType2").val();
			$("#type").val(callType);
		}
		
		showPopUp("open","maintpopup","Maintenance : "+$("#portletsubtitlediv").html(),"900","350");
		hideAutoCodelst();
		
	});
	
});
function hideAutoCodelst(){
	$("#autocontentdiv").addClass("portlethidden");
}
function loadAutoCodelstData(pagetype){
	var constructHTML = '<table id="autocontenttable">';
	var linkvar = "";
	var paramStr = "";
	var position;
	if(pagetype != null && pagetype == 'subtype'){
		paramStr = $("#subType").val();
		position = $("#subType").position();
	}else{
		paramStr = $("#description").val();
		position = $("#description").position();
	}
	var autocontentObj = document.getElementById("autocontentdiv");
	$("#autocontentdiv").addClass("portlethidden");
	if(paramStr != null && paramStr != ""){
		$.ajax({
	        type: "GET",
	        url : "loadAutoCodelstData.action?paramStr="+paramStr+"&pagetype="+pagetype,
	        async:false, 
	        dataType: 'json',
	
	        success: function (result){
	        	var treelst = result.mainTreelst;
	        	
	        	if(treelst != null && treelst.length != null){
	        		$("#autocontentdiv").removeClass("portlethidden");
	        		$("#autocontentdiv").css( { 
	                    position: 'absolute',
	                    zIndex: 5000,
	                    left: position.left, 
	                    top: position.top+30
	        		} );

	        	}	
	        	$.each(treelst, function(i) {
	            	if(pagetype != null && pagetype == 'subtype'){
	            		linkvar = 'javascript:setSubType("'+this.subType+'","'+this.codelstvalue+'")';	
	                }else{
	            		linkvar = 'javascript:setDescription("'+this.description+'")';
	                }	
	        		constructHTML += "<tr><td><input type='radio' id='autocontent' name='autocontent' onClick='"+linkvar+"' /></td><td>";
	        		if(pagetype != null && pagetype == 'subtype'){
	        			constructHTML += this.subType; 
	        			if(this.codelstvalue != null && this.codelstvalue != "null"){
	        				constructHTML += " : " + this.codelstvalue;
	        			}
	        		}else{
	        			constructHTML += this.description
	            	}	
	            	constructHTML += "</td></tr>";
	        	});	
	        	constructHTML += '</table>';
	        	$("#autocontenttable").html(constructHTML);
	        }
		});	
	}
}
function setDescription(desc){
	$("#description").val(desc);
}
function setSubType(subtype, value){
	$("#subType").val(subtype);
	if(value != null && value != "null"){
		$("#codelstvalue").val(value);
	}	
}


function checkQcValidation(obj){
	 if($("#fkCodelstProtocol").val() == ""){
		 	alert("Protocol is required.");
		 	$(obj).removeAttr("checked");
		 	return false;
	 }
	 if($("#fkCodelstSubheader").val() == ""){
		 	alert("Subheader is required.");
		 	$(obj).removeAttr("checked");
		 	return false;
	 }
	 return true;
}
function visibleReagents(){
	if($("#qcpkCodelst").val() != null && $("#qcpkCodelst").val() != ""){
		document.getElementById("reagentdiv").style.visibility="visible";
		
			
	}	
}

function loadqcdetails(object, codelstId, subtype, description,formId){
	//Temporary store exists reagent and suppliers data for the qc
	if($("#qcpkCodelst").val() != null && $("#qcpkCodelst").val() != ""){
		var reagentquantity = $("#tempqcreagent").val();
		reagentquantity += $("#qcpkCodelst").val()+"::";
		var reagentobj = document.getElementsByName("selreagentid");
		var quantityobj = document.getElementsByName("quantity");
		$(reagentobj).each(function (i){
			reagentquantity += reagentobj[i].value + ":" + quantityobj[i].value+",";
		});
		reagentquantity += ";";
		$("#tempqcreagent").val(reagentquantity);
	}
	var constructHTML = "<table><tr><th>Reagents and Supplies</th><th>Quantity</th></tr>";
	var reagentid = "";
	var raagentdesc = "";
	var reagentObj = document.getElementsByName("fkcodelstreagent");
	var reagentdescobj = document.getElementsByName("codelstreagentdesc");;
	var $tr = $(object).closest('tr');
	var rowIndex = $tr.index();
	
	var primaryobj = document.getElementsByName("qcPrmariyId");
	
	
	
	$("#qcpkCodelst").val(codelstId);
	$("#qcsubType").val(subtype);
	$("#qcdescription").val(description);
	$("#qc").val(primaryobj[rowIndex].value);
	var reagentquantity = "";
	var ajaxresult = ajaxCall('getQcMaintDetails',formId);
	
	//Clear
	var reagentidObj = document.getElementsByName("reagentid");
	
	for(i=0;i<reagentidObj.length;i++){
		reagentidObj[i].checked= false;
	}
	$("#reagentquantitytable").html('<table cellpadding="3" cellspacing="0" align="left" width="100%" id="reagentquantitytable">'+
	'<tr><th>Reagents and Supplies</th><th>Quantity</th></table>');
	
	
	
	if(ajaxresult.exisReagentlst != null){
		$.each(ajaxresult.exisReagentlst, function(i) {
			 $.each(this, function(key, value){
				 jQuery(' input[type="checkbox"]'+'[value="'+value+'"]').each(function() {
			            $(this).attr("checked","true");
			      });
				 if(key == "fkCodelstReagent"){
		            	reagentid = value;
		            	$(reagentObj).each(function (i){
		            		if(reagentObj[i].value == value){
		            			raagentdesc = reagentdescobj[i].value;
		            		}
		            	});
		           }
				 if(key == "qunatity"){
					 reagentquantity = value;
				 }
				 //
				 
			 });
			 
			 //
	
			 constructHTML += "<tr id='rowid1001"+reagentid+"'><td><input type='hidden' id='reagentid' name='selreagentid' value='"+reagentid+"'/>"+raagentdesc + "</td>";
			 constructHTML += "<td><input type='hidden' id='tempquantity"+i+"' value='"+reagentquantity+"'><input type='text' id='quantity"+i+"' name='quantity' class='smartspinner' value='"+reagentquantity+"'/>";
	
			$("#reagentquantitytable").html(constructHTML);
			
			 
		 }); 
		$.each(ajaxresult.exisReagentlst, function(i) {
			$('#quantity'+i).spinit();
			$('#quantity'+i).val($('#tempquantity'+i).val());
		});
	}
}

function newReagentSuppliers(){
	$("#reagentpkCodelst").val("");
	$("#reagentsubType").val("");
	$("#reagentdescription").val("");
	$("#reagentmessagetd").html("");
	$("#reagenttempdefault").removeAttr("checked");
	$("#reagentisHide").removeAttr("checked");
	
}

function showMaintPopUp(mode,Id,title,wd,ht){
	$("#"+Id).dialog({
		   autoOpen: false,
		   title: title,
		   modal: true, width:wd, height:ht,
		   close: function() {
	    		jQuery("#"+Id).dialog("destroy");
	    		
		   }
		});
	$("#"+Id).dialog(mode);
}

function clearvalue(){
	$("#pkCodelst").val("");
	$("#subType").val("");
	$("#description").val("");
	
	$("#messagetd").html("");
	$("#codelstvalue").val("");
	$("#comments").val("");
	$("#tempisHide").removeAttr("checked");
	$("#tempdefault").removeAttr("checked");
	$("#isHide").removeAttr("checked");
	$("#subType").removeAttr("readonly");
	$("#portletcontentdiv").html("");
	$("#reagentmessagetd").html("");
	$("#reagentpkCodelst").val("");
	$("#reagentsubType").val("");
	$("#reagentdescription").val("");
	$("#reagenttempdefault").removeAttr("checked");
	$("#reagentisHide").removeAttr("checked");
}
function saveCodelst(pagetype){
	
	
	if($("#tempisHide").is(':checked')){
		$("#isHide").val("Y");	
	}else{
		$("#isHide").val("N");
	}
	
	if($("#tempdefault").is(':checked')){
		$("#defaultValue").val("1");
		
	}else{
		$("#defaultValue").val("0");
	}
	
	if($("#tempisDelete").is(':checked')){
		$("#deletedFlag").val("1");	
	}else{
		$("#deletedFlag").val("0");
	}
	//alert($("#deletedFlag").val());
	var myTbl = document.getElementById('table-1');
	var rows = myTbl.getElementsByTagName('TR');
	var seqarr = "";
	var seqobj = document.getElementsByName("seq");
	var codelstidobj = document.getElementsByName("codelstid");
	//alert("seqobj : "+seqobj+"=="+seqobj.length);
	if(seqobj!=null && seqobj.length>0){
		for(i=0;i<rows.length-1;i++){
			if((i+1) != seqobj[i].value){
				seqarr += ";" + codelstidobj[i].value + ":" + (i+1);
			}
	
		}
	}
	if(pagetype == "seq"){
		if(seqarr != ""){
			var answer = confirm ("Do you want to change the Order of Sequence?");
			if(!answer){
				return false;
			}
			$("#seqarray").val(seqarr);
		}else{
			return false;
		}
	}else{
		if($("#subType").val() == ""){
			alert("Code is required");
			return false;
		}
		if($("#description").val() == ""){
			alert("Name is required");
			return false;
		}
	}
	var ajaxresult = ajaxCall('savemaintDetails','maintenanceform');
	$("#pkCodelst").val(ajaxresult.codelstId);
	$("#messagetd").html(ajaxresult.returnMessage);
	
	var tableVal=$("#calList").val();
	
	if(tableVal=="table-1")
		{
		loadsubtree($("#type").val(),"N");
		
		}
	else if(tableVal=="table-2")
		{
		loadcalist($("#type").val(),"N");
		
		}
	
	
	
	
	showPopUp("close","maintpopup","Maintenance : "+$("#portletsubtitlediv").html(),"900","350");
}
function saveNewReagentSuppliers(){
		
		
		if($("#reagenttempisHide").is(':checked')){
			$("#reagentisHide").val("Y");	
		}else{
			$("#reagentisHide").val("N");
		}
		
		if($("#reagenttempdefault").is(':checked')){
			$("#reagentdefaultValue").val("1");
			
		}else{
			$("#reagentdefaultValue").val("0");
		}

		
		if($("#reagentsubType").val() == ""){
			alert("Code is required");
			return false;
		}
		if($("#reagentdescription").val() == ""){
			alert("Name is required");
			return false;
		}
		$('.progress-indicator').css( 'display', 'block' );
		var ajaxresult = ajaxCall('savemaintDetails','reagentsNewForm');
		$("#reagentmessagetd").html(ajaxresult.returnMessage);
		//$("#addNewDialog").dialog("close");
		//$("#reagentpkCodelst").val(ajaxresult.codelstId);
		//$("#reagentpkCodelst").val("");
		//$("#reagentsubType").val("");
		//$("#reagentdescription").val("");
		//$("#reagentmessagetd").html("");
		//$("#reagenttempdefault").removeAttr("checked");
		//$("#reagentisHide").removeAttr("checked");
		loadlink("loadqcmaintenance","refresh");
		 $('.progress-indicator').css( 'display', 'none' );
	
}

function loadlink(pageurl,pagetype){
	$.ajax({
        type: "GET",
        url : pageurl,
        async:false, 
        success: function (result){
        	$("#qcmaintdiv").replaceWith($('#qcmaintdiv', $(result)));
        	/*//issued fixed for adding extra qcdiv in panel config page in qc option
        	$("#qcmaintdiv1").replaceWith($('#qcmaintdiv1', $(result)));
        	portletInit();*/
        	if($("#reagenttype").val() == 'prodtabs' || $("#reagenttype").val() == 'planprotocol'){
        		$("#qcheadertable").replaceWith($('#qcheadertable',$(result)));
        	}
        	if(pagetype == null || pagetype != "refresh"){
        		portletInit();
        	}else{
        		$("#qcheadertable").replaceWith($('#qcheadertable', $(result)));
        		$(".portlet .protocoladdnew").click(function() {
        			$("#reagenttype").val("prodprotocol");
        			showPopUp("open","addNewDialog","Protocol","450","350")
        		});
        		$( ".portlet" ).find( ".protocoladdnew" )
        		.append("<span style=\"float:left;\" class='ui-addnew'></span>");
        		autoCompleteCall();
        	}	
        }
	});
	
}
function loadQcPanel(mainPage, subPage){
	$('.progress-indicator').css( 'display', 'block' );
	$.ajax({
        type: "GET",
        url : pageurl,
        async:false, 
        success: function (result){
        	$('#main').load(pageUrl, function(){
        		$('#qcmaintdiv').html(result);	
        	});
        }
	});
	 $('.progress-indicator').css( 'display', 'none' );
}

function calldialog(){
//	alert("call dialog" + $("#qcmaintdiv").html());
	
	$("#qcmaintdiv").dialog({
		// title : qcDesc,
		 width : 1500,
	 	height : 650,
	 	autoOpen: true,
		modal: true,
		close: function() {
		jQuery("#qcmaintdiv").dialog("destroy");
		}
	 });
}
function loadsubdrop(maintIndex,treelength){
	for(var i=0;i<treelength;i++){
		$(".filtercontent"+i).css("display","none");
	}
	$(".filtercontent"+maintIndex).css("display","block");
}
function loadmaintree(parenttype){
	var treekey = "";
	var treevalue = "";
	var pageUrl = "";
	var subtreekey = "";
	var subtreevalue = "";
	var subpageUrl = "";
	var constructHTML = "";
	var subConstructHTML = "";	
	if(parenttype == null || parenttype == ""){
		parenttype = "stafa";
	}
	//var loadflag = loadPage('html/stafa/maintenance.jsp');
	//
	//if(loadflag){
		$('.progress-indicator').css( 'display', 'block' );
		 $.ajax({
		        type: "GET",
		        url : "loadMaintenance.action?type="+parenttype,
		        async:false, 
		        dataType: 'json',
	
		        success: function (result){
		        	var treelst = result.mainTreelst;
		        	constructHTML = '<ul id="navmenu">';
		        	var treesize = 0;
		        	if(treelst != null){
		        		treesize = treelst.length;
		        	}
		        	$.each(result.mainTreelst, function(maintIndex) { 
		        		$.each(this,function (i){
		        			if(i == 0){
				        		$.each(this, function(key, value){
									if(key == codelstparam[0]){
										treekey = value;
									}
									if(key == codelstparam[1]){
										treevalue = value;
									}
									if(key == codelstparam[2]){
										pageUrl = value;
									}
									
				        		});
		        			}
		        			//alert("i > "+i);
						if(i == 1){
							subConstructHTML = "";
							//Sub Module 
							if(this.length > 0 ) {
								subConstructHTML += "<ul class='filtercontent"+maintIndex+"' style='height: 400px; overflow-x: hidden;margin-top:25px;'>";
								
								subConstructHTML += '<li><a href="#"><div><input class="scanboxSearch" style="width:175px;" id="filterText'+maintIndex+ '" type="text" size="15" value="" name="search" placeholder=""/><br/></div></a></li>';
								$.each(this,function(){
									var subtreelink = 'javascript:loadsubtree("'+eval("this."+codelstparam[0])+'","Y","'+eval("this."+codelstparam[1])+'");';
									subtreelink = "onclick = '"+subtreelink + "'";
									//alert("subtreelink ===> "+subtreelink);
									subConstructHTML += '<li><a href="#" '+subtreelink +'>'+eval("this."+codelstparam[1])+'</a>&nbsp;&nbsp;';
									if(subpageUrl != null && subpageUrl != ""){
					        			var subpagelink = 'loadlink("'+subpageUrl+'");';
					        			subpagelink = "onclick = '" + subpagelink + "'";
					        			//subConstructHTML += "<a href='#' "+subpagelink+">Link</a>";
					        			$("#pageurl").val(eval("this."+codelstparam[2]));
					        			
					        		}	
									subConstructHTML += '</li>';
									
								});
								subConstructHTML += "</ul>";
							}
						}	
		        		});	
		        		var treelink = 'javascript:loadsubtree("'+treekey+'","Y","'+treevalue+'");';
		        		treelink = "onclick = '"+treelink + "'";
					constructHTML += '<li>';
					if(subConstructHTML == ""){
						constructHTML += '<a href="#" '+treelink+'>'+treevalue + '</a>';
					}else{
						treelink = 'loadsubdrop("'+maintIndex+'","'+treesize+'")';
						treelink = "<a href='#' onclick='" + treelink + "' > ";
						constructHTML += treelink+treevalue + '</a>';
					}

		        		if(pageUrl != null && pageUrl != ""){
		        			var pagelink = 'loadlink("'+pageUrl+'");';
		        			pagelink = "onclick = '" + pagelink + "'";
		        			//constructHTML += "<a href='#' "+pagelink+">Link</a>";
		        			$("#pageurl").val(pageUrl);
		        			
		        		}	
					constructHTML += subConstructHTML + '</li>';			        		
		        				        		
				});
		        	constructHTML += "</ul>";
		        	$("#navmenu").replaceWith(constructHTML);
		        	clearvalue();
		        	$.each(result.mainTreelst, function(maintIndex) {
		        		fn_filtertext(maintIndex);
		        	});
		        	// 
		        }
	 
		 });
	 
		 $('.progress-indicator').css( 'display', 'none' );
		
}


function loadcalist(paramtype,clearDataFlag,paramdesc){
	$("#divcon").show();
	var id = "";
	var type = "";
	var subtype = "";
	var desc = "";
	var ishide = "";
	var maintenable= "";
	var defaultvalue = "";
	var constructHTML = "";
	$("#callisttitle").html(paramdesc);
	$('.progress-indicator').css( 'display', 'block' );
	
	 $.ajax({
	        type: "GET",
	        url : "loadMaintenance.action?type="+paramtype,
	        async:false, 
	        dataType: 'json',

	        success: function (result){
	        	var treelst = result.mainTreelst;
	        	$('#table-2_wrapper').remove();
	        	$("#subtablemaint1").append('<table id="table-2" cellspacing="0" cellpadding="2"></table>');
	        	constructHTML = "<table width='100%' border='0' id='table-2' class='display'>";
	        	constructHTML += "<thead><tr><th align='left'>Display</th><th align='left'>Code</th><th align='left'>Sequence</th><th align='left'>Default</th><th align='left'>DeActivate</th><th align='left'>Non Maintainable</th></tr></thead><tbody>";
	        	//alert("paramtype : "+paramtype);
	        	$.each(result.mainTreelst, function(i) {
	        		paramtype = eval("this."+codelstparam[6]);
	        		//alert("paramtype ==>: "+paramtype);
					var maintlink = 'javascript:loadcalist("'+eval("this."+codelstparam[0])+'","Y","'+eval("this."+codelstparam[1])+'");';
	        		constructHTML += "<tr id="+i+" >";
	        		constructHTML += "<td><a href='#' onclick='"+maintlink+"'>"+eval("this."+codelstparam[1])+"</a></td><td>"+eval("this."+codelstparam[0])+"</td>";
	        		constructHTML += "<td><input type='hidden' id='codelstid' name='codelstid' value='"+eval("this."+codelstparam[5])+"'/><input type='hidden' id='seq' name='seq' value='"+(i+1)+"'/>"+(i+1)+"</td>";
	        		//IS Default
	        		if(eval("this."+codelstparam[7]) == "1"){
	        			constructHTML += "<td>Yes</td>";
	        		}else{
	        			constructHTML += "<td>No</td>";
	        		}
	        		//DeActive
	        		if(eval("this."+codelstparam[3]) == "Y"){
	        			constructHTML += "<td>Yes</td>";
	        		}else{
	        			constructHTML += "<td>No</td>";
	        		}
	        		
				if(maintenable == 1){
					constructHTML += "<td>Yes</td>";
				}else{
					constructHTML += "<td>No</td>";
				}
				
	        		constructHTML += "</tr>";
	        	});
	        	$("#calListTempType2").val(paramtype);
	        	constructHTML += "</tbody> </table>";
	        	$("#table-2").replaceWith(constructHTML);
	        	$("#type").val(paramtype);
	        	if(clearDataFlag == "Y"){
	        		clearvalue();
	        	}	
	        }
	
	 });
	 $('.progress-indicator').css( 'display', 'none' );
				 
	 // Make a nice striped effect on the table
	//$('#table-1 tr:even').addClass('alt');

	oTable = $('#table-2').dataTable({
		"bJQueryUI": true,
		"aaSorting": [[ 2, "asc" ]]
	});
	
	$('.tbody tr').click(function(){
		  changeClass(this);
	});
	
	// Initialise the second table specifying a dragClass and an onDrop function that will display an alert
	$("#table-2").tableDnD({
	    onDragClass: "myDragClass",
	    onDrop: function(table, row) {
            var rows = table.tBodies[0].rows;
            var debugStr = "Row dropped was "+row.id+". New order: ";
            for (var i=0; i<rows.length; i++) {
                debugStr += rows[i].id+" ";
            }
	       // $(#debugArea).html(debugStr);
	    },
		onDragStart: function(table, row) {
			//$(#debugArea).html("Started dragging row "+row.id);
		}
	});
	//alert("s");
	$("select").uniform();
}



function loadsubtree(paramtype,clearDataFlag,paramdesc){
	//alert("sssss");
	$('#divcon').hide();
	var id = "";
	var type = "";
	var subtype = "";
	var desc = "";
	var ishide = "";
	var maintenable= "";
	var defaultvalue = "";
	var constructHTML = "";
	$("#portletsubtitlediv").html(paramdesc);
	$('.progress-indicator').css( 'display', 'block' );
	
	 $.ajax({
	        type: "GET",
	        url : "loadMaintenance.action?type="+paramtype,
	        async:false, 
	        dataType: 'json',

	        success: function (result){
	        	var treelst = result.mainTreelst;
	        	$('#table-1_wrapper').remove();
	        	$("#subtablemaint").append('<table id="table-1" cellspacing="0" cellpadding="2"></table>');
	        	constructHTML = "<table width='100%' border='0' id='table-1' class='display'>";
	        	constructHTML += "<thead><tr><th align='left'>Display</th><th align='left'>Code</th><th align='left'>Sequence</th><th align='left'>Default</th><th align='left'>DeActivate</th><th align='left'>Non Maintainable</th></tr></thead><tbody>";
	        	$.each(result.mainTreelst, function(i) { 

					var maintlink = 'javascript:loadcalist("'+eval("this."+codelstparam[0])+'","Y","'+eval("this."+codelstparam[1])+'");';
	        		constructHTML += "<tr id="+i+" >";
	        		constructHTML += "<td><a href='#' onclick='"+maintlink+"'>"+eval("this."+codelstparam[1])+"</a></td><td>"+eval("this."+codelstparam[0])+"</td>";
	        		constructHTML += "<td><input type='hidden' id='codelstid' name='codelstid' value='"+eval("this."+codelstparam[5])+"'/><input type='hidden' id='seq' name='seq' value='"+(i+1)+"'/>"+(i+1)+"</td>";
	        		//IS Default
	        		if(eval("this."+codelstparam[7]) == "1"){
	        			constructHTML += "<td>Yes</td>";
	        		}else{
	        			constructHTML += "<td>No</td>";
	        		}
	        		//DeActive
	        		if(eval("this."+codelstparam[3]) == "Y"){
	        			constructHTML += "<td>Yes</td>";
	        		}else{
	        			constructHTML += "<td>No</td>";
	        		}
	        		
				if(maintenable == 1){
					constructHTML += "<td>Yes</td>";
				}else{
					constructHTML += "<td>No</td>";
				}
				
	        		constructHTML += "</tr>";
	        	});
	        	$("#calListTempType1").val(paramtype);
	        	constructHTML += "</tbody> </table>";
	        	$("#table-1").replaceWith(constructHTML);
	        	$("#type").val(paramtype);
	        	if(clearDataFlag == "Y"){
	        		clearvalue();
	        	}	
	        }
	
	 });
	 $('.progress-indicator').css( 'display', 'none' );
				 
	 // Make a nice striped effect on the table
	//$('#table-1 tr:even').addClass('alt');

	oTable = $('#table-1').dataTable({
		"bJQueryUI": true,
		"aaSorting": [[ 2, "asc" ]]
	});
	
	$('.tbody tr').click(function(){
		  changeClass(this);
	});
	
	// Initialise the second table specifying a dragClass and an onDrop function that will display an alert
	$("#table-1").tableDnD({
	    onDragClass: "myDragClass",
	    onDrop: function(table, row) {
            var rows = table.tBodies[0].rows;
            var debugStr = "Row dropped was "+row.id+". New order: ";
            for (var i=0; i<rows.length; i++) {
                debugStr += rows[i].id+" ";
            }
	       // $(#debugArea).html(debugStr);
	    },
		onDragStart: function(table, row) {
			//$(#debugArea).html("Started dragging row "+row.id);
		}
	});
	//alert("s");
	$("select").uniform();
	
}

/*function loadcallist()
{
	//alert("sssss");
	var id = "";
	var type = "";
	var subtype = "";
	var desc = "";
	var ishide = "";
	var maintenable= "";
	var defaultvalue = "";
	var constructHTML = "";
	$("#portletsubtitlediv").html(paramdesc);
	$('.progress-indicator').css( 'display', 'block' );
	
	 $.ajax({
	        type: "GET",
	        url : "loadMaintenance.action?type="+paramtype,
	        async:false, 
	        dataType: 'json',

	        success: function (result){
	        	var treelst = result.mainTreelst;
	        	$('#table-1_wrapper').remove();
	        	$("#subtablemaint").append('<table id="table-1" cellspacing="0" cellpadding="2"></table>');
	        	constructHTML = "<table width='100%' border='0' id='table-1' class='display'>";
	        	constructHTML += "<thead><tr><th align='left'>Display</th><th align='left'>Code</th><th align='left'>Sequence</th><th align='left'>Default</th><th align='left'>DeActivate</th><th align='left'>Non Maintainable</th></tr></thead><tbody>";
	        	$.each(result.mainTreelst, function(i) { 
					var maintlink = 'javascript:loadsubtree("'+eval("this."+codelstparam[0])+'","Y","'+eval("this."+codelstparam[1])+'");';
	        	
	        		constructHTML += "<tr id="+i+" >";
	        		constructHTML += "<td><a href='#' onclick='"+maintlink+"'>"+eval("this."+codelstparam[1])+"</a></td><td>"+eval("this."+codelstparam[0])+"</td>";
	        		constructHTML += "<td><input type='hidden' id='codelstid' name='codelstid' value='"+eval("this."+codelstparam[5])+"'/><input type='hidden' id='seq' name='seq' value='"+(i+1)+"'/>"+(i+1)+"</td>";
	        		//IS Default
	        		if(eval("this."+codelstparam[7]) == "1"){
	        			constructHTML += "<td>Yes</td>";
	        		}else{
	        			constructHTML += "<td>No</td>";
	        		}
	        		//DeActive
	        		if(eval("this."+codelstparam[3]) == "Y"){
	        			constructHTML += "<td>Yes</td>";
	        		}else{
	        			constructHTML += "<td>No</td>";
	        		}
	        		
				if(maintenable == 1){
					constructHTML += "<td>Yes</td>";
				}else{
					constructHTML += "<td>No</td>";
				}
				
	        		constructHTML += "</tr>";
	        	});
	        	constructHTML += "</tbody> </table>";
	        	$("#table-1").replaceWith(constructHTML);
	        	$("#type").val(paramtype);
	        	if(clearDataFlag == "Y"){
	        		clearvalue();
	        	}	
	        }
	
	 });
	 $('.progress-indicator').css( 'display', 'none' );
				 
	 // Make a nice striped effect on the table
	//$('#table-1 tr:even').addClass('alt');

	oTable = $('#table-1').dataTable({
		"bJQueryUI": true,
		"aaSorting": [[ 2, "asc" ]]
	});
	
	$('.tbody tr').click(function(){
		  changeClass(this);
	});
	
	// Initialise the second table specifying a dragClass and an onDrop function that will display an alert
	$("#table-1").tableDnD({
	    onDragClass: "myDragClass",
	    onDrop: function(table, row) {
            var rows = table.tBodies[0].rows;
            var debugStr = "Row dropped was "+row.id+". New order: ";
            for (var i=0; i<rows.length; i++) {
                debugStr += rows[i].id+" ";
            }
	       // $(#debugArea).html(debugStr);
	    },
		onDragStart: function(table, row) {
			//$(#debugArea).html("Started dragging row "+row.id);
		}
	});
	//alert("s");
	$("select").uniform();

}

*/




function loadmaintvalue(id, type, subtype, desc,ishide, maintenable,defaultvalue){
	//alert("hii");
	$("#pkCodelst").val(id);
	$("#type").val(type);
	$("#subType").val(subtype);
	$("#description").val(desc);
	$("#portletcontentdiv").html(desc);
	$("#isHide").val(ishide);
	$("#maintainable").val(maintenable);
	if(ishide == "Y"){
		$("#isHide").attr("checked","true");
	}else{
		$("#isHide").removeAttr("checked");
	}
	
	if(defaultvalue == "1"){
		$("#tempdefault").attr("checked","true");
	}else{
		$("#tempdefault").removeAttr("checked");
	}
	$("#subType").attr("readonly","true");
	$("#messagetd").html("");
	
	$("#tempisDelete").removeAttr("checked");
	showPopUp("open","maintpopup","Maintenance : "+$("#portletsubtitlediv").html(),"900","350");
	
	
}