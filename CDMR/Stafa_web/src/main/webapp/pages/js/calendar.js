
$(function() {
    var startDate;
    var endDate;
    var curr = new Date; // get current date
    var firstday = new Date(curr.setDate(curr.getDate() - curr.getDay())); 
    var lastday = new Date(curr.setDate(curr.getDate() - curr.getDay()+6));
    var firstdate = dateFormat(new Date(firstday), 'mmm dd, yyyy');
    var lastDate=dateFormat(new Date(lastday), 'mmm dd, yyyy');
    $("#startDate").text(firstdate);
    $("#endDate").text(lastDate);
  //  alert($("#startDate").text());
  //  alert($("#endDate").text());
    var selectCurrentWeek = function() {
        window.setTimeout(function () {
            $('.week-picker').find('.ui-datepicker-current-day a').addClass('ui-state-active')
        }, 1);
    }
    
    $('.week-picker').datepicker( {
    	dateFormat: 'M dd, yy',
        showOtherMonths: true,
        selectOtherMonths: true,
        onSelect: function(dateText, inst) { 
            var date = $(this).datepicker('getDate');
            startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
            endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
            var dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
            try{
                $('#startDate').text($.datepicker.formatDate( dateFormat, startDate, inst.settings ));
	            $('#endDate').text($.datepicker.formatDate( dateFormat, endDate, inst.settings ));
	            weekchangeEvent();
	           
            }catch(err){
            	
            }
    //        alert($("#startDate").text());
   //        alert($("#endDate").text());
            selectCurrentWeek();
            $(".week-picker").removeClass("block");
            $(".week-picker").addClass("hidden");
        },
        beforeShowDay: function(date) {
            var cssClass = '';
            if(date >= startDate && date <= endDate)
                cssClass = 'ui-datepicker-current-day';
            return [true, cssClass];
        },
        onChangeMonthYear: function(year, month, inst) {
            selectCurrentWeek();
        }
    });
    
    $('.week-picker .ui-datepicker-calendar tr').live('mousemove', function() { $(this).find('td a').addClass('ui-state-hover'); });
    $('.week-picker .ui-datepicker-calendar tr').live('mouseleave', function() { $(this).find('td a').removeClass('ui-state-hover'); });
});



