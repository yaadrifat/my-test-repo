//------------------------ freezed methods ----------------------------  
var oTable;
function constructTable(flag) {
		//alert("construct table");
		elementcount =0;
		$('.progress-indicator').css( 'display', 'block' );
		/* var strBreadcrumb = "<a href='#' onclick='constructTable(true);'>Protocol Library</a>";
		$("#breadcrumb").html(strBreadcrumb );
				$("#protocollist").removeClass("hidden");
				$("#protocolDetails").addClass("hidden"); */
	 	if(flag || flag =='true'){
	 		 oTable=$('#dataLibraryTable').dataTable().fnDestroy();
		}
		  var link ;
			try{
				oTable = $('#dataLibraryTable').dataTable( {
					"sPaginationType": "full_numbers",
					"aoColumns": [ { "bSortable": false},
					               null,
					               null,
					               null,
					               null,
					               null
					               ],
					"bProcessing": true,
					"bServerSide": true,
					"bRetrieve" : true,
					"sDom": 'C<"clear">Rlfrtip',
					 "bDestroy": true,
					 "bJQueryUI": true,
					 "aaSorting": [[ 1, "asc" ]],
					"oColVis": {
							"aiExclude": [ 0 ],
							"buttonText": "&nbsp;",
							"bRestore": true,
							"sAlign": "left",
							"iOverlayFade": 25     /* To Overcome the visibility while clicking the addButton*/
						},
					"oLanguage": {"sProcessing":""},									
					"fnServerParams": function ( aoData ) {
															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "maintenance_datafieldconfig"});	
															
															aoData.push( {"name": "col_1_name", "value": "lower(CODELST_DESC)" } );
															aoData.push( { "name": "col_1_column", "value": "lower(CODELST_DESC)"});
															
															aoData.push( {"name": "col_5_name", "value": "lower(CODELST_HIDE)" } );
															aoData.push( { "name": "col_5_column", "value": "lower(CODELST_HIDE)"});
															
															aoData.push( { "name": "col_2_name", "value": "lower(CODELST_SUBTYP)" } );
															aoData.push( { "name": "col_2_column", "value": "lower(CODELST_SUBTYP)"});
															
															aoData.push( {"name": "col_3_name", "value":"lower(CODELST_TYPE)" } );
															aoData.push( { "name": "col_3_column", "value": "lower(CODELST_TYPE)"});
															
															aoData.push( {"name": "col_4_name", "value":"lower(CODELST_COMMENTS)" } );
															aoData.push( { "name": "col_4_column", "value": "lower(CODELST_COMMENTS)"});
															//SELECT PK_CODELST, CODELST_DESC, CODELST_TYPE, CODELST_SUBTYP, CODELST_HIDE, LEVEL,CODELST_COMMENTS
					},
					"sAjaxSource": 'dataSearch.action',
					"sAjaxDataProp": "filterData",
					"aoColumnDefs": [
					                  {	"sTitle":"",
					                	  "aTargets": [ 0], "mDataProp": function ( source, type, val ) { 
					                	 		return "<input type='checkbox' id='pkDataQc' name='pkDataQc' value='"+source[0]+"' />";
					                	 		}},
					                	 		//SELECT PK_CODELST, CODELST_DESC, CODELST_TYPE, CODELST_SUBTYP, CODELST_HIDE, LEVEL,CODELST_COMMENTS
					                  {	"sTitle":"Field Name",
					                	 			"aTargets": [ 1], "mDataProp": function ( source, type, val ) {
					                	  var link = "<a href='#' onclick='javascript:loadMaintenanceDetails(\""+source[0]+"\",\""+source[2]+"\",\""+source[3]+"\",\""+source[4]+"\",\""+source[1]+"\",\""+source[6]+"\",this);'>"+source[1]+"</a>";
					                	  		return link;
					                	 		}},
					                {	"sTitle":"Code",
					                	 			"aTargets": [ 2], "mDataProp": function ( source, type, val ) { 
												    return source[3];
												    }},
									  {	"sTitle":"Module",
												    	"aTargets": [ 3], "mDataProp": function ( source, type, val ) { 
											    	return source[2];
											    }},
									  {	"sTitle":"Description",
											    	"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
										  var comments = "";
										  if(source[6]!=null){
											  comments = source[6];
										  }
										 	   return comments;
									  		}},
									  {	"sTitle":"Deactivate?",
									  			"aTargets": [ 5], "mDataProp": function ( source, type, val ) { 
										  		return source[4];
										  }}
									],
						"fnInitComplete": function () {
								        $("#dataLibraryColumn").html($('.ColVis:eq(0)'));
									},
						"fnServerData": function ( sSource, aoData, fnCallback ) {
							$('.progress-indicator').css( 'display', 'block' );
							$.ajax( {
									"dataType": 'json',
									"type": "POST",
									"url": 'dataSearch.action',
									"data":aoData,
									"success":fnCallback,  
									"complete": function () {
												$('.progress-indicator').css( 'display', 'none' );
												$("select").uniform();
												}
								  } );},
						"fnDrawCallback": function() { 
							//alert("fnDrawCallback");
							//$('.progress-indicator').css( 'display', 'none' );
				      }
					} );
				
				jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = parseInt(no_of_pages);
				oTable.thDatasorting('dataLibraryTable');
				}catch(err){
					alert("error " + err);
				}
			//	alert("end of dt");
				

	} 