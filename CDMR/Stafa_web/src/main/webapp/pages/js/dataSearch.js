//------------------------ freezed methods ----------------------------  
var oTable;
function constructTable(flag,tableId,colManager,fn_serverParam,fn_aoColumnDef,column_Sort,default_Sort,drawCallback) {
		//alert("construct table");
		//alert(tableId);
		elementcount =0;
		$('.progress-indicator').css( 'display', 'block' );
		/* var strBreadcrumb = "<a href='#' onclick='constructTable(true);'>Protocol Library</a>";
		$("#breadcrumb").html(strBreadcrumb );
				$("#protocollist").removeClass("hidden");
				$("#protocolDetails").addClass("hidden"); */
		
		if(drawCallback==null || drawCallback==undefined){
			drawCallback = function() { };
		}
		if(default_Sort==null || default_Sort==undefined){
			 default_Sort = [[ 1, "asc" ]];
		}
	 	if(flag || flag =='true'){
	 		oTable=$('#'+tableId).dataTable().fnDestroy();
		}
		$("#"+tableId).find(".ColVis").each(function(){
	 		$(this).remove();
	 	})
		  var link ;
			try{
				oTable=$('#'+tableId).dataTable( {
					"sPaginationType": "full_numbers",
					"aoColumns": column_Sort,
					"bProcessing": true,
					"bServerSide": true,
					"bAutoWidth": false,
					"bRetrieve" : true,
					"sDom": 'C<"clear">Rlfrtip',
					
					"bDestroy": true,
					"aaSorting": default_Sort,
					"oColVis": {
							"aiExclude": [ 0 ],
							"buttonText": "&nbsp;",
							"bRestore": true,
							"sAlign": "left",
							"iOverlayFade": 25
						},
					"oLanguage": {"sProcessing":""},									
					"fnServerParams": fn_serverParam,
					"sAjaxSource": 'dataSearch.action',
					"sAjaxDataProp": "filterData",
					"aoColumnDefs": fn_aoColumnDef,
						"fnInitComplete": colManager,
						"fnServerData": function ( sSource, aoData, fnCallback ) {
							$('.progress-indicator').css( 'display', 'block' );
							$.ajax( {
									"dataType": 'json',
									"type": "POST",
									"url": 'dataSearch.action',
									"async":false,
									"data":aoData,
									"success":fnCallback,  
									"complete": function () {
												$('.progress-indicator').css( 'display', 'none' );
												 $.uniform.restore('select');
											     $("select").uniform();
												}
								  } );},
								  "fnDrawCallback":drawCallback
						/*"fnDrawCallback": function() { 
							//alert("fnDrawCallback");
							//$('.progress-indicator').css( 'display', 'none' );
				      }*/
					} );
				jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = parseInt(no_of_pages);
				}catch(err){
					alert("error " + err);
				}
			//	alert("end of dt");
				$('.progress-indicator').css( 'display', 'none' );
	} 

function constructTableWithoutColManager(flag,tableId,fn_serverParam,fn_aoColumnDef,column_Sort) {
	//alert("construct table");
	//alert(tableId);
	elementcount =0;
	$('.progress-indicator').css( 'display', 'block' );
	/* var strBreadcrumb = "<a href='#' onclick='constructTable(true);'>Protocol Library</a>";
	$("#breadcrumb").html(strBreadcrumb );
			$("#protocollist").removeClass("hidden");
			$("#protocolDetails").addClass("hidden"); */
 	if(flag || flag =='true'){
 		oTable=$('#'+tableId).dataTable().fnDestroy();
	}
	  var link ;
		try{
			oTable=$('#'+tableId).dataTable( {
				"sPaginationType": "full_numbers",
				"aoColumns": column_Sort,
				"bProcessing": true,
				"bServerSide": true,
				"bAutoWidth": false,
				"bRetrieve" : true,
				"bDestroy": true,
				"aaSorting": [[ 1, "asc" ]],
				
				"oLanguage": {"sProcessing":""},									
				"fnServerParams": fn_serverParam,
				"sAjaxSource": 'dataSearch.action',
				"sAjaxDataProp": "filterData",
				"aoColumnDefs": fn_aoColumnDef,
					
					"fnServerData": function ( sSource, aoData, fnCallback ) {
						$('.progress-indicator').css( 'display', 'block' );
						$.ajax( {
								"dataType": 'json',
								"type": "POST",
								"url": 'dataSearch.action',
								"async":false,
								"data":aoData,
								"success":fnCallback,  
								"complete": function () {
											$('.progress-indicator').css( 'display', 'none' );
											$.uniform.restore('select');
											$("select").uniform();
											}
							  } );},
					/*"fnDrawCallback": function() { 
						//alert("fnDrawCallback");
						//$('.progress-indicator').css( 'display', 'none' );
			      }*/
				} );
			jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = parseInt(no_of_pages);
			}catch(err){
				alert("error " + err);
			}
		//	alert("end of dt");
			$('.progress-indicator').css( 'display', 'none' );
} 

function constructTableObjectWithoutColManager(flag,tableObject,fn_serverParam,fn_aoColumnDef,column_Sort) {
	//alert("construct table");
	//alert(tableId);
	elementcount =0;
	$('.progress-indicator').css( 'display', 'block' );
	/* var strBreadcrumb = "<a href='#' onclick='constructTable(true);'>Protocol Library</a>";
	$("#breadcrumb").html(strBreadcrumb );
			$("#protocollist").removeClass("hidden");
			$("#protocolDetails").addClass("hidden"); */
	
 	if(flag || flag =='true'){
 		oTable=$(tableObject).dataTable().fnDestroy();
	}
	  var link ;
		try{
			oTable=$(tableObject).dataTable( {
				"sPaginationType": "full_numbers",
				"aoColumns": column_Sort,
				"bProcessing": true,
				"bServerSide": true,
				"bAutoWidth": false,
				"bRetrieve" : true,
				"bDestroy": true,
				"aaSorting": [[ 1, "asc" ]],
				
				"oLanguage": {"sProcessing":""},									
				"fnServerParams": fn_serverParam,
				"sAjaxSource": 'dataSearch.action',
				"sAjaxDataProp": "filterData",
				"aoColumnDefs": fn_aoColumnDef,
					
					"fnServerData": function ( sSource, aoData, fnCallback ) {
						$('.progress-indicator').css( 'display', 'block' );
						$.ajax( {
								"dataType": 'json',
								"type": "POST",
								"url": 'dataSearch.action',
								"async":false,
								"data":aoData,
								"success":fnCallback,  
								"complete": function () {
											$('.progress-indicator').css( 'display', 'none' );
											$.uniform.restore('select');
											$("select").uniform();
											}
							  } );},
					/*"fnDrawCallback": function() { 
						//alert("fnDrawCallback");
						//$('.progress-indicator').css( 'display', 'none' );
			      }*/
				} );
			jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = parseInt(no_of_pages);
			}catch(err){
				alert("error " + err);
			}
		//	alert("end of dt");
} 
function constructMergeWithoutColManager(flag,tableId,fn_serverParam,fn_aoColumnDef,aoColumns,column_sort,mergeColumns,tabFlag,cboxClass,equipCboxHiddenId) {
	//alert("construct table");
	//alert(sDomVal);
	//alert(tableId);
	elementcount =0;
	$('.progress-indicator').css( 'display', 'block' );
	/* var strBreadcrumb = "<a href='#' onclick='constructTable(true);'>Protocol Library</a>";
	$("#breadcrumb").html(strBreadcrumb );
			$("#protocollist").removeClass("hidden");
			$("#protocolDetails").addClass("hidden"); */
 	if(flag || flag =='true'){
 		oTable=$('#'+tableId).dataTable().fnDestroy();
	}
	  var link ;
		try{
			oTable=$('#'+tableId).dataTable( {
				"sPaginationType": "full_numbers",
				"aoColumns": aoColumns,
				"bProcessing": true,
				"bServerSide": true,
				"bAutoWidth": false,
				"bRetrieve" : true,
				"bDestroy": true,
				"aaSorting": column_sort,
				
				"oLanguage": {"sProcessing":""},									
				"fnServerParams": fn_serverParam,
				"sAjaxSource": 'dataSearch.action',
				"sAjaxDataProp": "filterData",
				"aoColumnDefs": fn_aoColumnDef,
					"fnServerData": function ( sSource, aoData, fnCallback ) {
						
						$('.progress-indicator').css( 'display', 'block' );
												$.ajax( {
								"dataType": 'json',
								"type": 'POST',
								"url": 'dataSearch.action',
								"async":false,
								"data":aoData,
								"success":fnCallback,  
								"complete": function () {
											$('.progress-indicator').css( 'display', 'none' );
											$.uniform.restore('select');
											$("select").uniform();
											}
								
											
							  } );},
					"fnDrawCallback": function (){ 
											var checkedVals = "";
											if(tabFlag){
												$("."+cboxClass).bind("click",function(){
													if($(this).prop("checked")){
														checkedVals+=","+$(this).val()+"";
														$("#"+equipCboxHiddenId).val(checkedVals);
													}else{
														 var temp=$("#"+equipCboxHiddenId).val();
														 var splitArray = temp.split(",");
														 var selectedIndex = splitArray.indexOf($(this).val());
														 if(selectedIndex!=-1){
														        splitArray.splice($.inArray($(this).val(),splitArray),1);
														   }	
														 $("#"+equipCboxHiddenId).val(splitArray);
														 checkedVals = splitArray;
														 }
												});
											}
											merge(tableId,mergeColumns);
										}
							
				} );
			
			jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = parseInt(no_of_pages);
			}catch(err){
				alert("error " + err);
			}
		//	alert("end of dt");
} 
function constructTableWithMerge(flag,tableId,fn_serverParam,fn_aoColumnDef,aoColumns,column_sort,mergeColumns,colManager,sDomVal) {
	//alert("construct table");
	//alert(sDomVal);
	//alert(tableId);
	elementcount =0;
	$('.progress-indicator').css( 'display', 'block' );
	/* var strBreadcrumb = "<a href='#' onclick='constructTable(true);'>Protocol Library</a>";
	$("#breadcrumb").html(strBreadcrumb );
			$("#protocollist").removeClass("hidden");
			$("#protocolDetails").addClass("hidden"); */
 	if(flag || flag =='true'){
 		oTable=$('#'+tableId).dataTable().fnDestroy();
	}
	  var link ;
		try{
			oTable=$('#'+tableId).dataTable( {
				"sPaginationType": "full_numbers",
				"aoColumns": aoColumns,
				"bProcessing": true,
				"bServerSide": true,
				"bAutoWidth": false,
				"bRetrieve" : true,
				//"bStateSave": true,
				"sDom": sDomVal,
				"oColVis": {
					"aiExclude": [ 0 ],
					"buttonText": "&nbsp;",
					"bRestore": true,
					"sAlign": "left",
					"iOverlayFade": 25,
				},
				"bDestroy": true,
				"aaSorting": column_sort,
				
				"oLanguage": {"sProcessing":""},									
				"fnServerParams": fn_serverParam,
				"sAjaxSource": 'dataSearch.action',
				"sAjaxDataProp": "filterData",
				"aoColumnDefs": fn_aoColumnDef,
				"fnInitComplete": colManager,
					"fnServerData": function ( sSource, aoData, fnCallback ) {
						
						$('.progress-indicator').css( 'display', 'block' );
												$.ajax( {
								"dataType": 'json',
								"type": 'POST',
								"url": 'dataSearch.action',
								"async":false,
								"data":aoData,
								"success":fnCallback,  
								"complete": function () {
											$('.progress-indicator').css( 'display', 'none' );
											$.uniform.restore('select');
											$("select").uniform();
											}
								
											
							  } );},
					"fnDrawCallback": function (){ merge(tableId,mergeColumns)}
							
				} );
			
			jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = parseInt(no_of_pages);
			}catch(err){
				alert("error " + err);
			}
		//	alert("end of dt");
} 

// BB used for merge 

function merge(tableId,mergedCols) {
	//alert("merge " + tableId + " / " +mergedCols);
	/* Get the count of same value and remove redundant ones */
	//var mergedCols =[1,2,3,4];
	var rowspan =1;
	var preValue="" ,curValue="",className;
	var precelValue;
	var preObj;
	//alert("trace 1");
	var findVar = "#"+tableId +" tbody tr ";
	
	$(findVar).each( function (row) {
		//  alert("row " + row);
		  $(this).find("td").each(function (col){
			  $(this).addClass("col"+col);
			  //if(col ==8){
				//  $(this).attr("nowrap","nowrap");
			 // }
		  });
	});
	
	//alert("$(findVar) " + $(findVar).html());
	try{
		 
		 $.each(mergedCols, function(i, colNo) {
		 /*mergedCols.each(function(colNo) {*/
        //   alert("colNo" + colNo);
		
               className="col"+colNo;
               rowspan =1;
               preValue =null;
               preObj = null;
               var preFlag = true;
      //         alert("className" + className);
               $('#'+tableId).find('tbody').find(("."+className)).each(function(row){
				//	alert("row " + row + "/ " + preObj);
					if(row ==0){
						preObj= this;
						 preFlag = false;
						 preValue =null;
					}
					curValue = $(this).html();
					//alert(preValue+" == "+curValue); 
				//	alert($(this.parentNode).html());
					//alert("trace4");
					if(preValue == curValue){
						//alert(" pre " + $(this).prev().html());
						className = "col"+(colNo-1);
					//	alert( className +"/"+$(this).prev().hasClass(className));
					//	alert($(this).prev().attr('rowspan') +"/"+ colNo + "!= " +mergedCols[i] + " / " + $(this).prev().hasClass(className) + "/ " + className);
					//	alert(" rowspan " + $(this).prev().attr('rowspan')); 
						//if(($(this).prev().attr('rowspan') >1) || (colNo != mergedCols[i] && $(this).prev().hasClass(className))) {
						if(($(this).prev().attr('rowspan') >1) || (colNo != mergedCols[0] && $(this).prev().hasClass(className))) {
						
						//if((!preFlag && i!=0) ||(colNo != mergedCols[i] && $(this).prev().hasClass(className))) {
						
						//	alert("not merge");
							preObj= this;
							preFlag = true;
							//  preValue=null;
							
						}else{
							rowspan = parseInt($(preObj).attr('rowspan'));
							// check previous td
							//alert("before mergeRows " +row + "/rowspan " +rowspan  );
							if(rowspan>1){
								$(preObj).attr('rowspan',(rowspan+1) );
							}else{
								$(preObj).attr('rowspan',2);
							}
							this.parentNode.removeChild(this);
							
							//alert("Else Part");
						/*BB	$(preObj).addClass("merge1");
							alert("html" + $(this).closest("td").prev().html() + " / " + $(this).closest("td").prev().text() );
							if($(this).closest("td").prev().hasClass("merge1")){
								$(this).addClass("merge1");
							}else{
								$(this).addClass("merge");
							}
							if($(preObj).parent().hasClass("even")){
								$(this).addClass("even1");	
							}else if($(preObj).parent().hasClass("odd")){
								$(this).addClass("odd1");	
							}
							
							$(this).html("");
						*/	
						}
					}else{
						preObj= this;
						  //preValue=null;
						  preFlag = false;
					}
					preValue = curValue;
				})
	  });
	 }catch(err){
		 alert("Error " + err);
	 }
	 //alert("end of call back");
	}

