//------------------------ freezed methods ----------------------------  
var oTable;
function constructTable(flag) {
		//alert("construct table");
		
		elementcount =0;
		$('.progress-indicator').css( 'display', 'block' );
		/* var strBreadcrumb = "<a href='#' onclick='constructTable(true);'>Protocol Library</a>";
		$("#breadcrumb").html(strBreadcrumb );
				$("#protocollist").removeClass("hidden");
				$("#protocolDetails").addClass("hidden"); */
	 	if(flag || flag =='true'){
		 oTable=$('#acquisitionTable').dataTable().fnDestroy();
		}
	 
		  var link ;     
			try{
				oTable=$('#acquisitionTable').dataTable( {	                           
					"sPaginationType": "full_numbers",
					"bProcessing": true,
					"bServerSide": true,
					"bRetrieve" : true,
					"sDom": 'C<"clear">Rlfrtip',
					"sPaginationType": "full_numbers",
					"aoColumns": [ { "bSortable":false},
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null
					               ],
					 "bDestroy": true,
					"sScrollX": "100%",
					 "aaSorting": [[ 1, "asc" ]],
					 "oColVis": {
							"aiExclude": [ 0 ],
							"buttonText": "&nbsp;",
							"bRestore": true,
							"sAlign": "left"
						},
						//"sScrollX":"100%", 
						 //"sScrollXInner": "110%",
					"oLanguage": {"sProcessing":""},									
					"fnServerParams": function ( aoData ) {
															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "technician_acquisition"});	
															
															aoData.push( {"name": "col_1_name", "value": "lower(recipientId)" } );
															aoData.push( { "name": "col_1_column", "value": "lower(recipientId)"});
															
															aoData.push( { "name": "col_2_name", "value": " lower(recipientName)"});
															aoData.push( { "name": "col_2_column", "value": "lower(recipientName)"});
																		
															aoData.push( {"name": "col_3_name", "value": " lower(productSource)" } );
															aoData.push( { "name": "col_3_column", "value": "lower(productSource)"});
															
															aoData.push( {"name": "col_4_name", "value": " lower(productType)" } );
															aoData.push( { "name": "col_4_column", "value": "lower(productType)"});
															
															aoData.push( {"name": "col_5_name", "value": " lower(donorId)" } );
															aoData.push( { "name": "col_5_column", "value": "lower(donorId)"});
															
															aoData.push( {"name": "col_6_name", "value": " lower(donorName)" } );
															aoData.push( { "name": "col_6_column", "value": "lower(donorName)"});
															
															aoData.push( {"name": "col_7_name", "value": " lower(donorLocation)" } );
															aoData.push( { "name": "col_7_column", "value": "lower(donorLocation)"});
															
															aoData.push( {"name": "col_8_name", "value": " lower(processingFacility)" } );
															aoData.push( { "name": "col_8_column", "value": "lower(processingFacility)"});
															
															aoData.push( {"name": "col_9_name", "value": " lower(proposedFinalDisposition)" } );
															aoData.push( { "name": "col_9_column", "value": "lower(proposedFinalDisposition)"});
															
															aoData.push( {"name": "col_10_name", "value": " lower(productAppearance)" } );
															aoData.push( { "name": "col_10_column", "value": "lower(productAppearance)"});
															
															aoData.push( {"name": "col_11_name", "value": " lower(productId)" } );
															aoData.push( { "name": "col_11_column", "value": "lower(productId)"});
															
															aoData.push( {"name": "col_12_name", "value": " lower(physicianName)" } );
															aoData.push( { "name": "col_12_column", "value": "lower(physicianName)"});
															
															aoData.push( {"name": "col_13_name", "value": " lower(collectSection)" } );
															aoData.push( { "name": "col_13_column", "value": "lower(collectSection)"});
															
															aoData.push( {"name": "col_14_name", "value": " lower(collectionDate)" } );
															aoData.push( { "name": "col_14_column", "value": "lower(collectionDate)"});
															
															aoData.push( {"name": "col_15_name", "value": " lower(productStatus)" } );
															aoData.push( { "name": "col_15_column", "value": "lower(productStatus)"});
															
																	
					},
					"sAjaxSource": 'dataSearch.action',
					"sAjaxDataProp": "filterData",
					"aoColumnDefs": [
					                  {		
					                	  "aTargets": [ 0], "mDataProp": function ( source, type, val ) {
					                	   
				                	 		return "<input type='hidden' id='personprId' value="+ source.personproductid+">";
				                	     }},
					                  { 	"sTitle": "Recipient ID",
				                	    	 "aTargets": [ 1], "mDataProp": function ( source, type, val ) { 
					                	 		return source.recipientId;
					                	 }},
									  {"sTitle": "Recipient Name",
					                		 "aTargets": [ 2], "mDataProp": function ( source, type, val ) { 
											    return source.recipientName;
											    }},
									  {"sTitle": "Product Source",
											    	"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
										 	   return source.productSource;
									  		}},
									  {"sTitle": "Product Type",
									  			"aTargets": [ 3], "mDataProp": function ( source, type, val ) { 
										  		return source.productType;
										  }},
									  {"sTitle": "Donor ID",
											  "aTargets": [ 5], "mDataProp": function ( source, type, val ) { 
										  		return source.donorId;
										  }},
									  {"sTitle": "Donor Name",
											  "aTargets": [ 6], "mDataProp": function ( source, type, val ) { 
										  		return source.donorName;
										  }},
									  {"sTitle": "Donor Location",
											  "aTargets": [ 7], "mDataProp": function ( source, type, val ) { 
										  		return source.donorLocation;
										  }},
									  {"sTitle": "Processing Facility",
											  "aTargets": [ 8], "mDataProp": function ( source, type, val ) { 
										  		return source.processingFacility;
										  }},
									  {"sTitle": "Proposed Final Disposition",
											  "aTargets": [ 9], "mDataProp": function ( source, type, val ) { 
										  		return source.proposedFinalDisposition;
										  }},
									  {"sTitle": "Product Appearance OK?",
											  "aTargets": [ 10], "mDataProp": function ( source, type, val ) { 
										  		if (source.productAppearance=="on"){
										  			link='YES';
										  		}
										  		else{
										  			link='NO';
										  		}
										  		return link;
										  }},
									  {"sTitle": "Product ID",
											  "aTargets": [ 11], "mDataProp": function ( source, type, val ) { 
										  		return source.productId;
										  }},
									 {"sTitle": "Physician Name",
											  "aTargets": [ 12], "mDataProp": function ( source, type, val ) { 
										  		return source.physicianName;
										}},
									  {"sTitle": "Collect Section",
											  "aTargets": [ 13], "mDataProp": function ( source, type, val ) { 
										  		return source.collectSection;
										  }},
										  {"sTitle": "Collection Date",
											  "aTargets": [ 14], "mDataProp": function ( source, type, val ) {
												  if(source.collectionDate!=null){
													  return $.datepicker.formatDate('M dd, yy', new Date(source.collectionDate));
												  }else{
													  return "";
												  }
										  }},
									  {"sTitle": "Status",
											  "aTargets": [ 15], "mDataProp": function ( source, type, val ) { 
										  		return source.productStatus;
										  }}
									],
						"fnInitComplete": function () {
								        $("#acquisitionColumn").html($('.ColVis:eq(0)'));
									},
									
						"fnServerData": function ( sSource, aoData, fnCallback ) {
							$('.progress-indicator').css( 'display', 'block' );
							$.ajax( {
									"dataType": 'json',
									"type": "POST",
									"url": 'dataSearch.action',
									"data":aoData,
									"async":false,
									"success":fnCallback,  
									"complete": function () {
												$('.progress-indicator').css( 'display', 'none' );
												$("select").uniform();
												}
								  } );},
						/*"fnDrawCallback": function() { 
							//alert("fnDrawCallback");
							//$('.progress-indicator').css( 'display', 'none' );
				      }*/
											  
					} );
				jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = parseInt(no_of_pages);
				}catch(err){
					alert("error " + err);
				}
			//	alert("end of dt");
					
	} 