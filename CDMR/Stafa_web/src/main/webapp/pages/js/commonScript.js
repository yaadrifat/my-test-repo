//var accessionParam ="accLocation=acqlocation&productType=producttype&productSource=prodsourc&diagnosis=diagnosis&aboRH=abo/rh&begStatus=begstatus&planProtocol=planprotocol&reciprId=reciprid&prodRegistry=prodregistry&procSelec=procfacility&prodprotocol=prodprotocol";

//var acquisitionParam ="aquLocation=acqlocation&productType=producttype&finalDisp=finaldispos&donorLoc=location&procSelec=procfacility&productSource=prodsource";
/** By Mari */
/*
 * function callAction(page){ if(page=="accession"){ loadAccessionPage(); }
 * if(page=="acquisition"){ loadAcquisitionPage(); } }
 */

// getFile Size ?//Mari TODO future to move to ajaxfileUpload Js
var optionFlag = 0;
var ctlProductFlowType = 'ctl_product';

// Validate numbers only

function readOnlyDateField(id) {
	var arr = id.split(",");
	var len = arr.length;
	for ( var i = 0; i < len; i++)
		$("#" + arr[i]).bind('blur', function() {

			if ($(this).val() != null && $(this).val() != 'undefined') {
				if ($(this).val() != null && $.trim($(this).val()) != "") {
					var rtunflag = isDate($(this).val());
					if (rtunflag == false) {
						$(this).val('');
						var fieldid = $(this).attr('id');
						setTimeout(function() {
							setToFocus(fieldid);
						}, 1000);

					} else {
						var formatdate = new Date($(this).val());
						$(this).val(formatdate.format("mmm d, yyyy "));
					}
				}
			} else {
				if ($("#" + arr).val() != null && $("#" + arr).val() != "") {
					var rtunflag = isDate($("#" + arr).val());
					if (rtunflag == false) {
						// setTimeout ( "setToFocus('"+arr+"')", 1000 );
				setTimeout(function() {
					setToFocus(arr);
				}, 1000);
			} else {
				var formatdate = new Date($("#" + arr).val());
				$("#" + arr).val(formatdate.format("mmm d, yyyy "));
			}
		}
	}
}		);

}
var dtCh = " ";
var minYear = 1800;
var maxYear = 2100;
var months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep",
		"Oct", "Nov", "Dec" ];

function DaysArray(n) {
	for ( var i = 0; i < n; i++) {
		this[i] = 31
		if (i == 3 || i == 5 || i == 8 || i == 10) {
			this[i] = 30
		}
		if (i == 1) {
			this[i] = 29
		}
	}
	return this
}
function daysInFebruary(year) {
	// February has 29 days in any year evenly divisible by four,
	// EXCEPT for centurial years which are not also divisible by 400.
	return (((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29
			: 28);
}
function isDate(dtStr) {
	// alert("date string >"+dtStr+"<");
	var daysInMonth = DaysArray(12)
	var newdate;
	try {
		newdate = new Date(dtStr);
		newdate = newdate.format("mmm d, yyyy ");
	} catch (e) {
		alert("The date format should be : MMM DD, YYYY");
		return false;
	}

	var dateArr = newdate.split(dtCh);

	var strMonth = dateArr[0];
	var strDay = dateArr[1];
	var strYear = dateArr[2];
	strYr = strYear
	if (strDay.charAt(0) == "0" && strDay.length > 1)
		strDay = strDay.substring(1)
	if (strMonth.charAt(0) == "0" && strMonth.length > 1)
		strMonth = strMonth.substring(1)
	for ( var i = 1; i <= 3; i++) {
		if (strYr.charAt(0) == "0" && strYr.length > 1)
			strYr = strYr.substring(1)
	}
	month = jQuery.inArray(strMonth, months);
	day = parseInt(strDay)
	year = parseInt(strYr)
	if (strMonth.length < 1 || month == -1) {
		alert("Error: Please enter a valid month")
		return false
	}
	if (strDay != null && strDay.length < 1 || day < 1 || day > 31
			|| (month == 'Feb' && day > daysInFebruary(year))
			|| day > daysInMonth[month]) {
		alert("Error: Please enter a valid day")
		return false
	}
	if (strYear != null && strYear.length != 4 || year == 0 || year < minYear
			|| year > maxYear) {
		alert("Error: Please enter a valid 4 digit year between " + minYear
				+ " and " + maxYear)
		return false
	}
	return true;
}

$('.numbersOnly').bind("select cut copy paste", function(e) {
	// e.preventDefault();

	});
$(".numbersOnly")
		.live(
				'keypress',
				function(event) {
					// Allow: backspace, delete, tab, escape, and entercharCode
					// > 31 &&
					var value = event.which;

					var charCode = (event.which) ? event.which : event.keyCode;
					if ((event.ctrlKey && (charCode == 65 || charCode == 97
							|| charCode == 118 || charCode == 86
							|| charCode == 99 || charCode == 67))
							|| charCode < 58) {
						// if ( charCode >47 && charCode < 58){

						event.returnValue = true;
						event.ctrlKey = true;
						return event;
					} else {

						return false;
					}

				});
$(".dateEntry").live("click", function() {
	var id = $(this).attr("id");

	readOnlyDateField(id);

});
$('.timeformate').live("select cut copy paste", function(e) {
	// e.preventDefault();

	});
$(".timeformate").live("click", function(event) {
	/*
	 * var value = event.which;
	 * 
	 * var charCode = (event.which) ? event.which : event.keyCode; if (
	 * (event.ctrlKey && (charCode==65 || charCode ==97 || charCode==118 ||
	 * charCode==86 || charCode==99 || charCode==67 )) || charCode < 47 ){ //if (
	 * charCode >47 && charCode < 58){
	 * 
	 * event.returnValue = true; event.ctrlKey =true; return event; }else{
	 * 
	 * return false;
	 *  }
	 */
	var id = $(this).attr("id");

	checkTimeFormat(id);

});
function checkTimeFormat(field) {
	$("#" + field).bind("blur", function() {
		var errorMsg = "";
		var fieldid = $("#" + field).val();
		// alert(fieldid);
			// regular expression to match required time format
			// re = /^(\d{1,2}):(\d{2})(:00)?([ap]m)?$/;
			var re = /^\s*([01]?\d|2[0-3]):?([0-5]\d)\s*$/;

			if ($.trim(fieldid) != '') {
				if (regs = $("#" + field).val().match(re)) {
					// alert(regs[1].length);
					// alert("regs format--->"+regs);
					if (regs[4]) {
						// 12-hour time format with am/pm
						if (regs[1] < 1 || regs[1] > 12) {
							errorMsg = "Invalid value for hours: " + regs[1];
						}
					} else {
						// 24-hour time format
						if (regs[1] > 23) {
							errorMsg = "Invalid value for hours: " + regs[1];
						}
					}
					if (!errorMsg && regs[2] > 59) {
						errorMsg = "Invalid value for minutes: " + regs[2];
					}
				} else {
					errorMsg = "The Time format should be :HH:MM";
				}
			}

			if (errorMsg != "") {
				alert(errorMsg);
				$("#" + field).val("");
				$("#" + field).focus();
				return false;
			}

			return true;
		});

}
function setToFocus(field) {

	$("#" + field).focus();
}
function showportlet() {

	$(".portlet").find(".portlet-header").find("span").removeClass(
			"ui-icon-plusthick").addClass("ui-icon-minusthick");
	$(".portlet").find(".portlet-header").end().find(".portlet-content").show();
}

function ctlWorkFlow(entityId) {
	var entityType = "ctl_product";
	getNextFlow(entityId, entityType);

}

function showTracker(entityId, entityType, flowType, nextflowFlag, trackerView,
		nextflows) {
	// alert("showTracker");
	var jsonData = "entityId:'" + entityId + "',entityType:'" + entityType
			+ "',flowType:'" + flowType + "',nextflowFlag:'" + nextflowFlag
			+ "',activity:" + $("#activity").val();
	var preSeq = -1;
	var currSeq = -1;
	// alert("jsonData "+jsonData );
	var url = "loadTrackerList";
	$(".workFlowContainer").html("");
	response = jsonDataCall(url, "jsonData={" + jsonData + "}");
	createFlag = true;
	$("#nextOption").html("");
	$("#nextOption").append("<option value=''> Select</option>");
	$("#hnextOption").html("");
	$("#hnextOption").append("<option value='0'> Select</option>");
	$(response.trackerList).each(function(i, v) {
		var name;
		var status;
		var terFlag;
		var calculationurl;
		for (j = 0; j < v.length; j++) {

			if (j == 0) {
				sequence = v[0]
			} else if (j == 1) {
				name = v[1];
				// alert("name"+name);
		} else if (j == 2) {
			calculationurl = v[2];
			// alert("calculationurl"+v[2]);habvcnmmjkopiiyyyeertt3ewwqr
		} else if (j == 3) {
			status = v[3];
			// alert("status"+v[3]);
		} else if (j == 4) {
			terFlag = v[4];
		}
	}
	if ((name != undefined) && (status != undefined)) {

		var displayIndex = calculationurl.indexOf("#");
		var displaysubname;
		var displayValue;
		if (displayIndex != -1) {
			// alert("entityId " + entityId);
			var tmpValues = entityId.split(",");
			for ( var s = 0; s < tmpValues.length; s++) {
				calculationurl = calculationurl.replace(("#" + (s + 1) + "a"),
						tmpValues[s]);
				// alert("calculationurl" + calculationurl);
			}
			displayValue = calculationurl;
		} else {
			displayValue = calculationurl;
		}
		// alert("optionFlag"+optionFlag + "/ " + displayValue);
		// alert("status"+status);
		if (status == "0" && createFlag) {
			if (trackerView == undefined || trackerView) {
				addTracker(name, status, displayValue);
			}
			createFlag = false;
			optionFlag = 1;
		} else if (status == "1") {
			if (trackerView == undefined || trackerView) {
				addTracker(name, status, displayValue);
			}
		} else if (status == "0" && optionFlag == 1) {
			currSeq = sequence;
			if (preSeq == -1) {
				preSeq = currSeq;
			}
			if (preSeq == currSeq) {
				// ?BB$("#nextOption").append("<option
				// value='"+displayValue+"'>"+name+"</option>");
			}
			// $("#nextOption").append("<option
			// value='"+displayValue+"'>"+name+"</option>");
			// alert("nextflows " + nextflows);
			if (preSeq != currSeq && (nextflows == undefined || !nextflows)) {
				optionFlag = 0;
			}
		}
		// alert("name="+name+" status"+status);
	}

}	);

	$(response.nextFlowList).each(function(i, v) {
		var name;
		var status;
		var activity;
		var calculationurl;
		for (j = 0; j < v.length; j++) {
			if (j == 0) {
				sequence = v[0]
			} else if (j == 1) {
				name = v[1];
				// alert("name"+name);
		} else if (j == 2) {
			calculationurl = v[2];
			// alert("calculationurl"+v[2]);habvcnmmjkopiiyyyeertt3ewwqr
		} else if (j == 4) {
			activity = v[4];
		}
	}
	if ((name != undefined)) {

		var displayIndex = calculationurl.indexOf("#");
		var displaysubname;
		var displayValue;
		if (displayIndex != -1) {
			// alert("entityId " + entityId);
			var tmpValues = entityId.split(",");
			for ( var s = 0; s < tmpValues.length; s++) {
				calculationurl = calculationurl.replace(("#" + (s + 1) + "a"),
						tmpValues[s]);
				// alert("calculationurl" + calculationurl);
			}
			displayValue = calculationurl;
		} else {
			displayValue = calculationurl;
		}
		$("#nextOption").append(
				"<option value='" + displayValue + "'>" + name + "</option>");
		$("#hnextOption").append(
				"<option value='" + activity + "'>" + name + "</option>");
	}
}	);
	if (flowType == ctlProductFlowType) {
		$("#nextOption").append(
				"<option value='loadCTLTechHome.action'>HomePage</option>");
	} else {
		$("#nextOption").append(
				"<option value='loadNurseHomePage.action'>HomePage</option>");
	}

	$("#nextOption").append("<option value='logout.action'>Logout</option>");
	$("#nextOption").bind("change", function() {
		setActivity(0, 'next');
	});
}

function setActivity(activity, mode, obj) {
	// alert("trace ");
	if (mode == undefined) {
		$("#activity").val(activity);
	} else if (mode == "next") {
		// var selectedIndex = $('#nextOption :selected').index();
		var selectedIndex = $("#nextOption").prop("selectedIndex");
		$("#hnextOption").prop("selectedIndex", selectedIndex);
		// ?BB $("#activity").val($("#hnextOption").val());
	}
	// alert(" val " + $("#activity").val());
}

function getNextFlow(entityId, entityType, nextOption) {
	// alert("WORKFLOW");
	var url = "";

	if (nextOption != null) {
		url = nextOption;
	} else {
		var jsonData = "entityType:'" + entityType + "',entityId:'" + entityId
				+ "'";
		var url = "fetchNextFlow";
		response = jsonDataCall(url, "jsonData={" + jsonData + "}");
		// alert(response.nextFlow);
		if (response != null && response.nextFlow != null
				&& response.nextFlow != "null"
				&& response.nextFlow != "success") {
			url = response.nextFlow;
		} else {
			url = "";
		}
	}
	// alert("url " + url);
	if (url != null && $.trim(url) != "") {
		url = url.substring(3);
		// alert("url1 " + url);

		// check multiple options
		if (url.indexOf("@`") != -1) {
			var flowsArray = url.split("@`");
			// alert("length" + flowsArray.length);
			var flowHTML = "<table width='100%'>";
			for (i = 0; i < flowsArray.length; i++) {
				if (flowsArray[i].indexOf("->") != -1) {
					var activity = flowsArray[i].split("->");
					// alert(activity[0] + " /url " + activity[1] );
					var tmpArray = activity[0].split("_");
					flowHTML += "<tr><td><a href='#' onclick=\"setActivity(\'"
							+ tmpArray[0] + "\');loadAction(\'" + activity[1]
							+ "\');removeModelDialog();\" >" + tmpArray[1]
							+ "</a></td></tr>";
					// flowHTML += "<tr><td><a href='#'
					// onclick=\"removeModelDialog();\"
					// >"+activity[0]+"</a></td></tr>";
				}
			}
			flowHTML += "</table>";
			// alert("flowHTML " + flowHTML);
			$("#dialogContent").html(flowHTML).dialog( {
				autoOpen : true,
				title : 'Next Options',
				modal : true,
				width : 350,
				height : 300,
				close : function() {
					jQuery("#dialogContent").dialog("destroy");
					// $(this).html(" ");
				$(this).remove();
				$("#modeldialog").append('<div id="dialogContent"></div>');

			}
			});
		} else {
			var tmpArray = url.split("_");
			setActivity(tmpArray[0]);
			if (url.indexOf("->") != -1) {
				url = url.substring(url.indexOf("->") + 2);
			}

			// alert("final url" + url);
			loadAction(url);
		}

	} else {
		alert("Workflow completed");
	}
}

function removeModelDialog() {
	// alert("remove dialog");
	jQuery("#dialogContent").dialog("destroy");
	// $(this).html(" ");
	$("#dialogContent").remove();
	$("#modeldialog").append('<div id="dialogContent"></div>');
}

function addnewDonor() {
	// showPopUp("open","","","950","520");
	url = "loadAddDonor.action";
	$("#modalpopup").dialog( {
		autoOpen : false,
		title : "Add New Donor / Collection Cycle",
		modal : true,
		width : 1200,
		height : 550,
		close : function() {
			jQuery("#modalpopup").dialog("destroy");
			// ?BB hideHighting('dataLibraryTbody');
	}
	});
	$.ajax( {
		type : "POST",
		url : url,
		// async:false,
		success : function(result, status, error) {
			// $('.progress-indicator').css( 'display', 'block' );
		$("#modalpopup").html(result);
		// $('.progress-indicator').css( 'display', 'none' );
		$("select").uniform();

	},
	error : function(request, status, error) {
		alert("Error " + error);
		// alert(request.responseText);
	}

	});

	$("#modalpopup").dialog("open");
	$("#modalpopup").css("width", "95%");
	$('.ui-dialog select').uniform();
	$("#error").hide();
}

function addNewRecipient() {
	// showPopUp("open","","","950","520");
	url = "loadAddRecipient.action";
	$("#modalpopup").dialog( {
		autoOpen : false,
		title : "Add New Recipient",
		modal : true,
		width : 1200,
		height : 550,
		close : function() {
			jQuery("#modalpopup").dialog("destroy");
			// ?BB hideHighting('dataLibraryTbody');
	}
	});
	$.ajax( {
		type : "POST",
		url : url,
		// async:false,
		success : function(result, status, error) {
			// $('.progress-indicator').css( 'display', 'block' );
		$("#modalpopup").html(result);
		// $('.progress-indicator').css( 'display', 'none' );
		$("select").uniform();

	},
	error : function(request, status, error) {
		alert("Error " + error);
		// alert(request.responseText);
	}

	});

	$("#modalpopup").dialog("open");
	$("#modalpopup").css("width", "95%");
	$('.ui-dialog select').uniform();
	$("#error").hide();
}

function fileSelected() {
	var file = document.getElementById('fileToUpload').files[0];
	if (file) {
		var fileSize = 0;
		if (file.size > 1024 * 1024)
			fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100)
					.toString() + 'MB';
		else
			fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';

		// document.getElementById('fileName').innerHTML = 'Name: ' + file.name;
		return fileSize;
		document.getElementById('fileType').innerHTML = 'Type: ' + file.type;
	}
}

function updateSize(fileid) {
	var nBytes = 0;
	var nFiles = 0;
	oFiles = document.getElementById(fileid).files;
	nFiles = oFiles.length;
	for ( var nFileId = 0; nFileId < nFiles; nFileId++) {
		nBytes += oFiles[nFileId].size;
	}
	var sOutput = nBytes + " bytes";
	// optional code for multiples approximation
	for ( var aMultiples = [ "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB",
			"YiB" ], nMultiple = 0, nApprox = nBytes / 1024; nApprox > 1; nApprox /= 1024, nMultiple++) {
		sOutput = nApprox.toFixed(3) + " " + aMultiples[nMultiple] + " ("
				+ nBytes + " bytes)";
	}
	// document.getElementById("fileNum").innerHTML = nFiles;
	return sOutput;
}

function GetFileSize(fileid) {
	// alert(updateSize(fileid));

	return 1;
	/*
	 * try {
	 * 
	 * var fileSize = 0; //for IE if ($.browser.msie) { //before making an
	 * object of ActiveXObject, //please make sure ActiveX is enabled in your IE
	 * browser var objFSO = new ActiveXObject("Scripting.FileSystemObject"); var
	 * filePath = $("#" + fileid)[0].value; var objFile =
	 * objFSO.getFile(filePath); var fileSize = objFile.size; //size in kb
	 * fileSize = fileSize / 1048576; //size in mb } //for FF, Safari, Opeara
	 * and Others else { fileSize = $("#" + fileid)[0].files[0].size //size in
	 * kb fileSize = fileSize / 1048576; //size in mb } alert("Uploaded File
	 * Size is" + fileSize + "MB"); } catch (e) { alert("Error is :" + e); }
	 */
	// return fileSize;
	// return 1;
}

//

function taskAction(procedureid, methodname) {
	// alert("pro id="+procedureid+"method name="+methodname);
	// pro id=1625method name=transfer
	if (methodname == 'transfer') {
		// alert("transfer")
		taskActionData = "{transferIds:'" + procedureid
				+ "',terminateIds:\"\",selfAsignIds:\"\"}";
		url = "saveNurseHomePage";
		response = jsonDataCall(url, "jsonData=" + taskActionData);
		alert("Transfer Successfully");
	} else if (methodname == 'terminate') {
		// alert("terminate"+procedureid)
		taskActionData = "{transferIds:\"\",terminateIds:'" + procedureid
				+ "',selfAsignIds:\"\"}";
		url = "saveNurseHomePage";
		response = jsonDataCall(url, "jsonData=" + taskActionData);
		alert("Terminate Successfully");
	}
	// taskActionData =
	// "{transferIds:'"+transferIds+"',terminateIds:'"+terminateIds+"',selfAsignIds:'"+selfAsignIds+"'}";

}

// clear the form

jQuery.fn.reset = function() {
	$(this).each(function() {
		this.reset();
	});

	$.uniform.restore("select");
	$("select").uniform();
}
function disableDropDown() {

}
function clearTracker() {
	$(".workFlowContainer").empty();
	$("#pagename").empty();
}
function avoidFormSubmitting() {
	return false;
}

function showNotesCount(id, donor, falg) {
	// alert("id"+id);
	var url = "fetchNotes";
	var parameters = "{entityId:'" + id + "',entityType:'" + donor + "',flag:'"
			+ falg + "'}";
	var response = jsonDataCall(url, "jsonData=" + parameters);
	$(".subspan").html(response.notesList.length);
}

function newNote(el) {
	if ($('#newNoteCreate').html() == '') {
		var url = "loadNotesPage"
		$.ajax( {
			type : "POST",
			url : "newNote.action",
			async : false,
			success : function(result) {
				$("#newNoteCreate").html(result);
				$("#dialogEditor").htmlarea();
				$(".ui-dialog").find("#noteDialog").find("iframe").contents()
						.find("body").html("&nbsp");
				$("#noteEntityId").val($("#tempEntityId").val());
				$("#noteEntityType").val($("#tempEntityType").val());
			},
			error : function(xhr, ajaxOptions, thrownError) {
				alert(xhr.status);
				alert(thrownError);
			}
		});
	} else {
		$('#newNoteCreate').html('');
	}
}

function notes(id, donor, obj, falg) {
	var url = "fetchNotes";
	var parameters = "{entityId:'" + id + "',entityType:'" + donor + "',flag:'"
			+ falg + "'}";
	addNewPopup('Notes', 'notesContainer', url, "700", "700", "jsonData="
			+ parameters);
	$('#noteView').html('');
	$("#tempEntityId").val(id);
	$("#tempEntityType").val(donor);
	$('#newNoteCreate').html('');
	$("iframe html body").html("123");
}
/*
 * function fetchnotes(entityId,entityType){ var applcationCode="STAFA"; var
 * jsonData =
 * "applnCode:'"+applcationCode+"',entityId:'"+entityId+"',entityType:'"+entityType+"' ";
 * var url="fetchNotes"; response = jsonDataCall(url,"jsonData={"+jsonData+"}");
 * alert("response.notesDetails"+response.notesDetails); var notesHtml="<table><tr><td>";
 * $(response.notesDetails).each(function() {
 * $(this).find("note").each(function(){ var
 * notecontent=$(this).find("notedescription").text(); var
 * createdOn=$(this).find("createdOn").text(); alert("notecontent"+notecontent);
 * alert("createdBy"+createdOn); notesHtml+="<td>"+notecontent+"</td><td>"+createdOn+"</td></tr></table>"
 * $("#existingNotes").html(notesHtml); }); }); }
 */
function savingNotes() {
	/*
	 * var applcationCode="STAFA"; $("#dialogEditor").val($("
	 * #noteDialog").find("iframe").contents().find("body").html()); var
	 * content=$("#dialogEditor").val();
	 * 
	 * alert("content"+content); content=content.replace(/</g,"#lt;");
	 * alert("content"+content); content=content.replace(/>/g,"#gt;");
	 * 
	 * alert("content"+content);
	 * 
	 * var entityId=$("#donor").val(); var jsonData =
	 * "applnCode:'"+applcationCode+"',notedescription:\""+content+"\",entityId:'"+entityId+"',entityType:'donor' ";
	 * alert("jsonData=="+jsonData) var url="savingNotes"; response =
	 * jsonDataCall(url,"jsonData={"+encodeURIComponent(jsonData)+"}");
	 */
	// alert("value=="+$("#dialogEditor").val());
	if ($("#dialogEditor").val() != "") {
		$.ajax( {
			type : "POST",
			url : "savingNotes.action?falg=1",
			async : false,
			data : $("#notesForm").serialize(),
			success : function(result) {
				// alert("result " + result );
			// alert("Notes Saved Successfully");
			// stafaAlert('Notes Saved Successfully','');
			alertify.alert("Notes Saved Successfully");
			// tempCount=parseInt(tempCount)+parseInt(1);

			// $(".subspan").html(tempCount);
			$(".ui-dialog").find("#notesContainer").html(" test ");
			// alert("trace 1");
			$(".ui-dialog").find("#notesContainer").html(result);
			// clearForm("notesForm");
			$(".subspan").html($("#listSize").val());

		},
		error : function(request, status, error) {
			alert("Error " + error);
		}
		});
	} else {
		// stafaAlert('No data to save','');
		alert("No data to save");
	}

}
function verifyeSign(eSignVal, mode) {

	var url = $("#nextOption").val();
	var eSignErroMsg = "Incorrect e-Signature. Please enter again."
	if (mode == "next") {
		$("#loginForm #flowNextFlag").val(true);
	} else {
		$("#loginForm #flowNextFlag").val(false);
	}
	var eSignFlag = false;
	try {
		var eSign = $("#" + eSignVal).val();
		if (eSign == "" || eSign == "e-Sign") {
			alertify.alert(eSignErroMsg);
			return;
		}
		var response;
		$("#" + eSignVal).val("");
		$.jCryption.encrypt(eSign, keys, function(encrypted) {
			encryptedEsign = encrypted;
			response = jsonDataCall("checkeSign", "eSign=" + encryptedEsign);
		});

		if (response != null) {
			var $result = $(response).filter('#errordiv');
			// alert("inside jsoncall--->"+$result.size());
			if ($result.size() > 0) {
				loadAction('autoLogOof.action');
				return false;
			}
			eSignFlag = response.eSignFlag;
			if (eSignFlag == 'true') {
				// if(mode=="popup"&& checkValidation() && savePopup()){
				if (mode == "popup") {
					// popup save
					var popupId = $("#" + eSignVal).closest(
							".ui-dialog-content").attr("id");
					// alert("popupId=="+popupId);
					if (checkValidation(popupId)) {
						// alert("checkValidation");
						savePopup();
					}
				} else if (mode == "save" && savePages()) {
					
				} else if (mode == "submitInCDMMain") {
					submitInCDMMain();
				} else if (mode == "cdmrsettings") {
					saveforNotification();
				} else if (mode == "saveforNotification") {
					saveforNotification();
				} 
				else if (mode == "saveforNotification1") {
					saveforNotification1();
				} 
				else if (mode == "next" && checkValidation()
						&& savePages(mode)) {
					$("#activity").val($("#hnextOption").val());
					if (url != null && url != "") {
						loadAction(url);
					}
				}
			} else {
				alertify.alert(eSignErroMsg);
			}

		} else {
			alertify.alert(eSignErroMsg);
		}

	} catch (e) {
		alertify.alert(e);
	}
	;
}

function addTracker(name, status, displayValue) {
	var trackerHtml = "<td><table class='workflowContent' cellpadding=0 cellspacing=0><tbody><tr>";
	if (status == "0" && (name != "HomePage" && name != "Logout")) {
		trackerHtml += '<td class= "processinitial"></td><td class="initial" onclick=previousPageTracker("' + displayValue + '")></td><td class= "processend"></td>';
	} else if (name != "HomePage" && name != "Logout") {

		trackerHtml += '<td class="processstarted"></td><td class="completed" onclick=CompletePageTracker("' + displayValue + '")></td><td class="processstarted"></td>';
	}
	trackerHtml += "</tr>";
	if (name != "HomePage" && name != "Logout") {
		trackerHtml += "<tr><td colspan='3' align='center'>" + name
				+ "</td></tr></table></td>";
	}

	$(".workFlowContainer").append(trackerHtml);
	// alert($(".workFlowContainer").html());
	$(".workFlowContainer").find(".processinitial").closest("table").parent()
			.prev().find(".completed").next().removeClass("processstarted")
			.addClass("processinitial");

}
function CompletePageTracker(url) {
	loadAction(url);
	$(".nextContainer").hide();
}
function previousPageTracker(url) {
	loadAction(url);
	$(".nextContainer").show();
}
function deleteAllCookies() {
	var cookies = document.cookie.split(";");
	for ( var i = 0; i < cookies.length; i++) {
		var cookie = cookies[i];
		var eqPos = cookie.indexOf("=");
		var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
		document.cookie = name + '="";-1; path=/';
	}
}

/* for show and hide widgets by mohan */

/*
 * function stafa_Time(){ $(".hasDatepicker").each(function(){ //
 * alert("Trace"); if($(this).hasClass("dateEntry dateclass")==false){ var
 * name=$(this).attr("name"); // alert("name="+name); var
 * name_index=name.indexOf("_"); // alert("name_index="+name_index);
 * if(name_index!=-1){ var timeName=name.substring(0,name_index); var
 * timeValue=$(this).val(); $(this).append("<input type='hidden'
 * name='"+timeName+"' id='"+timeName+"' value='"+timeValue+"'>"); } } }); }
 * function setStafaTime(obj){ var name=$(obj).attr("name"); var
 * name_index=name.indexOf("_"); if(name_index!=-1){ var
 * timeName=name.substring(0,name_index); var tempPrimeTime=$(obj).val();
 * $("#"+timeName).attr("value",tempPrimeTime); } }
 */
function checkHighlight() {
	$(".subDivLeftContent").each(function() {
		if ($(this).hasClass("selectedDiv")) {
			$(this).removeClass("selectedDiv");
		}
	});

}
function stafaAlert(msg, title) {

	// jAlert(msg,title);
	alert("Data Saved Successfully");

}
function anticoagulantsPopup(donor, donorProcedure) {
	// showPopUp("open","","","950","520");
	// alert("donor::"+donor+"donorPr:;"+donorProcedure);
	url = "loadAnticoagulants.action?donor=" + donor + "&donorProcedure="
			+ donorProcedure;
	$("#modalpopup").dialog( {
		autoOpen : false,
		title : "Anticoagulants and Other Additives",
		modal : true,
		width : 950,
		height : 520,
		close : function() {
			jQuery("#modalpopup").dialog("destroy");
			// ?BB hideHighting('dataLibraryTbody');

	}
	});
	$.ajax( {
		type : "POST",
		url : url,
		// async:false,
		success : function(result, status, error) {
			// $('.progress-indicator').css( 'display', 'block' );
		$("#modalpopup").html(result);
		// $('.progress-indicator').css( 'display', 'none' );
		$("select").uniform();

	},
	error : function(request, status, error) {
		alert("Error " + error);
		alert(request.responseText);
	}

	});

	$("#modalpopup").dialog("open");

	$("#anticoagulantsAdditives").css("width", "95%");
	$('.ui-dialog select').uniform();
	$("#error").hide();
}
var checkMedicateDonorflag = false;
var checkMedicateNurseflag = false;
function medicationpopup(donormrn, donor, donorProcedure) {
	// showPopUp("open","","","950","520");
	url = "loadmedication.action?donormrn=" + donormrn + "&donor=" + donor
			+ "&donorProcedure=" + donorProcedure;
	$("#modalpopup").dialog( {
		autoOpen : false,
		title : "Medications",
		modal : true,
		width : 950,
		height : 520,
		close : function() {
			jQuery("#modalpopup").dialog("destroy");
			// ?BB hideHighting('dataLibraryTbody');

	}
	});
	$.ajax( {
		type : "POST",
		url : url,
		// async:false,
		success : function(result, status, error) {
			// $('.progress-indicator').css( 'display', 'block' );
		$("#modalpopup").html(result);
		// $('.progress-indicator').css( 'display', 'none' );
		$("select").uniform();

	},
	error : function(request, status, error) {
		alert("Error " + error);
		alert(request.responseText);
	}

	});

	$("#modalpopup").dialog("open");

	$("input[name='medi_donorid']").live(
			"keypress",
			function(e) {

				if (e.keyCode == 13) {
					checkdonor();
					if (checkMedicateDonorflag != false
							&& checkMedicateNurseflag != false) {
						$("#medicationtabblediv").show();
					}
				}

			});
	$("input[name='medi_nurseid']").live(
			"keypress",
			function(e) {

				if (e.keyCode == 13) {
					checknurse();
					if (checkMedicateDonorflag != false
							&& checkMedicateNurseflag != false) {
						$("#medicationtabblediv").show();
					}
				}

			});

	$("#addnewMedication").css("width", "95%");
	$('.ui-dialog select').uniform();
	$("#error").hide();
}

function checkdonor() {

	var currentDonorid = $("#medi_donorid").val();
	if (currentDonorid == donormrn) {
		$("#donormrnVerified")
				.html(
						'<div align="center"><img src = "images/icons/verified.png" class="cursor"/>Verified</div>');
		$("#donormrnVerified").show();
		checkMedicateDonorflag = true;
		// $("#medicationtabblediv").show();
	} else {
		alert("Donor MRN Mismatch");
		$("#donormrnVerified")
				.html(
						'<div align="center"><img src = "images/icons/delete.gif" class="cursor"/>Mismatch</div>');
		$("#donormrnVerified").show();
		checkMedicateDonorflag = false;
		$("#nursemrnVerified").html('');
		$("#medi_nurseid").val("");
		$("#medicationtabblediv").hide();
	}
}

function checknurse() {

	var currentDonorid = $("#medi_donorid").val();
	var nurseentered = $("input[name='medi_nurseid']").val();
	var url = "verifyCustomerId";
	var jsonData = "{customerId:'" + nurseentered + "'}";
	var response = jsonDataCall(url, "jsonData=" + jsonData);
	if (response.verifyStatus == "true") {
		checkMedicateNurseflag = true;
		$("#nursemrnVerified")
				.html(
						'<div align="center"><img src = "images/icons/verified.png" class="cursor"/>Verified</div>');
		$("#nursemrnVerified").show();
		// $("#medicationtabblediv").show();
	} else {
		alert(" Nurse ID Mismatch");
		checkMedicateNurseflag = false;
		$("#nursemrnVerified")
				.html(
						'<div align="center"><img src = "images/icons/delete.gif" class="cursor"/>Mismatch</div>');
		$("#nursemrnVerified").show();
		$("#donormrnVerified").html('');
		$("#medi_donorid").val("");
		$("#medicationtabblediv").hide();
	}

}

function hide_slidewidgets() {
	outerLayout.hide('west');

}
function hide_slidecontrol() {
	outerLayout.hide('east');

}
function hide_innernorth() {
	clearTracker();
	innerLayout.removePane('north');

}
function show_innernorth() {
	// innerLayout.show('north',true);
	innerLayout.addPane('north');

}
function hide_innersouth() {
	outerLayout.hide('south');

}
function allWidgetHide() {

	innerLayout.removePane('north');
	outerLayout.hide('west');
	outerLayout.hide('east');
}
function show_slidewidgets(divid, visible) {
	// alert("divid=="+divid);

	outerLayout.show('west', visible);
	var src = $("#" + divid).html();
	// alert("dividHTml==="+src);
	$("#innerwest").html(src);
	$("#" + divid).html("");
}
function show_slidecontrol(divid) {

	outerLayout.show('east', false);
	var src = $("#" + divid).html();
	// alert(src);
	$("#innereast").html(src);

	$("#" + divid).html("");

}

function addPane() {
	outerLayout.addPane('north');
	outerLayout.addPane('south');
	/* innerLayout.addPane('south'); */
	innerLayout.addPane('north');
	outerLayout.addPane('west');
	outerLayout.addPane('east');

}
function removePane() {
	outerLayout.removePane('west');
	outerLayout.removePane('north');
	outerLayout.removePane('south');
	innerLayout.removePane('south');
	innerLayout.removePane('north');
	outerLayout.removePane('east');
}

// ----------- Mask

function applyMask(maskingObj, type, placeholder) {
	switch (type) {
	case 'ssn':
		$(maskingObj).mask("?999-99-9999", {
			placeholder : placeholder
		});
		break;

	}
}

// for Date
var DATE_FORMAT = 'M dd, yy';

function addDatepicker() {
	$(".calDate").each(function(i) {
		$(".calDate").datepicker( {
			dateFormat : DATE_FORMAT
		});
		// alert("col " +$(this).hasClass("hasDatepicker") + " / values "+
		// $(this).val());
			if ($(this).hasClass("hasDatepicker")) {
				var this_id = $(this).attr("id"); // current inputs id
				var new_id = this_id + i; // a new id
				$(this).attr("id", new_id); // change to new id
				// alert("new_id " + new_id);
				$(this).removeClass('hasDatepicker'); // remove hasDatepicker
														// class
				$(this).datepicker( {
					dateFormat : DATE_FORMAT
				}); // re-init datepicker
			} else {
				$(this).datepicker( {
					dateFormat : DATE_FORMAT
				});
			}
		});
}

function converHDate(dateStr) {
	var returnValue = "";

	if (dateStr != null && dateStr != "") {
		var tmpDate = dateStr.split("-");
		var tmpDate1 = new Date(tmpDate[0], tmpDate[1] - 1, tmpDate[2]
				.substring(0, 2));

		returnValue = $.datepicker.formatDate('M dd, yy', tmpDate1);
	}
	return returnValue;
}

function converDate(dateValue) {
	var returnValue = "";
	if (dateValue != null && dateValue != "") {

		var tmpDate = dateValue.split("-");
		var tday = eval(tmpDate[1]);
		var tmon = eval(tmpDate[0]) - 1;
		var tyear = eval(tmpDate[2]);

		var tmpDate1 = new Date(tyear, tmon, tday);
		returnValue = $.datepicker.formatDate('M dd, yy', new Date(tmpDate1));
		if (returnValue.indexOf("NaN") > 0) {
			var tmpDate = dateValue.split("-");
			var tmpDate1 = new Date(tmpDate[0], tmpDate[1] - 1, tmpDate[2]
					.substring(0, 2));
			returnValue = $.datepicker.formatDate('M dd, yy', tmpDate1);
		}

	}
	return returnValue;
}
function convertDateTime(dateValue) {
	var returnValue = "";

	if (dateValue != null && dateValue != "") {
		if ($.browser.msie) {
			/*
			 * var tmpDate = dateValue.split("-"); alert("tmpDate " + tmpDate +" /
			 * len----- " + tmpDate[0] + "/" + tmpDate[1]+ " , " +
			 * tmpDate[2].substring(0,2));
			 * 
			 * var tmpDate1 = new Date(tmpDate[0] ,tmpDate[1]-1,
			 * tmpDate[2].substring(0,2)); // alert("date
			 * val===>"+dateValue+"date==>"+tmpDate1); returnValue =
			 * $.datepicker.formatDate('M dd, yy', tmpDate1); //returnValue =
			 * dateValue;
			 */
			// alert("dateValue " + dateValue + "/"+ dateFormat(dateValue,
			// "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"));
			returnValue = dateFormat(dateValue, "UTC:mmm dd, yyyy HH:MM TT");
		} else {
			// returnValue = $.datepicker.formatDate('M dd, yy hh:mi', new
			// Date(dateValue));
			returnValue = dateFormat(dateValue, 'mmm dd, yyyy HH:MM TT');
		}

	}
	return returnValue;
}
function convertDateTime1(dateValue) {
	// alert("convertDateTime1 " + dateValue);
	var returnValue = " ";

	if (dateValue != null && dateValue != "") {
		// alert(" browser " + $.browser.msie);
		if ($.browser.msie) {
			var tmpDate = dateValue.split("-");
			// alert("tmpDate " + tmpDate +" / len----- " + tmpDate[0] + "/" +
			// tmpDate[1]+ " , " + tmpDate[2].substring(0,2));
			// alert("time " + tmpDate[2].substring(3,5) + " / " +
			// tmpDate[2].substring(6,8));
			var tmpDate1 = new Date(tmpDate[0], tmpDate[1] - 1, tmpDate[2]
					.substring(0, 2), tmpDate[2].substring(3, 5), tmpDate[2]
					.substring(6, 8));
			// alert("date val===>"+dateValue+"date==>"+tmpDate1);

			// minDate: new Date(2011, 04, 06, 1, 30),
			// minDate: new Date(y, m, d, h, mn)

			// alert("dateValue " + dateValue + "/"+ dateFormat(dateValue,
			// "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"));

			returnValue = dateFormat(tmpDate1, "mmm dd, yyyy HH:MM TT");
		} else {
			// returnValue = $.datepicker.formatDate('M dd, yy hh:mi', new
			// Date(dateValue));
			returnValue = dateFormat(dateValue, 'mmm dd, yyyy HH:MM TT');
		}

	}
	return returnValue;
}
function forgotpassword() {
	$("#errormesssagetd").html("");
	$.jCryption.getKeys("getEncryptKey.action?generateKeypair=true", function(receivedKeys) {});
	$.ajax({
		type:"POST",
		url:"checkForUserExistance.action",
		data:{from:$("#from").val(), notifyTo:$("#notifyTo").val()},
		success:function(response){
			if(response.statusCode == "true"){
				$.ajax( {
					type : "POST",
					url : "getForgotPassword.action",
					data : $("#forgotform").serialize(),
					success : function(result) {
					alert("Reset password initiated.");
						location.reload();
						// $("#errormesssagetd").html(data.errorMessage);
					$("#forgotpasswddiv").dialog("close");

				},
				error : function() {
					alert("Error ");
				}

				});
				return false;
			}else{
				alert("User does not exist.");
				location.reload();
			}
		}
	});
	
	
	
	
}

var no_of_pages = 5;

/* Datatable Pagination Ends */

var confirm_deleteMsg = 'Delete selected records ?';
var err_delete = 'Please select any record to Delete';
var err_edit = 'Please select any record to Edit';
var save_success_msg = 'Data Saved Successfully';

var error_img = '#FFFFCC url(images/icons/Error.png)no-repeat 8% 38%';
var save_img = '#FFFFCC url(images/icons/accept1.png)no-repeat 17% 38%';

function alertbox(msg, type) {
	var bg;
	if (type == "SUCCESS") {
		bg = save_img;
	} else if (type == "ERROR") {
		bg = error_img;
	}

	$.blockUI( {
		message : msg,
		centerY : 0,
		css : {
			top : '40%',
			border : '2px solid #E6E6B8',
			background : bg
		},
		timeout : 2000

	});

}

/* palceholder */
/*
 * end placeholder
 * 
 * function sortable(){ $( ".column" ).sortable().sortable({ connectWith:
 * ".column", cursor: 'move', cancel: ".portlet-content" }); }
 */

function portletMinus() {
	$(".portlet")
			.each(
					function() {
						if (!$(this).find(".portlet-header").find("span").length > 0) {
							$(this)
									.addClass(
											"ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
									.find(".portlet-header")
									.addClass("ui-widget-header ui-corner-all")
									.prepend(
											"<span class='ui-icon ui-icon-minusthick'></span>");
							$(this).find(".portlet-header .ui-icon").click(
									function() {
										$(this).toggleClass(
												"ui-icon-minusthick")
												.toggleClass(
														"ui-icon-plusthick");
										$(this).parents(".portlet:first").find(
												".portlet-content").toggle();
									});
						}
					});
}
function changeBreadCrumb(link) {
	var breadCrumbHtml = "";
	// alert(link)
	if (link == "Qc") {
		breadCrumbHtml += "<a href='#' onclick=\"loadAction('qclib.action')\">Qc Test Library</a>";
		$("#breadCrumbLink").html(breadCrumbHtml);
	}
	if (link == "Immuno") {
		breadCrumbHtml += "<a href='#' onclick=\"loadAction('fetchImmnoPhenoData.action?immuno=immuno')\">Immunophenotyping</a>";
		$("#breadCrumbLink").html(breadCrumbHtml);
	}
}
/*
 * function updateBreadCrumb(link,action){ var breadCrumbHtml="<a href='#'
 * onclick=\"\">"+link+"</a>"; $("#breadCrumbLink").append(breadCrumbHtml); }
 */

function setColumnManager(tableId) {
	var showFlag1 = false;
	$('#targetall1').hide();
	$('#' + tableId).columnManager( {
		listTargetID : 'targetall1',
		onClass : 'advon',
		offClass : 'advoff',
		saveState : false
	});
	$('#ulSelectColumn').click(
			function() {
				if (showFlag1) {
					$('#targetall1').fadeOut();
					showFlag1 = false;
				} else {
					$("#targetall1").find('tbody').find("tr:first").find("td")
							.css("background", "#AAAAAA");
					$("#targetall1").find('tbody').find("tr:first").find(
							"td:first").removeClass("advon addoff");
					$("#targetall1").find('tbody').find("tr:first").find(
							"td:eq(1)").text("Restore Default");
					$("#targetall1").addClass("coltable");
					$("#targetall1").find('tbody').find("tr").find("td")
							.addClass("colcolumn");
					$('#targetall1').fadeIn();
					showFlag1 = true;
				}
			});
	$('#targetall1 tbody tr:first td').bind('click', function() {
		restoreAll(this);
	})
}
function restoreAll(el) {
	var tbodyObj = $(el).parent().parent();
	var isSelected = 0;

	$(tbodyObj).find('tr').each(function() {
		var temp = $(this).find('td:first').hasClass('advoff');
		if (temp) {
			isSelected = 1;
			return false;
		}

	});
	if (isSelected > 0) {
		$(tbodyObj).find('tr').each(function() {
			if ($(this).find('td:first').hasClass('advoff')) {
				$(this).triggerHandler("click");
			}
		});
	}

}

function getProductId(tableRowObj) {
	// alert(tableRowObj.rowIndex());
	var num = $(tableRowObj).index()

	/*
	 * if(num % 2 == 0) { // It's even $("#fkPersonProduct").val("5");
	 * $("#fkPersonProduct1").val("APH4139"); } else { // It's odd
	 * $("#fkPersonProduct").val("6"); $("#fkPersonProduct1").val("APH4140"); }
	 */

}
function func_placeholder() {
	// var placeholder = 'placeholder' in document.createElement('input');
	if ($.browser.msie && parseInt($.browser.version, 10) == 8) {
		$(".dateEntry").bind("change", function() {
			$(".dateEntry").css("color", "black");
		});
		$(".timepickers").bind("change", function() {
			$(".timepickers").css("color", "black");
		});

		$(":text").each(function(i, el) {
			var val = $(el).val();
			var palceholder = $(el).attr("placeholder");
			if (val != palceholder) {
				$(el).css("color", "black");
			}
		});
	}

}
function count() {

}
function sessionCheck() {

	// alert("sessionCheck" + sessiontimeout);
	var beforeTime = sessiontimeout;
	var it;
	var timeout = 60;

	$(document).bind(
			"idle.idleTimer",
			function() {

				if ($("#frmLogin").html() != null
						&& $("#frmLogin").html() != "") {
					it = setInterval(count, beforeTime);
				} else {

					clearInterval(it);
					$('#highlightCountdown').countdown( {
						until : 60,
						onTick : highlightLast5,
						format : 'S'
					});
					var sessionouts = beforeTime - it;

					if (window.opener != 'null' && window.opener != null
							&& window.opener != 'undefined') {
						window.close();
					}
					// window.location.href="?error=sessionexpired";

				}
			});

	$(document).bind("active.idleTimer", function() {
		it = setInterval(count, beforeTime);

	});

	$.idleTimer(beforeTime);

	$('#highlightButton').bind('click', function() {
		it = setInterval(count, beforeTime);
		$('#highlightCountdown').countdown('destroy');
		// loadAction('maintainSession.action');
			jsonDataCall("maintainSession", "");
			$('#sessionout').dialog('close');

		});
}

function getDonorDetails(donor, donorProcedure) {

	$("#donor").val(donor);
	$("#donorProcedure").val(donorProcedure);
	loadAction('loadDonorDetails.action?donor=' + donor + "&donorProcedure="
			+ donorProcedure);

	// response = ajaxCall("loadDonorDetails","aphresis");
	// alert("In home page"+response);
	// $('#main').html(response);
}
function highlightLast5(periods) {
	if ($.countdown.periodsToSeconds(periods) <= 60) {
		$('#sessionout').dialog( {
			autoOpen : false,
			resizable : false,
			width : 250,
			height : 150,
			closeOnEscape : false,
			modal : true
		}).siblings('.ui-dialog-titlebar').remove();
		$('#sessionout').dialog("open");
	}
	if ($.countdown.periodsToSeconds(periods) <= 5) {
		$("#highlightCountdown").addClass('texthighlight');
		if ($.countdown.periodsToSeconds(periods) <= 1) { // alert("sighn");
			$('#sessionout').dialog('close');
			loadAction('autoLogOof.action')
		}
	}
}
$(document).ready(function() {
	$('#blind').html("<<");
	var $box = $('#box').wrap('<div id="box-outer"></div>');
	$('#blind').click(function() {
		$box.blindToggle('fast');
	});
	$.getScript("js/placeholder.js");
	// func_placeholder();

	});

/*
 * jQuery.fn.blindToggle = function(speed, easing, callback) { var h =
 * this.width() + parseInt(this.css('paddingLeft')) +
 * parseInt(this.css('paddingRight')); if(parseInt(this.css('marginLeft'))<0){
 * $('#forfloat_right').animate( { width: "82%" }, { queue: false, duration: 200
 * }); alert("111"); $('#blind').html("<<"); return
 * this.animate({marginLeft:0},speed, easing, callback); } else{ $(
 * "#forfloat_right" ).animate( { width: "95%" }, { queue: false, duration: 250
 * });
 * 
 * $('#blind').html(">>"); return this.animate({marginLeft:-h},speed, easing,
 * callback);
 *  } };
 */

function loadFormPage(pageUrl, params) {
	// alert(" url " + params);
	try {
		// $('#formName').val(params);
		$('#main').load(pageUrl, function() {
			$('#formName').val(params);
			getFormData();
		});
		// sortable();
	} catch (err) {
		alert(" error in load page " + err);
	}
	// zooming();
}
function showSubQuest(el) {
	// alert("!");
	var id = $(el).attr("id");
	// $('#dropdown option:selected').text();
	var value = $("#" + id + ' option:selected').text().replace(/\s/g, "");
	// alert("div"+id+value );
	// value=value.replace
	groupShow("div" + id + value, el);
	// hideResponse(id,value);

}
function loadPage(pageUrl) {
	// alert(" url " + pageUrl);
	try {
		document.getElementById("main").innerHTML = "";
		$('#main').load(pageUrl);
		// sortable();
	} catch (err) {
		alert(" error in load page " + err);
	}
	// zooming();
}

function zoomingin(obj) {
	// $('.searching1').focusin(function () {
	$(obj).animate( {
		width : 210
	}, "medium");
	$(obj).addClass("zoom");

	// });
}
function zoomingout(obj) {
	// $('.searching1').focusout(function(){
	$(obj).removeClass("zoom");
	$(obj).animate( {
		width : 184
	}, "medium");
	$(obj).addClass("changecolor");
	// });
}
function showprogressMgs() {
	// alert("showprogressMgs");
	$('#ieprogressindicator').dialog( {
		autoOpen : false,
		resizable : false,
		width : 90,
		height : 90,
		closeOnEscape : false,
		modal : true
	}).siblings('.ui-dialog-titlebar').remove();
	$('#ieprogressindicator').dialog("open");
}
function closeprogressMsg() {
	setTimeout(function() {
		$('#ieprogressindicator').dialog("close");
		$('#ieprogressindicator').dialog("destroy");
	}, 1000);
}
function loadAction(url) {
	// alert("loadAction");
	// alert("load action 2 : "+url);
	// alert("$.browser.msie"+$.browser.msie);
	// $('.progress-indicator').css( 'display', 'block' );
	// alert(url+"?"+param);
	var msflag = false;
	if ($.browser.msie) {
		msflag = true;
		if (msflag) {
			showprogressMgs();
		}
	}
	innerLayout.show('south', true);
	innerLayout.show('north', true);

	try {
		// $('#main').html("");
		document.getElementById("main").innerHTML = "";

		$.ajax( {
			type : "POST",
			url : url,
			async : false,
			success : function(result) {
				// alert(" success" + result);
			if (url == "logout.action") {
				window.location = contextpath + "/pages/login.jsp";
			} else {

				$('#main').html(result);
				// func_placeholder();
			// sortable();

			//if (window.console || window.console.firebug) {
				//console.clear();
			//}
			//deleteAllCookies();
		}
	},
	error : function(request, status, error) {
		alert("Error " + error);

	}

		});

	} catch (err) {
		alert(" load action error " + err);
	}
	// alert("end of load action");
	// zooming();
	if ($.browser.msie) {

		closeprogressMsg();
		msflag = false
	}

}

function showNotes(mode) {

	$("#noteDialog").dialog( {
		autoOpen : false,
		title : 'Notes',
		modal : true,
		width : 470,
		height : 360,
		close : function() {
			jQuery("#noteDialog").dialog("destroy");
			$("#dialogEditor").htmlarea("dispose");
		}
	});
	$("#noteDialog").dialog(mode);
	$("#noteDialog").css("width", "100%");
	$("#dialogEditor").htmlarea();
}

function applyNotesPopUp() {
	var imgs = document.getElementsByTagName("img");
	alert(imgs.length);
	for (i = 0; i < imgs.length; i++) {
		alert($("#imgs").attr("src"));
	}
}

/** Table row selected Row color changes */
var oldObj;
function changeClass(newObj) {

	if ($(newObj).hasClass('tableRowSelected')) {
		$(newObj).removeClass('tableRowSelected');
		oldObj = null
		return;
	}
	if (oldObj) {
		$(oldObj).removeClass('tableRowSelected');
	}

	$(newObj).addClass('tableRowSelected');
	oldObj = $(newObj);
}

var oldObj1;
function changeMenuClass(newObj) {

	/*
	 * if($(newObj).hasClass('tableRowSelected')){
	 * $(newObj).removeClass('tableRowSelected'); oldObj1 = null return; }
	 */
	if (oldObj1) {
		$(oldObj1).removeClass('selectedMenu');
	}

	$(newObj).addClass('selectedMenu');
	oldObj1 = $(newObj);
}

function logOut(url) {
	// loadAction('logout.action');
	window.location.reload(true);
	/*
	 * var response = ajaxCall(url,""); $("#main").html(response);
	 */
}

function signout() {
	// window.location = cotextpath + "/signout";
	$.ajax( {
		type : "POST",
		url : contextpath + "/logout.action",
		async : false,
		success : function(result) {
			window.location = contextpath + "/pages/login.jsp";
		},
		error : function(request, status, error) {
			alert("Error " + error);
			alert(request.responseText);
		}

	});
}

function clearForm(form) {
	// iterate over all of the inputs for the form
	// element that was passed in
	$(':input', form).each(function() {
		var type = this.type;
		var tag = this.tagName.toLowerCase(); // normalize case
			// it's ok to reset the value attr of text inputs,
			// password inputs, and textareas
			if (type == 'text' || type == 'password' || tag == 'textarea')
				this.value = "";
			// checkboxes and radios need to have their checked state cleared
			// but should *not* have their 'value' changed
			else if (type == 'checkbox' || type == 'radio')
				this.checked = false;
			// select elements need to have their 'selectedIndex' property set
			// to -1
			// (this works for both single and multiple select elements)
			else if (tag == 'select')
				this.selectedIndex = -1;
		});
};

function confirmIfNo() {
	var answer = confirm("You have selected No. Please click 'Ok' to continue.!")
	/*
	 * if (answer) else
	 */
}
function hideHighting(param1) {
	$('#' + param1 + ' tr').each(function(i) {
		if ($(this).hasClass("highlighting")) {
			var id = $(this).attr("id");
			$("#" + id).removeClass("highlighting");
			$("#" + id).addClass(id);
			$("#" + id).attr("id", "");
		}
	});
}

function showPopUp(mode, Id, title, wd, ht) {
	// ?BB jQuery("#"+Id).dialog("destroy");
	/*
	 * $(".ui-dialog").find("#"+Id).each(function(){ $(this).remove();
	 * $(this).parent().remove(); });
	 */
	// alert("show popup");
	var screenwidth = screen.width;
	var screenheight = screen.height;
	if (screenwidth <= 1024 && screenheight <= 768) {
		wd = "950";
		ht = "500";
	}
	ht = "500";
	$("#" + Id).dialog( {
		autoOpen : false,
		title : title,
		modal : true,
		width : wd,
		height : ht,
		close : function() {
			jQuery("#" + Id).dialog("destroy");
			// ?BB hideHighting('dataLibraryTbody');
	}
	});
	$("#" + Id).dialog(mode);
	$("#" + Id).css("width", "95%");
	$("#error").hide();
	// alert("end of show popup");
}
function showPopupForm(title, Id, height, width) {
	var screenwidth = screen.width;
	var screenheight = screen.height;
	if (screenwidth <= 1024 && screenheight <= 768) {
		width = "950";
		height = "500";
	}
	height = "500";
	$('.progress-indicator').css('display', 'block');
	$("#" + Id).dialog( {
		autoOpen : false,
		title : title,
		resizable : true,
		closeText : '',
		closeOnEscape : false,
		modal : true,
		width : width,
		height : height,
		close : function() {
			jQuery("#" + Id).dialog("destroy");
		}
	});
	// var cont = loadFormPage('jsp/CIBMTR/CIBMTR_Form.jsp','2400 most updated
	// ');

	var url = "loadCibmtrForm";
	var data = "2400 most updated ";
	// var data = "2006 - HSCT Infusion";
	var response = ajaxCallWithParam(url, data);
	// alert("response : "+response);
	if (response != null) {
		var tempData = response.pagedata;
		$(".ui-dialog #formsDivTableId").html(
				response.pagedata.replace(/hideTr/g, "hideTr1"));
	}
	$("#" + Id).dialog("open");
	$('.ui-dialog select').uniform();
	$('.progress-indicator').css('display', 'none');
}

function createPopUp(mode, Id, title, wd, ht) {
	// alert("trace 1");
	$(".ui-dialog").find("#modelPopup").each(function() {
		$(this).remove();
		$(this).parent().remove();
	});

	$("#modelPopup").html($("#" + Id).html());
	showPopUp("open", "modelPopup", title, wd, ht);

	// ?BB
	/*
	 * $("#modelPopup").dialog( {autoOpen: true, title: title , modal: true,
	 * width:wd, height:ht, close: function() { //?BB
	 * jQuery("#modelPopup").dialog("destroy"); } } );
	 */
}
function stafaPopUp(mode, Id, title, wd, ht) {
	// alert("trace 1");
	$('.progress-indicator').css('display', 'block');
	$(".ui-dialog").find("#modelPopup").each(function() {
		// $(this).remove();
			// $(this).parent().remove();
			$(this).dialog("destroy");
		});
	$("#modelPopup").html("");
	// alert(" mode " + mode + " / modelpopup " + $("#modelPopup").html());
	// alert(Id + "/1 html"+ $("#"+Id).html());
	if (mode == "open") {
		$("#modelPopup").html($("#" + Id).html());
		// alert("html " + $("#modelPopup").html());
		showPopUp("open", "modelPopup", title, wd, ht);
	} else if (mode == "close") {
		$("#modelPopup").dialog("destroy");
		$("#modelPopup").html("");
	}
	$('.progress-indicator').css('display', 'none');
	// alert(Id + "/2 html"+ $("#"+Id).html());
}

function addNewPopup(title, Id, url, height, width, data) {
	jQuery("#" + Id).dialog("destroy");
	jQuery("#" + Id).remove();
	$("#modeldialog").append('<div id="' + Id + '"></div>');
	var screenwidth = screen.width;
	var screenheight = screen.height;
	// alert("screenheight" + screenwidth);
	if (screenwidth <= 1024 && screenheight <= 768) {
		width = "950";

	}
	$('.progress-indicator').css('display', 'block');
	height = "500";
	/*
	 * //?BB $("#"+Id).dialog( {autoOpen: false, title: title , resizable: true,
	 * closeText: '', closeOnEscape: false , modal: true, width : width, height :
	 * height, close: function() { jQuery("#"+Id).dialog("destroy");
	 *  } } );
	 */
	$.ajax( {
		type : "POST",
		url : url,
		data : data,
		success : function(result, status, error) {
			$('.progress-indicator').css('display', 'block');
			// alert("result=="+result);
		// ?BB $("#"+Id).html(result);

		$("#" + Id).html(result).dialog( {
			autoOpen : true,
			title : title,
			modal : true,
			width : width,
			height : height,
			close : function() {
				jQuery("#" + Id).dialog("destroy");
				// $(this).html(" ");
			$(this).remove();
			$("#modeldialog").append('<div id="' + Id + '"></div>');

		}
		});

		$('.progress-indicator').css('display', 'none');
		$("select").uniform();

	},
	error : function(request, status, error) {
		alert("Error " + error);
		alert(request.responseText);
	}

	});
	// ? $("#"+Id).dialog("open");
	$("#" + Id).css("width", "95%");
	// $("#dialogContent").css("width","95%");

	$('.ui-dialog select').uniform();
	$('.progress-indicator').css('display', 'none');
}

function destroyPopup(Id) {
	// alert("destroy");
	$("#" + Id).dialog("close");
	jQuery("#" + Id).dialog("destroy");
	// alert("end of destroy");
}
function getChildcreationTable() {
	var fileName = "xml/stafa.xml";
	var xmlData = getXmlFile(fileName);

	var helptmpObj = [];
	var helptmpObj1 = [];

	var table = $(xmlData).find("createchild").find("tables").find("table");
	var tableId = $(table).find("tableid").text();
	var tableHeaders = $(table).find("headers").find("head");
	var tableValues = $(table).find("values");
	var columManager = "<th style=\"width:50px\"id=\"thSelectColumn\"><div class=\"cmDiv1\"><table id=\"ulSelectColumn\" class=\"clickMenu\">	  <tr><td class=\"main hover\"><img title=\"select columns\" alt=\"select columns\" src=\"images/icons/selectcol.png\">	   <div class=\"outerbox inner\" style=\"position: absolute; display: block;\">             <table id=\"targetall1\" class=\"innerBox \" width=\"106%\">     </table>           </div>          </tr></td>        </table>     <div style=\"clear: both; visibility: hidden;\"></div>    </div>   </th>";
	var tableHead = "<thead><tr>" + columManager;
	var tableValue = "";
	$(tableHeaders)
			.each(
					function(index) {
						if (index == 0) {
							tableHead += "<th> <input type='checkbox' id='deleteallchild' onclick='checkall();'/>"
									+ $(this).text() + "</th>";
						} else {
							tableHead += "<th>" + $(this).text() + "</th>";
						}
					});

	tableHead += "</tr></thead><tbody class='tbody' id='childgeneration'>";

	/*
	 * $(tableValues).each(function(index) { tableValue+="<tr onclick='getProductId(this);'><td align='center'>-</td><td>"+$(this).find("recipientid").text()+"</td><td>"+$(this).find("recipientname").text()+"</td><td>"+$(this).find("productid").text()+"</td><td>"+$(this).find("productsource").text()+"</td><td>"+$(this).find("producttype").text()+"</td><td>"+$(this).find("processingfecility").text()+"</td><td>"+$(this).find("accesstime").text()+"</td><td>"+$(this).find("status").text()+"</td></tr>";
	 * });
	 */
	tableValue += "</tbody>";
	var tableContents = tableHead + tableValue;

	$(tableId).append(tableContents);
	// alert(tableContents);
	$("#childcreation").html(tableContents);
}
function getCommonTable(id) {
	var fileName = "xml/stafa.xml";
	var xmlData = getXmlFile(fileName);

	var helptmpObj = [];
	var helptmpObj1 = [];

	var table = $(xmlData).find("Panelconfigheader").find("tables").find(
			"table");
	var tableId = $(table).find("tableid").text();
	var tableHeaders = $(table).find("headers").find("head");
	var tableValues = $(table).find("values");
	var columManager = "<th style=\"width:50px\"id=\"thSelectColumn\"><div class=\"cmDiv1\"><table id=\"ulSelectColumn\" class=\"clickMenu\">	  <tr><td class=\"main hover\"><img title=\"select columns\" alt=\"select columns\" src=\"images/icons/selectcol.png\">	   <div class=\"outerbox inner\" style=\"position: absolute; display: block;\">             <table id=\"targetall1\" class=\"innerBox \" width=\"100%\">     </table>           </div>          </tr></td>        </table>     <div style=\"clear: both; visibility: hidden;\"></div>    </div>   </th>";
	var tableHead = "<thead><tr>" + columManager;
	var tableValue = "";
	$(tableHeaders)
			.each(
					function(index) {
						if (index == 0) {
							tableHead += "<th> <input type='checkbox' id='deleteallchild' onclick='checkall();'/>"
									+ $(this).text() + "</th>";
						} else {
							tableHead += "<th>" + $(this).text() + "</th>";
						}
					});

	tableHead += "</tr></thead><tbody class='tbody' id=''>";

	/*
	 * $(tableValues).each(function(index) { tableValue+="<tr onclick='getProductId(this);'><td align='center'>-</td><td>"+$(this).find("recipientid").text()+"</td><td>"+$(this).find("recipientname").text()+"</td><td>"+$(this).find("productid").text()+"</td><td>"+$(this).find("productsource").text()+"</td><td>"+$(this).find("producttype").text()+"</td><td>"+$(this).find("processingfecility").text()+"</td><td>"+$(this).find("accesstime").text()+"</td><td>"+$(this).find("status").text()+"</td></tr>";
	 * });
	 */
	tableValue += "</tbody>";
	var tableContents = tableHead + tableValue;

	$(tableId).append(tableContents);
	// alert(tableContents);
	$("#" + id).html(tableContents);
}

function getAccessionTable() {
	// alert("test-2");
	var fileName = "xml/stafa.xml";
	var xmlData = getXmlFile(fileName);

	var helptmpObj = [];
	var helptmpObj1 = [];

	var table = $(xmlData).find("accession").find("tables").find("table");
	var tableId = $(table).find("tableid").text();
	var tableHeaders = $(table).find("headers").find("head");
	var tableValues = $(table).find("values");
	var columManager = "<th style=\"width:50px\"id=\"thSelectColumn\"><div class=\"cmDiv1\"><table id=\"ulSelectColumn\" class=\"clickMenu\">	  <tr><td class=\"main hover\"><img title=\"select columns\" alt=\"select columns\" src=\"images/icons/selectcol.png\">	   <div class=\"outerbox inner\" style=\"position: absolute; display: block;\">             <table id=\"targetall1\" class=\"innerBox \" width=\"106%\">     </table>           </div>          </tr></td>        </table>     <div style=\"clear: both; visibility: hidden;\"></div>    </div>   </th>";
	var tableHead = "<thead><tr>" + columManager;
	var tableValue = "";
	$(tableHeaders).each(function(index) {
		tableHead += "<th>" + $(this).text() + "</th>";
	});

	tableHead += "</tr></thead><tbody class='tbody'>";

	/*
	 * $(tableValues).each(function(index) { tableValue+="<tr onclick='getProductId(this);'><td align='center'>-</td><td>"+$(this).find("recipientid").text()+"</td><td>"+$(this).find("recipientname").text()+"</td><td>"+$(this).find("productid").text()+"</td><td>"+$(this).find("productsource").text()+"</td><td>"+$(this).find("producttype").text()+"</td><td>"+$(this).find("processingfecility").text()+"</td><td>"+$(this).find("accesstime").text()+"</td><td>"+$(this).find("status").text()+"</td></tr>";
	 * });
	 */
	tableValue += "</tbody>";
	var tableContents = tableHead + tableValue;
	$(tableId).append(tableContents);
	// alert(tableContents);
	$("#accessionTable").html(tableContents);
}

function getAcquisitionTable() {
	// alert("test-2");
	var fileName = "xml/stafa.xml";
	var xmlData = getXmlFile(fileName);
	// alert("xml data " + xmlData);
	// alert($(xmlData).text());

	var helptmpObj = [];
	var helptmpObj1 = [];
	// alert($(xmlData).find("acquisition").text());

	// alert($(xmlData).find("acquisition").find("tables").text());
	var table = $(xmlData).find("acquisition").find("tables").find("table");
	var tableId = $(table).find("tableid").text();
	var tableHeaders = $(table).find("headers").find("head");
	var tableValues = $(table).find("values");
	var columManager = "<th style=\"width:50px\"id=\"thSelectColumn\"><div class=\"cmDiv1\"><table id=\"ulSelectColumn\" class=\"clickMenu\">	  <tr><td class=\"main hover\"><img title=\"select columns\" alt=\"select columns\" src=\"images/icons/selectcol.png\">	   <div class=\"outerbox inner\" style=\"position: absolute; display: block;\">             <table id=\"targetall1\" class=\"innerBox \" width=\"100%\">     </table>           </div>          </tr></td>        </table>     <div style=\"clear: both; visibility: hidden;\"></div>    </div>   </th>";
	var tableHead = "<thead><tr>" + columManager;
	var tableValue = "";
	$(tableHeaders).each(function(index) {
		tableHead += "<th>" + $(this).text() + "</th>";
	});

	tableHead += "</tr></thead><tbody class='tbody'>";

	/*
	 * $(tableValues).each(function(index) { //alert(index + ': ' +
	 * $(this).text()); //var tableName = $(this).find("tablename").text(); //"<td>"+$(this).find("recipientid").text()+"</td><td>"+$(this).find("recipientname").text()+"</td><td>"+$(this).find("producttype").text()+"</td>productsource").text()+"</td><td>"+$(this).find("donorid").text()+"</td><td>"+$(this).find("donorname").text()+"</td><td>"+$(this).find("donorlocation").text()+"</td><td>"+$(this).find("processingfecility").text()+"</td><td>"+$(this).find("proposeddisposition").text()+"</td><td>"+$(this).find("productappearance").text()+"</td><td>"+$(this).find("productid").text()+"</td><td>"+$(this).find("collectionsection").text()+"</td>
	 * 
	 * //tableValue+="<tr ><td>"+$(this).find("recipientid").text()+"</td><td>"+$(this).find("recipientname").text()+"</td><td>"+$(this).find("producttype").text()+"</td><td>"+$(this).find("productid").text()+"</td><td>"+$(this).find("harvestingfacility").text()+"</td><td>"+$(this).find("acquisitiontime").text()+"</td></tr>";
	 * 
	 * tableValue+="<tr onclick='getProductId(this);'><td align='center'>-</td><td>"+$(this).find("recipientid").text()+"</td><td>"+$(this).find("recipientname").text()+"</td><td>"+$(this).find("producttype").text()+"</td><td>"+$(this).find("productsource").text()+"</td><td>"+$(this).find("donorid").text()+"</td><td>"+$(this).find("donorname").text()+"</td><td>"+$(this).find("donorlocation").text()+"</td><td>"+$(this).find("processingfecility").text()+"</td><td>"+$(this).find("proposeddisposition").text()+"</td><td>"+$(this).find("productappearance").text()+"</td><td>"+$(this).find("productid").text()+"</td><td>"+$(this).find("collectionsection").text()+"</td><td>"+$(this).find("status").text()+"</td></tr>";
	 * 
	 * });
	 */
	tableValue += "</tbody>";
	// alert(tableValue);
	// alert(tableId);
	var tableContents = tableHead + tableValue;
	// alert("----------------"+tableContents);

	$(tableId).append(tableContents);
	// alert("<-->"+$(tableId));
	// alert(tableContents);
	$("#acquisitionTable").html(tableContents);
	// alert("-->"+$(tableId));
}

/** Xml file read start */
// / get xml file
function getXmlFile(fileName) {
	// alert(fileName);
	var resXml = "";

	var fileurl = fileName;

	// alert("fileurl " + fileurl);
	$.ajaxSetup( {
		url : fileurl
	});
	$.ajax( {
		type : "GET",

		dataType : "xml",
		async : false,
		cache : false,
		success : function(xml) {
			resXml = xml;
		},
		complete : function(xhr, status) {
			// alert("status " +status);
		if (status == 'parsererror') {
			xmlDoc = null;
			if (document.implementation.createDocument) {
				// code for Firefox, Mozilla, Opera, etc.
		var xmlhttp = new window.XMLHttpRequest();
		xmlhttp.open("GET", fileurl, false);
		xmlhttp.send(null);
		xmlDoc = xmlhttp.responseXML.documentElement;
	} else if (window.ActiveXObject) {

		xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async = "false";
		xmlDoc.loadXML(xhr.responseText);
	} else if (window.DOMParser) {
		// others
		parser = new DOMParser();
		xmlDoc = parser.parseFromString(xhr.responseText, "text/xml");
	} else {
		// 
		alert('Your browser cannot handle this script');
	}
	resXml = xmlDoc;
}

},
error : function(xmlhttp, status, error) {
// alert("Err " + error);
	}
	});
	// alert("resXml " + resXml);
	return resXml;
}

/** XML file read end */

/** ***Accession Scripts Start * */

function attachDocument() {
	setAttachmentDialog();
	$('#attachDocument').dialog('open');
}
function setAttachmentDialog() {
	jQuery("#attachDocument").dialog("destroy");
	$("#attachDocument").dialog( {
		autoOpen : false,
		modal : true,
		width : 650,
		hieght : 800,
		close : function() {
			jQuery("#attachDocument").dialog("destroy");
		}
	});
}

/** ***Accession Scripts End * */

/** By Mari */
/*
 * function loadAccessionPage(){ $.ajax({ type: "POST", url:
 * "getAccession.action", //data : {'eResIRB':eResIRB}, data :
 * {'accLocation':'acqlocation','productType':'producttype','productSource':'prodsourc','diagnosis':'diagnosis','aboRH':'abo/rh','begStatus':'begstatus','planProtocol':'planprotocol','reciprId':'reciprid','prodRegistry':'prodregistry','procSelec':'procfacility','prodprotocol':'prodprotocol'},
 * async:false, success: function (result){ $('#main').html(result); }, error:
 * function (request, status, error) { alert("Error " + error); } }); }
 * 
 * function loadAcquisitionPage(){ $.ajax({ type: "POST", url:
 * "getAcquisition.action", data :
 * {'aquLocation':'acqlocation','productType':'producttype','finalDisp':'finaldispos','donorLoc':'location','procSelec':'procfacility','productSource':'prodsource'},
 * async:false, success: function (result){ $('#main').html(result); }, error:
 * function (request, status, error) { alert("Error " + error); } }); }
 */

// Check with code to hide the carcode;
function hideBarcodes() {
	$("#imgtip0").css("display", "none");
	$("#imgtip1").css("display", "none");
	$("#imgtip2").css("display", "none");
	$("#imgtip3").css("display", "none");
}

/* Convert Html table to json* */
function getJsonFromHtmlTable(id, allColumns) {
	var tableObject = "";
	// var columns = ["pkAlias","aliasId","aliasSource","aliasType"];
	var columns = allColumns;

	// tableObject = $('#'+id+' tr').map(function(i) {
	tableObject = $(id).find('tr').map(function(i) {
		var row = {};
		// alert("s-->"+i);
			// Find all of the table cells on this row.
			$(this).find('td').each(function(i) {
				// Determine the cell's column name by comparing its index
					// within the row with the columns list we built previously.
					var rowName = columns[i];

					// Add a new property to the row object, using this cell's
					// column name as the key and the cell's text as the value.
					row[rowName] = $.trim($(this).text());
				});

			// Finally, return the row's object representation, to be included
			// in the array that $.map() ultimately returns.
			return row;

			// Don't forget .get() to convert the jQuery set to a regular array.
		}).get();
	return tableObject;
}

/** Notes Save and View Start* */
function saveNotes(fkCodelstNotesType) {
	var htmlContents = "<html>"
			+ $("#stafaNotes").contents().find('html').html() + "<html>";
	var moduleName = $('#stafaNotes').contents().find("#moduleName").val();
	var widgetName = $('#stafaNotes').contents().find("#widgetName").val();
	var pkNotes = $('#stafaNotes').contents().find("#pkNotes").val();

	// var fkNotes = fkCodelstNotes.split("-");
	// var fkCodelstNotesType = fkNotes[0];
	// var pkNotes = fkNotes[1];
	try {
		$.ajax( {
			type : "POST",
			url : "saveNotes.action",
			async : false,
			data : {
				data : htmlContents,
				moduleName : moduleName,
				widgetName : widgetName,
				pkNotes : pkNotes
			},
			success : function(result) {
				$("#noteDialog").dialog("close");
			},
			error : function(request, status, error) {
				alert("Error " + error);
			}

		});
	} catch (err) {
		alert(" load action error " + err);
	}

}

function showNotes(mode, This) {
	// alert("showNotes : "+showNotes);
	var Notes = $(This).parent(".notes");
	var moduleName = $(Notes).find("#moduleName").val();
	var widgetName = $(Notes).find("#widgetName").val();
	// alert("fkCodelstNotesType ==> "+fkCodelstNotesType);
	// var fkCodelstNotesType=fkcode;
	// alert("moduleName : "+moduleName);
	// alert("widgetName : "+widgetName);

	$.ajax( {
		type : "POST",
		url : "getNotes.action",
		async : false,
		data : {
			moduleName : moduleName,
			widgetName : widgetName
		},
		success : function(result) {
			// $("#").contents().find('html').append(result.returnJsonStr);
		// $("#stafaNotes").contents().find('html').html(result.returnJsonStr);
		// alert($("#stafaNotes").contents().find('html').html());
		showNotesData(mode, result, moduleName, widgetName);
	},
	error : function(request, status, error) {
		alert("Error " + error);
	}
	});

}
function showNotesData(mode, result1, moduleName, widgetName) {
	$("#noteDialog").dialog( {
		autoOpen : false,
		title : 'Notes',
		modal : true,
		width : 470,
		height : 360,
		close : function() {
			jQuery("#noteDialog").dialog("destroy");
			$("#dialogEditor").htmlarea("dispose");
		}
	});

	$("#noteDialog").dialog(mode);
	$("#noteDialog").css("width", "100%");
	$("#dialogEditor").htmlarea();
	var data = result1.returnJsonStr;
	var pkNotes = result1.pkNotes;
	if (pkNotes == null) {
		pkNotes = "";
	}
	if (data == null) {
		data = "";
	}
	data.replace('<html>', '');
	data.replace('</html>', '');
	$('#stafaNotes').contents().find('html').html(data);
	var htmlCon = $('#stafaNotes').contents().find('html').html();
	// alert("showNotesData: moduleName : "+moduleName);
	// alert("showNotesData: widgetName : "+widgetName);
	// alert(htmlCon);
	if (htmlCon.lastIndexOf("moduleName") < 0) {
		$("#stafaNotes").contents().find('html').append(
				"<input type='hidden' id='moduleName' value='" + moduleName
						+ "'>");
		$("#stafaNotes").contents().find('html').append(
				"<input type='hidden' id='widgetName' value='" + widgetName
						+ "'>");
		$("#stafaNotes").contents().find('html').append(
				"<input type='hidden' id='pkNotes' value='" + pkNotes + "'>");
		// alert("showNotesData: moduleName : "+moduleName);
		// alert("showNotesData: widgetName : "+widgetName);
		// alert("showNotesData: pkNotes : "+pkNotes);
	} else {
		// var hidValue = fkCodelstNotesType+"-"+pkNotes;
		$("#stafaNotes").contents().find('#moduleName').val(moduleName);
		$("#stafaNotes").contents().find('#widgetName').val(widgetName);
		$("#stafaNotes").contents().find('#pkNotes').val(pkNotes);
		// alert("showNotesData1: moduleName : "+moduleName);
		// alert("showNotesData1: widgetName : "+widgetName);
		// alert("showNotesData1: pkNotes : "+pkNotes);
	}
	// alert($("#stafaNotes").contents().find('html').html());
}
/** Notes Save and Vew End* */

/** *Show Qc start */
function showQcDetails(qctype, qcDesc) {
	// alert("showQC " + qctype);
	// $("#qctype").val(qctype);
	var selectedQcId = qctype;
	// refershModelWindow("divId","getQcDetails","frmQC")
	var ajaxresult = ajaxCallWithParam("getQcDetails", selectedQcId);

	// alert("1 " + ajaxresult.qcDetailsList);
	// var qcDetails=createXmlDOMObject(ajaxresult.qcDetails);

	// alert(qcDetails);
	// var qcDesc = $(qcDetails).find("displayname").text();

	var qcDetailsTbl = "<table id='qcDetails' cellpadding='0' cellspacing='0' border='0' class='display'><thead>";
	qcDetailsTbl += "<tr><th>Reagents and Supplies</th><th>Quantity</th><th>Manufacturer</th><th>Lot#</th></tr></thead><tbody>";

	$.each(ajaxresult.qcDetailsList, function(i, v) {
		// alert(v[0]);
			qcDetailsTbl += "<tr><td>" + v[4] + "</td><td>" + v[3]
					+ "</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
		});
	/*
	 * $(qcDetails).find("item").each(function (i){ qcDetailsTbl +="<tr><td>"+$(this).find("desc").text()+"</td><td>"+$(this).find("quantity").text()+"</td><td>"+$(this).find("manufacturer").text()+"</td><td>"+$(this).find("manufacturecode").text()+"</td></tr>";
	 * });
	 */

	qcDetailsTbl += "</tbody></table>";

	$("#modelPopup").html(qcDetailsTbl);

	var qcTable = $('#qcDetails').dataTable( {
	// "sPaginationType": "full_numbers"
			});

	$("select").uniform();
	$("#modelPopup").dialog( {
		title : qcDesc,
		width : 800,
		height : 420,
		autoOpen : true,
		modal : true,
		close : function() {
			jQuery("#modelPopup").dialog("destroy");
		}
	});

}
/** *Show Qc end* */

function ajaxCallWithParam(url, datas) {

	var response = "";
	$.ajax( {
		type : "POST",

		url : url + ".action",
		data : {
			'param' : datas
		},

		async : false,
		success : function(result) {
			// alert("result :: "+result);
		response = result;
	},
	error : function(request, status, error) {
		alert("Error " + error);
		alert(request.responseText);
		response = error;
	}
	});
	return response;

}

function showModal(mode, Id, title) {
	$("#" + Id).dialog( {
		autoOpen : false,
		title : title,
		modal : true,
		width : 600,
		height : 200,
		close : function() {
			jQuery("#" + Id).dialog("destroy");
			// $("#dialogEditor").htmlarea("dispose");
	}
	});
	$("#" + Id).dialog(mode);
	// $("#dialogEditor").htmlarea();
}

function tableToJson(table) {
	var data = [];

	// first row needs to be headers
	var headers = [];
	for ( var i = 0; i < table.rows[0].cells.length; i++) {
		headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(
				/ /gi, '');
	}

	// go through cells
	for ( var i = 1; i < table.rows.length; i++) {

		var tableRow = table.rows[i];
		var rowData = {};

		for ( var j = 0; j < tableRow.cells.length; j++) {

			rowData[headers[j]] = tableRow.cells[j].innerHTML;

		}

		data.push(rowData);
	}
	alert("data : " + data);
	return data;
}

/* Maintenance slidewidget Column Filter start here */
function maintColumnfilter(id) {
	var table = $('#' + id);
	var index = 0;
	var input = $('#Columnfilter');
	zFilter.setup(input, table, index)
}

/* Check and uncheck all the Checkboxes inside the table * */
function checkall(tableId, This) {
	var isChecked = $(This).attr('checked');
	// alert(isChecked);
	if (isChecked) {
		$("#" + tableId + " tbody input:checkbox").attr('checked', true);

	} else {
		$("#" + tableId + " tbody input:checkbox").attr('checked', false);
	}
}

function serializeFormObject(form) { // alert("form"+form)
	function trim(str) {
		return str.replace(/^\s+|\s+$/g, "");
	}

	var o = {};
	var a = $(form).serializeArray();
	// alert(a);
	$.each(a, function() {
		var nameParts = this.name.split('[');
		if (nameParts.length == 1) {
			// New value is not an array - so we simply add the new
			// value to the result object
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		} else {
			// New value is an array - we need to merge it into the
			// existing result object
			$.each(nameParts, function(index) {
				nameParts[index] = this.replace(/\]$/, '');
			});

			// This $.each merges the new value in, part by part
			var arrItem = this;
			var temp = o;
			$.each(nameParts, function(index) {
				var next;
				var nextNamePart;
				if (index >= nameParts.length - 1)
					next = arrItem.value || '';
				else {
					nextNamePart = nameParts[index + 1];
					if (trim(this) != '' && temp[this] !== undefined)
						next = temp[this];
					else {
						if (trim(nextNamePart) == '')
							next = [];
						else
							next = {};
					}
				}

				if (trim(this) == '') {
					temp.push(next);
				} else
					temp[this] = next;

				temp = next;
			});
		}
	});
	return o;
}

function removeValidation(formid) {
	var settings = $('#' + formid).validate().settings;
	for ( var i in settings.rules) {
		delete settings.rules[i].required;
	}
}

// set time picker for barcode
function setTimePicker() {
	$('#accessTime').timepicker( {
		ampm : true,
		hourMin : 6,
		hourMax : 20
	});
}

// For barcode popup open
function loadBarcodePage() {
	setTimePicker();
	loadpopup();
}

function loadpopup() {
	$("#maindiv").load("loadBarcodePopup.action").dialog( {
		autoOpen : false,
		// modal: true, width:1050, height:710,
		modal : true,
		width : 1050,
		height : 650,
		close : function() {
			jQuery("#maindiv").dialog("destroy");
			// $(this).html(" ");
		$("#maindiv").remove();
		$("#mainbarcodediv").append('<div id="maindiv"></div>');

	}
	});
	$("#maindiv").dialog("open");

}

function searchProductCodeDescription(This) {
	var barcodeClass = $("#barcodeClass").val();
	var modifier = $("#modifier").val();
	var manipulation = $("#manipulation").val();
	var cryoprotectant = $("#cryoprotectant").val();
	var collectionVolume = $("#collectionVolume").val();
	var anticoagulantType = $("#anticoagulantType").val();
	var storageTemprature = $("#storageTemprature").val();
	var productDesc = "";
	var coreCondition = "";
	var groupVariables = "";
	var finalProductDescCode = "";
	if (modifier != null && modifier != "") {
		productDesc += modifier + " " + barcodeClass;
	} else {
		productDesc += barcodeClass;
	}

	if (anticoagulantType != null && anticoagulantType != "") {
		coreCondition += "|" + anticoagulantType;
	}

	if (storageTemprature != null && storageTemprature != "") {
		coreCondition += "/XX/" + storageTemprature;
	}

	if (cryoprotectant != null && cryoprotectant != "") {
		coreCondition += "|" + cryoprotectant;
	}

	if (productDesc != "" && coreCondition != "") {
		finalProductDescCode = productDesc + coreCondition;
	} else {
		finalProductDescCode = productDesc;
	}
	// alert(finalProductDescCode);

	$('#productDescriptionInput').val(finalProductDescCode);
	$('#productDescriptionInput').keyup();
	$('#productDescriptionInput').keypress();
	$('#productDescriptionInput').keydown();
}

function RePrintBarcode() {
	// alert(" rePrint");
	var reason = $("#reasonForRprint").val();
	$("#reasonReprint").val(reason);
	printBarcode();
}

function showVerifyUserPopup(Obj) {
	$("#verifyUserPopup").dialog( {
		autoOpen : false,
		title : "Verify User",
		modal : true,
		width : 400,
		height : 200,
		close : function() {
			jQuery("#verifyUserPopup").dialog("destroy");
			// ?BB hideHighting('dataLibraryTbody');
	}
	});
	$("#verifyUserPopup").dialog("open");
	$("#verifyESignBut").attr("onclick", "verifyESign('" + Obj.id + "')");
	$("#custom3Id").focus();
}
function verifyESign(Id) {
	var custom3Id = $("#custom3Id").val();
	var eSign = $("#verifyesign").val();
	if (typeof custom3Id != 'undefined' && custom3Id != ""
			&& typeof eSign != 'undefined' && eSign != "") {
		url = "verifyUserESign";
		response = jsonDataCall(url, "jsonData={custom3Id:" + custom3Id
				+ ";eSign:" + eSign + "}");
		if (response != null) {
			eSignFlag = response.eSignFlag;
			if (eSignFlag == 'true') {
				jQuery("#verifyUserPopup").dialog("destroy");
				$("#" + Id).val(custom3Id);
			} else {
				alert("Invalid 3-4 Id or E-Sign");
				$("#custom3Id").val("");
				$("#verifyesign").val("");
				$("#custom3Id").focus();
			}
		}
	} else {
		if ((typeof custom3Id == 'undefined' || custom3Id == "")
				&& (typeof eSign == 'undefined' || eSign == "")) {
			alert("Please Specify 3-4 Id and e-Sign");
		} else {
			if (typeof custom3Id == 'undefined' || custom3Id == "") {
				alert("Please Specify 3-4 Id ");
			}
			if (typeof eSign == 'undefined' || eSign == "") {
				alert("Please Specify E-Sign");
			}
		}
	}

}

function documentPopup(docId, type, fileName, docEntityType, docEntityId) {
	var screenwidth = screen.width;
	var screenheight = screen.height;
	var docEntityId = "";
	var docEntityType = "";
	docsId = docId;
	docsType = type;
	docsName = fileName;
	var title = "Document Review";
	var url = "openDocumentReview";
	var Id = "docsReview";
	var height = "800";
	var width = "800";
	jQuery("#" + Id).dialog("destroy");
	if (screenwidth <= 1024 && screenheight <= 768) {
		width = "950";
		height = "500";

	}
	$('.progress-indicator').css('display', 'block');
	$("#" + Id).dialog( {
		autoOpen : false,
		title : title,
		resizable : true,
		closeText : '',
		closeOnEscape : false,
		modal : true,
		width : width,
		height : height,
		close : function() {
			jQuery("#docsReview").dialog("destroy");

		}
	});

	$.ajax( {
		type : "POST",
		url : url,
		success : function(result, status, error) {
			// alert("result=="+result);
		$("#" + Id).html(result);
		$("select").uniform();
		if (typeof docEntityType != 'undefined' && docEntityType != 'undefined'
				&& docEntityType != '') {
			$("#docEntityType").val(docEntityType);

		}
		if (docEntityId != 'undefined' && typeof docEntityId != 'undefined'
				&& docEntityId != '') {
			$("#docEntityId").val(docEntityId);
		}

	},
	error : function(request, status, error) {
		alert("Error " + error);
		alert(request.responseText);
	}

	});
	$("#" + Id).dialog("open");
	$("#" + Id).css("width", "95%");
	$('.ui-dialog select').uniform();
	$('.progress-indicator').css('display', 'none');
}

function loaddiv(url, divname) {

	$('.progress-indicator').css('display', 'block');
	$.ajax( {
		type : "POST",
		url : url,
		async : false,
		success : function(result) {
			// $('.ui-dialog').html("");
		// $('.ui-datepicker').html("");
		var $response = $(result);
		var errorpage = $response.find('#errorForm').html();
		if (errorpage != null && errorpage != "") {
			$('#main').html(result);
		} else {
			$("#" + divname).html(result);
		}
	},
	error : function(request, status, error) {
		alert("Error " + error);
		alert(request.responseText);
	}

	});

	$('.progress-indicator').css('display', 'none');
}

// /////////////////////////////////////////////////////////////////////////////////////////////////////

function openSettings() {
	$("#settings").load("jsp/cdmr/settings.jsp");
	$
			.blockUI( {
				title : '<img style="cursor:auto;width:16px;height:16px;" src="images/cdmr/closeicon.png" onclick="blockUIClose();"/>',
				theme : true,
				draggable : true,
				message : $('#settings'),
				themedCSS : {
					width : '81%',
					left : '100px',
					top : '10px',
					backgroundColor : '#FFFFFF'
				}
			});
}

function openNotification() {
	$("#notiwindow").load("jsp/cdmr/notification.jsp");
	$
			.blockUI( {
				title : '<img style="cursor:auto;width:16px;height:16px;" src="images/cdmr/closeicon.png" onclick="blockUIClose();"/>',
				theme : true,
				draggable : true,
				message : $('#notiwindow'),
				themedCSS : {
					width : '81%',
					left : '100px',
					top : '10px',
					backgroundColor : '#FFFFFF'
				}
			});
}

function blockUIClose() {
	$.unblockUI();
}


function hitEnterEvent(param){
	$(param).prev('input');
	
	var e = jQuery.Event( 'keyup', { which: $.ui.keyCode.ENTER } );
	
	$(param).prev('input').trigger(e);
    //alert(tmp.val());
    
    
}
