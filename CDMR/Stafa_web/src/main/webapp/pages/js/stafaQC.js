//alert("stafaQC.js");
function fn_calculation(){
	//alert(" calc start2");
	//values ->totalVolume,dilution,weight,hct,sample1,sample2
	var totalVolume,dilution,weight,hct,sample1,sample2,hgb;
	totalVolume = $("#totalVolume").val();
	dilution = $("#dilution").val();
	weight = $("#weight").val();
	hct = $("#hct").val();
	sample1 = Number($("#sample1").val());
	sample2= Number($("#sample2").val());
	hgb=Number($("#hgb").val());
// alert(totalVolume +","+ dilution+","+weight+","+hct+","+sample1+","+sample2);
 
  var avgcell,tnc,tncKg,rbc,rbcKg;
 
  var tsample = Number(sample1) + Number(sample2);
 //alert(" trace tsample " + tsample);
 // alert(" trace 5" + (tsample/2));
 //alert("number"+ Number(totalVolume));
 if(tsample > 0 ){
  avgcell = round2Decimal( (tsample/2));
 }else{
	 avgcell ="";
 }
  if(eval(avgcell) > 0 ){
  	tnc = round2Decimal( Number(avgcell)*Number(totalVolume)*Number(dilution) );
  }else{
	  tnc="";
  }
  if(Number(weight)>0){
      tncKg = round2Decimal( Number(tnc)/(Number(weight)*100) );
  }else{
	  tncKg="";
  }
  
  if(Number(tnc) > 0){
  		rbc = round2Decimal( Number(tnc) * Number(hgb));
  }else{
	  rbc="";
  }
  if(Number(weight)>0){
    rbcKg = round2Decimal( Number(rbc) / Number(weight));
  }else{
	  rbcKg="";
  }
	$("#avgcell").text(avgcell);
	$("#tnc").text(tnc);
	$("#tncKg").text(tncKg);
	$("#rbc").text(rbc);
	$("#rbcKg").text(rbcKg);
	
	
	
//	alert("end of calc");
	
	
}


$(function() {
	$( "#leftnav" ).leftnav();
	$( "#leftnav" ).removeClass('ui-widget ui-helper-reset');
});
   
$(document).ready(function() {
	// $('#right_new_wrapper').attr("style", "width: 83%");	
             $('#blind').html("<<");
               var $box = $('#box').wrap('<div id="box-outer"></div>');
               $('#blind').click(function() {
                 $box.blindToggle('fast');
               });
             });



               jQuery.fn.blindToggle = function(speed, easing, callback) {
               var h = this.width() + parseInt(this.css('paddingLeft')) +  parseInt(this.css('paddingRight'));
               if(parseInt(this.css('marginLeft'))<0){
			   	   $('#forfloat_right').animate( { width: "82%" }, { queue: false, duration: 200 });	
                   $('#blind').html("<<");
                   return this.animate({marginLeft:0},speed, easing, callback);
                   }
               else{
    			  $( "#forfloat_right" ).animate( { width: "95%" }, { queue: false, duration: 250 });
     
                   $('#blind').html(">>");
                   return this.animate({marginLeft:-h},speed, easing, callback);
				   
                     }
				
             };


function qcCalculation(formName){
	//alert("QC calculations");
	//ABO,CHIMERA,GRAMSTAIN,IMMNOPHENOTYPING,DIFFERENTIAL,ENUMERATION,STERILITY,VISIBILITY
	var selectedQC ="";
	
	
	var chk_arr = $("#qcContent input[type=checkbox]");
	var chklength = chk_arr.length;             
	//alert("chk_arr : "+chk_arr);
	//alert("chklength : "+chklength);
	var deLim="";
	for(qck=0;qck<chklength;qck++)
	{

		if ($(chk_arr[qck]).is(':checked')){
			selectedQC+=deLim+$(chk_arr[qck]).attr("id");
			deLim=","
		}
		
	}
	//alert("selectedQC" + selectedQC)
	/*if($('#70').is(':checked')) {
		selectedQC+="ABO,"
	}
	if($('#CHIMERA').is(':checked')) {
		selectedQC+="CHIMERA,"
	}
	if($('#GRAMSTAIN').is(':checked')) {
		selectedQC+="GRAMSTAIN,"
	}
	if($('#IMMNOPHENOTYPING').is(':checked')) {
		selectedQC+="IMMNOPHENOTYPING,"
	}
	if($('#DIFFERENTIAL').is(':checked')) {
		selectedQC+="DIFFERENTIAL,"
	}
	if($('#ENUMERATION').is(':checked')) {
		selectedQC+="ENUMERATION,"
	}
	if($('#STERILITY').is(':checked')) {
		selectedQC+="STERILITY,"
	}
	if($('#VISIBILITY').is(':checked')) {
		selectedQC+="VISIBILITY,"
	}*/
	
	//alert(" selectedQC " + selectedQC);
	// reagentsTable,equipmentsTable
	
	$('#selectedQC').val(selectedQC);
	
	try{
	//	var ajaxresult = ajaxCall("getQCCalulation","frmQC");
	   refreshTable("reagentsTable","reagentsdiv","getQCCalulation",formName);
	   $("select").uniform();
	   //refreshTable("equipmentsTable","equipmentdiv","getQCCalulation","frmQC");
		//alert(" ajaxresult " + ajaxresult);
	   $( ".dateEntry" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	        changeYear: true, yearRange: "c-50:c+45"});
	  //$( ".dateEntry" ).datepicker({dateFormat: 'M dd, yy'});
	}catch(err){
		alert("error in qc cal " + err);
	}
	return true;
}


$(function() {	
	//alert("test1");

	$( ".column" ).sortable({
		connectWith: ".column",
	    cursor: 'move',
	    cancel:	".portlet-content"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");

	/** For Information***/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".info" )
	.append("<span style=\"float:right;\" class='ui-info'></span>");
	
	$( ".portlet-header .ui-info" ).click(function() {
		loadQcInfo();
		showPopUp("open","addqc","QC Information","1200","600");
	});
	
//	$( ".portlet-header .ui-notes " ).click(function() {
//		showNotes('open');
//	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
	//$('.sortable').columnManager({listTargetID:'sortable', onClass: 'advon', offClass: 'advoff', hideInList: [1,3],
     //   saveState: true, colsHidden: [2,4]});
	//$( ".column" ).disableSelection();
	//alert("end of QC script");
});

//--------------------------------------- completed functions ------------------
function addRegent(){
	editRegent(0);
}

function deleteRegent(){
	//  alert("Delete Rows");
    var yes=confirm(confirm_deleteMsg);
	if(!yes){
		return;
	}
	var deletedIds = ""
	$("#regentSupplies #regent:checked").each(function (row){
		deletedIds += $(this).val() + ",";
	});
	if(deletedIds.length >0){
	  deletedIds = deletedIds.substr(0,deletedIds.length-1);
	}
	jsonData = "{deletedRegents:'"+deletedIds+"'}";
	url="deleteQcRegents";
    response = jsonDataCall(url,"jsonData="+jsonData);
	getRegentsSupplies();
}



var oTable;
function getRegentsSupplies()  {
	 // alert("getRegentsSupplies");
	$("#qcHidden").val("Reagents");
	  $('.progress-indicator').css( 'display', 'block' );
 		var breadCrum = "<a href='#' onclick='constructTable(true);'>Lab Test Library</a>  &gt;"
		breadCrum += "<a href='#' onclick='getQcDetails("+$("#qcId").val()+");'> " + $("#commondiv #qcName").val() + "</a> &gt; Regents and Supplies"  ;
		$("#breadCrumbLink").html(breadCrum );
				$("#regentqc").removeClass("hidden");
				$("#commondiv").addClass("hidden");
			
		var qcid = $("#qcId").val();
		var criteria =" and fk_qc = " + qcid;
		oTable=$('#regentSupplies').dataTable().fnDestroy();
	
		oTable=$('#regentSupplies').dataTable( {
			"sPaginationType": "full_numbers",
			"bProcessing": true,
			"bServerSide": true,
			"bRetrieve" : true,
			 "bDestroy": true,
			 "sScrollX":"100%",
			 "aaSorting": [[ 1, "asc" ]],
			 "aoColumns": [{"bSortable": false},
			               null,
			               null,
			               null,
			               null,
			               null,
			               null,
			               null
			               ],
			 "oLanguage": {"oPaginate": {"sNext": "","sPrevious":""},"sProcessing":""},									
			"fnServerParams": function ( aoData ) {
													aoData.push( { "name": "application", "value": "stafa"});
													aoData.push( { "name": "module", "value": "maintenance_panelconfig_QC_regentsupplies"});	
													aoData.push( { "name": "criteria","value": criteria});
													
													aoData.push( {"name": "col_1_name", "value": "lower(code.CODELST_DESC)" } );
													aoData.push( { "name": "col_1_column", "value": "lower(code.CODELST_DESC)"});
													
													aoData.push( { "name": "col_2_name", "value": " lower(code.CODELST_SUBTYP) "});
													aoData.push( { "name": "col_2_column", "value": "lower(code.CODELST_SUBTYP)"});
													
													aoData.push( {"name": "col_3_name", "value": " lower(code.CODELST_COMMENTS) " } );
													aoData.push( { "name": "col_3_column", "value": "lower(code.CODELST_COMMENTS)"});
													
													aoData.push( {"name": "col_4_name", "value": " lower(MANUFACTURER) " } );
													aoData.push( { "name": "col_4_column", "value": "lower(MANUFACTURER)"});
													
													aoData.push( {"name": "col_5_name", "value": " lower(LOTNUMBER) " } );
													aoData.push( { "name": "col_5_column", "value": "lower(LOTNUMBER)"});
													
													aoData.push( {"name": "col_6_name", "value": " lower(to_char(EXPDATE,'mon dd, yyyy')) " } );
													aoData.push( { "name": "col_6_column", "value": "lower(to_char(EXPDATE,'mon dd, yyyy'))"});
													
													aoData.push( {"name": "col_7_name", "value": " QUNATITY " } );
													aoData.push( { "name": "col_7_column", "value": "QUNATITY"});
													
			},
			"sAjaxSource": 'dataSearch.action',
			"sAjaxDataProp": "filterData",
			"aoColumnDefs": [
			                  
			                  { 	"sTitle":"Check All",
			                	  "aTargets": [ 0], "mDataProp": function ( source, type, val ) { 
			                	 return "<input type='checkbox' id='regent' name='regent' value='"+source[0] +"' >";}},
							  {  "sTitle": "Name",
			                		 "aTargets": [ 1], "mDataProp": function ( source, type, val ) { 
								  return source[2];}},
							  {		"sTitle":"Code",
									 "aTargets": [ 2], "mDataProp": function ( source, type, val ) { return source[3];}},
							  {		"sTitle":"Description",
									"aTargets": [ 3], "mDataProp": function ( source, type, val ) { return source[4];}},
							  {		"sTitle":"Manufacturer",
									 "aTargets": [ 4], "mDataProp": function ( source, type, val ) { return source[5];}},
							  {		"sTitle":"Lot #",
									"aTargets": [ 5], "mDataProp": function ( source, type, val ) { return source[6];}},
							  {		"sTitle":"Exp. Date",
									"aTargets": [ 6], "mDataProp": function ( source, type, val ) { return source[7];}},
							  {		"sTitle":"Quantity",
									"aTargets": [ 7], "mDataProp": function ( source, type, val ) { return source[8];}}
							  /*{		"sTitle":"Description",
									"aTargets": [ 7], "mDataProp": function ( source, type, val ) { return source[8];}}*/
							],
				"fnInitComplete": function () {
						       //$("#regentSupplies").html($('.ColVis:eq(0)'));
							},
				"fnServerData": function ( sSource, aoData, fnCallback ) {
					$('.progress-indicator').css( 'display', 'block' );
					$.ajax( {
							"dataType": 'json',
							"type": "POST",
							"url": 'dataSearch.action',
							"data":aoData,
							"success":fnCallback,  
							"complete": function () {
										$('.progress-indicator').css( 'display', 'none' );
										$("select").uniform();
										}
						  } );},
				"fnDrawCallback": function() { 
					//alert("fnDrawCallback");
					$('.progress-indicator').css( 'display', 'none' );
		      }
			} );
		jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = parseInt(no_of_pages);
  }



function constructQcResultTable(flag,qcId) {
		//alert("construct table : ");
		
		//elementcount =0;
		$('.progress-indicator').css( 'display', 'block' );
		var strBreadcrumb = "<a href='#' onclick='constructTable(true);'>Qc Library</a> &gt;";
		strBreadcrumb += "<a href='#' onclick='getQcDetails("+$("#qcId").val()+");'> " + $("#commondiv #qcName").val() + "</a> &gt; QC Results"  ;
		$("#breadCrumbLink").html(strBreadcrumb );
	 	
	oTable=	$('#qcResltTable').dataTable().fnDestroy();
			var criteria = " and FK_CODELIST_QC = "+ $("#qcId").val(); 		
		  var link ;
			try{
				oTable=	$('#qcResltTable').dataTable( {
					"sPaginationType": "full_numbers",
					"bProcessing": true,
					"bServerSide": true,
					"bRetrieve" : true,
					 "bDestroy": true,
					 "sScrollX":"100%",
					 "aaSorting": [[ 1, "asc" ]],
					 "aoColumns": [{"bSortable": false},
					               null,
					               null,
					               null,
					               null],
					 "oLanguage": {"sProcessing":""},									
					"fnServerParams": function ( aoData ) {
															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "maintenance_panelconfig_QC_QC_Result"});	
															aoData.push( { "name": "criteria", "value": criteria});
															
															aoData.push({"sColumns":"Protocol Name,Module,Version"});
															
//select cl.PK_CODELST,cl.CODELST_TYPE,cl.CODELST_SUBTYP,cl.CODELST_DESC,cl.CODELST_HIDE,cl.CODELST_COMMENTS,qr.FK_CODELIST_QC,qr.FK_CODELIST_QCRESULT,
							//qr.QCRESULT_HIDE

															
															
															aoData.push( { "name": "col_1_name", "value": "lower(cl.CODELST_DESC)"});
															aoData.push( { "name": "col_1_column", "value": "lower(cl.CODELST_DESC)"});
															
															aoData.push( {"name": "col_2_name", "value": "lower(cl.CODELST_SUBTYP)" } );
															aoData.push( { "name": "col_2_column", "value": "lower(cl.CODELST_SUBTYP)"});
															
															aoData.push( {"name": "col_3_name", "value": "lower(cl.CODELST_COMMENTS)" } );
															aoData.push( { "name": "col_3_column", "value": "lower(cl.CODELST_COMMENTS)"});
					},
					"sAjaxSource": 'dataSearch.action',
					"sAjaxDataProp": "filterData",
					"aoColumnDefs": [
					                 {	"sTitle":"Check All",
					                	 "aTargets": [ 0], "mDataProp": function ( source, type, val ) {
						                  
					                	 return "<input type='checkbox' id='qcResultId' name='qcResultId' value='"+source[0] +"' ><input type='hidden' id='codeListQcResultId' name='codeListQcResultId' value='"+source[7] +"' >";

					                	 }},
									  {	"sTitle": "Name",
					                		 "aTargets": [ 1], "mDataProp": function ( source, type, val ) { 
										 	  return source[4];
										  }},
									  {	"sTitle":"Code",
											  "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
										 	   return source[3];
									  		}},
									  {	"sTitle":"Description",
									  			"aTargets": [ 3], "mDataProp": function ( source, type, val ) { 
										  		return source[6];
										  }},
									  {	"sTitle":"Activated",
											  "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
										  link = ""; 
										  if(source[9]=='N'){
											  link = "<input type='radio' name='active_"+source[0]+"' id='activeyes' value='Yes' checked='checked' >Yes</input>";
											  link+= "<input type='radio' name='active_"+source[0]+"' id='active' value='No' >No</input>";
										  }else if(source[9]=='Y'){
											  link = "<input type='radio' name='active_"+source[0]+"' id='activeyes' value='Yes'  >Yes</input>";
											  link+= "<input type='radio' name='active_"+source[0]+"' id='active' value='No' checked='checked'>No</input>";
										  }
										  return link;}}
									],
						"fnServerData": function ( sSource, aoData, fnCallback ) {
							$('.progress-indicator').css( 'display', 'block' );
							$.ajax( {
									"dataType": 'json',
									"type": "POST",
									"url": 'dataSearch.action',
									"data":aoData,
									"success":fnCallback,  
									"complete": function () {
												$('.progress-indicator').css( 'display', 'none' );
												$("select").uniform();
												}
								  } );},
						"fnDrawCallback": function() { 
							//alert("fnDrawCallback");
							$('.progress-indicator').css( 'display', 'none' );
				      }
					} );
				jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = parseInt(no_of_pages);
				}catch(err){
					alert("error " + err);
				}
	} 



