// User Module

$(document).ready(function(){
	$('#userDiv').show();
	$('#addUserDiv').hide();
});
function cancelUserSave(){
	$('#addUserDiv').hide();
	$('#userDiv').show();
}
function saveNewUser(){
	var loginName=$("#login").val();
	var password=$("#password").val();
	var passwordFlag = 0, esignFlag = 0, loginFlag=0;
	if(loginName == undefined || loginName == ""){
		loginFlag=1;
	}
	if(password == undefined || password == ""){
		passwordFlag = 1;
	}
	else{
		if((password == loginName) || ($.trim(password).length < 8 ) || (password.match(/[0-9]+/) == null) || (password.match(/[A-Za-z]+/) == null)){
			alert(passordRule);
			$("#password").focus();
			return false;
		}
	}
	var eSign = $('#esign').val();
	if(eSign == null || eSign == ""){
		esignFlag = 1;
	}
	else{
		if($.trim(eSign).length < 4 ){
			esignFlag = 1;
			alert(esignRule);
			$('#esign').focus();
			return false;
		}
	}
	if(loginFlag==0){
	if(passwordFlag == 0){
		if(esignFlag==0){
			$('.progress-indicator').css( 'display', 'block' );
			var accExpDateStr=$("#accExpDate").val();
			if(accExpDateStr!="" && accExpDateStr!=null && accExpDateStr!=undefined){
				var now = new Date();
				now.format("M dd, yy");
				var accExpDate = new Date(accExpDateStr);
				if(accExpDate>=now){
					$("#acctIsActive").val("1");
				}
			}
			$.ajax({
				type: "POST",
				url: "saveUserDetails.action",
				async:false,
				data:$("#addUserForm").serialize(),
				success:function (result){
					alert(saveSuccess);
				},
				error: function (request, status, error) {
					alert("Error " + error);
				}
			});


			$('.progress-indicator').css( 'display', 'none' );
			$('#addUserDiv').hide();
			$('#userDiv').show();
			return false;
		}else{
			alert(esignErrMsg);
			$('#esign').focus();
			return false;
		}
	}else{
		alert(passwordErrMsg);
		$("#password").focus();
		return false;
	}
	}else{
		alert(loginNameErrMsg);
		$("#login").focus();
		return false;
	}
}
function userInformation(openMode){
	$('#userDiv').hide();
	/*var countryList="<option value='null'>Select</option>";	
	var jobTypeList="<option value='null'>Select</option>";	*/
	$.ajax({
		type: "POST",
        url: "addesecuser.action",
        async:false,
        success:function (result){
        	/*$(result.countryList).each(function(i,v){
				countryList=countryList+"<option value='"+v.pkCodelst+"'>"+v.description+"</option>";
			});	
        	$(result.jobTypeList).each(function(i,v){
        		jobTypeList=jobTypeList+"<option value='"+v.pkCodelst+"'>"+v.description+"</option>";
			});	*/
        	$("#addUserDiv").html(result);
        },
        error: function (request, status, error) {
            alert("Error " + error);
        }
	});
	/*$('#countrylst').html("<select name='country' id='country'>"+countryList+"</select>");
	$('#jobtypelst').html("<select name='jobType' id='jobType'>"+jobTypeList+"</select>");*/
	$('input:radio[name=status][value="1"]').attr('checked', 'checked'); 
	$("select").uniform();
	$('#addUserDiv').show();
	if(openMode == 'edit'){
		$('span[id=forEdit]').show();
		$('span[id=forAdd]').hide();
	}
	else{
		$('span[id=forEdit]').hide();
		$('span[id=forAdd]').show();
	}
}

function conformPassword(loginName){
	$("#confDialog").load("jsp/esecurity/Conform_Password.jsp",function(){
		$('#loginNamePop').val(loginName);
	}).dialog({
		   autoOpen: true,
		  title: "Conform Reset Password",
		   modal: true, width:500, height:100,
		   close: function(i,el) {
			   jQuery("#confDialog").dialog("destroy");
			   $(el).html("");
		   }
		});
}
function resetPassword(){
	var loginName = $('#loginNamePop').val();
	$.ajax({
        type: "POST",
        url: "resetPassword.action",
        async:false,
        data:"loginName="+loginName,
        success:function (result){
					$.ajax({
						url:"resetForgotPassword.action",
						type:"post",
						data:{"loginName":loginName},
						success:function(response){
							 alert("Reset Password successful");
		                    jQuery("#confDialog").dialog("destroy");
		                    $('#confDialog').html("");
						}
					});
                   
                },
        error: function (request, status, error) {
            alert("Error " + error);
        }
    });
}
function cancelPwd(){
	jQuery("#confDialog").dialog("destroy");
	$('#confDialog').html("");
}
function conformESign(loginName){
	$("#confDialog").load("jsp/esecurity/Conform_ESign.jsp",function(){
		$('#loginNamePop').val(loginName);
	}).dialog({
		   autoOpen: true,
		  title: "Conform Reset e-Sign",
		   modal: true, width:500, height:100,
		   close: function(i,el) {
			   jQuery("#confDialog").dialog("destroy");
			   $(el).html("");
		   }
		});
}
function reseteSign(){
	var loginName = $('#loginNamePop').val();
	$.ajax({
        type: "POST",
        url: "reseteSign.action",
        async:false,
        data:"loginName="+loginName,
        success:function (result){
                    alert("Reset E-Sign successful");
                    jQuery("#confDialog").dialog("destroy");
                    $('#confDialog').html("");
                },
        error: function (request, status, error) {
            alert("Error " + error);
        }
    });
}
function canceleSign(){
	jQuery("#confDialog").dialog("destroy");
	$('#confDialog').html("");
}
function conformSession(loginName){
	$("#confDialog").load("jsp/esecurity/Conform_Session.jsp",function(){
		$('#loginNamePop').val(loginName);
	}).dialog({
		   autoOpen: true,
		  title: "Conform Reset User Session",
		   modal: true, width:500, height:100,
		   close: function(i,el) {
			   jQuery("#confDialog").dialog("destroy");
			   $(el).html("");
		   }
		});
}
function resetUserAccount(){
	var loginName = $('#loginNamePop').val();
	$.ajax({
        type: "POST",
        url: "resetUserAccount.action",
        async:false,
        data:"loginName="+loginName,
        success:function (result){
                    alert("Reset User Session successful");
                    jQuery("#confDialog").dialog("destroy");
                    $('#confDialog').html("");
                },
        error: function (request, status, error) {
            alert("Error " + error);
        }
    });
}
function cancelSession(){
	jQuery("#confDialog").dialog("destroy");
	$('#confDialog').html("");
}
function editUser(loginName){
	$('#main').load('jsp/esecurity/edit_User.jsp',function(){
		callAccount(loginName);		 
	});
}

function updateUser(){
	$('.progress-indicator').css( 'display', 'block' );
	var passwordFlag = 0, esignFlag = 0;
	var password = $('#password').val();
	if(password == null || password == ""){
		passwordFlag = 0;
	}
	else{
		if((password == $('#loginName').val()) || ($.trim(password).length < 8 ) || (password.match(/[0-9]+/) == null) || (password.match(/[A-Za-z]+/) == null)){
			passwordFlag = 1;
			alert(passordRule);
			$("#password").focus();
		}
	}
	var eSign = $('#esign').val();
	if(eSign == null || eSign == ""){
		esignFlag = 0;
	}
	else{
		if($.trim(eSign).length < 4 ){
			esignFlag = 1;
			alert(esignRule);
			$('#esign').focus();
		}
	}

	if(passwordFlag == 0){
		if(esignFlag==0){
			var accExpDateStr=$("#accExpDate").val();
			if(accExpDateStr!="" && accExpDateStr!=null && accExpDateStr!=undefined){
				var now = new Date();
				now.format("M dd, yy");
				var accExpDate = new Date(accExpDateStr);
				if(accExpDate>=now){
					$("#acctIsActive").val("1");
				}
			}
			
		$.ajax({
			type: "POST",
		    url: "updateUser.action",
		    async:false,
		    data:$("#editAUserForm").serialize(),
		    success:function (result){
	    		alert(saveSuccess);
	    		$('#main').load('jsp/esecurity/User_Information.jsp',function(){
	    			
	    		});
			},
		    error: function (request, status, error) {
		    	alert("Error " + error);
			}
		});
		
	}
	}
	$('.progress-indicator').css( 'display', 'none' );
} 

function assign_rights(loginName){
	$('.progress-indicator').css( 'display', 'block' );
	$.ajax({
		type: "POST",
	    url: "loadModuleTreeDetails.action",
	    async:false,
	    data : "userName="+ loginName + "&nameType=U",
	    success:function (result){
	    	$("#loadRights").load("jsp/esecurity/assign_Rights.jsp",function(){
	    		var rolelst = result.roleList;
		        var rowstr = "";
		        var innerDiv = "";
	        	var maxlength = rolelst.length;
		        for(var i in rolelst){
		        	var rowObj = rolelst[i];
		        	var modules = (rowObj[3]).split('.');
		        	var modulelength = modules.length;
		        	innerDiv +='<tr id="'+rowObj[3]+'"><td style="padding-left:' + modulelength * 2 +'em"><img src="images/folder.gif" onclick="closeTree(this,\'close\');" id="img-'+i+'" /> '+rowObj[1]+'<input type="hidden" id="moduleName-'+i+'" name="moduleName-'+i+'" value="'+rowObj[1]+'"/> </td>';
		        	if(rowObj[2].indexOf('R') == -1)
		        	{
						innerDiv += "<td><input type='checkbox' class= 'view' id='view-"+i+"' name='view-"+i+"' /> </td>";
					}
		        	else{
		        		innerDiv += "<td><input type='checkbox' class= 'view' id='view-"+i+"' name='view-"+i+"' checked='checked' /> </td>";
		        	}
		        	if(rowObj[2].indexOf('A') == -1)
		        	{
						innerDiv += "<td> <input type='checkbox' class= 'add' id='add-"+i+"' name='add-"+i+"' /> </td>";
					}
		        	else{
		        		innerDiv += "<td><input type='checkbox' class= 'add' id='add-"+i+"' name='add-"+i+"' checked='checked' /> </td>";
		        	}
		        	if(rowObj[2].indexOf('M') == -1)
		        	{
						innerDiv += "<td> <input type='checkbox' class= 'edit' id='edit-"+i+"' name='edit-"+i+"' /> </td>";
					}
		        	else{
		        		innerDiv += "<td><input type='checkbox' class= 'edit' id='edit-"+i+"' name='edit-"+i+"' checked='checked' /> </td>";
		        	}
		        	if(rowObj[2].indexOf('D') == -1)
		        	{
						innerDiv += "<td> <input type='checkbox' class= 'delete' id='delete-"+i+"' name='delete-"+i+"' /> </td></tr>";
					}
		        	else{
		        		innerDiv += "<td><input type='checkbox' class= 'delete' id='delete-"+i+"' name='delete-"+i+"' checked='checked' /> </td></tr>";
		        	}
		        }
		        $('#rightsTable tbody').html(innerDiv);
		        $('#userName').val(loginName);
		        $('#maxlength').val(maxlength);
		        $('#transType').val('U');
		        $("#loadRights").dialog({
		        	autoOpen: true,
		        	title: "Assign Rights to Users : " + loginName,
				 	modal: true, width:850, height:500,
				 	close: function(i,el) {
				 		jQuery("#loadRights").dialog("destroy");
				 		$(el).html("");
				 	}
		        });
	    	});
	    }
	});
	$('.progress-indicator').css( 'display', 'none' );
}
function saveRights(){
	$('.progress-indicator').css( 'display', 'block' );
	$('input[type=checkbox]:checked').each(function(i,el){
		if($(el).attr('class')=='view'){
			$(el).attr('value','R');
		}
		if($(el).attr('class')=='add'){
			$(el).attr('value','A');
		}
		if($(el).attr('class')=='edit'){
			$(el).attr('value','M');
		}
		if($(el).attr('class')=='delete'){
			$(el).attr('value','D');
		}
	});
	$.ajax({
		type: "POST",
	    url: "saveModuleRights.action",
	    async:false,
	    data : $('#rightsForm').serialize(),
	    success:function (result){
	    	alert(saveSuccess);
	    	jQuery("#loadRights").dialog("destroy");
	    }
	});
	$('.progress-indicator').css( 'display', 'none' );
}
function loadPolicyDetails(){
	$.ajax({
		type: "POST",
	    url: "loadAllPolicyDetails.action",
	    async:false,
	    success:function (result){
	    	var roleList = result.roleList;
	    	var innerDiv = "";
	    	for(var i in roleList){
	        	var rowObj = roleList[i];
	        	if(rowObj[2]=='text'){
	        		innerDiv += "<tr><td>" + rowObj[1] + "</td><td> <input type='text' id='" + rowObj[0] + "' name='" + rowObj[0] + "' value='" + rowObj[4] + "' /><input type='hidden' id='sequence' name='sequence' value='"+rowObj[3]+"' /><input type='hidden' id='policycode' name='policycode' value='"+rowObj[0]+"' /><input type='hidden' id='policytype' name='policytype' value='"+rowObj[2]+"' /></td></tr>";
	        	}
	        	if(rowObj[2]=='combo'){
	        		innerDiv += "<tr><td>" + rowObj[1] + "</td><td> <select name='" + rowObj[0] + "' id='" + rowObj[0] + "'>"+rowObj[5]+"</select><input type='hidden' id='sequence' name='sequence' value='"+rowObj[3]+"' /><input type='hidden' id='policycode' name='policycode' value='"+rowObj[0]+"' /><input type='hidden' id='policytype' name='policytype' value='"+rowObj[2]+"' /></td></tr>";
	        	}
	        	//put radio here
	        	/*if(rowObj[2]=='radio'){
	        		innerDiv += "<tr><td>" + rowObj[1] + "</td><td> <input type='text' name='" + rowObj[0] + "' id='" + rowObj[0] + "'/>"+rowObj[5]+"</><input type='hidden' id='sequence' name='sequence' value='"+rowObj[3]+"' /><input type='hidden' id='policycode' name='policycode' value='"+rowObj[0]+"' /><input type='hidden' id='policytype' name='policytype' value='"+rowObj[2]+"' /></td></tr>";
	        	}*/
	        	
	    	}
	    	$('#policyTable').html(innerDiv);
	    }
	});
}
function savePolicy(){
	$('.progress-indicator').css( 'display', 'block' );
	var rowData="";
	var policycode;
	var value;
	var sequenceval;
	
	$("#policyTable tr").each(function(){
		policycode=$(this).find("#policycode").val();
		sequenceval =$(this).find("#sequence").val();
		value =$(this).find("#"+policycode).val();
		rowData+= "{code:'"+policycode+"',value:'"+value+"',sequence:'"+sequenceval+"'},";
	}); 
	
	 if(rowData.length >0){
        rowData = rowData.substring(0,(rowData.length-1));
		var jsonData = "jsonData={policyData:["+rowData+"]}";
		url="savePolicy";
		var response = jsonDataCall(url,jsonData);
    	alert(saveSuccess);
	    }
	 $('.progress-indicator').css( 'display', 'none' );
	return true;
}