
var elementcount =0;

// for Group_List.jsp

	function addRows(checkId,groupName,description){
		elementcount +=1;
		var trClass = '';
		if(elementcount%2 == 0){
			trClass = 'odd';
		}
		else{
			trClass = 'even';
		}
		var newRow = "<tr class='"+trClass+"'><td><input type='checkbox' id='chkId' value='"+checkId+"'> </td> <td> <input type='text' id='groupName' name='groupName' value='"+groupName+"' /></td><td><input type='text' id='description' name='description' value='"+description+"' /></td>";
		newRow += "<td><a href='#' onclick='getRole(this);'> Assign </a> </td>";
		newRow += "<td><a href='#' onclick = 'getUsers(this);'> Manage </a></td></tr>";
		$("#groupTable tbody[role='alert']").prepend(newRow); 
		   
		$("#groupTable .ColVis_MasterButton").triggerHandler("click");
		$('.ColVis_Restore:visible').triggerHandler("click");
		$("div.ColVis_collectionBackground").trigger("click");
	}

// for assign_Rights.jsp
	
	function getRole(el,userName){
		$('.progress-indicator').css( 'display', 'block' );
		var parent = $(el).parent().parent();
		var validateCheck = 0;
		var name = "";
		var desc = "";
		if($(parent).find('#groupName').length) {
			name = $(parent).find('#groupName').val();
			desc = $(parent).find('#description').val();
			if(name == null || $.trim(name) == "" || desc == null || $.trim(desc) == ""){
				validateCheck = 1;
			}
		}else{
			name = userName;
		}
		if(validateCheck == 0){
			$.ajax({
				type: "POST",
			    url: "loadModuleTreeDetails.action",
			    async:false,
			    data : "userName="+ name +"&nameType=R",
			    success:function (result){
			    	$("#loadRights").load("jsp/esecurity/assign_Rights.jsp",function(){
			    		var rolelst = result.roleList;
				        var rowstr = "";
				        var innerDiv = "";
			        	var maxlength = rolelst.length;
				        for(var i in rolelst){
				        	var rowObj = rolelst[i];
				        	var modules = (rowObj[3]).split('.');
				        	var modulelength = modules.length;
	
				        	innerDiv +='<tr id="'+rowObj[3]+'"><td style="padding-left:' + modulelength * 2 +'em"><img src="images/folder.gif" onclick="closeTree(this,\'close\');" id="img-'+i+'" /> '+rowObj[1]+'<input type="hidden" id="moduleName-'+i+'" name="moduleName-'+i+'" value="'+rowObj[1]+'"/> </td>';
				        	if(rowObj[2].indexOf('R') == -1)
				        	{
								innerDiv += "<td><input type='checkbox' class= 'view' id='view-"+i+"' name='view-"+i+"' /> </td>";
							}
				        	else{
				        		innerDiv += "<td><input type='checkbox' class= 'view' id='view-"+i+"' name='view-"+i+"' checked='checked' /> </td>";
				        	}
				        	if(rowObj[2].indexOf('A') == -1)
				        	{
								innerDiv += "<td> <input type='checkbox' class= 'add' id='add-"+i+"' name='add-"+i+"' /> </td>";
							}
				        	else{
				        		innerDiv += "<td><input type='checkbox' class= 'add' id='add-"+i+"' name='add-"+i+"' checked='checked' /> </td>";
				        	}
				        	if(rowObj[2].indexOf('M') == -1)
				        	{
								innerDiv += "<td> <input type='checkbox' class= 'edit' id='edit-"+i+"' name='edit-"+i+"' /> </td>";
							}
				        	else{
				        		innerDiv += "<td><input type='checkbox' class= 'edit' id='edit-"+i+"' name='edit-"+i+"' checked='checked' /> </td>";
				        	}
				        	if(rowObj[2].indexOf('D') == -1)
				        	{
								innerDiv += "<td> <input type='checkbox' class= 'delete' id='delete-"+i+"' name='delete-"+i+"' /> </td></tr>";
							}
				        	else{
				        		innerDiv += "<td><input type='checkbox' class= 'delete' id='delete-"+i+"' name='delete-"+i+"' checked='checked' /> </td></tr>";
				        	}
				        }
				        $('#descDiv').html(desc);
				        $('#rightsTable tbody').html(innerDiv);
				        $('#userName').val(name);
				        $('input[id=desc]').val(desc);
				        $('#maxlength').val(maxlength);
				        $('#transType').val('R');
				        $("#loadRights").dialog({
				        	autoOpen: true,
			                title: "Assign Rights to Group : " + name,
					 		modal: true, width:850, height:500,
					 		close: function(i,el) {
					 			jQuery("#loadRights").dialog("destroy");
					 			$(el).html("");
					 		}
					 	});
			    	});
			    }
			});
		}else{
			alert(nameDesc);
		}
		$('.progress-indicator').css( 'display', 'none' );
	}

	function closeTree(el,type){
		var id= $(el).attr('id');
		var id1 = $(el).closest('tr').attr('id');	
		var modules1 = id1.split('.');
		var idlength = modules1.length;
		$('#rightsTable tr').each(function(i,ele){
			if(i==0){}else{
				var element1 = $(ele).attr('id');
				var modules = element1.split('.');
				var elelength = modules.length;
				if(elelength <= idlength) { }
				else{
					var dif = elelength - idlength;
					var tokens = element1.split('.'),
					    med = tokens.slice(0,idlength),
					    result = med.join('.');
					if(id1 == result){
						if(type=='close') {
							$(ele).hide();
							$(el).replaceWith('<img src="images/folder-closed.gif" onclick="closeTree(this,\'open\');" id="'+id+'"/>');
						}
						else{
							$(ele).show();
							$(ele).find('img').attr('src','images/folder.gif');
							$(el).replaceWith('<img src="images/folder.gif" onclick="closeTree(this,\'close\');" id="'+id+'"/>');
						}
					}
				}
			}
		});
	}
	
	$('#viewAll').click(function(){
		if($('#viewAll').is(':checked')) {
			$('.view').attr('checked','checked');
		}
		else{
			$('.view').removeAttr('checked');
		}
	});
	$('#addAll').click(function(){
		if($('#addAll').is(':checked')) {
			$('.add').attr('checked','checked');
		}
		else{
			$('.add').removeAttr('checked');
		}
	});
	$('#editAll').click(function(){
		if($('#editAll').is(':checked')) {
			$('.edit').attr('checked','checked');
		}
		else{
			$('.edit').removeAttr('checked');
		}
	});
	$('#deleteAll').click(function(){
		if($('#deleteAll').is(':checked')) {
			$('.delete').attr('checked','checked');
		}
		else{
			$('.delete').removeAttr('checked');
		}
	});
	
	function saveRights(){
		$('.progress-indicator').css( 'display', 'block' );
		$('input[type=checkbox]:checked').each(function(i,el){
			if($(el).attr('class')=='view'){
				$(el).attr('value','R');
			}
			if($(el).attr('class')=='add'){
				$(el).attr('value','A');
			}
			if($(el).attr('class')=='edit'){
				$(el).attr('value','M');
			}
			if($(el).attr('class')=='delete'){
				$(el).attr('value','D');
			}
		});
		$.ajax({
			type: "POST",
		    url: "saveModuleRights.action",
		    async:false,
		    data : $('#rightsForm').serialize(),
		    success:function (result){
		    	alert(saveSuccess);
		    	jQuery("#loadRights").dialog("destroy");
		    	$('#loadRights').html("");
		    	if($('#transType').val() == 'R'){
			    	$('#main').load('jsp/esecurity/Group_List.jsp',function(){});
		    	}
		    	else{
		    		$('#main').load('jsp/esecurity/User_Information.jsp',function(){});
		    	}
		    }
		});
		$('.progress-indicator').css( 'display', 'none' );
	}
	
	 function cancelRights(){
		 jQuery("#loadRights").dialog("destroy");
		 $('#loadRights').html("");
	 }
	 
// for manage_Users.jsp
	 
	 function getUsers(el,userName){
		$('.progress-indicator').css( 'display', 'block' );
		var parent = $(el).parent().parent();
		var validateCheck = 0;
		var name = "";
			var desc = "";
		if($(parent).find('#groupName').length) { 
			name = $(parent).find('#groupName').val();
			desc = $(parent).find('#description').val();
			if(name == null || $.trim(name) == "" || desc == null || $.trim(desc) == ""){
				validateCheck = 1;
			}
		}else{
			name = userName;
		}
		if(validateCheck == 0){
			$.ajax({
				type: "POST",
			    url: "loadUserByRoleCode.action",
			    async:false,
			    data : "roleCode="+ name,
			    success:function (result){
			    	$("#loadRights").load("jsp/esecurity/manage_Users.jsp",function(){
			    		var rolelst = result.roleList;
			    		var innerDiv = "<tr>";
			    		for(var i in rolelst){
			    			var noofcol = parseFloat(i)+1;
				        	innerDiv += "<td><div id='' style='border:1px solid;padding:10px;'>" + rolelst[i] + "<img src='images/close1.gif' id='img-"+i+"' style='float:right;' /></div></td>";
				        	if((noofcol%5 == 0)){
				        		innerDiv += "</tr><tr>";
				        	}
			    		}
			    		innerDiv += "</tr>";
			    		$('#descDiv').html(desc);
			    		$('#roleName').val(name);
				        $('input[id=desc]').val(desc);
			    		$('#roleCode').val(name);
			    		$('#usersTable tbody').html(innerDiv);
			    		$("#loadRights").dialog({
			    			autoOpen: true,
					 		title: "Manage Users to Group : " + name,
					 		modal: true, width:850, height:500,
					 		close: function(i,el) {
					 			jQuery("#loadRights").dialog("destroy");
					 			$(el).html("");
					 		}
					 	});
			    	});
			    }
			});
		}else{
			alert(nameDesc);
		}
		$('.progress-indicator').css( 'display', 'none' );
	}
	
	
	function saveUserToRole(){
		$('.progress-indicator').css( 'display', 'block' );
		var userNames = "";
		$('input[type=checkbox]:checked').each(function(i,el){
			userNames += $(el).val() + ",";
		});
		var roleCode = $('#roleName').val();
		var desc = $('input[id=desc]').val();
		$.ajax({
			type: "POST",
		    url: "saveRoleUser.action",
		    async:false,
		    data : "roleName="+roleCode+"&desc="+desc+"&userNames="+userNames,
		    success:function (result){
		    	alert(saveSuccess);
		    	jQuery("#loadRights").dialog("destroy");
		    	$('#loadRights').html("");
		    	$('#main').load('jsp/esecurity/Group_List.jsp',function(){});
		    }
		});
		$('.progress-indicator').css( 'display', 'none' );
	}
	function cancelUsersForGroup(){
		jQuery("#loadRights").dialog("destroy");
    	$('#loadRights').html("");
	}