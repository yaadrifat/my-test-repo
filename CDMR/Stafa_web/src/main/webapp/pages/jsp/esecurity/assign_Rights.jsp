<%@ taglib prefix="s" uri="/struts-tags" %>
<script type="text/javascript" src="jsp/esecurity/js/Group_List.js"></script>
<script>

$(document).ready(function(){
	
});


function savePages(){
	saveRights();
} 
</script>
<div id="rightsDiv">
	<div  class="portlet">
		<form id="rightsForm">
			<div class="portlet-content">
				<table cellpadding="0" cellspacing="0" border="0" id="rightsTable" class="display" >
					<thead>
						<tr>
							<th> Module </th>
							<th> <input type="checkbox" id="viewAll" /> View </th> 
							<th> <input type="checkbox" id="addAll" /> Add </th>
							<th> <input type="checkbox" id="editAll" /> Edit </th>
							<th> <input type="checkbox" id="deleteAll" /> Delete </th>
						</tr>
					</thead>	
					<tbody></tbody>
				</table>
			</div>
			<div id="hiddenDiv">
					<input type="hidden" id="userName" name="userName" />
					<input type="hidden" id="desc" name="description" />
					<input type="hidden" id="maxlength" name="maxlength" />
					<input type="hidden" id="transType" name = "transType" />
			</div>	
			<div style="float:right;">
				<input type="password" id="eSignRights" placeholder="e-Sign" size="5">
				<input type="button" value="Save" onclick="verifyeSign('eSignRights','save');" />
				<input type="button" value="Cancel" onclick="cancelRights();" />
		    </div>
	    </form>
	</div>
</div>
