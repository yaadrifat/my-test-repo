function showAccount(){
	document.getElementById("main").innerHTML="";
	$('#main').load('jsp/esecurity/edit_User_Details.jsp',function(){
		callAccount();		 
	});
}
function callAccount(userName){
	$('.progress-indicator').css( 'display', 'block' );
	$.ajax({
        type: "POST",
        url: "loadUserDetails.action",
        data: "userName="+userName,
        async:false,
        success: function (result){
        	//alert(result);
        	//alert(result.errorMessage);
        	var esecUserInfo = result.esecUserObject;
        	var firstName = esecUserInfo.firstName;
			var lastName = esecUserInfo.lastName;
			var userId = esecUserInfo.userId;
			var addr1 = esecUserInfo.addr1;
			var addr2 = esecUserInfo.addr2;
			var city = esecUserInfo.city;
			var email = esecUserInfo.email;
			var state = esecUserInfo.state;
			var country = esecUserInfo.country;
			var phone = esecUserInfo.phone;
			var postalCode = esecUserInfo.zipcode;
			var password = esecUserInfo.password;
			var esign = esecUserInfo.esign;
			var loginName = esecUserInfo.loginName;
			var clientUserId = esecUserInfo.clientUserId;
			var statusVal = esecUserInfo.userIsActive;
			var accExpDate = esecUserInfo.acctexpDate;
			var jobType = esecUserInfo.jobType;
			var timeZone= esecUserInfo.timeZone;
			var countryList="<option value='null'>Select</option>";	
			var jobTypeList="<option value='null'>Select</option>";
			var timeZoneList="<option value='null'>Select</option>";
			$('#firstName').val(firstName);
			$('#lastName').val(lastName);
			$('#userId').val(userId);
			$('#addr1').val(addr1);
			$('#addr2').val(addr2);
			$('#city').val(city);
			$('#email').val(email);
			$('#state').val(state);
			$('#phone').val(phone);
			$(result.countryList).each(function(i,v) {
				countryList=countryList+"<option value='"+v.pkCodelst+"'>"+v.description+"</option>";
			});
			$(result.jobTypeList).each(function(i,v){
        		jobTypeList=jobTypeList+"<option value='"+v.pkCodelst+"'>"+v.description+"</option>";
			});	
			$(result.timeZoneList).each(function(i,v){
				timeZoneList=timeZoneList+"<option value='"+v.pkCodelst+"'>"+v.description+"</option>";
			});	
			$('#countrylst').html("<select name='country' id='country'>"+countryList+"</select>");
			$('#jobtypelst').html("<select name='jobType' id='jobType'>"+jobTypeList+"</select>");
			$('#timezonelst').html("<select name='timeZone' id='timeZone'>"+timeZoneList+"</select>");
			$('#country').val(country);
			$('#jobType').val(jobType);
			$('#timeZone').val(timeZone);
			$('#postalCode').val(postalCode);
			$('#password').val(password);
			$('#esign').val(esign);
			$('#loginName').val(loginName);
			$('#userId').val(loginName);
			$('#clientUserId').val(clientUserId);
			$('#accExpDate').val(accExpDate);
			$('input:radio[name=status][value="'+statusVal+'"]').attr('checked', 'checked');  
			$('select').uniform();
        	errorMessage = result.errorMessage;
        	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
	$('.progress-indicator').css( 'display', 'none' );
}