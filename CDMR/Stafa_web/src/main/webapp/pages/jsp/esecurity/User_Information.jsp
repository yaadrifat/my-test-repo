<%@ taglib prefix="s" uri="/struts-tags" %>
<script type="text/javascript" src="jsp/esecurity/js/User_Information.js"></script>
<style>
/*  for styling the anchor tags  */
#userDiv a {
	color:#571D8D;
	cursor:pointer
}

#userDiv a{
margin-right: 40px;
}

.dataTables_paginate{
	width: 125px;
}

</style>
<script>
$(document).ready(function(){
	hide_slidewidgets();
	hide_slidecontrol();
	clearTracker();
	show_innernorth();
	/*$.ajax({
		type: "POST",
	    url: "loadAllUser.action",
	    async:false,
	    success:function (result){
	    	var resultStr = result.resultJson;
	    	alert("resultStr->"+resultStr);
			//resultStr = 
	    	$.each(resultStr, function(i,v) {
	    		alert("v[0]->"+v);
	        	$('#userTable').dataTable().fnAddData( [
					"<input type='checkbox' />",v[0],v[1],v[3],"<a href='#' onclick = 'getRole(this,\""+v[2]+"\");'> Reset </a>","<a href='#' onclick = 'getUsers(this,\""+v[2]+"\");'>Reset</a>","<a href='#' onclick = 'getUsers(this,\""+v[2]+"\");'>Reset</a>","<a href='#' onclick = 'getUsers(this,\""+v[2]+"\");'>Assign</a>"]);                                        
	    	});
	    }
	});*/
	applyDatatables();
	$('#generalPolicy').load('jsp/esecurity/General_Policy.jsp',function(){
		loadPolicyDetails();
		$("select").uniform();
	});
	
	$("select").uniform();
});
function applyDatatables(){	
	$.ajax({
		type: "POST",
	    url: "loadAllUser.action",
	    async:false,
	    success:function (result){
	    	var resultStr = result.roleList;
	    	
	    	for(var i in resultStr){
	        	var rowObj = resultStr[i];
	        	$('#userTable').dataTable().fnAddData( [
					"<input type='checkbox' />",rowObj[1],rowObj[0],"<a href='#' onclick = 'editUser(\""+rowObj[0]+"\");'> "+rowObj[2]+" </a>","<a href='#' onclick = 'conformPassword(\""+rowObj[0]+"\");'>Reset</a>","<a href='#' onclick = 'conformESign(\""+rowObj[0]+"\");'>Reset</a>","<a href='#' onclick = 'conformSession(\""+rowObj[0]+"\");'>Reset</a>","<a href='#' onclick = 'assign_rights(\""+rowObj[0]+"\");'>Assign</a>"]);                                        
	    	}
	    }
	});
}

</script>
	<div class="column" id="userDiv">
		<form>       
			<div  class="portlet">
				<div class="portlet-header"> User List </div>
				<div class="portlet-content">
					<table>
						<tr>
							<td style="width:20%"><div onclick="userInformation('');"><img src = "images/icons/addnew.jpg" class="cursor" />&nbsp;&nbsp;<label class="cursor"><b>Add</b></label>&nbsp;</div></td>
							<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick=""/>&nbsp;&nbsp;<label class="cursor"  onclick="deleteRows();"><b>Delete</b></label>&nbsp;</div></td>
						</tr>
					</table>
					 <table cellpadding="0" cellspacing="0" border="0" id="userTable" class="display" >
		            	<thead>
		                   <tr>
		                        <th colspan="1"><input type="checkbox" /> Check All </th>
		                        <th colspan="1" > Group </th>
		                        <th colspan="1"> Login Name </th>
								<th colspan="1"> User Name </th>
		                        <th colspan="1"> Reset Password </th>
		                        <th colspan="1"> Reset e-Sign </th>
		                        <th colspan="1"> Reset User session </th>
		                        <th colspan="1"> Assign User Rights </th>
		                    </tr>
						</thead>
						<tbody>
		                    <!-- <tr>
		                    	<td><input type="checkbox" /></td>
		                        <td> Velos Inc. </td>
		                        <td> Supervisor </td>
		                        <td> <a href="#" onclick = "editUser('login1');"> Cody Kang </a> </td>
		                        <td> <a href="#" onclick = "resetPassword('login1');"> Reset </a> </td>
		                        <td> <a href="#" onclick = "reseteSign('login1');"> Reset </a> </td>
		                        <td> <a href="#"> Reset </a> </td>
		                        <td> <a href="#" onclick="assign_rights('login1');"> Assign </a> </td>
							</tr>
							<tr>
		                    	<td><input type="checkbox" /></td>
		                        <td> Velos Inc. </td>
		                        <td> Supervisor </td>
		                        <td> <a href="#" onclick = "editUser('mari');"> Sonia Gai </a> </td>
		                        <td> <a href="#" onclick = "resetPassword('mari');"> Reset </a> </td>
		                        <td> <a href="#" onclick = "reseteSign('mari');"> Reset </a> </td>
		                        <td> <a href="#"> Reset </a> </td>
		                        <td> <a href="#" onclick="assign_rights('mari');"> Assign </a> </td>
							</tr>
							<tr>
		                    	<td><input type="checkbox" /></td>
		                        <td> Velos Inc. </td>
		                        <td> Supervisor </td>
		                        <td> <a href="#" onclick = "editUser('nurse1');"> Ian King </a> </td>
		                        <td> <a href="#" onclick = "resetPassword('nurse1');"> Reset </a> </td>
		                        <td> <a href="#" onclick = "reseteSign('nurse1');"> Reset </a> </td>
		                        <td> <a href="#"> Reset </a> </td>
		                        <td> <a href="#" onclick="assign_rights('nurse1');"> Assign </a> </td>
							</tr>-->
		            	</tbody>
		            </table>
				</div>
			</div>
		</form>
		<div  class="portlet">
		<div class="portlet-header"> General User Setting </div>
		<div class="portlet-content" id="generalPolicy">
		</div></div>
	
		
	</div>

<form id="addUserForm">
	<div class="column" id="addUserDiv">       
	
	</div>
</form>

<div id="loadRights"></div>
<div id="confDialog"></div>

<script>

$(function() {
    $('.jclock').jclock();
    $( ".column" ).sortable().sortable({
        connectWith: ".column"
    });
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
 
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });
	//$('.portlet-header .ui-icon:eq(1)').toggleClass( "ui-icon-plusthick" ).toggleClass( "ui-icon-minusthick" );
	//$('.portlet-header .ui-icon:eq(1)').parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    
    $('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});   
});


</script>