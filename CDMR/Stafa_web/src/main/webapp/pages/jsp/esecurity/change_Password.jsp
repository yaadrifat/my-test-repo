<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="/pages/jsp/common/includes.jsp" %>
<script>
var errorPassword = 0;
	function changePassword(){
		$('.progress-indicator').css( 'display', 'block' );
		if(errorPassword == 1){
			alert(passNotMatch);
		}
		else{
			var password = $('#newPassword').val();
			if((password == $('#loginName').val()) || ($.trim(password).length < 8 ) || (password.match(/[0-9]+/) == null) || (password.match(/[A-Za-z]+/) == null)){
				alert(passordRule);
			}else{
				$.ajax({
					type: "POST",
				    url: "updateAuthentication.action",
				    async:false,
				    data:$("#changePasswordForm").serialize(),
				    success:function (result){
				    	if(result.errorMessage!=null && result.errorMessage != ""){
				    		alert(result.errorMessage);
				    		$('#oldPassword').val('');
				    		$('#newPassword').val('');
				    		$('#confirmPassword').val('');
				    		$('#passwordDays').val('');
				    	}
				    	else{
					    	alert("Password Changed Successsully. You have been logged out.");
					    	javascript:logOut();
				    	}
				    }
				});
			}
		}
		$('.progress-indicator').css( 'display', 'none' );
	}
	function checkPassword(){
		if(($('#confirmPassword').val()) != ($('#newPassword').val())){
			errorPassword = 1;
			$('#errorPassword').show();
		}
		else{
			errorPassword = 0;
			$('#errorPassword').hide();
		}
	}
</script>
<div>
	<form id="changePasswordForm">
		<table>
			<tr>
				<td> Old Password </td><td><input type="password" id="oldPassword" name="oldPassword" /></td>
			</tr>
			<tr>
				<td> New Password </td><td><input type="password" id="newPassword" name="newPassword" /></td>
			</tr>
<!-- 			<tr> -->
<%-- 				<td colspan="2" style="color:red;"><s:text name="stafa.esecurity.label.passwordRule"/> </td> --%>
<!-- 			</tr> -->
			<tr>
				<td> Confirm Password </td><td><input type="password" id="confirmPassword" name="confirmPassword" onblur="checkPassword();" /><div id="errorPassword" style="display:none;color:red;"><s:text name="stafa.esecurity.label.passNotMatch"/></div></td>
			</tr>
			<tr>
				<td colspan="2"><s:text name="stafa.esecurity.label.passExpiresAfter"/> <input id="passwordDays" name = "passwordDays" type="text" size=5 /> <s:text name="stafa.esecurity.label.days"/> </td>
			</tr>
		</table>
		<div id="hiddenDiv">
			<input type="hidden" id="loginName" name="loginName" />
		</div>
		<input type="button" value="Save" onclick="changePassword();" />
		</form>
</div>     