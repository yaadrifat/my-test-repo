<%@ taglib prefix="s" uri="/struts-tags" %>
<script type="text/javascript" src="jsp/esecurity/js/Organization_Details.js"></script>
<style>
/*  for styling the anchor tags  */
#organizationDiv a {
	color:#571D8D;
}
</style>
<script>
$(document).ready(function(){
	var oTable=$("#organizationTable").dataTable({
		"sPaginationType": "full_numbers",
		"aoColumns": [ { "bSortable":false},
			               null,
			               null,
			               null,
			               null
			              ]
	});
	$("select").uniform();
});
</script>
<form>
	<div class="column" id="organizationDiv">       
		<div  class="portlet">
			<div class="portlet-header"> Organization Details </div>
			<div class="portlet-content">
				<table>
					<tr>
						<td style="width:20%"><div onclick="addNewOrganization();"><img src = "images/icons/addnew.jpg" class="cursor" />&nbsp;&nbsp;<label class="cursor"><b> Add </b></label>&nbsp;</div></td>
						<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick=""/>&nbsp;&nbsp;<label class="cursor"  onclick="deleteRows();"><b> Delete </b></label>&nbsp;</div></td>
					</tr>
				</table>
				 <table cellpadding="0" cellspacing="0" border="0" id="organizationTable" class="display" >
	            	<thead>
	                   <tr>
	                        <th colspan="1"><input type="checkbox" /> <s:text name="stafa.esecurity.label.checkAll"/> </th>
	                        <th colspan="1"> <s:text name="stafa.esecurity.label.org.name"/> </th>
	                        <th colspan="1"> <s:text name="stafa.esecurity.label.org.type"/> </th>
	                        <th colspan="1" > <s:text name="stafa.esecurity.label.org.description"/> </th>
	                        <th colspan="1"> <s:text name="stafa.esecurity.label.org.parentOrg"/> </th>
	                    </tr>
					</thead>
					<tbody>
	                    <tr>
	                    	<td><input type="checkbox" /></td>
	                        <td> <a href="#"> Velos Inc. </a></td>
	                        <td> Vendor </td>
	                        <td> Producer of Velos Stafa-CTL </td>
	                        <td> N/A </td>
						</tr>
						<tr>
	                    	<td><input type="checkbox" /></td>
	                        <td> <a href="#"> HCA </a></td>
	                        <td> Hospital </td>
	                        <td> Hospital Corporation of America </td>
	                        <td> N/A </td>
						</tr>
	            	</tbody>
	            </table>
			</div>
		</div>
	</div>
</form>
<form>
	<div class="column" id="addOrganizationDiv" style="display:none;">       
		<div  class="portlet">
			<div class="portlet-header">Organization Details</div>
			<div class="portlet-content">
				<table>
					<tr><td><s:text name="stafa.esecurity.label.org.orgName"/> </td><td> <input type="text" /> </td></tr>
					<tr><td><s:text name="stafa.esecurity.label.org.type"/>  </td><td> <select ><option>Select...</option></select></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.org.description"/> </td><td>  <input type="text" /></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.org.parentOrg"/></td><td> <select ><option>Select...</option></select></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.org.siteID"/> </td><td>  <input type="text" /></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.org.address"/> </td><td>  <input type="text" size="50"/></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.org.city"/>  </td><td> <input type="text" /></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.org.state"/> </td><td>  <input type="text" /></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.org.postalCode"/> </td><td>  <input type="text" /></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.org.country"/>  </td><td> <input type="text" /></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.org.phone"/> </td><td> <input type="text" /></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.org.email"/> </td><td>  <input type="text" /></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.org.notes"/> </td><td>  <input type="text" size="50"/></td></tr>
				</table>
				<input type="checkbox" /> <s:text name="stafa.esecurity.label.org.hideOrg"/>
				<div style="float:right;">
				<input type="button" value="Cancel" onclick="cancelOrgSave();"/>
				<input type="text" placeholder="e-Sign" size="5">
				<input type="button" value="Save" onclick="saveNewOrg();"/>
				</div>
	        </div>
		</div>
	</div>
</form>

<script>

$(function() {

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
    .append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span>");
	
	$( ".portlet-header .ui-addnew" ).click(function() {
	    showPopUp("open","addNewDonor","Add New Donor","900","700")
	});
	/**For Notes**/
	/* 
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='ui-addnew'></span>");
	
	$( ".portlet-header .ui-addnew" ).click(function() {
		addNewPopup("Add New Product","SupervisoraddNewDialog","addnewAcquisition","700","1100");
	});
	 */
	
	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});
</script></script>