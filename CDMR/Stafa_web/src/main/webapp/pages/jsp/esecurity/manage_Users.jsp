<%@ taglib prefix="s" uri="/struts-tags" %>
<script type="text/javascript" src="jsp/esecurity/js/Group_List.js"></script>
<script>
function savePages(){
	saveUserToRole();
}


function assignUser(){
	try{
		var roleCode = $('#roleCode').val();
		$.ajax({
			type: "POST",
		    url: "loadUserNotInRoleCode.action",
		    async:false,
		    data : "roleCode="+ roleCode,
		    success:function (result){

		    	var rolelst = result.roleList;
		    	var firstNamelst = result.firstNamelst;
		    	var lastNamelst = result.lastNamelst;
		    	$('#newUserDiv').show();
		    	
	        	oTable = $('#newUserTable').dataTable({"bJQueryUI": true, "sDom": 'Rlfrtip',"bRetrieve": true});
		    	for(var i in rolelst){
		        	var rowObj = rolelst[i];
		        	//$("#newUserTable").dataTable().fnDestroy();
		        	//$("#newUserTable").empty();
		        	oTable.fnAddData( [
						"<input type='checkbox' id='user-"+i+"' value='" + rolelst[i] + "' />",rolelst[i],firstNamelst[i],lastNamelst[i]]);                                        
		    	}
		    	$("select").uniform();
		    }
		});
	}catch(ex){
		alert(ex);
	}
} 
$(function() {


	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});

	/**For Notes**/
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

	/* $( ".portlet-header .ui-addnew" ).click(function() {
		showPopUp("open","addNewDialog","Add New Acquision","900","450")
	}); */
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");
	$( ".portlet-header .ui-reset " ).click(function() {
		var formName = document.getElementById("login");
		//clearForm(formName);
		clear();
	});

// 	$( ".portlet-header .ui-notes " ).click(function() {
// 		showNotes('open');
// 	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	

	$("td").removeClass('sorting_1');
});

</script>
<style>
#newUserTable_paginate{
	wodth:200px;
}
#newUserTable_previous{
	margin-right:50px;
}

</style>
<div id="userDiv">
	<div  class="portlet">
		<form id="userForm">
			<div class="portlet-content">
				<table cellpadding="0" cellspacing="0" border="0" id="usersTable" class="display" >
					<thead>
						<tr>
							<th colspan="5"> User Assigned to group </th>
						</tr>
					</thead>	
					<tbody></tbody>
				</table>
			</div>
			<div id="hiddenDiv">
				<input type="hidden" id="roleName" name="roleName" />
				<input type="hidden" id="desc" name="description" />
				<input type="hidden" id="roleCode" name="roleCode" />
			</div>	
		</form>
		<img src="images/icons/addnew.jpg" onclick="assignUser();" />Assign user(s) to this group
		
		<div class="portlet-content" id="newUserDiv" style="display:none;">
			<table cellpadding="0" cellspacing="0" border="0" id="newUserTable" class="display" >
				<thead>
					<tr>
						<th> <input type="checkbox" id="checkAllUser" /> <s:text name="stafa.esecurity.label.checkAll"/> </th>
						<th> <s:text name="stafa.esecurity.label.availUser"/> </th>
						<th> <s:text name="stafa.esecurity.label.firstName"/></th>
						<th> <s:text name="stafa.esecurity.label.lastName"/> </th>
					</tr>
				</thead>	
				<tbody></tbody>
			</table>
		</div>
		<form>
			<div style="float:right;">
				<input type="password" id="eSignUser" placeholder="e-Sign" size="5">
				<input type="button" value="Save" onclick="verifyeSign('eSignUser','save');" />
				<input type="button" value="Cancel" onclick="cancelUsersForGroup();" />
		    </div>
		</form>
	</div>
</div>
