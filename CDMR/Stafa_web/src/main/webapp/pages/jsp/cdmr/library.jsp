<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/jquery/ColVis.js"></script>
<link type="text/css" href="css/cdmr/themes/base/jquery.ui.all.css" rel="stylesheet" />
<link href="css/cdmr/ColReorder.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/cdmr/ColReorder.js"></script>
<script type="text/javascript" src="js/cdmr/ColumnStateSave.js"></script>

<div class="cleaner"></div>
 
 <div class="column" >	
 	
  	<div  class="portlet">
	<div class="portlet-header ">Files Library</div>
		<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="cdmTableUpdate" class="display draggable" >
			<thead>
			  <!--  <tr>
					<th colspan="1"><s:text name="vcdmr.label.status"/></th>
					<th colspan="1"><s:text name="vcdmr.label.filename"/></th>
					<th colspan="1"><s:text name="vcdmr.label.lastrevision"/></th>
					<th colspan="1"><s:text name="vcdmr.label.resivedby"/></th>
				</tr>-->
			</thead>
			<tbody>
				
			</tbody>
			</table>
		</div>
		
	</div>	
	
</div>		

<div class="cleaner"></div>

<script>
var oTable;
$(document).ready(function(){
	$.ajax({
		url:"getbannertext.action",
		type:"post",
		data:null,
		cache:false,
			success:function(response){
				
	      	  if(response.import_status=='1'&&response.export_status=='0'){
	      		$("#bannercontent").show();
	 				$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
				}
				else if(response.export_status=='1'&& response.import_status=='0'){
					$("#bannercontent").show();
					$("#bannercontent").html("Export Interface is in progress. System slowness may occur")
				}
				else if(response.import_status=='1' && response.export_status=='1'){
					$("#bannercontent").show();
					$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
					$("#bannercontent").append("<div style='background-color: yellow;'>Export Interface is in progress. System slowness may occur</div>");
				}
				else{
					$("#bannercontent").hide();
				}
	       }

	});
	$.ajax({
		url:'fetchColumn.action',
		type:'post',
		 data: {
	            "MODULENAME": "CDM Update",
	        },
		success:function(response){
	        initializeDataTable(response['colData']);
		}
	});
});

function initializeDataTable(column){
	oTable = $("#cdmTableUpdate").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "bDestroy": true,
	    "bPaginate": true,
		'iDisplayLength': 100,
	    "bDestroy": true,
	    "sAjaxSource": "loadCDMRLibFiles.action",
	    "bJQueryUI": true,
	    "bLengthChange": true,
	    "bFilter": true,
	    "bSort": true,
	    "sScrollY": "280px",
	    "sDom": 'RC<"clear">frtlip',
	    "sPaginationType": "full_numbers",
	   "oColVis": {
	    	"bShowAll": true,
	    	"sShowAll": "Select All"
        },
        "aaSorting": [[ 2, "desc" ]],
	    "aoColumns": column/*[
	      			{ "mDataProp": "COMMENTS", "sTitle":"Status"},
	      			{ "mDataProp": "FILE_NAME", "sTitle":"File Name"},
	      			{ "mDataProp": "LAST_MODIFIED_DATE","sTitle":"Last Revision", "sClass":"alignContent"},
	      			{ "mDataProp": "USER_FIRSTNAME","sTitle":"Revised by" }
	      		]*/

	});
	$(".ColVis_MasterButton span").html("");
	
}

$(function() {
	$(".ColVis_MasterButton span").html("");

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});

	/**For Notes**/
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

	/* $( ".portlet-header .ui-addnew" ).click(function() {
		showPopUp("open","addNewDialog","Add New Acquision","900","450")
	}); */
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");
	$( ".portlet-header .ui-reset " ).click(function() {
		var formName = document.getElementById("login");
		//clearForm(formName);
		clear();
	});

// 	$( ".portlet-header .ui-notes " ).click(function() {
// 		showNotes('open');
// 	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

});

function processFile(fileID)
{
	$("#storageinput").val(fileID);
	$('.progress-indicator').css( 'display', 'block' );
	$(".ui-dialog-titlebar-close").removeAttr('class');
$.ajax({
		url:"updatefilestatus.action",
		type:"POST",
		data:{fileid:fileID},
		success:function(response){
			$.ajax({
				url:"jsp/cdmr/staging.jsp",
				type:"POST",
				data:null,
				success:function(response){
					$('.progress-indicator').css( 'display', 'none' );
					$('#main').html(response);
				}
			});
		}
	});
}

</script>
 
 <style>
 
 .selectbox{
	width:100px;
}

.dataTables_scrollBody{
	height: 500px;
}

.DataTables_sort_wrapper{font-weight: bold;}
 
.TableTools{
width: 105px;
padding-left:10px;
float: left;
}

.tools{
	margin-top: 15px;
}

.ColVis_MasterButton{
	width: 150%;
}

.ColVis_MasterButton span{
	margin-left:15px;
}

.selectbox{
	width:100px;
}

.dataTables_scrollBody{
	height: 500px;
}


.paging_full_numbers{
	height: 40px;
}
.dataTables_paginate
{
float: right;
padding: 5px, 10px 24 px 0;
text-align:right;
width: 44px;
}

.selector_1 select {

      border: 1px solid !important;  /*Removes border*/
      -moz-appearance: none; /* Removes Default Firefox style*/
      background-position: 82px 7px;  /*Position of the background-image*/
      /*Width of select dropdown to give space for arrow image*/
      

      /*My custom style for fonts*/
		width:50px;
		height: 25px;
      color: #1455a2;
      margin-left: :35px;
}

td{
	font-size: 12px;
}
.dataTables_filter{
	margin-top: -25px !important;
}

.dataTables_info{
	width: 20% !important;
}

.alignContent{
	text-align: center;	
}

td {
  word-wrap: break-word;
}
.paging_full_numbers .ui-button{
	padding: 1%;
	width: 30px;
}

.dataTables_filter input[type="text"]{
	/*padding: 1px 22px 1px 10px;*/
	padding: 2px 22px 0px 10px;
}
</style>