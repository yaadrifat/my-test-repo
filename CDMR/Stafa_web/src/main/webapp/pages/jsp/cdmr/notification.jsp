<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="../common/includes.jsp" %>
<% String contextpath=request.getContextPath();

String CDMR_NOTIFCATIONS="Notification";
String CDMR_NOTIFCATIONS_EXPORT="Export Notification";
String CDMR_NOTIFCATIONS_IMPORT="Import Notification";
String CDMR_EMAIL_SETTINGS="Email Settings";
%>
<%!
String checkAccessRights(String moduleName){
	//System.out.println(" moduleName--------------------------- " + moduleName+"=="+(String)moduleMap.get(moduleName));
	 return (String)moduleMap.get(moduleName);
}
 %>

<meta http-equiv="PRAGMA" content="NO-CACHE">
<%@ taglib prefix="s" uri="/struts-tags"%>
<link type="text/css" href="css/cdmr/themes/base/jquery.ui.all.css" rel="stylesheet" />

<div id="tabs1">
<ul>
	<li><a href="#tabs-1">Inbox</a></li>
	<li><a href="#tabs-2" id="importdata">Import File status</a></li>
	<li><a href="#tabs-3" id="exportdata">Export Data status</a></li>
	<li><a href="#tabs-4" id="emailsettings">Email Settings</a></li>
</ul>
<div id="tabs-1" style="overflow: scroll">
	<div id="tableDetail" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" style="margin-top:10px;">
		<div class="portlet-header ui-widget-header ui-corner-all">Notification</div>
		 	<div class="portlet-content">
			 	<table cellpadding="0" cellspacing="0" border="0" id="notificationInfoTable">
					<thead>
					   <tr style="height:15px">
						   <th colspan="1" style="width:50px;height:20px">Date</th>
						   <th colspan="1" style="width:200pxheight:20px">Message</th>
				           <th colspan="1" style="width:200pxheight:20px">Delete</th>
				      </tr>
				   </thead>
				  <tbody></tbody>
			   </table>
			</div>
	 </div>
 </div>

<div id="tabs-2" style="height: 450px; overflow: scroll">
	<div style="width: 32%; float: left">
		<label >Send Message To: </label>
    	<div style="margin-top: 10px">
    		<input type="text" name="email" id="email" /> 
    		<span><a href="#" onclick="	loadAllUserTable();">Select User(s)</a></span>
    	</div>
    	<div style="margin-top: 20px;">
    		<label>When File Status Changed To: </label><br>
    		<select id="statusbarDiv" class="statusBar" style="margin-top: 5px;width:160px;"></select>
    	</div>
    	<div style="margin-top: 20px;"> 
    		<label>e-Signature</label><br>
    		<input type="password" name="esignId" id="esignId" style="margin-top: 5px;" />
    	</div>
   	   <div style="margin-top: 20px;">
   	   	<input type="button" value="Submit" onclick="verifyeSign('esignId','saveforNotification');" class="apply_button"/>
   	   </div>
   </div>
   <div id="tableDiv" 	class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" style="margin-top: 10x; display:none;float: right;width: 66%;">
	 <div class="portlet-header ui-widget-header ui-corner-all">User Search</div>
	  <div class="portlet-content" style="height: 25px">
		<table cellpadding="0" cellspacing="0" border="0" id="userTable" class="display">
		 <thead>
			<tr>
			  <th colspan="1">Check</th>
			  <th colspan="1">User Name</th>
			  <th colspan="1">Email</th>
			</tr>
		</thead>
		<tbody></tbody>
 	</table>
  </div>
 </div>
 <div id="tableDetail" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"	style="margin-top: 20px;">
	<div class="portlet-header ui-widget-header ui-corner-all">Notification</div>
	<div class="portlet-content">
	 <table cellpadding="0" cellspacing="0" border="0" id="notificationTable">
		<thead>
		 <tr style="height: 15px">
			<th colspan="1" style="width: 50px;">For File Status</th>
			<th colspan="1" style="width: 100px;">Notify User Name</th>
			<th colspan="1" style="width: 100px;">Notify User Email</th>
			<th colspan="1" style="width: 50px;">Delete</th>
		</tr>
	  </thead>
	  <tbody></tbody>
   </table>
  </div>
</div>
</div>

<div id="tabs-3" style="height: 450px; overflow: scroll">
	<div style="width: 32%; float: left">
		<label >Send Message To: </label>
		<div style="margin-top: 10px">	
			<input type="text" name="email" id="email1" /> 
			<span><a href="#" onclick="loadAllUserTableExport();">Select User(s)</a></span>
		</div>
		<div style="margin-top: 20px;">
			<label>When Data Tansfer : </label><br>
			<select id="transferstatus" style="margin-top: 5px;width:160px;" class="statusBar"></select>
		</div>
	<div style="margin-top: 20px;">
		<label>e-Signature</label><br>
		<input type="password" name="esignId" id="esignId1" style="margin-top: 5px;" />
	</div>
	<div style="margin-top: 20px;">
		<input type="button" value="Submit" onclick="verifyeSign('esignId1','saveforNotification1');" class="apply_button"/>
	</div>
  </div>
 <div id="tableDiv1" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" style="display:none;width: 66%; float: right;">
 	<div class="portlet-header ui-widget-header ui-corner-all">User Search</div>
		<div class="portlet-content" style="height: 25px;">
 		  <table cellpadding="0" cellspacing="0" border="0" id="userTable1" class="display">
			<thead>
				<tr>
					<th colspan="1">Check</th>
					<th colspan="1">User Name</th>
					<th colspan="1">Email</th>
				</tr>
			</thead>
		 <tbody></tbody>
       </table>
    </div>
 </div>
 <div id="tableDetail" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" style="margin-top: 20px;">
	<div class="portlet-header ui-widget-header ui-corner-all">Notification</div>
	<div class="portlet-content">
		<table cellpadding="0" cellspacing="0" border="0" id="notificationTable1">
			<thead>
				<tr style="height: 15px">
					<th colspan="1" style="width: 50px;">For Status</th>
					<th colspan="1" style="width: 100px;">Notify User Name</th>
					<th colspan="1" style="width: 100px;">Notify User Email</th>
					<th colspan="1" style="width: 50px;">Delete</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
 </div>
</div>
 <div id="tabs-4">
	<div style="margin-top:10px;" id="emailblock">
	<div>
	   <label>SMTP HOST:</label> <input name="host" id="host"	type="text" /> 
	   <label style="margin-left: 2%;">SMTP Port:</label> <input name="port" id="port" type="text" />
	</div>
	<br>
	<div>
	  <label>From Email:</label> <input name="fromemail" id="fromemail" type="text" /> 
	  <label style="margin-left: 2%;">Password :</label> 
	  <input name="password" id="password" type="password" />
	</div>
	<br>
	<div>
	 	<label>Subject:</label> <input name="subject" id="subject" 	type="text" /> 
	 	<label style="margin-left: 4%;">Secure Connection(SSL):</label> 
	 	<input name="sslenable" id="sslenable" type="checkbox" />
	</div>
	<br>
	<div>
	  <input type="button" class="apply_button"	onclick="applyEmailSettings();" value="Apply" />
	 </div>
	</div>
 </div>
</div>
 <script><!--
	var checkdAll = new Array();
   	var emailArray = new Array();
   	var chkIndex=0;
	
	$(document).ready(function(){
		$("#tabs1" ).tabs();
		loadNotifications();
		loadNotifiedUer();
		getDataForFileStatus();
		getEmailSettings();
		getDataForTransferStatus();
		loadNotifiedUserExport();
		///for export acess rights
	    if (!checkExportRights()){
		    $("#exportdata").css("display","none");
		    }
	    
	    ////for import access rights
	    if(!checkImportRights()){
	    	 $("#importdata").css("display","none");
		 }
		
	    ///for email tab access rights
	    if(!checkEmailviewRights()){
	    	 $("#emailsettings").css("display","none");
		    }
	   if(!checkEmailEditRights()){
		    $("#port").attr("disabled","disabled");
  			$("#host").attr("disabled","disabled");
  			$("#fromemail").attr("disabled","disabled");
  			$("#subject").attr("disabled","disabled");
  			$("#password").attr("disabled","disabled");
  			$("#sslenable").attr("disabled","disabled");
		 }
	});

	function loadNotifications(){
		$("#notificationInfoTable").dataTable({
			"bProcessing": false,
		    "bServerSide": true,
		    "sAjaxSource": "loadAllNotification.action",
		     "bJQueryUI": true,
		    "bPaginate": true,
		    'iDisplayLength': 10,
		    "bLengthChange": false,
		    "bDestroy": true,
		    "bFilter": true,
		    "bSort": true,
		    "sDom": 'R<"clear">lfrtip',
		    "aaSorting": [[ 0, "desc" ]],
		    "aoColumns": [
							{"sWidth":"20%","mDataProp":"SENT_ON","bSortable":true},
							{ "sWidth":"70%","mDataProp":"MSG_TEXT","bSortable":true},
			      			{"sWidth":"10%","mDataProp":"delete", "bSortable":false}	
			      	    ],
		});
		$(".ColVis_MasterButton span").html("");
	}

 	function getDataForFileStatus(){
   		
   		$.ajax({
   			url:"getfilestatus.action",
   			type:"post",
   			data:null,
   			success:function(response){
   				var option = "";
   				for(var i=0; i<response.statusList.length;i++){
   					option = option + '<option value="'+response.statusList[i].pkID+'">'+response.statusList[i].desc+'</option>'
   				
   				}
   				$("#statusbarDiv").html(option);
   			}
   		});
   		
   	}
	   	 
     	
   	function saveforNotification(){
   
   		if(checkdAll.length == 0){
   			alertify.alert("Please Add User(s).");
   			return;
   			}
   		var pkID = "";
   		
   		 for(var i=0; i<checkdAll.length;i++){
   			pkID = pkID+checkdAll[i] + ",";
   		 }
   		 
   		 $.ajax({
   				url:"savefornotification.action",
   				type:"post",
   				data:{'pkID':pkID, 'fileStatus':$("#statusbarDiv").val()},
   				success:function(response){
   					alertify.alert("User Added Successfully.");
   					loadNotifiedUer();
   					
   				}
   			});
   	 }
 	
   	function saveforNotification1(){
   	   
   		if(checkdAll.length == 0){
   			alertify.alert("Please Add User(s).");
   			return;
   			}
   		var pkID = "";
   		
   		 for(var i=0; i<checkdAll.length;i++){
   			pkID = pkID+checkdAll[i] + ",";
   		 }
   		
   		 $.ajax({
   				url:"savefornotification.action",
   				type:"post",
   				data:{'pkID':pkID, 'fileStatus':$("#transferstatus").val()},
   				success:function(response){
   					alertify.alert("User Added Successfully.");
   					
   					loadNotifiedUserExport();
   					
   				}
   			});
   	 }
   	function deleteNotifiedUser(value){
   	
   		if(checkImportRightsDel() && checkExportRightsDel()) {
   		var r = alertify.confirm("Do you want to remove user?", function(r){
   
   			if(r){
   				$.ajax({
   					url:"deleteNotifiedUser.action",
   					type:"post",
   					data:{pkID:value},
   					success:function(response){
   					
   						alertify.alert("Deleted Successfully.");
   						loadNotifiedUer();
   						loadNotifiedUserExport();
   					}
   			  });
   			}else{
   			}
   		return;
   		});
   		}
   		else{
   		 alertify.alert("You are not authorised user for this action.");
   		}
   	}
  
   	function addInUserArray(value, email){
   
   		if($.inArray(value, checkdAll) != -1){
   			checkdAll.splice($.inArray(value, checkdAll), 1);
   			chkIndex = chkIndex-1;
   			return;
   		}
   		checkdAll[chkIndex] = value;
   		emailArray [chkIndex] = email
   		chkIndex = chkIndex + 1;
   	}

   	function loadNotifiedUer(){
   	      
   		$("#notificationTable").dataTable({
   			"bProcessing": false,
   		    "bServerSide": true,
   		    "sAjaxSource": "getnotification.action",
   		     "bJQueryUI": true,
   		    "bPaginate": true,
   		    'iDisplayLength': 10,
   		    "bLengthChange": false,
   		    "bDestroy": true,
   		    "bFilter": true,
   		    "bSort": true,
   		    "sDom": 'R<"clear">lfrtip',
   		    "aoColumns": [
   							{"sWidth":"40%", "mDataProp":"FILE_DESC","bSortable":true},
   							{"sWidth":"40%","mDataProp":"USER_NAME", "bSortable":true},
   			      			{"sWidth":"40%","mDataProp":"USER_EMAIL", "bSortable":true},
   			      			{"sWidth":"20%","mDataProp":"delete", "bSortable":false}	
   			      	    ],
   		    "fnDrawCallback": function () {
   		    	$('#allcb').prop('checked', false);
   		    	$('tbody tr td input[name="checkForUSer"]').each(function(){
   			    	if($.inArray($(this).val(), checkdAll) != -1){
   			    		$(this).prop('checked', true);
   			    		$('#allcb').prop('checked', true);
   			    	}
   		        });
   		    	
   		    }
   		});
   		$(".ColVis_MasterButton span").html("");
   
   	}
   
   	function loadAllUserTable(){
   		if(!checkImportRightsAdd()){
   			alertify.alert("You are not authorised user for this acrion.");
			 return;
			 }   
   		$("#tableDiv").show();
   		$("#userTable").dataTable({
   			"bProcessing": false,
   		    "bServerSide": true,
   		    "sAjaxSource": "loadAllUserForCDMR.action",
   		     "bJQueryUI": true,
   		    "bPaginate": true,
   		    'iDisplayLength': 10,
   		    "bLengthChange": false,
   		    "bDestroy": true,
   		    "bFilter": true,
   		    "bSort": true,
   		    "sDom": 'R<"clear">lfrtip',
   		    "aoColumns": [
   							{"sWidth":"10%","mDataProp":"check","bSortable":false},
   			      			{"sWidth":"30%","mDataProp":"NAME", "bSortable":true},
   			      			{"sWidth":"60%","mDataProp":"USER_EMAIL", "bSortable":true, "sClass":"emailIds"}	
   			      	    ],
   		    "fnDrawCallback": function () {
   		    	$('#allcb').prop('checked', false);
   		    	$('tbody tr td input[name="checkForUSer"]').each(function(){
   			    	if($.inArray($(this).val(), checkdAll) != -1){
   			    		$(this).prop('checked', true);
   			    		$('#allcb').prop('checked', true);
   			    	}
   		        });
   		    	
   		    }
   		});
   		$(".ColVis_MasterButton span").html("");
   		
   		$('<button id="addnotes" class="staging_button" onclick="addValueinText();">Submit</button>').appendTo(' div#userTable_filter');
   	}
   	function loadAllUserTableExport(){
   		if(!checkExportRightsAdd()){
   			alertify.alert("You are not authorised user for this action.");
			 return;
			 }
   		$("#tableDiv1").show();
   		$("#userTable1").dataTable({
   			"bProcessing": false,
   		    "bServerSide": true,
   		    "sAjaxSource": "loadAllUserForCDMR.action",
   		     "bJQueryUI": true,
   		    "bPaginate": true,
   		    'iDisplayLength': 10,
   		    "bLengthChange": false,
   		    "bDestroy": true,
   		    "bFilter": true,
   		    "bSort": true,
   		    "sDom": 'R<"clear">lfrtip',
   		    "aoColumns": [
   							{"sWidth":"10%","mDataProp":"check","bSortable":false},
   			      			{"sWidth":"30%","mDataProp":"NAME", "bSortable":true},
   			      			{"sWidth":"60%","mDataProp":"USER_EMAIL", "bSortable":true, "sClass":"emailIds"}	
   			      	    ],
   		    "fnDrawCallback": function () {
   		    	$('#allcb').prop('checked', false);
   		    	$('tbody tr td input[name="checkForUSer"]').each(function(){
   			    	if($.inArray($(this).val(), checkdAll) != -1){
   			    		$(this).prop('checked', true);
   			    		$('#allcb').prop('checked', true);
   			    	}
   		        });
   		    	
   		    }
   		});
   		$(".ColVis_MasterButton span").html("");
   		
   		$('<button id="addnotes" class="staging_button" onclick="addValueinText1();">Submit</button>').appendTo(' div#userTable1_filter');
   	   		
   	   	}
   	function loadNotifiedUserExport(){
 	      
   		$("#notificationTable1").dataTable({
   			"bProcessing": false,
   		    "bServerSide": true,
   		    "sAjaxSource": "getnotificationexport.action",
   		     "bJQueryUI": true,
   		    "bPaginate": true,
   		    'iDisplayLength': 10,
   		    "bLengthChange": false,
   		    "bDestroy": true,
   		    "bFilter": true,
   		    "bSort": true,
   		    "sDom": 'R<"clear">lfrtip',
   		    "aoColumns": [
   							{"sWidth":"40%", "mDataProp":"FILE_DESC","bSortable":true},
   							{"sWidth":"40%","mDataProp":"USER_NAME", "bSortable":true},
   			      			{"sWidth":"40%","mDataProp":"USER_EMAIL", "bSortable":true},
   			      			{"sWidth":"20%","mDataProp":"delete", "bSortable":false}	
   			      	    ],
   		    "fnDrawCallback": function () {
   		    	$('#allcb').prop('checked', false);
   		    	$('tbody tr td input[name="checkForUSer"]').each(function(){
   			    	if($.inArray($(this).val(), checkdAll) != -1){
   			    		$(this).prop('checked', true);
   			    		$('#allcb').prop('checked', true);
   			    	}
   		        });
   		    	
   		    }
   		});
   		$(".ColVis_MasterButton span").html("");
   
   	}

   	function getDataForTransferStatus(){
   		$.ajax({
   			url:"getdataTransferstatus.action",
   			type:"post",
   			data:null,
   			success:function(response){
   				
   				$("#transferstatus").html(response['transferstate']);
   			}
   		});
   		
   	}
	function addValueinText1(){
   		var e = "";
   		if(emailArray.length == 0){
   			alertify.alert("Please Select User(s).");
   			return;
   		}
   		for(var i=0; i<emailArray.length;i++){
   			e = e + emailArray[i] + ",";
   		}
   		$("#email1").val(e.slice(0,-1));
   		$("#tableDiv1").hide();
   	}
   	function addValueinText(){
   		var e = "";
   		if(emailArray.length == 0){
   			alertify.alert("Please Select User(s).");
   			return;
   		}
   		for(var i=0; i<emailArray.length;i++){
   			e = e + emailArray[i] + ",";
   		}
   		$("#email").val(e.slice(0,-1));
   		$("#tableDiv").hide();
   	}
   	
  
   
   	function applyEmailSettings(){
   		var sslEnable;
   		if($("#host").val() == "" || $("#port").val()== "" || $("#fromemail").val()== "" || $("#subject").val() ==""){
   			alertify.alert("All fields are mandatory.");
   			
   		}
   		else{
	   		if($("#host").val() == ""){ 
	   			alertify.alert("Please provide host address.");
	   			return;
	   		}
	
	   		if($("#port").val()== ""){ 
	   			alertify.alert("Please provide port.");
	   			return;
	   		}
	
	   		if($("#fromemail").val()== ""){ 
	   			alertify.alert("Please provide from address.");
	   			return;
	   		}
	
	   		if($("#subject").val() ==""){ 
	   			alertify.alert("Please provide subject.");
	   			return;
	   		}
   		}
   		if($("#sslenable").is(":checked")){
   			sslEnable = "true";
   		}else{
   			sslEnable = "false";
   		}
   
   		$.ajax({
   			url:"updateemailsettings.action",
   			type:"POST",
   			data:{host:$("#host").val(), port:$("#port").val(), fromemail:$("#fromemail").val(), password:$("#password").val(), subject:$("#subject").val(), 
   			sslenable:$("#sslenable").is(":checked")},
   			success:function(response){
   				alertify.alert("Email settings updated successfully.");
   				$.unblockUI(); 
   			}
   		});
   		
   	}
   	function getEmailSettings(){
   		$.ajax({
   			url:"getemailsettings.action",
   			type:"POST",
   			data:null, 
   			success:function(response){
   				var data = response.data;
   				for(var i=0; i<data.length; i++){
   					if(data[i].PARAM_SUBTYP == "PORT"){
   						$("#port").val(data[i].PARAM_VAL);
   					}
   					if(data[i].PARAM_SUBTYP == "PASSWORD"){
   						$("#password").val(data[i].PARAM_VAL);
   					}
   					if(data[i].PARAM_SUBTYP == "SUBJECT"){
   						$("#subject").val(data[i].PARAM_VAL);
   					}
   					if(data[i].PARAM_SUBTYP == "SSL" && data[i].PARAM_VAL == "true"){
   						$("#sslenable").attr("checked", "checked");
   					}
   					if(data[i].PARAM_SUBTYP == "SMTP"){
   						$("#host").val(data[i].PARAM_VAL);
   					}
   					if(data[i].PARAM_SUBTYP == "FROM"){
   						$("#fromemail").val(data[i].PARAM_VAL);
   					}
   				}
   			}
   		});
   	}
   	function deleteFileNotification(value){
   	 //////for inbox delete access
 	   
	    if(!NotifydelRights()){
	    	alertify.alert("You are not authorised user for this action.");
		    return ;
		    }
   		var r = alertify.confirm("Do you want to delete notification?", function(r){

   			if(r){
   				$.ajax({
   					url:"deleteNotification.action",
   					type:"post",
   					data:{pkID:value},
   					success:function(response){
   						alertify.alert("Deleted Successfully.");
   						//loadNotifiedUer();
   					}
   			  });
   			}else{
   			}
   		return;
   		});
   		
   	}
	////-------import block-------------
	function checkImportRights(){
		
		return checkAppRights("<%=checkAccessRights(CDMR_NOTIFCATIONS_IMPORT)%>",APPMODE_READ);
		
	}
	function checkImportRightsAdd(){
		
		return checkAppRights("<%=checkAccessRights(CDMR_NOTIFCATIONS_IMPORT)%>",APPMODE_ADD);
		
	}
	function checkImportRightsDel(){
	
		return checkAppRights("<%=checkAccessRights(CDMR_NOTIFCATIONS_IMPORT)%>",APPMODE_DELETE);
	}

	//////---------------import end-------------------
	////------------export---------------
	function checkExportRights(){

		return checkAppRights("<%=checkAccessRights(CDMR_NOTIFCATIONS_EXPORT)%>",APPMODE_READ);
		
	}
	function checkExportRightsAdd(){

		return checkAppRights("<%=checkAccessRights(CDMR_NOTIFCATIONS_EXPORT)%>",APPMODE_ADD);
		
	}
	function checkExportRightsDel(){

		return checkAppRights("<%=checkAccessRights(CDMR_NOTIFCATIONS_EXPORT)%>",APPMODE_DELETE);
		
	}
	////---------------export end-------------------
	///////------email settings-------------------
	function checkEmailviewRights(){
		return checkAppRights("<%=checkAccessRights(CDMR_EMAIL_SETTINGS)%>",APPMODE_READ);
	} 
	
	function checkEmailEditRights(){
		return checkAppRights("<%=checkAccessRights(CDMR_EMAIL_SETTINGS)%>",APPMODE_SAVE);
	}
	////----------email settings end---------------
	function NotifydelRights(){
		return checkAppRights("<%=checkAccessRights(CDMR_NOTIFCATIONS)%>",APPMODE_DELETE);
		}

	
--></script>

<style>

.apply_button {
	background-color: #4CAF50;
	border: 1px solid #4CAF50;
	width: 80px;
	height: 31px;
	font-size: 17px;
	-webkit-border-top-left-radius: 8px;
	-moz-border-radius-topleft: 8px;
	border-top-left-radius: 8px;
	-webkit-border-top-right-radius: 8px;
	-moz-border-radius-topright: 8px;
	border-top-right-radius: 8px;
	-webkit-border-bottom-right-radius: 8px;
	-moz-border-radius-bottomright: 8px;
	border-bottom-right-radius: 8px;
	-webkit-border-bottom-left-radius: 8px;
	-moz-border-radius-bottomleft: 8px;
	border-bottom-left-radius: 8px;
	color: #fff;
	margin-right: 0;
	cursor: pointer;
	-moz-box-shadow: inset 0px 1px 0px 0px #bcbcbc;
	-webkit-box-shadow: inset 0px 1px 0px 0px #bcbcbc;
	box-shadow: inset 0px 1px 0px 0px #bcbcbc;
}
.staging_button{
	-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf) );
	background:-moz-linear-gradient( center top, #ededed 5%, #dfdfdf 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf');
	background-color:#ededed;
	-webkit-border-top-left-radius:5px;
	-moz-border-radius-topleft:5px;
	border-top-left-radius:5px;
	-webkit-border-top-right-radius:5px;
	-moz-border-radius-topright:5px;
	border-top-right-radius:5px;
	-webkit-border-bottom-right-radius:5px;
	-moz-border-radius-bottomright:5px;
	border-bottom-right-radius:5px;
	-webkit-border-bottom-left-radius:5px;
	-moz-border-radius-bottomleft:5px;
	border-bottom-left-radius:5px;
	border:1px solid #dcdcdc;
	color:#777777;
	font-weight:bold;
	height:25px;
	line-height:23px;
	text-align:center;
	text-shadow:1px 1px 0px #ffffff;
}

.staging_button:hover{
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #dfdfdf), color-stop(1, #ededed) );
	background:-moz-linear-gradient( center top, #dfdfdf 5%, #ededed 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#dfdfdf', endColorstr='#ededed');
	background-color:#dfdfdf;
}

#addnotes{
	position: absolute;
	right: 220px;
}

#notificationInfoTable {
	width: 100% !important;
}

#notificationInfoTable_wrapper .dataTables_filter {
	margin-top: 2px !important;
}
#notificationTable_wrapper .dataTables_filter {
	margin-top: 2px !important;
}
#notificationTable1_wrapper .dataTables_filter {
	margin-top: 2px !important;
}
#userTable1_wrapper .dataTables_filter {
	margin-top: 2px !important;
}

#userTable_wrapper .dataTables_filter {
	margin-top: 2px !important;
}

td {
  word-wrap: break-word;
}

#notificationTable {
	width: 100% !important;
}

#notificationTable td {
	text-align: left;
	padding-left: 1%;
}
#notificationTable th {
	text-align: left;
}

#notificationTable1 {
	width: 100% !important;
}
#notificationTable1 td {
	text-align: left;
	padding-left: 1%;
}
#notificationTable1 th {
	text-align: left;
}
#notificationInfoTable td{
	padding-left: 1%;
}
</style>