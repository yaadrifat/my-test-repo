<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<script type="text/javascript" src="../../js/jquery/jquery-1.6.3.js"></script>
 <script type="text/javascript" src="../../js/jquery/jquery.blockUI.js"></script>
  <script type="text/javascript" src="../../js/cdmr/alertify.min.js"></script>
 <link rel="stylesheet" href="../../css/cdmr/alertify.core.css" type="text/css">
  <link rel="stylesheet" href="../../css/cdmr/alertify.bootstrap.css" type="text/css">
<script type="text/javascript">

$(document).ready(function(){
	parent.closeProcessWindow();
});

$('#iteneraryDocument').live('change', function(){ 
    $(".file").val($("#iteneraryDocument").val());
}); 

var _validFileExtensions = [".xls", ".xlsx"];

function Validate(oForm) {
    var arrInputs = oForm.getElementsByTagName("input");
    for (var i = 0; i < arrInputs.length; i++) {
        var oInput = arrInputs[i];
        if (oInput.type == "file") {
            var sFileName = oInput.value;
            if(sFileName == ""){
            	alert("Please Select File");
            	return false;
            }
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validFileExtensions.length; j++) {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }

                if (!blnValid) {
                	parent.showMessage(sFileName, _validFileExtensions);
                    //alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                    return false;
                }
            }
        }
    }
    parent.processWindow();
    return true;
}


</script>
<style type="text/css">
	.upload{
		position: absolute;
		height: 30px;
		width: 65px;
	}
	
	.fileupload{
		border: 1px solid;
		height:30px;
		margin-right: 10px;
	}
	
	body {
	font-family: 'Lucida Grande', 'Helvetica Neue', sans-serif;
	font-size: 13px;
}

div.custom_file_upload {
	width: 230px;
	height: 20px;
	
}

input.file {
	width: 225px;
	height: 32px;
	border: 1px solid #BBB;
	border-right: 0;
	color: #888;
	padding: 5px;
	
	-webkit-border-top-left-radius: 5px;
	-webkit-border-bottom-left-radius: 5px;
	-moz-border-radius-topleft: 5px;
	-moz-border-radius-bottomleft: 5px;
	border-top-left-radius: 5px;
	border-bottom-left-radius: 5px;
	
	outline: none;
}

div.file_upload {
	width: 80px;
	height: 24px;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf) );
	background:-moz-linear-gradient( center top, #ededed 5%, #dfdfdf 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf');
	background-color:#ededed;

	display: inline;
	position: absolute;
	overflow: hidden;
	cursor: pointer;
	
	-webkit-border-top-right-radius: 5px;
	-webkit-border-bottom-right-radius: 5px;
	-moz-border-radius-topright: 5px;
	-moz-border-radius-bottomright: 5px;
	border-top-right-radius: 5px;
	border-bottom-right-radius: 5px;
	left:45%;
	top:0%;
	font-weight: bold;
	color: #777777;
	text-align: center;
	padding-top: 8px;
}
div.file_upload:before {
	content: 'Browse';
	position: absolute;
	left: 0; right: 0;
	text-align: center;
	
	cursor: pointer;
}

div.file_upload input {
	position: relative;
	height: 30px;
	width: 250px;
	display: inline;
	cursor: pointer;
	opacity: 0;
	
}

.staging_button{
	-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf) );
	background:-moz-linear-gradient( center top, #ededed 5%, #dfdfdf 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf');
	background-color:#ededed;
	-webkit-border-top-left-radius:5px;
	-moz-border-radius-topleft:5px;
	border-top-left-radius:5px;
	-webkit-border-top-right-radius:5px;
	-moz-border-radius-topright:5px;
	border-top-right-radius:5px;
	-webkit-border-bottom-right-radius:5px;
	-moz-border-radius-bottomright:5px;
	border-bottom-right-radius:5px;
	-webkit-border-bottom-left-radius:5px;
	-moz-border-radius-bottomleft:5px;
	border-bottom-left-radius:5px;
	text-indent:0;
	border:1px solid #dcdcdc;
	display:inline-block;
	color:#777777;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	font-style:normal;
	height:30px;
	line-height:30px;
	text-decoration:none;
	text-align:center;
	text-shadow:1px 1px 0px #ffffff;
	margin-top: 10px;
	margin-left: 100px;
	position: absolute;
	top:-6%;
}

.staging_button:hover{
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #dfdfdf), color-stop(1, #ededed) );
	background:-moz-linear-gradient( center top, #dfdfdf 5%, #ededed 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#dfdfdf', endColorstr='#ededed');
	background-color:#dfdfdf;
}
</style>
</head>
			<body style="margin:0px;">
				<div class="custom_file_upload">
					<s:form action="uploadxlsFile.action" onsubmit="return Validate(this);" namespace="/" method="POST" enctype="multipart/form-data">
						<div>
							<s:textfield cssClass="file"></s:textfield>
							<div>
								<div class="file_upload" style="float: left;">
									<s:file name="fileUpload" cssClass="fileupload" label="Select a File to upload" size="20" id="iteneraryDocument"/>
								</div>
								<div style="float: right;">
									<s:submit value="Upload" id="fileUploadSubmit" name="submit" cssClass="staging_button"/>
								</div>
							</div>
						</div>
					</s:form>
				</div>
				
				</body>
			</html>	
			