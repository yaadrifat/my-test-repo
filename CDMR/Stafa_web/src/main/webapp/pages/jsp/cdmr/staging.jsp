<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/jquery/ColVis.js"></script>
<link type="text/css" href="css/cdmr/themes/base/jquery.ui.all.css" rel="stylesheet" />
<link href="css/cdmr/ColReorder.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/cdmr/ColReorder.js"></script>
<script type="text/javascript" src="js/notes.js"></script>
<script type="text/javascript" src="js/cdmr/ColumnStateSave.js"></script>


<div id="notesContainer"></div>
<div class="cleaner"></div>
 
 <div class="column" >	
 	<div  class="portlet" id="headerinfo" >
		<div id="fileinfo">
			<span id="filename"></span>
			<span id="mdate"></span>
			<span id="mby"></span>
			<span id="status"></span>		
		</div>
	</div>
  	<div  class="portlet">
	<div class="portlet-header ">Services in Staging</div>
		<div class="portlet-content">
		
			<table cellspacing="0" border="0" id="pendingDonorsTableTech" class="stagingdata" style="display: none" >
			<thead>
			  <!--  <tr>
					<th colspan="1"><s:text name="vcdmr.label.instCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.BCBSHCPCSCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.BCBSEffDate"/></th>
					<th colspan="1"><s:text name="vcdmr.label.MCAIDHCPCSCPTCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.MCAIDEffDate"/></th>
					<th colspan="1"><s:text name="vcdmr.label.CMSHCPCSCPTCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.CMSEffDate"/></th>
					<th colspan="1"><s:text name="vcdmr.label.PRSCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.PRSEffDate"/></th>
					<th colspan="1"><s:text name="vcdmr.label.RevClass"/></th>
					<th colspan="1"><s:text name="vcdmr.label.RevClassDesc"/></th>
					<th colspan="1"><s:text name="vcdmr.label.SVCCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.SVCCodeDesc"/></th>
					<th colspan="1"><s:text name="vcdmr.label.SVCCodeGNLDesc"/></th>
					<th colspan="1"><s:text name="vcdmr.label.GLKey"/></th>
					<th colspan="1"><s:text name="vcdmr.label.SVCCodeINACTDate"/></th>
					<th colspan="1"><s:text name="vcdmr.label.SVCPrice"/></th>
					<th colspan="1"><s:text name="vcdmr.label.SVCPriceEffDate"/></th>
					<th colspan="1"><s:text name="vcdmr.label.notes"/></th>
				</tr>-->
			</thead>
			<tbody>
				<!-- Datat Table Data -->
			</tbody> 
			</table>
			<!-- -Table for Professional Data -->
			<table cellspacing="0" border="0" id="pendingDonorsTablepro" class="prostagingdata" style="display: none" >
			<thead>
			  <!--  <tr>
					<th colspan="1"><s:text name="vcdmr.label.instCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.cptcode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.MCAIDHCPCSCPTCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.CMSHCPCSCPTCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.PRSCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.genrdesc"/></th>
					<th colspan="1"><s:text name="vcdmr.label.eiwINACTDate"/></th>
					<th colspan="1"><s:text name="vcdmr.label.prschrg"/></th>
					<th colspan="1"><s:text name="vcdmr.label.eiwEffDate"/></th>	
					<th colspan="1"><s:text name="vcdmr.label.notes"/></th>
				</tr>-->
			</thead>
			<tbody>
				<!-- Datat Table Data -->
			</tbody>
			</table>
		</div>
		
	</div>
	<div class="column" style="border: none;text-align: left">	
		<div  class="portlet" style="font-size: 11px;border: none;float: right;width: 70%;margin-top: -10px;">
			<div class="portlet-content" style="border: none;">
				<div id="assignDonorDiv" style="overflow-x:auto;overflow-y:hidden;width: 100%">
					<label style="margin-left: 25%">Next Status</label>
					<select id="statusbar" >
						<option selected="selected">Work in Progress</option>
					</select>
					<label style="margin-left: 5%;">e-Signature</label>
					<input type="password" id="eSign" class="textbox" />
					<input type="button" class=" btn_green" id="cdmsubmit"  value="Submit" onclick="verifyeSign('eSign','submitInCDMMain');" / >	
				</div>
			</div>
		</div>	
		</div>
	<div class="progress-indicator">
   		<img src="images/icon_loading_75x75.gif" alt="" />
	</div>
	<div  id="ieprogressindicator" class="" style="display:none;">
   		<center><img src="images/icon_loading_75x75.gif" alt="" /></center>
	</div>
</div>		

<div class="cleaner"></div>
<div id="groupDialog" style="display: none;">
	<div id="divInDialog">
		
	</div>
</div>

<div id="notesDialog" style="display: none;">
	<textarea rows="5" cols="20"></textarea>
</div>
<div id="dateDiv" style="display: none;">
	<div>
		<label>Select Date:</label><input type="text" id="effdatepicker">
	</div>
	</br>
	<div>
		Apply on Current Record: <input style="margin-left:16%" type="radio" checked="checked" name="applyType" id="applyType" value="current"></br>
	</div>
	</br>
	<div>
		Apply on Selected Records: <input style="margin-left:11%" type="radio" name="applyType" id="applyType" value="selected"></br>
	</div>
	</br>
	<div>
		Apply on All in Staging Records: <input type="radio" name="applyType" id="applyType" value="allStaging"></br>
	</div>
	</br>
	<div>
		Overwrite Existing Date: <input style="margin-left:17%" type="checkbox" name="overwrite" id="overwrite" value="overwrite">
	</div>
</div>
<div>
	<input type="hidden" id="grpval">
</div>

<div id="domMessage" style="display:none;height: 30px; "> 
    	<p style="font-size: 14px;">Updated Successfully.</p>
	</div> 
	
<!-- -GROUP -->
<div id="createGroup" style="width:100%;display: none;overflow:scroll;height: 550px;">
	 <div   id="pheader" style="margin-top:0px;" >
       <%-- <div id="header" style="float: left; margin-top:0px;width:200px">Select Services Group</div>--%>
	    <div id="cheader" style="float: left; margin-top:0px;"></div>
	</div>
	<div>
		<div class="column" style="width: 40%; float: left;">	
		<div  class="portlet" >
			<div class="portlet-header "><s:text name="vcdmr.label.listofavailablegrp"/></div>
			<div class="portlet-content">
				<div id="assignDonorDiv" style="overflow-x:auto;overflow-y:hidden;">
					<div id="divInDialog">
						<table class="gridtable" style="">
					        <thead>
					        <tr>
					            <th colspan="1" style="width:10%;">Level</th>
								<th colspan="1" style="width:45%;">Name</th>
								<th colspan="1" style="width:45%;">Create New</th>
							</tr>	
					        </thead>
					        <tbody id="divInDialogTab" style="margin-top:10px;width:550px;">
							
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>	
	<div class="column" style="text-align: left">	
		<div  class="portlet" style="font-size: 11px;">
			<div class="portlet-content">
				<div id="assignDonorDiv" style="overflow-x:auto;overflow-y:hidden;">
					<input type="text" name="grplevelname" id="grplevelname" style="display: none;">
					<input type="button" id="crtsergrplevel" class="staging_button" value="Create More Level" onclick="createMoreGroup();">
					<input type="button" class="staging_button" value="Remove Last Level" onclick="removeonemorlevel();"/>
					<input type="button" class="staging_button" value="Attach Service Section" onclick="submitInStagingComprac();" >
				</div>
			</div>
		</div>
	</div>
	
	<div class="column" >	
		<div  class="portlet" >
			<div class="portlet-header "><s:text name="vcdmr.label.groupworkarea"/></div>
			<div class="portlet-content">
				<div id="assignDonorDiv" style="overflow-x:auto;overflow-y:hidden;">
					<table cellspacing="0" border="0" id="groupdata" class="groupdata" style="display: none" >
						<thead>
						   <tr>
								<th colspan="1"><s:text name="vcdmr.label.serviceCode"/></th>
								<th colspan="1"><s:text name="vcdmr.label.Description"/></th>
								<th colspan="1"><s:text name="vcdmr.label.cptcode"/></th>
								<th colspan="1"><s:text name="vcdmr.label.prscode"/></th>
								<th colspan="1"><s:text name="vcdmr.label.hccchg"/></th>
								<th colspan="1"><s:text name="vcdmr.label.prschg"/></th>
								<th colspan="1"><s:text name="vcdmr.label.revclass"/></th>
								<th colspan="1"><s:text name="vcdmr.label.effdate"/></th>
							</tr>
						</thead>
						<tbody>
							<!-- Datat Table Data -->
						</tbody>
					</table>
					
					
					
			<table cellspacing="0" border="0" id="techstagingroupdata" class="techstagingroupdata" style="display: none" >
			   <thead>
			     <!--<tr>
			   		
					<th colspan="1"><s:text name="vcdmr.label.instCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.BCBSHCPCSCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.BCBSEffDate"/></th>
					<th colspan="1"><s:text name="vcdmr.label.MCAIDHCPCSCPTCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.MCAIDEffDate"/></th>
					<th colspan="1"><s:text name="vcdmr.label.CMSHCPCSCPTCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.CMSEffDate"/></th>
					<th colspan="1"><s:text name="vcdmr.label.PRSCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.PRSEffDate"/></th>
					<th colspan="1"><s:text name="vcdmr.label.RevClass"/></th>
					<th colspan="1"><s:text name="vcdmr.label.RevClassDesc"/></th>
					<th colspan="1"><s:text name="vcdmr.label.SVCCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.SVCCodeDesc"/></th>
					<th colspan="1"><s:text name="vcdmr.label.SVCCodeGNLDesc"/></th>
					<th colspan="1"><s:text name="vcdmr.label.GLKey"/></th>
					<th colspan="1"><s:text name="vcdmr.label.SVCCodeINACTDate"/></th>
					<th colspan="1"><s:text name="vcdmr.label.SVCPrice"/></th>
					<th colspan="1"><s:text name="vcdmr.label.SVCPriceEffDate"/></th>
					<th colspan="1"><s:text name="vcdmr.label.GroupCode"/></th>
					
				</tr>
			--></thead>
			<tbody>
				<!-- Datat Table Data -->
			</tbody>
			</table>
			<!-- -Table for Professional Data -->
			<table cellspacing="0" border="0" id="prostagingroupdata" class="prostagingroupdata" style="display: none" >
			<thead>
			   <!--<tr>
			   		
					<th colspan="1"><s:text name="vcdmr.label.instCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.cptcode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.MCAIDHCPCSCPTCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.CMSHCPCSCPTCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.PRSCode"/></th>
					<th colspan="1"><s:text name="vcdmr.label.genrdesc"/></th>
					<th colspan="1"><s:text name="vcdmr.label.eiwINACTDate"/></th>
					<th colspan="1"><s:text name="vcdmr.label.prschrg"/></th>
					<th colspan="1"><s:text name="vcdmr.label.eiwEffDate"/></th>
					<th colspan="1"><s:text name="vcdmr.label.GroupCode"/></th>			
					
				</tr>
			--></thead>
			<tbody>
				<!-- Datat Table Data -->
			</tbody>
			</table>
					
				</div>
			</div>
		</div>
	</div>
	<!-- <div class="column" style="border: none;text-align: left">	
		<div  class="portlet" style="font-size: 11px;border: none;">
			<div class="portlet-content" style="border: none;">
				<div id="assignDonorDiv" style="overflow-x:auto;overflow-y:hidden;float: right;">
					<input type="button" class="staging_button" value="Submit" onclick="submitInStagingComprac();" >
					<input type="button" class="staging_button" value="Ok" onclick="closePopupWindow();" >
				</div>
			</div>
		</div>
	</div>-->
</div>

<!-- Group End -->

<script><!--
var index = 1;
var groupCode;
var oTable;
var jqTds;
var jqSer;
var jqeTds ;
var checkdAll = new Array();
var chkIndex=0;
var staData;
var idholder;
var fileType;
var modulename;
var flagForCheck = false;
var redrawFlag = "";
var notesButLink = false;
$(document).ready(function(){
	$.ajax({
		url:"getbannertext.action",
		type:"post",
		data:null,
		cache:false,
			success:function(response){
				
	      	  if(response.import_status=='1'&&response.export_status=='0'){
	      		$("#bannercontent").show();
	 				$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
				}
				else if(response.export_status=='1'&& response.import_status=='0'){
					$("#bannercontent").show();
					$("#bannercontent").html("Export Interface is in progress. System slowness may occur")
				}
				else if(response.import_status=='1' && response.export_status=='1'){
					$("#bannercontent").show();
					$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
					$("#bannercontent").append("<div style='background-color: yellow;'>Export Interface is in progress. System slowness may occur</div>");
				}
				else{
					$("#bannercontent").hide();
				}
	       }

	});
	$("#nav li a").each(function(){
		$(this).css('color', 'white');
		$(this).css('text-decoration', 'none');
	});
	
	$("#nav li:nth-child(2)").find('a').css('color', 'yellow');
	
	$("#nav li:nth-child(2)").find('a').css('text-decoration', 'underline');
	
	$("#effdatepicker").datepicker({ dateFormat: 'dd-M-yy', changeMonth: true,
	    changeYear: true  });
	
	$('.stagingdata tbody td.ui-datepicker-inline input').live('focus', function (e){
		// $(this).datepicker({ dateFormat: 'mm-dd-yy' }).datepicker("show");
		$("#dateDiv").dialog('open');
	});

	
	$.ajax({
		url:"getStagingType.action",
		type:"post",
		data:{fileID:$("#storageinput").val()},
		success:function(response){
			fileType=response.fileType;
			modulename=response.moduleName;
			
			$.ajax({
				url:'fetchColumn.action',
				type:'post',
				 data: {
			            "MODULENAME": modulename,
			        },
				success:function(response){
			        	drawTableForStagingData((response['colData']));
				}
			});
		}
	});
	
	$("#effdate").datepicker();
	
	var nEditing = null;
    
    $('.stagingdata a.edit').live('click', function (e) {
    	
        e.preventDefault();
         
        /* Get the row as a parent of the link that was clicked on */
        var nRow = $(this).parents('tr')[0];
        if ( nEditing !== null && nEditing != nRow ) {
        	id = $(this).attr('id');
            /* A different row is being edited - the edit should be cancelled and this row edited */
            restoreRow( oTable, nEditing );
            editRow( oTable, nRow );
            nEditing = nRow;
        }
        else if ( nEditing == nRow && this.innerHTML == '<img src="images/cdmr/save.png">' ) {
            /* This row is being edited and should be saved */
            saveRow( oTable, nEditing);
            nEditing = null;
        }
        else {
            /* No row currently being edited */
            id = $(this).attr('id');
            editRow( oTable, nRow );
            nEditing = nRow;
        }
    } );
    
    
    getNextPossibleState();
    getGetHeaderInfo();
    checkForNotes();

    
	
});
var noteButtonFlag = false;

function notes(){
	noteButtonFlag = true;
	notesButLink = true;
	var fileID = $("#storageinput").val();
	var module = "Staging";
	var obj = "";
	var flag = false;
	var url="fetchNotes";
	var userNoteID ="STAG";
	var parameters="{entityId:'"+fileID+"',entityType:'"+module+"',flag:'"+flag+"', userNoteID:'"+userNoteID+"'}";
	addNewPopup('Notes','notesContainer',url,"700","700","jsonData="+parameters);
	$('#noteView').html('');
	$("#tempEntityId").val(id);
	$("#tempEntityType").val(donor);
	$('#newNoteCreate').html('');
	$("iframe html body").html("123");
}

/*function notes(){
	var fileID = $("#storageinput").val();
	var module = "Staging";
	var obj = "";
	var flag = false;
	var url="fetchNotes";
	var parameters="{entityId:'"+fileID+"',entityType:'"+module+"',flag:'"+flag+"'}";
	addNewPopup('Notes','notesContainer',url,"700","700","jsonData="+parameters);
	$('#noteView').html('');
	$("#tempEntityId").val(id);
	$("#tempEntityType").val(donor);
	$('#newNoteCreate').html('');
	$("iframe html body").html("123");
}*/

function submitInCDMMain(){
	if($("#statusbar").val()==""){
		alertify.alert("No Further Action Allowed.");
		return;
	}
	
	var fileID = $("#storageinput").val();
	$.ajax({
		url:"submitincdmmain.action",
		type:"post",
		data:{fileid:fileID, statusbar:$("#statusbar").val(), text:$("#statusbar option:selected").text()},
		success:function(response){
			$.blockUI({ message: $('#domMessage') }); 
			setTimeout(  function()  { }, 5000);
			$.unblockUI();
			getGetHeaderInfo();
			getNextPossibleState();
		}
    });
}


function fetchColumnProp(modname){
	var column;
	$.ajax({
		url:'fetchColumn.action',
		type:'post',
		async: false,
		 data: {
	            "MODULENAME": modname,
	        },
		success:function(response){
	        column=response['colData'];
		}
	});	
	return column;
}

function drawTableForStagingData(column){
	
	$(".stagingdata").removeAttr('style');
	$(".prostagingdata").css('display', 'none');
	var fileID = $("#storageinput").val();

	$(".stagingdata").empty();
	oTable = $(".stagingdata").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getcomprecdata.action",
	    "fnServerParams": function ( aoData ) {
	         aoData.push( { "name": "fileID", "value":  fileID} );
	     },
	     "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bLengthChange": true,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "responsive": true,
	    "bAutoWidth": true,
	  //  "sScrollY": "440px",
	    "sScrollY":($(window).height() - 500) + "px",
	    "sScrollX": "100%",
	    "sScrollXInner":"100%",
	    "sDom": 'RC<"clear">frtlip',
	    "sPaginationType": "full_numbers",
	    "oColVis": {
	    	"bShowAll": true,
	    	"sShowAll": "Select All"
        },
	    "aoColumns": fetchColumnProp(modulename)/*[
			{"sWidth":"50px", "mDataProp":"instcode", "bSortable":true,"sTitle":"RT" },
			{"sWidth":"100px","mDataProp":"BCBSHCPCSCode","bSortable":true,"sTitle":"BCBS HCPCS/CPT Code"},
			{"sWidth":"100px","mDataProp":"BCBSEffDate","bSortable":true,"sTitle":"BCBS Eff Date"},
			{"sWidth":"100px", "mDataProp":"MCAIDHCPCSCPTCode","bSortable":true,"sTitle":"MCAID HCPCS/CPT Code"},
			{"sWidth":"100px", "mDataProp":"MCAIDEffDate","bSortable":true,"sTitle":"MCAID Eff Date"},
			{"sWidth":"100px", "mDataProp":"CMSHCPCSCPTCode","bSortable":true,"sTitle":"CMS HCPCS/CPT Code"},
			{"sWidth":"100px","mDataProp":"CMSEffDate","bSortable":true,"sTitle":"CMS Eff Date"},
			{"sWidth":"100px","mDataProp":"PRSCode","bSortable":true,"sTitle":"PRS Code"},
			{"sWidth":"100px","mDataProp":"PRSEffDate","bSortable":true,"sTitle":"PRS Eff Date"},
			{"sWidth":"100px","mDataProp":"RevClass","bSortable":true,"sTitle":"REV Class"},
			{"sWidth":"200px","mDataProp":"RevClassDesc", "bSortable":true,"sTitle":"REV Class Desc"},
			{"sWidth":"100px","mDataProp":"SVCCode", "bSortable":true,"sTitle":"SVC Code"},
			{"sWidth":"200px","mDataProp":"SVCCodeDesc", "bSortable":true,"sTitle":"Service Description"},
			{"sWidth":"200px","mDataProp":"SVCCodeGNLDesc", "bSortable":true,"sTitle":"SVC Code Gnl Desc"},
			{ "sWidth":"100px","mDataProp":"GLKey", "bSortable":true,"sTitle":"GL Key"},
			{"sWidth":"100px","mDataProp":"SVCCodeINACTDate", "bSortable":true,"sTitle":"SVC Code Inact Date"},
			{"sWidth":"100px","mDataProp":"SVCPrice", "bSortable":true, "sClass":"priceCode","sTitle":"SVC Price"},
			{"sWidth":"100px","mDataProp":"SVCPriceEffDate", "bSortable":true,"sTitle":"Price Eff Date"},
			{"sWidth":"100px","mDataProp":"notestd","sTitle":"Notes"}
	    ]*/
	   
	});
	$(window).bind('resize', function () {
		oTable.fnAdjustColumnSizing();
		});
	
	$(".ColVis_MasterButton span").html("");
	//$('<button id="grouping" class="staging_button" onclick="openCreateGroupForTechnicalData();">Create Service Section</button>').appendTo('div.dataTables_filter');
	$('<button id="addnotes" class=" btn_blue" style="margin-top:-25px;" onclick="notes();">File Notes <img src="images/cdmr/notes.png" id="buttonNotes" style="display:none;"/></button>').appendTo('div.dataTables_filter');
	checkForNotes();
}

function checkForNotes(){
	var fileID = $("#storageinput").val();
	$.ajax({
		url:"checkfornotes.action",
		type:"post",
		data:{"fileID":fileID},
		success:function(response){
			if(response.checkForNotes == true){
				$("#buttonNotes").removeAttr('style');
			}
		}
	});
}
/*
function drawTableForProfessionalData(column){
	redrawFlag = "P";
	$(".prostagingdata").removeAttr('style');
	$(".stagingdata").css('display', 'none');
	var fileID = $("#storageinput").val();
	
	$(".prostagingdata").empty();
	oTable = $(".prostagingdata").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getcomprecdataforProf.action",
	    "fnServerParams": function ( aoData ) {
	         aoData.push( { "name": "fileID", "value":  fileID} );
	     },
	     "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bLengthChange": true,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "sScrollY": "270px",
	    "sScrollX": "100%",
	    "sScrollXInner":"100%",
	    "sDom": 'RC<"clear">frtlip',
	    "sPaginationType": "full_numbers",
	    "oColVis": {
	    	"bShowAll": true,
	    	"sShowAll": "Select All"
        },
	    "aoColumns": column/*[
			{"sWidth":"50px", "mDataProp":"instcode", "bSortable":true,"sTitle":"RT"},
			{"sWidth":"100px","mDataProp":"cptcode","bSortable":true,"sTitle":"CPT Code"},
			{"sWidth":"100px", "mDataProp":"MCAIDHCPCSCPTCode","bSortable":true,"sTitle":"MCAID HCPCS/CPT Code"},
			{"sWidth":"100px", "mDataProp":"CMSHCPCSCPTCode","bSortable":true,"sTitle":"CMS HCPCS/CPT Code"},
			{"sWidth":"100px","mDataProp":"PRSCode","bSortable":true,"sTitle":"PRS Code"},
			{"sWidth":"250px","mDataProp":"genrdesc","bSortable":true,"sTitle":"PRS Code Desc"},
			{"sWidth":"100px","mDataProp":"SVCCodeINACTDate", "bSortable":true,"sTitle":"EIW Inactive Date"},
			{"sWidth":"100px","mDataProp":"prschrg","bSortable":true,"sClass":"priceCode","sTitle":"PRS Price"},
			{"sWidth":"100px","mDataProp":"SVCPriceEffDate", "bSortable":true,"sTitle":"EIW Active Date"},
			{"sWidth":"100px","mDataProp":"notestd", "sTitle":"Notes"}
	    ]
	});
	$(".ColVis_MasterButton span").html("");
	//$('<button id="grouping" class="staging_button" onclick="openCreateGroupForProfessionalData();">Create Service Section</button>').appendTo('div.dataTables_filter');
	$('<button id="addnotes" class="staging_button" style="margin-top:-25px;" onclick="notes();">File Notes  <img src="images/cdmr/notes.png" id="buttonNotes" style="display:none;"/></button>').appendTo('div.dataTables_filter');
	checkForNotes();
}
*/


function notesForRow(id){
	notesButLink = false;
	var fileID = id;
	var module = "Staging";
	var obj = "";
	var flag = false;
	var url="fetchNotes";
	var userNoteID ="STAG";
	var parameters="{entityId:'"+fileID+"',entityType:'"+module+"',flag:'"+flag+"', userNoteID:'"+userNoteID+"'}";
	addNewPopup('Notes','notesContainer',url,"700","700","jsonData="+parameters);
	$('#noteView').html('');
	$("#tempEntityId").val(id);
	$("#tempEntityType").val(donor);
	$('#newNoteCreate').html('');
	$("iframe html body").html("123");

	
	
}


/*function notesForRow(id){
	var fileID = id;
	var module = "Staging";
	var obj = "";
	var flag = false;
	var url="fetchNotes";
	var parameters="{entityId:'"+fileID+"',entityType:'"+module+"',flag:'"+flag+"'}";
	addNewPopup('Notes','notesContainer',url,"700","700","jsonData="+parameters);
	$('#noteView').html('');
	$("#tempEntityId").val(id);
	$("#tempEntityType").val(donor);
	$('#newNoteCreate').html('');
	$("iframe html body").html("123");
}*/



function getNextPossibleState(){
	var fileID = $("#storageinput").val();
	$.ajax({
		url:"getnextpossiblestats.action",
		type:"post",
		data:{fileID:fileID },
		success:function(response){
			var filestatus = response.filestatus;
			var value = response.value;
			var option = '';
			if(filestatus.length == 0){
				option = '<option value="">No Action</option>';
			}
			for(var i=0; i<filestatus.length;i++){
				option += '<option value="'+value[i]+'">'+filestatus[i]+'</option>'
			}
			$("#statusbar").html(option);
		}
    });
}

function getGetHeaderInfo(){
	var fileID = $("#storageinput").val();
	$.ajax({
		url:"headerfileinfo.action",
		type:"post",
		data:{fileID:fileID },
		success:function(response){
			var fileinfo = response.fileInfo;
			//var date = fileinfo[4].split(" ")[0];

			//new Date(date)
			$("#filename").html('<b> &nbsp;&nbsp;File Name:</b>&nbsp;&nbsp;'+fileinfo[3]);
			$("#mdate").html('<b>&nbsp;&nbsp;Date Modified:</b>&nbsp;&nbsp;'+fileinfo[4]);
			$("#mby").html('<b>&nbsp;&nbsp;Modified By:</b> &nbsp;&nbsp;' +fileinfo[2] + ", " + fileinfo[1]);
			$("#status").html('<b>&nbsp;&nbsp;Current Status:</b> &nbsp;&nbsp;'+fileinfo[0] + '&nbsp&nbsp');
			
		}
    });
}
/*
function addInArray(value){

	if($.inArray(value, checkdAll) != -1){
		checkdAll.splice($.inArray(value, checkdAll), 1);
		return;
	}
	checkdAll[chkIndex] = value;
	chkIndex = chkIndex + 1;
}







function getGroups(pn, id){
	
	$.ajax({
		url:"getgropus.action",
		type:"post",
		data:{parentNode:pn},
		success:function(response){
		var data = response['groups'];
		var select = '<div class="dropdown"><select name="groupone'+pn+'" id="'+index+'" class="selectbox" onchange="getGroups(this.value, this.id);">';
		var option = '<option value="">Select Value</option>';
		if(data.length == 0)
			return;
		for (var i = 0; i < data.length; i++) {
			option += '<option value="'+data[i][0]+'">'+data[i][2]+'</option>';
		}
		select = select + option + '</select></div><br/>'
		$("#divInDialog").append(select);
		index++;
		}
		
	});

	

}
*/
function openNoteDialog(){
	$("#notesDialog").dialog('open');
}

$(function() {

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});

	/**For Notes**/
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

	/* $( ".portlet-header .ui-addnew" ).click(function() {
		showPopUp("open","addNewDialog","Add New Acquision","900","450")
	}); */
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");
	$( ".portlet-header .ui-reset " ).click(function() {
		var formName = document.getElementById("login");
		//clearForm(formName);
		clear();
	});

// 	$( ".portlet-header .ui-notes " ).click(function() {
// 		showNotes('open');
// 	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

});

/*
function openCreateGroupForTechnicalData(){
	
	flagForCheck = false;

	 var dialogData ="";
	  dialogData = $(".column #headerinfo").clone(true);
	  checkdAll = checkdAll.filter(function(n){ return n != undefined });
	  checkdAll = checkdAll.filter(function(n){ return n != 'on' });

		if(checkdAll.length == 0){
			alertify.alert("Please Select Items.");
			return;
		}
		drawtabletech();
	
	$(".ColVis_MasterButton span").html("");
	$("#divInDialogTab").html("");
	getSlectedGroups(0);
	 $("#createGroup #cheader").html("");
     $("#createGroup #cheader").append(dialogData);		
     $("#createGroup #headerinfo").removeAttr('style');
     $("#createGroup #headerinfo").attr('style','margin-top:0px;font-size: .8em');
	
	$(".ColVis_MasterButton span").html("");
	 $("#createGroup").css({'height':'550px'});
	$.blockUI({title:'<img style="cursor:auto;width:16px;height:16px;" src="images/cdmr/closeicon.png" onclick="blockUIClose();"/>', theme:true, draggable:true, message: $('#createGroup'), 
		themedCSS: {width: '81%', left:'100px', top:'10px'} }); 
}



function drawtabletech(column){
	$(".techstagingroupdata").removeAttr('style');
	$(".prostagingroupdata").css('display', 'none');
	checkdAll = checkdAll.filter(function(n){ return n != undefined });
	checkdAll = checkdAll.filter(function(n){ return n != 'on' });

	if(checkdAll.length == 0){
		alertify.alert("Please Select Items.");
		return;
	}
	
	arrayStr = "";
	for(var i=0; i<checkdAll.length; i++){
		arrayStr += checkdAll[i] + ",";
	}
	oTable = $(".techstagingroupdata").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getstagingthecData.action",
	    "fnServerParams": function ( aoData ) {
		     aoData.push( { "name": "pkString", "value": arrayStr},{ "name": "fileType", "value": fileType});
	     },
	        "bJQueryUI": true,
		    "bPaginate": true,
		    'iDisplayLength': 10,
		    "bLengthChange": false,
		    "bDestroy": true,
		    "bFilter": true,
		    "bSort": true,
		    "sScrollY": "200px",
		    "sScrollX": "100%",
		    "sDom": 'RC<"clear">lfrtip',
	        "aoColumns": [			
			{"sWidth":"100px", "mDataProp":"instcode", "bSortable":true},
			{"sWidth":"100px","mDataProp":"BCBSHCPCSCode","bSortable":true},
			{"sWidth":"100px","mDataProp":"BCBSEffDate","bSortable":true},
			{"sWidth":"100px", "mDataProp":"MCAIDHCPCSCPTCode","bSortable":true},
			{"sWidth":"100px", "mDataProp":"MCAIDEffDate","bSortable":true},
			{"sWidth":"100px", "mDataProp":"CMSHCPCSCPTCode","bSortable":true},
			{"sWidth":"100px","mDataProp":"CMSEffDate","bSortable":true},
			{"sWidth":"100px","mDataProp":"PRSCode","bSortable":true},
			{"sWidth":"100px","mDataProp":"PRSEffDate","bSortable":true},
			{"sWidth":"100px","mDataProp":"RevClass","bSortable":true},
			{"sWidth":"200px","mDataProp":"RevClassDesc", "bSortable":true},
			{"sWidth":"100px","mDataProp":"SVCCodeType", "bSortable":true},
			{"sWidth":"100px","mDataProp":"SVCCode", "bSortable":true},
			{"sWidth":"200px","mDataProp":"SVCCodeDesc", "bSortable":true},
			{"sWidth":"200px","mDataProp":"SVCCodeGNLDesc", "bSortable":true},
			{ "sWidth":"100px","mDataProp":"GLKey", "bSortable":true},
			{"sWidth":"100px","mDataProp":"SVCCodeINACTDate", "bSortable":true},
			{"sWidth":"100px","mDataProp":"SVCPrice", "bSortable":true,"sClass":"priceCode"},
			{"sWidth":"100px","mDataProp":"SVCPriceEffDate", "bSortable":true},
			{ "sWidth":"100px","mDataProp":"GroupCode", "bSortable":true}
			
	    ]
	});
	$(".ColVis_MasterButton span").html("");
}

function openCreateGroupForProfessionalData(){
	
	flagForCheck = false;

	 var dialogData ="";
	  dialogData = $(".column #headerinfo").clone(true);
	  checkdAll = checkdAll.filter(function(n){ return n != undefined });
	  checkdAll = checkdAll.filter(function(n){ return n != 'on' });
		if(checkdAll.length == 0){
			alertify.alert("Please Select Items.");
			return;
		}
	  drawtablePro();
	
	$(".ColVis_MasterButton span").html("");
	$("#divInDialogTab").html("");
	 getSlectedGroups(0);
	 $("#createGroup #cheader").html("");
     $("#createGroup #cheader").append(dialogData);		
     $("#createGroup #headerinfo").removeAttr('style');
     $("#createGroup #headerinfo").attr('style','margin-top:0px;font-size: .8em');
	
	$(".ColVis_MasterButton span").html("");
	 $("#createGroup").css({'height':'550px'});
	$.blockUI({title:'<img style="cursor:auto;width:16px;height:16px;" src="images/cdmr/closeicon.png" onclick="blockUIClose();"/>', theme:true, draggable:true, message: $('#createGroup'), 
		themedCSS: {width: '81%', left:'100px', top:'10px'} }); 
}

function drawtablePro(){
	
	$(".prostagingroupdata").removeAttr('style');
	$(".techstagingroupdata").css('display', 'none');
	checkdAll = checkdAll.filter(function(n){ return n != undefined });
	checkdAll = checkdAll.filter(function(n){ return n != 'on' });

	if(checkdAll.length == 0){
		alertify.alert("Please Select Items.");
		return;
	}
	
	arrayStr = "";
	for(var i=0; i<checkdAll.length; i++){
		arrayStr += checkdAll[i] + ",";
	}
	oTable = $(".prostagingroupdata").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getstagingproData.action",
	    "fnServerParams": function ( aoData ) {
	         aoData.push( { "name": "pkString", "value": arrayStr},{ "name": "fileType", "value": fileType});
	     },
	        "bJQueryUI": true,
		    "bPaginate": true,
		    'iDisplayLength': 10,
		    "bLengthChange": false,
		    "bDestroy": true,
		    "bFilter": true,
		    "bSort": true,
		    "sScrollY": "200px",
		    "sScrollX": "100%",
		    "sDom": 'RC<"clear">lfrtip',
	        "aoColumns": [			
			{"sWidth":"50px", "mDataProp":"instcode", "bSortable":true},
			{"sWidth":"100px","mDataProp":"cptcode","bSortable":true},
			{"sWidth":"100px", "mDataProp":"MCAIDHCPCSCPTCode","bSortable":true},
			{"sWidth":"100px", "mDataProp":"CMSHCPCSCPTCode","bSortable":true},
			{"sWidth":"100px","mDataProp":"PRSCode","bSortable":true},
			{"sWidth":"250px","mDataProp":"genrdesc","bSortable":true},
			{"sWidth":"100px","mDataProp":"SVCCodeINACTDate", "bSortable":true},
			{"sWidth":"100px","mDataProp":"prschrg","bSortable":true,"sClass":"priceCode"},
			{"sWidth":"100px","mDataProp":"SVCPriceEffDate", "bSortable":true},
			{ "sWidth":"100px","mDataProp":"GroupCode", "bSortable":true}
			
	    ]
	    
	});
	$(".ColVis_MasterButton span").html("");
}

function openCreateGroup(){

	flagForCheck = false;

	 var dialogData ="";
	  dialogData = $(".column #headerinfo").clone(true);

	checkdAll = checkdAll.filter(function(n){ return n != undefined });
	checkdAll = checkdAll.filter(function(n){ return n != 'on' });

	if(checkdAll.length == 0){
		alertify.alert("Please Select Items.");
		return;
	}
	
	arrayStr = "";
	for(var i=0; i<checkdAll.length; i++){
		arrayStr += checkdAll[i] + ",";
	}

	staData = $("#groupdata").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getstagingdata.action",
	    "fnServerParams": function ( aoData ) {
	         aoData.push( { "name": "pkString", "value": arrayStr});
	     },
	     "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 10,
	    "bLengthChange": false,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "sScrollY": "200px",
	    "sScrollX": "100%",
	    "sDom": 'RC<"clear">lfrtip',
	    "aoColumns": [
	      			{"sWidth":"110%", "sClass":"servcode", "mDataProp":"servcode","bSortable":true},
	      			{"sWidth":"200%","sClass":"desc", "mDataProp":"desc","bSortable":true},
	      			{"sWidth":"100%","sClass":"cptcode", "mDataProp":"cptcode","bSortable":true},
	      			{"sWidth":"100%","sClass":"prscode", "mDataProp":"prscode","bSortable":true},
	      			{"sWidth":"100%","sClass":"prschrg", "mDataProp":"prschrg","bSortable":true},
	      			{"sWidth":"100%","sClass":"hccchrg", "mDataProp":"hccchrg","bSortable":true},
	      			{"sWidth":"100%","sClass":"revclass", "mDataProp":"revclass","bSortable":true},
	      			{"sWidth":"110%","sClass":"editSer", "mDataProp":"effdate","bSortable":true}
	      	    ]
	});

	$(".ColVis_MasterButton span").html("");
	$("#divInDialogTab").html("");
	getSlectedGroups(0);
	 $("#createGroup #cheader").html("");
     $("#createGroup #cheader").append(dialogData);		
     $("#createGroup #headerinfo").removeAttr('style');
     $("#createGroup #headerinfo").attr('style','margin-top:0px;font-size: .8em');
	
	$(".ColVis_MasterButton span").html("");
	 $("#createGroup").css({'height':'550px'});
	$.blockUI({title:'<img style="cursor:auto;width:16px;height:16px;" src="images/cdmr/closeicon.png" onclick="blockUIClose();"/>', theme:true, draggable:true, message: $('#createGroup'), 
		themedCSS: {width: '81%', left:'100px', top:'10px'} }); 

	/*$(".blockUI").resize(function(){
		var heig = $(this).height();
		$("#createGroup").css({'height':heig});
	});
}

function getSlectedGroups(pn, id){

	idholder = id;
	
	groupCode = $("#"+(id)).val();
	
    if(groupCode != ""){
    	$("#groupname").val($("#"+(id) + " option:selected").text());
    
      }
  
	$.ajax({
		url:"getgropus.action",
		type:"post",
		data:{parentNode:pn},
		success:function(response){
			var data = response['groups'];
			//var save= '<td class="editTD"><a id="'+index+'" class="edit" href="#">	<img src="images/cdmr/edit.png"></a></td></tr>';
			if(data.length == 0){
				   
					$("#divInDialog #divInDialogTab tr").each(function(i){
					  
						if(id<=i){
							
							    $(this).remove();
							    --index;
							    
							    }
						});
				
				return;
				}
			
			$("#divInDialog #divInDialogTab tr").each(function(i){
				
				if(id<=i){
				
					    $(this).remove();
					    --index;

					    }
				    
				});

			var leftdata='<tr id="tr'+index+'"><td align="left"><label >L'+(index)+'</label></td>';
			var select = '<td class="dropdown"><select name="groupone'+pn+'" id="'+index+'" class="selectbox" onchange="getSlectedGroups(this.value, this.id);">';
			var option = '<option value="">Select Value</option>';
			for (var i = 0; i < data.length; i++) {
				option += '<option value="'+data[i][0]+'">'+data[i][2]+'</option>';
			}
			var addnew='<td id="td'+index+'"><div><img src ="images/cdmr/add.png" id="'+index+'" class="cursor"  style="width:16;height:16;cursor:pointer;" onclick="addNewGrp(this.id);"/></div></td></tr>';
			select = select + option + '</select></td>';
			var row = leftdata+select+addnew;
			$("#divInDialog #divInDialogTab").append(row);
			index++;
		}
	});


}

function blockUIClose(){
	$.unblockUI(); 
	index=1;
	if(flagForCheck){
		checkdAll.length = 0;
		$('#allcbpro').prop('checked', false);
		$('#allcbtech').prop('checked', false);
		$('tbody tr td input[type="checkbox"]').each(function(){
	        $(this).prop('checked', false);
	        if($.inArray($(this).val(), checkdAll) != -1){
	        	checkdAll.splice($.inArray($(this).val(), checkdAll), 1);
	        	
	    		return;
	        }
	    });
	}
	


	
	//$("#crtsergrplevel").val("Create More Level");
	//$("#grplevelname").css('display','none');
}

function addNewGrp(id){

	$( '#divInDialog #divInDialogTab tr').each(function(){
		var id = $(this).find('td:last').attr('id').replace("td", "");
		$(this).find('td:last').html('<div><img src ="images/cdmr/add.png" id="'+id+'" class="cursor"  style="width:16;height:16;cursor:pointer;" onclick="addNewGrp(this.id);"/></div>');
	});
	
	var tdcol="td"+id;
	var td='<div><input type="text" id="te'+id+'" style="width:55%;float:left" value=""/><span><a href="#" id="'+id+'" onclick="createNewGrup(this.id);"><img src="images/cdmr/save.png"/></a><a href="#" id="'+id+'" onclick="closeEditing(this.id);"><img src="images/cdmr/can.png"/></a></span></div>';
	$("#divInDialog #divInDialogTab #"+tdcol).html("");
	$("#divInDialog #divInDialogTab #"+tdcol).append(td);
}

function closeEditing(id){
	var tdcol="td"+id;
	$("#divInDialog #divInDialogTab #"+tdcol).html('<div><img src ="images/cdmr/add.png" id="'+id+'" class="cursor"  style="width:16;height:16;cursor:pointer;" onclick="addNewGrp(this.id);"/></div>');
}

function createNewGrup(id){
	var parentid;
	 var grupName=$("#te"+id).val();
	 var v = $("#" + (id-1)).val();
	 if(v == "" || v == 'undefine' || v == null || v == " "){
		parentid = 0;
	 }else{
		 parentid = v;
	 }
	 
	 if(grupName == ""){
		 alertify.alert("Please Enter Service Section.");
		return;
	 }
 
    $.ajax({
		url:"selectedgrupadd.action",
		type:"post",
		data:{grpName:grupName,flag:true, parentId:parentid,grpLev:id},
	 		success:function(response){
			$.ajax({
				url:"getgropus.action",
				type:"post",
				data:{parentNode:parentid},
				success:function(response){
					var data = response['groups'];
					var option = '<option value="">Select Value</option>';
					
					for (var i = 0; i < data.length; i++) {
						if(data[i][2] == grupName){
							option += '<option value="'+data[i][0]+'" selected>'+data[i][2]+'</option>';
							groupCode = data[i][0];
						}else{
							option += '<option value="'+data[i][0]+'">'+data[i][2]+'</option>';
						}
					}
					$("#"+id).html(option);
					$("#divInDialog #divInDialogTab tr").each(function(j){
						if(j >= id){
							$(this).remove();
							--index;
						}
						
					});
				}
			});
			alertify.alert("Added Successfully.");
			
		}
   });
   var tdcol="td"+id;
	var td='<div><img src ="images/cdmr/add.png" id="'+id+'" class="cursor"  style="width:16;height:16;cursor:pointer;" onclick="addNewGrp(this.id);"/></div>';
   
	//$("#divInDialog #divInDialogTab #"').append(td);
	$("#divInDialog #divInDialogTab #"+tdcol).html("");
	$("#divInDialog #divInDialogTab #"+tdcol).append(td);
}

function closePopupWindow(){
	
	try{
		index = 1;
		$.unblockUI();	
		if(flagForCheck){
			checkdAll.length = 0;
			$('#allcbpro').prop('checked', false);
			$('#allcbtech').prop('checked', false);
			
			$('tbody tr td input[type="checkbox"]').each(function(){
		        $(this).prop('checked', false);
		        if($.inArray($(this).val(), checkdAll) != -1){
		        	checkdAll.splice($.inArray($(this).val(), checkdAll), 1);
		        	
		    		return;
		        }
		    });
		}
		}catch(err){
		alert("err" + err);
	}
}

function submitInStagingComprac(){
	 var arrayStr = "";
	 
		
    if(checkdAll.length != 0){
    	
    	checkdAll = checkdAll.filter(function(n){ return n != undefined });
    	checkdAll = checkdAll.filter(function(n){ return n != 'on' });
    	
    	
				for(var i=0; i<checkdAll.length; i++){
					arrayStr += checkdAll[i] + ",";
	        }
 }	

    if($("#"+(idholder)).val()==""){
    	grupCode=$("#"+(idholder-1)).val();
        }
    else{
    	 grupCode=$("#"+(idholder)).val();
        }
    if(grupCode==""||grupCode==undefined){
    	alertify.alert("Please Select Service Section.");
          return;
        }
    

	$.ajax({
		url:"updateGrupselected.action",
		type:"post",
		data:{pkString:arrayStr,grupCode:groupCode,fileType:fileType},
		success:function(response){
			alertify.alert("Service Section Attached Successfully.");
			groupCode="";
			if(fileType == "EIWTD"){
				drawTableForTechnicalData();
				drawtabletech();
			}else if(fileType == "EIWPD"){
				drawTableForProfessionalData();
				drawtablePro();
			}
		}
  });

	flagForCheck = true;
}

function createMoreGroup(){
    if($("#"+(index-1)).val() == ""){
    	alertify.alert("Please Select Service Section.");
        return;
    }
    var pn=$("#"+(index-1)).val();
   
    var leftdata='<tr id="tr'+index+'"><td align="left"><label >L'+(index)+'</label></td>';
	var select = '<td class="dropdown"><select name="groupone'+pn+'" id="'+index+'" class="selectbox" onchange="getSlectedGroups(this.value, this.id);">';
	var option = '<option value="">Select Value</option>';
	
	var addnew='<td id="td'+index+'"><div><input type="text" id="te'+index+'" style="width:55%;float:left" value=""/><span><a href="#" id="'+index+'" onclick="createNewGrup(this.id);"><img src="images/cdmr/save.png"/></a><a href="#" id="'+index+'" onclick="removelastlevel(this.id);"><img src="images/cdmr/can.png"/></a></span></div></td></tr>';
	select = select + option + '</select></td>';
	var row = leftdata+select+addnew;
	$("#divInDialog #divInDialogTab").append(row);
	index++;
   
}

function removelastlevel(){

	$( '#divInDialog #divInDialogTab').find('tr:last').remove();
	index--;

}
function removeonemorlevel(){
	groupCode=$("#"+(index-1)).val();
	if(groupCode == ""){
		alertify.alert("Please Select Service Section.");
		return;
	}

	var r = alertify.confirm("Do you want to remove the last level?", function(r){
			if(r){
				$.ajax({
					url:"selectedgrupdeletefromcomprec.action",
					type:"post",
					data:{pkID:groupCode},
					success:function(response){
						if(response.checkData == true){
							alertify.alert("Service Section Deleted Successfully.");
							removelastlevel();
						}else{
							alertify.alert("The service section was not deleted as it is attached to some Items in Staging.");
						}
					}
			  });
			return;
			}else{
			}

		});


	
	/*$.ajax({
	url:"selectedgrupdelete.action",
	type:"post",
	data:{pkID:groupCode},
	success:function(response){
		refreshTable(index-1);
		alert("Group Removed Successfully.");
		removelastlevel();
	}
});
}

function refreshTable(id){

	
	var parentid;
	 var grupName=$("#te"+id).val();
	 var v = $("#" + (id-1)).val();
	 if(v == "" || v == 'undefine' || v == null || v == " "){
		parentid = 0;
	 }else{
		 parentid = v;
	 }
	 
	$.ajax({
		url:"getgropus.action",
		type:"post",
		data:{parentNode:parentid},
		success:function(response){
			var data = response['groups'];
			var option = '<option value="">Select Value</option>';
			
			if(data.length > 0){
				for (var i = 0; i < data.length; i++) {
					option += '<option value="'+data[i][0]+'">'+data[i][2]+'</option>';
				}
			}else{
				removelastlevel();
			}
			
			$("#"+id).html(option);
		}
	});
}
*/
$(document).ready(function(){
	$(".ColVis_Button TableTools_Button span").html('<input type="buutton" value="Show/Hide Button"/>');
	
	
});

--></script>
<style>

.selectbox{
	width:100px;
}

.dataTables_scrollBody{
	/*height: -1% !important;*/
}

.dataTables_info{
	width: 20% !important;
}

/*.DataTables_sort_wrapper{
	height: 25px;
	padding-top: 10px;
}.dataTables_length{
position: absolute;
top:420px;
width: 155px;

}
*/

.DataTables_sort_wrapper{font-weight: bold;}



.TableTools{
width: 105px;
padding-left:10px;
float: left;
}

.tools{
	margin-top: 15px;
}

#addnotes{
	position: absolute;
	right: 220px;
}

#grouping{
	position: absolute;
	right: 390px;
}

.ColVis_MasterButton{
	width: 150%;
	
}

.ColVis_MasterButton span{
	margin-left:15px;
}
table{

    table-layout: fixed;
    font-size: 12px;
    width: 100%;
 
}
.dataTables_length table{
	width:  auto !important;
}


tbody{
  table-layout: fixed;
  }
  
 thead{
   table-layout: fixed;
 }


#fileinfo{
	margin-top:0px;
	text-align: center;
}

#headerinfo{
	width: auto;
	height: 40px;
}

.checkall{
	width: 20px;
	text-align: center;
}
.instcode{
	width: 60px;
	text-align: center;
}

.editTD{
	width: 40px;
	text-align: center;
}

.notesTD{
	width: 40px;
	text-align: center;
}
.servcode{
	width: 90px;
	text-align: center;
}

.desc{
	width: 150px;
	text-align: left;
}



.cptcode{
	width: 60px;
	text-align: center;
}
.prscode{
width: 60px;
	text-align: center;
}

.prschrg{
width: 60px;
	text-align: right;
}

.hccchrg{
width: 60px;
	text-align: right;
}

.revclass{
width: 60px;
	text-align: center;
}

.editable{
	width: 90px;
	text-align: center;
}
.editSer{
	width: 100px;
	text-align: center;
}

.staging_button{
	-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf) );
	background:-moz-linear-gradient( center top, #ededed 5%, #dfdfdf 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf');
	background-color:#ededed;
	-webkit-border-top-left-radius:5px;
	-moz-border-radius-topleft:5px;
	border-top-left-radius:5px;
	-webkit-border-top-right-radius:5px;
	-moz-border-radius-topright:5px;
	border-top-right-radius:5px;
	-webkit-border-bottom-right-radius:5px;
	-moz-border-radius-bottomright:5px;
	border-bottom-right-radius:5px;
	-webkit-border-bottom-left-radius:5px;
	-moz-border-radius-bottomleft:5px;
	border-bottom-left-radius:5px;
	text-indent:0;
	border:1px solid #dcdcdc;
	display:inline-block;
	color:#777777;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	font-style:normal;
	height:25px;
	line-height:23px;
	text-decoration:none;
	text-align:center;
	text-shadow:1px 1px 0px #ffffff;
}

.staging_button:hover{
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #dfdfdf), color-stop(1, #ededed) );
	background:-moz-linear-gradient( center top, #dfdfdf 5%, #ededed 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#dfdfdf', endColorstr='#ededed');
	background-color:#dfdfdf;
}

 .textbox { 
    border: 1px solid #848484; 
    -webkit-border-radius: 5px; 
    -moz-border-radius: 5px; 
    border-radius: 5px; 
    outline:0; 
    height:25px; 
    width: 150px; 
    padding-left:10px; 
    padding-right:10px; 
  } 
  
   .select-style {
    border: 1px solid #ccc;
    width: 120px;
    border-radius: 3px;
    overflow: hidden;
    background: #fafafa url("img/icon-select.png") no-repeat 90% 50%;
}

.select-style select {
    padding: 5px 8px;
    width: 130%;
    border: none;
    box-shadow: none;
    background: transparent;
    background-image: none;

}

.select-style select:focus {
    outline: none;
}

#statusbar{
	/*position: absolute;
	 left: 470px; 
	 margin-top: 35px;
	 */
	  
	  height: 23px;
	  width:100px;
}

.paging_full_numbers{
	height: 40px;
	width: 25% !important;
}


.dropdown p {
	display: inline-block;
	font-weight: bold;
}

#statusbar {

      border: 1px solid !important;  /*Removes border*/
     
      -moz-appearance: none; /* Removes Default Firefox style*/
      background-position: 82px 7px;  /*Position of the background-image*/
      width: 100px; /*Width of select dropdown to give space for arrow image*/
      

      /*My custom style for fonts*/
		width:140px;
		height: 25px;
      color: #1455a2;
      margin-left: :35px;
}

.selector_1 select {

      border: 1px solid !important;  /*Removes border*/
    /*Removes default chrome and safari style*/
      -moz-appearance: none; /* Removes Default Firefox style*/
      background-position: 82px 7px;  /*Position of the background-image*/
      /*Width of select dropdown to give space for arrow image*/
      

      /*My custom style for fonts*/
		width:50px;
		height: 23px;
      color: #1455a2;
      margin-left: :35px;
}

input#effdatepicker {
   background: white;
    border: 1px solid #DDD;
    border-radius: 5px;
    box-shadow: 0 0 5px #DDD inset;
    color: #666;
    float: right;
    padding: 5px 10px;
    width: 160px;
    outline: none;

}

#pendingDonorsTableTech td{
	height:30px;
	padding-left: 1%;
}

#pendingDonorsTablepro td{
	height: 40px;
	padding-left: 2%;
}


.prostagingroupdata td{
	padding-left: 2%;
}

.techstagingroupdata  td{
	padding-left: 1%;
}
.dataTables_filter{
	margin-top: -30px;
	margin-bottom: -10px;
}

table.gridtable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}

.ui-widget button{
	font-size: 12px;
}

div.ColVis_collection button.ColVis_Button{
	height: 30px;
	width: 200px;
}

.priceCode{
	text-align: right;
}

.dataTables_scrollHead{
	width: 100% !important;
}

.buttonIcon{
	ut.button-add {
    background-image: url(images/cdmr/notes.png); /* 16px x 16px */
    background-color: transparent; /* make the button transparent */
    background-repeat: no-repeat;  /* make the background image appear only once */
    background-position: 0px 0px;  /* equivalent to 'top left' */
}
.dataTables_info{
	width: 20% !important;
}

td {
  word-wrap: break-word;
}
.paging_full_numbers .ui-button{
	padding: 1%;
	width: 30px;
}

.dataTables_filter input[type="text"]{
	/*padding: 1px 22px 1px 10px;*/
	padding: 2px 22px 0px 10px;
	height: 28px;
}
</style>
 
 