<%@ page contentType="text/html; charset=UTF-8"%>

<link rel="stylesheet" type="text/css" href="css/ui/base/jquery.ui.tabs.css" media="screen" />
    <script type="text/javascript" src="js/jquery/graph/jquery.jqplot.min.js"></script>
    <script type="text/javascript" src="js/jquery/graph/shCore.min.js"></script>
    <script type="text/javascript" src="js/jquery/graph/shBrushJScript.min.js"></script>
    <script type="text/javascript" src="js/jquery/graph/shBrushXml.min.js"></script>
   <script class="include" type="text/javascript" src="js/jquery/graph/jqplot.pieRenderer.js"></script>
   <script type="text/javascript" src="js/jquery/graph/jqplot.cursor.min.js"></script>

  <link type="text/css" href="css/jquery.jqplot.css" rel="stylesheet" />
	<script>
	var graph1,graph2,graph3,graph4;
	
	
	
</script>

  
<!-- graph -->


			<div id="accordion" > 
				  <div id="tabs">
				    <ul >
				     <li><a href="#tabs-1">Product Source</a></li>
				      <li><a href="#tabs-2">Processing Protocol</a></li>
				      <li><a href="#tabs-3">Product Type</a></li>
				      <li><a href="#tabs-4">Sterility Results</a></li>
				   <!--    <li><a href="#tabs-5">Accession Date</a></li>
				   -->
				    </ul>
				    <div id="tabs-1">
				    	<div id="chart1" style="margin-top:20px; margin-left:20px; height:300px;width:600px;"></div>
				    </div>
				    
				    <div id="tabs-2">
				    	<div id="chart2" style="margin-top:20px; margin-left:20px; height:300px;width:600px;"></div>
				    </div>
				    
				    <div id="tabs-3">
				      	<div id="chart3" style="margin-top:20px; margin-left:20px; height:300px;width:600px;"></div>
				    </div>
				    
				    <div id="tabs-4">
				      	<div id="chart4" style="margin-top:20px; margin-left:20px; height:300px;width:600px;"></div>
				    </div>
				    
				<!--     <div id="tabs-5">
				      	<div id="chart5" style="margin-top:20px; margin-left:20px; height:300px;width:600px;"></div>
				    </div>
				    -->
				  </div> 
		</div>

<!--  end of graph -->
 <script>
 

$( "#tabs" ).tabs();
$("#tabs ul").removeClass("ui-widget-header");   
	//alert("g1 " +$("#graph1").val());

$('#tabs').bind('tabsshow', function(event, ui) {
	//alert("show" + ui.index + " / " + plot1._drawCount + " / " + plot2._drawCount);
    if (ui.index == 0 && plot1._drawCount == 0) {
    	//alert("show" + ui.index);
      plot1.replot();
    }else if (ui.index === 1 && plot2._drawCount === 0) {
    	//alert("show" + ui.index);
        plot2.replot();
    }else if (ui.index === 2 && plot3._drawCount === 0) {
    	//alert("show" + ui.index);
        plot3.replot();
    }else if (ui.index === 3 && plot4._drawCount === 0) {
    	//alert("show" + ui.index);
        plot4.replot();
    }
   
  });
/*
$(function() {

	alert("show graph 9");	
	
	try{
	var searchUrl = "http://192.168.192.234:8180/TenseServerBMT/CTL/select";
	var storeString = "facet=true&q=*:*&rows=0&facet.field=PRODUCT_SOURCE";
	//?facet=true&q=*%3A*&rows=0&wt=json&facet.field=PRODUCT_SOURCE&facet.field=PROCESSING_PROTOCOL&facet.field=PRODUCT_TYPE&facet.field=STERILITY";
	graph1=new Array(); 
	$.ajax({
			  url: searchUrl + '?' + storeString + '&wt=json&json.wrf=?',
			  dataType: 'json',
			  success: function(data){
				   $(data.facet_counts.facet_fields).each(function(){
					 // alert("this " + this);
					  //alert( "PRODUCT_SOURCE"  + this.PRODUCT_SOURCE);
					  var tmpArr=new Array();
					  $(this.PRODUCT_SOURCE).each(function (counter,val){
					//	 alert("counter " + counter + " /val " + val ) ;
						 tmpCount = counter%2;
					//	 alert("tmpCount" + tmpCount);
						 if( tmpCount==0){
							 tmpArr.push(val);
						 }else {
							tmpArr.push(val);
						 	graph1.push(tmpArr);
						 	tmpArr=new Array();
						 }
					  });
				  });
				   var l1 = [['age < 50',7], ['age > 50', 26]];
					 var plot1 = $.jqplot('chart1', [l1], {
					       title: "Product Source",
					       height: 300,
					       width: 400,
					       series:[{renderer:$.jqplot.PieRenderer, 
					       rendererOptions: {
								 // Put data labels on the pie slices.
							 // By default, labels show the percentage of the slice.
							  showDataLabels: true}
							  }],
					       legend:{show:true}
					     });
				  
		 	  }, 
		 	 error: function(jqXHR, textStatus, errorThrown){
		 		 var responseTxt = jqXHR.responseText;
		 		alert("errorThrown " + errorThrown);
		 	
		 	  }
			});
	 
	
//alert("trace");	
		 
	//showGraph1();	   
	}catch(err){
		alert("error in get data " + err);
	} 
	  alert("end of graph");
}
);
*/ 
$.jqplot.config.enablePlugins = false;
var graph1,graph2,graph3,graph4;
var plot1;
var plot2,plot3,plot4;
$(function() {
	
	try{
	var searchUrl = "http://localhost:8180/TenseServerBMT/CTL/select";
	var storeString = "facet=true&q=*:*&rows=0&facet.field=PRODUCT_SOURCE&facet.field=PROCESSING_PROTOCOL&facet.field=PRODUCT_TYPE&facet.field=STERILITY";
	//?facet=true&q=*%3A*&rows=0&wt=json&facet.field=PRODUCT_SOURCE&facet.field=PROCESSING_PROTOCOL&facet.field=PRODUCT_TYPE&facet.field=STERILITY";
	graph1=new Array(); 
	graph2=new Array(); 
	graph3=new Array(); 
	graph4=new Array(); 
	$.ajax({
			  url: searchUrl + '?' + storeString + '&wt=json&json.wrf=?',
			  dataType: 'json',
			  success: function(data){
				 //  $(data.facet_counts.facet_fields).each(function(){
					 // alert("this " + this);
					  //alert( "PRODUCT_SOURCE"  + this.PRODUCT_SOURCE);
					  var tmpArr=new Array();
					  $(data.facet_counts.facet_fields.PRODUCT_SOURCE).each(function (counter,val){
					//	 alert("counter " + counter + " /val " + val ) ;
						 tmpCount = counter%2;
					//	 alert("tmpCount" + tmpCount);
						 if( tmpCount==0){
							 tmpArr.push(val);
						 }else {
							tmpArr.push(val);
						 	graph1.push(tmpArr);
						 	tmpArr=new Array();
						 }
					  });
					//  alert(" graph 1--> " + graph1);
					  
					 plot1 = $.jqplot('chart1', [graph1], {
					      title: "Product Source",
					      height: 300,
					      width: 400,
					      series:[{renderer:$.jqplot.PieRenderer, 
					      rendererOptions: {
								 // Put data labels on the pie slices.
							 // By default, labels show the percentage of the slice.
							  showDataLabels: true}
							  }],
					      legend:{show:true}
					    });
					 
					  tmpArr=new Array();
					//  alert("$(data.facet_counts.facet_fields.PROCESSING_PROTOCOL) " + $(data.facet_counts.facet_fields.PROCESSING_PROTOCOL));
					  $(data.facet_counts.facet_fields.PROCESSING_PROTOCOL).each(function (counter,val){
						//	 alert("counter " + counter + " /val " + val ) ;
						 tmpCount = counter%2;
						 if( tmpCount==0){
							 tmpArr.push(val);
						 }else {
							tmpArr.push(val);
						 	graph2.push(tmpArr);
						 	tmpArr=new Array();
						 }
					  });
					  
					//  alert(" graph2 " + graph2);
						plot2 = $.jqplot('chart2', [graph2], {
					      title: "Processing Protocol",
					      height: 300,
					      width: 400,
					      series:[{renderer:$.jqplot.PieRenderer, 
					      rendererOptions: {
								 // Put data labels on the pie slices.
							 // By default, labels show the percentage of the slice.
							  showDataLabels: true}
							  }],
					      legend:{show:true}
					    });  
						
					  tmpArr=new Array();
					  $(data.facet_counts.facet_fields.PRODUCT_TYPE).each(function (counter,val){
						 tmpCount = counter%2;
						 if( tmpCount==0){
							 tmpArr.push(val);
						 }else {
							tmpArr.push(val);
						 	graph3.push(tmpArr);
						 	tmpArr=new Array();
						 }
					  });
					  plot3 = $.jqplot('chart3', [graph3], {
						    title: "Product Type",
						    height: 300,
						    width: 400,
						    series:[{renderer:$.jqplot.PieRenderer, 
						    rendererOptions: {
									 // Put data labels on the pie slices.
								 // By default, labels show the percentage of the slice.
								  showDataLabels: true}
								  }],
						    legend:{show:true}
						  }); 
					  
					  tmpArr=new Array();
					  $(data.facet_counts.facet_fields.STERILITY).each(function (counter,val){
						 tmpCount = counter%2;
						 if( tmpCount==0){
							 tmpArr.push(val);
						 }else {
							tmpArr.push(val);
						 	graph4.push(tmpArr);
						 	tmpArr=new Array();
						 }
					  });
					  plot4 = $.jqplot('chart4', [graph4], {
						    title: "Sterility Results",
						    height: 300,
						    width: 400,
						    series:[{renderer:$.jqplot.PieRenderer, 
						    rendererOptions: {
									 // Put data labels on the pie slices.
								 // By default, labels show the percentage of the slice.
								  showDataLabels: true}
								  }],
						    legend:{show:true}
						  });  
				
				  // });
				// $("#graph1").val(graph1);
				//   alert(" graphdata  " + graph1 + " / " + graph2 + " / " + graph3 + " / " + graph4); 
		 	  }, 
		 	 error: function(jqXHR, textStatus, errorThrown){
		 		 var responseTxt = jqXHR.responseText;
		 		alert("errorThrown " + errorThrown);
		 	
		 	  }
			});
	 
	
//alert("trace");	
		 
	//showGraph1();	   
	}catch(err){
		alert("error in get data " + err);
	} 
	  //alert("end of graph");
});



//alert("graph data-->  " + graph1 );

//var l1 = [['age < 50',7], ['age > 50', 26]];
//var l1 = graph1;

//var l2 = [['age < 50',70], ['age > 50', 30]];


</script>
