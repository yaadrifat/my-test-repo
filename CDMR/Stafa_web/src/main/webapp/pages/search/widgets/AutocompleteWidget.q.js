(function ($) {
AjaxSolr.AutocompleteWidget = AjaxSolr.AbstractTextWidget.extend({
  afterRequest: function () {
    $(this.target).find('input').val('');

    var self = this;

    var callback = function (response) {
      var list = [];
      /*for (var i = 0; i < self.fields.length; i++) {
        var field = self.fields[i];
        for (var facet in response.facet_counts.facet_fields[field]) {
          list.push({
            field: field,
            value: facet,
            text: facet + ' (' + response.facet_counts.facet_fields[field][facet] + ') - ' + field
          });
        }
      }*/
      for (var i = 0; i < self.fields.length; i++) {
		var fieldIdAndName = self.fields[i];
		var fieldName ="";
		var fieldArraySingleValue = fieldIdAndName.split(":");
        //var field = self.fields[i];
        var field = fieldArraySingleValue[0];
		if( fieldArraySingleValue.length > 1)
			fieldName = fieldArraySingleValue[1];
		else
			fieldName = field;
		fieldName = fieldName.replace(/_/g, ' ');
        for (var facet in response.facet_counts.facet_fields[field]) {
          list.push({
            field: field,
            value: facet,
            //text: facet + ' (' + response.facet_counts.facet_fields[field][facet] + ') - ' + field
            text: facet + ' (' + response.facet_counts.facet_fields[field][facet] + ') - ' + fieldName 
          });
        }
      }
      self.requestSent = false;
      $(self.target).find('input').unautocomplete().autocomplete(list, {
        formatItem: function(facet) {
          return facet.text;
        }
      }).result(function(e, facet) {
        self.requestSent = true;
        if (self.manager.store.addByValue('fq', facet.field + ':' + facet.value)) {
          self.manager.doRequest(0);
        }
      }).bind('keydown', function(e) {
        if (self.requestSent === false && e.which == 13) {
          var value = $(this).val();
          if (value && self.set(value)) {
            self.manager.doRequest(0);
          }
        }
      });
    } // end callback
// changed from -1 to 200 by Sabeer
    var params = [ 'q=*:*&facet=true&facet.limit=200&facet.method=fc&facet.mincount=1&json.nl=map' ];
    for (var i = 0; i < this.fields.length; i++) {
      params.push('facet.field=' + this.fields[i]);
    }
    jQuery.getJSON(this.manager.solrUrl + '?' + params.join('&') + '&wt=json&json.wrf=?', {}, callback);
  }
});

})(jQuery);
