(function ($) {

	AjaxSolr.MenuListWidget = AjaxSolr.AbstractWidget.extend({
	    beforeRequest: function () {
	 
	        var currentPosition = 0;
	        var self = this;
	        var menuItems=new Array();
	        var divIds=new Array();
	        for(var i=0;i<self.fieldsNames.length;i++) {
	            menuItems[i]=self.fieldsNames[i];
	            divIds[i]=self.divnames[i];
	        }
	     
	        var menuLength=menuItems.length;
	         $(self.target).empty();
	       
	         if(menuLength > 8){
	             $(self.target).append('<li><span id="menuScrollup" style="text-align: center; cursor: pointer;"></span></li>');
	             for (var i = 0; i < menuItems.length; i++) {
	                 var fieldName = self.fieldsNames[i];
	                 
	                 
	                 var divName=self.divnames[i];
	                 var fname = fieldName;
	                 fname = fname.replace(/(<br\/)>/g,'');
	                 if(fieldName.length>=15) {
	                     var shortFieldName=fname.slice(0,15);
	                     shortFieldName += "...";
	                     $(self.target).append('<li><a href="#" class="listSelection menuHighLight" id=menuLink'+i+' style="display:block;" title="'+fieldName+'">'+shortFieldName+'</a><ul><div class="divClass" id="'+divName+'"></div></ul> </li>');
	                 } else {
	                     $(self.target).append('<li><a href="#" class="listSelection" id=menuLink'+i+' style="display:block;">'+fieldName+'</a><ul><div class="divClass" id="'+divName+'"></div></ul> </li>');
	                 }
	             
	             }
	             for(var i=8;i<menuItems.length;i++) {
	                 $('#menuLink'+i).css('display','none');
	             }
	             $(self.target).append('<li><span id="menuScrolldown" style="text-align: center; cursor: pointer"></span></li><input type="hidden" id="facetLength" name='+menuLength+'>');
	             
	             $('#menuScrollup').css("background-color", "#A9A9A9" );
	             
	             $('.listSelection').mouseenter(function(){
	                 $('#query').val("");
	                 $('#query').blur();
	                 $('#topsearchResult').css('display','none');
	                 $('.listSelection').css("background-color", "");
	                 $(this).css("background-color", "#a1d0ef");
	            });
	           
	             $(document).ready(function(){

	                    $('.menuHighLight').tooltip({
	                        track: true,
	                        delay: 0,
	                        showURL: false
	                    });

	             });
	            
	           $('#menuScrollup').mouseenter(function(){ 
	                var timer = 0;
	                timer = setInterval(function(){
	                  for(var i=0; i<menuItems.length;i++){
	                        if($('#menuLink'+i).css('display') == "block"){                     
	                            currentPosition = i-1;
	                            i=menuItems.length+1;
	                        }
	                    }
	                    if(currentPosition>=0){
	                        $('#menuScrollup').css("background-color", "" );
	                        $('#menuScrolldown').css("background-color", "" );
	                        var adjustmentLength=currentPosition;
	                        for(var i=0; i<8; i++,adjustmentLength++){
	                             $('#menuLink'+adjustmentLength).css('display','block');               
	                         }
	                        $('#menuLink'+(adjustmentLength)).css('display','none');
	                    }
	                    else{
	                        $('#menuScrollup').css("background-color", "#A9A9A9" );
	                    }
	                 }, 100);
	                $('#menuScrollup').mouseleave(function(){
	                    clearInterval(timer);
	                    /*$('#menuScrollup').css("display","block");
	                    return false;*/
	                });
	                 
	            });
	             
	            $('#menuScrolldown').mouseenter(function(){
	                var timer = 0;
	                timer = setInterval(function(){
	                    for(var i=0; i<menuItems.length;i++){
	                        if($('#menuLink'+i).css('display') == "block"){                     
	                            currentPosition = i+1;
	                            i=menuItems.length+1;
	                        }
	                    }
	                    if((currentPosition+7)!=menuItems.length){
	                        $('#menuScrolldown').css("background-color", "" );
	                        $('#menuScrollup').css("background-color", "" );
	                        $('#menuLink'+(currentPosition-1)).css('display','none');
	                        var adjustmentLength=currentPosition;
	                        for(var i=0; i<8; i++,adjustmentLength++){
	                             $('#menuLink'+adjustmentLength).css('display','block');
	                       
	                         }
	                    }
	                    else{
	                        $('#menuScrolldown').css("background-color", "#A9A9A9" );
	                    }
	                  
	                 }, 100);
	                $('#menuScrolldown').mouseleave(function(){
	                    clearInterval(timer);
	                   /* $('#menuScrolldown').css("display","block");
	                    return false;*/
	                });
	             
	            });
	         }
	         else{
	             for (var i = 0; i < self.fieldsNames.length; i++) {
	                  var fieldName = self.fieldsNames[i];
	                  var divName=self.divnames[i];
	                  $(self.target).append('<li><a href="#" class="listSelection" >'+fieldName+'</a><ul><div id="'+divName+'"></div></ul> </li>');
	              }
	         }
	         
	    }
	  });
	})(jQuery);