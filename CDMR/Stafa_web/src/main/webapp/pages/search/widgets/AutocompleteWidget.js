(function ($) {

AjaxSolr.AutocompleteWidget = AjaxSolr.AbstractFacetWidget.extend({
  afterRequest: function () {
	
    $(this.target).find('input').val('');
	//$(this.target).find('input').unbind().removeData('events').val('');

	//alert("afterRequest");
    var self = this;
	//this.manager.solrUrl = this.manager.solrUrl;
    var callback = function (response) {
      var list = [];
	  //alert("callback");
	  //this.manager.solrUrl = this.manager.solrUrl;

      for (var i = 0; i < self.fields.length; i++) {
		var fieldName = self.fields[i];
		var field = self.fields[i];
		/*
			var fieldIdAndName = self.fields[i];
			var fieldArraySingleValue = fieldIdAndName.split(":");
			//var field = self.fields[i];
			var field = fieldArraySingleValue[0];
		if( fieldArraySingleValue.length > 1)
			fieldName = fieldArraySingleValue[1];
		else
			fieldName = field;*/
			
		fieldName = fieldName.replace(/_/g, ' ');
        for (var facet in response.facet_counts.facet_fields[field]) {
          list.push({
            field: field,
            value: facet,
            //text: facet + ' (' + response.facet_counts.facet_fields[field][facet] + ') - ' + field
            text: facet + ' (' + response.facet_counts.facet_fields[field][facet] + ') - ' + fieldName 
          });
        }
      }
	//	alert(list);
      self.requestSent = false;
      $(self.target).find('input').unautocomplete().autocomplete(list, {
      //$(self.target).find('input').autocomplete(list, {
        formatItem: function(facet) { //alert("1"+facet.text);

          return facet.text;
        }
      }).result(function(e, facet) {
        self.requestSent = true;
		//	  alert("result");

        if (self.manager.store.addByValue('fq', facet.field + ':' + '\"'+facet.value+'\"')) {
          self.manager.doRequest(0);
          //self.manager.doRequest(0,"core1/select");

        }
      //}).bind('keydown', function(e) {
	});
	  $(self.target).find('input').bind('keydown', function(e) {

        if (self.requestSent === false && e.which == 13) {
          var value = $(this).val();
		  	  //alert("keydown");

          if (value /*&& self.add(value)*/) { /*alert(value)*/;
		  //Added By Sabeer
			self.manager.store.addByValue('q', value);
			self.manager.store.addByValue('hl.fl', '*');
		    self.manager.store.addByValue("hl.simple.pre", "<b>"); // is the key field
		    self.manager.store.addByValue("hl.simple.post", "</b>"); // is the key field
		    //self.manager.store.addByValue("hl.simple.pre", "<b style='color:black;background-color:#ffff66;'>"); // is the key field
		    //self.manager.store.addByValue("hl.simple.post", "</b>"); // is the key field

			self.manager.doRequest(0);
            //self.manager.doRequest(0,"core1/select");
          }
        }
      });
  	$(self.target).find('img').bind('click', function (evt) {
		    	var queryVal = $("input[id='query']").val();
		        if(queryVal!=""){
		        	self.manager.store.addByValue('q', queryVal);
					self.manager.store.addByValue('hl.fl', '*');
				    self.manager.store.addByValue("hl.simple.pre", "<b>"); 
				    self.manager.store.addByValue("hl.simple.post", "</b>"); 
					self.manager.doRequest(0);
		        }
		  });
    } // end callback

    //var params = [ 'q=*:*&facet=true&facet.limit=-1&facet.mincount=1&json.nl=map&hl=on&hl.fl=*' ];
    var params = [ 'facet=true&facet.limit=-1&facet.mincount=1&json.nl=map&hl=on&hl.fl=*' ];
    //var params = [ 'facet=true&facet.limit=-1&facet.mincount=1&json.nl=map&hl=on' ];
    for (var i = 0; i < this.fields.length; i++) {
      params.push('facet.field=' + this.fields[i]);
    }
	
	for(var i in orArray){
	  	  var orArrval = decodeURIComponent(orArray[i]);
	  	  $("input[name='"+orArrval+"']#radio_or").prop("checked",true);
	}
  	for(var i in qArray){
	  	  var qArrayVal = qArray[i];
	  	  $("input[name='"+qArrayVal+"']#radio_or").prop("checked",true);
	}

	
	var fqValue = (self.manager.store.valuesAsSolrParam("fq"));
	//alert(fqValue);
	if( fqValue != null && fqValue != '')
		params.push('fq=' + fqValue);
    //alert("Auto completeWidget -> param " + params.join('&'));
	//alert("AutoComplete::::"+this.manager.solrUrl);
	var qValue = (self.manager.store.valuesAsSolrParam("q"));
	if( qValue != null && qValue != '')
		params.push('q=' + qValue);
	else
		params.push('q=*:*');
	//alert(qValue);
    jQuery.getJSON(this.manager.solrUrl + '?' + params.join('&') + '&wt=json&json.wrf=?', {}, callback);
	//alert(params.join('&'));

  }
});

})(jQuery);
