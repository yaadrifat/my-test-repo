var manthisobj;
(function($){
	AjaxSolr.ResultWidget = AjaxSolr.AbstractWidget.extend({
		beforeRequest: function(){
		$(this.target).html($('<img/>').attr('src', 'search/images/ajax-loader.gif'));
	},

	facetLinks: function(facet_field, facet_values){
		var links = [];
		if (facet_values) {
			for (var i = 0, l = facet_values.length; i < l; i++) {
				links.push(AjaxSolr.theme('facet_link', facet_values[i], this.facetHandler(facet_field, facet_values[i])));
			}
		}
		return links;
	},

	facetHandler: function(facet_field, facet_value){
		var self = this;
		return function(){
			self.manager.store.remove('fq');
			self.manager.store.addByValue('fq', facet_field + ':' + facet_value);
			self.manager.doRequest(0);
			return false;
		};
	},

	afterRequest: function(){
		//alert("Resultwidget.js after request")
		$(this.target).empty();
		// Modified VA
		var displayType = displaytypeObj[0].childNodes[0].nodeValue;
		var displayObj = '<thead><tr><th id="thSelectColumn"><ul id="ulSelectColumn"><li><img src="search/images/selectcol.png" alt="select columns" title="select columns" /><ul id="targetall"></ul></li></ul></th>';
		var displayarr = [];
		var styleodd = rstyleObj[1].childNodes[0].nodeValue;
		var styleeven = rstyleObj[2].childNodes[0].nodeValue;
		var styler = styleodd;
		var noOfColumn = Number(noOfColumnObj[0].childNodes[0].nodeValue)+1;
		var noOfColumnToshow = Number(noOfColumnToshowObj[0].childNodes[0].nodeValue);
		var j = noOfColumn - noOfColumnToshow;

		var urlObj = webserviceObj[0].getElementsByTagName("URL");		
		var urlString = urlObj[0].childNodes[0].nodeValue;
		
		//var rg = /patient/g;
		//var match = rg.exec(urlValue);
		
		var columToShowArr=new Array();  
		var k = 0;
		for(var i=noOfColumn;i>noOfColumnToshow+1;i--){
			columToShowArr[k] = i;
			k++;
		}
		//Tabular Format
		if(displayType == tabularType){

			var index = 0;

			for (i=0;i<tagsize;i++)  {
//				alert("Value : " + tagObj[i].attributes[3].nodeValue + " Visibility value : " + tagObj[i].attributes[4].nodeValue);

				for (j=0;j<tagObj[i].attributes.length;j++){
					if(tagObj[i].attributes[j].nodeName == seqTagName){
						index = tagObj[i].attributes[j].nodeValue;
					}
					if(tagObj[i].attributes[j].nodeName  == visibilityTagName){
						if(tagObj[i].attributes[j].nodeValue == visibility_true){
							var fieldname = tagObj[i].attributes[0].nodeValue;
							displayarr[index-1] = '<th id="'+fieldname+'" nowrap class="' + stylee +'" onclick=sortTable("'+fieldname+'")>' + tagObj[i].childNodes[0].nodeValue+'<img id="'+fieldname+'_sort" src="search/images/sort_ascdsc.png" ></th>';
						}else{
							displayarr[index-1]="";
						}
					}
				}

			}
			for (i=0;i<tagsize;i++)  {
				displayObj = displayObj + displayarr[i];
			}
			displayObj += '</tr></thead><tbody>';
			$(this.target).append(displayObj);
			//alert("1:"+$("table[id='docs']").width());
			/*var width = $("table[id='docs']").width()+$("div[id='left']").width();

			var docsWidth = $("table[id='docs']").width();
			alert("table:"+docsWidth);


			//$("div[id='right']").width($("table[id='docs']"));
			//$("body").width(width+20);

			$("div[id='result']").width(docsWidth);
			$("div[id='result_table']").width(docsWidth);

			var rightWidth = $("div[id='right']").width();
			var leftWidth = $("div[id='left']").width();
			alert("left:"+leftWidth);
			alert("rigth:"+rightWidth);
			//$("div[id='right']").width(docsWidth-20);
			//var rightWidth = $("div[id='right']").width();
			//var leftWidth = $("div[id='left']").width();

			$("body").width(width+20);
			$("div[id='wrap']").width(width+20);*/

			// Changing the window width

			var docsWidth = $("table[id='docs']").width();
			if( docsWidth != 'undefined' || docsWidth != 0)
			{
				//alert("table:"+docsWidth);


				//var docsHeight = $("table[id='docs']").height();
				//alert(docsHeight);
				//if( docsHeight > 200)
				//	$("div[id='bottom']").height(docsHeight);
				//	alert($("div[id='bottom']").height());
				var leftWidth = $("div[id='left']").width();
				var width = leftWidth+docsWidth+20;
				// change the width of header
				//width = width+leftWidth;
				//$("body").width(width);
				//	$("div[id='wrap']").width(width);
				//$("div[id='header']").width(width);

				//	$("div[id='right']").width(docsWidth);
				//	$("div[id='result']").width(docsWidth);
				//	$("div[id='navigation']").width(docsWidth);
				//	$("div[id='pager-header']").width(docsWidth);
				//	$("div[id='morediv']").width(docsWidth);
				//	$("div[id='filter-box']").width(docsWidth);
				//	$("div[id='result_table']").width(docsWidth);

				docsWidth = $("table[id='docs']").width();
				//alert("table:"+docsWidth);
				leftWidth = $("div[id='left']").width();
				width = leftWidth+docsWidth+20;
				//	$("body").width(width);

				// Set the width of searchbox
				//	$("div[id='search-box']").width(docsWidth-leftWidth);
				//$("ul[id='search']").width(docsWidth-leftWidth);url(../images/search_field.png) no-repeat 0 0;
				//$("div[id='search-box']").css('background-image', 'url(../images/search_field1.png)');

				//	$("input[id='query']").width(docsWidth-leftWidth-20);
				var imageFile = $("input[id='query']").css("background-image");
				$(imageFile).width(docsWidth-leftWidth-20);
				$("input[id='query']").css("background-image", imageFile);  
				//alert($("input[id='query']").css("background-image"));
			}

			// End of Mod.
			//var highlight = new Array(this.manager.response.highlighting.numFound);
			if(this.manager.response.highlighting )
			{

				if(this.manager.response.highlighting['PK_STUDY_STR']) {
					doc.description =
						this.manager.response.highlighting['PK_STUDY_STR'][0];
					alert(doc.description);
				}
			} 

			/*Object { 186={...}, 745={...}, 723={...}}
		Object { text=[1], STUDY_PHASE=[1]}*/

			$.each(this.manager.response.highlighting, function(i,hitem){

				var rg = /<b>(.*?)<\/b>/g;
				if(hitem.text)
				{
					//alert(hitem.length);
					//alert(eval(hitem[i]))
					var res = new Array();
					var match = rg.exec(hitem.text[0]);
					while(match != null){
						res.push(match[1])
						match = rg.exec(hitem.text[0]);
					}
					// highlight[i]=res[0]
					                    //alert("res[0]"+res[0]);
					for (j=1 ;j<res.length;j++)
					{
						//highlight[i]= highlight[i]+","+res[j];
						//alert("res[j]"+res[j]);
					}
				}
			});

			if (this.manager.response.highlighting && this.manager.response.highlighting)
			{
				//var len = this.manager.response.highlighting.items.length;
				//alert("len == "+len);
				//alert("ccc"+this.manager.response.highlighting['PK_STUDY_STR'])
				//this.manager.response.highlighting.get(doc.id).get('f').get(0)
				// display this.manager.response.highlighting[doc.id]
				//alert (this.manager.response.highlighting.get(doc.id).get('f').get(0) );
			}

			else {
				// display result without highlighting

			}
			
			//To show Record Status/result 
			var docsDivLength; 
			docsDivLength = this.manager.response.response.docs.length;			
			$('#result_status').empty();
			$('#docs').show();
			$('#suggDiv').hide();
			if(docsDivLength == 0){
				$('#result_status').empty();
				$('#result_status').append('<br/><br/><br/><br/><span>No Record(s) Found</span>');
				docsLength = null;
				$('#result_status').show();
				checkSuggestion(this);
			}
			else
			{
				for (var i = 0, l = this.manager.response.response.docs.length; i < l; i++) {
					var doc = this.manager.response.response.docs[i];
					styler = (styler == styleeven) ? styleodd : styleeven;
					/*
	#macro(field $f)
	  #if($response.response.highlighting.get($docId).get($f).get(0))
	    $!response.response.highlighting.get($docId).get($f).get(0)
	  #else
	    #foreach($v in $doc.getFieldValues($f))
	      $v
	    #end
	  #end
	#end  */				//alert(this.manager.response.highlighting.docs);
	
	
	
					/*if (this.manager.response.highlighting &&
	                this.manager.response.highlighting[doc.PK_FORMSLINEAR][this.manager.response.responseHeader.params['hl.fl']]) {
	                    // display this.manager.response.highlighting[doc.id]
	                    alert ([this.manager.response.responseHeader.params['hl.fl']]);
	                }
	
	                else {
	                    // display result without highlighting
	
	                }*/
	
					$(this.target).append('<tr class="' + styler +'">' + AjaxSolr.theme('result', doc, AjaxSolr.theme('snippet', doc),styler) + '</tr>');
					var items = [];
					items = items.concat(this.facetLinks('topics', doc.topics));
					items = items.concat(this.facetLinks('organisations', doc.organisations));
					items = items.concat(this.facetLinks('exchanges', doc.exchanges));
					AjaxSolr.theme('list_items', '#links_' + doc.id, items);
				}
				$(this.target).append('</tbody>');
				
				//Added for column sorting
				$(this.target).tablesorter({widgets: ['zebra']});
				
				/*   Below if will check stats = true/false to show stats field in result table.
				 *   statscomponent used for summation.
				 */
				var statsFlag = this.manager.response.responseHeader.params.stats;
				if( statsFlag != undefined && statsFlag == 'true'){
					$(this.target).append('<tr>' + AjaxSolr.theme('fieldsum',this.manager.response) + '</tr>');
				}
				
				/* Below line is used to show the summation of the field value. 
				 * Mannual calculation from the response.
				 * 
				 */
				var groupSumFlag = this.manager.response.responseHeader.params['group.sum'];
				if(groupSumFlag != undefined && groupSumFlag == 'true'){
					$(this.target).append('<tr id="total">' + calculateSum(this.manager) + '</tr>');
				}
			}

			// Change the height of bottom div Sabeer

			var docsHeight = $("table[id='docs']").height();

			if( docsHeight > 200)
			{

				//	$("div[id='result_table']").height(docsHeight);
				var docsHeight = docsHeight + $("div[id='filter-box']").height() + $("div[id='navigation']").height();
				//	$("div[id='bottom']").height(docsHeight);
				//alert("All"+docsHeight);
				//	$("div[id='result']").height(docsHeight);
				//	$("div[id='bottom']").height(docsHeight);
				//alert("final"+$("div[id='result']").height());
			}




		}//End of Tabular Format
//		Free Form Format
		if(displayType == freeFormType){
			for (i=0;i<tagsize;i++)  {

				for (j=0;j<tagObj[i].attributes.length;j++){
					if(tagObj[i].attributes[j].nodeName == seqTagName){
						index = tagObj[i].attributes[j].nodeValue;
					}
					if(tagObj[i].attributes[j].nodeName  == visibilityTagName){
						if(tagObj[i].attributes[j].nodeValue == visibility_true){
							displayarr[index-1] = '<td class="' + stylee +'">' + tagObj[i].childNodes[0].nodeValue+'</td>';
						}else{
							displayarr[index-1]="";
						}
					}
				}

			}
			for (var i = 0, l = this.manager.response.response.docs.length; i < l; i++) {
				styler = (styler == styleeven) ? styleodd : styleeven;
				var doc = this.manager.response.response.docs[i];
				$(this.target).append( AjaxSolr.theme('resultfreeForm', doc, AjaxSolr.theme('snippet', doc),displayarr,styler) );
			}
		}//End of Free Form Format
//		Google Format
		if(displayType == googleFormType){
			for (i=0;i<tagsize;i++)  {
				for (j=0;j<tagObj[i].attributes.length;j++){
					if(tagObj[i].attributes[j].nodeName == seqTagName){
						index = tagObj[i].attributes[j].nodeValue;
					}
					if(tagObj[i].attributes[j].nodeName  == visibilityTagName){
						if(tagObj[i].attributes[j].nodeValue == visibility_true){
							displayarr[index-1] = tagObj[i].childNodes[0].nodeValue ;
						}else{
							displayarr[index-1]="";
						}
					}
				}

			}
			for (var i = 0, l = this.manager.response.response.docs.length; i < l; i++) {
				styler = (styler == styleeven) ? styleodd : styleeven;
				var doc = this.manager.response.response.docs[i];
				$(this.target).append( AjaxSolr.theme('resultgoogleForm', doc, AjaxSolr.theme('snippet', doc),displayarr,styler) );
			}

		}//End of Google Format
		loaddata(columToShowArr, urlString);
	},

	init: function(){
		$('a.more').livequery(function(){
			$(this).toggle(function(){
				$(this).parent().find('span').show();
				$(this).text('less');
				return false;
			}, function(){
				$(this).parent().find('span').hide();
				$(this).text('more');
				return false;
			});
		});
	}
	});

})(jQuery);


function checkSuggestion(selfobj){
	manthisobj = selfobj;
	var fqVal = ""+selfobj.manager.store.values('fq');
	var ORlasindex = fqVal.lastIndexOf("OR");
	var lastval = fqVal.substring(ORlasindex+2, fqVal.length-2); 
	lastval = lastval.replace("*","");
	var indexurl = urlObj[0].childNodes[0].nodeValue;
	var spellCheckURL = indexurl.replace("select", "spell");
	
	var fqValArr = manthisobj.manager.store.values('fq');
	var lastFq = fqValArr[(fqValArr.length-1)];
	manthisobj.manager.store.removeByValue("fq",lastFq);
	var fq = manthisobj.manager.store.values('fq');
	manthisobj.manager.store.addByValue("fq",lastFq);

	var fqvalStr = "";
	for(var i in fq){
		fqvalStr = fqvalStr +"&fq="+encodeURIComponent(fq[i]);
	}
	spellCheckURL = spellCheckURL +"?wt=json&spellcheck=true&spellcheck.q="+lastval+fqvalStr+"&json.wrf=?";
	 var suggArr = new Array();
	 $.ajax({
		url: spellCheckURL,
		dataType: 'json',
		success: function(data){
			 var spellcheckObj = data.spellcheck;
			 var suggestionsArr = spellcheckObj.suggestions;
			 for(var i in suggestionsArr){
				 var currentString = String(suggestionsArr[i]);
				 if(currentString == 'collation'){
					var collNum = Number(i)+1;
					var collArr = suggestionsArr[collNum];
					suggArr.push(collArr[1]);
				 }
			 }
			 if(suggArr.length > 0){
				 $('#suggDiv').show();
				 $('#result_status').hide();
				 $('#docs').hide();
				 $('#topsearchResult').hide();
				 $('#listPopUp').hide();
				 constructSuggestion(suggArr);
			 }
		} 
	 });
}

function constructSuggestion(suggArr){
	var suggLink = "";
	for(var i in suggArr){
		if(suggLink != ""){
			suggLink += "&nbsp;";
		}
		suggLink += "<a href='#' onclick='getSuggestedResult(this);'  >"+suggArr[i]+"</a>";
	}
	$('#suggLinks').html(suggLink+"<div class='questionMark'>?</div>");
}

function getSuggestedResult(elObj){
	var suggText = $(elObj).text();
	var fqVal = manthisobj.manager.store.values('fq');
	var lastVal = fqVal[(fqVal.length-1)];
	manthisobj.manager.store.removeByValue("fq",lastVal);
	var tempVal = suggText;
	tempVal = tempVal.toLowerCase();
	tempVal = escapeSpecialChars(tempVal);
	var facetname = "text";
	var fq_splchar = facetname+"_splchars:("+tempVal+" OR "+tempVal+"*)";
	var fq_general = facetname+"_general:("+tempVal+" OR "+tempVal+"*)";
	var fq_text = facetname+"_t:("+tempVal+" OR "+tempVal+"*)";
	var fullFQ = fq_splchar+" OR "+fq_general+" OR "+fq_text;
	manthisobj.manager.store.addByValue('fq', fullFQ);
	manthisobj.manager.doRequest(0);
}