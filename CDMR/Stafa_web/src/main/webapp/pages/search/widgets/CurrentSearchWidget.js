(function ($) {

AjaxSolr.CurrentSearchWidget = AjaxSolr.AbstractWidget.extend({
  afterRequest: function () {
    var self = this;
    var links = [];

    var fq = this.manager.store.values('fq');
    for (var i = 0, l = fq.length; i < l; i++) {
		var fieldIdAndName = fq[i];
		var fieldArraySingleValue = fieldIdAndName.split(":");
		var field ="";
		var fieldValue = fieldIdAndName;
		if(fieldArraySingleValue.length > 0)
		{
			field = fieldArraySingleValue[0];
			if(field != null && field != "")
			{
				field = field.replace(/_/g, ' ');
				fieldValue = field+":";
				for( var j=1; j < fieldArraySingleValue.length; j++)
				{
					fieldValue = fieldValue+fieldArraySingleValue[j];
				}
			}
		}	
		
		//links.push($('<a href="#"/>').text('(x) ' + fieldValue).click(self.removeFacet(fq[i])));
		links.push($('<a href="#" title="'+fieldValue+'"/>').text('(x) ' + fieldValue).click(self.removeFacet(fq[i])));
    }

	if (links.length > 1) {
      links.unshift($('<a href="#" title="Remove All"/>').text('Remove All').click(function () {
        self.manager.store.remove('fq');
        self.manager.doRequest(0);
        return false;
      }));
    }

    if (links.length) {
      AjaxSolr.theme('list_items', this.target, links);
    }
    else {
      $(this.target).html('<div>Viewing all documents!</div>');
    }
  },

  removeFacet: function (facet) {
    var self = this;
    return function () {
      if (self.manager.store.removeByValue('fq', facet)) {
        self.manager.doRequest(0);
      }
      return false;
    };
  }
});

})(jQuery);
