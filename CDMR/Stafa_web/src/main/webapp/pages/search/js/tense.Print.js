  $(document).ready(function(){
	 $( "#printDialog" ).dialog({
			modal: true,
			autoOpen: false,
			title: 'Print Search Result',
			position: 'center',
			resizable: false,
			width: '35%',
			height: 'auto'
		});
	$('#printDialog :text').mask("?999")
	$('#printDialog :text').attr("disabled",true);
	$('#printDialog :radio').click(function(){
		var selection = $('#printDialog :radio:checked').attr("id");
	 	if(selection=="all"){
	 		$('#printDialog :text').attr("disabled",true);
		}
	 	else{
	 		$('#printDialog :text').attr("disabled",false);
		}
	});
	
 });  
  
  
  
  function printPage(){
	   if(TotalPages == 0){
			 alert("Error: No Record(s) Found.");
			 return false;
		 }
	  	 $('#printDialog :text').val('');
 		 $("#totalPages").html("<b>Total "+TotalPages+" Page(s)</b>");
 		 $('#printDialog').dialog('open');
  }

  function doPrint(){

 	 	var selection = $('#printDialog :radio:checked').attr("id");
 	 	if(selection=="all"){
 	 		printTable(0, TotalPages);
 		}
 		else{
 		 	var pageFrom = $('#pageFrom').val();
 			var pageTo = $('#pageTo').val();
 			if(pageFrom==""){
 				$('#pageFrom').val('1');
 			}
 			else if(pageTo==""){
 				$('#pageTo').val(TotalPages)
 			}
 			else if(Number(pageFrom)>Number(TotalPages)){
 				alert("Error: From Page Shouldn't be Greater then Total Page.");
 			}
 			else if(Number(pageTo)>Number(TotalPages)){
 				alert("Error: To Page Shouldn't be Greater then Total Page.");
 			}
 			else{
 				printTable(pageFrom, pageTo);
 			}
 	}
  }
  
  function closepopUp(){
 	 $('#printDialog').dialog('close');
  }
 /*
  * This Function is Used to Print the Table Data by
  * User Specified start and End Page. 
  * */
	 function printTable(pageFrom, pageTo){
			var recordPerPage = 10;
			if(recorsPerPage!="undefined"&&recorsPerPage!=undefined){
				recordPerPage = Number(recorsPerPage);
			}
			$('#printDiv').empty();
			var displayObj = '<tr><th id="thSelectColumn"><ul class="testSelectColumn"><li><img src="search/images/selectcol.png" alt="select columns" title="select columns" /><ul id="sample"></ul></li></ul></th>';
			var displayarr = [];
			var styleodd = rstyleObj[1].childNodes[0].nodeValue;
			var styleeven = rstyleObj[2].childNodes[0].nodeValue;
			var styler = styleodd;
			var index = 0;
			var noOfColumn = Number(noOfColumnObj[0].childNodes[0].nodeValue)+1;
			var noOfColumnToshow = Number(noOfColumnToshowObj[0].childNodes[0].nodeValue);
			var j = noOfColumn - noOfColumnToshow;
			var columToShowArr=new Array();  
		
			for (i=0;i<tagsize;i++)  {
				for (j=0;j<tagObj[i].attributes.length;j++){
					if(tagObj[i].attributes[j].nodeName == seqTagName){
						index = tagObj[i].attributes[j].nodeValue;
					}
					if(tagObj[i].attributes[j].nodeName  == visibilityTagName){
						if(tagObj[i].attributes[j].nodeValue == visibility_true){
							displayarr[index-1] = '<th nowrap class="' + stylee +'">' + tagObj[i].childNodes[0].nodeValue+'</th>';
						}else{
							displayarr[index-1]="";
						}
					}
				}
			}
			for (i=0;i<tagsize;i++)  {
				displayObj = displayObj + displayarr[i];
			}
			displayObj += '</tr>';
			 var storeString = selfObj.manager.store.string();
			 if(pageFrom!="0"&&pageFrom!=0){
				 pageFrom = pageFrom -1;
			 }
			 var startpage = Number(pageFrom);
			 var start  = startpage * recordPerPage;
			 var endpage = Number(pageTo);
			 var end = endpage * recordPerPage;
			 var rows = end - start;
			 var printString = "&start="+start;
			 storeString = storeString.replace("rows="+recordPerPage,"rows="+rows);
			   var k = 0;
			   $('#docs th').each(function(i, el){
				   		var isVisble = $(el).css('display');
				   		if(isVisble=='none'){
							columToShowArr[k] = i+1;
							k++;
					   	}
				})
				jQuery.getJSON(selfObj.manager.solrUrl+ '?' + storeString + printString + '&facet.limit=-1&wt=json&json.wrf=?', {}, function (data) {
					var docs = data.response.docs;
					var x = 0;
					var s = $('<table class="printTable" cellpadding="0" cellspacing="0" border="1"></table>').append(displayObj);
					var filterTagSelection = $("#filter-box").text();
					var temp;
						filterTagSelection = filterTagSelection.replace(/Remove All/g, "");
						temp = filterTagSelection . search("(x)");
						while( temp != -1)
						{
							temp = filterTagSelection.search("(x)");
							if( temp != -1)
							{
								filterTagSelection = filterTagSelection.replace(filterTagSelection.substring(temp-1, temp+2),"");
							}
						}
			 			$('#printDiv').append('<span>'+filterTagSelection+'</span>');
						$('#printDiv').append("<br/><br/>");
					
					 for (var i = 0, l = data.response.docs.length; i < l; i++) {
							var doc = data.response.docs[i];
							styler = (styler == styleeven) ? styleodd : styleeven;
							$(s).append('<tr class="' + styler +'">' + AjaxSolr.theme('result', doc, AjaxSolr.theme('snippet', doc),styler) + '</tr>');
							x++;
							if(x==recordPerPage){
								x=0;
								var len = 1+$('.printTable').length;
								$('#printDiv').append('<span class="printHeader"> Page '+len+'</span>');
								$('#printDiv').append(s);
								$('#printDiv').append("<br/><br/>");
								s = $('<table class="printTable" cellpadding="0" cellspacing="0" border="1"></table>').append(displayObj);;
							}
							else if(i==(l-1)){
								x=0;
								var len = 1+$('.printTable').length;
								$('#printDiv').append('<span class="printHeader"> Page '+len+'</span>');
								$('#printDiv').append(s);
								$('#printDiv').append("<br/><br/>");
								s = $('<table class="printTable" cellpadding="0" cellspacing="0" border="1"></table>').append(displayObj);;
							}
						} 
					 /*
					  * This Column Manager function is used to Print the Columns Which are selected by
					  * the user in Table Column Manager. 
					  * */
					 $('.printTable').columnManager({
							listTargetID:'sample', 
							onClass: 'advon',
							offClass: 'advoff',
							hideInList:[1],
							colsHidden:columToShowArr
					    }); 
						$('.testSelectColumn').clickMenu({onClick: function(){}});
					 	$('.printTable tr th:first-child').hide();
						$(".printTable tr td:first-child").hide();
						$('#printDiv').print();
						$('#printDialog').dialog('close');
						
				});
		}