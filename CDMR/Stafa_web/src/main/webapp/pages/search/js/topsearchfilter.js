var facetSort="count";
var topSearchObj;
function createTopSearchFilter(selfobj){
	var elDivObj = $(selfobj.target);
	$(elDivObj).find(":text").val('');
	topSearchObj = selfobj;
}

var selectedObj = null;
/*
 * This function is used to filter the all Records without any Facet Field limitation.
 * This function will called in every keypress event of Top textbox.
 * */
function getSearchData(selfobj, elDivObj){
	var fields = selfobj.fields;
	var fieldStr = "";
	for(var i in fields){
		fieldStr = fieldStr +"&facet.field="+fields[i];
	}
	var fqvalStr = "";
	var fq = selfobj.manager.store.values('fq');
	for(var i in fq){
		fqvalStr = fqvalStr +"&fq="+encodeURIComponent(fq[i]);
	}

	var qVal = $(elDivObj).find(":text").val();
	qVal = $.trim(qVal);
	if(qVal!=""){
		qVal = qVal.toLowerCase();
		qVal = escapeSpecialChars(qVal);
		var q_splchar = "text_splchars:("+qVal +" OR "+qVal+"*)";
		var q_general = "text_general:("+qVal +" OR "+qVal+"*)";
		var q_rev = "text_rev:("+qVal +" OR "+qVal+"*)";
		var q_text = "text:("+qVal +" OR "+qVal+"*)";
		var fullQ = q_splchar+" OR "+q_general+" OR "+q_rev+" OR "+q_text;
		qVal = encodeURIComponent(fullQ);
	}
	else{
		$('#topsearchResult').css('display', 'none');
	   return false;
	}
	var startUrl = urlObj[0].childNodes[0].nodeValue;
	var url = startUrl+"?facet=true&wt=json&rows=0&json.wrf=?&facet.mincount=1&facet.limit=10&facet.offset=0"+fieldStr;
	url=url+"&facet.sort="+facetSort;
	url=url+'&q=*:*&fq='+qVal+fqvalStr;

	for(var i in orArray){
   	  var orArrval = orArray[i];
   	  var orFqval = "&fq="+orArrval;
   	  url = url.replace(orFqval,"OR "+orArrval);
   	}
	$.ajax({
		url: url,
		dataType: 'json',
		success: function(data) {
		  var listr="";
			for(var i in fields){
			   var field = fields[i];
			   var fieldName = field.replace(/_/g, ' ');
			   for (var j = 0; j<data.facet_counts.facet_fields[field].length;j++) {
				   var weight = j+1;
				   var facetVal = data.facet_counts.facet_fields[field][j];
				   var facetWei = data.facet_counts.facet_fields[field][weight];
				   j++;
				   var hiddenFacetVal = "<div id='facetVal' style='display:none;'>"+facetVal+"</div>";
				   var chckboxel='<input type="checkbox" value="'+facetVal+'"  style="display:none;" />';
				   var hiddenFacetName = "<div id='facetName' style='display:none;'>"+field+"</div>";
					var textLength = 20;
					if( facetVal != null && facetVal != "" && facetVal.length > textLength)
					{
						facetVal = facetVal.slice(0,textLength);
						facetVal = facetVal+"...";
					}
			      	for(var cur=0;cur<tagObj.length;cur++) {
						if(fieldName==(tagObj[cur].attributes[0].nodeValue).replace(/_/g, ' ')) {
							fieldName=tagObj[cur].childNodes[0].nodeValue;
							fieldName = fieldName.replace(/<br\/>/g, '');
						}
					}
			      	
//			      	added for highlighting the text in topsearch resultbox
			      	var temp = $(elDivObj).find(":text").val();
			      	temp = temp.toLowerCase();
			      	fValue = facetVal.toLowerCase();
			      	
			      	var startIndex = fValue.indexOf(temp);
			      	if(startIndex != -1){
			      		var strlength = temp.length;
			      		var matchedstr = facetVal.substr(startIndex, strlength);
			      		facetVal = facetVal.replace(matchedstr, '<b>'+matchedstr+'</b>');
			      	}
			      	listr = listr +"<li style='display:none;'><span style='display:none'>"+facetVal+"</span>"+facetVal+"("+facetWei+")"+"&nbsp;&nbsp;-&nbsp;&nbsp;"+fieldName+chckboxel+hiddenFacetVal+hiddenFacetName+"</li>";
			   }
			}
			var ulStr = "<ul>"+listr+"</ul>";
			$('#topsearchResult').html(ulStr);
			$('#topsearchResult').show();
			var searchVal = decodeURIComponent(qVal);
			var searchArr = searchVal.split("*) OR ");
			var firstQ = searchArr[0];
			var firstArr = firstQ.split(" OR ");
			var searchedString = firstArr[1];

			if(searchedString!=undefined){
				searchedString = unescapeSpecialChars(searchedString);
				
				if(searchedString != null && searchedString.indexOf("(") != -1){
					$('#topsearchResult li').find("span").each(function(i,el){
						var currentText = $(el).closest('li').text().toLowerCase();
						if(currentText.indexOf(searchedString) != -1){
							$(el).closest('li').show();
						}
					});
				}
				else {
					$('#topsearchResult li').find("span:contains('"+searchedString+"')").each(function(i,el){
				    if(i==7){
							return false;
						}
						$(el).closest('li').show();
					});
				}
			}
			var i=1,j=1,pre=1,len=1,l=1;
			$('#query').keydown(function (e){
				var len = $('#topsearchResult li').length;
				if (e.keyCode == 40)
                {
					for(;j<=len;j++) {
                		if($("#topsearchResult li:nth-child("+j+")").is(":visible")) {
                			$(".topsearchResultkb").removeClass("topsearchResultkb");
                			$("#topsearchResult li:nth-child("+j+")").addClass("topsearchResultkb");
                			pre=j;
                			len=j;
                			}
                		}
                	}
               if(e.keyCode==38)
                {
            	   l=1;
                	for(i=pre-1;i>=l;i--) {
                		if($("#topsearchResult li:nth-child("+i+")").is(":visible")) {
                			$(".topsearchResultkb").removeClass("topsearchResultkb");
                			//$("#topsearchResult li:nth-child("+pre+")").removeClass("topsearchResultkb");
                			$("#topsearchResult li:nth-child("+i+")").addClass("topsearchResultkb");
                			pre=i;
                			j=i+1;
                			l=i+1;
                			}
                	}
                }
            });
			$('#topsearchResult').find("li").click(function(){
				getFacetResult(this, selfobj);
			});
		 }
	});
}

function getFacetResult(el, selfobj){
	var facetVal = $(el).find(":checkbox").prop("value");
	var facetName = $(el).find('#facetName').text();
	selfobj.manager.store.addByValue('fq', facetName + ':"'+facetVal+'"');
	selfobj.manager.doRequest(0);
}

function customKeyUp(el,event){
	var elDivObj = $(el).closest("div");
	if($.browser.msie){
		if(event.keyCode==13){
			if($(".topsearchResultkb").length>0) {
				$('#topsearchResult').hide();
				$(".topsearchResultkb").triggerHandler('click');
			}
		else {
			$(elDivObj).find("img").triggerHandler('click');
		}
		}
		else if(event.keyCode!=38 && event.keyCode!=40){
			getSearchData(topSearchObj, elDivObj);
		}
	}
	else if(event.keyCode==13){
		if($(".topsearchResultkb").length>0) {
			$('#topsearchResult').hide();
			$(".topsearchResultkb").triggerHandler('click');
		}
	else {
		$(elDivObj).find("img").triggerHandler('click');
	}
	}
	else if(event.keyCode!=38 && event.keyCode!=40){
		getSearchData(topSearchObj, elDivObj);
	}
}


function customClick(el){
	var elDivObj = $(el).closest("div");
	var qVal = $(elDivObj).find(":text").val();
	qVal = $.trim(qVal);
	if(qVal!=""){
		qVal = qVal.toLowerCase();
		qVal = escapeSpecialChars(qVal);
		var q_splchar = "text_splchars:("+qVal +" OR "+qVal+"*)";
		var q_general = "text_general:("+qVal +" OR "+qVal+"*)";
		var q_rev = "text_rev:("+qVal +" OR "+qVal+"*)";
		var q_text = "text:("+qVal +" OR "+qVal+"*)";
		var fullQ = q_splchar+" OR "+q_general+" OR "+q_rev+" OR "+q_text;
		topSearchObj.manager.store.addByValue('fq', fullQ);
		topSearchObj.manager.doRequest(0);
	}
	$('#topsearchResult').hide();
}

/* SPLHANDLING */
function escapeSpecialChars(srcText)
{
	var qVal = srcText;
	if( qVal != "" && qVal != null)
	{
		qVal = qVal.replace(/\~/g, '\\~');
		qVal = qVal.replace(/\^/g, '\\^');
		qVal = qVal.replace(/&&/g, '\\&\\&');
		qVal = qVal.replace(/\(/g, '\\(');
		qVal = qVal.replace(/\)/g, '\\)');
		qVal = qVal.replace(/-/g, '\\-');
		qVal = qVal.replace(/\+/g, '\\+');
		qVal = qVal.replace(/\[/g, '\\[');
		qVal = qVal.replace(/\]/g, '\\]');
		qVal = qVal.replace(/\{/g, '\\{');
		qVal = qVal.replace(/\}/g, '\\}');
		qVal = qVal.replace(/\|\|/g, '\\|\\|');
		qVal = qVal.replace(/\:/g, '\\:');
		qVal = qVal.replace(/\//g, '\\/');
	}
	return qVal;
}

function unescapeSpecialChars(srcText)
{
	var qVal = srcText;
	if( qVal != "" && qVal != null)
	{
		qVal = qVal.replace(/\\~/g, '~');
		qVal = qVal.replace(/\\^/g, '^');
		qVal = qVal.replace(/\\&\\&/g, '&&');
		qVal = qVal.replace(/\\\(/g, '(');
		qVal = qVal.replace(/\\\)/g, ')');
		qVal = qVal.replace(/\\-/g, '-');
		qVal = qVal.replace(/\\\+/g, '+');
		qVal = qVal.replace(/\\\[/g, '[');
		qVal = qVal.replace(/\\\]/g, ']');
		qVal = qVal.replace(/\\\{/g, '{');
		qVal = qVal.replace(/\\\}/g, '}');
		qVal = qVal.replace(/\\\|\\\|/g, '||');
		qVal = qVal.replace(/\\\:/g, ':');
		qVal = qVal.replace(/\\\//g, '/');
		qVal = qVal.replace(/<br\/>/g, '');
	}
	return qVal;
}

function calculateSum(responseObj){
	var url;
	var hosturl = urlObj[0].childNodes[0].nodeValue;
	var fQuery = responseObj.store.values('fq');
	var fQueryStr = "";
	for(var i in fQuery){
		fQueryStr = fQueryStr +"&fq="+decodeURIComponent(fQuery[i]);
	}
	
	
	
	var query = responseObj.response.responseHeader.params.q;
	var filterList = responseObj.response.responseHeader.params['stats.field'];
	var groupField = responseObj.response.responseHeader.params['group.field']; 
	var colCountToShow = noOfColumnToshowObj[0].childNodes[0].nodeValue;
	if(groupField != undefined && groupField != null){
		url = hosturl+"?";
		if(query == "*:*" && fQuery == undefined){
			url = url+ "q=*%3A*";		 
		}
		else if(query == "*:*" && fQuery != undefined){
			url = url + "q=*%3A*"+fQueryStr;
		}
		
		if(filterList == undefined || filterList == null){
			filterList = "";
		}
		url = url + "&rows=-1&hl=true&hl.fl=text&json.nl=map&wt=json&group=true&group.format=simple&group.field="+groupField+"&fl="+filterList+"&json.wrf=?";
		for(var i in orArray){
			
		   	  var orArrval = decodeURIComponent(orArray[i]);
		   	  var orFqval = "&fq="+orArrval;
		   	  url = url.replace(orFqval,"OR "+orArrval);
		}
		$.ajax({ 
			url :url,
			dataType: 'json',
			async: false,
			success:function(data){
				var fl = []; 
				var responseheader = data.responseHeader.params.fl;
				fl=responseheader.split(',');
				var objects = {};
				objects = data.grouped[groupField].doclist.docs;
				var categories = new Array();
				var groupedObjects = new Array();
				var i = 0;
				var existingObj = new Object;
				for(k=0; k<fl.length; k++){
					 eval("existingObj."+fl[k]+"=0");
				}
				_.each(objects,function(obj){
				   for( j=0; j<fl.length; j++)
				    {
				        var a ="obj."+fl[j];
				        if( eval(a) != null && eval(a)!= undefined)
				        {
				             var preVal = eval("existingObj."+fl[j]);
				             var b = "existingObj."+fl[j]+' = '+(preVal +Number(eval(a)));
				             eval(b);                                 
				        }
				    }
				});
				
				var temp='<td><b>Total</b></td>';
				
				var colmgr=$.cookie(currentSelc+'docs');
			
				for(i=0;i<tagsize;i++){
					for (j=0;j<tagObj[i].attributes.length;j++){
						if(tagObj[i].attributes[j].nodeName  == visibilityTagName){
							if(tagObj[i].attributes[j].nodeValue == visibility_true){
								
								var nodeVal = tagObj[i].attributes[0].nodeValue;
								var total = eval('existingObj.'+nodeVal);
									if(total != null && !isNaN(total) && total != undefined){
										if(colmgr != null){
											if(colmgr[i+1] == 1){
												temp += '<td><b>'+total+'</b></td>';
											}
											else{
												temp += '<td style="display:none"><b>'+total+'</b></td>';
											}
										}
										else{
											if(i<colCountToShow){
												temp += '<td><b>'+total+'</b></td>';
											}
											else{
												temp += '<td style="display:none"><b>'+total+'</b></td>';
											}
										}
									}
									else{
										if(colmgr != null){
											if(colmgr[i+1] == 1){
												temp+='<td>  </td>';
											}
											else{
												temp+='<td style="display:none">  </td>';
											}
										}
										else{
											if(i<colCountToShow){
												temp+='<td>  </td>';
											}
											else{
												temp+='<td style="display:none">  </td>';
											}
										}
											
										
									}
							}
									
						}

					}
				}
				$('#total').html(temp);
			}
		});
	}
}

function formatDate(dateFormat,datetime)
{
//Object.prototype.toString.call(date) === '[object Date]'
	
	if(datetime != null && datetime !="" && datetime !="" && dateFormat != "")
	{
		var newDate = datetime;
		//alert(Object.prototype.toString.call(newDate));
		if(Object.prototype.toString.call(newDate) === '[object String]')
		{
			//alert(datetime);
			return(dateFormate(dateFormat, newDate));
		}
	}
	return datetime;
}

function dateFormate(dateFormat, datetime) {
    return $.datepicker.formatDate(dateFormat, new Date(datetime));
};
