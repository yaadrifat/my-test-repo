var saveSearchurl = getSaveSearchURL();
/*
 * This Function is Used to save the User Search Criteria.
 * This Function will trigger a AJAX call with the data to save.
 * */
function saveSearch(){
	var fq = topSearchObj.manager.store.values('fq');
	var qVal = ""+encodeURIComponent(topSearchObj.manager.store.values('q'));
	if(fq==""&&qVal=="*%3A*"){
		alert("Error: Please Select any Criteria to Save.");
		return false;
	}
	getSearchNameToSave();
}

function verifySearchName(){
	var searchName =  $('#saveSearchName').val();
	if($.trim(searchName)==""){
		alert("Error: Please Enter Search Name to Save.");
		$('#saveSearchName').focus();
		return false;
	}
	var visibleColumns = "";
	$('#docs th.tableth:visible').each(function(i, el){
		var thId = $(el).attr("id");
		if(visibleColumns != ""){
			visibleColumns += "||";
		}
		visibleColumns += thId;
	});
	var visibleColLen = $('#docs th.tableth:visible').length;
	if(visibleColLen == 0){
		alert("Error: No visible columns found.");		
		return false;
	}
	var comment = $('.saveNameTable textarea').val();
	if(comment != ""){
		comment = comment.replace(/\n/g,'``');
	}
	else{
		comment = " ";
	}
	var fq = topSearchObj.manager.store.values('fq');
	var qVal = ""+encodeURIComponent(topSearchObj.manager.store.values('q'));
	var fqvalStr = "";
	for(var i in fq){
		if(fqvalStr!=""){
			fqvalStr+="||";
		}
		fqvalStr = fqvalStr +encodeURIComponent(fq[i]);
	}
	var orArr = "";
	for(var i in orArray){
		if(orArr!=""){
			orArr+="||";
		}
		orArr = orArr + orArray[i];
   	}
	validateSearchName(searchName, fqvalStr, qVal, orArr, visibleColumns, comment);
}

function getSearchNameToSave(){
	var tabelStr = "<table class='saveNameTable'><tbody>" +
					"<tr><td>Name:</td><td><input type='text' id='saveSearchName' /></td></tr>" +
					"<tr><td>Commment:</td><td><textarea rows='3' cols='20' ></textarea></td></tr>" +
				   "</tbody></table>";
	
	var dialogDiv = "<div>"+tabelStr+"</div>";
	$( "#saverSearchDialogDiv").html(dialogDiv).dialog({
		modal: true,
		title: 'Enter Search Details to Save',
		position: 'center',
		width: 450,
		buttons: {
            "Close": function() {
                $( this ).dialog( "close" );
            },
            "Save": function() {
            	verifySearchName();
            }
        },
		close: function( event, ui ) {
			$('#saverSearchDialogDiv').empty();
			$('#saverSearchDialogDiv').dialog('destroy');
		},
		open: function( event, ui ) {
			$('#saveSearchName').focus();
		}
	});
}
/*
 * This Function is Used to Get the User Search Criteria.
 * This Function will trigger a AJAX call and get the Results
 * from Server and Show in GUI.
 * */
function openSavedSearch(){

	$.ajax({
		url: saveSearchurl,
		dataType: 'jsonp',
		data:{
			criteria: "fetch",
			domain: currentSelc
		},
		jsonpCallback: 'tenseCallback',
		success: function(data) {
			var message = data.message;
			var datalst = data.datalst;
			if(message=="error"||datalst.length==0){
				alert("No Record(s) Found.");
				return false;
			}
		    openSavedSerachDialog(datalst);
		} 
	});
}

function openSavedSerachDialog(datalst){
	var checkboxth = "<th class='ui-widget-header centerAlign'><input type='checkbox' class='selectAllRows' onclick='selectAllCheckbox()' /></th>";
	var tablehead = "<thead>"+checkboxth+"<th class='ui-widget-header'>Search Name</th><th class='ui-widget-header'>Saved Time</th>" +
			"<th class='ui-widget-header'>Search Facets</th><th class='ui-widget-header'>Comment</th></thead>";
	var tableRows = "";
	for(var i in datalst){
		var fqStr = "";
		var rowObj = datalst[i];
		var searchName = rowObj.searchName;
		var searchArr =  searchName.split("|");
		var fqVal = rowObj.fq;
		var qVal = rowObj.q;
		var orArr = rowObj.orArr;
		var comment = rowObj.comment;
		var decodedFq = decodeURIComponent(fqVal);
		comment = comment.replace(/``/g,'<br/>');
		var fqArr = decodedFq.split("||");
		for(var i in fqArr){
			var fqQueryArr = fqArr[i].split(":");
			var facetName = fqQueryArr[0];
			var facetVal = fqQueryArr[1];
			var facetActualName = $('#'+facetName).text();
			if(fqStr != ""){
				fqStr += "<br/>";
			}
			if(facetActualName == ""){
				facetActualName = "ALL";
				var firstIndex = facetVal.indexOf("OR");
				facetVal = facetVal.substr(1, firstIndex-2)+"*"; 
			}
			fqStr += ""+facetActualName+":&nbsp;"+ facetVal;
		}
		var hiddenval = "<input type='hidden' value="+rowObj.visibleColumns+" id='visibleColumns' /><input type='hidden' value="+rowObj.fq+" id='fq' /><input type='hidden' value="+qVal+" id='q' /><input type='hidden' value="+orArr+" id='orArr' />";
		searchName = "<a href='#' onclick=getSavedResult(this) >"+hiddenval+searchArr[0]+"</a>";

	    var checkboxStr="<input type='checkbox' class='rowSelection' value='"+rowObj.searchName+"' />";
		tableRows += "<tr><td class='centerAlign'>"+checkboxStr+"</td><td>"+searchName+"</td><td>"+searchArr[1]+"</td><td>"+fqStr+"</td><td>"+comment+"</td></tr>";
    }
	
	var table = "<table class='savedSearchTable ui-corner-all' >"+tablehead +"<tbody>"+tableRows+"</tbody></table>";
	var dialogDiv = "<div>"+table+"</div>";
	var dialogTitle = $('#'+currentSelc+" li.minwidth110 > a").text() + " - Saved Searches";
	
	var rowlength = datalst.length;
	var dialogHeight = "auto";
	if(rowlength > 5){
		dialogHeight = 500;
	}
	
	$( "#saverSearchDialogDiv").html(dialogDiv).dialog({
		modal: true,
		title: dialogTitle,
		position: 'center',
		width: '56%',
		height: dialogHeight,
		buttons: {
            "Close": function() {
                $( this ).dialog( "close" );
            },
            "Delete": function() {
            	deleteSavedSearch();
            }
        },
		close: function( event, ui ) {
			$('#saverSearchDialogDiv').empty();
			$('#saverSearchDialogDiv').dialog('destroy');
		}
	});
}

function selectAllCheckbox(){
	var checkedLen = $('.rowSelection:checked').length;
	var checkBoxLen = $('.rowSelection').length;
	if(checkedLen == checkBoxLen){
		$('.rowSelection').prop("checked", false);
		$('.selectAllRows').prop("checked", false);
	}
	else{
		$('.rowSelection').prop("checked",true);
		$('.selectAllRows').prop("checked", true);
	}
}

function toggleDiv(divObj,el){
	var pos = $(el).position();
	var topPos = pos.top+20;
	$(divObj).position({top:topPos});
	if($(divObj).css("display") == 'none'){
		$(divObj).css("display",'block');
	}
	else{
		$(divObj).css("display",'none');
	}
}

function validateSearchName(searchName, fqvalStr, qVal, orArr, visibleColumns, comment){
	$.ajax({
		url: saveSearchurl,
		dataType: 'jsonp',
		data:{
			criteria: "validate",
			domain: currentSelc,
			searchName: searchName,
			fq: fqvalStr,
			q: qVal,
			orArr: orArr,
			visibleColumns: visibleColumns,
			comment: comment
		},
		jsonpCallback: 'tenseCallback',
		success: function(data) {
			var message = data.message;
			if(message=="error"){
				alert("Error: Search Name Already Exists.");
			}
			else if(message == "counterror"){
				alert("Error: Only 10 Searches are Allowed to Save. You are Exceeded the Limit.");
			}
			else{
				alert("Data Saved Successfully.");
				$('#saverSearchDialogDiv').dialog('close');
			}
		}
	});
}

function getSavedResult(el){
	$('ul#targetall li.advon').trigger('click');
	topSearchObj.manager.store.remove('fq');
	topSearchObj.manager.store.remove('q');
	orArray = [];
	var fqVal = $(el).find("#fq").val();
	var fq = fqVal.split("||"); 
	for(var i in fq){
		if(fq[i]!='null'){
			topSearchObj.manager.store.addByValue('fq', decodeURIComponent(fq[i]));
		}
	}
	var orArr = $(el).find("#orArr").val();
	var orValues = orArr.split("||"); 
	for(var i in orValues){
		if(orValues[i]!='null'){
			orArray.push(orValues[i]);
		}
	}
	var  qVal = $(el).find("#q").val();
	$('#saverSearchDialogDiv').dialog('close');
	var visibleColumns = $(el).find("#visibleColumns").val();
	var visibleColumnArr = visibleColumns.split("||");
	for(var i in visibleColumnArr){
		var thId = visibleColumnArr[i];
		var thText = $('#'+thId).text();
		$('ul#targetall li.advoff:contains("'+thText+'")').triggerHandler('click');
	}
	topSearchObj.manager.store.addByValue('q', decodeURIComponent(qVal));
	topSearchObj.manager.doRequest(0);
}
/*
 * This function is Used to Delete the User Saved Search Criteria.
 * This function is Present in Open Search PopUp.
 * */
function deleteSavedSearch(){
	var searchNameArr = "";
	$('.rowSelection:checked').each(function(i, el){
		if(searchNameArr != ""){
			searchNameArr += "~";
		}
		searchNameArr  += $(el).val();
	});
	var length = $('.rowSelection:checked').length;
	if(length > 0){
	var answer = confirm("Are you sure to Delete "+length+" Record?");
	if(answer){
		$.ajax({
			url: saveSearchurl,
			dataType: 'jsonp',
			data:{
				criteria: "delete",
				domain: currentSelc,
				searchNameArr: searchNameArr
			},
			jsonpCallback: 'tenseCallback',
			success: function(data) {
				var message = data.message;
				if(message=="error"){
					alert("Error: Record Not Deleted.");
				}
				else{
					alert("Data Deleted Successfully.");
				}
				$('#saverSearchDialogDiv').dialog('close');
				openSavedSearch();
			}
		});
	}
	}
	else{
		alert("Error: Please Select a Record to Delete.");
	}
}



function selectionlist(listindex){
	var q=qsavedvalues[listindex];
	var qflag=gloqval[listindex];
	var fq=new Array();
	var fqlist="";
	var orlist="";
	var qlist="";
	orArray=[];
	qArray=[];
	if(fqsavedvalues){
		fqlist=""+fqsavedvalues[listindex];
		fq=fqlist.split(",!|");
	   	 
	}
	if(saveorarray[listindex]!=""){
		orlist=""+saveorarray[listindex];
		orArray=orlist.split(",");
	}
	if(saveqarray[listindex]!=""){
		qlist=""+saveqarray[listindex];
		qArray=qlist.split(",");
	}
		selfObj.manager.store.remove('q');
		selfObj.manager.store.addByValue('q',q);
		selfObj.manager.store.remove('fq');
	for (var i = 0; i < fq.length; i++) {
		selfObj.manager.store.addByValue('fq',fq[i]);
	}
	listIndex=listindex;
	selfObj.manager.doRequest(0);
}

function getSaveSearchURL(){
	var exporturl = urlObj[0].childNodes[0].nodeValue;
	var colonpos = exporturl.lastIndexOf(":");
    var beforeport = exporturl.substr(0,colonpos);
    var afterport = exporturl.substr(colonpos);
    var addressArr = afterport.split("/");
    var port = addressArr[0];
    var appname = addressArr[1];
    var reqUrl = beforeport+port+"/"+appname+"/savesearch";
    return reqUrl;
}
