$(function(){
	 $('body').on('click', '.tableRowSelc', function(event) {
		 var compTabRowLen = $('#compDocTable tr').length;
		 if(compTabRowLen == 0){
			 var headRow = $('#docs tr:first').html();
			 $('#compDocTable').append("<tr>"+headRow+"</tr>");
		 }
		 var checked = $(this).is(":checked");
		 var docId = String($(this).attr("id"));
		 if(checked){
			 var selecRowLen = $('#compDocTable tr').length;
			 if(selecRowLen > 5){
				 alert("Error: You can compare only five records at a time. Remove previous selection and add new.");
				 return false;
			 }
			 selcDocForComp[docId] = docId;
			 var rowEl = $(this).closest("tr");
			 $('#compDocTable').append("<tr>"+$(rowEl).html()+"</tr>");
		 }
		 else{
			 removeRowElement(docId);
		 }
	 });
 });

function convertTabletoVertical(){
	var myTable = document.getElementById('compDocTable');
	var newTable = document.createElement('table');
	var maxColumns = 0;
	// Find the max number of columns
	for(var r = 0; r < myTable.rows.length; r++) {
	    if(myTable.rows[r].cells.length > maxColumns) {
	        maxColumns = myTable.rows[r].cells.length;
	    }
	}

	for(var c = 0; c < maxColumns; c++) {
	    newTable.insertRow(c);
	    for(var r = 0; r < myTable.rows.length; r++) {
	        if(myTable.rows[r].length <= c) {
	            newTable.rows[c].insertCell(r);
	            newTable.rows[c].cells[r].innerHTML  = '-';
	        }
	        else {
	           newTable.rows[c].insertCell(r);
	           newTable.rows[c].cells[r].innerHTML  = myTable.rows[r].cells[c].innerHTML;
	        }
	    }
	}
	return newTable;
}
 
 function compareDocument(){
	 var selecRowLen = $('#compDocTable tr').length;
	 if(selecRowLen < 3){
		 alert("Error: Please Select at least two record(s) to compare.");
		 return false;
	 }
	 var newCompareTable = convertTabletoVertical();
	 $(newCompareTable).find("tr:first td").each(function(i,el){
		 if(i==0) {
			 $(el).html("<button type='button' onclick='remvAllElemFromComp()' >(x) Remove All</button>");
		 }
		 else {
			 var docId = $(el).find(":checkbox").attr("id");
			 $(el).html("<button type='button' onclick=removeFromCompare("+i+",'"+docId+"') >(x) Remove From Comparison</button>");
		 }
	 });
	 $(newCompareTable).find('img').remove();
	 var dialogTitle = $('#'+currentSelc+" li.minwidth110 > a").text() + " - Compare Results";
	 $( "#compareDocTableDialog").append($(newCompareTable)).dialog({
			modal: true,
			title: dialogTitle,
			position: 'center',
			height: '600',
			width: '99%',
			resizable: false,
			buttons: {
	            "Close": function() {
	                $( this ).dialog( "close" );
	            }
	        },
			close: function( event, ui ) {
				$('#compareDocTableDialog').empty();
				$('#compareDocTableDialog').dialog('destroy');
			}
		});
 }
 
 function removeFromCompare(index, docId){
	 removeRowElement(docId);
	 $('#compareDocTableDialog table tr').find('td:eq('+index+')').hide();
	 var selecRowLen = $('#compDocTable tr').length;
	 if(selecRowLen == 1){
		 $("#compareDocTableDialog").dialog( "close" );
	 }
	 $( "#compareDocTableDialog" ).dialog( "option", "position", "center");
 }
 
 function removeRowElement(docId){
	 var rowEl = $('#compDocTable').find('#'+docId);
	 if(rowEl != undefined){
		 $(rowEl).closest("tr").remove();
	 }
	 $('#'+docId).prop("checked",false);
	 delete selcDocForComp[docId];
 }
 
 function remvAllElemFromComp(){
	 $('.tableRowSelc').prop("checked",false);
	 $('#compDocTable').empty();
	 selcDocForComp = {};
	 $("#compareDocTableDialog").dialog( "close" );
 }