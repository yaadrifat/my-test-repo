<% String contextpath=request.getContextPath();
String errorMessage = "";

if(session != null && session.getAttribute("sessionInitial") == null){
	errorMessage = "Session Expired";
	session.setAttribute("sessionerror","sessionexpired");
}
%>

<script type="text/javascript">
var errorMessage = "<%=errorMessage%>";
$(document).ready(function(){ 
	if(errorMessage != null && errorMessage != "" && errorMessage != "null"){
		
		window.location = "<%=contextpath%>/pages/login.jsp" ;
	}
});

</script>
