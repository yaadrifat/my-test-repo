<%@ page contentType="text/html; charset=UTF-8"%>

	<link rel="shortcut icon" href = "search/images/favicon.ico" />
	<link rel="stylesheet" type="text/css" href="search/css/reset.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="search/css/tagsearch.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="search/ext/jquery.autocomplete.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="search/ext/smoothness/jquery-ui.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="search/ext/smoothness/jquery-ui.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="search/ext/smoothness/ui.theme.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="search/css/clickmenu.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="search/css/jquery.alerts.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="search/menu/style.css" media="screen" />
 
	
	<script type="text/javascript" src="search/js/declaration.js"></script>
	<script type='text/javascript' src="search/menu/menu.js"></script>
	<script type="text/javascript" src="search/js/jquery.tooltip.js"></script>
	<script type="text/javascript" src="search/js/jquery.columnmanager.js"></script>
	<script type="text/javascript" src="search/js/compareDocs.js"></script>
	<script type="text/javascript" src="search/js/saveresult.js"></script>
	<script type='text/javascript' src='search/js/tense.export.js'></script>
	<script type='text/javascript' src='search/js/jquery.Print.js'></script>
	<script type='text/javascript' src='search/js/tense.Print.js'></script>	
	<script type="text/javascript" src="search/js/search.js"></script>	


	<script>

	
	
	jQuery.expr[':'].contains = function(a, i, m) {
	  	  return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
		};
	var qsavedvalues=[];
	var fqsavedvalues=[];
	var saveindex=0;
	var gloqval=[];
	var resultType=[];
	var saveorarray=[];
	var saveqarray=[];
	var saveIndexName=[];
	var selcDocForComp = {};
/*
	var CTLArray = ['productId','recipientName','donorName','productType','productSource','accessionDate','processingProtocol','recipientWeightKg','aboRhRecipientPreTx','aboRhRecipientPostTx','aboRhDonor','cd34107','cd34Kg10Kg','tnc1010','tncKg108Kg','productVolumeMl','productWeightG','viability','numOfCryoBags','sterility'];

*/
	function getStyleInfo(){
		// alert("getstyleInfo");
			var stylrObj = styleObj[0].getElementsByTagName(styleTag);
			//var hstyleObj = headerStyleObj[0].getElementsByTagName(styleTag);
			//var headerStyle = hstyleObj[0].childNodes[0].nodeValue;
			for(i=0;i<stylrObj.length;i++){
					var divid = stylrObj[i].attributes[0].nodeValue;
					//alert(divid);
				if(document.getElementById(divid) != null && typeof(document.getElementById(divid)) != 'undefined'){
						document.getElementById(divid).className = 	stylrObj[i].childNodes[0].nodeValue;
			   }
			}
		}



function cleartext(){
	if($('#query').val()[0] == ' ')
	{
		$('#query').val('');
	}
}
function loaddata(columToShowArr, urlString){
	/*
	var len = urlString.length;
	if( urlString[(len-1)]=='/')
		urlString = urlString.substring(0,(len-1));
	var urlArray = urlString.split("/");
	len = urlArray.length;
	var lastString = urlArray[(len-1)];
	var secondLastString = urlArray[(len-2)];
	var coreName = "";
	if(lastString.toLowerCase() == "select")
		coreName = secondLastString;
	else
		coreName = lastString;

	if(coreName == "CTL")
	{
	}

	*/
	$('#docs').columnManager({
		listTargetID:'targetall',
		onClass: 'advon',
		offClass: 'advoff',
		hideInList:[1] ,
		saveState: true,
		colsHidden: columToShowArr
	});

//create the clickmenu from the target
	$('#ulSelectColumn').clickMenu({onClick: function(){}});

	$('#wrap').click(function(){
		$('#topsearchResult').hide();
		$('#listPopUp').hide();
	});


}

function hideMenuValues(myArray)
{
	$.each(myArray, function (inx, item){
		var idText = "div[id='"+item+"']";
		$.each($(idText).find('li'), function(i,hitem){
			//$("div[id='studyNumber']").remove(i);
			//$(idText).remove(i);
			//hitem.hide();
			$(idText).find('li').remove(i);
		});
		//alert("$(idText).find('ul')"+$(idText).find('ul'));
		$(idText).removeClass("ulContentDiv");
		$(idText).removeClass("menuFilterulContentDiv");
	});
	/*
	$('#wrap').click(function(){
		$('#topsearchResult').hide();
		$('#listPopUp').hide();
	});
	*/
}

function logOut(){
	 $.ajax({
		  url: "login?logout=true",
		  success: function(data){
			 window.location.href = "servletLoad.jsp";
		  }
		});
 }

 function loadAfterPagination(perpage){
	    var recordList="<li style='float:left'><select id='recorsPerPage' name='recorsPerPage' >"+
					   "<option value='10'>Show 10 results per page</option>"+
					   "<option value='25'>Show 25 results per page</option>"+
					   "<option value='50'>Show 50 results per page</option>"+
					   "<option value='75'>Show 75 results per page</option>"+
					   "<option value='100'>Show 100 results per page</option>"+
					   "</select></li><li style='float:left'><img title='Go' id='go' src='search/images/go.jpg' onclick='showRecords();' /></li>";
		$('#pager').append(recordList);
		if(perpage>10){
			$("option[value='"+recorsPerPage+"']").attr('selected', 'selected');
			}
	}

</script>

  <div id="wrap">
 	<div id="header">
 		<table style="width: 100%;">
 			<tr>
 			<!--Header left Side Content -->
 				<td style="width: 50%;">
 					
 				</td>
 				<!--Header Right Side Content -->
 				<td style="width: 100%;">
 					 <table style="width: 100%;">
 					 	<tr>
 					 		<td>
 					 				<div id="search-box">
 					 							<div id="topsearchDiv">
													<input type="text" id="query" name="query" onkeyup="customKeyUp(this,event)" onfocus="cleartext()" />
													<img src="search/images/search.png" onclick="customClick(this)" />
												</div>
									</div>
									<div id="topsearchResult" style="display: none;">
									</div>
 					 		</td>
 					 	</tr>
 					 </table>

 				</td>

 			</tr>
 		</table>
 	</div>

	<!-- End of Header -->

 <div id="right">
      <div id="result">
         <div id="filter-box">
               	  <ul id="selection"><li></li></ul>
        </div>
		<div id="navigation">
          <div id="menu">
          <a onclick='compareDocument()'>Compare</a>&nbsp;&nbsp;|
		  <a onclick='printPage()'>Print</a>&nbsp;&nbsp;|
		  <a onclick='exportData()'>Export</a>&nbsp;&nbsp;|
		  <a onclick='saveSearch();'>Save Search</a>&nbsp;&nbsp;|
		  <a id='savedList' title='Open Search Results' onclick='openSavedSearch()'>Open Search</a> &nbsp;&nbsp;|
		  <a onclick='showGraph();'>Graph</a>&nbsp;
          <ul id='displayValues'></ul>
          </div>
          <div id="pager-header"></div>
        </div>

		<div id="result_table">
		<table>
			<tr>
				<td>
					<table id="docs" cellpadding="0" cellspacing="0" border="1">
					</table>
					<div id="result_status"></div>
					<div id="suggDiv" style="display: none;">
						<div class='NoRecordMsg' >No Record(s) Found.</div><br/>
						<div>
						<span style="float: left;">Did you mean:&nbsp;</span>
							<div id="suggLinks"></div>
						</div>
					</div>
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
		</table>
		</div>
		<ul id="pager"></ul>
		<br/>
      </div>
	 <div id="printDiv" style="display: none;">
		<br/><br/><br/>
	</div>

   </div>

<ul>

<li>
	<div id="CTL">
		<ul id="tensenav">
			<li class="minwidth110"><a href="#" onclick="loadData('CTL')" style="background-color:#A4A6A5;">Stafa CTL</a>
				<ul id="CTLMenuList"></ul>
			</li>
		</ul>
	</div>
</li>


<!-- Changes in declaration.js,
<li>
<div id="form">
<ul id="nav">
    <li class="minwidth110"><a href="#"  onclick="loadData('Form')" >Form </a>
    <ul id="FormMenuList"></ul>
    </li>
</ul>
</div>
</li>
 -->
<li></li>
</ul>


<div id="dia"></div>
 </div>
<div id="printDialog" style="display: none;">
	<table>
		<tr>
			<td><div id="totalPages"></div></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td><b>Print Range</b></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td><label><input type="radio" name="pageSelec" id="all" checked="checked" />&nbsp;All</label></td>
		</tr>
		<tr>
			<td>
				<label><input type="radio" name="pageSelec"  id="selPages" />&nbsp;Pages</label>
				&nbsp;&nbsp;&nbsp;From:&nbsp;<input type="text" size="2" id="pageFrom" />&nbsp;&nbsp;To:&nbsp;<input type="text" id="pageTo"  size="2" />
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	</table>

	<table style="width: 100%;">
		<tr>
			<td style="text-align: right;"><img src="search/images/print.gif" id="print" onclick="doPrint()"></img></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;<img src="search/images/cancel.gif" id="cancel" onclick="closepopUp()"></img></td>
		</tr>
		<tr><td colspan="2">&nbsp;</td></tr>
	</table>

</div>

<div id="menuFilter" style="display: none;">
<div id="menuFilterContent" >
<input type="hidden" id="facetname"  />
<ol id="selectedOL" style="display: none;">
</ol>
<input type="text" id="menuFiltertxt"  /><img id="search" src = "search/images/search_button.gif"></img>&nbsp;&nbsp;<img id="close" src = "search/images/close.gif"></img>
<div class="intro" id="resultcount"></div>
<hr/>

<div class="alpha-nav">
			<button type="button" onclick="fnGetResult('',this);" class="active"><div class="linklikeDiv">All</div></button>
			<button type="button" onclick="fnGetResult('A',this);"><div class="linklikeDiv">A</div></button>
			<button type="button" onclick="fnGetResult('B',this);"><div class="linklikeDiv">B</div></button>
			<button type="button" onclick="fnGetResult('C',this);"><div class="linklikeDiv">C</div></button>
			<button type="button" onclick="fnGetResult('D',this);"><div class="linklikeDiv">D</div></button>
			<button type="button" onclick="fnGetResult('E',this);"><div class="linklikeDiv">E</div></button>
			<button type="button" onclick="fnGetResult('F',this);"><div class="linklikeDiv">F</div></button>
			<button type="button" onclick="fnGetResult('G',this);"><div class="linklikeDiv">G</div></button>
			<button type="button" onclick="fnGetResult('H',this);"><div class="linklikeDiv">H</div></button>
			<button type="button" onclick="fnGetResult('I',this);"><div class="linklikeDiv">I</div></button>
			<button type="button" onclick="fnGetResult('J',this);"><div class="linklikeDiv">J</div></button>
			<button type="button" onclick="fnGetResult('K',this);"><div class="linklikeDiv">K</div></button>
			<button type="button" onclick="fnGetResult('L',this);"><div class="linklikeDiv">L</div></button>
			<button type="button" onclick="fnGetResult('M',this);"><div class="linklikeDiv">M</div></button>
			<button type="button" onclick="fnGetResult('N',this);"><div class="linklikeDiv">N</div></button>
			<button type="button" onclick="fnGetResult('O',this);"><div class="linklikeDiv">O</div></button>
			<button type="button" onclick="fnGetResult('P',this);"><div class="linklikeDiv">P</div></button>
			<button type="button" onclick="fnGetResult('Q',this);"><div class="linklikeDiv">Q</div></button>
			<button type="button" onclick="fnGetResult('R',this);"><div class="linklikeDiv">R</div></button>
			<button type="button" onclick="fnGetResult('S',this);"><div class="linklikeDiv">S</div></button>
			<button type="button" onclick="fnGetResult('T',this);"><div class="linklikeDiv">T</div></button>
			<button type="button" onclick="fnGetResult('U',this);"><div class="linklikeDiv">U</div></button>
			<button type="button" onclick="fnGetResult('V',this);"><div class="linklikeDiv">V</div></button>
			<button type="button" onclick="fnGetResult('W',this);"><div class="linklikeDiv">W</div></button>
			<button type="button" onclick="fnGetResult('X',this);"><div class="linklikeDiv">X</div></button>
			<button type="button" onclick="fnGetResult('Y',this);"><div class="linklikeDiv">Y</div></button>
			<button type="button" onclick="fnGetResult('Z',this);"><div class="linklikeDiv">Z</div></button>
</div>
	<br/>
	<div id="menuFilterData"></div>
	<div class="cleaner"></div>
	<hr/>
	<div class="pagination" align="center">
		<table width="75%">
			<tr>
				<td><div id="previous"></div></td>
				<td><div id="first"></div></td>
				<td><div id="next"></div></td>
			</tr>
		</table>
	</div>
	</div>
</div>
 <div id="exportDialog" style="display: none;">
	<table>
		<tr>
			<td><div id="exportTotalPages"></div></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td><b>Export Range</b></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td><label><input type="radio" name="exportPageSelec" id="exportAll" checked="checked" />&nbsp;All</label></td>
		</tr>
		<tr>
			<td>
				<label><input type="radio" name="exportPageSelec"  id="exportSelPages" />&nbsp;Page(s)</label>
				&nbsp;&nbsp;&nbsp;From:&nbsp;<input type="text" size="2" id="exportPageFrom" />&nbsp;&nbsp;To:&nbsp;<input type="text" id="exportPageTo"  size="2" />
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	</table>

	<table style="width: 100%;">
		<tr>
			<td style="text-align: right;"><img src="search/images/export.gif" id="print" onclick="doExport()"></img></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;<img src="search/images/cancel.gif" id="cancel" onclick="closeDialog()"></img></td>
		</tr>
		<tr><td colspan="2">&nbsp;</td></tr>
	</table>

</div>
<div id="saverSearchDialogDiv" style="display: none;">
</div>
<div id='compareDocTableDiv' style="display: none;">
	<table id='compDocTable'>
		<tbody>
		</tbody>
	</table>
	<div id='compareDocTableDialog'>
	</div>
</div>




 <script>
function showGraph(){
	
 $("#dia").load("search/search_graph.jsp").dialog({
		   autoOpen: true,
		   modal: true, width:850, height:500,
		   close: function() {
			   jQuery("#dia").dialog("destroy");
			   //$(this).html(" ");
			   $(this).remove();
			  $("#wrap").append('<div id="dia"></div>');

			  
		   }
		});

}

   
/*

$(function() {
	var graph1,graph2,graph3,graph4;
	try{
	var searchUrl = "http://192.168.192.234:8180/TenseServerBMT/CTL/select";
	var storeString = "facet=true&q=*:*&rows=0&facet.field=PRODUCT_SOURCE";
	//?facet=true&q=*%3A*&rows=0&wt=json&facet.field=PRODUCT_SOURCE&facet.field=PROCESSING_PROTOCOL&facet.field=PRODUCT_TYPE&facet.field=STERILITY";
	graph1=new Array(); 
	$.ajax({
			  url: searchUrl + '?' + storeString + '&wt=json&json.wrf=?',
			  dataType: 'json',
			  success: function(data){
				   $(data.facet_counts.facet_fields).each(function(){
					 // alert("this " + this);
					  //alert( "PRODUCT_SOURCE"  + this.PRODUCT_SOURCE);
					  var tmpArr=new Array();
					  $(this.PRODUCT_SOURCE).each(function (counter,val){
					//	 alert("counter " + counter + " /val " + val ) ;
						 tmpCount = counter%2;
					//	 alert("tmpCount" + tmpCount);
						 if( tmpCount==0){
							 tmpArr.push(val);
						 }else {
							tmpArr.push(val);
						 	graph1.push(tmpArr);
						 	tmpArr=new Array();
						 }
					  });
				  });
				 $("#graph1").val(graph1);
				  
		 	  }, 
		 	 error: function(jqXHR, textStatus, errorThrown){
		 		 var responseTxt = jqXHR.responseText;
		 		alert("errorThrown " + errorThrown);
		 	
		 	  }
			});
	 
	
//alert("trace");	
		 
	//showGraph1();	   
	}catch(err){
		alert("error in get data " + err);
	} 
	  //alert("end of graph");
});


*/
   getStyleInfo();

</script>
