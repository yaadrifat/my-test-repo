
(function ($) {
   var curCount =0;
AjaxSolr.ListFacetWidget = AjaxSolr.AbstractFacetWidget.extend({
  afterRequest: function () {
    if (this.manager.response.facet_counts.facet_fields[this.field] === undefined) {
      $(this.target).html(AjaxSolr.theme('no_items_found'));
      return;
    }
   
    var maxCount = 0;
    var objectedItems = [];
// added by Babu
    	var showCount = this.manager.store.get("facet.showlimit").val();

     if(typeof(widgetFields[this.field]) == "undefined"){
     	curCount = showCount;
     	widgetFields[this.field] = curCount;
     }else{
       curCount = widgetFields[this.field];
     } 
// end     
   var facetLimit = this.manager.store.get("facet.limit").val();
   var loopCount = 0; 
    for (var facet in this.manager.response.facet_counts.facet_fields[this.field]) {
      loopCount = loopCount +1;
      var count = parseInt(this.manager.response.facet_counts.facet_fields[this.field][facet]);
      if (count > maxCount) {
        maxCount = count;
      }
      if(loopCount <= facetLimit){	
         objectedItems.push({ facet: facet, count: count });
      }
    }
    objectedItems.sort(function (a, b) {
      return a.facet < b.facet ? -1 : 1;
    });

   
   
    $(this.target).empty();
    for (var i = 0, l = objectedItems.length; i < l; i++) {
      var facet = objectedItems[i].facet;
     //alert(" i  " + i + "curCount  " + curCount + " l " + l);
      if(i<curCount){
              	$(this.target).append(AjaxSolr.theme('listfacet', facet, parseInt(objectedItems[i].count ), this.clickHandler(facet)));
   	}else{
   	// added by Babu    
	     $(this.target).append(AjaxSolr.theme('morefacet',curCount,showCount,this.manager,this.field,this ));
	    if(showCount<curCount){
	    	 $(this.target).append('&nbsp;&nbsp;&nbsp;');
   	         $(this.target).append(AjaxSolr.theme('lessfacet',curCount,showCount,this.manager,this.field ));
   	    }
   	     break;
   	 // end     
	}	
      }
        
  }
});

})(jQuery);
