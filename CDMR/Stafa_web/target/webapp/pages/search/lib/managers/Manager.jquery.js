// $Id: Manager.jquery.js,v 1.1 2013/02/13 08:55:09 bbabu Exp $

/**
 * @see http://wiki.apache.org/solr/SolJSON#JSON_specific_parameters
 */
 //alert("Manager.jquery.js");
AjaxSolr.Manager = AjaxSolr.AbstractManager.extend({
  executeRequest: function () {
    var self = this;
    if (this.proxyUrl) {
	// alert("1"+this.store.string());
      jQuery.post(this.proxyUrl, { query: this.store.string() }, function (data) { self.handleResponse(data); }, 'json');
    }
    else {
		 var storeString = this.store.string();
		 /*
		  * This For loop is Used to Perform AND OR condition 
		  * in the CurrentSearch Facet.  
		  * */
	   	 for(var i in orArray){
	    	  var orArrval = orArray[i];
	    	  var orFqval = "&fq="+orArrval;
	    	  var encodedOrArrval = encodeURIComponent(orArrval);
	    	  var encodedOrFqval = encodeURIComponent(orArrval);
	    	  encodedOrFqval = "&fq="+encodedOrFqval;
	    	  storeString = storeString.replace(orFqval," OR "+orArrval);
	    	  storeString = storeString.replace(encodedOrFqval," OR "+encodedOrArrval);
	    	 }
		 for(var i in qArray){
     	 	  var qArrval ="q="+qArray[i];
     	  	  storeString = storeString.replace(qArrval,"q=*%3A*");
     		 }
		 try{ // changed from -1 to 200 by Sabeer
			 $.ajax({
				  url: this.solrUrl + '?' + storeString + '&facet.limit=100&facet.method=fc&wt=json&json.wrf=?',
				  dataType: 'json',
				  success: function(data){
				 	self.handleResponse(data);
			 	  }, 
			 	 error: function(jqXHR, textStatus, errorThrown){
			 		 var responseTxt = jqXHR.responseText;
			 		 var isJson = IsJsonString(responseTxt);
			 		 if(isJson){
			 			 var jsonObj = jQuery.parseJSON(responseTxt);
			 			 if(jsonObj.error == "session"){
			 				 alert("Your Session has expired. For security reasons, the session will expire automatically, if the browser window is idle for a long time.");
			 				 window.location = "servletLoad.jsp";
			 			 }
			 		 }
			 	  }
				});
		 }
		 catch(Err){
			 alert(err);
		 }
      }
  }
});

function IsJsonString(str) {
    try {
    	jQuery.parseJSON(str);
    } catch (e) {
        return false;
    }
    return true;
}
