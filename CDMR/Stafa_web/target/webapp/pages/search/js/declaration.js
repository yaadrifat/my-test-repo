
//var searchtype="Study";
var searchtype = "CTL"
var currentSelc = "";
//xml variable
var moduleTag = "Module";
var webserviceTag = "WebService";
var urlTag = "URL";
var widgetTag = "Widget";
var parameterTag = "Parameter";
var outputTag = "Output";
var labelTag = "Label";
var noFieldsTag = "NoFields";
var stylesTag = "Styles";
var resultWidgetTag = "ResultWidget";
var styleHeaderTag = "Header";
var styleListWidgetTag = "ListWidget";
var styleTag = "Style";
var typeTag = "Type";
var tabularType = "TabularForm";
var freeFormType = "FreeForm";
var googleFormType = "GoogleForm";
var noOfColumnTag = "NoOfColumn";
var noOfColumnToshow = "NoOfColumnToshow";
var conditionANDOR = "ANDORcondition";
var hyperLinkTag = "LinkParameters";
var hyperLinkUrl = "LinkURL";
var uniqueField = "UniqueField";

var xhttp = null;
var xmlDoc = null;

//End

var browserIE = ($.browser.msie);
var browserFF = ($.browser.mozilla );
var browserSafari = ($.browser.webkit);
var browserOpera = ($.browser.opera);
var browserChrome = ($.browser.chrome);
var version = ($.browser.version);

//var guiFileName = "velosStudyGUI.xml";
var guiFileName = "search/CTLGUI.xml"
//alert("trace 1");
if(browserIE == true && (version == "8.0" || version == "9.0"))
{
	xhttp = new ActiveXObject("Msxml2.XMLHTTP");
	xmlDoc = new ActiveXObject("microsoft.xmldom");
	xhttp.open("GET",guiFileName,false);
	xhttp.send("");
	xmlDoc.loadXML(xhttp.responseText);

}
else
{

	if (window.XMLHttpRequest){
		xhttp=new XMLHttpRequest();
	}
	else
	{
		// for older IE 5/6
		xhttp=new ActiveXObject("Microsoft.XMLHTTP");
		xmlDoc = new ActiveXObject("microsoft.xmldom");

		if (typeof xhttp == "undefined")
		{
			xhttp = new ActiveXObject("Msxml2.XMLHTTP");
			xmlDoc = new ActiveXObject("microsoft.xmldom");
		}
	}

	xhttp.open("GET",guiFileName,false);
	xhttp.send("");

	if (window.XMLHttpRequest)
		xmlDoc=xhttp.responseXML;
	else
		xmlDoc.loadXML(xhttp.responseText);
}
//alert("xmlDoc " + xmlDoc);
//currentSelc = "Study";
currentSelc = "CTL"
var moduleObj = xmlDoc.getElementsByTagName(moduleTag);
var webserviceObj = xmlDoc.getElementsByTagName(webserviceTag);
var urlObj = webserviceObj[0].getElementsByTagName(urlTag);
var outputobject = xmlDoc.getElementsByTagName(outputTag);
var hyperLinkTagObj = xmlDoc.getElementsByTagName(hyperLinkTag);
var tagObj = outputobject[0].getElementsByTagName(labelTag);
var tagsize = outputobject[0].getElementsByTagName(noFieldsTag)[0].childNodes[0].nodeValue;
var styleObj = outputobject[0].getElementsByTagName(stylesTag);
var displaytypeObj = outputobject[0].getElementsByTagName(typeTag);
var noOfColumnObj = outputobject[0].getElementsByTagName(noOfColumnTag);
var conditionANDORObj = outputobject[0].getElementsByTagName(conditionANDOR);
var noOfColumnToshowObj = outputobject[0].getElementsByTagName(noOfColumnToshow);
var resultStyleObj = styleObj[0].getElementsByTagName(resultWidgetTag);
var rstyleObj = resultStyleObj[0].getElementsByTagName(styleTag);
var stylee = rstyleObj[0].childNodes[0].nodeValue;
var uniqFieldObj = webserviceObj[0].getElementsByTagName(uniqueField);


//Tag value for comparion
var pagerWidgetValue = "PagerWidget";
var seqTagName = "seq";
var visibilityTagName = "visibility";
var visibility_true = "true";
var repeat="repeat";
var repeatTrue="true";

function loadData(type){
	searchtype=type;
	if(currentSelc == type){
		return false;
	}
	currentSelc = type;
	/*
	if(currentSelc=="Patient"){
		$('#StudyMenuList').empty();
		$('#PatientMenuList').prev().css("background-color","#A9A9A9");
		$('#StudyMenuList').prev().css("background-color","#F5F5F5");
	}
	else if(currentSelc=="Study"){
		$('#PatientMenuList').empty();
		$('#PatientMenuList').prev().css("background-color","#F5F5F5");
		$('#StudyMenuList').prev().css("background-color","#A9A9A9");
	}
	*/
		if(currentSelc=="CTL"){
		$('#CTLMenuList').prev().css("background-color","#5690B6");
	}

	$("#displayValues").empty();
	$('.menuFilterulContentDiv').empty();

	/*
	if(type=="Patient"){
		//xhttp.open("GET","velosPatientGUI.xml",false);
		guiFileName = "velosPatientGUI.xml";
	}
	else if(type=="Study"){
		//xhttp.open("GET","velosStudyGUI.xml",false);
		guiFileName = "velosStudyGUI.xml";
	}else{
		//xhttp.open("GET","velosFormGUI.xml",false);
		guiFileName = "velosFormGUI.xml";
	}
	*/
		if(type=="CTL"){
		guiFileName = "search/CTLGUI.xml"
	}

	xhttp.open("GET",guiFileName,false);
	xhttp.send("");

	if(browserIE == true)
		xmlDoc.loadXML(xhttp.responseText);
	else
		if (window.XMLHttpRequest)
			xmlDoc=xhttp.responseXML;
		else
			xmlDoc.loadXML(xhttp.responseText);


	moduleObj = xmlDoc.getElementsByTagName(moduleTag);
	webserviceObj = xmlDoc.getElementsByTagName(webserviceTag);
	urlObj = webserviceObj[0].getElementsByTagName(urlTag);
	outputobject = xmlDoc.getElementsByTagName(outputTag);
	hyperLinkTagObj = xmlDoc.getElementsByTagName(hyperLinkTag);
	tagObj = outputobject[0].getElementsByTagName(labelTag);
	tagsize = outputobject[0].getElementsByTagName(noFieldsTag)[0].childNodes[0].nodeValue;
	styleObj = outputobject[0].getElementsByTagName(stylesTag);
	displaytypeObj = outputobject[0].getElementsByTagName(typeTag);
	noOfColumnObj = outputobject[0].getElementsByTagName(noOfColumnTag);
	conditionANDORObj = outputobject[0].getElementsByTagName(conditionANDOR);
	noOfColumnToshowObj = outputobject[0].getElementsByTagName(noOfColumnToshow);
	resultStyleObj = styleObj[0].getElementsByTagName(resultWidgetTag);
	rstyleObj = resultStyleObj[0].getElementsByTagName(styleTag);
	stylee = rstyleObj[0].childNodes[0].nodeValue;
	uniqFieldObj = webserviceObj[0].getElementsByTagName(uniqueField);

	//Tag value for comparion

	loadurl(urlObj[0].childNodes[0].nodeValue,moduleObj,webserviceObj);

}
