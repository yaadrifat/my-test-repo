
/*
 * This function is used to Create Normal Filter.
 * This function will append the menu filter div in the Given UL id. 
 * And it will contain the logic to show and hide menus.
 * */
function createFilter(elDiv, selfobj){
	var elDivObj = $("#"+elDiv);
	var exists =  $(elDivObj).find("#filterDiv").html();
	if(exists==null){
	var filterCons = '<div class="filterSection"><div id="filterDiv"><input id="texfil" type="text" class="filterText" />&nbsp;&nbsp;'
					 +'<img id="search" src = "search/images/search_button.gif"></img>&nbsp;&nbsp;<img id="close" src = "search/images/close.gif"></img><br/>'
				     +'&nbsp;&nbsp;<label><input type="radio" id="exactBox" name="'+elDiv+'" />&nbsp;Starts With&nbsp;&nbsp;</label>'
				     +'<label><input type="radio" id="containBox" name="'+elDiv+'" checked="true" />&nbsp;Contains</label></div></div>';

	$(elDivObj).before(filterCons);
	$(elDivObj).prev().find(":text").keyup(function(eve){
		var boxSelec = $(elDivObj).prev().find(':checked').attr("id");
		var val = $.trim($(this).val());
		if(val==""){
			$(elDivObj).find("li").show();
		}			
		else if(boxSelec=="containBox"){
			$(elDivObj).find("li").hide();
            $(elDivObj).find("span:contains('"+val+"')").each(function(i,el){
            	$(el).closest('li').show();
            });
            
            
            $(elDivObj).find("span:not(:contains('"+val+"'))").each(function(i,el){
            	if($(el).closest('li').find("input[type='checkbox']").is(":checked")){
            		$(el).closest('li').show();
            	}
            });
            
            
            $(elDivObj).find(":text").focus();
        }
        else{
            $(elDivObj).find("li").each(function(i,el){
            	    var aVal = $.trim($(el).text());
                    var weight = aVal.split("(");
                    var l=val.length;
                    var tempval = weight[0];
                    var splittedVal = tempval.split(' ');
                    var flag = 0;
                    if(weight[0].substring(0,l).toLowerCase()==val.toLowerCase()) {
                      $(el).show();
                    }
                    else{
                    	
                    	if(splittedVal.length > 1){
                        	for(var j=1; j<splittedVal.length; j++){
                        		if(splittedVal[j].substring(0,l).toLowerCase() == val.toLowerCase()){
                        			flag = 1;
                        			j = splittedVal.length;
                        		}
                            }
                        	if(flag){
                        		$(el).show();
                        	}
                        	else{
                        		if(i >= 0){
                        			if(!$(el).find("input[type='checkbox']").is(":checked")){
                        				$(el).hide();
                        			}
        	                        
        	                    }
                        	}
                        }else{
                        	if(i >= 0){
                        		if(!$(el).find("input[type='checkbox']").is(":checked")){
                    				$(el).hide();
                    			}
    	                    }
                        }
                    }
            });
        }
                
    });
	
	$("input[name="+elDiv+"]").bind("click",function(eve){
		$(elDivObj).prev().find(":text").triggerHandler("keyup");
	});
	$(elDivObj).prev().find(":text").bind("keydown",function(eve){
		if(eve.which == 13){
			$(elDivObj).prev().find("#search").triggerHandler("click");
		}
	});
	$(elDivObj).prev().find("#search").bind("click",function(eve){
		var boxSelec = $(elDivObj).prev().find(':radio:checked').attr("id");
		var txtBoxVal = $.trim($(elDivObj).prev().find(":text").val());
		var filterVal = "";
		var checkboxSelec = $(elDivObj).find(':checkbox:checked').length;
		$(elDivObj).closest('#nav').find('ul:first').css({"display":"none","visibility":" hidden"});
		$(elDivObj).closest("ul").attr("style","display:none;");
		if(checkboxSelec!=0){
			var selec = "";
			var isMultiple = false;
			$(elDivObj).find(':checkbox:checked').each(function(i,el){
					if(selec!=""){
						selec = selec +" OR ";
						isMultiple = true;
					}
					selec = selec +'"'+$(el).prop("value")+'"';
			});
			if(isMultiple){
				selec = "("+selec+")";
			}
			selfobj.manager.store.addByValue('fq', selfobj.field + ':' +selec);
			selfobj.manager.doRequest(0);
		}
		else if(txtBoxVal!=""){
			var facetname = selfobj.field;
			var tempVal = txtBoxVal;
			tempVal = $.trim(tempVal);
			tempVal = tempVal.toLowerCase();
			tempVal = escapeSpecialChars(tempVal);			
			var fq_splchar;
			var fq_general;
			var fq_text;
			if(boxSelec=="containBox"){
				fq_splchar = facetname+"_splchars:("+tempVal+" OR *"+tempVal+"*)";
				fq_general = facetname+"_general:("+tempVal+" OR *"+tempVal+"*)";
				fq_text = facetname+"_t:("+tempVal+" OR *"+tempVal+"*)";
			}
			else{
				fq_splchar = facetname+"_splchars:("+tempVal+" OR "+tempVal+"*)";
				fq_general = facetname+"_general:("+tempVal+" OR "+tempVal+"*)";
				fq_text = facetname+"_t:("+tempVal+" OR "+tempVal+"*)";
			}
			
			
			var fullFQ = fq_splchar+" OR "+fq_general+" OR "+fq_text;
			selfobj.manager.store.addByValue('fq', fullFQ);
			selfobj.manager.doRequest(0);
		}
		
		
	});
	
		$(elDivObj).closest("li").bind('mouseenter', function() {
			$(elDivObj).closest("li").bind('mouseleave', function() {
				$(this).closest("ul").css("display","block");
				$(elDivObj).closest("ul").css("display","block");
				return false;
			});
			$(this).closest("ul").css("display","block");
			$(elDivObj).closest("ul").css("display","block");
			
			$(this).closest("ul").children('li').bind('mouseenter', function() {
				$(elDivObj).closest("ul").css("display","none");
				$(elDivObj).closest("li").bind('mouseenter', function() {
					$(elDivObj).closest("ul").css("display","block");
				});
				return false;
			});
			return false;
		});
		
		$(elDivObj).prev().find("#close").click(function(eve){
			 $(elDivObj).closest('#nav').find('ul:first').css({"display":"none","visibility":" hidden"});
			 $(elDivObj).closest("ul").attr("style","display:none;");
			 $(elDivObj).closest("li").unbind('mouseleave');
		});
		
		$('#nav >li').bind('mouseenter', function() {
			 $(elDivObj).closest("ul").attr("style","display:none;");
		});
	}
}


/*
 * This function is used to Create Date Filter.
 * Now it can Used by Patient Menu for Enrolled Date.
 * The Date format is converted to solr Date Format to get Results.
 * */

function createDateFilter(elDiv, selfobj){
	var elDivObj = $("#"+elDiv);
	var fromBox = elDiv+'_fromBox';
	var toBox = elDiv+'_toBox';
	var filterCons = '<li><div  id="filterDateDiv"><table><tr>'
					 +'<td style="text-align:right">&nbsp;&nbsp;From&nbsp;</td><td style="text-align:left"><input type="text" id="'+fromBox+'" size="12"/></td>'
					 +'</tr><tr><td style="text-align:right">&nbsp;&nbsp;To&nbsp;</td><td style="text-align:left"><input type="text" id="'+toBox+'" size="12"/>'
					 +'</td></tr></table><div><img id="search" src = "search/images/search_button.gif"></img>&nbsp;&nbsp;<img id="close" src = "search/images/close.gif"></img>'
					 +'</div></div></li>';	
	$(elDivObj).html(filterCons);
	$(elDivObj).find(":text").dateMask({separator:'-', format:'ymd'});
	var fromDateObj = $(elDivObj).find('#'+fromBox);
	var toDateObj = $(elDivObj).find('#'+toBox);
	
	$(elDivObj).closest("li").bind('mouseenter', function() {
		$(elDivObj).closest("li").bind('mouseleave', function() {
			$(this).closest("ul").css("display","block");
			$(elDivObj).closest("ul").css("display","block");
			return false;
		});
		$(this).closest("ul").css("display","block");
		$(elDivObj).closest("ul").css("display","block");
		
		$(this).closest("ul").children('li').bind('mouseenter', function() {
			$(elDivObj).closest("ul").css("display","none");
			$(elDivObj).closest("li").bind('mouseenter', function() {
				$(elDivObj).closest("ul").css("display","block");
			});
			return false;
		});
		return false;
	});
	
	$(fromDateObj).datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: "yy-mm-dd",
			onSelect: function( selectedDate ) {
				selectedDate = getUSformatedDate(selectedDate);
				$(toDateObj).val('');
				$(toDateObj).datepicker( "destroy" );
				$(toDateObj).datepicker({
					changeMonth: true,
					changeYear: true,
					dateFormat: "yy-mm-dd",
					minDate: new Date(selectedDate)
				});
			} 
	}); 
	
	$(toDateObj).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "yy-mm-dd"
	});
	 
	$(elDivObj).find("#search").click(function(eve){
		 var fromdate = $(fromDateObj).val();
		 var todate = $(toDateObj).val();
		 var filedVal = "";
		 if(fromdate!=""&todate!=""){
			 var fdate=new Date(getUSformatedDate(fromdate));
			 var tdate=new Date(getUSformatedDate(todate));
			 if(tdate < fdate){
				 alert("Error: To date should be greater than From Date.");
				 return false;
			 }
			 if(fromdate == todate){
				 filedVal = fromdate;
			 }
			 else{
				 var solrFromdate = fromdate+"T00:00:00.00Z";
				 var solrTodate = todate+"T23:59:59.999Z";
				 filedVal = '['+solrFromdate+' TO '+solrTodate+']';
			 }
		 }
		 else if(fromdate==""&todate!=""){
			 var solrTodate = todate+"T23:59:59.999Z";
			 filedVal = '[* TO '+solrTodate+']';
		 }
		 else if(fromdate!=""&todate==""){
			 var solrFromdate = fromdate+"T00:00:00.00Z";
			 filedVal = '['+solrFromdate+' TO *]';
		 }
		 $(elDivObj).closest('#nav').find('ul:first').css({"display":"none","visibility":"hidden"});
		 $(elDivObj).closest("ul").attr("style","display:none;");
		 $(elDivObj).closest("li").unbind('mouseleave');
		 if(filedVal!=""){
			 selfobj.manager.store.addByValue('fq', selfobj.field + ':' +filedVal);
			 selfobj.manager.doRequest(0);
		 }
		
	});
	
	$(elDivObj).find("#close").click(function(eve){
		 $(elDivObj).closest('#nav').find('ul:first').css({"display":"none","visibility":" hidden"});
		 $(elDivObj).closest("ul").attr("style","display:none;");
		 $(elDivObj).closest("li").unbind('mouseleave');
		 
	});
	
	$('#nav >li').bind('mouseenter', function() {
		 $(elDivObj).closest("ul").attr("style","display:none;");
	});
	
}

function createNumberFilter(elDiv, selfobj){
	var elDivObj = $("#"+elDiv);
	var fromBox = "numberFrom_"+elDiv;
	var toBox = "numberTo_"+elDiv;
	var valBox = "numberVal_"+elDiv;
	var labelName = "";
	for( i=0; i<tagsize; i++){
		if(tagObj[i].attributes[0].nodeValue == selfobj.field){
			labelName = tagObj[i].childNodes[0].nodeValue;
			labelName = labelName.replace(/<br\/>/g, '');	
		}
	}

var filterCons = '<li><div  id="filterNumberDiv"><table><tr><td nowrap>&nbsp;<label><input class="verticalPosition" type="radio" id="numberRange" name="'+elDiv+'" value="numberRange" checked="checked" onClick=numberRange(this) ><b class="verticalPosition" >&nbsp;'+labelName+' Range</b></label></td></tr>'
	+'<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From&nbsp;<input type="text" size="10" id="'+fromBox+'" >&nbsp;&nbsp;To&nbsp;'
 +'<input type="text" id="'+toBox+'"  size="10"></td></tr> <tr><td>&nbsp;<label><input class="verticalPosition"  type="radio" name="'+elDiv+'" id="number" value="number" onClick=numberRange(this) ><b class="verticalPosition" >&nbsp;'+labelName+'&nbsp;</b></label></td></tr>'
 +'<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="'+valBox+'" size="10" disabled="disabled" >&nbsp;(exact)</td></tr>'
 +'</table><div><img id="search" src = "search/images/search_button.gif"></img>&nbsp;&nbsp;<img id="close" src = "search/images/close.gif"></img>'
 +'</div></div></li>';
	
	$(elDivObj).html(filterCons);
	
	// Function Called to apply the masking to the textboxes
	numbermask(elDiv);
	
	$('#'+valBox).css('background-color','#D8D8D8');
	var numberFromObj = $(elDivObj).find("#"+fromBox);
	var numberToObj = $(elDivObj).find("#"+toBox);
	var numberValObj= $(elDivObj).find("#"+valBox);
	$(elDivObj).closest("li").bind('mouseenter', function() {
		$(elDivObj).closest("li").bind('mouseleave', function() {
			$(this).closest("ul").css("display","block");
			$(elDivObj).closest("ul").css("display","block");
			return false;
		});
		$(this).closest("ul").css("display","block");
		$(elDivObj).closest("ul").css("display","block");
		
		$(this).closest("ul").children('li').bind('mouseenter', function() {
			$(elDivObj).closest("ul").css("display","none");
			$(elDivObj).closest("li").bind('mouseenter', function() {
				$(elDivObj).closest("ul").css("display","block");
			});
			return false;
		});
		return false;
	});
	$(elDivObj).find("#search").click(function(eve){
		 var numberFrom = $(numberFromObj).val();
		 var numberTo = $(numberToObj).val();
		 var numberVal=$(numberValObj).val();
		 var filedVal = "";
		 var boxSelec = $(elDivObj).find(':checked').attr("id");
		 if(boxSelec == 'numberRange'){	 
		 	if(numberFrom!=""&numberTo!=""){
					if(Number(numberFrom)>Number(numberTo)){
						 alert("Error: To value should be greater than From value.");
						 createFocus(toBox);
						 return false;
					 }
					filedVal = '['+ numberFrom +' TO '+ numberTo + ' ]';
			 }
			 else if(numberFrom==""&numberTo!=""){
				 filedVal = '[* TO '+numberTo+']';				 
			 }
			 else if(numberFrom!=""&numberTo==""){
				 filedVal = '['+numberFrom+' TO *]';				 
			 }
			 else{
				 alert("Error: Please enter the value(s) to search.");
				 createFocus(fromBox);
				 return false;
			 }
			 
			 if(validateDecimal(elDiv, 1)){
			      return false;
			 }
		 }
		 if(boxSelec=='number'){
			 if(numberVal!=""){
				 filedVal = numberVal;
			 }
			 else{
				 alert("Error: Please enter the value to search.");
				 createFocus(valBox);
				 return false;
			 }
			 if(validateDecimal(elDiv, 0)){
			      return false;
			 }
		 }
		 $(elDivObj).closest('#nav').find('ul:first').css({"display":"none","visibility":" hidden"});
		 $(elDivObj).closest("ul").attr("style","display:none;");
		 $(elDivObj).closest("li").unbind('mouseleave');
		 if(filedVal!=""){
			 selfobj.manager.store.addByValue('fq', selfobj.field+ '_td' + ':' +filedVal);
			 selfobj.manager.doRequest(0);
		 }
	});
	
	$(elDivObj).find(":text").bind("keydown",function(eve){
		if(eve.which == 13){
			$(elDivObj).find("#search").triggerHandler("click");
		}
	});

	$(elDivObj).find("#close").click(function(eve){
		 $(elDivObj).closest('#nav').find('ul:first').css({"display":"none","visibility":" hidden"});
		 $(elDivObj).closest("ul").attr("style","display:none;");
		 $(elDivObj).closest("li").unbind('mouseleave');
	});
	
	$('#nav >li').bind('mouseenter', function() {
		 $(elDivObj).closest("ul").attr("style","display:none;");
	});
	
}
	
function validateDecimal(elDiv, flag){
  
    var fromBox = "#numberFrom_"+elDiv;
    var toBox = "#numberTo_"+elDiv;
    var valBox = "#numberVal_"+elDiv;
    var status = "";
    
    if(flag){
      var temp = 1;
	if(doDecimalValidation(fromBox)){
	    status += "From range ";
	    $(fromBox).focus();
	    temp++;
	}
	if(doDecimalValidation(toBox)){
	  if(temp > 1){
	    status += "& To range ";
	    $(fromBox).focus();
	  }
	  else{
	      status += "To range ";
	      $(toBox).focus();
	  }
	}
	if(status != ""){
	    status += "not valid !";
	}
    }
    else{
	if(doDecimalValidation(valBox)){
	    status += "Number is not valid !";
	    $(valBox).focus();
	}
    }
    if(status != ""){
	alert(status);
	return true;
    }
	
}
function doDecimalValidation(textboxID){
  
	var textval = $(textboxID).val();
	var temp = 0;
	var textlen = textval.length-1;
	var status = 0;
	if(textlen == 0){
	    if(textval.charAt(0) == '.'){
	      status = 1;
	    }
	}
	else{
	    for(var i=0; i<=textlen; i++){
		
		if(textval.charAt(i) == '.'){
		  temp++;
		  if(temp > 1){
		    status = 1;
		    i= textlen;
		    
		  }
		
		}
	    }
	}
	return status;
}

function createFocus(elId){
	$('#'+elId).focus();
}
function numberRange(elObj)
{
	var elDiv = $(elObj).attr("name");
	var id = $("input[name="+elDiv+"]:checked").attr('id');
	if(id == "numberRange"){
		 $('#numberVal_'+elDiv).val('');
		 $('#numberVal_'+elDiv).attr('disabled','true');
		 $('#numberVal_'+elDiv).css('background-color','#D8D8D8');
		 $('#numberFrom_'+elDiv).removeAttr('disabled');
		 $('#numberTo_'+elDiv).removeAttr('disabled');
		 $('#numberFrom_'+elDiv).css('background-color','');
		 $('#numberTo_'+elDiv).css('background-color','');
		 $('#numberFrom_'+elDiv).mask("2?22222222222");
		 $('#numberTo_'+elDiv).mask("2?22222222222");
		 createFocus('numberFrom_'+elDiv);
	}
	else{
		 $('#numberFrom_'+elDiv).val('');
		 $('#numberTo_'+elDiv).val('');
		 $('#numberFrom_'+elDiv).attr('disabled','true');
		 $('#numberTo_'+elDiv).attr('disabled','true');
		 $('#numberFrom_'+elDiv).css('background-color','#D8D8D8');
		 $('#numberTo_'+elDiv).css('background-color','#D8D8D8');
		 $('#numberVal_'+elDiv).removeAttr('disabled');
		 $('#numberVal_'+elDiv).css('background-color','');
		 $('#numberVal_'+elDiv).mask("2?22222222222");
		 createFocus('numberVal_'+elDiv);
	}
 
}

function numbermask(elDiv){
	 $('#numberFrom_'+elDiv).mask("2?22222222222");
	 $('#numberTo_'+elDiv).mask("2?22222222222");
	 $('#numberVal_'+elDiv).mask("2?22222222222");
}

/*
 * This function is used to Create Age Range Filter.
 * The age values are converted to Date Ranges to get the
 * data from solr.  
 * */

function createAgeRangeFilter(elDiv, selfobj){
	var elDivObj = $("#"+elDiv);
 
	var filterCons = '<li class="filterSection"><div  id="filterAgeDiv"><table><tr><td>&nbsp;<input type="radio" id="range" name="ageRange" value="range" checked="checked" onClick="agerange()">&nbsp;Age Range</td></tr>'
	     			+'<tr> <td style="text-align: left;">&nbsp;&nbsp;&nbsp;From&nbsp;<input type="text" size="2" id="ageFrom" onClick="agemask()"></td><td style="text-align: right;">&nbsp;To&nbsp;</td>'
					 +'<td style="text-align: left;"><input type="text" id="ageTo" onClick="agemask()" size="2"></td></tr> <tr><td>&nbsp;<input type="radio" name="ageRange" id="age" value="age" onClick="agerange()">&nbsp;Age&nbsp;'
					 +'<input type="text" id="ageVal" size="2" disabled="disabled" onClick="agemask()"></td></tr>'
					 +'</table><div><button id="search">Search&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;<button id="close">Close&nbsp;&nbsp;&nbsp;</button>'
					 +'</div></div></li>';	
	
	$(elDivObj).html(filterCons);
	var ageFromObj = $(elDivObj).find("#ageFrom");
	var ageToObj = $(elDivObj).find("#ageTo");
	var ageValObj= $(elDivObj).find("#ageVal");
	$(elDivObj).closest("li").bind('mouseenter', function() {
		$(elDivObj).closest("li").bind('mouseleave', function() {
			$('#PatientMenuList').css("display","block");
			$(elDivObj).closest("ul").css("display","block");
			return false;
		})
		$('#PatientMenuList').css("display","block");
		$(elDivObj).closest("ul").css("display","block");
		return false;
	});
	$('#PatientMenuList > li').bind('mouseenter', function() {
		$(elDivObj).closest("ul").css("display","none");
		$(elDivObj).closest("li").bind('mouseenter', function() {
			$(elDivObj).closest("ul").css("display","block");
		});
		return false;
	});
	
	$(elDivObj).find("#search").click(function(eve){
		 var ageFrom = $(ageFromObj).val();
		 var ageTo = $(ageToObj).val();
		 var ageVal=$(ageValObj).val();
		 var filedVal = "";
		 var boxSelec = $(elDivObj).find(':checked').attr("id");
		 if(boxSelec=='range'){
		 if(ageFrom!=""&ageTo!=""){
			if(Number(ageFrom)>Number(ageTo)){
				 alert("Error: To age should be greater than From Age.");
				 return false;
			 }
			 var ageToDOB = getDOB(ageFrom)+"T23:59:59.999Z";
			 var ageFromDOB = getAgeAdjacent(ageTo)+"T00:00:00.00Z";
			 filedVal = '['+ageFromDOB+' TO '+ageToDOB+']';
		 }
		 else if(ageFrom==""&ageTo!=""){
			 var ageFromDOB = getAgeAdjacent(ageTo)+"T00:00:00.00Z";
			 filedVal = '['+ageFromDOB+' TO *]';
		 }
		 else if(ageFrom!=""&ageTo==""){
			 var ageToDOB = getDOB(ageFrom)+"T23:59:59.999Z";
			 filedVal = '[* TO '+ageToDOB+']';
		 }
		 else{
			 alert("Error: Enter the age range.");
			 return false;
		 }
		 }
		 if(boxSelec=='age'){
			 if(ageVal!=""){
			 var ageDOB = getDOB(ageVal)+'T23:59:99.99Z';
			 var agedate= getAgeDate(ageVal)+'T00:00:00.00Z';
			 filedVal = '['+agedate+' TO '+ageDOB+']';
			 }
			 else{
				 alert("Error:Please enter the age."); 
				 return false;
			 }
		 }
		 if(filedVal!=""){
			 selfobj.manager.store.addByValue('fq', "PATIENT_DOB" + ':' +filedVal);
			 selfobj.manager.doRequest(0);
		 }
		 $('#PatientMenuList').css({"display":"none","visibility":" hidden"});
		 $(elDivObj).closest("ul").css("display","none");
		 $(elDivObj).closest("li").unbind('mouseleave');
	});
	
	$(elDivObj).find("#close").click(function(eve){
		 $('#PatientMenuList').css({"display":"none","visibility":" hidden"});
		 $(elDivObj).closest("ul").attr("style","display:none;");
		 $(elDivObj).closest("li").unbind('mouseleave');
	});
	
	$('#nav >li').bind('mouseenter', function() {
		 $(elDivObj).closest("ul").attr("style","display:none;");
	});
	
}
function getDOB(age){
	 var newdate=new Date();
	 var curyear = newdate.getFullYear();
	 var curmonth = newdate.getMonth()+1;
	 var curDay = newdate.getDate(); 
	 if(curmonth<10){
		 curmonth = "0"+curmonth;
	 }
	 if(curDay<10){
		 curDay = "0"+curDay;	
	}
	 curyear=curyear-age;
	var fullDate = curyear+"-"+curmonth+"-"+curDay;
	return fullDate;
}
function getAgeDate(age){
	 var newdate=new Date();
	 var curyear = newdate.getFullYear();
	 var curmonth = newdate.getMonth()+1;
	 var curDay = newdate.getDate()+1; 
	 if(curmonth<10){
		 curmonth = "0"+curmonth;
	 }
	 if(curDay<10){
		 curDay = "0"+curDay;	
	}
	 curyear=curyear-age-1;
	var fullDate = curyear+"-"+curmonth+"-"+curDay;
	return fullDate;
}
function getAgeAdjacent(age){
	var newdate=new Date();
	 var curyear = newdate.getFullYear();
	 var curmonth = newdate.getMonth()+1;
	 var curDay = newdate.getDate(); 
	 if(curmonth<10){
		 curmonth = "0"+curmonth;
	 }
	 if(curDay<10){
		 curDay = "0"+curDay;	
	}
	 curyear=curyear-age-1;
	var fullDate = curyear+"-"+curmonth+"-"+curDay;
	return fullDate;
}
/*
 * This Function is Used To Convert the Date to ISO format.
 * Because Internet Explorer don't support default ISO Date
 * format Convert method. 
 * */
function getUSformatedDate(date){
	 var dateStr = $.trim(date); 
	 var newdateArr=dateStr.split("-");
	 var year = newdateArr[0];
	 var month = newdateArr[1];
	 var day = newdateArr[2]; 
	 var fullDate = month+"/"+day+"/"+year;
	 return fullDate;
}
/*
 * This Function is Used in Patient UL to filter the 
 * Records By Patient Age.
 * */
function agerange()
{
 if($('#range').attr('checked')=='checked') {
 $('#ageVal').val('');
 $('#ageVal').attr('disabled','true');
 $('#ageFrom').removeAttr('disabled');
 $('#ageTo').removeAttr('disabled');
 $('#ageFrom').mask("1?22");
 $('#ageTo').mask("1?22");
 }
 else {
	 $('#ageFrom').val('');
	 $('#ageTo').val('');
	 $('#ageFrom').attr('disabled','true');
	 $('#ageTo').attr('disabled','true');
	 $('#ageVal').removeAttr('disabled');
	 $('#ageVal').mask("1?22"); 
 }
}

function agemask() {
	$('#ageFrom').mask("1?22");
	 $('#ageTo').mask("1?22");
	 $('#ageVal').mask("1?22");
}

var ascdsc = "asc";
var prevSortField = "";
function sortTable(sortField){
	if(prevSortField == ""){
		prevSortField = sortField;
	}
	if( prevSortField != sortField){
		$('#'+prevSortField+'_sort').attr('src','search/images/sort_ascdsc.png');
		ascdsc="asc";
		prevSortField = sortField;
	}
	if(ascdsc == "asc"){
		
		$('#'+sortField+'_sort').attr('src','search/images/sort_up.png');
		ascdsc = "dsc";
	}
	else{
		$('#'+sortField+'_sort').attr('src','search/images/sort_down.png');
		ascdsc = "asc";
	}
	
}