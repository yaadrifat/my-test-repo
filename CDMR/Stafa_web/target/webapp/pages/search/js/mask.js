/*
	Masked Input plugin for jQuery
	Copyright (c) 2007-2013 Josh Bush (digitalbush.com)
	Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license)
	Version: 1.3.1
*/
(function($) {
	function getPasteEvent() {
    var el = document.createElement('input'),
        name = 'onpaste';
    el.setAttribute(name, '');
    return (typeof el[name] === 'function')?'paste':'input';             
}

var pasteEventName = getPasteEvent() + ".mask",
	ua = navigator.userAgent,
	iPhone = /iphone/i.test(ua),
	android=/android/i.test(ua),
	caretTimeoutId;

$.mask = {
	//Predefined character definitions
	definitions: {
		'9': "[0-9]",
		'a': "[A-Za-z]",
		'*': "[A-Za-z0-9]",
		'1': "[1-9]",
		'2': "[0-9.]"
	},
	dataName: "rawMaskFn",
	placeholder: '',
};

$.fn.extend({
	//Helper Function for Caret positioning
	caret: function(begin, end) {
		var range;

		if (this.length === 0 || this.is(":hidden")) {
			return;
		}

		if (typeof begin == 'number') {
			end = (typeof end === 'number') ? end : begin;
			return this.each(function() {
				if (this.setSelectionRange) {
					this.setSelectionRange(begin, end);
				} else if (this.createTextRange) {
					range = this.createTextRange();
					range.collapse(true);
					range.moveEnd('character', end);
					range.moveStart('character', begin);
					range.select();
				}
			});
		} else {
			if (this[0].setSelectionRange) {
				begin = this[0].selectionStart;
				end = this[0].selectionEnd;
			} else if (document.selection && document.selection.createRange) {
				range = document.selection.createRange();
				begin = 0 - range.duplicate().moveStart('character', -100000);
				end = begin + range.text.length;
			}
			return { begin: begin, end: end };
		}
	},
	unmask: function() {
		return this.trigger("unmask");
	},
	mask: function(mask, settings) {
		var input,
			defs,
			tests,
			partialPosition,
			firstNonMaskPos,
			len;

		if (!mask && this.length > 0) {
			input = $(this[0]);
			return input.data($.mask.dataName)();
		}
		settings = $.extend({
			placeholder: $.mask.placeholder, // Load default placeholder
			completed: null
		}, settings);


		defs = $.mask.definitions;
		tests = [];
		partialPosition = len = mask.length;
		firstNonMaskPos = null;

		$.each(mask.split(""), function(i, c) {
			if (c == '?') {
				len--;
				partialPosition = i;
			} else if (defs[c]) {
				tests.push(new RegExp(defs[c]));
				if (firstNonMaskPos === null) {
					firstNonMaskPos = tests.length - 1;
				}
			} else {
				tests.push(null);
			}
		});

		return this.trigger("unmask").each(function() {
			var input = $(this),
				buffer = $.map(
				mask.split(""),
				function(c, i) {
					if (c != '?') {
						return defs[c] ? settings.placeholder : c;
					}
				}),
				focusText = input.val();

			function seekNext(pos) {
				while (++pos < len && !tests[pos]);
				return pos;
			}

			function seekPrev(pos) {
				while (--pos >= 0 && !tests[pos]);
				return pos;
			}

			function shiftL(begin,end) {
				var i,
					j;

				if (begin<0) {
					return;
				}

				for (i = begin, j = seekNext(end); i < len; i++) {
					if (tests[i]) {
						if (j < len && tests[i].test(buffer[j])) {
							buffer[i] = buffer[j];
							buffer[j] = settings.placeholder;
						} else {
							break;
						}

						j = seekNext(j);
					}
				}
				writeBuffer();
				input.caret(Math.max(firstNonMaskPos, begin));
			}

			function shiftR(pos) {
				var i,
					c,
					j,
					t;

				for (i = pos, c = settings.placeholder; i < len; i++) {
					if (tests[i]) {
						j = seekNext(i);
						t = buffer[i];
						buffer[i] = c;
						if (j < len && tests[j].test(t)) {
							c = t;
						} else {
							break;
						}
					}
				}
			}

			function keydownEvent(e) {
				var k = e.which,
					pos,
					begin,
					end;

				//backspace, delete, and escape get special treatment
				if (k === 8 || k === 46 || (iPhone && k === 127)) {
					pos = input.caret();
					begin = pos.begin;
					end = pos.end;

					if (end - begin === 0) {
						begin=k!==46?seekPrev(begin):(end=seekNext(begin-1));
						end=k===46?seekNext(end):end;
					}
					clearBuffer(begin, end);
					shiftL(begin, end - 1);

					e.preventDefault();
				} else if (k == 27) {//escape
					input.val(focusText);
					input.caret(0, checkVal());
					e.preventDefault();
				}
			}

			function keypressEvent(e) {
				var k = e.which,
					pos = input.caret(),
					p,
					c,
					next;

				if (e.ctrlKey || e.altKey || e.metaKey || k < 32) {//Ignore
					return;
				} else if (k) {
					if (pos.end - pos.begin !== 0){
						clearBuffer(pos.begin, pos.end);
						shiftL(pos.begin, pos.end-1);
					}

					p = seekNext(pos.begin - 1);
					if (p < len) {
						c = String.fromCharCode(k);
						if (tests[p].test(c)) {
							shiftR(p);

							buffer[p] = c;
							writeBuffer();
							next = seekNext(p);

							if(android){
								setTimeout($.proxy($.fn.caret,input,next),0);
							}else{
								input.caret(next);
							}

							if (settings.completed && next >= len) {
								settings.completed.call(input);
							}
						}
					}
					e.preventDefault();
				}
			}

			function clearBuffer(start, end) {
				var i;
				for (i = start; i < end && i < len; i++) {
					if (tests[i]) {
						buffer[i] = settings.placeholder;
					}
				}
			}

			function writeBuffer() { input.val(buffer.join('')); }

			function checkVal(allow) {
				//try to place characters where they belong
				var test = input.val(),
					lastMatch = -1,
					i,
					c;

				for (i = 0, pos = 0; i < len; i++) {
					if (tests[i]) {
						buffer[i] = settings.placeholder;
						while (pos++ < test.length) {
							c = test.charAt(pos - 1);
							if (tests[i].test(c)) {
								buffer[i] = c;
								lastMatch = i;
								break;
							}
						}
						if (pos > test.length) {
							break;
						}
					} else if (buffer[i] === test.charAt(pos) && i !== partialPosition) {
						pos++;
						lastMatch = i;
					}
				}
				if (allow) {
					writeBuffer();
				} else if (lastMatch + 1 < partialPosition) {
					input.val("");
					clearBuffer(0, len);
				} else {
					writeBuffer();
					input.val(input.val().substring(0, lastMatch + 1));
				}
				return (partialPosition ? i : firstNonMaskPos);
			}

			input.data($.mask.dataName,function(){
				return $.map(buffer, function(c, i) {
					return tests[i]&&c!=settings.placeholder ? c : null;
				}).join('');
			});

			if (!input.attr("readonly"))
				input
				.one("unmask", function() {
					input
						.unbind(".mask")
						.removeData($.mask.dataName);
				})
				.bind("focus.mask", function() {
					clearTimeout(caretTimeoutId);
					var pos,
						moveCaret;

					focusText = input.val();
					pos = checkVal();
					
					caretTimeoutId = setTimeout(function(){
						writeBuffer();
						if (pos == mask.length) {
							input.caret(0, pos);
						} else {
							input.caret(pos);
						}
					}, 10);
				})
				.bind("blur.mask", function() {
					checkVal();
					if (input.val() != focusText)
						input.change();
				})
				.bind("keydown.mask", keydownEvent)
				.bind("keypress.mask", keypressEvent)
				.bind(pasteEventName, function() {
					setTimeout(function() { 
						var pos=checkVal(true);
						input.caret(pos); 
						if (settings.completed && pos == input.val().length)
							settings.completed.call(input);
					}, 0);
				});
			checkVal(); //Perform initial check for existing values
		});
	},
	
	/**
	 * Added by J.MuthuKumar.
	 * Date:06/16/2011
	 * Purpose: This Method is Used to set the masking for the date Fields with appropriate format
	 * 		    and do years validation for Minimum and Maximum years.
	 * */
	
	dateMask: function(settings) {

		//CONFIGURAÇÕES DO PLUGIN
		settings = $.extend({
			separator: "/", //CARACTER QUE SEPARA O DIA DO MÊS DO ANO
			format: 'dmy', //FORMATO PARA A DATA ONDE "D" REPRESENTA DIA, "M" REPRESENTA MÊS E "Y" REPRESENTA ANO
			minYear: 1,
			maxYear: 9999				
		}, settings);
		
		//VALIDAR PARÂMETROS DE CONFIGURAÇÃO
		if(/^[\\\/_\* \.,-]$/.exec(settings.separator) == null){
			alert('O parâmetro separador está incorreto.\r\nOs caracteres válidos são: "\/_* .,-"');
			return;
		}
		if(/[^d|m|y]/i.exec(settings.format) != null || settings.format.length > 3){
			alert('O parâmetro format está incorreto.\r\nOs caracteres válidos são: d, m ou y.\r\nEx.: "dmy"');
			return;
		}
	
		return this.each(function() {
			var input = $(this);
			
			function keydownFunc(e) {
				var k = e.keyCode;
				ignore = (k < 16 || (k > 16 && k < 32) || (k > 32 && k < 41) || k == 46 /*DEL*/);
				if(input.val() == getFormat()){
					input.val('');
				}
			};

			function keypressFunc(e) {
				if (ignore) {
					ignore = false;						
					return true;
				}
				e = e || window.event;
				var k = e.charCode || e.keyCode || e.which;

				if (e.ctrlKey || e.altKey || e.metaKey) {//Ignore
					return true;
				} else if ((k >= 32 && k <= 125) || k > 186) {//typeable characters
					var padrao = /[0-9]/;
					var valor = input.val();
					var caract = String.fromCharCode(k);
					if(valor.length == getFormat().length){ //SIMULA O MAX LENGTH
						return false;
					}
					if(padrao.exec(caract)){
						return true;
					}
				}
				return false;
			};
			
			function keyupFunc(e) {				
				formatDate();
			};
			
			function getFormat() {
				var result = '';
				for(var i = 0; i < settings.format.length; i++){
					switch(settings.format.charAt(i).toLowerCase()){
						case 'd':
							result += 'dd' + settings.separator;
						break;
						case 'm':
							result += 'mm' + settings.separator;
						break;
						case 'y':
							result += 'yyyy' + settings.separator;
						break;
					}
				}
				result = result.substr(0, result.length - 1);
				return result;
			}
			
			function formatDate(){
				var re = new RegExp('\\' + settings.separator, 'g');
				var value = input.val().replace(re, '');
				var result = '';
				var j = 0;
				var d = '', m = '', y = '';
				var completeBlock = false;
				
				for(var i = 0; i < value.length; i++){			
					
					completeBlock = (getFormat().charAt(i+j) == settings.separator || i == value.length - 1);
					
					if(getFormat().charAt(i+j) == settings.separator){
						result += getFormat().charAt(i+j);
						j++;
					}
					result += value.charAt(i);

					switch(getFormat().charAt(i+j)){
						case 'd':
							d += value.charAt(i);
						break;
						case 'm':
							m += value.charAt(i);
						break;
						case 'y':
							y += value.charAt(i);
						break;
					}
					
					if(completeBlock){
						
						//VALIDANDO O ANO
						if(y.length == 4){
							if(y < settings.minYear) y = settings.minYear;								
							else if (y > settings.maxYear) y = settings.maxYear;
							
							while((''+y).length < 4){
								y = '0' + y;
							}
							
							result = replaceBlock('y', result, y);
						}
						
						//VALIDANDO O MÊS
						if(m.length == 2){
							if(m < 1) m = '01';
							else if (m > 12) m = 12;
							result = replaceBlock('m', result, m);
						}
						
						//VALIDANDO O DIA
						if(d.length == 2 && m.length == 2){
							if(d < 1) d = '01';						
							if(isMonth31(m)){
								if(d > 31) d = 31;
								
							}else if(m == 2){
								if(y.length == 4){
									if ((y % 4 == 0) && ((y % 100 != 0) || (y % 400 == 0))){
										if(d > 29) d = 29;
									}else{
										if(d > 28) d = 28;
									}										
								}else if(d > 29){
									d = 29;
								}									
							}else{
								if( d > 30) d = 30;
							}								
						}else if(d > 31){
							d = 31;
						}							
						result = replaceBlock('d', result, d);
					}						
					completeBlock = false;
				}					
				input.val(result);
			}
			
			function replaceBlock(index, search, replace){
				var ini = getFormat().indexOf(index);
				var end = getFormat().lastIndexOf(index);
				
				return search.substring(0, ini) + replace + search.substring(end + 1);
			}
			
			function isMonth31(month) {					
				return /01|03|05|07|08|10|12/.exec(month) != null;
			}
			
			function focusFunc(e) {
				if(input.val() == ""){
					//input.val(getFormat());
				}
			};
			
			function blurFunc(e) {
				if(input.val().length != getFormat().length || input.val() == getFormat()){
					input.val('');
				}
			};
			
			function pasteFunc() {
				var val = input.val();
				input.val(val.replace(/[^0-9]/g, ''));
				formatDate();
			}

			if (!input.attr("readonly"))
				input
				.bind("keydown.dateMask", keydownFunc)
				.bind("keypress.dateMask", keypressFunc)
				.bind("keyup.dateMask", keyupFunc)
				.bind("blur.dateMask", blurFunc)
				.bind("focus.dateMask", focusFunc)
				.bind(pasteEventName, pasteFunc);
		});
	}
});


})(jQuery);