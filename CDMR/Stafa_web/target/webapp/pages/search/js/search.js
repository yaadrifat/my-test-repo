//alert("search.js starts");
var Manager;

//added by Babu
var widgetFields=new Array();
//end 

var url = urlObj[0].childNodes[0].nodeValue;
(function ($) {

	$(function () {
		//alert("url " + url);
	try{	
		Manager = new AjaxSolr.Manager({
			//solrUrl: 'http://66.237.42.94:9095/solr/vwip/select'

			solrUrl: url
		});

		var widgetstr = "";

//		Widget Creation 
		var attname="",attvalue="";
	//	alert("Module length " + moduleObj.length);
		for (i=0;i<moduleObj.length;i++)  { 
			var widgetObj =  moduleObj[i].getElementsByTagName(widgetTag);
			for(j=0;j<widgetObj.length;j++){	
				var attObj = widgetObj[j].attributes;
				var widgetName =  eval("AjaxSolr." + widgetObj[j].childNodes[0].nodeValue);

				widgetstr = "new widgetName({";
				for(l=0;l<attObj.length;l++){
					attname = attObj[l].nodeName;
					attvalue = attObj[l].nodeValue;
					if(attname == "fields" || attname == "divnames"|| attname == "fieldsNames"){
						widgetstr += attname+": "+attvalue ;
					}else{
						widgetstr += attname+": '"+attvalue +"'";
					}
					if(l<attObj.length-1){
						widgetstr += ", ";	 
					}
					//alert("widgetstr " + widgetstr);
				}//Attribute Loop end	
				if(widgetObj[j].childNodes[0].nodeValue == pagerWidgetValue){
					widgetstr += ", renderHeader:function(perPage,offset,total){$('#pager-header').html($('<span/>').text('Displaying ' + Math.min(total,offset+1) + ' to ' + Math.min(total,offset+perPage) + ' of ' + total));}";
				}
				widgetstr += "})";
				var widgetcont = eval(widgetstr);
				Manager.addWidget(widgetcont);
			}
		}	 
		//alert("before init");
		Manager.init();

		var paramkey = "", paramvalue = ""; 
		for (i=0;i<webserviceObj.length;i++)  { 
			var paramObj =  webserviceObj[i].getElementsByTagName(parameterTag);
			for(j=0;j<paramObj.length;j++){	
				paramkey = paramObj[j].attributes[0].nodeValue;
				paramvalue = paramObj[j].childNodes[0].nodeValue;
				Manager.store.addByValue(paramkey, paramvalue);
			}
		}

		Manager.doRequest();
	}catch(err){
		alert("Error in search.js " + err);
	}
	});
	//alert("end of search method");
})(jQuery);

function loadurl(newurl,newmoduleObj,newwebserviceObj){
	alert("load url");

	Manager = new AjaxSolr.Manager({
		//solrUrl: 'http://66.237.42.94:9095/solr/vwip/select'

		solrUrl: newurl
	});

	var widgetstr = "";

	//Widget Creation 
	var attname="",attvalue="";

	for (i=0;i<newmoduleObj.length;i++)  { 
		var widgetObj =  newmoduleObj[i].getElementsByTagName(widgetTag);
		for(j=0;j<widgetObj.length;j++){	
			var attObj = widgetObj[j].attributes;
			var widgetName =  eval("AjaxSolr." + widgetObj[j].childNodes[0].nodeValue);

			widgetstr = "new widgetName({";
			for(l=0;l<attObj.length;l++){
				attname = attObj[l].nodeName;
				attvalue = attObj[l].nodeValue;
				if(attname == "fields" || attname == "divnames"|| attname == "fieldsNames"){
					//alert("attname "+attname+"attvalue"+attvalue);
					widgetstr += attname+": "+attvalue ;
				}else{
					widgetstr += attname+": '"+attvalue +"'";
				}
				if(l<attObj.length-1){
					widgetstr += ", ";	 
				}
				//alert("widgetstr " + widgetstr);
			}//Attribute Loop end	
			if(widgetObj[j].childNodes[0].nodeValue == pagerWidgetValue){
				widgetstr += ", renderHeader:function(perPage,offset,total){$('#pager-header').html($('<span/>').text('Displaying ' + Math.min(total,offset+1) + ' to ' + Math.min(total,offset+perPage) + ' of ' + total));}";
			}
			widgetstr += "})";
			var widgetcont = eval(widgetstr);
			Manager.addWidget(widgetcont);
		}
	}	 
	//alert("before init");
	Manager.init();

	var paramkey = "", paramvalue = ""; 
	for (i=0;i<newwebserviceObj.length;i++)  {

		var paramObj =  newwebserviceObj[i].getElementsByTagName(parameterTag);

		for(j=0;j<paramObj.length;j++){	
			paramkey = paramObj[j].attributes[0].nodeValue;
			paramvalue = paramObj[j].childNodes[0].nodeValue;
			Manager.store.addByValue(paramkey, paramvalue);
		}
	}

	Manager.doRequest();
	//alert("end of search.js");

}
//alert("search.js ends");