
$(document).ready(function(){
	 $( "#exportDialog" ).dialog({
			modal: true,
			autoOpen: false,
			title: 'Export Search Result',
			position: 'center',
			resizable: false,
			width: '35%',
			height: 'auto'
		});
	$('#exportDialog :text').mask("?999")
	$('#exportDialog :text').attr("disabled",true);
	$('#exportDialog :radio').click(function(){
		var selection = $('#exportDialog :radio:checked').attr("id");
	 	if(selection=="exportAll"){
	 		$('#exportDialog :text').attr("disabled",true);
		}
	 	else{
	 		$('#exportDialog :text').attr("disabled",false);
		}
	});
	
 }); 

function exportData(){
	 if(TotalPages == 0){
		 alert("Error: No Record(s) Found.");
		 return false;
	 }
	 $('#exportDialog :text').val('');
	 $("#exportTotalPages").html("<b>Total "+TotalPages+" Page(s)</b>");
	 $('#exportDialog').dialog('open');
}

function exportPage(pageFrom, pageTo){
	var recordPerPage = 10;
	if(recorsPerPage!="undefined"&&recorsPerPage!=undefined){
		recordPerPage = Number(recorsPerPage);
	}
	var visibleFacetArray = "";
	var visibleFacetTextArray = "";
	$('#docs th.tableth').each(function(i, el){
		var isVisible = $(el).css("display");
		if(isVisible!="none"){
			if(visibleFacetArray!="" || visibleFacetTextArray!=""){
				visibleFacetArray = visibleFacetArray+",";
				visibleFacetTextArray = visibleFacetTextArray+",";
			}
			visibleFacetArray += $(el).attr("id");
			visibleFacetTextArray += $(el).text();
		}
	});
	var fq = topSearchObj.manager.store.values('fq');
	var domain = currentSelc;
	if(pageFrom!="0"&&pageFrom!=0){
		 pageFrom = pageFrom -1;
	}
	var startpage = Number(pageFrom);
	var start  = startpage * recordPerPage;
	var endpage = Number(pageTo);
	var end = endpage * recordPerPage;
	var rows = end - start;
	
	var exporturl = urlObj[0].childNodes[0].nodeValue;
	var colonpos = exporturl.lastIndexOf(":");
    var beforeport = exporturl.substr(0,colonpos);
    var afterport = exporturl.substr(colonpos);
    var addressArr = afterport.split("/");
    var port = addressArr[0];
    var appname = addressArr[1];
    var reqUrl = beforeport+port+"/"+appname+"/";
    
	var url = reqUrl+"exportexcel?visibleFacet="+visibleFacetArray+"&visibleFacetText="+encodeURIComponent(visibleFacetTextArray)+"&fq="+fq+"&domain="+domain+"&start="+start+"&rows="+rows;
	getExcel(url);
}

function doExport(){
	
	 	var selection = $('#exportDialog :radio:checked').attr("id");
	 	if(selection=="exportAll"){
	 		exportPage(0, TotalPages);
		}
		else{
		 	var pageFrom = $('#exportPageFrom').val();
			var pageTo = $('#exportPageTo').val();
			if(pageFrom == "" || pageTo ==""){
				alert("Error: Please Enter From and To Page(s).");
			}
			else if(Number(pageFrom)>Number(TotalPages)){
 				alert("Error: From Page Shouldn't be Greater then Total Page.");
 			}
 			else if(Number(pageTo)>Number(TotalPages)){
 				alert("Error: To Page Shouldn't be Greater then Total Page.");
 			}
 			else if(Number(pageFrom)>Number(pageTo)){
 				alert("Error: From Page Shouldn't Greater then To Page");
 			}
 			else{
 				exportPage(pageFrom, pageTo);
 			}
	}

}

function getExcel(url){
	$('#exportDialog').dialog('close');
	window.open(url,"Report","");
}

function closeDialog(){
	$('#exportDialog').dialog('close');
}
 