var currentAccoridan = "";
function removeSelec(el){
	var selec = $(el).prop("checked");
	if(selec){
		$(el).prop("checked",false);
	}
	else{
		$(el).prop("checked",true);
	}
}

function removeAllFq(){
	fqArr = [];
	currPage = 1;
	loadLuceneData();
}

function saveCrossIndexSearch(){
	var fqlen = 0; 
	for(var fq in fqArr){
		fqlen++;
	} 
	if(fqlen == 0){
		alert("Error: Please Select any Criteria to Save.");
		return false;
	}
	getSearchNameToSave('cross');
}

function verifyCrossIndexSearchName(){
	var searchName =  $('#saveSearchName').val();
	if($.trim(searchName)==""){
		alert("Error: Please Enter Search Name to Save.");
		$('#saveSearchName').focus();
		return false;
	}
	var comment = $('.saveNameTable textarea').val();
	if(comment != ""){
		comment = comment.replace(/\n/g,'``');
	}
	else{
		comment = " ";
	}
	var fqvalStr = "";
	for(var i in fqArr){
		if(fqvalStr!=""){
			fqvalStr+="||";
		}
		fqvalStr = fqvalStr +encodeURIComponent(fqArr[i]);
	}
	validateSearchName(searchName, fqvalStr, "null", "null", "null", comment, "crossIndex");
}

function openSavedSearchInCrossIndex(datalst){
	var checkboxth = "<th class='ui-widget-header centerAlign'><input type='checkbox' class='selectAllRows' onclick='selectAllCheckbox(this)' /></th>";
	var tablehead = "<thead>"+checkboxth+"<th class='ui-widget-header'>Search Name</th><th class='ui-widget-header'>Saved Time</th>" +
			"<th class='ui-widget-header'>Comment</th></thead>";
	var tableRows = "";
	for(var i in datalst){
		var rowObj = datalst[i];
		var domainName = rowObj.domainName;
		var searchName = rowObj.searchName;
		var searchArr =  searchName.split("|");
		var comment = rowObj.comment;
		comment = comment.replace(/``/g,'<br/>');
		var hiddenval = "<input type='hidden' value="+rowObj.visibleColumns+" id='visibleColumns' /><input type='hidden' value="+rowObj.fq+" id='fq' /><input type='hidden' value="+rowObj.qVal+" id='q' /><input type='hidden' value="+rowObj.orArr+" id='orArr' />";
		searchName = "<a href='#' onclick=getCrossIndexSavedResult(this,'"+domainName+"') >"+hiddenval+searchArr[0]+"</a>";
	    var checkboxStr="<input type='checkbox' class='rowSelection' value='"+rowObj.searchName+"' />";
		tableRows += "<tr class='"+domainName+"'><td class='centerAlign'>"+checkboxStr+"</td><td>"+searchName+"</td><td>"+searchArr[1]+"</td><td>"+comment+"</td></tr>";
    }
	var table = "<table>"+tablehead +"<tbody>"+tableRows+"</tbody></table>";
	var allTable = $(table);
	var noOfIndex = indexObj.length;
	var indexStr = "";
	var trStr = "";
	var accordDivObj = $('<div id="indexAccordionDiv" />');
	for(var i = 0; i < noOfIndex; i++){
		var indexName = indexObj[i].attributes[0].nodeValue;
		trStr = "";
		$(allTable).find('.'+indexName).each(function(i, el){
			trStr += "<tr>"+$(el).html()+"</tr>";
		});
		var table = "<table class='savedSearchTable ui-corner-all' >"+tablehead +"<tbody>"+trStr+"</tbody></table>";
		if(trStr == ""){
			table = "<div class='errorDiv'>No Record(s) Found.</div>";
		}
		indexStr +=  "<h3>&nbsp;&nbsp;&nbsp;&nbsp;"+indexObj[i].childNodes[0].nodeValue+"<input type='hidden' id='indexname' value='"+indexName+"' /></h3><div id='"+indexName+"_div' class='indexAccordion'>"+table+"</div>";
	}
	trStr = "";
	$(allTable).find('.crossIndex').each(function(i, el){
		trStr += "<tr>"+$(el).html()+"</tr>";
	});
	var table = "<table class='savedSearchTable ui-corner-all' >"+tablehead +"<tbody>"+trStr+"</tbody></table>";
	if(trStr == ""){
		table = "<div class='errorDiv'>No Record(s) Found.</div>";
	}
	indexStr =  "<h3>&nbsp;&nbsp;&nbsp;&nbsp;General<input type='hidden' id='indexname' value='crossIndex' /></h3><div id='crossIndex_div' class='indexAccordion'>"+table+"</div>"+indexStr;
	$(accordDivObj).append(indexStr);
	$(accordDivObj).accordion({
		collapsible: true
	});
	$(accordDivObj).find('h3').click(function(){
		currentAccoridan = $(this).find('#indexname').val();
	});
	var dialogTitle = "Saved Searches";
	var rowlength = datalst.length;
	var dialogHeight = "auto";
	if(rowlength > 5){
		dialogHeight = 500;
	}
	$( "<div id='saverSearchDialogDiv' />").html(accordDivObj).dialog({
		modal: true,
		title: dialogTitle,
		position: 'center',
		width: '56%',
		height: dialogHeight,
		buttons: {
            "Close": function() {
                $( this ).dialog( "close" );
            },
            "Delete": function() {
            	deleteSavedSearch(currentAccoridan, "crossFlag");
            }
        },
		close: function( event, ui ) {
			$('#saverSearchDialogDiv').dialog('destroy');
			$('#saverSearchDialogDiv').remove();
		},
		open: function( event, ui ) {
			currentAccoridan = "crossIndex";
			$('.indexAccordion').css({
				height: "auto" 
			});
		}
	});
}

function getCrossIndexSavedResult(elObj, domainName){
	$('#saverSearchDialogDiv').dialog('close');
	if(domainName == "crossIndex"){
		fqArr = [];
		var fqVal = $(elObj).find("#fq").val();
		var fq = fqVal.split("||"); 
		for(var i in fq){
			if(fq[i]!='null'){
				fqArr.push(decodeURIComponent(fq[i]));
			}
		}
		currPage = 1;
		$('#saverSearchDialogDiv').dialog('close');
		loadLuceneData();
	}
	else{
		saveSearchObj = elObj;
		currIndexText = $('#'+domainName).clone().children().remove().end().text();
		$('#mainContent').load("TenseADT/indexSearch.html",function(){
			loadData(domainName);
		});
	}
}

function printCrossIndexResults(){
	var pageSelcDivObj = validatePages("Print");
	if(pageSelcDivObj != false){
		createPageSelcDialog(pageSelcDivObj, "Print");
	}
}

function createPageSelcDialog(pageSelcDivObj, title){
	$('<div id="pageSelcDialog" ></div>').html(pageSelcDivObj).dialog({
		modal: true,
		resizable: false,
		title: title+" Search Result",
		position: 'center',
		width: '35%',
		height: 'auto',
		close: function( event, ui ) {
			$('#pageSelcDialog').dialog('destroy');
			$('#pageSelcDialog').remove();
		},
		open: function( event, ui ) {
			$('#pageSelcDialog #allpages').attr("checked",true);
		}
	});
	if(title == 'Print'){
		$( "#pageSelcDialog" ).dialog( "option", "buttons", {
			"Close": function() { 
				$(this).dialog("close");
			 },
			"Print": function() { 
				 fetchDataFromIndex();
			 } 
		});
	}
	else{
		$( "#pageSelcDialog" ).dialog( "option", "buttons", {
			"Close": function() { 
				$(this).dialog("close");
			 },
			"Export": function() { 
				fetchDataForExcel();
			 } 
		});
	}
}

function validatePages(title){
	var pageSelcDivObj = $('#pageSelcDiv').clone();
	
	var noOfRecords = $('#noOfRecords').val();
	if(Number(noOfRecords) == 0){
		alert("Error: No Record(s) Found.");
		return false;
	}
	
	var rows = Number($('#noOfRows').val());
	var totalpage = Number(noOfRecords)/rows;
	totalpage = Math.ceil(totalpage);
	
	$(pageSelcDivObj).find('#totalCrossIndexPages').text(totalpage);
	$(pageSelcDivObj).find('#dialogtitle').text(title);
	$(pageSelcDivObj).find(':text').mask("?999").attr("disabled",true);;
	
	$(pageSelcDivObj).find(':radio').click(function(){
		var selection = $(pageSelcDivObj).find(':radio:checked').attr("id");
	 	if(selection == "allpages"){
	 		$(pageSelcDivObj).find(':text').attr("disabled",true);
		}
	 	else{
	 		$(pageSelcDivObj).find(':text').attr("disabled",false);
		}
	});
	$(pageSelcDivObj).show();
	return pageSelcDivObj;
	
}

function fetchDataFromIndex(){
	var selection = $('#pageSelcDialog').find(':radio:checked').attr("id");
	var noOfRecords = $('#noOfRecords').val();
	var pageSelc = "";
	if(selection == "allpages"){
		pageSelc = "&rows="+noOfRecords+"&page="+1;
	}
	else{
		pageSelc = pageSelcvalidate();
		if(pageSelc == 'false'){
			return false;
		}
	}
	var queryStr = constructLuceneQuery();
	queryStr += pageSelc;
	$.ajax({
		url: ipvalue+urlvalue,
		dataType:"jsonp",
		jsonpCallback: 'tenseCallback',
		data: queryStr,
		success: function(data){
			$('#pageSelcDialog').dialog("close");
			var docs = data.docs;
			constructResultTable(docs, true);
		}
	});
}
function pageSelcvalidate(){
	var noOfRecords = $('#noOfRecords').val();
	var recordPerPage = Number($('#noOfRows').val());
 	var pageFrom = $('#pageSelcDialog #startPage').val();
	var pageTo = $('#pageSelcDialog #endPage').val();
	var totalpage = Number(noOfRecords)/recordPerPage;
	totalpage = Math.ceil(totalpage);
	
	if(pageFrom==""){
		$('#pageSelcDialog #startPage').val('1');
		pageFrom = 1;
	}
	if(pageTo==""){
		$('#pageSelcDialog #endPage').val(totalpage);
		pageTo = totalpage;
	}
	if(Number(pageFrom)>Number(totalpage)){
		alert("Error: From page shouldn't be greater then total page.");
		return false;
	}
	else if(Number(pageTo)>Number(totalpage)){
		alert("Error: To page shouldn't be greater then total page.");
		return false;
	}
	else if(Number(pageFrom)>Number(pageTo)){
		alert("Error: From page shouldn't be greater then To page.");
		return false;
	}
	else{
		 if(pageFrom!="0"&&pageFrom!=0){
			 pageFrom = pageFrom -1;
		 }
		 var startpage = Number(pageFrom);
		 var start  = startpage * recordPerPage;
		 var endpage = Number(pageTo);
		 var end = endpage * recordPerPage;
		 if(end > Number(noOfRecords)){
			 end = Number(noOfRecords);
		 }
		 pageSelc = "&start="+start+"&end="+end;
		 return pageSelc;
	}
}

function exportCrossIndexResults(){
	var pageSelcDivObj = validatePages("Export"); 
	if(pageSelcDivObj != false){
		createPageSelcDialog(pageSelcDivObj, "Export");
	}
}

function fetchDataForExcel(){
	var displaylen = displayObj.length;
	var fieldKey = "", fieldName = "";
	for(var k = 0; k < displaylen; k++){
			var cols = displayObj[k].getElementsByTagName("Column");
			var collen = cols.length;
			for(var j = 0; j< collen; j++){
				if(fieldKey != ""){
					fieldKey += ",";
				}
				if(fieldName != ""){
					fieldName += ",";
				}
				fieldKey += cols[j].attributes[0].nodeValue;
				fieldName += cols[j].childNodes[0].nodeValue;
			}
	}
	
	var queryStr = constructLuceneQuery();
	var selection = $('#pageSelcDialog').find(':radio:checked').attr("id");
	var noOfRecords = $('#noOfRecords').val();
	var pageSelc = "";
	if(selection == "allpages"){
		pageSelc = "&rows="+noOfRecords+"&page="+1;
	}
	else{
		pageSelc = pageSelcvalidate();
		if(pageSelc == false){
			return false;
		}
	}
	
	var displaylen = displayObj.length;
	var indexFields = "";
	for(var k = 0; k < displaylen; k++){
		var xmlIndexName = displayObj[k].attributes[0].nodeValue;
		var cols = displayObj[k].getElementsByTagName("Column");
		var collen = cols.length;
		var fieldKey = "", fieldName = "";
		for(var j = 0; j< collen; j++){
			if(fieldKey != ""){
				fieldKey += "~~";
			}
			if(fieldName != ""){
				fieldName += "~~";
			}
			fieldKey += cols[j].attributes[0].nodeValue;
			fieldName += cols[j].childNodes[0].nodeValue;
		}
		indexFields +=  "&indexFields="+xmlIndexName+"||"+fieldKey+"||"+encodeURIComponent(fieldName);
	}
	queryStr += indexFields;
	queryStr += pageSelc;
	
	var expUrl = ipvalue +"/luceneExport?"+ queryStr;
	
	window.open(expUrl,"Report","width=200,height=100");
}

