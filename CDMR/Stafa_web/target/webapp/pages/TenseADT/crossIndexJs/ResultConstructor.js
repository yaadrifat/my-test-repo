var entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
  };

  function escapeHtml(string) {
    return String(string).replace(/[&<>"'\/]/g, function (s) {
      return entityMap[s];
    });
  }

function loadLuceneData(){
	var queryStr = constructLuceneQuery();
	var rows = $('#noOfRows').val();
	queryStr += "&rows="+rows+"&page="+currPage;
	$.ajax({
		url: ipvalue+urlvalue,
		dataType:"jsonp",
		jsonpCallback: 'tenseCallback',
		data: queryStr,
		success: function(data){
			var docs = data.docs;
			constructResultTable(docs, false);
			var facets = data.facets;
			constructMenuFacet(facets);
			constructCurrSelc();
			var header = data.header;
			applyLucenePagination(header);
			$('#query').val('');
			if(docs.length == 0){
				$('.indexCount').text('(0)');
				$('#resultContentDiv').hide();
				$('#displayRecCount').html("Displaying 0 records");
				$('#errorMsgDiv table tbody').html("<tr><td><div id='tableErrorMsg'>No Record(s) Found</div></td></tr>");
				$('#errorMsgDiv').show();
			}
			else{
				$('#resultContentDiv').show();
				$('#errorMsgDiv').hide();
			}
			$('#resultTable').css({opacity:'1'});
		}
	});
}

function constructLuceneQuery(){
	var data = facetFq;
	var fqStr = "";
	for(var i in fqArr){
		var fqText = fqArr[i];
		fqText = fqText.replace(/%/g,"_percentage_");
		fqText = encodeURIComponent(fqText);
		fqStr += "&fq="+fqText;
	}
	if(fqStr != ""){
		data += fqStr;
	}
	var sortField = sortObj[0].getElementsByTagName("Field")[0].childNodes[0].nodeValue;
	var sortOrder = sortObj[0].getElementsByTagName("Order")[0].childNodes[0].nodeValue;
	data += "&sortfield="+sortField+"&sortorder="+sortOrder;
	return data;
}

function constructResultTable(docs, printFlag){
	var rowStr = "";
	var tdStr = "";
	var displaylen = displayObj.length;
	for(var i in docs){
		var doc = docs[i];
		tdStr = "";
		var indexName = doc["INDEX_NAME"];
		for(var k = 0; k < displaylen; k++){
			var xmlIndexName = displayObj[k].attributes[0].nodeValue;
			if(indexName == xmlIndexName){
				var cols = displayObj[k].getElementsByTagName("Column");
				var collen = cols.length;
				for(var j = 0; j< collen; j++){
					var visibility = cols[j].attributes[1].nodeValue;
					if(visibility == "true"){
						var fieldKey = cols[j].attributes[0].nodeValue;
						var fieldName = cols[j].childNodes[0].nodeValue;
						var fieldVal = doc[fieldKey];
						if(typeof(fieldVal) == 'undefined'){
							fieldVal = "-";
						}
						fieldVal = escapeHtml(fieldVal);
						
						var maxlenobj = cols[j].attributes[2].nodeValue;
						var dateFormat = cols[j].attributes[3].nodeValue;
						if(maxlenobj != ""){
							var maxlen = Number(maxlenobj);
							if(fieldVal.length > maxlen){
								fieldVal = fieldVal.slice(0, maxlen);
								fieldVal = fieldVal+"...";
							}
						}
						else if( dateFormat != ""){
							fieldVal = formatDate(dateFormat, fieldVal);
						}
						tdStr += "<td><b>"+fieldName+":</b> "+fieldVal+"</td>";
					}
				}
				var indexText = $('#'+indexName).text();
				var countPos = indexText.indexOf("(");
				if(countPos != -1){
					indexText = indexText.substr(0,countPos);
				}
				var docId = doc["DOCID"];
				var viewRecordStr = "<td><button class='viewRecbutton' onclick=loadSelectedData('"+docId+"','"+indexName+"') >View Record</button></td>";
				if(printFlag){
					viewRecordStr = "";
				}
				tdStr = "<td><i>"+indexText+"</i></td>"+viewRecordStr+tdStr;
			}
		}
		rowStr += "<tr>"+tdStr+"</tr>";
	}
	if(printFlag){
		$('#crossIndexPrintDiv').remove();
		$('<div id="crossIndexPrintDiv" />').html('<table cellpadding="0" cellspacing="0" border="1" ><tbody>'+rowStr+'</tbody></table>').print();
	}
	else{
		$('#resultTable tbody').html(rowStr);
	}
}

function constructMenuFacet(facets){
	$('#facetUL ul').remove();
	for(var f in facets){
		var facetObj = facets[f];
		for(var obj in facetObj){
			var facetName = obj;
			if(facetName != 'INDEX_NAME'){
				var facetType = $('#'+facetName+" #facetType").val();
				if(facetType == "AlphabeticSearch"){
					createAlphabetFilter(facetName);
					var facettotal = facetObj.facettotal;
					var totalDocuments = facetObj.totalDocuments;
					constructFacetResult(facetObj, facetName, "", null, facettotal, totalDocuments);
					$('#'+facetName+' .active').css({'font-weight':'bold',"font-size":"18px"});
					$('#'+facetName+' .active').addClass('addFontWeight');
				}
				else if(facetType == "NumberSearch"){
					createNumberRangeFilter(facetName);
				}
				else if(facetType == "DateSearch"){
					createDateRangeFilter(facetName);
				}
				else if(facetType == "FreeSearch"){
					createFreeSearch(facetName);
					var facettotal = facetObj.facettotal;
					var totalDocuments = facetObj.totalDocuments;
					constructFacetResult(facetObj, facetName, "", null, facettotal, totalDocuments);
				}
			}
			else{
				$('.indexCount').text('(0)');
				$('#INDEX_NAME').hide();
				var facetArr = facetObj[obj];
				for(var i in facetArr){
					 var indexVal = facetArr[i];
					 var countPos = indexVal.indexOf("(");
					 var indexId = indexVal.substr(0, countPos);
					 var indexCount = indexVal.substr(countPos, indexVal.length);
					 $('#'+indexId+" span").text(indexCount);
				}
			}
		}
	}
	$(" #facetUL ul ").css({display: "none"});
}

function constructCurrSelc(){
	var currSelc = "";
	var count = 0;
	for(var i in fqArr){
		var fq = fqArr[i].split(":");
		var fqName = fq[0];
		var fqText = $('#'+fqName).clone().children().remove().end().text();
		if(fqText == " " || fqText==""){
			fqText = $('#'+fqName).find("#facetText").val();
		}		
		var liText = "";
		if(fqArr[i].indexOf('text_') != -1){
			liText = removeTxtParameters(fqArr[i]);
		}
		else if(fqArr[i].indexOf('_splchars') != -1){
			liText = removeFQFieldCode(fqArr[i]);
			liText = getFacetText(liText);
		}
		else if(fqArr[i].indexOf('_td') != -1){
			var fqtext = fqArr[i].replace("_td","");
			liText = getFacetText(fqtext);
		}
		else if(fqArr[i].indexOf('_prefix') != -1){
			var fqtext = fqArr[i].replace("_prefix","");
			liText = getFacetText(fqtext);
		}
		else{
			liText = fqText+":"+fq[1];
		}
		liText = liText.replace(/%/g, "_percentage_");
		liText = decodeURIComponent(liText);
		liText = liText.replace(/_percentage_/g, "%");
		liText = escapeHtml(liText);
		currSelc += "<li><a href='#' onclick='removeFq(this)' ><input type='hidden' value='"+fqArr[i]+"' id='facet' />(x) "+liText+"</a></li>";
		count++;
	}
	if(count > 1){
		currSelc = "<li><a href='#' onclick='removeAllFq()' >(x) Remove All</a></li>" + currSelc;
	}
	if(currSelc != ""){
		var ulStr = "<ul id='currSelcUL'>"+currSelc+"</ul>";
		$('#currentSelc').html("<span id='selcHeader'>Your Selections:</span>"+ulStr);
	}
	else{
		$('#currentSelc').html("Viewing all documents!");
	}
}

function getFacetText(fqtext){
	var fqTextArr = fqtext.split(":");
	var fqName = $('#'+fqTextArr[0]).clone().children().remove().end().text();
	if(fqName == " " || fqName==""){
		fqName = $('#'+fqTextArr[0]).find("#facetText").val();
	}
	var fqVal = fqTextArr[1];
	var liText = fqName+":"+fqVal;
	return liText;
}


function loadIndex(el){
	var selc = $(el).attr("id");
	currIndexText = $(el).clone().children().remove().end().text();
	$('#mainContent').load("TenseADT/indexSearch.html",function(){
		loadData(selc);
		$("#filter-box").hide();
	});
}

function removeFq(elObj){
	var fq = $(elObj).find('#facet').val();
	var pos = $.inArray(fq,fqArr);
	delete fqArr[pos];
	currPage = 1;
	loadLuceneData();
}

function removeTxtParameters(searchVal){
	var searchArr = searchVal.split("*) OR ");
	var firstQ = searchArr[0];
	var firstArr = firstQ.split(" OR ");
	var searchedString = firstArr[1];
	var firstCharacter = searchedString.substring(0,1);
	if(firstCharacter == "*"){
		searchedString = searchedString.substring(1);
	}
	return unescapeSpecialChars(searchedString);
}

function removeFQFieldCode(fqVal){
	var searchArr = fqVal.split("*) OR ");
	var firstQ = searchArr[0];
	var firstArr = firstQ.split(" OR ");
	var searchedString = firstArr[0];
	searchedString = searchedString.replace("_splchars","");
	searchedString = searchedString.replace("("," ");
	return searchedString;
}

function applyLucenePagination(header){
	var rows = $('#noOfRows').val();
	var total = header.total;
	$('#noOfRecords').val(total);
	$('#paginator').pagination({
        items: total,
        itemsOnPage: rows,
        cssStyle: 'light-theme',
        displayedPages: 3,
        currentPage: currPage,
        hrefTextPrefix: "#",
        onPageClick: function(pageNumber){
        	currPage = pageNumber;
        	$('#paginator').pagination('destroy');
        	loadLuceneData();
        },
        onInit: function(){
        	$('#displayRecCount').html("Displaying "+header.start+" to "+header.end+" of "+total);
        }
    });
	
}