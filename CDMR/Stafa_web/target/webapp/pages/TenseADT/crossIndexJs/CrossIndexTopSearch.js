function cleartext(el){
	$(el).val('');
}

function getDataForText(event){
	var key = event.keyCode;
	if(key == 40){
		var classLen = $('.topsearchResultkb').length;
		if(classLen == 0){
			$('#topsearchResult li:first').addClass("topsearchResultkb");
		}
		else{
			var currEl = $('.topsearchResultkb').next("li");
			if($(currEl).html() != null){
				$('.topsearchResultkb').removeClass('topsearchResultkb');
				$(currEl).addClass("topsearchResultkb");
			}
		}
		return false;
	}
	else if(key == 38){
		var classLen = $('.topsearchResultkb').length;
		if(classLen > 0){
			var currEl = $('.topsearchResultkb').prev("li");
			if($(currEl).html() != null){
				$('.topsearchResultkb').removeClass('topsearchResultkb');
				$(currEl).addClass("topsearchResultkb");
			}
		}
		return false;
	}
	else if(key == 13){
		var len = $('.topsearchResultkb').length;
		if(len > 0){
			getTextBoxSelctionValue($('.topsearchResultkb'));
		}
		else{
			$('#topsearchDiv').find('img').triggerHandler('click');
		}
		return false;
	}
	var qVal = $('#query').val();
	qVal = $.trim(qVal);
	qVal = escapeSpecialChars(qVal);
	if(qVal!=""){
		qVal = qVal.toLowerCase();
		var txtboxval = qVal;
		var q_splchar = "text_splchars:("+qVal +" OR *"+qVal+"*)";
		var q_general = "text_general:("+qVal +" OR *"+qVal+"*)";
		var q_rev = "text_rev:("+qVal +" OR *"+qVal+"*)";
		var q_text = "text:("+qVal +" OR *"+qVal+"*)";
		var fullQ = q_splchar+" OR "+q_general+" OR "+q_rev+" OR "+q_text;
		qVal = fullQ;
		var data = facetFq;
		var fqStr = "";
		for(var i in fqArr){
			var fqText = fqArr[i];
			fqText = fqText.replace(/%/g,"_percentage_");
			fqText = encodeURIComponent(fqText);
			fqStr += "&fq="+fqText;
		}
		if(fqStr != ""){
			data += fqStr;
		}
		qVal = qVal.replace(/%/g,"_percentage_");
		qVal = encodeURIComponent(qVal);
		data += "&fq="+qVal+"&page=1&rows=10";
		var uniqueResultArr = [];
		$.ajax({
			url: ipvalue+urlvalue,
			dataType:"jsonp",
			jsonpCallback: 'tenseCallback',
			data: data,
			success: function(data){
				var docs = data.docs;
				for(var i in docs){
					var doc = docs[i];
					for(var key in doc){
						var fieldVal = doc[key];
						var lowCaseFieldval = fieldVal.toLowerCase();
						txtboxval = unescapeSpecialChars(txtboxval);
						if(lowCaseFieldval.indexOf(txtboxval) != -1){
							if($.inArray(fieldVal,uniqueResultArr) == -1){
								uniqueResultArr.push(fieldVal);
							}
						}
					}
				}
				var liStr = "";
				for(var i in uniqueResultArr){
					var value = escapeHtml(uniqueResultArr[i]);
					var tempValue = value;
					var textLength = 60;
					if( tempValue != "" && tempValue.length > textLength)
					{
						tempValue = tempValue.slice(0,textLength);
						tempValue = tempValue+"...";
					}
					liStr += "<li onclick='getTextBoxSelctionValue(this)' title='"+value+"' >"+tempValue+"<input type='hidden' value='"+value+"' /></li>";
				}
				var ulStr = "<ul>"+liStr+"</ul>";
				$('#topsearchResult').html(ulStr);
				$('#topsearchResult').show();
			}
		});
	}
	else{
		$('#topsearchResult').hide();
	}
}

function getTextBoxSelctionValue(elObj){
	$('#topsearchResult').hide();
	var qVal = $(elObj).find(":hidden").val();
	qVal = qVal.toLowerCase();
	qVal = escapeSpecialChars(qVal);
	var q_splchar = "text_splchars:("+qVal +" OR "+qVal+"*)";
	var q_general = "text_general:("+qVal +" OR "+qVal+"*)";
	var q_rev = "text_rev:("+qVal +" OR "+qVal+"*)";
	var q_text = "text:("+qVal +" OR "+qVal+"*)";
	var fullQ = q_splchar+" OR "+q_general+" OR "+q_rev+" OR "+q_text;
	fqArr.push(fullQ);
	loadLuceneData();
}

function getDataForTypedText(){
	$('#topsearchResult').hide();
	var qVal = $('#query').val();
	qVal = $.trim(qVal);
	qVal = escapeSpecialChars(qVal);
	if(qVal != ""){
		qVal = qVal.toLowerCase();
		var q_splchar = "text_splchars:("+qVal +" OR *"+qVal+"*)";
		var q_general = "text_general:("+qVal +" OR *"+qVal+"*)";
		var q_rev = "text_rev:("+qVal +" OR *"+qVal+"*)";
		var q_text = "text:("+qVal +" OR *"+qVal+"*)";
		var fullQ = q_splchar+" OR "+q_general+" OR "+q_rev+" OR "+q_text;
		fqArr.push(fullQ);
		loadLuceneData();
	}
}

function loadSelectedData(docId, indexName){
	selcDocId = docId;
	currIndexText = $('#'+indexName).clone().children().remove().end().text();
	$('#mainContent').load("TenseADT/indexSearch.html",function(){
		loadData(indexName);
	});
}

function clearDefault(){
	currPage = 1;
}