// $Id: Manager.jquery.js,v 1.3 2013/10/18 03:37:29 suma Exp $

/**
 * @see http://wiki.apache.org/solr/SolJSON#JSON_specific_parameters
 */
 //alert("Manager.jquery.js");
AjaxSolr.Manager = AjaxSolr.AbstractManager.extend({
  executeRequest: function () {
    var self = this;
    if (this.proxyUrl) {
	// alert("1"+this.store.string());
      jQuery.post(this.proxyUrl, { query: this.store.string() }, function (data) { self.handleResponse(data); }, 'json');
    }
    else {
    	$('#docs').css({opacity:'1'});
    	if(mrnlabimport==true){
    		var mrnval=$("#slide_donorId").html();
    		mrnval=$.trim(mrnval);
    		if(mrnval!=""){
    		this.store.addByValue("fq", "PATIENT_ID:"+mrnval);
    		}
    		mrnlabimport=false;
    	}
    	if(mrnimport==true){
    		var mrnval=$("#donorMRN").val();
    		mrnval=$.trim(mrnval);
    		if(mrnval!=""){
    		this.store.addByValue("fq", "PATIENT_ID:"+mrnval);
    		}
    		mrnimport=false;
    	}
    	//alert("before mrnlabimport"+mrnlabimport);
    	
		 var storeString = this.store.string();
		 /*
		  * This For loop is Used to Perform AND OR condition
		  * in the CurrentSearch Facet.
		  * */
	   	 for(var i in orArray){
	    	  var orArrval = orArray[i];
	    	  var orFqval = "&fq="+orArrval;
	    	  var encodedOrArrval = encodeURIComponent(orArrval);
	    	  var encodedOrFqval = encodeURIComponent(orArrval);
	    	  encodedOrFqval = "&fq="+encodedOrFqval;
	    	  storeString = storeString.replace(orFqval," OR "+orArrval);
	    	  storeString = storeString.replace(encodedOrFqval," OR "+encodedOrArrval);
	    	 }
		 for(var i in qArray){
     	 	  var qArrval ="q="+qArray[i];
     	  	  storeString = storeString.replace(qArrval,"q=*%3A*");
     		 }
		 try{
			 $.ajax({
				 // url: this.solrUrl + '&' + storeString + '&facet.limit=10&facet.numTerms=true&facet.method=fc&wt=json&json.wrf=?',
				  url: this.solrUrl + '?' + storeString + '&facet.limit=10&facet.numTerms=true&facet.method=fc&wt=json&json.wrf=?',
				  dataType: 'json',
				  success: function(data){
				 	self.handleResponse(data);
				 	construtAccodian();
				 	if(saveSearchObj != null){
				 		getSavedResult(saveSearchObj);
				 	}
			 	  },
			 	 error: function(jqXHR, textStatus, errorThrown){
			 		 var responseTxt = jqXHR.responseText;
			 		 alert("Error:"+responseTxt);
			 	  }
				});
		 }
		 catch(Err){
			 alert(err);
		 }
      }
  }
});

function construtAccodian(){
	$('#singleIndexDiv').accordion({
 		collapsible: true
 	});
 	$('#singleIndexDiv h3').click(function(){
 		 $('.singleIndexMenuli').hide();
 		$('#docs').css({opacity:'1'});
 	});
	$("#singleIndexDiv li a").show();
	$(".groupUL ul ").css({display: "none"});
	$(".groupUL > li").hover(function(el){
		  $('#topsearchResult').hide();
		  $('.singleIndexMenuli').hide();
		  $(this).find('ul:first').css({visibility: "visible",display: "inline",top:"0","z-index": "1003"});
		  $(this).find('ul:first').addClass("singleIndexMenuli");
		  $('#docs').css({opacity:'0.3'});
		},
		function(){
			//$(this).find('ul:first').css({visibility: "hidden"});
	});

	$('#menuregion').hide();
	$('.accrodionDiv').css({
		height: "auto"
	});

	$('.groupUL img').live("click",function(){
		$('#docs').css({opacity:'1'});
	});
}
