(function ($) {

	AjaxSolr.MenuListWidget = AjaxSolr.AbstractWidget.extend({
	    beforeRequest: function () {

	        var currentPosition = 0;
	        var self = this;
	        var menuItems=new Array();
	        var divIds=new Array();
	        for(var i=0;i<self.fieldsNames.length;i++) {
	            menuItems[i]=self.fieldsNames[i];
	            divIds[i]=self.divnames[i];
	        }

	        var menuLength=menuItems.length;
	         $(self.target).empty();

	         if(menuLength > 0){
	             $(self.target).append('<li><span id="menuScrollup" style="text-align: center; cursor: pointer;"></span></li>');
	             for (var i = 0; i < menuItems.length; i++) {
	                 var fieldName = self.fieldsNames[i];


	                 var divName=self.divnames[i];
	                 var fname = fieldName;
	                 fname = fname.replace(/(<br\/)>/g,'');
	                 if(fieldName.length>=20) {
	                     var shortFieldName=fname.slice(0,20);
	                     shortFieldName += "...";
	                     $(self.target).append('<li><a href="#" class="listSelection menuHighLight" id=menuLink'+i+' style="display:block;" title="'+fieldName+'">'+shortFieldName+'<input type="hidden" id="facetText" value="'+fieldName+'" /></a><ul><div class="divClass" id="'+divName+'"></div></ul> </li>');
	                 } else {
	                     $(self.target).append('<li><a href="#" class="listSelection" id=menuLink'+i+' style="display:block;">'+fieldName+'<input type="hidden" id="facetText" value="'+fieldName+'" /></a><ul><div class="divClass" id="'+divName+'"></div></ul> </li>');
	                 }

	             }
	             for(var i=8;i<menuItems.length;i++) {
	                 $('#menuLink'+i).css('display','none');
	             }
	             $(self.target).append('<li><span id="menuScrolldown" style="text-align: center; cursor: pointer"></span></li><input type="hidden" id="facetLength" name='+menuLength+'>');

	             $('#menuScrollup').css("background-color", "#A9A9A9" );

	             $('.listSelection').mouseenter(function(){
	                 $('#query').val("");
	                 $('#query').blur();
	                 $('#topsearchResult').css('display','none');
	                 $('.listSelection').css("background-color", "");
	                 //$(this).css("background-color", "#a1d0ef");
	            });

	            var tooltiplen = $('.menuHighLight_tooltip').length;
	            if(tooltiplen == 0){
	            	 $('.menuHighLight').tooltip({
		                    track: true,
		                    delay: 0,
		                    showURL: false
		            });
		            $('.menuHighLight').addClass('menuHighLight_tooltip');
	            }
	         }
	    }
	  });
	})(jQuery);