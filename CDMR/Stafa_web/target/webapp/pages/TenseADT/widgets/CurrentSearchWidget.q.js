var qflag;
var selfObj;
var orArray = [];
var qArray = [];
var datefieldArr = [];
var qsave;
var fqSave = "";
var listIndex;
var recorsPerPage;
var flag =0;
(function ($) {

AjaxSolr.CurrentSearchWidget = AjaxSolr.AbstractWidget.extend({
  afterRequest: function () {
    var self = this;
	selfObj = self;
    var links = [];
    var q = this.manager.store.get('q').val();
    qsave=q;
    var position=0;
    fqSave="";
    var fq = this.manager.store.values('fq');
    for(i=0;i<fq.length;i++){
    	if(fqSave!=""){
    		fqSave = fqSave+",!|";
    	}
    	fqSave=fqSave+fq[i];
    }
    var flagAndOr = conditionANDORObj[0].childNodes[0].nodeValue;
    if(typeof(listIndex)!='undefined'){
   	 q=qsavedvalues[listIndex];
   	 qflag=gloqval[listIndex];
   	 var fqlist="";
   	 if(fqsavedvalues){
   		fqlist=""+fqsavedvalues[listIndex];
   		fq=fqlist.split(",!|");
   	}
   	listIndex='undefined';
   }
    var varlength=fq.length;
    if (q != '*:*' && typeof(qflag)!='undefined' && varlength==0){
    	var aLink = $('<a href="#" title="'+removeCaseCode(q)+'"/>').text('(x) ' + removeCaseCode(q)).click(function () {
    		qflag='undefined';
        	self.manager.store.get('q').val('*:*');
        	qArray = [];
        	self.manager.doRequest(0);
        	return false;
    		});
		
		 if (links.length > 0 && flagAndOr=='true') {
			var options = "<label>AND <input type='radio' id='radio_and' checked='true' name='"+q+"' onclick='changeQValAnd(this)' value='AND' /> </label>" 
						  +"<label>OR <input type='radio' id='radio_or' name='"+q+"' onclick='changeQValOR(this)' value='OR' /> </label>"; 
			links.push($("<span>&nbsp;&nbsp;"+options+"&nbsp;&nbsp;</span>").append(aLink));
		 }
		 else{
			 links.push($("<span>&nbsp;</span>").append(aLink));
		 }
		 
    	q='*:*';
    }
    for (var i = 0,l = fq.length; i<l; i++) {
    	var fieldIdAndName = "";
		var fieldArraySingleValue = fq[i].split(":");
		var field ="";
		var fieldValue = fieldIdAndName;
		var dateTitle = "";
		if(fieldArraySingleValue.length > 0)
		{
			field = fieldArraySingleValue[0].replace('_td','');
			if(field != null && field != "")
			{
				field = field.replace(/_/g, ' ');
				if(field.match(/\sf$/)){
					field = field.substring(0,((field.length)-2) );
				}

				fieldValue = field+": ";
				dateTitle = fieldValue;
				for( var j=1; j < fieldArraySingleValue.length; j++)
				{
					fieldValue = fieldValue+fieldArraySingleValue[j];
				}
				
			}
		}	
		
		
		position++;
		if (q != '*:*' && i==qflag){
			var aLink = $('<a href="#" title="'+removeCaseCode(q)+'"/>').text('(x) ' + removeCaseCode(q)).click(function () {
	    		qflag='undefined';
	    	self.manager.store.get('q').val('*:*');
	    	qArray = [];
	    	self.manager.doRequest(0);
	    	return false;
			});

			 if (links.length > 0 && flagAndOr=='true') {
				var options = "<label>AND <input type='radio' id='radio_and' checked='true' name='"+q+"' onclick='changeQValAnd(this)' value='AND' /> </label>" 
							  +"<label>OR <input type='radio' id='radio_or' name='"+q+"' onclick='changeQValOR(this)' value='OR' /> </label>"; 
				links.push($("<span>&nbsp;&nbsp;"+options+"&nbsp;&nbsp;</span>").append(aLink));
			 }
			 else{
				 links.push($("<span>&nbsp;</span>").append(aLink));
			 }
			 
	    	q='*:*';
	    }
		
		var fullField = fq[i];
		var fieldArr = fullField.split(":");
		var dateFieldValue = "";
		var ageFieldValue="";
		/* Added For the Date Formation to display in Search header Tag*/
		if( fullField.indexOf("[") != -1)
		{
			var widgetObj =  moduleObj[0].getElementsByTagName(widgetTag);
			for(j=5;j<widgetObj.length;j++){
				if(fieldArr[0] == widgetObj[j].attributes[2].nodeValue && widgetObj[j].attributes[3].nodeValue == "DateSearch"){
					datefieldArr.push(fieldArr[0]);
					var startI = fullField.indexOf("[");
					if(startI != -1)
					{
						var endI = fullField.indexOf("]");
						var dateString = fullField.substring(startI + 1, endI);
						var dateArr = dateString.split("TO");
						var startDate = dateArr[0];
						var endDate = dateArr[1];
						if(startDate!="* "){
							dateFieldValue = removeTimeStamp(startDate);
						}
						else{
							dateFieldValue = "*";
						}
						dateFieldValue = dateFieldValue + " TO ";
						if(endDate!=" *"){
							dateFieldValue = dateFieldValue + removeTimeStamp(endDate);
						}
						else{
							dateFieldValue = dateFieldValue + "*";
						}
						fieldValue = dateTitle + dateFieldValue;
					}
				}
				else{
					falg = 0;
				}
			}
		}
		
		var fieldValuelength = fieldValue.length;
		if(fieldValue.charAt(fieldValuelength-2) == "\\" && fieldValue.charAt(fieldValuelength-3) == "\\" )
		{
			fieldValue=fieldValue.replace(fieldValue.charAt(fieldValuelength-2), '');
		}
		
		if(fieldValue.indexOf("splchars")!=-1){
			fieldValue = removeFQcaseCode(fieldValue);
		}
		if(fieldValue.search("text:")!=-1){
			fieldValue = fieldValue.replace("text:","");
		}
		if(fieldValue.indexOf(" ci") != -1){
			fieldValue = fieldValue.replace(/ ci|\(|\)|\*|\\/g,"");
		}
		var tempFieldValue = fieldValue.split(":");
		
		for(var j=0;j<tagObj.length;j++) {
			if(tempFieldValue[0].replace(/ /g,'_')==tagObj[j].attributes[0].nodeValue) {
				fieldValue = tagObj[j].childNodes[0].nodeValue + ": "+tempFieldValue[1];
			}
		}
		
		/* SPLHANDLING */
		fieldValue = unescapeSpecialChars(fieldValue); 

		var aLink = $('<a href="#" title="'+fieldValue+'"/>').text('(x) ' + fieldValue).click(self.removeFacet(fq[i],i));

		if (links.length > 0 && flagAndOr=='true') {
			var options = "<label>AND <input type='radio' id='radio_and' checked='true' name='"+fq[i]+"' onclick='changeQueryAnd(this)' value='AND' /> </label>" 
						  +"<label>OR <input type='radio' id='radio_or' name='"+fq[i]+"' onclick='changeQueryOR(this)' value='OR' /> </label>"; 
			links.push($("<span>&nbsp;&nbsp;"+options+"&nbsp;&nbsp;</span>").append(aLink));
		 }
		 else{
			 links.push($("<span>&nbsp;</span>").append(aLink));
		 }
		
     }
    if (q != '*:*' && qflag>=fq.length){
    	var aLink = $('<a href="#" title="'+removeCaseCode(q)+'"/>').text('(x) ' + removeCaseCode(q)).click(function () {
    		qflag='undefined';
    	self.manager.store.get('q').val('*:*');
    	qArray = [];
    	self.manager.doRequest(0);
    	return false;
		});
    	 if (links.length > 0 && flagAndOr=='true') {
				var options = "<label>AND <input type='radio' id='radio_and' checked='true' name='"+q+"' onclick='changeQValAnd(this)' value='AND' /> </label>" 
							  +"<label>OR <input type='radio' id='radio_or' name='"+q+"' onclick='changeQValOR(this)' value='OR' /> </label>"; 
				links.push($("<span>&nbsp;&nbsp;"+options+"&nbsp;&nbsp;</span>").append(aLink));
		 }
		 else{
				 links.push($("<span>&nbsp;</span>").append(aLink));
		 }
    	q='*:*';
    }
    if (q != '*:*'&& typeof(qflag)=='undefined'){
    	if(fq.length==0){
    		qflag=0;	
    	}else{
    		qflag=position;	
    	}
    	var aLink = $('<a href="#" title="'+removeCaseCode(q)+'"/>').text('(x) ' + removeCaseCode(q)).click(function () {
    	qflag='undefined';
    	self.manager.store.get('q').val('*:*');
    	qArray = [];
    	self.manager.doRequest(0);
    	return false;
		});
    	 if (links.length > 0 && flagAndOr=='true') {
				var options = "<label>AND <input type='radio' id='radio_and' checked='true' name='"+q+"' onclick='changeQValAnd(this)' value='AND' /> </label>" 
							  +"<label>OR <input type='radio' id='radio_or' name='"+q+"' onclick='changeQValOR(this)' value='OR' /> </label>"; 
				links.push($("<span>&nbsp;&nbsp;"+options+"&nbsp;&nbsp;</span>").append(aLink));
		 }
		 else{
				 links.push($("<span>&nbsp;</span>").append(aLink));
		 }
		 q='*:*';
    	}
    

    if (links.length > 1) {
      links.unshift($('<a href="#" title="Remove All"/>').text('Remove All').click(function () {
    	  qflag='undefined';
        self.manager.store.get('q').val('*:*');
        self.manager.store.remove('fq');
        orArray = [];
        qArray = [];
        datefieldArr = [];
        self.manager.doRequest(0);
        return false;
      }));
    }
    if (links.length) {
      AjaxSolr.theme('list_items', this.target, links);
	  	for(var i in orArray){
		  	  var orArrval = decodeURIComponent(orArray[i]);
		  	  $("input[name='"+orArrval+"']#radio_or").prop("checked",true);
		}
		for(var i in qArray){
		  	  var qArrayVal = qArray[i];
		  	  $("input[name='"+qArrayVal+"']#radio_or").prop("checked",true);
		}
		$(this.target).prepend("<li><button type='button' id='selctedIndexbutton' >(x) "+currIndexText+"</button></li>");
    }
    else {
      $(this.target).html("<div><button type='button' id='selctedIndexbutton' >(x) "+currIndexText+"</button></div>");
    }
    $('#selctedIndexbutton').click(function(){
    	$('#printDialog').remove();
    	$('#exportDialog').remove();
    	$('#saverSearchDialogDiv').remove();
    	$('#pivotDialogDiv').remove();
    	$('#chartDialogDiv').remove();
    	remvAllElemFromComp();
     	$('#mainContent').load("TenseADT/generalSearch.html",function(){
     		loadLuceneSearchScreen();
    	});
    });
  },

  removeFacet: function (facet,index) {
    var self = this;
    return function () {
    orArray = jQuery.grep(orArray, function(value) {
	   		return value != encodeURIComponent(facet);
    		});
     if (self.manager.store.removeByValue('fq', facet)) {
    	if(qflag!=0 && qflag>index){
    	    qflag=qflag-1;
    	    }
    	  var fq = self.manager.store.values('fq');
    	    if(fq.length==1){
    	    	orArray = [];
    	    }
    	    self.manager.doRequest(0);
      }
      return false;
    };
    
  }
});
orArray=[];
qArray =[];
})(jQuery);


function changeQueryOR(el){
	 var fieldValue = $(el).attr("name");
	 if(fieldValue.indexOf("splchars") != -1){
		 orArray.push(fieldValue);
	 }
	 else{
		 var fieldArr =  fieldValue.split(":");
		 //Added for the date
		 for(m=0; m<datefieldArr.length; m++){
			 if( fieldArr[0] == datefieldArr[m]){
				 var startI = fieldValue.indexOf("[");
				 fieldArr[1] = fieldValue.substring(startI);
			 }
		 }
		
		 var constrUrl = ":"+fieldArr[1];
		 var asciiChar = fieldArr[0]+encodeURIComponent(constrUrl);
		 orArray.push(asciiChar);
	 }
	 selfObj.manager.doRequest(0);
}

function changeQueryAnd(el){
	 var fieldValue = $(el).attr("name");
	 if(fieldValue.indexOf("splchars") != -1){
		 orArray = jQuery.grep(orArray, function(value) {
		   		return value != fieldValue;
		  });
	 }
	 else{
		 var fieldArr =  fieldValue.split(":");
		 // Added for the Date
		 for(m=0; m<datefieldArr.length; m++){
			 if( fieldArr[0] == datefieldArr[m]){
				 var startI = fieldValue.indexOf("[");
				 fieldArr[1] = fieldValue.substring(startI);
			 }
		 }
		 
		 var constrUrl = ":"+fieldArr[1];
		 var asciiChar = fieldArr[0]+encodeURIComponent(constrUrl);
		 orArray = jQuery.grep(orArray, function(value) {
		   		return value != asciiChar;
		  });
	 }
	 selfObj.manager.doRequest(0);
}

function changeQValAnd(el){
	 var fieldValue = $(el).attr("name");
	 qArray = [];
	 selfObj.manager.doRequest(0);
}

function changeQValOR(el){
	 var fieldValue = $(el).attr("name");
	 qArray.push(fieldValue);
	 selfObj.manager.doRequest(0);
}

function formatUSDate(date){
	 var dateStr = $.trim(date); 
	 var dateArr = dateStr.split("T");
	 var newdateArr=dateArr[0].split("-");
	 var year = newdateArr[0];
	 var month = newdateArr[1];
	 var day = newdateArr[2]; 
	 var fullDate = month+"/"+day+"/"+year;
	 return fullDate;
}
 
function removeTimeStamp(date){
	 var dateStr = $.trim(date); 
	 var dateArr = dateStr.split("T");
	 return dateArr[0];
}

function getAge(date){
	 var newdate=new Date();
	 var curyear = newdate.getFullYear();
	 var agedate=new Date(formatUSDate(date));
	 var yearDOB = agedate.getFullYear();
	 var age=curyear-yearDOB-1;
	 return age;
}
function getAgeDOB(date){
	 var age=getAge(date)+1;
	 return age;
}
function showRecords(){
	recorsPerPage=$('#recorsPerPage').val();
	Manager.store.addByValue('start',0);
	Manager.store.addByValue('rows',recorsPerPage);
	Manager.doRequest();
}
/*
 * This Function is Used To Remove the Additional Parameters 
 * added in Query(q) to solve the ignore-case and Special
 * characters Issue.
 * */
function removeCaseCode(qVal){
	var searchVal = decodeURIComponent(qVal);
	var searchArr = searchVal.split("*) OR ");
	var firstQ = searchArr[0];
	var firstArr = firstQ.split(" OR ");
	var searchedString = firstArr[1];
	return searchedString;
}
/*
 * This Function is Used To Remove the Additional Parameters 
 * added in Field Query(fq) to solve the ignore-case and Special
 * characters Issue.
 * */
function removeFQcaseCode(fqVal){
	fqVal = fqVal.replace(/%/g, "_percentage_");
	var searchVal = decodeURIComponent(fqVal);
	searchVal = searchVal.replace(/_percentage_/g, "%");
	var searchArr = searchVal.split("*) OR ");
	var firstQ = searchArr[0];
	var firstArr = firstQ.split(" OR ");
	var searchedString = firstArr[0];
	searchedString = searchedString.replace(" splchars","");
	searchedString = searchedString.replace("("," ");
	return searchedString;
}
