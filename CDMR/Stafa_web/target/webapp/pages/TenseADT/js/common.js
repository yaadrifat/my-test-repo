/*
 * Function added for shrink/expand the Facet Menu
*/
var facetMenu_showhideFlag = 0;
function facetMenu_showhide(){
	if(facetMenu_showhideFlag){
		$('#singleIndexDiv').css('display', 'block');
		$('div.right').css('width', '78%');
		$('#menuShrinkButton').removeClass('menuexpand');
		$('#menuShrinkButton').addClass('menushrink');
		facetMenu_showhideFlag = 0;
	}
	else{
		$('#singleIndexDiv').css('display', 'none');
		$('div.right').css('width', '98.5%');
		$('#menuShrinkButton').removeClass('menushrink');
		$('#menuShrinkButton').addClass('menuexpand');
		facetMenu_showhideFlag = 1;
	}
}
//	Facet Menu shrink/expand end

var crossIndexHome_showhideFlag = 0;
function crossIndexHomeMenu_showhide(){
	if(crossIndexHome_showhideFlag){
		$('.facetContent').find('div').css('display', 'block');
		$('#menuShrinkButton').removeClass('menuexpand');
		$('#menuShrinkButton').addClass('menushrink');
		crossIndexHome_showhideFlag = 0;
	}
	else{
		$('.facetContent').find('div').css('display', 'none');
		$('#menuShrinkButton').removeClass('menushrink');
		$('#menuShrinkButton').addClass('menuexpand');
		crossIndexHome_showhideFlag = 1;
	}
}


function formatDate(dateFormat,datetime)
{
//Object.prototype.toString.call(date) === '[object Date]'

	if(datetime != null && datetime !="" && datetime !="" && dateFormat != "")
	{
		var newDate = datetime;
		//alert(Object.prototype.toString.call(newDate));
		if(Object.prototype.toString.call(newDate) === '[object String]')
		{
			//alert(datetime);
			if(newDate != " ")
				return(dateFormate(dateFormat, newDate));
		}
	}
	return datetime;
}

function dateFormate(dateFormat, datetime) {
    return $.datepicker.formatDate(dateFormat, new Date(datetime));
};

function endingWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}
