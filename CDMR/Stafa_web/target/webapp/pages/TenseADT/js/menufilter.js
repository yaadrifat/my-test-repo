var facetOffset=10;
var facetSort="index";
var topSearchObj;
var recordlen;
function loadMenuFilter(selfobj){
	$(selfobj.target).empty();
	$(selfobj.target).addClass("menuFilterulContentDiv");
	var fieldId = $(selfobj.target).attr("id");
	createMenuFilter(fieldId,selfobj);
}
/*
 * This function is used to Create Facet Menu Filter.
 * This function will append the menu filter div in the Given UL id.
 * And it will contain the logic to show and hide menus.
 * */
function createMenuFilter(elDiv, selfobj){
	var elDivObj = $("#"+elDiv);
	var staticHtml = $('#menuFilter').html();
	$(elDivObj).html(staticHtml);
	var facetname = selfobj.field;
	$(elDivObj).find('#facetname').val(facetname);
	$.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase());
	if($.browser.chrome || $.browser.safari){
		$('.alpha-nav button').css('margin-left','-1px');
	}
	$(elDivObj).find('#search').click(function(){
		var txtBoxVal = $.trim($(elDivObj).find(":text").val());
		var olEle = $(elDivObj).find('#selectedOL');
		var checkboxSelec = $(olEle).find("li").length;
		if(typeof(checkboxSelec)!='undefined'&&checkboxSelec!=0){
			var selec = "";
			var isMultiple = false;
			$(olEle).find("li").each(function(i,el){
				if(selec!=""){
					selec = selec +" OR ";
					isMultiple = true;
				}
				var selVal = $(el).find(":hidden").prop("value");
				if(selVal.charAt((selVal.length-1)) == '\\')
				{
					selVal = selVal.replace(selVal.charAt(selVal.length-1), "\\\\");
				}
				selec = selec +'"'+selVal+'"';
			});
			if(isMultiple){
				selec = "("+selec+")";
			}
			selfobj.manager.store.addByValue('fq', selfobj.field + ':' +selec);
			$(elDivObj).find("#close").triggerHandler('click');
			selfobj.manager.doRequest(0);
		}
		else if(txtBoxVal!=""){
			var tempVal = txtBoxVal;
			tempVal = tempVal.toLowerCase();
			tempVal = escapeSpecialChars(tempVal);

			var facetname = selfobj.field;
			var fq_splchar = facetname+"_splchars:("+tempVal+" OR *"+tempVal+"*)";
			var fq_general = facetname+"_general:("+tempVal+" OR *"+tempVal+"*)";
			var fq_text = facetname+"_t:("+tempVal+" OR *"+tempVal+"*)";
			var fullFQ = fq_splchar+" OR "+fq_general+" OR "+fq_text;
			selfobj.manager.store.addByValue('fq', fullFQ);
			$(elDivObj).find("#close").triggerHandler('click');
			selfobj.manager.doRequest(0);
		}
		else{
			$(elDivObj).find("#close").triggerHandler('click');
		}
	});

	$(elDivObj).find(":text").keyup(function(eve){
		var txtVal = $.trim($(elDivObj).find(":text").val());
		if(txtVal==""){
			var activeLetter = $(elDivObj).find(".active").text();
			if(activeLetter != 'All'){
				txtVal = activeLetter;
			}
			fnGetResult(txtVal, $('.active'));
		}
		else{
			fnGetResultForTextBoxVal(txtVal, this, "");
		}

		removeHiddenList(elDivObj);
		if(eve.which == 13){
			$(elDivObj).find('#search').triggerHandler("click");
		}
	});



	$(elDivObj).find("#close").click(function(eve){
		$(elDivObj).closest("ul").attr("style","display:none;");
		$(elDivObj).closest("li").unbind('mouseleave');
		$('#result_table #docs').css({opacity:'1'});
	});

	$(document).click(function(e){
		if($(e.target).closest("#query").length) {
			$(elDivObj).closest('#nav').find('ul:first').css({"display":"none","visibility":" hidden"});
		}
		else {
			return;
		}
	});


}
/*
 * This Function is Used to Display the Constructed data by the
 * function fnCreateItem() for the Menu value enter by the User.
 * */
function fnDisplayResult(data, facetname, baseUrl, currentOffset, elDivObj)
{
	var idResultCount = $(elDivObj).find('#resultcount');
	var idText =  $(elDivObj).find('#menuFilterData');
	var objectedItems = [];
	var obj = data.facet_counts.facet_fields[facetname];
	if(obj==null||obj=='null'||typeof(obj)=='undefined'){
		var resultString = "Current result set: 0 documents";
		$(idResultCount).empty();
		$(idResultCount).append(resultString);
		$(idText).empty();
		$(elDivObj).find("#menuFilterContent").css({'margin-bottom':'20px'});
		return false;
	}

	if(typeof(obj.length) == 'undefined'){
		for(var i in obj){
			objectedItems.push({ facet: i, count: obj[i] });
		}
	}
	else{
		for (var i=0; i < (obj.length/2); i++) {
			var facet = obj[2*i];
			var count = parseInt(obj[2*i+1]);
			objectedItems.push({ facet: facet, count: count });
		}
	}

	$(elDivObj).find("#menuFilterContent").css({'margin-bottom':'0px'});
	var resultcount = data.response.numFound;
	$(idResultCount).empty();
	var resultString = "Current result set: "+resultcount+" documents";
	$(idResultCount).append(resultString);

	$(idText).empty();
	recordlen = objectedItems.length;
	for (var i = 0, l = objectedItems.length; i < l; i++) {
		var facet = objectedItems[i].facet;
		$(idText).append(fnCreateItem(facet, parseInt(objectedItems[i].count ), elDivObj));
	}

	var facettotal = data.facet_counts.facet_numTerms[facetname];
	var pagerName = $(elDivObj).attr("id")+"_paginator";
	var pagerLen = $('#'+pagerName).length;
	if(pagerLen == 0){
		$(elDivObj).append("<div id='"+pagerName+"' class='facet_paginator' align='center'></div>");
		if(facettotal > 10){
			$('#'+pagerName).pagination({
		        items: facettotal,
		        itemsOnPage: 10,
		        cssStyle: 'light-theme',
		        displayedPages: 3,
		        currentPage: 1,
		        hrefTextPrefix: "#",
		        onPageClick: function(pageNumber){
		        	fnGetPaginationResult(facetname, baseUrl, currentOffset, elDivObj, pageNumber-1);
		        }
		    });
		}
	}

}
/*
 * This Function is Used to Construct the Data Return from the solr
 * for the Menu value enter by the User.
 * */
function fnCreateItem(value, weight, elDivObj)
{
	value = escapeHtml(value);
	var tempValue = value;
	var textLength = 30;
	if( value != null && value != "" && value.length > textLength)
	{
		tempValue = value.slice(0,textLength);
		tempValue = tempValue+"...";
	}
	var checked  = "";
	var olEle = $(elDivObj).find('#selectedOL');
	$(olEle).find("li").each(function(i, el){
		var elVal = $(el).find(":hidden").prop("value");
		if(value == elVal){
			checked = "checked";
			return false;
		}
	});

	var chckboxel='<input type="checkbox" value="'+value+'" onclick="removeSelec(this)" '+checked+' />';
	var output = $('<li>'+chckboxel+'<button type="button" class="selection" title="'+value+'"  >'+"<b>"+tempValue+' ('+weight+')'+'</b></button></li>').addClass('tagcloud_size_' + weight).click(function(){
		var isChecked = $(this).find("input").prop("checked");
		if(isChecked){
			$(this).find("input").prop("checked", false);
		}
		else{
			$(this).find("input").prop("checked", true);
		}
		setSelectedValues(this);
	});
	return output;
}
/*
 * This function will get the Results For A-Z alphabets in
 * the AutoComplete Menu Facet.
 * */
function fnGetResult(value, el)
{

	var menuName = $(el).closest('.menuFilterulContentDiv').attr("id");

	var isButtonClick = $(el).is("button");
	$('#'+menuName+' .addFontWeight').css({'font-weight':'normal',"font-size":"14px"});

	if(isButtonClick){
		$(el).css({'font-weight':'bold',"font-size":"18px"});
		$(el).addClass('addFontWeight');
		$('#'+menuName+' .active').removeClass('active');
		$(el).addClass('active');
	}

	var fields = topSearchObj.fields;
	var fieldStr = "";
	for(var i in fields){
		fieldStr = fieldStr +"&facet.field="+fields[i];
	}
	var fq = topSearchObj.manager.store.values('fq');
	var fqvalStr = "";
	for(var i in fq){
		fqvalStr = fqvalStr +"&fq="+encodeURIComponent(fq[i]);
	}
	var qVal = topSearchObj.manager.store.values('q');
	var facetname = $(el).closest('#menuFilterContent').find('#facetname').val();
	var searchCriteria = "&fq="+facetname+":("+value+"* OR "+value.toLowerCase()+"*)";
	var startUrl = ipvalue+urlObj[0].childNodes[0].nodeValue;
	var numTerms = "&f."+facetname+".facet.numTerms=true";	// numTerms is added for Pagination Total count
	var url = startUrl+"?facet=true&wt=json&rows=0&facet.mincount=1&json.wrf=?&facet.limit=10&q="+qVal+fieldStr+numTerms;
	url=url+"&facet.offset=0"+searchCriteria;
	url=url+"&facet.sort="+facetSort+fqvalStr;
	for(var i in orArray){
		var orArrval = orArray[i];
		var orFqval = "&fq="+orArrval;
		url = url.replace(orFqval,"OR "+orArrval);
	}
	var baseUrl=url;
	var currentOffset = 0;
	var elDivObj = $(el).closest('.menuFilterulContentDiv');

	var pagerName = $(elDivObj).attr('id')+"_paginator";
	var pagerLen = $('#'+pagerName).length;
	if(pagerLen != 0){
		$(elDivObj).find("#selectedOL li").remove();
		$('#'+pagerName).pagination('destroy');
		$('#'+pagerName).remove();
	}

	$.ajax({
		url: url,
		dataType: 'json',
		success: function(data) {
			removeHiddenList(elDivObj);
			fnDisplayResult(data, facetname, baseUrl, currentOffset, elDivObj);
		}
	});
}
/*
 * This function is used to Get the Results For user Keypress in Menu Facet.
 * */
function fnGetResultForTextBoxVal(value, el, searchQuery)
{
	var fields = topSearchObj.fields;
	var fieldStr = "";
	for(var i in fields){
		fieldStr = fieldStr +"&facet.field="+fields[i];
	}
	var fq = topSearchObj.manager.store.values('fq');
	var fqvalStr = "";
	for(var i in fq){
		fqvalStr = fqvalStr +"&fq="+encodeURIComponent(fq[i]);
	}
	var qVal = topSearchObj.manager.store.values('q');
	var facetname = $(el).closest('#menuFilterContent').find('#facetname').val();
	var tempVal = value;
	var fqStr = "";
	if(tempVal!="" && searchQuery == ""){
		tempVal = $.trim(tempVal);
		tempVal = tempVal.toLowerCase();
		tempVal = escapeSpecialChars(tempVal);

		var fq_splchar = facetname+"_splchars:("+tempVal+" OR *"+tempVal+"*)";
		var fq_general = facetname+"_general:("+tempVal+" OR *"+tempVal+"*)";
		var fq_text = facetname+"_t:("+tempVal+" OR *"+tempVal+"*)";
		var fullFQ = fq_splchar+" OR "+fq_general+" OR "+fq_text;
		fullFQ = encodeURIComponent(fullFQ);
		fqStr = "&fq="+fullFQ;
	}
	else{
		fqStr = searchQuery;
	}
	var startUrl = ipvalue+urlObj[0].childNodes[0].nodeValue;
	var numTerms = "&f."+facetname+".facet.numTerms=true";	// numTerms is added for Pagination Total count
	var url = startUrl+"?facet=true&wt=json&rows=0&facet.mincount=1&json.wrf=?&facet.limit=10&q="+qVal+fieldStr+numTerms;
	url=url+"&facet.offset=0"+fqStr;
	url=url+"&facet.sort="+facetSort+fqvalStr;
	for(var i in orArray){
		var orArrval = orArray[i];
		var orFqval = "&fq="+orArrval;
		url = url.replace(orFqval,"OR "+orArrval);
	}
	var baseUrl=url;
	var currentOffset = 0;
	var elDivObj = $(el).closest('.menuFilterulContentDiv');

	var pagerName = $(elDivObj).attr('id')+"_paginator";
	var pagerLen = $('#'+pagerName).length;
	if(pagerLen != 0){
		$(elDivObj).find("#selectedOL li").remove();
		$('#'+pagerName).pagination('destroy');
		$('#'+pagerName).remove();
	}

	$.ajax({
		url: url,
		dataType: 'json',
		success: function(data) {
			fnDisplayResult(data, facetname, baseUrl, currentOffset, elDivObj);
		}
	});
}

function fnGetPaginationResult(facetname, baseUrl, cOffset, elDivObj, elementValue){
	if(baseUrl == ""){
		var fields = topSearchObj.fields;
		var fieldStr = "";
		for(var i in fields){
			fieldStr = fieldStr +"&facet.field="+fields[i];
		}
		var fq = topSearchObj.manager.store.values('fq');
		var fqvalStr = "";
		for(var i in fq){
			fqvalStr = fqvalStr +"&fq="+encodeURIComponent(fq[i]);
		}
		var searchCriteria = "&fq="+facetname+":(*)";
		var startUrl = ipvalue+urlObj[0].childNodes[0].nodeValue;
		var numTerms = "&f."+facetname+".facet.numTerms=true";	// numTerms is added for Pagination Total count
		var url = startUrl+"?facet=true&wt=json&rows=0&facet.mincount=1&json.wrf=?&facet.limit=10&q=*:*"+fieldStr+numTerms;
		url=url+"&facet.offset=0"+searchCriteria;
		url=url+"&facet.sort="+facetSort+fqvalStr;
		for(var i in orArray){
			var orArrval = orArray[i];
			var orFqval = "&fq="+orArrval;
			url = url.replace(orFqval,"OR "+orArrval);
		}
		baseUrl=url;
	}

	var offset = elementValue * 10;
	baseUrl = baseUrl.replace("&facet.offset="+cOffset, "&facet.offset="+offset);
	var url= baseUrl;
	$.ajax({
		url: url,
		dataType: 'json',
		success: function(data) {
			fnDisplayResult(data, facetname, baseUrl, offset, elDivObj);
		}
	});
}

function removeSelec(el){
	var selec = $(el).prop("checked");
	if(selec){
		$(el).prop("checked",false);
	}
	else{
		$(el).prop("checked",true);
	}
}
/*
 * This function is used to maintain the User Menu selection
 * values when doing PREVIOUS and NEXT selection(Pagination).
 * */
function setSelectedValues(el){
	var checkbox = $(el).find(":checkbox");
	var isChecked = $(checkbox).prop("checked");
	var elDivObj = $(el).closest('.menuFilterulContentDiv');
	var olEle = $(elDivObj).find('#selectedOL');
	var selecVal = $(checkbox).prop("value");
	if(isChecked){
		var liStr = "<li><input type='hidden' value='"+selecVal+"' /></li>";
		$(olEle).append(liStr);
	}
	else{
		$(olEle).find("li").each(function(i, el){
			var elVal = $(el).find(":hidden").prop("value");
			if(elVal == selecVal){
				$(el).remove();
			}
		});
	}
}

// function is added for the alphabet facet search
function removeHiddenList(elDivObj){

    var selectedOLObj = $(elDivObj).find('#selectedOL');
    var checkboxSelec = $(selectedOLObj).find("li").length;
    if(checkboxSelec != 0){
	$(selectedOLObj).empty();
    }
}