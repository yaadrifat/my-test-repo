<!--  <link rel="stylesheet" type="text/css" href="TenseADT/css/TenseADT.css"  />-->

	<script type="text/javascript" src="TenseADT/crossIndexJs/XMLreader.js"></script>
	<script type='text/javascript' src='TenseADT/menu/menu.js'></script>
	<script type="text/javascript" src="TenseADT/js/declaration.js"></script>
	<script type="text/javascript" src="TenseADT/js/search.js"></script>
	<script type="text/javascript" src="TenseADT/lib/core/Core.js"></script>
	<script type="text/javascript" src="TenseADT/lib/core/AbstractManager.js"></script>
	<script type="text/javascript" src="TenseADT/lib/managers/Manager.jquery.js"></script>
	<script type="text/javascript" src="TenseADT/lib/core/Parameter.js"></script>
	<script type="text/javascript" src="TenseADT/lib/core/ParameterStore.js"></script>
	<script type="text/javascript" src="TenseADT/lib/core/Parameter.js"></script>
	<script type="text/javascript" src="TenseADT/lib/core/ParameterStore.js"></script>
	<script type="text/javascript" src="TenseADT/lib/core/AbstractWidget.js"></script>
	<script type="text/javascript" src="TenseADT/widgets/ResultWidget.js"></script>
	<script type="text/javascript" src="TenseADT/lib/helpers/jquery/ajaxsolr.theme.js"></script>
	<script type="text/javascript" src="TenseADT/js/theme.js"></script>
    <script type="text/javascript" src="TenseADT/js/saveresult.js"></script>
    
    
    
    
	<script type="text/javascript" src="TenseADT/crossIndexJs/jquery.simplePagination.js"></script>
	<script type="text/javascript" src="TenseADT/crossIndexJs/ResultConstructor.js"></script>
	<script type="text/javascript" src="TenseADT/crossIndexJs/CrossIndexutil.js"></script>
	<script type="text/javascript" src="TenseADT/crossIndexJs/CrossIndexTopSearch.js"></script>
	<script type="text/javascript" src="TenseADT/crossIndexJs/CrossIndexFilter.js"></script>
	<script type="text/javascript" src="TenseADT/crossIndexJs/CrossRangeFilter.js"></script>
 	

	
	
	<script type="text/javascript" src="TenseADT/lib/widgets/jquery/PagerWidget.js"></script>
	<script type="text/javascript" src="TenseADT/lib/core/AbstractFacetWidget.js"></script>
	<script type="text/javascript" src="TenseADT/widgets/MenuListWidget.js"></script>
	<script type="text/javascript" src="TenseADT/lib/core/AbstractTextWidget.js"></script>
	<script type="text/javascript" src="TenseADT/widgets/ListFacetWidget.js"></script>
	<script type="text/javascript" src="TenseADT/widgets/TagcloudWidget.js"></script>
	<script type="text/javascript" src="TenseADT/widgets/CurrentSearchWidget.q.js"></script>
	<script type="text/javascript" src="TenseADT/widgets/MenuFacetWidget.js"></script>
	<script type='text/javascript' src='TenseADT/widgets/TopSearchWidget.js'></script>

	<script type='text/javascript' src='TenseADT/js/menufilter.js'></script>
	<script type='text/javascript' src='TenseADT/js/filter.js'></script>
	<script type='text/javascript' src='TenseADT/js/mask.js'></script>
	<script type='text/javascript' src='TenseADT/js/jquery.Print.js'></script>
	<script type='text/javascript' src='TenseADT/js/tense.Print.js'></script>
	<script type='text/javascript' src='TenseADT/js/topsearchfilter.js'></script>
	<script type='text/javascript' src='TenseADT/js/tense.export.js'></script>
	<script type="text/javascript" src="TenseADT/js/jquery.tooltip.js"></script>
	<script type="text/javascript" src="TenseADT/js/common.js"></script>
	<script type="text/javascript" src="TenseADT/js/jquery.pivot.js"></script>
	<script type="text/javascript" src="TenseADT/js/pivotTable.js"></script>
	<script type="text/javascript" src="TenseADT/js/compareDocs.js"></script> 
	
	<script type="text/javascript" src="TenseADT/js/jQuery.download.js"></script>
	<script type="text/javascript" src="TenseADT/js/chart/Chart.js"></script>
	<script type="text/javascript" src="TenseADT/js/chart/ChartDiagram.js"></script>
	<script type="text/javascript" src="TenseADT/js/htmlscript.js"></script>
	
  
	