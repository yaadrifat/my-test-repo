//------------------------ freezed methods -
--------------------------- 
var oTable;
function constructTable(flag,tableId,columnId) {
        //alert("construct table");
        elementcount =0;
        $('.progress-indicator').css( 'display', 'block' );
        /* var strBreadcrumb = "<a href='#' onclick='constructTable(true);'>Protocol Library</a>";
        $("#breadcrumb").html(strBreadcrumb );
                $("#protocollist").removeClass("hidden");
                $("#protocolDetails").addClass("hidden"); */
         if(flag || flag =='true'){
             oTable=$('#'+tableId).dataTable().fnDestroy();
        }
        
          var link ;
            try{
                if(tableId=="accessionTable"){
                    /*alert("Trace 1");
                    var source1 = function ( source, type, val ) {
                            return "<input type='hidden' id='accessionValues' value='"+source[12]+"-=-"+source[13]+"-=-"+source[11]+"-=-"+source[9]+"-=-"+source[10]+"-=-"+source[8]+"-=-"+source[7]+"'/>";
                        };
                        var source2 = function ( source, type, val ) {
                            return source[0];
                        };
                        var source3=function ( source, type, val ) {
                               return source[1];
                        };
                        var source4=function ( source, type, val ) {
                               return source[2];
                      };
                      var source5 = function ( source, type, val ) {
                             return source[3];
                        };
                        var source6 = function ( source, type, val ) {
                             return source[4];
                        };
                        var source7 = function ( source, type, val ) {
                             return source[5];
                        };
                        var source8=function ( source, type, val ) {
                             return source[6];
                        };
                      

                    var access_aTargets = [ {"aTargets": [ 0], "mDataProp": source1},
                                             {    "sTitle":"Recipient ID",
                                                     "aTargets": [ 1], "mDataProp": source2},
                                             {    "sTitle":"Recipient Name",
                                                     "aTargets": [ 2], "mDataProp": source3},
                                             {    "sTitle":"Product ID",
                                                     "aTargets": [ 3], "mDataProp": source4},
                                             {    "sTitle":"Product Source",
                                                   "aTargets": [ 4], "mDataProp": source5},
                                             {    "sTitle":"Product Type",
                                                     "aTargets": [ 5], "mDataProp": source6},
                                             {    "sTitle":"Accession Time",
                                                     "aTargets": [ 6], "mDataProp": source7},
                                             {    "sTitle":"Status",
                                                     "aTargets": [ 7], "mDataProp": source8}
                       ];
                alert("Trace 2==>"+access_aTargets);
                    var access_fnServerParams = function ( aoData1 ) {
                        aoData1.push( { "name": "application", "value": "stafa"});
                        aoData1.push( { "name": "module", "value": "technician_accession"});   
                       
                        aoData1.push( {"name": "col_1_name", "value": "lower((select PERSON_ALTID from person where pk_person=pp.fk_recipient))" } );
                        aoData1.push( { "name": "col_1_column", "value": "lower(recipientId)"});
                       
                        aoData1.push( { "name": "col_2_name", "value": " lower((select PERSON_LNAME from person where pk_person=pp.fk_recipient))"});
                        aoData1.push( { "name": "col_2_column", "value": "lower(recipientName)"});
                       
                        aoData1.push( {"name": "col_3_name", "value": " lower(sp.spec_id)" } );
                        aoData1.push( { "name": "col_3_column", "value": "lower(sp.spec_id)"});
                       
                        aoData1.push( {"name": "col_4_name", "value": " lower((select codelst_desc from er_codelst where pk_codelst=acq.fk_codelst_productsource))" } );
                        aoData1.push( { "name": "col_4_column", "value": "lower(productSource)"});
                       
                        aoData1.push( {"name": "col_5_name", "value": " lower((select codelst_desc from er_codelst where pk_codelst=acq.fk_codelst_producttype))" } );
                        aoData1.push( { "name": "col_5_column", "value": "lower(productType)"});
                       
                        aoData1.push( {"name": "col_6_name", "value": " lower(to_char(acq.ACQUISITION_DATETIME,'Mon dd, yyyy  HH:MM'))" } );
                        aoData1.push( { "name": "col_6_column", "value": "acq.ACQUISITION_DATETIME"});
                       
                        aoData1.push( {"name": "col_7_name", "value": " lower(sp.spec_status)" } );
                        aoData1.push( { "name": "col_7_column", "value": "lower(sp.spec_status)"});
                       
                       
                    };
                    var access_aoColumns=[ { "bSortable": false},
                                           null,
                                           null,
                                           null,
                                           null,
                                           null,
                                           null,
                                           null
                                           ];*/
                //alert("serverParam==>"+access_fnServerParams);
                oTable=$('#'+tableId).dataTable( {
                    "sPaginationType": "full_numbers",
                    "aoColumns": [ { "bSortable": false},
                                   null,
                                   null,
                                   null,
                                   null,
                                   null,
                                   null,
                                   null
                                   ],
                    "bProcessing": true,
//                    "sPaginationType": "full_numbers",
                    "bServerSide": true,
                    "bRetrieve" : true,
                    "sDom": 'C<"clear">Rlfrtip',
                     "bDestroy": true,
                     "bJQueryUI": true,
                     "aaSorting": [[ 1, "asc" ]],
                     "oColVis": {
                            "aiExclude": [ 0 ],
                            "buttonText": "&nbsp;",
                            "bRestore": true,
                            "sAlign": "left"
                        },
                    "oLanguage": {"sProcessing":""},                                   
                    "fnServerParams": function ( aoData ) {
                        aoData.push( { "name": "application", "value": "stafa"});
                        aoData.push( { "name": "module", "value": "technician_accession"});   
                       
                        aoData.push( {"name": "col_1_name", "value": "lower((select PERSON_ALTID from person where pk_person=dr.FK_RECEIPENT))" } );
                        aoData.push( { "name": "col_1_column", "value": "lower(recipientId)"});
                       
                        aoData.push( { "name": "col_2_name", "value": " lower((select PERSON_LNAME from person where pk_person=dr.FK_RECEIPENT))"});
                        aoData.push( { "name": "col_2_column", "value": "lower(recipientName)"});
                       
                        aoData.push( {"name": "col_3_name", "value": " lower(sp.spec_id)" } );
                        aoData.push( { "name": "col_3_column", "value": "lower(sp.spec_id)"});
                       
                        aoData.push( {"name": "col_4_name", "value": " lower((select codelst_desc from er_codelst where pk_codelst=acq.fk_codelst_productsource))" } );
                        aoData.push( { "name": "col_4_column", "value": "lower(productSource)"});
                       
                        aoData.push( {"name": "col_5_name", "value": " lower((select codelst_desc from er_codelst where pk_codelst=acq.fk_codelst_producttype))" } );
                        aoData.push( { "name": "col_5_column", "value": "lower(productType)"});
                       
                        aoData.push( {"name": "col_6_name", "value": " lower(to_char(acq.ACQUISITION_DATETIME,'Mon dd, yyyy  HH:MM'))" } );
                        aoData.push( { "name": "col_6_column", "value": "acq.ACQUISITION_DATETIME"});
                       
                        aoData.push( {"name": "col_7_name", "value": " lower(sp.spec_status)" } );
                        aoData.push( { "name": "col_7_column", "value": "lower(sp.spec_status)"});
                       
                       
                    },
                    "sAjaxSource": 'dataSearch.action',
                    "sAjaxDataProp": "filterData",
                    "aoColumnDefs":[
                                    {"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
                                        return "<input type='hidden' id='accessionValues' value='"+source[12]+"-=-"+source[13]+"-=-"+source[11]+"-=-"+source[9]+"-=-"+source[10]+"-=-"+source[8]+"-=-"+source[7]+"'/>";
                                        }},
                                     {    "sTitle":"Recipient ID",
                                            "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
                                        return source[0];
                                        }},
                                     {    "sTitle":"Recipient Name",
                                            "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
                                               return source[1];
                                               }},
                                     {    "sTitle":"Product ID",
                                                   "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
                                               return source[2];
                                             }},
                                     {    "sTitle":"Product Source",
                                                 "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
                                                 return source[3];
                                         }},
                                     {    "sTitle":"Product Type",
                                             "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
                                                 return source[4];
                                         }},
                                     {    "sTitle":"Accession Time",
                                             "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
                                                 return source[5];
                                         }},
                                     {    "sTitle":"Status",
                                             "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
                                                 return source[6];
                                         }}
                                   
                                   ],
                        "fnInitComplete": function () {
                                    $("#accessionColumn").html($('.ColVis:eq(0)'));
                                    },
                        "fnServerData": function ( sSource, aoData, fnCallback ) {
                            $('.progress-indicator').css( 'display', 'block' );
                            $.ajax( {
                                    "dataType": 'json',
                                    "type": "POST",
                                    "async":false,
                                    "url": 'dataSearch.action',
                                    "data":aoData,
                                    "success":fnCallback, 
                                    "complete": function () {
                                                $('.progress-indicator').css( 'display', 'none' );
                                                $("select").uniform();
                                                }
                                  } );},
                        "fnDrawCallback": function() {
                            //alert("fnDrawCallback");
                            //$('.progress-indicator').css( 'display', 'none' );
                      }
                    } );
                jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = parseInt(no_of_pages);
                oTable.thDatasorting(tableId);
                }else if (tableId=="productSearchTable"){
                    //alert(tableId);
                   
                    oTable=$('#'+tableId).dataTable( {
                        "sPaginationType": "full_numbers",
                        "bProcessing": true,
//                        "sPaginationType": "full_numbers",
                        "bServerSide": true,
                        "bRetrieve" : true,
                        "sDom": 'C<"clear">Rlfrtip',
                         "bDestroy": true,
                         "bJQueryUI": true,
                       
                         "oColVis": {
                                "aiExclude": [ 0 ],
                                "buttonText": "&nbsp;",
                                "bRestore": true,
                                "sAlign": "left"
                            },
                        "oLanguage": {"sProcessing":""},   
                        "sAjaxSource": 'dataSearch.action',
                        "sAjaxDataProp": "filterData",
                       
                        "fnInitComplete": function () {
                            $("#productSearchColumn").html($('.ColVis:eq(1)'));
                            },
                        "fnServerData": function ( sSource, aoData, fnCallback ) {
                            $('.progress-indicator').css( 'display', 'block' );
                            $.ajax( {
                                    "dataType": 'json',
                                    "type": "POST",
                                    "async":false,
                                    "url": 'dataSearch.action',
                                    "data":aoData,
                                    "success":fnCallback, 
                                    "complete": function () {
                                                $('.progress-indicator').css( 'display', 'none' );
                                                $("select").uniform();
                                                }
                                  } );},
                        "fnDrawCallback": function() {
                            //alert("fnDrawCallback");
                            //$('.progress-indicator').css( 'display', 'none' );
                      },
                       
                        "aaSorting": [[ 1, "asc" ]],
                        "aoColumns": [ { "bSortable": false},null,null,null,null,null,null,null,null],
                        "fnServerParams": function ( aoData ){
                           
                            aoData.push( { "name": "application", "value": "stafa"});
                            aoData.push( { "name": "module", "value": "technician_accession"});   
                           
                            aoData.push( {"name": "col_1_name", "value": "lower((select PERSON_ALTID from person where pk_person=dr.FK_RECEIPENT))" } );
                            aoData.push( { "name": "col_1_column", "value": "lower(recipientId)"});
                           
                            aoData.push( { "name": "col_2_name", "value": " lower((select PERSON_LNAME from person where pk_person=dr.FK_RECEIPENT))"});
                            aoData.push( { "name": "col_2_column", "value": "lower(recipientName)"});
                           
                            aoData.push( { "name": "col_3_name", "value": " lower((select PERSON_LNAME from person where pk_person=dr.fk_person))"});
                            aoData.push( { "name": "col_3_column", "value": "lower(donorName)"});
                           
                            aoData.push( {"name": "col_4_name", "value": " lower(sp.spec_id)" } );
                            aoData.push( { "name": "col_4_column", "value": "lower(sp.spec_id)"});
                           
                            aoData.push( {"name": "col_5_name", "value": " lower((select codelst_desc from er_codelst where pk_codelst=acq.fk_codelst_producttype))" } );
                            aoData.push( { "name": "col_5_column", "value": "lower(productType)"});
                           
                            aoData.push( {"name": "col_6_name", "value": " lower((select codelst_desc from er_codelst where pk_codelst=acq.fk_codelst_productsource))" } );
                            aoData.push( { "name": "col_6_column", "value": "lower(productSource)"});
                                               
                            aoData.push( {"name": "col_7_name", "value": " lower(sp.spec_status)" } );
                            aoData.push( { "name": "col_7_column", "value": "lower(sp.spec_status)"});
                           
                            aoData.push( {"name": "col_8_name", "value": " lower(to_char(acq.ACQUISITION_DATETIME,'Mon dd, yyyy  HH:MM'))" } );
                            aoData.push( { "name": "col_8_column", "value": "acq.ACQUISITION_DATETIME"});
                           
                           
                        },
                       
                        "aoColumnDefs": [
                                            {"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
                                                    return "<input type='checkbox' id='pkRecipient' name='pkRecipient' value='"+source[9]+"'/>";
                                                    }},
                                                 {    "sTitle":"Recipient ID",
                                                        "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
                                                    return source[0];
                                                    }},
                                                 {    "sTitle":"Recipient Name",
                                                        "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
                                                           return source[1];
                                                           }},
                                                 {    "sTitle":"Donor Name",
                                                        "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
                                                           return source[7];
                                                             }},
                                                 {    "sTitle":"Product ID",
                                                               "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
                                                           return source[2];
                                                         }},
                                                 {    "sTitle":"Product Type",
                                                             "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
                                                             return source[3];
                                                     }},
                                                 {    "sTitle":"Product Source",
                                                         "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
                                                             return source[4];
                                                     }},
                                                 {    "sTitle":"Status",
                                                         "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
                                                             return source[6];
                                                     }},
                                                 {    "sTitle":"ABO/Rh",
                                                         "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
                                                             return source[4];
                                                     }},
                                                
                                                
                                               ]
                           
                        } );
                    jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = parseInt(no_of_pages);
                    oTable.thDatasorting(tableId);
                    }
                }catch(err){
                    alert("error " + err);
                }
            //    alert("end of dt");
    } 