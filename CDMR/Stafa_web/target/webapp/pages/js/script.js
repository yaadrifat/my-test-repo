//alert("script.js");

/* $(document).ready(function(){     
   alert("script.js ready");
 });
 */

var APPMODE_SAVE ="SAVE";
var APPMODE_ADD="ADD";
var APPMODE_DELETE="DELETE";
var APPMODE_READ="READ";
var APPMODE_EDIT="MODIFY";
var errMsgNoRights ='Error: User does not have rights to ';

function checkAppRights(rights,mode){
	//alert("rights : "+rights+"---mode : "+mode);
    if(mode == APPMODE_READ && rights.indexOf("R") != -1 ){
			//alert(errMsgNoRights + mode);
    	//alert("check App");
			return true;      		
    }
    else if(mode == APPMODE_ADD && rights.indexOf("A") != -1 ){
		//alert(errMsgNoRights + mode);
    	return true;   
	}
    else if(mode == APPMODE_SAVE && rights.indexOf("M") != -1 ){
			//alert(errMsgNoRights + mode);
    		//alert(errMsgNoRights + " MODIFY");
			return true;
    	
    }else if(mode == APPMODE_DELETE && rights.indexOf("D") != -1 ){
			//alert(errMsgNoRights + mode);
			return true;
    	
    }
    
    return false;
}

//Pin Notications
    function pinAlert(){
    	alert("check");
    	$(".fulldiv").removeClass("hidden");
    }
    //End of Pin Notitications
function executeFormulas(divid){
   // alert("executeFormulas");
    var formula="";
    $("#"+divid).find(":hidden").each(function (){

        var hiddenObj = this;
        var hiddenId = $(hiddenObj).attr("id");
      // alert("id = " + hiddenId + " val = > " + $(hiddenObj).val());
        formula = $(this).val();
       // alert(formula);
        $("#"+divid).find("input:text").each(function (){
        //    alert($(this).attr("id") + " val = > " + $(this).val() );
          if($.trim($(this).val()) !="" ){
            formula = formula.replace($(this).attr("id") ,eval($(this).val()) );
          }else{
              // replace 0 to null values
              formula = formula.replace($(this).attr("id"),0);
          }

        });
    //    alert("formula before exec " + formula);
        var formulaValue= eval(formula);
      //  alert("eval " + formulaValue);
        //alert("isfinite" +  );
        
        if(isFinite(formulaValue)){
            //alert("in side if");
            formulaValue = round2Decimal(formulaValue);
        }else{
            //alert("in side else");
            formulaValue = "";
        }
        var objId =  hiddenId.substring(0,hiddenId.indexOf("_"));
    //    alert("obj id " + objId);
        $("#"+objId).val(formulaValue);
    });

}

function getProtocolCalculation(protocol,divCalc){
	 var ajaxresult = jsonDataCall('getProtocolCalc',"jsonData={protocol:"+protocol+"}");
	 return constructProtocolCalculation(ajaxresult.protocolFields,divCalc);

}


function constructProtocolCalculation(protocolFields,divCalc){
	//alert(protocol);
	//alert("Trace 1");
	//jsonData = "protocol="+protocol ;

	// var ajaxresult = jsonDataCall('getProtocolCalc',"jsonData={protocol:"+protocol+"}");
	 if(protocolFields != null){
		 var precolno =-1;
		 var colno =0;
		 var rowno = 0
		 var rowdata = new Array();
		 var coldata = new Array();
		 var calcHtml ='';
		 var fieldData = new Array();
		 var colcount = 0;
		 var rowcount = 0;
		 var strrownum ="";
		 var widgetCount =0;
		 var widgetno =0;
		 
		
		 var widgetFields = new Array();
		 var widgetData = new Array();
		 var strWidgetno ="";
		 
		 $.each(protocolFields, function() {
			// alert("this " + eval("this.columnnumber"));
			 widgetno =eval("this.widgetNumber");
			 if(strWidgetno.indexOf(","+widgetno+",")==-1){
				 widgetData[widgetno]=new Array();
				 strWidgetno +=","+widgetno+",";
				 strrownum="";
				 colcount =0;
				 rowcount =0;
				 widgetFields =  new Array();
				 rowdata = new Array();
			 }
			 
			 colno=eval("this.columnnumber");
			 rowno = eval("this.rownumber");
			 if(strrownum.indexOf(","+rowno+",")==-1){
				 rowdata[rowno]= new Array();
				 strrownum += ","+rowno+",";
			 }
			
			if(widgetno>widgetCount){
				widgetCount=widgetno;
				widgetFields = new Array();
				
			}
			
			widgetFields[0] = eval("this.widgetName");
			widgetFields[1] = rowno;
			widgetFields[2] = colno;
			 fieldData = new Array();
			 fieldData[0]=eval("this.fieldType");
			 fieldData[1]=eval("this.fieldValue");
			 fieldData[2]=eval("this.fieldId");
			 fieldData[3]=eval("this.pcid");
			 fieldData[4]=eval("this.fieldListId");
			//alert("trace 2");
			 rowdata[rowno][colno]=fieldData;
			 //alert("rowdata "+rowdata);
			 widgetFields[3] = rowdata;
			 widgetData[widgetno]= widgetFields;
			 
			 //alert("widgetno " + widgetno + "-->" + widgetFields);
		 });
		if(widgetData.length>0){
		for(var w =0;w<(widgetCount +1);w++){
			calcHtml +="<div  class='portlet' > <div class='portlet-header'>"+widgetData[w][0]+ "</div>";
			calcHtml +="<div class='portlet-content '>"; 
			calcHtml +="<table >";
			rowcount = widgetData[w][1];
			colcount = widgetData[w][2];
			var wData = widgetData[w][3];
			
		 for(var r=0;r<(rowcount+1);r++){
			 calcHtml+="<tr>";
			 for(c=0;c<(colcount+1);c++){
				 calcHtml+="<td>" ; 
				 fieldData = wData[r][c];
				// alert(w + " / " + r + " / " + c + " --> " + fieldData);
				// alert("[0] " + fieldData[0] + " / " + fieldData[1] + " 2 " + fieldData[2] );
				 if(fieldData!=undefined){
					 if(fieldData[0]== "LABEL"){
						 calcHtml+="<label>" + fieldData[1] +"</label>";
					 }else  if(fieldData[0]== "TEXT"){
						 calcHtml+="<input type='text' id='"+fieldData[1]+"' name='"+fieldData[1]+"' onkeyUp='executeFormulas(\""+divCalc+"\");' />";
					 }else  if(fieldData[0]== "CALC"){
						 var formula =""
						 var value="";
						 if(fieldData[1].indexOf("=")> 0){
		                        formula = fieldData[1].substring(fieldData[1].indexOf("=")+1);
		                        value = fieldData[1].substring(0,(fieldData[1].indexOf("=")));
						 }
						 calcHtml+="<input type='hidden' id='"+value+"_formula' name='"+value +"_formula' value='" +formula + "' />"
						 calcHtml+="<input type='text' id='"+value+"'   name='"+value+"'  />";
					 }else  if(fieldData[0]== "DROPDOWN"){
						// alert("Dropdown");
							jsonData="parentId="+fieldData[4];
		                     response = jsonDataCall("getChildValues",jsonData);
		                     var selectHtml = "<select id='"+fieldData[1]+"' name='"+fieldData[1] +"' ><option value=''>Select</option>"
		                     for (var i = 0; i < response.childDetails.length; i++) { 
		                    	 selectHtml +="<option value='"+ response.childDetails[i].pkCodelst+"'>"+response.childDetails[i].description+"</option>";
		                     }
		                     calcHtml+=selectHtml;
		                     calcHtml+"</select>";
					 }
				 }else{
					 calcHtml+="&nbsp;";
				 }
				 calcHtml+="</td>";
			 }
			 calcHtml+="</tr>";
		 }
		 //alert(" rowdata " + rowdata);
		 calcHtml+='</table >';
		 calcHtml+='</div ></div>';
		}
		}
		//alert(" calcHtml " + calcHtml);
		 $( "#"+divCalc).html(calcHtml);

		 $("#"+divCalc).find(".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
			.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
			 $("#"+divCalc).find(".portlet-header .ui-icon" ).click(function() {
				$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
				$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
			});
	 }
	 return true;
}





function refreshTable(tableId,parentId,url,formId){
	//alert("refershTable")
//	$('.progress-indicator').css( 'display', 'block' );
	try{
		var ajaxresult = ajaxCall(url,formId);
		//alert(" ajaxresult " + ajaxresult);
	    var $response=$(ajaxresult);
	            //query the jq object for the values
	            var errorpage = $response.find('#errorForm').html();
	            if(errorpage != null &&  errorpage != "" ){
	            	$('#main').html(result);
	            }else{
	            	$("#"+parentId).replaceWith($('#'+parentId, $(ajaxresult)));
	            	//constructTable();
	            	var oTableqc=$("#"+tableId).dataTable({
		        		 "bRetrieve":true,
		        		 "sPaginationType": "full_numbers"
		        	});
	            }
	            
	       
	}catch(err){
		alert("error in qc cal " + err);
	}
		// $('.progress-indicator').css( 'display', 'none' );
}

/* not in use
function refershModelWindow(divId,url,formId){
	try{
		var ajaxresult = ajaxCall(url,formId);
		//alert(" ajaxresult " + ajaxresult);
	    var $response=$(ajaxresult);
	            //query the jq object for the values
	            var errorpage = $response.find('#errorForm').html();
	            if(errorpage != null &&  errorpage != "" ){
	            	$('#main').html(result);
	            }else{
	            	$("#"+divId).replaceWith($('#'+divId, $(ajaxresult)));
	            	
	            	$("#"+divId).dialog({
	     			   autoOpen: true,
	     				   modal: true, width:500, height:460,
	     					close: function() {
	     		      		jQuery("#"+divId).dialog("destroy");				
	     			   		}
	     			  });
	            	
	            }
	     
	}catch(err){
		alert("error in qc cal " + err);
	}
	
}
*/


function jsonDataCall(url,jsondata){
	//alert("ajax Call " + url + " / " + form);

	var response = "";
	 $.ajax({
	        type: "POST",
	         
	        url : url+".action",
			data : jsondata,
			contentType: "application/x-www-form-urlencoded;charset=ISO-8859-15",
	        dataType: 'json',
	        async:false,
	        success: function (result){
	        	response=result;
	        },
	        error: function (request, status, error) {
	        	
	        	/*alert("Error " + error);
	            alert(request.responseText);*/
	        	
	        	response= request.responseText;
	        }

		});
	return response;
}


function ajaxCall(url,form){
	//alert("ajax Call " + url + " / " + form);

	var response = "";
	 $.ajax({
	        type: "POST",
	         
	        url : url.indexOf('.action')!=-1?url:url+".action",
			data : $("#"+form).serialize(),
	        
	        async:false,
	        success: function (result){
	        	//alert("success " + result);
	        	
	        	response=result;
	        	//var resultTxt = result;
	        	//alert($(response).text());
	        	//var webResult = result.userAuthentication;
	        	//alert(" webResult " + webResult);
	        	//webResult = createXmlDOMObject(webResult);
	        	//errorMessage = result.errorMessage;
      			
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        	
	        	response= error;
	        }

		});
	return response;
}


function createXmlDOMObject(xmlString)
{
    var xmlDoc = null;

    if( ! window.DOMParser )
    {
        // the xml string cannot be directly manipulated by browsers 
        // such as Internet Explorer because they rely on an external 
        // DOM parsing framework...
        // create and load an XML document object through the DOM 
        // ActiveXObject that it can deal with
        xmlDoc = new ActiveXObject( "Microsoft.XMLDOM" );
        xmlDoc.async = false;
        xmlDoc.loadXML( xmlString );
    }
    else
    {
        // the current browser is capable of creating its own DOM parser
        parser = new DOMParser();
        xmlDoc = parser.parseFromString( xmlString, "text/xml" ) ;
    }

    return xmlDoc;
}

function loadPanel(pageurl){
	document.getElementById("main").innerHTML="";
	$('.progress-indicator').css( 'display', 'block' );
	
	$.ajax({
        type: "GET",
        url : pageurl,
        async:false, 
        success: function (result){
        	$("#main").html(result);
        	$("#protocolHidden").val("saveProtocols");
        }
	});
	$('.progress-indicator').css( 'display', 'none' );
	$("select").uniform();
}

function saveRecipientWorkflowTask(formid){
	var url = "nextTask.action";
	var result = ajaxCall(url,formid);
	$("#main").html(result);
}

/*
 * not in use
 * 
function readXml(fileName){
	// alert(fileName);
	var resXml = "";
	var pathname = window.location.pathname;
	alert("pathname" + pathname);
	 var fileurl = pathname+ "/xmlData/"+fileName;
	
	 alert("fileurl " + fileurl);
	 $.ajaxSetup({
         url: fileurl
      });
	 $.ajax({
	        type: "GET",
	      
	        dataType: "xml",
	        async:false,
	        cache:false,
	        success: function(xml){
	        	resXml = xml;
	        },
			complete: function( xhr, status ){
				//alert("status "  +status);
				if( status == 'parsererror' ){
					xmlDoc = null;
					if (document.implementation.createDocument){
						// code for Firefox, Mozilla, Opera, etc.
						var xmlhttp = new window.XMLHttpRequest();
						xmlhttp.open("GET", fileurl, false);
						xmlhttp.send(null);
						xmlDoc = xmlhttp.responseXML.documentElement;
					}else  if (window.ActiveXObject){
						
					  xmlDoc=new ActiveXObject( "Microsoft.XMLDOM" ) ;
					  xmlDoc.async = "false" ;
					  xmlDoc.loadXML( xhr.responseText ) ;
					}  else if( window.DOMParser ) {
						// others
					  parser=new DOMParser();
					  xmlDoc=parser.parseFromString( xhr.responseText,"text/xml" ) ;
					}else{
						// 
						alert('Your browser cannot handle this script');
					} 
						resXml= xmlDoc ;				
				  }
				
			},
			error: function(xmlhttp, status, error){
	 			alert("Err" + error); 
			}
	 });
	// alert("resXml " + resXml);
	 return resXml;
}
*/
var Documents ={
		
		add : function(table,dropDownlstid, dateId, attachFileId, documentRef, documentTitle){
			
			if(checkModuleRights(STAFA_RECIPIENTTRACKERS, APPMODE_ADD)){
				 var docsTitle = $("#"+dropDownlstid).html();
				 //alert(docsTitle);
			     var newRow ="<tr>"+
			     "<td><input type='hidden' id='"+documentRef+"documentRef' value='0' name='"+documentRef+"documentRef'/></td>"+
			     "<td><div><select id='"+documentTitle+"documentTitle' name='"+documentTitle+"documentTitle' style='width:200px;'>"+docsTitle+"</select></div></td>"+
			     "<td></td>"+
			     "<td><input type='text' id='documentDate"+dateId+"' name='documentDate' class='dateEntry' /></td>"+
			   	 "<td><img src='images/file_loading.gif' id='fileUploadImage"+attachFileId+"' style='display:none'/>"+
			   	 "<div id='fileDisplayName"+attachFileId+"'></div><input type='hidden' class='fileName' id='fileName"+attachFileId+"' name='fileName"+attachFileId+"'/><input type='hidden' class='filePathId' name='filePathId"+attachFileId+"' id='filePathId"+attachFileId+"'/> <input type='hidden' class='fileUploadContentType' name='fileUploadContentType"+attachFileId+"' id='fileUploadContentType"+attachFileId+"'/>"+
			   	 "<div class='"+attachFileId+"' style='cursor: pointer;	color: #0101DF;' id='displayAttach"+attachFileId+"'></div><input size='10' style='display:block' class='"+attachFileId+"' type='file' name='fileUpload' onChange='AjaxAction.fileUpload(this,\""+attachFileId+"\")' id='attach"+attachFileId+"'/><span><s:text name='stafa.fileupload.message'/></span></td>"+
			     "<td></td>"+
			     "</tr>";
			     $("#"+table+" tbody").prepend(newRow);
			         $("#documentDate"+dateId).datepicker({dateFormat: 'M dd, yy',changeMonth: true, changeYear: true, yearRange: '1900:' + new Date().getFullYear()});
			     dateId=dateId+1;
			     $.uniform.restore('select');
				 $('select').uniform();
			
				 attachFileId=attachFileId+1;
				// var param ="{module:'APHERESIS',page:'DONOR_DETAILS'}";
				// markValidation(param);
			 }
		},		
		del : function(table){			
			if(checkModuleRights(STAFA_RECIPIENTTRACKERS, APPMODE_DELETE)){  	
				 var deletedIds="";
					try{
					$("#"+table+" tbody tr").each(function (row){
						 if($(this).find("#docid").is(":checked")){
							deletedIds += $(this).find("#docid").val() + ",";
						}
					 });
					  if(deletedIds.length >0){
						 var yes=confirm(confirm_deleteMsg);
						 if(!yes){
										return;
						 }
						  deletedIds = deletedIds.substr(0,deletedIds.length-1);
						 console.warn(deletedIds);
						  jsonData = "{deletedIds:'"+deletedIds+"',domainName:'SUB_DOMAIN_DOCUMENT',domainKey:'ATTACHMENT_ID'}";
						  url="commonDelete";					     
					     response = jsonDataCall(url,"jsonData="+jsonData);					
						constructDocTable(table);
					    
					 	
					  }
					}catch(err){
						alert(" error " + err);
					}
			}
		},
		save : function( entityid, entityType, tx, visit, widgetType, tableid, idPrefix ){
			
			try{
				var documentData = "";
				var donor = entityid;
				var entityType = entityType;
				 $("#"+tableid+" tbody tr").each(function (row){
				 	var docsTitle="";
				 	var docsDate="";
				 	var fileData="";
				 	var fileUploadContentType="";
				 	var reviewedStatus="";
				 	var documentFileName="";
				 	var documentId="";
				 	if($(this).find(".filePathId").val()!=undefined){
					$(this).find("td").each(function (col){
						if(col ==0){
							documentId=$(this).find("#"+idPrefix+"documentRef").val();
						}
						if(col ==1){
							docsTitle = $(this).find("#"+idPrefix+"documentTitle").val();
						}
						if(col ==3){
							docsDate = $(this).find(".dateEntry").val();
						}
						if(col ==4){
							fileData = $(this).find(".filePathId").val();
							fileData=fileData.replace(/\\/g,"/");
							documentFileName = $(this).find(".fileName").val();
							
							fileUploadContentType = $(this).find(".fileUploadContentType").val();
						}
					});
					if(fileData != undefined && fileData != ""){
						documentData+="{widgetType:'"+widgetType+"', pkDocumentId:'"+documentId+"',fkEntity:'"+donor+"',entityType:'"+entityType+"',fkCodelstDocumentType:'"+docsTitle+"',documentFileName:'"+documentFileName+"',documentUploadedDate:'"+docsDate+"',documentFile:'"+fileData+"',fileUploadContentType:'"+fileUploadContentType+"',reviewedStatus:'"+reviewedStatus+"',fkTxWorkflow:'"+tx+"',fkTxVisit:'"+visit+"'},";
					}
				 	}
				 });
					if(documentData.length >0){
						documentData = documentData.substring(0,(documentData.length-1));
						var url="uploadDocs";
						response = jsonDataCall(url,"jsonData={documentData:["+documentData+"]}");						
						$('.progress-indicator').css( 'display', 'none' );
						
					}
				}catch(e){
					alert("Error : "+e);
					}
				 return true;
				 
		},
		saveDoc : function(paramObj, tableObj){
			try{
			Documents.save(paramObj.entityId, paramObj.entityType, paramObj.txId, paramObj.visitId, paramObj.widgetType, tableObj.TABLE, tableObj.TABLEIdPrefix);
			}catch(e){alert("Error : "+e);}
		},
		saveDocs : function(paramObj, param2Obj, tableObj, table2Obj){
			try{
			Documents.save(paramObj.entityId, paramObj.entityType, paramObj.txId, paramObj.visitId, paramObj.widgetType, tableObj.TABLE, tableObj.TABLEIdPrefix);
			Documents.save(param2Obj.entityId, param2Obj.entityType, param2Obj.txId, param2Obj.visitId, param2Obj.widgetType, table2Obj.TABLE, table2Obj.TABLEIdPrefix);
			}catch(e){
				alert("Error : "+e);
			}
			return true;
		}
		
};

var  DocTable ={
		
		currentTable : function(flag, entity, entityType, tx, visit, tableId, docTableHeaders, displayedColumn, idPrefix, category){
			

    		var criteria = "";
    		if(visit!=null && visit!=""){
    			criteria =" AND FK_TX_VISIT="+visit;
    		}
    		var documentColumn = [ { "bSortable": false},
    			                	null,
    			                	null,
    			                	null,
    			                	null
    				             ];
    													
    		var documentServerParams = function ( aoData ) {
    										aoData.push( { "name": "application", "value": "stafa"});
    										aoData.push( { "name": "module", "value": "recipient_DocumentReview"});
    										aoData.push( { "name": "criteria", "value": criteria});
    										
    										aoData.push( { "name": "param_count", "value":"4"});
    										aoData.push( { "name": "param_1_name", "value":":ENTITY:"});
    										aoData.push( { "name": "param_1_value", "value":entity});

    										aoData.push( { "name": "param_2_name", "value":":ENTITYTYPE:"});
    										aoData.push( { "name": "param_2_value", "value":"'"+entityType+"'"});

    										aoData.push( { "name": "param_3_name", "value":":CATEGORY:"});
    										aoData.push( { "name": "param_3_value", "value":category});
    										
    										aoData.push( { "name": "param_4_name", "value":":TX:"});
    										aoData.push( { "name": "param_4_value", "value":tx});
    										
    										aoData.push( {"name": "col_1_name", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_DOCUMENTTYPE))" } );
    										aoData.push( { "name": "col_1_column", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_DOCUMENTTYPE))"});
    										
    										aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
    										aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
    													
    										
    					};
    		var documentaoColDef = [
    					                  {		
    					                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
    				                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
    				                	 			return "<input type='hidden' id='"+idPrefix+"documentRef' name='"+idPrefix+"documentRef' value='"+source[0] +"' ><input type='checkbox' id='docid' name='docid' value='"+source[0] +"' >";
    				                	 			//return "";
    				                	     }},
    					                  { "sTitle": docTableHeaders[0],//"Document Title",
    				                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
    					                	    	 //alert('docTitle'+source.documentFileName); 
    					                	 		return source[6];
    					                	 }},
    					                	 { "sTitle": docTableHeaders[1],//"Version #",
    					                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
    											    return source[7];
    										}},
    									  { "sTitle": docTableHeaders[2],//"Document Date",
    					                		 "aTargets": [3], "mDataProp": function ( source, type, val ) { 
    											    return source[2];
    											    }},
    									  { "sTitle": docTableHeaders[3],//"Attachment",
    											 "aTargets": [4], "mDataProp": function ( source, type, val ) {
    										 	   return source[1] + " &nbsp;&nbsp;<a href='#' onclick='openPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\")'> View </a>";
    									  		}}/*,
    									  { "sTitle": docTableHeaders[4],//"Reviewed",
    									  		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
    									  			 if(source[3] ==1){
    									  				 return "Reviewed";
    									  			 }else{
    									  				 return "";
    									  			 }
    										  		//return "<input type='checkbox'/>";
    										  }}*/
    									  
    									];
		
    		var documentColManager = function () {
    												$("#"+displayedColumn).html($("#"+tableId+"_wrapper .ColVis"));
    											};
    											
    										
    		constructTable(flag,tableId,documentColManager,documentServerParams,documentaoColDef,documentColumn);
    		
		},
currentTableExHead : function(flag, entity, entityType, tx, visit, tableId, docTableHeaders, displayedColumn, idPrefix, category){
			

    		var criteria = "";
    		if(visit!=null && visit!=""){
    			criteria =" AND FK_TX_VISIT="+visit;
    		}
    		var documentColumn = [ { "bSortable": false},
	   			                	null,
	   			                	null,
	   			                	null,
	   			                	null,
	   			                	null
   				             ];
    													
    		var documentServerParams = function ( aoData ) {
    										aoData.push( { "name": "application", "value": "stafa"});
    										aoData.push( { "name": "module", "value": "recipient_DocumentReview"});
    										aoData.push( { "name": "criteria", "value": criteria});
    										
    										aoData.push( { "name": "param_count", "value":"4"});
    										aoData.push( { "name": "param_1_name", "value":":ENTITY:"});
    										aoData.push( { "name": "param_1_value", "value":entity});

    										aoData.push( { "name": "param_2_name", "value":":ENTITYTYPE:"});
    										aoData.push( { "name": "param_2_value", "value":"'"+entityType+"'"});

    										aoData.push( { "name": "param_3_name", "value":":CATEGORY:"});
    										aoData.push( { "name": "param_3_value", "value":category});
    										
    										aoData.push( { "name": "param_4_name", "value":":TX:"});
    										aoData.push( { "name": "param_4_value", "value":tx});
    										
    										aoData.push( {"name": "col_1_name", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_DOCUMENTTYPE))" } );
    										aoData.push( { "name": "col_1_column", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_DOCUMENTTYPE))"});
    										
    										aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
    										aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
    													
    										
    					};
    		var documentaoColDef = [
    					                  {		
    					                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
    				                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
    				                	 			return "<input type='hidden' id='"+idPrefix+"documentRef' name='"+idPrefix+"documentRef' value='"+source[0] +"' ><input type='checkbox' id='docid' name='docid' value='"+source[0] +"' >";
    				                	 			//return "";
    				                	     }},
    					                  { "sTitle": docTableHeaders[0],//"Document Title",
    				                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
    					                	    	 //alert('docTitle'+source.documentFileName); 
    					                	 		return source[6];
    					                	 }},
    					                	 { "sTitle": docTableHeaders[1],//"Version #",
    					                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
    											    return source[7];
    										}},
    									  { "sTitle": docTableHeaders[2],//"Document Date",
    					                		 "aTargets": [3], "mDataProp": function ( source, type, val ) { 
    											    return source[2];
    											    }},
    									  { "sTitle": docTableHeaders[3],//"Attachment",
    											 "aTargets": [4], "mDataProp": function ( source, type, val ) {
    										 	   return source[1] + " &nbsp;&nbsp;<a href='#' onclick='openPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\")'> View </a>";
    									  		}},
  	    								 { "sTitle": docTableHeaders[4],//"Reviewed",
	    									  		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
	    									  			 if(source[3] ==1){
	    									  				 return "Reviewed";
	    									  			 }else{
	    									  				 return "";
	    									  			 }
	    										  }}
    									  
    									];
		
    		var documentColManager = function () {
    												$("#"+displayedColumn).html($("#"+tableId+"_wrapper .ColVis"));
    											};
    											
    										
    		constructTable(flag,tableId,documentColManager,documentServerParams,documentaoColDef,documentColumn);
    		
		},
		historyTable : function(flag, entity, entityType, tx, visit, tableId, docTableHeaders, displayedColumn, idPrefix, category){
			
			var criteria = "";
    		if(visit!=null && visit!=""){
    			criteria =" AND FK_TX_VISIT="+visit;
    		}
	    	var docHistoryaoColumn =  [ { "bSortable": false},
	    				                	null,
	    				                	null,
	    				                	null,
	    				                	null
	    					            ];
	    	
	    	var docHistoryServerParams = function ( aoData ) {
	    									aoData.push( { "name": "application", "value": "stafa"});
	    									aoData.push( { "name": "module", "value": "recipient_DocumentReview_History"});
	    									aoData.push( { "name": "criteria", "value": criteria});

	    									aoData.push( { "name": "param_count", "value":"4"});
  											aoData.push( { "name": "param_1_name", "value":":ENTITY:"});
  											aoData.push( { "name": "param_1_value", "value":entity});

  											aoData.push( { "name": "param_2_name", "value":":ENTITYTYPE:"});
  											aoData.push( { "name": "param_2_value", "value":"'"+entityType+"'"});

  											aoData.push( { "name": "param_3_name", "value":":CATEGORY:"});
  											aoData.push( { "name": "param_3_value", "value":category});
  											
  											aoData.push( { "name": "param_4_name", "value":":TX:"});
  											aoData.push( { "name": "param_4_value", "value":tx});
	    									
	    									aoData.push( {"name": "col_1_name", "value": "lower(DOCUMENT_FILENAME)" } );
	    									aoData.push( { "name": "col_1_column", "value": "lower(DOCUMENT_FILENAME)"});
	    									
	    									aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
	    									aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
	    												
	    									
	    				};
	    				
	    	var	docHistoryaoColDef = [
	    				                  {		
	    				                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
	    			                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
	    			                	 			return "<input type='hidden' id='"+idPrefix+"documentRef' name='"+idPrefix+"documentRef' value='"+source[0] +"' > ";
	    			                	 			//return "";
	    			                	     }},
	    				                  { "sTitle": docTableHeaders[0],//"Document Title",
	    			                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
	    				                	    	 //alert('docTitle'+source.documentFileName); 
	    				                	 		return source[6];
	    				                	 }},
	    								  { "sTitle": docTableHeaders[1],//"Version #",
	    				                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
	    										    return source[7];
	    										    }},
	    								  { "sTitle": docTableHeaders[2],//"Document Date",
	    										 "aTargets": [3], "mDataProp": function ( source, type, val ) {
	    									 	   return source[2] + " &nbsp;&nbsp;<a href='#' onclick='openPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\")'> View </a>";
	    								  		}},
	    								  		{ "sTitle": docTableHeaders[3],//"Attachment",
		    				                		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
		    										    return source[1];
		    										    }}/*,
	    								  { "sTitle": docTableHeaders[4],//"Reviewed",
	    								  		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
	    								  			 if(source[3] ==1){
	    								  				 return "Reviewed";
	    								  			 }else{
	    								  				 return "";
	    								  			 }
	    									  		//return "<input type='checkbox'/>";
	    									  }}*/
	    								  
	    								];
		
	    	
	    	var docHistoryColManager =  function () {
	    											$("#"+displayedColumn).html($("#"+tableId+"_wrapper .ColVis"));
	    								};
	    	constructTable(flag,tableId,docHistoryColManager,docHistoryServerParams,docHistoryaoColDef,docHistoryaoColumn);
	    					
		},
		historyTableExHead : function(flag, entity, entityType, tx, visit, tableId, docTableHeaders, displayedColumn, idPrefix, category){
			
			var criteria = "";
    		if(visit!=null && visit!=""){
    			criteria =" AND FK_TX_VISIT="+visit;
    		}
    		var docHistoryaoColumn =  [ { "bSortable": false},
    				                	null,
    				                	null,
    				                	null,
    				                	null,
    				                	null
    					            ];
	    	
	    	var docHistoryServerParams = function ( aoData ) {
	    									aoData.push( { "name": "application", "value": "stafa"});
	    									aoData.push( { "name": "module", "value": "recipient_DocumentReview_History"});
	    									aoData.push( { "name": "criteria", "value": criteria});

	    									aoData.push( { "name": "param_count", "value":"4"});
  											aoData.push( { "name": "param_1_name", "value":":ENTITY:"});
  											aoData.push( { "name": "param_1_value", "value":entity});

  											aoData.push( { "name": "param_2_name", "value":":ENTITYTYPE:"});
  											aoData.push( { "name": "param_2_value", "value":"'"+entityType+"'"});

  											aoData.push( { "name": "param_3_name", "value":":CATEGORY:"});
  											aoData.push( { "name": "param_3_value", "value":category});
  											
  											aoData.push( { "name": "param_4_name", "value":":TX:"});
  											aoData.push( { "name": "param_4_value", "value":tx});
	    									
	    									aoData.push( {"name": "col_1_name", "value": "lower(DOCUMENT_FILENAME)" } );
	    									aoData.push( { "name": "col_1_column", "value": "lower(DOCUMENT_FILENAME)"});
	    									
	    									aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
	    									aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
	    												
	    									
	    				};
	    				
	    	var	docHistoryaoColDef = [
	    				                  {		
	    				                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
	    			                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
	    			                	 			return "<input type='hidden' id='"+idPrefix+"documentRef' name='"+idPrefix+"documentRef' value='"+source[0] +"' > ";
	    			                	 			//return "";
	    			                	     }},
	    				                  { "sTitle": docTableHeaders[0],//"Document Title",
	    			                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
	    				                	    	 //alert('docTitle'+source.documentFileName); 
	    				                	 		return source[6];
	    				                	 }},
	    								  { "sTitle": docTableHeaders[1],//"Version #",
	    				                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
	    										    return source[7];
	    										    }},
	    								  { "sTitle": docTableHeaders[2],//"Document Date",
	    										 "aTargets": [3], "mDataProp": function ( source, type, val ) {
	    									 	   return source[2] + " &nbsp;&nbsp;<a href='#' onclick='openPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\")'> View </a>";
	    								  		}},
	    								  { "sTitle": docTableHeaders[3],//"Attachment",
		    				                		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
		    										    return source[1];
		    									}},
		    		    					{ "sTitle": docTableHeaders[4],//"Reviewed",
		   	    								  		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
		   	    								  			 if(source[3] ==1){
		   	    								  				 return "Reviewed";
		   	    								  			 }else{
		   	    								  				 return "";
		   	    								  			 }
		   	    								}}
	    								  
	    								];
		
	    	
	    	var docHistoryColManager =  function () {
	    											$("#"+displayedColumn).html($("#"+tableId+"_wrapper .ColVis"));
	    								};
	    	constructTable(flag,tableId,docHistoryColManager,docHistoryServerParams,docHistoryaoColDef,docHistoryaoColumn);
	    					
		}
};

var AjaxAction = {
	
	fileUpload : function(fileOb,rowId){	
	 //starting setting some animation when the ajax starts and completes
	 var fileId = $(fileOb).attr("id");
	 var fileSize = GetFileSize(fileId)
	 if(fileSize>2){
		 alert("Upload file size limit is 2 MB only");
	 }else{
	     $(fileOb).hide();
	     $('#fileUploadImage'+rowId).css( 'display', 'block' );
	     $('#displayAttach'+rowId).css( 'display', 'none' );     
	     //$("#fileName1").html($(fileOb).val());
	     //alert("trace 1 -- ajaxfileupload1");
	     var fileId = $(fileOb).attr("id");
	     //alert(fileId);
	     //alert("rowId : "+rowId);
	     var uploadUrl = "inlineAddDocuments.action";
	     if ($.browser.msie) {
	    	 uploadUrl = "inlineAddDocumentsIE.action";
	     }
	     //alert("uploadUrl : "+uploadUrl);
	     $.ajaxFileUpload({
	             url:uploadUrl,
	             secureuri:false,
	             fileElementId:fileId,
	             elementSeq:rowId,
	             dataType: 'html',
	             //dataType: 'json',
	             success: function (data, status){
	                 //alert("success ");
	                 //alert("data "+data);
	            	 if (!($.browser.msie)) {
		                 data = data.replace("<pre>","");
		                 data = data.replace("</pre>","");
		                 //alert(data);
		                 var obj = jQuery.parseJSON(data);
		                 //alert(obj);
		                 //alert("returnMessage:  "+obj.returnMessage);
		                 //alert("obj.fileUploadContentType:  "+obj.uploadContentType);
		                 $("#filePathId"+rowId).val(obj.returnMessage);
		                 $("#fileUploadContentType"+rowId).val(obj.uploadContentType);
	            	 }
	
	             },
	             error: function (data, status, e){
	                 //alert("error ");
	                 //alert(data + " / " + status);
	                 $('#fileUploadImage'+rowId).css( 'display', 'none' );
	                 //$("#fileName1").html($(fileOb).val());
	                 alert("Error : "+e);
	             },
	             complete: function(data){
		            	 if (!($.browser.msie)) {
			            	 $('#fileUploadImage'+rowId).css( 'display', 'none' );
			                 $("#fileDisplayName"+rowId).html($(fileOb).val());            
			                 $("#fileName"+rowId).val($(fileOb).val());
		            	 }
	                 }
	         }
	     )
	   // alert("trace end")
	     return false;
	 }
 }
		
};