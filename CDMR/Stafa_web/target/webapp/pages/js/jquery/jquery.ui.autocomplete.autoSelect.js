/*
 * jQuery UI Autocomplete Auto Select Extension
 *
 * Copyright 2010, Scott González (http://scottgonzalez.com)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * http://github.com/scottgonzalez/jquery-ui-extensions
 */
(function( $ ) {

//?BB $.ui.autocomplete.prototype.options.autoSelect = true;
$.ui.autocomplete.prototype.options.autoSelect = true;
$( ".ui-autocomplete-input" ).live( "blur", function( event ) {
	var autocomplete = $( this ).data( "autocomplete" );
	if ( !autocomplete.options.autoSelect || autocomplete.selectedItem ) { return; }

	var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" );
	autocomplete.widget().children( ".ui-menu-item" ).each(function() {
		var item = $( this ).data( "item.autocomplete" );
		if ( matcher.test( item.label || item.value || item ) ) {
			autocomplete.selectedItem = item;
			return false;
		}
	});
	if ( autocomplete.selectedItem ) {
		autocomplete._trigger( "select", event, { item: autocomplete.selectedItem } );
	}
});

}( jQuery ));


function autoCompleteCall(inputBoxName,hiddenFieldName){
	var b = $("#"+hiddenFieldName).val();
	 b = b.replace("[","");
	 b = b.replace("]","");
	var a = b.split(",");
	$( "#"+inputBoxName ).autocomplete({
		source: a
	});
}
/** for object */
function setObjectAutoComplete(obj,comboBoxId,idflag){
	//alert("setObjectAutoComplete");
	// BB some times give wrong value so change to x.options.length 
	// var lstLen = $('#'+comboBoxId+' > option').size();
	
	var lstVal = [];
	var flag = false;
	var x = document.getElementById(comboBoxId);
	 var lstLen = x.options.length;
	//alert(lstLen + " /opt len " + x.options.length);
	//alert( " length " +  $('#'+comboBoxId).length);
	//alert(lstLen + " / obj.id ->" + $(obj).attr("id") + "/ comboBoxId-> " + comboBoxId+ " /flag " + idflag );
	//alert( "x " + x);
	var i=0;
	try{
	if(idflag){
		for(i=1;i<lstLen;i++){
			var idx = i-1;
			//alert(x.options[i].text);
			lstVal[idx] = " " +x.options[i].text+" -> " + x.options[i].value;
			
		}
	}else{
		for(i=1;i<lstLen;i++){
			var idx = i-1;
			//alert(x.options[i].text);
			lstVal[idx] = " "+x.options[i].text+" ";
		}
	}
	//alert(lstVal);
	
	$(obj).autocomplete({
		source: lstVal,
 		select: function( event, ui ) {
 			//alert("val : ["+ui.item.label+"]");
 			setComboValue(ui.item.label,comboBoxId);
 			try{
				//alert("test");
				autoCompleteChangeEvent(obj);
			}catch(err){
				
			}
 		},
		change: function( event, ui ) {
			var inputBoxValue = $("#"+$(obj).attr("id")).val(); 
			ui.item ? ui.item.label : setCombo(inputBoxValue,comboBoxId);
			try{
				//alert("test1");
				autoCompleteChangeEvent(obj);
			}catch(err){
				
			}
		}
	});
	
	}catch(e){
		
		alert("error " + e + " / comboBoxId " + comboBoxId  + " /lstLen " +lstLen + " i " + i);
	}
	
}


/**Autocomplete for studyName Start*/
function autoCompleteCombo(inputBoxId,comboBoxId){
	try{
	var lstLen = $('#'+comboBoxId+' > option').size();
	var lstVal = [];
	var flag = false;
	var x = document.getElementById(comboBoxId);
	//alert(lstLen);
	for(var i=1;i<lstLen;i++){
		var idx = i-1;
		//alert(x.options[i].text);
		lstVal[idx] = " "+x.options[i].text+" ";
	}
	//alert(lstVal);
	$( "#"+inputBoxId ).autocomplete({
		matchContains: true,
		minChars: 0,
		source: lstVal,
 		select: function( event, ui ) {
 			//alert("val : ["+ui.item.label+"]");
 			setComboValue(ui.item.label,comboBoxId);
 		},
		change: function( event, ui ) {
			var inputBoxValue = $("#"+inputBoxId).val(); 
			ui.item ? ui.item.label : setCombo(inputBoxValue,comboBoxId);
		}
	});
	//alert("s1");
	}catch(e){
		//alert(e);
	}
}

function setCombo(selectedVal,comboBoxId){
	var trimValue = selectedVal.replace(/^\s+|\s+$/g,"");		//trim left and right spaces
	var val ="";
	var flag = false;
	if(trimValue.length<=0){									//when the user give only empty value or spaces only allowed here to change ComboBox as 'Select'
	flag = false;
	}else{
		val = trimValue;										//If user not selecting the text from auto complete list means, we have to check the text with comboBox.  
		var x = document.getElementById(comboBoxId);
		$("#"+comboBoxId).find("option:contains('"+val+"')").each(function(){
			//alert("["+$(this).text()+"]==["+val+"]");
		if( $(this).text() == val ) {
			$(this).attr("selected","selected");
			flag = true;
			}
		});
	}
	if(!flag){
		val = "";
		var x = document.getElementById(comboBoxId);
		$("#"+comboBoxId).find("option:contains('"+val+"')").each(function(){
		if( $(this).val() == val ) {
			$(this).attr("selected","selected");
			}
		});
	}
	$('#'+comboBoxId).trigger("onchange");
	//If partial name only given means we have to create the else part as per the business.
}
function setComboValue(val1,comboBoxId){
	//val = val1.replace(/^\s+/,"");		
	val = val1.replace(/^\s+|\s+$/g,"");		//trim left and right spaces
	//alert(val);
	var x = document.getElementById(comboBoxId);
	$("#"+comboBoxId).find("option:contains('"+val+"')").each(function(){
		//alert("["+$(this).text()+"]==["+val+"]");
	if( $(this).text() == val ) {
		$(this).attr("selected","selected");
		}
	});
}
/**Autocomplete for studyName End*/

/**Autocomplete with One Text field and One Array Start*/

function autoCompleteArray(inputBoxId,Arr){
	//alert(Arr);
	$( "#"+inputBoxId ).autocomplete({
		source: Arr
	});
}
/**Autocomplete with One Text field and One Array End*/
