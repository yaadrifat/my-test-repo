 function labelVerificationValidation(){
	
	var labelVeriflag=false;
	var isbt=$(".isbtclass").attr('name');
	if($("input[name='"+isbt+"']:checked").val()==1){
		
			var isbtreturnflag=ISBTcheck();
		
			$("#isbtattach").hide();
			labelVeriflag=isbtreturnflag;
	}else if($("input[name='"+isbt+"']:checked").val()==0){
			$("#isbtattach").hide();
			labelVeriflag=true;
	}else if(!$("input[name='"+isbt+"']").attr(":checked")){
			$("#isbtattach").show();
			labelVeriflag=false;
			return labelVeriflag;
	}
	var warninglbl=$(".warnlblclass").attr("name");
	if($("input[name='"+warninglbl+"']:checked").val()==1){
	
			var warnreturnflag=warnlblcheck();
		
			$("#warnlblattch").hide();
			labelVeriflag=warnreturnflag;
	}else if($("input[name='"+warninglbl+"']:checked").val()==0){
			$("#warnlblattch").hide();
			labelVeriflag=true;
	}else if($("input[name='"+warninglbl+"']:checked").val()==-1){
			$("#warnlblattch").hide();
			labelVeriflag=true;
	}else if(!$("input[name='"+isbt+"']").attr(":checked")){
			$("#warnlblattch").show();
			labelVeriflag=false;
			return labelVeriflag;
	}
	var autolbl=$(".warnlblclass").attr("name");
	if($("input[name='"+autolbl+"']:checked").val()==1){
			$("#autolbl").hide();
			return true;
	}else if($("input[name='"+autolbl+"']:checked").val()==0){
			$("#autolbl").hide();
			labelVeriflag=true;
	}else if($("input[name='"+autolbl+"']:checked").val()==-1){
			$("#autolbl").hide();
			labelVeriflag=true;
	}else if(!$("input[name='"+autolbl+"']").attr(":checked")){
			$("#autolbl").show();
			labelVeriflag=false;
			return labelVeriflag;
	}
	
	return labelVeriflag;
}
function warnlblcheck(){
			 var warnreactive=$(".warnrecativeclass").attr('name');
			 
				if($("input[name='"+warnreactive+"']:checked").val()==1){
					$("#warnreactive").hide();
					//return true;
				}else if($("input[name='"+warnreactive+"']:checked").val()==0){
					$("#warnreactive").hide();
					//return true;
				}else if($("input[name='"+warnreactive+"']:checked").val()==-1){
					$("#warnreactive").hide();
					//labelVeriflag=true;
				}else if(!$("input[name='"+warnreactive+"']").attr(":checked")){
					$("#warnreactive").show();
					return false;
				}
				
		var warnpatient=$(".warnpatientclass").attr('name');
				if($("input[name='"+warnpatient+"']:checked").val()==1){
					$("#warnpatient").hide();
					//return true;
				}else if($("input[name='"+warnpatient+"']:checked").val()==0){
					$("#warnpatient").hide();
					//return true;
				}else if($("input[name='"+warnpatient+"']:checked").val()==-1){
					$("#warnpatient").hide();
					//return true;
				}else if(!$("input[name='"+warnreactive+"']").attr(":checked")){
					$("#warnpatient").show();
					return false;
				}
	
		
		var warnnoteval=$(".warnnotevalclass").attr('name');
				if($("input[name='"+warnnoteval+"']:checked").val()==1){
					$("#warnnoteval").hide();
					//return true;
				}else if($("input[name='"+warnnoteval+"']:checked").val()==0){
					$("#warnnoteval").hide();
					//return true;
				}else if($("input[name='"+warnnoteval+"']:checked").val()==-1){
					$("#warnnoteval").hide();
					//return true;
				}else if(!$("input[name='"+warnnoteval+"']").attr(":checked")){
					$("#warnnoteval").show();
					return false;
				}
		var warnbiozard=$(".biohazardclass").attr('name');
				if($("input[name='"+warnbiozard+"']:checked").val()==1){
					$("#warnbiozard").hide();
					//return true;
				}else if($("input[name='"+warnbiozard+"']:checked").val()==0){
					$("#warnbiozard").hide();
					//return true;
				}else if($("input[name='"+warnbiozard+"']:checked").val()==-1){
					$("#warnbiozard").hide();
					//return true;
				}else if(!$("input[name='"+warnbiozard+"']").attr(":checked")){
					$("#warnbiozard").show();
					return false;
				}	
			return true;
}
function ISBTcheck(){
	var isbtcom=$(".isbtcomclass").attr('name');
	
	if($("input[name='"+isbtcom+"']:checked").val()==1){
		$("#isbtlabelcom").hide();
		return true;
	}else if($("input[name='"+isbtcom+"']:checked").val()==0){
		$("#isbtlabelcom").hide();
		return true;
	}else if(!$("input[name='"+isbtcom+"']").attr(":checked")){
		$("#isbtlabelcom").show();
		return false;
	}
}


function saveQuestions(tableid,entityId,entityType){
		var donorid= $("#donor").val();
		var donorProcedure = $("#donorProcedure").val();
		var rowData="";
		var questioncode;
		var questionvalue;
		var questionval=0;
		var inputtype;
		var questionsid =0;
		var comments="";
		if(typeof(donorid)=='undefined'){
			donorid="";
		}
		if(typeof(donorProcedure)=='undefined'){
			donorProcedure="";
		}
		if(typeof(entityType)=='undefined'){
			entityType="";
		}
		if(typeof(entityId)=='undefined'){
			entityId="";
		}
		$("#"+tableid).find("#questionsCode").each(function(){
			questioncode=$(this).val();
			questionsid = $("#"+questioncode +"_id").val();
			if(questionsid == undefined){
				questionsid=0;
			}
			//alert(questioncode);
			inputtype=$("[name="+questioncode+"]").prop('type');
			if(inputtype=='text'){
				questionvalue=$("#"+tableid+" tbody tr").find("input:text[name="+questioncode+"]").val();
			}else if(inputtype=='radio'){
				questionvalue=$("#"+tableid+" tbody tr").find("input:radio[name="+questioncode+"]:checked").val();
			}else if(inputtype=='select-one'){
				questionvalue=$("#"+tableid+" tbody tr").find("select[name="+questioncode+"]").val();
			}
			else if(inputtype=='checkbox'){
				questionvalue=$("#"+tableid+" tbody tr").find("input:checkbox[name="+questioncode+"]:checked").val();
				if(questionvalue==undefined){
					questionvalue=0;
					
				}
			}else if(inputtype=='textarea'){
				
				qcomments = $("[name="+questioncode+"]").attr("id");
				comments = $("#"+qcomments).val();
			}
			
			if(questionvalue!= 'undefined' && questionvalue!=null && questionvalue!=''){
				rowData+= "{questions:'"+questionsid+"',questionsCode:'"+questioncode+"',questionsValue:'"+questionvalue+"',questionsComments:'"+comments+"',donor:'"+donorid+"',donorplannedProcedure:'"+donorProcedure+"',entityId:'"+entityId+"',entityType:'"+entityType+"'},";
			}
		}); 
		//alert(rowData);
		 if(rowData.length >0){
	         rowData = rowData.substring(0,(rowData.length-1));
	     }
		 //alert("rowData::"+rowData);
				var jsonData = "jsonData={questionData:["+rowData+"]}";
           // alert(jsonData);
			  url="saveQuestions";
				 var response = jsonDataCall(url,jsonData);
				 // alert(url);
	 
	 //?Mari alert("Data saved Successfully");
	// stafaAlert('Data saved Successfully','');
		return true;		
				
	}
 
 
 function loadquestions(entityId,entityType){
	 
		var url = "loadquestionData";
		var donorid=$("#donor").val();
		var donorprocedure=$("#donorProcedure").val();
		
		
		response = jsonDataCall(url,"jsonData={donor:"+donorid+",donorplannedProcedure:"+donorprocedure+",entityId:"+entityId+",entityType:"+entityType+"}");
		
		$(response.questionList).each(function(i){
			//var questionValue=$("input[name="+this.questionsCode+"]").val();
			//alert("response--->"+JSON.stringify(response.questionList));
			inputtype=$("[name="+this.questionsCode+"]").prop('type');
			$("#"+this.questionsCode +"_id").val(this.questions);
			if(inputtype=='text'){
				$("#"+this.questionsCode).val(this.questionsValue);
			}else if(inputtype=='radio'){
				if(this.questionsValue==1){
					if(this.questionsCode=='isbtlblatt'){
						  $('.isbtlabelshow').show();
					}
					if(this.questionsCode=='warnlblatt'){
						  $('.warninglabel').show();
					}
					if(this.questionsCode=='postisbtlblatt'){
						  $('.isbtlabelshow').show();
					}
					if(this.questionsCode=='postwarnlblatt'){
						  $('.warninglabel').show();
					}
					if(this.questionsCode=='postPlasmaisbtlblatt'){
						  $('.isbtlabelshow').show();
					}
					if(this.questionsCode=='postPlasmawarnlblatt'){
						  $('.warninglabel').show();
					}
					if(this.questionsCode=='plasmaisbtlblcom'){
						  $('.isbtlabelshow').show();
					}
					if(this.questionsCode=='plasmawarnlblatt'){
						  $('.warninglabel').show();
					}
				}else if(this.questionsValue==0){
					if(this.questionsCode=='isbtlblatt'){
						  $('.isbtlabelshow').hide();
					}
					if(this.questionsCode=='warnlblatt'){
						  $('.warninglabel').hide();
					}
					if(this.questionsCode=='postisbtlblatt'){
						  $('.isbtlabelshow').hide();
					}
					if(this.questionsCode=='postwarnlblatt'){
						  $('.warninglabel').hide();
					}
					if(this.questionsCode=='plasmaisbtlblcom'){
						  $('.isbtlabelshow').hide();
					}
					if(this.questionsCode=='plasmawarnlblatt'){
						  $('.warninglabel').hide();
					}
					if(this.questionsCode=='postPlasmaisbtlblatt'){
						  $('.isbtlabelshow').hide();
					}
					if(this.questionsCode=='postPlasmawarnlblatt'){
						  $('.warninglabel').hide();
					}
				}
				$('input:radio[name='+this.questionsCode+'][value="'+this.questionsValue+'"]').attr('checked', 'checked');  
			}
			else if(inputtype=='select-one'){
				$("#"+this.questionsCode).val(this.questionsValue);
			}else if(inputtype=='checkbox'){
				if(this.questionsValue==1){
					$('#'+this.questionsCode).attr('checked',true);
				}else if(this.questionsValue==0){
					$('#'+this.questionsCode).attr('checked',false);
				}
			}
			else if(inputtype=='textarea'){
			if(this.questionsCode!= 'undefined'&& this.questionsCode!= null){
				$("#"+this.questionsCode).val(this.questionsComments);
			}
			}
		});
		
		
	}
 
 function saveProductQuestion(processType,tableid){     
     //  var donorid= $("#donor").val();
	//	var donorProcedure = $("#donorProcedure").val();
		
	   // var proProcessId;
		var rowData="";
		var questioncode;
		var questionvalue;
		var questionval=0;
		var inputtype;
		var questionsid =0;
		var comments="";
		$("#"+tableid).find("#questionsCode").each(function(){
			questioncode=$(this).val();
			questionsid = $("#"+questioncode +"_id").val();
			if(typeof(questionsid) == 'undefined'){
				questionsid=0;
			}
			//alert(questioncode);
			inputtype=$("[name="+questioncode+"]").prop('type');
			if(inputtype=='text'){
				questionvalue=$("#"+tableid+" tbody tr").find("input:text[name="+questioncode+"]").val();
			}else if(inputtype=='radio'){
				questionvalue=$("#"+tableid+" tbody tr").find("input:radio[name="+questioncode+"]:checked").val();
			}else if(inputtype=='select-one'){
				questionvalue=$("#"+tableid+" tbody tr").find("select[name="+questioncode+"]").val();
			}
			else if(inputtype=='checkbox'){
				questionvalue=$("#"+tableid+" tbody tr").find("input:checkbox[name="+questioncode+"]:checked").val();
				if(questionvalue==undefined){
					questionvalue=0;
					
				}
			}else if(inputtype=='textarea'){
				
				qcomments = $("[name="+questioncode+"]").attr("id");
				comments = $("#"+qcomments).val();
				
			}
			
			if(questionvalue!= 'undefined' && questionvalue!=null && questionvalue!=''){
				rowData+= "{questions:'"+questionsid+"',questionsCode:'"+questioncode+"',questionsValue:'"+questionvalue+"',questionsComments:'"+comments+"',entityType:'"+processType+"'},";
			}
		}); 
		
		 if(rowData.length >0){
	         rowData = rowData.substring(0,(rowData.length-1));
	     }
		
		// var jsonData = "jsonData={questionData:["+rowData+"]}";
         
		return rowData;	
		
}	