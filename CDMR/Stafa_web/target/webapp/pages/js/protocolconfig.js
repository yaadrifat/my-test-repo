$("#preview").dialog("destroy");

$(function() {
	
$( ".portlet" ).find( ".newprotocoladdnew" )
.append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right; width:5px;\" class='ui-addnew'></span>");

/** for Porotocol **/
$( ".portlet .newprotocoladdnew .ui-addnew" ).click(function() {
	//alert("add protocol");
	showPopUp("open","addProtocol","Add Protocol  ","700","350");
});

});



/*
var inputCount =0;
function dropevent(){
	
    $( "#dropDiv div").each(function (i, element){
        $(element).find("input").each(function(j){
        	if($(this).hasClass("pclabel")){
        		 setObjectAutoComplete($(this),"calcLabel");
        	}else if($(this).hasClass("pctext")){
        		//alert("TEXT");
        		 setObjectAutoComplete($(this),"calcFields",true);
        	}else if($(this).hasClass("pccalc")){
        		 $(this).bind("click",function(){
        		        showFormula($(this));
        		 });
       		}else if($(this).hasClass("pcdrop")){
	    		//alert("TEXT");
	   		 setObjectAutoComplete($(this),"calcdrop");
	       	}
        });
	});
	
    $( "#dropDiv" ).droppable({
        drop: function( event, ui ) {
            
		var dropDownFlag = false;
                dropObj = $(ui.draggable).find("input");
                dropid = $(dropObj).attr("id");
                if( dropid == "pclabel"){
                    setObjectAutoComplete(dropObj,"calcLabel");
                }else if( dropid == "pctext"){
                    setObjectAutoComplete(dropObj,"calcFields",true);
                }else if(dropid == "pccalc"){
                    $(dropObj).bind("click",function(){
                        showFormula($(this));
                    } );
                }else if(dropid == "pcdrop"){
                	//bb inputCount+=1;
                	//bb $(dropObj).attr("id",("pcdrop_"+inputCount));
                	setObjectAutoComplete(dropObj,"calcdrop");
                }
              
              if($(ui.draggable).find("img").html() == null){
            	//  alert("inside if1");
            	   $(dropObj).after('<a href="#" onclick="javascript:destroy(this);"><img src="images/icons/closebox.png"	height="16" width="16" class="destroy"/></a>');
            		// $(dropObj).before('<a href="#" onclick="javascript:destroy(this);"><img src="images/icons/closebox.png"	height="16" width="16" class="destroy"/></a>');
              }
        }
     });

    $( ".dropItem" ).sortable({
        connectWith: ".dropItem",
    });
    
   
  }
*/

/*//?BB not in use moved to widget_maintenance.jsp
function setFormula(){
    //alert("setFormula" + tmpObj);
    //alert( " formula " + $("#formulText").val());
    //alert( " formula " + $("#hformulText").val());
    $(tmpObj).val($("#hformulText").val());
    $("#preview").dialog("destroy");
}

function createFormula(obj){
	//alert("createFormula" + $(obj).text());
	
    var tmpval = $("#formulaText").val();
    var tmphval = $("#hformulText").val();

    if(tmpval ==""){
        tmpval = $("#formulaname").val() + "=";
        tmphval = $("#formulaname").val() + "=";
    }
    var tmpInputval = $("#inputs").val();
    //alert(" tmpInputval 1" + tmpInputval);
    
   
   
    if(tmpInputval !=""){
    	tmpval+= $("#inputs").val();
    	 tmpInputval = tmpInputval.substring(tmpInputval.indexOf("->")+2);
    	    //alert(" tmpInputval 2" + tmpInputval);
    	    if(tmpInputval.indexOf("=")>0){
    	        tmpInputval = tmpInputval.substring(0,(tmpInputval.indexOf("=")));
    	    }
    	    //alert(" tmpInputval 3" + tmpInputval);
    	    tmphval+= tmpInputval;
    }
    //alert("tmphval " + tmphval);
    if($(obj).text() != ""){
    	tmpval+= $(obj).text();
    	tmphval+=$(obj).text();
    }
    
    $("#formulText").val(tmpval );
    $("#hformulText").val(tmphval );
   
    $("#opr").val("");
    $("#inputs").val("");


}
*/
var tmpObj;


 function closeProtocol(){
	 jQuery("#addProtocol").dialog("destroy");
 }
 
 function checkQcValidation(obj){
		//alert(" checkQcValidation ");
		//alert(obj.value + " / " + obj.checked);
		var oldqcs = $("#oldQCs" ).val();
		var addedQC = $("#addedQCs").val(); 
		var deletedqc = $("#deletedQCs").val();
		var tmpValue = ","+obj.value+",";
		if(obj.checked){
			if(oldqcs.indexOf(tmpValue)== -1){
				addedQC+=obj.value+",";
			}
			if(deletedqc.indexOf(tmpValue)!= -1) {
				deletedqc = deletedqc.replace(tmpValue,',');
			}
		}else{
			if(oldqcs.indexOf(tmpValue)!= -1){
					deletedqc+=obj.value+",";
			}else if (addedQC.indexOf(tmpValue)!= -1){
				addedQC = addedQC.replace(tmpValue,',');
			}
		}
		$("#addedQCs").val(addedQC);

		$("#deletedQCs").val(deletedqc);
	}
 
 

 function destroy(obj){
 	$(obj).parent().remove();
 }

  

 
 
 
 /*Draggable Items functions Start*/
 $( ".dragItem ul" ).draggable({ revert: "invalid", connectToSortable: '.dropItem', helper: 'clone'});
 //$( "input[type='text']" ).disableSelection();
 /*Draggable Items functions End*/

 /*Droppable Items functions Start*/
  $( ".dropItem" ).sortable({
     connectWith: ".dropItem",
 });
 /*Droppable Items functions End*/
  
//------------------------ freezed methods ----------------------------  
  var elementcount =0;
 	  
	  function deleteRows(){
			 // alert("Delete Rows");
		    var yes=confirm(confirm_deleteMsg);
			if(!yes){
				return;
			}
			  var deletedIds = "";
			  try{
					$("#porotocolTable tbody tr").each(function (row){
						 if($(this).find("#protocol").is(":checked")){
							 deletedIds += $(this).find("#protocol").val() + ",";
						 }
					 });
				  if(deletedIds.length >0){
					  deletedIds = deletedIds.substr(0,deletedIds.length-1);
				  }
				//  alert("deletedIds " + deletedIds);
				  jsonData = "{deletedProtocols:'"+deletedIds+"'}";
				  url="deleteProtocol";
			        //alert("json data " + jsonData);
			     response = jsonDataCall(url,"jsonData="+jsonData);
			     constructProtocolTable(true);
			  }catch(error){
					alert("error " + error);
				}
	  }
	  
	
	

	 
	  
  
 