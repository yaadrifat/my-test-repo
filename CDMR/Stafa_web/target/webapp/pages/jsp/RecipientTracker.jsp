<%@ include file="common/includes.jsp" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/workFlow.js"></script>
<link type="text/css" href="css/workFlow.css" rel="stylesheet" />
<script>
$(function() {
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
    .append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span>");
    $('.jclock').jclock();
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });
  });
$(document).ready(function(){
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"}); 
	  $("select").uniform(); 
      $(window).resize();
	show_eastslidewidget("slidedivs");
	var oTable=$("#recipientTracker").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											             ],
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#recipientTrackerColumn").html($('.ColVis:eq(0)'));
												}
		});
	$('#recipientTracker tbody[role="alert"] tr').bind('click',function(){
		var form="";
		var url = "loadReferralPage";
		var result = ajaxCall(url,form);
		$("#main").html(result);
		}); 
		oTable=$("#recipientInsuranceDocuments").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										             ],
	 										"oColVis": {
	 											"aiExclude": [ 0 ],
	 											"buttonText": "&nbsp;",
	 											"bRestore": true,
	 											"sAlign": "left"
	 										},
	 										"fnInitComplete": function () {
	 									        $("#recipientInsuranceDocumentsColumn").html($('.ColVis:eq(1)'));
	 										}
									});
	 oTable=$("#patientBenefitsDocuments").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										             ],
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#patientBenefitsDocumentsColumn").html($('.ColVis:eq(2)'));
												}
									});
	 
    $("select").uniform(); 
});
</script>

<div class="cleaner"></div>

<!--right window start -->	
<div  id="slidedivs"  style="display:none">
<div id="uldiv">
<ul class="ulstyle">
<li class="listyle print_label"><img src = "images/icons/calendar.png"  style="width:30px;height:30px;"/></li>
<li class="listyle print_label" ><b>Calendar</b> </li>
<li class="listyle print_label"><img src = "images/icons/plan.png"  style="width:30px;height:30px;"/></li>
<li class="listyle notes_icon"><b>Plan</b> </li>
<li class="listyle print_label"><img src = "images/icons/Notification.png"  style="width:30px;height:30px;"/></li>
<li class="listyle print_label" ><b>Notification</b> </li>
</ul>
</div>
</div>
<form>
<div class="cleaner"></div>
		<div class="column">		
			<div  class="portlet">
				  <div class="portlet-header addnew">Recipient Tracker</div>
						<div class="portlet-content">
						<table cellpadding="0" cellspacing="0" border="0" id="recipientTracker" class="display">
						<thead>
							<tr>
								<th width="4%" id="recipientTrackerColumn"></th>
								<th colspan='1'>Select</th>
								<th colspan='1'>RecipientId</th>
								<th colspan='1'>Last Name</th>
								<th colspan='1'>First Name</th>
								<th colspan='1'>DOB</th>
								<th colspan='1'>Age</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Type of BMT</th>
								<th colspan='1'>BMT MD</th>
								<th colspan='1'>Plan</th>
								<th colspan='1'>Dept</th>
								<th colspan='1'>Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="recipientInsuranceVerified" onclick=""></td>
								<td>HCA423479</td>
								<td>Branofsky</td>
								<td>Renee</td>
								<td>Jan 19, 1988</td>
								<td>24</td>
								<td>AML</td>
								<td>Allo</td>
								<td>GR</td>
								<td >New Referral. Patient has a matched Sibling. Awaiting approval for Consult.</td>
								<td>Adults</td>
								<td>Pre-Transplant</td>
							</tr>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="recipientInsuranceVerified" onclick=""></td>
								<td>HCA877665</td>
								<td>Solikov</td>
								<td>Dori</td>
								<td>Aug 19, 1963</td>
								<td>49</td>
								<td>MM</td>
								<td>Auto</td>
								<td>TS</td>
								<td>New Referral</td>
								<td>Adults</td>
								<td>Pre-Transplant</td>
							</tr>
						</tbody>
						</table>
			</div>
		</div>
</div>
<div class="cleaner"></div>
<div class="cleaner"></div>
	
			

<%-- <div class="floatright_right">
	<jsp:include page="sidecontrols.jsp"></jsp:include>
 </div> --%>
 
<div class="cleaner"></div>
		
<div align="right" style="float:right;">

		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
			<tr>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;&nbsp;</td>
				<td><input type="button" value="Next" onclick="saveOvernightStorage()"/></td>
			</tr>
		</table>

</div>
</div>
</form>


<!-- <script type="text/javascript">



var now = new dateEntry();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivaldateEntry').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new dateEntry()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};
$("#timeZone").html(timezones[-offset / 60]);

</script>
 -->













