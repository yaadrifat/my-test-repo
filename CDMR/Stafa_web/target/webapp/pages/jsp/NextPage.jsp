<%@ taglib prefix="s" uri="/struts-tags"%>
<script >

function saveRecipientStatus()
{

	var personId = '<s:property value="personId"/>';
	var pkRecipient='<s:property value ="recipient"/>';
	var transplantId ='<s:property value ="transplantId"/>';
	var entityType ="recipient";

var recipientData ="{fkPerson:"+personId+",";
recipientData += "recipientStatus:'"+$("#recipientStatus").val()+ "',";
recipientData += "pkRecipient:"+pkRecipient+",comments:'"+$("#recipientStatusComments").val()+"'}";

var transplantData ="{pkTxWorkflow:"+transplantId+",";
transplantData += "entityId:'"+pkRecipient+ "',";
transplantData += "entityType:"+entityType+",fkRecipientStatus:'"+$("#recipientStatus").val()+"'}";

var url = "saveRecipientStatus";

var jsonData = "transplantData:"+transplantData+",recipientData:"+recipientData;
response = jsonDataCall(url,"jsonData={"+jsonData+"}");


return true;  
}

function saveDisclaimer()
{
	
	var question1="";
	var question2="";
if($('input[name=disclaimerQuestion1]:radio:checked').val()=='true')
	{
	question1=true;
	}
else if($('input[name=disclaimerQuestion1]:radio:checked').val()=='false')
	{
	question1=false;
	}
if($('input[name=disclaimerQuestion2]:radio:checked').val()=='true')
{
question2=true;
}
else if($('input[name=disclaimerQuestion2]:radio:checked').val()=='false')
{
question2=false;
}

var personId = '<s:property value="personId"/>';
var pkRecipient='<s:property value ="recipient"/>';
var transplantId ='<s:property value ="transplantId"/>';
var visitId = '<s:property value ="visitId"/>';

var diclaimerData ="{entityId:"+personId+",";
 if($("#pkDisclamier").val()!=null)
	{
	diclaimerData += "pkDisclaimer:'"+$("#pkDisclamier").val()+ "',";
	} 
diclaimerData += "disclaimerCode:'"+$("#disclaimerCode").val()+ "',";
diclaimerData += "fkTxId:'"+transplantId+ "',";
diclaimerData += "fkVisitId:'"+visitId+ "',";
diclaimerData += "entityType:'"+$("#entityType").val()+ "',";
diclaimerData += "question2:'"+question2+ "',";
diclaimerData += "question1:"+question1+",comment1:'"+$("#disclaimerComment1").val()+"'}";

var url = "saveDisclaimer";
response = jsonDataCall(url,"jsonData={data:["+diclaimerData+"]}");
return true;  
}

function savePages(mode){

	if(checkModuleRights(STAFA_RECIPIENTTRACKERS, APPMODE_SAVE)){

		try{
		
							//Save Disclaimer
							if(saveDisclaimer()){								
				 				 }
							
							 if(saveRecipientStatus()){								
							  }
							
							/*Save Documents*/
							jAlert("  Data Saved Successfully !", "Information ...", function () { 
								openUrl(LoadedPage.url,LoadedPage.workflow,LoadedPage.visitId,LoadedPage.div);
								if(RefreshData!=undefined){
								  Refreshner(RefreshElement,RefreshData);
								}
			                }); 

			                return true;
						
					
			}catch(e){
			alert("exception " + e);
		}
	}
}
</script>
<div  class="column">
  <div  class="portlet">
        <div class="portlet-header"><s:text name= "stafa.label.recipientDisclaimers"/></div>
    		<jsp:include page="disclaimer.jsp">
						<jsp:param value="referralDisclaimer" name="disclaimerType"/>
						</jsp:include>	 
    </div>
</div>
<div  class="column">
  <div  class="portlet">
        <div class="portlet-header"><s:text name= "stafa.label.recipientStatus"/></div>
        <div class="portlet-content">
        
        <jsp:include page="recipient_status.jsp"></jsp:include>
       		    
        </div>
    </div>
    </div>
	