<%@ include file="common/includes.jsp" %>
<script type="text/javascript" src="js/placeholder.js"></script>
<script type="text/javascript" src="js/dataSearch.js"></script>

<script>


 function getScanData(event,obj){
	  if(event.keyCode==13){
		//getAccessionData(event,obj);
		getdata(event);
	}
}
function getdata(e){
		var url = "loadphysicianData";
		var productSearch = $("#fake_conformProductSearch").val();
		if(e.keyCode==13 && productSearch!=""){
			e.preventDefault();	
			var response = ajaxCall(url,"physicianPendingForm");
			var listLength=response.maintenanceList.length;
			if(listLength>0){
				 $('#searchtxt').val(productSearch);
				 searctrigger(e,productSearch);
				 $('#pendingRecipientTable tbody[role="alert"] tr').bind('click',function(){
					var recipientId = $(this).children("td:nth-child(1)").find("#refPerson").val();
					var url = "loadPhysicianDonor";
					var result = ajaxCallWithParam(url,recipientId);
					$("#main").html(result);
					}); 
			}
			else
				{
				alert("Recipient Not found in the list");
				}
	} 
 
 } 

 function searctrigger(e,productSearch){
	    if((e.keyCode==13 && productSearch!="")){
	    	
	    $("#searchtxt").trigger(e);
	    }
	    if(e.keyCode ==8 && productSearch!="") {
	        $("#searchtxt").trigger(e);
	          return false;   
	        }else if (e.keyCode !=8 && e.keyCode != 13 ){
	            $("#searchtxt").trigger(e);
	            return false;
	        }
	    return false;
	}
$(function() {
	/* $('#previouse').attr("disabled", true);
	$('#next').attr("disabled", false);	
	$('#currentPage').val("s1"); */
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	/**for Next Button Start**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".next" )
	.append("<span style=\"float:right;\" class='ui-next'></span>");
	
	$( ".portlet-header .ui-next " ).click(function() {
		loadPage('jsp/physician_products.jsp');	
	});
	/**for Next Button End**/
	
	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open');
	});

	

	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});
/*Physician PendingRecipient Table Server Parameter Starts*/
	
	var pending_serverParam = function ( aoData ) {
		
		aoData.push( { "name": "application", "value": "stafa"});
		aoData.push( { "name": "module", "value": "physician_selectrecipient_pendingrecipient"});	
		
		aoData.push( {"name": "col_1_name", "value": "" } );
		aoData.push( { "name": "col_1_column", "value": ""});
		
		aoData.push( { "name": "col_2_name", "value": " lower(PERSON_ALTID)"});
		aoData.push( { "name": "col_2_column", "value": "lower(PERSON_ALTID)"});
		
		aoData.push( {"name": "col_3_name", "value": " lower(person_lname)" } );
		aoData.push( { "name": "col_3_column", "value": "lower(person_lname)"});
		
		aoData.push( {"name": "col_4_name", "value": " lower(to_char(PERSON_DOB,'Mon dd, yyyy  HH:MM'))" } );
		aoData.push( { "name": "col_4_column", "value": "PERSON_DOB"});
		
		aoData.push( {"name": "col_5_name", "value": " lower((select CODELST_DESC from er_codelst where PK_CODELST=person_diagnosis))" } );
		aoData.push( { "name": "col_5_column", "value": "lower(person_diagnosis)"});
		
		aoData.push( {"name": "col_6_name", "value": " lower(person_weight)" } );
		aoData.push( { "name": "col_6_column", "value": "lower(person_weight)"});
		
		aoData.push( {"name": "col_7_name", "value": "" } );
		aoData.push( { "name": "col_7_column", "value": ""});
		
		aoData.push( {"name": "col_8_name", "value": " lower(PERSON_DOB)" } );
		aoData.push( { "name": "col_8_column", "value": "lower(PERSON_DOB)"});
		
		};
	var pending_ColManager = function(){
		$("#pendingRecipientColumn").html($('.ColVis:eq(0)'));
	};	
	var pending_aoColumnDef = [
	               	                  {		
	            	                	  "aTargets": [ 0], "mDataProp": function ( source, type, val ) {
	            	                	   
	                            	 		return "<input type='hidden' id='refPerson' value="+source[6]+">";
	                            	     }},
	            	                  { 	"sTitle": "Transplant Physician",
	                            	    	 "aTargets": [ 1], "mDataProp": function ( source, type, val ) { 
	            	                	 		return "";
	            	                	 }},
	            					  {"sTitle": "Recipient ID",
	            	                		 "aTargets": [ 2], "mDataProp": function ( source, type, val ) { 
	            							    return source[0];
	            							    }},
	            					  {"sTitle": "Recipient Name",
	            							    	"aTargets": [ 3], "mDataProp": function ( source, type, val ) {
	            						 	   return source[1];
	            					  		}},
	            					  {"sTitle": "Recipient DOB",
	            					  			"aTargets": [ 4], "mDataProp": function ( source, type, val ) { 
	            						  		return source[4];
	            						  }},
	            					  {"sTitle": "Diagnosis",
	            							  "aTargets": [ 5], "mDataProp": function ( source, type, val ) { 
	            						  		return source[5];
	            						  }},
	            					  {"sTitle": "Recipient Weight(kg)",
	            							  "aTargets": [ 6], "mDataProp": function ( source, type, val ) { 
	            						  		return source[3];
	            						  }},
	            					  {"sTitle": "Planned Protocol",
	            							  "aTargets": [ 7], "mDataProp": function ( source, type, val ) { 
	            						  		return "";
	            						  }},
	            					  {"sTitle": "Planned Infusion Date",
	            							  "aTargets": [ 8], "mDataProp": function ( source, type, val ) { 
	            						  		return source[4];
	            						  }}
	            					];
	var pending_aoColumn = [ { "bSortable": false},
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null
				               ];
/*Physician PendingRecipient Table Server Parameter Ends*/
 
$(document).ready(function() {
	hide_slidecontrol();
	hide_slidewidgets();
	constructTable(false,'pendingRecipientTable',pending_ColManager,pending_serverParam,pending_aoColumnDef,pending_aoColumn);
	$('#pendingRecipientTable tbody[role="alert"] tr').bind('click',function(){
		var recipientId = $(this).children("td:nth-child(1)").find("#refPerson").val();
		var url = "loadPhysicianDonor";
		var result = ajaxCallWithParam(url,recipientId);
		$("#main").html(result);
		}); 
}); 
/*$('#pendingRecipientTable tbody[role="alert"] tr').bind('click',function(){
	
	var recipientId = $(this).children("td:nth-child(1)").find("#refPerson").val();
	var url = "loadPhysicianDonor";
	var result = ajaxCallWithParam(url,recipientId);
	$("#main").html(result);
	//loadPage('jsp/physician_selectDonor.jsp');
	});
	*/
</script>

<div class="cleaner"></div>

<!-- section ends here-->

<section>

 <div class="cleaner"></div>
	<form id="physicianPendingForm" name="physicianPendingForm">
	
	
<div class="column">		
		<div  class="portlet">
			<div class="portlet-header">Pending Recipient List</div><br>
		<div class="portlet-content">
		<div id="button_wrapper">
				<div style="margin-left:40%;">
					<input class="scanboxSearch barcodeSearch"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID" onfocus='changeText(this);' style="width:210px;"/>
					<input class="scanboxSearch barcodeSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" />
					
				</div>
			</div>
		<table cellpadding="0" cellspacing="0" border="0" id="pendingRecipientTable" class="display">
		<thead>
			<tr>
				<th width="4%" id="pendingRecipientColumn"></th>
				<th colspan="1">Transplant Physician</th>
				<th colspan="1">Recipient Id</th>
				<th colspan="1">Recipient Name</th>
				<th colspan="1">Recipient DOB</th>
				<th colspan="1">Diagnosis</th>
				<th colspan="1">Recipient Weight(kg)</th>
				<th colspan="1">Planned Protocol</th>
				<th colspan="1">Planned Infusion Date</th>
				
			</tr>
		</thead>
		</table>			
			</div>
		</div>
	</div>
	
</form>
</section>