<%@ include file="common/includes.jsp" %>
<!-- <script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script> -->
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
	<% 
	String path = request.getContextPath()+"/pages/";
	System.out.println("path:::"+path);
		%>
<script>
$(document).ready(function(){
	innerLayout.show('east',false);
	innerLayout.show('south',true);
	innerLayout.show('north',true);
	innerLayout.show('west',false);
	$("#innereast").load('<%=path%>jsp/sidecontrols.jsp');
	$("#innerwest").load('<%=path%>jsp/nurse_slidewidgets.jsp');
	var oTable=$("#disposableTable").dataTable({
										"bJQueryUI": true,
										"sDom": 'C<"clear">Rlfrtip',
										"aoColumns": [ { "bSortable":false},
 										               null,
 										               null,
 										               null,
 										               null,
 										               null,
 										               null,
 										             ],
 										"oColVis": {
 											"aiExclude": [ 0 ],
 											"buttonText": "&nbsp;",
 											"bRestore": true,
 											"sAlign": "left"
 										},
 										"fnInitComplete": function () {
 									        $("#disposableColumn").html($('.ColVis:eq(0)'));
 										}
									});
	 oTable.thDatasorting('disposableTable');
	 oTable=$("#reagenTable").dataTable({
										"bJQueryUI": true,
										"sDom": 'C<"clear">Rlfrtip',
										"aoColumns": [ { "bSortable":false},
 										               null,
 										               null,
 										               null,
 										               null,
 										               null,
 										               null,
 										             ],
											"oColVis": {
												"aiExclude": [ 0 ],
												"buttonText": "&nbsp;",
												"bRestore": true,
												"sAlign": "left"
											},
											"fnInitComplete": function () {
										        $("#reagenColumn").html($('.ColVis:eq(1)'));
											}
									});
	 oTable.thDatasorting('reagenTable');
    $("select").uniform(); 
});

/* function getAcquisitionData(e,This){
	
		var runningTabClass= $('#Runningtotaltable tbody tr td:nth-child(1)').attr("class");  			
		if((runningTabClass=="dataTables_empty")){
			$('#Runningtotaltable').dataTable().fnAddData([productSearch,0]);}
		var productVal = $('#Runningtotaltable tbody tr td:nth-child(1)').html();	
		if((productVal==productSearch)){
			getScanned(e,this);}}						
		else if((e.keyCode==13 && productSearch!="")){
	var flag =checkProduct(productSearch);
	
		if(flag)
			{
			getScanned(e,this);	
			}
			else
			{
			}			
	}
} */
</script>

<div class="cleaner"></div>
<!-- <div id="floatMenu">
	<div id="forfloat_wind">
		<div id="box-outer">
			<a href="#" style="text-decoration:none" id="blind">&lt;&lt; </a>
				<div id="box">
					<div id="leftnav" >
				
Patient Info start
			<div class="column">
				<div  class="portlet">
					<div class="portlet-header notes">Patient Info</div>
						<div class="portlet-content">
							<table width="100%" border="0">
							  <tr>
								<td width="50%">ID:</td>
							  </tr>
							  <tr>
								<td>Name:</td>
							  </tr>
							  <tr>
								<td>DOB:</td>
							  </tr>
							  <tr>
								<td>ABO/Rh:</td>
							  </tr>
							  <tr>
								<td>Procedure:</td>
							  </tr>
							   <tr>
								<td>Donation:</td>
							  </tr>
							   <tr>
								<td>BSA:</td>
							  </tr>
							   <tr>
								<td>BV:</td>
							  </tr>
							</table>
						</div>
				</div>
			</div>
				
Patient Info

Lab Results start
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header notes">Lab Results</div>
						<div class="portlet-content">
							<table width="100%" border="0">
								
							</table>
						</div>
				</div>
				</div>
				
Lab Results

Recipient Info start
				<div class="column">
					<div class="portlet">
						<div class="portlet-header notes">Recipient Info</div>
							<div class="portlet-content">
							
							</div>
					</div>
				</div>
				
Recipient Info end

				</div>
			</div>
		</div>
	</div>
</div> -->
<!--float window end-->

<!--right window start -->	

<div class="cleaner"></div>


<div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes">Pre-Cleaning</div>
				<div class="portlet-content">
				<table>
				<tr>
					<td style="width:20%"><div>Has the machine been pre-cleaned?&nbsp;<input type="checkbox" id="" onclick="">Yes</div></td>
				</tr>
				</table>
				</div>
			</div>
			
		</div>
	
<div class="cleaner"></div>


<div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes">Procedure</div>
				<div class="portlet-content">
				<div><b>Disposable and Supplies</b></div>
				<table cellpadding="0" cellspacing="0" border="0" id="disposableTable" class="display">
					<thead>
						<tr>
							<th width="4%" id="disposableColumn"></th>
							<th colspan="1">Disposable and Supplies</th>
							<th colspan="1">Quantity</th>
							<th colspan="1">Manufacturer</th>
							<th colspan="1">Lot#</th>
							<th colspan="1">Expiration Date</th>
							<th colspan="1">Appearance OK</th>
						</tr>
						<tr>
							<th width="4%" id="disposableColumn"></th>
							<th>Disposable and Supplies</th>
							<th>Quantity</th>
							<th>Manufacturer</th>
							<th>Lot#</th>
							<th>Expiration Date</th>
							<th>Appearance OK</th>
						</tr>
					</thead>
				</table>
				<div><b>Reagents</b></div>
				<table cellpadding="0" cellspacing="0" border="0" id="reagenTable" class="display">
					<thead>
						<tr>
							<th width="4%" id="reagenColumn"></th>
							<th colspan="1">Reagents</th>
							<th colspan="1">Quantity</th>
							<th colspan="1">Manufacturer</th>
							<th colspan="1">Lot#</th>
							<th colspan="1">Expiration Date</th>
							<th colspan="1">Appearance OK</th>
						</tr>
						<tr>
							<th width="4%" id="reagenColumn"></th>
							<th>Reagents</th>
							<th>Quantity</th>
							<th>Manufacturer</th>
							<th>Lot#</th>
							<th>Expiration Date</th>
							<th>Appearance OK</th>
						</tr>
					</thead>
				</table>
				</div>
			</div>
			
</div>

<%-- <div class="floatright_right">
	<jsp:include page="sidecontrols.jsp"></jsp:include>
 </div> --%>
 

<div class="cleaner"></div>
		
<div align="right" style="float:right;">
	<form>
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
			<tr>
				<td>Next:</td>
				<td><div class="mainselection"><select name="" id="" ><option value="">Select</option><option>Preparation</option><option>Documents Review</option><option>Collection</option><option>Logout</option></select></div></td>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;&nbsp;</td>
				<td><input type="button" value="Next" onclick="saveOvernightStorage()"/></td>
			</tr>
		</table>
	</form>
</div>



<!-- <script type="text/javascript">



var now = new Date();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivalDate').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new Date()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};
$("#timeZone").html(timezones[-offset / 60]);

</script>
 -->













