<%@ include file="common/includes.jsp" %>

<!-- //?BB need to change in common methodh (Portlet createion) -->
<script type="text/javascript" src="js/stafaQC.js"></script> 
<!--//?BB 
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/placeholder.js"></script> 
 -->
 <script>
 $(document).ready(function(){
	// alert("ready" + $("#donor").val());
	 var donor=$("#donor").val();
	 url="loadDonor";
	 response = jsonDataCall(url,"jsonData={donor:"+donor+"}");
	 if(response.donorDetails != null){
		 $(response.donorDetails).each(function () {
			// alert(this[0]);
			 $("#slide_donorId").html(this[0]);
			 $("#slide_donorName").html(this[1] + this[2]);
			 $("#slide_donorDOB").html(this[3]);
			 $("#slide_donorABORH").html(this[4]);
			 $("#slide_procedure").html(this[5]);
			 $("#slide_donation").html(this[6]);
			 $("#slide_donorWeight").html(this[7]);
			 $("#slide_targetVol").html(this[8]);
			 
		 });
	 }
 });
 </script>
<div id="left_Divs" style="align:left;">
				
<!-- Patient Info start-->
			<div class="column">
				<div  class="portlet">
					<div class="portlet-header ">Donor Info</div>
						<div class="portlet-content">
							<table width="100%" border="0">
							  <tr>
								<td width="50%">ID:</td>
								<td width="50%" id="slide_donorId" name="slide_donorId"></td>
							  </tr>
							  <tr>
								<td>Name:</td>
								<td id="slide_donorName" name="slide_donorName"></td>
							  </tr>
							  <tr>
								<td>DOB:</td>
								<td id="slide_donorDOB" name="slide_donorDOB"></td>
							  </tr>
							  <tr>
								<td>ABO/Rh:</td>
								<td id="slide_donorABORH" name="slide_donorABORH"></td>
							  </tr>
							  <tr>
								<td>Procedure:</td>
								<td id="slide_procedure" name="slide_procedure"></td>
							  </tr>
							   <tr>
								<td>Donation:</td>
								<td id="slide_donation" name="slide_donation"></td>
							  </tr>
							   <tr>
								<td>Weight:</td>
								<td id="slide_donorWeight" name="slide_donorWeight"></td>
							  </tr>
							   <tr>
								<td>Target Vol:</td>
								<td id="slide_targetVol" name="slide_targetVol"></td>
							  </tr>
							</table>
						</div>
				</div>
			</div>
				
<!-- Patient Info-->

<!-- Lab Results start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header ">Donor Lab</div>
						<div class="portlet-content">
							<table width="100%" border="0">
								<tr>
									<td width="50%">IDMs</td>
									<td width="50%"></td>
								</tr>
								<tr>
									<td width="50%">West Nile</td>
									<td width="50%"> </td>
								</tr>
								<tr>
									<td width="50%">Donor CD34 </td>
									<td width="50%"> </td>
								</tr>
							</table>
						</div>
				</div>
				</div>
				
<!-- Lab Results-->
			<div class="column">
					<div class="portlet">
						<div class="portlet-header ">Product Lab</div>
							<div class="portlet-content">
							<table width="100%" border="0">
								<tr>
									<td width="50%">CD34 Day1:</td>
									<td width="50%">1.5</td>
								</tr>
								<tr>
									<td width="50%">Cum. CD34:</td>
									<td width="50%">1.5</td>
								</tr>
							</table>
							</div>
					</div>
				</div>
<!-- Recipient Info start-->
				<div class="column">
					<div class="portlet">
						<div class="portlet-header ">Recipient Info</div>
							<div class="portlet-content">
							<table width="100%" border="0">
							<tr>
								<td width="50%">ID:</td>
								<td width="50%">MDA12983483</td>
							  </tr>
							  <tr>
								<td>Name:</td>
								<td>Marry Lee</td>
							  </tr>
							  <tr>
								<td>DOB:</td>
								<td>Feb 15,1963</td>
							  </tr>
							   <tr>
								<td>ABO/Rh:</td>
								<td>O</td>
							  </tr>
							   <tr>
								<td>Diagnosis</td>
								<td></td>
							  </tr>
							   <tr>
								<td>Weight</td>
								<td></td>
							  </tr>
							  </table>
							</div>
							 
					</div>
				</div>
				
<!--Recipient Info end-->
				</div>
				
			