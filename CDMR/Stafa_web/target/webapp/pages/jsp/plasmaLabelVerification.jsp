<%@ include file="common/includes.jsp" %>

<script>
function savePopup(){
	try{
		if(labelVerificationValidation()){
		if(saveQuestions('plasmalabelverification')){
			alert("Data Saved Successfully");
			$("#plasmaLabelVerification").dialog('close');
		}
		}else{
			return false;
		}
	}catch(e){
		alert("exception " + e);
	}
	return true;
}


$(function() {
	$('input[name=plasmaisbtlblatt]').live("change",function(){
    	var isbtval=$(this).val();
    	if(isbtval==1){
    		    $('.isbtlabelshow').show();
    	}
    	if(isbtval==0){
		    $('.isbtlabelshow').hide();
	}
    });
    $('input[name=plasmawarnlblatt]').live("change",function(){
    	var isbtval=$(this).val();
    	if(isbtval==1){
    		    $('.warninglabel').show();
    	}
    	if(isbtval==0){
		    $('.warninglabel').hide();
	}
});
    loadquestions();
});

function closePlasmalabelVerification(){
	$("#plasmaLabelVerification").dialog('close');
}
</script>

<form id="plasmalabelverificationform" onsubmit="return avoidFormSubmitting()">
<%-- <s:hidden id="donor" name="donor" value="%{donor}"/>
<s:hidden id="donorProcedure" name="donorProcedure" value="%{donorProcedure}"/> --%>
	<table  id="plasmalabelverification" cellpadding="0"  cellspacing="0" class="" align="center" border="0" width="100%">
		<tr>
			<td class="isbtlabel">ISBT Label Attached/Affixed<label class="mandatory">*</label></td>
			<td><div class="checkboxrequired" id="isbtattach" style="display:none;">ISBT Label Attached is Required </div><input type="hidden" name="plasmaisbtlblatt_id" id="plasmaisbtlblatt_id" value="0"><input type="radio" id="plasmaisbtlblatt_yes" name="plasmaisbtlblatt" value="1" class="isbtclass" onclick="labelVerificationValidation();"/>Yes&nbsp;<input type="radio" name="plasmaisbtlblatt" id="plasmaisbtlblatt_no" value="0" class="isbtclass" onclick="labelVerificationValidation();"/>No<input type="hidden" name="questionsCode" id="questionsCode" value="plasmaisbtlblatt" /></td>
			
		</tr>
		<tr class="isbtlabelshow" style="display:none;">
		
			<td align=""  style="padding-left:3%;">ISBT Label Complete and Legible<label class="mandatory">*</label></td>
			<td><div class="checkboxrequired" id="isbtlabelcom" style="display:none;">ISBT Label Complete and Legible is Required </div><input type="hidden" name="plasmaisbtlblcom_id" id="plasmaisbtlblcom_id" value="0"><input type="radio" id="plasmaisbtlblcom_yes" name="plasmaisbtlblcom" value="1" class="isbtcomclass" onclick="labelVerificationValidation();"/>Yes&nbsp;<input type="radio" id="plasmaisbtlblcom_no" name="plasmaisbtlblcom" value="0" class="isbtcomclass" onclick="labelVerificationValidation();"/>No<input type="hidden" name="questionsCode" id="questionsCode" value="plasmaisbtlblcom" /></td>
			
		</tr>	
		<tr>
			<td >Warning Label Attached/Affixed<label class="mandatory">*</label></td>
			<td><div class="checkboxrequired" id="warnlblattch" style="display:none;">Warning Label Attached is Required </div><input type="hidden" name="plasmawarnlblatt_id" id="plasmawarnlblatt_id" value="0"><input type="radio" id="plasmawarnlblatt_yes" name="plasmawarnlblatt"  value="1" class="warnlblclass" onclick="labelVerificationValidation()"/>Yes&nbsp;<input type="radio" id="plasmawarnlblatt_no"  name="plasmawarnlblatt" value="0" class="warnlblclass" onclick="labelVerificationValidation()"/>No&nbsp;<input type="radio" id="plasmawarnlblatt_na" name="plasmawarnlblatt" value="-1" class="warnlblclass" onclick="labelVerificationValidation()"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="plasmawarnlblatt" /></td>
			
		</tr>
		<tr class="warninglabel" style="display:none;">
		
			<td align="" style="padding-left:3%;">Warning:Reactive Test Results for<label class="mandatory">*</label></td>
			<td><div class="checkboxrequired" id="warnreactive" style="display:none;">Warning:Reactive Text is Required </div><input type="hidden" name="plasmawarnrecative_id" id="plasmawarnrecative_id" value="0"><input type="radio" id="plasmawarnrecative_yes" name="plasmawarnrecative" value="1" class="warnrecativeclass" onclick="warnlblcheck()"/>Yes&nbsp;<input type="radio" name="plasmawarnrecative" id="plasmawarnrecative_no" value="0" class="warnrecativeclass" onclick="warnlblcheck()"/>No&nbsp;<input type="radio" id="plasmawarnrecative_na" name="plasmawarnrecative" value="-1" class="warnrecativeclass" onclick="warnlblcheck()"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="plasmawarnrecative" /></td>
			
		</tr>	
		<tr class="warninglabel" style="display:none;">
		
			<td align="" style="padding-left:3%;">Warning:Advise Patient of Communicable Disease Risk<label class="mandatory">*</label></td>
			<td><div class="checkboxrequired" id="warnpatient" style="display:none;">Warning:Advise Patient is Required </div><input type="hidden" name="plasmawarndisease_id" id="plasmawarndisease_id" value="0"><input type="radio" id="plasmawarndisease_yes" name="plasmawarndisease" value="1" class="warnpatientclass" onclick="warnlblcheck()"/>Yes&nbsp;<input type="radio" id="plasmawarndisease_no" name="plasmawarndis" value="0" class="warnpatientclass" onclick="warnlblcheck()"/>No&nbsp;<input type="radio" id="plasmawarndisease_na" name="plasmawarndisease" value="-1" class="warnpatientclass" onclick="warnlblcheck()"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="plasmawarndisease" /></td>
			
		</tr>	
		<tr class="warninglabel" style="display:none;">
		
			<td align="" style="padding-left:3%;">Not Evaluated For Infectious Substances<label class="mandatory">*</label></td>
			<td><div class="checkboxrequired" id="warnnoteval" style="display:none;">Not Evaluated for Infectious is Required </div><input type="hidden" name="plasmainfectioussubst_id" id="plasmainfectioussubst_id" value="0"><input type="radio" id="plasmainfectioussubst_yes" name="plasmainfectioussubst" value="1" class="warnnotevalclass" onclick="warnlblcheck()"/>Yes&nbsp;<input type="radio" id="plasmainfectioussubst_no" name="plasmainfectioussubst" value="0" class="warnnotevalclass" onclick="warnlblcheck()"/>No&nbsp;<input type="radio" id="plasmainfectioussubst_na" name="plasmainfectioussubst" value="-1" class="warnnotevalclass" onclick="warnlblcheck()"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="plasmainfectioussubst" /></td>
			
		</tr>	
		<tr class="warninglabel" style="display:none;">
		
			<td align=""style="padding-left:3%;">Biohazard<label class="mandatory">*</label></td>
			<td><div class="checkboxrequired" id="warnbiozard" style="display:none;">Biohazard is Required</div><input type="hidden" name="plasmabiohazard_id" id="plasmabiohazard_id" value="0"><input type="radio" id="plasmabiohazard_yes" name="plasmabiohazard" value="1" class="biohazardclass" onclick="warnlblcheck()"/>Yes&nbsp;<input type="radio" id="plasmabiohazard_no" name="plasmabiohazard" value="0" class="biohazardclass" onclick="warnlblcheck()"/>No&nbsp;<input type="radio" id="plasmabiohazard_na" name="plasmabiohazard" value="-1" class="biohazardclass" onclick="warnlblcheck()"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="plasmabiohazard" /></td>
			
		</tr>	
		<tr>
			<td>AUTO only Label Attached/Affixed<label class="mandatory">*</label></td>
			<td><input type="hidden" name="plasmaautolblatt_id" id="plasmaautolblatt_id" value="0"><input type="radio" id="plasmaautolblatt_yes" name="plasmaautolblatt" value="1"/>Yes&nbsp;<input type="radio" id="plasmaautolblatt_no" name="plasmaautolblatt" value="0" />No&nbsp;<input type="radio" id="plasmaautolblatt_na" name="plasmaautolblatt" value="-1"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="plasmaautolblatt" /></td>
			
		</tr>
	<!-- </table> -->
	<!-- <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%"> -->
		
		<tr>
			<td>Comments</td>
			<td><input type="hidden" name="plasmaquestionsComments_id" id="plasmaquestionsComments_id" value="0"><textarea id="plasmaquestionsComments" name="plasmaquestionsComments" rows="4" cols="50" > </textarea><input type="hidden" name="questionsCode" id="questionsCode" value="plasmaquestionsComments" /></td>
			
		</tr>
	</table> 
</form>
<div align="right" style="float:right;">
		<form>
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td><span><input type="password" style="width:80px;" size="5" value="" id="plasmaeSignLabelVerification" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
					<input type="button" value="Save" onclick="verifyeSign('plasmaeSignLabelVerification','popup')"/>
					<input type="button" value="Cancel" onclick="closePlasmalabelVerification()"/></span></td>
				</tr>
			</table>
		</form>
	</div>  
 
