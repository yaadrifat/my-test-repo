<%@ include file="common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script>

<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<style>
#left_table_wrapper1 {
	width:47%;
	float:left;
	padding:5px 0px 5px 10px;
}
</style>

<script language="javascript">
$(document).ready(function() {
	$( "#collectionDate" ).datepicker({dateFormat: 'M dd, yy'});
	$('.signoffdates').datetimepicker();
});

$(function(){
    $("select").uniform();
    $('.jclock').jclock();
  });

</script>

<div class="cleaner"></div>

<!--float window-->

<div id="floatMenu">
<div id="forfloat_wind">
	<div id="box-outer">
		
		<a href="#" style="text-decoration:none" id="blind">&lt;&lt; </a>
			<div id="box">
				<div id="leftnav" >
				
<!-- Patient start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header"><s:hidden id="moduleName" value=""/><s:hidden id="widgetName" value="patient"/><s:hidden id="pkNotes" value=""/><%-- <s:text name="stafa.label.patientinfo"/> --%>Patient Info</div>
						<div class="portlet-content">
						<table width="225" border="0">
						  <tr>
							<td width="25"><%-- <s:text name="stafa.label.id"/> --%> ID</td>
							<td width="135"><div id="patientId">MDA129883483</div></td>
						  </tr>
						  <tr>
							<td><%-- <s:text name="stafa.label.name"/> --%>Name</td>
							<td><div id="PatientName"></div>Mare Lee</td>
						  </tr>
						  <tr>
						  	<td><%-- <s:text name="stafa.label.dob"/> --%>DOB</td>
						  	<td><div id="patientDob"></div>Feb 15 1963</td>
						  </tr>
						  <tr>
							<td><%-- <s:text name="stafa.label.abo"/> --%>ABO/Rh</td>
							<td><div id="patientAboRh"></div>o</td>
						  </tr>
						  <tr>
							<td><%-- <s:text name="stafa.label.procedure"/> --%>Procedure</td>
							<td><div id="procedure"></div>ApheresisDay2</td>
						  </tr>
						  <tr>
							<td><%-- <s:text name="stafa.label.donation"/> --%>Donation</td>
							<td><div id="donation"></div>AUTO</td>
						  </tr>
						  <tr>
							<td><%-- <s:text name="stafa.label.bsa"/> --%>BSA</td>
							<td><div id="bsa"></div>4M^2</td>
						  </tr>
						  <tr>
							<td><%-- <s:text name="stafa.label.bv"/> --%>BV</td>
							<td><div id="bv"></div> 3l</td>
						  </tr>
						</table>

					</div>
					</div>
					</div>
				
<!-- recipient-->
<!-- Lab Results-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header"><s:hidden id="moduleName" value=""/><s:hidden id="widgetName" value="Lab"/><s:hidden id="pkNotes" value=""/><%-- <s:text name="stafa.label.lab"/> --%>Lab Results</div>
						<div class="portlet-content">
						<table width="225" border="0">
							<tr>
							<td width="89"><%-- <s:text name="stafa.label.cd34Day1"/> --%>CD34 Day1</td>
							<td width="120"><div id="cd3day1">1.5</div></td>
						  </tr>
						  <tr>
							<td><%-- <s:text name="stafa.label.cum.cd34"/> --%>CUM.CD34</td>
							<td><div id="comdate"></div>1.5</td>
						  </tr>
						</table>						
					</div>
				</div>
				</div>
				
<!-- Product-->

<!-- Recipient info-->
					<div class="column">
					<div class="portlet">
						<div class="portlet-header"><s:hidden id="moduleName" value=""/><s:hidden id="widgetName" value="recipient"/><s:hidden id="pkNotes" value=""/><%-- <s:text name="stafa.label.recipient"/> --%>Recipient Info</div>
						<div class="portlet-content">
							<%-- <table width="225" border="0">
								<tr>
									<td width="89"><s:text name="stafa.label.id"/></td>
									<td width="120"><div id="recipeintId"></div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.name"/></td>
									<td><div id="recipientName"></div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.dob"/></td>
									<td><div id="recipientdob"></div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.donoraborh"/></td>
									<td><div id="donorAboRh"></div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.recipientdiagnosis"/></td>
									<td><div id="recipientdiagnosis"></div></td>
								</tr>								
							</table> --%>
						</div>
					</div>
					</div>
				</div>
				</div>
			
			</div>
		</div>

</div>
<!--float window end-->

<!--right window start -->	
<div id="forfloat_right">
<div id="" class="floatright_left">
<div class="column">		
		<div  class="portlet">
			<div class="portlet-header">Collection Information</div><br>
				<div class="portlet-content">
				<form>		
		<div id="button_wrapper" align="center"><input class="scanboxSearch" type="text" size="15" value="w90321498772" name="" id="" onkeyup=""  placeholder="Scan/Enter Product ID" style="width:200px;"/></div>
			<div id="table_inner_wrapper">
				<div id="left_table_wrapper1">
					<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
						<tr>
							<td width="50%" height="40px"><div class="txt_align_label" align="left">Product id</div></td>
							<td width="50%" height="40px"><input type="text" name="" id="" placeholder="" value="BMT12121"/></td>
						</tr>
						<tr>
							<td width="50%" height="40px"><div class="txt_align_label" align="left">CollectionDate</div></td>
							<td width="50%" height="40px"><input type="text" name="" class="dateEntry" id="collectionDate" placeholder=""/></td>
						</tr>
						<tr>
							<td width="50%" height="40px"><div class="txt_align_label" align="left">CollectionTime</div></td>
							<td width="50%" height="40px"><span type="text" class='jclock' id="collectionTime" style="width:100px; font-size:1.0em;"></span>&nbsp;<span id="timeZone">CST</span></td>
						</tr>
						<tr>
							<td width="50%" height="40px"><div class="txt_align_label" align="left">Product Type</div></td>
							<td width="50%" height="40px"><select name="" class="" id=""><option value="1">Select</option><option value="">Auto</option></select></td>
						</tr>
						<tr>
							<td width="50%" height="40px"><div class="txt_align_label" align="left">ProductSource</div></td>
							<td width="50%" height="40px"><select name="" class="" id=""><option value="1">Select</option><option value="">HPC-A</option></select></td>
						</tr>
					</table>
				</div>
			
			<div id="middle_line"></div>
			
			<div id="right_table_wrapper">
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
					<tr>
						<td width="50%" height="40px"><div class="txt_align_label" align="left">ProductVolume</div></td>
						<td width="50%" height="40px"><input type="text" name="" id="" placeholder="" value="350"/></td>
					</tr>
					<tr>
						<td width="50%" height="40px"><div class="txt_align_label" align="left">ProductContainer</div></td>
						<td width="50%" height="40px"><select name="" class="" id=""><option value="1">Select</option><option value="">500ml Bag</option></select></td>
					</tr>
					<tr>
						<td width="50%" height="40px"><div class="txt_align_label" align="left">Product Current Status</div></td>
						<td width="50%" height="40px"><select name="" class="" id=""><option value="1">Select</option><option value="">Fresh</option></select></td>
					</tr>
					<tr>
						<td width="50%" height="40px"><div class="txt_align_label" align="left">ProductCurrentTemperature</div></td>
						<td width="50%" height="40px"><select name="" class="" id=""><option value="1">Select</option><option value="">Room Temperature</option></select></td>
					</tr>
				</table>			
				<div class="cleaner"></div>				
			</div></div>
			</form>
		</div>
			
			</div>
		</div>
		
	
<div class="cleaner"></div>

 
 <form>
 <div class="column">
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value=""/><s:hidden id="widgetName" value=""/><s:hidden id="pkNotes" value=""/>Post-Cleaning</div>
			<div class="portlet-content">
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
						<tr>
							<td width="50%" height="40px"><div class="txt_align_label" align="left">Has The Machine been Post Cleaned</div></td>
							<td width="50%" height="40px"><div align="left"><input type="checkbox" value="checked" name="" id="" /></div></td>
						</tr>
			</table>			
			
	</div>
	</div>	
</div>		
</form>	
 <div class="column">
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value=""/><s:hidden id="widgetName" value=""/><s:hidden id="" value=""/>Product Signoff</div>
			<div class="portlet-content">
				<form>		
		<div id="button_wrapper" align="center"><input class="scanboxSearch" type="text" size="15" value="" name="" id="" onkeyup=""  placeholder="Scan/Enter Product ID" style="width:200px;"/></div>
			<div id="table_inner_wrapper">
				<div id="left_table_wrapper1">
					<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
						<tr>
							<td width="50%" height="40px"><div class="txt_align_label" align="left">Apheresis Nurse</div></td>
							<td width="50%" height="40px"><input type="text" name="" value="2937462187" id="" placeholder=""/></td>
						</tr>
						<tr>
							<td width="50%" height="40px"><div class="txt_align_label" align="left">Sign-off Date And Time</div></td>
							<td width="50%" height="40px"><input type="text" name="" id="signoffdate" class="signoffdates" placeholder=""/></td>
						</tr>
						<tr>
							<td width="50%" height="40px"><div class="txt_align_label" align="left">Apheresis Physician</div></td>
							<td width="50%" height="40px"><input type="text" name="" value="8793214714" id="" placeholder=""/></td></td>
						</tr>
						<tr>
							<td width="50%" height="40px"><div class="txt_align_label" align="left">Sign-off Date And Time</div></td>
							<td width="50%" height="40px"><input type="text" name="" id="" class="signoffdates" placeholder=""/></td>
						</tr>
					</table>
				</div>
			
			<div id="middle_line"></div>
			
			<div id="right_table_wrapper">
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
					<tr>
						<td width="50%" height="40px"><div class="txt_align_label" align="left">CTL Techinician</div></td>
						<td width="50%" height="40px"><div  class="mainselection" align="left"><input type="text" name="" value="9321483126" id="" placeholder=""/></div></td>
					</tr>
					<tr>
						<td width="50%" height="40px"><div class="txt_align_label" align="left">Sign-off Date And Time</div></td>
						<td width="50%" height="40px"><input type="text" name="" id="" class="signoffdates" placeholder=""/></td>
					</tr>					
				</table>			
				<div class="cleaner"></div>				
			</div></div>
			</form>
		</div>
	</div>	
</div>		
</div>
<div class="floatright_right">
<jsp:include page="sidecontrols.jsp"></jsp:include>
 </div>
</div>
<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
				<table cellpadding="0" cellspacing="0" class=""  border="0">
				<tr>
				<td>Next:</td>
				<td>
				<select name="" id="" class=""><option>Select</option><option>Homepage</option><option>Logout</option></select>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="" value="Next"/></td>
				</tr>
				</table>
		</form>
		</div>
		
		<div class="cleaner"></div>
