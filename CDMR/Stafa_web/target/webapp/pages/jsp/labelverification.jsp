<%@ include file="common/includes.jsp" %>

<script>
function savePopup(){
	try{
		if(labelVerificationValidation()){
		if(saveQuestions('labelverification')){
			alert("Data Saved Successfully");
			$("#labelverified").dialog('close');
		}
		else{
			return false;
		}
		}
	}catch(e){
		alert("exception " + e);
	}
	return true;
}


$(function() {
	$('input[name=isbtlblatt]').live("change",function(){
    	var isbtval=$(this).val();
    	if(isbtval==1){
    		    $('.isbtlabelshow').show();
    	}
    	if(isbtval==0){
		    $('.isbtlabelshow').hide();
	}
    });
    $('input[name=warnlblatt]').live("change",function(){
    	var isbtval=$(this).val();
    	if(isbtval==1){
    		    $('.warninglabel').show();
    	}
    	if(isbtval==0){
		    $('.warninglabel').hide();
	}
    });
   /* var donorid=$("#donor").val();
   var donorProcedure=("#donorProcedure").val();  */
    loadquestions();
});

function closelabelverification(){
	//labelVerificationValidation();
	$("#labelverified").dialog('close');
}
</script>

<form id="labelverificationform" onsubmit="return avoidFormSubmitting()">
<%-- <s:hidden id="donor" name="donor" value="%{donor}"/>
<s:hidden id="donorProcedure" name="donorProcedure" value="%{donorProcedure}"/> --%>
	<table  id="labelverification" cellpadding="0"  cellspacing="0" class="" align="center" border="0" width="100%">
		<tr>
			<td class="isbtlabel">ISBT Label Attached/Affixed<label class="mandatory">*</label></td>
			<td><div class="checkboxrequired" id="isbtattach" style="display:none;">ISBT Label Attached is Required </div><input type="hidden" name="isbtlblatt_id" id="isbtlblatt_id" value="0"><input type="radio" id="isbtlblatt_yes" name="isbtlblatt" value="1" class="isbtclass" onclick="labelVerificationValidation(true)"/>Yes&nbsp;<input type="radio" name="isbtlblatt" id="isbtlblatt_no" value="0" class="isbtclass" onclick="labelVerificationValidation(true)" />No<input type="hidden" name="questionsCode" id="questionsCode" value="isbtlblatt" /></td>
			
		</tr>
		<tr class="isbtlabelshow" style="display:none;">
		
			<td align=""  style="padding-left:3%;">ISBT Label Complete and Legible<label class="mandatory">*</label></td>
			<td><div class="checkboxrequired" id="isbtlabelcom" style="display:none;">ISBT Label Complete and Legible is Required </div><input type="hidden" name="isbtlblcom_id" id="isbtlblcom_id" value="0"><input type="radio" id="isbtlblcom_yes" name="isbtlblcom" value="1" class="isbtcomclass" onclick="labelVerificationValidation(true)"/>Yes&nbsp;<input type="radio" id="isbtlblcom_no" name="isbtlblcom" value="0" class="isbtcomclass" onclick="labelVerificationValidation(true)"/>No<input type="hidden" name="questionsCode" id="questionsCode" value="isbtlblcom" /></td>
			
		</tr>	
		<tr>
			<td >Warning Label Attached/Affixed<label class="mandatory">*</label></td>
			<td><div class="checkboxrequired" id="warnlblattch" style="display:none;">Warning Label Attached is Required </div><input type="hidden" name="warnlblatt_id" id="warnlblatt_id" value="0"><input type="radio" id="warnlblatt_yes" name="warnlblatt"  value="1" class="warnlblclass" onclick="labelVerificationValidation()"/>Yes&nbsp;<input type="radio" id="warnlblatt_no"  name="warnlblatt" value="0" class="warnlblclass" onclick="labelVerificationValidation()"/>No&nbsp;<input type="radio" id="warnlblatt_na" name="warnlblatt" value="-1" class="warnlblclass" onclick="labelVerificationValidation()"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="warnlblatt" /></td>
			
		</tr>
		<tr class="warninglabel" style="display:none;">
		
			<td align="" style="padding-left:3%;">Warning:Reactive Test Results for<label class="mandatory">*</label></td>
			<td><div class="checkboxrequired" id="warnreactive" style="display:none;">Warning:Reactive Text is Required </div><input type="hidden" name="warnrecative_id" id="warnrecative_id" value="0"><input type="radio" id="warnrecative_yes" name="warnrecative" value="1" class="warnrecativeclass" onclick="warnlblcheck()"/>Yes&nbsp;<input type="radio" name="warnrecative" id="warnrecative_no" value="0" class="warnrecativeclass" onclick="warnlblcheck()"/>No&nbsp;<input type="radio" id="warnrecative_na" name="warnrecative" value="-1" class="warnrecativeclass" onclick="labelVerificationValidation()"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="warnrecative" /></td>
			
		</tr>	
		<tr class="warninglabel" style="display:none;">
		
			<td align="" style="padding-left:3%;">Warning:Advise Patient of Communicable Disease Risk<label class="mandatory">*</label></td>
			<td><div class="checkboxrequired" id="warnpatient" style="display:none;">Warning:Advise Patient is Required </div><input type="hidden" name="warndisease_id" id="warndisease_id" value="0"><input type="radio" id="warndisease_yes" name="warndisease" value="1" class="warnpatientclass" onclick="warnlblcheck()"/>Yes&nbsp;<input type="radio" id="warndisease_no" name="warndisease" value="0" class="warnpatientclass" onclick="warnlblcheck()()"/>No&nbsp;<input type="radio" id="warndisease_na" name="warndisease" value="-1" class="warnpatientclass" onclick="warnlblcheck(true)"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="warndisease" /></td>
			
		</tr>	
		<tr class="warninglabel" style="display:none;">
		
			<td align="" style="padding-left:3%;">Not Evaluated For Infectious Substances<label class="mandatory">*</label></td>
			<td><div class="checkboxrequired" id="warnnoteval" style="display:none;">Not Evaluated for Infectious is Required </div><input type="hidden" name="infectioussubst_id" id="infectioussubst_id" value="0"><input type="radio" id="infectioussubst_yes" name="infectioussubst" value="1" class="warnnotevalclass" onclick="warnlblcheck()"/>Yes&nbsp;<input type="radio" id="infectioussubst_no" name="infectioussubst" value="0" class="warnnotevalclass" onclick="warnlblcheck()"/>No&nbsp;<input type="radio" id="infectioussubst_na" name="infectioussubst" value="-1" class="warnnotevalclass" onclick="warnlblcheck()"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="infectioussubst" /></td>
			
		</tr>	
		<tr class="warninglabel" style="display:none;">
		
			<td align=""style="padding-left:3%;">Biohazard<label class="mandatory">*</label></td>
			<td><div class="checkboxrequired" id="warnbiozard" style="display:none;">Biohazard is Required</div><input type="hidden" name="biohazard_id" id="biohazard_id" value="0"><input type="radio" id="biohazard_yes" name="biohazard" value="1" class="biohazardclass" onclick="warnlblcheck()"/>Yes&nbsp;<input type="radio" id="biohazard_no" name="biohazard" value="0" class="biohazardclass" onclick="warnlblcheck(true)"/>No&nbsp;<input type="radio" id="biohazard_na" name="biohazard" value="-1" class="biohazardclass" onclick="warnlblcheck(true)"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="biohazard" /></td>
			
		</tr>	
		<tr>
			<td>AUTO only Label Attached/Affixed<label class="mandatory">*</label></td>
			<td><div class="checkboxrequired" id="autolbl" style="display:none;">Auto only label Attached is Required </div><input type="hidden" name="autolblatt_id" id="autolblatt_id" value="0"><input type="radio" id="autolblatt_yes" name="autolblatt" value="1" class="autolbl"/>Yes&nbsp;<input type="radio" id="autolblatt_no" name="autolblatt" value="0"  class="autolbl"/>No&nbsp;<input type="radio" id="autolblatt_na" name="autolblatt" value="-1" class="autolbl"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="autolblatt" /></td>
			
		</tr>
	<!-- </table> -->
	<!-- <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%"> -->
		
		<tr>
			<td>Comments</td>
			<td><input type="hidden" name="questionsComments_id" id="questionsComments_id" value="0"><textarea id="questionsComments" name="questionsComments" rows="4" cols="50" > </textarea><input type="hidden" name="questionsCode" id="questionsCode" value="questionsComments" /></td>
			
		</tr>
	</table> 
</form>
  	<div align="right" style="float:right;">
		<form>
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td><span><input type="password" style="width:80px;" size="5" value="" id="eSignLabelVerification" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
					<input type="button" value="Save" onclick="verifyeSign('eSignLabelVerification','popup')"/>
					<input type="button" value="Cancel" onclick="closelabelverification()"/></span></td>
				</tr>
			</table>
		</form>
	</div>  
