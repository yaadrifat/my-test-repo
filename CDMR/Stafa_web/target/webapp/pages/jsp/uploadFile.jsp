<%@ taglib prefix="s" uri="/struts-tags" %>
<% 
	String path = request.getContextPath()+"/pages/";
	System.out.println("path:::"+path);
%>
<html>
<head>
<style>

#pro{  width:0px;height:5px;background-color:blue;}
.input {  border: 0px solid #006;background-color:white;cursor:pointer;cursor:hand;text-decoration:underline;color:blue}
.hide_attach{  height:10px; width:10px;}

</style>

<title>FILE UPLOAD</title>     

 <script type="text/javascript" src="<%=path %>js/jquery/jquery-1.6.3.js"></script>
 <script type="text/javascript" src="<%=path %>js/jquery/jquery.min.js"></script>
<!--  html5 js  for  supporting IE8 broswer-->
<script type="text/javascript" src="<%=path %>js/jquery/modernizr.js"></script>
<script type="text/javascript" src="<%=path %>js/jquery/html5shiv.js"></script>
<script type="text/javascript" src="<%=path %>js/jquery/html5shiv-printshiv.js"></script>
<!-- html js ends -->

<script type="text/javascript" src="<%=path %>js/jquery/jquery-ui-1.8.7.custom.min.js"></script>

<!-- <link type="text/css" href="../css/ajaxfileupload.css" rel="stylesheet" />
  -->
 
<%-- <script type="text/javascript" src="../js/ajaxfileupload.js"></script> --%>
<script type="text/javascript">
 
//alert("helllo"+typeof  jQuery);
  
//  new
 
 
 
 // end of  new ajax copy 
  jQuery.extend({
     

      createUploadIframe: function(id, uri){
              //create frame
              var frameId = 'jUploadFrame' + id;
              var iframeHtml = '<iframe id="' + frameId + '" name="' + frameId + '" style="position:absolute; top:-9999px; left:-9999px"';
              if(window.ActiveXObject)
              {
                  if(typeof uri== 'boolean'){
                      iframeHtml += ' src="' + 'javascript:false' + '"';

                  }
                  else if(typeof uri== 'string'){
                      iframeHtml += ' src="' + uri + '"';

                  }   
              }
              iframeHtml += ' />';
              jQuery(iframeHtml).appendTo(document.body);
				//alert("trace 1 createuploadframe ");
              return jQuery('#' + frameId).get(0);           
      },
      createUploadForm: function(id, fileElementId, data){
          //create form   
          var formId = 'jUploadForm' + id;
          var fileId = 'jUploadFile' + id;
          var form = jQuery('<form  action="" method="POST" name="' + formId + '" id="' + formId + '" enctype="multipart/form-data"></form>');   
          if(data){
              for(var i in data){
                  jQuery('<input type="hidden" name="' + i + '" value="' + data[i] + '" />').appendTo(form);
              }           
          }       
          var oldElement = jQuery('#' + fileElementId);
          var newElement = jQuery(oldElement).clone();
          jQuery(oldElement).attr('id', fileId);
          jQuery(oldElement).before(newElement);
          jQuery(oldElement).appendTo(form);


         
          //set attributes
          jQuery(form).css('position', 'absolute');
          jQuery(form).css('top', '-1200px');
          jQuery(form).css('left', '-1200px');
          jQuery(form).appendTo('body');   
         // alert("createuploadform");
          return form;
      },

      ajaxFileUpload: function(s) {
          // TODO introduce global settings, allowing the client to modify them for all requests, not only timeout
          //alert("ajaxfileupload");
          s = jQuery.extend({}, jQuery.ajaxSettings, s);
          var id = new Date().getTime()       
          var form = jQuery.createUploadForm(id, s.fileElementId, (typeof(s.data)=='undefined'?false:s.data));
          var io = jQuery.createUploadIframe(id, s.secureuri);
          var frameId = 'jUploadFrame' + id;
          var formId = 'jUploadForm' + id;       
          // Watch for a new set of requests
          if ( s.global && ! jQuery.active++ )
          {
              jQuery.event.trigger( "ajaxStart" );
          }           
          var requestDone = false;
          // Create the request object
          var xml = {}  
          if ( s.global )
              jQuery.event.trigger("ajaxSend", [xml, s]);
          // Wait for a response to come back
          var uploadCallback = function(isTimeout)
          {           
              var io = document.getElementById(frameId);
              try
              {
               
                  //alert("io.contentWindow:::"+io.contentWindow+":::io.contentDocument::"+io.contentDocument);
                  if(io.contentWindow)
                  {
                	   //alert("ie " + io.contentWindow.document.body);
                	  //alert("inside io.contentWindow:::");
                       xml.responseText = io.contentWindow.document.body?io.contentWindow.document.body.innerHTML:null;
                       xml.responseXML= io.contentWindow.document.XMLDocument?io.contentWindow.document.XMLDocument:io.contentWindow.document;
                      // alert("xml.responseText:::"+xml.responseText);
                       //alert("xml.responseXML::"+xml.responseXML);
                       
                  }else if(io.contentDocument)
                  {
                	 // alert("other than ie");
                	 // alert("inside io.contentDocument:::");
                       xml.responseText = io.contentDocument.document.body?io.contentDocument.document.body.innerHTML:null;
                      xml.responseXML = io.contentDocument.document.XMLDocument?io.contentDocument.document.XMLDocument:io.contentDocument.document;
                     // alert("xml.responseText:::"+xml.responseText);
                     // alert("xml.responseXML::"+xml.responseXML);
                  }
             
                  //xml.responseText = $("#"+frameId).html();
              }catch(e)
              {
                  alert("error in ajaxfileupload  " + e);
                  jQuery.handleError(s, xml, null, e);
              }
           //   alert("xml responseText " + xml.responseText);
            //  alert(" xml.responseXML " +  xml.responseXML);
              if ( xml || isTimeout == "timeout")
              {               
                  requestDone = true;
                  var status;
                  try {
                      status = isTimeout != "timeout" ? "success" : "error";
                      // Make sure that the request was successful or notmodified
                      if ( status != "error" )
                      {
                          // process the data (runs the xml through httpData regardless of callback)
                         // alert("XML:" + xml + " / " + s.dataType);
                          var data = jQuery.uploadHttpData( xml, s.dataType );   
                          // If a local callback was specified, fire it and pass it the data
                          if ( s.success )
                              s.success( data, status );
     
                          // Fire the global callback
                          if( s.global )
                              jQuery.event.trigger( "ajaxSuccess", [xml, s] );
                      } else
                          jQuery.handleError(s, xml, status);
                  } catch(e)
                  {
                      //alert("error in ajaxfileupload 2" + e);
                      status = "error";
                      jQuery.handleError(s, xml, status, e);
                  }

                  // The request was completed
                  if( s.global )
                      jQuery.event.trigger( "ajaxComplete", [xml, s] );

                  // Handle the global AJAX counter
                  if ( s.global && ! --jQuery.active )
                      jQuery.event.trigger( "ajaxStop" );

                  // Process result
                  if ( s.complete )
                      s.complete(xml, status);

                  jQuery(io).unbind()

                  setTimeout(function()
                                      {    try
                                          {
                                              jQuery(io).remove();
                                              jQuery(form).remove();   
                                             
                                          } catch(e)
                                          {
                                              //alert("error in ajaxfileupload 3 " + e);
                                              jQuery.handleError(s, xml, null, e);
                                          }                                   

                                      }, 100)

                  xml = null

              }
          }
          // Timeout checker
          if ( s.timeout > 0 )
          {
              setTimeout(function(){
                  // Check to see if the request is still happening
                  if( !requestDone ) uploadCallback( "timeout" );
              }, s.timeout);
          }
          try
          {

              var form = jQuery('#' + formId);
              jQuery(form).attr('action', s.url);
              jQuery(form).attr('method', 'POST');
              jQuery(form).attr('target', frameId);
              jQuery(form).attr('accept-charset', "UTF-8");
              if(form.encoding)
              {
                  jQuery(form).attr('encoding', 'multipart/form-data');                 
              }
              else
              {   
                  jQuery(form).attr('enctype', 'multipart/form-data');           
              }           
              jQuery(form).submit();

          } catch(e)
          {       
              //alert("error in ajaxfileupload 4 " + e);
              jQuery.handleError(s, xml, null, e);
          }
         
          jQuery('#' + frameId).load(uploadCallback    );
          return {abort: function () {}};   

      },

      uploadHttpData: function( r, type ) {
          var data = !type;
          data = type == "xml" || data ? r.responseXML : r.responseText;
        /*
          var dataparsed = r.responseText.split("{"); //added by Jude
        dataparsed = dataparsed[1].split("}"); //added by Jude

         data = type == "xml" || "{ " + dataparsed[0] + " }"; //added by Jude & BB
         
    //    alert(type + " data " + data);
          // If the type is "script", eval it in global context*/
          if ( type == "script" )
              jQuery.globalEval( data );
          // Get the JavaScript object, if JSON is used.
          if ( type == "json" )
              eval( "data = " + data );
          // evaluate scripts within html
          if ( type == "html" )
              $("<div>").html(data);

          return data;
      }
  });

 
 
 // end of ajax copy 
  function ajaxFileUpload1(){
      //starting setting some animation when the ajax starts and completes
     
      //alert("trace 1 -- ajaxfileupload1");
      $.ajaxFileUpload({
              url:'uploadCSV.action',
              secureuri:false,
              fileElementId:'attach',
              dataType: 'html',
              success: function (data, status){
                  //alert("success");
                 try{
                 $("#filediv").html(data);

                 //parent.trigger("drawGraph");
                  var cur1 = $("#strCurve1").val();
                 /* alert(cur1);
                  $(parent).trigger("drawGraph",{
                      cur1:cur1,
                      cur2:"0",
                      cur3:"1"
                      });
                  */
                  var event = $.Event("drawGraph");
                  event.cur1=cur1;
                  event.cur2="0";
                  event.cur3="1";
                 // alert("before trigger");
                  $(parent).trigger("drawGraph");
                 // alert("after trigger");
                 }catch(err){
                     alert("error in success method " + err)
                 }
              },
              error: function (data, status, e){
                  //alert("error ");
                  //alert(data + " / " + status);
                  alert(e);
              }
          }
      )
    // alert("trace end")
      return false;

  } 
var id1=0;

  function sampleattach(){
	  
  }
   
  </script>
</head> 
<body>
     <div id="filediv">
         <s:form action="uploadCSV.action" id="uploadForm" namespace="/" method="POST" enctype="multipart/form-data"  accept-charset="UTF-8">
             <s:file name="fileUpload" id="attach" class="input" onchange="sampleattach();"/>
            <!--  <input id='fakeAttach' type='button' class='input' value='Attach a file' onclick="javascript:openwindow();"/>&nbsp; --><input type='button' value='Update graph' onclick='ajaxFileUpload1();'/>
             <span id="maxSize"></span>
           </s:form>
         
          
         <input type="hidden" name="strCurve1" id="strCurve1"  value='<s:property value="strCurve1"/>' />
         <input type="hidden" name="strCurve2" id="strCurve2"  value='<s:property value="strCurve2"/>' />
         <input type="hidden" name="strCurve3" id="strCurve3"  value='<s:property value="strCurve3"/>' />
         <input type="hidden" name="documentId" id="documentId"  value='<s:property value="documentId"/>' />
       
        
        
   </div>
</body>
</html>






<!-- <html>
<head>
<script type="text/javascript" src="../js/jquery/jquery-1.6.3.js"></script>
<script>
function uploadDocs(This){
    //alert("s");
    $("#fileUploadSubmit").trigger('click');
    //$("#fileUploadSubmit").submit();
}
</script>
</head>
            <body style="margin:0px;">
                <s:form action="uploadCSV.action" namespace="/" method="POST" enctype="multipart/form-data">
                    <div>
                        <s:file name="fileUpload" label="Select a File to upload" size="20" onchange="uploadDocs(this)" id="iteneraryDocument"/>
                        <div style="display:none">
                        <input type="hidden" value="123" name="fileUploadFileName" id="fileUploadFileName" />
                        <input type="hidden" value="123" name="uploadProductId" id="uploadProductId" />
                        <s:submit value="submit" id="fileUploadSubmit" name="submit" />
                        </div>
                    </div>
                </s:form>
                </body>
            </html> -->