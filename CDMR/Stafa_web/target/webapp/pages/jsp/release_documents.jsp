<%@ taglib prefix="s" uri="/struts-tags"%>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script>
$(document).ready(function() {
//getProcessingData();
//$('#table111').dataTable({"aaSorting": [[ 1, "desc" ]]});
	oTable = $('#docsReviewTable').dataTable({
		//"bJQueryUI": true
		"sPaginationType": "full_numbers"
	});
	oTable = $('#reagentsTable').dataTable({
	//	"bJQueryUI": true
		"sPaginationType": "full_numbers"
	});
	$("select").uniform();
});

function generateTableFromJsnList(response){
	$.each(response.recipientDonorList, function(i,v) {
		//alert(i+"=-="+v[0]);
		$("#recipientName").html(v[0]);
		$("#recipientId").html(v[1]);
		$("#recipientDiagnosis").html(v[2]);
		$("#recipientAboRh").html(v[3]);
		$("#donorName").html(v[4]);
		$("#donorId").html(v[5]);
		$("#donorAboRh").html(v[6]);
		$("#productId").html(v[7]);
		$("#productType").html(v[9]);
	 	});
}
function getProcessingData(){
	//alert("s");
	var url = "getOriginalProductData";
	var productSearch = $("#productSearch").val();
	//if(e.keyCode==13 && productSearch!=""){
		//alert("s : "+e.keyCode);
		var response = ajaxCall(url,"documentsForm");
		//alert(response);
		//alert(response.qcList);
		//response = jQuery.parseJSON(response.recipientDonorList);
		//alert(response);	
		//alert(response.returnJsonStr);
		generateTableFromJsnList(response);
	//}
	//response = jQuery.parseJSON(response.recipientDonorList);

}

$(document).ready(function() {
	// $('#right_new_wrapper').attr("style", "width: 83%");	
            /*  $('#blind').html("<<");
               var $box = $('#box').wrap('<div id="box-outer"></div>');
               $('#blind').click(function() {
                 $box.blindToggle('fast');
               }); */
               show_slidewidgets('rel_docwidget');
             });



    /*    jQuery.fn.blindToggle = function(speed, easing, callback) {
               var h = this.width() + parseInt(this.css('paddingLeft')) +  parseInt(this.css('paddingRight'));
               if(parseInt(this.css('marginLeft'))<0){
			   	   $('#forfloat_right').animate( { width: "79%" }, { queue: false, duration: 200 });	
                   $('#blind').html("<<");
                   return this.animate({marginLeft:0},speed, easing, callback);
                   }
               else{
    			  $( "#forfloat_right" ).animate( { width: "95%" }, { queue: false, duration: 250 });
     
                   $('#blind').html(">>");
                   return this.animate({marginLeft:-h},speed, easing, callback);
				   
                     }
				
             };
 */
</script>
<div class="cleaner"></div>

<!--float window-->

<!-- <div id="floatMenu">
<div id="forfloat_wind">
	<div id="box-outer">
		
		<a href="#" style="text-decoration:none" id="blind">&lt;&lt; </a>
			<div id="box"> -->
				<div id="rel_docwidget" >
				
<!-- recipient start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header notes"><s:hidden id="moduleName" value="releasedocument"/><s:hidden id="widgetName" value="recipient"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.recipient"/></div>
						<div class="portlet-content">
						<table width="100%" border="0">
						  <tr>
							<td width="50%"><s:text name="stafa.label.id"/></td>
							<td width="50%"><div id="recipientId">789564</div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.name"/></td>
							<td><div id="recipientName">John Wilson</div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientaborh"/></td>
							<td><div id="recipientAboRh">A Positive</div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientdiagnosis"/></td>
							<td><div id="recipientDiagnosis"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientrid"/></td>
							<td><div id="recipientRID"></div></td>
						  </tr>
						</table>
					</div>
					</div>
					</div>
				
<!-- recipient-->


<!-- Product start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header notes"><s:hidden id="moduleName" value="releasedocument"/><s:hidden id="widgetName" value="product"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.product"/></div>
						<div class="portlet-content">
						<table width="100%" border="0">
							<tr>
							<td width="50%"><s:text name="stafa.label.id"/></td>
							<td width="50%"><div id="productId">BMT123-3-1</div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.type"/></td>
							<td><div id="productType">HPC-A</div></td>
						  </tr>
						</table>					
					</div>
				</div>
				</div>
				
<!-- Product-->

<!-- Donor start-->
					<div class="column">
					<div class="portlet">
						<div class="portlet-header notes"><s:hidden id="moduleName" value="releasedocument"/><s:hidden id="widgetName" value="donor"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.donor"/></div>
							<div class="portlet-content">
							<table width="100%" border="0">
								<tr>
									<td width="50%"><s:text name="stafa.label.id"/></td>
									<td width="50%"><div id="donorId">546743</div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.name"/></td>
									<td><div id="donorName">Johnson</div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.donoraborh"/></td>
									<td><div id="donorAboRh">A Positive</div></td>
								</tr>
							</table>
						</div>
					</div>
					</div>

<!-- Donor-->
				
				</div>
<!-- 				</div>
			
			</div>
		</div>

	

	

</div> -->
<!--float window end-->

<!--right window start -->	
<!-- <div id="forfloat_right"> -->	

<section>
<form action="#" id="documentsForm">
<input type="hidden" name="productSearch" id="productSearch" value="BMT4657"/> 
 <div class="cleaner"></div>
 <div class="column">		
		<div class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value="releasedocument"/><s:hidden id="widgetName" value="releaseproduct"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.releasingproducts"/></div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="reagentsTable" class="display">
					<thead>
						<tr>
							<th colspan="1">Recipient ID</th>
							<th colspan="1">Recipient Name</th>
							<th colspan="1">Product Type</th>
							<th colspan="1">Product ID</th>				
							<th colspan="1">Vessel</th>
							<th colspan="1">Date Collected</th>
							<th colspan="1">TNC/kg(x10^8/kg)</th>
							<th colspan="1">CD34/kg(x10^6/kg)</th>
						</tr>
						
					</thead>
					<tbody>
						<tr>
							<td>MDA789564</td>
							<td>John Wilson</td>
							<td>HPC-A</td>
							<td>BMT123-3-1</td>
							<td>100 ml Bag</td>
							<td>Aug 02 2008</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>MDA789564</td>
							<td>John Wilson</td>
							<td>HPC-A</td>
							<td>BMT123-3-2</td>
							<td>100 ml Bag</td>
							<td>Aug 02 2008</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</tbody>
			</table>
	</div>
	</div>	
</div>		
</form>
</section>	


<section>

 <div class="cleaner"></div>
 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value="releasedocument"/><s:hidden id="widgetName" value="documentreviewed"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.documentreviewed"/></div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="docsReviewTable" class="display">
					<thead>
						<tr>
							<th colspan="1">Document</th>
							<th colspan="1">Issued Date</th>
							<th colspan="1">Attachment</th>
							<th colspan="1">Reviewed</th>
						</tr>
						
					</thead>
					<tbody>
						<tr>
							<td>Physician Order</td>
							<td>July 23, 2008</td>
							<td><img src="images/icons/Black_Paperclip.png" width="16" height="16" border="0"> </td>
							<td><input type="checkbox" name="" checked="checked" id=""></td>
						</tr>
						<tr>
							<td>Infectious Disease Marker Test</td>
							<td>Aug 3, 2008</td>
							<td><img src="images/icons/Black_Paperclip.png" width="16" height="16" border="0"> </td>
							<td><input type="checkbox" name="" checked="checked" id=""></td>
						</tr>
						<tr>
							<td>Donor Certification Order</td>
							<td>Aug 5, 2008</td>
							<td><img src="images/icons/Black_Paperclip.png" width="16" height="16" border="0"> </td>
							<td><input type="checkbox" name="" checked="checked" id=""></td>
						</tr>
					</tbody>
			</table>
	</div>
	</div>	
</div>		
		
</section>	


	
<div class="cleaner"></div>
	
<section>

<div class="cleaner"></div>
		<div  style="float:right;">
		<form>
			Next Process:&nbsp;&nbsp;<select name="" id="" class="sel_process"><option>Thaw in Lab</option><option>Thaw on Recipient Floor</option></select>&nbsp;
			<input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>
			<input type="button" value="Next"/>
		</form>
		</div>
		<div class="cleaner"></div>
		
</section>
<!-- </div> -->
			
<script>
$(function() {	
	
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");

	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
	//$('.sortable').columnManager({listTargetID:'sortable', onClass: 'advon', offClass: 'advoff', hideInList: [1,3],
     //   saveState: true, colsHidden: [2,4]});
	//$( ".column" ).disableSelection();
});

</script>
