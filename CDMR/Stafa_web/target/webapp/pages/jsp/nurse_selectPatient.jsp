<%@ taglib prefix="s" uri="/struts-tags"%>
<script>
$(document).ready(function(){
		var oTable=$("#assignedPatientsTable").dataTable({
		 										"bJQueryUI": true,
		 										"sDom": 'C<"clear">Rlfrtip',
		 										"aoColumns": [ { "bSortable":false},
												               null,
												               null,
												               null,
												               null,
												               null,
												               null,
												               null,
												               null,
												               null,
												               null,
												               null
												             ],
		 										"oColVis": {
		 											"aiExclude": [ 0 ],
		 											"buttonText": "&nbsp;",
		 											"bRestore": true,
		 											"sAlign": "left"
		 										},
		 										"fnInitComplete": function () {
		 									        $("#assignedPatientsColumn").html($('.ColVis:eq(0)'));
		 										}
												});
	    $("select").uniform(); 
	    oTable.thDatasorting('assignedPatientsTable');
  });
</script>
<div class="cleaner"></div>

<div class="cleaner"></div>
 
 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header addnew notes"><s:text name="stafa.label.assignedpatients"/></div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="assignedPatientsTable" class="display">
			<thead>
				   <tr>
				   		<th width="4%" id="assignedPatientsColumn"></th>
						<th colspan="1"><s:text name="stafa.label.patientid"/></th>
						<th colspan="1"><s:text name="stafa.label.patientname"/></th>
						<th colspan="1"><s:text name="stafa.label.patientdob"/></th>
						<th colspan="1"><s:text name="stafa.label.bloodtype"/></th>
						<th colspan="1"><s:text name="stafa.label.donationtype"/></th>
						<th colspan="1"><s:text name="stafa.label.plannedprocedure"/></th>
						<th colspan="1"><s:text name="stafa.label.location"/></th>
						<th colspan="1"><s:text name="stafa.label.nurse"/></th>
						<th colspan="1"><s:text name="stafa.label.status"/></th>
						<th colspan="1"><s:text name="stafa.label.transfertask"/></th>
						<th colspan="1"><s:text name="stafa.label.taskcomplete"/></th>
					</tr>
					<tr>
				   		<th width="4%" id="assignedPatientsColumn"></th>
						<th><s:text name="stafa.label.patientid"/></th>
						<th><s:text name="stafa.label.patientname"/></th>
						<th><s:text name="stafa.label.patientdob"/></th>
						<th><s:text name="stafa.label.bloodtype"/></th>
						<th><s:text name="stafa.label.donationtype"/></th>
						<th><s:text name="stafa.label.plannedprocedure"/></th>
						<th><s:text name="stafa.label.location"/></th>
						<th><s:text name="stafa.label.nurse"/></th>
						<th><s:text name="stafa.label.status"/></th>
						<th><s:text name="stafa.label.transfertask"/></th>
						<th><s:text name="stafa.label.taskcomplete"/></th>
					</tr>
			</thead>
			<tbody>
					<tr>
						<td></td>
						<td>MDA12983483</td>
						<td>Marry Lee</td>
						<td>Feb 15,1963</td>
						<td>AB</td>
						<td>AUTO</td>
						<td>Apheresis Day 2</td>
						<td>Bed #5</td>
						<td>Nurse 1</td>
						<td>Ready for Collection</td>
						<td><input type="checkbox" id="" onclick="">Yes</td>
						<td><input type="checkbox" id="" onclick="">Yes</td>
					</tr>
					<tr>
						<td></td>
						<td>MDA12983489</td>
						<td>Jennifer Lee</td>
						<td>Nov 29,1967</td>
						<td>AB</td>
						<td>AUTO</td>
						<td>Therapeutic</td>
						<td>Bed #9</td>
						<td>Nurse 1</td>
						<td>Procedure Complete</td>
						<td><input type="checkbox" id="" onclick="">Yes</td>
						<td><input type="checkbox" id="" onclick="">Yes</td>
					</tr>
			</tbody>
			</table>
	</div>
	</div>	
</div>		
<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td>Next:</td>
				<td><div><div class="mainselection"><select name="" id=""><option value="">Select</option><option>Document Review</option><option>Preparation</option></select></div></div></td>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="nextProcessPreProduct();" value="Next"/></td>
				</tr>
				</table>
				</form>
		</div>

<script>

$(function() {
	$('.jclock').jclock();
	
	$( ".column" ).sortable({
		connectWith: ".column"
			/*,
        handle: '.portlet-header',
        cursor: 'move',
        forcePlaceholderSize: true*/
			
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});

	/**For Notes**/
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

	/* $( ".portlet-header .ui-addnew" ).click(function() {
		showPopUp("open","addNewDialog","Add New Acquision","900","450")
	}); */
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");
	$( ".portlet-header .ui-reset " ).click(function() {
		var formName = document.getElementById("login");
		//clearForm(formName);
		clear();
	});

// 	$( ".portlet-header .ui-notes " ).click(function() {
// 		showNotes('open');
// 	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});


var now = new Date();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivalDate').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new Date()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};



$("#timeZone").html(timezones[-offset / 60]);

</script>
 