
<%@ include file="common/includes.jsp" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script>
$(document).ready(function(){
    var oTable;
     oTable=$("#donortype_ranking").dataTable({
         "sPaginationType": "full_numbers",
         "sDom": 'C<"clear">Rlfrtip',
         "aoColumns": [ { "bSortable":false},
                         null,
                         null
                       ],
             "oColVis": {
                 "aiExclude": [ 0 ],
                 "buttonText": "&nbsp;",
                 "bRestore": true,
                 "sAlign": "left"
             },
             "fnInitComplete": function () {
                 $("#donortype_rankingCols").html($('.ColVis:eq(0)'));
             }
        });
     
    oTable=$("#producttype_ranking").dataTable({
        "sPaginationType": "full_numbers",
        "sDom": 'C<"clear">Rlfrtip',
        "aoColumns": [ { "bSortable":false},
                        null,
                        null,
                        null
                      ],
            "oColVis": {
                "aiExclude": [ 0 ],
                "buttonText": "&nbsp;",
                "bRestore": true,
                "sAlign": "left"
            },
            "fnInitComplete": function () {
                $("#producttype_rankingCols").html($('.ColVis:eq(1)'));
            }
        });
   
   
       oTable=$("#proposedprotocols").dataTable({
            "sPaginationType": "full_numbers",
            "sDom": 'C<"clear">Rlfrtip',
            "aoColumns": [ { "bSortable":false},
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null
                          ],
                          "sScrollX": "100%",
                "oColVis": {
                    "aiExclude": [ 0 ],
                    "buttonText": "&nbsp;",
                    "bRestore": true,
                    "sAlign": "left"
                },
                "fnInitComplete": function () {
                    $("#proposedprotocolsColumn").html($('.ColVis:eq(2)'));
                }
    });
      
       oTable=$("#researchplan").dataTable({
            "sPaginationType": "full_numbers",
            "sDom": 'C<"clear">Rlfrtip',
            "aoColumns": [ { "bSortable":false},
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null
                          ],
                          "sScrollX": "100%",
                "oColVis": {
                    "aiExclude": [ 0 ],
                    "buttonText": "&nbsp;",
                    "bRestore": true,
                    "sAlign": "left"
                },
                "fnInitComplete": function () {
                    $("#researchplanColumn").html($('.ColVis:eq(3)'));
                }
    });
      
     oTable=$("#researchconsents").dataTable({
         "sPaginationType": "full_numbers",
         "sDom": 'C<"clear">Rlfrtip',
         "aoColumns": [ { "bSortable":false},
                         null,
                         null,
                         null
                       
                       ],
             "oColVis": {
                 "aiExclude": [ 0 ],
                 "buttonText": "&nbsp;",
                 "bRestore": true,
                 "sAlign": "left"
             },
             "fnInitComplete": function () {
                 $("#researchconsentCol").html($('.ColVis:eq(4)'));
             }
 });
   oTable=$("#insuranceapproval").dataTable({
        "sPaginationType": "full_numbers",
        "sDom": 'C<"clear">Rlfrtip',
        "aoColumns": [ { "bSortable":false},
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                      ],
            "oColVis": {
                "aiExclude": [ 0 ],
                "buttonText": "&nbsp;",
                "bRestore": true,
                "sAlign": "left"
            },
            "fnInitComplete": function () {
                $("#insuranceapprovalColumn").html($('.ColVis:eq(5)'));
            }
});
 
});
$(function() {
    $('.jclock').jclock();
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");	
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });
    $("select").uniform();

  });
function addproducttype(){
    
    $("#producttype_ranking tbody").prepend("<tr>"+
              "<td><input type='checkbox' id=''/></td>"+
              "<td><input type='text' id=''/></td>"+
              "<td><select><option>select</option><option>Prep</option></select></td>"+  
              "<td><input type='text' id=''/></td>"+
              "</tr>");
      $("select").uniform();
      }
function adddonortype(){
    
    $("#donortype_ranking tbody").prepend("<tr>"+
              "<td><input type='checkbox' id=''/></td>"+
              "<td><input type='text' id=''/></td>"+
              "<td><select><option>select</option><option>Prep</option></select></td>"+                    
              "</tr>");
      $("select").uniform();
      }
      
function addResearchConsents(){
    
    $("#researchconsents tbody").prepend("<tr>"+
              "<td><input type='checkbox' id=''/></td>"+
              "<td><input type='text' id=''/></td>"+
              "<td><input type='text' id='' class='dateEntry'/></td>"+
              "<td><img src='images/icons/Black_Paperclip.jpg'></td>"+            
              "</tr>");
      $("select").uniform();

      }
 
    function addproposedprotocols(){
     
      $("#proposedprotocols tbody").prepend("<tr>"+
                "<td><input type='checkbox' id=''/></td>"+
                "<td><select><option>select</option><option>Prep</option></select></td>"+
                "<td><input type='text' id=''/></td>"+
                "<td><input type='text' id=''/></td>"+
                "<td><input type='text' id=''/></td>"+
                "<td><select><option>select</option><option>Yes</option><option>No</option></select></td>"+
                "<td><select><option>select</option><option>Pets</option><option>Adults</option></select></td>"+
                "<td><input type='text' id=''/></td>"+
                "<td><select><option>select</option><option>Pets</option><option>Adults</option></select>"+
                "</tr>");
        $("select").uniform();

        }
    function addresearchplan(){
       
        $("#researchplan tbody").prepend("<tr>"+
                  "<td><input type='checkbox' id=''/></td>"+
                  "<td><select><option>select</option><option>Prep</option></select></td>"+
                  "<td><input type='text' id=''/></td>"+
                  "<td><input type='text' id=''/></td>"+
                  "<td><input type='text' id=''/></td>"+
                  "<td><select><option>select</option><option>Yes</option><option>No</option></select></td>"+
                  "<td><select><option>select</option><option>Pets</option><option>Adults</option></select></td>"+
                  "<td><input type='text' id=''/></td>"+
                  "<td><select><option>select</option><option>Pets</option><option>Adults</option></select>"+
                  "</tr>");
          $("select").uniform();

          }
    function addInsuranceApprovals(){
  	  $("#insuranceapproval tbody").prepend("<tr>"+
                "<td><input type='checkbox' id=''/></td>"+
                "<td><select><option>select</option><option>Prep</option></select></td>"+
                "<td><select><option>select</option><option>Yes</option><option>No</option></select></td>"+
                "<td><input type='text' id=''/></td>"+
                "<td><input type='text' id=''/></td>"+
                "<td><input type='text' id=''class='dateEntry'/></td>"+
                "<td><input type='text' id=''/></td>"+
                "<td><input type='text' id='' class='dateEntry'/></td>"+
                "<td><input type='text' id='' class='dateEntry'/></td>"+
                "<td><a href='#'>View</a></td>"+

                "</tr>");
        $("select").uniform();

  	
  }
  </script>

<div class="column">
<form>
    <div  class="portlet">
    <div class="portlet-header">Diagonsis</div>
        <div class="portlet-content">
             <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Current Diagonsis</td>
                    <td class="tablewapper1"><select id='currentdiagonsis'><option>Select</option></select></td>
                    <td class="tablewapper">Diagonsis Date</td>
                    <td class="tablewapper1"><input type="text" id="diagonsisdate"  name="" class="dateEntry"/></td>
                </tr>
                <tr>
                    <td class="tablewapper"></td>
                    <td class="tablewapper1"><select id=''><option>Select</option></select></td>
                    <td class="tablewapper"></td>
                    <td class="tablewapper1"></td>
                </tr>
                <tr>
                    <td class="tablewapper">Target Disease Status</td>
                    <td class="tablewapper1"><select id="targetdiseasestatus"><option>Select</option></select></td>
                    <td class="tablewapper"></td>
                    <td class="tablewapper1"></td>
                </tr>
               
            </table>
        </div>
    </div>
    </form>
</div>

  <div class="cleaner"></div>


        <div class="column">      
            <div  class="portlet">
                <div class="portlet-header ">Donor Matching Plan</div>
                        <div class="portlet-content">
                        <table width="98%">
                            <tr >
                            <td width="45%" >
                            <div style="border:solid 1px #000;border-radius:4px;">
                               <br>
                                  <div id=""><b>Donor Type Ranking</b></div>
                               <br>  
                                <table>
                                    <tr>
                                        <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="adddonortype();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                                        <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                                      
                                    </tr>
                                </table>
                                <table cellpadding="0" cellspacing="0" border="0" id="donortype_ranking" class="display">
                                        <thead>
                                            <tr>
                                                <th width="4%" id="donortype_rankingCols"></th>
                                                <th colspan='1'>Rank</th>
                                                <th colspan='1'>Donor Type</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><input type='checkbox'  id="" onclick=""></td>
                                                <td>1</td>
                                                <td><select><option>Select</option><option>Auto</option><option>Allo-Unrelated</option><option>Allo-Related</option></select></td>
                                            </tr>
                                            <tr>
                                                <td><input type='checkbox'  id="" onclick=""></td>
                                                <td>2</td>
                                                <td><select><option>Select</option><option>Auto</option><option>Allo-Unrelated</option><option>Allo-Related</option></select></td>
                                            </tr>
                                             <tr>
                                                <td><input type='checkbox'  id="" onclick=""></td>
                                                <td>3</td>
                                                <td><select><option>Select</option><option>Auto</option><option>Allo-Unrelated</option><option>Allo-Related</option></select></td>
                                            </tr>
                                        </tbody>
                                </table>
                                </div>
                        </td>
                        <td width="45%" >
                        <div style="border:solid 1px #000;border-radius:4px; ">
                             <br>
                                <div id=""><b>Product Type Ranking</b></div>
                                <br>
                                <table>
                                    <tr>
                                        <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                                        <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                                      
                                    </tr>
                                </table>
                                <table cellpadding="0" cellspacing="0" border="0" id="producttype_ranking" class="display">
                                        <thead>
                                            <tr>
                                                <th width="4%" id="producttype_rankingCols"></th>
                                                <th colspan='1'>Rank</th>
                                                <th colspan='1'>Product Type</th>
                                                <th colspan='1'>If HPC-C,# of Cords</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><input type='checkbox'  id="" onclick=""></td>
                                                <td>1</td>
                                                <td><select><option>Select</option><option>HPC-A</option><option>HPC-M</option><option>HPC-C Single</option><option>HPC-C Multiple</option></select></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td><input type='checkbox'  id="" onclick=""></td>
                                                <td>2</td>
                                                 <td><select><option>Select</option><option>HPC-A</option><option>HPC-M</option><option>HPC-C Single</option><option>HPC-C Multiple</option></select></td>
                                                <td>1</td>
                                            </tr>
                                             <tr>
                                                <td><input type='checkbox'  id="" onclick=""></td>
                                                <td>3</td>
                                                  <td><select><option>Select</option><option>HPC-A</option><option>HPC-M</option><option>HPC-C Single</option><option>HPC-C Multiple</option></select></td>
                                                <td>2</td>
                                            </tr>
                                        </tbody>
                                </table>
                       </div>
                            </td>
                            </tr>
                        </table>

            </div>
        </div>
</div>

<div class="column">
<form>
    <div  class="portlet">
    <div class="portlet-header">Treatment Plan</div>
    <br>
        <div id="button_wrapper"><input type="radio" name="patients" id="">&nbsp;&nbsp;Inpatient&nbsp;&nbsp;&nbsp;<input type="radio" name="patients" id="">&nbsp;&nbsp;Outpatient</div><br>
        <div class="portlet-content">
             <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Auto-Collection Date</td>
                    <td class="tablewapper1"><input type="text" id=""  name="" class="dateEntry"/></td>
                    <td class="tablewapper">Status</td>
                    <td class="tablewapper1"><select id=""><option>Select</option></select></td>
                </tr>
               <tr>
                    <td class="tablewapper">Prep Starts Date</td>
                    <td class="tablewapper1"><input type="text" id=""  name="" class="dateEntry"/></td>
                    <td class="tablewapper">Status</td>
                    <td class="tablewapper1"><select id=""><option>Select</option></select></td>
                </tr>
               <tr>
                    <td class="tablewapper">Admission Date</td>
                    <td class="tablewapper1"><input type="text" id=""  name="" class="dateEntry"/></td>
                    <td class="tablewapper">Status</td>
                    <td class="tablewapper1"><select id=""><option>Select</option></select></td>
                </tr>
                 <tr>
                    <td class="tablewapper">Transplant Date</td>
                    <td class="tablewapper1"><input type="text" id=""  name="" class="dateEntry"/></td>
                    <td class="tablewapper">Status</td>
                    <td class="tablewapper1"><select id=""><option>Select</option></select></td>
                </tr>
                 <tr>
                    <td class="tablewapper">Number of prep days</td>
                    <td class="tablewapper1"><input type="text" id=""  name="" class=""/></td>
                    <td class="tablewapper"></td>
                    <td class="tablewapper1"></td>
                </tr>
                 <tr>
                    <td class="tablewapper">TBI</td>
                    <td class="tablewapper1"><input type="radio"/>Yes&nbsp;<input type="radio"/>No</td>
                    <td class="tablewapper"></td>
                    <td class="tablewapper1"></td>
                </tr>
               
            </table>
        </div>
    </div>
    </form>
</div>


<div class="cleaner"></div>
<div class="column">  
    <div  class="portlet">
            <div class="portlet-header ">Proposed Protocols</div>
            <div class="portlet-content">
            <table>
                <tr>
                    <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addproposedprotocols();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                    <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                   
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" id="proposedprotocols" class="display">
            <thead>
                <tr>
                    <th width="4%" id="proposedprotocolsColumn"></th>
                    <th >Protocol Type</th>
                    <th >Protocol Name</th>
                    <th >Protocol Source</th>
                    <th >Protocol id</th>
                    <th >Adults or Peds?</th>
                    <th >Proposed Start Date</th>
                    <th >Attachment</th>
                    <th >Status</th>
                   
                    
                   
                </tr>
            </thead>
            <tbody>
               
            </tbody>
            </table>
        </div>
    </div>
</div>

 <div class="cleaner"></div>


        <div class="column">      
            <div  class="portlet">
                <div class="portlet-header ">Research Plan</div>
                        <div class="portlet-content">
                        <table>
                            <tr>
                                <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addresearchplan();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                                <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                              
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" id="researchplan" class="display">
            <thead>
                <tr>
                    <th width="4%" id="researchplanColumn"></th>
                    <th >Protocol Type</th>
                    <th >Protocol Name</th>
                    <th >Protocol Source</th>
                    <th >Protocol id</th>
                    <th >Adults or Peds?</th>
                    <th >Proposed Start Date</th>
                    <th >Attachment</th>
                    <th >Status</th>
                </tr>
            </thead>
            <tbody>
               
            </tbody>
            </table>
            </div>
        </div>
</div>

  <div class="cleaner"></div>


        <div class="column">      
            <div  class="portlet">
                <div class="portlet-header ">Research Consents</div>
                        <div class="portlet-content"><br>
                        <div id="">Recipient Consents For Research</div><br>
                        <table>
                            <tr>
                                <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                                <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                              
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" id="researchconsents" class="display">
                        <thead>
                            <tr>
                                <th width="4%" id="researchconsentCol"></th>
                                <th colspan='1'>Consent</th>
                                <th colspan='1'>Date</th>
                                <th colspan='1'>Attachment</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type='checkbox'  id="" onclick=""></td>
                                <td>Censent of Research</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                        </table>
            </div>
        </div>
</div>

 <div class="cleaner"></div>


        <div class="column">   
        <form>
            <div  class="portlet">
                <div class="portlet-header ">Research - Insurance Approvals</div><br>
                        <div class="portlet-content">
                        <div id="">Insurance Approval for Research</div><br>
                        <table>
                            <tr>
                                <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                                <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                              
                            </tr>
                            </table>
                     <table cellpadding="0" cellspacing="0" border="0" id="insuranceapproval" class="display">
                            <thead>
                                <tr>
                                    <th width="4%" id="insuranceapprovalColumn"></th>
                                    <th >Approval For</th>
                                    <th >Protocol Name</th>
                                    <th >Name</th>
                                    <th >I-Plan Code</th>
                                    <th >Auth Date</th>
                                    <th >Auth Code</th>
                                    <th >Eff Date</th>
                                    <th >End Date</th>
                                    <th >Auth Attachment</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                   <td><input type='checkbox'  id="" onclick=""></td>
                                   <td><select id=""><option>Select</option></select></td>
                                   <td><select id=""><option>Select</option></select></td>
                                   <td></td>
                                   <td><input type="text" id=""  name="" class=""/></td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td></td>
                                   <td><a href="#" >View</a></td>     
                                   </tr>
                            </tbody>
                    </table>
            </div>
        </div>
        </form>
</div>

 <div class="cleaner"></div>

 <div class="column">
<form>
    <div  class="portlet">
    <div class="portlet-header">Physician Sign-Off</div>
        <div class="portlet-content">
        
             <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="90%">
                <tr>
                    <td class="">Name</td>
                    <td class=""><input type="text" id=""  name="" class=""/></td>
                    <td class="">Date</td>
                    <td class=""><input type="text" id=""  name="" class="dateEntry"/></td>
                    <td class="">e-sign</td>
                    <td class=""><input type="text" id=""  name="" class=""/></td>
                </tr>
                               
            </table>
        </div>
    </div>
    </form>
</div>

