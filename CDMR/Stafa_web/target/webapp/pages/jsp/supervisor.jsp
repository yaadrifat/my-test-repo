<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/acquisition.js"></script>
<script type="text/javascript">


function hideBarcodes(){
	$("#imgtip0").css("display","none");
	$("#imgtip1").css("display","none");
	$("#imgtip2").css("display","none");
	$("#imgtip3").css("display","none");
}
$(function(){
    $("#acquisitionNewForm").validate({
        rules:{
            "recipientId":"required",
             "recipientName":"required", 
            "donorId":"required",
             "donorName":"required", 
            "productId":"required"
            /* "collectSection":"required",
            "ProductSource":"required",
            "ProductType":"required",
            "processingFacility":"required",
            "donorLocation":"required",
            "ProposedFinalDisposition":"required",
            "productAppearance":"required" */
            },
            messages:{
                "recipientId":" Please Enter Recipient Id",
                 "recipientName":"Please Enter Recipient Name", 
                "donorId":"Please Enter Donor Id",
                 "donorName":"Please Enter Donor Name", 
                "productId":"Please Enter Product Id"
                /* "collectSection":"Please Enter Collection Location",
                "ProductSource":"Please select Product Source",
                "ProductType":"Please select Product Type",
                "processingFacility":"Please Enter Processing Facility",
                "donorLocation":"Please select Donor Location",
                "ProposedFinalDisposition":"Please select Proposed Final Disposition",
                "productAppearance":"Please select product appearance" */
            }
        });
    });
    
    



function addNewProduct(){
if($("#acquisitionNewForm").valid()){$.uniform.restore('select');
	setComboValue($("#fkCodelstProductsource").val(),"fkCodelstProductsource1");
	setComboValue($("#fkCodelstProductType").val(),"fkCodelstProductType1");
	
	var url = "addAcquisition";
	var response = ajaxCall(url,"acquisitionNewForm");
	response = jQuery.parseJSON(response.returnJsonStr);
 
	var tableObj = $("#acquisitionTable");
	var rowCount = $('#acquisitionTable tbody tr').length;
 	
//Based on workflow response we have to move the correspondig process Ex:Acquisition, Accesion, Production...etc
	var workflow = response.workflow;
	var productId = $("#productId").val();

 if(workflow!=null && workflow!="" && workflow=="success"){

 		if($("#fkCodelstProductFrom").val()=="1"){
		 	status = "Acquired";
		 	}
	 	else{
	 		status = "Active";
		 	}
        constructTable(true);

		alert("Data saved Successfully");
		showPopUp("close","SupervisoraddNewDialog","Add New Product","900","450");
	}	 
 	else{
 	 	//alert("productId : "+productId);
 	 	alert("Data saved Successfully");
 	 	 constructTable(true);
 		var url = response.workflow+".action?productId="+productId;
 		showPopUp("close","SupervisoraddNewDialog","","","");
 		loadAction(url);
 		}
	}
}
function closeNewAcquisition()
{	$("#SupervisoraddNewDialog").dialog( "destroy" )
	
	}
function setComboValue(val1,comboBoxId){
	//val = val1.replace(/^\s+/,"");		
	val = val1.replace(/^\s+|\s+$/g,"");		//trim left and right spaces
	//alert(val);
	var x = document.getElementById(comboBoxId);
	$("#"+comboBoxId).find("option:contains('"+val+"')").each(function(){
		//alert("["+$(this).text()+"]==["+val+"]");
	if( $(this).text() == val ) {
		$(this).attr("selected","selected");
		//This line added due to new dropdown changes ..Mari..
		$("#uniform-"+comboBoxId+" span").html(val1);
		}
	});
}

var resetFlag=false;

function checkProduct(productSearch){
	var flag=false;
	var productArr = [];
	
		$('#acquisitionTable tbody tr').each(function(i,el) {
		$(this).removeAttr("style");
	    var productId = this.cells[11].innerHTML;
	    var status = this.cells[13].innerHTML;
	    if(productId==productSearch && status=="Acquired"){
		    flag = true;
		    //resetFlag = true;
		   
	    }
	    else{
	    	productArr.push(productId);
	    }
	  });
		if($.inArray(productSearch, productArr)<0){
			 resetFlag = true;
		}
	
		 return flag;
}

var workFlowFlag = false;
function getAcquisitionData(e,This){
	var url = "getAcquisitionInfo.action?aquLocation=acqlocation&productType=producttype&finalDisp=finaldispos&donorLoc=location&procSelec=procfacility&productSource=prodsource";
	var productSearch = $("#productSearch").val();
	if((e.keyCode==13 && productSearch!="") || workFlowFlag ==true){

		var productAvailable = jQuery("#acquisitionTable:contains(\'"+productSearch+"\')").length
		var flag =checkProduct(productSearch);
		
		if(flag){
			alert("Product is already Acquired.");
			//return;
			}
			if(resetFlag){
				///** Reset the form values*/
				   $("#fkCodelstPersonlocation").val("");      
				   $("#fkCodelstFinalDisposition").val(""); 
				   $("#fkCodelstProcessSelection").val(""); 
				   $("#fkCodelstProductsource").val(""); 
				   $("#fkCodelstProductType").val(""); 
				   $("#fkCodelstLocation").val(""); 
				//return;
			}
		
				var availableFlag=0;
				$('#acquisitionTable tbody tr').each(function(i,el) {
					$(this).removeAttr("style");
				    var productId = this.cells[11].innerHTML;
				    var appearance = this.cells[10].innerHTML;
				    var status = this.cells[13].innerHTML;
				    //alert("productId : "+productId+", productSearch : "+productSearch+",status : "+status);
				    if(productId==productSearch && status=="Active"){
				    	//alert("b");
				    		availableFlag = true;
				    		$(this).attr("style","background:#33ffff");
							//$("#tempPersonProductId").val($(this).attr("id"));
				    		$("#recipientId").val(this.cells[1].innerHTML);
				    		$("#recipientName").val(this.cells[2].innerHTML);
				    		$("#donorName").val(this.cells[6].innerHTML);
				    		$("#donorId").val(this.cells[5].innerHTML);
				    		$("#status").val(status);

				    		if(appearance=="Yes"){
								//$('input[name=appearance]')[0].checked="checked";
							}else{
								//$('input[name=appearance]')[1].checked="checked";
								}
				    		setComboValue(this.cells[9].innerHTML,"fkCodelstFinalDisposition");
				    		setComboValue(this.cells[8].innerHTML,"fkCodelstProcessSelection");
				    		setComboValue(this.cells[7].innerHTML,"fkCodelstPersonlocation");
				    		setComboValue(this.cells[4].innerHTML,"fkCodelstProductsource");
				    		setComboValue(this.cells[3].innerHTML,"fkCodelstProductType");
				    		setComboValue(this.cells[12].innerHTML,"fkCodelstLocation");
					    }else if(productId==productSearch && status=="Active"){
					    	availableFlag = false;		
						 }else{
						 }
				});
		}
}
function checkAppearance(radioObj){
	
	if(radioObj.id=="radioYes"){
		$("#fkCodelstProdappear").val("1");	
		}else{
			$("#fkCodelstProdappear").val("0");
			}
}

function setAppearance(radioObj){
	//alert(radioObj.id);
	if(radioObj.id=="Yes"){
		//alert(radioObj.id);
		$("#productAppearance").val("Yes");
		alert(radioObj.id);	
		}else{
			$("#productAppearance").val("No");
			}
} 

$(document).ready(function() {
	hideBarcodes();
	hide_slidewidget();
	//getAcquisitionTable(); 
	constructTable(false);
	//columnMaager();
	//setColumnManager('acquisitionTable');
	//constructTable(false);
	$( "#Collectiondate" ).datepicker({dateFormat: 'M dd, yy'});
	oTable = $('#taskTable').dataTable({
	});
	
	$("select").uniform();	
	$('.tbody tr').click(function(){
		  changeClass(this);
		});
	$( "#acquisitionDateStr" ).datepicker({dateFormat: 'M dd, yy'});
	
	/*var url = "getNewProduct";
	var response = ajaxCall(url,"acquisitionNewForm");
	constructTable(true);*/
	
	});


function clearPopUp()
{	
		$('#SupervisoraddNewDialog #recipientId').val("");
		$('#SupervisoraddNewDialog #recipientName').val("");
		$('#DonorId').val("");
		$('#DonorName').val("")
	    $("#fkCodelstPersonlocation").val("");      
	   	$("#fkCodelstFinalDisposition").val(""); 
	   	$("#fkCodelstProcessSelection").val(""); 
	   	$("#fkCodelstProductsource").val(""); 
	   	$("#fkCodelstProductType").val(""); 
	   	$('#fkCodelstProductFrom').val("");
	   	$('#CollectSection').val("");
	   	$('#productId').val("");
	   	$('#ProcessingFacility').val("");
	   	$("#Yes").removeAttr('checked');
		$("#NO").removeAttr('checked');
	   }

</script>
<div class="cleaner"></div>

<section>

<div class="cleaner"></div>

 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header addnew"><s:hidden id="moduleName" value="taskManagement"/><s:hidden id="widgetName" value="productDetails"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.products"/></div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="acquisitionTable" class="display" >
				<thead>
				   <tr>
				   		<th width="4%" id="acquisitionColumn"></th>
						<th colspan="1" class="header"><s:text name="stafa.label.recipientid"/></th>
						<th colspan="1" class="header"><s:text name="stafa.label.recipientname"/></th>
						<th colspan="1"><s:text name="stafa.label.productsource"/></th>
						<th colspan="1"><s:text name="stafa.label.producttype"/></th>
						<th colspan="1"><s:text name="stafa.label.donorid"/></th>
						<th colspan="1"><s:text name="stafa.label.donorname"/></th>
						<th colspan="1"><s:text name="stafa.label.donorlocation"/></th>
						<th colspan="1"><s:text name="stafa.label.processingfacility"/></th>
						<th colspan="1"><s:text name="stafa.label.proposedfinaldisposition"/></th>
						<th colspan="1"><s:text name="stafa.label.productappearance"/></th>
						<th colspan="1"><s:text name="stafa.label.productid"/></th>
					    <th colspan="1"><s:text name="stafa.label.physicalname" /></th>
                        <th colspan="1"><s:text name="stafa.label.collectsection"/></th>
                        <th colspan="1"><s:text name="stafa.label.collectiondate" /></th>
                        <th colspan="1"><s:text name="stafa.label.status"/></th>
					</tr> 
				</thead>
			</table>
	</div>
	</div>	
</div>

 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header "><s:hidden id="moduleName" value="taskManagement"/><s:hidden id="widgetName" value="taskDetails"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.task"/></div>
			<div class="portlet-content">
			
			<table border="0" align="center" cellpadding="0" cellspacing="0" class="display" id="taskTable">
			<thead>	
			<tr>
			<th colspan="1"><s:text name="stafa.label.taskname"/></th>
			<th colspan="1"><s:text name="stafa.label.userroletype"/></th>
			<th colspan="1"><s:text name="stafa.label.completeflag"/></th>
			<th colspan="1"><s:text name="stafa.label.assignto"/></th>
			<th colspan="1"><s:text name="stafa.label.status"/></th>
			</tr>
			</thead>
			<tbody>
				<s:iterator value="taskList" var="rowVal" status="row">
				<tr>
						<td><s:property value="#rowVal[1]"/></td>
						<td><s:property value="#rowVal[2]"/></td>
						<td><s:property value="#rowVal[3]"/></td>
						<td><a href="#">Assign</a></td> 
						<td></td>
				</tr>
				</s:iterator>
			</tbody>
			</table>
			
	</div>
	</div>	
</div>		
		
</section>

<!-- section ends here-->
<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
				<table cellpadding="0" cellspacing="0" class="" align="" border="0" width="100%">
				<tr>
				<td>Next:</td>
				<td><div class="mainselection" >
				<select name="" id="" class=""><option>Select</option><option>Daily Assignments</option><option>Recurring Tasks</option></select></div></td>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="" value="Next"/></td>
				</tr>
				</table>
		</form>
		</div>

<div class="cleaner"></div>
	
<form action="#" id="acquisitionForm">
<s:hidden name="pkAcquisition" id="pkAcquisition"/>
<s:hidden name="recipientName" id="recipientName"/>
<s:hidden name="recipientId" id="recipientId"/>
<s:hidden name="donorName" id="donorName"/>
<s:hidden name="donorId" id="donorId"/>
<s:hidden name="" id="status"/>
<s:hidden name="returnJsonStr" id="returnJsonStr"/>
<s:hidden name="personproductid" id="tempPersonProductId"/>

</form>
	<!-- <div id="addNewDialog" title="addNew" style="display: none">
    <s:hidden name="pagesFrom" value="acquistion"/>
        <form id="acquisitionNewForm">
            <table border="0" >
                <tr>
                    <td><s:text name="stafa.label.recipientid" /></td>
                    <td><input type="text" id="recipientId" name="recipientId"/></td>
                    <td><s:text name="stafa.label.recipientname" /></td>
                    <td><input type="text" id="recipientName" name="recipientName"/></td>
                </tr>
                <tr>
                    <td><s:text name="stafa.label.donorid" /></td>
                    <td><input type="text" id="DonorId" name="donorId"/></td>
                    <td><s:text name="stafa.label.donorname" /></td>
                    <td><input type="text" id="DonorName" name="donorName"/></td>
                </tr>
   
                <tr>
                    <td><s:text name="stafa.label.productsource" /></td>
                    <td>
                        <s:select  headerKey="" headerValue="Select" list="productSourceList" listKey="description" listValue="description" name="ProductType" id="fkCodelstProductsource"/>
                        <s:select style="display:none" headerKey="" headerValue="Select" list="productSourceList" listKey="pkCodelst" listValue="description" name="ProductTypeTemp" id="fkCodelstProductsourceTemp"/>
                    </td>
                    <td><s:text name="stafa.label.producttype" /></td>
                    <td>
                        <s:select  headerKey="" headerValue="Select"  headerKey="" headerValue="Select"  list="productTypeList" listKey="description" listValue="description" name="ProductSource" id="fkCodelstProductType"/>
                        <s:select style="display:none" headerKey="" headerValue="Select" headerKey="" headerValue="Select" list="productTypeList" listKey="pkCodelst" listValue="description" name="ProductSourceTemp" id="fkCodelstProductTypeTemp"/>
                    </td>
                </tr>
   
                <tr>
                    <td><s:text name="stafa.label.donorlocation" /></td>
                    <td>
                        <s:select  headerKey="" headerValue="Select" list="donorLocList" listKey="description" listValue="description" name="donorLocation" id="fkCodelstPersonlocation"/>
                    </td>
                    <td><s:text name="stafa.label.processingfacility" /></td>
                    <td><input type="text" id="ProcessingFacility" name="processingFacility"/></td>
                </tr>
                <tr>
                    <td><s:text name="stafa.label.proposedfinaldisposition" /></td>
                    <td>
                        <s:select  headerKey="" headerValue="Select" list="finalDispList" listKey="description" listValue="description" name="ProposedFinalDisposition" id="fkCodelstFinalDisposition"/>
                    </td>
                    <td><s:text name="stafa.label.productappearance" /></td>
                    <td><input name="productAppearance" type="Radio" id="Yes"/> Y <input type="Radio" name="productAppearance"  id="NO"/> N <label id="error" class="error" for="productAppearance" generated="true"></label></td>
                   
                </tr>
                <tr>
                    <td><s:text name="stafa.label.productid" /></td>
                    <td><input type="text" id="productId" name="productId"/></td>
                    <td><s:text name="stafa.label.collectsection" /></td>
                    <td><input type="text" id="CollectSection" name="collectSection"/></td>
                </tr>
                <tr>
                    <td><s:text name="stafa.label.productFrom" /></td>
                    <td><select name="fkCodelstProductFrom" id="fkCodelstProductFrom">
                    <option value="0">Internal</option>
                    <option value="1">External</option>
                        </select></td>
                    <td><s:hidden name="workflow3" value="getAccession"></s:hidden>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr><td align="right" colspan="4"><input type="button" name="Cancel" value="Cancel" onclick="closeNewAcquisition();"/>&nbsp;<input type="button" name="Save" onclick="addNewProduct();" value="Save"/></td></tr>
            </table>
            </form>
            </div>-->
	<div id="SupervisoraddNewDialog" title="addNew" style="display: none">
	<s:hidden name="pagesFrom" value="acquistion"/>
		<%-- <jsp:include page="addnewproduct.jsp"></jsp:include> --%>
  </div>
	
	<div class="cleaner"></div>
	
<script>

$(function() {
	
	
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open');
	});

	/**For Notes**/
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='ui-addnew'></span>");
	
	$( ".portlet-header .ui-addnew" ).click(function() {
		addNewPopup("Add New Product","SupervisoraddNewDialog","addnewAcquisition","700","1100");
	});
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});


var now = new Date();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivalDate').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new Date()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};
$("#timeZone").html(timezones[-offset / 60]);

</script>
