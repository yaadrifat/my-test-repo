<%@ include file="common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script> 
<script type="text/javascript" src="js/placeholder.js"></script>

<div id="leftnav" style="align:left;">
				
<!-- recipient start-->
				<div class="column" id="recipientdiv">
				<div  class="portlet">
					<div class="portlet-header notes"><s:hidden id="moduleName" value="processing"/><s:hidden id="widgetName" value="recipient"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.recipient"/></div>
						<div class="portlet-content">
						<table width="225" border="0">
						  <tr>
							<td width="89"><s:text name="stafa.label.id"/></td>
							<td width="120"><div id="recipientId"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.name"/></td>
							<td><div id="recipientName"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientaborh"/></td>
							<td><div id="recipientAboRh"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientdiagnosis"/></td>
							<td><div id="recipientDiagnosis"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientrid"/></td>
							<td><div id="recipientRID"></div></td>
						  </tr>
						</table>

					</div>
					</div>
					</div>
				
<!-- recipient-->


<!-- Product start-->
				<div class="column" id="productdiv">
				<div  class="portlet">
					<div class="portlet-header notes"><s:hidden id="moduleName" value="processing"/><s:hidden id="widgetName" value="product"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.product"/></div>
						<div class="portlet-content">
						<table width="225" border="0">
							<tr>
							<td width="89"><s:text name="stafa.label.id"/></td>
							<td width="120"><div id="productId"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.type"/></td>
							<td><div id="productType"></div></td>
						  </tr>
						</table>						
					</div>
				</div>
				</div>
				
<!-- Product-->

<!-- Donor start-->
					<div class="column" id="donordiv">
					<div class="portlet">
						<div class="portlet-header notes"><s:hidden id="moduleName" value="processing"/><s:hidden id="widgetName" value="donnor"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.donor"/></div>
						<div class="portlet-content">
							<table width="225" border="0">
								<tr>
									<td width="89"><s:text name="stafa.label.id"/></td>
									<td width="120"><div id="donorId"></div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.name"/></td>
									<td><div id="donorName"></div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.donoraborh"/></td>
									<td><div id="donorAboRh"></div></td>
								</tr>
							</table>
						</div>
					</div>
					</div>

<!-- Donor-->
				
				</div>
				
			