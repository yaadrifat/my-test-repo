<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript" src="js/maintenance.js"></script>
<link rel="stylesheet" href="css/cssmenuhorizontal.css" type="text/css" />

<input type="hidden" id="pageurl" />
<div class="cleaner"></div>

<!--float window-->
<section>

<!-- Maintenance Tree start-->
				<div  class="portlet" style="height:96%">
					<div class="portlet-headers"><s:text name="stafa.maintenance.label.header" /></div>
						<div class="left_box">
							<ul  id="navmenu">			  
							 </ul>
						</div>
<!-- Maintenance Tree End-->

<!--float window end-->

<!--right window start -->	
<div class="right_box">	
	

	<div class="column">		
		<div  class="portlet">
		<div class="portlet-header maintaddnew" ><s:hidden id="callistTable" value="table-1"/><s:hidden id="callistType" value=""/><span id="portletsubtitlediv"></span> </div>
		<div class="portlet-content" id="subtablemaint">
				
			
<table id="table-1" cellspacing="0" cellpadding="2">

</table>

		</div>		
	</div>
	</div>
	
 <!-- Start of Second widget -->
 	<div class="column">		
		<div  class="portlet" id="divcon">
		<div class="portlet-header maintaddnew"><s:hidden id="callistTable" value="table-2"/><s:hidden id="callistType" value=""/><span id="callisttitle"></span> </div>
				<div class="portlet-content" id="subtablemaint1">
				<table id="table-2" cellspacing="0" cellpadding="2">

</table>
	</div>
	</div>
	</div>
	<!-- end of the second widget -->
	
	<div align="right"><form action="#"><input type="button" value="Save" onclick="javascript:return saveCodelst('seq');" /></form></div>

<div style="float:left;">
		Drag Row to reorder sequence
	</div>
</div>
</div>
</section>
<div class="cleaner"></div>
<div id="qcmaintdiv"></div>

<%-- 	<div id="maintpopup" style="display:none">
		<form action="#" id="maintenanceform">
			<input type="hidden" name="seqarray" id="seqarray" />	
			<input type="hidden" name="pkCodelst" id="pkCodelst"/>
			<input type="hidden" name="type" id="type">
			<input type="hidden" name="isHide" id="isHide">
			<input type="hidden" name="maintDeletedFlag" id="maintDeletedFlag" value="0">
			<input type="hidden" name="maintainable" id="maintainable" value="0">
			<input type="hidden" name="defaultValue" id="defaultValue" />
				<table width="100%">
					<tr>
						<td><s:text name="stafa.maintenance.label.name" /></td>
						<td><input type="text" name="description" id="description" onKeyUp="javascript:loadAutoCodelstData('');" /></td>
					
						<td><s:text name="stafa.maintenance.label.code" /></td>
						<td><input type="text" name="subType" id="subType" onKeyUp="javascript:loadAutoCodelstData('subtype');" /></td>
					</tr>
					<tr>
						<td><s:text name="stafa.maintenance.label.value" /></td>
						<td><input type="text" name="codelstvalue" id="codelstvalue" /></td>
					
						<td><s:text name="stafa.maintenance.label.comments" /></td>
						<td><input type="text" name="comments" id="comments" ></td>
					</tr>
					 <tr>
						<td><s:text name="stafa.maintenance.label.default" /></td>
						<td><input type="checkbox" name="tempdefault" id="tempdefault"></td>
						<td><s:text name="stafa.maintenance.label.deactivate" /></td>
						<td><input type="checkbox" name="tempisHide" id="tempisHide"></td>
						<td align="left"><s:text name="stafa.label.delete" /></td>
						<td align="left"><input type="checkbox" name="tempisDelete" id="tempisDelete"></td>
					</tr>
					
					<tr><td colspan="4">&nbs?Yeschild=on#p;</td></tr>
					<tr><td colspan="4" align="center" id="messagetd">&nbsp;</td></tr>
					<tr>
						<td colspan="4" align="center"><input type="button" value="New" onclick="javascript:clearvalue();" />&nbsp;&nbsp;<input type="button" value="Save" onclick="javascript:return saveCodelst();" />	</td>
					</tr>
			
				</table>	

					
		   </form>	
		   </div> --%>	
		   <div id="autocontentdiv" style="position:relative; display:none;" >
		   	<table cellpadding="0" cellspacing="0">
		   		<tr>
					<td width="25" class="popcornerlefttop">&nbsp;</td>
					<td class="poplinetop">&nbsp;</td>
					<td align="left" valign="bottom" class="popcornertopright"><a href="#" onclick="javascript:hideAutoCodelst();"><img src="images/close1.gif" alt="Close" width="14" height="13" border="0"></a></td>
				 </tr>
				  <tr>
				    <td class="poplineleft">&nbsp;</td>
				    <td><div  style="height:100px;overflow:auto">
				   		<table id="autocontenttable">
					   			
				   		</table></div>
				   	</td>
				   	<td class="poplineright">&nbsp;</td>
				  </tr>
				  <tr>
				    <td width="25" class="popcornerbottomleft">&nbsp;</td>
				    <td class="poplinebottom">&nbsp;</td>
				    <td class="popcornerrightbottom">&nbsp;</td>
				  </tr> 		 
		   		</table>
		   </div>	
	

