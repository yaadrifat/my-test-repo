<%@ include file="common/includes.jsp" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page import="com.velos.stafa.util.VelosStafaConstants" %>
<%
 String constant = null;
 if(request.getParameter("Contant")!=null)
	 constant = request.getParameter("Contant");
 String constantVal = null;
 if(request.getParameter("ConstantVal")!=null)
	 constantVal = request.getParameter("ConstantVal");
 String currentVal = null;
 if(request.getParameter("CurrentVal")!=null)
	 currentVal = request.getParameter("CurrentVal");
 String updateVal = null;
 if(request.getParameter("UpdateVal")!=null)
	 updateVal = request.getParameter("UpdateVal");
 request.setAttribute("currentVal", currentVal);
 request.setAttribute("constant", constant);
 request.setAttribute("constantVal", constantVal);
 request.setAttribute("updateVal", updateVal);
%>
<form id="nextform">
<s:hidden name="currentVal" value="%{#request.currentVal}"/>
<s:hidden name="updateVal" value="%{#request.updateVal}"/>
<s:hidden name="visitType" value="%{#request.constant}"/>
<s:hidden name="transplantId" value="%{#request.transplantId}"/>
<s:hidden name="visitId" value="%{#request.visitId}"/>
<s:hidden name="recipientD.pkRecipient" value="%{#request.recipient}"/>
	<div class="column">
	    <div  class="portlet">
	    <div class="portlet-header">Next Step</div>
	        <div class="portlet-content">
	             <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
	                 <tr>
	                    <td class="tablewapper">Next Step</td>
	                    <td class="tablewapper1">
	                             <select id='nextVal' name="nextVal">
	                                  <option value="" selected="selected">Select</option>
	                                  <option value="<s:property value="#request.constantVal"/>"><s:property value="#request.constant"/></option>
	                             </select>
	                    </td>
	                    <td class="tablewapper"></td>
	                    <td class="tablewapper1"></td>
	                </tr>
	                <tr>
	                    <td class="tablewapper">Sign Off</td>
	                    <td class="tablewapper1"><input type="text" id=""/></td>
	                    <td class="tablewapper">Date</td>
	                    <td class="tablewapper1"><input type="text" id="" class="dateEntry"/></td>
	                </tr>
	               
	            </table>
	        </div>
	    </div>
	</div>
	<section>
	<div class="cleaner"></div>
	        <div  style="float:right;">        
	            <input type="button" onclick="saveRecipientWorkflowTask('nextform')" value="Save" />        
	        </div>
	</section>
</form>