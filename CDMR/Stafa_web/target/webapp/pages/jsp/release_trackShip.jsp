<%@ taglib prefix="s" uri="/struts-tags"%>
<script>
$( ".column" ).sortable({
	connectWith: ".column",
    cursor: 'move',
    cancel:	".portlet-content"
});
$(document).ready(function(){
		var oTable = $("#trackShippingTable").dataTable({
		 										"bJQueryUI": true,
		 										"sDom": 'C<"clear">Rlfrtip',
		 										"aoColumns": [ { "bSortable":false},
		 										               null,
		 										               null,
		 										               null,
		 										               null,
		 										               null,
		 										               null,
		 										               null,
		 										               null
		 										              ],
		 										"oColVis": {
		 											"aiExclude": [ 0 ],
		 											"buttonText": "&nbsp;",
		 											"bRestore": true,
		 											"sAlign": "left"
		 										},
		 										"fnInitComplete": function () {
		 									        $("#trackShippingColumn").html($('.ColVis:eq(0)'));
		 										}
												});
		oTable.thDatasorting('trackShippingTable');
		
		
	    $("select").uniform(); 
  });
</script>
<div class="cleaner"></div>

<div class="cleaner"></div>
 
<div class="column">		
	<div  class="portlet">
		<div class="portlet-header notes">Track Shipping</div>
			<div class="portlet-content">
				<div style="overflow-x:auto;overflow-y:hidden;">
				<form>
				<table cellpadding="0" cellspacing="0" border="0" id="trackShippingTable" class="display">
				<thead>
					   	<tr>
					   		<th width="4%" id="trackShippingColumn"></th>
							<th><s:text name="stafa.label.patientid"/></th>
							<th><s:text name="stafa.label.patientname"/></th>
							<th><s:text name="stafa.label.productid"/></th>
							<th><s:text name="stafa.label.shippeddate"/></th>
							<th><s:text name="stafa.label.trackingnum"/></th>
							<th><s:text name="stafa.label.status"/></th>
							<th><s:text name="stafa.label.shiplog"/></th>
							<th><s:text name="stafa.label.recipientsignature"/></th>
					  	</tr>
					  	<tr>
					   		<th width="4%" id="trackShippingColumn"></th>
							<th><s:text name="stafa.label.patientid"/></th>
							<th><s:text name="stafa.label.patientname"/></th>
							<th><s:text name="stafa.label.productid"/></th>
							<th><s:text name="stafa.label.shippeddate"/></th>
							<th><s:text name="stafa.label.trackingnum"/></th>
							<th><s:text name="stafa.label.status"/></th>
							<th><s:text name="stafa.label.shiplog"/></th>
							<th><s:text name="stafa.label.recipientsignature"/></th>
					  	</tr>
				</thead>
				<tbody>
						<tr>
							<td></td>
							<td>MDA789539</td>
							<td>Jennifer Lee</td>
							<td>BMT125-3,BMT125-9</td>
							<td>Jul 9,2012</td>
							<td>184394294724</td>
							<td><select><option>In transit</option></select></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>MDA789539</td>
							<td>Sarah McDonald</td>
							<td>BMT124-1,BMT124-2,BMT124-3,BMT124-7,BMT124-8</td>
							<td>Jul 7,2012</td>
							<td>178324632203</td>
							<td><select><option>Received</option></select></td>
							<td><img src="images/icons/Black_Paperclip.jpg"></td>
							<td><img src="images/icons/Black_Paperclip.jpg"></td>
						</tr>
					</tbody>
				</table>
				</form>
			</div>
		</div>
	</div>	
</div>	




<script>

$(function() {
	$('.jclock').jclock();
	
	$( ".column" ).sortable({
		connectWith: ".column"
			/*,
        handle: '.portlet-header',
        cursor: 'move',
        forcePlaceholderSize: true*/
			
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});

	/**For Notes**/
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

	/* $( ".portlet-header .ui-addnew" ).click(function() {
		showPopUp("open","addNewDialog","Add New Acquision","900","450")
	}); */
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");
	$( ".portlet-header .ui-reset " ).click(function() {
		var formName = document.getElementById("login");
		//clearForm(formName);
		clear();
	});

// 	$( ".portlet-header .ui-notes " ).click(function() {
// 		showNotes('open');
// 	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});


var now = new Date();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivalDate').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new Date()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};



$("#timeZone").html(timezones[-offset / 60]);

</script>
 