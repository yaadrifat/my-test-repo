<%@ taglib prefix="s" uri="/struts-tags"%>
<!-- <link rel="text/css" href="css/widgets.css" type="text/css" /> -->
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<link rel="stylesheet" href="css/cssmenuhorizontal.css" type="text/css" />

<script type="text/javascript" src="js/dataSearch.js"></script>

<script type="text/javascript" src="js/maintenance.js"></script>

<script type="text/javascript" src="js/smartspinner.js"></script>
<!-- 
<script type="text/javascript" src="js/jquery/jquery.ui.droppable.js"></script>
<!-- 
<script type="text/javascript" src="js/protocolconfig.js"></script>
-->
<link type="text/css" href="css/smartspinner.css" rel="stylesheet" />
<!-- <script type="text/javascript" src="js/panel_maintenance.js"></script>
-->
<script type="text/javascript" src="js/util.js"></script>

<script>

$( ".dragItem ul" ).draggable({ revert: "valid", connectToSortable: '.dropItem', helper: 'clone'});
//$( "input[type='text']" ).disableSelection();
/*Draggable Items functions End*/

/*Droppable Items functions Start*/
 $( ".dropItem" ).sortable({
    connectWith: ".dropItem",
});

var elementCount =1;
 function destroy(obj){
	 	$(obj).parent().remove();
	 }

 
function setFormula(){
    //alert("setFormula" );
    //alert( " formula " + $("#formulText").val());
    //alert( " formula " + $("#hformulText").val());
    var fieldCount = $("#currentFieldCount").val();
    $("#formula_"+fieldCount).val($("#formulaText").val());
    $("#formula").val($("#formulaText").val());
    
    $("#fieldName").val($("#formulaname").val());
    $("#labelName").val($("#formulaname").val());
    $("#pccalc_"+fieldCount).val($("#formulaname").val());
    setValues();
    $("#preview").dialog("destroy");
}

	//?BB var fieldCount = 1;
	$(document).ready(function() {
		show_slidewidgets('slidecolumnfilter', false);
		hide_slidecontrol();
		constructTables(false);
		clearTracker();
		show_innernorth();
		$("#widgetCalc").removeClass("ui-helper-clearfix");
		$("#widgetlist").removeClass("ui-helper-clearfix");
		$("#fieldProperties").bind("change keyup ", function(){
			setValues();
		});
	})

	function savePages() {
		try {
			if(checkValidation()){
				if($("#widgetCalc").hasClass('hidden')){
					if (saveWidgets()) {
						$("#widgetCalc").removeClass("ui-helper-clearfix");
						
						return true;
					}
				}else{
					if (saveWidgetDetails()) {
						$("#widgetlist").removeClass("ui-helper-clearfix");
						return true;
					}
				}
			}
		} catch (e) {
			alert("exception " + e);
		}
	}

	function saveWidgetDetails(){
		//alert("saveWidgetdetails");
		var widgetLib= $("#widgetRef").val();
		var rowData ="";
		var fieldCount=0;
		var rowNum =0,colNum=-1,widgetConfig =0;
		$("#dropDiv").find(".dropItem ").each(function(i, element) {
			colNum+=1;
			rownum =0;
			$(this).find("ul").find("input").each(function() {
				tmpId = $(this).attr("id");
				tmpTd ="";
				if(tmpId.indexOf("_") >0){
					fieldCount = tmpId.substring(tmpId.indexOf("_")+1 );
				}
				widgetConfig = $("#widgetConfig_"+fieldCount).val();
				if(widgetConfig == undefined || widgetConfig =="" ){
					widgetConfig =0;
				}
				rowData +="{widgetConfig:'"+widgetConfig+"',widgetLib:"+widgetLib+",rowNumber:'"+rownum+"',colNumber:'"+colNum+"',";
				rowData +="fieldType:'"+$("#type_"+fieldCount).val()+"',fieldValue:'"+$("#formula_"+fieldCount).val()+"',datafieldConifg:'"+$("#configId_"+fieldCount).val()+"',";
				rowData +="labelName:'"+$("#labelName_"+fieldCount).val()+ "',required:'"+$("#required_"+fieldCount).val()+"',fieldName:'" +$("#name_"+fieldCount).val()+"'}," ;
				rownum +=1;
			});
		});
		
		if (rowData.length > 0) {
			rowData = rowData.substring(0, (rowData.length - 1));
		}
		jsonData = "{'widgetDetails':["+ rowData + "]}";
		//alert("jsonData " + jsonData);

		// save panel data
		$('.progress-indicator').css('display', 'block');
		url = "saveWidgetDetails";
		//alert("json data " + jsonData);
		response = jsonDataCall(url, "jsonData=" + jsonData);
		$('.progress-indicator').css('display', 'none');
		return true;		
	}
	
	function refreshPage() {
		constructTables(true);
		var url = "fetchWidget";
		var result = ajaxCall(url, '');
		$("#widgetFacet").replaceWith($('#widgetFacet', $(result)));
	}

	function constructTables(flag) {

		var widgets_serverParam = function(aoData) {
			aoData.push({
				"name" : "application",
				"value" : "stafa"
			});
			aoData.push({
				"name" : "module",
				"value" : "maintenance_widgetlibrary"
			});

			aoData.push({
				"name" : "col_1_name",
				"value" : ""
			});
			aoData.push({
				"name" : "col_1_column",
				"value" : ""
			});
			/*	
				aoData.push( { "name": "col_2_name", "value": "lower(RS_NAME)"});
				aoData.push( { "name": "col_2_column", "value": "lower(RS_NAME)"});
				
				aoData.push( {"name": "col_3_name", "value": "lower(RS_DESCRIPTION)" } );
				aoData.push( { "name": "col_3_column", "value": "lower(RS_DESCRIPTION)"});
				
				aoData.push( {"name": "col_4_name", "value": "QUANTITY" } );
				aoData.push( { "name": "col_4_column", "value": "QUANTITY"});
			 */
		};
		var link = "";
		var widgets_aoColumnDef = [
				{
					"aTargets" : [ 0 ],
					"mDataProp" : function(source, type, val) {
						return "";
					}
				},
				{
					"sTitle" : "Check All <input type='checkbox'  id='checkallWidgets' onclick='checkall(\"widgetTable\",this)'>",
					"aTargets" : [ 1 ],
					"mDataProp" : function(source, type, val) {
						return "<input type='checkbox' id='widgetLib' name='widgetLib' value='"+source.widgetLib +"' /><input type='hidden' name='code' id='code' value='"+ source.code +"' />";
					}
				},
				{
					"sTitle" : "Name",
					"aTargets" : [ 2 ],
					"mDataProp" : function(source, type, val) {

						link = "<a href='#' onclick='getWidgetDetails("
								+ source.widgetLib + ",\"" + source.name+ "\",\""+source.module+"\");' >" + source.name + "</a>";
						return link;
					}
				},
				{
					"sTitle" : "Module",
					"aTargets" : [ 3 ],
					"mDataProp" : function(source, type, val) {
						return source.module;
					}
				},
				{
					"sTitle" : "Description",
					"aTargets" : [ 4 ],
					"mDataProp" : function(source, type, val) {
						return source.description;
					}
				},
				{
					"sTitle" : "Activated",
					"aTargets" : [ 5 ],
					"mDataProp" : function(source, type, val) {

						if (source.active == 1) {
							link = "<input type='radio' name='active_"+source.widgetLib+"' id='activeyes' value='Yes' checked='checked' >Yes</input>";
							link += "<input type='radio' name='active_"+source.widgetLib+"' id='active' value='No' >No</input>";
						} else {
							link = "<input type='radio' name='active_"+source.widgetLib+"' id='activeyes' value='Yes'  >Yes</input>";
							link += "<input type='radio' name='active_"+source.widgetLib+"' id='active' value='No' checked='checked'>No</input>";
						}
						link += "<input type='hidden' name='appLock' id='appLock' value='" +source.applock  +"' />";
						return link;
					}
				} ];

		var widgets_aoColumn = [ {
			"bSortable" : false
		}, {
			"bSortable" : false
		}, null, null, null ];

		var widgets_ColManager = function() {
			$("#widgetsColumn").html($('#widgetTable_wrapper .ColVis'));
		};

		var strBreadcrumb = "<a href='#' id='breadlevel1' onclick='constructTables(true);'>Widget Library</a> ";
		$("#breadcrumb").html(strBreadcrumb);
		constructTable(flag, 'widgetTable', widgets_ColManager,
				widgets_serverParam, widgets_aoColumnDef, widgets_aoColumn);
		$("#widgetlist").removeClass("hidden");

		$("#widgetCalc").addClass("hidden");
		$("#widgetCalc").removeClass("ui-helper-clearfix");
	}

	function addRows(widgetLib, widgetName, module, widgetDesc, copyWidget,
			activeFlag) {
		var moduleDrop = "<select name='module' id='module'>"
				+ $.trim($("#codeListModule").html()) + "</select>";
		elementcount += 1;
		var newRow = "<tr><td>&nbsp;</td><td><input type='checkbox' id='widgetLib' value='"+widgetLib+"'> <input type='hidden' id='copyWidget' name='copyWidget' value='"+copyWidget+"' /></td><td><input type='text' id='widgetName' name='widgetName' value='"+widgetName+"' /></td>";
		newRow += "<td>" + moduleDrop + "</td>";
		newRow += "<td><input type='text' id='widgetDesc' name='widgetDesc' value='"+widgetDesc+"' /></td>";
		newRow += "<td><input type='radio' value='Yes' name='active_a"
				+ elementcount + "' id='activeyes' ";
		if (activeFlag) {
			newRow += " checked='checked' ";
		}
		newRow += " >Yes</input><input type='radio' value='No' name='active_a"
				+ elementcount + "' id='active' ";
		if (!activeFlag) {
			newRow += " checked='checked' ";
		}
		newRow += "  >No</input> <input type='hidden' name='appLock' id='appLock' value='0' /></td></tr>";

		$("#widgetTable tbody[role='alert']").prepend(newRow);

		if (module != "") {
			//alert("module"+module);
			//alert("html " + $("#widgetTable tbody[role='alert'] tr:first-child").html());
			$("#widgetTable tbody[role='alert'] tr:first-child")
					.find("#module").val(module);
		}
		$("#widgetTable .ColVis_MasterButton").triggerHandler("click");
		$('.ColVis_Restore:visible').triggerHandler("click");
		$("div.ColVis_collectionBackground").trigger("click");
		$.uniform.restore('select');
		$('select').uniform();

	}

	function copy_Widget(widgetlib, name, module, description) {
		//alert("copy protocol");
		var copyText = " - Copy";
		//	$("#breadlevel1").trigger('click');
		var strBreadcrumb = "<a href='#' id='breadlevel1' onclick='constructTables(true);'>Widget Library</a> ";
		$("#breadcrumb").html(strBreadcrumb);
		$("#widgetlist").removeClass("hidden");

		$("#widgetCalc").addClass("hidden");
		addRows(0, name + copyText, module, description, widgetlib, true);

	}

	function saveWidgets() {
		$('.progress-indicator').css('display', 'block');
		var rowData = "";
		var widgetLib;
		var name;
		var codes;
		var module;
		var copyWidget;
		var description;

		var isActivate = 0;
		$("#widgetTable").find("#widgetName").each(
				function(row) {
					rowHtml = $(this).closest("tr");
					//alert("rowHtml" +rowHtml);
					isActivate = 0;
					name = "";
					codes = "";
					module = "";
					copyWidget = "";
					description = "";
					appLock = "0";
					widgetLib = $(rowHtml).find("#widgetLib").val();
					//alert("protocol " + protocol);
					copyWidget = $(rowHtml).find("#copyWidget").val();
					if (copyWidget == undefined) {
						copyWidget = 0;
					}
					//alert("copyProtocol " + copyProtocol);
					name = $(rowHtml).find("#widgetName").val();
					//alert("protocolName " + protocolName);

					codes = $(rowHtml).find("#code").val();
					if (codes == undefined) {
						codes = name.replace(/\s/g, '').toLowerCase()
					}
					module = $(rowHtml).find("#module").val();

					description = $(rowHtml).find("#widgetDesc").val();
					appLock = $(rowHtml).find("#appLock").val();
					if (appLock != 1) {
						appLock = 0;
					}
					if ($(rowHtml).find("#activeyes").is(":checked")) {
						isActivate = 1;
					} else {
						isActivate = 0;
					}
					//alert("name " + name);
					if (name != undefined && name != "") {
						rowData += "{widgetLib:" + widgetLib + ",active ="
								+ isActivate + ",name:'" + name + "',code:'"
								+ codes + "',module:'" + module + "',appLock:'"
								+ appLock + "',description:'" + description
								+ "',copyWidget:'" + copyWidget + "'},";
					}
				});
		//alert("rowData " + rowData);
		if (rowData.length > 0) {
			rowData = rowData.substring(0, (rowData.length - 1));
		}
		url = "saveWidgets";
		response = jsonDataCall(url, "jsonData={widgetData:[" + rowData + "]}");

		//BB$("select").uniform();
		alert("Data Saved Successfully");
		$('.progress-indicator').css('display', 'none');
		refreshPage();
		return true;

	}

	function editRows() {
		//  alert("edit Rows");
		$("#widgetTable tbody tr").each(function(row) {
							if ($(this).find("#widgetLib").is(":checked")) {
								var tmpVal = $(this).find("#appLock").val();
								$(this).find("td").each(function(col) {
									if (col == 2) {
										if ($(this).find("#widgetName").val() == undefined) {
											$(this).html("<input type='text' id='widgetName' name='widgetName' value='"+ $(this).text()+ "' />");
										}
									} else if (col == 3) {
										if ($(this).find("#module").val() == undefined) {
											var oldModuleVal = $(this).text();
											var moduleDrop = "<select name='module' id='module'>"+ $.trim($("#codeListModule").html())+ "</select>";
											$(this).html(moduleDrop);
											$(this).find("#module").val(oldModuleVal);
											if (tmpVal == 1) {
												$(this).find("#module").attr("disabled",true);
											}
											//alert("sss");
											$.uniform.restore('select');
											$("select").uniform();
										}
									} else if (col == 4) {
										if ($(this).find("#widgetDesc").val() == undefined) {
											$(this).html("<input type='text' id='widgetDesc' name='widgetDesc' value='"+ $(this).text()+ "' />");
										}
									}
								});
							}
						});

	}

	function deleteRows() {
		var deletedIds = "";
		$("#widgetTable tbody tr").each(function(row) {
			if ($(this).find("#widgetLib").is(":checked")) {

				deletedIds += $(this).find("#widgetLib").val() + ",";
			}
		});
		//	alert("deletedIds "+ deletedIds);
		if (deletedIds.length > 0) {
			var yes = confirm(confirm_deleteMsg);
			if (!yes) {
				return;
			}
			deletedIds = deletedIds.substr(0, deletedIds.length - 1);
			//alert("deletedIds " + deletedIds);
			jsonData = "{deletedIds:'"
					+ deletedIds
					+ "',domainName:'DOMAIN_WIDGETLIB',domainKey:'WIDGETLIB_DOMAINKEY'}";
			url = "commonDelete";
			//alert("json data " + jsonData);
			response = jsonDataCall(url, "jsonData=" + jsonData);
			refreshPage();
		}
	}
	var jsonFields = [];
	function getWidgetDetails(widgetLib, widgetName,module) {
		//alert("getWidgetdetails");

		var strBreadcrumb = "<a href='#' onclick='constructTables(true);'>Widget Library</a> &gt; <a href='#' onclick='getWidgetDetails(\""
				+ widgetLib
				+ "\",\""
				+ widgetName
				+"\",\""
				+module
				+ "\");'> "
				+ widgetName
				+ "</a> &gt; Calculations";
		$("#breadcrumb").html(strBreadcrumb);
		$("#widgetlist").addClass("hidden");
		$("#widgetlist").removeClass("ui-helper-clearfix");
		$("#widgetCalc").removeClass("hidden");
		$("#widgetRef").val(widgetLib);
		var jsonData = '';
		 var jsonId='';
	try{
		var ajaxresult = jsonDataCall('widgetDetails', "jsonData={widgetLib:"+widgetLib + ",module:'"+module+"'}");
		$(ajaxresult.fieldList).each(function() {
		
			//alert("this.pkCodelst " + this.pkCodelst);
		//	  jsonData += '{"id":"'+this.pkCodelst+'","description":"'+this.description+'","fieldType":"'+this.fieldType+'","module":"'+this.module+'","subType":"'+this.subType+'","type":"'+this.type+'"},';
		//alert("trace1");
		jsonId += '"'+this.pkCodelst+'",';
		jsonData += '"'+this.pkCodelst+'" :{"description":"'+this.description+'","fieldType":"'+this.fieldType+'","module":"'+this.module+'","subType":"'+this.subType+'","type":"'+this.type+'"},';
			var option = $('<option />');
			option.attr('value', this.pkCodelst).text(this.description);
			$('#calcFields').append(option);
			

		});
		if (jsonData.length > 0) {
			jsonId = '"ids":['+jsonId.substring(0, (jsonId.length - 1))+']';
			//jsonData ='['+ jsonData.substring(0, (jsonData.length - 1)) +']';
			jsonData ='"data":{'+ jsonData.substring(0, (jsonData.length - 1)) +'}';
			//alert("jsonData " + jsonData);
			jsonData = '{'+jsonId+','+jsonData + '}';
			//alert("jsonData1 " + jsonData);
			jsonFields = JSON.parse(jsonData);
			//alert("jsonObject " + jsonFields);
			
			
		}
		
		var precolno = -1;
		var colno = -1;
		var calcHtml="";
		$(ajaxresult.widgetConfig).each(function() {
			
			
		// alert("calcHtml befor col " + calcHtml);
			colno = eval("this.colNumber");
			// alert(" col "+ precolno +"!="+ colno );
			 if(precolno == -1){
						calcHtml += '<div class="dropItem ui-droppable ui-sortable">';
						calcHtml += '<img class="destroydiv" width="16" height="16" onclick="javascript:removeObject(this);" src="images/icons/closebox.png">';
						//calcHtml += '<a href="#" onclick="javascript:destroy(this);"><img src="images/icons/closebox.png"	height="16" width="16" class="destroydiv"/></a>';
			 }
			 
			if (precolno != colno) {
				if (precolno != -1) {
					calcHtml += '</div><div class="dropItem ui-droppable ui-sortable">';
					calcHtml += '<img class="destroydiv" width="16" height="16" onclick="javascript:removeObject(this);" src="images/icons/closebox.png">';
					//calcHtml += '<a href="#" onclick="javascript:destroy(this);"><img src="images/icons/closebox.png"	height="16" width="16" class="destroydiv"/></a>';
				}
				precolno = colno;
			}

			calcHtml += '<ul class="ui-state-default ui-draggable" style="display: block;">';
			//calcHtml += '<input type="hidden" id="widgetLib_'+ colno + '_' + eval("this.rowNumber")+ '" value="' + eval("this.widgetLib")+ '">';
			
			calcHtml += '<input type="text" ';
			
			if (eval("this.fieldType") == "LABEL") {
				calcHtml += ' id="pclabel_'+elementCount+'" class="pclabel" placeholder="Label" ';
				
			} else if (eval("this.fieldType") == "TEXT") {
				calcHtml += ' id="pctext_'+elementCount+'" class="pctext" placeholder="EntryField" ';
			
			} else if (eval("this.fieldType") == "CALC") {
				calcHtml += ' id="pccalc_'+elementCount+'" class="pccalc" placeholder="Calculation" ';
			//	calcHtml += ' value = "' + eval("this.fieldValue") + '">';
			} else if (eval("this.fieldType") == "DROPDOWN") {
				
				calcHtml += ' id="pcdrop_' + elementCount + '" class="pcdrop pctext" ';
				
			}else{
				calcHtml += ' id="pctext_' + elementCount +'" class="pctext" ';
			} 
			calcHtml += ' value = "'+ eval("this.labelName") + '">';
			calcHtml += '</input><a href="#" onclick="javascript:destroy(this);"><img src="images/icons/closebox.png"	height="16" width="16" class="destroy"/></a>';

			calcHtml += '</ul>';
			
			fieldCount = elementCount ;
			//alert("calcHtml end of col " + calcHtml);
			var fieldHtml ="<span id='field_"+fieldCount+"'><input type='hidden' name='name_"+fieldCount+"' id='name_"+fieldCount+"' value='"+eval("this.fieldName")+"' />"+
			"<input type='hidden' name='type_"+fieldCount+"' id='type_"+fieldCount+"' value='"+eval("this.fieldType")+"' />"+
			"<input type='hidden' name='configId_"+fieldCount+"' id='configId_"+fieldCount+"' value='"+eval("this.datafieldConifg")+"' />"+
			"<input type='hidden' name='labelName_"+fieldCount+"' id='labelName_"+fieldCount+"' value='"+eval("this.labelName")+"' />"+
			"<input type='hidden' name='labelPosition_"+fieldCount+"' id='labelPosition_"+fieldCount+"'  />"+
			"<input type='hidden' name='masking_"+fieldCount+"' id='masking_"+fieldCount+"'  />"+
			"<input type='hidden' name='required_"+fieldCount+"' id='required_"+fieldCount+"' value='"+eval("this.required")+"' />"+
			"<input class='formula' type='hidden' name='formula_"+fieldCount+"' id='formula_"+fieldCount+"' value='"+eval("this.fieldValue")+"' />"+
			"</span>";
			$("#fieldDetails").append(fieldHtml);
			elementCount +=1;
		});
		calcHtml+='</div>';
		//alert(" calcHtml --------------> " + calcHtml);
		$("#dropDiv").html(calcHtml);
		dropevent();
			}catch(err){
				alert("error " + err);
			}
	}

	//--------------- end of new protocol config--------

	//? BB var inputCount = 0;
	function setProperties(obj,resetFlag){
		//alert("set setProperties");
		console.log("setProperties" );
		try{
		var objid = $(obj).attr("id");
		var objValue = $.trim($(obj).val());
		var fieldLibVal; 
		console.log("has class " + $(obj).hasClass("pclabel"));
		if($(obj).hasClass("pclabel") ){
			 fieldLibVal = $("#calcLabel option[value='"+ $("#calcLabel").val() +"']").text();
			 if(fieldLibVal == ""){
					fieldLibVal = objValue;
				}
		}else{
			 fieldLibVal = $("#calcFields option[value='"+ $("#calcFields").val() +"']").text();
		}
		
		console.log("objid " +objid + " />" + objValue+"< flag " + resetFlag + " / selectval>" + fieldLibVal+"<");
		if( fieldLibVal =="" && objValue != ""){
			$("#fieldId").val(0);
			$("#fieldName").val(objValue);
			$("#labelName").val(objValue);
			$("#fieldType").val("");
			setValues();
		}else{
		//$("#calcFields option:contains('"+objValue+"')").val();
		//var fieldLibVal = $("#calcFields option[value='"+ $("#calcFields").val() +"']").text();
		console.log("objid " +objid + " />" + objValue+"< flag " + resetFlag + " / selectval>" + fieldLibVal+"<");
		//console.log("autocomplete -/data"+$(obj).data() + " / " +$( obj ).data( "autocomplete" ).selectedItem );
	/*	var tmpObj = $(obj).data("autocomplete" ).element;
		$.each(tmpObj, function(key, info) {
			console.log(key +"/----->"+ info);
	 
	    });
	*/	var fieldCount = 0;
		if(objValue =="" && fieldLibVal !="Select"){
			objValue=fieldLibVal;
		}
			if(objid.indexOf("_") >0){
				 fieldCount = objid.substring(objid.indexOf("_")+1 );
			}
			
			if (objid != "formulaname"){
				$("#currentFieldCount").val(fieldCount);
				
			}
			
			if(objValue =="" || resetFlag){
				console.log("Trace0");
				$("#name_"+fieldCount).val("");
			}
			console.log("Trace1");
			// set from hidden
			if($("#name_"+fieldCount).val()!=undefined && $("#name_"+fieldCount).val() != ""){
				console.log("inside if " );
				$("#fieldId").val($("#configId_"+fieldCount).val());
				$("#fieldName").val($("#name_"+fieldCount).val());
				$("#fieldType").val($("#type_"+fieldCount).val());
				$("#labelName").val($("#labelName_"+fieldCount).val());
				$("#labelPosition").val($("#labelPosition_"+fieldCount).val());
				$("#formula").val($("#formula_"+fieldCount).val());
				$("#maskingchange").val($("#masking_"+fieldCount).val());
			 	if($("#required_"+fieldCount).val() == 1 ){
					$("#required_yes").attr('checked','checked');
				}else{
					$("#required_no").attr('checked','checked');
				} 
				
			}else{
				console.log("inside else " );
				// set from data field lib
				var fieldId = $("#calcFields option:contains('"+objValue+"')").val();
				console.log("objValue>"+objValue+"<fieldId>" + fieldId+"<");
				if(objValue == fieldId){
					$("#fieldId").val(objValue);
					$("#fieldName").val(objValue);
					$("#fieldType").val("LABEL");
					$("#labelName").val(objValue);
				}else if(objValue != "" && fieldId !=undefined && fieldId!=""){
					$("#fieldId").val(fieldId);
					$("#fieldName").val(jsonFields.data[fieldId].subType);
					$("#fieldType").val(jsonFields.data[fieldId].fieldType);
					$("#labelName").val(jsonFields.data[fieldId].description);
			    }else{
					// reset the previous values
					$("#fieldId").val();
					$("#fieldName").val("");
					$("#fieldType").val("");
					$("#labelName").val("");
				}
				$("#labelPosition").val("inline");
				$("#formula").val("");
				$("#maskingchange").val("");
				$("#required_no").attr('checked','checked');
				setValues();
			}
			$.uniform.restore('#fieldType');
			$('#fieldType').uniform();
			$.uniform.restore('#maskingchange');
			$('#maskingchange').uniform();
			$.uniform.restore('#labelPosition');
			$('#labelPosition').uniform();	
			
			//alert("class " + $(obj).hasClass("pccalc"));
			if ($(obj).hasClass("pccalc") || objid == "formulaname") {
				console.log("set Properties ----show formula");
				$("#formulatr").removeClass("hidden");
					showFormula($(obj));
					
			}else{
				console.log("set Properties ---- hide formula");
				$("#formulatr").addClass("hidden");
			}
		}
		}catch(err){
			alert("error in set prop " + err);
		}
	}
	
	function clearProperties(obj){
		//alert("set values");
		console.log("clearProperties");
		console.log("id " + $(obj).attr("id"));
		if ($(obj).attr("id")!= "formulaname"){
			$("#currentFieldCount").val("");
		}
		$("#calcFields").val("");
		$("#fieldId").val("");
		$("#fieldName").val("");
		$("#fieldType").val("");
		$("#labelName").val("");
		$("#labelPosition").val("");
		$("#formula").val("");
		$("#maskingchange").val("");
		//alert("check yes " + $("#required_yes").is(":checked"));
		
		$.uniform.restore('#fieldType');
		$('#fieldType').uniform();
		$.uniform.restore('#maskingchange');
		$('#maskingchange').uniform();
		$.uniform.restore('#labelPosition');
		$('#labelPosition').uniform();
		
		
	}
	
	function setValues(){
		//alert("set values");
		var fieldCount = $("#currentFieldCount").val();
		$("#name_"+fieldCount).val($("#fieldName").val());
		$("#type_"+fieldCount).val($("#fieldType").val());
		$("#labelName_"+fieldCount).val($("#labelName").val());
		$("#labelPosition_"+fieldCount).val($("#labelPosition").val());
		$("#formula_"+fieldCount).val($("#formula").val());
		$("#masking_"+fieldCount).val($("#maskingchange").val());
		$("#configId_"+fieldCount).val($("#fieldId").val());
		//alert("check yes " + $("#required_yes").is(":checked"));
		if($("#required_yes").is(":checked")){
			$("#required_"+fieldCount).val("1");
		}else{
			$("#required_"+fieldCount).val("0");
		}
		$.uniform.restore('#fieldType');
		$('#fieldType').uniform();
		$.uniform.restore('#maskingchange');
		$('#maskingchange').uniform();
		$.uniform.restore('#labelPosition');
		$('#labelPosition').uniform();
		
		
	}
	function autoCompleteChangeEvent(obj){
		console.log(" autoCompleteChangeEvent ");
		if(!$(obj).hasClass("formulaname")){
			clearProperties(obj);
			setProperties(obj);
		}
	}
	var activeFlag = false;
	var tmpId="";
	function dropevent() {
		
		$("#dropDiv div").each(function(i, element) {
			$(element).find("input").each(function(j) {
				$(this).bind("click", function() {
					console.log("dropevent click")
					clearProperties(this);
					setProperties(this,false);
				});
				$(this).bind("keyup select change", function() {
					console.log(" dropevent keyup select change event")
					setProperties(this,true);
				});
			 });
		});

		$("#dropDiv div").droppable({
							
							activate: function(event,ui) {
								console.log("activeFlag " + activeFlag);
								if(!activeFlag){
									activeFlag =true;
									console.log("activate flag start " + activeFlag);
									clearProperties();
								}
							},
							
							
							drop : function(event, ui) {
								console.log("Drop event activeFlag " + activeFlag);
								dropObj = $(ui.draggable).find("input");
								
								var fieldCount = elementCount;
								var dropDownFlag = false;
								//$("#formulatr").addClass("hidden");
								
								dropid = $(dropObj).attr("id");
								
								$(ui.draggable).find("input").bind("click", function() {
									console.log("click event")
									clearProperties();
									setProperties($(ui.draggable).find("input"));
								});
								$(ui.draggable).find("input").bind("keyup", function() {
									console.log("keyup select [change event")
									setProperties($(ui.draggable).find("input"),true);
								});
								
								if (dropid == "pclabel") {
									setObjectAutoComplete(dropObj, "calcLabel");
								} else if (dropid == "pctext") {
									setObjectAutoComplete(dropObj,
											"calcFields");
								} else if (dropid == "pccalc") {
									
								//	$("#formulatr").removeClass("hidden");
									//alert("calc");
									/* $(dropObj).bind("click", function() {
										showFormula($(this));
									}); */
								 
								} 
								if(activeFlag){
									console.log("event  " + event.type);
									console.log("fieldCount " + fieldCount);
									
									if($(dropObj).attr("id").indexOf("_") ==-1){
										tmpId = $(dropObj).attr("id")+"_"+ fieldCount;
										if($("#field_"+fieldCount).val()== undefined){
											var fieldHtml ="<span id='field_"+fieldCount+"'><input type='hidden' name='name_"+fieldCount+"' id='name_"+fieldCount+"' />"+
														"<input type='hidden' name='type_"+fieldCount+"' id='type_"+fieldCount+"' />"+
														"<input type='hidden' name='configId_"+fieldCount+"' id='configId_"+fieldCount+"' />"+
														"<input type='hidden' name='labelName_"+fieldCount+"' id='labelName_"+fieldCount+"' />"+
														"<input type='hidden' name='labelPosition_"+fieldCount+"' id='labelPosition_"+fieldCount+"' />"+
														"<input type='hidden' name='masking_"+fieldCount+"' id='masking_"+fieldCount+"' />"+
														"<input type='hidden' name='required_"+fieldCount+"' id='required_"+fieldCount+"' />"+
														"<input class='formula' type='hidden' name='formula_"+fieldCount+"' id='formula_"+fieldCount+"' />"+
														"</span>";
											
											//$("#currentFieldCount").val(fieldCount);
											fieldCount = fieldCount + 1;
											elementCount = elementCount +1;
											$("#fieldDetails").append(fieldHtml);
										}
									}
								}
								$(dropObj).attr("id", tmpId);
									//  alert("inside if1");
									
									
							
								activeFlag = false;
								console.log("img flag " + $(ui.draggable).find("img").html());
								if ($(ui.draggable).find("img").html() == null) {
									$(dropObj).after('<a href="#" onclick="javascript:removeObject(this);"><img src="images/icons/closebox.png"	height="16" width="16" class="destroy"/></a>');
									}	
							}
							
							
					
						});

		$(".dropItem").sortable({
			connectWith : ".dropItem",
		});

	}

	function removeObject(obj){
		//alert("remove obj");
		var dropId = $(obj).parent().find("input").attr("id");
		destroy(obj);
		//alert(" t1 " + dropId);
		if(dropId.indexOf("_") >0){
			dropId = "field_"+dropId.substring(dropId.indexOf("_")+1 );
			//alert("dropId" + dropId);
			//alert("html" +$("#"+dropId).html());
			$("#"+dropId).remove();
		}
		
	}
	//==================End of new change =========

	var selectedWidget;
	function widgetselect(obj) {
		$(".widget").removeClass('highlightWidget');
		$(obj).addClass('highlightWidget');
		selectedWidget = obj;
		var offset = $(obj).offset();

		$(".dragItem").offset({
			top : offset.top
		});
		//alert("dragItem");
	}

	function createColumns() {
		var noc = $("#noc").val();
		var colDiv = "";
		if (noc > 0) {
			for (i = 0; i < noc; i++) {
				colDiv += "<div class=\"dropItem\" >";
				colDiv += "<img src='images/icons/closebox.png' onclick='javascript:removeObject(this);'	height='16' width='16' class='destroydiv'/>";
				colDiv += "</div>";
			}
		} else {
			//colDiv ="<div class=\"dropItem\" ></div>"
		}
		//   alert("colDiv " + colDiv);
		// $("#dropDiv").append(colDiv);
		$("#dropDiv").append(colDiv);
		dropevent();
	}

	function executePreviewFormulas() {
		//alert("executePreviewFormulas");
		console.log("executePreviewFormulas");
		var formula = "";
		$("#preview").find(".formula").each(function() {

					var hiddenObj = this;
					var hiddenId = $(hiddenObj).attr("id");
					 //alert("id = " + hiddenId + " val = > " + $(hiddenObj).val());
					formula = $(this).val();
					formula = formula.substring(formula.indexOf("=")+1 );
					console.log("formula " + formula);
					$("#preview").find(":input:not(:hidden)").each(function() {
						try{
								console.log($(this).attr("id") + " val = > " + $(this).val() + " / " + isNaN($(this).val()) );
								if ($.trim($(this).val()) != "" && !isNaN($(this).val()) ) {
									formula = formula.replace($(this).attr("id"), eval($(this).val()));
								} else {
									// replace 0 to null values
									formula = formula.replace($(this).attr("id"), 0);
								}
						}catch(err){
							alert("error " + err);
						}
							});
					console.log("formula before exec " + formula);
					var formulaValue = eval(formula);
					//alert("eval " + formulaValue);
					//alert("isfinite" +  );
					if (isFinite(formulaValue)) {
						//alert("in side if");
						formulaValue = round2Decimal(formulaValue);
					} else {
						//alert("in side else");
						formulaValue = "";
					}
					var objId = hiddenId.substring(0, hiddenId.indexOf("_"));
					//    alert("obj id " + objId);
					$("#" + objId).val(formulaValue);
				});

	}

	function showFormula(obj) {
		//alert("obj " + obj);
		tmpObj = obj;
		//alert("tmpobj id " + $(tmpObj).attr("id"));
		var varHtml = "<table><tr><td colspan='2'> Click a button to place in Field Calculation</td></tr><tr><td colspan='2'> <input type='text' id='formulaname' class='formulaname' placeholder='Type to Select a Target Field' style='width:100%'/> </td></tr><tr><td colspan='2'><select id='inputs' ><option value='' > Select </option>";
		var inputcount = $("#dropDiv [type=text]").length;
		//alert(inputcount)
		$("#dropDiv [type=text]").each(
				function() {
					if (!$(this).hasClass("pclabel") && $(this).val() != "") {
						varHtml += "<option value='" + $(this).val() + "' > "
								+ $(this).val() + " </option> ";
					}
				});
		varHtml += "</select>";

		varHtml += " &nbsp;<input type='button' value='Add' onclick='javascript:createFormula();' /> </td> ";

		varHtml += "<tr><td colspan='2'>";
		varHtml += "<table><tr><td><div class='button_calc' onclick='javascript:createFormula(this);'>7</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>8</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>9</div></td><td>&nbsp;</td> <td><div class='button_calc' onclick='javascript:createFormula(this);'>+</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>-</div></td></tr>";
		varHtml += "<tr><td><div class='button_calc' onclick='javascript:createFormula(this);'>4</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>5</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>6</div></td><td>&nbsp;</td> <td><div class='button_calc' onclick='javascript:createFormula(this);'>*</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>/</div></td></tr>";
		varHtml += "<tr><td><div class='button_calc' onclick='javascript:createFormula(this);'>1</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>2</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>3</div></td><td>&nbsp;</td> <td><div class='button_calc' onclick='javascript:createFormula(this);'>+</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>-</div></td></tr>";
		varHtml += "<tr><td colspan='2'><div class='button_calc0' onclick='javascript:createFormula(this);'>0</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>.</div></td><td>&nbsp;</td><td><div class='button_calc' onclick='javascript:createFormula(this);'>(</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>)</div></td></tr>";

		varHtml += "</table></td></tr>";

		varHtml += "<tr><td colspan='5'>&nbsp;</td></tr>";
		var formulaTxt = '';
		varHtml += "<tr><td colspan='5'><textarea id='formulaText' type='' value='"
				+ formulaTxt
				+ "' style='width:100%'/><input value=''  type='hidden' id='hformulText' readonly='true' /> </td></tr>";
		varHtml += "<td><input type='button' value='Finish' onclick='javascript:setFormula();'/></td></tr></table>";
		$("#preview").html(varHtml);

		setObjectAutoComplete($("#formulaname"), "calcFields");
/*
		var fieldCount = $(tmpObj).attr("id")
		if(fieldCount.indexOf("_") >0){
			fieldCount = fieldCount.substring(fieldCount.indexOf("_")+1 );
		}
		$("#currentFieldCount").val(fieldCount);
	*/	
	$("pccalc_"+$("#currentFieldCount").val()).val($("#fieldName").val());
		$("#formulaname").val($("#fieldName").val());
		$("#formulaText").val($("#formula").val());
		setValues();
	    
		showPopUp("open", "preview", "Define Field Calculation", "550", "450");
		
		$.uniform.restore('#inputs');
		$('#inputs').uniform();
	}

	function createFormula(obj){
		//alert("createFormula" + $(obj).text());
		try{
	    var tmpval = $("#formulaText").val();
	    
	    if(tmpval ==""){
	        tmpval = $("#formulaname").val() + "=";
	      
	    }
	    var tmpInputval = $("#inputs").val();
	    //alert(" tmpInputval 1" + tmpInputval);
	    
	   
	   
	    if(tmpInputval !=""){
	    	tmpval+= $("#inputs").val();
	    	// tmpInputval = tmpInputval.substring(tmpInputval.indexOf("->")+2);
	    	    //alert(" tmpInputval 2" + tmpInputval);
	    	    if(tmpInputval.indexOf("=")>0){
	    	        tmpInputval = tmpInputval.substring(0,(tmpInputval.indexOf("=")));
	    	    }
	    	  
	    }
	    //alert("tmphval " + tmphval);
	    if($(obj).text() != ""){
	    	tmpval+= $(obj).text();
	    	
	    }
	    
	    $("#formulaText").val(tmpval );
	   
	    $("#opr").val("");
	    $("#inputs").val("");
		}catch(err){
			alert("error " + err);
		}

	}

	function preview() {
		//alert("preview");
		var varHtml = "";
		try {
			varHtml += "<div  class='portlet' > <div class='portlet-header'>"
				+ "Title"
				+ "</div>";
			varHtml += "<div class='portlet-content '>";
			
								//alert(" widget " + i);
								//alert("html : " + $(widget).html());
								var columns = $("#dropDiv").find(".dropItem ").length;

								var value = "";
								var elements;
								var elementValue = "";
								//    alert("no of colums " + columns);

								var rows = 0;
								$("#dropDiv").find(".dropItem ").each(
										function(i, element) {
											var tmpRows = $(element).find("input").length;
											if (rows < tmpRows) {
												rows = tmpRows;
											}
										});

								console.log(rows + " / col " + columns)
								elements = new Array(rows);
								for (i = 0; i < rows; i++) {
									elements[i] = new Array(columns);
								}
								var row = -1;
								var col = -1;
								//BB var dropdownid = "";
								$("#dropDiv").find(".dropItem").each(function(i1, element) {
									 try{
									col =0;
									row += 1;
									// alert(i1 + "->" + $(element).html());
									 
									$(this).find("ul").each(function() {
										
										//  alert( " final " + i2+ " j " + j + " / " +elementValue );
										//elementValue = $(this).html();
										elementValue = $(this);
										// alert("before " + elementValue);
									/*	$(elementValue).find(".destroy").each(function(a1) {
											// alert(" a22222222 " + this);
											elementValue = elementValue.replace($(this).parent().html()," ");
											
										});
					     	        */
										//  elementValue= elementValue.replace('<a onclick="javascript:destroy(this);" href="#"><img class="destroy" width="16" height="16" src="images/icons/closebox.png"></a>',' ');
										// alert("after " + elementValue);
										 console.log(row + " / " +col + "/ " + elementValue);
										elements[col][row] = elementValue;
										//console.log( "after elements " );
										col += 1;
										//console.log( "end of inner loop" );
									});
									//console.log( "end of inner loop1" );
									 }catch(e1){
										 alert("Error "+ e1);
									 }
									//  alert("i2 " + i2 + " /j " + j+ " /ele " + elements);
								}); // end of .dropItem

									
								
									//alert("elements " + elements);
									varHtml += "<table cellspacing='2' cellpadding='2'>";
									var tdvalue = "";
									var tmpTd;
									var tmpId;
									var fieldCount;
									var fieldType;
									// col =2 , r1=3, r2 = 4
									// alert("element length " + elements.length  );
									for (row = 0; row < elements.length; row++) {
										//	alert("row " + row);
										//alert(" elements " + elements);
										var colValues = elements[row];
										varHtml += "<tr>";
										//  alert("colValues length " + colValues.length  );
										for (col = 0; col < colValues.length; col++) {
											console.log(row + ","+col);
											
											tdvalue = elements[row][col];
											console.log("tdvalue " + tdvalue);
											if (tdvalue == undefined) {
												varHtml += "<td colspan='2'>";
												tdvalue = "&nbsp;";
											}else{
												varHtml += "<td>";
												$(tdvalue).find("input").each(function(){
													console.log("inside text");
													tmpId = $(this).attr("id");
													tmpTd ="";
													if(tmpId.indexOf("_") >0){
														fieldCount = tmpId.substring(tmpId.indexOf("_")+1 );
													}
													if($(this).hasClass("pctext")){
														//?BB todo check box, radio, select, hidden
														
																
														tmpTd +="<label class='"+$("#labelPosition_"+fieldCount).val()+ "'>"+ $("#labelName_"+fieldCount).val() +"</label></td><td>";
														fieldType = $("#type_"+fieldCount).val();
														if (fieldType == "TEXT"){
															tmpTd +="<input id='" + $("#name_"+fieldCount).val()+"' placeholder='"+$("#name_"+fieldCount).val() +"' onkeyUp='executePreviewFormulas();' /> ";
														}else if(fieldType == "TEXTAREA"){
															tmpTd +="<textarea id='" + $("#name_"+fieldCount).val()+"' placeholder='"+$("#name_"+fieldCount).val() +"' onkeyUp='executePreviewFormulas();' ></textarea> ";
														}else if(fieldType == "PASSWORD"){
																tmpTd +="<input type='password' id='" + $("#name_"+fieldCount).val()+"' placeholder='"+$("#name_"+fieldCount).val() +"' onkeyUp='executePreviewFormulas();' /> ";
														}else if (fieldType == "DATE"){
															tmpTd +="<input class='cal' id='" + $("#name_"+fieldCount).val()+"' placeholder='"+$("#name_"+fieldCount).val() +"' onkeyUp='executePreviewFormulas();' /> ";
																	
														}else if(fieldType == "DROPDOWN"){
															var configId = $("#configId_"+fieldCount).val();
															tmpTd += "<select id='"+$("#name_"+fieldCount).val()+"' onchange='executePreviewFormulas();' ><option value=''>Select</option>"
															// var dropdownId = $("#calcdrop option:contains('"+value+"')").val();
															if (configId !="" && configId!="0" ){
																
																 jsonData="parentId="+configId;
																 response = jsonDataCall("getChildValues",jsonData);
																  for (var i = 0; i < response.childDetails.length; i++) { 
																	 tmpTd +="<option value='"+ response.childDetails[i].pkCodelst+"'>"+response.childDetails[i].description+"</option>";
																
																 }
																
																// elementValue = selectHtml;
															}
															 tmpTd += "</select>";
														}else if(fieldType == "CHECKBOX"){
															var configId = $("#configId_"+fieldCount).val();
															if (configId !="" && configId!="0" ){
																 jsonData="parentId="+configId;
																 response = jsonDataCall("getChildValues",jsonData);
																  for (var i = 0; i < response.childDetails.length; i++) { 
																	  tmpTd += "<input type='checkbox' name='"+$("#name_"+fieldCount).val()+"' id='"+response.childDetails[i].pkCodelst +"' value='"+response.childDetails[i].subType+"' />"+response.childDetails[i].description ;
																 }
															}
														}else if(fieldType == "RADIO"){
															var configId = $("#configId_"+fieldCount).val();
															if (configId !="" && configId!="0" ){
																 jsonData="parentId="+configId;
																 response = jsonDataCall("getChildValues",jsonData);
																  for (var i = 0; i < response.childDetails.length; i++) { 
																	  tmpTd += "<input type='radio' name='"+$("#name_"+fieldCount).val()+"' id='"+response.childDetails[i].pkCodelst +"' value='"+response.childDetails[i].subType+"' />"+response.childDetails[i].description ;
																 }
															}
														}
													
													}else if($(this).hasClass("pccalc")){
														tmpTd +="<label class='"+$("#labelPosition_"+fieldCount).val()+ "'>"+ $("#labelName_"+fieldCount).val() +"</label></td><td>";
														tmpTd +="<input id='" + $("#name_"+fieldCount).val()+"' placeholder='"+$("#name_"+fieldCount).val() +"' onkeyUp='executePreviewFormulas();' /> ";
														tmpTd +="<input class='formula' type='hidden' id='" + $("#name_"+fieldCount).val()+"_formula' placeholder='"+$("#name_"+fieldCount).val() +"' onkeyUp='executePreviewFormulas();' value='"+ $("#formula_"+fieldCount).val()+"' /> ";
														
													}if($(this).hasClass("pclabel")){
														tmpTd +="<label class='"+$("#labelPosition_"+fieldCount).val()+ "'>"+ $("#labelName_"+fieldCount).val() +"</label></td><td>&nbsp;</td>";
														
													}
												});
												tdvalue = tmpTd;
											}
											console.log("tdvalue " + tdvalue);
											varHtml += tdvalue + "</td>";
										}
										varHtml += "</tr>";
									}
									varHtml += "</table>";
									varHtml += "</div></div>";
								
			//alert(" varHtml " + varHtml);
			$("#preview").html(varHtml);
			$.uniform.restore('select');
			$('select').uniform();
			$("#preview")
					.find(".portlet")
					.addClass(
							"ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
					.find(".portlet-header").addClass(
							"ui-widget-header ui-corner-all").prepend(
							"<span class='ui-icon ui-icon-minusthick'></span>");
			$("#preview").find(".portlet-header .ui-icon").click(
					function() {
						$(this).toggleClass("ui-icon-minusthick").toggleClass(
								"ui-icon-plusthick");
						$(this).parents(".portlet:first").find(
								".portlet-content").toggle();
					});
			// alert("show popup");
			//function showPopUp(mode,Id,title,wd,ht){
			showPopUp("open", "preview", "Preview", "850", "350")
			//alert("end ");
		} catch (err) {
			alert("error " + err);
		}
		
		
	}
</script>

<div id="slidecolumnfilter" style="display: none">
	<br> <br>
	<div class="portlet">
		<div class="portlet-header">
			Widget <input type="text" id="Columnfilter"
				class="inputboxSearch maintenance_slidewidget"
				onclick="maintColumnfilter('widgetFacet')">
		</div>
		<div class="cleaner"></div>

		<div class="portlet-content">
			<div style="height: 320px; overflow-y: auto;">
				<table width="100%" border="0" id="widgetFacet">
					<s:iterator value="widgetList" var="rowVal" status="row">
						<tr>
							<td><a href="#"
								onclick="javascript:getWidgetDetails( '<s:property value="widgetLib"/>','<s:property value="name"/>','<s:property value="module"/>',this)"><s:property
										value="name" /> </a></td>
							<td><img src="images/icons/folder.png" class="cursor"
								onclick="copy_Widget('<s:property value="widgetLib"/>','<s:property value="name"/>','<s:property value="module"/>','<s:property value="description"/>')" />
							</td>

						</tr>
					</s:iterator>
				</table>
			</div>
		</div>
	</div>
</div>

<!--float window end-->

<!--right window start -->
<span class='hidden'> <s:select name="codeListModule"
		id="codeListModule" list="moduleList" headerKey=""
		headerValue="Select" listKey="subType" listValue="description" /> 
		
		</span>
<!-- <div id="forfloat_right"> -->
<div>
	<span id="breadcrumb">Widget Library </span>
</div>
<br>
<div class="cleaner"></div>

<!-- Protocol Portlet Start -->

<div class="portlet" id="widgetlist">
	<div class="portlet-header ">Widget Library</div>

	<div class="portlet-content ">
		<table>
			<tr>
				<td style="width: 20%"><div
						onclick="addRows(0,'','','',0,true);">
						<img src="images/icons/addnew.jpg" class="cursor" />&nbsp;&nbsp;<label
							id="adchild" class="cursor"><b>Add</b> </label>&nbsp;
					</div></td>
				<td style="width: 20%"><div>
						<img src="images/icons/no.png" style="width: 16; height: 16;"
							class="cursor" id="" onclick="deleteRows();" />&nbsp;&nbsp;<label
							class="cursor" onclick="deleteRows();"><b>Delete</b> </label>&nbsp;
					</div></td>
				<td style="width: 20%"><div>
						<img src="images/icons/edit.png" style="width: 16; height: 16;"
							class="cursor" id="" onclick="editRows();" />&nbsp;&nbsp;<label
							id="dchild" class="cursor" onclick="editRows();"><b>Edit</b>
						</label>&nbsp;
					</div></td>
			</tr>
		</table>



		<table id="widgetTable" cellpadding="0" cellspacing="0" border="0"
			class="display">
			<thead>
				<tr>
					<th width="4%" id="widgetsColumn"></th>
					<th><s:text name="stafa.maintenance.label.name" /></th>
					<th><s:text name="stafa.label.code" /></th>
					<th><s:text name="stafa.label.module" /></th>
					<th><s:text name="stafa.maintenance.label.comments" /></th>
					<th><s:text name="stafa.label.activated" /></th>
				</tr>
			</thead>
		</table>


	</div>
</div>
<!--  end of protocol portlet -->




<!--  calculation -->


<div class="portlet hidden" id="widgetCalc">
	<div class="portlet-header">Calculations</div>
	<div class="portlet-content">
		<div style="min-height: 75px;">
			<div>
				<span style="display: none"> <select id="calcLabel"></select>
					<select id="calcFields" ><option value="">Select</option></select> 
					<input type="hidden" id="widgetRef" name="widgetRef" value="" />
					</span>

				<ul>
					<form onsubmit="return avoidFormSubmitting()">
						<span> Number of Column(s) <input type="text" name="noc"
							id="noc" style="border: 1px solid black; width: 50px;" />
							&nbsp;&nbsp;<input type="button" value="Create"
							onclick="javascript:createColumns();"> </span> <span> <input
							type="button" value="Preview" onclick="javascript:preview();" />
						</span>
					</form>
				</ul>
			</div>
			<div>
				<div class="dragItem ">
					<ul class="ui-state-default ulpadding">
						<input type="text" placeholder="Label" class="pclabel"
							id="pclabel" />
					</ul>
					<ul class="ui-state-default ulpadding">
						<input type="text" placeholder="Field" class="pctext" id="pctext" />
					</ul>
					<ul class="ui-state-default ulpadding">
						<input type="text" placeholder="Calculation" class="pccalc"
							id="pccalc" />
					</ul>
				</div>

				<div id="dropDiv"></div>

				<div id="properties" class="properties">
					<fieldset id="fieldProperties">
						<legend>Field Settings</legend>
						<table>
							<tr>
								<td><label>Name :</label></td>
								<td><input type="hidden" name="currentFieldCount" id="currentFieldCount" />
									<input type="hidden" name="fieldId" id="fieldId" />
									<input name="fieldName" id="fieldName" readonly="readonly"/></td>
							</tr>
							<tr>
								<td><label>Type :</label></td>
								<td><select id="fieldType" name="fieldType">
										<option value="">Select</option>
										<option value="TEXT">Text</option>
										<option value="DROPDOWN">Dropdown</option>
										<option value="TEXTAREA">Textarea</option>
										<option value="PASSWORD">Password</option>
										<option value="CALC">Calculation</option>
										<option value="LABEL">Label</option>
										
										<option value="DATE">Date</option>
										<!-- <option value="HIDDEN">Hidden</option>
										 -->
										<option value="CHECKBOX">Checkbox</option>
										<option value="RADIO">Radio</option>
								</select>
								</td>
							</tr>
							<tr>
								<td><label>Label Name :</label></td>
								<td><input name="labelName" id="labelName" /></td>
							</tr>
							<tr class="hidden">
								<td><label>Label Position :</label></td>
								<td><select name="labelPosition" id="labelPosition">
										<option value="inline">Inline</option>
										<option value="top">Top</option>
								</select></td>
							</tr>
							<tr class="hidden">
								<td><label>Masking :</label></td>
								<td><select class="txtMask" name="maskingChange" id="maskingchange" tabindex=9>
										<option value="select">Select Masking</option>
										<option value="phoneno">Phone Number</option>
										<option value="phonenoext">Phone Number + Ext</option>
										<option value="taxid">Tax Id</option>
										<option value="ssn">SSN</option>
										<option value="productkey">Product Key</option>
								</select>
								</td>
							</tr>
							<tr>
								<td><label>Required :</label></td>
								<td><input type="radio" value="yes" name="required" id="required_yes" onclick="setValues()"/> Yes
									<input type="radio" value="no" name="required" id="required_no" onclick="setValues()" /> No</td>
							</tr>
							<tr id="formulatr" class="hidden">
								<td>Formula</td><td><input type="text" id="formula" name="formula" /></td>
							</tr>
						</table>

					</fieldset>
					<div style="vissible: hidden" id="fieldDetails" name="fieldDetails" ></div>
				</div>
			</div>
		</div>
	</div>
</div>




<!--  right div -->
<!-- </div> -->



