<%@ include file="common/includes.jsp" %>
 <script type="text/javascript" src="js/util.js"></script>
 <script type="text/javascript" src="js/jquery/jquery.Print.js"></script>
<script type="text/javascript" src="js/jquery/jquery.printElement.js"></script>
 
<!-- <script type="text/javascript" src="js/stafaQC.js"></script> 
 --><link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script>
var timeid=0;
function setConditionMandatory(){
    setTempMandatory("bloodWarmerUsed");
    setTempMandatory("rbcPrime");
}

function setTempMandatory(objId){
	
  //alert("11"+$("#"+objId+"1").is(":checked"));
	//if($("#bloodWarmerUsed1").is(":checked")){
	if($("#"+objId+"1").is(":checked")){	
		addValidation(true,objId);
	}else{
		addValidation(false,objId);
	}
}
function addValidation(flag,objId){
	if(flag){
		//alert("12341234"+$("#bloodWarmerStartingTemp").hasClass("Validate_required"));
		if(objId=="bloodWarmerUsed"){
			if(!$("#bloodWarmerStartingTemp").hasClass("validate_required")){
				$("#bloodWarmerStartingTemp").addClass("validate_required condtionMandatory");
				//$(".condtionMandatory").after("<sup style='margin-left:2px'><label class='mandatory'>*</label></sup>");
				$("#bloodWarmerStartingTemp").after("<sup style='margin-left:2px'><label class='mandatory'>*</label></sup>");
				// bind event 
				$("#bloodWarmerStartingTemp").bind("keyup change",function(){
					//alert("key up");
					//alert("length" + this.value.length );
						if( this.value.length>0){
							hideErrorMessage(this);
						}else{
							showErrorMessage(this,messageArray["bloodWarmerStartingTemp"]);
						}
					});
			}
		}else if(objId== "rbcPrime"){
			if(!$("#rbcUnitComponentNumber").hasClass("validate_required")){
				$("#rbcUnitComponentNumber").addClass("validate_required condtionMandatory");
				$("#rbcUnitComponentNumber").after("<sup style='margin-left:2px'><label class='mandatory'>*</label></sup>");
				// bind event 
				$("#rbcUnitComponentNumber").bind("keyup change",function(){
					//alert("key up");
					//alert("length" + this.value.length );
						if( this.value.length>0){
							hideErrorMessage(this);
						}else{
							showErrorMessage(this,messageArray["rbcUnitComponentNumber"]);
						}
					});
			}
		}
	}else{
		if(objId=="bloodWarmerUsed"){
			$("#bloodWarmerStartingTemp").removeClass("validate_required"); 
			$("#bloodWarmerStartingTemp").removeClass("validate_required condtionMandatory errorBorder");
			$("#bloodWarmerStartingTemp").parent().find('sup').remove();
			$("#bloodWarmerStartingTemp").parent().find('.formError').remove();
			$("#bloodWarmerStartingTemp").unbind("keyup change");
		}else if(objId=="rbcPrime"){
			$("#rbcUnitComponentNumber").removeClass("validate_required"); 
			$("#rbcUnitComponentNumber").removeClass("validate_required condtionMandatory errorBorder");
			$("#rbcUnitComponentNumber").parent().find('sup').remove();
			$("#rbcUnitComponentNumber").parent().find('.formError').remove();
			$("#rbcUnitComponentNumber").unbind("keyup change");
		}
	}
} 
function checkPlasma(){
	if($("#collectPlasma1").is(":checked")){
		setValidation(true);
	}else{
		setValidation(false);
	}
}
function setValidation(flag){
	//alert("1");
	if(flag){
		if($("#plasmaId").hasClass("condtionMandatory")){
			$("#plasmaId").addClass("validate_required condtionMandatory");
		}
		if($("#plasmaCollectionDate").hasClass("condtionMandatory")){
			$("#plasmaCollectionDate").addClass("validate_required condtionMandatory");
		}
		if($("#plasmaLabelVerified1").hasClass("condtionMandatory")){
			$("#plasmaLabelVerified1").addClass("validate_required condtionMandatory");
		}
		if($("#plasmaLabelVerified0").hasClass("condtionMandatory")){
			$("#plasmaLabelVerified0").addClass("validate_required condtionMandatory");
		}
		if($("#tmpplasmaStartTime").hasClass("condtionMandatory")){
			$("#tmpplasmaStartTime").addClass("validate_required condtionMandatory");
		}
		if($("#plasmaToCollect").hasClass("condtionMandatory")){
			$("#plasmaToCollect").addClass("validate_required condtionMandatory");
		}
		if($("#notifiedPhysician1").hasClass("condtionMandatory")){
			$("#notifiedPhysician1").addClass("validate_required condtionMandatory");
		}
		if($("#notifiedPhysician0").hasClass("condtionMandatory")){
			$("#notifiedPhysician0").addClass("validate_required condtionMandatory");
		}
		$(".condtionMandatory").after("<sup style='margin-left:2px'><label class='mandatory'>*</label></sup>");
		var param ="{module:'APHERESIS',page:'COLLECTION'}";
		markValidation(param);
	}else{
		$("#plasmaId").parent().find('sup').remove();
		$("#plasmaId").removeClass("validate_required condtionMandatory errorBorder");
		$("#plasmaCollectionDate").parent().find('sup').remove();
		$("#plasmaCollectionDate").removeClass("validate_required condtionMandatory errorBorder");
		$("#plasmaLabelVerified1").parent().find('sup').remove();
		$("#plasmaLabelVerified1").removeClass("validate_required condtionMandatory");
		$("#plasmaLabelVerified0").parent().find('sup').remove();
		$("#plasmaLabelVerified0").removeClass("validate_required condtionMandatory");
		$("#tmpplasmaStartTime").parent().find('sup').remove();
		$("#tmpplasmaStartTime").removeClass("validate_required condtionMandatory errorBorder");
		$("#plasmaToCollect").parent().find('sup').remove();
		$("#plasmaToCollect").removeClass("validate_required condtionMandatory");
		$("#notifiedPhysician1").parent().find('sup').remove();
		$("#notifiedPhysician1").removeClass("validate_required condtionMandatory");
		$("#notifiedPhysician0").parent().find('sup').remove();
		$("#notifiedPhysician0").removeClass("validate_required condtionMandatory");
	}
	
	
}

function savePages(){
	
	if(checkModuleRights(STAFA_COLLECTION,APPMODE_SAVE)){
		try{
			if(saveCollection()){
				//alert("wait");
				return true;
				}
			}catch(e){
				alert("exception " + e);
				}
		}
}
var donormrn;
var donorplannedprocedure;
var donorVal;
//var entityType="";
$(document).ready(function(){
	try{
	var donor=$("#donor").val();
	var donorProcedure=$("#donorProcedure").val();
	donorplannedprocedure=$("#donorProcedure").val();
	donorVal = $("#donor").val();
	var endityId = donorProcedure+","+donor;
	showTracker(endityId,"donor","apheresis_nurse","false");
	showNotesCount(donor,"donor",0);

    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-plusthick'></span>").end()
    .find(".portlet-content").hide();
    showportlet();
   
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-plusthick" ).toggleClass( "ui-icon-minusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });
    $('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});   
    
	$(".titleText").html("<s:text name='stafa.label.collection'/>");
	show_innernorth();
	getslide_widgetdetails();
	loadCollectionData();
	 setplasmaid();
	donormrn="";
	donormrn=$("#slide_donorId").text();
	 $("input[name='productLabelVerified']").click(function(){
	     	var checkradio=$("input[name='productLabelVerified']:checked").val();
	    	 if(checkradio!=1){
	    		 $("#productverification_view").hide();
	    	 }
	     	else {
	     		//entityType = "COLLECTION"; 
	     		addNewPopup("Label Verification","labelverified","loadLabelVerf","700","1050");
	    		 $("#productverification_view").show();
	    	 }
	     });
	    
	    $('#productverification_view').click(function (e) {
	    	//entityType = "COLLECTION"; 
			 addNewPopup("Label Verification","labelverified","loadLabelVerf","700","1050");

	    	 
	    });
	    $("input[name='plasmaLabelVerified']").click(function(){
	     	var checkradio=$("input[name='plasmaLabelVerified']:checked").val();
	    	 if(checkradio!=1){
	    		 $("#plasmaverification_view").hide();
	    	 }
	     	else {
	     		//entityType = "COLLECTION"; 
	     		addNewPopup("Plasma Label Verification","plasmaLabelVerification","plasmaLoadLabelVerf","700","1050");
	    		 $("#plasmaverification_view").show();
	    	 }
	     });
	    $('#plasmaverification_view').click(function (e) {
	    	//entityType = "COLLECTION"; 
			 addNewPopup("Plasma Label Verification","plasmaLabelVerification","plasmaLoadLabelVerf","700","1050");
	    	//showPopUp("open","plasmaLabelVerification","Label Verfication","850","520");

	    	 
	    });
	//alert("proid"+plasmaid);
	constructProcessingParameterTable(false);
	//alert("ready");
	var param ="{module:'APHERESIS',page:'COLLECTION'}";
	markValidation(param);
	
	}catch(err){
		alert("Error *********** " + err);
	}
	
});
var donor=$("#donor").val();
function setplasmaid(){
	
	var proid=$("#specimen").val();
	var plasmaid=proid+"-P";
	if($("#collectPlasma1").is(":checked")){
		$("#plasmalabel").find( ".portlet-header .ui-icon" ).removeClass( "ui-icon-plusthick" ).addClass( "ui-icon-minusthick" ).end().find(".portlet-content").show();
		$("#plasmaId").val(plasmaid);
		checkPlasma();
	}else{
		$("#plasmalabel").find( ".portlet-header .ui-icon" ).removeClass( "ui-icon-minusthick" ).addClass( "ui-icon-plusthick" ).end().find(".portlet-content").hide();
		$("#plasmaId").val("");
		$("#plasmaCollectionDate").val("");
		$("#plasmaLabelVerified1").attr("checked",false);
		$("#plasmaLabelVerified0").attr("checked",false);
		$("#tmpplasmaStartTime").val("");
	
		checkPlasma();
		//alert("111");
		
	} 
	
}
function deleteprocessingData(){
	// alert("Delete Rows");
	if(checkModuleRights(STAFA_COLLECTION,APPMODE_DELETE)){
    var yes=confirm(confirm_deleteMsg);
	if(!yes){
		return;
	}
	var deletedIds = ""
	$("#processingParameterTable ").find("#processingParameter:checked").each(function (row){
		deletedIds += $(this).val() + ",";
	});
	if(deletedIds.length >0){
	  deletedIds = deletedIds.substr(0,deletedIds.length-1);
	}
	jsonData = "{deletedProcessingResult:'"+deletedIds+"'}";
	url="deleteProcessingData";
    response = jsonDataCall(url,"jsonData="+jsonData);
    constructProcessingParameterTable(true);
    processingparameter();
	}
}
 function loadCollectionData(){
	
	
	var url = "loadCollectionData";
	var donor=$("#donor").val();
	var donorProcedure=$("#donorProcedure").val();
	if(donor !=0 && donor != ""){
		 var jsonData = "donor:"+donor+",donorProcedure:"+donorProcedure;
		response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	}
	
	
	$(response.collectionlist).each(function(i){
	//alert("cc"+this.plasmaLabelVerified);
	if(this.collectionDate!= null && this.collectionDate!= "" ){
		var collectiondate=converHDate(this.collectionDate);
	}
		//alert("coll"+this.aphCollection);
		$("#aphCollection").val(this.aphCollection);
		$("#collectionDate").val(collectiondate);
		$("#bloodWarmerStartingTemp").val(this.bloodWarmerStartingTemp);
		$("#rbcUnitComponentNumber").val(this.rbcUnitComponentNumber);
		$("#totalvolume").val(this.totalvolume);
		$("#precollectionACD").val(this.precollectionACD);
		$("#precollectionInlet").val(this.precollectionInlet);
		$("#precollectionPlasma").val(this.precollectionPlasma);
		$("#precollectionReplace").val(this.precollectionReplace);
		$("#precollectionWBACDA").val(this.precollectionWBACDA);
		$("#precollectionRPM").val(this.precollectionRPM);
		$("#collectionAc").val(this.collectionAc);
		$("#processingInlet").val(this.processingInlet);
		$("#proplasmaVolume").val(this.proplasmaVolume);
		$("#proproductVolume").val(this.proproductVolume);
		
		$("#apheresisInput").val(this.apheresisInput);
		$("#apheresisOutput").val(this.apheresisOutput);
		$("#midCountWbc").val(this.midCountWbc);
		$("#midCountLitersCollected").val(this.midCountLitersCollected);
		$("#plasmaToCollect").val(this.plasmaToCollect);
		$("#plasmaId").val(this.plasmaId);
		if(this.bloodWarmerUsed==1){
			$("#bloodWarmerUsed1").prop("checked",true);
		}else if(this.bloodWarmerUsed==0){
			$("#bloodWarmerUsed0").prop("checked",true);
		}
		if(this.rbcPrime==1){
			$("#rbcPrime1").prop("checked",true);
		}else if(this.rbcPrime==0){
			$("#rbcPrime0").prop("checked",true);
		}
		if(this.productLabelVerified==1){
			$("#productLabelVerified1").prop("checked",true);
			$("#productverification_view").show();
		}else if(this.productLabelVerified==0){
			$("#productLabelVerified0").prop("checked",true);
			$("#productverification_view").hide();
		}
		if(this.bloodWarmerStartingTemp==1){
			$("#bloodWarmerStartingTemp1").prop("checked",true);
		}else if(this.bloodWarmerStartingTemp==0){
			$("#bloodWarmerStartingTemp0").prop("checked",true);
		}
		if(this.collectPlasma==1){
			$("#collectPlasma1").prop("checked",true);
		}else if(this.collectPlasma==0){
			$("#collectPlasma0").trigger("click");
 			$("#collectPlasma0").addClass("on");
 			
			$("#plasmalabel").find( ".portlet-header .ui-icon" ).removeClass( "ui-icon-minusthick" ).addClass( "ui-icon-plusthick" ).end().find(".portlet-content").hide();
			//$("#collectPlasma0").prop("checked",true);
			
		}
		if(this.notifiedPhysician==1){
			$("#notifiedPhysician1").prop("checked",true);
		}else if(this.notifiedPhysician==0){
			$("#notifiedPhysician0").prop("checked",true);
		}
		//alert("cc1"+this.plasmaLabelVerified);
		
		if(this.plasmaLabelVerified==1){
			//alert("2");
			$("#plasmaLabelVerified1").prop("checked",true);
			 $("#plasmaverification_view").show();
		}else if(this.plasmaLabelVerified==0){
			//alert("3");
			$("#plasmaLabelVerified0").prop("checked",true);
			 $("#plasmaverification_view").hide();
		}else{
			//alert("1");
			$("#plasmaLabelVerified1").prop("checked",false);
			$("#plasmaLabelVerified0").prop("checked",false);
		}
		$("#refAccess").val(this.refAccess);
		$("#refReturn").val(this.refReturn);
		$("#refWithdraw").val(this.refWithdraw);
		if(this.plasmaCollectionDate!= null && this.plasmaCollectionDate!= ""){
		var plasmaCollectiondate=converHDate(this.plasmaCollectionDate);
		}
		$("#plasmaCollectionDate").val(plasmaCollectiondate);

	 if(this.collectionEndTime!=null && this.collectionEndTime!=""){
			var tmpDate = new Date(this.collectionEndTime);
			if(tmpDate != null && tmpDate != "" && $.browser.msie){
				var tmpDate1 = this.collectionEndTime.split("-");
				//alert("tmpDate " + tmpDate1 +" / len----- " + tmpDate1.length + " , " + tmpDate1[2].substring(0,2) + " / " + tmpDate1[2].substring(3));
				var tmpTime = tmpDate1[2].substring(3,8);
				$("#tmpcollectionEndTime").val(tmpTime);
				// tmpDate = new Date(tmpDate1[0] ,tmpDate1[1], tmpDate1[2].substring(0,2) , tmpTime.substring(0,2) , );
			}else{
				
				collEndTime = (tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes();
				$("#tmpcollectionEndTime").val(collEndTime);
			}

		}else{
			$("#tmpcollectionEndTime").val("");
		}
	    setCollectionEndTime();
		if(this.collectionStartTime!=null && this.collectionStartTime!=""){
			var tmpDate = new Date(this.collectionStartTime);
			if(tmpDate != null && tmpDate != "" && $.browser.msie){
				var tmpDate1 = this.collectionStartTime.split("-");
				//alert("tmpDate " + tmpDate1 +" / len----- " + tmpDate1.length + " , " + tmpDate1[2].substring(0,2) + " / " + tmpDate1[2].substring(3));
				var tmpTime = tmpDate1[2].substring(3,8);
				$("#tmpcollectionStartTime").val(tmpTime);
				
				// tmpDate = new Date(tmpDate1[0] ,tmpDate1[1], tmpDate1[2].substring(0,2) , tmpTime.substring(0,2) , );
			}else{
				collStartTime = (tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes();
				$("#tmpcollectionStartTime").val(collStartTime);
			}

		}else{
			$("#tmpcollectionStartTime").val("");
		}
		setCollectionStartTime();
		if(this.plasmaCollectionStartTime!=null && this.plasmaCollectionStartTime!=""){
			var tmpDate = new Date(this.plasmaCollectionStartTime);
			if(tmpDate != null && tmpDate != "" && $.browser.msie){
				var tmpDate1 = this.plasmaCollectionStartTime.split("-");
				//alert("tmpDate " + tmpDate1 +" / len----- " + tmpDate1.length + " , " + tmpDate1[2].substring(0,2) + " / " + tmpDate1[2].substring(3));
				var tmpTime = tmpDate1[2].substring(3,8);
				$("#tmpplasmaStartTime").val(tmpTime);
				// tmpDate = new Date(tmpDate1[0] ,tmpDate1[1], tmpDate1[2].substring(0,2) , tmpTime.substring(0,2) , );
			}else{
				plasmaStartTime = (tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes();
				$("#tmpplasmaStartTime").val(plasmaStartTime);
			}
		}else{
			$("#tmpplasmaStartTime").val("");
		}
		setplasmaStartTime();
	});
} 

function getslide_widgetdetails(){
	
	 var donor=$("#donor").val();
	 var donorProcedure = $("#donorProcedure").val();
	 url="fetchDonorData";
	 var jsonData = "donor:"+donor+",donorProcedure:"+donorProcedure;
	 response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	 
	 var donorDetails = response.donorDetails;
	
	$("#slide_donorId").text(donorDetails.personMRN);
	$("#slide_donorDOB").text(donorDetails.dob);
	$("#slide_donorName").text(donorDetails.lastName + " " + donorDetails.firstName);
	$("#slide_procedure").text(donorDetails.plannedProceduredesc);
	var tcCheck = donorDetails.plannedProceduredesc.slice(-2);
 	if(tcCheck == "TC"){
 		$(".cd34").hide();
 		
 	}else{
 		$(".cd3").hide();
 	}
	$("#slide_donorWeight").text(donorDetails.personWeight);
	$("#slide_donorABORH").text(donorDetails.bloodGrpName);
	$("#slide_targetVol").text(donorDetails.targetProcessingVolume);
	$("#slide_donation").text(donorDetails.donorTypeName);
	$("#recipient_id").text(donorDetails.recipient_id);
	$("#recipient_name").text(donorDetails.recipient_name);
	$("#recipient_dob").text(donorDetails.recipient_dob);
	$("#recipient_aborh").text(donorDetails.recipient_bloodGroup);
	$("#recipient_diagnosis").text(donorDetails.diagonsis);
	$("#idms").text(donorDetails.idmsResult);
	var idmCheck = donorDetails.idmsResult;
	if(idmCheck=="Reactive"){
		$("#idms").addClass('idmReactive');
	}else if(idmCheck=="Pending"){
		$("#idms").addClass('idmPending');
	}
	$("#westnile").text(donorDetails.westNileDesc);
	$("#donorcd34").text(donorDetails.donorCD34);
	$("#cd34_day1").text(donorDetails.productCD34);
	$("#cum_CD34").text(donorDetails.productCUMCD34);
	$("#recipient_weight").text(donorDetails.recipient_weight);
	$("#slide_donorHeight").text(donorDetails.personHeight);
}

 function setCollectionStartTime(){
	// alert("test");
	 $('#collectionStartTime').val($('#collectionDate').val() +" "+ $('#tmpcollectionStartTime').val())
 }
 function setplasmaStartTime(){
		// alert("test");
		 $('#plasmaCollectionStartTime').val($('#plasmaCollectionDate').val() +" "+ $('#tmpplasmaStartTime').val())
	 }
  function setprocessingtime(obj){
	//	alert("obj " + obj);
		var  rowHtml =  $(obj).closest("tr");
	//	alert("row html " + $(rowHtml).html());
		$(rowHtml).find('#time').val($('#collectionDate').val() +" "+ $(rowHtml).find('input[name="tempprocessingTime"]').val());
		//?BB var timee= $('#time').val($('#collectionDate').val() +" "+ $('#temptime').val());
	 }
  function setCollectionEndTime(){
			$('#collectionEndTime').val($('#collectionDate').val() +" "+ $('#tmpcollectionEndTime').val());
	 } 
  
  
  function constructProcessingParameterTable(flag){
		 var processingCriteria = "";
		 var donor=$("#donor").val();
		 var donorProcedure=$("#donorProcedure").val();
			if(donor!=null && donor!=""){
				processingCriteria = " and pp.FK_DONOR ="+donor+" and pp.fk_plannedProcedure ="+donorProcedure;
			}
			
			var processingServerParam = function ( aoData ) {
				aoData.push( { "name": "application", "value": "stafa"});
				aoData.push( { "name": "module", "value": "collection_processing_parameter"});	
				aoData.push( { "name": "criteria", "value": processingCriteria});
				
				aoData.push( {"name": "col_1_name", "value": "" } );
				aoData.push( { "name": "col_1_column", "value": ""});
				
				aoData.push( { "name": "col_2_name", "value": ""});
				aoData.push( { "name": "col_2_column", "value": ""});
				
				aoData.push( {"name": "col_3_name", "value": "" } );
				aoData.push( { "name": "col_3_column", "value": ""});
				
				aoData.push( {"name": "col_4_name", "value": "" } );
				aoData.push( { "name": "col_4_column", "value": ""});
				
				aoData.push( {"name": "col_5_name", "value": "" } );
				aoData.push( { "name": "col_5_column", "value": ""});
				
				aoData.push( {"name": "col_6_name", "value": "" } );
				aoData.push( { "name": "col_6_column", "value": ""});
				
				aoData.push( { "name": "col_7_name", "value": ""});
				aoData.push( { "name": "col_7_column", "value": ""});
				
				aoData.push( {"name": "col_8_name", "value": "" } );
				aoData.push( { "name": "col_8_column", "value": ""});
				
				aoData.push( {"name": "col_9_name", "value": "" } );
				aoData.push( { "name": "col_9_column", "value": ""});
				
				aoData.push( {"name": "col_10_name", "value": "" } );
				aoData.push( { "name": "col_10_column", "value": ""});
				
			};
			var processingColDef = [
								 {
										"aTargets": [0], "mDataProp": function ( source, type, val ) { 
										    return "<input type='checkbox' id='processingParameter' name='processingParameter' value='"+source[0]+"'>";
								     }},
				                  {"sTitle":"Time",
										"aTargets": [1], "mDataProp": function ( source, type, val ) {
											var link ="<input type='hidden' name='processingTime' id='time' value='"+source[3]+ "'/>"+source[2];
											return link;
				                	 }},
								  {	"sTitle":"BP (mmHg)",
				                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
				                			 var concat=source[4]+","+source[13]
				                			 return concat;
				                	  }},
								  {	"sTitle":"HR (bpm)",
										  "aTargets": [3], "mDataProp": function ( source, type, val ) {
					                		 return source[5];
								  	  }},
								  {	"sTitle":"Temp (C)",
									  	  "aTargets": [4], "mDataProp": function ( source, type, val ) {
					                		 return source[6];
							  		  }},
								  {	"sTitle":"AC",
								  		  "aTargets": [5], "mDataProp": function ( source, type, val ) {
								  				var dataHtml="<input type='hidden' name='ac1' id='ac1' class='acclass' value='"+source[7]+ "' />"+source[7];
					                		 return dataHtml;
									  }},
									 
									  {	"sTitle":"Inlet",
								  		  "aTargets": [6], "mDataProp": function ( source, type, val ) {
								  			var dataHtml="<input type='hidden' name='it' id='it' class='inletclass' value='"+source[8]+ "' />"+source[8];
					                		 return dataHtml;
									  }},
									  {	"sTitle":"Plasma Volume (mL)",
								  		  "aTargets": [7], "mDataProp": function ( source, type, val ) {
								  			var dataHtml="<input type='hidden' name='plv' id='plv' class='plasmaclass' value='"+source[9]+ "' />"+source[9];
					                		 return dataHtml;
									  }},
									  {	"sTitle":"Product Volume (mL)",
								  		  "aTargets": [8], "mDataProp": function ( source, type, val ) {
								  			var dataHtml="<input type='hidden' name='prv' id='prv' class='productvolumeclass' value='"+source[10]+ "' />"+source[10];
					                		 return dataHtml;
								  			  
									  }},
									  
									  {	"sTitle":"Comments",
								  		  "aTargets": [9], "mDataProp": function ( source, type, val ) {
								  			  link = "<input type='hidden' name='tmpComment' id='tmpComment' value='"+source[12]+ "' />"+source[11];
					                		 return link;
									  }}
									  
								];
			var processingColManager = function () {
		        $("#parameterColumn").html($('#processingParameterTable_wrapper .ColVis'));
			};
			var processingCol = [ { "bSortable":false},
					               null,
					               null,
					               null,
					               null,
					               { "sClass": "acid", "aTargets": [5] },
					               { "sClass": "inletid", "aTargets": [6] },
					               { "sClass": "plasmaid", "aTargets": [6] },
					               { "sClass": "productvolumeid", "aTargets": [6] },
					               null
					              ];
			constructTable(flag,'processingParameterTable',processingColManager,processingServerParam,processingColDef,processingCol);

	}
  
$(document).ready(function(){
	
	show_slidewidgets("nurse_slidewidget",false);
	show_slidecontrol("slidecontrol");
	$('#inner_center').scrollTop(0);
	//document.getElementById('inner_center').scrollTop = document.documentElement.scrollTop = 0;
	
    $('#tmpplasmaStartTime').timepicker({});
    $('#tmpcollectionStartTime').timepicker({});
    $('#tmpcollectionEndTime').timepicker({});
    processingparameter();
  
	 var today = new Date();
	    $( "#collectionDate" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	        changeYear: true, yearRange: "c-50:c+45"});
	    var d = today.getDate();
	    var m = today.getMonth();
	    var y = today.getFullYear();

	    var h=today.getHours();
	    var mn=today.getMinutes()+1;
	    $( "#plasmaCollectionDate").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	        changeYear: true, yearRange: "c-50:c+45"});
	    checkPlasma();
	 
	  /*//?BB  setTempMandatory("bloodWarmerUsed");
	    setTempMandatory("rbcPrime");
	   */ setConditionMandatory();
	  
});

function editProcessingRows(){
	//  alert("edit Rows");
	if(checkModuleRights(STAFA_COLLECTION,APPMODE_EDIT)){
		  $("#processingParameterTable tbody tr").each(function (row){
			 if($(this).find("#processingParameter").is(":checked")){
				 $(this).find("td").each(function (col){
					 if(col ==1 ){
						 if ($(this).find("#temptime").val()== undefined){
							 var tmpHtml = "<input type='hidden' name='processingTime' id='time' value='"+$(this).find('#time').val()+ "'/><input type='text' class = 'temptime' name='tempprocessingTime' id='temptime' onchange='setprocessingtime(this);' value='"+$(this).text()+ "'/>";
							//$(this).html("<input type='hidden' name='processingTime' id='time'/><input type='text' class = 'temptime' name='tempprocessingTime' id='temptime' onchange='setprocessingtime(this);' value='"+$(this).text()+ "'/>");
							$(this).html(tmpHtml);
							$('.temptime').timepicker({});
						 }
					 }else if(col ==2){
						 if ($(this).find("#bp").val()== undefined ){
							var dataValue=$(this).text();
							 var tmpDate = dataValue.split(",");
							 if(tmpDate!=null && tmpDate!=""){
						 	$(this).html("<input type='text' name='bp' id='bp' class='bpclass' value='"+tmpDate[0]+ "'/><input type='text' name='bp1' id='bp1' class='bpclass' value='"+tmpDate[1]+ "'/>");
							 }
						 }
					 }else if(col ==3){
						 if ($(this).find("#hr").val()== undefined){
						 	$(this).html("<input type='text' name='hr' id='hr' class='hrclass' value='"+$(this).text()+ "'/>");
						 }
					 }else if(col ==4){
						 if ($(this).find("#temp").val()== undefined){
							 	$(this).html("<input type='text' name='temp' id='temp' class='tempclass' value='"+$(this).text()+ "'/>");
							 }
					}else if(col ==5){
						 if ($(this).find("#ac").val()== undefined){
							 	$(this).html("<input type='text' name='ac' id='ac' class='acclass'  onchange=calculatevalue('acclass') value='"+$(this).text()+ "'/>");
							 }
					}else if(col ==6){
						 if ($(this).find("#inlet").val()== undefined){
							 	$(this).html("<input type='text' name='inlet' id='inlet' class='inletclass'  value='"+$(this).text()+ "'/>");
							 }
					}else if(col ==7){
						 if ($(this).find("#plasmaVolume").val()== undefined){
							 	$(this).html("<input type='text' name='plasmaVolume' id='plasmavolume' class='plasmaclass'  onchange=calculatevalue('plasmaclass') value='"+$(this).text()+ "'/>");
							 }
					}else if(col ==8){
						 if ($(this).find("#productVolume").val()== undefined){
							 	$(this).html("<input type='text' id='productvolume' name='productVolume'  class='productvolumeclass' onchange=calculatevalue('productvolumeclass') value='"+$(this).text()+ "'/>");
							 }
					}else if(col ==9){
						 if ($(this).find("#comment").val()== undefined){
							 var oldSelected = $(this).find("#tmpComment").val();
							 	$(this).html("<select class='select1'  id='comment' name='comments' >"+$("#comments_hidden").html()+ "</select>");
							 	$(this).find("#comment").val(oldSelected) ;
							 }
					}
				 });
			 }
		  });
		 // $.uniform.restore('select');
	      $("select").uniform();
	}
  } 
function calculatevalue(classname,objid){
	var totalvalue=0;
	  $("#processingParameterTable").find("."+classname).each(function (col){
			 if($(this).val()!=""){
			 totalvalue=parseFloat(totalvalue)+parseFloat($(this).val());
			 }
			 
		}); 
	 
	   if(classname=="acclass"){
		 $("#collectionAc").val(totalvalue);
	 } 
	 if(classname=="inletclass"){
		 $("#processingInlet").val(totalvalue);
	 } 
	 if(classname=="plasmaclass"){
		 $("#proplasmaVolume").val(totalvalue);
	 }
	   if(classname=="productvolumeclass"){
		 $("#proproductVolume").val(totalvalue);
	 }   
}
function processingparameter(){
	
	var checkEmpty =$('#processingParameterTable tbody tr td:nth-child(1)');
	var runningTabClass= $(checkEmpty).attr("class");  			
	if((runningTabClass=="dataTables_empty")){
		$(checkEmpty).remove();
	}
	var rowCount = $('#processingParameterTable tr').length;
	//alert("rowCount " + rowCount);
	if(rowCount ==2){
		rowCount = 0;
	}else{
		rowCount= rowCount-1;
	}
	rowCount = 10-rowCount;
	for(i=0;i<rowCount;i++){
		addParameter("add");
	/*	var newRow="<tr>"+
		"<td><input type='checkbox' class='checkalls'/><input type='hidden' id='processingParameter' name='processingParameter' value='0'/></td>"+
		"<td><input type='text' name='processingTime' id='time'/><input type='text' class = 'temptime' name='tempprocessingTime' id='temptime' onchange='setprocessingtime(this);'/></td>"+
		"<td><input type='text' name='bp' id='bp' class='bpclass'  /></td>"+
		"<td><input type='text' name='hr' id='hr' class='hrclass' /></td>"+
		"<td><input type='text' name='temp' id='temp' class='tempclass'  /></td>"+
		"<td class='acid'><input type='text' name='ac' id='ac' class='acclass'  onchange=calculatevalue('acclass','acid') /></td>"+
		"<td class='inletid'><input type='text' name='inlet' id='inlet' class='inletclass' onchange=calculatevalue('inletclass','inletid') /></td>"+
		"<td class='plasmaid'><input type='text' name='plasmaVolume' id='plasmavolume' class='plasmaclass'  onchange=calculatevalue('plasmaclass','plasmaid') /></td>"+
		"<td class='productvolumeid'><input type='text' id='productvolume' name='productVolume'  class='productvolumeclass' onchange=calculatevalue('productvolumeclass','productvolumeid') /></td>"+
		"<td><select class='select1'  id='comment' name='comments' >"+$("#comments_hidden").html()+ "</select></td>"+
		"</tr>";
		$("#processingParameterTable tbody").prepend(newRow); 
		*/				
	}
	$('.temptime').timepicker({});
	$("select").uniform(); 	
	
}
function addParameter(flag){
		var newRow="<tr>"+
				"<td><input type='checkbox' class='checkalls'/><input type='hidden' id='processingParameter' name='processingParameter' value='0'/></td>"+
				"<td><input type='hidden' name='processingTime' id='time'/><input type='text' class = 'temptime' name='tempprocessingTime' id='temptime"+timeid+"' onchange='setprocessingtime(this);'/></td>"+
				"<td><input type='text' name='bp' id='bp' class='bpclass numbersOnly' /><input type='text' name='bp1' id='bp1' class='bpclass numbersOnly'  /></td>"+
				"<td><input type='text' name='hr' id='hr' class='hrclass numbersOnly' /></td>"+
				"<td><input type='text' name='temp' id='temp' class='tempclass numbersOnly'  /></td>"+
				"<td class='acid'><input type='text' name='ac' id='ac' class='acclass numbersOnly'  /></td>"+
				"<td class='inletid'><input type='text' name='inlet' id='inlet' class='inletclass numbersOnly'/></td>"+
				"<td class='plasmaid'><input type='text' name='plasmaVolume' id='plasmavolume' class='plasmaclass numbersOnly'  /></td>"+
				"<td class='productvolumeid'><input type='text' id='productvolume' name='productVolume'  class='productvolumeclass numbersOnly' /></td>"+
				"<td><select class='select1'  id='comment' name='comments' >"+$("#comments_hidden").html()+ "</select></td>"+
				"</tr>";
				
				if(flag != "add"){
					$("#processingParameterTable tbody").prepend(newRow);
					$("#processingParameterTable .ColVis_MasterButton").triggerHandler("click");
					$('.ColVis_Restore:visible').triggerHandler("click");
					$("div.ColVis_collectionBackground").trigger("click");
				}else{
					
					if(checkModuleRights(STAFA_COLLECTION,APPMODE_ADD)){
					$("#processingParameterTable tbody").append(newRow);
					}
				}
				$('.temptime').timepicker({});
				$("select").uniform(); 	
				timeid=timeid+1;		
} 

$(function(){
    $("select").uniform();
//	$("li#transfer_icon").hide();

  });
function saveCollection(){
	var url='saveCollection';
	
	 var rowData = "";
	 var time_val;
	 var bp_val;
	 var hr_val;
	 var temp_val;
	 var ac_val;
	 var inlet_val;
	 var plasmavolume;
	 var productvolume;
	 var comment;
	var donor=$("#donor").val();
	var donorProcedure=$("#donorProcedure").val();
	var rowHtml="";
	 $("#processingParameterTable").find("#bp").each(function (row){
			 rowHtml =  $(this).closest("tr");
	// $("#processingParameterTable tbody tr").each(function (i,row){
		 time_val = bp_val = hr_val= temp_val = ac_val = inlet_val = plasmavolume= productvolume= comment= "";		 
	 	
	 	//	alert("row " + rowHtml);
		 		  processing_val=$(rowHtml).find("#processingParameter").val();
			 	
		 		  time_val=$(rowHtml).find("#time").val();
			 	
			 		bp_val=$(rowHtml).find("#bp").val();
			 		bp1_val=$(rowHtml).find("#bp1").val();
			 	
			 		hr_val=$(rowHtml).find("#hr").val();
			 	
			 		temp_val=$(rowHtml).find("#temp").val();
			 	
			 		ac_val=$(rowHtml).find("#ac").val();
			 	 
			 		inlet_val=$(rowHtml).find("#inlet").val();
			 	
			 		plasmavolume=$(rowHtml).find("#plasmavolume").val();
			 	 
			 		//alert($(this).html()); 
			 		productvolume=$(rowHtml).find("#productvolume").val();
			 	
			 		comment=$(rowHtml).find("#comment").val();
			 	
		 
				 	
	 		if(time_val!= ""&& time_val!= undefined){
		 	  rowData+= "{processingParameter:"+processing_val+",processingTime:'"+time_val+"',bp:'"+bp_val+"',bp1:'"+bp1_val+"',hr:'"+hr_val+"',temp:'"+temp_val+"',ac:'"+ac_val+"',inlet:'"+inlet_val+"',plasmaVolume:'"+plasmavolume+"',productVolume:'"+productvolume+"',comments:'"+comment+"',donor:'"+donor+"',donorProcedure:'"+donorProcedure+"'},";
		 	
	 		}
	 		
	 		//alert("inlet_val : "+inlet_val);
	 		 
	 		
	 });
	 		 if(rowData.length >0){
	             rowData = rowData.substring(0,(rowData.length-1));
	         }
	 			//alert("rowData:::"+rowData);
				var results=JSON.stringify(serializeFormObject($('#collectionForm')));
				results = results.substring(1,(results.length-1));
				var results1=JSON.stringify(serializeFormObject($('#collectionform1')));
				results1 = results1.substring(1,(results1.length-1));
				var results2=JSON.stringify(serializeFormObject($('#collectiofinalvalues')));
				results2= results2.substring(1,(results2.length-1));
				
				//alert(results1);
				//alert(results);
				
				//var jsonData = "jsonData={parameterData:["+rowData+"],processingData:["+results+"]}";
			var jsonData = "jsonData={parameterData:["+rowData+"],collectionData:{"+results+","+results1+","+results2+"},flowNextFlag:'" +$("#flowNextFlag").val()+"'}";

				
				//alert("===============>");
			   //alert("alert(jsonData); : "+jsonData);
			    
			  //  return;
				 var response = jsonDataCall(url,jsonData);
				 // alert(url);
	 
	 //alert("Data saved Successfully");
	 stafaAlert('Data saved Successfully','');
	constructProcessingParameterTable(true);
	processingparameter();
	if($("#flowNextFlag").val()!="true"){
		var aphCollection = $("#aphCollection").val();
		if(aphCollection ==0){
			var donor=$("#donor").val();
			var donorProcedure=$("#donorProcedure").val();
			var url = "loadNurseCollection.action?donorProcedure="+donorProcedure +"&donor="+donor;
			loadAction(url);
		}
	} 
	//$("#main").html(response1);
	return true;
}


</script>

<div class="hidden">
  		<select name="comments_hidden" id="comments_hidden" >
				<option value="">Select</option>
				<s:iterator value="lstComments" var="rowVal" status="row">
						<option value='<s:property value="#rowVal.pkCodelst" />'><s:property value="#rowVal.description" /></option>
					
				</s:iterator> 		
			</select>
		
</div>	
<div  id="slidecontrol" style="display:none;">
	<ul class="ulstyle">
		<span>
			<li class="listyle print_label"><img src = "images/icons/Bar_Code_32.png" onclick="loadBarcodePage();" style="width:30px;height:30px;cursor:pointer;"/></li>
			<li class="listyle print_label" ><b><s:text name="stafa.label.label"/></b> </li>
		</span>
	
			<span>
				<li class="listyle notes_icon"  style="margin-left:18%"><img src = "images/icons/notes.png"  onclick="notes(donor,'donor',this,false);" style="width:30px;height:30px;"/><sup style="margin-left:2%"><span class='subspan'></span></sup></li>
				<li class="listyle notes_icon"><b><s:text name="stafa.label.notes"/></b> </li>
			</span>
			<span>
				<li class="listyle print_label"><img src = "images/icons/labresults.png"  style="width:30px;height:30px;cursor:pointer;" onclick='addNewPopup("Complete Lab Result","labResults","loadLabResult","700","1050");'/></li>
				<li class="listyle print_label" ><b><s:text name="stafa.label.labresult"/></b> </li>
			</span>
		<span>
			<li class="listyle medication_icon"><img src = "images/icons/capsule1.png" onclick="medicationpopup(donormrn,donorVal,donorplannedprocedure);" style="width:30px;height:30px;cursor:pointer;"/></li>
			<li class="listyle medication_icon"><b><s:text name="stafa.label.medication"/></b></li>
		</span>
		<span>
			<li class="listyle transfer_icon"><img src = "images/icons/transfer.png" onclick="taskAction(donorplannedprocedure,'transfer')"  style="width:30px;height:30px;cursor:pointer;"/></li>
			<li class="listyle transfer_icon" id="transfer_icon"><b><s:text name="stafa.label.transfer"/></b></li>
			
		</span>
		<span>
			<li class="listyle terminate_icon"><img src = "images/icons/terminated.png"  onclick="taskAction(donorplannedprocedure,'terminate')" style="width:30px;height:30px;cursor:pointer;"/></li>
			<li class="listyle terminate_icon"><b><s:text name="stafa.label.terminate"/></b></li>
		</span>
		<!-- <li class="listyle complete_icon"><b>Complete</b></li>
		<li class="listyle complete_icon"><img src = "images/icons/task_completed_icon.PNG"  style="width:30px;height:30px;cursor:pointer;"/></li> -->
	</ul> 
	
</div>
<div id="nurse_slidewidget" style="display:none;">
				
<!-- Patient Info start-->
			<div class="column">
				<div  class="portlet">
					<div class="portlet-header "><s:text name="stafa.label.donorinformation"/></div>
						<div class="portlet-content">
							<table width="100%" border="0">
							  <tr>
								<td width="50%"><s:text name="stafa.label.donorid"/></td>
								<td width="50%" id="slide_donorId" name="slide_donorId"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.name"/></td>
								<td id="slide_donorName" name="slide_donorName"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.dob"/></td>
								<td id="slide_donorDOB" name="slide_donorDOB"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.donoraborh"/></td>
								<td id="slide_donorABORH" name="slide_donorABORH"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.procedure"/></td>
								<td id="slide_procedure" name="slide_procedure"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.donation"/></td>
								<td id="slide_donation" name="slide_donation"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.weights"/></td>
								<td id="slide_donorWeight" name="slide_donorWeight"></td>
							  </tr>
							   <tr>
								<td>Height</td>
								<td id="slide_donorHeight" name="slide_donorHeight"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.targetvolume"/></td>
								<td id="slide_targetVol" name="slide_targetVol"></td>
							  </tr>
							</table>
						</div>
				</div>
			</div>
				
<!-- Patient Info-->

<!-- Lab Results start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header "><s:text name="stafa.label.donorlab"/></div>
						<div class="portlet-content">
							<table width="100%" border="0">
								<tr>
									<td width="50%"><s:text name="stafa.label.idms"/></td>
									<td width="50%" id="idms"></td>
								</tr>
								<tr>
									<td width="50%"><s:text name="stafa.label.westnile"/></td>
									<td width="50%" id="westnile"> </td>
								</tr>
								<tr>
									<td width="50%"><span class="cd34"><s:text name="stafa.label.donorcd34"/></span>
									<span class="cd3"><s:text name="stafa.label.donorcd3"/></span></td>
									<td width="50%" id="donorcd34"> </td>
								</tr>
							</table>
						</div>
				</div>
				</div>
				
<!-- Lab Results-->
			<div class="column">
					<div class="portlet">
						<div class="portlet-header "><s:text name="stafa.label.productlab"/></div>
							<div class="portlet-content">
							<table width="100%" border="0">
								<tr>
									<td width="50%">
									<span class="cd34"><s:text name="stafa.label.cd34_day1"/></span>
									<span class="cd3"><s:text name="stafa.label.cd3_day1"/></span>
									</td>
									<td width="50%" id="cd34_day1"></td>
								</tr>
								<tr>
									<td width="50%">
									<span class="cd34"><s:text name="stafa.label.cum_CD34"/></span>
									<span class="cd3"><s:text name="stafa.label.cum_CD3"/></span>
									</td>
									<td width="50%" id="cum_CD34"></td>
								</tr>
							</table>
							</div>
					</div>
				</div>
<!-- Recipient Info start-->
				<div class="column">
					<div class="portlet">
						<div class="portlet-header "><s:text name="stafa.label.recipientinformation"/></div>
							<div class="portlet-content">
							<table width="100%" border="0">
							<tr>
								<td width="50%"><s:text name="stafa.label.recipientmrn"/></td>
								<td width="50%" id="recipient_id"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.name"/></td>
								<td id="recipient_name"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.dob"/></td>
								<td id="recipient_dob"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.aborh"/></td>
								<td id="recipient_aborh"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.diagnosis"/></td>
								<td id="recipient_diagnosis"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.weights"/></td>
								<td id="recipient_weight"></td>
							  </tr>
							  </table>
							</div>
							 
					</div>
				</div>
				
<!--Recipient Info end-->
	</div>
<div class="cleaner"></div>

<form action="#" id="collectionForm" onsubmit="return avoidFormSubmitting();">
<s:hidden id="aphCollection" name="aphCollection" value="0"/>
<s:hidden id="donor" name="donor" value="%{donor}"/>
<s:hidden id="donorProcedure" name="donorProcedure" value="%{donorProcedure}"/>
<div class="column">		
		<div  class="portlet">
		<div class="portlet-header ">Product Label</div>
			<div class="portlet-content">
				<!-- <div id="button_wrapper">
      				<div style="margin-left:40%;">
       					<input class="scanboxSearch barcodeSearch"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;"/>
						<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" />
 					</div>
       			</div> -->
       			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
					<tr>
						<td class="tablewapper">Product ID</td>
						<td class="tablewapper1"><s:hidden name='productId' id='productId' value="%{productId}" /><s:textfield id="specimen" name="specimen" value="%{specimen}" /></td>
						<td class="tablewapper">Collection Date</td>
						<td class="tablewapper1"><s:textfield id="collectionDate" name="collectionDate" value="%{collectionDate}" Class="dateEntry dateclass"/></td>
					</tr>
					<tr>
						<td class="tablewapper">Product Label Verified:Nurse1</td>
					 <td class="tablewapper1"><s:radio name="productLabelVerified" id="productLabelVerified" list="#{'1':'Yes','0':'No'}" /><a href=# id="productverification_view" style="color:blue;display:none;">&nbsp;View</a></td>
						<td class="tablewapper">Collection Start Time</td>
						<td class="tablewapper1"><input type="hidden" id="collectionStartTime" name="collectionStartTime" Class=""><s:textfield id="tmpcollectionStartTime" name="tmpcollectionStartTime" Class="" onchange="setCollectionStartTime();"/></td>
	                </tr>
	               <!--  <tr>
						<td class="tablewapper">Warning Label Affixed</td>
						<td class="tablewapper1"><input type="text" id="" name="" placeholder=" "/> </div></td>
						<td class="tablewapper">Auto Only Label Affixed</td>
						<td class="tablewapper1"><input type="radio" id="" name="" class=""/>Y &nbsp; <input type="radio" id="" name="" class="selectDeselect"/> N</td>
					</tr> -->
				</table>
			</div>
		</div>
</div>
	
	<div class="cleaner"></div>

<div class="column">		
	<div  class="portlet">
	<div class="portlet-header ">Pre-Collection Information</div>
		<div class="portlet-content">
            	<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%"  style="table-layout:fixed !important; width: 100%">
                	<tr>
                    	<td class="tablewapper">Access</td>
                        <td class="tablewapper1"><s:select id="refAccess"  name="refAccess" list="lstAccess" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
                        <td class="tablewapper">Return</td>
                        <td class="tablewapper1"><s:select id="refReturn"  name="refReturn" list="lstReturn" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
                    </tr>
                    <tr>
                        <td class="tablewapper">Withdraw</td>
                        <td class="tablewapper1"><s:select id="refWithdraw"  name="refWithdraw" list="lstWithdraw" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
                    </tr>
                    <tr>
                        <td class="tablewapper">Blood Warmer Used</td>
                        <td class="tablewapper1"><s:radio name="bloodWarmerUsed" id="bloodWarmerUsed" list="#{'1':'Yes','0':'No'}"  onclick="setTempMandatory('bloodWarmerUsed');"/></td>
                         <td class="tablewapper">Blood Warmer Starting Temp. (C)</td>
                        <td class="tablewapper1"><s:textfield id="bloodWarmerStartingTemp" name="bloodWarmerStartingTemp" value="%{bloodWarmerStartingTemp}" Class=""/></td>
                    </tr>
                    <tr>
                        <td class="tablewapper">RBC Prime</td>
                        <td class="tablewapper1"><s:radio name="rbcPrime" id="rbcPrime" list="#{'1':'Yes','0':'No'}" onclick="setTempMandatory('rbcPrime');" /></td>
                         <td class="tablewapper">RBC Unit Component#:</td>
                        <td class="tablewapper1"><s:textfield id="rbcUnitComponentNumber" name="rbcUnitComponentNumber" Class="" value="%{bloodWarmerStartingTemp}"/></td>
                    </tr>
				</table> 

      		    <table cellpadding="0" cellspacing="0" class="display" align="center" border="1" width="100%">
	      		    <thead>
		      		    <tr>
			      		    <th></th>
			      		    <th>ACD</th>
			      		    <th>Inlet</th>
			      		    <th>Plasma</th>
			      		    <th>Replace</th>
			      		    <th>WB:ACD-A</th>
			      		    <th>RPM</th>
	      		    	</tr>
	      		    </thead>
	      		    <tbody>
			      	    <tr>
			      		    <td style="width:10%">Initial Flow</td>
			      		    <td><s:textfield  id="precollectionACD" name="precollectionACD" style="width:90%" cssClass="numbersOnly"/></td>
			      		    <td><s:textfield  id="precollectionInlet" name="precollectionInlet" style="width:90%" cssClass="numbersOnly"/></td>
			      		    <td><s:textfield  id="precollectionPlasma" name="precollectionPlasma" style="width:90%" cssClass="numbersOnly"/></td>
			      		    <td><s:textfield  id="precollectionReplace" name="precollectionReplace" style="width:90%" cssClass="numbersOnly"/></td>
			      		    <td><s:textfield  id="precollectionWBACDA" name="precollectionWBACDA" style="width:90%" cssClass="numbersOnly"/></td>
			      		    <td><s:textfield  id="precollectionRPM" name="precollectionRPM" style="width:90%" cssClass="numbersOnly" /></td>
			      		</tr>
	      		    </tbody>
      		    </table>
			</div>
		</div>
</div>
</form>
	<div class="cleaner"></div>
	
<div class="column">		
		<div  class="portlet">
			<div class="portlet-header ">Processing Parameters</div>
				<div class="portlet-content">
				<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="addParameter()"/>&nbsp;&nbsp;<label  class="cursor" onclick="addParameter()"><b>Add </b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteprocessingData();"/>&nbsp;&nbsp;<label class="cursor" onclick="deleteprocessingData();"><b>Delete</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" onclick="editProcessingRows()"/>&nbsp;&nbsp;<label  class="cursor" onclick="editProcessingRows()"><b>Edit</b></label>&nbsp;</div></td>
				</tr>
				</table>
				<div id="parameterDiv" style="overflow-x:auto;overflow-y:hidden;">
				<table cellpadding="0" cellspacing="0" border="0" id="processingParameterTable" class="display">
					<thead>
						<tr>
							<th width="4%" id="parameterColumn"></th>
							<th>Time</th>
							<th>BP (mmHg)</th>
							<th>HR (bpm) </th>
							<th>Temp (F)</th>
							<th>AC</th>
							<th>Inlet</th>
							<th>Plasma Volume (mL)</th>
							<th>Product Volume (mL)</th>
							<th>Comments</th>
						</tr>
						
					</thead>
					<tbody>
												
					</tbody>
				</table>
				</div>
				<div class="finalvalue"><b>Final Values</b>	</div>	
							<form id="collectiofinalvalues" onsubmit="return avoidFormSubmitting();">
				<%-- <s:hidden id="aphCollection" name="aphCollection" value="0"/> --%>
                    <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                       <tr>
	                        <td style="width:3%;" >AC</td>
	                        <td style="width:13%;"><s:textfield  id="collectionAc" name="collectionAc" cssClass="numbersOnly"/></td>
	                        <td style="width:3%;" >Inlet</td>
	                        <td style="width:13%;"><s:textfield  id="processingInlet" name="processingInlet" cssClass="numbersOnly"/></td>
	                        <td style="width:3%;" >Plasma Volume (mL)</td>
	                        <td style="width:13%;"><s:textfield type="text" id="proplasmaVolume" name="proplasmaVolume" cssClass="numbersOnly"/></td>
                    	</tr>
                     <tr>
	                    	<td style="width:3%;" >Product Volume (mL)</td>
	                        <td style="width:13%;"><s:textfield  id="proproductVolume" name="proproductVolume" cssClass="numbersOnly"/></td>
	                        <td style="width:3%;" >Collection End Time</td>
	                        <td style="width:13%;"><s:hidden  id="collectionEndTime" name="collectionEndTime" Class="" /><s:textfield id="tmpcollectionEndTime" name="tmpcollectionEndTime" Class="" onchange="setCollectionEndTime();"/></td>
	                        <td style="width:3%;" ></td>
	                        <td style="width:13%;"></td>
                    </tr>
                     <tr>
	                        <td style="width:3%;" >Apheresis Input(mL)</td>
	                        <td style="width:13%;"><s:textfield  id="apheresisInput" name="apheresisInput" cssClass="numbersOnly"/></td>
	                        <td style="width:3%;" >Apheresis Output(mL)</td>
	                        <td style="width:13%;"><s:textfield  id="apheresisOutput" name="apheresisOutput" cssClass="numbersOnly"/></td>
	                        <td style="width:3%;" >Net Input/Output (mL)</td>
	                        <td style="width:13%;"><s:textfield  id="totalvolume" name="totalvolume" cssClass="numbersOnly"/></td>
                    </tr>
                    </table>
                    </form>
				</div>	
			</div>
</div>
<div class="cleaner"></div>
<form action="#" id="collectionform1" onsubmit="return avoidFormSubmitting();">
<%-- <s:hidden id="aphCollection" name="aphCollection" value="0"/> --%>

<div class="column">		
		<div  class="portlet">
			<div class="portlet-header ">Mid-Count Information</div>
				<div class="portlet-content">
                    <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
	                	<tr>
	                		<td class="tablewapper" >Mid-Count WBC</td>
		                    <td class="tablewapper1"><input type="text" id="midCountWbc" name="midCountWbc" Class="numbersOnly"></td>
		                    <td class="tablewapper" >Mid-Count Liters Collected (L)</td>
	                        <td class="tablewapper1"><input type="text" id="midCountLitersCollected" name="midCountLitersCollected" Class="numbersOnly"></td>
	                    </tr>
                        <tr>
                        	<td class="tablewapper" >Collect Plasma</td>
	                        <td class="tablewapper1"><s:radio name="collectPlasma" id="collectPlasma" list="#{'1':'Yes','0':'No'}" value="" onclick="setplasmaid();"/></td>
	                        <td class="tablewapper" >Notified Physician </td>
	                        <td class="tablewapper1"><s:radio name="notifiedPhysician" id="notifiedPhysician" list="#{'1':'Yes','0':'No'}" value="" /></td>
                        </tr>
                         <tr>
	                       	<td class="tablewapper" >Amount Plasma to Collect(mL)</td>
	                        <td class="tablewapper1" ><input type="text" id="plasmaToCollect" name="plasmaToCollect" Class="numbersOnly"></td>
                  		 </tr>
                    </table> 
				</div>
		</div>
</div>

<div class="cleaner"></div>
<div class="column">		
		<div  class="portlet" id="plasmalabel">
			<div class="portlet-header ">Plasma Label</div>
				<div class="portlet-content">
				<!-- <div id="button_wrapper">
      				 <div style="margin-left:40%;">
       				 <input class="scanboxSearch barcodeSearch"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;"/>
					 <input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" />
		        <input class="scanboxSearch hidden" type="text" size="15" value="" name="productSearch" id="productSearch" onkeyup="getAccessionData(event,this);"  placeholder="Scan/Enter Product ID" style="width:200px;"/>
    
 					</div>
       			 </div> -->
				
                    <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                       <tr>
	                        <td class="tablewapper">Plasma Id</td>
	                        <td class="tablewapper1"><s:hidden name='plasmaRef' id='plasmaRef' value="%{plasmaRef}" /><s:textfield id="plasmaId" name="plasmaId" Class="" value="%{plasmaId}" /></td>  
	                        <td class="tablewapper">Collection Date</td>
	                        <td class="tablewapper1"><input type="text" id="plasmaCollectionDate" name="plasmaCollectionDate" Class="dateEntry dateclass"></td>
                    </tr>
                        <tr>
                        	<td class="tablewapper">Plasma Label Verified:Nurse1</td>
                            <td class="tablewapper1"><s:radio name="plasmaLabelVerified" id="plasmaLabelVerified" list="#{'1':'Yes','0':'No'}" value="" onclick="" /><a href=# id="plasmaverification_view" style="color:blue;display:none;">&nbsp;View</a></td>
                            <td class="tablewapper">Collection Start Time</td>
                            <td class="tablewapper1"><input type="hidden" id="plasmaCollectionStartTime" name="plasmaCollectionStartTime" Class=""><input type="text" id="tmpplasmaStartTime" name="tmpplasmaStartTime" Class="" onchange="setplasmaStartTime();"></td>
                        </tr>
                    

                    </table> 
				</div>
		</div>
</div>

<%--  <div class="floatright_right">
	<jsp:include page="sidecontrols.jsp"></jsp:include>
 </div> --%>
<div class="cleaner"></div>
</form>		
<form onsubmit="return avoidFormSubmitting()">	
<div align="left" style="float:right;">
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
			<tr>
				<td><span class='nextContainer'>Next:
				<select name="nextOption" id="nextOption" ></select>
				<input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
				<input type="button" value="Next" onclick="setConditionMandatory();verifyeSign('eSignNext','next')"/></span></td>
			</tr>
		</table>
</div>
</form>	



<!-- Barcode Starts -->		
<div  id ="mainbarcodediv" style="background-color:white; display:none; height:655px;overflow:auto;margin-top:1%;">  <!-- added by mohan j -->
<div id="maindiv"><div id="barimageTables" style="float:right;display:none; width:44%;height:40%;">
			<!-- From Template Start -->
<div id="barcodeMainDiv" >
		</div>
			<!-- From Template Ends -->
</div></div>
	
</div>
<!-- Barcode Ends -->

<script>

function showportlet(){
	
	 $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" ).find("span").removeClass("ui-icon-plusthick" ).addClass( "ui-icon-minusthick" );
	 $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" ).end().find(".portlet-content").show();
}


</script>

