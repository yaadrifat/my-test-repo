<%@ include file="common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script type="text/javascript" src="js/placeholder.js"></script>
<script language="javascript">


function getImmnoPhenoDetails(pkImmunoPheno,immnoCode,immnoParentCode,immnoPhenoDesc){
	//alert("pkImmunoPheno : "+pkImmunoPheno);
	//alert("immnoCode : "+immnoCode);
	//alert("immnoParentCode : "+immnoParentCode);
	//alert("immnoPhenoDesc : "+immnoPhenoDesc);
	$("#immuneOk").click(function(){
		if('input:radio[name=immnoOption]:checked'){
			var s=$("#qcSelect").val(immnoPhenoDesc);
			showPopUp("close","immnophenotypingDialog","","","");
		}
	});
	$("#immnoSubLegend").html(immnoPhenoDesc);
	$("#immuno").val("");
	$("#immunoCode").val(immnoCode);
	var ajaxresult = ajaxCall('fetchImmnoPhenoDetails','originalProductForm');
	viewImmnoPhenoSubList(ajaxresult,immnoCode);
}

function viewImmnoPhenoSubList(result,immnoCode){
	var nLevel="";
	var immnoTable="<table border='0' cellpadding='0' cellspacing='0' style=\"background-color:white;border:0.5px solid;\" width=\"100%\">";
	//immnoTable+="<tr><td>&nbsp;</td><td>&nbsp;</td></tr><tr>";
	var newCode="";
	var oLevel;
	var newFlag="";
	//alert(result.immnoPhenoList);
	$.each(result.immnoPhenoList, function(mainIndex, immnoval) {
		oLevel = immnoval[4];
		parentCode = immnoval[2];
		code = immnoval[1];
		/*****/
		//alert("0-->"+immnoval[0]+"\n1-->"+immnoval[1]+"\n2-->"+immnoval[2]+"\n3-->"+immnoval[3]);
		//SELECT PK_IMMNOPHENOTYPING, IMMNO_CODE, IMMNO_PARENTCODE,IMMNO_DESC, LEVEL,IMMNO_HIDE,IMMUNO_COMMENTS
		
		if(immnoCode==parentCode){
			if(newFlag=="1"){
				immnoTable+="<td>&nbsp;</td></tr>";
				}
			immnoTable+="<tr>";
			immnoTable+="<td>"+immnoval[3]+"</td>";
			newCode = immnoval[1];
			newFlag = "1";
			}
		if(newCode==parentCode){
			if(newFlag=="1"){
				immnoTable+="<td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>"+immnoval[3]+"</td>";
				}else{
				immnoTable+="</tr><tr><td>&nbsp;</td><td>"+immnoval[3]+"</td>";
				}
			newCode = immnoval[1];
			newFlag = "2";
			}
	});
	immnoTable+="</tr></table>";
	//alert(immnoTable);
	$("#immnoSubColumn").attr("style","display:none");
	//alert($("#immnoSubList").html());
	$("#immnoSubList").html(immnoTable);
	$("#immnoSubColumn").attr("style","display:block");
	
	//
	//openImmnophenotyping()
}

function getImmnoPhenoList(){
	 var ajaxresult = ajaxCall('fetchImmnoPhenoDetails','originalProductForm');
	 //alert(ajaxresult);
	 viewImmnoPhenoList(ajaxresult);
}

function viewImmnoPhenoList(result){
	var nLevel="";
	var immnoTable="<table>";
	$.each(result.immnoPhenoList, function(mainIndex, immnoval) {
		nLevel = immnoval[4];
		if(nLevel==2){
			immnoTable+="<tr><td>";
			immnoTable+="<input type='radio' name='immnoOption' id='' onclick='getImmnoPhenoDetails(\""+immnoval[0]+"\",\""+immnoval[1]+"\",\""+immnoval[2]+"\",\""+immnoval[3]+"\")'/></td><td>"+immnoval[3]+"</td>";
			immnoTable+="</tr>";
			//alert("val-->"+immnoval[3]);
			//alert(immnoTable);
			}
		
	});
	immnoTable+="</table>";
	//alert(immnoTable);
	$("#immnoMainList").replaceWith(immnoTable);
	openImmnophenotyping();
}
 /* function immunSelect(){
		 getSelected();
}
 */
function nextProcess(){

	 $('.progress-indicator').css( 'display', 'block' );

	//var	 rowData+= "{reagentSupplies:"+reagentSupplies+",isHide:"+appearance+",quantity:'"+quantity+"',lot:'"+lot+"',manufacturer:'"+manufacturer+"'},";
	
	var chk_arr = $("#qcContent input[type=checkbox]");
	var chklength = chk_arr.length;             
	//alert("qcCheckList : "+qcCheckList);
	var qcData = "";
	for(y=0;y< chklength;y++)
	{	
		var id = $(chk_arr[y]).attr("id");
		if($('#'+id).is(':checked')){
				qcData+= "{fk_qc:"+id+"},";			
			}
	} 

	if(qcData.length >0){
		qcData = qcData.substring(0,(qcData.length-1));
    }
	 
	 var rowData = "";
	 var quantity;
	 var manufacture;
	 var lot;
	 var expirationDate;
	 var appearance;
	 var isHide =0;
	 var appearance;
	 var reagentSupplies;
	 $("#reagentsTable tbody tr").each(function (row){
		 isHide =0;
	 	$(this).find("td").each(function (col){
		 		 		
		 	   if(col ==0){
			 		reagentSupplies= $(this).find("#reagentSupplies").val();
			 	 }else if(col ==1){
			 		quantity=$(this).html();
			 	 }else if(col ==2){
			 		manufacture=$(this).html();
			 	 }else if(col ==3){
			 		manufacturer=$(this).html();
			 	 }else if(col ==4){
			 		lot=$(this).html();
			 	 }else if(col ==5){
			 		 if($(this).find("#yes").is(":checked")){
			 			appearance = 'Y';
			 		 }else{
			 			appearance = 'N';
			 		 }
			 	 }
		  });
	 		if(reagentSupplies!= undefined && quantity != undefined){
		 	  rowData+= "{reagentSupplies:"+reagentSupplies+",isHide:"+appearance+",quantity:'"+quantity+"',lot:'"+lot+"',manufacturer:'"+manufacturer+"'},";
	 		}
	 		alert(rowData);
	 });
	
	 if(rowData.length >0){
            rowData = rowData.substring(0,(rowData.length-1));
        }
        url="saveOriginalProduct";
	//?BB    response = jsonDataCall(url,"jsonData={reagentSuppliesData:["+rowData+"],qcData:["+qcData+"],productId:12}");
	   response = jsonDataCall(url,"jsonData={reagentSuppliesData:["+rowData+"],qcData:["+qcData+"],productId:12}");
	var nextToOriginalProduct = $("#nextToOriginalProduct").val();
    $.ajax({
        type: "POST",
        url: "saveOriginalProduct.action",
        async:false,
        data : $("#originalProductForm").serialize(),
        success: function (result){
				alert(result);      
        },
        error: function (request, status, error) {
            alert("Error " + error);
        }
    });
        /* if(nextToOriginalProduct=="Processing")
		{
			javascript:loadAction('getProcessing.action');
		}
        
    	if(nextToOriginalProduct=="Cryoprep")
        {
    		javascript:loadPage('html/stafa/cryo_prep.html');
        }
     	 */
 
    $('.progress-indicator').css( 'display', 'none' );
}


function hideBarcodes(){
	$("#imgtip0").css("display","none");
	$("#imgtip1").css("display","none");
	$("#imgtip2").css("display","none");
	$("#imgtip3").css("display","none");
}


function generateTableFromJsnList(response){
	var arr = [];
	var pkCodeList = [];
	$.each(response.qcList, function(i,v) {
		
		pkCodeList.push(v[0]);
		arr.push(v[3]);
				
	 });var k=1;	
		//alert("outside"+k);
	var tab="<table id='qcTable' style='width:100%'><tr>";
		var tab1="<table style='width:100%'><tr>";		 
		$.each(arr, function(i,val){
		if((k%4==1)&&(i!=0)){
					if (arr[i].toLowerCase().indexOf("immunopheno") >= 0){
	
				            tab1+="</tr><tr><td>";	
				 			tab1+="<input type='checkbox'  name='ABO' id='"+pkCodeList[i]+"' onclick=\"javascript:qcCalculation('frmQC');\">&nbsp;<a href='#' onclick=\"javascript:showQcDetails('"+pkCodeList[i]+"','"+arr[i]+"')\">"+arr[i]+"</a>&nbsp;<input type=text id=qcSelect><input type=button value=Edit onclick='getImmnoPhenoList()'/></td>";
							}
			
					else{			
							tab+="</tr><tr><td>";
							tab+="<input type='checkbox'  name='ABO' id='"+pkCodeList[i]+"' onclick=\"javascript:qcCalculation('frmQC');\">&nbsp;<a href='#' onclick=\"javascript:showQcDetails('"+pkCodeList[i]+"','"+arr[i]+"')\">"+arr[i]+"</a></td>";
							k++;					
						}
				
				}
			 else{
				
				 if (arr[i].toLowerCase().indexOf("immunopheno") >= 0){
						 tab1+="</tr><tr><td>";	
					     tab1+="<input type='checkbox'  name='ABO' id='"+pkCodeList[i]+"' onclick=\"javascript:qcCalculation('frmQC');\">&nbsp;<a href='#' onclick=\"javascript:showQcDetails('"+pkCodeList[i]+"','"+arr[i]+"')\">"+arr[i]+"</a>&nbsp;<input type=text id=qcSelect><input type=button value=Edit onclick='getImmnoPhenoList()'/></td>";
						}
    				 else
						{
    					
    					tab+="<td>";			
						tab+="<input type='checkbox'  name='ABO' id='"+pkCodeList[i]+"' onclick=\"javascript:qcCalculation('frmQC');\">&nbsp;<a href='#' onclick=\"javascript:showQcDetails('"+pkCodeList[i]+"','"+arr[i]+"')\">"+arr[i]+"</a></td>";
						k++;
						}
    				 
				  }
		
		 });
	
	$("#qcContent").html(tab);
	$("#immunContent").html(tab1);
	return true;
}


function generateTableFromJsnList1(response){
	if(response.recipientDonorList.length<=0){
		alert("This product is not accessed. Please enter the correct product.");
		return;
		}
	$.each(response.recipientDonorList, function(i,v) {
		//alert(i+"=-="+v[0]);
		$("#recipientName").html(v[0]);
		$("#recipientId").html(v[1]);
		$("#recipientDiagnosis").html(v[2]);
		$("#recipientAboRh").html(v[3]);
		$("#donorName").html(v[4]);
		$("#donorId").html(v[5]);
		$("#donorAboRh").html(v[6]);
		$("#productId").html(v[7]);
		$("#productType").html(v[9]);
		$("#fkProtocol").val(v[11]);
	 	});
	//var qcCheckList = [];
	var qcCheckList = Array(2); 
	$.each(response.protocolQcList, function(i,v) {
		qcCheckList.push(v[2])
	 	});
 	
	var chk_arr = $("#qcContent input[type=checkbox]");
	var chklength = chk_arr.length;             
	//alert("qcCheckList : "+qcCheckList);
	for(y=0;y< chklength;y++)
	{	
		var id = $(chk_arr[y]).attr("id");
		//alert("id : "+id);
		if (Search_Array(qcCheckList, id)){
			//alert("Found : "+id);
			chk_arr[y].checked = true;
			$(chk_arr[y]).trigger('click');
			$("#"+id).attr("checked","checked");
			}
			else{
				//alert("Not Found");
				chk_arr[y].checked = false;
			}
	} 
	var mappedProtocol = $("#fkProtocol").val();
	//alert("mappedProtocol "+mappedProtocol);
	//$.uniform.restore('select');
	getProtocolCalculation(mappedProtocol,"calculationDiv");
	// BB
	$("select").uniform();
}

function Search_Array(ArrayObj, SearchFor){
  var Found = false;
  //alert(ArrayObj+"--"+SearchFor);
  for (var i = 0; i < ArrayObj.length; i++){
    if (ArrayObj[i] == SearchFor){
      return true;
      var Found = true;
      break;
    }
    else if ((i == (ArrayObj.length - 1)) && (!Found)){
      if (ArrayObj[i] != SearchFor){
        return false;
      }
    }
  }
}

function getProcessingData(e){
	//?BB
	//var url = "getOriginalProductData";
	var url = "getOrginalData";
	var productSearch = $("#productSearch").val();
	if(e.keyCode==13 && productSearch!=""){
		$('.progress-indicator').css( 'display', 'block' );
		//alert("s : "+e.keyCode);
		//var url = "getOriginalProductData";
	var url = "getOrginalData";
		var response = ajaxCall(url,"originalProductForm");
		generateTableFromJsnList1(response);
		$('.progress-indicator').css( 'display', 'none' );
	}
	//response = jQuery.parseJSON(response.recipientDonorList);
	

}
function openImmnophenotyping(){
	setImmnophenotyping();
	$('#immnophenotypingDialog').dialog('open');	
}

function setImmnophenotyping(){

	$("#immnophenotypingDialog").dialog({
		   autoOpen: false,
		   modal: true,
		   width:700,
		   hieght:900,
		   close: function() {
	    		jQuery("#immnophenotypingDialog").dialog("destroy");
		   }
		});
	
}

function showHide(This){
	$("#addtxt").hide();
	//$("#thSelectColumn").css('display: block');
	if(This.id=="childYes"){
		$("#childDiv").show();
		// $('#createchild'). find( ".portlet-header .ui-icon" ).removeClass('ui-icon-minusthick')addclass('ui-icon-plusthick');
		nchildrens();
		//$( ".portlet-header .ui-icon" ).trigger("click");
		}else{deleteallchild();
			//$("#childDiv").hide();
			//$( ".portlet-header .ui-icon" ).trigger("click");
	}
}

	var name = "#floatMenu";
	var menuYloc = null;
	

$(document).ready(function() {
	$("#childDiv").hide();
	//getChildcreationTable();   
	//alert($("#newProductId").val());
	 var today = new Date();
   
    var d = today.getDate();
    var m = today.getMonth();
    var y = today.getFullYear();

    var h=today.getHours();
    var mn=today.getMinutes()+1;
   
    $( "#originalDateStr" ).datepicker({
        dateFormat: 'M dd, yy',
        //minDate: new Date(2012, 11, 20, 8, 30)
        minDate: new Date(y, m, d, h,mn)
            });
	$('#timePreviousPro').timepicker({});
$('#number').spinit({min:0,stepInc:1,max:200,height: 30,width: 65 });
	hideBarcodes();
	$("#emptyId").hide();
	setImmnophenotyping();
	 oTable =$('#childcreation').dataTable({
		 "bJQueryUI": true,
			"bRetrieve": true,
			"bPaginate":false,
			 "oColVis": {
					"aiExclude": [ 0 ],
					"buttonText": "&nbsp;",
					"bRestore": true,
					"sAlign": "left",
					"iOverlayFade": 25 
				},
				"sDom": 'C<"clear">Rlfrtip',
			//sorting the input fields
		 "aoColumns": [{ "bSortable":false },
		               { "bSortable":false },
			   			null,
			   			null,
			   			{ "sSortDataType": "dom-select" },
			   			{ "sSortDataType": "dom-select" },
			   			{ "sSortDataType": "dom-text", "sType": "numeric" },
			   			{ "sSortDataType": "dom-text", "sType": "numeric" },
			   			{ "sSortDataType": "dom-text", "sType": "numeric" },
			   			{ "sSortDataType": "dom-text", "sType": "numeric" },
			   			{ "sSortDataType": "dom-text", "sType": "numeric" },
			   			{ "sSortDataType": "dom-text", "sType": "numeric" },
			   			{ "sSortDataType": "dom-select" }
		   		],
		   		"fnInitComplete": function () {
			        $("#childColumn").html($('.ColVis:eq(0)'));
				}
		
	});
	 oTable.thDatasorting('childcreation'); 
		//setColumnManager('childcreation');
	oTable = $('#parentProductTable').dataTable({
		"bJQueryUI": true,
		"bRetrieve": true
	});
	oTable = $('#parentProductTable').dataTable({
		"bJQueryUI": true,
		"bRetrieve": true
	});

	oTable = $('#scannedProductTable').dataTable({
		"bJQueryUI": true,
		"bRetrieve": true
		//"sPaginationType": "full_numbers"
	});


	oTable = $('#equipmentsTable').dataTable({
		"bJQueryUI": true,
		"aoColumnDefs": [{"sTitle":"Equiment", "aTargets": [ 0]},
		                 {"sTitle":"Manufacturer","aTargets": [ 1]},
		                 {"sTitle":"Serial#", "aTargets": [ 2]},
		                 {"sTitle":"Expiration Date", "aTargets": [ 3]},
		                 {"sTitle":"Appearance OK", "aTargets": [ 4]}
		]
		//"sPaginationType": "full_numbers"
	});
	oTable.thDatasorting('equipmentsTable');
	 oTable1 = $('#reagentsTable').dataTable({
		"bJQueryUI": true,
		"aoColumnDefs": [{"sTitle":"Reagents And Supplies", "aTargets": [ 0]},
		                 {"sTitle":"Quantity","aTargets": [ 1]},
		                 {"sTitle":"Manufacturer", "aTargets": [ 2]},
		                 {"sTitle":"Lot #", "aTargets": [ 3]},
		                 {"sTitle":"Expiration Date", "aTargets": [ 4]},
		                 {"sTitle":"Appearance OK", "aTargets": [ 5]}
		]
		//"sPaginationType": "full_numbers"
	}); 
	oTable1.thDatasorting('reagentsTable');
	menuYloc = parseInt($(name).css("top").substring(0,$(name).css("top").indexOf("px")))
	$(window).scroll(function () { 
		offset = menuYloc+$(document).scrollTop()+"px";
		$(name).animate({top:offset},{duration:500,queue:false});
	});
	//to get Qc list 
	var url = "getQc";
	var response = ajaxCall(url,"originalProductForm");
	var qcloadedflag = generateTableFromJsnList(response);
	
	//to get calculation list 
	var mappedProtocol = $("#fkProtocol").val();
	getProtocolCalculation(mappedProtocol,"calculationDiv");
	if(qcloadedflag){
		var ajaxresult = jsonDataCall('getProtocolDetails',"jsonData={protocol:"+mappedProtocol+"}");
		if(ajaxresult.exisQclst != null){
			var qcCheckList = Array(); 
			
			$.each(ajaxresult.exisQclst, function() {
				qcCheckList.push(this.fkCodelstQc);
			});
			
			var chk_arr = $("#qcContent input[type=checkbox]");
			var chklength = chk_arr.length;             
			//alert("qcCheckList : "+qcCheckList);
			for(y=0;y< chklength;y++)
			{	
				var id = $(chk_arr[y]).attr("id");
				//alert("id : "+id);
				if (Search_Array(qcCheckList, id)){
					//alert("Found : "+id);
					chk_arr[y].checked = true;
					$(chk_arr[y]).trigger('click');
					$("#"+id).attr("checked","checked");
					}
					else{
						//alert("Not Found");
						chk_arr[y].checked = false;
					}
			} 
		}
	}
	
});
/* Create an array with the values of all the select options in a column */
$.fn.dataTableExt.afnSortData['dom-select'] = function  ( oSettings, iColumn )
{
	var aData = [];
	$( 'td:eq('+iColumn+') select', oSettings.oApi._fnGetTrNodes(oSettings) ).each( function () {
		aData.push( $(this).val() );
	} );
	return aData;
}

/* Create an array with the values of all the input boxes in a column */
$.fn.dataTableExt.afnSortData['dom-text'] = function  ( oSettings, iColumn )
{
	var aData = [];
	$( 'td:eq('+iColumn+') input', oSettings.oApi._fnGetTrNodes(oSettings) ).each( function () {
		aData.push( this.value );
	} );
	return aData;
}

function columnMaager(){
	var showFlag1 = false;
	$('#targetall').hide();
	$('#tableall').columnManager({listTargetID:'targetall', onClass: 'advon', offClass: 'advoff', hideInList: [1,4,5], 
									saveState: true, colsHidden: [1,3]});
	$('#ulSelectColumn').click( function(){
		  if(showFlag1){
			$('#targetall').fadeOut();
			showFlag1 = false;
		  }else{
			  $('#targetall').fadeIn();
			  showFlag1 = true;
		  }
		});
}

function setTmpValues(){
	// set tmp values
	$("#tmptotalVolume").val( $("#totalVolume").val());
	 $("#tmpdilution").val($("#dilution").val());
	 $("#tmpweight").val($("#weight").val());
	 $("#tmphct").val($("#hct").val());
	 $("#tmpsample1").val($("#sample1").val());
	 $("#tmpsample2").val($("#sample2").val());
	 $("#tmphgb").val($("#hgb").val());
	 $("#tmptnc").val($("#tnc").text());
}
 $(function(){
     $("select").uniform(); 
  }); 
function setBufferValues(){
	$("#totalVolume").val( $("#tmptotalVolume").val());
	 $("#dilution").val($("#tmpdilution").val());
	 $("#weight").val($("#tmpweight").val());
	 $("#hct").val($("#tmphct").val());
	 $("#sample1").val($("#tmpsample1").val());
	 $("#sample2").val($("#tmpsample2").val());
	 $("#hgb").val($("#tmphgb").val());
	 fn_calculation();
	}
function closeEditImmuno()
{
	showPopUp("close","immnophenotypingDialog","","","");
	//$("#acquisitionNewForm").close();
	}
//alert("test");
function addtxt(){
	$('#deleteallchild').attr('checked', false);
	$("#addtxt").show();
	$("#addtxt").html('<table><tr><td># of Children</td> &nbsp;&nbsp;&nbsp;<td><input type=text  id=norow style=\"width:40%\"  onkeypress="createcols(event);"/></td></tr></table>');
		}
 function createcols(e){	
	 if(e.keyCode==13){
		Createchildrows();
		e.returnValue = false;
		return false;
		}
	} 
 function checkall() { 
	var isChecked = $('#deleteallchild').attr('checked');
 	if(isChecked)
 		{
	 $('.deleterows').attr('checked', true);
 		}
 	else
 		{
 		$('.deleterows').attr('checked', false);	
 		}
 }
 var dynoptions = Array();
 //var header = Array()
function Createchildrows(){
	 var Somewar="";
	var i=0; 
	var n=$("#norow").val();
	var dynrow=$('#childcreation tbody').find('tr').length;
	var runningTabClass= $('#childcreation tbody tr td:nth-child(1)').attr("class");
	//alert("dynrow"+dynrow)
	if(dynrow!=1){
		dynrow++;
	}
	
	for(i;i<n;i++){
		Somewar+=("<tr>"+ 
				"<td style=\"width:7%\">&nbsp;&nbsp;</td>"+
		        "<td style=\"width:4%\"><input type='checkbox' class='deleterows'/></td>"+
		        "<td style=\"width:6%\"><label>BMT123-"+dynrow+"</label></td>"+
		        "<td style=\"width:6%\"><div><select  style=\"width:50%\"><option>Select</option><option>Supernatant</option><option>RBC</option></select></div></td>"+
				"<td style=\"width:6%\"><div><select  style=\"width:50%\"><option>Select</option><option>Vail</option><option>Bag</option></select></div></td>"+
				"<td style=\"width:6%\"><input type=text  id=volid_"+dynrow+" style=\"width:75%\"/></td>"+
				"<td style=\"width:6%\"><input type=text  id=wgtid_"+dynrow+" style=\"width:75%\"/></td>"+
				"<td style=\"width:6%\"><input type=text  id=tncid_"+dynrow+" style=\"width:75%\"/></td>"+
				"<td style=\"width:6%\"><input type=text  id=tncids_"+dynrow+" style=\"width:75%\"/></td>"+
				"<td style=\"width:6%\"><input type=text  id=cdeid_"+dynrow+" style=\"width:75%\"/></td>"+
			    "<td style=\"width:6%\"><input type=text  id=cdekg_"+dynrow+" style=\"width:75%\"/></td>"+ 
				"<td style=\"width:6%\"><div><select style=\"width:50%\"><option>Select</option><option>Vol.Red.</option><option>Plasma Depletion</option><option>MNC</option><option>Wash</option><option>Buffy Coat</option><option>Cryo-prep</option><option>Release</option></select></div></td>"+
				"<td style=\"width:6%\"><input type=text  id=volid_"+dynrow+" style=\"width:75%\"/></td>"+
				"</tr>");
		dynrow++;
					}
				/* $("#childcreation thead tr th").each(function(i, v){
		        header[i] = $(this).attr('style');
		        	if(header[i].indexOf("display: none") >0){
		    	   $('.tbody').find('tr').children('td:nth-child('+(i+1)+')').hide();
		       }         	        
			 });  */
			  if(runningTabClass=="dataTables_empty"){
			 $('#childcreation tbody tr:first').remove(); 
			 }	  
				 $('#childcreation tbody').append(Somewar); 
				 $("select").uniform(); 
	$("#addtxt").hide();
    nchildrens();
    nextprotopt();
	}
	//add to nextProtocolList
	function nextprotopt(){
		var dynrow=$('#childcreation tbody').find('tr').length;
		var i=0;
		var txtval=$('#norow').val();
		i=dynrow;	
		for(i=0;i<txtval;i++){
			$('#nextProtocolList').append("<option title='newadd'>"+dynoptions[i]+"</option>");
		}	
	} 
	var oTable1= $('#childcreation').dataTable();
	function deleteallchild(){
		 var dynrow=$('#childcreation tbody').find('tr').length;
    	 if($('#childNo').attr('checked')){
    		 if(dynrow>1){  
    			 $('#deleteallchild').attr('checked', true);
    		checkall();
    		deletechildrows('childcreation');
        	 //$('.deleterows').attr('checked', false);
        	 $('#createchild'). find( ".portlet-header .ui-icon" ).trigger("click");
        	 $("#childDiv").hide();
    		 return;   		 
    		 }
    	 }
        else{
        		 $('input[name=Yeschild]').attr('checked', false);
        		 $('#childYes').attr('checked', true);
        		 $("#childDiv").show();
        		 return;
        }
    		 $("#childDiv").hide();
    		 $("#nextProtocolList option[title='newadd']").remove(); 
        	 $("#nextdropdown").find("tbody").find("tr").find("td").find("#uniform-nextProtocolList").find("span").text("Select");
    }
    	
 function deletechildrows(id) {
	 var table = document.getElementById(id);
		try {
		   	var isChecked = $('#childcreation tbody').find('tr').length;
		   	var isCheckedall = $('#deleteallchild').attr('checked');
		   	if(isChecked>=1 && $('.deleterows').is(':checked')){
		   	if(isCheckedall){
		   		var yes=confirm('Do you want delete all products ?');	
		   	}
		   	else{var yes=confirm('Do you want delete selected products?');
		   	}
		   	var rowCount = $('#childcreation').find('tr').length;
		    if(yes){
		    	$('#childcreation tbody tr').each(function(i,v){
						var cbox=$(v).children("td:eq(1)").find(':checkbox').is(":checked");
						if(cbox){
							//oTable1.fnDeleteRow(this);
							$(this).remove();
						}
				});
			 $('.deleterows').attr('checked', false);
			$("#nextProtocolList option[title='newadd']").remove();
			$("#nextdropdown").find("tbody").find("tr").find("td").find("#uniform-nextProtocolList").find("span").text("Select");
		    }
		}
		     //rowCount = table.rows.length;
		     for(var i=1; i<rowCount; i++) {
					var row = table.rows[i];
			        var cbox = row.cells[2];
			        var labelText = $(cbox).find('label').text();
			        if(labelText!="" && labelText!=null && labelText!="undefined"){
			        	var indexL = labelText.indexOf('-');
			        	labelText = labelText.substr(0,indexL+1);
			        	var temp=i;
			        	temp=temp-1;
			        	$('#nextProtocolList').append("<option title='newadd'>"+labelText+temp+"</option>"); 
			        	labelText= labelText+temp;
			        	$(cbox).find('label').text(labelText);
			        }   
		    }
	    }catch(e) {
	       // alert(e);
	    }
	    $('#deleteallchild').attr('checked', false);
	    nchildrens();
	 }  
 var dynrows;
 function nchildrens(){
	 dynrows=$('#childcreation tbody').find('tr').length;
	 var len=$("#childcreation").find("tbody").find("tr").find("td").text();
	 if(len=='No data available in table')
		 {dynrows=0;
		 $("#nchild").html('<b>Total No of Children</b> &nbsp;'+'<b>'+dynrows+'</b>');
		 }
	  if(dynrows>=2)
		 {
		  $("#nchild").html('<b>Total No of Children</b> &nbsp;'+'<b>'+dynrows+'</b>');
		 } else {
	 $("#nchild").html('<b>Total No of Children</b> &nbsp;'+'<b>'+dynrows+'</b>');
		 }
 }
 function getScanData(event,obj){
		if(event.keyCode ==13){
			getProcessingData(event);
		}
	}
</script>


<!--float window-->

<div id="floatMenu">
<div id="forfloat_wind">
	<div id="box-outer">
		
		<a href="#" style="text-decoration:none" id="blind">&lt;&lt; </a>
			<div id="box">
				<div id="leftnav" >
				
<!-- recipient start-->
					<div class="column">
					<div  class="portlet">
					<div class="portlet-header notes"><s:hidden id="moduleName" value="originalProduct"/><s:hidden id="widgetName" value="recipient"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.recipient"/></div>
						<div class="portlet-content">
						<table width="100%" border="0">
						  <tr>
							<td width="50%"><s:text name="stafa.label.id"/></td>
							<td width="50%"><div id="recipientId"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.name"/></td>
							<td><div id="recipientName"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientaborh"/></td>
							<td><div id="recipientAboRh"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientdiagnosis"/></td>
							<td><div id="recipientDiagnosis"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientrid"/></td>
							<td><div id="recipientRID"></div></td>
						  </tr>
						</table>

					</div>
					</div>
					</div>
				
<!-- recipient-->

<!-- Product start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header notes"><s:hidden id="moduleName" value="originalProduct"/><s:hidden id="widgetName" value="product"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.product"/></div>
						<div class="portlet-content">
						<table width="100%" border="0">
							<tr>
							<td width="50%"><s:text name="stafa.label.id"/></td>
							<td width="50%"><div id="productId"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.type"/></td>
							<td><div id="productType"></div></td>
						  </tr>
						</table>	

					
					</div>
				</div>
				</div>
<!-- Product-->

<!-- Donor start-->
					<div class="column">
					<div class="portlet">
						<div class="portlet-header notes"><s:hidden id="moduleName" value="originalProduct"/><s:hidden id="widgetName" value="donor"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.donor"/></div>
						<div class="portlet-content">
							<table width="100%" border="0">
								<tr>
									<td width="50%"><s:text name="stafa.label.id"/></td>
									<td width="50%"><div id="donorId"></div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.name"/></td>
									<td><div id="donorName"></div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.donoraborh"/></td>
									<td><div id="donorAboRh"></div></td>
								</tr>
							</table>


						</div>
					</div>
					</div>
					<!-- Donor-->
				</div>
				</div>
			</div>
		</div>
</div>
<!--float window end-->

<!--right window start -->	
<div id="forfloat_right">
<form id="originalProductForm">
<input type="hidden" name="originalProduct" id="originalProduct" value=""/>
<s:hidden id="newProductId" name="newProductId" value="%{newProductId}"/>
<s:hidden name="fkProtocol" id="fkProtocol" value="%{fkProtocol}"/>
<s:hidden id="immuno" name="immuno" value="immuno"/>
<s:hidden id="immunoCode" name="immunoCode" value=""/>
	<div class="column">

		<div  class="portlet">
		<div class="portlet-header notes"><s:hidden id="moduleName" value="originalProduct"/><s:hidden id="widgetName" value="nextProcess"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.nextprocess"/></div>
			
		<div class="portlet-content">

			<div id="table_inner_wrapper">
			<div id="left_table_wrapper">			
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
					<tr>
						<td width="62%" height="40px"><div class="txt_align_label" align="left">Date of Removal from Previous Step</div></td>
						<td width="38%" height="40px"><div align="left"><input type="text" id="originalDateStr" name="dateOfRemoval "placeholder="Date of Removal" class="dateEntry"/></div></td>
					</tr>
					<tr>
						<td width="62%" height="40px"><div class="txt_align_label" align="left">Time of Removal from Previous Page</div></td>
						<td width="38%" height="40px"><div align="left"><input type="text" id="timePreviousPro" name="timeOfRemoval1" class="timepickers" placeholder="Time of Removal" value=""/></div></td>
					</tr>
					<tr>
						<td width="62%" height="40px"><div class="txt_align_label" align="left">Removed By</div></td>
						<td width="38%" height="40px"><div  class="mainselection" align="left"><select><option>Select</option><option>Larry Smith</option></select></div></td>
					</tr>						
				</table>			
			</div>			
			<div id="middle_line"></div>
			</div>
		</div>
		</div>
		
		<div  class="portlet">
		<div class="portlet-header notes"><s:hidden id="moduleName" value="originalProduct"/><s:hidden id="widgetName" value="originalBag"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.originalbag"/></div><br>
			<div class="portlet-content">
			<div id="button_wrapper">
			<div style="margin-left:40%;">
			<input class="scanboxSearch" type="text" size="15" value=""  name="productSearch" autcomplete="off" id="conformProductSearch" placeholder="Scan/Enter Product ID"/>
			<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" />	
<!-- 			<input class="scanboxSearch" type="text" size="15" value="" name="productSearch" id="productSearch" onkeyup="getProcessingData(event);"  placeholder="Scan/Enter Product ID"/>
 -->			</div>
			<input type="text"  id="emptyId" onkeyup="" />
			</div>
				<div id="table_inner_wrapper">
					<div id="left_table_wrapper">					
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.accessingstation"/></div></td>
								<td width="50%" height="40px"><div  class ="mainselection" align="left"><s:select headerKey="" headerValue="Select"  list="accessingstationList" listKey="pkCodelst" listValue="description" name="refAccessingStation" id="refAccessingStation"/></div></td>
							</tr>
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left">Processing Station Cleaning</div></td>
								<td width="50%" height="40px"><div align="left"><a href="#" id = ""><input type="checkbox" id="" name="" class=""/></a></div></td>
							</tr>							
						</table>					
						
					</div>
										
					<div id="right_table_wrapper">					
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.accessinghood"/></div></td>
								<td width="50%" height="40px"><div class="mainselection" align="left"><s:select headerKey="" headerValue="Select"  list="accessinghoodList" listKey="pkCodelst" listValue="description" name="refAccessingHood" id="refAccessingHood"/></div></td>
							</tr>
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left">Processing Hood Cleaning</div></td>
								<td width="50%" height="40px"><div align="left"><a href="#" id = ""><input type="checkbox" id="" name="" class=""/></a></div></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
	<div class="cleaner"></div>
	
	
<div class="cleaner"></div>

<form id="frmQC">
 <input type="hidden" name="selectedQC" id="selectedQC" value="" />
 <input type="hidden" name="qctype" id="qctype" value="" />
 
<div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes info"><s:hidden id="moduleName" value="originalProduct"/><s:hidden id="widgetName" value="QC"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.qc"/></div>
			<!--<div id="button_wrapper"><input class="scanboxSearch" type="text" size="25" placeholder="Scan and Enter ID" name="search" /></div>-->
			<div id="qcContent" class="portlet-content">
			
			</div>
			<div style="border-bottom:2px solid black">
			</div>
			<div id="immunContent" class="portlet-content"></div>
			<div>
			<table>
				<tr>
					<td>
						<!-- <label><b>Selected Qc:</b></label>
						<input type="text" id="qcSelect"/>
						<input type="button" value="Edit" onclick='getImmnoPhenoList()'/> -->
					</td>
				</tr>
			</table>
			</div>
			
		</div>
	</div>
</form>		
	
<section>

 <div class="cleaner"></div>
 <form>
 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value="originalProduct"/><s:hidden id="widgetName" value="reagentsandSupplies"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.regandandsupplies"/></div>
			<div class="portlet-content">
			<div id="reagentsdiv">
			<table cellpadding="0" cellspacing="0" border="0" id="reagentsTable" class="display">
					<thead>
						<tr>
							<th colspan="1"><s:text name="stafa.label.regandandsupplies"/></th>
							<th colspan="1"><s:text name="stafa.label.quantity"/></th>
							<th colspan="1"><s:text name="stafa.label.manufacturer"/></th>
							<th colspan="1"><s:text name="stafa.label.lot"/></th>				
							<th colspan="1"><s:text name="stafa.label.expirationdate"/></th>
							<th colspan="1"><s:text name="stafa.label.appearanceok"/></th>
						</tr>
						<tr>
							<th></th>
							<th></th>
							<th></th>
							<th></th>				
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<s:iterator value="reagentsList" var="qcObject" status="row">
						<tr>
							<td><input type="hidden" id="reagentSupplies" value="<s:property value="reagentSuppliers"/>"/><s:property value="qcDesc"/></td>
							<td><s:property value="qcQuanitiy"/></td>
							<td><s:property value="qcManufacturer"/></td>
							<td><s:property value="qcMFCode"/></td>
							<td>&nbsp;</td>
							<td align="center">
							<input type="radio" id="yes" name="apprance[<s:property value="%{#row.index}"/>]" class=""/> Yes &nbsp; 
							<input type="radio" id="No" name="apprance[<s:property value="%{#row.index}"/>]" onClick="confirmIfNo();" class=""/> N
							</td>
						</tr>
					</s:iterator>
					</tbody>
			</table>
			</div>
	</div>
	</div>	
</div>		
</form>		
</section>	


<section>

 <div class="cleaner"></div>
 <form>
 <div class="column">
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value="originalProduct"/><s:hidden id="widgetName" value="Equipments"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.equipment"/></div>
			<div class="portlet-content">
			<div id="equipmentdiv">
			<table cellpadding="0" cellspacing="0" border="0" id="equipmentsTable" class="display">
				<thead>
						<tr>
							<th colspan="1"><s:text name="stafa.label.equipment"/></th>
							<th colspan="1"><s:text name="stafa.label.manufacturer"/></th>
							<th colspan="1"><s:text name="stafa.label.serial"/></th>
							<th colspan="1"><s:text name="stafa.label.expirationdate"/></th>				
							<th colspan="1"><s:text name="stafa.label.appearanceok"/></th>
						</tr>
						<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						</tr>
					</thead>
					<tbody>
					<s:iterator value="equipmentList" var="equipment">
						<tr>
							<td><s:property value="equipment"/></td>
							
							<td>Fisher</td>
							<td>908763</td>
							<td>Jul 20, 2015</td>
							<td align="center"><input type="radio" id="yes" name="yes" class=""/> Y &nbsp; <input type="radio" id="No" onClick="confirmIfNo();" name="no" class=""/> N</td>
						</tr>
					</s:iterator>			
					</tbody>
			</table>
			</div>
	</div>
	</div>	

</div>		
</form>		
</section>	


<div class="cleaner"></div>

 <form id="frmCalculation">
<div class="column">		
		
				<div id="calculationDiv" >
				</div>

	</div>
</form> 		
	
<div class="cleaner"></div>
<form>
  <div class="column">	
		<div  class="portlet" id="createchild">
			<div class="portlet-header notes addnew" ><s:hidden id="moduleName" value="originalProduct"/><s:hidden id="widgetName" value="childgeneration"/><s:hidden id="pkNotes" value=""/>Child Generation &nbsp;<input type="radio" id="childYes" name="Yeschild" onclick="showHide(this);" class=""/> Y &nbsp; <input type="radio" id="childNo" name="Yeschild" class="" onclick="showHide(this);" />N</div>
			<div class="portlet-content" id="childDiv">
			<table><tr>
			<td><img src = "images/icons/addnew.jpg" style="width:16;height:16;cursor:pointer;" onclick="addtxt();"/>&nbsp;&nbsp;<label id=""><b>Add Child</b></label></td>
			<td><div id="addtxt" style="width:180px;"></div></td>&nbsp;&nbsp;<td><div><img src = "images/icons/no.png" style="width:16;height:16;cursor:pointer;" id ="" onclick="deletechildrows('childcreation');"/>&nbsp;&nbsp;<label id=""><b>DeleteChilld</b></label></div></td>
			<td width="27%" height="40px"></td>
			<td width="25%" height="40px"><div id="nchild" align="right"></div></td>
			</tr></table>
			<div style="overflow-x:auto;overflow-y:hidden;">
			<table cellpadding="0" cellspacing="0" border="1" id="childcreation" class=" createchilddisplay ">
			<thead>
			<tr>	
							<th width="4%" id="childColumn"></th>
							<th colspan="1">Check all<input type="checkbox" name="" id="deleteallchild" onclick="checkall();"/></th>
							<th colspan="1">ProductId</th>
							<th colspan="1">Component</th>
							<th colspan="1">Container</th>
							<th colspan="1">Volume (ml)</th>
							<th colspan="1">Weight (kg)</th>
							<th colspan="1">TNC (*10^8)</th>
							<th colspan="1">TNC/kg (10^6/kg)</th>				
							<th colspan="1">CD34 (*10^6)</th>
							<th colspan="1">CD34/kg (*10^6/kg)</th>
							<th colspan="1">Intended Process</th>
							<th colspan="1">Comments</th>
			</tr>
				<tr>
							<th width="4%" id="childColumn"></th>
							<th>Check all</th>
							<th>ProductId</th>
							<th>Component</th>
							<th>Container</th>
							<th>Volume (ml)</th>
							<th>Weight (kg)</th>
							<th>TNC (*10^8)</th>
							<th>TNC/kg (10^6/kg)</th>				
							<th>CD34 (*10^6)</th>
							<th>CD34/kg (*10^6/kg)</th>
							<th>Intended Process</th>
							<th>Comments</th>
			</tr>
				</thead>			
			</table>
			</div>
	</div>
	</div>	
</div>
</form>
<%-- <div class="column" >		
		<div  class="portlet">
			<div class="portlet-header notes child" id="childCreateDiv" ><s:text name="stafa.label.childcreated" /></div>
			<div id="childCreateDiv" ><b>&nbsp;<s:text name="stafa.label.createchild" /><input type="radio" id="childYes" name="Yeschild" onclick="showHide(this);" class=""/> Y &nbsp; <input type="radio" id="childNo" name="yeschild" class="" onclick="showHide(this);" /> N  &nbsp;<s:text name="stafa.label.numchild" /></b><select name="Numchild" id="creation">
<!-- 			<option value="1">1</option> -->
<!-- 			<option value="2">2</option> -->
<!-- 			<option value="3">3</option> -->
<!-- 			<option value="4">4</option> -->
<!-- 			</select> -->
<!-- 			</div> -->
			<div id="childCreateDiv" ><b>&nbsp;<s:text name="stafa.label.createchild" /><input type="radio" id="childYes" name="Yeschild" onclick="showHide(this);" class=""/> Y &nbsp; <input type="radio" id="childNo" name="yeschild" class="" onclick="showHide(this);" /> N &nbsp;
			</div>
			<div class="portlet-content" id="childDiv" >
			<b><s:text name="stafa.label.numchild" /> &nbsp; <input type="text" id="number"/></b>
			<br><s:text name="stafa.label.childdiv"/>	
			<table cellpadding="0" cellspacing="0" border="0" id="parentProductTable" class="display">
					<thead>
						<tr>
							<th><s:text name="stafa.label.productid"/></th>
							<th><s:text name="stafa.label.volume"/></th>
							<th><s:text name="stafa.label.tnc"/></th>
							<th><s:text name="stafa.label.cd3"/></th>				
							<th><s:text name="stafa.label.cd34"/></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>BMT123-3</td>
							<td>303</td>
							<td>1.5</td>
							<td>0.6</td>
							<td>0.4</td>
						</tr>
					</tbody>
			</table>
			<form>
			<b><s:text name="stafa.label.scanproduct" /></b>
			<table cellpadding="0" cellspacing="0" border="0" id="scannedproductTable" class="display">
					<thead>
						<tr>
							<th><s:text name="stafa.label.bag"/></th>
							<th><s:text name="stafa.label.productid"/></th>
							<th><s:text name="stafa.label.volume"/></th>
							<th><s:text name="stafa.label.tnc"/></th>
							<th><s:text name="stafa.label.cd3"/></th>				
							<th><s:text name="stafa.label.cd34"/></th>
							<th><s:text name="stafa.label.plannedprocedure"/></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>BMT-123-3-1</td>
							<td>100</td>
							<td>0.5</td>
							<td>0.6</td>
							<td>0.4</td>
							<td><select name="child" id="plannedprocedure">
								<option value="selection">Selection...</option>
								<option value="Cryoprep">Cryoprep</option>
								<option value="Vol.Red.">Vol.Red.</option>
								<option value="Plasma Depletion">Plasma Depletion</option>
								<option value="Release">Release</option>
								</select></td>
						</tr>
						<tr>
							<td>2</td>
							<td>BMT-123-3-2</td>
							<td>102</td>
							<td>0.5</td>
							<td>0.6</td>
							<td>0.4</td>
							<td><select name="child" id="plannedprocedure">
								<option value="selection">Selection...</option>
								<option value="Cryoprep">Cryoprep</option>
								<option value="Vol.Red.">Vol.Red.</option>
								<option value="Plasma Depletion">Plasma Depletion</option>
								<option value="Release">Release</option>
								</select></td>
						</tr>
						<tr>
							<td>2</td>
							<td>BMT-123-3-1</td>
							<td>101</td>
							<td>0.5</td>
							<td>0.6</td>
							<td>0.4</td>
							<td><select name="child" id="plannedprocedure">
							<option>Selection...</option>
							<option value="Cryoprep">Cryoprep</option>
							<option value="Vol.Red.">Vol.Red.</option>
							<option value="Plasma Depletion">Plasma Depletion</option>
							<option value="Release">Release</option>
							</select></td>
						</tr>							
					</tbody>
			</table>
			</form>
			</div>
		</div>
	</div> --%>

<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
				<table cellpadding="0" id="nextdropdown"cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td>Next:</td>
				<td><div class="mainselection" ><s:select headerKey="" headerValue="Select"  listKey="protocol" listValue="protocolName" list="prodprotocolList" name="fkProtocol" id="nextProtocolList"/></div></td>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" value="Next" onClick="nextProcess();"/></td>
				</tr>
				</table>
				</form>
		</div>
		
</section>
</div>
<!-- 
<div id= "immnophenotypingDialog1" style="display: none">
<fieldset>
            <legend>Immnophenotyping</legend>
	       <table id = "" width="600px" border="0" cellpadding="0" cellspacing="0">
		   		<tbody>
						<tr>
							<td><input type="radio" id="immnophenotyping" name="immnophenotyping" />&nbsp;Short Panel Verson 2</td>
							<td rowspan="5" align="right"><fieldset>
					            <legend>Short Panel Verson 2</legend>
					            <table>
					            	<tr>
					            	<td>Mononuclear:</td>
					            	<td>% Region Analyzed</td>
					            	</tr>
					            	<tr>
					            	<td>&nbsp;</td>
					            	<td>% CD 34</td>
					            	</tr>
					            	<tr>
					            	<td>&nbsp;</td>
					            	<td>___________________</td>
					            	</tr>
					            	<tr>
					            	<td>Lymphocyte:</td>
					            	<td>% Region Analyzed</td>
					            	</tr>
					            	<tr>
					            	<td>&nbsp;</td>
					            	<td>% CD 3</td>
					            	</tr>
					            	
					            </table>
					            </fieldset>
            </td>
						</tr>
						<tr><td><input type="radio" id="immnophenotyping" name="immnophenotyping" />&nbsp;Long Panel Verson 2</td></tr>
						<tr><td><input type="radio" id="immnophenotyping" name="immnophenotyping" />&nbsp;Lymphoma Panel Verson 2</td></tr>
						<tr><td><input type="radio" id="immnophenotyping" name="immnophenotyping" />&nbsp;Meyloma Panel Verson 2</td></tr>
						<tr><td><input type="radio" id="immnophenotyping" name="immnophenotyping" />&nbsp;T-Cell Panel Verson 2</td></tr>
						
					</tbody>
			</table>
	</fieldset>
</div>
 -->
<div id= "immnophenotypingDialog" style="display: none">
	<table  border="0" width="100%">
			<tr>
				<th style="font-weight:bold">Select Immunophenotyping Panel</th>
			</tr>
			<tr>
				<td><div style="border-bottom:2px solid black;width:100%"></div></td>
			</tr>
			
	</table>
	<table border="0" width="100%">
	<tr>
				<td width="50%">
					<table height="100%" width="100%" bgcolor="#fff" style="border-radius:0px;border-right:2px solid black">
						<tr>
							<th style="font-weight:bold">Select Panel</th>
						</tr>
						<tr>
							
							<td>
								<div id="immnoMainList""></div>
							</td>
						</tr>
					</table>
				</td>
				
				<td width="50%">
					<table height="98%" width="100%" bgcolor="#fff" style="border-radius:0px;">
						<tr>
							<th style="font-weight:bold;">Panel Details</th>
						</tr>
						<tr>
							<td>
								<table height="100%" width="100%" bgcolor="#DDD" style="border-radius:0px;"> 
									<tr>
										<td><div id="immnoSubColumn" style="display:none"></div></td>
									</tr>
									<tr>
										<td>
											<div id="immnoSubLegend" align="left"></div>
										</td>
									</tr>
									<tr>
									<td>
										<div id="immnoSubList"></div>
									</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>	
				
			</tr> 
			
	</table>
<!-- <fieldset>
           
						            <table>
						            	<tr>
						            	<td>Mononuclear:</td>
						            	<td>% Region Analyzed</td>
						            	</tr>
						            	<tr>
						            	<td>&nbsp;</td>
						            	<td>% CD 34</td>
						            	</tr>
						            	<tr>
						            	<td>&nbsp;</td>
						            	<td>___________________</td>
						            	</tr>
						            	<tr>
						            	<td>Lymphocyte:</td>
						            	<td>% Region Analyzed</td>
						            	</tr>
						            	<tr>
						            	<td>&nbsp;</td>
						            	<td>% CD 3</td>
						            	</tr>
						            	
						            </table>
						            
					            
	</fieldset> -->
	<div align="right" style="float:right;">
		<form>
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td>
					<input type="button" id="immuneOk" value="Save" onClick="getSelected()"/>
					<input type="button" value="Cancel" onClick="closeEditImmuno()"/>
				</td>
				</tr>
				</table>
				</form>
		</div>
	</div>
