<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="common/includes.jsp"%>
<script type="text/javascript" src="js/ddimgtooltip.js"></script>
<script type="text/javascript" src="js/placeholder.js"></script>

<script type="text/javascript" src="js/jquery/jquery.Print.js"></script>
<script type="text/javascript" src="js/jquery/jquery.printElement.js"></script> 
<link type="text/css" href="css/ddimgtooltip.css" rel="stylesheet" media="screen"/>
<link type="text/css" href="css/ddimgtooltip.css" rel="stylesheet" media="print"/>
<script type="text/javascript" src="js/smartspinner.js"></script>
<script type="text/javascript" src="js/dataSearch.js"></script>
<link type="text/css" href="css/ui/jquery-ui-tab.css" rel="stylesheet" media="screen"/>
<link type="text/css" href="css/ui/jquery-ui-tab.css" rel="stylesheet" media="print"/>
<!--//?BB <script type="text/javascript" src="js/jquery/jquery-ui.js"></script>
-->
<script type="text/javascript" src="js/jquery/jquery.loadjson.js"></script>

<%-- <script type="text/javascript" src="js/barcodeData.js"></script>
 --%><link type="text/css" href="css/smartspinner.css" rel="stylesheet" media="print"/>
 <link type="text/css" href="css/smartspinner.css" rel="stylesheet" media="screen"/>
<style type="text/css" media="print">
/* css for timepicker */
.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
.ui-timepicker-div dl { text-align: left; }
.ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
.ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }
.bar{width:400px;height:30px;}

#pro
{
width:0px;
height:5px;
background-color:blue;
}
.input {  border: 0px solid #006;background-color:white;cursor:pointer;cursor:hand;text-decoration:underline;color:blue}
.labelinline{display:inline;}
ul.men li a{
margin-right:30px;
}
ul.men li{
display:inline;
}

</style>

<script>
var docsflag;
function getDocumentAdditives(formId,divId,Type){
	var pkSpecimenProduct = $("#pkSpecimenProduct").val();
    $("#columnType").val(Type);
    if(pkSpecimenProduct==null || pkSpecimenProduct=="" || pkSpecimenProduct<=0){
        alert("Please scan/enter the product ");
        docsflag = false;
        return false;
        }
	var url = "loadDocumentAdditives";
	var result = ajaxCall(url,formId);
	$("#"+divId).replaceWith($('#'+divId, $(result)));
	docsflag = true;
}

function saveDocumentAdditives(docuType,tableId,divId,Tablebody){
	var documentAdditive;
	var column1;
	var column2;
	var rowData="";
    var tbdy = document.getElementsByName(Tablebody);
	var columns = ["documentAdditives","column1","column2"];
    var obj = getJsonFromHtmlTable(tbdy,columns);
    var data = "{panelData:" + JSON.stringify(obj) +"}";

	var refProduct = $("#pkSpecimenProduct").val();
    if(refProduct==null || refProduct=="" || refProduct<=0){
        alert("Please scan/enter the product ");
        return;
        }
    /*var jsonData = "{jsonData:"+data+",refProduct:"+refProduct+",columnType:"+docuType+"}";
    var result = jsonDataCall("saveDocumentsAdditives",jsonData);*/
    $.ajax({
        type: "POST",
        url: "saveDocumentsAdditives.action",
        async:false,
        data :{jsonData:data,refProduct:refProduct,columnType:docuType},
        dataType: 'json',
        success: function (result){
            //$('#recipientAlias').dialog('close');
             alert("Data saved succesfully.");
            jQuery("#"+divId).dialog("close");
            
        }
    });
    
}
 
 
	
workFlowFlag = false;
function searchProductCodeDescription(This){
    var barcodeClass = $("#barcodeClass").val();
    var modifier = $("#modifier").val();
    var manipulation = $("#manipulation").val();
    var cryoprotectant = $("#cryoprotectant").val();
    var collectionVolume = $("#collectionVolume").val();
    var anticoagulantType = $("#anticoagulantType").val();
    var storageTemprature = $("#storageTemprature").val();
    var productDesc = "";
    var coreCondition = "";
    var groupVariables = "";
    var finalProductDescCode="";
    if(modifier!=null && modifier!=""){
        productDesc+=modifier+" "+barcodeClass;
    }else{
        productDesc+=barcodeClass;
    }

    if(anticoagulantType!=null && anticoagulantType!=""){
        coreCondition+="|"+anticoagulantType;
    }

    if(storageTemprature!=null && storageTemprature!=""){
        coreCondition+="/XX/<"+storageTemprature;
    }

    if(cryoprotectant!=null && cryoprotectant!=""){
        coreCondition+="|"+cryoprotectant;
    }

    if(productDesc!="" && coreCondition!=""){
        finalProductDescCode =productDesc+coreCondition;
        }else{
            finalProductDescCode =productDesc;
            }
    //alert(finalProductDescCode);
   
    $('#productDescriptionInput').val(finalProductDescCode);
    $('#productDescriptionInput').keyup();
    $('#productDescriptionInput').keypress();
    $('#productDescriptionInput').keydown();
}


   
$(function(){

            // Hook up the print link.
            $( "#printBarcodeTemplate" ).click(
                    function(){
                        // Print the DIV.
                        //alert("s");
                        $( "#barcodeTem" ).print();

                        // Cancel click event.
                        return( false );
                    }
                    )
            ;
         
        });

function loadPopupData(){
    //barcodeTable(false);
/*
    var url = "loadBarcodePopup";
    var response = ajaxCall(url,"");

    $("#barcodeDiv").replaceWith($('#barcodeDiv', $(response)));
    */
    $("#barcodeTem").hide();

    showPopUp('open','barcodeDiv','','600','600');
    
    $("#barcodeDiv").dialog({
           autoOpen: true,
           modal: true,
           width:auto,
           hieght:450,
           close: function(event, ui)  {
                jQuery("#barcodeDiv").dialog("destroy");
          }

	});
   
    
    //$("#modelPopup").html($("#barcodeDiv").html());
    //showPopUp('open','modelPopup1','','1200','600');
}


function setBarcodeSize(This){
    var value = $(This).val();
    if(value=="Partial Horizontal"){

        $("#topLeftQuad").show();
        $("#topRightQuad").show();
        $("#bottomLeftQuad").hide();
        $("#bottomRightQuad").hide();
       
    }else if(value=="Partial Vertical"){
         $("#topLeftQuad").show();
         $("#topRightQuad").hide();
         $("#bottomLeftQuad").show();
         $("#bottomRightQuad").hide();
    }else if(value=="Small Partial"){
        $("#topLeftQuad").hide();
        $("#topRightQuad").hide();
        $("#bottomLeftQuad").show();
        $("#bottomRightQuad").show();
       
    }else if(value=="Vial"){
         $("#topLeftQuad").hide();
         $("#topRightQuad").hide();
         $("#bottomLeftQuad").hide();
         $("#bottomRightQuad").show();
    }else if(value=="Full Label"){
        $("#topLeftQuad").show();
        $("#topRightQuad").show();
        $("#bottomLeftQuad").show();
        $("#bottomRightQuad").show();
    }
}
var alisflag;
function getAliasData(formId,divId,aliasType){
    //divId = "recipientAliasDiv";
    var pkSpecimenProduct = $("#pkSpecimenProduct").val();
    $("#fkCodelstAlisType").val(aliasType);
    if(pkSpecimenProduct==null || pkSpecimenProduct=="" || pkSpecimenProduct<=0){
        alert("Please scan/enter the product ");
        alisflag=false;
        return false;
        }
    $.ajax({
        type: "POST",
        url: "getAliasData.action",
        async:false,
        data:$("#"+formId).serialize(),
        success:function (result){
                    $("#"+divId).replaceWith($('#'+divId, $(result)));
                    alisflag=true;
                },
        error: function (request, status, error) {
            alert("Error " + error);
        }
    });
    return true;
}

function saveAlias(aliasType,aliasName,Id){
    var jsonTable = getJsonFromHtmlTable();
    var tbdy = document.getElementsByName(aliasName);
    //var obj = getJsonFromHtmlTable('recipientAliasTbody');
    var columns = ["pkAlias","aliasId","aliasSource","aliasType"];
    var obj = getJsonFromHtmlTable(tbdy[0],columns);
    var jsonData = "{panelData:" + JSON.stringify(obj) +"}";
    //var finalJsonData = "jsonData="+jsonData);
    //alert(jsonData);
    //return false;
    //alert("save");
    var pkSpecimenProduct = $("#pkSpecimenProduct").val();
    if(pkSpecimenProduct==null || pkSpecimenProduct=="" || pkSpecimenProduct<=0){
        alert("Please scan/enter the product ");
        return;
        }
    //alert("pkSpecimenProduct : "+pkSpecimenProduct);
    //return;
    $.ajax({
            type: "POST",
            url: "saveAlias.action",
            async:false,
            data :{jsonData:jsonData,pkSpecimenProduct:pkSpecimenProduct,fkCodelstAlisType:aliasType},
            dataType: 'json',
            success: function (result){
                //$('#recipientAlias').dialog('close');
                jQuery("#"+Id).dialog("close");
                alert("Data saved succesfully.");
            }
        });
    }
function hideBarcodes1(){
    $("#imgtip0").css("display","none");
    $("#imgtip1").css("display","none");
    $("#imgtip2").css("display","none");
    $("#imgtip3").css("display","none");
}


function setComboValue(val1,comboBoxId){
    //val = val1.replace(/^\s+/,"");       
    val = val1.replace(/^\s+|\s+$/g,"");        //trim left and right spaces
    //alert(val);
    var x = document.getElementById(comboBoxId);
    $("#"+comboBoxId).find("option:contains('"+val+"')").each(function(){
        //alert("["+$(this).text()+"]==["+val+"]");
        //alert("$(this).text() : "+$(this).text()+"--val1 : "+val1);
       
    if( ($(this).text() == val) || ($(this).text()=="Select" && val=="") ) {
        //alert("inside==>["+$(this).text()+"]==["+val+"]");
        $(this).attr("selected","selected");
        //This line added due to new dropdown changes ..Mari..
        //alert("val1 : "+val1);
        $("#uniform-"+comboBoxId+" span").html($(this).text());
        }
    });
}
/*function setComboValue(val1,comboBoxId){
    //val = val1.replace(/^\s+/,"");       
    val = val1.replace(/^\s+|\s+$/g,"");        //trim left and right spaces
    //alert(val);
    var x = document.getElementById(comboBoxId);
    $("#"+comboBoxId).find("option:contains('"+val+"')").each(function(){
        //alert("["+$(this).text()+"]==["+val+"]");
    if( $(this).text() == val ) {
        $(this).attr("selected","selected");
        //This line added due to new dropdown changes ..Mari..
        $("#uniform-"+comboBoxId+" span").html(val1);
        }
    });
}*/

function showDocument(docId,type){
    //alert( " docId " + docId);
    //alert("contextpath : "+contextpath);
    //var url = cotextpath+"/viewdocument.action?docId="+docId;
    var url = contextpath+"/viewdocument.action?docId="+docId;
    //alert(url);
    window.open( url, "Documents", "status = 1, height = 500, width = 800, resizable = 0" );

}

function closepopup(id){
    $('#'+id).dialog("close");
}

$(function(){
    setTimePicker();
    $("#accessionNewForm").validate({
        rules:{
            "recipientId":"required",
            "recipientName":"required",
            "productId":"required"
            },
            messages:{
                "recipientId":" Please Enter Recipient Id",
                "recipientName":"Please Enter Recipient Name",
                "productId":"Please Enter Product Id"
            }
        });
    });
/*var resetFlag=false;

function checkProduct(productSearch){
    var flag=false;
    var productArr = [];
    //var flag1=false;
        $('#acquisitionTable tbody[role="alert"] tr').each(function(i,el) {
        $(this).removeAttr("style");
        var productId = this.cells[11].innerHTML;
        var status = this.cells[13].innerHTML;
        if(productId==productSearch && status=="Acquired"){
            flag = true;
            //resetFlag = true;
        }
        else{
            productArr.push(productId);
        }
      });
        if($.inArray(productSearch, productArr)<0){
             resetFlag = true;
        }
    return flag;
  
}


function getAcquisitionData(e,This){
    var url = "getAcquisitionInfo.action?aquLocation=acqlocation&productType=producttype&finalDisp=finaldispos&donorLoc=location&procSelec=procfacility&productSource=prodsource";
    var productSearch = $("#productSearch").val();
    if(e.keyCode==13 && productSearch!=""){

        var productAvailable = jQuery("#acquisitionTable:contains(\'"+productSearch+"\')").length
        var flag = checkProduct(productSearch);
       
        if(flag){
            alert("Product is already Acquired.");
          
            }
            if(resetFlag){
                ///** Reset the form values*/
              
              
              
             //   return};
           
var resetFlag=false;  
var saveFlag = false;
var validIdFlag = false;
function checkProduct(productSearch){
    var flag=false;
    var productArr = [];
    $('#accessionTable tbody[role="alert"] tr').each(function(i,el) {
        $(this).removeAttr("style");
        var productId = this.cells[3].innerHTML;
        var status = this.cells[7].innerHTML;
        if(productId==productSearch && status!="Acquired"){
            flag = true;
        }
        else{
            productArr.push(productId);
        }
      });
        if($.inArray(productSearch, productArr)<0){
             resetFlag = true;
        }
    return flag;
  
}
function viewAccessionFromJsnList(response){
	 
    //alert(response.accessionViewList);
    $.each(response.accessionViewList, function(i,v) {
    	$("#recipientName").val(v[0]);
    	$("#recipientId").val(v[1]);
    	$("#personDiagnosis").val(v[2]);
    	$("#aboRh").val(v[3]);
    	$("#recipientWeight").val(v[4]);
    	$("#plannedProtocol").val(v[5]);
    	$("#donorName").val(v[6]);
    	$("#donorId").val(v[7]);
    	$("#donorAboRh").val(v[8]);
    	if(v[9]!=null){
    		$("#personDob").val($.datepicker.formatDate('M dd, yy', new Date(v[9])));
        	}else{
        		$("#personDob").val("");
            	}
    	
    	//$("#personDob").val(v[9]);
    	$("#specId").val(v[10]);
    	$("#pkSpecimen").val(v[11]);
    	$("#pkCollection").val(v[12]);
    	$("#fkCodelstFacility").val(v[13]);
    	$("#fkCodelstTimezone").val(v[14]);
    	$("#fkCodelstStatus").val(v[15]);
    	$("#fkCodelstLocation").val(v[16]);
    	$("#collMudCoordinator").val(v[17]);
    	$("#collMudCoordinContact").val(v[18]);
    	$("#collCallMudCoordin").val(v[19]);
    	$("#collCallMudCoordinContact").val(v[20]);
    	$("#pkAccession").val(v[21]);
    	$("#fkCodelstCourier").val(v[22]);
    	if(v[23]!=null){
    		$("#accessionDate").val($.datepicker.formatDate('M dd, yy', new Date(v[23])));
        	}else{
        		$("#accessionDate").val("");
            	}
    	
    	$("#accessionArrivalDate").val(v[24]);
    	$("#accessionTrackingNumber").val(v[25]);
    	$("#fkCodelsTTimeZone").val(v[26]);
    	//$("#fkCodelstAppearance").val(v[27]);
    	if(v[27]==1){fkCodelstAdditives0
    		$("#fkCodelstAppearance1").addClass("on"); 
    		$("#fkCodelstAppearance1").trigger("click");
        }else if(v[27]==null || v[27]==0){
        	$("#fkCodelstAppearance0").addClass("on");
        	$("#fkCodelstAppearance0").trigger("click");
            }
    	//$("#fkCodelstReqDocument").val(v[28]);
    	if(v[28]==1){
    		$("#fkCodelstReqDocument1").trigger("click");
    		$("#fkCodelstReqDocument1").addClass("on"); 
        }else if(v[28]==null || v[28]==0){
        	$("#fkCodelstReqDocument0").trigger("click");
        	$("#fkCodelstReqDocument0").addClass("on");
            }
    	
    	$("#fkCodelstOvernStorage").val(v[29]);
    	$("#fkCodelstComplete").val(v[30]);
    	if(v[30]==1){
    		$("#fkCodelstComplete1").trigger("click");
    		$("#fkCodelstComplete1").addClass("on"); 
        }else if(v[30]==null || v[30]==0){
        	$("#fkCodelstComplete0").trigger("click");
        	$("#fkCodelstComplete0").addClass("on");
            }
    	//$("#fkCodelstAdditives").val(v[31]);
    	if(v[31]==1){
    		$("#fkCodelstAdditives1").trigger("click");
    		$("#fkCodelstAdditives1").addClass("on"); 
        }else if(v[31]==null || v[31]==0){
        	$("#fkCodelstAdditives0").trigger("click");
        	$("#fkCodelstAdditives0").addClass("on");
            }
    	
    	$("#fkCodelstFinalStatus").val(v[32]);
    	$("#fkProduct").val(v[33]);
    	$("#rid").val(v[34]);
    	$("#productStatus").val(v[35]);
    	$("#fkCodlstAccessionLocation").val(v[36]);
    	$("#receivedBy").val(v[37]);
    	//$("#recipientAlias").val(v[38]);
    	//alert(v[38]);
    	if(v[38]==1){
    		$("#recipientAlias1").trigger("click");
    		$("#recipientAlias1").addClass("on"); 
        }else if(v[38]==null || v[38]==0){
        	$("#recipientAlias0").trigger("click");
        	$("#recipientAlias0").addClass("on");
            }
    	    	
    	//$("#donorAlias").val(v[39]);
    	if(v[39]==1){
    		$("#donorAlias1").trigger("click");
    		$("#donorAlias1").addClass("on"); 
        }else if(v[39]==null || v[39]==0){
        	$("#donorAlias0").trigger("click");
        	$("#donorAlias0").addClass("on");
            }
    	//$("#productAlias").val(v[40]);
    	//?BB alert(v[40]);
    	if(v[40]==1){
    		$("#productAlias1").trigger("click");
    		$("#productAlias1").addClass("on"); 
        }else if(v[40]==null || v[40]==0){
        	$("#productAlias0").trigger("click");
        	$("#productAlias0").addClass("on");
            }
    	
    	$("#altProductId").val(v[41]);
    	$("#fkCodelstProductType").val(v[42]);
    	$("#fkCodelstProductsource").val(v[43]);
    	$("#fkCodelstProcessSelection").val(v[44]);
    	$("#refShippingEnvironment").val(v[45]);
    	$("#recipientPk").val(v[46]);
        $("#donorPk").val(v[47]);
		//alert(v[48]+"/ "+v[49]);
		if(v[48]!=null && v[49]!=null && v[48]!="" && v[49]!=""){

			$("#testFrame").hide();
	        var val = '<div><a href="#" onclick="javascript: return showDocument(\''+v[48]+'\',\'doc\');">'+v[49]+'</a></div>';
	        $("#viewAttachment").html(val);
	        $("#viewAttachment").show();
	        }
	        else{
	            $("#testFrame").show();
	            $("#viewAttachment").hide();
	        }
				
    });
    $.uniform.restore('select');
	 $('select').uniform();
}

function viewAccession(specimenProduct,searchProductId){
	//alert("------------------>1specimenProduct : "+specimenProduct+"searchProductId : "+searchProductId)
    $.ajax({
        type: "POST",
        url: "getAccessionDetailsView.action",
        async:false,
        data :{pkSpecimenProduct:specimenProduct,searchProductId:searchProductId},
        dataType: 'json',
        success: function (result){
            //alert(result);
            viewAccessionFromJsnList(result);
        }
    });
}

function searctrigger(e,productSearch){
	
	
    if((e.keyCode==13 && productSearch!="")){
    $("#searchtxt").focus();
    $("#searchtxt").triggerHandler("keyup.DT",['13']);
    //alert("search"+e.which);
    }
    if(e.keyCode ==8 && productSearch!="") {
        $("#searchtxt").trigger(e);
          return false;  
        }else if (e.keyCode !=8 && e.keyCode != 13 ){
            $("#searchtxt").trigger(e);
            return false;
        }
    return false;
}
function getAccessionData(e,This){
    var searchUrl = "checkAccessionData";
    //$("#fake_conformProductSearch").val($("#fake_conformProductSearch").val());
    var productSearch = $("#fake_conformProductSearch").val();
    if((e.keyCode==13 && productSearch!="") || (workFlowFlag==true)){
    	var result = jsonDataCall(searchUrl,"productId="+productSearch);
    	var listLength=result.accessionList.length;
    	if(listLength <= 0){
        	alert("Product ID is not Found");
        	return;
        }
    	validIdFlag =false;
        	$('#searchtxt').val(productSearch);
        	searctrigger(e,productSearch);
        var productAvailable = jQuery("#acquisitionTable:contains(\'"+productSearch+"\')").length
        var flag = checkProduct(productSearch);
        var viewFlag = false;
        //alert("flag :: "+flag);
        if(flag){
       
            var answer = confirm("Product is already Accessed. Do you want to continue to view the records?");
            if (!answer){
                return false;
            }else{
                viewFlag = true;
                saveFlag = true;
                }
           
            //return;
            }   if(resetFlag){
                ///** Reset the form values*/
                  
                 
                  $("#recipientId").val("");
                  $("#recipientName").val("") ;
                  $("#fkCodelstProductsource").val("") ;
                  $("#fkCodelstProductType").val("") ;
                  //return;
                   };
                var availableFlag=0;
                $("#recipientPk").val("");
                   $("#donorPk").val("");
                $('#accessionTable tbody[role="alert"] tr').each(function(i,el) {
                    $(this).removeAttr("style");
                    var productId = this.cells[3].innerHTML;
                    var status = this.cells[7].innerHTML;
                    if((productId==productSearch && status=="Acquired") || (viewFlag==true && productId==productSearch)){
                    		
						availableFlag = true;
						var hiddenValues = $(this).find("#accessionValues").val();
						var accessionValue = hiddenValues.split("-=-");
						$("#pkSpecimenProduct").val(accessionValue[2]);
						$(this).attr("style","background:#33ffff");
						viewAccession($("#pkSpecimenProduct").val(),$("#fake_conformProductSearch").val());
						if((productId==productSearch && status=="Acquired")){
							saveFlag = false;
						}
                      }else if(productId==productSearch && status=="Active"){
                          availableFlag = false;
                          saveFlag = false;                                                       
                       }
                });
        }
    //alert("  productSearch----------final : "+$("#productSearch").val())
    }
function generateTableFromJsnList(response){
    $.each(response.accessionList, function(i,v) {
       
        recipientName = v[0];
        recipientId = v[1];
        donorName = v[2];
        donorId = v[3];
        productId = v[4];
        productSource = v[5];
        productType = v[6];
        productStatus = v[7];
        recipientPk = v[8];
        donorPk = v[9];
        pkSpecimen = v[10];
        pkDocument = v[11];
        fileName = v[12];
       
        processingFacility = "";
        accessTime = "";
       
                /*if(key == "productId"){
                    productId = value;
                }
                if(key == "recipientId"){
                    recipientId = value;
                }*/
               
         $('#accessionTable').dataTable().fnAddData( [
                                    //"<input type='hidden' id='documents"+i+"' value='"+pkDocument+"-"+fileName+"'/><input type='hidden' id='pkSpecimen"+i+"' value='"+pkSpecimen+"'/><input type='hidden' id='hiddId"+i+"' value='"+recipientPk+"-"+donorPk+"'/><input type='hidden' id='donorInfoId"+i+"' value='"+donorId+"-=-"+donorName+"'/>",recipientId,recipientName,productId,productSource,productType,processingFacility,accessTime,productStatus]);
                                    "<input type='hidden' id='accessionValues"+i+"' value='"+pkDocument+"-=-"+fileName+"-=-"+pkSpecimen+"-=-"+recipientPk+"-=-"+donorPk+"-=-"+donorId+"-=-"+donorName+"'/>",recipientId,recipientName,productId,productSource,productType,processingFacility,accessTime,productStatus]);
       
         //alert("s1");
         });
        setColumnManager('accessionTable');
}

function saveAccession(){
    $("#workflowProductId").val("");
    if($("#fake_conformProductSearch").val()=="" || $("#fake_conformProductSearch").length>2){
        alert("Please Enter the Product.");
        $("#fake_conformProductSearch").focus();
        return;
        }
    	var searchUrl = "checkAccessionData";
	    var result = jsonDataCall(searchUrl,"productId="+$("#fake_conformProductSearch").val());
		var listLength=result.accessionList.length;
		if(listLength <= 0){
	    	alert("Product ID is not Found");
	    	return;
	    }
    
    //alert(saveFlag)
	    if(saveFlag){
	    	alert("Product is already Accessed.");
	        return;
	    }
    
    /*if($("#nextProtocolList").val()=="" || $("#nextProtocolList").length>2){
        alert("Please select protocol from ProtocolList");
        $("#nextProtocolList").focus();
        return;
        }
   */
   
    var uploadProductId = $("#pkSpecimenProduct").val();
    var fileUploadFileName = $('#testFrame').contents().find("#fileUpload").val();
    //alert("uploadProductId : "+uploadProductId);
    $('#testFrame').contents().find("#uploadProductId").val(uploadProductId);
    $('#testFrame').contents().find("#fileUploadFileName").val(fileUploadFileName);
    //alert("---uploadProductId : "+$('#testFrame').contents().find("#uploadProductId").val());
    $('#testFrame').contents().find("#fileUploadSubmit").trigger('click');
    //alert("value :"+$('#testFrame').contents().find("#testText").val());
    //return;
   
    var productSearch = $("#productSearch").val();
    if($("#nextOption").val()>0){
    	$("#fkProtocol").val($("#nextOption").val());
    }
    try{
        var flag = checkProduct(productSearch);
        if(flag){
            alert("Product is already Accessed.");
            return;
            }
         $.ajax({
                type: "POST",
                url: "saveAccession.action",
                async:false,
                data : $("#accessionForm").serialize(),
                success: function (result){
                //alert(" success" + $(result).find("#returnMessage").val());
                   var res = $(result).find("#returnMessage").val();
              //  alert("res"+res);
                alert("Data saved succesfully.");
               
                //$("#accessionDiv").replaceWith($('#accessionDiv', $(result)));
                 //alert($('input:radio[name=fkCodelstOvernStorage]:checked').val());
              //?BB  var fkOvernightStorage = $('input:radio[name=fkCodelstOvernStorage]:checked').val();
                //alert($("#fkCodelstOvernStorage").val());
                if(res !="" && res != undefined && res !="undefined"){
              		 var url = res;
              		 loadAction(url);
               }
                /*?BB     if(fkOvernightStorage=="1"){
                        //alert("load overnight");
                        loadAction('getOvernightStorage.action?newProductId='+productSearch);
                    }else{
                       
                    $("#main").html(result);
               
                    $('#accessionTable tbody[role="alert"] tr').each(function(i,el) {
                            var productId = this.cells[11].innerHTML;
                            var status = this.cells[13].innerHTML;
                            if(productId==productSearch && status=="Acquired"){
                                this.cells[13].innerHTML="Accessed";
                                $(this).removeAttr("style");
                            }
                        });
               
                    }
		*/
                //$("#accessionDiv").replaceWith($('#accessionDiv', $(result)));
                //$("#accessionDiv").replaceWith($('#accessionDiv', $(result)));
               
                //$('#accessionDiv').html($(result).find("#accessionDiv").html());
                //$('#table_inner_wrapper').html($(result).find("#table_inner_wrapper").html());
                //have to change the code Mari.
                //$("#fkCodelstProductsource").val(29);
                //alert($(result).find("#returnJsonStr").val());
               
                },
                error: function (request, status, error) {
                    alert("Error " + error);
                }
            });
            }catch(err){
                alert(" load action error " + err);
            }
}

/* Accession Table Server Parameters Starts */

	var accession_serverParam = function ( aoData ) {

													aoData.push( { "name": "application", "value": "stafa"});
													aoData.push( { "name": "module", "value": "technician_accession"});	
													
													aoData.push( {"name": "col_1_name", "value": "lower((select PERSON_ALTID from person where pk_person=dr.FK_RECEIPENT))" } );
													aoData.push( { "name": "col_1_column", "value": "lower(recipientId)"});
													
													aoData.push( { "name": "col_2_name", "value": " lower((select PERSON_LNAME from person where pk_person=dr.FK_RECEIPENT))"});
													aoData.push( { "name": "col_2_column", "value": "lower(recipientName)"});
													
													aoData.push( {"name": "col_3_name", "value": " lower(sp.spec_id)" } );
													aoData.push( { "name": "col_3_column", "value": "lower(sp.spec_id)"});
													
													aoData.push( {"name": "col_4_name", "value": " lower((select codelst_desc from er_codelst where pk_codelst=acq.fk_codelst_productsource))" } );
													aoData.push( { "name": "col_4_column", "value": "lower(productSource)"});
													
													aoData.push( {"name": "col_5_name", "value": " lower((select codelst_desc from er_codelst where pk_codelst=acq.fk_codelst_producttype))" } );
													aoData.push( { "name": "col_5_column", "value": "lower(productType)"});
													
													aoData.push( {"name": "col_6_name", "value": " lower(to_char(acq.ACQUISITION_DATETIME,'Mon dd, yyyy  HH:MM'))" } );
													aoData.push( { "name": "col_6_column", "value": "acq.ACQUISITION_DATETIME"});
													
													aoData.push( {"name": "col_7_name", "value": " lower(sp.spec_status)" } );
													aoData.push( { "name": "col_7_column", "value": "lower(sp.spec_status)"});
													
													};
	var accession_aoColumnDef = [
                         
			                         {"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
			                             return "<input type='hidden' id='accessionValues' value='"+source[12]+"-=-"+source[13]+"-=-"+source[11]+"-=-"+source[9]+"-=-"+source[10]+"-=-"+source[8]+"-=-"+source[7]+"'/>";
			                             }},
			                          {    "sTitle":"Recipient ID",
			                                 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
			                             return source[0];
			                             }},
			                          {    "sTitle":"Recipient Name",
			                                 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
			                                    return source[1];
			                                    }},
			                          {    "sTitle":"Product ID",
			                                        "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
			                                    return source[2];
			                                  }},
			                          {    "sTitle":"Product Source",
			                                      "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
			                                      return source[3];
			                              }},
			                          {    "sTitle":"Product Type",
			                                  "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
			                                      return source[4];
			                              }},
			                          {    "sTitle":"Accession Time",
			                                  "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
			                                      return source[5];
			                              }},
			                          {    "sTitle":"Status",
			                                  "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
			                                      return source[6];
			                              }}
			                         
			                        ];

var accession_aoColumn = [ { "bSortable": false},
			               null,
			               null,
			               null,
			               null,
			               null,
			               null,
			               null
			             ];
var accession_ColManager = function(){
										$("#accessionColumn").html($('.ColVis:eq(0)'));
									 };

/*Accession Table Server Parameters Ends*/


$(document).ready(function() {
	hide_slidewidgets();
	hide_slidecontrol();
    hideBarcodes1();
    $("#personDob" ).css("width","33%");

	//?BB $('#nextProtocolList').append("<option>Next Product</option><option>Logout</option>");
    /* $("select").attr("id","small");    */
    /* oTable = $('#productSearchTable').dataTable({
        "bJQueryUI": true
    }); */

    donorWidgetShowHide();
    //getAccessionTable();
	constructTable(false,'accessionTable',accession_ColManager,accession_serverParam,accession_aoColumnDef,accession_aoColumn);

    //setColumnManager('accessionTable');
    $("#codeLibrary").hide();
    $('#collCallMudCoordinContact').mask("999-999-9999",{placeholder:"_"});
   
    /*$('#partialFull100spin').spinit();
    $('#partialSmall100spin').spinit();
    $('#partialSmallHoriSpin').spinit();*/

    var url = "getAccessionProduct";
    var response = ajaxCall(url,"accessionNewForm");
    //? Removed by Muthu
    //generateTableFromJsnList(response);

    $("#barcodeDiv").dialog({
           autoOpen: false,
           modal: true,
           width:800,
           hieght:800,
           close: function() {
               //$(this).dialog('destroy').remove()
               $(this).dialog().children().remove();
               $(this).dialog("close");
               $(this).dialog('destroy').remove()
                //jQuery("#barcodeDiv").dialog("destroy");
           }
        });
    $( "#personDob" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
        changeYear: true, yearRange: "c-50:c+45"});
    $( "#accessionDate" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
        changeYear: true, yearRange: "c-50:c+45"});
    $( "#collectionDateID" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
         changeYear: true, yearRange: "c-50:c+45" });
    $( "#productionDateID" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
        changeYear: true , yearRange: "c-50:c+45"});
    $( "#expirationDateID" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
        changeYear: true, yearRange: "c-50:c+45"}); 
    $( "#barcodeRecipientDOB" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
        changeYear: true, yearRange: "c-50:c+45"});
    $( "#barcodeDonorDOB" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
        changeYear: true, yearRange: "c-50:c+45"});
   
    $('#collectionTime').timepicker({});
    $('#productionTime').timepicker({});
    $('#expirationTime').timepicker({});
    hideBarcodes();
   
    /*$("#recipientAlias").dialog({
           autoOpen: false,
           modal: true,
           width:800,
           hieght:800,
           close: function() {
                jQuery("#recipientAlias").dialog("destroy");
                 }
    });*/

   
    /*$("#donorAlias").dialog({
           autoOpen: false,
           modal: true,
           width:800,
           hieght:800,
           position:"center",
           close: function(event, ui) {
            jQuery("#donorAlias").dialog("destroy");
           }
    });

        $("#alias3").dialog({
           autoOpen: false,
           modal: true,
           width:800,
           hieght:800,
           position:"center",
           close: function(event, ui) {
                jQuery("#alias3").dialog("destroy");
          }
    });*/

    $("#document").dialog({
           autoOpen: false,
           modal: true,
           width:800,
           hieght:800,
           position:"center",
           close: function(event, ui)  {
                jQuery("#document").dialog("destroy");
           }
    });

    $("#additives").dialog({
           autoOpen: false,
           modal: true,
           width:800,
           hieght:800,
           position:"center",
           close: function(event, ui)  {
                jQuery("#additives").dialog("destroy");
          }
    });
   
    $("#itinerary").dialog({
           autoOpen: false,
           modal: true,
           width:800,
           hieght:800,
           position:"center",
           close: function(event, ui)  {
                jQuery("#itinerary").dialog("destroy");
          }
    });

    $("#barcodeDiv").dialog({
           autoOpen: false,
           modal: true,
           width:1200,
           hieght:600,
           position:"center",
           close: function(event, ui)  {
                jQuery("#barcodeDiv").dialog("destroy");
          }
 });

    setAttachmentDialog();
    /**/
    var workflowProductId = $("#workflowProductId").val();
   
    //alert(workflowProductId);
    if(workflowProductId!=null && workflowProductId!=""){
        //alert("v:: workflowProductId ==>"+workflowProductId)
        $("#productSearch").val(workflowProductId);
        //alert("$(#productSearch).val : "+$("#productSearch").val())
        $("#productSearch").attr("onblur","getAccessionData('','')");
        workFlowFlag = true;
        $("#productSearch").blur();
        $("#productSearch").focus();
        $("#productSearch").removeAttr("onblur");
        workFlowFlag = false;
       
        }
    //select and deselecting radio button
    $(".selectDeselect").click( function(e){
           var itsOn = $(this).hasClass("on");
           $(":radio[name="+ this.name + "]").removeClass("on");
          if(itsOn){
                $(this).removeAttr('checked');
               }else {
                    $(this).addClass("on");
                  }
          }).filter(":checked").addClass("on");
         
    $("#barcodeTem").hide();
    //BB
    //getAccession.action?product=bmt1&productId=96
    var product = $("#product").val();
	//BB testing 
	//product="bmt1"
	 if(product !=""){
		// alert(product);
		 $("#fake_conformProductSearch").val(product);
		 $("#conformProductSearch").val(product);
		 var keyEvent = jQuery.Event("keyup");
		 keyEvent.keyCode=$.ui.keyCode.RIGHT;
		 $("#conformProductSearch").focus();
		 $(".barcodeSearch").trigger(keyEvent);
		 
		 var keyEvent1 = jQuery.Event("keyup");
		 keyEvent1.keyCode=$.ui.keyCode.ENTER;
		 $(".barcodeSearch").trigger(keyEvent1);

		 $("#productSearch").val(product);
		 $("#productSearch").trigger(keyEvent1);
	}

	//to Check unique UserID in DB
	 $.validator.addMethod("uniqueUserId", function(ic) {
	    	response = false;
	    	domain = "Person";
	    	propertyName = "personaltId";
	    	val = "'"+ic+"'";
	    	$.ajax({
	            type: "POST",
	            url: "checkDuplicate.action",
	            async:false,
	            data :{domainName:domain,property:propertyName,value:val},
	            dataType: 'json',
	            success: function (result){
	        	if(result.returnCount<=0){
	        		response = true;
	            	}
	            }
	        });
	        
	     return response;
	  }, "");

});





function columnMaager1(){
    var showFlag1 = false;
    $('#targetall1').hide();
    $('#accessionTable').columnManager({listTargetID:'targetall1', onClass: 'advon', offClass: 'advoff', saveState: true});
    $('#ulSelectColumn').click( function(){
          if(showFlag1){
            $('#targetall1').fadeOut();
            showFlag1 = false;
          }else{
              $('#targetall1').fadeIn();
              showFlag1 = true;
          }
        });
}
function donorWidgetShowHide(){
    var donorInfoViewId = $("#donorInfoViewId").val();
    var fkCodelstProductsource = $("#fkCodelstProductsource").val();
    if(donorInfoViewId==fkCodelstProductsource){
            //$("#donorInformation").hide();
        }else{
            //$("#donorInformation").show();
        }
}
function showBarcode(){
    var checkBx =  document.getElementsByName("check1");
   
    for(var i = 0;i<checkBx.length;i++){
        var id = checkBx[i].id+"a";
        if(checkBx[i].checked){
            $("#"+id).css("display","block");
        }else{
            $("#"+id).css("display","none");
        }
    }
}
function showHideBarcodes(buttonObj){
    if($(buttonObj).val()=="On"){
        $(buttonObj).val("Off");
        hideBarcodes();
    }else{
        $(buttonObj).val("On");
        showBarcode();
    }
}

$(function(){
    $("select").uniform();
    
    $("#accessionForm").validate({
    	rules:{
            "recipientId":{required : true,
            				uniqueUserId : true 
            	},
            	"donorId":{required : true,
            		uniqueUserId : true 
        		}
            },
            messages:{
                "recipientId":{
    						required: "Please Enter Recipient Id",
    						uniqueUserName:"This Recipient Id is already Exists!"
    				},
                "donorId":{
					required: "Please Enter Donor Id",
					uniqueDonorId:"This Donor Id is already Exists!"
				}
            }
       });
    
  });

 $("input[name='recipientAlias']").change(function(){
 	 var checkradio=$("input[name='recipientAlias']:checked").val();
	 if(checkradio!=1){
		 $("#recipient_view").hide();
		 }
	 else {		 
		 $("#recipient_view").show();
		 }
		
	 });

 $("input[name='productAlias']").change(function(){
 	 var checkradio=$("input[name='productAlias']:checked").val();
	 if(checkradio!=1){
		 $("#product_view").hide();
		 }
	 else {		 
		 $("#product_view").show();
		 }
		
	 });

 $("input[name='donorAlias']").change(function(){
 	 var checkradio=$("input[name='donorAlias']:checked").val();
	 if(checkradio!=1){
		 $("#donor_view").hide();
		 }
	 else {		 
		 $("#donor_view").show();
		 }
		
	 });
 
 $("input[name='fkCodelstAdditives']").change(function(){
 	 var checkradio=$("input[name='fkCodelstAdditives']:checked").val();
	 if(checkradio!=1){
		 $("#fkCodelstAdditives_view").hide();
		 }
	 else {		 
		 $("#fkCodelstAdditives_view").show();
		 }
		
	 });
 $("input[name='fkCodelstReqDocument']").click(function(){
 	var checkradio=$("input[name='fkCodelstReqDocument']:checked").val();
	 if(checkradio!=1){
		 $("#fkCodelstReqDocument_view").hide();
	 }
 	else {
		 $("#fkCodelstReqDocument_view").show();
	 }
 });
 

function setDialog(id,tit){
    $(id).dialog({
           autoOpen: false,
           modal: true,
           title:tit,
           width:800,
           hieght:800,
           close: function() {
                jQuery(id).dialog("destroy");
         }
 });
}


function hideBarcodes(){
    $("#full100a").css("display","none");
    $("#partialSmallHoria").css("display","none");
    $("#partialSmalla").css("display","none");
    $("#partialFull100a").css("display","none");
}


$('#recipient_view').live('click', function (e) {
                e.preventDefault();         
                getAliasData("accessionForm","recipientAliasDiv","1");
                if(alisflag!=false){
                setDialog('#recipientAlias',"Recipient alias");
	                $('#recipientAlias').dialog('open');
	                $('#recipientAlias').css("width","100%");
	                return false;
				}
            });
$('#product_view').live('click', function (e) {
                e.preventDefault();
                getAliasData("accessionForm","productAliasDiv","3");
                if(alisflag!=false){
                setDialog('#productAlias',"Product alias");
                $('#productAlias').dialog('open');
                $('#productAlias').css("width","100%");
                return false;
                }
            });
$('#donor_view').live('click', function (e) {
                e.preventDefault();
                getAliasData("accessionForm","donorAliasDiv","2");
                if(alisflag!=false){
                setDialog('#donorAlias',"Donor alias");
                $('#donorAlias').dialog('open');
                $('#donorAlias').css("width","100%");
                return false;
                }
            });
$('#fkCodelstReqDocument_view').live('click', function (e) {
                e.preventDefault();
                //onclick="saveDocumentAdditives('1','documentTable','requiredDocument
                getDocumentAdditives("accessionForm","requiredDocument","1");
                //alert(docsflag);
                if(docsflag){
	                setDialog('#requiredDocument',"Required documents");
	                $('#requiredDocument').dialog('open');
	                $('#requiredDocument').css("width","100%");
	                return false;
                }
            });
$('#fkCodelstAdditives_view').live('click', function (e) {
                e.preventDefault();
                getDocumentAdditives("accessionForm","additivesDiv","2");
                //alert(docsflag);
                if(docsflag){
	                setDialog('#additivesDiv',"Additives");
	                $('#additivesDiv').dialog('open');
	                $('#additivesDiv').css("width","100%");
	                return false;
                }
            });
           
$('#iti').live('change', function (e) {
                e.preventDefault();
                setDialog('#itinerary');
                $('#itinerary').dialog('open');
                $('#itinerary').css("width","100%");
                return false;
            });
var cellTextFlag=false;
function displayResult(id)
{
    //alert(id);
    //alert(rowsLen);
var tables=document.getElementById(id);
var rowsLen = tables.rows.length;
//alert(rowsLen);
//rowsLen=rowsLen-1;
//alert(rowsLen);
var row=tables.insertRow(rowsLen);
var cell0=row.insertCell(0);
var cell1=row.insertCell(1);
var cell2=row.insertCell(2);
var cell3=row.insertCell(3);
$(cell0).attr('style','display:none');
$(cell1).attr('style','width:100px');
$(cell2).attr('style','width:130px');
$(cell3).attr('style','width:70px');
$(cell1).click(function(event) {
    if(!(this.getAttribute('textFlag')=="true")){
    insertTextBox(this);
    cellTextFlag=true;
    }
});

$(cell2).click(function(event) {
    if(!(this.getAttribute('textFlag')=="true")){
    insertTextBox(this);
    cellTextFlag=true;
    }
});

$(cell3).click(function(event) {
    if(!(this.getAttribute('textFlag')=="true")){
    insertTextBox(this);
    cellTextFlag=true;
    }
});

cell1.innerHTML="&nbsp;";
cell2.innerHTML="&nbsp;";
cell3.innerHTML="&nbsp;";

cell1.setAttribute('textFlag',false);
cell2.setAttribute('textFlag',false);
cell3.setAttribute('textFlag',false);

}
// additives data

function displayResult1(id)
{
    //alert(id);
    //alert(rowsLen);
var tables=document.getElementById(id);
var rowsLen = tables.rows.length;
//alert(rowsLen);
//rowsLen=rowsLen-1;
//alert(rowsLen);
var row=tables.insertRow(rowsLen);
var cell0=row.insertCell(0);
var cell1=row.insertCell(1);
var cell2=row.insertCell(2);

$(cell0).attr('style','display:none');
$(cell1).attr('style','width:100px');
$(cell2).attr('style','width:130px');

$(cell1).click(function(event) {
    if(!(this.getAttribute('textFlag')=="true")){
   
        insertTextBox(this);
    cellTextFlag=true;
    }
});

$(cell2).click(function(event) {
    if(!(this.getAttribute('textFlag')=="true")){

        insertTextBox(this);
    cellTextFlag=true;
    }
});



cell1.innerHTML="&nbsp;";
cell2.innerHTML="&nbsp;";

cell1.setAttribute('textFlag',false);
cell2.setAttribute('textFlag',false);


}




//----------------------------------
function insertTextField(This){
    alert(This.getAttribute('textFlag'));

    if(!(This.getAttribute('textFlag')=="true")){
        insertTextBox(this);
        cellTextFlag=true;
        }
}

function barPrinting(This){
    var id= $(This).attr("id");
    if((id=="pageLibrary")||(id=="productcodeMenu")){
       
        $("#codeLibrary").show();
        $("#barcodePrinting").hide();
       
    }
    else if((id=="pagePrinting")||(id=="barcodeMenu")){
       
        $("#codeLibrary").hide();
        $("#barcodePrinting").show ();
       
    }
}
/* function flash(This) {
    //alert(get_type(This));
    //alert(This);
}
window.captureEvents(Event.CLICK);
window.onclick = (function(event) {
    //flash(this);
}); */

   
var oldObj;
function changeTextBox(newObj){

    if($(newObj).hasClass('tableRowSelected')){
        $(newObj).removeClass('tableRowSelected');
        oldObj = null
        return;
    }
     if (oldObj){
         $(oldObj).removeClass('tableRowSelected');
     }
 
 $(newObj).addClass('tableRowSelected');
 oldObj=$(newObj);
}

function get_type(thing){
    if(thing===null)return "[object Null]"; // special case
    return Object.prototype.toString.call(thing);
}


var oldCellObj;
var textCellFlag=false;
function insertTextBox(cellObj){
    //alert(cellObj.getAttribute('textFlag'))
   
    if(oldCellObj){
        ///alert("11-->"+oldCellObj.getAttribute('textFlag'));
        }
    if(cellObj.getAttribute('textFlag')=="false" && oldCellObj && (oldCellObj.getAttribute('textFlag')=="true")){
        //alert("-->"+cellObj.getAttribute('textFlag'))
        var tempValue = $("#tempId").val();
        //alert("-->tempValue"+tempValue)
        cellObj.innerHTML="<input type='text' id='tempId' style='width:99%'  onblur='setCellValue(this)' value='"+cellObj.innerHTML+"'/>";
        cellObj.setAttribute('textFlag',true);
        oldCellObj.innerHTML=tempValue;
        oldCellObj.setAttribute('textFlag',false);
        oldCellObj=cellObj;
        textCellFlag=true;
        /*$('#tempId').blur(function() {
            setCellValue(this);
            });*/
        $('#tempId').focus();
        return;
    }

    if(cellObj.getAttribute('textFlag')=="false"){
        //alert("=====>"+cellObj.getAttribute('textFlag'))
       
        cellObj.innerHTML="<input type='text' id='tempId'style='width:99%'  onblur='setCellValue(this)' value='"+cellObj.innerHTML+"'/>";
        cellObj.setAttribute('textFlag',true);
        /*$('#tempId').blur(function() {
              setCellValue(this);
            });*/
        oldCellObj=cellObj;
        textCellFlag=true;   
        $('#tempId').focus();
    }
}

function setCellValue(This){
    //alert(This.value);
    //alert(oldCellObj.innerHTML);
       
    oldCellObj.innerHTML=This.value;
    oldCellObj.setAttribute('textFlag',false);
    oldCellObj = null;
}

function getScanData(event,obj){
  	if(event.keyCode==13){
       
        getAccessionData(event,this);
    }
}


/* Scrolling Method Starts*/
$("#accessTable").scroll(function(){
	 var newMargin = $(this).scrollLeft();  
	 $('#accessionTable_length').css('marginLeft', newMargin);
	 $('#accessionTable_length').css('width', '20%');
	 $('#accessionTable_filter').css('width', '20%');
	 $('#accessionTable_info').css('marginLeft', newMargin);
	 $('#accessionTable_info').css('width', '40%');
});
</script>


<div class="cleaner"></div>

        <form>
        <div class="column">
        <div  class="portlet">
            <div class="portlet-header addnew"><s:hidden id="moduleName" value="accession"/><s:hidden id="widgetName" value="accessionProducts"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.accessionproducts" /></div>
            <div class="portlet-content">
			<div id='accessTable' style="overflow-x:auto;overflow-y:hidden;">
            <table cellpadding="0" cellspacing="0" border="0" id="accessionTable" class=display>
                <thead>
                   <tr>
                        <th width="4%" id="accessionColumn"></th>
                        <th colspan="1"><s:text name="stafa.label.recipientid"/></th>
                        <th colspan="1"><s:text name="stafa.label.recipientname"/></th>
                        <th colspan="1"><s:text name="stafa.label.productid"/></th>
                        <th colspan="1"><s:text name="stafa.label.productsource"/></th>
                        <th colspan="1"><s:text name="stafa.label.producttype"/></th>
                        <th colspan="1"><s:text name="stafa.label.accessiontime"/></th>
                        <th colspan="1"><s:text name="stafa.label.status"/></th>

                    </tr>
            	</thead>
            </table>
			</div>           
    </div>
    </div>
    </div>
    </form>
        <!-- <div  class="portlet">
            <div class="portlet-header">&nbsp;Product Accession and Assessment</div>
            <div id="button_wrapper"><input class="scanboxSearch" type="text" size="15" value="" name="search" placeholder="Scan Product/Enter ID"/></div>
            <div class="img_1"><a href="#"><img src="images/icons/refresh1.jpg" width="16" height="16" border="0" /></a>&nbsp;<a href="#"><img src="images/icons/edit.png" width="16" height="16" border="0" /></a></div>           
        </div>
         -->
         <s:form id="accessionForm" namespace="/" method="POST" enctype="multipart/form-data">
         <s:hidden name="returnMessage" id="returnMessage" value="%{returnMessage}"/>
         <s:hidden name="workflowProductId" id="workflowProductId" value="%{workflowProductId}"/>
        <s:hidden name="donorInfoViewId" id="donorInfoViewId" value="%{donorInfoViewId}"/>
        <s:hidden name="product" id="product" value="%{product}"/>
        <s:hidden name="fkProtocol" id="fkProtocol" value="%{fkProtocol}"/>
        <s:hidden name="productStatus" id="productStatus" />
        <s:hidden id="pkSpecimenProduct" name="pkSpecimenProduct" />
        <s:hidden id="fkCodelstAlisType" name="fkCodelstAlisType" />
        <s:hidden id="columnType" name="columnType" />
        <s:hidden id="donorPk" name="donorPk" />
        <s:hidden id="recipientPk" name="recipientPk" />
        <!-- <div class="column">-->
        <div class="column" id="accessionDiv">
        <div id="breadCrumb"></div>
        <div  class="portlet">
        <%
        System.out.println("(String)moduleMap.get(moduleName) ====================================="+(String)moduleMap.get(STAFA_BARCODE));
		if((String)moduleMap.get(STAFA_BARCODE) != null && checkAccessRightsForFields(STAFA_BARCODE).indexOf("R") != -1  ) { %>
			<div class="portlet-header printsmall"><s:hidden id="moduleName" value="accession"/><s:hidden id="widgetName" value="recipientInformation"/><s:hidden id="pkNotes" value=""/>&nbsp;<s:text name="stafa.label.recipientinformation" /></div>
		<%}else{%>
        	<div class="portlet-header printsmall"><s:hidden id="moduleName" value="accession"/><s:hidden id="widgetName" value="recipientInformation"/><s:hidden id="pkNotes" value=""/>&nbsp;<s:text name="stafa.label.recipientinformation" /></div>
        <% } %>           
        <div class="portlet-content">
               
        <div id="button_wrapper">
       
        <div style="margin-left:40%;">
        <input class="scanboxSearch barcodeSearch"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;"/>
		<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" />
<!--         <input class="scanboxSearch hidden" type="text" size="15" value="" name="productSearch" id="productSearch" onkeyup="getAccessionData(event,this);"  placeholder="Scan/Enter Product ID" style="width:200px;"/>
 -->    
 		</div>
        </div>
       
            <div id="table_inner_wrapper">
                <div id="left_table_wrapper">
                    <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                        <tr>
                            <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.recipientmdaid" /></div></td>
                            <td width="50%" height="40px"><div align="left"><input type="text" name="recipientId" id="recipientId" placeholder="Recipient Id"/></div></td>
                        </tr>
                        <tr>
                            <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.recipientname" /></div></td>
                            <td width="50%" height="40px"><div align="left"><input type="text" name="recipientName"  id="recipientName" placeholder="Recipient Name"/></div></td>
                        </tr>
                        <tr>
                            <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.otheralias" /></div></td>
                           <!--  <td width="50%" height="40px"><div align="left"><input type="radio" id="recpalise_yes" name="recpalise" class="selectDeselect"/>Y &nbsp; <input type="radio" id="recpalise_no" name="recpalise" class="selectDeselect"/> N <a href=# id="recipient_y" style="color:blue;display:none;">&nbsp;View</a> </div></td> -->
                            <td width="50%" height="40px"><div align="left"><s:radio name="recipientAlias" id="recipientAlias" list="#{'1':'Yes','0':'No'}" value="" cssClass="selectDeselect"/><a href=# id="recipient_view" style="color:blue;display:none;">&nbsp;View</a></div></td>
                        </tr>
                        <tr>
                            <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.recipientweight" /></div></td>
                            <td width="50%" height="40px"><div align="left"><input type="text" id="recipientWeight" name="personWeight" placeholder="Recipient Weight"/></div></td>
                        </tr>
                    </table>
                </div>
           
            <div id="middle_line"></div>
           
            <div id="right_table_wrapper">
                <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.diagnosis" /></div></td>
                        <td width="50%" height="40px"><s:select   headerKey="" headerValue="Select"   list="diagnosisList" listKey="pkCodelst" listValue="description" name="personDiagnosis" id="personDiagnosis" /></td>
                    </tr>
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.aborh" /></div></td>
                        <td width="50%" height="40px"><s:select headerKey="" headerValue="Select"  list="aboRhList" listKey="pkCodelst" listValue="description" name="aboRh" id="aboRh"/></td>
                    </tr>
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.plannedprotocol" /></div></td>
                        <td width="50%" height="40px"><s:select headerKey="" headerValue="Select"  list="planProtocolList" listKey="pkCodelst" listValue="description" name="plannedProtocol" id="plannedProtocol"/></td>
                    </tr>
                </table>           
                <div class="cleaner"></div>               
            </div>
        </div>
           
            </div>
            </div>
            </div>
           
        <div class="column">
        <div id="donorInformation" class="portlet">
        <div class="portlet-header "><s:hidden id="moduleName" value="accession"/><s:hidden id="widgetName" value="donorInformation"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.donorinformation" /></div>
        <div class="portlet-content">
            <div id="table_inner_wrapper">
            <div id="left_table_wrapper">
                <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.donormdaid" /></div></td>
                        <td width="50%" height="40px"><div align="left"><input type="text" placeholder="Donor Id" name="donorId" id="donorId"/></div></td>
                    </tr>
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.donorname" /></div></td>
                        <td width="50%" height="40px"><div align="left"><input type="text" placeholder="Last Name, First Name" name="donorName" id="donorName"/></div></td>
                    </tr>
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left">Donor Date of Birth</div></td>
                        <td width="50%" height="40px"><div align="left"><input type="text" id="personDob" name="personDob" size="" placeholder="Donor DOB" class="dateEntry"/>&nbsp;&nbsp;<input type="radio" id="" name="" class="selectDeselect"/>Unknown</div></td>
                       
                    </tr>
                </table>
            </div>
           
            <div id="middle_line"></div>
           
            <div id="right_table_wrapper">
           
                <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.otheralias" /></div></td>
                       <!--  <td width="50%" height="40px"><div align="left"><input type="radio" id="donoralise_yes" name="donoralise" class="selectDeselect"/>Y &nbsp; <input type="radio" id="" name="donoralise" class="selectDeselect"/> N <a href=# id = "donor_y" style="color:blue;display:none;">&nbsp;View</a> </div></td> -->
                       <td width="50%" height="40px"><div align="left"><s:radio name="donorAlias" id="donorAlias" list="#{'1':'Yes','0':'No'}" value="" cssClass="selectDeselect"/><a href=# id="donor_view" style="color:blue;display:none;">&nbsp;View</a></div></td>
                    </tr>
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.aborh" /></div></td>
                        <td width="50%" height="40px"><s:select headerKey="" headerValue="Select" list="aboRhList" listKey="pkCodelst" listValue="description" name="donorAboRh" id="donorAboRh"/></td>
                    </tr>                   
                </table>           
               
            </div>
            </div>
            </div>           
            </div>
           
            </div>   
           
        <div class="column">   
        <div  class="portlet">
        <div class="portlet-header "><s:hidden id="moduleName" value="accession"/><s:hidden id="widgetName" value="productInformation"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.productinformation" /></div>
           
        <div class="portlet-content">
        <!-- <div id="button_wrapper" align="center"><input class="scanboxSearch" type="text" size="15" value="" name="productSearch" id="productSearch" onkeyup="getAccessionData(event,this);"  placeholder="Scan/Enter Product ID" /></div> -->
       
            <div id="table_inner_wrapper">
            <div id="left_table_wrapper">
           
                <table cellpadding="0" cellspacing="0" class="" align="" border="0" width="100%">
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.productctlid" /></div></td>
                        <td width="50%" height="40px"><div align="left"><input type="text" placeholder="CTL Product Id" name="altProductId" id="altProductId"/></div></td>
                    </tr>
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.otheralias" /></div></td>
                       <!--  <td width="50%" height="40px"><div align="left"><input type="radio" id="proalise_yes" name="proalise" class="selectDeselect"/>Y &nbsp; <input type="radio" id="proalise_no" name="proalise" class="selectDeselect"/> N <a href=# id = "product_y" style="color:blue;display:none;">&nbsp;View</a> </div></td> -->
                       <td width="50%" height="40px"><div align="left"><s:radio name="productAlias" id="productAlias" list="#{'1':'Yes','0':'No'}" value="" cssClass="selectDeselect"/><a href=# id="product_view" style="color:blue;display:none;">&nbsp;View</a></div></td>
                    </tr>                   
                </table>           
            </div>
           
            <div id="middle_line"></div>
           
            <div id="right_table_wrapper">
           
                <table cellpadding="0" cellspacing="0" class="" align="" border="0" width="100%">
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.producttype" /></div></td>
                        <td width="50%" height="40px"><s:select headerKey="" headerValue="Select"  headerKey="" headerValue="Select" list="productTypeList" listKey="pkCodelst" listValue="description" name="fkCodelstProductType" id="fkCodelstProductType"/></td>
                    </tr>
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.productsource" /></div></td>
                        <td width="50%" height="40px"><s:select headerKey="" headerValue="Select"  headerKey="" headerValue="Select" list="productSourceList" listKey="pkCodelst" listValue="description" onchange="donorWidgetShowHide();" name="fkCodelstProductsource" id="fkCodelstProductsource"/></td>
                    </tr>
                    </table>           
   
            </div>
            </div>
            </div>
            </div>
            </div>
            
           
        <div class="column">
        <div  class="portlet">
        <div class="portlet-header "><s:hidden id="moduleName" value="accession"/><s:hidden id="widgetName" value="collectionFacility"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.collectionfacility" /></div>
           
        <div class="portlet-content">

            <div id="table_inner_wrapper">
            <div id="left_table_wrapper">
           
                <table cellpadding="0" cellspacing="0" class="" align="" border="0" width="100%">
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.productregistry" /></div></td>
                        <td width="50%" height="40px"><s:select headerKey="" headerValue="Select"  headerKey="" headerValue="Select" list="prodRegistryList" listKey="pkCodelst" listValue="description" name="fkCodelstFacility" id="fkCodelstFacility" /></td>
                    </tr>
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.beginningstatus" /></div></td>
                        <td width="50%" height="40px"><s:select headerKey="" headerValue="Select"  list="begStatusList" listKey="pkCodelst" listValue="description" name="fkCodelstStatus" id="fkCodelstStatus" /></td>
                    </tr>
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.collectionlocation" /></div></td>
                        <td width="50%" height="40px"><s:select headerKey="" headerValue="Select"  headerKey="" headerValue="Select" list="colllocationList" listKey="pkCodelst" listValue="description" name="fkCodelstLocation" id="fkCodelstLocation"/></td>
                    </tr>                   
                </table>           
            </div>
           
            <div id="middle_line"></div>
           
            <div id="right_table_wrapper">
           
                <table cellpadding="0" cellspacing="0" class="" align="" border="0" width="100%">
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.mudcoordinator" /></div></td>
                        <td width="50%" height="40px"><input type="text" name="collMudCoordinator" id="collMudCoordinator" ></td>
                    </tr>
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.mudcoordinatorcontact" /></div></td>
                        <td width="50%" height="40px"><div align="left"><input type="text"  name="collMudCoordinContact" id="collMudCoordinContact"/></div></td>
                    </tr>
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.mudcoordinatoroncall" /></div></td>
                        <td width="50%" height="40px"><input type="text" class="" name="collCallMudCoordin" id="collCallMudCoordin"></td>
                    </tr>
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.mudcoordinatoroncallcontact" /></div></td>
                        <td width="50%" height="40px"><div align="left"><input type="text" name="collCallMudCoordinContact" id="collCallMudCoordinContact"/></div></td>
                    </tr>
                </table>
            </div>
        </div>
            </div>
            </div>
            </div>

        <div  class="column">
        <div  class="portlet">
        <div class="portlet-header "><s:hidden id="moduleName" value="accession"/><s:hidden id="widgetName" value="shippingInformation"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.shippinginformation" /></div>
           
        <div class="portlet-content">

            <div id="table_inner_wrapper">
            <div id="left_table_wrapper">           
                <table cellpadding="0" cellspacing="0" class="" align="" border="0" width="100%">
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.couriername" /></div></td>
                        <td width="50%" height="40px"><div  class="mainselection" align="left"><s:select headerKey="" headerValue="Select"  headerKey="" headerValue="Select" list="courierNameList" listKey="pkCodelst" listValue="description" name="fkCodelstCourier" id="fkCodelstCourier"/></div></td>
                    </tr>
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><a href="#" id ="iti"><s:text name="stafa.label.itinerary" /></a></div></td>
                        <td width="50%" height="40px"><div align="left"><iframe height="29" src="jsp/fileUpload.jsp" style="border: medium none;" id="testFrame"></iframe>
                            <div id="fileAttachmentDiv">
                                <!-- <iframe height="50" src="jsp/fileUpload.jsp" style="border: medium none;" id="testFrame">
                                </iframe>-->
                                <div id="viewAttachment" style="display:none"></div>
                            </div>
                           
                            <!--<table width="100%" align="left">
                                <tr>
                                <td width="50%"></td>
                                <td width="50%"></td>
                                </tr>
                            </table>-->
                        </div></td>
                    </tr>
                        <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left">Shipping Environment</div></td>
                        <td width="50%" height="40px"><div  class="mainselection" align="left"><s:select headerKey="" headerValue="Select" list="shipEnvironmentList" listKey="pkCodelst" listValue="description" name="refShippingEnvironment" id="refShippingEnvironment"/></div></td>
                    </tr>                               
                </table>               
            </div>
           
            <div id="middle_line"></div>
           
            <div id="right_table_wrapper">
               
                <table cellpadding="0" cellspacing="0" class="" align="" border="0" width="100%">
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.trackingnumber" /></div></td>
                        <td width="50%" height="40px"><div align="left"><input type="text" placeholder="Tracking Number" name="accessionTrackingNumber" id="accessionTrackingNumber"/></div></td>
                    </tr>
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.finalstatus" /></div></td>
                        <td width="50%" height="40px"><s:select headerKey="" headerValue="Select"  headerKey="" headerValue="Select" list="finalstatusList" listKey="pkCodelst" listValue="description" name="fkCodelstFinalStatus" id="fkCodelstFinalStatus"/></td>
                    </tr>                               
                </table>
            </div>
            </div>
        </div>
        </div>
        </div>
           
            <div class="column">
            <div  class="portlet">
        <div class="portlet-header "><s:hidden id="moduleName" value="accession"/><s:hidden id="widgetName" value="Accession"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.accession" /></div>
           
        <div class="portlet-content">

            <div id="table_inner_wrapper">
            <div id="left_table_wrapper">
           
                <table cellpadding="0" cellspacing="0" class="" align="" border="0" width="100%">
                        <tr>
                            <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.accessionlocation" /></div></td>
                            <td width="50%" height="40px"><s:select headerKey="" headerValue="Select"  headerKey="" headerValue="Select" list="begStatusList" listKey="pkCodelst" listValue="description" list="procSelecList" listKey="pkCodelst" listValue="description" name="fkCodelstProcessSelection" id="fkCodelstProcessSelection"/></td>
                        </tr>
                        <tr>
                            <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.productappearance" /></div></td>
                            <td width="50%" height="40px"><div align="left"><s:radio name="fkCodelstAppearance" id="fkCodelstAppearance" list="#{'1':'Yes','0':'No'}" value="" cssClass="selectDeselect" /></div></td>
                        </tr>
                        <%-- <tr>
                            <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.overnightstorage" /></div></td>
                            <td width="50%" height="40px"><div align="left"><s:radio name="fkCodelstOvernStorage" id="fkCodelstOvernStorage" list="#{'1':'Yes','2':'No'}" value="" cssClass="selectDeselect"/></div></td>
                        </tr> --%>
                        <tr>
                            <td width="50%" height="40px"><div class="txt_align_label" align="left"><a href="#" id ="doc"><s:text name="stafa.label.requireddocumentation" /></a></div></td>
                            <td width="50%" height="40px"><div align="left"><s:radio name="fkCodelstReqDocument" id="fkCodelstReqDocument" list="#{'1':'Yes','0':'No'}" value="" cssClass="selectDeselect"/><a href=# id="fkCodelstReqDocument_view" style="color:blue;display:none;">&nbsp;View</a></div></td>
                            <!-- <td width="50%" height="40px"><div align="left"><input type="radio" id="fkCodelstReqDocument_yes" name="fkCodelstReqDocument" class="selectDeselect"/>Y &nbsp; <input type="radio" id="fkCodelstReqDocument_no" name="fkCodelstReqDocument" class="selectDeselect"/> N <a href=# id="fkCodelstReqDocument" style="color:blue;display:none;">&nbsp;View</a></div></td> -->
                        </tr>
                         <tr>
                        	<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.dateofaccession" /></div></td>
                        	<td width="50%" height="40px"><div align="left"><input type="text" id="accessionDate" name="accessionDate" placeholder="Accession Date" class="dateEntry"/></div></td>
                   		</tr>                       
                    </table>           
            </div>
            <div id="middle_line"></div>
           
            <div id="right_table_wrapper">
               
                <table cellpadding="0" cellspacing="0" class="" align="" border="0" width="100%">
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.completelabel" /></div></td>
                        <td width="50%" height="40px"><div align="left"><s:radio name="fkCodelstComplete" id="fkCodelstComplete" list="#{'1':'Yes','0':'No'}" value="" /></div></td>
                    </tr>
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><a href="#" id ="add"><s:text name="stafa.label.additives" /></a></div></td>
                        <td width="50%" height="40px"><div align="left"><s:radio name="fkCodelstAdditives" id="fkCodelstAdditives" list="#{'1':'Yes','0':'No'}" value="" /><a href=# id="fkCodelstAdditives_view" style="color:blue;display:none;">&nbsp;View</a></div></td>
                       <!--  <td width="50%" height="40px"><div align="left"><input type="radio" id="fkCodelstAdditives_yes" name="fkCodelstAdditives" class="selectDeselect"/>Y &nbsp; <input type="radio" id="fkCodelstAdditives_no" name="fkCodelstAdditives" class="selectDeselect"/> N <a href=# id="fkCodelstAdditives" style="color:blue;display:none;">&nbsp;View</a></div></td> -->
                    </tr>
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.receivedby" /></div></td>
                        <td width="50%" height="40px"><div align="left"><input type="text" id="receivedBy" name="receivedBy" placeholder="Received By"/></div></td>
                    </tr>
                    <%-- <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.dateofaccession" /></div></td>
                        <td width="50%" height="40px"><div align="left"><input type="text" id="accessionDate" placeholder="Accession Date" class="dateEntry"/></div></td>
                    </tr> --%>
                    <tr>
                        <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.timeofaccession" /></div></td>
                        <td width="50%" height="40px"><div align="left"><span type="text" class='jclock' id="arrivalDate" style="width:100px; font-size:1.0em;"></span>&nbsp;<span id="timeZone"></span></div></td>
                    </tr>                   
                </table>
            </div>
        </div>
            </div>
    </div>
    </div>
        <div class="cleaner"></div>
        <div align="right" style="float:right;">
        <form>
                <table cellpadding="0" cellspacing="0" class="" border="0" >
                <tr>
                <td>Next:</td>
                <td><div class="mainselection" >
                
                 <select name="nextOption" id="nextOption">
                 	<option value="select">Select</option>
               		<s:iterator value="nextOptions" status="row" var="cellValue">  
	               		<s:if test="%{#cellValue[2] == 0}">
	                 		<option value="<s:property value="#cellValue[1]"/>"> <s:property value="#cellValue[0]"/></option>
	                 	</s:if>
	                 	<s:elseif test="%{#cellValue[2] > 0}">	
	                 		<option value="<s:property value="#cellValue[2]"/>"> <s:property value="#cellValue[0]"/></option>
	                 	</s:elseif>
	                 </s:iterator>
                 </select>
                 </div></td>
                <td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
                <td><input type="button" onclick="saveAccession()" value="Next"/></td>
                </tr>
                </table>
        </form>
        </div>
        <!-- <div id="button_next_wrapper" align="right" style="float:right;">   
            <div id="button_next">
                <div class="scroll_text">Next:&nbsp;<select name="" id="" class="sel_process"><option>Self Assign</option><option>Logout</option></select>&nbsp;</div>
                <div class="button_img"><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;<input type="button" value="Save"/></div>
            </div>
        </div>
         -->
    </s:form>

           
           
<script>
$(function() {
    $('.jclock').jclock();
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
    /**For Notes**/
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
    .append("<span style=\"float:right;\" class='ui-notes'></span>");

    $( ".portlet-header .ui-notes " ).click(function() {
        showNotes('open',this);
    });

    /**For PopUp**/
   
    /*$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
    .append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span>");

    $( ".portlet-header .ui-addnew" ).click(function() {
        showPopUp("open","addNewDialog1","Add New accession","700","300")
    });*/
   
    /**For Print**/
        $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".printsmall" )
    .append("<span style=\"float:right;\" class='ui-printsmall'></span>");

    $( ".portlet-header .ui-printsmall " ).click(function() {
        setTimePicker();
        //addNewPopup("Barcode","mainbarcodediv","loadBarcodePopup","710","1050");
        loadpopup();
        
    });
   
    /**For Refresh**/
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
    .append("<span style=\"float:right;\" class='ui-reset'></span>");


    $( ".portlet-header .ui-reset " ).click(function() {
        var formName = document.getElementById("accessionRecipientForm");
        clearForm(formName);
    });

    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

    $('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});   
    //$('.sortable').columnManager({listTargetID:'sortable', onClass: 'advon', offClass: 'advoff', hideInList: [1,3],
     //   saveState: true, colsHidden: [2,4]});
    //$( ".column" ).disableSelection();
});

function loadpopup(){
	
	 $("#maindiv").load("loadBarcodePopup.action").dialog({
			   autoOpen: true,
			   modal: true, width:1050, height:710,
			   close: function() {
				   jQuery("#maindiv").dialog("destroy");
				   //$(this).html(" ");
				   $(this).remove();
				  $("#mainbarcodediv").append('<div id="maindiv"></div>');

				  
			   }
			});

	}

var now = new Date();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivalDate').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new Date()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};

$("#timeZone").html(timezones[-offset / 60]);
function printBarcode(){
	$("#printBarcodeDiv").html("");
	$("#printBarcodeDiv").append($(".ui-dialog").find("#barcodeTem").clone());
	//alert($("#printBarcodeDiv").html());
	//alert("before print")
    $('#printBarcodeDiv').show().printElement(); 
	//$('#printBarcodeDiv').show().print();
}
</script>


<div  id ="mainbarcodediv" style="background-color:white; display:none; height:655px;overflow:auto;margin-top:1%;">  <!-- added by mohan j -->
<div id="maindiv"><div id="barimageTables" style="float:right;display:none; width:44%;height:40%;">
			<!-- From Template Start -->
		<div id="barcodeMainDiv" >
			
			<div class="barcode_wrapper" id="barcodeTem">
			</div>
		</div>
			<!-- From Template Ends -->
</div></div>
</div>
        
<div id="recipientAlias" style="display: none">
<s:form id="recipientAliasForm">
<img src = "images/icons/addnew.jpg" style="width:16;height:16;" onclick="displayResult('recipientAliasTbody')" />
        <div id="recipientAliasDiv">          
           <table id = "recipientAliasTable" width="600px" border="1" cellpadding="3" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="display:none"></th>
                            <th>Recipient ID</th>
                            <th>Recipient Source</th>
                            <th>ID Type</th>
                        </tr>
                    </thead>
                    <tbody id="recipientAliasTbody" name="recipientAliasTbody">
                    <s:if test="%{aliasList!=null && aliasList.size()>0}">
                        <s:iterator value="aliasList">
                            <tr>
                                <td style="display:none"><s:property value="pkAlias"/></td>
                                <td style="width:100px" onclick="insertTextBox(this)" textflag="false"><s:property value="AliasId"/></td>
                                <td style="width:130px" onclick="insertTextBox(this)"  textflag="false"><s:property value="aliasSource"/></td>
                                <td style="width:70px" onclick="insertTextBox(this)"  textflag="false"><s:property value="aliasType"/></td>
                            </tr>
                        </s:iterator>
                    </s:if>
                    </tbody>
                </table>
                <table width="600px" border="0" cellpadding="3" cellspacing="0">
                <tr>
                <td colspan="3" align="right">
                    <div>
                            <form>
                            	 <input type="text" style="width:80px;" size="5" value=" "  placeholder="e-Sign"/>&nbsp;
                                <input type="button" value="Cancel"  onclick="closepopup('recipientAlias');" class="close"/>
                                <input type="button" value="Save" onclick="saveAlias('1','recipientAliasTbody','recipientAlias')"/>
                            </form>
                    </div>
                </td>
                </tr>
            </table>
        </div>
    </s:form>
</div>
               
<div id="donorAlias" style="display: none">
<s:form id="donorAliasForm">
<img src = "images/icons/addnew.jpg" style="width:16;height:16;" onclick="displayResult('donorAliasTbody')" />
        <div id="donorAliasDiv">          
           <table id = "donorAliasTable" width="600px" border="1" cellpadding="3" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="display:none"></th>
                            <th>Donor ID</th>
                            <th> Source</th>
                            <th>ID Type</th>
                        </tr>
                    </thead>
                    <tbody id="donorAliasTbody" name="donorAliasTbody">
                    <s:if test="%{aliasList!=null && aliasList.size()>0}">
                        <s:iterator value="aliasList">
                            <tr>
                                <td style="display:none"><s:property value="pkAlias"/></td>
                                <td style="width:100px" onclick="insertTextBox(this)" textflag="false"><s:property value="AliasId"/></td>
                                <td style="width:130px" onclick="insertTextBox(this)"  textflag="false"><s:property value="aliasSource"/></td>
                                <td style="width:70px" onclick="insertTextBox(this)"  textflag="false"><s:property value="aliasType"/></td>
                            </tr>
                        </s:iterator>
                    </s:if>
                    </tbody>
            </table>
            <table width="600px" border="0" cellpadding="3" cellspacing="0">
                <tr>
                <td colspan="3" align="right">
                    <div>
                            <form>
                            <input type="text" style="width:80px;" size="5" value=""  placeholder="e-Sign"/>&nbsp;
                                <input type="button" value="Cancel" onclick="closepopup('donorAlias');" class="close"/>
                                <input type="button" value="Save" onclick="saveAlias('2','donorAliasTbody','donorAlias')"/>
                            </form>
                    </div>
                </td>
                </tr>
                </table>
    </div>
</s:form>
                </div>
<div id="productAlias" style="display: none">
<s:form id="productAliasForm">
<img src = "images/icons/addnew.jpg" style="width:16;height:16;" onclick="displayResult('productAliasTbody')" />
        <div id="productAliasDiv">          
           <table id = "productAliasTable" width="600px" border="1" cellpadding="3" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="display:none"></th>
                            <th>Product ID</th>
                            <th>Product Source</th>
                            <th>ID Type</th>
                        </tr>
                    </thead>
            <tbody id="productAliasTbody" name="productAliasTbody">
                    <s:if test="%{aliasList!=null && aliasList.size()>0}">
                        <s:iterator value="aliasList">
                            <tr>
                                <td style="display:none"><s:property value="pkAlias"/></td>
                                <td style="width:100px" onclick="insertTextBox(this)" textflag="false"><s:property value="AliasId"/></td>
                                <td style="width:130px" onclick="insertTextBox(this)"  textflag="false"><s:property value="aliasSource"/></td>
                                <td style="width:70px" onclick="insertTextBox(this)"  textflag="false"><s:property value="aliasType"/></td>
                            </tr>
                        </s:iterator>
                    </s:if>
                    </tbody>       
            </table>
            <table width="600px" border="0" cellpadding="3" cellspacing="0">
                <tr>
                <td colspan="3" align="right">
                    <div>
                            <form>
                            <input type="text" style="width:80px;" size="5" value=""  placeholder="e-Sign"/>&nbsp;
                                <input type="button" value="Cancel" onclick="closepopup('productAlias');" class="close"/>
                                <input type="button" value="Save" onclick="saveAlias('3','productAliasTbody','productAlias')"/>
                            </form>
                    </div>
                </td>
                </tr>
            </table>
        </div>
    </s:form>
</div>
<div id= "requiredDocument" style="display: none">
           <table id = "documentTable" width="600px" border="1" cellpadding="3" cellspacing="0">
            <img src = "images/icons/addnew.jpg" style="width:16;height:16;" onclick="displayResult1('documentTbody')" />          
                    <thead>
                        <tr>
                            <th>Document</th>
                            <th>Check</th>
                        </tr>
                    </thead>
                    <tbody id="documentTbody" name="documentTbody">
                        <s:if test="%{documentAdditiveList!=null && documentAdditiveList.size()>0}">
                        <s:iterator value="documentAdditiveList">
                            <tr>
                                <td style="display:none"><s:property value="documentAdditives"/></td>
                                <td style="width:130px" onclick="insertTextBox(this)"  textflag="false"><s:property value="column1"/></td>
                                <td style="width:70px" onclick="insertTextBox(this)"  textflag="false"><s:property value="column2"/></td>
                            </tr>
                        </s:iterator>
                    </s:if>
                    </tbody>
            </table>
            <table width="600px" border="0" cellpadding="3" cellspacing="0">
                <tr>
                <td colspan="3" align="right">
                    <div>
                            <form>
                            <input type="text" style="width:80px;" size="5" value=""  placeholder="e-Sign"/>&nbsp;
                                <input type="button" value="Cancel" onclick="closepopup('document');" class="close"/>
                                <input type="button" value="Save" onclick="saveDocumentAdditives('1','documentTable','requiredDocument','documentTbody')"/>
                            </form>
                    </div>
                </td>
                </tr>
                </table>
</div>
<div id="additivesDiv" style="display: none">
	
	     <img src = "images/icons/addnew.jpg" style="width:16;height:16;" onclick="displayResult1('additiveTbody')" />          
           <table id = "additivesTable" width="600px" border="1" cellpadding="3" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Anticoagulants & Additives</th>
                            <th>Volume For Additives (ml)</th>
                        </tr>
                    </thead>
                    <tbody id="additiveTbody" name="additiveTbody">
                    <s:if test="%{documentAdditiveList!=null && documentAdditiveList.size()>0}">
                        <s:iterator value="documentAdditiveList">
                            <tr>
                                <td style="display:none"><s:property value="documentAdditives"/></td>
                                <td style="width:130px" onclick="insertTextBox(this)"  textflag="false"><s:property value="column1"/></td>
                                <td style="width:70px" onclick="insertTextBox(this)"  textflag="false"><s:property value="column2"/></td>
                            </tr>
                        </s:iterator>
                    </s:if>
                    </tbody> 
            </table>
            <table width="600px" border="0" cellpadding="3" cellspacing="0">
                <tr>
                <td colspan="3" align="right">
                    <div>
                            <form>
                            <input type="text" style="width:80px;" size="5" value=""  placeholder="e-Sign"/>&nbsp;
                                <input type="button" value="Cancel" onclick="closepopup('additivesDiv');" class="close"/>
                                <input type="button" value="Save" onclick="saveDocumentAdditives('2','documentTable','additivesDiv','additiveTbody')"/>
                            </form>
                    </div>
                </td>
                </tr>
                </table>
   
</div>   
<div id= "itinerary" style="display: none">
           <table id = "abc5" width="600px" border="1" cellpadding="3" cellspacing="0">
            <img src = "images/icons/addnew.jpg" style="width:16;height:16;" onclick="displayResult('abc5')" />          
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Location</th>
                            <th>Status</th>
                        </tr>
                    </thead>
            </table>
            <table width="600px" border="0" cellpadding="3" cellspacing="0">
                <tr>
                <td colspan="3" align="right">
                    <div>
                            <form>
                            <input type="text" style="width:80px;" size="5" value=""  placeholder="e-Sign"/>&nbsp;
                                <input type="button" value="Cancel" onclick="closepopup('itinerary');" class="close"/>
                                <input type="button" value="Save" onclick="saveAlias()"/>
                            </form>
                    </div>
                </td>
                </tr>
                </table>
</div>
<div id= "attachDocument" style="display: none;">

    <!-- <iframe width="100%" height="125" src="jsp/fileUpload.jsp" style="border: medium none;" id="testFrame">
         <html>
            <body>
                <s:form action="addDocuments.action" namespace="/" method="POST" enctype="multipart/form-data">
                      <table id = "attachDocumentTable" width="600px" border="0" cellpadding="0" cellspacing="0">
                           <tbody>
                                <tr>
                                    <td>Attach Document : </td>
                                    <td ><s:file name="fileUpload" label="Select a File to upload" size="20" id="iteneraryDocument"/></td>
                                </tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr>
                                    <td colspan="2" align="center"><input type="button" value="Cancel"/>&nbsp;<s:submit value="submit" name="submit" /></td>
                                </tr>
                            </tbody>
                    </table>
                </s:form>
                </body>
            </html>
       
        </iframe>-->
</div>




