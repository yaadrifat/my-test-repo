<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript">
$(document).ready(function() {
	hide_slidewidget();
	$( ".task" ).css("width","20%");
	hideBarcodes();
	oTable = $('#pendingtask').dataTable({
		"bJQueryUI": true
	});
	oTable.thDatasorting('pendingtask');
	oTable = $('#assignedtask').dataTable({
		"bJQueryUI": true
	});
	oTable = $('#acceptedtask').dataTable({
		"bJQueryUI": true
	});
	
	$("select").uniform();	 
	$('.tbody tr').click(function(){
		  changeClass(this);
	
	});
});

</script>
 <div class="column">	
 <form>	
		<div  class="portlet">
			<div class="portlet-header addnew"><s:hidden id="moduleName" value=""/><s:hidden id="widgetName" value=""/><s:hidden id="pkNotes" value=""/>Daily Assignments</div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="pendingtask" class="display" >
				<thead>
						<tr>
							<th colspan="1">Task</th>
							<th colspan="1">Product ID</th>
							<th colspan="1">Recipient ID</th>
							<th colspan="1">Product Status</th>
							<th colspan="1">Task Status</th>
							<th colspan="1" width="25%">User Assignment</th>			
						</tr>
						<tr>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th width="25%"></th>			
						</tr>
					</thead>
					<tbody>
					<tr>
					<td><select>
							<option>Select</option>
							<option>Vol.REd.</option>
							<option>MNC</option>
							
							</select></td>
					<td><img src = "images/icons/addnew.jpg" style="cursor:pointer; float:right;"/></td>
						<td></td>
					<td>
					
					</td> 
					<td>
					
					</td>
				
					<td><input type="text" id="" class="task" value="Task1" />
						<select>
							<option>Select</option>
							<option>Primary</option>
							<!-- <option>Tech 3</option>
							<option>Tech 4</option> -->
						</select><img src = "images/icons/addnew.jpg" style="cursor:pointer;"/>
					</td>
				</tr>
					</tbody>
				
			</table>
		<div align="right" style="float:right;">
			
			<!-- <input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;&nbsp; -->
			 <input type="button" value="Save"/> 
	
		</div>
	</div>
	</div>	
	</form>
</div>
<%--  <div class="column">	
 <form>	
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value=""/><s:hidden id="widgetName" value=""/><s:hidden id="pkNotes" value=""/>Assigned Task</div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="assignedtask" class="display" >
				<thead>
						<tr>
							<th>Task</th>
							<th>Product ID</th>
							<th>Recipient ID</th>
							<th>Product Status</th>
							<th>Task Status</th>
							<th>User Assignment</th>				
						</tr>
					</thead>
				<tbody>
						<tr>
						<td>BMT123</td>
						<td>Pending for Acquisition</td>
						<td>Acquisition</td>
						<td><select><option>Select</option><option>Tech1</option><option>Tech2</option><option>Tech3</option></select></td> 
						</tr>
						<tr>
						<td>BMT123</td>
						<td>Storage</td>
						<td>Infusion release</td>
						<td><select><option>Select</option><option>Tech1</option><option>Tech2</option><option>Tech3</option></select></td> 
						</tr>
						<tr>
						<td>BMT123</td>
						<td>Accessed</td>
						<td>Original Product</td>
						<td><select><option>Select</option><option>Tech1</option><option>Tech2</option><option>Tech3</option></select></td> 
						</tr>
				 </tbody>	
			</table>
	</div>
	</div>	
	</form>
</div>
 <div class="column">	
 <form>	
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value=""/><s:hidden id="widgetName" value=""/><s:hidden id="pkNotes" value=""/>Accepted Task</div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="acceptedtask" class="display" >
				<thead>
						<tr>
							<th>Product</th>
							<th>Status</th>
							<th>Task</th>
							<th>Assigned To</th>				
						</tr>
					</thead>
					<tbody>
					<tr>
						<td>BMT128</td>
						<td>Pending in Acquisition</td>
						<td>Acquisition</td>
						<td><select><option>Select</option><option>Tech A</option><option>Tech B</option><option>Tech C</option></select></td> 
					</tr>	
					</tbody>
			</table>
	</div>
	</div>	
	</form>
</div>
	<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
				<table cellpadding="0" cellspacing="0" class="" align="" border="0" width="100%">
				<tr>
				<td>Next:</td>
				<td><div class="mainselection" >
				<select name="" id="" class=""><option>Select</option><option>Recurring Tasks</option><option>Logout</option></select></div</td>&nbsp;&nbsp;
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="" value="Next"/></td>
				</tr>
				</table>
		</form> 
		</div> --%>
<script>

$(function() {
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open');
	});

	/**For Notes**/
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='ui-addnew'></span>");
	
	$( ".portlet-header .ui-addnew" ).click(function() {
		clearPopUp();
		showPopUp("open","addNewDialog","Add New Product","950","520")
	});
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});
</script>