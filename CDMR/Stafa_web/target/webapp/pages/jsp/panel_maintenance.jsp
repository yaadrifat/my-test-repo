<%@ taglib prefix="s" uri="/struts-tags"%>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<link rel="stylesheet" href="css/cssmenuhorizontal.css" type="text/css" />

<script type="text/javascript" src="js/dataSearch.js"></script>

<script type="text/javascript" src="js/maintenance.js"></script>

<script type="text/javascript" src="js/smartspinner.js"></script>

<script type="text/javascript" src="js/protocolconfig.js"></script>
<link type="text/css" href="css/smartspinner.css" rel="stylesheet" />
<!-- <script type="text/javascript" src="js/panel_maintenance.js"></script>
-->
<script type="text/javascript" src="js/util.js"></script>
	<script src="js/jquery/jquery.ui.draggable.js"></script>
	<script src="js/jquery/jquery.ui.droppable.js"></script>
<style>

</style>

<script>

function loadProtocolPages() {
	var protocol = $("#protocolDetail #protocol").val();
	// alert("protocol " + protocol);
	var ajaxresult = jsonDataCall('loadProtocolPages', "jsonData={protocol:"
			+ protocol + "}");
	var protocolName = $("#protocolDetail #protocolName").val();
	var strBreadcrumb = "<a href='#' onclick='constructProtocolTable(true);'>Protocol Library</a> &gt; <a href='#' onclick='getProtocolDetails(\""
			+ protocol
			+ "\",\""
			+ protocolName
			+ "\");'> "
			+ protocolName
			+ "</a> &gt; Page Configuration";
	$("#breadcrumb").html(strBreadcrumb);
	var widgets ="";
	var pageWidgets ="";
	
	if (ajaxresult.widgetsList != null) {
		
		$(ajaxresult.widgetsList).each(function() {
			//alert("name" + this.name);
			// check for default
			widgets +='<ul class="ui-state-default" style="display: block;border:solid 1px;" ><label>'+this.name+'</label></ul>';
			//$( ".dragItem ul" ).draggable({ revert: "valid", connectToSortable: '.dropItem', helper: 'clone'});
		});

		$(ajaxresult.pageWidgetsList).each(function() {

			
		});

		$("#dragDiv").html(widgets);

		dropevent();

		$("#protocolDetails").addClass("hidden");
		$("#protocollist").addClass("hidden");
		$("#divProtocolQC").addClass("hidden");
		$("#protocolCalc").removeClass("hidden");
	}

}

$(document).ready(function(){
	show_slidewidgets('slidecolumnfilter',false);
	hide_slidecontrol();
	  constructProtocolTable(false);
	  clearTracker();
	  show_innernorth();
})

function savePages(){
	try{
		if(checkValidation()){
				if($("#protocolHidden").val()=="saveProtocols"){
						if(saveProtocols()){
						return true;
					} 
				}else if($("#protocolHidden").val()=="ProtocolDetails"){
					 if(updateProtocol()){
						return true;
					} 
				}else if($("#protocolHidden").val()=="ProtocolQC"){
						if(saveProtocolQC()){
						return true;
					} 
				}else if($("#protocolHidden").val()=="ProtocolInventory"){
					if(save_inventory()){
						return true;
					}
				}
		}
	}catch(e){
		alert("exception " + e);
	}
}

function getInventory(){
	//alert("getInventory()");
	$("#protocolHidden").val("ProtocolInventory");
	$("#inventory").removeClass("hidden");
	$("#protocolDetails").addClass("hidden");
	$("#protocollist").addClass("hidden");
	$("#divProtocolQC").addClass("hidden");
	$("#protocolCalc").addClass("hidden");
	$("#divDonorLabQC").addClass("hidden");
	
	$("#spanReagents").html("Reagents & Supplies");
	$("#spanEquipment").html("Equipment");
	//regentSupplies,equipment
	constructTables(true);
	
}



function add_ProtocolRegent(){
	//alert("add_ProtocolRegent");
	 var newRow = "<tr><td>&nbsp;</td><td><input type='checkbox' id='protocolReagents' name ='protocolReagents' value='0' /></td><td><input type='text' id='reagentsName' name='reagentsName' value='' class='plain' onkeyup='setReagentsDescription(this);' onchange='setReagentsDescription(this);' /></td>";
	
	 	newRow+="<td><span id='spandesc'/></td>";
	 	newRow+="<td><input type='text' name='quantity' id='quantity' value='' /></td>";
	   newRow += " </tr>";
	   $("#regentSupplies").prepend(newRow); 
	   $(".plain").each(function (){
		   $(this).removeClass("plain");
		   setObjectAutoComplete($(this),"reagents");
	   });
	/*   
	   $('#reagentsName').on('input', function() {
		    // do your stuff
		alert("The text has been changed.");
		});
	*/
}

function edit_ProtocolRegent(){
	$("#regentSupplies tbody tr").each(function (row){
		 if($(this).find("#protocolReagents").is(":checked")){
			 rowHtml =  $(this).closest("tr");
			 $(this).find("td").each(function (col){
				 if(col ==2 ){
						if ($(this).find("#reagentsName").val()== undefined){
							$(this).html("<input type='text' name='reagentsName' class='plain' id='reagentsName' value='" + $(this).text() +"'/>");
							$(".plain").each(function (){
								   $(this).removeClass("plain");
								   setObjectAutoComplete($(this),"reagents");
							   });
						}
					}else if(col ==4  ){ 
						if ($(this).find("#quantity").val()== undefined){
							$(this).html("<input type='text' name='quantity' id='quantity' value='" + $(this).text() +"'/>");
						}
					}
		 });
		}
	 });
	
}


function edit_Equipment(){
	$("#equipment tbody tr").each(function (row){
		 if($(this).find("#protocolReagents").is(":checked")){
			 rowHtml =  $(this).closest("tr");
			 $(this).find("td").each(function (col){
				 if(col ==2 ){
						if ($(this).find("#reagentsName").val()== undefined){
							$(this).html("<input type='text' name='reagentsName' class='plain' id='reagentsName' value='" + $(this).text() +"'/>");
							$(".plain").each(function (){
								   $(this).removeClass("plain");
								   setObjectAutoComplete($(this),"equipments");
							   });
						}
					}
		 });
		}
	 });
	
}

function delete_Inventory(tableId){
		//alert("delete_Inventory " + tableId );
		var deletedIds="";
		try{
			$("#"+tableId+" tbody tr").each(function (row){
				if($(this).find("#protocolReagents").is(":checked")){
					deletedIds += $(this).find("#protocolReagents").val() + ",";
				}
			});
			//	alert("deletedIds "+ deletedIds);
			var yes=confirm(confirm_deleteMsg);
			if(!yes){
				return;
			}
			  if(deletedIds.length >0){
				  deletedIds = deletedIds.substr(0,deletedIds.length-1);
				  //alert("deletedIds " + deletedIds);
				  jsonData = "{deletedIds:'"+deletedIds+"',domainName:'DOMAIN_PROTOCOLREAGENTSSUPPLIES',domainKey:'PROTOCOLREAGENTSSUPPLIES_DOMAINKEY'}";
				  url="commonDelete";
			        //alert("json data " + jsonData);
			     response = jsonDataCall(url,"jsonData="+jsonData);
			     constructTables(true);
			  }
		}catch(error){
			alert("error " + error);
		}
			  
}
function setReagentsDescription(obj){
	var rowHtml = $(obj).closest('tr');
	$(rowHtml).find("#spandesc").html("");
	var tmpVal = $("#reagents option:contains('" + $.trim($(rowHtml).find("#reagentsName").val()) + "')").val();
	var tmpDesc = $("#reagentsdesc option[value='"+ tmpVal +"']").text();
	$(rowHtml).find("#spandesc").html(tmpDesc);
}

function add_Equipment(){
	//alert("add_Equipment");
	 var newRow = "<tr><td>&nbsp;</td><td><input type='checkbox' id='protocolReagents' name ='protocolReagents' value='0' /></td><td><input type='text' id='reagentsName' name='reagentsName' value='' class='plain' onkeyup='setEquipementDescription(this);' /></td>";
	 	newRow+="<td><span id='spandesc'/></td>";
	 	newRow += " </tr>";
	   $("#equipment").prepend(newRow); 
	   $(".plain").each(function (){
		   $(this).removeClass("plain");
		   setObjectAutoComplete($(this),"equipments");
	   });
}

function setEquipementDescription(obj){
	var rowHtml = $(obj).closest('tr');
	$(rowHtml).find("#spandesc").html("");
	var tmpVal = $("#equipments option:contains('" + $.trim($(rowHtml).find("#reagentsName").val()) + "')").val();
	var tmpDesc = $("#equipmentsdesc option[value='"+ tmpVal +"']").text();
	$(rowHtml).find("#spandesc").html(tmpDesc);
}


function save_inventory(){
	//alert("save inventory");
	var rowHtml;
	var rowData ="";
	var protocol =    $("#protocolDetail #protocol").val();
	$("#regentSupplies #reagentsName").each(function (row){
		 rowHtml =  $(this).closest("tr");
		 var tmpVal = $("#reagents option:contains('" + $.trim($(this).val()) + "')").val();
		 rowData+= "{protocolReagents:'"+$(rowHtml).find("#protocolReagents").val()+"',protocol:'"+protocol+"',reagents:'"+tmpVal+"',quantity:'"+$(rowHtml).find("#quantity").val()+"',prs_type:'REAGENTS'},";
	});
	$("#equipment #reagentsName").each(function (row){
		 rowHtml =  $(this).closest("tr");
		 var tmpVal = $("#equipments option:contains('" + $.trim($(this).val()) + "')").val();
		 rowData+= "{protocolReagents:'"+$(rowHtml).find("#protocolReagents").val()+"',protocol:'"+protocol+"',reagents:'"+tmpVal+"',prs_type:'EQUIPMENT'},";
	});
	
	
	if(rowData.length >0){
        rowData = rowData.substring(0,(rowData.length-1));
    }
	//alert("rowData " + rowData);
	 url ="saveProtocolRegents";
	   response = jsonDataCall(url,"jsonData={protocolRegentsData:["+rowData+"]}");
	   getInventory(); 
}

function constructTables( flag){
	var protocol =    $("#protocolDetail #protocol").val();
	var criteria = " and  FK_PROTOCOL ="+ protocol  +" and PRS_TYPE = 'REAGENTS'" ;
	var protocolReagents_serverParam = function ( aoData ) {
		aoData.push( { "name": "application", "value": "stafa"});
		aoData.push( { "name": "module", "value": "maintenance_panelconfig_Protocol_Reagents"});	
		aoData.push( { "name": "criteria", "value": criteria});	
									
		aoData.push( {"name": "col_1_name", "value": "" } );
		aoData.push( { "name": "col_1_column", "value": ""});
		
		aoData.push( { "name": "col_2_name", "value": "lower(RS_NAME)"});
		aoData.push( { "name": "col_2_column", "value": "lower(RS_NAME)"});
		
		aoData.push( {"name": "col_3_name", "value": "lower(RS_DESCRIPTION)" } );
		aoData.push( { "name": "col_3_column", "value": "lower(RS_DESCRIPTION)"});
		
		aoData.push( {"name": "col_4_name", "value": "QUANTITY" } );
		aoData.push( { "name": "col_4_column", "value": "QUANTITY"});
		};

	var protocolReagents_aoColumnDef =  [ 
				{
					"aTargets" : [ 0 ],
					"mDataProp" : function(source, type, val) {
						return "";
					}
				},
				{
					"sTitle" : "Check All <input type='checkbox'  id='checkallReagents' onclick='checkall(\"regentSupplies\",this)'>",
					"aTargets" : [ 1 ],
					"mDataProp" : function(source, type, val) {
						return "<input type='checkbox' id='protocolReagents' name='protocolReagents' value='"+source[0] +"' />";
					}
				},
				{
					"sTitle" : "Name",
					"aTargets" : [ 2 ],
					"mDataProp" : function(source, type, val) {
						link = source[1];
						return link;
					}
				},
							{
					"sTitle" : "Description",
					"aTargets" : [ 3 ],
					"mDataProp" : function(source, type, val) {
						return source[2];
					}
				},
				{
					"sTitle" : "Quantity",
					"aTargets" : [ 4 ],
					"mDataProp" : function(source, type, val) {
						return source[3];;
					}
				} ];

		var protocolReagents_aoColumn = [ 
			{"bSortable" : false},
			{"bSortable" : false},
			null, null,null ];

		var protocolReagents_ColManager = function() {
			$("#protocolReagentColumn").html($('#regentSupplies_wrapper .ColVis'));
		};
		
		var criteria1 = " and  FK_PROTOCOL ="+ protocol  +" and PRS_TYPE = 'EQUIPMENT'" ;
		var protocolEquipment_serverParam = function ( aoData ) {
			aoData.push( { "name": "application", "value": "stafa"});
			aoData.push( { "name": "module", "value": "maintenance_panelconfig_Protocol_Reagents"});	
			aoData.push( { "name": "criteria", "value": criteria1});	
										
			aoData.push( {"name": "col_1_name", "value": "" } );
			aoData.push( { "name": "col_1_column", "value": ""});
			
			aoData.push( { "name": "col_2_name", "value": "lower(RS_NAME)"});
			aoData.push( { "name": "col_2_column", "value": "lower(RS_NAME)"});
			
			aoData.push( {"name": "col_3_name", "value": "lower(RS_DESCRIPTION)" } );
			aoData.push( { "name": "col_3_column", "value": "lower(RS_DESCRIPTION)"});
			
			
			};

		var protocolEquipment_aoColumnDef =  [ 
					{
						"aTargets" : [ 0 ],
						"mDataProp" : function(source, type, val) {
							return "";
						}
					},
					{
						"sTitle" : "Check All <input type='checkbox'  id='checkallReagents' onclick='checkall(\"regentSupplies\",this)'>",
						"aTargets" : [ 1 ],
						"mDataProp" : function(source, type, val) {
							return "<input type='checkbox' id='protocolReagents' name='protocolReagents' value='"+source[0] +"' />";
						}
					},
					{
						"sTitle" : "Name",
						"aTargets" : [ 2 ],
						"mDataProp" : function(source, type, val) {
							link = source[1];
							return link;
						}
					},
								{
						"sTitle" : "Description",
						"aTargets" : [ 3 ],
						"mDataProp" : function(source, type, val) {
							return source[2];
						}
					
					} ];

			var protocolEquipment_aoColumn = [ 
				{"bSortable" : false},
				{"bSortable" : false},
				null, null];

			var protocolEquipment_ColManager = function() {
				$("#protocolEquipmentColumn").html($('#equipment_wrapper .ColVis'));
			};
		
		
		
		
		constructTable(flag, 'regentSupplies', protocolReagents_ColManager,protocolReagents_serverParam, protocolReagents_aoColumnDef,
				protocolReagents_aoColumn);
		
		constructTable(flag, 'equipment', protocolEquipment_ColManager,protocolEquipment_serverParam, protocolEquipment_aoColumnDef,
				protocolEquipment_aoColumn);
		

	}

	var oldQCids = ",";

	function constructProtocolTable(flag) {
		//alert("construct table");
		elementcount = 0;
		$('.progress-indicator').css('display', 'block');
		var strBreadcrumb = "<a href='#' onclick='constructProtocolTable(true);'>Protocol Library</a>";
		$("#breadcrumb").html(strBreadcrumb);
		$("#protocollist").removeClass("hidden");
		$("#protocolDetails").addClass("hidden");
		$("#divProtocolQC").addClass("hidden");
		$("#divProtocolQC").removeClass("ui-helper-clearfix");
		$("#protocolCalc").addClass("hidden");
		$("#protocolCalc").removeClass("ui-helper-clearfix");
		$("#divDonorLabQC").removeClass("ui-helper-clearfix");
		$("#divDonorLabQC").addClass("hidden");

		/*		
		if(flag || flag =='true'){
			oTable=$('#porotocolTable').dataTable().fnDestroy();
			//alert(" destroy 1");
			//$('#porotocolTable').dataTable().fnClearTable();
		}
		 */
		var link;

		var porotocolTable_serverParam = function(aoData) {
			aoData.push({
				"name" : "application",
				"value" : "stafa"
			});
			aoData.push({
				"name" : "module",
				"value" : "maintenance_panelconfig_protocol"
			});

			aoData.push({
				"name" : "col_1_name",
				"value" : " lower(protocolName) "
			});
			aoData.push({
				"name" : "col_1_column",
				"value" : "lower(protocolName)"
			});

			aoData.push({
				"name" : "col_2_name",
				"value" : " lower(protocolCode) "
			});
			aoData.push({
				"name" : "col_2_column",
				"value" : "lower(protocolCode)"
			});

			aoData.push({
				"name" : "col_3_name",
				"value" : "lower(protocolModule)"
			});
			aoData.push({
				"name" : "col_3_column",
				"value" : "lower(protocolModule)"
			});

			aoData.push({
				"name" : "col_4_name",
				"value" : " lower(protocolDesc) "
			});
			aoData.push({
				"name" : "col_4_column",
				"value" : "lower(protocolDesc)"
			});
		};

		var porotocolTable_aoColumnDef = [
				{
					"aTargets" : [ 0 ],
					"mDataProp" : function(source, type, val) {
						return "<input type='checkbox' id='protocol' name='protocol' value='"+source.protocol +"' >";
					}
				},

				{
					"sTitle" : "Name",
					"aTargets" : [ 1 ],
					"mDataProp" : function(source, type, val) {
						link = "<a href='#' onclick='javascript:getProtocolDetails(\""
								+ source.protocol
								+ "\",\""
								+ source.protocolName
								+ "\");'>"
								+ source.protocolName + "</a>";
						return link;
					}
				},
				{
					"sTitle" : "Code",
					"aTargets" : [ 2 ],
					"mDataProp" : function(source, type, val) {
						link = "<input type='hidden' name='appLock' id='appLock' value='" + source.appLock +"' />"
								+ source.protocolCode;
						return link;
					}
				},
				{
					"sTitle" : "Module",
					"aTargets" : [ 3 ],
					"mDataProp" : function(source, type, val) {
						return source.protocolModule;
					}
				},
				{
					"sTitle" : "Description",
					"aTargets" : [ 4 ],
					"mDataProp" : function(source, type, val) {
						return source.protocolDesc;
					}
				},
				{
					"sTitle" : "Activated",
					"aTargets" : [ 5 ],
					"mDataProp" : function(source, type, val) {
						if (source.isActivate == 1) {
							link = "<input type='radio' name='active_"+source.protocol+"' id='activeyes' value='Yes' checked='checked' >Yes</input>";
							link += "<input type='radio' name='active_"+source.protocol+"' id='active' value='No' >No</input>";
						} else {
							link = "<input type='radio' name='active_"+source.protocol+"' id='activeyes' value='Yes'  >Yes</input>";
							link += "<input type='radio' name='active_"+source.protocol+"' id='active' value='No' checked='checked'>No</input>";
						}
						return link;
					}
				} ];

		var porotocolTable_aoColumn = [ {
			"bSortable" : false
		}, null, null, null, null, null ];

		var porotocolTable_ColManager = function() {
			$("#protocolColumn").html($('.ColVis:eq(0)'));
		};
		constructTable(flag, 'porotocolTable', porotocolTable_ColManager,
				porotocolTable_serverParam, porotocolTable_aoColumnDef,
				porotocolTable_aoColumn);

		//	alert("end of dt");
	}

	function addRows(protocol, protocolName, protocolCode, protocolDesc,
			copyProtocol, activeFlag) {
		var moduleDrop = "<select name='module' id='module'>"
				+ $.trim($("#codeListModule").html()) + "</select>";
		elementcount += 1;
		var newRow = "<tr><td><input type='checkbox' id='protocol' value='"+protocol+"'> <input type='hidden' id='copyProtocol' name='copyProtocol' value='"+copyProtocol+"' /></td><td><input type='text' id='protocolName' name='protocolName' value='"+protocolName+"' /></td><td>&nbsp;</td>";
		newRow += "<td>" + moduleDrop + "</td>";
		newRow += "<td><input type='text' id='protocolDesc' name='protocolDesc' value='"+protocolDesc+"' /></td>";
		newRow += "<td><input type='radio' value='Yes' name='active_a"
				+ elementcount + "' id='activeyes';";
		if (activeFlag) {
			newRow += " checked='checked' ";
		}
		newRow += " >Yes</input><input type='radio' value='No' name='active_a"
				+ elementcount + "' id='active' ";
		if (!activeFlag) {
			newRow += " checked='checked' ";
		}
		newRow += "  >No</input></td></tr>";
		$("#porotocolTable tbody[role='alert']").prepend(newRow);

		$("#porotocolTable .ColVis_MasterButton").triggerHandler("click");
		$('.ColVis_Restore:visible').triggerHandler("click");
		$("div.ColVis_collectionBackground").trigger("click");
		$.uniform.restore('select');
		$('select').uniform();
		
		  var param ="{module:'APHERESIS',page:'PROTOCOLLIBRARY'}";
			markValidation(param);  
	}

	function saveProtocols() {
		$('.progress-indicator').css('display', 'block');
		var rowData = "";
		var protocol1;
		// var protocolName;
		// var protocolCode;
		var protocolModule;
		// var copyProtocol;
		// var protocolDesc ;

		var isActivate = 0;
		$("#porotocolTable").find("#protocolName").each(
				function(row) {
					rowHtml = $(this).closest("tr");
					isActivate = 0;
					protocolNames = "";
					protocolCodes = "";
					protocolModule = "";
					copyProl = "";
					protocolDescription = "";
					apLock = "";
					protocol1 = $(rowHtml).find("#protocol").val();
					//alert("protocol " + protocol);
					copyProl = $(rowHtml).find("#copyProtocol").val();
					if (copyProl == undefined) {
						copyProl = 0;
					}
					//alert("copyProtocol " + copyProtocol);
					protocolNames = $(rowHtml).find("#protocolName").val();
					//alert("protocolName " + protocolName);

					protocolCodes = $(rowHtml).find("#protocolCode").val();
					if (protocolCodes == undefined) {
						protocolCodes = protocolNames.replace(/\s/g, '')
								.toLowerCase()
					}
					protocolModule = $(rowHtml).find("#module").val();

					protocolDescription = $(rowHtml).find("#protocolDesc")
							.val();
					apLock = $(rowHtml).find("#appLock").val();
					if (apLock != 1) {
						apLock = 0;
					}
					if ($(rowHtml).find("#activeyes").is(":checked")) {
						isActivate = 1;
					} else {
						isActivate = 0;
					}

					if (protocolNames != undefined && protocolNames != "") {
						rowData += "{protocol:" + protocol1 + ",isActivate ="
								+ isActivate + ",protocolName:'"
								+ protocolNames + "',protocolCode:'"
								+ protocolCodes + "',protocolModule:'"
								+ protocolModule + "',appLock:'" + apLock
								+ "',protocolDesc:'" + protocolDescription
								+ "',copyProtocol:'" + copyProl + "'},";

					}
				});

		if (rowData.length > 0) {
			rowData = rowData.substring(0, (rowData.length - 1));
		}
		url = "saveProtocol";
		response = jsonDataCall(url, "jsonData={protocolData:[" + rowData
				+ "]}");

		//BB$("select").uniform();
		alert("Data Saved Successfully");
		$('.progress-indicator').css('display', 'none');
		constructProtocolTable(true);
		var url = "loadProtocols";
		var result = ajaxCall(url, '');
		$("#portocolFacet").replaceWith($('#portocolFacet', $(result)));
		return true;

	}

	function editRows() {
		//  alert("edit Rows");

		$("#porotocolTable tbody tr")
				.each(
						function(row) {
							if ($(this).find("#protocol").is(":checked")) {

								var tmpVal = $(this).closest("tr").find(
										"#appLock").val();
								$(this)
										.find("td")
										.each(
												function(col) {
													if (col == 1) {
														if ($(this)
																.find(
																		"#protocolName")
																.val() == undefined) {
															$(this)
																	.html(
																			"<input type='text' id='protocolName' name='protocolName' value='"
																					+ $(
																							this)
																							.text()
																					+ "' />");
														}
													} else if (col == 2) {
														if ($(this)
																.find(
																		"#protocolCode")
																.val() == undefined) {

															var tmpHtml = "<input type='hidden' name='appLock' id='appLock' value='" +tmpVal  +"' />";
															tmpHtml += "<input type='text' id='protocolCode' name='protocolCode' value='"
																	+ $(this)
																			.text()
																	+ "' ";

															if (tmpVal == 1) {
																tmpHtml += " readonly='true' "
															}
															tmpHtml += "/>";
															$(this).html(
																	tmpHtml);
														}
													} else if (col == 3) {

														if ($(this).find(
																"#module")
																.val() == undefined) {
															var oldModuleVal = $(
																	this)
																	.text();
															var moduleDrop = "<select name='module' id='module'>"
																	+ $
																			.trim($(
																					"#codeListModule")
																					.html())
																	+ "</select>";
															$(this).html(
																	moduleDrop);
															$(this)
																	.find(
																			"#module")
																	.val(
																			oldModuleVal);
															if (tmpVal == 1) {
																$(this)
																		.find(
																				"#module")
																		.attr(
																				"disabled",
																				true);
															}
															//alert("sss");
															$.uniform
																	.restore('select');
															$("select")
																	.uniform();
														}
													} else if (col == 4) {
														if ($(this)
																.find(
																		"#protocolDesc")
																.val() == undefined) {
															$(this)
																	.html(
																			"<input type='text' id='protocolDesc' name='protocolDesc' value='"
																					+ $(
																							this)
																							.text()
																					+ "' />");
														}
													}
												});
							}
						});
		  var param ="{module:'APHERESIS',page:'PROTOCOLLIBRARY'}";
			markValidation(param);  
	}

	// ----------- protocol QC ---
	var oTable;
	function constructQCTable(flag) {
		//alert("construct table");
		elementcount = 0;
		$('.progress-indicator').css('display', 'block');
		var criteria = "";

		//?BB module is not saver criteria = " and lower(module) =lower('"+$("#protocolModule").val()+"') ";
		criteria = " ";
		var protocol = $("#protocolDetail #protocol").val();
		var protocolName = $("#protocolDetail #protocolName").val();
		var strBreadcrumb = "<a href='#' onclick='constructProtocolTable(true);'>Protocol Library</a> &gt; <a href='#' onclick='getProtocolDetails(\""
				+ protocol
				+ "\",\""
				+ protocolName
				+ "\");'> "
				+ protocolName
				+ "</a> &gt; Selected Default QCs for the Protocol";
		$("#breadcrumb").html(strBreadcrumb);
		$("#protocollist").addClass("hidden");
		$("#protocolDetails").addClass("hidden");
		$("#divProtocolQC").removeClass("hidden");
		$("#divProtocolQC").removeClass("ui-helper-clearfix");

		$("#divDonorLabQC").removeClass("ui-helper-clearfix");
		$("#divDonorLabQC").addClass("hidden");

		var porotocolQCTable_serverParam = function(aoData) {
			aoData.push({
				"name" : "application",
				"value" : "stafa"
			});
			aoData.push({
				"name" : "module",
				"value" : "maintenance_panelconfig_protocol_QC"
			});
			aoData.push({
				"name" : "criteria",
				"value" : criteria
			});
			aoData.push({
				"name" : "col_1_name",
				"value" : ""
			});
			aoData.push({
				"name" : "col_1_column",
				"value" : ""
			});

			aoData.push({
				"name" : "col_2_name",
				"value" : " description "
			});
			aoData.push({
				"name" : "col_2_column",
				"value" : "lower(description)"
			});

			aoData.push({
				"name" : "col_3_name",
				"value" : " type "
			});
			aoData.push({
				"name" : "col_3_column",
				"value" : "lower(type)"
			});

			aoData.push({
				"name" : "col_4_name",
				"value" : " lower(description) "
			});
			aoData.push({
				"name" : "col_4_column",
				"value" : "lower(description)"
			});
		};
		var porotocolQCTable_aoColumnDef = [
				{
					"aTargets" : [ 0 ],
					"mDataProp" : function(source, type, val) {
						return "";
					}
				},
				{
					"sTitle" : "Check All",
					"aTargets" : [ 1 ],
					"mDataProp" : function(source, type, val) {
						return "<input type='checkbox' id='protocolQC' name='protocolQC' value='"+source.pkCodelst +"' >";
					}
				},
				{
					"sTitle" : "Name",
					"aTargets" : [ 2 ],
					"mDataProp" : function(source, type, val) {
						//link = "<a href='#' onclick='javascript:alert();'>" + source.description + "</a>";
						return source.description;
					}
				},
				{
					"sTitle" : "Code",
					"aTargets" : [ 3 ],
					"mDataProp" : function(source, type, val) {
						return source.type;
					}
				},
				{
					"sTitle" : "Description",
					"aTargets" : [ 4 ],
					"mDataProp" : function(source, type, val) {
						return source.comments;
					}
				}
			/* 	{
					"sTitle" : "Activated",
					"aTargets" : [ 5 ],
					"mDataProp" : function(source, type, val) {
						if (source.isHide == "N") {
							link = "<input type='radio' name='product_active_"+source.pkCodelst+"' id='activeyes' value='Yes' checked='checked' >Yes</input>";
							link += "<input type='radio' name='product_active_"+source.pkCodelst+"' id='active' value='No' >No</input>";
						} else {
							link = "<input type='radio' name='product_active_"+source.pkCodelst+"' id='activeyes' value='Yes'  >Yes</input>";
							link += "<input type='radio' name='product_active_"+source.pkCodelst+"' id='active' value='No' checked='checked'>No</input>";
						}
						return link;
					} }*/
					];
		
		var donorQCTable_aoColumnDef = [
		                    				{
		                    					"aTargets" : [ 0 ],
		                    					"mDataProp" : function(source, type, val) {
		                    						return "";
		                    					}
		                    				},
		                    				{
		                    					"sTitle" : "Check All",
		                    					"aTargets" : [ 1 ],
		                    					"mDataProp" : function(source, type, val) {
		                    						return "<input type='checkbox' id='protocolQC' name='protocolQC' value='"+source.pkCodelst +"' >";
		                    					}
		                    				},
		                    				{
		                    					"sTitle" : "Name",
		                    					"aTargets" : [ 2 ],
		                    					"mDataProp" : function(source, type, val) {
		                    						//link = "<a href='#' onclick='javascript:alert();'>"+ source.description + "</a>";
		                    						return source.description;
		                    					}
		                    				},
		                    				{
		                    					"sTitle" : "Code",
		                    					"aTargets" : [ 3 ],
		                    					"mDataProp" : function(source, type, val) {
		                    						return source.type;
		                    					}
		                    				},
		                    				{
		                    					"sTitle" : "Description",
		                    					"aTargets" : [ 4 ],
		                    					"mDataProp" : function(source, type, val) {
		                    						return source.comments;
		                    					}
		                    				}
		                    				/*{
		                    					"sTitle" : "Activated",
		                    					"aTargets" : [ 5 ],
		                    					"mDataProp" : function(source, type, val) {
		                    						if (source.isHide == "N") {
		                    							link = "<input type='radio' name='donor_active_"+source.pkCodelst+"' id='activeyes' value='Yes' checked='checked' >Yes</input>";
		                    							link += "<input type='radio' name='donor_active_"+source.pkCodelst+"' id='active' value='No' >No</input>";
		                    						} else {
		                    							link = "<input type='radio' name='donor_active_"+source.pkCodelst+"' id='activeyes' value='Yes'  >Yes</input>";
		                    							link += "<input type='radio' name='donor_active_"+source.pkCodelst+"' id='active' value='No' checked='checked'>No</input>";
		                    						}
		                    						return link;
		                    					}}*/
		                    				];
		
		
		var porotocolQCTable_aoColumn = [ {
			"bSortable" : false
		}, {
			"bSortable" : false
		}, null, null, null ];
		var porotocolQCTable_ColManager = function() {

			$("#productQCColumn").html($('#porotocolQCTable_wrapper .ColVis'));
		};
		constructTable(flag, 'porotocolQCTable', porotocolQCTable_ColManager,
				porotocolQCTable_serverParam, porotocolQCTable_aoColumnDef,
				porotocolQCTable_aoColumn);

		$("#donorLabQC").removeClass("ui-helper-clearfix");
		$("#donorLabQC").addClass("hidden");

		if ($("#protocolModule").val() == "Apheresis") {

			$("#divDonorLabQC").removeClass("hidden");
			$("#divDonorLabQC").removeClass("ui-helper-clearfix");

			var donorQCTable_ColManager = function() {
				$("#donorQCColumn").html($('#donorQCTable_wrapper .ColVis'));
			};

			constructTable(flag, 'donorQCTable', donorQCTable_ColManager,
					porotocolQCTable_serverParam, donorQCTable_aoColumnDef,
					porotocolQCTable_aoColumn);
		}

		return true;
	}

	function getProtocolQC() {
		$("#protocolHidden").val("ProtocolQC");
		//alert("protocol QC");
		var protocol = $("#protocolDetail #protocol").val();
		// alert("protocol " + protocol);
		$("#protocolDetails").addClass("hidden");
		$("#protocollist").addClass("hidden");
		//$("#divProtocolQC").removeClass("hidden");
		var flag = false;
		if ($("#porotocolQCTable tbody tr").length > 0) {
			flag = true
		}
		//?BB constructQCTable(flag);

		oldQCids = $("#oldQCs").val();
		//alert(" product qc test " + oldQCids);

		if (constructQCTable(flag)) {
			$("#porotocolQCTable tbody tr")
					.each(
							function(row) {
								if (oldQCids
										.indexOf((","
												+ $(this).find("#protocolQC")
														.val() + ",")) != -1) {
									$(this).find("#protocolQC").attr('checked',
											'checked');
								} else {
									$(this).find("#protocolQC").removeAttr(
											'checked');
								}
							});
			oldQCids = $("#donorOldQCs").val();
			$("#donorQCTable tbody tr")
					.each(
							function(row) {
								if (oldQCids
										.indexOf((","
												+ $(this).find("#protocolQC")
														.val() + ",")) != -1) {
									$(this).find("#protocolQC").attr('checked',
											'checked');
								} else {
									$(this).find("#protocolQC").removeAttr(
											'checked');
								}
							});
		}
		/*$("#porotocolQCTable").ajaxComplete(function(e, xhr, settings) {	
			alert("ajax complete");
			  $("#porotocolQCTable tbody tr").each(function (row){
					 if(oldQCids.indexOf(("," + $(this).find("#protocolQC").val() +","))!=-1){
						 $(this).find("#protocolQC").attr('checked','checked');
					 }else{
						 $(this).find("#protocolQC").removeAttr('checked');
					 }
				});
			 alert("end of ajax");
		});
		
		 oldQCids =$("#donorOldQCs").val();
		 alert("oldQCids" + oldQCids)
		$("#donorQCTable").ajaxComplete(function(e, xhr, settings) {	
			  $("#donorQCTable tbody tr").each(function (row){
					 if(oldQCids.indexOf(("," + $(this).find("#protocolQC").val() +","))!=-1){
						 $(this).find("#protocolQC").attr('checked','checked');
					 }else{
						 $(this).find("#protocolQC").removeAttr('checked');
					 }
				});
		});
		 */
		return true;
	}

	//--------------- end of new protocol config--------

	var inputCount = 0;
	function dropevent() {
	
		/*
		  /*Draggable Items functions Start*/
			$( ".dragItem ul" ).draggable({ revert: "invalid", connectToSortable: '.dropItem'});
			$( ".dropItem ul" ).draggable({ revert: "invalid", connectToSortable: '.dropItem1'});
			/*Draggable Items functions End*/
			
			/*Droppable Items functions Start*/
		//	$( "#dropDiv" ).droppable({ accept: '.dragItem li' });
			$( ".dropItem" ).sortable({
				connectWith: ".dropItem",
			});
			/*Droppable Items functions End*/
		 
	
	}

	//==================End of new change =========

	function copyToNewProtocol1() {

		//alert("test");
		var protocolName = $("#newName").val();
		var protocolCode = $("#newCode").val();
		if (protocolName != undefined && protocolName != ""
				&& protocolCode != undefined && protocolCode != "") {
			$('.progress-indicator').css('display', 'block');
			var rowData = "{protocol:0,isActivate:1,protocolName:'"
					+ protocolName + "',protocolCode:'" + protocolCode
					+ "',protocolDesc:'" + $("#protocolDetail #desc").val()
					+ "',copyProtocol:'" + $("#protocolDetail #protocol").val()
					+ "'}";

			var url = "saveProtocol";
			response = jsonDataCall(url, "jsonData={protocolData:[" + rowData
					+ "]}");

			//BB$("select").uniform();
			$('.progress-indicator').css('display', 'none');
			alertbox(save_success_msg, 'SUCCESS');

			url = "loadProtocols";
			var result = ajaxCall(url, '');
			$("#portocolFacet").replaceWith($('#portocolFacet', $(result)));

		} else {
			alert(" Please enter New Name & code");
		}

	}

	function copyProtocol(protocol, protocolName, protocolCode, protocolDesc) {
		//alert("copy protocol");
		var copyText = " - Copy";
		//	$("#breadlevel1").trigger('click');
		var strBreadcrumb = "<a href='#' id='breadlevel1' onclick='constructProtocolTable(true);'>Protocol Library</a> ";
		$("#breadcrumb").html(strBreadcrumb);
		$("#protocolDetails").addClass("hidden");
		$("#protocollist").removeClass("hidden");
		$("#divProtocolQC").addClass("hidden");
		$("#protocolCalc").addClass("hidden");
		addRows(0, protocolName + copyText, protocolCode + copyText,
				protocolDesc, protocol, false);
		//? Babu	
		//constructTable(false);
		/*	$("#porotocolTable").ajaxComplete(function(e, xhr, settings) {
		 addRows(0,protocolName +copyText,protocolCode +copyText,protocolDesc,protocol,false);	
		 });
		 */
	}

	function loadscripts() {
		//alert("test");
		//	setObjectAutoComplete($("#protocols"),"copyProtocol");
		//alert("test1");

	}

	var strBreadcrumb;



	function updateProtocol() {
		//alert("update protocol");
		$('.progress-indicator').css('display', 'block');
		var rowData = "";
		try {
			var protocol = $("#protocolDetail #protocol").val();
			var protocolName = $("#protocolDetail #protocolName").val();
			var protocolCode = $("#protocolDetail #protocolCode").val();
			var copyProtocol = $("#protocolDetail #copyProtocol").val();
			var protocolDesc = $("#protocolDetail #desc").val();
			// alert("protocolDesc " + protocolDesc);
			var isActivate = 0;
			if ($("#protocolDetail #activateYes").is(":checked")) {
				isActivate = 1;
			}
			//alert("trace 1");
			rowData = "{protocol:" + protocol + ",isActivate =" + isActivate
					+ ",protocolName:'" + protocolName + "',protocolCode:'"
					+ protocolCode + "',protocolDesc:'" + protocolDesc
					+ "',copyProtocol:'" + copyProtocol + "'}";
			//alert( rowData);
			url = "saveProtocol";
			response = jsonDataCall(url, "jsonData={protocolData:[" + rowData
					+ "]}");
		} catch (err) {
			alert(" error " + err);
		}
		//BB$("select").uniform();
		alert("Data Saved Successfully");
		$('.progress-indicator').css('display', 'none');
		return true;
	}

	function getProtocolDetails(protocol, protocolName, obj) {
		//alert("getProtocolDetails" + protocol);
		$("#protocolHidden").val("ProtocolDetails");
		if ($("#protocol").val() == "") {
			return false;
		}
		$('#portocolFacet tbody tr td a').each(function(i, el) {
			$(this).removeAttr("style");
		});
		if ($("#porotocolQCTable tbody tr").length > 0) {
			$('#porotocolQCTable').dataTable().fnDestroy();
		}
		$(obj).attr("style", "background:#33ffff");

		$("#oldQCs").val(",");
		$("#protocols").val("");

		$('.progress-indicator').css('display', 'block');

		try {
			var ajaxresult = jsonDataCall('getProtocolDetails',
					"jsonData={protocol:" + protocol + "}");

			$("#protocolDetails").removeClass("hidden");
			$("#protocollist").addClass("hidden");
			$("#divProtocolQC").addClass("hidden");
			$("#protocolCalc").addClass("hidden");
			$("#divDonorLabQC").addClass("hidden");

			$("#protocollist").removeClass("ui-helper-clearfix");
			strBreadcrumb = "<a href='#' id='breadlevel1' onclick='constructProtocolTable(true);'>Protocol Library</a> ";

			if (ajaxresult.protocolDetails != null) {

				$.each(ajaxresult.protocolDetails, function() {
					//alert("inside set")
					strBreadcrumb += " > " + this.protocolName;
					//alert("strBreadcrumb " + strBreadcrumb);
					$("#breadcrumb").html(strBreadcrumb);
					$("#protocolDetail #protocolName").val(this.protocolName);
					$("#protocolDetail #protocolCode").val(this.protocolCode);
					$("#protocolDetail #desc").val(this.protocolDesc);
					$("#protocolDetail #protocol").val(this.protocol);
					$("#protocolDetail #protocolModule").val(
							this.protocolModule);

					if (this.isActivate == 1) {
						$("#protocolDetail #activateYes").attr("checked",
								"true")
					} else {
						$("#protocolDetail #activateNo")
								.attr("checked", "true")
					}
					//$("#protocolName").val(this.protocolName);
					//alert("end of set");
				});

			}
			//This is used to get Only qc Details 

			oldQCids = ",";
			if (ajaxresult.exisQclst != null) {
				$
						.each(ajaxresult.exisQclst,
								function() {
									if (oldQCids.indexOf((","
											+ this.fkCodelstQc + ",")) == -1) {
										oldQCids += this.fkCodelstQc + ",";
									}
								});
				$("#oldQCs").val(oldQCids);
			}

			var donorOldQCids = ",";
			if (ajaxresult.existDonorQCList != null) {
				$
						.each(
								ajaxresult.existDonorQCList,
								function() {
									if (donorOldQCids.indexOf((","
											+ this.fkCodelstQc + ",")) == -1) {
										donorOldQCids += this.fkCodelstQc + ",";
									}
								});
				$("#donorOldQCs").val(donorOldQCids);
			}

		} catch (e) {
			alert("error in " + e);
		}

		$('.progress-indicator').css('display', 'none');
	}

	var selectedWidget;
	function widgetselect(obj) {
		$(".widget").removeClass('highlightWidget');
		$(obj).addClass('highlightWidget');
		selectedWidget = obj;
		var offset = $(obj).offset();
		//alert(" 1 this top " +  offset.left + " / " + );
		$(".dragItem").offset({
			top : offset.top
		});
		//alert("dragItem");
	}

	function createWidget() {
		var colDiv = "";

		colDiv += "<div class='widgetdiv'><a onclick=\"javascript:destroy(this);\"><img src=\"images/icons/closebox.png\"	height=\"16\" width=\"16\" class=\"destroywidget\"/></a><div id='widget' class=\" widget \" onclick='widgetselect(this);'>";
		colDiv += "<span id='portlet' ><input type='text' id='portlettitle' placeholder='Title'/></span>";
		colDiv += "<div class='portlet-content ' ></div>";
		// colDiv +="<img src='images/icons/closebox.png' onclick='javascript:destroy(this);'	height='16' width='16' class='destroy'/>";
		colDiv += "</div></div>";
		$("#dropDiv").append(colDiv);
		dropevent();
		$(".widget")
				.addClass(
						"ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
				.addClass("ui-widget-header ui-corner-all")
				.prepend(
						"<img src='images/icons/closebox.png' onclick='javascript:destroy(this);' height='16' width='16' class='destroy'/>");
	}

	function createColumns() {
		var noc = $("#noc").val();
		var colDiv = "";
		if (noc > 0) {
			for (i = 0; i < noc; i++) {
				colDiv += "<div class=\"dropItem\" >";
				colDiv += "<img src='images/icons/closebox.png' onclick='javascript:destroy(this);'	height='16' width='16' class='destroydiv'/>";
				colDiv += "</div>";
			}
		} else {
			//colDiv ="<div class=\"dropItem\" ></div>"
		}
		//   alert("colDiv " + colDiv);
		// $("#dropDiv").append(colDiv);
		$(selectedWidget).find(".portlet-content").append(colDiv);
		dropevent();
	}

	$(function() {
		$("#addProtocolForm").validate({
			rules : {
				"protocolName" : "required",
				"protocolCode" : "required",
			},
			messages : {
				"protocolName" : " Please Enter Protocol name",
				"protocolCode" : "Please Enter Protocol Code",

			}
		});
	});

	function executePreviewFormulas() {
		//alert("executePreviewFormulas");
		var formula = "";
		$("#preview").find(":hidden").each(
				function() {

					var hiddenObj = this;
					var hiddenId = $(hiddenObj).attr("id");
					// alert("id = " + hiddenId + " val = > " + $(hiddenObj).val());
					formula = $(this).val();
					$("#preview").find(":input:not(:hidden)").each(
							function() {
								//  alert($(this).attr("id") + " val = > " + $(this).val() );
								if ($.trim($(this).val()) != "") {
									formula = formula.replace($(this)
											.attr("id"), eval($(this).val()));
								} else {
									// replace 0 to null values
									formula = formula.replace($(this)
											.attr("id"), 0);
								}

							});
					// alert("formula before exec " + formula);
					var formulaValue = eval(formula);
					//alert("eval " + formulaValue);
					//alert("isfinite" +  );
					if (isFinite(formulaValue)) {
						//alert("in side if");
						formulaValue = round2Decimal(formulaValue);
					} else {
						//alert("in side else");
						formulaValue = "";
					}
					var objId = hiddenId.substring(0, hiddenId.indexOf("_"));
					//    alert("obj id " + objId);
					$("#" + objId).val(formulaValue);
				});

	}

	function showFormula(obj) {
		//alert("obj " + obj);
		tmpObj = obj;
		//alert("tmpobj id " + $(tmpObj).attr("id"));
		var varHtml = "<table><tr><td colspan='2'> Click a button to place in Field Calculation</td></tr><tr><td colspan='2'> <input type='text' id='formulaname' placeholder='Type to Select a Target Field' style='width:100%'/> </td></tr><tr><td colspan='2'><select id='inputs' ><option value='' > Select </option>";
		var inputcount = $("#dropDiv [type=text]").length;
		//alert(inputcount)
		$("#dropDiv [type=text]").each(
				function() {
					if (!$(this).hasClass("pclabel") && $(this).val() != "") {
						varHtml += "<option value='" + $(this).val() + "' > "
								+ $(this).val() + " </option> ";
					}
				});
		varHtml += "</select>";

		varHtml += " &nbsp;<input type='button' value='Add' onclick='javascript:createFormula();' /> </td> ";

		varHtml += "<tr><td colspan='2'>";
		varHtml += "<table><tr><td><div class='button_calc' onclick='javascript:createFormula(this);'>7</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>8</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>9</div></td><td>&nbsp;</td> <td><div class='button_calc' onclick='javascript:createFormula(this);'>+</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>-</div></td></tr>";
		varHtml += "<tr><td><div class='button_calc' onclick='javascript:createFormula(this);'>4</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>5</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>6</div></td><td>&nbsp;</td> <td><div class='button_calc' onclick='javascript:createFormula(this);'>*</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>/</div></td></tr>";
		varHtml += "<tr><td><div class='button_calc' onclick='javascript:createFormula(this);'>1</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>2</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>3</div></td><td>&nbsp;</td> <td><div class='button_calc' onclick='javascript:createFormula(this);'>+</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>-</div></td></tr>";
		varHtml += "<tr><td colspan='2'><div class='button_calc0' onclick='javascript:createFormula(this);'>0</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>.</div></td><td>&nbsp;</td><td><div class='button_calc' onclick='javascript:createFormula(this);'>(</div></td><td><div class='button_calc' onclick='javascript:createFormula(this);'>)</div></td></tr>";

		varHtml += "</table></td></tr>";

		varHtml += "<tr><td colspan='5'>&nbsp;</td></tr>";
		var formulaTxt = '';
		varHtml += "<tr><td colspan='5'><textarea id='formulText' type='' value='"
				+ formulaTxt
				+ "' style='width:100%'/><input value=''  type='hidden' id='hformulText' readonly='true' /> </td></tr>";
		varHtml += "<td><input type='button' value='Finish' onclick='javascript:setFormula();'/></td></tr></table>";
		$("#preview").html(varHtml);

		setObjectAutoComplete($("#formulaname"), "calcFields", true);

		showPopUp("open", "preview", "Define Field Calculation", "550", "450")

	}

	function saveProtocolQC() {

		var oldQC = $("#oldQCs").val();
		var addedQC = $("#addedQCs").val();
		var deletedQC = $("#deletedQCs").val();
		var val;

		$("#porotocolQCTable #protocolQC ").each(function(row) {
			val = $(this).val();
			if ($(this).is(":checked")) {
				if (oldQC.indexOf("," + val + ",") == -1) {
					addedQC += val + ",";
				}
			} else {
				if (oldQC.indexOf("," + val + ",") != -1) {
					deletedQC += $(this).val() + ",";
				}
			}
		});

		if (addedQC != ",") {
			addedQC = addedQC.substring(1, (addedQC.length - 1));

		} else {
			addedQC = "";
		}
		if (deletedQC != ",") {
			deletedQC = deletedQC.substring(1, (deletedQC.length - 1));

		} else {
			deletedQC = "";
		}
		// alert(addedQC + " / " + deletedQC);
		//    ---------- donor QC 
		var donorOldQCs = $("#donorOldQCs").val();
		var donorAddedQC = ",";
		var donorDeletedQc = ",";
		$("#donorQCTable #protocolQC ").each(function(row) {
			val = $(this).val();
			if ($(this).is(":checked")) {
				if (donorOldQCs.indexOf("," + val + ",") == -1) {
					donorAddedQC += val + ",";
				}
			} else {
				if (donorOldQCs.indexOf("," + val + ",") != -1) {
					donorDeletedQc += $(this).val() + ",";
				}
			}
		});

		if (donorAddedQC != ",") {
			donorAddedQC = donorAddedQC.substring(1, (donorAddedQC.length - 1));

		} else {
			donorAddedQC = "";
		}
		if (donorDeletedQc != ",") {
			donorDeletedQc = donorDeletedQc.substring(1,
					(donorDeletedQc.length - 1));

		} else {
			donorDeletedQc = "";
		}
		//-------- end of of donor QC

		// alert("template " + template);
		var protocol = $("#protocolDetail #protocol").val();
		var protocolName = $("#protocolDetail #protocolName").val();

		jsonData = "{protocol:" + protocol + ",protocolName:'" + protocolName
				+ "',qcData:{'addedQC':'" + addedQC + "','deletedQC':'"
				+ deletedQC + "',testType:'Product'},donorQc:{'addedQC':'"
				+ donorAddedQC + "','deletedQC':'" + donorDeletedQc
				+ "',testType:'Donor'}}";
		//=   alert("jsonData " + jsonData);

		// save panel data
		$('.progress-indicator').css('display', 'block');
		url = "saveProtocolQC";
		//alert("json data " + jsonData);
		ajaxresult = jsonDataCall(url, "jsonData=" + jsonData);
		oldQCids = ",";

		if (ajaxresult.exisQclst != null) {
			$.each(ajaxresult.exisQclst, function() {
				if (oldQCids.indexOf(("," + this.fkCodelstQc + ",")) == -1) {
					oldQCids += this.fkCodelstQc + ",";
				}
			});
			$("#oldQCs").val(oldQCids);
		}
		var donorOldQCids = ",";
		if (ajaxresult.existDonorQCList != null) {
			$
					.each(ajaxresult.existDonorQCList,
							function() {
								if (donorOldQCids.indexOf((","
										+ this.fkCodelstQc + ",")) == -1) {
									donorOldQCids += this.fkCodelstQc + ",";
								}
							});
			$("#donorOldQCs").val(donorOldQCids);
		}

		getProtocolQC();

		$('.progress-indicator').css('display', 'none');
	}

	function saveCalculation() {
		// alert("save calculation4");
		var fieldType = "";
		var fieldValue = "";
		var fieldID = 0;
		var jsonData = "";
		var rowData = "";
		var protocol = $("#protocolDetail #protocol").val();
		var protocolName = $("#protocolDetail #protocolName").val();
		var pcid = 0;
		var fieldListId = 0;
		try {
			//construct panel data
			var widgetName;
			$("#dropDiv #widget")
					.each(
							function(i, widget) {

								widgetName = $(widget).find("#portlettitle")
										.val();
								// alert(" widget count " + i + " / " + widgetName);
								$(widget)
										.find("div .dropItem")
										.each(
												function(col) {
													//alert("col " + col)
													$(this)
															.find("ul")
															.each(
																	function(
																			row) {

																		fieldType = "";
																		$(this)
																				.find(
																						"input[type=text]")
																				.each(
																						function() {
																							if ($(
																									this)
																									.hasClass(
																											"pclabel")) {
																								fieldType = "LABEL";
																							} else if ($(
																									this)
																									.hasClass(
																											"pctext")) {
																								fieldType = "TEXT";
																							} else if ($(
																									this)
																									.hasClass(
																											"pccalc")) {
																								fieldType = "CALC";
																							} else if ($(
																									this)
																									.hasClass(
																											"pcdrop")) {
																								fieldType = "DROPDOWN";
																							} else {
																								return true;
																							}
																							fieldValue = ""
																									+ $(
																											this)
																											.val();
																							if (fieldType == "LABEL") {
																								fieldValue = $
																										.trim(fieldValue);
																								fieldId = $(
																										"#calcLabel option:contains('"
																												+ fieldValue
																												+ "')")
																										.val();
																								// alert(fieldValue + " = " + $("#calcLabel option:contains('"+fieldValue+"')").html() + " - " + fieldId);
																							} else {
																								if (fieldValue
																										.indexOf("->") > 0) {
																									fieldValue = fieldValue
																											.substring(fieldValue
																													.indexOf("->") + 2);
																								}
																								fieldValue = $
																										.trim(fieldValue);
																								// alert(fieldType + "/" + fieldValue );
																								if (fieldType == "CALC") {
																									fieldId = $(
																											"#calcFieldId option:contains('"
																													+ fieldValue
																															.substring(
																																	0,
																																	fieldValue
																																			.indexOf("="))
																													+ "')")
																											.val();
																									//fieldId =0;
																								} else if (fieldType == "DROPDOWN") {
																									fieldId = $(
																											"#calcFieldId option:contains('"
																													+ fieldValue
																													+ "')")
																											.val();
																									fieldListId = $(
																											"#calcdrop option:contains('"
																													+ $
																															.trim($(
																																	this)
																																	.val())
																													+ "')")
																											.val();
																									//alert(" trace 3 fieldListId " + fieldListId);
																								} else {
																									fieldId = $(
																											"#calcFieldId option:contains('"
																													+ fieldValue
																													+ "')")
																											.val();
																								}
																							}
																							if (fieldId == undefined) {
																								fieldId = "";
																							}

																							pcid = $(
																									"#pccalid_"
																											+ col
																											+ "_"
																											+ row)
																									.val();
																							if (pcid == undefined) {
																								pcid = "";
																							}
																							rowData += "{widgetNumber:"
																									+ i
																									+ ",widgetName:'"
																									+ widgetName
																									+ "', protocol:"
																									+ protocol
																									+ ","
																									+ "columnnumber:"
																									+ col
																									+ ",rownumber:"
																									+ row
																									+ ",fieldType:'"
																									+ fieldType
																									+ "',fieldValue:'"
																									+ fieldValue
																									+ "',fieldId:'"
																									+ fieldId
																									+ "',fieldListId:'"
																									+ fieldListId
																									+ "'},";

																						});

																	});

												});
							});
			//   alert("1" + rowData)
			if (rowData.length > 0) {
				rowData = rowData.substring(0, (rowData.length - 1));
			}

			//  alert(addedQC + " / " + deletedQC);

			// alert("template " + template);

			jsonData = "{protocol:" + protocol + ",protocolName:'"
					+ protocolName + "', panelCalcData:["
					+ encodeURIComponent(rowData) + "]}";
			//    alert("jsonData " + jsonData);

			// save panel data
			$('.progress-indicator').css('display', 'block');
			url = "saveProtocolCalculation";
			//alert("json data " + jsonData);
			response = jsonDataCall(url, "jsonData=" + jsonData);
			$('.progress-indicator').css('display', 'none');

		} catch (e) {
			alert("error " + e);
		}

		// alert("save calc end");
	}

	function preview() {
		//alert("preview");
		var varHtml = "";
		try {
			$("#dropDiv #widget")
					.each(
							function(i, widget) {

								varHtml += "<div  class='portlet' > <div class='portlet-header'>"
										+ $(widget).find("#portlettitle").val()
										+ "</div>";
								varHtml += "<div class='portlet-content '>";

								//alert(" widget " + i);
								//alert("html : " + $(widget).html());
								var columns = $(widget).find(".dropItem ").length;

								var value = "";
								var elements;
								var elementValue = "";
								//    alert("no of colums " + columns);

								var rows = 0;
								$(widget).find(".dropItem ").each(
										function(i, element) {
											var tmpRows = $(element).find(
													"input").length;
											if (rows < tmpRows) {
												rows = tmpRows;
											}
										})

								// alert(rows + " / col " + columns)
								elements = new Array(rows);
								for (i = 0; i < rows; i++) {
									elements[i] = new Array(columns);
								}
								var i2 = 0;
								var j = -1;
								//BB var dropdownid = "";
								$(widget)
										.find(".dropItem")
										.each(
												function(i1, element) {
													j = -1;
													// alert(i + "->" + $(element).html());
													$(element)
															.find("ul")
															.each(
																	function(j1) {
																		j += 1;
																		//  alert( " final " + i2+ " j " + j + " / " +elementValue );
																		elementValue = $(
																				this)
																				.html();
																		// alert("before " + elementValue);
																		$(
																				elementValue)
																				.find(
																						".destroy")
																				.each(
																						function(
																								a1) {
																							// alert(" a22222222 " + this);

																							elementValue = elementValue
																									.replace(
																											$(
																													this)
																													.parent()
																													.html(),
																											"a");
																						})

																		//  elementValue= elementValue.replace('<a onclick="javascript:destroy(this);" href="#"><img class="destroy" width="16" height="16" src="images/icons/closebox.png"></a>',' ');
																		// alert("after " + elementValue);
																		elements[j][i2] = elementValue;

																	});
													//  alert("i2 " + i2 + " /j " + j+ " /ele " + elements);
													i2 += 1;

													/*
													 $(element).find("input[type=text]").each(function(j1){
													 // alert(i+ " j " + j + " /---1-----" + i1 + " / " + j1);

													 value =  $.trim($(this).val());
													 if(value ==""){
													 elementValue =" ";
													 }else{
													 if(value.indexOf("->")>0){
													 value = value.substring(value.indexOf("->") +2);
													 }
													 value=$.trim(value);


													 if($(this).hasClass("pclabel")){
													 elementValue= "<label>"+value+"</label>";
													 }else if($(this).hasClass("pctext")){
													 if(value.indexOf("=")>0){
													 value = value.substring(value.indexOf("=") +1);
													 }
													 value=$.trim(value);
													 //    alert(" value " + value + " / text " );
													 elementValue= "<input type='text' name='" +value +"' id='" + value + "' onkeyUp='executePreviewFormulas();' />";

													 }else if($(this).hasClass("pccalc")){
													 elementValue ="";
													 if(value.indexOf("=")> 0){
													 var formula = value.substring(value.indexOf("=")+1);
													 value = value.substring(0,(value.indexOf("=")));
													 //    alert(value + " = " + formula);
													 elementValue = "<input type='hidden' id='"+value+"_formula' name='"+value +"_formula' value='" +formula + "' />";
													 }

													 elementValue+= "<input type='text' name='" +value +"' id='" + value + "' />";
													 }else if($(this).hasClass("pcdrop")){
													
													 var fieldId = $.trim($(this).val());
													 if(fieldId.indexOf("->")>0){
													 fieldId = fieldId.substring(fieldId.indexOf("->") +2);
													 }
													 fieldId = $.trim(fieldId);
													 var dropdownId = $("#calcdrop option:contains('"+value+"')").val();
													 jsonData="parentId="+dropdownId;
													 response = jsonDataCall("getChildValues",jsonData);
													 var selectHtml = "<select id='"+fieldId+"' onchange='executePreviewFormulas();' ><option value=''>Select</option>"
													 for (var i = 0; i < response.childDetails.length; i++) { 
													 selectHtml +="<option value='"+ response.childDetails[i].pkCodelst+"'>"+response.childDetails[i].description+"</option>";
													
													 }
													 selectHtml += "</select>";
													 elementValue = selectHtml;
													
													 }
													 }
													
													 j+=1;
													 //  alert( " final " + i2+ " j " + j + " / " +elementValue );
													 elements[j][i2] = elementValue;
													 //    alert("row " + j + " col " + i + " val " + elementValue );
													 });
													
													 i2+=1;
													 */
												});
								alert("elements " + elements);
								varHtml += "<table>";
								var tdvalue = "";
								// col =2 , r1=3, r2 = 4
								// alert("element length " + elements.length  );
								for (row = 0; row < elements.length; row++) {
									//	alert("row " + row);
									//alert(" elements " + elements);
									var colValues = elements[row];
									varHtml += "<tr>";
									//  alert("colValues length " + colValues.length  );
									for (col = 0; col < colValues.length; col++) {
										//  	alert(row + ","+col);
										tdvalue = elements[row][col];
										alert("tdvalue " + tdvalue);
										if (tdvalue == undefined) {

											tdvalue = "&nbsp;";
										}
										varHtml += "<td>" + tdvalue + "</td>";
									}
									varHtml += "</tr>";
								}
								varHtml += "</table>";
								varHtml += "</div></div>";
							});
			alert(" varHtml " + varHtml);
			$("#preview").html(varHtml);

			$("#preview")
					.find(".portlet")
					.addClass(
							"ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
					.find(".portlet-header").addClass(
							"ui-widget-header ui-corner-all").prepend(
							"<span class='ui-icon ui-icon-minusthick'></span>");
			$("#preview").find(".portlet-header .ui-icon").click(
					function() {
						$(this).toggleClass("ui-icon-minusthick").toggleClass(
								"ui-icon-plusthick");
						$(this).parents(".portlet:first").find(
								".portlet-content").toggle();
					});
			// alert("show popup");
			//function showPopUp(mode,Id,title,wd,ht){
			showPopUp("open", "preview", "Preview", "850", "350")
			//alert("end ");
		} catch (err) {
			alert("error " + err);
		}
	}
</script>

<s:hidden name="protocolHidden" name="protocolHidden" value=""/>
			<div id="slidecolumnfilter" style="display:none"> 
			<br>
			<br>
				<div  class="portlet">
				<div class="portlet-header">Protocol <input type="text" id="Columnfilter" class="inputboxSearch maintenance_slidewidget"  onclick="maintColumnfilter('portocolFacet')"></div>
				<div class="cleaner"></div>

					<div class="portlet-content">
					<div style="height:320px;overflow-y:auto;">
						<table width="100%" border="0" id="portocolFacet">
					   		<s:iterator value="protocolList" var="rowVal" status="row">
					  		<tr >
								<td ><a href="#" onclick="javascript:getProtocolDetails( '<s:property value="protocol"/>','<s:property value="protocolName"/>',this)" ><s:property value="protocolName"/></a></td>
								<td><img src = "images/icons/folder.png" class="cursor" onclick="copyProtocol('<s:property value="protocol"/>','<s:property value="protocolName"/>','<s:property value="protocolCode"/>','<s:property value="protocolDesc"/>')"/></td>
					 		</tr>
							</s:iterator>
						</table>
					</div>	
					</div>
				</div>
				</div> 

<!--float window end-->

<!--right window start -->	
<span class='hidden'>
	<s:select name="codeListModule" id="codeListModule" list="moduleList" headerKey="" headerValue="Select" listKey="subType" listValue="description"/>

<select name="reagents" id="reagents">
   <option value="">Select</option>
	<s:iterator value="reagenstsList" status="row" var="cellValue">  
		 <option value="<s:property value="#cellValue[0]"/>"> <s:property value="#cellValue[1]"/></option>
	</s:iterator>
</select>	

<select name="reagentsdesc" id="reagentsdesc">
   <option value="">Select</option>
	<s:iterator value="reagenstsList" status="row" var="cellValue">  
		 <option value="<s:property value="#cellValue[0]"/>"> <s:property value="#cellValue[2]"/></option>
	</s:iterator>
</select>
 
	<select name="equipments" id="equipments" >
		<option value="">Select</option>
		<s:iterator value="equipmentList" status="row" var="cellValue">  
			 <option value="<s:property value="#cellValue[0]"/>"> <s:property value="#cellValue[1]"/></option>
		</s:iterator>
	</select>
	
	<select name="equipmentsdesc" id="equipmentsdesc" >
		<option value="">Select</option>
		<s:iterator value="equipmentList" status="row" var="cellValue">  
			 <option value="<s:property value="#cellValue[0]"/>"> <s:property value="#cellValue[2]"/></option>
		</s:iterator>
	</select>
</span>	
<!-- <div id="forfloat_right"> -->
 <div > <span id="breadcrumb">Protocol Library </span> </div>
 <br>
	<div class="cleaner"></div>

	<!-- Protocol Portlet Start -->
	
	<div  class="portlet" id="protocollist">
		<div  class="portlet-header ">Protocol Library</div>
		
		<div class="portlet-content " >
			<table>
				<tr>
					<td style="width:20%"><div onclick="addRows(0,'','','',0,false);"><img src = "images/icons/addnew.jpg" class="cursor"  />&nbsp;&nbsp;<label id="adchild" class="cursor" ><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteRows();"/>&nbsp;&nbsp;<label class="cursor"  onclick="deleteRows();"><b>Delete</b></label>&nbsp;</div></td>
			   	 	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;" class="cursor" id ="" onclick="editRows();"/>&nbsp;&nbsp;<label id="dchild" class="cursor" onclick="editRows();"><b>Edit</b></label>&nbsp;</div></td>
					<!-- <td style="width:20%"><div><img src = "images/icons/save3.png" style="width:16;height:16;" class="cursor" id ="" onclick="saveProtocols()" >&nbsp;&nbsp;<label id="schild" class="cursor" onclick="saveProtocols()"><b>Save</b></label>&nbsp;</div></td> -->
				</tr>
			</table>
		
<!-- BB data table -->

  			<table id="porotocolTable" cellpadding="0" cellspacing="0" border="0" class="display">
			<thead>
				   <tr>
				        <th width="4%" id="protocolColumn"></th>
						<th><s:text name="stafa.maintenance.label.name"/></th>
						<th><s:text name="stafa.label.code"/></th>
						 <th><s:text name="stafa.label.module"/></th>
						<th><s:text name="stafa.maintenance.label.comments"/></th>
						<th><s:text name="stafa.label.activated"/></th>
					</tr>
				</thead>		
  			</table>
  			
<!--  end of BB data table -->
		</div>
	</div>
 <!--  end of protocol portlet -->	


<!--  Protocol  details -->
<div id="protocolDetails" class="hidden">

<div  class="portlet ">
		<div class="portlet-header">Protocol Details</div>
		
			<div class="portlet-content">
				<div id="table_inner_wrapper">
				  <form id="protocolDetail" action="#" onsubmit="return avoidFormSubmitting()">
				     <input type="hidden"  name="protocol" id="protocol"/>	
				     <input type="hidden" name="protocolModule" id="protocolModule" />			
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
							<tr>
								<td width="25%" height="40px" class="txt_align_label">Protocol Name</td>
								<td width="25%" ><input type="text" id="protocolName" name="protocolName" /></td>
								<td width="25%" class="txt_align_label" align="left">Deactivate?</td>
								<td width="25%" align="left"><input type="radio" id="activateYes" name="deactivate"/> Yes &nbsp;<input type="radio" id="activateNo" name="deactivate"/> No</td>
						
							</tr>
							<tr>
								<td class="txt_align_label" align="left">Code</td>
								<td align="left"><input type="text" name="protocolCode" id ="protocolCode" /></td>
								
								<td class="txt_align_label1" align="left" >		
								<span class="hidden"><s:select id="copyProtocol"  name="copyProtocol" list="protocolList" listKey="protocol" listValue="protocolName" headerKey="" headerValue="Select"/>
										</span> 
																					
								<input type="text" name="newName" id="newName" placeholder='New Name'/>&nbsp;</td>
								<!--
								<input type="text" name="copyTONew" id="copyTONew" placeholder='Copy To' />&nbsp;</td>
								-->
								<td align="left">
								<input type="text" name="newCode" id="newCode" placeholder='New Code'/>&nbsp;</td>
								
							</tr>
							<tr>
								<td class="txt_align_label" align="left">Description</td>
								<td align="left" colspan="2"><textarea rows="3" cols="25" name="desc" id="desc"></textarea></td>
								 <td><input type="button" id="" name="" value="Copy to New Protocol" onclick="javascript:copyToNewProtocol1();" /></td>
							
							</tr>
							<tr >
								<td colspan="4">&nbsp;</td>
							</tr>
							
							<tr>
								<td colspan ="4"><a id="qcProtocol" class="cursor" onclick="getProtocolQC()">Select Default Labs for the Protocol</a></td>
							</tr>
							
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
							<tr>
								<td colspan ="4"><a id="qcProtocol" class="cursor" onclick="getInventory()">Inventory Management</a></td>
							</tr>
							<tr><td colspan="4">&nbsp;</td></tr>
							<tr>
								<td colspan="4"><a id="clcProtocol" class="cursor" onclick="loadProtocolPages()">Page Configuration</a></td>
							</tr>
							
							<tr><td colspan="4">&nbsp;</td></tr>
							<tr>
							<td colspan="3">&nbsp;</td>
									<!--  update protocol status Delete, Activated yes/no -->
								<!-- <td><input type="text" placeholder="e-Sign" value="" size="5" style="width:80px;"> &nbsp;<input type="button" value="Save" onclick="updateProtocol()"></td> -->
							</tr>
						</table>					
						</form>
					</div>
			</div>
		</div>

</div>

<!--  end of protocol details -->

<!-- Protocol QC Portlet Start -->
	<div  class="portlet hidden" id="divDonorLabQC" >
		<div  class="portlet-header" id="donorLabTest">Donor Lab Test</div>
		
		<div class="portlet-content " >
<!-- BB data table -->
  			<table id="donorQCTable" cellpadding="0" cellspacing="0" border="0" class="display">
  			<thead>
				   <tr>
						<th width="4%" id="donorQCColumn"></th>
						<th width="10%"><s:text name="stafa.label.checkall"/><input type="checkbox"  id="deleteallProtocol" onclick="checkall('porotocolQCTable',this)"></th>
						<th><s:text name="stafa.maintenance.label.name"/></th>
						<th><s:text name="stafa.label.code"/></th>
						<th><s:text name="stafa.maintenance.label.comments"/></th>
						<%-- <th><s:text name="stafa.label.activated"/></th> --%>
					</tr>
				</thead>	
			</table>
	</div>
	</div>
	
	<div  class="portlet hidden" id="divProtocolQC" >
		<div  class="portlet-header" id="productLabTest">Product Lab Test</div>
		
		<div class="portlet-content " >
<!-- BB data table -->
  			<table id="porotocolQCTable" cellpadding="0" cellspacing="0" border="0" class="display">
  			<thead>
				   <tr>
						<th width="4%" id="productQCColumn"></th>
						<th width="10%"><s:text name="stafa.label.checkall"/><input type="checkbox"  id="deleteallProtocol" onclick="checkall('porotocolQCTable',this)"></th>
						<th><s:text name="stafa.maintenance.label.name"/></th>
						<th><s:text name="stafa.label.code"/></th>
						<th><s:text name="stafa.maintenance.label.comments"/></th>
						<%-- <th><s:text name="stafa.label.activated"/></th> --%>
					</tr>
				</thead>	
			</table>
  	<!--  end of BB data table -->
			<div class="cleaner"></div>
				<form id="protocolQCForm" action="#" onsubmit="return avoidFormSubmitting()">
					<input type="hidden" id="oldQCs" name="oldQCs" value="," />
					<input type="hidden" id="addedQCs" name="addedQCs" value=","/>
					<input type="hidden" id="deletedQCs" name="deletedQCs" value="," /> 
					<input type="hidden" id="donorOldQCs" name="donorOldQCs" value="," />
					
					
			 		<div align="right" style="float:right;">
						<table class="" width="100%" cellspacing="0" cellpadding="0" border="0" align="">
						 <!-- <tr>
							<td><input type="text" placeholder="e-Sign" value="" size="5" style="width:80px;"></td>
							
							<td><input type="button" value="Save" onclick="saveProtocolQC()"></td>
						 </tr> -->
						</table>
					</div>
			   </form>
		</div>
	</div>
 <!--  end of protocol QC portlet -->	
<!-- Inventory Management starts -->

<div class="hidden" id="inventory">		
		<div  class="portlet">
			<div class="portlet-header"><span id="spanReagents"> </span></div>			
		<form onsubmit="return avoidFormSubmitting()">
	
			<div class="portlet-content" >
			<table>
			<tr>
				<td style="width:20%"><div><a onclick="add_ProtocolRegent(); " ><img src = "images/icons/addnew.jpg" class="cursor" />&nbsp;&nbsp;<label class="cursor"><b>Add</b></label></a>&nbsp;</div></td>
				<td style="width:20%"><div><a onclick="delete_Inventory('regentSupplies');"><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor"/>&nbsp;&nbsp;<label class="cursor"><b>Delete</b></label></a>&nbsp;</div></td>
		    	<td style="width:20%"><div><a onclick="edit_ProtocolRegent();"><img src = "images/icons/edit.png" style="width:16;height:16;" class="cursor"/>&nbsp;&nbsp;<label class="cursor"><b>Edit</b></a></label></a>&nbsp;</div></td>
				
			</tr>
			</table>
			<div style="overflow-x:auto;overflow-y:hidden">
			<table  id="regentSupplies" cellspacing="0" cellpadding="0" border="0"  class="display" >
				<thead>
					<tr>
						<th width="4%" id="protocolReagentColumn"></th>
						<th style="width:8%" colspan="1">Check All<input type="checkbox"  id="checkallReagents" onclick="checkall('regentSupplies',this)"></th>
						<th >Name</th>
						<th >Description</th>
						<th width="10%" >Quantity</th>
					</tr>
				</thead>
			<tbody>
			
			</tbody>	
			</table>
		</div>
		</div>
	</form>
	</div>                    	 
	
	
	<div  class="portlet">
			<div class="portlet-header"><span id="spanEquipment"> </span></div>			
		<form onsubmit="return avoidFormSubmitting()">
	
			<div class="portlet-content" >
			<table>
			<tr>
				<td style="width:20%"><div><a onclick="add_Equipment(); " ><img src = "images/icons/addnew.jpg" class="cursor" />&nbsp;&nbsp;<label class="cursor"><b>Add</b></label></a>&nbsp;</div></td>
				<td style="width:20%"><div><a onclick="delete_Inventory('equipment');"><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor"/>&nbsp;&nbsp;<label class="cursor"><b>Delete</b></label></a>&nbsp;</div></td>
		    	<td style="width:20%"><div><a onclick="edit_Equipment();"><img src = "images/icons/edit.png" style="width:16;height:16;" class="cursor"/>&nbsp;&nbsp;<label class="cursor"><b>Edit</b></a></label></a>&nbsp;</div></td>
				
			</tr>
			</table>
			<div style="overflow-x:auto;overflow-y:hidden">
			<table  id="equipment" cellspacing="0" cellpadding="0" border="0"  class="display" >
				<thead>
					<tr>
						<th width="4%" id="protocolEquipmentColumn"></th>
						<th style="width:8%" colspan="1">Check All<input type="checkbox"  id="checkallEquipement" onclick="checkall('equipment',this)"></th>
						<th >Name</th>
						<th >Description</th>
					</tr>
				</thead>
			<tbody>
			
			</tbody>	
			</table>
		 </div>
		</div>
	</form>
	</div>         
</div>


<!-- End of Inverntory Management -->
<!--  page config -->
<div class="portlet hidden" id="protocolCalc" >
	<div class="portlet-header">Processing - </div>
	<div class="portlet-content" >
		<div style="min-height: 75px;" >
		  	<div id="dragDiv" class="dragItem dropItem1">	</div>
		  	<div id="dropDiv" class="dropItem " ></div>
		</div>
	</div>
</div>
		

<!--  end to page config -->
<!-- //BB:not in use calculation -->



<!--  right div -->
<!-- </div> -->



