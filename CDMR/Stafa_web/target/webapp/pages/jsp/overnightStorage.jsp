<%@ include file="common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script type="text/javascript" src="js/placeholder.js"></script>
<script>

function loadQcInfo(){
	$("#qcResultInfo").html("");
	var chk_arr = $("#qcContent input[type=checkbox]");
	var chklength = chk_arr.length;
	var SelectedQcID ="";
	for(i=0;i<chklength;i++){
	if(chk_arr[i].checked == true){
		if(SelectedQcID==""){
			SelectedQcID+= $(chk_arr[i]).attr("id");
		}
		else{
			SelectedQcID+=",";
			SelectedQcID+= $(chk_arr[i]).attr("id");
			}
		}
	}
	var temptable="";
	var url="loadQcInformation";
	var result = ajaxCallWithParam(url,SelectedQcID);
	var id;
	$.each(result.qcInfoList, function(i,v) {
		var tempID=v[3];
		if(tempID==id){
			temptable+="<tr><td width='60%'>"+v[2]+"</td><td width='40%'><input type='text' id="+v[5]+" name="+v[5]+"></td></tr>";
			}
		else
			{
			if(temptable!==""){
				temptable+="</table></fieldset>";
				$("#qcResultInfo").prepend(temptable);
				temptable="";
				temptable+="<fieldset style='width:90%;border-radius:5px' id="+v[4]+ "><legend style='color:blue'>"+v[4]+"</legend><table>";
				temptable+="<tr><td width='60%'>"+v[2]+"</td><td width='40%'><input type='text' id="+v[5]+" name="+v[5]+"></td></tr>";	
				id=tempID;
				}
			else{
				temptable="<fieldset style='width:90%;border-radius:5px' id="+v[4]+"><legend style='color:blue'>"+v[4]+"</legend><table>";
				temptable+="<tr><td width='60%'>"+v[2]+"</td><td width='40%'><input type='text' id="+v[5]+" name="+v[5]+"></td></tr>";	
				id=tempID;
				}
			}
		});
	$("#qcResultInfo").prepend(temptable);
	}
function saveOvernightStorage(){
	if($("#nextProcess").val()=="Production"){
		javascript:loadAction('getOriginalProduct.action?newProductId='+productId+"&fkProtocol="+fkProtocol);
		}		
}

$(function() {
	$( "#leftnav" ).leftnav();
	$( "#leftnav" ).removeClass('ui-widget ui-helper-reset');
});
     


$(document).ready(function() {
	//alert("ready");
	$("select").attr("id","small");  
//$('#table111').dataTable({"aaSorting": [[ 1, "desc" ]]});
	oTable = $('#equipmentsTable').dataTable({
		"bJQueryUI": true,
		"aoColumnDefs": [{"sTitle":"Equiment", "aTargets": [ 0]},
		                 {"sTitle":"Manufacturer","aTargets": [ 1]},
		                 {"sTitle":"Serial#", "aTargets": [ 2]},
		                 {"sTitle":"Expiration Date", "aTargets": [ 3]},
		                 {"sTitle":"Appearance OK", "aTargets": [ 4]}
		]
		//"sPaginationType": "full_numbers"
	});
	oTable.thDatasorting('equipmentsTable');
	 oTable1 = $('#reagentsTable').dataTable({
		"bJQueryUI": true,
		"aoColumnDefs": [{"sTitle":"Reagents And Supplies", "aTargets": [ 0]},
		                 {"sTitle":"Quantity","aTargets": [ 1]},
		                 {"sTitle":"Manufacturer", "aTargets": [ 2]},
		                 {"sTitle":"Lot #", "aTargets": [ 3]},
		                 {"sTitle":"Expiration Date", "aTargets": [ 4]},
		                 {"sTitle":"Appearance OK", "aTargets": [ 5]}
		]
		//"sPaginationType": "full_numbers"
	}); 
	oTable1.thDatasorting('reagentsTable');
	var url = "getQc";
	var response = ajaxCall(url,"overnightStorageForm");
	generateTableFromJsnList(response);
	//getOvernightStorageData(event);
	//alert("trace 1")
	//?BB
			
/* 	document.getElementById('equipmentsTable_next').setAttribute("onClick","removeTableClass()");
	document.getElementById('equipmentsTable_previous').setAttribute("onClick","removeTableClass()");
	document.getElementById('reagentsTable_next').setAttribute("onClick","removeTableClass()");
	document.getElementById('reagentsTable_previous').setAttribute("onClick","removeTableClass()");
	removeTableClass();
	removeTableClassOnChange("equipmentsTable_length");
	removeTableClassOnChange("reagentsTable_length"); */
	//BB
		//alert("trace 2")
 var mappedProtocol = $("#fkProtocol").val();
	alert("mappedProtocol "+mappedProtocol);
	//$.uniform.restore('select');
	getProtocolCalculation(mappedProtocol,"calculationDiv");
	// BB
	$("select").uniform(); 
	
	});
	
	
function getScanData(event,obj){
	
	if(event.keyCode ==13){
				getOvernightStorageData(event);
	}
}
	
	
function getOvernightStorageData(e){
		var url = "getOvernightStorageData";
	var productSearch = $("#conformProductSearch").val();
	
	if(e.keyCode==13 && productSearch!=""){
		//alert("s : "+e.keyCode);
		var response = ajaxCall(url,"overnightStorageForm");
		//alert(response);
		//alert(response.qcList);
		//response = jQuery.parseJSON(response.recipientDonorList);
		//alert(response);	
		//alert(response.returnJsonStr);
		generateTableFromJsnList1(response);
	}
	//response = jQuery.parseJSON(response.recipientDonorList);

}
function generateTableFromJsnList1(response){
	alert("response.recipientDonorList :: "+response.recipientDonorList);
	$.each(response.recipientDonorList, function(i,v) {
	
		$("#recipientName").html(v[0]);
		$("#recipientId").html(v[1]);
		$("#recipientDiagnosis").html(v[2]);
		$("#recipientAboRh").html(v[3]);
		$("#donorName").html(v[4]);
		$("#donorId").html(v[5]);
		$("#donorAboRh").html(v[6]);
		$("#productId").html(v[7]);
		$("#productType").html(v[9]);
		$("#fkProtocol").val(v[11]);
	 	});
	//var qcCheckList = [];
	var qcCheckList = Array(2); 
	$.each(response.protocolQcList, function(i,v) {
		qcCheckList.push(v[2])
	 	});
 	
	var chk_arr = $("#qcContent input[type=checkbox]");
	var chklength = chk_arr.length;             
	//alert("qcCheckList : "+qcCheckList);
	for(k=0;k< chklength;k++)
	{	

		var id = $(chk_arr[k]).attr("id");
		//alert("id : "+id);
		if (Search_Array(qcCheckList, id)){
			//alert("Found : "+id);
			chk_arr[k].checked = true;
			$(chk_arr[k]).trigger('click');
			$("#"+id).attr("checked","checked");
			}
			else{
				//alert("Not Found");
				chk_arr[k].checked = false;
			}
	} 
	var mappedProtocol = $("#fkProtocol").val();
	//alert("mappedProtocol "+mappedProtocol);
	getProtocolCalculation(mappedProtocol,"calculationDiv");
}

function Search_Array(ArrayObj, SearchFor){
	  var Found = false;
	  //alert(ArrayObj+"--"+SearchFor);
	  for (var i = 0; i < ArrayObj.length; i++){
	    if (ArrayObj[i] == SearchFor){
	      return true;
	      var Found = true;
	      break;
	    }
	    else if ((i == (ArrayObj.length - 1)) && (!Found)){
	      if (ArrayObj[i] != SearchFor){
	        return false;
	      }
	    }
	  }
	}
function generateTableFromJsnList(response){
	var arr = [];
	var pkCodeList = [];
	$.each(response.qcList, function(i,v) {
		pkCodeList.push(v[0]);
		arr.push(v[3]);		
	 });
	var tab="<table style='width:100%'><tr>"
		 $.each(arr, function(i,val){
		var k=i+1;
		if((i%4==0)&&(i!=0)){
					//alert(pkCodeList[i]);
					tab+="</tr><tr><td>";	
					tab+="<input type='checkbox'  name='ABO' id='"+pkCodeList[i]+"' onclick=\"javascript:qcCalculation('overnightStorageForm');\">&nbsp;<a href='#' onclick=\"javascript:showQcDetails('"+pkCodeList[i]+"','"+arr[i]+"')\">"+arr[i]+"</a></td>";
			
		}
			 else{
				 
					tab+="<td>";			
					tab+="<input type='checkbox'  name='ABO' id='"+pkCodeList[i]+"' onclick=\"javascript:qcCalculation('overnightStorageForm');\">&nbsp;<a href='#' onclick=\"javascript:showQcDetails('"+pkCodeList[i]+"','"+arr[i]+"')\">"+arr[i]+"</a></td>";
		
			 }

		 });
	$("#qcContent").html(tab);
}
function removeTableClass(){
	
	$('#equipmentsTable_next').removeClass("ui-state-default");
	$('#equipmentsTable_previous').removeClass("ui-state-default");
	$('#reagentsTable_next').removeClass("ui-state-default");
	$('#reagentsTable_previous').removeClass("ui-state-default");

}

</script>

<div class="cleaner"></div>
<div id="floatMenu">
<div id="forfloat_wind">
	<div id="box-outer">
		
		<a href="#" style="text-decoration:none" id="blind">&lt;&lt; </a>
			<div id="box">
				<div id="leftnav" >
				
<!-- recipient start-->
					<div class="column">
					<div  class="portlet">
					<div class="portlet-header notes"><s:hidden id="moduleName" value="overnightStorage"/><s:hidden id="widgetName" value="recipient"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.recipient"/></div>
						<div class="portlet-content">
						<table width="100%" border="0">
						  <tr>
							<td width="50%"><s:text name="stafa.label.id"/></td>
							<td width="50%"><div id="recipientId"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.name"/></td>
							<td><div id="recipientName"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientaborh"/></td>
							<td><div id="recipientAboRh"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientdiagnosis"/></td>
							<td><div id="recipientDiagnosis"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientrid"/></td>
							<td><div id="recipientRID"></div></td>
						  </tr>
						</table>

					</div>
					</div>
					</div>
				
<!-- recipient-->

<!-- Product start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header notes"><s:hidden id="moduleName" value="overnightStorage"/><s:hidden id="widgetName" value="product"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.product"/></div>
						<div class="portlet-content">
						<table width="100%" border="0">
							<tr>
							<td width="50%"><s:text name="stafa.label.id"/></td>
							<td width="50%"><div id="productId"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.type"/></td>
							<td><div id="productType"></div></td>
						  </tr>
						</table>	

					
					</div>
				</div>
				</div>
				
<!-- Product-->

<!-- Donor start-->
					<div class="column">
					<div class="portlet">
						<div class="portlet-header notes"><s:hidden id="moduleName" value="overnightStorage"/><s:hidden id="widgetName" value="donor"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.donor"/></div>
						<div class="portlet-content">
							<table width="100%" border="0">
								<tr>
									<td width="50%"><s:text name="stafa.label.id"/></td>
									<td width="50%"><div id="donorId"></div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.name"/></td>
									<td><div id="donorName"></div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.donoraborh"/></td>
									<td><div id="donorAboRh"></div></td>
								</tr>
							</table>


						</div>
					</div>
					</div>
					<!-- Donor-->
				</div>
				</div>
			</div>
		</div>
</div>
<!--float window end-->

<!--right window start -->	
<div id="forfloat_right" style="width:82%;">
<form action="#" id="overnightStorageForm">
  <div class="column">	
		<div  class="portlet" id="barcodeScan">
			<div class="portlet-header notes addnew" >Barcode Scanning</div>
			<div class="portlet-content" id="scan">
				<div id="button_wrapper">
					<div style="margin-left:40%;">
					<input class="scanboxSearch barcodeSearch" type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"/>
					<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" />
					</div>
				</div>
			</div>
	</div>	
</div>

<div class="cleaner"></div>


		<s:hidden name="selectedQcId" id="selectedQcId" value=""/>
		<!--<s:hidden name="productSearch" id="productSearch" value="%{newProductId}"/>-->
		<!--<s:hidden name="productSearch" id="productSearch" value="BMT1236"/>-->
		<s:hidden name="fkProtocol" id="fkProtocol" value="%{fkProtocol}"/>
		<!--<s:hidden name="fkProtocol" id="fkProtocol" value="0"/>-->
		<input type="hidden" name="selectedQC" id="selectedQC" value="" />
 		<input type="hidden" name="qctype" id="qctype" value="" /> 


<div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes info"><s:hidden id="moduleName" value="overnightStorage"/><s:hidden id="widgetName" value="QC"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.qc"/></div>			
				<div  id="qcContent" class="portlet-content">
					
				</div>			
				
		</div>
		</div>
			
	
<section>

 <div class="cleaner"></div>
 <form>
  <div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value="overnightStorage"/><s:hidden id="widgetName" value="reagentsandSupplies"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.regandandsupplies"/></div>
			<div class="portlet-content">
			<div id="reagentsdiv">
			<table cellpadding="0" cellspacing="0" border="0" id="reagentsTable" class="display">
					<thead>
						<tr>
							<th><s:text name="stafa.label.regandandsupplies"/></th>
							<th><s:text name="stafa.label.quantity"/></th>
							<th><s:text name="stafa.label.manufacturer"/></th>
							<th><s:text name="stafa.label.lot"/></th>				
							<th><s:text name="stafa.label.expirationdate"/></th>
							<th><s:text name="stafa.label.appearanceok"/></th>
						</tr>
						<tr>
							<th></th>
							<th></th>
							<th></th>
							<th></th>				
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<s:iterator value="reagentsList" var="qcObject">
						<tr>
							<td><s:property value="qcDesc"/></td>
							<td><s:property value="qcQuanitiy"/></td>
							<td><s:property value="qcManufacturer"/></td>
							<td><s:property value="qcMFCode"/></td>
							<td>&nbsp;</td>
							<td align="center"><input type="radio" id="yes" name="yes" class=""/> Y &nbsp; <input type="radio" id="No" onClick="confirmIfNo();" name="no" class=""/> N</td>
						</tr>
					</s:iterator>
					</tbody>
			</table>
			</div>
	</div>
	</div>	
</div>	
</form>		
</section>	


<!-- <div id="addqc" title="add qc" style="display:none">
<div class="portlet" style="width:98%">
	<div class="portlet-header">ABO/RH</div>
	<div class="portlet-content">
		<table cellpadding="0" cellspacing="0" border="0" id="ABROHTable" class="display">
		<tr>
		<th>Recipient:</th>
		<th> <input type="Text"> </th>
		<th>Donor:</th>
		<th> <input type="Text"> </th>
		</tr>
		</table>
		</div>
		</div>
<div class="portlet" style="width:98%">
	<div class="portlet-header">CBC AND Differential</div>
		<div class="portlet-content">
		<b>Complete Blood Count</b>
		<table cellpadding="0" cellspacing="0" border="0" id="CBC AND Differential" class="display">
		<tr> <th></th> <th COLSPAN="3" style="border-bottom:2px solid black" >Sysmex Read</th><th><th> </TR>
		<tr><td></td> <td>Reading1</td><td>Reading2</td><td>Average</<td> <td>Pathology Read</td> </TR>
		
		<tbody>
		<tr>
		<td>WBC(x10^6/ml):</td>
		<td> <input type="Text"> </td>
		<td> <input type="Text"> </td>
		<td>AUTO #</td>
		<td> <input type="Text"> </td>
		</tr>
		<tr>
		<td>HCT (%):</td>
		<td> <input type="Text"> </td>
		<td> <input type="Text"> </td>
		<td>AUTO #</td>
		<td> <input type="Text"> </td>
		</tr>
		<tr>
		<td>HGB (%):</td>
		<td> <input type="Text"> </td>
		<td> <input type="Text"> </td>
		<td>AUTO #</td>
		<td> <input type="Text"> </td>
		</tr>			
				<tr>
		<td>MCV (fL):</td>
		<td> <input type="Text"> </td>
		<td> <input type="Text"> </td>
		<td>AUTO #</td>
		<td> <input type="Text"> </td>
		</tr>
		<tr>
		<td>MCH (pg):</td>
		<td> <input type="Text"> </td>
		<td> <input type="Text"> </td>
		<td>AUTO #</td>
		<td> <input type="Text"> </td>
		</tr>
		<tr>
		<td>MCHC (g/dL):</td>
		<td> <input type="Text"> </td>
		<td> <input type="Text"> </td>
		<td>AUTO #</td>
		<td> <input type="Text"> </td>
		</tr>
		<tr>	
		<td>Platelets (x10^6/ml):</td>
		<td> <input type="Text"> </td>
		<td> <input type="Text"> </td>
		<td>AUTO #</td>
		<td> <input type="Text"> </td>
		</tr>
		</tbody>
		</table>
	
		<table cellpadding="0" cellspacing="0" border="0" id="CBC AND Differential" class="display">
				<tr> <th></th> <th COLSPAN="3" style="border-bottom:2px solid black" >Sysmex Read</th><th><th> </TR>
		<tr><td></td> <td>Reading1</td><td>Reading2</td><td>Average</<td> <td>Pathology Read</td> </TR>
		<b>Differential</b>
		<br><br>
		<tr>
		<td>Neutrophils (%)</td>
		<td> <input type="Text"> </td>
		<td> <input type="Text"> </td>
		<td>AUTO #</td>
		<td> <input type="Text"> </td>
		</tr>
		<tr>
		<td>Lymphocytes (%):</td>
		<td> <input type="Text"> </td>
		<td> <input type="Text"> </td>
		<td>AUTO #</td>
		<td> <input type="Text"> </td>
		</tr>
		<tr>
		<td>Monocytes (%):</td>
		<td> <input type="Text"> </td>
		<td> <input type="Text"> </td>
		<td>AUTO #</td>
		<td> <input type="Text"> </td>
		</tr>			
				<tr>
		<td>Basophils (%):</td>
		<td> <input type="Text"> </td>
		<td> <input type="Text"> </td>
		<td>AUTO #</td>
		<td> <input type="Text"> </td>
		</tr>
		<tr>
		<td>Eosinophils (%):</td>
		<td> <input type="Text"> </td>
		<td> <input type="Text"> </td>
		<td>AUTO #</td>
		<td> <input type="Text"> </td>
		</tr>
		
		</table>
		</div>
	</div>
	
	<!-- - end of this2 -->
	<!-- -wiget 3 -->
	<!-- <div class="portlet" style="width:98%">
	<div class="portlet-header">Immunophenotyping</div>
	<div class="portlet-content">
		<table cellpadding="0" cellspacing="0" border="0" id="CBC AND Differential" class="display">
		<br>
		<b>Preliminary Data</b>
		<br><br>
		<tr>
		<td>Region Analyzed%:</td><td><input type="Text"> </td>
		</tr>
		<tr>
		<td>Total CD34:</td><td><input type="Text"> </td>
		</tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" id="CBC AND Differential" class="display">
		<br>
		<b>Final Data</b>
		<br><br>
		<tr>
		<td>TNC:</td>
		<td> <input type="Text"> </td>
		<td>Total CD3:</td>
		<td> <input type="Text"> </td>
		</tr>
		<tr>
		<td>Region Analyzed%:</td>
		<td> <input type="Text"> </td>
		<td>Total CD3/kg</td>
		<td> <input type="Text"> </td>
		
		</tr>
		</tr>
		<tr>
		<td>Total CD34:</td>
		<td> <input type="Text"> </td>
		<td>PathoRest</td>
		<td> <input type="Text"> </td>
		</tr>			
				<tr>
		<td>Total CD34/kg</td>
		<td> <input type="Text"> </td>
		<td>Comment</td>
		<td> <input type="Text"> </td>
		</tr>
				</table>
		</div>
	</div>
	
	</div>
	<!--  end of widget  -->
	<!--  end of widget  -->
	
	
 
<section>

 <div class="cleaner"></div>
 <form>
 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value="overnightStorage"/><s:hidden id="widgetName" value="Equipments"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.equipments"/></div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="equipmentsTable" class="display">
					<thead>
						<tr>
							<th colspan="1"><s:text name="stafa.label.equipment"/></th>
							<th colspan="1"><s:text name="stafa.label.manufacturer"/></th>
							<th colspan="1"><s:text name="stafa.label.serial"/></th>
							<th colspan="1"><s:text name="stafa.label.expirationdate"/></th>				
							<th colspan="1"><s:text name="stafa.label.appearanceok"/></th>
						</tr>
						<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						</tr>
					</thead>
					<tbody>
						
						
						<tr>
							<td>Biological Saftey cabinet</td>
							<td>Fisher</td>
							<td>908763</td>
							<td>Jul 20, 2015</td>
							<td align="center"><input type="radio" id="yes" name="yes13" class=""/> Y &nbsp; <input type="radio" id="No" onClick="confirmIfNo();" name="yes13" class=""/> N</td>
						</tr>	
						
											
					</tbody>
			</table>
	</div>

	</div>	

</div>		
</form>	
</section>	


<div class="cleaner"></div>



<form id="frmCalculation">
<div class="column">	
		<div id="calculationDiv" >
				</div>
		
	</div>
</form>		
	<div class="cleaner"></div>
	

	
<div class="cleaner"></div>
<!-- 
<form>
<div class="column">       
        <div  class="portlet">
        <div class="portlet-header notes "><s:hidden id="moduleName" value="overnightStorage"/><s:hidden id="widgetName" value="Refrigerator"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.refrigerator"/></div>
            <div class="portlet-content">
           <div align="right" style="float:right;"><b>Print Label:</b><img src = "images/icons/print_small.png" align="right" style="width:20;height:20;float:right;"/></div>  
            <table cellpadding="0" cellspacing="0" border="0" id="refrigeratorTable" class="displays" style="width:400px">
                    <thead>
                        <tr>
                            <th>Product ID</th>
                            <th>Volume(mL)</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>BMT123-3</td>
                            <td>350</td>
                           
                        </tr>                                       
                    </tbody>
            </table>
           
    </div>
                                <div class="table_cell_input1">
                                	<form>
                                	<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
										<tr>
                               				<td width="17%" height="40px" valign="left"><div class="txt_align_label" align="left">Refrigerator Selection</div></td>
											<td width="48%" height="40px" valign="middle"><div class="mainselection"><select name="" id="" class=""><option>Select</option></select></div></td>
                                		</tr>
                                		</table>
                                	</form>
                               </div>   
                            <div align="right" style="float:right;">
                             <form>
                                <input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;&nbsp;
                                <input type="button" value="Next"/>
                            </form>
                            </div>
                   
        </div>
        </div>
        </form>
         -->
<!--   <div class="column">		
		<div  class="abc">
			<div class="abb">Refridgerator</div>
			</div>
</div>-->		

<section>


		
<div class="cleaner"></div>
</section>

</form>
</div>
<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td>Next:</td>
				<td><div class="mainselection"><select name="" id="nextProcess" ><option value="">Select</option><option>Production</option><option>logout</option></select></div></td>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;&nbsp;</td>
				<td><input type="button" value="Next" onclick="saveOvernightStorage()"/></td>
				</tr>
				</table>
				</form>
		</div>

<script>
$(function() {	
	
	
/*	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".info" )
	.append("<span style=\"float:right;\" class='ui-info'></span>");
	
	$( ".portlet-header .ui-info" ).click(function() {
		showPopUp("open","addqc","QC Information","1200","600");
	});
	
*/
});
$(function(){
    $("select").uniform();
  });
</script>
