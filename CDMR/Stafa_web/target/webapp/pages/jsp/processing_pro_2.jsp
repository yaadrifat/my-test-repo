<%@ include file="common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script>

function nextProcessing(){
	var nextToOriginalProduct = $("#nextToProcessing").val();
	//alert("s");
    	if(nextToOriginalProduct=="Cryopreservation")
        {
    		javascript:loadPage('html/stafa/cryo_prep.html');
        }
}


$(document).ready(function() {
	try{
		//alert("test");
//$('#table111').dataTable({"aaSorting": [[ 1, "desc" ]]});
	oTable = $('#equipmentsTable').dataTable({
		"bJQueryUI": true
		//"sPaginationType": "full_numbers"
	});
	oTable = $('#reagentsTable').dataTable({
		"bJQueryUI": true
		//"sPaginationType": "full_numbers"
	});
	
	// chedk with mari
	/*
	document.getElementById('equipmentsTable_next').setAttribute("onClick","removeTableClass()");
	document.getElementById('equipmentsTable_previous').setAttribute("onClick","removeTableClass()");
	document.getElementById('reagentsTable_next').setAttribute("onClick","removeTableClass()");
	document.getElementById('reagentsTable_previous').setAttribute("onClick","removeTableClass()");
	
	removeTableClass();
	removeTableClassOnChange("equipmentsTable_length");
	removeTableClassOnChange("reagentsTable_length");
	*/
	}catch (err){
		alert(err);
	}
});

$(function(){
    $("select").uniform();
  });
function removeTableClass(){
	
	$('#equipmentsTable_next').removeClass("ui-state-default");
	$('#equipmentsTable_previous').removeClass("ui-state-default");
	$('#reagentsTable_next').removeClass("ui-state-default");
	$('#reagentsTable_previous').removeClass("ui-state-default");

}
 function checkpoint(){
	 try{
		// alert("checkpoint");
	 var tnc= Number($("#tmptnc").val()) ;
	 var tnc2 = Number($("#tnc").text());
	 //alert("tnc2 " + tnc2 + "tnc " + tnc);
	 $("#tmptnc2").val(tnc2);
	// alert(" val " +  $("#tmptnc2").val());
	 $("#tmptv2").val($("#totalVolume").val());
	 $("#tmprbc2").val($("#rbc").text());
	if(tnc >0 && tnc2>0){
	 var tncr = round2Decimal(tnc/tnc2 *100);
	 if(Number(tncr)>0){
	//	 alert( tnc + " / " + tnc2);
	
	 $("#tncRecovery").text(tncr+"%");
 	}else{
 		 $("#tncRecovery").text("");
 	}
   }else{
		 $("#tncRecovery").text("");
   }
	 }catch(err){
		 alert("err" + err);
	 }
 }


</script>

<div class="cleaner"></div>

<!--float window-->

<div id="floatMenu">
<div id="forfloat_wind">
	<div id="box-outer">
		
		<a href="#" style="text-decoration:none" id="blind">&lt;&lt; </a>
			<div id="box">
				<div id="leftnav" >
				
<!-- recipient start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header notes"><s:hidden id="moduleName" value="processing"/><s:hidden id="widgetName" value="recipient"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.recipient"/></div>
							<div class="portlet-content">
						<table width="225" border="0">
						  <tr>
							<td width="89"><s:text name="stafa.label.id"/></td>
							<td width="120"><div id="recipientId"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.name"/></td>
							<td><div id="recipientName"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientaborh"/></td>
							<td><div id="recipientAboRh"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientdiagnosis"/></td>
							<td><div id="recipientDiagnosis"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientrid"/></td>
							<td><div id="recipientRID"></div></td>
						  </tr>
						</table>
					</div>
					</div>
					</div>
				
<!-- recipient-->


<!-- Product start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header notes"><s:hidden id="moduleName" value="processing"/><s:hidden id="widgetName" value="product"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.product"/></div>
						<div class="portlet-content">
						<table width="225" border="0">
							<tr>
							<td width="89"><s:text name="stafa.label.id"/></td>
							<td width="120"><div id="productId"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.type"/></td>
							<td><div id="productType"></div></td>
						  </tr>
						</table>		
					</div>
				</div>
				</div>
				
<!-- Product-->

<!-- Donor start-->
					<div class="column">
					<div class="portlet">
						<div class="portlet-header notes"><s:hidden id="moduleName" value="processing"/><s:hidden id="widgetName" value="donor"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.donor"/></div>
							<div class="portlet-content">
							<table width="225" border="0">
								<tr>
									<td width="89"><s:text name="stafa.label.id"/></td>
									<td width="120"><div id="donorId"></div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.name"/></td>
									<td><div id="donorName"></div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.donoraborh"/></td>
									<td><div id="donorAboRh"></div></td>
								</tr>
							</table>
						</div>
					</div>
					</div>

<!-- Donor-->
				
				</div>
				</div>
			
			</div>
		</div>

	

	

</div>
<!--float window end-->

<!--right window start -->	
<div id="forfloat_right">
	
<form action="#" id="login">
<div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value="processing"/><s:hidden id="widgetName" value="Calculation"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.calculation"/></div>
			<!--<div id="button_wrapper"><input class="scanboxSearch" type="text" size="25" placeholder="Scan and Enter ID" name="search" /></div>-->
			<!-- <div class="img_1"><a href="#"><img src="images/icons/edit.png" width="16" height="16" border="0" /></a></div> -->
		<div id="table_inner_wrapper">
						<div id="left_table_wrapper">					
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.totalvolume" /></div></td>
								<td width="50%" height="40px"><div align="left"><input type="text" id="totalVolume" name="totalVolume" class="date_input"  value="" onchange="javascript:fn_calculation();setTmpValues();" /></div></td>
							</tr>
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.dilution" /></div></td>
								<td width="50%" height="40px"><div align="left"><input type="text" id="dilution" name="dilution" class="date_input"  value="" onchange="javascript:fn_calculation();setTmpValues();"/></div></td>
							</tr>
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.weight" /></div></td>
								<td width="50%" height="40px"><div align="left"><input type="text" id="weight" name="weight" class="date_input"  value="" onchange="javascript:fn_calculation();setTmpValues();"/></div></td>
							</tr>
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.sample1" /></div></td>
								<td width="50%" height="40px"><div align="left"><input type="text" id="sample1" name="sample1" class="date_input"  value="" onchange="javascript:fn_calculation();setTmpValues();"/></div></td>
							</tr>
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.sample2" /></div></td>
								<td width="50%" height="40px"><div align="left"><input type="text" id="sample2" name="sample2" class="date_input"  value="" onchange="javascript:fn_calculation();setTmpValues();"/></div></td>
							</tr>
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.hct" /></div></td>
								<td width="50%" height="40px"><div align="left"><input type="text" id="hct" name="hct" class="date_input"  value="" onchange="javascript:fn_calculation();setTmpValues();"/></div></td>
							</tr>
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.hgb" /></div></td>
								<td width="50%" height="40px"><div align="left"><input type="text" id="hgb" name="hgb" class="date_input"  value="" onkeyup="javascript:fn_calculation();setTmpValues();"/></div></td>
							</tr>
						</table>					
					</div>
					<div id="middle_line"></div>
					<div id="right_inner_wrapper">				
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.avgcell" /></div></td>
								<td width="50%" height="40px"><div align="left"><label class="txt_label" id="avgcell" name="avgcell">Automatic #</label></div></td>
							</tr>
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.tnc" /></td>
								<td width="50%" height="40px"><div align="left"><label class="txt_label" id="tnc" name="tnc">Automatic #</label></div></td>
							</tr>
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.tncKg" /></div></td>
								<td width="50%" height="40px"><div align="left"><label class="txt_label" id="tncKg" name="tncKg">Automatic #</label></div></td>
							</tr>
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.rbc" /></div></td>
								<td width="50%" height="40px"><div align="left"><label class="txt_label" id="rbc" name="rbc">Automatic #</label></div></td>
							</tr>
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.rbcKg" /></div></td>
								<td width="50%" height="40px"><div align="left"><label class="txt_label" id="rbcKg" name="rbcKg">Automatic #</label></div></td>
							</tr>
						</table>					
					</div>
				</div>
		</div>
	</div>
</form>		
	
<div class="cleaner"></div>
	
<%-- <div class="column">		
		<div  class="portlet">
			<div class="portlet-header"><s:text name="stafa.label.endcheckpoints"/></div><br>
		<div class="portlet-content">
		<form action="#" id="login">
				<div id="table_inner_wrapper">
					<div id="left_table_wrapper">
						<div id="table_container_input">
							<div class="table_row_input1">
								<div class="table_cell_input1"><s:text name="stafa.label.tncrecovery"/></div>
								<div class="table_cell_input2"><span id="tncRecovery">90%</span>&nbsp;&nbsp;<input type="checkbox" name=""></div>
							</div>	
						</div>
					</div>
					
					<div id="right_table_wrapper">
						<div id="table_container_input">
							<div class="table_row_input1">
								<div class="table_cell_input1">TNC/kg (x10^8)</div>
								<div class="table_cell_input2">1.21&nbsp;&nbsp;&nbsp;<input type="checkbox" name=""></div>
							</div>						
						</div>
					</div>
				</div>
		   </form>			
			</div>
		</div>
	</div>	
	
<div class="cleaner"></div>

<div class="column">		
		<div  class="portlet">
			<div class="portlet-header"><s:text name="stafa.label.supernatant"/></div><br>
		<div class="portlet-content">
		<form action="#" id="login">
				<div id="table_inner_wrapper">
					<div id="left_table_wrapper">
						<div id="table_container_input">
							<div class="table_row_input1">
								<div class="table_cell_input1"><s:text name="stafa.label.supernatantvol"/></div>
								<div class="table_cell_input2"><input type="text" id="date" name="date"  value="" placeholder=""/></div>
							</div>	
							<div class="table_row_input1">
								<div class="table_cell_input1"><s:text name="stafa.label.disposition"/></div>
								<div class="table_cell_input2"><div class="mainselection"><select name="" id=""><option>Discard</option><option>Infuse to Donor</option><option>Re-Process</option><option>Re-Count</option></select></div></div>
							</div>	
						</div>
					</div>
					
					<div id="right_table_wrapper">
						<div id="table_container_input">
							<div class="table_row_input1">
								<div class="table_cell_input1"><s:text name="stafa.label.supernatanttnc"/></div>
								<div class="table_cell_input2"><input type="text" id="date" name="date" value="" placeholder=""/></div>
							</div>						
						</div>
					</div>
				</div>
		   </form>			
			</div>
		</div>
	</div>	 --%>
	
<div class="cleaner"></div>

<section>

<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td><b>Processing Page<a href="#" id="" class="cursor" onclick="javascript:loadAction('getProcessing.action');" >1</a>&nbsp;&nbsp;&nbsp;<a id="processing2" href="#" class="cursor" onclick="javascript:loadPage('html/stafa/processing_pro_2.html');" >2</a></b></td><td><div class="mainselection" ><s:select headerKey="" headerValue="Select"  listKey="protocol" listValue="protocolName" list="prodprotocolList" name="fkProtocol" id="nextProtocolList"/></div></td>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" value="Next" onClick="nextProcessing();"/></td>
				</tr>
			</table>
		</form>
		</div>

		<div class="cleaner"></div>
</section>
</div>
		
