
<%@ include file="common/includes.jsp" %>
<!-- <script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script> -->
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script type="text/javascript" src="js/barcodeScanBox.js"></script>
<script type="text/javascript" src="js/dataSearch.js"></script>

<script language="javascript">
function setValidation(flag){
	if(!flag){
		$("#aphSignOffDate").removeClass("validate_required ");
		$("#aphSignOffDate").removeClass("errorBorder");
		$("#aphSignOffDate").parent().find("sup").remove();
		$("#tempAphSignOffTime").removeClass("validate_required ");
		$("#tempAphSignOffTime").removeClass("errorBorder");
		$("#tempAphSignOffTime").parent().find("sup").remove();
		$("#aphVesselTempHigh").removeClass("validate_required");
		$("#aphVesselTempHigh").removeClass("errorBorder")
		$("#aphVesselTempHigh").parent().find("sup").remove();
		$("#aphVesselTempLow").removeClass("validate_required");
		$("#aphVesselTempLow").removeClass("errorBorder");
		$("#aphVesselTempLow").parent().find("sup").remove();
		$("#bloodBankProductArrival0").removeClass("validate_required");
		$("#bloodBankProductArrival0").removeClass("errorBorder");
		//$("#bloodBankProductArrival0").parent().find("sup").remove();
		$("#bloodBankProductArrival1").removeClass("validate_required");
		$("#bloodBankProductArrival1").removeClass("errorBorder");
		$("#bloodBankProductArrival1").parent().find("sup").remove();
		}else{
			var param ="{module:'APHERESIS',page:'TRANSPORT'}";
			markValidation(param);
		}
	}
function savePages(modeType){
	//alert("modeType"+modeType);
    // saveTransport();      
    if(modeType!="next"){
        $("#completedFlag").val("0");
    }else{
    	$("#completedFlag").val("1");
    }
    try{
        if(saveTransport()){
            return true;
        }
    }catch(e){
        alert("exception " + e);
    }
}


function getScanData(event,obj){
      if(event.which==13){
    	  event.which=13;
        getdata(event);
    }
}
function showTransportPopup(){
    showPopUp("open","transportedProducts","Transported Product Details","600","550");
}
function closePopup(){
    try{
        showPopUp("close","transportedProducts","Transported Product Details","600","550");
      }catch(err){
        alert("err" + err);
    }
}
function viewTransportList(response){
    var transportIds;
    var productSignTime;
    var productSignDate;
    var productTempHigh;
    var productTempLow;
    var bagIntact;
    var proDestination;
    var apheresisSignTime;
    var apheresisSignDate;
    var apheresisTempHigh;
    var apheresisTempLow;
    var notifiedBloodBank;
    $(response.transportList).each(function(i,v) {
        for(j=0;j<v.length;j++){
        	//$("#transport").val(v[0]);
            transportIds = v[13];
                /*var slicedTid = transportIds.substring(2);
                criteriaValue = slicedTid.substring(0,slicedTid.length-2);*/
            productSignTime = v[3];
                if(v[3]!=null){
                    var tmpDate = new Date(v[3]);
                    productSignTime = (tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes();
                    //alert("V[3] " + v[3]);
                    //productSignTime = ($.datepicker.formatTime('HH:mm', new Date(v[3])));

                }else{
                    productSignTime = "";
                }
            productSignDate = v[2];
                if(v[2]!=null){
                	productSignDate = converHDate(v[2]);
                   /* productSignDate = ($.datepicker.formatDate('M dd, yy', new Date(v[2])));*/
                        }else{
                            productSignDate = "";
                }
            productTempHigh = v[4];
           // productTempLow  = v[5];
            bagIntact = v[6];
                if(bagIntact=='1'){
                    bagIntact = 'Yes';
                }else{
                    bagIntact = 'No';
                }
            if(v[7]!=null){
                proDestination = $("#productdestination option[value='"+v[7]+"']").text();
            }
            apheresisSignTime = v[9];
                if(v[9]!=null){
                	var tmpDate = new Date(v[9]);
                    apheresisSignTime = (tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes();
                }else{
                    apheresisSignTime = "";
                }
            apheresisSignDate = v[8];
                if(v[8]!=null){
                	apheresisSignDate = converHDate(v[8]);
                    /*apheresisSignDate = ($.datepicker.formatDate('M dd, yy', new Date(v[8])));*/
                        }else{
                            apheresisSignDate = "";
                }
            if(v[10]!=null){
            	apheresisTempHigh = v[10];
            }else{
            	apheresisTempHigh = "";
            }
            /* if(v[11]!=null){
            	apheresisTempLow = v[11];
            }else{
            	apheresisTempLow = "";
            } */
            notifiedBloodBank = v[12];
                if(notifiedBloodBank=='1'){
                    notifiedBloodBank = 'Yes';  
                }else{
                    notifiedBloodBank = 'No';  
                }
        }
    });
    var popTable  = "<table cellpadding='10' border='1' align='center' cellspacing='0'>"+
                         "<tr><td>Transported Product ID</td><td>"+transportIds+"</tr>"+
                         "<tr><td>Sign-Off Time</td><td>"+productSignTime+"</td></tr>"+
                         "<tr><td>Sign-Off Date</td><td>"+productSignDate+"</td></tr>"+
                         "<tr><td>Transport Vessel Temp (C)</td><td>"+productTempHigh+"</td></tr>"+
                         /* "<tr><td>Product Temp.-Low(C)</td><td>"+productTempLow+"</td></tr>"+ */
                         "<tr><td>Product Bag Intact</td><td>"+bagIntact+"</td></tr>"+
                         "<tr><td>Product Destination</td><td>"+proDestination+"</td></tr>"+
                         "<tr><td>Apheresis Sign-Off Time</td><td>"+apheresisSignTime+"</td></tr>"+
                         "<tr><td>Apheresis Sign-Off Date</td><td>"+apheresisSignDate+"</td></tr>"+
                         "<tr><td>Transport Vessel Temp (C)</td><td>"+apheresisTempHigh+"</td></tr>"+
                         /* "<tr><td>Apheresis Temp.-Low(C)</td><td>"+apheresisTempLow+"</td></tr>"+ */
                         "<tr><td>Notified Blood Bank of Product Transport</td><td>"+notifiedBloodBank+"</td></tr>"+
                    "</table>"+
                      "<div align='right' style='float:right;'>"+
                          "<table cellpadding='0' cellspacing='0' class='' align='' border='0'>"+
                             "<tr><td><input type='button' onclick='closePopup();' value='Cancel'/></td></tr>"+
                        "</table>"+
                    "</div>";
        creatTable(popTable);
        $.uniform.restore('select');
        $('select').uniform();
}
function creatTable(popTable){
    showTransportPopup();
    $("#transportProductForm").html(popTable);
}

//? Muthu

/*function getdata(e){
    var searchUrl = "checkTransportData";
    var transportSearch =  $("#conformProductSearch").val();
    if((e.keyCode==13 && transportSearch!="")){
        var result = jsonDataCall(searchUrl,"transProducts="+transportSearch);
        var listLength=result.transportList.length;
        if(listLength<=0){
            var selectedProduct = $("#transProducts").val();
            var index = selectedProduct.indexOf(",'"+$("#conformProductSearch").val()+"',");
            if(index==-1){
                selectedProduct+="'"+$("#conformProductSearch").val()+"',";
            }
            $("#transProducts").val(selectedProduct);
            var slicedVal = selectedProduct.substring(1);
            criteriaValue = slicedVal.substring(0,slicedVal.length-1);
            constructTrasportTable(true,'transportProductTable');
        }else{
            viewTransportList(result);
        }}
}

function deleteProduct(deleteId){
var selectedProduct = $("#transProducts").val();
if(selectedProduct.indexOf(",'"+deleteId+"',")!=-1){
    selectedProduct= selectedProduct.replace((",'"+deleteId+"',") ,",");
}
$("#transProducts").val(selectedProduct);
var removedValue = selectedProduct.substring(1);
criteriaValue = removedValue.substring(0,removedValue.length-1);
constructTrasportTable(true,'transportProductTable');
}*/
/* function getdata(e){
	   var searchUrl = "checkBloodBankData";
	    var bloodSearch =  $("#conformProductSearch").val();
	    if((e.keyCode==13 && bloodSearch!="")){
	    	var result = jsonDataCall(searchUrl,"bloodProducts="+bloodSearch);
	        var listLength=result.bloodBankList.length;
	        if(listLength<=0){
	        	var selectedProduct = $("#bloodProducts").val();
	        	var index = selectedProduct.indexOf($("#conformProductSearch").val());
	        	if(index==-1){
	        		 if(selectedProduct == "," ){
	        			 selectedProduct+=$("#conformProductSearch").val();
	        			 var slicedVal = selectedProduct.substring(1);
	        			 $("#bloodProducts").val(slicedVal);
	        			  criteriaValue = "'"+slicedVal+"'";
	        			  //criteriaValue = slicedVal;
	        		 }else{
	        			 selectedProduct+=","+$("#conformProductSearch").val()+"";
	                     $("#bloodProducts").val(selectedProduct);
	                     var str = selectedProduct.lastIndexOf(",");
	                     var slicedVal = selectedProduct.substring(str,selectedProduct.length);
	                      criteriaValue+= ",'"+slicedVal.substring(1)+"'";
	                      //criteriaValue+= ","+slicedVal.substring(1);
	        		 	}
	        	}
	        	constructBloodBankTable(true,'bloodBankProductTable');
	        }else{
	        	viewBloodBankList(result);
	        }
	    }
	} */
function getdata(e){
    var searchUrl = "checkTransportData";
    var transportSearch =  $("#conformProductSearch").val();
   // alert("transportSearch:::"+transportSearch);
    if((e.which==13 && transportSearch!="")){
        var result = jsonDataCall(searchUrl,"transProducts="+transportSearch);
        var listLength=result.transportList.length;
       // alert("listLength::"+listLength);
        if(listLength<=0){
        	/* $(result.transportList).each(function(i,v){
        		alert(v.length);
        	}); */
            var selectedProduct = $("#transProducts").val();
           // alert("selectedProduct::"+selectedProduct);
            var index = selectedProduct.indexOf($("#conformProductSearch").val());
           // alert("Index:::"+index);
            if(index>=-1){
                if(selectedProduct == "," ){
                    selectedProduct+=$("#conformProductSearch").val();
                    var slicedVal = selectedProduct.substring(1);
                    $("#transProducts").val(slicedVal);
                    criteriaValue = "'"+slicedVal+"'";
                }else{
                    selectedProduct+=","+$("#conformProductSearch").val()+"";
                    $("#transProducts").val(selectedProduct);
                    var str = selectedProduct.lastIndexOf(",");
                    var slicedVal = selectedProduct.substring(str,selectedProduct.length);
                    criteriaValue+= ",'"+slicedVal.substring(1)+"'";
                    }
            }
           /*  var splitCriteria = criteriaValue.split(",");
            var deLim = "";
            finalCriteria = "";
            finalCriteria = " and ( ";
			for(var i=0;i<splitCriteria.length;i++){
				//finalCriteria += deLim+"pp.fk_product in (select pk_specimen from er_specimen where SPEC_ID in("+splitCriteria[i]+")) or ";
				//finalCriteria += " d.pk_donor = (select FK_DONOR from er_apheresis_collection where PLASMA_ID="+splitCriteria[i]+")";
				//finalCriteria += " and pp.pk_plannedprocedure = (select FK_PLANNEDPROCEDURE from er_apheresis_collection where PLASMA_ID="+splitCriteria[i]+")";
				//deLim = " or "; 
				finalCriteria += deLim+"pp.fk_product in (select pk_specimen from er_specimen where SPEC_ID in("+splitCriteria[i]+") union select fk_specimen from er_specimen where SPEC_ID in("+splitCriteria[i]+") )";
			}
			finalCriteria += ")"; */
            constructTrasportTable(true,'transportProductTable');
        }else{
        	 $(result.transportList).each(function(i,v){
        		if(v[14]=="0"){
        			$("#transport").val(v[0]);
        			$("#signOffDate").val(converHDate(v[2]));
        			
        			var signOfDate = new Date(v[3]);
                    var signOfTime = (signOfDate.getHours()<10?'0':'') + signOfDate.getHours()+":"+(signOfDate.getMinutes()<10?'0':'') + signOfDate.getMinutes();
                    
                    $("#tmpSignOffTime").val(signOfTime);
					$("#signOffTime").val(v[3]);
                    $("#vesselTempHigh").val(v[4]);
                    //$("#vesselTempLow").val(v[5]);
                    
                    if(v[6]!=null && v[6]==1){
			 			$("#productBag1").trigger("click");
			 			$("#productBag1").addClass("on");
			 		}else if(v[6]!=null && v[6]==0){
			 			$("#productBag0").trigger("click");
			 			$("#productBag0").addClass("on");
			 		}
                    
                    $("#productdestination").val(v[7]);
                    showBelowWidgets();
                    $("#aphSignOffDate").val(converHDate(v[8]));
                    
                    var aphSignOfDate = new Date(v[9]);
                    var aphSignOfTime = (aphSignOfDate.getHours()<10?'0':'') + aphSignOfDate.getHours()+":"+(aphSignOfDate.getMinutes()<10?'0':'') + aphSignOfDate.getMinutes();
                    
                    $("#tempAphSignOffTime").val(aphSignOfTime);
                    $("#aphSignOffTime").val(v[9]);
                    $("#aphVesselTempHigh").val(v[10]);
                    //$("#aphVesselTempLow").val(v[11]);
                    
                    if(v[12]!=null && v[12]==1){
			 			$("#bloodBankProductArrival1").trigger("click");
			 			$("#bloodBankProductArrival1").addClass("on");
			 		}else if(v[6]!=null && v[6]==0){
			 			$("#bloodBankProductArrival0").trigger("click");
			 			$("#bloodBankProductArrival0").addClass("on");
			 		}
                    $("#transProducts").val(v[13]);
                    constructTrasportTable(true,'transportProductTable');
        		}else if(v[14]=="1"){ 
        			 viewTransportList(result);
        	 	}
        	}); 
        	 $.uniform.restore('select');
        	 $('select').uniform();
        }
    }
}
function removeProduct(){
   var deleteIds = [];
   $("#transportProductTable #productId:checked").each(function(i){
        deleteIds.push($(this).val());
        deleteProduct($(this).val());
   });
}

function deleteProduct(deleteId){
   var temp=$("#transProducts").val();
   var splitArray = temp.split(",");
   var selectedIndex = splitArray.indexOf(deleteId);
   if(selectedIndex!=-1){
        splitArray.splice($.inArray(deleteId,splitArray),1);
   }
   $("#transProducts").val(splitArray);
   criteriaValue ="'"+splitArray+"'";
   constructTrasportTable(true,'transportProductTable');
   if(criteriaValue == "''"){
	   $("#transProducts").val(",");
   }
}
var criteriaValue;
var finalCriteria;

function constructTrasportTable(flag,tableId){
	
	//var criteria=finalCriteria;
	var criteria="";           
//	criteria = " SPEC_ID ="+criteriaValue+")";
    var transportColDef = [
                             {
                                    "aTargets": [0], "mDataProp": function ( source, type, val ) {
                                        return "<input type='checkbox' id='productId' name='productId' value='"+source[8] +"' >";
                                 }},
                              {"sTitle":"Product ID",
                                    "aTargets": [1], "mDataProp": function ( source, type, val ) {
                                        return source[8];
                                 }},
                              {    "sTitle":"Recipient Name",
                                     "aTargets": [2], "mDataProp": function ( source, type, val ) {
                                         return source[2];
                                  }},
                              {    "sTitle":"Recipient MRN",
                                      "aTargets": [3], "mDataProp": function ( source, type, val ) {
                                         return source[3];
                                    }},
                              {    "sTitle":"Donor Name",
                                        "aTargets": [4], "mDataProp": function ( source, type, val ) {
                                         return source[4];
                                    }},
                              {    "sTitle":"Donor MRN",
                                        "aTargets": [5], "mDataProp": function ( source, type, val ) {
                                         return source[5];
                                  }},
                              {    "sTitle":"Donor Type",
                                    "aTargets": [6], "mDataProp": function ( source, type, val ) {
                                     return source[9];
                              }},
                              {    "sTitle":"Product Type",
                                        "aTargets": [7], "mDataProp": function ( source, type, val ) {
                                         return source[6];
                                  }},
                             {    "sTitle":"Product Status",
                                    "aTargets": [8], "mDataProp": function ( source, type, val ) {
                                     return source[7];
                              }}
                            ];
    var transportColumns = [{"bSortable": false},
                               null,
                               null,
                               null,
                               null,
                               null,
                               null,
                               null,
                               null
                               ];
    var transportColManager = function () {
            $("#transportProductColumn").html($('#transportProductTable_wrapper .ColVis'));
    };

    var transportServerParams = function ( aoData ) {
                                                    aoData.push( { "name": "application", "value": "stafa"});
                                                    aoData.push( { "name": "module", "value": "apheresis_transport"});  
                                                   //aoData.push( { "name": "criteria", "value": criteria});
                                                    aoData.push( { "name": "param_count", "value":"1"});
													aoData.push( { "name": "param_1_name", "value":":SPEC_ID:"});
													aoData.push( { "name": "param_1_value", "value":criteriaValue});
                                                  
                                                    aoData.push( {"name": "col_1_name", "value": "lower(nvl((select SPEC_ID from er_specimen where pk_specimen =pp.fk_product),' '))" } );
                                                    aoData.push( { "name": "col_1_column", "value": "lower(proID)"});
                                                  
                                                    aoData.push( { "name": "col_2_name", "value": "lower(nvl( (select nvl(PERSON_LNAME, ' ' ) || ' ' ||nvl(PERSON_FNAME, ' ' ) from person where pk_person=d.fk_receipent),' '))"});
                                                    aoData.push( { "name": "col_2_column", "value": "lower(recip_name)"});
                                                  
                                                    aoData.push( {"name": "col_3_name", "value": "lower(nvl((select nvl(p.person_mrn,' ') from person where pk_person=d.fk_receipent),' '))" } );
                                                    aoData.push( { "name": "col_3_column", "value": "lower(recip_id)"});
                                                  
                                                    aoData.push( {"name": "col_4_name", "value": "lower(nvl(p.PERSON_LNAME, ' ' ) || ' ' ||nvl(p.PERSON_FNAME, ' ' ))" } );
                                                    aoData.push( { "name": "col_4_column", "value": "lower(donorname)"});
                                                  
                                                    aoData.push( {"name": "col_5_name", "value": "lower(nvl(p.person_mrn,' '))" } );
                                                    aoData.push( { "name": "col_5_column", "value": "lower(donorMRN)"});
                                                  
                                                    aoData.push( {"name": "col_6_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst =d.fk_codelstdonortype ),' '))" } );
                                                    aoData.push( { "name": "col_6_column", "value": "lower(donortype)"});
                                                  
                                                    aoData.push( {"name": "col_7_name", "value": "lower(nvl((select CODELST_DESC from er_codelst where PK_CODELST=d.FK_CODELSTPRODUCTTYPE),' '))" } );
                                                    aoData.push( { "name": "col_7_column", "value": "lower(pType)"});
                                                  
                                                    aoData.push( {"name": "col_8_name", "value": "lower(nvl((select CODELST_DESC from er_codelst where PK_CODELST=pp.FK_CODELSTDONORSTATUS),' '))" } );
                                                    aoData.push( { "name": "col_8_column", "value": "lower(status)"});
                                                  
                                                };
    constructTable(flag,tableId,transportColManager,transportServerParams,transportColDef,transportColumns);                                          
}

$(document).ready(function() {
    $(".titleText").html("<s:text name='stafa.label.transport'/>");
    hide_slidewidgets();
    hide_slidecontrol();
    clearTracker();
    show_innernorth();
    $('#inner_center').scrollTop(0);
    $("#tmpSignOffTime").timepicker({});
    $("#tempAphSignOffTime").timepicker({});
    try{
        showPopUp("close","transportedProducts","Transported Product Details","600","550");
    }catch(err){
        alert("err " + err);
    }
    var samTable = $('#transportProductTable').dataTable({
        "sPaginationType": "full_numbers",
        "aoColumns":[{"bSortable": false},
                       null,
                       null,
                       null,
                       null,
                       null,
                       null,
                       null,
                       null
                       ],
            "sDom": 'C<"clear">Rlfrtip',
            "oColVis": {
                "aiExclude": [ 0 ],
                "buttonText": "&nbsp;",
                "bRestore": true,
                "sAlign": "left"
            },
            "fnInitComplete": function () {
                $("#transportProductColumn").html($('#transportProductTable_wrapper .ColVis'));
            }
    });
      
    $( "#signOffDate" ).datepicker({
                                        dateFormat: 'M dd, yy',
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "c-50:c+45"
                                    });
    $( "#aphSignOffDate" ).datepicker({
                                        dateFormat: 'M dd, yy',
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "c-50:c+45"
                                    });
    
    
    var param ="{module:'APHERESIS',page:'TRANSPORT'}";
	markValidation(param);
	showBelowWidgets();
});
/* Scrolling Method Starts*/
$("#transportDiv").scroll(function(){
         var newMargin = $(this).scrollLeft();
         $('#transportProductTable_length').css('marginLeft', newMargin);
         $('#transportProductTable_length').css('width', '20%');
         $('#transportProductTable_filter').css('width', '20%');
         $('#transportProductTable_info').css('marginLeft', newMargin);
         $('#transportProductTable_info').css('width', '40%');
    });

function saveTransport(){
    var url='saveTransport';
    var transProducts="";
    $("#transportProductTable tbody tr").each(function (row){
    	$(this).find("td").each(function (col){
    		if(col == 1){
    			transProducts+= $(this).text() +",";
    		}
    	});
    		
    });
    if(transProducts.length >0){
    	transProducts = transProducts.substring(0,(transProducts.length-1));
    }
    $("#transProducts").val(transProducts);
    var response = ajaxCall(url,"transportForm");
    //alert("Data saved succesfully");
    stafaAlert('Data saved Successfully','');
    $("#transport").val(response.transport);
    //$("#main").html(response);
    return true;
}
$(function(){
    $("select").uniform();
});
function setvalues(cBoxId,textId){
    var s=$('#'+cBoxId).attr('checked');
    if(s=='checked'){
        $('#'+textId).val("N/A");
    }else {
        $('#'+textId).val("");
    }
}
function setSignOffTime(){
    $('#signOffTime').val($('#signOffDate').val() +" "+ $('#tmpSignOffTime').val());
}
function apheresisSignOffTime(){
    $('#aphSignOffTime').val($('#aphSignOffDate').val() +" "+ $('#tempAphSignOffTime').val());
}
function showBelowWidgets(){
    var selectedValue=$("#productdestination option:selected").text();
     if(selectedValue=="Blood Bank"){
        $("#apheresisSignOffWidget").css("display","block");
		setValidation(true);
    }else{
        $("#apheresisSignOffWidget").css("display","none");
		setValidation(false);
    }
}
</script>

<div class="cleaner"></div>

<form action="#" id="transportForm" onsubmit="return avoidFormSubmitting();">
<s:hidden id="transport" name="transport" value="0"/>
<s:hidden id="completedFlag" name="completedFlag"/>
<s:hidden id="transProducts" name="transProducts" value=","/>
<div class="column">
    <div  class="portlet">
    <div class="portlet-header"><s:text name="stafa.label.producttable"/></div><br>
        <div class="portlet-content">
            <div id="button_wrapper">
	            <div style="margin-left:40%;">
       				<input class="scanboxSearch barcodeSearch"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;" onkeyup="performScanData(event,this);" />
					<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" onfocus="changeType(this)"/>
	 			</div>
            </div>
            <table>
                <tr>
                    <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="removeProduct();"/>&nbsp;&nbsp;<label class="cursor"  onclick="removeProduct();"><b>Remove</b></label>&nbsp;</div></td>
                </tr>
            </table>
            <div id="transportDiv" style="overflow-x:auto;overflow-y:hidden;">
            <table cellpadding="0" cellspacing="0" border="0" id="transportProductTable" class="display">
                <thead>
                    <tr>
                        <th width="4%" id="transportProductColumn"></th>
                        <th><s:text name="stafa.label.productid"/></th>
                        <th><s:text name="stafa.label.recipientname"/></th>
                        <th><s:text name="stafa.label.recipientmrn"/></th>
                        <th><s:text name="stafa.label.donorname"/></th>
                        <th><s:text name="stafa.label.donorid"/></th>
                        <th>Donor Type</th>
                        <th><s:text name="stafa.label.producttype"/></th>
                        <th><s:text name="stafa.label.productstatus"/></th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

    <div class="cleaner"></div>

<div class="column">
    <div  class="portlet">
    <div class="portlet-header"><s:text name="stafa.label.productdeparture"/></div>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper"><s:text name="stafa.label.signoffdate"/></td>
                    <td class="tablewapper1"><input type="text" id="signOffDate" name='signOffDate' class="dateEntry"/></td>
                    <td class="tablewapper">Transport Vessel Temp (C)</td>
                    <td class="tablewapper1"><input type="text" id="vesselTempHigh" name="vesselTempHigh" class=""></td>
                </tr>
                <tr>
                    <td class="tablewapper"><s:text name="stafa.label.signofftime"/></td>
                    <td class="tablewapper1"><s:hidden  id="signOffTime" name="signOffTime" Class="" /><s:textfield id="tmpSignOffTime" name="tmpSignOffTime" Class="" onchange="setSignOffTime();"/></td>
                    <td class="tablewapper">Product Bag Intact</td>
                    <td class="tablewapper1"><s:radio name="productBag" list="#{'1':'Yes','0':'No'}"/></td>
                    <%-- <td class="tablewapper"><s:text name="stafa.label.transporttemplow"/></td>
                    <td class="tablewapper1"><input type="text" id="vesselTempLow" name="vesselTempLow" class="">&nbsp;<input type="checkbox" id="vesselTempNA" name="vesselTempNA"  onclick="setvalues('vesselTempNA','vesselTempLow')"/>N/A</td> --%>
                </tr>
               <%--  <tr>
                    <td class="tablewapper"><s:text name="stafa.label.signofftime"/></td>
                    <td class="tablewapper1"><s:hidden  id="signOffTime" name="signOffTime" Class="" /><s:textfield id="tmpSignOffTime" name="tmpSignOffTime" Class="" onchange="setSignOffTime();"/></td>
                    <td class="tablewapper"><s:text name="stafa.label.transporttemplow"/></td>
                    <td class="tablewapper1"><input type="text" id="vesselTempLow" name="vesselTempLow" class="">&nbsp;<input type="checkbox" id="vesselTempNA" name="vesselTempNA"  onclick="setvalues('vesselTempNA','vesselTempLow')"/>N/A</td>
                </tr>
                <tr>
                    <td class="tablewapper"></td>
                    <td class="tablewapper1"></td>
                    <td class="tablewapper">Product Bag Intact</td>
                    <td class="tablewapper1"><s:radio name="productBag" list="#{'1':'Yes','0':'No'}"/></td>
                </tr> --%>
            </table>
        </div>
    </div>  
</div>

    <div class="cleaner"></div>

<div class="column">
    <div  class="portlet">
    <div class="portlet-header"><s:text name="stafa.label.productdestination"/></div>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper"><s:text name="stafa.label.productdestination"/></td>
                    <td class="tablewapper1"><s:select id="productdestination"  name="productdestination" list="lstProductDestination" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="showBelowWidgets()"/></td>
                    <td class="tablewapper">&nbsp;</td>
                    <td class="tablewapper1">&nbsp;</td>
                 </tr>
            </table>
            <div id="apheresisSignOffWidget" style="display:none">
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td style="font-weight:bold;padding:1%"><s:text name="stafa.label.apheresissignoff"/></td>
                </tr>
            </table>
          
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper"><s:text name="stafa.label.signoffdate"/></td>
                    <td class="tablewapper1"><input type="text" id="aphSignOffDate" name='aphSignOffDate' class="dateEntry"/></td>
                    <td class="tablewapper">Transport Vessel Temp (C)</td>
                    <td class="tablewapper1"><input type="text" id="aphVesselTempHigh" name="aphVesselTempHigh" class=""></td>
                </tr>
                <tr>
                    <td class="tablewapper"><s:text name="stafa.label.signofftime"/></td>
                    <td class="tablewapper1"><s:hidden  id="aphSignOffTime" name="aphSignOffTime" Class="" /><s:textfield id="tempAphSignOffTime" name="tempAphSignOffTime" Class="" onchange="apheresisSignOffTime();"/></td>
                    <%-- <td class="tablewapper"><s:text name="stafa.label.transporttemplow"/></td>
                    <td class="tablewapper1"><input type="text" id="aphVesselTempLow" name="aphVesselTempLow" class="">&nbsp;<input type="checkbox" id="aphvesselTempNA" name="aphvesselTempNA"  onclick="setvalues('aphvesselTempNA','aphVesselTempLow')"/>N/A</td> --%>
                </tr>
            </table>
          
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td style="font-weight:bold;padding:1%"><s:text name="stafa.label.bloodbank"/></td>
                </tr>
            </table>
          
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="questionsAlign">Notified Blood Bank of Product Transport</td>
                    <td width="25%" height="40px"><s:radio name="bloodBankProductArrival" list="#{'1':'Yes','0':'No'}"/></td>
                </tr>
            </table>
            </div>
        </div>
    </div>  
</div>
</form> 
<div class="cleaner"></div>
  <form onsubmit="return avoidFormSubmitting()">
<div align="left" style="float:right;">
    <table width="100%" cellpadding="0" cellspacing="0" class="" align="" border="0">
        <tr>
        <td><span><label>Next:</label>
        <select name="nextOption" id="nextOption" class=""><option value="loadNurseHomePage.action">HomePage</option><option value="logout.action">Logout</option></select>
        <input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
        <input type="button" value="Next" onclick="verifyeSign('eSignNext','next')"/></span></td>
        </tr>
    </table>
</div>
</form>  
<div id="transportedProducts" class="model" title="addNew" style="display: none">
    <form action="#" id="transportProductForm">
    </form>
</div>
<div class="cleaner"></div>
<script>

$(function() {
 
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
 



    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

});
</script>