<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page import="com.velos.stafa.util.VelosStafaConstants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientStatus"/></td>
       		            		<td class="tablewapper1"><s:select cssStyle = "width:auto" id="recipientStatus" value = "%{recipientDetails.recipientStatus}"  name="" list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@RECIPIENT_STATUS]" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientComments"/></td>
       		            		<td class="tablewapper1"><s:textarea id="recipientStatusComments" name="recipientStatusComments"  rows="5" cols="50" wrap="hard" value= "%{recipientDetails.recipientStatusComments}" style="width:800;height:190;" ></s:textarea></td>
       		            		
       		            	</tr>
       		            	
       		            </table>