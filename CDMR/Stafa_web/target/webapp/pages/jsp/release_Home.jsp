<%@ taglib prefix="s" uri="/struts-tags"%>
<style>
.addspan td
{
vertical-align:top;
white-space:nowrap;
}

</style>
<script>
$( ".column" ).sortable({
	connectWith: ".column",
    cursor: 'move',
    cancel:	".portlet-content"
});
$(document).ready(function(){
	
	$("#dailyAssignTable").dataTable({
										"bJQueryUI": true,
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null
	 										              ],
											"oColVis": {
												"aiExclude": [ 0 ],
												"buttonText": "&nbsp;",
												"bRestore": true,
												"sAlign": "left"
											},
											"fnInitComplete": function () {
										        $("#dailyAssignedColumn").html($('.ColVis:eq(0)'));
											}
	});
	$("#monthlyDutyTable").dataTable({
										"bJQueryUI": true,
										"sDom": 'C<"clear">Rlfrtip',
										"aoColumns": [ { "bSortable":false},
 										               null,
 										               null,
 										               null,
 										               null
 										              ],
										"oColVis": {
											"aiExclude": [ 0 ],
											"buttonText": "&nbsp;",
											"bRestore": true,
											"sAlign": "left"
										},
										"fnInitComplete": function () {
									        $("#monthlyDutyColumn").html($('.ColVis:eq(1)'));
										}
	});
	$("#dailyAssignTable tbody tr td").each(function(i){
		$(this).find('span').hide();
		var len = $(this).find('span').find('td').length;
		if (len > 0){
			
			$('img').attr("style","visibility:true");
		}
	});	

	

	
	
	    $("select").uniform(); 
		oTable.thDatasorting('dailyAssignTable');
		oTable.thDatasorting('monthlyDutyTable');
});
function showTable(id){
	
	$(this).find('span').show();
	$("#"+id).attr("style","visibility:true");
	
}	
	
</script>
<div class="cleaner"></div>

<div class="cleaner"></div>
 


<div class="cleaner"></div>
<div class="column">		
	<div  class="portlet">
		<div class="portlet-header notes">Daily Assigned Tasks</div>
			<div class="portlet-content">
				<div style="overflow-x:auto;overflow-y:hidden;">
				<form>
					<table cellpadding="0" cellspacing="0" border='0' id="dailyAssignTable" class="display addspan">		
					<thead>
			        	<tr>
			        		<th width="4%" id="dailyAssignedColumn">
				            <th colspan='1'>Task</th>
				            <th colspan='1'>Product ID</th>
				            <th colspan='1'>Recipient ID</th>
				            <th colspan='1'>Recipient Name</th>
				            <th colspan='1'>Product Type</th>
				            <th colspan='1'>Facility</th>
				            <th colspan='1'>Product Status</th>
				            <th colspan='1'>Intended Disposition</th>
				            <th colspan='1'>Options</th>
				            <th colspan='1'>Task Complete</th>
			          </tr>
			          <tr>
			        		<th width="4%" id="dailyAssignedColumn">
				            <th>Task</th>
				            <th>Product ID</th>
				            <th>Recipient ID</th>
				            <th>Recipient Name</th>
				            <th>Product Type</th>
				            <th>Facility</th>
				            <th>Product Status</th>
				            <th>Intended Disposition</th>
				            <th>Options</th>
				            <th>Task Complete</th>
			          </tr>
			        </thead>
			        <tbody>
			       		<tr>
				        	<td></td>
				            <td>Release for Relocation</td>
				            <td>
				            	
				            	BMT125-3 <img style="visibility:hidden" src='images/icons/addnew.jpg' onclick="showTable('a');">
						    		<span id="a" style="visibility:hidden">
							    		<table>
									    	<tr>
									    		<td>BMT125-9</td>
									    	</tr>
									    	<tr>
									    		<td>BMT125-9</td>
									    	</tr>
										</table>
						    		</span>
						    	
				            </td>
				            <td >
				            	
				            	MDA789539<img style="visibility:hidden" src='images/icons/addnew.jpg' onclick="showTable('b');">
					                <span id="b" style="visibility:hidden">
							    		<table >
									    	<tr>
									    		<td>MDA789539</td>
									    	</tr>
									    	<tr>
									    		<td>MDA789539</td>
									    	</tr>
										</table>
						    		</span>
					    	</td>
				            <td >
				            	
				            	Jennifer Lee <img style="visibility:hidden" src='images/icons/addnew.jpg' onclick="showTable('c');">
					                <span id="c" style="visibility:hidden">
							    		<table >
									    	<tr>
									    		<td >Jennifer Lee</td>
									    	</tr>
									    	<tr>
									    		<td >Jennifer Lee</td>
									    	</tr>
										</table>
						    		</span>
					    	</td>
				            <td nowrap style="v-align:top">
				            	
				            	HPC-A<img style="visibility:hidden" src='images/icons/addnew.jpg' onclick="showTable('d');">
					                <span id="d" style="visibility:hidden">
							    		<table>
									    	<tr>
									    		<td>HPC-A</td>
									    	</tr>
									    	<tr>
									    		<td>HPC-A</td>
									    	</tr>
										</table>
						    		</span>
				            </td>
				            <td >

				            		CORE Lab<img style="visibility:hidden" src='images/icons/addnew.jpg' onclick="showTable('e');">
					                <span id="e" style="visibility:hidden">
						    			<table>
									    	<tr>
									    		<td>CORE Lab</td>
									    	</tr>
									    	<tr>
									    		<td>CORE Lab</td>
									    	</tr>
										</table>
						    		</span>
				            </td>
				            <td>
				            	
				            	Shipped<img style="visibility:hidden" src='images/icons/addnew.jpg' onclick="showTable('f');">
					                <span id="f" style="visibility:hidden">
							    		<table>
									    	<tr align="left">
									    		<td>Shipped</td>
									    	</tr>
									    	<tr align="left">
									    		<td>Shipped</td>
									    	</tr>
										</table>
						    		</span>
						    </td>
				            <td >
				            
				            	Release for Relocation<img style="visibility:hidden" src='images/icons/addnew.jpg' onclick="showTable('g');">
					                <span id="g" style="visibility:hidden">
							    		<table>
									    	<tr>
									    		<td>Release for Relocation</td>
									    	</tr>
									    	<tr>
									    		<td >Release for Relocation</td>
									    	</tr>
										</table>
						    		</span>
				            </td>
			            	<td><select><option>Select</option></select></td>
			            	<td><input type="checkbox" /></td>
			        	</tr> 
			    	</tbody>
				</table>
       		</form>
    		</div>
    	</div>
	</div>
</div>

<div class="cleaner"></div>

<div class="column">		
	<div  class="portlet">
		<div class="portlet-header notes"><input type="hidden" name="Notes1" id="1"/>Monthly Duty</div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="monthlyDutyTable" class="display">
			<thead>
				<tr>
					<th width="4%" id="monthlyDutyColumn">
					<th colspan='1'>Time Frame</th>
					<th colspan='1'>Duty</th>
					<th colspan='1'>Owner</th>
					<th colspan='1'>Status</th>
				</tr>
				<tr>
					<th width="4%" id="monthlyDutyColumn">
					<th>Time Frame</th>
					<th>Duty</th>
					<th>Owner</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td>Jul 2-7, 2012</td>
					<td>Retrieve Products from Cryogene</td>
					<td>Tech2</td>
					<td>In Progress</td>
								
				</tr>
				<tr>
					<td></td>
					<td>Jul 2-9, 2012</td>
					<td>Track Shipping</td>
					<td>Tech2</td>
					<td>In Progress</td>
					
				</tr>
			</tbody>
			</table>
		</div>
	</div>	
</div>

<script>
$(function() {
	$('.jclock').jclock();
	
	$( ".column" ).sortable({
		connectWith: ".column"
			/*,
        handle: '.portlet-header',
        cursor: 'move',
        forcePlaceholderSize: true*/
			
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});

	/**For Notes**/
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

	/* $( ".portlet-header .ui-addnew" ).click(function() {
		showPopUp("open","addNewDialog","Add New Acquision","900","450")
	}); */
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");
	$( ".portlet-header .ui-reset " ).click(function() {
		var formName = document.getElementById("login");
		//clearForm(formName);
		clear();
	});

// 	$( ".portlet-header .ui-notes " ).click(function() {
// 		showNotes('open');
// 	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});


var now = new Date();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivalDate').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new Date()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};



$("#timeZone").html(timezones[-offset / 60]);

</script>
 