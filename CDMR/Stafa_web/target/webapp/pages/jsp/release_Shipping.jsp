<%@ taglib prefix="s" uri="/struts-tags"%>
<style>
.shippingSpace td{
padding:5px 10px;
}
</style>
<script>
$( ".column" ).sortable({
	connectWith: ".column",
    cursor: 'move',
    cancel:	".portlet-content"
});
$(document).ready(function(){
		var oTable = $("#shippingTable").dataTable({
		 										"bJQueryUI": true,
		 										"sDom": 'C<"clear">Rlfrtip',
		 										"aoColumns": [ { "bSortable":false},
		 										               null,
		 										               null,
		 										               null,
		 										               null,
		 										               null,
		 										               null,
		 										               null,
		 										               null,
		 										               null,
		 										               null,
		 										               null
		 										               ],	
		 										    "oColVis": {
		 											"aiExclude": [ 0 ],
		 											"buttonText": "&nbsp;",
		 											"bRestore": true,
		 											"sAlign": "left"
		 										},
		 										"fnInitComplete": function () {
		 									        $("#shippingColumn").html($('.ColVis:eq(0)'));
		 										}
												});
		oTable.thDatasorting('shippingTable');
		
	    $("select").uniform(); 
  });
</script>
<div class="cleaner"></div>

<div class="cleaner"></div>
 
<div class="column">		
	<div  class="portlet">
		<div class="portlet-header notes">Products to be Shipped</div>
			<div class="portlet-content">
				<div style="overflow-x:auto;overflow-y:hidden;">
				<table cellpadding="0" cellspacing="0" border="0" id="shippingTable" class="display">
				<thead>
					   <tr>
					   		<th width="4%" id="shippingColumn"></th>
							<th colspan="1"><s:text name="stafa.label.recipientid"/></th>
							<th colspan="1"><s:text name="stafa.label.recipientname"/></th>
							<th colspan="1"><s:text name="stafa.label.donorid"/></th>
							<th colspan="1"><s:text name="stafa.label.donorname"/></th>
							<th colspan="1"><s:text name="stafa.label.productid"/></th>
							<th colspan="1"><s:text name="stafa.label.producttype"/></th>
							<th colspan="1">Collection Date</th>
							<th colspan="1">Vessel</th>
							<th colspan="1">Slot Location</th>
							<th colspan="1">Documents</th>
							<th colspan="1">Retrieval Date</th>
					   </tr>
					   <tr>
					   		<th width="4%" id="shippingColumn"></th>
							<th ><s:text name="stafa.label.recipientid"/></th>
							<th ><s:text name="stafa.label.recipientname"/></th>
							<th ><s:text name="stafa.label.donorid"/></th>
							<th ><s:text name="stafa.label.donorname"/></th>
							<th ><s:text name="stafa.label.productid"/></th>
							<th ><s:text name="stafa.label.producttype"/></th>
							<th >Collection Date</th>
							<th >Vessel</th>
							<th >Slot Location</th>
							<th >Documents</th>
							<th >Retrieval Date</th>
					   </tr>
				</thead>
				<tbody>
						<tr>
							<td></td>
							<td>MDA789539</td>
							<td>Jennifer Lee</td>
							<td>MDA789539</td>
							<td>Jennifer Lee</td>
							<td>BMT125-3</td>
							<td>HPC-M</td>
							<td>06-11-2012</td>
							<td>100ml Bag</td>
							<td>T20-2-b</td>
							<td><input type="checkbox"></td>
							<td>Jul 9,2012</td>
						</tr>
						<tr>
							<td></td>
							<td>MDA789539</td>
							<td>Jennifer Lee</td>
							<td>MDA789539</td>
							<td>Jennifer Lee</td>
							<td>BMT125-9</td>
							<td>HPC-M</td>
							<td>06-11-2012</td>
							<td>100ml Bag</td>
							<td>T20-2-b</td>
							<td><input type="checkbox"></td>
							<td>Jul 9,2012</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>	
</div>	
<div class="column">		
	<div  class="portlet">
		<div class="portlet-header notes">Shipping Info</div>
			<div class="portlet-content">
				<div style="overflow-x:auto;overflow-y:hidden;">
				<form>
				<table cellpadding="0" cellspacing="0" border="0" id="shippingInfoTable" class="shippingSpace">
				<tbody>
					<tr>
						<td><s:text name="stafa.label.requestdate"/></td>
						<td><input type="text"/></td>
						<td><s:text name="stafa.label.shippingdate"/></td>
						<td><input type="text"/></td>
						<td><s:text name="stafa.label.institution"/></td>
						<td><input type="text"/></td>
						<td><s:text name="stafa.label.department"/></td>
						<td><input type="text"/></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.pickuplocation"/></td>
						<td><input type="text"/></td>
						<td><s:text name="stafa.label.contactname"/></td>
						<td><input type="text"/></td>
						<td><s:text name="stafa.label.contactphone"/></td>
						<td><input type="text"/></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.couriername"/></td>
						<td><select><option>Select</option></select></td>
						<td><s:text name="stafa.label.shippingmethod"/></td>
						<td><select><option>Select</option></select></td>
						<td><s:text name="stafa.label.emergencycontact"/></td>
						<td><input type="text"/></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.typeofstorage"/></td>
						<td><select><option>Select</option></select></td>
						<td><s:text name="stafa.label.samplecontainer"/></td>
						<td><select><option>Select</option></select></td>
						<td><s:text name="stafa.label.noofbox"/></td>
						<td><input type="text"/></td>
						<td><s:text name="stafa.label.biohazardmaterials"/></td>
						<td><input type="radio"/>Y&nbsp;<input type="radio"/>N</td>
					</tr>
					<tr>
						<td>Lab Tech 1</td>
						<td><input type="text"/></td>
						<td>Sign-off Time</td>
						<td><span class="jclock"></span></td>
						<td>Lab Tech 2</td>
						<td><input type="text"/></td>
						<td>Sign-off Time</td>
						<td><span class="jclock"></span></td>
					</tr>
				</tbody>
				</table>
				</form>
			</div>
		</div>
	</div>	
</div>	

<div class="cleaner"></div>
 <div class="column">		
	<div  class="portlet">
		<div class="portlet-header notes">Tracking Information</div>
			<div class="portlet-content">
				<form>
				<table cellpadding="0" cellspacing="0" border="0" id="shippingTable" class="shippingSpace">
				<tbody>
					<tr>
						<td><s:text name="stafa.label.trackingnumber"/></td>
						<td><input type="text"/></td>
					</tr>
				</tbody>
				</table>
			</form>
		</div>
	</div>	
</div>	
<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td>Next:</td>
				<td><div><div class="mainselection"><select name="" id=""><option value="">Select</option><option>Document Review</option><option>Preparation</option></select></div></div></td>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="nextProcessPreProduct();" value="Next"/></td>
				</tr>
				</table>
				</form>
		</div>

<script>

$(function() {
	$('.jclock').jclock();
	
	$( ".column" ).sortable({
		connectWith: ".column"
			/*,
        handle: '.portlet-header',
        cursor: 'move',
        forcePlaceholderSize: true*/
			
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});

	/**For Notes**/
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

	/* $( ".portlet-header .ui-addnew" ).click(function() {
		showPopUp("open","addNewDialog","Add New Acquision","900","450")
	}); */
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");
	$( ".portlet-header .ui-reset " ).click(function() {
		var formName = document.getElementById("login");
		//clearForm(formName);
		clear();
	});

// 	$( ".portlet-header .ui-notes " ).click(function() {
// 		showNotes('open');
// 	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});


var now = new Date();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivalDate').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new Date()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};



$("#timeZone").html(timezones[-offset / 60]);

</script>
 