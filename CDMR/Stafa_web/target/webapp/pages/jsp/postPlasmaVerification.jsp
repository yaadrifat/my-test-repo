<%@ include file="common/includes.jsp" %>

<script>
function savePopup(){
	try{
		if(saveQuestions('postPlasmalabelverification')){
			alert("Data Saved Successfully");
			$("#plasmaLabelVerification").dialog('close');
		}
	
	}catch(e){
		alert("exception " + e);
	}
	return true;
}


$(function() {
	$('input[name=postPlasmaisbtlblatt]').bind("change",function(){
    	var isbtval=$(this).val();
    	if(isbtval==1){
    		    $('.isbtlabelshow').show();
    	}
    	if(isbtval==0){
		    $('.isbtlabelshow').hide();
	}
    });
    $('input[name=postPlasmawarnlblatt]').bind("change",function(){
    	var isbtval=$(this).val();
    	if(isbtval==1){
    		    $('.warninglabel').show();
    	}
    	if(isbtval==0){
		    $('.warninglabel').hide();
	}
});
    loadquestions();
});

function closePlasmalabelVerification(){
	$("#plasmaLabelVerification").dialog('close');
}
</script>

<form id="postPlasmalabelverificationform" onsubmit="return avoidFormSubmitting()">
<%-- <s:hidden id="donor" name="donor" value="%{donor}"/>
<s:hidden id="donorProcedure" name="donorProcedure" value="%{donorProcedure}"/> --%>
	<table  id="postPlasmalabelverification" cellpadding="0"  cellspacing="0" class="" align="center" border="0" width="100%">
		<tr>
			<td class="isbtlabel">ISBT Label Attached/Affixed</td>
			<td><input type="hidden" name="postPlasmaisbtlblatt_id" id="postPlasmaisbtlblatt_id" value="0"><input type="radio" id="postPlasmaisbtlblatt_yes" name="postPlasmaisbtlblatt" value="1" class="comclass"/>Yes&nbsp;<input type="radio" name="postPlasmaisbtlblatt" id="postPlasmaisbtlblatt_no" value="0" class="comclass" />No<input type="hidden" name="questionsCode" id="questionsCode" value="postPlasmaisbtlblatt" /></td>
			
		</tr>
		<tr class="isbtlabelshow" style="display:none;">
		
			<td align=""  style="padding-left:3%;">ISBT Label Complete and Legible</td>
			<td><input type="hidden" name="postPlasmaisbtlblcom_id" id="postPlasmaisbtlblcom_id" value="0"><input type="radio" id="postPlasmaisbtlblcom_yes" name="postPlasmaisbtlblcom" value="1" class="comclass"/>Yes&nbsp;<input type="radio" id="postPlasmaisbtlblcom_no" name="postPlasmaisbtlblcom" value="0" class="isbtcom"/>No<input type="hidden" name="questionsCode" id="questionsCode" value="postPlasmaisbtlblcom" /></td>
			
		</tr>	
		<tr>
			<td >Warning Label Attached/Affixed</td>
			<td><input type="hidden" name="postPlasmawarnlblatt_id" id="postPlasmawarnlblatt_id" value="0"><input type="radio" id="postPlasmawarnlblatt_yes" name="postPlasmawarnlblatt"  value="1"/>Yes&nbsp;<input type="radio" id="postPlasmawarnlblatt_no"  name="postPlasmawarnlblatt" value="0"/>No&nbsp;<input type="radio" id="postPlasmawarnlblatt_na" name="postPlasmawarnlblatt" value="-1"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="postPlasmawarnlblatt" /></td>
			
		</tr>
		<tr class="warninglabel" style="display:none;">
		
			<td align="" style="padding-left:3%;">Warning:Reactive Test Results for</td>
			<td><input type="hidden" name="postPlasmawarnrecative_id" id="postPlasmawarnrecative_id" value="0"><input type="radio" id="postPlasmawarnrecative_yes" name="postPlasmawarnrecative" value="1"/>Yes&nbsp;<input type="radio" name="postPlasmawarnrecative" id="postPlasmawarnrecative_no" value="0"/>No&nbsp;<input type="radio" id="postPlasmawarnrecative_na" name="postPlasmawarnrecative" value="-1"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="postPlasmawarnrecative" /></td>
			
		</tr>	
		<tr class="warninglabel" style="display:none;">
		
			<td align="" style="padding-left:3%;">Warning:Advise Patient of Communicable Disease Risk</td>
			<td><input type="hidden" name="postPlasmawarndisease_id" id="postPlasmawarndisease_id" value="0"><input type="radio" id="postPlasmawarndisease_yes" name="postPlasmawarndisease" value="1"/>Yes&nbsp;<input type="radio" id="postPlasmawarndisease_no" name="postPlasmawarndisease" value="0"/>No&nbsp;<input type="radio" id="postPlasmawarndisease_na" name="postPlasmawarndisease" value="-1"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="postPlasmawarndisease" /></td>
			
		</tr>	
		<tr class="warninglabel" style="display:none;">
		
			<td align="" style="padding-left:3%;">Not Evaluated For Infectious Substances</td>
			<td><input type="hidden" name="postPlasmainfectioussubst_id" id="postPlasmainfectioussubst_id" value="0"><input type="radio" id="postPlasmainfectioussubst_yes" name="postPlasmainfectioussubst" value="1"/>Yes&nbsp;<input type="radio" id="postPlasmainfectioussubst_no" name="postPlasmainfectioussubst" value="0"/>No&nbsp;<input type="radio" id="postPlasmainfectioussubst_na" name="postPlasmainfectioussubst" value="-1"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="postPlasmainfectioussubst" /></td>
			
		</tr>	
		<tr class="warninglabel" style="display:none;">
		
			<td align=""style="padding-left:3%;">Biohazard</td>
			<td><input type="hidden" name="postPlasmabiohazard_id" id="postPlasmabiohazard_id" value="0"><input type="radio" id="postPlasmabiohazard_yes" name="postPlasmabiohazard" value="1" />Yes&nbsp;<input type="radio" id="postPlasmabiohazard_no" name="postPlasmabiohazard" value="0"/>No&nbsp;<input type="radio" id="postPlasmabiohazard_na" name="postPlasmabiohazard" value="-1" />N/A<input type="hidden" name="questionsCode" id="questionsCode" value="postPlasmabiohazard" /></td>
			
		</tr>	
		<tr>
			<td>AUTO only Label Attached/Affixed</td>
			<td><input type="hidden" name="postPlasmaautolblatt_id" id="postPlasmaautolblatt_id" value="0"><input type="radio" id="postPlasmaautolblatt_yes" name="postPlasmaautolblatt" value="1"/>Yes&nbsp;<input type="radio" id="postPlasmaautolblatt_no" name="postPlasmaautolblatt" value="0" />No&nbsp;<input type="radio" id="postPlasmaautolblatt_na" name="postPlasmaautolblatt" value="-1"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="postPlasmaautolblatt" /></td>
			
		</tr>
	<!-- </table> -->
	<!-- <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%"> -->
		
		<tr>
			<td>Comments</td>
			<td><input type="hidden" name="postplasmaquestionsComments_id" id="postplasmaquestionsComments_id" value="0"><textarea id="postplasmaquestionsComments" name="postplasmaquestionsComments" rows="4" cols="50" > </textarea><input type="hidden" name="questionsCode" id="questionsCode" value="postplasmaquestionsComments" /></td>
			
		</tr>
	</table> 
</form>
<div align="right" style="float:right;">
		<form>
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td><span><input type="password" style="width:80px;" size="5" value="" id="plasmaeSignLabelVerification" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
					<input type="button" value="Save" onclick="verifyeSign('plasmaeSignLabelVerification','popup')"/>
					<input type="button" value="Cancel" onclick="closePlasmalabelVerification()"/></span></td>
				</tr>
			</table>
		</form>
	</div>  
 
