<%@ include file="common/includes.jsp" %>
<!-- <script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script> -->
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script type="text/javascript" src="js/barcodeScanBox.js"></script>
<script type="text/javascript" src="js/dataSearch.js"></script>



<script language="javascript">

function savePages(modeType){
    if(modeType!="next"){
        $("#completedFlag").val("0");
    }else{
    	$("#completedFlag").val("1");
    }
    try{
        if(saveBloodBank()){
            return true;
        }
    }catch(e){
        alert("exception " + e);
    }
}

function showBloodBankPopup(){
	showPopUp("open","bloodBankProducts","BloodBank Product Details","400","350");
}
function closePopup(){
    try{
    	showPopUp("close","bloodBankProducts","BloodBank Product Details","400","350");
      }catch(err){
        alert("err" + err);
    }
}
function getScanData(event,obj){
    if(event.which==13){
      getdata(event);
  }
}

var criteriaValue; 

function getdata(e){
   var searchUrl = "checkBloodBankData";
    var bloodSearch =  $("#conformProductSearch").val();
    if((e.which==13 && bloodSearch!="")){
    	var result = jsonDataCall(searchUrl,"bloodProducts="+bloodSearch);
        var listLength=result.bloodBankList.length;
        if(listLength<=0){
        	var selectedProduct = $("#bloodProducts").val();
        	var index = selectedProduct.indexOf($("#conformProductSearch").val());
        	if(index>=-1){
        		 if(selectedProduct == "," ){
        			 selectedProduct+=$("#conformProductSearch").val();
        			 var slicedVal = selectedProduct.substring(1);
        			 $("#bloodProducts").val(slicedVal);
        			  criteriaValue = "'"+slicedVal+"'";
        			  //criteriaValue = slicedVal;
        		 }else{
        			 selectedProduct+=","+$("#conformProductSearch").val()+"";
                     $("#bloodProducts").val(selectedProduct);
                     var str = selectedProduct.lastIndexOf(",");
                     var slicedVal = selectedProduct.substring(str,selectedProduct.length);
                      criteriaValue+= ",'"+slicedVal.substring(1)+"'";
                      //criteriaValue+= ","+slicedVal.substring(1);
        		 	}
        	}
        	constructBloodBankTable(true,'bloodBankProductTable');
        }else{
        	 $(result.bloodBankList).each(function(i,v){
        		if(v[7]=="0"){
        			$("#bloodbank").val(v[0]);
        			
        			if(v[5]!=null && v[5]==1){
 			 			$("#productQuarantined1").trigger("click");
 			 			$("#productQuarantined1").addClass("on");
 			 		}else if(v[5]!=null && v[5]==0){
 			 			$("#productQuarantined0").trigger("click");
 			 			$("#productQuarantined0").addClass("on");
 			 		}
       			  	
        			var bloodSignOfDate = new Date(v[3]);
                    var bloodSignOfTime = (bloodSignOfDate.getHours()<10?'0':'') + bloodSignOfDate.getHours()+":"+(bloodSignOfDate.getMinutes()<10?'0':'') + bloodSignOfDate.getMinutes();
                     
                    $("#tmpSignOffTime").val(bloodSignOfTime);
                    $("#bloodsignOffTime").val(v[3]);
        			$("#bloodsignOffDate").val(converHDate(v[2])); 
        			$("#bloodProducts").val(v[6]);
        			$("#storageTemp").val(v[4]);
        			constructBloodBankTable(true,'bloodBankProductTable');
        		}else if(v[7]=="1"){
        			viewBloodBankList(result);
        		}
        	}); 
        }
    }
}

function removeProduct(){
	   var deleteIds = [];
	   $("#bloodBankProductTable #productId:checked").each(function(i){
	        deleteIds.push($(this).val());
	        deleteProduct($(this).val());
	   });
}

function deleteProduct(deleteId){
   var temp=$("#bloodProducts").val();
   var splitArray = temp.split(",");
   var selectedIndex = splitArray.indexOf(deleteId);
   if(selectedIndex!=-1){
        splitArray.splice($.inArray(deleteId,splitArray),1);
   }
   $("#bloodProducts").val(splitArray);
   //?Muthu criteriaValue ="'"+splitArray+"'";
   criteriaValue = splitArray;
   constructBloodBankTable(true,'bloodBankProductTable');
   if(criteriaValue == "''"){
	   $("#bloodProducts").val(",");
   }
}
function viewBloodBankList(response){
    var bloodBankIds;
    var bloodSignTime;
    var bloodSignDate;
    var storageTemp;
    var productQuarantine;
    $(response.bloodBankList).each(function(i,v) {
        for(j=0;j<v.length;j++){
        	bloodBankIds = v[6];
            bloodSignTime = v[3];
                if(v[3]!=null){
                    var tmpDate = new Date(v[3]);
                    bloodSignTime = (tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes();
                }else{
                	bloodSignTime = "";
                }
            bloodSignDate = v[2];
                if(v[2]!=null){
                	bloodSignDate = converHDate(v[2]);
                        }else{
                        	bloodSignDate = "";
                }
            storageTemp = v[4];
            productQuarantine = v[5];
	            if(productQuarantine=='1'){
	            	productQuarantine = 'Yes';  
	            }else{
	            	productQuarantine = 'No';  
	            }
        }
    });
  var popTable  = "<table cellpadding='10' border='1' align='center' cellspacing='0'>"+
                         "<tr><td>BloodBank Product ID</td><td>"+bloodBankIds+"</tr>"+
                         "<tr><td>Sign-Off Time</td><td>"+bloodSignTime+"</td></tr>"+
                         "<tr><td>Sign-Off Date</td><td>"+bloodSignDate+"</td></tr>"+
                         "<tr><td>Storage Temp</td><td>"+storageTemp+"</td></tr>"+
                         "<tr><td>Product Quarantined</td><td>"+productQuarantine+"</td></tr>"+
                    "</table>"+
                      "<div align='right' style='float:right;'>"+
                          "<table cellpadding='0' cellspacing='0' class='' align='' border='0'>"+
                             "<tr><td><input type='button' onclick='closePopup();' value='Cancel'/></td></tr>"+
                        "</table>"+
                    "</div>";
      	creatTable(popTable);
        $.uniform.restore('select');
        $('select').uniform();
}
function creatTable(popTable){
    showBloodBankPopup();
    $("#bloodBankProductForm").html(popTable);
}
function constructBloodBankTable(flag,tableId){
    var criteria="";           
//  criteria = " SPEC_ID ="+criteriaValue+")";
//	criteria = "and pp.fk_product in (select  pk_specimen from er_specimen where instr(','||tr.trn_products||',',','||"+criteriaValue+"||',')>0)";

 var bloodBank_serverParam = function ( aoData ) {

													aoData.push( { "name": "application", "value": "stafa"});
													aoData.push( { "name": "module", "value": "bloodBank_Product"});	
													//?BB aoData.push( { "name": "criteria", "value": criteria});
													aoData.push( { "name": "param_count", "value":"1"});
													aoData.push( { "name": "param_1_name", "value":":SPEC_ID:"});
													aoData.push( { "name": "param_1_value", "value":criteriaValue});
													
													aoData.push( {"name": "col_1_name", "value": "lower(nvl((select SPEC_ID from er_specimen where pk_specimen =pp.fk_product),' '))" } );
													aoData.push( { "name": "col_1_column", "value": "lower(proID)"});
													
													aoData.push( { "name": "col_2_name", "value": "lower(nvl( (select nvl(PERSON_LNAME, ' ' ) || ' ' ||nvl(PERSON_FNAME, ' ' ) from person where pk_person=d.fk_receipent),' '))"});
                                                    aoData.push( { "name": "col_2_column", "value": "lower(recip_name)"});
                                                    
                                                    aoData.push( {"name": "col_3_name", "value": "lower(nvl((select nvl(p.person_mrn,' ') from person where pk_person=d.fk_receipent),' '))" } );
                                                    aoData.push( { "name": "col_3_column", "value": "lower(recip_id)"});
                                                    
                                                    aoData.push( {"name": "col_4_name", "value": "lower(nvl(p.PERSON_LNAME, ' ' ) || ' ' ||nvl(p.PERSON_FNAME, ' ' ))" } );
                                                    aoData.push( { "name": "col_4_column", "value": "lower(donorname)"});
                                                    
                                                    aoData.push( {"name": "col_5_name", "value": "lower(nvl(p.person_mrn,' '))" } );
                                                    aoData.push( { "name": "col_5_column", "value": "lower(donorMRN)"});
                                                    
                                                    aoData.push( {"name": "col_6_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst =d.fk_codelstdonortype ),' '))" } );
                                                    aoData.push( { "name": "col_6_column", "value": "lower(donortype)"});
                                                    
                                                    aoData.push( {"name": "col_7_name", "value": "lower(nvl((select CODELST_DESC from er_codelst where PK_CODELST=d.FK_CODELSTPRODUCTTYPE),' '))" } );
                                                    aoData.push( { "name": "col_7_column", "value": "lower(pType)"});
                                                    
                                                    aoData.push( {"name": "col_8_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst =tr.fk_product_destination),' '))" } );
                                                    aoData.push( { "name": "col_8_column", "value": "lower(destination)"});
                                                    
                                                    aoData.push( {"name": "col_9_name", "value": "lower(nvl((select CODELST_DESC from er_codelst where PK_CODELST=pp.FK_CODELSTDONORSTATUS),' '))" } );
                                                    aoData.push( { "name": "col_9_column", "value": "lower(status)"});
                                                    
												};
 var bloodBank_ColManager =function (){
									     $("#bloodBankProductColumn").html($('#bloodBankProductTable_wrapper .ColVis'));
										};
 var bloodBank_aoColumn = [{"bSortable": false},
			               null,
			               null,
			               null,
			               null,
			               null,
			               null,
			               null,
			               null,
			               null
		               	];												
 var bloodBank_aoColDef = [
                          {	"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
                     				 return "<input type='hidden'id='transport' name='transport' value='"+source[1]+"'/>";
                    	 	 }},
	                       {	"sTitle":'<s:text name="stafa.label.productid"/>',
	                         	"aTargets": [ 1], "mDataProp": function ( source, type, val ) {
	                                 return source[9];
	                         }},
	                       {	"sTitle":'<s:text name="stafa.label.recipientname"/>',
	                         	"aTargets": [ 2], "mDataProp": function ( source, type, val ) {
	                         		 return source[3];
	                        }},
	                       {	"sTitle":'<s:text name="stafa.label.recipientmrn"/>',
	                         	"aTargets": [ 3], "mDataProp": function ( source, type, val ) {
	                         		 return source[4];
	                        }},
	                       {	"sTitle":'<s:text name="stafa.label.donorname"/>',
	                         	"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
	                         		 return source[5];
	                        }},
	                       {	"sTitle":'<s:text name="stafa.label.donorid"/>',
	                         	"aTargets": [ 5], "mDataProp": function ( source, type, val ) {
	                         		 return source[6];
	                        }},
	                       {	"sTitle":'Donor Type',
	                         	"aTargets": [ 6], "mDataProp": function ( source, type, val ) {
	                         		 return source[10];
	                        }},
	                      
	                       {	"sTitle":'<s:text name="stafa.label.producttype"/>',
	                         	"aTargets": [ 7], "mDataProp": function ( source, type, val ) {
	                         		 return source[7];
	                        }},
	                         
	                       {	"sTitle":'Destination',
	                         	"aTargets": [ 8], "mDataProp": function ( source, type, val ) {
	                         		 return source[11];
	                        }},
	                        {	"sTitle":'<s:text name="stafa.label.productstatus"/>',
	                         	"aTargets": [ 9], "mDataProp": function ( source, type, val ) {
	                         		 return source[8];
	                        }}
	                      
	                     ];
 			constructTable(flag,tableId,bloodBank_ColManager,bloodBank_serverParam,bloodBank_aoColDef,bloodBank_aoColumn);
        }
$(document).ready(function(){
	hide_slidewidgets();
	hide_slidecontrol();
	show_innernorth();
	$('#inner_center').scrollTop(0);
	$("#tmpSignOffTime").timepicker({});
	$("#tempAphSignOffTime").timepicker({});
	    try{
	        showPopUp("close","bloodBankProducts","BloodBank Product Details","400","350");
	    }catch(err){
	        alert("err " + err);
	    }
	    var samTable = $('#bloodBankProductTable').dataTable({
	        "sPaginationType": "full_numbers",
	        "aoColumns":[{"bSortable": false},
	                       null,
	                       null,
	                       null,
	                       null,
	                       null,
	                       null,
	                       null,
	                       null,
	                       null
	                       ],
	            "sDom": 'C<"clear">Rlfrtip',
	            "oColVis": {
	                "aiExclude": [ 0 ],
	                "buttonText": "&nbsp;",
	                "bRestore": true,
	                "sAlign": "left"
	            },
	            "fnInitComplete": function () {
	                $("#bloodBankProductColumn").html($('#bloodBankProductTable_wrapper .ColVis'));
	            }
	    });
	      
	   $( "#bloodsignOffDate" ).datepicker({
	                                        dateFormat: 'M dd, yy',
	                                        changeMonth: true,
	                                        changeYear: true,
	                                        yearRange: "c-50:c+45"
	                                    });
	$("select").uniform();
	
	
    
    var param ="{module:'APHERESIS',page:'BLOODBANK'}";
	markValidation(param);
});
function setSignOffTime(){
    $('#bloodsignOffTime').val($('#bloodsignOffDate').val() +" "+ $('#tmpSignOffTime').val());
}
function setvalues(cBoxId,textId){
    var s=$('#'+cBoxId).attr('checked');
    if(s=='checked'){
        $('#'+textId).val("N/A");
    }else {
        $('#'+textId).val("");
    }
}
function saveBloodBank(){
    var url='saveBloodBank';
    var response = ajaxCall(url,"bloodBankForm");
    //alert("Data saved succesfully");
    stafaAlert('Data saved Successfully','');
    $("#bloodbank").val(response.bloodbank);
    //$("#main").html(response);
    return true;
}
</script>

<div class="cleaner"></div>

<form action="#" id="bloodBankForm">
<s:hidden id="bloodbank" name="bloodbank" value="0"/>
<s:hidden id="completedFlag" name="completedFlag"/>
<s:hidden id="bloodProducts" name="bloodProducts" value=","/>
<div class="column">
	<div  class="portlet">
	<div class="portlet-header"><s:text name="stafa.label.producttable"/></div><br>
		<div class="portlet-content">
			<div id="button_wrapper">
	            <div style="margin-left:40%;">
       				<input class="scanboxSearch barcodeSearch"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;" onkeyup="performScanData(event,this);" />
					<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" onfocus="changeType(this)"/>
	 			</div>
            </div>
			<div id="bloodBankDiv" style="overflow-x:auto;overflow-y:hidden;">
			<table cellpadding="0" cellspacing="0" border="0" id="bloodBankProductTable" class="display">
				<thead>
					<tr>
						<th width="4%" id="bloodBankProductColumn"></th>
						<th><s:text name="stafa.label.productid"/></th>
						<th><s:text name="stafa.label.recipientname"/></th>
						<th><s:text name="stafa.label.recipientmrn"/></th>
						<th><s:text name="stafa.label.donorname"/></th>
						<th><s:text name="stafa.label.donorid"/></th>
						<th>Donor Type</th>
						<th><s:text name="stafa.label.producttype"/></th>
						<th>Destination</th>
						<th><s:text name="stafa.label.productstatus"/></th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>

<div class="cleaner"></div>

<div class="column">
	<div  class="portlet">
	<div class="portlet-header">Product Receipt</div><br>
		<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Product Quarantined</td>
					<td class="tablewapper1"><s:radio name="productQuarantined" list="#{'1':'Yes','0':'No'}"/></td>
					<td class="tablewapper"><s:text name="stafa.label.signofftime"/></td>
					<td class="tablewapper1"><s:hidden  id="bloodsignOffTime" name="bloodsignOffTime" Class="" /><s:textfield id="tmpSignOffTime" name="tmpSignOffTime" Class="" onchange="setSignOffTime();"/></td>
				</tr>
				<tr>
					<td class="tablewapper"><s:text name="stafa.label.signoffdate"/></td>
					<td class="tablewapper1"><input type="text" id="bloodsignOffDate" name='bloodsignOffDate' class="dateEntry"/></td>
					<td class="tablewapper">Storage Temp(C)</td>
					<td class="tablewapper1"><input type="text" id="storageTemp" name="storageTemp" class="">&nbsp;<input type="checkbox" id="storageTempNA" name="storageTempNA"  onclick="setvalues('storageTempNA','storageTemp')"/>N/A</td>
                </tr>
			</table>
		</div>
	</div>
</div>
</form>
<form onsubmit="return avoidFormSubmitting()">
<div class="cleaner"></div>
	
<div align="left" style="float:right;">
	<table width="100%" cellpadding="0" cellspacing="0" class="" align="" border="0">
		<tr>
		<td><span><label>Next:</label>
		<select name="nextOption" id="nextOption" class=""><option value="loadBloodBank.action">Next Product</option><option value="logout.action">Logout</option></select>
		<input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
		<input type="button" value="Next" onclick="verifyeSign('eSignNext','next')"/></span></td>
		</tr>
	</table>
</div>
</form>
<div class="cleaner"></div>
<div id="bloodBankProducts" class="model" title="addNew" style="display: none">
    <form action="#" id="bloodBankProductForm">
    </form>
</div>
<script>

$(function() {
  
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
   



    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

});
</script>