<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div class="column">
		<div class="portlet">
			<div class="portlet-header ">
			<s:if test="%{#preCollection=='true'}">
				<s:text name = "stafa.label.precollectchecklst"/>
			</s:if>
			<s:else>
			    <s:text name = "stafa.label.visitchecklist"/>
			</s:else>
			</div>	
		<div class="portlet-content">				
				<br>
				<table>
						<tr>
							<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="addVisitCheckListRows('visitCheckListTable');"/>&nbsp;&nbsp;<label  class="cursor" onclick="addVisitCheckListRows('visitCheckListTable');"><b>Add </b></label>&nbsp;</div></td>
							<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteVisitCheckList();" />&nbsp;&nbsp;<label class="cursor" onclick="deleteVisitCheckList();"><b>Delete</b></label>&nbsp;</div></td> 
				 		    <td style="width:20%"><div><img src = "images/icons/edit.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="editVisitCheckList('visitCheckListTable');"/>&nbsp;&nbsp;<label class="cursor" onclick="editVisitCheckList('visitCheckListTable');"><b>Edit</b></label>&nbsp;</div></td>
						</tr>
					</table>
				<s:form id="visitCheckListForm" name="visitCheckListForm">				
				<div class="tableOverflowDiv">
				<table cellpadding="0" cellspacing="0" border="0" id="visitCheckListTable" class="display" width="100%">
					<thead>
						<tr>
							<th width="4%" id="visitCheckListColumn"></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>							
						</tr>
					</thead>
				</table>
				</div>
				</s:form>
				</div>
		</div>	
		<div class="cleaner"></div>	
	</div>
	
	<span class="hidden">
		<s:if test="%{#preCollection=='true'}">
			<s:select id="visitCheckListItemsId" name="visitCheckListItems1" list="preCollectionChecklst"  headerKey="-1" headerValue="Select"/>
		</s:if>
		<s:else>	
			<s:select id="visitCheckListItemsId" name="visitCheckListItems1" list="visitCheckListItems"  headerKey="-1" headerValue="Select"/>
		</s:else>
			<s:select id="visitCheckListStatusId" name="visitCheckListStatus1" list="visitCheckListStatus" headerKey="-1" headerValue="Select"/>
	</span>
				
<script type="text/javascript">

var VisitCheckListTableHeader = {
		
		select:'<s:text name = "stafa.label.select"/>',
		items: '<s:text name = "stafa.label.item"/>',		
		date:'<s:text name = "stafa.label.date"/>',
		status: '<s:text name = "stafa.label.status"/>'
};

var VisitCheckListParamObj ={
		entityid:'<s:property value="recipient"/>',
		entityType:'<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_RECIPIENT"/>',
		txid:'<s:property value="transplantId"/>',	
		visitId:'<s:property value="visitId"/>'
};

function constructVisitCheckListTable(flag, entity, entityType, visitId, tableId, columnId, tableHead) {
	/*var criteria = "";
	if(entity!=null && entity!=""){
		criteria = entity;
	}*/
	var documentColumn = [ { "bSortable": false},
		                	null,
		                	null,
		                	null			                	
			             ];
												
	var documentServerParams = function ( aoData ) {
									aoData.push( { "name": "application", "value": "stafa"});
									aoData.push( { "name": "module", "value": "CheckList"});
									//aoData.push( { "name": "criteria", "value": criteria});
									
									aoData.push( { "name": "param_count", "value":"3"});
									
									aoData.push( { "name": "param_1_name", "value":":ENTITY:"});
									aoData.push( { "name": "param_1_value", "value":entity});

									aoData.push( { "name": "param_2_name", "value":":ENTITYTYPE:"});
									aoData.push( { "name": "param_2_value", "value":"'"+entityType+"'"});
									
									aoData.push( { "name": "param_3_name", "value":":VISIT:"});
									aoData.push( { "name": "param_3_value", "value":visitId});
									
									/*aoData.push( {"name": "col_1_name", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_DOCUMENTTYPE))" } );
									aoData.push( { "name": "col_1_column", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_DOCUMENTTYPE))"});
									
									aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
									aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});*/
												
									
				};
				var documentaoColDef = [
						                  {		  //"sTitle":'<s:text name = "stafa.label.select"/>',//"Select",
						                	  	  "aTargets": [0], "mDataProp": function ( source, type, val ) {
					                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
					                	 			return "<input type='checkbox' id='visitCheckId' name='visitCheckId' value='"+source[0] +"' />";
					                	 			//return "";
					                	     }},
						                  { 	 "sTitle":tableHead.items,
					                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
						                	 		return source[1];
						                	 }},
						                	 { 	 "sTitle":tableHead.date,
						                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
												    return source[2];
											}},
										  { 	 "sTitle":tableHead.status,
						                		 "aTargets": [3], "mDataProp": function ( source, type, val ) { 
												    return source[3];
											}}
										];
				
	var documentColManager = function () {
											$("#"+columnId).html($("#"+tableId+"_wrapper .ColVis"));
										};
										
	constructTable(flag,tableId,documentColManager,documentServerParams,documentaoColDef,documentColumn);
} 

/* add rows */
var attachVisitCheckListRowId ='W1';
function addVisitCheckListRows(table){
	 if(checkModuleRights(STAFA_RECIPIENTTRACKERS,APPMODE_ADD)){
		 var items = $("#visitCheckListItemsId").html();
		 var status = $("#visitCheckListStatusId").html();
	     var newRow ="<tr>"+	     
	     "<td><input type='checkbox' id='visitCheckId' name='visitCheckId' value='0' /></td>"+
	     "<td><div><select id='visitCheckListItemId' name='visitCheckListItem' style='width:200px;'>"+items+"</select></div></td>"+	    
	   	 "<td><input type='text' id='date"+attachVisitCheckListRowId+"' class='datePic dateEntry' name='date"+attachVisitCheckListRowId+"' style='width: 130px;'/></td>"+
	   	 "<td><select id='visitCheckListStatusId' name='visitCheckListStatus"+attachVisitCheckListRowId+"' style='width:100px;'>"+status+"</select></td>"+
	     "</tr>";
	     $("#"+table+" tbody").prepend(newRow);	 

		 $("#date"+attachVisitCheckListRowId).datepicker({dateFormat: 'M dd, yy',changeMonth: true, changeYear: true, yearRange: '1900:' + new Date().getFullYear()});

		 $.uniform.restore('select');
		 $('select').uniform();		
		 attachVisitCheckListRowId = attachVisitCheckListRowId+1;
		 /* var param ="{module:'APHERESIS',page:'DONOR_DETAILS'}";
			markValidation(param);*/
	 }
}
var visitCheckListEditId ='VCL';
function editVisitCheckList(table){
	var items;
	var status;
	var checkedStatus;
	var item;
	var state;
	var date;
	
	
		items = $("#visitCheckListItemsId").html()
	
		status = $("#visitCheckListStatusId").html()
	
	
	$("#"+table+" tbody tr").each(function (row){
	 	 weight ="";
	 	 unit ="";
	 	 date ="";
	 	 checkedStatus=false;
			$(this).find("td").each(function (col){
			if(col ==0){			
				if($(this).find("#visitCheckId").is(':checked')){
					checkedStatus=true;
				}else 
					return false;
			}
			if(col ==1){
				item = $(this).html();
				if(checkedStatus && item.indexOf('select')<0){					
						 $(this).html('');					
						 $(this).html("<select id='visitCheckListItemId' name='visitCheckListItem' style='width:200px;'>"+items+"</select>");
						 $.uniform.restore('select');
						 $('select').uniform();
				}else {return false;}
			}
			if(col ==2){
				date = $(this).html();
				if(checkedStatus && date.indexOf('input')<0){					
					$(this).html('');
					$(this).html("<input type='text' id='date"+visitCheckListEditId+"' class='datePic dateEntry' name='' value='"+date+"' style='width: 130px;'/>");
					$("#date"+visitCheckListEditId).datepicker({dateFormat: 'M dd, yy',changeMonth: true, changeYear: true, yearRange: '1900:' + new Date().getFullYear()});
					visitCheckListEditId +=1;
				}else {return false;}				
			}
			if(col ==3){
				state = $(this).html();
				if(checkedStatus && state.indexOf('select')<0){					
					 $(this).html('');
					 $(this).html("<select id='visitCheckListStatusId' name='' style='width:100px;'>"+status+"</select>");
					//$(this).find("#unitid").attr('selected', unit);
					 $.uniform.restore('select');
					 $('select').uniform();
				}else{ return false;}
			}
			
		});
			
		
	 
	 });
	
}


function deleteVisitCheckList(){
	 if(checkModuleRights(STAFA_RECIPIENTTRACKERS, APPMODE_DELETE)){  	
		 var deletedIds="";
			try{
				
			$("#visitCheckListTable tbody tr").each(function (row){
				 if($(this).find("#visitCheckId").is(":checked")){
					deletedIds += $(this).find("#visitCheckId").val() + ",";
				}
			 });			
		
			  if(deletedIds.length >0){
				 var yes=confirm(confirm_deleteMsg);
				 if(!yes){
					return;
				 }
				  deletedIds = deletedIds.substr(0,deletedIds.length-1);
				  jsonData = "{deletedIds:'"+deletedIds+"',domainName:'DOMAIN_CHECKLIST',domainKey:'CHECKLIST_ID'}";
				  url="commonDelete";
			     
			     response = jsonDataCall(url,"jsonData="+jsonData);
			 
			     constructVisitCheckListTable(false, VisitCheckListParamObj.entityid, VisitCheckListParamObj.entityType, VisitCheckListParamObj.visitId, 'visitCheckListTable', 'visitCheckListColumn', VisitCheckListTableHeader);
			 	
			  }
			}catch(err){
				alert(" error " + err);
			}
	  }
}


function saveCheckListData( entityid, entityType, tx, visit, tableid){
	var checkListData ="";
	$("#"+tableid+" tbody tr").each(function (row){
		
	 	var checkListStatus="";
	 	var checkListDate="";
	 	var checkListItems="";	 
	 	var checkListId="";
	 	
	 
		$(this).find("td").each(function (col){
			
			if(col ==0){
				checkListId = $(this).find("#visitCheckId").val();
			}			
			if(col ==1){
				checkListItems = $(this).find("#visitCheckListItemId").val();
			}
			if(col ==2){
				checkListDate = $(this).find(".dateEntry").val();
			}
			if(col ==3){
				checkListStatus = $(this).find("#visitCheckListStatusId").val();
			}
			
		});
		if(checkListId!==undefined && checkListId!=='' && checkListId!=null){
			checkListData +="{pkVisitChecklist:'"+checkListId+"',entityId:'"+entityid+"',entityType:'"+entityType+"',checklistItem:'"+checkListItems+"',checklistDate:'"+checkListDate+"',checklistStatus:'"+checkListStatus+"',fkTxWorkflow:'"+tx+"',fkTxVisit:'"+visit+"'},";
		}
	 });

	if( checkListData.length >0 ){
		checkListData = checkListData.substring(0,(checkListData.length-1));
		var url="saveCheckList";
		response = jsonDataCall(url,"jsonData={checkListData:["+checkListData+"]}");						
		$('.progress-indicator').css( 'display', 'none' );
		
	}
	return true;
	
}

function saveCheckList(){
	
 saveCheckListData(VisitCheckListParamObj.entityid, VisitCheckListParamObj.entityType, VisitCheckListParamObj.txid, VisitCheckListParamObj.visitId, "visitCheckListTable");
}
constructVisitCheckListTable(false, VisitCheckListParamObj.entityid, VisitCheckListParamObj.entityType, VisitCheckListParamObj.visitId, 'visitCheckListTable', 'visitCheckListColumn', VisitCheckListTableHeader);


</script>