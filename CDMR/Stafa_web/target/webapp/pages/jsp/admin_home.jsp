<%@ include file="common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script type="text/javascript">
$(document).ready(function(){
	hide_slidecontrol();
	hide_slidewidgets();
	var oTable=$("#inboxtable").dataTable({"bJQueryUI": true});
	
	$("select").uniform();

});

</script>
 <style type="text/css">
	.divwrapper {
				/* border:1px solid black; */
				/* float:left; */
				margin:0%;
				min-height:100px;
				 padding:1%; 
				width:97.5%;
				height:530px;
	}
	.commondivclass{
			float:left;
			margin:0%;
			/* min-height:100px; */
			padding:0%;
			width:50%;
			height:100px;
	
	}
	.leftdiv{
	    	/* border:1px solid black; */
			float:left;
			margin:1%;
			min-height:15em; 
			padding:1%;
			width:16%;
			height:auto;
			background-color:#ffffff;
		
		}
		.rightdiv{
					/* border:1px solid black; */
					float:left;
					margin:1%;
					 min-height:100px; 
					padding:1%;
					width:70%;
					height:auto;
					background-color:white;
		
		}
			.cursoroption{
				cursor:pointer;
			
			}
		.upperdiv{
						/* border:1px solid black; */
						float:left;
						margin:1%;
						 min-height:60px; 
						padding:1%;
						width:94%;
						height:70px;
						background-color:#EEEEEE;
						border-radius:3px;
		
		}
</style>

	<div id="firstdiv" class="upperdiv">
				<table cellpadding="0" cellspacing="2" border="0" width="55%">
					<tbody >
						<tr>
							<td width="15%" style="text-align:center;"><img src = "images/icons/dashboard.png" style="width:46px;height:36px; text-align:center;" /></td>
							<td width="15%" style="text-align:center;"><img src = "images/icons/procedure.png" style="width:46px;height:36px;text-align:center;" /></td>
							<td width="15%" style="text-align:center;"><img src = "images/icons/report.png" style="width:46px;height:36px;text-align:center;" /></td>
							<td width="15%" style="text-align:center;"><img src = "images/icons/usergroup.png" style="width:46px;height:36px;text-align:center;" /></td>
						</tr>
						<tr>
							<td width="15%" style="text-align:center;"><b>Dashboard</b></td>
							<td width="15%" style="text-align:center;"><b>Procedure</b></td>
							<td width="15%" style="text-align:center;"><b>Reports</b></td>
							<td width="15%" style="text-align:center;"><b>User</b></td>
						</tr>
					
					</tbody>
				
				</table>
	</div>
 <div class="divwrapper">
<div class="cleaner"></div>
<div class="commondivclass">
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header "><div><img src = "images/icons/dashboard.png" style="width:16px;height:16px;"/>&nbsp;&nbsp;<label><b>Dash Board</b></label>&nbsp;</div></div>
						<div class="portlet-content">
							<div class="leftdiv">
						
							<ul class="ulstyle">
							
							<li class="listyle"><img src = "images/icons/bloodpacknov.png"  style="width:20px;height:28px;" class="cursoroption"/></li>
							<li class="listyle"><b>Product</b> </li>
							
							<li class="listyle"><img src = "images/icons/userconfig.png"  style="width:20px;height:28px;" class="cursoroption"/></li>
							<li class="listyle"><b>Donors</b></li>
							
							<li class="listyle"><img src = "images/icons/nures.png" onclick="transfer()"  style="width:20px;height:28px;" class="cursoroption"/></li>
							<li class="listyle"><b>Nurses</b></li>
							
							</ul>
						
							</div>
							<span style="padding:1%; margin:0%;"><b>Monitoring</b></span>
							<div class="rightdiv"><img src = "images/others/barchart.png"   style="height:230px;width:458px" class="cursoroption"/></div>
						</div>
				</div>
		</div>
</div> 
		
 
  <div class=" commondivclass column">		
		<div  class="portlet">
			<div class="portlet-header "><div><img src = "images/icons/email.png" style="width:20px;height:20px;"/>&nbsp;&nbsp;<label><b>Inbox</b></label>&nbsp;</div></div><br>
		<div class="portlet-content">
		<table>
		<tr>
			<td style="width:20%"><div class="cursoroption"><img src = "images/icons/no.png" style="width:16px;height:16px;" class="" id ="" onclick="deleteRows();"/>&nbsp;&nbsp;<label class=""  onclick="deleteRows();"><b>Delete</b></label>&nbsp;</div></td>	
		</tr>
	</table>
				<table cellpadding="0" cellspacing="0" border="0" id="inboxtable" class="display">
		<thead>
			<tr>
				<th >Select</th>
				<th >Name</th>
				<th >Subject</th>
				<th >Date/Time</th>				
			</tr>
		</thead>
		<tbody>
			<tr style="cursor:pointer;">
				<td><input type="checkbox" name="cbox"/></td>
				<td> ! Claire Bayer</td>
				<td>Alert! Spill</td>
				<td>10.22am</td>
			</tr>
			<tr style="cursor:pointer;">
				<td><input type="checkbox" name="cbox"/></td>
				<td> ! Claire Bayer</td>
				<td>Alert! Unexpected Temp Change</td>
				<td>10.12</td>
			</tr>
			<tr  style="cursor:pointer;">
				<td><input type="checkbox" name="cbox"/></td>
				<td>Mark Raul</td>
				<td>Alert! Bag Dropped</td>
				<td>oct 24 2012</td>
			</tr>
		</tbody>
		</table>			
			</div>
		</div>
	</div>
		
</div> 
<script>
$(function() {
	
	
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" );
	
		/* $( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	}); */

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});
</script>