<div id="advanceSearchnew" 	style=" display: none;">
	<table id="advanceSearchTable">
		<tbody><tr>
			<td style="padding-right:40px; padding-top: 20px;">Choose Filter:</td>
			<td style="padding-top: 20px"><select id="searchfilter"
				style="width: 180px; height: 30px; margin-left:10px;"></select></td></tr>
			<tr>
				<td style="padding-top: 20px">Enter value:</td>
				<td style="padding-top: 20px"><input id="matchvalue" type="text" disabled='disabled'></td>
				<td style="padding-top: 20px;padding-left:10px"><input type="button" value="Add" class="advancesearch_button"
				onclick="showFilters();"></td>
			</tr>
		</tbody>
	</table>
<hr></hr>
	<table>
		<tbody><tr>
			<td style="float: left">Selected Filters:</td>
			<td style="padding-left: 30px"><a href="#"
				onclick="clearFilters();">Clear all filters</a></td>
		</tr>
		</tbody>
	</table>
	<ul id="showfilters_area" style="width: auto; height:auto; overflow: hidden;padding-left:8px;">
	<!--Filters will be added here to show on popup	-->
	</ul>
	<div id="show_initialy">
   	<input id="savefilterbutton" type="button" value="Save Filter Set" class="save_filter_button"
	style="margin-left: 20%; cursor: pointer;" onclick="ClicktoSaveandApply();" /> 
	<input id="applyfilterbutton" type="button" value="Apply" class="apply_button"
	style="margin-left: 3%;cursor: pointer;" onclick="applyFilters();" />
	</div>
	<div id="showtosavefilter" style="display: none">
  	<input id="entertosave" type="text" name="savevalue" placeholder="Save"
	style="width: 110px; height: 20px; margin-left: 5%;" /> 
	<input type="button" value="Save & Apply" class="apply_button"
	style="margin-left: 3%; width:100px;cursor: pointer;" onclick="SaveNamedFilter();" id="save&apply"/>
   <input type="button" value="Cancel" class="cancel_button"
	style="margin-left: 3%;" onclick="ClicktoCancel();" />
	</div>
</div>
<div id="advanceSearcheditform" style="display: none;">
<table id="editadvanceSearchTable">
	<tbody>
		<tr>
			<td style="padding-right: 40px; padding-top: 20px">Choose
			Filter:</td>
			<td style="padding-top: 20px"><select id="searchfiltersave"
				style="width: 180px; height: 30px; margin-left: 10px;">
			</select></td>
		</tr>
		<tr>
			<td style="padding-top: 20px">Enter value:</td>
			<td style="padding-top: 20px"><input id="enterval" type="text"
				disabled='disabled'></td>
			<td style="padding-top: 20px"><input type="button" value="Add"
				class="advancesearch_button" onclick="showNewFilters();"></td>
		</tr>
	</tbody>
</table>
<hr></hr>
<table>
	<tbody>
		<tr>
			<td style="float: left">Selected Filters:</td>
			<td style="padding-left: 30px"><a href="#"
				onclick="clearFiltersandDelete();">Clear all filters</a></td>
		</tr>
	</tbody>
</table>
<ul id="savedfilterset_area"
	style="width: 80%; height: 45%; overflow: hidden;">
	<!--Saved filters will be populated here-->
</ul>
<table>
	<tbody>
		<tr>
			<td><input id="savedval" type="text" name="savedname"
				style="margin-left: 5%;" /></td>
			<td><input id="updatefilters" type="button" value="Save & Apply"
				class="apply_button" style="margin-left: 5%;cursor: pointer;"
				onclick="saveUpdatedFilters();" /></td>
			<td><input type="button" onclick="delFilter();" value="Delete"
				style="margin-left: 5%;" class="cancel_button" /></td>
			<td><input type="button" value="Cancel" class="cancel_button"
				style="margin-left: 5%;" onClick="cancelUpdate();" /></td>
		</tr>
	</tbody>
</table>
</div>

<div id="advanceSearchsave" style="display: none;">
<table>
	<tbody>
		<tr>
			<td style="padding-top: 40px"><select id="savedfilter"
				style="width:140px; height: 30px;">
			</select></td>
			<td style="padding-top: 40px;"><input type="button" style="cursor: pointer;"
				id="savefilter" value="Apply" onClick="applySelectedfilterName();"
				class="apply_button"></input></td>
		</tr>

		<tr>
			<td style="padding-top: 50px"><a href="#" id="editfilterset"
				onclick="editFilter();" style="padding-left: 45px;pointer-events: none;"><img
				style="cursor: auto; width: 16px; height: 16px;"
				src="images/cdmr/editfilter.png" />Edit</a></td>
			<td style="padding-top: 50px"><a id="delfiltername" href="#"
				onclick="delFilter();" style="padding-left: 5px; pointer-events: none;"><img
				style="cursor: auto; width: 16px; height: 16px;"
				src="images/cdmr/deletefilter.jpeg" />Delete</a></td>
		</tr>

	</tbody>
</table>
</div>

<div id="date_selectordialog" style="display: none;"><label
	style="padding-right: 6%">Filter by: </label> <input type="radio"
	value="exactoption" name="selectdate" /> <label
	style="padding-right: 6%">Exact</label> <input type="radio"
	value="rangeoption" name="selectdate" checked="checked" /> <label
	style="padding-right: 6%">Range</label> <input id="showdate"
	type="button" value="Add" class="advancesearch_button"
	onclick="addDate();" />
<hr></hr>
<table id="exactdate" style="display: none;">
	<tbody>
		<tr>
			<td><label>Date:</label><input id="exactdatetext" type="text"
				class="datepicker_AS" style="width: 120px;" /></td>
		</tr>
	</tbody>
</table>
<table id="rangedate">
	<tbody>
		<tr>
			<td><label>From:</label><input id="fromdate" type="text"
				class="datepicker_AS" style="width: 120px;" /></td>
			<td><label>To:</label><input id="todate" type="text"
				class="datepicker_AS" style="width: 120px;" /></td>
		</tr>
	</tbody>
</table>
</div>

<div id="charge_selector" style="display: none"><label
	style="padding-right: 6%">Filter by: </label> <input type="radio"
	name="charge" value="exactcostoption" /> <label
	style="padding-right: 5%">Exact</label> <input type="radio"
	name="charge" value="rangecostoption" checked="checked" /> <label
	style="padding-right: 3%">Range</label> <input id="showcharge"
	type="button" value="Add" class="advancesearch_button"
	onclick="addCharge();" />
<hr></hr>
<table id="costrange">
	<tbody>
		<tr>
			<td><label>From:</label><input id="fromchg" type="text"
				style="width: 80px;" class="number_only" /></td>
			<td><label>To:</label><input id="tochg" type="text"
				style="width: 80px" class="number_only" /></td>
		</tr>
	</tbody>
</table>
<table id="exactcost" style="display: none;">
	<tbody>
		<tr>
			<td><label>Cost:</label><input id="exactcosttext" type="text"
				style="width: 80px;" class="number_only" /></td>
		</tr>
	</tbody>
</table>
</div>

<style>
.apply_button {
	background-color: #4CAF50;
	width: 100px;
	height: 31px;
	font-size: 17px;
	-webkit-border-top-left-radius: 8px;
	-moz-border-radius-topleft: 8px;
	border-top-left-radius: 8px;
	-webkit-border-top-right-radius: 8px;
	-moz-border-radius-topright: 8px;
	border-top-right-radius: 8px;
	-webkit-border-bottom-right-radius: 8px;
	-moz-border-radius-bottomright: 8px;
	border-bottom-right-radius: 8px;
	-webkit-border-bottom-left-radius: 8px;
	-moz-border-radius-bottomleft: 8px;
	border-bottom-left-radius: 8px;
	color: #fff;
	margin-right: 0;
}

.cancel_button {
	background-color: red;
	width: 100px;
	height: 31px;
	font-size: 17px;
	-webkit-border-top-left-radius: 8px;
	-moz-border-radius-topleft: 8px;
	border-top-left-radius: 8px;
	-webkit-border-top-right-radius: 8px;
	-moz-border-radius-topright: 8px;
	border-top-right-radius: 8px;
	-webkit-border-bottom-right-radius: 8px;
	-moz-border-radius-bottomright: 8px;
	border-bottom-right-radius: 8px;
	-webkit-border-bottom-left-radius: 8px;
	-moz-border-radius-bottomleft: 8px;
	border-bottom-left-radius: 8px;
	color: #fff;
	margin-right: 0;
}

a.addFilter {
	float: right;
	font-weight: bold;
	margin-right: 18px;
	padding-bottom:4px;
}

a.saveFilter {
	float: right;
	clear: right;
	padding-left: 10px;
	padding-right: 20px;
	font-weight: bold;
	padding-bottom:4px;
}

#chgstat_dropdown {
	margin-top: 0px;
	width: 85px;
	cursor: pointer;
	background-color:#ededed;
    border: 1px solid #dcdcdc;
	display: inline-block;
	color: #777777;
	font-family: Arial;
	font-size: 15px;
	font-weight: bold;
	font-style: normal;
	height: 31px;
	line-height: 30px;
	text-decoration: none;
	text-align: center;
	-webkit-border-radius: 12px 0 0 12px;
	-moz-border-radius: 12px 0 0 12px;
	border-radius: 12px 0 0 12px; 
	}

#showfilter_indt {
	font-size: 12px;
	font-weight: bold;
	padding-top: 0.5%;
	margin-left:120px;
	padding-right:4px;
}

.clearall_list1 {
	-moz-border-radius: 50px;
	-webkit-border-radius: 50px;
	border-radius: 50px;
	/*padding: 1% 1% 1.5%;*/
	font-size: 12px;
	height:15px;
	display: -moz-stack;
	Background: #e50000;
    font-color: #FFFFFF;
    text-decoration:none; 
    color:#FFFFFF !important;
    border:medium ridge #ffffff !important;
    width:70px;
    margin-left:5px;
    
}

.clearall_grp {
	-moz-border-radius: 50px;
	-webkit-border-radius: 50px;
	border-radius: 50px;
	/*padding: 1% 1% 1.5% 1%;*/
	font-size: 12px;
	height:9px;
	margin-left:70%;
	display: -moz-stack;
	Background: #e50000;
    font-color: #FFFFFF;
    text-decoration:none; 
    color:#FFFFFF !important;
    border:medium ridge #ffffff !important;
    width:70px;
   margin-left:2%;
}

.li_style {
	display: inline-block;
	background-color: #D3D3D3;
	-moz-border-radius: 50px;
	-webkit-border-radius: 50px;
	border-radius: 50px;
	padding: 0.1% 3% 3% 3%;
	font-size: 11px;
	margin-bottom: 7px;
	border-style: ridge;
	border-color: #ffffff;
	height: 9px;
	margin-top:4px;
}

.remove_li {
	font-weight: bold;
	font-size: 12px;
	padding-left: 20px;
	cursor: pointer;
	font: caption;
}

.listview {
	position: absolute;
	margin-left:23.5%;
	overflow-y: auto;
	height: 35px;
	width: 50%;
	margin-top:22px;
}

.listview li{
height:0px;
padding-bottom:21px;
padding-left:8px;
padding-left:8px;
padding-top:0;
}

.sersec {
	position: absolute;
	margin-left:6%;
	width:50%;
	margin-top:-3%;
}

.grpdata {
	margin-left:20%;
	margin-top: -18px;
	width: 44%;
	height:45px;
	overflow-x: hidden;
	overflow-y: auto;
	position: absolute;
	margin-top:-0.25%;
}
.grpdata li{
height:0px;
padding-bottom:21px;
padding-left:8px;
padding-left:8px;
padding-top:0;
}
.serset{
    position: absolute;
	margin-left:6%;
	width:50%;
    margin-top:-2.75%;
}
.setgrpdata {
	margin-left: 20%;
	margin-top: 0%;
	width:44%;
	overflow-x: hidden;
	overflow-y: auto;
	height: 45px;
	position: absolute;
}
.setgrpdata li{
height:0px;
padding-bottom:21px;
padding-left:8px;
padding-left:8px;
padding-top:0;
}
.advancesearch_button {
	background-color: #008CBA;
	border: 1px solid #008CBA;
	width: 65px;
	height: 30px;
	font-size: 17px;
	border-radius: 12px;
	color: #fff;
	margin-right: 0;
	cursor: pointer;
	-moz-box-shadow: inset 0px 1px 0px 0px #aeaeae;
	-webkit-box-shadow: inset 0px 1px 0px 0px #aeaeae;
	box-shadow: inset 0px 1px 0px 0px #aeaeae;
}

.cancel_button {
	background-color: red;
	width:80px;
	height: 31px;
	font-size: 16px;
	border: 1px solid red;
	-webkit-border-top-left-radius: 8px;
	-moz-border-radius-topleft: 8px;
	border-top-left-radius: 8px;
	-webkit-border-top-right-radius: 8px;
	-moz-border-radius-topright: 8px;
	border-top-right-radius: 8px;
	-webkit-border-bottom-right-radius: 8px;
	-moz-border-radius-bottomright: 8px;
	border-bottom-right-radius: 8px;
	-webkit-border-bottom-left-radius: 8px;
	-moz-border-radius-bottomleft: 8px;
	border-bottom-left-radius: 8px;
	color: #fff;
	margin-right: 0;
	-moz-box-shadow: inset 0px 1px 0px 0px #868686;
	-webkit-box-shadow: inset 0px 1px 0px 0px #868686;
	box-shadow: inset 0px 1px 0px 0px #868686;
	cursor: pointer;
}

.specified_search_dialog {
	z-index: 1025 !important;
    border: 1px solid grey  !important;
	width:30% !important;
	overflow:none;
}
.advncesrchdialog{
	overflow:hidden;
	border: 1px solid grey  !important;
	
	width:auto !important;
}
.advncesrchdialogsave{
	overflow:hidden;
	border: 1px solid grey  !important;
	left:750px !important;
	width:auto !important;
}
.datablock {
	display: flex;
	margin-left: 14%;
	overflow-x: auto;
	width: 62%;
}

.save_filter_button {
	background-color: #4CAF50;
	width: 120px;
	height: 31px;
	font-size: 17px;
	-webkit-border-top-left-radius: 8px;
	-moz-border-radius-topleft: 8px;
	border-top-left-radius: 8px;
	-webkit-border-top-right-radius: 8px;
	-moz-border-radius-topright: 8px;
	border-top-right-radius: 8px;
	-webkit-border-bottom-right-radius: 8px;
	-moz-border-radius-bottomright: 8px;
	border-bottom-right-radius: 8px;
	-webkit-border-bottom-left-radius: 8px;
	-moz-border-radius-bottomleft: 8px;
	border-bottom-left-radius: 8px;
	color: #fff;
	margin-right: 0;
}
</style>
