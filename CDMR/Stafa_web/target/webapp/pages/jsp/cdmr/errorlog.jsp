<meta http-equiv="PRAGMA" content="NO-CACHE">
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/jquery/ColVis.js"></script>
<link type="text/css" href="css/cdmr/themes/base/jquery.ui.all.css"
	rel="stylesheet" />
<link href="css/cdmr/ColReorder.css" rel="stylesheet" type="text/css">
<link href="css/cdmr/jquery.multiDialog.css" rel="stylesheet"
	type="text/css">
<link href="css/cdmr/jquery.treeview.css" rel="stylesheet"
	type="text/css">
<script type="text/javascript" src="js/cdmr/ColReorder.js"></script>
<script type="text/javascript" src="js/cdmr/jquery.treeview.js"></script>
<script type="text/javascript" src="js/cdmr/jquery.treeview.async.js"></script>
<div id="notification" >
	 <div id="notificationDiv"  style="height: 450px; overflow:scroll">
		
			<div>
				<div style="margin-top: 10px">
		
				<div>
					<div id="tableDetail" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" style="margin-top: 20px;">
						<div class="portlet-header ui-widget-header ui-corner-all">Notification</div>
							<div class="portlet-content">
								<table cellpadding="0" cellspacing="0" border="0" id="errorLogTable">
					         		<thead>
					                <tr style="height:15px">
					                     <th colspan="1" style="width:50px;height:20px">Date</th>
										 <th colspan="1" style="width:200pxheight:20px">Exception Name</th>
					                     <th colspan="1" style="width:200pxheight:20px">Exception Description</th>
					                 </tr>
									</thead>
									<tbody></tbody>
					     		</table>
							</div>
						</div>
					</div>
					</div>
					
				</div>
			</div>
		
		
	</div>
		




 <script>



	
	
	
	
	



	$(document).ready(function(){

		
		loadNotifiedUer();
	});

	function loadNotifiedUer(){


		$("#errorLogTable").dataTable({
			"bProcessing": false,
		    "bServerSide": true,
		    "sAjaxSource": "loaderrorlog.action",
		     "bJQueryUI": true,
		    "bPaginate": true,
		    'iDisplayLength': 10,
		    "bLengthChange": false,
		    "bDestroy": true,
		    "bFilter": true,
		    "bSort": true,
		    "sDom": 'R<"clear">lfrtip',
		    "aoColumns": [
							{"sWidth":"20%","mDataProp":"ERR_REP_ON","bSortable":true},
							{ "sWidth":"70%","mDataProp":"ERR_NAME","bSortable":true},
			      			{"sWidth":"10%","mDataProp":"ERR_DESC", "bSortable":false}	
			      	    ],
		});
		$(".ColVis_MasterButton span").html("");
		

		
		/*$("#notificationTable").dataTable({
			"bProcessing": false,
		    "sAjaxSource": "getnotification.action",
		     "bJQueryUI": true,
		    "bPaginate": true,
		    'iDisplayLength': 10,
		    "bLengthChange": false,
		    "bDestroy": true,
		    "bFilter": true,
		    "bSort": true,
		    "sScrollY": "200px",
		    "sScrollX": "100%",
		    "sScrollXInner":"100%",
		    "aoColumns": [
							{"sWidth":"50px", "mDataProp":"STATE_TO","bSortable":true},
			      			{"sWidth":"200px","mDataProp":"USER_EMAIL", "bSortable":true},
			      			{"sWidth":"100px","mDataProp":"delete", "bSortable":true}	
			      	    ],
		    "fnDrawCallback": function () {
		    	$('#allcb').prop('checked', false);
		    	$('tbody tr td input[name="checkForUSer"]').each(function(){
			    	if($.inArray($(this).val(), checkdAll) != -1){
			    		$(this).prop('checked', true);
			    		$('#allcb').prop('checked', true);
			    	}
		        });
		    	
		    }
		});*/
	}


	
</script>

<style>
	
.staging_button{
	-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf) );
	background:-moz-linear-gradient( center top, #ededed 5%, #dfdfdf 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf');
	background-color:#ededed;
	-webkit-border-top-left-radius:5px;
	-moz-border-radius-topleft:5px;
	border-top-left-radius:5px;
	-webkit-border-top-right-radius:5px;
	-moz-border-radius-topright:5px;
	border-top-right-radius:5px;
	-webkit-border-bottom-right-radius:5px;
	-moz-border-radius-bottomright:5px;
	border-bottom-right-radius:5px;
	-webkit-border-bottom-left-radius:5px;
	-moz-border-radius-bottomleft:5px;
	border-bottom-left-radius:5px;
	text-indent:0;
	border:1px solid #dcdcdc;
	display:inline-block;
	color:#777777;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	font-style:normal;
	height:25px;
	line-height:23px;
	text-decoration:none;
	text-align:center;
	text-shadow:1px 1px 0px #ffffff;
}

.staging_button:hover{
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #dfdfdf), color-stop(1, #ededed) );
	background:-moz-linear-gradient( center top, #dfdfdf 5%, #ededed 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#dfdfdf', endColorstr='#ededed');
	background-color:#dfdfdf;
}

#addnotes{
	position: absolute;
	right: 220px;
}

.dropdown select {

      border: 1px solid !important;  /*Removes border*/
    
      -moz-appearance: none; /* Removes Default Firefox style*/
      background-position: 82px 7px;  /*Position of the background-image*/
      width: 100px; /*Width of select dropdown to give space for arrow image*/
      

      /*My custom style for fonts*/
		width:150px;
		height: 25px;
      color: #1455a2;
      margin-left: :35px;
}

.dataTables_filter{
	margin-top: 0px;
}
#tableDetail .dataTables_filter {
	margin-top: -20px;
	margin-bottom: -10px;
	padding-top: 30px;
}
#userdiv tr{
	width: 270px;
}


	
	#errorLogTable {
	width: 100% !important;
}

	

.dataTables_filter{
	margin-top: -25px !important;
}
.dataTables_filter {
	margin-top: -28px;
	margin-bottom: -10px;
}
.dataTables_filter {
	margin-top: -20px;
	margin-bottom: -10px;
}

.ColVis_MasterButton span {
	font-size: 10px;
}

.ColVis_MasterButton {
	width: 150%;
}

.ColVis_MasterButton span {
	margin-left: 15px;
}

.TableTools {
	width: 105px;
	padding-left: 10px;
	float: left;
}

td {
  word-wrap: break-word;
}

</style>