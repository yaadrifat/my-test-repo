
<!DOCTYPE HTML>
<html>
<head>
	<title>YUI 3 Treeble Example</title>
	<meta http-equiv=content-type content="text/html; charset=utf-8">

	<script src="http://yui.yahooapis.com/3.10.2/build/yui/yui-min.js"></script>
	<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.10.2/build/cssreset/cssreset-min.css">
	<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.10.2/build/cssfonts/cssfonts-min.css" />
	<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.10.2/build/cssbase/cssbase-min.css" />
<!--
	<script src="gallery-treeble-debug.js"></script>
	<script src="gallery-paginator-debug.js"></script>
	<script src="gallery-datatable-state-debug.js"></script>
	<script src="gallery-funcprog-debug.js"></script>
	<script src="gallery-object-extras-debug.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/gallery-treeble.css" />
	<link rel="stylesheet" type="text/css" href="assets/gallery-paginator.css" />
-->
	<style type="text/css">
	body
	{
		margin: 8px;
	}

	th, td
	{
		border: 0;	/* cancel out base.css */
	}

	.pg-rpp-label
	{
		padding-left: 2em;
	}

	.yui3-skin-sam .yui3-datatable caption
	{
		padding: 0 !important;
	}
	</style>
</head>
<body class="yui3-skin-sam">

<h1><a href="http://yuilibrary.com/gallery/show/treeble">YUI 3 Treeble</a> Example</h1>

<div id="pg"></div>
<div id="treeble"></div>

<p>Checkbox state is preserved during expand/collapse courtesy of <a href="http://yuilibrary.com/gallery/show/datatable-state">gallery-datatable-state</a></p>
<p><a href="index.html">Show me an example with a local dataset</a></p>
<p><a href="../yuidoc/modules/gallery-treeble.html">Show me the documentation</a></p>
<p><a href="/yui2/treeble/index.html">Show me the YUI 2 example</a></p>
<p><a href="/yui-modules/">Back to YUI 3 project list</a></p>

<script type="text/javascript">
YUI({
//	filter: 'raw', combine: false,
	gallery: 'gallery-2013.06.26-23-09'
}).use('datatable', 'gallery-treeble', 'gallery-paginator', 'gallery-datatable-state', function(Y)
{
	function sendRequest()
	{
		table.datasource.load(
		{
			request:
			{
				startIndex:  pg.getStartIndex(),
				resultCount: pg.getRowsPerPage()
			}
		});
	}

	// column configuration

	var cols =
	[
		{ key: 'checkbox', label: '&nbsp;', allowHTML: true,
			formatter: function(o)
			{
				return '<input type="checkbox" />';
			}
		},
		{
			label: '&nbsp;',
			nodeFormatter: Y.Treeble.buildTwistdownFormatter(sendRequest)
		},
		{
			key: 'title',
			label: 'Title',
			formatter: Y.Treeble.treeValueFormatter,
			allowHTML: true
		},
		{ key: 'year', label: 'Year' },
		{ key: 'quantity', label: 'Quantity' }
	];

	// treeble config to be set on root datasource

	var schema =
	{
		resultFields:
		[
			'quantity','year','title', '_open',
			{key: 'kiddies', parser: 'treebledatasource'}
		]
	};

	var schema_plugin_config =
	{
		fn:  Y.Plugin.DataSourceJSONSchema,
		cfg: {schema:schema}
	};

	var treeble_config =
	{
		generateRequest:        function() { },
		schemaPluginConfig:     schema_plugin_config,
		childNodesKey:          'kiddies',
		nodeOpenKey:            '_open',
		totalRecordsReturnExpr: '.meta.totalRecords'
	};

	// root data source

	var root            = new Y.DataSource.IO({source: 'http://jafl.github.io/yui3-gallery/treeble/data/root.json'});
	root.treeble_config = Y.clone(treeble_config, true);
	root.plug(schema_plugin_config);

	// TreebleDataSource

	var ds = new Y.TreebleDataSource(
	{
		root:             root,
		paginateChildren: false,
		uniqueIdKey:      'title'	// normally, it would a database row id, but title happens to be unique in this example
	});

	// Paginator

	var pg = new Y.Paginator(
	{
		totalRecords: 1,
		rowsPerPage: 1,
		rowsPerPageOptions: [1,2,5,10,25,50],
		template: '{FirstPageLink} {PreviousPageLink} {PageLinks} {NextPageLink} {LastPageLink} <span class="pg-rpp-label">Rows per page:</span> {RowsPerPageDropdown}'
	});
	pg.render('#pg');

	pg.on('changeRequest', function(state)
	{
		this.setPage(state.page, true);
		this.setRowsPerPage(state.rowsPerPage, true);
		this.setTotalRecords(1000, true);
		sendRequest();
	});

	ds.on('response', function(e)
	{
		pg.setTotalRecords(e.response.meta.totalRecords, true);
		pg.render();
	});

	// DataTable

	var table = new Y.Treeble({columns: cols});
	table.plug(Y.Plugin.DataTableDataSource, {datasource: ds});
	table.plug(Y.Plugin.DataTableState,
	{
		uniqueIdKey: 'title',	// normally, it would be a database row id, but title happens to be unique in this example
		paginator:   pg,
		save:
		[
			{
				column: 'checkbox',
				node:   'input',
				key:    'checked',
				temp:   true
			}
		]
	});
	table.render("#treeble");

	sendRequest();
});
</script>

</body>
</html>
