<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="../common/includes.jsp" %>
<% String contextpath=request.getContextPath();

String CDMR_SETTINGS_IMPORT="Import Data";
String CDMR_SETTINGS_EXPORT="Export Data";

%>
<%!
String checkAccessRights(String moduleName){
	//System.out.println(" moduleName--------------------------- " + moduleName+"=="+(String)moduleMap.get(moduleName));
	 return (String)moduleMap.get(moduleName);
}
 %>



<meta http-equiv="PRAGMA" content="NO-CACHE">
<%@ taglib prefix="s" uri="/struts-tags"%>
<link type="text/css" href="css/cdmr/themes/base/jquery.ui.all.css" rel="stylesheet" />

<div id="tabs">
<ul>
	<li><a href="#tabs-1" style="display:none" id="import">Import Data</a></li>
	<li><a href="#tabs-2" style="display:inline">Files at Home Page</a></li>
	<li><a href="#tabs-3" style="display:none" id="export">Export Data</a></li>
</ul>
<div id="tabs-1" >
	
	<input id= "import" type="button" class="import_button" value="Import Now" onclick="importNow();" style="float:right">
	<span id="scheduletime" style="float:left;"></span>
	<br>
	<p style="margin-top:0px">Note: Time uses the 24-hour clock. For example, 1 min after midnight is 00:01</p>
	
	<hr>
	<br/>
	<div style="float:left"><label>Schedule Task: &nbsp;&nbsp;</label> 
		<select id="dayOption" style="width: 100px;" onchange="updateUI(this.value);">
			<option value="1">Daily</option>
			<option value="2">Weekly</option>
		</select> &nbsp;&nbsp;&nbsp;&nbsp; <label>Enter Time:&nbsp;&nbsp;</label> 
		<input type="text" id="timepicker" class="timepicker">
	</div>
	<div style="margin-left: 600px;"><input type="button" class="apply_button" id="reschedule" value="Apply" onclick="rescheduleTime();"></div>
	<div id="weekdays" style="display: none; margin-top: 15px;">
	<label>Every :</label>
		<input type="checkbox" name="days" id="days" value="MON" />Mon&nbsp; 
		<input type="checkbox" name="days" id="days" value="TUE" />Tue&nbsp; 
		<input type="checkbox" name="days" id="days" value="WED" />Wed&nbsp; 
		<input type="checkbox" name="days" id="days" value="THU" />Thu&nbsp; 
		<input type="checkbox" name="days" id="days" value="FRI" />Fri&nbsp;
	    <input type="checkbox" name="days" id="days" value="SUT" />Sat&nbsp; 
	    <input type="checkbox" name="days" id="days" value="SUN" />Sun&nbsp;
    </div>
	<br>
</div>
<div id="tabs-2">
	<div><br/>
		<div><label>No. of Files to display at Home Page: 	&nbsp;&nbsp;</label> 
			<input type="text" id="nooffiles"> 
			<input type="button" value="Submit" onclick="saveInDB();" class="apply_button">
		</div>
	</div>
	<br>
</div>
<div id="tabs-3">
	<input id="export" type="button" class="import_button" value="Export Now" onclick="exportNow();" style="float:right">
	<span id="scheduletimeexport" style="float:left;"></span>
	<br>
	<p style="margin-top:0px">Note: Time uses the 24-hour clock. For example, 1 min after midnight is 00:01</p>
	<hr>
	<br>
	
	<div style="float:left">
		<label>Schedule Task: &nbsp;&nbsp;</label> 
		<select id="dayOption1" style="width: 100px;" onchange="updateUI2(this.value);">
			<option value="1">Daily</option>
			<option value="2">Weekly</option>
		 </select> &nbsp;&nbsp;&nbsp;&nbsp; <label>Enter Time:&nbsp;&nbsp;</label>
		 <input type="text" id="timepicker1" class="timepicker">
	 </div>
	 <div style="margin-left: 600px;">
	 	<input type="button" class="apply_button" id="reschedule1" value="Apply" onclick="rescheduleTimeExport();">
	 </div>
	 <div id="weekdays1" style="display: none; margin-top: 15px;">
	 <label>Every :</label>
		<input type="checkbox" name="days" id="days1" value="MON" />Mon&nbsp; 
		<input type="checkbox" name="days" id="days1" value="TUE" />Tue&nbsp; 
		<input type="checkbox" name="days" id="days1" value="WED" />Wed&nbsp; 
		<input type="checkbox" name="days" id="days1" value="THU" />Thu&nbsp; 
		<input type="checkbox" name="days" id="days1" value="FRI" />Fri&nbsp; 
		<input type="checkbox" name="days" id="days1" value="SUT" />Sat&nbsp; 
		<input type="checkbox" name="days" id="days1" value="SUN" />Sun&nbsp;
	 </div>
 <br>
</div>

</div>

<script>
	//var checkdAll = new Array();
   	//var emailArray = new Array();
   	//var chkIndex=0;
   	var importstatus=0;
   	var exportstatus=0;
	$(function() {
   		var option = '';
   		for(var i = 1; i<=12; i++){
   			option = option + '<option value="'+i+'">'+i+'</option>';
   		}
   		$("#hours").html(option);
   		for(var i = 13; i<=60; i++){
   			option = option + '<option value="'+i+'">'+i+'</option>';
   		}
   		$("#minute").html(option);
   		$("#seconds").html(option);
   
   		$('.timepicker').timepicker({
   			hourGrid: 4,
   			minuteGrid: 10,
   			 timeFormat: 'HH:mm', 
   			  ampm: true
   		});
   
   		
   	});
   	$(document).ready(function(){
   		$("#tabs" ).tabs();
   		getScheduledTime();
   		getScheduledTimeexport();
   		getNoOfFileValue();
   		$.ajax({
   			url:"getbannertext.action",
   			type:"post",
   			data:null,
   			cache:false,
   				success:function(response){
   				importstatus=response.import_status;
   				exportstatus=response.export_status;
   		      	  if(response.import_status=='1'&&response.export_status=='0'){
   		      		$("#import").attr("disabled","true");
   					}
   					else if(response.export_status=='1'&& response.import_status=='0'){
   						$("#export").attr("disabled","true");
   					}
   					else if(response.import_status=='1' && response.export_status=='1'){
   						$("#import").attr("disabled","true");
   						$("#export").attr("disabled","true");
   					}
   					else{
   						$("#import").removeAttr('disabled');
   						$("#import").removeAttr('disabled');
   					}
   		       }

   		});
   		if(checkExportRights()){
             $("#export").css("display","inline");
   	   		}
   		if(checkImportRights()){
   	   		
            $("#import").css("display","inline");
           
	   		}
   		if(!checkExportRights() && !checkImportRights()){
   			
   			$('#tabs').tabs('select', '#tabs-2');
   	   		}
	   	if(!checkImportRights()){
	   		$('#tabs').tabs('select', '#tabs-2');
		   	}	
	   	if(!checkImportRightsEdit()){
		   	$("#timepicker").attr("disabled","disabled");
		   	$("#import").attr("disabled","true");
			
		   	}
	   	if(!checkExportRightsEdit()){
	   		$("#timepicker1").attr("disabled","disabled");
	   		$("#export").attr("disabled","true");
	   	}
	   	if(!checkSettingsWindowRightsedit()){
			
			$("#nooffiles").attr("disabled","disabled");
			}	
   	});
  	function importNow(){
   		var r = alertify.confirm("This may take several mintues. Do you wish to proceed?", function(r){
			if(r){
					$("#bannercontent").show();
					if(exportstatus=='1'){
						$("#bannercontent").html("Export Interface is in progress. System slowness may occur");
						$("#bannercontent").append("<div style='background-color: yellow;'>Import Interface is in progress. System slowness may occur</div>");
						}
					else{
						$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
						}
					
					$.unblockUI(); 
					alertify.alert("Interface started successfully.");
					$.ajax({
			      		url:"importDATAnow.action",
				  		type:"post",
				  		data:null,
				   	    success:function(response){	
						if(exportstatus=='1'){
							$("#bannercontent").html("Export Interface is in progress. System slowness may occur");
						}
						else{
					    	$("#bannercontent").hide();
						}
						 }
			  	    });
				}
			else{
   				}
   			return;
   		});
   	}

	function exportNow(){
			$.ajax({
		      url:"getexportcount.action",
			  type:"post",
			  data:null,
			  success:function(response){
				var r = alertify.confirm("There are currently "+response.export_count+" of changes pending. This process may take several minutes. Do you wish to proceed?", function(r){
			  	if(r){
				  	if(response.export_count!=0){
				     	$.unblockUI(); 
				     	alertify.alert("Interface started successfully.");
				     	$("#bannercontent").show();
				     	if(importstatus=='1'){
							$("#bannercontent").html("Export Interface is in progress. System slowness may occur");
							$("#bannercontent").append("<div style='background-color: yellow;'>Import Interface is in progress. System slowness may occur</div>");
							}
				     	else{
     						$("#bannercontent").html("Export Interface is in progress. System slowness may occur");
				     	}
			         	$.ajax({
			       			url:"exportDATAnow.action",
				   			type:"post",
				   			data:null,
				  	   	    success:function(response){
			         		if(importstatus=='1'){
								$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
							}
			         		else{
			         			$("#bannercontent").hide();
			         		}
			         		}
			           });
				  	}
			  		else{
			  			alertify.alert("No changes found to export/update.");	
				  	}
			   }
			  	else{
	   			}
				 return;
				});
				
			}
			
   		
   		 });
 }
	  	
   	function getNoOfFileValue(){
   		$.ajax({
   			url:"getnooffiles.action",
   			type:"post",
   			data:null,
   			success:function(response){
   			
   			 $("#nooffiles").val(response.nooffiles);
   			}
   		});
   	}
      	
   	function saveInDB(){
   		if($("#nooffiles").val() == 0){
   			alertify.alert("Please enter valid number.");
   			return;
   		}else if(isNaN($("#nooffiles").val())){
   			alertify.alert("Please enter valid number.");
   			return;
   		}
   
   		$.ajax({
   			url:"updatenooffiles.action",
   			type:"post",
   			data:{"nooffiles":$("#nooffiles").val()},
   			success:function(response){
   	   		
   				alertify.alert("Updated Successfully.");
   				
   				$.unblockUI(); 
   				
   				closeProcessWindow();
   			}
   		});
   	}
   
   	function getScheduledTime(){
   		
   		$.ajax({
   			url:"getschedulrdtime.action",
   			type:"post",
   			data:null,
   			success:function(response){
   			
   				$("#scheduletime").html(response.time);
   			}
   		});
   	}
   	function getScheduledTimeexport(){
   	   	
   		$.ajax({
   			url:"getscheduledtimeexport.action",
   			type:"post",
   			data:null,
   			success:function(response){
   			
   				$("#scheduletimeexport").html(response.timeexport);
   			}
   		});
   	}
      	
   function rescheduleTime(){
   		var type = $("#dayOption").val();
   		var timer = $("#timepicker").val();
   		if(timer == ""){
   			alertify.alert("Please Enter Time.");
   		}
   		var arrayData = timer.split(":");
   		var hours = arrayData[0];
   		var minutes =  arrayData[1];
   		if(minutes.indexOf('am') > -1){
   			minutes = minutes.replace(" am", "");
   		}else if(minutes.indexOf('pm') > -1){
   			minutes = minutes.replace(" pm", "");
   		}
   		if($("#dayOption").val() == "1"){
   			timer = "0 " + minutes + " " + hours + " ? * *";
   		}else if($("#dayOption").val() == "2"){
   			var ids=[]; 
   			var day = "";
   			$('input[id=days]:checked').each(function(){
   			    ids.push($(this).val());
   			});
   
   			if(ids.length == 0){
   				alertify.alert("Please Select Day(s).");
   				return;
   			}
   			for(var i=0; i<ids.length;i++){
   				day = day + ids[i] + ",";
   			}
   			day = day.replace(/,(\s+)?$/, '');
   			timer = "0 " + minutes + " " + hours + " ? * " + day
   		}
   		   
   		$.ajax({
   			url:"updatescheduler.action",
   			type:"post",
   			data:{"timer":timer},
   			//data:{"timer":timer,"type":"IMPORT_SCHEDULER"},
   			success:function(response){
   				getScheduledTime();
   				
   				alertify.alert("Timer Updated Successfully.");
   				$.unblockUI(); 
   			}
   		});
   	}
   function rescheduleTimeExport(){
	   var type = $("#dayOption1").val();
  		var timer = $("#timepicker1").val();
  		if(timer == ""){
  			alertify.alert("Please Enter Time.");
  		}
  		var arrayData = timer.split(":");
  		var hours = arrayData[0];
  		var minutes =  arrayData[1];
  		if(minutes.indexOf('am') > -1){
  			minutes = minutes.replace(" am", "");
  		}else if(minutes.indexOf('pm') > -1){
  			minutes = minutes.replace(" pm", "");
  		}
  		if($("#dayOption1").val() == "1"){
  			timer = "0 " + minutes + " " + hours + " ? * *";
  		}else if($("#dayOption1").val() == "2"){
  			var ids=[]; 
  			var day = "";
  			$('input[id=days1]:checked').each(function(){
  			    ids.push($(this).val());
  			});
  
  			if(ids.length == 0){
  				alertify.alert("Please Select Day(s).");
  				return;
  			}
  			for(var i=0; i<ids.length;i++){
  				day = day + ids[i] + ",";
  			}
  			day = day.replace(/,(\s+)?$/, '');
  			timer = "0 " + minutes + " " + hours + " ? * " + day
  		}
  		   		
  		$.ajax({
  			url:"updateschedulerexport.action",
  			type:"post",
  			data:{"timer":timer},//"type":"EXPORT_SCHEDULER"},
  			success:function(response){
  				getScheduledTimeexport();
  				alertify.alert("Timer Updated Successfully.");
  				$.unblockUI(); 
  			}
  		});
	   }
   	function updateUI(value){
   		if(value=="2"){
   			$("#weekdays").css('display', 'block');
   		}else if(value=="1"){
   			$("#weekdays").css('display', 'none');
   		}
   		
   	}
 	function updateUI2(value){
   		if(value=="2"){
   			$("#weekdays1").css('display', 'block');
   		}else if(value=="1"){
   			$("#weekdays1").css('display', 'none');
   		}
   		
   	}   
 	function checkExportRights(){

			return checkAppRights("<%=checkAccessRights(CDMR_SETTINGS_EXPORT)%>",APPMODE_READ);
			
		}
	function checkImportRights(){
			return checkAppRights("<%=checkAccessRights(CDMR_SETTINGS_IMPORT)%>",APPMODE_READ);
		}
	function checkImportRightsEdit(){
		return checkAppRights("<%=checkAccessRights(CDMR_SETTINGS_IMPORT)%>",APPMODE_SAVE);
	}
	function checkExportRightsEdit(){
		return checkAppRights("<%=checkAccessRights(CDMR_SETTINGS_EXPORT)%>",APPMODE_SAVE);
	}
	  
</script>
<style>
.apply_button {
	background-color: #4CAF50;
	border: 1px solid #4CAF50;
	width: 80px;
	height: 31px;
	font-size: 17px;
	-webkit-border-top-left-radius: 8px;
	-moz-border-radius-topleft: 8px;
	border-top-left-radius: 8px;
	-webkit-border-top-right-radius: 8px;
	-moz-border-radius-topright: 8px;
	border-top-right-radius: 8px;
	-webkit-border-bottom-right-radius: 8px;
	-moz-border-radius-bottomright: 8px;
	border-bottom-right-radius: 8px;
	-webkit-border-bottom-left-radius: 8px;
	-moz-border-radius-bottomleft: 8px;
	border-bottom-left-radius: 8px;
	color: #fff;
	margin-right: 0;
	cursor: pointer;
	-moz-box-shadow: inset 0px 1px 0px 0px #bcbcbc;
	-webkit-box-shadow: inset 0px 1px 0px 0px #bcbcbc;
	box-shadow: inset 0px 1px 0px 0px #bcbcbc;
}
.import_button {
	background-color: #008CBA;
	border: 1px solid #008CBA;
	width: 100px;
	height: 30px;
	font-size: 17px;
	border-radius: 12px;
	color: #fff;
	margin-right: 0;
	cursor: pointer;
	-moz-box-shadow: inset 0px 1px 0px 0px #aeaeae;
	-webkit-box-shadow: inset 0px 1px 0px 0px #aeaeae;
	box-shadow: inset 0px 1px 0px 0px #aeaeae;
}

</style>