
<meta http-equiv="PRAGMA" content="NO-CACHE">
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/jquery/ColVis.js"></script>
<link type="text/css" href="css/cdmr/themes/base/jquery.ui.all.css"	rel="stylesheet" />
<link href="css/cdmr/ColReorder.css" rel="stylesheet" type="text/css">
<link href="css/cdmr/jquery.multiDialog.css" rel="stylesheet" type="text/css">
<link href="css/cdmr/jquery.treeview.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/cdmr/ColReorder.js"></script>
<script type="text/javascript" src="js/cdmr/jquery.treeview.js"></script>
<script type="text/javascript" src="js/cdmr/jquery.treeview.async.js"></script>
<script type="text/javascript" src="js/cdmr/ColumnStateSave.js"></script>
<script type="text/javascript" src="js/cdmr/advancesearch.js"></script>

<jsp:include page="advancesearch.jsp"/>
<div class="cleaner"></div>
<div style="margin-bottom:5px;background-color:#cccccc;width:30%;height:20px !important;border-radius:3px;padding:5px;">
<span style="padding-left:15px;" id="showLibraryName"></span></div>
<div class="column">
<div class="portlet">

<div class="portlet-content">
<div id="assignDonorDiv" style="overflow-x: auto; overflow-y: hidden;">
<div style="float: left; margin-top: -4px;">

<table cellspacing="10">
	<tr>
		<td><label>View By</label></td>
		<td><input type="radio" name="type" value="list"
			checked="checked" id="bylist" onclick="loadListView();" />List</td>
		<td><input type="radio" name="type" value="group" id="bysersec"
			onclick="loadGroupView();" />Service Section</td>
		<td><input type="radio" name="type" value="service" id="byserset"
			onclick="loadServiceSetView();" />Service Set</td>
		<td style="padding-left: 25px;"><div id = "charge_type_caption"style = "display:none"><label>Charge Type</label></div></td>
		<td>
		<div id = "charge_type_dropdown" class="dropdown" style = "display:none">
		<select id="chgtype" style="width: 100px;"
			onchange="updateViewForType(this.value);">	
		</select></div>
		</td>
		<!--<td style="padding-left: 10px;"><label>Charge Status</label></td>
		<td>
		<div class="dropdown"><select id="chgstat" style="width: 90px;" onchange="updateViewForType(this.value);">
			<option value="1" selected="selected">Active Chgs</option>
			<option value="2">Inactive Chgs</option>
			<option value="3">Both Chgs</option>
		</select></div>
		</td>
	--></tr>
</table>
</div>
<div style="float: left; margin-left: 5%;">
<table>
	<tr>
		<td><input id="createSerSecBut" type="button"
			onclick="openCreateGroup();" value="Create Service Section"
			class="btn_blue" /></td>
		<td><input id="createSerSetBut" type="button"
			onclick="openCreateSerSet();" value="Create Service Set"
			class="btn_blue" /></td><!--staging_button--></tr>
</table>
</div>
<div style="float: right;">
<table>
	<tr>
		<td>Preview Previous Version:</td>
		<td>
		<div class="dropdown"><select id="preprvver"
			onchange="updateViewForVersion(this.value);"></select></div>
		</td>
	</tr>
</table>
</div>
</div>
</div>
</div>
<div class="column" style="width: 100%">
	<div class="column" style="width: 30%; float: left; display: none;"	id="forTreeView">
		<div class="portlet">
			<div class="portlet-header "><s:text name="vcdmr.label.availablegrp" />	</div>
			<div id="maincdmData_filter1" class="dataTables_filter"	style="margin-left : 2%; margin-top: 0%; margin-right: 0%; width: 98%; padding-top: 35px;">
				<div style="float: left;">
					<label> <input type="button" value="Edit" class="btn_green" onclick="editServiceSection();" id="EditSerSec"> </label>
				</div>
				<div style="float: right;">
					<label> 
					<input id="searchgrp" class="searchinggrp1" type="text" aria-controls="maincdmData">
					<input type="button" class="mysearchbtn" value="" onclick="hitEnterEvent(this);" aria-controls="maincdmData"> </label>
				</div>
			</div>
			<div id="treediv" class="portlet-content"
				style="height: 280px; overflow: scroll; margin-top: 15%;"></div>
		</div>
	</div>
<div class="column" style="width: 30%; float: left; display: none;"
	id="forSetsView">
<div class="portlet">
<div class="portlet-header "><s:text
	name="vcdmr.label.availabless" /></div>
<div class="portlet-content" style="height: 335px;">
<table cellspacing="0" border="0" id="servSetsView" class="servSetsView">
	<thead>
		<tr>
			<th width="4%" id="checkall"></th>
			<th colspan="1"><s:text name="vcdmr.label.ServiceSets" /></th>
		</tr>
	</thead>
	<tbody>		
	</tbody>
</table>
</div>
</div>
</div>
<div class="column" style="float: right; width: 100%" id="forGroupView">
<div class="portlet">
<div class="portlet-header "><s:text
	name="vcdmr.label.mainchargemaster" />&nbsp;&nbsp;<span
	id="selectedname"></span></div>
<div class="portlet-content">

<table cellspacing="0" border="0" id="maincdmData" class="cdmmaindata">
	<thead>
		<!-- <tr>
			<th width="4%" id="checkall"><s:text name="vcdmr.label.checkall" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeGNLDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.CMSHCPCSCPTCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.CMSEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.PRSCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.MCAIDHCPCSCPTCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.MCAIDEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.cptcode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.BCBSHCPCSCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.BCBSEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.RevClass" /></th>
			<th colspan="1"><s:text name="vcdmr.label.RevClassDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.GLKey" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCPrice" /></th>
			<th colspan="1"><s:text name="vcdmr.label.prschrg" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCPriceEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeINACTDate" /></th>
		</tr>-->
	</thead>
	<tbody>
		<!-- Datat Table Data -->
	</tbody>
</table>
</div>
</div>
</div>
</div>
<input type="hidden" id="indexHolder"></div>
<div id="createSerSet"
	style="width: 100%; display: none; overflow: scroll; height: 550px;">
<div>
<div class="column" style="width: 30%; float: left;">
<div class="portlet">
<div class="portlet-header "><s:text
	name="vcdmr.label.ListofAvailbleSet" /></div>
<div class="portlet-content">
	<div style="font-size:11px;padding:5px;border-bottom:1px solid #ddd;width:96%;height:25px;">
		<input type="text" name="servsetname" id="servsetname" style="display: none;"> 
		<input type="button" id="crtserset" class="btn_blue" value="Add New Service Set" onclick="createServiceSetDialog();"> 
		<input type="button" id="cancelbutton" style="display: none;" class="btn_red" value="Cancel" onclick="hideServiceSetDialog();"> 
	</div>
	<div id="assignDonorDiv" style="overflow-x: auto; overflow-y: hidden;padding-top:5px;width:100%;">
		<table cellspacing="0" border="0" id="servSets" class="servSets">
			<thead>
				<tr>
					<th width="4%" id="checkall"></th>
					<th colspan="1"><s:text name="vcdmr.label.ServiceSets" /></th>
				</tr>
			</thead>
			<tbody>
				<!-- Datat Table Data -->
			</tbody>
		</table>
	</div>
</div>
</div>
</div>
<div class="column" style="width: 70%; float: right;">
<div class="portlet">
<div class="portlet-header "><s:text
	name="vcdmr.label.iteminselectedset" /></div>
<div class="portlet-content">
<div id="assignDonorDiv" style="overflow-x: auto; overflow-y: hidden;">
<table cellspacing="0" border="0" id="servLink" class="servLink">
	<thead>
		<!-- <tr>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeGNLDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.CMSHCPCSCPTCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.CMSEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.PRSCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.MCAIDHCPCSCPTCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.MCAIDEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.cptcode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.BCBSHCPCSCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.BCBSEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.RevClass" /></th>
			<th colspan="1"><s:text name="vcdmr.label.RevClassDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.GLKey" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCPrice" /></th>
			<th colspan="1"><s:text name="vcdmr.label.prschrg" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCPriceEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeINACTDate" /></th>
		</tr>-->
	</thead>
	<tbody id="sslink">
		<!-- Datat Table Data -->
	</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
<div class="column" style="text-align: left">
	<div class="portlet" style="font-size: 11px;">
		<div class="portlet-content">
			<div id="assignDonorDiv" style="overflow-x: auto; overflow-y: hidden;">
				<!-- <input type="text" name="servsetname" id="servsetname" style="display: none;"> 
				<input type="button" id="crtserset" class="btn_blue" value="Add New Service Set" onclick="createServiceSetDialog();"> 
				<input type="button" id="cancelbutton" style="display: none;" class="btn_red" value="Cancel" onclick="hideServiceSetDialog();"> --> 
				<input type="button" class="btn_blue" value="Attach Service Set" onclick="attachServiceSets();">
			</div>
		</div>
	</div>
</div>

<div class="column">
<div class="portlet">
<div class="portlet-header "><s:text
	name="vcdmr.label.servicesetworkarea" /></div>
<div class="portlet-content">
<div id="assignDonorDiv" style="overflow-x: auto; overflow-y: hidden;">
<table cellspacing="0" border="0" id="servicedata" class="servicedata">
	<thead>
		<!-- <tr>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeGNLDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.CMSHCPCSCPTCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.CMSEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.PRSCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.MCAIDHCPCSCPTCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.MCAIDEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.cptcode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.BCBSHCPCSCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.BCBSEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.RevClass" /></th>
			<th colspan="1"><s:text name="vcdmr.label.RevClassDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.GLKey" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCPrice" /></th>
			<th colspan="1"><s:text name="vcdmr.label.prschrg" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCPriceEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeINACTDate" /></th>
		</tr>-->
	</thead>
	<tbody>
		<!-- Datat Table Data -->
	</tbody>
</table>
</div>
</div>
</div>
</div>
<!-- <div class="column" style="border: none;text-align: left">	
		<div  class="portlet" style="font-size: 11px;border: none;">
			<div class="portlet-content" style="border: none;">
				<div id="assignDonorDiv" style="overflow-x:auto;overflow-y:hidden;float: right;">
					<input type="button" class="staging_button" value="Submit" onclick="submitInCdmMain();" 	
					<input type="button" class="staging_button" value="Ok" onclick="closePopupWindow();" >
				</div>
			</div>
		</div>
	</div>--></div>
<!-- Apply For Group -->


<div id="createGroup"
	style="width: 100%; display: none; overflow: scroll; height: 550px;">
<div>
<div class="column" style="width: 40%; float: left;">
<div class="portlet">
<div class="portlet-header "><s:text
	name="vcdmr.label.listofavailablegrp" /></div>
<div class="portlet-content">
<div id="assignDonorDiv" style="overflow-x: auto; overflow-y: hidden;">
<div id="divInDialog">
<table class="gridtable" style="">
	<thead>
		<tr>
			<th colspan="1" style="width: 10%;">Level</th>
			<th colspan="1" style="width: 45%;">Name</th>
			<th colspan="1" style="width: 45%;">Create New</th>
		</tr>
	</thead>
	<tbody id="divInDialogTab" style="margin-top: 10px; width: 550px;">

	</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
<div class="column" style="width: 60%; float: right;">
<div class="portlet">
<div class="portlet-header "><s:text
	name="vcdmr.label.iteminselectedgrp" /></div>
<div class="portlet-content">
<div id="assignDonorDiv" style="overflow-x: auto; overflow-y: hidden;">
<table cellspacing="0" border="0" id="grpLink" class="grpLink">
	<thead>
		<!-- <tr>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeGNLDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.CMSHCPCSCPTCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.CMSEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.PRSCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.MCAIDHCPCSCPTCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.MCAIDEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.cptcode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.BCBSHCPCSCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.BCBSEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.RevClass" /></th>
			<th colspan="1"><s:text name="vcdmr.label.RevClassDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.GLKey" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCPrice" /></th>
			<th colspan="1"><s:text name="vcdmr.label.prschrg" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCPriceEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeINACTDate" /></th>
		</tr>-->
	</thead>
	<tbody id="sslink">
		<!-- Datat Table Data -->
	</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
<div class="column" style="text-align: left">
<div class="portlet" style="font-size: 11px;">
<div class="portlet-content">
<div id="assignDonorDiv" style="overflow-x: auto; overflow-y: hidden;">
<input type="text" name="grplevelname" id="grplevelname"
	style="display: none;"> <input type="button"
	id="crtsergrplevel" class="btn_blue" value="Create New Level"
	onclick="createMoreGroup();"> <input type="button"
	class="btn_red" value="Remove Last Level"
	onclick="removeonemorlevel();"> <input type="button"
	class="btn_blue" value="Attach Service Section"
	onclick="submitInCdmMain();"></div>
</div>
</div>
</div>

<div class="column">
<div class="portlet">
<div class="portlet-header "><s:text
	name="vcdmr.label.groupworkarea" /></div>
<div class="portlet-content">
<div id="assignDonorDiv" style="overflow-x: auto; overflow-y: hidden;">
<table cellspacing="0" border="0" id="groupdata" class="groupdata">
	<thead>
		<!-- <tr>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeGNLDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.CMSHCPCSCPTCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.CMSEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.PRSCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.MCAIDHCPCSCPTCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.MCAIDEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.cptcode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.BCBSHCPCSCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.BCBSEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.RevClass" /></th>
			<th colspan="1"><s:text name="vcdmr.label.RevClassDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.GLKey" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCPrice" /></th>
			<th colspan="1"><s:text name="vcdmr.label.prschrg" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCPriceEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeINACTDate" /></th>
		</tr>-->
	</thead>
	<tbody>
		<!-- Datat Table Data -->
	</tbody>
</table>
</div>
</div>
</div>
</div>
<!-- <div class="column" style="border: none;text-align: left">	
		<div  class="portlet" style="font-size: 11px;border: none;">
			<div class="portlet-content" style="border: none;">
				<div id="assignDonorDiv" style="overflow-x:auto;overflow-y:hidden;float: right;">
					<input type="button" class="staging_button" value="Submit" onclick="submitInCdmMain();" >	
					<input type="button" class="staging_button" value="Ok" onclick="closePopupWindow();" >
				</div>
			</div>
		</div>
	</div>--></div>


<!-- Apply For group close -->

<!-- Apply for service section edit -->
<div id="editServiceSection"
	style="width: 100%; display: none; overflow: scroll; height: 550px;">
<div>
<div class="column" style="width: 40%;">
<div class="portlet">
<div class="portlet-header "><s:text name="vcdmr.label.selectedSS" /></div>
<div class="portlet-content">
<div id="assignDonorDiv" style="overflow-x: auto; overflow-y: hidden;">
<div id="divInDialog"><input type="text" name="serviesectionName"
	id="serviesectionName" /></div>
</div>
</div>
</div>
</div>
<div class="column" style="width: 100%;">
<div class="portlet">
<div class="portlet-header "><s:text
	name="vcdmr.label.iteminselectedgrp" /></div>
<div class="portlet-content">
<div id="assignDonorDiv" style="overflow-x: auto; overflow-y: hidden;">
<table cellspacing="0" border="0" id="editGroupSectionTable"
	class="editGroupSectionTable">
	<thead>
		<!-- <tr>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeGNLDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.CMSHCPCSCPTCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.CMSEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.PRSCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.MCAIDHCPCSCPTCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.MCAIDEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.cptcode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.BCBSHCPCSCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.BCBSEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.RevClass" /></th>
			<th colspan="1"><s:text name="vcdmr.label.RevClassDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.GLKey" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCPrice" /></th>
			<th colspan="1"><s:text name="vcdmr.label.prschrg" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCPriceEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeINACTDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.DeleteServiceItem" /></th>
		</tr>-->
	</thead>
	<tbody id="sslink">
		<!-- Datat Table Data -->
	</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
<div class="column" style="text-align: left">
<div class="portlet" style="font-size: 11px;border: none;">
<div class="portlet-content" style="border: none;">
<div id="assignDonorDiv" style="overflow-x: auto; overflow-y: hidden;">
<input type="button" class="btn_red" value="Delete Service Section" onclick="completeDeleteServiceSec();">
<input type="button" class="btn_red" style="float: right;" value="Cancel" onclick="closePopUp();">
<input type="button" class="btn_green"  style="float: right;margin-right: 15px;"  value="Submit" onclick="updateCdmMain();"> 

</div>
</div>
</div>
</div>
</div>

<!-- Apply For ServiceSet Edit -->

<!-- Apply for service Set edit -->
<div id="editServiceSet"
	style="width: 100%; display: none; overflow: scroll; height: 550px;">
<div>
<div class="column" style="width: 40%;">
<div class="portlet">
<div class="portlet-header "><s:text
	name="vcdmr.label.selectedSet" /></div>
<div class="portlet-content">
<div id="assignDonorDiv" style="overflow-x: auto; overflow-y: hidden;">
<div id="divInDialog"><input type="text" name="serviesetName"
	id="serviesetName" /></div>
</div>
</div>
</div>
</div>
<div class="column" style="width: 100%;">
<div class="portlet">
<div class="portlet-header "><s:text
	name="vcdmr.label.iteminselectedset" /></div>
<div class="portlet-content">
<div id="assignDonorDiv" style="overflow-x: auto; overflow-y: hidden;">
<table cellspacing="0" border="0" id="editServiceSetTable"
	class="editServiceSetTable">
	<thead>
		<!-- <tr>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeGNLDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.CMSHCPCSCPTCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.CMSEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.PRSCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.MCAIDHCPCSCPTCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.MCAIDEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.cptcode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.BCBSHCPCSCode" /></th>
			<th colspan="1"><s:text name="vcdmr.label.BCBSEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.RevClass" /></th>
			<th colspan="1"><s:text name="vcdmr.label.RevClassDesc" /></th>
			<th colspan="1"><s:text name="vcdmr.label.GLKey" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCPrice" /></th>
			<th colspan="1"><s:text name="vcdmr.label.prschrg" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCPriceEffDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.SVCCodeINACTDate" /></th>
			<th colspan="1"><s:text name="vcdmr.label.DeleteServiceItem" /></th>
		</tr>-->
	</thead>
	<tbody id="sslink">
		<!-- Datat Table Data -->
	</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
<div class="column" style="text-align: left">
<div class="portlet" style="font-size: 11px; border: none;">
<div class="portlet-content" style="border: none;">
<div id="assignDonorDiv" style="overflow-x: auto; overflow-y: hidden;">
<input type="button" class="btn_red" value="Delete Service Set" onclick="completeDeleteServiceSet();"> 
<input type="button" style="float: right;" class="btn_red" value="Cancel" onclick="closePopUp();">
<input type="button" style="float: right;margin-right: 15px;" class="btn_green" value="Submit" onclick="updateSSLINKForSerSet();">

</div>
</div>
</div>
</div>
</div>
<div class="progress-indicator" style="display: none;"><img
	src="images/icon_loading_75x75.gif" alt="" /></div>
<!-- Advance Search Form
<div id="advanceSearch" style="width: 100%; display: none; overflow: scroll; height: 550px;">
	<div>
		
		<table cellspacing="0" border="0" id="advanceSearchTable"	class="editServiceSetTable" style="border-spacing: 10px;">
			<tbody id="sslink">
				<tr>
					<td><label>SVC CODE GNL DESC</label><div><input type="text" id="svcCodeGnlDesc"/></div></td>
					<td><label>CMS HCPCS/CPT CODE</label><div><input type="text" id="cmsHcpcsCptCode"/></div></td>
					<td><label>CMS EFF DATE</label><div><input type="text" id="cmsEffDate" class="datepicker_AS"/></div></td>
					<td><label>SVC CODE</label><div><input type="text" id="svcCode"/></div></td>
					<td><label>PRS CODE</label><div><input type="text" id="prsCode"/></div></td>
				</tr>
				<tr>
					<td><label>MCAID HCPCS/CPT CODE</label><div><input type="text" id="mcaidHcpcsCptCode"/></div></td>
					<td><label>MCAID EFF DATE</label><div><input type="text" id="mcaidEffDate" class="datepicker_AS"/></div></td>
					<td><label>CPT CODE</label><div><input type="text"/ id="cptCode"></div></td>
					<td><label>BCBS HCPCS/CPT CODE</label><div><input type="text" id="bcbsHcpcsCptCode"/></div></td>
					<td><label>BCBS EFF DATE</label><div><input type="text" id="bcbsEffDate" class="datepicker_AS"/></div></td>
				</tr>
				<tr>
					<td><label>REV CLASS</label><div><input type="text" id="revClass"/></div></td>
					<td><label>REV CLASS DESC</label><div><input type="text" id="revClassDesc"/></div></td>
					<td><label>GL KEY</label><div><input type="text" id="glKey"/></div></td>
					<td><label>SVC PRICE</label><div><input type="text" id="svcPrice"/></div></td>
					<td><label>PRS PRICE</label><div><input type="text" id="prsPrice"/></div></td>
				</tr>
				<tr>
					<td><label>PRICE EFF DATE</label><div><input type="text" id="priceEffDate" class="datepicker_AS"/></div></td>
					<td><label>SVC CODE INACT DATE</label><div><input type="text" id="svcCodeInactDate" class="datepicker_AS"/></div></td>
				</tr>
			</tbody>
		</table>
		
	</div>
	<div>
		<input type="button" value="Search" style="margin-left: 45%" onclick="advanceSearchFilterDataTable();"/>
	</div>
</div>
Advance Search Form End-->

<%
    String cdmtype = request.getParameter("cdmtype");
%>
<input type="hidden" id="cdmtype" value="<%=cdmtype %>"></input>



<script><!--
var oTable;
var checkdAll = new Array(); 
var ssLinkID = new Array(); 
var chkIndex=0;
var ssLinkIndex = 0
var arrayStr = "";
var serSetId;
var serLink;
var serData;
var serSet;
var dialog;
var index = 1;
var groupCode;
var grpLink;
var idholder;
var treeid = "asynctree";
var servSecFlag = false;
var servSetsFlag = false;
var editServSecId;
var deleteSerSec = new Array(); 
var editServiceSetId = "";
var deleteSerSet = new Array(); 
var deleteSerSetItmPk = new Array();
var esse, ess,test;
var eventHolder;
var currentView = 'List';
var chgmodulename,chggrpmodname,chgdelmodname;
var chgtype;
var chgstatval=1;
getModuleMain();


$(document).ready(function(){
	
	$.ajax({
		url:"getchgtype.action",
		type:"post",
		data:null,
		success:function(response){
		if(response.chrgType != ""){
				$("#charge_type_caption").show();	
				$("#charge_type_dropdown").show();
			$("#chgtype").html(response['chrgType']);
		   }
		
		}
	});
	
});
	

$(document).ready(function(){
	$.ajax({
		url:"getbannertext.action",
		type:"post",
		data:null,
		cache:false,
			success:function(response){
				
	      	  if(response.import_status=='1'&&response.export_status=='0'){
	      		$("#bannercontent").show();
	 				$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
				}
				else if(response.export_status=='1'&& response.import_status=='0'){
					$("#bannercontent").show();
					$("#bannercontent").html("Export Interface is in progress. System slowness may occur")
				}
				else if(response.import_status=='1' && response.export_status=='1'){
					$("#bannercontent").show();
					$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
					$("#bannercontent").append("<div style='background-color: yellow;'>Export Interface is in progress. System slowness may occur</div>");
				}
				else{
					$("#bannercontent").hide();
				}
	       }
	});
	 


	$.ajax({
		url:"loadVersionLimit.action",
		type:"post",
		data:{"FILE_ID":$("#cdmtype").val()},
		success:function(response){
			var option = "";
			for(var i=0; i< response.dataList.length; i++){
				if(i==0)
					option = option + '<option selected value="'+response.dataList[i].VERSION_ID+'">'+response.dataList[i].VERSION_DESC+'</option>'
				else
					option = option + '<option value="'+response.dataList[i].VERSION_ID+'">'+response.dataList[i].VERSION_DESC+'</option>'
			}
			$("#preprvver").html(option);
		}
	});
	$("#searchgrp").keyup(function(){

		// Retrieve the input field text and reset the count to zero
		var filter = $(this).val(), count = 0;
		// Loop through the comment list
		$("#treediv li").each(function(){

			// If the list item does not contain the text phrase fade it out
			if ($(this).text().search(new RegExp(filter, "i")) < 0) {
				$(this).hide();
				//$(this).attr('class', "expandable");
				$(this).children("div").attr('class', "hitarea expandable-hitarea");
				//$(this).closest("ul").css('display', "none");
			// Show the list item if the phrase matches and increase the count by 1
			} else {
				$(this).show();
				//$(this).attr('class', "collapsable");
				$(this).children("div").attr('class', "hitarea collapsable-hitarea");
				$(this).closest("ul").css('display', "block");
				count++;
			}
		});
		//$("#treediv ul:first-child").css('display', "block");
		// Update the count
		var numberItems = count;
		if(filter.length == 0){
			$("#treediv li div").each(function(){
				$(this).attr('class', "hitarea expandable-hitarea");
			});

			

			$("#treediv li ul").each(function(){
				$(this).css('display', "none");
			});
		}
			
		
	});
	
	loadTreeData();
	
	$(".ColVis_MasterButton span").html("");
	
	$("#servsetname").css('float','none');

	initilizeCheckBox();

	$.ajax({
		url:"getcurrentlibname.action",
		type:"post",
		data:{"EVENT_ID":$("#cdmtype").val()},
		success:function(response){
			var libsubMsg = "<b>Showing Results From:</b> ";
			$("#showLibraryName").html(libsubMsg + response.libName);
			
			if($("#cdmtype").val() == "ALLLIB"){
				$("#bysersec").attr('disabled', 'disabled').css('pointer-events', 'none');
				$("#byserset").attr('disabled', 'disabled').css('pointer-events', 'none');
				$("#createSerSecBut").attr('disabled', 'disabled').css('pointer-events', 'none');
				$("#createSerSetBut").attr('disabled', 'disabled').css('pointer-events', 'none');
				$("#preprvver").attr('disabled', 'disabled');
			}else{
				$("#bysersec").removeAttr('disabled').css('pointer-events', '');
				$("#byserset").removeAttr('disabled').css('pointer-events', '');
				$("#createSerSecBut").removeAttr('disabled').css('pointer-events', '');
				$("#createSerSetBut").removeAttr('disabled').css('pointer-events', '');
				$("#preprvver").removeAttr('disabled');
			}
		}
	})
});

function updateViewForType(value){
	checkdAll.length = 0;
	$("#maincdmData").dataTable().fnDestroy();
	$("#maincdmData").empty();
	drawDataTable();
}

function updateViewForChgStat(value){
	//
	chgstatval= value;
	checkdAll.length = 0;
	$("#maincdmData").dataTable().fnDestroy();
	$("#maincdmData").empty();
	drawDataTable();
	$("#chgstat_dropdown").val(chgstatval);
}
function updateViewForChgStatSec(value){
	chgstatval= value;
	checkdAll.length = 0;
	currentView = 'ServiceSection';
	$("#chgtype").attr('disabled', 'disabled');
	$("#forGroupView").css('width', '70%');
	$("#forTreeView").css('display', '');
	$("#forSetsView").css('display', 'none');
	 oTable = $("#maincdmData").dataTable({
	    	
	    	"bProcessing": false,
		    "bServerSide": true,
		    "sAjaxSource": "getcdmsearchgrpdata.action",
		    "fnServerParams": function ( aoData ) {
	    	     aoData.push( { "name": "grpID", "value": editServSecId}),
	    	     aoData.push( {"name": "chgtype", "value":  $("#chgtype").val()} );
				 aoData.push( {"name": "preprvver", "value":$("#preprvver").val()});
				 aoData.push( {"name": "advanceSearch", "value":  true });
				 aoData.push( {"name": "chgstat", "value":chgstatval});
				 aoData.push( {"name": "FILE_ID", "value":$("#cdmtype").val()});
				 
				 },
		     "bJQueryUI": true,
		    "bPaginate": true,
		    'iDisplayLength': 100,
		    "bLengthChange": true,
		    "bDestroy": true,
		    "bFilter": true,
		    "bSort": true,
		    "sScrollY": "200px",
		    "sScrollX": "100%",
		    "sScrollXInner":"100%",
		    "sDom": 'RC<"clear">frtlip',
		    "sPaginationType": "full_numbers",
		    "oColVis": {
		    	"bShowAll": true,
		    	"sShowAll": "Select All"
	        },
		    "aoColumns": fetchColumnProp(chgmodulename),

		    "fnDrawCallback": function () {
		    	$('#allcb').prop('checked', false);
		    	$('tbody tr td input[name="check"]').each(function(){
			    	if($.inArray($(this).val(), checkdAll) != -1){
			    		$(this).prop('checked', true);
			    		$('#allcb').prop('checked', true);
			    	}
		        });
		    	
		    }
	        
	        });
	 $(".ColVis_MasterButton span").html("");
	 //$('<select id="chgstat_dropdown" onchange="updateViewForChgStatSec(this.value)";><option value="1">Active  </option><option value="2">Inactive </option><option value="3">Both </option></select>').insertBefore('#maincdmData_filter label');
	 $('<select id="chgstat_dropdown" onchange="updateViewForChgStatSec(this.value)";><option value="1">Active  </option><option value="2">Inactive </option><option value="3">Both </option></select>').insertBefore('div.inner-addon input:first');
	    $('<a class="addFilter" href="#" onclick="showAddFilterform();">Add Filters</a>').insertAfter('#maincdmData_filter');
	    $('<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>').insertAfter('#maincdmData_filter');
		
		 initilizeCheckBox();
		 $("#chgstat_dropdown").val(chgstatval);
}
function updateViewForChgStatSet(value){
	chgstatval= value;
	$("#maincdmData").dataTable().fnDestroy();
	$("#maincdmData").empty();
	oTable = $("#maincdmData").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getsslinkdata.action",
	    "fnServerParams": function ( aoData ) {
			aoData.push( { "name": "ssID", "value": serSetId},{ "name": "viewtype", "value": "SS"});
			aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
			aoData.push({"name": "chgstat", "value":chgstatval});
						
	     },
	     "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bLengthChange": true,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "sScrollY": "200px",
	    "sScrollX": "100%",
	    "sScrollXInner":"100%",
	    "sDom": 'RC<"clear">frtlip',
	    "sPaginationType": "full_numbers",
	    "oColVis": {
	    	"bShowAll": true,
	    	"sShowAll": "Select All"
        },
	    "aoColumns": fetchColumnProp(chgmodulename),
	    "fnDrawCallback": function () {
	    	$('#allcb').prop('checked', false);
	    	$('tbody tr td input[name="check"]').each(function(){
		    	var v = $(this).val().split(',');
		    	if(($.inArray(v[0], checkdAll) != -1) && ($.inArray(v[1], ssLinkID) != -1)){
		    		$(this).prop('checked', true);
		    		$('#allcb').prop('checked', true);
		    	}
	        });
	    	
	    }
	});
	//$('<select id="chgstat_dropdown" onchange="updateViewForChgStatSet(this.value)";><option value="1">Active  </option><option value="2">Inactive </option><option value="3">Both </option></select>').insertBefore('#maincdmData_filter label');
	$('<select id="chgstat_dropdown" onchange="updateViewForChgStatSet(this.value)";><option value="1">Active  </option><option value="2">Inactive </option><option value="3">Both </option></select>').insertBefore('#maincdmData_filter label div.inner-addon input:first');
	$('<a class="addFilter" href="#" onclick="showAddFilterform();">Add Filters</a>').insertAfter('#maincdmData_filter');
    $('<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>').insertAfter('#maincdmData_filter');
	$(".ColVis_MasterButton span").html("");
	initilizeCheckBox();
	$("#chgstat_dropdown").val(chgstatval);
}

function closePopUp(){
	deleteSerSec.length = 0;
	deleteSerSet.length = 0;
	$.unblockUI(); 
}

$(".datepicker_AS").datepicker({ dateFormat: 'dd-M-yy',changeMonth: true,
    changeYear: true,yearRange: 'c-115:c+100', });
$('#ui-datepicker-div').addClass('blockMsg');

function updateViewForVersion(value){
	
	$.ajax({
		url:"checkForVersionChange.action",
		type:"post",
		data:{"versionid":value},
		async:false,
		success:function(response){
		
			if(response.checkCurrentVersion == false){
				$("#createSerSecBut").attr('disabled', 'disabled');
				$("#createSerSetBut").attr('disabled', 'disabled');
				$("#EditSerSec").attr('disabled', 'disabled');
				$("#editSerSetBut").attr('disabled', 'disabled');
				$("#byserset").attr('disabled', 'disabled');
				$("#bysersec").attr('disabled', 'disabled');
				$("#bylist").attr('checked', 'checked');
				loadListView();
				
			}else{
				$("#createSerSecBut").removeAttr('disabled');
				$("#createSerSetBut").removeAttr('disabled');
				$("#EditSerSec").removeAttr('disabled');
				$("#editSerSetBut").removeAttr('disabled');
				$("#byserset").removeAttr('disabled');
				$("#bysersec").removeAttr('disabled');
				$("#byserset").removeAttr('checked');
				$("#bysersec").removeAttr('checked');
				$("#bylist").attr('checked', 'checked');
				drawDataTable();
			}
		}
	});
	//drawDataTable();
}
function fetchColumnProp(modulname){
	
	var column;
	$.ajax({
		url:'fetchColumn.action',
		type:'post',
		async: false,
		 data: {
	            "MODULENAME": modulname
	        },
		success:function(response){
	        column=response['colData'];
		}
	});	
	
	return column;
}
function getModuleMain(){

	$.ajax({
		url:'getModuleMain.action',
		type:'post',
		async: false,
		 data:{"FILE_ID":$("#cdmtype").val()},
		success:function(response){
	        chgmodulename = response.modNameMain;
	        }
	});	
}
function getModuleGrp(){
	$.ajax({
		url:'getModuleGrp.action',
		type:'post',
		async: false,
		 data:{"FILE_ID":$("#cdmtype").val()},
		success:function(response){
	            chggrpmodname = response.modNameGrp;
	     
	    }
	});	

	
}
function getModuleEdit(){
	$.ajax({
		url:'getModuleEdit.action',
		type:'post',
		async: false,
		 data:{"FILE_ID":$("#cdmtype").val()},
		success:function(response){
	          chgdelmodname = response.modNamedel;

	    }
	});	

	
}
var oSettings = {
		
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getcdmsearchdata.action",
	    "fnServerParams": function ( aoData ) {
	         aoData.push( { "name": "chgtype", "value":  $("#chgtype").val()} );
			 aoData.push({"name": "preprvver", "value":$("#preprvver").val()});
			 aoData.push({"name": "chgstat", "value":chgstatval});
			 aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
			 aoData.push( {"name":"filterValues","value":JSON.stringify(selectedValues)});
			 aoData.push( {"name":"savedfilters","value":JSON.stringify(savedfiltercolvals)});
			 
	     },
	     "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bLengthChange": true,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "sScrollY": "400px",
	    "sScrollX": "100%",
	    "sScrollXInner":"100%",
	    "sDom": 'RC<"clear">frtlip',
	    "sPaginationType": "full_numbers",
	    "oColVis": {
	    	"bShowAll": true,
	    	"sShowAll": "Select All"
        },
	    "aoColumns": "",
	    "fnDrawCallback": function () {
	    	$('#allcb').prop('checked', false);
	    	$('tbody tr td input[name="check"]').each(function(){
		    	if($.inArray($(this).val(), checkdAll) != -1){
		    		$(this).prop('checked', true);
		    		$('#allcb').prop('checked', true);
		    	}
	        });
	    	
	    }
	};

var advanceSearch = {
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getcdmsearchdata.action",
	    "fnServerParams": function ( aoData ) {
	  		/* aoData.push( { "name": "SERVICE_DESC2_AS", "value":  $("#svcCodeGnlDesc").val()} );
			 aoData.push({"name": "CMS_HCPCS_CPT_AS", "value":$("#cmsHcpcsCptCode").val()});
			 aoData.push( { "name": "CMS_HCPCS_CPT_EFF_DATE_AS", "value":  $("#cmsEffDate").val()} );
			 aoData.push({"name": "SERVICE_CODE_AS", "value":$("#svcCode").val()});
			 aoData.push( { "name": "PRS_CODE_AS", "value":  $("#prsCode").val()} );
			 aoData.push({"name": "MCAID_HCPCS_CPT_AS", "value":$("#mcaidHcpcsCptCode").val()});
			 aoData.push( { "name": "MCAID_HCPCS_CPT_EFF_DATE_AS", "value":  $("#mcaidEffDate").val()} );
			 aoData.push({"name": "CPT_CODE_AS", "value":$("#cptCode").val()});
			 aoData.push( { "name": "BLUE_CROSS_HCPCS_CPT_AS", "value":  $("#bcbsHcpcsCptCode").val()} );
			 aoData.push({"name": "BLUE_CROSS_HCPCS_CPT_EFF_DATE_AS", "value":$("#bcbsEffDate").val()});
			 aoData.push( { "name": "REV_CLASS_AS", "value":  $("#revClass").val()} );
			 aoData.push({"name": "CUSTOM_DESC1_AS", "value":$("#revClassDesc").val()});
			 aoData.push( { "name": "REV_CODE_SA", "value":  $("#glKey").val()} );
			 aoData.push({"name": "HCC_TECH_CHG_AS", "value":$("#svcPrice").val()});
			 aoData.push( { "name": "PRS_PHY_CHG_AS", "value":  $("#prsPrice").val()} );
			 aoData.push({"name": "EFF_FROM_DATE_AS", "value":$("#priceEffDate").val()});
			 aoData.push( { "name": "EFF_TO_DATE_AS", "value":  $("#svcCodeInactDate").val()} );*/
			 aoData.push( {"name": "chgtype", "value":  $("#chgtype").val()} );
			 aoData.push( {"name": "preprvver", "value":$("#preprvver").val()});
			 aoData.push( {"name": "advanceSearch", "value":  true });
			 aoData.push( {"name": "chgstat", "value":chgstatval});
			 aoData.push( {"name": "FILE_ID", "value":$("#cdmtype").val()});
			 aoData.push( {"name":"filterValues","value":JSON.stringify(selectedValues)});
			 aoData.push( {"name":"savedfilters","value":JSON.stringify(savedfiltercolvals)});
			 			 
	     },
	     "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bLengthChange": true,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "sScrollY": "230px",
	    "sScrollX": "100%",
	    "sScrollXInner":"100%",
	    "sDom": 'RC<"clear">frtlip',
	    "sPaginationType": "full_numbers",
	    "oColVis": {
	    	"bShowAll": true,
	    	"sShowAll": "Select All"
        },
	    "aoColumns":fetchColumnProp(chgmodulename),

	    "fnDrawCallback": function () {
	    	$('#allcb').prop('checked', false);
	    	$('tbody tr td input[name="check"]').each(function(){
		    	if($.inArray($(this).val(), checkdAll) != -1){
		    		$(this).prop('checked', true);
		    		$('#allcb').prop('checked', true);
		    	}
	        });
	    	
	    }
	};

$(function() {

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});

	/**For Notes**/
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

	/* $( ".portlet-header .ui-addnew" ).click(function() {
		showPopUp("open","addNewDialog","Add New Acquision","900","450")
	}); */
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");
	$( ".portlet-header .ui-reset " ).click(function() {
		var formName = document.getElementById("login");
		//clearForm(formName);
		clear();
	});

// 	$( ".portlet-header .ui-notes " ).click(function() {
// 		showNotes('open');
// 	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});


	

});

$(function() {

	$("#effdate").datepicker({ dateFormat: 'dd-M-yy', changeMonth: true,
	    changeYear: true  });
    
	  $.ajax({
			url:"getgroupnameautocomplete.action",
			type:"post",
			data:{level:1},
			success:function(response){
				var dataList = response.dataList;
				var option = '<option value="">Select Group L1</option>';
				for(var i=0; i<dataList.length;i++){
					option += '<option value="'+dataList[i].ID+'">'+dataList[i].VALUE+'</option>';
						
				}

				$("#grplev1").html(option);
	  		}
		});

	  drawDataTable();
});

function hideServiceSetDialog(){

	$("#servsetname").hide();
	$("#cancelbutton").css('display','none');
	$("#crtserset").val("Add New Service Set");
	$("#crtserset").removeClass('btn_green').addClass('btn_blue');
	$("#crtserset").css('float','right');
}

function createServiceSetDialog(){
	if($("#crtserset").val() == "Add New Service Set"){
		$("#servsetname").removeAttr('style');
		$("#cancelbutton").removeAttr('style');
		$("#servsetname").css('float', 'none');
		$("#crtserset").val("Submit");
		$("#crtserset").removeClass('btn_blue').addClass('btn_green');
		$("#crtserset").css('float','');
		return;
	}
	
	if($("#crtserset").val() == "Submit"){
		if($("#servsetname").val() == ""){
			alertify.alert("Please Enter Service Set Name.");
			return;
		}
		$.ajax({
			url:"createserviceset.action",
			type:"post",
			data:{"servsetname":$("#servsetname").val(),"FILE_ID":$("#cdmtype").val()},
			success:function(response){
			
				if(response.checkForExisting == false){
					alertify.alert('Service Set "'+$("#servsetname").val()+'" already exists.');
					return;
				}
				$("#crtserset").val("Add New Service Set");
				$("#crtserset").removeClass('btn_green').addClass('btn_blue');
				$("#servsetname").css('display','none');
				$("#cancelbutton").css('display','none');
				$("#crtserset").css('float','right');
				refreshServiceSet();
				alertify.alert("Created Successfully.");
			}
		});
	}
}

function refreshServiceSet(){
	//$("#servSets").empty();
	serSet.dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getserviceset.action",
	    "fnServerParams": function ( aoData ) {
	         aoData.push({ "name": "checkedId", "value":  ""});
	         aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
	     },
	     "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bLengthChange": false,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": false,
	    "sScrollY": "160px",
	    "sDom": 'RC<"clear">lfrtip',
	    "aoColumns": [
				{  "sWidth":"50px",  "sClass":"checkall", "mDataProp":"check","bSortable":false},
				{  "sWidth":"220px", "sClass":"sersets", "mDataProp":"sersets"},
					      			
	      	    ],
	      	  "fnDrawCallback": function() {
	    	    	$("#servSets .checkall input").each(function(){
	    	    		if($(this).is(':checked')){
	    	    			var nextTD = $(this).closest("td").next();
	    	    			serSetId = $(this).val();
	    	    			getServiceSetValue(serSetId);
	    	    		}
	    	    	});
	         	    }
	});
	$("#servSets_wrapper .ColVis_MasterButton").hide();
	$(".ColVis_MasterButton span").html("");
}

function createMoreGroup(){
	
	if($("#crtsergrplevel").val() == "Create New Level"){
		$("#grplevelname").removeAttr('style');
		$("#grplevelname").css('float', 'none');
		$("#crtsergrplevel").val("Submit");
		return;
	}

	if($("#crtsergrplevel").val() == "Submit"){
		if($("#grplevelname").val() == ""){
			alertify.alert("Please Enter Service Section.");
			return;
		}
		$.ajax({
			url:"selectedgrupadd.action",
			type:"post",
			data:{grpName:$("#grplevelname").val(),pkID:groupCode,flag:false,"FILE_ID":$("#cdmtype").val()},
			success:function(success){
				$("#crtsergrplevel").val("Create New Level");
				$("#grplevelname").css('display','none');
				alertify.alert("New Service Section Created Successfully.");
			}
		});
	}

   
}

function removeonemorlevel(){
	
	
	groupCode=$("#"+(index-1)).val();
	if(groupCode == ""){
		alertify.alert("Please Select Service Section.");
		return;
	}
	
	var r = alertify.confirm("Do you want to remove the last level?", function(r){

		if(r){
			$.ajax({
				url:"selectedgrupdelete.action",
				type:"post",
				data:{pkID:groupCode},
				success:function(response){
					if(response.checkData == true){
						alertify.alert("Service Section Deleted Successfully.");
						removelastlevel();
					}else{
						alertify.alert("The service section was not deleted as it is attached to some CDM item.");
					}
				}
		  });
		}else{
		}
	return;
	});
	


	
	/*$.ajax({
		url:"selectedgrupdelete.action",
		type:"post",
		data:{pkID:groupCode},
		success:function(response){
			refreshTable(index-1);
			alert("Group Removed Successfully.");
			//removelastlevel();
			
			
		}
  });*/
}

function refreshTable(id){

	var parentid;
	 var grupName=$("#te"+id).val();
	 var v = $("#" + (id-1)).val();
	 if(v == "" || v == 'undefine' || v == null || v == " "){
		parentid = 0;
	 }else{
		 parentid = v;
	 }
	 
	$.ajax({
		url:"getgropus.action",
		type:"post",
		data:{parentNode:parentid,"FILE_ID":$("#cdmtype").val()},
		success:function(response){
			var data = response['groups'];
			var option = '<option value="">Select Value</option>';
			
			if(data.length > 0){
				for (var i = 0; i < data.length; i++) {
					option += '<option value="'+data[i][0]+'">'+data[i][2]+'</option>';
				}
			}else{
				removelastlevel();
			}
			
			$("#"+id).html(option);
		}
	});
}

function getGroupLev(pkId, level){
	$.ajax({
		url:"getgroupnameautocomplete.action",
		type:"post",
		data:{pkGrpID:pkId, level:level},
		success:function(response){
			var dataList = response.dataList;
			var option = '<option value="">Select Service Section Level</option>';
			for(var i=0; i<dataList.length;i++){
				option += '<option value="'+dataList[i].ID+'">'+dataList[i].VALUE+'</option>';
					
			}
			if(level=="2"){
				$("#grplev2").removeAttr("disabled");
				$("#grplev2").html(option);
				}
				
			if(level == "3"){
				$("#grplev3").removeAttr("disabled");
				$("#grplev3").html(option);
				}
				
			
  		}
	});
}

function attachServiceSets(){
	servSetsFlag = true;


	/*$.ajax({
		url:"checkforexistingitems.action",
		type:"post",
		data:{"ids":arrayStr, "serSetId":serSetId},
		success:function(response){
			if(response.checkExisting == true){
				var r = alertify.confirm("Few Or All Service Item(s) already attached in this Service Set. Do you want to add again?", function(r){

					if(r){
						$.ajax({
							url:"attachserviceset.action",
							type:"post",
							data:{"ids":arrayStr, "serSetId":serSetId},
							success:function(response){
								getServiceSetValue(serSetId);
								alertify.alert("Attached Successfully.");
							}
						});
					}else{
						
					}
				return;
				});
			}
		}
	});*/


	$.ajax({
		url:"attachserviceset.action",
		type:"post",
		data:{"ids":arrayStr,"serSetId":serSetId,"FILE_ID":$("#cdmtype").val()},
		success:function(response){
			getServiceSetValue(serSetId);
			alertify.alert("Attached Successfully.");
		}
	});
	
	
}

function getSearcgData(){
	oTable = $("#maincdmData").dataTable(oSettings);
	initilizeCheckBox();
}

function drawDataTable(){

       var col=fetchColumnProp(chgmodulename);
		oSettings.aoColumns=col;
		oTable = $("#maincdmData").dataTable(oSettings);

		//$('<select id="chgstat_dropdown" onchange="updateViewForChgStat(this.value);"><option value="1">Active  </option><option value="2">Inactive  </option><option value="3">Both  </option></select>').insertBefore('#maincdmData_filter label');
		$('<select id="chgstat_dropdown" onchange="updateViewForChgStat(this.value);"><option value="1">Active  </option><option value="2">Inactive  </option><option value="3">Both  </option></select>').insertBefore('div.inner-addon input:first');
	    $('<a class ="addFilter" href="#" onclick="showAddFilterform();">Add Filters</a>').insertAfter('#maincdmData_filter');
	    $('<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>').insertAfter('#maincdmData_filter');
	    $(".ColVis_MasterButton span").html("");
		initilizeCheckBox(); 	   

}

function blockUIClose(){
	//loadTreeData();
	deleteSerSec.length = 0;
	deleteSerSet.length = 0;
	
	if($('#bysersec').is(':checked')){
		loadGroupView();
	}

	if($('#byserset').is(':checked')){
		loadServiceSetView();
	}	
	$.unblockUI(); 
	index=1;
	
	$("#crtsergrplevel").val("Create New Level");
	$("#grplevelname").css('display','none');
	
	
	if(servSecFlag){
		checkdAll.length = 0;
		$('#allcb').prop('checked', false);
		
		$('tbody tr td input[type="checkbox"]').each(function(){
	        $(this).prop('checked', false);
	        if($.inArray($(this).val(), checkdAll) != -1){
	        	checkdAll.splice($.inArray($(this).val(), checkdAll), 1);
	        	
	    		return;
	        }
	    });
	}
	if(servSetsFlag){
		checkdAll.length = 0;
		$('#allcb').prop('checked', false);
		
		$('tbody tr td input[type="checkbox"]').each(function(){
	        $(this).prop('checked', false);
	        if($.inArray($(this).val(), checkdAll) != -1){
	        	checkdAll.splice($.inArray($(this).val(), checkdAll), 1);
	        	
	    		return;
	        }
	    });
	}
	
}

function submitInCdmMain(){
	 var arrayStr = "";
	 servSecFlag = true;
		
   if(checkdAll.length != 0){
   	
   	checkdAll = checkdAll.filter(function(n){ return n != undefined });
   	checkdAll = checkdAll.filter(function(n){ return n != 'on' });
   	
   	
				for(var i=0; i<checkdAll.length; i++){
					arrayStr += checkdAll[i] + ",";
	        }
}	
   
   if($("#"+idholder).val()==""){
   		grupCode=$("#"+(idholder-1)).val();
   }
   else{
   	 	grupCode=$("#"+(idholder)).val();
    }


   
   if(grupCode==""||grupCode==undefined){
         alertify.alert("Please Select Service Section.");
         return;
   }
   
	$.ajax({
		url:"updateGroupCDMMain.action",
		type:"post",
		//data:{pkString:arrayStr,grupCode:groupCode},
		data:{pkString:arrayStr,grupCode:grupCode},
		success:function(response){
			alertify.alert("Service Section Updated Successfully.");
			updateAfterAtachServiceSection(grupCode);
			blockUIClose();
			destroyOnclose2();
			//groupCode="";
			//drawDataTable();
			
		}
 });
}
function destroyOnclose(){
	$("#servSets").dataTable().fnDestroy();
	$("#servicedata").dataTable().fnDestroy();
	$("#servLink").dataTable().fnDestroy();
}
function destroyOnclose2(){
	$("#groupdata").dataTable().fnDestroy();
	$("#grpLink").dataTable().fnDestroy();
	
}
function initilizeCheckBox(){
	checkdAll.length = 0;
	$('#allcb').change(function(){

	    if($(this).prop('checked')){
	        $('tbody tr td input[type="checkbox"]').each(function(){
	            $(this).prop('checked', true);
	           
	            if((($(this).val()).split(',')).length > 1){

	            	/*if($.inArray($(this).val().split(',')[0], checkdAll) != -1){
		            	checkdAll.splice($.inArray($(this).val().split(',')[0], checkdAll), 1);
		            	chkIndex--;
		            }*/
		            if($(this).is(":checked")){
		            	checkdAll[chkIndex] = $(this).val().split(',')[0];
			            ssLinkID[ssLinkIndex] = $(this).val().split(',')[1];
			            ssLinkIndex++;
			            chkIndex++;
		            }else{
		            	checkdAll.splice($.inArray($(this).val().split(',')[0], checkdAll), 1);
		            	chkIndex--;
		            }
		            
		            
	            }else{
	            	if($.inArray($(this).val(), checkdAll) != -1){
		            	checkdAll.splice($.inArray($(this).val(), checkdAll), 1);
		            	chkIndex--;
		            }
		            checkdAll[chkIndex] = $(this).val();
		            chkIndex++;
		        }
	            
	            
	        });
	    }else{
	        $('tbody tr td input[type="checkbox"]').each(function(){
	            $(this).prop('checked', false);

				
	            if((($(this).val()).split(',')).length > 1){

	            	if($.inArray($(this).val().split(',')[0], checkdAll) != -1){
		            	checkdAll.splice($.inArray($(this).val().split(',')[0], checkdAll), 1);
		            	chkIndex--;
		            }
	            	if($.inArray($(this).val().split(',')[1], ssLinkID) != -1){
	            		ssLinkID.splice($.inArray($(this).val().split(',')[1], ssLinkID), 1);
	            		ssLinkIndex--;
		            }
		            
	            }else{
	            	 
		            if($.inArray($(this).val(), checkdAll) != -1){
		            	checkdAll.splice($.inArray($(this).val(), checkdAll), 1);
		            	chkIndex--;
		        		return;
		            }
		        }

	           
	        });
	    }
	});
}


function getServiceSetValue(id){
	serSetId = id;
	$("#servLink").empty();
	serLink.dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getsslinkdataforedit.action",
	    "fnServerParams": function ( aoData ) {
			aoData.push( { "name": "ssID", "value": serSetId});
			aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
	     },
	    "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "sScrollY": "150px",
	    "sScrollX": "100%",
	    "sScrollXInner":"100%",
	    "bLengthChange": true,
	    "sDom": 'RC<"clear">frtlip',
	    "sPaginationType": "full_numbers",
	    "oColVis": {
	    	"bShowAll": true,
	    	"sShowAll": "Select All"
        },
	    "aoColumns": fetchColumnProp(chggrpmodname)
	    /*[
	      				{"sWidth":"200px","mDataProp":"SERVICE_DESC1", "bSortable":true,"sTitle": "Service Description"},
	      				{"sWidth":"200px","mDataProp":"SERVICE_DESC2", "bSortable":true,"sTitle": "SVC Code Gnl Desc"},
	      				{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT", "bSortable":true,"sTitle": "CMS HCPCS/CPT Code"},
	      				{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "CMS Eff Date"},
	      				{"sWidth":"100px", "mDataProp":"SERVICE_CODE", "bSortable":true,"sTitle": "SVC Code"},
	      				{"sWidth":"100px","mDataProp":"PRS_CODE", "bSortable":true,"sTitle": "PRS Code"},
	      				{"sWidth":"100px", "mDataProp":"MCAID_HCPCS_CPT", "bSortable":true,"sTitle": "MCAID HCPCS/CPT Code"},
	      				{"sWidth":"100px","mDataProp":"MCAID_HCPCS_CPT_EFF_DATE","bSortable":true,"sTitle": "MCAID Eff Date"},
	      				{"sWidth":"100px","mDataProp":"CPT_CODE", "bSortable":true,"sTitle": "CPT Code"},
	      				{"sWidth":"100px","mDataProp":"BLUE_CROSS_HCPCS_CPT", "bSortable":true,"sTitle": "BCBS HCPCS/CPT Code"},
	      				{"sWidth":"100px", "mDataProp":"BLUE_CROSS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "BCBS Eff Date"},
	      				{"sWidth":"100px","mDataProp":"REV_CLASS", "bSortable":true, "sClass" : "centerAlign","sTitle": "REV Class"},
	      				{"sWidth":"200px", "mDataProp":"CUSTOM_DESC1", "bSortable":true,"sTitle": "REV Class Desc"},
	      				{"sWidth":"100px", "mDataProp":"REV_CODE", "bSortable":true, "sClass" : "centerAlign","sTitle": "GL Key"},
	      				{"sWidth":"100px", "mDataProp":"HCC_TECH_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "SVC Price"},
	      				{"sWidth":"100px", "mDataProp":"PRS_PHY_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "PRS Price"},
	      				{"sWidth":"100px","mDataProp":"EFF_FROM_DATE", "bSortable":true, "sTitle": "Price Eff Date"},
	      				{"sWidth":"100px", "mDataProp":"EFF_TO_DATE", "bSortable":true, "sTitle":"SVC Code Inact Date"}
	      		    ]*/
	});
	
	$(".ColVis_MasterButton span").html("");
}

function openCreateSerSet(){
	
	getModuleGrp();
	var col=fetchColumnProp(chggrpmodname);

	servSetsFlag = false;
	checkdAll = checkdAll.filter(function(n){ return n != undefined });
	checkdAll = checkdAll.filter(function(n){ return n != 'on' });
    
	if(checkdAll.length == 0){
		alertify.alert("Please Select Items.");
		return;
	}

	tempStr = "";
	if(ssLinkID.length != 0){
		ssLinkID = ssLinkID.filter(function(n){ return n != undefined });
		ssLinkID = ssLinkID.filter(function(n){ return n != 'on' });
		for(var i=0; i<ssLinkID.length; i++){
			tempStr += ssLinkID[i] + ",";
		}
	}
	
	arrayStr = "";
	for(var i=0; i<checkdAll.length; i++){
		arrayStr += checkdAll[i] + ",";
	}
	//$("#servSets").empty();
	serSet = $("#servSets").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getserviceset.action",
	    "fnServerParams": function ( aoData ) {
	         aoData.push({ "name": "checkedId", "value":  ""});
	         aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
	     },
	     "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bLengthChange": false,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "sScrollY": "160px",
	    "sDom": 'RC<"clear">lfrtip',
	    "aoColumns": [
				{  "sWidth":"50px",  "sClass":"checkall", "mDataProp":"check","bSortable":false},
				{  "sWidth":"220px", "sClass":"sersets", "mDataProp":"sersets", "bSortable":true},
					      			
	      	    ],
	      	  "fnDrawCallback": function() {
	    	    	$("#servSets .checkall input").each(function(){
	    	    		if($(this).is(':checked')){
	    	    			var nextTD = $(this).closest("td").next();
	    	    			getServiceSetValue($(this).val(),col);
	    	    		}
	    	    	});
	         	    }
	});

	$("#servSets_wrapper .ColVis_MasterButton").hide();
	
	
	$("#servicedata").empty();
	serData = $("#servicedata").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getservicesetdata.action",
	    "fnServerParams": function ( aoData ) {
	         aoData.push( { "name": "ids", "value": arrayStr});
	         aoData.push( { "name": "tempids", "value": tempStr});
	         aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
	     },
	     "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bLengthChange": true,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "sScrollY": "200px",
	    "sScrollX": "100%",
	    "sScrollXInner":"100%",
	    "sDom": 'RC<"clear">frtlip',
	    "sPaginationType": "full_numbers",
	    "oColVis": {
	    	"bShowAll": true,
	    	"sShowAll": "Select All"
        },
	    "aoColumns": col
		    /*[
	      				{"sWidth":"200px","mDataProp":"SERVICE_DESC1", "bSortable":true,"sTitle": "Service Description"},
	      				{"sWidth":"200px","mDataProp":"SERVICE_DESC2", "bSortable":true,"sTitle": "SVC Code Gnl Desc"},
	      				{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT", "bSortable":true,"sTitle": "CMS HCPCS/CPT Code"},
	      				{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "CMS Eff Date"},
	      				{"sWidth":"100px", "mDataProp":"SERVICE_CODE", "bSortable":true,"sTitle": "SVC Code"},
	      				{"sWidth":"100px","mDataProp":"PRS_CODE", "bSortable":true,"sTitle": "PRS Code"},
	      				{"sWidth":"100px", "mDataProp":"MCAID_HCPCS_CPT", "bSortable":true,"sTitle": "MCAID HCPCS/CPT Code"},
	      				{"sWidth":"100px","mDataProp":"MCAID_HCPCS_CPT_EFF_DATE","bSortable":true,"sTitle": "MCAID Eff Date"},
	      				{"sWidth":"100px","mDataProp":"CPT_CODE", "bSortable":true,"sTitle": "CPT Code"},
	      				{"sWidth":"100px","mDataProp":"BLUE_CROSS_HCPCS_CPT", "bSortable":true,"sTitle": "BCBS HCPCS/CPT Code"},
	      				{"sWidth":"100px", "mDataProp":"BLUE_CROSS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "BCBS Eff Date"},
	      				{"sWidth":"100px","mDataProp":"REV_CLASS", "bSortable":true, "sClass" : "centerAlign","sTitle": "REV Class"},
	      				{"sWidth":"200px", "mDataProp":"CUSTOM_DESC1", "bSortable":true,"sTitle": "REV Class Desc"},
	      				{"sWidth":"100px", "mDataProp":"REV_CODE", "bSortable":true, "sClass" : "centerAlign","sTitle": "GL Key"},
	      				{"sWidth":"100px", "mDataProp":"HCC_TECH_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "SVC Price"},
	      				{"sWidth":"100px", "mDataProp":"PRS_PHY_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "PRS Price"},
	      				{"sWidth":"100px","mDataProp":"EFF_FROM_DATE", "bSortable":true, "sTitle": "Price Eff Date"},
	      				{"sWidth":"100px", "mDataProp":"EFF_TO_DATE", "bSortable":true, "sTitle":"SVC Code Inact Date"}
	      		    ]*/
	});

	$("#servLink").empty();
	serLink = $("#servLink").dataTable({
	     "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "sScrollY": "150px",
	    "sScrollX": "100%",
	    "sScrollXInner":"100%",
	    "sDom": 'RC<"clear">frtlip',
	    "sPaginationType": "full_numbers",
	    "bLengthChange": true,
	    "oColVis": {
	    	"bShowAll": true,
	    	"sShowAll": "Select All"
        },
	    "aoColumns":col
	    /* [
	      				{"sWidth":"200px","mDataProp":"SERVICE_DESC1", "bSortable":true,"sTitle": "Service Description"},
	      				{"sWidth":"200px","mDataProp":"SERVICE_DESC2", "bSortable":true,"sTitle": "SVC Code Gnl Desc"},
	      				{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT", "bSortable":true,"sTitle": "CMS HCPCS/CPT Code"},
	      				{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "CMS Eff Date"},
	      				{"sWidth":"100px", "mDataProp":"SERVICE_CODE", "bSortable":true,"sTitle": "SVC Code"},
	      				{"sWidth":"100px","mDataProp":"PRS_CODE", "bSortable":true,"sTitle": "PRS Code"},
	      				{"sWidth":"100px", "mDataProp":"MCAID_HCPCS_CPT", "bSortable":true,"sTitle": "MCAID HCPCS/CPT Code"},
	      				{"sWidth":"100px","mDataProp":"MCAID_HCPCS_CPT_EFF_DATE","bSortable":true,"sTitle": "MCAID Eff Date"},
	      				{"sWidth":"100px","mDataProp":"CPT_CODE", "bSortable":true,"sTitle": "CPT Code"},
	      				{"sWidth":"100px","mDataProp":"BLUE_CROSS_HCPCS_CPT", "bSortable":true,"sTitle": "BCBS HCPCS/CPT Code"},
	      				{"sWidth":"100px", "mDataProp":"BLUE_CROSS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "BCBS Eff Date"},
	      				{"sWidth":"100px","mDataProp":"REV_CLASS", "bSortable":true, "sClass" : "centerAlign","sTitle": "REV Class"},
	      				{"sWidth":"200px", "mDataProp":"CUSTOM_DESC1", "bSortable":true,"sTitle": "REV Class Desc"},
	      				{"sWidth":"100px", "mDataProp":"REV_CODE", "bSortable":true, "sClass" : "centerAlign","sTitle": "GL Key"},
	      				{"sWidth":"100px", "mDataProp":"HCC_TECH_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "SVC Price"},
	      				{"sWidth":"100px", "mDataProp":"PRS_PHY_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "PRS Price"},
	      				{"sWidth":"100px","mDataProp":"EFF_FROM_DATE", "bSortable":true, "sTitle": "Price Eff Date"},
	      				{"sWidth":"100px", "mDataProp":"EFF_TO_DATE", "bSortable":true, "sTitle":"SVC Code Inact Date"}
	      		    ]*/
	});

	$(".ColVis_MasterButton span").html("");

	 $("#createSerSet").css({'height':'550px'});
	$.blockUI({title:'<img style="cursor:auto;width:16px;height:16px;" src="images/cdmr/closeicon.png" onclick="blockUIClose();destroyOnclose();"/>', theme:true, draggable:true, message: $('#createSerSet'), 
		themedCSS: {width: '81%', left:'100px', top:'10px'} }); 
	hideServiceSetDialog();
	//$.blockUI.defaults.css = {overflow: 'scroll'};
	//$( ".blockUI" ).resizable();
	//$( ".blockUI" ).css('overflow', 'scroll');
}

function addInArray(value, obj){

	
	if($.inArray(value, checkdAll) != -1){
		checkdAll.splice($.inArray(value, checkdAll), 1);
		return;
	}
	checkdAll[chkIndex] = value;
	chkIndex = chkIndex + 1;
	
}

function addInArraySSLink(value, obj){
	if($(obj).is(":checked")){
		var v = value.split(',');
		
		checkdAll[chkIndex] = v[0];
		chkIndex = chkIndex + 1;
		ssLinkID[ssLinkIndex] = v[1];
		ssLinkIndex = ssLinkIndex + 1;
	}else{
		var v = value.split(',');
		if($.inArray(v[0], checkdAll) != -1){
			checkdAll.splice($.inArray(v[0], checkdAll), 1);
			
		}
		if($.inArray(v[1], ssLinkID) != -1){
			ssLinkID.splice($.inArray(v[1], ssLinkID), 1);
		}
	}
	
}

function openCreateGroup(){
	getModuleGrp();
	 var col=fetchColumnProp(chggrpmodname);
	 $("#divInDialogTab").html("");
	servSecFlag = false;
	checkdAll = checkdAll.filter(function(n){ return n != undefined });
	checkdAll = checkdAll.filter(function(n){ return n != 'on' });
	if(checkdAll.length == 0){
		alertify.alert("Please Select Items.");
		return;	}	
	arrayStr = "";
	for(var i=0; i<checkdAll.length; i++){
		arrayStr += checkdAll[i] + ",";
	}
	getSlectedGroups(0);
	$("#groupdata").empty();
	serData = $("#groupdata").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getservicesetdata.action",
	    "fnServerParams": function ( aoData ) {
	         aoData.push( { "name": "ids", "value": arrayStr});
	         aoData.push( { "name": "tempids", "value": ""});
	         aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
	     },
	     "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bLengthChange": true,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "sScrollY": "200px",
	    "sScrollX": "100%",
	    "sScrollXInner":"100%",
	    "sDom": 'RC<"clear">frtlip',
	    "sPaginationType": "full_numbers",
	    "oColVis": {
	    	"bShowAll": true,
	    	"sShowAll": "Select All"
        },
	    "aoColumns":col
		   
	});

	
	$("#grpLink").empty();
	
	grpLink = $("#grpLink").dataTable({
	     "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "bLengthChange": true,
	    "sScrollY": "150px",
	    "sScrollX": "100%",
	    "sScrollXInner":"100%",
	    "sDom": 'RC<"clear">frtlip',
	    "sPaginationType": "full_numbers",
	    "oColVis": {
	    	"bShowAll": true,
	    	"sShowAll": "Select All"
        },
	    "aoColumns":col 
		    
	});

	//$("#divInDialogTab").html("");
	//getSlectedGroups(0);

	$(".ColVis_MasterButton span").html("");
	$("#createGroup").css({'height':'550px'});
	$.blockUI({title:'<img style="cursor:auto;width:16px;height:16px;" src="images/cdmr/closeicon.png" onclick="blockUIClose();destroyOnclose2();"/>', theme:true, draggable:true, message: $('#createGroup'), 
		themedCSS: {width: '81%', left:'100px', top:'10px'} }); 
}

function getSlectedGroups(pn, id){

	idholder = id;
	groupCode = $("#"+(id)).val();
	
	
    if(groupCode != ""){
    	$("#groupname").val($("#"+(id) + " option:selected").text());
    
      }

	if(typeof  groupCode == 'undefined')
	{
		groupCode = '999999999';
	}
    
	$.ajax({
		url:"getgropus.action",
		type:"post",
		data:{parentNode:pn,"FILE_ID":$("#cdmtype").val()},
		success:function(response){
			var data = response['groups'];
			//var save= '<td class="editTD"><a id="'+index+'" class="edit" href="#">	<img src="images/cdmr/edit.png"></a></td></tr>';
			if(data.length == 0){
				   
					$("#divInDialog #divInDialogTab tr").each(function(i){
					  
						if(id<=i){
							
							    $(this).remove();
							    --index;
							    
							    }
						});
				
				return;
				}
			
			$("#divInDialog #divInDialogTab tr").each(function(i){
				
				if(id<=i){
				
					    $(this).remove();
					    --index;

					    }
				    
				});

			var leftdata='<tr id="tr'+index+'"><td align="left"><label >L'+(index)+'</label></td>';
			var select = '<td class="dropdown"><select name="groupone'+pn+'" id="'+index+'" class="selectbox" onchange="getSlectedGroups(this.value, this.id);">';
			var option = '<option value="">Select Value</option>';
			for (var i = 0; i < data.length; i++) {
				option += '<option value="'+data[i][0]+'">'+data[i][2]+'</option>';
			}
			var addnew='<td id="td'+index+'"><div><img src ="images/cdmr/add.png" id="'+index+'" class="cursor"  style="width:16;height:16;cursor:pointer;" onclick="addNewGrp(this.id);"/></div></td></tr>';
			select = select + option + '</select></td>';
			var row = leftdata+select+addnew;
			$("#divInDialog #divInDialogTab").append(row);
			index++;
		}
	});

	if(groupCode){
		
		$("#grpLink").empty();
		grpLink = $("#grpLink").dataTable({
			"bProcessing": false,
		    "bServerSide": true,
		    "sAjaxSource": "getgrouplinkdata.action",
		    "fnServerParams": function ( aoData ) {
		         aoData.push( { "name": "groupcode", "value": groupCode});
		         aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
		     },
		    "bJQueryUI": true,
		    "bPaginate": true,
		    'iDisplayLength': 100,
		    "bDestroy": true,
		    "bFilter": true,
		    "bSort": true,
		    "sScrollY": "150px",
		    "sScrollX": "100%",
		    "sScrollX": "100%",
		    "sScrollXInner":"100%",
		    "bLengthChange": true,
		    "sDom": 'RC<"clear">frtlip',
		    "sPaginationType": "full_numbers",
		    "oColVis": {
		    	"bShowAll": true,
		    	"sShowAll": "Select All"
	        },
  	      	 "aoColumns": fetchColumnProp(chggrpmodname)
  	      	 /*[
  	      				{"sWidth":"200px","mDataProp":"SERVICE_DESC1", "bSortable":true,"sTitle": "Service Description"},
  	      				{"sWidth":"200px","mDataProp":"SERVICE_DESC2", "bSortable":true,"sTitle": "SVC Code Gnl Desc"},
  	      				{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT", "bSortable":true,"sTitle": "CMS HCPCS/CPT Code"},
  	      				{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "CMS Eff Date"},
  	      				{"sWidth":"100px", "mDataProp":"SERVICE_CODE", "bSortable":true,"sTitle": "SVC Code"},
  	      				{"sWidth":"100px","mDataProp":"PRS_CODE", "bSortable":true,"sTitle": "PRS Code"},
  	      				{"sWidth":"100px", "mDataProp":"MCAID_HCPCS_CPT", "bSortable":true,"sTitle": "MCAID HCPCS/CPT Code"},
  	      				{"sWidth":"100px","mDataProp":"MCAID_HCPCS_CPT_EFF_DATE","bSortable":true,"sTitle": "MCAID Eff Date"},
  	      				{"sWidth":"100px","mDataProp":"CPT_CODE", "bSortable":true,"sTitle": "CPT Code"},
  	      				{"sWidth":"100px","mDataProp":"BLUE_CROSS_HCPCS_CPT", "bSortable":true,"sTitle": "BCBS HCPCS/CPT Code"},
  	      				{"sWidth":"100px", "mDataProp":"BLUE_CROSS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "BCBS Eff Date"},
  	      				{"sWidth":"100px","mDataProp":"REV_CLASS", "bSortable":true, "sClass" : "centerAlign","sTitle": "REV Class"},
  	      				{"sWidth":"200px", "mDataProp":"CUSTOM_DESC1", "bSortable":true,"sTitle": "REV Class Desc"},
  	      				{"sWidth":"100px", "mDataProp":"REV_CODE", "bSortable":true, "sClass" : "centerAlign","sTitle": "GL Key"},
  	      				{"sWidth":"100px", "mDataProp":"HCC_TECH_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "SVC Price"},
  	      				{"sWidth":"100px", "mDataProp":"PRS_PHY_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "PRS Price"},
  	      				{"sWidth":"100px","mDataProp":"EFF_FROM_DATE", "bSortable":true, "sTitle": "Price Eff Date"},
  	      				{"sWidth":"100px", "mDataProp":"EFF_TO_DATE", "bSortable":true, "sTitle":"SVC Code Inact Date"}
  	      		    ]*/,
		});
		$(".ColVis_MasterButton span").html("");
	}
	

}

function addNewGrp(id){

	$( '#divInDialog #divInDialogTab tr').each(function(){
		var id = $(this).find('td:last').attr('id').replace("td", "");
		$(this).find('td:last').html('<div><img src ="images/cdmr/add.png" id="'+id+'" class="cursor"  style="width:16;height:16;cursor:pointer;" onclick="addNewGrp(this.id);"/></div>');
	});
	
	var tdcol="td"+id;
	var td='<div><input type="text" id="te'+id+'" style="width:55%;float:left" value=""/><span><a href="#" id="'+id+'" onclick="createNewGrup(this.id);"><img src="images/cdmr/save.png"/></a><a href="#" id="'+id+'" onclick="closeEditing(this.id);"><img src="images/cdmr/can.png"/></a></span></div>';
	$("#divInDialog #divInDialogTab #"+tdcol).html("");
	$("#divInDialog #divInDialogTab #"+tdcol).append(td);
}

function closeEditing(id){
	var tdcol="td"+id;
	$("#divInDialog #divInDialogTab #"+tdcol).html('<div><img src ="images/cdmr/add.png" id="'+id+'" class="cursor"  style="width:16;height:16;cursor:pointer;" onclick="addNewGrp(this.id);"/></div>');
}

function createNewGrup(id){
	var parentid;
	 var grupName=$("#te"+id).val();
	 var v = $("#" + (id-1)).val();
	 if(v == "" || v == 'undefine' || v == null || v == " "){
		parentid = 0;
	 }else{
		 parentid = v;
	 }
	 
	 if(grupName == ""){
		alertify.alert("Please Enter Service Section.");
		return;
	 }

	 var refreshGrpCodeOpt = "";
    $.ajax({
		url:"selectedgrupadd.action",
		type:"post",
		data:{grpName:grupName,flag:true, parentId:parentid,grpLev:id,"FILE_ID":$("#cdmtype").val()},
	 		success:function(response){
			if(response.checkForExisting == false){
				alertify.alert('Service Section "'+grupName+'" already exists.');
				return;
			}
			
			$.ajax({
				url:"getgropus.action",
				type:"post",
				data:{parentNode:parentid,"FILE_ID":$("#cdmtype").val()},
				success:function(response){
					var data = response['groups'];
					
					var option = '<option value="">Select Value</option>';
					for (var i = 0; i < data.length; i++) {
						if(data[i][2] == grupName){
							option += '<option value="'+data[i][0]+'" selected>'+data[i][2]+'</option>';
							refreshGrpCodeOpt = data[i][0];
							groupCode = data[i][0];
							idholder = id;
						}else{
							option += '<option value="'+data[i][0]+'">'+data[i][2]+'</option>';
						}
					}
					$("#"+id).html(option);
					$("#divInDialog #divInDialogTab tr").each(function(j){
						if(j >= id){
							$(this).remove();
							--index;
						}
						
					});

					$("#treediv").html("");
					  treeid = treeid + "a";			  
					  refreshGroupLink(refreshGrpCodeOpt);
					  loadTreeData();			  
					alertify.alert("Added Successfully.");
				}
			});
			
			
		}
   });
   var tdcol="td"+id;
	var td='<div><img src ="images/cdmr/add.png" id="'+id+'" class="cursor"  style="width:16;height:16;cursor:pointer;" onclick="addNewGrp(this.id);"/></div>';
   
	//$("#divInDialog #divInDialogTab #"').append(td);
	$("#divInDialog #divInDialogTab #"+tdcol).html("");
	$("#divInDialog #divInDialogTab #"+tdcol).append(td);

	
   
}



function refreshGroupLink(groupCode){
	
	$("#grpLink").empty();
	grpLink = $("#grpLink").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getgrouplinkdata.action",
	    "fnServerParams": function ( aoData ) {
	         aoData.push( { "name": "groupcode", "value": groupCode});
	         aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
	     },
	    "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "sScrollY": "150px",
	    "sScrollX": "100%",
	    "sScrollX": "100%",
	    "sScrollXInner":"100%",
	    "bLengthChange": false,
	    "sDom": 'RC<"clear">frtlip',
	    "oColVis": {
	    	"bShowAll": true,
	    	"sShowAll": "Select All"
        },
	      	 "aoColumns":fetchColumnProp(chggrpmodname) 
		      	 /*[
	      				{"sWidth":"200px","mDataProp":"SERVICE_DESC1", "bSortable":true,"sTitle": "Service Description"},
	      				{"sWidth":"200px","mDataProp":"SERVICE_DESC2", "bSortable":true,"sTitle": "SVC Code Gnl Desc"},
	      				{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT", "bSortable":true,"sTitle": "CMS HCPCS/CPT Code"},
	      				{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "CMS Eff Date"},
	      				{"sWidth":"100px", "mDataProp":"SERVICE_CODE", "bSortable":true,"sTitle": "SVC Code"},
	      				{"sWidth":"100px","mDataProp":"PRS_CODE", "bSortable":true,"sTitle": "PRS Code"},
	      				{"sWidth":"100px", "mDataProp":"MCAID_HCPCS_CPT", "bSortable":true,"sTitle": "MCAID HCPCS/CPT Code"},
	      				{"sWidth":"100px","mDataProp":"MCAID_HCPCS_CPT_EFF_DATE","bSortable":true,"sTitle": "MCAID Eff Date"},
	      				{"sWidth":"100px","mDataProp":"CPT_CODE", "bSortable":true,"sTitle": "CPT Code"},
	      				{"sWidth":"100px","mDataProp":"BLUE_CROSS_HCPCS_CPT", "bSortable":true,"sTitle": "BCBS HCPCS/CPT Code"},
	      				{"sWidth":"100px", "mDataProp":"BLUE_CROSS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "BCBS Eff Date"},
	      				{"sWidth":"100px","mDataProp":"REV_CLASS", "bSortable":true, "sClass" : "centerAlign","sTitle": "REV Class"},
	      				{"sWidth":"200px", "mDataProp":"CUSTOM_DESC1", "bSortable":true,"sTitle": "REV Class Desc"},
	      				{"sWidth":"100px", "mDataProp":"REV_CODE", "bSortable":true, "sClass" : "centerAlign","sTitle": "GL Key"},
	      				{"sWidth":"100px", "mDataProp":"HCC_TECH_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "SVC Price"},
	      				{"sWidth":"100px", "mDataProp":"PRS_PHY_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "PRS Price"},
	      				{"sWidth":"100px","mDataProp":"EFF_FROM_DATE", "bSortable":true, "sTitle": "Price Eff Date"},
	      				{"sWidth":"100px", "mDataProp":"EFF_TO_DATE", "bSortable":true, "sTitle":"SVC Code Inact Date"}
	      		    ]*/,
	});
	$(".ColVis_MasterButton span").html("");
}

////////////////////////////////////////////////////////////////////////

function createMoreGroup(){
    if($("#"+(index-1)).val() == ""){
        alertify.alert("Please Select Service Section.");
        return;
    }
    var pn=$("#"+(index-1)).val();
   
    var leftdata='<tr id="tr'+index+'"><td align="left"><label >L'+(index)+'</label></td>';
	var select = '<td class="dropdown"><select name="groupone'+pn+'" id="'+index+'" class="selectbox" onchange="getSlectedGroups(this.value, this.id);">';
	var option = '<option value="">Select Value</option>';
	
	var addnew='<td id="td'+index+'"><div><input type="text" id="te'+index+'" style="width:55%;float:left" value=""/><span><a href="#" id="'+index+'" onclick="createNewGrup(this.id);"><img src="images/cdmr/save.png"/></a><a href="#" id="'+index+'" onclick="removelastlevel(this.id);"><img src="images/cdmr/can.png"/></a></span></div></td></tr>';
	select = select + option + '</select></td>';
	var row = leftdata+select+addnew;
	$("#divInDialog #divInDialogTab").append(row);
	index++;
   
}

function removelastlevel(){

	$( '#divInDialog #divInDialogTab').find('tr:last').remove();
	index--;
	
	idholder--;
	
}

function loadTreeData(){
	$("#treediv").html('<ul id="'+treeid+'"></ul>');
	$("#"+treeid).treeview({
		  url: "loadParentGroup.action",
		  ajax: {
				data: {
					"additional": function() {
						return "time: " + new Date().getTime();
					}
				},
				type: "post"
			}
	});
	
	$("#" + treeid+" li").live('click', myLiveEventHandler);
}

function myLiveEventHandler(event)
{

	chgstatval="1";
	eventHolder = $(this);
  if(event.handled !== true)
  {
	  var v =  $(this).attr('id');
	 
	  $("#selectedname").html("  (" + $("#"+v + ' span').html() + ")");
	  $("#"+treeid+" li span").css('background-color', '');
	 // $("#"+v + ' span').css('background-color', 'yellow');
	
	 $(this).find('span:first').css('background-color', 'yellow');
	 var grpId = $(this).attr('id');
	  editServSecId = grpId.replace("grp", "");
	  $("#maincdmData").dataTable().fnDestroy();
	$("#maincdmData").empty();
	
	oTable = $("#maincdmData").dataTable({
			"bProcessing": false,
		    "bServerSide": true,
		    "sAjaxSource": "getcdmsearchgrpdata.action",
		    "fnServerParams": function ( aoData ) {
		            aoData.push({ "name": "grpID", "value":  grpId});
		            aoData.push( {"name": "chgstat", "value":chgstatval});
		         	aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
		         	aoData.push( {"name":"filterValues","value":JSON.stringify(selectedValues)});
					 aoData.push( {"name":"savedfilters","value":JSON.stringify(savedfiltercolvals)});
		     },
		     "bJQueryUI": true,
		    "bPaginate": true,
		    'iDisplayLength': 100,
		    "bLengthChange": true,
		    "bDestroy": true,
		    "bFilter": true,
		    "bSort": true,
		    "sScrollY": "230px",
		    "sScrollX": "100%",
		    "sScrollXInner":"100%",
		    "sDom": 'RC<"clear">frtlip',
		    "sPaginationType": "full_numbers",
		    "oColVis": {
		    	"bShowAll": true,
		    	"sShowAll": "Select All"
	        },
		    "aoColumns": fetchColumnProp(chgmodulename)
			    /*[
		      			{"sWidth":"50px", "mDataProp":"check","bSortable":false,"sTitle": '<input type="checkbox" id="allcb"/>'},
		      			{"sWidth":"200px","mDataProp":"SERVICE_DESC1", "bSortable":true,"sTitle": "Service Description"},
		      			{"sWidth":"200px","mDataProp":"SERVICE_DESC2", "bSortable":true,"sTitle": "SVC Code Gnl Desc"},
		      			{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT", "bSortable":true,"sTitle": "CMS HCPCS/CPT Code"},
		      			{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "CMS Eff Date"},
		      			{"sWidth":"100px", "mDataProp":"SERVICE_CODE", "bSortable":true,"sTitle": "SVC Code"},
		      			{"sWidth":"100px","mDataProp":"PRS_CODE", "bSortable":true,"sTitle": "PRS Code"},
		      			{"sWidth":"100px", "mDataProp":"MCAID_HCPCS_CPT", "bSortable":true,"sTitle": "MCAID HCPCS/CPT Code"},
		      			{"sWidth":"100px","mDataProp":"MCAID_HCPCS_CPT_EFF_DATE","bSortable":true,"sTitle": "MCAID Eff Date"},
		      			{"sWidth":"100px","mDataProp":"CPT_CODE", "bSortable":true,"sTitle": "CPT Code"},
		      			{"sWidth":"100px","mDataProp":"BLUE_CROSS_HCPCS_CPT", "bSortable":true,"sTitle": "BCBS HCPCS/CPT Code"},
		      			{"sWidth":"100px", "mDataProp":"BLUE_CROSS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "BCBS Eff Date"},
		      			{"sWidth":"100px","mDataProp":"REV_CLASS", "bSortable":true, "sClass" : "centerAlign","sTitle": "REV Class"},
		      			{"sWidth":"200px", "mDataProp":"CUSTOM_DESC1", "bSortable":true,"sTitle": "REV Class Desc"},
		      			{"sWidth":"100px", "mDataProp":"REV_CODE", "bSortable":true, "sClass" : "centerAlign","sTitle": "GL Key"},
		      			{"sWidth":"100px", "mDataProp":"HCC_TECH_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "SVC Price"},
		      			{"sWidth":"100px", "mDataProp":"PRS_PHY_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "PRS Price"},
		      			{"sWidth":"100px","mDataProp":"EFF_FROM_DATE", "bSortable":true, "sTitle": "Price Eff Date"},
		      			{"sWidth":"100px", "mDataProp":"EFF_TO_DATE", "bSortable":true, "sTitle":"SVC Code Inact Date"}
		      	    ]*/,
		    	    "fnDrawCallback": function () {
		    	    	$('#allcb').prop('checked', false);
		    	    	$('tbody tr td input[name="check"]').each(function(){
		    		    	if($.inArray($(this).val(), checkdAll) != -1){
		    		    		$(this).prop('checked', true);
		    		    		$('#allcb').prop('checked', true);
		    		    	}
		    	        });
		    	    	
		    	    }
		});
	//$("#advance_search").remove();
	//$('<button value="Advance Search" name="advance_search" class="advance_search" onclick="showAdvanceSearchForm();" id="advance_search" style="float:right;">Advance Search</button>').insertBefore('#maincdmData_filter label');
	 $('<select id="chgstat_dropdown" onchange="updateViewForChgStatSec(this.value)";><option value="1">Active  </option><option value="2">Inactive </option><option value="3">Both </option></select>').insertBefore('div.inner-addon input:first');
    $('<a class="addFilter" href="#" onclick="showAddFilterform();">Add Filters</a>').insertAfter('#maincdmData_filter');
    $('<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>').insertAfter('#maincdmData_filter');
	 
	 $(".ColVis_MasterButton span").html("");
	// $("#advance_search").hide();
     event.handled = true;
     initilizeCheckBox();
  }
  return false;
}
function loadGroupView(){
	//$("#groupdata").dataTable().fnDestroy();
   // $("#grpLink").dataTable().fnDestroy();
	currentView = 'ServiceSection';
	$("#servSetsView").dataTable().fnDestroy();
	$("#chgtype").attr('disabled', 'disabled');
	$("#chgstat").attr('disabled', 'disabled');
	checkdAll.length = 0;
	$("#forGroupView").css('width', '70%');
	$("#forTreeView").css('display', '');
	$("#forSetsView").css('display', 'none');
	if($("#searchgrp").val().length >0){
		$("#searchgrp").val("");
		$("#treediv li").each(function(){
				$(this).show();
				//$(this).attr('class', "collapsable");
				$(this).children("div").attr('class', "hitarea collapsable-hitarea");
				$(this).closest("ul").css('display', "block");
		});
		$("#treediv li div").each(function(){
			$(this).attr('class', "hitarea expandable-hitarea");
		});

		$("#treediv li ul").each(function(){
			$(this).css('display', "none");
		});
	}
	$("#"+treeid+" li span").first().trigger('click');
	
}

function loadListView(){

    currentView = 'List'
    	chgstatval="1";
	editServiceSetId = "";
   // $("#groupdata").dataTable().fnDestroy();
   // $("#grpLink").dataTable().fnDestroy();
    $("#servSetsView").dataTable().fnDestroy();
   // $("#servSetsView").empty();
	$("#selectedname").html("");
	$("#chgtype").removeAttr('disabled');
	$("#chgstat").removeAttr('disabled');
	checkdAll.length = 0;
	$("#forGroupView").css('width', '100%');
	$("#forTreeView").css('display', 'none');
	$("#forSetsView").css('display', 'none');
	$("#maincdmData").dataTable().fnDestroy();
	$("#maincdmData").empty();
	$("#advance_search").hide();
	drawDataTable();
}

function loadServiceSetView(){
	//$("#groupdata").dataTable().fnDestroy();
  //  $("#grpLink").dataTable().fnDestroy();
	currentView ='ServiceSet';
	$('#chgtype').prop('selectedIndex',0);
	checkdAll.length = 0;
	ssLinkID.length = 0;
	$("#chgtype").attr('disabled', 'disabled');
	$("#chgstat").attr('disabled', 'disabled');
	$("#forGroupView").css('width', '70%');
	$("#forTreeView").css('display', 'none');
	$("#forSetsView").css('display', '');
	 $("#servSetsView").dataTable();
	serSet = $("#servSetsView").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getserviceset.action",
	    "fnServerParams": function ( aoData ) {
	         aoData.push({ "name": "viewtype", "value":  "SS"});
	         aoData.push({ "name": "checkedId", "value":  editServiceSetId});
	         aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
	     },
	     "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bLengthChange": false,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "sScrollY": "190px",
	    "sDom": 'RC<"clear">lfrtip',
	    "aoColumns": [
				{  "sWidth":"50px", "sClass":"checkallView", "mDataProp":"check","bSortable":false},
				{   "sClass":"sersetsview", "mDataProp":"sersets", "bSortable":true},
					      			
	      	    ],
     	  "fnDrawCallback": function() {
	    	var checkFSS = true;
	    	$(".checkallView input").each(function(){
	    		if($(this).is(':checked')){
	    			var nextTD = $(this).closest("td").next();
	    			checkFSS = false;
	    			$("#selectedname").html("  ("+nextTD.html() + ")");
	    			getServiceSetValueForView($(this).val());
	    		}
	    		
	    	});
	    	if(checkFSS){
	    		getServiceSetValueForView(0);
    		}
     	    }
	});
	
	$("#servSetsView_wrapper .ColVis_MasterButton").hide();
	//$('<div style="float:left;margin-top:0%;"><button id="editSerSetBut" onclick="editServiceSet();">Edit</button><div>').appendTo('#servSetsView_filter');
	$('<div style="float:left;margin-top:35px;"><button id="editSerSetBut" class="btn_green" onclick="editServiceSet();">Edit</button></div>').appendTo('#servSetsView_filter label div.inner-addon');
	//$('<div style="float:left;margin-top:-30px;"><label><button id="editSerSetBut" onclick="editServiceSet();">Edit</button><label></div>').appendTo('#servSetsView_filter');
	$(".ColVis_MasterButton span").html("");
	
	
}



function getServiceSetValueForView(id){
	var nextTD = $("#"+id).closest("td").next();
	$("#selectedname").html("  (" + nextTD.html() + ")");
	serSetId = id;
	editServiceSetId = id;
	chgstatval="1";
	$("#maincdmData").dataTable().fnDestroy();
	$("#maincdmData").empty();
	oTable = $("#maincdmData").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getsslinkdata.action",
	    "fnServerParams": function ( aoData ) {
			aoData.push( { "name": "ssID", "value": serSetId},{ "name": "viewtype", "value": "SS"});
			aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
			aoData.push({"name": "chgstat", "value":chgstatval});
			 aoData.push( {"name":"filterValues","value":JSON.stringify(selectedValues)});
			 aoData.push( {"name":"savedfilters","value":JSON.stringify(savedfiltercolvals)});			
	     },
	     "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bLengthChange": true,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "sScrollY": "200px",
	    "sScrollX": "100%",
	    "sScrollXInner":"100%",
	    "sDom": 'RC<"clear">frtlip',
	    "sPaginationType": "full_numbers",
	    "oColVis": {
	    	"bShowAll": true,
	    	"sShowAll": "Select All"
        },
	    "aoColumns": fetchColumnProp(chgmodulename)
		    /*[
	      			{"sWidth":"50px", "mDataProp":"check","bSortable":false,"sTitle": '<input type="checkbox" id="allcb"/>'},
	      			{"sWidth":"200px","mDataProp":"SERVICE_DESC1", "bSortable":true,"sTitle": "Service Description"},
	      			{"sWidth":"200px","mDataProp":"SERVICE_DESC2", "bSortable":true,"sTitle": "SVC Code Gnl Desc"},
	      			{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT", "bSortable":true,"sTitle": "CMS HCPCS/CPT Code"},
	      			{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "CMS Eff Date"},
	      			{"sWidth":"100px", "mDataProp":"SERVICE_CODE", "bSortable":true,"sTitle": "SVC Code"},
	      			{"sWidth":"100px","mDataProp":"PRS_CODE", "bSortable":true,"sTitle": "PRS Code"},
	      			{"sWidth":"100px", "mDataProp":"MCAID_HCPCS_CPT", "bSortable":true,"sTitle": "MCAID HCPCS/CPT Code"},
	      			{"sWidth":"100px","mDataProp":"MCAID_HCPCS_CPT_EFF_DATE","bSortable":true,"sTitle": "MCAID Eff Date"},
	      			{"sWidth":"100px","mDataProp":"CPT_CODE", "bSortable":true,"sTitle": "CPT Code"},
	      			{"sWidth":"100px","mDataProp":"BLUE_CROSS_HCPCS_CPT", "bSortable":true,"sTitle": "BCBS HCPCS/CPT Code"},
	      			{"sWidth":"100px", "mDataProp":"BLUE_CROSS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "BCBS Eff Date"},
	      			{"sWidth":"100px","mDataProp":"REV_CLASS", "bSortable":true, "sClass" : "centerAlign","sTitle": "REV Class"},
	      			{"sWidth":"200px", "mDataProp":"CUSTOM_DESC1", "bSortable":true,"sTitle": "REV Class Desc"},
	      			{"sWidth":"100px", "mDataProp":"REV_CODE", "bSortable":true, "sClass" : "centerAlign","sTitle": "GL Key"},
	      			{"sWidth":"100px", "mDataProp":"HCC_TECH_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "SVC Price"},
	      			{"sWidth":"100px", "mDataProp":"PRS_PHY_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "PRS Price"},
	      			{"sWidth":"100px","mDataProp":"EFF_FROM_DATE", "bSortable":true, "sTitle": "Price Eff Date"},
	      			{"sWidth":"100px", "mDataProp":"EFF_TO_DATE", "bSortable":true, "sTitle":"SVC Code Inact Date"}
	      	    ]*/,
	    "fnDrawCallback": function () {
	    	$('#allcb').prop('checked', false);
	    	$('tbody tr td input[name="check"]').each(function(){
		    	var v = $(this).val().split(',');
		    	if(($.inArray(v[0], checkdAll) != -1) && ($.inArray(v[1], ssLinkID) != -1)){
		    		$(this).prop('checked', true);
		    		$('#allcb').prop('checked', true);
		    	}
	        });
	    	
	    }
	});
	//$("#advance_search").remove();
	//$('<button value="Advance Search" name="advance_search" class="advance_search" onclick="showAdvanceSearchForm();" id="advance_search" style="float:right;">Advance Search</button>').insertBefore('#maincdmData_filter label');
	//$('<select id="chgstat_dropdown" onchange="updateViewForChgStatSet(this.value)";><option value="1">Active  </option><option value="2">Inactive </option><option value="3">Both </option></select>').insertBefore('#maincdmData_filter label');
	$('<select id="chgstat_dropdown" onchange="updateViewForChgStatSet(this.value)";><option value="1">Active  </option><option value="2">Inactive </option><option value="3">Both </option></select>').insertBefore('#maincdmData_filter label div.inner-addon input:first');
	$('<a class="addFilter" href="#" onclick="showAddFilterform();">Add Filters</a>').insertAfter('#maincdmData_filter');
    $('<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>').insertAfter('#maincdmData_filter');
	$(".ColVis_MasterButton span").html("");
	//$("#advance_search").hide();
	initilizeCheckBox();
	//$("#chgstat_dropdown").val(chgstatval);
}

/************************************************/
 var ssecName;
 
 function editServiceSection(){
	 getModuleEdit();
	 if(!editServSecId){
		 alertify.alert("Please Select Service Section.");
		 return;
	 }
	 $("#serviesectionName").val($("#"+editServSecId + 'grp span').html());
	 ssecName = $("#serviesectionName").val();

	
	 $("#editGroupSectionTable").empty();
	 ess = $("#editGroupSectionTable").dataTable({
			"bProcessing": false,
		    "bServerSide": true,
		    "sAjaxSource": "getcdmsearchgrpdataforedit.action",
		    "fnServerParams": function ( aoData ) {
		         aoData.push({ "name": "grpID", "value":  editServSecId});
		         aoData.push({ "name": "editchk", "value":  "true"});
		         aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
		     },
		     "bJQueryUI": true,
		    "bPaginate": true,
		    'iDisplayLength': 100,
		    "bLengthChange": true,
		    "bDestroy": true,
		    "bFilter": true,
		    "bSort": true,
		    "sScrollY": "190px",
		    "sScrollX": "100%",
		    "sScrollXInner":"100%",
		    "sDom": 'RC<"clear">frtlip',
		    "sPaginationType": "full_numbers",
		    "oColVis": {
		    	"bShowAll": true,
		    	"sShowAll": "Select All"
	        },
		    "aoColumns":fetchColumnProp(chgdelmodname) ,
		    	    "fnDrawCallback": function () {
		    	    	$("img#deletegrpItem").each(function(){
			    	    	
								val = $(this).attr('value');
								for(var i=0; i<deleteSerSec.length;i++){
									if(val == deleteSerSec[i]){
										$(this).attr('src', 'images/cdmr/restore.png');
										$(this).closest('tr').addClass('deleteTr');
									}
								}
			    	    });
		    	    	
		    	    }
		});
		
	 $("#editServiceSection").css({'height':'550px'});
		$.blockUI({title:'<img style="cursor:auto;width:16px;height:16px;" src="images/cdmr/closeicon.png" onclick="blockUIClose();"/>', theme:true, draggable:true, message: $('#editServiceSection'), 
			themedCSS: {width: '81%', left:'100px', top:'10px'} }); 
		$(".ColVis_MasterButton span").html("");
 }

function deleteServSec(value, obj){
	//deleteSerSec.push(value);
	if($(obj).attr('src') == "images/cdmr/restore.png"){
		$(obj).attr('src', 'images/cdmr/delete.png');
		$(obj).closest('tr').removeClass('deleteTr');
		deleteSerSec.splice($.inArray(value, deleteSerSec), 1);
	}else{
		$(obj).attr('src', 'images/cdmr/restore.png');
		$(obj).closest('tr').addClass('deleteTr');
		deleteSerSec.push(value);
	}
	
}

function updateCdmMain(){
	if($("#serviesectionName").val() == ""){
		 alertify.alert("Please Enter Service Section Name.");
		 return;
	}

	var SerSecName = $("#serviesectionName").val();
	var changeFlagSec = true;
	if(ssecName.trim() == SerSecName.trim()){
		changeFlagSec = false;
	}
	var arrayStr = "";
	if(deleteSerSec.length != 0){
    	
		deleteSerSec = deleteSerSec.filter(function(n){ return n != undefined });
		deleteSerSec = deleteSerSec.filter(function(n){ return n != 'on' });
    	
    	
				for(var i=0; i<deleteSerSec.length; i++){
					arrayStr += deleteSerSec[i] + ",";
	        }
 	}	

 	$.ajax({
		url:"updatecdmforsersec.action",
		type:"post",
		data:{"sersecname": SerSecName, "deletedPk":arrayStr, "sersecId":editServSecId, "changeFlagSec":changeFlagSec},
		success:function(response){
			if(response.checkForExistingName == false){
				alertify.alert("Service Section already exists. No Updates will be done.");
				return;
			}
			deleteSerSec.length = 0;
			$("#treediv").html("");
		   treeid = treeid + "a";
		   loadTreeData();
		   updateForGroupEditView(editServSecId)
		   $.unblockUI();
			alertify.alert("Service Section Edited Successfully.");
 		}
 	});

 	
	
}

function completeDeleteServiceSec(){

	var r = alertify.confirm("This action will delete complete Service Section hierarchy. Do you want to continue?", function(r){

		if(r){
			$.ajax({
				url:"deletecompleteSerSec.action",
				type:"post",
				data:{"sersecId":editServSecId},
				success:function(response){
					treeid = treeid + "a";
					loadTreeData();
					updateForGroupEditView('999999grp');
					alertify.alert("Service Section Edited Successfully.");
					$.unblockUI();
				}
		  });
		}else{
			
		}
	return;
	});
	$("#divInDialogTab").html("");
	$("#editGroupSectionTable").dataTable().fnDestroy();
}
////////////////////////////////////////////

var ssName;
function editServiceSet(){
	getModuleEdit();	

	$.ajax({
		url:"getServiceSetName.action",
		type:"post",
		data:{"sersetId":editServiceSetId},
		success:function(response){
			$("#serviesetName").val(response.serviceSetName);
			ssName = $("#serviesetName").val();
		}
  });

	//$("#editServiceSetTable").dataTable().fnDestroy();
	$("#editServiceSetTable").empty();
	esse =  $("#editServiceSetTable").dataTable({
			"bProcessing": false,
		    "bServerSide": true,
		    "sAjaxSource": "getsslinkdataforedit.action",
		    "fnServerParams": function ( aoData ) {
		         aoData.push({ "name": "ssID", "value":  editServiceSetId});
		         aoData.push({ "name": "editchk", "value":  "true"});
		         aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
		     },
		     "bJQueryUI": true,
		    "bPaginate": true,
		    'iDisplayLength': 100,
		    "bLengthChange": true,
		    "bDestroy": true,
		    "bFilter": true,
		    "bSort": true,
		    "sScrollY": "190px",
		    "sScrollX": "100%",
		    "sScrollXInner":"100%",
		    "sDom": 'RC<"clear">frtlip',
		    "sPaginationType": "full_numbers",
		    "oColVis": {
		    	"bShowAll": true,
		    	"sShowAll": "Select All"
	        },
		    "aoColumns":fetchColumnProp(chgdelmodname) /*[
							{"sWidth":"50px", "mDataProp":"deleteset", "bSortable":false,"sTitle":"Delete"},
		      				{"sWidth":"200px","mDataProp":"SERVICE_DESC1", "bSortable":true,"sTitle": "Service Description"},
		      				{"sWidth":"200px","mDataProp":"SERVICE_DESC2", "bSortable":true,"sTitle": "SVC Code Gnl Desc"},
		      				{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT", "bSortable":true,"sTitle": "CMS HCPCS/CPT Code"},
		      				{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "CMS Eff Date"},
		      				{"sWidth":"100px", "mDataProp":"SERVICE_CODE", "bSortable":true,"sTitle": "SVC Code"},
		      				{"sWidth":"100px","mDataProp":"PRS_CODE", "bSortable":true,"sTitle": "PRS Code"},
		      				{"sWidth":"100px", "mDataProp":"MCAID_HCPCS_CPT", "bSortable":true,"sTitle": "MCAID HCPCS/CPT Code"},
		      				{"sWidth":"100px","mDataProp":"MCAID_HCPCS_CPT_EFF_DATE","bSortable":true,"sTitle": "MCAID Eff Date"},
		      				{"sWidth":"100px","mDataProp":"CPT_CODE", "bSortable":true,"sTitle": "CPT Code"},
		      				{"sWidth":"100px","mDataProp":"BLUE_CROSS_HCPCS_CPT", "bSortable":true,"sTitle": "BCBS HCPCS/CPT Code"},
		      				{"sWidth":"100px", "mDataProp":"BLUE_CROSS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "BCBS Eff Date"},
		      				{"sWidth":"100px","mDataProp":"REV_CLASS", "bSortable":true, "sClass" : "centerAlign","sTitle": "REV Class"},
		      				{"sWidth":"200px", "mDataProp":"CUSTOM_DESC1", "bSortable":true,"sTitle": "REV Class Desc"},
		      				{"sWidth":"100px", "mDataProp":"REV_CODE", "bSortable":true, "sClass" : "centerAlign","sTitle": "GL Key"},
		      				{"sWidth":"100px", "mDataProp":"HCC_TECH_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "SVC Price"},
		      				{"sWidth":"100px", "mDataProp":"PRS_PHY_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "PRS Price"},
		      				{"sWidth":"100px","mDataProp":"EFF_FROM_DATE", "bSortable":true, "sTitle": "Price Eff Date"},
		      				{"sWidth":"100px", "mDataProp":"EFF_TO_DATE", "bSortable":true, "sTitle":"SVC Code Inact Date"}
		      			
		      	    ]*/,
		    	    "fnDrawCallback": function () {
		    	    	$("img#deletesetItem").each(function(){
			    	    	 
								val = $(this).attr('value').split(",")[1];
								
								for(var i=0; i<deleteSerSet.length;i++){
									if(val == deleteSerSet[i]){
										$(this).attr('src', 'images/cdmr/restore.png');
										$(this).closest('tr').addClass('deleteTr');
									}
								}
			    	    });
		    	    	
		    	    }
		});
		
	 $("#editServiceSet").css({'height':'550px'});
		$.blockUI({title:'<img style="cursor:auto;width:16px;height:16px;" src="images/cdmr/closeicon.png" onclick="blockUIClose();"/>', theme:true, draggable:true, message: $('#editServiceSet'), 
			themedCSS: {width: '81%', left:'100px', top:'10px'} }); 
		$(".ColVis_MasterButton span").html("");
}

function deleteServSet(value, obj){
	
	var v = $(obj).attr('value').split(",");
	if($(obj).attr('src') == "images/cdmr/restore.png"){
		$(obj).attr('src', 'images/cdmr/delete.png');
		$(obj).closest('tr').removeClass('deleteTr');
		deleteSerSet.splice($.inArray(v[1], deleteSerSet), 1);
		deleteSerSetItmPk.splice($.inArray(v[0], deleteSerSetItmPk), 1);
		
	}else{
		$(obj).attr('src', 'images/cdmr/restore.png');
		$(obj).closest('tr').addClass('deleteTr');
		deleteSerSet.push(v[1]);
		deleteSerSetItmPk.push(v[0]);
	}

	
	
}

function updateSSLINKForSerSet(){
	if($("#serviesetName").val() == ""){
		 alertify.alert("Please Enter Service Set Name.");
		 return;
	}

	var SerSetName = $("#serviesetName").val();
    var changeFlagSS = true;
	if(ssName.trim() == SerSetName.trim()){
		changeFlagSS = false;
	}
	var arrayStr = "";
	var arrayStrPk = "";
	if(deleteSerSet.length != 0){
   	
		deleteSerSet = deleteSerSet.filter(function(n){ return n != undefined });
		deleteSerSet = deleteSerSet.filter(function(n){ return n != 'on' });
		deleteSerSetItmPk = deleteSerSetItmPk.filter(function(n){ return n != undefined });
		deleteSerSetItmPk = deleteSerSetItmPk.filter(function(n){ return n != 'on' });
   	
   	
				for(var i=0; i<deleteSerSet.length; i++){
					arrayStr += deleteSerSet[i] + ",";
	        	}
				for(var i=0; i<deleteSerSetItmPk.length; i++){
					arrayStrPk += deleteSerSetItmPk[i] + ",";
	        	}
	}	

	$.ajax({
		url:"updatecdmforserset.action",
		type:"post",
		data:{"sersetname": SerSetName, "deletedPk":arrayStr, "sersetId":editServiceSetId, "changeFlagSS":changeFlagSS, "deleteCDMPk":arrayStrPk,"FILE_ID":$("#cdmtype").val()},
		success:function(response){
			if(response.checkForExistingName == false){
				alertify.alert("Service Set already exists. No Updates will be done.");
				return;
			}
			deleteSerSet.length = 0;
			deleteSerSetItmPk.length = 0;
			getServiceSetValueForView(editServiceSetId);
			loadServiceSetView();
			alertify.alert("Service Set Edited Successfully.");
			$.unblockUI();
		}
	});

	
}

function completeDeleteServiceSet(){

	var r = alertify.confirm("This action will delete Service Set. Do you want to continue?", function(r){

		if(r){
			$.ajax({
				url:"deletecompleteSerSet.action",
				type:"post",
				data:{"sersetId":editServiceSetId,"FILE_ID":$("#cdmtype").val()},
				success:function(response){
					loadServiceSetView();
					alertify.alert("Service Set Edited Successfully.");
					deleteSerSet.length = 0;
					$.unblockUI();
					
				}
		  });
		}else{
			
		}
	return;
	});
	
}


function updateForGroupEditView(grpId){
	
	 oTable.dataTable({
			"bProcessing": false,
		    "bServerSide": true,
		    "sAjaxSource": "getcdmsearchgrpdata.action",
		    "fnServerParams": function ( aoData ) {
		            aoData.push({ "name": "grpID", "value":  grpId});
		            aoData.push( {"name": "chgstat", "value":chgstatval});
		         	aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
		         	aoData.push( {"name":"filterValues","value":JSON.stringify(selectedValues)});
					 aoData.push( {"name":"savedfilters","value":JSON.stringify(savedfiltercolvals)});
		     },
		     "bJQueryUI": true,
		    "bPaginate": true,
		    'iDisplayLength': 100,
		    "bLengthChange": true,
		    "bDestroy": true,
		    "bFilter": true,
		    "bSort": true,
		    "sScrollY": "230px",
		    "sScrollX": "100%",
		    "sScrollXInner":"100%",
		    "sDom": 'RC<"clear">frtlip',
		    "sPaginationType": "full_numbers",
		    "oColVis": {
		    	"bShowAll": true,
		    	"sShowAll": "Select All"
	        },
		    "aoColumns":fetchColumnProp(chgmodulename) 
			    /*[
		      			{"sWidth":"50px", "mDataProp":"check","bSortable":false,"sTitle": '<input type="checkbox" id="allcb"/>'},
		      			{"sWidth":"200px","mDataProp":"SERVICE_DESC1", "bSortable":true,"sTitle": "Service Description"},
		      			{"sWidth":"200px","mDataProp":"SERVICE_DESC2", "bSortable":true,"sTitle": "SVC Code Gnl Desc"},
		      			{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT", "bSortable":true,"sTitle": "CMS HCPCS/CPT Code"},
		      			{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "CMS Eff Date"},
		      			{"sWidth":"100px", "mDataProp":"SERVICE_CODE", "bSortable":true,"sTitle": "SVC Code"},
		      			{"sWidth":"100px","mDataProp":"PRS_CODE", "bSortable":true,"sTitle": "PRS Code"},
		      			{"sWidth":"100px", "mDataProp":"MCAID_HCPCS_CPT", "bSortable":true,"sTitle": "MCAID HCPCS/CPT Code"},
		      			{"sWidth":"100px","mDataProp":"MCAID_HCPCS_CPT_EFF_DATE","bSortable":true,"sTitle": "MCAID Eff Date"},
		      			{"sWidth":"100px","mDataProp":"CPT_CODE", "bSortable":true,"sTitle": "CPT Code"},
		      			{"sWidth":"100px","mDataProp":"BLUE_CROSS_HCPCS_CPT", "bSortable":true,"sTitle": "BCBS HCPCS/CPT Code"},
		      			{"sWidth":"100px", "mDataProp":"BLUE_CROSS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "BCBS Eff Date"},
		      			{"sWidth":"100px","mDataProp":"REV_CLASS", "bSortable":true, "sClass" : "centerAlign","sTitle": "REV Class"},
		      			{"sWidth":"200px", "mDataProp":"CUSTOM_DESC1", "bSortable":true,"sTitle": "REV Class Desc"},
		      			{"sWidth":"100px", "mDataProp":"REV_CODE", "bSortable":true, "sClass" : "centerAlign","sTitle": "GL Key"},
		      			{"sWidth":"100px", "mDataProp":"HCC_TECH_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "SVC Price"},
		      			{"sWidth":"100px", "mDataProp":"PRS_PHY_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "PRS Price"},
		      			{"sWidth":"100px","mDataProp":"EFF_FROM_DATE", "bSortable":true, "sTitle": "Price Eff Date"},
		      			{"sWidth":"100px", "mDataProp":"EFF_TO_DATE", "bSortable":true, "sTitle":"SVC Code Inact Date"}
		      	    ]*/,
		});
	 $('<select id="chgstat_dropdown" onchange="updateViewForChgStatAdvSrchSet(this.value);"><option value="1" >Active  </option><option value="2">Inactive </option><option value="3">Both </option></select>').insertBefore('#maincdmData_filter label');
     $('<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>').insertAfter('#maincdmData_filter');
     $('<a href="#" onclick="showAddFilterform();" style ="float:left;font-weight:bold;margin-left:76%">Add Filters</a>').insertAfter('#maincdmData_filter');
    
	 $(".ColVis_MasterButton span").html(""); 
	 initilizeCheckBox();
}

function updateAfterAtachServiceSection(groupCode){
	$("#grpLink").empty();
	$("#grpLink").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getgrouplinkdata.action",
	    "fnServerParams": function ( aoData ) {
	         aoData.push( { "name": "groupcode", "value": groupCode});
	         aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
	     },
	    "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "sScrollY": "150px",
	    "sScrollX": "100%",
	    "sScrollX": "100%",
	    "sScrollXInner":"100%",
	    "bLengthChange": true,
	    "sDom": 'RC<"clear">frtlip',
	    "sPaginationType": "full_numbers",
	    "oColVis": {
	    	"bShowAll": true,
	    	"sShowAll": "Select All"
        },
	    "aoColumns":fetchColumnProp(chggrpmodname) 
		    /*[
	      				{"sWidth":"200px","mDataProp":"SERVICE_DESC1", "bSortable":true,"sTitle": "Service Description"},
	      				{"sWidth":"200px","mDataProp":"SERVICE_DESC2", "bSortable":true,"sTitle": "SVC Code Gnl Desc"},
	      				{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT", "bSortable":true,"sTitle": "CMS HCPCS/CPT Code"},
	      				{"sWidth":"100px","mDataProp":"CMS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "CMS Eff Date"},
	      				{"sWidth":"100px", "mDataProp":"SERVICE_CODE", "bSortable":true,"sTitle": "SVC Code"},
	      				{"sWidth":"100px","mDataProp":"PRS_CODE", "bSortable":true,"sTitle": "PRS Code"},
	      				{"sWidth":"100px", "mDataProp":"MCAID_HCPCS_CPT", "bSortable":true,"sTitle": "MCAID HCPCS/CPT Code"},
	      				{"sWidth":"100px","mDataProp":"MCAID_HCPCS_CPT_EFF_DATE","bSortable":true,"sTitle": "MCAID Eff Date"},
	      				{"sWidth":"100px","mDataProp":"CPT_CODE", "bSortable":true,"sTitle": "CPT Code"},
	      				{"sWidth":"100px","mDataProp":"BLUE_CROSS_HCPCS_CPT", "bSortable":true,"sTitle": "BCBS HCPCS/CPT Code"},
	      				{"sWidth":"100px", "mDataProp":"BLUE_CROSS_HCPCS_CPT_EFF_DATE", "bSortable":true,"sTitle": "BCBS Eff Date"},
	      				{"sWidth":"100px","mDataProp":"REV_CLASS", "bSortable":true, "sClass" : "centerAlign","sTitle": "REV Class"},
	      				{"sWidth":"200px", "mDataProp":"CUSTOM_DESC1", "bSortable":true,"sTitle": "REV Class Desc"},
	      				{"sWidth":"100px", "mDataProp":"REV_CODE", "bSortable":true, "sClass" : "centerAlign","sTitle": "GL Key"},
	      				{"sWidth":"100px", "mDataProp":"HCC_TECH_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "SVC Price"},
	      				{"sWidth":"100px", "mDataProp":"PRS_PHY_CHG", "bSortable":true, "sClass" : "rightAlign","sTitle": "PRS Price"},
	      				{"sWidth":"100px","mDataProp":"EFF_FROM_DATE", "bSortable":true, "sTitle": "Price Eff Date"},
	      				{"sWidth":"100px", "mDataProp":"EFF_TO_DATE", "bSortable":true, "sTitle":"SVC Code Inact Date"}
	      		    ]*/
	});
	$(".ColVis_MasterButton span").html("");
}

function showAdvanceSearchForm(){
	$.blockUI({title:'<img style="cursor:auto;width:16px;height:16px;" src="images/cdmr/closeicon.png" onclick="closePopupWindow();"/>', theme:true, draggable:true, message: $('#advanceSearch'), 
		themedCSS: {width: '70%', left:'15%', top:'20%', height:'300px', overflow:'hidden'} }); 
	$("#advanceSearch").css('overflow', 'hidden');
}

function closePopupWindow(){
	$('#advanceSearch').find('input:text').val(''); 
	$.unblockUI();
}
function advanceSearchFilterDataTable(){
	
	if(currentView == 'List'){
		
		oTable = $("#maincdmData").dataTable(advanceSearch);
		$("#advance_search").remove();
		$('<button value="Advance Search" name="advance_search" onclick="showAdvanceSearchForm();" class="advance_search" id="advance_search" style="float:right;">Advance Search</button>').insertBefore('#maincdmData_filter label');
		$(".ColVis_MasterButton span").html("");
		initilizeCheckBox();
		
	}else if(currentView == 'ServiceSection'){
		advanceSearchForServiceSection();
	}else if(currentView == 'ServiceSet'){
		advanceSearchForServiceSet();
	}
	$('#advanceSearch').find('input:text').val(''); 
	$("#advance_search").hide();
	$.unblockUI();
}


function advanceSearchForServiceSet(){
	$('.progress-indicator').css( 'display', 'block' );
	var filterName="";
	filterName=$('#entertosave').val();
	$("#maincdmData").dataTable().fnDestroy();
	$("#maincdmData").empty();
	oTable = $("#maincdmData").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getsslinkdata.action",
	    "fnServerParams": function ( aoData ) {
					 aoData.push( { "name": "ssID", "value": serSetId}),
				    /* aoData.push( { "name": "SERVICE_DESC2_AS", "value":  $("#svcCodeGnlDesc").val()} );
					 aoData.push({"name": "CMS_HCPCS_CPT_AS", "value":$("#cmsHcpcsCptCode").val()});
					 aoData.push( { "name": "CMS_HCPCS_CPT_EFF_DATE_AS", "value":  $("#cmsEffDate").val()} );
					 aoData.push({"name": "SERVICE_CODE_AS", "value":$("#svcCode").val()});
					 aoData.push( { "name": "PRS_CODE_AS", "value":  $("#prsCode").val()} );
					 aoData.push({"name": "MCAID_HCPCS_CPT_AS", "value":$("#mcaidHcpcsCptCode").val()});
					 aoData.push( { "name": "MCAID_HCPCS_CPT_EFF_DATE_AS", "value":  $("#mcaidEffDate").val()} );
					 aoData.push({"name": "CPT_CODE_AS", "value":$("#cptCode").val()});
					 aoData.push( { "name": "BLUE_CROSS_HCPCS_CPT_AS", "value":  $("#bcbsHcpcsCptCode").val()} );
					 aoData.push({"name": "BLUE_CROSS_HCPCS_CPT_EFF_DATE_AS", "value":$("#bcbsEffDate").val()});
					 aoData.push( { "name": "REV_CLASS_AS", "value":  $("#revClass").val()} );
					 aoData.push({"name": "CUSTOM_DESC1_AS", "value":$("#revClassDesc").val()});
					 aoData.push( { "name": "REV_CODE_SA", "value":  $("#glKey").val()} );
					 aoData.push({"name": "HCC_TECH_CHG_AS", "value":$("#svcPrice").val()});
					 aoData.push( { "name": "PRS_PHY_CHG_AS", "value":  $("#prsPrice").val()} );
					 aoData.push({"name": "EFF_FROM_DATE_AS", "value":$("#priceEffDate").val()});
					 aoData.push( { "name": "EFF_TO_DATE_AS", "value":  $("#svcCodeInactDate").val()} );*/
					 aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
					 aoData.push( { "name": "advanceSearch", "value":  true });
					 aoData.push( {"name":"filterValues","value":JSON.stringify(selectedValues)});
		             aoData.push({"name": "chgstat", "value":chgstatval});
	     },
	     "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bLengthChange": true,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "sScrollY": "200px",
	    "sScrollX": "100%",
	    "sScrollXInner":"100%",
	    "sDom": 'RC<"clear">frtlip',
	    "sPaginationType": "full_numbers",
	    "oColVis": {
	    	"bShowAll": true,
	    	"sShowAll": "Select All"
        },
	    "aoColumns": fetchColumnProp(chgmodulename),
	    "fnDrawCallback": function () {
	    	$('#allcb').prop('checked', false);
	    	$('tbody tr td input[name="check"]').each(function(){
		    	var v = $(this).val().split(',');
		    	if(($.inArray(v[0], checkdAll) != -1) && ($.inArray(v[1], ssLinkID) != -1)){
		    		$(this).prop('checked', true);
		    		$('#allcb').prop('checked', true);
		    	}
	        });
	    	
	    }

	});
//	$("#advance_search").remove();
	//$('<button value="Advance Search" name="advance_search" class="advance_search" onclick="showAdvanceSearchForm();" id="advance_search" style="float:right;">Advance Search</button>').insertBefore('#maincdmData_filter label');
	 $('<select id="chgstat_dropdown" onchange="updateViewForChgStatAdvSrchSet(this.value);"><option value="1" >Active  </option><option value="2">Inactive </option><option value="3">Both </option></select>').insertBefore('#maincdmData_filter label div.inner-addon input:first');//.insertBefore('#maincdmData_filter label');
     $('<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>').insertAfter('#maincdmData_filter');
     $('<a href="#" onclick="showAddFilterform();" style ="float:left;font-weight:bold;margin-left:76%">Add Filters</a>').insertAfter('#maincdmData_filter');
     if ($('#showfilters_area li').length != 0 && $("#maincdmData_filter").not(":contains('Search results for')") ){
    		$('<div class="serset"><label id="showfilter_indt" >Search results for : '+filterName+'</label><a href="#" onclick="clearAllFiltersSet();" class= "clearall_grp" style="padding-left:10px"> CLEAR ALL </a></div>').insertAfter('#maincdmData_filter');
     		$('<div class="setgrpdata">'+ $("#showfilters_area").html()+'</div>').insertAfter('#maincdmData_filter');
   	    } 
  	$('.progress-indicator').css( 'display', 'none' );
	$(".ColVis_MasterButton span").html("");
	//$("#advance_search").hide();
	initilizeCheckBox();
}

function advanceSearchForServiceSection(){
	$('.progress-indicator').css( 'display', 'block' );
	$("#maincdmData").dataTable().fnDestroy();
	$("#maincdmData").empty();
	var filterName="";
	filterName=$('#entertosave').val();
	oTable = $("#maincdmData").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "sAjaxSource": "getcdmsearchgrpdata.action",
	    "fnServerParams": function ( aoData ) {
					 aoData.push( { "name": "grpID", "value": editServSecId}),
				    /* aoData.push( { "name": "SERVICE_DESC2_AS", "value":  $("#svcCodeGnlDesc").val()} );
					 aoData.push({"name": "CMS_HCPCS_CPT_AS", "value":$("#cmsHcpcsCptCode").val()});
					 aoData.push( { "name": "CMS_HCPCS_CPT_EFF_DATE_AS", "value":  $("#cmsEffDate").val()} );
					 aoData.push({"name": "SERVICE_CODE_AS", "value":$("#svcCode").val()});
					 aoData.push( { "name": "PRS_CODE_AS", "value":  $("#prsCode").val()} );
					 aoData.push({"name": "MCAID_HCPCS_CPT_AS", "value":$("#mcaidHcpcsCptCode").val()});
					 aoData.push( { "name": "MCAID_HCPCS_CPT_EFF_DATE_AS", "value":  $("#mcaidEffDate").val()} );
					 aoData.push({"name": "CPT_CODE_AS", "value":$("#cptCode").val()});
					 aoData.push( { "name": "BLUE_CROSS_HCPCS_CPT_AS", "value":  $("#bcbsHcpcsCptCode").val()} );
					 aoData.push({"name": "BLUE_CROSS_HCPCS_CPT_EFF_DATE_AS", "value":$("#bcbsEffDate").val()});
					 aoData.push( { "name": "REV_CLASS_AS", "value":  $("#revClass").val()} );
					 aoData.push({"name": "CUSTOM_DESC1_AS", "value":$("#revClassDesc").val()});
					 aoData.push( { "name": "REV_CODE_SA", "value":  $("#glKey").val()} );
					 aoData.push({"name": "HCC_TECH_CHG_AS", "value":$("#svcPrice").val()});
					 aoData.push( { "name": "PRS_PHY_CHG_AS", "value":  $("#prsPrice").val()} );
					 aoData.push({"name": "EFF_FROM_DATE_AS", "value":$("#priceEffDate").val()});
					 aoData.push( { "name": "EFF_TO_DATE_AS", "value":  $("#svcCodeInactDate").val()} );*/
					 aoData.push( {"name": "chgstat", "value":chgstatval});
					 aoData.push({"name": "FILE_ID", "value":$("#cdmtype").val()});
					 aoData.push( { "name": "advanceSearch", "value":  true });
					 aoData.push( {"name":"filterValues","value":JSON.stringify(selectedValues)});
					 },
	     "bJQueryUI": true,
	    "bPaginate": true,
	    'iDisplayLength': 100,
	    "bLengthChange": true,
	    "bDestroy": true,
	    "bFilter": true,
	    "bSort": true,
	    "sScrollY": "200px",
	    "sScrollX": "100%",
	    "sScrollXInner":"100%",
	    "sDom": 'RC<"clear">frtlip',
	    "sPaginationType": "full_numbers",
	    "oColVis": {
	    	"bShowAll": true,
	    	"sShowAll": "Select All"
        },

        
	    "aoColumns": fetchColumnProp(chgmodulename),
	    "fnDrawCallback": function () {
	    	$('#allcb').prop('checked', false);
	    	$('tbody tr td input[name="check"]').each(function(){
		    	var v = $(this).val().split(',');
		    	if(($.inArray(v[0], checkdAll) != -1) && ($.inArray(v[1], ssLinkID) != -1)){
		    		$(this).prop('checked', true);
		    		$('#allcb').prop('checked', true);
		    	}
	        });
	    	
	    }
	});
	$("#advance_search").remove();
	$('<button value="Advance Search" name="advance_search" class="advance_search" onclick="showAdvanceSearchForm();" id="advance_search" style="float:right;">Advance Search</button>').insertBefore('#maincdmData_filter label');
	$('<select id="chgstat_dropdown" onchange="updateViewForChgStatAdvSrchSec(this.value);"><option value="1" >Active  </option><option value="2">Inactive </option><option value="3">Both </option></select>').insertBefore('div.inner-addon input:first');//.insertBefore('#maincdmData_filter label');
	$('<a class ="saveFilter" href="#" onclick="showSaveFilterpopup();">Saved Filters</a>').insertAfter('#maincdmData_filter');
    $('<a href="#" onclick="showAddFilterform();" style ="float:left;font-weight:bold;margin-left:76%">Add Filters</a>').insertAfter('#maincdmData_filter');
    if ($('#showfilters_area li').length != 0 && $("#maincdmData_filter").not(":contains('Search results for')") ){
		$('<div class="sersec"><label id="showfilter_indt" >Search results for : '+filterName+'</label><a href="#" onclick="clearAllFiltersSec();" class= "clearall_grp" style="padding-left:10px"> CLEAR ALL </a></div>').insertAfter('#maincdmData_filter');
    	$('<div class="grpdata">'+ $("#showfilters_area").html()+'</div>').insertAfter('#maincdmData_filter');
   	 }
    $('.progress-indicator').css( 'display', 'none' );
	$(".ColVis_MasterButton span").html("");
	$("#advance_search").hide();
	initilizeCheckBox();
}

--></script>
<style>
tr.rowPadding td {
	padding-bottom: 15px;
}

.dropdown select {
	border: 1px solid !important; /*Removes border*/
	-moz-appearance: none; /* Removes Default Firefox style*/
	background-position: 82px 7px; /*Position of the background-image*/
	width: 100px;
	/*Width of select dropdown to give space for arrow image*/
	/*My custom style for fonts*/
	width: 150px;
	height: 25px;
	color: #1455a2;
	margin-left: : 35px;
}

#servSetsView_filter  input[type="text"] {
	margin-top: 8%;
	-webkit-border-radius: 12px 0 0 12px !important;
    -moz-border-radius: 12px 0 0 12px !important;
    border-radius: 12px 0 0 12px !important;
}

#servSets_wrapper  input[type="text"] {
	margin-top: 10%;
	margin-left: -40%;
}

#asynctree li {
	cursor: pointer;
}

.selectbox {
	width: 100px;
}

.dataTables_scrollBody {
	height: 580px;
}

/*.DataTables_sort_wrapper{
	height: 25px;
	padding-top: 10px;
}
#maincdmData_length {
	position: absolute;
	top: 350px;
	width: 0%;
	margin-top: 20px;
}

*/
.DataTables_sort_wrapper{font-weight: bold;}

.TableTools {
	width: 105px;
	padding-left: 10px;
	float: left;
}


.tools {
	margin-top: 15px;
}

#crtservset {
	position: absolute;
	right: 215px;
}

#crtgrp {
	position: absolute;
	right: 390px;
}

#viewby {
	position: absolute;
	right: 800px;
}

#bylistlabel {
	position: absolute;
	right: 750px;
}

#bygrplabel {
	position: absolute;
	right: 670px;
}

#bysersetlabel {
	position: absolute;
	right: 550px;
}

.ColVis_MasterButton {
	width: 150%;
}

.ColVis_MasterButton span {
	margin-left: 15px;
}

table {
	table-layout: fixed;
	font-size: 12px;
}

#maincdmData {
table-layout: fixed;
	width: 100%;
}

tbody {
	table-layout: fixed;
}

thead {
	table-layout: fixed;
}

#fileinfo {
	margin-top: 12px;
}

#headerinfo {
	width: 900px;
	height: 40px;
}

.checkall {
	width: 20px;
	text-align: center;
}

.instcode {
	width: 60px;
	text-align: center;
}

.editTD {
	width: 40px;
	text-align: center;
}

.notesTD {
	width: 40px;
	text-align: center;
}

.servcode {
	width: 90px;
	text-align: center;
}

.desc {
	width: 150px;
	text-align: left;
}

#pendingDonorsTable td {
	height: 40px;
}

.cptcode {
	width: 60px;
	text-align: center;
}

.prscode {
	width: 60px;
	text-align: center;
}

.prschrg {
	width: 60px;
	text-align: right;
}

.hccchrg {
	width: 60px;
	text-align: right;
}

.revclass {
	width: 60px;
	text-align: center;
}

.editable {
	width: 90px;
	text-align: center;
}

.editSer {
	width: 100px;
	text-align: center;
}

tr.top td {
	border-top: thin solid black;
	height: 20px;
	padding-top: 5px;
}

.staging_button {
	-moz-box-shadow: inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow: inset 0px 1px 0px 0px #ffffff;
	box-shadow: inset 0px 1px 0px 0px #ffffff;
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ededed
		), color-stop(1, #dfdfdf) );
	background: -moz-linear-gradient(center top, #ededed 5%, #dfdfdf 100%);
	filter: progid : DXImageTransform.Microsoft.gradient ( startColorstr =
		'#ededed', endColorstr = '#dfdfdf' );
	background-color: #ededed;
	-webkit-border-top-left-radius: 5px;
	-moz-border-radius-topleft: 5px;
	border-top-left-radius: 5px;
	-webkit-border-top-right-radius: 5px;
	-moz-border-radius-topright: 5px;
	border-top-right-radius: 5px;
	-webkit-border-bottom-right-radius: 5px;
	-moz-border-radius-bottomright: 5px;
	border-bottom-right-radius: 5px;
	-webkit-border-bottom-left-radius: 5px;
	-moz-border-radius-bottomleft: 5px;
	border-bottom-left-radius: 5px;
	text-indent: 0;
	border: 1px solid #dcdcdc;
	display: inline-block;
	color: #777777;
	font-family: Arial;
	font-size: 15px;
	font-weight: bold;
	font-style: normal;
	height: 30px;
	line-height: 30px;
	text-decoration: none;
	text-align: center;
	text-shadow: 1px 1px 0px #ffffff;
}

.staging_button:hover {
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #dfdfdf
		), color-stop(1, #ededed) );
	background: -moz-linear-gradient(center top, #dfdfdf 5%, #ededed 100%);
	filter: progid : DXImageTransform.Microsoft.gradient ( startColorstr =
		'#dfdfdf', endColorstr = '#ededed' );
	background-color: #dfdfdf;
}

.select-style {
	border: 1px solid #ccc;
	width: 120px;
	border-radius: 3px;
	overflow: hidden;
	background: #fafafa url("img/icon-select.png") no-repeat 90% 50%;
}

.select-style select {
	padding: 5px 8px;
	width: 130%;
	border: none;
	box-shadow: none;
	background: transparent;
	background-image: none;
}

.select-style select:focus {
	outline: none;
}

#statusbar {
	position: absolute;
	left: 470px;
	margin-top: 35px;
	width: 145px;
	height: 25px;
}

.paging_full_numbers {
	height: 40px;
	width:29% !important;
}

.selector_1 select {
	border: 1px solid !important; /*Removes border*/
	-moz-appearance: none; /* Removes Default Firefox style*/
	background-position: 82px 7px; /*Position of the background-image*/
	/*Width of select dropdown to give space for arrow image*/
	/*My custom style for fonts*/
	width: 50px;
	height: 25px;
	color: #1455a2;
	margin-left: : 35px;
}

table input[type="text"] {
	background: white;
	border: 1px solid #DDD;
	border-radius: 5px;
	box-shadow: 0 0 5px #DDD inset;
	color: #666;
	float: right;
	padding: 5px 10px;
	width: 85%;
	outline: none;
	margin-left: 0px;
}

.ui-widget-header {
	font-size: 12px;
}

.dataTables_info {
	font-size: 11px;
}

.ColVis_MasterButton span {
	font-size: 10px;
}

.dataTables_info {
	width: 25%;
}

.dataTables_filter {
	margin-top: -20px;
	margin-bottom: -10px;
}

#servSetsView_filter {
	width: 100%;
}

.dataTables_filter input[type="text"] {
	background-color: #ffffff;
}

.dataTables_paginate {
	padding-top: 5px;
}

table.gridtable {
	font-family: verdana, arial, sans-serif;
	font-size: 11px;
	color: #333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}

table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
}

table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
}

.dataTables_filter {
	margin-top: -28px;
	margin-bottom: -10px;
}

#maincdmData td {
	padding-left: 1%;
}

#groupdata td {
	padding-left: 1%;
}

#groupdata {
	width: 100%;
}

#servicedata td {
	padding-left: 1%;
}

#servicedata {
	width: 100%;
}

#grpLink td {
	padding-left: 1%;
}

#grpLink {
	width: 100%;
}

.rightAlign {
	text-align: right;
}

.centerAlign {
	text-align: center;
}

#servLink {
	width: 100%;
}

#servLink td {
	padding-left: 1%;
}

#editGroupSectionTable td {
	padding-left: 1%;
}

#editServiceSetTable td {
	padding-left: 1%;
}

.deleteTr {
	text-decoration: line-through;
}

.restoreTr {
	text-decoration: none;
}

#serviesectionName {
	height: 20px;
	width: 90%
}

#serviesetName {
	height: 20px;
	width: 90%
}

.dataTables_scrollHead{
	width: 100% !important;
}

.dataTables_filter{
	margin-top: -25px !important;
}

td {
  word-wrap: break-word;
}

.paging_full_numbers .ui-button{
	padding: 1%;
	width: 30px;
}

.dataTables_filter input[type="text"]{
	/*padding: 1px 22px 1px 10px;*/
	padding: 2px 22px 0px 10px;
}

#servSetsView_wrapper .mysearchbtn{
	margin-top: 28px;
}

#servSets_filter .mysearchbtn{
	margin-top: 16px;
}
#maincdmData_filter input[type="text"]{
  
    -webkit-border-radius: 0 0 0 0 !important;
    -moz-border-radius: 0 0 0 0 !important;
    border-radius: 0 0 0 0 !important;
    
}
#searchgrp{

 -webkit-border-radius: 12px 0 0 12px !important;
    -moz-border-radius: 12px 0 0 12px !important;
    border-radius: 12px 0 0 12px !important;
   
}
#maincdmData_filter .mysearchbtn{
height:30px !important;
}
#chgtype{
margin-right:-30px;
}
 
#maincdmData{
    white-space:pre-wrap;
}    
#servSetsView_info{
	width: 200px !important;
}

.mysearchbtn {
  position: absolute;
  padding: 8px;

}
.mysearchbtn { 
	right: 15px;
}
#searchgrp {
	 margin-right: 8px;
	 border-radius: 14px  !important;	 
}
#maincdmData_filter label div.inner-addon input#searchtxt {	 
	 border-top-right-radius:14px !important;
	 border-bottom-right-radius:14px !important;		 
}
#servSetsView_filter label div.inner-addon input#searchtxt {	 
	 border-top-right-radius:14px !important;
	 border-bottom-right-radius:14px !important;		 
}

@media screen and (min-width: 1400px) {
	#servSetsView_filter label div.inner-addon input.mysearchbtn {	 
		margin-top:32px !important;
	}
	#servSets_filter label div.inner-addon input.mysearchbtn {	 
		margin-top:17px !important;
	}
}
@media screen and (min-width: 1500px) {
	#servSetsView_filter label div.inner-addon input.mysearchbtn  {	 
		margin-top:34px !important;
	}
	#servSets_filter label div.inner-addon input.mysearchbtn  {	 
		margin-top:19px !important;
	}
}
@media screen and (min-width: 1600px) {
	#servSetsView_filter label div.inner-addon input.mysearchbtn  {	 
		margin-top:36px !important;
	}
	#servSets_filter label div.inner-addon input.mysearchbtn  {	 
		margin-top:21px !important;
	}
}
@media screen and (min-width: 1700px) {
	#servSetsView_filter label div.inner-addon input.mysearchbtn  {	 
		margin-top:38px !important;
	}
	#servSets_filter label div.inner-addon input.mysearchbtn  {	 
		margin-top:23px !important;
	}
}
@media screen and (min-width: 1800px) {
	#servSetsView_filter label div.inner-addon input.mysearchbtn {	 
		margin-top:40px !important;
	}
	#servSets_filter label div.inner-addon input.mysearchbtn  {	 
		margin-top:25px !important;
	}
}
@media screen and (min-width: 1800px) {
	#servSetsView_filter label div.inner-addon input.mysearchbtn {	 
		margin-top:42px !important;
	}
	#servSets_filter label div.inner-addon input.mysearchbtn  {	 
		margin-top:27px !important;
	}
}
@media screen and (max-width: 1400px) {	
	#servSets_filter label div.inner-addon input#searchtxt  {	 
		margin-top:20px !important;
	}
}
</style>