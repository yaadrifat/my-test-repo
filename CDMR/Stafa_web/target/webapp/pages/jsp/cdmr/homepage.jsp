<%@ taglib prefix="s" uri="/struts-tags"%>
<link href="css/cdmr/cdmr_main.css" rel="stylesheet" type="text/css">
<link href="css/cdmr/fileUploader.css" rel="stylesheet" type="text/css">
<link href="css/cdmr/fileUploader.css" rel="stylesheet" type="text/css">
<link href="css/cdmr/ColReorder.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/cdmr/ColReorder.js"></script>
<script type="text/javascript" src="js/cdmr/ColumnStateSave.js"></script>

<script type="text/javascript">
var oTable;
$(document).ready(function(){
	$.ajax({
		url:"getbannertext.action",
		type:"post",
		data:null,
		cache:false,
			success:function(response){
				
	      	  if(response.import_status=='1'&&response.export_status=='0'){
	      		$("#bannercontent").show();
	 				$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
				}
				else if(response.export_status=='1'&& response.import_status=='0'){
					$("#bannercontent").show();
					$("#bannercontent").html("Export Interface is in progress. System slowness may occur")
				}
				else if(response.import_status=='1' && response.export_status=='1'){
					$("#bannercontent").show();
					$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
					$("#bannercontent").append("<div style='background-color: yellow;'>Export Interface is in progress. System slowness may occur</div>");
				}
				else{
					$("#bannercontent").hide();
				}
	       }

	});

	if($("#storageinput").val() == "" || $("#storageinput").val() == 'undefine'){
		$.ajax({
			url:"getlatestfileid.action",
			type:"post",
			data:null,
			success:function(response){
				fileID = response.fileID;
				$("#storageinput").val(fileID);
			}
		
		});
		
	}
	$.ajax({
		url:'fetchColumn.action',
		type:'post',
		 data: {
	            "MODULENAME": "Home",
	        },
		success:function(response){
	        initializeDataTable(response['colData']);
		}
	});	
	

});
function initializeDataTable(column){
	
	$(".ColVis_MasterButton span").html("");
	
	oTable = $(".display").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "bDestroy": true,
	    "sAjaxSource": "loadCDMRfilesdetails.action",
	    "bJQueryUI": true,
	    'iDisplayLength': 5,
	    "bLengthChange": false,
	    "responsive": true,
	    "bAutoWidth": true,
	    "bFilter": false,
	    "bSort": true,
	    "sScrollY": "250px",
	    "sDom": 'Rlfrtip',
	    "aaSorting": [[ 2, "desc" ]],
	    "aoColumns": column,
	    colReorder: {
	       realtime: true
	    }
	});
}




function showMessage(sFileName, _validFileExtensions){
	alertify.alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
}


function processFile(fileID)
{
	$("#storageinput").val(fileID);
	$('.progress-indicator').css( 'display', 'block' );
	$(".ui-dialog-titlebar-close").removeAttr('class');

	$.ajax({
		url:"updatefilestatus.action",
		type:"POST",
		data:{fileid:fileID},
		success:function(response){
			$.ajax({
				url:"jsp/cdmr/staging.jsp",
				type:"POST",
				data:null,
				success:function(response){
					$('.progress-indicator').css( 'display', 'none' );
					$('#main').html(response);
				}
			});
		}
	});


	
	
}

function loadLibraryPage(){

	$("#nav li a").each(function(){
		$(this).css('color', 'white');
		$(this).css('text-decoration', 'none');
	});
	
	$("#nav li:nth-child(3)").find('a').css('color', 'yellow');
	
	$("#nav li:nth-child(3)").find('a').css('text-decoration', 'underline');
	
	$.ajax({
		url:"jsp/cdmr/library.jsp",
		type:"POST",
		data:null,
		success:function(response){
			$('#main').html(response);
		}
	});
}

function processWindow(){

	$.blockUI({ message: $('#domMessage') }); 
}

function closeProcessWindow(){
	$.unblockUI(); 
	oTable = $(".display").dataTable({
		"bProcessing": false,
	    "bServerSide": true,
	    "bDestroy": true,
	    "sAjaxSource": "loadCDMRfilesdetails.action",
	    "bJQueryUI": true,
	    'iDisplayLength': 5,
	    "bLengthChange": false,
	    "bFilter": false,
	    "bSort": true,
	    "sScrollY": "200px",
	    "sDom": 'Rlfrtip',
	    "aoColumns": [
		      			{ "mDataProp": "COMMENTS","sWidth":"100px"  ,"bSortable":true },
		      			{ "mDataProp": "FILE_NAME","sWidth":"100px"  ,"bSortable":true },
		      			{ "mDataProp": "REVICEDON","sWidth":"100px"   ,"bSortable":true},
		      			{ "mDataProp": "USER_FIRSTNAME","sWidth":"100px"  ,"bSortable":true }
		      		]
	});
}

$(document).ready(function(){

	$(".px-input-button").removeAttr('class');
	$(".ui-button-icon-primary").removeAttr('class');

	$("#nav li a").first().css('color', 'yellow');
				
	$("#nav li a").first().css('text-decoration', 'underline');
	
	
});

</script>
<style type="text/css">
.DataTables_sort_wrapper{
	padding-left:10px;
	font-weight: bold;
}

#pendingDonorsTable_info, #pendingDonorsTable_paginate{
	display: none;
}
</style>
<div class="cleaner"></div>
 
 <div class="column">		
  	<!-- <div  class="portlet" >
	<div class="portlet-header "><s:text name="vcdmr.label.fileupload"/></div>
		<div class="portlet-content">
			<div id="assignDonorDiv" style="overflow-x:auto;overflow-y:hidden;">
				<div id="fileuploadDiv" style="height:50px">
					<iframe src="jsp/cdmr/fileuploading.jsp" style="border: none;margin-left: 30%;width: 500px"></iframe>
				</div>
			</div>	
		</div>
	</div>-->
	
	<div  class="portlet" style="height:600px">
	<div class="portlet-header "><s:text name="vcdmr.label.workqueue"/></div>
		<div class="portlet-content" >
			<table cellpadding="0" cellspacing="0" border="0" id="pendingDonorsTable" class="display draggable">
			<thead><!--
			   <tr>
					<th colspan="1"><s:text name="vcdmr.label.status"/></th>
					<th colspan="1"><s:text name="vcdmr.label.filename"/></th>
					<th colspan="1"><s:text name="vcdmr.label.lastrevision"/></th>
					<th colspan="1"><s:text name="vcdmr.label.resivedby"/></th>
				</tr>
			--></thead>
			<tbody>
				
			</tbody>
			</table>
		</div>
		<div>
			<div id="showfiles" ><input type="button" class="btn_blue" onclick="loadLibraryPage();" value="Show All Files"/></div>
		</div>
	</div>	
	<div class="progress-indicator">
   		<img src="images/icon_loading_75x75.gif" alt="" />
	</div>
	<div  id="ieprogressindicator" class="" style="display:none;">
   		<center><img src="images/icon_loading_75x75.gif" alt="" /></center>
	</div>
	<div id="domMessage" style="display:none;height: 30px; "> 
    	<p style="font-size: 14px;">File processing is in progress. Please wait.</p>
	</div> 
</div>		

<div class="cleaner"></div>
<!-- 
<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td>Next:</td>
				<td><div><div class="mainselection"><select name="" id=""><option value="">Select</option><option>Preparation</option><option>Verification</option></select></div></div></td>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="nextProcessPreProduct();" value="Next"/></td>
				</tr>
				</table>
				</form>
		</div>
 -->
<script>

$(function() {

	

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});

	/**For Notes**/
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

	/* $( ".portlet-header .ui-addnew" ).click(function() {
		showPopUp("open","addNewDialog","Add New Acquision","900","450")
	}); */
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");
	$( ".portlet-header .ui-reset " ).click(function() {
		var formName = document.getElementById("login");
		//clearForm(formName);
		clear();
	});

// 	$( ".portlet-header .ui-notes " ).click(function() {
// 		showNotes('open');
// 	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	

	$("td").removeClass('sorting_1');
});



</script>

<style>
.upload-data.pending, {
	margin-left: 350px;
}

#pxupload1_text{
	margin-left: 350px;
}

#pendingDonorsTable td{
	height: 20px;
	font-size: 12px;
}

table {
	table-layout: fixed;
	font-size: 12px;
}

td {
  word-wrap: break-word;
}

</style>
 