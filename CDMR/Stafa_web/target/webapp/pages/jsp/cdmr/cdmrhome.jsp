<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/dataSearch.js"></script>
<script>
function getEquipmentMaintentance(){
		response = ajaxCall("loadEquipmentMaintenance","");
	//alert(response);
	$('#main').html(response);
}

function savePages(){
	if(checkModuleRights(STAFA_NURSEHOMEPAGE,APPMODE_SAVE)){
		try{
			if(saveHomePage()){
				return true;
			}
		}catch(e){
			alert("exception " + e);
		}
	}
}
function saveHomePage(){
	//alert("savehome page");
	//1. assigned donor set assign to null, notification to supervisor
	//2. assigned donor to nurse, notification to supervisor
	
	var selfAsignValue = $("#selfAsignValue").val();
	
	var transferValue = $("#transferValue").val();
 	var terminateValue = $("#terminateValue").val(); 
 	var taskActionData = "", transferIds ="",terminateIds="",selfAsignIds="";
 	//alert("transferValue " + transferValue + "/terminateValue"+terminateValue +"/selfAsignValue"+selfAsignValue);
	$("#assignedDonorsTable tbody tr").each(function (){
	//	alert("row " + $(this).html());
		//alert("value" + $(this).find("#taskAction").val());
		if($(this).find("#taskAction").val()== transferValue){
			transferIds+= $(this).find("#plannedprocedure").val() +",";
		}else if($(this).find("#taskAction").val()== terminateValue){
			terminateIds+= $(this).find("#plannedprocedure").val() +",";
		}
	});
	$("#pendingDonorsTable tbody tr").each(function (){
		
		if($(this).find("#taskAsign").val()== selfAsignValue){
			selfAsignIds+= $(this).find("#plannedprocedure").val() +",";
		}
	});
	if(transferIds.length >0){
		transferIds = transferIds.substr(0,transferIds.length-1);
		}
	if(terminateIds.length >0){
		terminateIds = terminateIds.substr(0,terminateIds.length-1);
		}
	if(selfAsignIds.length >0){
		selfAsignIds = selfAsignIds.substr(0,selfAsignIds.length-1);
		}
	taskActionData = "{transferIds:'"+transferIds+"',terminateIds:'"+terminateIds+"',selfAsignIds:'"+selfAsignIds+"'}";
	//alert("taskActionData " + taskActionData);
	url="saveNurseHomePage";
    response = jsonDataCall(url,"jsonData="+taskActionData);
    alert("Data Saved Successfully");
	constructTable(true,'assignedDonorsTable',assignedDonorsTable_ColManager,assignedDonorsTable_serverParam,assignedDonorsTable_aoColumnDef,assignedDonorsTable_aoColumn,assigned_default_Sort);
	constructTable(true,'recurringTasksTable',recurringTasksTable_ColManager,recurringTasksTable_serverParam,recurringTasksTable_aoColumnDef,recurringTasksTable_aoColumn)
	constructTable(true,'pendingDonorsTable',pendingDonorsTable_ColManager,pendingDonorsTable_serverParam,pendingDonorsTable_aoColumnDef,assignedDonorsTable_aoColumn);

}

function getPreparation(donorProcedure,donorId){
	//alert("donorid " + donorId);
	//? $("#dnrsrnpopup").remove();// to remove div for uniqueness
	entityType="apheresis_nurse";
	var entityId = donorProcedure +","+donorId;
	getNextFlow(entityId,entityType,null);
 /*	
	$("#donor").val(donorId);
	
	response = ajaxCall("loadPreparation","aphresis");
	//alert(response);
	$('#main').html(response);
	//url="loadPreparation";
    //response = jsonDataCall(url,"jsonData={donorId:"+donorId+"}");
   */
}


/* Assigned Donor Table Server Parameters Starts */
var assignedCriteria = "and PP_ISASSIGNED =1 and ( instr(','||PP_PRIMARYUSERIDS||',',',"+$("#user").val() +",') >0 or instr(','||PP_SECONDARYUSERIDS||',',',"+$("#user").val()+",') >0 )";
var pendingCriteria = "and nvl(PP_ISASSIGNED,0) =0 ";

var assignedDonorsTable_serverParam = function ( aoData ) {

															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "aphresis_nursehome_asigned"});	
															aoData.push( { "name": "criteria", "value": assignedCriteria});
															
															aoData.push( {"name": "col_1_name", "value": "lower(nvl(p.PERSON_MRN,' '))" } );
															aoData.push( { "name": "col_1_column", "value": "lower(nvl(p.PERSON_MRN,' '))"});
															
															aoData.push( { "name": "col_2_name", "value": "lower(nvl(p.PERSON_LNAME, ' ' )|| ' ' || nvl(p.PERSON_FNAME, ' ' ))"});
															aoData.push( { "name": "col_2_column", "value": "lower(pname)"});
															
															aoData.push( { "name": "col_3_name", "value": "lower(to_char(p.person_dob,'Mon dd, yyyy'))"});
															aoData.push( { "name": "col_3_column", "value": "lower(pdob)"});
															
															aoData.push( { "name": "col_4_name", "value": "lower((select codelst_desc from er_codelst where pk_codelst=p.FK_CODELST_BLOODGRP))"});
															aoData.push( { "name": "col_4_column", "value": "lower(bloodGroup)"});
															
															aoData.push( { "name": "col_5_name", "value": "lower((select codelst_desc from er_codelst where pk_codelst = d.fk_codelstdonortype))"});
															aoData.push( { "name": "col_5_column", "value": "lower(donationtype)"});
															
															aoData.push( { "name": "col_6_name", "value": "lower(nvl((select nvl(PROTOCOL_NAME,' ') from ER_PROTOCOL where PK_PROTOCOL = pp.fk_codelstplannedprocedure),' '))"});
															aoData.push( { "name": "col_6_column", "value": "lower(planprocedure)"});
															
															aoData.push( { "name": "col_7_name", "value": "lower((select nvl(to_char(max(MOBILIZATION_DATE),'Mon DD, YYYY'),' ') from MOBILIZATION where  FK_PLANNEDPROCEDURE =pp.pk_plannedprocedure  ))"});
															aoData.push( { "name": "col_7_column", "value": "lower(mobDate)"});
															
															aoData.push( { "name": "col_8_name", "value": "lower(nvl(pp.PP_PRIMARYUSERNAMES, ''))"});
															aoData.push( { "name": "col_8_column", "value": "lower(nurse)"});
															
															aoData.push( { "name": "col_9_name", "value": "lower((select codelst_desc from er_codelst where pk_codelst =pp.fk_codelstdonorstatus))"});
															aoData.push( { "name": "col_9_column", "value": "lower(status)"});
																										
														};
var pendingDonorsTable_serverParam = function ( aoData ) {

															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "aphresis_nursehome_pending"});	
															
															aoData.push( {"name": "col_1_name", "value": "lower(nvl(p.PERSON_MRN,' '))" } );
															aoData.push( { "name": "col_1_column", "value": "lower(nvl(p.PERSON_MRN,' '))"});
															
															aoData.push( { "name": "col_2_name", "value": "lower(nvl(p.PERSON_LNAME, ' ' )|| ' ' || nvl(p.PERSON_FNAME, ' ' ))"});
															aoData.push( { "name": "col_2_column", "value": "lower(pname)"});
															
															aoData.push( { "name": "col_3_name", "value": "lower(to_char(p.person_dob,'Mon dd, yyyy'))"});
															aoData.push( { "name": "col_3_column", "value": "lower(pdob)"});
															
															aoData.push( { "name": "col_4_name", "value": "lower((select codelst_desc from er_codelst where pk_codelst=p.FK_CODELST_BLOODGRP))"});
															aoData.push( { "name": "col_4_column", "value": "lower(bloodGroup)"});
															
															aoData.push( { "name": "col_5_name", "value": "lower((select codelst_desc from er_codelst where pk_codelst = d.fk_codelstdonortype))"});
															aoData.push( { "name": "col_5_column", "value": "lower(donationtype)"});
															
															aoData.push( { "name": "col_6_name", "value": "lower(nvl((select nvl(PROTOCOL_NAME,' ') from ER_PROTOCOL where PK_PROTOCOL = pp.fk_codelstplannedprocedure),' '))"});
															aoData.push( { "name": "col_6_column", "value": "lower(planprocedure)"});
															
															aoData.push( { "name": "col_7_name", "value": "lower((select nvl(to_char(max(MOBILIZATION_DATE),'Mon DD, YYYY'),' ') from MOBILIZATION where  FK_PLANNEDPROCEDURE =pp.pk_plannedprocedure  ))"});
															aoData.push( { "name": "col_7_column", "value": "lower(mobDate)"});
															
															aoData.push( { "name": "col_8_name", "value": "lower(nvl(pp.PP_PRIMARYUSERNAMES, ''))"});
															aoData.push( { "name": "col_8_column", "value": "lower(nurse)"});
															
															aoData.push( { "name": "col_9_name", "value": "lower((select codelst_desc from er_codelst where pk_codelst =pp.fk_codelstdonorstatus))"});
															aoData.push( { "name": "col_9_column", "value": "lower(status)"});
																										
														};														
var assignedDonorsTable_aoColumnDef = [
		                         {"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
		                             return "<input type='hidden' id='accessionValues' value='"+source[1]+"-=-"+"-=-"+source[8]+"-=-"+source[7]+"'/>";
		                             }},
		                          {    "sTitle":'<s:text name="stafa.label.donorid"/>',
		                                 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
		                                	 var link=source[1];
		                                	
		                                	
		                             return link;
		                             }},
		                          {    "sTitle":'<s:text name="stafa.label.donorname"/>',
		                                 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
		                                	 link = "<a href='#' onclick='javascript:getDonorDetails(\""+source[0] +"\",\""+source[13]+"\");'>"+source[2] +"</a>";
				                             return link;
		                                 
		                                    }},
		                          {    "sTitle":'<s:text name="stafa.label.donordob"/>',
		                                        "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
		                                    return source[3];
		                                  }},
		                          {    "sTitle":'<s:text name="stafa.label.donorabo"/>',
		                                      "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
		                                      return source[4];
		                              }},
		                          {    "sTitle":'<s:text name="stafa.label.donortype"/>',
		                                  "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
		                                      return source[5];
		                              }},
		                          {    "sTitle":'<s:text name="stafa.label.plannedprocedure"/>',
		                                  "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
		                                	  link ="";
		                                	  var linkflag =false;
		                                	  if(source[10] != source[11]){
			                                	 link += "<input type='hidden' name='plannedprocedure' id='plannedprocedure' value='"+source[13]+"' /><a href='#' onclick='javascript:getPreparation(\""+source[13] +"\",\""+source[0]+"\");'>";
			                                	 linkflag = true;
		                                	  }
		                                	  link +=source[6];
		                                	  if(linkflag){
		                                		  link +="</a>";
		                                	  }
		                                      return link;
		                              }},
		                          {    "sTitle":'<s:text name="stafa.label.mobilizationdate"/>',
		                                  "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
		                                      return source[7];
		                              }},
		                              {    "sTitle":'<s:text name="stafa.label.nurse"/>',
		                                  "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
		                                      return source[8];
		                              }},
		                              {    "sTitle":'<s:text name="stafa.label.status"/>',
		                                  "aTargets": [ 9], "mDataProp": function ( source, type, val ) {
		                                      return source[9];
		                              }},
		                              {    "sTitle":'<s:text name="stafa.label.taskactions"/>',
		                                  "aTargets": [ 10], "mDataProp": function ( source, type, val ) {
		                                	  link ="";
		                                	  if(source[10] != source[11]){
		                                	  	link = $("#taskActionspan").html();
		                                	  }
		                                      return link;
		                              }}
		                         
		                        ];
var pendingDonorsTable_aoColumnDef = [
      		                         {"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
      		                             return "<input type='hidden' id='accessionValues' value='"+source[1]+"-=-"+"-=-"+source[8]+"-=-"+source[7]+"'/>";
      		                             }},
      		                          {    "sTitle":'<s:text name="stafa.label.donorid"/>',
      		                                 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
      		                                	 var link=source[1];
      		                                	
      		                                	
      		                             return link;
      		                             }},
      		                          {    "sTitle":'<s:text name="stafa.label.donorname"/>',
      		                                 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
      		                                	 link = "<a href='#' onclick='javascript:getDonorDetails(\""+source[0] +"\",\""+source[13]+"\");'>"+source[2] +"</a>";
      				                             return link;
      		                                 
      		                                    }},
      		                          {    "sTitle":'<s:text name="stafa.label.donordob"/>',
      		                                        "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
      		                                    return source[3];
      		                                  }},
      		                          {    "sTitle":'<s:text name="stafa.label.donorabo"/>',
      		                                      "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
      		                                      return source[4];
      		                              }},
      		                          {    "sTitle":'<s:text name="stafa.label.donortype"/>',
      		                                  "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
      		                                      return source[5];
      		                              }},
      		                          {    "sTitle":'<s:text name="stafa.label.plannedprocedure"/>',
      		                                  "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
      		                                	  link ="<input type='hidden' name='plannedprocedure' id='plannedprocedure' value='"+source[13]+"' />" +source[6];
      		                                	  /*//?BB var linkflag =false;
      		                                	  if(source[10] != source[11]){
      			                                	 link += "<a href='#' onclick='javascript:getPreparation(\""+source[13] +"\",\""+source[0]+"\");'>";
      			                                	 linkflag = true;
      		                                	  }
      		                                	  link +=source[6];
      		                                	  if(linkflag){
      		                                		  link +="</a>";
      		                                	  }
      		                                	  */
      		                                      return link;
      		                              }},
      		                          {    "sTitle":'<s:text name="stafa.label.mobilizationdate"/>',
      		                                  "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
      		                                      return source[7];
      		                              }},
      		                              {    "sTitle":'<s:text name="stafa.label.nurse"/>',
      		                                  "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
      		                                      return source[8];
      		                              }},
      		                              {    "sTitle":'<s:text name="stafa.label.status"/>',
      		                                  "aTargets": [ 9], "mDataProp": function ( source, type, val ) {
      		                                      return source[9];
      		                              }},
      		                              {    "sTitle":'<s:text name="stafa.label.taskactions"/>',
      		                                  "aTargets": [ 10], "mDataProp": function ( source, type, val ) {
      		                                	  link = $("#taskAsignspan").html();
      		                                	// link ="";
      		                                      return link;
      		                              }}
      		                         
      		                        ];

var assignedDonorsTable_aoColumn = [ { "bSortable": false},
                                     null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null
		             ];
var assignedDonorsTable_ColManager = function(){
									//$("#assignedDonorsColumn").html($('.ColVis:eq(0)'));
									 $("#assignedDonorsColumn").html($("#assignedDonorsTable_wrapper .ColVis"));
								 };
var pendingDonorsTable_ColManager = function(){
										//$("#pendingDonorsColumn").html($('.ColVis:eq(1)'));
										 $("#pendingDonorsColumn").html($("#pendingDonorsTable_wrapper .ColVis"));
									 };								 
 var assigned_default_Sort = [[9,"asc"]];
/*Assigned Donor Table Server Parameters Ends*/

/*Recurring Task Table Server Parameters Starts*/
 	var recurringCriteria = " and RT_USER ="+$("#user").val();
 	var recurringTasksTable_ColManager = function () {
										        $("#recurringTasksColumn").html($('#recurringTasksTable_wrapper .ColVis'));
											};
	var recurringTasksTable_aoColumnDef = [
	 								
									 {  "aTargets": [ 0], "mDataProp": function ( source, type, val ) {
										return "<input type='hidden' id='recurringTask' value='"+source[0]+"' />";
										 
									 }},
									 {    "sTitle":'Task',
									    "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
									    	if(source[1]=="Equipment Maintenance"){
									   			 var link = "<a href='#' onclick='javascript:getEquipmentMaintentance();'>"+source[1] +"</a>";
				                             return link;
									    	}else{
									    		return source[1];
									    	}
									 }},
									 {    "sTitle":'Description',
									    "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
									     return source[2];
									 
									 }},
									 {    "sTitle":'User Assignments',
									    "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
									    	var link = "<input type='hidden' id='tmpUser' name='tmpUser' value='"+source[3]+"'/><input type='hidden' id='tmpTaskLevel' name='tmpTaskLevel' value='"+source[4]+"'/>"+ $("#nurseUserspan #nurseUser option[value='"+source[3]+"']").text();
									    	link = link + "&nbsp;" +  $("#tasklevelspan #rtCodelstTaskLevel option[value='"+source[4]+"']").text();
									    //return source[3];
									      return link;
									 }},
									 {    "sTitle":'Start Date',
									    "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
									    		var	startDate = ($.datepicker.formatDate('M dd, yy', new Date(source[5])));
									    return startDate;
									 }},
									 {    "sTitle":'Due Date',
									        "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
									    		var	dueDate = ($.datepicker.formatDate('M dd, yy', new Date(source[6])));
									    return dueDate;
									 }},
									 {    "sTitle":'Frequency',
									        "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
									    return source[7];
									 }} ,
									 {    "sTitle":'Status',
									        "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
									    		
									    return source[8];
									 }}
										
								];	
	var recurringTasksTable_serverParam = function ( aoData ) {
														aoData.push( { "name": "application", "value": "stafa"});
														aoData.push( { "name": "module", "value": "recurring_task"});
														aoData.push( { "name": "criteria", "value": recurringCriteria});
														
														aoData.push( {"name": "col_1_name", "value": "lower((select codelst_desc from er_codelst where pk_codelst=FK_CODELSTTASK))" } );
														aoData.push( { "name": "col_1_column", "value": "lower(codeTask)"});
														
														aoData.push( { "name": "col_2_name", "value": "lower(rt_Description)"});
														aoData.push( { "name": "col_2_column", "value": "lower(rt_Description)"});
														
														aoData.push( { "name": "col_3_name", "value": "lower(rt_User)"});
														aoData.push( { "name": "col_3_column", "value": "lower(rt_User)"});
														
														aoData.push( { "name": "col_4_name", "value": "lower(nvl(RT_STARTDATE,''))"});
														aoData.push( { "name": "col_4_column", "value": "lower(starDate)"});
														
														aoData.push( { "name": "col_5_name", "value": "lower(nvl(RT_DUEDATE,''))"});
														aoData.push( { "name": "col_5_column", "value": "lower(dueDate)"});
														
														aoData.push( { "name": "col_6_name", "value": "lower((select codelst_desc from er_codelst where pk_codelst=FK_CODELSTFREQUENCY))"});
														aoData.push( { "name": "col_6_column", "value": "lower(codelstFreq)"});
														
														//?BB aoData.push( { "name": "col_7_name", "value": "STATUS"});
														aoData.push( { "name": "col_7_column", "value": "9"});
														
													};
	var recurringTasksTable_aoColumn = 	[ { "bSortable":false},
							               null,
							               null,
							               null,
							               null,
							               null,
							               null,
							               null
							             ];

/*Recurring Task Server Paramaters Ends Here*/

$(document).ready(function(){
	$(".workFlowContainer").html("");
	$(".titleText").html("");
	hide_slidewidgets();
	hide_slidecontrol();
	show_innernorth();
	$('#inner_center').scrollTop(0);
	constructTable(false,'assignedDonorsTable',assignedDonorsTable_ColManager,assignedDonorsTable_serverParam,assignedDonorsTable_aoColumnDef,assignedDonorsTable_aoColumn,assigned_default_Sort);
	constructTable(false,'recurringTasksTable',recurringTasksTable_ColManager,recurringTasksTable_serverParam,recurringTasksTable_aoColumnDef,recurringTasksTable_aoColumn)
	constructTable(false,'pendingDonorsTable',pendingDonorsTable_ColManager,pendingDonorsTable_serverParam,pendingDonorsTable_aoColumnDef,assignedDonorsTable_aoColumn);
 	/*
	oTable=$("#otherTasksTable").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
										               null,
										               null,
										               null,
										               null,
										               null,
										               null,
										               null,
										               null
										             ],
											"oColVis": {
												"aiExclude": [ 0 ],
												"buttonText": "&nbsp;",
												"bRestore": true,
												"sAlign": "left"
											},
											"fnInitComplete": function () {
										        $("#ohterTasksColumn").html($('.ColVis:eq(1)'));
											}
	});
 	*/
$("select").uniform(); 
});
/* Scrolling Method Starts*/
$("#assignDonorDiv").scroll(function(){
		 var newMargin = $(this).scrollLeft();  
		 $('#assignedDonorsTable_length').css('marginLeft', newMargin);
		 $('#assignedDonorsTable_length').css('width', '20%');
		 $('#assignedDonorsTable_filter').css('width', '20%');
		 $('#assignedDonorsTable_info').css('marginLeft', newMargin);
		 $('#assignedDonorsTable_info').css('width', '40%');
	});
$("#taskDiv").scroll(function(){
	 var newMargin = $(this).scrollLeft();  
	 $('#recurringTasksTable_length').css('marginLeft', newMargin);
	 $('#recurringTasksTable_length').css('width', '20%');
	 $('#recurringTasksTable_filter').css('width', '20%');
	 $('#recurringTasksTable_info').css('marginLeft', newMargin);
	 $('#recurringTasksTable_info').css('width', '40%');
});
</script>
<div class="cleaner"></div>

<span id="hiddendropdowns" class="hidden">
 <span id="taskActionspan">
 <s:select id="taskAction"  name="taskAction" list="lstTaskAction" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" />
 
</span>

<span id="taskAsignspan">
 <s:select id="taskAsign"  name="taskAsign" list="lstTaskAsign" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" />
</span>

 <span id="nurseUserspan">
<select id="nurseUser"  name="nurseUser" >
	<option value="">Select</option> 
 <s:iterator value="lstNurseUsers" var="rowVal" status="row">
 	<option value="<s:property value="#rowVal[0]"/>"> <s:property value="#rowVal[1]"/> <s:property value="#rowVal[2]"/></option>
 </s:iterator>
</select>
</span>


</span>
 
 <div class="column">		
 <form id="aphresis" name="aphresis">
  <input type="hidden" id="donor" name="donor" value=""/>
  <input type="hidden" id="donorProcedure" name="donorProcedure" value=""/>
  <s:hidden id="user" name="user" value="%{user}" /> 
   <s:hidden id="transferValue" name="transferValue" value="%{transferValue}" /> 
   <s:hidden id="terminateValue" name="terminateValue" value="%{terminateValue}" /> 
   <s:hidden id="selfAsignValue" name="selfAsignValue" value="%{selfAsignValue}" /> 
  	<div  class="portlet">
	<div class="portlet-header "><s:text name="vcdmr.label.fileupload"/></div>
		<div class="portlet-content">
		<div id="assignDonorDiv" style="overflow-x:auto;overflow-y:hidden;">
			<!-- -File Upload Form -->
			</div>
		</div>
	</div>	
	
	<div  class="portlet">
	<div class="portlet-header ">Pending Donors</div>
		<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="pendingDonorsTable" class="display">
			<thead>
			   <tr>
			   		<th width="4%" id="pendingDonorsColumn"></th>
					<th colspan="1"><s:text name="stafa.label.donorid"/></th>
					<th colspan="1"><s:text name="stafa.label.donorname"/></th>
					<th colspan="1"><s:text name="stafa.label.donordob"/></th>
					<th colspan="1"><s:text name="stafa.label.donorabo"/></th>
					<th colspan="1"><s:text name="stafa.label.donationtype"/></th>
					<th colspan="1"><s:text name="stafa.label.plannedprocedure"/></th>
					<th colspan="1"><s:text name="stafa.label.mobilizationdate"/></th>
					<th colspan="1"><s:text name="stafa.label.nurse"/></th>
					<th colspan="1"><s:text name="stafa.label.status"/></th>
					<th colspan="1"><s:text name="stafa.label.taskactions"/></th>
				</tr>
			</thead>
			<tbody>

			</tbody>
			</table>
		</div>
	</div>	
	
</form>	
</div>		

<div class="cleaner"></div>
 
 <div class="column">		
	<div  class="portlet">
	<div class="portlet-header ">Recurring Tasks</div>
		<div class="portlet-content">
		<div id="taskDiv" style="overflow-x:auto;overflow-y:hidden;">
			<table cellpadding="0" cellspacing="0" border="0" id="recurringTasksTable" class="display">
			<thead>
			   <tr>
			   		<th width="4%" id="recurringTasksColumn"></th>
				    <th>Task</th>
					<th>Description</th>
					<th>User Assignments</th>
					<th>Start Date</th>
					<th>Due Date</th>
					<th>Frequency</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<!-- //?BB to do -->
			</tbody>
			</table>
			</div>
		</div>
	</div>	
</div>	
<!-- 
<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td>Next:</td>
				<td><div><div class="mainselection"><select name="" id=""><option value="">Select</option><option>Preparation</option><option>Verification</option></select></div></div></td>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="nextProcessPreProduct();" value="Next"/></td>
				</tr>
				</table>
				</form>
		</div>
 -->
<script>

$(function() {
	
	$( ".column" ).sortable({
		connectWith: ".column"
			/*,
        handle: '.portlet-header',
        cursor: 'move',
        forcePlaceholderSize: true*/
			
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});

	/**For Notes**/
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

	/* $( ".portlet-header .ui-addnew" ).click(function() {
		showPopUp("open","addNewDialog","Add New Acquision","900","450")
	}); */
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");
	$( ".portlet-header .ui-reset " ).click(function() {
		var formName = document.getElementById("login");
		//clearForm(formName);
		clear();
	});

// 	$( ".portlet-header .ui-notes " ).click(function() {
// 		showNotes('open');
// 	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});



</script>
<style>
	
td {
  word-wrap: break-word;
}
	
</style>
 