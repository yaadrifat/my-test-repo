<%@ include file="common/includes.jsp" %>
<script>

	var donor,donorProcedure;
$(document).ready(function(){
	donor= $("#donor").val();
	donorProcedure = $("#donorProcedure").val();
	//alert("donor::"+donor+"donorPr:;"+donorProcedure);
	constructAnticoagulantsTable(false,donor,donorProcedure);
});
	function savePopup(){
		if(saveAnticoagulants()){
			return true;
		}
	}

	function loadTmpDropdown(obj,fieldName,criteria){
		 url="loadDropdown";
		 response = jsonDataCall(url,"jsonData={fieldName:\""+fieldName+"\",criteria:\""+criteria+"\"}");
		 $("#tmpSelect").find('option').remove().end().append('<option value="">Select</option>');
		 $(response.dropdownList).each(function(i){
			 $("#tmpSelect").find('option').end().append('<option value="'+this+ '">'+ this+'</option>');
		 });	 
		
		 
	} 
	
	function loadDropdown(obj,name){
		//alert("loadDropdown");
		try{
		 url="loadDropdown";
		 var fieldName="";
		 if($.trim($(obj).val()) ==""){
			 return false;
		 }
		 
		 var row = $(obj).parent().closest("tr");
		 var tmpValue="";
		 var criteria=" and ";
		 if(name=="antigulantsLot"){
			 criteria+= " rs_type='REAGENTS'  and name ='"+$.trim($(obj).val())+"'";
			 fieldName = "lotNumber";
			 $(row).find("#expiryDate").val("");
		 }else if(name=="expiryDate"){
			 criteria+= " rs_type='REAGENTS'  and name ='"+$.trim($(row).find("#antigulantsName").val()) +"' and lotNumber='"+$.trim($(obj).val())+"' ";
			 fieldName = "to_char(expDate,'Mon DD, YYYY'),reagentsupplies";
			 $(row).find("#expiryDate").val("");
		 }else if(name== "additivesLot"){
			 criteria+= " rs_type='REAGENTS'  and name ='"+$.trim($(obj).val())+"'";
			 fieldName = "lotNumber";
			 $(row).find("#additivesExpiryDate").val("");
		 }else if(name== "additivesExpiryDate"){
			 criteria+= " rs_type='REAGENTS'  and name ='"+$.trim($(row).find("#additivesName").val()) +"' and lotNumber='"+$.trim($(obj).val())+"' ";
			 fieldName = "to_char(expDate,'Mon DD, YYYY'),reagentsupplies";
			 $(row).find("#additivesExpiryDate").val("");
		 }
		 
		 $('.progress-indicator').css( 'display', 'block' );
		 response = jsonDataCall(url,"jsonData={fieldName:\""+fieldName+"\",criteria:\""+criteria+"\"}");
		
		
		 if(name== "expiryDate"){
	 		$(response.dropdownList).each(function(i){
				 if(jQuery.type(this[0]) == "string"){
				 	$(row).find("#"+name).val(this[0]);
				 	$(row).find("#reagentsupplies").val(this[1]);
				 }
				 
			 });
		 }else if (name=="additivesExpiryDate"){
			 $(response.dropdownList).each(function(i){
					 if(jQuery.type(this[0]) == "string"){
					 	$(row).find("#"+name).val(this[0]);
					 	$(row).find("#reagentsupplies").val(this[1]);
					 }
					 
				 });
		} else{
			 $(row).find("#"+name).find('option').remove().end()
			    .append('<option value="">Select</option>');
			 $(response.dropdownList).each(function(i){
				 $(row).find("#"+name).find('option').end().append('<option value="'+this+ '">'+ this+'</option>');
			 });
			
		 }
		  $.uniform.restore('select');
	      $("select").uniform();
	      $('.progress-indicator').css( 'display', 'none' );
	      
		}catch(e){
			alert("Error : "+e);
		}
	}
	
	function addnewanticoagulants(){
		var newRow;
		 		newRow =  "<tr><td></td>"+
			              "<td><input type='checkbox' id='anticoagulantsrow' name='anticoagulantsrow' value='0'><input type='hidden' id='antigulants' name='antigulants' value='0'/></td>"+
			              "<td><select class='select1'  id='antigulantsName' name='antigulantsName' onchange='javascript:loadDropdown(this,\"antigulantsLot\");'>"+$("#reagent_hidden").html() + "</select></td>"+
			              "<td><div><select class='select1' id='antigulantsLot' name='antigulantsLot' onchange='javascript:loadDropdown(this,\"expiryDate\");' ></select></div></td>"+
			              "<td><input type='text' class='dateEntry dateclass'id='expiryDate' name='expiryDate'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/></td>"+
			              "<td><input type='text' id='antigulantsVolume' name='antigulantsVolume'/></td>"+
			              "<td><input type='text' id='antigulantsAdditionDate' name='antigulantsAdditionDate' class='dateEntry'/></td>"+
			              "<td><input type='hidden' id='antigulantsAdditionTime' name='antigulantsAdditionTime'/><input type='text' id='tempantigulantsAdditionTime' name='tempantigulantsAdditionTime' class='antitimepicker' onchange='setanticoagulantstime()'/></td></td>"+
			              "<td><input type='text' id='antigulantsComments' name='antigulantsComments'/></td>"+
			              "</tr>";
			             
		$("#anticoagulantsTable tbody").prepend(newRow);
		
		$("#anticoagulantsTable .ColVis_MasterButton").triggerHandler("click");
		$('.ColVis_Restore:visible').triggerHandler("click");
		$("div.ColVis_collectionBackground").trigger("click");
		
		$("select").uniform();
		$(".dateEntry").datepicker({
				dateFormat: 'M dd, yy',
				changeMonth: true,
				changeYear: true, 
				yearRange: "c-50:c+45"
			});
		$(".antitimepicker").timepicker({});
		 var param ="{module:'APHERESIS',page:'POSTCOLLECTION'}";
			markValidation(param);
	  }
	  function addnewadditives(){
			var newRow;
		 		newRow =  "<tr><td></td>"+
			              "<td><input type='checkbox' id='additivesrow' name='additivesrow' value='0'><input type='hidden' id='additives' name='additives' value='0'/></td>"+
			              "<td><select class='select1'  id='additivesName' name='additivesName' onchange='javascript:loadDropdown(this,\"additivesLot\");'>"+$("#reagent_hidden").html() + "</select></td>"+
			              "<td><div><select class='select1' id='additivesLot' name='additivesLot' onchange='javascript:loadDropdown(this,\"additivesExpiryDate\");' ></select></div></td>"+
			              "<td><input type='text' class='dateEntry dateclass'id='additivesExpiryDate' name='additivesExpiryDate'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/></td>"+
			              "<td><input type='text' id='additivesVolume' name='additivesVolume'/></td>"+
			              "<td><input type='text' id='additivesAdditionDate' name='additivesAdditionDate' class='dateEntry'/></td>"+
			              "<td><input type='hidden' id='additivesAdditionTime' name='additivesAdditionTime'/><input type='text' id='tempadditivesAdditionTime' name='tempadditivesAdditionTime' class='antitimepicker' onchange='setadditivetime()'/></td></td>"+
			              "<td><input type='text' id='additivesComments' name='additivesComments'/></td>"+
			              "</tr>";
			                    
			$("#additiveTable tbody").prepend(newRow);
			
			$("#anticoagulantsTable .ColVis_MasterButton").triggerHandler("click");
			$('.ColVis_Restore:visible').triggerHandler("click");
			$("div.ColVis_collectionBackground").trigger("click");
			
			$("select").uniform();
			$(".dateEntry").datepicker({
					dateFormat: 'M dd, yy',
					changeMonth: true,
					changeYear: true, 
					yearRange: "c-50:c+45"
				});
			$(".antitimepicker").timepicker({});
			 var param ="{module:'APHERESIS',page:'POSTCOLLECTION'}";
				markValidation(param);
		 }
		  function setadditivetime(){
			  $('#additivesAdditionTime').val($('#additivesAdditionDate').val() +" "+ $('#tempadditivesAdditionTime').val());
		  }
		  function setanticoagulantstime(){
			  $('#antigulantsAdditionTime').val($('#antigulantsAdditionDate').val() +" "+ $('#tempantigulantsAdditionTime').val());
		  }
	 function deleteanticoagulantsData(){
			// alert("Delete Rows");
		    var donorVal=$("#donor").val();
			var donorProcedure=$("#donorProcedure").val();
			var yes=confirm(confirm_deleteMsg);
			if(!yes){
				return;
			}
			var deletedIds = ""
			$("#anticoagulantsTable #antigulants:checked").each(function (row){
				deletedIds += $(this).val() + ",";
			});
			if(deletedIds.length >0){
			  deletedIds = deletedIds.substr(0,deletedIds.length-1);
			}
			jsonData = "{deletedAnticoagulantsResult:'"+deletedIds+"'}";
			url="deleteAnticoagulantsData";
		    response = jsonDataCall(url,"jsonData="+jsonData);
			constructAnticoagulantsTable(true,donorVal,donorProcedure);
			
		}
	 function deleteadditivesData(){
			//alert("Delete Rows");
			var donorVal=$("#donor").val();
			var donorProcedure=$("#donorProcedure").val();
		    var yes=confirm(confirm_deleteMsg);
			if(!yes){
				return;
			}
			var deletedIds = ""
			$("#additiveTable #additives:checked").each(function (row){
				deletedIds += $(this).val() + ",";
			});
			if(deletedIds.length >0){
			  deletedIds = deletedIds.substr(0,deletedIds.length-1);
			}
			jsonData = "{deletedAdditivesResult:'"+deletedIds+"'}";
			url="deleteAdditivesData";
		    response = jsonDataCall(url,"jsonData="+jsonData);
		    constructAnticoagulantsTable(true,donorVal,donorProcedure);
		}
	 function editAnticoagulants(){
		 // alert("edit Rows");
		 var colObj;
		  $("#anticoagulantsTable tbody tr").each(function (row){
			 
			 if($(this).find("#antigulants").is(":checked")){
				 $(this).find("td").each(function (col){
					 if(col ==2 ){
						 if ($(this).find("#antigulantsName").val()== undefined){
							 tmpValue = $(this).text();
							$(this).html("<select class='select1'  id='antigulantsName' name='antigulantsName' onchange='javascript:loadDropdown(this,\"antigulantsLot\");'>"+$("#reagent_hidden").html() + "</select>");
							$(this).find("#reagentsname").val(tmpValue);
							colObj = $(this).find("#antigulantsName");
						 }
					 }else if(col ==3){
						 if ($(this).find("#antigulantsLot").val()== undefined){
							 tmpValue = $(this).text();
						 	$(this).html("<div><select class='select1' id='antigulantsLot' name='antigulantsLot' onchange='javascript:loadDropdown(this,\"expiryDate\");' ></select></div>");
						 	loadDropdown($(colObj),"antigulantsLot");
						 	$(this).find("#antigulantsLot").val(tmpValue);
						 	colObj = $(this).find("#antigulantsLot");
						 }
					 }else if(col ==4){
						 if ($(this).find("#expiryDate").val()== undefined){
							 tmpValue = $(this).text();
							 	$(this).html("<input type='text' class='dateEntry dateclass'id='expiryDate' name='expiryDate' value='"+$(this).text()+ "'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/>");
							 	$(this).find("#expiryDate").val(tmpValue);
							 }
					}else if(col ==5){
						 if ($(this).find("#antigulantsVolume").val()== undefined){
							 	tmpValue = $(this).text();
							 	$(this).html("<input type='text' id='antigulantsVolume' name='antigulantsVolume' value='"+$(this).text()+ "'/>");
							 	$(this).find("#antigulantsVolume").val(tmpValue);
							 }
					}else if(col ==6){
						 if ($(this).find("#antigulantsAdditionDate").val()== undefined){
							 	tmpValue = $(this).text();
							 	$(this).html("<input type='text' id='antigulantsAdditionDate' name='antigulantsAdditionDate' class='dateEntry'value='"+$(this).text()+ "'/>");
							 	$(this).find("#antigulantsAdditionDate").val(tmpValue);
							 }
					}else if(col ==7){   
						 if ($(this).find("#antigulantsAdditionTime").val()== undefined){
							 	tmpValue = $(this).text();
							 	$(this).html("<input type='hidden' id='antigulantsAdditionTime' name='antigulantsAdditionTime' value='"+$(this).text()+ "'/><input type='text' id='tempantigulantsAdditionTime' name='tempantigulantsAdditionTime' class='antitimepicker' onchange='setanticoagulantstime()'/>");
							 	$(this).find("#antigulantsAdditionTime").val(tmpValue);
							 }
					}else if(col ==8){   
						 if ($(this).find("#antigulantsComments").val()== undefined){
							 	tmpValue = $(this).text();
							 	$(this).html("<input type='text' id='antigulantsComments' name='antigulantsComments' value='"+$(this).text()+ "'/>");
							 	$(this).find("#antigulantsComments").val(tmpValue);
							 }
					}
				 });
			 }
		  });
	      $.uniform.restore('select');
	      $("select").uniform();
	      $(".dateEntry").datepicker({
				dateFormat: 'M dd, yy',
				changeMonth: true,
				changeYear: true, 
				yearRange: "c-50:c+45"
			});
		  $(".antitimepicker").timepicker({});
	     
	  }
	 function editAdditives(){
		 // alert("edit Rows");
		 var colObj;
		  $("#additiveTable tbody tr").each(function (row){
			 
			 if($(this).find("#additives").is(":checked")){
				 $(this).find("td").each(function (col){
					 if(col ==2 ){
						 if ($(this).find("#additivesName").val()== undefined){
							 tmpValue = $(this).text();
							$(this).html("<select class='select1'  id='additivesName' name='additivesName' onchange='javascript:loadDropdown(this,\"additivesLot\");'>"+$("#reagent_hidden").html() + "</select>");
							$(this).find("#additivesName").val(tmpValue);
							colObj = $(this).find("#additivesName");
						 }
					 }else if(col ==3){
						 if ($(this).find("#additivesLot").val()== undefined){
							 tmpValue = $(this).text();
						 	$(this).html("<div><select class='select1' id='additivesLot' name='additivesLot' onchange='javascript:loadDropdown(this,\"additivesExpiryDate\");' ></select></div>");
						 	loadDropdown($(colObj),"additivesLot");
						 	$(this).find("#additivesLot").val(tmpValue);
						 	colObj = $(this).find("#additivesLot");
						 }
					 }else if(col ==4){
						 if ($(this).find("#additivesExpiryDate").val()== undefined){
							 tmpValue = $(this).text();
							 	$(this).html("<input type='text' class='dateEntry dateclass'id='additivesExpiryDate' name='additivesExpiryDate' value='"+$(this).text()+ "'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/>");
							 	$(this).find("#additivesExpiryDate").val(tmpValue);
							 }
					}else if(col ==5){
						 if ($(this).find("#additivesVolume").val()== undefined){
							 	tmpValue = $(this).text();
							 	$(this).html("<input type='text' id='additivesVolume' name='additivesVolume' value='"+$(this).text()+ "'/>");
							 	$(this).find("#additivesVolume").val(tmpValue);
							 }
					}else if(col ==6){
						 if ($(this).find("#additivesAdditionDate").val()== undefined){
							 	tmpValue = $(this).text();
							 	$(this).html("<input type='text' id='additivesAdditionDate' name='additivesAdditionDate' class='dateEntry' value='"+$(this).text()+ "'/>");
							 	$(this).find("#additivesAdditionDate").val(tmpValue);
							 }
					}else if(col ==7){   
						 if ($(this).find("#additivesAdditionTime").val()== undefined){
							 	tmpValue = $(this).text();
							 	$(this).html("<input type='hidden' id='additivesAdditionTime' name='additivesAdditionTime' value='"+$(this).text()+ "'/><input type='text' id='tempadditivesAdditionTime' name='tempadditivesAdditionTime' class='antitimepicker' onchange='setadditivetime()'/>");
							 	$(this).find("#additivesAdditionTime").val(tmpValue);
							 }
					}else if(col ==8){   
						 if ($(this).find("#additivesComments").val()== undefined){
							 	tmpValue = $(this).text();
							 	$(this).html("<input type='text' id='additivesComments' name='additivesComments' value='"+$(this).text()+ "'/>");
							 	$(this).find("#additivesComments").val(tmpValue);
							 }
					}
				 });
			 }
		  });
	     $.uniform.restore('select');
	     $("select").uniform();
	     $(".dateEntry").datepicker({
				dateFormat: 'M dd, yy',
				changeMonth: true,
				changeYear: true, 
				yearRange: "c-50:c+45"
			});
		 $(".antitimepicker").timepicker({});
	 }
	 function saveAnticoagulants(){
			var rowData = "";
			var donorVal=$("#donor").val();
			var donorProcedure=$("#donorProcedure").val();
			 $("#anticoagulantsTable").find("#antigulantsName").each(function (row){
				 rowHtml =  $(this).closest("tr");	 
				antiname = "";
				antilot="";
				antiexpdate="";
				antivolume="";
				antiadmdate="";
				antiadmtime="";
				anticomments="";
				antiname=$(rowHtml).find("#antigulantsName").val();
				antiid=$(rowHtml).find("#antigulants").val()
			 		antilot=$(rowHtml).find("#antigulantsLot").val();
					antiexpdate=$(rowHtml).find("#expiryDate").val();
			 		antivolume=$(rowHtml).find("#antigulantsVolume").val()
			 		antiadmdate=$(rowHtml).find("#antigulantsAdditionDate").val();
			 		antiadmtime=$(rowHtml).find("#antigulantsAdditionTime").val()
			 		anticomments=$(rowHtml).find("#antigulantsComments").val();
			 		
				 	  	rowData+="{antigulants:"+antiid+",antigulantsVolume:'"+antivolume+"',antigulantsName:'"+antiname+"',antigulantsLot:'"+antilot+"',expiryDate:'"+antiexpdate+"',antigulantsAdditionDate:'"+antiadmdate+"',antigulantsAdditionTime:'"+antiadmtime+"',antigulantsComments:'"+anticomments+"',donor:'"+donorVal+"',donorProcedure:'"+donorProcedure+"'},";
			 		
			 });
			 
			//alert("row data " + rowData)
			 if(rowData.length >0){
		            rowData = rowData.substring(0,(rowData.length-1));
		        }
			
			var rowData1="";
			var adname;
			var adlot;
			var adexpdate;
			var advolume;
			var addiadmdate;
			var addiadmtime;
			var adcomments;
			$("#additiveTable").find("#additivesName").each(function (row){
				 rowHtml =  $(this).closest("tr");	 
				 	adname="";
					adlot="";
					adexpdate="";
					advolume="";
					addiadmdate="";
					addiadmtime="";
					adcomments="";
					adname=$(rowHtml).find("#additivesName").val();
					addid=$(rowHtml).find("#additives").val()
			 		adlot=$(rowHtml).find("#additivesLot").val();
					adexpdate=$(rowHtml).find("#additivesExpiryDate").val();
					advolume=$(rowHtml).find("#additivesVolume").val()
			 		addiadmdate=$(rowHtml).find("#additivesAdditionDate").val();
					addiadmtime=$(rowHtml).find("#additivesAdditionTime").val()
			 		adcomments=$(rowHtml).find("#additivesComments").val();
			 		
				 	  	rowData1+="{additives:"+addid+",additivesName:'"+adname+"',additivesLot:'"+adlot+"',additivesExpiryDate:'"+adexpdate+"',additivesVolume:'"+advolume+"',additivesAdditionDate:'"+addiadmdate+"',additivesAdditionTime:'"+addiadmtime+"',additivesComments:'"+adcomments+"',donor:'"+donorVal+"',donorProcedure:'"+donorProcedure+"'},";
			 		
			 });
			 
			//alert("row data " + rowData1)
			 if(rowData1.length >0){
		            rowData1 = rowData1.substring(0,(rowData1.length-1));
		        }
			
			
			 url = "saveAnticoagulants";
			 var jsonData = "jsonData={anticoagulantsData:[" + rowData + "],additivesData:["+ rowData1 + "]}";
		//alert("jsonData====>"+jsonData);
		$('.progress-indicator').css('display', 'block');

		response = jsonDataCall(url, jsonData);
		constructAnticoagulantsTable(true,donorVal,donorProcedure);

		$('.progress-indicator').css('display', 'none');
		//alert("Data saved Successfully");
		stafaAlert('Data saved Successfully','');
	}
	function constructAnticoagulantsTable(flag,donor,donorProcedure){
			//alert("consMethod");
			var criteria = "and fk_donor="+donor+"and fk_plannedprocedure="+donorProcedure;
			
			/*Anticoagulants ServerParameters Starts*/
			var anticoagulants_ColManager = function(){
		        										$("#anticoagulantsColumn").html($('#anticoagulantsTable_wrapper .ColVis'));
												 };
										 
			var anticoagulants_aoColumn = [ { "bSortable":false},
				         		            { "bSortable":false},
				    		               	null,
				    			            null,
				    			            null,
				    			            null,
				    			            null,
				    			            null,
				    			            null
			    			             ];	
			var anticoagulantsServerParams = function ( aoData ) {
																	aoData.push( { "name": "application", "value": "stafa"});
																	aoData.push( { "name": "module", "value": "popup_anticoagulants"});
																	aoData.push( { "name": "criteria", "value": criteria});
																	
																	aoData.push( {"name": "col_1_name", "value": "" } );
																	aoData.push( { "name": "col_1_column", "value": ""});
																	
																	aoData.push( { "name": "col_2_name", "value": ""});
																	aoData.push( { "name": "col_2_column", "value": ""});												
																};
																
			var anticoagulants_aoColumnDef = [
				                         {		"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
				                             		return "";
				                             }},
				                          {	"sTitle":'Check All <input type="checkbox"/>',
				                            	"aTargets": [ 1], "mDataProp": function ( source, type, val ) {
				                             		return "<input name='antigulants' id= 'antigulants' type='checkbox' value='"+source.antigulants+"' />";
				                            }},
				                          {	"sTitle":'Name',
				                            	"aTargets": [ 2], "mDataProp": function ( source, type, val ) {
				                                    return source.antigulantsName;
				                           }},
				                          {	"sTitle":'Lot#',
				                            	"aTargets": [ 3], "mDataProp": function ( source, type, val ) {
				                                    return source.antigulantsLot;
				                           }},
				                          {	"sTitle":'Exp.Date',
				                            	"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
				                                    return converHDate(source.expiryDate);
				                           }},
				                          {	"sTitle":'Volume',
				                            	"aTargets": [ 5], "mDataProp": function ( source, type, val ) {
				                                    return source.antigulantsVolume;
				                           }},
				                          {	"sTitle":'Date of Addition',
				                            	"aTargets": [ 6], "mDataProp": function ( source, type, val ) {
				                                    return converHDate(source.antigulantsAdditionDate);
				                           }},
				                         
				                          {	"sTitle":'Time of Addition',
				                            	"aTargets": [ 7], "mDataProp": function ( source, type, val ) {
				                                    return convertDateTime1(source.antigulantsAdditionTime);
				                           }},
				                            
				                          {	"sTitle":'Comment',
				                            	"aTargets": [ 8], "mDataProp": function ( source, type, val ) {
				                                    return source.antigulantsComments;
				                           }}
				                           
				                        ];
			
			/*Additives Table ServerParameters Starts*/
			var additives_ColManager = function(){
													$("#additivesColumn").html($('#additiveTable_wrapper .ColVis'));
		 										 };	
			var additives_aoColumn = [ 		{ "bSortable":false},
			         		                { "bSortable":false},
			    		               	    null,
			    			                null,
			    			                null,
			    			                null,
			    			                null,
			    			                null,
			    			                null
			    			             ];
			var additiveServerParams = function ( aoData ) {
																	aoData.push( { "name": "application", "value": "stafa"});
																	aoData.push( { "name": "module", "value": "popup_additives"});
																	aoData.push( { "name": "criteria", "value": criteria});
																	
																	aoData.push( {"name": "col_1_name", "value": "" } );
																	aoData.push( { "name": "col_1_column", "value": ""});
																	
																	aoData.push( { "name": "col_2_name", "value": ""});
																	aoData.push( { "name": "col_2_column", "value": ""});												
																};
																
			var additives_aoColumnDef = [
				                         {		"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
				                             		return "";
				                             }},
				                          {	"sTitle":'Check All <input type="checkbox"/>',
				                            	"aTargets": [ 1], "mDataProp": function ( source, type, val ) {
				                             		return "<input name='additives' id= 'additives' type='checkbox' value='"+source.additives+"' />";
				                            }},
				                          {	"sTitle":'Name',
				                            	"aTargets": [ 2], "mDataProp": function ( source, type, val ) {
				                                    return source.additivesName;
				                           }},
				                          {	"sTitle":'Lot#',
				                            	"aTargets": [ 3], "mDataProp": function ( source, type, val ) {
				                                    return source.additivesLot;
				                           }},
				                          {	"sTitle":'Exp.Date',
				                            	"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
				                                    return  converHDate(source.additivesExpiryDate);
				                           }},
				                          {	"sTitle":'Volume',
				                            	"aTargets": [ 5], "mDataProp": function ( source, type, val ) {
				                                    return source.additivesVolume;
				                           }},
				                          {	"sTitle":'Date of Addition',
				                            	"aTargets": [ 6], "mDataProp": function ( source, type, val ) {
				                                    return converHDate(source.additivesAdditionDate);
				                           }},
				                         
				                          {	"sTitle":'Time of Addition',
				                            	"aTargets": [ 7], "mDataProp": function ( source, type, val ) {
				                                    return convertDateTime1(source.additivesAdditionTime);
				                           }},
				                            
				                          {	"sTitle":'Comment',
				                            	"aTargets": [ 8], "mDataProp": function ( source, type, val ) {
				                                    return source.additivesComments;
				                           }}
				                          
				                        ];
					constructTable(flag,'anticoagulantsTable',anticoagulants_ColManager,anticoagulantsServerParams,anticoagulants_aoColumnDef,anticoagulants_aoColumn);
					constructTable(flag,'additiveTable',additives_ColManager,additiveServerParams,additives_aoColumnDef,additives_aoColumn);
			}	
	function closeAnticoagulants(){
		$("#modalpopup").dialog('close');
		
	}

</script>

<div id="anticoagulantsAdditives" title="addNew">
<form>
<s:hidden id="donor" name="donor" value="%{donor}"/>
<s:hidden id="donorProcedure" name="donorProcedure" value="%{donorProcedure}"/>

<div class="cleaner"></div>

<div  class="portlet">
	<div class=" "><b>Anticoagulants</b></div>
		<div class="portlet-content" style="overflow-x:auto">
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewanticoagulants();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteanticoagulantsData()"/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
			    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" onclick="editAnticoagulants();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="anticoagulantsTable" class="display">
			<thead>
				<tr>
					<th width="4%" id="anticoagulantsColumn"></th>
					<th>Check all <input type="checkbox" id="checkall"/></th>
					<th colspan='1'>Name</th>
					<th colspan='1'>Lot #</th>
					<th colspan='1'>Exp.Date</th>
					<th colspan='1'>Volume</th>
					<th colspan='1'>Date of Addition</th>
					<th colspan='1'>Time of Addition</th>
					<th colspan='1'>Comment</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			</table>
		</div>
</div>

<div class="cleaner"></div>

<div  class="portlet">
	<div class=" "><b>Other Additives</b></div>
		<div class="portlet-content" style="overflow-x:auto">
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewadditives();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" " onclick="deleteadditivesData()"/>&nbsp;&nbsp;<label class="cursor"  onclick="deleteadditivesData();"><b>Delete</b></label>&nbsp;</div></td>
			    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" onclick="editAdditives();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" id="additiveTable" width="100%" class="display">
			<thead>
				<tr>
					<th width="4%" id="additivesColumn"></th>
					<th>Check all <input type="checkbox" id="checkall"/></th>
					<th>Name</th>
					<th>Lot #</th>
					<th>Exp.Date</th>
					<th>Volume</th>
					<th>Date of Addition</th>
					<th>Time of Addition</th>
					<th>Comment</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			</table>
		</div>
</div>  

<div class="cleaner"></div>

<div align="right" style="float:right;">
	<table>
	
	         <tr>
	           <td width="" align="" >
	           <input type="password" style="width:80px;" size="5" value="" id="postPrepEsign" placeholder="e-Sign"/>&nbsp;<input type="button" name="Save" onclick="verifyeSign('postPrepEsign','popup');" value="Save"/>&nbsp;<input type="button" name="Cancel" value="Cancel" onclick="closeAnticoagulants();"/>
	           </td>
	         </tr>
	</table>
</div>
</form>
</div>
