<%@ include file="common/includes.jsp" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/workFlow.js"></script>
<link type="text/css" href="css/workFlow.css" rel="stylesheet" />
<style>
.workFlowContainer{
background-color:	#FFFFFF;;
border-radius:5px;
}
.subLinkInsurance{
border:1px solid white;
font-weight:bold;
width:60%;
text-align:center;
line-height:1.3 em;
padding:2%;
background-color:#DADADA;
}
.subMenuWrapper{
background-color:	#B5B5B5;
border-radius:5px;
}
.subDivPortlet{
background-color:	#DADADA;
border-radius:5px;
}
#box-outer {
	width:230px;
}
#subDivLeftWrapper{
border-radius:5px;
}
.subDivLeftContent{
border:1px solid black;
font-weight:bold;
text-align:center;
line-height:1.3 em;
padding:2%;
background-color:#FFFFFF;
border-radius:5px;
}
#subDIvTitle{
height:30px;
font-weight:bold;
padding-top:6px;
padding-left:2px;
font-size:1.0em;
}
</style>

<script>

function consult(){
	var form="";
	var url = "loadConsultPage";
	var result = ajaxCall(url,form);
	$("#main").html(result);
}
function showConsult_VisitPage(){
	var form1="";
	var url = "loadConsultVisitPage";
	var result = ajaxCall(url,form1);
	$("#forfloat_right").html(result);
}
function showConsult_LabPage(){
	var form="";
	var url = "loadConsult_LabPage";
	var result = ajaxCall(url,form);
	$("#forfloat_right").html(result);
}
function showConsult_PhysicianPage(){
	var form="";
	var url = "loadConsult_PhysicianPage";
	var result = ajaxCall(url,form);
	$("#forfloat_right").html(result);
}
function showConsult_InsurancePage(){
	var form="";
	var url = "loadConsult_InsurancePage";
	var result = ajaxCall(url,form);
	$("#forfloat_right").html(result);
}
function showConsult_NextStepPage(){
	var form="";
	var url = "loadConsult_NextStepPage";
	var result = ajaxCall(url,form);
	$("#forfloat_right").html(result);
}
/* function show_eastslidewidget(divid){
    // to Hide the left side widget
	innerLayout.hide('west');
    innerLayout.show('south',true);
    innerLayout.show('north',true);
    innerLayout.show('east',false);
    var src = $("#"+divid).html();
    $("#innereast").html(src);
    $("#"+divid).html("slidedivs");
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
         
}  */
		
$(function() {
    $('.jclock').jclock();
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });
  });
$(document).ready(function(){
    //$("select").uniform(); 
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"});
      $(window).resize();
	show_eastslidewidget("slidedivs");
		 			var  oTable3=$("#consultEvalChecklistDetails").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null
	 										             ],
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#consultEvalChecklistColumn").html($('.ColVis:eq(0)'));
												}
									});
		 			var  oTable=$("#consultEvalEducationDetails").dataTable({
						"sPaginationType": "full_numbers",
						"sDom": 'C<"clear">Rlfrtip',
						"aoColumns": [ { "bSortable":false},
							               null,
							               null,
							               null,
							               null
							               ],
							"oColVis": {
								"aiExclude": [ 0 ],
								"buttonText": "&nbsp;",
								"bRestore": true,
								"sAlign": "left"
							},
							"fnInitComplete": function () {
						        $("#consultEvalEducationColumn").html($('.ColVis:eq(1)'));
							}
				});
					    
});
$(window).resize(function(){
    var height;
    var screenheight=screen.height;
    var innerheight=$("#innercenter").height();
    var innerheight=594;
    var constanthgt=$("#constantdiv").height();
    /* if(innerheight) */
    if(screenheight<= 768){
       height =innerheight -(constanthgt+240);
    }else if(screenheight>=1024){
           height =innerheight -(constanthgt+40);

    }else if(screenheight>=800 && screenheight < 900){
           height =innerheight -(constanthgt+240);
    }else if(screenheight==900){
           height =innerheight -(constanthgt+130);
    }else if(screenheight==960){
           height =innerheight -(constanthgt+80);
    }
       $('#subdiv').height(height);
    });
$("select").uniform(); 
</script>

<div class="cleaner"></div>

<!--right window start -->	
<div  id="slidedivs"  style="display:none">
<div id="uldiv">
<ul class="ulstyle">
<li class="listyle print_label"><img src = "images/icons/calendar.png"  style="width:30px;height:30px;"/></li>
<li class="listyle print_label" ><b>Calendar</b> </li>
<li class="listyle print_label"><img src = "images/icons/plan.png"  style="width:30px;height:30px;"/></li>
<li class="listyle notes_icon"><b>Plan</b> </li>
<li class="listyle print_label"><img src = "images/icons/Notification.png"  style="width:30px;height:30px;"/></li>
<li class="listyle print_label" ><b>Notification</b> </li>
</ul>
</div>
</div>
<form>
<div class="column">
<div  class="portlet">
	<div class="portlet-header">Referral</div>
		<div class="portlet-content">
			<br>
			<div class="subMenuWrapper" >
				<table cellpadding="0" cellspacing="0" border="0" id="" class="display">
						<tr>
							<td style="padding-right:20px"><div class="subLinkInsurance">Recipient Workflow</div></td>
							<td style="padding-right:20px"><div class="subLinkInsurance">Recipient Profile</div></td>
							<td style="padding-right:20px"><div class="subLinkInsurance">Insurance</div></td>
							<td style="padding-right:20px"><div class="subLinkInsurance">Donor Matching</div></td>
							<td style="padding-right:20px"><div class="subLinkInsurance">Available Products</div></td>
						</tr>
			</table>
			</div>
			<br>
			<div class="subDivPortlet">
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
							<tr>
								<td align="right" style="padding-top:1%">Recipient First Name : </td>
								<td  align="left" style="padding-top:1%">&nbsp;Renee</td>
								<td align="right" style="padding-top:1%">Diagnosis :</td>
								<td  align="left" style="padding-top:1%">&nbsp;AML</td>
								<td  align="right" style="padding-top:1%">Age :</td>
								<td  align="left" style="padding-top:1%">&nbsp;24</td>
								<td  align="right" style="padding-top:1%">Diagnosis :</td>
								<td  align="left" style="padding-top:1%">&nbsp;Peds</td>
						</tr>
						<tr>
								<td  align="right" style="padding-top:1%">Recipient Last Name : </td>
								<td  align="left" style="padding-top:1%">&nbsp;Branofsky</td>
								<td  align="right" style="padding-top:1%">Diagnosis Date:</td>
								<td  align="left" style="padding-top:1%">&nbsp;Jan 6, 2012</td>
								<td  align="right" style="padding-top:1%">SSN:</td>
								<td  align="left" style="padding-top:1%">&nbsp;465983875</td>
								<td  align="right">ABO/Rh:</td>
								<td  align="left">&nbsp;A+</td>
						</tr>
						<tr>
							<td  align="right" style="padding-top:1%">Status:</td>
							<td style="padding-top:1%">&nbsp;Pre-Transplant</td>
							<td  align="right" style="padding-top:1%">DOB :</td>
							<td style="padding-top:1%">&nbsp;Jan 19, 1988</td>
							<td  align="right" style="padding-top:1%">MRN :</td>
							<td style="padding-top:1%">&nbsp;586790</td>
						</tr>
							
					</table>
				</div>  
				<br><br>
		<div class="workFlowContainer">
			 <div class="workFlow">
				 <div class='workflowContent' >
				 		<div class='processstarted' onclick='checkBox(this)'></div>
				 		<div class='completed' onclick='check(this)'><p class='samplet-text'>Referal</p></div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' onclick='checkBox(this)'></div>
				 	<div class='onprogress' onclick='check(this),consult()'>
				 		<p class='samplet-text'>Consult/Eval</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' onclick='checkBox(this)'></div>
				 	<div class='initial' onclick='check(this)'>
				 		<p class='samplet-text'>Pre-Transplant Visit</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' onclick='checkBox(this)'></div>
				 	<div class='initial' onclick='check(this)'>
				 		<p class='samplet-text'>Stem Cell Collection</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' onclick='checkBox(this)'></div>
				 	<div class='initial' onclick='check(this)'>
				 		<p class='samplet-text'>Preparative Regimen</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' onclick='checkBox(this)'></div>
				 	<div class='initial' onclick='check(this)'>
				 		<p class='samplet-text'>	Selected Products</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' onclick='checkBox(this)'></div>
				 	<div class='initial' onclick='check(this)'>
				 		<p class='samplet-text'>Infusion</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
					 <div class='processinitial' onclick='checkBox(this)'></div>
					 <div class='initial' onclick='check(this)'>
					 	<p class='samplet-text'>Followup</p>
					 </div>
					 </div>
			</div>
		</div>
		</div>
	</div>
</div>
<div class="cleaner"></div>
	<div id="subdiv" style="height:15em;overflow:auto;">
	<div class="subMenuWrapper" id="sub">
		<div id="subDIvTitle">Consult/Eval - Visit</div>
	</div>
	<br>
<div id="floatSubDivMenu">
<div id="forfloat_wind">
	<div id="box-outer">
		
<!--  <a href="#" style="text-decoration:none" id="blind">&lt;&lt; </a>  -->
			<div id="box">
				<div id="leftnav" >
				
	<!-- recipient start-->
				
					<div  class="portlet">
					<div class="portlet-header"></div>
						<div class="portlet-content">
						<div id="subDivLeftWrapper" >
				<table cellpadding="0" cellspacing="0" border="0" id="" class="display">
						<tr>
							<td ><div class="subDivLeftContent" onclick="showConsult_VisitPage()">Visit</div></td></tr>
							<tr><td><div class="subDivLeftContent" onclick="showConsult_LabPage()">Labs/Tests</div></td></tr>
							<tr><td><div class="subDivLeftContent" onclick="showConsult_PhysicianPage()">Physician's Plan</div></td></tr>
							<tr><td><div class="subDivLeftContent" onclick="showConsult_InsurancePage()">Insurance Approvals</div></td></tr>
							<tr><td ><div class="subDivLeftContent" onclick="showConsult_NextStepPage()">Next Step</div></td>
						</tr>
			</table>
			</div>
			</div>
			</div>
			</div>
			</div>
			</div>
		</div>
</div>

<div id="forfloat_right">
<%-- <div class="column">       
    <div  class="portlet">
    <div class="portlet-header">Registration Details</div><br>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Recipient Weight(Kg)</td>
					<td>
						<div  class="portlet11">
							<div class="portlet-content">
							<table>
							<tr>
								<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
								<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
						    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
							</tr>
						</table>
						<table cellpadding="0" cellspacing="0" border="1" id="referralDetails" class="display">
						<thead>
							<tr>
								<th colspan='1'>Select</th>
								<th colspan='1'>Weight</th>
								<th colspan='1'>unit</th>
								<th colspan='1'>Date</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><input type='checkbox'  id="recipientInsuranceVerified" onclick=""></td>
								<td></td>
								<td></td>
								<td>	</td>
								</tr>
						</tbody>
						</table>
			</div>
		</div>
					</td>
                </tr>
                </table>
                 <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Recipient Height(cm)</td>
					<td class="tablewapper1"><input type="text" id="recipientHeight" name=""/></td>
                    <td class="tablewapper">ABO/Rh</td>
					<td class="tablewapper1"><select id="ABO/Rh"><option>Select</option></select></td>
                </tr>
                 <tr>
                    <td class="tablewapper">Recipient Height(cm)</td>
					<td colspan=3><textarea cols="80" rows="4" id="rxBenefitPlan"></textarea></td>
                 </tr>
                </table>
        </div>
    </div>
</div>	 --%>
<!-- <div class="column">
	<div  class="portlet">
				<div class="portlet-header ">Consult/Eval Checklist</div>
					<div class="portlet-content">
						<table>
							<tr>
								<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
								<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
						    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
							</tr>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" id="consultEvalChecklistDetails" class="display">
						<thead>
							<tr>
								<th width="4%" id="consultEvalChecklistColumn"></th>
								<th colspan='1'>Select</th>
								<th colspan='1'>Items</th>
								<th colspan='1'>Date</th>
								<th colspan='1'>Document Name</th>
								<th colspan='1'>Attachment</th>
								<th colspan='1'>Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="consultEvalChecklistVerified" onclick=""></td>
								<td>Eval / Assesment By MD</td>
								<td></td>
								<td>	</td>
								<td><input type="button" id="" value="Browse" /></td>
								<td></td>
						</tr>
						</tbody>
						</table>
			</div>
		</div>
	</div>	 -->
	
	<%-- <div class="column">
	<div  class="portlet">
				<div class="portlet-header ">Consult/Eval Education</div>
					<div class="portlet-content">
						<table>
							<tr>
								<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
								<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
						    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
							</tr>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" id="consultEvalEducationDetails" class="display">
						<thead>
							<tr>
								<th width="4%" id="consultEvalEducationColumn"></th>
								<th colspan='1'>Select</th>
								<th colspan='1'>Items</th>
								<th colspan='1'>Date</th>
								<th colspan='1'>Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="consultEvalDocumentVerified" onclick=""></td>
								<td>Patient info Packet</td>
								<td></td>
								<td>	<select id="ABO/Rh"><option>Select</option></select></td>
							</tr>
						</tbody>
						</table>
			</div>
		</div>
	</div>	 --%>
</div>
</div>
</form>

<!-- <script type="text/javascript">



var now = new dateEntry();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivaldateEntry').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new dateEntry()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};
$("#timeZone").html(timezones[-offset / 60]);

</script>
 -->













