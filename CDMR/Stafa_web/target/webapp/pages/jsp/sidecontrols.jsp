<%@ include file="common/includes.jsp" %>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />

<script>
function transfer(){
	showPopUp("open","transfer","Treatment-MDA129833483","950","520");
}
function medication(){
	showPopUp("open","addnewMedicationScan","Medications","950","520");
}
function addrows_therapy(id){
	var runningTabClass= $('#'+id+' tbody tr td:nth-child(1)').attr("class");
	alert(runningTabClass);
	if(runningTabClass=="dataTables_empty"){
			$('#'+id+' tbody').replaceWith("<tr><td><select class='select1'><option>Select</option></select></td></td><td><input type='type' onclick='getRunningTotal(),calculateVolume()'></td><td><input type='type' onclick='getRunningTotal(),calculateVolume()'></td><td>1</td><td><input type='type' onclick='getRunningTotal(),calculateVolume()'></td><td><select class='select1'><option>Select</option></select></td></td></tr>");
	}
	else
		{
		$('#'+id+' tbody').prepend("<tr><td><select class='select1'><option>Select</option></select></td></td><td><input type='type' onclick='getRunningTotal(),calculateVolume()'></td><td><input type='type' onclick='getRunningTotal(),calculateVolume()'></td><td>1</td><td><input type='type' onclick='getRunningTotal(),calculateVolume()'></td><td><select class='select1'><option>Select</option></select></td></td></tr>");
		}
		$("select").uniform();
}
$(document).ready(function(){
	oTable = $('#pre-collectionTherapy').dataTable({
		"bRetrieve": true,
		"bPaginate":true
		//"sPaginationType": "full_numbers"
	});
	oTable = $('#medicationTable').dataTable({
		"bRetrieve": true,
		"bPaginate":true
		//"sPaginationType": "full_numbers"
	});
	
	oTable=$("#medicateElectro").dataTable({
		"sPaginationType": "full_numbers",
		"sDom": 'C<"clear">Rlfrtip',
		"aoColumns": [ { "bSortable":false},
		               { "bSortable":false},
			               null,
			               null,
			               null,
			               null,
			               null,
			               null,
			               null
			             ],
			"oColVis": {
				"aiExclude": [ 0 ],
				"buttonText": "&nbsp;",
				"bRestore": true,
				"sAlign": "left"
			},
			"fnInitComplete": function () {
		        $("#medicateElectroColumn").html($('.ColVis:eq(3)'));
			}
	});
	oTable=$("#otherMedicate").dataTable({
		"sPaginationType": "full_numbers",
		"sDom": 'C<"clear">Rlfrtip',
		"aoColumns": [ { "bSortable":false},
		               { "bSortable":false},
			               null,
			               null,
			               null,
			               null,
			               null,
			               null,
			               null
			             ],
			"oColVis": {
				"aiExclude": [ 0 ],
				"buttonText": "&nbsp;",
				"bRestore": true,
				"sAlign": "left"
			},
			"fnInitComplete": function () {
		        $("#otherMedicateColumn").html($('.ColVis:eq(4)'));
			}
	});
	$("select").uniform();
});
$("#medicateBarcode tr").bind('click',function(){
	showPopUp("open","addnewMedication","Medications-HCA12983483","1100","520");
});
</Script>
<div class="divborder">
	<ul class="ulstyle">
		<li class="listyle print_label" ><b>Label</b> </li>
		<li class="listyle print_label"><img src = "images/icons/print_small.png"  style="width:30px;height:30px;"/></li>
		<li class="listyle print_label" ><b>Calendar</b> </li>
		<li class="listyle print_label"><img src = "images/icons/calendar.png"  style="width:30px;height:30px;"/></li>
		<li class="listyle notes_icon"><b>Notes</b> </li>
		<li class="listyle notes_icon"><img src = "images/icons/notes.png"  style="width:30px;height:30px;"/></li>
		<li class="listyle medication_icon"><b>Medication</b></li>
		<li class="listyle medication_icon"><img src = "images/icons/capsule1.png" onclick="medication();" style="width:30px;height:30px;"/></li>
		<li class="listyle transfer_icon" id="transfer_icon"><b>Transfer</b></li>
		<li class="listyle transfer_icon"><img src = "images/icons/transfer.png" onclick="transfer()"  style="width:30px;height:30px;"/></li>
		<li class="listyle terminate_icon"><b>Terminate</b></li>
		<li class="listyle terminate_icon"><img src = "images/icons/terminated.png"  style="width:30px;height:30px;"/></li>
		<li class="listyle complete_icon"><b>Complete</b></li>
		<li class="listyle complete_icon"><img src = "images/icons/task_completed_icon.PNG"  style="width:30px;height:30px;"/></li>
	</ul>
</div>

<!-- <div id="addnewMedication" title="addNew" style="display: none">

</div> -->

<div id="addnewMedicationScan" title="addNew" style="display: none">
	<div><b>Barcode Scanning</b></div>
  	<form>
	 	<table cellpadding="0" cellspacing="0" class="" align="center" id="medicateBarcode" border="1" width="100%">
	        <tr>
		         <td class="tablewapper">Donor Id</td>
		         <td class="tablewapper1"> <input type="text" id="" placeholder="Scan/Enter Patient Id" /></td>
		         <td class="tablewapper"> <input type="checkbox" id="" />Verified</td>
	        </tr>
	        <tr>
	             <td class="tablewapper">Nurse Id</td>
	             <td class="tablewapper1"><input type="text" id="" placeholder="Scan/Enter Patient Id"/></td>
	             <td class="tablewapper"><input type="checkbox" id="" />Verified</td>
	        </tr>
		</table> 
  	</form>  
</div>

<div id="addnewMedication" title="addNew" style="display: none">
	<div><b>Electrolyte Management</b></div>
  	<form>
  		<table>
			<tr>
				<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addRows();"/>&nbsp;&nbsp;<label  class="cursor" onclick="addRows();"><b>Add</b></label>&nbsp;</div></td>
				<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteRows();"/>&nbsp;&nbsp;<label class="cursor"  onclick="deleteRows();"><b>Delete</b></label>&nbsp;</div></td>
		    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" onclick="editRows();"/>&nbsp;&nbsp;<label  class="cursor" onclick="editRows();"><b>Edit</b></label>&nbsp;</div></td>
		    	<td style="width:20%"><div><img src = "images/icons/save3.png" style="width:16;height:16;" class="cursor" id ="" onclick="save();" >&nbsp;&nbsp;<label id="schild" class="cursor" onclick="save();"><b>Save</b></label>&nbsp;</div></td>
			</tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" id="medicateElectro" class="display">
	 		<thead>
	 			<tr>
	 				<th width="4%" id="medicateElectroColumn"></th>
	 				<th>Check All<input type='checkbox'/></th>
	 				<th>Name</th>
	 				<th>Lot #</th>
	 				<th>Exp. Date</th>
	 				<th>Volume (mL)</th>
	 				<th>Date of Addition</th>
	 				<th>Time of Addition</th>
	 				<th>Comment</th>
	 			</tr>
	 		</thead>
	 		<tbody>
		        <tr>
		        	<td width="4%" id="medicateElectroColumn"></td>
		        	<td><input type='checkbox'/></td>
		        	<td><select><option>Select...</option></select></td>
		        	<td><select><option>Select...</option></select></td>
		        	<td><input type='text' class='dateEntry'/></td>
		        	<td><input type='text'/></td>
		        	<td><input type='text' class='dateEntry'/></td>
		        	<td><input type='text'/></td>
		        	<td><input type='text'/></td>
		        </tr>
	        </tbody>
		</table>
		
<div>&nbsp;</div>
<div class="cleaner"></div>
<div>&nbsp;</div>

		<div><b>Other Medications</b></div>
		<table>
			<tr>
				<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addRows();"/>&nbsp;&nbsp;<label  class="cursor" onclick="addRows();"><b>Add</b></label>&nbsp;</div></td>
				<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteRows();"/>&nbsp;&nbsp;<label class="cursor"  onclick="deleteRows();"><b>Delete</b></label>&nbsp;</div></td>
		    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" onclick="editRows();"/>&nbsp;&nbsp;<label  class="cursor" onclick="editRows();"><b>Edit</b></label>&nbsp;</div></td>
		    	<td style="width:20%"><div><img src = "images/icons/save3.png" style="width:16;height:16;" class="cursor" id ="" onclick="save();" >&nbsp;&nbsp;<label id="schild" class="cursor" onclick="save();"><b>Save</b></label>&nbsp;</div></td>
			</tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" id="otherMedicate" class="display">
	 		<thead>
	 			<tr>
	 				<th width="4%" id="otherMedicateColumn"></th>
	 				<th>Check All<input type='checkbox'/></th>
	 				<th>Name</th>
	 				<th>Lot #</th>
	 				<th>Exp. Date</th>
	 				<th>Volume (mL)</th>
	 				<th>Date of Addition</th>
	 				<th>Time of Addition</th>
	 				<th>Comment</th>
	 			</tr>
	 		</thead>
	 		<tbody>
		        <tr>
		        	<td width="4%" id="otherMedicateColumn"></td>
		        	<td><input type='checkbox'/></td>
		        	<td><select><option>Select...</option></select></td>
		        	<td><select><option>Select...</option></select></td>
		        	<td><input type='text' class='dateEntry'/></td>
		        	<td><input type='text'/></td>
		        	<td><input type='text' class='dateEntry'/></td>
		        	<td><input type='text'/></td>
		        	<td><input type='text'/></td>
		        </tr>
	        </tbody>
		</table> 
  	</form>
  	<div align="right" style="float:right;">
		<form>
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td><input type="button" value="Save" onclick=""/></td>
					<td><input type="button" value="Cancel" onclick=""/></td>
				</tr>
			</table>
		</form>
	</div>  
</div>
