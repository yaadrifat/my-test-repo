<%@ taglib prefix="s" uri="/struts-tags" %>
<script type="text/javascript" src="jsp/esecurity/js/Group_List.js"></script>
<style>
/*  for styling the anchor tags  */
#groupDiv a {
	color:#571D8D;
}

#groupDiv a{
margin-right: 40px;
}

.dataTables_paginate{
	width: 125px;
}
</style>
<script>
$(document).ready(function(){
	$.ajax({
		url:"getbannertext.action",
		type:"post",
		data:null,
		cache:false,
			success:function(response){
				
	      	  if(response.import_status=='1'&&response.export_status=='0'){
	      		$("#bannercontent").show();
	 				$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
				}
				else if(response.export_status=='1'&& response.import_status=='0'){
					$("#bannercontent").show();
					$("#bannercontent").html("Export Interface is in progress. System slowness may occur")
				}
				else if(response.import_status=='1' && response.export_status=='1'){
					$("#bannercontent").show();
					$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
					$("#bannercontent").append("<div style='background-color: yellow;'>Export Interface is in progress. System slowness may occur</div>");
				}
				else{
					$("#bannercontent").hide();
				}
	       }

	});
	hide_slidewidgets();
	hide_slidecontrol();
	clearTracker();
	show_innernorth();
	applyDatatables();
	
	$("select").uniform();
});
function applyDatatables(){	
	$.ajax({
		type: "POST",
	    url: "loadAllRoles.action",
	    async:false,
	    success:function (result){
	    	var resultStr = result.roleList;
	    	
	    	for(var i in resultStr){
	        	var rowObj = resultStr[i];
	        	$('#groupTable').dataTable().fnAddData( [
					"<input type='checkbox' />",rowObj[0],rowObj[1],"<a href='#' onclick = 'getRole(this,\""+rowObj[2]+"\");'> Assign </a>","<a href='#' onclick = 'getUsers(this,\""+rowObj[2]+"\");'>Manage</a>"]);                                        
	    	}
	    }
	});
}
</script>
<form>
	<div class="column" id="groupDiv">       
		<div  class="portlet">
			<div class="portlet-header"> Group List </div>
			<div class="portlet-content">
				<table>
					<tr>
						<td style="width:20%"><div onclick="addRows(0,'','');"><img src = "images/icons/addnew.jpg" class="cursor" />&nbsp;&nbsp;<label class="cursor"><b>Add</b></label>&nbsp;</div></td>
						<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick=""/>&nbsp;&nbsp;<label class="cursor"  onclick="deleteRows();"><b>Delete</b></label>&nbsp;</div></td>
					</tr>
				</table>
				<table cellpadding="0" cellspacing="0" border="0" id="groupTable" class="display" >
	            	<thead>
						<tr>
							<th colspan="1"><input type="checkbox" /> <s:text name="stafa.esecurity.label.checkAll"/> </th>
	                        <th colspan="1"> <s:text name="stafa.esecurity.label.group.name"/> </th>
	                        <th colspan="1"> <s:text name="stafa.esecurity.label.group.description"/> </th>
							<th colspan="1"> <s:text name="stafa.esecurity.label.group.groupRights"/> </th>
	                        <th colspan="1"> <s:text name="stafa.esecurity.label.group.gruopUsers"/> </th>
						</tr>
					</thead>
					 <tbody>
	                <!--     <tr>
	                    	<td><input type="checkbox" /></td>
	                        <td id="desc">Tech</td>
	                        <td>Technician</td>
	                        <td> <a href="#" onclick = "getRole(this);"> Assign </a> </td>
	                        <td> <a href="#" onclick = "getUsers(this);"> Manage </a> </td>
						</tr>
						<tr>
	                    	<td><input type="checkbox" /></td>
	                        <td id="desc">nurse</td>
	                        <td>Nurse</td>
	                        <td> <a href="#" onclick = "getRole(this);"> Assign </a> </td>
	                        <td> <a href="#" onclick = "getUsers(this);"> Manage </a> </td>
						</tr>-->
	            	</tbody>
	            </table>
	            <div  class="portlet" style="border: none;">
			
				<form id="loginForm"  onsubmit="return avoidFormSubmitting()">
					<table cellpadding="0" cellspacing="0" class="" align="" border="0" width="100%">
					<tr>
							<td><span id="pagename"  Class="titleText" style="float:left;">  </span> </td>
							<td> <span id="pagecontrols" style="float:right;"><input type="hidden" name="flowNextFlag" id="flowNextFlag" value=""/><input type="password" style="width:80px;" size="5" value="" id="eSign" name="eSign" placeholder="e-Sign"  autocomplete="off" />&nbsp;&nbsp;
								<input type="button" value="Save" onclick="verifyeSign('eSign','save')"/>  </span></td>
					</tr>
					<tr>
					       <td colspan="2"> <table id="tracker" cellpadding="0" cellspacing="0"><tr class="workFlowContainer"></tr></table></td>
					</tr>
					</table>
				</form>
			</div>
			</div>
			
		</div>
		
	</div>
</form>

<div id="loadRights">
	
	
</div>

<script>

$(function() {
    $('.jclock').jclock();
    $( ".column" ).sortable().sortable({
        connectWith: ".column"
    });
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");

    /**For Refresh**/
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
    .append("<span style=\"float:right;\" class='ui-reset'></span>");
    $( ".portlet-header .ui-reset " ).click(function() {
        var formName = document.getElementById("login");
        //clearForm(formName);
        clear();
    });

//     $( ".portlet-header .ui-notes " ).click(function() {
//         showNotes('open');
//     });


    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

    $('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});   
});


var now = new Date();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivalDate').html(lastLog);


</script>