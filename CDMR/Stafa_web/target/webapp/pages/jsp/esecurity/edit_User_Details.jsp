<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="/pages/jsp/common/includes.jsp" %>
<script>
var errorPassword=0, errorEsign = 0;
$("select").uniform();
$(document).ready(function(){
	hide_slidewidgets();
	hide_slidecontrol();
	clearTracker();
	show_innernorth();
});
function showPassword(){
	if($('#changePassword').is(":checked")){
		$('#oldPassword').removeAttr("disabled");
		$('#newPassword').removeAttr("disabled");
		$('#confirmPassword').removeAttr("disabled");
		$('#passwordDays').removeAttr("disabled");
		//$('#newPassword').attr('name','password');
		//$('#password').removeAttr('name');
	}
	else{
		$('#oldPassword').attr('disabled','disabled');
		$('#newPassword').attr('disabled','disabled');
		$('#confirmPassword').attr('disabled','disabled');
		$('#passwordDays').attr('disabled','disabled');
		//$('#newPassword').removeAttr('name');
		//$('#password').attr('name','password');
	}
}
function showeSign(){
	if($('#changeeSign').is(":checked")){
		$('#oldEsign').removeAttr("disabled");
		$('#newEsign').removeAttr("disabled");
		$('#confirmEsign').removeAttr("disabled");
		$('#esignDays').removeAttr("disabled");
		//$('#newEsign').attr('name','esign');
	}
	else{
		$('#oldEsign').attr('disabled','disabled');
		$('#newEsign').attr('disabled','disabled');
		$('#confirmEsign').attr('disabled','disabled');
		$('#esignDays').attr('disabled','disabled');
		//$('#newEsign').removeAttr('name');
	}
}
function checkPassword(){
	if(($('#confirmPassword').val()) != ($('#newPassword').val())){
		errorPassword = 1;
		$('#errorPassword').show();
	}
	else{
		errorPassword = 0;
		$('#errorPassword').hide();
	}
}
function checkeSign(){
	if(($('#confirmEsign').val()) != ($('#newEsign').val())){
		errorEsign = 1;
		$('#erroreSign').show();
	}
	else{
		errorEsign = 0;
		$('#erroreSign').hide();
	}
}
function updateUserDetails(){
	$('.progress-indicator').css( 'display', 'block' );
	var passwordFlag = 0, esignFlag = 0;
	if(errorPassword == 1){
		alert(passNotMatch);
	}
	else if(errorEsign == 1){
		alert(esignNotMatch);
	}
	else{
		if($('#changePassword').is(":checked")){
			var password = $('#newPassword').val();
			if((password == $('#loginName').val()) || ($.trim(password).length < 8 ) || (password.match(/[0-9]+/) == null) || (password.match(/[A-Za-z]+/) == null)){
				passwordFlag = 1;
				alert(passordRule);
			}
		}
		if($('#changeeSign').is(":checked")){
			var eSign = $('#newEsign').val();
			if($.trim(eSign).length < 4 ){
				esignFlag = 1;
				alert(esignRule);
			}
		}
	
		if(passwordFlag == 0 && esignFlag == 0){
			$.ajax({
				type: "POST",
			    url: "updateUserDetails.action",
			    async:false,
			    data:$("#editUserForm").serialize(),
			    success:function (result){
			    	if(result.errorMessage!=null){
			    		alert(result.errorMessage);
			    	}
			    	else{
			    		alert(saveSuccess);
			    		callAccount();
			    		$('#changePassword').removeAttr('checked');
			    		$('#changeeSign').removeAttr('checked');
			    		$('#oldPassword').val("");
			    		$('#newPassword').val("");
			    		$('#confirmPassword').val("");
			    		$('#passwordDays').val("");
			    		$('#oldEsign').val("");
			    		$('#newEsign').val("");
			    		$('#confirmEsign').val("");
			    		$('#esignDays').val("");
			    	}
				},
			    error: function (request, status, error) {
			    	alert("Error " + error);
				}
			});
		}
	}
	$('.progress-indicator').css( 'display', 'none' );
	return true;
} 

function savePages(){
	try{
		if(updateUserDetails()){
			return true;
		}
	}catch(e){
		alert("exception " + e);
	}
} 

</script>


<form id="editUserForm">
	<div class="column" id="editUserDiv">       
		<div class="portlet">
			<div class="portlet-header"> My Profile </div>
			<div class="portlet-content">
			<input type="hidden" id="loginName" name="loginName"/>
				<table>
					<tr><td><s:text name="stafa.esecurity.label.userID"/> </td><td> <input id="userId" name="userId" type="text" /> </td><td>Address (line 1) </td><td> <input id="addr1" name="addr1" type="text" /> </td></tr>
					<tr><td><s:text name="stafa.esecurity.label.firstName"/> </td><td> <input id="firstName" name="firstName" type="text" /></td><td>Address (line 2) </td><td> <input id="addr2" name="addr2" type="text" /> </td></tr>
					<tr><td><s:text name="stafa.esecurity.label.lastName"/> </td><td>  <input id="lastName" name="lastName" type="text" /></td><td>City  </td><td> <input id="city" name="city" type="text" /></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.phone"/> </td><td>  <input id="phone" name="phone" type="text" /><td>State </td><td>  <input id="state" name="state" type="text" /></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.email"/> </td><td>  <input id="email" name="email" type="text" size="50"/></td><td>Zip / Postal Code </td><td>  <input id="postalCode" name="postalCode" type="text" /></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.jobtype"/></td><td><span id="jobtypelst"></span></td><td>Country  </td><td> <span id="countrylst"></span></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.clientUserId"/></td><td> <input id="clientUserId" name="clientUserId" type="text" /></td><td><input id="accExpDate" name="accExpDate" class="dateEntry" type="hidden" size="50"/><s:text name="stafa.esecurity.label.timeZone"/></td><td><span id="timezonelst"></span></td></tr>
				</table>
			</div>
		</div>
		<div class="portlet">
			<div class="portlet-header"> Security </div>
			<div class="portlet-content">
				<table>
					<tr> <td colspan="2"> <input type="checkbox" id="changePassword" name="changePassword" onchange="showPassword();" value="1"/> <s:text name="stafa.esecurity.label.passwordCheck"/> </td><td colspan="2"> <input type="checkbox" id="changeeSign" name="changeeSign" onchange="showeSign();" value="1"/><s:text name="stafa.esecurity.label.esignCheck"/> </td></tr>
					<tr><td colspan="2" style="color:red;"><s:text name="stafa.esecurity.label.passwordRule"/> </td><td colspan="2" style="color:red;"><s:text name="stafa.esecurity.label.esignRule"/>  </td> </tr>
					<tr><td><s:text name="stafa.esecurity.label.curPassword"/> </td><td><input id="oldPassword" name="oldPassword" type="password" disabled="disabled"/></td><td><s:text name="stafa.esecurity.label.curEsign"/> </td><td> <input id="oldEsign" name="oldEsign" type="password" disabled="disabled"/> </td></tr>
					<tr><td><s:text name="stafa.esecurity.label.newPassword"/> </td><td>  <input id="newPassword" name="newPassword" type="password" disabled="disabled"/> </td><td> <s:text name="stafa.esecurity.label.newEsign"/> </td><td> <input id="newEsign" name="newEsign" type="password" disabled="disabled"/> </td></tr>
					<tr><td><s:text name="stafa.esecurity.label.confirmPassword"/> </td><td>  <input id="confirmPassword" type="password" onblur="checkPassword();" disabled="disabled"/> <div id="errorPassword" style="display:none;color:red;"><s:text name="stafa.esecurity.label.passNotMatch"/></div></td><td> <s:text name="stafa.esecurity.label.confirmEsign"/> </td><td><input id="confirmEsign" type="password" onblur="checkeSign();" disabled="disabled"/><div id="erroreSign" style="display:none;color:red;"><s:text name="stafa.esecurity.label.esignNotMatch"/></div></td></tr>
					<tr><td colspan="2"><s:text name="stafa.esecurity.label.passExpiresAfter"/> <input id="passwordDays" name = "passwordDays" type="text" disabled="disabled" /> <s:text name="stafa.esecurity.label.days"/> </td> <td colspan="2"><s:text name="stafa.esecurity.label.esignExpiresAfter"/>  <input id="esignDays" type="text" disabled="disabled" /> <s:text name="stafa.esecurity.label.days"/> </td></tr>
				</table>
			</div>
			<div style="float:right;">
				<!-- <input type="password" id="eSign" placeholder="e-Sign" size="5">
				<input type="button" value="Save" onclick="verifyeSign('eSign','save')" /> -->
<!-- 				<input type="button" value="Save" onclick="updateUserDetails()" /> -->
	        </div>
		</div>
	</div>
</form>
<script>

$(function() {
    $( ".column" ).sortable().sortable({
        connectWith: ".column"
    });
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
 
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });
    
    $('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});   
});
</script>