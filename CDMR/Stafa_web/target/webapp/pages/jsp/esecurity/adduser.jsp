<%@ taglib prefix="s" uri="/struts-tags" %>
<script>
$(document).ready(function(){
$( "#accExpDate" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'M dd, yy',
		autoSize: true,
		onSelect: function(dateText, inst) { 
				$(this).change();		
		}
	});
	$("select").uniform();
});
function savePages(){
	saveNewUser();
}
</script>
	<div  class="portlet">
			<div class="portlet-header"> Member Information </div>
			<div class="portlet-content">
				<table>
					<tr><td><s:text name="stafa.esecurity.label.userID"/></td><td> <input id="userId" name="userId" type="text" /> </td><td>Address (line 1) </td><td> <input id="addr1" name="addr1" type="text" /> </td></tr>
					<tr><td><s:text name="stafa.esecurity.label.firstName"/></td><td> <input id="firstName" name="firstName" type="text" /></td><td>Address (line 2) </td><td> <input id="addr2" name="addr2" type="text" /> </td></tr>
					<tr><td><s:text name="stafa.esecurity.label.lastName"/></td><td>  <input id="lastName" name="lastName" type="text" /></td><td>City  </td><td> <input id="city" name="city" type="text" /></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.phone"/></td><td>  <input id="phone" name="phone" type="text" /><td>State </td><td>  <input id="state" name="state" type="text" /></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.email"/></td><td>  <input id="email" name="email" type="text" size="50"/></td><td>Zip / Postal Code </td><td>  <input id="postalCode" name="postalCode" type="text" /></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.jobtype"/></td><td> <s:select id="jobType" name="jobType" list="jobTypeList" listKey="pkCodeList" listValue="codeListDesc"  headerKey="" headerValue="Select" /></td><td>Country  </td>
					<td> <s:select id="country" name="country" list="countryList" listKey="pkCodeList" listValue="codeListDesc"  headerKey="" headerValue="Select" ></s:select></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.accExpDate"/> </td><td>  <input id="accExpDate" name="accExpDate" class="dateEntry" type="text" size="50"/></td><td>Time Zone </td><td><s:select id="timeZone" name="timeZone" list="timeZoneList" listKey="pkCodeList" listValue="codeListDesc"  headerKey="" headerValue="Select" ></s:select></td></tr>
					<tr><td><s:text name="stafa.esecurity.label.clientUserId"/></td><td> <input id="clientUserId" name="clientUserId" type="text" /></td><td><input type="hidden" name="acctIsActive" id="acctIsActive"/></td></tr>
				</table>
			</div>
		</div>
		<div  class="portlet">
			<div class="portlet-header"> Account Information </div>
			<div class="portlet-content">
				<table>
					<!-- <tr><td>User's Primary Organization </td><td><select id="userPrimaryOrg" name="userPrimaryOrg"><option>Select...</option></select></td><td>Security Question</td><td> <input id="securityQue" name="securityQue" type="text" /> </td></tr>
					<tr><td>Default Group  </td><td> <select id="defaultGroup" name="defaultGroup"><option>Select...</option></select></td><td>Answer</td><td> <input id="answer" name="answer" type="text" /> </td></tr>-->
					<tr><td>Login </td><td>  <input id="login" name="login" type="text" /></td><td>User Status</td><td><input id="status1" name="status" type="radio" value="1" />Activated<input id="status2" name="status" type="radio" value="0" />Deactivated</td></tr>
					<tr><td>Password </td><td>  <input id="password" name="password" type="password" /> <br/> <span id="forAdd" style="color:red;"> (Minimum 8 characters, with at least one number and one character, case-sensitive, <br/> cannot be same as login name)</span><span id="forEdit"> <a href="#" style="color:#571D8D;" > Reset Password</a></span></td></tr>
					<tr><td>e-Sign </td><td>  <input id="esign" name="esign" type="password"/> <span id="forAdd" style="color:red;"> (Minimum 4 digits) </span><span id="forEdit"> <a href="#" style="color:#571D8D;"> Reset e-Sign</a></span></td></tr>
				</table>
			</div>
			<input type="checkbox" /> Hide this user in user selection Lookups
			<div style="float:right;">
				<input type="button" value="Cancel" onclick="cancelUserSave();" />
				<input type="password" id="eSignNew" placeholder="e-Sign" size="5">
				<input type="button" value="Save" onclick="verifyeSign('eSignNew','save');" />
	        </div>
		</div>
		<script>

$(function() {
    $('.jclock').jclock();
    $( ".column" ).sortable().sortable({
        connectWith: ".column"
    });
    $(".portlet").each(function(i,el){
    	 if(!$(el).hasClass("ui-widget")){
    	    	$(el).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    	        .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
    	     
    	        $(el).find(".portlet-header .ui-icon" ).click(function() {
    	            $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
    	            $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    	        });
    	    	//$('.portlet-header .ui-icon:eq(1)').toggleClass( "ui-icon-plusthick" ).toggleClass( "ui-icon-minusthick" );
    	    	//$('.portlet-header .ui-icon:eq(1)').parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    	    }
    });
    
    $('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});   
});


</script>