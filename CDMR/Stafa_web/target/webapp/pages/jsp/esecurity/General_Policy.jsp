<%@ taglib prefix="s" uri="/struts-tags" %>
<%-- <script type="text/javascript" src="jsp/esecurity/js/User_Information.js"></script> --%>
<script>
	function savePages(){
		try{
			if(savePolicy()){
				return true;
			}
		}catch(e){
			alert("exception " + e);
		}
	}
</script>

<form id="generalPolicyForm">
	
			<table id="policyTable">
				<!-- <tr><td> Default User Time Zone </td><td><select style="width:80%;"><option>Select...</option></select></td><td><input type="checkbox"/> Can be Overwritten </td></tr>
				<tr><td> Number of Concurrent Logins </td><td>  <input id="concurrentLogins" name="concurrentLogins" type="text"/></td><td><input type="checkbox"/> Can be Overwritten </td></tr>
				<tr><td> Maximum Sessions </td><td>  <input id="maxSessions" name="maxSessions" type="text" /></td><td><input type="checkbox"/> Can be Overwritten</td></tr>
				<tr><td> Number of Failed Login Attempts (before account is blocked) </td><td>  <input id="failedAttempts" name="failedAttempts" type="text" /></td><td> <input type="checkbox"/> Can be Overwritten </td></tr>
				<tr><td> Default Automatic Logout Time (min)</td><td>  <input id="logoutTime" name="logoutTime" type="text"/> (less than 300 min) </td><td><input type="checkbox"/> Can be Overwritten</td></tr>
				<tr><td> Default # of Days of Password Expiration </td><td>  <input id="pwdExp" name="pwdExp" type="text"/> </td><td><input type="checkbox"/> Can be Overwritten</td></tr>
				<tr><td> Default # of Days of e-Sign Expiration </td><td>  <input id="eSignExp" name="eSignExp" type="text"/> </td><td><input type="checkbox"/> Can be Overwritten</td></tr>
				<tr><td> Enforce Password History </td><td>  <input id="pwdHistory" name="pwdHistory" type="text"/> </td><td><input type="checkbox"/> Can be Overwritten</td></tr>
				<tr><td> Minimum Password Length </td><td>  <input id="minLength" name="minLength" type="text"/> </td><td><input type="checkbox"/> Can be Overwritten</td></tr>
				<tr><td> Maximum Password Length </td><td>  <input id="maxLength" name="maxLength" type="text"/> </td><td><input type="checkbox"/> Can be Overwritten</td></tr>
				<tr><td> Password Policy: Minimum number of upper case letter </td><td>  <input id="minUpperCase" name="minUpperCase" type="text"/> </td><td><input type="checkbox"/> Can be Overwritten</td></tr>
				<tr><td> Password Policy: Minimum number of lower case letter </td><td>  <input id="minLowerCase" name="minLowerCase" type="text"/> </td><td><input type="checkbox"/> Can be Overwritten</td></tr>
				<tr><td> Password Policy: Minimum number of special characters </td><td>  <input id="minSplChar" name="minSplChar" type="text"/> </td><td><input type="checkbox"/> Can be Overwritten</td></tr>
				<tr><td> Password Policy: Minimum number of numbers </td><td>  <input id="minNumbers" name="minNumbers" type="text"/> </td><td><input type="checkbox"/> Can be Overwritten</td></tr>
				<tr><td> Number of Password Policy to be applied </td><td>  <select style="width:80%;"><option>Select...</option></select> </td><td><input type="checkbox"/> Can be Overwritten</td></tr>
				<tr><td> Role Combination Logic </td><td>  <select style="width:80%;"><option>Select...</option></select> </td><td><input type="checkbox"/> Can be Overwritten</td></tr>
				<tr><td> Default Password </td> <td> <input type="password" id="password" name="password" /></td></tr>
				<tr><td> Default e-Sign </td> <td> <input type="password" id="eSign" name="eSign" /></td></tr>  -->
			</table>
		
</form>
