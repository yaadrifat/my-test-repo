<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script type="text/javascript" src="js/placeholder.js"></script>
<script type="text/javascript" src="js/dataSearch.js"></script>
<script type="text/javascript" src="js/jquery/jquery.loadjson.js"></script>
<style type="text/css">
       /*  body { font-family:Lucida Sans, Lucida Sans Unicode, Arial, Sans-Serif; font-size:13px; margin:0px auto;} */
        .tabs { margin:0; padding:0; list-style:none; overflow:hidden; }
        .tabs li { float:left; display:block; padding:5px; background-color:#bbb; margin-right:5px;}
        .tabs li a { color:#fff; text-decoration:none; }
        .tabs li.current { background-color:#e1e1e1;}
        .tabs li.current a { color:#000; text-decoration:none; }
        .tabs li a.remove { color:#f00; margin-left:10px;}
        .content { background-color:#FFFFFF;}
        .content p { margin: 0; padding:20px 20px 100px 20px;}
        
        .main { width:900px; margin:0px auto; overflow:hidden;background-color:#F6F6F6; margin-top:20px;
             -moz-border-radius:10px;  -webkit-border-radius:10px; padding:30px;}
        .wrapper, #doclist { float:left; margin:0 20px 0 0;}
        .doclist { width:150px; border-right:solid 1px #dcdcdc;}
        .doclist ul { margin:0; list-style:none;}
        .doclist li { margin:10px 0; padding:0;}
        .documents { margin:0; padding:0;}
        
        .wrapper { width:700px; margin-top:20px;}
</style>
<script type="text/javascript" >
function savePages(mode){   
	 try{
	        if(saveTrackShipping(mode)){
	            return true;
	        }
	    }catch(e){
	        alert("exception " + e);
	    }
}
var attachFileId=0;
$(document).ready(function(){
	clearTracker();
	show_innernorth();
	 $("#displayAttach"+attachFileId).click(function(){
		 var idSeq = $(this).attr("class");
		 $("#displayAttach"+attachFileId).hide();
		 $("#attach"+idSeq).click();
	});
	/* $('#trackShippingTable').dataTable({
        "sPaginationType": "full_numbers",
        "aoColumns":[{"bSortable": false},
                       null,
                       null,
                       null,
                       null,
                       null,
                       null,
                       null,
                       null
                       ],
            "sDom": 'C<"clear">Rlfrtip',
            "oColVis": {
                "aiExclude": [ 0 ],
                "buttonText": "&nbsp;",
                "bRestore": true,
                "sAlign": "left"
            },
            "fnInitComplete": function () {
                $("#trackShippingColumn").html($('#trackShippingTable_wrapper .ColVis'));
            }
    }); */
	construtcTable(false);
    //addDocumentID();
	$("select").uniform(); 
	//loadquestions();
});
function construtcTable(flag){
	try{
		var criteria = " ";
	
		var trackShippingTable_serverParam = function ( aoData ) {
																aoData.push( { "name": "application", "value": "stafa"});
																aoData.push( { "name": "module", "value": "Ctl_TrackShipping"});
																
																aoData.push( { "name": "param_count", "value":"1"});
																aoData.push( { "name": "param_1_name", "value":":dailyAssignment:"});
																aoData.push( { "name": "param_1_value", "value":$("#dailyAssignment").val()}); 
											}		
														
		var trackShippingTable_aoColumnDef = [
			                         {			 "aTargets": [ 0], "mDataProp": function ( source, type, val ) {
			                        	 			prodid=source[7];
			                             			 return "<input name='shippingInformation' id= 'shippingInformation' type='checkbox' value='" + source[0] +"' onchange=''/>";
			                             }},
			                          {    "sTitle":'Shipped Date',
			                     	             "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
			    							        	return converDate(source[1]);
			                             			
			                             }},
			                          {    "sTitle":'Tracking #',
			                     	              "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
			                     	            	 return source[2];
			                             }},
			                          {    "sTitle":'Recipient MRN',
			                                      "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
			                                    	  return source[3];
			                             }},
			                          {    "sTitle":'Recipient Name',
				                                  "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
				                                	  return source[4];
				                         }}, 
				                      {    "sTitle":'Product ID',
				                                  "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
				                                	  return source[5];
				                         }}, 
				                              
			                          {    "sTitle":'Shipping Status',
			                                      "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
				                                    	if(source[6]!=null){
				                                    		var link = $("#statusspan #shipStatus option[value='"+source[6]+"']").text();
				                                    		return link;
				                                    	}else{
			                                    	  	  var selectTxt = "<span id='status'>"+ $("#statusspan").html();
				                                    	  selectTxt += "</span>";
				                                    	  return selectTxt;
				                                    	}
			                             }},    
			                          {    "sTitle":'Received By',
			                                      "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
			                                    	  if(source[7]!=null){
			                                    		  return source[7];
			                                    	  }else{
			                                    	  	 return "<input type='text' id='receivedBy' name='receivedBy'/>";
			                                    	  }
			                             }},    
			                          {    "sTitle":'Shipping Documents',
			                                      "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
			                                    	var documentId = '0';
			                                    	var documentHtml="<input type='hidden' id='document' name='document' value='"+ documentId+"'><img src='images/file_loading.gif' class='uploadImage' id='fileUploadImage' style='display:none'/>"+
					                                 	 "<div id='fileDisplayName' class='fileDisplay'></div><input type='hidden' class='fileName' id='fileName' name='fileName'/><input type='hidden' class='filePathId' name='filePathId' id='filePathId' value=''/><input type='hidden' class='fileUploadContentType' name='fileUploadContentType' id='fileUploadContentType' value=''/>"+
					                                   	 "<div class='attach' style='cursor: pointer;	color: #0101DF;' id='displayAttach'></div><input style='display:block' class='fileUpload' type='file' name='fileUpload' onChange='ajaxFileUpload1(this,\""+attachFileId+"\")' id='attach'/>";
				                            	    return documentHtml;
			                             }},  
			                        ];
		var trackShippingTable_aoColumn = [ { "bSortable": false},
		                               	   null,
							               null,
							               null,
							               null,
							               null,
							               null,
							               null,
							               null
				             ];
		var trackShippingTable_ColManager = function(){
			 									$("#trackShippingColumn").html($('#trackShippingTable_wrapper .ColVis'));
										 };
	 	var trackShippingTable_sort = [[ 1, "asc" ]];
	 	var trackShippingTable_MergeCols = [1,2,3,4];
		
		//constructMergeWithoutColManager(true,"dailyAssignmentTable",dailyAssignmentTable_serverParam,dailyAssignmentTable_aoColumnDef,dailyAssignmentTable_aoColumn,dailyAssignmentTable_sort,dailyAssignmentTable_MergeCols);
		constructTable(flag,'trackShippingTable',trackShippingTable_ColManager,trackShippingTable_serverParam,trackShippingTable_aoColumnDef,trackShippingTable_aoColumn);
	}catch (err) {
		alert("error " + err);
	}
}
/* function addDocumentID(){
	var i=0;
	$("#trackShippingTable tr").each(function(){
		if($(this).find("td").hasClass("col8")){
			if($(this).children().find("img").hasClass("uploadImage")){
				var cur_id=$(this).find("img").attr("id");
				var new_id=cur_id+i;
				$(this).find("img").attr("id", new_id);
			}
			if($(this).children().find("div").hasClass("fileDisplay")){
				var cur_id=$(this).children().find(".fileDisplay").attr("id");
				var new_id=cur_id+i;
				$(this).children().find(".fileDisplay").attr("id", new_id);
			}
			if($(this).children().find("input").hasClass("fileName")){
				var cur_id=$(this).children().find(".fileName").attr("id");
				var new_id=cur_id+i;
				$(this).children().find(".fileName").attr("id", new_id);
				//$(this).children().find(".fileName").attr("name", new_id);
			}
			if($(this).children().find("input").hasClass("filePathId")){
				var cur_id=$(this).children().find(".filePathId").attr("id");
				var new_id=cur_id+i;
				$(this).children().find(".filePathId").attr("id", new_id);
				//$(this).children().find(".filePathId").attr("name", new_id);
			}
			if($(this).children().find("input").hasClass("fileUploadContentType")){
				var cur_id=$(this).children().find(".fileUploadContentType").attr("id");
				var new_id=cur_id+i;
				$(this).children().find(".fileUploadContentType").attr("id", new_id);
			}
			if($(this).children().find("div").hasClass("attach")){
				var cur_id=$(this).children().find(".attach").attr("id");
				var new_id=cur_id+i;
				$(this).children().find(".attach").attr("id", new_id);
			}
			if($(this).children().find("input").hasClass("fileUpload")){
				var cur_id=$(this).children().find(".fileUpload").attr("id");
				var new_id=cur_id+i;
				$(this).children().find(".fileUpload").attr("id", new_id);
			//	$(this).children().find(".fileUpload").attr("name", new_id);
				var dat="ajaxFileUpload1(this,'"+i+"')";
				$(this).children().find(".fileUpload").attr("onchange",dat);
			} 
		i++;
		}
	});
} */
function saveTrackShipping(mode){
	
	try{
		var url="saveTrackShipping";
		var entityId = $("#entityId").val();
		//var entityType = $("#entityType").val();
		var rowData = "";
		var receivedBy="",shipStatus="",shippingInformation=""; 
		if(mode =="next"){
			$("#nextFlag").val("1");
		}
		$("#trackShippingTable tbody tr").each(function (row){
			$(this).find("td").each(function (col){
				if(col==0){
					shippingInformation=$(this).find("#shippingInformation").val();
				}
				if(col ==6){
					shipStatus=$(this).find("#shipStatus").val();
				}else if(col ==7){
					receivedBy=$(this).find("#receivedBy").val();
				}
			});
			rowData+= "{shippingInformation:"+shippingInformation+",shipStatus:"+shipStatus+",receivedBy:"+receivedBy+"},";
		});
		//alert("rowData::"+rowData);
		 if(rowData.length >0){
             rowData = rowData.substring(0,(rowData.length-1));
         }
		response = jsonDataCall(url,"jsonData={trackShippingData:["+rowData+"],nextFlag:"+$("#nextFlag").val()+",dailyAssignment:"+$("#dailyAssignment").val()+"}");
		construtcTable(true);
		$("select").uniform(); 
	   //  $('.progress-indicator').css( 'display', 'none' );
	  // alert("Data Saved Successfully");
	}catch(err){
		alert("err");
	}
	//var results1=JSON.stringify(serializeFormObject($('#trackShippingForm')));
	//var response = jsonDataCall(url,"jsonData={trackShippingData:["+results1+"],nextFlag:"+$("#nextFlag").val()+",dailyAssignment:"+$("#dailyAssignment").val()+"}");
}
</script>
<span id="hiddendropdowns" class="hidden">
	<span id="statusspan">
		 <s:select id='shipStatus'  name='shipStatus' list='lstTrackShippingStatus' listKey='pkCodelst' listValue='description'  headerKey='' headerValue='Select'/>
	</span>
</span>
<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Track Shipping</div>
		<div class="portlet-content">
			<form id="trackShippingForm">
				<s:hidden id="nextFlag" name="nextFlag" value="0" />
				<s:hidden id="dailyAssignment" name="dailyAssignment" value="%{dailyAssignment}"/>
				<s:hidden id="entityId" name="entityId" value="%{dailyAssignment}"/>
				<s:hidden name="entityType" id="entityType" value="TRACKSHIPPING"/>
	 			<div id="trackShippingTableDiv" class="dataTableOverFlow">		
				<table cellpadding="0" cellspacing="0" border="1" id="trackShippingTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='trackShippingColumn'></th>
							<th>Shipping Date</th>
							<th>Tracking #</th>
							<th>Recipient MRN</th>
							<th>Recipient Name</th>
							<th>Product ID</th>
							<th>Shipping Status</th>
							<th>Received By</th>
							<th>Shipping Documents</th>
						</tr>
					</thead>
				</table>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="cleaner"></div>

 <form onsubmit="return avoidFormSubmitting()">
	<div align="left" style="float:right;">
	    <table width="100%" cellpadding="0" cellspacing="0" class="" align="" border="0">
	        <tr>
	        <td><span><label>Next:</label>
	        <select name="nextOption" id="nextOption" class=""><option value="loadCTLTechHome.action">HomePage</option><option value="logout.action">Logout</option></select>
	        <input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
	        <input type="button" value="Next" onclick="verifyeSign('eSignNext','next')"/></span></td>
	        </tr>
	    </table>
	</div>
</form>  

<script>
$(function() {
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");

    /**For add new popup**/
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
    .append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

    $( ".portlet-header .ui-addnew" ).click(function() {
    	
    });
   
    /**For Refresh**/
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });
});


</script>