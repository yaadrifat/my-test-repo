<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script type="text/javascript" src="js/placeholder.js"></script>
<script type="text/javascript" src="js/dataSearch.js"></script>
<script type="text/javascript" src="js/jquery/jquery.loadjson.js"></script>
<script type="text/javascript" src="jsp/ctl/technicians/js/productTable.js"></script>
<style type="text/css">
       /*  body { font-family:Lucida Sans, Lucida Sans Unicode, Arial, Sans-Serif; font-size:13px; margin:0px auto;} */
        .tabs { margin:0; padding:0; list-style:none; overflow:hidden; }
        .tabs li { float:left; display:block; padding:5px; background-color:#bbb; margin-right:5px;}
        .tabs li a { color:#fff; text-decoration:none; }
        .tabs li.current { background-color:#e1e1e1;}
        .tabs li.current a { color:#000; text-decoration:none; }
        .tabs li a.remove { color:#f00; margin-left:10px;}
        .content { background-color:#FFFFFF;}
        .content p { margin: 0; padding:20px 20px 100px 20px;}
        
        .main { width:900px; margin:0px auto; overflow:hidden;background-color:#F6F6F6; margin-top:20px;
             -moz-border-radius:10px;  -webkit-border-radius:10px; padding:30px;}
        .wrapper, #doclist { float:left; margin:0 20px 0 0;}
        .doclist { width:150px; border-right:solid 1px #dcdcdc;}
        .doclist ul { margin:0; list-style:none;}
        .doclist li { margin:10px 0; padding:0;}
        .documents { margin:0; padding:0;}
        
        .wrapper { width:700px; margin-top:20px;}
</style>
<script type="text/javascript" >
function savePages(mode){   
	 try{
	        if(saveDiscard(mode)){
	            return true;
	        }
	    }catch(e){
	        alert("exception " + e);
	    }
}
$(document).ready(function(){
	clearTracker();
	showTracker($("#dailyAssignment").val(),null,ctlProductFlowType,null,false,true);
	show_slidewidgets("ctl_SlideWidget", false);
	show_slidecontrol("slidecontrol");
	show_innernorth();
	var dailyAssignment = $("#dailyAssignment").val();
	constructProductList(true);
	$("select").uniform(); 
	$(".dateEntry").datepicker({dateFormat: 'M dd, yy',
								changeMonth: true,
								changeYear: true,
								yearRange: '1900:' + new Date().getFullYear()
							});
	loadDiscardData();
	loadquestions(dailyAssignment,'DISCARD');
});
function loadDiscardData(){
	var productValue="";
	var discardresponse="";
	var url = "loadDiscardData";
	var dailyAssignment = $("#dailyAssignment").val();
	var insEntityType=$("#inspectionEntityType").val();
	if(dailyAssignment !=0 && dailyAssignment != "" && typeof(dailyAssignment)!='undefined'){
		 var jsonData = "dailyAssignment:"+dailyAssignment+",inspectionEntityType:"+insEntityType;
		 discardresponse = jsonDataCall(url,"jsonData={"+jsonData+"}");
	}
	
	var listlength=discardresponse.ctlInspection.length;
	addTab("inspectionTab_ul","inspectionContent","inspectionForm",listlength);
   
    if(discardresponse.ctlInspection.length>0){
	    $(discardresponse.ctlInspection).each(function(i){
	     	productid=this.product;
			$("#inspectionTab_ul li").find("input").each(function(j,v){
				var refprod=$(this).attr("refproductid");
 				var hiddenid=$(this).attr("id");
  				if(refprod==productid){
	  				//alert($(this).prop("id"));
					// alert(JSON.stringify(retrievalresponse.ctlInspection[i]));
	  				$(this).val(JSON.stringify(discardresponse.ctlInspection[i]));
					loadData1(hiddenid,"inspectionForm",refprod,'inspectionTab_ul');
  				} 
			});
	    });
    }
	$(discardresponse.discardList).each(function(i){
		//alert(this.discardDate);
		this.discardDate=converDate(this.discardDate);
		$("#discard").val(this.discard);
		$("#discardDate").val(this.discardDate);
		$("#supervisorSign").val(this.supervisorSign);
	}); 
}
function loadData1(hiddenid,formid,refproduct,tabid){
	var data="";
	data=$("#"+hiddenid).val();
	var result = jQuery.parseJSON(data);
	
	$("#"+formid).loadJSON(result);
	$.uniform.restore('select');
	$('select').uniform();
	loadData(hiddenid,formid,refproduct);

}
/*function constructProductList(flag,criteriaValue){
	var criteria = " ";
	var productFlag = false;
	var productCriteria;

		$("#scanId").val($("#divProductScan #fake_conformProductSearch").val() );
		//alert($("#scanId").val());
		criteria = " and pk_person=fk_person ";
		if($("#scanId").val()!=""){
			productCriteria= "SPEC_ID in ("+criteriaValue+")";
			//productCriteria= " lower(SPEC_ID) like lower('%" + $("#scanId").val() +"%')";
			productFlag = true;
		}
		else{
			productCriteria= "SPEC_ID in ("+criteriaValue+")";	
			productFlag = true;
		}
		if(productFlag){
				criteria += "and (";
				criteria += productCriteria;
				criteria += ")";
		}

	var productDataTable_serverParam = function ( aoData ) {
																aoData.push( { "name": "application", "value": "stafa"});
																aoData.push( { "name": "module", "value": "Ctl_currentProduct_List"});
																aoData.push( { "name": "criteria", "value": criteria});	
																
																aoData.push( { "name": "col_1_name", "value": "lower(nvl( SPEC_ID ,' '))"});
																aoData.push( { "name": "col_1_column", "value": "7"});
																
																aoData.push( { "name": "col_2_name", "value": "lower(nvl( (select nvl(PERSON_LNAME, ' ' ) || ' ' ||nvl(PERSON_FNAME, ' ' ) from person where pk_person=d.fk_receipent),' '))"});
																aoData.push( { "name": "col_2_column", "value": "2"});
																
																aoData.push( {"name": "col_3_name", "value": "lower(nvl((select nvl(person_mrn,' ') from person where pk_person=d.fk_receipent),' '))" } );
																aoData.push( { "name": "col_3_column", "value": "1"});
																
																 
																
															
													};
	var productDataTable_aoColumnDef = [
			                         {			 "aTargets": [ 0], "mDataProp": function ( source, type, val ) {
			                        	 			prodid=source[7];
			                             			 return "<input name='productcheck' id= 'productcheck' type='checkbox' value='" + source[0] +"' onchange='ontrigger(this,\""+prodid+"\")'/><input name='pkSpecimen' id= 'pkSpecimen' type='hidden' value='" + source[10] +"' />";
			                             }},
			                          {    "sTitle":'Product ID',
			                     	             "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
			                     	            	 product_id="<input type='hidden' class ='"+source[7]+"' id='specId' name='specId' value='"+source[7]+"'> ";
			                     	            	product_id+=source[7];
			    							        	return product_id;
			                             			
			                             }},
			                          {    "sTitle":'Recipient Name',
			                     	              "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
					                             	 return source[2];
			                             }},
			                          {    "sTitle":'Recipient MRN',
			                                      "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
			   			                             return source[1];
			                             }},
			                          {    "sTitle":'Recipient ABO/Rh',
				                                  "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
							                         return source[3];
				                         }}, 
				                      {    "sTitle":'<s:text name="stafa.label.donorname"/>',
				                                  "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
							                         return source[5];
				                         }}, 
				                              
			                          {    "sTitle":'Donor MRN',
			                                      "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
			   			                             return source[4];
			                             }},    
			                          {    "sTitle":'Donor ABO/Rh',
			                                      "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
			   			                             return source[6];
			                             }},    
			                          {    "sTitle":'Donor Type',
			                                      "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
			   			                             return source[9];
			                             }},  
			                          {    "sTitle":'Product Type',
			                                      "aTargets": [ 9], "mDataProp": function ( source, type, val ) {
			   			                             return source[8];
			                             }},    
			                          {    "sTitle":'Product Status',
			                                      "aTargets": [ 10], "mDataProp": function ( source, type, val ) {
			   			                             return source[11];
			                             }} 
			                        ];
	var productDataTable_aoColumn = [ { "bSortable": false},
	                               	   null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null
			             ];
	var productDataTable_ColManager = function(){
										$("#productDataTableColumn").html($('#productDataTable_wrapper .ColVis'));
									 };
	constructTable(flag,'productDataTable',productDataTable_ColManager,productDataTable_serverParam,productDataTable_aoColumnDef,productDataTable_aoColumn);
} */
function getScanData(event,obj){
	
  	if(event.keyCode==13){
  		 var selectedProduct = $("#searchProducts").val();
  	     var index = selectedProduct.indexOf($("#conformProductSearch").val());
  	     if(index==-1){
  	         if(selectedProduct == "," ){
  	             selectedProduct+=$("#conformProductSearch").val();
  	             var slicedVal = selectedProduct.substring(1);
  	             $("#searchProducts").val(slicedVal);
  	             criteriaValue = "'"+slicedVal+"'";
  	         }else{
  	             selectedProduct+=","+$("#conformProductSearch").val()+"";
  	             $("#searchProducts").val(selectedProduct);
  	             var str = selectedProduct.lastIndexOf(",");
  	             var slicedVal = selectedProduct.substring(str,selectedProduct.length);
  	             criteriaValue+= ",'"+slicedVal.substring(1)+"'";
  	             }
  	     }
		constructProductList(true,criteriaValue);
		resetForm($('#inspectionForm'));
		addTab("inspectionTab_ul","inspectionContent","inspectionForm");
	 	$("#inspectionTab_ul").find("li").each(function(j,v){
				 if($(this).hasClass("current")){
					var hiddenid=$(this).find("input").prop('id');
					refproduct=$(this).find("input").prop('refproduct')
					$("#activeInspectionTab").val(hiddenid); 
					loadData(hiddenid,'inspectionForm',refproduct);
				 }
		});
  	}
}
function addTab(tab_ulid,contentid,formid,listlength){
	 var proid="";
	 var proclas="";
	 var refproduct="";
	 var tempcontend=contentid;
	 var k=0;
	 $('#productDataTable tbody tr').each(function(i,v){
			proclas=$(v).find("#specId").attr("class");  
			proid=$(v).find("#specId").val();
			refproduct=$(v).find("#pkSpecimen").val();
			
	 	if($("#conformProductSearch").val()!=proid && $("#conformProductSearch").val()!=""){
			 if ($("#"+proid).length !=0 )
				{
		        	return;
				}
		   } 
	//?BB });  
	           		 $("#"+tab_ulid+" li").removeClass("current");
						if(listlength<=0){
							$("#pkInspection").val(0);
							$("#inspectionForm_product").val(refproduct);
						}
	            	
					var results=JSON.stringify(serializeFormObject($('#'+formid)));
					//alert("results-->"+results);
					
					 $("#"+tab_ulid).append("<li id='li_"+proid+formid +"_"+refproduct+"'class='current'><input type='hidden' id="+proid+formid+"   value='"+results+"' refproductid="+refproduct+"><a class='tab' id='a_" +
							 proid+formid+ "'href='#"+contentid+"' onclick=loadData('"+proid+formid+"','"+formid+"','"+refproduct+"') rel="+refproduct+">" + proid + 
			                "</a><a href='#' id="+proid+" class='remove' onclick=deletedtab(this,'"+proid+"','"+formid+"')>x</a></li>");
			        
			            $("#"+contentid).show();
				
	             if($("#"+tab_ulid+" li").hasClass("current")){
	            	 $("#pkInspection").val(0);
						$("#inspectionForm_product").val(refproduct);
	            }  
	            $('#'+tab_ulid+' a.tab').live('click', function() {
	                // Get the tab name
	                $("#"+tab_ulid+" li").removeClass("current");
	           
	                $(this).parent().addClass("current");
	            });
	 });         
}   
function loadData(hiddenid,formid,refproduct){
	//alert("1");
	var reresult=JSON.stringify(serializeFormObject($('#'+formid)));
	var hiddentabid= $("#activeInspectionTab").val();
	$("#"+hiddentabid).val(reresult);
	$("#activeInspectionTab").val(hiddenid);

	var data="";
	data=$("#"+hiddenid).val();
	var result = jQuery.parseJSON(data);

	$("#"+formid).loadJSON(result);
	$.uniform.restore('select');
	$('select').uniform();
}
function resetForm($form) {
    $form.find('input:text, input:password, input:file, select, textarea').val('');
    $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
    $.uniform.restore("select");
	$("select").uniform();
}
function setvalues(cBoxId,textId){
    var s=$('#'+cBoxId).attr('checked');
    if(s=='checked'){
        $('#'+textId).val("N/A");
    }else {
        $('#'+textId).val("");
    }
}
function checkCurrentProduct(){
    $("#inspectionTab_ul").find("li").each(function(i,v){
           if($(this).hasClass("current")){
               var prid= $(this).find("input").attr("id");
               var refid=$(this).find("a").attr('rel');
                loadData(prid,'inspectionForm',refid);
           }
        });
}
function saveDiscard(mode){
	var url='saveDiscard';
    var entityId = $("#entityId").val();
    var entityType = $("#entityType").val();
   // alert("entityId::::"+entityId+" entityType::"+entityType);
	var rowData = "";
	 if(mode =="next"){
			$("#nextFlag").val("1");
		}
	checkCurrentProduct();
	$("#inspectionTab_ul li").find("input").each(function(i,e){
			//rowdata+ =$(v).find("input[type=text]").val();
			tmpId1 = $(this).attr("id");
			
			if(i==0){
				rowData+=$("#"+tmpId1).val();
				}else{
					rowData+=","+$("#"+tmpId1).val();	
				}
			
	});
	//alert("rowData:::::"+rowData);
	
	saveQuestions('distributionVerification',entityId,entityType);
	var results1=JSON.stringify(serializeFormObject($('#discardInformation')));
		//results1 = results1.substring(1,(results1.length-1));
	
	var jsonData = "jsonData={inspectionData:["+rowData+"],discardData:["+results1+"],nextFlag:"+$("#nextFlag").val()+",dailyAssignment:"+$("#dailyAssignment").val()+"}";
	
	//var jsonData = "jsonData={discardData:{"+results1+"}}";
	//alert("jsondata---->"+jsonData);
	var response = jsonDataCall(url,jsonData);
	stafaAlert('Data saved Successfully','');
	return; 
}
</script>
<div id="ctl_SlideWidget" style="display:none;">

	<!-- Recipient Info start-->
	<div class="column">
		<div class="portlet">
			<div class="portlet-header ">Recipient Info</div>
			<div class="portlet-content">
				<table width="100%" border="0">
					<tr>
						<td width="50%"><s:text name="stafa.label.recipientmrn"/></td>
						<td width="50%" id="recipient_id"></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.name"/></td>
						<td id="recipient_name"></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.dob"/></td>
						<td id="recipient_dob"></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.aborh"/></td>
						<td id="recipient_aborh"></td>
					</tr>
					<tr>
						<td>AbS</td>
						<td id=""></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.diagnosis"/></td>
						<td id="recipient_diagnosis"></td>
					</tr>
					   <tr>
						<td><s:text name="stafa.label.weights"/></td>
						<td id="recipient_weight"></td>
					</tr>
				  </table>
			</div>
		</div>
	</div>
	<!--Recipient Info end-->
	
	<!-- Donor Info start-->
	<div class="column">
		<div class="portlet">
			<div class="portlet-header ">Donor Info</div>
			<div class="portlet-content">
				<table width="100%" border="0">
					<tr>
						<td width="50%">Donor MRN</td>
						<td width="50%" id="recipient_id"></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.name"/></td>
						<td id="recipient_name"></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.dob"/></td>
						<td id="recipient_dob"></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.aborh"/></td>
						<td id="recipient_aborh"></td>
					</tr>
					<tr>
						<td>AbS</td>
						<td id="recipient_aborh"></td>
					</tr>
					<tr>
						<td>Donor Type</td>
						<td id="recipient_diagnosis"></td>
					</tr>
				  </table>
			</div>
		</div>
	</div>
	<!--Donor Info end-->
	
</div>

<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Product Table</div>
		<div class="portlet-content">
			<form>
				<input type="hidden" id="activeRetrivalTab" name="activeRetrivalTab"/>
		        <input type="hidden" id="activeInspectionTab" name="activeInspectionTab"/>
				<!-- <input type="hidden" name="entityType" id="entityType" value="PRODUCT" /> -->
		        <input type="hidden" id="scanId" name="scanId"/>
		        <s:hidden id="searchProducts" name="searchProducts" value=","/>
				<div id="button_wrapper">
					<div id="divProductScan" style="margin-left:40%;">
						<input class="scanboxSearch barcodeSearch multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;" onkeyup="performScanData(event,this)"/>
						<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" onfocus="changeType(this)"/>
					</div>
	 			</div>
	 			<div id="productTableDiv" class="dataTableOverFlow">		
				<table cellpadding="0" cellspacing="0" border="1" id="productDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='productDataTableColumn'></th>
							<th>Product ID</th>
							<th>Recipient Name</th>
							<th>Recipient MRN</th>
							<th>Recipient ABO/Rh</th>
							<th>Donor Name</th>
							<th>Donor MRN</th>
							<th>Donor ABO/Rh</th>
							<th>Donor Type</th>
							<th>Product Type</th>
							<th>Product Status</th>
						</tr>
					</thead>
				</table>
				</div>
			</form>
		</div>
	</div>
</div>


<div class="cleaner"></div>

<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Product Inspection</div>
		<div class="portlet-content">
			<div id="discardInspectionTab">
				<ul class="tabs"  id="inspectionTab_ul">
	           
	        	</ul>
		        <form id="inspectionForm">
			        <input type="hidden" id="pkInspection" name="pkInspection" value="0"/>
	             	<input type="hidden" id="inspectionForm_product" name="product" value="0"/>
	             	<s:hidden id="dailyAssignment" name="dailyAssignment" value="%{dailyAssignment}" />
	             	<input type="hidden" name="inspectionEntityType" id="inspectionEntityType" value="DISCARD" />
		            <input type="hidden" id="entityType" name="entityType" value="DISCARD"/>
					<div id="inspectionContent" class="content">  
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
							<tr>
								<td class="tablewapper">Label Verfication</td>
								<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="isVerified" id="isVerified"/></td>
								<td class="tablewapper1">Reason for Discard</td>
								<td class="tablewapper1"><s:select id="fkCodelstDiscardReason"  name="fkCodelstDiscardReason" list="lstDiscardReason" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
							</tr>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>   

<div class="cleaner"></div>
         
<div class="column">		
	<div  class="portlet">
		<div class="portlet-header ">Distribution Verification</div>
		<div class="portlet-content">
			<form id="distributionVerification">
				<s:hidden name="entityId" id="entityId" value="%{dailyAssignment}"/>
				<input type="hidden" name="entityType" id="entityType" value="DISCARD"/>
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
	                <tr>
	                    <td class="questionsAlign">Cellular Therapy Product Discard Form Signed by Transplant Physician and CTL Medical Director</td>
	                    <%-- <td width="11%" height="40px"><s:radio name="" list="#{'1':'Yes','0':'No'}"/></td> --%>
	                    <td width="11%" height="40px"><input type="hidden" name="ctlProductDiscardForm_id" id="ctlProductDiscardForm_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="ctlProductDiscardForm"/><input type="radio" name="ctlProductDiscardForm" id="ctlProductDiscardForm_yes" value="1"/>Yes&nbsp;<input type="radio" id="ctlProductDiscardForm_no" name="ctlProductDiscardForm" value="0"/>No</td>
	                </tr>
	                <tr>
	                    <td class="questionsAlign">CTL Technician2 Signature</td>
	                    <td class="tablewapper1"><input type="hidden" name="ctlTech2Sign_id" id="ctlTech2Sign_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="ctlTech2Sign" /><input type="text" id="ctlTech2Sign" name="ctlTech2Sign"/>&nbsp;<input type="checkbox" name="ctlTech2Sign" id="ctlTech2Sign_na" onclick="setvalues('ctlTech2Sign_na','ctlTech2Sign')" value="-1"/>N/A</td>
	                   <%--  <td width="11%" height="40px"><input type='text'><s:radio name="" list="#{'-1':'N/A'}"/></td> --%>
	                </tr>
	            </table>
            </form>
		</div>
	</div>
</div>

<div class="cleaner"></div>

<div class="column">		
	<div  class="portlet">
		<div class="portlet-header ">Discard Information</div>
		<div class="portlet-content">
			<form id="discardInformation">
				<s:hidden id="nextFlag" name="nextFlag" value="0" />
				<s:hidden name="discard" id="discard" value="0"/>
			 	<s:hidden id="dailyAssignment" name="dailyAssignment" value="%{dailyAssignment}"/> 
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
					<tr>
	                   <td class="tablewapper">Discard Date</td>
			           <td class="tablewapper1"><input type='text' id="discardDate" name="discardDate" class="dateEntry"/></td>
			           <td class="tablewapper1">Supervisor Signature</td>
			           <td class="tablewapper"><input type='text' id="supervisorSign" name="supervisorSign"/></td>
					</tr>
	            </table>
            </form>
		</div>
	</div>
</div>

 <form onsubmit="return avoidFormSubmitting()">
	<div align="left" style="float:right;">
	    <table width="100%" cellpadding="0" cellspacing="0" class="" align="" border="0">
	        <tr>
	        <td><span><label>Next:</label>
	        <select name="nextOption" id="nextOption" class=""><option value="loadCTLTechHome.action">HomePage</option><option value="logout.action">Logout</option></select>
	        <input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
	        <input type="button" value="Next" onclick="verifyeSign('eSignNext','next')"/></span></td>
	        </tr>
	    </table>
	</div>
</form>  

<script>
$(function() {
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");

    /**For add new popup**/
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
    .append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

    $( ".portlet-header .ui-addnew" ).click(function() {
    	
    });
   
    /**For Refresh**/
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });
});


</script>