<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script type="text/javascript" src="js/placeholder.js"></script>
<script type="text/javascript" src="js/dataSearch.js"></script>
<script type="text/javascript" src="js/jquery/jquery.loadjson.js"></script>
<script type="text/javascript" src="jsp/ctl/technicians/js/productTable.js"></script>
<style type="text/css">
       /*  body { font-family:Lucida Sans, Lucida Sans Unicode, Arial, Sans-Serif; font-size:13px; margin:0px auto;} */
        .tabs { margin:0; padding:0; list-style:none; overflow:hidden; }
        .tabs li { float:left; display:block; padding:5px; background-color:#bbb; margin-right:5px;}
        .tabs li a { color:#fff; text-decoration:none; }
        .tabs li.current { background-color:#e1e1e1;}
        .tabs li.current a { color:#000; text-decoration:none; }
        .tabs li a.remove { color:#f00; margin-left:10px;}
        .content { background-color:#FFFFFF;}
        .content p { margin: 0; padding:20px 20px 100px 20px;}
        
        .main { width:900px; margin:0px auto; overflow:hidden;background-color:#F6F6F6; margin-top:20px;
             -moz-border-radius:10px;  -webkit-border-radius:10px; padding:30px;}
        .wrapper, #doclist { float:left; margin:0 20px 0 0;}
        .doclist { width:150px; border-right:solid 1px #dcdcdc;}
        .doclist ul { margin:0; list-style:none;}
        .doclist li { margin:10px 0; padding:0;}
        .documents { margin:0; padding:0;}
        
        .wrapper { width:700px; margin-top:20px;}
</style>
<script language="javascript">

$(document).ready(function(){
	clearTracker();
	showTracker($("#dailyAssignment").val(),null,ctlProductFlowType,null,false,true);
	show_slidewidgets("ctl_SlideWidget", false);
	show_slidecontrol("slidecontrol");
	show_innernorth();
	var dailyAssignment = $("#dailyAssignment").val();
	constructProductList(true);
	constructDocumentTable(false,dailyAssignment,'documentreviewTable');
	constructDocumentHistoryTable(false,dailyAssignment,'documentreviewTable1');
	$("select").uniform(); 
	$(".dateEntry").datepicker({dateFormat: 'M dd, yy',
								changeMonth: true,
								changeYear: true,
								yearRange: '1900:' + new Date().getFullYear()
							}); 
	loadInventoryData();
	/* var productFrom = $("#productFrom").val();
	if(productFrom=="Internal"){
		$("#internalInspection").show();
		$("#externalInspection").hide();
	}else{
		$("#internalInspection").hide();
		$("#externalInspection").show();
	} */
	$("#internalInspection").hide();
	$("#externalInspection").show();	
});
function loadInventoryData(){
	var productValue="";
	var inventoryResponse="";
	var url = "loadInventoryData";
	var dailyAssignment = $("#dailyAssignment").val();
	var insEntityType=$("#inspectionEntityType").val();
	if(dailyAssignment !=0 && dailyAssignment != "" && typeof(dailyAssignment)!='undefined'){
		 var jsonData = "dailyAssignment:"+dailyAssignment+",inspectionEntityType:"+insEntityType;
		 inventoryResponse = jsonDataCall(url,"jsonData={"+jsonData+"}");
	}
	$(inventoryResponse.inventoryDonorList).each(function(i,v){
		$("#donorType").val(v[1]);
		$("#productType").val(v[2]);
		$("#ProductState").val(v[3]);
		$("#collectionfacility").val(v[18]);
		$("#donordiagnosis").val(v[7]);
		$("#recipientMRN").val(v[4]);
		$("#recipientName").val(v[5]);
		$("#recipientabo").val(v[6]);
		$("#recipientAbs").val(v[9]);
		$("#donorAbs").val(v[10]);
		$("#recipientWeight").val(v[15]);
		$("#recipientDob").val(converHDate(v[17]));
		$("#donorMRN").val(v[11]);
		$("#donorWeight").val(v[14]);
		$("#donorabo").val(v[13]);
		$("#donorName").val(v[12]);
		$("#personDob").val(converHDate(v[16]));
		$("#donorUnitsWeight").val(v[22]);
		$("#recipientUnitsWeight").val(v[23]);
		$("#recipientWeightTaken").val(v[8]);
		$("#productFrom").val(v[24]);
		if(v[24] == "Internal"){
			$("#shipInfo").addClass("hidden");
		}else if(v[24] == "External"){
			$("#shipInfo").removeClass("hidden");
		}
	});
	/* addTab("inspectionTab_ul","inspectionContent","inspectionForm",listlength);
   
    if(discardresponse.ctlInspection.length>0){
	    $(discardresponse.ctlInspection).each(function(i){
	     	productid=this.product;
			$("#inspectionTab_ul li").find("input").each(function(j,v){
				var refprod=$(this).attr("refproductid");
 				var hiddenid=$(this).attr("id");
  				if(refprod==productid){
	  				//alert($(this).prop("id"));
					// alert(JSON.stringify(retrievalresponse.ctlInspection[i]));
	  				$(this).val(JSON.stringify(discardresponse.ctlInspection[i]));
					loadData1(hiddenid,"inspectionForm",refprod,'inspectionTab_ul');
  				} 
			});
	    });
    } */
	
	$.uniform.restore('select');
	$('select').uniform();
}
/* function getScanData(event,obj){
	
  	if(event.keyCode==13){
  		 var selectedProduct = $("#searchProducts").val();
  	     var index = selectedProduct.indexOf($("#conformProductSearch").val());
  	     if(index==-1){
  	         if(selectedProduct == "," ){
  	             selectedProduct+=$("#conformProductSearch").val();
  	             var slicedVal = selectedProduct.substring(1);
  	             $("#searchProducts").val(slicedVal);
  	             criteriaValue = "'"+slicedVal+"'";
  	         }else{
  	             selectedProduct+=","+$("#conformProductSearch").val()+"";
  	             $("#searchProducts").val(selectedProduct);
  	             var str = selectedProduct.lastIndexOf(",");
  	             var slicedVal = selectedProduct.substring(str,selectedProduct.length);
  	             criteriaValue+= ",'"+slicedVal.substring(1)+"'";
  	             }
  	     }
		constructProductList(true);
	 	resetForm($('#inspectionForm'));
		addTab("inspectionTab_ul","inspectionContent","inspectionForm");
	 	$("#inspectionTab_ul").find("li").each(function(j,v){
				 if($(this).hasClass("current")){
					var hiddenid=$(this).find("input").prop('id');
					refproduct=$(this).find("input").prop('refproduct')
					$("#activeInspectionTab").val(hiddenid); 
					loadData(hiddenid,'inspectionForm',refproduct);
				 }
		}); 
  	}
}
function addTab(tab_ulid,contentid,formid,listlength){
	 var proid="";
	 var proclas="";
	 var refproduct="";
	 var tempcontend=contentid;
	 var k=0;
	 $('#productDataTable tbody tr').each(function(i,v){
			proclas=$(v).find("#specId").attr("class");  
			proid=$(v).find("#specId").val();
			refproduct=$(v).find("#pkSpecimen").val();
			
	 	if($("#conformProductSearch").val()!=proid && $("#conformProductSearch").val()!=""){
			 if ($("#"+proid).length !=0 )
				{
		        	return;
				}
		   } 
	//?BB });  
	           		 $("#"+tab_ulid+" li").removeClass("current");
						if(listlength<=0){
							$("#pkInspection").val(0);
							$("#inspectionForm_product").val(refproduct);
						}
	            	
					var results=JSON.stringify(serializeFormObject($('#'+formid)));
					//alert("results-->"+results);
					
					 $("#"+tab_ulid).append("<li id='li_"+proid+formid +"_"+refproduct+"'class='current'><input type='hidden' id="+proid+formid+"   value='"+results+"' refproductid="+refproduct+"><a class='tab' id='a_" +
							 proid+formid+ "'href='#"+contentid+"' onclick=loadData('"+proid+formid+"','"+formid+"','"+refproduct+"') rel="+refproduct+">" + proid + 
			                "</a><a href='#' id="+proid+" class='remove' onclick=deletedtab(this,'"+proid+"','"+formid+"')>x</a></li>");
			        
			            $("#"+contentid).show();
				
	             if($("#"+tab_ulid+" li").hasClass("current")){
	            	 $("#pkInspection").val(0);
						$("#inspectionForm_product").val(refproduct);
	            }  
	            $('#'+tab_ulid+' a.tab').live('click', function() {
	                // Get the tab name
	                $("#"+tab_ulid+" li").removeClass("current");
	           
	                $(this).parent().addClass("current");
	            });
	 });         
}   
function loadData(hiddenid,formid,refproduct){
	//alert("1");
	var reresult=JSON.stringify(serializeFormObject($('#'+formid)));
	var hiddentabid= $("#activeInspectionTab").val();
	$("#"+hiddentabid).val(reresult);
	$("#activeInspectionTab").val(hiddenid);

	var data="";
	data=$("#"+hiddenid).val();
	var result = jQuery.parseJSON(data);

	$("#"+formid).loadJSON(result);
	$.uniform.restore('select');
	$('select').uniform();
}
function resetForm($form) {
   $form.find('input:text, input:password, input:file, select, textarea').val('');
   $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
   $.uniform.restore("select");
	$("select").uniform();
}
function setvalues(cBoxId,textId){
   var s=$('#'+cBoxId).attr('checked');
   if(s=='checked'){
       $('#'+textId).val("N/A");
   }else {
       $('#'+textId).val("");
   }
}
function checkCurrentProduct(){
   $("#inspectionTab_ul").find("li").each(function(i,v){
          if($(this).hasClass("current")){
              var prid= $(this).find("input").attr("id");
              var refid=$(this).find("a").attr('rel');
               loadData(prid,'inspectionForm',refid);
          }
 */
function constructDocumentTable(flag,dailyAssignment,tableId) {
	//alert("construct table : "+tableId+",Donor : "+product);
		var criteria = "";
		if(dailyAssignment!=null && dailyAssignment!=""){
			criteria = " and fk_Entity ="+dailyAssignment ;
		}
		
		var documentColumn = [ { "bSortable": false},
			                	null,
			                	null,
			                	null,
			                	null,
			                	null
				             ];
													
		var documentServerParams = function ( aoData ) {
										aoData.push( { "name": "application", "value": "stafa"});
										aoData.push( { "name": "module", "value": "aphresis_DocumentReview"});
										aoData.push( { "name": "criteria", "value": criteria});
										
										aoData.push( {"name": "col_1_name", "value": "lower(DOCUMENT_FILENAME)" } );
										aoData.push( { "name": "col_1_column", "value": "lower(DOCUMENT_FILENAME)"});
										
										aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
										aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
													
										
					};
		var documentaoColDef = [
					                  {		
					                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
				                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
				                	 			return "<input type='hidden' id='documentRef' name='documentRef' value='"+source[0] +"' > <input name='docid' id='docid' type='checkbox' value='"+source[0] +"' />" ;
				                	 			//return "";
				                	     }},
					                  { "sTitle": "Document Title",
				                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
					                	    	 //alert('docTitle'+source.documentFileName); 
					                	 		return source[6];
					                	 }},
									  { "sTitle": "Document Date",
					                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
											    return source[2];
											    }},
									  { "sTitle": "Attachment",
											 "aTargets": [3], "mDataProp": function ( source, type, val ) {
										 	   return source[1] + " &nbsp;&nbsp;<a href='#' onclick='openPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\")'> View </a>";
									  		}},
									  { "sTitle": "Reviewed",
									  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
									  			 if(source[3] ==1){
									  				 return "Reviewed";
									  			 }else{
									  				 return "";
									  			 }
										  		//return "<input type='checkbox'/>";
										  }},
									  { "sTitle": "Version #",
					                		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
											    return source[7];
											    }}
									];
		var documentColManager = function () {
												$("#documentreviewColumn").html($("#"+tableId+"_wrapper .ColVis"));
											};
											
		constructTable(flag,tableId,documentColManager,documentServerParams,documentaoColDef,documentColumn);
	} 


function constructDocumentHistoryTable(flag,dailyAssignment,tableId) {
	//alert("construct table : "+tableId+",Donor : "+donor);
	var criteria = "";
	if(dailyAssignment!=null && dailyAssignment!=""){
		criteria = " and fk_Entity ="+dailyAssignment ;
	}
	var docHistoryaoColumn =  [ { "bSortable": false},
				                	null,
				                	null,
				                	null,
				                	null,
				                	null
					            ];
	
	var docHistoryServerParams = function ( aoData ) {
									aoData.push( { "name": "application", "value": "stafa"});
									aoData.push( { "name": "module", "value": "aphresis_DocumentReview_History"});
									aoData.push( { "name": "criteria", "value": criteria});
									
									aoData.push( {"name": "col_1_name", "value": "lower(DOCUMENT_FILENAME)" } );
									aoData.push( { "name": "col_1_column", "value": "lower(DOCUMENT_FILENAME)"});
									
									aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
									aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
												
									
				};
				
	var	docHistoryaoColDef = [
				                  {		
				                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
			                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
			                	 			return "<input type='hidden' id='documentRef' name='documentRef' value='"+source[0] +"' >";
			                	 			//return "";
			                	     }},
				                  { "sTitle": "Document Title",
			                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
				                	    	 //alert('docTitle'+source.documentFileName); 
				                	 		return source[6];
				                	 }},
								  { "sTitle": "Document Date",
				                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
										    return source[2];
										    }},
								  { "sTitle": "Attachment",
										 "aTargets": [3], "mDataProp": function ( source, type, val ) {
									 	   return source[1] + " &nbsp;&nbsp;<a href='#' onclick='openPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\")'> View </a>";
								  		}},
								  { "sTitle": "Reviewed",
								  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
								  			 if(source[3] ==1){
								  				 return "Reviewed";
								  			 }else{
								  				 return "";
								  			 }
									  		//return "<input type='checkbox'/>";
									  }},
								  { "sTitle": "Version #",
				                		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
										    return source[7];
										    }}
								];
	
	var docHistoryColManager =  function () {
											$("#documentreviewColumn1").html($("#"+tableId+"_wrapper .ColVis"));
								};
								
	constructTable(flag,tableId,docHistoryColManager,docHistoryServerParams,docHistoryaoColDef,docHistoryaoColumn);

	$("#documentreviewTable1_wrapper").find("#"+tableId).removeAttr("style");
} 
</script>

<div class="cleaner"></div>
<s:hidden id="productFrom" name="productFrom" value=""/>
<%-- 
<div class="column">
    <div  class="portlet">
    <div class="portlet-header">Product Table</div><br>
        <div class="portlet-content">
	        <form>
	        	<input type="hidden" id="activeRetrivalTab" name="activeRetrivalTab"/>
			    <input type="hidden" id="activeInspectionTab" name="activeInspectionTab"/>
				<!-- <input type="hidden" name="entityType" id="entityType" value="PRODUCT" /> -->
			    <input type="hidden" id="scanId" name="scanId"/>
			    <s:hidden id="searchProducts" name="searchProducts" value=","/>
	            <div id="button_wrapper">
		            <div id="internalTransportScan" style="margin-left:40%;">
	       				<input class="scanboxSearch barcodeSearch"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;" onkeyup="" />
						<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" onfocus="changeType(this)"/>
		 			</div>
	            </div>
	            <div id="internalTransportDiv" style="overflow-x:auto;overflow-y:hidden;">
		            <table cellpadding="0" cellspacing="0" border="0" id="internalTransportTable" class="display">
		                <thead>
		                    <tr>
		                        <th width="4%" id="internalTransportColumn"></th>
		                        <th>Product ID</th>
		                        <th>Recipient Name</th>
		                        <th>Recipient MRN</th>
		                        <th>Recipient ABO/Rh</th>
		                        <th>Donor Name</th>
		                        <th>Donor MRN</th>
		                        <th>Donor ABO/Rh</th>
		                        <th>Donor Type</th>
		                        <th>Product Type</th>
		                        <th>Product Status</th>
		                    </tr>
		                </thead>
		                <tbody>
		                  <s:select id="fkCodelstDiscardReason"  name="fkCodelstDiscardReason" list="lstDiscardReason" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/>
		                </tbody>
		            </table>
	            </div>
			</form>
        </div>
    </div>
</div> --%>
<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Product Table</div>
		<div class="portlet-content">
			<form>
				<input type="hidden" id="activeRetrivalTab" name="activeRetrivalTab"/>
		        <input type="hidden" id="activeInspectionTab" name="activeInspectionTab"/>
				<!-- <input type="hidden" name="entityType" id="entityType" value="PRODUCT" /> -->
		        <input type="hidden" id="scanId" name="scanId"/>
		        <s:hidden id="searchProducts" name="searchProducts" value=","/>
				<div id="button_wrapper">
					<div id="divProductScan" style="margin-left:40%;">
						<input class="scanboxSearch barcodeSearch multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;" onkeyup="performScanData(event,this)"/>
						<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" onfocus="changeType(this)"/>
					</div>
	 			</div>
	 			<div id="productTableDiv" class="dataTableOverFlow">		
				<table cellpadding="0" cellspacing="0" border="1" id="productDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='productDataTableColumn'></th>
							<th>Product ID</th>
							<th>Recipient Name</th>
							<th>Recipient MRN</th>
							<th>Recipient ABO/Rh</th>
							<th>Donor Name</th>
							<th>Donor MRN</th>
							<th>Donor ABO/Rh</th>
							<th>Donor Type</th>
							<th>Product Type</th>
							<th>Product Status</th>
						</tr>
					</thead>
				</table>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="cleaner"></div>

<div class="column">
    <div  class="portlet">
    <div class="portlet-header">Product Receipt</div>
        <div class="portlet-content">
        <form>
        	<input type='hidden' id='receiptInformation' name='receiptInformation' value='0'/>
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Receipt Date</td>
                    <td class="tablewapper1"><input type="text" id="receiptDate" name='receiptDate' class="dateEntry"/></td>
                    <td class="tablewapper">Receipt Temperature. -High(C)</td>
                    <td class="tablewapper1"><input type="text" id="receiptTempHigh" name='receiptTempHigh'/></td>
                </tr>
                <tr>
                    <td class="tablewapper">Receipt Time</td>
                    <td class="tablewapper1"><input type="text" id="receiptTime" name='receiptTime'/></td>
                    <td class="tablewapper">Receipt Temperature. -Low(C)</td>
                    <td class="tablewapper1" style="width:22%"><input type="text" id="receiptTempLow" name='receiptTempLow'/>&nbsp;<input type='radio'/>N/A</td>
                </tr>
                <tr>
                    <td class="tablewapper">Receipt Location</td>
                    <td class="tablewapper1"><select><option>CTL - Station 2</option></select></td>
                    <td class="tablewapper">Data Logger in Alarm</td>
                    <td class="tablewapper1"><s:radio name="dataLogger" list="#{'1':'Yes','0':'No','-1':'N/A'}"/></td>
                </tr>
                <tr>
                    <td class="tablewapper"># of Bags Received</td>
                    <td class="tablewapper1"><input type='text' id='noOfBags' name='noOfBags'/></td>
                    <td class="tablewapper">CTL Temperature(C)</td>
                    <td class="tablewapper1"><input type="text" id='ctlTemp' name='ctlTemp'/></td>
                </tr>
                 <tr>
                    <td class="tablewapper"># of Vials/Tubes Received</td>
                    <td class="tablewapper1"><input type='text' id='noOfVials' name='noOfVials'/></td>
                    <td class="tablewapper">Outer Container Temperature(C)</td>
                    <td class="tablewapper1" style="width:22%"><input type="text" id='outerContainTemp' name='outerContainTemp'/>&nbsp;<input type='radio'/>N/A</td>
                </tr>
			</table> 
		</form>
        </div>
    </div>  
</div>

<div class="cleaner"></div>

<div class="column">
    <div  class="portlet">
    <div class="portlet-header">Product Information</div>
        <div class="portlet-content">
        <form>
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Donor Type</td>
                    <td class="tablewapper1"><s:select id="donorType"  name="donorType" list="lstDonationType" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
                     <td class="tablewapper">Review Lab Results</td>
                    <td class="tablewapper1"><input type="radio" id="" name=''/>Yes<input type="radio" id="" name=''/>No&nbsp;<a href="#">View</a></td>
                </tr>
                <tr>
                    <td class="tablewapper">Product Type</td>
                    <td class="tablewapper1"><s:select id="productType"  name="productType" list="lstProductType" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
                    <td class="tablewapper">Additives and Anticoagulants</td>
                    <td class="tablewapper1"><input type="radio" id="" name=''/>Yes<input type="radio" id="" name=''/>No&nbsp;<a href="#">View</a></td>
                </tr>
                <tr>
                    <td class="tablewapper">Product Volume(mL)</td>
                    <td class="tablewapper1"><input type='text'/></td>
                    <td class="tablewapper">Other Alias</td>
                    <td class="tablewapper1"><input type="radio" id="" name=''/>Yes<input type="radio" id="" name=''/>No&nbsp;<a href="#">View</a></td>
                </tr>
                <tr>
                    <td class="tablewapper">Product Weight(g)</td>
                    <td class="tablewapper1"><input type='text'/></td>
                    <td class="tablewapper">Type Check Ordered</td>
                    <td class="tablewapper1"><input type="radio" id="" name=''/>Yes<input type="radio" id="" name=''/>No</td>
                </tr>
                 <tr>
                    <td class="tablewapper">Product Container</td>
                    <td class="tablewapper1"><select><option>Select...</option></select></td>
                    <td class="tablewapper">Product Quarantined</td>
                    <td class="tablewapper1"><input type="radio" id="" name=''/>Yes<input type="radio" id="" name=''/>No</td>
                </tr>
                <tr>
                    <td class="tablewapper">Product State</td>
                    <td class="tablewapper1"><s:select id="ProductState"  name="ProductState" list="lstProductState" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
                    <td class="tablewapper"></td>
                    <td class="tablewapper1"></td>
                </tr>
			</table>
		</form>
        </div>
    </div>  
</div>

<div class="cleaner"></div>

<div class="column">
    <div  class="portlet">
    <div class="portlet-header">Plasma Information</div>
        <div class="portlet-content">
        	<div id="button_wrapper">
	            <div style="margin-left:40%;">
       				<input class="scanboxSearch barcodeSearch"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;" onkeyup="" />
					<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" onfocus="changeType(this)"/>
	 			</div>
            </div>
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Plasma ID</td>
                    <td class="tablewapper1"><select><option>ALLO Unrelated</option></select></td>
                     <td class="tablewapper">Collection Date</td>
                    <td class="tablewapper1"><input type="text" id="" name='' class="dateEntry"/></td>
                </tr>
                <tr>
                    <td class="tablewapper">Plasma Volume</td>
                    <td class="tablewapper1"><select><option>HPC-A</option></select></td>
                    <td class="tablewapper">Collection Start Time</td>
                    <td class="tablewapper1"><input type="text" id="" name=''/></td>
                </tr>
			</table> 
        </div>
    </div>  
</div>
 
<div class="cleaner"></div>

<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Product Inspection</div>
		<div class="portlet-content">
			<div id="proInventoryInspectionTab">
				<ul class="tabs"  id="inspectionTab_ul">
	           
	        	</ul>
		        <form id="inspectionForm">
			        <input type="hidden" id="pkInspection" name="pkInspection" value="0"/>
	             	<input type="hidden" id="inspectionForm_product" name="product" value="0"/>
	             	<s:hidden id="dailyAssignment" name="dailyAssignment" value="%{dailyAssignment}" />
	             	<input type="hidden" name="inspectionEntityType" id="inspectionEntityType" value="PRODUCTINVENTORY" />
		            <input type="hidden" id="entityType" name="entityType" value="PRODUCTINVENTORY"/>
					<div id="inspectionContent" class="content">  
						<table cellpadding="0" cellspacing="0" class="" align="center" id="internalInspection" border="0" width="100%">
							<tr>
								<td class="tablewapper">Product Container Intact With No Leaking</td>
								<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="containerIntactLeaking" id="containerIntactLeaking"  /></td>
								<td class="tablewapper">Product Appearance OK</td>
								<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="appearance" id="appearance"/></td>
							</tr>
							<tr>
								<td class="tablewapper">Label Verfication</td>
								<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="isVerified" id="isVerified"  /></td>
								<td class="tablewapper">Explanations</td>
								<td class="tablewapper1"><input type='text' name="explanations" id="explanations"/></td>
							</tr>
						</table>
						<table cellpadding="0" cellspacing="0" class="" align="center" id="externalInspection" border="0" width="100%">
							<tr>
								<td class="tablewapper" style="width:26%">Outside Transport Container Locked and Intact</td>
								<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="outsideTransContainer" id="outsideTransContainer"  /></td>
								<td class="tablewapper">Physician Notified of Product Arrival</td>
								<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="physicianNotified" id="physicianNotified"/></td>
							</tr>
							<tr>
								<td class="tablewapper" style="width:26%">Inside Transport Container Locked and Intact</td>
								<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="insideTransContainer" id="insideTransContainer"/></td>
								<td class="tablewapper">Product Appearance OK</td>
								<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="appearance" id="appearance"/></td>
							</tr>
							<tr>
								<td class="tablewapper">Product Container Intact with No Leaking</td>
								<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="containerIntactLeaking" id="containerIntactLeaking"/></td>
								<td class="tablewapper">Explanations</td>
								<td class="tablewapper1"><input type='text' name="explanations" id="explanations"/></td>
							</tr>
							<tr>
								<td class="tablewapper">Product Completely Frozen Upon Arrival</td>
								<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="productFrozen" id="productFrozen"/></td>
								<td class="tablewapper">Label Verfication</td>
								<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="isVerified" id="isVerified"  /></td>
							</tr>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>  
 
 
 
 
 
 
 
<%--  
 <div class="column">
	<div class="portlet">

		<div class="portlet-header ">Product Inspection</div>
		<div class="portlet-content">
		<div id="producttabs">
				   <ul class="tabs"  id="inspectionTab_ul">
           
        </ul>
        <form id="inspectionForm">
        	<input type="hidden" id="pkInspection" name="pkInspection" value="0"/>
	        <input type="hidden" id="fkPreparation" name="fkPreparation" value="0"/>
	        <input type="hidden" id="inspectionForm_product" name="product" value="0" />
	        <s:hidden id="dailyAssignment" name="dailyAssignment" value="%{dailyAssignment}" /> 
	        <input type="hidden" name="inspectionEntityType" id="inspectionEntityType" value="INS_PREPARATION" />
			<div id="inspectionContent" class="content">  
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
					<tr>
						<td class="tablewapper">Product Appearance OK</td>
						<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="appearance" id="appearance"/></td>
						<td class="tablewapper">Label Verfication</td>
						<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="isVerified" id="isVerified"  /></td>
						
					</tr>
					<tr>
						<td class="tablewapper">Product Container Intact</td>
						<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="containerIntact" id="containerIntact"/></td>
						<td class="tablewapper">Product Requires ACD</td>
						<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="requiresACD" id="requiresACD"/></td>
					</tr>
				</table>
			</div>
		
		</form>
	</div>
</div>
</div>
</div> --%>
 
 
 
 
 
 
<div class="cleaner"></div>

<div class="column">
    <div  class="portlet">
    <div class="portlet-header">Document Reciew</div>
        <div class="portlet-content">
        	<div id="tabs">
			    <ul >
			    	<li><a href="#tabs-1">Current</a></li>
			    	<li><a href="#tabs-2">Historic</a></li>
			    </ul>
			    <div id="tabs-1">
			    <div>&nbsp;</div>
					<div class="tableOverflowDiv">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" id="documentreviewTable" class="display">
							<thead>
								<tr>
									<th width="4%" id="documentreviewColumn"></th>
									<th>Document Title</th>
									<th>Document Date</th>
									<th>Attachment</th>
									<th>Reviewed</th>
									<th>Version #</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
        		</div>
        		<div id="tabs-2">
        		<div>&nbsp;</div>
			  		<div class="tableOverflowDiv">
						<table cellpadding="0" cellspacing="0" border="0" id="documentreviewTable1" width="100%" class="display">
							<thead>
								<tr>
									<th width="4%" id="documentreviewColumn1"></th>
									<th>Document Title</th>
									<th>Document Date</th>
									<th>Attachment</th>
									<th>Reviewed</th>
									<th>Version #</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 
<div class="cleaner"></div>

<div class="column" id="shipInfo">
    <div  class="portlet">
    <div class="portlet-header">Shipping Information</div>
        <div class="portlet-content">
        <form>
        	<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Shipping Company</td>
                    <td class="tablewapper1"><select><option>Select</option></select></td>
                    <td class="tablewapper">Shipping Environment</td>
                    <td class="tablewapper1"><select><option>Select</option></select></td>
                </tr>
                <tr>
                    <td class="tablewapper">Shipping Document Date</td>
                    <td class="tablewapper1"><input type="file" id="" name=''/></td>
                    <td class="tablewapper"></td>
                    <td class="tablewapper1"></td>
                </tr>
			</table> 
		</form>
        </div>
	</div>
</div>	              
 
<div class="cleaner"></div>
 
<div class="column">
    <div  class="portlet">
    <div class="portlet-header">Collection Information</div>
        <div class="portlet-content">
        <form>
        	<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Collection Facility</td>
                    <td class="tablewapper1"><s:select id="collectionfacility"  name="collectionfacility" list="lstCollectionFacility" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
                    <td class="tablewapper">Collection Start Time</td>
                    <td class="tablewapper1" style="width:22%"><input type="text" id="" name=''/>&nbsp;<input type='radio' id="" name=""/>&nbsp;N/A</td>
                </tr>
                <tr>
                    <td class="tablewapper">Collection Date</td>
                    <td class="tablewapper1"><input type="text" id="" name=''/></td>
                    <td class="tablewapper">Collection End Time</td>
                    <td class="tablewapper1" style="width:22%"><input type="text" id="" name=''/>&nbsp;<input type='radio' id="" name=""/>&nbsp;N/A</td>
                </tr>
			</table>
		</form>  
        </div>
	</div>
</div>
 
<div class="cleaner"></div>
 
<div class="column">
    <div  class="portlet">
    <div class="portlet-header">Recipient Information</div>
        <div class="portlet-content">
        <form>
        	<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Recipient MRN</td>
                    <td class="tablewapper1"><input type='text' id="recipientMRN" name="recipientMRN"/></td>
                    <td class="tablewapper">Diagnosis</td>
                    <td class="tablewapper1"><s:select id="donordiagnosis"  name="donordiagnosis" list="lstDiagonosis" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
                </tr>
                <tr>
                    <td class="tablewapper">Other Alias</td>
                    <td class="tablewapper1"><input type="radio" id="" name=''/>Yes<input type="radio" id="" name=''/>No&nbsp;<a href="#">View</a></td>
                    <td class="tablewapper">Recipient ABO/Rh</td>
                    <td class="tablewapper1"><s:select id="recipientabo"  name="recipientabo" list="lstABORH" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeABO('recipient');"/></td>
                </tr>
                <tr>
                    <td class="tablewapper">Recipient Name</td>
                    <td class="tablewapper1"><input type="text" id="recipientName" name='recipientName'/></td>
                    <td class="tablewapper">Recipient AbS</td>
                    <td class="tablewapper1"><s:select id="recipientAbs"  name="recipientAbs" list="lstABS" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"  onchange="changeABS('recipient')"/></td>
                </tr>
                <tr>
                    <td class="tablewapper">Recipient DOB</td>
                    <td class="tablewapper1"><input type="text" id="recipientDob" name='recipientDob' class='dateEntry'/></td>
                    <td class="tablewapper">Recipient Gender</td>
                    <td class="tablewapper1"><s:select id="gender"  name="gender" list="lstGender" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
                </tr>
                <tr>
                    <td class="tablewapper">Recipient Weight (kg)</td>
                    <td class="tablewapper1"><input type="text" id="recipientWeight" name="recipientWeight"style="width:55px;" class='decimalTextBox '/>&nbsp;<span class='smallSelector '><s:select id="recipientUnitsWeight"  name="recipientUnitsWeight" list="lstUnitWeight" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeUnit('recipient');" /></span></td>
                    <td class="tablewapper">Transplant Physician</td>
                    <td class="tablewapper1"><s:select id="transplantPhysician"  name="transplantPhysician" list="lstTransplantPhysician" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
                </tr>
                <tr>
                    <td class="tablewapper">Date of Recipient Weight Taken</td>
                     <td class="tablewapper1"><input type="text" id="recipientWeightTaken" name="recipientWeightTaken" class="dateEntry"/></td>
                    <td class="tablewapper"></td>
                    <td class="tablewapper1"></td>
                </tr>
			</table>
		</form>
        </div>
	</div>
</div>	              
 
<div class="cleaner"></div>

<div class="column">
    <div  class="portlet">
    <div class="portlet-header">Donor Information</div>
        <div class="portlet-content">
        <form>
        	<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Donor MRN</td>
                    <td class="tablewapper1"><input type='text' id="donorMRN" name="donorMRN"/></td>
                    <td class="tablewapper">Donor ABO/Rh</td>
                    <td class="tablewapper1"><s:select id="donorabo"  name="donorabo" list="lstABORH" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeABO('donor');"/></td>
                </tr>
                <tr>
                    <td class="tablewapper">Other Alias</td>
                    <td class="tablewapper1"><input type="radio" id="" name=''/>Yes<input type="radio" id="" name=''/>No&nbsp;<a href="#">View</a></td>
                    <td class="tablewapper">Donor AbS</td>
                    <td class="tablewapper1"><s:select id="donorAbs"  name="donorAbs" list="lstABS" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeABS('donor')"/></td>
                </tr>
                <tr>
                    <td class="tablewapper">Donor Name</td>
                    <td class="tablewapper1"><input type="text" id="donorName" name='donorName'/></td>
                    <td class="tablewapper">Donor Weight(kg)</td>
                    <td class="tablewapper1"><input type="text" id="donorWeight" name="donorWeight"style="width:55px;" class='decimalTextBox'/>&nbsp;<span class='smallSelector '><s:select id="donorUnitsWeight"  name="donorUnitsWeight" list="lstUnitWeight" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeUnit('donor');"/></span></td>
                   
                </tr>
                <tr>
                	<td class="tablewapper">Donor DOB</td>
                    <td class="tablewapper1"><input type="text" id="personDob" name='personDob' class='dateEntry'/></td>
                 	<td class="tablewapper">Donor Gender</td>
                    <td class="tablewapper1"><s:select id="gender"  name="gender" list="lstGender" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
                </tr>
                <tr>
                    <td class="tablewapper">Donor Eligibility</td>
                    <td class="tablewapper1"><select><option>Select</option></select></td>
                   <td class="tablewapper">Donor Physician</td>
                    <td class="tablewapper1"><s:select id="physicianID"  name="physicianID" list="lstDonorPhysician" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
                </tr>
			</table>
		</form>
        </div>
	</div>
</div>

<div class="column">
    <div  class="portlet">
    	<div class="portlet-header">Donor Information</div>
        <div class="portlet-content">
         <form>
        	<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Processing Protocol Pages</td>
                    <td class="tablewapper1"><select><option>Select...</option></select>&nbsp;<img src = "images/icons/addnew.jpg" class="cursor" id="" onclick=""/></td>
                    <td class="tablewapper">Supervisor Approval - Signature</td>
                    <td class="tablewapper1"><input type="text" id="" name=""/>&nbsp;<input type="checkbox" name="" id=""/>N/A</td>
                </tr>
			</table>
		</form>
        </div>
	</div>
</div>
<form onsubmit="return avoidFormSubmitting()">
<div align="left" style="float:right;">
    <table width="100%" cellpadding="0" cellspacing="0" class="" align="" border="0">
        <tr>
        <td><span><label>Next:</label>
        <select name="nextOption" id="nextOption" class=""><option value="">Product Inventory</option><option value="logout.action">Logout</option></select>
        <input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
        <input type="button" value="Next" onclick=""/></span></td>
        </tr>
    </table>
</div>
</form>
<script>

$(function() {
 
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
 
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });
    $( "#tabs" ).tabs();
    $("select").uniform();
});
</script>