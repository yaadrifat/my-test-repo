<!-- <script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script> -->
<%@ include file="../../common/includes.jsp" %>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script type="text/javascript" src="js/placeholder.js"></script>
<script type="text/javascript" src="js/dataSearch.js"></script>
<script type="text/javascript" src="jsp/ctl/technicians/js/productTable.js"/></script>

<script language="javascript">
function savePages(){   
	 try{
	        if(saveInternalTransport()){
	            return true;
	        }
	    }catch(e){
	        alert("exception " + e);
	    }
}
$(document).ready(function(){
	$("#productDataTable").dataTable({
		 "sPaginationType": "full_numbers",
	        "aoColumns":[{"bSortable": false},
	                       null,
	                       null,
	                       null,
	                       null,
	                       null,
	                       null,
	                       null,
	                       null,
	                       null,
	                       null
	                       ],
	            "sDom": 'C<"clear">Rlfrtip',
	            "oColVis": {
	                "aiExclude": [ 0 ],
	                "buttonText": "&nbsp;",
	                "bRestore": true,
	                "sAlign": "left"
	            },
	            "fnInitComplete": function () {
	                $("#productDataTableColumn").html($('#productDataTable_wrapper .ColVis'));
	            }
	}); 
	$("#tempDepartSignOffTime").timepicker({});
    $("#tempArriveSignOffTime").timepicker({});
	$( "#departSignOffDate" ).datepicker({
								         dateFormat: 'M dd, yy',
								         changeMonth: true,
								         changeYear: true,
								         yearRange: "c-50:c+45"
	     });
	$( "#arriveSignOffDate" ).datepicker({
								         dateFormat: 'M dd, yy',
								         changeMonth: true,
								         changeYear: true,
								         yearRange: "c-50:c+45"
     	});

	$("select").uniform();
});
function getScanData(event,obj){
    if(event.keyCode==13){
      getdata(event);
  }
}
function getdata(e){
	constructProductList(true);
	loadInternalTransportData();
}
function loadInternalTransportData(){
	$("#productId").val($("#pkSpecimen").val());
	var productVal =  $("#productId").val();
	var url = "loadInternalTransportData";
	 if(productVal !=0 && productVal != ""){
	         var jsonData = "refProduct:"+productVal;
	        response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	    }
	 $(response.internalTransportList).each(function(i){
		$("#internalTransport").val(this.internalTransport);
		$("#productComingFrom").val(this.productComingFrom);
		$("#departSignOffDate").val(converDate(this.departSignOffDate));
		  if(this.departSignOffTime!=null){
          	var tmpDate = new Date(this.departSignOffTime);
              $("#tempDepartSignOffTime").val((tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes());
          }else{
        	  $("#tempDepartSignOffTime").val("");
          }
		$("#departTempHigh").val(this.departTempHigh);
		$("#departTempLow").val(this.departTempLow);
		$("#arriveTempHigh").val(this.arriveTempHigh);
		$("#arriveTempLow").val(this.arriveTempLow);
		$("#arriveSignOffDate").val(converDate(this.arriveSignOffDate));
		 if(this.arriveSignOffTime!=null){
			$("#tempArriveSignOffTime").val((tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes());
		 }else{
			$("#tempArriveSignOffTime").val("");
		 }
	 });
	 $.uniform.restore('select');
     $('select').uniform();
}
function setvalues(cBoxId,textId){
    var s=$('#'+cBoxId).attr('checked');
    if(s=='checked'){
        $('#'+textId).val("N/A");
    }else {
        $('#'+textId).val("");
    }
}
function setDepartSignOffTime(){
    $('#departSignOffTime').val($('#departSignOffDate').val() +" "+ $('#tempDepartSignOffTime').val());
}
function setArriveSignOffTime(){
    $('#arriveSignOffTime').val($('#arriveSignOffDate').val() +" "+ $('#tempArriveSignOffTime').val());
}
function saveInternalTransport(){
	  var url='saveInternalTransport';
	  var response = ajaxCall(url,"interTransportForm");
	  stafaAlert('Data saved Successfully','');
	  return true;
}   

</script>

<div class="cleaner"></div>

<form id="interTransportForm" action="#">
<input type='hidden' id="internalTransport" name="internalTransport" value="0"/>
<input type='hidden' id="productId" name="productId"/>
 <div class="column">
    <div class="portlet">
        <div class="portlet-header ">Product Table</div>
        	<div class="portlet-content">
        		<input type="hidden" name="entityType" id="entityType" value="PRODUCT" />
        		<input type="hidden" id="scanId" name="scanId"/>
        		<div id="button_wrapper">
                      <div id="divProductScan" style="margin-left:40%;">
                           <input class="scanboxSearch barcodeSearch multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;" onkeyup="performScanData(event,this)"/>
                        <input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" onfocus="changeType(this)"/>
                     </div>
             	</div>       
                <table cellpadding="0" cellspacing="0" border="1" id="productDataTable" class="display">
                    <thead>
                        <tr>
		                    <th width='4%' id='productDataTableColumn'></th>
		                    <th>Product ID</th>
		                    <th>Recipient Name</th>
		                    <th>Recipient MRN</th>
		                    <th>Recipient ABO/Rh</th>
		                    <th>Donor Name</th>
		                    <th>Donor MRN</th>
		                    <th>Donor ABO/Rh</th>
		                    <th>Donor Type</th>
		                    <th>Product Type</th>
		                    <th>Product Status</th>
                		</tr>
            	</thead>
        	</table>
    	</div>
	</div>
</div>

<div class="cleaner"></div>

<div class="column">
    <div  class="portlet">
    <div class="portlet-header">Product Transportation</div>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Product Coming From</td>
                    <td class="tablewapper1"><s:select id="productComingFrom"  name="productComingFrom" list="lstProductComingFrom" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
                    <td class="tablewapper"></td>
                    <td class="tablewapper1"></td>
                </tr>
			</table> 
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td style="font-weight:bold;padding:1%">Departure</td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Sign-Off Date</td>
                    <td class="tablewapper1"><input type="text" id="departSignOffDate" name='departSignOffDate' class="dateEntry"/></td>
                    <td class="tablewapper">Transport Temp. -High(C)</td>
                    <td class="tablewapper1"><input type="text" id="departTempHigh" name="departTempHigh" class=""></td>
                </tr>
                <tr>
                    <td class="tablewapper">Sign-Off Time</td>
                    <td class="tablewapper1"><input type='hidden' id="departSignOffTime" name="departSignOffTime" Class="" /><input type='text' id="tempDepartSignOffTime" name="temDepartSignOffTime" Class="" onchange="setDepartSignOffTime();"/></td>
                    <td class="tablewapper">Transport Temp. -Low(C)</td>
                    <td class="tablewapper1" style="width:22%"><input type="text" id="departTempLow" name="departTempLow" class="">&nbsp;<input type="checkbox" id="departTempNA" name="departTempNA"  onclick="setvalues('departTempNA','departTempLow')"/>N/A</td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td style="font-weight:bold;padding:1%">Arrival:CTL</td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Sign-Off Date</td>
                    <td class="tablewapper1"><input type="text" id="arriveSignOffDate" name='arriveSignOffDate' class="dateEntry"/></td>
                    <td class="tablewapper">Transport Temp. -High(C)</td>
                    <td class="tablewapper1"><input type="text" id="arriveTempHigh" name="arriveTempHigh" class=""></td>
                </tr>
                <tr>
                    <td class="tablewapper">Sign-Off Time</td>
                    <td class="tablewapper1"><input type='hidden'  id="arriveSignOffTime" name="arriveSignOffTime" Class="" /><input type="text" id="tempArriveSignOffTime" name="tempArriveSignOffTime" Class="" onchange="setArriveSignOffTime();"/></td>
                    <td class="tablewapper">Transport Temp. -Low(C)</td>
                    <td class="tablewapper1" style="width:22%"><input type="text" id="arriveTempLow" name="arriveTempLow" class="">&nbsp;<input type="checkbox" id="arriveTempNA" name="arriveTempNA"  onclick="setvalues('arriveTempNA','arriveTempLow')"/>N/A</td>
                </tr>
            </table>
        </div>
    </div>  
</div>
</form>
 
<div class="cleaner"></div>

<form onsubmit="return avoidFormSubmitting()">
<div align="left" style="float:right;">
    <table width="100%" cellpadding="0" cellspacing="0" class="" align="" border="0">
        <tr>
        <td><span><label>Next:</label>
        <select name="nextOption" id="nextOption" class=""><option value="">Product Inventory</option><option value="logout.action">Logout</option></select>
        <input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
        <input type="button" value="Next" onclick=""/></span></td>
        </tr>
    </table>
</div>
</form>
<script>

$(function() {
 
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
 
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

});
</script>