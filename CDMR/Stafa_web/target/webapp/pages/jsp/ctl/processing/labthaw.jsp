<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" >
$(document).ready(function(){
	//hide_slidecontrol();
	show_slidecontrol("slidecontrol");
	show_slidewidgets("nurse_slidewidget");
	var oTable1=$("#productDataTable").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null
											              
											             ],
											             "sScrollX": "100%",
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#productDataTableColumn").html($('.ColVis:eq(0)'));
												}
		});
		
$("select").uniform(); 

});



</script>

<div id="nurse_slidewidget" style="display:none;">

	<!-- Recipient Info start-->
				<div class="column">
					<div class="portlet">
						<div class="portlet-header "><s:text name="stafa.label.recipientinformation"/></div>
							<div class="portlet-content">
							<table width="100%" border="0">
							<tr>
								<td width="50%"><s:text name="stafa.label.recipientmrn"/></td>
								<td width="50%" id="recipient_id">HCA297889</td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.name"/></td>
								<td id="recipient_name">Anna Jackson</td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.dob"/></td>
								<td id="recipient_dob">Jun 23,1964</td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.aborh"/></td>
								<td id="recipient_aborh">O</td>
							  </tr>
							   <tr>
								<td>AbS:</td>
								<td id="recipient_aborh"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.diagnosis"/></td>
								<td id="recipient_diagnosis">Leukemia</td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.weights"/></td>
								<td id="recipient_weight">63 kg</td>
							  </tr>
							  </table>
							</div>
							 
					</div>
				</div>		
				<!--Recipient Info end-->
					
<!-- Donor Info start-->
			<div class="column">
				<div  class="portlet">
					<div class="portlet-header "><s:text name="stafa.label.donorinformation"/></div>
						<div class="portlet-content">
							<table width="100%" border="0">
							  <tr>
								<td width="50%"><s:text name="stafa.label.donorid"/></td>
								<td width="50%" id="slide_donorId" name="slide_donorId"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.name"/></td>
								<td id="slide_donorName" name="slide_donorName"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.dob"/></td>
								<td id="slide_donorDOB" name="slide_donorDOB"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.donoraborh"/></td>
								<td id="slide_donorABORH" name="slide_donorABORH"></td>
							  </tr>
							   <tr>
								<td>AbS:</td>
								<td id="" name=""></td>
							  </tr>
							
							   <tr>
								<td><s:text name="stafa.label.donation"/></td>
								<td id="slide_donation" name="slide_donation"></td>
							  </tr>
							   							  
							</table>
						</div>
				</div>
			</div>
				
<!-- Patient Info-->




				
				</div>


<div  id="slidecontrol" style="display:none;">
	<ul class="ulstyle">
	<span>
			<li class="listyle print_label"><img src = "images/icons/Bar_Code_32.png" class="barcodePrint" onclick="" style="width:30px;height:30px;"/></li>
			<li class="listyle print_label" ><b><s:text name="stafa.label.label"/></b> </li>
	</span>
	<span id="notesSpan">
		<li class="listyle notes_icon"  style="margin-left:18%"><img src = "images/icons/notes.png"  onclick="" style="width:30px;height:30px;"/><sup style="margin-left:2%"><span class='subspan'></span></sup></li>
		<li class="listyle notes_icon"><b><s:text name="stafa.label.notes"/></b> </li>
	</span>
	<span>
		<li class="listyle print_label"><img src = "images/icons/labresults.png"  style="width:30px;height:30px;cursor:pointer;" onclick='addNewPopup("Lab Result","modalpopup","loadLabresult","600","1050");'/></li>
		<li class="listyle print_label" ><b><s:text name="stafa.label.labresult"/></b> </li>
	</span>
	<span>
		<li class="listyle medication_icon"><img src = "images/icons/soplib.png" onclick='addNewPopup("SOP Library","modalpopup","loadSOPModal","600","1050");' style="width:30px;height:30px;"/></li>
		<li class="listyle medication_icon"><b>SOP</b></li>
	</span>
	<span>
	<span>
		<li class="listyle terminate_icon"><img src = "images/icons/billchecklist.png"  style="width:30px;height:30px;"/></li>
		<li class="listyle terminate_icon"><b>Billing CheckList</b></li>
	</span>
		<li class="listyle transfer_icon"><img src = "images/icons/transfer.png" onclick=""  style="width:30px;height:30px;"/></li>
		<li class="listyle transfer_icon" id="transfer_icon"><b><s:text name="stafa.label.transfer"/></b></li>
	</span>
	</ul>
</div>



<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Product Table</div>
		<div class="portlet-content">
		<div id="button_wrapper">
		<form>
			        		<div id="divProductScan" style="margin-left:40%;">
			       				 <input class="scanboxSearch barcodeSearch multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Recipient ID"  style="width:210px;" onkeyup="setScanFlag('RECIPIENT');performScanData(event,this)" />
								<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Recipient ID"  style="width:210px;display:none;" onfocus="changeType(this)" />
					 		</div>
					 		</form>
				        </div>
			<div id="producDataTableDiv"
				style="overflow-x: auto; overflow-y: hidden;">
				<table cellpadding="0" cellspacing="0" border="1"
					id="productDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='productDataTableColumn'>Check All</th>
							<th>Product ID</th>
							<th>Recipient Name</th>
							<th>Recipient MRN</th>
							<th>Donor Name</th>
							<th>Donor MRN</th>
							<th>Volume(mL)</th>
							<th>Product Type</th>
							<th>Product Status</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><input type="checkbox" id="" name="" /></td>
							<td>BMT12137</td>
							<td>Allen Johnson</td>
							<td>HCA34587612</td>
							<td>Edward Wallace</td>
							<td>HCA56734220</td>
							<td>521</td>
							<td>HPC-M</td>
							<td>Pending</td>
						</tr>
						
						</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Pre-Thaw Information</div>
		<div class="portlet-content">
		<form>
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Product Volume</td>
					<td class="tablewapper1"><input  type="text" id="" name=""/></td>
					<td class="tablewapper">Total Cells/kg(*10^8/kg)</td>
					<td class="tablewapper1"><input  type="text" id="" name=""/></td>
					
				</tr>
				<tr>
					<td class="tablewapper">Total Cells(*10^10)</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">Total CD34 + Cells </td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
                </tr>
            
				</table>
		</form>
		</div>
	</div>

</div>

<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Thaw Station Setup</div>
		<div class="portlet-content">
		<form>
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Thawing Station</td>
					<td class="tablewapper1"><select><option>Select</option></select></td>
					<td class="tablewapper">Thawing Method</td>
					<td class="tablewapper1"><select><option>Select</option></select></td>
					
				</tr>
				<tr>
					<td class="tablewapper">Station Pre-Cleaned</td>
					<td class="tablewapper1"><input type="radio" id="" name=""/>Yes<input type="radio" id="" name=""/>No</td>
					
                </tr>
            
				</table>
		</form>
		</div>
	</div>

</div>
<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Thaw Preparation</div>
		<div class="portlet-content">
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td colspan="3" class="" style="padding:0% 0% 0% 8%">Thawing Station</td>
					<td><input type="radio" id="" name=""/>Yes<input type="radio" id="" name=""/>No</td>
					
					
				</tr>
				<tr>
					<td colspan="3" class="" style="padding:0% 0% 0% 8%">Infustion Bag(5-10 mL Wash Solution in Labeled Transfer Bag)Prepared</td>
					<td><input type="radio" id="" name=""/>Yes<input type="radio" id="" name=""/>No</td>
					
					
				</tr>
				<tr>
					<td colspan="3" class="" style="padding:0% 0% 0% 8%">Sterility-Connected Bags Together,per SOP,and Placed this Bag Set-up in 2-8C Refrigerator</td>
					<td><input type="radio" id="" name=""/>Yes<input type="radio" id="" name=""/>No</td>
					
					
				</tr>
				</table>
		
		</div>
	</div>
</div>




<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Thaw Information</div>
		<div class="portlet-content">
		<form>
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Water Bath#</td>
					<td class="tablewapper1"><select><option>Select</option></select></td>
					<td class="tablewapper">Thawing Start Time</td>
					<td class="tablewapper1"><input  type="text" id="" name=""/></td>
					
				</tr>
				<tr>
					<td class="tablewapper">Water Bath Temp.(C)</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">WBC Count(*10^6/mL) </td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
                </tr>
                <tr>
					<td class="tablewapper">Thawing Date</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">ACD Added</td>
					<td class="tablewapper1"><input type="radio" id="" name="acd"/>Yes<input type="radio" id="" name="acd"/>No<input type="radio" id="" name="acd"/>N/A</td>
                </tr>
                <tr>
                <td colspan="3" class="tablewapper">Verified Product,Placed in Zip Bag,and Thaw Product in Water Bath</td>
                <td><input type="radio" id="" name="acd"/>Yes<input type="radio" id="" name="acd"/>No</td>
                </tr>
                 <tr>
                <td colspan="3" class="tablewapper">CBU Remained intact</td>
                <td><input type="radio" id="" name="acd"/>Yes<input type="radio" id="" name="acd"/>No</td>
                </tr>
                 <tr>
                <td colspan="3" class="tablewapper">Transferred Product Contents into infustion Bag and Rinsed the Cryobag with Wash Solution</td>
                <td><input type="radio" id="" name="acd"/>Yes<input type="radio" id="" name="acd"/>No</td>
                </tr>
                 <tr>
                <td colspan="3" class="tablewapper">Disconnected Infusion Bag For Sample</td>
                <td><input type="radio" id="" name="acd"/>Yes<input type="radio" id="" name="acd"/>No</td>
                </tr>
                 <tr>
					<td class="tablewapper">Thawing End Time</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">Water Bath Cleaned</td>
					<td class="tablewapper1"><input type="radio" id="" name="acd"/>Yes<input type="radio" id="" name="acd"/>No<input type="radio" id="" name="acd"/>N/A</td>
                </tr>
             </form>   
            
				</table>
		
		</div>
	</div>

</div>


<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Lab Test <input type="radio" id="" class="" />Yes <input type="radio" id="" class="" />No </div>
		<div class="portlet-content">
		<div class="portlet-content">
					
				<div id="labtabs">
				    <ul >
				     <li><a href="#tabs-1">BMT1217</a></li>
				     
				    </ul>
			  <div id="tabs-1">   
					<form>
			<table cellpadding="0" cellspacing="0" class="" align="left" border="0" width="100%">
				
			
                <tr>
					<td class="" width="12%" style="padding:1% 1% 1% 1%"><input type="checkbox" id="" name=""/>CFU</td>
					<td class="" width="18%" style="padding:1% 1% 1% 1%">Plated By<input type="text" id="" name=" " style="width:90%"/></td>
					<td class="" width="18%" style="padding:1% 1% 1% 1%">Date<input type="text" id="" name="" style="width:90%" class="dateEntry"/></td>
					<td class="" width="12%" style="padding:1% 1% 1% 1%"><input type="checkbox" id="" name="" />TNCCB</td>
					<td class="" width="18%" style="padding:1% 1% 1% 1%">Ref#.<input type="text" id="" name="" style="width:90%"/></td>
					<td class="" width="12%" style="padding:1% 1% 1% 1%"><input type="checkbox" id="" name=""/>CD34</td>
					<td class="" width="18%" style="padding:1% 1% 1% 1%">Ref#.<input type="text" id="" name="" style="width:90%"/></td>
                </tr>
                 <tr>
					<td class="" style="padding:1% 1% 1% 1%"><input type="checkbox" id="" name=""/>Sterility:PCB</td>
					<td class=""style="padding:1% 1% 1% 1%"><input type="checkbox" id="" name=""/>RFLP Card</td>
					<td class="" style="padding:1% 1% 1% 1%"><input type="checkbox" id="" name=""/>Post-Thaw Viability</td>
					
					
                </tr>
                <tr>
				<td><img onclick="" style="width:16;height:16;cursor:pointer;" src="images/icons/addnew.jpg">&nbsp;&nbsp;Add Lab Test</td>
				<td class=""><select id="" name=""><option>Select</option></select></td>
				</tr>
                
               	</table>
				</form>
				</div>	
			
			</div>
				
			</div>
		
		
		</div>
	</div>

</div>

<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Post-Thaw Information</div>
		<div class="portlet-content">
		<form>
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Product Volume(mL)</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">Total Cells/Kg(*10^8/kg)</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					
				</tr>
				<tr>
					<td class="tablewapper">WBC Count(*10^6)</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">TNC % Recovery</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					
				</tr>
					<tr>
					<td class="tablewapper">Total Cells(*10^10)</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">% Viability</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					
				</tr>
				</table>
		</form>
		</div>
	</div>

</div>

<div class="column">
	<div class="portlet">

		<div class="portlet-header ">End-Point Check</div>
		<div class="portlet-content">
			<form>
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Station Post-Cleaned</td>
					<td class="tablewapper1"><input type="radio" id="" name=""/>Yes<input type="radio" id="" name=""/>No</td>
					<td class="tablewapper">Product Appearence OK</td>
					<td class="tablewapper1"><input type="radio" id="" name=""/>Yes<input type="radio" id="" name=""/>No</td>
					
				</tr>
				<tr>
					<td class="tablewapper">Product Container Intact</td>
					<td class="tablewapper1"><input type="radio" id="" name=""/>Yes<input type="radio" id="" name=""/>No</td>
					<td class="tablewapper">Label Verification</td>
					<td class="tablewapper1"><input type="radio" id="" name=""/>Yes<input type="radio" id="" name=""/>No<a href="#">View</a></td>
                </tr>
            
				</table>
				</form>
		</div>
	</div>

</div>
<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Reagents & Supplies</div>
		<div class="portlet-content"></div>
	</div>

</div>
<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Equiment</div>
		<div class="portlet-content"></div>
	</div>

</div>
<div class="cleaner"></div>
		<div align="left" style="float:right;">
		<form onsubmit="return avoidFormSubmitting()">
				<table cellpadding="0" cellspacing="0" class="" align="" border="0">
				<tr>
				<td><span>Next:
				<select name="nextOption" id="nextOption" class=""></select>
				<input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
				<input type="button" value="Next" onclick=""/></span></td>
				</tr>
				</table>
		</form>
		</div>

<script>
	$(function() {
		$("#tabs").tabs();
		$("#labtabs").tabs();
		$(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all").find(".portlet-header").addClass(
						"ui-widget-header ui-corner-all").prepend(
						"<span class='ui-icon ui-icon-minusthick'></span>");

		/**For add new popup**/

		$(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all").find(".addnew")
				.append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

		$(".portlet-header .ui-addnew").click(function() {

		});

		/**For Refresh**/

		$(".portlet-header .ui-icon").click(function() {
					$(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
					$(this).parents(".portlet:first").find(".portlet-content").toggle();
				});

	});
</script>

