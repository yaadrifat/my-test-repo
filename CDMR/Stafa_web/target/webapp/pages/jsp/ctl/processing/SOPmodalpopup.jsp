
<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" >
$(document).ready(function(){

	constructSOPLibraryModal(false);
		
$("select").uniform(); 

});
function constructSOPLibraryModal(flag){
	//alert("construct ProductList");
	var sopmodalTable_serverParam = function ( aoData ) {
																aoData.push( { "name": "application", "value": "stafa"});
																aoData.push( { "name": "module", "value": "Ctl_modalSOP_SOPLibrary"});	
																
																aoData.push( {"name": "col_1_name", "value": "nvl(s.SOP_NAME, ' ' )" } );
																aoData.push( { "name": "col_1_column", "value": "1"});
																
																aoData.push( { "name": "col_2_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst=s.FK_CODELST_CATEGORY),' '))"});
																aoData.push( { "name": "col_2_column", "value": "2"});
																
																aoData.push( { "name": "col_3_name", "value": "nvl(s.SOP_LINK,' ')"});
																aoData.push( { "name": "col_3_column", "value": "3"});
																
																/* aoData.push( { "name": "col_4_name", "value": "lower(nvl(s.SOP_ACTIVATE,0))"});
																aoData.push( { "name": "col_4_column", "value": "4"}); */
																
													};
	var sopmodalTable_aoColumnDef = [
			                        
			                          {    "sTitle":'SOP Name',
			                     	             "aTargets": [ 0], "mDataProp": function ( source, type, val ) {
			                             			 return source[1];
			                             }},
			                          {    "sTitle":'Category',
			                     	              "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
					                             	 return source[2];
			                             }},
			                          {    "sTitle":'Link',
			                                      "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
			                                    	  link="<a href='http://"+source[4]+"' target='_blank' >"+source[4]+"</a>";
			   			                             return link;
			                             }}/* ,
			                          {    "sTitle":'Activated',
				                                  "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
				                                	//  alert(source[5]);
				                                	  if (source[5] == 1) {
				              							isActivate = "<input type='radio' name='sopActivate"+source[0]+"' id='sopActivate' value='Yes' checked='checked' >Yes</input>";
				              							isActivate += "<input type='radio' name='sopActivate"+source[0]+"' id='sopActivate' value='No' >No</input>";
				              						} else {
				              							isActivate = "<input type='radio' name='sopActivate"+source[0]+"' id='sopActivate' value='Yes'  >Yes</input>";
				              							isActivate += "<input type='radio' name='sopActivate"+source[0]+"' id='sopActivate' value='No' checked='checked'>No</input>";
				              						}
				              						return isActivate;
							                       //  return source[5];
				                         }} */
				                      
			                        ];
	var sopmodalTable_aoColumn = [ 
	                               	   null,
						               null,
						               null
						              
						               
			             ];
	/* var sopmodalTable_ColManager = function(){
										$("#sopModalColumn").html($('#sopmodalTable_wrapper .ColVis'));
									 }; */
									// constructTable(flag,'sopmodalTable',sopmodalTable_ColManager,sopmodalTable_serverParam,sopmodalTable_aoColumnDef,sopmodalTable_aoColumn);
									 constructTableWithoutColManager(flag,'sopmodalTable',sopmodalTable_serverParam,sopmodalTable_aoColumnDef,sopmodalTable_aoColumn)
}

function sopnewwindow(link){
	
	  window.open(link);
	  return false;	
}

</script>


<div id="sopmodal" title="addNew">

  	<form>
  	
  	<div  class="portlet" id="soptabblediv" >
	<div class="portlet-header ">SOP Library</div>
  	<div class="portlet-content" style="overflow-x:auto;overflow-y:hidden;">
  		
		<table cellpadding="0" cellspacing="0" border="0" id="sopmodalTable" class="display">
	 		<thead>
	 			<tr>
	 				<th id="sopModalColumn"> SOP Name</th>
	 				<th>Category</th>
	 				<th>Link</th>
	 				
	 			</tr>
	 		</thead>
	 		<tbody>
	 		
	        </tbody>
		</table>
		</div>
		</div>
<div>&nbsp;</div>
<div class="cleaner"></div>
<div>&nbsp;</div>

  	</form>
  	<!-- <div align="right" style="float:right;">
		<form onsubmit="return avoidFormSubmitting()">
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					
					
					<td><input type="button" value="Cancel" onclick="closeMedication()"/></td>
				</tr>
			</table>
		</form>
	</div>   -->
</div>


<script>
$(function() {
  

    $( ".ui-dialog .portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
   
   

    /**For add new popup**/
   
    $( ".ui-dialog .portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
    .append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

    $( ".ui-dialog .portlet-header .ui-addnew" ).click(function() {
    	
    });
   
    /**For Refresh**/
  

	
    $( ".ui-dialog .portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

    
});


</script>