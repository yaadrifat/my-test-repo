<%@ include file="../../common/includes.jsp" %>
<script>
$(function(){
$("#labtbs").tabs();
	
	
});



</script>



<div id="labtbs">
	<ul id="">
		 <li><a href="#maindiv">BMT1217/1</a></li>
		 <li><a href="#maindiv">BMT1217-Plasma</a></li>
	</ul>


	<div id="maindiv" class="">
		<fieldset style="width: 90%; border-radius: 5px">
			<legend style="color: blue"><b>Type Check</b></legend>
			<br><br>
			<table width="100%" class="ctltables">
				<tr>
					<th>Donor Result</th>
					<th>Product Result</th>
				</tr>
				<tbody>
					<tr>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</fieldset>
		<br><br>
		<fieldset style="width: 90%; border-radius: 5px">
			<legend style="color: blue"><b>Microbail</b></legend>
			<br><br>
			<table width="100%" class="ctltables">
				<tr>
					<th>Results</th>
					<th># of Days</th>
					<th>Sensitivity(if positive)</th>
					<th>Organism Identified(if positive)</th>
				</tr>
				<tbody>
					<tr>
						<td></td>
						<td></td>
						<td><input type="text" id="" name=""/></td>
						<td><select><option>select</option></select></td>
					</tr>
				</tbody>
			</table>
		</fieldset>
		<br><br>
		<fieldset style="width: 90%; border-radius: 5px">
			<legend style="color: blue"><b>Processing Lab</b></legend>
			<br><br>
			<b>General Lab Results</b>
			<table width="100%" class="ctltables">
				<tr>
					<th>Test Stage</th>
					<th>Product Weight(g)</th>
					<th>Product Volume(mL)</th>
					<th>RBC(*10^3/mL)</th>
					<th>HCT(%)</th>
					<th>PLT(*10^6/mL)</th>
				</tr>
				<tbody>
					<tr>
						<td>Pre-Processing</td>
						<td>324</td>
						<td>301</td>
						<td>0.40</td>
						<td>3.1</td>
						<td>1986</td>
					</tr>
				</tbody>
			</table>
			<br><br>
			<b>Differential</b>
			<table width="100%" class="ctltables">
				<tr>
					<th>Test Stage</th>
					<th>Cell Counted(# Cells)</th>
					<th>Segs(%)</th>
					<th>Band(%)</th>
					<th>Lymphocyte</th>
					<th>Monocyte</th>
					<th>Meta(%)</th>
					<th>Myelocyte</th>
				</tr>
				<tbody>
					<tr>
						<td>Pre-Processing</td>
						<td>100</td>
						<td>3</td>
						<td>5</td>
						<td>27</td>
						<td>60</td>
						<td>2</td>
						<td>3</td>
					</tr>
				</tbody>
			</table>
			<br><br>
			<b>Cell Count</b>
			<table width="100%" class="ctltables">
				<tr>
					<th>Test Stage</th>
					<th>WBC(*10^6/mL)</th>
					<th>Total Cells(*10^10)</th>
					<th>Total Cells/kg(*10^8/kg)</th>
					<th>Total Cells/kg/Bag(*10^8)</th>
				</tr>
				<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<br><br>
				<b>Manual Differential</b>
			<table width="100%" class="ctltables">
				<tr>
					<th>Test Stage</th>
					<th>Total MNC(*10^10)</th>
					<th>MNC/kg(*10^8/kg)</th>
					<th>MNC/kg/Bag(*10^8)</th>
				</tr>
				<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<br><br>
			<b>Manual Differential</b>
			<table width="100%" class="ctltables">
				<tr>
					<th>Test Stage</th>
					<th>Total MNC(*10^10)</th>
					<th>MNC/kg(*10^8/kg)</th>
					<th>MNC/kg/Bag(*10^8)</th>
				</tr>
				<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<br><br>
			<b>Flow Cytometry-CD34</b>
			<table width="100%" class="ctltables">
				<tr>
					<th>Test Stage</th>
					<th>Abs.CD34</th>
					<th>Total CD3(*10^7)</th>
					<th>CD34/kg(*10^6/kg)</th>
					<th>CD34/kg/Bag(*10^6)</th>
				</tr>
				<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<br><br>
			<b>Flow Cytometry-CD3</b>
			<table width="100%" class="ctltables">
				<tr>
					<th>Test Stage</th>
					<th>Abs.CD3(cells/uL)</th>
					<th>Total CD3(*10^7)</th>
					<th>CD3/kg(*10^6/kg)</th>
					<th>CD3/kg/Bag(*10^6)</th>
					<th>CD3 %</th>
				</tr>
				<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<br><br>
			<b>CFU Plate</b>
			<table width="100%" class="ctltables">
				<tr>
					<th>Test Stage</th>
					<th>BFU-E</th>
					<th>CFU-GM</th>
					<th>CFU-Mix</th>
					<th>Read Date</th>
				</tr>
				<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>

					</tr>
				</tbody>
			</table>
			<br><br>
			<b>CTL QC</b>
			<table width="100%" class="ctltables">
				<tr>
					<th>Test Stage</th>
					<th>%Viability</th>
					<th>TNC % Recovery</th>
					
				</tr>
				<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
					

					</tr>
				</tbody>
			</table>
						
		</fieldset>
		<br><br>
		<fieldset style="width: 90%; border-radius: 5px">
			<legend style="color: blue"><b>Cumulative</b></legend>
			<br><br>
			<table width="100%" class="ctltables">
				<tr>
					<th>Cumul.Total Cells/kg(*10^8/kg)</th>
					<th>Cumul.MNC/kg(*10^8/kg)</th>
					<th>Cumul.CD34/kg(*10^6/kg)</th>
					<th>Total Volume(mL)</th>
					<th># of Bags</th>
				</tr>
				<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</fieldset>
<br><br>
	</div>
</div>

<div align="right" style="float:right;">
		<form onsubmit="return avoidFormSubmitting()">
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td><input type="password" style="width:80px;" size="5" value="" id="" name="" placeholder="e-Sign"/>&nbsp;&nbsp;				
					<input type="button" value="Save" onclick=""/>
					<input type="button" value="Cancel" onclick=""/></td>
				</tr>
			</table>
		</form>
	</div>  
