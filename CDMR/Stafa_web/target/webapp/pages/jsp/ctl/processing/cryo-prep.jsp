<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" >
$(document).ready(function(){
	//hide_slidecontrol();
show_slidecontrol("slidecontrol");
	show_slidewidgets("nurse_slidewidget");
	$("#freezesolution").validationEngine();
	var oTable1=$("#productDataTable").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null
											              
											             ],
											             "sScrollX": "100%",
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#productDataTableColumn").html($('.ColVis:eq(0)'));
												}
		});
	
	var oTable1=$("#splitprodDataTable").dataTable({
		"sPaginationType": "full_numbers",
		"sDom": 'C<"clear">Rlfrtip',
		"aoColumns": [ { "bSortable":false},
		               null,
		               null,
		               null,
		               null,
		               null,
		               null,
		               null,
		               null
		         
		              
		             ],
		             "sScrollX": "100%",
			"oColVis": {
				"aiExclude": [ 0 ],
				"buttonText": "&nbsp;",
				"bRestore": true,
				"sAlign": "left"
			},
			"fnInitComplete": function () {
		        $("#splitproDataTableColumn").html($('.ColVis:eq(1)'));
			}
});


		
$("select").uniform(); 

});


function addtxt(){/* $('#deleteallchild').attr('checked', false); */

$("#addtext").html('# of Components &nbsp;<input type=text  id=norow style="width:33%;" onkeypress="createcols(event);"/>');
$("#addtext").show();
	}
function createcols(e){	
	 if(e.keyCode==13){
		// $.uniform.restore('select');	
		Createsplitrows();			
		return false;
		}
	}	
function Createsplitrows(){
	var i=0; 
	var n=$("#norow").val();
	var dynrow=$('#splitprodDataTable').find('tr').length;
	if(dynrow!=1){
		dynrow++;
	} 
	
	for(i;i<n;i++){	
		$("#splitprodDataTable tbody").prepend("<tr>"+
				"<td><input type='checkbox' id=''/></td>"+
				"<td><select><option>select</option><option>Parent Bag</option></select></td>"+
				"<td><select><option>select</option><option>600mL</option></select></td>"+
				"<td><select><option>select</option><option>N/A</option></select></td>"+
				"<td><input type='text' id='' name=''></td>"+
				"<td><input type='text' id='' name=''></td>"+
				"<td><input type='text' id='' name=''></td>"+
				"<td><select><option>Select</option><option>Buffy Coat Prep</option></select></td>"+
				"<td><input type='text' id='' name=''></td>"+
				"</tr>");

	
	}
	$("select").uniform(); 
	nchildrens();
}	
function nchildrens(){
	 dynrows=$('#splitprodDataTable').find('tr').length;
	// alert(dynrows);
	 var len=$("#splitprodDataTable").find("tbody").find("tr").find("td").text();
	 if(len=='No data available in table')
		 {dynrows=0;
		 $("#nchild").html('<b>Total No of Components</b> &nbsp;'+'<b>'+dynrows+'</b>');
		 }
	  if(dynrows>=2)
		 {dynrows=dynrows-2;
		  //alert("t1"+dynrows)
		  $("#nchild").html('<b>Total No of Components</b> &nbsp;'+'<b>'+dynrows+'</b>');
		 } else {
	 $("#nchild").html('<b>Total No of Components</b> &nbsp;'+'<b>'+dynrows+'</b>');
		 }
}

</script>


<div id="nurse_slidewidget" style="display:none;">

	<!-- Recipient Info start-->
				<div class="column">
					<div class="portlet">
						<div class="portlet-header "><s:text name="stafa.label.recipientinformation"/></div>
							<div class="portlet-content">
							<table width="100%" border="0">
							<tr>
								<td width="50%"><s:text name="stafa.label.recipientmrn"/></td>
								<td width="50%" id="recipient_id">HCA297889</td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.name"/></td>
								<td id="recipient_name">Anna Jackson</td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.dob"/></td>
								<td id="recipient_dob">Jun 23,1964</td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.aborh"/></td>
								<td id="recipient_aborh">O</td>
							  </tr>
							   <tr>
								<td>AbS:</td>
								<td id="recipient_aborh"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.diagnosis"/></td>
								<td id="recipient_diagnosis">Leukemia</td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.weights"/></td>
								<td id="recipient_weight">63 kg</td>
							  </tr>
							  </table>
							</div>
							 
					</div>
				</div>		
				<!--Recipient Info end-->
					
<!-- Donor Info start-->
			<div class="column">
				<div  class="portlet">
					<div class="portlet-header "><s:text name="stafa.label.donorinformation"/></div>
						<div class="portlet-content">
							<table width="100%" border="0">
							  <tr>
								<td width="50%"><s:text name="stafa.label.donorid"/></td>
								<td width="50%" id="slide_donorId" name="slide_donorId"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.name"/></td>
								<td id="slide_donorName" name="slide_donorName"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.dob"/></td>
								<td id="slide_donorDOB" name="slide_donorDOB"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.donoraborh"/></td>
								<td id="slide_donorABORH" name="slide_donorABORH"></td>
							  </tr>
							   <tr>
								<td>AbS:</td>
								<td id="" name=""></td>
							  </tr>
							
							   <tr>
								<td><s:text name="stafa.label.donation"/></td>
								<td id="slide_donation" name="slide_donation"></td>
							  </tr>
							   							  
							</table>
						</div>
				</div>
			</div>
				
<!-- Patient Info-->




				
				</div>
<div  id="slidecontrol" style="display:none;">
	<ul class="ulstyle">
	<span>
			<li class="listyle print_label"><img src = "images/icons/Bar_Code_32.png" class="barcodePrint" onclick="" style="width:30px;height:30px;"/></li>
			<li class="listyle print_label" ><b><s:text name="stafa.label.label"/></b> </li>
	</span>
	<span id="notesSpan">
		<li class="listyle notes_icon"  style="margin-left:18%"><img src = "images/icons/notes.png"  onclick="" style="width:30px;height:30px;"/><sup style="margin-left:2%"><span class='subspan'></span></sup></li>
		<li class="listyle notes_icon"><b><s:text name="stafa.label.notes"/></b> </li>
	</span>
	<span>
		<li class="listyle print_label"><img src = "images/icons/labresults.png"  style="width:30px;height:30px;cursor:pointer;" onclick='addNewPopup("Lab Result","modalpopup","loadLabresult","600","1050");'/></li>
		<li class="listyle print_label" ><b><s:text name="stafa.label.labresult"/></b> </li>
	</span>
	<span>
		<li class="listyle medication_icon"><img src = "images/icons/soplib.png" onclick='addNewPopup("SOP Library","modalpopup","loadSOPModal","600","1050");' style="width:30px;height:30px;"/></li>
		<li class="listyle medication_icon"><b>SOP</b></li>
	</span>
	<span>
	<span>
		<li class="listyle terminate_icon"><img src = "images/icons/billchecklist.png"  style="width:30px;height:30px;"/></li>
		<li class="listyle terminate_icon"><b>Billing CheckList</b></li>
	</span>
		<li class="listyle transfer_icon"><img src = "images/icons/transfer.png" onclick=""  style="width:30px;height:30px;"/></li>
		<li class="listyle transfer_icon" id="transfer_icon"><b><s:text name="stafa.label.transfer"/></b></li>
	</span>
	</ul>
</div>



<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Product Table</div>
		<div class="portlet-content">
		<div id="button_wrapper">
			        		<div id="divProductScan" style="margin-left:40%;">
			        		<form>
			       				 <input class="scanboxSearch barcodeSearch multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Recipient ID"  style="width:210px;" onkeyup="setScanFlag('RECIPIENT');performScanData(event,this)" />
								<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Recipient ID"  style="width:210px;display:none;" onfocus="changeType(this)" />
								</form>
					 		</div>
				        </div>
			<div id="producDataTableDiv"
				style="overflow-x: auto; overflow-y: hidden;">
				<table cellpadding="0" cellspacing="0" border="1"
					id="productDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='productDataTableColumn'>Check All</th>
							<th>Product ID</th>
							<th>Recipient Name</th>
							<th>Recipient MRN</th>
							<th>Donor Name</th>
							<th>Donor MRN</th>
							<th>Volume(mL)</th>
							<th>Product Type</th>
							<th>Product Status</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><input type="checkbox" id="" name="" /></td>
							<td>BMT12137/1</td>
							<td>Allen Johnson</td>
							<td>HCA34587612</td>
							<td>Edward Wallace</td>
							<td>HCA56734220</td>
							<td>521</td>
							<td>HPC-M</td>
							<td>Processed</td>
						</tr>
						<tr>
							<td><input type="checkbox" id="" name="" /></td>
							<td>BMT12137/2</td>
							<td>Allen Johnson</td>
							<td>HCA34587612</td>
							<td>Edward Wallace</td>
							<td>HCA56734220</td>
							<td>521</td>
							<td>HPC-M</td>
							<td>Processed</td>
						</tr>
						</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Freeze Solution</div>
		<div class="portlet-content">
		<form id="freezesolution">
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Saline Volume</td>
					<td class="tablewapper1"><input type="text" id="salinevolume" class="validate[required] text-input"/> </td>
					<td class="tablewapper">DMSO Volume(mL)</td>
					<td class="tablewapper1"><input type="text" id="dmosvolume" class="validate[required] text-input"/> </td>
					
				</tr>
				<tr>
					<td colspan= "2" class="tablewapper">Freeze Solution Placed in Fridge For At Least 30mins</td>
					<td class="tablewapper1"><input type="radio" id="" name=""/>Yes<input type="radio" id="" name=""/>No</td>
					
                </tr>
            
				</table>
		</form>
		</div>
	</div>

</div>

<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Cryo Calculation</div>
		<div class="portlet-content">
		<form>
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				
                <tr>
					<td class="tablewapper">Product Volume(mL)</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">Cryo Bag Volume </td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
                </tr>
                <tr>
					<td class="tablewapper">Freeze Solution Volume(mL)</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">No. of Bags</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
                </tr>
                 <tr>
					<td class="tablewapper">Total Cryo Volume(mL)</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">No of Cryovial Samples</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
                </tr>
              
            
				</table>
		</form>
		</div>
	</div>

</div>
<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Split Product <input type="radio" id="splityes" class="maxportlet" name="splitwidget" />Yes <input type="radio" id="" class="minportlet"   name="splitwidget"/>No </div>
		<div class="portlet-content">
		<table><tr>
			<td><img src = "images/icons/addnew.jpg" style="width:16;height:16;cursor:pointer;" onclick="addtxt();"/>&nbsp;&nbsp;<label id=""><b>Add Child</b></label></td>
			<td><div id="addtext" style="width:180px;"> </div></td>
			<td width="27%" height="40px">&nbsp;&nbsp;&nbsp;<div><img src = "images/icons/no.png" style="width:16;height:16;cursor:pointer;" id ="" onclick="deletechildrows('');"/>&nbsp;&nbsp;<label id=""><b>Delete Child</b></label></div></td>
			<td width="25%" height="40px"><div id="nchild" align="right"></div></td>
			</tr></table>
			<div id="splitprodDataTableDiv"
				style="overflow-x: auto; overflow-y: hidden;">
				<table cellpadding="0" cellspacing="0" border="1"
					id="splitprodDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='splitproDataTableColumn'>Check All</th>
							<th>Component</th>
							<th>Bag</th>
							<th>Component ID</th>
							<th>Weight(g)</th>
							<th>Volume(mL)</th>
							<th>CD34(*10^6)</th>
							<th>Intended Process</th>
							<th>Comments</th>
						</tr>
					</thead>
					<tbody>
						<form>
						
						</form>
						
						</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Lab Test <input type="radio" id="" name="labtestwidget" class="maxportlet" />Yes <input type="radio" id="" name="labtestwidget" class="minportlet" />No </div>
		<div class="portlet-content">
				<div id="labtabs">
				    <ul >
				     <li><a href="#tabs-1">BMT1217</a></li>
				     
				    </ul>
			  <div id="tabs-1">   
					<form>
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				
			
                <tr>
					<td class=""><input type="checkbox" id="" name=""/>Plasma SCPPP</td>
					<td class="">Ref#.<input type="text" id="" name=""/></td>
					<td class=""><input type="checkbox" id="" name=""/>Sterility STPT</td>
					<td class=""><input type="checkbox" id="" name=""/>Post-Processing Viability</td>
                </tr>
                <tr>
				<td><img onclick="" style="width:16;height:16;cursor:pointer;" src="images/icons/addnew.jpg">&nbsp;&nbsp;Add Lab Test</td>
				<td class=""><select id="" name=""><option>Select</option></select></td>
				</tr>
                
               	</table>
				</form>
				</div>	
			
			</div>
				
			</div>
	
	</div>

</div>

<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Product Inspection</div>
		<div class="portlet-content">
		<form>
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td colspan="" class="tablewapper">Product Container Intact</td>
					<td class="tablewapper1"><input type="radio" id="" name=""/>Yes<input type="radio" id="" name=""/>No<input type="radio" id="" name=""/>N/A</td>
					<td class="tablewapper">Label Verification</td>
					<td class="tablewapper1"><input type="radio" id="cryolabelverification_yes" name="cryolabelverification" value="1"/>Yes<input type="radio" id="cryolabelverification_no" name="cryolabelverification" value="0"/>No &nbsp;<a href="#" id="cryolabelview" style="color:blue;">View</a></td>
					
				</tr>
				<tr>
					<td colspan="" class="tablewapper">Product Appearance OK</td>
					<td class="tablewapper1"><input type="radio" id="" name=""/>Yes<input type="radio" id="" name=""/>No<input type="radio" id="" name=""/>N/A</td>
					<td class="tablewapper">CTL Technician 2 Signature</td>
					<td class="tablewapper1"><input type="text" id name=""/><input type="radio" id="" name=""/>N/A</td>
					
				</tr>
			
              </table>  
		</form>
		</div>
	</div>

</div>


<div class="column">		
		<div  class="portlet">
			<div class="portlet-header">CRF Freezer Curve</div><br>
		<div class="portlet-content">
		<form>
				<div id="table_inner_wrapper">
					<div id="left_table_wrapper">
						<div id="table_container_input">
							
							<div class="table_row_input1">
								<div class="table_cell_input1">CRF Serial #</div>
								<div class="table_cell_input2"><div class="mainselection"><select name="" id=""><option>abc123</option></select></div>
								</div>
							</div>	
							<div class="table_row_input1">
								<div class="table_cell_input1">CRF Program</div>
								<div class="table_cell_input2"><div class="mainselection"><select name="" id=""><option>CTL-F1</option></select></div>
							</div>
							</div>	
							<div class="table_row_input1">
								<div class="table_cell_input1">Starting Temp.(C)</div>
								<div class="table_cell_input2"><span id="startingTemp"><input type="text" id="" name=""/></span></div>
							</div>
							<div class="table_row_input1">
								<div class="table_cell_input1">Final Temp.(C)</div>
								<div class="table_cell_input2"><span id="finalTemp"><input type="text" id="" name=""/></span></div>
							</div>
							<div class="table_row_input1">
								<div class="table_cell_input1">File :<input type="text" id="" name=""/> </div>
								<div class="table_cell_input2">
								<iframe  src="" style="border: medium none;" id="uploadFrame" ></iframe>							
								</div>
							</div>
						</div>
						
						

					</div>
						
						
					</div>
					
					<div id="right_table_wrapper">
						
						<div id="table_container_input">
						
							<div class="table_row_input1">
								
								
				
						<div class="table_cell_input2" align="left">

						 <div class="jqPlot" id="chart1" style="height:320px; width:600px;"><img src = "images/others/graph2.jpg" class="cursor" id="" onclick=""/></div>
						<div class="jqPlot" id="chart2" style="margin-top: 30px; height:100px; width:600px;"><img src = "images/others/graph2.jpg" class="cursor" style="height:100px; width:600px;" id="" onclick=""/></div>
							<button class="hidden"  id="resetGraph" value="reset" onclick="">Reset</button>
						 
							</div>						
						</div>
						
						
						
					</div>
					
					
				</div>
		   </form>			
			</div>
		</div>
	</div>	
<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Reagents & Supplies</div>
		<div class="portlet-content"></div>
	</div>

</div>
<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Equiment</div>
		<div class="portlet-content"></div>
	</div>

</div>
<div class="cleaner"></div>
		<div align="left" style="float:right;">
		<form onsubmit="return avoidFormSubmitting()">
				<table cellpadding="0" cellspacing="0" class="" align="" border="0">
				<tr>
				<td><span>Next:
				<select name="nextOption" id="nextOption" class=""></select>
				<input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
				<input type="button" value="Next" onclick=""/></span></td>
				</tr>
				</table>
		</form>
		</div>

<script>
	$(function() {
		$(".minportlet").trigger("change");
		$("#tabs").tabs();
		$("#labtabs").tabs();
		$(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all").find(".portlet-header").addClass(
						"ui-widget-header ui-corner-all").prepend(
						"<span class='ui-icon ui-icon-minusthick'></span>");

		/**For add new popup**/

		$(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all").find(".addnew")
				.append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

		$(".portlet-header .ui-addnew").click(function() {

		});

		/**For Refresh**/

		$(".portlet-header .ui-icon").click(function() {
					$(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
					$(this).parents(".portlet:first").find(".portlet-content").toggle();
				});
		$(".maxportlet").live('change',function() {
			// $(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick"); 
			//$(this).removeClass("ui-icon-plusthick").addClass("ui-icon-minusthick");
			
			$(this).parents(".portlet:first").find(".portlet-content").show();
		});
		$(".minportlet").live('change',function() {
			// $(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick"); 
			//$(this).removeClass("ui-icon-plusthick").addClass("ui-icon-minusthick");
			
			$(this).parents(".portlet:first").find(".portlet-content").hide();
		});
		
		$("#cryolabelview").hide();
		$("input[name='cryolabelverification']").click(function(){
	     	var checkradio=$("input[name='cryolabelverification']:checked").val();
	    	 if(checkradio!=1){
	    		 $("#cryolabelview").hide();
	    	 }
	     	else {
	    		 $("#cryolabelview").show();
	    	 }
	     });
	    
	    $('#cryolabelview').live('click', function (e) {
	    	
	    	//showPopUp('open','anticoagulants','Anticogulants and Additives','850','550');
	    	 
	    });

	});
</script>

