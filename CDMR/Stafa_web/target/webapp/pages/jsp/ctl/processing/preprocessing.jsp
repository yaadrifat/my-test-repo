<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script type="text/javascript" src="js/placeholder.js"></script>
<script type="text/javascript" src="js/dataSearch.js"></script>
<script type="text/javascript" src="js/jquery/jquery.loadjson.js"></script>
<script type="text/javascript" src="jsp/ctl/technicians/js/productTable.js"></script>
<script type="text/javascript" >

function loadNextDropdown(obj,name,type){
	 url="loadNextDropdown";
	 var fieldName="";
	 if($.trim($(obj).val()) ==""){
		 return false;
	 }
	 
	 var row = $(obj).parent().closest("tr");
	 var tmpValue="";
	 var criteria=" and sp_type='"+type+"'  and";
	 if(name=="bag"){
		 criteria+= "  component ='"+$.trim($(obj).val())+"'";
		 fieldName = "bag";
		 $(row).find("#componentId").find('option').remove();
		 $(row).find("#intendedprocess").find('option').remove();
	 }else if(name=="componentId"){
		 tmpValue = $(row).find("#component").val();
		 criteria+= " component='"+$.trim(tmpValue) +"' and bag='"+$.trim($(obj).val())+"' ";
		 fieldName = "componentId";
		 $(row).find("#intendedprocess").find('option').remove();
	 }else if(name=="intendedprocess"){
		 criteria+= " component ='"+$.trim($(row).find("#component").val()) +"' and bag='"+$.trim($(row).find("#bag").val())+"' and componentId='"+$.trim($(obj).val())+"' ";
		 //fieldName = "to_char(expDate,'Mon DD, YYYY'),reagentsupplies";
		 fieldName = "intendedProcess,splitPool";
		 //$(row).find("#intendedprocess").find('option').remove();
	 }
	 
	 //alert("fieldName "+fieldName+",criteria : "+criteria);
	 $('.progress-indicator').css( 'display', 'block' );
	 response = jsonDataCall(url,"jsonData={fieldName:\""+fieldName+"\",criteria:\""+criteria+"\"}");
	
	//alert(response.dropdownList);
	if (name=="intendedprocess"){
		$(row).find("#"+name).find('option').remove().end()
	    .append('<option value="">Select</option>');
		 $(response.dropdownList).each(function(i){
	 			//alert("this " + this);
	 			//alert(this[0] + " / " + this[1]);
				 if(jQuery.type(this[0]) == "string"){
					 $(row).find("#"+name).find('option').end().append('<option value="'+this[0]+ '">'+ this[0]+'</option>');
				 	$(row).find("#splitPool").val(this[1]);
				 }
				 
			 });
	}else{
		 $(row).find("#"+name).find('option').remove().end()
		    .append('<option value="">Select</option>');
		 $(response.dropdownList).each(function(i){
			 $(row).find("#"+name).find('option').end().append('<option value="'+this+ '">'+ this+'</option>');
			// alert(i+"/ base "+  $(row).find("#"+name).find('option').html());
		 });
		
	 }
	  $.uniform.restore('select');
      $("select").uniform();
      $('.progress-indicator').css( 'display', 'none' );
}
function constructPoolProduct(flag,productId){
	//alert("flag:LL"+flag+"pro::"+productId);
	var parentId = "'Prod_3333'";
	alert("parent::"+parentId);
	var poolProductServerParam = function ( aoData ) {
														aoData.push( { "name": "application", "value": "stafa"});
														aoData.push( { "name": "module", "value": "CTL_Pool_Product"});	
														
														aoData.push( { "name": "param_count", "value":"1"});
														aoData.push( { "name": "param_1_name", "value":":PARENTID:"});
														aoData.push( { "name": "param_1_value", "value":parentId});
														
														/* aoData.push( {"name": "col_1_name", "value": "" } );
														aoData.push( { "name": "col_1_column", "value": ""});
														
														aoData.push( { "name": "col_2_name", "value": "lower(prereg.QUANTITY)"});
														aoData.push( { "name": "col_2_column", "value": "lower(prereg.QUANTITY)"});
														
														aoData.push( {"name": "col_3_name", "value": "lower(rs.RS_MANUFACTURE)" } );
														aoData.push( { "name": "col_3_column", "value": "lower(rs.RS_MANUFACTURE)"});
														
														aoData.push( {"name": "col_4_name", "value": "lower(rs.RS_LOTNUMBER)" } );
														aoData.push( { "name": "col_4_column", "value": "lower(rs.RS_LOTNUMBER)"});
														
														aoData.push( {"name": "col_5_name", "value": "lower(rs.RS_EXPDATE)" } );
														aoData.push( { "name": "col_5_column", "value": "lower(rs.RS_EXPDATE)"}); */
														
	};
	var poolProductColDef = [
							  {
									"aTargets": [0], "mDataProp": function ( source, type, val ) { 
									    return "<input type='checkbox id='splitPoolProduct' name='splitPoolProduct' value='"+source[0]"'/>";
							     }},
			                  {"sTitle":"Component",
									"aTargets": [1], "mDataProp": function ( source, type, val ) {
										return "";	
			                	 }},
							  {	"sTitle":"Bag",
			                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
											return "";	
			                	  }},
							  {	"sTitle":"Parent ID",
									  "aTargets": [3], "mDataProp": function ( source, type, val ) {
				                			 return "";
							  	  }},
							  {	"sTitle":"Component ID",
								  	  "aTargets": [4], "mDataProp": function ( source, type, val ) {
			                			 return "";
						  		  }},
							  {	"sTitle":"Weight(g)",
							  		  "aTargets": [5], "mDataProp": function ( source, type, val ) {
			                			 return "";
								  }},
							  {	"sTitle":"Volume(mL)",
						  		  "aTargets": [5], "mDataProp": function ( source, type, val ) {
		                			 return "";
							  }},
							  {	"sTitle":"CD34(*10^6)",
						  		  "aTargets": [5], "mDataProp": function ( source, type, val ) {
		                			 return "";
							  }},
							  {	"sTitle":"Intended Process",
						  		  "aTargets": [5], "mDataProp": function ( source, type, val ) {
		                			 return "";
							  }},
							  {	"sTitle":"Comments",
						  		  "aTargets": [5], "mDataProp": function ( source, type, val ) {
		                			 return "";
							  }}
						];
	var poolProductColManager = function () {
							$("#poolproDataTableColumn").html($('#poolprodDataTable_wrapper .ColVis'));
	};
	var poolProductCol = [{"bSortable": false},
		               null,
		               null,
		               null,
		               null,
		               null
		               ];
	 constructTable(flag,'poolprodDataTable',poolProductColManager,poolProductServerParam,poolProductColDef,poolProductCol);
}
function ontrigger(rowObj,productId){
	var url = "fetchPreProcessingData";
	var data = "productId="+productId;
	var response = jsonDataCall(url,data);
	//alert("response : "+response);
	$("#splitProductId").val(productId);
	constructPoolProduct(false,productId);
	//addpoolproduct(productId);
}

function openClosePoolWidget(radioObj){
	if($(radioObj).val()==1){
		$(".portlet").find("#poolDiv").find(".ui-icon-plusthick").trigger("click");		
	}else{
		$(".portlet").find("#poolDiv").find(".ui-icon-minusthick").trigger("click");
		}
}

function openCloseSplitWidget(radioObj){
	if($(radioObj).val()==1){
		$(".portlet").find("#splitDiv").find(".ui-icon-plusthick").trigger("click");
	}else{
		$(".portlet").find("#splitDiv").find(".ui-icon-minusthick").trigger("click");
		}
}

function openCloseReagentsEquipment(radioObj){
	
	if($(radioObj).val()==1){
		
		$(".portlet").find("#reagentsDiv").find(".ui-icon-plusthick").trigger("click");
		$(".portlet").find("#equipmentsDiv").find(".ui-icon-plusthick").trigger("click");
				
		}else{
			$(".portlet").find("#reagentsDiv").find(".ui-icon-minusthick").trigger("click");
			$(".portlet").find("#equipmentsDiv").find(".ui-icon-minusthick").trigger("click");
			}
}

function createcols(e){	
	 if(e.keyCode==13){
		 var n=$("#norow").val();
			for(i=0;i<n;i++){
				addsplitproduct();
			}			
		}
	} 



function savePages(){
	try{
		if(savePreProcessing()){
			return true;
			}
		}catch(e){
			alert(e);
			}
}

function savePreProcessing(){
	var rowData = "";
	var component = "";
	var bag = "";
	var parentId = "";
	var componentId = "";
	var volume = "";
	var weight = "";
	var cd34_10_6 = "";
	var intendedprocess = "";
	var comments = "";
	var splitProducts;
	var splitPool = "";
	
	 $("#poolprodDataTable").find("#component").each(function (row){
	 	rowObj =  $(this).closest("tr");	 
	 	splitPoolProduct = 0;

	 	component=$(rowObj).find("#component").val();
	 	bag=$(rowObj).find("#bag").val();
	 	parentId=$(rowObj).find("#parentId").text();
	 	componentId=$(rowObj).find("#componentId").val();
	 	volume=$(rowObj).find("#volume").val();
	 	weight=$(rowObj).find("#weight").val();
	 	cd34_10_6=$(rowObj).find("#cd34_10_6").val();
	 	intendedprocess=$(rowObj).find("#intendedprocess").val();
	 	comments=$(rowObj).find("#comments").val();
	 	splitPool=$(rowObj).find("#splitPool").val();
		if(component!= undefined && component!='undefined'&& component!=''&& component!=0 ){
			  	rowData+= "{splitPoolProduct:"+splitPoolProduct+",component:'"+component+"',bag:'"+bag+"',parentId:'"+parentId+"',componentId:'"+componentId+"',volume:'"+volume+"',weight:'"+weight+"',cd34_10_6:'"+cd34_10_6+"',intendedprocess:'"+intendedprocess+"',comments:'"+comments+"',type:'POOL',refSplitPool:'"+splitPool+"'},";
		}
	 });
	 

	 if(rowData.length >0){
            rowData = rowData.substring(0,(rowData.length-1));
        }
	
	 var rowData1= "";
	 $("#splitprodDataTable").find("#component").each(function (row){
		 	rowObj =  $(this).closest("tr");	 
		 	splitPoolProduct = 0;
		 	parentId=$("#splitProductId").val();
		 	component=$(rowObj).find("#component").val();
		 	bag=$(rowObj).find("#bag").val();
		 	componentId=$(rowObj).find("#componentId").val();
		 	volume=$(rowObj).find("#volume").val();
		 	weight=$(rowObj).find("#weight").val();
		 	cd34_10_6=$(rowObj).find("#cd34_10_6").val();
		 	intendedprocess=$(rowObj).find("#intendedprocess").val();
		 	comments=$(rowObj).find("#comments").val();
		 	splitPool=$(rowObj).find("#splitPool").val();
			if(component!= undefined && component!='undefined'&& component!=''&& component!=0 ){
				rowData1+= "{splitPoolProduct:"+splitPoolProduct+",component:'"+component+"',bag:'"+bag+"',parentId:'"+parentId+"',componentId:'"+componentId+"',volume:'"+volume+"',weight:'"+weight+"',cd34_10_6:'"+cd34_10_6+"',intendedprocess:'"+intendedprocess+"',comments:'"+comments+"',type='SPLIT',refSplitPool:'"+splitPool+"'},";
			}
		 });
	 
	 if(rowData1.length >0){
		 rowData1 = rowData1.substring(0,(rowData1.length-1));
     }
	 	//alert("s1");

		var rowData2="";
	 	var quantity;
		var preparationregents;
		 var reagentSupplies;
		 $("#reagenTable").find("#reagentsname").each(function (row){
			 rowHtml =  $(this).closest("tr");	 
			 reagentSupplies = "";
			 quantity="";
			
		 		quantity=$(rowHtml).find("#reagents_quality").val();
		 		preparationregents=$(rowHtml).find("#preparationReagents").val()
		 		reagentsSup=$(rowHtml).find("#reagentsupplies").val();
		 		//alert("test " + quantity);
				if(reagentsSup!= undefined && reagentsSup!='undefined'&& reagentsSup!=''&& reagentsSup!=0 ){
					rowData2+= "{preparationReagents:"+preparationregents+",quantity:'"+quantity+"',refReagentsSupplies:'"+reagentsSup+"'},";
		 		}
		 		
		 		
		 		
		 });
		 
		//alert("row data " + rowData2)
		 if(rowData2.length >0){
			 rowData2 = rowData2.substring(0,(rowData2.length-1));
	        }
		
		 var rowData3= "";
		 var equimentName;
		 var manufacturer;
		 var modelNumber;
		 var maintainanceDate;
		 var mhsNumber;
		 var pkeqData;
		 var reagentsSup;
		 $("#equipmentTable").find("#equiment_name").each(function (r){
			 row =  $(this).closest("tr");
			   equimentName="";
			   reagentsSup="";
		 		pkeqData= $(row).find("#preparationEquipmentsId").val();
				equimentName= $(row).find("#equiment_name").val();
				//maintainanceDate=$(this).find(".dateEntry").val();
				reagentsSup=$(row).find("#reagentsupplies").val();
		 	
		 	if(reagentsSup !="0" && reagentsSup!="" && reagentsSup!=0){
		 		rowData3+= "{preparationEquipmentsId:"+pkeqData+",refReagentsSupplies:'"+reagentsSup+"'},";
	 		}
		 		
		 		//alert(equipments + " / " + manufacturer + " / " + rowData3 );
		 });
		 
		 if(rowData3.length >0){
			 rowData3 = rowData3.substring(0,(rowData3.length-1));
	     }
	 	
		url = "savePreProcessing";

		var preProcessing  = 0; //Have to make change as per the value, now default is 0
		var wbcCount  = $("#wbcCount").val();
		var totalProductVolume  = $("#totalProductVolume").val();
		var tnc_10_10  = $("#tnc_10_10").val();
		var totalProductWeight  = $("#totalProductWeight").val();
		 
		//results = results.substring(1, (results.length - 1));
		var jsonData = "jsonData={poolProductData:[" + rowData + "],splitProductData:["
				+ rowData1 + "],reagentData:[" + rowData2 + "],equipmentData:[" + rowData3 + "],preProcessingData:{preProcessing:"+preProcessing+",wbcCount:'"+wbcCount+"',totalProductVolume:'"+totalProductVolume+"',tnc_10_10:'"+tnc_10_10+"',totalProductWeight:'"+totalProductWeight+"'},nextOption:\""+ escape($("#nextOption").val()) + "\",flowNextFlag:'" +$("#flowNextFlag").val()+"'}";
		//alert("s3");
		$('.progress-indicator').css('display', 'block');
		//alert("jsonData " + jsonData)
		response = jsonDataCall(url, jsonData);
		//alert("s4");

		$('.progress-indicator').css('display', 'none');
		//alert("Data saved Successfully");
		
		
		//have to reload all the data tables here
		
		//Next flow functionality apply here
		
		return true;
	}

$(document).ready(function(){
	//hide_slidecontrol();
	
	show_slidecontrol("slidecontrol");
	hide_slidewidgets();
/*	var oTable1=$("#productDataTable").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null
											              
											             ],
											             //"sScrollX": "100%",
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#productDataTableColumn").html($('.ColVis:eq(0)'));
												}
		});
		*/
/* 	var oTable1=$("#poolprodDataTable").dataTable({
		"sPaginationType": "full_numbers",
		"sDom": 'C<"clear">Rlfrtip',
		"aoColumns": [ { "bSortable":false},
		               null,
		               null,
		               null,
		               null,
		               null,
		               null,
		               null,
		               null,
		               null
		              
		             ],
		             //"sScrollX": "100%",
			"oColVis": {
				"aiExclude": [ 0 ],
				"buttonText": "&nbsp;",
				"bRestore": true,
				"sAlign": "left"
			},
			"fnInitComplete": function () {
		        
		        $("#poolproDataTableColumn").html($('#poolprodDataTable_wrapper .ColVis'));
			}
}); */
	var oTable1=$("#splitprodDataTable").dataTable({
		"sPaginationType": "full_numbers",
		"sDom": 'C<"clear">Rlfrtip',
		"aoColumns": [ { "bSortable":false},
		               null,
		               null,
		               null,
		               null,
		               null,
		               null,
		               null,
		               null
		         
		              
		             ],
		             //"sScrollX": "100%",
			"oColVis": {
				"aiExclude": [ 0 ],
				"buttonText": "&nbsp;",
				"bRestore": true,
				"sAlign": "left"
			},
			"fnInitComplete": function () {
		        //$("#splitproDataTableColumn").html($('.ColVis:eq(1)'));
		        $("#splitproDataTableColumn").html($('#splitprodDataTable_wrapper .ColVis'));
			}
			
});

	var donorId="";
	constructProProcessList(false);
	constructEquipmentTable(false,donorId);
	constructReagentsTable(false,donorId);

		
$("select").uniform(); 
//alert($(".portlet").find("#poolDiv").find("input").prop("checked"));

//poolDivContent

});

/* Reagents and equipment starts*/

var donorId = $("#donor").val();
var oTable;
function constructReagentsTable(flag,donorId){
	 var reagentCriteria = "";
		/*if(donorId!=null && donorId!=""){
			reagentCriteria = " and pre.FK_DONOR = '"+donorId+"'";
		}
		*/
		var preparation = "";
		var plannedProcedure = $("#donorProcedure").val();
		
		var reagentsServerParam = function ( aoData ) {
			aoData.push( { "name": "application", "value": "stafa"});
			aoData.push( { "name": "module", "value": "apheresis_Preparation_Reagents"});	
			//aoData.push( { "name": "criteria", "value": reagentCriteria});
			aoData.push( { "name": "param_count", "value":"2"});
			aoData.push( { "name": "param_1_name", "value":":PREPARATION:"});
			aoData.push( { "name": "param_1_value", "value":preparation});
			aoData.push( { "name": "param_2_name", "value":":PLANNEDPROCEDURE:"});
			aoData.push( { "name": "param_2_value", "value":plannedProcedure});
			
			
			aoData.push( {"name": "col_1_name", "value": "lower(rs.RS_NAME)" } );
			aoData.push( { "name": "col_1_column", "value": "2"});
			
			aoData.push( { "name": "col_2_name", "value": "lower(prereg.QUANTITY)"});
			aoData.push( { "name": "col_2_column", "value": "lower(prereg.QUANTITY)"});
			
			aoData.push( {"name": "col_3_name", "value": "lower(rs.RS_MANUFACTURE)" } );
			aoData.push( { "name": "col_3_column", "value": "lower(rs.RS_MANUFACTURE)"});
			
			aoData.push( {"name": "col_4_name", "value": "lower(rs.RS_LOTNUMBER)" } );
			aoData.push( { "name": "col_4_column", "value": "lower(rs.RS_LOTNUMBER)"});
			
			aoData.push( {"name": "col_5_name", "value": "lower(rs.RS_EXPDATE)" } );
			aoData.push( { "name": "col_5_column", "value": "lower(rs.RS_EXPDATE)"});
			
		};
		var reagentColDef = [
							 {
									"aTargets": [0], "mDataProp": function ( source, type, val ) { 
									    return "<input type='checkbox' id='preparationReagents' name='preparationReagents' value='"+source[0]+"'/><input id='reagentsupplies' type='hidden' value='"+source[6]+"' name='reagentsupplies'/>";
							     }},
			                  {"sTitle":"Reagents and Supplies",
									"aTargets": [1], "mDataProp": function ( source, type, val ) {
										link = source[1];
										if (source[0] == 0){
											 link ="<select class='select1'  id='reagentsname' name='reagentsname' onchange='javascript:loadDropdown(this,\"reagents_manufacturer\");'>";
												 var selectTxt = $("#reagent_hidden").html();
		                                	  var searchTxt = 'value="'+source[1] +'"'; 
		                                	  selectTxt = selectTxt.replace(searchTxt,searchTxt +' selected')
												link+= selectTxt + "</select>";
										}
										return link;	
			                	 }},
							  {	"sTitle":"Quantity",
			                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
			                			 link = source[7];
											if (source[0] == 0){
												 link ="<input type='text' id='reagents_quality' name='quantity' value='"+source[7]+ "' />";
											}
											return link;	
			                	  }},
							  {	"sTitle":"Manufacturer",
									  "aTargets": [3], "mDataProp": function ( source, type, val ) {
										  link = source[3];
				                			 if (source[0] == 0){
				                				 link = "<select class='select1' id='reagents_manufacturer' name='reagents_manufacturer' onchange='javascript:loadDropdown(this,\"reagents_lot\");'>";
				                				 var tmpCr = "  and rs_type='REAGENTS'  and name ='"+source[1] +"'";
				                				 loadTmpDropdown($("#tmpSelect"),"manufacture",tmpCr);
				                				// alert(" tmp " +$("#tmpSelect").html() );
				                				link+= $("#tmpSelect").html() +"</select>";
				                			 }
				                			 return link;
							  	  }},
							  {	"sTitle":"Lot#",
								  	  "aTargets": [4], "mDataProp": function ( source, type, val ) {
								  		 link = source[4];
			                			 if (source[0] == 0){
			                				 link = "<select class='select1' id='reagents_lot' name='reagents_lot' onchange='javascript:loadDropdown(this,\"reagents_expiration_date\");' >";
			                				// var tmpCr = "  and rs_type='REAGENTS'  and name ='"+source[1]) +"' and manufacture='"+$.trim($(obj).val())+"' ";
			                				// loadTmpDropdown($("#tmpSelect"),"manufacture",tmpCr);
			                				// alert(" tmp " +$("#tmpSelect").html() );
			                				//			link+= $("#tmpSelect").html() +"</select>";
			                				 link+= "</select>";
			                			 }
			                			 return link;
						  		  }},
							  {	"sTitle":"Expiration Date",
							  		  "aTargets": [5], "mDataProp": function ( source, type, val ) {
							  			link = source[5];
			                			 if (source[0] == 0){
			                				 link= "<input type='text' id='reagents_expiration_date' name='reagents_expiration_date' class='dateEntry dateclass' value=''  />";
			                			 }
			                			 return link;
								  }}
							];
		var reagentColManager = function () {
			//$("#reagenColumn").html($('.ColVis:eq(1)'));
			$("#reagenColumn").html($('#reagenTable_wrapper .ColVis'));
		};
		var reagentCol = [{"bSortable": false},
			               null,
			               null,
			               null,
			               null,
			               null
			               ];
		 constructTable(flag,'reagenTable',reagentColManager,reagentsServerParam,reagentColDef,reagentCol);

}
function constructEquipmentTable(flag,donorId){
	 var criteria = "";
		/*if(donorId!=null && donorId!=""){
			criteria = " and pre.FK_DONOR = '"+donorId+"'";
		}*/
		var preparation = "";
		var plannedProcedure = $("#donorProcedure").val();
		var link;
			var equipServerParam =  function ( aoData ) {
															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "apheresis_Preparation_Equipments"});	
															//aoData.push( { "name": "criteria", "value": criteria});
															
															aoData.push( { "name": "param_count", "value":"2"});
															aoData.push( { "name": "param_1_name", "value":":PREPARATION:"});
															aoData.push( { "name": "param_1_value", "value":preparation});
															aoData.push( { "name": "param_2_name", "value":":PLANNEDPROCEDURE:"});
															aoData.push( { "name": "param_2_value", "value":plannedProcedure});
															
															
															aoData.push( {"name": "col_1_name", "value": "" } );
															aoData.push( { "name": "col_1_column", "value": ""});
															
															aoData.push( { "name": "col_2_name", "value": ""});
															aoData.push( { "name": "col_2_column", "value": ""});
															
															aoData.push( {"name": "col_3_name", "value": "" } );
															aoData.push( { "name": "col_3_column", "value": ""});
															
															aoData.push( {"name": "col_4_name", "value": "" } );
															aoData.push( { "name": "col_4_column", "value": ""});
															
															aoData.push( {"name": "col_5_name", "value": "" } );
															aoData.push( { "name": "col_5_column", "value": ""});
															
										};
										
					var equipColDef =  [
									 {
											"aTargets": [0], "mDataProp": function ( source, type, val ) { 
												return "<input type='checkbox' id='preparationEquipmentsId' name='preparationEquipmentsId' value='"+source[0]+"'/><input id='reagentsupplies' type='hidden' value='"+source[6]+"' name='reagentsupplies'/>";
									     }},
					                  {"sTitle":"Equipment",
											"aTargets": [1], "mDataProp": function ( source, type, val ) {
												
												if (source[0] == 0){
													 link ="<select class='select1' id='equiment_name' name='equiment_name' onchange='javascript:loadDropdown(this,\"equipment_manufacturer\");' >";
														 var selectTxt = $("#equipment_hidden").html();
				                                	  var searchTxt = 'value="'+source[1] +'"'; 
				                                	  selectTxt = selectTxt.replace(searchTxt,searchTxt +' selected')
														link+= selectTxt + "</select>";
												}else{
													link = source[1];
												}
												
												return link;	
													
												//return source[1];
					                	 }},
									  {	"sTitle":"Manufacturer",
					                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
					                			 link = source[3];
					                			 if (source[0] == 0){
					                				 link = "<select class='select1' id='equipment_manufacturer' name='manufacturer' onchange='javascript:loadDropdown(this,\"modelno\");'>";
					                				 var tmpCr = "  and rs_type='EQUIPMENT'  and name ='"+source[1] +"'";
					                				 loadTmpDropdown($("#tmpSelect"),"manufacture",tmpCr);
					                				// alert(" tmp " +$("#tmpSelect").html() );
					                				link+= $("#tmpSelect").html() +"</select>";
					                			 }
					                			 return link;
					                	  }},
									  {	"sTitle":"Model#",
											  "aTargets": [3], "mDataProp": function ( source, type, val ) {
												  link = source[4];
						                			 if (source[0] == 0){
						                				 link = "<select class='select1' id='modelno' name='modelno' onchange='javascript:loadDropdown(this,\"mhs\");'></select>";
						                			 }
						                		 return link;
									  	  }},
									  {	"sTitle":"MHS#",
										  	  "aTargets": [4], "mDataProp": function ( source, type, val ) {
										  		link = source[2];
					                			 if (source[0] == 0){
					                				 link = "<select class='select1' id='mhs' name='mhs' onchange='javascript:loadDropdown(this,\"maintainanceDueDate\");'></select>";
					                			 }
					                		 return link;
								  		  }},
									  {	"sTitle":"Maintenance Due Date",
									  		  "aTargets": [5], "mDataProp": function ( source, type, val ) { 
									  			link = source[5];
					                			 if (source[0] == 0){
					                				 link = "<input type='text' class='dateEntry dateclass calDate' id='maintainanceDueDate' name='maintainanceDueDate'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/>";
					                			 }
						                		 return link;
										  }}
									];
						var equipColManager =  function () {
								        //$("#equipmentTableColumn").html($(".ColVis:eq(0)"));
								        $("#equipmentTableColumn").html($('#equipmentTable_wrapper .ColVis'));
									};
						var equipCol = [{"bSortable": false},
							               null,
							               null,
							               null,
							               null,
							               null
							               ];
						
					constructTable(flag,'equipmentTable',equipColManager,equipServerParam,equipColDef,equipCol);

									
} 

var reagents_elementcount=0;
var disposable_elementcount=0;
var machine_elementcount=0;
function addnewRegents(activeFlag){
	var newRow;
		 newRow="<tr>"+
			"<td><input type='checkbox' id='preparationReagents' name='preparationReagents' value='0'></td>"+
			"<td><select class='select1'  id='reagentsname' name='reagentsname' onchange='javascript:loadDropdown(this,\"reagents_manufacturer\");'>"+$("#reagent_hidden").html() + "</select></td>"+
			"<td><input type='text' id='reagents_quality' name='quantity' /></td>"+
			"<td><select class='select1' id='reagents_manufacturer' name='reagents_manufacturer' onchange='javascript:loadDropdown(this,\"reagents_lot\");'></select></div></td>"+
			"<td><div><select class='select1' id='reagents_lot' name='reagents_lot' onchange='javascript:loadDropdown(this,\"reagents_expiration_date\");' ></select></div></td>"+
			"<td><input type='text' class='dateEntry dateclass'id='reagents_expiration_date' name='reagents_expiration_date'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/></td>"+
			"</tr>";
			//alert(newRow);
		$("#reagenTable tbody").prepend(newRow); 
		//alert("1");
		 $(".plain").each(function (){
			   $(this).removeClass("plain");
			    setObjectAutoComplete($(this),"reagent_hidden")
		   });
		 $("#reagenTable .ColVis_MasterButton").triggerHandler("click");
		  $('.ColVis_Restore:visible').triggerHandler("click");
		  $("div.ColVis_collectionBackground").trigger("click");
	//$("select").uniform();
    $("select").uniform(); 	
    $( ".dateclass" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
        changeYear: true, yearRange: "c-50:c+45"});
	
}
var equipmentTable=0;
function addnewequipment(activeFlag){
	var newRow;
	equipmentTable+=1;
	 newRow="<tr>"+
		"<td><input type='checkbox' id='preparationEquipmentsId' name='preparationEquipmentsId' value='0'/></td>"+
		"<td><select class='select1' id='equiment_name'name='equiment_name' onchange='javascript:loadDropdown(this,\"equipment_manufacturer\");'>"+$("#equipment_hidden").html() + "</select></td>"+
		"<td><select class='select1' id='equipment_manufacturer' name='manufacturer' onchange='javascript:loadDropdown(this,\"modelno\");'></select></td>"+
		"<td><select class='select1' id='modelno' name='modelno' onchange='javascript:loadDropdown(this,\"mhs\");'></select></td>"+
		"<td><select class='select1' id='mhs' name='mhs' onchange='javascript:loadDropdown(this,\"maintainanceDueDate\");'></select></td>"+
		"<td><input type='text' class='dateEntry dateclass' id='maintainanceDueDate' name='maintainanceDueDate'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/></td>"+
		"</tr>";
		$("#equipmentTable tbody").prepend(newRow); 
		$("select").uniform(); 	
		$( ".dateclass" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
		 changeYear: true, yearRange: "c-50:c+45"});
		var param ="{module:'APHERESIS',page:'PREPARATION'}";
		markValidation(param);
}  

function loadDropdown(obj,name){
	//alert("loadDropdown");
	//alert("val " + $(obj).val() + " / " + name);
	 url="loadDropdown";
	 var fieldName="";
	 if($.trim($(obj).val()) ==""){
		 return false;
	 }
	 
	 var row = $(obj).parent().closest("tr");
	 var tmpValue="";
	 var criteria=" and ";
	 if(name== "reagents_manufacturer"){
		 criteria+= " rs_type='REAGENTS'  and name ='"+$.trim($(obj).val())+"'";
		 fieldName = "manufacture";
		 $(row).find("#reagents_lot").find('option').remove();
		 $(row).find("#reagents_expiration_date").val("");
		 
	 }else if(name=="reagents_lot"){
		 tmpValue = $(row).find("#reagentsname").val();
		 criteria+= " rs_type='REAGENTS'  and name ='"+$.trim(tmpValue) +"' and manufacture='"+$.trim($(obj).val())+"' ";
		 fieldName = "lotNumber";
		 $(row).find("#reagents_expiration_date").val("");
	 }else if(name=="reagents_expiration_date"){
		 criteria+= " rs_type='REAGENTS'  and name ='"+$.trim($(row).find("#reagentsname").val()) +"' and manufacture='"+$.trim($(row).find("#reagents_manufacturer").val())+"' and lotNumber='"+$.trim($(obj).val())+"' ";
		 fieldName = "to_char(expDate,'Mon DD, YYYY'),reagentsupplies";
		 $(row).find("#reagents_expiration_date").val("");
		 
	 }else if(name== "equipment_manufacturer"){
		 criteria+= " rs_type='EQUIPMENT'  and name ='"+$.trim($(obj).val())+"'";
		 fieldName = "manufacture";
		 $(row).find("#modelnumber").find('option').remove();
		 $(row).find("#mhs").find('option').remove();
		 $(row).find("#maintainanceDueDate").val("");
	 }else if(name== "modelno"){
		 criteria+= " rs_type='EQUIPMENT'  and name ='"+$.trim($(row).find("#equiment_name").val()) +"' and manufacture='"+$.trim($(obj).val())+"'";
		 fieldName = "lotNumber";
		
		 $(row).find("#mhs").find('option').remove();
		 $(row).find("#maintainanceDueDate").val("");
	 }else if(name== "mhs"){
		 criteria+= " rs_type='EQUIPMENT'  and name ='"+$.trim($(row).find("#equiment_name").val()) +"' and manufacture='"+$.trim($(row).find("#equipment_manufacturer").val())+"' and lotNumber ='"+$.trim($(obj).val())+"'";
		 fieldName = "mhs";
		
		 $(row).find("#mhs").find('option').remove();
		 $(row).find("#maintainanceDueDate").val("");
	 }else if(name=="maintainanceDueDate"){
		 criteria+= " rs_type='EQUIPMENT'  and name ='"+$.trim($(row).find("#equiment_name").val()) +"' and manufacture='"+$.trim($(row).find("#equipment_manufacturer").val())+"' and lotNumber ='"+$.trim($(row).find("#modelno").val())+"' and mhs='"+$.trim($(obj).val()) +"'";
		 fieldName = "to_char(expDate,'Mon DD, YYYY'),reagentsupplies";
		 $(row).find("#maintainanceDueDate").val("");
		 
	 }
	 
	 //alert("fieldName "+fieldName+",criteria : "+criteria);
	 $('.progress-indicator').css( 'display', 'block' );
	 response = jsonDataCall(url,"jsonData={fieldName:\""+fieldName+"\",criteria:\""+criteria+"\"}");
	
	
	 if(name== "reagents_expiration_date"){
 		$(response.dropdownList).each(function(i){
 			//alert("this " + this);
 			//alert(this[0] + " / " + this[1]);
			 if(jQuery.type(this[0]) == "string"){
			 	$(row).find("#"+name).val(this[0]);
			 	$(row).find("#reagentsupplies").val(this[1]);
			 }
			 
		 });
	 }else if (name=="maintainanceDueDate"){
		 $(response.dropdownList).each(function(i){
	 			//alert("this " + this);
	 			//alert(this[0] + " / " + this[1]);
				 if(jQuery.type(this[0]) == "string"){
				 	$(row).find("#"+name).val(this[0]);
				 	$(row).find("#reagentsupplies").val(this[1]);
				 }
				 
			 });
	}else{
		 $(row).find("#"+name).find('option').remove().end()
		    .append('<option value="">Select</option>');
		 $(response.dropdownList).each(function(i){
			 $(row).find("#"+name).find('option').end().append('<option value="'+this+ '">'+ this+'</option>');
			// alert(i+"/ base "+  $(row).find("#"+name).find('option').html());
		 });
		
	 }
	  $.uniform.restore('select');
      $("select").uniform();
      $('.progress-indicator').css( 'display', 'none' );
}

/* Reagents and equipment ends*/
function addpoolproduct(parentId){
	
	$("#poolprodDataTable tbody").prepend("<tr>"+
			"<td><input type='checkbox' id=''/><input type='hidden' name='splitPool' id='splitPool'/></td>"+
			"<td><select name='component' id='component' onchange='javascript:loadNextDropdown(this,\"bag\",\"POOL\");'>"+$("#poolComponent").html()+"</select></td>"+
			"<td><select name='bag' id='bag' onchange='javascript:loadNextDropdown(this,\"componentId\",\"POOL\");'></select></td>"+
			"<td id='parentId' name='parentId'>"+parentId+"</td>"+
			"<td><select name='componentId' id='componentId' onchange='javascript:loadNextDropdown(this,\"intendedprocess\",\"POOL\");'></select></td>"+
			"<td><input type='text' id='volume' name='volume'></td>"+
			"<td><input type='text' id='weight' name='weight'></td>"+
			"<td><input type='text' id='cd34_10_6' name='cd34_10_6'></td>"+
			"<td><select name='intendedprocess' id='intendedprocess'></select></td>"+
			"<td><input type='text' id='comments' name='comments'></td>"+
			"</tr>");
	
$("select").uniform(); 

} 
function addsplitproduct(){

	$("#splitprodDataTable tbody").prepend("<tr>"+
			"<td><input type='checkbox' id=''/><input type='hidden' name='splitPool' id='splitPool'/></td>"+
			"<td><select name='component' id='component' onchange='javascript:loadNextDropdown(this,\"bag\",\"SPLIT\");'>"+$("#splitComponent").html()+"</select></td>"+
			"<td><select name='bag' id='bag' onchange='javascript:loadNextDropdown(this,\"componentId\",\"SPLIT\");'></select></td>"+
			"<td><select name='componentId' id='componentId' onchange='javascript:loadNextDropdown(this,\"intendedprocess\",\"SPLIT\");'></select></td>"+
			"<td><input type='text' id='volume' name='volume'></td>"+
			"<td><input type='text' id='weight' name='weight'></td>"+
			"<td><input type='text' id='cd34_10_6' name='cd34_10_6'></td>"+
			"<td><select name='intendedprocess' id='intendedprocess'></select></td>"+
			"<td><input type='text' id='comments' name='comments'></td>"+
			"</tr>");

$("select").uniform(); 
} 


</script>
 <div  id="slidecontrol" style="display:none;">
	<ul class="ulstyle">
	<span>
			<li class="listyle print_label"><img src = "images/icons/Bar_Code_32.png" class="barcodePrint" onclick="" style="width:30px;height:30px;"/></li>
			<li class="listyle print_label" ><b><s:text name="stafa.label.label"/></b> </li>
	</span>
	<span id="notesSpan">
		<li class="listyle notes_icon"  style="margin-left:18%"><img src = "images/icons/notes.png"  onclick="" style="width:30px;height:30px;"/><sup style="margin-left:2%"><span class='subspan'></span></sup></li>
		<li class="listyle notes_icon"><b><s:text name="stafa.label.notes"/></b> </li>
	</span>
	<span>
		<li class="listyle print_label"><img src = "images/icons/labresults.png"  style="width:30px;height:30px;cursor:pointer;" onclick=''/></li>
		<li class="listyle print_label" ><b><s:text name="stafa.label.labresult"/></b> </li>
	</span>
	<span>
		<li class="listyle medication_icon"><img src = "images/icons/capsule1.png" onclick="medicationpopup(donormrn);" style="width:30px;height:30px;"/></li>
		<li class="listyle medication_icon"><b>SOP</b></li>
	</span>
	<span>
	<span>
		<li class="listyle terminate_icon"><img src = "images/icons/terminated.png"  style="width:30px;height:30px;"/></li>
		<li class="listyle terminate_icon"><b>Billing CheckList</b></li>
	</span>
		<li class="listyle transfer_icon"><img src = "images/icons/transfer.png" onclick=""  style="width:30px;height:30px;"/></li>
		<li class="listyle transfer_icon" id="transfer_icon"><b><s:text name="stafa.label.transfer"/></b></li>
	</span>
	</ul>
</div>


<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Product Table</div>
		<div class="portlet-content">
			<form>
		        <input type="hidden" id="scanId" name="scanId"/>
				<div id="button_wrapper">
					<div id="divProductScan" style="margin-left:40%;">
						<input class="scanboxSearch barcodeSearch multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;" onkeyup="performScanData(event,this)"/>
						<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" onfocus="changeType(this)"/>
					</div>
	 			</div>
	 			<div id="productTableDiv" class="dataTableOverFlow">		
				<table cellpadding="0" cellspacing="0" border="1" id="productDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='productDataTableColumn'></th>
							<th>Product ID</th>
							<th>Recipient Name</th>
							<th>Recipient MRN</th>
							<th>Donor Name</th>
							<th>Donor MRN</th>
							<th>Volume(mL)</th>
							<th>Product Type</th>
							<th>Product Status</th>
						</tr>
					</thead>
				</table>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- <div class="column">
	<div class="portlet">
		<div class="portlet-header ">Product Table</div>
		<div class="portlet-content">
		<div id="button_wrapper">
			        		<div id="divProductScan" style="margin-left:40%;">
			       				 <input class="scanboxSearch barcodeSearch multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Recipient ID"  style="width:210px;" onkeyup="setScanFlag('RECIPIENT');performScanData(event,this)" />
								<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Recipient ID"  style="width:210px;display:none;" onfocus="changeType(this)" />
					 		</div>
				        </div>
			<div id="producDataTableDiv" style="overflow-x: auto; overflow-y: hidden;">
				<table cellpadding="0" cellspacing="0" border="1"
					id="productDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='productDataTableColumn'>Check All</th>
							<th>Product ID</th>
							<th>Recipient Name</th>
							<th>Recipient MRN</th>
							<th>Donor Name</th>
							<th>Donor MRN</th>
							<th>Volume(mL)</th>
							<th>Product Type</th>
							<th>Product Status</th>
						</tr>
					</thead>
					<tbody>
						
						</tbody>
				</table>
			</div>
		</div>
	</div>
</div> -->

<div class="column">
	<div class="portlet">
		<div class="portlet-header " id="poolDiv" >Pool Product<span><s:radio list="#{'1':'Yes','0':'No'}" name="poolProduct" id="poolProduct" onclick="openClosePoolWidget(this)"/></span></div>
		<div class="portlet-content" id="poolDivContent">
		<span class="hidden">
			<select name="poolComponent" id="poolComponent" >
				<option value="">Select</option>
			<s:iterator value="poolComponentList" var="rowVal" status="row">
				<option value='<s:property value="#rowVal"/>'> <s:property value="#rowVal"/> </option>
			</s:iterator> 		
			</select>
			<s:select style="display:none" headerKey="" headerValue="Select" list="poolBagList" listKey="pkCodelst" listValue="description" name="poolBag" id="poolBag"/>
			<s:select style="display:none" headerKey="" headerValue="Select" list="poolComponentIdList" listKey="pkCodelst" listValue="description" name="poolComponentId" id="poolComponentId"/>
			<s:select style="display:none" headerKey="" headerValue="Select" list="poolIntendedProcessList" listKey="pkCodelst" listValue="description" name="poolIntendedProcess" id="poolIntendedProcess"/>
		</span>
		<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="addpoolproduct("")"/>&nbsp;&nbsp;<label  class="cursor" onclick="addpoolproduct()"><b>Add </b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deletedRecurringTaskData()"/>&nbsp;&nbsp;<label class="cursor" onclick="deletedRecurringTaskData()" ><b>Delete</b></label>&nbsp;</div></td>
					<td style="width:40%" align="right">Total # of Components : <div id="countComponent"></div></td>
					
				</tr>
				</table>
			<div id="poolprodDataTableDiv"
				style="overflow-x: auto; overflow-y: hidden;">
				<table cellpadding="0" cellspacing="0" border="1"
					id="poolprodDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='poolproDataTableColumn'></th>
							<th>Component</th>
							<th>Bag</th>
							<th>Parent ID</th>
							<th>Component ID</th>
							<th>Weight(g)</th>
							<th>Volume(mL)</th>
							<th>CD34(*10^6)</th>
							<th>Intended Process</th>
							<th>Comments</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="column">
	<div class="portlet">
		<div id="splitDiv" class="portlet-header ">Split Product<span><s:radio list="#{'1':'Yes','0':'No'}" name="splitProduct" id="splitProduct" onclick="openCloseSplitWidget(this)"/></span></div>
		<div class="portlet-content">
		<span class="hidden">
		<select name="splitComponent" id="splitComponent" >
				<option value="">Select</option>
			<s:iterator value="splitComponentList" var="rowVal" status="row">
				<option value='<s:property value="#rowVal"/>'> <s:property value="#rowVal"/> </option>
			</s:iterator>
			</select>
			
			<s:select style="display:none" headerKey="" headerValue="Select" list="splitBagList" listKey="pkCodelst" listValue="description" name="splitBag" id="splitBag" onChange="requireFields(this);"/>
			<s:select style="display:none" headerKey="" headerValue="Select" list="splitComponentIdList" listKey="pkCodelst" listValue="description" name="splitComponentId" id="splitComponentId" onChange="requireFields(this);"/>
			<s:select style="display:none" headerKey="" headerValue="Select" list="splitIntendedProcessList" listKey="pkCodelst" listValue="description" name="splitIntendedProcess" id="splitIntendedProcess" onChange="requireFields(this);"/>
			<s:hidden id="splitProductId" name="splitProductId"/>
			<select name="tmpSelect" id="tmpSelect" ><option value="">Select</option></select>
			
			<select  name="equipment_hidden" id="equipment_hidden" >
				<option value="">Select</option>
				<s:iterator value="equipmentList" var="rowVal" status="row">
					<option value='<s:property value="#rowVal"/>'> <s:property value="#rowVal"/> </option>
				</s:iterator> 		
			</select>
			
			<select name="reagent_hidden" id="reagent_hidden" >
				<option value="">Select</option>
			<s:iterator value="reagentsnameList" var="rowVal" status="row">
				<option value='<s:property value="#rowVal"/>'> <s:property value="#rowVal"/> </option>
			</s:iterator> 		
			</select>
			
		</span>
		
		<table>
			<tr>
				<td><img src = "images/icons/addnew.jpg" style="width:16;height:16;cursor:pointer;" onclick="addsplitproduct()"/>&nbsp;&nbsp;<label id=""><b>Add Child</b></label></td>
				<td><div id="addtxt" style="width:180px;"><table><tr><td># of Children</td> &nbsp;&nbsp;&nbsp;<td><input type=text  id=norow style="width:30%"  onkeypress="createcols(event);"/></td></tr></table></div></td>&nbsp;&nbsp;<td><div><img src = "images/icons/no.png" style="width:16;height:16;cursor:pointer;" id ="" onclick="deletechildrows('childcreation');"/>&nbsp;&nbsp;<label id=""><b>Delete Child</b></label></div></td>
				<td width="27%" height="40px"></td>
				<td width="25%" height="40px"><div id="nchild" align="right"></div></td>
			</tr>
		</table>
		<!-- 
		<table>
			<tr>
				<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="addsplitproduct()"/>&nbsp;&nbsp;<label  class="cursor" onclick="addsplitproduct()"><b>Add </b></label>&nbsp;</div></td>
				<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deletedRecurringTaskData()"/>&nbsp;&nbsp;<label class="cursor" onclick="deletedRecurringTaskData()" ><b>Delete</b></label>&nbsp;</div></td>
				<td style="width:40%" align="right">Total # of Components : <div id="countComponent"></div></td>
			</tr>
		</table>
		 -->
		<div id="splitprodDataTableDiv"
				style="overflow-x: auto; overflow-y: hidden;">
				<table cellpadding="0" cellspacing="0" border="1"
					id="splitprodDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='splitproDataTableColumn'>Check All</th>
							<th>Component</th>
							<th>Bag</th>
							<th>Component ID</th>
							<th>Weight(g)</th>
							<th>Volume(mL)</th>
							<th>CD34(*10^6)</th>
							<th>Intended Process</th>
							<th>Comments</th>
						</tr>
					</thead>
					<tbody>
						
						</tbody>
				</table>
			</div>
		</div>
	</div>
</div>




<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Pre-Processing Calculation</div>
		<div class="portlet-content">
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">WBC Count(*10^6/mL)</td>
					<td class="tablewapper1"><input type="text" id="wbcCount" name="wbcCount"/></td>
					<td class="tablewapper">Total Product Volume(mL)</td>
					<td class="tablewapper1"><input type="text" id="totalProductVolume" name="totalProductVolume"/></td>
					
				</tr>
				<tr>
					<td class="tablewapper">TNC(*10^10)</td>
					<td class="tablewapper1"><input type="text" id="tnc_10_10" name="tnc_10_10"/></td>
					<td class="tablewapper">Total Product Weight(g)</td>
					<td class="tablewapper1"><input type="text" id="totalProductWeight" name="totalProductWeight"/></td>
                </tr>
            
				</table>
		
		</div>
	</div>

</div>
<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Lab Test<span><s:radio list="#{'1':'Yes','0':'No'}" name="labTest" id="labTest" onclick="openCloseReagentsEquipment(this)"/></span></div>
		<div class="portlet-content"></div>
	</div>

</div>
<div class="column">
	<div class="portlet">

		<div id="reagentsDiv" class="portlet-header notes"><s:text name="stafa.label.regandandsupplies"/></div>
		<div class="portlet-content">
		
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewRegents(false)"/>&nbsp;&nbsp;<label  class="cursor" onclick="addnewRegents(false)"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteReagentsData()"/>&nbsp;&nbsp;<label class="cursor"  onclick="deleteReagentsData()"><b>Delete</b></label>&nbsp;</div></td>
			    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;"onclick="editReagentsRows()"/>&nbsp;&nbsp;<label  class="cursor" onclick="editReagentsRows()"><b>Edit</b></label>&nbsp;</div></td>
				</tr>
			</table>
			<div style="overflow-y:auto;overflow-y:hidden;">
			<table cellpadding="0" cellspacing="0" border="0" id="reagenTable" width="100%"class="display">
				<thead>
					<tr>
						<th width="4%" id="reagenColumn"></th>
						<th colspan="1"><s:text name="stafa.label.regandandsupplies"/></th>
						<th colspan="1"><s:text name="stafa.label.quantity"/></th>
						<th colspan="1"><s:text name="stafa.label.manufacturer"/></th>
						<th colspan="1"><s:text name="stafa.label.lot"/></th>
						<th colspan="1"><s:text name="stafa.label.expirationdate"/></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
			</div>
		</div>
	</div>

</div>
<div class="column">
	<div class="portlet">
 
			<div id="equipmentsDiv" class="portlet-header notes"><s:text name="stafa.label.equipment"/></div>
		<div class="portlet-content" >
			
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewequipment(false);"/>&nbsp;&nbsp;<label  class="cursor" onclick="addnewequipment(false);"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteEquimentData()"/>&nbsp;&nbsp;<label class="cursor"  onclick="deleteEquimentData()"><b>Delete</b></label>&nbsp;</div></td>
			    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" onclick="editEquimentsRows()"/>&nbsp;&nbsp;<label  class="cursor" onclick="editEquimentsRows()"><b>Edit</b></label>&nbsp;</div></td>
				</tr>
			</table>
			<div style="overflow-y:auto;overflow-y:hidden;">
			<table cellpadding="0" cellspacing="0" border="0" id="equipmentTable" class="display">
			<thead>
				<tr>
					<th width="4%" id="equipmentTableColumn"></th>
					<th colspan='1'><s:text name="stafa.label.equipment"/></th>
					<th colspan='1'><s:text name="stafa.label.manufacturer"/></th>
					<th colspan='1'><s:text name="stafa.label.model"/></th>
					<th colspan='1'><s:text name="stafa.label.mhs"/></th>
					<th colspan='1'><s:text name="stafa.label.maintainance_Duedate"/></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			</table>
			</div>
		</div>
	</div>

</div>


<script>
	$(function() {
		$("#tabs").tabs();
		$(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all").find(".portlet-header").addClass(
						"ui-widget-header ui-corner-all").prepend(
						"<span class='ui-icon ui-icon-minusthick'></span>");


		$(".portlet-header .ui-icon").click(function() {
					$(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
					$(this).parents(".portlet:first").find(".portlet-content").toggle();
					
		});

		$(".portlet").find("#splitDiv").find(".ui-icon").trigger("click");
		$(".portlet").find("#poolDiv").find(".ui-icon").trigger("click");
			 
		$(".portlet").find("#reagentsDiv").find(".ui-icon").trigger("click");
		$(".portlet").find("#equipmentsDiv").find(".ui-icon").trigger("click");

		
	});
	
</script>

