<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/placeholder.js"></script>
<script type="text/javascript" src="js/dataSearch.js"></script>
<script type="text/javascript">

$(document).ready(function(){
	hide_slidecontrol();
	hide_slidewidgets();
	constructCurrentProduct(false);
	$("#selectProductTable").dataTable({
				"sPaginationType": "full_numbers",
				"sDom": 'C<"clear">Rlfrtip',
				"aoColumns": [ { "bSortable":false},
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null
				             ],
					"oColVis": {
						"aiExclude": [ 0 ],
						"buttonText": "&nbsp;",
						"bRestore": true,
						"sAlign": "left"
					},
					"fnInitComplete": function () {
						$("#selectProductColumn").html($('#selectProductTable_wrapper .ColVis'));
					}
	});
	$("#searchArchiveTable").dataTable({
		"sPaginationType": "full_numbers",
		"sDom": 'C<"clear">Rlfrtip',
		"aoColumns": [ { "bSortable":false},
		               null,
		               null,
		               null,
		               null,
		               null,
		               null,
		               null
		             ],
			"oColVis": {
				"aiExclude": [ 0 ],
				"buttonText": "&nbsp;",
				"bRestore": true,
				"sAlign": "left"
			},
			"fnInitComplete": function () {
				$("#searchArchiveColumn").html($('#searchArchiveTable_wrapper .ColVis'));
			}
});
	$("select").uniform(); 
});
function getScanData(event,obj){
    if(event.keyCode==13){
      getdata(event);
  }
}
var searchArchProduct="";
function getdata(e){
	searchArchProduct = $("#conformProductSearch").val();
	$("#searchArchiveTable").dataTable().fnDestroy();
	constructSearchArchiveTable(false,searchArchProduct);
}
function constructSearchArchiveTable(flag,searchProduct){
	var criteria = "";
	if(flag){
		var slicedVal = hiddenVal.substring(1);
		criteria+= "and d.pk_donor not in ("+slicedVal+") and (nvl( SPEC_ID ,' ')) like ('%"+searchProduct+"%')";
	}else{
		criteria+="and (nvl( SPEC_ID ,' ')) like ('%"+searchProduct+"%')"
	}
	var searchArchiveTable_serverParam = function ( aoData ) {
																aoData.push( { "name": "application", "value": "stafa"});
																aoData.push( { "name": "module", "value": "Ctl_currentProduct_List"});
																aoData.push( { "name": "criteria", "value": criteria});
																
																aoData.push( { "name": "col_2_name", "value": "lower(nvl( SPEC_ID ,' '))"});
																aoData.push( { "name": "col_2_column", "value": "7"});
																
																aoData.push( {"name": "col_3_name", "value": "lower(nvl((select nvl(person_mrn,' ') from person where pk_person=d.fk_receipent),' '))" } );
																aoData.push( { "name": "col_3_column", "value": "1"});
																
																aoData.push( { "name": "col_4_name", "value": "lower(nvl( (select nvl(PERSON_LNAME, ' ' ) || ' ' ||nvl(PERSON_FNAME, ' ' ) from person where pk_person=d.fk_receipent),' '))"});
																aoData.push( { "name": "col_4_column", "value": "2"});
																
																aoData.push( { "name": "col_5_name", "value": "lower(nvl(p.PERSON_MRN, ' '))"});
																aoData.push( { "name": "col_5_column", "value": "4"});
																
																aoData.push( { "name": "col_6_name", "value": "lower(nvl(p.PERSON_LNAME, ' ' )|| ' ' || nvl(p.PERSON_FNAME, ' ' ))"});
																aoData.push( { "name": "col_6_column", "value": "5"});
																
																aoData.push( { "name": "col_7_name", "value": ""});
																aoData.push( { "name": "col_7_column", "value": ""}); 
													};
	var searchArchiveTable_aoColumnDef = [

											{	 
													"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
												return "";
											}},
											{	 "sTitle":"Check All&nbsp;<input type='checkbox' onclick='checkall(\"currentproductTable\",this);'/>",
													"aTargets": [ 1], "mDataProp": function ( source, type, val ) {
													return "<input name='donor' id= 'donor' type='checkbox' value='" + source[0] +"' /><input name='pkSpecimen' id= 'pkSpecimen' type='hidden' value='" + source[10] +"' />";
											}},
											{    "sTitle":'Product ID',
													"aTargets": [ 2], "mDataProp": function ( source, type, val ) {
													return source[7];
											}},    
											
											{    "sTitle":'Recipient MRN',
													"aTargets": [ 3], "mDataProp": function ( source, type, val ) {
													return source[1];
											}},
											{    "sTitle":'Recipient Name',
													"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
													return source[2];
											}},
											
											{    "sTitle":'<s:text name="stafa.label.donorid"/>',
													"aTargets": [ 5], "mDataProp": function ( source, type, val ) {
													return source[4];
											}}, 
											{    "sTitle":'<s:text name="stafa.label.donorname"/>',
													"aTargets": [ 6], "mDataProp": function ( source, type, val ) {
													return source[5];
											}}, 
											
											{    "sTitle":'Product Status',
													"aTargets": [ 7], "mDataProp": function ( source, type, val ) {
													return source[11];
											}} 
			                        ];
	var searchArchiveTable_aoColumn = [ { "bSortable": false},
	                                    { "bSortable": false},
						               null,
						               null,
						               null,
						               null,
						               null,
						               null
			             ];
	var searchArchiveTable_ColManager = function(){
										$("#searchArchiveColumn").html($('#searchArchiveTable_wrapper .ColVis'));
									 };
	constructTable(flag,'searchArchiveTable',searchArchiveTable_ColManager,searchArchiveTable_serverParam,searchArchiveTable_aoColumnDef,searchArchiveTable_aoColumn);
}
function constructCurrentProduct(flag){
	var criteria = "";
	if(flag){
		var slicedVal = hiddenVal.substring(1);
		criteria+= "and d.pk_donor not in ("+slicedVal+")";
	}
	var currentProductTable_serverParam = function ( aoData ) {
																aoData.push( { "name": "application", "value": "stafa"});
																aoData.push( { "name": "module", "value": "Ctl_currentProduct_List"});
																aoData.push( { "name": "criteria", "value": criteria});
																
																aoData.push( { "name": "col_2_name", "value": "lower(nvl( SPEC_ID ,' '))"});
																aoData.push( { "name": "col_2_column", "value": "7"});
																
																aoData.push( {"name": "col_3_name", "value": "lower(nvl((select nvl(person_mrn,' ') from person where pk_person=d.fk_receipent),' '))" } );
																aoData.push( { "name": "col_3_column", "value": "1"});
																
																aoData.push( { "name": "col_4_name", "value": "lower(nvl( (select nvl(PERSON_LNAME, ' ' ) || ' ' ||nvl(PERSON_FNAME, ' ' ) from person where pk_person=d.fk_receipent),' '))"});
																aoData.push( { "name": "col_4_column", "value": "2"});
																
																aoData.push( { "name": "col_5_name", "value": "lower(nvl(p.PERSON_MRN, ' '))"});
																aoData.push( { "name": "col_5_column", "value": "4"});
																
																aoData.push( { "name": "col_6_name", "value": "lower(nvl(p.PERSON_LNAME, ' ' )|| ' ' || nvl(p.PERSON_FNAME, ' ' ))"});
																aoData.push( { "name": "col_6_column", "value": "5"});
																
																aoData.push( { "name": "col_7_name", "value": ""});
																aoData.push( { "name": "col_7_column", "value": ""}); 
													};
	var currentProductTable_aoColumnDef = [

											{	 
													"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
												return "";
											}},
											{	 "sTitle":"Check All&nbsp;<input type='checkbox' onclick='checkall(\"currentproductTable\",this);'/>",
													"aTargets": [ 1], "mDataProp": function ( source, type, val ) {
													return "<input name='donor' id= 'donor' type='checkbox' value='" + source[0] +"' /><input name='pkSpecimen' id= 'pkSpecimen' type='hidden' value='" + source[10] +"' />";
											}},
											{    "sTitle":'Product ID',
													"aTargets": [ 2], "mDataProp": function ( source, type, val ) {
													return source[7];
											}},    
											
											{    "sTitle":'Recipient MRN',
													"aTargets": [ 3], "mDataProp": function ( source, type, val ) {
													return source[1];
											}},
											{    "sTitle":'Recipient Name',
													"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
													return source[2];
											}},
											
											{    "sTitle":'<s:text name="stafa.label.donorid"/>',
													"aTargets": [ 5], "mDataProp": function ( source, type, val ) {
													return source[4];
											}}, 
											{    "sTitle":'<s:text name="stafa.label.donorname"/>',
													"aTargets": [ 6], "mDataProp": function ( source, type, val ) {
													return source[5];
											}}, 
											
											{    "sTitle":'Product Status',
													"aTargets": [ 7], "mDataProp": function ( source, type, val ) {
													return source[11];
											}} 
			                        ];
	var currentProductTable_aoColumn = [ { "bSortable": false},
	                                     { "bSortable": false},
						               null,
						               null,
						               null,
						               null,
						               null,
						               null
			             ];
	var currentProductTable_ColManager = function(){
										$("#currentproductColumn").html($('#currentproductTable_wrapper .ColVis'));
									 };
	constructTable(flag,'currentproductTable',currentProductTable_ColManager,currentProductTable_serverParam,currentProductTable_aoColumnDef,currentProductTable_aoColumn);
}
var hiddenVal;
var hiddenProID;
var hiddenRecMRN;
var hiddenStatus;
var hiddenSpecimen ;

function addSelectedProducts(tableId){
	hiddenVal = $("#selectedProducts").val();
	hiddenProID = $("#selProductID").val();
	hiddenRecMRN = $("#selRecipMRN").val();
	hiddenStatus = $("#selStatus").val();
	hiddenSpecimen = $("#hiddenSpecId").val();
	
	$("#"+tableId+" tbody tr").each(function (i,row){
		if($(this).find("#donor").is(":checked")){
			$(this).find("td").each(function(col){
				if(col==1){
					if(hiddenVal==","){
						hiddenVal+=$(this).find("#donor").val();
						hiddenSpecimen+=$(this).find("#pkSpecimen").val();
					}else{
						hiddenVal+=","+$(this).find("#donor").val();
						hiddenSpecimen+=","+$(this).find("#pkSpecimen").val();
					}
				}else if(col==2){
					if(hiddenProID==","){
						hiddenProID+=$(this).text();
					}else{
						hiddenProID+=","+$(this).text();
					}
				}else if(col==3){
					if(hiddenRecMRN==","){
						hiddenRecMRN+=$(this).text();
					}else{
						hiddenRecMRN+=","+$(this).text();
					}
				}else if(col==7){
					if(hiddenStatus==","){
						hiddenStatus+=$(this).text();
					}else{
						hiddenStatus+=","+$(this).text();
					}
				}
			});
		}
	});
	if(tableId=="searchArchiveTable"){
		constructSearchArchiveTable(true,searchArchProduct);
	}else{ 
		constructCurrentProduct(true);
	}
	 $("#selectProductTable").dataTable().fnDestroy();
     constructSelectedProduct(false);
    
}
function saveSelProducts(){
	/* var sliceProdVal = hiddenProID.substring(1);
    var sliceRecipMRN = hiddenRecMRN.substring(1);
    var sliceStatus = hiddenStatus.substring(1);
   
    $("#selProductID").val(sliceProdVal);
    $("#selRecipMRN").val(sliceRecipMRN);
    $("#selStatus").val(sliceStatus); */
   
    $("#selectedProducts",window.parent.document).val(hiddenVal);
    $("#selProductID",window.parent.document).val(hiddenProID);
    $("#selRecipMRN",window.parent.document).val(hiddenRecMRN);
    $("#selStatus",window.parent.document).val(hiddenStatus);
    $("#hiddenSpecId",window.parent.document).val(hiddenSpecimen);
    
    /*var proArray = $("#selProductID").val().split(",");
	var mrnArray = $("#selRecipMRN").val().split(",");
	var statusArray = $("#selStatus").val().split(",");
	
	 var newRow = "<tr>"+
				 "<td class='col0'><input type='checkbox' id='ctlTasks' name='ctlTasks' value='0' /></td>"+
				 "<td class='col1' rowspan="+proArray.length+">"+$("#taskspan").html()+"</td>";
	for(i=0;i<proArray.length;i++){
		if(i==0){
		newRow += "<td class='col2'><input type='text' id='fkEntities' value='"+proArray[i]+"'/><img align='right' src = 'images/icons/addnew.jpg' class='cursor' id='' onclick='addNewPopup(\"Select Products\",\"modalpopup\",\"loadDailyAssignmentProduct\",\"700\",\"1050\")'/></td>"+
				  "<td class='col3'><input type='text' id='recipientMRN' value='"+mrnArray[i]+"'/></td>"+
				  "<td class='col4'><input type='text' id='proStatus' value='"+statusArray[i]+"'/></td>"+
				  "<td class='col5' rowspan="+proArray.length+"><input type='hidden' id='codelstStatus' value=''/></td>"+
				  "<td class='col6' rowspan="+proArray.length+" nowrap='nowrap'>"+$("#nurseUserspan").html() + "&nbsp;" +$("#tasklevelspan").html()  +"&nbsp;<img src = 'images/icons/addnew.jpg' class='cursor' id='' onclick='add_users(this)'/></td>"
				  "</tr>";
		}else{
			newRow +="</tr><tr><td class='col0'><input type='checkbox' id='ctlTasks' name='ctlTasks' value='0' /></td>"+
					 "<td class='col2'><input type='text' id='fkEntities' value='"+proArray[i]+"'/></td>"+
					 "<td class='col3'><input type='text' id='recipientMRN' value='"+mrnArray[i]+"'/></td>"+
			  		 "<td class='col4'><input type='text' id='proStatus' value='"+statusArray[i]+"'/></td>"+
			  		 "</tr>";
		}
	} 
	alert(newRow);
    $('#dailyAssignmentTable tbody tr:eq(0)', window.parent.document).replaceWith(newRow);
    $.uniform.restore('select');
  	$("select").uniform();
    $('#selectedMRN',window.parent.document).val(sliceRecipMRN);
    $('#selectedStatus',window.parent.document).val(sliceStatus);*/
    closePopup(); 
    parent.generateSelProducts();
    $("#selectedProducts").val(",");
}
function add_users(rowid){
	var cell = $(rowid).closest('td');
	var row = cell.closest('tr');
	curr_row = row[0].rowIndex-1;
	//var newUsers = $('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(7)").html()+ "<br>"+$("#nurseUserspan").html() + "&nbsp;" +$("#tasklevelspan").html() ;
	var newUsers = "<br>"+$("#nurseUserspan").html() + "&nbsp;" +$("#tasklevelspan").html() ;
	$('#dailyAssignmentTable tbody tr:eq('+curr_row+')', window.parent.document).find("td:eq(6)").append(newUsers);
	$.uniform.restore('select');
	$("select").uniform(); 
}
function closePopup(){
try{
		$("#modalpopup").dialog('close');
  	}catch(err){
		alert("err" + err);
	}
}
function constructSelectedProduct(flag){
	var slicedVal = hiddenVal.substring(1);
    $("#selectedProducts").val(slicedVal);
	var criteria = "and d.pk_donor in ("+slicedVal+")";
	var selectProductTable_serverParam = function ( aoData ) {
											aoData.push( { "name": "application", "value": "stafa"});
											aoData.push( { "name": "module", "value": "Ctl_currentProduct_List"});	
											aoData.push( { "name": "criteria", "value": criteria});
											
											aoData.push( { "name": "col_2_name", "value": "lower(nvl( SPEC_ID ,' '))"});
											aoData.push( { "name": "col_2_column", "value": "7"});
											
											aoData.push( {"name": "col_3_name", "value": "lower(nvl((select nvl(person_mrn,' ') from person where pk_person=d.fk_receipent),' '))" } );
											aoData.push( { "name": "col_3_column", "value": "1"});
											
											aoData.push( { "name": "col_4_name", "value": "lower(nvl( (select nvl(PERSON_LNAME, ' ' ) || ' ' ||nvl(PERSON_FNAME, ' ' ) from person where pk_person=d.fk_receipent),' '))"});
											aoData.push( { "name": "col_4_column", "value": "2"});
											
											aoData.push( { "name": "col_5_name", "value": "lower(nvl(p.PERSON_MRN, ' '))"});
											aoData.push( { "name": "col_5_column", "value": "4"});
											
											aoData.push( { "name": "col_6_name", "value": "lower(nvl(p.PERSON_LNAME, ' ' )|| ' ' || nvl(p.PERSON_FNAME, ' ' ))"});
											aoData.push( { "name": "col_6_column", "value": "5"});
											
											aoData.push( { "name": "col_7_name", "value": ""});
											aoData.push( { "name": "col_7_column", "value": ""}); 
									};
	var selectProductTable_aoColumnDef = [
											{	 
													"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
												return "";
											}},
											{	 "sTitle":"Check All&nbsp;<input type='checkbox' checked='checked'/>",
													"aTargets": [ 1], "mDataProp": function ( source, type, val ) {
													return "<input name='donor' id= 'donor' type='checkbox' checked='chekced' value='" + source[0] +"' /><input name='pkSpecimen' id= 'pkSpecimen' type='hidden' value='" + source[10] +"' />";
											}},
											{    "sTitle":'Product ID',
													"aTargets": [ 2], "mDataProp": function ( source, type, val ) {
													return source[7];
											}},    
											
											{    "sTitle":'Recipient MRN',
													"aTargets": [ 3], "mDataProp": function ( source, type, val ) {
													return source[1];
											}},
											{    "sTitle":'Recipient Name',
													"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
													return source[2];
											}},
											
											{    "sTitle":'<s:text name="stafa.label.donorid"/>',
													"aTargets": [ 5], "mDataProp": function ( source, type, val ) {
													return source[4];
											}}, 
											{    "sTitle":'<s:text name="stafa.label.donorname"/>',
													"aTargets": [ 6], "mDataProp": function ( source, type, val ) {
													return source[5];
											}}, 
											
											{    "sTitle":'Product Status',
													"aTargets": [ 7], "mDataProp": function ( source, type, val ) {
													return source[11];
											}} 
		];
	var selectProductTable_aoColumn = [ { "bSortable": false},
	                                    { "bSortable": false},
										null,
										null,
										null,
										null,
										null,
										null
									];
	var selectProductTable_ColManager = function(){
											$("#selectProductColumn").html($('#selectProductTable_wrapper .ColVis'));
										};
	constructTable(flag,'selectProductTable',selectProductTable_ColManager,selectProductTable_serverParam,selectProductTable_aoColumnDef,selectProductTable_aoColumn);
}

</script>
<form>
<div class="column">
	<div  class="portlet">
	<div class="portlet-header ">Select Products</div>
		<div class="portlet-content">
			<div id="tabs">
				<ul >
					<li><a href="#tabs-1">Current Product</a></li>
				    <li><a href="#tabs-2">Search Archive</a></li>
				</ul>
			  	<div id="tabs-1">   
					<div style="width:100%;height:35px;"></div>
					<div class="tableOverflowDiv">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" id="currentproductTable" class="display">
							<thead>
								<tr>
									<th width="4%" id="currentproductColumn"></th>
									<th>Check All<input type="checkbox" id="" name=""/></th>
									<th width="14%">Product ID</th>
									<th width="14%">Recipient MRN</th>
									<th width="14%">Recipient Name</th>
									<th width="14%">Donor MRN</th>
									<th width="14%">Donor Name</th>
									<th width="14%">Product Status</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div align='right' style='float:right;'>
	                	<table cellpadding='1' cellspacing='1' class='' align='' border='0'>
	                    	<tr><td style="padding:35% 0% 5% 0%"><input type='button' onclick='addSelectedProducts("currentproductTable");' value='Add'/></td></tr>
	               		</table>
              		</div>
				</div>	
				<div id="tabs-2">
					<div id="button_wrapper">
	            		<div style="margin-left:40%;">
       						<input class="scanboxSearch barcodeSearch"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;" onkeyup="" />
							<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" onfocus="changeType(this)"/>
	 					</div>
           			</div>
				  	<div class="tableOverflowDiv">
				  		<table cellpadding="0" cellspacing="0" border="0" id="searchArchiveTable" width="100%" class="display">
							<thead>
								<tr>
									<th width="4%" id="searchArchiveColumn"></th>
									<th>Check All&nbsp;<input type="checkbox" id="" name=""/></th>
									<th width="14%">Product ID</th>
									<th width="14%">Recipient MRN</th>
									<th width="14%">Recipient Name</th>
									<th width="14%">Donor MRN</th>
									<th width="14%">Donor Name</th>
									<th width="14%">Product Status</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div align='right' style='float:right;'>
	                	<table cellpadding='1' cellspacing='1' class='' align='' border='0'>
	                    	<tr><td style="padding:35% 0% 5% 0%"><input type='button' onclick='addSelectedProducts("searchArchiveTable");' value='Add'/></td></tr>
	               		</table>
               		</div>
				</div>
			</div>
		</div>	
	</div>
</div>

<div class="cleaner"></div>

<input type='hidden' id='selectedProducts' value=','/>
<input type='hidden' id='selProductID' value=','/>
<input type='hidden' id='selRecipMRN' value=','/>
<input type='hidden' id='selStatus' value=','/>

<div class="column">
	<div  class="portlet">
	<div class="portlet-header ">Select Product</div>
		<div class="portlet-content">
			<div class="tableOverflowDiv">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" id="selectProductTable" class="display">
					<thead>
						<tr>
							<th width="4%" id="selectProductColumn"></th>
							<th>Check All&nbsp;<input type="checkbox" id="" name=""/></th>
							<th width="14%">Product ID</th>
							<th width="14%">Recipient MRN</th>
							<th width="14%">Recipient Name</th>
							<th width="14%">Donor MRN</th>
							<th width="14%">Donor Name</th>
							<th width="14%">Product Status</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>	
	</div>
</div>
<div align="left" style="float:right;">
    <table width="100%" cellpadding="0" cellspacing="0" class="" align="" border="0">
        <tr>
        <td><span><input type="button" value="Save" onclick="saveSelProducts();"/><input type="button" value="Cancel" onclick=""/></span></td>
        </tr>
    </table>
</div>
</form>
<script>
$(function() {
  $("#tabs").tabs();
  $( ".ui-dialog .portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
  .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
 

  $( ".ui-dialog .portlet-header .ui-icon" ).click(function() {
      $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
      $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
  });
    
});
</script>
