function saveProProcess(tableId){
	var productId=$("#refProductId").val();
	var processType=$('#processType').val();
	var proProcessId=$("#productProcessId").val();
	url="saveProProcess";
	if(typeof(proProcessId) == 'undefined' || proProcessId==""){
		proProcessId=0;
	}
	if(typeof(productId)!='undefined' && productId!=null && productId!=''){
		var questionData=saveProductQuestion(processType,tableId);
		var jsonData="jsonData={refProductId:'"+productId+"',processType:'"+processType+"',productProcessId:'"+proProcessId+"',questionData:["+questionData+"]}";
	}

	var response = jsonDataCall(url,jsonData);
	
	return true;
}
function loadProProcess(productVal,processType){
	var entityId="";
	var url="loadProProcessingDetails";
	if(typeof(productVal) !='undefined' && typeof(processType) !='undefined' && productVal!="" && processType!=""){
		var jsonData = "refProduct:'"+productVal+"',processType:'"+processType+"'";
		response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	}
	if(response.productProcessDetails!='null' && response.productProcessDetails!=''){
		$(response.productProcessDetails).each(function(i){
			if(this.productProcessId!="null"){
				entityId=this.productProcessId;
				$("#productProcessId").val(entityId);
				loadquestions(entityId,processType);
			}else{
				$("#productProcessId").val("0");
				$('input:radio').attr('checked', false);  
				$('input:text').val("");  
			}
		});
		
	}else{
			$("#productProcessId").val("0");
			$('input:radio').attr('checked', false);  
			$('input:text').val("");  
	}
}
function  productAssoPerson(productId){
	if(typeof(productId)!='undefined'){
		url="loadProductAssoPerson";
		response = jsonDataCall(url,"jsonData={productId:"+productId+"}");
		if(response.productPersonDetails != null){
			 $(response.productPersonDetails).each(function () {
				// alert(this[0]);
				 if(this[0]!='null'){
					 $("#donorMRN").html(this[0]);
				 }else{
				 $("#donorMRN").html(''); 
				 }
				 if(this[1]!='null'){
				 $("#recpMRN").html(this[1]);
				 }else{
				 $("#recpMRN").html(''); 
				 }
				 if(this[2]!='null'){
				 $("#donorName").html(this[2] +" "+ this[3]);
				 if(this[3]!='null'){
					 $("#donorName").append(this[3]); 
				 }
				 }else{
				 $("#donorName").html(''); 
				 }
				 if(this[4]!='null'){
				 $("#donorName").html(this[4]);
				 }else{
				 $("#donorName").html(''); 
				 }
				 if(this[5]!='null'){
				 $("#donorAboRh").html(this[5]);
				 }else{
				 $("#donorAboRh").html(''); 
				 }
				 if(this[6]!='null'){
				 $("#recpName").html(this[6]+" ");
				 if(this[7]!='null'){
					 $("#recpName").append(this[7]); 
				 }
				 }else{
				 $("#recpName").html(''); 
				 }
				 if( this[8] !='null'){
				 $("#recpDOB").html(this[8]);
				 }else{
				 $("#recpDOB").html(''); 
				 }
				 if(this[9]!='null'){
				 $("#recpAboRh").html(this[9]);
				 }else{
				 $("#recpAboRh").html(''); 
				 }
				 if(this[10]!='null'){
				 $("#donorAbS").html(this[10]);
				 }else{
				 $("#donorAbS").html(''); 
				 }
				 if(this[11]!='null'){
				 $("#recpAbS").html(this[11]);
				 }else{
				 $("#recpAbS").html(''); 
				 }
				 if(this[12]!='null'){
				 $("#recpDiagnosis").html(this[12]);
				 }else{
				 $("#recpDiagnosis").html(''); 
				 }
				 if(this[13]!='null'){
				 $("#recpWeight").html(this[13]);
				 }else{
				 $("#recpWeight").html(''); 
				 }
				 if(this[14]!='null'){
				 $("#donorType").html(this[14]);
				 }else{
				 $("#donorType").html(''); 
				 }
			 });
		}
	}
}



