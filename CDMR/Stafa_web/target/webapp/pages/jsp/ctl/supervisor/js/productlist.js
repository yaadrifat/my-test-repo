


function savePopup(){
	try{
		var donorMrnLength=$("#donorMRN").val().length;
		var recipientMrnLength=$("#recipientMRN").val().length;
		if( donorMrnLength <=10 && recipientMrnLength <=10){
				if(saveProduct()){
					return true;
					}
			}else{
				alert("Please Enter the MRN with Ten Digits");
				if(donorMrnLength>10){
					$("#donorMRN").val("");
					}
				if(recipientMrnLength>10){
						$("#recipientMRN").val("");
						}
				return false;
				}
	}catch(e){
		alert("exception " + e);
	}
}
function saveProduct(){
	//alert("save donor");
		//if(autoCheck()){
			var url="saveProduct";
			var formName = "productNewForm";
			//alert("form " + $("#"+formName).html());
			response = ajaxCall(url,formName);
			//alert("response " + response.status);
				if(response.status !=""){
				//closePopup();
					constructProductList(true);
				//constructDonorList(true);
					alert("Data saved Successfully");
					closePopup();
				return true;
				}
				
		//}
}

function closePopup(){
	try{
		
		$("#productNewForm").reset();
		$("#modalpopup").dialog('close');
    	//jQuery("#addNewDonor").dialog("destroy");
    	
		//?BB stafaPopUp("close");
		
  	}catch(err){
		alert("err" + err);
	}
}


//var scanPerson;

function setScanFlag(type){
	$("#scanType").val(type);
}

function getScanData(event,obj){
  	if(event.keyCode==13){
  		var scanType = $("#scanType").val();
  		if(scanType=="product" || scanType=="PRODUCT"){
  			searchProduct(scanType);
  		}
  		else if(scanType=="recipient" || scanType=="RECIPIENT"){
  			serachRec_Donor(scanType);
  		}
  		else if(scanType=="donor" || scanType=="DONOR"){
  			serachRec_Donor(scanType);
  		}
  		$("#scanType").val("");
  	}
  	/*	var donorType= $("#donorType").find('option:selected').text();
      if((scanType=="recipient" || scanType=="RECIPIENT" ) && ( donorType != "AUTO")) { 
    	  resetRecipientData();
    	  }else if((scanType=="donor" || scanType=="DONOR" )&&( donorType !="AUTO")){ 
    		  resetDonorData();
    		  }else if(donorType =="AUTO"){ 
    		  resetDonorData();
    		  resetRecipientData();
    		  }
      
      //alert("!"+scanType)
      searchPerson(scanType);
      $("#scanType").val("");
    }*/
}

function searchProduct(type){
	
	try{
		var criteria = " ";
		var productFlag = false;
		var productCriteria;
		
		if(type=="PRODUCT"){
			$("#specId").val($("#divProductScan #fake_conformProductSearch").val() );
			criteria = " and pk_person=fk_person ";
			if($("#specId").val()!=""){
				productCriteria= " lower(SPEC_ID) like lower('%" + $("#specId").val() +"%')";
				productFlag = true;
			}
			if(productFlag){
					criteria += "and (";
					criteria += productCriteria;
					criteria += ")";
			}
		}
		
		var searchProduct_aoColumn = [ { "bSortable": false},
		                               null,
						               null,
						               null,
						               null,
						               null,
						               null
		             ];
		var searchProduct_serverParam = function ( aoData ) {

								aoData.push( { "name": "application", "value": "stafa"});
								aoData.push( { "name": "module", "value": "Ctl_addNewProduct_ProductSearch"});	
								aoData.push( { "name": "criteria", "value": criteria});	
								
								aoData.push( {"name": "col_1_name", "value": "" } );
								aoData.push( { "name": "col_1_column", "value": ""});
								
								aoData.push( { "name": "col_2_name", "value": ""});
								aoData.push( { "name": "col_2_column", "value": ""});
														
					};
		var searchProduct_aoColumnDef = [
								{"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
									return "";
										
								}},
								
								{    "sTitle":'Product ID',
								 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
									 return_id="<input type='hidden' id='' name='specId' value='"+source[0]+"'> ";
									 return_id+=source[0];
							        	return return_id;
									 
								}},
								{    "sTitle":'Recipient Name',
									 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
										 recipientname="<input type='hidden' id='' name='specId' value='"+source[12]+"'> ";
										 recipientname+=source[12];
								        	return recipientname;
										 
									}},
									{    "sTitle":'Recipient MRN',
										 "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
											 recipientmrn="<input type='hidden' id='' name='specId' value='"+source[11]+"'> ";
											 recipientmrn+=source[11];
									        	return recipientmrn;
											 
										}},	
										{    "sTitle":'Donor Name',
											 "aTargets": [4], "mDataProp": function ( source, type, val ) {
												 donorname="<input type='hidden' id='' name='specId' value='"+source[14]+"'> ";
												 donorname+=source[14];
										        	return donorname;
												 
											}},	
								{    "sTitle":'Donor MRN',
											 "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
											 donormrn="<input type='hidden' id='' name='specId' value='"+source[13]+"'> ";
											 donormrn+=source[13];
									     	return donormrn;
													 
												}},	
								{    "sTitle":'Product Type',
								 "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
									 return_protype="<input type='hidden' id='' name='productType' value='"+source[6]+"'> ";
									 return_protype+=source[1];
							        	return return_protype;
								 
								    }},
								{    "sTitle":'Product Status',
								        "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
								        	 return_prostatus="<input type='hidden' id='' name='ProductStatus' value='"+source[7]+"'> ";
								        	 return_prostatus+=source[2];
									        	return return_prostatus;
								        
								  }},
								  {    "sTitle":'Product From',
								        "aTargets": [8], "mDataProp": function ( source, type, val ) {
								        	 return_proform="<input type='hidden' id='' name='ProductForm' value='"+source[8]+"'> ";
								        	 return_proform+=source[3];
									        	return return_proform;
								  }} ,
								{    "sTitle":'Product State',
								        "aTargets": [9], "mDataProp": function ( source, type, val ) {
								        	 return_prostate="<input type='hidden' id='' name='ProductState' value='"+source[9]+"'  > ";
								        	 return_prostate+=source[4];
									        	return return_prostate;
								  }},    
								{    "sTitle":' Donor Type',
								        "aTargets": [10], "mDataProp": function ( source, type, val ) {
								        	 return_donationtype="<input type='hidden' id='' name='donorType' value='"+source[10]+"'> ";
								        	 return_donationtype+=source[5];
									        	return return_donationtype;
								  }}
								];
        
    	var searchProduct_ColManager = function(){
    										$("#productResultsColumn").html($('#productResults_wrapper .ColVis'));
    									 };
    									 
    									 var searchTableObj;
    									 
    									 if(type=="PRODUCT"){
    											$("#productResults").dataTable().fnDestroy();
    											searchTableObj = constructTable(true,'productResults',searchProduct_ColManager,searchProduct_serverParam,searchProduct_aoColumnDef,searchProduct_aoColumn);
    											//alert("obj" + $('#modelPopup').find('#donorResults').html());
    											//constructTableObjectWithoutColManager(true,$('#modelPopup').find('#donorResults'),searchDonor_serverParam,searchDonor_aoColumnDef,search_aoColumn);
    											
    											var emptyCheckClass= $("#productResults tbody tr td:nth-child(1)").attr("class");	
    											//alert("emptyCheckClass " + emptyCheckClass);
    										
    											if(emptyCheckClass=="dataTables_empty"){
    												//$(".donortr").removeClass("hidden");
    												$(".productResultTr").addClass("hidden");
    												 $("#specId").val($("#divProductScan #fake_conformProductSearch").val() );
    											}else{
    												alert("Product Already Exists");
    												$(".productResultTr").removeClass("hidden");
    												$("#specId").val("");
    												//$(".donortr").addClass("hidden");
    											}
    											$(".productResultTr").find("#productResults").removeAttr("style");
    											
    											//$(".donortr").removeClass("hidden");
    										}
    	
		//alert("criteria     --->"+criteria);
	}catch(err){
		alert("error " + err);
	}
}


function serachRec_Donor(type){
	try{
		var criteria = " ";
		var recipientFlag = false;
		var donorFlag = false;
		var mrnCriteria;
		if(type=="RECIPIENT"){
			$("#recipientMRN").val($("#divRecipientScan #fake_conformProductSearch").val());
			criteria = "and p.pk_person=d.fk_receipent ";
			if($("#recipientMRN").val()!=""){
				mrnCriteria= " lower(PERSON_MRN) like lower('%" + $("#recipientMRN").val() +"%')";
				recipientFlag = true;
			}
			if(recipientFlag){
					criteria += "and (";
					criteria += mrnCriteria;
					criteria += ")";
			}
		}
		
		
		var searchReciDonor_aoColumn = [ { "bSortable": false},
		                               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null
		             ];
		
		          
		
		 if(type=="RECIPIENT"){
			 var searchRecipient_serverParam = function ( aoData ) {

					aoData.push( { "name": "application", "value": "stafa"});
					aoData.push( { "name": "module", "value": "Ctl_addNewProduct_RecipientSearch"});	
					aoData.push( { "name": "criteria", "value": criteria});	
					
					aoData.push( {"name": "col_1_name", "value": "" } );
					aoData.push( { "name": "col_1_column", "value": ""});
					
					aoData.push( { "name": "col_2_name", "value": ""});
					aoData.push( { "name": "col_2_column", "value": ""});
											
		};
		
					var searchRecipient_ColManager = function(){
					$("#recipientResultsColumn").html($('#recipientResults_wrapper .ColVis'));
					};
					var searchRecipient_aoColumnDef = [
							{"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
									return "<input type='radio' id='' name='pkrecipient' value='"+source[0] +"' onclick='javascript:setRecipientDetails(this)' > ";
							}},
							{    "sTitle":'Recipient MRN',
							 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
								 return_mrn="<input type='hidden' id='' name='recipientMRN' value='"+source[1] +"'  > ";
						        	return_mrn+=source[1];
						        	return return_mrn;
								
							}},
							{    "sTitle":'Recipient Last Name',
							 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
								 return_lname="<input type='hidden' id='' name='recipientLastName' value='"+source[2] +"' > ";
						        	return_lname+=source[2];
						        	return return_lname;
								
							 
							    }},
							{    "sTitle":'Recipient First Name',
							        "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
							        	return_fname="<input type='hidden' id='' name='recipientFirstName' value='"+source[3] +"' > ";
								        	return_fname+=source[3];
								        	return return_fname;
							  }},
							  {    "sTitle":'Recipient DOB',
							        "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
							        	return_dob="<input type='hidden' id='' name='recipientDob' value='"+source[4] +"' > ";
							        	return_dob+=source[4];
							        	return return_dob;
							  }} ,
							{    "sTitle":'Recipient ABO/Rh',
							        "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
							        	
							        	return_abo="<input type='hidden' id='' name='recipientabo' value='"+source[6] +"' > ";
							        	return_abo+=source[5];
							        	return return_abo;
							  }},    
							{    "sTitle":'Recipient AbS',
							        "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
							        	return_abs="<input type='hidden' id='' name='recipientAbs' value='"+source[8] +"' > ";
							        	return_abs+= source[7];
							        	return return_abs;
							  }},
							{    "sTitle":'Diagnosis',
						        "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
						        	return_diagnosis="<input type='hidden' id='' name='donordiagnosis' value='"+source[10] +"' > ";
						        	return_diagnosis+= source[9];
						        	return return_diagnosis;
						        	
						  }},
							{    "sTitle":'Recipient Weight',
						        "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
						        	return_recweight="<input type='hidden' id='' name='recipientWeight' value='"+source[13] +"' ><input type='hidden' id='' name='recipientUnitsWeight' value='"+source[12] +"' > ";
						        	return_recweight+= source[13] + " "+ source[11];
						        	return return_recweight;
						  }},
							{    "sTitle":'Date of Recipient Weight Taken',
						        "aTargets": [ 9], "mDataProp": function ( source, type, val ) {
						        	return_weighttaken="<input type='hidden' id='' name='recipientWeightTaken' value='"+source[14] +"' > ";
						        	return_weighttaken+=source[14];
						        	return return_weighttaken;
						  }}
							];
				$("#recipientResults").dataTable().fnDestroy();
				searchTableObj =  constructTable(false,'recipientResults',searchRecipient_ColManager,searchRecipient_serverParam,searchRecipient_aoColumnDef,searchReciDonor_aoColumn);
				//constructTableObjectWithoutColManager(true,$('#modelPopup').find('#recipientResults'),searchDonor_serverParam,searchRecipient_aoColumnDef,search_aoColumn);
				var emptyCheckClass= $("#recipientResults tbody tr td:nth-child(1)").attr("class");	
				
				if(emptyCheckClass=="dataTables_empty"){
					$(".recipientResultTr").addClass("hidden");
				}else{
					$(".recipientResultTr").removeClass("hidden");
				}
				
				$(".recipientResultTr").find("#recipientResults").removeAttr("style");
				
			}
			 if(type=="DONOR"){
					 $("#donorMRN").val($("#divDonorScan #fake_conformProductSearch").val());
						criteria = "and  p.pk_person=d.fk_person ";
						if($("#donorMRN").val()!=""){
							mrnCriteria= " lower(PERSON_MRN) like lower('%" + $("#donorMRN").val() +"%')";
							recipientFlag = true;
						}
						if(recipientFlag){
								criteria += "and (";
								criteria += mrnCriteria;
								criteria += ")";
						}
			 var searchDonor_serverParam = function ( aoData ) {

					aoData.push( { "name": "application", "value": "stafa"});
					aoData.push( { "name": "module", "value": "Ctl_addNewProduct_DonorSearch"});	
					aoData.push( { "name": "criteria", "value": criteria});	
					
					aoData.push( {"name": "col_1_name", "value": "" } );
					aoData.push( { "name": "col_1_column", "value": ""});
					
					aoData.push( { "name": "col_2_name", "value": ""});
					aoData.push( { "name": "col_2_column", "value": ""});
											
			 	};
			 	
			 	
			 	var searchDonor_ColManager = function(){
					$("#donorResultsCol").html($('#donorResults_wrapper .ColVis'));
				 };
				 var searchDonor_aoColumnDef = [
					  								{"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
					  										return "<input type='radio' id='' name='' value='"+source[0] +"' onclick='javascript:setDonorDetails(this)' > ";
					  								}},
					  								{    "sTitle":'Donor MRN',
					  								 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
					  									 return_mrn="<input type='hidden' id='' name='donorMRN' value='"+source[1] +"'  > ";
					 						        	return_mrn+=source[1];
					 						        	return return_mrn;
					  								}},
					  								{    "sTitle":'Donor Last Name',
					  								 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
					  									 return_dlname="<input type='hidden' id='' name='donorLastName' value='"+source[3] +"'  > ";
					  									return_dlname+=source[2];
					 						        	return return_dlname;
					  								 
					  								    }},
					  								{    "sTitle":'Donor First Name',
					  								        "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
					  								        	 return_dfname="<input type='hidden' id='' name='donorFirstName' value='"+source[3] +"'  > ";
								  									return_dfname+=source[3];
					  								        	return return_dfname;
					  								  }},
					  								  {    "sTitle":'Donor DOB',
					  								        "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
					  								        	 return_dob="<input type='hidden' id='' name='donorDob' value='"+source[4] +"'  > ";
					  								        	return_dob+=source[4];
					  								        	return return_dob;
					  								  }} ,
					  								{    "sTitle":'Donor ABO/Rh',
					  								        "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
					  								        	
					  								        	return_abo="<input type='hidden' id='' name='donorabo' value='"+source[6] +"' > ";
					  								        	return_abo+=source[5];
					  								        	return return_abo;
					  								  }},    
					  								{    "sTitle":'Donor AbS',
					  								        "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
					  								        	return_abs="<input type='hidden' id='' name='donorAbs' value='"+source[8] +"' > ";
					  								        	return_abs+= source[7];
					  								        	return return_abs;
					  								  }},
					  								{    "sTitle":'Donor Weight',
				  								        "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
				  								        	return_weight="<input type='hidden' id='' name='donorWeight' value='"+source[11] +"' ><input type='hidden' id='' name='donorUnitsWeight' value='"+source[10] +"' >";
				  								        	return_weight+= source[11]+" "+source[9];
				  								        	return return_weight;
				  								        	
				  								  }},
					  								{    "sTitle":'Donor Location',
				  								        "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
				  								      	return_donorlocation="<input type='hidden' id='' name='donorLocation' value='"+source[13] +"' > ";
				  								      	return_donorlocation+=source[12];
			  								        	return return_donorlocation;
				  								  }},
					  								{    "sTitle":'Collection Facility',
				  								        "aTargets": [ 9], "mDataProp": function ( source, type, val ) {
				  								        	return_collection="<input type='hidden' id='' name='collectionfacility' value='"+source[14] +"'  > ";
				  								        	return_collection+=source[14];
						 						        	return return_collection;
				  								  }}
					  								];
			 	
				 $("#donorResults").dataTable().fnDestroy();
					searchTableObj =  constructTable(false,'donorResults',searchDonor_ColManager,searchDonor_serverParam,searchDonor_aoColumnDef,searchReciDonor_aoColumn);
					//constructTableObjectWithoutColManager(true,$('#modelPopup').find('#recipientResults'),searchDonor_serverParam,searchRecipient_aoColumnDef,search_aoColumn);
					var emptyCheckClass= $("#donorResults tbody tr td:nth-child(1)").attr("class");	
					
					if(emptyCheckClass=="dataTables_empty"){
						$(".donorResultTr").addClass("hidden");
					}else{
						$(".donorResultTr").removeClass("hidden");
					}
					
					$(".donorResultTr").find("#donorResults").removeAttr("style");
					
		 }
	}catch(err){
		alert("error " + err);
	}
}
function setDonorDetails(donorobj){
	var donordata=[];
	 $(donorobj).closest('tr').each(function() {		
		var  serialized;
	        var elements = $(this).find('input, textarea, select')
	        if (elements.size() > 0) {
	             serialized = $(this).find('input, textarea, select').serializeFormJSON();
	             donordata.push(serialized);
	          
	        }
	    });
//	var formdata= JSON.stringify(prodata)
	// console.log(donordata);
	    $('table#donordetails').loadJSON(donordata);
	    //console.log(formdata);
	    $.uniform.restore('.ui-dialog select');
		$('.ui-dialog select').uniform();
		$("#donorResults").dataTable().fnDestroy();
		$(".donorResultTr").addClass("hidden");
	
}
function setProductDetails(proobj){
	 var prodata=[];
	 $(proobj).closest('tr').each(function() {		
		var  serialized;
	        var elements = $(this).find('input, textarea, select')
	        if (elements.size() > 0) {
	             serialized = $(this).find('input, textarea, select').serializeFormJSON();
	             prodata.push(serialized);
	          
	        }
	    });
//	var formdata= JSON.stringify(prodata)
	 console.log(prodata);
	    $('table#productdetails').loadJSON(prodata);
	    //console.log(formdata);
	    $.uniform.restore('.ui-dialog select');
		$('.ui-dialog select').uniform();
		$("#productResults").dataTable().fnDestroy();
		$(".productResultTr").addClass("hidden");

}
 function setRecipientDetails(recobj){
	 var data=[];
	 $(recobj).closest('tr').each(function() {		
		var  serialized;
	        var elements = $(this).find('input, textarea, select')
	        if (elements.size() > 0) {
	             serialized = $(this).find('input, textarea, select').serializeFormJSON();
	           
	           data.push(serialized);
	          
	        }
	    });
	// console.log(data);
	    $('table#recipientdetails').loadJSON(data);
	  
	    $.uniform.restore('.ui-dialog select');
		$('.ui-dialog select').uniform();
		$("#recipientResults").dataTable().fnDestroy();
		$(".recipientResultTr").addClass("hidden");
 }
$(function() {
	$.fn.serializeFormJSON = function() {
		var o = {};
		   var a = this.serializeArray();
		   $.each(a, function() {
		       if (o[this.name]) {
		           if (!o[this.name].push) {
		               o[this.name] = [o[this.name]];
		           }
		           o[this.name].push(this.value || '');
		       } else {
		           o[this.name] = this.value || '';
		       }
		   });
		   return o;
		};
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true, changeYear: true, yearRange: '1900:' + new Date().getFullYear(), maxDate: "+1m +1w"});
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" );
});