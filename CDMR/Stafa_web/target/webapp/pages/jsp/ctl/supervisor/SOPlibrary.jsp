<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" >
$(document).ready(function(){
	hide_slidecontrol();
	
	hide_slidewidgets();
	show_innernorth();
	constructSOPLibrary(false);
		
$("select").uniform(); 


});
function constructSOPLibrary(flag){
	//alert("construct ProductList");
	var SOPLibraryTable_serverParam = function ( aoData ) {
																aoData.push( { "name": "application", "value": "stafa"});
																aoData.push( { "name": "module", "value": "Ctl_addNewSOP_SOPLibrary"});	
																
																aoData.push( {"name": "col_1_name", "value": "nvl(s.SOP_NAME, ' ' )" } );
																aoData.push( { "name": "col_1_column", "value": "1"});
																
																aoData.push( { "name": "col_2_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst=s.FK_CODELST_CATEGORY),' '))"});
																aoData.push( { "name": "col_2_column", "value": "2"});
																
																aoData.push( { "name": "col_3_name", "value": "nvl(s.SOP_LINK,' ')"});
																aoData.push( { "name": "col_3_column", "value": "3"});
																
																aoData.push( { "name": "col_4_name", "value": "lower(nvl(s.SOP_ACTIVATE,0))"});
																aoData.push( { "name": "col_4_column", "value": "4"});
																
													};
	var SOPLibraryTable_aoColumnDef = [
			                         {			 "aTargets": [ 0], "mDataProp": function ( source, type, val ) {
			                             			 return "<input name='soplibraryid' id= 'soplibraryid' type='checkbox' value='" + source[0] +"' />";
			                             }},
			                          {    "sTitle":'SOP Name',
			                     	             "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
			                             			 return source[1];
			                             }},
			                          {    "sTitle":'Category',
			                     	              "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
					                             	 return source[2];
			                             }},
			                          {    "sTitle":'Link',
			                                      "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
			                                    	  link="<a href='http://"+source[4]+"' target='_blank'>"+source[4]+"</a>";
			   			                             return link;
			                             }},
			                          {    "sTitle":'Activated',
				                                  "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
				                                	//  alert(source[5]);
				                                	  if (source[5] == 1) {
				              							isActivate = "<input type='radio' name='sopActivate"+source[0]+"' id='sopActivate' value='Yes' checked='checked' >Yes</input>";
				              							isActivate += "<input type='radio' name='sopActivate"+source[0]+"' id='sopActivate' value='No' >No</input>";
				              						} else {
				              							isActivate = "<input type='radio' name='sopActivate"+source[0]+"' id='sopActivate' value='Yes'  >Yes</input>";
				              							isActivate += "<input type='radio' name='sopActivate"+source[0]+"' id='sopActivate' value='No' checked='checked'>No</input>";
				              						}
				              						return isActivate;
							                       //  return source[5];
				                         }}
				                      
			                        ];
	var SOPLibraryTable_aoColumn = [ { "bSortable": false},
	                               	   null,
						               null,
						               null,
						               null
						               
			             ];
	var SOPLibraryTable_ColManager = function(){
										$("#sopDataTableColumn").html($('#sopDataTable_wrapper .ColVis'));
									 };
									 constructTable(flag,'sopDataTable',SOPLibraryTable_ColManager,SOPLibraryTable_serverParam,SOPLibraryTable_aoColumnDef,SOPLibraryTable_aoColumn);
}
function delete_sopLibrary(){
	//alert("delter doc review")
	var deletedIds="";
	var specdeletedIds="";
	try{
	$("#sopDataTable tbody tr").each(function (row){
		 if($(this).find("#soplibraryid").is(":checked")){
			deletedIds += $(this).find("#soplibraryid").val() + ",";
		}
		
	 });
	  if(deletedIds.length >0){
		 var yes=confirm(confirm_deleteMsg);
		 if(!yes){
						return;
		 }
		  deletedIds = deletedIds.substr(0,deletedIds.length-1);
		  jsonData = "{deletedIds:'"+deletedIds+"',domainName:'DOMAIN_SOPLIBRARY',domainKey:'SOPLIBRARY_DOMAINKEY'}";
		 	 url="commonDelete";
		     response = jsonDataCall(url,"jsonData="+jsonData);
		     constructSOPLibrary(true);
	    
	 	
	  }
	}catch(err){
		alert(" error " + err);
	}
}
function savePages(){
	try{
	
				
			if(saveSOPLibrary()){
				return true;
			}
	}catch(e){
		alert("exception " + e);
	}
}
function saveSOPLibrary(){
	var url='saveSOPLibrary';
	
	 var rowData = "";
	 var SopName;
	 var SopCategory;
	 var SopLink;
	 var SopActivate;
	 var SopModule;
	 var Sopid;
	
	var rowHtml="";
	 $("#sopDataTable").find("#sopName").each(function (row){
			 rowHtml =  $(this).closest("tr");
		 		  Sopid=$(rowHtml).find("#soplibraryid").val();
			 	
		 			SopName=$(rowHtml).find("#sopName").val();
		 			SopCategory=$(rowHtml).find("#fkCodelstCategory").val();
		 			SopLink=$(rowHtml).find("#sopLink").val();
		 			SopActivate=$(rowHtml).find("#sopActivate").val();
		 			SopModule=$(rowHtml).find("#sopActivate").val();
			 	
				 	
	 		if(SopName!= ""&& SopName!= undefined){
		 	  rowData+= "{sopLibarary:"+Sopid+",sopName:'"+SopName+"',fkCodelstCategory:'"+SopCategory+"',sopLink:'"+SopLink+"',sopActivate:'"+SopActivate+"',module:'"+SopModule+"'},";
		 	
	 		}
	 		 
	 		
	 });
	 		 if(rowData.length >0){
	             rowData = rowData.substring(0,(rowData.length-1));
	         }
	 		var jsonData = "jsonData={sopData:["+rowData+"]}";

			 var response = jsonDataCall(url,jsonData);
			 alert("Data saved Successfully"); 
			 constructSOPLibrary(true);
			 return true;
	 } 		 

function edit_SopLibrary(){
	  $("#sopDataTable tbody tr").each(function (row){
			 if($(this).find("#soplibraryid").is(":checked")){
				 $(this).find("td").each(function (col){
					 if(col ==1){
						 if ($(this).find("#sopName").val()== undefined){
						 	$(this).html("<input type='text' name='sopName' id='sopName' value='"+$(this).text()+ "'/>");
						 }
					 }else if(col ==2){
						 if ($(this).find("#fkCodelstCategory").val()== undefined){
							 	$(this).html("<input type='text' name='fkCodelstCategory' id='fkCodelstCategory'  value='"+$(this).text()+ "'/>");
							 }
					}else if(col ==3){
						 if ($(this).find("#sopLink").val()== undefined){
							 	$(this).html("<input type='text' name='sopLink' id='sopLink'  value='"+$(this).text()+ "'/>");
							 }
					}
				 });
			 }
		  });
		 // $.uniform.restore('select');
	      $("select").uniform();
}
function addSopLibrary(){
	
	
	var newRow;
	//	 reagents_elementcount +=1;
	
		 newRow="<tr>"+
			"<td><input type='checkbox' id='soplibraryid' name='soplibraryid' value='0'></td>"+
			"<td><input type='text' id='sopName' name='sopName' /></td>"+
			"<td><select class='select1'  id='fkCodelstCategory' name='fkCodelstCategory' >"+$("#sopcategory_hidden").html()+ "</select></td></td>"+
			"<td><input type='text' id='sopLink' name='sopLink' /></td>"+
			"<td><input type='radio' id='sopActivate' name='sopActivate' value='1'>YES<input type='radio' id='sopActivate' name='sopActivate' value='0'>NO</td>"+
			"</tr>";
		$("#sopDataTable tbody").prepend(newRow); 
		
		
		 $("#sopDataTable .ColVis_MasterButton").triggerHandler("click");
		  $('.ColVis_Restore:visible').triggerHandler("click");
		  $("div.ColVis_collectionBackground").trigger("click");
	//$("select").uniform();
    $("select").uniform(); 	
    $( ".dateclass" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
        changeYear: true, yearRange: "c-50:c+45"});
   
	
}

</script>


<div class="hidden">
  		<select name="sopcategory_hidden" id="sopcategory_hidden" >
				<option value="">Select</option>
				<s:iterator value="lstCategory" var="rowVal" status="row">
						<option value='<s:property value="#rowVal.pkCodelst" />'><s:property value="#rowVal.description" /></option>
					
				</s:iterator> 		
			</select>
		
</div>	

<div class="column">
	<div class="portlet">
		<div class="portlet-header ">SOP Library</div>
		<div class="portlet-content">
		<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="addSopLibrary();"/>&nbsp;&nbsp;<label  class="cursor" onclick="addSopLibrary();"><b>Add </b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="delete_sopLibrary()"/>&nbsp;&nbsp;<label class="cursor" onclick="delete_sopLibrary()" ><b>Delete</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/edit.png" class="cursor" style="width:16;height:16;cursor:pointer;" onclick='edit_SopLibrary();'/>&nbsp;&nbsp;<label class="cursor" onclick='edit_SopLibrary()'><b>Edit</b></label>&nbsp;</div></td>
				    
				</tr>
				</table>
			<div id="sopDataTableDiv"
				style="overflow-x: auto; overflow-y: hidden;">
				<table cellpadding="0" cellspacing="0" border="1" id="sopDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='sopDataTableColumn'></th>
							<th>SOP Name</th>
							<th>Category</th>
							<th>Link</th>
							<th>Activated</th>
							
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
$(function() {
  

    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
   
   

    /**For add new popup**/
   
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
    .append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

    $( ".portlet-header .ui-addnew" ).click(function() {
    	
    });
   
    /**For Refresh**/
  

	
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

    
});


</script>