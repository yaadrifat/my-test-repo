<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" >
function savePages(modeType){
    try{
        if(save_dailyAssignment('dailyAssignmentTable')){
            return true;
        }
    }catch(e){
        alert("exception " + e);
    }
}

var dailyAssignmentTable_MergeCols = [1,5,6];
var dailyAssignmentTable_serverParam = function ( aoData ) {

												aoData.push( { "name": "application", "value": "stafa"});
												aoData.push( { "name": "module", "value": "Ctl_DailyAssignment"});	
	};
var dailyAssignmentTable_aoColumn = [ { "bSortable":false},
					               null,
					               null,
					               null,
					               null,
					               null,
					               null
					             ];
var dailyAssignmentTable_sort = [[ 1, "asc" ]];
var dailyAssignmentTable_aoColumnDef = [
			                         { "aTargets": [ 0], "mDataProp": function ( source, type, val ) {
			                             return "<input type='checkbox' id='ctlTasks' value="+source[0]+" /><input name='pkSpecimen' id= 'pkSpecimen' type='hidden' value='" + source[7] +"' />";
			                            	 }},
			                          {    "sTitle":'Task',
			                                 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
			                             		return "<input type='hidden' name='taskGroup' id='taskGroup' value='"+source[10]+"' /><input type='hidden' id='tmpTask' name='tmpTask' value='"+source[6]+"'/><a href='#' onclick='javascript:loadAction(\""+source[11] +"\")'>"+source[3]+"</a>";
			                             	  }},
			                          {    "sTitle":'Product ID',
			                                 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
					                             return source[1];
			                                  }},
			                          {    "sTitle":'Recipient MRN',
			                                        "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
			                                     return source[2];
			                                  }},
			                          {    "sTitle":'Product Status',
			                                        "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
			                                     return source[8];
			                                  }},    
			                          {    "sTitle":'Task Status',
			                                        "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
			                                     return source[9];
			                                  }},       
			                                  
	                                  {    "sTitle":'User Assignment',
										    "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
										    	 var link = "<input type='hidden' id='tmpUser' name='tmpUser' value='"+source[4]+"'/><input type='hidden' id='tmpTaskLevel' name='tmpTaskLevel' value='"+source[5]+"'/>"+ $("#nurseUserspan #primaryId option[value='"+source[4]+"']").text();
										    	 link = link + "&nbsp;" +  $("#tasklevelspan #secondaryId option[value='"+source[5]+"']").text();
                    					         return link;
										 }}
			                        ];
$(document).ready(function(){
	show_innernorth();
	hide_slidecontrol();
	hide_slidewidgets();
	try{
		constructMergeWithoutColManager(false,"dailyAssignmentTable",dailyAssignmentTable_serverParam,dailyAssignmentTable_aoColumnDef,dailyAssignmentTable_aoColumn,dailyAssignmentTable_sort,dailyAssignmentTable_MergeCols);
	}catch(err){
		alert("err");
	}
	$("select").uniform(); 

});
var groupTR = 0;
var currentGrpId;
function addDailyAssignTask(){
	groupTR+=1;
	var addRow= "<tr id='groupId_"+groupTR+"'>"+
					"<td class='col0'><input type='checkbox' id='ctlTasks' name='ctlTasks' value='0'/></td>"+
				    "<td class='col1'><input type='hidden' name='taskGroup' id='taskGroup' value='0' />"+$("#taskspan").html()+"</td>"+
				    "<td class='col2'><img align='right' src = 'images/icons/addnew.jpg' class='cursor' id='groupId_"+groupTR+"' onclick='add_Popup(\"groupId_"+groupTR+"\");'/></td>"+
				    "<td class='col3'></td>"+
				    "<td class='col4'></td>"+
				    "<td class='col5'></td>"+
				    "<td class='col6' nowrap='nowrap'>"+$("#nurseUserspan").html() + "&nbsp;" +$("#tasklevelspan").html()  +"&nbsp;<img src = 'images/icons/addnew.jpg' class='cursor' id='' onclick='add_users(this)'/></td>"+
				"</tr>";
	$("#dailyAssignmentTable tbody").prepend(addRow); 
	$("#dailyAssignmentTable .ColVis_MasterButton").triggerHandler("click");
	$('.ColVis_Restore:visible').triggerHandler("click");
	$("div.ColVis_collectionBackground").trigger("click");
	  
	$.uniform.restore('select');
  	$("select").uniform();
}
function add_Popup(groupId){
	currentGrpId = groupId;
	addNewPopup("Select Products","modalpopup","loadDailyAssignmentProduct","700","1050");
}

var hiddenVal;
var hiddenProID;
var hiddenRecMRN;
var hiddenStatus;
var hiddenSpecimen ;
function generateSelProducts(){
	hiddenVal = $("#selectedProducts").val();
	hiddenProID = $("#selProductID").val();
	hiddenRecMRN = $("#selRecipMRN").val();
	hiddenStatus = $("#selStatus").val();
	hiddenSpecimen = $("#hiddenSpecId").val();
	
	var tableId = "dailyAssignmentTable";
	
	var sliceProdVal = hiddenProID.substring(1);
    var sliceRecipMRN = hiddenRecMRN.substring(1);
    var sliceStatus = hiddenStatus.substring(1);
    var sliceSpecId =  hiddenSpecimen.substring(1);
    
	var proArray = sliceProdVal.split(",");
	var mrnArray = sliceRecipMRN.split(",");
	var statusArray = sliceStatus.split(",");
	var specimenArray = sliceSpecId.split(",");
   
	var length = proArray.length;
	var cell_rowspan = $("#"+tableId+" tbody").find("#"+currentGrpId+" .col1").attr("rowSpan");
	
 	if(cell_rowspan == undefined){
		$("#"+tableId+" tbody").find("#"+currentGrpId+" .col1").attr("rowSpan",(length));
		$("#"+tableId+" tbody").find("#"+currentGrpId+" .col5").attr("rowSpan",(length));
		$("#"+tableId+" tbody").find("#"+currentGrpId+" .col6").attr("rowSpan",(length));
	}else{
		var newlen = parseInt(cell_rowspan) + parseInt(length);
		$("#"+tableId+" tbody").find("#"+currentGrpId+" .col1").attr("rowSpan",(newlen));
		$("#"+tableId+" tbody").find("#"+currentGrpId+" .col5").attr("rowSpan",(newlen));
		$("#"+tableId+" tbody").find("#"+currentGrpId+" .col6").attr("rowSpan",(newlen));
	}
	var newRow = "";
	for(i=0;i<proArray.length;i++){
		if(i==0 && (cell_rowspan==undefined)){
			$("#"+tableId+" tbody").find("#"+currentGrpId+" .col0").html("<input type='checkbox' id='ctlTasks' name='ctlTasks' value='0'/><input type='hidden' id='pkSpecimen' value='"+specimenArray[i]+"'/>");
			$("#"+tableId+" tbody").find("#"+currentGrpId+" .col2").html("<input type='text' id='fkEntities' value='"+proArray[i]+"'/><img align='right' src = 'images/icons/addnew.jpg' class='cursor' id='groupId_"+groupTR+"' onclick='add_Popup(\"groupId_"+groupTR+"\");'/>");
			$("#"+tableId+" tbody").find("#"+currentGrpId+" .col3").html("<input type='text' id='recipientMRN' value='"+mrnArray[i]+"'/>");
			$("#"+tableId+" tbody").find("#"+currentGrpId+" .col4").html("<input type='text' id='proStatus' value='"+statusArray[i]+"'/>");
		}else{
		  newRow+= "<tr><td class='col0'><input type='checkbox' id='ctlTasks' name='ctlTasks' value='0' /><input type='hidden' id='pkSpecimen' value='"+specimenArray[i]+"'/></td>"+
		  		  "<td class='col2'><input type='text' id='fkEntities' value='"+proArray[i]+"'/></td>"+
		 		  "<td class='col3'><input type='text' id='recipientMRN' value='"+mrnArray[i]+"'/></td>"+
		 		  "<td class='col4'><input type='text' id='proStatus' value='"+statusArray[i]+"'/></td>"+
		 		  "</tr>"; 
		} 
	}
		var htm = $("#"+tableId+" tbody").find("#"+currentGrpId).closest("tr");
		$(newRow).insertAfter(htm);
		
		$("#selectedProducts").val(",");
		$("#selProductID").val(",");
		$("#selRecipMRN").val(",");
		$("#selStatus").val(",");
		$("#hiddenSpecId").val(",");
} 
function save_dailyAssignment(table) {
	try{
		var ctlTasks,taskLibrary,fkEntities,productStatus;
		var entityType = "PRODUCT";
		var rowHtml;
		var rowData = "";
		var colObj;
		var curGroup,sameGroup,newGroup;
		var dailyAssignmentRowHtml="";
		curGroup =-1;
		var refCounter=0;
		$("#"+table).find("#ctlTasks").each(function(row) {
							rowHtml = $(this).closest("tr");
							ctlTasks = "", taskLibrary = "",fkEntities="";
							var taskGroup =0;
							if ($(rowHtml).find("td").hasClass("col0")) {
								ctlTasks = $(rowHtml).find("#ctlTasks").val();
								if (ctlTasks == undefined) {
									taskDetails = 0;
								}
								
							}
							
							if ($(rowHtml).find("td").hasClass("col1")) {
								
								taskLibrary = $(rowHtml).find("#taskLibrary").val();
								if (taskLibrary == undefined) {
									taskLibrary = $(rowHtml).find("#tmpTask").val();
								}
								taskGroup=$(rowHtml).find("#taskGroup").val();
								curGroup = taskGroup;
								newGroup = true;
								sameGroup =false;
								refCounter+=1;
								if(taskGroup ==0){
									dailyAssignmentRowHtml+="{dailyAssignments:0,status:"+$("#pendingId").val() +",aliasName:'dailyAssignment_"+refCounter +"'},";
								}
							}else{
								sameGroup =true;
								newGroup = false;
								$(rowHtml).prevAll("tr").each(function(r) {
									 if($(this).find("td").hasClass("col1")){
										 colObj = $(this).find(".col1");
										 taskGroup=$(this).find("#taskGroup").val();
										 taskLibrary = $(this).find("#taskLibrary").val();
										 if (taskLibrary == undefined) {
											 taskLibrary = $(colObj).find("#tmpTask").val();
											}
											return false;
									 }
								});
							}
							if ($(rowHtml).find("td").hasClass("col0")) {
								fkEntities = $(rowHtml).find("#pkSpecimen").val();
								if (fkEntities == undefined) {
									colObj = $(this).find(".col0");
									fkEntities = $(colObj).find("#pkSpecimen").val();
									//alert("fkEntities:;"+fkEntities);
								}
							}
							/* if($(rowHtml).find("td").hasClass("col4")){
								codelstStatus = $(rowHtml).find("#proStatus").val();
								if (primaryId == undefined) {
									primaryId = $(rowHtml).find(".col5").text();
								}
							}else{
								$(rowHtml).prevAll("tr").each(function(r) {
									 if($(this).find("td").hasClass("col4")){
										 colObj = $(this).find(".col4");
										 productStatus = $(this).find("#proStatus").val();
										 if (productStatus == undefined) {
											 productStatus = $(colObj).text();
											}
											return false;
									 }
								});
							} */
						 	if ($(rowHtml).find("td").hasClass("col6")) {
								primaryId = $(rowHtml).find("#primaryId").val();
								if (primaryId == undefined) {
									primaryId = $(rowHtml).find("#tmpUser").val();
								}
								secondaryId = $(rowHtml).find("#secondaryId").val();
								if (secondaryId == undefined) {
									secondaryId = $(rowHtml).find("#tmpTaskLevel").val();
								}
							}else{
								$(rowHtml).prevAll("tr").each(function(r) {
									 if($(this).find("td").hasClass("col6")){
										 colObj = $(this).find(".col6");
										 primaryId = $(this).find("#primaryId").val();
										 secondaryId = $(this).find("#secondaryId").val();
										 if (primaryId == undefined) {
											 primaryId = $(colObj).text();
											}
										 if (secondaryId == undefined) {
											 secondaryId = $(colObj).text();
											}
											return false;
									 }
								});
							} 
							
							
							rowData += "{ctlTasks:'"+ctlTasks
										+"',taskGroup:'"+taskGroup
										+"',taskLibrary:'"+ taskLibrary
										+"',fkEntities:'"+fkEntities
										+"',entityType:'"+entityType
										//+"',productStatus:'"+productStatus
										+"',primaryId:'"+primaryId
										+"',secondaryId:'"+secondaryId+"'";
							if(taskGroup ==0){
								rowData +=",referenceFieldName:'taskGroup',referenceKey:'dailyAssignment_"+refCounter +".dailyAssignments'";
							}		
							rowData +="},";
							
		});
		//alert("row::"+rowData);
		if (rowData.length > 0) {
			rowData = rowData.substring(0, (rowData.length - 1));
			if(dailyAssignmentRowHtml.length > 0) {
				dailyAssignmentRowHtml = dailyAssignmentRowHtml.substring(0, (dailyAssignmentRowHtml.length - 1));
			}
			var url = "saveDailyAssignment";
			response = jsonDataCall(url,"jsonData={taskdata:["+rowData+"],dailyAssignmentData:["+dailyAssignmentRowHtml+"]}");
			alert("Data Saved Successfully");
		}
		constructMergeWithoutColManager(true,"dailyAssignmentTable",dailyAssignmentTable_serverParam,dailyAssignmentTable_aoColumnDef,dailyAssignmentTable_aoColumn,dailyAssignmentTable_sort,dailyAssignmentTable_MergeCols);
	}catch (err) {
		alert("error " + err);
	}
}
function add_users(rowid){
	var cell = $(rowid).closest('td');
	var row = cell.closest('tr');
	curr_row = row[0].rowIndex-1;
	//var newUsers = $('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(7)").html()+ "<br>"+$("#nurseUserspan").html() + "&nbsp;" +$("#tasklevelspan").html() ;
	var newUsers = "<br>"+$("#nurseUserspan").html() + "&nbsp;" +$("#tasklevelspan").html() ;
	$('#dailyAssignmentTable tbody tr:eq('+curr_row+')').find("td:eq(6)").append(newUsers);
	$.uniform.restore('select');
	$("select").uniform(); 
}
/*function editDailyAssignTask(table){
	//alert("Edit Table::"+table);
	var tmpValue;
	var rowHtml;
	var colObj;
	$("#"+table+" tbody tr").each(function(row){
		if ($(this).find("#ctlTasks").is(":checked")) {
			$(this).find("td").each(function (col){
				
			});
		}
	});
	$.uniform.restore('select');
    $("select").uniform();
}*/
function deleteDailyAssignTask(table) {
	//alert("Delete Rows");
	//$('.progress-indicator').css('display', 'block');
	var deletedIds = "";
	try{
		  $("#"+table+" tbody tr").each(function (row){
				 if($(this).find("#ctlTasks").is(":checked")){
					deletedIds += $(this).find("#ctlTasks").val() + ",";
				 }
	});
		  var yes=confirm(confirm_deleteMsg);
			if(!yes){
				return;
			}
		if (deletedIds.length > 0) {

			deletedIds = deletedIds.substr(0, deletedIds.length - 1);

			//alert("deletedIds " + deletedIds);
			jsonData = "{deletedIds:'" + deletedIds + "'}";
			//alert("json data " + jsonData);
			url = "deleteDailyAssignTask";
			response = jsonDataCall(url, "jsonData=" + jsonData);
		}
		constructMergeWithoutColManager(true,"dailyAssignmentTable",dailyAssignmentTable_serverParam,dailyAssignmentTable_aoColumnDef,dailyAssignmentTable_aoColumn,dailyAssignmentTable_sort,dailyAssignmentTable_MergeCols);
	} catch (err) {
		alert("error " + err);
	}
	//?BB showPlusButtons(table);
	//$('.progress-indicator').css('display', 'none');
}
</script>
<span id="hiddendropdowns" class="hidden">
	<span id="nurseUserspan">
		<select id="primaryId"  name="primaryId" >
			<option value="">Select</option> 
			<s:iterator value="lstNurseUsers" var="rowVal" status="row">
			 	<option value="<s:property value="#rowVal[0]"/>"> <s:property value="#rowVal[1]"/> <s:property value="#rowVal[2]"/></option>
			</s:iterator>
		</select>
	</span>
	<span id="taskspan">
	 <s:select id="taskLibrary"  name="taskLibrary" list="lstDailyAssignTask" listKey="taskLibrary" listValue="name"  headerKey="" headerValue="Select" />
	</span>
	<span id="destinationspan"> 
	  <s:select id="destination"  name="destination" list="lstDestination" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" />
	</span>
	<span id="tasklevelspan"> 
	  <s:select id="secondaryId"  name="secondaryId" list="lstTaskLevel" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" />
	</span>
</span>
<form id="dailyAssignmentForm" action="#">
<input type='hidden' id='selectedProducts' value=','/>
<input type='hidden' id='selProductID' value=','/>
<input type='hidden' id='selRecipMRN' value=','/>
<input type='hidden' id='selStatus' value=','/>
<input type='hidden' id='hiddenSpecId' value=','/>
  <s:hidden id="pendingId" name="pendingId" value="%{pendingId}" /> 
 
<div class="column">		
	<div  class="portlet">
	<div class="portlet-header ">Daily Assignment</div>
		<div class="portlet-content">
			<table>
				<tr>
					<!-- <td><input type='button' id='saveBtn' value='Save' onclick="save_dailyAssignment('dailyAssignmentTable')"/></td> -->
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="addDailyAssignTask()"/>&nbsp;&nbsp;<label  class="cursor" onclick="addDailyAssignTask()"><b>Add </b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteDailyAssignTask('dailyAssignmentTable')"/>&nbsp;&nbsp;<label class="cursor" onclick="deleteDailyAssignTask('dailyAssignmentTable')" ><b>Delete</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/edit.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="editDailyAssignTask('dailyAssignmentTable');"/>&nbsp;&nbsp;<label class="cursor" onclick="editDailyAssignTask('dailyAssignmentTable');"><b>Edit</b></label>&nbsp;</div></td>
					<!--  <td style="width:20%"><div><img src = "images/icons/save3.png" class="cursor" style="width:16;height:16;" class="cursor" onclick="saverecurringtask();" >&nbsp;&nbsp;<label class="cursor" onclick="saverecurringtask()"><b>Save</b></label></div></td> -->
				</tr>
			</table>
			<div class='dataTableOverFlow'>
				<table cellpadding="0" cellspacing="0" border="1" id="dailyAssignmentTable" class="display">
					<thead>
						<tr>
							<th width="4%" id="dailyAssignmentColumn"></th>
							<th width="14%">Task</th>
							<th width="14%">Product ID</th>
							<th width="14%">Recipient MRN</th>
							<th width="14%">Product Status</th>
							<th width="14%">Task Status</th>
							<th width="14%">User Assignment</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>	
	</div>
</div>
</form>

<script>
$(function() {
  

    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
   
   

    /**For add new popup**/
   
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
    .append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

    $( ".portlet-header .ui-addnew" ).click(function() {
    	
    });
   
    /**For Refresh**/
  

	
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

    
});


</script>