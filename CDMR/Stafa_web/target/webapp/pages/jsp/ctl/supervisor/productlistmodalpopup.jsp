<%@ include file="../../common/includes.jsp" %>

<script type="text/javascript" src="js/donor.js"></script>
<script type="text/javascript" src="js/barcodeScanBox.js"></script>
<script type="text/javascript" src="js/jquery/jquery.loadjson.js"></script>
<script type="text/javascript" src="jsp/ctl/supervisor/js/productlist.js"></script>





<div id="productlistaddnew" class="model" title="Add New Product" >
      <form action="#" id="productNewForm"><br>
            <input type="hidden" name="tmpproduct" id="tmpproduct" value="0" /> 
            <input type="hidden" name="tmpdonorType" id="tmpdonorType" value="0" />
           <input type="hidden" name="donorPerson" id="donorPerson" value="0" /> 
           <input type="hidden" name="recipientPerson" id="recipientPerson" value="0" />
           <input type="hidden" name="entityType" id="entityType" value="CTL" />
           <input type="hidden" id="scanType" name="scanType" value=""/>
               <table border="0" id="productdetails">
	            <tr>
		            <td colspan="4"> 
			            <div id="button_wrapper">
			        		<div id="divProductScan" style="margin-left:40%;">
			       				 <input class="scanboxSearch barcodeSearch multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;" onkeyup="setScanFlag('PRODUCT');performScanData(event,this)"/>
								<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" onfocus="changeType(this)" />
					 		</div>
				        </div>
				     </td>	
			    </tr>
			    <tr>
               
                <tr>
                 	<td class="tablewapper">Donor Type</td>
                 	<td class="tablewapper1"><s:select id="donorType"  name="donorType" list="lstDonationType" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" class='mandatory' onchange="showRecipientTable(this,'ctl')"/></td>
                    <td class="tablewapper">Product Status</td>
                    <td class="tablewapper1"><s:select id="ProductStatus"  name="ProductStatus" list="lstProductStatus" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" class='mandatory'/></td>
               </tr>
               <tr>
              		<td class="tablewapper">Product Type</td>
              		<td class="tablewapper1"><s:select id="productType"  name="productType" list="lstProductType" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" class='mandatory'/></td>
                  	<td class="tablewapper">Product From</td>
                   <td class="tablewapper1"><s:select id="ProductForm"  name="ProductForm" list="lstProductForm" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" class='mandatory'/></td> 
                </tr>
    			 <tr>
                	<td class="tablewapper">Product ID</td>
                     <td class="tablewapper1"><input type="text" id="specId" name="specId"/></td>
                	<td class="tablewapper">Product State</td>
                     <td class="tablewapper1"><s:select id="ProductState"  name="ProductState" list="lstProductState" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" class='mandatory'/></td>   
                </tr>
                 <tr class="productResultTr hidden " ><td colspan="4" width="100%">
                  <div  class="portlet">
               		 <div class="portlet-header "><label><b>Potential Products</b></label>&nbsp;</div>
                	<div class="portlet-content">
                 <table id="productResults" class="display" width="100%" ><thead>
						<tr>
							<th width="4%" id="productResultsColumn"></th>
							<th>Product ID</th>
							<th>Recipient Name</th>
							<th>Recipient MRN</th>
							<th>Donor Name</th>
							<th>Donor MRN</th>
							<th>Product Type</th>
							<th>Product Status</th>
							<th>Product From</th>
							<th>Product State</th>
							<th>Donor Type</th>
							
							
						</tr>
						
					</thead>
					<tbody>
											
					</tbody></table>
					</div>
					</div>
					</td></tr>
           </table>
            <table border="0" id="recipientdetails">
	            <tr>
		            <td colspan="4"> 
			            <div id="button_wrapper">
			        		<div id="divRecipientScan" style="margin-left:40%;">
			       				 <input class="scanboxSearch barcodeSearch multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Recipient ID"  style="width:210px;" onkeyup="setScanFlag('RECIPIENT');performScanData(event,this);" />
								<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Recipient ID"  style="width:210px;display:none;" onfocus="changeType(this)" />
					 		</div>
				        </div>
				     </td>	
			    </tr>
               
                <tr>
                 	<td class="tablewapper">Recipient MRN</td>
                    <td class="tablewapper1"><input type="text" id="recipientMRN" name="recipientMRN"/></td>
                    <td class="tablewapper">Recipient AbS</td>
                    <td class="tablewapper1"><s:select id="recipientAbs"  name="recipientAbs" list="lstABS" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"  onchange="changeABS('recipient')"/></td>
               </tr>
               <tr>
              		<td class="tablewapper">Recipient Last Name</td>
                    <td class="tablewapper1"><input type="text" id="recipientLastName" name="recipientLastName"/></td>
                  	<td class="tablewapper">Diagnosis</td>
					<td class="tablewapper1"><s:select id="donordiagnosis"  name="donordiagnosis" list="lstDiagonosis" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>      
                </tr>
    			 <tr>
                	<td class="tablewapper">Recipient First Name</td>
                     <td class="tablewapper1"><input type="text" id="recipientFirstName" name="recipientFirstName"/></td>
                	<td class="tablewapper">Recipient Weight</td>
                    <td class="tablewapper1"><input type="text" id="recipientWeight" name="recipientWeight"style="width:55px;" class='decimalTextBox '/>&nbsp;<span class='smallSelector '><s:select id="recipientUnitsWeight"  name="recipientUnitsWeight" list="lstUnitWeight" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeUnit('recipient');" /></span></td>   
                </tr>
                <tr>
                	<td class="tablewapper">Recipient DOB</td>
                     <td class="tablewapper1"><input type="text" id="recipientDob" name="recipientDob" class="dateEntry"/></td>
                	<td class="tablewapper">Date of Recipient Weight Taken</td>
                     <td class="tablewapper1"><input type="text" id="recipientWeightTaken" name="recipientWeightTaken" class="dateEntry"/></td>   
                </tr>
                <tr>
                	<td class="tablewapper">Recipient ABO/Rh</td>
                    <td class="tablewapper1"><s:select id="recipientabo"  name="recipientabo" list="lstABORH" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeABO('recipient');"/></td>
                	
                </tr>
                 <tr class="recipientResultTr hidden" ><td colspan="4" width="100%">
                  <div  class="portlet">
               		 <div class="portlet-header "><label><b>Potential Recipients</b></label>&nbsp;</div>
                	<div class="portlet-content">
                 <table id="recipientResults" class="display " width="100%" ><thead>
						<tr>
							<th width="4%" id="recipientResultsColumn"></th>
							<th>Recipient MRN</th>
							<th>Recipient Last Name</th>
							<th>Recipient First Name</th>
							<th>Recipient DOB</th>
							<th>Recipient ABO/Rh</th>
							<th>Recipient AbS</th>
							<th>Diagnosis</th>
							<th>Recipient Weight</th>
							<th>Date of Recipient Weight Taken</th>
						</tr>
						
					</thead>
					<tbody>
											
					</tbody></table>
					</div>
					</div>
					</td></tr>
                
           </table>
          <table border="0" id="donordetails">
	            <tr>
		            <td colspan="4"> 
			            <div id="button_wrapper">
			        		<div id="divDonorScan" style="margin-left:40%;">
			       				 <input class="scanboxSearch barcodeSearch multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Donor ID"  style="width:210px;" onkeyup="setScanFlag('DONOR');performScanData(event,this)" />
								<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Donor ID"  style="width:210px;display:none;" onfocus="changeType(this)" />
					 		</div>
				        </div>
				     </td>	
			    </tr>
               
                <tr>
                 	<td class="tablewapper">Donor MRN</td>
                    <td class="tablewapper1"><input type="text" id="donorMRN" name="donorMRN"/></td>
                    <td class="tablewapper">Donor AbS</td>
              		<td class="tablewapper1"> <s:select id="donorAbs"  name="donorAbs" list="lstABS" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeABS('donor')"/></td>
               </tr>
               <tr>
              		<td class="tablewapper">Donor Last Name</td>
                    <td class="tablewapper1"><input type="text" id="donorLastName" name="donorLastName"/></td>
                  	<td class="tablewapper">Donor Weight(kg)</td>
                    <td class="tablewapper1"><input type="text" id="donorWeight" name="donorWeight"style="width:55px;" class='decimalTextBox'/>&nbsp;<span class='smallSelector '><s:select id="donorUnitsWeight"  name="donorUnitsWeight" list="lstUnitWeight" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeUnit('donor');"/></span></td>
                </tr>
    			 <tr>
                	<td class="tablewapper">Donor First Name</td>
                     <td class="tablewapper1"><input type="text" id="donorFirstName" name="donorFirstName"/></td>
                	<td class="tablewapper">Donor Location</td>
                 	<td class="tablewapper1"><s:select id="donorLocation"  name="donorLocation" list="lstLocation" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" class='mandatory'/></td>
                     <!-- <td class="tablewapper1"><select id="" name=""><option>Select</option><option>Out Patient</option></select></td> -->   
                </tr>
                <tr>
                	<td class="tablewapper">Donor DOB</td>
                     <td class="tablewapper1"><input type="text" id="donorDob" name="donorDob" class="dateEntry"/></td>
                	<td class="tablewapper">Collection Facility</td>
                	<td class="tablewapper1"><s:select id="collectionfacility"  name="collectionfacility" list="lstCollectionFacility" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" class='mandatory'/></td>
                </tr>
                <tr>
                	<td class="tablewapper">Donor ABO/Rh</td>
                    <td class="tablewapper1"><s:select id="donorabo"  name="donorabo" list="lstABORH" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeABO('donor');"/></td>
                </tr>
                 <tr class="donorResultTr hidden" ><td colspan="4" width="100%">
                  <div  class="portlet">
               		 <div class="portlet-header "><label><b>Potential Donor</b></label>&nbsp;</div>
                	<div class="portlet-content">
                 <table id="donorResults" class="display " width="100%" ><thead>
						<tr>
							<th width="4%" id="donorResultsCol"></th>
							<th>Donor MRN</th>
							<th>Donor AbS</th>
							<th>Donor Last Name</th>
							<th>Donor Weight(kg)</th>
							<th>Donor First Name</th>
							<th>Donor Location</th>
							<th>Donor DOB</th>
							<th>Collection Facility</th>
							<th>Donor ABO/Rh</th>
							
						</tr>
						
					</thead>
					<tbody>
											
					</tbody></table>
					</div>
					</div>
					</td></tr>	
            <tr>
            <td align="right" colspan="4"><input type="password" style="width:80px;" size="5" value="" id="ctleSign" name="ctleSign" placeholder="e-Sign"/>&nbsp;&nbsp;
				<!-- <input type="button" value="Save" onclick="verifyeSign('ctleSign','popup')"/>-->
				<input type="button" value="Save" onclick="saveProduct()"/>
				<input type="button" value="Cancel" onclick="javascript:closePopup()"/>
            <!-- <td align="right" colspan="4"><input type="button" value="Save" onclick="javascript:saveDonor();"/>&nbsp;<input type="button" name="cancel" onclick="javascript:closePopup();" value="Cancel"/> -->
            </td>
            </tr>
            </table>
            
           
   </form>
</div>
<script>

function closePopup(){
	$("#modalpopup").dialog('close');
	
}



</script>