<!DOCTYPE html>
<html >
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
<meta content="utf-8" http-equiv="encoding"/>
<!--<meta http-equiv="X-UA-Compatible" content="IE=10;FF=3;OtherUA=4" /> -->
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7;IE=EmulateIE8; IE=EmulateIE9"/>
<style>
.buttonActive{
background:	#70B8FF;
border-radius:10px;
}
</style>

</head>

<%@ taglib prefix="s" uri="/struts-tags" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script type="text/javascript" src="js/dataSearch.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/monthPicker.js"></script>

<body>

<script type="text/javascript">



/* Inbox Table Server Parameters Starts */
 
     
$(document).ready(function(){
	//$("#chart1").html("");
	hide_slidecontrol();
	hide_slidewidgets();
	hide_innernorth();   
	
	
	// showGraph('Product');
	$("select").uniform();

	var oTable1=$("#inboxTable").dataTable({
		"sPaginationType": "full_numbers",
		"sDom": 'C<"clear">Rlfrtip',
		"aoColumns": [ { "bSortable":false},
		               null,
		               null,
		               null,
		               null

		             ],
		             "sScrollX": "100%",
			"oColVis": {
				"aiExclude": [ 0 ],
				"buttonText": "&nbsp;",
				"bRestore": true,
				"sAlign": "left"
			},
			"fnInitComplete": function () {
		        $("#inboxTableColumn").html($('.ColVis:eq(0)'));
			}
});
	
});
function monthView(){
	$("#mode").val("month");
	$("#weekView").removeClass("buttonActive");
	$("#monthView").addClass("buttonActive");
	$(".week-picker").addClass("hidden");
	$('.monthpicker').monthpicker();
	$('.monthpicker').monthpicker('show');
}
function weekView(){
	$("#mode").val("week");
	$("#monthView").removeClass("buttonActive");
	$("#weekView").addClass("buttonActive");
	$("#monthpicker").removeClass("block");
	$("#monthpicker").addClass("hidden");
	showCalendar();
}
function modeChange(){
	var mode=$("#mode").val();
	if(mode=="week"){
		weekView();	
	}else if(mode=="month"){
		monthView();
	}
}   
function  weekchangeEvent(){
	//alert("update");
	var startDt = $("#startDate").text();
	var endDt = $("#endDate").text();
	
		
}
function showCalendar(){
  	 if($(".week-picker").hasClass("block")){
			$(".week-picker").removeClass("block");
			$(".week-picker").addClass("hidden");
		}else{
			$(".week-picker").removeClass("hidden");
			$(".week-picker").addClass("block");	
    	}
    } 
</script>
	
<div id="homePageDiv">
<div id="firstdiv" class="upperdiv">
    <table cellpadding="0" cellspacing="2" border="0" width="55%">
        <tbody >
            <tr>
                <td width="15%" style="text-align:center;" class="cursor"><img src = "images/icons/dashboard.png" style="width:46px;height:36px; text-align:center;" /></td>
             <td width="15%" style="text-align:center;" class="cursor"><img src = "images/icons/archiveDonor.png" style="width:46px;height:36px;text-align:center;" onclick="showArchiveList();"/></td>
                <td width="15%" style="text-align:center;" class="cursor"><img src = "images/icons/report.png" style="width:46px;height:36px;text-align:center;" onclick="showValidationSheet();"/></td>
                   <td width="15%" style="text-align:center;" class="cursor"><img src = "images/icons/userconfig.png" style="width:46px;height:36px;text-align:center;" onclick=""/></td>
                    <td width="15%" style="text-align:center;" class="cursor"><img src = "images/icons/equiments.png" style="width:46px;height:36px;text-align:center;" onclick=""/></td>
               <!-- procedure.png -->
                 <td width="15%" style="text-align:center;"><img src = "images/icons/usergroup.png" style="width:46px;height:36px;text-align:center;" /></td>
            
            </tr>
            <tr>
                <td width="15%" style="text-align:center;" class="cursor"><b>Dashboard</b></td>
               <td width="15%" style="text-align:center;" onclick="showArchiveList();" class="cursor"><b>Archived Donors</b></td>
                <td width="15%" style="text-align:center;" onclick="showValidationSheet();" class="cursor"><b>Reports</b></td>
                <td width="15%" style="text-align:center;" onclick="" class="cursor"><b>Technician Home</b></td>
              <td width="15%" style="text-align:center;" onclick="" class="cursor"><b>Equipment MT</b></td>
               <td width="15%" style="text-align:center;"><b>Comaiba</b></td>
            
            </tr>
        </tbody>
    </table>
</div>
<div class="divwrapper">
<div class="cleaner"></div>
    <div class="">
        <div class=" commondivleft column">
            <div  class="portlet">
                <div class="portlet-header "><div><img src = "images/icons/dashboard.png" style="width:16px;height:16px;"/>&nbsp;&nbsp;<label><b>Dash Board</b></label>&nbsp;</div></div>
                <div class="portlet-content">
                    <div class="leftdiv">
                        <ul class="ulstyle">   <input type="hidden" id="tmpYear" name="tmpYear" value="" />
                            <li class="listyle productstyle" ><img src = "images/icons/bloodpacknov.png" onclick="showGraph('Product');"  style="width:20px;height:28px;" class="cursoroption"/></li>
                            <li class="listyle productstyle"><span onclick="showGraph('Product');" class="cursoroption" ><b>Product</b></span> </li>
                            <li class="listyle donorstyle"><img src = "images/icons/userconfig.png" onclick="showGraph('Recipient');"  style="width:20px;height:28px;" class="cursoroption"/></li>
                            <li class="listyle donorstyle"><span onclick="showGraph('Donors');" class="cursoroption"><b>Recipient</b></span></li>
                            <li class="listyle nursestyle"><img src = "images/icons/technicians.png" onclick="showGraph('Technicians');"  style="width:20px;height:28px;" class="cursoroption"/></li>
                            <li class="listyle nursestyle"><span onclick="showGraph('Nurse');" class="cursoroption"><b>Technicians</b></span></li>
                            <li class="listyle nursestyle"><img src = "images/icons/bloodpacknov.png" onclick="showGraph('Transplants');"  style="width:20px;height:28px;" class="cursoroption"/></li>
                            <li class="listyle nursestyle"><span onclick="showGraph('Nurse');" class="cursoroption"><b>Transplants</b></span></li>
                        </ul>
                    </div>
                    <span style="padding:1%; margin:0%;vertical-align:calc;" >
                    	<b>Monitoring:</b>
                    </span>
                    <span id="selectedtitle" style="font-weight:bold;">
                    </span>&nbsp;&nbsp;
                    <!-- <input class="monthpicker" data-start-year="2009" data-final-year="2011" data-selected-year="2010" style="display:none;width:30px;"> -->
             
                    <span class="week-picker hidden" style="z-index:1000;position:absolute;" >
                    </span>
                    
                    <span class="cursoroption" style="padding:1%; margin:0%; font-weight:bold;vertical-align:middle;">
                    		 <span id="monthText" class="hidden"><input class="monthpicker" data-start-year="1999" data-final-year="2020"  style="width:60px;"></span>
                    		 <span class="graphDates" onclick="modeChange()">
                    		<span id="startDate" >
                    		</span> - 
                    		<span id="endDate" >
                    		</span>&nbsp;</span>
                    	<input id="weekView"  class="buttonActive" type='button' value='Week' onclick="weekView()">&nbsp;<input ID="monthView" type='button' value='Month' onclick="monthView()">
                    </span>

                    <!-- <div class="rightdiv"><img src = "images/others/barchart.png"   style="height:230px;width:458px" class="cursoroption"/></div> -->
                    <input type="hidden" name="mode" id="mode" value='week' />
                    <div class="rightdiv">
                    <%-- <input type="hidden" name="productData" id="productData" value='<s:property value="productData"/>' />
                    <input type="hidden" name="donorData" id="donorData" value='<s:property value="donorData"/>' />
                    <input type="hidden" name="nurseData" id="nurseData" value='<s:property value="nurseData"/>' />
                    <input type="hidden" name="ticksData" id="ticksData" value='<s:property value="ticksData"/>' />
                    <input type="hidden" name="graphType" id="graphType" value="Product" /> --%>
                    
                   <div id="chart1"></div>
                </div>
            </div>
        </div>
      </div>
      <div class=" commondivright column">       
        <div  class="portlet">
            <div class="portlet-header "><div><img src = "images/icons/email.png" style="width:20px;height:20px;"/>&nbsp;&nbsp;<label><b>Inbox</b></label>&nbsp;</div></div><br>
            <div class="portlet-content">
                <table>
                    <tr>
                        <td style="width:20%"><div class="cursoroption"><img src = "images/icons/no.png" style="width:16px;height:16px;" class="" id ="" onclick="delete_Inbox();"/>&nbsp;&nbsp;<label class=""  onclick="delete_Inbox();"><b>Delete</b></label>&nbsp;</div></td>   
                    </tr>
                </table> 
                <table cellpadding="0" cellspacing="0" border="0" id="inboxTable" class="display">
                    <thead>
                        <tr>
                            <th id="inboxTableColumn" width="4%"></th>
                            <th>Select</th>
                            <th>Name</th>
                            <th>Subject</th>
                            <th width="25%">Date/Time</th>               
                        </tr>
                    </thead>
                    <tbody>
                       
                    </tbody>
                </table>           
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!--   <input type="text" name="monthyear" id="monthyear" value="12/2013" /> -->
<div id="archiveDonorDiv" class='hidden'>
	<div class="column">
		<div  class="portlet">
			<div class="portlet-header ">Archived Donor List</div>
			<div class="portlet-content">
				<form id="reports" name="reports">
	  				<input type="hidden" id="donor" name="donor" value=""/>
	  				<input type="hidden" id="donorProcedure" name="donorProcedure" value=""/>
	  			</form>
				<table cellpadding="0" cellspacing="0" border="0" id="archivedDonorTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='archiveDonorColumn'></th>
							<th>Donor ID</th>
							<th>Donor Name</th>
							<th>Donor DOB</th>
							<th>Product ID</th>
							<th>Collection Date</th>
							<th>Donor Type</th>
							<th>Planned Procedure</th>
							<th>Nurse</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="validationDataDiv" class='hidden'>
<div id="tabs">
	<ul>
		<li><a href="#cannedTab">Canned Report</a></li>
		<li><a href="#adhocTab">Ad Hoc Report</a></li>
	</ul>
	<div class="column">
		<div class="portlet">
			<div class="portlet-header ">Validation Data Sheet</div>
			<div class="portlet-content">
				<div id="validationDataTableDiv" style="overflow-x:auto;overflow-y:hidden;">
					<table cellpadding="0" cellspacing="0" border="1" id="validationDataTable" class="display">
						<thead>
							<tr>
								<th width='4%' id='validationDataTableColumn'></th>
								<th>Donor Name</th>
								<th>Donor Type</th>
								<th>Donor DOB</th>
								<th>Donor Weight(kg)</th>
								<th>Diagnosis</th>
								<th>Recipient Weight(kg)</th>
								<th>Prescription</th>
								<th>Liters Ordered</th>
								<th>Final Inlet(L)</th>
								<th>Collection Day</th>
								<th>Pre WBC</th>
								<th>CD34</th>
								<th>Pre HCT</th>
								<th>Pre %MNC</th>
								<th>Mid-Count WBC</th>
								<th>Product WBC</th>
								<th>Product HCT</th>
								<th>Product %MNC</th>
								<th>CD/Yield</th>
								<th>CD/Cum</th>
								<th>Equipment</th>
								<th>IDM Verified Date</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</body>
<script>

$(function() {
	
	
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" );
	
		/* $( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	}); */
	$( "#tabs" ).tabs();
	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
	/* $("#chart1").mouseenter(function() {
		alert("mouse over");
		return false;
		
	}); */
	
});


</html>
