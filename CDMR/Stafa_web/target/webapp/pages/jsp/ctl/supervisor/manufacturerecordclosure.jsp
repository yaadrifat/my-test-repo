<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/barcodeScanBox.js"></script>
<script type="text/javascript" src="js/ajaxfileupload.js"></script>
<script type="text/javascript" src="jsp/ctl/technicians/js/productTable.js"></script>
<script type="text/javascript" src="jsp/ctl/supervisor/js/productprocess.js"></script>
<script type="text/javascript" >
$(document).ready(function(){
	hide_slidecontrol();
	hide_slidewidgets();
	show_slidewidgets("sidewidgets");
	constructDocumentTable(false,produtVal,enType,'documentreviewTable');
	constructDocumentHistoryTable(false,produtVal,enType,'documentreviewTable1');
});
function constructDocumentTable(flag,product,entityType,tableId) {
		var criteria = "";
		if(product!=null && product!=""){
			criteria = " and fk_Entity ="+product ;
		}
		
		var documentColumn = [ { "bSortable": false},
			                	null,
			                	null,
			                	null,
			                	null,
			                	null
				             ];
													
		var documentServerParams = function ( aoData ) {
										aoData.push( { "name": "application", "value": "stafa"});
										aoData.push( { "name": "module", "value": "aphresis_DocumentReview"});
										aoData.push( { "name": "criteria", "value": criteria});
										
										aoData.push( {"name": "col_1_name", "value": "lower(DOCUMENT_FILENAME)" } );
										aoData.push( { "name": "col_1_column", "value": "lower(DOCUMENT_FILENAME)"});
										
										aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
										aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
													
										
					};
		var documentaoColDef = [
					                  {		
					                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
				                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
				                	 			return "<input type='hidden' id='documentRef' name='documentRef' value='"+source[0] +"' > <input name='docid' id='docid' type='checkbox' value='"+source[0] +"' />" ;
				                	 			//return "";
				                	     }},
					                  { "sTitle": "Document Title",
				                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
					                	    	 //alert('docTitle'+source.documentFileName); 
					                	 		return source[6];
					                	 }},
									  { "sTitle": "Document Date",
					                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
											    return source[2];
											    }},
									  { "sTitle": "Attachment",
											 "aTargets": [3], "mDataProp": function ( source, type, val ) {
										 	   return source[1] + " &nbsp;&nbsp;<a href='#' onclick='documentPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\",\""+entityType+"\",\""+product+"\")'> View </a>";
									  		}},
									  { "sTitle": "Reviewed",
									  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
									  			 if(source[3] ==1){
									  				 return "Reviewed";
									  			 }else{
									  				 return "";
									  			 }
										  		//return "<input type='checkbox'/>";
										  }},
									  { "sTitle": "Version #",
					                		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
											    return source[7];
											    }}
									];
		var documentColManager = function () {
												$("#documentreviewColumn").html($("#"+tableId+"_wrapper .ColVis"));
											};
											
		constructTable(flag,tableId,documentColManager,documentServerParams,documentaoColDef,documentColumn);
	} 
var enType;
function getScanData(event,obj){
  	if(event.keyCode==13){
  	  	if(typeof ($("#scanId").val())=='undefined'){
  	  	constructProProcessList(false);
	  	}else{
	  	constructProProcessList(true);
	  	}
  	    var	productVal=$("#pkSpecimen").val();
  	 	var processType= $('#processType').val();
  		$("#refProductId").val(productVal);
  		enType=$('#entityType').val();
  		constructDocumentTable(true,productVal,enType,'documentreviewTable');
  		constructDocumentHistoryTable(true,productVal,enType,'documentreviewTable1');
  		productAssoPerson(productVal);
  		if($("#slidecontrol").html()!=""){
  	 		show_slidecontrol("slidecontrol");
  	  	}
  		showNotesCount(productVal,"product",0);
  		$("#productNotes").attr("onclick","notes('"+productVal+"','product',this)");
  		loadProProcess(productVal,processType);
  		}
  	}

function constructDocumentHistoryTable(flag,product,type,tableId) {
  	var criteria = "";
 	if(product!=null && product!=""){
 		criteria = " and fk_Entity ="+product ;
 	}
 	var docHistoryaoColumn =  [ { "bSortable": false},
 				                	null,
 				                	null,
 				                	null,
 				                	null,
 				                	null
 					            ];
 	
 	var docHistoryServerParams = function ( aoData ) {
 									aoData.push( { "name": "application", "value": "stafa"});
 									aoData.push( { "name": "module", "value": "aphresis_DocumentReview_History"});
 									aoData.push( { "name": "criteria", "value": criteria});
 									
 									aoData.push( {"name": "col_1_name", "value": "lower(DOCUMENT_FILENAME)" } );
 									aoData.push( { "name": "col_1_column", "value": "lower(DOCUMENT_FILENAME)"});
 									
 									aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
 									aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
 												
 									
 				};
 				
 	var	docHistoryaoColDef = [
 				                  {		
 				                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
 			                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
 			                	 			return "<input type='hidden' id='documentRef' name='documentRef' value='"+source[0] +"' >";
 			                	 			//return "";
 			                	     }},
 				                  { "sTitle": "Document Title",
 			                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
 				                	    	 //alert('docTitle'+source.documentFileName); 
 				                	 		return source[6];
 				                	 }},
 								  { "sTitle": "Document Date",
 				                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
 										    return source[2];
 										    }},
 								  { "sTitle": "Attachment",
 										 "aTargets": [3], "mDataProp": function ( source, type, val ) {
 									 	   return source[1] + " &nbsp;&nbsp;<a href='#' onclick='documentPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\",\""+product+"\",\""+type+"\")'> View </a>";
 								  		}},
 								  { "sTitle": "Reviewed",
 								  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
 								  			 if(source[3] ==1){
 								  				 return "Reviewed";
 								  			 }else{
 								  				 return "";
 								  			 }
 									  		//return "<input type='checkbox'/>";
 									  }},
 								  { "sTitle": "Version #",
 				                		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
 										    return source[7];
 										    }}
 								];
 	
 	var docHistoryColManager =  function () {
 											$("#documentreviewColumn1").html($("#"+tableId+"_wrapper .ColVis"));
 								};
 								
 	constructTable(flag,tableId,docHistoryColManager,docHistoryServerParams,docHistoryaoColDef,docHistoryaoColumn);

 	$("#documentreviewTable1_wrapper").find("#"+tableId).removeAttr("style");
 } 
function save_documentReview(){
	//$('.progress-indicator').css( 'display', 'block' );
	try{
	var documentData = "";
	var productVal = $("#pkSpecimen").val();
	var entityType = $("#entityType").val();
	 $("#documentreviewTable tbody tr").each(function (row){
	 	var docsTitle="";
	 	var docsDate="";
	 	var fileData="";
	 	var fileUploadContentType="";
	 	var reviewedStatus="";
	 	var documentFileName="";
	 	var documentId="";
	 	if($(this).find(".filePathId").val()!=undefined){
		$(this).find("td").each(function (col){
			if(col ==0){
				documentId = $(this).find("#documentRef").val();
			}
			if(col ==1){
				docsTitle = $(this).find("#documentTitle").val();
			}
			if(col ==2){
				docsDate = $(this).find(".dateEntry").val();
			}
			if(col ==3){
				fileData = $(this).find(".filePathId").val();
				fileUploadContentType = $(this).find(".fileUploadContentType").val();
				fileData=fileData.replace(/\\/g,"/");
				documentFileName = $(this).find(".fileName").val();
				if(fileData!=undefined && fileData !="undefined" && fileData != ""){
					fileData = fileData.replace(/\\/g,"/");
				}
			}
		});
		if(fileData != undefined && fileData != ""){
			documentData+="{pkDocumentId:'"+documentId+"',fkEntity:'"+productVal+"',entityType:'"+entityType+"',fkCodelstDocumentType:'"+docsTitle+"',documentFileName:\""+documentFileName+"\",documentUploadedDate:'"+docsDate+"',documentFile:\""+fileData+"\",fileUploadContentType:\""+fileUploadContentType+"\",reviewedStatus:'"+reviewedStatus+"'},";
		}
	 	}
	 });
	 	//alert("documentData " + documentData);
		if(documentData.length >0){
			documentData = documentData.substring(0,(documentData.length-1));
			var url="saveMultipleDocument";
			response = jsonDataCall(url,"jsonData={documentData:["+documentData+"]}");
			//alert("Data Saved Successfully");
			//stafaAlert('Data saved Successfully','');
			$('.progress-indicator').css( 'display', 'none' );
			constructDocumentTable(true,productVal,entityType,'documentreviewTable');
			constructDocumentHistoryTable(true,productVal,enType,'documentreviewTable1');
		}
	}catch(e){
		alert("Error : "+e);
		}
	 return true;
}
var attachFileId=0;
var dateId=0;
function add_documentReview(){
	 var docsTitle = $("#docsTitleTemp").html();
	 //alert(docsTitle);
    var newRow ="<tr>"+
    "<td><input type='hidden' id='documentRef' name='documentRef' value='0'/></td>"+
    "<td><div><select id='documentTitle' name='documentTitle' style='width:200px;'>"+docsTitle+"</select></div></td>"+
    "<td><input type='text' id='documentDate"+dateId+"' name='documentDate' class='dateEntry'/></td>"+
  	 "<td><img src='images/file_loading.gif' id='fileUploadImage"+attachFileId+"' style='display:none'/>"+
  	 "<div id='fileDisplayName"+attachFileId+"'></div><input type='hidden' class='fileName' id='fileName"+attachFileId+"' name='fileName"+attachFileId+"'/><input type='hidden' class='filePathId' name='filePathId"+attachFileId+"' id='filePathId"+attachFileId+"'/> <input type='hidden' class='fileUploadContentType' name='fileUploadContentType"+attachFileId+"' id='fileUploadContentType"+attachFileId+"'/>"+
  	 "<div class='"+attachFileId+"' style='cursor: pointer;	color: #0101DF;' id='displayAttach"+attachFileId+"'></div><input style='display:block' class='"+attachFileId+"' type='file' name='fileUpload' onChange='ajaxFileUpload1(this,\""+attachFileId+"\")' id='attach"+attachFileId+"'/><span><s:text name='stafa.fileupload.message'/></span></td>"+
    //"<td><input type='file' name='fileUpload' id='attachedFile"+attachFileId+"'/></td>"+
    "<td></td>"+
    "</tr>";
    
    $("#documentreviewTable tbody").prepend(newRow);
    $("#documentDate"+dateId).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
        changeYear: true, yearRange: "c-50:c+45"});
    dateId=dateId+1;
    $.uniform.restore('select');
	 $('select').uniform();
	 $("#displayAttach"+attachFileId).click(function(){
		 var idSeq = $(this).attr("class");
		 $("#displayAttach"+attachFileId).hide();
		 $("#attach"+idSeq).click();
	});
	 attachFileId=attachFileId+1;
	 
	/*  var param ="{module:'APHERESIS',page:'VERIFICATION'}";
		markValidation(param); */
		
	 
}
function delete_documentReview(){
	  //  alert("Delete Rows");
	    
	  	  var deletedIds = ""
	  	  $("#documentreviewTable #docid:checked").each(function (row){
	  			//alert($(this).val());
	  			deletedIds += $(this).val() + ",";
	  		 });
	  		 if(deletedIds!=""){
	  			 if(deletedIds.length >0){
	  		  		  deletedIds = deletedIds.substr(0,deletedIds.length-1);
	  		  	  }
	  			var yes=confirm(confirm_deleteMsg);
			  	if(!yes){
			  		return;
			  	}
	  			jsonData = "{deletedIds:'"+deletedIds+"',domainName:'DOMAIN_DOCUMENT',domainKey:'DOCUMENT_ID'}";
	  			url="commonDelete";
	  			response = jsonDataCall(url,"jsonData="+jsonData);
	  			productVal=$("#pkSpecimen").val();
	  	  		enType=$("#entityType").val();
	  	  		constructDocumentHistoryTable(true,productVal,enType,'documentreviewTable1');
	  	  		constructDocumentTable(true,productVal,enType,'documentreviewTable');
		  	}else{
				alert("Please Check to Delete Document");
			}
}
function savePages(){	
	try{
			if(save_documentReview()){
						
			  }  
				if(saveProProcess('manRecordClosure')){
					alert("Data Saved Sucessfully");
				}
				return true;
			
		
	}catch(e){
		alert("exception " + e);
	}
}
</script>
<div  id="slidecontrol" style="display:none;">
	<ul class="ulstyle">
	<li class="listyle notes_icon"  style="margin-left:17%"><img src = "images/icons/notes.png" id="productNotes"  style="width:30px;height:30px;cursor:pointer;"/><sup style="margin-left:2%"><span class='subspan'></span></sup></li>
	<li class="listyle notes_icon"><b><s:text name="stafa.label.notes"/></b> </li>
	</ul>
</div>
<div id="sidewidgets" style="display:none">
	<div  class="portlet">
    	<div class="portlet-header ">Recipient Info</div>
		<div class="portlet-content">
			
							<table width="100%" border="0">
								<tr>
									<td width="50%"><s:text name="stafa.label.recipientmrn"/></td>
									<td width="50%" id="recpMRN">&nbsp;</td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.name"/></td>
									<td id="recpName">&nbsp;</td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.dob"/></td>
									<td id="recpDOB">&nbsp;</td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.donoraborh"/></td>
									<td id="recpAboRh">&nbsp;</td>
								</tr>
								
								<tr>
									<td>ABs</td>
									<td id="recpAbS">&nbsp;</td>
								</tr>
								
								<tr>
									<td>Diagnosis</td>
									<td id="recpDiagnosis">&nbsp;</td>
								</tr>
								<tr>
									<td>Weight</td>
									<td id="recpWeight">&nbsp;</td>
								</tr>
							
							</table>

						
		</div>
	</div>
	<div  class="portlet">
    	<div class="portlet-header ">Donor Info</div>
		<div class="portlet-content">
			
							<table width="100%" border="0">
								<tr>
									<td width="50%"><s:text name="stafa.label.donorid"/></td>
									<td width="50%" id="donorMRN">&nbsp;</td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.name"/></td>
									<td id="donorName">&nbsp;</td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.dob"/></td>
									<td id="donorDOB">&nbsp;</td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.donoraborh"/></td>
									<td id="donorAboRh">&nbsp;</td>
								</tr>
								<tr>
									<td>AbS</td>
									<td id="donorAbS">&nbsp;</td>
								</tr>
								<tr>
									<td>Donation Type</td>
									<td id="donorType">&nbsp;</td>
								</tr>
															
							</table>

						
		</div>
	</div>
</div>

<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Product Table</div>
		<div class="portlet-content">
		<form>
		<input type="hidden" id="entityType" name="entityType" value="PRODUCT"/>
		 <input type="hidden" id="scanId" name="scanId"/>
		<div id="button_wrapper">
			 <div id="divProductScan" style="margin-left:40%;">
			       				 <input class="scanboxSearch barcodeSearch multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Recipient ID"  style="width:210px;" onkeyup="performScanData(event,this)" />
								<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Recipient ID"  style="width:210px;display:none;" onfocus="changeType(this)" />
			</div>
			</div>
		 </form>
			<div id="producDataTableDiv"
				style="overflow-x: auto; overflow-y: hidden;">
				<table cellpadding="0" cellspacing="0" border="1"
					id="productDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='productDataTableColumn'>Check All</th>
							<th>Product ID</th>
							<th>Recipient Name</th>
							<th>Recipient ID</th>
							<th>Donor Name</th>
							<th>Donor ID</th>
							<th>Volume(mL)</th>
							<th>Donation Type</th>
							<th>Product Status</th>
						</tr>
					</thead>
					</table>
			</div>
		</div>
	</div>
</div>

<span class="hidden"><s:select id="docsTitleTemp" style="display:none;" name="docsTitleTemp" list="ctlDocumentType" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></span>
<div class="cleaner"></div>
<div class="column">
		<div  class="portlet">
			<div class="portlet-header ">Document Review</div>
				<div class="portlet-content">
					
				<div id="tabs">
				    <ul >
				     <li><a href="#tabs-1">Current</a></li>
				      <li><a href="#tabs-2">Historic</a></li>
				    </ul>
			  <div id="tabs-1">   
				<br>
				<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="add_documentReview()"/>&nbsp;&nbsp;<label  class="cursor" onclick="add_documentReview()"><b>Add </b></label>&nbsp;</div></td>
 				<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="delete_documentReview()" />&nbsp;&nbsp;<label class="cursor" onclick="delete_documentReview()"><b>Delete</b></label>&nbsp;</div></td> 
					<!-- <td style="width:20%"><div><img src = "images/icons/save3.png" style="width:16;height:16;" class="cursor" id ="" onclick="save_documentReview();" >&nbsp;&nbsp;<label id="schild" class="cursor" onclick="save_documentReview();"><b>Save</b></label>&nbsp;</div></td> -->
				</tr>
				</table>
			<s:form id="attachedDocsForm" name="attachedDocsForm" >
				
				<div class="tableOverflowDiv" style="width:100%">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" id="documentreviewTable" class="display">
					<thead>
						<tr>
							<th width="4%" id="documentreviewColumn"></th>
							<th>Document Title</th>
							<th>Document Date</th>
							<th>Attachment</th>
							<th>Reviewed</th>
							<th>Version #</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				</div>
				</s:form>
				</div>	
			<div id="tabs-2">
			 
			  <div class="tableOverflowDiv" style="width:100%">
			  <table cellpadding="0" cellspacing="0" border="0" id="documentreviewTable1" width="100%" class="display">
					<thead>
						<tr>
							<th width="4%" id="documentreviewColumn1"></th>
							<th>Document Title</th>
							<th>Document Date</th>
							<th>Attachment</th>
							<th>Reviewed</th>
							<th>Version #</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				</div>
			</div>	
			</div>
				
				</div>	
			</div>
</div>

<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Product Manufacturing Record</div>
		<div class="portlet-content"></div>
	</div>

</div>

<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Closure Sign-Off</div>
		<div class="portlet-content">
		<form>
		<input type="hidden" name="refProductId" id="refProductId"/>
		<input type="hidden" id="processType" name="processType" value="MANUFACRECORD"/>
		<input type="hidden" id="productProcessId" name="productProcessId" />
		<table cellpadding="0" cellspacing="0" border="0" id="manRecordClosure" width="100%" class="">
			
			<tr>	
			    <td class="tdclosurecaptionAlign">Donor IDM Completed Within 30(7 Days for TC) of Collection</td>
				<td><input type="hidden" name="donorIDMComplete_id" id="donorIDMComplete_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="donorIDMComplete" />
				<input type="radio" id="donorIDMComplete_yes" name="donorIDMComplete" value="1"/>Yes</td>
				<td><input type="radio" id="donorIDMComplete_no" name="donorIDMComplete" value="0"/>No</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="tdclosurecaptionAlign">Donor IDM Results</td>
				<td><input type="hidden" name="donorIDMResult_id" id="donorIDMResult_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="donorIDMResult" />
				<input type="radio" id="donorIDMResult_neg" name="donorIDMResult" value="0" />Negative</td>
				<td><input type="radio" id="donorIDMResult_negpos" name="donorIDMResult" value="2" />Negative(CMV Positive)</td>
				<td><input type="radio" id="donorIDMResult_pos" name="donorIDMResult" value="1"/>Positive</td>
			</tr>
			<tr>
				<td class="tdcaptionAlign">SOP Followed</td>
				<td><input type="hidden" name="sopFollowed_id" id="sopFollowed_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="sopFollowed" />
				<input type="radio" id="sopFollowed_no" name="sopFollowed" value="0"/>No Significant Deviation</td>
				<td><input type="radio" id="sopFollowed_yes" name="sopFollowed" value="1"/>Significiant Deviation</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="tdclosurecaptionAlign">Cell Processing Lab Target Values</td>
				<td><input type="hidden" name="cellProcessLabTarget_id" id="cellProcessLabTarget_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="cellProcessLabTarget" />
				<input type="radio" id="cellProcessLabTarget_no" name="cellProcessLabTarget" value="0"/>No Significant Deviation</td>
				<td><input type="radio" id="cellProcessLabTarget_yes" name="cellProcessLabTarget" value="1"/>Significiant Deviation</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="tdclosurecaptionAlign">Pathology Review</td>
				<td><input type="hidden" name="pathologyreview_id" id="pathologyreview_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="pathologyreview" />
				<input type="radio" id="pathologyreview_no" name="pathologyreview" value="0"/>No Malignant Cell Identified</td>
				<td><input type="radio" id="pathologyreview_yes" name="pathologyreview" value="1"/>Malignant Cell Identified</td>
				<td><input type="radio" id="pathologyreview_na" name="pathologyreview" value="2"/>N/A</td>
			</tr>
			<tr>
				<td class="tdclosurecaptionAlign">Pre-Processing Microbial Results</td>
				<td><input type="hidden" name="preProcMicroResult_id" id="preProcMicroResult_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="preProcMicroResult" />
				 <input type="radio" id="preProcMicroResult_neg" name="preProcMicroResult" value="0"/>Negative</td>
				<td><input type="radio" id="preProcMicroResult_pos" name="preProcMicroResult" value="1"/>Positive</td>
				<td><input type="radio" id="preProcMicroResult_na" name="preProcMicroResult" value="2"/>N/A</td>
			</tr>
			<tr>
				<td class="tdclosurecaptionAlign">Post-Processing Microbial Results</td>
				<td><input type="hidden" name="postProcMicroResult_id" id="postProcMicroResult_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="postProcMicroResult" />
				 <input type="radio" id="postProcMicroResult_neg" name="postProcMicroResult" value="0"/>Negative</td>
				<td><input type="radio" id="postProcMicroResult_pos" name="postProcMicroResult" value="1"/>Positive</td>
				<td><input type="radio" id="postProcMicroResult_pen" name="postProcMicroResult" value="2"/>Pending</td>
			</tr>
			<tr>
				<td class="tdclosurecaptionAlign">All Manufacturing Record Reviewed</td>
				<td><input type="hidden" name="allManufacRecRev_id" id="allManufacRecRev_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="allManufacRecRev" />
				<input type="radio" id="allManufacRecRev_yes" name="allManufacRecRev" value="1"/>Yes</td>
				<td><input type="radio" id="allManufacRecRev_no" name="allManufacRecRev" value="0"/>No</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="tdclosurecaptionAlign">Processing Facility Direction/Designee Sign-Off</td>
				<td><input type="hidden" name="PFDorDesignSignOff_id" id="PFDorDesignSignOff_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="PFDorDesignSignOff" />
				<input type="text" id="PFDorDesignSignOff" name="PFDorDesignSignOff" />
				</td>
				<td colspan="2">&nbsp;</td>
			</tr>
		</table>
		</form>
	</div>
</div>
</div>
<div align="left" style="float:right;">
		<form onsubmit="return avoidFormSubmitting()">
				<table cellpadding="0" cellspacing="0" class="" align="" border="0">
				<tr>
				<td>
				<input type="password" style="width:80px;" size="5" value="" id="eSign" name="eSign" placeholder="e-Sign"/>&nbsp;&nbsp;
				<input type="button" value="Save" onclick="verifyeSign('eSign','save')"/></td>
				</tr>
				</table>
		</form>
		</div>
<script>
	$(function() {
		$("#tabs").tabs();
		$(".portlet").addClass(
				"ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
				.find(".portlet-header").addClass(
						"ui-widget-header ui-corner-all").prepend(
						"<span class='ui-icon ui-icon-minusthick'></span>");

		/**For add new popup**/

		$(".portlet")
				.addClass(
						"ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
				.find(".addnew")
				.append(
						"<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

		$(".portlet-header .ui-addnew").click(function() {

		});

		/**For Refresh**/

		$(".portlet-header .ui-icon").click(
				function() {
					$(this).toggleClass("ui-icon-minusthick").toggleClass(
							"ui-icon-plusthick");
					$(this).parents(".portlet:first").find(".portlet-content")
							.toggle();
				});

	});
</script>

