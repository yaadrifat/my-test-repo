<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" >
$(document).ready(function(){
	hide_slidecontrol();
	hide_slidewidgets();
	constructProductList(false);
	$("select").uniform(); 

});
function delete_productlist(){
	//alert("delter doc review")
	var deletedIds="";
	var specdeletedIds="";
	try{
	$("#proListDataTable tbody tr").each(function (row){
		 if($(this).find("#donor").is(":checked")){
			deletedIds += $(this).find("#donor").val() + ",";
			specdeletedIds += $("#proListDataTable tbody tr td").find("#pkSpecimen").val() + ",";
		}
		
	 });
	
	  if(deletedIds.length >0){
		 var yes=confirm(confirm_deleteMsg);
		 if(!yes){
						return;
		 }
		  deletedIds = deletedIds.substr(0,deletedIds.length-1);
		  specdeletedIds=specdeletedIds.substr(0,specdeletedIds.length-1)
		  jsonData = "{deletedIds:'"+deletedIds+"',domainName:'DOMAIN_DONOR',domainKey:'DONOR_DOMAINKEY'}";
		  spec_jsonData = "{deletedIds:'"+specdeletedIds+"',domainName:'SPECIMEN',domainKey:'SPECIMEN_ID'}";
		 	 url="commonDelete";
		     response = jsonDataCall(url,"jsonData="+jsonData);
		     response = jsonDataCall(url,"jsonData="+spec_jsonData);
		constructProductList(true);
	    
	 	
	  }
	}catch(err){
		alert(" error " + err);
	}
}

function constructProductList(flag){
	//alert("construct ProductList");
	var productListTable_serverParam = function ( aoData ) {
																aoData.push( { "name": "application", "value": "stafa"});
																aoData.push( { "name": "module", "value": "Ctl_currentProduct_List"});	
																
																aoData.push( {"name": "col_1_name", "value": "lower(nvl((select nvl(person_mrn,' ') from person where pk_person=d.fk_receipent),' '))" } );
																aoData.push( { "name": "col_1_column", "value": "1"});
																
																aoData.push( { "name": "col_2_name", "value": "lower(nvl( (select nvl(PERSON_LNAME, ' ' ) || ' ' ||nvl(PERSON_FNAME, ' ' ) from person where pk_person=d.fk_receipent),' '))"});
																aoData.push( { "name": "col_2_column", "value": "2"});
																
																aoData.push( { "name": "col_3_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst = (select fk_codelst_bloodgrp from person where pk_person=d.fk_receipent)),' '))"});
																aoData.push( { "name": "col_3_column", "value": "3"});
																
																aoData.push( { "name": "col_4_name", "value": "lower(nvl(p.PERSON_MRN, ' '))"});
																aoData.push( { "name": "col_4_column", "value": "4"});
																
																aoData.push( { "name": "col_5_name", "value": "lower(nvl(p.PERSON_LNAME, ' ' )|| ' ' || nvl(p.PERSON_FNAME, ' ' ))"});
																aoData.push( { "name": "col_5_column", "value": "5"});
																
																aoData.push( { "name": "col_6_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst = p.fk_codelst_bloodgrp),0))"});
																aoData.push( { "name": "col_6_column", "value": "6"});
																
																aoData.push( { "name": "col_7_name", "value": "lower(nvl( SPEC_ID ,' '))"});
																aoData.push( { "name": "col_7_column", "value": "7"});
																
																aoData.push( { "name": "col_8_name", "value": "lower(nvl((select CODELST_DESC from er_codelst where PK_CODELST=d.FK_CODELSTPRODUCTTYPE),' '))"});
																aoData.push( { "name": "col_8_column", "value": "8"});
																
																aoData.push( { "name": "col_9_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst = d.fk_codelstdonortype),' '))"});
																aoData.push( { "name": "col_9_column", "value": "9"});
																
																aoData.push( { "name": "col_10_name", "value": ""});
																aoData.push( { "name": "col_10_column", "value": ""});
													};
	var productListTable_aoColumnDef = [
			                         {			 "aTargets": [ 0], "mDataProp": function ( source, type, val ) {
			                             			 return "<input name='donor' id= 'donor' type='checkbox' value='" + source[0] +"' /><input name='pkSpecimen' id= 'pkSpecimen' type='hidden' value='" + source[10] +"' />";
			                             }},
			                          {    "sTitle":'Recipient ID',
			                     	             "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
			                             			 return source[1];
			                             }},
			                          {    "sTitle":'Recipient Name',
			                     	              "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
					                             	 return source[2];
			                             }},
			                          {    "sTitle":'Recipient ABO/Rh',
			                                      "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
			   			                             return source[3];
			                             }},
			                          {    "sTitle":'<s:text name="stafa.label.donorid"/>',
				                                  "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
							                         return source[4];
				                         }}, 
				                      {    "sTitle":'<s:text name="stafa.label.donorname"/>',
				                                  "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
							                         return source[5];
				                         }}, 
				                              
			                          {    "sTitle":'Donor ABO/Rh',
			                                      "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
			   			                             return source[6];
			                             }},    
			                          {    "sTitle":'Product ID',
			                                      "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
			   			                             return source[7];
			                             }},    
			                          {    "sTitle":'Product Type',
			                                      "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
			   			                             return source[8];
			                             }},  
			                          {    "sTitle":'Donor Type',
			                                      "aTargets": [ 9], "mDataProp": function ( source, type, val ) {
			   			                             return source[9];
			                             }},    
			                          {    "sTitle":'Product Status',
			                                      "aTargets": [ 10], "mDataProp": function ( source, type, val ) {
			   			                             return source[11];
			                             }} 
			                        ];
	var productListTable_aoColumn = [ { "bSortable": false},
	                               	   null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null
			             ];
	var productListTable_ColManager = function(){
										$("#proListDataTableColumn").html($('#proListDataTable_wrapper .ColVis'));
									 };
	constructTable(flag,'proListDataTable',productListTable_ColManager,productListTable_serverParam,productListTable_aoColumnDef,productListTable_aoColumn);
}
</script>

<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Current Product List</div>
		<div class="portlet-content">
		<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  style="width:16;height:16;cursor:pointer;" onclick='addNewPopup("Add New Product","modalpopup","loadAddNewProduct","700","1050")'/>&nbsp;&nbsp;<label class="cursor"  onclick='addNewPopup("Add New Product","modalpopup","loadAddNewProduct","700","1050")'><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="delete_productlist();"/>&nbsp;&nbsp;<label class="cursor" onclick="delete_productlist();"><b>Delete</b></label>&nbsp;</div></td>
				  
				</tr>
			</table>
			<div id="productListDataTableDiv"
				style="overflow-x: auto; overflow-y: hidden;">
				<table cellpadding="0" cellspacing="0" border="1" id="proListDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='proListDataTableColumn'></th>
							<th>Recipient ID</th>
							<th>Recipient Name</th>
							<th>Recipient ABO/Rh</th>
							<th>Donor ID</th>
							<th>Donor Name</th>
							<th>Donor ABO/Rh</th>
							<th>Product ID</th>
							<th>Product Type</th>
							<th>Donor Type</th>
							<th>Product Status</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
$(function() {
  
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
   
    /**For add new popup**/
   
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
    .append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

    $( ".portlet-header .ui-addnew" ).click(function() {
    	addNewPopup("Add New Product","modalpopup","loadAddNewProduct","700","1050");
    });
   
    /**For Refresh**/
	
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

    
});
</script>