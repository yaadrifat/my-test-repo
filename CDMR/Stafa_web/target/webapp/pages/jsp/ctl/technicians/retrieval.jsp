

<%@ include file="../../common/includes.jsp" %>

<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/barcodeScanBox.js"></script>

<script type="text/javascript" src="js/jquery/jquery.loadjson.js"></script>
 <style type="text/css">
       /*  body { font-family:Lucida Sans, Lucida Sans Unicode, Arial, Sans-Serif; font-size:13px; margin:0px auto;} */
        .tabs { margin:0; padding:0; list-style:none; overflow:hidden; }
        .tabs li { float:left; display:block; padding:5px; background-color:#bbb; margin-right:5px;}
        .tabs li a { color:#fff; text-decoration:none; }
        .tabs li.current { background-color:#e1e1e1;}
        .tabs li.current a { color:#000; text-decoration:none; }
        .tabs li a.remove { color:#f00; margin-left:10px;}
        .content { background-color:#FFFFFF;}
        .content p { margin: 0; padding:20px 20px 100px 20px;}
        
        .main { width:900px; margin:0px auto; overflow:hidden;background-color:#F6F6F6; margin-top:20px;
             -moz-border-radius:10px;  -webkit-border-radius:10px; padding:30px;}
        .wrapper, #doclist { float:left; margin:0 20px 0 0;}
        .doclist { width:150px; border-right:solid 1px #dcdcdc;}
        .doclist ul { margin:0; list-style:none;}
        .doclist li { margin:10px 0; padding:0;}
        .documents { margin:0; padding:0;}
        
        .wrapper { width:700px; margin-top:20px;}
            
        

    </style>
<script type="text/javascript" >
show_innernorth();

function constructProductList(flag,criteriaValue){
	var criteria = " ";
	



	$("#scanId").val($("#divProductScan #fake_conformProductSearch").val() );
	//alert($("#scanId").val());
	criteria = " ";
	if($("#scanId").val()!=""){
		//productCriteria= " lower(SPEC_ID) like lower('%"+ criteriaValue +"%')";
		criteria= " or lower(SPEC_ID) like lower('%" + $("#scanId").val() +"%')";
		
	}
		//$("#scanId").val($("#divProductScan #fake_conformProductSearch").val() );
		//criteria = " and SPEC_ID in ("+criteriaValue+")";
	

	var productDataTable_serverParam = function ( aoData ) {
																aoData.push( { "name": "application", "value": "stafa"});
																aoData.push( { "name": "module", "value": "Ctl_reviewProduct_List"});
																aoData.push( { "name": "criteria", "value": criteria});	
																
																aoData.push( { "name": "col_1_name", "value": "lower(nvl( SPEC_ID ,' '))"});
																aoData.push( { "name": "col_1_column", "value": "7"});
																
																aoData.push( { "name": "col_2_name", "value": "lower(nvl( (select nvl(PERSON_LNAME, ' ' ) || ' ' ||nvl(PERSON_FNAME, ' ' ) from person where pk_person=d.fk_receipent),' '))"});
																aoData.push( { "name": "col_2_column", "value": "2"});
																
																aoData.push( {"name": "col_3_name", "value": "lower(nvl((select nvl(person_mrn,' ') from person where pk_person=d.fk_receipent),' '))" } );
																aoData.push( { "name": "col_3_column", "value": "1"});

																aoData.push( { "name": "param_count", "value":"2"});
																aoData.push( { "name": "param_1_name", "value":":dailyAssignment:"});
																aoData.push( { "name": "param_1_value", "value":$("#dailyAssignment").val()}); 
																
																aoData.push( { "name": "param_2_name", "value":":criteria:"});
																aoData.push( { "name": "param_2_value", "value":criteria}); 
																 
																
															/* 	aoData.push( { "name": "col_4_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst = (select fk_codelst_bloodgrp from person where pk_person=d.fk_receipent)),' '))"});
																aoData.push( { "name": "col_4_column", "value": "3"});
																
																aoData.push( { "name": "col_4_name", "value": "lower(nvl(p.PERSON_MRN, ' '))"});
																aoData.push( { "name": "col_4_column", "value": "4"});
																
																aoData.push( { "name": "col_5_name", "value": "lower(nvl(p.PERSON_LNAME, ' ' )|| ' ' || nvl(p.PERSON_FNAME, ' ' ))"});
																aoData.push( { "name": "col_5_column", "value": "5"});
																
																aoData.push( { "name": "col_6_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst = p.fk_codelst_bloodgrp),0))"});
																aoData.push( { "name": "col_6_column", "value": "6"});
																
																aoData.push( { "name": "col_7_name", "value": "lower(nvl( SPEC_ID ,' '))"});
																aoData.push( { "name": "col_7_column", "value": "7"});
																
																aoData.push( { "name": "col_8_name", "value": "lower(nvl((select CODELST_DESC from er_codelst where PK_CODELST=d.FK_CODELSTPRODUCTTYPE),' '))"});
																aoData.push( { "name": "col_8_column", "value": "8"});
																
																aoData.push( { "name": "col_9_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst = d.fk_codelstdonortype),' '))"});
																aoData.push( { "name": "col_9_column", "value": "9"});
																
																aoData.push( { "name": "col_10_name", "value": ""});
																aoData.push( { "name": "col_10_column", "value": ""}); */ 
													};
	var productDataTable_aoColumnDef = [
			                         {			 "aTargets": [ 0], "mDataProp": function ( source, type, val ) {
			                        	 			prodid=source[7];
			                             			 return "<input name='productcheck' id= 'productcheck' type='checkbox' value='" + source[0] +"' onchange='ontrigger(this,\""+prodid+"\")'/><input name='pkSpecimen' id= 'pkSpecimen' type='hidden' value='" + source[10] +"' />";
			                             }},
			                          {    "sTitle":'Product ID',
			                     	             "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
			                     	            	 product_id="<input type='hidden' class ='"+source[7]+"' id='specId' name='specId' value='"+source[7]+"'> ";
			                     	            	product_id+=source[7];
			    							        	return product_id;
			                             			
			                             }},
			                          {    "sTitle":'Recipient Name',
			                     	              "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
					                             	 return source[2];
			                             }},
			                          {    "sTitle":'Recipient MRN',
			                                      "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
			   			                             return source[1];
			                             }},
			                          {    "sTitle":'Recipient ABO/Rh',
				                                  "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
							                         return source[3];
				                         }}, 
				                      {    "sTitle":'<s:text name="stafa.label.donorname"/>',
				                                  "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
							                         return source[5];
				                         }}, 
				                              
			                          {    "sTitle":'Donor MRN',
			                                      "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
			   			                             return source[4];
			                             }},    
			                          {    "sTitle":'Donor ABO/Rh',
			                                      "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
			   			                             return source[6];
			                             }},    
			                          {    "sTitle":'Donor Type',
			                                      "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
			   			                             return source[9];
			                             }},  
			                          {    "sTitle":'Product Type',
			                                      "aTargets": [ 9], "mDataProp": function ( source, type, val ) {
			   			                             return source[8];
			                             }},    
			                          {    "sTitle":'Product Status',
			                                      "aTargets": [ 10], "mDataProp": function ( source, type, val ) {
			   			                             return source[11];
			                             }} 
			                        ];
	var productDataTable_aoColumn = [ { "bSortable": false},
	                               	   null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null
			             ];
	var productDataTable_ColManager = function(){
										$("#productDataTableColumn").html($('#productDataTable_wrapper .ColVis'));
									 };
	constructTable(flag,'productDataTable',productDataTable_ColManager,productDataTable_serverParam,productDataTable_aoColumnDef,productDataTable_aoColumn);
}
$(document).ready(function(){

//	constructProductList();
	$("#retrivalContent").hide();
	$("#inspectionContent").hide();
	  $('#tmpretrievalTime').timepicker({});
	  showTracker($("#dailyAssignment").val(),null,ctlProductFlowType,null,false,true);
	  $(".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true, changeYear: true, yearRange: '1900:' + new Date().getFullYear()});
	constructProductList(true);
	loadRetrievalData();
	var retrival_li=$('#retrivalTab_ul li').first().find("a").first().attr("id");
	$("#"+retrival_li).trigger("click");
	var inspection_li=$('#inspectionTab_ul li').first().find("a").first().attr("id");
	$("#"+inspection_li).trigger("click");
	
  });

function setRetrievalTime(){
	$('#retrievalTime').val($('#retrievalDate').val() +" "+ $('#tmpretrievalTime').val())
}  
function loadRetrievalData(){
	var dailyAssignval="";
	 var retrievalresponse="";
	var url = "loadRetrievalData";
	dailyAssignval=$("#dailyAssignment").val();
	if(dailyAssignval !=0 && dailyAssignval != "" && typeof(dailyAssignval)!='undefined'){
		 var jsonData = "dailyAssignment:"+dailyAssignval;
		retrievalresponse = jsonDataCall(url,"jsonData={"+jsonData+"}");
	}
	$(retrievalresponse.ctlRetrieval).each(function(i){
		
		this.retrievalDate=converDate(this.retrievalDate);
		this.retrievalTime=this.retrievalDate+" "+timeConversion(this.retrievalTime);
	});
	
		
		
	var listlength=retrievalresponse.ctlRetrieval.length;	
   addTab("retrivalTab_ul","retrivalContent","retrivalForm",listlength);
  
   if(retrievalresponse.ctlRetrieval.length>0){
      $(retrievalresponse.ctlRetrieval).each(function(i){
    	var  productid=this.product;
    	//alert("hello"+this.fkProduct);
    	 $("#retrivalTab_ul li").find("input").each(function(j,v){
    		var refprod=$(this).attr("refproductid");
    		var hiddenid=$(this).attr("id");
    		
    	  if(refprod==productid){
    		//  alert(JSON.stringify(retrievalresponse.ctlRetrieval[i]));
    		  $(this).val(JSON.stringify(retrievalresponse.ctlRetrieval[i]));
    		  loadData1(hiddenid,"retrivalForm",refprod,'retrivalTab_ul');
    	  } 
    	 
    	 });
    	 
  			
  			
  		});
   }
   var inslength=retrievalresponse.ctlRetrieval.length;	   
      addTab("inspectionTab_ul","inspectionContent","inspectionForm",inslength);
      
      if(retrievalresponse.ctlInspection.length>0){
      $(retrievalresponse.ctlInspection).each(function(i){
       	  productid=this.product;
       	 $("#inspectionTab_ul li").find("input").each(function(j,v){
       		var refprod=$(this).attr("refproductid");
       	 var hiddenid=$(this).attr("id");
       	  if(refprod==productid){
       		  //alert($(this).prop("id"));
       		// alert(JSON.stringify(retrievalresponse.ctlInspection[i]));
       		  $(this).val(JSON.stringify(retrievalresponse.ctlInspection[i]));
       		loadData1(hiddenid,"inspectionForm",refprod,'inspectionTab_ul');
       	  } 
       	
       	 });
      });
      }
} 
function getScanData(event,obj){
	
  	if(event.keyCode==13){
  		 var selectedProduct = $("#searchProducts").val();
  	     var index = selectedProduct.indexOf($("#conformProductSearch").val());
  	     if(index==-1){
  	         if(selectedProduct == "," ){
  	             selectedProduct+=$("#conformProductSearch").val();
  	             var slicedVal = selectedProduct.substring(1);
  	             $("#searchProducts").val(slicedVal);
  	             criteriaValue = "'"+slicedVal+"'";
  	         }else{
  	             selectedProduct+=","+$("#conformProductSearch").val()+"";
  	             $("#searchProducts").val(selectedProduct);
  	             var str = selectedProduct.lastIndexOf(",");
  	             var slicedVal = selectedProduct.substring(str,selectedProduct.length);
  	             criteriaValue+= ",'"+slicedVal.substring(1)+"'";
  	             }
  	     }
  	//  var checkproductflag=false;

   	                                   
  		constructProductList(true,criteriaValue);
  	
  	   /* 	$('#productDataTable tbody tr').each(function(i,v){
  			proid=$(v).find("#specId").val();
  		if($("#conformProductSearch").val()==proid && $("#conformProductSearch").val()!=""){
  				   checkproductflag=true;
  			   }
  			}); */
  		var retrivaltablen=$("#retrivalTab_ul").find("li").length;
  		var inspectiontablen=$("#inspectionTab_ul").find("li").length;
  		//alert(tablen);
  		if(retrivaltablen>=1){
  		 $("#retrivalTab_ul").find("li").each(function(j,v){
			 if($(this).hasClass("current")){
				 var hiddenid=$(this).find("input").prop('id');
				 refproduct=$(this).find("input").prop('refproduct')
				 $("#activeRetrivalTab").val(hiddenid);
					loadData(hiddenid,'retrivalForm',refproduct);
				
			 }
		 });
  		}
  		if(inspectiontablen>=1){
  		 $("#inspectionTab_ul").find("li").each(function(j,v){
			 if($(this).hasClass("current")){
				 var hiddenid=$(this).find("input").prop('id');
				 refproduct=$(this).find("input").prop('refproduct')
				 $("#activeRetrivalTab").val(hiddenid);
					loadData(hiddenid,'inspectionForm',refproduct);
				
			 }
		 });
  		}
  	
  		resetForm($('#retrivalForm'));
  		resetForm($('#inspectionForm'));
	   addTab("retrivalTab_ul","retrivalContent","retrivalForm");
 	   addTab("inspectionTab_ul","inspectionContent","inspectionForm");

 	  $("#retrivalTab_ul").find("li").each(function(j,v){
			 if($(this).hasClass("current")){
				 var hiddenid=$(this).find("input").prop('id');
				 refproduct=$(this).find("input").prop('refproduct')
		//	 $("#activeRetrivalTab").val("");
				 $("#activeRetrivalTab").val(hiddenid);
					loadData(hiddenid,'retrivalForm',refproduct);
				// $("#fkProduct").val(refproduct);
			//	 re_activeTabJsonId = hiddenid;
			 }
		 });
 	  
 	 $("#inspectionTab_ul").find("li").each(function(j,v){
			 if($(this).hasClass("current")){
				 var hiddenid=$(this).find("input").prop('id');
				 refproduct=$(this).find("input").prop('refproduct')
				// $("#activeInspectionTab").val("");
					 $("#activeInspectionTab").val(hiddenid); 
					 loadData(hiddenid,'inspectionForm',refproduct);
				//	 $("#inspectionForm_fkProduct").val(refproduct);
				
			//	 re_activeTabJsonId = hiddenid;
			 }
		 });
 	 
 
 	// resetForm($('#retrivalForm'));
	// resetForm($('#inspectionForm')); 
		 
  		}
  	}
  	

 function addTab(tab_ulid,contentid,formid,listlength){
	 var proid="";
	 var proclas="";
	 var refproduct="";
	 var tempcontend=contentid;
	 var k=0;
	 $('#productDataTable tbody tr').each(function(i,v){
			proclas=$(v).find("#specId").attr("class");  
			proid=$(v).find("#specId").val();
			refproduct=$(v).find("#pkSpecimen").val();
			
			
	 	if($("#conformProductSearch").val()!=proid && $("#conformProductSearch").val()!=""){
	 		
			 if ($("#"+proid).length !=0 )
				{
		        	return;
				}
		   } 
	//?BB });  
	           		 $("#"+tab_ulid+" li").removeClass("current");
	           		if(listlength<=0){
	           		 if(formid=='retrivalForm'){
		            		$("#retrival_Product").val(refproduct);
		            		$("#pkRetrival").val(0);
		            		
		            		
		 	            }
		 	            else if(formid=='inspectionForm'){
		 	            	$("#inspectionForm_Product").val(refproduct)
		 	            	$("#pkInspection").val(0);
		 	            	
		 	            }
					}
	            	
					var results=JSON.stringify(serializeFormObject($('#'+formid)));
		
				
					
					 $("#"+tab_ulid).append("<li id='li_"+proid+formid +"_"+refproduct+"'class='current'><input type='hidden' id="+proid+formid+"   value='"+results+"' refproductid="+refproduct+"><a class='tab' id='a_" +
							 proid+formid+ "'href='#"+contentid+"' onclick=loadData('"+proid+formid+"','"+formid+"','"+refproduct+"') rel="+refproduct+">" + proid + 
			                "</a><a href='#' id="+proid+" class='remove' onclick=deletedtab(this,'"+proid+"','"+formid+"')>x</a></li>");
			        
			            $("#"+contentid).show();
				
	          
	           
	             if($("#"+tab_ulid+" li").hasClass("current")){
	            	 
	            	 if(formid=='retrivalForm'){
	            		$("#retrival_Product").val(refproduct);
	            		$("#pkRetrival").val(0);
	            		
	            		
	 	            }
	 	            else if(formid=='inspectionForm'){
	 	            	$("#inspectionForm_Product").val(refproduct)
	 	            	$("#pkInspection").val(0);
	 	            	
	 	            }
	            } 
	            $('#'+tab_ulid+' a.tab').live('click', function() {
	                // Get the tab name
	          
	                $("#"+tab_ulid+" li").removeClass("current");
	           
	                $(this).parent().addClass("current");
	             
	            });
	          
	      			 
	 });         
			
	        
	 
	 
}    

	function deletedtab(obj,tabprodid,formid){
		 var yes=confirm(confirm_deleteMsg);
		 if(!yes){
						return;
		 }
		if(formid=='retrivalForm'){
				var retrivalid= $("#retrivalTab_ul").find("#"+tabprodid).closest('li').find('input').attr('id');
				var retrival_value=jQuery.parseJSON($("#"+retrivalid).val());
				var InspectionId= $("#inspectionTab_ul").find("#"+tabprodid).closest('li').find('input').attr('id');
				var inspection_value=jQuery.parseJSON($("#"+InspectionId).val());
			      jsonData = "{deletedIds:'"+inspection_value.pkInspection+"',domainName:'DOMAIN_INSPECTION',domainKey:'INSPECTION_DOMAINKEY'}";
				 spec_jsonData = "{deletedIds:'"+retrival_value.pkRetrival+"',domainName:'DOMAIN_RETRIEVAL',domainKey:'RETRIVAL_DOMAINKEY'}";
				  url="commonDelete";
			     response = jsonDataCall(url,"jsonData="+jsonData);
			     response = jsonDataCall(url,"jsonData="+spec_jsonData);
			     $("#inspectionTab_ul").find("#"+tabprodid).parent().remove();
			     $("#retrivalTab_ul").find("#"+tabprodid).parent().remove();
		         var retrievalfirsttab = $("#retrivalTab_ul li:first-child").find("a").first().attr("id");
		         $("#"+retrievalfirsttab).trigger("click");
		         var inspectionfirsttab = $("#inspectionTab_ul li:first-child").find("a").first().attr("id");
		         $("#"+inspectionfirsttab).trigger("click");
		}else if(formid=='inspectionForm'){
			
			var retrivalid= $("#retrivalTab_ul").find("#"+tabprodid).closest('li').find('input').attr('id');
					var retrival_value=jQuery.parseJSON($("#"+retrivalid).val());
					var InspectionId= $("#inspectionTab_ul").find("#"+tabprodid).closest('li').find('input').attr('id');
					var inspection_value=jQuery.parseJSON($("#"+InspectionId).val());
					 jsonData = "{deletedIds:'"+inspection_value.pkInspection+"',domainName:'DOMAIN_INSPECTION',domainKey:'INSPECTION_DOMAINKEY'}";
					 spec_jsonData = "{deletedIds:'"+retrival_value.pkRetrival+"',domainName:'DOMAIN_RETRIEVAL',domainKey:'RETRIVAL_DOMAINKEY'}";
					 url="commonDelete";
				     response = jsonDataCall(url,"jsonData="+jsonData);
				     response = jsonDataCall(url,"jsonData="+spec_jsonData);
				     $("#inspectionTab_ul").find("#"+tabprodid).parent().remove();
				     $("#retrivalTab_ul").find("#"+tabprodid).parent().remove();
			         var retrievalfirsttab = $("#retrivalTab_ul li:first-child").find("a").first().attr("id");
			         $("#"+retrievalfirsttab).trigger("click");
			         var inspectionfirsttab = $("#inspectionTab_ul li:first-child").find("a").first().attr("id");
			         $("#"+inspectionfirsttab).trigger("click");
		}
		var criteriaValue="";
		$("#retrivalTab_ul li").find("a:eq(1)").each(function(i,v){
			if(criteriaValue==""){
				var prodid=$(this).attr("id");
			criteriaValue+="'"+ $(this).attr("id") +"'";
			}else{
				var prodid=$(this).attr("id");
				criteriaValue+=','+"'"+ $(this).attr("id") +"'";
			}
			
		});
		//alert(criteriaValue);
		constructProductList(true,criteriaValue);
	}	
  function resetForm($form) {
	                $form.find('input:text,input:hidden,  input:password, input:file, select, textarea').val('');
	                $form.find('input:radio, input:checkbox')
	                     .removeAttr('checked').removeAttr('selected');
	                $.uniform.restore("select");
	          	  $("select").uniform();
	            }

 function loadData(hiddenid,formid,refproduct){
		
	
	var reresult=JSON.stringify(serializeFormObject($('#'+formid)));
	
		 if(formid=='retrivalForm'){
			
			var hiddentabid= $("#activeRetrivalTab").val();
			$("#"+hiddentabid).val(reresult);
			$("#activeRetrivalTab").val(hiddenid);
		      }
		      else if(formid=='inspectionForm'){
		    	  var hiddentabid= $("#activeInspectionTab").val();
					$("#"+hiddentabid).val(reresult);
					$("#activeInspectionTab").val(hiddenid);
		      }
		//to Maintain active tab and previous tab
		var data="";
		data=$("#"+hiddenid).val();
		var result = jQuery.parseJSON(data);
		
		result.product=refproduct;
		
	  
		$("#"+formid).loadJSON(result);
	//	alert()
		 $("#tmpretrievalTime").val($("#retrievalTime").val());
	
		$.uniform.restore('select');
		$('select').uniform();
		
 }
 function loadData1(hiddenid,formid,refproduct,tabid){
		var data="";
		data=$("#"+hiddenid).val();
		var result = jQuery.parseJSON(data);
		
		//checkCurrentProduct();
		$("#"+formid).loadJSON(result);
		$.uniform.restore('select');
		$('select').uniform();
		loadData(hiddenid,formid,refproduct);
	
 }
 
 function setTime(){
	
		//result.retrievalDate=converDate(result.retrievalDate);
		var timeconvert=$("#tmpretrievalTime").val($("retrievalTime").val());
	//	setRetrievalTime();
	//return timeconvert;
	}
 function timeConversion(result){
	 if(result!=null && result!=""){
			var tmpDate = new Date(result);
		
			if(tmpDate != null && tmpDate != "" && $.browser.msie){
				var tmpDate1 = result.split("-");
				var tmpTime = tmpDate1[2].substring(3,8);
				result=tmpTime;
			}else{
				
				result = (tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes();
			//	$("#tmpTransfusionEndTime").val(transfusionEndTime);
				result=result;
			}

		}
	 
	 return result;
 }
 function checkCurrentProduct(){
	 $("#retrivalTab_ul").find("li").each(function(i,v){
			if($(this).hasClass("current")){
				var prid= $(this).find("input").attr("id");
				var refid=$(this).find("a").attr('rel');
				//alert("refid"+refid);
				 loadData(prid,'retrivalForm',refid);
		    } 
		 });
		 $("#inspectionTab_ul").find("li").each(function(i,v){
				if($(this).hasClass("current")){
					var prid= $(this).find("input").attr("id");
					var refid=$(this).find("a").attr('rel');
					 loadData(prid,'inspectionForm',refid);
			    } 
			 });
 }
  function saveRetrival(mode){
	 var rowdata='';
	 var rowdata1='';
	 checkCurrentProduct();
	 if(mode =="next"){
			$("#nextFlag").val("1");
		}
	 $("#retrivalTab_ul li").find("input").each(function(i,e){
		
		//rowdata+ =$(v).find("input[type=text]").val();
		tmpId = $(this).attr("id");
		if(i==0){
		rowdata+=$("#"+tmpId).val();
		}else{
			rowdata+=","+$("#"+tmpId).val();	
		}
		
	 });
	 $("#inspectionTab_ul li").find("input").each(function(i,e){
			//rowdata+ =$(v).find("input[type=text]").val();
			tmpId1 = $(this).attr("id");
			if(i==0){
				rowdata1+=$("#"+tmpId1).val();
				}else{
					rowdata1+=","+$("#"+tmpId1).val();	
				}
			
		 });
	
	var url="saveRetrival";
		response = jsonDataCall(url,"jsonData={retrievalData:["+rowdata+"],inspectionData:["+rowdata1+"],nextFlag:"+$("#nextFlag").val()+",dailyAssignment:"+$("#dailyAssignment").val()+"}");
		alert("Data Saved Successfully");
	// return false;
 } 
   function savePages(mode){
 	
 	try{
 				if(saveRetrival(mode)){
 					//alert("wait");
 					
 				}
 				return true;
 	}catch(e){
 		alert("exception " + e);
 	}
 	
 } 
   
  /*  function setconIntact(){
	var cIntact= $("input[name=conIntact]").val();
	$("#containerIntact").val(cIntact);
   } */
   
   function setTechSign(obj){
	   
	   var s=$("#ctlsign").attr('checked');
	    if(s=='checked'){
	    	 $("#technicianSignature").val("N/A");
	    }else {
	    	 $("#technicianSignature").val("");
	    }
	
   }

</script>







<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Product Table</div>
		<div class="portlet-content">
		<form>
		<input type="hidden" id="activeRetrivalTab" name="activeRetrivalTab"/>
             <input type="hidden" id="activeInspectionTab" name="activeInspectionTab"/>
		<input type="hidden" name="entityType" id="entityType" value="PRODUCT" />
	<!-- 	<input type="hidden" name="entityType" id="entityType" value="CTL" /> -->
           <input type="hidden" id="scanId" name="scanId"/>
           <s:hidden id="searchProducts" name="searchProducts" value=","/>
		 <div id="button_wrapper">
      				<div id="divProductScan" style="margin-left:40%;">
       					<input class="scanboxSearch barcodeSearch multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;" onkeyup="performScanData(event,this)"/>
						<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" onfocus="changeType(this)"/>
 					</div>
 			</div>		
				<table cellpadding="0" cellspacing="0" border="1" id="productDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='productDataTableColumn'></th>
							<th>Product ID</th>
							<th>Recipient Name</th>
							<th>Recipient MRN</th>
							<th>Recipient ABO/Rh</th>
							<th>Donor Name</th>
							<th>Donor MRN</th>
							<th>Donor ABO/Rh</th>
							<th>Donor Type</th>
							<th>Product Type</th>
							<th>Product Status</th>
						</tr>
					</thead>
				</table>
		
			</form>	
			
			</div>
		</div>
	</div>


<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Product Retrieval</div>
		<div class="portlet-content">
		<div id="productretrivaltabs" class="">
				   
		<ul class="tabs"  id="retrivalTab_ul">
           
        </ul>
        <form id="retrivalForm">
             <input type="hidden" id="pkRetrival" name="pkRetrival" value="0"/>
             <s:hidden id="nextFlag" name="nextFlag" value="0" />
             <input type="hidden" id="retrival_Product" name="product" value="0"/>
            <s:hidden id="dailyAssignment" name="dailyAssignment" value="%{dailyAssignment}" /> 
       	   <div class="content" id="retrivalContent" >
           <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Retrieval Date</td>
					<td class="tablewapper1"><input type="text" id="retrievalDate" class="dateEntry" name="retrievalDate"/></td>
					<td class="tablewapper">Retrieval Location</td>
					<td class="tablewapper1"><s:select id="fkCodelstRecipientLocation"  name="fkCodelstRecipientLocation" list="ctlRecipientLocation" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
					
				</tr>
				<tr>
					<td class="tablewapper">Retrieval Time</td>
					<td class="tablewapper1"><input id="retrievalTime" name="retrievalTime" type="hidden" class=""/><s:textfield id="tmpretrievalTime" name="tmpretrievalTime" Class="" onchange="setRetrievalTime();"/></td>
					
					
				</tr>
				
				
				</table>
        </div>
	</form>
	</div>
</div>
</div>
</div>

<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Product Inspection</div>
		<div class="portlet-content">
		<div id="productretrivaltabs">
				<ul class="tabs"  id="inspectionTab_ul">
           
        </ul>
        <form id="inspectionForm">
             <input type="hidden" id="pkInspection" name="pkInspection" value="0"/>
             <input type="hidden" id="inspectionForm_Product" name="product" value="0"/>
             <s:hidden id="inspection_dailyAssignment" name="dailyAssignment" value="%{dailyAssignment}" /> 
			  <div id="inspectionContent" class="content">  
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Product Container Intact With No Leaking</td>
					<td class="tablewapper1"><!-- <input type="hidden" id="containerIntact" name="containerIntact" value=""/> --><s:radio list="#{'1':'Yes','0':'No'}" name="containerIntact" id="containerIntact"  /></td>
					<td class="tablewapper">Label Verfication</td>
					<td class="tablewapper1"><!-- <input type="hidden" id="isVerified" name="isVerified" value=""/> --><s:radio list="#{'1':'Yes','0':'No'}" name="isVerified" id="isVerified"  /></td>
				</tr>
				<tr>
					<td class="tablewapper">Product Appearance OK</td>
					<td class="tablewapper1"><!-- <input type="hidden" id="appearance" name="appearance" value=""/> --><s:radio list="#{'1':'Yes','0':'No'}" name="appearance" id="appearance" /></td>
				</tr>
				<tr>
					<td class="tablewapper">CTL Technician 2 Signature</td>
					<td class="tablewapper1"><input type="text" id="technicianSignature" name="technicianSignature"/><input type="checkbox" id="ctlsign" name="ctlsign" onchange="setTechSign(this)"/>N/A</td>
				</tr>
				
				
				</table>
		
		</div>
	</form>
	</div>
</div>
</div>
</div>
 <form onsubmit="return avoidFormSubmitting()">
	<div align="left" style="float:right;">
	    <table width="100%" cellpadding="0" cellspacing="0" class="" align="" border="0">
	        <tr>
	        <td><span><label>Next:</label>
	        <select name="nextOption" id="nextOption" class=""><option value="loadCTLTechHome.action">HomePage</option><option value="logout.action">Logout</option></select>
	        <input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
	        <input type="button" value="Next" onclick="verifyeSign('eSignNext','next')"/></span></td>
	        </tr>
	    </table>
	</div>
</form>
<script>
$(function() {
		
		$(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all").find(".portlet-header").addClass("ui-widget-header ui-corner-all").prepend(
						"<span class='ui-icon ui-icon-minusthick'></span>");

		/**For add new popup**/

		$(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all").find(".addnew")
				.append(
						"<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");



		$(".portlet-header .ui-icon").click(
				function() {
					$(this).toggleClass("ui-icon-minusthick").toggleClass(
							"ui-icon-plusthick");
					$(this).parents(".portlet:first").find(".portlet-content")
							.toggle();
				});
	});
	
</script>