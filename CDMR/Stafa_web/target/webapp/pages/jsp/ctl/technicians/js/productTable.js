 function constructProductList(flag){
	//alert("construct ProductList1"+criteriaValue);
	var criteria = " ";



		$("#scanId").val($("#divProductScan #fake_conformProductSearch").val() );
		//alert($("#scanId").val());
		criteria = " ";
		if($("#scanId").val()!=""){
			//productCriteria= " lower(SPEC_ID) like lower('%"+ criteriaValue +"%')";
			criteria= " or lower(SPEC_ID) like lower('%" + $("#scanId").val() +"%')";
			
		}
		

	var productDataTable_serverParam = function ( aoData ) {
																aoData.push( { "name": "application", "value": "stafa"});
																aoData.push( { "name": "module", "value": "Ctl_reviewProduct_List"});
																
																
																aoData.push( { "name": "col_1_name", "value": "lower(nvl( SPEC_ID ,' '))"});
																aoData.push( { "name": "col_1_column", "value": "7"});
																
																aoData.push( { "name": "col_2_name", "value": "lower(nvl( (select nvl(PERSON_LNAME, ' ' ) || ' ' ||nvl(PERSON_FNAME, ' ' ) from person where pk_person=d.fk_receipent),' '))"});
																aoData.push( { "name": "col_2_column", "value": "2"});
																
																aoData.push( {"name": "col_3_name", "value": "lower(nvl((select nvl(person_mrn,' ') from person where pk_person=d.fk_receipent),' '))" } );
																aoData.push( { "name": "col_3_column", "value": "1"});
																
																 aoData.push( { "name": "param_count", "value":"2"});
																aoData.push( { "name": "param_1_name", "value":":dailyAssignment:"});
																aoData.push( { "name": "param_1_value", "value":$("#dailyAssignment").val()}); 
																
																aoData.push( { "name": "param_2_name", "value":":criteria:"});
																aoData.push( { "name": "param_2_value", "value":criteria}); 
															/* 	aoData.push( { "name": "col_4_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst = (select fk_codelst_bloodgrp from person where pk_person=d.fk_receipent)),' '))"});
																aoData.push( { "name": "col_4_column", "value": "3"});
																
																aoData.push( { "name": "col_4_name", "value": "lower(nvl(p.PERSON_MRN, ' '))"});
																aoData.push( { "name": "col_4_column", "value": "4"});
																
																aoData.push( { "name": "col_5_name", "value": "lower(nvl(p.PERSON_LNAME, ' ' )|| ' ' || nvl(p.PERSON_FNAME, ' ' ))"});
																aoData.push( { "name": "col_5_column", "value": "5"});
																
																aoData.push( { "name": "col_6_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst = p.fk_codelst_bloodgrp),0))"});
																aoData.push( { "name": "col_6_column", "value": "6"});
																
																aoData.push( { "name": "col_7_name", "value": "lower(nvl( SPEC_ID ,' '))"});
																aoData.push( { "name": "col_7_column", "value": "7"});
																
																aoData.push( { "name": "col_8_name", "value": "lower(nvl((select CODELST_DESC from er_codelst where PK_CODELST=d.FK_CODELSTPRODUCTTYPE),' '))"});
																aoData.push( { "name": "col_8_column", "value": "8"});
																
																aoData.push( { "name": "col_9_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst = d.fk_codelstdonortype),' '))"});
																aoData.push( { "name": "col_9_column", "value": "9"});
																
																aoData.push( { "name": "col_10_name", "value": ""});
																aoData.push( { "name": "col_10_column", "value": ""}); */ 
													};
	var productDataTable_aoColumnDef = [
			                         {			 "aTargets": [ 0], "mDataProp": function ( source, type, val ) {
			                        	 			prodid=source[7];
			                             			 return "<input name='productcheck' id= 'productcheck' type='checkbox' value='" + source[0] +"' onchange='ontrigger(this,\""+prodid+"\")'/><input name='pkSpecimen' id= 'pkSpecimen' type='hidden' value='" + source[10] +"' />";
			                             }},
			                          {    "sTitle":'Product ID',
			                     	             "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
			                     	            	 product_id="<input type='hidden' class ='"+source[7]+"' id='specId' name='specId' value='"+source[7]+"'> ";
			                     	            	product_id+=source[7];
			    							        	return product_id;
			                             			
			                             }},
			                          {    "sTitle":'Recipient Name',
			                     	              "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
					                             	 return source[2];
			                             }},
			                          {    "sTitle":'Recipient MRN',
			                                      "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
			   			                             return source[1];
			                             }},
			                          {    "sTitle":'Recipient ABO/Rh',
				                                  "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
							                         return source[3];
				                         }}, 
				                      {    "sTitle":'<s:text name="stafa.label.donorname"/>',
				                                  "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
							                         return source[5];
				                         }}, 
				                              
			                          {    "sTitle":'Donor MRN',
			                                      "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
			   			                             return source[4];
			                             }},    
			                          {    "sTitle":'Donor ABO/Rh',
			                                      "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
			   			                             return source[6];
			                             }},    
			                          {    "sTitle":'Donor Type',
			                                      "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
			   			                             return source[9];
			                             }},  
			                          {    "sTitle":'Product Type',
			                                      "aTargets": [ 9], "mDataProp": function ( source, type, val ) {
			   			                             return source[8];
			                             }},    
			                          {    "sTitle":'Product Status',
			                                      "aTargets": [ 10], "mDataProp": function ( source, type, val ) {
			   			                             return source[11];
			                             }} 
			                        ];
	var productDataTable_aoColumn = [ { "bSortable": false},
	                               	   null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null
			             ];
	var productDataTable_ColManager = function(){
										$("#productDataTableColumn").html($('#productDataTable_wrapper .ColVis'));
									 };
	constructTable(flag,'productDataTable',productDataTable_ColManager,productDataTable_serverParam,productDataTable_aoColumnDef,productDataTable_aoColumn);
} 
 
 function constructProProcessList(flag){
		//alert("construct ProductList1"+criteriaValue);
		var criteria = " ";
		var productFlag = false;
		var productCriteria;
			$("#scanId").val($("#divProductScan #fake_conformProductSearch").val() );
			//alert($("#scanId").val());
			criteria = " and pk_person=fk_person ";
			if($("#scanId").val()!=""){
				//productCriteria= " lower(SPEC_ID) like lower('%"+ criteriaValue +"%')";
				productCriteria= " lower(SPEC_ID) like lower('%" + $("#scanId").val() +"%')";
				productFlag = true;
			}
			if(productFlag){
					criteria += "and (";
					criteria += productCriteria;
					criteria += ")";
			}

		var productDataTable_serverParam = function ( aoData ) {
																	aoData.push( { "name": "application", "value": "stafa"});
																	aoData.push( { "name": "module", "value": "Ctl_currentProduct_List"});
																	aoData.push( { "name": "criteria", "value": criteria});	
																	
																	aoData.push( { "name": "col_1_name", "value": "lower(nvl( SPEC_ID ,' '))"});
																	aoData.push( { "name": "col_1_column", "value": "7"});
																	
																	aoData.push( { "name": "col_2_name", "value": "lower(nvl( (select nvl(PERSON_LNAME, ' ' ) || ' ' ||nvl(PERSON_FNAME, ' ' ) from person where pk_person=d.fk_receipent),' '))"});
																	aoData.push( { "name": "col_2_column", "value": "2"});
																	
																	aoData.push( {"name": "col_3_name", "value": "lower(nvl((select nvl(person_mrn,' ') from person where pk_person=d.fk_receipent),' '))" } );
																	aoData.push( { "name": "col_3_column", "value": "1"});
																	
																	 
																	
																/* 	aoData.push( { "name": "col_4_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst = (select fk_codelst_bloodgrp from person where pk_person=d.fk_receipent)),' '))"});
																	aoData.push( { "name": "col_4_column", "value": "3"});
																	
																	aoData.push( { "name": "col_4_name", "value": "lower(nvl(p.PERSON_MRN, ' '))"});
																	aoData.push( { "name": "col_4_column", "value": "4"});
																	
																	aoData.push( { "name": "col_5_name", "value": "lower(nvl(p.PERSON_LNAME, ' ' )|| ' ' || nvl(p.PERSON_FNAME, ' ' ))"});
																	aoData.push( { "name": "col_5_column", "value": "5"});
																	
																	aoData.push( { "name": "col_6_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst = p.fk_codelst_bloodgrp),0))"});
																	aoData.push( { "name": "col_6_column", "value": "6"});
																	
																	aoData.push( { "name": "col_7_name", "value": "lower(nvl( SPEC_ID ,' '))"});
																	aoData.push( { "name": "col_7_column", "value": "7"});
																	
																	aoData.push( { "name": "col_8_name", "value": "lower(nvl((select CODELST_DESC from er_codelst where PK_CODELST=d.FK_CODELSTPRODUCTTYPE),' '))"});
																	aoData.push( { "name": "col_8_column", "value": "8"});
																	
																	aoData.push( { "name": "col_9_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst = d.fk_codelstdonortype),' '))"});
																	aoData.push( { "name": "col_9_column", "value": "9"});
																	
																	aoData.push( { "name": "col_10_name", "value": ""});
																	aoData.push( { "name": "col_10_column", "value": ""}); */ 
														};
		var productDataTable_aoColumnDef = [
				                         {			 "aTargets": [ 0], "mDataProp": function ( source, type, val ) {
				                        	 			prodid=source[7];
				                             			 return "<input name='productcheck' id= 'productcheck' type='checkbox' value='" + source[0] +"' onchange='ontrigger(this,\""+prodid+"\")'/><input name='pkSpecimen' id= 'pkSpecimen' type='hidden' value='" + source[10] +"' />";
				                             }},
				                          {    "sTitle":'Product ID',
				                     	             "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
				                     	            	 product_id="<input type='hidden' class ='"+source[7]+"' id='specId' name='specId' value='"+source[7]+"'> ";
				                     	            	product_id+=source[7];
				    							        	return product_id;
				                             			
				                             }},
				                          {    "sTitle":'Recipient Name',
				                     	              "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
						                             	 return source[2];
				                             }},
				                          {    "sTitle":'Recipient MRN',
				                                      "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
				   			                             return source[1];
				                             }},
				                          {    "sTitle":'Donor Name',
					                                  "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
								                         return source[5];
					                         }}, 
					                              
				                          {    "sTitle":'Donor MRN',
				                                      "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
				   			                             return source[4];
				                             }},    
				                          {    "sTitle":'Volume (mL)',
				                                      "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
				   			                             return "";
				                             }},  
				                          {    "sTitle":'Product Type',
				                                      "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
				   			                             return source[8];
				                             }},    
				                          {    "sTitle":'Product Status',
				                                      "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
				   			                             return source[11];
				                             }} 
				                        ];
		var productDataTable_aoColumn = [ { "bSortable": false},
		                               	   null,
							               null,
							               null,
							               null,
							               null,
							               null,
							               null,
							               null
				             ];
		var productDataTable_ColManager = function(){
											$("#productDataTableColumn").html($('#productDataTable_wrapper .ColVis'));
										 };
		constructTable(flag,'productDataTable',productDataTable_ColManager,productDataTable_serverParam,productDataTable_aoColumnDef,productDataTable_aoColumn);
	} 