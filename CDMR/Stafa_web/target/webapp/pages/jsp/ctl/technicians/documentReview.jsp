
<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/barcodeScanBox.js"></script>
<script type="text/javascript" src="js/ajaxfileupload.js"></script>
<script type="text/javascript" src="jsp/ctl/technicians/js/productTable.js"></script>

<script type="text/javascript" >


var enType;	
$(document).ready(function(){
	show_innernorth();
	//alert("value===>"+ctlDocumentReviewData.transDateVerified);
	constructProductList();
	
	showTracker($("#dailyAssignment").val(),null,ctlProductFlowType,null,false,true);
	//constructDocumentTable(true,productVal,entityType,'documentreviewTable');
	var productVal=$("#dailyAssignment").val();
	constructDocumentTable(false,productVal,enType,'documentreviewTable');
	
});
function loadDocmentReviewData(productVal){
	
	$("#pkInfusion").val("");
	
	var url = "loadDocmentReviewData";
	/* var infusionid=$("#pkInfusion").val();
	var refproduct=$("#fkProduct").val(); */
	if(typeof(productVal) !='undefined' && productVal != ""){
		 var jsonData = "refProduct:"+productVal;
		response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	}
	$(response.ctlDocumentReviewData).each(function(i){
		$("#pkInfusion").val(this.pkInfusion);
		if(this.transDateVerified==1){
			$("#transDateVerified_yes").prop("checked",true);
		}else{
			$("#transDateVerified_no").prop("checked",true);
		}
		if(this.physicianNotified==1){
			$("#physicianNotified_yes").prop("checked",true);
		}else{
			$("#physicianNotified_no").prop("checked",true);
		}
	});
}
function savePages(mode){	
	try{
			if(save_documentReview()){
						
				}  
				if(saveDocumentReviewpage(mode)){
					//alert("wait");
				}
				return true;
			
		
	}catch(e){
		alert("exception " + e);
	}
}


function getScanData(event,obj){
  	if(event.keyCode==13){
  		alert(" getscan " + $("#divProductScan #fake_conformProductSearch").val());
  		constructProductList(true);
  	/* var	productVal=$("#pkSpecimen").val();
  		$("#fkProduct").val(productVal);
  		constructDocumentTable(false,productVal,enType,'documentreviewTable');
  		constructDocumentHistoryTable(true,productVal,enType,'documentreviewTable1');
  		loadDocmentReviewData(productVal); */
  		}
  		
  	}
  

	function saveDocumentReviewpage(mode){
		var url = "saveDocumentReview";
		if(mode =="next"){
			$("#nextFlag").val("1");
		}	
		var results=JSON.stringify(serializeFormObject($('#documentreviewform')));
	    results = results.substring(1,(results.length-1)); 
			
		var jsonData = "jsonData={documentReviewData:{"+results+"}}";
		//	alert("jsonData==="+jsonData);
				
				
			   
				var response = jsonDataCall(url,jsonData);
				 alert("Data Saved Successfully");
	}	
	

 function save_documentReview(){
		//$('.progress-indicator').css( 'display', 'block' );
		try{
		var documentData = "";
		var productVal = $("#dailyAssignment").val();
		var entityType = $("#entityType").val();
		 $("#documentreviewTable tbody tr").each(function (row){
		 	var docsTitle="";
		 	var docsDate="";
		 	var fileData="";
		 	var fileUploadContentType="";
		 	var reviewedStatus="";
		 	var documentFileName="";
		 	var documentId="";
		 	if($(this).find(".filePathId").val()!=undefined){
			$(this).find("td").each(function (col){
				if(col ==0){
					documentId = $(this).find("#documentRef").val();
				}
				if(col ==1){
					docsTitle = $(this).find("#documentTitle").val();
				}
				if(col ==2){
					docsDate = $(this).find(".dateEntry").val();
				}
				if(col ==3){
					fileData = $(this).find(".filePathId").val();
					fileUploadContentType = $(this).find(".fileUploadContentType").val();
					fileData=fileData.replace(/\\/g,"/");
					documentFileName = $(this).find(".fileName").val();
					if(fileData!=undefined && fileData !="undefined" && fileData != ""){
						fileData = fileData.replace(/\\/g,"/");
					}
				}
			});
			if(fileData != undefined && fileData != ""){
				documentData+="{pkDocumentId:'"+documentId+"',fkEntity:'"+productVal+"',entityType:'"+entityType+"',fkCodelstDocumentType:'"+docsTitle+"',documentFileName:\""+documentFileName+"\",documentUploadedDate:'"+docsDate+"',documentFile:\""+fileData+"\",fileUploadContentType:\""+fileUploadContentType+"\",reviewedStatus:'"+reviewedStatus+"'},";
			}
		 	}
		 });
		 	//alert("documentData " + documentData);
			if(documentData.length >0){
				documentData = documentData.substring(0,(documentData.length-1));
				var url="saveMultipleDocument";
				response = jsonDataCall(url,"jsonData={documentData:["+documentData+"]}");
				//alert("Data Saved Successfully");
				//stafaAlert('Data saved Successfully','');
				$('.progress-indicator').css( 'display', 'none' );
				constructDocumentTable(true,productVal,entityType,'documentreviewTable');
				constructDocumentHistoryTable(true,productVal,enType,'documentreviewTable1');
			}
		}catch(e){
			alert("Error : "+e);
			}
		 return true;
	}
 var attachFileId=0;
 var dateId=0;
 function add_documentReview(){
	 var docsTitle = $("#docsTitleTemp").html();
	 //alert(docsTitle);
     var newRow ="<tr>"+
     "<td><input type='hidden' id='documentRef' name='documentRef' value='0'/></td>"+
     "<td><div><select id='documentTitle' name='documentTitle' style='width:200px;'>"+docsTitle+"</select></div></td>"+
     "<td><input type='text' id='documentDate"+dateId+"' name='documentDate' class='dateEntry'/></td>"+
   	 "<td><img src='images/file_loading.gif' id='fileUploadImage"+attachFileId+"' style='display:none'/>"+
   	 "<div id='fileDisplayName"+attachFileId+"'></div><input type='hidden' class='fileName' id='fileName"+attachFileId+"' name='fileName"+attachFileId+"'/><input type='hidden' class='filePathId' name='filePathId"+attachFileId+"' id='filePathId"+attachFileId+"'/> <input type='hidden' class='fileUploadContentType' name='fileUploadContentType"+attachFileId+"' id='fileUploadContentType"+attachFileId+"'/>"+
   	 "<div class='"+attachFileId+"' style='cursor: pointer;	color: #0101DF;' id='displayAttach"+attachFileId+"'></div><input style='display:block' class='"+attachFileId+"' type='file' name='fileUpload' onChange='ajaxFileUpload1(this,\""+attachFileId+"\")' id='attach"+attachFileId+"'/><span><s:text name='stafa.fileupload.message'/></span></td>"+
     //"<td><input type='file' name='fileUpload' id='attachedFile"+attachFileId+"'/></td>"+
     "<td></td>"+
     "</tr>";
     
     $("#documentreviewTable tbody").prepend(newRow);
     $("#documentDate"+dateId).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
         changeYear: true, yearRange: "c-50:c+45"});
     dateId=dateId+1;
     $.uniform.restore('select');
	 $('select').uniform();
	 $("#displayAttach"+attachFileId).click(function(){
		 var idSeq = $(this).attr("class");
		 $("#displayAttach"+attachFileId).hide();
		 $("#attach"+idSeq).click();
	});
	 attachFileId=attachFileId+1;
	 
	/*  var param ="{module:'APHERESIS',page:'VERIFICATION'}";
		markValidation(param); */
		
	 
 }
 function constructDocumentTable(flag,product,type,tableId) {
		//alert("construct table : "+tableId+",Donor : "+product);
			var criteria = "";
			if(product!=null && product!=""){
				criteria = " and fk_Entity ="+product ;
			}
			
			var documentColumn = [ { "bSortable": false},
				                	null,
				                	null,
				                	null,
				                	null,
				                	null
					             ];
														
			var documentServerParams = function ( aoData ) {
											aoData.push( { "name": "application", "value": "stafa"});
											aoData.push( { "name": "module", "value": "aphresis_DocumentReview"});
											aoData.push( { "name": "criteria", "value": criteria});
											
											aoData.push( {"name": "col_1_name", "value": "lower(DOCUMENT_FILENAME)" } );
											aoData.push( { "name": "col_1_column", "value": "lower(DOCUMENT_FILENAME)"});
											
											aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
											aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
														
											
						};
			var documentaoColDef = [
						                  {		
						                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
					                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
					                	 			return "<input type='hidden' id='documentRef' name='documentRef' value='"+source[0] +"' > <input name='docid' id='docid' type='checkbox' value='"+source[0] +"' />" ;
					                	 			//return "";
					                	     }},
						                  { "sTitle": "Document Title",
					                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
						                	    	 //alert('docTitle'+source.documentFileName); 
						                	 		return source[6];
						                	 }},
										  { "sTitle": "Document Date",
						                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
												    return source[2];
												    }},
										  { "sTitle": "Attachment",
												 "aTargets": [3], "mDataProp": function ( source, type, val ) {
											 	   return source[1] + " &nbsp;&nbsp;<a href='#' onclick='openPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\")'> View </a>";
										  		}},
										  { "sTitle": "Reviewed",
										  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
										  			 if(source[3] ==1){
										  				 return "Reviewed";
										  			 }else{
										  				 return "";
										  			 }
											  		//return "<input type='checkbox'/>";
											  }},
										  { "sTitle": "Version #",
						                		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
												    return source[7];
												    }}
										];
			var documentColManager = function () {
													$("#documentreviewColumn").html($("#"+tableId+"_wrapper .ColVis"));
												};
												
			constructTable(flag,tableId,documentColManager,documentServerParams,documentaoColDef,documentColumn);
		} 
 
	
 function constructDocumentHistoryTable(flag,product,type,tableId) {
  	//alert("construct table : "+tableId+",Donor : "+donor);
 	var criteria = "";
 	if(product!=null && product!=""){
 		criteria = " and fk_Entity ="+product ;
 	}
 	var docHistoryaoColumn =  [ { "bSortable": false},
 				                	null,
 				                	null,
 				                	null,
 				                	null,
 				                	null
 					            ];
 	
 	var docHistoryServerParams = function ( aoData ) {
 									aoData.push( { "name": "application", "value": "stafa"});
 									aoData.push( { "name": "module", "value": "aphresis_DocumentReview_History"});
 									aoData.push( { "name": "criteria", "value": criteria});
 									
 									aoData.push( {"name": "col_1_name", "value": "lower(DOCUMENT_FILENAME)" } );
 									aoData.push( { "name": "col_1_column", "value": "lower(DOCUMENT_FILENAME)"});
 									
 									aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
 									aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
 												
 									
 				};
 				
 	var	docHistoryaoColDef = [
 				                  {		
 				                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
 			                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
 			                	 			return "<input type='hidden' id='documentRef' name='documentRef' value='"+source[0] +"' >";
 			                	 			//return "";
 			                	     }},
 				                  { "sTitle": "Document Title",
 			                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
 				                	    	 //alert('docTitle'+source.documentFileName); 
 				                	 		return source[6];
 				                	 }},
 								  { "sTitle": "Document Date",
 				                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
 										    return source[2];
 										    }},
 								  { "sTitle": "Attachment",
 										 "aTargets": [3], "mDataProp": function ( source, type, val ) {
 									 	   return source[1] + " &nbsp;&nbsp;<a href='#' onclick='openPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\")'> View </a>";
 								  		}},
 								  { "sTitle": "Reviewed",
 								  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
 								  			 if(source[3] ==1){
 								  				 return "Reviewed";
 								  			 }else{
 								  				 return "";
 								  			 }
 									  		//return "<input type='checkbox'/>";
 									  }},
 								  { "sTitle": "Version #",
 				                		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
 										    return source[7];
 										    }}
 								];
 	
 	var docHistoryColManager =  function () {
 											$("#documentreviewColumn1").html($("#"+tableId+"_wrapper .ColVis"));
 								};
 								
 	constructTable(flag,tableId,docHistoryColManager,docHistoryServerParams,docHistoryaoColDef,docHistoryaoColumn);

 	$("#documentreviewTable1_wrapper").find("#"+tableId).removeAttr("style");
 } 
</script>







<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Product Table</div>
		<div class="portlet-content">
		<form>
		<input type="hidden" name="entityType" id="entityType" value="PRODUCT" />
        <input type="hidden" id="scanId" name="scanId"/>
		 <div id="button_wrapper">
      				<div id="divProductScan" style="margin-left:40%;">
       					<input class="scanboxSearch barcodeSearch multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;" onkeyup="performScanData(event,this)"/>
						<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" onfocus="changeType(this)"/>
 					</div>
 			</div>		
				<table cellpadding="0" cellspacing="0" border="1" id="productDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='productDataTableColumn'></th>
							
							<th>Product ID</th>
							<th>Recipient Name</th>
							<th>Recipient MRN</th>
							<th>Recipient ABO/Rh</th>
							<th>Donor Name</th>
							<th>Donor MRN</th>
							<th>Donor ABO/Rh</th>
							<th>Donor Type</th>
							<th>Product Type</th>
							<th>Product Status</th>
						</tr>
					</thead>
				</table>
			</form>	
			</div>
		</div>
	</div>


<div class="column">
	<div class="portlet">
	<s:form id="documentreviewform">
		<s:hidden id="nextFlag" name="nextFlag" value="0" /> 
		<s:hidden id="dailyAssignment" name="dailyAssignment" value="%{dailyAssignment}" /> 
		<div class="portlet-header ">Infusion Verification</div>
		<div class="portlet-content">
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
		<tr>
		
		<s:if test="ctlDocumentReviewData.size > 0">
			<s:iterator value="ctlDocumentReviewData" var="rowVal" status="row">
					<s:hidden name="pkInfusion" id="pkInfusion" value="%{pkInfusion}"  />
						<td class="tablewapper">Physician Notified</td>
						<td class="tablewapper1">
						<s:radio list="#{'1':'Yes','0':'No'}" name="physicianNotified" id="physicianNotified" /></td>
						<td class="tablewapper">Transfusion Date Verified</td>
						<td class="tablewapper1">
						<s:radio list="#{'1':'Yes','0':'No'}" name="transDateVerified" id="transDateVerified" />
						</td>
					
			</s:iterator>
		</s:if>
		
		<s:if test="ctlDocumentReviewData.size ==0">
			<s:hidden name="pkInfusion" id="pkInfusion" value="0"  />
		 			 <td class="tablewapper">Physician Notified</td>
					<td class="tablewapper1">
					<s:radio list="#{'1':'Yes','0':'No'}" name="physicianNotified" id="physicianNotified" /></td>
					<td class="tablewapper">Transfusion Date Verified</td>
					<td class="tablewapper1">
					<s:radio list="#{'1':'Yes','0':'No'}" name="transDateVerified" id="transDateVerified" />
					</td>
		</s:if>
		</tr>		
		</table>
		
		</div>
		</s:form>
	</div>

</div>



<span class="hidden"><s:select id="docsTitleTemp" style="display:none;" name="docsTitleTemp" list="ctlDocumentType" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></span>
<div class="cleaner"></div>



<div class="column">
		<div  class="portlet">
			<div class="portlet-header ">Document Review</div>
				<div class="portlet-content">
					
				<div id="tabs">
				    <ul >
				     <li><a href="#tabs-1">Current</a></li>
				      <li><a href="#tabs-2">Historic</a></li>
				    </ul>
			  <div id="tabs-1">   
				<br>
				<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="add_documentReview()"/>&nbsp;&nbsp;<label  class="cursor" onclick="add_documentReview()"><b>Add </b></label>&nbsp;</div></td>
 				<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="delete_documentReview()" />&nbsp;&nbsp;<label class="cursor" onclick="delete_documentReview()"><b>Delete</b></label>&nbsp;</div></td> 
					<!-- <td style="width:20%"><div><img src = "images/icons/save3.png" style="width:16;height:16;" class="cursor" id ="" onclick="save_documentReview();" >&nbsp;&nbsp;<label id="schild" class="cursor" onclick="save_documentReview();"><b>Save</b></label>&nbsp;</div></td> -->
				</tr>
				</table>
				<s:form id="attachedDocsForm" name="attachedDocsForm" >
				<div class="tableOverflowDiv">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" id="documentreviewTable" class="display">
					<thead>
						<tr>
							<th width="4%" id="documentreviewColumn"></th>
							<th>Document Title</th>
							<th>Document Date</th>
							<th>Attachment</th>
							<th>Reviewed</th>
							<th>Version #</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				</div>
				</s:form>
				</div>	
			<div id="tabs-2">
			 
			  <div class="tableOverflowDiv">
			  <table cellpadding="0" cellspacing="0" border="0" id="documentreviewTable1" width="100%" class="display">
					<thead>
						<tr>
							<th width="4%" id="documentreviewColumn1"></th>
							<th>Document Title</th>
							<th>Document Date</th>
							<th>Attachment</th>
							<th>Reviewed</th>
							<th>Version #</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				</div>
			</div>	
			</div>
				
				</div>	
			</div>
			
</div>

<div class="cleaner"></div>
 <form onsubmit="return avoidFormSubmitting()">
	<div align="left" style="float:right;">
	    <table width="100%" cellpadding="0" cellspacing="0" class="" align="" border="0">
	        <tr>
	        <td><span><label>Next:</label>
	        <select name="nextOption" id="nextOption" class=""><option value="loadCTLTechHome.action">HomePage</option><option value="logout.action">Logout</option></select>
	        <input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
	        <input type="button" value="Next" onclick="verifyeSign('eSignNext','next')"/></span></td>
	        </tr>
	    </table>
	</div>
</form>  

<script>
	$(function() {
		$( "#tabs" ).tabs();
		$(".portlet").addClass(
				"ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
				.find(".portlet-header").addClass(
						"ui-widget-header ui-corner-all").prepend(
						"<span class='ui-icon ui-icon-minusthick'></span>");

		/**For add new popup**/

		$(".portlet")
				.addClass(
						"ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
				.find(".addnew")
				.append(
						"<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");



		$(".portlet-header .ui-icon").click(
				function() {
					$(this).toggleClass("ui-icon-minusthick").toggleClass(
							"ui-icon-plusthick");
					$(this).parents(".portlet:first").find(".portlet-content")
							.toggle();
				});

	});
</script>





