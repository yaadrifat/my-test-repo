
<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/barcodeScanBox.js"></script>
<script type="text/javascript" src="js/ajaxfileupload.js"></script>
<script type="text/javascript" src="jsp/ctl/technicians/js/productTable.js"></script>
<script type="text/javascript" src="jsp/ctl/supervisor/js/productprocess.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	show_slidecontrol("slidecontrol");
	show_slidewidgets("ctl_slidewidget");
	 $('#tmpTransfusionStartTime').timepicker({});
	 $('#tmpTransfusionEndTime').timepicker({});
	 showTracker($("#dailyAssignment").val(),null,ctlProductFlowType,null,false,true);
	 constructProductList(true);
	 loadInfusionInfoData();
	 var refspecimen=$("#productDataTable tbody tr").first().find("td").find("#pkSpecimen").val();
	 productAssoPerson(refspecimen);
	 $(".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true, changeYear: true, yearRange: '1900:' + new Date().getFullYear()});
	 $('select').uniform();
});
function getScanData(event,obj){
	if(event.keyCode==13){
 		 var selectedProduct = $("#searchProducts").val();
 	     var index = selectedProduct.indexOf($("#conformProductSearch").val());
 	     if(index==-1){
 	         if(selectedProduct == "," ){
 	             selectedProduct+=$("#conformProductSearch").val();
 	             var slicedVal = selectedProduct.substring(1);
 	             $("#searchProducts").val(slicedVal);
 	             criteriaValue = "'"+slicedVal+"'";
 	         }else{
 	             selectedProduct+=","+$("#conformProductSearch").val()+"";
 	             $("#searchProducts").val(selectedProduct);
 	             var str = selectedProduct.lastIndexOf(",");
 	             var slicedVal = selectedProduct.substring(str,selectedProduct.length);
 	             criteriaValue+= ",'"+slicedVal.substring(1)+"'";
 	             }
 	     }
 	     
 		constructProductList(true,criteriaValue);
  	
  	}
}
function savePages(mode){	
	try{
			if(saveinfusionInformation(mode)){
						
				}  
				
				return true;
			
		
	}catch(e){
		alert("exception " + e);
	}
}	

function loadInfusionInfoData(){
	
	
	var url = "loadInfusionInfoData";
	dailyAssignval=$("#dailyAssignment").val();
	if(dailyAssignval !=0 && dailyAssignval != ""){
		 var jsonData = "dailyAssignment:"+dailyAssignval;
		response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	}
	$(response.ctlInfusionInformation).each(function(i){
		$("#pkInfusionInformation").val(this.pkInfusionInformation);
		$("#transfusionNurseId").val(this.transfusionNurseId);
		
		if(this.casseteDocontaminate==1){
			$("#casseteDocontaminate_yes").prop("checked",true);
		}else{
			$("#casseteDocontaminate_no").prop("checked",true);
		}
		$("#transfusionVolume").val(this.transfusionVolume);
		if(this.transfusionEndDate!= null && this.transfusionEndDate!= ""){
			var transfusionEndDate=converDate(this.transfusionEndDate);
			}
			$("#transfusionEndDate").val(transfusionEndDate);
		if(this.transfusionStartDate!= null && this.transfusionStartDate!= ""){
			var transfusionStartDate=converDate(this.transfusionStartDate);
			}
			$("#transfusionStartDate").val(transfusionStartDate);
			
			 if(this.transfusionStartTime!=null && this.transfusionStartTime!=""){
					var tmpDate = new Date(this.transfusionStartTime);
					if(tmpDate != null && tmpDate != "" && $.browser.msie){
						var tmpDate1 = this.transfusionStartTime.split("-");
						var tmpTime = tmpDate1[2].substring(3,8);
						$("#tmpTransfusionStartTime").val(tmpTime);
					}else{
						
						transfusionStartTime = (tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes();
						$("#tmpTransfusionStartTime").val(transfusionStartTime);
					}

				}else{
					$("#tmpcollectionEndTime").val("");
				}
			 setTransfusionStartTime();
			 if(this.transfusionEndTime!=null && this.transfusionEndTime!=""){
					var tmpDate = new Date(this.transfusionEndTime);
					if(tmpDate != null && tmpDate != "" && $.browser.msie){
						var tmpDate1 = this.transfusionEndTime.split("-");
						var tmpTime = tmpDate1[2].substring(3,8);
						$("#transfusionEndTime").val(tmpTime);
					}else{
						
						transfusionEndTime = (tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes();
						$("#tmpTransfusionEndTime").val(transfusionEndTime);
					}

				}else{
					$("#tmpTransfusionEndTime").val("");
				}
			 setTransfusionEndTime();
	});
}

		function saveinfusionInformation(mode){
						var url = "saveInfusionInformation";
						if(mode =="next"){
							$("#nextFlag").val("1");
						}	
						var results=JSON.stringify(serializeFormObject($('#transfusionInformationForm')));
					    results = results.substring(1,(results.length-1)); 
							
						var jsonData = "jsonData={infusionInformationData:{"+results+"}}";
							//alert("jsonData==="+jsonData);
								
								
							   
					var response = jsonDataCall(url,jsonData);
					 alert("Data Saved Successfully");
					 return true;
		}	
		
		function setTransfusionStartTime(){
			$('#transfusionStartTime').val($('#transfusionStartDate').val() +" "+ $('#tmpTransfusionStartTime').val())
		}  	

		function setTransfusionEndTime(){
			$('#transfusionEndTime').val($('#transfusionEndDate').val() +" "+ $('#tmpTransfusionEndTime').val())	
		}


</script>

<div id="ctl_slidewidget" style="display:none;">

	<!-- Recipient Info start-->
				<div  class="portlet">
    	<div class="portlet-header ">Recipient Info</div>
		<div class="portlet-content">
			
							<table width="100%" border="0">
								<tr>
									<td width="50%"><s:text name="stafa.label.recipientmrn"/></td>
									<td width="50%" id="recpMRN">&nbsp;</td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.name"/></td>
									<td id="recpName">&nbsp;</td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.dob"/></td>
									<td id="recpDOB">&nbsp;</td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.donoraborh"/></td>
									<td id="recpAboRh">&nbsp;</td>
								</tr>
								
								<tr>
									<td>ABs</td>
									<td id="recpAbS">&nbsp;</td>
								</tr>
								
								<tr>
									<td>Diagnosis</td>
									<td id="recpDiagnosis">&nbsp;</td>
								</tr>
								<tr>
									<td>Weight</td>
									<td id="recpWeight">&nbsp;</td>
								</tr>
							
							</table>

						
		</div>
	</div>		
				<!--Recipient Info end-->
					
<!-- Donor Info start-->
			
							
</div>


<div  id="slidecontrol" style="display:none;">
	<ul class="ulstyle">
	<span>
			<li class="listyle print_label"><img src = "images/icons/Bar_Code_32.png" class="barcodePrint" onclick="" style="width:30px;height:30px;"/></li>
			<li class="listyle print_label" ><b><s:text name="stafa.label.label"/></b> </li>
	</span>
	<span id="notesSpan">
		<li class="listyle notes_icon"  style="margin-left:18%"><img src = "images/icons/notes.png"  onclick="" style="width:30px;height:30px;"/><sup style="margin-left:2%"><span class='subspan'></span></sup></li>
		<li class="listyle notes_icon"><b><s:text name="stafa.label.notes"/></b> </li>
	</span>
	<span>
		<li class="listyle print_label"><img src = "images/icons/labresults.png"  style="width:30px;height:30px;cursor:pointer;" onclick='addNewPopup("Lab Result","modalpopup","loadLabresult","600","1050");'/></li>
		<li class="listyle print_label" ><b><s:text name="stafa.label.labresult"/></b> </li>
	</span>
	<span>
		<li class="listyle medication_icon"><img src = "images/icons/soplib.png" onclick='addNewPopup("SOP Library","modalpopup","loadSOPModal","600","1050");' style="width:30px;height:30px;"/></li>
		<li class="listyle medication_icon"><b>SOP</b></li>
	</span>
	<span>
	<span>
		<li class="listyle terminate_icon"><img src = "images/icons/billchecklist.png"  style="width:30px;height:30px;"/></li>
		<li class="listyle terminate_icon"><b>Billing CheckList</b></li>
	</span>
		<li class="listyle transfer_icon"><img src = "images/icons/transfer.png" onclick=""  style="width:30px;height:30px;"/></li>
		<li class="listyle transfer_icon" id="transfer_icon"><b><s:text name="stafa.label.transfer"/></b></li>
	</span>
	</ul>
</div>

<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Product Table</div>
		<div class="portlet-content">
		<form>
		<!-- <input type="hidden" name="entityType" id="entityType" value="PRODUCT" /> -->
	<!-- 	<input type="hidden" name="entityType" id="entityType" value="CTL" /> -->
           <input type="hidden" id="scanId" name="scanId"/>
		 <div id="button_wrapper">
      				<div id="divProductScan" style="margin-left:40%;">
       					<input class="scanboxSearch barcodeSearch multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;" onkeyup="performScanData(event,this)"/>
						<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" onfocus="changeType(this)"/>
 					</div>
 			</div>		
				<table cellpadding="0" cellspacing="0" border="1" id="productDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='productDataTableColumn'></th>
							
							<th>Product ID</th>
							<th>Recipient Name</th>
							<th>Recipient MRN</th>
							<th>Recipient ABO/Rh</th>
							<th>Donor Name</th>
							<th>Donor MRN</th>
							<th>Donor ABO/Rh</th>
							<th>Donor Type</th>
							<th>Product Type</th>
							<th>Product Status</th>
						</tr>
					</thead>
				</table>
			</form>	
			</div>
		</div>
	</div>


<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Transfusion Information</div>
		<div class="portlet-content">
		<form id="transfusionInformationForm">
				<input type="hidden" name="pkInfusionInformation" id="pkInfusionInformation" value="0" />
				 <s:hidden id="dailyAssignment" name="dailyAssignment" value="%{dailyAssignment}" />
				<input type="hidden" name="fkProduct" id="fkProduct"/>
				<s:hidden id="nextFlag" name="nextFlag" value="0" /> 
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Transfusion Start Date</td>
					<td class="tablewapper1"><input id="transfusionStartDate" name="transfusionStartDate" type="text" class="dateEntry"/></td>
					<td class="tablewapper">Transfusion Nurse ID</td>
					<td class="tablewapper1"><input id="transfusionNurseId"  name="transfusionNurseId" type="text"/></td>
					
					
				</tr>
				<tr>
					<td class="tablewapper">Transfusion Start Time</td>
					<td class="tablewapper1"><input id="transfusionStartTime" name="transfusionStartTime" type="hidden" class=""/><s:textfield id="tmpTransfusionStartTime" name="tmpTransfusionStartTime" Class="" onchange="setTransfusionStartTime();"/></td>
					<td class="tablewapper">Transfusion End Date</td>
					<td class="tablewapper1"><input id="transfusionEndDate"  name="transfusionEndDate" type="text" class="dateEntry" /></td>
					
					
				</tr>
				<tr>
					<td class="tablewapper">Transfusion Volume(mL)</td>
					<td class="tablewapper1"><input id="transfusionVolume" name="transfusionVolume" type="text" class=""/></td>
					<td class="tablewapper">Transfusion End Time</td>
					<td class="tablewapper1"><input id="transfusionEndTime" name="transfusionEndTime" type="hidden" /><s:textfield id="tmpTransfusionEndTime" name="tmpTransfusionEndTime" Class="" onchange="setTransfusionEndTime();"/></td>
					
					
				</tr>
				<tr>
					<td class="tablewapper">Receipient Location</td>
					<td class="tablewapper1"><s:select id="fkCodelstRecipientLocation"  name="fkCodelstRecipientLocation" list="ctlRecipientLocation" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
				</tr>
				<tr>
					<td class="tablewapper" colspan="3">Cassette is Decontaminated and Return</td>
					<td class=""><input type="radio" id="casseteDocontaminate_yes" name="casseteDocontaminate" value="1"/>Yes<input type="radio" id="casseteDocontaminate" name="casseteDocontaminate" value="0"/>No</td>
				</tr>
			</table>
		</form>
		</div>
	</div>
</div>

<div align="left" style="float:right;">
	<form onsubmit="return avoidFormSubmitting()">
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
			<tr>
				<td><span class='nextContainer'>Next:
				<select name="nextOption" id="nextOption" ><option>Select</option></select>
				<input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
				<input type="button" value="Next" onclick="verifyeSign('eSignNext','next')"/></span></td>
			</tr>
		</table>
	</form>

</div> 

<script>
	$(function() {
		$("#tabs").tabs();
		$(".portlet").addClass(
				"ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
				.find(".portlet-header").addClass(
						"ui-widget-header ui-corner-all").prepend(
						"<span class='ui-icon ui-icon-minusthick'></span>");

		/**For add new popup**/

		$(".portlet")
				.addClass(
						"ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
				.find(".addnew")
				.append(
						"<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");



		$(".portlet-header .ui-icon").click(
				function() {
					$(this).toggleClass("ui-icon-minusthick").toggleClass(
							"ui-icon-plusthick");
					$(this).parents(".portlet:first").find(".portlet-content")
							.toggle();
				});

	});
</script>
