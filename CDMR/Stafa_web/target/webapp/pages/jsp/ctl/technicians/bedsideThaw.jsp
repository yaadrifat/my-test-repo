<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/barcodeScanBox.js"></script>
<script type="text/javascript" src="jsp/ctl/technicians/js/productTable.js"></script>
<script type="text/javascript" src="jsp/ctl/supervisor/js/productprocess.js"></script>
<script type="text/javascript" >


$(document).ready(function(){

	show_slidecontrol("slidecontrol");
	show_slidewidgets("ctl_sidewidget");
	show_innernorth();
	constructProductList(true);
	$.uniform.restore('select');
	$('select').uniform();
	showTracker($("#dailyAssignment").val(),null,ctlProductFlowType,null,false,true);
	var refspecimen=$("#productDataTable tbody tr").first().find("td").find("#pkSpecimen").val();
	productAssoPerson(refspecimen);
	loadBedSideThaw();
    $(".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true, changeYear: true, yearRange: '1900:' + new Date().getFullYear()});
});


function getScanData(event,obj){
	if(event.keyCode==13){
		 var selectedProduct = $("#searchProducts").val();
	     var index = selectedProduct.indexOf($("#conformProductSearch").val());
	     if(index==-1){
	         if(selectedProduct == "," ){
	             selectedProduct+=$("#conformProductSearch").val();
	             var slicedVal = selectedProduct.substring(1);
	             $("#searchProducts").val(slicedVal);
	             criteriaValue = "'"+slicedVal+"'";
	         }else{
	             selectedProduct+=","+$("#conformProductSearch").val()+"";
	             $("#searchProducts").val(selectedProduct);
	             var str = selectedProduct.lastIndexOf(",");
	             var slicedVal = selectedProduct.substring(str,selectedProduct.length);
	             criteriaValue+= ",'"+slicedVal.substring(1)+"'";
	             }
	     }
	     
		constructProductList(true,criteriaValue);
 	
 	}
}
function savePages(mode){	
	try{
			 
				if(saveBedThaw(mode)){
					//alert("wait");
				}
				return true;
			
		
	}catch(e){
		alert("exception " + e);
	}
}
function saveBedThaw(mode){
	var url = "saveBedThaw";
	if(mode =="next"){
		$("#nextFlag").val("1");
	}
	var results=JSON.stringify(serializeFormObject($('#bedThawForm')));
    results = results.substring(1,(results.length-1)); 
		
	var jsonData = "jsonData={bedThawData:{"+results+"}}";
	var response = jsonDataCall(url,jsonData);
	alert("Data Saved Successfully");
	return true;
}
function loadBedSideThaw(){
	
	var url = "loadBedSideThawData";
	var dailyAssignval=$("#dailyAssignment").val();
	var thawtype=$("#thawType").val();
	if(dailyAssignval !=0 && dailyAssignval != ""){
		 var jsonData = "dailyAssignment:"+dailyAssignval+",thawType:"+thawtype;
		response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	}
	setBedSideThawData(response);
}

function setBedSideThawData(response){
	
	$(response.ctlBedsideThaw).each(function(i,v){
		$("#thawId").val(this.thawId);
		$("#codelstMethod").val(this.codelstMethod);
		$("#codelstStation").val(this.codelstStation);
		$("#codelstWaterBath").val(this.codelstWaterBath);
		$("#thawWBCCount").val(this.thawWBCCount);
		$("#thawWaterBathTemp").val(this.thawWaterBathTemp);
		$("#thrawVolume").val(this.thrawVolume);
		$("#totalCellCD34").val(this.totalCellCD34);
		$("#totalCellPer10Kg").val(this.totalCellPer10Kg);
		$("#totalCellPer8Kg").val(this.totalCellPer8Kg);
		if(this.thawIsACD==1){
			$("#thawIsACD1").prop("checked",true);
		}else if(this.thawIsACD==0){
			$("#thawIsACD0").prop("checked",true);
		}else if(this.thawIsACD==2){
			$("#thawIsACD2").prop("checked",true);
		}
		
		if(this.thawIsClean==1){
			$("#thawIsClean1").prop("checked",true);
		}else if(this.thawIsClean==0){
			$("#thawIsClean0").prop("checked",true);
		}
		if(this.thawIsPostClean==1){
			$("#thawIsPostClean1").prop("checked",true);
		}else if(this.thawIsClean==0){
			$("#thawIsPostClean0").prop("checked",true);
		}
		
		if(this.thawIsWaterClean==1){
			$("#thawIsWaterClean1").prop("checked",true);
		}else if(this.thawIsWaterClean==0){
			$("#thawIsWaterClean0").prop("checked",true);
		}
		
		thawingDate=converDate(this.thawingDate);
		$("#thawingDate").val(thawingDate);
		
		
	});
	$.uniform.restore("select");
	  $("select").uniform();
	
	
}

</script>

<div id="ctl_sidewidget" style="display:none">
	<div  class="portlet">
    	<div class="portlet-header ">Recipient Info</div>
		<div class="portlet-content">
			
							<table width="100%" border="0">
								<tr>
									<td width="50%"><s:text name="stafa.label.recipientmrn"/></td>
									<td width="50%" id="recpMRN">&nbsp;</td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.name"/></td>
									<td id="recpName">&nbsp;</td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.dob"/></td>
									<td id="recpDOB">&nbsp;</td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.donoraborh"/></td>
									<td id="recpAboRh">&nbsp;</td>
								</tr>
								
								<tr>
									<td>ABs</td>
									<td id="recpAbS">&nbsp;</td>
								</tr>
								
								<tr>
									<td>Diagnosis</td>
									<td id="recpDiagnosis">&nbsp;</td>
								</tr>
								<tr>
									<td>Weight</td>
									<td id="recpWeight">&nbsp;</td>
								</tr>
							
							</table>

						
		</div>
	</div>

</div>


<div  id="slidecontrol" style="display:none;">
	<ul class="ulstyle">
	<span>
			<li class="listyle print_label"><img src = "images/icons/Bar_Code_32.png" class="barcodePrint" onclick="" style="width:30px;height:30px;"/></li>
			<li class="listyle print_label" ><b><s:text name="stafa.label.label"/></b> </li>
	</span>
	<span id="notesSpan">
		<li class="listyle notes_icon"  style="margin-left:18%"><img src = "images/icons/notes.png"  onclick="" style="width:30px;height:30px;"/><sup style="margin-left:2%"><span class='subspan'></span></sup></li>
		<li class="listyle notes_icon"><b><s:text name="stafa.label.notes"/></b> </li>
	</span>
	<span>
		<li class="listyle print_label"><img src = "images/icons/labresults.png"  style="width:30px;height:30px;cursor:pointer;" onclick='addNewPopup("Lab Result","modalpopup","loadLabresult","600","1050");'/></li>
		<li class="listyle print_label" ><b><s:text name="stafa.label.labresult"/></b> </li>
	</span>
	<span>
		<li class="listyle medication_icon"><img src = "images/icons/soplib.png" onclick='addNewPopup("SOP Library","modalpopup","loadSOPModal","600","1050");' style="width:30px;height:30px;"/></li>
		<li class="listyle medication_icon"><b>SOP</b></li>
	</span>
	<span>
	<span>
		<li class="listyle terminate_icon"><img src = "images/icons/billchecklist.png"  style="width:30px;height:30px;"/></li>
		<li class="listyle terminate_icon"><b>Billing CheckList</b></li>
	</span>
		<li class="listyle transfer_icon"><img src = "images/icons/transfer.png" onclick=""  style="width:30px;height:30px;"/></li>
		<li class="listyle transfer_icon" id="transfer_icon"><b><s:text name="stafa.label.transfer"/></b></li>
	</span>
	</ul>
</div>


<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Product Table</div>
		<div class="portlet-content">
		<form>
		
		 <input type="hidden" id="scanId" name="scanId"/>
		<div id="button_wrapper">
		
			        		<div id="divProductScan" style="margin-left:40%;">
			       				 <input class="scanboxSearch barcodeSearch multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Recipient ID"  style="width:210px;" onkeyup="performScanData(event,this)" />
								<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Recipient ID"  style="width:210px;display:none;" onfocus="changeType(this)" />
					 		</div>
				        </div>
				        </form>
			<div id="productDataTableDiv"
				style="overflow-x: auto; overflow-y: hidden;">
				<table cellpadding="0" cellspacing="0" border="1" id="productDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='productDataTableColumn'></th>
							
							<th>Product ID</th>
							<th>Recipient Name</th>
							<th>Recipient MRN</th>
							<th>Recipient ABO/Rh</th>
							<th>Donor Name</th>
							<th>Donor MRN</th>
							<th>Donor ABO/Rh</th>
							<th>Donor Type</th>
							<th>Product Type</th>
							<th>Product Status</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<s:form id="bedThawForm">
<s:hidden id="dailyAssignment" name="dailyAssignment" value="%{dailyAssignment}" />
<s:hidden name="thawId" id="thawId" value="0" />
<s:hidden name="thawType" id="thawType" value="bedSideThaw" />
<s:hidden id="nextFlag" name="nextFlag" value="0" />
<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Pre-Thaw Information</div>
		<div class="portlet-content">
		<div id="productretrivaltabs">
				  
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Product Volume(mL)</td>
					<td class="tablewapper1"><s:textfield id="thrawVolume" name="thrawVolume"/></td>
					<td class="tablewapper">Total Cell/kg(*10^8/kg)</td>
					<td class="tablewapper1"><s:textfield id="totalCellPer8Kg"  name="totalCellPer8Kg"/></td>
					
					
				</tr>
				<tr>
					<td class="tablewapper">Total Cells(*10^10)</td>
					<td class="tablewapper1"><s:textfield id="totalCellPer10Kg"  name="totalCellPer10Kg"/></td>
					<td class="tablewapper">Total CD34+Cells</td>
					<td class="tablewapper1"><s:textfield id="totalCellCD34" name="totalCellCD34"/></td>
					
					
				</tr>
			</table>
		
		</div>
	</div>
</div>
</div>

<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Thaw Station Setup</div>
		<div class="portlet-content">
		
				  
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Thawing Station</td>
					<td class="tablewapper1"><s:select id="codelstStation" name="codelstStation" list="lstThawStation" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"></s:select></td>
					<td class="tablewapper">Thawing Method</td>
					<td class="tablewapper1"><s:select id="codelstMethod" name="codelstMethod" list="lstThawMethod" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"></s:select></td>
					
					
				</tr>
				<tr>
					<td class="tablewapper">Station Pre-Cleaned</td>
					<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" id="thawIsClean" name="thawIsClean"/></td>
					
					
					
				</tr>
			</table>
		
		</div>
	</div>
</div>


<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Thaw Information</div>
		<div class="portlet-content">
		
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Water Bath#</td>
					<td class="tablewapper1"><s:select id="codelstWaterBath" name="codelstWaterBath"  list="lstThawWaterBath" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"></s:select></td>
					<td class="tablewapper">WBC Count(*10^6/mL)</td>
					<td class="tablewapper1"><s:textfield id="thawWBCCount" name="thawWBCCount" /></td>
					
					
				</tr>
				<tr>
					<td class="tablewapper">Water Bath Temp.(C)</td>
					<td class="tablewapper1"><s:textfield id="thawWaterBathTemp" name="thawWaterBathTemp" /></td>
					<td class="tablewapper">ACD Added</td>
					<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No','2':'N/A'}" id="thawIsACD"  name="thawIsACD"/></td>
				</tr>
				<tr>
					<td class="tablewapper">Thawing Date</td>
					<td class="tablewapper1"><input type="text" id="thawingDate" name="thawingDate" class="dateEntry" /></td>
					<td class="tablewapper">Water Bath Cleaned</td>
					<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" id="thawIsWaterClean" name="thawIsWaterClean"/></td>
				</tr>
					<tr>
					<td class="tablewapper">Thaw Station Post-Cleaned</td>
					<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" id="thawIsPostClean" name="thawIsPostClean"/></td>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
		

	</div>
</div>
</div>
<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Reagents & Supplies</div>
		<div class="portlet-content">
		
		

	</div>
</div>
</div>
<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Equiment</div>
		<div class="portlet-content">
		
		

	</div>
	
	
</div>
</div>
</s:form>
<div align="left" style="float:right;">
	<form onsubmit="return avoidFormSubmitting()">
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
			<tr>
				<td><span class='nextContainer'>Next:
				<select name="nextOption" id="nextOption" ><option>Select</option></select>
				<input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
				<input type="button" value="Next" onclick="verifyeSign('eSignNext','next')"/></span></td>
			</tr>
		</table>
	</form>

</div> 
<script>
	$(function() {
		$(".portlet").addClass(
				"ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
				.find(".portlet-header").addClass(
						"ui-widget-header ui-corner-all").prepend(
						"<span class='ui-icon ui-icon-minusthick'></span>");

		/**For add new popup**/

		$(".portlet")
				.addClass(
						"ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
				.find(".addnew")
				.append(
						"<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");



		$(".portlet-header .ui-icon").click(
				function() {
					$(this).toggleClass("ui-icon-minusthick").toggleClass(
							"ui-icon-plusthick");
					$(this).parents(".portlet:first").find(".portlet-content")
							.toggle();
				});

	});
</script>