
<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/barcodeScanBox.js"></script>

<script type="text/javascript" src="js/jquery/jquery.loadjson.js"></script>
<script type="text/javascript" src="jsp/ctl/supervisor/js/productprocess.js"></script>
<style type="text/css">
       /*  body { font-family:Lucida Sans, Lucida Sans Unicode, Arial, Sans-Serif; font-size:13px; margin:0px auto;} */
        .tabs { margin:0; padding:0; list-style:none; overflow:hidden; }
        .tabs li { float:left; display:block; padding:5px; background-color:#bbb; margin-right:5px;}
        .tabs li a { color:#fff; text-decoration:none; }
        .tabs li.current { background-color:#e1e1e1;}
        .tabs li.current a { color:#000; text-decoration:none; }
        .tabs li a.remove { color:#f00; margin-left:10px;}
        .content { background-color:#FFFFFF;}
        .content p { margin: 0; padding:20px 20px 100px 20px;}
        
        

    </style>
<script type="text/javascript" >
var EntityID;
var EntityType;

$(document).ready(function(){
	$(".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
        changeYear: true, yearRange: "c-50:c+45"});
	 $('select').uniform();	
	 $("#inspectionContent").hide();
	 $('#tmpTransportTime').timepicker({});
	 $('#tmpIssueTime').timepicker({});
	 showTracker($("#dailyAssignment").val(),null,ctlProductFlowType,null,false,true);
	 constructProductList(true);
	 EntityID=$("#entityId").val();
	 EntityType=$("#entityType").val();
	 loadquestions(EntityID,EntityType);
	 loadPreparationData();
	 var refspecimen=$("#productDataTable tbody tr").first().find("td").find("#pkSpecimen").val();
	 productAssoPerson(refspecimen);
	 var inspection_li=$('#inspectionTab_ul li').first().find("a").first().attr("id");
	$("#"+inspection_li).trigger("click");
});


function loadPreparationData(){
	var refproduct="";
	var dailyAssignval="";
	var url = "loadInfusionPreparationData";
	dailyAssignval=$("#dailyAssignment").val();
	insEntityType=$("#inspectionEntityType").val();
	if(dailyAssignval !=0 && dailyAssignval != "" && typeof(dailyAssignval)!='undefined'){
		 var jsonData = "dailyAssignment:"+dailyAssignval+",inspectionEntityType:"+insEntityType;
		 preparationResponse = jsonDataCall(url,"jsonData={"+jsonData+"}");
	}
	var listlength=preparationResponse.ctlInspection.length;
	addTab("inspectionTab_ul","inspectionContent","inspectionForm",listlength);
    if(preparationResponse.ctlInspection.length>0){
    $(preparationResponse.ctlInspection).each(function(i){
     	  productid=this.product;
     	 $("#inspectionTab_ul li").find("input").each(function(j,v){
     		var refprod=$(this).attr("refproductid");
     	 var hiddenid=$(this).attr("id");
     	 
     	  if(refprod==productid){
     		
     		  $(this).val(JSON.stringify(preparationResponse.ctlInspection[i]));
     		loadData1(hiddenid,"inspectionForm",refprod,'inspectionTab_ul');
     	  } 
     	
     	 });
    });
    } 
    if(preparationResponse.ctlPreparation.length>0){
	setPreparationData(preparationResponse);
    }
}	

	function setPreparationData(preparationResponse){
		$(preparationResponse.ctlPreparation).each(function(i,v){
			$("#pkPreparation").val(this.pkPreparation);
	 	if(this.arrivalHighTemp!='N/A'){
			$("#arrivalHighTemp").val(this.arrivalHighTemp);
			$("#check_arrivalHighTemp").attr("checked",false)
		}else{
			$("#arrivalHighTemp").val(this.arrivalHighTemp);
			$("#check_arrivalHighTemp").attr("checked",true)
			
		}
		if(this.arrivalLowTemp!='N/A'){
			$("#arrivalLowTemp").val(this.arrivalLowTemp);
			$("#check_arrivalLowTemp").attr("checked",false)
	}else{
			$("#arrivalLowTemp").val(this.arrivalLowTemp);
			$("#check_arrivalLowTemp").attr("checked",true)
		} 
		if(this.transportLowTemp!='N/A'){
			$("#transportLowTemp").val(this.transportLowTemp);
			$("#check_transportLowTemp").attr("checked",false)
	}else{
			$("#transportLowTemp").val(this.transportLowTemp);
			$("#check_transportLowTemp").attr("checked",true)
		} 
		if(this.transportTechSign!='N/A'){
			$("#transportTechSign").val(this.transportTechSign);
			$("#check_techsign").attr("checked",false)
	}else{
			$("#transportTechSign").val(this.transportTechSign);
			$("#check_techsign").attr("checked",true)
		} 
		$("#fkCodelstTransportVessels").val(this.fkCodelstTransportVessels);
		$("#receivingPersonalId").val(this.receivingPersonalId);
		$("#transportHighTemp").val(this.transportHighTemp);
		if(this.passedInspection==1){
			$("#passedInspection1").prop("checked",true);
		}else if(this.passedInspection==0){
			$("#passedInspection0").prop("checked",true);
		}
		
		if(this.productIssued==1){
			$("#productIssued1").prop("checked",true);
		}else if(this.productIssued==0){
			$("#productIssued0").prop("checked",true);
		}
		if(this.productReturned==1){
			$("#productReturned1").prop("checked",true);
		}else if(this.productReturned==0){
			$("#productReturned0").prop("checked",true);
		}
		issuedate=converDate(this.issueDate);
		$("#issueDate").val(issuedate);
		transdate=converDate(this.transportDate);
		$("#transportDate").val(transdate);
	
		var issuetime=timeConversion(this.issueTime);
		$("#tmpIssueTime").val(issuetime);
		setIssueTime();
		var transporttime=timeConversion(this.transportTime);
		$("#tmpTransportTime").val(transporttime);
		setTransportTime();
	
	});
		
		
	}
	 function timeConversion(result){
		 if(result!=null && result!=""){
				var tmpDate = new Date(result);
			
				if(tmpDate != null && tmpDate != "" && $.browser.msie){
					var tmpDate1 = result.split("-");
					var tmpTime = tmpDate1[2].substring(3,8);
					result=tmpTime;
				}else{
					
					result = (tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes();
				//	$("#tmpTransfusionEndTime").val(transfusionEndTime);
					result=result;
				}

			}
		 
		 return result;
	 }
	 function loadData1(hiddenid,formid,refproduct,tabid){
			var data="";
			data=$("#"+hiddenid).val();
			var result = jQuery.parseJSON(data);
	    	
			$("#"+formid).loadJSON(result);
			$.uniform.restore('select');
			$('select').uniform();
			loadData(hiddenid,formid,refproduct);
		
	 }
	 	
function setIssueTime(){
	$('#issueTime').val($('#issueDate').val() +" "+ $('#tmpIssueTime').val())
} 
function setTransportTime(){
	$('#transportTime').val($('#transportDate').val() +" "+ $('#tmpTransportTime').val())
} 

function constructProductList(flag,criteriaValue){
	var criteria = " ";
	



	$("#scanId").val($("#divProductScan #fake_conformProductSearch").val() );
	//alert($("#scanId").val());
	criteria = " ";
	if($("#scanId").val()!=""){
		//productCriteria= " lower(SPEC_ID) like lower('%"+ criteriaValue +"%')";
		criteria= " or lower(SPEC_ID) like lower('%" + $("#scanId").val() +"%')";
		
	}
		//$("#scanId").val($("#divProductScan #fake_conformProductSearch").val() );
		//criteria = " and SPEC_ID in ("+criteriaValue+")";
	

	var productDataTable_serverParam = function ( aoData ) {
																aoData.push( { "name": "application", "value": "stafa"});
																aoData.push( { "name": "module", "value": "Ctl_reviewProduct_List"});
																aoData.push( { "name": "criteria", "value": criteria});	
																
																aoData.push( { "name": "col_1_name", "value": "lower(nvl( SPEC_ID ,' '))"});
																aoData.push( { "name": "col_1_column", "value": "7"});
																
																aoData.push( { "name": "col_2_name", "value": "lower(nvl( (select nvl(PERSON_LNAME, ' ' ) || ' ' ||nvl(PERSON_FNAME, ' ' ) from person where pk_person=d.fk_receipent),' '))"});
																aoData.push( { "name": "col_2_column", "value": "2"});
																
																aoData.push( {"name": "col_3_name", "value": "lower(nvl((select nvl(person_mrn,' ') from person where pk_person=d.fk_receipent),' '))" } );
																aoData.push( { "name": "col_3_column", "value": "1"});

																aoData.push( { "name": "param_count", "value":"2"});
																aoData.push( { "name": "param_1_name", "value":":dailyAssignment:"});
																aoData.push( { "name": "param_1_value", "value":$("#dailyAssignment").val()}); 
																
																aoData.push( { "name": "param_2_name", "value":":criteria:"});
																aoData.push( { "name": "param_2_value", "value":criteria}); 
																 
																
															/* 	aoData.push( { "name": "col_4_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst = (select fk_codelst_bloodgrp from person where pk_person=d.fk_receipent)),' '))"});
																aoData.push( { "name": "col_4_column", "value": "3"});
																
																aoData.push( { "name": "col_4_name", "value": "lower(nvl(p.PERSON_MRN, ' '))"});
																aoData.push( { "name": "col_4_column", "value": "4"});
																
																aoData.push( { "name": "col_5_name", "value": "lower(nvl(p.PERSON_LNAME, ' ' )|| ' ' || nvl(p.PERSON_FNAME, ' ' ))"});
																aoData.push( { "name": "col_5_column", "value": "5"});
																
																aoData.push( { "name": "col_6_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst = p.fk_codelst_bloodgrp),0))"});
																aoData.push( { "name": "col_6_column", "value": "6"});
																
																aoData.push( { "name": "col_7_name", "value": "lower(nvl( SPEC_ID ,' '))"});
																aoData.push( { "name": "col_7_column", "value": "7"});
																
																aoData.push( { "name": "col_8_name", "value": "lower(nvl((select CODELST_DESC from er_codelst where PK_CODELST=d.FK_CODELSTPRODUCTTYPE),' '))"});
																aoData.push( { "name": "col_8_column", "value": "8"});
																
																aoData.push( { "name": "col_9_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst = d.fk_codelstdonortype),' '))"});
																aoData.push( { "name": "col_9_column", "value": "9"});
																
																aoData.push( { "name": "col_10_name", "value": ""});
																aoData.push( { "name": "col_10_column", "value": ""}); */ 
													};
	var productDataTable_aoColumnDef = [
			                         {			 "aTargets": [ 0], "mDataProp": function ( source, type, val ) {
			                        	 			prodid=source[7];
			                             			 return "<input name='productcheck' id= 'productcheck' type='checkbox' value='" + source[0] +"' onchange='ontrigger(this,\""+prodid+"\")'/><input name='pkSpecimen' id= 'pkSpecimen' type='hidden' value='" + source[10] +"' />";
			                             }},
			                          {    "sTitle":'Product ID',
			                     	             "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
			                     	            	 product_id="<input type='hidden' class ='"+source[7]+"' id='specId' name='specId' value='"+source[7]+"'> ";
			                     	            	product_id+=source[7];
			    							        	return product_id;
			                             			
			                             }},
			                          {    "sTitle":'Recipient Name',
			                     	              "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
					                             	 return source[2];
			                             }},
			                          {    "sTitle":'Recipient MRN',
			                                      "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
			   			                             return source[1];
			                             }},
			                          {    "sTitle":'Recipient ABO/Rh',
				                                  "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
							                         return source[3];
				                         }}, 
				                      {    "sTitle":'<s:text name="stafa.label.donorname"/>',
				                                  "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
							                         return source[5];
				                         }}, 
				                              
			                          {    "sTitle":'Donor MRN',
			                                      "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
			   			                             return source[4];
			                             }},    
			                          {    "sTitle":'Donor ABO/Rh',
			                                      "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
			   			                             return source[6];
			                             }},    
			                          {    "sTitle":'Donor Type',
			                                      "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
			   			                             return source[9];
			                             }},  
			                          {    "sTitle":'Product Type',
			                                      "aTargets": [ 9], "mDataProp": function ( source, type, val ) {
			   			                             return source[8];
			                             }},    
			                          {    "sTitle":'Product Status',
			                                      "aTargets": [ 10], "mDataProp": function ( source, type, val ) {
			   			                             return source[11];
			                             }} 
			                        ];
	var productDataTable_aoColumn = [ { "bSortable": false},
	                               	   null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null
			             ];
	var productDataTable_ColManager = function(){
										$("#productDataTableColumn").html($('#productDataTable_wrapper .ColVis'));
									 };
	constructTable(flag,'productDataTable',productDataTable_ColManager,productDataTable_serverParam,productDataTable_aoColumnDef,productDataTable_aoColumn);
} 

function getScanData(event,obj){
	
  	if(event.keyCode==13){
  		 var selectedProduct = $("#searchProducts").val();
  	     var index = selectedProduct.indexOf($("#conformProductSearch").val());
  	     if(index==-1){
  	         if(selectedProduct == "," ){
  	             selectedProduct+=$("#conformProductSearch").val();
  	             var slicedVal = selectedProduct.substring(1);
  	             $("#searchProducts").val(slicedVal);
  	             criteriaValue = "'"+slicedVal+"'";
  	         }else{
  	             selectedProduct+=","+$("#conformProductSearch").val()+"";
  	             $("#searchProducts").val(selectedProduct);
  	             var str = selectedProduct.lastIndexOf(",");
  	             var slicedVal = selectedProduct.substring(str,selectedProduct.length);
  	             criteriaValue+= ",'"+slicedVal.substring(1)+"'";
  	             }
  	     }
  		constructProductList(true,criteriaValue);
  		var inspectiontablen=$("#inspectionTab_ul").find("li").length;
  		if(inspectiontablen>=1){
  	  		 $("#inspectionTab_ul").find("li").each(function(j,v){
  				 if($(this).hasClass("current")){
  					 var hiddenid=$(this).find("input").prop('id');
  					 refproduct=$(this).find("input").prop('refproduct')
  					 $("#activeRetrivalTab").val(hiddenid);
  						loadData(hiddenid,'inspectionForm',refproduct);
  					
  				 }
  			 });
  	  		}
  	  	
  		resetForm($('#inspectionForm'));
 	   addTab("inspectionTab_ul","inspectionContent","inspectionForm");
 	  var refproduct="";
 	  $("#inspectionTab_ul").find("li").each(function(j,v){
			 if($(this).hasClass("current")){
				 var hiddenid=$(this).find("input").prop('id');
				 refproduct=$(this).find("input").prop('refproduct')
				// $("#activeInspectionTab").val("");
					 $("#activeInspectionTab").val(hiddenid); 
					 loadData(hiddenid,'inspectionForm',refproduct);
			 }
		 });
 	
  	}
}
function addTab(tab_ulid,contentid,formid,listlength){
	 var proid="";
	 var proclas="";
	 var refproduct="";
	 var tempcontend=contentid;
	 var k=0;
	 $('#productDataTable tbody tr').each(function(i,v){
			proclas=$(v).find("#specId").attr("class");  
			proid=$(v).find("#specId").val();
			refproduct=$(v).find("#pkSpecimen").val();
			
	 	if($("#conformProductSearch").val()!=proid && $("#conformProductSearch").val()!=""){
	 		
			 if ($("#"+proid).length !=0 )
				{
		        	return;
				}
		   } 
	//?BB });  
	           		 $("#"+tab_ulid+" li").removeClass("current");

						if(listlength<=0){
							$("#pkInspection").val(0);
							$("#inspectionForm_product").val(refproduct);
						}
	            	
					var results=JSON.stringify(serializeFormObject($('#'+formid)));
					
					
					 $("#"+tab_ulid).append("<li id='li_"+proid+formid +"_"+refproduct+"'class='current'><input type='hidden' id="+proid+formid+"   value='"+results+"' refproductid="+refproduct+"><a class='tab' id='a_" +
							 proid+formid+ "'href='#"+contentid+"' onclick=loadData('"+proid+formid+"','"+formid+"','"+refproduct+"') rel="+refproduct+">" + proid + 
			                "</a><a href='#' id="+proid+" class='remove' onclick=deletedtab(this,'"+proid+"','"+formid+"')>x</a></li>");
			        
			            $("#"+contentid).show();
				
	          
	            $('#'+tab_ulid+' a.tab').live('click', function() {
	                // Get the tab name
	          
	                $("#"+tab_ulid+" li").removeClass("current");
	           
	                $(this).parent().addClass("current");
	             
	            });
	          
	      			 
	 });         
			
	        
	 
	 
}  


function loadData(hiddenid,formid,refproduct){
	
		var reresult=JSON.stringify(serializeFormObject($('#'+formid)));
		 var hiddentabid= $("#activeInspectionTab").val();
		$("#"+hiddentabid).val(reresult);
		$("#activeInspectionTab").val(hiddenid);

		//to Maintain active tab and previous tab
		var data="";
		data=$("#"+hiddenid).val();
		var result = jQuery.parseJSON(data);
	
		$("#"+formid).loadJSON(result);

		$.uniform.restore('select');
		$('select').uniform();
		
}

function resetForm($form) {
    $form.find('input:text, input:password, input:file, select, textarea').val('');
    $form.find('input:radio, input:checkbox')
         .removeAttr('checked').removeAttr('selected');
    $.uniform.restore("select");
	  $("select").uniform();
}


function setvalues(textId,cBoxId){
    var s=$('#'+cBoxId).attr('checked');
    if(s=='checked'){
        $('#'+textId).val("N/A");
    }else {
        $('#'+textId).val("");
    }
}
function checkCurrentProduct(){
	 
		 $("#inspectionTab_ul").find("li").each(function(i,v){
				if($(this).hasClass("current")){
					var prid= $(this).find("input").attr("id");
					var refid=$(this).find("a").attr('rel');
					 var currentclick=$(this).find("a").first().attr("id");
					 $("#"+currentclick).trigger("click");

					
					loadData(prid,'inspectionForm',refid);
			    } 
			 });
}

function saveInfusionPreparation(mode){
	
	var url='saveInfusionPreparation';
	if(mode =="next"){
		$("#nextFlag").val("1");
	}	
	 var rowData = "";
	 checkCurrentProduct();
	 $("#inspectionTab_ul li").find("input").each(function(i,e){
			//rowdata+ =$(v).find("input[type=text]").val();
			tmpId1 = $(this).attr("id");
			
			if(i==0){
				rowData+=$("#"+tmpId1).val();
				}else{
					rowData+=","+$("#"+tmpId1).val();	
				}
			
		 });
	
		var results1=JSON.stringify(serializeFormObject($('#distributionVerification')));
		results1 = results1.substring(1,(results1.length-1));
		var results2=JSON.stringify(serializeFormObject($('#transportInformation')));
		results2= results2.substring(1,(results2.length-1));
		var results3=JSON.stringify(serializeFormObject($('#productIssueRecord')));
		results3= results3.substring(1,(results3.length-1));
		
		var jsonData = "jsonData={inspectionData:["+rowData+"],preparationData:{"+results2+","+results3+"}}";
		var response = jsonDataCall(url,jsonData);
		alert("Data Saved Sucessfully");
		return true;
}
		
 function savePages(mode){
 	
 	try{
 				if(saveInfusionPreparation(mode)){
 					//alert("wait");
 					//return false;
 				}
 				if(saveQuestions('distribution',EntityID,EntityType)){
 					//alert("wait");
 					//return false;
 				}
 				return true;
 				
 	}catch(e){
 		alert("exception " + e);
 	}
 	alert("Data Saved Successfully");
 }  
   
</script>

<div id="ctl_slidewidget" style="display:none;">

	<!-- Recipient Info start-->
				<div  class="portlet">
    	<div class="portlet-header ">Recipient Info</div>
		<div class="portlet-content">
			
							<table width="100%" border="0">
								<tr>
									<td width="50%"><s:text name="stafa.label.recipientmrn"/></td>
									<td width="50%" id="recpMRN">&nbsp;</td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.name"/></td>
									<td id="recpName">&nbsp;</td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.dob"/></td>
									<td id="recpDOB">&nbsp;</td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.donoraborh"/></td>
									<td id="recpAboRh">&nbsp;</td>
								</tr>
								
								<tr>
									<td>ABs</td>
									<td id="recpAbS">&nbsp;</td>
								</tr>
								
								<tr>
									<td>Diagnosis</td>
									<td id="recpDiagnosis">&nbsp;</td>
								</tr>
								<tr>
									<td>Weight</td>
									<td id="recpWeight">&nbsp;</td>
								</tr>
							
							</table>

						
		</div>
	</div>		
				<!--Recipient Info end-->

							
</div>


<div  id="slidecontrol" style="display:none;">
	<ul class="ulstyle">
	<span>
			<li class="listyle print_label"><img src = "images/icons/Bar_Code_32.png" class="barcodePrint" onclick="" style="width:30px;height:30px;"/></li>
			<li class="listyle print_label" ><b><s:text name="stafa.label.label"/></b> </li>
	</span>
	<span id="notesSpan">
		<li class="listyle notes_icon"  style="margin-left:18%"><img src = "images/icons/notes.png"  onclick="" style="width:30px;height:30px;"/><sup style="margin-left:2%"><span class='subspan'></span></sup></li>
		<li class="listyle notes_icon"><b><s:text name="stafa.label.notes"/></b> </li>
	</span>
	<span>
		<li class="listyle print_label"><img src = "images/icons/labresults.png"  style="width:30px;height:30px;cursor:pointer;" onclick='addNewPopup("Lab Result","modalpopup","loadLabresult","600","1050");'/></li>
		<li class="listyle print_label" ><b><s:text name="stafa.label.labresult"/></b> </li>
	</span>
	<span>
		<li class="listyle medication_icon"><img src = "images/icons/soplib.png" onclick='addNewPopup("SOP Library","modalpopup","loadSOPModal","600","1050");' style="width:30px;height:30px;"/></li>
		<li class="listyle medication_icon"><b>SOP</b></li>
	</span>
	<span>
	<span>
		<li class="listyle terminate_icon"><img src = "images/icons/billchecklist.png"  style="width:30px;height:30px;"/></li>
		<li class="listyle terminate_icon"><b>Billing CheckList</b></li>
	</span>
		<li class="listyle transfer_icon"><img src = "images/icons/transfer.png" onclick=""  style="width:30px;height:30px;"/></li>
		<li class="listyle transfer_icon" id="transfer_icon"><b><s:text name="stafa.label.transfer"/></b></li>
	</span>
	</ul>
</div>


<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Product Table</div>
		<div class="portlet-content">
		<form>
		<input type="hidden" id="activeRetrivalTab" name="activeRetrivalTab"/>
             <input type="hidden" id="activeInspectionTab" name="activeInspectionTab"/>
           <input type="hidden" id="scanId" name="scanId"/>
           <s:hidden id="searchProducts" name="searchProducts" value=","/>
		 <div id="button_wrapper">
      				<div id="divProductScan" style="margin-left:40%;">
       					<input class="scanboxSearch barcodeSearch multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;" onkeyup="performScanData(event,this)"/>
						<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" onfocus="changeType(this)"/>
 					</div>
 			</div>		
				<table cellpadding="0" cellspacing="0" border="1" id="productDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='productDataTableColumn'></th>
							<th>Product ID</th>
							<th>Recipient Name</th>
							<th>Recipient MRN</th>
							<th>Recipient ABO/Rh</th>
							<th>Donor Name</th>
							<th>Donor MRN</th>
							<th>Donor ABO/Rh</th>
							<th>Donor Type</th>
							<th>Product Type</th>
							<th>Product Status</th>
						</tr>
					</thead>
				</table>
		
			</form>	
			
			</div>
		</div>
	</div>



<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Product Inspection</div>
		<div class="portlet-content">
		<div id="producttabs">
				   <ul class="tabs"  id="inspectionTab_ul">
           
        </ul>
        <form id="inspectionForm">
             <input type="hidden" id="pkInspection" name="pkInspection" value="0"/>
             <input type="hidden" id="fkPreparation" name="fkPreparation" value="0"/>
             <input type="hidden" id="inspectionForm_product" name="product" value="0" />
             <s:hidden id="dailyAssignment" name="dailyAssignment" value="%{dailyAssignment}" /> 
             <input type="hidden" name="inspectionEntityType" id="inspectionEntityType" value="INS_PREPARATION" />
			  <div id="inspectionContent" class="content">  
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Product State</td>
					<td class="tablewapper1"><s:select  headerKey="" headerValue="Select" list="ctlProductState" listKey="pkCodelst" listValue="description" name="fkCodelstProductStatus" id="fkCodelstProductStatus"/></td>
				</tr>
				<tr>
					<td class="tablewapper">Product Appearance OK</td>
					<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="appearance" id="appearance"/></td>
					<td class="tablewapper">Label Verfication</td>
					<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="isVerified" id="isVerified"  /></td>
					
				</tr>
				<tr>
					<td class="tablewapper">Product Container Intact</td>
					<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="containerIntact" id="containerIntact"/></td>
					<td class="tablewapper">Product Requires ACD</td>
					<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="requiresACD" id="requiresACD"/></td>
				</tr>
				</table>
				</div>
		
		</form>
	</div>
</div>
</div>
</div>

<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Distribution Verification</div>
		<div class="portlet-content">
			<input type="hidden" name="entityType" id="entityType" value="INFUSION" />
			<s:hidden id="entityId" name="entityId" value="%{dailyAssignment}" />
			<form id="distributionVerification"> 
				<table id="distribution" cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td colspan="2"class="tablewapper">Release of Product Review and infusion OrderAccompaines the Product</td>
					<td class="tablewapper1" colspan="1"><input type="hidden" name="releaseInfusionOrder_id" id="releaseInfusionOrder_id" value="0"><input type="radio" id="releaseInfusionOrder_yes" name="releaseInfusionOrder" value="1"/>Yes&nbsp;<input type="radio" id="releaseInfusionOrder_no" name="releaseInfusionOrder" value="0"/>No<input type="hidden" name="questionsCode" id="questionsCode" value="releaseInfusionOrder" /></td>
				</tr>
				<tr>
					<td class="tablewapper" colspan="2">Circular of Information Accompanies the Product</td>
					<td class="tablewapper1" colspan="1"><input type="hidden" name="circulationinfo_id" id="circulationinfo_id" value="0"><input type="radio" id="circulationinfo_yes" name="circulationinfo" value="1"/>Yes&nbsp;<input type="radio" id="circulationinfo_no" name="circulationinfo" value="0"/>No<input type="hidden" name="questionsCode" id="questionsCode" value="circulationinfo" /></td>
				</tr>
				<tr>
				<td class="tablewapper" colspan="2">Product Manufacturing Record accompanies the Product</td>
				<td class="tablewapper1" colspan="1"><input type="hidden" name="productManuRecord_id" id="productManuRecord_id" value="0"><input type="radio" id="productManuRecord_yes" name="productManuRecord" value="1"/>Yes&nbsp;<input type="radio" id="productManuRecord_no" name="productManuRecord" value="0"/>No<input type="hidden" name="questionsCode" id="questionsCode" value="productManuRecord" /></td>
				</tr>
				<!-- <tr>
				<td class="tablewapper">CTL Technician 2 Signature</td>
				<td class="tablewapper1"><input type="text" id="ctlSignature" name="ctlSignature"/><input type="checkbox" id="check_ctlSignature" name="check_ctlSignature"/>N/A</td>
				</tr> -->
				</table>
		</form>
	
	</div>
</div>
</div>
<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Transport Information</div>
		<div class="portlet-content">
		
			<form id="transportInformation">	  
		
				<input type="hidden" id="pkPreparation" name="pkPreparation" value="0"/>
			    <s:hidden id="dailyAssignment" name="dailyAssignment" value="%{dailyAssignment}" /> 
				<s:hidden id="nextFlag" name="nextFlag" value="0" />
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Transport Vessel</td>
					<td class="tablewapper1"><select id=""><option>Select</option></select></td>
					<td class="tablewapper">Transport Temp.(C)-High</td>
					<td class="tablewapper1"><input id="transportHighTemp"  name="transportHighTemp" type="text"/></td>
					
					
				</tr>
				<tr>
					<td class="tablewapper">Transport Date</td>
					<td class="tablewapper1"><input id="transportDate"  name="transportDate" type="text" class="dateEntry"/></td>
					<td class="tablewapper">Transport Temp.(C)-Low</td>
					<td class="tablewapper1"><input id="transportLowTemp"  name="transportLowTemp" type="text"/><input type="checkbox" id="check_transportLowTemp" name="check_transportLowTemp" onclick="setvalues('transportLowTemp','check_transportLowTemp')"/>N/A</td>
					
					
				</tr>
				<tr>
					<td class="tablewapper">Transport Time</td>
					<td class="tablewapper1"><input id="transportTime" name="transportTime" type="hidden"/><input id="tmpTransportTime" name="tmpTransportTime" type="text" onchange="setTransportTime()"/></td>
					<td class="tablewapper">CTL Technician 2 Signature</td>
					<td class="tablewapper1"><input id="transportTechSign" name="transportTechSign" type="text"/><input type="checkbox" id="check_techsign" name="check_techsign" onclick="setvalues('transportTechSign','check_techsign')"/>N/A</td>
					
					
				</tr>
			
				
				
				</table>
		</form>
	</div>
</div>
</div>
<div class="column">
	<div class="portlet">

		<div class="portlet-header ">Product Issue Record</div>
		<div class="portlet-content">
				 <form id="productIssueRecord"> 
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Arrival Temp.(C)-High</td>
					<td class="tablewapper1"><input id="arrivalHighTemp" name="arrivalHighTemp" type="text"/><input type="checkbox" id="check_arrivalHighTemp" name="check_arrivalHighTemp" onclick="setvalues('arrivalHighTemp','check_arrivalHighTemp')"/>N/A</td>
					<td class="tablewapper">Arrival Temp.(C)-Low</td>
					<td class="tablewapper1"><input id="arrivalLowTemp"  name="arrivalLowTemp" type="text"/><input type="checkbox"" id="check_arrivalLowTemp" name="check_arrivalLowTemp" onclick="setvalues('arrivalLowTemp','check_arrivalLowTemp')"/>N/A</td>
				</tr>
				<tr>
					<td class="tablewapper">Product Issued</td>
					<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="productIssued" id="productIssued"/></td>
				</tr>
				<tr>
					<td class="tablewapper">Issued Date</td>
					<td class="tablewapper1"><input id="issueDate"  name="issueDate" type="text" class="dateEntry"/></td>
					<td class="tablewapper">Product Passed Inspection</td>
					<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="passedInspection" id="passedInspection"/></td>
					
				</tr>
				<tr>
					<td class="tablewapper">Issued Time</td>
					<td class="tablewapper1"><input id="issueTime" name="issueTime" type="hidden" class=""/><input id="tmpIssueTime" name="tmpIssueTime" type="text" class="" onchange="setIssueTime()"/></td>
					<td class="tablewapper">Receiving  Personnel ID</td>
					<td class="tablewapper1"><input id="receivingPersonalId" name="receivingPersonalId" type="text" class=""/></td>
				</tr>
			
					<tr>
					
					<td class="tablewapper">Product Returned</td>
					<td class="tablewapper1"><s:radio list="#{'1':'Yes','0':'No'}" name="productReturned" id="productReturned"/></td>
				</tr>
				
				
				</table>
		</form>
		</div>
	</div>
</div>

<div align="left" style="float:right;">
	<form onsubmit="return avoidFormSubmitting()">
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
			<tr>
				<td><span class='nextContainer'>Next:
				<select name="nextOption" id="nextOption" ><option>Select</option></select>
				<input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
				<input type="button" value="Next" onclick="verifyeSign('eSignNext','next')"/></span></td>
			</tr>
		</table>
	</form>

</div> 
<script>
$(function() {
		
		$(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all").find(".portlet-header").addClass("ui-widget-header ui-corner-all").prepend(
						"<span class='ui-icon ui-icon-minusthick'></span>");

		/**For add new popup**/

		$(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all").find(".addnew")
				.append(
						"<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");



		$(".portlet-header .ui-icon").click(
				function() {
					$(this).toggleClass("ui-icon-minusthick").toggleClass(
							"ui-icon-plusthick");
					$(this).parents(".portlet:first").find(".portlet-content")
							.toggle();
				});
	});
	
</script>
