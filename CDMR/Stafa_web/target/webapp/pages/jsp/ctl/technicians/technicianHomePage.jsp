
<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/dataSearch.js"></script>
<script type="text/javascript" >

$(document).ready(function(){
	clearTracker();
	var assignedCriteria = " and ( instr(','||ct.primary_userids||',',',"+$("#user").val() +",') >0 or instr(','||ct.secondary_userids||',',',"+$("#user").val()+",') >0 )";
	var pendingCriteria = " and ct.primary_userids is null and  ct.secondary_userids is null ";
    var orderby = " order by CT.TASK_GROUPID "
	var assignedTask_serverParam = function ( aoData ) {
															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "Ctl_TechTask"});	
															aoData.push( { "name": "criteria", "value": assignedCriteria + orderby});
														};
	var assignedTask_aoColumnDef = [
	      		                         {"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
	      		                             return "<input type='hidden' name='task' id='task' value ='"+source[0]+"'/><input type='hidden' name='taskGroup' id='taskGroup' value='"+source[1] +"'/>";
	      		                             }},
	      		                          {    "sTitle":'Task',
	      		                                 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
	      		                                	 var link= "<a href='#' onclick='javascript:ctlWorkFlow(\""+source[1] +"\");'>"+source[2] +"</a>";
	      		                                	
	      		                             return link;
	      		                             }},
	      		                          {    "sTitle":'Task Actions',
	      		                                 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
	      		                                	 link = "<select></select>";
	      				                             return link;
	      		                                 
	      		                                    }},
	      		                          {    "sTitle":'Product ID',
	      		                                        "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
	      		                                    return source[3];
	      		                                  }},
	      		                          {    "sTitle":'Recipient MRN',
	      		                                      "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
	      		                                      return source[4];
	      		                              }},
	      		                          {    "sTitle":'Recipient Name',
	      		                                  "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
	      		                                      return source[5];
	      		                              }},
	      		                          {    "sTitle":'Recipient ABO/Rh',
	      		                                  "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
	      		                                	 
	      		                                	  return source[6];
	      		                                	 	      		                                     
	      		                              }},
	      		                          {    "sTitle":'Donor ABO/Rh',
	      		                                  "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
	      		                                      return source[7];
	      		                              }},
	      		                              {    "sTitle":'Product Type',
	      		                                  "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
	      		                                      return source[8];
	      		                              }},
	      		                              {    "sTitle":'Donor Type',
	      		                                  "aTargets": [ 9], "mDataProp": function ( source, type, val ) {
	      		                                      return source[9];
	      		                              }},
	      		                              {    "sTitle":'Station',
	      		                                  "aTargets": [ 10], "mDataProp": function ( source, type, val ) {
	      		                                	  return source[10];
	      		                              }},
	      		                              {    "sTitle":'Product Status',
	      		                                  "aTargets": [ 11], "mDataProp": function ( source, type, val ) {
	      		                                	  return source[11];
	      		                              }}

	      		                         
	      		                        ];
	     		                       
	var assignedTask_aoColumn = [ { "bSortable": false},
	                                     null,
							               null,
							               null,
							               null,
							               null,
							               null,
							               null,
							               null,
							               null,
							               null
			             ];
	var assignedTask_ColManager = function(){
		//$("#pendingDonorsColumn").html($('.ColVis:eq(1)'));
		 $("#assignedTaskDataTableColumn").html($("#assignedTaskDataTable_wrapper .ColVis"));
	 };	
	 
	 var assignedTask_MergeCols = [1];
var assigned_default_Sort = [[1,"asc"]];
/*	var oTable1=$("#assignedTaskDataTable").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null
											              
											             ],
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#assignedTaskDataTableColumn").html($('.ColVis:eq(0)'));
												}
		});

	*/	
		
		//alert("ready");
		try{
		//constructTable(false,'assignedTaskDataTable',assignedTask_ColManager,assignedTask_serverParam,assignedTask_aoColumnDef,assignedTask_aoColumn,assigned_default_Sort);
		constructMergeWithoutColManager(false,'assignedTaskDataTable',assignedTask_serverParam,assignedTask_aoColumnDef,assignedTask_aoColumn,assigned_default_Sort,assignedTask_MergeCols);
								//		(false,"dailyAssignmentTable",dailyAssignmentTable_serverParam,dailyAssignmentTable_aoColumnDef,dailyAssignmentTable_aoColumn,dailyAssignmentTable_sort,dailyAssignmentTable_MergeCols);
	var oTable1=$("#pendingTaskDataTable").dataTable({
		"sPaginationType": "full_numbers",
		"sDom": 'C<"clear">Rlfrtip',
		"aoColumns": [ { "bSortable":false},
		               null,
		               null,
		               null,
		               null,
		               null,
		               null,
		               null,
		               null,
		               null,
		               null,
		               null
		              
		             ],
			"oColVis": {
				"aiExclude": [ 0 ],
				"buttonText": "&nbsp;",
				"bRestore": true,
				"sAlign": "left"
			},
			"fnInitComplete": function () {
		        $("#pendingTaskDataTableColumn").html($('.ColVis:eq(1)'));
			}
});
	var oTable1=$("#recurringTaskDataTable").dataTable({
		"sPaginationType": "full_numbers",
		"sDom": 'C<"clear">Rlfrtip',
		"aoColumns": [ { "bSortable":false},
		               null,
		               null,
		               null,
		               null,
		               null,
		               null,
		               null
		         
		              
		             ],
			"oColVis": {
				"aiExclude": [ 0 ],
				"buttonText": "&nbsp;",
				"bRestore": true,
				"sAlign": "left"
			},
			"fnInitComplete": function () {
		        $("#recurringTaskDataTableColumn").html($('.ColVis:eq(2)'));
			}
			
	
});
		}catch(err){
			alert("error " + err);
		}

		
$("select").uniform(); 

});




</script>
 <s:hidden id="user" name="user" value="%{user}" /> 
<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Assigned Tasks</div>
		<div class="portlet-content">
		
			<div id="assignedTaskDataTableDiv"
				style="overflow-x: auto; overflow-y: hidden;">
				<table cellpadding="0" cellspacing="0" border="1" id="assignedTaskDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='assignedTaskDataTableColumn'></th>
							<th>Task</th>
							<th>Task Actions</th>
							<th>Product ID</th>
							<th>Recipient MRN</th>
							<th>Recipient Name</th>
							<th>Recipient ABO/Rh</th>
							<th>Donor ABO/Rh</th>
							<th>Product Type</th>
							<th>Donor Type</th>
							<th>Station</th>
							<th>Product Status</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="column">
	<div class="portlet">
		<div class="portlet-header ">Pending Tasks</div>
		<div class="portlet-content">
		
			<div id="pendingTaskDataTableDiv"
				style="overflow-x: auto; overflow-y: hidden;">
				<table cellpadding="0" cellspacing="0" border="1" id="pendingTaskDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='pendingTaskDataTableColumn'></th>
							<th>Task</th>
							<th>Task Actions</th>
							<th>Product ID</th>
							<th>Recipient MRN</th>
							<th>Recipient Name</th>
							<th>Recipient ABO/Rh</th>
							<th>Donor ABO/Rh</th>
							<th>Product Type</th>
							<th>Donor Type</th>
							<th>Station</th>
							<th>Product Status</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="column">
	<div class="portlet">
		<div class="portlet-header addnew">Recuring Task</div>
		<div class="portlet-content">
			<div id="recurringTaskDataTableDiv"
				style="overflow-x: auto; overflow-y: hidden;">
				<table cellpadding="0" cellspacing="0" border="1" id="recurringTaskDataTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='recurringTaskDataTableColumn'></th>
							<th>Task</th>
							<th>Description</th>
							<th>Start Date</th>
							<th>Due Date</th>
							<th>Frequency</th>
							<th>Owner</th>
							<th>Status</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>


<script>

$(function() {
  
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
   
    /**For add new popup**/
   
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
    .append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

  
   
    /**For Refresh**/
	
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

    
});

</script>