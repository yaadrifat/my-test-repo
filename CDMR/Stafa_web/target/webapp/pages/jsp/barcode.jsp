<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/dataSearch.js"></script>
<script type="text/javascript" src="js/jquery/jquery.loadjson.js"></script>
<script type="text/javascript" src="js/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="js/barcode.js"></script>
<style type="text/css">

select {
width:10px;
}


.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
.ui-timepicker-div dl { text-align: left; }
.ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
.ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }

.ui-timepicker-rtl{ direction: rtl; }
.ui-timepicker-rtl dl { text-align: right; }
.ui-timepicker-rtl dl dd { margin: 0 65px 10px 10px; }

</style>
<script type="text/javascript">

function displayAnticoagulantVolume(anticougulantObj){
	var anticougulantVal = $(anticougulantObj).val()
	var anticougulantList = anticougulantVal.split("+");
	var deLim = "";
	var volumeRow = "";

	//If the user change the option, then we have reomve the existing option
	var anticoagulantVolumeNo = $("#anticoagulantVolumeNo").val();
	if(anticoagulantVolumeNo!=null && anticoagulantVolumeNo>=0){
		//alert($("#productFieldtable tr[id='anticoagulantRow']").index());
		for(i=0;i<anticoagulantVolumeNo;i++){
			var row = $("#productFieldtable tr[id='anticougulantNewRow"+i+"']").remove();
			}
	}
	
	for(i=0;i<anticougulantList.length;i++){
		volumeRow+='<tr id="anticougulantNewRow'+i+'" class="anticoagulant" ><td id="label_padding"><label>'+anticougulantList[i]+' volume</label></td>'+
		'<td id="field_padding"><input name="anticougulantVol'+i+'" id="anticougulantVol'+i+'" type="text"></td></tr>';
		}
	//If the user agin change the option, then we have to this appended row, that why we have keep this value  
	$("#anticoagulantVolumeNo").val(anticougulantList.length)
	$("#productFieldtable tr[id='anticoagulantRow']").after(volumeRow);
}

$(".numbersOnly").keydown(function(event) {
    // Allow: backspace, delete, tab, escape, and enter
    if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
         // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) || 
         // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault(); 
        }   
    }
});


function autoCompleteDynamic(inputBoxId,resultBoxId){
	//alert("autoCompleteCombo");
	try{
	
	var searchString = $("#"+inputBoxId).val();
	searchString = encodeURI(searchString);	
	$( "#"+inputBoxId ).autocomplete({
		matchContains: true,
		minChars: 0,
		source:function(request, response) {
		       $.ajax({
		           //url: "fetchAutoCompleteData.action?searchStr="+encodeURI($("#"+inputBoxId).val())+"&domainName=CollectionFacilities&columnName=firmName",
		           url: "fetchAutoCompleteData.action?searchStr="+encodeURI($("#"+inputBoxId).val())+"&domainName=CollectionFacilities&columnName=firmName",
		           dataType: "json",
		           type: "post",
		           data: {
		               maxRows: 20,
		               term: request.term
		           },
		           success: function(data) {
			           
		               response($.map(data.returnList, function(item) {
			               //alert(item.fin+"~-~"+item.city+"~-~"+item.stateProvince+"~-~"+item.country+"~-~"+item.postalCode);
		                   return {

		                	   		//listKey="fin+'~-~'+city+'~-~'+stateProvince+'~-~'+country+'~-~'+postalCode" listValue="firmName"
		                	   		
		                           label: item.firmName,
		                           value: item.firmName,
		                           code: item.fin+"~-~"+item.city+"~-~"+item.stateProvince+"~-~"+item.country+"~-~"+item.postalCode
		                   }
		               }));
		               
		           }
		       })
		   },
		
 		select: function( event, ui ) {
 	 		//alert("s");
 			//alert("val : ["+ui.item.label+"]");
 			//alert("value : ["+ui.item.code+"]");
 			$("#"+resultBoxId).val(ui.item.code);
 		},
		change: function( event, ui ) {
			//alert("s12");
			//alert("change:");
			$("#"+resultBoxId).val(ui.item.code);
		}
	});
	//alert("s1");
	}catch(e){
		alert(e);
	}
}


function autoCompleteCall(inputBoxId){
	//alert("autoCompleteCombo(");
	try{
	
	var searchString = $("#"+inputBoxId).val()+"";
	searchString = encodeURI(searchString);	
	$( "#"+inputBoxId ).autocomplete({
		matchContains: true,
		minChars: 0,
		//source: lstVal,
		source:function(request, response) {
		       $.ajax({
		           url: "autoCompleteSource.action?searchStr="+encodeURI($("#"+inputBoxId).val()),
		           dataType: "json",
		           type: "post",
		           data: {
		               maxRows: 20,
		               term: request.term
		           },
		           success: function(data) {
		               response($.map(data.productDescriptionCodeList, function(item) {
		                   return {
		                           label: item.productDescription,
		                           value: item.productDescription,
		                           code: item.productDescripCode
		                   }
		               }));
		           }
		       })
		   },
		
 		select: function( event, ui ) {
 			//alert("val : ["+ui.item.label+"]");
 			//alert("value : ["+ui.item.code+"]");
 			$("#productDescriptionSelect").val(ui.item.code);
 		},
		change: function( event, ui ) {
			//alert("change:");
			$("#productDescriptionSelect").val(ui.item.code);
		}
	});
	//alert("s1");
	}catch(e){
		alert(e);
	}
}
</script>

<!-- <div class="column">
<div id="archiveProductLabel" class="hidden">
	<div class="portlet">
	<div class="portlet-header notes">Archived Product Labels</div>
		<form>
		    <div class="portlet-content">
		        <table cellpadding="0" cellspacing="0" border="0" id="archiveLabels" class="display" >
		           <thead>
		              <tr>
		                   <th width="4%" id="archiveLabelsColumn"></th>
		                   <th>Product DIN</th>
		                   <th>Type of Label</th>
		                   <th>Product Status at Printing</th>
		                   <th>Product Description</th>
		                   <th>Original Print Date</th>
		                   <th>Number Printed</th>
		               </tr>
		           </thead>
		   	</table>
		   	<div align="right" style="float:right;"><input type="button" value="Print Selected"  onclick="addTab();"/></div>
		   	</div>
		</form>
	</div>
</div>
</div> 

<div id="tempDivforIE" class="">-dddddddd-</div>
<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"><span id="ui-dialog-title-modelPopup" class="ui-dialog-title">&nbsp;</span><a role="button" class="ui-dialog-titlebar-close ui-corner-all" href="#"><span class="ui-icon ui-icon-closethick">close</span></a></div><div style="width: auto; min-height: 0px; height: 439.133px;" class="ui-dialog-content ui-widget-content" id="modelPopup"><div><input style="width:80px;" size="5" value="" id="barcodeeSign" name="barcodeeSign" placeholder="e-Sign" type="password"><input class="cursor" id="printBut" value="Print" onclick="printBarcode();" type="button"></div>
<div class="barcode_wrapper_128VerticalPartial" id="barcodeTem">
<table align="center" border="1" cellpadding="0" cellspacing="0" width="98%">
<tbody><tr><td>

<div class="barcode_inner_box_left">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="98%">
<tbody><tr>
<td colspan="5" align="right"><img class="ref" id="dinBarcodeImg" src="/Stafa_web/fetchBarcode.action?barcodeString==Z5321131273500&amp;barcodeType=code128&amp;barcodeName=barcode1&amp;date=1381989454435"></td>
</tr>
<tr>
<td colspan="5" style="font-size:10px;padding:0%;"><div style="float:left;" class="datebox"><span class="day">  Z5321 13 12735 </span><span class="rotate">00</span></div> <div class="checkChar">L</div></td>
</tr>
<tr>
<td colspan="5" align="right"></td>
</tr>
<tr>
<td colspan="5" align="center"><div class="txt2">Oddzial Terenowy RCKiK Nv 56<br>
Ciechanow,
Poland,06-400.<br>
FDA Reg. NO:555555</div></td>
</tr>
<tr>
<td colspan="5">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr>
<td class="txt2" width="22%">Production Date/Time</td>
<td align="right" width="78%"><img class="ref" id="collecionDateImg" src="/Stafa_web/fetchBarcode.action?barcodeString=%26}1121&amp;barcodeType=code128&amp;barcodeName=barcode2&amp;date=1381989454436"></td>
</tr>
<tr>
<td colspan="5"><div style="font-size:10px;text-align:right;padding:0px 54px" id="collectionDateText">1121</div></td>
</tr>
</tbody></table>
</td>
</tr>
<tr>
<td colspan="5" class="txt2">Oct 14, 2013 11:21</td>
</tr>
<tr>
<td colspan="5" class="txt3">(Oct 14, 2013 11:21 UTC)</td>
</tr>
<tr>
<td colspan="5" class="txt2" align="center">&nbsp;<br>
&nbsp;</td>
</tr>
</tbody></table>
</div>
</td><td>
<div></div>
</td></tr>
<tr><td>
<div class="barcode_inner_box_left">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr>
<td><table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr>
<td width="28%">&nbsp;</td>
<td width="72%"><img class="ref" id="productDescImg" src="/Stafa_web/fetchBarcode.action?barcodeString==<S1583400&amp;barcodeType=code128&amp;barcodeName=barcode5&amp;date=1381989454436"></td>
</tr>
<tr>
<td colspan="5" style="text-align:right"><span style="font-size:10px;text-align:left;height:10px" id="collectionDateText">S1583400</span>&nbsp;&nbsp;<span style="font-size:10px;text-align:right;height:10px" id="collectionDateText">DESIGNATED</span></td>
</tr>
</tbody></table></td>
</tr>
<tr>
<td class="txt10">CRYOPRESERVED</td>
</tr>
<tr>
<td class="txt1">HPC, APHERESIS</td>
</tr>
<tr>
<td class="txt2normal"></td>
</tr>
<tr>
<td class="txt2normal">10% DMSO</td>
</tr>
<tr>
<td class="txt2normal">B-cell reduced</td>
</tr>
<tr>
<td class="txt2normal">&nbsp;</td>
</tr>
<tr>
<td class="txt2normal">Other Additives Present</td>
</tr>
<tr>
<td class="txt2normal">&nbsp;</td>
</tr>
<tr>
<td class="txt2normal">&nbsp;</td>
</tr>
<tr>
<td class="txt6">Aprox <u>777</u> mL in approx <u>6</u> mL Citrate+Heparin</td>
</tr>
<tr>
<td class="txt2">Store at -120 C or colder</td>
</tr>
<tr>
<td class="txt1" align="center">&nbsp;</td>
</tr>
</tbody></table>
</div>
</td><td>
</td></tr>
</tbody></table>
</div>
</div><div class="ui-resizable-handle ui-resizable-n"></div><div class="ui-resizable-handle ui-resizable-e"></div><div class="ui-resizable-handle ui-resizable-s"></div><div class="ui-resizable-handle ui-resizable-w"></div><div style="z-index: 1001;" class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se ui-icon-grip-diagonal-se"></div><div style="z-index: 1002;" class="ui-resizable-handle ui-resizable-sw"></div><div style="z-index: 1003;" class="ui-resizable-handle ui-resizable-ne"></div><div style="z-index: 1004;" class="ui-resizable-handle ui-resizable-nw"></div>
-->

<!-- Start Test 

<DIV id=barcodeTem2 class=barcode_wrapper>
<TABLE border=0 cellSpacing=0 cellPadding=0 width="98%" align=center>
<TBODY>
<TR>
<TD>
<DIV class=barcode_inner_box_left>
<TABLE border=0 cellSpacing=0 cellPadding=0 width="98%" align=center>
<TBODY>
<TR>
<TD colSpan=5 align=right><IMG id=dinBarcodeImg class=ref src="/Stafa_web/fetchBarcode.action?barcodeString==Z5321131277600&amp;barcodeType=code128&amp;barcodeName=barcode1&amp;date=1382582600251"></TD></TR>
<TR>
<TD style="PADDING-BOTTOM: 0%; PADDING-LEFT: 0%; PADDING-RIGHT: 0%; FONT-SIZE: 10px; PADDING-TOP: 0%" colSpan=5>
<DIV style="FLOAT: left" class=datebox><SPAN class=day>Z5321 13 12776 </SPAN><SPAN class=rotate>00</SPAN></DIV>
<DIV class=checkChar>5</DIV></TD></TR>
<TR>
<TD colSpan=5 align=right></TD></TR>
<TR>
<TD colSpan=5 align=middle>
<DIV class=txt2>Oddzial Terenowy RCKiK Nv 56<BR>Ciechanow, Poland,06-400.<BR>FDA Reg. NO:555555</DIV></TD></TR>
<TR>
<TD colSpan=5>
<TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
<TBODY>
<TR>
<TD class=txt2 width="22%">Production Date/Time</TD>
<TD width="78%" align=right><IMG id=collecionDateImg class=ref src="/Stafa_web/fetchBarcode.action?barcodeString=%26}1121&amp;barcodeType=code128&amp;barcodeName=barcode2&amp;date=1382582600251"></TD></TR>
<TR>
<TD colSpan=5>
<DIV style="TEXT-ALIGN: right; PADDING-BOTTOM: 0px; PADDING-LEFT: 54px; PADDING-RIGHT: 54px; FONT-SIZE: 10px; PADDING-TOP: 0px" id=collectionDateText>1121</DIV></TD></TR></TBODY></TABLE></TD></TR>
<TR>
<TD class=txt2 colSpan=5>Oct 14, 2013 11:21</TD></TR>
<TR>
<TD class=txt3 colSpan=5>(Oct 14, 2013 11:21 UTC)</TD></TR>
<TR>
<TD class=txt2 colSpan=5 align=middle>&nbsp;<BR>&nbsp;</TD></TR></TBODY></TABLE></DIV></TD>
<TD>
<DIV class=barcode_inner_box_right>
<TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
<TBODY>
<TR>
<TD>
<TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
<TBODY>
<TR>
<TD style="TEXT-ALIGN: left"><IMG id=aborhImg class=ref src="/Stafa_web/fetchBarcode.action?barcodeString==%H200&amp;barcodeType=code128&amp;barcodeName=barcode4&amp;date=1382582600251"></TD>
<TD>
<TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
<TBODY>
<TR>
<TD style="FONT-SIZE: 14px" align=middle>Bombay</TD></TR>
<TR>
<TD style="FONT-SIZE: 12px" align=middle>RhD positive</TD></TR></TBODY></TABLE></TD></TR>
<TR>
<TD colSpan=5>
<DIV style="TEXT-ALIGN: left; FONT-SIZE: 10px" id=aboBarcodeText>H200</DIV></TD></TR></TBODY></TABLE></TD></TR>
<TR>
<TD align=middle>&nbsp;</TD></TR>
<TR>
<TD class=txt2>&nbsp;</TD></TR>
<TR>
<TD class=txt2>&nbsp;</TD></TR>
<TR>
<TD class=txt2>Donor/Recipient<BR>Pradeep, nair<BR>Donor# working123<BR>Date of Birth:Jun 14, 2013</TD></TR></TBODY></TABLE></DIV>
<DIV></DIV></TD></TR>
<TR>
<TD>
<DIV class=barcode_inner_box_left>
<TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
<TBODY>
<TR>
<TD>
<TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
<TBODY>
<TR>
<TD width="28%">&nbsp;</TD>
<TD width="72%"><IMG id=productDescImg class=ref src="/Stafa_web/fetchBarcode.action?barcodeString==<S1583400&amp;barcodeType=code128&amp;barcodeName=barcode5&amp;date=1382582600251"></S1583400></TD></TR>
<TR>
<TD style="TEXT-ALIGN: right" colSpan=5><SPAN style="TEXT-ALIGN: left; HEIGHT: 10px; FONT-SIZE: 10px" id=collectionDateText>S1583400</SPAN>&nbsp;&nbsp;<SPAN style="TEXT-ALIGN: right; HEIGHT: 10px; FONT-SIZE: 10px" id=collectionDateText>DESIGNATED</SPAN></TD></TR></TBODY></TABLE></TD></TR>
<TR>
<TD class=txt10>CRYOPRESERVED</TD></TR>
<TR>
<TD class=txt1>HPC, APHERESIS</TD></TR>
<TR>
<TD class=txt2normal></TD></TR>
<TR>
<TD class=txt2normal>10% DMSO</TD></TR>
<TR>
<TD class=txt2normal>B-cell reduced</TD></TR>
<TR>
<TD class=txt2normal>&nbsp;</TD></TR>
<TR>
<TD class=txt2normal>Other Additives Present</TD></TR>
<TR>
<TD class=txt2normal>&nbsp;</TD></TR>
<TR>
<TD class=txt2normal>&nbsp;</TD></TR>
<TR>
<TD class=txt6>Aprox <U>777</U> mL in approx <U>6</U> mL Citrate+Heparin</TD></TR>
<TR>
<TD class=txt2>Store at -120 C or colder</TD></TR>
<TR>
<TD class=txt1 align=middle>&nbsp;</TD></TR></TBODY></TABLE></DIV></TD>
<TD>
<DIV class=barcode_inner_box_right>
<TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
<TBODY>
<TR>
<TD>
<TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
<TBODY>
<TR>
<TD height=37 width="73%"><IMG id=expireDateImg class=ref src="/Stafa_web/fetchBarcode.action?barcodeString=%26>0132801120&amp;barcodeType=code128&amp;barcodeName=barcode3&amp;date=1382582600251"></TD>
<TD class=txt2 width="27%">Expiration Date and Time</TD></TR>
<TR>
<TD height=13 colSpan=5>
<DIV style="TEXT-ALIGN: left; FONT-SIZE: 10px">0132801120</DIV></TD></TR></TBODY></TABLE></TD></TR>
<TR>
<TD class=txt2 height=11>Oct 08, 2013 11:20</TD></TR>
<TR>
<TD class=txt3 height=11>(Oct 08, 2013 11:20 UTC)</TD></TR>
<TR>
<TD class=txt5>Donor/Recipient:<BR>Pradeep, nair<BR>MRN:working123<BR>Date of Birth:Jun 14, 2013</TD></TR>
<TR>
<TD class=txt2>
<TABLE border=0 cellSpacing=0 cellPadding=0 width="100%">
<TBODY>
<TR>
<TD width="19%"><IMG id=marrixImg class=ref src="/Stafa_web/fetchBarcode.action?barcodeString==+04003=Z5321131277600=%H200=<S1583400&amp;>0132801120&amp;barcodeType=DataMatrix&amp;barcodeName=2DMatrix&amp;date=1382582600251"></TD>
<TD class=txt2 width="81%">Oddzial Terenowy RCKiK Nv 56<BR>Ciechanow,<BR>Poland,06-400.<BR>FDA Reg. NO:555555</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></DIV></TD></TR></TBODY></TABLE></DIV>



<!-- End Test -->

<div class="cleaner"></div>

<div  id="barcodeDiv">
    <div class="column">
    <div id="productFind">   
        <div  class="portlet">
            <div class="portlet-header notes" id="protoggle"><a href="#" id = "proSearch"><input type="radio" id="prosearchid" name="proSearch"  Class="selectDeselect"/></a>Product Search &nbsp;<a href="#" id = "archiveSearch"><input type="radio" id="archiveSearchId" name="proSearch" Class="selectDeselect"/></a>Archived Product Search</div>
            <form>
			     <div class="portlet-content hidden" id="archiveProductLabel">
			      <div><b>Archived Product Labels</b></div>
			     	<table cellpadding="0" cellspacing="0" border="0" id="archiveLabels" class="display" >
			           <thead>
			              <tr>
			                   <th width="4%" id="archiveLabelsColumn"></th>
			                   <th>Product DIN</th>
			                   <th>Type of Label</th>
			                   <th>Product Status at Printing</th>
			                   <th>Product Description</th>
			                   <th>Original Print Date</th>
			                   <th>Number Printed</th>
			               </tr>
			           </thead>
			   		</table>
			   		<div align="right" style="float:right;"><input class='hidden' type="button" value="Print Selected"  onclick="addTab();"/></div>
			   	</div>
                <div class="portlet-content" id="proFind">
                	<table cellpadding="0" cellspacing="0" border="0" id="productSearchTable" class="display" >
	                    <thead>
	                       <tr>
	                            <th width="4%" id="productSearchColumn"></th>
	                            <th colspan="1"><s:text name="stafa.label.recipientmrn"/></th>
	                            <th colspan="1"><s:text name="stafa.label.recipientname"/></th>
	                            <th colspan="1" ><s:text name="stafa.label.donorname"/></th>
	                            <th colspan="1"><s:text name="stafa.label.productid"/></th>
	                            <th colspan="1"><s:text name="stafa.label.plasmaid"/></th>
	                            <th colspan="1"><s:text name="stafa.label.producttype"/></th>
	                            <th colspan="1"><s:text name="stafa.label.productsource"/></th>
	                            <th colspan="1"><s:text name="stafa.label.status"/></th>
	                            <th colspan="1"><s:text name="stafa.label.abo"/></th>
	                        </tr>
	                    </thead>
            		</table>
                    <div align="right" style="float:right;"><input type="button" id="printSelected" value="Print Selected"  onclick="addTab();"/></div>
                </div>
            </form>
        </div>
    </div>
    </div>

<div class="cleaner"></div>

 
    <div id="barcodetabs"  style="height:45px">
        <ul id="bartab">
           
        </ul>
	
		
  <div  id ="secondarydiv" class="secondarydivclass">  <!-- added by mohan j -->
         <br><br>
         <form id='tempBarcodeForm' name='tempBarcodeForm'>
	         <div style='display:none' id='tempBarcodeDiv' name='tempBarcodeDiv'>
	         
	         <s:hidden id='productLabel' name='barcode_label'/>
	         <s:hidden id='reasonReprint' name='reasonReprint'/>
	         <s:hidden id='productDin' name='barcodeText'/>
	         <s:hidden id='productDes' name='productDescription'/>
	         <s:hidden id='productStatus' name='productStatus'/>
	         <s:hidden id='typeOfLabel' name='typeOfLabel'/>
	         
	         <s:hidden id='printDate' name='originalPrintDate'/>
	         <s:hidden id='barcodeRecipient' name='recipient'/>
	         <s:hidden id='barcodeDonor' name='donor'/>
	         <s:hidden id='barcodeEntity' name='barcodeEntity'/>
	         <s:hidden id='intendedUseSelectedText' name='intendedUseSelectedText'/>
	         <s:hidden id='sequenceNumber' name='sequenceNumber'/>
	         <s:hidden id='anticoagulantVolumeNo' name='anticoagulantVolumeNo'/>
	         </div>
         </form>
         
          <form name="barcodeForm" id="barcodeForm" >
          <s:hidden name="pkSpecimen" name="pkSpecimen"/>
          <s:hidden name="productStatus" name="productStatus" value="Accession"/>
          
          <s:hidden name="aboSelectedText" name="aboSelectedText"/>
          <s:hidden name="donorRelationDisplayText" name="donorRelationDisplayText"/>
            <!-- <ul class="men">
            <li> <a href="#" id="barcodeMenu" style=text-decoration:none;" class="cursor" onclick="barPrinting(this)" ><b>BarCode Printing</b></a></li>
            </ul>  -->
    <div id="barcodePrinting" style="background-color:white;">
        <table  border="0" cellpadding="3" cellspacing="0" >
            <tr>
                <td id="label_padding" width="5%"><label>Label Template</label></td>
                <td id="field_padding" width="5%">
                <!-- <div><s:select headerKey="" headerValue="Select" list="barcodemodelList" listKey="barcodeMaintenance" listValue="maintenanceName" name="barcodemodel" id="barcodemodel" onChange="readRequiredFields(this);"/></div> -->
                <div><s:select headerKey="" headerValue="Select" list="barcodemodelList" listKey="barcodeMaintenance" listValue="maintenanceName" name="barcodemodel" id="barcodemodel" onChange="requireFields(this);"/></div>
                </td>
                <td style="padding:1% 0% 0% 0%" width="8%">&nbsp;&nbsp;</td>
              	<td style="padding:0% 0% 0% 0%" width="1%">&nbsp;&nbsp;</td>
                <td style="padding:1% 0% 0% 0%" width="6%">
                <input type="button" value="Generate Barcode" id="activeProduct" onclick="fetchBarcodeTemplate('active');" style="width:75%;"/>
                <input type="button" value="Generate Barcode" id="archieved" onclick="fetchBarcodeTemplate('RePrint');" style="width:75%;"/>
                </td>
                <td style="padding:1% 0% 0% 0%" width="1%"><div><input type="button" Value="Apply To All" onclick="applytoall();"></div></td>
            </tr>
        </table>

    <br>
    <div id="mainfielddiv" class="mainfielddivs">
    <s:hidden id="specimenId" name="specimenId"/>
   	<div  id ="leftdiv"style="width:48%;float:left; background-color:white;"> 
	<div id="bardiv2" style="border-radius:10px;border:1px solid black;width:100%;margin-left:0%">
		<table  border="0" cellpadding="0" cellspacing="0" id="productFieldtable">
			<tr>
				<td colspan="2" id="label_padding" width="60%"><b>Product Field Selections</b></td>
			</tr>
			<tr>
				<td style="padding:2% 0% 0% 2%"><label>Class</label></td>
				<td style="padding:2% 0% 0% 0%">
				<s:select headerKey="" headerValue="Select" list="classList" listKey="subType" listValue="description" name="barcodeClass" onchange="searchProductCodeDescription(this);autoCompleteCall('productDescriptionInput')" id="barcodeClass"/></td>
			</tr>
			<tr>
				<td id="label_padding" ><label>Modifier</label></td>
				<td id="field_padding" ><s:select headerKey="" headerValue="Select" list="modifierList" listKey="subType" listValue="description" onchange="searchProductCodeDescription(this);autoCompleteCall('productDescriptionInput')" name="modifier" id="modifier"/></td>
			</tr>
			<tr>
				<td id="label_padding"><label>Manipulation</label></td>
				<td id="field_padding" >
				<s:select headerKey="" headerValue="Select" list="manipulationList" listKey="subType" listValue="description" name="manipulation" id="manipulation"  onchange="searchProductCodeDescription(this);autoCompleteCall('productDescriptionInput')"/>
			   	</td>
			</tr>
			<tr>
				<td id="label_padding"><label>Cryoprotectant</label></td>
				<td id="field_padding">
				<s:select headerKey="" headerValue="Select" list="cryoprotectantList" listKey="subType" listValue="description" name="cryoprotectant" id="cryoprotectant" onchange="searchProductCodeDescription(this);autoCompleteCall('productDescriptionInput')"/>
			       	</td>
			</tr>
			<tr>
				<td id="label_padding"><label>Collection Volume (mL)</label></td>
				<td id="field_padding"><input name="collectionVolume" id="collectionVolume" class="numbersOnly" type="text" ></td>
			</tr>
			<tr>
				<td id="label_padding"><label>Product Description Code</label></td>
				<td id="field_padding"><input name="productDescriptionInput" id="productDescriptionInput" type="text" value="" onblur="getProductValue(this);">
				<s:hidden name="productDescriptionSelect" id="productDescriptionSelect"/>
				</td>
			</tr>
			<tr>
				<td id="label_padding"><label>Division code</label></td>
				<td id="field_padding"><input name="divisionCode" id="divisionCode" type="text" value="" maxLength="2">
				</td>
			</tr>
			<tr id="anticoagulantRow">
				<td id="label_padding"><label>Anticoagulant Type</label></td>
				<td id="field_padding">
				<!--<s:select headerKey="" name="anticoagulantType" headerValue="Select" list="antiCoagulantTypeList" listKey="subType" listValue="description" id="anticoagulantType"  onchange="searchProductCodeDescription(this);autoCompleteCall('productDescriptionInput')"/>-->
				<s:select headerKey="" name="anticoagulantType" headerValue="Select" list="antiCoagulantTypeList" listKey="subType" listValue="description" id="anticoagulantType"  onchange="displayAnticoagulantVolume(this);searchProductCodeDescription(this);autoCompleteCall('productDescriptionInput');"/>
			   	</td>
			</tr>			
			<tr>
				<td id="label_padding"><label>Storage Temp</label></td>
				<td id="field_padding" >
				<s:select headerKey="" headerValue="Select" name="storageTemprature" list="storageTempList" listKey="subType" listValue="description" id="storageTemprature" onchange="searchProductCodeDescription(this);autoCompleteCall('productDescriptionInput')" />
				<s:select headerKey="" headerValue="Select" name="storageTempratureText" list="storageTempTextList" listKey="description" listValue="subType" id="storageTempratureText"/>
			   	</td>
			</tr>
			<tr>
				<td id="label_padding"><label>Intended Use</label></td>
				<td id="field_padding" >
				<s:select headerKey="" name="intendedUse" headerValue="Select" list="intendedUseList" listKey="pkCodelst" listValue="description" id="intendedUse"/>
			   	</td>
			</tr>
			<tr>
				<td id="label_padding"><label>Donation Type</label></td>
				<td id="field_padding" >
				<s:select headerKey="" headerValue="Select" name="donationType" list="donationTypeList" listKey="subType" listValue="description" id="donationType"/>
			   	</td>
			</tr>
			<tr>
				<td style="padding:1% 0% 1% 2%"><label>ABO/Rh</label></td>
				<td style="padding:1% 0% 1% 0%" >
				<s:select headerKey="" name="aborh" headerValue="Select"  list="aboRhList" listKey="pkCodelst" listValue="description" id="aboRh"/></td>
				<!--<select name="Class"><option value="Full Label">O Positive</option></select>-->
			</tr>
		</table>
	</div>
	<br>
	<div id="bardiv3" style="border-radius:10px;border:1px solid black;width:100%;margin-left:0%">
		<table  border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td id="label_padding" width="60%" colspan="2"><b>Recipient and Donor Information</b></td>
			</tr>
			<tr>
				<td style="padding:2% 0% 0% 2%"><label>Recipient Name</label></td>
				<td id="field_padding"><input type="text" value="" name="barcodeRecipientName" id="barcodeRecipientName">
				<input type="hidden" id="recipient" name="recipient"/></td>
			</tr>
			<tr>
				<td id="label_padding" ><label>Recipient DOB</label></td>
				<td id="field_padding"><input type="text" value="" id="barcodeRecipientDOB" name="barcodeRecipientDOB" readonly="readonly" class="dateEntry"></td>
			</tr>
			<tr>
				<td id="label_padding"><label>Recipient MRN</label></td>
				<td id="field_padding"><input type="text" value="" id="barcodeRecipientMRN" name="barcodeRecipientMRN"></td>
			</tr>
			<tr>
				<td style="padding:5% 0% 0% 2%"><label>Donor Name</label></td>
				<td style="padding:5% 0% 0% 0%"><input type="text" value="" id="barcodeDonorName" name="barcodeDonorName">
				<input type="hidden" id="donor" name="donor"/></td>
			</tr>
			<tr>
				<td id="label_padding"><label>Donor DOB</label></td>
				<td id="field_padding"><input type="text" value="" id="barcodeDonorDOB" name="barcodeDonorDOB" readonly="readonly" class="dateEntry"></td>
			</tr>
			<tr>
				<td id="label_padding"><label>Donor MRN</label></td>
				<td id="field_padding"><input type="text" value="" id="barcodeDonorNo" name="barcodeDonorNo"></td>
			</tr>
			<tr>
				<td id="label_padding"><label>Donor Relation</label></td>
				<td id="field_padding" ><s:select headerKey="" headerValue="Select" list="donorRelationList" listKey="subType" listValue="description" id="donorRelation" name="donorRelation"/>
				<s:select headerKey="" headerValue="Select" list="dnrRelationDisplayTextList" listKey="description" listValue="subType" id="donorRelationText" name="donorRelationText"/></td>
			</tr>
		</table>
	</div>
	<br>
<div id="bardiv4" style="border-radius:10px;border:1px solid black;width:100%;margin-left:0%">
		<table  border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td  colspan="2" id="label_padding" width="60%"><b>Product Information and Warning Selections</b></td>
			</tr>
			<tr>
				<td style="padding:2% 0% 0% 2%"><label>Component from 3rd Party</label></td>
				<td id="field_padding"><s:checkbox name="thirdPartyComponent" id="thirdPartyComponent"/></td>
			</tr>
			<tr>
				<td id="label_padding" ><label>Other Additives</label></td>
				<td id="field_padding"><s:checkbox name="otherAdditives" id="otherAdditives"/></td>
			</tr>
			<tr>
				<td id="label_padding"><label>Genetically Modified</label></td>
				<td id="field_padding"><s:checkbox name="geneticallyModified" id="geneticallyModified"/></td>
			</tr>
			<tr>
				<td id="label_padding"><label>Biohazard</label></td>
				<td id="field_padding"><s:checkbox name="biohazard" id="biohazard"/></td>
			</tr>
			<tr>
				<td id="label_padding"><label>Autologous Use Only</label></td>
				<td id="field_padding"><s:checkbox name="autologousUse" id="autologousUse"/></td>
			</tr>
			<tr>
				<td id="label_padding"><label>For Intended Recipient Only</label></td>
				<td id="field_padding"><s:checkbox name="intendedRecipient" id="intendedRecipient"/></td>
			</tr>
			<tr>
				<td id="label_padding"><label>Do Not Irradiate</label></td>
				<td id="field_padding"><s:checkbox name="doNotIrradiate" id="doNotIrradiate"/></td>
			</tr>
			<tr>
				<td style="padding:1% 0% 1% 2%"><label>Do Not Use Leukocyte Reduction Filter</label></td>
				<td style="padding:1% 0% 1% 0%"><s:checkbox name="doNotUseLeukocyteReduction" id="doNotUseLeukocyteReduction"/></td>
			</tr>
			<tr>
				<td style="padding:1% 0% 1% 2%"><label>See Attached Documentation for Details</label></td>
				<td style="padding:1% 0% 1% 0%"><s:checkbox name="attachedDocsDetails" id="attachedDocsDetails"/></td>
			</tr>
			
		</table>
	</div>
	</div>	<!-- leftdiv ends here -->
 <div  id ="rightdiv"style="width:49%;padding-right:1%; float:right;background-color:white;"> 
	<div id="barcode_1" style="border-radius:10px;border:1px solid black;width:100%;margin-left:0%;" >
                    <table cellpadding="0" cellspacing="0">
	                    <tr>
	                        <td  colspan="2" style="padding:0% 0% 0% 1%"><b>Donation ID Number Information</b></td>
	                    </tr>
	                    <tr><td>&nbsp; </td></tr>
	                    <tr>
	                        <td style="padding:0% 0% 1% 2%">Collection Facility</td>
	                        <td style="padding:0% 0% 1% 0%"><input type="Text" id="collectionFacility" name="collectionFacility" value="" size="30" onkeyup="autoCompleteDynamic('collectionFacility','collectionFacilities');" onblur="getAddress('collectionFacilities','facilityAddress');" >
	                        <s:hidden id="collectionFacilities" name="collectionFacilities"/>
	                        </td>
	                        </tr>
	                        <tr>
	                            <td style="padding:0% 0% 1% 2%">Year</td>
	                            <td  style="padding:0% 0% 1% 0%" width="50px"><div><input type="text" id="dinYear" name="dinYear" class="smartspinner" /></div></td>
	                        </tr>
	                        <tr>
	                            <td style="padding:0% 0% 1% 2%">Sequence Number</td>
	                            <td  style="padding:0% 0% 1% 0%" width="20%"><input type="Text" id="sequenceId" name="sequenceId" value=""></td>
	                        </tr>
	                                           
	                        <tr>
	                            <td style="padding:0% 0% 0% 2%"> Flag Characters </td>
	                            <td  style="padding:0% 0% 1% 0%"><select name="flagCharacter" id="flagCharacter" class=""><option value="">Select</option><option value="00">Flag Not Used(00)</option><option value="05">Re-Print(05)</option></select></td>
	                        </tr>
	                                               
	                        <tr>
	                            <td style="padding:0% 0% 0% 2%">Address</td>
	                            <td  style="padding:0% 0% 1% 0%"><textarea rows="2" cols="20" name="facilityAddress" id="facilityAddress"></textarea></td>
	                        </tr>
	                        <tr>
	                            <td style="padding:0% 0% 0% 2%">FDA Reg. NO</td>
	                            <td  style="padding:0% 0% 1% 0%"><input type="text" name="collectionFda" id="collectionFda" onkeyup="getSetCollectionFacility();"/></td>
	                        </tr>
                        </table>
                    </div>
                    <br>
                      <div id="barcode-2" style="border-radius:10px;border:1px solid black;width:100%;margin-left:0%;valign:top">
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td  colspan="2"><input type="checkbox" name="processingFacilityName" id="processingFacilityName" onclick="getSetCollectionFacility();" value="">Processing Facility same as Collection Facility</td>
							</tr>
							<tr>
								<td style="padding:0% 0% 0% 2%">Facility</td>
								<td style="padding:0% 0% 1% 0%"><input type="Text" id="processingFacility" name="processingFacility" value="" size="30" onkeyup="autoCompleteDynamic('processingFacility','processingFacilities');" onblur="getAddress('processingFacilities','processingAddress')" >
								<s:hidden id="processingFacilities" name="processingFacilities"/>
								</td>
							</tr>
							<tr>
								<td style="padding:0% 0% 0% 2%">Address</td>
								<td  style="padding:0% 0% 1% 0%"><textarea rows="2" cols="20" id="processingAddress" name="processingAddress"></textarea></td>
							</tr>
							<tr>
								<td style="padding:0% 0% 0% 2%">FDA Reg. NO</td>
								<td  style="padding:0% 0% 1% 0%"><input type="text" name="processingFda" id="processingFda" /></td>
							</tr>
						</table>
                       
                        </div>
                        <br>
                        <div id="barcode_3" style="border-radius:10px;border:1px solid black;width:100%;margin-left:0%;" >
                    <table cellpadding="0" cellspacing="0">
                   
                    <tr>
                        <td colspan="2" style="padding:0% 0% 0% 1%"><b>Time and Date</b></td>
                    </tr>
                    <tr><td>&nbsp; </td></tr>
                    <tr>	
                        <td style="padding:0% 0% 0% 2%"> Collection Date</td>
                        <td> <input type="Text" id="collectionDateID" name="collectionDate"  size="30" class="dateEntry"></td>
                        </tr>   
                        <tr>
                            <td style="padding:0% 0% 0% 2%">Collection Time</td>
                            <td width="50%" height="40px"><input type="Text" id="collectionTime" name="collectionTime" size="15" ></td>
                        </tr>                                           
                        <tr>
                            <td style="padding:0% 0% 0% 2%"> Production Date </td>
                            <td > <input type="Text" id="productionDateID" size="30" class="dateEntry "   name="productionDate"></td>
                        </tr>
                        <tr>
                            <td style="padding:0% 0% 0% 2%"> Production Time</td>
                            <td width="50%" height="40px"><input type="Text" id="productionTime" name="productionTime" size="15" ></td>
                            </tr>
                            <tr>
                            <td style="padding:0% 0% 0% 2%"> Expiration Date </td>
                            <td> <input type="Text" id="expirationDateID" name="expirationDate" size="30"    class="dateEntry cdates"></td>
                        </tr>               
                        <tr>
                            <td style="padding:0% 0% 0% 2%"> Expiration Time</td>
                            <td width="50%" height="40px"><input type="Text" id="expirationTime"    name="expirationTime" size="15" ></td>
                            </tr>
                        </table>
                    </div> 
                    <br>
                 <!--     comment by mohan -->
                 <!--    <div id="barSave" style=" width: 100%;height:50%;">
                       
		                        
		                 Page:&nbsp;&nbsp;<a href="#" id="pagePrinting" class="cursor" onclick="barPrinting(this)" >1</a>&nbsp;&nbsp;&nbsp;<a href="#" id="pageLibrary"  class="cursor" onclick="barPrinting(this)" >2</a>&nbsp;&nbsp;
		           
		           
		            <input type="button" value="Print Barcode" id="printBarcodeTemplate" onclick="printBarcodeTemplate();" style="width:34%;"/>
		            
            
        
                       
   				 </div> -->
                    </div>    
             </div>          
	</div> 			
	<!-- by mohan j --> 
	</form>
</div>	
</div>       
    </div>
 
<script>
$(function(){

	$("#proSearch").bind("click",function(){
		//$("#archiveLabels").addClass('hidden');
        $("#flagCharacter option[value=05]").hide();
        $("#flagCharacter option[value=00]").show();
        $("#flagCharacter option[value=00]").attr('selected', 'selected');
        $.uniform.restore('.ui-dialog select');
    	$('.ui-dialog select').uniform();
    	HideDropDowns();
    });
    $("#archiveSearch").bind("click",function(){
        $("#flagCharacter option[value=05]").show();
        $("#flagCharacter option[value=05]").attr('selected', 'selected');
        $("#flagCharacter option[value=00]").hide();
        $.uniform.restore('.ui-dialog select');
    	$('.ui-dialog select').uniform();
    	HideDropDowns();
    });

    
    
	    $( ".ui-dialog .portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	    $( ".portlet-header .ui-icon" ).click(function() {
	        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
	        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	    });
	   // $("#proFind").hide();
	    $('#prosearchid').click(function (e) {
	    	/*Product Search Table server Parameters Starts*/
	    	$("#printSelected").show();
	    	
	    	showBar();
	    	//alert("s");
	    	var proSearch_serverParam = function ( aoData ){

	    													aoData.push( { "name": "application", "value": "stafa"});
	    													aoData.push( { "name": "module", "value": "technician_accession_barcode"});	
	    													
	    													aoData.push( {"name": "col_1_name", "value": "lower((SELECT PERSON_MRN FROM person WHERE PK_PERSON=d.FK_RECEIPENT))"} );
	    													aoData.push( { "name": "col_1_column", "value": "lower(RecMRN)"});
	    													
	    													aoData.push( { "name": "col_2_name", "value": " lower((SELECT (PERSON_LNAME || PERSON_FNAME) FROM person WHERE PK_PERSON=d.FK_RECEIPENT))"});
	    													aoData.push( { "name": "col_2_column", "value": "lower(recipientName)"});
	    													
	    													aoData.push( { "name": "col_3_name", "value": " lower((SELECT (PERSON_LNAME || PERSON_FNAME) FROM person WHERE PK_PERSON=d.fk_person))"});
	    													aoData.push( { "name": "col_3_column", "value": "lower(donorName)"});
	    													
	    													aoData.push( {"name": "col_4_name", "value": " lower(sp.spec_id)" } );
	    													aoData.push( { "name": "col_4_column", "value": "lower(sp.spec_id)"});
	    													
	    													aoData.push( {"name": "col_4_name", "value": "" } );
	    													aoData.push( { "name": "col_4_column", "value": ""});
	    													
	    													aoData.push( {"name": "col_6_name", "value": " lower((SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST = d.FK_CODELSTPRODUCTTYPE))" } );
	    													aoData.push( { "name": "col_6_column", "value": "lower(productType)"});
	    													
	    													aoData.push( {"name": "col_7_name", "value": " lower((SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST = d.FK_CODELSTDONORTYPE))" } );
	    													aoData.push( { "name": "col_7_column", "value": "lower(productSource)"});
	    																		
	    													aoData.push( {"name": "col_8_name", "value": " lower((SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=pp.fk_codelstdonorstatus))" } );
	    													aoData.push( { "name": "col_8_column", "value": "lower(status)"});
	    													
	    												  	aoData.push( {"name": "col_9_name", "value": " lower((SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST = (SELECT FK_CODELST_BLOODGRP FROM person WHERE PK_PERSON=d.FK_RECEIPENT)))" } );
	    													aoData.push( { "name": "col_9_column", "value": "lower(BLOODGRP)"});
	    													 
	    													
	    													};

	    var proSearch_aoColumnDef = [
	    	 	                       		{"aTargets": [ 0], "mDataProp": function ( source, type, val ) { 
	    		 	                      	 return "<input type='checkbox' id='pkRecipient' name='pkRecipient' value='"+source[9]+"'/><input type='hidden' id='productDonor' name='productDonor' value='"+source[10]+"'/><input type='hidden' id='hiddenpersonprId' value="+source[2]+"><input type='hidden' id='specimenId' name='specimenId' value='"+source[11]+"'/><input type='hidden' id='plasmaId' name='plasmaId' value='"+source[19]+"'/><input type='hidden' id='plasmaName' name='plasmaName' value='"+source[20]+"'/>";
	    		 	                      	 }},
	    		 	                        {	"sTitle":"Recipient MRN",
	    		 	                      		 "aTargets": [ 1], "mDataProp": function ( source, type, val ) { 
	    		 	                      	 return source[15];
	    		 	                      	 }},
	    		 	                        {	"sTitle":"Recipient Name",
	    		 	                      		 "aTargets": [ 2], "mDataProp": function ( source, type, val ) { 
	    		 	                      		    return source[1]+"<br/>"+source[17];
	    		 	                      		    }},
	    		 	                        {	"sTitle":"Donor Name",
	    		 	                      		 "aTargets": [ 3], "mDataProp": function ( source, type, val ) { 
	    		 	                      			return source[7]+"<br/>"+source[18];
	    		 	                      			  }},
	    		 	                        {	"sTitle":"Product ID",
	    		 	                      		    	"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
	    		 	                      	 	   return source[2];
	    		 	                        		}},
   		 	                        		{	"sTitle":"Plasma ID",
   		 	                      		    	"aTargets": [ 5], "mDataProp": function ( source, type, val ) {
   		 	                      	 	   return source[20];
   		 	                        		}},
	    		 	                        {	"sTitle":"Product Type",
	    		 	                        			"aTargets": [ 6], "mDataProp": function ( source, type, val ) { 
	    		 	                      	  		return source[3];
	    		 	                      	  }},
	    		 	                        {	"sTitle":"Product Source",
	    		 	                      		  "aTargets": [ 7], "mDataProp": function ( source, type, val ) { 
	    		 	                      	  		return source[4];
	    		 	                      	  }},
	    		 	                        {	"sTitle":"Status",
	    		 	                      		  "aTargets": [ 8], "mDataProp": function ( source, type, val ) { 
	    		 	                      	  		return source[6];
	    		 	                      	  }},
	    		 	                        {	"sTitle":"ABO/Rh",
	    		 	                      		  "aTargets": [9], "mDataProp": function ( source, type, val ) { 
	    		 	                      	  		return source[16];
	    		 	                      	  }}		 	                        
	    		 	                 ];

	    var proSearch_aoColumn = [ { "bSortable": false},null,null,null,null,null,null,null,null,null];
	    var proSearch_ColManager = function(){
												$("#productSearchColumn").html($('#productSearchTable_wrapper .ColVis'));
	    									 };
	    									 
	    	/*Product Search Table server Parameters Ends*/
	    	
	    	constructTable(true,'productSearchTable',proSearch_ColManager,proSearch_serverParam,proSearch_aoColumnDef,proSearch_aoColumn);
	    	$("#activeProduct").show();
	    	$("#archieved").hide();


	    	/*$("#barcodetabs").hide();
	    	$("#protoggle").toggleClass( "ui-icon-plusthick" ).toggleClass( "ui-icon-minusthick" );
			$("#protoggle").parents( ".portlet:first" ).find( "#proFind" ).toggle();
			$("#protoggle").find("span:first").removeClass("ui-icon ui-icon-plusthick").addClass("ui-icon ui-icon-minusthick");
			*/
	    	//alert("7");
	    	/*$('#prosearchid').attr('checked',true);
	    	$('#archiveSearchId').attr('checked',false);
	    	*/
	    	//alert("8");
	    	//return false;
	    });
	    $('#archiveSearchId').click(function (e) {
	    	$("#printSelected").hide();
	    	showBar();
	    	//alert("ss");
	    	var searchStr = "technician_accession_barcode";
	    	/*Product Search Table server Parameters Starts*/
	    	var searchStr = "technician_accession"; 
	    	var proSearch_serverParam = function ( aoData ){

	    													aoData.push( { "name": "application", "value": "stafa"});
	    													aoData.push( { "name": "module", "value": "technician_accession_barcode_archieved"});	
	    													
	    													aoData.push( {"name": "col_1_name", "value": "lower((SELECT PERSON_MRN FROM person WHERE PK_PERSON=d.FK_RECEIPENT))" } );
	    													aoData.push( { "name": "col_1_column", "value": "lower(RecMRN)"});
	    													
	    													aoData.push( { "name": "col_2_name", "value": " lower((select (PERSON_LNAME || PERSON_FNAME) from person where pk_person=d.fk_receipent))"});
	    													aoData.push( { "name": "col_2_column", "value": "lower(recipientName)"});
	    													
	    													aoData.push( { "name": "col_3_name", "value": " lower((select (PERSON_LNAME || PERSON_FNAME) from person where pk_person=d.FK_PERSON))"});
	    													aoData.push( { "name": "col_3_column", "value": "lower(donorName)"});
	    													
	    													aoData.push( {"name": "col_4_name", "value": " lower(sp.spec_id)" } );
	    													aoData.push( { "name": "col_4_column", "value": "lower(sp.spec_id)"});
	    													
	    													aoData.push( {"name": "col_4_name", "value": "" } );
	    													aoData.push( { "name": "col_4_column", "value": ""});
	    													
	    													aoData.push( {"name": "col_6_name", "value": " lower((SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST = d.FK_CODELSTPRODUCTTYPE))" } );
	    													aoData.push( { "name": "col_6_column", "value": "lower(productType)"});
	    													
	    													aoData.push( {"name": "col_7_name", "value": " lower((SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST = d.FK_CODELSTDONORTYPE))" } );
	    													aoData.push( { "name": "col_7_column", "value": "lower(productSource)"});
	    																		
	    													aoData.push( {"name": "col_8_name", "value": " lower((SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST=pp.fk_codelstdonorstatus))" } );
	    													aoData.push( { "name": "col_8_column", "value": "lower(status)"});
	    													
	    												  	aoData.push( {"name": "col_9_name", "value": " lower((SELECT CODELST_DESC FROM er_codelst WHERE PK_CODELST = (SELECT FK_CODELST_BLOODGRP FROM person WHERE PK_PERSON=d.FK_RECEIPENT)))" } );
	    													aoData.push( { "name": "col_9_column", "value": "lower(BLOODGRP)"});
	    													
	    													};

	    var proSearch_aoColumnDef = [
	    	 	                       		{"aTargets": [ 0], "mDataProp": function ( source, type, val ) { 
	    		 	                      	 //return "<input type='checkbox' id='pkRecipient' name='pkRecipient' value='"+source[10]+"'/><input type='hidden' id='productDonor' name='pkRecipient' value='"+source[11]+"'/><input type='hidden' id='hiddenpersonprId' value="+source[3]+"><input type='hidden' id='specimenId' name='specimenId' value='"+source[12]+"'/>";
	    	 	                       		return "<input type='hidden' id='pkRecipient' name='pkRecipient' value='"+source[10]+"'/><input type='hidden' id='productDonor' name='pkRecipient' value='"+source[11]+"'/><input type='hidden' id='hiddenpersonprId' value="+source[3]+"><input type='hidden' id='specimenId' name='specimenId' value='"+source[12]+"'/><input type='hidden' id='plasmaId' name='plasmaId' value='"+source[20]+"'/><input type='hidden' id='plasmaName' name='plasmaName' value='"+source[21]+"'/>";
	    		 	                      	 }},
	    		 	                        {	"sTitle":"Recipient MRN",
	    		 	                      		 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
		    		 	                      		 
	    		 	                      	 return source[16];
	    		 	                      	 }},
	    		 	                        {	"sTitle":"Recipient Name",
	    		 	                      		 "aTargets": [ 2], "mDataProp": function ( source, type, val ) { 
	    		 	                      		    return source[2]+"<br/>"+source[18];
	    		 	                      		    }},
	    		 	                        {	"sTitle":"Donor Name",
	    		 	                      		 "aTargets": [ 3], "mDataProp": function ( source, type, val ) { 
	    		 	                      			return source[8]+"<br/>"+source[19];
	    		 	                      			  }},
	    		 	                        {	"sTitle":"Product ID",
	    		 	                      		    	"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
	    		 	                      	 	   return source[3];
	    		 	                        		}},
   		 	                        		{	"sTitle":"Plasma ID",
   		 	                      		    	"aTargets": [ 5], "mDataProp": function ( source, type, val ) {
   		 	                      	 	   		return source[21];
   		 	                        		}},
	    		 	                        {	"sTitle":"Product Type",
	    		 	                        			"aTargets": [ 6], "mDataProp": function ( source, type, val ) { 
	    		 	                      	  		return source[4];
	    		 	                      	  }},
	    		 	                        {	"sTitle":"Product Source",
	    		 	                      		  "aTargets": [ 7], "mDataProp": function ( source, type, val ) { 
	    		 	                      	  		return source[5];
	    		 	                      	  }},
	    		 	                        {	"sTitle":"Status",
	    		 	                      		  "aTargets": [ 8], "mDataProp": function ( source, type, val ) { 
	    		 	                      	  		return source[7];
	    		 	                      	  }},
	    		 	                        {	"sTitle":"ABO/Rh",
	    		 	                      		  "aTargets": [ 9], "mDataProp": function ( source, type, val ) { 
	    		 	                      	  		return source[17];
	    		 	                      	  }}		 	                        
	    		 	                 ];

	    var proSearch_aoColumn = [ { "bSortable": false},null,null,null,null,null,null,null,null,null];
	    var proSearch_ColManager = function(){
	    										$("#productSearchColumn").html($('#productSearchTable_wrapper .ColVis'));
	    									 };

	  // var drawCallBack = "productSearchTableRowClick"; 
	   var barcode_default_Sort = [[1,"asc"]];
	    /*Product Search Table server Parameters Ends*/ 
	    	constructTable(true,'productSearchTable',proSearch_ColManager,proSearch_serverParam,proSearch_aoColumnDef,proSearch_aoColumn,barcode_default_Sort,drawCall)
	    	//constructTable(flag,tableId,colManager,fn_serverParam,fn_aoColumnDef,column_Sort,default_Sort,drawCallback)
		    		    	//productSearchTableRowClick()
	    	$("#activeProduct").hide();
	    	$("#archieved").show();

	    	var printDate="";
	    	var productDesc="";


	    	/*$("#barcodetabs").hide();
	    	$("#protoggle").toggleClass( "ui-icon-plusthick" ).toggleClass( "ui-icon-minusthick" );
			$("#protoggle").parents( ".portlet:first" ).find( "#proFind" ).toggle();
			$("#protoggle").find("span:first").removeClass("ui-icon ui-icon-plusthick").addClass("ui-icon ui-icon-minusthick");
			*/
	    	/*$('#prosearchid').attr('checked',false);
	    	$('#archiveSearchId').attr('checked',true);*/
	    	//alert("5");
	    	//---------------------- Here 
	    	//alert("6");
	    	//return false;
	    });
	    //alert("1");
	    
	    $('#prosearchid').attr('checked',true);
	   // alert("2");
	    //$('#prosearchid').attr('checked','checked');
	    $('#prosearchid').trigger('click');
	    //alert("3");
	    
});

function showLabelPopup(productDesc,printDate){
	//alert("s");
    try{
	$("#archiveLabels tbody[role='alert'] tr").bind('click',function(){
		 $(".ui-dialog").find("#barcodeprint").each(function(i){
			 if(i>1){
 		    	$(this).remove();
 		    	$(this).parent().remove();
			 }
		    });

		 var barcode_label_id = $(this).find("#barcode_label_id").val();
		 //alert("barcode_label_id : "+barcode_label_id);
		 var url = "fetchBarcodeLabel";
		 
		 //var res = jsonDataCall(url,"barcode_label_id='"+barcode_label_id+"'");
		 
		$.ajax({
        type: "POST",
        url: "fetchBarcodeLabel.action?barcode_label_id='"+barcode_label_id+"'",
        async:false,
        data:"",
        success:function (result){
        	var $response=$(result);
            var retStr = $response.find('#returnJsonStr').val();
            	//alert("retStr : "+retStr);
            	//retStr = retStr.replace("barcode_wrapper","barcode_wrapper1")
            	$("#barcodeMainDiv").removeClass("barcode_wrapper");
            	retStr = retStr.replace('>00<','>05<');
            	retStr = retStr.replace('00&','05&');
            	$("#barcodeMainDiv").html(retStr);

            	$("#productLabel").val(retStr);
            	$("#reasonForRprint").val("");
            	$("#barcodeMainDiv").prepend('<div><input type="password" style="width:80px;" size="5" value="" id="barcodeeSign" name="barcodeeSign" placeholder="e-Sign"/><input type="button" class="cursor" id="printBut" value="Print" onclick="RePrintBarcode();"/></div>');
            	//alert($("#barcodeMainDiv").html());
            	$("#rePrintOrigPrintDate").html(printDate)
    			 $("#rePrintProductDescription").html(productDesc);
        			},
        	        error: function (request, status, error) {
        	            alert("Error " + error);
        	        }
		});    		 
		/* showPopUp("open","barcodeprinting","barcodeprinting","700","300"); */
		 $("#barcodeprint").dialog({
			width : 800,
 			height : 500,
 			autoOpen: false,
 			title:"Barcode Printing",
			modal: true,
			close: function() {
 				jQuery("#barcodeprint").dialog("destroy");
			}
			});
			
		$('#barcodeprint').dialog('open');
		$("#barcodeprint img").each(function(){
		       var timeStamp = (new Date()).getTime();
		       //alert("==>"+$(this).attr("src"));
		       if($(this).hasClass("ref")){
		       	$(this).attr("src", $(this).attr("src") +"&date="+ timeStamp);
		       }
		    });
		 
	});
    }catch(e){
        alert("Error : "+e);
        }
}

function showArchiveLabel(divid){
    	//alert("divid "+divid);
    	if($("#"+divid).hasClass('hidden')){
		$("#proFind").addClass("hidden");
		$("#"+divid).removeClass("hidden");
		}
	}

var drawCall = function productSearchTableRowClick(){
	//alert("R");
	$('#productSearchTable tbody[role="alert"] tr').click(function(){
    	showArchiveLabel('archiveProductLabel');
		var oTable;
		var flag=true;
		var barEntity = $(this).find("#specimenId").val();
		
		var criteria = "";
		if(barEntity!=null && barEntity!=""){
			criteria = "where barcode_entity= '"+barEntity+"'";
		}
		if(flag || flag =='true'){
	 		oTable=$('#archiveLabels').dataTable().fnDestroy();
		}
		$("#archiveLabels").find(".ColVis").each(function(){
            $(this).remove();
        });
		  var link ;
			try{
				oTable=$('#archiveLabels').dataTable( {
					"sPaginationType": "full_numbers",
					"aoColumns":[{"bSortable": false},
					               null,
					               null,
					               null,
					               null,
					               null,
					               null
					               ],
					"bProcessing": true,
					"bServerSide": true,
					"bAutoWidth": false,
					"bRetrieve" : true,
					"sDom": 'C<"clear">Rlfrtip',
					"bDestroy": true,
					"aaSorting": [[ 1, "asc" ]],
					"oColVis": {
							"aiExclude": [ 0 ],
							"buttonText": "&nbsp;",
							"bRestore": true,
							"sAlign": "left",
							"iOverlayFade": 25,
						},
					"oLanguage": {"sProcessing":""}	,								
					"fnServerParams": function ( aoData ) {
															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "barcode_ArchiveLabels"});	
															aoData.push( { "name": "criteria", "value": criteria});
															
															aoData.push( {"name": "col_1_name", "value": "lower(barcode_text)" } );
															aoData.push( { "name": "col_1_column", "value": "lower(barcode_text)"});
															
															aoData.push( { "name": "col_2_name", "value": "lower((SELECT maintenance_name FROM er_barcode_maintenance WHERE pk_barcode_maintenance=type_of_label))"});
															aoData.push( { "name": "col_2_column", "value": "lower((SELECT maintenance_name FROM er_barcode_maintenance WHERE pk_barcode_maintenance=type_of_label))"});
															
															aoData.push( {"name": "col_3_name", "value": "lower(product_status)" } );
															aoData.push( { "name": "col_3_column", "value": "lower(product_status)"});
															
															aoData.push( {"name": "col_4_name", "value": "lower((SELECT prod_description FROM er_product_description_code WHERE prod_descrip_code=PRODUCT_DESCRIPTION))" } );
															aoData.push( { "name": "col_4_column", "value": "lower((SELECT prod_description FROM er_product_description_code WHERE prod_descrip_code=PRODUCT_DESCRIPTION))"});
															
															aoData.push( {"name": "col_5_name", "value": "lower(original_print_date)" } );
															aoData.push( { "name": "col_5_column", "value": "lower(original_print_date)"});
															
															aoData.push( {"name": "col_6_name", "value": "lower(no_of_print)" } );
															aoData.push( { "name": "col_6_column", "value": "lower(no_of_print)"});
										},
					"sAjaxSource": 'dataSearch.action',
					"sAjaxDataProp": "filterData",
					"aoColumnDefs": [
									 {
											"aTargets": [0], "mDataProp": function ( source, type, val ) { 
											    return "<input type='hidden' id='barcode_label_id' name='barcode_label_id' value='"+source[0] +"' >";
									     }},
					                  {"sTitle":"Product DIN",
											"aTargets": [1], "mDataProp": function ( source, type, val ) {
												return source[2];
					                	 }},
									  {	"sTitle":"Type of Label",
					                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
					                			 return source[3];
					                	  }},
									  {	"sTitle":"Product Status at Printing",
											  "aTargets": [3], "mDataProp": function ( source, type, val ) {
						                		 return source[4];
									  	  }},
									  {	"sTitle":"Product Description",
										  	  "aTargets": [4], "mDataProp": function ( source, type, val ) {
										  		
										    	productDesc=source[5];
						                		 return source[5];
								  		  }},
									  {	"sTitle":"Original Print Date",
									  		  "aTargets": [5], "mDataProp": function ( source, type, val ) {
									  			printDate=converHDate(source[6]); 
						                		 return printDate;
										  }},
									  {	"sTitle":"Number Printed",
									  		  "aTargets": [6], "mDataProp": function ( source, type, val ) { 
						                		 return source[7];
										  }}
									],
						"fnInitComplete": function () {
								        $("#archiveLabelsColumn").html($("#archiveLabels_wrapper .ColVis"));
									},
						"fnServerData": function ( sSource, aoData, fnCallback ) {
							$('.progress-indicator').css( 'display', 'block' );
							$.ajax({
									"dataType": 'json',
									"type": "POST",
									"url": 'dataSearch.action',
									"async":false,
									"data":aoData,
									"success":fnCallback,  
									"complete": function () {
												$('.progress-indicator').css( 'display', 'none' );
												$("select").uniform();
												}
								  });},
					"fnDrawCallback": function( oSettings ) {
										//alert( 'DataTables has redrawn the table' );
								    }
								  
					}  );
				jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = parseInt(no_of_pages);
				}catch(err){
					alert("error " + err);
				}
				//
				showLabelPopup(productDesc,printDate);
	});
}

function showBar(){
	if($("#proFind").hasClass('hidden')){
		$("#proFind").removeClass('hidden');
		$("#archiveProductLabel").addClass('hidden');
	}
}

</script>