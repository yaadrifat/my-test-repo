<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/barcodeScanBox.js"></script>
<script>
$(document).ready(function(){
	hide_slidecontrol();
	hide_slidewidgets();
var oTable=$("#donordetailsTable").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null
											             ],
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#donordetails").html($('.ColVis:eq(0)'));
												}
		});
		
$("select").uniform(); 

});
function getScanData(event,obj){
  	if(event.keyCode==13){
       
    
      
    }
}
</script>






 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header addnew">Donor List</div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="donordetailsTable" class="display" >
				<thead>
				   <tr>
				   		<th width="4%" id="donordetails"></th>
						<th >Donor Id</th>
						<th >Donor Name</th>
						<th >Donor DOB</th>
						<th >Lab Result</th>
						<th >Documents</th>
						<th >Donation Type</th>
						<th >Planned Procedure</th>
						<th >Nurse</th>
						<th >Status</th>
					</tr> 
				</thead>
				<tbody>
				 	<tr>
						<td> </td>	
						<td>HCA12376397</td>
						<td> John Wilson</td>
						<td> Aug 05,1987</td>
						<td> Reviewed</td>
						<td>Reviewed </td>
						<td>Auto </td>
						<td>Apheresis Day 1-HPC </td>
						<td> Nurse1</td>
						<td>Collection in Progress </td>					
					</tr>
					<tr>
						<td> </td>	
						<td> HCA12376349</td>
						<td> Steve Johnson </td>
						<td> Mar 15,1987</td>
						<td> Reviewed</td>
						<td>Reviewed </td>
						<td>Allo Related</td>
						<td> Nurse2</td>
						<td> Apheresis Day 1-HPC </td>
						<td>Pending Arrival </td>					
					</tr>
					<tr>
						<td> </td>	
						<td> HCA12376343</td>
						<td> Mary Smith </td>
						<td> Sep 25,1987</td>
						<td> Reviewed</td>
						<td>Reviewed </td>
						<td>Allo Related </td>
						<td> </td>
						<td>  Apheresis Day2-HPC</td>
						<td>Ready for Collection </td>					
					</tr>
					<tr>
						<td> </td>	
						<td> HCA12376345</td>
						<td> Allan Johnson</td>
						<td> Nov 03,1987</td>
						<td> </td>
						<td> </td>
						<td>Auto </td>
						<td>Follow up Day1-HPC </td>
						<td>Nurse 1 </td>
						<td>Procedure Complete</td>					
					</tr>
					<tr>
						<td> </td>	
						<td>HCA12376383 </td>
						<td>Mary Lee</td>
						<td> Jan 22,1987</td>
						<td> </td>
						<td> </td>
						<td>Allo Related </td>
						<td> </td>
						<td> </td>
						<td>Pending Arrival</td>					
					</tr>
				</tbody>
			</table>
	</div>
	</div>	
</div>
<div id="addNewDonor" title="addNew" style="display: none">
        <form action="#" id="donorNewForm"><br>
          <div id="button_wrapper">
        		<div style="margin-left:40%;">
       				 <input class="scanboxSearch barcodeSearch"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;" onkeyup="performScanData(event,this)" />
					<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" onfocus="changeType(this)"/>
	 		</div>
        </div><br>
            <table border="0" >
                <tr>
                    <td class="tablewapper"><s:text name="stafa.label.donorid" /></td>
                    <td class="tablewapper1"><input type="text" id="donorid" name="donorid"/></td>
                    <td class="tablewapper"><s:text name="stafa.label.donorabo" /></td>
                    <td class="tablewapper1"><div>
                    <select name="	donorabo">
                    <option >Select</option>
                    <option value="0">0</option>
                        </select></div></td>
                </tr>
                <tr>
                    <td class="tablewapper"><s:text name="stafa.label.recipient_LastName"/></td>
                    <td class="tablewapper1"><input type="text" id="recipientLastName" name="recipientLastName"/></td>
                    <td class="tablewapper"><s:text name="stafa.label.recipientdiagnosis"/></td>
                     <td class="tablewapper1"><input type="text" id="diagnosis" name="diagnosis"/></td>
               </tr>
                <tr>
                    <td class="tablewapper"><s:text name="stafa.label.recipient_FirstName"/></td>
                    <td class="tablewapper1"><input type="text" id="recipientFirstName" name="donorFirstName"/></td>
                    <td class="tablewapper"><s:text name="stafa.label.weight"/></td>
                     <td class="tablewapper1"><input type="text" id="recipientWeight" name="recipientWeight"/></td>
               </tr>
   				  <tr>
                    <td class="tablewapper"><s:text name="stafa.label.recipientdob"/></td>
                    <td class="tablewapper1"><input type="text" id="recipientDob" name="recipientDob" class="dateEntry"/></td>
                    <td class="tablewapper"><s:text name="stafa.label.donorPhysician"/></td>
                    <td class="tablewapper1"><div>
                    <select name="donorPhysician">
                    <option >Select</option>
                    <option >Hay, Nancy</option>
                    
                        </select></div></td>
               </tr>
   
            </table>
            <br>
            <div id="button_wrapper">
        		<div style="margin-left:40%;">
       				 <input class="scanboxSearch barcodeSearch multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;" onkeyup="performScanData(event,this)" />
					<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" onfocus="changeType(this)" />
	 		</div>
        </div><br>
            <table border="0" >
                <tr>
                    <td class="tablewapper"><s:text name="stafa.label.recipientid" /></td>
                    <td class="tablewapper1"><input type="text" id="recipientid" name="recipientid"/></td>
                    <td class="tablewapper"><s:text name="stafa.label.recipientabo" /></td>
                    <td class="tablewapper1"><div>
                    <select name="recipientabo">
                    <option >Select</option>
                    <option value="0">0</option>
                        </select></div></td>
                </tr>
                <tr>
                    <td class="tablewapper"><s:text name="stafa.label.donor_LastName"/></td>
                    <td class="tablewapper1"><input type="text" id="donorLastName" name="donorLastName"/></td>
                    <td class="tablewapper"><s:text name="stafa.label.Donor_Weight"/></td>
                     <td class="tablewapper1"><input type="text" id="donorWeight" name="donorWeight"/></td>
               </tr>
                <tr>
                    <td class="tablewapper"><s:text name="stafa.label.donor_FirstName"/></td>
                    <td class="tablewapper1"><input type="text" id="donorFirstName" name="donorFirstName"/></td>
                    <td class="tablewapper"><s:text name="stafa.label.Donor_Height"/></td>
                     <td class="tablewapper1"><input type="text" id="donorHeight" name="donorHeight"/></td>
               </tr>
   				  <tr>
                    <td class="tablewapper"><s:text name="stafa.label.donordob"/></td>
                    <td class="tablewapper1"><input type="text" id="donorDob" name="donorDob" class="dateEntry"/></td>
                    <td class="tablewapper"><s:text name="stafa.label.transplant_Physician"/></td>
                    <td class="tablewapper1"><div>
                    <select name="transplantPhysician">
                    <option >Select</option>
                    <option >Hay, Nancy</option>
                    
                        </select></div></td>
               </tr>
               <tr> <td style="font-weight:bold">Procedure</td></tr>
 			  <tr>
                    <td class="tablewapper"><s:text name="stafa.label.donortype"/></td>
                    <td class="tablewapper1"><div>
                    	<select name="donorType">
                    		<option >Select</option>
                    		<option >Hay, Nancy</option>
                        </select></div>
                      </td>
                       <td class="tablewapper"><s:text name="stafa.label.plannedprocedure"/></td>
                    <td class="tablewapper1"><div>
                    	<select name="plannedprocedure">
                    		<option >Select</option>
                    		<option >Hay, Nancy</option>
                        </select></div>
                      </td>
            	</tr>
            	 <tr>
                    <td class="tablewapper"><s:text name="stafa.label.producttype"/></td>
                    <td class="tablewapper1"><div>
                    	<select name="productType">
                    		<option >Select</option>
                    		<option >Hay, Nancy</option>
                        </select></div>
                      </td>
                       <td class="tablewapper"><s:text name="stafa.label.status"/></td>
                    <td class="tablewapper1"><div>
                    	<select name="status">
                    		<option >Select</option>
                    		<option >Hay, Nancy</option>
                        </select></div>
                      </td>
            	</tr>
               <tr><td align="right" colspan="4"><input type="button" name="Submit" value="Submit"/>&nbsp;<input type="button" name="cancel" onclick="" value="Cancel"/></td></tr>
            </table>
            
            </form>
</div>
<script>
$(function() {

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
    .append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span>");
	
	$( ".portlet-header .ui-addnew" ).click(function() {
	    showPopUp("open","addNewDonor","Add New Donor","900","700")
	});
	/**For Notes**/
	/* 
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='ui-addnew'></span>");
	
	$( ".portlet-header .ui-addnew" ).click(function() {
		addNewPopup("Add New Product","SupervisoraddNewDialog","addnewAcquisition","700","1100");
	});
	 */
	
	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});
</script>