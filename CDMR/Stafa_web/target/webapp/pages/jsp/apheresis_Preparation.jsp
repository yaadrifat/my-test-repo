<%@ include file="common/includes.jsp" %>
<script type="text/javascript" src="js/placeholder.js"></script>
<script type="text/javascript" src="js/jquery/jquery.Print.js"></script>
<script type="text/javascript" src="js/jquery/jquery.printElement.js"></script>
<!-- <script type="text/javascript" src="js/stafa_Time.js"></script> -->
<script>
function checkDateData(){
	 if($("#equipmentPrimedDate").val()!=""){
			setvalidation(true);
		}else{
			setvalidation(false);
		}
}
function setvalidation(flag){
	if(flag){
		if($("#tempPrimedTime").hasClass("validate_required")==false){
			$("#tempPrimedTime").addClass("validate_required condtionMandatory");
			$(".condtionMandatory").after("<sup style='margin-left:2px'><label class='mandatory'>*</label></sup>");
		}
	}else{
		$("#tempPrimedTime").removeClass("validate_required condtionMandatory errorBorder");
		$("#tempPrimedTime").parent().find('sup').remove();
	}
}


function savePages(){
	if(checkModuleRights(STAFA_PREPARATION,APPMODE_SAVE)){
		try{
			if(savePreparation()){
				return true;
			}
		}catch(e){
			alert("exception " + e);
		}
	}
}
/* function notes(){
    showNotes('open',this);
    fetchnotes();
} */
/* function fetchnotes(){
	var applcationCode="STAFA";
	var entityId=$("#donor").val();
	var jsonData = "applnCode:'"+applcationCode+"',entityId:'"+entityId+"',entityType:'donor' ";
	var url="fetchNotes";
	response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	alert("response.notesDetails"+response.notesDetails);
	var notesHtml="<table><tr><td>";
	 $(response.notesDetails).each(function() {
		 		 $(this).find("note").each(function(){
		 			 var notecontent=$(this).find("notedescription").text();
		 			 var createdOn=$(this).find("createdOn").text();
		 			 alert("notecontent"+notecontent);
		 			alert("createdBy"+createdOn);
		 			notesHtml+="<td>"+notecontent+"</td><td>"+createdOn+"</td></tr></table>"
		 			$("#existingNotes").html(notesHtml);
		 		 });
	 });
} */
 /* function savingNotes(){
	var applcationCode="STAFA";
	$("#dialogEditor").val($(" #noteDialog").find("iframe").contents().find("body").html());
	var content=$("#dialogEditor").val(); */
	/*
	alert("content"+content);
	content=content.replace(/</g,"#lt;");
	alert("content"+content);
	content=content.replace(/>/g,"#gt;");
	*/
	/* alert("content"+content);
	
	var entityId=$("#donor").val();
	var jsonData = "applnCode:'"+applcationCode+"',notedescription:\""+content+"\",entityId:'"+entityId+"',entityType:'donor' ";
	alert("jsonData=="+jsonData)
	var url="savingNotes";
	response = jsonDataCall(url,"jsonData={"+encodeURIComponent(jsonData)+"}");
} 
 */
/*For Barcode Start*/
  
  function RePrintBarcode(){
	//alert(" rePrint");
	 var reason = $("#reasonForRprint").val();
	 $("#reasonReprint").val(reason);
	 printBarcode();
	}
 
 
 
 
function searchProductCodeDescription(This){
    var barcodeClass = $("#barcodeClass").val();
    var modifier = $("#modifier").val();
    var manipulation = $("#manipulation").val();
    var cryoprotectant = $("#cryoprotectant").val();
    var collectionVolume = $("#collectionVolume").val();
    var anticoagulantType = $("#anticoagulantType").val();
    var storageTemprature = $("#storageTemprature").val();
    var productDesc = "";
    var coreCondition = "";
    var groupVariables = "";
    var finalProductDescCode="";
    if(modifier!=null && modifier!=""){
        productDesc+=modifier+" "+barcodeClass;
    }else{
        productDesc+=barcodeClass;
    }

    if(anticoagulantType!=null && anticoagulantType!=""){
        coreCondition+="|"+anticoagulantType;
    }

    if(storageTemprature!=null && storageTemprature!=""){
        coreCondition+="/XX/<"+storageTemprature;
    }

    if(cryoprotectant!=null && cryoprotectant!=""){
        coreCondition+="|"+cryoprotectant;
    }

    if(productDesc!="" && coreCondition!=""){
        finalProductDescCode =productDesc+coreCondition;
        }else{
            finalProductDescCode =productDesc;
            }
    //alert(finalProductDescCode);
   
    $('#productDescriptionInput').val(finalProductDescCode);
    $('#productDescriptionInput').keyup();
    $('#productDescriptionInput').keypress();
    $('#productDescriptionInput').keydown();
}
/*
function loadBarcodePage(){
	setTimePicker();
    loadpopup();
}

function loadpopup(){
	
	 $("#maindiv").load("loadBarcodePopup.action").dialog({
			   autoOpen: true,
			   modal: true, width:1050, height:710,
			   close: function() {
				   jQuery("#maindiv").dialog("destroy");
				   //$(this).html(" ");
				   $(this).remove();
				  $("#mainbarcodediv").append('<div id="maindiv"></div>');

				  
			   }
			});

	}
*/
/*For Barcode End*/
var donorplannedprocedure;

function deleteReagentsData(){
	// alert("Delete Rows");
	if(checkModuleRights(STAFA_PREPARATION,APPMODE_DELETE)){
	    var yes=confirm(confirm_deleteMsg);
		if(!yes){
			return;
		}
		var deletedIds = ""
		$("#reagenTable #preparationReagents:checked").each(function (row){
			deletedIds += $(this).val() + ",";
		});
		if(deletedIds.length >0){
		  deletedIds = deletedIds.substr(0,deletedIds.length-1);
		}
		jsonData = "{deletedReagentsResult:'"+deletedIds+"'}";
		url="deleteReagentsData";
	    response = jsonDataCall(url,"jsonData="+jsonData);
	    constructReagentsTable(true,donorId);
	}
}
 
function deleteEquimentData(){
	// alert("Delete Rows");
	if(checkModuleRights(STAFA_PREPARATION,APPMODE_DELETE)){
	    var yes=confirm(confirm_deleteMsg);
		if(!yes){
			return;
		}
		var deletedIds = ""
		$("#equipmentTable #preparationEquipmentsId:checked").each(function (row){
			deletedIds += $(this).val() + ",";
		});
		if(deletedIds.length >0){
		  deletedIds = deletedIds.substr(0,deletedIds.length-1);
		}
		jsonData = "{deletedEquimentResult:'"+deletedIds+"'}";
		url="deleteEquimentData";
	    response = jsonDataCall(url,"jsonData="+jsonData);
	    constructEquipmentTable(true,donorId);
	}
}
 
 
var donorId = $("#donor").val();
var oTable;
function constructReagentsTable(flag,donorId){
	 var reagentCriteria = "";
		/*if(donorId!=null && donorId!=""){
			reagentCriteria = " and pre.FK_DONOR = '"+donorId+"'";
		}
		*/
		var preparation = $("#preparation").val();
		var plannedProcedure = $("#donorProcedure").val();
		var reagentsServerParam = function ( aoData ) {
			aoData.push( { "name": "application", "value": "stafa"});
			aoData.push( { "name": "module", "value": "apheresis_Preparation_Reagents"});	
			//aoData.push( { "name": "criteria", "value": reagentCriteria});
			aoData.push( { "name": "param_count", "value":"2"});
			aoData.push( { "name": "param_1_name", "value":":PREPARATION:"});
			aoData.push( { "name": "param_1_value", "value":preparation});
			aoData.push( { "name": "param_2_name", "value":":PLANNEDPROCEDURE:"});
			aoData.push( { "name": "param_2_value", "value":plannedProcedure});
			
			
			aoData.push( {"name": "col_1_name", "value": "lower(rs.RS_NAME)" } );
			aoData.push( { "name": "col_1_column", "value": "2"});
			
			aoData.push( { "name": "col_2_name", "value": "lower(prereg.QUANTITY)"});
			aoData.push( { "name": "col_2_column", "value": "lower(prereg.QUANTITY)"});
			
			aoData.push( {"name": "col_3_name", "value": "lower(rs.RS_MANUFACTURE)" } );
			aoData.push( { "name": "col_3_column", "value": "lower(rs.RS_MANUFACTURE)"});
			
			aoData.push( {"name": "col_4_name", "value": "lower(rs.RS_LOTNUMBER)" } );
			aoData.push( { "name": "col_4_column", "value": "lower(rs.RS_LOTNUMBER)"});
			
			aoData.push( {"name": "col_5_name", "value": "lower(rs.RS_EXPDATE)" } );
			aoData.push( { "name": "col_5_column", "value": "lower(rs.RS_EXPDATE)"});
			
		};
		var reagentColDef = [
							 {
									"aTargets": [0], "mDataProp": function ( source, type, val ) { 
									    return "<input type='checkbox' id='preparationReagents' name='preparationReagents' value='"+source[0]+"'/><input id='reagentsupplies' type='hidden' value='"+source[6]+"' name='reagentsupplies'/>";
							     }},
			                  {"sTitle":"Reagents and Supplies",
									"aTargets": [1], "mDataProp": function ( source, type, val ) {
										link = source[1];
										if (source[0] == 0){
											 link ="<select class='select1'  id='reagentsname' name='reagentsname' onchange='javascript:loadDropdown(this,\"reagents_manufacturer\");'>";
												 var selectTxt = $("#reagent_hidden").html();
		                                	  var searchTxt = 'value="'+source[1] +'"'; 
		                                	  selectTxt = selectTxt.replace(searchTxt,searchTxt +' selected')
												link+= selectTxt + "</select>";
										}
										return link;	
			                	 }},
							  {	"sTitle":"Quantity",
			                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
			                			 link = source[7];
											if (source[0] == 0){
												 link ="<input type='text' id='reagents_quality' name='quantity' value='"+source[7]+ "' />";
											}
											return link;	
			                	  }},
							  {	"sTitle":"Manufacturer",
									  "aTargets": [3], "mDataProp": function ( source, type, val ) {
										  link = source[3];
				                			 if (source[0] == 0){
				                				 link = "<select class='select1' id='reagents_manufacturer' name='reagents_manufacturer' onchange='javascript:loadDropdown(this,\"reagents_lot\");'>";
				                				 var tmpCr = "  and rs_type='REAGENTS'  and name ='"+source[1] +"'";
				                				 loadTmpDropdown($("#tmpSelect"),"manufacture",tmpCr);
				                				// alert(" tmp " +$("#tmpSelect").html() );
				                				link+= $("#tmpSelect").html() +"</select>";
				                			 }
				                			 return link;
							  	  }},
							  {	"sTitle":"Lot#",
								  	  "aTargets": [4], "mDataProp": function ( source, type, val ) {
								  		 link = source[4];
			                			 if (source[0] == 0){
			                				 link = "<select class='select1' id='reagents_lot' name='reagents_lot' onchange='javascript:loadDropdown(this,\"reagents_expiration_date\");' >";
			                				// var tmpCr = "  and rs_type='REAGENTS'  and name ='"+source[1]) +"' and manufacture='"+$.trim($(obj).val())+"' ";
			                				// loadTmpDropdown($("#tmpSelect"),"manufacture",tmpCr);
			                				// alert(" tmp " +$("#tmpSelect").html() );
			                				//			link+= $("#tmpSelect").html() +"</select>";
			                				 link+= "</select>";
			                			 }
			                			 return link;
						  		  }},
							  {	"sTitle":"Expiration Date",
							  		  "aTargets": [5], "mDataProp": function ( source, type, val ) {
							  			link = source[5];
			                			 if (source[0] == 0){
			                				 link= "<input type='text' id='reagents_expiration_date' name='reagents_expiration_date' class='dateEntry dateclass' value=''  />";
			                			 }
			                			 return link;
								  }}
							];
		var reagentColManager = function () {
			$("#reagenColumn").html($('.ColVis:eq(1)'));
		};
		var reagentCol = [{"bSortable": false},
			               null,
			               null,
			               null,
			               null,
			               null
			               ];
		 constructTable(flag,'reagenTable',reagentColManager,reagentsServerParam,reagentColDef,reagentCol);

}
function constructEquipmentTable(flag,donorId){
	 var criteria = "";
		/*if(donorId!=null && donorId!=""){
			criteria = " and pre.FK_DONOR = '"+donorId+"'";
		}*/
		var preparation = $("#preparation").val();
		var plannedProcedure = $("#donorProcedure").val();
		var link;
			var equipServerParam =  function ( aoData ) {
															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "apheresis_Preparation_Equipments"});	
															//aoData.push( { "name": "criteria", "value": criteria});
															
															aoData.push( { "name": "param_count", "value":"2"});
															aoData.push( { "name": "param_1_name", "value":":PREPARATION:"});
															aoData.push( { "name": "param_1_value", "value":preparation});
															aoData.push( { "name": "param_2_name", "value":":PLANNEDPROCEDURE:"});
															aoData.push( { "name": "param_2_value", "value":plannedProcedure});
															
															
															aoData.push( {"name": "col_1_name", "value": "" } );
															aoData.push( { "name": "col_1_column", "value": ""});
															
															aoData.push( { "name": "col_2_name", "value": ""});
															aoData.push( { "name": "col_2_column", "value": ""});
															
															aoData.push( {"name": "col_3_name", "value": "" } );
															aoData.push( { "name": "col_3_column", "value": ""});
															
															aoData.push( {"name": "col_4_name", "value": "" } );
															aoData.push( { "name": "col_4_column", "value": ""});
															
															aoData.push( {"name": "col_5_name", "value": "" } );
															aoData.push( { "name": "col_5_column", "value": ""});
															
										};
					var equipColDef =  [
									 {
											"aTargets": [0], "mDataProp": function ( source, type, val ) { 
												return "<input type='checkbox' id='preparationEquipmentsId' name='preparationEquipmentsId' value='"+source[0]+"'/><input id='reagentsupplies' type='hidden' value='"+source[6]+"' name='reagentsupplies'/>";
									     }},
					                  {"sTitle":"Equipment",
											"aTargets": [1], "mDataProp": function ( source, type, val ) {
												
												if (source[0] == 0){
													 link ="<select class='select1' id='equiment_name' name='equiment_name' onchange='javascript:loadDropdown(this,\"equipment_manufacturer\");' >";
														 var selectTxt = $("#equipment_hidden").html();
				                                	  var searchTxt = 'value="'+source[1] +'"'; 
				                                	  selectTxt = selectTxt.replace(searchTxt,searchTxt +' selected')
														link+= selectTxt + "</select>";
												}else{
													link = source[1];
												}
												
												return link;	
													
												//return source[1];
					                	 }},
									  {	"sTitle":"Manufacturer",
					                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
					                			 link = source[3];
					                			 if (source[0] == 0){
					                				 link = "<select class='select1' id='equipment_manufacturer' name='manufacturer' onchange='javascript:loadDropdown(this,\"modelno\");'>";
					                				 var tmpCr = "  and rs_type='EQUIPMENT'  and name ='"+source[1] +"'";
					                				 loadTmpDropdown($("#tmpSelect"),"manufacture",tmpCr);
					                				// alert(" tmp " +$("#tmpSelect").html() );
					                				link+= $("#tmpSelect").html() +"</select>";
					                			 }
					                			 return link;
					                	  }},
									  {	"sTitle":"Model#",
											  "aTargets": [3], "mDataProp": function ( source, type, val ) {
												  link = source[4];
						                			 if (source[0] == 0){
						                				 link = "<select class='select1' id='modelno' name='modelno' onchange='javascript:loadDropdown(this,\"mhs\");'></select>";
						                			 }
						                		 return link;
									  	  }},
									  {	"sTitle":"MHS#",
										  	  "aTargets": [4], "mDataProp": function ( source, type, val ) {
										  		link = source[2];
					                			 if (source[0] == 0){
					                				 link = "<select class='select1' id='mhs' name='mhs' onchange='javascript:loadDropdown(this,\"maintainanceDueDate\");'></select>";
					                			 }
					                		 return link;
								  		  }},
									  {	"sTitle":"Maintenance Due Date",
									  		  "aTargets": [5], "mDataProp": function ( source, type, val ) { 
									  			link = source[5];
					                			 if (source[0] == 0){
					                				 link = "<input type='text' class='dateEntry dateclass calDate' id='maintainanceDueDate' name='maintainanceDueDate'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/>";
					                			 }
						                		 return link;
										  }}
									];
						var equipColManager =  function () {
								        $("#equipmentTableColumn").html($(".ColVis:eq(0)"));
									};
						var equipCol = [{"bSortable": false},
							               null,
							               null,
							               null,
							               null,
							               null
							               ];
					constructTable(flag,'equipmentTable',equipColManager,equipServerParam,equipColDef,equipCol);

									
} 
function loadPreparationData(){
	
	var donor=$("#donor").val();
	var donorProcedure=$("#donorProcedure").val();
	var url = "loadPreparationData";
	if(donor !=0 && donor != ""){
		 var jsonData = "donor:"+donor+",donorProcedure:"+donorProcedure;
		response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	}
	var qcObj;
	$(response.preparationList).each(function(i){
		 	var dateformat=converHDate(this.equipmentPrimedDate);		 	
			$("#equipmentPrimedDate").val(dateformat); 
			$("#preparation").val(this.preparation);
			//alert(this.preCleaned);
			$("#preCleaned").val(this.preCleaned);
			if($("#preCleaned").val()==1){
				$("#tmppreCleaned").prop("checked",true);
			}else{
				$("#tmppreCleaned").prop("checked",false);
			}
			
			$("#alarmChecked").val(this.alarmChecked);
			
			if($("#alarmChecked").val()==1){
				$("#tmpalarmChecked").prop("checked",true);
			}else{
				$("#tmpalarmChecked").prop("checked",false);
			}
			
			 if(this.equipmentPrimedTime!=null){
				 $("#equipmentPrimedTime").val(this.equipmentPrimedTime);
					var tmpDate = new Date(this.equipmentPrimedTime);
					if($.browser.msie){
						var tmpDate1 = this.equipmentPrimedTime.split("-");
						//alert("tmpDate " + tmpDate1 +" / len----- " + tmpDate1.length + " , " + tmpDate1[2].substring(0,2) + " / " + tmpDate1[2].substring(3));
						var tmpTime = tmpDate1[2].substring(3,8);
						$("#tempPrimedTime").val(tmpTime);
						// tmpDate = new Date(tmpDate1[0] ,tmpDate1[1], tmpDate1[2].substring(0,2) , tmpTime.substring(0,2) , );
					}else{
					collEndTime = (tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes();
					
					$("#tempPrimedTime").val(collEndTime);
					}
				}else{
					$("#tempPrimedTime").val("");
				}
			
		});
    	
		return true;
}
/* function setEquimentPrimeTime(obj){
//	alert("equipmentPrimedDate"+$("#equipmentPrimedDate").val());
		var name=$(obj).attr("name");
		var name_index=name.indexOf("_");
			if(name_index!=-1){
				var timeName=name.substring(0,name_index);
				var tempPrimeTime=$(obj).val();
				var equipmentPrimedDate=$("#equipmentPrimedDate").val();
			//	 $("#"+timeName').val($('#equipmentPrimedDate').val() +" "+('tempPrimedTime')';
			 $("#"+timeName).attr("value",equipmentPrimedDate+" "+tempPrimeTime);
			}
 } */
 
function setvalues(id){
    var s=$('#tmp'+id).attr('checked');
    if(s=='checked'){
        $('#'+id).val(1);
    }else{
        $('#'+id).val(0);
    }
    
}
 var donormrn;
function getslide_widgetdetails(){
	
	var donor=$("#donor").val();
	var donorProcedure = $("#donorProcedure").val();
	url="fetchDonorData";
	 var jsonData = "donor:"+donor+",donorProcedure:"+donorProcedure;
	 response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	 var donorDetails = response.donorDetails;
	 donormrn="";
	 donormrn=donorDetails.personMRN;
	$("#slide_donorId").text(donorDetails.personMRN);
	$("#slide_donorDOB").text(donorDetails.dob);
	
	$("#slide_donorName").text(donorDetails.lastName + " " + donorDetails.firstName);
	$("#slide_procedure").text(donorDetails.plannedProceduredesc);
	var tcCheck = donorDetails.plannedProceduredesc.slice(-2);
 	if(tcCheck == "TC"){
 		$(".cd34").hide();
 		
 	}else{
 		$(".cd3").hide();
 	}
	$("#slide_donorWeight").text(donorDetails.personWeight);
	$("#slide_donorABORH").text(donorDetails.bloodGrpName);
	$("#slide_targetVol").text(donorDetails.targetProcessingVolume);
	$("#slide_donation").text(donorDetails.donorTypeName);
	$("#recipient_id").text(donorDetails.recipient_id);
	$("#recipient_name").text(donorDetails.recipient_name);
	$("#recipient_dob").text(donorDetails.recipient_dob);
	$("#recipient_aborh").text(donorDetails.recipient_bloodGroup);
	$("#recipient_diagnosis").text(donorDetails.diagonsis);
	$("#idms").text(donorDetails.idmsResult);
	var idmCheck = donorDetails.idmsResult;
	if(idmCheck=="Reactive"){
		$("#idms").addClass('idmReactive');
	}else if(idmCheck=="Pending"){
		$("#idms").addClass('idmPending');
	}
	$("#westnile").text(donorDetails.westNileDesc);
	$("#donorcd34").text(donorDetails.donorCD34);
	$("#cd34_day1").text(donorDetails.productCD34);
	$("#cum_CD34").text(donorDetails.productCUMCD34);
	$("#recipient_weight").text(donorDetails.recipient_weight);
	$("#slide_donorHeight").text(donorDetails.personHeight);
}

function loadTmpDropdown(obj,fieldName,criteria){
	//alert("loadDropdown");
	//alert("val " + $(obj).val() + " / " + name);
	// alert(" trac1 " + $("#tmpSelect").find('option').html() + " /select-> " + $("#tmpSelect").html());
	 url="loadDropdown";
	 response = jsonDataCall(url,"jsonData={fieldName:\""+fieldName+"\",criteria:\""+criteria+"\"}");
	 $("#tmpSelect").find('option').remove().end().append('<option value="">Select</option>');
	// alert(" trac " + $("#tmpSelect").find('option').html());
	 $(response.dropdownList).each(function(i){
		 $("#tmpSelect").find('option').end().append('<option value="'+this+ '">'+ this+'</option>');
	//	 alert(i+"/"+ this +" / "+ $("#tmpSelect").find('option').html());
	 });	
	
	 
}

function loadDropdown(obj,name){
	//alert("loadDropdown");
	//alert("val " + $(obj).val() + " / " + name);
	 url="loadDropdown";
	 var fieldName="";
	 if($.trim($(obj).val()) ==""){
		 return false;
	 }
	 
	 var row = $(obj).parent().closest("tr");
	 var tmpValue="";
	 var criteria=" and ";
	 if(name== "reagents_manufacturer"){
		 criteria+= " rs_type='REAGENTS'  and name ='"+$.trim($(obj).val())+"'";
		 fieldName = "manufacture";
		 $(row).find("#reagents_lot").find('option').remove();
		 $(row).find("#reagents_expiration_date").val("");
		 
	 }else if(name=="reagents_lot"){
		 tmpValue = $(row).find("#reagentsname").val();
		 criteria+= " rs_type='REAGENTS'  and name ='"+$.trim(tmpValue) +"' and manufacture='"+$.trim($(obj).val())+"' ";
		 fieldName = "lotNumber";
		 $(row).find("#reagents_expiration_date").val("");
	 }else if(name=="reagents_expiration_date"){
		 criteria+= " rs_type='REAGENTS'  and name ='"+$.trim($(row).find("#reagentsname").val()) +"' and manufacture='"+$.trim($(row).find("#reagents_manufacturer").val())+"' and lotNumber='"+$.trim($(obj).val())+"' ";
		 fieldName = "to_char(expDate,'Mon DD, YYYY'),reagentsupplies";
		 $(row).find("#reagents_expiration_date").val("");
		 
	 }else if(name== "equipment_manufacturer"){
		 criteria+= " rs_type='EQUIPMENT'  and name ='"+$.trim($(obj).val())+"'";
		 fieldName = "manufacture";
		 $(row).find("#modelnumber").find('option').remove();
		 $(row).find("#mhs").find('option').remove();
		 $(row).find("#maintainanceDueDate").val("");
	 }else if(name== "modelno"){
		 criteria+= " rs_type='EQUIPMENT'  and name ='"+$.trim($(row).find("#equiment_name").val()) +"' and manufacture='"+$.trim($(obj).val())+"'";
		 fieldName = "lotNumber";
		
		 $(row).find("#mhs").find('option').remove();
		 $(row).find("#maintainanceDueDate").val("");
	 }else if(name== "mhs"){
		 criteria+= " rs_type='EQUIPMENT'  and name ='"+$.trim($(row).find("#equiment_name").val()) +"' and manufacture='"+$.trim($(row).find("#equipment_manufacturer").val())+"' and lotNumber ='"+$.trim($(obj).val())+"'";
		 fieldName = "mhs";
		
		 $(row).find("#mhs").find('option').remove();
		 $(row).find("#maintainanceDueDate").val("");
	 }else if(name=="maintainanceDueDate"){
		 criteria+= " rs_type='EQUIPMENT'  and name ='"+$.trim($(row).find("#equiment_name").val()) +"' and manufacture='"+$.trim($(row).find("#equipment_manufacturer").val())+"' and lotNumber ='"+$.trim($(row).find("#modelno").val())+"' and mhs='"+$.trim($(obj).val()) +"'";
		 fieldName = "to_char(expDate,'Mon DD, YYYY'),reagentsupplies";
		 $(row).find("#maintainanceDueDate").val("");
		 
	 }
	 
	 $('.progress-indicator').css( 'display', 'block' );
	 response = jsonDataCall(url,"jsonData={fieldName:\""+fieldName+"\",criteria:\""+criteria+"\"}");
	
	
	 if(name== "reagents_expiration_date"){
 		$(response.dropdownList).each(function(i){
 			//alert("this " + this);
 			//alert(this[0] + " / " + this[1]);
			 if(jQuery.type(this[0]) == "string"){
			 	$(row).find("#"+name).val(this[0]);
			 	$(row).find("#reagentsupplies").val(this[1]);
			 }
			 
		 });
	 }else if (name=="maintainanceDueDate"){
		 $(response.dropdownList).each(function(i){
	 			//alert("this " + this);
	 			//alert(this[0] + " / " + this[1]);
				 if(jQuery.type(this[0]) == "string"){
				 	$(row).find("#"+name).val(this[0]);
				 	$(row).find("#reagentsupplies").val(this[1]);
				 }
				 
			 });
	}else{
		 $(row).find("#"+name).find('option').remove().end()
		    .append('<option value="">Select</option>');
		 $(response.dropdownList).each(function(i){
			 $(row).find("#"+name).find('option').end().append('<option value="'+this+ '">'+ this+'</option>');
			// alert(i+"/ base "+  $(row).find("#"+name).find('option').html());
		 });
		
	 }
	  $.uniform.restore('select');
      $("select").uniform();
      $('.progress-indicator').css( 'display', 'none' );
}
 function editReagentsRows(){
	//  alert("edit Rows");
	if(checkModuleRights(STAFA_PREPARATION,APPMODE_EDIT)){
		 var colObj;
		  $("#reagenTable tbody tr").each(function (row){
			 
			 if($(this).find("#preparationReagents").is(":checked")){
				 $(this).find("td").each(function (col){
					 if(col ==1 ){
						 if ($(this).find("#reagentsname").val()== undefined){
							 tmpValue = $(this).text();
							$(this).html("<select class='select1'  id='reagentsname' name='reagentsname' onchange='javascript:loadDropdown(this,\"reagents_manufacturer\");'>"+$("#reagent_hidden").html() + "</select>");
							$(this).find("#reagentsname").val(tmpValue);
							colObj = $(this).find("#reagentsname");
						 }
					 }else if(col ==2){
						 if ($(this).find("#reagents_quality").val()== undefined){
						 	$(this).html("<input type='text' id='reagents_quality' name='quantity' value='"+$(this).text()+ "' />");
						 }
					 }else if(col ==3){
						 if ($(this).find("#reagents_manufacturer").val()== undefined){
							 tmpValue = $(this).text();
						 	$(this).html("<select class='select1' id='reagents_manufacturer' name='reagents_manufacturer' onchange='javascript:loadDropdown(this,\"reagents_lot\");'></select>");
						 	loadDropdown($(colObj),"reagents_manufacturer");
						 	$(this).find("#reagents_manufacturer").val(tmpValue);
						 	colObj = $(this).find("#reagents_manufacturer");
						 }
					 }else if(col ==4){
						 if ($(this).find("#reagents_lot").val()== undefined){
							 tmpValue = $(this).text();
							 	$(this).html("<select class='select1' id='reagents_lot' name='reagents_lot' onchange='javascript:loadDropdown(this,\"reagents_expiration_date\");' ></select>");
							 	loadDropdown($(colObj),"reagents_lot");
							 	$(this).find("#reagents_lot").val(tmpValue);
							 	colObj = $(this).find("#reagents_lot");
							 }
					}else if(col ==5){
						 if ($(this).find("#reagents_expiration_date").val()== undefined){
							 tmpValue = $(this).text();
							 	$(this).html("<input type='text' id='reagents_expiration_date' name='reagents_expiration_date' class='dateEntry dateclass' value='"+$(this).text()+ "' />");
							 	
							 	$(this).find("#reagents_expiration_date").val(tmpValue);
							 }
					}
				 });
			 }
		  });
		 // $.uniform.restore('select');
	      $("select").uniform();
	}
  } 
 function editEquimentsRows(){
	 if(checkModuleRights(STAFA_PREPARATION,APPMODE_EDIT)){
		//  alert("edit Rows");
		var tmpValue;
		var rowHtml;
		var colObj;
		  $("#equipmentTable tbody tr").each(function (row){
			  rowHtml = $(this);
			 if($(this).find("#preparationEquipmentsId").is(":checked")){
				 $(this).find("td").each(function (col){
					 if(col ==1 ){
						 
						 if ($(this).find("#equiment_name").val()== undefined){
							 tmpValue = $(this).text();
							$(this).html("<select class='select1' id='equiment_name' name='equiment_name' onchange='javascript:loadDropdown(this,\"equipment_manufacturer\");'>"+$("#equipment_hidden").html() + "</select>");
							$(this).find("#equiment_name").val(tmpValue);
							colObj = $(this).find("#equiment_name");
							
							//$(this).find("#equiment_name").trigger("onchange");
							
						 }
					 }else if(col ==2){
						 if ($(this).find("#equipment_manufacturer").val()== undefined){
							 tmpValue = $(this).text();
						 	$(this).html("<select class='select1' id='equipment_manufacturer' name='manufacturer' onchange='javascript:loadDropdown(this,\"modelno\");'></select>");
						 	loadDropdown($(colObj),"equipment_manufacturer");
						 	$(this).find("#equipment_manufacturer").val(tmpValue);
						 	colObj = $(this).find("#equipment_manufacturer");
						 }
					 }else if(col ==3){
						 if ($(this).find("#modelno").val()== undefined){
							 tmpValue = $(this).text();
						 	$(this).html("<select class='select1' id='modelno' name='modelno' onchange='javascript:loadDropdown(this,\"mhs\");'></select>");
						 	loadDropdown($(colObj),"modelno");
						 	$(this).find("#modelno").val(tmpValue);
						 	colObj = $(this).find("#modelno");
						 }
					 }else if(col ==4){
						 if ($(this).find("#mhs").val()== undefined){
							 	tmpValue = $(this).text();
							 	$(this).html("<select class='select1' id='mhs' name='mhs' onchange='javascript:loadDropdown(this,\"maintainanceDueDate\");'></select></select>");
							 	loadDropdown($(colObj),"mhs");
							 	$(this).find("#mhs").val(tmpValue);
							 	colObj = $(this).find("#mhs");
							 }
					}else if(col ==5){
						 if ($(this).find("#maintainanceDueDate").val()== undefined){
							 	tmpValue = $(this).text();
							 	$(this).html("<input type='text' class='dateEntry dateclass calDate' id='maintainanceDueDate' name='maintainanceDueDate'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/>");
							 	$(this).find("#maintainanceDueDate").val(tmpValue);
							 }
					}
				 });
				 
			 }
		  });
		  $.uniform.restore('select');
	      $("select").uniform();
	      addDatepicker();
	 }
}

var reagents_elementcount=0;
var disposable_elementcount=0;
var machine_elementcount=0;
function addnewRegents(activeFlag){
	
	if(checkModuleRights(STAFA_PREPARATION,APPMODE_ADD)){
		var newRow;
		//	 reagents_elementcount +=1;
		
			 newRow="<tr>"+
				"<td><input type='checkbox' id='preparationReagents' name='preparationReagents' value='0'></td>"+
				//"<td><input type='text'  id='reagentsname' name='reagentsname' class='plain' onchange='javascript:loadDropdown(this,\"reagents_manufacturer\");' value=''></td>"+
				"<td><select class='select1'  id='reagentsname' name='reagentsname' onchange='javascript:loadDropdown(this,\"reagents_manufacturer\");'>"+$("#reagent_hidden").html() + "</select></td>"+
				"<td><input type='text' id='reagents_quality' name='quantity' /></td>"+
				"<td><select class='select1' id='reagents_manufacturer' name='reagents_manufacturer' onchange='javascript:loadDropdown(this,\"reagents_lot\");'></select></div></td>"+
				"<td><div><select class='select1' id='reagents_lot' name='reagents_lot' onchange='javascript:loadDropdown(this,\"reagents_expiration_date\");' ></select></div></td>"+
				"<td><input type='text' class='dateEntry dateclass'id='reagents_expiration_date' name='reagents_expiration_date'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/></td>"+
				"</tr>";
			$("#reagenTable tbody").prepend(newRow); 
			
			 $(".plain").each(function (){
				   $(this).removeClass("plain");
				    setObjectAutoComplete($(this),"reagent_hidden")
			   });
			 $("#reagenTable .ColVis_MasterButton").triggerHandler("click");
			  $('.ColVis_Restore:visible').triggerHandler("click");
			  $("div.ColVis_collectionBackground").trigger("click");
		//$("select").uniform();
	    $("select").uniform(); 	
	    $( ".dateclass" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	        changeYear: true, yearRange: "c-50:c+45"});
	   /*  if ( oTable.length > 0 ) {
	        oTable.fnAdjustColumnSizing( true);
	    }  */
	}
}
var equipmentTable=0;
function addnewequipment(activeFlag){
	if(checkModuleRights(STAFA_PREPARATION,APPMODE_ADD)){
		var newRow;
		
		equipmentTable+=1;
		 newRow="<tr>"+
			"<td><input type='checkbox' id='preparationEquipmentsId' name='preparationEquipmentsId' value='0'/></td>"+
			"<td><select class='select1' id='equiment_name'name='equiment_name' onchange='javascript:loadDropdown(this,\"equipment_manufacturer\");'>"+$("#equipment_hidden").html() + "</select></td>"+
			"<td><select class='select1' id='equipment_manufacturer' name='manufacturer' onchange='javascript:loadDropdown(this,\"modelno\");'></select></td>"+
			"<td><select class='select1' id='modelno' name='modelno' onchange='javascript:loadDropdown(this,\"mhs\");'></select></td>"+
			"<td><select class='select1' id='mhs' name='mhs' onchange='javascript:loadDropdown(this,\"maintainanceDueDate\");'></select></td>"+
			"<td><input type='text' class='dateEntry dateclass' id='maintainanceDueDate' name='maintainanceDueDate'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/></td>"+
			"</tr>";
		$("#equipmentTable tbody").prepend(newRow); 
		/*
		$("#equipmentTable .ColVis_MasterButton").triggerHandler("click");
		  $('.ColVis_Restore:visible').triggerHandler("click");
		  $("div.ColVis_collectionBackground").trigger("click");
		 */ 
	//$("select").uniform();
			$("select").uniform(); 	
			$( ".dateclass" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
			 changeYear: true, yearRange: "c-50:c+45"});
			var param ="{module:'APHERESIS',page:'PREPARATION'}";
			markValidation(param);
	}
}  

 function savePreparation(){
	var rowData = "";
	var quantity;
	var preparationregents;
	 var reagentSupplies;
	 $("#reagenTable").find("#reagentsname").each(function (row){
		 rowHtml =  $(this).closest("tr");	 
		 reagentSupplies = "";
		 quantity="";
		
	 		quantity=$(rowHtml).find("#reagents_quality").val();
	 		preparationregents=$(rowHtml).find("#preparationReagents").val()
	 		reagentsSup=$(rowHtml).find("#reagentsupplies").val();
	 		//alert("test " + quantity);
			if(reagentsSup!= undefined && reagentsSup!='undefined'&& reagentsSup!=''&& reagentsSup!=0 ){
		 	  	rowData+= "{preparationReagents:"+preparationregents+",quantity:'"+quantity+"',refReagentsSupplies:'"+reagentsSup+"'},";
	 		}
	 		
	 		
	 		
	 });
	 
	//alert("row data " + rowData)
	 if(rowData.length >0){
            rowData = rowData.substring(0,(rowData.length-1));
        }
	
	 var rowData1= "";
	 var equimentName;
	 var manufacturer;
	 var modelNumber;
	 var maintainanceDate;
	 var mhsNumber;
	 var pkeqData;
	 var reagentsSup;
	 $("#equipmentTable").find("#equiment_name").each(function (r){
		 row =  $(this).closest("tr");
		   equimentName="";
		   reagentsSup="";
	 		pkeqData= $(row).find("#preparationEquipmentsId").val();
			equimentName= $(row).find("#equiment_name").val();
			//maintainanceDate=$(this).find(".dateEntry").val();
			reagentsSup=$(row).find("#reagentsupplies").val();
	 	
	 	if(reagentsSup !="0" && reagentsSup!="" && reagentsSup!=0){
	 		rowData1+= "{preparationEquipmentsId:"+pkeqData+",refReagentsSupplies:'"+reagentsSup+"'},";
 		}
	 		
	 		//alert(equipments + " / " + manufacturer + " / " + rowData1 );
	 });
	 
	 if(rowData1.length >0){
		 rowData1 = rowData1.substring(0,(rowData1.length-1));
     }

		url = "savePreparation";
		var results = JSON
				.stringify(serializeFormObject($('#preparationForm')));
		results = results.substring(1, (results.length - 1));
		var jsonData = "jsonData={reagentData:[" + rowData + "],equimentData:["
				+ rowData1 + "],preparationData:{" + results + "},nextOption:\""+ escape($("#nextOption").val()) + "\",flowNextFlag:'" +$("#flowNextFlag").val()+"'}";

		$('.progress-indicator').css('display', 'block');
	//alert("jsonData " + jsonData)
		response = jsonDataCall(url, jsonData);

		$('.progress-indicator').css('display', 'none');
		alert("Data saved Successfully");
		//?BB stafaAlert('Data saved Successfully','');
		constructEquipmentTable(true,donorId);
		constructReagentsTable(true,donorId);
		
		if($("#flowNextFlag").val()!="true"){
			var preparation = $("#preparation").val();
			if(preparation ==0){
				var donor=$("#donor").val();
				var donorProcedure=$("#donorProcedure").val();
				var url = "loadPreparation.action?donorProcedure="+donorProcedure +"&donor="+donor;
				loadAction(url);
			}
		}
		
		return true;
	}

	function getScanData(event, obj) {
		if (event.keyCode == 13) {

			getDonorData(event, this);
		}
	}

	function getDonorData() {
		alert("getdonorData")

	}
	var contentheight;

	/* function calculateHeight(){	
	 var mainheight = $("#innercenter").height();
	 var titlediv = $("#title").height();
	 contentheight = ((mainheight-titlediv)-12)/16;
	 $("#contentdiv").css("height",contentheight +"em");
	 $("#contentdiv").css("position","relative");
	 } */
	$(document).ready(function() {
		//alert("!");
		donorId=$("#donor").val();
		var donorProcedure=$("#donorProcedure").val();
		donorplannedprocedure=$("#donorProcedure").val();
		var endityId = donorProcedure+","+donorId;
		showTracker(endityId,"donor","apheresis_nurse","false");
		show_innernorth();
		$('#inner_center').scrollTop(0);
		getslide_widgetdetails();
		if(loadPreparationData()){
			constructEquipmentTable(false,donorId);
			constructReagentsTable(false,donorId);
		}
		showNotesCount(donorId,"donor",0);
		//alert("DonorID="+donorId);
//		$("#donor").val(donorId);
		//alert("DonorID="+donorId);
		$("#refDonor").val($("#donor").val());
		$('#tempPrimedTime').timepicker({});
			 $( "#equipmentPrimedDate" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
			        changeYear: true, yearRange: "c-50:c+45"});
			
		var param ="{module:'APHERESIS',page:'PREPARATION'}";
		markValidation(param);
		checkDateData();
		$(".titleText").html("<s:text name='stafa.label.preparation'/>");
		show_slidewidgets("nurse_slidewidget", false);
		show_slidecontrol("slidecontrol");
		/* calculateHeight();
		$(window).resize(calculateHeight); */
		stafa_Time();
		/* $("#equipmentPrimedDate").live("keypress" ,function (evt) {
			alert($("#equipmentPrimedDate").val());
			
		}); */
	
	});
	 function setEquimentPrimeTime(){
         $('#equipmentPrimedTime').val($('#equipmentPrimedDate').val() +" "+ $('#tempPrimedTime').val());
     }
</script>

<div  id="slidecontrol" style="display:none;">
	<ul class="ulstyle">
	<span>
			<li class="listyle print_label"><img src = "images/icons/Bar_Code_32.png" class="barcodePrint" onclick="loadBarcodePage();" style="width:30px;height:30px;"/></li>
			<li class="listyle print_label" ><b><s:text name="stafa.label.label"/></b> </li>
	</span>
	<span id="notesSpan">
		<li class="listyle notes_icon"  style="margin-left:18%"><img src = "images/icons/notes.png"  onclick="notes(donorId,'donor',this,false);" style="width:30px;height:30px;"/><sup style="margin-left:2%"><span class='subspan'></span></sup></li>
		<li class="listyle notes_icon"><b><s:text name="stafa.label.notes"/></b> </li>
	</span>
	<span>
		<li class="listyle print_label"><img src = "images/icons/labresults.png"  style="width:30px;height:30px;cursor:pointer;" onclick='addNewPopup("Complete Lab Result","labResults","loadLabResult","700","1050");'/></li>
		<li class="listyle print_label" ><b>Lab Result</b> </li>
	</span>
	<span>
		<li class="listyle medication_icon"><img src = "images/icons/capsule1.png" onclick="medicationpopup(donormrn,donorId,donorplannedprocedure);" style="width:30px;height:30px;"/></li>
		<li class="listyle medication_icon"><b><s:text name="stafa.label.medication"/></b></li>
	</span>
	<span>
			<li class="listyle transfer_icon"><img src = "images/icons/transfer.png" onclick="taskAction(donorplannedprocedure,'transfer')"  style="width:30px;height:30px;cursor:pointer;"/></li>
			<li class="listyle transfer_icon" id="transfer_icon"><b><s:text name="stafa.label.transfer"/></b></li>
			
		</span>
	<span>
			<li class="listyle terminate_icon"><img src = "images/icons/terminated.png"  onclick="taskAction(donorplannedprocedure,'terminate')" style="width:30px;height:30px;cursor:pointer;"/></li>
			<li class="listyle terminate_icon"><b><s:text name="stafa.label.terminate"/></b></li>
		</span>
		<%-- <li class="listyle complete_icon"><b><s:text name="stafa.label.complete"/></b></li>
		<li class="listyle complete_icon"><img src = "images/icons/task_completed_icon.PNG"  style="width:30px;height:30px;"/></li> --%>
	</ul>
</div>
<div id="nurse_slidewidget" style="display:none;">
				
<!-- Patient Info start-->
			<div class="column">
				<div  class="portlet">
					<div class="portlet-header "><s:text name="stafa.label.donorinformation"/></div>
						<div class="portlet-content">
							<table width="100%" border="0">
							  <tr>
								<td width="50%"><s:text name="stafa.label.donorid"/></td>
								<td width="50%" id="slide_donorId" name="slide_donorId"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.name"/></td>
								<td id="slide_donorName" name="slide_donorName"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.dob"/></td>
								<td id="slide_donorDOB" name="slide_donorDOB"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.donoraborh"/></td>
								<td id="slide_donorABORH" name="slide_donorABORH"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.procedure"/></td>
								<td id="slide_procedure" name="slide_procedure"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.donation"/></td>
								<td id="slide_donation" name="slide_donation"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.weights"/></td>
								<td id="slide_donorWeight" name="slide_donorWeight"></td>
							  </tr>
							  <tr>
								<td>Height</td>
								<td id="slide_donorHeight" name="slide_donorHeight"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.targetvolume"/></td>
								<td id="slide_targetVol" name="slide_targetVol"></td>
							  </tr>
							</table>
						</div>
				</div>
			</div>
				
<!-- Patient Info-->

<!-- Lab Results start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header "><s:text name="stafa.label.donorlab"/></div>
						<div class="portlet-content">
							<table width="100%" border="0">
								<tr>
									<td width="50%"><s:text name="stafa.label.idms"/></td>
									<td width="50%" id="idms"></td>
								</tr>
								<tr>
									<td width="50%"><s:text name="stafa.label.westnile"/></td>
									<td width="50%" id="westnile"> </td>
								</tr>
								<tr>
									<td width="50%"><span class="cd34"><s:text name="stafa.label.donorcd34"/></span>
									<span class="cd3"><s:text name="stafa.label.donorcd3"/></span></td>
									<td width="50%" id="donorcd34"> </td>
								</tr>
							</table>
						</div>
				</div>
				</div>
				
<!-- Lab Results-->
			<div class="column">
					<div class="portlet">
						<div class="portlet-header "><s:text name="stafa.label.productlab"/></div>
							<div class="portlet-content">
							<table width="100%" border="0">
								<tr>
									<td width="50%">
									<span class="cd34"><s:text name="stafa.label.cd34_day1"/></span>
									<span class="cd3"><s:text name="stafa.label.cd3_day1"/></span>
									</td>
									<td width="50%" id="cd34_day1"></td>
								</tr>
								<tr>
									<td width="50%">
									<span class="cd34"><s:text name="stafa.label.cum_CD34"/></span>
									<span class="cd3"><s:text name="stafa.label.cum_CD3"/></span>
									</td>
									<td width="50%" id="cum_CD34"></td>
								</tr>
							</table>
							</div>
					</div>
				</div>
<!-- Recipient Info start-->
				<div class="column">
					<div class="portlet">
						<div class="portlet-header "><s:text name="stafa.label.recipientinformation"/></div>
							<div class="portlet-content">
							<table width="100%" border="0">
							<tr>
								<td width="50%"><s:text name="stafa.label.recipientmrn"/></td>
								<td width="50%" id="recipient_id"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.name"/></td>
								<td id="recipient_name"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.dob"/></td>
								<td id="recipient_dob"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.aborh"/></td>
								<td id="recipient_aborh"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.diagnosis"/></td>
								<td id="recipient_diagnosis"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.weights"/></td>
								<td id="recipient_weight"></td>
							  </tr>
							  </table>
							</div>
							 
					</div>
				</div>
				
<!--Recipient Info end-->
				</div>


<!--right window start -->	
	<%-- <div id="title" class="titlediv">
			<b><s:text name="stafa.label.preparation"/></b>
	</div> --%>
   	
<div class="hidden">
	<select name="tmpSelect" id="tmpSelect" ><option value="">Select</option></select>	
  <s:hidden id="donor" name="donor" value="%{donor}"/>

  
			<select  name="equipment_hidden" id="equipment_hidden" >
				<option value="">Select</option>
				<s:iterator value="equipmentList" var="rowVal" status="row">
					<option value='<s:property value="#rowVal"/>'> <s:property value="#rowVal"/> </option>
				</s:iterator> 		
			</select>
			
			<select name="reagent_hidden" id="reagent_hidden" >
				<option value="">Select</option>
			<s:iterator value="reagentsnameList" var="rowVal" status="row">
				<option value='<s:property value="#rowVal"/>'> <s:property value="#rowVal"/> </option>
			</s:iterator> 		
			</select>
		
</div>	
<div class="cleaner"></div>

<div class="column">
	 <div  class="portlet">
	<div class="portlet-header notes"><s:text name="stafa.label.equipment"/></div>
		<div class="portlet-content" >
			
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewequipment(false);"/>&nbsp;&nbsp;<label  class="cursor" onclick="addnewequipment(false);"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteEquimentData()"/>&nbsp;&nbsp;<label class="cursor"  onclick="deleteEquimentData()"><b>Delete</b></label>&nbsp;</div></td>
			    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" onclick="editEquimentsRows()"/>&nbsp;&nbsp;<label  class="cursor" onclick="editEquimentsRows()"><b>Edit</b></label>&nbsp;</div></td>
				</tr>
			</table>
			<div style="overflow-y:auto;overflow-y:hidden;">
			<table cellpadding="0" cellspacing="0" border="0" id="equipmentTable" class="display">
			<thead>
				<tr>
					<th width="4%" id="equipmentTableColumn"></th>
					<th colspan='1'><s:text name="stafa.label.equipment"/></th>
					<th colspan='1'><s:text name="stafa.label.manufacturer"/></th>
					<th colspan='1'><s:text name="stafa.label.model"/></th>
					<th colspan='1'><s:text name="stafa.label.mhs"/></th>
					<th colspan='1'><s:text name="stafa.label.maintainance_Duedate"/></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
		
<div class="cleaner"></div>

<form action="" id="preparationForm" name="preparationForm" onsubmit="return avoidFormSubmitting();">
<s:hidden id="preparation" name="preparation" value="0"/>
<s:hidden id="refDonor" name="refDonor"  value="%{donor}"/>
<s:hidden id="donorProcedure" name="donorProcedure" value="%{donorProcedure}"/>
<div class="column">		
		<div  class="portlet">
		
			<div class="portlet-header notes"><s:text name="stafa.label.preparation"/></div>
				<div class="portlet-content">
				
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper"><s:text name="stafa.label.equipment_pre-cleaned"/></td>
					<td class="tablewapper1"><input type="hidden" id="preCleaned" name="preCleaned" value="0"/><input type="checkbox" id="tmppreCleaned" name="tmppreCleaned"  onclick="setvalues('preCleaned')"/>Yes</td>
					<td class="tablewapper"><s:text name="stafa.label.alerm_checked"/></td>
					<td class="tablewapper1"><input type="hidden" id="alarmChecked" name="alarmChecked" value="0"/> <input type="checkbox" id="tmpalarmChecked" name="tmpalarmChecked"  onclick="setvalues('alarmChecked')"value="0"/>Yes</td>
				</tr>
				<tr>
					<td class="tablewapper"><s:text name="stafa.label.equipment_primedate"/></td>
					<td class="tablewapper1"><input type="text" id="equipmentPrimedDate" name="equipmentPrimedDate" Class="dateEntry dateclass" onkeyup="checkDateData()" onchange="checkDateData()"></td>
					<td class="tablewapper"><s:text name="stafa.label.equipment_primetime"/></td>
					<td class="tablewapper1"><s:hidden  id="equipmentPrimedTime" name="equipmentPrimedTime" Class=""/><s:textfield id="tempPrimedTime" name="tempPrimedTime" Class="" onchange="setEquimentPrimeTime();"/></td>				
				</tr>
				</table>
				</div>
			</div>
			
		</div>
</form>

<div class="cleaner"></div>

<div class="column">
		
	<div  class="portlet">
	<div class="portlet-header notes"><s:text name="stafa.label.regandandsupplies"/></div>
		<div class="portlet-content">
		
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewRegents(false)"/>&nbsp;&nbsp;<label  class="cursor" onclick="addnewRegents(false)"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteReagentsData()"/>&nbsp;&nbsp;<label class="cursor"  onclick="deleteReagentsData()"><b>Delete</b></label>&nbsp;</div></td>
			    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;"onclick="editReagentsRows()"/>&nbsp;&nbsp;<label  class="cursor" onclick="editReagentsRows()"><b>Edit</b></label>&nbsp;</div></td>
				</tr>
			</table>
			<div style="overflow-y:auto;overflow-y:hidden;">
			<table cellpadding="0" cellspacing="0" border="0" id="reagenTable" width="100%"class="display">
				<thead>
					<tr>
						<th width="4%" id="reagenColumn"></th>
						<th colspan="1"><s:text name="stafa.label.regandandsupplies"/></th>
						<th colspan="1"><s:text name="stafa.label.quantity"/></th>
						<th colspan="1"><s:text name="stafa.label.manufacturer"/></th>
						<th colspan="1"><s:text name="stafa.label.lot"/></th>
						<th colspan="1"><s:text name="stafa.label.expirationdate"/></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<%-- <div class="floatright_right">
	<jsp:include page="sidecontrols.jsp"></jsp:include>
 </div> --%>
 
<div class="cleaner"></div>
		
<div align="left" style="float:right;">
	<form onsubmit="return avoidFormSubmitting()">
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
			<tr>
				<td><span class='nextContainer'>Next:
				<select name="nextOption" id="nextOption" ></select>
				<input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
				<input type="button" value="Next" onclick="verifyeSign('eSignNext','next')"/></span></td>
			</tr>
		</table>
	</form>
	<!--  <div id="barimageTables" style="float:right;display:none; width:44%;height:40%;">
			<!-- From Template Start -->
			<!-- <div class="barcode_wrapper" id="barcodeTem">
			<div><input type="button" class="cursor" id='printBut' value="Print" onclick="printBarcode();"/></div>
			</div>
			 -->
			<!-- From Template Ends -->
</div> 
<div  id ="mainbarcodediv" style="background-color:white; display:none; height:655px;overflow:auto;margin-top:1%;">  <!-- added by mohan j -->
<div id="maindiv"><div id="barimageTables" style="float:right;display:none; width:44%;height:40%;">
			<!-- From Template Start -->
		<div id="barcodeMainDiv" >
			
			
		</div>
			<!-- From Template Ends -->
</div></div>
	
</div>


<script>

$(function() {
  
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
   



    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

    $('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});   
});
</script>
