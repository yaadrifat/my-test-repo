<%@ taglib prefix="s" uri="/struts-tags"%>
<script>
$(document).ready(function() {
//$('#table111').dataTable({"aaSorting": [[ 1, "desc" ]]});
	oTable = $('#storageRetrieveTable').dataTable({
		"bJQueryUI": true
		//"sPaginationType": "full_numbers"
	});
	oTable.thDatasorting('storageRetrieveTable')
	$("select").uniform();
});
</script>
<div class="cleaner"></div>
	
<div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value="storageretrieve"/><s:hidden id="widgetName" value="proretrieve"/><s:hidden id="pkNotes" value=""/>&nbsp;<s:text name="stafa.label.proretrieve" /></div>
		<div class="portlet-content">
		<form action="#" id="login">
				<div id="table_inner_wrapper">
					<div id="left_table_wrapper">
						<div id="table_container_input">
							<div class="table_row_input1">
								<div class="table_cell_input1"><s:text name="stafa.label.currentlocation" /></div>
								<div class="table_cell_input2"><div class="mainselection"><s:select   headerKey="" headerValue="Select" list="currentLocationList" listKey="pkCodelst" listValue="description" name="currentLocation" id="currentLocation"/></div></div>
							</div>					
						</div>
					</div>
					

					
					<div id="right_table_wrapper">
						<div id="table_container_input">
							<div class="table_row_input1">
								<div class="table_cell_input1"><s:text name="stafa.label.intendedlocation" /></div>
								<div class="table_cell_input2"><div class="mainselection"><s:select headerKey="" headerValue="Select"  headerKey="" headerValue="Select" list="intendedLocationList" listKey="pkCodelst" listValue="description" name="intendedLocation" id="intendedLocation"/></div></div>												
							</div>
						</div>
					</div>
				</div>
				</form>			

			<table cellpadding="0" cellspacing="0" border="0" id="storageRetrieveTable" class="display">
					<thead>
						<tr>
							<th colspan="1">Recipient ID</th>
							<th colspan="1">Recipient Name</th>
							<th colspan="1">Product ID</th>				
							<th colspan="1">Vessel</th>
							<th colspan="1">Slot Location</th>
							<th colspan="1">Reason</th>
							<th colspan="1">Verification</th>
						</tr>
						<tr>
							<th></th>
							<th></th>
							<th></th>				
							<th></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>MDA7242464</td>
							<td>Jeremy Burt</td>
							<td>BMT123-3-1</td>
							<td>100 ml Bag</td>
							<td>T20-2-a</td>
							<td>Recipient deceased</td>
							<td align="center"><input class="scanboxSearch" type="text" value="" style="width:150px" name="search" placeholder="Scan/Enter Id"/></td>
						</tr>
						<tr>
							<td>MDA0234694</td>
							<td>Milly Day</td>
							<td>BMT123-3-2</td>
							<td>100 ml Bag</td>
							<td>T20-2-b</td>
							<td>Recipient deceased</td>
							<td align="center"><input class="scanboxSearch" type="text" style="width:150px"  value="" name="search" placeholder="Scan/Enter Id"/></td>
						</tr>
					</tbody>
			</table>
	</div>
	</div>	
</div>		

<div class="cleaner"></div>

<div class="column">		
		<div class="portlet">
		<div class="portlet-header"><s:text name="stafa.label.disposalinformation" /></div>
		<div class="portlet-content">&nbsp;	
			</div>
		</div>
	</div>

		<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
				<input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>
				<input type="button" value="Save"/>
				</form>
		</div>
		<div class="cleaner"></div>
<script>
$(function() {	
	
	$( ".column" ).sortable({
		connectWith: ".column"
	});


	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
	//$('.sortable').columnManager({listTargetID:'sortable', onClass: 'advon', offClass: 'advoff', hideInList: [1,3],
     //   saveState: true, colsHidden: [2,4]});
	//$( ".column" ).disableSelection();
});

</script>