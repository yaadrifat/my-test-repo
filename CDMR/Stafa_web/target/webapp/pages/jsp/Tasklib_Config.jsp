<%@ include file="common/includes.jsp"%>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<style type="text/css">
	.maintenance_slidewidget{
	 border: 1px solid #d1d1d1;
     -webkit-transition: all 0.6s ease 0s;
    -moz-transition: all 0.6s ease 0s;
    -o-transition: all 0.6s ease 0s;
    transition: all 0.6s ease 0s;
    float: right;
    width: 50%;
     -webkit-border-radius: 12px;
    -moz-border-radius: 12px;
    border-radius: 12px;
    text-shadow: 0 2px 3px rgba(0, 0, 0, 0.1);
    -webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset;
    -moz-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset;
	margin-right: 2%;
  padding: 0px 22px 1px 10px;
 
	
}
</style>
<script>
var oTable1;
$(document).ready(function(){
	maintColumnfilter('tasklib');
	var oTable1=$("#taskLibTable").dataTable({"bJQueryUI": true,"bPaginate":true, "bRetrieve": true, "bDestroy": true});
	var oTable2=$("#commontable").dataTable({"bJQueryUI": true,"bPaginate":true,"bRetrieve": true, "bDestroy": true});
	var oTable3=$("#recurringtask").dataTable({"bJQueryUI": true,"bPaginate":true});
	$("select").uniform(); 
	
		setcolumnManager('taskLibTable'); 
		maintColumnfilter('tasklib');
  });
   /*
   DataTable data
  */
  var mainid=null;
  function replaceslide(id,caption)
  {
  if($('#tasklibrary').css('display') == 'block'){
 		 $('#'+id).css("display","block");
 		$(".spanid").html(caption);
 		$('#tasklibrary').css("display","none");
	} else if($('#dailyassign').css('display') == 'block'){
		$('#'+id).css("display","block");
 		$(".spanid").html(caption);
 		$('#dailyassign').css("display","none");
	}else if($('#recurringtaskdiv').css('display') == 'block'){
		$('#'+id).css("display","block");
 		$(".spanid").html(caption);
 		$('#recurringtaskdiv').css("display","none");
	}
  if(mainid!=null){
	  $('#'+mainid).css("display","none");
	  $(".spanid").html(caption);
	  $('#'+id).css("display","block");
  }
	mainid=id;
  } 
  $("#taskLibTable tbody tr ").mouseenter(function(){
	  $(this).attr("style","background:#33ffff");	
	 // $(this).css("cursor","default");
  }).mouseleave(function() {
	  $(this).removeAttr("style");
	  $(this).css("cursor","default");
  });
  var code;
   
  $("#taskLibTable tbody tr ").click(function(){  
	  $(this).css("cursor","pointer");
	$(this).find('td:eq(1)').click(function(){	
		var caption= $(this).text();
		code= $(this).next().text();
		if(code=='DailyAssign'){
		 replacediv(caption,'dailyassign'); 
		}
		else{
			 replacediv(caption,'recurringtaskdiv');	
		}
	 });	 
	
  });

  function replacediv(caption,id){ 
	  if($('#tasklibrary').css('display') == 'block'){
		 $('#'+id).css("display","block");
	 		$(".spanid").html(caption);
	 		$('#tasklibrary').css("display","none");
		} else if($('#dailyassign').css('display') == 'block'){
			$('#'+id).css("display","block");
	 		$(".spanid").html(caption);
	 		$('#dailyassign').css("display","none");
		}else if($('#recurringtaskdiv').css('display') == 'block'){
			$('#'+id).css("display","block");
	 		$(".spanid").html(caption);
	 		$('#recurringtaskdiv').css("display","none");
		}

  }
   function addrows(id){
		
			//$('#'+id+' thead tr th').unbind('click');
			$('#'+id).prepend("<tr>"+"<td><input type='checkbox' class='deleterows'/></td>"+
			 "<td><input type=text  id=''/></td>"+
			 "<td><input type=text  id=''/></td>"+
			 "<td><input type=text  id=''/></td>"+
			 "<td><input type=text  id=''/></td>"+"</tr>"); 
		
 
	
  }   
 
   
  /*  function displayResult(id)
  {
  var tables=document.getElementById(id);
  var rowsLen = tables.rows.length;
  //alert(rowsLen);
  //rowsLen=rowsLen-1;
  //alert(rowsLen);displayResult
  var row=tables.insertRow(rowsLen);
$('#'+id).dataTable().fnAddData(["<input type='checkbox'  class='deleterows' id=''>","","","","<input type='radio' id='yes' name='activate'>Yes <input type='radio' id='no' name='activate'>no"]);
setAttributeFlag(id);
 // $(cell0).attr('style','width:20px');
  $(cell1).attr('style','width:100px');
  $(cell2).attr('style','width:100px');
  $(cell3).attr('style','width:100px');
  $(cell4).attr('style','width:100px');
  $(cell5).attr('style','width:100px');
   $(cell2).click(function(event) {
      if(!(this.getAttribute('textFlag')=="true")){
      insertTextBox(this);
      cellTextFlag=true;
      }
  });
  $(cell3).click(function(event) {
      if(!(this.getAttribute('textFlag')=="true")){
      insertTextBox(this);
      cellTextFlag=true;
      }
  });
  $(cell4).click(function(event) {
          if(!(this.getAttribute('textFlag')=="true")){
          insertTextBox(this);
          cellTextFlag=true;
          }
  });
   cell0.innerHTML="&nbsp;";
  cell1.innerHTML="<input type='checkbox' class='deleterows' id=''>";
  cell2.innerHTML="&nbsp;";
  cell3.innerHTML="&nbsp;";
  cell4.innerHTML="&nbsp;";
  cell5.innerHTML="<input type='radio' id='yes' name='activate'>Yes <input type='radio' id='no' name='activate'>no";
   cell0.setAttribute('class','hidden'); 
  cell1.setAttribute('textFlag',false);
  cell2.setAttribute('textFlag',false);
  cell3.setAttribute('textFlag',false);
  cell4.setAttribute('textFlag',false);
  cell5.setAttribute('textFlag',false);
   }
  // additives data
  var oldCellObj;
  var textCellFlag=false;
  function insertTextBox(cellObj){
      //alert(cellObj.getAttribute('textFlag'))

      if(oldCellObj){
          ///alert("11-->"+oldCellObj.getAttribute('textFlag'));
          }
      if(cellObj.getAttribute('textFlag')=="false" && oldCellObj && (oldCellObj.getAttribute('textFlag')=="true")){
          //alert("-->"+cellObj.getAttribute('textFlag'))
          var tempValue = $("#tempId").val();
          //alert("-->tempValue"+tempValue)
          cellObj.innerHTML="<input type='text' id='tempId' style='width:99%'  onblur='setCellValue(this)' value='"+cellObj.innerHTML+"'/>";
          cellObj.setAttribute('textFlag',true);
          oldCellObj.innerHTML=tempValue;
          oldCellObj.setAttribute('textFlag',false);
          oldCellObj=cellObj;
          textCellFlag=true;
          /*$('#tempId').blur(function() {
              setCellValue(this);
              });
          $('#tempId').focus();
          return;
      }
      if(cellObj.getAttribute('textFlag')=="false"){
          //alert("=====>"+cellObj.getAttribute('textFlag'))

          cellObj.innerHTML="<input type='text' id='tempId'style='width:99%'  onblur='setCellValue(this)' value='"+cellObj.innerHTML+"'/>";
          cellObj.setAttribute('textFlag',true);
          /*$('#tempId').blur(function() {
                setCellValue(this);
              });
          oldCellObj=cellObj;
          textCellFlag=true;
          $('#tempId').focus();
      }
      
  }
  
  
  function setAttributeFlag(id){
	   var productsTable = document.getElementById(id);
	     	for(i=1;i<productsTable.rows.length;i++){ 
	     		//productsTable.rows[i].cells[3].setAttribute();
	     		$(productsTable.rows[i].cells[1]).attr("textflag",false);
	     		$(productsTable.rows[i].cells[1]).attr("onclick","insertTextBox(this)");
	     		$(productsTable.rows[i].cells[2]).attr("textflag",false);
	     		$(productsTable.rows[i].cells[2]).attr("onclick","insertTextBox(this)");
	     		$(productsTable.rows[i].cells[3]).attr("textflag",false);
	     		$(productsTable.rows[i].cells[3]).attr("onclick","insertTextBox(this)");
	     			     	}
}
 /*  function setCellValue(This){
      //alert(This.value);
      //alert(oldCellObj.innerHTML);

      oldCellObj.innerHTML=This.value;
      oldCellObj.setAttribute('textFlag',false);
      oldCellObj = null;
  }  */
jQuery.fn.blindToggle = function(speed, easing, callback) {
    var h = this.width() + parseInt(this.css('paddingLeft')) 
+  parseInt(this.css('paddingRight'));
    if(parseInt(this.css('marginLeft'))<0){
	   	   $('#forfloat_right').animate( { width: "82%" }, { queue: false, duration: 200 });	
        $('#blind').html("<<");
        return this.animate({marginLeft:0},speed, easing, callback);
        }
    else{
		  $( "#forfloat_right" ).animate( { width: "95%" }, { queue: false, duration: 250 });

        $('#blind').html(">>");
        return this.animate({marginLeft:-h},speed, easing, callback);
		   
          }
		
  };
 
  
  function checkall() { 
		var isChecked = $('#deleteallProtocol').attr('checked');
	 	if(isChecked)
	 		{
		 $('.deleterows').attr('checked', true);	
	 		}
	 	else
	 		{
	 		$('.deleterows').attr('checked', false);	
	 		}
	 }
 // var oTable13=$("#protocolLibTable").dataTable({"bJQueryUI": true,"bPaginate":true});

  function deletechildrows(id) {var  oTable1= $('#'+id).dataTable();
	  $("#deleteallProtocol").attr('checked', false);
	  try {
		  
             var isChecked = $('#'+id).find('tr').length;
             var isCheckedall = $('#deleteallProtocol').attr('checked');
             if(isChecked>=1 && $('.deleterows').is(':checked')){
             if(isCheckedall){
                 var yes=confirm('Do you want delete all products ?');   
             }
             else{var yes=confirm('Do you want delete selected products?');
             }
             var rowCount = $('#'+id).find('tr').length;
          if(yes){
               $('#'+id+' tbody tr').each(function(i,v){
                      var cbox=$(v).children("td:eq(0)").find(':checkbox').is(":checked");
                      if(cbox){
                    	  oTable1.fnDeleteRow(this);
                      
                      }
              });
           $('.deleterows').attr('checked', false);

          }
      }
           //rowCount = table.rows.length;
     
      }catch(e) {
          alert(e);
      }
      $('#deleteallProtocol').attr('checked', false);
      

   }  
  
</script>

<div class="cleaner"></div>
<div id="floatMenu">
<div id="forfloat_wind">
	<div id="box-outer">
		
		<a href="#" style="text-decoration:none" id="blind">&lt;&lt; </a>
			<div id="box">
				<div id="leftnav" >
					<div  class="portlet">
					<div class="portlet-header"><s:hidden id="moduleName" value="overnightStorage"/><s:hidden id="widgetName" value="recipient"/><s:hidden id="pkNotes" value=""/>Task<input type="text" id="Columnfilter" class="inputboxSearch maintenance_slidewidget" onclick="maintColumnfilter('tasklib');"></div>
						<div class="portlet-content">
						<table width="100%" border="0" id="tasklib">
						  <tr>
							<td width="90%" style="padding:2%"><a class="cursor" onclick="javascript:replaceslide('dailyassign','Daily Assignments');">Daily Assignments</a></td>
							<td width="10%"><div><img src = "images/icons/folder.png" cursor:pointer;" onclick="createrow()"/></td>
						  </tr>
						  <tr>
							<td width="90%" style="padding:2%"><a class="cursor" onclick="javascript:replaceslide('recurringtaskdiv','Recurrings Tasks');">Recurrings Tasks </a></td>
							<td width="10%"><div><img src = "images/icons/folder.png" cursor:pointer;" onclick="createrow()"/></td>
						  </tr>
				
</table>

					</div>
					</div>
			</div>
				</div>
			</div>
		</div>
</div>

<!--right window start -->	
<div id="forfloat_right">
<div class="cleaner"></div>
<div class="" id="tasklibrary">		
		<div  class="portlet">
			<div class="portlet-header notes info"> Task Library</div>			
		<form>
	
			<div class="portlet-content" id="childDiv">
			<table><tr><td>&nbsp;</td></tr>
			<tr>
			<td style="width:20%"><img src = "images/icons/addnew.jpg" cursor:pointer;" onclick="addrows('taskLibTable')"/>&nbsp;&nbsp;<b>Add</b>&nbsp;</td>
			<td style="width:20%"><img src = "images/icons/no.png" style="width:16;height:16;cursor:pointer;" id ="" onclick="deletechildrows('commontable');"/>&nbsp;&nbsp;<b>Delete</b>&nbsp;</td>
		    <td style="width:20%"><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" id ="" onclick="editrows('');"/>&nbsp;&nbsp;<b>Edit</b>&nbsp;</td>
			
			</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="" id="taskLibTable" class="createchilddisplay" width="100%">
	<thead>
				<tr>
				
					<th style="width:8%"><input type="checkbox"  id="deleteallProtocol" onclick='checkall()'>Check All</th>
					<th> Name </th>
					<th> Code</th>
					<th> Description</th>
					<th>Activated</th>
					</tr>
			</thead>
			<tbody>
				 <tr>
				 	<td width=%" ><input type="checkbox"  id="deleteallProtocol"></td>
				 	<td width="" >Daily Assignments</td>
					<td width="" >DailyAssign</td>
					<td width="">&nbsp;</td>
					<td width="">&nbsp;</td>
				</tr>
				<tr>
					<td width=""><input type="checkbox"  id="deleteallProtocol" ></td>
					<td width="">Recurring Tasks </td>
					<td width="">RecurTask</td>
					<td width="">&nbsp;</td>
					<td width="">&nbsp;</td>
				 </tr> 
				
			</tbody>	
			</table>
				<div id="bor" style="float:right;">
		<form>
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="" value="Save"/></td>
				</tr>
				</table>
				</form>
		</div>
	</div>
</form>
		</div>                    	 	
</div>
<div class="cleaner"></div><!--  dailyassign div Starts here -->
<div class="" id="dailyassign" style="display:none">		
		<div  class="portlet">
			<div class="portlet-header notes info"><span class="spanid"> </span></div>			
		<form>
	
			<div class="portlet-content" id="childDiv">
			<table><tr><td>&nbsp;</td></tr>
			<tr>
			<td style="width:20%"><img src = "images/icons/addnew.jpg" cursor:pointer;" onclick="addrows('commontable')"/>&nbsp;&nbsp;<b>Add</b>&nbsp;</td>
			<td style="width:20%"><img src = "images/icons/no.png" style="width:16;height:16;cursor:pointer;" id ="" onclick="deletechildrows('commontable');"/>&nbsp;&nbsp;<b>Delete</b>&nbsp;</td>
		    <td style="width:20%"><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" id ="" onclick="editrows('');"/>&nbsp;&nbsp;<b>Edit</b>&nbsp;</td>
			</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="1" id="commontable" class="createchilddisplay" width="100%">
	<thead>
				<tr>
			
					<th style="width:8%"><input type="checkbox"  id="deleteallProtocol" onclick='checkall()'>Check All</th>
					<th> Name </th>
					<th> Code</th>
					<th> Description</th>
					<th>Activated</th>
					</tr>
			</thead>
			<tbody>
		
			</tbody>	
			</table>
				<div id="bor" style="float:right;">

				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="" value="Save"/></td>
				</tr>
				</table>
				
		</div>
	</div>
	</form>
	</div>                    	 
		
	
</div>
<!--  common div  ends here -->


		<!--  Recurring task div Starts here -->
	<div class=""  id="recurringtaskdiv"style="display:none">		
		<div  class="portlet">
			<div class="portlet-header notes info"><span id="spanid"><span class="spanid"> </span></span></div>			
		<form>
	
			<div class="portlet-content" id="childDiv">
			<table><tr><td>&nbsp;</td></tr>
			<tr>
			<td style="width:20%"><img src = "images/icons/addnew.jpg" cursor:pointer;" onclick="addrows('recurringtask')"/>&nbsp;&nbsp;<b>Add</b>&nbsp;</td>
			<td style="width:20%"><img src = "images/icons/no.png" style="width:16;height:16;cursor:pointer;" id ="" onclick="deletechildrows('recurringtask');"/>&nbsp;&nbsp;<b>Delete</b>&nbsp;</td>
		    <td style="width:20%"><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" id ="" onclick="editrows('');"/>&nbsp;&nbsp;<b>Edit</b>&nbsp;</td>
			
			</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="1" id="recurringtask" class="createchilddisplay" width="100%">
	<thead>
				<tr>
				
					<th style="width:8%"><input type="checkbox"  id="" onclick='checkall()'>Check All</th>
					<th> Name </th>
					<th> Code</th>
					<th> Description</th>
					<th>Activated</th>
					</tr>
			</thead>
			<tbody>
		
			</tbody>	
			</table>
				<div id="bor" style="float:right;">
		<form>
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="" value="Save"/></td>
				</tr>
				</table>
				</form>
		</div>
	</div>
	</form>
	</div>                    	 
		
	
	</div>	 
	<!--  Recurring div Ends here -->	
		
		
		
		
		
		
		
	</div>
