<%@ include file="common/includes.jsp" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script>
$(function() {
    $('.jclock').jclock();
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");	
    $( ".portlet-header .ui-icon" ).click(function() {
       $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });
  });
$(document).ready(function(){
    //$("select").uniform(); 
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"});
      $(window).resize();
	show_eastslidewidget("slidedivs");
		 			var  oTable3=$("#consultEvalChecklistDetails").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null
	 										             ],
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#consultEvalChecklistColumn").html($('.ColVis:eq(0)'));
												}
									});
		 			var  oTable=$("#consultEvalEducationDetails").dataTable({
						"sPaginationType": "full_numbers",
						"sDom": 'C<"clear">Rlfrtip',
						"aoColumns": [ { "bSortable":false},
							               null,
							               null,
							               null,
							               null
							               ],
							"oColVis": {
								"aiExclude": [ 0 ],
								"buttonText": "&nbsp;",
								"bRestore": true,
								"sAlign": "left"
							},
							"fnInitComplete": function () {
						        $("#consultEvalEducationColumn").html($('.ColVis:eq(1)'));
							}
				});
					    
		 		   
});
$("select").uniform(); 
</script>
 <div class="column">       
    <div  class="portlet">
    <div class="portlet-header">Basic Clinical Info</div><br>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Recipient Weight(Kg)</td>
					<td>
						<div  class="portlet11">
							<div class="portlet-content">
							<table>
							<tr>
								<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
								<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
						    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
							</tr>
						</table>
						<table cellpadding="0" cellspacing="0" border="1" id="referralDetails" class="display">
						<thead>
							<tr>
								<th colspan='1'>Select</th>
								<th colspan='1'>Weight</th>
								<th colspan='1'>unit</th>
								<th colspan='1'>Date</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><input type='checkbox'  id="recipientInsuranceVerified" onclick=""></td>
								<td></td>
								<td></td>
								<td>	</td>
								</tr>
						</tbody>
						</table>
			</div>
		</div>
					</td>
                </tr>
                </table>
                 <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Recipient Height(cm)</td>
					<td class="tablewapper1"><input type="text" id="recipientHeight" name=""/></td>
                    <td class="tablewapper">ABO/Rh</td>
					<td class="tablewapper1"><select id="ABO/Rh"><option>Select</option></select></td>
                </tr>
                 <tr>
                    <td class="tablewapper">Recipient Height(cm)</td>
					<td colspan=3><textarea cols="80" rows="4" id="rxBenefitPlan"></textarea></td>
                 </tr>
                </table>
        </div>
    </div>
</div>	 
<div class="column">
	<div  class="portlet">
				<div class="portlet-header ">Consult/Eval Checklist</div><br>
					<div class="portlet-content">
						<table>
							<tr>
								<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
								<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
						    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
							</tr>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" id="consultEvalChecklistDetails" class="display">
						<thead>
							<tr>
								<th width="4%" id="consultEvalChecklistColumn"></th>
								<th colspan='1'>Select</th>
								<th colspan='1'>Items</th>
								<th colspan='1'>Date</th>
								<th colspan='1'>Document Name</th>
								<th colspan='1'>Attachment</th>
								<th colspan='1'>Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="consultEvalChecklistVerified" onclick=""></td>
								<td>Eval / Assesment By MD</td>
								<td></td>
								<td>	</td>
								<td><input type="button" id="" value="Browse" /></td>
								<td></td>
						</tr>
						</tbody>
						</table>
			</div>
		</div>
	</div>	 
	
<div class="column">
	<div  class="portlet">
				<div class="portlet-header ">Consult/Eval Education</div><br>
					<div class="portlet-content">
						<table>
							<tr>
								<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
								<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
						    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
							</tr>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" id="consultEvalEducationDetails" class="display">
						<thead>
							<tr>
								<th width="4%" id="consultEvalEducationColumn"></th>
								<th colspan='1'>Select</th>
								<th colspan='1'>Items</th>
								<th colspan='1'>Date</th>
								<th colspan='1'>Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="consultEvalDocumentVerified" onclick=""></td>
								<td>Patient info Packet</td>
								<td></td>
								<td>	<select id="ABO/Rh"><option>Select</option></select></td>
							</tr>
						</tbody>
						</table>
			</div>
		</div>
	</div>	 