<%@ include file="common/includes.jsp"%>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<style type="text/css">
	.maintenance_slidewidget{
	 border: 1px solid #d1d1d1;
     -webkit-transition: all 0.6s ease 0s;
    -moz-transition: all 0.6s ease 0s;
    -o-transition: all 0.6s ease 0s;
    transition: all 0.6s ease 0s;
    float: right;
    width: 50%;
     -webkit-border-radius: 12px;
    -moz-border-radius: 12px;
    border-radius: 12px;
    text-shadow: 0 2px 3px rgba(0, 0, 0, 0.1);
    -webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset;
    -moz-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset;
	margin-right: 2%;
  padding: 0px 22px 1px 10px;
}
</style>
<script>
/**Bread crumb for ImmunoPhenoTyping starts here**/
var breadCrumbLevel=0;
var globalBreadCrumb = new Array();
function breadCrumbImmuno(formId,immunoCode,Desc,fromDivId,toDivId){
	$(".spanid").html(Desc+" Details");
	breadCrumbLevel+=1;
	var oldBreadCrumb = $("#breadCrumbLink").html();
	var strBreadcrumb = oldBreadCrumb+" > <a href=\"#\" onclick='changeBreadCrumbImmuno("+breadCrumbLevel+",\""+$.trim(Desc)+"\",\""+$.trim(immunoCode)+"\");constructTable(true,\""+immunoCode+"\",\"immunoLibTable\");'>"+$.trim(Desc)+"</a>";
	globalBreadCrumb.push(strBreadcrumb);
	$("#breadCrumbLink").html(strBreadcrumb);
}

function changeBreadCrumbImmuno(immunoLevel,Desc,immunoCode){
	$(".spanid").html(Desc+" Details");
	var strBreadcrumb = "";
	breadCrumbLevel = immunoLevel;
	immunoLevel=immunoLevel-1;
	strBreadcrumb = globalBreadCrumb[immunoLevel];
	$("#breadCrumbLink").html("");
	$("#breadCrumbLink").html(strBreadcrumb);
}
function changeBreadCrumbSideWidgetImmuno(immunoLevel,Desc,immunoCode){
	$(".spanid").html(Desc+" Details");
	breadCrumbLevel=1;
	var strBreadcrumb = "<a href='#' onclick=\"loadAction('fetchImmnoPhenoData.action?immuno=immuno')\">Immunophenotyping</a> > <a href=\"#\" onclick='changeBreadCrumbImmuno("+breadCrumbLevel+",\""+$.trim(Desc)+"\",\""+$.trim(immunoCode)+"\");constructTable(true,\""+immunoCode+"\",\"immunoLibTable\");'>"+$.trim(Desc)+"</a>";
	globalBreadCrumb = new Array();
	globalBreadCrumb.push(strBreadcrumb);
	$("#breadCrumbLink").html(strBreadcrumb);
}
/**Bread crumb for ImmunoPhenoTyping ends here**/

var oTable1;
$(document).ready(function(){
	show_slidewidgets('slidecolumnfilter');
	hide_slidecontrol();
	changeBreadCrumb("Immuno");
	$(".spanid").html("Immunophenotyping Details");
	$("#breadCrumbLevel").val('1');
	maintColumnfilter('qclib');
	constructTable(false,'immuno','immunoLibTable');
	$("#immunoCode").val('immuno');
	$("select").uniform(); 
  });
   /*
   DataTable data
  */
  $("#immunoLibTable tbody tr ").mouseenter(function(){
	  $(this).attr("style","background:#33ffff");	
  }).mouseleave(function() {
	  $(this).removeAttr("style");
	  $(this).css("cursor","default");
  });
  var code;
   
  $("#immunoLibTable tbody tr ").click(function(){  
	  $(this).css("cursor","pointer");
	$(this).find('td:eq(1)').click(function(){	
		code = $(this).text();
		//replacediv('immunoChild1',code,'qclibrary'); 
	 });	 
  });

  var code1=null;
  function replacediv(id,code,rid){
	  $(".spanid").html(code+" Details");
	  $("#qcName").val($.trim(code));
	  $('#'+id).css("display","block");
	  $('#'+rid).css("display","none");
	  //$('div #'+rid).replaceWith($('div #'+id));
  }
  function replaceqcDetails(id,code)
  {
	$(".spanid1").html(code);
	$('#'+id).css("display","block");
  	$('#immunoChild1').replaceWith($('#'+id));
  }
   
function displayResult(id)
{
	var tables=document.getElementById(id);
	var rowsLen = tables.rows.length;
	var row=tables.insertRow(rowsLen);
	$('#'+id).dataTable().fnAddData(["<input type='checkbox'  class='deleterows' id=''>","","","","<input type='radio' id='yes' name='activate'>Yes <input type='radio' id='no' name='activate'>no"]);
	setAttributeFlag(id);

	$(cell1).attr('style','width:100px');
	$(cell2).attr('style','width:100px');
	$(cell3).attr('style','width:100px');
	$(cell4).attr('style','width:100px');
	$(cell5).attr('style','width:100px');
  $(cell2).click(function(event) {
     if(!(this.getAttribute('textFlag')=="true")){
     insertTextBox(this);
     cellTextFlag=true;
      }
  });
  $(cell3).click(function(event) {
      if(!(this.getAttribute('textFlag')=="true")){
      insertTextBox(this);
      cellTextFlag=true;
      }
  });
  $(cell4).click(function(event) {
          if(!(this.getAttribute('textFlag')=="true")){
          insertTextBox(this);
          cellTextFlag=true;
          }
  });
   //cell0.innerHTML="&nbsp;";
  cell1.innerHTML="<input type='checkbox' class='deleterows' id=''>";
  cell2.innerHTML="&nbsp;";
  cell3.innerHTML="&nbsp;";
  cell4.innerHTML="&nbsp;";
  cell5.innerHTML="<input type='radio' id='yes' name='activate'>Yes <input type='radio' id='no' name='activate'>no";
  /* cell0.setAttribute('class','hidden'); */
  cell1.setAttribute('textFlag',false);
  cell2.setAttribute('textFlag',false);
  cell3.setAttribute('textFlag',false);
  cell4.setAttribute('textFlag',false);
  //cell5.setAttribute('textFlag',false);
   }
  // additives data
  var oldCellObj;
  var textCellFlag=false;
  function insertTextBox(cellObj){
      //alert(cellObj.getAttribute('textFlag'))

      if(oldCellObj){
          ///alert("11-->"+oldCellObj.getAttribute('textFlag'));
          }
      if(cellObj.getAttribute('textFlag')=="false" && oldCellObj && (oldCellObj.getAttribute('textFlag')=="true")){
          //alert("-->"+cellObj.getAttribute('textFlag'))
          var tempValue = $("#tempId").val();
          //alert("-->tempValue"+tempValue)
          cellObj.innerHTML="<input type='text' id='tempId' style='width:99%'  onblur='setCellValue(this)' value='"+cellObj.innerHTML+"'/>";
          cellObj.setAttribute('textFlag',true);
          oldCellObj.innerHTML=tempValue;
          oldCellObj.setAttribute('textFlag',false);
          oldCellObj=cellObj;
          textCellFlag=true;
          /*$('#tempId').blur(function() {
              setCellValue(this);
              });*/
          $('#tempId').focus();
          return;
      }
      if(cellObj.getAttribute('textFlag')=="false"){
          //alert("=====>"+cellObj.getAttribute('textFlag'))

          cellObj.innerHTML="<input type='text' id='tempId'style='width:99%'  onblur='setCellValue(this)' value='"+cellObj.innerHTML+"'/>";
          cellObj.setAttribute('textFlag',true);
          /*$('#tempId').blur(function() {
                setCellValue(this);
              });*/
          oldCellObj=cellObj;
          textCellFlag=true;
          $('#tempId').focus();
      }
      
  }
  
  
  function setAttributeFlag(id){
	   var productsTable = document.getElementById(id);
	     	for(i=1;i<productsTable.rows.length;i++){
	     		/* alert(productsTable.rows.length); */
	     		//productsTable.rows[i].cells[3].setAttribute();
	     		$(productsTable.rows[i].cells[1]).attr("textflag",false);
	     		$(productsTable.rows[i].cells[1]).attr("onclick","insertTextBox(this)");
	     		$(productsTable.rows[i].cells[2]).attr("textflag",false);
	     		$(productsTable.rows[i].cells[2]).attr("onclick","insertTextBox(this)");
	     		$(productsTable.rows[i].cells[3]).attr("textflag",false);
	     		$(productsTable.rows[i].cells[3]).attr("onclick","insertTextBox(this)");
	     			     	}
}
  function setCellValue(This){
      //alert(This.value);
      //alert(oldCellObj.innerHTML);

      oldCellObj.innerHTML=This.value;
      oldCellObj.setAttribute('textFlag',false);
      oldCellObj = null;
  }
jQuery.fn.blindToggle = function(speed, easing, callback) {
    var h = this.width() + parseInt(this.css('paddingLeft')) 
+  parseInt(this.css('paddingRight'));
    if(parseInt(this.css('marginLeft'))<0){
	   	   $('#forfloat_right').animate( { width: "82%" }, { queue: false, duration: 200 });	
        $('#blind').html("<<");
        return this.animate({marginLeft:0},speed, easing, callback);
        }
    else{
		  $( "#forfloat_right" ).animate( { width: "95%" }, { queue: false, duration: 250 });

        $('#blind').html(">>");
        return this.animate({marginLeft:-h},speed, easing, callback);
		   
          }
		
  };
 
  
  
 // var oTable13=$("#protocolLibTable").dataTable({"bJQueryUI": true,"bPaginate":true});

  function deletechildrows(id) {var  oTable1= $('#'+id).dataTable();
	  $("#deleteallProtocol").attr('checked', false);
	  try {
            var isChecked = $('#'+id).find('tr').length;
            var isCheckedall = $('#deleteallProtocol').attr('checked');
            if(isChecked>=1 && $('.deleterows').is(':checked')){
            if(isCheckedall){
                 var yes=confirm('Do you want delete all products ?');   
            }
            else{var yes=confirm('Do you want delete selected products?');
            }
            var rowCount = $('#'+id).find('tr').length;
          if(yes){
               $('#'+id+' tbody tr').each(function(i,v){
                      var cbox=$(v).children("td:eq(0)").find(':checkbox').is(":checked");
                      if(cbox){
                    	  oTable1.fnDeleteRow(this);
                      
                      }
              });
           $('.deleterows').attr('checked', false);

          }
      }
           //rowCount = table.rows.length;
     
      }catch(e) {
          alert(e);
      }
      $('#deleteallProtocol').attr('checked', false);
   }


function loadImmunoDetails(formId,immunoCode,Desc,fromDivId,toDivId){
	$("#immunoCode").val(immunoCode);
	constructTable(true,immunoCode,'immunoLibTable');
}

/*****************New Datatable save, Delete and Edit starts here******/
function copyImmunoPheno(immunoId,immunoName,immunoCode,immunoDesc,parentCode){
		//alert("copy protocol");
		$("#immunoCode").val(parentCode);
		var copyText =" - Copy";
		strBreadcrumb = "<a href='#' id='breadlevel1' onclick='constructTable(true);'>ImmunoPhenoTyping</a> ";
		var immuno = $("#immunoCode").val();
		//$("#immunoLibTable").ajaxComplete(function(e, xhr, settings) {
			addRows(0,immunoName+copyText,immunoCode +copyText,immunoDesc,immunoId,false);
		//});
		//constructTable(true,immuno,'immunoLibTable');
	}

function addRows(immunoPheno,immunoPhenoName,immunoPhenoCode,immunoPhenoDesc,copyProtocol,activeFlag){
	 //alert(" add Rows");
	 elementcount +=1;
	 var newRow = "<tr><td>&nbsp;</td><td><input type='checkbox' id='immunophenoTyping' value='"+immunoPheno+"'> <input type='hidden' id='copyProtocol' name='copyProtocol' value='"+copyProtocol+"' /></td><td><input type='text' id='immunoPhenoName' name='immunoPhenoName' value='"+immunoPhenoName+"' /></td><td><input type='text' id='immunoPhenoCode' name='immunoPhenoCode' value='"+immunoPhenoCode+"' /></td>";
	   newRow += "<td><input type='text' id='immunoPhenoDesc' name='immunoPhenoDesc' value='"+immunoPhenoDesc+"' /></td>";
	   newRow += "<td><input type='radio' value='Yes' name='active_a"+elementcount+"' id='activeyes';" ;
	   //alert(newRow);
	   if(activeFlag){	
		   newRow += " checked='checked' ";
	   }   
	   newRow += " >Yes</input><input type='radio' value='No' name='active_a"+elementcount+"' id='active' " ; 
	   if(!activeFlag){	
		   newRow += " checked='checked' ";
	   }
		   newRow += "  >No</input></td></tr>";
		  // alert("s "+newRow);
	   $("#immunoLibTable").prepend(newRow); 
	   $("#immunoLibTable .ColVis_MasterButton").triggerHandler("click");
	   $('.ColVis_Restore:visible').triggerHandler("click");
	   $("div.ColVis_collectionBackground").trigger("click");
 }

/*function addRows(tableId){
		 elementcount +=1;
		 $("#"+tableId).prepend("<tr><td><input type='checkbox' id='immunophenoTyping' value='0'></td><td><input type='text' id='immunoPhenoName' name='immunoPhenoName' /></td><td><input type='text' id='immunoPhenoCode' name='immunoPhenoCode' /></td><td><input type='text' id='immunoPhenoDesc' name='immunoPhenoDesc' /></td><td><input type='radio' value='Yes' name='active_a"+elementcount+"' id='activeyes' >Yes</input><input type='radio' value='No' name='active_a"+elementcount+"' id='active' >No</input></td></tr>");  
	}
*/
function deleteRows(){
		  //alert("Delete Rows");
		  var deletedIds = ""
		  $("#immunoLibTable #immunophenoTyping:checked").each(function (row){
				//alert($(this).val());
				deletedIds += $(this).val() + ",";
			 });
			 //alert("deletedIds.length "+deletedIds.length);
			 //return;
		  if(deletedIds.length >0){
			  deletedIds = deletedIds.substr(0,deletedIds.length-1);
		  }
		  //alert("deletedIds " + deletedIds);
		  jsonData = "{deletedImmunos:'"+deletedIds+"'}";
		  url="deleteImmunoPhenoTyping";
	     //alert("json data " + jsonData);
	     response = jsonDataCall(url,"jsonData="+jsonData);
	     var immuno = $("#immunoCode").val();
	     constructTable(true,immuno,'immunoLibTable');

		//to Refresh side Widgets data
		var url = "fetchImmnoPhenoData";
		var result = ajaxCall(url,'immunoSideWidgets');
		$("#qclib").replaceWith($('#qclib', $(result)));

	}

function editRows(tableId){
		  //alert("edit Rows");
		
		  $("#"+tableId+" tbody tr").each(function (row){
			 if($(this).find("#immunophenoTyping").is(":checked")){
				 $(this).find("td").each(function (col){
					 if(col ==1 ){
						 if ($(this).find("#immunoPhenoName").val()== undefined){
							$(this).html("<input type='text' id='immunoPhenoName' name='immunoPhenoName' value='"+$.trim($(this).text())+ "' />");
						 }
					 }else if(col ==2){
						 if ($(this).find("#immunoPhenoCode").val()== undefined){
						 	$(this).html("<input type='text' id='immunoPhenoCode' name='immunoPhenoCode' value='"+$.trim($(this).text())+ "' />");
						 }
					 }else if(col ==3){
						 if ($(this).find("#immunoPhenoDesc").val()== undefined){
						 	$(this).html("<input type='text' id='immunoPhenoDesc' name='immunoPhenoDesc' value='"+$.trim($(this).text())+ "' />");
						 }
					 }
				 });
			 }
		  });
	  }
	  

function saveImmunoPheno(){
	var immuno = $("#immunoCode").val();
	
		 $('.progress-indicator').css( 'display', 'block' );
		 var rowData = "";
		 var immunophenoTyping=0;
		 var immunoPhenoName;
		 var immunoPhenoCode;
		 var immunoParrentCode;
		 var protocolModule;
		 var copyCodelst;
		 var immunoPhenoDesc ;
		 var isActivate =0;
		 $("#immunoLibTable tbody tr").each(function (row){
			 isActivate =0;
		 	$(this).find("td").each(function (col){
			 		//Have to add Parent Code
			 		immunoParrentCode = immuno; 		
			 	   if(col ==0){
				 		immunophenoTyping= $(this).find("#immunophenoTyping").val();
				 		copyCodelst =$(this).find("#copyProtocol").val();
				 		if(copyCodelst == undefined){
				 			copyCodelst =0;
				 		}
				 		//alert("copyCodelst " + copyCodelst);
				 	 }else if(col ==1){
				 		immunoPhenoName=$(this).find("#immunoPhenoName").val();
				 	 }else if(col ==2){
				 		immunoPhenoCode=$(this).find("#immunoPhenoCode").val();
				 	 }else if(col ==3){
				 		immunoPhenoDesc=$(this).find("#immunoPhenoDesc").val();
				 	 }else if(col ==4){
				 		 if($(this).find("#activeyes").is(":checked")){
				 		    isActivate = 'N';
				 		 }else{
				 			isActivate = 'Y';
				 		 }
				 	 }
			  });
		 		if(immunophenoTyping!= undefined && immunoPhenoName != undefined){
			 	  rowData+= "{immnopheId:"+immunophenoTyping+",isHide ="+isActivate+",description:'"+immunoPhenoName+"',immnoCode:'"+immunoPhenoCode+"',immunoComments:'"+immunoPhenoDesc+"',immnoParentCode:'"+immunoParrentCode+"',copyImmuno:'"+copyCodelst+"'},";
		 		}
		 		//alert(rowData);
		 });
		
		 if(rowData.length >0){
	            rowData = rowData.substring(0,(rowData.length-1));
	        }
	        url="saveImmnoPhenoTyping";
		    response = jsonDataCall(url,"jsonData={protocolData:["+rowData+"]}");

			//to Refresh side Widgets data
			var url = "fetchImmnoPhenoData";
			var result = ajaxCall(url,'immunoSideWidgets');
			$("#qclib").replaceWith($('#qclib', $(result)));
			    
		     //BB$("select").uniform();
		     alert("Data Saved Sucessfully");
		     $('.progress-indicator').css( 'display', 'none' );
		     //constructTable(true);
		     var immuno = $("#immunoCode").val();
		     constructTable(true,immuno,'immunoLibTable');
	 }

function getQcDetails(pkCodelst,desc,isHide){
	  	$(".spanid").html(desc+" Details");
	  	$("#qcName").val($.trim(desc));
	  	if(isHide=="Y"){
	  		$("#deActivateYes").checked = false;
	  		$("#deActivateNo").checked = true;
		  	}
	  	if(isHide=="N"){
	  		$("#deActivateYes").checked = true;
	  		$("#deActivateNo").checked = false;
		  	}
	  	//alert($('#commondiv').html());	
	  	$('#qclibrary').html($('#commondiv').html());
	  	alert($("#qcName").val());
	  	//alert("d");
	  	//$('#immunoLibTable').replaceWith($('#commondiv'));
	  }
	  var oTable;
function constructTable(flag,immuno,tableId) {
		//alert("construct table : "+tableId+",immuno : "+immuno);
		var criteria = "";
		if(immuno!=null && immuno!=""){
			criteria = "START WITH IMMNO_CODE = '"+immuno+"' CONNECT BY PRIOR IMMNO_CODE = IMMNO_PARENTCODE";
		}
		else{
			criteria = "START WITH IMMNO_CODE = '"+immuno+"' CONNECT BY PRIOR IMMNO_CODE = IMMNO_PARENTCODE";
			}
		
		elementcount =0;
		//return;
		$('.progress-indicator').css( 'display', 'block' );
		var strBreadcrumb = "<a href='#' onclick='constructTable(true);'></a>";
		$("#breadcrumb").html(strBreadcrumb );
				$("#protocollist").removeClass("hidden");
				$("#protocolDetails").addClass("hidden");
	 	if(flag || flag =='true'){
	 		oTable=$('#immunoLibTable').dataTable().fnDestroy();
		}
		  var link ;
			try{
				oTable=$('#'+tableId).dataTable( {
					"sPaginationType": "full_numbers",
					"bProcessing": true,
					"bServerSide": true,
					"bRetrieve" : true,
					"sDom": 'C<"clear">Rlfrtip',
					 "bDestroy": true,
					 "sScrollX":"100%",
					 "aaSorting": [[ 1, "asc" ]],
					 "aoColumns": [{"bSortable": false},
					               {"bSortable": false},
						               null,
						               null,
						               null,
						               null],
					"oColVis": {
								"aiExclude": [ 0 ],
								"buttonText": "&nbsp;",
								"bRestore": true,
								"sAlign": "left",
								"iOverlayFade": 25     /* To Overcome the visibility while clicking the addButton*/
						},						               
						"oLanguage": {"sProcessing":""},									
					"fnServerParams": function ( aoData ) {
															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "maintenance_panelconfig_immnophenotyping"});	
															aoData.push( { "name": "criteria", "value": criteria});
															
															aoData.push({"sColumns":"Protocol Name,Module,Version"});
															
//SELECT PK_IMMNOPHENOTYPING, IMMNO_CODE, IMMNO_PARENTCODE,IMMNO_DESC, LEVEL,IMMNO_HIDE,IMMUNO_COMMENTS FROM er_immnophenotyping

															aoData.push( {"name": "col_0_name", "value": "lower(IMMNO_DESC)" } );
															aoData.push( { "name": "col_0_column", "value": "lower(IMMNO_DESC)"});
															
															aoData.push( { "name": "col_1_name", "value": "lower(IMMNO_CODE)"});
															aoData.push( { "name": "col_1_column", "value": "lower(IMMNO_CODE)"});
															
															aoData.push( {"name": "col_2_name", "value": "lower(IMMUNO_COMMENTS)" } );
															aoData.push( { "name": "col_2_column", "value": "3"});
															
															aoData.push( {"name": "col_3_name", "value": "lower(IMMNO_HIDE)" } );
															aoData.push( { "name": "col_3_column", "value": "lower(IMMNO_HIDE)"});
					},
					"sAjaxSource": 'dataSearch.action',
					"sAjaxDataProp": "filterData",
					"aoColumnDefs": [
									 {
										 	"aTargets": [ 0], "mDataProp": function ( source, type, val ) { 
											 return "";}},
					                  {"sTitle":"Check All <input type='checkbox' id='deleteallProtocol' onclick='checkall(\"immunoLibTable\",this)'/>",
											"aTargets": [ 1], "mDataProp": function ( source, type, val ) {
						                  
					                	 return "<input type='checkbox' id='immunophenoTyping' name='immunophenoTyping' value='"+source[0] +"' >";

					                	 }},
									  {	"sTitle":"Name",
					                		 "aTargets": [ 2], "mDataProp": function ( source, type, val ) { 
										    link = "<a href='#' onclick='javascript:breadCrumbImmuno(\"immunoformMain\",\""+source[1]+"\",\""+source[3]+"\",\"qclibrary\",\"immunoChild1\");loadImmunoDetails(\"immunoformMain\",\""+source[1]+"\",\""+source[3]+"\",\"qclibrary\",\"immunoChild1\");'>"+source[3]+"</a>";
										  return link;}},
									  {	"sTitle":"Code",
											  "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
										 	   return source[1];
									  		}},
									  {	"sTitle":"Description",
									  		  "aTargets": [ 4], "mDataProp": function ( source, type, val ) { 
										  		return source[6];
										  }},
									  {	"sTitle":"Activated",
											  "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
										  link = ""; 
										  if(source[5]=='N'){
											  link = "<input type='radio' name='active_"+source[0]+"' id='activeyes' value='Yes' checked='checked' >Yes</input>";
											  link+= "<input type='radio' name='active_"+source[0]+"' id='active' value='No' >No</input>";
										  }else if(source[5]=='Y'){
											  link = "<input type='radio' name='active_"+source[0]+"' id='activeyes' value='Yes'  >Yes</input>";
											  link+= "<input type='radio' name='active_"+source[0]+"' id='active' value='No' checked='checked'>No</input>";
										  }
										  return link;}}
									],
						"fnInitComplete": function () {
								        $("#immunoColumn").html($('.ColVis:eq(0)'));
							},
						"fnServerData": function ( sSource, aoData, fnCallback ) {
							$('.progress-indicator').css( 'display', 'block' );
							$.ajax( {
									"dataType": 'json',
									"type": "POST",
									"url": 'dataSearch.action',
									"data":aoData,
									"success":fnCallback,  
									"complete": function () {
												$('.progress-indicator').css( 'display', 'none' );
												$("select").uniform();
												}
								  } );},
						"fnDrawCallback": function() { 
							//alert("fnDrawCallback");
							//$('.progress-indicator').css( 'display', 'none' );
				      }
					} );
				jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = parseInt(no_of_pages);
				}catch(err){
					alert("error " + err);
				}
				//alert("end of dt");
	} 

/**New Datatable save, Delete and Edit ends here******/

</script>

<div class="cleaner"></div>
<!-- recipient start-->
					<div id="slidecolumnfilter" style="display:none"> 
					<br>
					<div class="portlet" id="panelMaintenanceDiv" >
					<div class="portlet-header"><s:hidden id="moduleName" value="overnightStorage"/><s:hidden id="widgetName" value="recipient"/><s:hidden id="pkNotes" value=""/>Immunophenotyping <input type="text" id="Columnfilter" class="inputboxSearch maintenance_slidewidget" onclick="maintColumnfilter('qclib')"></div>
						<div class="cleaner"></div>
						<div class="portlet-content">
						<form id="immunoSideWidgets">
						<s:hidden name="immuno" id="immuno" value="immuno"/>
						<s:hidden name="breadCrumbLevel" id="breadCrumbLevel" value=""/>
						<div style="height:320px;overflow-y:auto;">
						<table width="100%" border="0" id="qclib">
						<!-- <tr>
						<td>
						<input type="text" id="Columnfilter" class="">
						</td>
						</tr> -->
							<s:iterator value="ImmnoPhenoList" var="rowVal" status="row">
								<!---<s:iterator value="rowVal" var="cellValue" status="col">-->
									<s:if test="#rowVal[4]==2 && #rowVal[3]!=#immnoDesc">
									<s:set name="immnoDesc" value="#rowVal[3]"></s:set>
									<tr>
										<td style="padding:2%" width="90%">
										<input type="hidden" id="qcPrmariyId" name="qcPrmariyId" /><a href="#" onclick="javascript:changeBreadCrumbSideWidgetImmuno('1','<s:property value="#rowVal[3]"/>','<s:property value="#rowVal[1]"/>');loadImmunoDetails('immunoformMain', '<s:property value="#rowVal[1]"/>','<s:property value="#rowVal[3]"/>','qclibrary','immunoChild1');$('#immunoChild2').css('display','none')" ><s:property value="#rowVal[3]"/></a></td>
										<td width="10%"><div><img src = "images/icons/folder.png" class="cursor" onclick="copyImmunoPheno('<s:property value="#rowVal[0]"/>','<s:property value="#rowVal[3]"/>','<s:property value="#rowVal[1]"/>','<s:property value="#rowVal[6]"/>','<s:property value="#rowVal[2]"/>')"/></td>
									</tr>
									</s:if>
								<!--</s:iterator>-->
							</s:iterator>  
						</table>
						</div>
						</form>
					</div>
					</div>
					</div> 
		
<!--float window end-->

<!--right window start -->	
<!-- <div id="forfloat_right"> -->
<div id="breadCrumbLink" ></div>
<div class="cleaner"></div>
<div class="column" id="rightMainDiv">
		<div class="column" id="qclibrary">		
		<div  class="portlet">
		<div class="portlet-header notes info"><span class="spanid"> </span></div>			
		<form id="immunoformMain">
		<s:hidden id="immunoCode" name="immunoCode"/>
			<div class="portlet-content" id="childDiv">
			<table>
			<tr>
			<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addRows(0,'','','',0,false);displayResult('qclibrary')"/>&nbsp;&nbsp;<label id="adchild"><b>Add</b></label>&nbsp;</div></td>
			<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;cursor:pointer;" id ="" onclick="deleteRows();deletechildrows('qclibrary');"/>&nbsp;&nbsp;<label id="dchild"><b>Delete</b></label>&nbsp;</div></td>
		    <td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" id ="" onclick="editRows('immunoLibTable')"/>&nbsp;&nbsp;<label id="dchild"><b>Edit</b></label>&nbsp;</div></td>
		    <td style="width:20%"><div><img src = "images/icons/save3.png" style="width:16;height:16;" class="cursor" id ="" onclick="saveImmunoPheno()" >&nbsp;&nbsp;<label id="schild" class="cursor" onclick="saveProtocols()"><b>Save</b></label>&nbsp;</div></td>
			</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" id="immunoLibTable" class="display">
				<thead>
					<tr>
						<th width="4%" id="immunoColumn"></th>
						<th><s:text name="stafa.label.checkall"/><input type="checkbox"  id="deleteallProtocol" onclick="checkall('immunoLibTable',this)"></th>
						<th><s:text name="stafa.maintenance.label.name"/></th>
						<th><s:text name="stafa.label.code"/></th>
						<th><s:text name="stafa.maintenance.label.comments"/></th>
						<th><s:text name="stafa.label.activated"/></th>
					</tr>
				</thead>
			</table>
				<!-- <div id="bor" style="float:right;">
				<form>
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="saveImmunoPheno()" value="Save"/></td>
				</tr>
				</table>
				</form>
		</div>-->
	</div>
	</form>
	</div>                    	 	
	</div>
</div>
<!--  Child 1 div  start here -->
<!-- 
<div class="column" id="immunoChild1" style="display:none">		
		<div  class="portlet">
			<div class="portlet-header notes info"><span class="spanid"> </span></div>			
			<div class="portlet-content">
			<form id="immunoformChild1">
			<s:hidden id="immunoCode" name="immunoCode"/>
				<div id="table_inner_wrapper">
					<div >					
						<table>
			<tr>
			<td style="width:20%"><div><img src = "images/icons/addnew.jpg" cursor:pointer;"  onclick="displayResult('qclibrary')"/>&nbsp;&nbsp;<label id="adchild"><b>Add</b></label>&nbsp;</div></td>
			<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;cursor:pointer;" id ="" onclick="deletechildrows('qclibrary');"/>&nbsp;&nbsp;<label id="dchild"><b>Delete</b></label>&nbsp;</div></td>
		    <td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" id ="""/>&nbsp;&nbsp;<label id="dchild"><b>Edit</b></label>&nbsp;</div></td>
			</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" id="immunoLibTable1" class="createchilddisplay" width="100%">
				
			</table>
				<div id="bor" style="float:right;">
		
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="saveQc()" value="Save"/></td>
				</tr>
				</table>					
						
					</div>
				</div>
				</div>
				</form>			
			</div>
		</div>
	</div>
 -->

<!--  Child 2 div  start here -->
<!-- 	
	<div class="column" id="immunoChild2" style="display:none">		
		<div  class="portlet">
			<div class="portlet-header notes info"><span class="spanid"> </span></div>			
			<div class="portlet-content">
			<form id="immunoformChild2">
			<s:hidden id="immunoCode" name="immunoCode"/>
				<div id="table_inner_wrapper">
					<div >					
						<table>
			<tr>
			<td style="width:20%"><div><img src = "images/icons/addnew.jpg" cursor:pointer;"  onclick="displayResult('qclibrary')"/>&nbsp;&nbsp;<label id="adchild"><b>Add</b></label>&nbsp;</div></td>
			<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;cursor:pointer;" id ="" onclick="deletechildrows('qclibrary');"/>&nbsp;&nbsp;<label id="dchild"><b>Delete</b></label>&nbsp;</div></td>
		    <td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" id ="""/>&nbsp;&nbsp;<label id="dchild"><b>Edit</b></label>&nbsp;</div></td>
			</tr>
			</table>
			
			<table cellpadding="0" cellspacing="0" border="0" id="immunoLibTable2" class="createchilddisplay" width="100%">
			
			</table>
			
				<div id="bor" style="float:right;">
		
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="saveQc()" value="Save"/></td>
				</tr>
				</table>					
						
					</div>
				</div>
				</div>
				</form>			
			</div>
		</div>
	</div>
	 -->
		
<!-- </div> -->
