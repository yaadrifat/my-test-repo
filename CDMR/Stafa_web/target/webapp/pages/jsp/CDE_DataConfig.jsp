<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript">

function saveCDEDataMapping(){
	
}

function listCdeValue(){
var stafaCdeDataCodelst = $("#stafaCdeDataCodelst").val();
//alert(stafaCdeDataCodelst);

$.ajax({
    type: "POST",
    url: "fetchCdeValues.action",
    async:false,
    data:{stafaCdeDataCodelst:stafaCdeDataCodelst},
    success:function (result){
    	generateTableFromJsnList(result);
    		},
	error: function (request, status, error) {
		alert("Error " + error);
	}
});

}

function generateTableFromJsnList(response){
	var tab = "<table border='1' cellpadding='0' cellspacing='0'>";
	$.each(response.cdeDataValueList, function(i,v) {
		tab+="<tr><td>"+v[0]+"</td><td>"+v[1]+"<td></tr>";
	 	});
	tab+="</table>";
	//alert(tab);
	$("#listVal").html(tab);
}

$(document).ready(function() {

	oTable = $('#CDETable').dataTable({
		"bJQueryUI": true
		//"sPaginationType": "full_numbers"
	});
		
});	
 $("#stafacdecodelst").change(function () {
			var k=$(this).val();
						if(k==0)
				{
				$("#stafacdecodelst1").html("<option value=>Select</option>,<option value=0>MDACC Temporary</option>,<option value=1>MDA Aphersis</option>,<option value=2>MDA North</option>");
				}
			 if(k==1)
				{
				$("#stafacdecodelst1").html("<option value=>Select</option>,<option value=0>Heart1</option>");
				}
			 if(k==2)
				{
				$("#stafacdecodelst1").html("<option value=>Select</option>,<option value=0>Out Patient</option>,<option value=1>Others</option>,<option value=2>In Patient</option>");	
				}
			 if(k==3){
				$("#stafacdecodelst1").html("<option value=>Select</option>,<option value=0>Core</option>,<option value=1>Flow</option>,<option value=2>Research</option>");	
			}
			 if(k==4){
				$("#stafacdecodelst1").html("<option value=>Select</option>,<option value=0>Culture</option>,<option value=1>Fresh</option>");	
			}
			if(k==11)
				{
				$("#stafacdecodelst1").html("<option value=>Select</option>");
				}

});
 $(function(){
	    $("select").uniform();
	  });
</script>

<section>
<div class="cleaner"></div>
<form>
<div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes "><s:hidden id="moduleName" value="CDEmappinginfo"/><s:hidden id="widgetName" value="cdemapinfo"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.cdemapinfo"/></div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="CDETable" class="display" >
			<thead>
						<tr>
							<th>CDE Code List</th>
							<th>Stafa Code List Description</th>
							<th>Valid Values</th>
							<th>Value Meaning</th>				
						</tr>
			</thead>
			<tbody>
			<tr><td>Acq Location</td><td>MDACC Temp</td><td>Not Tested</td><td>Not Tested</td></tr>
			<tr><td>Acq Products</td><td>Heart</td><td>Not Measurable</td><td>Non-Measurable</td></tr>
			</tbody>		
			</table>
	</div>
	</div>	
</div>
</form>
<form action="#" id="cdeForm">
<div class="column">		
		<div  class="portlet">
		<div class="portlet-header notes reset"><s:hidden id="moduleName" value="cdemapping"/><s:hidden id="widgetName" value="cdemapping"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.cdemapping"/></div>
		<div class="portlet-content">
		<div id="button_wrapper" align="center">
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="68%">
						<tr>
								<td width="25%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.applicationname"/></div></td>
		
								<td width="50%" height="40px"><div  class="mainselection" align="left"><s:select  headerKey="" headerValue="Select" list="#{'0':'CDE Data','1':'eResearch'}" name="appcodelstname" id="appcodelstname" /></div></td>
								</tr>
		</table>
		</div>
			<div id="table_inner_wrapper">
			<div id="left_table_wrapper">
					<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
						<tr>
						
						<td width="50%" height="40px"><div class="txt_align_label" align="left"> Parent Code</div></td>
					<td width="50%" height="40px"><div  class="mainselection" align="left"><s:select  headerKey="11" headerValue="Select" list="#{'0':'Acquistion Location','1':'Acquistion Products','2':'Donor Location','3':'Processing Selection','4':'Proposed Final Diposition'}" name="stafacdecodelst" id="stafacdecodelst" />
</div></td>						
							<td width="50%" height="40px"><div class="txt_align_label" align="left"> Description</div></td>
					<td width="50%" height="40px"><td width="50%" height="40px"><div  class="mainselection" align="left"><s:select  headerKey="" headerValue="Select" list="#{}" name="stafacdecodelst" id="stafacdecodelst1"/></div></td>
	

						
						<%--						<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.stafacodelst" /></div></td>
 <td width="50%" height="40px"><div align="left"><s:select   headerKey="pkCodelst" headerValue="Select" list="accLocationList" listKey="pkCodelst" listValue="pkCodelst+'-'+description"  name="stafacodelst" id="stafacodelst"/></div></td></s:hidden>
 --%>						</tr>
					</table>
			</div>
		<div id="middle_line"></div>
				<div id="right_table_wrapper">					
					
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="68%">				
						<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.stafacdecodelst" /></div></td>
								<td width="50%" height="40px"><div align="left"><s:select  headerKey="" headerValue="Select" list="cdeDataList" listKey="dataElementPublicId" listValue="dataElementLongName" name="stafaCdeDataCodelst" id="stafaCdeDataCodelst" onChange="listCdeValue();"/></div></td>
						</tr>
						<tr>
								<td width="50%" height="80px" colspan="2" id="listVal"></td>
						</tr>
						
						</table>
						
				</div>
			</div>
			<div id="button_wrapper" align="center"><input type="button" value="Save" onClick="saveCDEDataMapping()"/>
 </div>
			</div>
		</div>
		</div>	
</form>		
	</section>	
<div class="cleaner"></div>	
	<script>	
	$(function() {
		
		$( ".column" ).sortable({
			connectWith: ".column"
		});

		$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
		.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
		

		$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
		.append("<span style=\"float:right;\" class='ui-notes'></span>");

		$( ".portlet-header .ui-notes " ).click(function() {
			showNotes('open',this);
		});


		$( ".portlet-header .ui-icon" ).click(function() {
			$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
			$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
		});

		$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
	});

	</script>
	


