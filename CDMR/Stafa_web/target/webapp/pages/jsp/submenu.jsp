<%@ page contentType="text/html; charset=UTF-8"%>
<%@ include file="common/includes.jsp" %>
<% String contextpath=request.getContextPath();
String STAFA_ADMINISTRATOR = "AdministratorOption";
String STAFA_DATAFIELD = "DataFieldConfig";
String STAFA_PANEL = "PanelConfig";
String STAFA_TECHNICIAN = "TechnicianView";
String STAFA_HOME = "Home";
String STAFA_ACQUISITION = "Acquisition";
String STAFA_ACCESSION = "Accession";
String STAFA_PRODUCTION = "Production";
String STAFA_PREPRODUCT ="Pre-Product";
String STAFA_OVERNIGHTSTOR = "OvernightStorage";
String STAFA_ORGPRODUCT = "OriginalProduct";
String STAFA_PROCESSING = "Processing";
String STAFA_CRYOPREP="Cryoprep";
String STAFA_STORAGE = "Storage";
String STAFA_STOREPROD = "StoreProduct";
String STAFA_TRANSFER = "Transfer";
String STAFA_RETRIEVE="Retrieve";
String STAFA_RELEASE = "Release";
String STAFA_DOCUMENTS="Documents";
String STAFA_THAW="Thaw";
String STAFA_WASH="Wash";
String STAFA_INFRELEASE="InfusionRelease";
String STAFA_PHYSICIAN="PhysicianView";
String STAFA_SELPRODUCT="SelectProduct";
String STAFA_SUPERVISOR="Supervisor";
String STAFA_TASK="TaskManagement";
String STAFA_NURSEVIEW="NurseView";
String STAFA_SUPERVISORVIEW="SupervisorView";
String STAFA_RECIPIENTTRACKER="RecipientTracker";
String CDMR_SETTINGSVIEW="Settings";
String CDMR_NOTIFYVIEW= "Notification";
%>
<%!
String checkAccessRights(String moduleName){
	//System.out.println(" moduleName--------------------------- " + moduleName+"=="+(String)moduleMap.get(moduleName));
	 return (String)moduleMap.get(moduleName);
}
 %>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset=utf-8>
<link rel="stylesheet" href="css/jquery.megamenu.css" type="text/css" media="screen" />
<script src="js/jquery/jquery.megamenu.js" type="text/javascript"></script>

<style type="text/css">

#main-nav {
	margin: 0px 0px 10px 2px;
	text-align: left;
	min-height: 25px;
	padding-top: 10px;
	padding-left: 0px;
}

#main-nav li {
	display: inline;
	list-style: none;
}
#main-nav li a {
	margin-right: 5px;
	font-size: 15px;
	text-decoration: none;
	color: #000;
	font-weight: bold;
	padding: 3px 10px 3px 10px;
	outline: 0;
	position: relative;
	border-radius: 4px 4px 4px 4px;
	top: -3px;
	font-size:1.1em;
}
#main-nav li ul li a {
	margin-right: 5px;
	font-size: 15px;
	text-decoration: none;
	color: #fff;
	font-weight: normal;
	padding: 3px 10px 3px 10px;
	outline: 0;
	position: relative;
	border-radius: 4px 4px 4px 4px;
	top: -2px;
	font-size:1.1em;
}
#main-nav li a:hover, #main-nav li a.active {
	background: #84845a;
	color:#fff;
	/*background: #F7F7F7;*/
}

.activeMenu {
	background: #84845a;
	color:#fff;
}

#main-nav li a.selected {
	background: #84845a;
}

#sub-link-bar {
	background: #000000;
	display: block;
    position: relative;
    top: 32px;
	display:none;
	

}
.sub-links {
	display: none;
	position: absolute;
	width:100%;
	top:32px;
	padding-top:10px;
	text-align: left;
	left: 0px;
	background:#a0a07c;
	border-radius: 4px 4px 4px 4px;
}
#main-nav li .sub-links li a:hover{
	background: #84845a;
}

#main-nav li .sub-links li a:selected{
	background: #a0a07c;
	color:#fff;
}

#main-nav li a.close{
	display: none;	
	position: absolute;
	
}
#main-nav li a.close:hover{
	background: #900;
}
<!--Thanks Spiffy Corners--> 
.round {
	display:block
}
.round * {
	display:block;
	height:1px;
	overflow:hidden;
	font-size:.01em;
	background:#645546
}
.round1 {
	margin-left:3px;
	margin-right:3px;
	padding-left:1px;
	padding-right:1px;
	border-left:1px solid #443a30;
	border-right:1px solid #443a30;
	background:#56493c
}
.round2 {
	margin-left:1px;
	margin-right:1px;
	padding-right:1px;
	padding-left:1px;
	border-left:1px solid #322a23;
	border-right:1px solid #322a23;
	background:#594c3e
}
.round3 {
	margin-left:1px;
	margin-right:1px;
	border-left:1px solid #594c3e;
	border-right:1px solid #594c3e;
}
.round4 {
	border-left:1px solid #443a30;
	border-right:1px solid #443a30
}
.round5 {
	border-left:1px solid #56493c;
	border-right:1px solid #56493c
}
.roundfg {
	background:#645546
}
/* CDMR CSS*/
#logo{
	height: 50px;
	width: 200px;
	background-color: gray;
}

#headline{
	margin-left: 860px;;
}

ul.dropdown li{
width: 150px;
}

#fileInfoTable {
	width: 960px !important;
}

	#fileInfoTable th{
	width: 320px !important;
}

	#fileInfoTable td{
	padding-left: 1%;
}
/*CDMR CSS END*/


 /* pin Notifications Style */


</style>
<!-- End of pin Notification style -->




<script type="text/javascript">
$(document).ready(function(){
	//$(".fulldiv").draggable();

	//$("#userprofile").text($("#userDisplay").text()+"--->");
	//$("#userprofile").text(<%userInfo.getFirstName();%>+"--->");

	//alert(" sub menu" + $("#userDisplay").text());
	$("#main-nav li a.main-link").hover(function(){
		$("#main-nav li a.close").fadeIn();
		$("#main-nav li a.main-link").removeClass("active");												 
		$(this).addClass("active");
		//var s2 = $(".active").parent().find(".sub-links");
		var submenuDisplay = $(".active").parent().hasClass(".hasSubmenu");
		//alert(submenuDisplay);
		if(submenuDisplay){
			$("#sub-link-bar").animate({
				height: "38px"
			});
			$(".active").parent().addClass("activeMenu");
		}else{
			$("#sub-link-bar").animate({
				height: "0px"					   
			});
			//$(".sub-links").hide();
			$(".sub-links").css("display","none");
			//$(".sub-links").fadeOut();

		}
		//alert("b4")
		//$(".sub-links").hide();
		//alert("s")
		//$(this).siblings(".sub-links").fadeIn();
		$(this).siblings(".sub-links").css("display","block");
		/*if($(".active").hasClass("selectedMenu")){
			alert("s");
			$(".selectedMenu").removeClass("active");
		}
		*/

	});
});

function openReport(targetId) {
$("#report").dialog({
modal:true,
resizable:true,
height:700,
width:1000,
closeText:'',
close:function() { $("#report" ).dialog("destroy"); }
});

$('#report').load('../pages/jsp/common/report.jsp');
}
function pinAlert(){
	//alert("1");
	var length= $(".fulldiv").length;
	$(".fulldiv1").append("<div class='fulldiv'><img src='/images/client/anderson_logo.jpg' id='closeimg' onclick='closepin(this)' title='close'><img src='images/icons/ic_minimize.png' id='minusimage' onclick='minPin(this)' title='Minimise'><div id='imageSpan'><div id='image' title='Click the image to Drop'><img src='images/icons/un pin.png' onclick='pinit(this)'>Alert "+(length+1)+"</div></div><div id='content'><p> Informations can be stored here or notifications can be viewed here depend on the user's wish</p></div></div>");
	//$(".fulldiv").draggable();
}



$("#nav li a").live('click', function(){
	$("#nav li a").each(function(){
		$(this).css('color', 'white');
		$(this).css('text-decoration', 'none');
	});
	$(this).css('color', 'yellow');
	$(this).css('text-decoration', 'underline');
	$(this).parents("li").find('a:first').css('color', 'yellow');
	$(this).parents("li").find('a:first').css('text-decoration', 'underline');
});

function OpenInNewTab(url) {
	  var win = window.open(url, '_blank');
	  win.focus();
	}

/*function toBeDone(){
	jAlert('To Be Done!', 'Information',
			function(r) {
				if (r == true){
					
					 
				}
  });
}
*/
function showReports(){
	//alert("show report");
	
	var url ="loadCannedReport.action";
	loadAction(url)
	//$('#main').load(url);
}

function loadTest(){
	var pageUrl ="search/search.jsp";
	$('#main').load(pageUrl);
	
}
var now = new Date();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") + "<span class='jclock'>";

var lastLog = "" + now.format("dddd, mmmm dS, yyyy h:MM:ss ");
var userDetails = "Admin " + "[Last Log in: " + lastLog + "]";


$('#datestr').html(divHtml);


$(function($) {
	  $('.jclock').jclock();
	});	
	
var contextpath = "<%=contextpath%>";

/*
 * var APPMODE_SAVE ="SAVE";
 var APPMODE_ADD="ADD";
 var APPMODE_DELETE="DELETE";
 */

// add module names
var STAFA_DOCUMENT="Documents";
var STAFA_DONORLIST = "DonorList";

var STAFA_DONORASSIGNMENT="DonorAssignment";
var STAFA_RECURRINGTASK="RecurringTask";
var STAFA_HOMEPAGE="SupervisorHomePage";
var STAFA_NURSEHOMEPAGE="HomePage";
var STAFA_PREPARATION="Preparation";
var STAFA_VERIFICATION="Verification";
var STAFA_COLLECTION="Collection";
var STAFA_POSTCOLLECTION="PostCollection";
var STAFA_FOLLOWUP="Followup";
var STAFA_RECIPIENTTRACKERS="RecipientTracker";

// Need to Modify
var STAFA_DONORDETAILS="DonorList";


function checkModuleRights(module,mode){
//alert(" checkAppRights1  " + module + ", " + mode );
	var rights = "";
	var data = "moduleName="+module;
	var rightsFlag = true;
	$.ajax({
        type: "POST",
        url: "getRights.action",
        data: data,
        async:false,
        success: function (result){
        	//alert(" Result " + result)
        	rights = result.moduleRights;
        //alert("rights " + rights);
         if(mode == APPMODE_READ && rights.indexOf("R") == -1 ){
        	 rightsFlag= false; 
     			//alert(errMsgNoRights + mode);
     			     		
         }else if(mode == APPMODE_ADD && rights.indexOf("A") == -1 ){
        	 rightsFlag= false; 
     			//alert(errMsgNoRights + mode);
     		     		
         }else if(mode == APPMODE_EDIT && rights.indexOf("M") == -1 ){
             
     			//alert(errMsgNoRights + mode);
     			rightsFlag= false; 
         		//alert(errMsgNoRights + " MODIFY");
         		    		
         }else if(mode == APPMODE_DELETE && rights.indexOf("D") == -1 ){
        	 rightsFlag= false; 	
        	 //alert(errMsgNoRights + mode);
     			   		
         }else if(mode == APPMODE_SAVE && rights.indexOf("M") == -1 ){
  			//alert(errMsgNoRights + mode);
  			rightsFlag= false; 
      		//alert(errMsgNoRights + " SAVE");
      		    		
      }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
        
        }

	});
	return rightsFlag;
}

function checknurseview(){

	return checkAppRights("<%=checkAccessRights(STAFA_NURSEVIEW)%>",APPMODE_READ);
	
}
function checkSupervisorView(){

	return checkAppRights("<%=checkAccessRights(STAFA_SUPERVISORVIEW)%>",APPMODE_READ);
	
}

function checkRecipientTracker(){

	return checkAppRights("<%=checkAccessRights(STAFA_RECIPIENTTRACKER)%>",APPMODE_READ);
	
}
function checkSettingsWindowRights(){

	return checkAppRights("<%=checkAccessRights(CDMR_SETTINGSVIEW)%>",APPMODE_READ);
	
}
function checkNotificationWindowRights(){
	return checkAppRights("<%=checkAccessRights(CDMR_NOTIFYVIEW)%>",APPMODE_READ);
}
function checkSettingsWindowRightsedit(){

	return checkAppRights("<%=checkAccessRights(CDMR_SETTINGSVIEW)%>",APPMODE_SAVE);
	
}

$(document).ready(function () {
	if(checkSettingsWindowRights()){
	    $("#settingsTab").css("display","inline");
	    $("#settingsspan").css("display","inline");
		}
	if(checkNotificationWindowRights()){
	    $("#notifyTab").css("display","inline");
	    $("#notifyspan").css("display","inline");
		}
	
    $.ajax({
        type: "GET",
        url: "xml/aithmenu.xml",
        dataType: "xml",
        success: xmlParser,
		complete: function( xhr, status )
		{
		  //alert( "COMPLETE.  You got:\n\n" + xhr.responseText ) ;
		  if( status == 'parsererror' )
		  {
			//alert( "There was a PARSERERROR.  Luckily, we know how to fix that.\n\n" +
				//   "The complete server response text was " + xhr.responseText ) ;

			xmlDoc = null;

			// Create the xml document from the responseText string.
			// This uses the w3schools method.
			// see also
			if (document.implementation.createDocument)
			{// code for Firefox, Mozilla, Opera, etc.
				alert("firefox");
				var xmlhttp = new window.XMLHttpRequest();
				xmlhttp.open("GET", "aithmenu.xml", false);
				xmlhttp.send(null);
				xmlDoc = xmlhttp.responseXML.documentElement;
			}			
			else  if (window.ActiveXObject) // Internet Explorer
			{
			  xmlDoc=new ActiveXObject( "Microsoft.XMLDOM" ) ;
			  xmlDoc.async = "false" ;
			  xmlDoc.loadXML( xhr.responseText ) ;
			}  else if( window.DOMParser )
			{
			  parser=new DOMParser();
			  xmlDoc=parser.parseFromString( xhr.responseText,"text/xml" ) ;
			}
			else
			{
				alert('Your browser cannot handle this script');
			} 
			$( '# ' ).append( '<p>complete event/xmlDoc: ' + xmlDoc + '</p>' ) ;
			$( '#response' ).append( '<p>complete event/status: ' + status + '</p>' ) ;

			xmlParser( xmlDoc ) ;
		  }
		},

		error: function(xmlhttp, status, error){
				 //alert(error); 
				}
    });
	   
});

function dummycall(){
	return false;
}

var userNotesId;
function xmlParser(xml) {
	var someVar='<ul id="nav" class="dropdown dropdown-horizontal">';
	var someVar1="";
	var moduleXml = "<%=userInfo.getRightsXml()%>";
	//alert("moduleXml "  +moduleXml);
		var xmlRights = "";
		var indexPos;
		var submenu="";
		var action ="";
		var dummycall = "javascript:dummycall();";
    $(xml).find("menu").each(function (i,el) {
    	 var name= $(el).find("name").text();
    	 var code= $(el).find("code").text();
    	 action =$(el).find("action").text();
    	
    //	 alert("name " + name + "/ code " +code + "/action " + action);
		var username= "<%=userInfo.getLastName()==null?"":userInfo.getLastName()+ ", "%> <%=userInfo.getFirstName()==null?"":userInfo.getFirstName()%>";
		$("#userprofile").text(username);
		userNotesId = "<%=userInfo.getUserId()%>";
		 xmlRights = "";
		indexPos = moduleXml.indexOf("<"+code+">");
		//alert(" indexPos " + indexPos + " / " + moduleXml.indexOf("</"+x+">") );
		if( indexPos >= 0){
			xmlRights = moduleXml.substr((indexPos +code.length+2), (moduleXml.indexOf("</"+code+">")-(indexPos + code.length + 2)));
		}
		
		 try{
			//	alert(moduleXml + "/indexPos"+ indexPos +"/code " + code + "/xmlRights " + xmlRights); 
			if(xmlRights.indexOf("R")!= -1){
			
				   
		    		 someVar+='<li><a href="#" onclick="';
		    		 if (action != null && action != "" && action != undefined){
		    			 someVar+= action + '" ';
		    		 }else{
		    			 someVar+= dummycall+ '" ';
		    		 }
		    				    		 
		    		 if($(el).find("submenu").length >0){
		    			 someVar+= ' class="dir" >'+name+'</a><ul>';
		    		 }else{
		    			 someVar+= '  >'+name+'</a>';
		    		 }
		    		
		    		// alert("someVar 1" + someVar);
		    		 
		    		 $(el).find("submenu").each(function (i,el1) {
		    			
		    			 xmlRights = "";
		    			 submenu = $(el1).find("submenu_name").text();
		    			 action =  $(el1).find("submenu_action").text();
		    			 code= $(el1).find("submenu_code").text();
		    			 indexPos = moduleXml.indexOf("<"+code+">");
		    				//alert(" indexPos " + indexPos + " / " + moduleXml.indexOf("</"+x+">") );
		    			 if( indexPos >= 0){
		    					xmlRights = moduleXml.substr((indexPos +code.length+2), (moduleXml.indexOf("</"+code+">")-(indexPos + code.length + 2)));
		    			 }
		    			// alert(submenu + " / " + xmlRights);
		    			 if(xmlRights.indexOf("R")!= -1){
		    				 someVar+='<li><a href="#" onclick="';
		    				 if (action != null && action != "" && action != undefined){
				    			 someVar+= action + '" ';
			 	    		}else{
			 	    			someVar+= dummycall+ '" ';
			 	    		}
		    				
		    				 if($(el1).find("submenu1").length >0){
				    			 someVar+= ' class="dir" >'+submenu+'</a><ul>';
				    		 }else{
				    			 someVar+= '  >'+submenu+'</a>';
				    		 }	
			 	    		
			 	    		 $(el1).find("submenu1").each(function (i,el2) {
			 	    			xmlRights = "";
			 	    			submenu = $(el2).find("submenu1_name").text();
			 	    			code= $(el2).find("submenu1_code").text();
			 	    			 action =  $(el2).find("submenu1_action").text();
			 	    			indexPos = moduleXml.indexOf("<"+code+">");
			    				//alert(" indexPos " + indexPos + " / " + moduleXml.indexOf("</"+x+">") );
			    			 	if( indexPos >= 0){
			    					xmlRights = moduleXml.substr((indexPos +code.length+2), (moduleXml.indexOf("</"+code+">")-(indexPos + code.length + 2)));
			    			 	}
			    			 //	alert(smenuname + " / " + xmlRights);
			    			 	 if(xmlRights.indexOf("R")!= -1){
			    			 		 someVar+='<li><a href="#" onclick="';
				    				 if (action != null && action != "" && action != undefined){
						    			 someVar+= action + '" ';
					 	    		}else{
					 	    			someVar+= dummycall+ '" ';
					 	    		}
				    				 if($(el2).find("submenu2").length >0){
						    			 someVar+= ' class="dir" >'+submenu+'</a><ul>';
						    		 }else{
						    			 someVar+= '  >'+submenu+'</a>';
						    		 }	
				    				 $(el2).find("submenu2").each(function (i,el3) {
						 	    			xmlRights = "";
						 	    			submenu = $(el3).find("submenu2_name").text();
						 	    			code= $(el3).find("submenu2_code").text();
						 	    			 action =  $(el3).find("submenu2_action").text();
						 	    			indexPos = moduleXml.indexOf("<"+code+">");
						    				//alert(" indexPos " + indexPos + " / " + moduleXml.indexOf("</"+x+">") );
						    			 	if( indexPos >= 0){
						    					xmlRights = moduleXml.substr((indexPos +code.length+2), (moduleXml.indexOf("</"+code+">")-(indexPos + code.length + 2)));
						    			 	}
						    			 //	alert(smenuname + " / " + xmlRights);
						    			 	 if(xmlRights.indexOf("R")!= -1){
						    			 		someVar+='<li><a href="#" onclick="';
							    				 if (action != null && action != "" && action != undefined){
									    			 someVar+= action + '" ';
								 	    		}else{
								 	    			someVar+= dummycall+ '" ';
								 	    		}
							    				 if($(el3).find("submenu3").length >0){
									    			 someVar+= ' class="dir" >'+submenu+'</a><ul>';
									    		 }else{
									    			 someVar+= '  >'+submenu+'</a>';
									    		 }
							    				 someVar+='</li>';
						    			 	 }
				    				 });
				    				 if($(el2).find("submenu2").length >0){
						    			 someVar+= '</ul>';
						    		 }
				 	                 someVar += '</li>';
			    			 	 }
			 	       	 	});
			 	    		 if($(el1).find("submenu1").length >0){
				    			 someVar+= '</ul>';
				    		 }
	 	            		someVar+='</li>';
		    			 }
	 	        });
		    		 if($(el).find("submenu").length >0){
		    			 someVar+= '</ul>';
		    		 }	 
		    		 
	 	        someVar+='</li>';
	 	   }
		 }catch(error){
			 alert("error " + error);
		 }
	
		
		/*  End of Admin */
    
    		
 });
    
    someVar+='</ul>';
//	alert("someVar :: "+someVar);
	$(".MegaMenu").append(someVar);
	//alert($(".MegaMenu").html());
	
	$(document).ajaxComplete(function () {
		setvalue();
	});

	 $("#MegaMenu a.main-link").hover(function(){
			$("#MegaMenu a.close").fadeIn();
			$("#MegaMenu a.main-link").removeClass("active");												 
			$(this).addClass("active");
			//var s2 = $(".active").parent().find(".sub-links");
			var submenuDisplay = $(".active").parent().hasClass(".hasSubmenu");
			//alert(submenuDisplay);
			if(submenuDisplay){
				$("#sub-link-bar").animate({
					height: "38px"
				});
				$(".active").parent().addClass("activeMenu");
			}else{
				$("#sub-link-bar").animate({
					height: "0px"					   
				});
				//$(".sub-links").hide();
				$(".sub-links").css("display","none");
				//$(".sub-links").fadeOut();

			}
			//alert("b4")
			//$(".sub-links").hide();
			//alert("s")
			//$(this).siblings(".sub-links").fadeIn();
			$(this).siblings(".sub-links").css("display","block");
			/*if($(".active").hasClass("selectedMenu")){
				alert("s");
				$(".selectedMenu").removeClass("active");
			}
			*/

		});
	 //alert("xmlparser3");
}
var dolphintabs={
	subcontainers:[], last_accessed_tab:null,

	revealsubmenu:function(curtabref){
	this.hideallsubs()
	if (this.last_accessed_tab!=null)
		//this.last_accessed_tab.className=""
	if (curtabref.getAttribute("rel")) //If there's a sub menu defined for this tab item, show it
	document.getElementById(curtabref.getAttribute("rel")).style.display="block"
	//curtabref.className="current"
	this.last_accessed_tab=curtabref
	},

	hideallsubs:function(){
	for (var i=0; i<this.subcontainers.length; i++)
		document.getElementById(this.subcontainers[i]).style.display="none"
	},


	init:function(menuId, selectedIndex){
	var tabItems=document.getElementById(menuId).getElementsByTagName("a")
		for (var i=0; i<tabItems.length; i++){
			if (tabItems[i].getAttribute("rel"))
				this.subcontainers[this.subcontainers.length]=tabItems[i].getAttribute("rel") //store id of submenu div of tab menu item
			if (i==selectedIndex){ //if this tab item should be selected by default
				//tabItems[i].className="current"
				this.revealsubmenu(tabItems[i])
			}
		tabItems[i].onmouseover=function(){
		dolphintabs.revealsubmenu(this)
		}
		} //END FOR LOOP
	}

}

/*function downloadFile(){
	$.ajax({
		url:"gethelpfile.action",
		type:"POST",
		data:null,
		success:function(response){
		}
	});
}*/
function setvalue(){
	//alert("OK");
	//alert(document.getElementById('MegaMenu').innerHTML);
        var SelfLocation = window.location.href.split('?');
        switch (SelfLocation[1]) {
            case "defined_width":
                $(".MegaMenuLink").megamenu(".MegaMenuContent", {
                    width: "86.5%"
                });
                break;
            case "auto_width_right":
                $(".MegaMenuLink").megamenu(".MegaMenuContent", {
                    justify: "left"
                });
                $('.MegaMenu').css("text-align", "left");
                break;
            case "defined_width_right":
                $(".MegaMenuLink").megamenu(".MegaMenuContent", {
                    justify: "left",
                    width: "86.5%"
                });
                $('.MegaMenu').css("text-align", "left");
                break;
            default:
                $(".MegaMenuLink").megamenu(".MegaMenuContent");
                break;
        }
    };
    
function loadmaint(pageUrl){
	$('#main').load(pageUrl, function(){
		loadmaintree();
	});
}
/* pin Notifications Starts */
var pinoff=true;
var minimise=true;
function minPin(This){
	if(minimise){
		$(This).parent().children().last().toggle();
		$(This).parent().children(":first").next().attr("src","images/icons/ic_maximize.png");
		//$("#minusimage").attr("src","images/icons/ic_maximize.png");	
		$("#minusimage").attr("title","Maximize");	
		minimise=false;
	}
	else if(minimise==false){
		$(This).parent().children().last().toggle();
		$(This).parent().children(":first").next().attr("src","images/icons/ic_minimize.png");
		//$("#minusimage").attr("src","images/icons/ic_minimize.png");
		$("#minusimage").attr("title","Minimize");	
		minimise=true;
	}
}
function closepin(This){
	
	 $(This).parent().hide("scale");
	 
}
function pinit(This){
	//alert("hi");
	var src=$(This).parent().children().attr("src");
	//alert("src"+src);
	if(src=="images/icons/pin.png"){
		//alert("move add span");
		$("#image").css("padding-top","0%");
		$("#imageSpan").append("<span></span>");
		$("div").attr("title","Click the image to Drop");
		$(This).parent().children().attr("src","images/icons/un pin.png");
		//$(This).parent().parent().parent().draggable();
		//$(".fulldiv").draggable();
		$(This).parent().parent().parent().draggable("enable");
		//$(".fulldiv").draggable();
		pinoff=true;
	}
	else if(src=="images/icons/un pin.png"){
		//alert("move remove span");
		//$("#image").next('span').remove(); 
		$("#image").css("padding-top","5%");
		$("div").attr("title","Click the image to Drag");
		$(This).parent().children().attr("src","images/icons/pin.png");
		//$("#image img").attr();
	$(This).parent().parent().parent().draggable("disable");
	//	$(".fulldiv1").draggable();
	//	$(".fulldiv").draggable("disable");
		$(This).parent().parent().parent().removeClass("ui-state-disabled");

		pinoff=false;
	}
}
//pin Notifications End 

function notesUser(){
	
	var userID =userNotesId;//$("#storageinput").val();
	var module = "Staging";
	var obj = "";
	var flag = false;
	var url="fetchNotesUser";
	var userNoteID ="USER";
	var parameters="{entityId:'"+userID+"',createdBy:'"+userID+"',entityType:'"+module+"',flag:'"+flag+"', userNoteID:'"+userNoteID+"'}";

	addNewPopup('Notes','notesContainer',url,"700","700","jsonData="+parameters);
	$('#noteView').html('');
	$("#tempEntityId").val(userID);
	$("#tempEntityType").val(module);
	$('#newNoteCreate').html('');
	$("iframe html body").html("123");
}


$.ajax({
	url:"getbannertext.action",
	type:"post",
	data:null,
	cache:false,
		success:function(response){
			
      	  if(response.import_status=='1'&&response.export_status=='0'){
      		$("#bannercontent").show();
 				$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
			}
			else if(response.export_status=='1'&& response.import_status=='0'){
				$("#bannercontent").show();
				$("#bannercontent").html("Export Interface is in progress. System slowness may occur")
			}
			else if(response.import_status=='1' && response.export_status=='1'){
				$("#bannercontent").show();
				$("#bannercontent").html("Import Interface is in progress. System slowness may occur");
				$("#bannercontent").append("<div style='background-color: yellow;'>Export Interface is in progress. System slowness may occur</div>");
			}
			else{
				$("#bannercontent").hide();
			}
       }

});

</script>

</head>

<body>
<div style="float: right;width: 500px;padding-top:5px;">
					Welcome: <span id="userprofile"></span>
					
					<a href="#" style="margin-left:60px;" onclick="notesUser();">Notes </a><span> | </span> 
				
					<a href="#" onclick="openSettings();" id="settingsTab" style="display:none">Settings </a><span id="settingsspan" style="display:none"> | </span>
					
					<a href="#"  onclick="openNotification();"id="notifyTab" style="display:none">Notification </a><span id="notifyspan" style="display:none"> | </span>
						<!-- <a href="#" onclick="toBeDone();">Notification </a><span> | </span>-->
					<a href="#" onclick="OpenInNewTab('pdf/CDMRHelp.pdf');">Help </a><span> | </span>
					<a href="javascript:signout();">Logout</a>
				</div>
				
				 <div style="height: 35px;width: 350px;">
					<div style="padding-top: 5px;width:325px;"><span style="margin-left:20px;" id="datestr"></span></div>
					<!-- <img alt="logo.png" src="images/velos/velos-logo.gif">-->
				</div>
<div class="cleaner"></div>

<div id="sub-link-bar" style="height: 0px; display: block;"> </div>
<!-- End sub-link-bar -->
    <div class="velos_navigation">

        <table id="Container" style="width:100%">
            <tr>
				<td valign="top">
                    <div id="Header">
                    </div>
					<div class="MegaMenu " id="MegaMenu">					
					</div>
				</td>
				<!-- <td>
				<input class="inputboxSearch searching1" id="search1" type="text" size="15" value="" name="search"/>
				</td> -->
			</tr>
		</table>
	


 <!-- End roundfg -->

	 </div>
  <!-- End main-handle-->

<!-- End wrap -->
<div id ="bannercontent" style="display:none"></div>
<div id="settings" style="display: none;background-color: white;">
		
</div>
<div id="notiwindow" style="display: none;background-color: white;">

</div>

</body>

</html>