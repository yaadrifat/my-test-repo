<%@ include file="common/includes.jsp"%>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<style type="text/css">
.maintenance_slidewidget{
	 border: 1px solid #d1d1d1;
     -webkit-transition: all 0.6s ease 0s;
    -moz-transition: all 0.6s ease 0s;
    -o-transition: all 0.6s ease 0s;
    transition: all 0.6s ease 0s;
    float: right;
    width: 50%;
     -webkit-border-radius: 12px;
    -moz-border-radius: 12px;
    border-radius: 12px;
    text-shadow: 0 2px 3px rgba(0, 0, 0, 0.1);
    -webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset;
    -moz-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.15) inset;
	margin-right: 2%;
  padding: 0px 22px 1px 10px;
}
</style>
<script>
function savePages(){
	try{
		if(checkValidation()){
			if($("#qcHidden").val()=="qc"){
				if(saveQc()){
					return true;
				}
			}else if($("#qcHidden").val()=="QcDetails"){
				if(updateNewQc()){
					return true;
				}
			}else if($("#qcHidden").val()=="Reagents"){
				if(saveRegent()){
					return true;
				}
			}else if($("#qcHidden").val()=="QcResult"){
				if(saveQcResult()){
					return true;
				}
			}
		}
	}catch(e){
		alert("exception " + e);
	}
}
//Update New Qc
function updateNewQc(){
	 $('.progress-indicator').css( 'display', 'block' );
	 var rowData = "";
	 var pkQcProtocol=0;
	 var codeListQcName;
	 var codeListQcCode;
	 var codeListQcDesc ;
	 var isActivate = 'N';
	 var type="qc";
	 var copyCodelst=0;
	 //commondiv,qcName,qcName,qcDescription,copyQcCode,copyQcName,qcId

			 		codeListQcName=$("#commondiv #qcName").val();
			 		codeListQcCode=$("#commondiv #qcCode").val();
			 		codeListQcDesc=$("#commondiv #qcDescription").val();
			 		 if($("#commondiv #deActivateYes").is(":checked")){
			 		    isActivate = 'Y';
			 		 }
			 		pkQcProtocol = $("#commondiv #qcId").val();
	 		if(pkQcProtocol!=undefined && codeListQcName != undefined){
		 	  rowData+= "{pkCodelst:"+pkQcProtocol+",isHide ="+isActivate+",description:'"+codeListQcName+"',subType:'"+codeListQcCode+"',comments:'"+codeListQcDesc+"',type:'"+type+"',copyCodelst:'"+copyCodelst+"'},";
	 		}
	
	 if(rowData.length >0){
           rowData = rowData.substring(0,(rowData.length-1));
       }
       url="saveCodeListQc";
      // alert(rowData);
	    response = jsonDataCall(url,"jsonData={protocolData:["+rowData+"]}");

	    //BB$("select").uniform();
	    alert("Data Upddated Sucessfully");
	    refreshSideWidget();
	    $('.progress-indicator').css( 'display', 'none' );
}


//Copy to New Qc
function copyNewQc(){
	 $('.progress-indicator').css( 'display', 'block' );
	 var rowData = "";
	 var pkQcProtocol=0;
	 var codeListQcName;
	 var codeListQcCode;
	 var codeListQcDesc ;
	 var isActivate = 'N';
	 var type="qc";
	 var copyCodelst;
	 //commondiv,qcName,qcName,qcDescription,copyQcCode,copyQcName,qcId

			 		codeListQcName=$("#commondiv #copyQcName").val();
			 		codeListQcCode=$("#commondiv #copyQcCode").val();
			 		codeListQcDesc=$("#commondiv #qcDescription").val();
			 		 if($("#commondiv #deActivateYes").is(":checked")){
			 		    isActivate = 'Y';
			 		 }
			 		copyCodelst = $("#commondiv #qcId").val();
	 		if(pkQcProtocol!=undefined && codeListQcName != undefined){
		 	  rowData+= "{pkCodelst:"+pkQcProtocol+",isHide ="+isActivate+",description:'"+codeListQcName+"',subType:'"+codeListQcCode+"',comments:'"+codeListQcDesc+"',type:'"+type+"',copyCodelst:'"+copyCodelst+"'},";
	 		}

	
	 if(rowData.length >0){
            rowData = rowData.substring(0,(rowData.length-1));
        }
        url="saveCodeListQc";
       // alert(rowData);
	    response = jsonDataCall(url,"jsonData={protocolData:["+rowData+"]}");

	    //$("#panelMaintenanceDiv").replaceWith($('#panelMaintenanceDiv', $(response)));
	    //BB$("select").uniform();
	    alert("Copied Data Saved Sucessfully");
	    refreshSideWidget();
	    $("#commondiv #copyQcName").val("");
 		$("#commondiv #copyQcCode").val("");
	    $('.progress-indicator').css( 'display', 'none' );
	    //constructTable(true);
}

/*****************New Datatable save, Delete and Edit starts here for QcResult Table******/
  function showQCDetails(divid){
	  	showDiv("commondiv");
		var breadCrum = "<a href='#' onclick='constructTable(true);'>Lab Test Library</a>  &gt;" + $("#commondiv #qcName").val() ;
		$("#breadCrumbLink").html(breadCrum); 	
  }

  function saveRegent(){
   // alert("save regents")
    var rowData = "";
  	 var regentId,regentName,regentCodeId,regentCode,regentDesc,manufacturer,lotNumber,expDate,quantity;
  	 var qcid = $("#qcId").val();
  	 var isActivate =0;
  	 $("#regentSupplies tbody tr").each(function (row){
  		 regentName = "";
  		 regentId=0;
  		 regentCodeId =0;
  		 regentCode= "";
  		 regentDesc= "";
  		 manufacturer= "";
  		 lotNumber= "";
  		 expDate= "";
  		 quantity= "";
  	 	$(this).find("td").each(function (col){
  	 	   	if(col ==0){
  	 		  regentId = $(this).find("#regent").val();
  	 	   	}else if(col ==1){	 
  	 		  regentName= $(this).find("#regentName").val();
  	 		  regentCodeId = $("#regentList option:contains('" + $.trim(regentName) + "')").val();
  	 		  if(regentCodeId == undefined){
  	 			 regentCodeId =0;
  	 		  }
  	 		 // alert("regentName>"+regentName+"<" + " / " + regentCodeId);
  	 	    }else if(col ==2){
  	 	    	regentCode=$(this).find("#regentCode").val();
  		 	 }else if(col ==3){
  		 		regentDesc=$(this).find("#regentDesc").val();
  		 		 
  		 	 }else if(col ==4){
  		 		manufacturer=$(this).find("#manufacturer").val();
  		 	 }else if(col ==5){
  		 		lotNumber=$(this).find("#lotNumber").val();
  		 	 }else if(col ==6){
  		 		//expDate=$(this).find("#expDate").val();
  		 		expDate = $(this).find("input[name=expDate]").val();
  		 		//alert(expDate);
  			 }else if(col ==7){
  				 quantity=$(this).find("#quantity").val();	
  			 }
  		  });
  	 		if(regentCodeId != 0 && regentName != undefined && regentName != ""){
  		 	  rowData+= "{reagentSuppliersId:"+regentId+",qcId:"+qcid+",fkCodelstReagent:'"+regentCodeId+"', regentName:'"+ regentName+"',regentDesc:'"+regentDesc+"',manufacturer:'"+manufacturer+"',lotNumber:'"+lotNumber+"',expDate:'"+expDate + "', qunatity:'"+quantity +"'},";
  		 	}
  	 });
  	
  	 if(rowData.length >0){
              rowData = rowData.substring(0,(rowData.length-1));
          }
        // alert( " rowData " + rowData);
         url ="saveQcRegents";
  	   response = jsonDataCall(url,"jsonData={qcRegentsData:["+rowData+"]}");
  	    // alert("Data Saved Successfully");
  	       $('.progress-indicator').css( 'display', 'none' );
  	       getRegentsSupplies();
  }



  function editRegent(regentid){
   //? if the values not in codelist need to add it runtime (code,desc ) and assign the new value  
    if(regentid ==0){
  	  var newRow = "<tr><td><input type='checkbox' id='regent' name ='regent' value='0' /></td><td><input type='text' style='width:100%;height:100%;margin:0;padding:5px;' id='regentName' name='regentName' value='' class='plain' /></td>";
  	 // newRow +="<td><input type='text' id='regentCode' name='regentCode' value='' /></td>";
  	 //  newRow += "<td><input type='text' id='regentDesc' name='regentDesc' value='' /></td>";
  	   newRow += "<td>&nbsp;</td><td>&nbsp;</td>";
  	   newRow += "<td><input style='width:100%;height:100%;margin:0;padding:5px;' type='text' id='manufacturer' name='manufacturer' value='' /></td>";
  	   newRow += "<td><input style='width:100%;height:100%;margin:0;padding:5px;' type='text' id='lotNumber' name='lotNumber' value='' /></td>";
  	   newRow += "<td><input style='width:100%;height:100%;margin:0;padding:5px;' type='text' id='expDate' name='expDate' value='' class='calDate' /></td>";
  	   newRow += "<td><input style='width:100%;height:100%;margin:0;padding:5px;' type='text' id='quantity' name='quantity' value='' /></td>";
  	   newRow += " </tr>";
  	   $("#regentSupplies").prepend(newRow); 
  	   $(".plain").each(function (){
  		   $(this).removeClass("plain");
  		   setObjectAutoComplete($(this),"regentList");
  	   });
  	   
    }else{
    	$("#regentSupplies tbody tr").each(function (row){
  		 if($(this).find("#regent").is(":checked")){
  			 $(this).find("td").each(function (col){
  				if(col ==1 ){
  					 if ($(this).find("#regentName").val()== undefined){
  						$(this).html("<input style='width:100%;height:100%;margin:0;padding:5px;' type='text' id='regentName' name='regentName' value='"+$(this).text()+ "' />");
  						setObjectAutoComplete($(this).find("#regentName"),"regentList");
  					 }
  				}else if(col ==2){
  					 if ($(this).find("#regentCode").val()== undefined){
  					// 	$(this).html("<input type='text' id='regentCode' name='regentCode' value='"+$(this).text()+ "' />");
  					 }
  				}else if(col ==3){
  					 if ($(this).find("#regentDesc").val()== undefined){
  					// 	$(this).html("<input type='text' id='regentDesc' name='regentDesc' value='"+$(this).text()+ "' />");
  					 }
  				}else if(col ==4){
  				 		if ($(this).find("#manufacturer").val()== undefined){
  				 			$(this).html("<input style='width:100%;height:100%;margin:0;padding:5px;' type='text' id='manufacturer' name='manufacturer' value='"+$(this).text()+ "' />");
  				 		}
  			 	}else if(col ==5){
  			 		if ($(this).find("#lotNumber").val()== undefined){
  			 			$(this).html("<input style='width:100%;height:100%;margin:0;padding:5px;' type='text' id='lotNumber' name='lotNumber' value='"+$(this).text()+ "' />");
  			 		}
  		 		}else if(col ==6){
  			 		if ($(this).find("#expDate").val()== undefined){
  			 			$(this).html("<input style='width:100%;height:100%;margin:0;padding:5px;' type='text' class='calDate' id='expDate' name='expDate' value='"+$(this).text()+ "' />");
  			 		}
  		 		}else if(col ==7){
  			 		if ($(this).find("#quantity").val()== undefined){
  			 			$(this).html("<input style='width:100%;height:100%;margin:0;padding:5px;' type='text' id='quantity' name='quantity' value='"+$(this).text()+ "' />");
  			 		}
  		 		}
  			 });
  		 }
  	  });
    }
    	
   
    $(".calDate").each(function (){
     	 if($(this).hasClass("hasDatepicker")){
          var this_id = $(this).attr("id"); // current inputs id
           var new_id = this_id +1; // a new id
           $(this).attr("id", new_id); // change to new id
           $(this).removeClass('hasDatepicker'); // remove hasDatepicker class
           $(this).datepicker({dateFormat: 'M dd, yy'}); // re-init datepicker
          }else{
      	   $(this).datepicker({dateFormat: 'M dd, yy'});
         }
     });
    
  }


	function addQcResult(){
		editQcResult(0);
	}
	
  function deleteQcResult(){
		// alert("Delete Rows");
	    var yes=confirm(confirm_deleteMsg);
		if(!yes){
			return;
		}
		var deletedIds = ""
		$("#qcResltTable #qcResultId:checked").each(function (row){
			deletedIds += $(this).val() + ",";
		});
		if(deletedIds.length >0){
		  deletedIds = deletedIds.substr(0,deletedIds.length-1);
		}
		jsonData = "{deletedQcResult:'"+deletedIds+"'}";
		url="deleteQCResults";
	    response = jsonDataCall(url,"jsonData="+jsonData);
	    constructQcResultTable();
	}
  function saveQcResult(){
	//  alert("save saveQcResult");
	  var rowData = "";
		 var qcResultId,qcResultName,qcResultCodeId,qcResultCode,qcResultDesc,qcResultActive;
		 var qcid = $("#qcId").val();
		 var isActivate =0;
		 $("#qcResltTable tbody tr").each(function (row){
			 qcResultId =0;
			 qcResultName ="";
			 qcResultCodeId="";
			 qcResultCode="";
			 qcResultDesc="";
			 qcResultActive=0;
		 	$(this).find("td").each(function (col){
		 	   	if(col ==0){
		 	   		qcResultId = $(this).find("#qcResultId").val();
		 	   	}else if(col ==1){	 
			 	   	qcResultName= $(this).find("#qcResultName").val();
			 	    qcResultCodeId = $("#qcresultList option:contains('" + $.trim(qcResultName) + "')").val();
			 	   
			 		  if(qcResultCodeId == undefined){
			 			 qcResultCodeId =0;
			 		  }
			 		 // alert("qcResultName>"+qcResultName+"<" + " / " + qcResultCodeId);
		 	    }else if(col ==2){
		 	    	//regentCode=$(this).find("#regentCode").val();
			 	 }else if(col ==3){
			 		//regentDesc=$(this).find("#regentDesc").val();
			 		 
			 	 }else if(col ==4){
			 		 if($(this).find("#activeyes").is(":checked")){
			 				qcResultActive = "N";
				 		 }else{
				 			qcResultActive = "Y";
				 		 }
			 	 }
			  });
		 		if(qcResultCodeId != 0 && qcResultName != undefined && qcResultName != ""){
			 	  rowData+= "{qcResultId:"+qcResultId+",fkCodelistQc:"+qcid+",fkCodelistQcresult:'"+qcResultCodeId+"', qcResultName:'"+ qcResultName+"',qcResultHide:'"+qcResultActive +"'},";
			 	}
		 });
		
		 if(rowData.length >0){
	            rowData = rowData.substring(0,(rowData.length-1));
	        }
	  //     alert( " rowData " + rowData);
	       url ="saveQcResults";
	      
		     response = jsonDataCall(url,"jsonData={qcResultData:["+rowData+"]}");
		    
		    // alert("Data Saved Successfully");
		       $('.progress-indicator').css( 'display', 'none' );
		       constructQcResultTable();
		       return true;
	       
  }
  
  
  function editQcResult(qcresultid){
	// alert("editQcResult");
	  
	  if(qcresultid ==0){
		  elementcount +=1;
		  var newRow = "<tr><td><input type='checkbox' id='qcResultId' name ='qcResultId' value='0' /></td><td><input type='text' id='qcResultName' name='qcResultName' value='' class='plain' /></td>";
		 // newRow +="<td><input type='text' id='regentCode' name='regentCode' value='' /></td>";
		 //  newRow += "<td><input type='text' id='regentDesc' name='regentDesc' value='' /></td>";
		   newRow += "<td>&nbsp;</td><td>&nbsp;</td>";
		   newRow +="<td><input type='radio' name='active_el"+elementcount+"' id='activeyes' value='Yes' checked='checked' >Yes</input>";
		   newRow += "<input type='radio' name='active_el"+elementcount+"' id='active' value='No' >No</input></td>";
		   newRow += " </tr>";
		   $("#qcResltTable").prepend(newRow); 
		   $(".plain").each(function (){
			   $(this).removeClass("plain");
			   setObjectAutoComplete($(this),"qcresultList");
		   });
		   
	  }else{
	  	$("#qcResltTable tbody tr").each(function (row){
			 if($(this).find("#qcResultId").is(":checked")){
				 $(this).find("td").each(function (col){
					if(col ==1 ){
						 if ($(this).find("#regentName").val()== undefined){
							$(this).html("<input type='text' id='qcResultName' name='qcResultName' value='"+$(this).text()+ "' />");
							setObjectAutoComplete($(this).find("#qcResultName"),"qcresultList");
						 }
					}else if(col ==2){
						 if ($(this).find("#qcResultCode").val()== undefined){
						// 	$(this).html("<input type='text' id='regentCode' name='regentCode' value='"+$(this).text()+ "' />");
						 }
					}else if(col ==3){
						 if ($(this).find("#qcResultDesc").val()== undefined){
						// 	$(this).html("<input type='text' id='regentDesc' name='regentDesc' value='"+$(this).text()+ "' />");
						 }
					}
				 });
			 }
		  });
	  }
	  
		
  }
  
  
  function showDiv(divid){
	  // BB
	  
		$("#qclibrary").addClass("hidden");
		$("#qcresult").addClass("hidden");
		$("#regentqc").addClass("hidden");
		$("#commondiv").addClass("hidden");
		$("#"+divid).removeClass("hidden");
			
  }
  




function addQcResultTable(){
		 elementcount +=1;
		 $("#qcResltTable").prepend("<tr><td><input type='checkbox' id='pkQcResult' value='0'></td><td><input type='text' id='qcResultName' name='qcResultName' /></td>"+
				 "<td><input type='text' id='qcResultCode' name='qcResultCode' /></td><td><input type='text' id='qcResultDesc' name='qcResultDesc' /></td>"+
				 "<td><input type='radio' value='Yes' checked name='active_a"+elementcount+"' id='activeyes' >Yes</input><input type='radio' value='No' name='active_a"+elementcount+"' id='active' >No</input></td></tr>");  
	}

function getQcResult(title){
	$("#qcHidden").val("QcResult");
	//alert("getQcResult ")
	$(".spanid1").html(title);
	var qcResultQcId = $("#qcId").val();
	constructQcResultTable(false,qcResultQcId);
	$("#qcresult #qcResultQcId").val(qcResultQcId);
	 
	hideShowDivs('qcresult','commondiv','regentqc','qclibrary');  
}


/*****************New Datatable save, Delete and Edit ends here for QcResult Table******/
 
function hideShowDivs(showDiv,div1,div2,div3){
	//alert("showDiv : "+showDiv);
	$("#"+showDiv).removeClass("hidden");
	$("#"+div1).addClass("hidden");
	$("#"+div2).addClass("hidden");
	$("#"+div3).addClass("hidden");
} 


var oTable1;
$(document).ready(function(){
	//changeBreadCrumb("Qc");
	show_slidewidgets('slidecolumnfilter');
	hide_slidecontrol();
	constructTable(false);
	setColumnManager('taskLibTable');
	maintColumnfilter('qclib');
	clearTracker();
	show_innernorth();
	//var oTable1=$("#taskLibTable").dataTable({"bJQueryUI": true,"bPaginate":true});
	var oTable2=$("#dailyAssign").dataTable({"bJQueryUI": true,"bPaginate":true});
	  	$("select").uniform(); 
		//setcolumnManager('taskLibTable'); 		
  });
   /*
   DataTable data
  */
  $("#taskLibTable tbody tr ").mouseenter(function(){
	  $(this).attr("style","background:#33ffff");	
	 // $(this).css("cursor","default");
  }).mouseleave(function() {
	  $(this).removeAttr("style");
	  $(this).css("cursor","default");
  });
  var code;
   
  /*$("#taskLibTable tbody tr ").click(function(){  
	  $(this).css("cursor","pointer");
  });*/
  
  var code1=null;
  function replacediv(id,code,rid){
	  $(".spanid").html(code+" Details");
	  $("#qcName").val($.trim(code));
	  /*$('div #'+id).css("display","block");
	  $('div #'+rid).replaceWith($('div #'+id));*/
	  hideShowDivs('regentqc','qcresult','commondiv','qclibrary');
  }
  
  function replaceqcDetails(id,code)  {
	//  alert("replaceQCdetails");
	$(".spanid1").html(code);
	$('#'+id).css("display","block");
  	$('#commondiv').replaceWith($('#'+id));
  }
   
jQuery.fn.blindToggle = function(speed, easing, callback) {
    var h = this.width() + parseInt(this.css('paddingLeft')) 
+  parseInt(this.css('paddingRight'));
    if(parseInt(this.css('marginLeft'))<0){
	   	   $('#forfloat_right').animate( { width: "82%" }, { queue: false, duration: 200 });	
        $('#blind').html("<<");
        return this.animate({marginLeft:0},speed, easing, callback);
        }
    else{
		  $( "#forfloat_right" ).animate( { width: "95%" }, { queue: false, duration: 250 });

        $('#blind').html(">>");
        return this.animate({marginLeft:-h},speed, easing, callback);
		   
          }
  };
  
 // var oTable13=$("#protocolLibTable").dataTable({"bJQueryUI": true,"bPaginate":true});

  function deletechildrows(id) {
	  var  oTable1= $('#'+id).dataTable();
	  $("#deleteallProtocol").attr('checked', false);
	  try {
		  
             var isChecked = $('#'+id).find('tr').length;
             var isCheckedall = $('#deleteallProtocol').attr('checked');
             if(isChecked>=1 && $('.deleterows').is(':checked')){
             if(isCheckedall){
                 var yes=confirm('Do you want delete all products ?');   
             }
             else{var yes=confirm('Do you want delete selected products?');
             }
             var rowCount = $('#'+id).find('tr').length;
          if(yes){
               $('#'+id+' tbody tr').each(function(i,v){
                      var cbox=$(v).children("td:eq(0)").find(':checkbox').is(":checked");
                      if(cbox){
                    	  oTable1.fnDeleteRow(this);
                      }
              });
           $('.deleterows').attr('checked', false);

          }
      }
           //rowCount = table.rows.length;
     
      }catch(e) {
          alert(e);
      }
      $('#deleteallProtocol').attr('checked', false);
   }

  function saveQcOld(){
	    //alert(tableToJson(document.getElementById('dataFieldLibValueTable')));
	    //var codelstparam = ["subType","description","pageUrl","isHide","maintainable","pkCodelst","type","defaultValue","codelstvalue"];
	    var columns = ["pkCodelst","checkBox","description","subType","type","sequence","defaultValue1","isHide1"];
	    var obj = getJsonFromHtmlTable(document.getElementById('taskLibTable'),columns);
	    var jsonData = "{panelData:" + JSON.stringify(obj) +"}";
		//alert(jsonData);
		return;
	    $.ajax({
	        type: "POST",
	        url: "saveDataFiledValues.action",
	        async:false,
	        data :{jsonData:jsonData},
	        dataType: 'json',
	        success: function (result){
	            //$('#recipientAlias').dialog('close');
	            //jQuery("#"+Id).dialog("close");
	            alert("Saved");
	        }
	    });
	    jQuery("#addDataField").dialog("close");
	    //alert(tableToJson($('#dataFieldLibValueTable')));
	    hideHighting('dataLibraryTbody');
	}  

  
/*****************New Datatable save, Delete and Edit starts here for taskLibTable******/
 
 function copyQc(pkCodelst,description,subType,copyCodelst,type,comments){
		//alert("copy protocol");
		$("#codeListType").val(type);
		var copyText =" - Copy";
		strBreadcrumb = "<a href='#' id='breadlevel1' onclick='constructTable(true);'>Lab Test Library</a> ";
		//constructTable(true);
		addRows(0,description+copyText,subType +copyText,comments,pkCodelst,false);
		//? 
		/*$("#taskLibTable").ajaxComplete(function(e, xhr, settings) {
			addRows(0,description+copyText,subType +copyText,comments,pkCodelst,false);
		});*/
	}
 

 function addRows(pkCodelst,description,subType,comments,copyCodelst,activeFlag,module){
	 //alert(" add Rows");
	 //restoreAll('#targetall1 tbody tr:first td');
	  var moduleDrop = "<select name='module' id='module'>"+$.trim($("#codeListModule").html())+"</select>";
	
	 elementcount +=1;
	 var newRow = "<tr><td>&nbsp;</td><td><input type='checkbox' id='pkQcProtocol' value='"+pkCodelst+"'> <input type='hidden' id='copyCodelst' name='copyCodelst' value='"+copyCodelst+"' /></td><td><input type='text' id='codeListQcName' name='codeListQcName' value='"+description+"' /></td><td>&nbsp;</td>";
	 newRow +="<td>"+moduleDrop+"</td>";  
	 newRow += "<td><input type='text' id='codeListQcDesc' name='codeListQcDesc' value='"+comments+"' /></td>";
	   newRow += "<td><input type='radio' value='Yes' name='active_a"+elementcount+"' id='activeyes';" ;
	   if(activeFlag){	
		   newRow += " checked='checked' ";
	   }   
	   newRow += " >Yes</input><input type='radio' value='No' name='active_a"+elementcount+"' id='active' " ; 
	   if(!activeFlag){	
		   newRow += " checked='checked' ";
	   }
		   newRow += "  >No</input></td></tr>";
	   $("#taskLibTable tbody[role='alert']").prepend(newRow);  

	 	$.uniform.restore('select');
		 $("select").uniform();
	   $("#taskLibTable .ColVis_MasterButton").triggerHandler("click");
	   $('.ColVis_Restore:visible').triggerHandler("click");
	   $("div.ColVis_collectionBackground").trigger("click");
	   
	   var param ="{module:'APHERESIS',page:'LAB_TEST_LIBRARY'}";
		markValidation(param);
	   
 }

 
 /* function addRows(tableId){
		 elementcount +=1;
		 $("#"+tableId).prepend("<tr><td><input type='checkbox' id='pkQcProtocol' value='0'></td><td><input type='text' id='codeListQcName' name='codeListQcName' /></td><td><input type='text' id='codeListQcCode' name='codeListQcCode' /></td><td><input type='text' id='codeListQcDesc' name='codeListQcDesc' /></td><td><input type='radio' value='Yes' name='active_a"+elementcount+"' id='activeyes' >Yes</input><input type='radio' value='No' name='active_a"+elementcount+"' id='active' >No</input></td></tr>");  
		  
	}
  */
  function deleteRows(){
		  //alert("Delete Rows");
		  var deletedIds = "";
		  try{
			  $("#taskLibTable tbody tr").each(function (row){
					 if($(this).find("#pkQcProtocol").is(":checked")){
						 deletedIds += $(this).find("#pkQcProtocol").val() + ",";
					 }
			  }); 
			  var yes=confirm(confirm_deleteMsg);
					if(!yes){
						return;
					}
			 //alert("deletedIds.length "+deletedIds.length);
			 //return;
		  if(deletedIds.length >0){
			  deletedIds = deletedIds.substr(0,deletedIds.length-1);
		  }
		  //alert("deletedIds " + deletedIds);
		  jsonData = "{deletedQcs:'"+deletedIds+"'}";
		  url="deleteCodeListQc";
	        //alert("json data " + jsonData);
	     response = jsonDataCall(url,"jsonData="+jsonData);

   		//Refresh side widgets
	    var url = "qclib";
		var result = ajaxCall(url,'');
		$("#qclib").replaceWith($('#qclib', $(result)));
	     
	     constructTable(true);
	}catch(error){
		alert("error " + error);
	}
  }

  function editRows(tableId){
		 // alert("edit Rows");
		
		  $("#"+tableId+" tbody tr").each(function (row){
			 if($(this).find("#pkQcProtocol").is(":checked")){
				 //alert("s");
				 $(this).find("td").each(function (col){
					 if(col ==2 ){
						 if ($(this).find("#codeListQcName").val()== undefined){
							$(this).html("<input type='text' id='codeListQcName' name='codeListQcName' value='"+$.trim($(this).text())+ "' />");
						 }
					 }else if(col ==3){
						 if ($(this).find("#codeListQcCode").val()== undefined){
						 	$(this).html("<input type='text' id='codeListQcCode' class='codeListCode' name='codeListQcCode' value='"+$.trim($(this).text())+ "' />");
						 }
					 } else if(col ==4){
						 if ($(this).find("#module").val()== undefined){
							 var oldModuleVal = $(this).text();
							 var moduleDrop = "<select name='module' id='module'>"+$.trim($("#codeListModule").html())+"</select>";
						 
						 	$(this).html(moduleDrop);
							$(this).find("#module").val(oldModuleVal);
							
						 	$.uniform.restore('select');
							 $("select").uniform();
						 }
					 
					 } else if(col ==5){
						 if ($(this).find("#codeListQcDesc").val()== undefined){
						 	$(this).html("<input type='text' id='codeListQcDesc' name='codeListQcDesc' value='"+$.trim($(this).text())+ "' />");
						 }
					 }
				 });
			 }
		  });
	  }
	  

  function saveQc(){
		 $('.progress-indicator').css( 'display', 'block' );
		 var rowData = "";
		 var pkQcProtocol;
		 var codeListQcName;
		 var codeListQcCode;
		 var protocolModule;
		 var copyProtocol;
		 var codeListQcDesc ;
		 var isActivate =0;
		 var type="qc";
		 var copyCodelst;
		 $("#taskLibTable").find("#codeListQcName").each(function (row){
		// $("#taskLibTable tbody tr").each(function (row){
			rowHtml =  $(this).closest("tr");
			 isActivate =0;
		 	
		 			pkQcProtocol= $(rowHtml).find("#pkQcProtocol").val();
			 		copyCodelst =$(rowHtml).find("#copyCodelst").val();
			 		if(copyCodelst == undefined){
			 			copyCodelst =0;
			 		}
			 		
				 		codeListQcName=$(rowHtml).find("#codeListQcName").val();
				 		/* if($(rowHtml).find("#codeListQcCode").hasClass("codeListCode")){
				 			codeListQcCode=$(rowHtml).find("#codeListQcCode").val();
						 	 }else if($.trim($(rowHtml).text())==""){
								 	 if(codeListQcName!=undefined){
							 			//var randomCode = codeListQcName.replace('',' ');
							 			var randomCode = codeListQcName.replace(/\s/g, '').toLowerCase();
							 			//alert("randomCode : "+randomCode);
							 			codeListQcCode=$.trim(randomCode);
								 	 }
							 }else{
								 codeListQcCode = $(rowHtml).text();
									}
				 		alert("codeListQcCode"+codeListQcCode); */
				 		
				 		if($(rowHtml).find("#codeListQcCode").hasClass("codeListCode")){
				 				codeListQcCode=$(rowHtml).find("#codeListQcCode").val();
						 	 }else if(codeListQcName!=undefined){
						 			//var randomCode = codeListQcName.replace('',' ');
						 			var randomCode = codeListQcName.replace(/\s/g, '').toLowerCase();
						 			//alert("randomCode : "+randomCode);
						 			codeListQcCode=$.trim(randomCode);
						 			//alert("codeListQcCode"+codeListQcCode);
							 	 }
				 		//codeListQcCode=$(rowHtml).find("#codeListQcCode").val();
				 		codeListQcDesc=$(rowHtml).find("#codeListQcDesc").val();
				 	
				 		 if($(rowHtml).find("#activeyes").is(":checked")){
				 		    isActivate = 'N';
				 		 }else{
				 			isActivate = 'Y';
				 		 }
				 		module = $(rowHtml).find("#module").val();
			  
		 		if(pkQcProtocol!=undefined && codeListQcName != undefined){
			 	  rowData+= "{pkCodelst:"+pkQcProtocol+",isHide ="+isActivate+",description:'"+codeListQcName+"',subType:'"+codeListQcCode+"',comments:'"+codeListQcDesc+"',type:'"+type+"',copyCodelst:'"+copyCodelst+"',module:'"+module+"'},";
		 		}
		 });
		
		 if(rowData.length >0){
	            rowData = rowData.substring(0,(rowData.length-1));
	        }
	        url="saveCodeListQc";
	        //alert(rowData);
		    response = jsonDataCall(url,"jsonData={protocolData:["+encodeURIComponent(rowData)+"]}");

		    //$("#panelMaintenanceDiv").replaceWith($('#panelMaintenanceDiv', $(response)));
		    //BB$("select").uniform();
		    alert("Data Saved Sucessfully");
		    refreshSideWidget();
		    $('.progress-indicator').css( 'display', 'none' );
		    constructTable(true);
		    return true;
	 }

	 function refreshSideWidget(){
		    //Refresh side widgets
		    var url = "qclib";
			var result = ajaxCall(url,'');
			$("#qclib").replaceWith($('#qclib', $(result)));
		 }

  function getQcDetails(pkCodelist){
	  $("#qcHidden").val("QcDetails");	
	  //get values from db.
	  	var url = "fetchCodeListQc";
	  	var ajaxResult = ajaxCallWithParam(url,pkCodelist);
	  	var breadCrum = "<a href='#' onclick='constructTable(true);'>Lab Test Library</a>  &gt;" ;
		//? need to load 2 dropdowns
		$('#regentList').empty();
		$('#qcresultList').empty();
		 $('#regentList').append('<option value="">Select</option>');
		$.each(ajaxResult.reagentlst, function(i, val) {
		    $('#regentList').append('<option value="' + val.pkCodelst+ '">' + val.description + '</option>');
		});

		 $('#qcresultList').append('<option value="">Select</option>');
			$.each(ajaxResult.qcResults, function(i, val) {
			    $('#qcresultList').append('<option value="' + val.pkCodelst+ '">' + val.description + '</option>');
			});
	  	$.each(ajaxResult.codeListValueList, function(i,value) {
	  		$("#qcId").val(value[0]);
		  	$("#qcName").val(value[3]);
		  	breadCrum += value[3];
		  	$("#qcCode").val(value[2]);
		  	$("#qcDescription").val(value[5]);
		  	if(value[4]=="Y"){
	              $("#deActivateYes").checked = false;
	              $("#deActivateNo").checked = true;
	              }
	          if(value[4]=="N"){
	              $("#deActivateYes").checked = true;
	              $("#deActivateNo").checked = false;
	              }
	  	});
		  
		/*$("#qclibrary").addClass("hidden");
		$("#commondiv").removeClass("hidden");
		*/
		$("#breadCrumbLink").html(breadCrum );
		hideShowDivs('commondiv','qcresult','regentqc','qclibrary');
	  }
var oTable;
  function constructTable(flag) {
		//alert("construct table");
	$("#qcHidden").val("qc");
		elementcount =0;
		$('.progress-indicator').css( 'display', 'block' );
		var strBreadcrumb = "<a href='#' onclick='constructTable(true);'>Lab Test Library</a>";
		$("#breadCrumbLink").html(strBreadcrumb );
		showDiv("qclibrary");	
	 	if(flag || flag =='true'){
	 	 oTable=$('#taskLibTable').dataTable().fnDestroy();
		}
	 	
		  var link ;
			try{
				oTable=$('#taskLibTable').dataTable( {
					"sPaginationType": "full_numbers",
					"bProcessing": true,
					"bServerSide": true,
					"bRetrieve" : true,
					"sDom": 'C<"clear">Rlfrtip',
					 "bDestroy": true,
					 //"sScrollX":"100%",
					 "aaSorting": [[ 1, "asc" ]],
					 "oColVis": {
							"aiExclude": [ 0 ],
							"buttonText": "&nbsp;",
							"bRestore": true,
							"sAlign": "left",
							"iOverlayFade": 25     /* To Overcome the visibility while clicking the addButton*/
						},
						"aoColumns": [{"bSortable": false},
						              {"bSortable": false},
						               null,
						               null,
						               null,
						               null,
						               null],
				  "oLanguage": {"sProcessing":""},									
					"fnServerParams": function ( aoData ) {
															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "maintenance_panelconfig_qc"});	
															
															aoData.push({"sColumns":"Protocol Name,Module,Version"});
																						
															aoData.push( {"name": "col_1_name", "value": "" } );
															aoData.push( { "name": "col_1_column", "value": ""});
															
															aoData.push( { "name": "col_2_name", "value": "lower(description)"});
															aoData.push( { "name": "col_2_column", "value": "lower(description)"});
															
															aoData.push( {"name": "col_3_name", "value": "lower(subType)" } );
															aoData.push( { "name": "col_3_column", "value": "lower(subType)"});
															
															aoData.push( {"name": "col_4_name", "value": "lower(module)" } );
															aoData.push( { "name": "col_4_column", "value": "lower(module)"});
															
															aoData.push( {"name": "col_5_name", "value": "lower(comments)" } );
															aoData.push( { "name": "col_5_column", "value": "lower(comments)"});
					},
					"sAjaxSource": 'dataSearch.action',
					"sAjaxDataProp": "filterData",
					"aoColumnDefs": [ 
					                  {		
					                	  "aTargets": [ 0], "mDataProp": function ( source, type, val ) { 
	                	 				 return "";}},
					                  {		"sTitle":"Check All <input type='checkbox'  id='deleteallProtocol' onclick='checkall(\"taskLibTable\",this)'>",
	                	 					 "aTargets": [ 1], "mDataProp": function ( source, type, val ) { 
					                	 return "<input type='checkbox' id='pkQcProtocol' name='pkQcProtocol' value='"+source.pkCodelst +"' >";}},
									  {		"sTitle":"Name",
					                		 "aTargets": [ 2], "mDataProp": function ( source, type, val ) { 
										    link = "<a href='#' onclick='javascript:getQcDetails(\""+source.pkCodelst+"\",\""+source.description+"\",\""+source.isHide+"\",\""+source.comments+"\",\""+source.type+"\",\""+source.subType+"\");'>"+source.description+"</a>";
										  return link;}},
									  {		"sTitle":"Code",
											  "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
										 	   return source.subType;
									  		}},
									  {		"sTitle":"Module",
									  		"aTargets": [ 4], "mDataProp": function ( source, type, val ) { 
										  	return source.module;
										}},
									  {		"sTitle":"Description",
									  			"aTargets": [ 5], "mDataProp": function ( source, type, val ) { 
										  		return source.comments;
										  }},
										   
									  {		"sTitle":"Activated",
											  "aTargets": [ 6], "mDataProp": function ( source, type, val ) { 
										  if(source.isHide=='N'){
											  link = "<input type='radio' name='active_"+source.pkCodelst+"' id='activeyes' value='Yes' checked='checked' >Yes</input>";
											  link+= "<input type='radio' name='active_"+source.pkCodelst+"' id='active' value='No' >No</input>";
										  }else if(source.isHide=='Y'){
											  link = "<input type='radio' name='active_"+source.pkCodelst+"' id='activeyes' value='Yes'  >Yes</input>";
											  link+= "<input type='radio' name='active_"+source.pkCodelst+"' id='active' value='No' checked='checked'>No</input>";
										  }
										  return link;}}
									],
						"fnInitComplete": function () {
								        $("#taskLibColumn").html($('.ColVis:eq(0)'));
									},
						"fnServerData": function ( sSource, aoData, fnCallback ) {
							$('.progress-indicator').css( 'display', 'block' );
							$.ajax( {
									"dataType": 'json',
									"type": "POST",
									"url": 'dataSearch.action',
									"data":aoData,
									"success":fnCallback,  
									"complete": function () {
												$('.progress-indicator').css( 'display', 'none' );
												$("select").uniform();
												}
								  } );},
						"fnDrawCallback": function() { 
							//alert("fnDrawCallback");
							//$('.progress-indicator').css( 'display', 'none' );
				      }
					} );
				jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = parseInt(no_of_pages);
				}catch(err){
					alert("error " + err);
				}
				
			//	alert("end of dt");
	} 

  /**New Datatable save, Delete and Edit ends here******/
</script>

<div class="cleaner"></div>

				<s:hidden name="qcHidden" id="qcHidden" value="%{qcHidden}"/>
					<div id="slidecolumnfilter" style="display:none"> 
					<br>
					<div class="portlet" id="panelMaintenanceDiv">
					<div class="portlet-header"><s:hidden id="moduleName" value="overnightStorage"/><s:hidden id="widgetName" value="recipient"/><s:hidden id="pkNotes" value=""/>Lab Test Library<input type="text" id="Columnfilter" class="inputboxSearch maintenance_slidewidget" onclick="maintColumnfilter('qclib')"></div>
					<div class="cleaner"></div>
						<div class="portlet-content" style="height:320px;overflow-y:auto;width:94%" >
							<table width="100%" border="0" id="qclib">
							<!-- <tr>
							<td>
							<input type="text" id="Columnfilter" class="">
							</td>
							</tr> -->
								<s:iterator value="qcmaintlst" var="rowVal" status="row">
									<s:iterator value="rowVal" var="cellValue" status="col">
										<tr>
											<td style="padding:2%" width="90%">
											<input type="hidden" id="qcPrmariyId" name="qcPrmariyId" /><a href="#" class="cursor" onclick="javascript:getQcDetails('<s:property value="pkCodelst"/>','<s:property value="description"/>','<s:property value="idHide"/>','<s:property value="comments"/>','<s:property value="type"/>','<s:property value="subType"/>');" ><s:property value="description"/></a></td>
											<td width="10%"><div><img src = "images/icons/folder.png" class="cursor" onclick="copyQc('<s:property value="pkCodelst"/>','<s:property value="description"/>','<s:property value="subType"/>','<s:property value="copyCodelst"/>','<s:property value="type"/>','<s:property value="comments"/>')"/></div></td>
										</tr>
									</s:iterator>
								</s:iterator>  
							</table>
					  </div>
					</div>
					</div>
		
<!--float window end-->

<!--right window start -->	
<span class='hidden'>
	<s:select name="codeListModule" id="codeListModule" list="moduleList" headerKey="" headerValue="Select" listKey="subType" listValue="description"/>
</span>
<!-- <div id="forfloat_right"> -->
	<div id="breadCrumbLink" ></div>
	<div class="cleaner"></div>
	<div  id="qclibrary">		
			<div  class="portlet">
				<div class="portlet-header">Lab Test Library</div>
			<form id="qcForm">
				<s:hidden id="codeListType" value="qc"/>
				<div class="portlet-content" id="childDiv">
				<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addRows(0,'','','',0,false);"/>&nbsp;&nbsp;<label  class="cursor" onclick="addRows(0,'','','',0,false);"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteRows();"/>&nbsp;&nbsp;<label class="cursor"  onclick="deleteRows();"><b>Delete</b></label>&nbsp;</div></td>
			    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" onclick="editRows('taskLibTable')"/>&nbsp;&nbsp;<label  class="cursor" onclick="editRows('taskLibTable')"><b>Edit</b></label>&nbsp;</div></td>
			    	<!-- <td style="width:20%"><div><img src = "images/icons/save3.png" style="width:16;height:16;" class="cursor" id ="" onclick="saveQc()" >&nbsp;&nbsp;<label id="schild" class="cursor" onclick="saveQc()"><b>Save</b></label>&nbsp;</div></td> -->
				</tr>
				</table>
				<div class="tableOverflowDiv">
				<table cellpadding="0" cellspacing="0" border="0" id="taskLibTable" class="display" >
					<thead>
				         <tr>
				            <th width="4%" id="taskLibColumn"></th>
							<th><s:text name="stafa.label.checkall"/><input type="checkbox" id="deleteallProtocol" onclick="checkall('taskLibTable',this)"></th>
							<th><s:text name="stafa.maintenance.label.name"/></th>
							<th><s:text name="stafa.label.code"/></th>
							 <th><s:text name="stafa.label.module"/></th>
							<th><s:text name="stafa.maintenance.label.comments"/></th>
							<th><s:text name="stafa.label.activated"/></th>
						</tr>
					</thead>
				</table>
				</div>
			 	<!--
			 	 //? Babu
				 <div id="bor" style="float:right;">
					<form>
					<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
					<tr>
					<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				 	<td><input type="button" onclick="saveQc()" value="Save"/></td> 
					</tr>
					</table>
					</form>
				</div>
			-->
		</div>
	</form>
			</div>                    	 	
	</div>
<!--  common div  start here -->
	<div class="hidden" id="commondiv" >		
		<div  class="portlet">
			<div class="portlet-header"><span class="spanid"> </span></div>			
			<div class="portlet-content">
			<form>
				<div id="table_inner_wrapper">
					<div id="left_table_wrapper">					
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
							<tr>
								<td width="50%" height="40px"><input type="hidden" id="qcId" name="qcId" /><div class="txt_align_label" align="left">Lab Test Name</div></td>
								<td width="50%" height="40px"><div align="left"><input type="text" id="qcName" value=""/></div></td>
							</tr>
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" id="qcSubType" align="left">Code</div></td>
								<td width="50%" height="40px"><div align="left"><input type="text" id="qcCode" value=""/></div></td>
							</tr>
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" id="qcComments" align="left">Description</div></td>
								<td width="50%" height="40px"><div align="left"><textarea id="qcDescription" rows="3" cols="25"></textarea></div></td>
							</tr>
							<tr>
								<td><div>&nbsp;</div></td>
							</tr>
							<tr>
								<td><div>&nbsp;</div></td>
							</tr>
							<tr>
								<td><div class="space">
									<a id="qcProtocol" class="cursor" onclick="javascript:getRegentsSupplies();">Reagents and Supplies</a></div></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>
								<%
								System.out.println("(String)moduleMap.get(moduleName) ====================================="+(String)moduleMap.get(STAFA_QCRESULT));
								if((String)moduleMap.get(STAFA_BARCODE)!=null && checkAccessRightsForFields(STAFA_BARCODE).indexOf("R") != -1  ) { %>
									<div class="space"><a id="qcProtocol" class="cursor" onclick="javascript:getQcResult('QC Results');">Lab Test Results</a></div>
								<%}%>
								</td>
								<!-- <td><div class="space"><a id="qcProtocol" class="cursor" onclick="javascript:replaceqcDetails('qcresult','QC Results');">QC Results</a></div></td>-->
							</tr>
							
							
						</table>					
						
					</div>
										
					<div id="right_table_wrapper">					
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="113%">
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label" align="left">Deactivate?</div></td>
								<td width="50%" height="40px"><div align="left"><input type="radio" id="deActivateYes" class=""/> Yes &nbsp;<input type="radio" id="deActivateNo" class="" checked/> No</div></td>
							</tr>
							<tr>
								<td width="50%" height="40px"><div class="txt_align_label1" align="center"><input type="text" id="copyQcName" placeholder="Select Lab Test"/>&nbsp;</div></td>
								<td width="50%" height="40px"><div class="" align="left"><input type="text" id="copyQcCode" placeholder="copyLabTestCode"/>&nbsp;</div></td>
							</tr>
							<tr>
								<td colspan="2" width="50%" height="40px"><div align="center"><input type="button" id="" name="" value="Copy to New LabTest" onclick="copyNewQc()"/></div></td>
							</tr>
							<tr><td colspan="2">&nbsp;</td></tr>
							<tr><td colspan="2">&nbsp;</td></tr>
							<tr><td colspan="2">&nbsp;</td></tr>
							<tr><td colspan="2">&nbsp;</td></tr>
							<tr><td colspan="2">&nbsp;</td></tr>
							<tr><td colspan="2">&nbsp;</td></tr>
							<tr>
							<td>&nbsp;</td>
									<!--  update protocol status Delete, Activated yes/no -->
								<!-- <td><input type="text" placeholder="e-Sign" value="" size="5" style="width:80px;"> &nbsp;<input type="button" value="Save" onclick="updateNewQc()"></td> -->
							</tr>
						</table>
					</div>
				</div>
				</form>			
			</div>
		</div>
	</div>

<!--  common div  end here -->
<div class="hidden" id="regentqc">		
		<div  class="portlet">
			<div class="portlet-header"><span class="spanid1"> </span></div>			
		<form>
	
			<div class="portlet-content" id="childDiv">
			<table>
			<tr>
				<td style="width:20%"><div><a onclick="addRegent(); " ><img src = "images/icons/addnew.jpg" class="cursor" />&nbsp;&nbsp;<label class="cursor"><b>Add</b></label></a>&nbsp;</div></td>
				<td style="width:20%"><div><a onclick="deleteRegent();"><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor"/>&nbsp;&nbsp;<label class="cursor"><b>Delete</b></label></a>&nbsp;</div></td>
		    	<td style="width:20%"><div><a onclick="editRegent();"><img src = "images/icons/edit.png" style="width:16;height:16;" class="cursor"/>&nbsp;&nbsp;<label class="cursor"><b>Edit</b></a></label></a>&nbsp;</div></td>
				<td style="width:20%" ><div class="hidden"><a onclick="saveRegent();"><img src = "images/icons/save3.png" style="width:16;height:16;" class="cursor" >&nbsp;&nbsp;<label class="cursor" ><b>Save</b></label></a></div><span class="hidden"><select id="regentList" class="hidden"></select></span></td>
				
			</tr>
			</table>
			<table  id="regentSupplies" cellspacing="0" cellpadding="0" border="0"  class="display" width="100%">
				<thead>
					<tr>
						<th style="width:8%" colspan="1">Check All<input type="checkbox"  id="deleteallProtocol" onclick="checkall('regentSupplies',this)"></th>
						<th colspan="1">Name</th>
						<th colspan="1">Code</th>
						<th colspan="1">Description</th>
						<th colspan="1">Manufacturer</th>
						<th colspan="1">Lot #</th>
						<th colspan="1">Exp. Date</th>
						<th width="10%" colspan="1">Quantity</th>
					</tr>
				</thead>
			<tbody>
			
			</tbody>	
			</table>
		 <!-- 	
				<div id="bor" style="float:right;">

				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="" value="Save"/></td>
				</tr>
				</table>
			</div>
		-->	
	</div>
	</form>
	</div>                    	 
	
</div>


  <div class="hidden" id="qcresult">       
        <div  class="portlet">
            <div class="portlet-header"><span class="spanid1"> </span></div>           
        <form>
   
            <div class="portlet-content" id="childDiv">
            <input type="hidden" id="qcResultQcId" value="216">
            <table><tr><td>&nbsp;</td></tr>
            <tr>
            	<td style="width:20%"><div><a onclick="addQcResult(); " ><img src = "images/icons/addnew.jpg" style="width:16;height:16;" class="cursor" />&nbsp;&nbsp;<label class="cursor"><b>Add</b></label></a>&nbsp;</div></td>
				<td style="width:20%"><div><a onclick="deleteQcResult();"><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor"/>&nbsp;&nbsp;<label class="cursor"><b>Delete</b></label></a>&nbsp;</div></td>
		    	<td style="width:20%"><div><a onclick="editQcResult();"><img src = "images/icons/edit.png" style="width:16;height:16;" class="cursor"/>&nbsp;&nbsp;<label class="cursor"><b>Edit</b></a></label></a>&nbsp;</div></td>
				<td style="width:20%"><div class="hidden"><a onclick="saveQcResult();"><img src = "images/icons/save3.png" style="width:16;height:16;" class="cursor" >&nbsp;&nbsp;<label class="cursor" ><b>Save</b></label></a></div><span class="hidden"><select id="qcresultList" class="hidden"></select></span></td>
	
            
                      <!-- //? Babu <td style="width:20%"><div><img src = "images/icons/Preview.png" style="width:16;height:16;cursor:pointer;" id ="" onclick=""/>&nbsp;&nbsp;<label id="previews"><b>Preview</b></label>&nbsp;</div></td>
             -->  
            </tr>
            </table>
            
		<table cellpadding="0" cellspacing="0" border="0" id="qcResltTable" class="display" width="100%">
				<thead>
					<tr>
						<th><s:text name="stafa.label.checkall"/><input type="checkbox"  id="deleteallProtocol" onclick="checkall('qcResltTable',this)"></th>
						<th><s:text name="stafa.maintenance.label.name"/></th>
						<th><s:text name="stafa.label.code"/></th>
						<th><s:text name="stafa.maintenance.label.comments"/></th>
						<th><s:text name="stafa.label.activated"/></th>
					</tr>
				</thead>
			</table>            
			<!-- //? babu 
			<div id="bor" style="float:right;">
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="" value="Save"/></td>
				</tr>
				</table>
			
        	</div>
        	-->	
    </div>
    </form>
    </div>                         
   
</div>
<!-- </div> -->