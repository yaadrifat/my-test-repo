<%@ taglib prefix="s" uri="/struts-tags"%>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script>
$(document).ready(function() {
//	getProcessingData();
//$('#table111').dataTable({"aaSorting": [[ 1, "desc" ]]});
	oTable = $('#washEquipmentsTable').dataTable({
		"bJQueryUI": true
		//"sPaginationType": "full_numbers"
	});
	oTable.thDatasorting('washEquipmentsTable');
	oTable = $('#washReagentsSuppliesTable').dataTable({
		"bJQueryUI": true
		//"sPaginationType": "full_numbers"
	});
	oTable.thDatasorting('washReagentsSuppliesTable');
	oTable = $('#releaseProductTable').dataTable({
		"bJQueryUI": true
		//"sPaginationType": "full_numbers"
	});
	oTable.thDatasorting('releaseProductTable');
	$("select").uniform(); 
});


$(document).ready(function() {
	// $('#right_new_wrapper').attr("style", "width: 83%");	
    show_slidewidgets('rel_washwidget');
             });


/* 
       jQuery.fn.blindToggle = function(speed, easing, callback) {
               var h = this.width() + parseInt(this.css('paddingLeft')) +  parseInt(this.css('paddingRight'));
               if(parseInt(this.css('marginLeft'))<0){
			   	   $('#forfloat_right').animate( { width: "79%" }, { queue: false, duration: 200 });	
                   $('#blind').html("<<");
                   return this.animate({marginLeft:0},speed, easing, callback);
                   }
               else{
    			  $( "#forfloat_right" ).animate( { width: "95%" }, { queue: false, duration: 250 });
     
                   $('#blind').html(">>");
                   return this.animate({marginLeft:-h},speed, easing, callback);
				   
                     }
				
             }; */
             function generateTableFromJsnList(response){
            		$.each(response.recipientDonorList, function(i,v) {
            			//alert(i+"=-="+v[0]);
            			$("#recipientName").html(v[0]);
            			$("#recipientId").html(v[1]);
            			$("#recipientDiagnosis").html(v[2]);
            			$("#recipientAboRh").html(v[3]);
            			$("#donorName").html(v[4]);
            			$("#donorId").html(v[5]);
            			$("#donorAboRh").html(v[6]);
            			$("#productId").html(v[7]);
            			$("#productType").html(v[9]);
            		 	});
            	}
            	function getProcessingData(){
            		//alert("s");
            		var url = "getOriginalProductData";
            		var productSearch = $("#productSearch").val();
            		//if(e.keyCode==13 && productSearch!=""){
            			//alert("s : "+e.keyCode);
            			var response = ajaxCall(url,"washForm");
            			//alert(response);
            			//alert(response.qcList);
            			//response = jQuery.parseJSON(response.recipientDonorList);
            			//alert(response);	
            			//alert(response.returnJsonStr);
            			generateTableFromJsnList(response);
            		//}
            		//response = jQuery.parseJSON(response.recipientDonorList);

            	}
          
            	</script>

<div class="cleaner"></div>

<!--float window-->

<!-- <div id="floatMenu">
<div id="forfloat_wind">
	<div id="box-outer">
		
		<a href="#" style="text-decoration:none" id="blind">&lt;&lt; </a> -->
				<div id="rel_washwidget" >
				
<!-- recipient start-->
					<div class="column">
					<div  class="portlet">
					<div class="portlet-header notes"><s:hidden id="moduleName" value="releasewash"/><s:hidden id="widgetName" value="recipient"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.recipient"/></div>
						<div class="portlet-content">
						<table width="100%" border="0">
						  <tr>
							<td width="50%"><s:text name="stafa.label.id"/></td>
							<td width="50%"><div id="recipientId">789564</div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.name"/></td>
							<td><div id="recipientName">John Wilson</div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientaborh"/></td>
							<td><div id="recipientAboRh">A Positive</div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientdiagnosis"/></td>
							<td><div id="recipientDiagnosis"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientrid"/></td>
							<td><div id="recipientRID"></div></td>
						  </tr>
						</table>
					</div>
					</div>
					</div>
				
<!-- recipient-->

<!-- Product start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header notes"><s:hidden id="moduleName" value="releasetwash"/><s:hidden id="widgetName" value="product"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.product"/></div>
						<div class="portlet-content">
						<table width="100%" border="0">
							<tr>
							<td width="50%"><s:text name="stafa.label.id"/></td>
							<td width="50%"><div id="productId">BMT123-3-1</div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.type"/></td>
							<td><div id="productType">HPC-A</div></td>
						  </tr>
						</table>		
					</div>
				</div>
				</div>
<!-- Product-->

<!-- Donor start-->
					<div class="column">
					<div class="portlet">
						<div class="portlet-header notes"><s:hidden id="moduleName" value="releasewash"/><s:hidden id="widgetName" value="donor"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.donor"/></div>
						<div class="portlet-content">
							<table width="100%" border="0">
								<tr>
									<td width="50%"><s:text name="stafa.label.id"/></td>
									<td width="50%"><div id="donorId">546743</div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.name"/></td>
									<td><div id="donorName">Johnson</div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.donoraborh"/></td>
									<td><div id="donorAboRh">A Positive</div></td>
								</tr>
							</table>
						</div>
					</div>
					</div>
					<!-- Donor-->
				</div>
		<!-- 	</div>
		</div>
</div> -->
<!--float window end-->


<!--right window start -->	
<form action="#" id="washForm">
<input type="hidden" name="productSearch" id="productSearch" value="BMT4657"/>
 <div class="column" id="headerids">		
		<div  class="portlet">
			<div class="portlet-header"><s:text name="stafa.label.releaseproduct"/></div>
			<div id="button_wrapper"><input type="radio" name="pool_products" id="">&nbsp;&nbsp;Pool Products&nbsp;&nbsp;&nbsp;<input type="radio" name="pool_products" id="">&nbsp;&nbsp;Single Product</div>
			<div class="portlet-content">				

			<table cellpadding="0" cellspacing="0" border="0" id="releaseProductTable" class="display">
					<thead>
						<tr>
							<th colspan="1">Product ID</th>
							<th colspan="1">Product Type</th>				
							<th colspan="1">Facility</th>
							<th colspan="1">Cryo Date</th>
							<th colspan="1">Storage Location</th>
						</tr>
						
					</thead>
					<tbody>
						<tr>
							<td><input type="checkbox" checked="checked" name="" id="">&nbsp;BMT123-3-1</td>
							<td>HPC-A</td>
							<td>CORE Lab</td>
							<td>Jan 21, 2009</td>
							<td>TD--10-a</td>
						</tr>
						<tr>
							<td><input type="checkbox" checked="checked" name="" id="">&nbsp;BMT123-3-2</td>
							<td>HPC-A</td>
							<td>CORE Lab</td>
							<td>Mar 30, 2010</td>
							<td>TD--10-b</td>
						</tr>
					</tbody>
			</table>
	</div>
	</div>		
</div>		
</form>		
</section>


<form action="#" id="login">
<div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value="releasewash"/><s:hidden id="widgetName" value="QC"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.qc"/></div>
			<!--<div id="button_wrapper"><input class="scanboxSearch" type="text" size="25" placeholder="Scan and Enter ID" name="search" /></div>-->
		<div class="portlet-content">
			<div id="table_inner_wrapper">
			<div id="left_table_wrapper">
				<div id="table_container_input">
					<div class="table_row_input1">
						<div class="table_cell_input1"><input type="checkbox" checked="checked" name="" id="">&nbsp;<a href="#">ABO/Rh</a></div>
						<div class="table_cell_input2"><input type="checkbox" checked="checked" name="" id="">&nbsp;<a href="#">Chimera</a></div>												
					</div>		
					
					<div class="table_row_input2">
						<div class="table_cell_input1"><input type="checkbox" checked="checked" name="" id="">&nbsp;<a href="#">Gram Stain</a></div>
						<div class="table_cell_input2"><input type="checkbox" checked="checked" name="" id="">&nbsp;<a href="#">Immnophenotyping</a></div>
					</div>						
				</div>
			</div>			
			
			<div id="right_table_wrapper">
				<div id="table_container_input">
					<div class="table_row_input1">
						<div class="table_cell_input1"><input type="checkbox" checked="checked" name="" id="">&nbsp;<a href="#">Differential</a></div>
						<div class="table_cell_input2"><input type="checkbox" checked="checked" name="" id="">&nbsp;<a href="#">Enumeration</a></div>												
					</div>		
					
					<div class="table_row_input2">
						<div class="table_cell_input1"><input type="checkbox" checked="checked" name="" id="">&nbsp;<a href="#">Sterility</a></div>
						<div class="table_cell_input2"><input type="checkbox" checked="checked" name="" id="">&nbsp;<a href="#">Viability</a></div>
					</div>	
					
				</div>			
				
			</div>
		</div>
			
			</div>
		</div>
	</div>
</form>


<section>

 <div class="cleaner"></div>
 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value="releasethaw"/><s:hidden id="widgetName" value="thawregsupplies"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.washregsupplies"/></div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="washReagentsSuppliesTable" class="display">
					<thead>
						<tr>
							<th colspan="1"><s:text name="stafa.label.regandandsupplies"/></th>
							<th colspan="1"><s:text name="stafa.label.manufacturer"/></th>
							<th colspan="1"><s:text name="stafa.label.lot"/></th>				
							<th colspan="1"><s:text name="stafa.label.expirationdate"/></th>
							<th colspan="1"><s:text name="stafa.label.appearanceok"/></th>
						</tr>
						
					</thead>
					<tbody>
						<tr>
							<td>1 ml Syringe</td>
							<td>BD</td>
							<td>#309602</td>
							<td>Dec 10 2011</td>
							<td align="center"><input type="checkbox" name="" checked="checked" id=""></td>
						</tr>
						<tr>
							<td>3 ml Syringe</td>
							<td>BD</td>
							<td>#309650</td>
							<td>Sep 05 2011</td>
							<td align="center"><input type="checkbox" name="" checked="checked" id=""></td>
						</tr>
						<tr>
							<td>Y-Adapter Set</td>
							<td>B Braun</td>
							<td>#SA2020Y</td>
							<td>Oct 31 2011</td>
							<td align="center"><input type="checkbox" name="" checked="checked" id=""></td>
						</tr>
						<tr>
							<td>Sample Site Coupler</td>
							<td>Baxter</td>
							<td>#4C2405</td>
							<td>Dec 16 2011</td>
							<td align="center"><input type="checkbox" name="" checked="checked" id=""></td>
						</tr>
						<tr>
							<td>16 Gauge Needle</td>
							<td>BD</td>
							<td>#305198</td>
							<td>Dec 15 2011</td>
							<td align="center"><input type="checkbox" name="" checked="checked" id=""></td>
						</tr>
						<tr>
							<td>Sterile Ziplock Bag</td>
							<td>Unknown</td>
							<td>Unknown</td>
							<td>Nov 03 2011</td>
							<td align="center"><input type="checkbox" name="" checked="checked" id=""></td>
						</tr>
						<tr>
							<td>12x75 mm Snap-Cap Tube</td>
							<td>Falcon</td>
							<td>#352058</td>
							<td>Jan 17 2012</td>
							<td align="center"><input type="checkbox" name="" checked="checked" id=""></td>
						</tr>
						<tr>
							<td>COBE 2991 Blood Processing Test</td>
							<td>Baxter</td>
							<td>#2B13248</td>
							<td>Nov 03 2011</td>
							<td align="center"><input type="checkbox" name="" checked="checked" id=""></td>
						</tr>
					</tbody>
			</table>
			<div align="right" style="float:right;">
			<form>			
				<input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>
				<input type="button" value="Next"/>
			</form>
		</div>
	</div>
	</div>
</div>
		
</section>
	
<div class="cleaner"></div>


<section>

 <div class="cleaner"></div>
 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value="releasewash"/><s:hidden id="widgetName" value="washequipment"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.washequipment"/></div>
			<div class="portlet-content">
				<table cellpadding="0" cellspacing="0" border="0" id="washEquipmentsTable" class="display">
					<thead>
						<tr>
							<th colspan="1"><s:text name="stafa.label.thawequipment"/></th>
							<th colspan="1"><s:text name="stafa.label.manufacturer"/></th>
							<th colspan="1"><s:text name="stafa.label.serial"/></th>
							<th colspan="1"><s:text name="stafa.label.expirationdate"/></th>				
							<th colspan="1"><s:text name="stafa.label.appearanceok"/></th>
						</tr>
						
					</thead>
					<tbody>
						<tr>
							<td>Electronic Balance</td>
							<td>Mettler</td>
							<td>#392-175</td>
							<td>Oct 30 2011</td>
							<td align="center"><input type="checkbox" name="" checked="checked" id=""></td>
						</tr>
						<tr>
							<td>Cell Counter</td>
							<td>Coutler JT</td>
							<td>Unknown</td>
							<td>Dec 23 2011</td>
							<td align="center"><input type="checkbox" name="" checked="checked" id=""></td>
						</tr>
						<tr>
							<td>Sterile Connecting Device</td>
							<td>Terumo TSCD SC 201A</td>
							<td>Unknown</td>
							<td>Dec 15 2011</td>
							<td align="center"><input type="checkbox" name="" checked="checked" id=""></td>
						</tr>
						<tr>
							<td>Heat Sealer</td>
							<td>Sebra</td>
							<td>Unknown</td>
							<td>Nov 03 2011</td>
							<td align="center"><input type="checkbox" name="" checked="checked" id=""></td>
						</tr>
						<tr>
							<td>I.V. Solution Pole</td>
							<td>Unknown</td>
							<td>Unknown</td>
							<td>Sep 13 2011</td>
							<td align="center"><input type="checkbox" name="" checked="checked" id=""></td>
						</tr>						
						<tr>
							<td>COBE 2991 Cell Processor</td>
							<td>Caridian BCT</td>
							<td>Unknown</td>
							<td>Jan 03 2011</td>
							<td align="center"><input type="checkbox" name="" checked="checked" id=""></td>
						</tr>
					</tbody>
			</table>
			<div align="right" style="float:right;">
			<form>			
				<input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>
				<input type="button" value="Next"/>
			</form>
		</div>
	</div>
	</div>
</div>

		
</section>


<form action="#" id="login">
<div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value="releasewash"/><s:hidden id="widgetName" value="equipmentSetup"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.equipmentsetup"/></div>
		<div class="portlet-content">
			<div id="table_inner_wrapper">
			<div id="left_inner_wrapper">
				<div id="table_container_input">
					<div class="table_row_input1">
						<div class="table_cell_input1"><s:text name="stafa.label.program"/></div>
						<div class="table_cell_input2"><select name="" id="" class="sel_loc"><option>12345</option></select></div>
					</div>					
				</div>	
			</div>
			
			<div id="middle_line_2"></div>
			
			<div id="middle_inner_wrapper">
				<div id="table_container_input">
					<div class="table_row_input1">
						<div class="table_cell_input1"><s:text name="stafa.label.speed"/></div>
						<div class="table_cell_input2"><input type="text" id="time" placeholder="3000" maxlength="100"/></div>
					</div>								
				</div>
			</div>
			
			<div id="middle_line_2"></div>
			
			<div id="right_inner_wrapper">
				<div id="table_container_input">
					<div class="table_row_input1">
						<div class="table_cell_input1"><s:text name="stafa.label.excesspressure"/></div>
						<div class="table_cell_input2"><input type="text" id="time" placeholder="8" maxlength="100"/></div>
					</div>									
				</div>
			</div>
		</div>
			
			</div>
		</div>
	</div>
	
</form>	

<div class="cleaner"></div>

<form action="#" id="login">
<div class="column">		
	<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value="releasewash"/><s:hidden id="widgetName" value="washcalc"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.postwashcalc"/></div>
		<div class="portlet-content">
				<div id="table_inner_wrapper">
					<div id="left_table_wrapper">
						<div id="table_container_input">
							<div class="table_row_input1">
								<div class="table_cell_input1"><s:text name="stafa.label.totalprodvolume"/></div>
								<div class="table_cell_input2"><input type="text" id="date" name="date" class="date_input"  value=""/></div>												
							</div>		
							
							<div class="table_row_input2">
								<div class="table_cell_input1"><s:text name="stafa.label.cellcount1(x10^6)"/></div>
								<div class="table_cell_input2"><input type="text" id="date" name="date" class="date_input"  value=""/></div>
							</div>	
							
							<div class="table_row_input3">
								<div class="table_cell_input1"><s:text name="stafa.label.cellcount2(x10^6)"/></div>
								<div class="table_cell_input2"><input type="text" id="date" name="date" class="date_input"  value=""/></div>
							</div>
							
							<div class="table_row_input3">
								<div class="table_cell_input1"><s:text name="stafa.label.tnc/kg(x10^8)"/></div>
								<div class="table_cell_input2"><input type="text" id="date" name="date" class="date_input"  value=""/></div>
							</div>												
						</div>
					</div>			
					
					<div id="right_table_wrapper">
						<div id="table_container_input">							
							<div class="table_row_input1">
								<div class="table_cell_input1"><s:text name="stafa.label.tncrecovery"/></div>
								<div class="table_cell_input2"><label class="txt_label">Automatic #</label></div>												
							</div>
							
							<div class="table_row_input1">
								<div class="table_cell_input1"><s:text name="stafa.label.cde34/kg(x10^6/kg)"/></div>
								<div class="table_cell_input2"><label class="txt_label">Automatic #</label></div>												
							</div>
							
							<div class="table_row_input1">
								<div class="table_cell_input1"><s:text name="stafa.label.cde3/kg(x10^6/kg)"/></div>
								<div class="table_cell_input2"><label class="txt_label">Automatic #</label></div>
							</div>
						</div>
					</div>
				</div>

			
			</div>
		</div>
	</div>
</form>

<section>

<div class="cleaner"></div>
		<div  style="float:right;">
		<form>
			Next Process:&nbsp;&nbsp;<select name="" id="" class="sel_process"><option>Infusion</option><option>Culture</option></select>&nbsp;
			<input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>
			<input type="button" value="Next"/>
		</form>
		</div>
		<div class="cleaner"></div>
</section>
</div>
			
<script>
$(function() {
	
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");

	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
	//$('.sortable').columnManager({listTargetID:'sortable', onClass: 'advon', offClass: 'advoff', hideInList: [1,3],
     //   saveState: true, colsHidden: [2,4]});
	//$( ".column" ).disableSelection();
});

</script>
