<%@ include file="common/includes.jsp" %>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />

<script>

function saveSelectedProducts(){
	alert("save Method");
	
	 var rowData = "";
	 var physicianTotalDose;
	 var refDonor;
	 var refCollection;
	 var tnckg10_8;
	 var cd34kg10_6;
	 var cd3kg10_6;
	 var volume;

	 $("#totalDoseTable tbody tr").each(function (row){
	 	$(this).find("td").each(function (col){
			 	   if(col==0){
			 		  physicianTotalDose= $(this).find("#physicianTotalDose").val();
				 	 }else if(col==1){
				 		tnckg10_8=$(this).text();
				 	 }else if(col==2){
				 		cd34kg10_6=$(this).text();
				 	 }else if(col==3){
				 		cd3kg10_6=$(this).text();
				 	 }else if(col==4){
				 		volume=$.trim($(this).text());
				 	 }
		  });
	 	
	 		if(volume!= undefined && volume>0){

		 	  rowData+= "{physicianTotalDose:0,tnckg10_8:"+tnckg10_8+",cd34kg10_6:"+cd34kg10_6+",cd3kg10_6:'"+cd3kg10_6+"'},";

	 		}
	 		alert(rowData);
	 });
	
	 if(rowData.length >0){
            rowData = rowData.substring(0,(rowData.length-1));
        }

	var rowData1="";
	var physSelectProduct;
	var refPhysTotalDose;
	var fkrefProductTtype;
	var collectionDate;
	var refTask;
	var refSterility;
	var tnckg10_8;
	var cd34kg10_6;
	var cd3kg10_6;
	var volume;

	 $("#availableProductsTable tbody tr").each(function (row){
		 alert($(this).find("td[5]").text());
		 alert($(this).find("td[6]").html());
		 return;
	 	/*$(this).find("td").each(function (col){
	 			if(col==0){
	 				physSelectProduct= $(this).find("#physSelectProduct").val();
			 	 }else if(col==1){
			 		fkrefProductTtype=$(this).text();
			 	 }else if(col==2){
			 		cd34kg10_6=$(this).text();
			 	 }else if(col==3){
			 		cd3kg10_6=$(this).text();
			 	 }else if(col==4){
			 		volume=$.trim($(this).text());
			 	 }else if(col==5){
			 		tnckg10_8=$(this).text();
			 	 }else if(col==6){
			 		cd34kg10_6=$(this).text();
			 	 }else if(col==7){
			 		cd3kg10_6=$(this).text();
			 	 }else if(col==8){
			 		volume=$.trim($(this).text());
			 	 }
		 	 
		 	  // alert(equipments + " / " + manufacture );
		  });*/
	 	if(volume!= undefined && volume != undefined){
		 	  //rowData1+= "{equipmentsId:0,equipments:'"+equipments+"',appearance:"+appearance+",serialNumber:"+serialNumber+",manufacturer:'"+manufacturer+"',expirationDate:'"+expirationDate+"',processing:''},";
	 		//rowData1+= "{equipmentsId:0,equipments:'"+equipments+"',appearance:'"+appearance+"',serialNumber:'"+serialNumber+"',manufacturer:'"+manufacturer+"',expirationDate:'"+expirationDate+"',processing:''},";
 		}
	 		//alert(equipments + " / " + manufacturer + " / " + rowData1 );
	 });
	 return;
		 if(rowData1.length >0){
			 rowData1 = rowData1.substring(0,(rowData1.length-1));
	     }
     
        url="saveProcessing";
		var jsonData = "jsonData={totalDose:["+rowData+"],selectedProducts:["+rowData1+"]}";
	   
		//alert(jsonData);
		$('.progress-indicator').css( 'display', 'block' );
		  response = jsonDataCall(url,jsonData);
		 
		 // alert("response "+response.returnMessage);
		  var res = response.returnMessage;
           if(res !=""){
          		 var url = res;
          		 loadAction(url);
           }
           $('.progress-indicator').css( 'display', 'none' );  
}


$(document).ready(function() {
	var donorId = $("#donorId").val();
	var recipientId = $("#recipientId").val();
	var donorName = $("#donorName").val();
	var data =recipientId+","+donorId;
	var url="loadCollectionRecipientData";
	var result = ajaxCallWithParam(url,data);
	generateTableFromJsnList(result);
	oTable = $('#totalDoseTable').dataTable({
		"sPaginationType": "full_numbers",
		"sScrollX": "100%",
		"sDom": 'C<"clear">Rlfrtip',
		"aoColumns": [ { "bSortable": false},
		               null,
		               null,
		               null,
		               null
		               ],
		 "aaSorting": [[ 1, "asc" ]],
		 "oColVis": {
				"aiExclude": [ 0 ],
				"buttonText": "&nbsp;",
				"bRestore": true,
				"sAlign": "left"
			},
		
		"fnInitComplete": function () {
				$("#totalDoseColumn").html($('.ColVis:eq(0)'));
			    }
	});	
	oTable = $('#availableProductsTable').dataTable({
		"sPaginationType": "full_numbers",
		"sScrollX": "100%",
		"sDom": 'C<"clear">Rlfrtip',
		"aoColumns": [ { "bSortable": false},
		               null,
		               null,
		               null,
		               null,
		               null,
		               null,
		               null,
		               null,
		               null
		               ],
		 "aaSorting": [[ 1, "asc" ]],
		 "oColVis": {
				"aiExclude": [ 0 ],
				"buttonText": "&nbsp;",
				"bRestore": true,
				"sAlign": "left"
			},
		
		"fnInitComplete": function () {
				$("#availableProductColumn").html($('.ColVis:eq(1)'));
			    }
	});
	$("select").uniform();
});
function generateTableFromJsnList(result){
	$.each(result.collectionRecipientList, function(i,v) {
		if(i==0){
			$("#recipient_Id").html(v[0]);
			$("#recipientName").html(v[1]);
			$("#plannedProtocol").html(v[2]);
			$("#recipientWeight").html(v[3]);
			$("#recipientDob").html(v[4]);
			$("#recipientDiagnosis").html(v[5]);
			$("#recipientAboRh").html(v[6]);
		}
		if(i==1){
			$("#donorInfo_ID").html(v[0]);
			$("#donorName").html(v[1]);
			$("#donorDob").html(v[4]);
			$("#donorAboRh").html(v[6]);
			$("#donorType").html(v[5]);
		}
		
	});
}
$(document).ready(function() {
	show_slidewidgets("selprod_slidewidget",false);
    });


jQuery.fn.blindToggle = function(speed, easing, callback) {
var h = this.width() + parseInt(this.css('paddingLeft')) +  parseInt(this.css('paddingRight'));
if(parseInt(this.css('marginLeft'))<0){
$('#forfloat_right').animate( { width: "79%" }, { queue: false, duration: 200 });	
 $('#blind').html("<<");
 return this.animate({marginLeft:0},speed, easing, callback);
 }
else{
$( "#forfloat_right" ).animate( { width: "95%" }, { queue: false, duration: 250 });
 	$('#blind').html(">>");
 	return this.animate({marginLeft:-h},speed, easing, callback);
   }
};
function calculateTotalDose(){
	var rowDose = document.getElementById("totalDoseTable").rows[1];
	var productsTable = document.getElementById("availableProductsTable");
	var tnc=0;
	var cd34=0;
	var cd3=0;
	var val=0;
	for(i=1;i<productsTable.rows.length;i++){
		var row = productsTable.rows[i];
		//alert("s"+i);
		if(productsTable.rows[i].cells[9].childNodes[0].checked){
			if(i!=5){
			tnc+=Number($.trim(row.cells[5].innerHTML));
			cd34+=Number($.trim(row.cells[6].innerHTML));
			cd3+=Number($.trim(row.cells[7].innerHTML));
			val+=Number($.trim(row.cells[8].innerHTML));
			}
		}
	}
	if($('#totalDoseTable tbody tr td').hasClass("dataTables_empty")){
		$('#totalDoseTable').dataTable().fnAddData( [
	                                                 "<input type='hidden' id='physicianTotalDose' value='0' name='physicianTotalDose' />",
	                                                 parseFloat(tnc).toFixed(2),
	                                                 parseFloat(cd34).toFixed(2),
	                                                 parseFloat(cd3).toFixed(2),
	                                                 parseFloat(val)]);
	}else{
		rowDose.cells[1].innerHTML = parseFloat(tnc).toFixed(2);
		rowDose.cells[2].innerHTML = parseFloat(cd34).toFixed(2);
		rowDose.cells[3].innerHTML = parseFloat(cd3).toFixed(2);
		rowDose.cells[4].innerHTML = parseFloat(val);
	}
}

</script>

<div class="cleaner"></div>
<!--float window-->
<!-- recipient start-->
 <div id="selprod_slidewidget" style="display:none;">
		<div  class="portlet">
			<div class="portlet-header notes">Recipient Info</div>
				<div class="portlet-content">
				<table width="225" border="0">
				  <tr>
					<td width="89">ID:</td>
					<td width="120"><div id="recipient_Id"></div></td>
				  </tr>
				  <tr>
					<td>Name:</td>
					<td><div id="recipientName"></div></td>
				  </tr>
				  <tr>
					<td>DOB:</td>
					<td><div id="recipientDob"></div></td>
				  </tr>
				  <tr>
					<td>ABO/Rh:</td>
					<td><div id="recipientAboRh"></div></td>
				  </tr>
				   <tr>
					<td>Planned Protocol:</td>
					<td><div id="plannedProtocol"></div></td>
				  </tr>
				  <tr>
					<td>Diagnosis:</td>
					<td><div id="recipientDiagnosis"></div></td>
				  </tr>
				  <tr>
					<td>Weight:</td>
					<td><div id="recipientWeight"></div></td>
				  </tr>
</table>

			</div>
			</div>
		
<!-- recipient-->

<!-- Donor start-->
			<div class="portlet">
				<div class="portlet-header notes">Donor Info</div>
				<div class="portlet-content">
					<table width="225" border="0">
						<tr>
							<td width="89">ID:</td>
							<td width="120"><div id="donorInfo_ID"></div></td>
						</tr>
						<tr>
							<td>Name:</td>
							<td><div id="donorName"></div></td>
						</tr>
						<tr>
							<td>DOB:</td>
							<td><div id="donorDob"></div></td>
						</tr>
						<tr>
							<td>ABO/Rh:</td>
							<td><div id="donorAboRh"></div></td>
						</tr>
						<tr>
							<td>Donor Type:</td>
							<td><div id="donorType"></div></td>
						</tr>
					</table>


				</div>
			</div>
<!-- Donor-->
<!--float window end-->
</div>

<div class="cleaner"></div>
<!--right window start -->	
<s:hidden id="donorId" name="donorId" value="%{donorId}"/>
<s:hidden id="donorName" name="donorName" value="%{donorName}"/>
<s:hidden id="recipientId" name="recipientId" value="%{recipientId}"/>
	
	<section>
 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes">Total Dose</div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="totalDoseTable" class="display">
					<thead>
						<tr>
							<th width="4%" id="totalDoseColumn"></th>
							<th colspan="1">TNC/kg(x10^8/kg)</th>
							<th colspan="1">CD34/kg(x10^6/kg)</th>
							<th colspan="1">CD3/kg(x10^6/kg)</th>
							<th colspan="1">Volume(mL)</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
			</table>
	</div>
	</div>	
</div>
	
 <div class="cleaner"></div>
 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header">Available Products</div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="availableProductsTable" class="display">
					<thead>
						<tr>
							<th width="4%" id="availableProductColumn"></th>
							<th colspan="1">Product Type</th>
							<th colspan="1">Collection Date</th>
							<th colspan="1">Processing</th>
							<th colspan="1">Sterility</th>				
							<th colspan="1">TNC/kg(x10^8/kg)</th>
							<th colspan="1">CD34/kg(x10^6/kg)</th>
							<th colspan="1">CD3/kg(x10^6/kg)</th>
							<th colspan="1">Volume(mL)</th>
							<th colspan="1">Select</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td> <input type='hidden' name='physSelectProduct' id='physSelectProduct'/></td>
							<td>HPC-A</td>
							<td>Jan 7, 2011</td>
							<td>Vol. Red.</td>
							<td>Negative</td>
							<td>8</td>
							<td>0.7</td>
							<td>0.4</td>
							<td>100</td>
							<td align="center"><input id="check" onclick="calculateTotalDose(this)" type="checkbox" checked="checked" /></td>
						</tr>
						<tr onclick="">
							<td></td>
							<td>HPC-A</td>
							<td>Jan 8, 2011</td>
							<td>Vol. Red.</td>
							<td>Negative</td>
							<td>7</td>
							<td>0.6</td>
							<td>0.3</td>
							<td>100</td>
							<td align="center"><input onclick="calculateTotalDose(this)"  type="checkbox" checked="checked" /></td>
						</tr>
						<tr style="color:red">
							<td></td>
							<td>HPC-A</td>
							<td>Jan 10, 2011</td>
							<td></td>
							<td>Positive</td>
							<td>4</td>
							<td>0.3</td>
							<td>0.2</td>
							<td>100</td>
							<td align="center"><input onclick="calculateTotalDose(this)" type="checkbox" /></td>
						</tr>
					</tbody>
			</table>
	</div>
	</div>	
</div>		
		
</section>	

<div class="cleaner"></div>

<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
		<table cellpadding="0" cellspacing="0" class="" border="0">
                <tr>
                <td>Next:</td>
                <td><select name="" id="" ><option>Select...</option><option>Next Donor</option><option>Next Collection Cycle</option><option>Sign-off</option></select></td>
                <td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
                <td><input type="button"  value="Save" onclick="saveSelectedProducts();"/></td> 
                </tr>
        </table>
		</form>
		</div>
		<div class="cleaner"></div>
<script>
$(function() {	
	
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open');
	});
		
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");
	

	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});

</script>

<div id="sterility" title="sterility" style="display: none">
		<b>Proceed ?</b>&nbsp;<input type="checkbox" value=""/>Y&nbsp;<input type="checkbox" value=""/>N
    </div>