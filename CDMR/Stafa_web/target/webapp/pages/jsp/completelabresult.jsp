	<%@ include file="common/includes.jsp" %>
	<%@ taglib prefix="s" uri="/struts-tags"%>
	<script type="text/javascript" src="js/dataSearch.js"></script>
	<script type="text/javascript">
	var drawCallfn = function donorLabCallBack( ){
		 //alert("dataTableJson==> " + dataTableJson);
		 try{
		var headerRow ="<tr>";
		 $(dataTableJson.headerData).each(function(i,v){
			 //alert("cDay=="+this.collectionDay);
			 headerRow += "<td class='firstRow'><input type='checkbox' id='donorLabResult' name='donorLabResult' value='"+this.donorLabResult +"' ></td>";
			 headerRow += "<td class='firstRow'>"+ this.collectionDay + "</td>";
			 headerRow += "<td class='firstRow'>"+ this.wbc + "</td>";
			 headerRow += "<td class='firstRow'>"+ this.cd34 + "</td>";
			 headerRow += "<td class='firstRow'>"+ this.cd34per + "</td>";
			 headerRow += "<td class='firstRow'>"+ this.htc + "</td>";
			 headerRow += "<td class='firstRow'>"+ this.plt + "</td>";
			 headerRow += "<td class='firstRow'>"+ this.mncper + "</td>";
			 headerRow += "<td class='firstRow'>"+ this.kPlus + "</td>";
		 });
		 headerRow +="</tr>";
		 $("#donorLabResultTable > tbody > tr:eq(0)").before(headerRow);
		 /*  $.each(dataTableJson, function(key, inK){
	       	alert(key +"/"+ inK);
	       }); */
		 }catch(err){
			 alert("error "+ err);
		 }
	}
	var drawCallProduct = function productLabCallBack(){
		//alert("drawCallProduct");
		var footerRow ="<tr>";
		 $(dataTableJson.footerData).each(function(i,v){
			// alert("i=="+i+"v== "+v+" Data::  " +v[i]);
			 footerRow += "<td colspan='2' class='firstRow'>Cumulative</td>";
			 for(k=0;k<v.length;k++){
				 //alert("i=="+i+"v== "+v+" Data::  " +v[i]);
				 if(v[k]==null){
					 v[k]=0;
				 }
				 footerRow += "<td class='firstRow'>"+ v[k] + "</td>";
				
			 }
			 footerRow += "</tr>";
		 });
		 
		$("#productLabResultTable > tbody > tr:last").after(footerRow);
	}
	$(document).ready(function(){
		$("div[id='dnrsrnpopup']").remove();
		$("#tabs").append('<div id= "dnrsrnpopup" style="margin-left:-1%;margin-top:1%"></div>');
		$("#tabs").tabs();
		var donorVal = $("#donor").val();
		var donorProcedure=$("#donorProcedure").val();
		//getDonorData();
		donorLabTable(false,donorVal,'donorLabResultTable',donorProcedure);
		productLabTable(false,donorVal,'productLabResultTable',donorProcedure);
		if(constructADTScreeningTable(true,'donorScreeningTable')){
			//alert("inside if");
			checkDataExists('donorScreeningTable');	
		}
	});
	var cd34_3="";
	
	function checkDataExists(tableId){
		//alert("tableId::"+tableId);
			var currTabClass= $('#'+tableId+' tbody tr td:nth-child(1)').attr("class");
			//alert("currTabClass::"+currTabClass);
			
			if( currTabClass=="dataTables_empty"){
				addDefaultTests(tableId);
			
			}
			//alert("end of check data ")
	}

	function addDefaultTests(tableId){
		//alert("tableId::"+tableId); 
		var testNameArray = [ "Chagas", "HBSAG",   "HBCAB",   "Hep C",    "HCVAB C",   "RPR",   "HIV-1/2","HTLV 1/2", "HIV/HCV/HBV","WESNIL"];
		var testCodeArray = ["DONCHAGT","DONHBSAGT","DONHBCAB","DONHCVABT","DONHCVABC"  ,"DONRPRT","DONHIVT","DONHTLV12","DONHIVHCVNT","DONWESTNIL"];
		//var testNameArray = ["CREATININE","GFR NON AFRICAN AMERICAN","GFR AFRICAN AMERICAN","THEOPHYLLINE","HCVAB C","RPR","HIV-1/2","HTLV 1/2","HIV/HCV/HBV","WESNIL"];
		//var testCodeArray = ["CREATI","GFRNAA","GFRAA","THEO","hcvabc","rpr","hiv1/2","htlv1/2","hivhcv","wesnil"];
		$("#"+tableId).dataTable().fnDestroy();
		 $('#'+tableId).dataTable({
			    "sPaginationType": "full_numbers",
			    "aoColumns":[{"bSortable": false},
			                   null,
			                   null,
			                   null,
			                   null,
			                   null,
			                   null
			                ],
				"aaSorting":[[ 1, "asc" ]]
		 });
		// alert("t1");
		for(i=0;i<testNameArray.length;i++){
		//	alert(i + "Name Array::"+testNameArray[i]);
			 $("#"+tableId).dataTable().fnAddData(["<input type='hidden' id='adtScreening' name='adtScreening' value='0'/>","<span id='panelSpan'><input type='text' name='panelName' id='panelName' value=''/></span>",
			                                      "<input type='hidden' name='testCode' id='testCode' value='"+testCodeArray[i]+"'/>"+testNameArray[i],"<span id='refSpan' ><input type='text' name='refNumber' id='refNumber' value='' /></span>","<span id='testDateSpan'><input type='text' name='testDate' id='testDate' class='dateEntry calDate' value=''/></span>",
			                                      "<span id='resultSpan'><select id='results' name='results'>"+$("#idmResults").html()+"</select></span>",
			                                      "<span id='unitSpan'><input type='text' name='unit' id='unit' value=''/></span>"]); 
					
		}
		// add default test W00005111
		//$("#"+tableId).dataTable().fnAddData(["","1","2","3","4","5","6"],["","1","2","3","4","5","7"]);
		
		//<td></td><td>IDM</td><td><input type='hidden' name='testCode' id='testCode' value='chages' /> Chagas </td>
//		alert("t end");
	 	addDatepicker();  
	}
	
	function constructADTScreeningTable(flag,tableId){
		//alert("constructADTScreeningTable 1");
		//alert("construct table : "+tableId+",Donor : "+donor);
		var link;
		var donor = $("#donor").val();
		var criteria ="";
		if(donor!=null && donor!=""){
			//?BB lab result common for all plannedprocedure 
				//?BBcriteria = " and donorId ="+donor+"and donorProcedure ="+donorProcedure;
			criteria = donor;
		}
		var adtScreeningServerParams = function ( aoData ) {
															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "ADT_Screening_Popup"});
															//aoData.push( { "name": "criteria", "value": criteria});
															
															aoData.push( { "name": "param_count", "value":"1"});
															aoData.push( { "name": "param_1_name", "value":":DONOR:"});
															aoData.push( { "name": "param_1_value", "value":criteria});
															
															aoData.push( {"name": "col_1_name", "value": "lower(panel_name)" } );
															aoData.push( { "name": "col_1_column", "value": "lower(panel_name)"});
															
															aoData.push( { "name": "col_2_name", "value": "lower(test_name)"});
															aoData.push( { "name": "col_2_column", "value": "lower(test_name)"});
																		
															aoData.push( {"name": "col_3_name", "value": "lower(reference_number)" } );
															aoData.push( { "name": "col_3_column", "value": "lower(reference_number)"});
															
															//aoData.push( {"name": "col_4_name", "value": "lower(test_date)" } );
															//aoData.push( { "name": "col_4_column", "value": "lower(test_date)"});
															
															aoData.push( {"name": "col_5_name", "value": "lower((select codelst_desc from er_codelst where pk_codelst = results))" } );
															aoData.push( { "name": "col_5_column", "value": "lower((select codelst_desc from er_codelst where pk_codelst = results))"});
															
															aoData.push( {"name": "col_6_name", "value": "lower(unit)" } );
															aoData.push( { "name": "col_6_column", "value": "lower(unit)"}); 
												};
		var adtScreeningaoColDef = [
				                  {		
				                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
			                	 			return "<input type='hidden' id='adtScreening' value='"+source[0]+"'/><input type='hidden' name='donor' id='donor' value='"+source[1]+"'/>";
			                	     }},
				                  {"sTitle": "Panel",
			                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) { 
			                	    		return source[2];
				                	 }},
								  {"sTitle": "Test Name",
				                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
				                	 		return source[3];
										    }},
								  {"sTitle": "Ref #",
										 "aTargets": [3], "mDataProp": function ( source, type, val ) {
				                	 		return source[4];
								  		}},
								  {"sTitle": "Test Date",
								  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
				                	 		return converHDate(source[5]);
									  }},
								  {"sTitle": "Result",
										 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
				                	 		return source[6];
									  }},
								  {"sTitle": "Units",
										 "aTargets": [6], "mDataProp": function ( source, type, val ) { 
				                	 		return source[7];
									  }}
								]; 
		var adtScreeningColManager = function () {
												$("#donorScreenColumn").html($('#donorScreeningTable_wrapper .ColVis'));
												};
		var adtScreeningaoColumn = [ { "bSortable":false},
					               null,
					               null,
					               null,
					               null,
					               null,
					               null
					             ];
		var adtScreeningDefault_sort = [[ 1, "asc" ]];
		//alert("constructADTScreeningTable 2");
		constructTable(flag,tableId,adtScreeningColManager,adtScreeningServerParams,adtScreeningaoColDef,adtScreeningaoColumn,adtScreeningDefault_sort);
		//alert("constructADTScreeningTable 3");
		addDatepicker(); 
		//alert("constructADTScreeningTable 4");
		return true;
	}
	
	function donorLabTable(flag,donor,tableId,donorProcedure) {
		//alert("construct table : "+tableId+",Donor : "+donor);
		var criteria = "";
		if(donor!=null && donor!=""){
			//? Muthu criteria = " and donorId ="+donor+"and donorProcedure ="+donorProcedure;
			criteria = " and donorId ="+donor;
		}
		var donorLabServerParams =   function ( aoData ) {
													aoData.push( { "name": "application", "value": "stafa"});
													aoData.push( { "name": "module", "value": "aphresis_DonorlabResults"});
													aoData.push( { "name": "criteria", "value": criteria});
													
													aoData.push( {"name": "col_1_name", "value": "lower(collectionDay)" } );
													aoData.push( { "name": "col_1_column", "value": "lower(collectionDay)"});
													
													aoData.push( { "name": "col_2_name", "value": "lower(wbc)"});
													aoData.push( { "name": "col_2_column", "value": "lower(wbc)"});
																
													aoData.push( {"name": "col_3_name", "value": "lower(cd34)" } );
													aoData.push( { "name": "col_3_column", "value": "lower(cd34)"});
													
													aoData.push( {"name": "col_4_name", "value": "lower(cd34per)" } );
													aoData.push( { "name": "col_4_column", "value": "lower(cd34per)"});
													
													aoData.push( {"name": "col_5_name", "value": "lower(htc)" } );
													aoData.push( { "name": "col_5_column", "value": "lower(htc)"});
													
													aoData.push( {"name": "col_6_name", "value": "lower(plt)" } );
													aoData.push( { "name": "col_6_column", "value": "lower(plt)"});
													
													aoData.push( {"name": "col_7_name", "value": "lower(mncper)" } );
													aoData.push( { "name": "col_7_column", "value": "lower(mncper)"});
													
													aoData.push( {"name": "col_8_name", "value": "lower(kPlus)" } );
													aoData.push( { "name": "col_8_column", "value": "lower(kPlus)"});
															
								};
		var donorLabaoColDef =     [
					                  {		
					                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
				                	 			return "<input type='checkbox' id='donorLabResult' name='donorLabResult' value='"+source.donorLabResult +"' >";
				                	     }},
					                  { "sTitle": "Collection Day",
				                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) { 
					                	 		return source.collectionDay;
					                	 }},
									  { "sTitle": "WBC",
					                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
											    return source.wbc;
											    }},
									  { "sTitle": ""+cd34_3,
											 "aTargets": [3], "mDataProp": function ( source, type, val ) {
										 	   return source.cd34;
									  		}},
									  { "sTitle": "%"+cd34_3,
									  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
										  		return source.cd34per;
										  }},
									  { "sTitle": "HCT",
											 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
										  		return source.htc;
										  }},
									  { "sTitle": "PLT",
											 "aTargets": [6], "mDataProp": function ( source, type, val ) { 
										  		return source.plt;
										  }},
									  { "sTitle": "%MNC",
											 "aTargets": [7], "mDataProp": function ( source, type, val ) { 
										  		return source.mncper;
										  }},
									  { "sTitle": "K+",
											 "aTargets": [8], "mDataProp": function ( source, type, val ) { 
										  		return source.kPlus;
										  }}
									];
		var donorLabColManager =  function () {
												$("#donorLabResultColumn").html($('#donorLabResultTable_wrapper .ColVis'));
												};
		var donorLabaoColumn = [ { "bSortable":false},
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null
				             ];
		var donorDefault_sort = [[ 1, "asc" ]];
		var drawBack = drawCallfn;
		constructTable(flag,tableId,donorLabColManager,donorLabServerParams,donorLabaoColDef,donorLabaoColumn,donorDefault_sort,drawBack);
	} 
	function productLabTable(flag,donor,tableId,donorProcedure){
		var criteria = "";
		if(donor!=null && donor!=""){
			//?Muthu criteria = " and donorId ="+donor+"and donorProcedure ="+donorProcedure;
			criteria = " and donorId ="+donor;
		}
		var productLabServerParams = function ( aoData ) {
															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "aphresis_ProductlabResults"});
															aoData.push( { "name": "criteria", "value": criteria});
															
															aoData.push( { "name": "param_count", "value":"1"});
															aoData.push( { "name": "param_1_name", "value":":DONOR:"});
															aoData.push( { "name": "param_1_value", "value":donor});
															
															aoData.push( {"name": "col_1_name", "value": "lower(collectionDay)" } );
															aoData.push( { "name": "col_1_column", "value": "lower(collectionDay)"});
															
															aoData.push( { "name": "col_2_name", "value": "lower(wbc)"});
															aoData.push( { "name": "col_2_column", "value": "lower(wbc)"});
																		
															aoData.push( {"name": "col_3_name", "value": "lower(hct)" } );
															aoData.push( { "name": "col_3_column", "value": "lower(hct)"});
															
															aoData.push( {"name": "col_4_name", "value": "lower(plt)" } );
															aoData.push( { "name": "col_4_column", "value": "lower(plt)"});
															
															aoData.push( {"name": "col_5_name", "value": "lower(mncper)" } );
															aoData.push( { "name": "col_5_column", "value": "lower(mncper)"});
															
															aoData.push( {"name": "col_6_name", "value": "lower(cd34)" } );
															aoData.push( { "name": "col_6_column", "value": "lower(cd34)"});
															
															/* aoData.push( {"name": "col_7_name", "value": "lower(cdyield)" } );
															aoData.push( { "name": "col_7_column", "value": "lower(cdyield)"}); */
															
															aoData.push( {"name": "col_7_name", "value": "lower(litterProcessed)" } );
															aoData.push( { "name": "col_7_column", "value": "lower(litterProcessed)"});
															
															aoData.push( {"name": "col_8_name", "value": "lower(machineVolume)" } );
															aoData.push( { "name": "col_8_column", "value": "lower(machineVolume)"});
															
															aoData.push( {"name": "col_9_name", "value": "lower(scaleVoulume)" } );
															aoData.push( { "name": "col_9_column", "value": "lower(scaleVoulume)"});
															
															/* aoData.push( {"name": "col_11_name", "value": "lower(noOfBag)" } );
															aoData.push( { "name": "col_11_column", "value": "lower(noOfBag)"}); */
												};
		var productLabaoColDef = [
				                  {		
				                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
			                	 			return "<input type='checkbox' id='productLabResult' value="+ source.productLabResult+">";
			                	     }},
				                  {"sTitle": "Collection Day",
			                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) { 
				                	 		return source.collectionDay;
				                	 }},
								  {"sTitle": "WBC",
				                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
										    return source.wbc;
										    }},
								  {"sTitle": "HCT",
										 "aTargets": [3], "mDataProp": function ( source, type, val ) {
									 	   return source.hct;
								  		}},
								  {"sTitle": "PLT",
								  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
									  		return source.plt;
									  }},
								  {"sTitle": "%MNC",
										 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
									  		return source.mncper;
									  }},
								  {"sTitle": ""+cd34_3,
										 "aTargets": [6], "mDataProp": function ( source, type, val ) { 
									  		return source.cd34;
									  }},
								 /*  {"sTitle": "CD Yield",
										 "aTargets": [7], "mDataProp": function ( source, type, val ) { 
									  		return source.cdyield;
									  }}, */
								  {"sTitle": "Liter Processed(L)",
										 "aTargets": [7], "mDataProp": function ( source, type, val ) { 
									  		return source.litterProcessed;
									  }},
								  {"sTitle": "Machine Volume(L)",
										 "aTargets": [8], "mDataProp": function ( source, type, val ) { 
									  		return source.machineVolume;
									  }},
								  {"sTitle": "Scale Volume",
										 "aTargets": [9], "mDataProp": function ( source, type, val ) { 
									  		return source.scaleVoulume;
									  }}
								  /* {"sTitle": "#of Bags",
										 "aTargets": [11], "mDataProp": function ( source, type, val ) { 
									  		return source.noOfBag;
									  }} */
								];
		var productLabColManager = function () {
												$("#productLabResultColumn").html($('#productLabResultTable_wrapper .ColVis'));
												};
		var productLabaoColumn = [ { "bSortable":false},
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null
					              
					             ];
		var productDefault_sort = [[ 1, "asc" ]];
		var productDrawBack = drawCallProduct;
		constructTable(flag,tableId,productLabColManager,productLabServerParams,productLabaoColDef,productLabaoColumn,productDefault_sort,productDrawBack);
		
	}
	function getDonorData(){
		//alert("get donordata");
		var donorProcedure = $("#donorProcedure").val();
		var donor = $("#donor").val();
		var url ="fetchDonorData";
		var jsonData = "donor:"+donor+",donorProcedure:"+donorProcedure;
			response = jsonDataCall(url,"jsonData={"+jsonData+"}");
		var donorDetails = response.donorDetails;
		var tcCheck = donorDetails.plannedProceduredesc.slice(-2);
		cd34_3="CD34";
		if(tcCheck == "TC"){
			cd34_3="CD3";
		}
		$("#h1vtype1").val(donorDetails.h1vtype1);
		$("#htclVirus1").val(donorDetails.htclVirus1);
		$("#h1vtype2").val(donorDetails.h1vtype2);
		$("#htclVirus2").val(donorDetails.htclVirus2);
		$("#hepatitisB").val(donorDetails.hepatitisB);
		$("#westnileVirus").val(donorDetails.westnileVirus);
		$("#hepatitisC").val(donorDetails.hepatitisC);
		$("#typonosoma").val(donorDetails.typonosoma);
		$("#treponemapallidum").val(donorDetails.treponemapallidum);
		$("#cmv").val(donorDetails.cmv);
	}
	</script>
	
<div id="tabs" style="overflow-y:auto">
				    <ul>
				     <li><a href="#completeLabRst">Complete Lab Result</a></li>
				      <li><a href="#dnrsrnpopup" onclick="javascript:labScreenImport();" >Lab Result</a></li>
				    </ul>
 <div id="completeLabRst" > 
<form>
<%-- 
<s:hidden id="donor" name="donor" value="%{donor}"/>
<s:hidden id="donorProcedure" name="donorProcedure" value="%{donorDetails.plannedProcedure}"/>
 --%><div class="column">       
	<div  class="portlet">
    	<div class="portlet-header ">Donor Screening Results</div>
		<div class="portlet-content">
	        <div class="tableOverflowDiv">
				<table cellpadding="0" cellspacing="0" border="1" id="donorScreeningTable" class="display" width="100%">
					<thead>
						<tr>
							<th width="4%" id="donorScreenColumn"></th>
							<th>Panel</th>
							<th>Test Name</th>
							<th>Ref #</th>
							<th>Test Date</th>
							<th>Result</th>
							<th>Units</th>
						</tr>
					</thead>
					<tbody>
												
					</tbody>
				</table>
				
			</div>
        </div>
    </div>
</div>



  				
<div style="height:5px;width:100%"></div>
<div class="column">		
	<div  class="portlet">
		<div class="portlet-header ">Donor Lab Results</div>
			<div class="portlet-content">
				<!-- <table>
					<tr>
						<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="add_donorLabResults()"/>&nbsp;&nbsp;<label  class="cursor" onclick="add_donorLabResults()"><b>Add </b></label>&nbsp;</div></td>
						<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="delete_donorLabResults();" />&nbsp;&nbsp;<label class="cursor" onclick="delete_donorLabResults();"><b>Delete</b></label>&nbsp;</div></td>
				</tr>
				</table> -->
				<div class="tableOverflowDiv">
					<table cellpadding="0" cellspacing="0" border="1" id="donorLabResultTable" class="display">
						<thead>
							<tr>
								<th width="4%" id="donorLabResultColumn"></th>
								<th>Collection Day</th>
								<th>WBC</th>
								<th>CD34</th>
								<th>%CD34</th>
								<th>HCT</th>
								<th>PLT</th>
								<th>%MNC</th>
								<th>K+</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
		</div>	
	</div>
</div>

<div style="height:5px;width:100%"></div>
<div class="column">		
	<div  class="portlet">
		<div class="portlet-header ">Product Lab Results</div>
			<div class="portlet-content">
				<!-- <table>
					<tr>
						<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="add_productLabResults()"/>&nbsp;&nbsp;<label  class="cursor" onclick="add_productLabResults()"><b>Add </b></label>&nbsp;</div></td>
						<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="delete_productLabResults();" />&nbsp;&nbsp;<label class="cursor" onclick="delete_productLabResults();"><b>Delete</b></label>&nbsp;</div></td>
					</tr>
				</table> -->
				<div class="tableOverflowDiv">
					<table cellpadding="0" cellspacing="0" border="1" id="productLabResultTable" class="display">
						<thead>
							<tr>
								<th width="4%" id="productLabResultColumn"></th>
								<th>Collection Day</th>
								<th>WBC</th>
								<th>HCT</th>
							    <th>PLT</th>
							    <th>%MNC</th>
								<th>CD34</th>
								<!-- <th>CD Yield</th> -->
								<th>Liter Processed(L)</th>
							    <th>Machine Volume(L)</th>
							    <th>Scale Volume(L)</th>
<!-- 								<th># of Bags</th>
 -->							</tr>
						</thead>
						<tbody>
													
						</tbody>
					</table>
				</div>
			</div>	
	</div>
</div>
</form>
</div>

<div id= "dnrsrnpopup" style="display:none;"></div>
			
</div>

<script>
$(function() {

    $( ".ui-dialog .portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
   

    $( ".ui-dialog .portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

    $('.ui-dialog .searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});   
});
</script>