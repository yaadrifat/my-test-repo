
<%@ include file="common/includes.jsp" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/workFlow.js"></script>
<link type="text/css" href="css/workFlow.css" rel="stylesheet" />
<script>


$(document).ready(function(){
   
});
$(function() {
    $('.jclock').jclock();
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");	
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });
    $("select").uniform();

  });
</script>
<div class="column">
<form>
    <div  class="portlet">
    <div class="portlet-header">Next Step</div>
        <div class="portlet-content">
             <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Recipient Status</td>
                    <td class="tablewapper1"><select id='currentdiagonsis'><option>Medical Management</option><option>Sent Out for More Treatment</option><option>Ineligible for Treatment</option></select></td>
                    <td class="tablewapper"></td>
                    <td class="tablewapper1"></td>
                </tr>
                <tr>
                    <td class="tablewapper">Next Step</td>
                    <td class="tablewapper1"><select id=''><option>Pre-Transplant Visit</option><option>Interim Visit</option></select></td>
                    <td class="tablewapper"></td>
                    <td class="tablewapper1"></td>
                </tr>
                <tr>
                    <td class="tablewapper">Sign Off</td>
                    <td class="tablewapper1"><input type="text" id=""/></td>
                    <td class="tablewapper">Date</td>
                    <td class="tablewapper1"><input type="text" id="" class="dateEntry"/></td>
                </tr>
               
            </table>
        </div>
    </div>
    </form>
</div>

<section>

<div class="cleaner"></div>
        <div  style="float:right;">
        <form>
            <input type="button" value="Save"/>
        </form>
        </div>
</section>
