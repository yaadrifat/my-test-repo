<%@ include file="../common/includes.jsp" %>
<script type="text/javascript" src="js/placeholder.js"></script>
<script type="text/javascript" src="js/dataSearch.js"></script>
<script type="text/javascript" src="js/ajaxfileupload.js"></script>

<script type="text/javascript">

function savePages(){
	
	try{
		if(save_Reagents('reagentsTable')){
			//return true;
		}
		if(save_Reagents('equipmentTable')){
			return true;
		}
	}catch(e){
		alert("exception " + e);
	}
}

//alert("new inv page");
var reagentsTable_MergeCols = [1,2,3,4];
var equipmentTable_MergeCols = [1,2,3,4];

//?BB
var equipmentTable_DrawCallback_old = function() {
	//alert("callback");
	/* Get the count of same value and remove redundant ones */
	var mergedCols =[1,2,3,4];
	var rowspan =1;
	var preValue="" ,curValue="",className;
	var precelValue;
	var preObj;
	//alert("trace 1");
	$('#equipmentTable tbody tr ').each( function (row) {
		//  alert("row " + row);
		  $(this).find("td").each(function (col){
			  $(this).addClass("col"+col);
			  if(col ==8){
				  $(this).attr("nowrap","nowrap");
			  }
		  });
	});
	try{
	 mergedCols.forEach(function(colNo) {
          // alert("colNo" + colNo);
               className="col"+colNo;
               rowspan =1;
               preValue ="";
               preObj = null;
      //         alert("className" + className);
               $("#equipmentTable tbody").find(("."+className)).each(function(row){
					//alert("row " + row + "/ " + preObj);
					if(row ==0){
						preObj= this;
					}
					curValue = $(this).html();
				//	alert(preValue+" == "+curValue);
				//	alert($(this.parentNode).html());
					if(preValue == curValue){
						//alert(" pre " + $(this).prev().html());
						className = "col"+(colNo-1);
						//alert( className +"/"+$(this).prev().hasClass(className));
						if(colNo != mergedCols[0] && $(this).prev().hasClass(className)){
						//	alert("not merge");
							preObj= this;
							//  preValue=null;
							 ;
						}else{
							rowspan = parseInt($(preObj).attr('rowspan'));
							// check previous td
						//	alert("before mergeRows " +row + "/rowspan " +rowspan  );
							if(rowspan>1){
								$(preObj).attr('rowspan',(rowspan+1) );
							}else{
								$(preObj).attr('rowspan',2);
							}
							this.parentNode.removeChild(this);
						}
					}else{
						preObj= this;
						  preValue=null;
					}
					preValue = curValue;
				})
		
	  });
	 }catch(err){
		 alert("Error " + err);
	 }
	// alert("end of call back");
	}
var equipmentTable_criteria = " and rs_type='EQUIPMENT'";

var equipmentTable_serverParam = function ( aoData ) {

				aoData.push( { "name": "application", "value": "stafa"});
				aoData.push( { "name": "module", "value": "Equipment_Library"});	
				aoData.push( { "name": "criteria", "value": equipmentTable_criteria});
				
				aoData.push( { "name": "col_1_name", "value": "lower(RS_NAME)" } );
				aoData.push( { "name": "col_1_column", "value": "1"});
				
				aoData.push( { "name": "col_2_name", "value": "lower(RS_CODE)"});
				aoData.push( { "name": "col_2_column", "value": "2"});
				
				aoData.push( { "name": "col_3_name", "value": "lower(RS_DESCRIPTION)"});
				aoData.push( { "name": "col_3_column", "value": "3"});
				
				aoData.push( { "name": "col_4_name", "value": "lower(RS_MANUFACTURE)"});
				aoData.push( { "name": "col_4_column", "value": "4"});
				
				aoData.push( { "name": "col_5_name", "value": "lower(RS_LOTNUMBER)"});
				aoData.push( { "name": "col_5_column", "value": "5"});
				
				aoData.push( { "name": "col_6_name", "value": "lower(RS_MHS)"});
				aoData.push( { "name": "col_6_column", "value": "10"});
				
				aoData.push( { "name": "col_7_name", "value": "lower(RS_GE)"});
				aoData.push( { "name": "col_7_column", "value": "lower(RS_GE)"});
				
				aoData.push( { "name": "col_8_name", "value": "lower(to_char(RS_EXPDATE,'Mon dd, yyyy'))"});
				aoData.push( { "name": "col_8_column", "value": "6"});
				
	};
var equipmentTable_aoColumnDef = [
		                         {"sTitle":'Check All',"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
		                             return "<input type='checkbox' id='reagentsupplies' value="+source[0]+" />";
		                             }},
		                          {    "sTitle":'Equipment',
		                                 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
		                                	
		                             return source[1];
		                             }},
		                          {    "sTitle":'Code',
		                                 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
		                                	
				                             return source[2];
		                                 
		                                    }},
		                          {    "sTitle":'Description',
		                                        "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
		                                    return source[3];
		                                  }},
		                          {    "sTitle":'Manufacturer',
		                                        "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
		                                       var link = source[4] + '<img align="right"  src = "images/icons/addnew.jpg" onclick="add_manufacturer(this,\'equipmentTable\');">'; 	
		                                    return link;
		                                  }},    
		                          {    "sTitle":'Model #',
		                                        "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
		                                    return source[5] + '<img align="right"  src = "images/icons/addnew.jpg" onclick="add_manufacturer(this,\'equipmentTable\')">';
		                                  }},   
                                  {   "sTitle":'MHS #',
	                                  "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
	                                     // return source.expDate;
	                                     return source[10];
	                              }},  
	                              {   "sTitle":'GE #',                                                     
	                                  "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
	                                     // return source.expDate;
	                                     return source[14];
	                              }},
		                          {   "sTitle":'Maintenance Due Date',
		                                  "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
		                                     // return source.expDate;
		                                     return source[6];
		                              }},
		                         
		                          {    "sTitle":'Activated',
		                                  "aTargets": [ 9], "mDataProp": function ( source, type, val ) {
		                                	  var link= '<input type="radio" id="activated_yes" name="activated_'+source[0]+'"';
		                                	  if(source[8] ==1){
		                                		  link+=' checked '; 
		                                	  }
		                                	  		                                	  
		                                	  link+='/> Y &nbsp;<input type="radio" id="activated_no" name="activated_'+source[0]+'"';
		                                	  if(source[8] !=1){
		                                		  link+=' checked '; 
		                                	  }
		                                	  link+='/> N &nbsp;';
		                                      return link;
		                              }}
		                         
		                        ];

	var equipmentTable_aoColumn = [ { "bSortable":false},
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null
				             ];	



	//?BB
	var reagentsTable_DrawCallback_old = function() {
		//alert("callback");
		/* Get the count of same value and remove redundant ones */
		var mergedCols =[1,2,3,4];
		var rowspan =1;
		var preValue="" ,curValue="",className;
		var precelValue;
		var preObj;
		//alert("trace 1");
		$('#reagentsTable tbody tr ').each( function (row) {
			//  alert("row " + row);
			  $(this).find("td").each(function (col){
				  $(this).addClass("col"+col);
				  if(col ==8){
					  $(this).attr("nowrap","nowrap");
				  }
			  });
		});
		try{
		 mergedCols.forEach(function(colNo) {
	          // alert("colNo" + colNo);
	               className="col"+colNo;
	               rowspan =1;
	               preValue ="";
	               preObj = null;
	      //         alert("className" + className);
	               $("#reagentsTable tbody").find(("."+className)).each(function(row){
						//alert("row " + row + "/ " + preObj);
						if(row ==0){
							preObj= this;
						}
						curValue = $(this).html();
					//	alert(preValue+" == "+curValue);
					//	alert($(this.parentNode).html());
						if(preValue == curValue){
							//alert(" pre " + $(this).prev().html());
							className = "col"+(colNo-1);
							//alert( className +"/"+$(this).prev().hasClass(className));
							if(colNo != mergedCols[0] && $(this).prev().hasClass(className)){
							//	alert("not merge");
								preObj= this;
								//  preValue=null;
								 ;
							}else{
								rowspan = parseInt($(preObj).attr('rowspan'));
								// check previous td
							//	alert("before mergeRows " +row + "/rowspan " +rowspan  );
								if(rowspan>1){
									$(preObj).attr('rowspan',(rowspan+1) );
								}else{
									$(preObj).attr('rowspan',2);
								}
								this.parentNode.removeChild(this);
							}
						}else{
							preObj= this;
							  preValue=null;
						}
						preValue = curValue;
					})
			
		  });
		 }catch(err){
			 alert("Error " + err);
		 }
		// alert("end of call back");
		}
	
var reagentsTable_criteria = " and rs_type='REAGENTS'";

var reagentsTable_serverParam = function ( aoData ) {

													aoData.push( { "name": "application", "value": "stafa"});
													aoData.push( { "name": "module", "value": "Equipment_Library"});	
													aoData.push( { "name": "criteria", "value": reagentsTable_criteria});
													
													aoData.push( { "name": "col_1_name", "value": "lower(RS_NAME)" } );
													aoData.push( { "name": "col_1_column", "value": "lower(RS_NAME)"});
													
													aoData.push( { "name": "col_2_name", "value": "lower(RS_CODE)"});
													aoData.push( { "name": "col_2_column", "value": "lower(RS_CODE)"});
													
													aoData.push( { "name": "col_3_name", "value": "lower(RS_DESCRIPTION)"});
													aoData.push( { "name": "col_3_column", "value": "lower(RS_DESCRIPTION)"});
													
													aoData.push( { "name": "col_4_name", "value": "lower(RS_MANUFACTURE)"});
													aoData.push( { "name": "col_4_column", "value": "lower(RS_MANUFACTURE)"});
													
													aoData.push( { "name": "col_5_name", "value": "lower(RS_LOTNUMBER)"});
													aoData.push( { "name": "col_5_column", "value": "lower(RS_LOTNUMBER)"});
													
													aoData.push( { "name": "col_6_name", "value": "lower(to_char(RS_EXPDATE,'Mon dd, yyyy'))"});
													aoData.push( { "name": "col_6_column", "value": "lower(to_char(RS_EXPDATE,'Mon dd, yyyy'))"});
													
													};
																										
												try{
													
	var reagentsTable_aoColumnDef = [
			                         {"sTitle":'Check All',"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
			                             return "<input type='checkbox' id='reagentsupplies' value="+source[0]+" />";
			                             }},
			                          {    "sTitle":'Reagents & Supplies Name',
			                                 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
			                                	
			                             return source[1];
			                             }},
			                          {    "sTitle":'Code',
			                                 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
			                                	
					                             return source[2];
			                                 
			                                    }},
			                          {    "sTitle":'Description',
			                                        "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
			                                    return source[3];
			                                  }},
			                          {    "sTitle":'Manufacturer',
			                                        "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
			                                       var link = source[4] + '<img align="right" src = "images/icons/addnew.jpg" onclick="add_manufacturer(this,\'reagentsTable\');">'; 	
			                                    return link;
			                                  }},    
			                          {    "sTitle":'Lot #',
			                                        "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
			                                    return source[5] + '<img align="right"  src = "images/icons/addnew.jpg" onclick="add_manufacturer(this,\'reagentsTable\');">';
			                                  }},       
			                                  
			                          {   "sTitle":'Exp.Date',
			                                  "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
			                                	  return source[6];
			                              }},
			                          {    "sTitle":'Certificate of Analysis',
			                                  "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
				                                  /*if(source.document>0){
			                                      	return "<a onclick=\"openPopup(\'"+source.document+"','null','index.jpg')\" href=\"#\"> View </a>";
				                                  }else{
					                                  return "";
					                                  }
				                                  */
			                                	  var documentId= source[7];
				                            	   var documentHtml=source[12]+"&nbsp;&nbsp;<input type='hidden' id='document' name='document' value='"+ documentId+"'><a href='#' onclick='openPopup(\""+documentId+"\",\""+source[13]+"\",\""+source[12]+"\")'> View </a>";
				                            	   if(source[7]==null){
				                            		   /*documentId='0';
				                            		   documentHtml="<input type='hidden' id='document' name='document' value='"+ documentId+"'><img src='images/file_loading.gif' class='uploadImage' id='fileUploadImage' style='display:none'/>"+
					                                 	 "<div id='fileDisplayName' class='fileDisplay'></div><input type='hidden' class='fileName' id='fileName' name='fileName'/><input type='hidden' class='filePathId' name='filePathId' id='filePathId' value=''/><input type='hidden' class='fileUploadContentType' name='fileUploadContentType' id='fileUploadContentType' value=''/>"+
					                                   	 "<div class='attach' style='cursor: pointer;	color: #0101DF;' id='displayAttach'></div><input style='display:block' class='fileUpload' type='file' name='fileUpload' onChange='ajaxFileUpload1(this,\""+attachFileId+"\")' id='attach'/>";
					                                   	 */
				                            		   documentHtml="";
				                            	   }
				                            	    return documentHtml;
			                              }},
			                          {    "sTitle":'Activated',
			                                  "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
			                                	  var link= '<input type="radio" id="activated_yes" name="activated_'+source[0]+'"';
			                                	  if(source[8] ==1){
			                                		  link+=' checked '; 
			                                	  }
			                                	  		                                	  
			                                	  link+='/> Y &nbsp;<input type="radio" id="activated_no" name="activated_'+source[0]+'"';
			                                	  if(source[8] !=1){
			                                		  link+=' checked '; 
			                                	  }
			                                	  link+='/> N &nbsp;';
			                                      return link;
			                              }}
			                         
			                        ];
												}catch(e){
													alert(e);
													}
	var reagentsTable_aoColumn = [ { "bSortable":false},
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null
			             ];


	var column_sort = [[ 1, "asc" ]];

	var sDomVal = '<"clear">Rlfrtip';
$(document).ready(function() {
	hide_slidewidgets();
	hide_slidecontrol();
	clearTracker();
	show_innernorth();
	
//alert("column_sort " + column_sort);
try{
	constructMergeWithoutColManager(false,"reagentsTable",reagentsTable_serverParam,reagentsTable_aoColumnDef,reagentsTable_aoColumn,column_sort,reagentsTable_MergeCols);
	constructMergeWithoutColManager(false,"equipmentTable",equipmentTable_serverParam,equipmentTable_aoColumnDef,equipmentTable_aoColumn,column_sort,equipmentTable_MergeCols);
	 //constructTableWithMerge(false,"reagentsTable",reagentsTable_serverParam,reagentsTable_aoColumnDef,reagentsTable_aoColumn,column_sort,reagentsTable_MergeCols,sDomVal);
	// constructTableWithMerge(false,"equipmentTable",equipmentTable_serverParam,equipmentTable_aoColumnDef,equipmentTable_aoColumn,column_sort,equipmentTable_MergeCols,sDomVal);
	
}catch(err){
	alert("err");
}	
	// for testing 
	/*
	var oTable = $('#reagentsTable1').dataTable({
			"sPaginationType": "full_numbers",
			"bRetrieve": true,
			"bPaginate":true,
			"aaSorting": [[ 1, "asc" ],[2,"asc"],[3,"asc"],[4,"asc"]],
				"aoColumns": [ 
			               { "bSortable":false},
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null
				             ],
				"oColVis": {
					"aiExclude": [ 0 ],
					"buttonText": "&nbsp;",
					"bRestore": true,
					"sAlign": "left"
				},
				
				"fnDrawCallback": function (left, right) {
					
					//alert("callback");
					//Get the count of same value and remove redundant ones 
					var mergedCols =[1,2,3,4];
					var rowspan =1;
					var preValue="" ,curValue="",className;
					var precelValue;
					var preObj;
					//alert("trace 1");
					try{
					 mergedCols.forEach(function(colNo) {
			              // alert("colNo" + colNo);
			                   className="col"+colNo;
			                   rowspan =1;
			                   preValue ="";
			                   preObj = null;
			                 //  alert("className" + className);
				               $("#reagentsTable tbody").find(("."+className)).each(function(row){
									//alert("row " + row + "/ " + preObj);
									if(row ==0){
										preObj= this;
									}
									curValue = $(this).html();
								//	alert(preValue+" == "+curValue);
								//	alert($(this.parentNode).html());
									if(preValue == curValue){
										//alert(" pre " + $(this).prev().html());
										className = "col"+(colNo-1);
										//alert( className +"/"+$(this).prev().hasClass(className));
										if(colNo != mergedCols[0] && $(this).prev().hasClass(className)){
										//	alert("not merge");
											preObj= this;
											//  preValue=null;
											
										}else{
											rowspan = parseInt($(preObj).attr('rowspan'));
											// check previous td
										//	alert("before mergeRows " +row + "/rowspan " +rowspan  );
											if(rowspan>1){
												$(preObj).attr('rowspan',(rowspan+1) );
											}else{
												$(preObj).attr('rowspan',2);
											}
											this.parentNode.removeChild(this);
										}
									}else{
										preObj= this;
										  preValue=null;
									}
									preValue = curValue;
								})
						
					  });
					 }catch(err){
						 alert("Error " + err);
					 }
					// alert("end of call back");
					}
		});
	 */
	

    $("select").uniform();
});




function add_manufacturer(cur,table){
	//alert("add manufacture");
	var newRow ="";
   // alert("newRow"+newRow);

	var cell = $(cur).parent();
	var cols = cell.closest("tr").children("td").index(cell);
    var rows = cell.closest("tbody").children("tr").index(cell.closest("tr"));
    var coltemp = cols;
    var rowtemp = rows;
	//alert(" rows " + rows + " /cols " + cols  );
	var tmpRow = rows; 
	
	var row  = cell.closest("tr");
	var mergedCols =[1,2,3,4];
	
		
		newRow ="<tr>";
		var rowSpanFlag = false;
		var colCell;
	//alert("before row td");
		
			//alert("c "+ c);
            // fetch all cells of this row
            //check if this cell comes before our cell
            var className;
            var tmpRows;
            var rowspan;
            $.each(mergedCols, function(i, colNo) {
           /* mergedCols.forEach(function(colNo) { */
             //  alert("colNo" + colNo);
              
	               className="col"+colNo;
	              if(!$(cell).hasClass(className) ){
	               tmpRows=0;
	               $("#"+table+" tbody").find(("."+className)).each(function(i){
	            	 // alert($(this).html() );
	            	   rowspan = parseInt($(this).attr("rowspan"));
	            	//  alert(i +"/colNo " +colNo + " /rowspan "+rowspan);
	            	  if(rowspan >1){
	            		  // alert("(tmpRows+rowspan) " + (tmpRows+rowspan) + "/ " + rows );
	            		  //?BB if( (tmpRows+rowspan)>rows){
	            		   if( (tmpRows)==rows){	   
	            		   		$(this).attr("rowspan",(rowspan+1));
	            		   		//tmpRows=-1000;
	            		   		return false;
	            		   } else if(tmpRows <rows && (tmpRows+rowspan)>rows){
			            	   $(this).attr("rowspan",(rowspan+1));
			      		   		//tmpRows=-1000;
			      		   		return false;
	            		   }else{
	            			   tmpRows = tmpRows +rowspan;
	            		   }
	            	   }else{
	            		 //  alert(" else");
	            		  
	            		   if(tmpRows==rows){
	            			//   alert("inside else rowspan");
	            			 $(this).attr("rowspan",2);
	            		   }
	            		   tmpRows = tmpRows +1;
	            	   }
	            	   
	            	 
	               });
               }
			});
           
	  		
	newRow += "<td class='col0'><input type='checkbox' name='reagentsupplies' id='reagentsupplies' value=0 /></td>";
		if ($(cell).hasClass("col4")) {
			newRow += "<td class='col4'><input type='text' name='manufacture' id='manufacture' value='' /></td>";
		}
		newRow += "<td class='col5'><input type='text' name='lotNumber' id='lotNumber' value='' /></td>";

		/*newRow+="<td class='col6'><input type='text' name='expDate' id='expDate' class='calDate' value='' /></td>";
		if(table =="reagentsTable"){
			newRow+="<td class='col7'><input type='file' name='docs' id='docs' /></td>";
		} */
		if (table == "equipmentTable") {
			newRow += "<td class='col6'><input type='text' name='mhs' id='mhs' value='' /></td>";
			newRow += "<td class='col7'><input type='text' name='ge' id='ge' value='' /></td>";
			newRow += "<td class='col8'><input type='text' name='expDate' id='expDate' value='' class='calDate' /></td>";
			newRow += "<td class='col9' nowrap='nowrap'><input type='radio' name='activated_"+tmpRow+"' id='activated_yes' />Y &nbsp;<input type='radio' name='activated_"+tmpRow+"' id='activated_no' />N</td>";
		} else if (table == "reagentsTable") {
			newRow += "<td class='col6'><input type='text' name='expDate' id='expDate' value='' class='calDate' /></td>";
			newRow += "<td class='col7'><input type='file' name='docs' id='docs' /><span><s:text name='stafa.fileupload.message'/></span></td>";
			newRow += "<td class='col8' nowrap='nowrap'><input type='radio' name='activated_"+tmpRow+"' id='activated_yes' />Y &nbsp;<input type='radio' name='activated_"+tmpRow+"' id='activated_no' />N</td>";
		}
		//newRow+="<td class='col8' nowrap='nowrap'><input type='radio' name='activated_"+tmpRow+"' id='activated_yes' />Y &nbsp;<input type='radio' name='activated_"+tmpRow+"' id='activated_no' />N</td>";

		newRow += "</tr>";
		rowspan = parseInt($(cell).attr("rowspan"));
		tmpRow = rows;
		// alert("rowspan " + rowspan + "/tmpRow"+tmpRow);
		if (rowspan > 1) {
			tmpRow = tmpRow + rowspan;
		} else {
			tmpRow = tmpRow + 1;
		}

		//tmpRow = rows+1;
		//alert(tmpRow + " / new Row " + newRow);
		$("#" + table + " tbody tr:nth-child(" + tmpRow + ")").after(newRow)
		addDatepicker();
		// alert("final rows " + rows + " /cols " + cols);

	}

	var attachFileId = 0;
	function add_Reagents(table) {
		//alert("add reagents");
		var rowCount = $('#' + table + ' tr').length + 1;
		var newRow = "<tr>"
		newRow += "<td class='col0'><input type='checkbox' name='reagentsupplies' id='reagentsupplies' value=0 /></td>";
		newRow += "<td class='col1'><input type='text' name='name' id='name'  /></td>";
		newRow += "<td class='col2'><input type='hidden' name='code' id='code'  /></td>";
		newRow += "<td class='col3'><input type='text' name='description' id='description'  /></td>";
		newRow += "<td class='col4'><input type='text' name='manufacture' id='manufacture' value='' /></td>";
		newRow += "<td class='col5'><input type='text' name='lotNumber' id='lotNumber' value='' /></td>";
		if (table == "equipmentTable") {
			newRow += "<td class='col6'><input type='text' name='mhs' id='mhs' value='' /></td>";
			newRow += "<td class='col7'><input type='text' name='ge' id='ge' value='' /></td>";
			newRow += "<td class='col8'><input type='text' name='expDate' id='expDate' value='' class='calDate' /></td>";
			newRow += "<td class='col9' nowrap='nowrap'><input type='radio' name='activated_"+rowCount+"' id='activated_yes' />Y <input type='radio' name='activated_"+rowCount+"' id='activated_no' />N</td>";
		} else if (table == "reagentsTable") {
			newRow += "<td class='col6'><input type='text' name='expDate' id='expDate' value='' class='calDate' /></td>";
			//newRow+="<td class='col7'><input type='file' name='docs' id='docs' /></td>";
			newRow += "<td class='col7'><img src='images/file_loading.gif' id='fileUploadImage"+attachFileId+"' style='display:none'/>"
					+ "<div id='fileDisplayName"+attachFileId+"'></div><input type='hidden' class='fileName' id='fileName"+attachFileId+"' name='fileName"+attachFileId+"'/><input type='hidden' class='documentId' id='documentId"+attachFileId+"' name='documentId"+attachFileId+"' value='0'/><input type='hidden' class='filePathId' name='filePathId"+attachFileId+"' id='filePathId"+attachFileId+"'/> <input type='hidden' class='fileUploadContentType' name='fileUploadContentType"+attachFileId+"' id='fileUploadContentType"+attachFileId+"'/>"
					+ "<div class='"+attachFileId+"' style='cursor: pointer;	color: #0101DF;' id='displayAttach"+attachFileId+"'></div><input size='10' style='display:block' class='"
					+ attachFileId
					+ "' type='file' name='fileUpload' onChange='ajaxFileUpload1(this,\""
					+ attachFileId
					+ "\")' id='attach"
					+ attachFileId
					+ "'/><span><s:text name='stafa.fileupload.message'/></span></td>";
			newRow += "<td class='col8' nowrap='nowrap'><input type='radio' name='activated_"+rowCount+"' id='activated_yes' />Y <input type='radio' name='activated_"+rowCount+"' id='activated_no' />N</td>";
		}
		$("#" + table + " tbody[role='alert']").prepend(newRow);
		attachFileId += 1;
		// common method to construct calendar for calDate class refer commonscript.js
		addDatepicker();
	}

	function edit_Reagents(table) {
		//alert("edit reagents");
		$("#" + table + " tbody tr")
				.each(
						function(row) {
							if ($(this).find("#reagentsupplies").is(":checked")) {
								$(this)
										.find("td")
										.each(
												function(col) {

													if ($(this)
															.hasClass('col1')) {
														if ($(this).find(
																"#name").val() == undefined) {
															$(this)
																	.html(
																			"<input type='text' name='name' id='name' value='"
																					+ $(
																							this)
																							.text()
																					+ "'/>");
														}
													} else if ($(this)
															.hasClass('col2')) {
														if ($(this).find(
																"#code").val() == undefined) {
															$(this)
																	.html(
																			"<input type='text' name='code' id='code' value='"
																					+ $(
																							this)
																							.text()
																					+ "'/>");
														}
													} else if ($(this)
															.hasClass('col3')) {
														if ($(this).find(
																"#description")
																.val() == undefined) {
															$(this)
																	.html(
																			"<input type='text' name='description' id='description' value='"
																					+ $(
																							this)
																							.text()
																					+ "'/>");
														}
													} else if ($(this)
															.hasClass('col4')) {
														if ($(this).find(
																"#manufacture")
																.val() == undefined) {
															$(this)
																	.html(
																			"<input type='text' name='manufacture' id='manufacture' value='"
																					+ $(
																							this)
																							.text()
																					+ "'/>");
														}
													} else if ($(this)
															.hasClass('col5')) {
														if ($(this).find(
																"#lotNumber")
																.val() == undefined) {
															$(this)
																	.html(
																			"<input type='text' name='lotNumber' id='lotNumber' value='"
																					+ $(
																							this)
																							.text()
																					+ "' />");
														}
													} else if ($(this)
															.hasClass('col6')) {
														if (table == "reagentsTable") {
															if ($(this)
																	.find(
																			"input[name=expDate]")
																	.val() == undefined) {
																$(this)
																		.html(
																				"<input type='text' name='expDate' id='expDate' class='calDate'  value='"
																						+ $(
																								this)
																								.text()
																						+ "'  />");
															}
														} else if (table == "equipmentTable") {
															if ($(this).find(
																	"#mhs")
																	.val() == undefined) {
																$(this)
																		.html(
																				"<input type='text' name='mhs' id='mhs' value='"
																						+ $(
																								this)
																								.text()
																						+ "' />");
															}
														}

													} else if ($(this)
															.hasClass('col7')) {
														if (table == "equipmentTable") {
															if ($(this)
																	.find(
																			"input[name=ge]")
																	.val() == undefined) {
																$(this)
																		.html(
																				"<input type='text' name='ge' id='ge' value='"
																						+ $(
																								this)
																								.text()
																						+ "'  />");
															}
														}
														if (table == "reagentsTable") {
															//$(this).html("<input type='text' name='expDate' id='expDate' class='calDate'  value='" + $(this).text() +"'  />");
															$(this)
																	.html(
																			"<img src='images/file_loading.gif' id='fileUploadImage"+attachFileId+"' style='display:none'/>"
																					+ "<div id='fileDisplayName"+attachFileId+"'></div><input type='hidden' class='fileName' id='fileName"+attachFileId+"' name='fileName"+attachFileId+"'/><input type='hidden' class='documentId' id='documentId"+attachFileId+"' name='documentId"+attachFileId+"' value='0'/><input type='hidden' class='filePathId' name='filePathId"+attachFileId+"' id='filePathId"+attachFileId+"'/> <input type='hidden' class='fileUploadContentType' name='fileUploadContentType"+attachFileId+"' id='fileUploadContentType"+attachFileId+"'/>"
																					+ "<div class='"+attachFileId+"' style='cursor: pointer;	color: #0101DF;' id='displayAttach"+attachFileId+"'></div><input size='10' style='display:block' class='"
																					+ attachFileId
																					+ "' type='file' name='fileUpload' onChange='ajaxFileUpload1(this,\""
																					+ attachFileId
																					+ "\")' id='attach"
																					+ attachFileId
																					+ "'/>");

														}
													} else if ($(this)
															.hasClass('col8')) {
														if (table == "equipmentTable") {
															if ($(this)
																	.find(
																			"input[name=expDate]")
																	.val() == undefined) {
																$(this)
																		.html(
																				"<input type='text' name='expDate' id='expDate' class='calDate'  value='"
																						+ $(
																								this)
																								.text()
																						+ "'  />");
															}
														}
													}
													attachFileId += 1;
												});
							}
						});
		// common method to construct calendar for calDate class refer commonscript.js
		addDatepicker();
	}

	function delete_Reagents(table) {
		//alert("Delete Rows");
		//$('.progress-indicator').css('display', 'block');
		var deletedIds = "";
		try{
			  $("#"+table+" tbody tr").each(function (row){
					 if($(this).find("#reagentsupplies").is(":checked")){
						deletedIds += $(this).find("#reagentsupplies").val() + ",";
					 }
		});
			  var yes=confirm(confirm_deleteMsg);
				if(!yes){
					return;
				}
			if (deletedIds.length > 0) {

				deletedIds = deletedIds.substr(0, deletedIds.length - 1);

				//alert("deletedIds " + deletedIds);
				jsonData = "{deletedIds:'" + deletedIds + "'}";
				url = "deleteReagents";
				//alert("json data " + jsonData);
				response = jsonDataCall(url, "jsonData=" + jsonData);
			}
			if (table == "reagentsTable") {
				constructMergeWithoutColManager(true, table, reagentsTable_serverParam,
						reagentsTable_aoColumnDef, reagentsTable_aoColumn,
						column_sort, reagentsTable_MergeCols);
			} else if (table == "equipmentTable") {
				constructMergeWithoutColManager(true, table,
						equipmentTable_serverParam, equipmentTable_aoColumnDef,
						equipmentTable_aoColumn, column_sort,
						equipmentTable_MergeCols);
			}
		} catch (err) {
			alert("error " + err);
		}
		//?BB showPlusButtons(table);
		//$('.progress-indicator').css('display', 'none');
	}

	function save_Reagents(table) {
		//alert("save_Reagents");
		try {
			var rowData = "";
			var documentData = "";
			var reagentsupplies, name, code, description, manufacture, lotNumber, expDate, document, mhs, ge, activated
			var entityType = "INVENTORY";
			var documentFileName = "";
			var documentId = "0";
			var fileUploadContentType = "";
			var fileData = "";
			var docsDate = $.datepicker.formatDate('M dd, yy', new Date());
			//$('.progress-indicator').css( 'display', 'block' );
			var rowHtml;
			var colObj;
			$("#" + table)
					.find("#lotNumber")
					.each(
							function(row) {
								rowHtml = $(this).closest("tr");
								// alert("rowHtml " + $(rowHtml).html());
								reagentsupplies = "", name = "", code = "",
										description = "", manufacture = "",
										lotNumber = "", expDate = "",
										document = "", ge = "",
										entityType = "INVENTORY",
										documentFileName = "",
										documentId = "0",
										fileUploadContentType = "",
										fileData = "", docsDate = $.datepicker
												.formatDate('M dd, yy',
														new Date());
								activated = 0;

								if ($(rowHtml).find("td").hasClass("col0")) {
									reagentsupplies = $(rowHtml).find(
											"#reagentsupplies").val();
									if (reagentsupplies == undefined) {
										taskDetails = 0;
									}
								}
								if ($(rowHtml).find("td").hasClass("col1")) {
									name = $(rowHtml).find("#name").val();

								} else {
									$(rowHtml)
											.prevAll("tr")
											.each(
													function(r) {
														// alert("prev all " + r + " / " + $(this).html() + " / col1 " + $(this).find("td").hasClass("col1"));
														if ($(this).find("td")
																.hasClass(
																		"col1")) {
															colObj = $(this)
																	.find(
																			".col1");
															//alert("colobj " + $(colObj).text());
															name = $(colObj)
																	.find(
																			"#name")
																	.val();
															// alert("name " + name);
															if (name == undefined) {
																name = $(colObj)
																		.text();
															}
															return false;
														}
													});
								}
								//alert("name " + name);
								if ($(rowHtml).find("td").hasClass("col2")) {
									code = $(rowHtml).find("#code").val();
								} else {
									$(rowHtml)
											.prevAll("tr")
											.each(
													function(r) {
														if ($(this).find("td")
																.hasClass(
																		"col2")) {
															colObj = $(this)
																	.find(
																			".col2");
															code = $(colObj)
																	.find(
																			"#code")
																	.val();
															if (code == undefined) {
																code = $(colObj)
																		.text();
															}
															return false;
														}
													});
								}
								if ($(rowHtml).find("td").hasClass("col3")) {
									description = $(rowHtml).find(
											"#description").val();
								} else {
									$(rowHtml)
											.prevAll("tr")
											.each(
													function(r) {
														if ($(this).find("td")
																.hasClass(
																		"col3")) {
															colObj = $(this)
																	.find(
																			".col3");
															description = $(
																	colObj)
																	.find(
																			"#description")
																	.val();
															if (description == undefined) {
																description = $(
																		colObj)
																		.text();
															}
															return false;
														}
													});
								}
								if ($(rowHtml).find("td").hasClass("col4")) {
									manufacture = $(rowHtml).find(
											"#manufacture").val();
								} else {
									$(rowHtml)
											.prevAll("tr")
											.each(
													function(r) {
														if ($(this).find("td")
																.hasClass(
																		"col4")) {
															colObj = $(this)
																	.find(
																			".col4");
															manufacture = $(
																	colObj)
																	.find(
																			"#manufacture")
																	.val();
															if (manufacture == undefined) {
																manufacture = $(
																		colObj)
																		.text();
															}
															return false;
														}
													});
								}

								if ($(rowHtml).find("td").hasClass("col7")) {
									//document =$(rowHtml).find("#document").val();
									fileData = $(rowHtml).find(".filePathId")
											.val();
									if (fileData != undefined) {
										fileData = fileData.replace(/\\/g, "/");
										documentFileName = $(rowHtml).find(
												".fileName").val();
										documentId = $(rowHtml).find(
												".documentId").val();
										fileUploadContentType = $(rowHtml)
												.find(".fileUploadContentType")
												.val();
									}
								} else {
									$(rowHtml)
											.prevAll("tr")
											.each(
													function(r) {
														if ($(this).find("td")
																.hasClass(
																		"col7")) {
															colObj = $(this)
																	.find(
																			".col7");
															//description =$(colObj).find("#document").val();
															fileData = $(colObj)
																	.find(
																			".filePathId")
																	.val();
															if (fileData != undefined) {
																fileData = fileData
																		.replace(
																				/\\/g,
																				"/");
																documentFileName = $(
																		colObj)
																		.find(
																				".fileName")
																		.val();
																fileUploadContentType = $(
																		colObj)
																		.find(
																				".fileUploadContentType")
																		.val();
																documentId = $(
																		colObj)
																		.find(
																				".documentId")
																		.val();
															}
															if (fileData == undefined) {
																//description =$(colObj).text();
																fileData = $(
																		colObj)
																		.find(
																				".filePathId")
																		.val();
																if (fileData != undefined) {
																	fileData = fileData
																			.replace(
																					/\\/g,
																					"/");
																	documentFileName = $(
																			colObj)
																			.find(
																					".fileName")
																			.val();
																	documentId = $(
																			colObj)
																			.find(
																					".documentId")
																			.val();
																	fileUploadContentType = $(
																			colObj)
																			.find(
																					".fileUploadContentType")
																			.val();
																}
															}
															return false;
														}
													});
								}

								if (fileData == undefined) {
									fileData = "";
								}
								ge = $(rowHtml).find("#ge").val();
								lotNumber = $(rowHtml).find("#lotNumber").val();
								expDate = $(rowHtml)
										.find("input[name=expDate]").val();
								mhs = $(rowHtml).find("#mhs").val();
								if ($(rowHtml).find("#activated_yes").is(
										":checked")) {
									activated = 1;
								} else {
									activated = 0;
								}
								var type = "REAGENTS";
								if (table == "equipmentTable") {
									type = "EQUIPMENT";
								}

								//if(name != undefined && name != ""){
								rowData += "{reagentsupplies:'"
										+ reagentsupplies + "',name:'" + name
										+ "',code:'" + code + "',description:'"
										+ description + "',manufacture:'"
										+ manufacture + "',lotNumber:'"
										+ lotNumber + "',expDate:'" + expDate
										+ "',activated:" + activated
										+ ",rs_type:'" + type + "',mhs:'" + mhs
										+ "',pkDocumentId:'" + documentId
										+ "',entityType:'" + entityType
										+ "',documentFileName:'"
										+ documentFileName
										+ "',documentUploadedDate:'" + docsDate
										+ "',documentFile:'" + fileData
										+ "',fileUploadContentType:'"
										+ fileUploadContentType + "',ge:'" + ge
										+ "'},";
								//rowData+= "{reagentsupplies:'"+reagentsupplies+"',name:'"+name+"',code:'"+code+"',description:'"+description+"',manufacture:'"+manufacture+"',lotNumber:'"+lotNumber+"',expDate:'"+expDate+"',activated:"+activated+",rs_type:'"+type+"',mhs:'"+mhs+"',pkDocumentId:'"+documentId+"',entityType:'"+entityType+"',documentFileName:'"+documentFileName+"',documentFile:'"+fileData+"',fileUploadContentType:'"+fileUploadContentType+"'},";

								//}
							});
			//alert("rowData " + rowData);
			if (rowData.length > 0) {
				rowData = rowData.substring(0, (rowData.length - 1));
				var url = "saveInventory";
				response = jsonDataCall(url, "jsonData={data:[" + rowData
						+ "],documentData:[" + documentData + "]}");
				alert("Data Saved Successfully");

				if (table == "reagentsTable") {
					constructMergeWithoutColManager(true, table,
							reagentsTable_serverParam,
							reagentsTable_aoColumnDef, reagentsTable_aoColumn,
							column_sort, reagentsTable_MergeCols);
				} else if (table == "equipmentTable") {
					constructMergeWithoutColManager(true, table,
							equipmentTable_serverParam,
							equipmentTable_aoColumnDef,
							equipmentTable_aoColumn, column_sort,
							equipmentTable_MergeCols);
				}
				//?BB showPlusButtons(table);
			}
		} catch (err) {
			alert("error " + err);
		}
		$('.progress-indicator').css('display', 'none');
		return true;
	}

	//--- ***********complete methods***************
</script>

<div class="cleaner"></div>

<div class="cleaner"></div>
<form>
<div class="column">	
	<div  class="portlet">
		<div class="portlet-header">Reagents & Supplies Library</div>
		<div class="portlet-content">
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  style="width:16;height:16;cursor:pointer;" onclick="add_Reagents('reagentsTable');"/>&nbsp;&nbsp;<label class="cursor"  onclick="add_Reagents('reagentsTable');"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="delete_Reagents('reagentsTable');"/>&nbsp;&nbsp;<label class="cursor" onclick="delete_Reagents('reagentsTable');"><b>Delete</b></label>&nbsp;</div></td>
				    <td style="width:20%"><div><img src = "images/icons/edit.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="edit_Reagents('reagentsTable');"/>&nbsp;&nbsp;<label class="cursor" onclick="edit_Reagents('reagentsTable');"><b>Edit</b></label>&nbsp;</div></td>
				    <!-- <td style="width:20%"><div><img src = "images/icons/save3.png" class="cursor" style="width:16;height:16;" class="cursor" onclick="save_Reagents('reagentsTable');" >&nbsp;&nbsp;<label class="cursor" onclick="save_Reagents('reagentsTable')"><b>Save</b></label></div></td> -->
				</tr>
			</table>
			<div style="overflow-x:auto;overflow-y:hidden">
			<table cellpadding="0" cellspacing="0" border="1" id="reagentsTable" class="display">
				<thead>
					<tr>
						
						<th class='col0'>Check All <input type="checkbox"  onclick="checkall('reagentsTable',this);"/></th>
						<th width="20%">Reagents & Supplies Name</th>
						<th>Code</th>
						<th>Description</th>
						<th>Manufacturer</th>
						<th>Lot #</th>
						<th>Exp.Date</th>
						<th with="20%">Certificate of Analysis</th>
						<th width="15%" nowrap="nowrap">Activated</th>
					</tr>
				</thead>
				<tbody>
				<!-- testing
					<tr>
					
						<td class='col0'><input type="checkbox"/></td>
						<td class='col1'>12x75mm Snap Cap Tube0</td>
						<td class='col2'>snapcapt1</td>
						<td class='col3'></td>
						<td class='col4'>Fisher Scientific
							<img align="right" style="visibility:hidden" src = "images/icons/addnew.jpg" onclick="add_manufacturer(this);">
							
						</td>
						<td class='col5'>14-869-26
							<img align="right"style="visibility:hidden"  src = "images/icons/addnew.jpg" onclick="add_manufacturer(this);">
						</td>
						<td class='col6'>2013-11-21</td>
						<td class='col7'>browse</td>
						<td class='col8'><input type="radio" id="" name=""/>Y&nbsp;<input type="radio" id="" name=""/>N</td>
					</tr>
					<tr>
					
						<td class='col0'><input type="checkbox"/></td>
						<td class='col1'>12x75mm Snap Cap Tube</td>
						<td class='col2'>snapcapt0</td>
						<td class='col3'></td>
						<td class='col4'>Fisher Scientific1
							<img align="right" style="visibility:hidden" src = "images/icons/addnew.jpg" onclick="add_manufacturer(this);">
							
						</td>
						<td class='col5'>14-869-26
							<img align="right"style="visibility:hidden"  src = "images/icons/addnew.jpg" onclick="add_manufacturer(this);">
						</td>
						<td class='col6'>2013-11-21</td>
						<td class='col7'>browse</td>
						<td class='col8' ><input type="radio" id="" name=""/>Y&nbsp;<input type="radio" id="" name=""/>N</td>
					</tr>
					<tr>
						
						<td class='col0'><input type="checkbox"/></td>
						<td class='col1'>12x75mm Snap Cap Tube0</td>
						<td class='col2'>snapcapt</td>
						<td class='col3'></td>
						<td class='col4' >Fisher Scientific1
							<img align="right"style="visibility:hidden" src = "images/icons/addnew.jpg" onclick="add_manufacturer(this);">
							
						</td>
						<td class='col5'>14-869-26
							<img align="right"style="visibility:hidden"  src = "images/icons/addnew.jpg" onclick="add_manufacturer(this);">
						</td>
						<td class='col6'>2013-11-21</td>
						<td class='col7'>browse</td>
						<td class='col8' ><input type="radio" id="" name=""/>Y&nbsp;<input type="radio" id="" name=""/>N</td>
					</tr>
			<tr>
						
						<td class='col0'><input type="checkbox"/></td>
						<td class='col1'>12x75mm Snap Cap Tube</td>
						<td class='col2'>snapcapt1</td>
						<td class='col3'></td>
						<td class='col4' >Fisher Scientific
							<img align="right"style="visibility:hidden" src = "images/icons/addnew.jpg" onclick="add_manufacturer(this);">
							
						</td>
						<td class='col5'>14-869-26
							<img align="right"style="visibility:hidden"  src = "images/icons/addnew.jpg" onclick="add_manufacturer(this);">
						</td>
						<td class='col6'>2013-11-21</td>
						<td class='col7'>browse</td>
						<td class='col8' ><input type="radio" id="" name=""/>Y&nbsp;<input type="radio" id="" name=""/>N</td>
					</tr>
					
					<tr>
						
						<td class='col0'><input type="checkbox"/></td>
						<td class='col1'>12x75mm Snap Cap Tube</td>
						<td class='col2'>snapcapt1</td>
						<td class='col3'></td>
						<td class='col4' >Fisher Scientific
							<img align="right"style="visibility:hidden" src = "images/icons/addnew.jpg" onclick="add_manufacturer(this);">
							
						</td>
						<td class='col5'>14-869-26
							<img align="right"style="visibility:hidden"  src = "images/icons/addnew.jpg" onclick="add_manufacturer(this);">
						</td>
						<td class='col6'>2013-11-21</td>
						<td class='col7'>browse</td>
						<td class='col8' ><input type="radio" id="" name=""/>Y&nbsp;<input type="radio" id="" name=""/>N</td>
					</tr>
					-->
				</tbody>
			</table>
			</div>			
		</div>
	</div>
</div>
</form>

<div class="cleaner"></div>

<form>
<div class="column">	
	<div  class="portlet">
		<div class="portlet-header">Equipments Library</div>
		<div class="portlet-content">
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  style="width:16;height:16;cursor:pointer;" onclick="add_Reagents('equipmentTable');"/>&nbsp;&nbsp;<label class="cursor"  onclick="add_Reagents('equipmentTable');"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="delete_Reagents('equipmentTable');"/>&nbsp;&nbsp;<label class="cursor" onclick="delete_Reagents('equipmentTable');"><b>Delete</b></label>&nbsp;</div></td>
				    <td style="width:20%"><div><img src = "images/icons/edit.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="edit_Reagents('equipmentTable');"/>&nbsp;&nbsp;<label class="cursor" onclick="edit_Reagents('equipmentTable');"><b>Edit</b></label>&nbsp;</div></td>
				    <!-- <td style="width:20%"><div><img src = "images/icons/save3.png" class="cursor" style="width:16;height:16;" class="cursor" onclick="save_Reagents('equipmentTable');" >&nbsp;&nbsp;<label class="cursor" onclick="save_Reagents('equipmentTable')"><b>Save</b></label></div></td> -->
				</tr>
			</table>
			<div style="overflow-x:auto;overflow-y:hidden">
			<table cellpadding="0" cellspacing="0" border="1" id="equipmentTable" class="display">
				<thead>
					<tr>
						<th>Check All<input type="checkbox" onclick="checkall('equipTable',this);"/></th>
						<th>Equipment</th>
						<th>Code</th>
						<th>Description</th>
						<th>Manufacturer</th>
						<th>Serial #</th>
						<th>MHS #</th>
						<th>GE#</th>
						<th>Maintenance Due Date</th>
						<th width="25%">Activated</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>	
			</div>			
		</div>
	</div>
</div>
</form>

<script>

$(function() {
 
    $( ".column" ).sortable({
        connectWith: ".column"
   });

    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
 

    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

   
});



</script>