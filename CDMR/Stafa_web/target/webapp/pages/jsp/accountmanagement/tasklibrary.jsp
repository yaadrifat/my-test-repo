<%@ include file="../common/includes.jsp"%>
<script type="text/javascript" src="js/portlet.js"></script>
<script type="text/javascript" src="js/dataSearch.js"></script>
<script>
 // alert("new task lib page");

  /*
   DataTable data
  */
  function savePages(){
		try{
			if(checkValidation()){
				if($("#taskHidden").val()=="save_TaskLib"){
						if(save_TaskLib()){
						return true;
					} 
				}else if($("#taskHidden").val()=="save_TaskDetails"){
					 if(save_TaskDetails()){
						return true;
					} 
				}
			}
		}catch(e){
			alert("exception " + e);
		}
	}
  
  $("#taskLibTable tbody tr ").mouseenter(function(){
	  $(this).attr("style","background:#33ffff");	
	 // $(this).css("cursor","default");
  }).mouseleave(function() {
	  $(this).removeAttr("style");
	  $(this).css("cursor","default");
  });
 
	 function refreshSideWidget(){
		    //Refresh side widgets
		    var url = "qclib";
			var result = ajaxCall(url,'');
			$("#qclib").replaceWith($('#qclib', $(result)));
		 }

function delete_TaskDetails(){
		 // alert("Delete Rows");
		  var deletedIds = ""
		 try{
			  $("#taskDetails tbody tr").each(function (row){
			  if($(this).find("#taskDetails").is(":checked")){
					
					deletedIds += $(this).find("#taskDetails").val() + ",";
			  }
				 });
			  //alert("deletedIds::=>"+deletedIds);
				
			  if(deletedIds.length >0){
				  
				  deletedIds = deletedIds.substr(0,deletedIds.length-1);
				  var yes=confirm(confirm_deleteMsg);
					if(!yes){
						return;
					}else{
					  jsonData = "{deletedIds:'"+deletedIds+"'}";
					  url="deleteTaskLibrary";
				        //alert("json data " + jsonData);
				     response = jsonDataCall(url,"jsonData="+jsonData);
				     constructTaskDetails(true);
					}
			  }
					 
				  //alert("deletedIds " + deletedIds);
				
		 		//Refresh side widgets
		 /*   var url = "qclib";
			var result = ajaxCall(url,'');
			$("#qclib").replaceWith($('#qclib', $(result)));
		   */  
		}catch(error){
		alert("error " + error);
	}
}


function save_TaskDetails(){

		 var rowData = "";
		 var taskDetails,parentTask,name,code,description,activated;
		 $('.progress-indicator').css( 'display', 'block' );
		 parentTask = $("#taskLibId").val();
		 $("#taskDetails tbody tr").each(function (row){
			 taskLibrary =0;
			 copyTask=0;
			 name="";
			 code="";
			 description="";
			 activated =0;
		 	$(this).find("td").each(function (col){
		 		//alert(col + " / " + $(this).html() );
			 	   if(col ==1){
			 		  taskDetails =$(this).find("#taskDetails").val();
				 		if(taskDetails == undefined){
				 			taskDetails =0;
				 		}
				 		
				 	 }else if(col ==2){
				 		name=$(this).find("#name").val();
				 	 }else if(col ==3){
					 		
					 	 if($(this).find(".taskDetailsCode").hasClass("taskDetailsCode")){
				 			code=$(this).find("#code").val();
					 	 }else if($.trim($(this).text())==""){
							 	 if(name!=undefined){
						 			//var randomCode = codeListQcName.replace('',' ');
						 			var randomCode = name.replace(/\s/g, '').toLowerCase();
						 			//alert("randomCode : "+randomCode);
						 			code=$.trim(randomCode);
						 			//alert("code::"+code);
							 	 }
						 }else{
							 code = $(this).text();
								}
				 	}/* else if(col ==3){
				 		code=$(this).find("#code").val();
				 	 } */else if(col ==4){
				 		description=$(this).find("#description").val();
				 	 }else if(col ==5){
				 		if($(this).find("#activeyes").is(":checked")){
				 			activated=1;
				 		}else{
				 			activated=0;
				 		}
				 	 }
			  });
		 		if(name != undefined && name != ""){
			 	  rowData+= "{taskLibrary:"+taskDetails+",parentTask:"+parentTask+",name:'"+name+"',code:'"+code+"',description:'"+description+"',activated:"+activated+"},";
		 		}
		 });
		//alert("rowData " + rowData);
		  if(rowData.length >0){
	            rowData = rowData.substring(0,(rowData.length-1));
	       var url="saveTaskLibrary";
		     response = jsonDataCall(url,"jsonData={data:["+rowData+"]}");
		       alert("Data Saved Successfully");
		      
		       constructTaskDetails(true);
		    
		  }
		  $('.progress-indicator').css( 'display', 'none' );
		  return true;
}



//--- ***********complete methods***************
	$(document).ready(function(){
		constructTaskLibTable(true);
		clearTracker();
		show_innernorth();
	});


function edit_TaskDetails(){
	 // alert("edit Rows");
	
	  $("#taskDetails tbody tr").each(function (row){
		 if($(this).find("#taskDetails").is(":checked")){
			 //alert("s");
			 $(this).find("td").each(function (col){
				 if(col ==2 ){
					 if ($(this).find("#name").val()== undefined){
						$(this).html("<input type='text' id='name' name='name' value='"+$.trim($(this).text())+ "' />");
					 }
				 }else if(col ==3){
					 if ($(this).find("#code").val()== undefined){
					 	$(this).html("<input type='text' id='code' class='taskDetailsCode' name='code' value='"+$.trim($(this).text())+ "' />");
					 }
				 }else if(col ==4){
					 if ($(this).find("#description").val()== undefined){
					 	$(this).html("<input type='text' id='description' name='description' value='"+$.trim($(this).text())+ "' />");
					 }
				 }
			 });
		 }
	  });
	  mark
}


	function add_TaskDetails(){
		// alert(" add Rows");
		 //restoreAll('#targetall1 tbody tr:first td');
		 elementcount +=1;
		 var newRow = "<tr><td>&nbsp;</td><td><input type='checkbox' id='taskDetails' value='0'></td><td><input type='text' id='name' name='name' value='' /></td><td>&nbsp;</td>";
		   newRow += "<td><input type='text' id='description' name='description' value='' /></td>";
		   newRow += "<td><input type='radio' value='Yes' name='active_a"+elementcount+"' id='activeyes';" ;
		  	   newRow += " checked='checked' ";
		     
		   newRow += " >Yes</input><input type='radio' value='No' name='active_a"+elementcount+"' id='active' " ; 
		    newRow += "  >No</input></td></tr>";
		   $("#taskDetails tbody[role='alert']").prepend(newRow);  
		   
		  // $("#taskDetails .ColVis_MasterButton").triggerHandler("click");
		 //  $('.ColVis_Restore:visible').triggerHandler("click");
		 //  $("div.ColVis_collectionBackground").trigger("click");
	}

	 function constructTaskDetails(flag) {
			//alert("constructTaskDetails table");
			elementcount =0;
			var hideColumn=1;
		//	$('.progress-indicator').css( 'display', 'block' );
			var strBreadcrumb = "<a href='#' onclick='constructTaskLibTable(true);'>Task Library</a> &gt; &nbsp;" +$("#taskspan").html();
			$("#breadCrumbLink").html(strBreadcrumb );
			$("#tasks").removeClass("hidden");
			$("#taskLib").addClass("hidden");
		 	
		 	
		 			 	
		 	
		 	var criteria = " and parentTask ='"+$("#taskLibId").val() +"'"; 
			  var link ;
				try{
					
							 
					  								
					  var taskDetails_serverParam = function ( aoData ) {
																aoData.push( { "name": "application", "value": "stafa"});
																aoData.push( { "name": "module", "value": "AccountManagement_TaskLibrary_TaskDetail"});	
																aoData.push( { "name": "criteria", "value": criteria});	
																
																							
																aoData.push( {"name": "col_1_name", "value": "" } );
																aoData.push( { "name": "col_1_column", "value": ""});
																
																aoData.push( { "name": "col_2_name", "value": "lower(name)"});
																aoData.push( { "name": "col_2_column", "value": "lower(name)"});
																
																aoData.push( {"name": "col_3_name", "value": "lower(code)" } );
																aoData.push( { "name": "col_3_column", "value": "lower(code)"});
																
																aoData.push( {"name": "col_4_name", "value": "lower(description)" } );
																aoData.push( { "name": "col_4_column", "value": "lower(description)"});
						};
						
						var taskDetails_aoColumnDef =  [ 
						                  {		
						                	  "aTargets": [ 0], "mDataProp": function ( source, type, val ) { 
		                	 				 return "";}},
						                  {		"sTitle":"Check All <input type='checkbox'  id='deleteallProtocol' onclick='checkall(\"taskLibTable\",this)'>",
		                	 					 "aTargets": [ 1], "mDataProp": function ( source, type, val ) { 
						                	 return "<input type='checkbox' id='taskDetails' name='taskLibrary' value='"+source.taskLibrary +"' />" ;}},
										  {		"sTitle":"Name",
						                		 "aTargets": [ 2], "mDataProp": function ( source, type, val ) { 
											    link = source.name;
											  return link;}},
										  {		"sTitle":"Code",
												  "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
											 	   return source.code;
										  		}},
										 			
										  {		"sTitle":"Description",
										  			"aTargets": [ 4], "mDataProp": function ( source, type, val ) { 
											  		return source.description;
											  }},
										  {		"sTitle":"Activated",
												  "aTargets": [ 5], "mDataProp": function ( source, type, val ) { 
											  if(source.activated=='1'){
												  link = "<input type='radio' name='active_"+source.taskLibrary+"' id='activeyes' value='Yes' checked='checked' >Yes</input>";
												  link+= "<input type='radio' name='active_"+source.taskLibrary+"' id='active' value='No' >No</input>";
											  }else{
												  link = "<input type='radio' name='active_"+source.taskLibrary+"' id='activeyes' value='Yes'  >Yes</input>";
												  link+= "<input type='radio' name='active_"+source.taskLibrary+"' id='active' value='No' checked='checked'>No</input>";
											  }
											  return link;}}
										];
						
						var taskDetails_aoColumn =[{"bSortable": false},
										              {"bSortable": false},
										               null,
										               null,
										               null,
										               null];
				  		  
						
				  		var taskDetails_ColManager = function(){
							$("#taskDetailsColumn").html($('#taskDetails_wrapper .ColVis'));
						 };
						 constructTable(flag,'taskDetails',taskDetails_ColManager,taskDetails_serverParam,taskDetails_aoColumnDef,taskDetails_aoColumn);

						
						
					}catch(err){
						alert("error " + err);
					}
		}		 
		 

	 function getTaskList(id,title){
		 $("#taskHidden").val("save_TaskDetails")
		 //alert("takslist")
	 	$("#taskLibId").val(id);
	 	$("#taskspan").html(title);
	 	
	 	constructTaskDetails(true);
	 }
var oTable;
function constructTaskLibTable(flag) {
	 $("#taskHidden").val("save_TaskLib")
		elementcount =0;
		$('.progress-indicator').css( 'display', 'block' );
		var strBreadcrumb = "<a href='#' onclick='constructTaskLibTable(true);'>Task Library</a>";
		$("#breadCrumbLink").html(strBreadcrumb );
		$("#tasks").addClass("hidden");
		$("#taskLib").removeClass("hidden");
	 /*	if(flag || flag =='true'){
	 	 oTable=$('#taskLibTable').dataTable().fnDestroy();
		}
	 */
	 	var criteria = " and parentTask is null "; 
		  var link ;
			try{
									  							
				var taskLibTable_serverParam	= function ( aoData ) {
															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "AccountManagement_TaskLibrary"});	
															aoData.push( { "name": "criteria", "value": criteria});	
																						
															aoData.push( {"name": "col_1_name", "value": "" } );
															aoData.push( { "name": "col_1_column", "value": ""});
															
															aoData.push( { "name": "col_2_name", "value": "lower(name)"});
															aoData.push( { "name": "col_2_column", "value": "lower(name)"});
															
															aoData.push( {"name": "col_3_name", "value": "lower(code)" } );
															aoData.push( { "name": "col_3_column", "value": "lower(code)"});
															
															aoData.push( {"name": "col_4_name", "value": "lower(description)" } );
															aoData.push( { "name": "col_4_column", "value": "lower(description)"});
					};
					
					var taskLibTable_aoColumnDef =  [ 
					                  {		
					                	  "aTargets": [ 0], "mDataProp": function ( source, type, val ) { 
	                	 				 return "";}},
					                  {		"sTitle":"Check All <input type='checkbox'  id='deleteallProtocol' onclick='checkall(\"taskLibTable\",this)'>",
	                	 					 "aTargets": [ 1], "mDataProp": function ( source, type, val ) { 
					                	 return "<input type='checkbox' id='taskLibrary' name='taskLibrary' value='"+source.taskLibrary +"' /><input type='hidden' name='appLock' id='appLock' value='"+source.appLock +"' />";}},
									  {		"sTitle":"Name",
					                		 "aTargets": [ 2], "mDataProp": function ( source, type, val ) { 
										    link = "<a href='#' onclick='javascript:getTaskList(\""+source.taskLibrary+"\",\""+source.name+"\");'>"+source.name+"</a>";
										  return link;}},
									  {		"sTitle":"Code",
											  "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
										 	   return source.code;
									  		}},
									  {		"sTitle":"Module",
												  "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
											 	   return source.module;
										 }},			
									  {		"sTitle":"Description",
									  			"aTargets": [ 5], "mDataProp": function ( source, type, val ) { 
										  		return source.description;
										  }},
									  {		"sTitle":"Activated",
											  "aTargets": [ 6], "mDataProp": function ( source, type, val ) { 
										  if(source.activated=='1'){
											  link = "<input type='radio' name='active_"+source.taskLibrary+"' id='activeyes' value='Yes' checked='checked' >Yes</input>";
											  link+= "<input type='radio' name='active_"+source.taskLibrary+"' id='active' value='No' >No</input>";
										  }else{
											  link = "<input type='radio' name='active_"+source.taskLibrary+"' id='activeyes' value='Yes'  >Yes</input>";
											  link+= "<input type='radio' name='active_"+source.taskLibrary+"' id='active' value='No' checked='checked'>No</input>";
										  }
										  return link;}}
									];
					var taskLibTable_aoColumn =  [{"bSortable": false},
									              {"bSortable": false},
									               null,
									               null,
									               null,
									               null,
									               null];
					var taskLibTable_ColManager = function(){
						$("#taskLibColumn").html($('#taskLibTable_wrapper .ColVis'));
					 };
				 	constructTable(flag,'taskLibTable',taskLibTable_ColManager,taskLibTable_serverParam,taskLibTable_aoColumnDef,taskLibTable_aoColumn);
				 			
				
				}catch(err){
					alert("error " + err);
				}
				$('.progress-indicator').css( 'display', 'none' );		
	} 

function delete_TaskLib(){
		  //alert("Task Delete Rows");
	var deletedIds = "";
	try{
	  $("#taskLibTable tbody tr").each(function(row){
		 if($(this).find("#taskLibrary").is(":checked")){
			 deletedIds += $(this).find("#taskLibrary").val() + ",";
		 	}
	  });
	  //alert("deletedIds " + deletedIds);
	  if(deletedIds.length >0){
		  
		  deletedIds = deletedIds.substr(0,deletedIds.length-1);
		  var yes=confirm(confirm_deleteMsg);
			if(!yes){
				return;
			}else{
				 jsonData = "{deletedIds:'"+deletedIds+"'}";
				  url="deleteTaskLibrary";
			     //alert("json data " + jsonData);
			     response = jsonDataCall(url,"jsonData="+jsonData);
			     constructTaskLibTable(true);
			}
	  }
	}catch(error){
		alert("error " + error);
	}
}


function save_TaskLib(){

		 var rowData = "";
		 var taskLibrary,copyTask,name,code,description,activated,module,appLock;
		 $('.progress-indicator').css( 'display', 'block' );
		
		 $("#taskLibTable").find("#name").each(function (row){
			 rowHtml =  $(this).closest("tr");
			 taskLibrary =0;
			 copyTask=0;
			 name="";
			 code="";
			 description="";
			 activated =0;
			 module ="";
			 appLock=0;

						taskLibrary =$(rowHtml).find("#taskLibrary").val();
				 		if(taskLibrary == undefined){
				 			taskLibrary =0;
				 		}
				 		copyTask = $(rowHtml).find("#copyTask").val();
				 		if(copyTask == undefined){
				 			copyTask =0;
				 		}
				 	
				 		name=$(rowHtml).find("#name").val();
				 		
				 		if($(rowHtml).find("#code").hasClass("taskCode")){
				 			code=$(rowHtml).find("#code").val();
					 	 }else if(name!=undefined){
					 			//var randomCode = codeListQcName.replace('',' ');
					 			var randomCode = name.replace(/\s/g, '').toLowerCase();
					 			//alert("randomCode : "+randomCode);
					 			code=$.trim(randomCode);
					 			//alert("code"+code);
						 	 }
				 		
				 		//code=$(rowHtml).find("#code").val();
				 	 
				 		description=$(rowHtml).find("#description").val();
				 	
				 		if($(rowHtml).find("#activeyes").is(":checked")){
				 			activated=1;
				 		}else{
				 			activated=0;
				 		}
				 		
				 		module = $(rowHtml).find("#module").val();
				 		appLock = $(rowHtml).find("#appLock").val();
				 		if(appLock != 1){
				 			appLock =0;
				 		}
			  
		 		if(name != undefined && name != ""){
			 	  rowData+= "{taskLibrary:"+taskLibrary+",copyTask:"+copyTask+",name:'"+name+"',code:'"+code+"',module:'"+module+"',description:'"+description+"',activated:"+activated+",appLock:'"+appLock+"'},";
		 		}
		 });
		//alert("rowData " + rowData);
		  if(rowData.length >0){
	            rowData = rowData.substring(0,(rowData.length-1));
	       var url="saveTaskLibrary";
		     response = jsonDataCall(url,"jsonData={data:["+rowData+"]}");
		       alert("Data Saved Successfully");
		      
		       constructTaskLibTable(true);
		    
		  }
		  $('.progress-indicator').css( 'display', 'none' );
		  return true;
}
var elementcount =0;
 function addRows(pkCodelst,name,code,comments,copyCodelst,activeFlag){
	 //alert(" add Rows");
	 //restoreAll('#targetall1 tbody tr:first td');
	  var moduleDrop = "<select name='module' id='module'>"+$.trim($("#codeListModule").html())+"</select>";
	  
	 elementcount +=1;
	 var newRow = "<tr><td>&nbsp;</td><td><input type='checkbox' id='taskLibrary' value='"+pkCodelst+"'> <input type='hidden' id='copyTask' name='copyTask' value='"+copyCodelst+"' /></td><td><input type='text' id='name' name='name' value='"+name+"' /></td><td>&nbsp;</td>";
	 newRow += "<td>"+moduleDrop+"</td>" ;
	 newRow += "<td><input type='text' id='description' name='description' value='"+comments+"' /></td>";
	   newRow += "<td><input type='radio' value='Yes' name='active_a"+elementcount+"' id='activeyes';" ;
	   if(activeFlag){	
		   newRow += " checked='checked' ";
	   }   
	   newRow += " >Yes</input><input type='radio' value='No' name='active_a"+elementcount+"' id='active' " ; 
	   if(!activeFlag){	
		   newRow += " checked='checked' ";
	   }
		   newRow += "  >No</input></td></tr>";
	   $("#taskLibTable tbody[role='alert']").prepend(newRow);  
	   
	   $("#taskLibTable .ColVis_MasterButton").triggerHandler("click");
	   $('.ColVis_Restore:visible').triggerHandler("click");
	   $("div.ColVis_collectionBackground").trigger("click");
	   $.uniform.restore('select');
		 $('select').uniform();
			var param ="{module:'APHERESIS',page:'TASKLIBRARY'}";
			markValidation(param);
 }

 function edit_taskLibTable(){
	 // alert("edit Rows");
	var tmpHtml="";
	  $("#taskLibTable tbody tr").each(function (row){
		 if($(this).find("#taskLibrary").is(":checked")){
			 //alert("s");
			  var tmpVal =  $(this).closest("tr").find("#appLock").val();
			 $(this).find("td").each(function (col){
				 if(col ==2 ){
					 if ($(this).find("#name").val()== undefined){
						$(this).html("<input type='text' id='name' name='name' value='"+$.trim($(this).text())+ "' />");
					 }
				 }else if(col ==3){
					 if ($(this).find("#code").val()== undefined){
						 tmpHtml = "<input type='text' id='code' class='taskCode' name='code' value='"+$.trim($(this).text())+ "'";
					 	if(tmpVal ==1){
					 		tmpHtml +=" readonly='true' ";
					 	}
					 	tmpHtml +="/>";
						$(this).html(tmpHtml);
					 }
				 }else if(col==4){
					 if ($(this).find("#module").val()== undefined){
						 var oldModuleVal = $(this).text();
						 var moduleDrop = "<select name='module' id='module'>"+$.trim($("#codeListModule").html())+"</select>";
					 	$(this).html(moduleDrop);
						$(this).find("#module").val(oldModuleVal);
						if(tmpVal==1){
							$(this).find("#module").attr("disabled", true); 
						}
					 	//alert("sss");
					 	$.uniform.restore('select');
						 $("select").uniform();
					 }
				 }else if(col ==5){
					 if ($(this).find("#description").val()== undefined){
					 	$(this).html("<input type='text' id='description' name='description' value='"+$.trim($(this).text())+ "' />");
					 }
				 }
			 });
		 }
	  });
	  var param ="{module:'APHERESIS',page:'TASKLIBRARY'}";
		markValidation(param);  
 }

function editRows(tableId){
	 // alert("edit Rows");
	var tmpHtml="";
	  $("#"+tableId+" tbody tr").each(function (row){
		 if($(this).find("#taskLibrary").is(":checked")){
			 //alert("s");
			  var tmpVal =  $(this).closest("tr").find("#appLock").val();
			 $(this).find("td").each(function (col){
				 if(col ==2 ){
					 if ($(this).find("#name").val()== undefined){
						$(this).html("<input type='text' id='name' name='name' value='"+$.trim($(this).text())+ "' />");
					 }
				 }else if(col ==3){
					 if ($(this).find("#code").val()== undefined){
						 tmpHtml = "<input type='text' id='code' name='code' value='"+$.trim($(this).text())+ "'";
					 	if(tmpVal ==1){
					 		tmpHtml +=" readonly='true' ";
					 	}
					 	tmpHtml +="/>";
						$(this).html(tmpHtml);
					 }
				 }
				 
				 else if(col ==4){
					 if ($(this).find("#description").val()== undefined){
					 	$(this).html("<input type='text' id='description' name='description' value='"+$.trim($(this).text())+ "' />");
					 }
				 }
			 });
		 }
	  });
	 
 }
 

</script>
<s:hidden  name="taskHidden" id="taskHidden" value="%{taskHidden}"/>
<div class="cleaner"></div>
<div id="slidecolumnfilter" style="display:none"> 
	<br>
	<div class="portlet" id="panelMaintenanceDiv">
		<div class="portlet-header"><s:hidden id="pkNotes" value=""/>Tasks<input type="text" id="Columnfilter" class="inputboxSearch maintenance_slidewidget" onclick="maintColumnfilter('qclib')"></div>
		<div class="cleaner"></div>
		<div class="portlet-content" style="height:320px;overflow-y:auto;width:94%" >
			<table width="100%" border="0" id="qclib">
				<s:iterator value="qcmaintlst" var="rowVal" status="row">
					<s:iterator value="rowVal" var="cellValue" status="col">
						<tr>
							<td style="padding:2%" width="90%">
							<input type="hidden" id="qcPrmariyId" name="qcPrmariyId" /><a href="#" class="cursor" onclick="javascript:getQcDetails('<s:property value="pkCodelst"/>','<s:property value="description"/>','<s:property value="idHide"/>','<s:property value="comments"/>','<s:property value="type"/>','<s:property value="subType"/>');" ><s:property value="description"/></a></td>
							<td width="10%"><div><img src = "images/icons/folder.png" class="cursor" onclick="copyQc('<s:property value="pkCodelst"/>','<s:property value="description"/>','<s:property value="subType"/>','<s:property value="copyCodelst"/>','<s:property value="type"/>','<s:property value="comments"/>')"/></div></td>
						</tr>
					</s:iterator>
				</s:iterator>  
			</table>
		  </div>
	</div>
</div>
		
<!--float window end-->

<!--right window start -->	
<!-- <div id="forfloat_right"> -->

<div id="breadCrumbLink" ></div>

<div class="cleaner"></div>
<span class='hidden'>
	<s:select name="codeListModule" id="codeListModule" list="moduleList" headerKey="" headerValue="Select" listKey="subType" listValue="description"/>
</span>
<div  id="taskLib">		
	<div  class="portlet">
	<div class="portlet-header">Task Library</div>
		<form id="qcForm">
		<s:hidden id="taskLibId" name="taskLibId" value=""/>
		<div class="portlet-content" id="childDiv">
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addRows(0,'','','',0,true);"/>&nbsp;&nbsp;<label  class="cursor" onclick="addRows(0,'','','',0,false);"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="delete_TaskLib();"/>&nbsp;&nbsp;<label class="cursor"  onclick="delete_TaskLib();"><b>Delete</b></label>&nbsp;</div></td>
			    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" onclick="edit_taskLibTable()"/>&nbsp;&nbsp;<label  class="cursor" onclick="edit_taskLibTable()"><b>Edit</b></label>&nbsp;</div></td>
			    	<!-- <td style="width:20%"><div><img src = "images/icons/save3.png" style="width:16;height:16;" class="cursor" id ="" onclick="save_TaskLib()" >&nbsp;&nbsp;<label id="schild" class="cursor" onclick="save_TaskLib()"><b>Save</b></label>&nbsp;</div></td> -->
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" id="taskLibTable" class="display" >
				<thead>
			         <tr>
			            <th width="4%" id="taskLibColumn"></th>
						<th><s:text name="stafa.label.checkall"/><input type="checkbox" id="deleteallProtocol" onclick="checkall('taskLibTable',this)"></th>
						<th><s:text name="stafa.maintenance.label.name"/></th>
						<th><s:text name="stafa.label.code"/></th>
						 <th><s:text name="stafa.label.module"/></th>
						<th><s:text name="stafa.maintenance.label.comments"/></th>
						<th><s:text name="stafa.label.activated"/></th>
					</tr>
				</thead>
			</table>
		</div>
		</form>
	</div>                    	 	
</div>
	

<div class="hidden" id="tasks">       
	<div  class="portlet">
    	<div class="portlet-header"><span id="taskspan"> </span></div>           
        <form>
            <div class="portlet-content" id="childDiv">
	            <table><tr><td>&nbsp;</td></tr>
		            <tr>
		            	<td style="width:20%"><div><img src = "images/icons/addnew.jpg" style="width:16;height:16;" class="cursor" onclick="add_TaskDetails();" />&nbsp;&nbsp;<label class="cursor" onclick="add_TaskDetails()"><b>Add</b></label>&nbsp;</div></td>
						<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" onclick="delete_TaskDetails();" />&nbsp;&nbsp;<label class="cursor" onclick="delete_TaskDetails()"><b>Delete</b></label>&nbsp;</div></td>
				    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;" class="cursor" onclick="edit_TaskDetails();" />&nbsp;&nbsp;<label class="cursor" onclick="edit_TaskDetails()"><b>Edit</b></label>&nbsp;</div></td>
						<!-- <td style="width:20%"><div><img src = "images/icons/save3.png" style="width:16;height:16;" class="cursor" onclick="save_TaskDetails();" >&nbsp;&nbsp;<label class="cursor" onclick="save_TaskDetails()"><b>Save</b></label></div></td> -->
		            </tr>
	            </table>
				<table cellpadding="0" cellspacing="0" border="0" id="taskDetails" class="display" width="100%">
					<thead>
						<tr>
						 	<th width="4%" id="taskDetailsColumn"></th>
							<th><s:text name="stafa.label.checkall"/><input type="checkbox"  id="deleteallProtocol" onclick="checkall('taskDetails',this)"></th>
							<th><s:text name="stafa.maintenance.label.name"/></th>
							<th><s:text name="stafa.label.code"/></th>
							<th><s:text name="stafa.maintenance.label.comments"/></th>
							<th><s:text name="stafa.label.activated"/></th>
						</tr>
					</thead>
				</table>            
    		</div>
    	</form>
    </div>                         
</div>
