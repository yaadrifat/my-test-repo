<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


				<%-- <div class="portlet-header "><s:text name = "stafa.label.recipientReferralDocuments"/></div><br>--%>
		<div class="portlet-content">					
				<div id='<s:property value="id"/>Tabs' width="100%">
				    <ul >
				      <li><a href="#tabs-1">Current</a></li>
				      <li><a href="#tabs-2">Historic</a></li>
				    </ul>
			  <div id="tabs-1" >   
				<br>
				<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="Documents.add('<s:property value="id"/>CurrTable', '<s:property value="id"/>List', '<s:property value="id"/>', '<s:property value="id"/>', '<s:property value="id"/>', '<s:property value="id"/>')"/>&nbsp;&nbsp;<label  class="cursor" onclick="Documents.add('<s:property value="id"/>CurrTable', '<s:property value="id"/>List', '<s:property value="id"/>', '<s:property value="id"/>', '<s:property value="id"/>', '<s:property value="id"/>')"><b>Add </b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="Documents.del('<s:property value="id"/>CurrTable')" />&nbsp;&nbsp;<label class="cursor" onclick="Documents.del('<s:property value="id"/>CurrTable')"><b>Delete</b></label>&nbsp;</div></td> 
					<!-- <td style="width:20%"><div><img src = "images/icons/save3.png" style="width:16;height:16;" class="cursor" id ="" onclick="save_documentReview();" >&nbsp;&nbsp;<label id="schild" class="cursor" onclick="save_documentReview();"><b>Save</b></label>&nbsp;</div></td> -->
				</tr>
				</table>
				<s:form id="attachedDocsForm" name="attachedDocsForm" >			
				<span class="hidden">
				</span>
				<div class="tableOverflowDiv">
				<table cellpadding="0" cellspacing="0" border="0" id="<s:property value="id"/>CurrTable" class="display" width="100%">
					<thead>
						<tr>
							<th width="4%" id="<s:property value="id"/>CurrColumn"></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<%--<s:if test="%{#column=='false'}">
							 <th></th>
							</s:if>
							<s:else>
							 <th></th>
							</s:else>--%>
						</tr>
					</thead>
				</table>
				</div>
				</s:form>
				</div>	
			<div id="tabs-2" ><br/>
			  <div class="tableOverflowDiv">
			  <table cellpadding="0" cellspacing="0" border="0" id="<s:property value="id"/>HistTable" class="display" width="100%">
					<thead>
						<tr>
								<th width="4%" id="<s:property value="id"/>HistColumn"></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							<s:if test="%{#column=='false'}">
							 	<th></th>
							</s:if>
							<s:else>
							 	<th></th>
							</s:else>
						</tr>
					</thead>
				</table>
				</div>
			 </div>	
			</div>
			</div>

<script>



/***************** Document Table Header ******************/
/* Prepared Upload Document table *************
 * @param flag as boolean
 * @param donor as entity pk
 * @param type as entityType
 * @param tableId as constructed Table Id
 */

/*function constructDocumentTable(flag,donor,type,tableId, docTableHeaders) {
	//alert("construct table : "+tableId+",Donor : "+donor);
		var criteria = "";
		if(donor!=null && donor!=""){
			criteria = donor ;
		}
		var documentColumn = [ { "bSortable": false},
			                	null,
			                	null,
			                	null,
			                	null,
			                	null
				             ];
													
		var documentServerParams = function ( aoData ) {
										aoData.push( { "name": "application", "value": "stafa"});
										aoData.push( { "name": "module", "value": "recipient_DocumentReview"});
										//aoData.push( { "name": "criteria", "value": criteria});
										
										aoData.push( { "name": "param_count", "value":"1"});
										aoData.push( { "name": "param_1_name", "value":":DONOR:"});
										aoData.push( { "name": "param_1_value", "value":criteria});
										
										aoData.push( {"name": "col_1_name", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_DOCUMENTTYPE))" } );
										aoData.push( { "name": "col_1_column", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_DOCUMENTTYPE))"});
										
										aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
										aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
													
										
					};
		var documentaoColDef = [
					                  {		
					                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
				                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
				                	 			return "<input type='hidden' id='documentRef' name='documentRef' value='"+source[0] +"' ><input type='checkbox' id='docid' name='docid' value='"+source[0] +"' >";
				                	 			//return "";
				                	     }},
					                  { "sTitle": docTableHeaders[0],//"Document Title",
				                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
					                	    	 //alert('docTitle'+source.documentFileName); 
					                	 		return source[6];
					                	 }},
					                	 { "sTitle": docTableHeaders[1],//"Version #",
					                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
											    return source[7];
										}},
									  { "sTitle": docTableHeaders[2],//"Document Date",
					                		 "aTargets": [3], "mDataProp": function ( source, type, val ) { 
											    return source[2];
											    }},
									  { "sTitle": docTableHeaders[3],//"Attachment",
											 "aTargets": [4], "mDataProp": function ( source, type, val ) {
										 	   return source[1] + " &nbsp;&nbsp;<a href='#' onclick='openPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\")'> View </a>";
									  		}},
									  { "sTitle": docTableHeaders[4],//"Reviewed",
									  		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
									  			 if(source[3] ==1){
									  				 return "Reviewed";
									  			 }else{
									  				 return "";
									  			 }
										  		//return "<input type='checkbox'/>";
										  }},
									  
									];
		var documentColManager = function () {
												$("#documentreviewColumn").html($("#"+tableId+"_wrapper .ColVis"));
											};
											
		constructTable(flag,tableId,documentColManager,documentServerParams,documentaoColDef,documentColumn);
	} */
	
/* Construct Uploaded Document History Table ***************	
 
 * Prepared Upload Document table
 * @param flag as boolean
 * @param donor as entity pk
 * @param type as entityType
 * @param tableId as constructed Table Id
 */
/*function constructDocumentHistoryTable(flag,donor,type,tableId) {
 	//alert("construct table : "+tableId+",Donor : "+donor);
	var criteria = "";
	if(donor!=null && donor!=""){
		criteria = " and fk_Entity ="+donor ;
	}
	var docHistoryaoColumn =  [ { "bSortable": false},
				                	null,
				                	null,
				                	null,
				                	null,
				                	null
					            ];
	
	var docHistoryServerParams = function ( aoData ) {
									aoData.push( { "name": "application", "value": "stafa"});
									aoData.push( { "name": "module", "value": "recipient_DocumentReview_History"});
									aoData.push( { "name": "criteria", "value": criteria});
									
									aoData.push( {"name": "col_1_name", "value": "lower(DOCUMENT_FILENAME)" } );
									aoData.push( { "name": "col_1_column", "value": "lower(DOCUMENT_FILENAME)"});
									
									aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
									aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
												
									
				};
				
	var	docHistoryaoColDef = [
				                  {		
				                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
			                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
			                	 			return "<input type='hidden' id='documentRef' name='documentRef' value='"+source[0] +"' > ";
			                	 			//return "";
			                	     }},
				                  { "sTitle": "Document Title",
			                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
				                	    	 //alert('docTitle'+source.documentFileName); 
				                	 		return source[6];
				                	 }},
								  { "sTitle": "Document Date",
				                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
										    return source[2];
										    }},
								  { "sTitle": "Attachment",
										 "aTargets": [3], "mDataProp": function ( source, type, val ) {
									 	   return source[1] + " &nbsp;&nbsp;<a href='#' onclick='openPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\")'> View </a>";
								  		}},
								  { "sTitle": "Reviewed",
								  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
								  			 if(source[3] ==1){
								  				 return "Reviewed";
								  			 }else{
								  				 return "";
								  			 }
									  		//return "<input type='checkbox'/>";
									  }},
								  { "sTitle": "Version #",
				                		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
										    return source[7];
										    }}
								];
	
	var docHistoryColManager =  function () {
											$("#documentreviewColumn1").html($("#"+tableId+"_wrapper .ColVis"));
								};
								
	constructTable(flag,tableId,docHistoryColManager,docHistoryServerParams,docHistoryaoColDef,docHistoryaoColumn);

					
} */
	
	
/* add doc upload row */
/*function add_documentReview(){
	 if(checkModuleRights(STAFA_DOCUMENT,APPMODE_ADD)){
		 var docsTitle = $("#docsTitleTemp").html();
		 //alert(docsTitle);
	     var newRow ="<tr>"+
	     "<td><input type='hidden' id='documentRef' value='0' name='documentRef'/></td>"+
	     "<td><div><select id='documentTitle' name='documentTitle' style='width:200px;'>"+docsTitle+"</select></div></td>"+
	     "<td><input type='text' id='documentDate"+dateId+"' name='documentDate' class='dateEntry' /></td>"+
	   	 "<td><img src='images/file_loading.gif' id='fileUploadImage"+attachFileId+"' style='display:none'/>"+
	   	 "<div id='fileDisplayName"+attachFileId+"'></div><input type='hidden' class='fileName' id='fileName"+attachFileId+"' name='fileName"+attachFileId+"'/><input type='hidden' class='filePathId' name='filePathId"+attachFileId+"' id='filePathId"+attachFileId+"'/> <input type='hidden' class='fileUploadContentType' name='fileUploadContentType"+attachFileId+"' id='fileUploadContentType"+attachFileId+"'/>"+
	   	 "<div class='"+attachFileId+"' style='cursor: pointer;	color: #0101DF;' id='displayAttach"+attachFileId+"'></div><input size='10' style='display:block' class='"+attachFileId+"' type='file' name='fileUpload' onChange='ajaxFileUpload1(this,\""+attachFileId+"\")' id='attach"+attachFileId+"'/><span><s:text name='stafa.fileupload.message'/></span></td>"+
	     "<td></td>"+
	     "</tr>";
	     $("#documentreviewTable tbody").prepend(newRow);
	   
	         $("#documentDate"+dateId).datepicker({dateFormat: 'M dd, yy',changeMonth: true, changeYear: true, yearRange: '1900:' + new Date().getFullYear()});
	     dateId=dateId+1;
	     $.uniform.restore('select');
		 $('select').uniform();
		
		 attachFileId=attachFileId+1;
		 var param ="{module:'APHERESIS',page:'DONOR_DETAILS'}";
			markValidation(param);
	 }
}*/

/****  Delet Document ******/
 
/* function delete_documentReview(){
	 if(checkModuleRights(STAFA_DOCUMENT,APPMODE_DELETE)){  	
		 var deletedIds="";
			try{
			$("#documentreviewTable tbody tr").each(function (row){
				 if($(this).find("#docid").is(":checked")){
					deletedIds += $(this).find("#docid").val() + ",";
				}
			 });
			//	alert("deletedIds "+ deletedIds);
			  if(deletedIds.length >0){
				 var yes=confirm(confirm_deleteMsg);
				 if(!yes){
								return;
				 }
				  deletedIds = deletedIds.substr(0,deletedIds.length-1);
				  //alert("deletedIds " + deletedIds);
				  jsonData = "{deletedIds:'"+deletedIds+"',domainName:'DOMAIN_DOCUMENT',domainKey:'DOCUMENT_ID'}";
				  url="commonDelete";
			     
			     response = jsonDataCall(url,"jsonData="+jsonData);
			 	var donor = $("#donor").val();
				var entityType = $("#entityType").val();
				constructDocumentTable(true,donor,entityType,'documentreviewTable');
			    
			 	
			  }
			}catch(err){
				alert(" error " + err);
			}
	 }
 }
*/

</script>