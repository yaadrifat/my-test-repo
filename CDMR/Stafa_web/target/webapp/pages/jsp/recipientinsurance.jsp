<%@ include file="common/includes.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<link rel="stylesheet" href="css/cssmenuhorizontal.css" type="text/css" />
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript">
$(document).ready(function(){
fn_resizable();
$("select").uniform(); 	
$(window).resize();
constructInsuranceProviderTable(false);
});



//script to load calendar on date fields
$( "#mcrpeffdate,#subscriberdob,#verifieddate,#effectivedate,#termdate,#networkStartDate,#networkEndDate,#asOfInDate,#asOfOutDate,#dateSeenByRCA" ).datepicker({
                                        dateFormat: 'M dd, yy',
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "c-50:c+45"
                                    });	
//function to make div collapseable									

function fn_resizable(){
	$(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
		.find(".portlet-header")
			.prepend('<span class="ui-icon ui-icon-minusthick"></span>')
			.end()
		.find(".portlet-content");
		var flag=true;
		var divHeight;
	$(".portlet-header .ui-icon").bind("click",function(){
		$(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
		$(this).parents(".portlet:first").find(".portlet-content").toggle();
		var divid = $(this).parent().parent().attr("id");
		if(flag)
		divHeight=document.getElementById(divid).style.height;
		if($(this).is(".ui-icon-minusthick")){
		$(this).parents(".portlet:first").css('height',divHeight);
		}
		flag=false;			
		});
}	

//function to add new row 
var rowIndex=0;
function addRecipientInsurance(){
	var newRow;
	rowIndex=rowIndex+1;
	newRow="<tr>"+
			//"<td><input type='checkbox' id='recipientInsuranceCheckBox' name='recipientInsuranceCheckBox' value='0'></td>"+
			"<td><input type='radio' id='insuranceViewactive' name='insuranceViewactive' onclick='openRecipientInsDetails()' checked='checked' value='0'>"+
			"<td>"+$('#providerNameSelectDiv').html()+"</td>"+
			"<td><input type='radio' id='isPrimary' name='isPrimary"+rowIndex+"' value='P'></td>"+
			"<td><input type='radio' id='isSecondary' name='isPrimary"+rowIndex+"' value='S'></td>"+
			"<td>"+$("#planStatusSelectDiv").html()+"</td>"+
			"<td><input type='text' id='dateOfStatusChange"+rowIndex+"' name='dateOfStatusChange' class='dateEntry'/></td>"+
			"</tr>";
		$("#recipientInsurance tbody").prepend(newRow);										
		$('input[name="dateOfStatusChange"]').datepicker({
                                        dateFormat: 'M dd, yy',
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "c-50:c+45"
 
					});
   		$.uniform.restore('select');
		$("select").uniform(); 
		openRecipientInsDetails();
}		

//function to save Insurance details 
function saveInsuranceDetails(){
var row,insPersonId,refProviderCompany, planStatus, statuschangeDate,isPrimaryIns,entityType,employmentId;	
	var rowData = "";
    $("#recipientInsurance").find("#providerNameSelect").each(function(){
	    row=$(this).closest("tr");
	    insPersonId = $(row).find('input[name="insuranceViewactive"]').val();
		refProviderCompany = $(row).find("#providerNameSelect").val();
		refPlanStatus = $(row).find('#planStatusSelect').val();	
		statuschangeDate = $(row).find('input[name="dateOfStatusChange"]').val(); 
		entityId=personIdOnIns;
		entityType="Person";
		employmentId=$("#employmentPk").val();
		if($(row).find("#isPrimary").is(':checked')){
			isPrimaryIns=$("#isPrimary").val();
			}else{
			isPrimaryIns=$("#isSecondary").val();
			}
	  if($(row).find("#insuranceViewactive").is(':checked')){
	    $.ajax({
	        "type": "POST",
	        "url": "saveCompleteInsuranceInfo.action?entityId="+personIdOnIns+"&refProviderCompany="+refProviderCompany+"&refPlanStatus="+refPlanStatus+"&statuschangeDate="+statuschangeDate+"&entityType="+entityType+"&isPrimaryIns="+isPrimaryIns+"&insPersonId="+insPersonId+"&employmentPk="+employmentId,
			"data": $("#recipientInsuranceDetailsForm,#networkDetailsForm,#patientBenefiForm,#employmentInfoForm").serialize(),
	        "async":false,
	        "success": function (result){
					var $response=$(result);
                   $('#recipientInsuranceDetailsDiv').html($(result).find('#recipientInsuranceDetailsDiv').html());
				   $('#networkDetailsDiv').html($(result).find('#networkDetailsDiv').html());
				   $('#employmentInfoDiv').html($(result).find('#employmentInfoDiv').html());
				   $('#patientBenefitsDiv').html($(result).find('#patientBenefitsDiv').html());
	        }
	       });
	   }else{
		//alert(rowData);
		rowData+= "{insPersonId:'"+insPersonId+"',refProviderCompany:'"+refProviderCompany+"',refPlanStatus:'"+refPlanStatus+"',statuschangeDate:'"+statuschangeDate+"',entityId:'"+entityId+"',isPrimaryIns:'"+isPrimaryIns+"',entityType:'"+entityType+"'},";		
		if(rowData.length >0 && insPersonId ==0){
	        rowData = rowData.substring(0,(rowData.length-1));
	 	var url="saveInsuranceDetails";
		response = jsonDataCall(url,"jsonData={data:["+rowData+"]}");	
		}
		}
		});
		alert("Data Saved Successfully");
		constructInsuranceProviderTable(true); 
}		

function constructInsuranceProviderTable(flag){
    var recipientIns="";           
	recipientInsId = personIdOnIns;
	var recipientInsProviderListTable_serverParam = function ( aoData ) {

		aoData.push( { "name": "application", "value": "stafa"});
		aoData.push( { "name": "module", "value": "insurance_recipient"});	
        aoData.push( { "name": "criteria", "value": recipientInsId});
		 
		 
		/*aoData.push( {"name":  "col_0_name", "value": "pkpersonins" } );
		aoData.push( { "name": "col_0_column", "value": "0"});*/
		
		aoData.push( { "name": "col_0_name", "value": "view"});
		aoData.push( { "name": "col_0_column", "value": "0"}); 
		
		aoData.push( {"name": "col_1_name", "value": "lower(providername)" } );
		aoData.push( { "name": "col_1_column", "value": "1"});
		
		aoData.push( { "name": "col_2_name", "value": "lower(primarystatus)"});
		aoData.push( { "name": "col_2_column", "value": "2"});
		
		aoData.push( { "name": "col_3_name", "value": "lower(secondarystatus)"});
		aoData.push( { "name": "col_3_column", "value": "2"});
		
		aoData.push( { "name": "col_4_name", "value": "lower(planstatus)"});
		aoData.push( { "name": "col_4_column", "value": "3"});
		
		aoData.push( { "name": "col_5_name", "value": "dateofstatuschange"});
		aoData.push( { "name": "col_5_column", "value": "5"});
		
		/*aoData.push( { "name": "col_6_name", "value": "view"});
		aoData.push( { "name": "col_6_column", "value": "0"});*/
		};

													
	var recipientInsListTable_aoColumnDef = [
			                         /*{ "aTargets": [0], "mDataProp": function ( source, type, val ) {
                                    	 return "<input  type='checkbox' id='recipientInsuranceCheckBox"+ source[0] +"' name='recipientInsuranceCheckBox' value='" + source[0] +"' >";
                                 }}*/
								 {    
                                     "aTargets": [0], "mDataProp": function ( source, type, val ) {
                                         return "<input  type='radio' id='insuranceViewactive' name='insuranceViewactive' value='" + source[0] +"' onclick='openRecipientInsDetails("+source[0]+")' />";
                                  }},
                                 {"sTitle":"<s:text name='stafa.label.providerName'/>",
                                    "aTargets": [1], "mDataProp": function ( source, type, val ) {
                                        return source[2];
                                 }},
                                 {    "sTitle":"<s:text name='stafa.label.primary'/>",
                                     "aTargets": [2], "mDataProp": function ( source, type, val ) {
									 if(source[3]=='P'){
                                        return "<input  type='radio' checked='checked' id='isPrimary' name='isPrimary"+ source[0] +"' value='" + source[3] +"'/>";
										}else{
										return "<input  type='radio'  id='isPrimary' name='isPrimary"+ source[0] +"' value='" + source[3] +"'/>";
										}
                                  }},
                                 {    "sTitle":"<s:text name='stafa.label.secondary'/>",
                                     "aTargets": [3], "mDataProp": function ( source, type, val ) {
                                     if(source[3]=='S'){
										 return "<input  type='radio' checked='checked' id='isSecondary' name='isPrimary"+ source[0] +"' value='" + source[3] +"'/>";
										}else{
										return "<input  type='radio' id='isSecondary' name='isPrimary"+ source[0] +"' value='" + source[3] +"'/>";
										}
								 }},
                                 {    "sTitle":"<s:text name='stafa.label.planstatus'/>",
                                     "aTargets": [4], "mDataProp": function ( source, type, val ) {
                                         return source[5];
                                  }},
								   {    "sTitle":"<s:text name='stafa.label.dateofstatuschange'/>",
                                     "aTargets": [5], "mDataProp": function ( source, type, val ) {
                                         return $.datepicker.formatDate('M dd, yy', new Date(source[6]));
                                  }}/*,
                                  {    "sTitle":"<s:text name='stafa.label.view'/>",
                                     "aTargets": [6], "mDataProp": function ( source, type, val ) {
                                         return "<input  type='radio' id='insuranceViewactive' name='insuranceViewactive' value='" + source[0] +"' onclick='openRecipientInsDetails("+source[0]+")' />";
                                  }}*/
			                        
									];
   
	var recipientInsListTable_aoColumn = [ { "bSortable": false},
	                               	  null,
						               null,
						               null,
						               null,
						               null
						];
	//?BB var column_sort = [[ 1, "asc" ]];
	var column_sort = [];
	var recipientInsListTable_ColManager = function(){
										$("#recipientInsuranceColumn").html($('.ColVis:eq(0)'));
									 };
	var sDomVal = 'C<"clear">Rlfrtip';
	constructTable(flag,"recipientInsurance",recipientInsListTable_ColManager,recipientInsProviderListTable_serverParam,recipientInsListTable_aoColumnDef,recipientInsListTable_aoColumn);
}

//function to view open recipient details div	

function openRecipientInsDetails(id){
	if(id){
		var url = "loadRecipientInsViewDetails.action?personInsId="+id;
	    var result = ajaxCall(url);
		//$("#main").html(result); 	
		$('#recipientInsuranceDetailsDiv').html($(result).find('#recipientInsuranceDetailsDiv').html());
		$('#networkDetailsDiv').html($(result).find('#networkDetailsDiv').html());
		$('#employmentInfoDiv').html($(result).find('#employmentInfoDiv').html());
		$('#patientBenefitsDiv').html($(result).find('#patientBenefitsDiv').html());
		$("select").uniform(); 	
	    $('#insuranceWidgetsDiv').css('display','block');

	    RecipInsDocParam.entityId = id;
	    PatBenDocParam.entityId = id;
	  //Recipient Insurance Document Widget
	    DocTable.currentTableExHead(true, RecipInsDocParam.entityId, RecipInsDocParam.entityType, RecipInsDocParam.txId, RecipInsDocParam.visitId, RecipInsCurrentDoc.TABLE, docTableHeaders, RecipInsCurrentDoc.TABLECol, RecipInsCurrentDoc.TABLEIdPrefix, RecipInsDocParam.widgetType);
		DocTable.historyTableExHead(true, RecipInsDocParam.entityId, RecipInsDocParam.entityType, RecipInsDocParam.txId, RecipInsDocParam.visitId, RecipInsHistDoc.TABLE, docTableHeaders, RecipInsHistDoc.TABLECol, RecipInsHistDoc.TABLEIdPrefix, RecipInsDocParam.widgetType);

	    
	 // Patient Benifit Documents
		DocTable.currentTableExHead(true, PatBenDocParam.entityId, PatBenDocParam.entityType, PatBenDocParam.txId, PatBenDocParam.visitId, PatBenDocCurrentDoc.TABLE, docTableHeaders, PatBenDocCurrentDoc.TABLECol, PatBenDocCurrentDoc.TABLEIdPrefix, PatBenDocParam.widgetType);
		DocTable.historyTableExHead(true, PatBenDocParam.entityId, PatBenDocParam.entityType, PatBenDocParam.txId, PatBenDocParam.visitId, PatBenDocHistDoc.TABLE, docTableHeaders, PatBenDocHistDoc.TABLECol, PatBenDocHistDoc.TABLEIdPrefix, PatBenDocParam.widgetType);
    $( "#mcrpeffdate,#subscriberdob,#verifieddate,#effectivedate,#termdate,#networkStartDate,#networkEndDate,#asOfInDate,#asOfOutDate,#dateSeenByRCA" ).datepicker({
                                        dateFormat: 'M dd, yy',
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "c-50:c+45"
                                    });		
	}else{
	    $(':input','#recipientInsuranceDetailsForm,#networkDetailsForm,#patientBenefiForm,#employmentInfoForm').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').attr('selected', 'selected');
		$('#insuranceWidgetsDiv').css('display','block');
	}
}

function savePages(mode){
	if(checkModuleRights(STAFA_RECIPIENTTRACKERS,APPMODE_SAVE)){
		try{
			if(saveInsuranceDetails()){
			}
			if(savePersonFundCaseManagement()){
			}
			if(Documents.saveDocs(RecipInsDocParam, PatBenDocParam, RecipInsCurrentDoc, PatBenDocCurrentDoc)){								
			}
		}catch(e){
			alert("exception " + e);
		}
	}
}

$(window).resize(function(){
    var height;
    var screenheight=screen.height;
    var innerheight=$("#innercenter").height();
    var innerheight=594;
    var constanthgt=$("#constantdiv").height();
    /* if(innerheight) */
    if(screenheight<= 768){
       height =innerheight -(constanthgt+240);
    }else if(screenheight>=1024){
           height =innerheight -(constanthgt+40);

    }else if(screenheight>=800 && screenheight < 900){
           height =innerheight -(constanthgt+240);
    }else if(screenheight==900){
           height =innerheight -(constanthgt+130);
    }else if(screenheight==960){
           height =innerheight -(constanthgt+80);
    }
       $('#subdiv').height(height);
    });
	
/*function saveEmploymentInfo(){
$.ajax({
	        "type": "POST",
	        "url": "saveInsEmploymentInfo.action?entityId="+personIdOnIns,
			"data": $("#employmentInfoForm").serialize(),
	        "async":false,
	        "success": function (result){
					var $response=$(result);
                   $("#main").html(result); 
	        }
	       });
}	*/	

function savePersonFundCaseManagement(){
var caseMgmtId=$("#caseMgmtPk").val();
$.ajax({
	        "type": "POST",
	        "url": "savePersonFundCaseManagement.action?entityId="+personIdOnIns+"&caseMgmtPk="+caseMgmtId,
			"data": $("#caseMgmtForm").serialize(),
	        "async":false,
	        "success": function (result){
				  var $response=$(result);
                  $('#caseMgmtDiv').html($(result).find('#caseMgmtDiv').html());
	        }
	       });
}	

var deletedflag=false;

function deleteRecipientInsurance(){
	   deletedflag=false;
		var deletedIds = "";
			try{
				$("input[name='insuranceViewactive']:checked").each(function(){
					row=$(this).closest("tr");
					if($(row).find('input[name="insuranceViewactive"]').val() != 0)
					{
				     deletedIds += $(row).find('input[name="insuranceViewactive"]').val() + ",";
					 deletedflag=true;
				    }
			});
			if(deletedIds.length >0){
			  deletedIds = deletedIds.substr(0,deletedIds.length-1);
			}
			if(deletedflag!=false){
				 var yes=confirm(confirm_deleteMsg);
					if(!yes){
						return;
					}
			jsonData = "{deletedIds:'"+deletedIds+"'}";
			url="deleteRecipientInsurance";
		   response = jsonDataCall(url,"jsonData="+jsonData);
		  constructInsuranceProviderTable(true);
			}
			if(deletedflag==false){
				alert("Please Select the Record");
			}
			deletedflag=false;
		   
			}catch(error){
				alert("error " + error);
			}

	}	
	
function editRecipientInsurance(){			
	var editFlag=false;
			$("#recipientInsurance tbody tr").each(function (i,row){
				 if($(this).find('input[name="insuranceViewactive"]').is(":checked")){
					 $(this).find("td").each(function (col){
						 if(col ==0){
			  	 		  }else if(col ==1 ){			
			  	 			 if ($(this).find("#providerNameSelect").val()== undefined){
								 	var providerNameSelectDiv = $(this).html();
								 	$(this).html($("#providerNameSelectDiv").html());
								 	$(this).find("#providerNameSelect option:contains("+providerNameSelectDiv+")").attr('selected', 'selected');
									$.uniform.restore($(this).find("#providerNameSelect"));
									$(this).find("#providerNameSelect").uniform(); 	
							 }   
						 }else if(col ==4){
							 if ($(this).find("#planStatusSelect").val()== undefined){
								 	var planStatusSelectDiv = $(this).html();
								 	$(this).html($("#planStatusSelectDiv").html());
								 	$(this).find("#planStatusSelect option:contains("+planStatusSelectDiv+")").attr('selected', 'selected');
									$.uniform.restore($(this).find("#planStatusSelect"));
									$(this).find("#planStatusSelect").uniform(); 	
							 }  
						 }else if(col ==5){
							 if ($(this).find(".startDate").val()== undefined){
								 	$(this).html("<input type='text' id='dateOfStatusChange"+i+"' name='dateOfStatusChange' class='startDate dateEntry dateclass' value='"+$(this).text()+ "'/>")
								  	$("#dateOfStatusChange"+i).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
								        changeYear: true, yearRange: "c-50:c+45"});
								 }
						 }
					 });
					 editFlag=true;
				 }
				 
			  });
			if(editFlag==false){
				alert("Please Select the Record");
			}
		}

	
</script>



<div id="providerNameSelectDiv" class="hidden">
<s:select id="providerNameSelect"  name="providerName" list="lstProviderName" listKey="pkInsMaintenance" listValue="insCompanyName"  headerKey="" headerValue="Select" />				
</div>
<div id="planStatusSelectDiv" class="hidden">
<s:select id="planStatusSelect"  name="insPlanStatus" list="lstInsPlanStatus" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" />		
</div>

		
<div align="right" style="float:right;">

		<!--<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
			<tr>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;&nbsp;</td>
				<td><input type="button" value="Save" onclick="saveInsuranceDetails()"/></td>
			</tr>
		</table> -->

</div>
<div class="cleaner"></div>
<div id="recipientInsuranceDiv" >
<form>
	<div class="column">
		<div class="portlet">
			<div class="portlet-header">Recipient Insurance</div>
			<div class="portlet-content">
				<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="addRecipientInsurance();"/>&nbsp;&nbsp;<label  class="cursor" onclick="addRecipientInsurance();"><b>Add </b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteRecipientInsurance();"/>&nbsp;&nbsp;<label class="cursor" onclick="deleteRecipientInsurance();" ><b>Delete</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/edit.png" class="cursor" style="width:16;height:16;cursor:pointer;" onclick='editRecipientInsurance();'/>&nbsp;&nbsp;<label class="cursor" onclick='editRecipientInsurance();'><b>Edit</b></label>&nbsp;</div></td>
				</tr>
				</table>
				<div id="recipientInsuranceDataTableDiv"
				style="overflow-x: auto; overflow-y: hidden;">
				<table cellpadding="0" cellspacing="0" border="1"
					id="recipientInsurance" class="display">
					<thead>
						<tr>
							<th   width="4%" id="recipientInsuranceColumn" colspan='1'>Select</th>
							<th colspan='1'><s:text name='stafa.label.providerName'/></th>
							<th colspan='1'><s:text name='stafa.label.primary'/></th>
							<th colspan='1'><s:text name='stafa.label.secondary'/></th>
							<th colspan='1'><s:text name='stafa.label.planstatus'/></th>
							<th colspan='1'><s:text name='stafa.label.dateofstatuschange'/></th>
							<!--<th colspan='1'><s:text name='stafa.label.view'/></th>-->
						</tr>
					</thead>
					<tbody>
					
					</tbody>
				</table>
				</div>
			</div>
		</div>
	</div>
</form>
</div>
<div id="insuranceWidgetsDiv" style="display:none;">

<div id="recipientInsuranceDocsDiv">
<s:set var="id" value="%{'recipientInsuranceDocuments'}" />
 <div class="column">
	<div class="portlet">
		<div class="portlet-header"><s:text name="stafa.label.recipientinsdocs"/></div>
		<span class="hidden">
					<s:select id="%{#attr['id']+'List'}" name="recipientInsueDocsList1" list="recipientInsueDocs"  headerKey="" headerValue="Select"/>
				</span>
		<jsp:include page="doc_activity.jsp"></jsp:include>
	</div>	
 </div>
</div>
<script  type="text/javascript">

//Documents Scripts

/* Category mean widget id */

var RecipInsDocParam={		 
		
			 widgetType : <s:property value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@TYPE_INSURANCE, @com.velos.stafa.util.VelosStafaConstants@SUBTYPE_RECIPIENTINSDOC)"/>,
			 entityId   : '',
			 entityType : '<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_PERSON"/>',
			 txId       : '<s:property value="transplantId"/>',
			 visitId    : ''//'<s:property value="visitId"/>'  

 };

 var RecipInsCurrentDoc={

	     TABLE: '<s:property value="id"/>CurrTable',
		 TABLECol : '<s:property value="id"/>CurrColumn',
		 TABLEIdPrefix : '<s:property value="id"/>'

 };

 var RecipInsHistDoc={

	     TABLE: '<s:property value="id"/>HistTable',
		 TABLECol : '<s:property value="id"/>HistColumn',
		 TABLEIdPrefix : '<s:property value="id"/>'
 };

	/*	var TABLE_1= '<s:property value="id"/>CurrTable';
		var TABLE_1HIST = '<s:property value="id"/>HistTable';
		var TABLE_1HISTDispCol = '<s:property value="id"/>HistColumn';
		var TABLE_1DispCol = '<s:property value="id"/>CurrColumn';
		var TABLE_1IdPrefix = '<s:property value="id"/>';
		var widgetType=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@TYPE_INSURANCE, @com.velos.stafa.util.VelosStafaConstants@SUBTYPE_RECIPIENTINSDOC)"/>;
		var entityId;
		var entityType='<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_INSURANCE"/>';
      */

 var docTableHeaders = new Array();
	    docTableHeaders[0]='<s:text name = "stafa.lable.documentTitle"/>';
	    docTableHeaders[1]='<s:text name = "stafa.lable.documentVersion"/>';
	    docTableHeaders[2]='<s:text name = "stafa.lable.documentDate"/>';
	    docTableHeaders[3]='<s:text name = "stafa.lable.Attachment"/>';
	    docTableHeaders[4]='Status';

	$('#<s:property value="id"/>Tabs' ).tabs();	    




function constructDocTable(table){
	if(table==RecipInsCurrentDoc.TABLE){
		DocTable.currentTableExHead(true, RecipInsDocParam.entityId, RecipInsDocParam.entityType, RecipInsDocParam.txId, RecipInsDocParam.visitId, table, docTableHeaders, RecipInsCurrentDoc.TABLECol, RecipInsCurrentDoc.TABLEIdPrefix, RecipInsDocParam.widgetType);
		
		
	}else if(table==PatBenDocCurrentDoc.TABLE){
		
		DocTable.currentTableExHead(true, PatBenDocParam.entityId, PatBenDocParam.entityType, PatBenDocParam.txId, PatBenDocParam.visitId, table, docTableHeaders, PatBenDocCurrentDoc.TABLECol, PatBenDocCurrentDoc.TABLEIdPrefix, PatBenDocParam.widgetType);	
	}
	
}

//preparing document widget
//constructDocumentTable(false, entityId, entityType, TABLE_1, docTableHeaders, TABLE_1DispCol, TABLE_1IdPrefix,widgetType);
//constructDocumentHistoryTable(false,entityId,entityType, TABLE_1HIST, TABLE_1HISTDispCol, TABLE_1IdPrefix, widgetType);
</script>

<div id="recipientInsuranceDetailsDiv">
<form id="recipientInsuranceDetailsForm">
<div class="column">
		<div class="portlet">
			<div class="portlet-header">Recipient Insurance Details</div>
			<div class="portlet-content">
				<table cellpadding="0" cellspacing="0" border="0"
					id="recipientInsuranceDetails" class="display">
					<tbody>
						<tr>
							<td><s:text name="stafa.label.subscriberrelation"/></td>
							<td><s:select id="subscriberRelationSelect"  name="recipientInsViewDetails.subscriberRelation" list="lstInsSuscriberreltion" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
							<td><s:text name="stafa.label.companyInfoContactPerson"/></td>
							<td><s:textfield value = "%{recipientInsViewDetails.insCmpnyCntctPerInfo}" id="insCmpnyCntctPerInfo" name="recipientInsViewDetails.insCmpnyCntctPerInfo"/></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.subscriberlastname"/></td>
							<td><s:textfield value = "%{recipientInsViewDetails.subscriberLName}" name="recipientInsViewDetails.subscriberLName"/></td>
							<td><s:text name="stafa.label.companyInfoPhoneNumber"/></td>
							<td><s:textfield value = "%{recipientInsViewDetails.insCmpnyCntctPerPhNo}" name="recipientInsViewDetails.insCmpnyCntctPerPhNo"/></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.subscriberfirstname"/></td>
							<td><s:textfield value = "%{recipientInsViewDetails.subscriberFName}" name="recipientInsViewDetails.subscriberFName"/></td>
							<td><s:text name="stafa.label.mcrpartdeffdateentry"/></td>
							<td><s:textfield value = "%{recipientInsViewDetails.insMcrpDate}" id="mcrpeffdate" name="recipientInsViewDetails.insMcrpDate" cssClass="dateEntry calDate"/></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.subscriberdob"/></td>
							<td><s:textfield value = "%{recipientInsViewDetails.subscriberDOB}" id="subscriberdob" name="recipientInsViewDetails.subscriberDOB" cssClass="dateEntry calDate"/></td>
							<td><s:text name="stafa.label.groupname"/></td></td>
							<td><s:textfield name="recipientInsViewDetails.insGroupName" value = "%{recipientInsViewDetails.insGroupName}" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.subscriberssn"/></td>
							<td><s:textfield name="recipientInsViewDetails.subscriberSSN" value = "%{recipientInsViewDetails.subscriberSSN}" /></td>
							<td><s:text name="stafa.label.groupnumber"/></td>
							<td><s:textfield name="recipientInsViewDetails.insGroupNo" value = "%{recipientInsViewDetails.insGroupNo}" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.globalStatus"/></td>
							<td><s:select id="globalStatusSelect"  name="recipientInsViewDetails.refInsGlobalStatus" list="lstInsGlobalStatus" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
							<td><s:text name="stafa.label.insverified"/></td>
							<td><s:radio name="recipientInsViewDetails.isVerified" list="#{'1':'Yes','0':'No'}" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.globalplan"/></td>
							<td><s:select id="globalplanSelect"  name="recipientInsViewDetails.refInsGlobalPlan" list="lstInsGlobalPlan" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
							<td><s:text name="stafa.label.verifiedby"/></td>
							<td><s:select id="verifiedById" name="recipientInsViewDetails.refPerInsVerifiedBy" list="lstPhysicianName"  listKey="" headerKey="" headerValue="Select"/></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.payerLevel"/></td>
							<td><s:select id="payerLevelSelect"  name="recipientInsViewDetails.refInsPayerLevel" list="lstInsPayerLevel" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
							<td><s:text name="stafa.label.verifieddate"/></td>
							<td><s:textfield name="recipientInsViewDetails.insVerificationDate" value = "%{recipientInsViewDetails.insVerificationDate}" id="verifieddate" cssClass="dateEntry calDate"/></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.paymentMethodology"/></td>
							<td><s:select id="paymentMethodologySelect"  name="recipientInsViewDetails.refInspaymentMethod" list="lstInsPaymentMethod" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
							<td><s:text name="stafa.label.rxBenefitPlan"/></td>
							<td><s:textfield name="recipientInsViewDetails.rxBenifitPlan" value = "%{recipientInsViewDetails.rxBenifitPlan}" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.idNumber"/></td>
							<td><s:textfield name="recipientInsViewDetails.persInsIdNo" value = "%{recipientInsViewDetails.persInsIdNo}" /> </td>
							<td><s:text name="stafa.label.rxPlanPhone"/></td>
							<td><s:textfield name="recipientInsViewDetails.insRxPlanPhNo" value = "%{recipientInsViewDetails.insRxPlanPhNo}" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.effetivedate"/></td>
							<td><s:textfield name="recipientInsViewDetails.insEffectiveDate" value = "%{recipientInsViewDetails.insEffectiveDate}" id="effectivedate"  cssClass="dateEntry calDate"/></td>
							<td><s:text name="stafa.label.inpatientadmissionsphonenumber"/></td>
							<td><s:textfield name="recipientInsViewDetails.insInPatAdmissionPhNo" value = "%{recipientInsViewDetails.insInPatAdmissionPhNo}" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.termdate"/></td>
							<td><s:textfield name="recipientInsViewDetails.insTermDate" value = "%{recipientInsViewDetails.insTermDate}" id="termdate" cssClass="dateEntry calDate"/></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
						<td><s:text name="stafa.label.notes"/></td>
						<td><s:textarea name="recipientInsViewDetails.insNotes" value = "%{recipientInsViewDetails.insNotes}" label="notes" cols="40" rows="4"/></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</form>
</div>

<div id="networkDetailsDiv">
<form id="networkDetailsForm">
<div class="column">
		<div class="portlet">
			<div class="portlet-header">Network Details</div>
			<div class="portlet-content">
				<table cellpadding="0" cellspacing="0" border="0"
					id="networkDetails" class="display">
					<tbody>
						<tr>
							<td><s:text name="stafa.label.networkname"/></td>
							<td><s:textfield name="recipientInsViewDetails.insNetworkName" value = "%{recipientInsViewDetails.insNetworkName}" /></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.networkstartdate"/></td>
							<td><s:textfield name="recipientInsViewDetails.insNetworkStartDate"  value = "%{recipientInsViewDetails.insNetworkStartDate}" id="networkStartDate" cssClass="dateEntry calDate"/></td>
							<td><s:text name="stafa.label.networkenddate"/></td>
							<td><s:textfield name="recipientInsViewDetails.insNetworkEndDate" value = "%{recipientInsViewDetails.insNetworkEndDate}" id="networkEndDate"  cssClass="dateEntry calDate"/></td>
						</tr>
						<tr>
							<td></td>
							<td><s:text name="stafa.label.in"/></td>
							<td></td>
							<td><s:text name="stafa.label.out"/></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.copay"/></td>
							<td><s:textfield name="recipientInsViewDetails.inCopay" value = "%{recipientInsViewDetails.inCopay}" /></td>
							<td></td>
							<td><s:textfield name="recipientInsViewDetails.outCoPay" value = "%{recipientInsViewDetails.outCoPay}" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.transplantlifetimebenifit"/></td>
							<td><s:textarea name="recipientInsViewDetails.inLifeTimeBenifits" value = "%{recipientInsViewDetails.inLifeTimeBenifits}" label="notes"  cols="20" rows="4"/></td>
							<td></td>
							<td><s:textarea name="recipientInsViewDetails.outLifeTimeBenifits"  value = "%{recipientInsViewDetails.outLifeTimeBenifits}" label="notes" cols="20" rows="4"/></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.annualdeductible"/></td>
							<td><s:textfield name="recipientInsViewDetails.inAnnualDeductable" value = "%{recipientInsViewDetails.inAnnualDeductable}" /></td>
							<td></td>
							<td><s:textfield name="recipientInsViewDetails.outAnnualDeductable" value = "%{recipientInsViewDetails.outAnnualDeductable}" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.annualdeductiblemet"/></td>
							<td><s:textfield name="recipientInsViewDetails.inAnnualDeductableMet" value = "%{recipientInsViewDetails.inAnnualDeductableMet}" /></td>
							<td></td></td>
							<td><s:textfield name="recipientInsViewDetails.outAnnualDeductableMet" value = "%{recipientInsViewDetails.outAnnualDeductableMet}" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.asofdate"/></td>
							<td><s:textfield name="recipientInsViewDetails.inAsOfDate" value = "%{recipientInsViewDetails.inAsOfDate}" id="asOfInDate"  cssClass="dateEntry calDate"/></td>
							<td></td>
							<td><s:textfield name="recipientInsViewDetails.outAsOfDate" value = "%{recipientInsViewDetails.outAsOfDate}" id="asOfOutDate"  cssClass="dateEntry calDate"/></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.outofpocket"/></td>
							<td><s:textfield name="recipientInsViewDetails.inoutOfPocket" value = "%{recipientInsViewDetails.inoutOfPocket}" /></td>
							<td></td>
							<td><s:textfield name="recipientInsViewDetails.outOutOfPocket" value = "%{recipientInsViewDetails.outOutOfPocket}" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.outofpocketmet"/></td>
							<td><s:textfield name="recipientInsViewDetails.inoutOfPocket" value = "%{recipientInsViewDetails.inoutOfPocketMet}" /></td>
							<td></td>
							<td><s:textfield name="recipientInsViewDetails.outOutOfPocketMet" value = "%{recipientInsViewDetails.inoutOfPocketMet}" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.lifetimemax"/></td>
							<td><s:textfield name="recipientInsViewDetails.inLifeTimemax" value = "%{recipientInsViewDetails.inLifeTimemax}" /></td>
							<td></td>
							<td><s:textfield name="recipientInsViewDetails.outLifeTimeMax" value = "%{recipientInsViewDetails.outLifeTimeMax}" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.physcopay"/></td>
							<td><s:textfield name="recipientInsViewDetails.inPhysCopay" value = "%{recipientInsViewDetails.inPhysCopay}" /></td>
							<td></td>
							<td><s:textfield name="recipientInsViewDetails.outPhysCoPay" value = "%{recipientInsViewDetails.outPhysCoPay}" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.insurancepatientresponsibility"/></td>
							<td><s:textarea name="recipientInsViewDetails.inInsResponsibility" value = "%{recipientInsViewDetails.inInsResponsibility}" label="notes"  cols="20" rows="4"/></td>
							<td></td>
							<td><s:textarea name="recipientInsViewDetails.outInsResponsibility" value = "%{recipientInsViewDetails.outInsResponsibility}" label="notes" cols="20" rows="4"/></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.roomandboard"/></td>
							<td><s:select id="roomandboardSelectIn"  name="recipientInsViewDetails.refInRoomAndBoard" list="lstRoomAndBoard" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
							<td></td>
							<td><s:select id="roomandboardSelectOut"  name="recipientInsViewDetails.refOutRoomAndBoard" list="lstRoomAndBoard" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
						</tr>
						<tr>
						<td><s:text name="stafa.label.comments"/></td>
						<td><s:textarea name="recipientInsViewDetails.insNetworkNotes" value = "%{recipientInsViewDetails.insNetworkNotes}" label="notes"  cols="40" rows="4"/></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</form>
</div>

<div id="employmentInfoDiv">
<form id="employmentInfoForm">
<s:hidden  id="employmentPk"  value="%{employmentDetails.pkEmploymentinfo}" />
<div class="column">
		<div class="portlet">
			<div class="portlet-header">Employment Info</div>
			<div class="portlet-content">
				<table cellpadding="0" cellspacing="0" border="0"
					id="employmentInfo" class="display">
					<tbody>
						<tr>
							<td><s:text name="stafa.label.employmentstatus"/></td>
							<td><s:select id="employmentStatus" name="employmentDetails.refEmploymentStatus" list="lstEmploymentStatus" listKey="pkCodelst" listValue="description" headerKey="" headerValue="Select" /></td>
							<td><s:text name="stafa.label.employeraddress"/></td>
							<td><s:textfield value = "%{employmentDetails.employmentAddresss}" name="employmentDetails.employmentAddresss" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.employer"/></td>
							<td><s:textfield value = "%{employmentDetails.employmentName}" name="employmentDetails.employmentName" /></td>
							<td><s:text name="stafa.label.city"/></td>
							<td><s:textfield value = "%{employmentDetails.employmentCity}" name="employmentDetails.employmentCity"/></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.employerphonenumber"/></td>
							<td><s:textfield value = "%{employmentDetails.employmentPhNo}" name="employmentDetails.employmentPhNo"/></td>
							<td><s:text name="stafa.label.state"/></td>
							<td><s:select id="employmentState" name="employmentDetails.refEmploymentState" list="lstState" listKey="pkCodelst" listValue="description" headerKey="" headerValue="Select" /></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td><s:text name="stafa.label.country"/></td>
							<td><s:select id="employmentCountry" name="employmentDetails.refEmploymentCountry" list="lstCountry" listKey="pkCodelst" listValue="description" headerKey="" headerValue="Select" /></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td><s:text name="stafa.label.zipCode"/></td>
							<td><s:textfield value = "%{employmentDetails.employmentZipCode}" name="employmentDetails.employmentZipCode" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.employernotes"/></td>
							<td><s:textarea value = "%{employmentDetails.employerNotes}" name="employmentDetails.employerNotes" label="notes" cols="40" rows="4"/></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</form>
</div>


<div id="patientBenefitsDiv">
<form id="patientBenefiForm">
<div class="column">
		<div class="portlet">
			<div class="portlet-header">Patient Benefits</div>
			<div class="portlet-content">
				<table cellpadding="0" cellspacing="0" border="0"
					id="patientBenifits" class="display">
					<tbody>
						<tr>
							<td><s:text name="stafa.label.skillednursing"/></td>
							<td><s:textarea name="recipientInsViewDetails.benfSkilledNursing" value = "%{recipientInsViewDetails.benfSkilledNursing}" cols="20" rows="4"/></td>
							<td><s:text name="stafa.label.homehealth"/></td>
							<td><s:textarea name="recipientInsViewDetails.benfHomeHealth" value = "%{recipientInsViewDetails.benfHomeHealth}" cols="20" rows="4"/></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.lodging"/></td>
							<td><s:textarea name="recipientInsViewDetails.benfLodging" value = "%{recipientInsViewDetails.benfLodging}"  cols="20" rows="4"/></td>
							<td><s:text name="stafa.label.travel"/></td>
							<td><s:textarea name="recipientInsViewDetails.benfTravel" value = "%{recipientInsViewDetails.benfTravel}"  cols="20" rows="4"/></td>
						</tr>
						<tr>
							<td><s:text name="stafa.labelcardiacrehabphystherapy"/></td>
							<td><s:textarea name="recipientInsViewDetails.benfCardiacPhyTherapy" value = "%{recipientInsViewDetails.benfCardiacPhyTherapy}"  cols="20" rows="4"/></td>
							<td><s:text name="stafa.label.neupogen"/></td>
							<td><s:textarea name="recipientInsViewDetails.benfNeupogen" value = "%{recipientInsViewDetails.benfNeupogen}"  cols="20" rows="4"/></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.donorbenifits"/></td>
							<td><s:textarea name="recipientInsViewDetails.benfDonor" value = "%{recipientInsViewDetails.outInsResponsibility}"  cols="20" rows="4"/></td>
							<td><s:text name="stafa.label.annualdeductiblemet"/></td>
							<td><s:textfield value = "%{recipientInsViewDetails.benfAnnualDeductibleMet}" name="recipientInsViewDetails.benfAnnualDeductibleMet" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.inscasemanager"/></td>
							<td><s:textfield value = "%{recipientInsViewDetails.benfInscaseManager}" name="recipientInsViewDetails.benfInscaseManager" /></td>
							<td><s:text name="stafa.label.mhscasemngrcoordinator"/></td>
							<td><s:textfield value = "%{recipientInsViewDetails.benfCordinatorManager}" name="recipientInsViewDetails.benfCordinatorManager" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.insurancecasemanagerphno"/></td>
							<td><s:textfield value = "%{recipientInsViewDetails.benfInsCaseManagerPhNo}" name="recipientInsViewDetails.benfInsCaseManagerPhNo" /></td>
							<td><s:text name="stafa.label.mhscasemngrcoordinatorphno"/></td>
							<td><s:textfield value = "%{recipientInsViewDetails.benfCordinatorManagerPhNo}" name="recipientInsViewDetails.benfCordinatorManagerPhNo" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.insurancecasemanagerfaxno"/></td>
							<td><s:textfield value = "%{recipientInsViewDetails.benfInsCasemanagerFaxno}" name="recipientInsViewDetails.benfInsCasemanagerFaxno" /></td>
							<td><s:text name="stafa.label.mhscasemngrcoordinatorfaxno"/></td>
							<td><s:textfield value = "%{recipientInsViewDetails.benfCordinatorManagerFaxNo}" name="recipientInsViewDetails.benfCordinatorManagerFaxNo" /></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</form>
</div>

<s:set var="id" value="%{'patientbenifitdocs'}" />   
<div id="patientBenefitsDocsDiv">
	<div class="column">
	<div  class="portlet">
				<div class="portlet-header "><s:text name = "stafa.label.patientbenifitdocs"/></div>		
				<span class="hidden">
				  <s:select id="%{#attr['id']+'List'}" name="patientBenefitDocsList1" list="patientBenefitDocs"  headerKey="" headerValue="Select"/>
				</span>
				<jsp:include page="doc_activity.jsp"></jsp:include>					
		</div>
	</div>
</div>


<div id="caseMgmtDiv">
<form id="caseMgmtForm">
<s:hidden  id="caseMgmtPk"  value="%{personFundCaseMgmtDetails.pkFundcaseMgmt}" />
<div class="column">
		<div class="portlet">
			<div class="portlet-header">Unfunded/Underfunded Case Management</div>
			<div class="portlet-content">
				<table cellpadding="0" cellspacing="0" border="0"
					id="patientBenifits" class="display">
					<tbody>
						<tr>
							<td><s:text name="stafa.label.patientmonthlyincome"/></td>
							<td><s:textfield name="personFundCaseMgmtDetails.monthlyIncome"  value = "%{personFundCaseMgmtDetails.monthlyIncome}"  /></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.patientfaimlyincome"/></td>
							<td><s:textfield name="personFundCaseMgmtDetails.faimlyIncome" value = "%{personFundCaseMgmtDetails.faimlyIncome}"  /></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.minorsinthehousehold"/></td>
							<td><s:radio name="personFundCaseMgmtDetails.isMinorInHouseHold" list="#{'1':'Yes','0':'No'}" /></td>
							<td><s:text name="stafa.label.numberofminors"/></td>
							<td><s:textfield name="personFundCaseMgmtDetails.minorsNoInHouseHold" value = "%{personFundCaseMgmtDetails.minorsNoInHouseHold}" /></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.seenbyrca"/></td>
							<td><s:radio name="personFundCaseMgmtDetails.isSeenByRCA" list="#{'1':'Yes','0':'No'}" /></td>
							<td><s:text name="stafa.label.dateseenbyrca"/></td>
							<td><s:textfield name="personFundCaseMgmtDetails.rcaSeenDate" value = "%{personFundCaseMgmtDetails.rcaSeenDate}" id="dateSeenByRCA"  cssClass="dateEntry calDate"/></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.qualifymedicaid"/></td>
							<td><s:radio name="personFundCaseMgmtDetails.isQualifyForMedicaid" list="#{'1':'Yes','0':'No'}" /></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.qualifyssi"/></td>
							<td><s:radio name="personFundCaseMgmtDetails.isQualifySSI" list="#{'1':'Yes','0':'No'}" /></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.qualifyssid"/></td>
							<td><s:radio name="personFundCaseMgmtDetails.isQualifySSID" list="#{'1':'Yes','0':'No'}" /></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><s:text name="stafa.label.pendingdisabilitycase"/></td>
							<td><s:radio name="personFundCaseMgmtDetails.isPendingDisabilityCase" list="#{'1':'Yes','0':'No'}" /></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</form>
</div>

</div>



<script  type="text/javascript">

var PatBenDocParam={		 
		
		 widgetType : <s:property value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@TYPE_INSURANCE, @com.velos.stafa.util.VelosStafaConstants@SUBTYPE_PATIENTBENIDOC)"/>,
		 entityId   : '',
		 entityType : '<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_PERSON"/>',
		 txId       : '<s:property value="transplantId"/>',
		 visitId    : ''//'<s:property value="visitId"/>'  

};

var PatBenDocCurrentDoc={

     TABLE: '<s:property value="id"/>CurrTable',
	 TABLECol : '<s:property value="id"/>CurrColumn',
	 TABLEIdPrefix : '<s:property value="id"/>'

};

var PatBenDocHistDoc={

     TABLE: '<s:property value="id"/>HistTable',
	 TABLECol : '<s:property value="id"/>HistColumn',
	 TABLEIdPrefix : '<s:property value="id"/>'
};

var TABLE_2= '<s:property value="id"/>CurrTable';
var TABLE_2HIST = '<s:property value="id"/>HistTable';
var TABLE_2HISTDispCol = '<s:property value="id"/>HistColumn';
var TABLE_2DispCol = '<s:property value="id"/>CurrColumn';
var TABLE_2IdPrefix = '<s:property value="id"/>';
var widgetType1=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@TYPE_INSURANCE, @com.velos.stafa.util.VelosStafaConstants@SUBTYPE_PATIENTBENIDOC)"/>;
$('#<s:property value="id"/>Tabs' ).tabs();

// Preparing reciepient Consent
//constructDocumentTable(false,entityId,entityType, TABLE_2, docTableHeaders, TABLE_2DispCol, TABLE_2IdPrefix, widgetType1);
//constructDocumentHistoryTable(false,entityId,entityType, TABLE_2HIST, TABLE_2HISTDispCol, TABLE_2IdPrefix, widgetType1);

</script>
