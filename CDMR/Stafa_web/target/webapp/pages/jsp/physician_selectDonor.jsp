<%@ include file="common/includes.jsp" %>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />

<script>

function constructTable(flag,recipientId){
	
	var criteria  = "WHERE pk_person IN(SELECT fk_donorid FROM er_personproduct WHERE fk_recipient="+recipientId+")";
 	if(flag || flag =='true'){
 		oTable=$('#selectDonorTable').dataTable().fnDestroy();
	}
	  var link ;
		try{
			oTable=$('#selectDonorTable').dataTable( {
				"sPaginationType": "full_numbers",
				"bProcessing": true,
				"bServerSide": true,
				"bRetrieve" : true,
				"sDom": 'C<"clear">Rlfrtip',
				 "bDestroy": true,
				 "sScrollX": "100%",
				 "aaSorting": [[ 1, "asc" ]],
				 "aoColumns": [{"bSortable": false},
				              	   null,
					               null,
					               null,
					               null,
					               null
					          ],
				"oColVis": {
							"aiExclude": [ 0 ],
							"buttonText": "&nbsp;",
							"bRestore": true,
							"sAlign": "left",
							"iOverlayFade": 25     /* To Overcome the visibility while clicking the addButton*/
					},						               
					"oLanguage": {"sProcessing":""},									
				"fnServerParams": function ( aoData ) {
														aoData.push( { "name": "application", "value": "stafa"});
														aoData.push( { "name": "module", "value": "physician_selectdonor"});	
														aoData.push( { "name": "criteria", "value": criteria});
														
														//aoData.push({"sColumns":"Protocol Name,Module,Version"});
														
//SELECT PK_IMMNOPHENOTYPING, IMMNO_CODE, IMMNO_PARENTCODE,IMMNO_DESC, LEVEL,IMMNO_HIDE,IMMUNO_COMMENTS FROM er_immnophenotyping

														aoData.push( {"name": "col_1_name", "value": "lower(person_altid)" } );
														aoData.push( { "name": "col_1_column", "value": "lower(person_altid)"});
														
														aoData.push( { "name": "col_2_name", "value": "lower(person_altid)"});
														aoData.push( { "name": "col_2_column", "value": "lower(person_altid)"});
														
														aoData.push( {"name": "col_3_name", "value": "lower(person_lname)" } );
														aoData.push( { "name": "col_3_column", "value": "lower(person_lname)"});
														
														aoData.push( {"name": "col_4_name", "value": "lower(to_char(person_dob,'Mon dd, yyyy  HH:MM'))" } );
														aoData.push( { "name": "col_4_column", "value": "person_dob"});
														
														aoData.push( {"name": "col_5_name", "value": "lower((SELECT count(fk_product)  FROM er_personproduct where fk_donorid=pk_person))" } );
														aoData.push( { "name": "col_5_column", "value": "lower((SELECT count(fk_product)  FROM er_personproduct where fk_donorid=pk_person))"});
														
														
				},
				"sAjaxSource": 'dataSearch.action',
				"sAjaxDataProp": "filterData",
				"aoColumnDefs": [
								 {
									 	"aTargets": [ 0], "mDataProp": function ( source, type, val ) { 
									 		return "<input type='hidden' id='donorId' value="+source[0]+"><input type='hidden' id='donorName' value="+source[2]+">";
									 }},
				                 {	"sTitle":"Donor Type",
										 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
				                	 		return "";
				                	 }},
								  {	"sTitle":"Donor ID",
				                		 "aTargets": [ 2], "mDataProp": function ( source, type, val ) { 
									  		return source[1];
									 }},
								  {	"sTitle":"Donor Name",
										  "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
									 	   return source[2];
								  		}},
								  {	"sTitle":"Donor DOB",
								  		  "aTargets": [ 4], "mDataProp": function ( source, type, val ) { 
									  		return source[3];
									  }},
								  {	"sTitle":"# of Bags",
										  "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
											return source[4];
		  							  }}
								],
					"fnInitComplete": function () {
							        $("#selectDonorColumn").html($('.ColVis:eq(0)'));
						},
					"fnServerData": function ( sSource, aoData, fnCallback ) {
						$('.progress-indicator').css( 'display', 'block' );
						$.ajax( {
								"dataType": 'json',
								"type": "POST",
								"url": 'dataSearch.action',
								"data":aoData,
								"success":fnCallback,  
								"complete": function () {
											$('.progress-indicator').css( 'display', 'none' );
											$("select").uniform();
											$('#selectDonorTable tbody[role="alert"] tr').bind('click',function(){
												var donorId = $(this).children("td:nth-child(1)").find("#donorId").val();
												var donorName = $(this).children("td:nth-child(1)").find("#donorName").val();
												var recipientId = $("#recipientId").val();
												var url = "loadCollectionCycle";
												//var donorId=$("#donorId").val();
												
												var data = "{recipientId:"+recipientId+",donorId:"+donorId+",donorName:"+donorName+"}";
												var result = ajaxCallWithParam(url,data);
												/*var url1 = "loadCollectionDonor";
												var result1 = ajaxCallWithParam(url1,donornewId);*/	
												$("#main").html(result);
											});
											}
							  } );},
					"fnDrawCallback": function() { 
						//alert("fnDrawCallback");
						//$('.progress-indicator').css( 'display', 'none' );
			      }
				} );
			jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = parseInt(no_of_pages);
			}catch(err){
				alert("error " + err);
			}
			//alert("end of dt");
	
} 
function generateTableFromJsnList(result){
	$.each(result.recipientList, function(i,v) {
		$("#recipient_Id").html(v[0]);
		$("#recipientName").html(v[1]);
		$("#recipientDob").html(v[4]);
		$("#recipientAboRh").html(v[6]);
		$("#plannedProtocol").html(v[2]);
		$("#recipientDiagnosis").html(v[5]);
		$("#recipientWeight").html(v[3]);
	 	});
}
$(document).ready(function() {
	var recipientId = $("#recipientId").val();
	constructTable(false,recipientId);
	var url="loadRecipientData";
	var result = ajaxCallWithParam(url,recipientId);
	generateTableFromJsnList(result);
	$("select").uniform();
});
$(document).ready(function() {
	show_slidewidgets("seldonor_slidewidget",false);
    });


jQuery.fn.blindToggle = function(speed, easing, callback) {
var h = this.width() + parseInt(this.css('paddingLeft')) +  parseInt(this.css('paddingRight'));
if(parseInt(this.css('marginLeft'))<0){
$('#forfloat_right').animate( { width: "79%" }, { queue: false, duration: 200 });	
 $('#blind').html("<<");
 return this.animate({marginLeft:0},speed, easing, callback);
 }
else{
$( "#forfloat_right" ).animate( { width: "95%" }, { queue: false, duration: 250 });
 	$('#blind').html(">>");
 	return this.animate({marginLeft:-h},speed, easing, callback);
   }
};
/* function calculateTotalDose(){
	var rowDose = document.getElementById("selectDonorTable").rows[1];
	var productsTable = document.getElementById("availableProductsTable");
	var tnc=0;
	var cd34=0;
	var cd3=0;
	for(i=1;i<productsTable.rows.length;i++){
		var row = productsTable.rows[i];
		if(productsTable.rows[i].cells[8].childNodes[0].checked){
			if(i!=5){
			tnc+=Number($.trim(row.cells[4].innerHTML));
			cd34+=Number($.trim(row.cells[5].innerHTML));
			cd3+=Number($.trim(row.cells[6].innerHTML));
			}
		}
	}
	rowDose.cells[0].innerHTML = tnc;
	rowDose.cells[1].innerHTML = cd34;
	rowDose.cells[2].innerHTML = cd3;
} */

</script>

<div class="cleaner"></div>
<!--float window-->
<s:form>
		
<!-- recipient start-->
 <div id="seldonor_slidewidget" style="display:none;">
		<div  class="portlet">
			<div class="portlet-header notes">Recipient Info</div>
				<div class="portlet-content">
				<table width="225" border="0">
				  <tr>
					<td width="89">ID:</td>
					<td width="120"><div id="recipient_Id"></div></td>
				  </tr>
				  <tr>
					<td>Name:</td>
					<td><div id="recipientName"></div></td>
				  </tr>
				  <tr>
					<td>DOB:</td>
					<td><div id="recipientDob"></div></td>
				  </tr>
				  <tr>
					<td>ABO/Rh:</td>
					<td><div id="recipientAboRh"></div></td>
				  </tr>
				   <tr>
					<td>Planned Protocol:</td>
					<td><div id="plannedProtocol"></div></td>
				  </tr>
				  <tr>
					<td>Diagnosis:</td>
					<td><div id="recipientDiagnosis"></div></td>
				  </tr>
				  <tr>
					<td>Weight:</td>
					<td><div id="recipientWeight"></div></td>
				  </tr>
</table>

			</div>
			</div>
		
<!-- recipient-->

</div>
<!--float window end-->


<div class="cleaner"></div>
<!--right window start -->	
<s:hidden id="recipientId" name="recipientId" value="%{recipientId}"/>
<section>
 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes">Select Donor</div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="selectDonorTable" class="display">
					<thead>
						<tr>
							<th width="4%" colspan='1' id="selectDonorColumn"></th>
							<th colspan="1">Donor Type</th>
							<th colspan="1">Donor ID</th>
							<th colspan="1">Donor Name</th>
							<th colspan="1">Donor DOB</th>
							<th colspan="1"># of Bags</th>
						</tr>
					</thead>
			</table>
	</div>
	</div>	
</div>

 <div class="cleaner"></div>
 
</section>	
</s:form>
<div class="cleaner"></div>

<div class="cleaner"></div>
		<!-- <div align="right" style="float:right;">
		<form>
		<table cellpadding="0" cellspacing="0" class="" border="0">
                <tr>
                <td>Next:</td>
                <td><select name="" id="" ><option>Select</option></select></td>
                <td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
                <td><input type="button"  value="Save"/></td>
                </tr>
        </table>
		</form>
		</div>
		<div class="cleaner"></div>
</div> -->
<script>
$(function() {	
	
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open');
	});
	
	

	/**for Next Button Start**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".next" )
	.append("<span style=\"float:right;\" class='ui-next'></span>");
	
	$( ".portlet-header .ui-next " ).click(function() {
		loadPage('jsp/physician_product1.jsp');	
	});
	/**for Next Button End**/
	
	/**for Previous Button Start**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".previous" )
	.append("<span style=\"float:right;\" class='ui-previous'></span>");
	
	$( ".portlet-header .ui-previous " ).click(function() {
		loadPage('jsp/physician_dose.jsp');	
	});
	/**for Previous Button End**/
	
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");
	

	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});

</script>

<div id="sterility" title="sterility" style="display: none">
		<b>Proceed ?</b>&nbsp;<input type="checkbox" value=""/>Y&nbsp;<input type="checkbox" value=""/>N
    </div>