<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<style type="text/css">
p 
{
padding-left:2em;
}
</style>
<div class="column">
	<div  class="portlet">
				<div class="portlet-header "><s:text name = "stafa.label.recipientbasicclinicalinfo"/></div>
				 <div class="portlet-content">
				 <%-- <input type="button" onclick="save_ClinicalInfo(paramObj);" name="Save Clinical"> --%>
				 <table width="100%">
				  <tr width="100%">
				   <td width="100%" colspan="4">
				    <div style="float:left;margin-left: 1em;width:45%" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
				    <div class="column">
				    <div class="portlet-header "><s:text name = "stafa.label.recipientweight"/></div>
				    <div class="portlet-content">
					<table>
						<tr>
							<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="add_WeightClinicalRows('weightTable');"/>&nbsp;&nbsp;<label  class="cursor" onclick="add_WeightClinicalRows('weightTable');"><b>Add </b></label>&nbsp;</div></td>
							<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteClinicalInfo();" />&nbsp;&nbsp;<label class="cursor" onclick="deleteClinicalInfo();"><b>Delete</b></label>&nbsp;</div></td> 
				 		    <td style="width:20%"><div><img src = "images/icons/edit.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="editClinicalInfo('weightTable');"/>&nbsp;&nbsp;<label class="cursor" onclick="editClinicalInfo('weightTable');"><b>Edit</b></label>&nbsp;</div></td>
						</tr>
					</table>
					<s:form id="basicWeightClinicalInfo" name="basicWeightClinicalInfo" >
					  <div class="tableOverflowDiv">
					     <table cellpadding="0" cellspacing="0" border="0" id="weightTable" class="display" width="100%">
							<thead>
							  <tr>
								<th width="5%" id="weightColumn"></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						  </thead>
					   </table>
				</div>
					</s:form>
					</div>					
					</div>
					</div>
					<div style="float:right;margin-right: 1em;width:45%" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
					<div class="column">
				    <div class="portlet-header "><s:text name = "stafa.label.recipientheight"/></div>
				    <div class="portlet-content">
						<table>
						 <tr>
						 	<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="add_HeightClinicalRows('heightTable');"/>&nbsp;&nbsp;<label  class="cursor" onclick="add_HeightClinicalRows('heightTable');"><b>Add </b></label>&nbsp;</div></td>
						  	<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteClinicalInfo();" />&nbsp;&nbsp;<label class="cursor" onclick="deleteClinicalInfo();"><b>Delete</b></label>&nbsp;</div></td> 
						    <td style="width:20%"><div><img src = "images/icons/edit.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="editClinicalInfo('heightTable');"/>&nbsp;&nbsp;<label class="cursor" onclick="editClinicalInfo('heightTable');"><b>Edit</b></label>&nbsp;</div></td>
						</tr>
					  </table>
					<s:form id="basicHeightClinicalInfo" name="basicHeightClinicalInfo" >
					  <div class="tableOverflowDiv">
					     <table cellpadding="0" cellspacing="0" border="0" id="heightTable" class="display" width="100%">
							<thead>
							  <tr>
								<th width="5%" id="heightColumn"></th>
								<th></th>
								<th></th>
								<th></th>																						
							</tr>
						  </thead>
					   </table>
					 </div>
				</s:form>
				</div>
				</div>
				</div>
				 </td>
				</tr>
				<tr width="100%" height="30px">
					<td width="100%" colspan="4"></td>
				</tr>				
				 <tr width="100%">
				   <td width="20%"><p><s:text name = "stafa.label.aboRH"/></p></td>
				   <td width="40%"><s:select headerKey="" id="bloodGroup_id" headerValue="Select" list="bloodGroups" name="bloodGroup"/></td>
				</tr>
				<tr width="100%">
				   <td width="20%"><p><s:text name = "stafa.label.allergies"/></p></td>
				   <td width="40%"><s:textarea id="allergies_id" name="allergies"  rows="4" cols="60" wrap="hard" style="width:800;height:150;" ></s:textarea></td>
				</tr>
				<tr width="100%">
				   <td width="20%"><p><s:text name = "stafa.label.antibodyscrn"/></p></td>
				   <td width="40%"><s:select headerKey="" id="antibody_id" headerValue="Select" 	list="antibodys" name="antibody"/></td>
				</tr>
				<tr width="100%">
				   <td width="20%"><p><s:text name = "stafa.label.recipientComments"/></p></td>
				   <td width="40%"><s:textarea id="comments_id" name="comments"  rows="4" cols="60" wrap="hard" style="width:800;height:150;" ></s:textarea></td>
				</tr>
			</table>			 
				 </div>					
	</div> 
</div>
<div class="cleaner"></div>
<span class="hidden">
<s:select id="weightUnit" name="referralDocList1" list="weightUnits"  headerKey="-1" headerValue="Select"/>
<s:select id="heightUnit" name="referralDocList1" list="heightUnits" headerKey="-1" headerValue="Select"/>
</span>
<SCRIPT type="text/javascript">


var editedDateId='E1';


var WeightTableHeader = {
		select:'<s:text name = "stafa.label.select"/>',
		result: '<s:text name = "stafa.label.weight"/>',
		unit: '<s:text name = "stafa.label.unit"/>',
		date:'<s:text name = "stafa.label.date"/>'
		};
var HeightTableHeader = {
		select:'<s:text name = "stafa.label.select"/>',
		result: '<s:text name = "stafa.label.height"/>',
		unit: '<s:text name = "stafa.label.unit"/>',
		date:'<s:text name = "stafa.label.date"/>'
		};

/***************** Clinical Tables Header ******************/
/* Prepared Upload Document table *************
 * @param flag as boolean
 * @param donor as entity pk
 * @param type as entityType
 * @param tableId as constructed Table Id
 */

 var codeLstHeight=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@TYPE_VITALSIGN, @com.velos.stafa.util.VelosStafaConstants@SUBTYPE_VITALSIGNHEIGHT)"/>;
 var codeLstWeight =<s:property value="getCodeListPkByTypeAndSubtype( @com.velos.stafa.util.VelosStafaConstants@TYPE_VITALSIGN, @com.velos.stafa.util.VelosStafaConstants@SUBTYPE_VITALSIGNWEIGHT)"/>;	

function constructClinicalTable(flag, entity, entityType, vitalType, tableId, docTableHeaders, columnId, tableHead) {
		/*var criteria = "";
		if(entity!=null && entity!=""){
			criteria = entity;
		}*/
		var documentColumn = [ { "bSortable": false},
			                	null,
			                	null,
			                	null			                	
				             ];
													
		var documentServerParams = function ( aoData ) {
										aoData.push( { "name": "application", "value": "stafa"});
										aoData.push( { "name": "module", "value": "Basic_Clinical"});
										//aoData.push( { "name": "criteria", "value": criteria});
										
										aoData.push( { "name": "param_count", "value":"3"});
										
										aoData.push( { "name": "param_1_name", "value":":ENTITY:"});
										aoData.push( { "name": "param_1_value", "value":entity});

										aoData.push( { "name": "param_2_name", "value":":ENTITYTYPE:"});
										aoData.push( { "name": "param_2_value", "value":"'"+entityType+"'"});
										
										aoData.push( { "name": "param_3_name", "value":":VITALSIGN:"});
										aoData.push( { "name": "param_3_value", "value":vitalType});
										
										/*aoData.push( {"name": "col_1_name", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_DOCUMENTTYPE))" } );
										aoData.push( { "name": "col_1_column", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_DOCUMENTTYPE))"});
										
										aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
										aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});*/
													
										
					};
					var documentaoColDef = [
							                  {		  //"sTitle":'<s:text name = "stafa.label.select"/>',//"Select",
							                	  	  "aTargets": [0], "mDataProp": function ( source, type, val ) {
						                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
						                	 			return "<input type='checkbox' id='vitalId' name='vitalId' value='"+source[0] +"' />";
						                	 			//return "";
						                	     }},
							                  { 	 "sTitle":tableHead.result,//"Weight",
						                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
							                	 		return source[1];
							                	 }},
							                	 { 	 "sTitle":tableHead.unit,//"Unit",
							                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
													    return source[2];
												}},
											  { 	 "sTitle":tableHead.date,//"Date",
							                		 "aTargets": [3], "mDataProp": function ( source, type, val ) { 
													    return source[3];
												}}
											];
					
		var documentColManager = function () {
												$("#"+columnId).html($("#"+tableId+"_wrapper .ColVis"));
											};
											
		constructTable(flag,tableId,documentColManager,documentServerParams,documentaoColDef,documentColumn);
	} 

/* add rows */
var attachWeightRowId ='W1';
function add_WeightClinicalRows(table){
	 if(checkModuleRights(STAFA_RECIPIENTTRACKERS,APPMODE_ADD)){
		 var title = $("#weightUnit").html();
	     var newRow ="<tr>"+	     
	     "<td><input type='checkbox' id='vitalId' name='vitalId' value='' /></td>"+
	     "<td><div><input type='text' id='vitalResult' name='weight"+attachWeightRowId+"' style='width: 100px;' /></div></td>"+
	     "<td><select id='unitid' name='unit"+attachWeightRowId+"' style='width:100px;'>"+title+"</select></td>"+
	   	 "<td><input type='text' id='date"+attachWeightRowId+"' class='datePic dateEntry' name='date"+attachWeightRowId+"' style='width: 130px;'/></td>"+
	     "</tr>";
	     $("#"+table+" tbody").prepend(newRow);	 

		 $("#date"+attachWeightRowId).datepicker({dateFormat: 'M dd, yy',changeMonth: true, changeYear: true, yearRange: '1900:' + new Date().getFullYear()});

		 $.uniform.restore('select');
		 $('select').uniform();		
		 attachWeightRowId=attachWeightRowId+1;
		 /* var param ="{module:'APHERESIS',page:'DONOR_DETAILS'}";
			markValidation(param);*/
	 }
}
var attachHeightRowId='H1';
function add_HeightClinicalRows(table){
	 if(checkModuleRights(STAFA_RECIPIENTTRACKERS,APPMODE_ADD)){
		 var title = $("#heightUnit").html();
		 //alert(docsTitle);
	     var newRow ="<tr>"+
	     "<td><input type='checkbox' id='vitalId' name='vitalId' value='' /></td>"+
	     "<td><div><input type='text' id='vitalResult' name='weight"+attachHeightRowId+"' style='width: 100px;' /></div></td>"+
	     "<td><select id='unitid' name='unit"+attachHeightRowId+"' style='width:100px;'>"+title+"</select></td>"+
	   	 "<td><input type='text' id='date"+attachHeightRowId+"' class='datePic dateEntry' name='date"+attachHeightRowId+"' style='width: 130px;'/></td>"+	     
	     "</tr>";
	     $("#"+table+" tbody").prepend(newRow);
	   
	     $("#date"+attachHeightRowId).datepicker({dateFormat: 'M dd, yy',changeMonth: true, changeYear: true, yearRange: '1900:' + new Date().getFullYear()});

	     $.uniform.restore('select');
		 $('select').uniform();		
		 attachHeightRowId=attachHeightRowId+1;
		 /* var param ="{module:'APHERESIS',page:'DONOR_DETAILS'}";
			markValidation(param);*/
	 }
}





function editClinicalInfo(table){
	var content;
	var checkedStatus;
	var weight;
	var unit;
	var date;
	
	if(table==="weightTable"){
		content = $("#weightUnit").html()
	}else{
		content = $("#heightUnit").html()
		}
	
	$("#"+table+" tbody tr").each(function (row){
	 	 weight ="";
	 	 unit ="";
	 	 date ="";
	 	 checkedStatus=false;
			$(this).find("td").each(function (col){
			if(col ==0){			
				if($(this).find("#vitalId").is(':checked')){
					checkedStatus=true;
				}else 
					return false;
			}
			if(col ==1){
				weight = $(this).html();
				if(checkedStatus && weight.indexOf('input')<0){					
						$(this).html('');					
						$(this).html("<input type='text' id='vitalResult' name='' value='"+weight+"' style='width: 100px;' />");
				}else {return false;}
			}
			if(col ==2){
				unit = $(this).html();
				if(checkedStatus && unit.indexOf('select')<0){					
					 $(this).html('');
					 $(this).html("<select id='unitid' name='' style='width:100px;'>"+content+"</select>");
					//$(this).find("#unitid").attr('selected', unit);
					 $.uniform.restore('select');
					 $('select').uniform();
				}else{ return false;}
			}
			if(col ==3){
				date = $(this).html();
				if(checkedStatus && date.indexOf('select')<0){					
					$(this).html('');
					$(this).html("<input type='text' id='date"+editedDateId+"' class='datePic dateEntry' name='' value='"+date+"' style='width: 130px;'/>");
					$("#date"+editedDateId).datepicker({dateFormat: 'M dd, yy',changeMonth: true, changeYear: true, yearRange: '1900:' + new Date().getFullYear()});
					editedDateId +=1;
				}else {return false;}				
			}
		});
			
		
	 
	 });
	
}

/*** Save Basic Clinical Info*****/
function save_ClinicalInfo(paramObject){
	try{
	var entityid= paramObject.entityid;
	var entityType= paramObject.entityType;	
	var  clinicalData = "";
	var  clinicalPersonData="";	
	var widgetType='';
	//var  updateClinicalData="";
	var weight="";
 	var unit="";
 	var date="";
 	var checkVal="";
	// Height Table Data Collection
	 $("#heightTable tbody tr").each(function (row){
	 	 weight ="";
	 	 unit ="";
	 	 date ="";
	     checkVal="";
			$(this).find("td").each(function (col){
			if(col ==0){
				checkVal = $(this).find("#vitalId").val();
			}
			if(col ==1){
				weight = $(this).find("#vitalResult").val();
			}
			if(col ==2){
				unit = $(this).find("#unitid").val();
			}
			if(col ==3){
				date = $(this).find(".datePic").val();				
			}
		});
		if((checkVal!=="") && (weight != undefined && weight != "") && (unit != undefined && unit != "") && (date != undefined && date != "")){
			
			clinicalData +="{widgetType:'"+widgetType+"',pkPersonVitalsign:'"+checkVal+"',domainName:'DOMAIN_CLINICAL',domainKey:'CLINICAL_ID',fkPerson:'"+entityid+"',personType:'"+entityType+"',fkVitalsign:'"+codeLstHeight+"',vitalsignResult:'"+weight+"',fkVitalsignUnit:'"+unit+"',vitalsignDate:'"+date+"'},";

		}else if((checkVal ==="") && (weight != undefined && weight != "") && (unit != undefined && unit != "") && (date != undefined && date != "")){

			clinicalData +="{widgetType:'"+widgetType+"',fkPerson:'"+entityid+"',personType:'"+entityType+"',fkVitalsign:'"+codeLstHeight+"',vitalsignResult:'"+weight+"',fkVitalsignUnit:'"+unit+"',vitalsignDate:'"+date+"'},";
		}		 
	 });

	 //Weight Table Data Collection
	 
	 $("#weightTable tbody tr").each(function (row){
	 	 weight="";
	 	 unit="";
	 	 date="";
	 	 checkVal="";
			$(this).find("td").each(function (col){
			if(col ==0){
				checkVal = $(this).find("#vitalId").val();
			}
			if(col ==1){
				weight = $(this).find("#vitalResult").val();
			}
			if(col ==2){
				unit = $(this).find("#unitid").val();
			}
			if(col ==3){
				date = $(this).find(".datePic").val();				
			}
		});
			if((checkVal!=="") && (weight != undefined && weight != "") && (unit != undefined && unit != "") && (date != undefined && date != "")){
				console.warn("inside update row");
				
				clinicalData +="{widgetType:'"+widgetType+"',pkPersonVitalsign:'"+checkVal+"',domainName:'DOMAIN_CLINICAL',domainKey:'CLINICAL_ID',fkPerson:'"+entityid+"',personType:'"+entityType+"',fkVitalsign:'"+codeLstWeight+"',vitalsignResult:'"+weight+"',fkVitalsignUnit:'"+unit+"',vitalsignDate:'"+date+"'},";

			}else if((checkVal ==="") && (weight != undefined && weight != "") && (unit != undefined && unit != "") && (date != undefined && date != "")){

				clinicalData +="{widgetType:'"+widgetType+"',fkPerson:'"+entityid+"',personType:'"+entityType+"',fkVitalsign:'"+codeLstWeight+"',vitalsignResult:'"+weight+"',fkVitalsignUnit:'"+unit+"',vitalsignDate:'"+date+"'},";
		    }
	 
	 });
	 //Preparing person data
	 var bloodGroup = $('#bloodGroup_id').val();
	 var allergies =  $('#allergies_id').val();	
	 var antibody = $('#antibody_id').val();
	 var comment = $('#comments_id').val();
	 var PersonData={
			 fkCodelstBloodgrp:$('#bloodGroup_id').val(),
			 personAllergies: $('#allergies_id').val(),
			 fkAntibodyScreen:  $('#antibody_id').val(),
			 personComment:   $('#comments_id').val()
	 };
	 var data="";
	 var flag=false;
	 for(var prop in PersonData) {
		   if(PersonData.hasOwnProperty(prop) && PersonData[prop]!=null && PersonData[prop]!=='' && PersonData[prop]!= undefined){
			  // console.warn("key : "+prop);
			  //  console.warn("data : "+PersonData[prop]);
			    //$('#'+eleObj[prop]).html(dataObj[prop]);
			    data += prop+":'"+PersonData[prop]+"',";
			    flag = true;
			   // console.warn(data);
		        }
		}
	 if(flag){
			data = data.substring(0,(data.length-1));
			clinicalPersonData ="{pkPerson:'"+entityid+"',"+data+"}"
		}
	/* if(bloodGroup!=='' || allergies!=='' || antibody!=='' || comment!==''){ 
	  clinicalPersonData ="{pkPerson:'"+entityid+"',fkCodelstBloodgrp='"+$('#bloodGroup_id').val()+"',personAllergies='"+$('#allergies_id').val()+"',fkAntibodyScreen='"+$('#antibody_id').val()+"',personComment='"+$('#comments_id').val()+"'}";
	 }*/
		if(clinicalData.length >0 || clinicalPersonData.length>0 ){
			//console.warn(clinicalData);
			//console.warn(clinicalPersonData);
			clinicalData = clinicalData.substring(0,(clinicalData.length-1));			
			var url="saveClinicalInfo";
			response = jsonDataCall(url,"clinicalJsonData={clinicalData:["+clinicalData+"],personClinicalData:["+clinicalPersonData+"]}");
			$('.progress-indicator').css( 'display', 'none' );
		}
	}catch(e){
		alert("Error : "+e);
		}
	 return true;
}

function deleteClinicalInfo(){
	 if(checkModuleRights(STAFA_RECIPIENTTRACKERS, APPMODE_DELETE)){  	
		 var deletedIds="";
			try{
				
			$("#weightTable tbody tr").each(function (row){
				 if($(this).find("#vitalId").is(":checked")){
					deletedIds += $(this).find("#vitalId").val() + ",";
				}
			 });
			
			$("#heightTable tbody tr").each(function (row){
				 if($(this).find("#vitalId").is(":checked")){
					deletedIds += $(this).find("#vitalId").val() + ",";
				}
			 });
		
			  if(deletedIds.length >0){
				 var yes=confirm(confirm_deleteMsg);
				 if(!yes){
					return;
				 }
				  deletedIds = deletedIds.substr(0,deletedIds.length-1);
				  console.warn("\n\ndeletedIds :\n"+deletedIds);
				  jsonData = "{deletedIds:'"+deletedIds+"',domainName:'DOMAIN_CLINICAL',domainKey:'CLINICAL_ID'}";
				  url="commonDelete";
			     
			     response = jsonDataCall(url,"jsonData="+jsonData);
			 
			    constructClinicalTable(true, paramObj.entityid, paramObj.entityType, codeLstWeight, 'weightTable', '', 'weightColumn', WeightTableHeader);
			 	constructClinicalTable(true, paramObj.entityid, paramObj.entityType, codeLstHeight, 'heightTable', '', 'heightColumn', HeightTableHeader);
			    
			 	
			  }
			}catch(err){
				alert(" error " + err);
			}
	  }
}



	
	constructClinicalTable(false, paramObj.entityid, paramObj.entityType, codeLstWeight, 'weightTable', '', 'weightColumn', WeightTableHeader);
	constructClinicalTable(false, paramObj.entityid, paramObj.entityType, codeLstHeight, 'heightTable', '', 'heightColumn', HeightTableHeader);


		

</SCRIPT>