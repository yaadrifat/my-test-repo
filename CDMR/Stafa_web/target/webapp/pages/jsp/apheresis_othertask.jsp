<%@ taglib prefix="s" uri="/struts-tags"%>
<script>

$(document).ready(function(){
	hide_slidecontrol();
	hide_slidewidgets();
	var oTable1=$("#othertaskTable").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null
											             
											             ],
											             "sScrollX": "100%",
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#othertaskColumn").html($('.ColVis:eq(0)'));
												}
		});
		
$("select").uniform(); 

});

function add_othertask(){
	var runningTabClass= $('#othertaskTable tbody tr td:nth-child(1)').attr("class");
	if(runningTabClass=="dataTables_empty"){
		$("#othertaskTable tbody").replaceWith("<tr>"+
								"<td><input type='checkbox' id=''/></td>"+
								"<td><select><option>select</option><option>Product Transplatation</option></select></td>"+
								"<td id='productid'><div style='text-align:right;margin-top:-18%;'><img src = 'images/icons/addnew.jpg' class='cursor' id='' onclick='add_popup(this)'/></div></td>"+
								"<td id='donorid'></td>"+
								"<td id='donorname'></td>"+
								"<td><select><option>select</option><option>Blood Bank</option></select></td>"+
								"<td id='status'></td>"+
								"<td><select><option>Select</option><option>Nurse1</option></select><select><option>Select</option><option>Primary</option></select><div style='text-align:right;margin-top:-8%;'><img src = 'images/icons/addnew.jpg' class='cursor' id='' onclick=''/></div></td>"+
								"</tr>");
	}
	else{
		$("#othertaskTable tbody").prepend("<tr>"+
				"<td><input type='checkbox' id=''/></td>"+
				"<td><select><option>select</option><option>Product Transplatation</option></select></td>"+
				"<td id='productid'><div style='text-align:right;margin-top:-18%;'><img src = 'images/icons/addnew.jpg' class='cursor' id='' onclick='add_popup(this)'/></div></td>"+
				"<td id='donorid'></td>"+
				"<td id='donorname'></td>"+
				"<td><select><option>select</option><option>Blood Bank</option></select></td>"+
				"<td id='status'></td>"+
				"<td><select><option>Select</option><option>Nurse1</option></select><select><option>Select</option><option>Primary</option></select><div style='text-align:right;margin-top:-8%;'><img src = 'images/icons/addnew.jpg' class='cursor' id='' onclick=''/></div></td>"+
				"</tr>");
		}
	$("select").uniform(); 


} 
var curr_row;
var img;
function add_popup(rowid){
	var cell = $(rowid).closest('td');
	var row = cell.closest('tr');
	curr_row = row[0].rowIndex-1;
    showPopUp('open','addmodal','Select Products','600','600');
   
}

var indexes;
var rawData='';
function addProduct(){
         var rows = [];
         var productId = "";
         var donorId = "";
         var donarName = "";
         var status = "";
        $('#selectproduct tbody tr').each(function(i, n){
        	var $row = $(n);
    		var cbox=$(n).children("td:eq(0)").find(':checkbox').is(":checked");
    		if(cbox){
    			productId+=$row.find("td:eq(1)").text()+"<br/>";
    			donorId+=$row.find("td:eq(2)").text()+"<br/>";
    			donarName+=$row.find("td:eq(3)").text()+"<br/>";
    			status+=$row.find("td:eq(4)").text()+"<br/>";
    		}
        });
       
    	$('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(2)").html(productId+"<div style='text-align:right;margin-top:-20%;'><img src = 'images/icons/addnew.jpg' class='cursor' id='' onclick='add_popup(this)'/></div>");
        $('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(3)").html(donorId);
  		$('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(4)").html(donarName);
   		$('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(6)").html(status);  
        
        
    /*  getJsonFromTable= JSON.stringify(rows);  
    console.log(getJsonFromTable);
   $.each(rows, function(key, value){
	   $('#othertaskTable tbody tr:eq('+curr_row+')').after().append("<tr>"
			   +"<td><input type='checkbox' id=''/></td>"+
				"<td><select><option>select</option><option>Product Transplatation</option></select></td>"+
				"<td id='productid'>"+rows[key].productid+"<img src = 'images/icons/addnew.jpg' class='cursor' id='' onclick='add_popup(this)'/></td>"+
				"<td id='donorid'></td>"+
				"<td id='donorname'></td>"+
				"<td><select><option>select</option><option>Blood Bank</option></select></td>"+
				"<td id='status'></td>"+
				"<td><select><option>Select</option><option>Nurse1</option></select><select><option>Select</option><option>Primary</option></select></td>"+"</tr>");
	   
    /* 	$('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(2)").text(rows[key].productid);
        $('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(3)").text(rows[key].donorid);
  		$('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(4)").text(rows[key].donorname);
   		$('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(6)").text(rows[key].status);  
   		

    }); */
   showPopUp('close','addmodal','Select Products','600','600');
 }
</script>

<div id="addmodal" style="display:none">
<table id="selectproduct"  border="1" cellpadding="3" cellspacing="0" class="display">
					<thead>
						<tr>
							<th width="4%" id="">Check All <input type="checkbox" name="" class="selectall"/></th>
							<th>Product ID</th>
							<th>Donor Id</th>
							<th>Donor Name</th>
							<th>Status</th>
						</tr>
						
					</thead>
					<tbody>
						<tr>
						<td><input type="checkbox" name="" class="checkselect"/> </td>
						<td >BMT12127</td>
						<td >HCA12443523</td>
						<td >Jackson</td>
						<td>Collection Completed</td>
						</tr>	
						<tr>
						<td><input type="checkbox" name="" class="checkselect"/> </td>
						<td >BMT12145</td>
						<td >HCA12443523</td>
						<td >Mary</td>
						<td >Collection in Progress</td>
						</tr>
						<tr>
						<td><input type="checkbox" name="" class="checkselect"/> </td>
						<td >BMT123127</td>
						<td >HCA12456523</td>
						<td>johnson</td>
						<td >Collection Completed</td>
						</tr>					
					</tbody>
					
				</table>
				<br>
				<br>
				<div align="right">
				<form>
				<input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;<input type="button" name="submit" onclick="addProduct();" value="Submit"/>&nbsp;<input type="button" name="Cancel" value="Cancel" onclick="close();"/>
				</form>
				</div>
								
</div>

<div class="column">		
		<div  class="portlet">
			<div class="portlet-header ">Other Tasks </div>
				<div class="portlet-content">
				<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="add_othertask()"/>&nbsp;&nbsp;<label  class="cursor" onclick="add_othertask()"><b>Add </b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id =""/>&nbsp;&nbsp;<label class="cursor" ><b>Delete</b></label>&nbsp;</div></td>
				</tr>
				</table>
				<form>
				<table cellpadding="0" cellspacing="0" border="0" id="othertaskTable" class="display">
					<thead>
						<tr>
							<th width="4%" id="othertaskColumn"></th>
							<th width="14%">Task</th>
							<th width="10%">Product Id</th>
							<th width="10%">Donor Id</th>
							<th width="10%">Donor Name</th>
							<th width="14%">Destination</th>
							<th width="10%">Status</th>
							<th width="14%">User Assignments</th>
							
						</tr>
						
					</thead>
					<tbody>
												
					</tbody>
				</table>
				</form>		
				</div>	
			</div>
</div>

<script>
$(function() {

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	

	/**For Notes**/
	/* 
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='ui-addnew'></span>");
	
	$( ".portlet-header .ui-addnew" ).click(function() {
		addNewPopup("Add New Product","SupervisoraddNewDialog","addnewAcquisition","700","1100");
	});
	 */
	
	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});
	// add multiple select / deselect functionality
    $(".selectall").live('click',function () {
          $('.checkselect').attr('checked', this.checked);
    });
 
    // if all checkbox are selected, check the selectall checkbox
    // and viceversa
    $(".checkselect").live('click',function(){
 
        if($(".checkselect").length == $(".case:checked").length) {
            $(".selectall").attr("checked", "checked");
        } else {
            $(".selectall").removeAttr("checked");
        }
 
    });
	  
});
</script>