<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page import="com.velos.stafa.util.VelosStafaConstants" %>
<%-- <%@ include file="common/includes.jsp" %> --%>
<script type="text/javascript" src="js/donor.js"></script>
<script type="text/javascript" src="js/dataSearch.js"></script>
<script type="text/javascript" src="js/barcodeScanBox.js"></script>
<script>

function filter(e)
{  
    var evt = e || window.event;
   
    var keyPressed = evt.which || evt.keyCode;
	if (keyPressed == 13){
		
		$("#recipientMatchResults").css( 'display', 'block' );
		searchPerson("type");
		//$("#mrnForSearch").val(" ");
		var mrn = $("#recipientMRN").val();
		var table = "recipientMatchResults";
		//getADTData(mrn,table);
		
	return false;
	}
	return true;
}


function setRecipientDetails(var1,var2,var3,var4,var5,var6,var7,var8,var9){
//	$("#donorPerson").val(var1);
	$("#recipientLastName").val(var2);
	$("#recipientFirstName").val(var3);
	$("#recipientMiddleName").val(var8);
	$("#recipientDOB").val(var4);
	$("#recipientMRN").val(var5);
	$("#recipientSSN").val(var6);
	$("#recipientabo option[value="+var9+"]").attr('selected','selected');
	
	
}

function populateRecipientData(id)
{
	
	var mrn = "'"+id+"'";;
	
	var url ="fetchTempData";
	var jsonData = "tempRecipientMrn:"+mrn;

//	var tempRecipientMrn =  "'"+id+"'";
	var url="fetchTempData";
	
	
	response = jsonDataCall(url,"jsonData={"+jsonData+"}"); 
	
	
	var recipientDetails = response.recipientDetails;
	
	$("#recipientMRN").val(recipientDetails.recipientMrn);
	$("#recipientSSN").val(recipientDetails.recipientSsn);
	$("#recipientLastName").val(recipientDetails.recipientLastName);
	$("#recipientFirstName").val(recipientDetails.recipientFirstName);
	$("#recipientMiddleName").val(recipientDetails.recipientMiddleName);
	//$("#recipientabo").val(recipientDetails.recipientabo);
	
	$("#recipientabo option[value="+recipientDetails.recipientabo+"]").attr('selected','selected');
	
	$("#recipientDOB").val(recipientDetails.recipientDob);
	//Hidden fields//
	//$("#primDiagnosis").val(recipientDetails.primDiagnosis);
	$("#dateOfRefferal").val(recipientDetails.dateOfRefferal);
	$("#gender").val(recipientDetails.gender);
	$("#recipientAddressLine1").val(recipientDetails.recipientAddressLine1);
	$("#recipientAddressLine2").val(recipientDetails.recipientAddressLine2);
	$("#recipientCity").val(recipientDetails.recipientCity);
	$("#recipientState").val(recipientDetails.recipientState);
	$("#recipientCountry").val(recipientDetails.recipientCountry);
	$("#recipientZipCode").val(recipientDetails.recipientZipCode);
	$("#primaryLanguge").val(recipientDetails.primaryLanguge);
	$("#isTranslaortneed").val(recipientDetails.isTranslaortneed);
	$("#recipinetPrimaryPhone").val(recipientDetails.recipinetPrimaryPhone);
	$("#recipinetHomePhone").val(recipientDetails.recipinetHomePhone);
	$("#recipinetWorkPhone").val(recipientDetails.recipinetWorkPhone);
	$("#recipientEmail").val(recipientDetails.recipientEmail);
	$("#maritialStatus").val(recipientDetails.maritialStatus);
	$("#other").val(recipientDetails.other);
	$("#kinLastName").val(recipientDetails.kinLastName);
	$("#kinFirstName").val(recipientDetails.kinFirstName);
	$("#race").val(recipientDetails.race);
	$("#ethnicity").val(recipientDetails.ethnicity);
	$("#kinAddressLine1").val(recipientDetails.kinAddressLine1);
	$("#kinAddressLine2").val(recipientDetails.kinAddressLine2);
	$("#kinCity").val(recipientDetails.kinCity);
	$("#kinState").val(recipientDetails.kinState);
	$("#kinCountry").val(recipientDetails.kinCountry);
	$("#kinZip").val(recipientDetails.kinZip);
	$("#primCntactPhne").val(recipientDetails.primCntactPhne);
	$("#primCntactRelation").val(recipientDetails.primCntactRelation);
	$("#kinHomePhone").val(recipientDetails.kinHomePhone);
	$("#kinWorkPhone").val(recipientDetails.kinWorkPhone);
	$("#isTempData").val(true);
	
	
}


function searchPerson(type){
	try{
	
	//alert("trace1");
	var search_aoColumn = [ { "bSortable": false},
	                                   null,
						               null,
						               null,
						               null,
						               null,
						               null
			             ];
	var criteria = " ";
	var mrnFlag = false;
	var ssnFlag = false;
	var mrnCriteria,ssnCriteria;
	var donorType;
	var searchMrn = $("#mrnForSearch").val();
	var count =0;
	if(($("#mrnForSearch").val()!="" )|| ($("#recipientMRN").val()!="")  )
		{
			if($("#mrnForSearch").val()!="" )
			{
				criteria= " and lower(RECIPIENT_MRN) like lower('%" + $("#mrnForSearch").val() +"%')";
			}
			else
				{
				criteria= " and lower(RECIPIENT_MRN) like lower('%" + $("#recipientMRN").val() +"%')";
				}
		count++;
		}
	
	if($("#recipientSSN").val()!="")
		{
		if(count>0)
			criteria+= " or lower(Recipient_Ssn) like lower('%" + $("#recipientSSN").val() +"%')";
		else
			criteria+= " and lower(Recipient_Ssn) like lower('%" + $("#recipientSSN").val() +"%')";
		count++;
		}
	if($("#recipientLastName").val()!="")
		{
		if(count>0)
			criteria+= " or lower(Recipient_Lname) like lower('%" + $("#recipientLastName").val() +"%')";
		else
			criteria+= " and lower(Recipient_Lname) like lower('%" + $("#recipientLastName").val() +"%')";
		count++;
		}
	if($("#recipientFirstName").val()!="")
		{
		if(count>0)
			criteria+= " or lower(Recipient_Fname) like lower('%" + $("#recipientFirstName").val() +"%')";
		else
			criteria+= " and lower(Recipient_Fname) like lower('%" + $("#recipientFirstName").val() +"%')";
		count++;
		}
	if($("#recipientMiddleName").val()!="")
		{
		if(count>0)
			criteria+= " or lower(RECIPIENT_MNAME) like lower('%" + $("#recipientMiddleName").val() +"%')";
		else
			criteria+= " and lower(RECIPIENT_MNAME) like lower('%" + $("#recipientMiddleName").val() +"%')";
		count++;
		}
	if($("#recipientDOB").val()!="")
	{
	if(count>0)
		criteria+= " or lower(Recipient_Dob) like '" + $("#recipientDOB").val() +"'";
	else
		criteria+= " and lower(Recipient_Dob) like '" + $("#recipientDOB").val() +"'";
	count++;
	}
	
	if($("#recipientabo").val()>0)
	{
	if(count>0)
		criteria+= " or lower(Fk_Codelst_Rep_Aborh) = " + $("#recipientAbo").val() ;
	else
		criteria+= " and lower(Fk_Codelst_Rep_Aborh) = " + $("#recipientAbo").val() ;
	count++;
	}
	
	
	var searchDonor_serverParam = function ( aoData ) {

		aoData.push( { "name": "application", "value": "stafa"});
		aoData.push( { "name": "module", "value": "aphresis_recipient_match"});	
		aoData.push( { "name": "criteria", "value": criteria});	
		
		/* aoData.push( { "name": "param_count", "value":"1"});
		aoData.push( { "name": "param_1_name", "value":":PERSONMRN:"});
		aoData.push( { "name": "param_1_value", "value":"'"+searchMrn+"'"}); */
													
		};
	
	
	
	var searchRecipient_aoColumnDef = [	
		                               
	   	               				{"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
	   	               					link ="<input type='radio' name='recipientSelect' id='recipientSelect' onclick='javascript:setRecipientDetails(\""+source[0]+"\",\""+source[1]+"\",";
	   	               					link+="\""+source[2]+"\",\""+source[3]+"\",\""+source[4] +"\",\""+source[5] +"\",\""+source[6]+"\",\""+source[9] +"\",\""+source[10]+"\");'/>";
	   	               					
	   	               				return link;
	   	               				}},
	   	               				{    "sTitle":'Recipient Name',
	   	               				 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
	   	               					 link = source[1] + " "+ source[2];
	   	               				return link;
	   	               				}},
	   	               				{    "sTitle":'<s:text name="stafa.label.recipientdob"/>',
	   	               				 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
	   	               					 link = source[3];
	   	               				     return link;
	   	               				 
	   	               				    }},
	   	               				{    "sTitle":'Recipient MRN',
	   	               				        "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
	   	               				    return source[4];
	   	               				  }},
	   	               				  {    "sTitle":'Recipient SSN',
	   	               				        "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
	   	               				    return source[5];
	   	               				  }} ,
	   	               				{    "sTitle":'<s:text name="stafa.label.recipientabo"/>',
	   	               				        "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
	   	               				    return source[6];
	   	               				  }} ,
	   	               				{    "sTitle":'Language Spoken',
	   	            				        "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
	   	            				    return source[7];
	   	            				  }}    
	   	               							
	   	               				];		
	
	//alert("type " + type);
//?BB	var adtData = getADTData($("#donorMRN").val());
	
	var searchTableObj;
		//alert("search_aoColumn"+search_aoColumn.length);

		searchTableObj = constructTableWithoutColManager(true,'recipientMatchResults',searchDonor_serverParam,searchRecipient_aoColumnDef,search_aoColumn);
		
		
		
	}catch(err){
		alert("error " + err);
	}
}






function savePopup(){
	//saveDonor();
	 try{
		//var donorMrnLength=$("#donorMRN").val().length;
		var recipientMrnLength=$("#recipientMRN").val().length;
		var recipientSsnLength=$("#recipientSSN").val().length;
		if(  recipientMrnLength >0){
			 if(recipientSsnLength >0) 
				 {
				if(saveRecipient()){
					return true;
					}
				 }
			 else
				 {
				 alert("Please enter SSN");
				 return false;
				 }
			}else{
				alert("Please Enter the MRN");
				
				return false;
				}
	}catch(e){
		alert("exception " + e);
	} 
}
function saveRecipient(){
		
		var mrn = "'"+$("#recipientMRN").val()+"'";
		var url = "checkDuplicateMrn.action?recipientMrn="+mrn;
		var form="recipientNewForm";
		var result = ajaxCall(url,form);
		if(result.status !="" && result.status !=null){
			alert("Already Exists");
		}
		else
			{
			var url="saveRecipient";
			var formName = "recipientNewForm";
			//alert("form " + $("#"+formName).html());
			response = ajaxCall(url,formName);
			//alert("response " + response.status);
				if(response.status !=""){
				alert("Data Saved Successfully");
				closePopup();
				constructRecipientTrackerList(true);
				return true;
				}
		} 
}
function closePopup(){
	try{
		
		$("#recipientNewForm").reset();
		$("#modalpopup").dialog('close');
    	//jQuery("#addNewDonor").dialog("destroy");
    	
		//?BB stafaPopUp("close");
		
  	}catch(err){
		alert("err" + err);
	}
}
$(document).ready(function(){
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"}); 
	  $("select").uniform(); 
      $(window).resize();
      hide_slidewidgets();
      hide_slidecontrol();      
     // constructRecipientMatchList(false);
    
								
	 
    $("select").uniform(); 
});
$(function() {
	$("#recipientDOB").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
        changeYear: true, yearRange: "c-50:c+45"});
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true, changeYear: true, yearRange: '1900:' + new Date().getFullYear(), maxDate: "+1m +1w"});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" );
});
</script>



<div id="addNewRecipient" class="model" >
	<form action="#" id="recipientNewForm"><br>
	<s:hidden name="primDiagnosis" id="primDiagnosis"/>
	<s:hidden name="dateOfRefferal" id="dateOfRefferal"/>
	<s:hidden name="gender" id="gender"/>
	<s:hidden name="recipientAddressLine1" id="recipientAddressLine1"/>
	<s:hidden name="recipientAddressLine2" id="recipientAddressLine2"/>
	<s:hidden name="recipientCity" id="recipientCity"/>
	<s:hidden name="recipientState" id="recipientState"/>
	<s:hidden name="recipientCountry" id="recipientCountry"/>
	<s:hidden name="recipientZipCode" id="recipientZipCode"/>
	<s:hidden name="isTranslaortneed" id="isTranslaortneed"/>
	<s:hidden name="recipinetPrimaryPhone" id="recipinetPrimaryPhone"/>
	<s:hidden name="recipinetHomePhone" id="recipinetHomePhone"/>
	<s:hidden name="recipinetWorkPhone" id="recipinetWorkPhone"/>
	<s:hidden name="recipientEmail" id="recipientEmail"/>
	<s:hidden name="maritialStatus" id="maritialStatus"/>
	<s:hidden name="other" id="other"/>
	<s:hidden name="kinLastName" id="kinLastName"/>
	<s:hidden name="kinFirstName" id="kinFirstName"/>
	<s:hidden name="race" id="race"/>
	<s:hidden name="ethnicity" id="ethnicity"/>
	<s:hidden name="kinAddressLine1" id="kinAddressLine1"/>
	<s:hidden name="kinAddressLine2" id="kinAddressLine2"/>
	<s:hidden name="kinCity" id="kinCity"/>
	<s:hidden name="kinState" id="kinState"/>
	<s:hidden name="kinCountry" id="kinCountry"/>
	<s:hidden name="kinZip" id="kinZip"/>
	<s:hidden name="primCntactPhne" id="primCntactPhne"/>
	<s:hidden name="primCntactRelation" id="primCntactRelation"/>
	<s:hidden name="kinWorkPhone" id="kinWorkPhone"/>
	  <input type="hidden" id="isTempData" name="isTempData" value=""/>
    <input type="hidden" id="scanType" name="scanType" value=""/>
		
       
            <table border="0" width="100%" >
             <tr> <td style="font-weight:bold"><s:text name = "stafa.label.recipient"/></td><input type="hidden" name="recipientPerson" id="recipientPerson" value="0" />
 			 
 			 <tr>
 			 <td>
 			 </td>
 			 <td align="right">
 			 		 <s:textfield  size="15" id="mrnForSearch"  placeholder="Enter Recipient MRN"  style="width:210px;" onkeypress="return filter(event);" />
 			 </td>
 			 </tr>
 			 
 			  <tr>
                    <td class="tablewapper"><s:text name="stafa.label.recipientMRN"/></td>
                    <td class="tablewapper1">
                    	<s:textfield name="recipientMRN"   id = "recipientMRN" onkeypress="return filter(event);"/>
                      </td>
                       <td class="tablewapper"><s:text name="stafa.label.recipientssn"/></td>
                    	<td class="tablewapper1">
                    	<s:textfield name="recipientSSN"    id = "recipientSSN" onkeypress="return filter(event);" />
                      </td>
                    	
                    	
                   
            	</tr>
            	 <tr>
                    <td class="tablewapper"><s:text name="stafa.label.recipient_LastName"/></td>
                    <td class="tablewapper1">
                    	<s:textfield name="recipientLastName"  id = "recipientLastName" onkeypress="return filter(event);"/>
                      </td>
                       <td class="tablewapper"><s:text name="stafa.label.recipientabo"/></td>
                    	<td class="tablewapper1">
                    	 <s:select id="recipientAbo" onkeypress="return filter(event);" name="recipientAbo"  list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@ABO]" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/> 
                      </td>
                    	
                    	
                   
            	</tr>
				<tr>
                    	<td class="tablewapper"><s:text name="stafa.label.recipient_FirstName"/></td>
                    	<td class="tablewapper1">
                    	<s:textfield name="recipientFirstName"   id = "recipientFirstName" onkeypress="return filter(event);" />
                      </td>
                       
            	</tr>
            	<tr>
            		<td class="tablewapper"><s:text name="stafa.label.recipient_MiddleInitial"/></td>
                    	<td class="tablewapper1">
                    	<s:textfield name="recipientMiddleName"  id = "recipientMiddleName" onkeypress="return filter(event);" />
                      </td>
                       
            	</tr>
            	<tr>
            	<td class="tablewapper"><s:text name="stafa.label.recipientdob"/></td>
                  	<td class="tablewapper1">
                    	
                    	
                    	<s:date   id="dob"  name="dob" format="MMM dd, yyyy" />
	       		            		<s:textfield id="recipientDOB" name="recipientDOB" cssClass="dateEntry calDate" onkeypress="return filter(event);" />
                    	
                   </td>
               
            	</tr>
            	            <tr>
            <td align="right" colspan="4"><input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
				<input type="button" value="Save" onclick="verifyeSign('eSignNext','popup')"/>
				<input type="button" value="Cancel" onclick="javascript:closePopup()"/>
           
            </td>
            </tr>
  
         
            	
            
            
            	</table>	
            	   <table cellpadding="0" cellspacing="0" border="0" id="recipientMatchResults" style="display:none"  class="display">
						<thead>
							<tr>
								<th width="4%" id="recipientMatchColumn"></th>
								<th colspan='1'>Select</th>
								<th colspan='1'><s:text name ="stafa.label.lastName"/></th>
								<th colspan='1'><s:text name ="stafa.label.firstName"/></th>
								<th colspan='1'><s:text name ="stafa.label.recipientDOB"/></th>
								<th colspan='1'><s:text name ="stafa.label.recipientMRN"/></th>
								<th colspan='1'><s:text name ="stafa.label.recipientSSN"/></th>
								<th colspan='1'><s:text name ="stafa.label.recipientabo"/></th>
								<th colspan='1'><s:text name ="stafa.label.languageSpoken"/></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						</table>
            </form>
</div>