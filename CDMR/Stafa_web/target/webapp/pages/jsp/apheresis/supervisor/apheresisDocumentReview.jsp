<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
$(function() {
	$(".ui-dialog-titlebar-close").click(function(){
		$('#imageFile').css("display","none");
		$('#imageFile').attr("src","");
		});
	
	var documentId = docsId;
	var documentType = docsType;
	var document
	var url = contextpath+"/viewdocument.action?docId="+documentId;
	
	//alert("Type : "+documentType+"==="+documentType.indexOf("image"));
	//alert("test");
	try{
	if(documentType.indexOf("image")>=0){
		$('#pdfFile').css("display","none");
		$('#imageFile').css("display","block");
		$('#imageFile').attr("src",url);
		}
	else{
		$('#imageFile').css("display","none");
		$('#pdfFile').css("display","block");
		$('#pdfFile').attr("type",documentType);
		$('#pdfFile').attr("src",url);
		$("#spanFileName").text(docsName);
	}
	}catch(error){
		alert("error in file plugin");
	}
	url = url+"&type='download'";
	if ($.browser.msie) {
		
		window.open( url, "Documents", "status = 1, height = 0, width = 0, resizable = 0" );	
	}
});
function closeReview(){
	jQuery("#docsReview").dialog("destroy");
	$('#imageFile').css("display","none");
	$('#imageFile').attr("src","");
}

function reviewedDoc(){
	//alert("reviewedDocs ");
	
	closeReview();
	/*var docs = $(window.parent.document);
	  //alert("donor : "+$(docs).find("#donor").val());
	  $("#donor").val($(docs).find("#donor").val());
	  donorVal=$("#donor").val();
	  enType=$("#entityType").val();
	 // alert(donorVal);alert(enType);
	 */
	// alert("test");
	 var docEntityType=$("#docEntityType").val();
	 var docEntityId=$("#docEntityId").val();
	 var jsonData = "documentIds:" +docsId;
	 var url="reviewDocument";
	 var response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	 if(typeof docEntityType!='undefined' && typeof docEntityId!='undefined' && docEntityType!='' && docEntityId!=''){ 
		 constructDocumentTable(true,docEntityId,docEntityType,'documentreviewTable');
		 constructDocumentHistoryTable(true,docEntityId,docEntityType,'documentreviewTable1');
	 }else{
		constructDocumentTable(true,donorVal,enType,'documentreviewTable');
	}
		  
}
</script>
</head>
<body>
<input type="hidden" id="docEntityType"/>
<input type="hidden" id="docEntityId"/>
<table width="100%" height="600px" border="0">
	<tr>
		<td>FileName: <span id="spanFileName">&nbsp;</span></td>
	
		<td><form><input type="button" onclick="javascript:reviewedDoc();" value="Reviewed"> &nbsp; <input type="button" onclick="javascript:closeReview();" value="Cancel"></form> </td>
	</tr>
	<tr>
		<td colspan="3" height="600px"><div class="" style="height:100%;width:100%;" >
			 <embed height="95%" id="pdfFile" width="100%" src="pdf1/preeScreen.pdf#toolbar=0&navpanes=0&scrollbar=1" type="application/pdf" />
			 <img width="100%" height="95%" id="imageFile" style="display:none" src=""> 
			</div>
			</td>
	</tr>
</table>
</body>
</html>
