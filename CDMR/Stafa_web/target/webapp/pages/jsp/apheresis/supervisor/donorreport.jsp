<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/dataSearch.js"></script>

<script>

$(document).ready(function(){
	var donor = $("#donor").val();
	var donorProcedure = $("#donorProcedure").val();
	hide_slidewidgets();
	hide_slidecontrol();
	constructLabTable(false,'labResultTable',donor,donorProcedure);
	constructCollTable(false,'collectionInfoTable',donor,donorProcedure);
	constructDocumentTable(false,donor,'documentreviewTable');
	constructDocumentHistoryTable(false,donor,'documentreviewTable1');
	$("select").uniform(); 	
});

function constructCollTable(flag,tableId,donor,donorProcedure){
	var criteria = " and coll.fk_plannedprocedure ="+donorProcedure;
	var collInfo_ColManager = function(){
											$("#collectionInfoTablecol").html($("#collectionInfoTable_wrapper .ColVis"));
										};
	var collInfo_aoColumn = [ { "bSortable":false},
			              	   null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null
				             ];
	var collInfoServerParams = function ( aoData ){
														aoData.push( { "name": "application", "value": "stafa"});
														aoData.push( { "name": "module", "value": "reports_collection_information"});
														aoData.push( { "name": "criteria", "value": criteria});
																																							
														aoData.push( {"name": "col_1_name", "value": "" } );
														aoData.push( { "name": "col_1_column", "value": ""});
														
														aoData.push( { "name": "col_2_name", "value": ""});
														aoData.push( { "name": "col_2_column", "value": ""});	
		
													};
	var collInfo_aoColDef = [
	                           {	"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
                          				 return "<input name='' id= '' type='checkbox' value=''/>";
                         	 	 }},
		                       {	"sTitle":'Collection Day',
		                         	"aTargets": [ 1], "mDataProp": function ( source, type, val ) {
		                                 return source[8];
		                         }},
		                       {	"sTitle":'Mobilization Date',
		                         	"aTargets": [ 2], "mDataProp": function ( source, type, val ) {
		                                 return source[1];
		                        }},
		                       {	"sTitle":'Collection Date',
		                         	"aTargets": [ 3], "mDataProp": function ( source, type, val ) {
		                       	         return source[2];
		                        }},
		                       {	"sTitle":'Target Prescription',
		                         	"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
		                       	         return source[7];
		                        }},
		                       {	"sTitle":'Target Processing Volume(L)',
		                         	"aTargets": [ 5], "mDataProp": function ( source, type, val ) {
		                                 return source[3];
		                        }},
		                       {	"sTitle":'Final Inlet(L)',
		                         	"aTargets": [ 6], "mDataProp": function ( source, type, val ) {
		                                 return source[4];
		                        }},
		                      
		                       {	"sTitle":'Machine',
		                         	"aTargets": [ 7], "mDataProp": function ( source, type, val ) {
		                                 return source[5];
		                        }},
		                         
		                       {	"sTitle":'Product ID',
		                         	"aTargets": [ 8], "mDataProp": function ( source, type, val ) {
		                                 return source[6];
		                        }}
		                      
		                     ];
	constructTable(flag,tableId,collInfo_ColManager,collInfoServerParams,collInfo_aoColDef,collInfo_aoColumn);
}													

/*Lab Results Server Parameter Starts*/

function constructLabTable(flag,tableId,donor,donorProcedure){
	
	var criteria = "and pp.pk_plannedprocedure ="+donorProcedure;
	var labResults_ColManager = function(){
												$("#labresultColumn").html($("#"+tableId+"_wrapper .ColVis"));
											 };
	var labResults_aoColumn = [ { "bSortable": false},
	                               null,
	 				               null,
	 				               null,
	 				               null,
	 				               null,
	 				               null,
	 				               null,
	 				               null,
	 				               null,
	 				               null
	 	             ];	
	var labResultsServerParams = function ( aoData ) {
														aoData.push( { "name": "application", "value": "stafa"});
														aoData.push( { "name": "module", "value": "reports_lab_results"});
														aoData.push( { "name": "criteria", "value": criteria});
														
														aoData.push( {"name": "col_1_name", "value": "lower(NVL((SELECT PROTOCOL_NAME FROM ER_PROTOCOL WHERE PK_PROTOCOL =  PP.FK_CODELSTPLANNEDPROCEDURE),''))" } );
														aoData.push( { "name": "col_1_column", "value": "lower(collectionday)"});
														
														aoData.push( { "name": "col_2_name", "value": "lower(NVL(DLRES.DLR_WBC,0))"});
														aoData.push( { "name": "col_2_column", "value": "lower(WBC)"});	
														
														aoData.push( { "name": "col_3_name", "value": "lower(NVL(DLRES.DLR_CD34,0))"});
														aoData.push( { "name": "col_3_column", "value": "lower(CD34)"});		
														
														aoData.push( { "name": "col_4_name", "value": "lower(nvl(dlres.dlr_cd34per,0))"});
														aoData.push( { "name": "col_4_column", "value": "lower(CD34PER)"});		
														
														aoData.push( { "name": "col_5_name", "value": "lower(NVL(DLRES.DLR_HTC,0))"});
														aoData.push( { "name": "col_5_column", "value": "lower(HCT)"});		
														
														aoData.push( { "name": "col_6_name", "value": "lower(NVL(DLRES.DLR_MNCPER,0))"});
														aoData.push( { "name": "col_6_column", "value": "lower(MNCPER)"});		
														
														aoData.push( { "name": "col_7_name", "value": "lower(NVL(PLRES.PLR_WBC,0))"});
														aoData.push( { "name": "col_7_column", "value": "lower(PWBC)"});		
														
														aoData.push( { "name": "col_8_name", "value": "lower(NVL(PLRES.PLR_HCT,0))"});
														aoData.push( { "name": "col_8_column", "value": "lower(PHCT)"});		
														
														aoData.push( { "name": "col_9_name", "value": "lower(NVL(PLRES.PLR_MNCPER,0))"});
														aoData.push( { "name": "col_9_column", "value": "lower(PMNCPER)"});		
														
														/* aoData.push( { "name": "col_10_name", "value": "lower(NVL(PLRES.PLR_CDYIELD,0))"});
														aoData.push( { "name": "col_10_column", "value": "lower(PCDYIELD)"});	 */	
														
														aoData.push( { "name": "col_10_name", "value": ""});
														aoData.push( { "name": "col_10_column", "value": ""});		
													};
	var labResults_aoColumnDef = [
	                         {		"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
	                             		return "<input name='' id= '' type='checkbox' value='' />";
	                             }},
	                          {	"sTitle":'Collection Day',
	                            	"aTargets": [ 1], "mDataProp": function ( source, type, val ) {
	                                    return source[18];
	                            }},
	                          {	"sTitle":'Donor WBC',
	                            	"aTargets": [ 2], "mDataProp": function ( source, type, val ) {
	                                    return source[9];
	                           }},
	                          {	"sTitle":'Donor CD34',
	                            	"aTargets": [ 3], "mDataProp": function ( source, type, val ) {
	                          	         return source[10];
	                           }},
	                          {	"sTitle":'Donor %CD34',
	                            	"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
	                          	         return source[11];
	                           }},
	                          {	"sTitle":'Donor HCT',
	                            	"aTargets": [ 5], "mDataProp": function ( source, type, val ) {
	                                    return source[12];
	                           }},
	                          {	"sTitle":'Donor %MNC',
	                            	"aTargets": [ 6], "mDataProp": function ( source, type, val ) {
	                                    return source[13];
	                           }},
	                         
	                          {	"sTitle":'Product WBC',
	                            	"aTargets": [ 7], "mDataProp": function ( source, type, val ) {
	                                    return source[14];
	                           }},
	                            
	                          {	"sTitle":'Product HCT',
	                            	"aTargets": [ 8], "mDataProp": function ( source, type, val ) {
	                                    return source[15];
	                           }},
	                          {	"sTitle":'Product %MNC',
	                            	"aTargets": [ 9], "mDataProp": function ( source, type, val ) {
	                                    return source[16];
	                           }}, 
	                         /*  {	"sTitle":'CD/Yield',
	                            	"aTargets": [ 10], "mDataProp": function ( source, type, val ) {
	                                    return source[17];
	                           }}, */
	                          {	"sTitle":'CD/Cuml',
	                            	"aTargets": [ 10], "mDataProp": function ( source, type, val ) {
	                                    return source[23];
	                           }}
	                           
	                        ];
		constructTable(flag,tableId,labResults_ColManager,labResultsServerParams,labResults_aoColumnDef,labResults_aoColumn);
}
function constructDocumentTable(flag,donor,tableId) {
	//alert("construct table : "+tableId+",Donor : "+donor);
		var criteria = "";
		if(donor!=null && donor!=""){
			//criteria = " and fk_Entity ="+donor ;
			criteria = donor ;
		}
		var documentColumn = [ { "bSortable": false},
			                	null,
			                	null,
			                	null,
			                	null,
			                	null
				             ];
													
		var documentServerParams = function ( aoData ) {
										aoData.push( { "name": "application", "value": "stafa"});
										aoData.push( { "name": "module", "value": "aphresis_DocumentReview"});
										//aoData.push( { "name": "criteria", "value": criteria});
										
										aoData.push( { "name": "param_count", "value":"1"});
										aoData.push( { "name": "param_1_name", "value":":DONOR:"});
										aoData.push( { "name": "param_1_value", "value":criteria});
										
										aoData.push( {"name": "col_1_name", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_DOCUMENTTYPE))" } );
										aoData.push( { "name": "col_1_column", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_DOCUMENTTYPE))"});
										
										aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
										aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
													
										
					};
		var documentaoColDef = [
					                  {		
					                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
				                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
				                	 			return "<input type='hidden' id='documentRef' name='documentRef' value='"+source[0] +"' ><input type='checkbox' id='docid' name='docid' value='"+source[0] +"' >";
				                	 			//return "";
				                	     }},
					                  { "sTitle": "Document Title",
				                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
					                	    	 //alert('docTitle'+source.documentFileName); 
					                	 		return source[6];
					                	 }},
									  { "sTitle": "Document Date",
					                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
											    return source[2];
											    }},
									  { "sTitle": "Attachment",
											 "aTargets": [3], "mDataProp": function ( source, type, val ) {
										 	   return source[1] + " &nbsp;&nbsp;<a href='#' onclick='openPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\")'> View </a>";
									  		}},
									  { "sTitle": "Reviewed",
									  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
									  			 if(source[3] ==1){
									  				 return "Reviewed";
									  			 }else{
									  				 return "";
									  			 }
										  		//return "<input type='checkbox'/>";
										  }},
									  { "sTitle": "Version #",
					                		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
											    return source[7];
											    }}
									];
		var documentColManager = function () {
												$("#documentreviewColumn").html($("#"+tableId+"_wrapper .ColVis"));
											};
											
		constructTable(flag,tableId,documentColManager,documentServerParams,documentaoColDef,documentColumn);
	} 
	
function constructDocumentHistoryTable(flag,donor,tableId) {
	 	//alert("construct table : "+tableId+",Donor : "+donor);
		var criteria = "";
		if(donor!=null && donor!=""){
			criteria = " and fk_Entity ="+donor ;
		}
		var docHistoryaoColumn =  [ { "bSortable": false},
					                	null,
					                	null,
					                	null,
					                	null,
					                	null
						            ];
		
		var docHistoryServerParams = function ( aoData ) {
										aoData.push( { "name": "application", "value": "stafa"});
										aoData.push( { "name": "module", "value": "aphresis_DocumentReview_History"});
										aoData.push( { "name": "criteria", "value": criteria});
										
										aoData.push( {"name": "col_1_name", "value": "lower(DOCUMENT_FILENAME)" } );
										aoData.push( { "name": "col_1_column", "value": "lower(DOCUMENT_FILENAME)"});
										
										aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
										aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
													
										
					};
					
		var	docHistoryaoColDef = [
					                  {		
					                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
				                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
				                	 			return "<input type='hidden' id='documentRef' name='documentRef' value='"+source[0] +"' > ";
				                	 			//return "";
				                	     }},
					                  { "sTitle": "Document Title",
				                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
					                	    	 //alert('docTitle'+source.documentFileName); 
					                	 		return source[6];
					                	 }},
									  { "sTitle": "Document Date",
					                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
											    return source[2];
											    }},
									  { "sTitle": "Attachment",
											 "aTargets": [3], "mDataProp": function ( source, type, val ) {
										 	   return source[1] + " &nbsp;&nbsp;<a href='#' onclick='openPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\")'> View </a>";
									  		}},
									  { "sTitle": "Reviewed",
									  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
									  			 if(source[3] ==1){
									  				 return "Reviewed";
									  			 }else{
									  				 return "";
									  			 }
										  		//return "<input type='checkbox'/>";
										  }},
									  { "sTitle": "Version #",
					                		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
											    return source[7];
											    }}
									];
		
		var docHistoryColManager =  function () {
												$("#documentreviewColumn1").html($("#"+tableId+"_wrapper .ColVis"));
									};
									
		constructTable(flag,tableId,docHistoryColManager,docHistoryServerParams,docHistoryaoColDef,docHistoryaoColumn);

						
	} 
	                        
</script>
<form>
<s:hidden id="donor" name="donor" value="%{donor}"/>
<s:hidden id="donorProcedure" name="donorProcedure" value="%{donorProcedure}"/>

<div class="column">       
	<div  class="portlet">
    	<div class="portlet-header ">Donor Details</div>
		<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Donor MRN</td>
					<td class="tablewapper1"><s:property value="%{donorDetails.personMRN}"/></td>
					<td class="tablewapper">Donor ABO/Rh</td>
					<td class="tablewapper1"><s:property value="%{donorDetails.bloodGrpName}"/></td>
				</tr>
				<tr>
					<td class="tablewapper">Donor Name</td>
					<td class="tablewapper1"><s:property value="%{donorDetails.lastName}"/>&nbsp;<s:property value="%{donorDetails.firstName}"/></td>
					<td class="tablewapper">Donor Abs</td>
					<td class="tablewapper1"><s:property value="%{donorDetails.donorAbsDesc}"/></td>
                </tr>	
                <tr>
                 	<td class="tablewapper">Donor DOB</td>
               		<td class="tablewapper1"><s:property value="%{donorDetails.dob}"/></td>
					<td class="tablewapper">Donor Weight</td>
					<td class="tablewapper1"><s:property value="%{donorDetails.personWeight}"/></td>
				</tr>
				<tr>
					<td class="tablewapper">Donor Type</td>
               		<td class="tablewapper1"><s:property value="%{donorDetails.donorTypeName}"/></td>
					<td class="tablewapper">Donor Physician</td>
					<td class="tablewapper1"><s:property value="%{donorDetails.physicianName}"/></td>
				</tr>
			</table>
        </div>
    </div>
</div>

<div class="column">       
	<div  class="portlet">
    	<div class="portlet-header ">Recipient Details</div>
		<div class="portlet-content">
		
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Recipient Name</td>
					<td class="tablewapper1"><s:property value="%{donorDetails.recipient_name}"/></td>
					<td class="tablewapper">Recipient ABO/Rh</td>
					<td class="tablewapper1"><s:property value="%{donorDetails.recipient_bloodGroup}"/></td>
				</tr>
				<tr>
					<td class="tablewapper">Recipient Weight</td>
					<td class="tablewapper1"><s:property value="%{donorDetails.recipient_weight}"/></td>
					<td class="tablewapper">Recipient Abs</td>
					<td class="tablewapper1"><s:property value="%{donorDetails.recipientAbsDesc}"/></td>
                </tr>
               		
			</table>
        </div>
    </div>
</div>

<div class="column">
	<div  class="portlet">
		<div class="portlet-header ">Document Review</div>
		<div class="portlet-content">
			<div id="tabs">
			    <ul >
			     <li><a href="#tabs-1">Current</a></li>
			      <li><a href="#tabs-2">Historic</a></li>
			    </ul>
			  	<div id="tabs-1" >
			  		<div>&nbsp;</div>
					<%-- <s:form id="attachedDocsForm" name="attachedDocsForm" >
					<s:hidden id="donor" name="donor" value="%{donor}"/>
					<s:hidden id="entityType" name="entityType" value="DONOR"/>
					<span class="hidden">
						<s:select id="docsTitleTemp" name="docsTitleTemp" list="lstDocumentType" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/>
					</span> --%>
					<div class="tableOverflowDiv">
						<table cellpadding="0" cellspacing="0" border="0" id="documentreviewTable" class="display" width="100%">
							<thead>
								<tr>
									<th width="4%" id="documentreviewColumn"></th>
									<th>Document Title</th>
									<th>Document Date</th>
									<th>Attachment</th>
									<th>Reviewed</th>
									<th>Version #</th>
								</tr>
							</thead>
						</table>
					</div>
					<%-- </s:form> --%>
				</div>	
				<div id="tabs-2" >
				<div>&nbsp;</div>
				<!-- <div><b>Historic documents</b></div> -->
					<div class="tableOverflowDiv">
					  	<table cellpadding="0" cellspacing="0" border="0" id="documentreviewTable1" class="display" width="100%">
							<thead>
								<tr>
									<th width="4%" id="documentreviewColumn1"></th>
									<th>Document Title</th>
									<th>Document Date</th>
									<th>Attachment</th>
									<th>Reviewed</th>
									<th>Version #</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>	
			</div>
		</div>	
	</div>
</div>

<div class="column">		
	<div  class="portlet">
		<div class="portlet-header">Lab Result</div>
		<div class="portlet-content">
			<div style="overflow-x:auto;overflow-y:hidden">
				<table cellpadding="0" cellspacing="0" border="1" id="labResultTable" class="display" >
					<thead>
					   <tr>
					   		<th width="4%" id="labresultColumn"></th>
							<th >Collection Day</th>
							<th >Donor WBC</th>
							<th >Donor CD34</th>
							<th >Donor% CD34</th>
							<th >Donor HCT</th>
							<th >Donor% MNC</th>
							<th >Product WBC</th>
							<th >Product HCT</th>
							<th >Product %MNC</th>
							<!-- <th>CD Yield</th> -->
							<th>CD Cuml</th>
						</tr> 
					</thead>
					<tbody>
					 	
					</tbody>
				</table>
			</div>
		</div>
	</div>	
</div>

<div class="column">		
		<div  class="portlet">
			<div class="portlet-header">Collection Information</div>
			<div class="portlet-content">
			<div style="overflow-x:auto;overflow-y:hidden">
			<table cellpadding="0" cellspacing="0" border="1" id="collectionInfoTable" class="display" >
				<thead>
				   <tr>
				   		<th width="4%" id="collectionInfoTablecol"></th>
						<th>Collection Day</th>
						<th>Mobilization Date</th>
						<th>Collection Date</th>
						<th>Target Prescription</th>
						<th>Target Processing Volume(L)</th>
						<th>Final Inlet(L)</th>
						<th>Machine</th>
						<th>Product ID</th>
				</tr> 
				</thead>
				<tbody>
				 	
				</tbody>
			</table>
			</div>
	</div>
	</div>	
</div>
</form>
<script>
$(function() {
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c",maxDate: '0'});
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});
	$( "#tabs" ).tabs();

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});


</script>
