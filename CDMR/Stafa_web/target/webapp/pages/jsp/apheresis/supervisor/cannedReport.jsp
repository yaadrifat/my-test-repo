<%@ include file="../../common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/ui/base/jquery.ui.tabs.css" media="screen" />
<script type="text/javascript" src="js/dataSearch.js"></script>


<script type="text/javascript">

/*Validation Data Sheet Server Parameter Starts*/
var validationData_ColManager = function(){
											$("#validationDataTableColumn").html($("#validationDataTable_wrapper .ColVis"));
										 };
var validationData_aoColumn = [ { "bSortable": false},
                               null,
 				               null,
 				               null,
 				               null,
 				               null,
 				               null,
 				               null,
 				               null,
 				               null,
 				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null
 	             ];	
var validationDataServerParams = function ( aoData ) {
														aoData.push( { "name": "application", "value": "stafa"});
														aoData.push( { "name": "module", "value": "validation_data_sheet"});	
														
														aoData.push( {"name": "col_1_name", "value": "lower(NVL(P.PERSON_LNAME, ' ' )|| ' ' || NVL(P.PERSON_FNAME, ' ' ))" } );
														aoData.push( { "name": "col_1_column", "value": "lower(PNAME)"});
														
														aoData.push( { "name": "col_2_name", "value": "lower((select codelst_desc from er_codelst where pk_codelst = d.fk_codelstdonortype))"});
														aoData.push( { "name": "col_2_column", "value": "lower(donationtype)"});
														
														aoData.push( { "name": "col_3_name", "value": "lower(TO_CHAR(P.PERSON_DOB,'Mon dd, yyyy'))"});
														aoData.push( { "name": "col_3_column", "value": "P.PERSON_DOB"});
														
														aoData.push( { "name": "col_4_name", "value": "lower(P.PERSON_WEIGHT)"});
														aoData.push( { "name": "col_4_column", "value": "P.PERSON_WEIGHT"});
														
														aoData.push( { "name": "col_5_name", "value": "lower(NVL((select codelst_desc from er_codelst where pk_codelst = d.fk_donor_diagnosis),' '))"});
														aoData.push( { "name": "col_5_column", "value": "lower(DIAGNOSIS)"});
														
														aoData.push( { "name": "col_6_name", "value": "lower(nvl((select nvl(person_weight,0) from person where pk_person=d.fk_receipent),0))"});
														aoData.push( { "name": "col_6_column", "value": "recip_weight"});
														
														aoData.push( { "name": "col_7_name", "value": "lower(nvl((select co_targetcd34counts ||' '|| nvl((select  codelst_desc from er_codelst where pk_codelst=FK_CODELST_TARGETCD34UNITS),' ')   from collection_order where entitytype = 'PLANNEDPROCEDURE' and fk_entity = pp.pk_plannedprocedure),' '))"});
														aoData.push( { "name": "col_7_column", "value": "23"});
														
														aoData.push( { "name": "col_8_name", "value": "lower(lower(NVL((SELECT to_char(CO_TARGETPROCESSINGVOLUME,'9999999990.99999') from collection_order where entitytype = 'PLANNEDPROCEDURE' and fk_entity = pp.pk_plannedprocedure),0)))"});
														aoData.push( { "name": "col_8_column", "value": "MCLC"});
														
														aoData.push( { "name": "col_9_name", "value": "lower(NVL((SELECT max(PROCESSING_INLET) FROM ER_APHERESIS_COLLECTION WHERE FK_PLANNEDPROCEDURE=PP.PK_PLANNEDPROCEDURE),' '))"});
														aoData.push( { "name": "col_9_column", "value": "to_number(INLET)"});
														
														aoData.push( { "name": "col_10_name", "value": "lower(nvl((select protocol_name from er_protocol where pk_protocol =  pp.fk_codelstplannedprocedure),''))"});
														aoData.push( { "name": "col_10_column", "value": "lower(collectionday)"});
														
														aoData.push( { "name": "col_11_name", "value": "lower(NVL(DLRES.DLR_WBC,0))"});
														aoData.push( { "name": "col_11_column", "value": "9"});
														
														aoData.push( { "name": "col_12_name", "value": "lower(NVL(DLRES.DLR_CD34,0))"});
														aoData.push( { "name": "col_12_column", "value": "10"});
														
														aoData.push( { "name": "col_13_name", "value": "lower(NVL(DLRES.DLR_HTC,0))"});
														aoData.push( { "name": "col_13_column", "value": "11"});
														
														aoData.push( { "name": "col_14_name", "value": "lower(NVL(DLRES.DLR_MNCPER,0))"});
														aoData.push( { "name": "col_14_column", "value": "12"});
														
														aoData.push( { "name": "col_15_name", "value": "lower(NVL((SELECT max(MID_COUNT_WBC) FROM ER_APHERESIS_COLLECTION WHERE FK_PLANNEDPROCEDURE=PP.PK_PLANNEDPROCEDURE),' '))"});
														aoData.push( { "name": "col_15_column", "value": "to_number(MCW)"});
														
														aoData.push( { "name": "col_16_name", "value": "lower(NVL(PLRES.PLR_WBC,0))"});
														aoData.push( { "name": "col_16_column", "value": "13"});
														
														aoData.push( { "name": "col_17_name", "value": "lower(NVL(PLRES.PLR_HCT,0))"});
														aoData.push( { "name": "col_17_column", "value": "14"});
														
														aoData.push( { "name": "col_18_name", "value": "lower(NVL(PLRES.PLR_MNCPER,0))"});
														aoData.push( { "name": "col_18_column", "value": "to_number(PMNCPER)"});
														
														/*aoData.push( { "name": "col_19_name", "value": "lower(NVL(PLRES.PLR_CDYIELD,0))"});
														aoData.push( { "name": "col_19_column", "value": "16"}); */
														
														aoData.push( { "name": "col_19_name", "value": "lower((select  sum(nvl(regexp_replace(plr_cd34, '[^[:digit:]|\.]'),0)) from product_labresult where deletedflag =0 and fk_plannedprocedure = pp.pk_plannedprocedure))"});
														aoData.push( { "name": "col_19_column", "value": "to_number(cdcuml)"});
														
														aoData.push( { "name": "col_20_name", "value": "lower((select rs_name from reagentsupplies where pk_reagentsupplies = (select max(fk_reagentsupplies) from er_preparation_equipment where fk_preparation in (select pk_preparation from er_preparation where fk_plannedprocedure = pp.pk_plannedprocedure and deletedflag =0) and deletedflag =0) ))"});
														aoData.push( { "name": "col_20_column", "value": "lower(equipment)"});
														
														aoData.push( { "name": "col_21_name", "value": "lower((select to_char(created_on, 'Mon dd, yyyy') from adt_screening_results where pk_adtscreening = (select max(pk_adtscreening) from adt_screening_results  where FK_DONOR = D.PK_DONOR and deletedflag =0   )))"});
														aoData.push( { "name": "col_21_column", "value": "IDMTestDrawn"}); 
													};
var validationData_aoColumnDef = [
	                         {		"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
	                             		return "<input name='' id= '' type='checkbox' value='' />";
	                             }},
	                          {	"sTitle":'<s:text name="stafa.label.donorname"/>',
	                                "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
	                                			link="<a href='#' onclick='javascript:getDonorReport(\""+source[1] +"\",\""+source[0]+"\");'>"+source[3]+"</a>";
	                                	return link;
	                             }},
                              {	"sTitle":'<s:text name="stafa.label.donortype"/>',
                                  	"aTargets": [ 2], "mDataProp": function ( source, type, val ) {
                                    	return source[4];
                             	}},
	                          {	"sTitle":'<s:text name="stafa.label.donordob"/>',
	                                "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
	                                	return source[5];
	                                 
	                            }},
	                          {	"sTitle":'Donor Weight(kg)',
	                            	"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
	                                	return source[7];
	                            }},
	                          {	"sTitle":'Diagnosis',
	                            	"aTargets": [ 5], "mDataProp": function ( source, type, val ) {
	                                    return source[6];
	                            }},    
	                          {	"sTitle":'Recipient Weight(kg)',
	                            	"aTargets": [ 6], "mDataProp": function ( source, type, val ) {
	                                    return source[8];
	                            }},       
	                                  
                              {	"sTitle":'Prescription',
                            		"aTargets": [ 7], "mDataProp": function ( source, type, val ) {
                                    	return source[23];
                            	}},
                              {	"sTitle":'Liters Ordered',
                            		"aTargets": [ 8], "mDataProp": function ( source, type, val ) {
                                    	return source[18];
                            	}},
  	                          {	"sTitle":'Final Inlet(L)',
	                            	"aTargets": [ 9], "mDataProp": function ( source, type, val ) {
	                                    return source[20];
	                            }},
	                          {	"sTitle":'Collection Day',
	                            	"aTargets": [ 10], "mDataProp": function ( source, type, val ) {
	                                    return source[17];
	                            }},
	                          {	"sTitle":'Pre WBC',
	                            	"aTargets": [ 11], "mDataProp": function ( source, type, val ) {
	                                    return source[9];
	                           }},
	                          {	"sTitle":'CD34',
	                            	"aTargets": [ 12], "mDataProp": function ( source, type, val ) {
	                          	         return source[10];
	                           }},
	                          {	"sTitle":'Pre HCT',
	                            	"aTargets": [ 13], "mDataProp": function ( source, type, val ) {
	                                    return source[11];
	                           }},
	                          {	"sTitle":'Pre %MNC',
	                            	"aTargets": [ 14], "mDataProp": function ( source, type, val ) {
	                                    return source[12];
	                           }},
	                          {	"sTitle":'Mid-Count WBC',
	                            	"aTargets": [ 15], "mDataProp": function ( source, type, val ) {
	                                    return source[19];
	                           }},
	                          {	"sTitle":'Product WBC',
	                            	"aTargets": [ 16], "mDataProp": function ( source, type, val ) {
	                                    return source[13];
	                           }},
	                            
	                          {	"sTitle":'Product HCT',
	                            	"aTargets": [ 17], "mDataProp": function ( source, type, val ) {
	                                    return source[14];
	                           }},
	                          {	"sTitle":'Product %MNC',
	                            	"aTargets": [ 18], "mDataProp": function ( source, type, val ) {
	                                    return source[15];
	                           }}, 
	                         /*  {	"sTitle":'CD/Yield',
	                            	"aTargets": [ 19], "mDataProp": function ( source, type, val ) {
	                                    return source[16];
	                           }}, */
	                          {	"sTitle":'CD/Cum',
	                            	"aTargets": [ 19], "mDataProp": function ( source, type, val ) {
	                                    return source[22];
	                           }},
	                          {	"sTitle":'Equipment',
	                            	"aTargets": [ 20], "mDataProp": function ( source, type, val ) {
	                                    return source[21];
	                           }},
	                          {	"sTitle":'IDM Verified Date',
	                            	"aTargets": [ 21], "mDataProp": function ( source, type, val ) {
	                                    return source[24];
	                           }}
	                        ];	
$(document).ready(function(){
	//alert("ready");
	hide_slidecontrol();
	hide_slidewidgets();
	hide_innernorth();   
	constructTable(false,'validationDataTable',validationData_ColManager,validationDataServerParams,validationData_aoColumnDef,validationData_aoColumn);
	$("select").uniform();

});	   
function getDonorReport(donor,donorProcedure){
	$("#donor").val(donor);
	$("#donorProcedure").val(donorProcedure);
	//alert("donor:"+donor+"dp::"+donorProcedure);
	response = ajaxCall("loadDonorReport","reports");
	//alert("response::"+response);
	$('#main').html(response);
	//document.getElementById("main").innerHTML=response;
}
</script>
<div id="validationDataDiv">
<div id="tabs">
	<ul>
		<li><a href="#cannedTab">Canned Report</a></li>
		<li><a href="#adhocTab">Ad Hoc Report</a></li>
	</ul>
	<div id="cannedTab" class="column">
		<div class="portlet">
			<div class="portlet-header ">Validation Data Sheet</div>
			<div class="portlet-content">
				<form id="reports" name="reports">
		  				<input type="hidden" id="donor" name="donor" value=""/>
		  				<input type="hidden" id="donorProcedure" name="donorProcedure" value=""/>
		  		</form>
				<div id="validationDataTableDiv" style="overflow-x:auto;overflow-y:hidden;">
					<table cellpadding="0" cellspacing="0" border="1" id="validationDataTable" class="display">
						<thead>
							<tr>
								<th width='4%' id='validationDataTableColumn'></th>
								<th>Donor Name</th>
								<th>Donation Type</th>
								<th>Donor DOB</th>
								<th>Donor Weight(kg)</th>
								<th>Diagnosis</th>
								<th>Recipient Weight(kg)</th>
								<th>Prescription</th>
								<th>Liters Ordered</th>
								<th>Final Inlet(L)</th>
								<th>Collection Day</th>
								<th>Pre WBC</th>
								<th>CD34</th>
								<th>Pre HCT</th>
								<th>Pre %MNC</th>
								<th>Mid-Count WBC</th>
								<th>Product WBC</th>
								<th>Product HCT</th>
								<th>Product %MNC</th>
								<!-- <th>CD/Yield</th> -->
								<th>CD/Cum</th>
								<th>Equipment</th>
								<th>IDM Verified Date</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<!-- 	<div id="adhocTab" class="column">
	</div> -->
</div>
</div>
<script>
$(function() {
	
	
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" );
	
		/* $( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	}); */
	$( "#tabs" ).tabs();
	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});
</script>