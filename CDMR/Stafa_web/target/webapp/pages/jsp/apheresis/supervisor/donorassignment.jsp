<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/dataSearch.js"></script>
<script>
function edit_AssignedDonor(){
	if(checkModuleRights(STAFA_DONORASSIGNMENT,APPMODE_EDIT)){
		var newUsers = "<span id='users'>"+$("#nurseUserspan").html() ;
		
		newUsers = newUsers.replace("onchange=\"alert('change')\""," ");
		newUsers += $("#taskLevelspan").html()+ "</span>&nbsp;<img src = 'images/icons/addnew.jpg' class='cursor' id='' onclick='add_users(this,\"assigndonordetailsTable\")'/>"
		 $("#assigndonordetailsTable tbody tr").each(function (row){
			 if($(this).find("#assignedDonor").is(":checked")){
				 rowHtml =  $(this).closest("tr");
				 if ($(rowHtml).find("#nurseUser").val()== undefined){
					 var tmpNursename=$(rowHtml).find("#nurse").parent().text();
					 newUsers +=  $(rowHtml).find("#nurse").parent().html().replace(tmpNursename,"");
					 $(rowHtml).find("#nurse").parent().html(newUsers);
					 $(rowHtml).find("#nurseUser").val($(rowHtml).find("#nurse").val());
					 $(rowHtml).find("#taskLevel").val('primary');
					 $.uniform.restore('select');
					 $('select').uniform();
				 }
			 }
		 });
	}
}
function getDonorDetails(donor,donorProcedure){
	//alert("donor " + donor);
	$("#donor").val(donor);
	$("#donorProcedure").val(donorProcedure);
	response = ajaxCall("loadDonorDetails","aphresis");
	//alert(response);
	$('#main').html(response);
}
function add_users(rowid,tableid){
	var cell = $(rowid).closest('td');
	var row = cell.closest('tr');
	curr_row = row[0].rowIndex-1;
	//var newUsers = $('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(7)").html()+ "<br>"+$("#nurseUserspan").html() + "&nbsp;" +$("#tasklevelspan").html() ;
	var newUsers = "<br><span id='users'>"+$("#nurseUserspan").html() ;
	
	newUsers = newUsers.replace("onchange=\"alert('change')\"","onchange=javascript:setTaskChangeFlag("+$(row).find("#donor").val()+");");
	newUsers += $("#taskLevelspan").html() +"</span>"
	//$('#pendingdonordetailsTable tbody tr:eq('+curr_row+')').find("td:eq(8)").append(newUsers);
	$('#'+tableid+' tbody tr:eq('+curr_row+')').find("td:eq(8)").append(newUsers);
	$.uniform.restore('select');
	$("select").uniform(); 
}

function savePages(){
	if(checkModuleRights(STAFA_DONORASSIGNMENT,APPMODE_SAVE)){
		try{
			if(saveDonorAssignment()){
				return true;
			}
		}catch(e){
			alert("exception " + e);
		}
	}
}

function saveDonorAssignment(){
	//alert("save");
	
	var rowData="";
	var changeFlag;
	try{
	 $("#pendingdonordetailsTable tbody tr").each(function (row){
	//	alert(" row " + row);
		changeFlag = $("#changeFlag_"+$(this).find("#donor").val()).val();
		taskChangeFlag = $("#taskChangeFlag_"+$(this).find("#donor").val()).val();

		if(changeFlag == 1){
			rowData+= "{entityName:'"+$(this).find("#donorId").val()+"',entityType:'DONOR',entityId:"+$(this).find("#donor").val()+",donor:"+$(this).find("#donor").val()+",plannedProcedure:'"+$(this).find("#donorProcedure").val()+"'";
			var taskData ="";
			var primaryUserIds="",primaryUserNames="",secondaryUserIds="",secondaryUserNames="";
			var taskLevel;
			$(this).find("#users").each(function(){
				
				//?BBtaskData +="{level:'"+$(this).find("#taskLevel").val() +"',userId:'"+$(this).find("#nurseUser").val()+"',userName:'"+$.trim($(this).find("#nurseUser option:selected").text())+"'},";
				taskLevel = $(this).find("#taskLevel").val();
				if(taskLevel =="primary"){
					primaryUserIds += $(this).find("#nurseUser").val()+",";
					primaryUserNames += $.trim($(this).find("#nurseUser option:selected").text())+",";
				}else if(taskLevel =="secondary"){
					secondaryUserIds += $(this).find("#nurseUser").val()+",";
					secondaryUserNames += $.trim($(this).find("#nurseUser option:selected").text())+",";
				}
			});
			//?BB taskData = taskData.substring(0,(taskData.length-1));
			if(primaryUserIds.length >0){
				primaryUserIds = primaryUserIds.substring(0,(primaryUserIds.length-1));
			}
			
			if(secondaryUserIds.length >0){
				secondaryUserIds = secondaryUserIds.substring(0,(secondaryUserIds.length-1));
			}
			if(primaryUserNames.length >0){
				primaryUserNames = primaryUserNames.substring(0,(primaryUserNames.length-1));
			}
			if(secondaryUserNames.length >0){
				secondaryUserNames = secondaryUserNames.substring(0,(secondaryUserNames.length-1));
			}
			rowData+= ",primaryUserIds:'"+primaryUserIds+"',primaryUserNames:'"+primaryUserNames+"',secondaryUserIds:'"+secondaryUserIds+"',secondaryUserNames:'"+secondaryUserNames+"',taskType:'STAFA_DAILYASSIGN'";
			/*//?BB
			if(taskChangeFlag ==1){
				//rowData+= ",assignTo:"+$(this).find("#nurseUser").val()+",assignedUser:'"+$(this).find("#nurseUser option:selected").text() +"',sharedUser:"+$(this).find("#nurseUser").val()+",taskType:'STAFA_DAILYASSIGN'";
				rowData+= ",assignTo:'"+$(this).find("#nurseUser").val()+"',assignedUser:'"+$(this).find("#nurseUser option:selected").text() +"',taskType:'STAFA_DAILYASSIGN'";
			}
			*/
			//?BB rowData+= ",taskData:["+taskData+"],taskType:'STAFA_DAILYASSIGN'";
			rowData+= "},";
		}
		
 	});
	 
	// nurseUser
	//BB assigndonordetailsTable
	$("#assigndonordetailsTable #nurseUser").each(function (row){
		//----------
		 rowHtml =  $(this).closest("tr");
		rowData+= "{entityName:'"+$(rowHtml).find("#donorId").val()+"',entityType:'DONOR',entityId:"+$(rowHtml).find("#donor").val()+",donor:"+$(rowHtml).find("#donor").val()+",plannedProcedure:'"+$(rowHtml).find("#donorProcedure").val()+"'";
		var taskData ="";
		var primaryUserIds="",primaryUserNames="",secondaryUserIds="",secondaryUserNames="";
		var taskLevel;
		$(rowHtml).find("#users").each(function(){
			
			//?BBtaskData +="{level:'"+$(this).find("#taskLevel").val() +"',userId:'"+$(this).find("#nurseUser").val()+"',userName:'"+$.trim($(this).find("#nurseUser option:selected").text())+"'},";
			taskLevel = $(this).find("#taskLevel").val();
			if(taskLevel =="primary"){
				primaryUserIds += $(this).find("#nurseUser").val()+",";
				primaryUserNames += $.trim($(this).find("#nurseUser option:selected").text())+",";
			}else if(taskLevel =="secondary"){
				secondaryUserIds += $(this).find("#nurseUser").val()+",";
				secondaryUserNames += $.trim($(this).find("#nurseUser option:selected").text())+",";
			}
		});
		//?BB taskData = taskData.substring(0,(taskData.length-1));
		if(primaryUserIds.length >0){
			primaryUserIds = primaryUserIds.substring(0,(primaryUserIds.length-1));
		}
		
		if(secondaryUserIds.length >0){
			secondaryUserIds = secondaryUserIds.substring(0,(secondaryUserIds.length-1));
		}
		if(primaryUserNames.length >0){
			primaryUserNames = primaryUserNames.substring(0,(primaryUserNames.length-1));
		}
		if(secondaryUserNames.length >0){
			secondaryUserNames = secondaryUserNames.substring(0,(secondaryUserNames.length-1));
		}
		rowData+= ",primaryUserIds:'"+primaryUserIds+"',primaryUserNames:'"+primaryUserNames+"',secondaryUserIds:'"+secondaryUserIds+"',secondaryUserNames:'"+secondaryUserNames+"',taskType:'STAFA_DAILYASSIGN'";
		/*//?BB
		if(taskChangeFlag ==1){
			//rowData+= ",assignTo:"+$(this).find("#nurseUser").val()+",assignedUser:'"+$(this).find("#nurseUser option:selected").text() +"',sharedUser:"+$(this).find("#nurseUser").val()+",taskType:'STAFA_DAILYASSIGN'";
			rowData+= ",assignTo:'"+$(this).find("#nurseUser").val()+"',assignedUser:'"+$(this).find("#nurseUser option:selected").text() +"',taskType:'STAFA_DAILYASSIGN'";
		}
		*/
		//?BB rowData+= ",taskData:["+taskData+"],taskType:'STAFA_DAILYASSIGN'";
		rowData+= "},";

		
		
		
		
		//-------------------
	});
	
	}catch(err){
		alert("error " + err);
	}
	//alert("rowData " + rowData);
	 if(rowData.length >0){
	//	 $('.progress-indicator').css( 'display', 'block' );
         rowData = rowData.substring(0,(rowData.length-1));
         var url="createTask";
         response = jsonDataCall(url,"jsonData={taskData:["+rowData+"]}");
	       alert("Data Saved Successfully");
	       $('.progress-indicator').css( 'display', 'none' );
	       constructAssigneddonordetails(true);
	       constructPendingdonordetails(true);
	       $.uniform.restore('select');
			$("select").uniform();
	  }
	
}
function constructPendingdonordetails(flag){
	/* Assigned Donor Table Server Parameters Starts */
//alert("construct donor");
	var donorListTable_serverParam = function ( aoData ) {

													aoData.push( { "name": "application", "value": "stafa"});
													aoData.push( { "name": "module", "value": "aphresis_DonorAssignment"});	
													
													aoData.push( {"name": "col_1_name", "value": "lower(p.PERSON_MRN)" } );
													aoData.push( { "name": "col_1_column", "value": "lower(p.PERSON_MRN)"});
													
													aoData.push( { "name": "col_2_name", "value": "lower(NVL(P.PERSON_LNAME, ' ' )|| ' ' || NVL(P.PERSON_FNAME, ' ' ))"});
													aoData.push( { "name": "col_2_column", "value": "lower(PNAME)"});
													
													aoData.push( { "name": "col_3_name", "value": "lower(TO_CHAR(P.PERSON_DOB,'Mon dd, yyyy'))"});
													aoData.push( { "name": "col_3_column", "value": "lower(PDOB)"});
													
													aoData.push( { "name": "col_4_name", "value": "lower((select count(*) from DONOR_LABRESULT where FK_DONOR=d.pk_donor and FK_PLANNEDPROCEDURE=pp.PK_PLANNEDPROCEDURE))"});
													aoData.push( { "name": "col_4_column", "value": "lower(labresult)"});
													
													aoData.push( { "name": "col_5_name", "value": ""});
													aoData.push( { "name": "col_5_column", "value": ""});
													
													aoData.push( { "name": "col_6_name", "value": "lower((select codelst_desc from er_codelst where pk_codelst = d.fk_codelstdonortype))"});
													aoData.push( { "name": "col_6_column", "value": "lower(donationtype)"});
													
													aoData.push( { "name": "col_7_name", "value": "lower(nvl(pp.pk_plannedprocedure,0))"});
													aoData.push( { "name": "col_7_column", "value": "lower(planprocedure)"});
													
													aoData.push( { "name": "col_8_name", "value": ""});
													aoData.push( { "name": "col_8_column", "value": ""});
													
													aoData.push( { "name": "col_9_name", "value": "lower(nvl((select codelst_desc from er_codelst where pk_codelst = d.fk_codelstdonorstatus),'Pending Assignment'))"});
													aoData.push( { "name": "col_9_column", "value": "lower(status)"});
													
													};
	var donorListTable_aoColumnDef = [
			                         {"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
			                             return "<input type='hidden' name='changeFlag_"+source[0] +"' id='changeFlag_"+source[0] +"' value='0'/><input type='hidden' name='taskChangeFlag_"+source[0] +"' id='taskChangeFlag_"+source[0] +"' value='0'/><input type='hidden' name='donor' id='donor' value='"+source[0]+"'/><input type='hidden' name='donorId' id='donorId' value='"+source[1]+"'/>";
			                             }},
			                          {    "sTitle":'<s:text name="stafa.label.donorid"/>',
			                                 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
			                                	 //?BB link = "<a href='#' onclick='javascript:getDonorDetails(\""+source[0] +"\");'>"+source[1]+"</a>";
			                                	 link = source[1];
			                             return link;
			                             }},
			                          {    "sTitle":'<s:text name="stafa.label.donorname"/>',
			                                 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
			                                	 //?BB link = "<a href='#' onclick='javascript:getDonorDetails(\""+source[0] +"\");'>"+source[2] +"</a>";
			                                	 link = source[2];
					                             return link;
			                                 
			                                    }},
			                          {    "sTitle":'<s:text name="stafa.label.donordob"/>',
			                                        "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
			                                    return source[3];
			                                  }},
			                          {    "sTitle":'Lab Result',
			                                        "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
			                                    if(source[4]!=0){
	                                        		return"Reviewed";
	                                        	}else{
	                                        		return"Not Reviewed";
	                                        	}
			                                  }},    
			                          {    "sTitle":'Documents',
			                                        "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
			                                        	/* if(source[5]=="1"){
			                                        		return "Reviewed";
			                                        	}else{
			                                        		return"Not Reviewed";
			                                        	} */
			                                        	//alert("source[5]" + source[5]);
			                                        	//?BB return source[5];
			                                        	return "Reviewed";
			                                  }},       
			                                  
			                          {   "sTitle":'Donor Type',
			                                  "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
			                                      return source[6];
			                              }},
			                          {    "sTitle":'<s:text name="stafa.label.plannedprocedure"/>',
			                                  "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
			                                	//?BB 
			                                	/*  var selectTxt = $("#plannedProcedurespan").html();
			                                	  var searchTxt = 'value="'+source[7] +'"'; 
			                                	  selectTxt = selectTxt.replace(searchTxt,searchTxt +' selected');
			                                	  selectTxt = selectTxt.replace("onchange=\"alert('change')\"","onchange=javascript:setChangeFlag("+source[0]+");");
			                                	 return selectTxt;
			                                	*/ 
			                                	
			                                	var link = "<input type='hidden' id='donorProcedure' name='donorProcedure' value='"+ source[7]+"' />" ;
			                                	link += "<a href='#' onclick='javascript:getDonorDetails(\""+source[0] +"\",\""+source[7]+"\" );'>"+source[8] +"</a>";
			                                	 return link;
			                              }},
			                          {    "sTitle":'<s:text name="stafa.label.nurse"/>',
			                                  "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
			                                	  var selectTxt = "<span id='users'>"+ $("#nurseUserspan").html();
			                                	   selectTxt = selectTxt.replace("onchange=\"alert('change')\"","onchange=javascript:setTaskChangeFlag("+source[0]+");");
			                                	  selectTxt += $("#taskLevelspan").html()+ "</span>&nbsp;<img src = 'images/icons/addnew.jpg' class='cursor' id='' onclick='add_users(this,\"pendingdonordetailsTable\")'/>";
				                                	 return selectTxt;
			                              }},
			                              {    "sTitle":'Status',
			                                  "aTargets": [ 9], "mDataProp": function ( source, type, val ) {
			                                	 
			                                	  return source[10];
			                                	
			                              }} 
			                        ];

	var donorListTable_aoColumn = [ { "bSortable": false},
	                                null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null
			             ];
	var donorListTable_ColManager = function(){
										$("#pendingdonordetails").html($('.ColVis:eq(0)'));
										/*alert("set colmanager");
										// $.uniform.restore('select');
										var restoreFlag = false;
										if($(".selector").each(function(){
										//	alert(" .sel " + $(this).html());
											restoreFlag = true;
										}));
										if(restoreFlag){
										//	alert("inside if");
											//$.uniform.restore('select');
										}
									      // $("select").uniform();
									      */
									 };
									 constructTable(flag,'pendingdonordetailsTable',donorListTable_ColManager,donorListTable_serverParam,donorListTable_aoColumnDef,donorListTable_aoColumn);
									
									
}

function constructAssigneddonordetails(flag){
	
//alert("construct donor");
	var donorListTable_serverParam = function ( aoData ) {

													aoData.push( { "name": "application", "value": "stafa"});
													aoData.push( { "name": "module", "value": "aphresis_DonorAssignment_AssignedDonor"});	
													
													aoData.push( {"name": "col_1_name", "value": "lower(p.PERSON_MRN)" } );
													aoData.push( { "name": "col_1_column", "value": "lower(p.PERSON_MRN)"});
													
													aoData.push( { "name": "col_2_name", "value": "lower(nvl(p.PERSON_LNAME, ' ' )|| ' ' || nvl(p.PERSON_FNAME, ' ' ))"});
													aoData.push( { "name": "col_2_column", "value": "lower(pname)"});
													
													aoData.push( { "name": "col_3_name", "value": "lower(to_char(p.person_dob,'Mon dd, yyyy'))"});
													aoData.push( { "name": "col_3_column", "value": "lower(pdob)"});
													
													aoData.push( { "name": "col_4_name", "value": "lower((select count(*) from DONOR_LABRESULT where FK_DONOR=d.pk_donor and FK_PLANNEDPROCEDURE=pp.PK_PLANNEDPROCEDURE ))"});
													aoData.push( { "name": "col_4_column", "value": "lower(labresult)"});
													
													aoData.push( { "name": "col_5_name", "value": ""});
													aoData.push( { "name": "col_5_column", "value": "lower(documents)"});
													
													aoData.push( { "name": "col_6_name", "value": "lower((select codelst_desc from er_codelst where pk_codelst = d.fk_codelstdonortype))"});
													aoData.push( { "name": "col_6_column", "value": "lower(donationtype)"});
													
													aoData.push( { "name": "col_7_name", "value": "lower(nvl((select protocol_name from er_protocol where pk_protocol =pp.fk_codelstplannedprocedure),' '))"});
													aoData.push( { "name": "col_7_column", "value": "lower(planprocedure)"});
													
													aoData.push( { "name": "col_8_name", "value": "lower(nvl(pp.PP_PRIMARYUSERNAMES,' '))"});
													aoData.push( { "name": "col_8_column", "value": "lower(primarynurse)"});
													
													aoData.push( { "name": "col_9_name", "value": "lower((select codelst_desc from er_codelst where pk_codelst =pp.fk_codelstdonorstatus))"});
													aoData.push( { "name": "col_9_column", "value": "lower(status)"});
																								
													};
	var donorListTable_aoColumnDef = [
			                         {"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
			                             return "<input type='checkbox' id='assignedDonor' name='assignedDonor' value='"+source[0] +"' /> <input type='hidden' name='donor' id='donor' value='"+source[0]+"'/><input type='hidden' name='donorId' id='donorId' value='"+source[2]+"'/>";
			                             }},
			                          {    "sTitle":'<s:text name="stafa.label.donorid"/>',
			                                 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
			                                	 link = source[2];
			                             return link;
			                             }},
			                          {    "sTitle":'<s:text name="stafa.label.donorname"/>',
			                                 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
			                                	 link = source[3];
					                             return link;
			                                 
			                                    }},
			                          {    "sTitle":'<s:text name="stafa.label.donordob"/>',
			                                        "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
			                                    return source[4];
			                                  }},
			                          {    "sTitle":'Lab Result',
			                                        "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
			                                        	if(source[5]!=0){
			                                        		return"Reviewed";
			                                        	}else{
			                                        		return"Not Reviewed";
			                                        	}
			                                  }},    
			                          {    "sTitle":'Documents',
			                                        "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
			                                        	if(source[6]==0 && source[12] >0 ){
			                                        		return"Reviewed";
			                                        	}else{
			                                        		return"Not Reviewed";
			                                        	}
			                                  }},       
			                                  
			                          {   "sTitle":'Donor Type',
			                                  "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
			                                      return source[7];
			                              }},
			                          {    "sTitle":'<s:text name="stafa.label.plannedprocedure"/>',
			                                  "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
			                                	  var link = "<input type='hidden' id='donorProcedure' name='donorProcedure' value='"+ source[1]+"' />" ;
				                                	
			                                	  link += "<a href='#' onclick='javascript:getDonorDetails(\""+source[0] +"\",\""+source[1]+"\" );'>"+source[8] +"</a>";
			                                		 return link;
			                                    
			                              }},
			                          {    "sTitle":'<s:text name="stafa.label.nurse"/>',
			                                  "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
			                                	 link="<input type='hidden' id='nurse' name='nurse' value='"+source[13] + "' />";
			                                	  link += source[9];
			                                	  if(source[9] != " " && source[10] != " "){
			                                		  link +=",";
			                                	  }
			                                	  link += source[10];
			                                      return link;
			                              }},
			                              {    "sTitle":'Status',
			                                  "aTargets": [ 9], "mDataProp": function ( source, type, val ) {
			                                		 return source[11];
			                                     
			                              }} 
			                         
			                        ];

	var donorListTable_aoColumn = [ { "bSortable": false},
	                                null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null
			             ];
	var donorListTable_ColManager = function(){
										$("#assigndonordetails").html($('.ColVis:eq(1)'));
									 };
									 constructTable(flag,'assigndonordetailsTable',donorListTable_ColManager,donorListTable_serverParam,donorListTable_aoColumnDef,donorListTable_aoColumn);
									 
}


var oTable;
$(document).ready(function(){
	hide_slidecontrol();
	hide_slidewidgets();
	constructPendingdonordetails(false);
	constructAssigneddonordetails(false);
	show_innernorth();
    clearTracker();
	/*
	oTable = $("#assigndonordetailsTable").dataTable({
			"sPaginationType": "full_numbers",
			"sDom": 'C<"clear">Rlfrtip',
			"aoColumns": [ { "bSortable":false},
			               null,
			               null,
			               null,
			               null,
			               null,
			               null,
			               null,
			               null,
			               null
			             ],
				"oColVis": {
					"aiExclude": [ 0 ],
					"buttonText": "&nbsp;",
					"bRestore": true,
					"sAlign": "left"
				},
				"fnInitComplete": function () {
			        $("#assigndonordetails").html($('.ColVis:eq(1)'));
				} 
	});
	*/
	//?BB $("select").uniform();
				
});

function setChangeFlag(id){
	//alert("set change flag " + id);
	$("#changeFlag_"+id).val(1);
	
}
function setTaskChangeFlag(id){
	//alert("set change flag " + id);
	$("#taskChangeFlag_"+id).val(1);
	$("#changeFlag_"+id).val(1);
	
}
</script>



<span id="hiddendropdowns" class="hidden">
 <span id="nurseUserspan">
<select id="nurseUser"  name="nurseUser" onchange="alert('change')">
	<option value="">Select</option> 
 <s:iterator value="lstNurseUsers" var="rowVal" status="row">
 	<option value="<s:property value="#rowVal[0]"/>"> <s:property value="#rowVal[1]"/> <s:property value="#rowVal[2]"/></option>
 </s:iterator>
</select>
</span>
<span id="plannedProcedurespan">
 <s:select id="plannedProcedure"  name="plannedProcedure" list="lstPlannedProcedure" listKey="protocol" listValue="protocolName"  headerKey="" headerValue="Select" />
</span>

<span id="taskLevelspan">
 <s:select id="taskLevel"  name="taskLevel" list="lstTaskLevel" listKey="subType" listValue="description"  headerKey="" headerValue="Select" />
</span>

<%-- <span id="statusspan"> 
  <s:select id="status"  name="status" list="lstStatus" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="alert('change')"/>
</span> --%>
</span>
    <form id="aphresis" name="aphresis">
  				<input type="hidden" id="donor" name="donor" value=""/>
  				<input type="hidden" id="donorProcedure" name="donorProcedure" value=""/>
  			</form>	                                     	 
 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header ">Pending Donors</div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="pendingdonordetailsTable" class="display" >
				<thead>				   <tr>
				   		<th width="4%" id="pendingdonordetails"></th>
						<th >Donor Id</th>
						<th >Donor Name</th>
						<th >Donor DOB</th>
						<th >Lab Result</th>
						<th >Documents</th>
						<th >Donor Type</th>
						<th >Planned Procedure</th>
						<th >Nurse</th>
						<th >Status</th>
					</tr> 
				</thead>
				<tbody>
				 	
					
				</tbody>
			</table>
	</div>
	</div>	
</div>

 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header ">Assigned Donors</div>
			<div class="portlet-content">
			 <table><tr><td>&nbsp;</td></tr>
		            <tr>
		            	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;" class="cursor" onclick="edit_AssignedDonor();" />&nbsp;&nbsp;<label class="cursor" onclick="edit_AssignedDonor()"><b>Edit</b></label>&nbsp;</div></td>
						
		            </tr>
	            </table>
			<table cellpadding="0" cellspacing="0" border="0" id="assigndonordetailsTable" class="display" >
				<thead>
				   <tr>
				   		<th width="4%" id="assigndonordetails"></th>
						<th>Donor Id</th>
						<th>Donor Name</th>
						<th>Donor DOB</th>
						<th>Lab Result</th>
						<th>Documents</th>
						<th>Donor Type</th>
						<th>Planned Procedure</th>
						<th>Nurse</th>
						<th>Status</th>
					</tr> 
				</thead>
				<tbody>
				 	
				</tbody>
			</table>
	</div>
	</div>	
</div>
<%-- <div align="left" style="float:right;">
    <form onsubmit="return avoidFormSubmitting()">
        <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
            <tr>
                <td><span>Next:
                <select name="nextOption" id="nextOption" ><option value="">Select</option><option value="loadDonorAssignment.action">Donor Assignment</option><option value="logout.action">Log Out</option></select>
                <input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
				<input type="button" value="Next" onclick="verifyeSign('eSignNext','next')"/></span></td>
            </tr>
        </table>
    </form>
</div> --%>
<script>

$(function() {

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	

	/**For Notes**/
	/* 
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='ui-addnew'></span>");
	
	$( ".portlet-header .ui-addnew" ).click(function() {
		addNewPopup("Add New Product","SupervisoraddNewDialog","addnewAcquisition","700","1100");
	});
	 */
	
	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	//$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});
</script>