<!DOCTYPE html>
<html >
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
<meta content="utf-8" http-equiv="encoding"/>
<!--<meta http-equiv="X-UA-Compatible" content="IE=10;FF=3;OtherUA=4" /> -->
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7;IE=EmulateIE8; IE=EmulateIE9"/>








</head>
<body>
<button  onclick="showGraph1();">ShowChart</button>
<button  onclick="showBar();">ShowBarChart</button>
<div  id="chart1" style="height:320px; width:600px;"></div>
<div  id="chart2" style="margin-top: 30px; height:100px; width:600px;"></div>
<button value="reset" onclick="controllerPlot.resetZoom();">Reset</button>

</body>




<script type="text/javascript" id="dfe24990-f3a7-48a1-a1dc-a9e4c001e266">

//alert("t1");

//    alert("ready");
    var InPr ='[[0.0,85.0],[10.0,87.4],[20.0,96.8],[30.0,96.4],[40.0,96.6],[50.0,96.6]]';
var OutPr = '[[0.0,88.1],[10.0,180.3],[20.5,180.3],[30.0,181.5],[40.0,182.0],[50.0,180.3]]';
var ERPM = '[[0.0,15],[10.0,300],[20.0,305.6],[30.0,150],[40.0,150],[50.0,305.],[60.0,150.]]';

   function showGraph1(){

                   jQuery.jqplot.config.enablePlugins = true;
            var jsonstr="[['AUSTRALIA',22],['AUSTRIA',5],['BERMUDA',16],['CANADA',225],['CHILE',38],['COSTA RICA',55],['DENMARK',7],['ESTONIA',1],['FINLAND',8],['FRANCE',61],['FRENCH POLYNESIA',1],['GERMANY',88],['GUADELOUPE',2],['GUAM',1],['GUERNSEY',2],['HUNGARY',7],['ICELAND',1],['IRELAND',10],['ISRAEL',44],['ITALY',41],['JAPAN',11],['KOREA  REPUBLIC OF',14],['LUXEMBOURG',7],['NETHERLANDS',33],['NEW CALEDONIA',1],['NEW ZEALAND',4],['NORWAY',13],['PUERTO RICO',23],['QATAR',2],['SOUTH AFRICA',10],['SPAIN',86],['SWEDEN',21],['UNITED KINGDOM',210],['UNITED STATES',7986]]";
            alert("jsonstr"+jsonstr);
      plot7 = jQuery.jqplot('chart1',[eval(jsonstr)],
        {
          title: ' ',
          seriesDefaults: {shadow: true, renderer: jQuery.jqplot.PieRenderer, rendererOptions: { showDataLabels: true },
          highlighter: {
              show: true,
              formatString:'%s, %P',

              tooltipLocation: 'sw',
                tooltipAxes: 'pieref', // exclusive to this version
                tooltipAxisX: 80, // exclusive to this version
                tooltipAxisY: 60, // exclusive to this version
              useAxesFormatters:false
            }
            },
          legend:{
                    show:true,
                    placement: 'outside',
                    rendererOptions: {
                        numberRows: 5
                    },
                    location:'s',
                    marginTop: '15px',
                    marginLeft:'0px'
                      }

        }
      );

}

  function showBar(){
	    $.jqplot.config.enablePlugins = true;
	    var s1 = [1,2,3,4,5];


	var ticks = ['Mon', 'Tue', 'Wed', 'Thu','Fri'];

	    plot1 = $.jqplot('chart1', [s1], {
	        // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
	       // animate: !$.jqplot.use_excanvas,
	        seriesDefaults:{
	            renderer:$.jqplot.BarRenderer,
	            pointLabels: { show: true }
	        },
	        axes: {
	            xaxis: {
	                renderer: $.jqplot.CategoryAxisRenderer,
	                ticks: ticks
	            }
	        },
	        highlighter: { show: false }
	    });

	    $('#chart1').bind('jqplotDataClick', 
	        function (ev, seriesIndex, pointIndex, data) {
	            $('#info1').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
	        }
	    );
	}


    function destroy(){
        alert("test 1" );
        $.jqplot.replot();
    }
</script>

<link rel="stylesheet" type="text/css" href="../../../css/jquery.jqplot.min.css" />
<!-- <script type="text/javascript"  src="js/jquery-1.7.1.min.js"></script>

<script type="text/javascript" src="../src/plugins/jqplot.barRenderer.min.js"></script>
 <script type="text/javascript" src="../src/plugins/jqplot.categoryAxisRenderer.min.js"></script>
  <script type="text/javascript" src="../src/plugins/jqplot.pointLabels.min.js"></script>

<script type="text/javascript" src="../src/plugins/jqplot.dateAxisRenderer.min.js"></script> 
<script type="text/javascript" src="../src/plugins/jqplot.canvasTextRenderer.min.js"></script> 
 <script type="text/javascript" src="../../../js/jquery/graph/jqplot.canvasAxisTickRenderer.min.js"></script> 
-->




<script type="text/javascript" src="../../../js/jquery/jquery-1.6.3.js"></script>
<script  type="text/javascript" src="../../../js/jquery/graph/excanvas.js" ></script>
<script  type="text/javascript" src="../../../js/jquery/graph/jquery.jqplot.min.js" ></script>
<!-- Pie chart -->
<script  type="text/javascript" src="../../../js/jquery/graph/jqplot.pieRenderer.min.js" ></script>



  <script class="include" type="text/javascript" src="../../../js/jquery/graph/jqplot.barRenderer.min.js"></script>
  
  <script  type="text/javascript" src="../../../js/jquery/graph/jqplot.categoryAxisRenderer.min.js"></script>
  <script  type="text/javascript" src="../../../js/jquery/graph/jqplot.pointLabels.js"></script>

</html>
