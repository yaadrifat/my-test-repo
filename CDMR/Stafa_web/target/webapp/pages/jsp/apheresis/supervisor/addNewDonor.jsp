<%@ taglib prefix="s" uri="/struts-tags"%>
<%-- <%@ include file="common/includes.jsp" %> --%>
<script type="text/javascript" src="js/donor.js"></script>
<script type="text/javascript" src="js/barcodeScanBox.js"></script>
<script>
function savePopup(){
	try{
		var donorMrnLength=$("#donorMRN").val().length;
		var recipientMrnLength=$("#recipientMRN").val().length;
		if( donorMrnLength <=10 && recipientMrnLength <=10){
				if(saveDonor()){
					return true;
					}
			}else{
				alert("Please Enter the MRN with Ten Digits");
				if(donorMrnLength>10){
					$("#donorMRN").val("");
					}
				if(recipientMrnLength>10){
						$("#recipientMRN").val("");
						}
				return false;
				}
	}catch(e){
		alert("exception " + e);
	}
}
function saveDonor(){
	//alert("save donor");
		if(autoCheck()){
			var url="saveDonor";
			var formName = "donorNewForm";
			//alert("form " + $("#"+formName).html());
			response = ajaxCall(url,formName);
			//alert("response " + response.status);
				if(response.status !=""){
				alert("Data Saved Successfully");
				closePopup();
				constructDonorList(true);
				return true;
				}
		}
}
function closePopup(){
	try{
		
		$("#donorNewForm").reset();
		$("#modalpopup").dialog('close');
    	//jQuery("#addNewDonor").dialog("destroy");
    	
		//?BB stafaPopUp("close");
		
  	}catch(err){
		alert("err" + err);
	}
}
$(document).ready(function(){
	try{
	$(".ssnClass").each(function(i){
		applyMask(this,"ssn","_");
	});
	}catch(err){
		alert("err " + err);
	}
	  $("select").uniform();
	var param ="{module:'APHERESIS',page:'ADD_DONOR'}";
	
	markValidation(param);

});
$(function() {
	$("#plannedCollectionDate").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
        changeYear: true, yearRange: "c-50:c+45"});
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true, changeYear: true, yearRange: '1900:' + new Date().getFullYear(), maxDate: "+1m +1w"});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" );
});
</script>



<div id="addNewDonor" class="model" >
	<form action="#" id="donorNewForm"><br>
    <input type="hidden" id="scanType" name="scanType" value=""/>
    <span id="hiddendropdowns" class="hidden">
    	<s:select id="gender"  name="gender" list="lstGender" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" />
       	<s:select id="language"  name="language" list="lstLanguage" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/>
    </span>  		
       	<input type="hidden" id="donorGender" name="donorGender"/>
       	<input type="hidden" id="donorPrilang" name="donorPrilang"/>
       
      	<input type="hidden" id="recipientGender" name="recipientGender"/>
       	<input type="hidden" id="recipientPrilang" name="recipientPrilang"/>
            
            <table border="0" width="100%" >
             <tr> <td style="font-weight:bold">Procedure</td><td colspan="3">&nbsp;<input type="hidden" name="tmpdonorType" id="tmpdonorType" value="0" /><input type="hidden" name="donorPerson" id="donorPerson" value="0" /> <input type="hidden" name="recipientPerson" id="recipientPerson" value="0" /></td></tr>
 			  <tr>
                    <td class="tablewapper"><s:text name="stafa.label.donortype"/></td>
                    <td class="tablewapper1">
                    	<s:select id="donorType"
                    	  name="donorType" list="lstDonorType" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="javascript:showRecipientTable(this,'apheresis');"/>
                    	
                      </td>
                       <td class="tablewapper"><s:text name="stafa.label.plannedprocedure"/></td>
                    <td class="tablewapper1"><label></label>
                    	<s:select id="plannedProcedure"  name="plannedProcedure" list="lstPlannedProcedure" listKey="protocol" listValue="protocolName"   headerKey="" headerValue="Select" class="mandatory"/>
                    	
                      </td>
            	</tr>
            	 <tr>
                    <td class="tablewapper"><s:text name="stafa.label.producttype"/></td>
                    <td class="tablewapper1">
                    	<s:select id="productType"  name="productType" list="lstProductType" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" class='mandatory'/>
                      </td>
                       <td class="tablewapper"><s:text name="stafa.label.plannedcollectiondate"/></td>
                       <td class="tablewapper1">	<input type="text" id="plannedCollectionDate" name="plannedCollectionDate" class="dateEntry calDate" /></td>
            	</tr>
            	<tr>
            	    <td colspan='4'>  <div id="button_wrapper">
        		<div id="divDonorScan" style="margin-left:40%;">
       				 <input class="scanboxSearch  multiple donorscan"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Donor ID"  style="width:210px;" onkeyup="setScanFlag('DONOR');performScanData(event,this);" />
					<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Donor ID"  style="width:210px;display:none;" onfocus="changeType(this)"/>
	 		</div>
        </div></td>
         
            	</tr>
              
                <tr> 
                    <td class="tablewapper"><s:text name="stafa.label.donorid"/></td>
                    <td class="tablewapper1"><input type="text" id="donorMRN" name="donorMRN"/></td>
                   <td class="tablewapper"><s:text name="stafa.label.donorabo" /></td>
                    <td class="tablewapper1">
                    	<s:select id="donorabo"  name="donorabo" list="lstABORH" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeABO('donor');"/>
                  	</td>
                 
                  
               </tr>
                <tr>
                  <td class="tablewapper"><s:text name="stafa.label.donorssn"/></td>
                	<td class="tablewapper1"><input class="ssnClass" type="text" id="donorSSN" name="donorSSN"/></td>
                 <td class="tablewapper"><s:text name="stafa.label.donorabs"/></td>
                    <td class="tablewapper1">
                    	<s:select id="donorAbs"  name="donorAbs" list="lstABS" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeABS('donor')"/></td>
                 
                   <%--  --%>
               </tr>
               <tr>
                <td class="tablewapper"><s:text name="stafa.label.donor_LastName"/></td>
                    <td class="tablewapper1"><input type="text" id="donorLastName" name="donorLastName"/></td>
                	<td class="tablewapper"><s:text name="stafa.label.Donor_Weight"/></td>
                    <td class="tablewapper1"><input type="text" id="donorWeight" name="donorWeight"style="width:55px;" class='decimalTextBox'/>&nbsp;<span class='smallSelector '><s:select id="donorUnitsWeight"  name="donorUnitsWeight" list="lstUnitWeight" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeUnit('donor');"/></span></td>
                	
                </tr>
               <tr>
              		<td class="tablewapper"><s:text name="stafa.label.donor_FirstName"/></td>
                    <td class="tablewapper1"><input type="text" id="donorFirstName" name="donorFirstName"/></td>
                     <td class="tablewapper"><s:text name="stafa.label.Donor_Height"/></td>
                     <td class="tablewapper1"><input type="text" id="donorHeight" name="donorHeight"style="width:55px;" class='decimalTextBox'/>&nbsp;<span class='smallSelector '><s:select id="donorUnitsHeight"  name="donorUnitsHeight" list="lstUnitHeight" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></span></td>  
                </tr>  
                
                <tr >
                 <td class="tablewapper"><s:text name="stafa.label.donor_MiddleInitial"/></td>
                     <td class="tablewapper1"><input type="text" id="donorMiddleName" name="donorMiddleName"/></td>
                    <td class="tablewapper"><s:text name="stafa.label.donorPhysician"/></td>
                    <td class="tablewapper1">
                    	<s:select id="physicianID"  name="physicianID" list="lstDonorPhysician" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/>
                   </td>
                </tr>
                <tr >
                      <td class="tablewapper"><s:text name="stafa.label.donordob"/></td>
                    <td class="tablewapper1"><input type="text" id="donorDob" name="donorDob" class="dateEntry calDate" /></td>
                    <td class="tablewapper">&nbsp;</td>
                   <td class="tablewapper">&nbsp;</td>
                </tr> 
                <!-- 
                 <tr >
                    <td align="center" colspan="2"><input type="button" value="Search" onclick="javascript:searchPerson('DONOR');"/></td>
                </tr>
                 -->
                 <tr class="donorResultsTR hidden" ><td colspan="4" width="100%">
                 <div  class="portlet">
               		 <div class="portlet-header "><label><b>Potential Donors</b></label>&nbsp;</div>
                <div class="portlet-content">
                  	<table id="donorResults" class="display " width="100%"><thead>
						<tr>
							<th width="4%" id="">&nbsp;</th>
							<th>Donor Name</th>
							<th>Donor dob</th>
							<th>Donor MRN</th>
							<th>SSN</th>
							<th>Abo/rh</th>
							<th>language</th>
							<th>Source</th>
						</tr></thead>
					<tbody>
					</tbody></table>
					</div>
					</div>
					</td>
					
				 </tr>
               <!-- 
                <tr class="donortr hidden">
               
                <td class="tablewapper"><s:text name="stafa.label.status"/></td>
                    <td class="tablewapper1">
                    	<s:select id="status"  name="status" list="lstStatus" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/>
                      </td>
   			</tr>
           -->
          
            <tr><td colspan="4">
            <table border="0"  width="100%"  id="recipientTable">
	            <tr>
		            <td colspan="4"> 
			            <div id="button_wrapper">
			        		<div id="divRecipientScan" style="margin-left:40%;">
			       				 <input class="scanboxSearch  multiple recipientscan"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Recipient ID"  style="width:210px;" onkeyup="setScanFlag('RECIPIENT');performScanData(event,this)" />
								<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Recipient ID"  style="width:210px;display:none;" onfocus="changeType(this)" />
					 		</div>
				        </div>
				     </td>	
			    </tr>
               
                <tr>
                 	<td class="tablewapper"><s:text name="stafa.label.recipientmrn" /></td>
                    <td class="tablewapper1"><input type="text" id="recipientMRN" name="recipientMRN" onblur="autoCheck()"/></td>
                      <td class="tablewapper"><s:text name="stafa.label.recipientabo" /></td>
                    <td class="tablewapper1">
                    	<s:select id="recipientabo"  name="recipientabo" list="lstABORH" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"  onchange="changeABO('recipient');"/>
                    </td>
                      
               </tr>
               <tr>
              		 <td class="tablewapper"><s:text name="stafa.label.recipientssn" /></td>
                	<td class="tablewapper1"><input class="ssnClass" type="text" id="recipientSSN" name="recipientSSN"/></td>
                	 <td class="tablewapper"><s:text name="stafa.label.recipientabs"/></td>
                    <td class="tablewapper1">
                    	<s:select id="recipientAbs"  name="recipientAbs" list="lstABS" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"  onchange="changeABS('recipient')"/></td>
              		 <%-- 
                   --%>
                </tr>
    			 <tr><td class="tablewapper"><s:text name="stafa.label.recipient_LastName"/></td>
                    <td class="tablewapper1"><input type="text" id="recipientLastName" name="recipientLastName"/></td>
                    <td class="tablewapper"><s:text name="stafa.label.recipientdiagnosis"/></td>
                    <td class="tablewapper1">
                     	<s:select id="donordiagnosis"  name="donordiagnosis" list="lstDiagonosis" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/>
                    </td>
                	<%-- 
                	 --%>
                </tr>
                 <tr>
                   <td class="tablewapper"><s:text name="stafa.label.recipient_FirstName"/></td>
                   <td class="tablewapper1"><input type="text" id="recipientFirstName" name="recipientFirstName"/></td>
                   <td class="tablewapper"><s:text name="stafa.label.recipient_weight"/></td>
                   <td class="tablewapper1"><input type="text" id="recipientWeight" name="recipientWeight"style="width:55px;" class='decimalTextBox'/>&nbsp;<span class='smallSelector '><s:select id="recipientUnitsWeight"  name="recipientUnitsWeight" list="lstUnitWeight" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeUnit('recipient');" /></span></td>
                 </tr>
                 
                 
                 <tr>
                 <td class="tablewapper"><s:text name="stafa.label.recipient_MiddleInitial"/></td>
                     <td class="tablewapper1"><input type="text" id="recipientMiddleName" name="recipientMiddleName"/></td>
                     <td class="tablewapper"><s:text name="stafa.label.transplant_Physician"/></td>
                    <td class="tablewapper1">
                          	<s:select id="transplantPhysician"  name="transplantPhysician" list="lstTransplantPhysician" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/>
              		</td>
                   <!-- <input type="button" value="Search" onclick="javascript:searchPerson('RECIPIENT');"/></td>
                    -->
                </tr> 
               <tr >
                   <td class="tablewapper"><s:text name="stafa.label.recipientdob"/></td>
                    <td class="tablewapper1"><input type="text" id="recipientDob" name="recipientDob" class="dateEntry calDate"/></td>
                    <td class="tablewapper">&nbsp;</td>
                   <td class="tablewapper">&nbsp;</td>
                </tr>                 
                 <tr class="recipientResultTr hidden" ><td colspan="4" width="100%">
                  <div  class="portlet">
               		 <div class="portlet-header "><label><b>Potential Recipients</b></label>&nbsp;</div>
                	<div class="portlet-content">
                 <table id="recipientResults" class="display " width="100%" ><thead>
						<tr>
							<th width="4%" id="">&nbsp;</th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th>Source</th>
						</tr>
						
					</thead>
					<tbody>
											
					</tbody></table>
					</div>
					</div>
					</td></tr>
           </table>
            </tr>
            <tr>
            <td align="right" colspan="4"><input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
				<input type="button" value="Save" onclick="verifyeSign('eSignNext','popup')"/>
				<input type="button" value="Cancel" onclick="javascript:closePopup()"/>
            <!-- <td align="right" colspan="4"><input type="button" value="Save" onclick="javascript:saveDonor();"/>&nbsp;<input type="button" name="cancel" onclick="javascript:closePopup();" value="Cancel"/> -->
            </td>
            </tr>
            </table>
               
            </form>
</div>