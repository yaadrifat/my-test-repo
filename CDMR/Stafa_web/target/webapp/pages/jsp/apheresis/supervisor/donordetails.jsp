<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/placeholder.js"></script>
<script type="text/javascript" src="js/ajaxfileupload.js"></script>
<script type="text/javascript" src="js/donor.js"></script>
<style type="text/css">
.firstRow{
background-color: #A4A4A4;
}
</style>
<script>


function converHDate1(dateStr){
	var returnValue ="";
//alert("converHDate1" + dateStr)
 try{
	if(dateStr!= null && dateStr != ""){	
		var tmpDate =dateStr.split("-");
		var tmpDate1 = new Date(tmpDate[0] ,tmpDate[1]-1, tmpDate[2].substring(0,2));
		
		returnValue = $.datepicker.formatDate('M dd, yy', tmpDate1);
	 }
  }catch(err){
	  alert("error "+ err);
  }
	// alert("returnValue " +returnValue);
	return returnValue;
}
var fieldArray = ["wbc","cd34","cd34per","hct","plt","kPlus"  ];

var testCodeArray1 = ["WBC","CD3KG","CD3%","HCT","PLT","K"  ];
var testCodeArray2 = ["WBC","CD34PE","CD34P%","HCT","PLT","K"  ];


var productCodeArray1 = ["WBCSP","CD3PLUSSP","CD34P%","HCTSP","PLTSP","K"  ];
var productCodeArray2 = ["WBCSP","CD34PLUSSP","CD34P%","HCTSP","PLTSP","K"  ];
// mncper =  LY% + MO%
var testCodeArray;

function getLabData(obj,type){
	//alert("getLabData " + $(obj).val());
	
	var jsonData = "{protocol:"+$(obj).val()+",type:'"+type+ "',donor:"+ $("#donor").val()+"}";
	var url ="loadReferenceNumbers";
	var params = "PATIENT_ID:\""+$("#donorMRN").val()+"\" AND  ";
	var refNumbers="";
	//0529:WM:C00003R" OR "0529:WM:C00002R""
	var orFlag = false;
	response = jsonDataCall(url,"jsonData="+jsonData);
	 $(response.lstRefNumbers).each(function (){
		 var tmpArray= this;
		 if(!orFlag){
		 	//?BB remove test code mapping refNumbers = "(SPECIMEN_NUMBER:\""+tmpArray[0] +"\" AND TEST_CUSTOM_CODE:\""+ tmpArray[1].replace("per","%25").toUpperCase() + "\")";
		 	refNumbers = "(SPECIMEN_NUMBER:\""+tmpArray[0] + "\")";
		 	orFlag = true;
		 }else{
			//?BB remove test code mapping  refNumbers = refNumbers+" OR (SPECIMEN_NUMBER:\""+ tmpArray[0] +"\" AND TEST_CUSTOM_CODE:\""+ tmpArray[1].replace("per","%25").toUpperCase() + "\")";;
			 refNumbers = refNumbers+" OR (SPECIMEN_NUMBER:\""+ tmpArray[0] + "\")";;
		 }
	 });
	 
	 var rowHtml =  $(obj).closest("tr");
	 
	 if(type=="PRODUCT"){
		// litterProcessed,machineVolume,scaleVoulume
		 $(response.lstProductLab).each(function (){
		 		var tmpArray= this;
		 		$(rowHtml).find("#litterProcessed").val(tmpArray[0] );
		       	$(rowHtml).find("#litterProcessed").closest("td").append(tmpArray[0]);
		       	
		       	$(rowHtml).find("#machineVolume").val(tmpArray[1] );
		       	$(rowHtml).find("#machineVolume").closest("td").append(tmpArray[1]);
		 
				$(rowHtml).find("#scaleVoulume").val(tmpArray[2] );
		       	$(rowHtml).find("#scaleVoulume").closest("td").append(tmpArray[2]);
	
		 });
	 }
	 if(orFlag){
	//var mrn='';
		var searchUrl= TENSEURL +"/TenseADTServer/patient_lab/select?q=*:*&wt=json&json.wrf=?&rows=1000&fq="+ params +"("+refNumbers +")" ;
		
		
		//alert("t1");
		var plannedProceduredesc = $("#plannedProceduredesc").val();
			var checkTC = plannedProceduredesc.slice(-2);
			if(type=="PRODUCT"){
				if(checkTC == "TC"){
					testCodeArray = productCodeArray1;
				}else{
					testCodeArray = productCodeArray2;
				}
			}else{
			 	if(checkTC == "TC"){
					testCodeArray = testCodeArray1;
				}else{
					testCodeArray = testCodeArray2;
				} 
			}
			//alert("t2" +testCodeArray);	
		$.ajax({
			  url: searchUrl,
			  dataType: 'json',
			  success: function(data){
				  //testCode= $(this).find("#testCode").val();
				  var result = data.response.docs;
				  if(result.length ==0){
					  alert("No Data found");
				  }
				  rowHtml =  $(obj).closest("tr");
		       	  for(var i=0;i<testCodeArray.length;i++){
		       		 var testCode = testCodeArray[i];
		       		// alert("testCode " + testCode);
		      
		       	 		$(result).each(function (counter,val){
		       	 			//alert(this.TEST_CUSTOM_CODE + " inside loop testCode " + testCode );
		       	 			if(testCode== this.TEST_CUSTOM_CODE){
			       	 			var result = this.TEST_RESULT;
					       		var units = this.TEST_UNITS;
					       		//alert("Values " + fieldArray[i]+ "/"+testCode + " / " + result + " / " + units);
					       		
					       		$(rowHtml).find("#"+fieldArray[i]).val(result + " " +units );
					       		$(rowHtml).find("#"+fieldArray[i]).closest("td").append( result + " " +units);
					       		// ?BB $(rowHtml).find("#"+fieldArray[i]).closest("td").text( result + " " +units);
		       	 			}
		       	 			
		       	 		});
		       	  }
		       	  
		       	var mncperValue=0;
				  var mncperValueUnit="";
		if(type=="PRODUCT"){	  
		       	$(result).each(function (counter,val){
		       		if(this.TEST_CUSTOM_CODE == 'MONSP' || this.TEST_CUSTOM_CODE == 'LYMPHSP' ){
	       	 			// mncper =  LY% + MO% 
	       	 				var result = eval(this.TEST_RESULT);
	       	 			  // alert( this.TEST_CUSTOM_CODE +"/ " + result);
	       	 				mncperValue = mncperValue + result;
	       	 				mncperValueUnit=this.TEST_UNITS;
		       	 			
		       	 		}
		       	});
		       	var rowHtml =  $(obj).closest("tr");  
		       	$(rowHtml).find("#mncper").val(mncperValue + " " +mncperValueUnit);
	       		 $(rowHtml).find("#mncper").closest("td").append( mncperValue + " " +mncperValueUnit);
	       		//?BB $(rowHtml).find("#mncper").closest("td").text( mncperValue + " " +mncperValueUnit);
		}else{
			$(result).each(function (counter,val){
	       		if(this.TEST_CUSTOM_CODE == 'MO%' || this.TEST_CUSTOM_CODE == 'LY%' ){
       	 			// mncper =  LY% + MO% 
       	 				var result = eval(this.TEST_RESULT);
       	 			  // alert( this.TEST_CUSTOM_CODE +"/ " + result);
       	 				mncperValue = mncperValue + result;
       	 				mncperValueUnit=this.TEST_UNITS;
	       	 			
	       	 		}
	       	});
	       	var rowHtml =  $(obj).closest("tr");  
	       	$(rowHtml).find("#mncper").val(mncperValue + " " +mncperValueUnit);
       		 $(rowHtml).find("#mncper").closest("td").append( mncperValue + " " +mncperValueUnit);
       		//?BB $(rowHtml).find("#mncper").closest("td").text( mncperValue + " " +mncperValueUnit);
			
			
		}  		
		       		
			  }
		});
	 }else{
		 alert("Specimen number(s) not configured");
	 }
}


function constructADTScreeningTable(flag,tableId){
	//alert("constructADTScreeningTable 1");
	//alert("construct table : "+tableId+",Donor : "+donor);
	var link;
	var donor = $("#donor").val();
	var criteria ="";
	if(donor!=null && donor!=""){
		//?BB lab result common for all plannedprocedure 
			//?BBcriteria = " and donorId ="+donor+"and donorProcedure ="+donorProcedure;
		criteria = " and donor ="+donor;
	}
	var adtScreeningServerParams = function ( aoData ) {
														aoData.push( { "name": "application", "value": "stafa"});
														aoData.push( { "name": "module", "value": "aphresis_adtScreenResults"});
														aoData.push( { "name": "criteria", "value": criteria});
														aoData.push( {"name": "col_1_name", "value": "lower(panelName)" } );
														aoData.push( { "name": "col_1_column", "value": "lower(panelName)"});
														
														aoData.push( { "name": "col_2_name", "value": "lower(testName)"});
														aoData.push( { "name": "col_2_column", "value": "lower(testName)"});
																	
														aoData.push( {"name": "col_3_name", "value": "lower(refNumber)" } );
														aoData.push( { "name": "col_3_column", "value": "lower(refNumber)"});
														
														aoData.push( {"name": "col_4_name", "value": "lower(testDate)" } );
														aoData.push( { "name": "col_4_column", "value": "lower(testDate)"});
														
														aoData.push( {"name": "col_5_name", "value": "lower(results)" } );
														aoData.push( { "name": "col_5_column", "value": "lower(results)"});
														
														/* aoData.push( {"name": "col_6_name", "value": "lower(unit)" } );
														aoData.push( { "name": "col_6_column", "value": "lower(unit)"}); */
											};
	var adtScreeningaoColDef = [
			                  {		
			                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
		                	 			return "<input type='hidden' id='adtScreening' value='"+source.adtScreening+"'/><input type='hidden' name='donor' id='donor' value='"+source.donor+"'/>";
		                	     }},
			                  {"sTitle": "Panel",
		                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) { 
		                	    		 link= source.panelName;
		                	    		 if(source.resultFrom !="ADT"){
		                	    			 if(link == null){
		                	    				 link ="";
		                	    			 }
		                	    			link = "<span id='panelSpan'><input type='text' name='panelName' id='panelName' value='"+link+"'/></span>";
		                	    		 }
			                	 		return link;
			                	 }},
							  {"sTitle": "Test Name",
			                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
			                			 link= source.testName+"<input type='hidden' id='testCode' name='testCode' value='"+source.testCode+"'/>";
			                	 		return link;
									    }},
							  {"sTitle": "Ref #",
									 "aTargets": [3], "mDataProp": function ( source, type, val ) {
										 link= source.refNumber;
		                	    		 if(source.resultFrom !="ADT"){
		                	    			 if(link == null){
		                	    				 link ="";
		                	    			 }
		                	    			link = "<span id='refSpan'><input type='text' name='refNumber' id='refNumber' value='"+link+"'/></span>";
		                	    		 }
			                	 		return link;
							  		}},
							  {"sTitle": "Test Date",
							  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
							  		 	 link= source.testDate;
								  		 	if(link == null){
		                	    				 link ="";
		                	    			 }
								  		 	//alert("link " + link);
								  		 if(link !=""){
								  			link = converHDate1(link);
								  		 }
		                	    		 if(source.resultFrom !="ADT"){
		                	    			link = "<span id='testDateSpan'><input type='text' name='testDate' id='testDate' class='dateEntry calDate' value='"+link+"'/></span>";
		                	    		 } 
		                	    		 
			                	 		return link;
								  }},
							  {"sTitle": "Result",
									 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
										 link= source.results;
		                	    		 if(source.resultFrom !="ADT"){
		                	    			 if(link == null){
		                	    				 link ="";
		                	    			 }
		                	    			// alert("result"+source.results);
		                	    		//?BB	link = "<span id='resultSpan'><input type='text' name='results' id='results' value='"+link+"'/></span>";
		                	    			
		                	    			 var selectTxt = $("#idmResults").html();
		                	    			// alert("selectTxt " + selectTxt);
		                                	if(link !=""){
			                	    		 	// var searchTxt = 'value="'+link +'"';
			                	    		 	 var searchTxt = 'value='+link ;
			                                	// alert( selectTxt + " / " + searchTxt);
			                                	if(selectTxt.indexOf(searchTxt)!=-1){
			                                	 	selectTxt = selectTxt.replace(searchTxt,searchTxt +' selected') ;
			                                	}else{
			                                		searchTxt = 'value="'+link +'"';
			                                		selectTxt = selectTxt.replace(searchTxt,searchTxt +' selected') ;
			                                	}
		                                	}
		                                	//alert("t1");
		                                	  link = "<span id='resultSpan'><select id='results' name='results'>" + selectTxt + "</select></span>";
		                                	 
		                	    		 }
		                	    		// alert("link " + link);
			                	 		return link;
								  }}
							  /* {"sTitle": "Units",
									 "aTargets": [6], "mDataProp": function ( source, type, val ) { 
										 link= source.unit;
		                	    		 if(source.resultFrom !="ADT"){
		                	    			 if(link == null){
		                	    				 link ="";
		                	    			 }
		                	    			link = "<span id='unitSpan'><input type='text' name='unit' id='unit' value='"+link+"'/></span>";
		                	    		 }
			                	 		return link;
								  }} */
							]; 
	var adtScreeningColManager = function () {
											$("#donorScreenColumn").html($('#donorScreeningTable_wrapper .ColVis'));
											};
	var adtScreeningaoColumn = [ { "bSortable":false},
				               null,
				               null,
				               null,
				               null,
				               null
				              /*  null */
				             ];
	var adtScreeningDefault_sort = [[ 1, "asc" ]];
	//alert("constructADTScreeningTable 2");
	constructTable(flag,tableId,adtScreeningColManager,adtScreeningServerParams,adtScreeningaoColDef,adtScreeningaoColumn,adtScreeningDefault_sort);
	//alert("constructADTScreeningTable 3");
	addDatepicker(); 
	//alert("constructADTScreeningTable 4");
	return true;
}	

function save_adtScreeningResults(){
	//alert("inside save productLabResults ");
	try{
		var adtScreening,panelName,testName,testCode,refNumber,testDate,results;/* unit; */
		var resultFrom;
		var rowData = "";
		var colObj;
	 	var donor = $("#donor").val();
	 	$("#donorScreeningTable tbody tr").each(function(row) {
	 		adtScreening="";
	 		panelName="";
	 		testName="";
	 		testCode="";
	 		refNumber="";
	 		testDate="";
	 		results="";
	 		//unit="";
	 		resultFrom="";
	 		if($(this).find("#refNumber").val() == undefined){
	 			resultFrom = "ADT";
	 		}else{
	 			resultFrom = "MANUAL";
	 		}
	 		adtScreening = $(this).find("#adtScreening").val();
	 		testCode = $(this).find("#testCode").val();
	 		$(this).find("td").each(function (col){
		 		if(col ==1 ){
		 			if(resultFrom == "ADT"){
		 				panelName = $(this).text();
		 			}else{
		 				panelName = $(this).find("#panelName").val();
		 			}
		 		 }else if(col ==2){
		 			testName = $(this).text();
		 		 }else if(col ==3){
		 			if(resultFrom == "ADT"){
		 				refNumber = $(this).text();
		 			}else{
		 				refNumber = $(this).find("#refNumber").val();
		 			}
		 		 }else if(col ==4){
			 			if(resultFrom == "ADT"){
			 				testDate = $(this).text();
			 			}else{
			 				//testDate = $(this).find("#testDate").val();
			 				testDate =$(this).find("input[name=testDate]").val();
			 			}
			 	}else if(col ==5){
			 			if(resultFrom == "ADT"){
			 				results = $(this).text();
			 			}else{
			 				results = $(this).find("#results").val();
			 			}
			 	}/* else if(col ==6){
			 		if(resultFrom == "ADT"){	
			 			unit = $(this).text();
		 			}else{
		 				unit = $(this).find("#unit").val();
		 			}
	 		 	}  */
	 		});
	 		if(testDate =="" || testDate == undefined){
	 			//alert("inside if")
	 			testDate ='';
	 		}else{
	 			testDate = "',testDate:'" + testDate;
	 		}
	 		rowData += "{donor:'"+donor
						+"',resultFrom:'"+ resultFrom
						+"',adtScreening:'"+adtScreening
						+"',panelName:'"+panelName
						+"',testName:'"+testName
						+"',testCode:'"+testCode
						+"',refNumber:'"+refNumber
						+testDate
						+"',results:'"+results+"'},";
						//+"',unit:'"+unit
	 		
	 		//alert("rowData::"+rowData);
	 		
	 	});
	 	if(rowData.length >0){
 			rowData = rowData.substring(0,(rowData.length-1));
 			var url="saveADTScreeningResult";
 		    response = jsonDataCall(url,"jsonData={adtResults:["+rowData+"]}");
 		   // alert("response::"+response);
 		}
	}catch(err){alert("error" + err);}
	return true;
}
function donorScreenImport(tablename){
	//alert(tablename);
	mrnimport=true;
	/* $("#dnrsrnpopup").dialog({
		autoOpen: false,
		modal: true,
		title: 'Donor Screen Import',
		width:1100,
		height:500,
		close: function(){
			jQuery("#dnrsrnpopup").dialog("destroy");
		}
	    
		}); */
	/* $(".ui-dialog").each(function(){
		$(this).remove();
	});	 */
	url="loadTenseADT.action";
	//alert("mrnimport"+mrnimport);
	$.ajax({
        type: "POST",
        url:  url,
        async:false,
        success:function (result){
        	
        	$("#dnrsrnpopup").html(result).dialog({
				   autoOpen: true,
				   title:'ADT Lab',
				   modal: true, width:1100, height:500,
				   close: function() {
					   jQuery("#dnrsrnpopup").dialog("destroy");
					 /*  $(this).remove(); // by uma
					  $("#modeldialog").append('<div id="dnrsrnpopup"></div>');*/

					  
				   }
				});
        	$('#importdonor').die('click');
        	$('#importdonor').live('click', function(e){
        		dataImport(tablename);
        	 });
        	/*
        	jQuery("#dnrsrnpopup").dialog("destroy");
        	$("#dnrsrnpopup").html(result);
        	$('#importdonor').die('click');
        	$('#importdonor').live('click', function(e){
        		dataImport(tablename);
        	 });
        	//alert("tablename"+tablename);
        	$("#dnrsrnpopup").dialog({
				autoOpen: true,
				modal: true,
				title: 'Donor Screen Import',
				width:1100,
				height:500,
				close: function(){
					jQuery("#dnrsrnpopup").dialog("destroy");
				}
			    
				});
        	*/
        	//$("#dnrsrnpopup").dialog("open");
        }
    });
	
}

function checkDataExists(tableId){
	//alert("tableId::"+tableId);
		var currTabClass= $('#'+tableId+' tbody tr td:nth-child(1)').attr("class");
		//alert("currTabClass::"+currTabClass);
		
		if( currTabClass=="dataTables_empty"){
			addDefaultTests(tableId);
		
		}
		//alert("end of check data ")
}

function addDefaultTests(tableId){
	//alert("tableId::"+tableId); 
	var testNameArray = [ "Chagas", "HBSAG",   "HBCAB",   "Hep C",    "HCVAB C",   "RPR",   "HIV-1/2","HTLV 1/2", "HIV/HCV/HBV","WESNIL"];
	var testCodeArray = ["DONCHAGT","DONHBSAGT","DONHBCAB","DONHCVABT","DONHCVABC"  ,"DONRPRT","DONHIVT","DONHTLV12","DONHIVHCVNT","DONWESTNIL"];
	//var testNameArray = ["CREATININE","GFR NON AFRICAN AMERICAN","GFR AFRICAN AMERICAN","THEOPHYLLINE","HCVAB C","RPR","HIV-1/2","HTLV 1/2","HIV/HCV/HBV","WESNIL"];
	//var testCodeArray = ["CREATI","GFRNAA","GFRAA","THEO","hcvabc","rpr","hiv1/2","htlv1/2","hivhcv","wesnil"];
	$("#"+tableId).dataTable().fnDestroy();
	 $('#'+tableId).dataTable({
		    "sPaginationType": "full_numbers",
		    "aoColumns":[{"bSortable": false},
		                   null,
		                   null,
		                   null,
		                   null,
		                   null
		                   /* null */
		                ],
			"aaSorting":[[ 1, "asc" ]]
	 });
	// alert("t1");
	for(i=0;i<testNameArray.length;i++){
	//	alert(i + "Name Array::"+testNameArray[i]);
		 $("#"+tableId).dataTable().fnAddData(["<input type='hidden' id='adtScreening' name='adtScreening' value='0'/>","<span id='panelSpan'><input type='text' name='panelName' id='panelName' value=''/></span>",
		                                      "<input type='hidden' name='testCode' id='testCode' value='"+testCodeArray[i]+"'/>"+testNameArray[i],"<span id='refSpan' ><input type='text' name='refNumber' id='refNumber' value='' /></span>","<span id='testDateSpan'><input type='text' name='testDate' id='testDate' class='dateEntry calDate' value=''/></span>",
		                                      "<span id='resultSpan'><select id='results' name='results'>"+$("#idmResults").html()+"</select></span>"]);
		                                     /*  "<span id='unitSpan'><input type='text' name='unit' id='unit' value=''/></span>" */ 
				
	}
	// add default test W00005111
	//$("#"+tableId).dataTable().fnAddData(["","1","2","3","4","5","6"],["","1","2","3","4","5","7"]);
	
	//<td></td><td>IDM</td><td><input type='hidden' name='testCode' id='testCode' value='chages' /> Chagas </td>
//	alert("t end");
 	addDatepicker();  
}



function dataImport(tablename){
	//alert("dataImport " + tablename);
	try{
	 var importLen = $('#compDocTable tr').length;
	 if(importLen>1){
	 var cond="";
	 var docId = uniqFieldObj[0].childNodes[0].nodeValue;
	 $('#compDocTable .tableRowSelc').each(function(i,el){
		 if(cond !="" ){
			 cond += " OR ";
		 }
		 cond=cond+docId+':"'+el.id+'"';
	 });
	 var startUrl = ipvalue+urlObj[0].childNodes[0].nodeValue;
	 startUrl+="?rows="+importLen+"&wt=json&q=*:*&json.wrf=?&fq="+cond;
	 var panelName="";
	 var panelCode="";
	 var testName="";
	 var referNum="";
	 var reportDate="";
	 var testResult="";
	 var units="";
  var mncperValue =0;
  var mncperValueUnit;
	   $.ajax({
	       url: startUrl,
	       dataType: 'json',
	       success: function(data) {
	    	   var testCode;
	    	   if(tablename=="donorScreeningTable"){
		       	   $("#donorScreeningTable tbody tr").each(function (row){
		       		  // alert("trace 1");
			       		testCode= $(this).find("#testCode").val();
			       	  // alert("trace 2 " +testCode);
			       		var dataArray = byKey($(data.response.docs),"TEST_CUSTOM_CODE",testCode);
			       		//alert("dataArray " + dataArray);
			       		if(dataArray != undefined){
			    			var tmpData = dataArray["LAB_ORDER_NUMBER"];
			    			//alert("value " + tmpData);
			    			if(tmpData != undefined){
			    				$(this).find("#refSpan").html(tmpData);
			    			}else{
			    				$(this).find("#refSpan").html("");
			    			}
			    			tmpData = dataArray["TEST_REPORT_DATE"];
			    			
			    			if(tmpData != undefined){
			    				//05-13-2013
			    				var tmpDate = tmpData.split("-");
			    				var tmpDate1 = new Date(tmpDate[2] ,tmpDate[0]-1, tmpDate[1]);
			    				var returnValue = $.datepicker.formatDate('M dd, yy', tmpDate1);
			    				//alert("returnValue : "+ returnValue); 
			    				$(this).find("#testDateSpan").html(returnValue);
			    			}else{
			    				$(this).find("#testDateSpan").html("");
			    			}
			    			tmpData = dataArray["TEST_RESULT"];
			    			if(tmpData != undefined){
			    				$(this).find("#resultSpan").html(tmpData);
			    			}else{
			    				$(this).find("#resultSpan").html("");
			    			}
			    			/* tmpData = dataArray["TEST_UNITS"];
			    			if(tmpData != undefined){
			    				$(this).find("#unitSpan").html(tmpData);
			    			}else{
			    				$(this).find("#unitSpan").html("");
			    			} */
			    			tmpData = dataArray["TEST_CATEGORY_NAME"];
			    			if(tmpData != undefined){
			    				$(this).find("#panelSpan").html(tmpData);
			    			}else{
			    				$(this).find("#panelSpan").html("");
			    			}
			    		}
			          
			       });
	    	   }else if(tablename=="donorLabResultTable"){
	    		  // alert(" donorLabResultTable");
	    		   var plannedProceduredesc = $("#plannedProceduredesc").val();
		   			var checkTC = plannedProceduredesc.slice(-2);
		   			var rowHtml =  $(".firstRow").closest("tr");
		   			if(checkTC == "TC"){
		   				testCodeArray = testCodeArray1;
		   			}else{
		   				testCodeArray = testCodeArray2;
		   			}
		   			for(var i=0;i<testCodeArray.length;i++){
			       		 var testCode = testCodeArray[i];
			       		var dataArray = byKey($(data.response.docs),"TEST_CUSTOM_CODE",testCode);
			       	//	alert("testCode " + testCode + "/dataArray " + dataArray);
			       		if(dataArray != undefined){
			    			var tmpData = dataArray["TEST_RESULT"] + " " + dataArray["TEST_UNITS"] ;
			    		 //	alert("testCode " + testCode + " / " +tmpData );
			    		 	$(rowHtml).find("#"+fieldArray[i]).val(tmpData );
				       		//$(rowHtml).find("#"+fieldArray[i]).closest("td").append( result + " " +units)
		       	 			
			       		}
		   			}
		   			
	       	 			// mncper =  LY% + MO% 
	       	 			var dataArray = byKey($(data.response.docs),"TEST_CUSTOM_CODE","LY%");
	       	 		if(dataArray != undefined){
	       	 			mncperValue = mncperValue + eval(dataArray["TEST_RESULT"]);
	       	 			mncperValueUnit=dataArray["TEST_UNITS"];
	       	 		}
	       	 			var dataArray1 = byKey($(data.response.docs),"TEST_CUSTOM_CODE","MO%");
	       	 		if(dataArray1 != undefined){
	       	 			mncperValue = mncperValue + eval(dataArray1["TEST_RESULT"]);
	       	 			mncperValueUnit=dataArray1["TEST_UNITS"];
	       	 		}	
	       	 				
				       	$(rowHtml).find("#mncper").val(mncperValue + " " +mncperValueUnit);
			       		//$(rowHtml).find("#mncper").closest("td").append( mncperValue + " " +mncperValueUnit);
		   			
	    		   
	    	   }
	    	   jQuery("#dnrsrnpopup").dialog("destroy");
	    	  /*  $("#dnrsrnpopup").remove(); //? comment by uma 
				$("#modeldialog").append('<div id="dnrsrnpopup"></div>');*/
	       }
	   });
	}else{
		alert("Select a row to import");
	}
	}catch(e1){
		alert("error " + e1);
	}
}

	 function byKey(arr, key,value) {
		 try{
			// alert( key + "/"+value + "/ " + arr);
		  for ( var i=0; i<arr.length; i++ ) {
			 // alert("arr[i] " + arr[i] + " / " + arr[i][key]);
		    if ( arr[i][key] === value ) {
		      return arr[i];
		    }
		  }
		 }catch(err){
			 alert("error " + err);
		 }
		}






//--------


function changeDonorType(){
	var donorType= $("#donorType option:selected").text(); 
	//alert("donorType"+donorType);
	if(donorType=="Auto" || donorType=="AUTO"){
		$("#tmpdonorType").val("1");
	}else{
		$("#tmpdonorType").val("0");
	}
	autoCheck();
}
function savePages(mode){
	if(checkModuleRights(STAFA_DONORDETAILS,APPMODE_SAVE)){
		var donorMrnLength=$("#donorMRN").val().length;
		var recipientMrnLength=$("#recipientMRN").val().length;
		try{
			if( donorMrnLength <=10 && recipientMrnLength <=10){
					if(autoCheck()){
							if(saveDonorDetails()){
								//alert("saveDonorDetails");
							}
							
							if(save_donorLabResults()){
							//	alert("save_donorLabResults");
							}
							if(save_productLabResults()){
								//alert("save_productLabResults");
							}
							if(save_documentReview()){
								//alert('Data saved Successfully','');
							} 
							
							if(save_adtScreeningResults()){
								//alert(save_adtScreeningResults);
							} 
							//Fetch Donor details
							//alert("!111");
							//if( mode!="next"){
								$('.progress-indicator').css( 'display', 'block' );
								var donor=$("#donor").val();
								var donorProcedure=$("#donorProcedure").val();
								loadAction('loadDonorDetails.action?donor='+donor+"&donorProcedure="+donorProcedure);
								//response = ajaxCall("loadDonorDetails","donorDetailForm");
								//alert("2222");
								//$('#main').html(response);
						
					
							return true;
						}
					}else{
						alert("Please Enter the MRN with Ten Digits");
						if(donorMrnLength>10){
							$("#donorMRN").val("");
							}
						if(recipientMrnLength>10){
								$("#recipientMRN").val("");
						}
							return false;
								
						}
			}catch(e){
			alert("exception " + e);
		}
	}
}
function save_documentReview(){
	//alert("inside save_documentReview");
	//
	try{
	var documentData = "";
	var donor = $("#donor").val();
	var entityType = $("#entityType").val();
	 $("#documentreviewTable tbody tr").each(function (row){
	 	var docsTitle="";
	 	var docsDate="";
	 	var fileData="";
	 	var fileUploadContentType="";
	 	var reviewedStatus="";
	 	var documentFileName="";
	 	var documentId="";
	 	if($(this).find(".filePathId").val()!=undefined){
		$(this).find("td").each(function (col){
			if(col ==0){
				documentId=$(this).find("#documentRef").val();
			}
			if(col ==1){
				docsTitle = $(this).find("#documentTitle").val();
			}
			if(col ==2){
				docsDate = $(this).find(".dateEntry").val();
			}
			if(col ==3){
				fileData = $(this).find(".filePathId").val();
				//alert("fileData : "+fileData);
				fileData=fileData.replace(/\\/g,"/");
				documentFileName = $(this).find(".fileName").val();
				
				fileUploadContentType = $(this).find(".fileUploadContentType").val();
			}
		});
		if(fileData != undefined && fileData != ""){
			documentData+="{pkDocumentId:'"+documentId+"',fkEntity:'"+donor+"',entityType:'"+entityType+"',fkCodelstDocumentType:'"+docsTitle+"',documentFileName:'"+documentFileName+"',documentUploadedDate:'"+docsDate+"',documentFile:'"+fileData+"',fileUploadContentType:'"+fileUploadContentType+"',reviewedStatus:'"+reviewedStatus+"'},";
		}
	 	}
	 });
	 	//alert("documentData " + documentData);
		if(documentData.length >0){
			documentData = documentData.substring(0,(documentData.length-1));
			var url="saveMultipleDocument";
			response = jsonDataCall(url,"jsonData={documentData:["+documentData+"]}");
			//alert("Data Saved Successfully");
			//stafaAlert('Data saved Successfully','');
			$('.progress-indicator').css( 'display', 'none' );
			//constructDocumentTable(true,donorVal,enType,'documentreviewTable');
			//constructDocumentHistoryTable(true,donorVal,enType,'documentreviewTable1');
			//alert("end of save_documentReview");
		}
	}catch(e){
		alert("Error : "+e);
		}
	 return true;
}


 var attachFileId=0;
 var dateId=0;
 function add_documentReview(){
	 if(checkModuleRights(STAFA_DOCUMENT,APPMODE_ADD)){
		 var docsTitle = $("#docsTitleTemp").html();
		 //alert(docsTitle);
	     var newRow ="<tr>"+
	     "<td><input type='hidden' id='documentRef' value='0' name='documentRef'/></td>"+
	     "<td><div><select id='documentTitle' name='documentTitle' style='width:200px;'>"+docsTitle+"</select></div></td>"+
	     "<td><input type='text' id='documentDate"+dateId+"' name='documentDate' class='dateEntry' /></td>"+
	   	 "<td><img src='images/file_loading.gif' id='fileUploadImage"+attachFileId+"' style='display:none'/>"+
	   	 "<div id='fileDisplayName"+attachFileId+"'></div><input type='hidden' class='fileName' id='fileName"+attachFileId+"' name='fileName"+attachFileId+"'/><input type='hidden' class='filePathId' name='filePathId"+attachFileId+"' id='filePathId"+attachFileId+"'/> <input type='hidden' class='fileUploadContentType' name='fileUploadContentType"+attachFileId+"' id='fileUploadContentType"+attachFileId+"'/>"+
	   	 "<div class='"+attachFileId+"' style='cursor: pointer;	color: #0101DF;' id='displayAttach"+attachFileId+"'></div><input size='10' style='display:block' class='"+attachFileId+"' type='file' name='fileUpload' onChange='ajaxFileUpload1(this,\""+attachFileId+"\")' id='attach"+attachFileId+"'/><span><s:text name='stafa.fileupload.message'/></span></td>"+
	     //"<td><input type='file' name='fileUpload' id='attachedFile"+attachFileId+"'/></td>"+
	     "<td></td>"+
	     "</tr>";
	     //attachFileId=attachFileId+1;
	     $("#documentreviewTable tbody").prepend(newRow);
	     //?mohan
	    /*  $("#documentDate"+dateId).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	         changeYear: true, yearRange: "c-50:c+45"}); */
	         $("#documentDate"+dateId).datepicker({dateFormat: 'M dd, yy',changeMonth: true, changeYear: true, yearRange: '1900:' + new Date().getFullYear()});
	     dateId=dateId+1;
	     $.uniform.restore('select');
		 $('select').uniform();
		 /*$("#displayAttach"+attachFileId).click(function(){
			 var idSeq = $(this).attr("class");
			 $("#displayAttach"+attachFileId).hide();
			 $("#attach"+idSeq).click();
		});
		*/
		 attachFileId=attachFileId+1;
		 var param ="{module:'APHERESIS',page:'DONOR_DETAILS'}";
			markValidation(param);
	 }
 }


 function delete_documentReview(){
	 if(checkModuleRights(STAFA_DOCUMENT,APPMODE_DELETE)){  	
		 var deletedIds="";
			try{
			$("#documentreviewTable tbody tr").each(function (row){
				 if($(this).find("#docid").is(":checked")){
					deletedIds += $(this).find("#docid").val() + ",";
				}
			 });
			//	alert("deletedIds "+ deletedIds);
			  if(deletedIds.length >0){
				 var yes=confirm(confirm_deleteMsg);
				 if(!yes){
								return;
				 }
				  deletedIds = deletedIds.substr(0,deletedIds.length-1);
				  //alert("deletedIds " + deletedIds);
				  jsonData = "{deletedIds:'"+deletedIds+"',domainName:'DOMAIN_DOCUMENT',domainKey:'DOCUMENT_ID'}";
				  url="commonDelete";
			     
			     response = jsonDataCall(url,"jsonData="+jsonData);
			 	var donor = $("#donor").val();
				var entityType = $("#entityType").val();
				constructDocumentTable(true,donor,entityType,'documentreviewTable');
			    
			 	
			  }
			}catch(err){
				alert(" error " + err);
			}
	 }
 }
 
 
function constructtab(){
	/*alert("constructtab");

	$("#attachedFile #tabs").tabs();
	$("#attachedFile tabs ul").removeClass("ui-widget-header");
	*/
}



function getScanData(event,obj){
	//alert("scan data");
  	if(event.keyCode==13){
        getDonorData();
    }
}

function getDonorData(){
	//alert("get donordata");
	var donorProcedure = $("#donorProcedure").val();
	var url ="fetchDonorData";
	var jsonData = "donorId:"+$("#conformProductSearch").val();
		response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	var donorDetails = response.donorDetails;
	//alert("cmv" + donorDetails.cmv);

	$("#person").val(donorDetails.personId);
	$("#donor").val(donorDetails.donorId);
	$("#donorScreening").val(donorDetails.donorScreening);
	$("#collectionOrder").val(donorDetails.collectionOrder);
	$("#mobilizationDetail").val(donorDetails.mobilizationDetail);
	$("#mobilization").val(donorDetails.mobilization);


	$("#altId").val(donorDetails.personMRN);
	$("#donorLastName").val(donorDetails.lastName);
	$("#donorFirstName").val(donorDetails.firstName);
	$("#donorDob").val(donorDetails.dob);
	$("#gender").val(donorDetails.gender);
	$("#donorMRN").val(donorDetails.personMRN);
	$("#donorWeight").val(donorDetails.personWeight);
	$("#height").val(donorDetails.personHeight);
	$("#abo").val(donorDetails.fkCodelstBloodgrp);
	
	

	$("#physician").val(donorDetails.physician);
	$("#language").val(donorDetails.language);
	$("#donorType").val(donorDetails.donorType);
	
	$("#translateneed").val(donorDetails.isTranslaortneed);
	if(donorDetails.isTranslaortneed ==1){
		$("#isTranslaortYes").attr('checked', 'checked');
	}else{
		$("#isTranslaortNo").attr('checked', 'checked');
	}
	
	/*//?BB
	$("#h1vtype1").val(donorDetails.h1vtype1);
	$("#htclVirus1").val(donorDetails.htclVirus1);
	$("#h1vtype2").val(donorDetails.h1vtype2);
	$("#htclVirus2").val(donorDetails.htclVirus2);
	$("#hepatitisB").val(donorDetails.hepatitisB);
	$("#westnileVirus").val(donorDetails.westnileVirus);
	$("#hepatitisC").val(donorDetails.hepatitisC);
	$("#typonosoma").val(donorDetails.typonosoma);
	$("#treponemapallidum").val(donorDetails.treponemapallidum);
	$("#cmv").val(donorDetails.cmv);
	*/
		// Collection Order
	$("#collectionOrderIssue").val(donorDetails.collectionOrderIssue);
	$("#targetCD34Counts").val(donorDetails.targetCD34Counts);
	$("#targetProcessingVolume").val(donorDetails.targetProcessingVolume);
	
	// mobilization 
	$("#mobilizationOrderIssuedby").val(donorDetails.mobilizationOrderIssuedby);
	$("#mobilizationNurse").val(donorDetails.mobilizationNurse);

	$("#mobilizationDate").val(donorDetails.mobilizationDate);

	$("#method").val(donorDetails.method);
	$("#mobilizationTime").val(donorDetails.mobilizationTime);
	
	 $.uniform.restore('select');
	 $('select').uniform();
	 donorVal = $("#donor").val();
	 
	 enType=$("#entityType").val();
	 constructDonorLabTable(true,donorVal,'donorLabResultTable',donorProcedure);
	 constructProductTable(true,donorVal,'productLabResultTable');
	 constructDocumentTable(true,donorVal,enType,'documentreviewTable');
	 constructDocumentHistoryTable(true,donorVal,enType,'documentreviewTable1');
}

function constructDocumentTable(flag,donor,type,tableId) {
	//alert("construct table : "+tableId+",Donor : "+donor);
		var criteria = "";
		if(donor!=null && donor!=""){
			criteria = donor ;
		}
		var documentColumn = [ { "bSortable": false},
			                	null,
			                	null,
			                	null,
			                	null,
			                	null
				             ];
													
		var documentServerParams = function ( aoData ) {
										aoData.push( { "name": "application", "value": "stafa"});
										aoData.push( { "name": "module", "value": "aphresis_DocumentReview"});
										//aoData.push( { "name": "criteria", "value": criteria});
										
										aoData.push( { "name": "param_count", "value":"1"});
										aoData.push( { "name": "param_1_name", "value":":DONOR:"});
										aoData.push( { "name": "param_1_value", "value":criteria});
										
										aoData.push( {"name": "col_1_name", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_DOCUMENTTYPE))" } );
										aoData.push( { "name": "col_1_column", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_DOCUMENTTYPE))"});
										
										aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
										aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
													
										
					};
		var documentaoColDef = [
					                  {		
					                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
				                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
				                	 			return "<input type='hidden' id='documentRef' name='documentRef' value='"+source[0] +"' ><input type='checkbox' id='docid' name='docid' value='"+source[0] +"' >";
				                	 			//return "";
				                	     }},
					                  { "sTitle": "Document Title",
				                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
					                	    	 //alert('docTitle'+source.documentFileName); 
					                	 		return source[6];
					                	 }},
									  { "sTitle": "Document Date",
					                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
											    return source[2];
											    }},
									  { "sTitle": "Attachment",
											 "aTargets": [3], "mDataProp": function ( source, type, val ) {
										 	   return source[1] + " &nbsp;&nbsp;<a href='#' onclick='openPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\")'> View </a>";
									  		}},
									  { "sTitle": "Reviewed",
									  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
									  			 if(source[3] ==1){
									  				 return "Reviewed";
									  			 }else{
									  				 return "";
									  			 }
										  		//return "<input type='checkbox'/>";
										  }},
									  { "sTitle": "Version #",
					                		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
											    return source[7];
											    }}
									];
		var documentColManager = function () {
												$("#documentreviewColumn").html($("#"+tableId+"_wrapper .ColVis"));
											};
											
		constructTable(flag,tableId,documentColManager,documentServerParams,documentaoColDef,documentColumn);
	} 
	
function constructDocumentHistoryTable(flag,donor,type,tableId) {
	 	//alert("construct table : "+tableId+",Donor : "+donor);
		var criteria = "";
		if(donor!=null && donor!=""){
			criteria = " and fk_Entity ="+donor ;
		}
		var docHistoryaoColumn =  [ { "bSortable": false},
					                	null,
					                	null,
					                	null,
					                	null,
					                	null
						            ];
		
		var docHistoryServerParams = function ( aoData ) {
										aoData.push( { "name": "application", "value": "stafa"});
										aoData.push( { "name": "module", "value": "aphresis_DocumentReview_History"});
										aoData.push( { "name": "criteria", "value": criteria});
										
										aoData.push( {"name": "col_1_name", "value": "lower(DOCUMENT_FILENAME)" } );
										aoData.push( { "name": "col_1_column", "value": "lower(DOCUMENT_FILENAME)"});
										
										aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
										aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
													
										
					};
					
		var	docHistoryaoColDef = [
					                  {		
					                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
				                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
				                	 			return "<input type='hidden' id='documentRef' name='documentRef' value='"+source[0] +"' > ";
				                	 			//return "";
				                	     }},
					                  { "sTitle": "Document Title",
				                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
					                	    	 //alert('docTitle'+source.documentFileName); 
					                	 		return source[6];
					                	 }},
									  { "sTitle": "Document Date",
					                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
											    return source[2];
											    }},
									  { "sTitle": "Attachment",
											 "aTargets": [3], "mDataProp": function ( source, type, val ) {
										 	   return source[1] + " &nbsp;&nbsp;<a href='#' onclick='openPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\")'> View </a>";
									  		}},
									  { "sTitle": "Reviewed",
									  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
									  			 if(source[3] ==1){
									  				 return "Reviewed";
									  			 }else{
									  				 return "";
									  			 }
										  		//return "<input type='checkbox'/>";
										  }},
									  { "sTitle": "Version #",
					                		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
											    return source[7];
											    }}
									];
		
		var docHistoryColManager =  function () {
												$("#documentreviewColumn1").html($("#"+tableId+"_wrapper .ColVis"));
									};
									
		constructTable(flag,tableId,docHistoryColManager,docHistoryServerParams,docHistoryaoColDef,docHistoryaoColumn);

						
	} 

function saveDonorDetails(){
	//alert("inside donordetails");
	
	// person 
	var personId = $("#person").val();
	var donor = $("#donor").val();
	var donorProcedure=$("#donorProcedure").val();
	var donorScreening =$("#donorScreening").val();
	var collectionOrder=$("#collectionOrder").val();
	var mobilizationDetail = $("#mobilizationDetail").val();
	var mobilization=$("#mobilization").val();
	var personData = "{pkPerson:"+personId+",personCode:'"+$("#altId").val() +"',";
	personData += "personaltId:'"+$("#altId").val()+ "',";
	personData +="personLname:'"+ $("#donorLastName").val()+ "',";
	personData +="personFname:'"+ $("#donorFirstName").val()+ "',";
	personData +="personMname:'"+ $("#donorMiddleName").val()+ "',";
	personData +="personDob:'" + $("#donorDob").val()+"',";
	personData +="fkCodelstGender:'" + $("#gender").val()+"',";
	personData +="personSsn:'" + $("#donorSSN").val()+ "',";
	personData +="personMRN:'" + $("#donorMRN").val()+"',";
	
	personData +="personWeight:'" + $("#donorWeight").val()+"',";
	personData +="personHeight:'" + $("#height").val()+"',";
	personData +="fkCodelstAbs:'" + $("#donorAbs").val()+"',";
	personData +="fkCodelstWeightUnits:'"+ $("#donorUnitsWeight").val()+ "',";
	personData +="fkCodelstHeightUnits:'"+ $("#donorUnitsHeight").val()+ "',";
	personData +="fkCodelstBloodgrp:'" + $("#donorabo").val()+"'}";
	
	//alert("person data " + personData);
	
	//alert("recipientData");
	var recipientId=$("#recipient").val();
	var recipientData="{pkPerson:"+recipientId+",";
	recipientData +="personMRN:'"+ $("#recipientMRN").val()+ "',";
	recipientData +="personSsn:'"+ $("#recipientSSN").val()+ "',";
	recipientData +="personLname:'"+ $("#recipientLastName").val()+ "',";
	recipientData +="personFname:'"+ $("#recipientFirstName").val()+ "',";
	recipientData +="personMname:'"+ $("#recipientMiddleName").val()+ "',";
	recipientData +="personWeight:'"+ $("#recipientWeight").val()+ "',";
	//recipientData +="personDiagnosis:'"+ $("#diagnosis").val()+ "',";
	recipientData +="personDob:'"+ $("#recipientDob	").val()+ "',";
	recipientData +="fkCodelstGender:'" + $("#recipientGender").val()+"',";
	recipientData +="fkCodelstAbs:'"+ $("#recipientAbs	").val()+ "',";
	recipientData +="fkCodelstWeightUnits:'"+ $("#recipientUnitsWeight").val()+ "',";
	recipientData +="fkCodelstBloodgrp:'" +$("#recipientabo").val()+"'}";
	
	
	
	// donor 
	var donorData ="{donorId:"+donor+",personId:"+personId+",";
	donorData +="physicianID:'" + $("#physician").val()+"',";
	donorData +="language:'" + $("#language").val()+"',";
	donorData +="donorType:'" + $("#donorType").val()+"',";
	donorData +="donordiagnosis:'" + $("#donordiagnosis").val()+"',";
	donorData +="productType:'" + $("#productType").val()+"',";
	donorData +="transplantPhysician:'" + $("#transplantPhysician").val()+"',"
	donorData +="donorWeightTaken:'" + $("#donorWeightTaken").val()+"',"
	donorData +="donorEligibility:'" + $("#donorEligibility").val()+"',"
	if($("#plannedCollectionDate").val() != '' && $("#plannedCollectionDate").val() != undefined){
		donorData +="plannedCollectionDate:'" + $("#plannedCollectionDate").val()+"',"
	}
	donorData +="recipient:'" + $("#recipient").val()+"',";
	
	var tmpValue =0;
	if($("#isTranslaortYes").is(":checked")){
		tmpValue=1;
	}
	//alert("tmpValue" +tmpValue);
	donorData +="isTranslatorNeed:'" + tmpValue+"'}";
	
	//alert("donorData " + donorData)
	//Screening Results
	var screeningData = "{h1vtype1:'" +$("#h1vtype1").val()+"',";
	screeningData += "htclVirus1:'" +$("#htclVirus1").val()+"',";
	screeningData += "h1vtype2:'" +$("#h1vtype2").val()+"',";
	screeningData += "htclVirus2:'" +$("#htclVirus2").val()+"',";
	screeningData += "hepatitisB:'" +$("#hepatitisB").val()+"',";
	screeningData += "westnileVirus:'" +$("#westnileVirus").val()+"',";
	screeningData += "hepatitisC:'" +$("#hepatitisC").val()+"',";
	screeningData += "typonosoma:'" +$("#typonosoma").val()+"',";
	screeningData += "treponemapallidum:'" +$("#treponemapallidum").val()+"',";
	screeningData += "cmv:'" +$("#cmv").val()+"',";
	screeningData +="donorId:'"+donor+"',";
	if($("#DateIdmDrawn").val() != "" && $("#DateIdmDrawn").val() !=undefined){
		screeningData +="dateIdmDrawn:'"+$("#DateIdmDrawn").val()+"',";
	}
	screeningData +="donorScreening:'"+donorScreening+"'}"; 
	//alert("screeningData " + screeningData);
	// Collection Order
	var collectionOrderData = "{issuedBy :'" +$("#collectionOrderIssue").val()+"',";
	collectionOrderData += "targetCD34Counts :'" +$("#targetCD34Counts").val()+"',";
	collectionOrderData += "targetProcessingVolume :'" +$("#targetProcessingVolume").val()+"',";
	collectionOrderData += "entityId:'"+donorProcedure+"',";
	collectionOrderData += "fkCodelstTargetcd34Units:'"+$("#targetCD34Unit").val()+"',";
	
	collectionOrderData += "entityType:'PLANNEDPROCEDURE',collectionOrder:'"+collectionOrder+"'}";
	//alert("collectionOrderData" + collectionOrderData);
	//Mobilization Information
	
	var mobilizationData = "{issuedBy :'" +$("#mobilizationOrderIssuedby").val()+"',";
	mobilizationData += "nurse :'" +$("#mobilizationNurse").val()+"',";
	var mob_Date=$("#mobilizationDate").val();
	if($.trim(mob_Date)!=""){
		mobilizationData += "mobilizationDate:'" +$("#mobilizationDate").val()+"',";
	}
	mobilizationData += "method :'" +$("#method").val()+"',";
	var mob_Time=$("#mobilizationTime").val();
	if($.trim(mob_Time)!=""){
		mobilizationData += "mobilizationTime :'" +$("#mobilizationTime").val()+"',";
	}
	mobilizationData += "mobilization:'"+mobilization+"',";
	mobilizationData += "donor:'"+donor+"',";
	mobilizationData += "plannedProcedure:'"+donorProcedure+"'}";
	
	//?BB pending multiple
	
	var mobilizationDetail = "";
	var deLim = "";
	$("#mobilizationrow tr").each(function (r){
		mobilizationDetail+=deLim+"{mobilizationDetail:'"+$(this).find("#mobilizationDetail").val()+"',mobilizerName :'" +$(this).find("#mobilizerName").val()+"',mobilizerDose :'"+$(this).find("#mobilizerDose").val()+"',mobilization:'"+mobilization+"'}";
		deLim=",";
	});
		
	var url="saveDonorDetails";
	//?BBvar jsonData = "personData:"+personData+",recipientData:"+recipientData+",donorData:"+donorData+",screeningData:"+screeningData+",collectionOrderData:"+collectionOrderData+",mobilizationData:"+mobilizationData+",mobilizationDetail:["+mobilizationDetail+"]";
	var jsonData = "personData:"+personData+",recipientData:"+recipientData+",donorData:"+donorData+",collectionOrderData:"+collectionOrderData+",mobilizationData:"+mobilizationData+",mobilizationDetail:["+mobilizationDetail+"]";

	response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	
	//alert("end save_donorDetails");
	stafaAlert('Data saved Successfully','');
	//var donor=$("#donor").val();
	//var donorProcedure=$("#donorProcedure").val()
	return true;
}


//?BB to do datatable load, reload, edit rows and delete test 
function add_donorLabResults(){
	if(checkModuleRights(STAFA_DONORDETAILS,APPMODE_ADD)){
		var runningTabClass= $('#donorLabResultTable tbody tr td:nth-child(1)').attr("class");
		var collectionDayStr = $("#procedure").html();
		var newRow = "<tr>"+
		"<td style='width:65px'>&nbsp;</td>"+
		"<td style='width:217px'><select id='collectionDay' name='collectionDay' onchange='getLabData(this,\"DONOR\");'>" + collectionDayStr +"</select></td>"+
		"<td style='width:217px'><input type='hidden' id='wbc' name='wbc'/></td>"+
		"<td style='width:217px'><input type='hidden' id='cd34' name='cd34' /></td>"+
		"<td style='width:217px'><input type='hidden' id='cd34per' name='cd34per' /></td>"+
		"<td style='width:217px'><input type='hidden' id='hct' name='hct' /></td>"+
		"<td style='width:217px'><input type='hidden' id='plt' name='plt' /></td>"+
		"<td style='width:217px'><input type='hidden' id='mncper' name='mncper' /></td>"+
		"<td style='width:217px'><input type='hidden' id='kPlus' name='kPlus' /></td>"+
		"</tr>";
		if(runningTabClass=="dataTables_empty"){
			$("#donorLabResultTable tbody").replaceWith(newRow);
		}else{
			$("#donorLabResultTable tbody").prepend(newRow);
		}
		$.uniform.restore('select');
		 $('select').uniform();
	}
} 
function delete_donorLabResults(){
	//alert("Delete Rows");
	if(checkModuleRights(STAFA_DONORDETAILS,APPMODE_DELETE)){
		  var donorProcedure = $("#donorProcedure").val();
		  var deletedIds = ""
		  $("#donorLabResultTable").find("#donorLabResult:checked").each(function (row){
				//alert($(this).val());
				deletedIds += $(this).val() + ",";
			 });
		 if(deletedIds.length >0){
			  deletedIds = deletedIds.substr(0,deletedIds.length-1);
			  var yes=confirm(confirm_deleteMsg);
				if(!yes){
					return;
				}
		 
		//  alert("deletedIds " + deletedIds);
		  jsonData = "{deletedIds:'"+deletedIds+"'}";
		  url="deleteLabResult";
	        //alert("json data " + jsonData);
	     response = jsonDataCall(url,"jsonData="+jsonData);
	     constructDonorLabTable(true,donorVal,'donorLabResultTable',donorProcedure);
		 }
	}
}
function edit_donorLabResults(){
		//alert("edit Rows");
		if(checkModuleRights(STAFA_DONORDETAILS,APPMODE_EDIT)){
			var donorProtocol;
			  $("#donorLabResultTable tbody tr").each(function (row){
				 if($(this).find("#donorLabResult").is(":checked")){
					 donorProtocol=$(this).find("#donorProtocol").val();
					 $(this).find("td").each(function (col){
						 if(col ==1){
							 if ($(this).find("#collectionDay").val()== undefined){
								//$(this).html("<input type='text' id='collectionDay' name='collectionDay' value='"+$(this).text()+ "' />");
					 			//var oldVal=$(this).text();	
								var tmpHtml= "<select id='collectionDay' name='collectionDay' onchange='getLabData(this,\"DONOR\");' >" +  $("#procedure").html() +"</select>";
								$(this).html(tmpHtml);
								$(this).find("#collectionDay").val(donorProtocol);
								donorProtocol="";
								$.uniform.restore('select');
								 $('select').uniform();
							 }
						 }else if(col ==2){
							 if ($(this).find("#wbc").val()== undefined){
							 	$(this).html("<input type='text' id='wbc' name='wbc' value='"+$(this).text()+ "' />");
							 }
						 }else if(col ==3){
							 if ($(this).find("#cd34").val()== undefined){
							 	$(this).html("<input type='text' id='cd34' name='cd34' value='"+$(this).text()+ "' />");
							 }
						 }else if(col ==4){
							 if ($(this).find("#cd34per").val()== undefined){
								 	$(this).html("<input type='text' id='cd34per' name='cd34per' value='"+$(this).text()+ "' />");
								 }
						 }else if(col ==5){
							 if ($(this).find("#hct").val()== undefined){
								 	$(this).html("<input type='text' id='hct' name='hct' value='"+$(this).text()+ "' />");
								 }
						 }else if(col ==6){
							 if ($(this).find("#plt").val()== undefined){
								 	$(this).html("<input type='text' id='plt' name='plt' value='"+$(this).text()+ "' />");
								 }
						 }else if(col ==7){
							 if ($(this).find("#mncper").val()== undefined){
								 	$(this).html("<input type='text' id='mncper' name='mncper' value='"+$(this).text()+ "' />");
								 }
						 }else if(col ==8){
							 if ($(this).find("#kPlus").val()== undefined){
								 	$(this).html("<input type='text' id='kPlus' name='kPlus' value='"+$(this).text()+ "' />");
								 }
						 }
						 
					 });
				 }
			  });
		}
}
function save_donorLabResults(){
	//alert("inside save lab results");
	try{
	 var rowData = "";
	 var donorProcedure = $("#donorProcedure").val();
	 var donorLabResult,collectionDay,wbc,cd34,cd34per,hct,plt,mncper;
	 //$('.progress-indicator').css( 'display', 'block' );
	 var donor = $("#donor").val();
	 var blankDataFlag = false;
	 $("#donorLabResultTable tbody tr").each(function (row){
		 donorLabResult =0;
		 collectionDay="";
		 wbc="";
		 cd34="";
		 cd34per="";
		 hct="" ;
		 plt=mncper="";
		// alert("row" +row);
		 blankDataFlag = false;
		 if(row ==0){
			 if($(this).find("#wbc").val()!="" || $(this).find("#cd34").val() !="" || $(this).find("#cd34per").val()!="" ||$(this).find("#plt").val()!=""||
					 $(this).find("#mncper").val()!="" ||  $(this).find("#kPlus").val() != ""){
				// alert("inside if")
				 blankDataFlag = false;
			 }else{
				 blankDataFlag = true;
				// alert("inside else")
			 }
		 }
		// alert(blankDataFlag);
	if(!blankDataFlag){
	 	$(this).find("td").each(function (col){
	 		//alert(col + " / " + $(this).html() );
		 	   if(col ==0){
			 	  donorLabResult =$(this).find("#donorLabResult").val();
			 		if(donorLabResult == undefined){
			 			donorLabResult =0;
			 		}
			 	 }else if(col ==1){
			 		 if($(this).hasClass('firstRow')){
			 			collectionDay = $(this).text();
			 		 }else{
			 			collectionDay=$(this).find("#collectionDay") .find('option:selected').text();
			 		 }
			 		//alert("collectionDay" + collectionDay);
			 		
			 		protocol=$(this).find("#collectionDay").val();
			 	 }else if(col ==2){
			 		wbc=$(this).find("#wbc").val();
			 	 }else if(col ==3){
			 		cd34=$(this).find("#cd34").val();
			 	 }else if(col ==4){
			 		cd34per=$(this).find("#cd34per").val();
			 	 }else if(col ==5){
			 		hct=$(this).find("#hct").val();
			 	 }else if(col ==6){
			 		plt=$(this).find("#plt").val();
			 	 }else if(col ==7){
			 		mncper=$(this).find("#mncper").val();
			 	 }else if(col ==8){
			 		kPlus=$(this).find("#kPlus").val();
			 	 }
		  });
		
	 		if(collectionDay != undefined && collectionDay != ""){
		 	  rowData+= "{donorId:"+donor+",donorLabResult:"+donorLabResult+",collectionDay:'"+collectionDay+"',wbc:'"+wbc+"',cd34:'"+cd34+"',cd34per:'"+cd34per+"',htc:'"+hct+"',plt:'"+plt+"',mncper:'"+mncper+"',kPlus:'"+kPlus+"',donorProcedure:'"+donorProcedure+"',protocol:'"+protocol+"'},";
	 		}
	}
	 });
	//alert("rowData " + rowData);
	  if(rowData.length >0){
            rowData = rowData.substring(0,(rowData.length-1));
       var url="saveLabResult";
	     response = jsonDataCall(url,"jsonData={labResults:["+encodeURIComponent(rowData)+"]}");
	       //alert("Data Saved Successfully");
	      // stafaAlert('Data saved Successfully','');
	       $('.progress-indicator').css( 'display', 'none' );
	     //  constructDonorLabTable(true,donorVal,'donorLabResultTable',donorProcedure);
	  }
	  //alert("end save_dolab results");
	}catch(err){alert("error" + err);}
	return true;
}


function add_productLabResults(){
	if(checkModuleRights(STAFA_DONORDETAILS,APPMODE_ADD)){
		//alert("add product lab");
		var runningTabClass= $('#productLabResultTable tbody tr td:nth-child(1)').attr("class");
		var collectionDayStr = $("#procedure").html();
		var newRow ="<tr>"+
		"<td style='width:4%'>&nbsp;</td>"+
		"<td><select id='collectionDay' name='collectionDay' onchange='getLabData(this,\"PRODUCT\");'>" + collectionDayStr +"</select></td>"+
		"<td><input type='hidden' id='wbc' name='wbc' /></td>"+
		"<td><input type='hidden' id='hct' name='hct' /></td>"+
		"<td><input type='hidden' id='plt' name='plt' /></td>"+
		"<td><input type='hidden' id='mncper' name='mncper' /></td>"+
		"<td><input type='hidden' id='cd34' name='cd34' /></td>"+
		/* "<td><input type='text' id='cdyield' name='cdyield' /></td>"+ */
		"<td><input type='hidden' id='litterProcessed' name='litterProcessed' /></td>"+
		"<td><input type='hidden' id='machineVolume' name='machineVolume'/></td>"+
		"<td><input type='hidden' id='scaleVoulume' name='scaleVoulume'/></td>"+
		/* "<td><input type='text' id='noOfBag' name='noOfBag' /></td>"+ */
		"</tr>";
		if(runningTabClass=="dataTables_empty"){
			$("#productLabResultTable tbody").replaceWith(newRow);
		}else{
			$("#productLabResultTable tbody").prepend(newRow);
		}
		 $.uniform.restore('select');
		 $('select').uniform();
	}
}
//?check
function delete_productLabResults(){
	//  alert("Delete Rows");
	if(checkModuleRights(STAFA_DONORDETAILS,APPMODE_DELETE)){
		var donorProcedure = $("#donorProcedure").val();
	    var yes=confirm(confirm_deleteMsg);
		if(!yes){
			return;
		}
		  var deletedIds = ""
		  $("#productLabResultTable").find("#productLabResult:checked").each(function (row){
				//alert($(this).val());
				deletedIds += $(this).val() + ",";
			 });
		  if(deletedIds.length >0){
			  deletedIds = deletedIds.substr(0,deletedIds.length-1);
		  }
		//  alert("deletedIds " + deletedIds);
		  jsonData = "{deletedIds:'"+deletedIds+"'}";
		  url="deleteProductLabResult";
	        //alert("json data " + jsonData);
	     response = jsonDataCall(url,"jsonData="+jsonData);
		 constructProductTable(true,donorVal,'productLabResultTable',donorProcedure);
	}
}
function edit_productLabResults(){
	//alert("edit Rows");
	if(checkModuleRights(STAFA_DONORDETAILS,APPMODE_EDIT)){
		  $("#productLabResultTable tbody tr").each(function (row){
			 if($(this).find("#productLabResult").is(":checked")){
				 var productProtocol=$(this).find("#productProtocol").val()
				 $(this).find("td").each(function (col){
					 if(col ==1){
						 if ($(this).find("#collectionDay").val()== undefined){
							 //var oldVal=$(this).val();
								var tmpHtml= "<select id='collectionDay' name='collectionDay' onchange='getLabData(this,\"PRODUCT\");' >" +  $("#procedure").html() +"</select>";
								$(this).html(tmpHtml);
								$(this).find("#collectionDay").val(productProtocol);
								$.uniform.restore('select');
								 $('select').uniform();
							}
					 }else if(col ==2){
						 if ($(this).find("#wbc").val()== undefined){
						 	$(this).html("<input type='text' id='wbc' name='wbc' value='"+$(this).text()+ "' />");
						 }
					 }else if(col ==3){
						 if ($(this).find("#hct").val()== undefined){
						 	$(this).html("<input type='text' id='hct' name='hct' value='"+$(this).text()+ "' />");
						 }
					 }else if(col ==4){
						 if ($(this).find("#plt").val()== undefined){
							 	$(this).html("<input type='text' id='plt' name='plt' value='"+$(this).text()+ "' />");
							 }
					 }else if(col ==5){
						 if ($(this).find("#mncper").val()== undefined){
							 	$(this).html("<input type='text' id='mncper' name='mncper' value='"+$(this).text()+ "' />");
							 }
					 }else if(col ==6){
						 if ($(this).find("#cd34").val()== undefined){
							 	$(this).html("<input type='text' id='cd34' name='cd34' value='"+$(this).text()+ "' />");
							 }
					 }/* else if(col ==7){
						 if ($(this).find("#cdyield").val()== undefined){
							 	$(this).html("<input type='text' id='cdyield' name='cdyield' value='"+$(this).text()+ "' />");
							 }
					 } */else if(col ==7){
						 if ($(this).find("#litterProcessed").val()== undefined){
							 	$(this).html("<input type='text' id='litterProcessed' name='litterProcessed' value='"+$(this).text()+ "' />");
							 }
					 }else if(col ==8){
						 if ($(this).find("#machineVolume").val()== undefined){
							 	$(this).html("<input type='text' id='machineVolume' name='machineVolume' value='"+$(this).text()+ "' />");
							 }
					 }else if(col ==9){
						 if ($(this).find("#scaleVoulume").val()== undefined){
							 	$(this).html("<input type='text' id='scaleVoulume' name='scaleVoulume' value='"+$(this).text()+ "' />");
							 }
					 }/* else if(col ==11){
						 if ($(this).find("#noOfBag").val()== undefined){
							 	$(this).html("<input type='text' id='noOfBag' name='noOfBag' value='"+$(this).text()+ "' />");
							 }
					 } */
					 
				 });
			 }
		  });
	}
}
function save_productLabResults(){
	//alert("inside save productLabResults ");
	try{
	 var rowData = "";
	 var donorProcedure=$("#donorProcedure").val();
	 var productLabResult,collectionDay,wbc,hct,plt,mncper,cd34,cdyield,litterProcessed,machineVolume,scaleVoulume,noOfBag,protocol;
	 //?BB $('.progress-indicator').css( 'display', 'block' );
	 var donor = $("#donor").val();
	 $("#productLabResultTable tbody tr").each(function (row){
		 productLabResult =0;
		 collectionDay="";
		 wbc="";
		 cd34="";
		 cdyield="";
		 hct="" ;
		 plt=mncper=litterProcessed="";
	 	$(this).find("td").each(function (col){
	 		//alert(col + " / " + $(this).html() );
		 	   if(col ==0){
		 		  productLabResult =$(this).find("#productLabResult").val();
			 		if(productLabResult == undefined){
			 			productLabResult =0;
			 		}
			 	 }else if(col ==1){
			 		collectionDay=$(this).find("#collectionDay") .find('option:selected').text();
			 		protocol=$(this).find("#collectionDay").val();
			 	 }else if(col ==2){
			 		wbc=$(this).find("#wbc").val();
			 	 }else if(col ==3){
			 		hct=$(this).find("#hct").val();
			 	 }else if(col ==4){
			 		plt=$(this).find("#plt").val();
			 	 }else if(col ==5){
			 		mncper=$(this).find("#mncper").val();
			 	 }else if(col ==6){
			 		cd34=$(this).find("#cd34").val();
			 	 }/* else if(col ==7){
			 		cdyield=$(this).find("#cdyield").val();
			 	 } */else if(col ==7){
			 		litterProcessed=$(this).find("#litterProcessed").val();
			 	 }else if(col ==8){
			 		machineVolume=$(this).find("#machineVolume").val();
			 	 }else if(col ==9){
			 		scaleVoulume=$(this).find("#scaleVoulume").val();
			 	 }/* else if(col ==11){
			 		noOfBag=$(this).find("#noOfBag").val();
			 	 } */
		  });
	 		if(collectionDay != undefined && collectionDay != ""){
		 	  rowData+= "{donorId:"+donor+",productLabResult:"+productLabResult+",collectionDay:'"+collectionDay+"',wbc:'"+wbc+"',cd34:'"+cd34+"',hct:'"+hct+"',plt:'"+plt+"',mncper:'"+mncper+"',litterProcessed:'"+litterProcessed+"',machineVolume:'"+machineVolume+"',scaleVoulume:'"+scaleVoulume+"',donorProcedure:'"+donorProcedure+"',protocol:'"+protocol+"'},";
	 		}
	 });
	//alert("rowData " + rowData);
	  if(rowData.length >0){
            rowData = rowData.substring(0,(rowData.length-1));
       var url="saveProductLabResult";
	     response = jsonDataCall(url,"jsonData={labResults:["+encodeURIComponent(rowData)+"]}");
	       //alert("Data Saved Successfully");
	      // stafaAlert('Data saved Successfully','');
	       $('.progress-indicator').css( 'display', 'none' );
	   		//constructProductTable(true,donorVal,'productLabResultTable',donorProcedure);
	  }
	  //alert("end save productLabResults");
	}catch(err){alert("error" + err);}
	return true;
}
/*
function add_documentReview(){
    var newRow ="<tr>"+
    "<td><input type='checkbox' id='' name=''/></td>"+
    "<td><input type='text' id='docTitle' name='docTitle'/></td>"+
    "<td><input type='text' id='docDate' name='docDate' class='dateEntry'/></td>"+
    "<td><input type='file' id='attachedFile'/></td>"+
    "<td><input type='checkbox' id='reviewed' name='reviewed'/></td>"+
    "</tr>";
    $("#documentreviewTable tbody").prepend(newRow);
}
*/

function setHeaderValues(donorLabResult,collectionDayName,wbc,cd34,cd34per,htc,plt,mncper,kPlus){
	
	if(wbc == null){
		wbc="";
	}
	if(cd34==null){
		cd34="";
	}
	if(cd34per==null){
		cd34per="";
	}
	if(htc==null){
		htc="";
	}
	if(plt==null){
		plt="";
	}
	if(mncper==null){
		mncper="";
	}
	if(kPlus==null){
		kPlus="";
	}
	
	var headerRow ="<tr>";
	 //alert("cDay=="+this.collectionDay);
	 headerRow += "<td class='firstRow'><input type='hidden' id='donorLabResult' name='donorLabResult' value='"+donorLabResult +"' > <input id='donorProtocol' type='hidden' value='"+$("#preCollection").val()+"' name='donorProtocol' /></td>";
	//?BB hard code label name headerRow += "<td class='firstRow'><input type='hidden' id='collectionDay' name='collectionDay' value='"+$("#preCollection").val() + "'/>"+collectionDayName+ "<input type='button' value='Import' onclick='donorScreenImport(\"donorLabResultTable\");'/></td>";
	 headerRow += "<td class='firstRow'><input type='hidden' id='collectionDay' name='collectionDay' value='"+$("#preCollection").val() + "'/>Pre-Collection<input type='button' value='Import' onclick='donorScreenImport(\"donorLabResultTable\");'/></td>";
	 headerRow += "<td class='firstRow'><input type='text' id='wbc' name='wbc' value='"+ wbc + "'/></td>";
	 headerRow += "<td class='firstRow'><input type='text' id='cd34' name='cd34' value='"+ cd34 + "'/></td>";
	 headerRow += "<td class='firstRow'><input type='text' id='cd34per' name='cd34per' value='"+ cd34per + "'/></td>";
	 headerRow += "<td class='firstRow'><input type='text' id='hct' name='hct' value='"+ htc+ "'/></td>";
	 headerRow += "<td class='firstRow'><input type='text' id='plt' name='plt' value='"+ plt+ "'/></td>";
	 headerRow += "<td class='firstRow'><input type='text' id='mncper' name='mncper' value='"+ mncper + "'/></td>";
	 headerRow += "<td class='firstRow'><input type='text' id='kPlus' name='kPlus' value='"+ kPlus + "'/></td>";
	 headerRow +="</tr>";
	 //alert("headerRow::"+headerRow);
	 $("#donorLabResultTable > tbody > tr:eq(0)").before(headerRow);
}

var drawCallfn = function donorLabCallBack( ){
	 //alert("dataTableJson==> " + dataTableJson);
	 try{
	var hasHeaderData =false;
	 $(dataTableJson.headerData).each(function(i,v){
		 hasHeaderData =true;
		 setHeaderValues(this.donorLabResult, $("#preCollectionName").val(),this.wbc,this.cd34,this.cd34per, this.htc,this.plt,this.mncper, this.kPlus);
	 });
	 if(!hasHeaderData){
		 
		 setHeaderValues(0,$("#preCollectionName").val(),"","","","","","","");

	 }
	 //$("#donorLabResultTable > tbody > tr:eq(0)").before(headerRow);
	
	 }catch(err){
		 alert("error "+ err);
	 }
}
var drawCallProduct = function productLabCallBack(){
	//alert("drawCallProduct");
	var footerRow ="<tr>";
	 $(dataTableJson.footerData).each(function(i,v){
		// alert("i=="+i+"v== "+v+" Data::  " +v[i]);
		 footerRow += "<td colspan='2' class='firstRow'>Cumulative</td>";
		 for(k=0;k<v.length;k++){
			 //alert("i=="+i+"v== "+v+" Data::  " +v[i]);
			 if(v[k]==null){
				 v[k]=0;
			 }
			 footerRow += "<td class='firstRow'>"+ v[k] + "</td>";
			
		 }
		 footerRow += "</tr>";
	 });
	 
	$("#productLabResultTable > tbody > tr:last").after(footerRow);
}
/* 	$("#donorLabResultTable > tbody > tr:eq(0)").before("<tr>"+
															"<td><input type='checkbox'/></td>"+
															"<td>Apheresis</td>"+
															"<td>3</td>"+
															"<td>4</td>"+
															"<td>5</td>"+
															"<td>6</td>"+
															"<td>7</td>"+
															"<td>8</td>"+
															"<td>9</td>"+
														"</tr>");
}; */
function constructDonorLabTable(flag,donor,tableId,donoProcedure) {
	var criteria = "";
	if(donor!=null && donor!=""){
		//?BB lab result common for all plannedprocedure 
			//?BBcriteria = " and donorId ="+donor+"and donorProcedure ="+donorProcedure;
		criteria = " and donorId ="+donor;
	}
	var donorLabServerParams = function ( aoData ) {
												aoData.push( { "name": "application", "value": "stafa"});
												aoData.push( { "name": "module", "value": "aphresis_DonorlabResults"});
												aoData.push( { "name": "criteria", "value": criteria});
												
												aoData.push( {"name": "col_1_name", "value": "lower(collectionDay)" } );
												aoData.push( { "name": "col_1_column", "value": "lower(collectionDay)"});
												
												aoData.push( { "name": "col_2_name", "value": "lower(wbc)"});
												aoData.push( { "name": "col_2_column", "value": "lower(wbc)"});
															
												aoData.push( {"name": "col_3_name", "value": "lower(cd34)" } );
												aoData.push( { "name": "col_3_column", "value": "lower(cd34)"});
												
												aoData.push( {"name": "col_4_name", "value": "lower(cd34per)" } );
												aoData.push( { "name": "col_4_column", "value": "lower(cd34per)"});
												
												aoData.push( {"name": "col_5_name", "value": "lower(htc)" } );
												aoData.push( { "name": "col_5_column", "value": "lower(htc)"});
												
												aoData.push( {"name": "col_6_name", "value": "lower(plt)" } );
												aoData.push( { "name": "col_6_column", "value": "lower(plt)"});
												
												aoData.push( {"name": "col_7_name", "value": "lower(mncper)" } );
												aoData.push( { "name": "col_7_column", "value": "lower(mncper)"});
												
												aoData.push( {"name": "col_8_name", "value": "lower(kPlus)" } );
												aoData.push( { "name": "col_8_column", "value": "lower(kPlus)"});
														
							};
							
	var donorLabColDef =  [
				                  {		
				                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
			                	 			return "<input type='checkbox' id='donorLabResult' name='donorLabResult' value='"+source.donorLabResult +"' ><input type='hidden' id='donorProtocol' name='donorProtocol' value='"+source.protocol+"'>";
			                	     }},
				                  { "sTitle": "Collection Day",
			                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) { 
				                	 		return source.collectionDay;
				                	 }},
								  { "sTitle": "WBC",
				                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
										    return source.wbc;
										    }},
								  { "sTitle": ""+cd34_3,
										 "aTargets": [3], "mDataProp": function ( source, type, val ) {
									 	   return source.cd34;
								  		}},
								  { "sTitle": "%"+cd34_3,
								  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
									  		return source.cd34per;
									  }},
								  { "sTitle": "HCT",
										 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
									  		return source.htc;
									  }},
								  { "sTitle": "PLT",
										 "aTargets": [6], "mDataProp": function ( source, type, val ) { 
									  		return source.plt;
									  }},
								  { "sTitle": "%MNC",
										 "aTargets": [7], "mDataProp": function ( source, type, val ) { 
									  		return source.mncper;
									  }},
								  { "sTitle": "K+",
										 "aTargets": [8], "mDataProp": function ( source, type, val ) { 
									  		return source.kPlus;
									  }}
								];
	var donorColManager =  function () {
							$("#donorLabResultColumn").html($('#donorLabResultTable_wrapper .ColVis'));
		};
		var donorLabaoColumn = [ { "bSortable":false},
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null
				             ];
		var donorDefault_sort = [[ 1, "asc" ]];
		var drawBack = drawCallfn;
		constructTable(flag,tableId,donorColManager,donorLabServerParams,donorLabColDef,donorLabaoColumn,donorDefault_sort,drawBack);
} 
var cd34_3="";

function constructProductTable(flag,donor,tableId,donorProcedure){
	//alert("construct table : "+tableId+",Donor : "+donor);
	var criteria = "";
	if(donor!=null && donor!=""){
		//?BB criteria = " and donorId ="+donor+"and donorProcedure ="+donorProcedure;
		criteria = " and donorId ="+donor
	}
	var productLabServerParams = function ( aoData ) {
														aoData.push( { "name": "application", "value": "stafa"});
														aoData.push( { "name": "module", "value": "aphresis_ProductlabResults"});
														aoData.push( { "name": "criteria", "value": criteria});
														
														aoData.push( { "name": "param_count", "value":"1"});
														aoData.push( { "name": "param_1_name", "value":":DONOR:"});
														aoData.push( { "name": "param_1_value", "value":donor});
														
														
														
														aoData.push( {"name": "col_1_name", "value": "lower(collectionDay)" } );
														aoData.push( { "name": "col_1_column", "value": "lower(collectionDay)"});
														
														aoData.push( { "name": "col_2_name", "value": "lower(wbc)"});
														aoData.push( { "name": "col_2_column", "value": "lower(wbc)"});
																	
														aoData.push( {"name": "col_3_name", "value": "lower(hct)" } );
														aoData.push( { "name": "col_3_column", "value": "lower(hct)"});
														
														aoData.push( {"name": "col_4_name", "value": "lower(plt)" } );
														aoData.push( { "name": "col_4_column", "value": "lower(plt)"});
														
														aoData.push( {"name": "col_5_name", "value": "lower(mncper)" } );
														aoData.push( { "name": "col_5_column", "value": "lower(mncper)"});
														
														aoData.push( {"name": "col_6_name", "value": "lower(cd34)" } );
														aoData.push( { "name": "col_6_column", "value": "lower(cd34)"});
														
														//aoData.push( {"name": "col_7_name", "value": "lower(cdyield)" } );
														//aoData.push( { "name": "col_7_column", "value": "lower(cdyield)"});
														
														aoData.push( {"name": "col_7_name", "value": "lower(litterProcessed)" } );
														aoData.push( { "name": "col_7_column", "value": "lower(litterProcessed)"});
														
														aoData.push( {"name": "col_8_name", "value": "lower(machineVolume)" } );
														aoData.push( { "name": "col_8_column", "value": "lower(machineVolume)"});
														
														aoData.push( {"name": "col_9_name", "value": "lower(scaleVoulume)" } );
														aoData.push( { "name": "col_9_column", "value": "lower(scaleVoulume)"});
														
														/* aoData.push( {"name": "col_11_name", "value": "lower(noOfBag)" } );
														aoData.push( { "name": "col_11_column", "value": "lower(noOfBag)"}); */
											};
	var productLabaoColDef = [
			                  {		
			                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
		                	 			return "<input type='checkbox' id='productLabResult' value="+ source.productLabResult+"><input type='hidden' id='productProtocol' name='productProtocol' value='"+source.protocol+"'>";
		                	     }},
			                  {"sTitle": "Collection Day",
		                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) { 
			                	 		return source.collectionDay;
			                	 }},
							  {"sTitle": "WBC",
			                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
									    return source.wbc;
									    }},
							  {"sTitle": "HCT",
									 "aTargets": [3], "mDataProp": function ( source, type, val ) {
								 	   return source.hct;
							  		}},
							  {"sTitle": "PLT",
							  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
								  		return source.plt;
								  }},
							  {"sTitle": "%MNC",
									 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
								  		return source.mncper;
								  }},
							  {"sTitle": ""+cd34_3,
									 "aTargets": [6], "mDataProp": function ( source, type, val ) { 
								  		return source.cd34;
								  }},
							 /*  {"sTitle": "CD Yield",
									 "aTargets": [7], "mDataProp": function ( source, type, val ) { 
								  		return source.cdyield;
								  }}, */
							  {"sTitle": "Liter Processed(L)",
									 "aTargets": [7], "mDataProp": function ( source, type, val ) { 
								  		return source.litterProcessed;
								  }},
							  {"sTitle": "Machine Volume(L)",
									 "aTargets": [8], "mDataProp": function ( source, type, val ) { 
								  		return source.machineVolume;
								  }},
							  {"sTitle": "Scale Volume",
									 "aTargets": [9], "mDataProp": function ( source, type, val ) { 
								  		return source.scaleVoulume;
								  }}
							 /*  {"sTitle": "#of Bags",
									 "aTargets": [11], "mDataProp": function ( source, type, val ) { 
								  		return source.noOfBag;
								  }} */
							];
	var productLabColManager = function () {
											$("#productLabResultColumn").html($('#productLabResultTable_wrapper .ColVis'));
											};
	var productLabaoColumn = [ { "bSortable":false},
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null
				              // null
				               
				             ];
	var productDefault_sort = [[ 1, "asc" ]];
	var productDrawBack = drawCallProduct;
	constructTable(flag,tableId,productLabColManager,productLabServerParams,productLabaoColDef,productLabaoColumn,productDefault_sort,productDrawBack);
	
}


//?Muthu Not in Use

/* function consDnrScrTable(tablename){
	$('#'+tablename).dataTable({
	    "sPaginationType": "full_numbers",
	    "aoColumns":[{"bSortable": false},
	                   null,
	                   null,
	                   null,
	                   null,
	                   null,
	                   null],
	        "sDom": 'C<"clear">Rlfrtip',
	        "aaSorting":[[ 1, "asc" ]],
	        "oColVis": {
	            "aiExclude": [ 0 ],
	            "buttonText": "&nbsp;",
	            "bRestore": true,
	            "sAlign": "left"
	        },
	        "fnInitComplete": function () {
	            $("#donorScreenColumn").html($('#'+tablename+'_wrapper .ColVis'));
	        }
	});
} */

var donorVal;
var donorProcedure
$(document).ready(function(){
	
	$("div[id='dnrsrnpopup']").remove();
	$("#tenseSpan").append('<div id= "dnrsrnpopup" style="display:none;"></div>');
	donorProcedure= $("#donorProcedure").val();
	try{
		$("#tabs" ).tabs();
		$("#tabs ul").removeClass("ui-widget-header");
		}catch(err){
			alert("error " + err);
		}
		//alert("end of tab");
		
	//hide_slidewidgets();
	show_innernorth();
	show_slidewidgets("sidewidgets");
	hide_slidecontrol();
	$('#inner_center').scrollTop(0);
	 $("#divDonorAboRh").text($("#donorabo option:selected").text());
	 $("#divDonation").text($("#donorType option:selected").text());
	 $('#tempMobilizationTime').timepicker({});
	       $("#plannedCollectionDate").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	           changeYear: true, yearRange: "c-50:c+45"});
	       $( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true, changeYear: true, yearRange: '1900:' + new Date().getFullYear()});
	       $(".dateClass").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	           changeYear: true, yearRange: "c-50:c+45"});
	   	addDatepicker();     
	       
	//show_techslideprocess('sidecont');
	donorVal = $("#donor").val();
	
	//for Planned Procedure Check
	var plannedProceduredesc = $("#plannedProceduredesc").val();
	var checkTC = plannedProceduredesc.slice(-2);
	cd34_3="CD34+";
	if(checkTC == "TC"){
		cd34_3="CD3+";
		$("#cd34").hide();
	}else{
		$("#cd3").hide();
	}
	
	constructDonorLabTable(false,donorVal,'donorLabResultTable',donorProcedure);
	
	constructProductTable(true,donorVal,'productLabResultTable',donorProcedure);
	//consDnrScrTable('donorScreeningTable');
	//alert("t1");
	if(constructADTScreeningTable(true,'donorScreeningTable')){
		//alert("inside if");
		checkDataExists('donorScreeningTable');	
	}	
	//alert("trace end ");
	var donorType= $("#donorType option:selected").text(); 
	//alert("donorType>" + donorType+"<");
	if(donorType=="Auto" || donorType=="AUTO"){
		$("#tmpdonorType").val("1");
		bindAutoType(true);
	}
	if($("#translateneed").val()==1){
		$("#isTranslaortYes").attr('checked', 'checked');
	}else{
		$("#isTranslaortNo").attr('checked', 'checked');
	}
	donorVal = $("#donor").val();
	enType=$("#entityType").val();
	constructDocumentTable(false,donorVal,enType,'documentreviewTable');
	constructDocumentHistoryTable(false,donorVal,enType,'documentreviewTable1');
	 $('select').uniform();
	 var param ="{module:'APHERESIS',page:'DONOR_DETAILS'}";
		markValidation(param);
		$("#donorWeight").addClass("decimalTextBox");
		$("#recipientWeight").addClass("decimalTextBox");
		$("#height").addClass("decimalTextBox");
	
		//?BB set  blank for first time
		if($("#collectionOrder").val() ==0){
			
			$("#targetProcessingVolume").val("");
			$("#targetCD34Counts").val("");
		}
});

function setMobilizationTime(){
    $('#mobilizationTime').val($('#mobilizationDate').val() +" "+ $('#tempMobilizationTime').val());
}
 

</script>
<div id="sidecont" style="display:none">
<div class="" >
<ul class="ulstyle">
<!-- <li class="listyle print_label" ><b>Label</b> </li>
<li class="listyle print_label"><img src = "images/icons/print_small.png"  style="width:30px;height:30px;"/></li> -->
<li class="listyle print_label" ><b>Calendar</b> </li>
<li class="listyle print_label"><img src = "images/icons/calendar.png"  style="width:30px;height:30px;"/></li>
<li class="listyle notes_icon"><b>Notes</b> </li>
<li class="listyle notes_icon"><img src = "images/icons/notes.png"  style="width:30px;height:30px;"/></li>

</ul>
</div>
</div>
<span id="spanIDMResults" class="hidden">
  <s:select id="idmResults"  name="idmResults" list="lstHIVTYPE1" listKey="pkCodelst" listValue="description"  headerKey="0" headerValue="Select"/>
</span>

<div id="sidewidgets" style="display:none">
	<div  class="portlet">
    	<div class="portlet-header ">Donor Info</div>
		<div class="portlet-content">
			
							<table width="100%" border="0">
								<tr>
									<td width="50%"><s:text name="stafa.label.donorid"/></td>
									<td width="50%"><s:property  value="%{donorDetails.personMRN}" /></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.name"/></td>
									<td><s:property  value="%{donorDetails.lastName}" /> &nbsp; <s:property  value="%{donorDetails.firstName}" /></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.dob"/></td>
									<td><s:property  value="%{donorDetails.dob}" /></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.donoraborh"/></td>
									<td><div id="divDonorAboRh"></div></td>
								</tr>
								<tr>
									<td>Procedure</td>
									<td><s:property  value="%{donorDetails.plannedProceduredesc}" /></td>
								</tr>
								
								<tr>
									<td>Donor Type</td>
									<td><div id="divDonation"></div></td>
								</tr>
								<tr>
									<td>Weight</td>
									<td><s:property  value="%{donorDetails.personWeight}" /></td>
								</tr>
								<tr>
									<td>Height</td>
									<td><s:property  value="%{donorDetails.personHeight}" /></td>
								</tr>
								<tr>
									<td>Target Vol</td>
									<td><s:property  value="%{donorDetails.targetProcessingVolume}" /></td>
								</tr>
							</table>

						
		</div>
	</div>
	
	<div  class="portlet">
    	<div class="portlet-header ">Recipient Info</div>
		<div class="portlet-content">
			
							<table width="100%" border="0">
								<tr>
									<td width="50%"><s:text name="stafa.label.recipientmrn"/></td>
									<td width="50%"><s:property  value="%{donorDetails.recipient_id}" /></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.name"/></td>
									<td><s:property  value="%{donorDetails.recipient_name}" /> </td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.dob"/></td>
									<td><s:property  value="%{donorDetails.recipient_dob}" /></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.donoraborh"/></td>
									<td><s:property  value="%{donorDetails.recipient_bloodGroup}" /></td>
								</tr>
								
								
								<tr>
									<td>Diagnosis</td>
									<td><s:property  value="%{donorDetails.diagonsis}" /></td>
								</tr>
								<tr>
									<td>Weight</td>
									<td><s:property  value="%{donorDetails.recipient_weight}" /></td>
								</tr>
							
							</table>

						
		</div>
	</div>
	
</div>

<form id="donorDetailForm">

<s:hidden id="donor" name="donor" value="%{donor}"/>
<s:hidden id="donorProcedure" name="donorProcedure" value="%{donorDetails.plannedProcedure}"/>
<s:hidden id="person" name="person" value="%{donorDetails.personId}"/>
<s:hidden id="donorScreening" name="donorScreening" value="%{donorDetails.donorScreening}"/>
<s:hidden id="collectionOrder" name="collectionOrder" value="%{donorDetails.collectionOrder}"/>
<s:hidden id="mobilization" name="mobilization" value="%{donorDetails.mobilization}"/>
<s:hidden id="productType" name="productType" value="%{donorDetails.productType}"/>
<s:hidden id="recipient" name="recipient" value="%{donorDetails.recipient}"/>
<s:hidden id="plannedProceduredesc" name="plannedProceduredesc" value="%{donorDetails.plannedProceduredesc}"/>
<input type="hidden" name="tmpdonorType" id="tmpdonorType" value="0" />
<div class="hidden">
  		<select name="mobilizationName_hidden" id="mobilizationName_hidden" >
				<option value="">Select</option>
				<s:iterator value="lstMobilizerName" var="rowVal" status="row">
						<option value='<s:property value="#rowVal.pkCodelst" />'><s:property value="#rowVal.description" /></option>
				</s:iterator> 		
			</select>
</div>	
<s:iterator var="rowVal" value="lstPreCollection" status="rowStatus">
				<s:if test="lstPreCollection.size>0">
					<s:hidden id="preCollection" name="preCollection" value='%{#rowVal[0]}'></s:hidden>
					<s:hidden id="preCollectionName" name="preCollectionName" value='%{#rowVal[1]}'></s:hidden>
				</s:if>
				<s:else>
					<s:hidden id="preCollection" name="preCollection" value='0'></s:hidden>
					<s:hidden id="preCollectionName" name="preCollectionName" value=''></s:hidden>
				</s:else>
</s:iterator>
				
<div class="column">       
	<div  class="portlet">
    	<div class="portlet-header ">Donor Details</div>
		<div class="portlet-content">
		<!-- <div id="button_wrapper">
				<div style="margin-left:40%;">
					<input class="scanboxSearch barcodeSearch"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;"/>
					<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" />
				</div>
			</div> -->
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<%-- <td class="tablewapper">Donor Id</td>
					<td class="tablewapper1"><s:textfield  value="%{donorDetails.personaltId}" id="altId" name="altId"/></td> --%>
					<td class="tablewapper"><s:text name="stafa.label.donorid"/></td>
					<td class="tablewapper1"><s:textfield value="%{donorDetails.personMRN}" id="donorMRN" name="donorMRN"  /></td>
					<td class="tablewapper"><s:text name="stafa.label.donorGender"/></td>
					<td class="tablewapper1"><s:select value="%{donorDetails.gender}" id="gender"  name="gender" list="lstGender" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeGender('donor')"/></td>
				</tr>
				<tr>
					<td class="tablewapper"><s:text name="stafa.label.donorssn"/></td>
               		<td class="tablewapper1"><s:textfield value="%{donorDetails.donorSsn}" id="donorSSN" name="donorSSN"/></td>
               		<td class="tablewapper"><s:text name="stafa.label.Donor_Weight"/></td>
					<td class="tablewapper1"><s:textfield value="%{donorDetails.personWeight}" id="donorWeight" name="donorWeight" style="width:60px;"/>&nbsp;<span class='smallSelector '><s:select value="%{donorDetails.donorWeightUnits}"  id="donorUnitsWeight"  name="donorUnitsWeight" list="lstUnitWeight" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeUnit('donor');"/>
						</span>
					</td>
                </tr>	
				<tr>
					<td class="tablewapper"><s:text name="stafa.label.donor_LastName"/></td>
               		<td class="tablewapper1"><s:textfield value="%{donorDetails.lastName}" id="donorLastName" name="donorLastName"/></td>
               		<td class="tablewapper"><s:text name="stafa.label.donorWeightTaken"/></td>
                   <td class="tablewapper1"><s:textfield Class="dateEntry" value="%{donorDetails.donorWeightTaken}" id="donorWeightTaken" name="donorWeightTaken"/></td>
               		
					
                </tr>
                <tr>
                 	<td class="tablewapper"><s:text name="stafa.label.donor_FirstName"/></td>
               		<td class="tablewapper1"><s:textfield value="%{donorDetails.firstName}" id="donorFirstName" name="donorFirstName"/></td>
					<td class="tablewapper"><s:text name="stafa.label.Donor_Height"/></td>
				<td class="tablewapper1"><s:textfield value="%{donorDetails.personHeight}" id="height" name="height" style="width:60px;"/>&nbsp;<span class='smallSelector '><s:select value="%{donorDetails.donorHeightUnits}"  id="donorUnitsHeight"  name="donorUnitsHeight" list="lstUnitHeight" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/>
</span></td>
				</tr>
				<tr>
					<%-- <td class="tablewapper">Donor DOB</td>
					<td class="tablewapper1"><s:textfield Class="dateEntry" value="%{donorDetails.dob}" id="dob" name="dob"/></td> --%>
					<td class="tablewapper"><s:text name="stafa.label.donor_MiddleInitial"/></td>
               		<td class="tablewapper1"><s:textfield value="%{donorDetails.donorMiddleName}" id="donorMiddleName" name="donorMiddleName"/></td>
					<td class="tablewapper"><s:text name="stafa.label.donorPhysician"/></td>
					<td class="tablewapper1"><s:select value="%{donorDetails.physician}" name="physician" id="physician" list="lstDonorPhysician" listKey="pkCodelst" listValue="description" headerKey="" headerValue="Select"  /></td>
					
				</tr>
				<tr>
					<%-- <td class="tablewapper">Donor ABO/Rh</td>
					<td class="tablewapper1"><s:select value="%{donorDetails.fkCodelstBloodgrp}" id="abo" name="abo" list="lstABORH" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/> </td> --%>
					<td class="tablewapper"><s:text name="stafa.label.donordob"/></td>
					<td class="tablewapper1"><s:textfield Class="dateEntry" value="%{donorDetails.dob}" id="donorDob" name="donorDob"/></td>
					<td class="tablewapper"><s:text name='stafa.label.languageSpoken'/></td>
					<td class="tablewapper1"><s:select value="%{donorDetails.language}" id="language"  name="language" list="lstLanguage" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
					
				</tr>
				<tr>
					<td class="tablewapper"><s:text name='stafa.label.donortype'/></td>
					<td class="tablewapper1">
						<s:select value="%{donorDetails.donorType}" id="donorType"  name="donorType" list="lstDonorType" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeDonorType()"/>
                    </td>
					<td class="tablewapper"><s:text name='stafa.label.translatorNeeded'/></td>
					<td class="tablewapper1"><s:hidden name="translateneed" id="translateneed" value="%{donorDetails.isTranslaortneed}" /><input type="radio" name="isTranslaortneed" id ="isTranslaortYes" value=1/>Yes&nbsp;<input type="radio" id="isTranslaortNo" name="isTranslaortneed" value=0/>No</td>
					
				</tr>
				  <tr>
                <td class="tablewapper"><s:text name='stafa.label.donoraborh'/></td>
					<td class="tablewapper1"><s:select value="%{donorDetails.fkCodelstBloodgrp}" id="donorabo" name="donorabo" list="lstABORH" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeABO('donor');"/> </td>
					<td class="tablewapper"><s:text name="stafa.label.donoreligibility"/></td>
					<td class="tablewapper1"><s:select  value="%{donorDetails.donorEligibility}" id="donorEligibility" name="donorEligibility" list="lstDonorEligibility" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/> </td>
					<%--  --%>
				</tr> 
				 <tr >
                    <td class="tablewapper"><s:text name="stafa.label.donorabs"/></td>
                    <td class="tablewapper1">
                    	<s:select value="%{donorDetails.donorAbs}"  id="donorAbs"  name="donorAbs" list="lstABS" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeABS('donor');"/></td>
                    <td class="tablewapper"><s:text name="stafa.label.plannedcollectiondate"/></td>
                   <td class="tablewapper1"><s:textfield Class="dateEntry" value="%{donorDetails.plannedCollectionDate}" id="plannedCollectionDate" name="plannedCollectionDate"/></td>
                </tr>
              
			</table>
        </div>
    </div>
</div>
</form>

<form>
<div class="column">       
	<div  class="portlet">
    	<div class="portlet-header ">Recipient Details</div>
		<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
			<tr>
                 	<td class="tablewapper"><s:text name='stafa.label.recipientmrn'/></td>
                    <td class="tablewapper1"><s:textfield value="%{donorDetails.recipient_id}" id="recipientMRN" name="recipientMRN" onblur="autoCheck()" /></td>
                    <td class="tablewapper"><s:text name="stafa.label.recipientabo" /></td>
                    <td class="tablewapper1">
                    	<s:select value="%{donorDetails.recipientAbo}"  id="recipientabo"  name="recipientabo" list="lstABORH" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeABO('recipient');"/>
                    </td>
                     <%--   --%>
               </tr>
               <tr>
                	<td class="tablewapper"><s:text name="stafa.label.recipientssn" /></td>
                	<td class="tablewapper1"><s:textfield value="%{donorDetails.recipientSsn}" id="recipientSSN" name="recipientSSN"/></td>
                	<td class="tablewapper"><s:text name="stafa.label.recipientabs"/></td>
                    <td class="tablewapper1">
                    	<s:select value="%{donorDetails.recipientAbs}"  id="recipientAbs"  name="recipientAbs" list="lstABS" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeABS('recipient');"/>
                    </td>
                </tr>
    			 <tr>
                	 <td class="tablewapper"><s:text name="stafa.label.recipient_LastName"/></td>
                    <td class="tablewapper1"><s:textfield value="%{donorDetails.recipientLname}" id="recipientLastName" name="recipientLastName"/></td>
                    <td class="tablewapper"><s:text name="stafa.label.recipientGender"/></td>
					<td class="tablewapper1"><s:select value="%{donorDetails.gender}" id="recipientGender"  name="recipientGender" list="lstGender" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeGender('recipient')"/></td>
                </tr>
                 <tr>
                 <td class="tablewapper"><s:text name="stafa.label.recipient_FirstName"/></td>
                    <td class="tablewapper1"><s:textfield value="%{donorDetails.recipientFname}" id="recipientFirstName" name="recipientFirstName"/></td>
                    <td class="tablewapper"><s:text name="stafa.label.recipientweight"/></td>
                     <td class="tablewapper1"><s:textfield value="%{donorDetails.recipient_weight}" id="recipientWeight" name="recipientWeight" style="width:60px;"/>&nbsp;<span class='smallSelector '><s:select value="%{donorDetails.recipientWeightUnits}" id="recipientUnitsWeight"  name="recipientUnitsWeight" list="lstUnitWeight" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" onchange="changeUnit('recipient');"/>
</span></td>
                    
                 </tr>
                 
                 
                 <tr>
                 <td class="tablewapper"><s:text name="stafa.label.recipient_MiddleInitial"/></td>
                     <td class="tablewapper1"><s:textfield value="%{donorDetails.recipientMname}" id="recipientMiddleName" name="recipientMiddleName"/></td>
                     <td class="tablewapper"><s:text name="stafa.label.transplant_Physician"/></td>
                    <td class="tablewapper1">
						<s:select value="%{donorDetails.recipientTransplantPhysician}"  id="transplantPhysician"  name="transplantPhysician" list="lstTransplantPhysician" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/> 
              		</td>
                   <!-- <input type="button" value="Search" onclick="javascript:searchPerson('RECIPIENT');"/></td>
                    -->
                </tr> 
                  <tr >
                   <td class="tablewapper"><s:text name="stafa.label.recipientdob"/></td>
                    <td class="tablewapper1"><s:textfield Class="dateEntry" value="%{donorDetails.recipient_dob}" id="recipientDob" name="recipientDob"/></td> 
                    <td class="tablewapper"><s:text name="stafa.label.recipientdiagnosis"/></td>
                    <td class="tablewapper1">
                          <s:select value="%{donorDetails.donorDiagnosis}"  id='donordiagnosis'  name="donordiagnosis" list="lstDiagonosis" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/>
                     </td>
                </tr>     
                
		</table>
		</div>
	</div>
</div>
</form>
	



<form>
<div class="column">       
	<div  class="portlet">
    	<div class="portlet-header ">Donor Screening Results<button id="donorScnIm" type="button" onclick="javascript:donorScreenImport('donorScreeningTable');">Import</button></div>
		<div class="portlet-content">
						
		
        <div class="tableOverflowDiv">
					<table cellpadding="0" cellspacing="0" border="1" id="donorScreeningTable" class="display" width="100%">
						<thead>
							<tr>
								<th width="4%" id="donorScreenColumn"></th>
								<th>Panel</th>
								<th>Test Name</th>
								<th>Ref #</th>
								<th>Test Date</th>
								<th>Result</th>
								<!-- <th>Units</th> -->
							</tr>
						</thead>
						<tbody>
													
						</tbody>
					</table>
					<span id="tenseSpan">
						<div id= "dnrsrnpopup" style="display:none;"></div>
					</span>
				</div>
        
        
        
        </div>
    </div>
</div>
</form>
<span id="procedureSpan" class="hidden">
 		<s:select id="procedure"  name="procedure" list="lstPlannedProcedure" listKey="protocol" listValue="protocolName"  headerKey="" headerValue="Select"/>
</span>
<div class="column">		
	<div  class="portlet">
		<div class="portlet-header ">Donor Lab Results</div>
		<div class="portlet-content">
			<table>
			<tr>
				<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="add_donorLabResults()"/>&nbsp;&nbsp;<label  class="cursor" onclick="add_donorLabResults()"><b>Add </b></label>&nbsp;</div></td>
				<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="delete_donorLabResults();" />&nbsp;&nbsp;<label class="cursor" onclick="delete_donorLabResults();"><b>Delete</b></label>&nbsp;</div></td>
				<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;" class="cursor" id ="" onclick="edit_donorLabResults();"/>&nbsp;&nbsp;<label id="dchild" class="cursor" onclick="edit_donorLabResults();"><b>Edit</b></label>&nbsp;</div></td>
				<!-- <td style="width:20%"><div><img src = "images/icons/save3.png" style="width:16;height:16;" class="cursor" id ="" onclick="save_donorLabResults();" >&nbsp;&nbsp;<label id="schild" class="cursor" onclick="save_donorLabResults();"><b>Save</b></label>&nbsp;</div></td> -->
			</tr>
			</table>
			<form>
				<div class="tableOverflowDiv">
					<table cellpadding="0" cellspacing="0" border="1" id="donorLabResultTable" class="display" width="100%">
						<thead>
							<tr>
								<th width="4%" id="donorLabResultColumn"></th>
								<th>Collection Day</th>
								<th>WBC</th>
								<th>CD34</th>
								<th>%CD34</th>
								<th>HCT</th>
								<th>PLT</th>
								<th>%MNC</th>
								<th>K+</th>
							</tr>
						</thead>
						<tbody>
													
						</tbody>
					</table>
				</div>
			</form>		
		</div>	
	</div>
</div>

<div class="column">		
		<div  class="portlet">
			<div class="portlet-header ">Product Lab Results</div>
				<div class="portlet-content">
				<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="add_productLabResults()"/>&nbsp;&nbsp;<label  class="cursor" onclick="add_productLabResults()"><b>Add </b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="delete_productLabResults();" />&nbsp;&nbsp;<label class="cursor" onclick="delete_productLabResults();"><b>Delete</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;" class="cursor" id ="" onclick="edit_productLabResults();"/>&nbsp;&nbsp;<label id="dchild" class="cursor" onclick="edit_productLabResults();"><b>Edit</b></label>&nbsp;</div></td>
					<!-- <td style="width:20%"><div><img src = "images/icons/save3.png" style="width:16;height:16;" class="cursor" id ="" onclick="save_productLabResults();" >&nbsp;&nbsp;<label id="schild" class="cursor" onclick="save_productLabResults();"><b>Save</b></label>&nbsp;</div></td> -->
				</tr>
				</table>
				
				<form>
					<div class="tableOverflowDiv">
						<table cellpadding="0" cellspacing="0" border="1" id="productLabResultTable" class="display" width="100%">
							<thead>
								<tr>
									<th width="4%" id="productLabResultColumn"></th>
									<th>Collection Day</th>
									<th>WBC</th>
									<th>HCT</th>
								    <th>PLT</th>
								    <th>%MNC</th>
									<th>CD34</th>
									<!-- <th>CD Yield</th> -->
									<th>Liter Processed(L)</th>
								    <th>Machine Volume(L)</th>
								    <th>Scale Volume(L)</th>
									<!-- <th># of Bags</th> -->
								</tr>
							</thead>
							<tbody>
														
							</tbody>
						</table>
					</div>
				</form>		
				</div>	
			</div>
</div>

<div class="column">
		<div  class="portlet">
			<div class="portlet-header ">Document Review</div>
				<div class="portlet-content">
					
				<div id="tabs" width="100%">
				    <ul >
				     <li><a href="#tabs-1">Current</a></li>
				      <li><a href="#tabs-2">Historic</a></li>
				    </ul>
			  <div id="tabs-1" >   
				<br>
				<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="add_documentReview()"/>&nbsp;&nbsp;<label  class="cursor" onclick="add_documentReview()"><b>Add </b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="delete_documentReview()" />&nbsp;&nbsp;<label class="cursor" onclick="delete_documentReview()"><b>Delete</b></label>&nbsp;</div></td> 
					<!-- <td style="width:20%"><div><img src = "images/icons/save3.png" style="width:16;height:16;" class="cursor" id ="" onclick="save_documentReview();" >&nbsp;&nbsp;<label id="schild" class="cursor" onclick="save_documentReview();"><b>Save</b></label>&nbsp;</div></td> -->
				</tr>
				</table>
				<s:form id="attachedDocsForm" name="attachedDocsForm" >
				<s:hidden id="donor" name="donor" value="%{donor}"/>
				<s:hidden id="entityType" name="entityType" value="DONOR"/>
				<span class="hidden">
				<s:select id="docsTitleTemp" name="docsTitleTemp" list="lstDocumentType" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/>
				</span>
				<div class="tableOverflowDiv">
				<table cellpadding="0" cellspacing="0" border="0" id="documentreviewTable" class="display" width="100%">
					<thead>
						<tr>
							<th width="4%" id="documentreviewColumn"></th>
							<th>Document Title</th>
							<th>Document Date</th>
							<th>Attachment</th>
							<th>Reviewed</th>
							<th>Version #</th>
						</tr>
					</thead>
				</table>
				</div>
				</s:form>
				</div>	
			<div id="tabs-2" >
			  Historic documents
			  <div class="tableOverflowDiv">
			  <table cellpadding="0" cellspacing="0" border="0" id="documentreviewTable1" class="display" width="100%">
					<thead>
						<tr>
							<th width="4%" id="documentreviewColumn1"></th>
							<th>Document Title</th>
							<th>Document Date</th>
							<th>Attachment</th>
							<th>Reviewed</th>
							<th>Version #</th>
						</tr>
					</thead>
				</table>
				</div>
			</div>	
			</div>
				
				</div>	
			</div>
</div>
 
 <form>
 <div class="column">       
	<div  class="portlet">
    	<div class="portlet-header ">Collection Order</div>
		<div class="portlet-content">
		
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Collection Order Issued By</td>
					<td class="tablewapper1"><s:select value="%{donorDetails.collectionOrderIssue}" name="collectionOrderIssue" id= "collectionOrderIssue" list="lstCollectionOrderBy" listKey="pkCodelst" listValue="description" headerKey="" headerValue="Select"  /></td>
					<td class="tablewapper"><span id="cd34">Target CD34 Counts</span><span id="cd3">Target CD3 Counts</span></td>
					<td class="tablewapper1"><s:textfield value="%{donorDetails.targetCD34Counts}" id="targetCD34Counts" name="targetCD34Counts"style="width:60px;"/>&nbsp;<span class='smallSelector'><s:select value="%{donorDetails.targetCD34Units}"  id="targetCD34Unit"  name="targetCD34Unit" list="lstUnittTargetCD34" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></span></td>
					
				</tr>
				<tr>
					<td class="tablewapper">Target Processing Volume(L)</td>
					<td class="tablewapper1"><s:textfield value="%{donorDetails.targetProcessingVolume}" id="targetProcessingVolume" name="targetProcessingVolume"/></td>
					<td class="tablewapper"></td>
					<td class="tablewapper1"></td>
                </tr>
               
			</table>
        </div>
    </div>
</div>
</form>
<form>
 <div class="column">       
	<div  class="portlet">
    	<div class="portlet-header ">Most Recent Mobilization Information</div>
		<div class="portlet-content">
		
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Mobilization Order Issued By</td>
					<td class="tablewapper1"><s:select value="%{donorDetails.mobilizationOrderIssuedby}" name="mobilizationOrderIssuedby" id="mobilizationOrderIssuedby" list="lstMobilizationOrderBy" listKey="pkCodelst" listValue="description" headerKey="" headerValue="Select"  /></td>
					<td class="tablewapper">Mobilization Nurse</td>
					<td class="tablewapper1"><s:select value="%{donorDetails.mobilizationNurse}" name="mobilizationNurse" id="mobilizationNurse" list="lstMobilizationNurse" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"  /></td>
					
				</tr>
				<tr>
					<td class="tablewapper">Mobilization Date</td>
					<td class="tablewapper1"><s:textfield value="%{donorDetails.mobilizationDate}" id="mobilizationDate" name="mobilizationDate" Class="dateEntry"/></td>
					<td class="tablewapper">Mobilization Method</td>
					<td class="tablewapper1"><s:select value="%{donorDetails.method}" id="method"  name="method" list="lstMethod" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
					
				</tr>
				<tr>
					<td class="tablewapper">Time of Mobilization</td>
					<td class="tablewapper1"><s:hidden  id="mobilizationTime" name="mobilizationTime" Class=""/><s:textfield value="%{donorDetails.mobilizationTime}" id="tempMobilizationTime" name="tempMobilizationTime" onchange="setMobilizationTime();"/></td>
					<td class="tablewapper"></td>
					<td class="tablewapper1"></td>
                </tr>
               
			</table>
			<hr>
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%" id="mobilizationrow">
			<s:iterator var="rowVal" value="lstMobilizerDetails" status="rowStatus">
				<s:if test="lstMobilizerDetails.size>0">
					<tr id="mobilizationcontent">
						<td class="tablewapper"><s:hidden id="mobilizationDetail" name="mobilizationDetail" value='%{#rowVal.mobilizationDetail}' /> Mobilizer Name</td>
						<td class="tablewapper1">
							<s:select id="mobilizerName"  class="mobilaztionSelect" value='%{#rowVal.mobilizerName}' name="mobilizerName" list="lstMobilizerName" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/>
								 <s:if test="%{#rowStatus.index == 0}">  
									<span><img src = "images/icons/addnew.jpg" class="cursor mobilizationAdd" id="" onclick="add_mobilization()"/></span>
								</s:if> 
								
						  </td>
						<td class="tablewapper">Mobilizer Dose</td>
						<td class="tablewapper1"><s:textfield value='%{#rowVal.mobilizerDose}'  id="mobilizerDose" name="mobilizerDose"/>
						
						</td>
					</tr>
					</s:if>
					<s:else><tr id="mobilizationcontent">
						<td class="tablewapper"><s:hidden id="mobilizationDetail" name="mobilizationDetail" value="0"/> Mobilizer Name</td>
						<td class="tablewapper1">
							<s:select id="mobilizerName" class="mobilaztionSelect" value="%{donorDetails.mobilizerName}" name="mobilizerName" list="lstMobilizerName" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/>
								<span><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="add_mobilization()"/></span>
						  </td>
						<td class="tablewapper">Mobilizer Dose</td>
						<td class="tablewapper1"><s:textfield value="%{donorDetails.mobilizerDose}" id="mobilizerDose" name="mobilizerDose"/></td>
					</tr>
					</s:else>
				</s:iterator>
               
			</table>
        </div>
    </div>
</div>
</form>

 

<div align="left" style="float:right;">
    <form onsubmit="return avoidFormSubmitting()">
        <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
            <tr>
                <td><span>Next:
                <select name="nextOption" id="nextOption" ><option value="">Select</option><option value="loadDonorAssignment.action">Donor Assignment</option><option value="logout.action">Log Out</option></select>
                <input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
				<input type="button" value="Next" onclick="verifyeSign('eSignNext','next')"/></span></td>
            </tr>
        </table>
    </form>
</div>


<script>

function add_mobilization(){
		/* //alert("add "+$("#mobilizationcontent").html());
	$("#mobilizerName").addClass("mobilizerClass");
	
	$("#mobilizationrow").append("<tr>"+$("#mobilizationcontent").html()+"</tr>");
	alert("length"+$(".mobilizationAdd").length);
	if($(".mobilizationAdd").length>1){
		$(".mobilizationAdd:first").remove();
	}
	// $.uniform.restore('#mobilizationcontent td select');
	$("#mobilizerName").addClass("mobilizerClass");
	$.uniform.restore(".mobilizerClass");
	$(".mobilizerClass").uniform();
	//$("#mobilizationrow tr select").uniform(); 
	<select class='select1'  id='comment' name='comments' >"+$("#comments_hidden").html()+ "</select>
	<s:select id="mobilizerName" class="mobilaztionSelect" value="%{donorDetails.mobilizerName}" name="mobilizerName" list="lstMobilizerName" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/>
	*/
	$("#mobilizerName").addClass("mobilizerClass");
	$("#mobilizationrow").append("<tr><td class='tablewapper'><input type='hidden'  id='mobilizationDetail' name='mobilizationDetail' value='0'/> Mobilizer Name</td><td class='tablewapper1'><select class='mobilizerClass'  id='mobilizerName' name='mobilizerName' >"+$("#mobilizationName_hidden").html()+"</td><td class='tablewapper'>Mobilizer Dose</td><td class='tablewapper1'><input type='text' value=''  id='mobilizerDose' name='mobilizerDose'/></td></tr>");
	$("#mobilizerName").addClass("mobilizerClass");
	$.uniform.restore(".mobilizerClass");
	$(".mobilizerClass").uniform();
}
$(function() {

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	

	/**For Notes**/
	/* 
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='ui-addnew'></span>");
	
	$( ".portlet-header .ui-addnew" ).click(function() {
		addNewPopup("Add New Product","SupervisoraddNewDialog","addnewAcquisition","700","1100");
	});
	 */
	
	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});
</script>