<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/barcodeScanBox.js"></script>
<script type="text/javascript" src="js/dataSearch.js"></script>
<script type="text/javascript" src="js/donor.js"></script>
<script type="text/javascript" src="js/globalMessages.js"></script>



<script>

function getADTData(mrn,table){
	
	// http://192.168.192.234:8180/TenseADTServer/patient_adt/select?q=*:*&wt=json&fq=PATIENT_ID:W00006295
	  var searchUrl= TENSEURL +"/TenseADTServer/patient_adt/select?q=*:*&wt=json&json.wrf=?&fq=PATIENT_ID:"+mrn ;
	 
	  var mrn="";
	  var firstName="";
	  var lastName="";var dob="";var ssn="";
	  var source='ADT';
	  var lang,abo;
	  var sex ="";
	  var middleName="";
	 var tmpData = {};
	 var source;
      
	  $.ajax({
		  url: searchUrl,
		  dataType: 'json',
		  success: function(data){
			 	  $(data.response.docs).each(function (counter,val){
			 		 
					  mrn= this.PATIENT_ID;
					  if(this.FIRST_NAME!=undefined){
					  	firstName= this.FIRST_NAME;
					  }
					  if(this.LAST_NAME!=undefined){
					  	lastName= this.LAST_NAME;
					  }
					  if(this.MIDDLE_NAME!=undefined){
					  	middleName =this.MIDDLE_NAME;
					  }
					  if(this.DATE_OF_BIRTH!=undefined){
						
					  	dob= converDate(this.DATE_OF_BIRTH);
					 /*   alert("dob" + dob + "/"+this.DATE_OF_BIRTH)
					    var tmpDate = this.DATE_OF_BIRTH.split("-");
						//alert("tmpDate " + tmpDate +" / len----- " + tmpDate[0] + "/" + tmpDate[1]+ " , " + tmpDate[2].substring(0,2));
						var tmpDate1 = new Date(tmpDate[0] ,tmpDate[1]-1, tmpDate[2].substring(0,2));
						//	alert("date val===>"+dateValue+"date==>"+tmpDate1);
						returnValue = $.datepicker.formatDate('M dd, yy', tmpDate1);
						alert("returnValue " + returnValue);
						*/
					  }
					  //alert(this.SSN);
					  if(this.SSN!=undefined){
					  	ssn= this.SSN;
					  }
					  abo="";
					  lang="";
					  if(this.SEX_DESC!=undefined){
						  sex= this.SEX_DESC;
					  }
					  source="ADT";
					//  alert("mrn " + mrn + "firstName "+firstName+"dob"+dob);
					 // alert(table);
					  $("#"+table).dataTable().fnDestroy();
					  //alert(ssn);
					  //alert("mrn===>"+mrn);
					  var tempId=0;
					var  donorLink ="<input type='radio' name='donorSelect' id='donorSelect' onclick='javascript:setDonorDetails(\""+tempId+"\",\""+lastName+"\",";
					donorLink+="\""+firstName+"\",\""+dob+"\",\""+mrn +"\",\""+ssn +"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\""+middleName+"\",\""+ middleName+"\",\""+sex+"\",\""+lang+"\");'/>";
					var recipientLink="<input type='radio' name='recipientSelect' id='recipientSelect' onclick='javascript:setRecipientDetails(\""+tempId+"\",\""+lastName+"\",";
					recipientLink+="\""+firstName+"\",\""+dob+"\",\""+mrn +"\",\""+ssn +"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\""+ middleName+"\",\""+ middleName+"\",\""+sex+"\",\""+lang+"\");'/>";
					
					//alert("recipientLink " + recipientLink);
					if(table=="donorResults"){
					 	$("#"+table).dataTable().fnAddData([donorLink,firstName+" "+lastName,dob,mrn,ssn,abo,lang,source]);
					 }else  if(table=="recipientResults"){
						 $("#"+table).dataTable().fnAddData([recipientLink,firstName+" "+lastName,dob,mrn,ssn,abo,lang,source]);
					 }
					 $("select").uniform();
				  });
			 	  //alert(mrn);
			 	 if(mrn==""){
			 		//$(".donorResultsTR")
			 		//alert("recipientResultTr==>"+table);
			 		 //$("#"+table+"TR").addClass("hidden");
			 		// $("#"+table+"Tr").addClass("hidden"); */
			 		 if(table=="donorResults"){
			 			$(".donorResultsTR").addClass("hidden");
			 		 }else if(table=="recipientResults"){
			 			$(".recipientResultTr").addClass("hidden");
			 		 }
			 	 }
		  },
		  error:  function(jqXHR, textStatus, errorThrown){
		 		 var responseTxt = jqXHR.responseText;
		 		 alert("responseTxt " + responseTxt);
			 		alert("errorThrown " + errorThrown);
			 	
			 	  }
	  
	  });
	
	}

var mergedCols =[1,2,3,4,5,6,7,8,9,10,11,12];
var autoFlag=false;



function add_Donor(){
	//?BB $("#donorNewForm").reset();
	if(checkModuleRights(STAFA_DONORLIST,APPMODE_ADD)){
		addnewDonor();
	}
	// showPopUp("open","addNewDonor","Add New Donor / Collection Cycle","1200","550");
	

	//alert("test");
	/*//?BB 
	  stafaPopUp("open","addNewDonor","Add New Donor / Collection Cycle","900","600");
	  //$("#addNewDonor").dialog("open");
	   $.uniform.restore('select');
		$("select").uniform();
		addDatepicker();
	*/
}



function save_Donor(tableId){
	//alert("save_Donor");
	if(checkModuleRights(STAFA_DONORLIST,APPMODE_SAVE)){
		
		var row,donorId, plannedProcedure, procedure;	
		var rowData = "";
		$("#"+tableId).find("#procedure").each(function (){
			//var row=$(this).closest("tbody").children("tr");
			row=$(this).closest("tr");
			//alert("html "+ $(row).html());
			donorId = $(row).find("#donorCheck").val();
			plannedProcedure = $(row).find("#plannedProcedure").val();
			procedure = $(this).val();	
			
			//alert(donorId + ","+ plannedProcedure +","+ procedure);
			  rowData+= "{plannedProcedure:'"+plannedProcedure+"',donorId:'"+donorId+"',procedure:'"+procedure+"'},";
				
		});
		if(rowData.length >0){
	        rowData = rowData.substring(0,(rowData.length-1));
	   	var url="saveProcedure";
		    response = jsonDataCall(url,"jsonData={data:["+rowData+"]}");
		       alert("Data Saved Successfully");
		       constructDonorList(true);
		}
	}
}
function add_procedure(cur,table){
	//alert("add manufacture");
	var newRow ="";
	//alert("newRow"+newRow);

	var cell = $(cur).parent();
	var cols = cell.closest("tr").children("td").index(cell);
    var rows = cell.closest("tbody").children("tr").index(cell.closest("tr"));
    var coltemp = cols;
    var rowtemp = rows;
	//alert(" rows " + rows + " /cols " + cols  );
	var tmpRow = rows; 
	
	var row  = cell.closest("tr");
	
	//alert(" mer cols"+mergedCols);
		
		newRow ="<tr>";
		var rowSpanFlag = false;
		var colCell;
	//	alert("before row td");
		
			//alert("c "+ c);
            // fetch all cells of this row
            //check if this cell comes before our cell
            var className;
            var tmpRows;
            var rowspan;
            $.each(mergedCols, function(i, colNo) {
           /* mergedCols.forEach(function(colNo) { */
             // alert("colNo" + colNo);
              
	               className="col"+colNo;
	              if(!$(cell).hasClass(className) ){
	               tmpRows=0;
	               $("#"+table+" tbody").find(("."+className)).each(function(i){
	            	 // alert($(this).html() );
	            	   rowspan = parseInt($(this).attr("rowspan"));
	            	//  alert(i +"/colNo " +colNo + " /rowspan "+rowspan + "/tmpRows" +tmpRows + "/rows"+rows);
	            	   if(rowspan >1){
	            		  // alert("(tmpRows+rowspan) " + (tmpRows+rowspan) + "/ " + rows );
	            		  //?BB if( (tmpRows+rowspan)>rows){
	            		   if( (tmpRows)==rows){	   
	            		   		$(this).attr("rowspan",(rowspan+1));
	            		   		//tmpRows=-1000;
	            		   		return false;
	            		   } else if(tmpRows <rows && (tmpRows+rowspan)>rows){
			            	   $(this).attr("rowspan",(rowspan+1));
			      		   		//tmpRows=-1000;
			      		   		return false;
	            		   }else{
	            			   tmpRows = tmpRows +rowspan;
	            		   }
	            	   }else{
	            		 //  alert(" else");
	            		  
	            		   if(tmpRows==rows){
	            			//   alert("inside else rowspan");
	            			 $(this).attr("rowspan",2);
	            		   }
	            		   tmpRows = tmpRows +1;
	            	   }
	            	 
	               });
               }
			});
           
	  		newRow+="<td class='col0'><input type='checkbox' name='donorCheck' id='donorCheck' value="+$(row).find("#donorCheck").val()+" /><input name='plannedProcedure' id= 'plannedProcedure' type='hidden' value='0' /></td>";
			
			newRow+="<td class='col13'>"+$("#procedureSpan").html()+"</td>";
			newRow+="<td class='col14'>&nbsp;</td>";
			newRow+="<td class='col15'>&nbsp;</td>";
			
			newRow+="</tr>";
		 rowspan = parseInt($(cell).attr("rowspan"));
		 tmpRow = rows;
		// alert("rowspan " + rowspan + "/tmpRow"+tmpRow);
            if(rowspan >1){
            	tmpRow = tmpRow+rowspan;
            }else{
            	tmpRow =tmpRow +1;
            }
		
		//tmpRow = rows+1;
		//alert(tmpRow + " / new Row " + newRow);
		$("#"+table+" tbody tr:nth-child("+tmpRow+")").after(newRow);
		//addDatepicker();
   // alert("final rows " + rows + " /cols " + cols);
		$.uniform.restore('select');
		$("select").uniform();
	
	
}

var editflag=false;
function edit_Donor(tableId){
	if(checkModuleRights(STAFA_DONORLIST,APPMODE_EDIT)){
	
		var htmlString =$("#procedureSpan").html();
		$("#"+tableId+" tbody tr").each(function (row){
			 if($(this).find("#donorCheck").is(":checked")){
				 
			editflag=true;
		//		 alert("inside if " + row);
			/*//?BB	 var tdText = $(this).find(".col13").text();
				 $(this).find(".col13").html(htmlString);
				 var optvalue = $("#procedure option:contains('" + tdText +"')").val();
				// alert("option value " +optvalue + " / "+tdText );
				 $(this).find("#procedure").val(optvalue);
					$.uniform.restore('select');
					$("select").uniform();
					if($(this).find(".col13").find("img"))
			*/
			var status=$(this).find(".col15").html();
			var $elems = $(this).find(".col13 img");
			var $selectEle=$(this).find(".col13 select")
			var displayValue=$(this).find(".col13 ").text();
			var optvalue = $("#procedure option:contains("+displayValue+")").val();
			var length = $elems.length;
			var selectLength=$selectEle.length;
			if(length==0 && status!="pending" && status!="Pending"){
				$(this).find(".col13").append('<img align="right"  src = "images/icons/addnew.jpg" onclick="add_procedure(this,\'donordetailsTable\');">');
				}else if((status=="Pending"||status=="pending")&&(selectLength==0)){
					$(this).find(".col13").html($("#procedureSpan").html());
					$(this).find(".col13 select").val(optvalue);
					$.uniform.restore($(this).find(".col13 select"));
					$($(this).find(".col13 select")).uniform(); 	
				}
			 }
		});
		if(editflag==false){
			alert("Please Select the Record");
		} 
		editflag=false;
	}
}




function constructDonorList(flag){
	/* Assigned Donor Table Server Parameters Starts */
//alert("construct donor");
	var donorListTable_serverParam = function ( aoData ) {

		aoData.push( { "name": "application", "value": "stafa"});
		aoData.push( { "name": "module", "value": "aphresis_Donorlist"});	
		
		aoData.push( {"name": "col_1_name", "value": "lower(mrn)" } );
		aoData.push( { "name": "col_1_column", "value": "3"});
		
		aoData.push( { "name": "col_2_name", "value": "lower(pname)"});
		aoData.push( { "name": "col_2_column", "value": "4"});
		
		aoData.push( { "name": "col_3_name", "value": "lower(pdob)"});
		aoData.push( { "name": "col_3_column", "value": "lower(pdob)"});
		
		aoData.push( { "name": "col_4_name", "value": "lower(donorabo)"});
		aoData.push( { "name": "col_4_column", "value": "16"});
		
		aoData.push( { "name": "col_5_name", "value": "lower(recip_id)"});
		aoData.push( { "name": "col_5_column", "value": "11"});
		
		aoData.push( { "name": "col_6_name", "value": "lower(recip_name)"});
		aoData.push( { "name": "col_6_column", "value": "12"});
		
		aoData.push( { "name": "col_7_name", "value": "lower(recip_dob)"});
		aoData.push( { "name": "col_7_column", "value": "13"});
		
		aoData.push( { "name": "col_8_name", "value": "lower(recip_blood)"});
		aoData.push( { "name": "col_8_column", "value": "14"});
		
		aoData.push( { "name": "col_9_name", "value": "lower(diagnosis)"});
		aoData.push( { "name": "col_9_column", "value": "15"});
		
		aoData.push( { "name": "col_10_name", "value": "lower(labresult)"});
		aoData.push( { "name": "col_10_column", "value": "5"});
		
		aoData.push( { "name": "col_11_name", "value": "lower(documents)"});
		aoData.push( { "name": "col_11_column", "value": "6"});
		
		aoData.push( { "name": "col_12_name", "value": "lower(donationtype)"});
		aoData.push( { "name": "col_12_column", "value": "7"});
		
		aoData.push( { "name": "col_13_name", "value": "lower(planprocedure)"});
		aoData.push( { "name": "col_13_column", "value": "8"});
		
		aoData.push( { "name": "col_14_name", "value": "lower(nurse)"});
		aoData.push( { "name": "col_14_column", "value": "9"});
		
		aoData.push( { "name": "col_15_name", "value": "lower(status)"});
		aoData.push( { "name": "col_15_column", "value": "16"}); 

													};
	var donorListTable_aoColumnDef = [
			                         {"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
			                             return "<input name='donorCheck' id= 'donorCheck' type='checkbox' value='" + source[0] +"' /><input name='plannedProcedure' id= 'plannedProcedure' type='hidden' value='" + source[1] +"' />";
			                             }},
			                          {    "sTitle":'<s:text name="stafa.label.donorid"/>',
			                                 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
			                                	 //?BB link = "<a href='#' onclick='javascript:getDonorDetails(\""+source[0] +"\");'>"+source[2]+"</a>";
			                                	 link = source[2];
			                             return link;
			                             }},
			                          {    "sTitle":'<s:text name="stafa.label.donorname"/>',
			                                 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
			                                	 //?BB link = "<a href='#' onclick='javascript:getDonorDetails(\""+source[0] +"\");'>"+source[3] +"</a>";
			                                	 link = source[3] ;
					                             return link;
			                                 
			                                    }},
			                          {    "sTitle":'<s:text name="stafa.label.donordob"/>',
			                                        "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
			                                    return source[4];
			                                  }},
			                                  {    "sTitle":'Donor ABO/Rh',
				                                  "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
				                                      return source[15];
				                              }}, 
				                              {    "sTitle":'Recipient MRN',
				                                  "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
				                                      return source[10];
				                              }}, 
				                              
			                          {    "sTitle":'Recipient Name',
			                                        "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
			                                    return source[11];
			                                  }},    
			                                  {    "sTitle":'Recipient DOB',
			                                        "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
			                                    return source[12];
			                                  }},    
			                                  {    "sTitle":'Recipient ABO/Rh',
			                                        "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
			                                    return source[13];
			                                  }},  
			                                  {    "sTitle":'Diagnosis',
			                                        "aTargets": [ 9], "mDataProp": function ( source, type, val ) {
			                                    return source[14];
			                                  }},    
			                          {    "sTitle":'Lab Result',
			                                        "aTargets": [ 10], "mDataProp": function ( source, type, val ) {
			                                   /*  if(source[5]!=0){
	                                        		return "Reviewed";
	                                        	}else{
	                                        		return "Not Reviewed";
	                                        	} */return source[5];
			                                  }},    
			                          {    "sTitle":'Documents',
			                                        "aTargets": [ 11], "mDataProp": function ( source, type, val ) {
			                                        	/* if(source[6]==0 && source[18] >0 ){
			                                        		return "Reviewed";
			                                        	}else{
			                                        		return "Not Reviewed";
			                                        	} */return source[6];
			                                  }},       
			                                  
			                          {   "sTitle":'Donor Type',
			                                  "aTargets": [ 12], "mDataProp": function ( source, type, val ) {
			                                      return source[7];
			                              }},
			                          {    "sTitle":'<s:text name="stafa.label.plannedprocedure"/>',
			                                  "aTargets": [ 13], "mDataProp": function ( source, type, val ) {
			                                	  link = "<a href='#' onclick='javascript:getDonorDetails(\""+source[0] +"\",\""+source[1]+"\");'>"+ source[8] +"</a>";
			                                      return link;
			                              }},
			                          {    "sTitle":'<s:text name="stafa.label.nurse"/>',
			                                  "aTargets": [ 14], "mDataProp": function ( source, type, val ) {
			                                      return source[9];
			                              }},
			                              {    "sTitle":'Status',
			                                  "aTargets": [ 15], "mDataProp": function ( source, type, val ) {
			                                	  if(source[16] == 'Pending' && source[5]=='Reviewed' && source[6]=='Reviewed'){
			                                		  return "Pending Assignment"
			                                	  }else{
			                                      	return source[16];
			                                	  }
			                              }} 
			                         
			                        ];

	var donorListTable_aoColumn = [ { "bSortable": false},
	                               	  null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null
			             ];
	//?BB var column_sort = [[ 1, "asc" ]];
	var column_sort = [];
	var reagentsTable_MergeCols = mergedCols;
	var donorListTable_ColManager = function(){
										$("#donordetails").html($('.ColVis:eq(0)'));
									 };
	var sDomVal = 'C<"clear">Rlfrtip';
									//constructTable(flag,'donordetailsTable',donorListTable_ColManager,donorListTable_serverParam,donorListTable_aoColumnDef,donorListTable_aoColumn);
									 constructTableWithMerge(flag,"donordetailsTable",donorListTable_serverParam,donorListTable_aoColumnDef,donorListTable_aoColumn,column_sort,reagentsTable_MergeCols,donorListTable_ColManager,sDomVal);
}

$(document).ready(function(){
	//alert("ready");
	
	hide_slidewidgets();
	hide_slidecontrol();
	hide_innernorth();
	constructDonorList(false);
	// available in common scripts
	
	$("select").uniform(); 

	
});






function clearDropDown(id){
	$("#"+id).val("");
	 $.uniform.restore("#"+id);
		$("#"+id).uniform();
}



function setScanFlag(type){
	$("#scanType").val(type);
}



var scanPerson;
function getScanData(event,obj){
  	if(event.which==13){
  		var donorType= $("#donorType").find('option:selected').text();
   	var scanType = $("#scanType").val();

      if((scanType=="recipient" || scanType=="RECIPIENT" ) && ( donorType != "AUTO")) { 
    	  resetRecipientData();
    	  }else if((scanType=="donor" || scanType=="DONOR" )&&( donorType !="AUTO")){ 
    		  resetDonorData();
    		  }else if(donorType =="AUTO"){ 
    		  resetDonorData();
    		  resetRecipientData();
    		  }
      
      //alert("!"+scanType)
      searchPerson(scanType);
      $("#scanType").val("");
    }
}


function searchPerson(type){
	//alert("searchPerson 2");
	try{
	
	//alert("trace1");
	var search_aoColumn = [ { "bSortable": false},
	                                null,
						               null,
						               null,
						               null,
						               null,
						               null,null
			             ];
	var criteria = " ";
	var mrnFlag = false;
	var ssnFlag = false;
	var mrnCriteria,ssnCriteria;
	var donorType;
	if(type=="DONOR"){
		$("#donorMRN").val($("#divDonorScan #fake_conformProductSearch").val() );
		criteria = " and d.pk_donor = (select max(pk_donor) from donor where fk_person = p.pk_person)  and pk_person=fk_person ";
		if($("#donorMRN").val()!=""){
			mrnCriteria= " lower(PERSON_MRN) like lower('%" + $("#donorMRN").val() +"%')";
			mrnFlag = true;
		}
		donorType= $("#donorType").find('option:selected').text();
		if(donorType=="Auto" || donorType=="AUTO"){
			$("#recipientMRN").val($("#divDonorScan #fake_conformProductSearch").val() );	
		}
	/*	if($("#donorSSN").val()!=""){
			ssnCriteria= " lower(PERSON_SSN) like lower('%" + $("#donorSSN").val() +"%')";
			ssnFlag = true;
		};
	*/	var conditionFlag = false;
		if(mrnFlag || ssnFlag){
			criteria += " and (";
			if(mrnFlag){
				criteria += mrnCriteria;
				conditionFlag = true;
			}
			if(ssnFlag){
				if(conditionFlag){
					criteria += " or ";
				}
				criteria += ssnCriteria;
			}
			criteria += ")";
		}
	}else{
		//alert("Recipient criteria");
		donorType= $("#donorType").find('option:selected').text();
		//alert("donorType"+donorType);
		//alert("mrn"+$("#divRecipientScan #fake_conformProductSearch").val());
		$("#recipientMRN").val($("#divRecipientScan #fake_conformProductSearch").val() );
		if(donorType=="Auto" || donorType=="AUTO"){
			$("#donorMRN").val($("#divRecipientScan #fake_conformProductSearch").val() );	
		}
		criteria = " and pk_person=fk_receipent ";
		if($("#recipientMRN").val()!=""){
			mrnCriteria= " lower(PERSON_MRN) like lower('%" + $("#recipientMRN").val() +"%')";
			mrnFlag = true;
		}
		if($("#recipientSSN").val()!=""){
			ssnCriteria= " lower(PERSON_SSN) like lower('%" + $("#recipientSSN").val() +"%')";
			ssnFlag = true;
		};
		var conditionFlag = false;
		if(mrnFlag || ssnFlag){
			criteria += " and (";
			if(mrnFlag){
				criteria += mrnCriteria;
				conditionFlag = true;
			}
			if(ssnFlag){
				if(conditionFlag){
					criteria += " or ";
				}
				criteria += ssnCriteria;
			}
			criteria += ")";
		}
	}
	var searchDonor_serverParam = function ( aoData ) {

		aoData.push( { "name": "application", "value": "stafa"});
		aoData.push( { "name": "module", "value": "addDonor_PersonSearch"});	
		aoData.push( { "name": "criteria", "value": criteria});	
		
		aoData.push( {"name": "col_1_name", "value": "" } );
		aoData.push( { "name": "col_1_column", "value": ""});
		
		aoData.push( { "name": "col_2_name", "value": ""});
		aoData.push( { "name": "col_2_column", "value": ""});
													
		};
	
	var searchDonor_aoColumnDef = [
  
				{"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
					link ="<input type='radio' name='donorSelect' id='donorSelect' onclick='javascript:setDonorDetails(\""+source[13]+"\",\""+source[1]+"\",";
					link+="\""+source[2]+"\",\""+source[3]+"\",\""+source[4] +"\",\""+source[5] +"\",\""+source[7]+"\",\""+source[10] +"\",\""+source[11]+"\",\""+source[12] +"\",\""+source[16] +"\",\""+source[17] +"\",\""+source[18] +"\",\""+source[19] +"\",\""+source[20] +"\",\""+source[21] +"\",\""+source[22] +"\");'/>";
					
				return link;
				}},
				{    "sTitle":'<s:text name="stafa.label.donorname"/>',
				 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
					 link = source[1] + " "+ source[2];
				return link;
				}},
				{    "sTitle":'<s:text name="stafa.label.donordob"/>',
				 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
					 link = source[3];
				     return link;
				 
				    }},
				{    "sTitle":'Donor MRN',
				        "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
				    return source[4];
				  }},
				  {    "sTitle":'Donor SSN',
				        "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
				    return source[5];
				  }} ,
				{    "sTitle":'<s:text name="stafa.label.donorabo"/>',
				        "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
				    return source[6];
				  }},    
				{    "sTitle":'Language Spoken',
				        "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
				    return source[8];
				  }},
     				{    "sTitle":'Source',
					  "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
					     return "STAFA";
  				  }}        
							
				];
	
	var searchRecipient_aoColumnDef = [	
	                               
	               				{"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
	               					link ="<input type='radio' name='recipientSelect' id='recipientSelect' onclick='javascript:setRecipientDetails(\""+source[14]+"\",\""+source[1]+"\",";
	               					link+="\""+source[2]+"\",\""+source[3]+"\",\""+source[4] +"\",\""+source[5] +"\",\""+source[7]+"\",\""+source[10] +"\",\""+source[11]+"\",\""+source[15] +"\",\""+source[16] +"\",\""+source[17] +"\",\""+source[18] +"\",\""+source[19] +"\",\""+source[20] +"\",\""+source[21] +"\",\""+source[22] +"\");'/>";
	               					
	               				return link;
	               				}},
	               				{    "sTitle":'Recipient Name',
	               				 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
	               					 link = source[1] + " "+ source[2];
	               				return link;
	               				}},
	               				{    "sTitle":'<s:text name="stafa.label.recipientdob"/>',
	               				 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
	               					 link = source[3];
	               				     return link;
	               				 
	               				    }},
	               				{    "sTitle":'Recipient MRN',
	               				        "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
	               				    return source[4];
	               				  }},
	               				  {    "sTitle":'Recipient SSN',
	               				        "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
	               				    return source[5];
	               				  }} ,
	               				{    "sTitle":'<s:text name="stafa.label.donorabo"/>',
	               				        "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
	               				    return source[6];
	               				  }} ,
	               				{    "sTitle":'Language Spoken',
	            				        "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
	            				    return source[8];
	            				  }},
		               				{    "sTitle":'Source',
	               					  "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
	             					     return "STAFA";
	               				  }}      
	               							
	               				];	
	
	//alert("type " + type);
//?BB	var adtData = getADTData($("#donorMRN").val());
	
	var searchTableObj;
	
	if(type=="DONOR"){
		$("#donorResults").dataTable().fnDestroy();
		searchTableObj = constructTableWithoutColManager(true,'donorResults',searchDonor_serverParam,searchDonor_aoColumnDef,search_aoColumn);
		//alert("obj" + $('#modelPopup').find('#donorResults').html());
		//constructTableObjectWithoutColManager(true,$('#modelPopup').find('#donorResults'),searchDonor_serverParam,searchDonor_aoColumnDef,search_aoColumn);
		
		var emptyCheckClass= $("#donorResults tbody tr td:nth-child(1)").attr("class");	
		//alert("emptyCheckClass " + emptyCheckClass);
		
		if(emptyCheckClass=="dataTables_empty"){
			var tempDonorMrn=$("#donorMRN").val();
			getADTData(tempDonorMrn,"donorResults");
			$(".donorResultsTR").removeClass("hidden");
			//$(".donortr").addClass("hidden");
			 //$("#donorMRN").val($("#divDonorScan #fake_conformProductSearch").val() );
		}else{
			$(".donorResultsTR").removeClass("hidden");
			//$(".donortr").addClass("hidden");
		}
		
		$(".donorResultsTR").find("#donorResults").removeAttr("style");
		
		//$(".donortr").removeClass("hidden");
	}else if(type=="RECIPIENT"){
		
		$("#recipientResults").dataTable().fnDestroy();
		searchTableObj = constructTableWithoutColManager(true,'recipientResults',searchDonor_serverParam,searchRecipient_aoColumnDef,search_aoColumn);
		//constructTableObjectWithoutColManager(true,$('#modelPopup').find('#recipientResults'),searchDonor_serverParam,searchRecipient_aoColumnDef,search_aoColumn);
		var emptyCheckClass= $("#recipientResults tbody tr td:nth-child(1)").attr("class");	
		
		if(emptyCheckClass=="dataTables_empty"){
			var tempRecipientMrn=$("#recipientMRN").val();
			getADTData(tempRecipientMrn,"recipientResults");
			//$(".recipientResultTr").addClass("hidden");
			$(".recipientResultTr").removeClass("hidden");
		}else{
			$(".recipientResultTr").removeClass("hidden");
		}
		
		$(".recipientResultTr").find("#recipientResults").removeAttr("style");
		
	}
	}catch(err){
		alert("error " + err);
	}
}


function setDonorDetails(var1,var2,var3,var4,var5,var6,var7,var8,var9,var10,var11,var12,var13,var14,var15,var16,var17,sex,lang){
	$("#donorPerson").val(var1);
	$("#donorLastName").val(var2);
	$("#donorFirstName").val(var3);
	$("#donorDob").val(var4);
	$("#donorMRN").val(var5);
	$("#donorSSN").val(var6);
	$("#donorabo").val(var7);
	$("#physicianID").val(var8);
	$("#donorWeight").val(var9);
	$("#donorHeight").val(var10);
	$("#donorAbs").val(var11);
	$("#donorUnitsHeight").val(var13);
	$("#donorUnitsWeight").val(var14);
	$("#donorMiddleName").val(var16);
	$.uniform.restore('select');
	$("select").uniform(); 	
	$("#donorResults").dataTable().fnDestroy();
	$(".donorResultsTR").addClass("hidden");
	$("#donorGender").val($("#gender option:contains('" + $.trim(sex) + "')").val());
	$("#donorPrilang").val($("#language option:contains('" + $.trim(lang) + "')").val());
	
	//?BB $(".donortr").removeClass("hidden");
	var donorType= $("#tmpdonorType").val(); 
	//alert("donorType>" + donorType+"<");
	if(donorType=="1" ){
		setRecipientDetails(var1,var2,var3,var4,var5,var6,var7,var8,var9,var10,var11,var12,var13,var14,var15,var16,var17,sex,lang);
	}
}

function setRecipientDetails(var1,var2,var3,var4,var5,var6,var7,var8,var9,var10,var11,var12,var13,var14,var15,var16,var17,sex,lang){
	$("#recipientPerson").val(var1);
	$("#recipientLastName").val(var2);
	$("#recipientFirstName").val(var3);
	$("#recipientDob").val(var4);
	$("#recipientMRN").val(var5);
	$("#recipientSSN").val(var6);
	$("#recipientMiddleName").val(var17);
	$("#recipientabo").val(var7);
	$("#transplantPhysician").val(var8);
	$("#recipientWeight").val(var9);
	$("#recipientAbs").val(var12);
	$("#recipientUnitsWeight").val(var15);
	$("#diagnosis").val(var10);
	//
	$.uniform.restore('select');
	$("select").uniform(); 	
	$("#recipientResults").dataTable().fnDestroy();
	$(".recipientResultTr").addClass("hidden");
	$(".recipientTr").removeClass("hidden");
	$("#recipientGender").val($("#gender option:contains('" + $.trim(sex) + "')").val());
	$("#recipientPrilang").val($("#language option:contains('" + $.trim(lang) + "')").val());
	//alert("donorType"+donorType);
	var donorType= $("#tmpdonorType").val(); 
	if(donorType=="1" ){
		setAutoDonorDetails();
	}
}


var deletedflag=false;
function delete_Donor(){
	// alert("Delete Rows");
   deletedflag=false;
	var deletedIds = "";
	if(checkModuleRights(STAFA_DONORLIST,APPMODE_DELETE)){
		try{
			$("#donordetailsTable tbody tr").each(function (row){
				 if($(this).find("#donorCheck").is(":checked")){
			//?BB not working in IE $("#donordetailsTable #donorCheck:checked").each(function (row){
			//	alert("inside for");
			//	deletedIds += $(this).closest("tr").find("#plannedProcedure").val() + ",";
			deletedIds += $(this).find("#plannedProcedure").val() + ",";
				deletedflag=true;
			}
		});
		if(deletedIds.length >0){
		  deletedIds = deletedIds.substr(0,deletedIds.length-1);
		}
		if(deletedflag!=false){
			 var yes=confirm(confirm_deleteMsg);
				if(!yes){
					return;
				}
		
		jsonData = "{deletedIds:'"+deletedIds+"'}";
		url="deleteDonorList";
	    response = jsonDataCall(url,"jsonData="+jsonData);
	    constructDonorList(true);
		}
		if(deletedflag==false){
			alert("Please Select the Record");
		}
		deletedflag=false;
	   // constructReagentsTable(true,donorId);
		}catch(error){
			alert("error " + error);
		}
	}
}
</script>






 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header addnew">Current Donor List</div>
			<div class="portlet-content">
			 <form id="aphresis" name="aphresis">
			 <span class="hidden">
			 <span id="procedureSpan">
			 	<s:select id="procedure"  name="procedure" list="lstPlannedProcedure" listKey="protocol" listValue="protocolName"  headerKey="" headerValue="Select"/>
  			</span></span>
  				<input type="hidden" id="donor" name="donor" value=""/>
  				<input type="hidden" id="donorProcedure" name="donorProcedure" value=""/>
  				
  			</form>	<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  style="width:16;height:16;cursor:pointer;" onclick="add_Donor('donordetailsTable');"/>&nbsp;&nbsp;<label class="cursor"  onclick="add_Donor('donordetailsTable');"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="delete_Donor('donordetailsTable');"/>&nbsp;&nbsp;<label class="cursor" onclick="delete_Donor('donordetailsTable');"><b>Delete</b></label>&nbsp;</div></td>
				    <td style="width:20%"><div><img src = "images/icons/edit.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="edit_Donor('donordetailsTable');"/>&nbsp;&nbsp;<label class="cursor" onclick="edit_Donor('donordetailsTable');"><b>Edit</b></label>&nbsp;</div></td>
				    <td style="width:20%"><div><img src = "images/icons/save3.png" class="cursor" style="width:16;height:16;" class="cursor" onclick="save_Donor('donordetailsTable');" >&nbsp;&nbsp;<label class="cursor" onclick="save_Donor('donordetailsTable')"><b>Save</b></label></div></td>
				</tr>
			</table>
			<div style="overflow-x:auto;overflow-y:hidden">
			<table cellpadding="0" cellspacing="0" border="1" id="donordetailsTable" class="display" >
				<thead>
				   <tr>
				   		<th width="4%" id="donordetails"></th>
						<th >Donor MRN</th>
						<th >Donor Name</th>
						<th >Donor DOB</th>
						<th>Donor ABO/Rh</th>
						<th>Recipient MRN</th>
						<th>Recipient Name</th>
						<th>Recipeint DOB</th>
						<th>Recipeint ABO/Rh</th>
						<th>Diagnosis</th>
						<th >Lab Result</th>
						<th >Documents</th>
						<th >Donor Type</th>
						<th >Planned Procedure</th>
						<th >Nurse</th>
						<th >Status</th>
					</tr> 
				</thead>
				<tbody>
				 	
				</tbody>
			</table>
			</div>
	</div>
	</div>	
</div>

<script>
$(function() {
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true, changeYear: true, yearRange: '1900:' + new Date().getFullYear(), maxDate: "+1m +1w"});
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	/*
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
    .append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span>");
	
	
	$( ".portlet-header .ui-addnew" ).click(function() {
		 jQuery("#addNewDonor").dialog("destroy");
		 showPopUp("open","addNewDonor","Add New Donor","900","700");
		//createPopUp("open","addNewDonor","Add New Donor","900","700");
	   // addNewPopup("Document Review","addNewDonor","openDocumentReview","800","800");
	});
	*/
	/**For Notes**/
	/* 
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='ui-addnew'></span>");
	
	$( ".portlet-header .ui-addnew" ).click(function() {
		addNewPopup("Add New Product","SupervisoraddNewDialog","addnewAcquisition","700","1100");
	});
	 */
	
	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});


</script>