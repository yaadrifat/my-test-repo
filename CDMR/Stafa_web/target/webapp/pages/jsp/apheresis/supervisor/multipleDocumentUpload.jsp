<%@ taglib prefix="s" uri="/struts-tags" %>
<% 
	String path = request.getContextPath()+"/pages/";
	System.out.println("path:::"+path);
%>
<html>
<head>
<style>

#pro{  width:0px;height:5px;background-color:blue;}
.input {  border: 0px solid #006;background-color:white;cursor:pointer;cursor:hand;text-decoration:underline;color:blue}
.hide_attach{  height:10px; width:10px;}

</style>

<title>FILE UPLOAD</title>     
<script type="text/javascript" src="<%=path%>js/jquery/jquery-1.6.3.js"></script>
<script type="text/javascript" src="<%=path%>js/jquery/jquery.ui.core.js"></script>
<script type="text/javascript" src="<%=path%>js/jquery/jquery.ui.widget.js"></script>
<script type="text/javascript" src="<%=path%>js/jquery/jquery-ui-1.8.7.custom.min.js"></script>

<link href="<%=path%>css/ui/jquery-ui-1.8.7.custom.css" rel="stylesheet" type="text/css">
<link type="text/css" href="<%=path%>css/themes/grey.css" rel="stylesheet" />
<link type="text/css" href="<%=path%>css/main_style.css" rel="stylesheet" />
<link type="text/css" href="<%=path%>css/fonts.css" rel="stylesheet" />
 <link type="text/css" href="<%=path%>css/portlet.css" rel="stylesheet" /> 
<link type="text/css" href="<%=path%>css/ui/base/jquery.ui.all.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<%=path%>css/demo_table.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=path%>css/demo_page.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=path%>css/ui/jquery-ui.css">
<script type="text/javascript" src="<%=path%>js/commonScript.js"></script>
<!--  html5 js  for  supporting IE8 broswer-->
<script type="text/javascript" src="<%=path%>js/jquery/modernizr.js"></script>
<script type="text/javascript" src="<%=path%>js/jquery/html5shiv.js"></script>
<script type="text/javascript" src="<%=path%>js/jquery/html5shiv-printshiv.js"></script>
<!-- html js ends -->


<script type="text/javascript" src="<%=path%>js/jquery/jquery.dataTables.js"></script>
<link rel="Stylesheet" type="text/css" href="<%=path%>css/dataTable.extend.css" />
<script type="text/javascript" src="<%=path%>js/jquery/jquery.uniform.js"></script>
  <link rel="stylesheet" href="<%=path%>css/uniform.default.css" type="text/css" media="screen">
   <script class="include" language="javascript" type="text/javascript" src="<%=path%>js/jquery/columManager/jquery.cookie.js"></script>
  <!--<script class="include" language="javascript" type="text/javascript" src="<%=path%>js/jquery/columManager/jquery.columnmanager.min.js"></script>-->
  <script class="include" language="javascript" type="text/javascript" src="<%=path%>js/jquery/jquery.columnmanager.js"></script>
  <script class="include" language="javascript" type="text/javascript" src="<%=path%>js/jquery/columManager/jquery.clickmenu.js"></script>
  
  <link rel="Stylesheet" type="text/css" href="<%=path%>css/columManager/clickmenu.css" />
  <link rel="Stylesheet" type="text/css" href="<%=path%>css/columManager/columnManagerStyle.css" />
  <!-- Column Filter Css/JavaScripts Start -->
<script class="include" language="javascript" type="text/javascript" src="<%=path%>js/jquery/columnfilter.js"></script>
<!-- Column Filter Css/JavaScripts End -->
<script type="text/javascript" src="<%=path%>js/globalMessages.js"></script>
<script type="text/javascript" src="<%=path%>js/jquery.layout.js"></script>
<script type="text/javascript" src="<%=path%>js/modalDialogHeightWidth.js"></script>

<link rel="stylesheet" type="text/css" href="<%=path%>css/ui/base/jquery.ui.tabs.css" media="screen" />

<script type="text/javascript">

 

  function save_documentReview(){
	  var attachedFrame = $("#attachedFile");
	  //$(attachedFrame).parent
		var frm = $("#attachedDocsForm");
	    jQuery(frm).attr('action', "uploadDocument");
	    jQuery(frm).attr('method', 'POST');
	    jQuery(frm).attr('target', "attachedFile");
	    jQuery(frm).attr('accept-charset', "UTF-8");
	    if(frm.encoding)
	    {
	        //alert("s");
	        jQuery(frm).attr('encoding', 'multipart/form-data');                 
	    }
	    else
	    {   
	        //alert("s1");
	        jQuery(frm).attr('enctype', 'multipart/form-data');           
	    }           
	    jQuery(frm).submit();	
	}


  var attachFileId=0;
  var dateId=0;
  function add_documentReview(){
      var newRow ="<tr>"+
      "<td><input type='hidden' id='documentRef' name='documentRef'/></td>"+
      "<td><input type='text' id='documentTitle' name='documentTitle'/></td>"+
      "<td><input type='text' id='documentDate"+dateId+"' name='documentDate' class='dateEntry'/></td>"+
      "<td><input type='file' name='fileUpload' id='attachedFile"+attachFileId+"'/></td>"+
      "<td><input type='checkbox' id='reviewed' name='reviewed'/></td>"+
      "</tr>";
      attachFileId=attachFileId+1;
      $("#documentreviewTable tbody").prepend(newRow);
      $("#documentDate"+dateId).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
          changeYear: true, yearRange: "c-50:c+45"});
      
  }
 
 //?BB not in use 
/*
  function showDocument(docId){
	    alert( " docId " + docId);
	    //alert("contextpath : "+contextpath);
	    //var url = cotextpath+"/viewdocument.action?docId="+docId;
	    var contextpath = "<%=request.getContextPath()%>";
	    var url = contextpath+"/viewdocument.action?docId="+docId;
	    //alert(url);
	    window.open( url, "Documents", "status = 1, height = 500, width = 800, resizable = 0" );

	}
  */
  function delete_documentReview(){
  	
  //  alert("Delete Rows");
      var yes=confirm(confirm_deleteMsg);
  	if(!yes){
  		return;
  	}
  	  var deletedIds = ""
  	  $("#documentreviewTable #documentId:checked").each(function (row){
  			//alert($(this).val());
  			deletedIds += $(this).val() + ",";
  		 });
  	  if(deletedIds.length >0){
  		  deletedIds = deletedIds.substr(0,deletedIds.length-1);
  	  }
  	//  alert("deletedIds " + deletedIds);
  	  jsonData = "{deletedIds:'"+deletedIds+"'}";
  	  url="deleteDocumentReview";
          //alert("json data " + jsonData);
       response = jsonDataCall(url,"jsonData="+jsonData);
  	//?BB reload table 	
  }
  function constructDocumentTable(flag,donor,type,tableId) {
	//alert("construct table : "+tableId+",Donor : "+donor);
		var criteria = "";
		if(donor!=null && donor!=""){
			criteria = " and fkEntity ="+donor ;
		}
	//alert(criteria);
		elementcount =0;
		//return;
	 	if(flag || flag =='true'){
	 		oTable=$('#'+tableId).dataTable().fnDestroy();
		}
		  var link ;
			try{
				oTable=$('#'+tableId).dataTable( {
					"sPaginationType": "full_numbers",
					"bProcessing": true,
					"bServerSide": true,
					"bRetrieve" : true,
					"sDom": 'C<"clear">Rlfrtip',
					"sScrollX":"110%",
					 "bDestroy": true,
					 "aaSorting": [[ 1, "asc" ]],
					 "aoColumns": [ { "bSortable": false},
					                	null,
					                	null,
					                	null,
					                	null
						             ],
					"oColVis": {
								"aiExclude": [ 0 ],
								"buttonText": "&nbsp;",
								"bRestore": true,
								"sAlign": "left",
								"iOverlayFade": 25     /* To Overcome the visibility while clicking the addButton*/
						},						               
						"oLanguage": {"sProcessing":""},									
					"fnServerParams":  function ( aoData ) {
													aoData.push( { "name": "application", "value": "stafa"});
													aoData.push( { "name": "module", "value": "aphresis_DocumentReview"});
													aoData.push( { "name": "criteria", "value": criteria});
													
													aoData.push( {"name": "col_1_name", "value": "lower(documentFileName)" } );
													aoData.push( { "name": "col_1_column", "value": "lower(documentFileName)"});
													
													aoData.push( { "name": "col_2_name", "value": "lower(documentUploadedDate)"});
													aoData.push( { "name": "col_2_column", "value": "lower(documentUploadedDate)"});
																
													aoData.push( {"name": "col_3_name", "value": "lower(documentContentType)" } );
													aoData.push( { "name": "col_3_column", "value": "lower(documentContentType)"});
								},
					"sAjaxSource": 'dataSearch.action',
					"sAjaxDataProp": "filterData",
					"aoColumnDefs": [
					                  {		
					                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
				                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
				                	 			return "";
				                	     }},
					                  { "sTitle": "Documnet Title",
				                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
					                	    	 //alert('docTitle'+source.documentFileName); 
					                	 		return source[1];
					                	 }},
									  { "sTitle": "Document Date",
					                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
											    return source[2];
											    }},
									  { "sTitle": "Attachment",
											 "aTargets": [3], "mDataProp": function ( source, type, val ) {
										 	   return "<a href='#' onclick='window.parent.openPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\")'> View </a>";
									  		}},
									  { "sTitle": "Reviewed",
									  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
									  			 if(source[3] ==1){
									  				 return "Reviewed";
									  			 }else{
									  				 return "";
									  			 }
										  		//return "<input type='checkbox'/>";
										  }}
									],
						"fnInitComplete": function () {
												$("#documentreviewColumn").html($('.ColVis:eq(2)'));
							},
						"fnServerData": function ( sSource, aoData, fnCallback ) {
							$('.progress-indicator').css( 'display', 'block' );
							$.ajax( {
									"dataType": 'json',
									"type": "POST",
									"url": 'dataSearch.action',
									"data":aoData,
									"success":fnCallback,  
									"complete": function () {
												$('.progress-indicator').css( 'display', 'none' );
												$("select").uniform();
												}
								  } );},
						"fnDrawCallback": function() { 
							//alert("fnDrawCallback");
							//$('.progress-indicator').css( 'display', 'none' );
				      }
					} );
				jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = parseInt(no_of_pages);
				}catch(err){
					alert("error " + err);
				}
				//alert("end of dt");
	} 
var donorVal;
var enType;
  $(document).ready(function(){
	 // alert("sub ready");
	  var docs = $(window.parent.document);
	  //alert("donor : "+$(docs).find("#donor").val());
	  $("#donor").val($(docs).find("#donor").val());
	  donorVal=$("#donor").val();
	  enType=$("#entityType").val();
	  //alert(donorVal);alert(enType);
	  constructDocumentTable(false,donorVal,enType,'documentreviewTable');
	  /*$('#documentreviewTable').dataTable({
	        "bJQueryUI": true
	    });*/
	  $("select").uniform();
	     
	  });
  </script>
</head> 
<body>
    <div>
				<div id="tabs">
				    <ul >
				     <li><a href="#tabs-1">Current</a></li>
				      <li><a href="#tabs-2">Historic</a></li>
				    </ul>
			  <div id="tabs-1">   
				
				<table>
				<tr>
					<td style="width:20%"><div><img src = "../../../images/icons/addnew.jpg" class="cursor" id="" onclick="add_documentReview()"/>&nbsp;&nbsp;<label  class="cursor" onclick="add_documentReview()"><b>Add </b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "../../../images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="delete_documentReview()" />&nbsp;&nbsp;<label class="cursor" onclick="delete_documentReview()"><b>Delete</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "../../../images/icons/save3.png" style="width:16;height:16;" class="cursor" id ="" onclick="save_documentReview();" >&nbsp;&nbsp;<label id="schild" class="cursor" onclick="save_documentReview();"><b>Save</b></label>&nbsp;</div></td>
				</tr>
				</table>
				<s:form id="attachedDocsForm" name="attachedDocsForm" >
				<s:hidden id="donor" name="donor" value="%{donor}"/>
				<s:hidden id="entityType" name="entityType" value="DONOR"/>
				<table cellpadding="0" cellspacing="0" border="0" id="documentreviewTable" class="display">
					<thead>
						<tr>
							<th width="4%" id="documentreviewColumn"></th>
							<th>Document Title</th>
							<th>Document Date</th>
							<th>Attachment</th>
							<th>Reviewed</th>
						</tr>
					</thead>
				</table>
				</s:form>
				</div>	
			<div id="tabs-2">
			  Historic documents
			  <table cellpadding="0" cellspacing="0" border="0" id="documentreviewTable1" class="display">
					<thead>
						<tr>
							<th width="4%" id="documentreviewColumn1"></th>
							<th>Document Title</th>
							<th>Document Date</th>
							<th>Attachment</th>
							<th>Reviewed</th>
						</tr>
					</thead>
				</table>
			</div>	
			</div>
</div>

<script type="text/javascript">
//alert("tab");
try{
$("#tabs" ).tabs();
$("#tabs ul").removeClass("ui-widget-header");
}catch(err){
	alert("error " + err);
}
//alert("end of tab");
$('#tabs').bind('tabsshow', function(event, ui) {
	alert("show" + ui.index + " / " + plot1._drawCount + " / " + plot2._drawCount);
    /*if (ui.index == 0 && plot1._drawCount == 0) {
    	//alert("show" + ui.index);
      plot1.replot();
    }else if (ui.index === 1 && plot2._drawCount === 0) {
    	//alert("show" + ui.index);
        plot2.replot();
    }else if (ui.index === 2 && plot3._drawCount === 0) {
    	//alert("show" + ui.index);
        plot3.replot();
    }else if (ui.index === 3 && plot4._drawCount === 0) {
    	//alert("show" + ui.index);
        plot4.replot();
    }
   */
  });

</script>
</body>
</html>
