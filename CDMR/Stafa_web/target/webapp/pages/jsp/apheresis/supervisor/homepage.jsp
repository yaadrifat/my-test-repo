<!DOCTYPE html>
<html >
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type"/>
<meta content="utf-8" http-equiv="encoding"/>
<!--<meta http-equiv="X-UA-Compatible" content="IE=10;FF=3;OtherUA=4" /> -->
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7;IE=EmulateIE8; IE=EmulateIE9"/>
<style>
.buttonActive{
background:	#70B8FF;
border-radius:10px;
}
</style>

</head>

<%@ taglib prefix="s" uri="/struts-tags" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script type="text/javascript" src="js/dataSearch.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/monthPicker.js"></script>
<Style>
</Style>


<body>

<script type="text/javascript">


function delete_Inbox(){
	if(checkModuleRights(STAFA_HOMEPAGE,APPMODE_DELETE)){
		var deletedIds="";
		$("#inboxTable tbody tr").each(function (row){
			 if($(this).find("#inboxid").is(":checked")){
	//$("#inboxTable #inboxid:checked").each(function (row){
		// alert("inside if");
			deletedIds += $(this).find("#inboxid").val() + ",";
			}
		 });
		//	alert("deletedIds "+ deletedIds);
		  if(deletedIds.length >0){
			 var yes=confirm(confirm_deleteMsg);
						if(!yes){
							return;
						}
			  deletedIds = deletedIds.substr(0,deletedIds.length-1);
			  //alert("deletedIds " + deletedIds);
			  jsonData = "{deletedIds:'"+deletedIds+"',domainName:'NOTIFICATION_DOMAIN',domainKey:'NOTIFICATION_DOMAINKEY'}";
			  url="commonDelete";
		        //alert("json data " + jsonData);
		     response = jsonDataCall(url,"jsonData="+jsonData);
		     constructTable(true,'inboxTable',inboxTable_ColManager,inboxTable_serverParams,inboxTable_aoColumnDef,inboxTable_aoColumn);
		 	
		  }
	}
}

function  weekchangeEvent(){
	//alert("update");
	var startDt = $("#startDate").text();
	var endDt = $("#endDate").text();
	
	//alert(" startDt " + startDt + " / " + endDt )
	url="fetchSupervisorMonitorData";
	response = jsonDataCall(url,"jsonData={startDate:'"+startDt+"',endDate:'"+endDt+"'}");
	//alert("response " + response);
	
	
	 $("#donorData").val(response.donorData);
	 $("#nurseData").val(response.nurseData);
	 $("#productData").val(response.productData);
	 $("#ticksData").val(response.ticksData);
	 
	 showGraph($("#graphType").val());
	   
		

		
		
}

/* Inbox Table Server Parameters Starts */
 
var inboxTable_ColManager = function(){
											$("#inboxTableColumn").html($('#inboxTable_wrapper .ColVis'));
										};
var inboxTable_aoColumn = [ { "bSortable": false},
                            { "bSortable": false},
				               null,
				               null,
				               null
	            		  ];	
var inboxTable_serverParams = function ( aoData ) {
												aoData.push( { "name": "application", "value": "stafa"});
												aoData.push( { "name": "module", "value": "supervisor_inbox"});
												
												//aoData.push( {"name": "col_1_name", "value": "" } );
												//aoData.push( { "name": "col_1_column", "value": ""});
												
												aoData.push( {"name": "col_2_name", "value": "lower(NOTIFY_FROM)" } );
												aoData.push( { "name": "col_2_column", "value": "lower(NOTIFY_FROM)"});
												
												aoData.push( {"name": "col_3_name", "value": "lower(NOTIFY_CONTENT)" } );
												aoData.push( { "name": "col_3_column", "value": "lower(NOTIFY_CONTENT)"});
												
												aoData.push( {"name": "col_4_name", "value": "lower(nvl(to_char(CREATED_ON,'Mon dd, yyyy'),' '))" } );
												aoData.push( { "name": "col_4_column", "value": "CREATED_ON"}); 
											};
var inboxTable_aoColumnDef =  [
	  	                         {		 "aTargets": [ 0], "mDataProp": function ( source, type, val ) {
		                             		 return "";
		                             }},
		                          {    "sTitle":'Select',
		                                 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
		                                	 return "<input name='inboxid' id= 'inboxid' type='checkbox' value='"+source[0]+"' />"; 
		                             }},
		                          {    "sTitle":'Name',
		                                 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
				                             return source[1];
		                             }},
	                              {    "sTitle":'Subject',
	                                     "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
			                                 return source[2];
	                                 }},
                                  {    "sTitle":'Date/Time',
                                         "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
		                                     return source[3];
                                        	// return source.createdOn;
                                     }}
	                          ];
function showCalendar(){
	                   	 if($(".week-picker").hasClass("block")){
	                 			$(".week-picker").removeClass("block");
	                 			$(".week-picker").addClass("hidden");
	                 		}else{
	                 			$(".week-picker").removeClass("hidden");
	                 			$(".week-picker").addClass("block");	
	                     	}
	                     }     
$(document).ready(function(){
	//$("#chart1").html("");
	hide_slidecontrol();
	hide_slidewidgets();
	hide_innernorth();   
	
	 constructTable(false,'inboxTable',inboxTable_ColManager,inboxTable_serverParams,inboxTable_aoColumnDef,inboxTable_aoColumn);
	// showGraph('Product');
	$("select").uniform();
/* 	$('#monthView').bind('click', function () {
	    $('.monthpicker').monthpicker('show');
	}); */	
	//$("#graphType").val("Product");
	
	
});

function monthView(){
	$("#mode").val("month");
	$("#weekView").removeClass("buttonActive");
	$("#monthView").addClass("buttonActive");
	$(".week-picker").addClass("hidden");
	$('.monthpicker').monthpicker();
	$('.monthpicker').monthpicker('show');
}
function weekView(){
	$("#mode").val("week");
	$("#monthView").removeClass("buttonActive");
	$("#weekView").addClass("buttonActive");
	$("#monthpicker").removeClass("block");
	$("#monthpicker").addClass("hidden");
	showCalendar();
}
function modeChange(){
	var mode=$("#mode").val();
	if(mode=="week"){
		weekView();	
	}else if(mode=="month"){
		monthView();
	}
}
function showArchiveList(){
	$("#homePageDiv").addClass('hidden');
	$("#archiveDonorDiv").removeClass('hidden');
	constructTable(false,'archivedDonorTable',archiveDonor_ColManager,archiveServerParams,archive_aoColumnDef,archive_aoColumn);
	$("select").uniform();
}
function showValidationSheet(){
	$("#homePageDiv").addClass('hidden');
	//$("#validationDataDiv").removeClass('hidden');
	var url ="loadCannedReport.action";
	loadAction(url);
	//constructTable(false,'validationDataTable',validationData_ColManager,validationDataServerParams,validationData_aoColumnDef,validationData_aoColumn);
}
function getHomeDonorReport(donor,donorProcedure){
	$("#donor").val(donor);
	$("#donorProcedure").val(donorProcedure);
	//alert("donor:"+donor+"dp::"+donorProcedure);
	var url ="loadDonorReport.action?donor="+donor+"&donorProcedure="+donorProcedure;
	loadAction(url);
	//response = ajaxCall("loadDonorReport","reports");
	//$('#main').html(response);
}



	                        
/*Archive Donor List Server Parameter Starts*/

var archiveDonor_ColManager = function(){
											$("#archiveDonorColumn").html($('#archivedDonorTable_wrapper .ColVis'));
										 };
var archive_aoColumn = [ { "bSortable": false},
                               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null
	             ];										 
var archiveServerParams = function ( aoData ) {
												aoData.push( { "name": "application", "value": "stafa"});
												aoData.push( { "name": "module", "value": "archive_Donor_List"});	
												
												aoData.push( {"name": "col_1_name", "value": "lower(p.PERSON_MRN)" } );
												aoData.push( { "name": "col_1_column", "value": "lower(p.PERSON_MRN)"});
												
												aoData.push( { "name": "col_2_name", "value": "lower(nvl(p.PERSON_LNAME, ' ' )|| ' ' || nvl(p.PERSON_FNAME, ' ' ))"});
												aoData.push( { "name": "col_2_column", "value": "lower(pname)"});	
												
												aoData.push( { "name": "col_3_name", "value": "lower(to_char(p.person_dob,'Mon dd, yyyy'))"});
												aoData.push( { "name": "col_3_column", "value": "lower(pdob)"});	
												
												aoData.push( { "name": "col_4_name", "value": "lower(nvl((select SPEC_ID from er_specimen where pk_specimen =pp.fk_product),' '))"});
												aoData.push( { "name": "col_4_column", "value": "lower(proID)"});
												
												aoData.push( { "name": "col_5_name", "value": "lower((select nvl(to_char(max(COLLECTION_DATE),'Mon DD, YYYY'),' ') from er_apheresis_collection where FK_plannedprocedure =pp.pk_plannedprocedure ))"});
												aoData.push( { "name": "col_5_column", "value": "lower(collectionDate)"});	
												
												aoData.push( { "name": "col_6_name", "value": "lower((select codelst_desc from er_codelst where pk_codelst = d.fk_codelstdonortype))"});
												aoData.push( { "name": "col_6_column", "value": "6"});	
												
												aoData.push( { "name": "col_7_name", "value": "lower(nvl((select nvl(protocol_name,' ') from er_protocol where pk_protocol = pp.fk_codelstplannedprocedure),' '))"});
												aoData.push( { "name": "col_7_column", "value": "lower(planprocedure)"});	
												
												aoData.push( { "name": "col_8_name", "value": "lower(pp.pp_primaryusernames)"});
												aoData.push( { "name": "col_8_column", "value": "lower(nurse)"});	
												
												aoData.push( { "name": "col_9_name", "value": "lower((select codelst_desc from er_codelst where pk_codelst =pp.fk_codelstdonorstatus))"});
												aoData.push( { "name": "col_9_column", "value": "lower(status)"});	
											};
var archive_aoColumnDef = [
	                         {"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
	                             return "<input name='' id= '' type='checkbox' value='' />";
	                             }},
	                          {    "sTitle":'<s:text name="stafa.label.donorid"/>',
	                                 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
	                                	 return source[1];
	                             }},
	                          {    "sTitle":'<s:text name="stafa.label.donorname"/>',
	                                 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
	                                	 link = "<a href='#' onclick='javascript:getHomeDonorReport(\""+source[0] +"\",\""+source[10]+"\");'>"+source[2]+"</a>";
			                             return link;
	                                 
	                                    }},
	                          {    "sTitle":'<s:text name="stafa.label.donordob"/>',
	                                  "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
	                                      return source[3];
	                                  }},
	                          {    "sTitle":'Product ID',
	                                  "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
	                                      return source[11];
	                                  }},    
	                          {    "sTitle":'Collection Date',
	                                  "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
	                                      return source[12];
	                                  }},       
	                                  
	                          {   "sTitle":'<s:text name="stafa.label.donortype"/>',
	                                  "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
	                                      return source[5];
	                              }},
	                          {    "sTitle":'<s:text name="stafa.label.plannedprocedure"/>',
	                                  "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
	                                      return source[6];
	                              }},
	                          {    "sTitle":'<s:text name="stafa.label.nurse"/>',
	                                  "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
	                                      return source[8];
	                              }},
	                              {    "sTitle":'Status',
	                                  "aTargets": [ 9], "mDataProp": function ( source, type, val ) {
	                                      return source[9];
	                              }} 
	                         
	                        ];
	                        
</script>
	
<div id="homePageDiv">
<div id="firstdiv" class="upperdiv">
    <table cellpadding="0" cellspacing="2" border="0" width="55%">
        <tbody >
            <tr>
                <td width="15%" style="text-align:center;" class="cursor"><img src = "images/icons/dashboard.png" style="width:46px;height:36px; text-align:center;" /></td>
             <td width="15%" style="text-align:center;" class="cursor"><img src = "images/icons/archiveDonor.png" style="width:46px;height:36px;text-align:center;" onclick="showArchiveList();"/></td>
                <td width="15%" style="text-align:center;" class="cursor"><img src = "images/icons/report.png" style="width:46px;height:36px;text-align:center;" onclick="showValidationSheet();"/></td>
                   <td width="15%" style="text-align:center;" class="cursor"><img src = "images/icons/nures.png" style="width:46px;height:36px;text-align:center;" onclick="javascript:loadAction('loadNurseHomePage.action')"/></td>
               <!-- procedure.png -->
                 <td width="15%" style="text-align:center;" class="cursor"><img src = "images/icons/usergroup.png" style="width:46px;height:36px;text-align:center;" onclick="javascript:window.open('http://comaiba.dyndns-free.com/healthcare/login.do')" /></td>
            
            </tr>
            <tr>
                <td width="15%" style="text-align:center;" class="cursor"><b>Dashboard</b></td>
               <td width="15%" style="text-align:center;" onclick="showArchiveList();" class="cursor"><b>Archived Donors</b></td>
                <td width="15%" style="text-align:center;" onclick="showValidationSheet();" class="cursor"><b>Reports</b></td>
                <td width="15%" style="text-align:center;" onclick="javascript:loadAction('loadNurseHomePage.action')" class="cursor"><b>Nurse Home</b></td>
              
               <td width="15%" style="text-align:center;" onclick="javascript:window.open('http://comaiba.dyndns-free.com/healthcare/login.do')" class="cursor" ><b>Comaiba</b></td>
            
            </tr>
        </tbody>
    </table>
</div>
<div class="divwrapper">
<div class="cleaner"></div>
    <div class="">
        <div class=" commondivleft column">
            <div  class="portlet">
                <div class="portlet-header "><div><img src = "images/icons/dashboard.png" style="width:16px;height:16px;"/>&nbsp;&nbsp;<label><b>Dashboard</b></label>&nbsp;</div></div>
                <div class="portlet-content">
                    <div class="leftdiv">
                        <ul class="ulstyle">   <input type="hidden" id="tmpYear" name="tmpYear" value="" />
                            <li class="listyle productstyle" ><img src = "images/icons/bloodpacknov.png" onclick="showGraph('Product');"  style="width:20px;height:28px;" class="cursoroption"/></li>
                            <li class="listyle productstyle"><span onclick="showGraph('Product');" class="cursoroption" ><b>Product</b></span> </li>
                            <li class="listyle donorstyle"><img src = "images/icons/userconfig.png" onclick="showGraph('Donors');"  style="width:20px;height:28px;" class="cursoroption"/></li>
                            <li class="listyle donorstyle"><span onclick="showGraph('Donors');" class="cursoroption"><b>Donors</b></span></li>
                            <li class="listyle nursestyle"><img src = "images/icons/nures.png" onclick="showGraph('Nurse');"  style="width:20px;height:28px;" class="cursoroption"/></li>
                            <li class="listyle nursestyle"><span onclick="showGraph('Nurse');" class="cursoroption"><b>Nurses</b></span></li>
                        </ul>
                    </div>
                    <span style="padding:1%; margin:0%;vertical-align:calc;" >
                    	<b>Monitoring:</b>
                    </span>
                    <span id="selectedtitle" style="font-weight:bold;">
                    </span>&nbsp;&nbsp;
                    <!-- <input class="monthpicker" data-start-year="2009" data-final-year="2011" data-selected-year="2010" style="display:none;width:30px;"> -->
             
                    <span class="week-picker hidden" style="z-index:1000;position:absolute;" >
                    </span>
                    
                    <span class="cursoroption" style="padding:1%; margin:0%; font-weight:bold;vertical-align:middle;">
                    		 <span id="monthText" class="hidden"><input class="monthpicker" data-start-year="1999" data-final-year="2020"  style="width:60px;"></span>
                    		 <span class="graphDates" onclick="modeChange()">
                    		<span id="startDate" >
                    		</span> - 
                    		<span id="endDate" >
                    		</span>&nbsp;</span>
                    	<input id="weekView"  class="buttonActive" type='button' value='Week' onclick="weekView()">&nbsp;<input ID="monthView" type='button' value='Month' onclick="monthView()">
                    </span>
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                    <!-- <div class="rightdiv"><img src = "images/others/barchart.png"   style="height:230px;width:458px" class="cursoroption"/></div> -->
                    <input type="hidden" name="mode" id="mode" value='week' />
                    <div class="rightdiv">
                    <input type="hidden" name="productData" id="productData" value='<s:property value="productData"/>' />
                    <input type="hidden" name="donorData" id="donorData" value='<s:property value="donorData"/>' />
                    <input type="hidden" name="nurseData" id="nurseData" value='<s:property value="nurseData"/>' />
                    <input type="hidden" name="ticksData" id="ticksData" value='<s:property value="ticksData"/>' />
                    <input type="hidden" name="graphType" id="graphType" value="Product" />
                    
                   <div id="chart1"></div>
                </div>
            </div>
        </div>
      </div>
      <div class=" commondivright column">       
        <div  class="portlet">
            <div class="portlet-header "><div><img src = "images/icons/email.png" style="width:20px;height:20px;"/>&nbsp;&nbsp;<label><b>Inbox</b></label>&nbsp;</div></div><br>
            <div class="portlet-content">
                <table>
                    <tr>
                        <td style="width:20%"><div class="cursoroption"><img src = "images/icons/no.png" style="width:16px;height:16px;" class="" id ="" onclick="delete_Inbox();"/>&nbsp;&nbsp;<label class=""  onclick="delete_Inbox();"><b>Delete</b></label>&nbsp;</div></td>   
                    </tr>
                </table> 
                <table cellpadding="0" cellspacing="0" border="0" id="inboxTable" class="display">
                    <thead>
                        <tr>
                            <th id="inboxTableColumn" width="4%"></th>
                            <th>Select</th>
                            <th>Name</th>
                            <th>Subject</th>
                            <th width="25%">Date/Time</th>               
                        </tr>
                    </thead>
                    <tbody>
                       
                    </tbody>
                </table>           
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!--   <input type="text" name="monthyear" id="monthyear" value="12/2013" /> -->
<div id="archiveDonorDiv" class='hidden'>
	<div class="column">
		<div  class="portlet">
			<div class="portlet-header ">Archived Donor List</div>
			<div class="portlet-content">
				<form id="reports" name="reports">
	  				<input type="hidden" id="donor" name="donor" value=""/>
	  				<input type="hidden" id="donorProcedure" name="donorProcedure" value=""/>
	  			</form>
				<table cellpadding="0" cellspacing="0" border="0" id="archivedDonorTable" class="display">
					<thead>
						<tr>
							<th width='4%' id='archiveDonorColumn'></th>
							<th>Donor ID</th>
							<th>Donor Name</th>
							<th>Donor DOB</th>
							<th>Product ID</th>
							<th>Collection Date</th>
							<th>Donor Type</th>
							<th>Planned Procedure</th>
							<th>Nurse</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="validationDataDiv" class='hidden'>
<div id="tabs">
	<ul>
		<li><a href="#cannedTab">Canned Report</a></li>
		<li><a href="#adhocTab">Ad Hoc Report</a></li>
	</ul>
	<div class="column">
		<div class="portlet">
			<div class="portlet-header ">Validation Data Sheet</div>
			<div class="portlet-content">
				<div id="validationDataTableDiv" style="overflow-x:auto;overflow-y:hidden;">
					<table cellpadding="0" cellspacing="0" border="1" id="validationDataTable" class="display">
						<thead>
							<tr>
								<th width='4%' id='validationDataTableColumn'></th>
								<th>Donor Name</th>
								<th>Donor Type</th>
								<th>Donor DOB</th>
								<th>Donor Weight(kg)</th>
								<th>Diagnosis</th>
								<th>Recipient Weight(kg)</th>
								<th>Prescription</th>
								<th>Liters Ordered</th>
								<th>Final Inlet(L)</th>
								<th>Collection Day</th>
								<th>Pre WBC</th>
								<th>CD34</th>
								<th>Pre HCT</th>
								<th>Pre %MNC</th>
								<th>Mid-Count WBC</th>
								<th>Product WBC</th>
								<th>Product HCT</th>
								<th>Product %MNC</th>
								<th>CD/Yield</th>
								<th>CD/Cum</th>
								<th>Equipment</th>
								<th>IDM Verified Date</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</body>
<script>

$(function() {
	
	
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" );
	
		/* $( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	}); */
	$( "#tabs" ).tabs();
	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
	/* $("#chart1").mouseenter(function() {
		alert("mouse over");
		return false;
		
	}); */
	
});

function showGraph(dataType){
	document.getElementById("chart1").innerHTML="";
	document.getElementById("selectedtitle").innerHTML="";
	document.getElementById("selectedtitle").innerHTML=dataType;
	$(".productstyle").removeClass("lihighlight");
	$(".donorstyle").removeClass("lihighlight");
	$(".nursestyle").removeClass("lihighlight");
	$("#graphType").val(dataType);
   	
	var myData;

	//var ticks = ['Mon', 'Tue', 'Wed', 'Thu','Fri','Sat','Sun'];
	myData=$("#ticksData").val();
	//alert("alert3"+myData);
 if(myData.length >0){
	myData=(myData.substring(0,(myData.length-1)));
	var ticks =myData.split(",");
 //alert("ticks"+ticks);


    $.jqplot.config.enablePlugins = false;
	  
	
	var graphData;
    if(dataType =="Donors"){
    	$(".donorstyle").addClass("lihighlight");
    	myData = $("#donorData").val();
    	//alert("pro"+myData)
     }else if(dataType =="Nurse"){
    	 $(".nursestyle").addClass("lihighlight");
    	myData = $("#nurseData").val();
    	//alert("nurseData"+myData)
     }else if(dataType =="Product"){
    	 $(".productstyle").addClass("lihighlight");
    	myData = $("#productData").val();
    	//alert("productData"+myData)
     }else{
    	myData="1,2,3,4,5,";
    }
    var	 plot1;
   	//	alert(" myData " + myData);
    	myData=(myData.substring(0,(myData.length-1)));
    	try{
		   //	alert(" myData1 " + myData);
		   	var graphData= myData.split(",");
		   //	alert("graph1"+graphData);
		   //alert("max " + Math.max.apply(Math, graphData));	
		   var maxValue =  Math.max.apply(Math, graphData);
		   if(maxValue ==0){
		  	 maxValue=1;
		   }
		   if(maxValue > 0){
			    plot1 = $.jqplot('chart1', [graphData], {
			        // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
			       // animate: !$.jqplot.use_excanvas,
			        seriesDefaults:{
			            renderer:$.jqplot.BarRenderer,
			            pointLabels: { show: true }
			           
			        },
			        axes: {
			            xaxis: {
			                renderer: $.jqplot.CategoryAxisRenderer,
			                ticks: ticks
			            }
			        },
			        axesDefaults: {
			            show: false,    // wether or not to renderer the axis.  Determined automatically.
			            min: 0,      // minimum numerical value of the axis.  Determined automatically.
			            max: (maxValue+3)      // maximum numverical value of the axis.  Determined automatically.
			            
			        }
			    });
			  // plot1.axes.xaxis.numberTicks = 5;
			    // plot1.axes.yaxis.numberTicks = 6;
			     plot1.replot();
		   }
			   	
	    }catch(err){
	    	//alert("error " + err);
	    	//plot1.replot();
	    }
    }
   // plot1.replot();
  /*  plot1.series[0].data = myData;
    plot1.resetAxesScale();
    plot1.replot();
   */
  // alert("end of graph");
   return true;

}

	showGraph('Product');
	
	//showGraph('Nurse');
</script>

<!-- 

<link rel="stylesheet" type="text/css" href="css/ui/base/jquery.ui.tabs.css" media="screen" />
<link type="text/css" href="css/jquery.jqplot.min.css" rel="stylesheet" />
<script  type="text/javascript" src="js/jquery/graph/excanvas.js" ></script>
<script type="text/javascript" src="js/jquery/graph/jquery.jqplot.min.js"></script>
<script class="include" type="text/javascript" src="js/jquery/graph/jqplot.barRenderer.min.js"></script>
<script class="include" type="text/javascript" src="js/jquery/graph/jqplot.categoryAxisRenderer.min.js"></script>
<script class="include" type="text/javascript" src="js/jquery/graph/jqplot.pointLabels.js"></script>
<script class="include" type="text/javascript" src="js/jquery/jquery.livequery.js"></script>
 -->
</html>
