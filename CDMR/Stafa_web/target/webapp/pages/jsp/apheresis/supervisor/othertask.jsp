<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/dataSearch.js"></script>
<script>

$(document).ready(function(){
	//alert("ready ");
	clearTracker();
	hide_slidecontrol();
	hide_slidewidgets();
	/* constructProductTable(false); */
	constructRecurringTable(false,'recurringTable');
	$("select").uniform(); 

});


function savePages(){
	if(checkModuleRights(STAFA_RECURRINGTASK,APPMODE_SAVE)){
		try{
			if(saverecurringtask()){
				return true;
			}
		}catch(e){
			alert("exception " + e);
		}
	}
}


function constructRecurringTable(flag,tableId){
	var recurring_ColManager = function () {
										        $("#othertaskColumn").html($('#recurringTable_wrapper .ColVis'));
											};
	var selectproduct_aoColumnDef = [
	 								
									 {  "aTargets": [ 0], "mDataProp": function ( source, type, val ) {
										return "<input type='checkbox' id='recurringTask' value='"+source[0]+"' />";
										 
									 }},
									 {    "sTitle":'Task',
									    "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
										return source[1];
									 }},
									 {    "sTitle":'Description',
									    "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
									     return source[2];
									 
									 }},
									 {    "sTitle":'User Assignments',
									    "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
									    	var link = "<input type='hidden' id='tmpUser' name='tmpUser' value='"+source[3]+"'/><input type='hidden' id='tmpTaskLevel' name='tmpTaskLevel' value='"+source[4]+"'/>"+ $("#nurseUserspan #rtUser option[value='"+source[3]+"']").text();
									    	link = link + "&nbsp;" +  $("#tasklevelspan #rtCodelstTaskLevel option[value='"+source[4]+"']").text();
									    //return source[3];
									      return link;
									 }},
									 {    "sTitle":'Start Date',
									    "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
									    		var	startDate = ($.datepicker.formatDate('M dd, yy', new Date(source[5])));
									    return startDate;
									 }},
									 {    "sTitle":'Due Date',
									        "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
									    		var	dueDate = ($.datepicker.formatDate('M dd, yy', new Date(source[6])));
									    return dueDate;
									 }},
									 {    "sTitle":'Frequency',
									        "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
									    return source[7];
									 }},
									 {    "sTitle":'Status',
									        "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
									    return source[8];
									 }}  
										
								];	
	var recurringServerParams = function ( aoData ) {
														aoData.push( { "name": "application", "value": "stafa"});
														aoData.push( { "name": "module", "value": "recurring_task"});	
														
														aoData.push( {"name": "col_1_name", "value": "lower((select TASK_NAME from TASK_LIBRARY where PK_TASKLIBRARY=FK_CODELSTTASK))" } );
														aoData.push( { "name": "col_1_column", "value": "1"});
														
														aoData.push( { "name": "col_2_name", "value": "lower(RT_DESCRIPTION)"});
														aoData.push( { "name": "col_2_column", "value": "2"});
														
														aoData.push( { "name": "col_3_name", "value": "lower((select USER_LASTNAME || ' ' ||USER_FIRSTNAME from  esec_stafa.sec_user where pk_userid = RT_USER))"});
														aoData.push( { "name": "col_3_column", "value": "3"});
														
														aoData.push( { "name": "col_4_name", "value": "lower(nvl(to_char(RT_STARTDATE,'Mon dd, yyyy'),''))"});
														aoData.push( { "name": "col_4_column", "value": "5"});
														
														aoData.push( { "name": "col_5_name", "value": "lower(nvl(to_char(RT_DUEDATE,'Mon dd, yyyy'),''))"});
														aoData.push( { "name": "col_5_column", "value": "6"});
														
														aoData.push( { "name": "col_6_name", "value": "lower((select codelst_desc from er_codelst where pk_codelst=FK_CODELSTFREQUENCY))"});
														aoData.push( { "name": "col_6_column", "value": "8"});
														
														aoData.push( { "name": "col_7_name", "value": "lower(decode(SIGN( RT_DUEDATE - sysdate),-1,'Complete','Pending'))"});
														aoData.push( { "name": "col_7_column", "value": "9"});
													};
	var recurring_aoCol = 	[ { "bSortable":false},
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null
				             ];
	constructTable(flag,tableId,recurring_ColManager,recurringServerParams,selectproduct_aoColumnDef,recurring_aoCol);
}
/* function constructProductTable(flag){
	
	var selectproduct_serverParam = function ( aoData ) {
											aoData.push( { "name": "application", "value": "stafa"});
											aoData.push( { "name": "module", "value": "aphresis_othertask_selectproduct"});	
											aoData.push( {"name": "col_1_name", "value": "" } );
											aoData.push( { "name": "col_1_column", "value": ""});
											aoData.push( { "name": "col_2_name", "value": "d.fk_product"});
											aoData.push( { "name": "col_2_column", "value": "d.fk_product"});
											};
	var selectproduct_aoColumnDef = [
								
									{"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
										var col0="<input type='checkbox' value='"+source[1]+"' />";
										return col0;
									}},
									{    "sTitle":'<s:text name="stafa.label.productid"/>',
									 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
										return source[2];
									}},
									{    "sTitle":'<s:text name="stafa.label.donorid"/>',
									 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
									     return source[3];
									 
									    }},
									{    "sTitle":'<s:text name="stafa.label.donorname"/>',
									    "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
									    return source[4];
									  }},
									{    "sTitle":'<s:text name="stafa.label.status"/>',
									        "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
									    return source[5];
									  }}    
									
									];
	
	var selectproduct_aoColumn = [ { "bSortable": false},
									null,
									null,
									null,
									null
									];
	
	constructTableWithoutColManager(flag,'selectproduct',selectproduct_serverParam,selectproduct_aoColumnDef,selectproduct_aoColumn);
	
} */
var dateId=0;
function addrecurringTask(){
	if(checkModuleRights(STAFA_RECURRINGTASK,APPMODE_ADD)){
		var newRow="<tr>"+"<td><input type='checkbox' id='recurringTask' name='recurringTask' value='0' /><input type='hidden' id='' name='' value='0'/></td>"+
	            "<td>"+$("#taskspan").html()+"</td>"+
	            "<td><input type='text' id='rtDescription' name='rtDescription'/></td>"+
	            "<td nowrap='nowrap'>"+$("#nurseUserspan").html() + "&nbsp;" +$("#tasklevelspan").html()  +"&nbsp;<img src = 'images/icons/addnew.jpg' class='cursor' id='' onclick='add_users(this)'/></td>"+
	            "<td><input type='text' id='rtStartDate"+dateId+"' name='rtStartDate' class='startDate dateEntry dateclass"+dateId+"'/></td>"+
	            "<td><input type='text' id='rtDueDate"+dateId+"' name='rtDueDate' class='dueDate dateEntry dateclass'/>"+
	            "<td>"+$("#frequency").html()+"</td><td>&nbsp;</td>"+
	            "</tr>";
	            
	       $("#recurringTable tbody").prepend(newRow); 
		  $("#recurringTable .ColVis_MasterButton").triggerHandler("click");
			$('.ColVis_Restore:visible').triggerHandler("click");
			$("div.ColVis_collectionBackground").trigger("click");
			  $("#rtDueDate"+dateId).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
			        changeYear: true, yearRange: "c-50:c+45"});
			  $("#rtStartDate"+dateId).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
			        changeYear: true, yearRange: "c-50:c+45"});
			  
		$.uniform.restore('select');
		  $("select").uniform();
		  dateId=dateId+1;
	}
}

function editrecurringRows(){
	//  alert("edit Rows");
	if(checkModuleRights(STAFA_RECURRINGTASK,APPMODE_EDIT)){
		  $("#recurringTable tbody tr").each(function (i,row){
			 if($(this).find("#recurringTask").is(":checked")){
				 $(this).find("td").each(function (col){
					 if(col ==0){
						 recurringTask = $(this).find("#recurringTask").val();
		  	 		  }else if(col ==1 ){
						 if ($(this).find("#codelstTask").val()== undefined){
							 //var oldSelected = $(this).find("#codelstTask").val();
							 	var taskSpan = $(this).html();
							 	//alert("taskSpan : "+taskSpan);
							 	$(this).html($("#taskspan").html());
							 	
							 	$(this).find("#codelstTask option:contains("+taskSpan+")").attr('selected', 'selected');
							  	
								$.uniform.restore($(this).find("#codelstTask"));
								$(this).find("#codelstTask").uniform(); 	
						 }
					 }else if(col ==2){
						 if ($(this).find("#rtDescription").val()== undefined){
						 	$(this).html("<input type='text' name='rtDescription' id='rtDescription' class='' value='"+$(this).text()+ "'/>");
						 }
					 }else if(col ==3){
						 if ($(this).find("#rtUser").val()== undefined){
							 var tmpUser = $("#tmpUser").val();
							 var tmpTaskLevel = $("#tmpTaskLevel").val();
						 	$(this).html($("#nurseUserspan").html() + "&nbsp;" +$("#tasklevelspan").html()  +"&nbsp;<img src = 'images/icons/addnew.jpg' class='cursor' id='' onclick='add_users(this)'/>");
						 	 
						 	$(this).find("#rtUser").val(tmpUser);
						 	$.uniform.restore($(this).find("#rtUser"));
						 	$(this).find("#rtUser").uniform(); 
						 	$(this).find("#rtCodelstTaskLevel").val(tmpTaskLevel);
						 	$.uniform.restore($(this).find("#rtCodelstTaskLevel"));
							$(this).find("#rtCodelstTaskLevel").uniform(); 
						 }
					 }else if(col ==4){
						 if ($(this).find(".startDate").val()== undefined){
							 	$(this).html("<input type='text' id='rtStartDate"+i+"' name='rtStartDate' class='startDate dateEntry dateclass' value='"+$(this).text()+ "'/>")
							  	$("#rtStartDate"+i).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
							        changeYear: true, yearRange: "c-50:c+45"});
							 }
					}else if(col ==5){
						 if ($(this).find("#dueDate").val()== undefined){
							 	$(this).html("<input type='text' id='rtDueDate"+i+"' name='rtDueDate' class='dueDate dateEntry dateclass' value='"+$(this).text()+"'/>")
							 	$("#rtDueDate"+i).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
							        changeYear: true, yearRange: "c-50:c+45"});
							 }
					}else if(col ==6){
						 if ($(this).find("#rtCodelstFrequency").val()== undefined){
							 	
							 	var frequency = $(this).html();
							 	//alert("frequency : "+frequency);
							 	$(this).html($("#frequency").html());
							 	$(this).find("#rtCodelstFrequency option:contains("+frequency+")").attr('selected', 'selected');
								$.uniform.restore($(this).find("#rtCodelstFrequency"));
								$(this).find("#rtCodelstFrequency").uniform();
							 }
					}
				 });
			 }
		  });
		 // $.uniform.restore('select');
	      $("select").uniform();
	}
}
function deletedRecurringTaskData(){
	if(checkModuleRights(STAFA_RECURRINGTASK,APPMODE_DELETE)){
	    var yes=confirm(confirm_deleteMsg);
		if(!yes){
			return;
		}
		var deletedIds = ""
		$("#recurringTable tbody tr ").find("#recurringTask:checked").each(function (row){
			
			deletedIds += $(this).val() + ",";
		});
		if(deletedIds.length >0){
		  deletedIds = deletedIds.substr(0,deletedIds.length-1);
		}
		jsonData = "{deletedRecurringTask:'"+deletedIds+"'}";
		
		url="deletedRecurringTask";
	    response = jsonDataCall(url,"jsonData="+jsonData);
	    constructRecurringTable(true,'recurringTable');
	    //constructMedicationTable(true,donor,donorProcedure);
	}
}
function saverecurringtask(){
	if(checkModuleRights(STAFA_RECURRINGTASK,APPMODE_SAVE)){
		var rowData = "";
		 $("#recurringTable tbody tr").each(function (row,rowel){
			 rowHtml =  $(this).find("#codelstTask").closest('tr');	
			 	recurringTask = $(rowHtml).find("#recurringTask").val();
				 	if(recurringTask == undefined){
				 		recurringTask =0;
			 		}
		 		codelsttask=$(rowHtml).find("#codelstTask").val();
		 		description=$(rowHtml).find("#rtDescription").val();
		 		freq=$(rowHtml).find("#rtCodelstFrequency").val();
		 		startdate=$(rowHtml).find(".startDate").val();
		 		duedate=$(rowHtml).find(".dueDate").val(); 
		 		 		
		 		$(rowel).find("#rtUser").each(function (i){
		 			var currSelcDivObj = $(this).closest('div');
		 			var codetasklevel = $(currSelcDivObj).next("div").find('#rtCodelstTaskLevel').val();
		 			var user=$(this).val();
		 			rowData+= "{recurringTask:'"+recurringTask+"',codelstTask:'"+codelsttask+"',rtDescription:'"+description+"',rtUser:'"+user+"',rtCodelstTaskLevel:'"+codetasklevel+"',rtStartDate:'"+startdate+"',rtDueDate:'"+duedate+"',rtCodelstFrequency:'"+freq+"'},";
		 			
		 		});
			
		 		
		 });
		
		
		 if(rowData.length >0){
	            rowData = rowData.substring(0,(rowData.length-1));
	        }
		
		 if(rowData!=null && rowData!=''){
		url="saveRecurringTask"
		
			 response = jsonDataCall(url,"jsonData={recurringData:["+rowData+"]}");
		//jsonDataCall(url, "jsonData=" + rowData);
		 alert("Data Saved Successfully");
		 constructRecurringTable(true,'recurringTable');
		
		 }
		 return true;
	}
}
/* function add_Task(){
	var runningTabClass= $('#othertaskTable tbody tr td:nth-child(1)').attr("class");
	var newRow = "<tr>"+
	"<td></td>"+
	"<td>"+ $("#taskspan").html() +"</td>"+
	"<td id='productid'><div style='text-align:right;'><img src = 'images/icons/addnew.jpg' class='cursor' id='' onclick='add_popup(this)'/></div></td>"+
	"<td id='donorid'></td>"+
	"<td id='donorname'></td>"+
	"<td>"+$("#destinationspan").html() + "</td>"+
	"<td id='status'></td>"+
	"<td nowrap='nowrap'>"+$("#nurseUserspan").html() + "&nbsp;" +$("#tasklevelspan").html()  +"&nbsp;<img src = 'images/icons/addnew.jpg' class='cursor' id='' onclick='add_users(this)'/></td>"+
	
	"</tr>";
	//alert("before");
	if(runningTabClass=="dataTables_empty"){
		$("#othertaskTable tbody").replaceWith(newRow);
	}else{
		$("#othertaskTable tbody").prepend(newRow);
	}
	$.uniform.restore('select');
	$("select").uniform();  
	//alert("end");

}  */
var curr_row;
var img;
/* function add_popup(rowid){
	// jQuery("#addmodal").dialog("destroy");
	destroyPopup('addmodal');
	var cell = $(rowid).closest('td');
	var row = cell.closest('tr');
	curr_row = row[0].rowIndex-1;
    showPopUp('open','addmodal','Select Products','600','600');
   
}
 */
function add_users(rowid){
	var cell = $(rowid).closest('td');
	var row = cell.closest('tr');
	curr_row = row[0].rowIndex-1;
	//var newUsers = $('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(7)").html()+ "<br>"+$("#nurseUserspan").html() + "&nbsp;" +$("#tasklevelspan").html() ;
	var newUsers = "<br>"+$("#nurseUserspan").html() + "&nbsp;" +$("#tasklevelspan").html() ;
	$('#recurringTable tbody tr:eq('+curr_row+')').find("td:eq(3)").append(newUsers);
	$.uniform.restore('select');
	$("select").uniform(); 
}

/* var indexes;
var rawData='';
function addProduct(){
         var rows = [];
         var productId = "";
         var donorId = "";
         var donarName = "";
         var status = "";
        $('#selectproduct tbody tr').each(function(i, n){
        	//alert( i + " / " + n);
        	var $row = $(n);
    		//?BB var cbox=$(n).children("td:eq(0)").find(':checkbox').is(":checked");
    		var cbox=$(n).children("td:eq(0)").find(':checkbox');
    		//?BB if(cbox){
    		if($(cbox).is(":checked")){
    			//alert("checked " + $(cbox).val());
    			productId+="<input type='hidden' id='product' value='"+$(cbox).val() +"' />"+ $row.find("td:eq(1)").text()+"<br/>";
    			donorId+=$row.find("td:eq(2)").text()+"<br/>";
    			donarName+=$row.find("td:eq(3)").text()+"<br/>";
    			status+=$row.find("td:eq(4)").text()+"<br/>";
    		}
        });
       
    	$('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(2)").html(productId+"<div style='text-align:right;margin-top:-20%;'><img src = 'images/icons/addnew.jpg' class='cursor' id='' onclick='add_popup(this)'/></div>");
        $('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(3)").html(donorId);
  		$('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(4)").html(donarName);
   		$('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(6)").html(status);  
        
        
    /*  getJsonFromTable= JSON.stringify(rows);  
    console.log(getJsonFromTable);
   $.each(rows, function(key, value){
	   $('#othertaskTable tbody tr:eq('+curr_row+')').after().append("<tr>"
			   +"<td><input type='checkbox' id=''/></td>"+
				"<td><select><option>select</option><option>Product Transplatation</option></select></td>"+
				"<td id='productid'>"+rows[key].productid+"<img src = 'images/icons/addnew.jpg' class='cursor' id='' onclick='add_popup(this)'/></td>"+
				"<td id='donorid'></td>"+
				"<td id='donorname'></td>"+
				"<td><select><option>select</option><option>Blood Bank</option></select></td>"+
				"<td id='status'></td>"+
				"<td><select><option>Select</option><option>Nurse1</option></select><select><option>Select</option><option>Primary</option></select></td>"+"</tr>");
	   
     	$('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(2)").text(rows[key].productid);
        $('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(3)").text(rows[key].donorid);
  		$('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(4)").text(rows[key].donorname);
   		$('#othertaskTable tbody tr:eq('+curr_row+')').find("td:eq(6)").text(rows[key].status);  
   		

    }); 
    closeProduct();
   //showPopUp('close','addmodal','Select Products','600','600');
 } */
 
/*  function closeProduct(){
	//alert("close");
	 destroyPopup('addmodal');
 } */
 
</script>

<span id="hiddendropdowns" class="hidden">
 <span id="nurseUserspan">
<select id="rtUser"  name="rtUser" >
	<option value="">Select</option> 
 <s:iterator value="lstNurseUsers" var="rowVal" status="row">
 	<option value="<s:property value="#rowVal[0]"/>"> <s:property value="#rowVal[1]"/> <s:property value="#rowVal[2]"/></option>
 </s:iterator>
</select>
</span>
<span id="taskspan">
 <s:select id="codelstTask"  name="codelstTask" list="lstrecurringTask" listKey="taskLibrary" listValue="name"  headerKey="" headerValue="Select" />
</span>

<span id="destinationspan"> 
  <s:select id="destination"  name="destination" list="lstDestination" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" />
</span>

<span id="tasklevelspan"> 
  <s:select id="rtCodelstTaskLevel"  name="rtCodelstTaskLevel" list="lstTaskLevel" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" />
</span>
<span id="frequency"> 
  <%-- <s:select id="frequency"  name="frequency" list="lstFrequency" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /> --%>
  <s:select id="rtCodelstFrequency" name="rtCodelstFrequency" list="lstFrequency" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/>
</span>
</span>


<div id="addmodal" style="display:none">
<table id="selectproduct"  border="1" cellpadding="3" cellspacing="0" class="display">
					<thead>
						<tr>
							<th width="4%" id="">Check All <input type="checkbox" name="" class="selectall"/></th>
							<th>Product ID</th>
							<th>Donor Id</th>
							<th>Donor Name</th>
							<th>Status</th>
						</tr>
						
					</thead>
					<tbody>
											
					</tbody>
					
				</table>
				<br>
				<br>
				<div align="right">
				<form>
					<input type="button" name="submit" onclick="addProduct();" value="Submit"/>&nbsp;<input type="button" name="Cancel" value="Cancel" onclick="closeProduct();"/>
				</form>
				</div>
								
</div>

<div class="column">		
		<div  class="portlet">
			<div class="portlet-header ">Recurring Tasks </div>
				<div class="portlet-content">
				<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="addrecurringTask()"/>&nbsp;&nbsp;<label  class="cursor" onclick="addrecurringTask()"><b>Add </b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deletedRecurringTaskData()"/>&nbsp;&nbsp;<label class="cursor" onclick="deletedRecurringTaskData()" ><b>Delete</b></label>&nbsp;</div></td>
				    <td style="width:20%"><div><img src = "images/icons/edit.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="editrecurringRows();"/>&nbsp;&nbsp;<label class="cursor" onclick="editrecurringRows();"><b>Edit</b></label>&nbsp;</div></td>
				    <td style="width:20%"><div><img src = "images/icons/save3.png" class="cursor" style="width:16;height:16;" class="cursor" onclick="saverecurringtask();" >&nbsp;&nbsp;<label class="cursor" onclick="saverecurringtask()"><b>Save</b></label></div></td>
				
				</tr>
				</table>
				<form>
				<div class='dataTableOverFlow'>
				<table cellpadding="0" cellspacing="0" border="0" id="recurringTable" class="display">
					<thead>
						<tr>
							<th width="4%" id="othertaskColumn"></th>
							<th width="14%">Task</th>
							<th width="14%">Description</th>
							<th width="14%">User Assignments</th>
							<th width="14%">Start Date</th>
							<th width="14%">Due Date</th>
							<th width="14%">Frequency</th>
							<th width="14%">Status</th>
							
						</tr>
						
					</thead>
					<tbody>
												
					</tbody>
				</table>
				</div>
				</form>		
				</div>	
			</div>
</div>

<script>
$(function() {

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	

	/**For Notes**/
	/* 
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='ui-addnew'></span>");
	
	$( ".portlet-header .ui-addnew" ).click(function() {
		addNewPopup("Add New Product","SupervisoraddNewDialog","addnewAcquisition","700","1100");
	});
	 */
	
	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});
	// add multiple select / deselect functionality
    $(".selectall").live('click',function () {
          $('.checkselect').attr('checked', this.checked);
    });
 
    // if all checkbox are selected, check the selectall checkbox
    // and viceversa
    $(".checkselect").live('click',function(){
 
        if($(".checkselect").length == $(".case:checked").length) {
            $(".selectall").attr("checked", "checked");
        } else {
            $(".selectall").removeAttr("checked");
        }
 
    });
	  
});
</script>