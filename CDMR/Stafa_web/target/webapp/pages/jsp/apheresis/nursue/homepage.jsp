<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/dataSearch.js"></script>

<div class="cleaner"></div>

<span id="hiddendropdowns" class="hidden">
 <span id="taskActionspan">
 <s:select id="taskAction"  name="taskAction" list="lstTaskAction" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" />
 
</span>

<span id="taskAsignspan">
 <s:select id="taskAsign"  name="taskAsign" list="lstTaskAsign" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" />
</span>

 <span id="nurseUserspan">
<select id="nurseUser"  name="nurseUser" >
	<option value="">Select</option> 
 <s:iterator value="lstNurseUsers" var="rowVal" status="row">
 	<option value="<s:property value="#rowVal[0]"/>"> <s:property value="#rowVal[1]"/> <s:property value="#rowVal[2]"/></option>
 </s:iterator>
</select>
</span>


</span>
 
 <div class="column">		
 <form id="aphresis" name="aphresis">
  <input type="hidden" id="donor" name="donor" value=""/>
  <input type="hidden" id="donorProcedure" name="donorProcedure" value=""/>
  <s:hidden id="user" name="user" value="%{user}" /> 
   <s:hidden id="transferValue" name="transferValue" value="%{transferValue}" /> 
   <s:hidden id="terminateValue" name="terminateValue" value="%{terminateValue}" /> 
   <s:hidden id="selfAsignValue" name="selfAsignValue" value="%{selfAsignValue}" /> 
  	<div  class="portlet">
	<div class="portlet-header "><s:text name="vcdmr.label.fileupload"/></div>
		<div class="portlet-content">
		<div id="assignDonorDiv" style="overflow-x:auto;overflow-y:hidden;">
			<table cellpadding="0" cellspacing="0" border="0" id="assignedDonorsTable" class="display">
			<thead>
			   <tr>
			   		<th width="4%" id="assignedDonorsColumn"></th>
					<th colspan="1"><s:text name="stafa.label.donorid"/></th>
					<th colspan="1"><s:text name="stafa.label.donorname"/></th>
					<th colspan="1"><s:text name="stafa.label.donordob"/></th>
					<th colspan="1"><s:text name="stafa.label.donorabo"/></th>
					<th colspan="1"><s:text name="stafa.label.donationtype"/></th>
					<th colspan="1"><s:text name="stafa.label.plannedprocedure"/></th>
					<th colspan="1"><s:text name="stafa.label.mobilizationdate"/></th>
					<th colspan="1"><s:text name="stafa.label.nurse"/></th>
					<th colspan="1"><s:text name="stafa.label.status"/></th>
					<th colspan="1"><s:text name="stafa.label.taskactions"/></th>
				</tr>
			</thead>
			<tbody>

			</tbody>
			</table>
			</div>
		</div>
	</div>	
	
	<div  class="portlet">
	<div class="portlet-header ">Pending Donors</div>
		<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="pendingDonorsTable" class="display">
			<thead>
			   <tr>
			   		<th width="4%" id="pendingDonorsColumn"></th>
					<th colspan="1"><s:text name="stafa.label.donorid"/></th>
					<th colspan="1"><s:text name="stafa.label.donorname"/></th>
					<th colspan="1"><s:text name="stafa.label.donordob"/></th>
					<th colspan="1"><s:text name="stafa.label.donorabo"/></th>
					<th colspan="1"><s:text name="stafa.label.donationtype"/></th>
					<th colspan="1"><s:text name="stafa.label.plannedprocedure"/></th>
					<th colspan="1"><s:text name="stafa.label.mobilizationdate"/></th>
					<th colspan="1"><s:text name="stafa.label.nurse"/></th>
					<th colspan="1"><s:text name="stafa.label.status"/></th>
					<th colspan="1"><s:text name="stafa.label.taskactions"/></th>
				</tr>
			</thead>
			<tbody>

			</tbody>
			</table>
		</div>
	</div>	
	
</form>	
</div>		

<div class="cleaner"></div>
 
 <div class="column">		
	<div  class="portlet">
	<div class="portlet-header ">Recurring Tasks</div>
		<div class="portlet-content">
		<div id="taskDiv" style="overflow-x:auto;overflow-y:hidden;">
			<table cellpadding="0" cellspacing="0" border="0" id="recurringTasksTable" class="display">
			<thead>
			   <tr>
			   		<th width="4%" id="recurringTasksColumn"></th>
				    <th>Task</th>
					<th>Description</th>
					<th>User Assignments</th>
					<th>Start Date</th>
					<th>Due Date</th>
					<th>Frequency</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<!-- //?BB to do -->
			</tbody>
			</table>
			</div>
		</div>
	</div>	
</div>	
<!-- 
<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td>Next:</td>
				<td><div><div class="mainselection"><select name="" id=""><option value="">Select</option><option>Preparation</option><option>Verification</option></select></div></div></td>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="nextProcessPreProduct();" value="Next"/></td>
				</tr>
				</table>
				</form>
		</div>
 -->
<script>

$(function() {
	
	$( ".column" ).sortable({
		connectWith: ".column"
			/*,
        handle: '.portlet-header',
        cursor: 'move',
        forcePlaceholderSize: true*/
			
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});

	/**For Notes**/
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

	/* $( ".portlet-header .ui-addnew" ).click(function() {
		showPopUp("open","addNewDialog","Add New Acquision","900","450")
	}); */
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");
	$( ".portlet-header .ui-reset " ).click(function() {
		var formName = document.getElementById("login");
		//clearForm(formName);
		clear();
	});

// 	$( ".portlet-header .ui-notes " ).click(function() {
// 		showNotes('open');
// 	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});



</script>
 