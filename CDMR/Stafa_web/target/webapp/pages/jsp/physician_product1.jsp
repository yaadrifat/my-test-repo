<%@ include file="common/includes.jsp" %>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script>
$(function() {
	
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open');
	});

	
	/**for Next Button Start**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".next" )
	.append("<span style=\"float:right;\" class='ui-next'></span>");
	
	$( ".portlet-header .ui-next " ).click(function() {
		loadPage('jsp/physician_pendingList.jsp');	
	});
	/**for Next Button End**/
	
	/**for Previous Button Start**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".previous" )
	.append("<span style=\"float:right;\" class='ui-previous'></span>");
	
	$( ".portlet-header .ui-previous " ).click(function() {
		loadPage('jsp/physician_dose1.jsp');	
	});
	/**for Previous Button End**/
	

	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});

$(document).ready(function() {
	//alert("ready");
//$('#table111').dataTable({"aaSorting": [[ 1, "desc" ]]});
	$('#previouse').attr("disabled", false);
	$('#next').attr("disabled", true);
	$('#currentPage').val("s6");
	oTable = $('#availableProducts').dataTable({
		"bJQueryUI": true
	});
	oTable.thDatasorting('availableProducts');
	oTable = $('#incomingProducts').dataTable({
		"bJQueryUI": true
	});
	oTable.thDatasorting('incomingProducts');
	
	$("select").uniform();
	showModal('open','arrivalDetails','');
	
});
$(document).ready(function() {
    $('#blind').html("<<");
      var $box = $('#box').wrap('<div id="box-outer"></div>');
      $('#blind').click(function() {
        $box.blindToggle('fast');
      });
    });


jQuery.fn.blindToggle = function(speed, easing, callback) {
var h = this.width() + parseInt(this.css('paddingLeft')) +  parseInt(this.css('paddingRight'));
if(parseInt(this.css('marginLeft'))<0){
$('#forfloat_right').animate( { width: "79%" }, { queue: false, duration: 200 });	
 $('#blind').html("<<");
 return this.animate({marginLeft:0},speed, easing, callback);
 }
else{
$( "#forfloat_right" ).animate( { width: "95%" }, { queue: false, duration: 250 });
 	$('#blind').html(">>");
 	return this.animate({marginLeft:-h},speed, easing, callback);
   }
};

</script>

<div class="cleaner"></div>
<div id="floatMenu">
<div id="forfloat_wind">
	<div id="box-outer">
		<a href="#" style="text-decoration:none" id="blind">&lt;&lt; </a>
			<div id="box">
				<div id="leftnav" >
				
<!-- recipient start-->
				<div  class="portlet">
					<div class="portlet-header notes">Recipient</div>
						<div class="portlet-content">
						<table width="225" border="0">
						  <tr>
							<td width="89">ID:</td>
							<td width="120">MDA789564</td>
						  </tr>
						  <tr>
							<td>Name:</td>
							<td>John Wilson</td>
						  </tr>
						  <tr>
							<td>ABO/Rh:</td>
							<td>O/Pos</td>
						  </tr>
						  <tr>
							<td>Diagnosis:</td>
							<td>Leukemia</td>
						  </tr>
						  <tr>
							<td>Planned Protocol:</td>
							<td>SCT-Allo</td>
						  </tr>
						  <tr>
							<td>Planned Infusion Date:</td>
							<td>Sep 2, 2011</td>
						  </tr>
		</table>

					</div>
					</div>
	<!-- recipient-->
				</div>
				</div>
			
			</div>
		</div>
</div>
<!-- section ends here-->
<div class="cleaner"></div>
<div id="forfloat_right">
<div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes previous">Available Products</div><br>
		<div class="portlet-content">
				<table cellpadding="0" cellspacing="0" border="0" id="availableProducts" class="display">
		<thead>
			<tr>
				<th colspan="1">Donor Type</th>
				<th colspan="1">Donor Name</th>
				<th colspan="1">Donor ID</th>
				<th colspan="1">Protocol Type</th>				
			</tr>
			<tr>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>ALLO-related</td>
				<td>Mary Wilson</td>
				<td>MDA789569</td>
				<td>HPC-A</td>
			</tr>
			<tr>
				<td>ALLO-related</td>
				<td>Harry Wilson</td>
				<td>MDA789569</td>
				<td>HPC-A</td>
			</tr>
			<tr>
				<td>ALLO-unrelated</td>
				<td>Lisa White</td>
				<td>MDA987421</td>
				<td>HPC-M</td>
			</tr>
			<tr>
				<td>ALLO-unrelated</td>
				<td>Lisa White</td>
				<td>MDA987421</td>
				<td>HPC-M</td>
			</tr>
			
		</tbody>
		</table>			
			</div>
		</div>
	</div>
	
	
	<div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes">Incoming Products</div><br>
		<div class="portlet-content">
		<table cellpadding="0" cellspacing="0" border="0" id="incomingProducts" class="display">
		<thead>
			<tr>
				<th colspan="1">Donor Type</th>
				<th colspan="1">Donor Name</th>
				<th colspan="1">Donor ID</th>
				<th colspan="1">Protocol Type</th>				
			</tr>
			<tr>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>AUTO</td>
				<td>John Wilson</td>
				<td>MDA789564</td>
				<td>HPC-A</td>
			</tr>
			<tr>
				<td>AUTO</td>
				<td>John Wilson</td>
				<td>MDA789564</td>
				<td>HPC-A</td>
			</tr>
		</tbody>
		</table>
			</div>
		</div>
	</div>
	
	<div id="arrivalDetails" style="display: none">
		<div class="table_row_input1">
						<div class="table_cell_input1">Planned Arrival Date:</div>
						<div class="table_cell_input2"><input type="text" id="date" placeholder="Planned Arrival Date" value="Sep 02, 2011" class="dateEntry"/></div>												
					</div>	
					<div class="table_row_input1">
						<div class="table_cell_input1">Planned Arrival Time:</div>
						<div class="table_cell_input2"><input type="text" id="date" placeholder="Planned Arrival Time" value="02:30" style="width:100px; font-size:1.0em;"/> &nbsp; &nbsp;<select name="" id="" class="sel_time" ><option>CST</option><option>PST</option></select></div>												
		</div>
    </div>
    </div>