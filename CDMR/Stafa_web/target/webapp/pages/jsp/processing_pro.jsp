<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/util.js"></script>
<%--//? mohan <script type="text/javascript" src="js/stafaQC.js"></script> --%>
<script type="text/javascript" src="js/placeholder.js"></script>
<!-- //?BB <script type="text/javascript" src="js/jquery/jquery-ui.js"></script>
-->
<%-- <% 
	String path = request.getContextPath()+"/pages/";
	System.out.println("path:::"+path);
		%> --%>
<script language="javascript">

function resetPage(){

		//alert("side");
	$("#recipientName").html("");
	$("#recipientId").html("");
	$("#recipientDiagnosis").html("");
	$("#recipientAboRh").html("");
	$("#donorName").html("");
	$("#donorId").html("");
	$("#donorAboRh").html("");
	$("#productId").html("");
	$("#productType").html("");	
	
	$("#refProcessingStation").val("");
	$("#refProcedure").val("");
	$("#refProcessingHood").val("");
	$("#refMethod").val("");
	
	//alert("qc");
	var chk_arr = $("#qcContent input[type=checkbox]");
	var chklength = chk_arr.length;
	//alert("chklength : "+chklength);
	for(y=0;y< chklength;y++){	
		var id = $(chk_arr[y]).attr("id");
		if($('#'+id).is(':checked')){
			//alert(id);
			chk_arr[y].checked = false;
			$(chk_arr[y]).trigger('click');
			chk_arr[y].checked = false;
			//alert("==>"+id);
		}
	} 
	//alert("pr")
	$("#refProcessingStation").val("");
	$("#refProcedure").val("");
	$("#refProcessingHood").val("");
	$("#refMethod").val("");
	$("#processing").val(0);
	$("#refProductId").val("");
	//alert("eq");
	$("#eqName").val("");
	$("#eqManufacture").val("");
	$("#eqSerNumber").val("");
	$("#eqExpDate1").val("");
	//alert("calc");
	$("#calculationDiv").html("");
	$.uniform.restore('select');
    $("select").uniform();
}

var name = "#floatMenu";
var menuYloc = null;
$(document).ready(function() {
	//alert("hi");
	//show_techslidewidgets();
	show_slidecontrol('sidecont');
	show_slidewidgets('pro_slidewidget');
	<%-- $("#innerwest").load('<%=path%>jsp/product_information.jsp'); --%>
	var oTable = $('#equipmentsTable').dataTable({
		"sPaginationType": "full_numbers",
		"sScrollX":"100%",
		"aoColumnDefs": [{"sTitle":"Equiment", "aTargets": [ 0]},
		                 {"sTitle":"Manufacturer","aTargets": [ 1]},
		                 {"sTitle":"Serial#", "aTargets": [ 2]},
		                 {"sTitle":"Expiration Date", "aTargets": [ 3]},
		                 {"sTitle":"Appearance OK", "aTargets": [ 4]}
		]
	});
	 var oTable1 = $('#reagentsTable').dataTable({
		"sPaginationType": "full_numbers",
		"sScrollX":"100%",
		"aoColumnDefs": [{"sTitle":"Reagents And Supplies", "aTargets": [ 0]},
		                 {"sTitle":"Quantity","aTargets": [ 1]},
		                 {"sTitle":"Manufacturer", "aTargets": [ 2]},
		                 {"sTitle":"Lot #", "aTargets": [ 3]},
		                 {"sTitle":"Expiration Date", "aTargets": [ 4]},
		                 {"sTitle":"Appearance OK", "aTargets": [ 5]}
		]
	}); 
	 var url = "getQc";
		var response = ajaxCall(url,"processingForm");
		generateTableFromJsnList(response);
		
		var product = $("#product").val();
		//BB testing 
		//product="bmt1"
		 if(product !=""){
			 //alert(product);
			 $("#fake_conformProductSearch").val(product);
			 $("#conformProductSearch").val(product);
			 var keyEvent = jQuery.Event("keyup");
			 keyEvent.keyCode=$.ui.keyCode.RIGHT;
			 $("#conformProductSearch").focus();
			 $(".barcodeSearch").trigger(keyEvent);
			 
			 var keyEvent1 = jQuery.Event("keyup");
			 keyEvent1.keyCode=$.ui.keyCode.ENTER;
			 $(".barcodeSearch").trigger(keyEvent1);

			 $("#productSearch").val(product);
			 $("#productSearch").trigger(keyEvent1);
		}


	// check with Mari
	/*
	document.getElementById('equipmentsTable_next').setAttribute("onClick","removeTableClass()");
	document.getElementById('equipmentsTable_previous').setAttribute("onClick","removeTableClass()");
	document.getElementById('reagentsTable_next').setAttribute("onClick","removeTableClass()");
	document.getElementById('reagentsTable_previous').setAttribute("onClick","removeTableClass()");
	
	removeTableClass();
	
	
	//removeTableClassOnChange("equipmentsTable_length");
	//removeTableClassOnChange("reagentsTable_length");
	*/

	// calculations
//?BB	
 	/* var rbcv = round2Decimal((Number($("#tmptotalVolume").val()) * Number($("#tmphct").val()))/100); 
	var maxPVR = Number($("#tmptotalVolume").val() - Number(rbcv));
	var minPVR = round2Decimal(Number(maxPVR)*0.8);
	$("#maxPlasma").val(maxPVR);
	$("#minPlasma").val(minPVR);
	 */ 
	 //alert("ssss");
	 /*menuYloc = parseInt($(name).css("top").substring(0,$(name).css("top").indexOf("px")))
	 alert("ssss1");
		$(window).scroll(function () { 
			offset = menuYloc+$(document).scrollTop()+"px";
			$(name).animate({top:offset},{duration:500,queue:false});
		});
		*/
	 //alert("ssss3");
});
function saveProcessings(){
	//alert("save Method");
	
	var recipientId = $("#leftnav #recipientId").val();
	var productSearch = $("#fake_conformProductSearch").val();
	var product = $("#newProductId").val()
	
	if(product==null || product=="" || product==0){

		alert("Please Scan/Enter valid Product Id");
		return;
		
		}
	//alert($("#processing").val());
	if($("#processing").val()>0){
		alert("Product is already processed.");
		return;
		}
	//alert("s");
	//return;
	var chk_arr = $("#qcContent input[type=checkbox]");
	var chklength = chk_arr.length;
	//alert("qcCheckList : "+qcCheckList);
	var qcData = "";
	var refProductProtocol = $("#fkProtocol").val();
	for(y=0;y< chklength;y++){	
		var id = $(chk_arr[y]).attr("id");
		if($('#'+id).is(':checked')){
				qcData+= "{productQc:0,refQc:"+id+",processing:''},";			

			}
	} 

	if(qcData.length >0){
		qcData = qcData.substring(0,(qcData.length-1));
    }
	 
	 var rowData = "";
	
	 var quantity;
	 var manufacture;
	 var lot;
	 var exDate="";
	 var expirationDate;
	 var appearance;
	 var isHide =0;
	 var reagentSupplies;
	 $("#reagentsTable tbody tr").each(function (row){
		 isHide =0;
	 	$(this).find("td").each(function (col){
		 		 		
		 	   if(col ==0){
			 		reagentSupplies= $(this).find("#reagentSupplies").val();
			 	 }else if(col ==1){
			 		quantity=$(this).text();
			 	 }else if(col ==2){
			 		manufacture=$(this).find("#qcManufacture").val();
			 	 }else if(col ==3){
			 		lot=$(this).find("#lotNumber").val();
			 	 }else if(col ==4){
			 		exDate=$(this).find(".dateEntry").val();
			 	 }else if(col ==5){
			 		 if($(this).find("#yes").is(":checked")){
			 			appearance = 1;
			 		 }else{
			 			appearance = 0;
			 		 }
			 	 }
		  });
	 	
	 	
	 		if(reagentSupplies!= undefined && quantity != undefined){

		 	  rowData+= "{productReagntSupp:0,refReagntSupp:"+reagentSupplies+",appearance:"+appearance+",quantity:'"+quantity+"',lot:'"+lot+"',manufacturer:'"+manufacture+"',expiration_date:'"+exDate+"',processing:''},";

	 		}
	 		//alert(rowData);
	 });
	
	 if(rowData.length >0){
            rowData = rowData.substring(0,(rowData.length-1));
        }

	 var rowData1="";
	 var equipments;
	 var manufacturer;
	 var serialNumber;
	 var expirationDate;
	 var appearance;
	 var isHide =0;
	 $("#equipmentsTable tbody tr").each(function (row){
		 isHide =0;
	 	$(this).find("td").each(function (col){
	 		//alert(col + " / " +$(this).html()); 		
		 	   	if(col ==0){
			 		//equipments= $(this).text();
			 		equipments= $(this).find("#eqName").val();
		 	   	
			 	}else if(col ==1){
			 		manufacturer=$(this).find("#eqManufacture").val();
			 	//	alert(" inside col 1" + manufacturer );
			 	}else if(col ==2){
			 		serialNumber=$(this).find("#eqSerNumber").val();
			 	}else if(col ==3){
			 		expirationDate=$(this).find(".dateEntry").val();
			 	}else if(col ==4){
			 		 if($(this).find("#yes").is(":checked")){
			 			appearance = 1;
			 		 }else{
			 			appearance = 0;
			 		 }
			 	 }
		 	  // alert(equipments + " / " + manufacture );
		  });
	 	if(equipments!= undefined && manufacturer != undefined){
		 	  //rowData1+= "{equipmentsId:0,equipments:'"+equipments+"',appearance:"+appearance+",serialNumber:"+serialNumber+",manufacturer:'"+manufacturer+"',expirationDate:'"+expirationDate+"',processing:''},";
	 		rowData1+= "{equipmentsId:0,equipments:'"+equipments+"',appearance:'"+appearance+"',serialNumber:'"+serialNumber+"',manufacturer:'"+manufacturer+"',expirationDate:'"+expirationDate+"',processing:''},";
 		}
	 		
	 		//alert(equipments + " / " + manufacturer + " / " + rowData1 );
	 });

		 if(rowData1.length >0){
			 rowData1 = rowData1.substring(0,(rowData1.length-1));
	     }
     
        url="saveProcessing";
		var results=JSON.stringify(serializeFormObject($('#processingForm')));
		results = results.substring(1,(results.length-1));

		var calculations =JSON.stringify(serializeFormObject($('#frmCalculation'))); 
		//alert("calculations " + calculations);
		var jsonData = "jsonData={reagentSuppliesData:["+rowData+"],qcData:["+qcData+"],equipments:["+rowData1+"],calculations:["+calculations+"],processingData:{"+results+"},nextOption:'"+$("#nextOption").val()+"'}";
	   
		//alert(jsonData);
		$('.progress-indicator').css( 'display', 'block' );
		  response = jsonDataCall(url,jsonData);
		 
		 // alert("response "+response.returnMessage);
		  var res = response.returnMessage;
           if(res !=""){
          		 var url = res;
          		 loadAction(url);
           }
           $('.progress-indicator').css( 'display', 'none' );  
		/*//?BB 
		var nextToOriginalProduct = $("#nextToOriginalProduct").val();

		//alert("jsonData={reagentSuppliesData:["+rowData+"],qcData:["+qcData+"],equipments:["+rowData1+"],"+results+"}");
	    var response = jsonDataCall(url,"jsonData={reagentSuppliesData:["+rowData+"],qcData:["+qcData+"],equipments:["+rowData1+"],"+results+"}");
		

		alert("Data Saved Successfully");
		/*var result = ajaxCall('getProcessing','');
		alert(result);
	    $("#equipmentdiv").replaceWith($('#equipmentdiv', $(result)));
	    $("#reagentsdiv").replaceWith($('#reagentsdiv', $(result)));
	    $("#qcContent").replaceWith($('#qcContent', $(result)));
	    $("#table_inner_wrapper").replaceWith($('#table_inner_wrapper', $(result)));
		
		loadAction('getProcessing.action');
		*/
}


function generateTableFromJsnList(response){
	var arr = [];
	var pkCodeList = [];
	$.each(response.qcList, function(i,v) {
		pkCodeList.push(v[0]);
		arr.push(v[3]);		
	 });
	var tab="<table style='width:100%'><tr>"
		 $.each(arr, function(i,val){
		var k=i+1;
		if((i%4==0)&&(i!=0)){
					
					tab+="</tr><tr><td>";	
					tab+="<input type='checkbox'  name='ABO' id='"+pkCodeList[i]+"' onclick=\"javascript:return qcCalculation('processingForm');\">&nbsp;<a href='#' onclick=\"javascript:showQcDetails('"+pkCodeList[i]+"','"+arr[i]+"')\">"+arr[i]+"</a></td>";
			
		}
			 else{
				 
					tab+="<td>";			
					tab+="<input type='checkbox'  name='ABO' id='"+pkCodeList[i]+"' onclick=\"javascript:return qcCalculation('processingForm');\">&nbsp;<a href='#' onclick=\"javascript:showQcDetails('"+pkCodeList[i]+"','"+arr[i]+"')\">"+arr[i]+"</a></td>";
		
			 }

		 });
	$("#qcContent").html(tab);
}

//?BB not in use
function generateTableFromJsnList1(response){
	$.each(response.recipientDonorList, function(i,v) {
		//alert(i+"=-="+v[0]);
		$("#recipientName").html(v[0]);
		$("#recipientId").html(v[1]);
		$("#recipientDiagnosis").html(v[2]);
		$("#recipientAboRh").html(v[3]);
		$("#donorName").html(v[4]);
		$("#donorId").html(v[5]);
		$("#donorAboRh").html(v[6]);
		$("#productId").html(v[7]);
		$("#productType").html(v[9]);
		//$("#fkProtocol").val(v[11]);
	 	});
	//var qcCheckList = [];
	var qcCheckList = Array(2); 
	$.each(response.protocolQcList, function(i,v) {
		qcCheckList.push(v[2])
	 	});
 	
	var chk_arr = $("#qcContent input[type=checkbox]");
	var chklength = chk_arr.length; 
	///alert("qcCheckList : "+qcCheckList);
	//alert("chklength : "+chklength);
	for(var m=0;m<chklength;m++)
	{	
		//alert("loop Start");
		var id = $(chk_arr[m]).attr("id");
		//alert("id : "+id);
		if (Search_Array(qcCheckList, id)){
			//alert("Found : "+id);
			chk_arr[m].checked = true;
			$(chk_arr[m]).trigger('click');
			$("#"+id).attr("checked","checked");
			}
			else{
				//alert("Not Found");
				chk_arr[m].checked = false;
			}
		
		//alert("loop End");
		//alert("k : "+m);
	} 
	var mappedProtocol = $("#fkProtocol").val();
	//alert("mappedProtocol "+mappedProtocol);
	if(mappedProtocol !="" ){
		if(getProtocolCalculation(mappedProtocol,"calculationDiv")){
			//alert("end of calc");
			return true;
		}
	}
	
}
function Search_Array(ArrayObj, SearchFor){
	  var Found = false;
	  //alert(ArrayObj+"--"+SearchFor);
	  for (var i = 0; i < ArrayObj.length; i++){
	    if (ArrayObj[i] == SearchFor){
	      return true;
	      var Found = true;
	      break;
	    }
	    else if ((i == (ArrayObj.length - 1)) && (!Found)){
	      if (ArrayObj[i] != SearchFor){
	        return false;
	      }
	    }
	  }
	}
$(function(){
	$('#nextProtocolList').append("<option>Next Product</option><option>Logout</option>");
    $("select").uniform();
  });
function searctrigger(e,This,productSearch){
    if((e.keyCode==13 && productSearch!="")){
    $("#searchtxt").trigger(e);
    }
    if(e.keyCode ==8 && productSearch!="") {
        $("#searchtxt").trigger(e);
          return false;  
        }else if (e.keyCode !=8 && e.keyCode != 13 ){
            $("#searchtxt").trigger(e);
            return false;
        }
    return false;
}
function getProcessingData(e){
	var url = "getProcessingData";
	var searchUrl = "checkProcessingData";
	var productSearch = $("#fake_conformProductSearch").val();
	var result;
	if(e.keyCode==13 && productSearch!=""){
		$('.progress-indicator').css( 'display', 'block' );
		 result = jsonDataCall(searchUrl,"productId="+productSearch);
    	var listLength=result.processingList.length;
    	//alert("listLength::"+listLength);
		if(listLength <= 0){
        	alert("Product ID is not Found");
    		$("#newProductId").val(0);
        	$('.progress-indicator').css( 'display', 'none' );
        	resetPage();
        	return;
        }else{
        	if(result.processingData.length<=0){
        		$("#processing").val(0);
        		resetPage();
        	}
        	//alert( "product id " + result.processingList[0]);
        	$("#newProductId").val(result.processingList[0][0]);
        	$("#product").val(result.processingList[0][0]);
        	$("#fkProtocol").val(result.processingList[0][1]);
        	//alert(result.processingData.length);
        	
        	$(result.processingData).each(function(){
				//	alert(this.refProcessingStation + " / " + this.refProcedure + " / " +this.refProcessingHood + "/ " + this.refMethod );
					$("#refProcessingStation").val(this.refProcessingStation);
					$("#refProcedure").val(this.refProcedure);
					$("#refProcessingHood").val(this.refProcessingHood);
					//$("#refProcessingStation").val(this.processing);
					$("#refMethod").val(this.refMethod);
					$("#processing").val(this.processing);
					$("#refProductId").val(this.product);
					
					
				});
        	$.each(result.recipientDonorList, function(i,v) {
        		//alert(i+"=-="+v[0]);
        		$("#recipientName").html(v[0]);
        		$("#recipientId").html(v[1]);
        		$("#recipientDiagnosis").html(v[2]);
        		$("#recipientAboRh").html(v[3]);
        		$("#donorName").html(v[4]);
        		$("#donorId").html(v[5]);
        		$("#donorAboRh").html(v[6]);
        		$("#productId").html(v[7]);
        		$("#productType").html(v[9]);
        		//$("#fkProtocol").val(v[11]);
        	 	});
        	
        	var qcObj;
        	var qcFlag = false;
        	//alert("result.processingQc : "+result.processingQc);
        	$(result.processingQc).each(function(){
        		qcObj= $("#"+this.refQc);
        		qcObj.checked = true;
        		$(qcObj).attr("checked","checked");
        		qcFlag = true;
           	});
        	//alert("qcCheckList : "+result.protocolQcList);
        	/**Start*/
			var qcCheckList = Array(2); 
			if(result.protocolQcList!=null && result.protocolQcList!=""){
				$.each(result.protocolQcList, function(i,v) {
					qcCheckList.push(v[2])
				 	});
					
				var chk_arr = $("#qcContent input[type=checkbox]");
				var chklength = chk_arr.length; 
				//alert("qcCheckList : "+qcCheckList);
				//alert("chklength : "+chklength);
				for(var m=0;m<chklength;m++)
				{	
					//alert("loop Start");
					var id = $(chk_arr[m]).attr("id");
					//alert("id : "+id);
					if (Search_Array(qcCheckList, id)){
						//alert("Found : "+id);
						qcFlag = true;
						chk_arr[m].checked = true;
						$(chk_arr[m]).trigger('click');
						$("#"+id).attr("checked","checked");
						}
						else{
							//alert("Not Found");
							chk_arr[m].checked = false;
						}
				
				}
			}
        	
        	/*Ends*/
           	
        	if(qcFlag){
				//alert("test");
				var qcflag2=qcCalculation('processingForm');
				 
				/*var qcflag2= $(qcObj).trigger('click',function(){
					alert("click");
					//qcFlag = false;
					return true;
				 });
				*/
				//alert("flag " + qcflag2);
					var regSupID; 
					var trObj;
				if(qcflag2){	
					$(result.processingSupp).each(function(){
						
						regSupID = this.refReagntSupp; 
						//alert("regSupID " + regSupID);
						trObj = $(":input[id='reagentSupplies'][value='"+regSupID+"']").parent().parent();
					//	alert("trObj " + $(trObj).html() + " / " + this.lot);
						$(trObj).find("#qcManufacture").val(this.manufacturer);
						$(trObj).find("#lotNumber").val(this.lot);
						$(trObj).find(".hasDatepicker").val(converDate(this.expiration_date));
					});
					
					//alert("end of click function ");
				}
				//alert("qcFlag " + qcFlag);
				$(qcObj).attr("checked","checked");
			}
        	
        	$(result.processingEquipment).each(function(){
					$("#eqName").val(this.equipments);
					$("#eqManufacture").val(this.manufacturer);
					$("#eqSerNumber").val(this.serialNumber);
//					alert(this.expirationDate + " / " +converDate(this.expirationDate) );
					$("#eqExpDate1").val(converDate(this.expirationDate));
					//?BB TO DO
					
					if(this.appearance == 1){
						
					}else{
						
					}
					//$("#eqApprance").val(this.refMethod);
				});
        	
        	if(constructProtocolCalculation(result.protocolCalcList,"calculationDiv")){
        		$(result.processingCalc).each(function(){
    				var tmpobj = $("[id='"+this.fieldName+"']");
    				//alert(this.fieldName + " / " + $(tmpobj).is('select') + " / " + $(tmpobj).is('input'));
    			//	if($(tmpobj).is('select')){
    				$(tmpobj).val(this.fieldValue);
    			});
        	}
        	$('.progress-indicator').css( 'display', 'none' );
        }
        //alert("s");
        $.uniform.restore('select');
        $("select").uniform();	
        $('.progress-indicator').css( 'display', 'none' );
        //alert("s1");	
        //?BB$('#searchtxt').val(productSearch);
      	//?BB searctrigger(e,productSearch);
		
      	/*//?BB not in use
		var response = ajaxCall(url,"");
		
		if(generateTableFromJsnList1(response)){
			//alert("end of page load");
			//	alert( "result " + result);
			
			};
		
		*/
		
	}
}
function removeTableClass(){
	
	$('#equipmentsTable_next').removeClass("ui-state-default");
	$('#equipmentsTable_previous').removeClass("ui-state-default");
	$('#reagentsTable_next').removeClass("ui-state-default");
	$('#reagentsTable_previous').removeClass("ui-state-default");

}
function getScanData(event,obj){
	if(event.keyCode ==13){
		getProcessingData(event);
	}
}
</script>

<div class="cleaner"></div>

<!--float window-->

<div id="pro_slidewidget" style="display:none;">
				
<!-- recipient start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header "><s:hidden id="moduleName" value="processing"/><s:hidden id="widgetName" value="recipient"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.recipient"/></div>
						<div class="portlet-content">
						<table width="225" border="0">
						  <tr>
							<td width="89"><s:text name="stafa.label.id"/></td>
							<td width="120"><div id="recipientId"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.name"/></td>
							<td><div id="recipientName"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientaborh"/></td>
							<td><div id="recipientAboRh"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientdiagnosis"/></td>
							<td><div id="recipientDiagnosis"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientrid"/></td>
							<td><div id="recipientRID"></div></td>
						  </tr>
						</table>

					</div>
					</div>
					</div>
				
<!-- recipient-->


<!-- Product start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header "><s:hidden id="moduleName" value="processing"/><s:hidden id="widgetName" value="product"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.product"/></div>
						<div class="portlet-content">
						<table width="225" border="0">
							<tr>
							<td width="89"><s:text name="stafa.label.id"/></td>
							<td width="120"><div id="productId"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.type"/></td>
							<td><div id="productType"></div></td>
						  </tr>
						</table>						
					</div>
				</div>
				</div>
				
<!-- Product-->

<!-- Donor start-->
					<div class="column">
					<div class="portlet">
						<div class="portlet-header "><s:hidden id="moduleName" value="processing"/><s:hidden id="widgetName" value="donnor"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.donor"/></div>
						<div class="portlet-content">
							<table width="225" border="0">
								<tr>
									<td width="89"><s:text name="stafa.label.id"/></td>
									<td width="120"><div id="donorId"></div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.name"/></td>
									<td><div id="donorName"></div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.donoraborh"/></td>
									<td><div id="donorAboRh"></div></td>
								</tr>
							</table>
						</div>
					</div>
					</div>

<!-- Donor-->
				
</div>
			
<!--float window end-->
<div id="sidecont" style="display:none">
<div class="" >
<ul class="ulstyle">
<li class="listyle print_label" ><b>Label</b> </li>
<li class="listyle print_label"><img src = "images/icons/print_small.png"  style="width:30px;height:30px;"/></li>
<li class="listyle print_label" ><b>Calendar</b> </li>
<li class="listyle print_label"><img src = "images/icons/calendar.png"  style="width:30px;height:30px;"/></li>
<li class="listyle notes_icon"><b>Notes</b> </li>
<li class="listyle notes_icon"><img src = "images/icons/notes.png"  style="width:30px;height:30px;"/></li>

</ul>
</div>
</div>
<!--right window start -->	
<form action="" id="processingForm" name="processingForm">
 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header"><s:text name="stafa.label.processingstationsetup"/></div><br>
		<div class="portlet-content">
			<s:hidden name="newProductId" id="newProductId" value="%{newProductId}"/>
			<s:hidden name="product" id="product" value="%{product}"/>
			<input type="hidden" name="selectedQC" id="selectedQC" value="" />
 			<input type="hidden" name="qctype" id="qctype" value="" />
 			<s:hidden name="protocol" id="fkProtocol" value="%{fkProtocol}"/> 
 			
				<div id="table_inner_wrapper">
				<div id="button_wrapper">
			 	<s:hidden id="processing" name="processing" value="0"/>
			
				<s:hidden id="refProductId" name="refProductId" value="0"/>
				<div style="margin-left:40%;">
							<input class="scanboxSearch barcodeSearch" type="text" size="15" value="" name="productSearch" id="conformProductSearch" placeholder="Scan/Enter Product ID"/>
							<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" />
							</div>
							<input type="text"  id="emptyId" onkeyup="" style="display:none"/>
						</div>
					<div id="left_table_wrapper">
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
							<tr>
								<td width="52%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.processingstation"/></div></td>
								<td width="50%" height="40px"><s:select   headerKey="" headerValue="Select"   list="processingStatioList" listKey="pkCodelst" listValue="description" name="refProcessingStation" id="refProcessingStation" /></td>
							</tr>
							<tr>
								<td width="52%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.procedure"/></div></td>
								<td width="50%" height="40px"><s:select   headerKey="" headerValue="Select"   list="procedureList" listKey="pkCodelst" listValue="description" name="refProcedure" id="refProcedure" /></td>
							</tr>							
						</table>
					</div>
					
					<div id="right_table_wrapper">
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
							<tr>
								<td width="52%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.processinghood"/></div></td>
								<td width="50%" height="40px"><s:select   headerKey="" headerValue="Select"   list="processingHoodList" listKey="pkCodelst" listValue="description" name="refProcessingHood" id="refProcessingHood" /></td>
							</tr>
							<tr>
								<td width="52%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.method"/></div></td>
								<td width="50%" height="40px"><s:select   headerKey="" headerValue="Select"   list="methodList" listKey="pkCodelst" listValue="description" name="refMethod" id="refMethod" /></td>
							</tr>							
						</table>						
					</div>
				</div>
			</div>
		</div>
	</div>	
	</form>	
<div class="cleaner"></div>

<form action="#" id="frmQC">
 <input type="hidden" name="selectedQC" id="selectedQC" value="" />
 <input type="hidden" name="qctype" id="qctype" value="" />

<div class="column">		
		<div  class="portlet">
			<div class="portlet-header "><s:hidden id="moduleName" value="processing"/><s:hidden id="widgetName" value="QC"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.qc"/></div>		
		<div id="qcContent" class="portlet-content">
			
					
				</div>			
				
			</div>
		</div>
			
	
</form>		
<section>

 <div class="cleaner"></div>
 
 <div class="column">
		<div  class="portlet">
			<div class="portlet-header "><s:hidden id="moduleName" value="processing"/><s:hidden id="widgetName" value="reagentsandSupplies"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.regandandsupplies"/></div>
			<div class="portlet-content">
			<div id="reagentsdiv">
			<table cellpadding="0" cellspacing="0" border="0" id="reagentsTable" class="display">
					<thead>
						<tr>
							<th colspan="1"><s:text name="stafa.label.regandandsupplies"/></th>
							<th colspan="1"><s:text name="stafa.label.quantity"/></th>
							<th colspan="1"><s:text name="stafa.label.manufacturer"/></th>
							<th colspan="1"><s:text name="stafa.label.lot"/></th>				
							<th colspan="1"><s:text name="stafa.label.expirationdate"/></th>
							<th colspan="1"><s:text name="stafa.label.appearanceok"/></th>
						</tr>
					</thead>
					<tbody>
						<s:iterator value="reagentsList" var="qcObject" status="row">
						<tr>
							<td><input type="hidden" id="reagentSupplies" value="<s:property value="reagentSuppliers"/>"/><s:property value="qcDesc"/></td>
							<td><s:property value="qcQuanitiy"/></td>
							<td><input type="text" id="qcManufacture" /></td>
							<td><input type="text" id="lotNumber"/></td>
							<td><input type="text" id="expDate[<s:property value="%{#row.index}"/>]" name="expDate" class="dateEntry"/></td>
							<td align="center">
							<input type="radio" id="yes" name="apprance[<s:property value="%{#row.index}"/>]" class=""/> Y &nbsp; <input type="radio" id="No" name="apprance[<s:property value="%{#row.index}"/>]" onClick="confirmIfNo();" class=""/> N
							</td>						
						</tr>
					</s:iterator>						
					</tbody>
			</table>
			</div>
	</div>
	</div>	
</div>		
	
</section>	


<section>

 <div class="cleaner"></div>
 
 <div class="column">	
 	<form>	
		<div  class="portlet">
			<div class="portlet-header "><s:hidden id="moduleName" value="processing"/><s:hidden id="widgetName" value="Equipments"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.equipments"/></div>
			<div class="portlet-content">
			<div id="equipmentdiv">
			<table cellpadding="0" cellspacing="0" border="0" id="equipmentsTable" class="display">
					<thead>
						<tr>
							<th colspan="1"><s:text name="stafa.label.equipment"/></th>
							<th colspan="1"><s:text name="stafa.label.manufacturer"/></th>
							<th colspan="1"><s:text name="stafa.label.serial"/></th>
							<th colspan="1"><s:text name="stafa.label.expirationdate"/></th>				
							<th colspan="1"><s:text name="stafa.label.appearanceok"/></th>
						</tr>
					</thead>
					<tbody>
						
						<tr>
							<td><input type="text" id="eqName"/></td>
							<td><input type="text" id="eqManufacture"/></td>
							<td><input type="text" id="eqSerNumber"/></td>
							<td><input type="text" id="eqExpDate1" name="eqExpDate" class='dateEntry'/></td>
							<td align="center">
							<input type="radio" id="yes" name="eqApprance" class="" /> Y &nbsp; <input type="radio" id="No" name="eqApprance" onClick="confirmIfNo();" class=""/> N
							</td>	
						</tr> 
											
					</tbody>
			</table>
			</div>
	</div>
<!-- 	<div align="right" style="float:right;">
		<form>				
			<input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;&nbsp;
			<input type="button" value="Next"/>
		</form>
		</div>
-->	</div>	
</form>
</div>		
</section>	


<div class="cleaner"></div>


<%-- <form action="#" id="login">
<div class="column">		
		<div  class="portlet">
				<div class="portlet-header notes"><s:hidden id="moduleName" value="processing"/><s:hidden id="widgetName" value="equipmentSetup"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.equipmentsetup"/></div>
			<div class="cleaner"></div>
		<div class="portlet-content">
			<div id="table_inner_wrapper">
			<div id="left_table_wrapper">
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
					<tr>
						<td width="52%" height="40px" valign="middle"><div class="txt_align_label" align="left"><s:text name="stafa.label.program"/></div></td>
						<td width="48%" height="40px" valign="middle"><div class="mainselection"><select name="" id="" class=""><option>Select</option></select></div></td>
					</tr>
					<tr>
						<td width="52%" height="40px" valign="middle"><div class="txt_align_label" align="left"><s:text name="stafa.label.speed"/></div></td>
						<td width="48%" height="40px" valign="middle"><div align="left"><input type="text" id="time" placeholder="3000" /></div></td>
					</tr>
				</table>
			</div>
			
			<div id="right_table_wrapper">
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
					<tr>
						<td width="52%" height="40px" valign="middle"><div class="txt_align_label" align="left"><s:text name="stafa.label.excesspressure"/></div></td>
						<td width="48%" height="40px" valign="middle"><div align="left"><input type="text" id="time" placeholder="" maxlength="100"/></div></td>
					</tr>
					<tr>
						<td width="52%" height="40px" valign="middle"><div class="txt_align_label" align="left">&nbsp;</div></td>
						<td width="48%" height="40px" valign="middle"><div align="left">&nbsp;</div></td>
					</tr>
				</table>
			</div>
		</div>
			
			</div>
		</div>
	</div>
</form>	 --%>
	<div class="cleaner"></div>
	
	 <form id="frmCalculation">
<div class="column">	
<div id="calculationDiv" class="table_inner_wrapper">
				</div>	
		
	</div>
</form> 
	<!-- 
<div class="column">		
		<div  class="portlet">
			<div class="portlet-header"><s:text name="stafa.label.volumereductioncalculation"/></div><br>
		<div class="portlet-content">
		<form action="#" id="login">
				<div id="table_inner_wrapper">
					<div id="left_table_wrapper">					
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
							<tr>
								<td width="52%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.maxplasma"/></div></td>
								<td width="48%" height="40px"><div align="left"><input type="text" id="maxPlasma" name="maxPlasma" placeholder="" maxlength="100" value=""/></div></td>
							</tr>
						</table>					
					</div>	

					
					<div id="right_table_wrapper">					
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
							<tr>
								<td width="52%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.minplasma"/></div></td>
								<td width="48%" height="40px"><div align="left"><input type="text" id="minPlasma" name="minPlasma" placeholder="" maxlength="100" value=""/></div></td>
							</tr>
						</table>					
					</div>
				</div>
		   </form>			
			</div>
		</div>
	</div>	
	-->
<div class="cleaner"></div>

<section>

<div class="cleaner"></div>
<div align="right" style="float:right;">
		<form>
				<table cellpadding="0" id="nextdropdown"cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td>Next:</td>
				<td><div class="mainselection" > <select name="nextOption" id="nextOption">
                 	<option value="select">Select</option>
               		<s:iterator value="nextOptions" status="row" var="cellValue">  
	               		<s:if test="%{#cellValue[2] == 0}">
	                 		<option value="<s:property value="#cellValue[1]"/>"> <s:property value="#cellValue[0]"/></option>
	                 	</s:if>
	                 </s:iterator>
                 </select></div></td>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" value="Next" onClick="saveProcessings();"/></td>
				</tr>
				</table>
				</form>
		</div>
	<!--  //?BB	
		<div align="right" style="float:right;">
		<form>
				<b>Processing Page:&nbsp;&nbsp;<a href="#" id="processing" class="cursor" onclick="javascript:loadAction('getProcessing');" >1</a>&nbsp;&nbsp;&nbsp;<a id="processing2" href="#" class="cursor" onclick="javascript:loadAction('getProcessing2');" >2</a></b>&nbsp;&nbsp;
			
			<input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;&nbsp;
			<input type="button" value="Next"  onclick="saveProcessings();"/>
		</form>
		</div>
	-->	
		<div class="cleaner"></div>
</section>
<script>
$(function() {
    $('.jclock').jclock();
    $( ".column" ).sortable().sortable({
        connectWith: ".column"
    });
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
   



    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

    $('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});   
});
</script>