<%@ include file="common/includes.jsp" %>
<!-- <script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script> -->
<script type="text/javascript" src="js/dataSearch.js"></script>
<script type="text/javascript" src="js/ajaxfileupload.js"></script>
<script type="text/javascript" src="js/jquery/jquery.Print.js"></script>
<script type="text/javascript" src="js/jquery/jquery.printElement.js"></script>

<link rel="stylesheet" type="text/css" href="css/ui/base/jquery.ui.tabs.css" media="screen" />
 
<script language="javascript">
function checkCVC(){
	var CVCflag=false;
	if($("#Vcvcdry").is(':checked')){
		$("#cvcrequired").hide();
		CVCflag=true;
	}else if($("#Vcvcclea").is(':checked')){
		$("#cvcrequired").hide();
		CVCflag=true;
	}else if($("#Vcvcintact").is(':checked')){
		$("#cvcrequired").hide();
		CVCflag=true;
	}else if($("#Vcvcsoiled").is(':checked')){
		$("#cvcrequired").hide();
		CVCflag=true;
	}else if($("#Vcvcna").is(':checked')){
		$("#cvcrequired").hide();
		CVCflag=true;
	}
	else {
		CVCflag=false
		$("#cvcrequired").show();
		$("#Vcvcdry").focus();
	}
//	alert("CVCflag " + CVCflag);
	return CVCflag;
}

function checkDonorStatus(){
	var CVCflag=false;
	if($("#Vdonorstatusnone").is(':checked')){
		$("#donorrequired").hide();
		CVCflag=true;
	}else if($("#Vdonorstatuspain").is(':checked')){
		$("#donorrequired").hide();
		CVCflag=true;
	}else if($("#Vdonorstatusnause").is(':checked')){
		$("#donorrequired").hide();
		CVCflag=true;
	}else if($("#Vdonorstatusdiarrhea").is(':checked')){
		$("#donorrequired").hide();
		CVCflag=true;
	}else if($("#Vdonorstatusfebrile").is(':checked')){
		$("#donorrequired").hide();
		CVCflag=true;
	}else {
		CVCflag=false
		$("#donorrequired").show();
		$("#Vcvcdrydonornone").focus();
	}
	//alert("donor CVCflag " + CVCflag);
	return CVCflag;
}


function savePages(mode){
	if(checkModuleRights(STAFA_VERIFICATION,APPMODE_SAVE)){
		try{
			//if(($("#personId").val()!="")&&(checkdonorflag!=false && checknurseflag!=false)){
			if(($("#personId").val()!="")&& checkdonormrn() && checknurseid()){			
				
				if($("#donorId").val()==""){
					alert("Please Enter the Donor MRN");
					return false;
				}else{
					if(save_documentReview()){
							
					} 
					if(saveVerfication(mode)){
						//alert("wait");
						
					}else{
						return false;
					}
					return true;
				}
			}/* else{
				alert("Please Verify Donor MRN/Nurse ID");
			} */
		}catch(e){
			alert("exception " + e);
		}
	}
}

function delete_documentReview(){
	//alert("delter doc review")
	if(checkModuleRights(STAFA_DOCUMENT,APPMODE_DELETE)){  
		var deletedIds="";
		try{
		$("#documentreviewTable tbody tr").each(function (row){
			 if($(this).find("#docid").is(":checked")){
				deletedIds += $(this).find("#docid").val() + ",";
			}
		 });
		//	alert("deletedIds "+ deletedIds);
		  if(deletedIds.length >0){
			 var yes=confirm(confirm_deleteMsg);
			 if(!yes){
							return;
			 }
			  deletedIds = deletedIds.substr(0,deletedIds.length-1);
			  //alert("deletedIds " + deletedIds);
			  jsonData = "{deletedIds:'"+deletedIds+"',domainName:'DOMAIN_DOCUMENT',domainKey:'DOCUMENT_ID'}";
			  url="commonDelete";
		     
		     response = jsonDataCall(url,"jsonData="+jsonData);
		 	var donor = $("#donor").val();
			var entityType = $("#entityType").val();
			constructDocumentTable(true,donor,entityType,'documentreviewTable');
		    
		 	
		  }
		}catch(err){
			alert(" error " + err);
		}
	}
}




function constructDocumentTable(flag,donor,type,tableId) {
		//alert("construct table : "+tableId+",Donor : "+donor);
			var criteria = "";
			if(donor!=null && donor!=""){
				criteria = donor ;
			}
			var documentColumn = [ { "bSortable": false},
				                	null,
				                	null,
				                	null,
				                	null,
				                	null
					             ];
														
			var documentServerParams = function ( aoData ) {
											aoData.push( { "name": "application", "value": "stafa"});
											aoData.push( { "name": "module", "value": "aphresis_DocumentReview"});
											//aoData.push( { "name": "criteria", "value": criteria});
											
											aoData.push( { "name": "param_count", "value":"1"});
											aoData.push( { "name": "param_1_name", "value":":DONOR:"});
											aoData.push( { "name": "param_1_value", "value":criteria});
											
											aoData.push( {"name": "col_1_name", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_DOCUMENTTYPE))" } );
											aoData.push( { "name": "col_1_column", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_DOCUMENTTYPE))"});
											
											aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
											aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
														
											
						};
			var documentaoColDef = [
						                  {		
						                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
					                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
					                	 			return "<input type='hidden' id='documentRef' name='documentRef' value='"+source[0] +"' > <input name='docid' id='docid' type='checkbox' value='"+source[0] +"' />" ;
					                	 			//return "";
					                	     }},
						                  { "sTitle": "Document Title",
					                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
						                	    	 //alert('docTitle'+source.documentFileName); 
						                	 		return source[6];
						                	 }},
										  { "sTitle": "Document Date",
						                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
												    return source[2];
												    }},
										  { "sTitle": "Attachment",
												 "aTargets": [3], "mDataProp": function ( source, type, val ) {
											 	   return source[1] + " &nbsp;&nbsp;<a href='#' onclick='openPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\")'> View </a>";
										  		}},
										  { "sTitle": "Reviewed",
										  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
										  			 if(source[3] ==1){
										  				 return "Reviewed";
										  			 }else{
										  				 return "";
										  			 }
											  		//return "<input type='checkbox'/>";
											  }},
										  { "sTitle": "Version #",
						                		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
												    return source[7];
												    }}
										];
			var documentColManager = function () {
													$("#documentreviewColumn").html($("#"+tableId+"_wrapper .ColVis"));
												};
												
			constructTable(flag,tableId,documentColManager,documentServerParams,documentaoColDef,documentColumn);
		} 
		
function constructDocumentHistoryTable(flag,donor,type,tableId) {
 	//alert("construct table : "+tableId+",Donor : "+donor);
	var criteria = "";
	if(donor!=null && donor!=""){
		criteria = " and fk_Entity ="+donor ;
	}
	var docHistoryaoColumn =  [ { "bSortable": false},
				                	null,
				                	null,
				                	null,
				                	null,
				                	null
					            ];
	
	var docHistoryServerParams = function ( aoData ) {
									aoData.push( { "name": "application", "value": "stafa"});
									aoData.push( { "name": "module", "value": "aphresis_DocumentReview_History"});
									aoData.push( { "name": "criteria", "value": criteria});
									
									aoData.push( {"name": "col_1_name", "value": "lower(DOCUMENT_FILENAME)" } );
									aoData.push( { "name": "col_1_column", "value": "lower(DOCUMENT_FILENAME)"});
									
									aoData.push( { "name": "col_2_name", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
									aoData.push( { "name": "col_2_column", "value": "lower(to_char(DOCUMENT_UPLOADEDDATE,'Mon DD, YYYY'))"});
												
									
				};
				
	var	docHistoryaoColDef = [
				                  {		
				                	  	"aTargets": [0], "mDataProp": function ( source, type, val ) {
			                	 			//return "<input type='checkbox' id='documentId' name='documentId' value='"+source[0] +"' >";
			                	 			return "<input type='hidden' id='documentRef' name='documentRef' value='"+source[0] +"' >";
			                	 			//return "";
			                	     }},
				                  { "sTitle": "Document Title",
			                	    	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
				                	    	 //alert('docTitle'+source.documentFileName); 
				                	 		return source[6];
				                	 }},
								  { "sTitle": "Document Date",
				                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
										    return source[2];
										    }},
								  { "sTitle": "Attachment",
										 "aTargets": [3], "mDataProp": function ( source, type, val ) {
									 	   return source[1] + " &nbsp;&nbsp;<a href='#' onclick='openPopup(\""+source[0]+"\",\""+source[4]+"\",\""+source[1]+"\")'> View </a>";
								  		}},
								  { "sTitle": "Reviewed",
								  		 "aTargets": [4], "mDataProp": function ( source, type, val ) { 
								  			 if(source[3] ==1){
								  				 return "Reviewed";
								  			 }else{
								  				 return "";
								  			 }
									  		//return "<input type='checkbox'/>";
									  }},
								  { "sTitle": "Version #",
				                		 "aTargets": [5], "mDataProp": function ( source, type, val ) { 
										    return source[7];
										    }}
								];
	
	var docHistoryColManager =  function () {
											$("#documentreviewColumn1").html($("#"+tableId+"_wrapper .ColVis"));
								};
								
	constructTable(flag,tableId,docHistoryColManager,docHistoryServerParams,docHistoryaoColDef,docHistoryaoColumn);

	$("#documentreviewTable1_wrapper").find("#"+tableId).removeAttr("style");
} 
/** File upload start*/


function save_documentReview(){
	//$('.progress-indicator').css( 'display', 'block' );
	try{
	var documentData = "";
	var donor = $("#donor").val();
	var entityType = $("#entityType").val();
	 $("#documentreviewTable tbody tr").each(function (row){
	 	var docsTitle="";
	 	var docsDate="";
	 	var fileData="";
	 	var fileUploadContentType="";
	 	var reviewedStatus="";
	 	var documentFileName="";
	 	var documentId="";
	 	if($(this).find(".filePathId").val()!=undefined){
		$(this).find("td").each(function (col){
			if(col ==0){
				documentId = $(this).find("#documentRef").val();
			}
			if(col ==1){
				docsTitle = $(this).find("#documentTitle").val();
			}
			if(col ==2){
				docsDate = $(this).find(".dateEntry").val();
			}
			if(col ==3){
				fileData = $(this).find(".filePathId").val();
				fileUploadContentType = $(this).find(".fileUploadContentType").val();
				fileData=fileData.replace(/\\/g,"/");
				documentFileName = $(this).find(".fileName").val();
				if(fileData!=undefined && fileData !="undefined" && fileData != ""){
					fileData = fileData.replace(/\\/g,"/");
				}
			}
		});
		if(fileData != undefined && fileData != ""){
			documentData+="{pkDocumentId:'"+documentId+"',fkEntity:'"+donor+"',entityType:'"+entityType+"',fkCodelstDocumentType:'"+docsTitle+"',documentFileName:\""+documentFileName+"\",documentUploadedDate:'"+docsDate+"',documentFile:\""+fileData+"\",fileUploadContentType:\""+fileUploadContentType+"\",reviewedStatus:'"+reviewedStatus+"'},";
		}
	 	}
	 });
	 	//alert("documentData " + documentData);
		if(documentData.length >0){
			documentData = documentData.substring(0,(documentData.length-1));
			var url="saveMultipleDocument";
			response = jsonDataCall(url,"jsonData={documentData:["+documentData+"]}");
			//alert("Data Saved Successfully");
			//stafaAlert('Data saved Successfully','');
			$('.progress-indicator').css( 'display', 'none' );
			constructDocumentTable(true,donor,entityType,'documentreviewTable');
			constructDocumentHistoryTable(true,donorVal,enType,'documentreviewTable1');
		}
	}catch(e){
		alert("Error : "+e);
		}
	 return true;
}


 var attachFileId=0;
 var dateId=0;
 function add_documentReview(){
	 if(checkModuleRights(STAFA_DOCUMENT,APPMODE_ADD)){
		 var docsTitle = $("#docsTitleTemp").html();
		 //alert(docsTitle);
	     var newRow ="<tr>"+
	     "<td><input type='hidden' id='documentRef' name='documentRef' value='0'/></td>"+
	     "<td><div><select id='documentTitle' name='documentTitle' style='width:200px;'>"+docsTitle+"</select></div></td>"+
	     "<td><input type='text' id='documentDate"+dateId+"' name='documentDate' class='dateEntry'/></td>"+
	   	 "<td><img src='images/file_loading.gif' id='fileUploadImage"+attachFileId+"' style='display:none'/>"+
	   	 "<div id='fileDisplayName"+attachFileId+"'></div><input type='hidden' class='fileName' id='fileName"+attachFileId+"' name='fileName"+attachFileId+"'/><input type='hidden' class='filePathId' name='filePathId"+attachFileId+"' id='filePathId"+attachFileId+"'/> <input type='hidden' class='fileUploadContentType' name='fileUploadContentType"+attachFileId+"' id='fileUploadContentType"+attachFileId+"'/>"+
	   	 "<div class='"+attachFileId+"' style='cursor: pointer;	color: #0101DF;' id='displayAttach"+attachFileId+"'></div><input style='display:block' class='"+attachFileId+"' type='file' name='fileUpload' onChange='ajaxFileUpload1(this,\""+attachFileId+"\")' id='attach"+attachFileId+"'/><span><s:text name='stafa.fileupload.message'/></span></td>"+
	     //"<td><input type='file' name='fileUpload' id='attachedFile"+attachFileId+"'/></td>"+
	     "<td></td>"+
	     "</tr>";
	     
	     $("#documentreviewTable tbody").prepend(newRow);
	     $("#documentDate"+dateId).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	         changeYear: true, yearRange: "c-50:c+45"});
	     dateId=dateId+1;
	     $.uniform.restore('select');
		 $('select').uniform();
		 $("#displayAttach"+attachFileId).click(function(){
			 var idSeq = $(this).attr("class");
			 $("#displayAttach"+attachFileId).hide();
			 $("#attach"+idSeq).click();
		});
		 attachFileId=attachFileId+1;
		 
		 var param ="{module:'APHERESIS',page:'VERIFICATION'}";
			markValidation(param);
			
	 } 
 }


 
 
 
 /***/
function saveVerfication(mode){
		  	 var url = "saveVerfication";
			/*var response = ajaxCall(url,"verificationform");
			 alert("Data saved succesfully.");
			$("#main").html(response);
	            }catch(err){
	                alert(" load action error " + err);
	            } */
		  	
		   var results=JSON.stringify(serializeFormObject($('#verificationform')));
			results = results.substring(1,(results.length-1));
			/*var results1=JSON.stringify(serializeFormObject($('#verificationform1')));
			results1 = results1.substring(1,(results1.length-1));*/
			
			//var jsonData = "jsonData={parameterData:["+rowData+"],processingData:["+results+"]}";
			
			var jsonData = "jsonData={VerificationData:{"+results+"},flowNextFlag:'" +$("#flowNextFlag").val()+"'}";
			//alert("jsonData==="+jsonData);
			if(mode=="next" && ( !checkCVC() || !checkDonorStatus())){
	  			return false;
	  		}
			
			  //  return;
				 var response = jsonDataCall(url,jsonData);
				 // alert(url);
	saveQuestions('interimprecollection');
	saveQuestions('preassessment');	
	saveQuestions('donorQC');
	 //alert("Data saved Successfully");
	stafaAlert('Data saved Successfully','');

	if($("#flowNextFlag").val()!="true"){
		var verification = $("#verification").val();
		if(verification ==0){
			var donor=$("#donor").val();
			var donorProcedure=$("#donorProcedure").val();
			var url = "loadVerification.action?donorProcedure="+donorProcedure +"&donor="+donor;
			loadAction(url);
		}
	}
 return true;
 }
 
var donormrn;

function getslide_widgetdetails(){

	 var donor=$("#donor").val();
	 var donorProcedure = $("#donorProcedure").val();
	 url="fetchDonorData";
	 var jsonData = "donor:"+donor+",donorProcedure:"+donorProcedure;
	 response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	 var donorDetails = response.donorDetails;
	 donormrn="";
	 donormrn=donorDetails.personMRN;
	 
	$("#slide_donorId").text(donorDetails.personMRN);
	$("#slide_donorDOB").text(donorDetails.dob);
	$("#slide_donorName").text(donorDetails.lastName + " " + donorDetails.firstName );
	$("#slide_procedure").text(donorDetails.plannedProceduredesc);
	var tcCheck = donorDetails.plannedProceduredesc.slice(-2);
 	if(tcCheck == "TC"){
 		$(".cd34").hide();
 		
 	}else{
 		$(".cd3").hide();
 	}
	$("#slide_donorWeight").text(donorDetails.personWeight);
	$("#slide_donorABORH").text(donorDetails.bloodGrpName);
	$("#slide_targetVol").text(donorDetails.targetProcessingVolume);
	$("#slide_donation").text(donorDetails.donorTypeName);
	$("#recipient_id").text(donorDetails.recipient_id);
	$("#recipient_name").text(donorDetails.recipient_name);
	$("#recipient_dob").text(donorDetails.recipient_dob);
	$("#recipient_aborh").text(donorDetails.recipient_bloodGroup);
	$("#recipient_diagnosis").text(donorDetails.diagonsis);
	$("#idms").text(donorDetails.idmsResult);
	var idmCheck = donorDetails.idmsResult;
	if(idmCheck=="Reactive"){
		$("#idms").addClass('idmReactive');
	}else if(idmCheck=="Pending"){
		$("#idms").addClass('idmPending');
	}
	$("#westnile").text(donorDetails.westNileDesc);
	$("#donorcd34").text(donorDetails.donorCD34);
	$("#cd34_day1").text(donorDetails.productCD34);
	$("#cum_CD34").text(donorDetails.productCUMCD34);
	$("#recipient_weight").text(donorDetails.recipient_weight);
	$("#slide_donorHeight").text(donorDetails.personHeight);
}

function setvalues(id){
    var s=$('#'+id).attr('checked');
    if(s=='checked'){
        $('#'+id).val(1);
    }else{
        $('#'+id).val(0);
    }
}

function displayLabTest(subTypVal,nextTD){
	var hiddenVal = $("#veriLabTest").val();
	var splitVal = (hiddenVal.split(","));
	var listValue;
	var pkValue;
	var rHtml="<tr>";
	for(s=0;s<splitVal.length;s++){
		$("#tempLabTest option").each(function(i){
			listValue = $(this).val();
			pkValue = (listValue.split("~-~"));
		if(pkValue[0] == "" || pkValue[0] == undefined){
			return;
		}else if(pkValue[0]== splitVal[s]){
					rHtml+="<td class=\"tablewapper\">";
					rHtml+="<input type=\"hidden\" name='"+pkValue[1]+"_id' id='"+pkValue[1]+"_id' value=\"0\"><input type=\"hidden\" name=\"questionsCode\" id=\"questionsCode\" value='"+pkValue[1]+"'/>";
					rHtml+="<input type=\"checkbox\" name='"+pkValue[1]+"' id='"+pkValue[1]+"' checked=\"checked\" value='1'/>&nbsp;"+$(this).text()+"</td>";
					
					rHtml+="<td class=\"tablewapper1\"><input type=\"hidden\" name='"+pkValue[1]+"ref_id' id='"+pkValue[1]+"ref_id' value=\"0\">";
					rHtml+="<input type=\"hidden\" name=\"questionsCode\" id=\"questionsCode\" value='"+pkValue[1]+"ref'/>";
					rHtml+="Ref #:&nbsp;<input type='text' id='"+pkValue[1]+"ref' name='"+pkValue[1]+"ref'/></td></tr>";
				}
		});
	}
	rHtml+="</tr>";
	//alert("rHTML::"+rHtml);
		$('#donorQCTable > tbody > tr:eq(0)').before(rHtml);
}
	
/*//?Muthu
	
	
	
	//alert(splitVal);
	//$("#porotocolQCTable tbody tr").each(function (row){
	constructLab(false);
	$('[name="pkCodelst"]').each(function(i,e) { 
		if(hiddenVal.indexOf(("," + $(this).val() +","))!=-1){
			alert("true");
			 $(this).attr('checked','checked');
		 }else{
			 $(this).removeAttr('checked');
		 }
		/*chkValues += $(this).val() + ",";
		 subTypVal.push($(this).siblings('input:hidden').val());
		 nextTD.push($(this).parent().siblings('td').html());*/
	/*});
	var rHtml="<tr>";
	var currentTD=0;
	for(s=0;s<splitVal.length;s++){
		
		alert("subTypVal:::::"+subTypVal[s]);
		if(subTypVal[s] == "undefined"||subTypVal[s] == undefined){
			alert("true");	
			return;
		}else{
				rHtml+="<td class=\"tablewapper\">";
				rHtml+="<input type=\"hidden\" name='"+subTypVal[s]+"_id' id='"+subTypVal[s]+"_id' value=\"0\"><input type=\"hidden\" name=\"questionsCode\" id=\"questionsCode\" value='"+subTypVal[s]+"'/>";
				rHtml+="<input type=\"checkbox\" name='"+subTypVal[s]+"' id='"+subTypVal[s]+"' checked=\"checked\" value='1'/>&nbsp;"+nextTD[s]+"</td>";
				
				rHtml+="<td class=\"tablewapper1\"><input type=\"hidden\" name='"+subTypVal[s]+"ref_id' id='"+subTypVal[s]+"ref_id' value=\"0\">";
				rHtml+="<input type=\"hidden\" name=\"questionsCode\" id=\"questionsCode\" value='"+subTypVal[s]+"_ref'/>";
				rHtml+="Ref #:&nbsp;<input type='text' id='"+subTypVal[s]+"_ref' name='"+subTypVal[s]+"_ref'/></td></tr>";
			}
		}
		/*$("#tempLabTest option").each(function(i){
			if(i!=0){
				if($(this).val()==splitVal[s]){
					currentTD=currentTD+1;				
					if(currentTD%2 == 0){
						rHtml+="<td class=\"tablewapper1\"><input type=\"checkbox\" checked=\"checked\" onclick=\"\" id=\"\" name=\"\">"+$(this).text()+"</td>";
					}
					else{
						rHtml+="<td class=\"tablewapper\"><input type=\"checkbox\" checked=\"checked\" onclick=\"\" id=\"\" name=\"\">"+$(this).text()+"</td>";
					}	
				}
			}
		});
	}
	rHtml+="</tr>";
	if(currentTD!=0){
		$('#donorQCTable > tbody > tr:eq(0)').before(rHtml);
	}*/
	/*$('#donorQCTable > tbody > tr:eq(0)').before(rHtml);
}*/

function loadDonorQcData(){
	var qcName = [];
	var pkCodeList = [];
	var subTyp = [];
	var donorProcedure = $("#donorProcedure").val();
	var url = "loadDonorQcData";
	var jsonData = "donorProcedure:"+donorProcedure;
	response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	$(response.existDonorQCList).each(function(i,v){
		pkCodeList.push(this.pkCodelst);
		qcName.push(this.description);
		subTyp.push(this.subType);
	});
	var tab="<tr>";
	for(s=0;s<response.existDonorQCList.length;s++){
				tab+="<td class=\"tablewapper\">";
				tab+="<input type=\"hidden\" name='"+subTyp[s]+"_id' id='"+subTyp[s]+"_id' value=\"0\"><input type=\"hidden\" name=\"questionsCode\" id=\"questionsCode\" value='"+subTyp[s]+"'/>";
				tab+="<input type=\"checkbox\" name='"+subTyp[s]+"' id='"+subTyp[s]+"' checked=\"checked\" value='1'/>&nbsp;"+qcName[s]+"</td>";
				
				tab+="<td class=\"tablewapper1\"><input type=\"hidden\" name='"+subTyp[s]+"ref_id' id='"+subTyp[s]+"ref_id' value=\"0\">";
				tab+="<input type=\"hidden\" name=\"questionsCode\" id=\"questionsCode\" value='"+subTyp[s]+"ref'/>";
				tab+="Ref #:&nbsp;<input type='text' id='"+subTyp[s]+"ref' name='"+subTyp[s]+"ref'/></td></tr>";
		 };
	tab+="<tr><td class=\"tablewapper\"><img onclick=\"addLab();\" style=\"width:16;height:16;cursor:pointer;\" src=\"images/icons/addnew.jpg\">&nbsp;&nbsp;Add Lab Test</td>";
	tab+="</tr>";
	$("#donorQCTable").html(tab);
}
function loadVerficationData(){
	 var url = "loadVerificationData";
	 var donor=$("#donor").val();
	 var donorProcedure = $("#donorProcedure").val();
		if(donor !=0 && donor != ""){
			var jsonData = "donor:"+donor+",donorProcedure:"+donorProcedure;
			response = jsonDataCall(url,"jsonData={"+jsonData+"}");
		}
		$(response.verificatioList).each(function(i){
			$("#verification").val(this.verification);
			$("#donorId").val(this.donorId);
			$("#cbcdRefNumber").val(this.cbcdRefNumber);
			$("#donorCd34RefNumber").val(this.donorCd34RefNumber);
			$("#westLiteRefNumber").val(this.donorCd34RefNumber);
			$("#cmpRefNumber").val(this.donorCd34RefNumber);
			$("#location").val(this.location)
			$("#donorNameVerbalVerified").val(this.donorNameVerbalVerified);
			
			if($("#donorNameVerbalVerified1").val()==1){
				$("#donorNameVerbalVerified1").prop("checked",true);
			}else{
				$("#donorNameVerbalVerified0").prop("checked",true);
			}
			$("#donorDobVerbalVerified").val(this.donorDobVerbalVerified);
			if($("#donorDobVerbalVerified1").val()==1){
				$("#donorDobVerbalVerified1").prop("checked",true);
			}else{
				$("#donorDobVerbalVerified0").prop("checked",true);
			}
			$("#westLite").val(this.westLite);
			
			if($("#westLite").val()==1){
				$("#westLite").prop("checked",true);
			}else{
				$("#westLite").prop("checked",false);
			}
			$("#cbcd").val(this.cbcd);
			if($("#cbcd").val()==1){
				$("#cbcd").prop("checked",true);
			}else{
				$("#cbcd").prop("checked",false);
			}
			$("#donorCd34").val(this.donorCd34);
			if($("#donorCd34").val()==1){
				$("#donorCd34").prop("checked",true);
			}else{
				$("#donorCd34").prop("checked",false);
			}
			$("#cmp").val(this.cmp);
			if($("#cmp").val()==1){
				$("#cmp").prop("checked",true);
			}else{
				$("#cmp").prop("checked",false);
			}
			$("#purpleBullet").val(this.purpleBullet);
			if($("#cmp").val()==1){
				$("#purpleBullet").prop("checked",true);
			}else{
				$("#purpleBullet").prop("checked",false);
			}
			$("#typeCheck").val(this.typeCheck);
			if($("#typeCheck").val()==1){
				$("#typeCheck").prop("checked",true);
			}else{
				$("#typeCheck").prop("checked",false);
			}
			$("#veriLabTest").val(this.veriLabTest);
		});
}

var enType;	
var verificationId;
var checkdonorflag=false;
var checknurseflag=false;
function loadVerificationdetails(){
	getslide_widgetdetails();
	$("#nurse_slidewidget1").html($("#nurse_slidewidget").html())
	show_slidewidgets("nurse_slidewidget1",false);
	$("#slidecontrol1").html($("#slidecontrol").html())
    show_slidecontrol("slidecontrol1");
	loadVerficationData();
	loadDonorQcData();
    displayLabTest(subTypVal,nextTD);
    loadquestions();
    constructDocumentTable(false,donorVal,enType,'documentreviewTable');
	constructDocumentHistoryTable(true,donorVal,enType,'documentreviewTable1');
    
}
function checkdonormrn(){
	checkdonorflag=false;
			 var enteredVal = $("input[name='donorId']").val();
			 var nurseentered=$("input[name='nurseID']").val();
			 //var nurseentered=$("input[name='nurseID']").val();
			 var url="checkVerificationData";
			 var verificationId;
			 var donor=$("#donor").val();
				 if(donor !=0 && donor != ""){
						var jsonData = "donor:"+donor;
						response = jsonDataCall(url,"jsonData={"+jsonData+"}");
					}
				 $("#personId").val("");
				 $(response.verificationCheckList).each(function(i,v){
					for(m=0;m<v.length;m++){
						verificationId = v[0];
						$("#personId").val(v[1]);
					} 
				 });
				
			 if(enteredVal==verificationId){
				$("#donorVerified").html('<div align="center"><img src = "images/icons/verified.png" class="cursor"/>Verified</div>');
		    	$("#donorVerified").show();
		    	checkdonorflag=true;
		    	
			 }else{
				 alert("Donor MRN Mismatch ");
				 checkdonorflag=false;
				// checknurseflag=false;
				 hide_slidewidgets();
				hide_slidecontrol();
				$("#donorVerified").html('<div align="center"><img src = "images/icons/delete.gif" class="cursor"/>Mismatch</div>');
				 // $("#nurseVerified").html('<div align="center"><img src = "images/icons/delete.gif" class="cursor"/>Mismatch</div>');
			    $("#donorVerified").show();
			    nurseentered = "";
			    $("#nurseVerified").html('');
			    $("#verificationform1").find(":input").each(function(){
			    	 switch(this.type) {
	   	 	            case 'text':
	   	 	            case 'textarea':
	   	 	                $(this).val('');
	   	 	                break;
	   	 	            case 'checkbox':
	   	 	            case 'radio':
	   	 	                this.checked = false;
	   	 	                break;
	   	 	        }
			    });
			      $("input[name='donorId']").val(enteredVal);
			      $("input[name='nurseID']").val(nurseentered);
			      
			    $("#fileAttachDiv").attr("style","display:block");
				$("#fileNameDiv").attr("style","display:none");
			 }
		return checkdonorflag;
}
function checknurseid(){
	checknurseflag=false;
	 var nurseentered=$("input[name='nurseID']").val();
	 var enteredVal = $("input[name='donorId']").val();
	 //alert("nurseentered::"+nurseentered);
	 var url="verifyCustomerId";
		 var jsonData = "{customerId:'"+nurseentered+"'}";
		 var response = jsonDataCall(url,"jsonData="+jsonData);
	 if(response.verifyStatus=="true"){
	
    	$("#nurseVerified").html('<div align="center"><img src = "images/icons/verified.png" class="cursor"/>Verified</div>');
    	$("#nurseVerified").show();
    	checknurseflag=true;
    	
	 }else{
		 alert("Nurse ID Mismatch");
		  $("#nurseVerified").html('<div align="center"><img src = "images/icons/delete.gif" class="cursor"/>Mismatch</div>');
			//$("#donorVerified").html('<div align="center"><img src = "images/icons/delete.gif" class="cursor"/>Mismatch</div>');
        
	    $("#nurseVerified").show();
	    enteredVal = "";
	    $("#donorVerified").html('');
	    hide_slidewidgets();
		hide_slidecontrol();
	    checknurseflag=false;
	    //checkdonorflag=false;
	    $("#verificationform1").find(":input").each(function(){
	    	 switch(this.type) {
	 	            case 'text':
	 	            case 'textarea':
	 	                $(this).val('');
	 	                break;
	 	            case 'checkbox':
	 	            case 'radio':
	 	                this.checked = false;
	 	                break;
	 	        }
	    });
	    $("input[name='donorId']").val(enteredVal);
	    $("input[name='nurseID']").val(nurseentered);
	    $("#fileAttachDiv").attr("style","display:block");
		$("#fileNameDiv").attr("style","display:none");
	 }
	 return checknurseflag;
}
//? Mohan function compareID(){
 /*$("input[name='donorId']").bind("keyup",function(e){
	 if(e.keyCode==13){
		
		
		 checkdonormrn();
		 if( checkdonorflag!=false && checknurseflag!=false){
			loadVerificationdetails();
		}
	 }
	});
$("input[name='nurseID']").bind("keyup",function(e){
	 if(e.keyCode==13){
		
		
		 checknurseid();
		 if( checkdonorflag!=false && checknurseflag!=false){
			loadVerificationdetails();
		}
	 }
	});
	
} */
var donorVal;
var donorplannedprocedure;
var nurseVal;
$(document).ready(function() { 
	$(".titleText").html("<s:text name='stafa.label.verification'/>");
	donorVal=$("#donor").val();
	nurseVal = $("#nurseHiddenID").val();
	var verification = $("#verification").val();
	showNotesCount(donorVal,"donor",0);
	var donorProcedure = $("#donorProcedure").val();
	donorplannedprocedure=$("#donorProcedure").val();
	//alert("donorVal " +donorVal+"donorproc::"+donorplannedprocedure);
	var entityId = donorProcedure +"," + donorVal;
	//alert("entityId" + entityId);
	showTracker(entityId,"donor","apheresis_nurse","false");
	show_innernorth();
	hide_slidewidgets();
	hide_slidecontrol();
	$('#inner_center').scrollTop(0);
	mandatory("Vdonorstatusnone");
	$("input[name='donorId']").bind("keyup",function(e){
		 if(e.keyCode==13){
			 checkdonormrn();
			 if( checkdonorflag!=false && checknurseflag!=false){
				loadVerificationdetails();
			}
		 }else{
			 checkdonorflag=false;
		 }
		});
	$("input[name='nurseID']").bind("keyup",function(e){
		 if(e.keyCode==13){
			 checknurseid();
			 if( checkdonorflag!=false && checknurseflag!=false){
				loadVerificationdetails();
			}
		 }else{
			 checknurseflag=false;
		 }
		});
	if(verification>0){
		/* getslide_widgetdetails();
		show_slidewidgets("nurse_slidewidget",false);
	    show_slidecontrol("slidecontrol");
	    loadVerficationData();
	    loadDonorQcData();
	    displayLabTest(subTypVal,nextTD);
	    loadquestions();
	    constructDocumentTable(false,donorVal,enType,'documentreviewTable');
		constructDocumentHistoryTable(true,donorVal,enType,'documentreviewTable1'); */
		loadVerificationdetails();
		checkdonormrn();
		$("#nurseID").val(nurseVal);
		checknurseid();
	}/* else{
		compareID();
	} */
	
 	
	/* 	oTable = $('#equipmentsTable').dataTable({
		"bJQueryUI": true,
		 "bRetrieve": true,
		
	}); */
	/* oTable = $('#documentConformation').dataTable({
													"sPaginationType": "full_numbers",
													"sScrollX":"100%",
													"bRetrieve": true,
													"bPaginate":true,
													"sDom": 'C<"clear">Rlfrtip',
													"aoColumns": [ { "bSortable":false},
			 										               null,
			 										               null,
			 										               null,
			 										               null
			 										             ],
			 										"oColVis": {
			 											"aiExclude": [ 0 ],
			 											"buttonText": "&nbsp;",
			 											"bRestore": true,
			 											"sAlign": "left"
			 										},
			 										"fnInitComplete": function () {
			 									        $("#documentColumn").html($('.ColVis:eq(0)'));
			 										}
	}); */

	var url = "getQc";
	var response = ajaxCall(url,"processingForm");
	generateTableFromJsnList(response);
	
	
	
	

	// check with Mari
	/*
	document.getElementById('equipmentsTable_next').setAttribute("onClick","removeTableClass()");
	document.getElementById('equipmentsTable_previous').setAttribute("onClick","removeTableClass()");
	document.getElementById('reagentsTable_next').setAttribute("onClick","removeTableClass()");
	document.getElementById('reagentsTable_previous').setAttribute("onClick","removeTableClass()");
	
	removeTableClass();
	
	
	//removeTableClassOnChange("equipmentsTable_length");
	//removeTableClassOnChange("reagentsTable_length");
	*/

	// calculations
/*?BB check
		var rbcv = round2Decimal((Number($("#tmptotalVolume").val()) * Number($("#tmphct").val()))/100); 
	var maxPVR = Number($("#tmptotalVolume").val() - Number(rbcv));
	var minPVR = round2Decimal(Number(maxPVR)*0.8);
	$("#maxPlasma").val(maxPVR);
	$("#minPlasma").val(minPVR);
*/	// $( "#tabs" ).tabs();
	//alert("ready");
	var param ="{module:'APHERESIS',page:'VERIFICATION'}";
	markValidation(param);
	
});



function generateTableFromJsnList(response){
	var arr = [];
	var pkCodeList = [];
	$.each(response.qcList, function(i,v) {
		pkCodeList.push(v[0]);
		arr.push(v[3]);		
	 });
	var tab="<table style='width:100%'><tr>"
		 $.each(arr, function(i,val){
		var k=i+1;
		if((i%4==0)&&(i!=0)){
					
					tab+="</tr><tr><td>";	
					tab+="<input type='checkbox'  name='ABO' id='"+pkCodeList[i]+"' onclick=\"javascript:qcCalculation('processingForm');\">&nbsp;<a href='#' onclick=\"javascript:showQcDetails('"+pkCodeList[i]+"','"+arr[i]+"')\">"+arr[i]+"</a></td>";
			
		}
			 else{
				 
					tab+="<td>";			
					tab+="<input type='checkbox'  name='ABO' id='"+pkCodeList[i]+"' onclick=\"javascript:qcCalculation('processingForm');\">&nbsp;<a href='#' onclick=\"javascript:showQcDetails('"+pkCodeList[i]+"','"+arr[i]+"')\">"+arr[i]+"</a></td>";
		
			 }

		 });
	$("#qcContent").html(tab);
}

function generateTableFromJsnList1(response){
	$.each(response.recipientDonorList, function(i,v) {
		//alert(i+"=-="+v[0]);
		$("#recipientName").html(v[0]);
		$("#recipientId").html(v[1]);
		$("#recipientDiagnosis").html(v[2]);
		$("#recipientAboRh").html(v[3]);
		$("#donorName").html(v[4]);
		$("#donorId").html(v[5]);
		$("#donorAboRh").html(v[6]);
		$("#productId").html(v[7]);
		$("#productType").html(v[9]);
		$("#fkProtocol").val(v[11]);
	 	});
	//var qcCheckList = [];
	var qcCheckList = Array(2); 
	$.each(response.protocolQcList, function(i,v) {
		qcCheckList.push(v[2])
	 	});
 	
	var chk_arr = $("#qcContent input[type=checkbox]");
	var chklength = chk_arr.length;             
	//alert("qcCheckList : "+qcCheckList);
	for(k=0;k< chklength;k++)
	{	

		var id = $(chk_arr[k]).attr("id");
		//alert("id : "+id);
		if (Search_Array(qcCheckList, id)){
			//alert("Found : "+id);
			chk_arr[k].checked = true;
			$(chk_arr[k]).trigger('click');
			$("#"+id).attr("checked","checked");
			}
			else{
				//alert("Not Found");
				chk_arr[k].checked = false;
			}
	} 
	var mappedProtocol = $("#fkProtocol").val();
	//alert("mappedProtocol "+mappedProtocol);
	getProtocolCalculation(mappedProtocol,"calculationDiv");
}
function Search_Array(ArrayObj, SearchFor){
	  var Found = false;
	  //alert(ArrayObj+"--"+SearchFor);
	  for (var i = 0; i < ArrayObj.length; i++){
	    if (ArrayObj[i] == SearchFor){
	      return true;
	      var Found = true;
	      break;
	    }
	    else if ((i == (ArrayObj.length - 1)) && (!Found)){
	      if (ArrayObj[i] != SearchFor){
	        return false;
	      }
	    }
	  }
	}
$(function(){
	$( "#tabs" ).tabs();

    $("select").uniform();
  });




function getProcessingData(e){
	var url = "getOriginalProductData";
	var productSearch = $("#productSearch").val();
	if(e.keyCode==13 && productSearch!=""){
		//alert("s : "+e.keyCode);
		var url = "getOriginalProductData";
		var response = ajaxCall(url,"processingForm");
		generateTableFromJsnList1(response);
	}
}



/*function getProcessingData(){
	//alert("s11111111");
	var url = "getOriginalProductData";
	var productSearch = $("#productSearch").val();
		var response = ajaxCall(url,"processingForm");
		generateTableFromJsnList1(response);
}*/
function removeTableClass(){

	$('#equipmentsTable_next').removeClass("ui-state-default");
	$('#equipmentsTable_previous').removeClass("ui-state-default");
	$('#reagentsTable_next').removeClass("ui-state-default");
	$('#reagentsTable_previous').removeClass("ui-state-default");

}
$("#printBut").click(function() {
	showPopUp('open','widgetcollect','Widgets','500','300');
});
/* function checkWidget(obj,widgetId){
	//alert(widgetId);
	if($(obj).attr("checked")=="checked"){
		//alert($(obj).html());
		cloneOb = $("#"+widgetId).html();
		//alert(cloneOb);
		$("#printDiv").append(cloneOb);
	}
} */
function printToDiv(){
	var cloneOb = [];
	var idArray = [];
	$("#printDiv").html("");
	$("#widgetcollect :checked").each(function(i) {
	    idArray.push($(this).attr("id"));
	    cloneOb.push( $("#"+idArray[i]).html());
	    $("#printDiv").append(cloneOb[i]);
	});
}
function closeWidgets(){
	$("#widgetcollect").dialog( "destroy" )
	}
function loadLabResult(){
	  showPopUp("open","labResults","Complete Lab Results","1130","660");
}


function constructLab(flag){
var addLabColDef = [
					 {
							"aTargets": [0], "mDataProp": function ( source, type, val ) { 
							    return "<input type='checkbox' id='pkCodelst' name='pkCodelst' value='"+source[1] +"'/><input type='hidden' id='subType' name='subType' value='"+source[2] +"'/>";
					     }},
	                  {"sTitle":"Code",
							"aTargets": [1], "mDataProp": function ( source, type, val ) {
								return source[0];
	                	 }},
					  {	"sTitle":"Description",
	                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
	                			 return "";
	                	  }}
					]; 
var addLabColumns = [{"bSortable": false},
		               null,
		               null
		               ];
var addLabServerParams = function ( aoData ) {
											aoData.push( { "name": "application", "value": "stafa"});
											aoData.push( { "name": "module", "value": "add_Lab_Test"});	
											
											aoData.push( {"name": "col_1_name", "value": "lower(codelst_desc)" } );
											aoData.push( { "name": "col_1_column", "value": "lower(codelst_desc)"});
											
											aoData.push( { "name": "col_2_name", "value": ""});
											aoData.push( { "name": "col_2_column", "value": ""});
										};
		constructTableWithoutColManager(flag,'addLabTable',addLabServerParams,addLabColDef,addLabColumns);
						
}
function addLab(){
	var splitVal =  new Array();
	var finalVal = new Array();
	var hiddenVal;
	if($("#veriLabTest").val()==""){
		showPopUp("open","addLabTest","Add Lab Test","600","550");
		constructLab(false);
	}else{
		hiddenVal = $("#veriLabTest").val();
		splitVal = (hiddenVal.split(","));
		for(s=0;s<splitVal.length;s++){
			$("#tempLabTest option").each(function(i){
				if($(this).val()==splitVal[s]){
					finalVal.push($(this).text());
				}
			});
		}
		showPopUp("open","addLabTest","Add Lab Test","600","550");
		constructLab(true);
	}
}	
function closePopup(){
	try{
    		$("#addLabTest").dialog("close");
  		}catch(err){
			alert("err" + err);
		}
}
var subTypVal = [];
var nextTD = [];
function saveLabTest(){
	var chkValues = "";
	$('[name="pkCodelst"]:checked').each(function(i,e) { 
			 chkValues += $(this).val() + ",";
			 subTypVal.push($(this).siblings('input:hidden').val());
			 nextTD.push($(this).parent().siblings('td').html());
	});
	chkValues = chkValues.substring(0, chkValues.length - 1);
	$("#veriLabTest").val(chkValues);
	displayLabTest(subTypVal,nextTD);
	closePopup();
}

</script>
<div id="slidecontrol1" style="display:none;">
</div>
<div id="nurse_slidewidget1" style="display:none;">
</div>
<div  id="slidecontrol" style="display:none;">
	<ul class="ulstyle">
	<span>
		<li class="listyle print_label"><img src = "images/icons/Bar_Code_32.png" onclick="loadBarcodePage();" style="width:30px;height:30px;cursor:pointer;"/></li>
		<li class="listyle print_label" ><b><s:text name="stafa.label.label"/></b> </li>
	</span>
	<span>
		<li class="listyle print_label"><img src = "images/icons/labresults.png"  style="width:30px;height:30px;cursor:pointer;" onclick='addNewPopup("Complete Lab Result","labResults","loadLabResult","700","1050");'/></li>
		<li class="listyle print_label" ><b><s:text name="stafa.label.labresult"/></b> </li>
	</span>
	<span>
		<li class="listyle notes_icon"  style="margin-left:18%"><img src = "images/icons/notes.png" onclick="notes(donorVal,'donor',this);"  style="width:30px;height:30px;cursor:pointer;"/><sup style="margin-left:2%"><span class='subspan'></span></sup></li>
		<li class="listyle notes_icon"><b><s:text name="stafa.label.notes"/></b> </li>
	</span>
	<span>	
		<li class="listyle medication_icon"><img src = "images/icons/capsule1.png" onclick="medicationpopup(donormrn,donorVal,donorplannedprocedure);" style="width:30px;height:30px;cursor:pointer;"/></li>
		<li class="listyle medication_icon"><b><s:text name="stafa.label.medication"/></b></li>
	</span>
	<span>
			<li class="listyle transfer_icon"><img src = "images/icons/transfer.png" onclick="taskAction(donorplannedprocedure,'transfer')"  style="width:30px;height:30px;cursor:pointer;"/></li>
			<li class="listyle transfer_icon" id="transfer_icon"><b><s:text name="stafa.label.transfer"/></b></li>
			
		</span>
		<span>
			<li class="listyle terminate_icon"><img src = "images/icons/terminated.png"  onclick="taskAction(donorplannedprocedure,'terminate')" style="width:30px;height:30px;cursor:pointer;"/></li>
			<li class="listyle terminate_icon"><b><s:text name="stafa.label.terminate"/></b></li>
		</span>
		
		<!-- <li class="listyle complete_icon"><b>Complete</b></li>
		<li class="listyle complete_icon"><img src = "images/icons/task_completed_icon.PNG"  style="width:30px;height:30px;cursor:pointer;"/></li> -->
	</ul>
</div>
<div id="nurse_slidewidget" style="display:none;">
				
<!-- Patient Info start-->
			<div class="column">
				<div  class="portlet">
					<div class="portlet-header "><s:text name="stafa.label.donorinformation"/></div>
						<div class="portlet-content">
							<table width="100%" border="0">
							  <tr>
								<td width="50%"><s:text name="stafa.label.donorid"/></td>
								<td width="50%" id="slide_donorId" name="slide_donorId" ></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.name"/></td>
								<td id="slide_donorName" name="slide_donorName"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.dob"/></td>
								<td id="slide_donorDOB" name="slide_donorDOB"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.donoraborh"/></td>
								<td id="slide_donorABORH" name="slide_donorABORH"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.procedure"/></td>
								<td id="slide_procedure" name="slide_procedure"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.donation"/></td>
								<td id="slide_donation" name="slide_donation"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.weights"/></td>
								<td id="slide_donorWeight" name="slide_donorWeight"></td>
							  </tr>
							   <tr>
								<td>Height</td>
								<td id="slide_donorHeight" name="slide_donorHeight"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.targetvolume"/></td>
								<td id="slide_targetVol" name="slide_targetVol"></td>
							  </tr>
							</table>
						</div>
				</div>
			</div>
				
<!-- Patient Info-->

<!-- Lab Results start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header "><s:text name="stafa.label.donorlab"/></div>
						<div class="portlet-content">
							<table width="100%" border="0">
								<tr>
									<td width="50%"><s:text name="stafa.label.idms"/></td>
									<td width="50%" id="idms"></td>
								</tr>
								<tr>
									<td width="50%"><s:text name="stafa.label.westnile"/></td>
									<td width="50%" id="westnile"> </td>
								</tr>
								<tr>
									<td width="50%"><span class="cd34"><s:text name="stafa.label.donorcd34"/></span>
									<span class="cd3"><s:text name="stafa.label.donorcd3"/></span></td>
									<td width="50%" id="donorcd34"> </td>
								</tr>
							</table>
						</div>
				</div>
				</div>
				
<!-- Lab Results-->
			<div class="column">
					<div class="portlet">
						<div class="portlet-header "><s:text name="stafa.label.productlab"/></div>
							<div class="portlet-content">
							<table width="100%" border="0">
								<tr>
									<td width="50%">
									<span class="cd34"><s:text name="stafa.label.cd34_day1"/></span>
									<span class="cd3"><s:text name="stafa.label.cd3_day1"/></span>
									</td>
									<td width="50%" id="cd34_day1"></td>
								</tr>
								<tr>
									<td width="50%">
									<span class="cd34"><s:text name="stafa.label.cum_CD34"/></span>
									<span class="cd3"><s:text name="stafa.label.cum_CD3"/></span>
									</td>
									<td width="50%" id="cum_CD34"></td>
								</tr>
							</table>
							</div>
					</div>
				</div>
<!-- Recipient Info start-->
				<div class="column">
					<div class="portlet">
						<div class="portlet-header "><s:text name="stafa.label.recipientinformation"/></div>
							<div class="portlet-content">
							<table width="100%" border="0">
							<tr>
								<td width="50%"><s:text name="stafa.label.recipientmrn"/></td>
								<td width="50%" id="recipient_id"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.name"/></td>
								<td id="recipient_name"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.dob"/></td>
								<td id="recipient_dob"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.aborh"/></td>
								<td id="recipient_aborh"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.diagnosis"/></td>
								<td id="recipient_diagnosis"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.weights"/></td>
								<td id="recipient_weight"></td>
							  </tr>
							  </table>
							</div>
							 
					</div>
				</div>
				
<!--Recipient Info end-->
				</div>
<div class="cleaner"></div>

<s:form id="verificationform" onsubmit="return avoidFormSubmitting();" >
<!-- <input type='button' value='Print' id='printBut'/> -->
<s:hidden id="verification" name="verification" value="%{verification}"/>
<s:hidden id="refdonor" name="refdonor" value="%{donor}"/>
<s:hidden id="donor" name="donor" value="%{donor}"/>
<s:hidden id="donorProcedure" name="donorProcedure" value="%{donorProcedure}"/>
<s:hidden id="procedure" name="procedure" value="%{procedure}"/>
<s:hidden id="veriLabTest" name="veriLabTest" value=""/>
<s:hidden id="personId"/>
<s:hidden id="nurseHiddenID" name="nurseHiddenID" value="%{nurseId}"/>
<span class='hidden'><s:select id="tempLabTest" style='display:none;' name="tempLabTest" list="verificationLabList" listKey="pkCodelst+'~-~'+subType+'~-~'+type" listValue="description"  headerKey="" headerValue="Select"/></span>
<div id='preCollection' class="column">
	<div  class="portlet">
	<div class="portlet-header">Pre-Collection Verification</div><br>
		<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" class="" align="center" border="1" width="100%">
				<tr>
					<td><div class="txt_align_label" style="font-weight:bold;"><s:text name="stafa.label.donorid"/></div></td>
					<td><div align="center" style="padding:1%;"><input type="text" id="donorId" name="donorId" value=""/></div></td>
					<td style="display:none;" id="donorVerified"></td>
				</tr>
				<tr>
					<td><div class="txt_align_label" style="font-weight:bold;"><s:text name="stafa.label.nurseid"/></div></td>
					<td><div align="center" style="padding:1%;"><input type="text" id="nurseID" name="nurseID" value=""/></div></td>
					<td style="display:none;" id="nurseVerified"></td>
				</tr>
			</table>
			<div>&nbsp;</div>
			<table width="100%">
				<tr>
					<td width="20%"><div>Location</div></td>
                     <td width="20%"><s:select id="location"  name="location" list="lstCollectionLocation" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
                     <td width="20%">&nbsp;</td>
                     <td width="20%">&nbsp;</td>
                </tr>
                <tr>
					<td><div>Donor Name Verbally Verified</div></td>
					<td>
						<div>
							<%-- <s:checkbox id="donorNameVerbalVerified_yes" name="donorNameVerbalVerified" fieldValue="" label="Yes" value ="%{}" />&nbsp;&nbsp;
							<s:checkbox id="donorNameVerbalVerified_no" name="donorNameVerbalVerified" fieldValue="" value ="%{}" label="No"/><br/> --%>
						    <s:radio list="#{'1':'Yes','0':'No'}" id= "donorNameVerbalVerified" name="donorNameVerbalVerified" label="Checkbox"/>
							
						</div>
					</td>
					
					<td><div>Donor DOB Verbally Verified</div></td>
					<td>
						<div>
						  <s:radio list="#{'1':'Yes','0':'No'}" name="donorDobVerbalVerified" id="donorDobVerbalVerified"/>
					<%-- <s:checkbox id="donorDobVerbalVerified_yes" name="donorDobVerbalVerified" fieldValue=""  value ="%{}"/>&nbsp;&nbsp;<s:checkbox id="donorDobVerbalVerified_no" name="donorDobVerbalVerified" fieldValue="" value ="%{}" label="donornameNo"/> --%>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>

</div>
</s:form>
<span class="hidden"><s:select id="docsTitleTemp" style="display:none;" name="docsTitleTemp" list="lstDocumentType" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></span>
<div class="cleaner"></div>
<div class="column">
		<div  class="portlet">
			<div class="portlet-header ">Document Review</div>
				<div class="portlet-content">
					
				<div id="tabs">
				    <ul >
				     <li><a href="#tabs-1">Current</a></li>
				      <li><a href="#tabs-2">Historic</a></li>
				    </ul>
			  <div id="tabs-1">   
				<br>
				<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="add_documentReview()"/>&nbsp;&nbsp;<label  class="cursor" onclick="add_documentReview()"><b>Add </b></label>&nbsp;</div></td>
 				<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="delete_documentReview()" />&nbsp;&nbsp;<label class="cursor" onclick="delete_documentReview()"><b>Delete</b></label>&nbsp;</div></td> 
					<!-- <td style="width:20%"><div><img src = "images/icons/save3.png" style="width:16;height:16;" class="cursor" id ="" onclick="save_documentReview();" >&nbsp;&nbsp;<label id="schild" class="cursor" onclick="save_documentReview();"><b>Save</b></label>&nbsp;</div></td> -->
				</tr>
				</table>
				<s:form id="attachedDocsForm" name="attachedDocsForm" >
				
				<s:hidden id="entityType" name="entityType" value="DONOR"/>
				<div class="tableOverflowDiv">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" id="documentreviewTable" class="display">
					<thead>
						<tr>
							<th width="4%" id="documentreviewColumn"></th>
							<th>Document Title</th>
							<th>Document Date</th>
							<th>Attachment</th>
							<th>Reviewed</th>
							<th>Version #</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				</div>
				</s:form>
				</div>	
			<div id="tabs-2">
			 
			  <div class="tableOverflowDiv">
			  <table cellpadding="0" cellspacing="0" border="0" id="documentreviewTable1" width="100%" class="display">
					<thead>
						<tr>
							<th width="4%" id="documentreviewColumn1"></th>
							<th>Document Title</th>
							<th>Document Date</th>
							<th>Attachment</th>
							<th>Reviewed</th>
							<th>Version #</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				</div>
			</div>	
			</div>
				
				</div>	
			</div>
</div>
<div class="cleaner"></div>
		
<div id='preCollQuestions' class="column">
	<div  class="portlet">
	<div class="portlet-header notes">Interim Pre-Collection Questions</div>
		<div class="portlet-content">
			<table  id="interimprecollection" cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
	            <tr>
	                <td  colspan="3"class="questionsAlign">Anything new happened overnight?</td>
	                <td width="11%" height="40px"><input type="hidden" name="overnighthappen_id" id="overnighthappen_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="overnighthappen" /><input type="radio" id="overnighthappen_yes" name="overnighthappen" value="1"/>Yes&nbsp;<input type="radio" id="overnighthappen_no" name="overnighthappen" value="0"/>No</td>
	            </tr>
	            <tr>
	                <td  colspan="3" class="questionsAlign">Any nausea/vomiting/diarrhea/fevers/chills overnight ?</td>
	                <td width="11%" height="40px"><input type="hidden" name="nauseavomit_id" id="nauseavomit_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="nauseavomit" /><input type="radio" id="nauseavomit_yes" name="nauseavomit" value="1"/>Yes&nbsp;<input type="radio" id="nauseavomit_no" name="nauseavomit" value="0"/>No</td>
	            </tr>
	            <tr>
	                <td  colspan="3" class="questionsAlign">Any issues since catheter placement yesterday (if applicable)?</td>
	                <td width="11%" height="40px"><input type="hidden" name="catheterplacement_id" id="catheterplacement_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="catheterplacement" /><input type="radio" id="catheterplacement_yes" name="catheterplacement" value="1"/>Yes&nbsp;<input type="radio" id="catheterplacement_no" name="catheterplacement" value="0"/>No</td>
	            </tr>
	            <tr>
	                <td  colspan="3" class="questionsAlign">Any bone pain/body aches/flu like symptoms?</td>
	                <td width="11%" height="40px"><input type="hidden" name="bodypainsymptoms_id" id="bodypainsymptoms_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="bodypainsymptoms" /><input type="radio" id="bodypainsymptoms_yes" name="bodypainsymptoms" value="1"/>Yes&nbsp;<input type="radio" id="bodypainsymptoms_no" name="bodypainsymptoms" value="0"/>No</td>
	            </tr>
	   
		        <tr>
	                <td  colspan="3"class="questionsAlign">When you received mobilizer,did clinic nurse give you  or tell you to take any medication?</td>
	                <td width="11%" height="40px"><input type="hidden" name="receivedmobilizer_id" id="receivedmobilizer_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="receivedmobilizer" /><input type="radio" id="receivedmobilizer_yes" name="receivedmobilizer" value="1"/>Yes&nbsp;<input type="radio" id="receivedmobilizer_no" name="receivedmobilizer" value="0" />No</td>
	            </tr>
	
			
		
				<tr>
					<td class="tablewapper">Any pain in general?</td>
					<td class="tablewapper1"><input type="hidden" name="generalpain_id" id="generalpain_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="generalpain" /><input type="radio" id="generalpain_yes" name="generalpain" value="1"/>Yes&nbsp;<input type="radio" id="generalpain_no" name="generalpain" value="0"/>No</td>
					<td class="tablewapper">Did you sleep well?</td>
					<td class="tablewapper1"><input type="hidden" name="sleepwell_id" id="sleepwell_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="sleepwell" /><input type="radio" id="sleepwell_yes" name="sleepwell" value="1"/>Yes&nbsp;<input type="radio" id="sleepwell_no" name="sleepwell" value="0"/>No</td>
				</tr>
				<tr>
					<td class="tablewapper">Any trouble breathing?</td>
					<td class="tablewapper1"><input type="hidden" name="troublebreathing_id" id="troublebreathing_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="troublebreathing" /><input type="radio" id="troublebreathing_yes" name="troublebreathing" value="1"/>Yes&nbsp;<input type="radio" id="troublebreathing_no" name="troublebreathing" value="0"/>No</td>
					<td class="tablewapper">Are you pregnant?</td>
					<td class="tablewapper1"><input type="hidden" name="pregnant_id" id="pregnant_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="pregnant" /><input type="radio" id="pregnant_yes" name="pregnant" value="1"/>Yes&nbsp;<input type="radio" id="pregnant_no" name="pregnant" value="0"/>No</td>
                </tr>
                <tr>
					<td class="tablewapper">Did you eat?</td>
					<td class="tablewapper1"><input type="hidden" name="eat_id" id="eat_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="eat" /><input type="radio" id="eat_yes" name="eat" value="1"/>Yes&nbsp;<input type="radio" id="eat_no" name="eat" value="0"/>No</td>
					<td class="tablewapper">Took your morning medications?</td>
					<td class="tablewapper1"><input type="hidden" name="morningmedication_id" id="morningmedication_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="morningmedication" /><input type="radio" id="morningmedication_yes" name="morningmedication" value="1"/>Yes&nbsp;<input type="radio" id="morningmedication_no" name="morningmedication" value="0"/>No</td>
				</tr>
			</table>
        </div>
	</div>
</div>

<div class="cleaner"></div>

<div id='preAssessment' class="column">
	<div  class="portlet">
	<div class="portlet-header notes">Pre-Assessment</div>
		<div class="portlet-content">
		 	<table  id="preassessment" cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Blood Pressure (mmHg)</td>
					<td class="tablewapper1"><input type="hidden" name="Vbloodpressure_id" id="Vbloodpressure_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vbloodpressure" /><input type="text" id="Vbloodpressure" name="Vbloodpressure"  class="numbersOnly" style="width:35% !important"/>
					<input type="hidden" name="Vbloodpressure1_id" id="Vbloodpressure1_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vbloodpressure1" /><input type="text" id="Vbloodpressure1" name="Vbloodpressure1" class="numbersOnly" style="width:35% !important"/></td>
					<td class="tablewapper">Temp.(F)</td>
					<td class="tablewapper1"><input type="hidden" name="Vtemp_id" id="Vtemp_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vtemp" /><input type="text" id="Vtemp" name="Vtemp" class="numbersOnly"/></td>
					
				</tr>
				<tr>
					<td class="tablewapper">Heart Rate (bpm)</td>
					<td class="tablewapper1"><input type="hidden" name="Vheartrate_id" id="Vheartrate_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vheartrate" /><input type="text" id="Vheartrate" name="Vheartrate" class="numbersOnly"/></td>
					
                </tr>
                <tr>
					<td class="tablewapper">CVC Dressing/Site Appearance<label class="mandatory">*</label></td>
					<td class="tablewapper1"><div class="checkboxrequired" id="cvcrequired" style="display:none;">CVC is Required</div><input type="hidden" name="Vcvcdry_id" id="Vcvcdry_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vcvcdry" /><input type="checkbox" id="Vcvcdry" name="Vcvcdry" value="1" onclick="checkCVC()"/>Dry&nbsp;<input type="hidden" name="Vcvcclea_id" id="Vcvcclea_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vcvcclea" /><input type="checkbox" id="Vcvcclea" name="Vcvcclea" value="1" onclick="checkCVC()"/>Clean&nbsp;<input type="hidden" name="Vcvcintact_id" id="Vcvcintact_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vcvcintact" /><input type="checkbox" id="Vcvcintact" name="Vcvcintact" value="1" onclick="checkCVC()"/>Intact&nbsp;<input type="hidden" name="Vcvcsoiled_id" id="Vcvcsoiled_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vcvcsoiled" /><input type="checkbox" id="Vcvcsoiled" name="Vcvcsoiled" value="1" onclick="checkCVC()"/>Soiled&nbsp;<input type="hidden" name="Vcvcna_id" id="Vcvcna_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vcvcna"/><input type="checkbox" name="Vcvcna" id="Vcvcna" value="1" onclick="checkCVC()"/>N/A</td>
					<td class="tablewapper">Comments</td>
					<td class="tablewapper1"><input type="hidden" name="Vcvccomments_id" id="Vcvccomments_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vcvccomments" /><input type="text" id="Vcvccomments" name="Vcvccomments"/></td>
				</tr>
				 <tr>
					<td class="tablewapper">Donor Status<label class="mandatory">*</label></td>
					<!-- <td width="26%"><input type="hidden" name="Vdonorstatusnone_id" id="Vdonorstatusnone_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vdonorstatusnone" /><input type="checkbox" id="Vdonorstatusnone" name="Vdonorstatusnone" value="1"/>No Complains&nbsp;<input type="hidden" name="Vdonorstatusnause_id" id="Vdonorstatusnause_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vdonorstatusnause" /><input type="checkbox" id="Vdonorstatusnause" name="Vdonorstatusnause" value="1"/>Nausea</td> -->
					<td width="26%"><div class="checkboxrequired" id="donorrequired" style="display:none;">Donor Status is Required</div><input type="hidden" name="Vdonorstatusnone_id" id="Vdonorstatusnone_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vdonorstatusnone" /><input type="checkbox" id="Vdonorstatusnone" name="Vdonorstatusnone" value="1" onclick="checkDonorStatus()"/>No Complaints<input type="hidden" name="Vdonorstatuspain_id" id="Vdonorstatuspain_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vdonorstatuspain" /><input type="checkbox" id="Vdonorstatuspain" name="Vdonorstatuspain" value="1" onclick="checkDonorStatus()" />Pain<input type="hidden" name="Vdonorstatusnause_id" id="Vdonorstatusnause_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vdonorstatusnause" /><input type="checkbox" id="Vdonorstatusnause" name="Vdonorstatusnause" value="1" onclick="checkDonorStatus()"/>Nausea<input type="hidden" name="Vdonorstatusdiarrhea_id" id="Vdonorstatusdiarrhea_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vdonorstatusdiarrhea" /><input type="checkbox" id="Vdonorstatusdiarrhea" name="Vdonorstatusdiarrhea" value="1" onclick="checkDonorStatus()"/>Diarrhea<input type="hidden" name="Vdonorstatusfebrile_id" id="Vdonorstatusfebrile_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vdonorstatusfebrile" value="1"/><input type="checkbox" id="Vdonorstatusfebrile" name="Vdonorstatusfebrile" value="1" onclick="checkDonorStatus()"/>Febrile</td>
					<td class="tablewapper">Comments</td>
					<td class="tablewapper1"><input type="hidden" name="Vdonorstatuscomments_id" id="Vdonorstatuscomments_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="Vdonorstatuscomments" /><input type="text" id="Vdonorstatuscomments" name="Vdonorstatuscomments"/></td>
				</tr>
			
	            <tr>
	                <td class="questionsAlign" colspan="3">Review of Collection Process with Donor</td>
	                <td width="25%" height="40px"><input type="hidden" name="collectionprocess_id" id="collectionprocess_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="collectionprocess" /><input type="radio" value="1" name="collectionprocess" id="collectionprocess_yes"/>Yes&nbsp;<input type="radio" name="collectionprocess" id="collectionprocess_no" value="0"/>No</td>
	            </tr>
	            <tr>
	                <td class="questionsAlign" colspan="3">Donor Aware of Potential Side Effects</td>
	                <td width="25%" height="40px"><input type="hidden" name="potentialsideeffects_id" id="potentialsideeffects_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="potentialsideeffects" /><input type="radio"  id="potentialsideeffects_yes" name="potentialsideeffects" value="1"/>Yes&nbsp;<input type="radio" id="potentialsideeffects_no" name="potentialsideeffects" value="0"/>No</td>
	            </tr>
	            <tr>
	                <td class="questionsAlign" colspan="3">Donor Aware to Verbalize Any Side Effects to RN</td>
	                <td width="25%" height="40px"><input type="hidden" name="verbalizesideeffects_id" id="verbalizesideeffects_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="verbalizesideeffects" /><input type="radio" value="1" name="verbalizesideeffects" id="verbalizesideeffects_yes"/>Yes&nbsp;<input type="radio" id="verbalizesideeffects_no" name="verbalizesideeffects" value="0"/>No</td>
	            </tr>
	            <tr>
	                <td class="questionsAlign" colspan="3">Physician Informed of Collection Start</td>
	                <td width="25%" height="40px"><input type="hidden" name="physicianinformed_id" id="physicianinformed_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="physicianinformed" /><input type="radio" value="1" id="physicianinformed_yes" name="physicianinformed"/>Yes&nbsp;<input type="radio" id="physicianinformed_no" name="physicianinformed" value="0"/>No</td>
	            </tr>
		    </table>
		</div>
	</div>
</div>


<s:form id="verificationform1" onsubmit="return avoidFormSubmitting();" >
<div id='donorQC' class="column">
	<div  class="portlet">
	<div class="portlet-header notes">Donor Lab Test</div>
		<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%" id="donorQCTable">
				<!-- <tr>
					<td class="tablewapper"><input type="checkbox" id="cbcd" name="cbcd"  onclick="setvalues('cbcd')">CBCD</td>
					<td class="tablewapper1">Ref #:&nbsp;<input type="text" id="cbcdRefNumber" name="cbcdRefNumber"/></td>
					<td class="tablewapper"><input type="checkbox" id="donorCd34" name="donorCd34" onclick="setvalues('donorCd34')">Donor CD34</td>
					<td class="tablewapper1">Ref #:&nbsp;<input type="text" id="donorCd34RefNumber" name="donorCd34RefNumber"/></td>
				</tr>
				<tr>
					<td class="tablewapper"><input type="checkbox" id="westLite" name="westLite"  onclick="setvalues('westLite')" >West Nile</td>
					<td class="tablewapper1">Ref #:&nbsp;<input type="text" id="westLiteRefNumber" name="westLiteRefNumber"/></td>
					<td class="tablewapper"><input type="checkbox" id="cmp" name="cmp"  onclick="setvalues('cmp')">CMP</td>
					<td class="tablewapper1">Ref #:&nbsp;<input type="text" id="cmpRefNumber" name="cmpRefNumber"/></td>
                </tr>
                <tr>
					<td class="tablewapper"><input type="checkbox" name="purpleBullet" id="purpleBullet"  onclick="setvalues('purpleBullet')" >Purple Bullet</td>
					<td class="tablewapper1"></td>
					<td class="tablewapper"><input type="checkbox" name="typeCheck" id="typeCheck"  onclick="setvalues('typeCheck')" >Type Check</td>
					<td class="tablewapper1"></td>
				</tr>
				<tr>
					<td class="tablewapper"><img onclick="addLab();" style="width:16;height:16;cursor:pointer;" src="images/icons/addnew.jpg">&nbsp;&nbsp;Add Lab Test</td>					<td class="tablewapper1"><select><option>Select...</option></select></td>
				</tr> -->
			</table>
        </div>
	</div>
</div>
</s:form>

<%-- <div class="floatright_right">
<jsp:include page="sidecontrols.jsp"></jsp:include>
 </div> --%>
<!-- Popup for Print Widgets Starts-->
<div id="widgetcollect" title="Widgets" style="display:none;">
	<form id="">
		<table border="0" >
			<tr>
				<td  style="font-weight:bold">Donor Info</td><td><input type="checkbox" id='donorInfo' /></td>
			</tr>
			<tr>
				<td  style="font-weight:bold">Donor Lab</td><td><input type="checkbox" id='donorLab' /></td>
			</tr>
			<tr>
				<td  style="font-weight:bold">Product Lab</td><td><input type="checkbox" id='productLab' /></td>
			</tr>
			<tr>
				<td  style="font-weight:bold">Recipient Info</td><td><input type="checkbox" id='recipientInfo' /></td>
			</tr>
			<tr>
				<td  style="font-weight:bold">Pre-collection Verification</td><td><input type="checkbox" id='preCollection' /></td>
			</tr>
			<tr>
				<td  style="font-weight:bold">Document Review</td><td><input type="checkbox" id='documentReview' /></td>
			</tr>
			<tr>
				<td  style="font-weight:bold">Interim Pre-collection Questions</td><td><input type="checkbox" id='preCollQuestions' /></td>
			</tr>
			<tr>
				<td  style="font-weight:bold">Pre-Assessment</td><td><input type="checkbox" id='preAssessment' /></td>
			</tr>
			<tr>
				<td  style="font-weight:bold">DonorQC</td><td><input type="checkbox" id='donorQC' /></td>
			</tr>
		</table>
		<div align="right" style="float:right;">
			<table cellpadding="0" cellspacing="0" class="" align="" border="0">
				<tr>
					<td><input type="button" onclick="printToDiv();" value="Print"/></td>
					<td><input type="button" onclick="closeWidgets();" value="Cancel"/></td>
				</tr>
			</table>
		</div>
	</form>
</div>
<!-- Popup for Print Widgets Ends	-->

<!-- popup starts -->
<%-- 	<div id="transfer" title="addNew" style="display:none;">
		<form id="">
			<table border="0" >
				<tr>
					<td  style="font-weight:bold"><s:text name="stafa.label.pre_collection_therapy" /></td>
				</tr>
				<tr>
					<td></td>
				</tr>
				<tr>
					<td><img onclick="addrows_therapy('pre-collectionTherapy')" style="width:16;height:16;cursor:pointer;" src="images/icons/addnew.jpg">
							<label id=""><b>Add Medication</b></label>
					</td><td> <img onclick="addtxt();" style="width:16;height:16;cursor:pointer;" src="images/icons/no.png">
							<label id=""><b>Delete</b></label>
					</td>
				</tr>
			</table>
			 <table cellpadding="0" cellspacing="0" border="0" id="pre-collectionTherapy" class="display createchilddisplay" >
			 	<thead>
			 		<tr>
			 			<th>Thearpy Name</th>
			 			<th>Volume</th>
			 			<th>Dosage</th>
			 			<th>Date of Therapy</th>
			 			<th>Time of Therapy</th>
			 			<th>Reason for Therapy</th>
			 		</tr>	
			 	</thead>
			 	<tbody>
			 	</tbody>
			 
			 </table>
			 <br>
			 <div style="height:14px;width:2%;"></div>
			 <table border="0">
				<tr>
					<td  style="font-weight:bold"><s:text name="stafa.label.medication" /></td>
				</tr>
				<tr>
					<td></td>
				</tr>
				<tr>
					<td><img onclick="addrows_therapy('medicationTable')" style="width:16;height:16;cursor:pointer;" src="images/icons/addnew.jpg">
							<label id=""><b>Add Medication</b></label>
					</td><td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img onclick="addtxt();" style="width:16;height:16;cursor:pointer;" src="images/icons/no.png">
							<label id=""><b>Delete</b></label>
					</td>
				</tr>
			</table>
			 <table cellpadding="0" cellspacing="0" border="0" id="medicationTable" class="display createchilddisplay" height="auto">
			 	<thead>
			 		<tr>
			 			<th>Medication Name</th>
			 			<th>Volume</th>
			 			<th>Dosage</th>
			 			<th>Date of Medication</th>
			 			<th>Time of Medication</th>
			 			<th>Reason for Medication</th>
			 		</tr>	
			 	</thead>
			 	<tbody>
			 	</tbody>
			 
			 </table>
				<div style="float:right">
				<input type="button" onclick="save()" value=Save>
				<input type="button" onclick="cancel()" value="Cancel">
				</div>
			 
			</form>
			</div>
--%>
	
<div class="cleaner"></div>
		<div align="left" style="float:right;">
		<form onsubmit="return avoidFormSubmitting()">
				<table cellpadding="0" cellspacing="0" class="" align="" border="0">
				<tr>
				<td><span class='nextContainer'>Next:
				<select name="nextOption" id="nextOption" class=""></select>
				<input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
				<input type="button" value="Next" onclick="verifyeSign('eSignNext','next')"/></span></td>
				</tr>
				</table>
		</form>
		</div>
		
		
		
		<div class="cleaner"></div>
</section> 
<!-- Barcode Starts -->		
<div  id ="mainbarcodediv" style="background-color:white; display:none; height:655px;overflow:auto;margin-top:1%;">  <!-- added by mohan j -->
<div id="maindiv"><div id="barimageTables" style="float:right;display:none; width:44%;height:40%;">
			<!-- From Template Start -->
<div id="barcodeMainDiv" >
		</div>
			<!-- From Template Ends -->
</div></div>
	
</div>
<!-- Barcode Ends -->
<script>

$(function() {
    $('.jclock').jclock();
    $( ".column" ).sortable().sortable({
        connectWith: ".column"
    });
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
   

    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

    $('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});   
});
</script>