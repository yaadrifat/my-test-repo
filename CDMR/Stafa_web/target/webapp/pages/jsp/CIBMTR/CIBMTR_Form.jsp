<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="./js/CIBMTR/formValidation.js"></script>
<style type="text/css">
.formContainer{
max-height: 580px;
min-height:500px;
}
#pagination{
text-align:center;
}
#pagination a{
margin-right:0.1em;
font-size:1em;
text-decoration :none;
}
.visible{
 display:block;
}
.link{
padding:1%;
color:#333333;
font-weight:bold;
}
.link:hover {
color:#FF00FF;
}  
a.link.visited{
color:blue !important;
}	
.hideTr{
display:none;
}
.paginationprev{
float:right;
}
.subQuestion{
border:1px solid blue;
border-radius:5px;
padding:3px 14px 0px 11px;
width:75%;
}
.title{
	font-weight:bold;
	font-size:1.2em;
}
.divlength{
margin-left:2%;
}
#formContent{
max-height:450px;
overflow:auto;
}
</style>

 <script>
$(document).ready(function(){
    hide_slidewidget();
});

	 $(function() {
		 $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
		    .addClass( "ui-widget-header ui-corner-all" );
	 });
	 var currentIndex=0;
	 var xmlFormData="";
	 var dataLength;
	 var selectValue;
	  function getFormData(){
		  $('.progress-indicator').css( 'display', 'block' );
		 var data=$("#formName").val();
		 if(data == '2400 most updated '){
			 $('.portlet-header').html("CIBMTR Form 2400 - Pre-Transplant Essential Data (Pre-TED)");
		 }else if(data =='2006 - HSCT Infusion ') {
             $('.portlet-header').html("CIBMTR Form 2006 - HSCT Infusion");
         }else{
             $('.portlet-header').html(data);
         } 
		 
		 var url = "loadCibmtrForm";
		    var response = ajaxCallWithParam(url,data);
		    if(response!=null){
		    	var tempData = response.pagedata;
		    	$("#formData").html(response.pagedata);
		    	xmlFormData = tempData.split('</tr>');
		    	dataLength=xmlFormData.length;
		    	changeIndex();
		    	
		    	 $('.progress-indicator').css( 'display', 'none' );
		    	// next(currentIndex);
		    	var followupDetails = response.followupDetails;
				$("#hideKeysHTML").html(response.hideKeysHTML);
		    	$("#hiddenFollowup").html(followupDetails);
		    	$("#hiddenFollowup table tbody tr").removeClass("hideTr");
		    	$( ".formDateBox").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
		            changeYear: true, yearRange: "c-50:c+45"});
		    }else{
		    	alert("error");
		    }
	 }
	function showPage(indexPos,el){
		$("#pagination").find("a").removeClass("visited");
		$('#paginate_'+indexPos).addClass("visited");
	//	$("#formData").html("");
	//alert("selectValue"+selectValue);
		var curIndex = (indexPos * selectValue);
		//alert("curIndex"+curIndex);
		var maxIndex=curIndex+parseInt(selectValue);
		//alert("maxIndex"+maxIndex);					
		/* $.each($("#formData tr"),function (ind){	
			$(this).addClass("hideTr");
		}); */
		//alert("Hide all");
		$.each($("#formData tr"),function (ind){
			//alert("ind"+ind+"curIndex"+curIndex+"maxIndex"+maxIndex);
			//alert("ind==="+ind+"curIndx=="+curIndx+"maxIndex=="+maxIndex);
		//	alert("lllllll1");	
			if((ind >= curIndex)  && (ind <maxIndex)){
							//alert("1");
							//alert($(this).html());
	  						$(this).removeClass("hideTr");
	  						//$(this).addClass("visible");
	  						//alert("trace2");
	  						//alert("After visible");
			  			//	$( ".formDateBox").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
					     //       changeYear: true, yearRange: "c-50:c+45"}); 
				 	$("select").uniform();
				//}
			}else{
			
			//	$(this).removeClass("visible");
	  			$(this).addClass("hideTr");
				return;
			}
		});
		//alert("End of the Method");
	}	 
	/*  var index=0;
	  function showDefaultSub(){
		  $("#formData tbody tr td").each(function(){
					if(($(this).children().html()!=null)&&($(this).children().html()!="")){
										var id=$(this).find("select").attr("id");		
										var value=$("#"+id+' option:selected').text();
										if(value=="Yes"){
											alert($(this).children("eq(1)").html());
											check($(this).children().html());
										}
					}
			  	
		  });
	  } */
	  /* function check(el){
		  var id = $(el).attr("id");
		  alert(id);
	  } */
	 function next(curIndex){
		 if(curIndex >9){
		 $('.paginationprev').css("display","block");
		 }
		 var maxIndex=curIndex+10;
		 currentIndex=maxIndex; 
		 var maxlength=dataLength-1;
		 $("#formData").html("");
		 for(var i=curIndex; i<maxIndex;i++){
			 		if((curIndex<maxlength)&&(xmlFormData[i]!=undefined)){
			  				$("#formData").append(xmlFormData[i]+"</tr>");	
			  				$( ".formDateBox").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
					            changeYear: true, yearRange: "c-50:c+45"});
			 		}else{
			 			currentIndex=currentIndex;
			 			$('.pagination').hide();
			 			return false;
			 			}
			 		$("select").uniform();
			 		$('.pagination').css("display","block");
			 		}
		 }
	 $('.pagination').click(function(){
		 next(currentIndex);
	 });
	 $('.paginationprev').click(function(){
		 prev(currentIndex);
	 });
	 function prev(curIndex){
		 if(curIndex<=20){
			 $('.paginationprev').css("display","none");
		 }
		 var maxIndex=curIndex;
		 currentIndex=maxIndex-20;
		 var maxlength=dataLength-1;
		 $("#formData").html("");
		 for(var i=currentIndex; i<currentIndex+10;i++){
			 $("#formData").append(xmlFormData[i]+"</tr>");
			 $( ".formDateBox").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
		            changeYear: true, yearRange: "c-50:c+45"});
			 $("select").uniform();
			 }
		 currentIndex=currentIndex+10;
		 if((currentIndex)<dataLength){
			 $('.pagination').css("display","block");
			 }
		 }
	 function changeIndex(){
		 selectValue=$("#selectIndex").val();
		 //alert("selectValue=="+selectValue);
		 var paginationHtml = "";
	    	for(var i=0;i<(dataLength/selectValue);i++){
	    		paginationHtml += "<a id='paginate_"+i+"' href='javascript: showPage("+i+",this)' class='link' >"+(i+1)+"</a>&nbsp;";
		    }
	    	$("#pagination").html(paginationHtml);
	    	showPage(0);
	 }
 </script>
<form>
   <div  class="formContainer portlet">
  	  	<input type="hidden"  id="formName"/>
  	  	 <div class="portlet-header"></div>
  	 	 <div class="portlet-content"> 
  	 		<div class="divlength"><table><tr><td><label>Show</label></td><td><div class="selector_1"><select id="selectIndex"  onChange="changeIndex(this)"><option>10</option><option>20</option><option>30</option></select></div></td><td><label>Entries</label></td></tr></table></div>
  	 		<div id="formContent">
  	 			<table id="formData"  width="100%"  cellpadding="2" cellspacing="2">
  	 			</table>
  	 			<br/>
  	 			</div>
  	 			<br>
  	 			<br>
  	 			<div id="pagination" style="align:center" >
  	 			</div>
		
  	</div>
 </div>
</form>
<div id="hiddenFollowup"></div>
<div id="hideKeysHTML"></div>