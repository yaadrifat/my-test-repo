<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/dataSearch.js"></script>
<script type="text/javascript" src="js/placeholder.js"></script>
<script>

function displayValuesFromJsonList(response){
    $.each(response.acquisitionList, function(i) {
        $.each(this, function(key, value){
        	//alert("key :"+key+"value :"+value);
        	$("#acquisitionForm #"+key).val(value);
        	if(key=='fkCodelstProdappear'){
            	if($("#fkCodelstProdappear").val()==1){
                    $('input[name=radio1]')[0].checked="checked";
                }else{
                    $('input[name=radio1]')[1].checked="checked";
                    }
            }
        	if(key=='acquisitionDatetime'){
            	$("#acquisitionDateStr").val($.datepicker.formatDate('M dd, yy', new Date(value)));
            }
        });
        
        
        
    });
    $.uniform.restore('select');
	$('select').uniform();
}

function hideBarcodes(){
    $("#imgtip0").css("display","none");
    $("#imgtip1").css("display","none");
    $("#imgtip2").css("display","none");
    $("#imgtip3").css("display","none");
}

function clearPopUp()
{		 var select="Select";
		$('#addNewDialog #recipientId').val("");
		$('#addNewDialog #recipientName').val("");
		$('#DonorId').val("");
		$('#DonorName').val("")
	   	$('#CollectSection').val("");
	   	$('#productId').val("");
	   	$('#ProcessingFacility').val("");
	   	$('#Physicianname').val("");
	   	$('#Collectdate').val("");
	   	$("#Yes").removeAttr('checked');
		$("#NO").removeAttr('checked');
	    setComboValue(select,"fkCodelstFinalDisposition");
	    setComboValue(select,"fkCodelstProcessSelection");
	    setComboValue(select,"fkCodelstPersonlocation");
	    setComboValue(select,"fkCodelstProductsource");
	    setComboValue(select,"fkCodelstProductType");
	    setComboValue(select,"fkCodelstLocation");
	    $("#radioYes").removeAttr('checked');
	    $("#radioNo").removeAttr('checked');
	   }
function closeNewAcquisition()
{	$("#addNewDialog").dialog( "destroy" )
	
	}
    
function getScanData(event,obj){
	if(event.keyCode ==13){
	getAcquisitionData(event,this);
	}
}
function generateTableFromJsnList(response){
    $.each(response.acquisitionList, function(i) {
         $.each(this, function(key, value){
                if(key == "productId"){
                    productId = value;
                }
                if(key == "recipientId"){
                    recipientId = value;
                }
                if(key == "recipientName"){
                    recipientName = value;
                }
                if(key == "productType"){
                    productType = value;
                }
                if(key == "productSource"){
                    productSource = value;
                }
                if(key == "donorId"){
                    donorId = value;
                }
                if(key == "donorName"){
                    donorName = value;
                }
                if(key == "donorLocation"){
                    donorLocation = value;
                }
                if(key == "processingFacility"){
                    processingFacility = value;
                }
                if(key == "proposedFinalDisposition"){
                    proposedFinalDisposition = value;
                }
                if(key == "productAppearance"){
                    productAppearance = value;
                }
                if(key == "productId"){
                    productId = value;
                }
                if(key == "collectSection"){
                    collectSection = value;
                }
                if(key == "productStatus"){
                    productStatus = value;
                }
                if(key == "productStatus"){
                    productStatus = value;
                }
                if(key == "personproductid"){
                    personproductid = value;
                }
                if(key == "physicianName"){
                	Physicianname = value;
                }
                if(key == "collectionDate"){
                	collectionDate = value;
                }
         });
         $('#acquisitionTable').dataTable().fnAddData( [
										"<input type='hidden' value="+personproductid+">",recipientId,recipientName,productType,productSource,
                                        donorId,donorName,donorLocation,processingFacility,proposedFinalDisposition,
                                        productAppearance,productId,physicianName,collectSection,collectionDate,productStatus]);
     });
   
        setColumnManager('acquisitionTable');
}
var availableFlag=false;
function addNewProduct(){
	$('.progress-indicator').css( 'display', 'block' );
	if($("#acquisitionNewForm").valid()){
		setComboValue($(".ui-dialog #fkCodelstProductsource").val(),"fkCodelstProductsource1");
		setComboValue($(".ui-dialog #fkCodelstProductType").val(),"fkCodelstProductType1");
		var proId = $('#productId').val();
		var url = "checkAvailableProduct";
		var response = jsonDataCall(url,"proId="+proId);
		if(response!=null && response.acquisitionList!=null && response.acquisitionList.length>0){
			alert(productAlreadyAvailable);
			return;
		}
		var url = "addAcquisition";
		var response = ajaxCall(url,"acquisitionNewForm");
		
		response = jQuery.parseJSON(response.returnJsonStr);
		var tableObj = $("#acquisitionTable");
		var rowCount = $('#acquisitionTable tbody[role="alert"] tr').length;
		  //alert("rowCount : "+rowCount);
	     ///alert("response.workflow :: "+response.workflow);
	     
	     //Based on workflow response we have to move the correspondig process Ex:Accesion, Production...etc
	     var workflow = response.workflow;
	     var productId = $("#productId").val();
	    // alert("productID " +productId + " />" + $("#fkCodelstProductFrom").val()+"<");
	 //?BB    if(workflow!=null && workflow!="" && workflow=="getAcquisition"){
			 alert("Data saved Successfully");
			 showPopUp("close","addNewDialog","Add New Acquision","900","450");
	         if($("#fkCodelstProductFrom").val()=="1" || $("#fkCodelstProductFrom").val()==1 ){
	        	// alert("inside if");
	             status = "Acquired";
	             url = "getAccession.action?productId="+productId;
	             loadAction(url);
	         }else{
	        	// alert(" else ");
	             status = "Active";
	             constructTable(true,'acquisitionTable',acquisition_ColManager,acquisition_serverParam,acquisition_aoColumnDef,acquisition_aoColumn);
	 	        
	         }
	          		
	       
	       
	        /*//?Mari
	        $("#conformProductSearch").val(productId);
	        $("#fake_conformProductSearch").attr("onblur","getAcquisitionData('','')");
	        workFlowFlag = true;
	        $("#conformProductSearch").blur();
	        $("#conformProductSearch").focus();
	        $("#conformProductSearch").removeAttr("onblur");
	        workFlowFlag = false;
	        */
	//?BB    }else{
	 //?BB        var url = response.workflow+".action?productId="+productId;
	 //?BB        showPopUp("close","addNewDialog","","","");
	 //?BB        loadAction(url);
	        /*//?Mari  
	        $("#fake_conformProductSearch").val(productId);
	        $("#productSearch").attr("onblur","getAcquisitionData('','')");
	        workFlowFlag = true;
	        $("#conformProductSearch").blur();
	        $("#conformProductSearch").focus();
	        $("#conformProductSearch").removeAttr("onblur");
	        workFlowFlag = false;
		    */
	  //?BB  }
	}
        
}
function closeAquisition()
{  
    $(addNewDialog).dialog("close");
}
function setComboValue(val1,comboBoxId){
    //val = val1.replace(/^\s+/,"");       
    val = val1.replace(/^\s+|\s+$/g,"");        //trim left and right spaces
    //alert(val);
    var x = document.getElementById(comboBoxId);
    $("#"+comboBoxId).find("option:contains('"+val+"')").each(function(){
        //alert("["+$(this).text()+"]==["+val+"]");
        //alert("$(this).text() : "+$(this).text()+"--val1 : "+val1);
       
    if( ($(this).text() == val) || ($(this).text()=="Select" && val=="") ) {
        //alert("inside==>["+$(this).text()+"]==["+val+"]");
        $(this).attr("selected","selected");
        //This line added due to new dropdown changes ..Mari..
        //alert("val1 : "+val1);
        $("#uniform-"+comboBoxId+" span").html($(this).text());
        }
    });
}

var resetFlag=false;
var productAvailableFlag=false;
function checkProduct(productSearch){
    var flag=false;
    var productArr = [];
   
        $('#acquisitionTable tbody[role="alert"] tr').each(function(i,el) {
        $(this).removeAttr("style");
        //$(this).removeAttr("style");
        var productId = this.cells[11].innerHTML;
        var status = this.cells[15].innerHTML;
        if(productId==productSearch && status=="Acquired"){
            flag = true;
            //resetFlag = true;
          
        }
        else{
            productArr.push(productId);
        }
      });
        if($.inArray(productSearch, productArr)<0){
             resetFlag = true;
        }
   
         return flag;
}
function searctrigger(e,This,productSearch){
    if((e.keyCode==13 && productSearch!="")){
    $("#searchtxt").trigger(e);
    }
    if(e.keyCode ==8 && productSearch!="") {
        $("#searchtxt").trigger(e);
          return false;  
        }else if (e.keyCode !=8 && e.keyCode != 13 ){
            $("#searchtxt").trigger(e);
           
            return false;
        }
    return false;
}

var workFlowFlag = false;
function getAcquisitionData(e,This){
    var url = "getAcquisitionInfo";
    var response = ajaxCall(url,"acquisitionForm");
    //var arr = response.acquisitionList;
    
    if(response.acquisitionList!=null && response.acquisitionList.length > 0){

    	var answer = confirm("Product is already Acquired. Do you want to continue to view the records?");
        if (!answer){
            return false;
        }else{
        	displayValuesFromJsonList(response);
        	return false;
            }
    }
    //return;
    var searchUrl = "checkAcquisitionData";
    var productSearch = $("#fake_conformProductSearch").val();
    if((e.keyCode==13 && productSearch!="") || workFlowFlag ==true){
    	var result = jsonDataCall(searchUrl,"productId="+productSearch);
    	var listLength=result.acquisitionList.length;
    	//alert(listLength);
        if(listLength <= 0){
        	alert(productIdInvalid);
        	resetvalues();
        	return;
        }
        	$('#searchtxt').val(productSearch);
        	searctrigger(e,productSearch);
    		var productAvailable = jQuery("#acquisitionTable:contains(\'"+productSearch+"\')").length
        	/*/? Mari var flag =checkProduct(productSearch);
        	//alert("flag : "+flag)
        	if(flag){
            	alert("Product is already Acquired.");
            	//return;
            	}
        	*/
            	if(resetFlag){
                	///** Reset the form values*/
                	resetvalues();
                	//return;
            	}
       
               
                $('#acquisitionTable tbody[role="alert"] tr').each(function(i,el) {
                    $(this).removeAttr("style");
                    var productId = this.cells[11].innerHTML;
                    var appearance = this.cells[10].innerHTML.toLowerCase();
                    var status = this.cells[15].innerHTML;
                    //alert("productId : "+productId+", productSearch : "+productSearch+",status : "+status);
                    if(productId==productSearch && status=="Active"){
                            //alert("b");
                            availableFlag = true;
                            $(this).attr("style","background:#33ffff");
                            //$("#tempPersonProductId").val($(this).attr("id"));
                            $("#recipientId").val(this.cells[1].innerHTML);
                            $("#recipientName").val(this.cells[2].innerHTML);
                            $("#donorName").val(this.cells[6].innerHTML);
                            $("#donorId").val(this.cells[5].innerHTML);
                            $("#status").val(status);
                            var hiddenValues = $(this).find("#personprId").val();
                            //alert("hiddenValues "+hiddenValues);
                            $("#personproductid").val(hiddenValues);
                            
                            if(appearance=="on" || appearance=="yes" || appearance=="true" ){
                              // $('input[name=radio]')[0].checked="checked";
                                 $("#radioYes").attr("checked","checked");
                            }else if (appearance=="no" || appearance=="off" || appearance=="false" ){
                               // $('input[name=radio]')[1].checked="checked";
                            	  $("#radioNo").attr("checked","checked");
                                }
                            //alert("["+this.cells[9].innerHTML+"]");
                            setComboValue(this.cells[9].innerHTML,"fkCodelstFinalDisposition");
                            //alert("["+this.cells[8].innerHTML+"]");
                            setComboValue(this.cells[8].innerHTML,"fkCodelstProcessSelection");
                            //alert("["+this.cells[7].innerHTML+"]");
                            setComboValue(this.cells[7].innerHTML,"fkCodelstPersonlocation");
                            setComboValue(this.cells[3].innerHTML,"fkCodelstProductsource");
                            setComboValue(this.cells[4].innerHTML,"fkCodelstProductType");
                            setComboValue(this.cells[12].innerHTML,"fkCodelstLocation");
                        }else if(productId==productSearch && status=="Active"){
                            availableFlag = false;       
                         }
                });
                $("#pkAcquisition").val("");
                //alert(availableFlag);
               
                //to get data from db
               
            /*var response = ajaxCall(url,"acquisitionForm");
       
            var arr = response.returnJsonStr;
            var response = jQuery.parseJSON(response.returnJsonStr);

            $("#fkCodelstProductsource").val(response.fkCodelstProductsource);
            $("#fkPersonProduct").val(response.fkPersonProduct);
            $("#acquisitionProduct").val(response.acquisitionProduct);
            $("#fkCodelstPersonlocation").val(response.fkCodelstPersonlocation);
            $("#fkCodelstProductType").val(response.fkCodelstProductType);
            $("#productAnticoagulantName").val(response.productAnticoagulantName);
            $("#specId").val(response.specId);
            $("#fkCodelstProdappear").val(response.fkCodelstProdappear);
            $("#fkCodelstProcessSelection").val(response.fkCodelstProcessSelection);
            $("#fkCodelstLocation").val(response.fkCodelstLocation);
            $("#pkAcquisition").val(response.pkAcquisition);
            $("#acquisitionInformation").val(response.acquisitionInformation);
            $("#fkCodelstFinalDisposition").val(response.fkCodelstFinalDisposition);
           
       
            if($("#fkCodelstProdappear").val()==1){
                $('input[name=radio]')[0].checked="checked";
               
            }else{
               
                $('input[name=radio]')[1].checked="checked";
                               
                }*/
           
        //$('#button_wrapper').html($(response).find("#button_wrapper").html());
        //$('#table_inner_wrapper').html($(response).find("#table_inner_wrapper").html());
        }
    }


/*function checkAppearance(radioObj){
    if(radioObj.id=="radioYes"){
        $("#fkCodelstProdappear").val("1");   
        }else{
             $("#fkCodelstProdappear").val("0");
        }
}*/

function setAppearance(radioObj){

    alert(radioObj.id);
    if(radioObj.id=="Yes"){
        alert(radioObj.id);
        $("#productAppearance").val("Yes");
        alert(radioObj.id);   
        }else{
            $("#productAppearance").val("No");
            }
}
/*Acquisition Server Parameter Starts*/
	var acquisition_ColManager = function () {
   												$("#acquisitionColumn").html($('.ColVis:eq(0)'));
											 };
	var acquisition_serverParam = function ( aoData ) {
														aoData.push( { "name": "application", "value": "stafa"});
														aoData.push( { "name": "module", "value": "technician_acquisition"});	
														
														aoData.push( {"name": "col_1_name", "value": "lower(recipientId)" } );
														aoData.push( { "name": "col_1_column", "value": "lower(recipientId)"});
														
														aoData.push( { "name": "col_2_name", "value": " lower(recipientName)"});
														aoData.push( { "name": "col_2_column", "value": "lower(recipientName)"});
																	
														aoData.push( {"name": "col_3_name", "value": " lower(productSource)" } );
														aoData.push( { "name": "col_3_column", "value": "lower(productSource)"});
														
														aoData.push( {"name": "col_4_name", "value": " lower(productType)" } );
														aoData.push( { "name": "col_4_column", "value": "lower(productType)"});
														
														aoData.push( {"name": "col_5_name", "value": " lower(donorId)" } );
														aoData.push( { "name": "col_5_column", "value": "lower(donorId)"});
														
														aoData.push( {"name": "col_6_name", "value": " lower(donorName)" } );
														aoData.push( { "name": "col_6_column", "value": "lower(donorName)"});
														
														aoData.push( {"name": "col_7_name", "value": " lower(donorLocation)" } );
														aoData.push( { "name": "col_7_column", "value": "lower(donorLocation)"});
														
														aoData.push( {"name": "col_8_name", "value": " lower(processingFacility)" } );
														aoData.push( { "name": "col_8_column", "value": "lower(processingFacility)"});
														
														aoData.push( {"name": "col_9_name", "value": " lower(proposedFinalDisposition)" } );
														aoData.push( { "name": "col_9_column", "value": "lower(proposedFinalDisposition)"});
														
														aoData.push( {"name": "col_10_name", "value": " lower(productAppearance)" } );
														aoData.push( { "name": "col_10_column", "value": "lower(productAppearance)"});
														
														aoData.push( {"name": "col_11_name", "value": " lower(productId)" } );
														aoData.push( { "name": "col_11_column", "value": "lower(productId)"});
														
														aoData.push( {"name": "col_12_name", "value": " lower(physicianName)" } );
														aoData.push( { "name": "col_12_column", "value": "lower(physicianName)"});
														
														aoData.push( {"name": "col_13_name", "value": " lower(collectSection)" } );
														aoData.push( { "name": "col_13_column", "value": "lower(collectSection)"});
														
														aoData.push( {"name": "col_14_name", "value": " lower(collectionDate)" } );
														aoData.push( { "name": "col_14_column", "value": "lower(collectionDate)"});
														
														aoData.push( {"name": "col_15_name", "value": " lower(productStatus)" } );
														aoData.push( { "name": "col_15_column", "value": "lower(productStatus)"});
																
													};
	var acquisition_aoColumnDef = [
					                  {		
					                	  "aTargets": [ 0], "mDataProp": function ( source, type, val ) {
					                	   
				                	 		return "<input type='hidden' id='personprId' value="+ source.personproductid+">";
				                	     }},
					                  {"sTitle": "Recipient ID",
				                	    	 "aTargets": [ 1], "mDataProp": function ( source, type, val ) { 
					                	 		return source.recipientId;
					                	 }},
									  {"sTitle": "Recipient Name",
					                		 "aTargets": [ 2], "mDataProp": function ( source, type, val ) { 
											    return source.recipientName;
											    }},
									  {"sTitle": "Product Source",
											    	"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
										 	   return source.productSource;
									  		}},
									  {"sTitle": "Product Type",
									  			"aTargets": [ 3], "mDataProp": function ( source, type, val ) { 
										  		return source.productType;
										  }},
									  {"sTitle": "Donor ID",
											  "aTargets": [ 5], "mDataProp": function ( source, type, val ) { 
										  		return source.donorId;
										  }},
									  {"sTitle": "Donor Name",
											  "aTargets": [ 6], "mDataProp": function ( source, type, val ) { 
										  		return source.donorName;
										  }},
									  {"sTitle": "Donor Location",
											  "aTargets": [ 7], "mDataProp": function ( source, type, val ) { 
										  		return source.donorLocation;
										  }},
									  {"sTitle": "Processing Facility",
											  "aTargets": [ 8], "mDataProp": function ( source, type, val ) { 
										  		return source.processingFacility;
										  }},
									  {"sTitle": "Proposed Final Disposition",
											  "aTargets": [ 9], "mDataProp": function ( source, type, val ) { 
										  		return source.proposedFinalDisposition;
										  }},
									  {"sTitle": "Product Appearance OK?",
											  "aTargets": [ 10], "mDataProp": function ( source, type, val ) { 
										  		if (source.productAppearance=="on"){
										  			link='YES';
										  		}
										  		else{
										  			link='NO';
										  		}
										  		return link;
										  }},
									  {"sTitle": "Product ID",
											  "aTargets": [ 11], "mDataProp": function ( source, type, val ) { 
										  		return source.productId;
										  }},
									  {"sTitle": "Physician Name",
											  "aTargets": [ 12], "mDataProp": function ( source, type, val ) { 
										  		return source.physicianName;
										  }},
									  {"sTitle": "Collect Section",
											  "aTargets": [ 13], "mDataProp": function ( source, type, val ) { 
										  		return source.collectSection;
										  }},
									  {"sTitle": "Collection Date",
											  "aTargets": [ 14], "mDataProp": function ( source, type, val ) {
												  if(source.collectionDate!=null){
													  return $.datepicker.formatDate('M dd, yy', new Date(source.collectionDate));
												  }else{
													  return "";
												  }
										  }},
									  {"sTitle": "Status",
											  "aTargets": [ 15], "mDataProp": function ( source, type, val ) { 
										  		return source.productStatus;
										  }}
										  
									];
var acquisition_aoColumn =  [ { "bSortable":false},
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null,
				               null
				               ];
/*Acquisition Server Parameter Ends*/
$(document).ready(function() {
/*     $("select").attr("id","small");  */ 
    //$("#content_wrapper").css("margin-bottom","55px");
    hide_slidewidgets();
	hide_slidecontrol();
    hideBarcodes();
    //setColumnManager('acquisitionTable');
    //getAcquisitionTable();
	$('#nextProtocolList').append("<option>Next Product</option><option>Logout</option>");
	constructTable(false,'acquisitionTable',acquisition_ColManager,acquisition_serverParam,acquisition_aoColumnDef,acquisition_aoColumn);
    var today = new Date();
    $( "#Collectdate" ).datepicker({dateFormat: 'M dd, yy'});
    var d = today.getDate();
    var m = today.getMonth();
    var y = today.getFullYear();

    var h=today.getHours();
    var mn=today.getMinutes()+1;
    $( "#acquisitionDateStr").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
        changeYear: true, yearRange: "c-50:c+45"});
   /*$( "#acquisitionDateStr" ).datepicker({
        dateFormat: 'M dd, yy',
        //minDate: new Date(2012, 11, 20, 8, 30)
        minDate: new Date(y, m, d, h,mn)
            }); */
   
    /*var url = "getAcquisitionProduct";
    var response = ajaxCall(url,"acquisitionNewForm");
    constructTable(true);*/
    //? Removed by Muthu
     // generateTableFromJsnList(response);

     //Mohan
     $(".selectDelect").click( function(e){
    	   
             var itsOn = $(this).hasClass("on");
             
            $(":radio[name="+ this.name + "]").removeClass("on");
          
            if(itsOn){
              $(this).removeAttr('checked');
             
            } else {
              $(this).addClass("on");
            }
           
            var c = $(this).attr('id');
           
             if(c=="radioYes"){
                 if($('#radioYes').attr('checked')){
                     $("#fkCodelstProdappear").val("1");
                     }
                 }
              if(c=="radioNo"){
                  if($('#radioNo').attr('checked')){
                      $("#fkCodelstProdappear").val("0");
                      //alert("selecting the No option");
                  }
                 }
              
          }).filter(":checked").addClass("on");
     
     var workflowProductId = $("#workflowProductId").val();
       
        if(workflowProductId!=null && workflowProductId!=""){
            $("#productSearch").val(workflowProductId);
            workFlowFlag = true;
            $("#productSearch").focus();
            getAcquisitionData('','');
            workFlowFlag = false;
           
            }
     
    });

function columnMaager(){
    var showFlag1 = false;
    $('#targetall').hide();
    $('#tableall').columnManager({listTargetID:'targetall', onClass: 'advon', offClass: 'advoff', hideInList: [1,4,5],
                                    saveState: true, colsHidden: [1,3]});
    $('#ulSelectColumn').click( function(){
          if(showFlag1){
            $('#targetall').fadeOut();
            showFlag1 = false;
          }else{
              $('#targetall').fadeIn();
              showFlag1 = true;
          }
        });
}

/*function setColumnManager(tableId){
    var showFlag1 = false;
    $('#targetall1').hide();
    $('#'+tableId).columnManager({listTargetID:'targetall1', onClass: 'advon', offClass: 'advoff', saveState: true});
    $('#ulSelectColumn').click( function(){
          if(showFlag1){
            $('#targetall1').fadeOut();
            showFlag1 = false;
          }else{
              $('#targetall1').fadeIn();
              showFlag1 = true;
          }
        });
}
*/

function saveAcquisition(){
	if($("#pkAcquisition").val()!=""){
		alert("Product is already Acquired.");
		return;
	}
    if($("#fake_conformProductSearch").val()=="" || $("#fake_conformProductSearch").length>2){
        alert("Please Enter the Product.");
        $("#fake_conformProductSearch").focus();
        return;
        }
    var productSearch = $("#fake_conformProductSearch").val();
    try{
        var flag = checkProduct(productSearch);
        if(flag){
            alert("Product is already Acquired.");
            return;
            }
         $.ajax({
                type: "POST",
                  url: "saveAcquisition.action",
                async:false,
                data : $("#acquisitionForm").serialize(),
                success: function (result){
                    //alert(" success" + $(result).find("#returnMessage").val());
                var res = $(result).find("#returnMessage").val();

               //alert(res);
                if(res !=""){
               		 var url = res;
               		 loadAction(url);
                }
                //Based on workflow response we have to move the correspondig process Ex:Acquisition, Accesion, Production...etc
         /*/?BB       
                var workflow =  $(result).find("#workflow").val();
                //alert("workflow : "+workflow);

                var productId = $("#workflowProductId").val();

                if(workflow!=null && workflow!="" && workflow=="success"){
                   
                    $("#button_wrapper").replaceWith($('#button_wrapper', $(result)));
                    $("#table_inner_wrapper").replaceWith($('#table_inner_wrapper', $(result)));
                    //have to change the code Mari.
                    $("#fkCodelstProductsource").val("");

                    $('#acquisitionTable tbody[role="alert"] tr').each(function(i,el) {
                        var productId = this.cells[11].innerHTML;
                        var status = this.cells[15].innerHTML;
                        if(productId==productSearch && status=="Active"){
                            this.cells[15].innerHTML="Acquired";
                            $(this).removeAttr("style");
                        }
                        $( "#acquisitionDateStr" ).datepicker({dateFormat: 'M dd, yy'});
                    });
                   
                 }
                else{
                  //    alert("productId : "+productId);
                     var url = workflow+".action?productId="+productId;
                     //showPopUp("close","addNewDialog","","","");
                     //alert(url);
                     loadAction(url);
                     //alert("after load");
                }
			*/
                },
                error: function (request, status, error) {
                    alert("Error " + error);
                }
            });
            }catch(err){
                alert(" load action error " + err);
            }
}
function clear(){
    var select="Select"
        ///** Reset the form values*/
        $("#acquisitionDateStr").val("");
        setComboValue(select,"fkCodelstProcessSelection");
        setComboValue(select,"fkCodelstLocation");
        $("#radioYes").removeAttr('checked');
        $("#radioNo").removeAttr('checked');
    }
function resetvalues(){
    var select="Select";
    setComboValue(select,"fkCodelstFinalDisposition");
    setComboValue(select,"fkCodelstProcessSelection");
    setComboValue(select,"fkCodelstPersonlocation");
    setComboValue(select,"fkCodelstProductsource");
    setComboValue(select,"fkCodelstProductType");
    setComboValue(select,"fkCodelstLocation");
    $("#radioYes").removeAttr('checked');
    $("#radioNo").removeAttr('checked');
    }

function checkActionCall(){
    //alert(contextpath);
    try{
         $.ajax({
                type: "POST",
                url: contextpath+"saveAcquisition.action?aquLocation=acqlocation&productType=producttype&finalDisp=finaldispos&donorLoc=location&procSelec=procfacility&productSource=prodsource",
                async:false,
                data : $("#acquisitionForm").serialize(),
                success: function (result){
                //alert(" success" + $(result).find("#returnMessage").val());
                var res = $(result).find("#returnMessage").val();
                //alert(" success" + res);
                $("#returnMessage").val(res);
                //$('#main').html(result);
                //alert($(result).find("#button_wrapper").html());
                $('#button_wrapper').html($(result).find("#button_wrapper").html());
                $('#table_inner_wrapper').html($(result).find("#table_inner_wrapper").html());
              
                },
                error: function (request, status, error) {
                    alert("Error " + error);
                }
            });
            }catch(err){
                alert(" load action error " + err);
            }
}

$(function(){
    $("select").uniform();
  });

</script>
<div class="cleaner"></div>

<!-- section ends here-->

<section>

 <div class="cleaner"></div>
 
 <div class="column">       
        <div  class="portlet">
            <div class="portlet-header addnew"><s:hidden id="moduleName" value="acquisition"/><s:hidden id="widgetName" value="acquisitionProducts"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.acquisitionproducts"/></div>
            <div class="portlet-content" >
            <table cellpadding="0" cellspacing="0" border="0" id="acquisitionTable" class="display" >
            	<thead>
                   <tr>
                        <th width="4%" id="acquisitionColumn"></th>
                        <th colspan="1"><s:text name="stafa.label.recipientid"/></th>
                        <th colspan="1"><s:text name="stafa.label.recipientname"/></th>
                        <th colspan="1" ><s:text name="stafa.label.productsource"/></th>
                        <th colspan="1"><s:text name="stafa.label.producttype"/></th>
                        <th colspan="1"><s:text name="stafa.label.donorid"/></th>
                        <th colspan="1"><s:text name="stafa.label.donorname"/></th>
                        <th colspan="1"><s:text name="stafa.label.donorlocation"/></th>
                        <th colspan="1"><s:text name="stafa.label.processingfacility"/></th>
                        <th colspan="1"><s:text name="stafa.label.proposedfinaldisposition"/></th>
                        <th colspan="1"><s:text name="stafa.label.productappearance"/></th>
                        <th colspan="1"><s:text name="stafa.label.productid"/></th>
                        <th colspan="1"><s:text name="stafa.label.physicalname" /></th>
                        <th colspan="1"><s:text name="stafa.label.collectsection"/></th>
                        <th colspan="1"><s:text name="stafa.label.collectiondate" /></th>
                        <th colspan="1"><s:text name="stafa.label.status"/></th>
                    </tr>
            	</thead>
            </table>
    </div>
    </div>   
</div>       

</section>

<!-- section ends here-->

<div class="cleaner"></div>
   
<form action="#" id="acquisitionForm">
<s:hidden name="pkAcquisition" id="pkAcquisition"/>
<s:hidden name="personproductid" id="personproductid"/>
<s:hidden name="recipientName" id="recipientName"/>
<s:hidden name="recipientId" id="recipientId"/>
<s:hidden name="donorName" id="donorName"/>
<s:hidden name="donorId" id="donorId"/>
<s:hidden name="" id="status"/>
<s:hidden name="workflowProductId" id="workflowProductId" value="%{workflowProductId}"/>
<s:hidden name="returnJsonStr" id="returnJsonStr"/>
<s:hidden name="workflow" id="workflow"/>
<div class="column">       
        <div  class="portlet">
            <div class="portlet-header"><s:hidden id="moduleName" value="acquisition"/><s:hidden id="widgetName" value="acquisitionInformation"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.acquisitioninformation"/></div>
        <div class="portlet-content">
       <div id="button_wrapper">
		<div style="margin-left:40%;">
		
		<input class="scanboxSearch barcodeSearch"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;"/>
		<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" />
	
		
		
		
		</div>
		</div>
            <div id="table_inner_wrapper">
            <s:hidden id="returnMessage" value="%{returnMessage}"/>
                <div id="left_table_wrapper">
                    <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                        <tr>
                            <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.acquisitiondatestr"/></div></td>
                            <td width="50%" height="40px"><div align="left"><input type="text" id="acquisitionDateStr" name="acquisitionDateStr" placeholder="" class="dateEntry"/></div></td>
                        </tr>
                        <tr>
                            <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.acquiredtime" /></div></td>
                            <td width="50%" height="40px"><div align="left"><span style="width: 100px;" id="arrivalDate" class="jclock" name="acquiredTime" type="text"></span>&nbsp;<span name="acquiredTimeZone" id="timeZone"></span></div></td>
                        </tr>
                        <tr>
                            <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.fkcodelstlocation" /></div></td>
                            <td width="50%" height="40px"><div align="left"><s:select  headerKey="" headerValue="Select"  headerKey="" headerValue="Select" list="acqLocationList" listKey="pkCodelst" listValue="description" name="fkCodelstLocation" id="fkCodelstLocation"/></div></td>
                        </tr>
                        <tr>
                            <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.producttype" /></div></td>
                            <td width="50%" height="40px"><div align="left"><s:select  headerKey="" headerValue="Select"  list="productTypeList" listKey="pkCodelst" listValue="description" name="fkCodelstProductType" id="fkCodelstProductType"/></div></td>
                        </tr>
                        <tr>
                                <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.productsource" /></div></td>
                                <td width="50%" height="40px"><div align="left"><s:select  headerKey="" headerValue="Select" list="productSourceList" listKey="pkCodelst" listValue="description" name="fkCodelstProductsource" id="fkCodelstProductsource"/></div></td>
                            </tr>
                           
       
     
                           
                           
                           
                    </table>
                </div>

                <div id="middle_line"></div>
                <div id="right_table_wrapper">                   
                   
                        <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                           
                            <tr>
                                <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.fkcodelstpersonlocation" /></div></td>
                                <td width="50%" height="40px"><div align="left"><s:select  headerKey="" headerValue="Select" list="donorLocList" listKey="pkCodelst" listValue="description" name="fkCodelstPersonlocation" id="fkCodelstPersonlocation"/></div></td>
                            </tr>
                            <tr>
                                <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.fkcodelstprocessselection" /></div></td>
                                <td width="50%" height="40px"><div align="left"><s:select  headerKey="" headerValue="Select" list="procSelecList" listKey="pkCodelst" listValue="description" name="fkCodelstProcessSelection" id="fkCodelstProcessSelection"/></div></td>
                            </tr>
                            <tr>
                                <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.fkcodelstfinaldisposition" /></div></td>
                                <td width="50%" height="40px"><div align="left"><s:select  headerKey="" headerValue="Select" list="finalDispList" listKey="pkCodelst" listValue="description" name="fkCodelstFinalDisposition" id="fkCodelstFinalDisposition"/></div></td>
                            </tr>
                            <tr>
                                <td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.fkcodelstprodappear" /></div></td>
                                <td width="50%" height="40px"><div align="left"><input type="radio" class="selectDelect" onclick="" name="radio1" id="radioYes"> Y &nbsp; <input type="radio" class="selectDelect" name="radio1" id="radioNo"  onclick="" /> N</div>
                                <input type="hidden" id="fkCodelstProdappear" value="1" name="fkCodelstProdappear"/></td>
                            </tr>
                        </table>
                </div>
            </div>
            </div>
        </div><br/>
       

       
    </div>
            <div id="bor" style="float:right;">
        
                <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                <td>Next:</td>
    			<td>
                 
                 <div>
                 <select name="nextOption" id="nextOption">
                 	<option value="select">Select</option>
               		<s:iterator value="nextOptions" status="row" var="cellValue">  
	               		<s:if test="%{#cellValue[2] == 0}">
	                 		<option value="<s:property value="#cellValue[1]"/>"> <s:property value="#cellValue[0]"/></option>
	                 	</s:if>

	                 </s:iterator>
                 </select>
                 </div></td>
                
                <td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
                <td><input type="button" onclick="saveAcquisition();" value="Save"/></td>
                </tr>
                </table>
          
        </div>
</form>

   <div id="addNewDialog" title="addNew" style="display: none">
	<s:hidden name="pagesFrom" value="acquistion"/>
		<%-- <jsp:include page="addnewproduct.jsp"></jsp:include> --%>
  </div>
    <div class="cleaner"></div>
   
<script>

$(function() {
    $('.jclock').jclock();

    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
    /**For Notes**/
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
    .append("<span style=\"float:right;\" class='ui-notes'></span>");

    $( ".portlet-header .ui-notes " ).click(function() {
        showNotes('open',this);
    });

    /**For add new popup**/
   
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
    .append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

    $( ".portlet-header .ui-addnew" ).click(function() {
    	addNewPopup("Add New Product","addNewDialog","addnewAcquisition","700","1050");
    });
   
    /**For Refresh**/
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
    .append("<span style=\"float:right;\" class='ui-reset'></span>");
    $( ".portlet-header .ui-reset " ).click(function() {
        var formName = document.getElementById("login");
        //clearForm(formName);
        clear();
    });

	
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

    $('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});   
});


var now = new Date();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivalDate').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new Date()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};


$("#timeZone").html(timezones[-offset / 60]);

</script>