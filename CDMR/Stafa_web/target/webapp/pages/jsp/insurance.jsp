<%@ include file="common/includes.jsp" %>
<script type="text/javascript" src="js/workFlow.js"></script>
<link type="text/css" href="css/workFlow.css" rel="stylesheet" />
<style>
.workFlowContainer{
background-color:	#FFFFFF;;
border-radius:5px;
}
.subLinkInsurance{
border:1px solid white;
font-weight:bold;
width:60%;
text-align:center;
line-height:1.3 em;
padding:2%;
background-color:#DADADA;
}
#subMenuWrapper{
background-color:	#B5B5B5;
border-radius:5px;
}
.subDivPortlet{
background-color:	#DADADA;
border-radius:5px;
}
</style>

<script>
/* function show_eastslidewidget(divid){
    // to Hide the left side widget
	innerLayout.hide('west');
    innerLayout.show('south',true);
    innerLayout.show('north',true);
    innerLayout.show('east',false);
    var src = $("#"+divid).html();
    $("#innereast").html(src);
    $("#"+divid).html("slidedivs");
   
}  */
$(function() {
    $('.jclock').jclock();
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
    /**For Notes**/
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
    .append("<span style=\"float:right;\" class='ui-notes'></span>");

    $( ".portlet-header .ui-notes " ).click(function() {
        showNotes('open',this);
    });
});
$(document).ready(function(){
	  $("select").uniform(); 
      $(window).resize();
	show_eastslidewidget("slidedivs");
	var oTable=$("#recipientInsurance").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
											               null,
											               null,
											               null,
											               null,
											             ],
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#recipientInsuranceColumn").html($('.ColVis:eq(0)'));
												}
		});
		oTable=$("#recipientInsuranceDocuments").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										             ],
	 										"oColVis": {
	 											"aiExclude": [ 0 ],
	 											"buttonText": "&nbsp;",
	 											"bRestore": true,
	 											"sAlign": "left"
	 										},
	 										"fnInitComplete": function () {
	 									        $("#recipientInsuranceDocumentsColumn").html($('.ColVis:eq(1)'));
	 										}
									});
	 oTable=$("#patientBenefitsDocuments").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										             ],
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#patientBenefitsDocumentsColumn").html($('.ColVis:eq(2)'));
												}
									});
	 
    $("select").uniform(); 
});
$(window).resize(function(){
    var height;
    var screenheight=screen.height;
    var innerheight=$("#innercenter").height();
    var innerheight=594;
    var constanthgt=$("#constantdiv").height();
    /* if(innerheight) */
    if(screenheight<= 768){
       height =innerheight -(constanthgt+240);
    }else if(screenheight>=1024){
           height =innerheight -(constanthgt+40);

    }else if(screenheight>=800 && screenheight < 900){
           height =innerheight -(constanthgt+240);
    }else if(screenheight==900){
           height =innerheight -(constanthgt+130);
    }else if(screenheight==960){
           height =innerheight -(constanthgt+80);
    }
       $('#subdiv').height(height);
    });

</script>

<div class="cleaner"></div>

<!--right window start -->	
<div  id="slidedivs"  style="display:none">
<div id="uldiv">
<ul class="ulstyle">
<li class="listyle print_label"><img src = "images/icons/calendar.png"  style="width:30px;height:30px;"/></li>
<li class="listyle print_label" ><b>Calendar</b> </li>
<li class="listyle print_label"><img src = "images/icons/plan.png"  style="width:30px;height:30px;"/></li>
<li class="listyle notes_icon"><b>Plan</b> </li>
<li class="listyle print_label"><img src = "images/icons/Notification.png"  style="width:30px;height:30px;"/></li>
<li class="listyle print_label" ><b>Notification</b> </li>
</ul>
</div>
</div>

<div class="column">
<div  class="portlet">
	<div class="portlet-header notes">Recipient Insurance and Benefits</div>
		<div class="portlet-content">
			<br>
			<div id="subMenuWrapper" >
				<table cellpadding="0" cellspacing="0" border="0" id="" class="display">
						<tr>
							<td style="padding-right:20px"><div class="subLinkInsurance">Recipient Workflow</div></td>
							<td style="padding-right:20px"><div class="subLinkInsurance">Recipient Profile</div></td>
							<td style="padding-right:20px"><div class="subLinkInsurance">Insurance</div></td>
							<td style="padding-right:20px"><div class="subLinkInsurance">Donor Matching</div></td>
							<td style="padding-right:20px"><div class="subLinkInsurance">Available Products</div></td>
						</tr>
			</table>
			</div>
			<br>
			<div class="subDivPortlet">
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
							<tr>
								<td align="right" style="padding-top:1%">Recipient First Name : </td>
								<td  align="left" style="padding-top:1%">&nbsp;Renee</td>
								<td align="right" style="padding-top:1%">Diagnosis :</td>
								<td  align="left" style="padding-top:1%">&nbsp;AML</td>
								<td  align="right" style="padding-top:1%">Age :</td>
								<td  align="left" style="padding-top:1%">&nbsp;24</td>
								<td  align="right" style="padding-top:1%">Diagnosis :</td>
								<td  align="left" style="padding-top:1%">&nbsp;Peds</td>
						</tr>
						<tr>
								<td  align="right" style="padding-top:1%">Recipient Last Name : </td>
								<td  align="left" style="padding-top:1%">&nbsp;Branofsky</td>
								<td  align="right" style="padding-top:1%">Diagnosis Date:</td>
								<td  align="left" style="padding-top:1%">&nbsp;Jan 6, 2012</td>
								<td  align="right" style="padding-top:1%">SSN:</td>
								<td  align="left" style="padding-top:1%">&nbsp;465983875</td>
								<td  align="right">ABO/Rh:</td>
								<td  align="left">&nbsp;A+</td>
						</tr>
						<tr>
							<td  align="right" style="padding-top:1%">Status:</td>
							<td style="padding-top:1%">&nbsp;Pre-Transplant</td>
							<td  align="right" style="padding-top:1%">DOB :</td>
							<td style="padding-top:1%">&nbsp;Jan 19, 1988</td>
							<td  align="right" style="padding-top:1%">MRN :</td>
							<td style="padding-top:1%">&nbsp;586790</td>
						</tr>
							
					</table>
				</div>  
				<br><br>
		<div class="workFlowContainer">
			 <div class="workFlow">
				 <div class='workflowContent' ><div class='processstarted' onclick='checkBox(this)'></div><div class='completed' onclick='check(this)'><p class='samplet-text'>Referal</p></div></div><div class='workflowContent'><div class='processinitial' onclick='checkBox(this)'></div><div class='initial' onclick='check(this)'><p class='samplet-text'>Consult/Eval</p></div></div><div class='workflowContent'><div class='processinitial' onclick='checkBox(this)'></div><div class='initial' onclick='check(this)'><p class='samplet-text'>Pre-Transplant Visit</p></div></div><div class='workflowContent'><div class='processinitial' onclick='checkBox(this)'></div><div class='initial' onclick='check(this)'><p class='samplet-text'>Stem Cell Collection</p></div></div><div class='workflowContent'><div class='processinitial' onclick='checkBox(this)'></div><div class='initial' onclick='check(this)'><p class='samplet-text'>Preparative Regimen</p></div></div><div class='workflowContent'><div class='processinitial' onclick='checkBox(this)'></div><div class='initial' onclick='check(this)'><p class='samplet-text'>	Selected Products</p></div></div><div class='workflowContent'><div class='processinitial' onclick='checkBox(this)'></div><div class='initial' onclick='check(this)'><p class='samplet-text'>Infusion</p></div></div><div class='workflowContent'><div class='processinitial' onclick='checkBox(this)'></div><div class='initial' onclick='check(this)'><p class='samplet-text'>Followup</p></div></div>
			</div>
		</div>
		</div>
	</div>
</div>
<form>
<div class="cleaner"></div>
	<div id="subdiv" style="height:15em;overflow:auto;">
		<div class="column">		
			<div  class="portlet">
				<div class="portlet-header notes">Recipient Insurance</div>
						<div class="portlet-content">
						<table>
							<tr>
								<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
								<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
						    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
							</tr>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" id="recipientInsurance" class="display">
						<thead>
							<tr>
								<th width="4%" id="recipientInsuranceColumn"></th>
								<th colspan='1'>Select</th>
								<th colspan='1'>Provider Name</th>
								<th colspan='1'>Insurance Status</th>
								<th colspan='1'>dateEntry of Status Change</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="recipientInsuranceVerified" onclick=""></td>
								<td>Medicaid</td>
								<td><select><option>Select</option><option>Active</option><option>Expired</option></select></td>
								<td><input type="text"  class="dateEntry"></td>
							</tr>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="recipientInsuranceVerified" onclick=""></td>
								<td>Blue Cross</td>
								<td><select><option>Select</option><option>Active</option><option>Expired</option></select></td>
								<td><input type="text"  class="dateEntry"></td>
							</tr>
						</tbody>
						</table>
			</div>
		</div>
</div>
<div class="cleaner"></div>
<div class="column">	
	<div  class="portlet">
			<div class="portlet-header notes">Recipient Insurance Documents</div>
			<div class="portlet-content">
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
			    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" id="recipientInsuranceDocuments" class="display">
			<thead>
				<tr>
					<th width="4%" id="recipientInsuranceDocumentsColumn"></th>
					<th colspan='1'>Select</th>
					<th colspan='1'>Document Name</th>
					<th colspan='1'>dateEntry</th>
					<th colspan='1'>Attachment</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td><input type='checkbox'  id="DocumentVerified" onclick=""></td>
					<td><input type="text"  id=""></td>
					<td><input type="text"  class="dateEntry"></td>
					<td><img src="images/icons/Black_Paperclip.jpg"></td>
				</tr>
				<tr>
					<td></td>
					<td><input type='checkbox'  id="DocumentVerified" onclick=""></td>
					<td><input type="text"  id=""></td>
					<td><input type="text"  class="dateEntry"></td>
					<td><img src="images/icons/Black_Paperclip.jpg"></td>
				</tr>
			</tbody>
			</table>
		</div>
	</div>
</div>
<div class="cleaner"></div>
<div class="column">
	<div  class="portlet">
	<div class="portlet-header notes">Recipient Inusrance Details</div>
		<div class="portlet-content">
		 	<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">ProviderName</td>
					<td class="tablewapper1"><select><option>Select</option><option>Medicaid</option></select></td>
					<td class="tablewapper">Compant Info Contact Person</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
				</tr>
				<tr>
					<td class="tablewapper">Special Handling</td>
					<td class="tablewapper1"><select><option>Select</option><option>Medicaid</option></select></td>
					<td class="tablewapper">Compant Info Phone Number</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
                </tr>
                <tr>
					<td class="tablewapper">Subscriber</td>
					<td class="tablewapper1"><select><option>Select</option><option>Medicaid</option></select></td>
					<td class="tablewapper">MCRP artD EffdateEntry</td>
					<td class="tablewapper1"><input type="text" id="" name="" class="dateEntry"/></td>
				</tr>
				<tr>
					<td class="tablewapper">Global Status</td>
					<td class="tablewapper1"><select><option>Select</option><option>Medicaid</option></select></td>
					<td class="tablewapper">TxRxAuth Code</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
				</tr>
				<tr>
					<td class="tablewapper">Payer Level</td>
					<td class="tablewapper1"><select><option>Select</option><option>Medicaid</option></select></td>
					<td class="tablewapper">Ins. Coverage</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
				</tr>
				<tr>
					<td class="tablewapper">Payment Methodology</td>
					<td class="tablewapper1"><select><option>Select</option><option>Medicaid</option></select></td>
					<td class="tablewapper">Group Name</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
				</tr>
				<tr>
					<td class="tablewapper">ID Number</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">Group Number</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
				</tr>
				<tr>
					<td class="tablewapper">Cobra Term dateEntry</td>
					<td class="tablewapper1"><input type="text" id="" name="" class="dateEntry"/></td>
					<td class="tablewapper">Is Verified</td>
					<td class="tablewapper1"><input type="radio"/>Yes&nbsp;<input type="radio"/>No<input type="radio"/></td>
				</tr>
				<tr>
					<td class="tablewapper">Effective dateEntry</td>
					<td class="tablewapper1"><input type="text" id="" name="" class="dateEntry"/></td>
					<td class="tablewapper">Verified  by?</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
				</tr>
				<tr>
					<td class="tablewapper">Term dateEntry</td>
					<td class="tablewapper1"><input type="text" id="" name="" class="dateEntry"/></td>
					<td class="tablewapper">Verified dateEntry</td>
					<td class="tablewapper1"><input type="text" id="" name="" class="dateEntry"/></td>
				</tr>
				<tr>
					<td class="tablewapper">Annual Deductable Met</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">Rx Plan Phone</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
				</tr>
				<tr>
						<td class="tablewapper">Rx Benefit Plan</td>
						<td colspan=3><textarea cols="100" rows="4"></textarea></td>
				</tr>
				<tr>
						<td class="tablewapper">Other Insurance Info</td>
						<td colspan=3><textarea cols="100" rows="4"></textarea></td>
				</tr>
				<tr>
						<td class="tablewapper">Notes</td>
						<td colspan=3><textarea cols="100" rows="4"></textarea></td>
				</tr>
			</table>
		</div>
	</div>
</div>
	
<div class="column">       
    <div  class="portlet">
    <div class="portlet-header">Network Details</div><br>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Network Name</td>
                    <td class="tablewapper1"><input type="text" id="" name=""/></td>
                    <td class="tablewapper">&nbsp;</td>
                    <td class="tablewapper1">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tablewapper">Network Start Date</td>
                    <td class="tablewapper1"><input type="text" id="networkstartDate" class="dateEntry"/></td>
                    <td class="tablewapper">Network End Date</td>
                    <td class="tablewapper1"><input type="text" id="networkendDate" class="dateEntry"/></td>
                </tr>
                <tr>
                    <td class="tablewapper">&nbsp;</td>
                    <td class="tablewapper1" align='center'>In</td>
                    <td class="tablewapper">&nbsp;</td>
                    <td class="tablewapper1" align='center'>Out</td>
                </tr>
                <tr>
                    <td class="tablewapper">Co-Pay</td>
                    <td class="tablewapper1"><input type="text" id="" name=""/></td>
                    <td class="tablewapper">&nbsp;</td>
                    <td class="tablewapper1"><input type="text" id="" name=""/></td>
                </tr>
                <tr>
                    <td class="tablewapper">Tx - Lifetime Benefits</td>
                    <td class="tablewapper1"><textarea rows="4" cols="20"></textarea></td>
                    <td class="tablewapper">&nbsp;</td>
                    <td class="tablewapper1"><textarea rows="4" cols="20"></textarea></td>
                </tr>
                <tr>
                    <td class="tablewapper">Annual Deductible</td>
                    <td class="tablewapper1"><input type="text" id="" name=""/></td>
                    <td class="tablewapper">&nbsp;</td>
                    <td class="tablewapper1"><input type="text" id="" name=""/></td>
                </tr>
                <tr>
                    <td class="tablewapper">Annual Deductible Max</td>
                    <td class="tablewapper1"><input type="text" id="" name=""/></td>
                    <td class="tablewapper">&nbsp;</td>
                    <td class="tablewapper1"><input type="text" id="" name=""/></td>
                </tr>
                <tr>
                    <td class="tablewapper">As of Date</td>
                    <td class="tablewapper1"><input type="text" id="asofDate" class="dateEntry"/></td>
                    <td class="tablewapper">&nbsp;</td>
                    <td class="tablewapper1"><input type="text" id="asofendDate" class="dateEntry"/></td>
                </tr>
                <tr>
                    <td class="tablewapper">Out of Pocket</td>
                    <td class="tablewapper1"><input type="text" id="" name=""/></td>
                    <td class="tablewapper">&nbsp;</td>
                    <td class="tablewapper1"><input type="text" id="" name=""/></td>
                </tr>
                <tr>
                    <td class="tablewapper">Out of Pocket Max</td>
                    <td class="tablewapper1"><input type="text" id="" name=""/></td>
                    <td class="tablewapper">&nbsp;</td>
                    <td class="tablewapper1"><input type="text" id="" name=""/></td>
                </tr>
                <tr>
                    <td class="tablewapper">Lifetime Max</td>
                    <td class="tablewapper1"><input type="text" id="" name=""/></td>
                    <td class="tablewapper">&nbsp;</td>
                    <td class="tablewapper1"><input type="text" id="" name=""/></td>
                </tr>
               <tr>
                    <td style="padding-left:8%" width="16%">Comments</td>
                    <td class="tablewapper1" colspan=4><textarea rows="4" cols="94"></textarea></td>
                </tr>
                <tr>
                    <td class="tablewapper">Phys Co-Pay</td>
                    <td class="tablewapper1"><input type="text" id="" name=""/></td>
                    <td class="tablewapper">&nbsp;</td>
                    <td class="tablewapper1"><input type="text" id="" name=""/></td>
                </tr>
                <tr>
                    <td class="tablewapper">Ins Resp/Pat Resp</td>
                    <td class="tablewapper1"><textarea rows="4" cols="20"></textarea></td>
                    <td class="tablewapper">&nbsp;</td>
                    <td class="tablewapper1"><textarea rows="4" cols="20"></textarea></td>
                </tr>
                <tr>
                    <td class="tablewapper">Room & Board</td>
                    <td class="tablewapper1"><textarea rows="4" cols="20"></textarea></td>
                    <td class="tablewapper">&nbsp;</td>
                    <td class="tablewapper1"><textarea rows="4" cols="20"></textarea></td>
                </tr>
            </table>
        </div>
    </div>
</div>
			
				
<div class="column">
	<div  class="portlet">
	<div class="portlet-header notes">Employment Info</div>
		<div class="portlet-content">
		 	<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%"  id="employmentInfoTable">
				<tr>
					<td class="tablewapper">Employment Status</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">Employment Address</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
				</tr>
				<tr>
					<td class="tablewapper">Employer</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">City</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
				</tr>
				<tr>
					<td class="tablewapper">Employer Phone Number</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">State</td>
					<td class="tablewapper1"><select><option>Select</option><option>Medicaid</option></select></td>
				</tr>
				<tr>
					<td class="tablewapper">&nbsp;</td>
					<td class="tablewapper1">&nbsp;</td>
					<td class="tablewapper">Country</td>
					<td class="tablewapper1"><select><option>Select</option><option>Medicaid</option></select></td>
				</tr>
				<tr>
					<td class="tablewapper">&nbsp;</td>
					<td class="tablewapper1">&nbsp;</td>
					<td class="tablewapper">Zipcode</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
				</tr>
				</table>
				<table cellpadding="0" cellspacing="0" border="0" id="machineTable" class="display">
				<tr>
					<td class="tablewapper">Employer Notes;</td>
					<td class="tablewapper1">&nbsp;</td>
					<td class="tablewapper">&nbsp;</td>
					<td class="tablewapper1">&nbsp;</td>
				</tr>
				<tr>
					<td colspan=4><textarea cols="120" rows="4"></textarea></td>
				</tr>
			</table>
		</div>
	</div>
</div>

<div class="column">
	<div  class="portlet">
	<div class="portlet-header notes">Patient Benefits</div>
		<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="patient BenefitsTable" class="display">
		 		<tr>
                    <td style="padding-left:8%" width="16%">Skilled Nursing</td>
                    <td class="tablewapper1" colspan=4><textarea rows="4" cols="94"></textarea></td>
                </tr>
				<tr>
					<td style="padding-left:8%" width="16%">Home Health</td>
					<td class="tablewapper1" colspan=3><textarea cols="94" rows="4"></textarea></td>
			   </tr>
				<tr>
					<td style="padding-left:8%" width="16%">Lodging</td>
					<td class="tablewapper1" colspan=3><textarea cols="94" rows="4"></textarea></td>
				</tr>
				<tr>
					<td style="padding-left:8%" width="16%">Travel</td>
					<td class="tablewapper1" colspan=3><textarea cols="94" rows="4"></textarea></td>
				</tr>
				<tr>
					<td style="padding-left:8%" width="16%">Cardiac Rehab/Phys Therapy</td>
					<td class="tablewapper1" colspan=3><textarea cols="94" rows="4"></textarea></td>
				</tr>
				<tr>
					<td style="padding-left:8%" width="16%">Neupogen</td>
					<td class="tablewapper1" colspan=3><textarea cols="94" rows="4"></textarea></td>
				</tr>
				<tr>
					<td style="padding-left:8%" width="16%">Donor Benefits</td>
					<td class="tablewapper1" colspan=3><textarea cols="94" rows="4"></textarea></td>
				</tr>
				<tr>
					<td style="padding-left:8%" width="16%">Ins Desginated Lab</td>
					<td><input type="text" id="" name=""/></td>
					<td style="padding-left:8%" width="16%">MHS Case Manger/Coordinator</td>
					<td><input type="text" id="" name=""/></td>
				</tr>
				<tr>
					<td style="padding-left:8%" width="16%">Ins Desginated Lab</td>
					<td ><input type="text" id="" name=""/></td>
					<td style="padding-left:8%" width="16%">MHS Case Manger/Coordinator Phone #</td>
					<td ><input type="text" id="" name=""/></td>
				</tr>
				<tr>
					<td style="padding-left:8%" width="16%">Ins Case Manger Phone Number</td>
					<td ><input type="text" id="" name=""/></td>
					<td style="padding-left:8%" width="16%">MHS Case Manger/Coordinator Fax #</td>
					<td ><input type="text" id="" name=""/></td>
				</tr>
				</table>
		</div>
	</div>

<div class="column">		
		<div  class="portlet">
		<div class="portlet-header notes">Patient Benefits Documents</div>
		<div class="portlet-content">
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
			    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" id="patientBenefitsDocuments" class="display">
			<thead>
				<tr>
					<th width="4%" id="patientBenefitsDocumentsColumn"></th>
					<th colspan='1'>Select</th>
					<th colspan='1'>Document Name</th>
					<th colspan='1'>dateEntry</th>
					<th colspan='1'>Attachment</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td><input type='checkbox'  id="DocumentVerified" onclick=""></td>
					<td><input type="text"  id=""></td>
					<td><input type="text"  class="dateEntry"></td>
					<td><img src="images/icons/Black_Paperclip.jpg"></td>
				</tr>
				<tr>
					<td></td>
					<td><input type='checkbox'  id="DocumentVerified" onclick=""></td>
					<td><input type="text"  id=""></td>
					<td><input type="text"  class="dateEntry"></td>
					<td><img src="images/icons/Black_Paperclip.jpg"></td>
				</tr>
			</tbody>
			</table>
		</div>
	</div>
</div>
	<div class="column">
	<div  class="portlet">
	<div class="portlet-header notes">Unfunded/Underfunded Case Management</div>
		<div class="portlet-content">
		 	<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%"  id="unfundedUnderfundedTable">
				<tr>
					<td class="tablewapper">Patient Monthly Income $</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">&nbsp;</td>
					<td class="tablewapper1">&nbsp;</td>
				</tr>
			<tr>
					<td class="tablewapper">Patient Family Income -Total $</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">&nbsp;</td>
					<td class="tablewapper1">&nbsp;</td>
			</tr>
			<tr>
					<td class="tablewapper">Minors in the household</td>
					<td class="tablewapper1"><input type="radio"/>Yes&nbsp;<input type="radio"/>No<input type="radio"/></td>
					<td class="tablewapper">Number of Minors </td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
			</tr>
			<tr>
					<td class="tablewapper">Minors in the household</td>
					<td class="tablewapper1"><input type="radio"/>Yes&nbsp;<input type="radio"/>No<input type="radio"/></td>
					<td class="tablewapper">Number of Minors </td>
					<td class="tablewapper1"><input type="text" id="" name="" class="dateEntry"/></td>
				</tr>
			</table>
		</div>
	</div>
</div>

<%-- <div class="floatright_right">
	<jsp:include page="sidecontrols.jsp"></jsp:include>
 </div> --%>
 
<div class="cleaner"></div>
		
<div align="right" style="float:right;">

		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
			<tr>
				<td>Next:</td>
				<td><div class="mainselection"><select name="" id="" ><option value="">Select</option><option>Preparation</option><option>Verification</option><option>Collection</option><option>Post-Collection</option></select></div></td>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;&nbsp;</td>
				<td><input type="button" value="Next" onclick="saveOvernightStorage()"/></td>
			</tr>
		</table>

</div>
</div>
</div>
</form>


<!-- <script type="text/javascript">



var now = new dateEntry();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivaldateEntry').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new dateEntry()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};
$("#timeZone").html(timezones[-offset / 60]);

</script>
 -->













