<%@ include file="common/includes.jsp" %>
<!-- <script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script> -->

<!-- <link rel="stylesheet"href="css/uniform.default.css" type="text/css" media="print">
<link type="text/css" href="css/Velos_style.css" rel="stylesheet" media="print"/>
<link type="text/css" href="css/fonts.css" rel="stylesheet" media="print"/>
<link  type="text/css" href="css/menu/velos_default.css" media="print" rel="stylesheet" type="text/css" /> 
<link  type="text/css" href="css/menu/velos_default.ultimate.css" media="print" rel="stylesheet" type="text/css" /> 
<link  type="text/css" href="css/menu/velos_dropdown.css" media="print" rel="stylesheet" type="text/css" /> -->
<script type="text/javascript" src="js/ajaxfileupload.js"></script>
<script language="javascript">
function checkCollectionAgain(i){
	//alert(i +"<-checkCollectionAgain->" + $("#collectAgain1").val());
	 if($("#collectAgain1").is(":checked")){
	//?BBif($("#collectAgainYes").is(":checked")){	
		setValidation(true);
	}else{
		setValidation(false);
	}
}
/*
function checkCollectionAgain1(){
	alert("<-checkCollectionAgain1->" + $("#collectAgainYes").val() + " / " + $("#collectAgainYes").is(":checked"));
	//?BB if($("#collectAgain1").is(":checked")){
	if($("#collectAgainYes").is(":checked")){	
		setValidation(true);
	}else{
		setValidation(false);
	}
}
	*/
function setValidation(flag){
	if(flag){
		//alert("if");
		if($("#dayOfCollection").hasClass("validate_required")==false){
			$("#dayOfCollection").addClass("validate_required condtionMandatory");
			$("#uniform-dayOfCollection").after("<sup style='margin-left:2px'><label class='mandatory'>*</label></sup>");
			$("#dayOfCollection").bind("change",function(){
				if( this.value=="" || this.value=="Select") {
					showErrorMessage(this,messageArray['dayOfCollection']);
				}else{
					hideErrorMessage(this);
				}
			});
		}
	}else{
		//alert("else");
			$("#dayOfCollection").removeClass("validate_required");
			$("#uniform-dayOfCollection").parent().find("sup").remove();
			$("#dayOfCollection").unbind("change");
			hideErrorMessage($("#dayOfCollection"));
		}
	
	
	 $.uniform.restore('#dayOfCollection');
	 $('#dayOfCollection').uniform();
}
/*$(function() {
	$("#attacheFileTextDiv").click(function(){
		$("#attach").click();
	})
});
*/


function delete_document(docId){
	//alert("delter doc ::"+docId);
	var deletedIds="";
	try{
			deletedIds += docId + ",";
			deletedIds = deletedIds.substr(0,deletedIds.length-1);
			
			jsonData = "{deletedIds:'"+deletedIds+"',domainName:'DOMAIN_DOCUMENT',domainKey:'DOCUMENT_ID'}";
			
			url="commonDelete";
			response = jsonDataCall(url,"jsonData="+jsonData);
			//alert("Response:::"+response);
			loadFollowupData();
			$("#fileAttachDiv").css("display","block");
			$("#attach").css( 'display', 'block' );
	}catch(err){
		alert(" error " + err);
	} 
}
var donormrn;

function getslide_widgetdetails(){
	
	var donor=$("#donor").val();
	url="fetchDonorData";
	var jsonData = "donor:"+donor+",donorProcedure:"+donorProcedure;
	//alert(jsonData);
	response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	var donorDetails = response.donorDetails;
	donormrn="";
	donormrn=donorDetails.personMRN;
	$("#slide_donorId").text(donorDetails.personMRN);
	$("#slide_donorDOB").text(donorDetails.dob);
	$("#slide_donorName").text(donorDetails.lastName + " " + donorDetails.firstName);
	$("#slide_procedure").text(donorDetails.plannedProceduredesc);
	var tcCheck = donorDetails.plannedProceduredesc.slice(-2);
 	if(tcCheck == "TC"){
 		$(".cd34").hide();
 		
 	}else{
 		$(".cd3").hide();
 	}
	$("#slide_donorWeight").text(donorDetails.personWeight);
	$("#slide_donorABORH").text(donorDetails.bloodGrpName);
	$("#slide_targetVol").text(donorDetails.targetProcessingVolume);
	$("#slide_donation").text(donorDetails.donorTypeName);
	$("#recipient_id").text(donorDetails.recipient_id);
	$("#recipient_name").text(donorDetails.recipient_name);
	$("#recipient_dob").text(donorDetails.recipient_dob);
	$("#recipient_aborh").text(donorDetails.recipient_bloodGroup);
	$("#recipient_diagnosis").text(donorDetails.diagonsis);
	$("#idms").text(donorDetails.idmsResult);
	var idmCheck = donorDetails.idmsResult;
	if(idmCheck=="Reactive"){
		$("#idms").addClass('idmReactive');
	}else if(idmCheck=="Pending"){
		$("#idms").addClass('idmPending');
	}
	$("#westnile").text(donorDetails.westNileDesc);
	$("#donorcd34").text(donorDetails.donorCD34);
	$("#cd34_day1").text(donorDetails.productCD34);
	$("#cum_CD34").text(donorDetails.productCUMCD34);
	$("#recipient_weight").text(donorDetails.recipient_weight);
	$("#slide_donorHeight").text(donorDetails.personHeight);
}

function viewDocument(docId){
    var contextpath = "<%=request.getContextPath()%>";
    var url = contextpath+"/viewdocument.action?docId="+docId;
    window.open( url, "Documents", "status = 1, height = 500, width = 800, resizable = 0" );
}

function loadFollowupData(){
	//alert("loadFollowupData");
	$("#fileName").val("");
	$("#filePathId").val("");
	$("#fileUploadContentType").val("");
	 $("#pkDocumentId").val("0");
	
	var url = "loadFollowupData";
	var donor=$("#donor").val();
	var donorProcedure = $("#donorProcedure").val();
	if(donor !=0 && donor != ""){
		var jsonData = "donor:"+donor+",donorProcedure:"+donorProcedure;
		response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	}
	var fileValue = "<table>";
	if($(response.followupList).length == 0){
		 $(response.contactInfoList).each(function(i,t){
				$("#donorHomePhone").val(t[1]);
	    		$("#donorCellPhone").val(t[2]);
	    		$("#donorOtherPhone").val(t[3]);
		});
		 
	 }else{
		$(response.fileList).each(function(i,k){
			
				if(k[0]!=null){
					 fileValue+="<tr><td><a href='#' onclick='viewDocument(\""+k[0]+"\")' >"+k[1]+"</a></td>"+
					 			"<td><a href='#' onclick='delete_document(\""+k[0]+"\")'>Delete</a></td></tr>";
					 $("#fileDisplayName").html(""); 
					 $("#pkDocumentId").val(k[0]);
					 
					 $("#fileAttachDiv").attr("style","display:none");
					 $("#fileNameDiv").attr("style","display:block");
				}/*else{
					 $("#fileAttachDiv").attr("style","display:block");
					 $("#fileNameDiv").attr("style","display:none");
				}*/
		});
			 fileValue+= "</table>";
			 $("#fileNameDiv").html(fileValue);
		 $(response.followupList).each(function(i,v) {
	    	
	    		$("#followUp").val(v[14]);
	    		$("#donorId").val(v[0]);
	    		$("#donorHomePhone").val(v[1]);
	    		$("#donorCellPhone").val(v[2]);
	    		$("#donorOtherPhone").val(v[3]);
	    		$("#physicianName").val(v[13]);
	    		
    	 		if(v[4]!==null && v[4]==1){
    	 			
    	 			$("#collectAgain1").addClass("on");
	    			$("#collectAgain1").trigger("click");
	    			
	    		}else if(v[4]!==null && v[4]==0){
	    			
	    			$("#collectAgain0").addClass("on");
	    			$("#collectAgain0").trigger("click");
	    			
	    		}
    	 		if(v[0]!=null){
					var orderIssueDate = converHDate(v[5]);
					$("#orderIssueDate").val(orderIssueDate);
				}else{
					$("#orderIssueDate").val("");
				}
    	 		/* if(v[5]!=null){
    				$("#orderIssueDate").val(($.datepicker.formatDate('M dd, yy', new Date(v[5]))));
    			}else{
    				$("#orderIssueDate").val("");
    			} */
    	 		if(v[6]!=null && v[6]==1){
    	 			$("#donorAfterCollection1").trigger("click");
    	 			$("#donorAfterCollection1").addClass("on");
    	 		}else if(v[6]!=null && v[6]==0){
    	 			$("#donorAfterCollection0").trigger("click");
    	 			$("#donorAfterCollection0").addClass("on");
    	 		}
    	 		if(v[7]!=null && v[7]==1){
    	 			$("#receiveMozobil1").trigger("click");
    	 			$("#receiveMozobil1").addClass("on");
    	 		}else if(v[7]!=null && v[7]==0){ 
    	 			$("#receiveMozobil0").trigger("click");
    	 			$("#receiveMozobil0").addClass("on");
    	 		}else if(v[7]!=null && v[7]==2){
    	 			$("#receiveMozobil2").trigger("click");
    	 			$("#receiveMozobil2").addClass("on");
    	 		}
    	 		if(v[8]!=null){
    				//var tmpDate = new Date(v[8]);
    				$("#tempReceiveMozobilTime").val(v[8]);
    				//$("#tempReceiveMozobilTime").val((tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes());
				}else{
					$("#receiveMozobilPost").val("");
					$("#tempReceiveMozobilTime").val("");
				}
    	 		if(v[9]!=null && v[9]==1){
    	 			$("#pocInstructionGiven1").trigger("click");
    	 			$("#pocInstructionGiven1").addClass("on");
    	 		}else if(v[9]!=null && v[9]==0){
    	 			$("#pocInstructionGiven0").trigger("click");
    	 			$("#pocInstructionGiven0").addClass("on");
    	 		}
    	 		if(v[10]!=null && v[10]==1){
    	 			$("#scannedToPharmacist1").attr("checked","checked");
    	 			//$("#scannedToPharmacist1").addClass("on");
    	 		}else if(v[10]!=null && v[10]==0){
    	 			$("#scannedToPharmacist0").attr("checked","checked");
    	 			//$("#scannedToPharmacist0").addClass("on");
    	 		}
    	 		if(v[15]!=null){
    				//var tmpDate = new Date(v[8]);
    				//$("#tempReceiveMozobilTime").val((tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes());
    				
    				$("#tempfollowReceiveOrder").val(v[15]);
				}else{
					$("#tempfollowReceiveOrder").val("");
				}
    	 		if(v[16]!==null && v[16]==1){
	    			$("#followChardBMC1").attr("checked","checked");
	    		//	$("#followChardBMC1").addClass("on");
	    		}else if(v[16]!==null && v[16]==0){
	    			$("#followChardBMC0").attr("checked","checked");
	    		//	$("#followChardBMC0").addClass("on");
	    		}
    	 		if(v[17]!==null && v[17]==1){
	    			$("#notifyDonor1").attr("checked","checked");
	    		//	$("#notifyDonor1").addClass("on");
	    		}else if(v[17]!==null && v[17]==0){
	    			$("#notifyDonor0").attr("checked","checked");
	    			//$("#notifyDonor0").addClass("on");
	    		}
    	 		$("#istructionToDonor").val(v[11]);
    	 		$("#dayOfCollection").val(v[12]);
    	 		$("#physicianName").val(v[13]);
    	 		/* if(v[12]!=null && v[12]==1){
    	 			$("#dayOfCollection1").trigger("click");
    	 			$("#dayOfCollection1").addClass("on");
    	 		}else{
    	 			$("#dayOfCollection0").trigger("click");
    	 			$("#dayOfCollection0").addClass("on");
    	 		} */
	    	
		});
	 }
	 $.uniform.restore('select');
	 $('select').uniform();
}
var checkdonorflag=false;
var checknurseflag=false;
function loadVerificationdetails(){
	getslide_widgetdetails();
	$("#nurse_slidewidget1").html($("#nurse_slidewidget").html())
	show_slidewidgets("nurse_slidewidget1",false);
	$("#slidecontrol1").html($("#slidecontrol").html())
    show_slidecontrol("slidecontrol1");
	mobilizationInstruction();
    loadFollowupData();
   
}
function checkdonormrn(){
	checkdonorflag=false;
			 var enteredVal = $("input[name='donorId']").val();
			 var nurseentered=$("input[name='nurseID']").val();
			 //var nurseentered=$("input[name='nurseID']").val();
			 var url="checkFollowupData";
			 var verificationId;
			 var donor=$("#donor").val();
				 if(donor !=0 && donor != ""){
						var jsonData = "donor:"+donor;
						response = jsonDataCall(url,"jsonData={"+jsonData+"}");
					}
				 $("#personId").val("");
				 $(response.followupCheckList).each(function(i,v){
					for(m=0;m<v.length;m++){
						verificationId = v[0];
						$("#personId").val(v[1]);
					} 
				 });
				
			 if(enteredVal==verificationId){
				$("#donorVerified").html('<div align="center"><img src = "images/icons/verified.png" class="cursor"/>Verified</div>');
		    	$("#donorVerified").show();
		    	checkdonorflag=true;
		    	
			 }else{
				 alert("Donor MRN Mismatch ");
				 checkdonorflag=false;
				// checknurseflag=false;
				 hide_slidewidgets();
				hide_slidecontrol();
				$("#donorVerified").html('<div align="center"><img src = "images/icons/delete.gif" class="cursor"/>Mismatch</div>');
				 // $("#nurseVerified").html('<div align="center"><img src = "images/icons/delete.gif" class="cursor"/>Mismatch</div>');
			    $("#donorVerified").show();
			    nurseentered = "";
			    $("#nurseVerified").html('');
			      $("#followupForm").find(":input").each(function(){
			    	 switch(this.type) {
	   	 	            case 'text':
	   	 	            case 'textarea':
	   	 	                $(this).val('');
	   	 	                break;
	   	 	            case 'checkbox':
	   	 	            case 'radio':
	   	 	                this.checked = false;
	   	 	                break;
	   	 	        }
			    });
			      $("input[name='donorId']").val(enteredVal);
			      $("input[name='nurseID']").val(nurseentered);
			      
			    $("#fileAttachDiv").attr("style","display:block");
				$("#fileNameDiv").attr("style","display:none");
			 }
			 return checkdonorflag;
}
function checknurseid(){
	checknurseflag=false;
	 var nurseentered=$("input[name='nurseID']").val();
	 var enteredVal = $("input[name='donorId']").val();
	 var url="verifyCustomerId";
		 var jsonData = "{customerId:'"+nurseentered+"'}";
		 var response = jsonDataCall(url,"jsonData="+jsonData);
	 if(response.verifyStatus=="true"){
	
    	$("#nurseVerified").html('<div align="center"><img src = "images/icons/verified.png" class="cursor"/>Verified</div>');
    	$("#nurseVerified").show();
    	checknurseflag=true;
    	
	 }else{
		 alert("Nurse ID Mismatch");
		  $("#nurseVerified").html('<div align="center"><img src = "images/icons/delete.gif" class="cursor"/>Mismatch</div>');
			//$("#donorVerified").html('<div align="center"><img src = "images/icons/delete.gif" class="cursor"/>Mismatch</div>');

	    $("#nurseVerified").show();
	    enteredVal="";
	    $("#donorVerified").html('');
	    hide_slidewidgets();
		hide_slidecontrol();
	    checknurseflag=false;
	    //checkdonorflag=false;
	    $("#followupForm").find(":input").each(function(){
	    	 switch(this.type) {
	 	            case 'text':
	 	            case 'textarea':
	 	                $(this).val('');
	 	                break;
	 	            case 'checkbox':
	 	            case 'radio':
	 	                this.checked = false;
	 	                break;
	 	        }
	    });
	    $("input[name='donorId']").val(enteredVal);
	    $("input[name='nurseID']").val(nurseentered);
	    $("#fileAttachDiv").attr("style","display:block");
		$("#fileNameDiv").attr("style","display:none");
	 }
	 return checknurseflag;
}
//?Mohan  function compareID(){
/*$("input[name='donorId']").bind("keypress",function(e){
	 if(e.keyCode==13){
		
		
		 checkdonormrn();
		 if( checkdonorflag!=false && checknurseflag!=false){
			loadVerificationdetails();
		}
	 }
	});
$("input[name='nurseID']").bind("keypress",function(e){
	 if(e.keyCode==13){
		
		
		 checknurseid();
		 if( checkdonorflag!=false && checknurseflag!=false){
			loadVerificationdetails();
		}
	 }
	});
	
} */
var donorVal,donorProcedure,donorplannedprocedure,nurseVal;;
$(document).ready(function(){
	$(".titleText").html("<s:text name='stafa.label.followup'/>");
	donorVal=$("#donor").val();
	donorProcedure=$("#donorProcedure").val();
	donorplannedprocedure=$("#donorProcedure").val();
	nurseVal = $("#nurseHiddenID").val();
	var followUp = $("#followUp").val();
	$("#refDonor").val($("#donor").val());
	var entityId = $("#donorProcedure").val() +"," + donorVal;
	showTracker(entityId,"donor","apheresis_nurse","false");
	showNotesCount(donorVal,"donor",0);
	hide_slidewidgets();
	hide_slidecontrol();
	show_innernorth();
	$('#inner_center').scrollTop(0);
	$("#tempReceiveMozobilTime").timepicker({});
	$("#tempfollowReceiveOrder").timepicker({});
	$("input[name='donorId']").bind("keypress",function(e){
		 if(e.keyCode==13){
			 checkdonormrn();
			 if( checkdonorflag!=false && checknurseflag!=false){
				loadVerificationdetails();
			}
		 }else{
			 checkdonorflag=false;
		 }
		});
	$("input[name='nurseID']").bind("keypress",function(e){
		 if(e.keyCode==13){
			 checknurseid();
			 if( checkdonorflag!=false && checknurseflag!=false){
				loadVerificationdetails();
			}
		 }else{
			 checknurseflag=false;
		 }
		});
	if(followUp>0){
		loadVerificationdetails();
		checkdonormrn();
		$("#nurseID").val(nurseVal);
		checknurseid();
	}/* else{
		compareID();
	} */
	
	 $("#orderIssueDate").datepicker({
									dateFormat: 'M dd, yy',
									changeMonth: true,
									changeYear: true, 
									yearRange: "c-50:c+45"
								 });
	$("#electrolyteTable").dataTable({
										"bJQueryUI": true,
										"sDom": 'C<"clear">Rlfrtip',
 										"oColVis": {
 											"aiExclude": [ 0 ],
 											"buttonText": "&nbsp;",
 											"bRestore": true,
 											"sAlign": "left"
 										},
 										"fnInitComplete": function () {
 									        $("#electrolyteColumn").html($('.ColVis:eq(0)'));
 										}
									});
	$("#parameterTable").dataTable({
										"bJQueryUI": true,
										"sDom": 'C<"clear">Rlfrtip',
											"oColVis": {
												"aiExclude": [ 0 ],
												"buttonText": "&nbsp;",
												"bRestore": true,
												"sAlign": "left"
											},
											"fnInitComplete": function () {
										        $("#parameterColumn").html($('.ColVis:eq(1)'));
											}
									});
    $("select").uniform(); 
    $('.phoneMask').mask('999-999-9999');
    
    
    var param ="{module:'APHERESIS',page:'FOLLOWUP'}";
	markValidation(param);
	checkCollectionAgain();
});
function savePages(mode){
	
	if(checkModuleRights(STAFA_FOLLOWUP,APPMODE_SAVE)){
	try{
		//if(($("#personId").val()!="")&&(checkdonorflag!=false && checknurseflag!=false)){
		if(($("#personId").val()!="")&& checkdonormrn() && checknurseid()){	
			if(mode=="next" && !setMobilizationValidation()){
				return false;
			}
			
			if(saveFollowup()){
				return true;
			}
		}
	}catch(e){
		//alert("exception " + e);
	}
	}
}
var curdate=$.datepicker.formatDate('M dd, yy', new Date());
function receiveMozobilTime(){
   $('#receiveMozobilPost').val(curdate +" "+ $('#tempReceiveMozobilTime').val());
}

function followReceiveOrders(){
   $('#followReceiveOrder').val(curdate +" "+ $('#tempfollowReceiveOrder').val());
}

function saveFollowup(){
	//alert($("#flowNextFlag").val());
	$("#followupForm").find("#flowNextFlag").val($("#loginForm #flowNextFlag").val());
		
		
	var url='saveFollowup';
	var response = ajaxCall(url,"followupForm");
	//alert("Data saved succesfully");
	stafaAlert('Data saved Successfully','');
	loadFollowupData();
	//?Pradeep $("#main").html(response);
	 return true;
}

/*Print Methods Starts Here */
function printOpt(){
	var tab="";
	tab+= "<form action='#' id='widgetcollectForm'> <table cellpadding='0' cellspacing='0' border='0'>";
	$("#followUpSide .portlet-header").each(function(i){
		tab+="<tr><td  style='font-weight:bold'>"+$(this).text()+"</td>";
		tab+="<td><input type='checkbox' id='"+$(this).parent().parent().attr('id')+"'/></td></tr>";
		tab+="<div align='right' style='float:right;'>";
	});
		$("#followupForm .portlet-header").each(function(i){
			tab+="<tr><td  style='font-weight:bold'>"+$(this).text()+"</td>";
			tab+="<td><input type='checkbox' id='"+$(this).parent().parent().attr('id')+"'/></td></tr>";
			tab+="<div align='right' style='float:right;'>";
		});
	tab+="</table>";
	tab+="<table cellpadding='0' align='right' cellspacing='0' border='0'>";
	tab+="<tr>";
	tab+="<td><input type='button' onclick='printToDiv();' value='Print'/></td>";
	tab+="<td><input type='button' onclick='closePopup();' value='Cancel'/></td>";	
	tab+="<tr></table></div> </form>";
	$("#widgetCollect").html(tab);
	showPopUp("open","widgetCollect","Widgets","500","300");
}
function printToDiv(){
	$("#printDiv").html("");
	var cloneOb = [];
	var idArray = [];
	$("#widgetCollect :checked").each(function(i) {
	    idArray.push($(this).attr("id"));
	    cloneOb.push( $("#"+idArray[i]).clone(true));
	    $("#printDiv").append(cloneOb[i]);
	   
	});
	showPopUp("close","widgetCollect","Widgets","500","300");
	//$('#printDiv').jqprint({ operaSupport: true });
	PrintDiv('printDiv');
	// $('<div style="width:100%;height:100%;" />').html("<div>asdfdsfjhsdjhv </div>").printData();
}
function PrintDiv(divName) {
    var divToPrint = document.getElementById(divName);
   /*  $j("#printDiv textarea").each(function (i, el) {
        var  text = $j(this).text();
        var parent = $j(el).closest("textarea");
        $j(parent).replaceWith(text);
    });  */
    var popupWin = window.open('', '_blank', 'width=600,height=500,scrollbars=yes');
    popupWin.document.open();
    popupWin.document.write('<html><body onload="window.print();window.close()">' + divToPrint.innerHTML + '</body></html>');
    popupWin.document.close();
} 
function closePopup(){
	try{
    		$("#widgetCollect").dialog("destroy");
  		}catch(err){
			alert("err" + err);
		}
}
/*function printWidgets(){
//	showPopUp("open","addLabTest","Add Donor Lab Test","600","550");
	showPopUp("open","widgetCollect","Widgets","500","300");

}*/
/*function closeWidgets(){
	$("#widgetcollect").dialog( "destroy" )
	}*/
function setMobilizationValidation(){
		
		var recmobile= $('input:radio[name=receiveMozobil]:checked').val();
		//var recmobile=$('input[name=receiveMozobil]').val();
		var mobflag=false;
		if(recmobile==1){
	
			if($("input[name='scannedToPharmacist']").is(":checked")){
						$("#receivemob_yes").hide();
						mobflag=true;
					}else{
						$("#receivemob_yes").show();
						mobflag=false;
					}
					var templength1=$("#tempReceiveMozobilTime").val().length;
						
						if(templength1 > 1){
								$("#receiveMozobilTime").hide()
								mobflag=true
						}else if(templength1<=0){
								$("#receiveMozobilTime").show()
								mobflag=false;
						}
					
		}else if(recmobile==0){				
					if($("input[name='followChardBMC']").is(':checked')){
							$("#donor_bmc").hide();
							mobflag=true;
						}else{
							$("#donor_bmc").show();
							mobflag=false;
						}
			
		}else if(recmobile==2){
				
						if($("input[name='notifyDonor']").is(":checked")){
							$("#notify_donor").hide();
							mobflag=true;
						}else{
							$("#notify_donor").show();
							mobflag=false;
						}

					var templength=$("#tempfollowReceiveOrder").val().length;
						if(templength>0){
							$("#rncallmd").hide();
							mobflag=true ;
						}else if (templength<=0){
							$("#rncallmd").show();
							mobflag=false;
						}		
		}
		return mobflag;
}



function mobilizationInstruction(){
	$('.receivemozobil_pending').hide();
    $('.receivemozobil_yes').hide();
    $('.receivemozobil_no').hide();
$('input[name=receiveMozobil]').bind("click",function(){
	var recval=$(this).val();
	
	if(recval==1){
		//$("#receiveMozobil1").trigger("click");
		    $('.receivemozobil_yes').css("display","table-row");
		    $('.receivemozobil_no').hide();
		    $('.receivemozobil_pending').hide();
	}
	if(recval==0){
		//$("#receiveMozobil0").trigger("click");
	    $('.receivemozobil_pending').hide();
	    $('.receivemozobil_yes').hide();
	    $('.receivemozobil_no').css("display","table-row");
	    
}
	if(recval==2){
		//$("#receiveMozobil2").trigger("click");
		//$('.receivemozobil_pending').hide();
	    $('.receivemozobil_pending').css("display","table-row");
	    $('.receivemozobil_yes').hide();
	    $('.receivemozobil_no').hide();
	    
}
});
}
</script>
<div id="slidecontrol1" style="display:none;"></div>
<div  id="slidecontrol" style="display:none;">
	<ul class="ulstyle">
	<span>
		<li class="listyle "><img src = "images/icons/print_small.png" onclick="printOpt();" style="width:30px;height:30px;"/></li>
		<li class="listyle"><b>Print</b></li>
	</span>
	<span>
			<li class="listyle print_label"><img src = "images/icons/Bar_Code_32.png" class="barcodePrint" onclick="loadBarcodePage();" style="width:30px;height:30px;"/></li>
			<li class="listyle print_label" ><b><s:text name="stafa.label.label"/></b> </li>
	</span>
	<span id="notesSpan">
		<li class="listyle notes_icon"  style="margin-left:18%"><img src = "images/icons/notes.png"  onclick="notes(donorVal,'donor',this,false);" style="width:30px;height:30px;"/><sup style="margin-left:2%"><span class='subspan'></span></sup></li>
		<li class="listyle notes_icon"><b><s:text name="stafa.label.notes"/></b> </li>
	</span>
	<span>
		<li class="listyle print_label"><img src = "images/icons/labresults.png"  style="width:30px;height:30px;cursor:pointer;" onclick='addNewPopup("Complete Lab Result","labResults","loadLabResult","500","1050");'/></li>
		<li class="listyle print_label" ><b><s:text name="stafa.label.labresult"/></b> </li>
	</span>
	<span>
		<li class="listyle medication_icon"><img src = "images/icons/capsule1.png" onclick="medicationpopup(donormrn,donorVal,donorplannedprocedure);" style="width:30px;height:30px;"/></li>
		<li class="listyle medication_icon"><b><s:text name="stafa.label.medication"/></b></li>
	</span>
	<span>
			<li class="listyle transfer_icon"><img src = "images/icons/transfer.png" onclick="taskAction(donorplannedprocedure,'transfer')"  style="width:30px;height:30px;cursor:pointer;"/></li>
			<li class="listyle transfer_icon" id="transfer_icon"><b><s:text name="stafa.label.transfer"/></b></li>
			
		</span>
		<span>
			<li class="listyle terminate_icon"><img src = "images/icons/terminated.png"  onclick="taskAction(donorplannedprocedure,'terminate')" style="width:30px;height:30px;cursor:pointer;"/></li>
			<li class="listyle terminate_icon"><b><s:text name="stafa.label.terminate"/></b></li>
		</span>
		<%-- <li class="listyle complete_icon"><b><s:text name="stafa.label.complete"/></b></li>
		<li class="listyle complete_icon"><img src = "images/icons/task_completed_icon.PNG"  style="width:30px;height:30px;"/></li> --%>
	</ul>
</div>
<div id="nurse_slidewidget1" style="display:none;">
</div>
<div id="followUpSide">
<div id="nurse_slidewidget" style="display:none;">
				
<!-- Patient Info start-->
			<div id="donorInfoWidget" class="column">
				<div  class="portlet">
					<div class="portlet-header "><s:text name="stafa.label.donorinformation"/></div>
						<div class="portlet-content">
							<table width="100%" border="0">
							  <tr>
								<td width="50%"><s:text name="stafa.label.donorid"/></td>
								<td width="50%" id="slide_donorId" name="slide_donorId"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.name"/></td>
								<td id="slide_donorName" name="slide_donorName"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.dob"/></td>
								<td id="slide_donorDOB" name="slide_donorDOB"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.donoraborh"/></td>
								<td id="slide_donorABORH" name="slide_donorABORH"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.procedure"/></td>
								<td id="slide_procedure" name="slide_procedure"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.donation"/></td>
								<td id="slide_donation" name="slide_donation"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.weights"/></td>
								<td id="slide_donorWeight" name="slide_donorWeight"></td>
							  </tr>
							  <tr>
								<td>Height</td>
								<td id="slide_donorHeight" name="slide_donorHeight"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.targetvolume"/></td>
								<td id="slide_targetVol" name="slide_targetVol"></td>
							  </tr>
							</table>
						</div>
				</div>
			</div>
				
<!-- Patient Info-->

<!-- Lab Results start-->
				<div id="donorLabWidget" class="column">
				<div  class="portlet">
					<div class="portlet-header "><s:text name="stafa.label.donorlab"/></div>
						<div class="portlet-content">
							<table width="100%" border="0">
								<tr>
									<td width="50%"><s:text name="stafa.label.idms"/></td>
									<td width="50%" id="idms"></td>
								</tr>
								<tr>
									<td width="50%"><s:text name="stafa.label.westnile"/></td>
									<td width="50%" id="westnile"> </td>
								</tr>
								<tr>
									<td width="50%"><span class="cd34"><s:text name="stafa.label.donorcd34"/></span>
									<span class="cd3"><s:text name="stafa.label.donorcd3"/></span></td>
									<td width="50%" id="donorcd34"> </td>
								</tr>
							</table>
						</div>
				</div>
				</div>
				
<!-- Lab Results-->
			<div id="productLabWidget" class="column">
					<div class="portlet">
						<div class="portlet-header "><s:text name="stafa.label.productlab"/></div>
							<div class="portlet-content">
							<table width="100%" border="0">
								<tr>
									<td width="50%">
									<span class="cd34"><s:text name="stafa.label.cd34_day1"/></span>
									<span class="cd3"><s:text name="stafa.label.cd3_day1"/></span>
									</td>
									<td width="50%" id="cd34_day1"></td>
								</tr>
								<tr>
									<td width="50%">
									<span class="cd34"><s:text name="stafa.label.cum_CD34"/></span>
									<span class="cd3"><s:text name="stafa.label.cum_CD3"/></span>
									</td>
									<td width="50%" id="cum_CD34"></td>
								</tr>
							</table>
							</div>
					</div>
				</div>
<!-- Recipient Info start-->
				<div id="recipientInfoWidget" class="column">
					<div class="portlet">
						<div class="portlet-header "><s:text name="stafa.label.recipientinformation"/></div>
							<div class="portlet-content">
							<table width="100%" border="0">
							<tr>
								<td width="50%"><s:text name="stafa.label.recipientmrn"/></td>
								<td width="50%" id="recipient_id"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.name"/></td>
								<td id="recipient_name"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.dob"/></td>
								<td id="recipient_dob"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.aborh"/></td>
								<td id="recipient_aborh"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.diagnosis"/></td>
								<td id="recipient_diagnosis"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.weights"/></td>
								<td id="recipient_weight"></td>
							  </tr>
							  </table>
							</div>
							 
					</div>
				</div>
				
<!--Recipient Info end-->
				</div>
		</div>
	<div class="cleaner"></div>

<form action="#" id="followupForm" onsubmit="return avoidFormSubmitting();">
<input type="hidden" name="flowNextFlag" id="flowNextFlag" value=""/>
<s:hidden id="followUp" name="followUp" value="%{followUp}"/>
<s:hidden id="donor" name="donor" value="%{donor}"/>
<s:hidden id="donorProcedure" name="donorProcedure" value="%{donorProcedure}"/>
<s:hidden id="entityType" name="entityType" value="FOLLOWUP"/>
<s:hidden id='docType' name='docType' value="%{docType}"/>
<s:hidden id="refDonor" name="refDonor"/>
<s:hidden id="personId"/>
<s:hidden id="nurseHiddenID" name="nurseHiddenID" value="%{nurseId}"/>
<div id="followupVerification" class="column">		
	<div  class="portlet">
	<div class="portlet-header">Follow-Up Verification</div><br>
	<div class="portlet-content">
		<div id="barcodeScanning">
			<table cellpadding="0" cellspacing="0" class="" align="center" border="1" width="100%">
				<tr>
					<td><div class="txt_align_label" style="font-weight:bold;"><s:text name="stafa.label.donorid"/></div></td>
					<td><div align="center" style="padding:1%;"><input type="text" id="donorId" name="donorId" value=""/></div></td>
					<td style="display:none;" id="donorVerified"></td>
				</tr>
				<tr>
					<td><div class="txt_align_label" style="font-weight:bold;"><s:text name="stafa.label.nurseid"/></div></td>
					<td><div align="center" style="padding:1%;"><input type="text" id="nurseID" name="nurseID" value=""/></div></td>
					<td style="display:none;" id="nurseVerified"></td>
				</tr>
			</table>
		</div>
	</div>
	</div>
</div>

<div class="cleaner"></div>

<div id="donorCtctInfo" class="column">		
	<div  class="portlet">
	<div class="portlet-header"><s:text name="stafa.label.donorcontactinformation"/></div><br>
		<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper"><s:text name="stafa.label.donorhomephone"  /></td>
					<td class="tablewapper1"><input type="text" id="donorHomePhone" name="donorHomePhone" class="phoneMask" /></td>
					<td class="tablewapper"><s:text name="stafa.label.donorcellphone" /></td>
					<td class="tablewapper1"><input type="text" id="donorCellPhone" name="donorCellPhone" class="phoneMask"/></td>
				</tr>
				<tr>
					<td class="tablewapper"><s:text name="stafa.label.donorotherphone"/></td>
					<td class="tablewapper1"><input type="text" id="donorOtherPhone" name="donorOtherPhone" class="phoneMask" /></td>
				</tr>
			</table>
		</div>
	</div>
</div>

<div class="cleaner"></div>

<div id="nextDayColl" class="column">		
	<div  class="portlet">
	<div class="portlet-header"><s:text name="stafa.label.nextdaycollection"/></div><br>
		<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper"><s:text name="stafa.label.donorcollectedagain"/></td>
					<td class="tablewapper1"><s:radio id="collectAgain" name="collectAgain" list="#{'1':'Yes','0':'No'}" onclick="checkCollectionAgain();"/></td>
					<td class="tablewapper"><s:text name="stafa.label.daycollection"/></td>
					<%-- <td class="tablewapper1"><s:radio id="dayOfCollection" name="dayOfCollection" list="#{'1':'Yes','0':'No'}"/></td> --%>
					<td class="tablewapper1"><s:select id="dayOfCollection"  name="dayOfCollection" list="lstPlannedProcedure" listKey="protocol" listValue="protocolName"  headerKey="0" headerValue="Select"/></td>
				</tr>
			</table>
		</div>
	</div>
</div>

<div class="cleaner"></div>

<div id="mobileOrder" class="column">		
	<div  class="portlet">
	<div class="portlet-header"><s:text name="stafa.label.mobilizationorder"/></div><br>
		<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper"><s:text name="stafa.label.physicalname"/></td>
					<td class="tablewapper1"><s:select id="physicianName"  name="physicianName" list="lstDonorPhysician" listKey="pkCodelst" listValue="description"  headerKey="0" headerValue="Select"/></td>
					<td class="tablewapper"><s:text name="stafa.label.orderissuedate"/></td>
					<td class="tablewapper1"><input type="text" id="orderIssueDate" name="orderIssueDate" class="dateEntry dateclass"/></td>
				</tr>
				<tr>
					<td class="tablewapper"><s:text name="stafa.label.attachmobilization"/></td>
					<td class="tablewapper1">
						<div id="fileNameDiv" style="display:none"></div>
						<div id="fileAttachDiv"align="left">
						<input type='hidden' value='0' id='pkDocumentId' name='pkDocumentId'/>
						
						<img src='images/file_loading.gif' id='fileUploadImage' style='display:none'/>
						<div id='fileDisplayName'></div>
						<input type='hidden' class='fileName' id='fileName' name='documentFileName'/>
						<input type='hidden' class='filePathId' name='documentFile' id='filePathId'/>
						<input type='hidden' class='fileUploadContentType' name='fileUploadContentType' id='fileUploadContentType'/>
   	 					<div class="attachFile" style="cursor: pointer;	color: #0101DF;	"id="attacheFileTextDiv"></div><input class='' style="display:block" type='file' name='fileUpload' onChange='ajaxFileUpload1(this,"")' id='attach'/>
						
						<!-- <iframe height="29" src="jsp/fileUpload.jsp" style="border: medium none;" id="testFrame"></iframe>
		                	<div id="fileAttachmentDiv">
		                    	<div id="viewAttachment" style="display:none"></div>
		                	</div>
		                -->
                		</div>
                		<span><s:text name='stafa.fileupload.message'/></span>
                	</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<div class="cleaner"></div>

<div id="mobileInstruction" class="column">
	<div  class="portlet">
	<div class="portlet-header notes"><s:text name="stafa.label.mobilizationinstruction"/></div>
		<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
	            <tr>
	                <td class="questionsAlign">Mozobil Administered to Donor After Collection</td>
	                <td width="25%" height="40px"><s:radio id="donorAfterCollection" name="donorAfterCollection" list="#{'1':'Yes','0':'No'}"/></td>
	            </tr>
	            <tr>
	                <td class="questionsAlign">Donor Will Receive Mozobil</td>
	                <td width="25%" height="40px"><s:radio id="receiveMozobil" name="receiveMozobil" list="#{'1':'Yes','0':'No','2':'Pending'}"/></td>
	            </tr>
	            
	             <tr class="receivemozobil_pending">
	                <td class="questionsAlign">RN to Call MD if Haven't Received Order By<label class="mandatory">*</label></td>
	               <td width="25%" height="40px"><div class="checkboxrequired" id="rncallmd" style="display:none;">RN to Call MD is Required </div><input type="hidden" id="followReceiveOrder" name="followReceiveOrder"/><s:textfield id="tempfollowReceiveOrder" name="tempfollowReceiveOrder" Class="" onchange="followReceiveOrders();setMobilizationValidation();"/></td>
	               <%--  <td width="25%" height="40px"><s:radio id="followReceiveOrder" name="followReceiveOrder" list="#{'1':'Yes','0':'No'}"/></td> --%>
	            </tr>
	            <tr class="receivemozobil_yes">
	                <td class="questionsAlign">Donor Will Receive Mozobil Post Collection At <label class="mandatory">*</label></td>
	                <td width="25%" height="40px"><div class="checkboxrequired" id="receiveMozobilTime" style="display:none;">Donor Reiceive Mozobil is Required </div><s:hidden  id="receiveMozobilPost" name="receiveMozobilPost" Class="" /><s:textfield id="tempReceiveMozobilTime" name="tempReceiveMozobilTime" Class="" onchange="receiveMozobilTime();setMobilizationValidation();"/></td>
	                <!-- <td width="25%" height="40px"><input type="radio"/>Yes&nbsp;<input type="radio"/>No</td> -->
	            </tr>
	            <tr class="receivemozobil_yes">
	                <td class="questionsAlign">Order Has Been Scanned To Pharmacist<label class="mandatory">*</label></td>
	                <td width="25%" height="40px"><div class="checkboxrequired" id="receivemob_yes" style="display:none;">Scanned To Pharmacist is Required</div><s:radio id="scannedToPharmacist" name="scannedToPharmacist" list="#{'1':'Yes','0':'No'}" onclick="setMobilizationValidation()"/></td>
	            </tr>
	            <tr class="receivemozobil_no">
	                <td class="questionsAlign">Donor's Chart is in the BMC <label class="mandatory">*</label></td>
	                <td width="25%" height="40px"><div class="checkboxrequired" id="donor_bmc" style="display:none;">Donor Chart is Required</div><s:radio id="followChardBMC" name="followChardBMC" list="#{'1':'Yes','0':'No'}" onclick="setMobilizationValidation()"/></td>
	            </tr>
	            <tr class="receivemozobil_pending">
	                <td class="questionsAlign">RN Needs to Notify Donor of the POC Once Order Received<label class="mandatory">*</label></td>
	                <!-- <td width="25%" height="40px"><select><option>Select...</option></select></td> -->
	                <td width="25%" height="40px"><div class="checkboxrequired" id="notify_donor" style="display:none;">RN Needs to Notify Donor is Required</div><s:radio id="notifyDonor" name="notifyDonor" list="#{'1':'Yes','0':'No'}" onclick="setMobilizationValidation();"/></td>
	            </tr>
	            <tr class="">
	                <td class="questionsAlign">Donor Has Been Given Instructions Regarding POC</td>
	                <!-- <td width="25%" height="40px"><select><option>Select...</option></select></td> -->
	                <td width="25%" height="40px"><s:radio id="pocInstructionGiven" name="pocInstructionGiven" list="#{'1':'Yes','0':'No'}"/></td>
	            </tr>
		    </table>
		</div>
	</div>
</div>

<div class="cleaner"></div>

<div id="donorInstruction" class="column">
	<div  class="portlet">
	<div class="portlet-header notes"><s:text name="stafa.label.donorinstruction"/></div>
		<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Instruction Given To Donor</td>
					<td class="tablewapper1"><textarea rows="4" cols="50" name="istructionToDonor" id="istructionToDonor"></textarea></td>
	            </tr>
			</table>
		</div>
	</div>
</div>

<div class="cleaner"></div>

<%-- <div class="column">
	<div  class="portlet">
	<div class="portlet-header notes"><s:text name="stafa.label.mobilizationcompletion"/></div>
		<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="questionsAlign">Mobilization Completed At Scheduled Date & Time</td>
					<td width="25%" height="40px"><input type="radio"/>Yes&nbsp;<input type="radio"/>No</td>
	            </tr>
			</table>
		</div>
	</div>
</div>		 --%>	
</form>

<div class="cleaner"></div>

<div align="left" style="float:right;">
	<form onsubmit="return avoidFormSubmitting()">
			<table cellpadding="0" cellspacing="0" class="" align="" border="0">
			<tr>
			<td><span class='nextContainer'>Next:
			<select name="nextOption" id="nextOption" class=""></select>
			<input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
			<input type="button" value="Next" onclick="verifyeSign('eSignNext','next')"/></span></td>
			</tr>
			</table>
	</form>
</div>


<!-- Barcode Starts -->		
<div  id ="mainbarcodediv" style="background-color:white; display:none; height:655px;overflow:auto;margin-top:1%;">  <!-- added by mohan j -->
<div id="maindiv"><div id="barimageTables" style="float:right;display:none; width:44%;height:40%;">
			<!-- From Template Start -->
<div id="barcodeMainDiv" >
		</div>
			<!-- From Template Ends -->
</div></div>
	
</div>
<!-- Barcode Ends -->
		
<script>

$(function() {
    $('.jclock').jclock();
    $( ".column" ).sortable().sortable({
        connectWith: ".column"
    });
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
   



    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

    $('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});   
});
</script>
