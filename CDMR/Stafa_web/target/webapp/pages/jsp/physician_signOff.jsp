<%@ include file="common/includes.jsp" %>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />

<script>
$(document).ready(function() {
	oTable = $('#selectDonorTable').dataTable({
		"bJQueryUI": true
	});
	oTable.thDatasorting('selectDonorTable');
	$('.signoffdates').datetimepicker();
	$("select").uniform();
   
});


$(document).ready(function() {
    $('#blind').html("<<");
      var $box = $('#box').wrap('<div id="box-outer"></div>');
      $('#blind').click(function() {
        $box.blindToggle('fast');
      });
    });


jQuery.fn.blindToggle = function(speed, easing, callback) {
var h = this.width() + parseInt(this.css('paddingLeft')) +  parseInt(this.css('paddingRight'));
if(parseInt(this.css('marginLeft'))<0){
$('#forfloat_right').animate( { width: "79%" }, { queue: false, duration: 200 });	
 $('#blind').html("<<");
 return this.animate({marginLeft:0},speed, easing, callback);
 }
else{
$( "#forfloat_right" ).animate( { width: "95%" }, { queue: false, duration: 250 });
 	$('#blind').html(">>");
 	return this.animate({marginLeft:-h},speed, easing, callback);
   }
};
/* function calculateTotalDose(){
	var rowDose = document.getElementById("selectDonorTable").rows[1];
	var productsTable = document.getElementById("availableProductsTable");
	var tnc=0;
	var cd34=0;
	var cd3=0;
	for(i=1;i<productsTable.rows.length;i++){
		var row = productsTable.rows[i];
		if(productsTable.rows[i].cells[8].childNodes[0].checked){
			if(i!=5){
			tnc+=Number($.trim(row.cells[4].innerHTML));
			cd34+=Number($.trim(row.cells[5].innerHTML));
			cd3+=Number($.trim(row.cells[6].innerHTML));
			}
		}
	}
	rowDose.cells[0].innerHTML = tnc;
	rowDose.cells[1].innerHTML = cd34;
	rowDose.cells[2].innerHTML = cd3;
} */

</script>

<div class="cleaner"></div>
<!--float window-->

<div id="floatMenu">
<div id="forfloat_wind">
<div id="box-outer">
<a href="#" style="text-decoration:none" id="blind">&lt;&lt; </a>
	<div id="box">
		<div id="leftnav" >
		
<!-- recipient start-->
		<div  class="portlet">
			<div class="portlet-header notes">Recipient Info</div>
				<div class="portlet-content">
				<table width="225" border="0">
				  <tr>
					<td width="89">ID:</td>
					<td width="120">HCA789564</td>
				  </tr>
				  <tr>
					<td>Name:</td>
					<td>John Wilson</td>
				  </tr>
				  <tr>
					<td>DOB:</td>
					<td>June 23, 1964</td>
				  </tr>
				  <tr>
					<td>ABO/Rh:</td>
					<td>O</td>
				  </tr>
				   <tr>
					<td>Planned Protocol:</td>
					<td>SCT-Allo</td>
				  </tr>
				  <tr>
					<td>Diagnosis:</td>
					<td>Leukemia</td>
				  </tr>
				  <tr>
					<td>Weight:</td>
					<td>150 kg</td>
				  </tr>
</table>

			</div>
			</div>
		
<!-- recipient-->

<!-- Donor start-->
			<div class="portlet">
				<div class="portlet-header notes">Donor Info</div>
				<div class="portlet-content">
				</div>
			</div>
<!-- Donor-->
		</div>
		</div>
	
	</div>
</div>
</div>

<!--float window end-->


<div class="cleaner"></div>
<!--right window start -->	
<div id="forfloat_right">
<section>
  <div class="column">
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value=""/><s:hidden id="widgetName" value=""/><s:hidden id="" value=""/>Indication of Infusion</div>
			<div class="portlet-content">
				<form>		
					<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
						<tr>
							<td><input type="radio" class="" onclick="" name="radio1" id="">Reconstitution of Hematopoiesis</td>
							<td><input type="radio" class="" onclick="" name="radio1" id="">Immunotherapy for Treatment of Malignancy</td>
							<td><input type="radio" class="" onclick="" name="radio1" id="">Hematopoietic Support After Myelosupperssive Chemotherapy</td>
							
						</tr>
						<tr>
							<td><input type="radio" class="" onclick="" name="radio1" id="">Treatment of Graft Failure</td>
							<td><input type="radio" class="" onclick="" name="radio1" id="">Supplement infusion</td>
							<td><input type="radio" class="" onclick="" name="radio1" id="">Other Treatment,Specify <input type="text" name="" value="" id="" placeholder=""/></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>	

 <div class="column">
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value=""/><s:hidden id="widgetName" value=""/><s:hidden id="" value=""/>Order Sign-off</div>
			<div class="portlet-content">
				<form>		
					<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
						<tr>
							<td><div class="txt_align_label" align="left">Ordering Physician</div></td>
							<td><input type="text" name="" value="M.Anderson" id="" placeholder=""/></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>	
</div>	
		
 <div class="cleaner"></div>
 
</section>	

<div class="cleaner"></div>

<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
		<table cellpadding="0" cellspacing="0" class="" border="0">
                <tr>
                <td>Next:</td>
                <td><select name="" id="" ><option>Select</option></select></td>
                <td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
                <td><input type="button"  value="Save"/></td>
                </tr>
        </table>
		</form>
		</div>
		<div class="cleaner"></div>

<script>
$(function() {	
	
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open');
	});
	
	

	/**for Next Button Start**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".next" )
	.append("<span style=\"float:right;\" class='ui-next'></span>");
	
	$( ".portlet-header .ui-next " ).click(function() {
		loadPage('jsp/physician_product1.jsp');	
	});
	/**for Next Button End**/
	
	/**for Previous Button Start**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".previous" )
	.append("<span style=\"float:right;\" class='ui-previous'></span>");
	
	$( ".portlet-header .ui-previous " ).click(function() {
		loadPage('jsp/physician_dose.jsp');	
	});
	/**for Previous Button End**/
	
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");
	

	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});

</script>

<div id="sterility" title="sterility" style="display: none">
		<b>Proceed ?</b>&nbsp;<input type="checkbox" value=""/>Y&nbsp;<input type="checkbox" value=""/>N
    </div>