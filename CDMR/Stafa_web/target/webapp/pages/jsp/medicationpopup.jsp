<%@ include file="common/includes.jsp" %>
<script>

var donor,donorProcedure,donorMRN,nurseVal;
function savePopup(){
		try{
			var currentDonorid=$("#medi_donorid").val();
			var currentNurseid=$("#medi_nurseid").val();
			if((donorMRN==currentDonorid)&&(nurseVal==currentNurseid)){
				if(saveMedication()){
				}
			return true;
			}else{
				alert("Please Verify Donor MRN/Nurse ID");
			}
		}catch(e){
			alert("exception " + e);
		}
	}
$(document).ready(function(){
	donor= $("#donor").val();
	donorProcedure = $("#donorProcedure").val();
	donorMRN=$("#donormrn").val();
	nurseVal=$("#nurseId").val();
	var medicateVal = $("#medication").val();
	$("#medicationtabblediv").hide();
	if(medicateVal>0){
		$("#medi_donorid").val(donorMRN);
		checkdonor();
		$("#medi_nurseid").val(nurseVal);
		checknurse();
		$("#medicationtabblediv").show();
		constructMedicationTable(true,donor,donorProcedure);
	}else{
		constructMedicationTable(false,donor,donorProcedure);
	}
});
function closeMedication(){
	$("#modalpopup").dialog('close');
	
}
var dateId=0;
function addmedicationRows(){
	
	var newRow="<tr><td></td>"+
	"<td><input type='checkbox' id='medicationrow' name='' class='checkalls'/><input type='hidden' id='medications' name='medication' value='0'/></td>"+
	"<td><input type='text'name='medicationName' id='medicationName'value='' /></td>"+
	"<td><input type='text' name='medicationLot' id='medicationLot' class=''/></td>"+
	"<td><input type='text' name='expiryDate' id='expiryDate"+dateId+"' class=' expiryDate dateEntry dateclass' /></td>"+
	"<td><input type='text' name='medicationDosage' id='medicationDosage' class=''/></td>"+
	"<td><input type='text' name='administrationDate' id='administrationDate"+dateId+"' class=' adminDate dateEntry dateclass' /></td>"+
	"<td><input type='hidden' name='administrationTime' id='administrationTime' class='admintime'/><input type='text' id='tempAdministrationTime"+dateId+"' name='tempAdministrationTime' Class='tempAdministrationTime' onchange='setAdministrationTime();'/></td>"+
	"<td><input type='text' name='medicationComments' id='medicationComments' class=''/></td>"+
	"</tr>";
	$("#medicationTable tbody").prepend(newRow); 
	dateId=dateId+1;
	$("#medicationTable .ColVis_MasterButton").triggerHandler("click");
	$('.ColVis_Restore:visible').triggerHandler("click");
	$("div.ColVis_collectionBackground").trigger("click");
	  $( ".dateclass" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	        changeYear: true, yearRange: "c-50:c+45"});
	  $(".tempAdministrationTime").timepicker({});
	  $("select").uniform();
	  var param ="{module:'APHERESIS',page:'MEDICATION'}";
		markValidation(param);
}
function setAdministrationTime(){
    $('#administrationTime').val($('.adminDate').val() +" "+ $('input[name="tempAdministrationTime"]').val());
}
function saveMedication(){
	var rowData = "";
	 $("#medicationTable").find("#medicationName").each(function (i,row){
		 rowHtml =  $(this).closest("tr");	 
		 
		 medicationname = "";
		 medicationlot="";
		 expdate="";
		 dosage="";
		 admdate="";
		 admtime="";
		 medcomments="";
	 		medicationname=$(rowHtml).find("#medicationName").val();
	 		medicationid=$(rowHtml).find("#medications").val();
	 		medicationlot=$(rowHtml).find("#medicationLot").val();
	 		expdate=$(rowHtml).find(".expiryDate").val();
	 		dosage=$(rowHtml).find("#medicationDosage").val();
	 		admdate=$(rowHtml).find(".adminDate").val();
	 		admtime=$(rowHtml).find("#administrationTime").val();
	 		medcomments=$(rowHtml).find("#medicationComments").val();
		 	  	rowData+= "{medication:"+medicationid+",medicationName:'"+medicationname+"',medicationLot:'"+medicationlot+"',expiryDate:'"+expdate+"',medicationDosage:'"+dosage+"',administrationDate:'"+admdate+"',administrationTime:'"+admtime+"',medicationComments:'"+medcomments+"',donor:'"+donor+"',donorProcedure:'"+donorProcedure+"'},";
	 		
	 });
	 if(rowData.length >0){
            rowData = rowData.substring(0,(rowData.length-1));
        }
	 url = "saveMedication";
	 var jsonData = "jsonData={medicationData:[" + rowData + "]}";


$('.progress-indicator').css('display', 'block');

response = jsonDataCall(url, jsonData);

$('.progress-indicator').css('display', 'none');

stafaAlert('Data saved Successfully','');
constructMedicationTable(true,donor,donorProcedure);
return true;
}

function deletemedicationData(){
    var yes=confirm(confirm_deleteMsg);
	if(!yes){
		return;
	}
	var deletedIds = ""
	$("#medicationTable #medicationrow:checked").each(function (row){
		deletedIds += $(this).val() + ",";
	});
	if(deletedIds.length >0){
	  deletedIds = deletedIds.substr(0,deletedIds.length-1);
	}
	jsonData = "{deletedMedicationResult:'"+deletedIds+"'}";
	url="deleteMedicationData";
    response = jsonDataCall(url,"jsonData="+jsonData);
    constructMedicationTable(true,donor,donorProcedure);
}
function constructMedicationTable(flag,donor,donorProcedure){
	var criteria = "and fk_donor="+donor+"and fk_plannedprocedure="+donorProcedure;
	var medication_ColManager = function(){
										 	$("#medicateColumn").html($('#medicationTable_wrapper .ColVis'));
										 };
	var medication_aoColumn = [    { "bSortable":false},
		     		               { "bSortable":false},
					               null,
					               null,
					               null,
					               null,
					               null,
					               null,
					               null
				             ];	
	var medicationServerParams = function ( aoData ) {
															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "popup_medication"});
															aoData.push( { "name": "criteria", "value": criteria});
															
															aoData.push( {"name": "col_1_name", "value": "" } );
															aoData.push( { "name": "col_1_column", "value": ""});
															
															aoData.push( { "name": "col_2_name", "value": "lower(medicationName)"});
															aoData.push( { "name": "col_2_column", "value": "lower(medicationName)"});
															
															aoData.push( { "name": "col_3_name", "value": "lower(medicationLot)"});
															aoData.push( { "name": "col_3_column", "value": "lower(medicationLot)"});
															
															aoData.push( { "name": "col_4_name", "value": "lower(expiryDate)"});
															aoData.push( { "name": "col_4_column", "value": "lower(expiryDate)"});
															
															aoData.push( { "name": "col_5_name", "value": "lower(medicationDosage)"});
															aoData.push( { "name": "col_5_column", "value": "lower(medicationDosage)"});
															
															aoData.push( { "name": "col_6_name", "value": "lower(administrationDate)"});
															aoData.push( { "name": "col_6_column", "value": "lower(administrationDate)"});

															aoData.push( { "name": "col_7_name", "value": "lower(to_char(administrationTime,'Mon dd, yyyy  HH:MM'))"});
															aoData.push( { "name": "col_7_column", "value": "lower(administrationTime)"});
															
															aoData.push( { "name": "col_8_name", "value": "lower(medicationComments)"});
															aoData.push( { "name": "col_8_column", "value": "lower(medicationComments)"});
														};
	var medication_aoColumnDef = [
		                         {		"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
		                             		return "";
		                             }},
		                          {	"sTitle":'Check All',
		                            	"aTargets": [ 1], "mDataProp": function ( source, type, val ) {
		                             		return "<input name='medicationrow' id= 'medicationrow' type='checkbox' value='"+source.medication+"' />";
		                            }},
		                          {	"sTitle":'Name',
		                            	"aTargets": [ 2], "mDataProp": function ( source, type, val ) {
		                                    return source.medicationName;
		                           }},
		                          {	"sTitle":'Lot#',
		                            	"aTargets": [ 3], "mDataProp": function ( source, type, val ) {
		                                    return source.medicationLot;
		                           }},
		                          {	"sTitle":'Exp.Date',
		                            	"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
		                            		if(source.expiryDate!=null){
		                            			var expireDate=converHDate(source.expiryDate);
		                    	    			//var expireDate = ($.datepicker.formatDate('M dd, yy', new Date(source.expiryDate)));
		                    	    	        	}else{
		                    	    	        		expireDate = "";
		                    	    	    }
		                                    return expireDate;
		                           }},
		                          {	"sTitle":'Dosage',
		                            	"aTargets": [ 5], "mDataProp": function ( source, type, val ) {
		                                    return source.medicationDosage;
		                           }},
		                          {	"sTitle":'Date of Administration',
		                            	"aTargets": [ 6], "mDataProp": function ( source, type, val ) {
		                            		if(source.administrationDate!=null){
		                            			var administorDate=converHDate(source.administrationDate);
		                    	    			//var administorDate = ($.datepicker.formatDate('M dd, yy', new Date(source.administrationDate)));
		                    	    	        	}else{
		                    	    	        		administorDate = "";
		                    	    	    }
		                                    return administorDate;
		                           }},
		                         
		                          {	"sTitle":'Time of Administration',
		                            	"aTargets": [ 7], "mDataProp": function ( source, type, val ) {
		                            		if(source.administrationTime!=null){
		                        				//
		                        				if($.browser.msie){
		                    						var tmpDate1 = source.administrationTime.split("-");
		                    						//alert("tmpDate " + tmpDate1 +" / len----- " + tmpDate1.length + " , " + tmpDate1[2].substring(0,2) + " / " + tmpDate1[2].substring(3));
		                    						var administorTime = tmpDate1[2].substring(3,8);
		                    						// tmpDate = new Date(tmpDate1[0] ,tmpDate1[1], tmpDate1[2].substring(0,2) , tmpTime.substring(0,2) , );
		                    					}else{
		                    						var tmpDate = new Date(source.administrationTime);
			                        				var administorTime = (tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes();

		                    					}
		                        			}else{
		                        				administorTime = "";
		                        			}
		                                    return administorTime;
		                           }},
		                            
		                          {	"sTitle":'Comment',
		                            	"aTargets": [ 8], "mDataProp": function ( source, type, val ) {
		                                    return source.medicationComments;
		                           }}
		                           
		                        ];
			constructTable(flag,'medicationTable',medication_ColManager,medicationServerParams,medication_aoColumnDef,medication_aoColumn);
	}
</script>

<div id="addnewMedication">

<form id="medicationForm">
<s:hidden id="donor" name="donor" value="%{donor}"/>
<s:hidden id="donorProcedure" name="donorProcedure" value="%{donorProcedure}"/>
<s:hidden id="donormrn" name="donormrn" value="%{donormrn}"/>
<s:hidden id="medication" name="medication" value="%{medication}"/>
<s:hidden id="nurseId" name="nurseId" value="%{nurseId}"/>
<div><b>Barcode Scanning</b></div>
 	<table cellpadding="0" cellspacing="0" class="" align="center" id="medicateBarcode" border="1" width="100%">
        <tr>
	         <td class="tablewapper">Donor MRN</td>
	         <td class="tablewapper1"><input type="text" id="medi_donorid" name="medi_donorid" placeholder="Scan/Enter Patient Id" /></td>
	         <td class="tablewapper" id="donormrnVerified" style="display:none"></td>
        </tr>
        <tr>
             <td class="tablewapper">Nurse Id</td>
             <td class="tablewapper1"><input type="text" id="medi_nurseid" name="medi_nurseid" placeholder="Scan/Enter Patient Id"/></td>
             <td class="tablewapper" id="nursemrnVerified" style="display:none"></td>
        </tr>
	</table> 
 	<div style="height:20px;width:20px;">
		<div class="cleaner"></div>
		<div>&nbsp;</div>
	</div>
	<br>
	<br>
  
<div  class="portlet" id="medicationtabblediv" style="display:none;">
	<div style="height:35px;width:100%;background-color:F0F0F0;"><span style="padding:10px;line-height:2;"><b>Medication</b></span></div>
  		<div class="portlet-content" style="overflow-x:auto;overflow-y:hidden;">
	<%--<span class='hidden'><s:select id="medicationname" style='display:none;' name="medicationname" list="medicationNameList" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></span> --%>
			<s:hidden id="" name="" value=""/>
	  		<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addmedicationRows();"/>&nbsp;&nbsp;<label  class="cursor" onclick="addmedicationRows();"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deletemedicationData();"/>&nbsp;&nbsp;<label class="cursor"  onclick="deletemedicationData();"><b>Delete</b></label>&nbsp;</div></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" id="medicationTable" class="display">
		 		<thead>
		 			<tr>
		 				<th width="4%" id="medicateColumn"></th>
		 				<th>Check All<input type='checkbox'/></th>
		 				<th>Name</th>
		 				<th>Lot #</th>
		 				<th>Exp. Date</th>
		 				<th>Dosage</th>
		 				<th>Date of Administration</th>
		 				<th>Time of Administration</th>
		 				<th>Comment</th>
		 			</tr>
		 		</thead>
		 		<tbody>
			       
		        </tbody>
			</table>
		</div>
</div>

<div>&nbsp;</div>
<div class="cleaner"></div>
<div>&nbsp;</div>
</form>

<div align="right" style="float:right;">
	<form onsubmit="return avoidFormSubmitting()">
		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
			<tr>
				<td><input type="password" style="width:80px;" size="5" value="" id="eSignMedication" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;</td>				
				<td><input type="button" value="Save" onclick="verifyeSign('eSignMedication','popup')"/></td>
				<td><input type="button" value="Cancel" onclick="closeMedication()"/></td>
			</tr>
		</table>
	</form>
</div>
  
</div>

<script>


$(function() {
   
   /*  $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
  
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

    $('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});    */
});



</script>