	<%@ include file="common/includes.jsp" %>
<!-- <script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script> -->
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script type="text/javascript" src="js/placeholder.js"></script>
<script type="text/javascript" src="js/jquery/jquery.Print.js"></script>
<script type="text/javascript" src="js/jquery/jquery.printElement.js"></script>
<script type="text/javascript" src="js/barcodeScanBox.js"></script>
<script>
function setplasmaid(){
	
	//alert("allo"+$("#collectPlasma").val());
	if($("#collectPlasma").val()==1){
		//alert("allo"+$("#collectPlasma").val());
		$("#plasmaInfo").find( ".portlet-header .ui-icon" ).removeClass( "ui-icon-plusthick" ).addClass( "ui-icon-minusthick" ).end().find(".portlet-content").show();
		checkPlasma();
	}else{
		checkPlasma();
		$("#plasmaInfo").find( ".portlet-header .ui-icon" ).removeClass( "ui-icon-minusthick" ).addClass( "ui-icon-plusthick" ).end().find(".portlet-content").hide();
		$("#plasmaId").val("");
		$("#plasmaCollectionDate").val("");
		$("#plasmaLabelVerified1").attr("checked",false);
		$("#plasmaLabelVerified0").attr("checked",false);
		$("#specPlasmaAppearanceOk1").attr("checked",false);
		$("#specPlasmaAppearanceOk0").attr("checked",false);
		
		$("#proplasmaVolume").val("");
		$("#tempPlasmaCollStartTime").val("");
		
		//alert("111");
		
	} 
	
}
function checkPlasma(){
	if($("#collectPlasma").val()==1){
		setValidation(true);
	}else{
		setValidation(false);
	}
}
function setValidation(flag){
	if(!flag){
		$("#plasmaId").removeClass("validate_required");
		$("#plasmaId").parent().find("sup").remove();
		$("#specPlasmaAppearanceOk1").removeClass("validate_required");
		$("#specPlasmaAppearanceOk1").parent().find("sup").remove();
		$("#specPlasmaAppearanceOk0").removeClass("validate_required");
		$("#specPlasmaAppearanceOk0").parent().find("sup").remove();
		$("#plasmaVerify").removeClass("validate_required");
		$("#plasmaVerify").parent().find("sup").remove();
		$("#plasmaInfo").find("span").removeClass( "ui-icon ui-icon-minusthick" ).addClass("ui-icon ui-icon-plusthick").end().find(".portlet-content").hide();
		}else{
			 $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" ).find("span").removeClass("ui-icon-plusthick" ).addClass( "ui-icon-minusthick" );
			 $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" ).end().find(".portlet-content").show();
		}
}


function checkCVC(){
	var CVCflag=false;
	if($("#cvcdry").is(':checked')){
		$("#cvcrequired").hide();
		CVCflag=true;
	}else if($("#cvcclear").is(':checked')){
		$("#cvcrequired").hide();
		CVCflag=true;
	}else if($("#cvcintact").is(':checked')){
		$("#cvcrequired").hide();
		CVCflag=true;
	}else if($("#cvcsoiled").is(':checked')){
		$("#cvcrequired").hide();
		CVCflag=true;
	}else if($("#cvcna").is(':checked')){
		$("#cvcrequired").hide();
		CVCflag=true;
	}
	else {
		CVCflag=false
		$("#cvcrequired").show();
		$("#cvcdry").focus();
	}
	
	return CVCflag;
}

function checkDonorStatus(){
	var CVCflag=false;
	if($("#donornone").is(':checked')){
		$("#donorrequired").hide();
		CVCflag=true;
	}else if($("#donorpain").is(':checked')){
		$("#donorrequired").hide();
		CVCflag=true;
	}else if($("#donornause").is(':checked')){
		$("#donorrequired").hide();
		CVCflag=true;
	}else if($("#donordiarrhea").is(':checked')){
		$("#donorrequired").hide();
		CVCflag=true;
	}else if($("#donorfebrile").is(':checked')){
		$("#donorrequired").hide();
		CVCflag=true;
	}else {
		CVCflag=false
		$("#donorrequired").show();
		$("#donornone").focus();
	}
	return CVCflag;
}
function savePages(mode){
		if(checkModuleRights(STAFA_POSTCOLLECTION,APPMODE_SAVE)){
		try{
			
			if($("#scanFlag").val()=="true"){
			 
				if(savePostCollection(mode)){
					//alert("wait");
					return true;
				}
				
			}else{
				alert("Please Scan/Enter Product ID");
			}
		}catch(e){
				alert("exception " + e);
				}
		}
}

function getslide_widgetdetails(){
	
	 var donor=$("#donor").val();
	 var donorProcedure = $("#donorProcedure").val();
	 url="fetchDonorData";
	 var jsonData = "donor:"+donor+",donorProcedure:"+donorProcedure;
	response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	var donorDetails = response.donorDetails;
	$("#slide_donorId").text(donorDetails.personMRN);
	$("#slide_donorDOB").text(donorDetails.dob);
	$("#slide_donorName").text(donorDetails.lastName + " " + donorDetails.firstName );
	$("#slide_procedure").text(donorDetails.plannedProceduredesc);
	var tcCheck = donorDetails.plannedProceduredesc.slice(-2);
 	if(tcCheck == "TC"){
 		$(".cd34").hide();
 		
 	}else{
 		$(".cd3").hide();
 	}
	$("#slide_donorWeight").text(donorDetails.personWeight);
	$("#slide_donorABORH").text(donorDetails.bloodGrpName);
	$("#slide_targetVol").text(donorDetails.targetProcessingVolume);
	$("#slide_donation").text(donorDetails.donorTypeName);
	$("#recipient_id").text(donorDetails.recipient_id);
	$("#recipient_name").text(donorDetails.recipient_name);
	$("#recipient_dob").text(donorDetails.recipient_dob);
	$("#recipient_aborh").text(donorDetails.recipient_bloodGroup);
	$("#recipient_diagnosis").text(donorDetails.diagonsis);
	$("#idms").text(donorDetails.idmsResult);
	var idmCheck = donorDetails.idmsResult;
	if(idmCheck=="Reactive"){
		$("#idms").addClass('idmReactive');
	}else if(idmCheck=="Pending"){
		$("#idms").addClass('idmPending');
	}
	$("#westnile").text(donorDetails.westNileDesc);
	$("#donorcd34").text(donorDetails.donorCD34);
	$("#cd34_day1").text(donorDetails.productCD34);
	$("#cum_CD34").text(donorDetails.productCUMCD34);
	$("#recipient_weight").text(donorDetails.recipient_weight);
	$("#slide_donorHeight").text(donorDetails.personHeight);
	//$("#productId").val(donorDetails.productId);
}
function setScanFlag(type){
	$("#scanType").val(type);
}
function getScanData(event,obj){
    if(event.which==13){
    	var scanType = $("#scanType").val();
    	event.which=13;
      	getdata(event,scanType);
      	$("#scanType").val("");
  }
}
function getdata(e,type){
	if(type=="PLASMA"){
		loadPlasma(e);
	}else if(type=="PRODUCT"){
		loadPostCollectionData(e);
	}
}
function loadPlasma(e){
	var searchUrl = "checkPlasmaData";
	var donor=$("#donor").val();
	var donorProcedure = $("#donorProcedure").val();
	var plasmaSearch =  $("#divPlasmaScan #fake_conformProductSearch").val();
	
	if((e.which==13 && plasmaSearch!="")){
		var jsonData = "plasmaId:"+plasmaSearch+",donorProcedure:"+donorProcedure+",donor:"+donor;
		var result = jsonDataCall(searchUrl,"jsonData={"+jsonData+"}");
		var listLength=0;
		if(result!=null && result.plasmaList.length>0){
			listLength=result.plasmaList.length;
		}
		if(listLength!=null && listLength>0){
			$(result.plasmaList).each(function(i,v){
				for(n=0;n<v.length;n++){
					$("#proplasmaVolume").val(v[0]);
					$("#plasmaId").val(v[1]);
					$("#plasmaLabelVerified").val(v[2]);
					if(v[3]!=null){
						var plasmaCollectionDate = converHDate(v[3]);
						$("#plasmaCollectionDate").val(plasmaCollectionDate);
					}else{
						$("#plasmaCollectionDate").val("");
					}
					if(v[4]!=null){
						$("#tempPlasmaCollStartTime").val(v[4]);
					}else{
						$("#tempPlasmaCollStartTime").val("");
					}
					setPlasmaCollStartTime();
					if(v[5]!=null && v[5]==1){
			 			$("#specPlasmaAppearanceOk1").trigger("click");
			 			$("#specPlasmaAppearanceOk1").addClass("on");
			 		}else if(v[5]!=null && v[5]==0){
			 			$("#specPlasmaAppearanceOk0").trigger("click");
			 			$("#specPlasmaAppearanceOk0").addClass("on");
			 		}
				}
			});
			$("#scanFlag").val(true);
		}else{
			$("#plasmaInformation").find(":input").each(function(){
		    	 switch(this.type) {
   	 	            case 'text':
   	 	            case 'textarea':
   	 	                $(this).val('');
   	 	                break;
   	 	            case 'checkbox':
   	 	            case 'radio':
   	 	                this.checked = false;
   	 	                break;
   	 	        }
		    });
			$("#scanFlag").val(false);
		}
	}
}
function loadPostCollectionData(e){
	//alert(" loadPostCollectionData " + e.which);
	var collStartTime;
	var collEndTime;
	var plasmaStartTime;
	var url = "loadPostCollectionData";
	var donor=$("#donor").val();
	var donorProcedure = $("#donorProcedure").val();
	var proSearch = $("#divProductScan #fake_conformProductSearch").val();
	//alert("proSearch " + proSearch);
	if((e.which==13 && proSearch!="")){
		var jsonData = "proId:"+proSearch+",donorProcedure:"+donorProcedure+",donor:"+donor;
		response = jsonDataCall(url,"jsonData={"+jsonData+"}");
		var listLength=0;
		if(response!=null && response.postCollectionList.length>0){
			listLength=response.postCollectionList.length;
		}
		if(listLength!=null && listLength>0){
			$(response.postCollectionList).each(function(i,v){
				
					if(v[0]!=null){
						
						var collectionDate = converHDate(v[0]);
						$("#collectionDate").val(collectionDate);
					}else{
						$("#collectionDate").val("");
					}
					
					if(v[1]!=null){
						$("#tempCollectionStartTime").val(v[1]);
					}else{
						$("#tempCollectionStartTime").val("");
					}
					setCollectionStartTime();
					
					if(v[2]!=null){
						$("#tempCollectionEndTime").val(v[2]);
					}else{
						$("#tempCollectionEndTime").val("");
					}
					setCollectionEndTime();
					
					
					$("#midCountWbc").val(v[5]);
					/* if(v[7]!=null && v[7]==1){
						$("#plasmaVerify").val("Yes");
						$("#spanPlasmaLink").removeClass("hidden");
						$("#spanPlasma").addClass("hidden");
					} */
					if(v[7]!=null && v[7]==-1){
						$("#plasmaVerify").val("N/A");
					}
					/* else{
						$("#plasmaVerify").val("No");
					} */
					
					if(v[11]!=null){
						$("#aphCollection").val(v[11]);

					}else{
						$("#aphCollection").val("0");
					}
					$("#specimen").val(v[10]);
					$("#productId").val(v[12]);
					$("#specProductBagTarred").val(v[14]);
					if($("#specProductBagTarred").val()==1){
						$("#specProductBagTarred").prop("checked",true);
					}else{
						$("#specProductBagTarred").prop("checked",false);
					}
					$("#specProductWeightNurse1").val(v[15]);
					//$("#specWeightVerifiedNurse2").val(v[16]);
					if(v[17]!=null && v[17]==1){
			 			$("#specAnticoagulantsAdditives1").trigger("click");
			 			$("#specAnticoagulantsAdditives1").addClass("on");
			 			$("#anticogulants_view").show();
			 		}else if(v[17]!=null && v[17]==0){
			 			$("#specAnticoagulantsAdditives0").trigger("click");
			 			$("#specAnticoagulantsAdditives0").addClass("on");
			 			$("#anticogulants_view").hide();
			 		}
					if(v[18]!=null && v[18]==1){
			 			$("#specProductAppearanceOk1").trigger("click");
			 			$("#specProductAppearanceOk1").addClass("on");
			 		}else if(v[18]!=null && v[18]==0){
			 			$("#specProductAppearanceOk0").trigger("click");
			 			$("#specProductAppearanceOk0").addClass("on");
			 		}
					if(v[19]!=null && v[19]==1){
			 			$("#specQuarantineProduct1").trigger("click");
			 			$("#specQuarantineProductk1").addClass("on");
			 		}else if(v[19]!=null && v[19]==0){
			 			$("#specQuarantineProduct0").trigger("click");
			 			$("#specQuarantineProduct0").addClass("on");
			 		}
					//$("#specType").val(v[21]);
					$("#productType").val(v[21]);
					
					/* if(v[22]!=null && v[22]==1){
						//$("#specProductLVN2").val("Yes");
						$("#spanProductLink").removeClass("hidden");
						$("#spanProduct").addClass("hidden");
					}*/
					
					/* if(v[22]!=null && v[22]==-1){
						$("#specProductLVN2").val("N/A");
					}
					else{
						//$("#specProductLVN2").val("No");
					} */
				
			});
			$(response.postCollDataList).each(function(i,v){
				$("#postCollLabTest").val(this.postCollLabTest);
				$("#productLabelVerifiedNurse2").val(this.productLabelVerifiedNurse2);
				$("#weightVerifiedNurse2").val(this.weightVerifiedNurse2);
				$("#plasmaLabelVerifiedNurse2").val(this.plasmaLabelVerifiedNurse2);
				$("#postCollection").val(this.postCollection);
				$("#scppRefNumber").val(this.scppRefNumber);
				$("#cd3RefNumber").val(this.cd3RefNumber);
				$("#productScaleVolume").val(this.productScaleVolume);
				$("#scpp").val(this.scpp);
				if($("#scpp").val()==1){
						$("#scpp").prop("checked",true);
					}else{
						$("#scpp").prop("checked",false);
					}
				$("#cd3").val(this.cd3);
					if($("#cd3").val()==1){
						$("#cd3").prop("checked",true);
					}else{
						$("#cd3").prop("checked",false);
					}
				$("#biohazardWasteDisposed").val(this.biohazardWasteDisposed);
					if($("#biohazardWasteDisposed").val()==1){
						$("#biohazardWasteDisposed").prop("checked",true);
					}else{
						$("#biohazardWasteDisposed").prop("checked",false);
					}
				$("#equipmentCleaning").val(this.equipmentCleaning);
					if($("#equipmentCleaning").val()==1){
						$("#equipmentCleaning").prop("checked",true);
					}else{
						$("#equipmentCleaning").prop("checked",false);
					}
			});
			$("#scanFlag").val(true);
		}else{
			$("#productInformation").find(":input").each(function(){
		    	 switch(this.type) {
  	 	            case 'text':
  	 	            case 'textarea':
  	 	                $(this).val('');
  	 	                break;
  	 	            case 'checkbox':
  	 	            case 'radio':
  	 	                this.checked = false;
  	 	                break;
  	 	        }
		    });
			$("#scanFlag").val(false);
		}
	}
	 $.uniform.restore('select');
	 $('select').uniform();
}
function loadProductQcData(){
	var qcName = [];
	var pkCodeList = [];
	var subTyp = [];
	var donorProcedure = $("#donorProcedure").val();
	var url = "loadProductQcData";
	var jsonData = "donorProcedure:"+donorProcedure;
	response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	$(response.exisProductQclst).each(function(i,v){
		pkCodeList.push(this.pkCodelst);
		qcName.push(this.description);
		subTyp.push(this.subType);
	});
	var tab="<tr>";
	for(s=0;s<response.exisProductQclst.length;s++){
				tab+="<td class=\"tablewapper\">";
				tab+="<input type=\"hidden\" name='P"+subTyp[s]+"_id' id='P"+subTyp[s]+"_id' value=\"0\"><input type=\"hidden\" name=\"questionsCode\" id=\"questionsCode\" value='P"+subTyp[s]+"'/>";
				tab+="<input type=\"checkbox\" name='P"+subTyp[s]+"' id='P"+subTyp[s]+"' checked=\"checked\" value='1'/>&nbsp;"+qcName[s]+"</td>";
				
				tab+="<td class=\"tablewapper1\"><input type=\"hidden\" name='P"+subTyp[s]+"ref_id' id='P"+subTyp[s]+"ref_id' value=\"0\">";
				tab+="<input type=\"hidden\" name=\"questionsCode\" id=\"questionsCode\" value='P"+subTyp[s]+"ref'/>";
				tab+="Ref #:&nbsp;<input type='text' id='P"+subTyp[s]+"ref' name='P"+subTyp[s]+"ref'/></td></tr>";
		 };
	tab+="<td class=\"tablewapper\"><img onclick=\"addLab();\" style=\"width:16;height:16;cursor:pointer;\" src=\"images/icons/addnew.jpg\">&nbsp;&nbsp;Add Lab Test</td>";
	tab+="</tr>";
	$("#productQCTable").html(tab);
}
/* function loadProductQcData(){
	var qcName = [];
	var pkCodeList = [];
	var subTyp = [];
	var donorProcedure = $("#donorProcedure").val();
	var url = "loadProductQcData";
	var jsonData = "donorProcedure:"+donorProcedure;
	response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	$(response.exisProductQclst).each(function(i,v){
		pkCodeList.push(this.pkCodelst);
		qcName.push(this.description);
		subTyp.push(this.subType);
	});
	var tab="<tr>";
	for(s=0;s<response.exisProductQclst.length;s++){
				tab+="<td class=\"tablewapper\">";
				tab+="<input type=\"hidden\" name='P"+subTyp[s]+"_id' id='P"+subTyp[s]+"_id' value=\"0\"><input type=\"hidden\" name=\"questionsCode\" id=\"questionsCode\" value='P"+subTyp[s]+"'/>";
				tab+="<input type=\"checkbox\" name='P"+subTyp[s]+"' id='P"+subTyp[s]+"' checked=\"checked\" value='1'/>&nbsp;"+qcName[s]+"</td>";
				
				tab+="<td class=\"tablewapper1\"><input type=\"hidden\" name='P"+subTyp[s]+"ref_id' id='P"+subTyp[s]+"ref_id' value=\"0\">";
				tab+="<input type=\"hidden\" name=\"questionsCode\" id=\"questionsCode\" value='P"+subTyp[s]+"ref'/>";
				tab+="Ref #:&nbsp;<input type='text' id='P"+subTyp[s]+"ref' name='P"+subTyp[s]+"ref'/></td></tr>";
		 };
	tab+="<td class=\"tablewapper\"><img onclick=\"addLab();\" style=\"width:16;height:16;cursor:pointer;\" src=\"images/icons/addnew.jpg\">&nbsp;&nbsp;Add Lab Test</td>";
	tab+="</tr>";
	$("#productQCTable").html(tab);
} */

var donormrn;
var donorplannedprocedure;
var donorValue;
$(document).ready(function(){
	$(".titleText").html("<s:text name='stafa.label.postCollection'/>");
	var donorVal=$("#donor").val();
	donorValue = $("#donor").val();
	var donorProcedure=$("#donorProcedure").val();
	donorplannedprocedure=$("#donorProcedure").val();
	showNotesCount(donorVal,"donor",0);
	var entityId = donorProcedure +"," + donorVal;
	showTracker(entityId,"donor","apheresis_nurse","false");
	show_innernorth();
	show_slidewidgets("nurse_slidewidget",false);
	show_slidecontrol("slidecontrol");
	$('#inner_center').scrollTop(0);
	getslide_widgetdetails();
	loadPostData();
	//loadPostCollectionData();
	loadProductQcData();
	displayLabTest(subTypVal,nextTD);
	loadquestions();
	donormrn="";
	donormrn=$("#slide_donorId").text();
	$('.jclock').jclock();
	$("#tempCollectionStartTime").timepicker({});
	$("#tempCollectionEndTime").timepicker({});
	$("#tempPlasmaCollStartTime").timepicker({});
	$("#collectionDate").datepicker({
								dateFormat: 'M dd, yy',
								changeMonth: true,
								changeYear: true, 
								yearRange: "c-50:c+45"
	 });
	$("#plasmaCollectionDate").datepicker({
								dateFormat: 'M dd, yy',
								changeMonth: true,
								changeYear: true, 
								yearRange: "c-50:c+45"
	 });
    var oTable=$("#parameterTable").dataTable({
        "sDom": 'C<"clear">Rlfrtip',
        "aoColumns": [ { "bSortable":false},
                           null,
                           null,
                           null,
                           null,
                           null,
                           null,
                           null,
                           null,
                           null
                          ],
            "sScrollX": "100%",        
            "oColVis": {
                "aiExclude": [ 0 ],
                "buttonText": "&nbsp;",
                "bRestore": true,
                "sAlign": "left"
            },
            "fnInitComplete": function () {
                $("#parameterColumn").html($('.ColVis:eq(0)'));
            }
    });
    //constructAnticoagulantsTable(false,donor,donorProcedure);
	//constructEquipmentTable(false,donorVal);
	//constructReagentsTable(false,donorVal); 
	//If page already saved then fetch all info
	var param ="{module:'APHERESIS',page:'POSTCOLLECTION'}";
	markValidation(param);
	 $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	 setplasmaid();
	$("#specProductWeightNurse1").bind("keyup",function(){
		var productscale=($(this).val()/1.058);
		$("#productScaleVolume").val(productscale.toFixed(3));
	});
	 $.uniform.restore('select');
     $("select").uniform();
});

function savePopup(){
	if(saveAnticoagulants()){
		return true;
	}
}

function saveReagentEquiments(){
	var donorVal=$("#donor").val();
	var donorProcedure=$("#donorProcedure").val();
	var rowData = "";
	var quantity;
	var preparationregents;
	 var reagentSupplies;
	 $("#reagenTable").find("#reagentsname").each(function (row){
		 rowHtml =  $(this).closest("tr");	 
		 reagentSupplies = "";
		 quantity="";
		
	 		quantity=$(rowHtml).find("#reagents_quality").val();
	 		preparationregents=$(rowHtml).find("#preparationReagents").val()
	 		reagentsSup=$(rowHtml).find("#reagentsupplies").val();
	 		//alert("test " + quantity);
		 	  	rowData+= "{preparationReagents:"+preparationregents+",quantity:'"+quantity+"',refReagentsSupplies:'"+reagentsSup+"'},";
	 		
	 		
	 		
	 });
	 
	//alert("row data " + rowData);
	 if(rowData.length >0){
            rowData = rowData.substring(0,(rowData.length-1));
        }
	
	 var rowData1= "";
	 var equimentName;
	 var manufacturer;
	 var modelNumber;
	 var maintainanceDate;
	 var mhsNumber;
	 var pkeqData;
	 var reagentsSup;
	 $("#equipmentTable").find("#equiment_name").each(function (r){
		 row =  $(this).closest("tr");
		   equimentName="";
		   reagentsSup="";
	 		pkeqData= $(row).find("#preparationEquipmentsId").val();
			equimentName= $(row).find("#equiment_name").val();
			//maintainanceDate=$(this).find(".dateEntry").val();
			reagentsSup=$(row).find("#reagentsupplies").val();
	 	
	 	//if(pkeqData!='undefined'&& pkeqData!='' && reagentsSup!=''&& reagentsSup!='undefined'){
	 		rowData1+= "{preparationEquipmentsId:"+pkeqData+",refReagentsSupplies:'"+reagentsSup+"'},";
 		//}
	 		
	 		//alert(equipments + " / " + manufacturer + " / " + rowData1 );
	 });
	 
	 if(rowData1.length >0){
		 rowData1 = rowData1.substring(0,(rowData1.length-1));
     }

		url = "savePreparation";
		
		var preprationId = $("#preparation").val(); 
		
		//alert("rowData : "+rowData);
		//alert("rowData1 : "+rowData1);
		var jsonData = "jsonData={reagentData:[" + rowData + "],equimentData:["+rowData1+"],preparationData:{preparation:'"+preprationId+"'}}";
		
		//var jsonData = "jsonData={reagentData:[" + rowData + "]}";

		$('.progress-indicator').css('display', 'block');
		//alert("jsonData " + jsonData)
		response = jsonDataCall(url, jsonData);

		$('.progress-indicator').css('display', 'none');
		alert("Data saved Successfully");
		//?BB stafaAlert('Data saved Successfully','');
		constructEquipmentTable(true,donorVal);
		constructReagentsTable(true,donorVal);
		return true;
}
function loadTmpDropdown(obj,fieldName,criteria){
	//alert("loadTmpDropdown");
	//alert("val " + $(obj).val() + " / " + name);
	// alert(" trac1 " + $("#tmpSelect").find('option').html() + " /select-> " + $("#tmpSelect").html());
	 url="loadDropdown";
	 response = jsonDataCall(url,"jsonData={fieldName:\""+fieldName+"\",criteria:\""+criteria+"\"}");
	 $("#tmpSelect").find('option').remove().end().append('<option value="">Select</option>');
	// alert(" trac " + $("#tmpSelect").find('option').html());
	 $(response.dropdownList).each(function(i){
		 $("#tmpSelect").find('option').end().append('<option value="'+this+ '">'+ this+'</option>');
	//	 alert(i+"/"+ this +" / "+ $("#tmpSelect").find('option').html());
	 });	 
	
	 
} 
function loadDropdown(obj,name){
	//alert("loadDropdown");
	try{
	//alert("val " + $(obj).val() + " / " + name);
	 url="loadDropdown";
	 var fieldName="";
	 if($.trim($(obj).val()) ==""){
		 return false;
	 }
	 
	 var row = $(obj).parent().closest("tr");
	 var tmpValue="";
	 var criteria=" and ";
	 if(name=="antigulantsLot"){
		 criteria+= " rs_type='REAGENTS'  and name ='"+$.trim($(obj).val())+"'";
		 fieldName = "lotNumber";
		 $(row).find("#expiryDate").val("");
	 }else if(name=="expiryDate"){
		 criteria+= " rs_type='REAGENTS'  and name ='"+$.trim($(row).find("#antigulantsName").val()) +"' and lotNumber='"+$.trim($(obj).val())+"' ";
		 fieldName = "to_char(expDate,'Mon DD, YYYY'),reagentsupplies";
		 $(row).find("#expiryDate").val("");
	 }else if(name== "additivesLot"){
		 criteria+= " rs_type='REAGENTS'  and name ='"+$.trim($(obj).val())+"'";
		 fieldName = "lotNumber";
		 $(row).find("#additivesExpiryDate").val("");
	 }else if(name== "additivesExpiryDate"){
		 criteria+= " rs_type='REAGENTS'  and name ='"+$.trim($(row).find("#additivesName").val()) +"' and lotNumber='"+$.trim($(obj).val())+"' ";
		 fieldName = "to_char(expDate,'Mon DD, YYYY'),reagentsupplies";
		 $(row).find("#additivesExpiryDate").val("");
	 }
	 
	 $('.progress-indicator').css( 'display', 'block' );
	 response = jsonDataCall(url,"jsonData={fieldName:\""+fieldName+"\",criteria:\""+criteria+"\"}");
	
	
	 if(name== "expiryDate"){
 		$(response.dropdownList).each(function(i){
 			//alert("this " + this);
 			//alert(this[0] + " / " + this[1]);
			 if(jQuery.type(this[0]) == "string"){
			 	$(row).find("#"+name).val(this[0]);
			 	$(row).find("#reagentsupplies").val(this[1]);
			 }
			 
		 });
	 }else if (name=="additivesExpiryDate"){
		 $(response.dropdownList).each(function(i){
	 			//alert("this " + this);
	 			//alert(this[0] + " / " + this[1]);
				 if(jQuery.type(this[0]) == "string"){
				 	$(row).find("#"+name).val(this[0]);
				 	$(row).find("#reagentsupplies").val(this[1]);
				 }
				 
			 });
	} else{
		 $(row).find("#"+name).find('option').remove().end()
		    .append('<option value="">Select</option>');
		 $(response.dropdownList).each(function(i){
			 $(row).find("#"+name).find('option').end().append('<option value="'+this+ '">'+ this+'</option>');
			// alert(i+"/ base "+  $(row).find("#"+name).find('option').html());
		 });
		
	 }
	  $.uniform.restore('select');
      $("select").uniform();
      $('.progress-indicator').css( 'display', 'none' );
      
	}catch(e){
		alert("Error : "+e);
	}
}
var equipmentTable=0;
function addnewequipment(activeFlag){
	var newRow;
	
	equipmentTable+=1;
	 newRow="<tr>"+
		"<td><input type='checkbox' id='preparationEquipmentsId' name='preparationEquipmentsId' value='0'/></td>"+
		"<td><select class='select1' id='equiment_name'name='equimentname' onchange='javascript:loadDropdown(this,\"equipment_manufacturer\");'>"+$("#equipment_hidden").html() + "</select></td>"+
		"<td><select class='select1' id='equipment_manufacturer' name='manufacturer' onchange='javascript:loadDropdown(this,\"modelno\");'></select></td>"+
		"<td><select class='select1' id='modelno' name='modelno' onchange='javascript:loadDropdown(this,\"mhs\");'></select></td>"+
		"<td><select class='select1' id='mhs' name='mhs' onchange='javascript:loadDropdown(this,\"maintainanceDueDate\");'></select></td>"+
		"<td><input type='text' class='dateEntry dateclass' id='maintainanceDueDate' name='maintainanceDueDate'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/></td>"+
		"</tr>";
	$("#anticoagulants #equipmentTable tbody").prepend(newRow); 
	
		$("select").uniform(); 	
		$( ".dateclass" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
		 changeYear: true, yearRange: "c-50:c+45"});

	
} 
function addnewRegents(activeFlag){
	var newRow;
		 newRow="<tr>"+
			"<td><input type='checkbox' id='preparationReagents' name='preparationReagents' value='0'></td>"+
			//"<td><input type='text'  id='reagentsname' name='reagentsname' class='plain' onchange='javascript:loadDropdown(this,\"reagents_manufacturer\");' value=''></td>"+
			"<td><select class='select1'  id='reagentsname' name='reagentsname' onchange='javascript:loadDropdown(this,\"reagents_manufacturer\");'>"+$("#reagent_hidden").html() + "</select></td>"+
			"<td><input type='text' id='reagents_quality' name='quantity' /></td>"+
			"<td><select class='select1' id='reagents_manufacturer' name='reagents_manufacturer' onchange='javascript:loadDropdown(this,\"reagents_lot\");'></select></div></td>"+
			"<td><div><select class='select1' id='reagents_lot' name='reagents_lot' onchange='javascript:loadDropdown(this,\"reagents_expiration_date\");' ></select></div></td>"+
			"<td><input type='text' class='dateEntry dateclass'id='reagents_expiration_date' name='reagents_expiration_date'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/></td>"+
			"</tr>";
		$("#reagenTable tbody").prepend(newRow); 
		
		 $(".plain").each(function (){
			   $(this).removeClass("plain");
			    setObjectAutoComplete($(this),"reagent_hidden")
		   });
		 $("#reagenTable .ColVis_MasterButton").triggerHandler("click");
		  $('.ColVis_Restore:visible').triggerHandler("click");
		  $("div.ColVis_collectionBackground").trigger("click");
    $("select").uniform(); 	
    $( ".dateclass" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
        changeYear: true, yearRange: "c-50:c+45"});
}
function editEquimentsRows(){
	//  alert("edit Rows");
	var tmpValue;
	var rowHtml;
	var colObj;
	  $("#equipmentTable tbody tr").each(function (row){
		  rowHtml = $(this);
		 if($(this).find("#preparationEquipmentsId").is(":checked")){
			 $(this).find("td").each(function (col){
				 if(col ==1 ){
					 
					 if ($(this).find("#equiment_name").val()== undefined){
						 tmpValue = $(this).text();
						$(this).html("<select class='select1' id='equiment_name' name='equimentname' onchange='javascript:loadDropdown(this,\"equipment_manufacturer\");'>"+$("#equipment_hidden").html() + "</select>");
						$(this).find("#equiment_name").val(tmpValue);
						colObj = $(this).find("#equiment_name");
						
						//$(this).find("#equiment_name").trigger("onchange");
						
					 }
				 }else if(col ==2){
					 if ($(this).find("#equipment_manufacturer").val()== undefined){
						 tmpValue = $(this).text();
					 	$(this).html("<select class='select1' id='equipment_manufacturer' name='manufacturer' onchange='javascript:loadDropdown(this,\"modelno\");'></select>");
					 	loadDropdown($(colObj),"equipment_manufacturer");
					 	$(this).find("#equipment_manufacturer").val(tmpValue);
					 	colObj = $(this).find("#equipment_manufacturer");
					 }
				 }else if(col ==3){
					 if ($(this).find("#modelno").val()== undefined){
						 tmpValue = $(this).text();
					 	$(this).html("<select class='select1' id='modelno' name='modelno' onchange='javascript:loadDropdown(this,\"mhs\");'></select>");
					 	loadDropdown($(colObj),"modelno");
					 	$(this).find("#modelno").val(tmpValue);
					 	colObj = $(this).find("#modelno");
					 }
				 }else if(col ==4){
					 if ($(this).find("#mhs").val()== undefined){
						 	tmpValue = $(this).text();
						 	$(this).html("<select class='select1' id='mhs' name='mhs' onchange='javascript:loadDropdown(this,\"maintainanceDueDate\");'></select></select>");
						 	loadDropdown($(colObj),"mhs");
						 	$(this).find("#mhs").val(tmpValue);
						 	colObj = $(this).find("#mhs");
						 }
				}else if(col ==5){
					 if ($(this).find("#maintainanceDueDate").val()== undefined){
						 	tmpValue = $(this).text();
						 	$(this).html("<input type='text' class='dateEntry dateclass calDate' id='maintainanceDueDate' name='maintainanceDueDate'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/>");
						 	$(this).find("#maintainanceDueDate").val(tmpValue);
						 }
				}
			 });
			 
		 }
	  });
	  $.uniform.restore('select');
      $("select").uniform();
      addDatepicker();
     
  }
function editReagentsRows(){
	//  alert("edit Rows");
	 var colObj;
	  $("#reagenTable tbody tr").each(function (row){
		 
		 if($(this).find("#preparationReagents").is(":checked")){
			 $(this).find("td").each(function (col){
				 if(col ==1 ){
					 if ($(this).find("#reagentsname").val()== undefined){
						 tmpValue = $(this).text();
						$(this).html("<select class='select1'  id='reagentsname' name='reagentsname' onchange='javascript:loadDropdown(this,\"reagents_manufacturer\");'>"+$("#reagent_hidden").html() + "</select>");
						$(this).find("#reagentsname").val(tmpValue);
						colObj = $(this).find("#reagentsname");
					 }
				 }else if(col ==2){
					 if ($(this).find("#reagents_quality").val()== undefined){
					 	$(this).html("<input type='text' id='reagents_quality' name='quantity' value='"+$(this).text()+ "' />");
					 }
				 }else if(col ==3){
					 if ($(this).find("#reagents_manufacturer").val()== undefined){
						 tmpValue = $(this).text();
					 	$(this).html("<select class='select1' id='reagents_manufacturer' name='reagents_manufacturer' onchange='javascript:loadDropdown(this,\"reagents_lot\");'></select>");
					 	loadDropdown($(colObj),"reagents_manufacturer");
					 	$(this).find("#reagents_manufacturer").val(tmpValue);
					 	colObj = $(this).find("#reagents_manufacturer");
					 }
				 }else if(col ==4){
					 if ($(this).find("#reagents_lot").val()== undefined){
						 tmpValue = $(this).text();
						 	$(this).html("<select class='select1' id='reagents_lot' name='reagents_lot' onchange='javascript:loadDropdown(this,\"reagents_expiration_date\");' ></select>");
						 	loadDropdown($(colObj),"reagents_lot");
						 	$(this).find("#reagents_lot").val(tmpValue);
						 	colObj = $(this).find("#reagents_lot");
						 }
				}else if(col ==5){
					 if ($(this).find("#reagents_expiration_date").val()== undefined){
						 tmpValue = $(this).text();
						 	$(this).html("<input type='text' id='reagents_expiration_date' name='reagents_expiration_date' class='dateEntry dateclass' value='"+$(this).text()+ "' />");
						 	
						 	$(this).find("#reagents_expiration_date").val(tmpValue);
						 }
				}
			 });
		 }
	  });
      $.uniform.restore('select');
      $("select").uniform();
      addDatepicker();
  } 
/* function deleteEquimentData(){
	// alert("Delete Rows");
    var yes=confirm(confirm_deleteMsg);
	if(!yes){
		return;
	}
	var deletedIds = ""
	$("#equipmentTable #preparationEquipmentsId:checked").each(function (row){
		deletedIds += $(this).val() + ",";
	});
	if(deletedIds.length >0){
	  deletedIds = deletedIds.substr(0,deletedIds.length-1);
	}
	jsonData = "{deletedEquimentResult:'"+deletedIds+"'}";
	url="deleteEquimentData";
    response = jsonDataCall(url,"jsonData="+jsonData);
    constructEquipmentTable(true,donorVal);
}
function deleteReagentsData(){
	//alert("Delete Rows");
    var yes=confirm(confirm_deleteMsg);
	if(!yes){
		return;
	}
	var deletedIds = ""
	$("#reagenTable #preparationReagents:checked").each(function (row){
		deletedIds += $(this).val() + ",";
	});
	if(deletedIds.length >0){
	  deletedIds = deletedIds.substr(0,deletedIds.length-1);
	}
	jsonData = "{deletedReagentsResult:'"+deletedIds+"'}";
	url="deleteReagentsData";
    response = jsonDataCall(url,"jsonData="+jsonData);
    constructReagentsTable(true,donorVal);
}
function constructEquipmentTable(flag,donorId){
	 var criteria = "";
	 
	 try{
			
		
		var preparationVal = $("#preparation").val();
		var plannedProcedure = $("#donorProcedure").val();
		var link;
			var equipServerParam =  function ( aoData ) {
															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "apheresis_Preparation_Equipments"});	
															aoData.push( { "name": "param_count", "value":"2"});
															
															aoData.push( { "name": "param_1_name", "value":":PREPARATION:"});
															aoData.push( { "name": "param_1_value", "value":preparationVal});
															
															aoData.push( { "name": "param_2_name", "value":":PLANNEDPROCEDURE:"});
															aoData.push( { "name": "param_2_value", "value":plannedProcedure});
															
															
															aoData.push( {"name": "col_1_name", "value": "" } );
															aoData.push( { "name": "col_1_column", "value": ""});
															
															aoData.push( { "name": "col_2_name", "value": ""});
															aoData.push( { "name": "col_2_column", "value": ""});
															
															aoData.push( {"name": "col_3_name", "value": "" } );
															aoData.push( { "name": "col_3_column", "value": ""});
															
															aoData.push( {"name": "col_4_name", "value": "" } );
															aoData.push( { "name": "col_4_column", "value": ""});
															
															aoData.push( {"name": "col_5_name", "value": "" } );
															aoData.push( { "name": "col_5_column", "value": ""});
															
										};
			var equipColDef =  [
								 {
										"aTargets": [0], "mDataProp": function ( source, type, val ) { 
											return "<input type='checkbox' id='preparationEquipmentsId' name='preparationEquipmentsId' value='"+source[0]+"'/><input id='reagentsupplies' type='hidden' value='"+source[6]+"' name='reagentsupplies'/>";
								     }},
				                  {"sTitle":"Equipment",
										"aTargets": [1], "mDataProp": function ( source, type, val ) {
											
											if (source[0] == 0){
												 link ="<select class='select1' id='equiment_name' name='equimentname' onchange='javascript:loadDropdown(this,\"equipment_manufacturer\");' >";
													 var selectTxt = $("#equipment_hidden").html();
			                                	  var searchTxt = 'value="'+source[1] +'"'; 
			                                	  selectTxt = selectTxt.replace(searchTxt,searchTxt +' selected')
													link+= selectTxt + "</select>";
											}else{
												link = source[1];
											}
											
											return link;	
												
											//return source[1];
				                	 }},
								  {	"sTitle":"Manufacturer",
				                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
				                			 link = source[3];
				                			 if (source[0] == 0){
				                				 link = "<select class='select1' id='equipment_manufacturer' name='manufacturer' onchange='javascript:loadDropdown(this,\"modelno\");'>";
				                				 var tmpCr = "  and rs_type='EQUIPMENT'  and name ='"+source[1] +"'";
				                				 loadTmpDropdown($("#tmpSelect"),"manufacture",tmpCr);
				                				// alert(" tmp " +$("#tmpSelect").html() );
				                				link+= $("#tmpSelect").html() +"</select>";
				                			 }
				                			 return link;
				                	  }},
								  {	"sTitle":"Model#",
										  "aTargets": [3], "mDataProp": function ( source, type, val ) {
											  link = source[4];
					                			 if (source[0] == 0){
					                				 link = "<select class='select1' id='modelno' name='modelno' onchange='javascript:loadDropdown(this,\"mhs\");'></select>";
					                			 }
					                		 return link;
								  	  }},
								  {	"sTitle":"MHS#",
									  	  "aTargets": [4], "mDataProp": function ( source, type, val ) {
									  		link = source[2];
				                			 if (source[0] == 0){
				                				 link = "<select class='select1' id='mhs' name='mhs' onchange='javascript:loadDropdown(this,\"maintainanceDueDate\");'></select>";
				                			 }
				                		 return link;
							  		  }},
								  {	"sTitle":"Maintenance Due Date",
								  		  "aTargets": [5], "mDataProp": function ( source, type, val ) { 
								  			link = source[5];
				                			 if (source[0] == 0){
				                				 link = "<input type='text' class='dateEntry dateclass calDate' id='maintainanceDueDate' name='maintainanceDueDate'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/>";
				                			 }
					                		 return link;
									  }}
								];
						var equipColManager =  function () {
								        $("#equipmentTableColumn").html($(".ColVis:eq(0)"));
									};
						var equipCol = [{"bSortable": false},
							               null,
							               null,
							               null,
							               null,
							               null
							               ];
					constructTable(flag,'equipmentTable',equipColManager,equipServerParam,equipColDef,equipCol);
	 }catch(e){
			alert("Error : "+e);
		}
									
}
function constructReagentsTable(flag,donorId){
	 var reagentCriteria = "";
		
		var preparationVal = $("#preparation").val();
		var plannedProcedure = $("#donorProcedure").val();
		var reagentsServerParam = function ( aoData ) {
														aoData.push( { "name": "application", "value": "stafa"});
														aoData.push( { "name": "module", "value": "apheresis_Preparation_Reagents"});	
														aoData.push( { "name": "param_count", "value":"2"});
														
														aoData.push( { "name": "param_1_name", "value":":PREPARATION:"});
														aoData.push( { "name": "param_1_value", "value":preparationVal});
														
														aoData.push( { "name": "param_2_name", "value":":PLANNEDPROCEDURE:"});
														aoData.push( { "name": "param_2_value", "value":plannedProcedure});
														
														
														aoData.push( {"name": "col_1_name", "value": "lower(rs.RS_NAME)" } );
														aoData.push( { "name": "col_1_column", "value": "2"});
														
														aoData.push( { "name": "col_2_name", "value": "lower(prereg.QUANTITY)"});
														aoData.push( { "name": "col_2_column", "value": "lower(prereg.QUANTITY)"});
														
														aoData.push( {"name": "col_3_name", "value": "lower(rs.RS_MANUFACTURE)" } );
														aoData.push( { "name": "col_3_column", "value": "lower(rs.RS_MANUFACTURE)"});
														
														aoData.push( {"name": "col_4_name", "value": "lower(rs.RS_LOTNUMBER)" } );
														aoData.push( { "name": "col_4_column", "value": "lower(rs.RS_LOTNUMBER)"});
														
														aoData.push( {"name": "col_5_name", "value": "lower(rs.RS_EXPDATE)" } );
														aoData.push( { "name": "col_5_column", "value": "lower(rs.RS_EXPDATE)"});
			
		};
		var reagentColDef = [
							 {
									"aTargets": [0], "mDataProp": function ( source, type, val ) { 
									    return "<input type='checkbox' id='preparationReagents' name='preparationReagents' value='"+source[0]+"'/><input id='reagentsupplies' type='hidden' value='"+source[6]+"' name='reagentsupplies'/>";
							     }},
			                  {"sTitle":"Reagents and Supplies",
									"aTargets": [1], "mDataProp": function ( source, type, val ) {
										link = source[1];
										if (source[0] == 0){
											 link ="<select class='select1'  id='reagentsname' name='reagentsname' onchange='javascript:loadDropdown(this,\"reagents_manufacturer\");'>";
												 var selectTxt = $("#reagent_hidden").html();
		                                	  var searchTxt = 'value="'+source[1] +'"'; 
		                                	  selectTxt = selectTxt.replace(searchTxt,searchTxt +' selected')
												link+= selectTxt + "</select>";
										}
										return link; 
			                	 }},
							  {	"sTitle":"Quantity",
			                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
			                			  link = source[7];
											if (source[0] == 0){
												 link ="<input type='text' id='reagents_quality' name='quantity' value='"+source[7]+ "' />";
											}
											return link; 	
			                	  }},
							  {	"sTitle":"Manufacturer",
									  "aTargets": [3], "mDataProp": function ( source, type, val ) {
										  link = source[3];
				                			 if (source[0] == 0){
				                				 link = "<select class='select1' id='reagents_manufacturer' name='reagents_manufacturer' onchange='javascript:loadDropdown(this,\"reagents_lot\");'>";
				                				 var tmpCr = "  and rs_type='REAGENTS'  and name ='"+source[1] +"'";
				                				 loadTmpDropdown($("#tmpSelect"),"manufacture",tmpCr);
				                				// alert(" tmp " +$("#tmpSelect").html() );
				                				link+= $("#tmpSelect").html() +"</select>";
				                			 }
				                			 return link; 
							  	  }},
							  {	"sTitle":"Lot#",
								  	  "aTargets": [4], "mDataProp": function ( source, type, val ) {
								  		 link = source[4];
			                			 if (source[0] == 0){
			                				 link = "<select class='select1' id='reagents_lot' name='reagents_lot' onchange='javascript:loadDropdown(this,\"reagents_expiration_date\");' >";
			                				// var tmpCr = "  and rs_type='REAGENTS'  and name ='"+source[1]) +"' and manufacture='"+$.trim($(obj).val())+"' ";
			                				// loadTmpDropdown($("#tmpSelect"),"manufacture",tmpCr);
			                				// alert(" tmp " +$("#tmpSelect").html() );
			                				//			link+= $("#tmpSelect").html() +"</select>";
			                				 link+= "</select>";
			                			 }
			                			 return link; 
						  		  }},
							  {	"sTitle":"Expiration Date",
							  		  "aTargets": [5], "mDataProp": function ( source, type, val ) {
							  			link = source[5];
			                			 if (source[0] == 0){
			                				 link= "<input type='text' id='reagents_expiration_date' name='reagents_expiration_date' class='dateEntry dateclass' value=''  />";
			                			 }
			                			 return link; 
								  }}
							];
		var reagentColManager = function () {
			$("#reagenColumn").html($('.ColVis:eq(1)'));
		};
		var reagentCol = [{"bSortable": false},
			               null,
			               null,
			               null,
			               null,
			               null
			               ];
		 constructTable(flag,'reagenTable',reagentColManager,reagentsServerParam,reagentColDef,reagentCol);

} */
function loadPostData(){
	//alert("load");
	var donor=$("#donor").val();
	var donorProcedure = $("#donorProcedure").val();
	var url = "loadPostData";
	var jsonData = "donorProcedure:"+donorProcedure+",donor:"+donor;
	//alert("jsonData::"+jsonData);
	response = jsonDataCall(url,"jsonData={"+jsonData+"}");
	
	 var keyEvent1 = jQuery.Event("keyup");
	 keyEvent1.which=13;
	
	if(response.postList!=null && response.postList.length>0){
		$("#preparation").val(response.preparationId);
		var postId = response.postList[0][2];
		//alert("postId"+postId);
		if(postId!=null && postId>0){
			var productId = response.postList[0][0];
			var plasmaId = response.postList[0][1];
			if(productId!=null && productId!=""){
				$("#divProductScan #fake_conformProductSearch").val(productId);
				$("#divProductScan #fake_conformProductSearch").css("display","inline");
				$("#divProductScan #conformProductSearch").val(productId);
				$("#divProductScan #conformProductSearch").css("display","none");
				$("#scanType").val("PRODUCT");
				getScanData(keyEvent1,null);
				$("#productFlag").val(true);
			}
			
				//alert("plasmaId : "+plasmaId);
				if(plasmaId!=null && plasmaId!=""){
					$("#divPlasmaScan #fake_conformProductSearch").val(plasmaId);
					$("#divPlasmaScan #fake_conformProductSearch").css("display","inline");
					$("#divPlasmaScan #conformProductSearch").val(plasmaId);
					$("#divPlasmaScan #conformProductSearch").css("display","none");
					$("#scanType").val("PLASMA");
					getScanData(keyEvent1,null);
					$("#productFlag").val(true);
				}
			  
				$("#scanType").val("");
		}
	}
	
}

function constructAnticoagulantsTable(flag,donor,donorProcedure){
	//alert("Cons Method");
	var criteria = "and fk_donor="+donor+"and fk_plannedprocedure="+donorProcedure;
	
	/*Anticoagulants ServerParameters Starts*/
	var anticoagulants_ColManager = function(){
        										$("#anticoagulantsColumn").html($('#anticoagulantsTable_wrapper .ColVis'));
										 };
								 
	var anticoagulants_aoColumn = [ { "bSortable":false},
		         		            { "bSortable":false},
		    		               	null,
		    			            null,
		    			            null,
		    			            null,
		    			            null,
		    			            null,
		    			            null
	    			             ];	
	var anticoagulantsServerParams = function ( aoData ) {
															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "popup_anticoagulants"});
															aoData.push( { "name": "criteria", "value": criteria});
															
															aoData.push( {"name": "col_1_name", "value": "" } );
															aoData.push( { "name": "col_1_column", "value": ""});
															
															aoData.push( { "name": "col_2_name", "value": ""});
															aoData.push( { "name": "col_2_column", "value": ""});												
														};
														
	var anticoagulants_aoColumnDef = [
		                         {		"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
		                             		return "";
		                             }},
		                          {	"sTitle":'Check All <input type="checkbox"/>',
		                            	"aTargets": [ 1], "mDataProp": function ( source, type, val ) {
		                             		return "<input name='antigulants' id= 'antigulants' type='checkbox' value='"+source.antigulants+"' />";
		                            }},
		                          {	"sTitle":'Name',
		                            	"aTargets": [ 2], "mDataProp": function ( source, type, val ) {
		                                    return source.antigulantsName;
		                           }},
		                          {	"sTitle":'Lot#',
		                            	"aTargets": [ 3], "mDataProp": function ( source, type, val ) {
		                                    return source.antigulantsLot;
		                           }},
		                          {	"sTitle":'Exp.Date',
		                            	"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
		                            		/* if(source.expiryDate!=null){
		                    	    			var expireDate = ($.datepicker.formatDate('M dd, yy', new Date(source.expiryDate)));
		                    	    	        	}else{
		                    	    	        		expireDate = "";
		                    	    	    } */
		                                    return converHDate(source.expiryDate);
		                           }},
		                          {	"sTitle":'Volume',
		                            	"aTargets": [ 5], "mDataProp": function ( source, type, val ) {
		                                    return source.antigulantsVolume;
		                           }},
		                          {	"sTitle":'Date of Addition',
		                            	"aTargets": [ 6], "mDataProp": function ( source, type, val ) {
		                            		/* if(source.antigulantsAdditionDate!=null){
		                    	    			var additionDate = ($.datepicker.formatDate('M dd, yy', new Date(source.antigulantsAdditionDate)));
		                    	    	        	}else{
		                    	    	        		additionDate = "";
		                    	    	    } */
		                                    return converHDate(source.antigulantsAdditionDate);
		                           }},
		                         
		                          {	"sTitle":'Time of Addition',
		                            	"aTargets": [ 7], "mDataProp": function ( source, type, val ) {
		                            		 if(source.antigulantsAdditionTime!=null){
			                     					var tmpDate = new Date(source.antigulantsAdditionTime);
			                     					if($.browser.msie){
			                     						var tmpDate1 = source.antigulantsAdditionTime.split("-");
			                     						var antiAdditionTime = tmpDate1[2].substring(3,8);
			                     					}else{
				                        				var antiAdditionTime = (tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes();
			                     					}
			                     				}
		                            		/* if(source.antigulantsAdditionTime!=null){
		                        				var tmpDate = new Date(source.antigulantsAdditionTime);
		                        				var additionTime = (tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes();
		                        			}else{
		                        				additionTime = "";
		                        			} */
		                                    return antiAdditionTime;
		                           }},
		                            
		                          {	"sTitle":'Comment',
		                            	"aTargets": [ 8], "mDataProp": function ( source, type, val ) {
		                                    return source.antigulantsComments;
		                           }}
		                           
		                        ];
	
	/*Additives Table ServerParameters Starts*/
	var additives_ColManager = function(){
											$("#additivesColumn").html($('#additiveTable_wrapper .ColVis'));
 										 };	
	var additives_aoColumn = [ 		{ "bSortable":false},
	         		                { "bSortable":false},
	    		               	    null,
	    			                null,
	    			                null,
	    			                null,
	    			                null,
	    			                null,
	    			                null
	    			             ];
	var additiveServerParams = function ( aoData ) {
															aoData.push( { "name": "application", "value": "stafa"});
															aoData.push( { "name": "module", "value": "popup_additives"});
															aoData.push( { "name": "criteria", "value": criteria});
															
															aoData.push( {"name": "col_1_name", "value": "" } );
															aoData.push( { "name": "col_1_column", "value": ""});
															
															aoData.push( { "name": "col_2_name", "value": ""});
															aoData.push( { "name": "col_2_column", "value": ""});												
														};
														
	var additives_aoColumnDef = [
		                         {		"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
		                             		return "";
		                             }},
		                          {	"sTitle":'Check All <input type="checkbox"/>',
		                            	"aTargets": [ 1], "mDataProp": function ( source, type, val ) {
		                             		return "<input name='additives' id= 'additives' type='checkbox' value='"+source.additives+"' />";
		                            }},
		                          {	"sTitle":'Name',
		                            	"aTargets": [ 2], "mDataProp": function ( source, type, val ) {
		                                    return source.additivesName;
		                           }},
		                          {	"sTitle":'Lot#',
		                            	"aTargets": [ 3], "mDataProp": function ( source, type, val ) {
		                                    return source.additivesLot;
		                           }},
		                          {	"sTitle":'Exp.Date',
		                            	"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
		                            		/* if(source.additivesExpiryDate!=null){
		                    	    			var expireDate = ($.datepicker.formatDate('M dd, yy', new Date(source.additivesExpiryDate)));
		                    	    	        	}else{
		                    	    	        		expireDate = "";
		                    	    	    } */
		                                    return  converHDate(source.additivesExpiryDate);
		                           }},
		                          {	"sTitle":'Volume',
		                            	"aTargets": [ 5], "mDataProp": function ( source, type, val ) {
		                                    return source.additivesVolume;
		                           }},
		                          {	"sTitle":'Date of Addition',
		                            	"aTargets": [ 6], "mDataProp": function ( source, type, val ) {
		                            		/* if(!=null){
		                    	    			var additionDate = ($.datepicker.formatDate('M dd, yy', new Date(source.additivesAdditionDate)));
		                    	    	        	}else{
		                    	    	        		additionDate = "";
		                    	    	    } */
		                                    return converHDate(source.additivesAdditionDate);
		                           }},
		                         
		                          {	"sTitle":'Time of Addition',
		                            	"aTargets": [ 7], "mDataProp": function ( source, type, val ) {
		                            		 if(source.additivesAdditionTime!=null){
		                     					var tmpDate = new Date(source.additivesAdditionTime);
		                     					if($.browser.msie){
		                     						var tmpDate1 = source.additivesAdditionTime.split("-");
		                     						var additionTime = tmpDate1[2].substring(3,8);
		                     					}else{
			                        				var additionTime = (tmpDate.getHours()<10?'0':'') + tmpDate.getHours()+":"+(tmpDate.getMinutes()<10?'0':'') + tmpDate.getMinutes();
		                     					}
		                     				}
		                            		/* if(source.additivesAdditionTime!=null){
		                        				var tmpDate = new Date(source.additivesAdditionTime);
		                        			}else{
		                        				additionTime = "";
		                        			} */
		                                    return additionTime;
		                           }},
		                            
		                          {	"sTitle":'Comment',
		                            	"aTargets": [ 8], "mDataProp": function ( source, type, val ) {
		                                    return source.additivesComments;
		                           }}
		                          
		                        ];
			constructTable(flag,'anticoagulantsTable',anticoagulants_ColManager,anticoagulantsServerParams,anticoagulants_aoColumnDef,anticoagulants_aoColumn);
			constructTable(flag,'additiveTable',additives_ColManager,additiveServerParams,additives_aoColumnDef,additives_aoColumn);
	}

/* function addParameter(){
    var runningTabClass= $('#parameterTable tbody tr td:nth-child(1)').attr("class");
    if(runningTabClass=="dataTables_empty"){
        $("#parameterTable tbody").replaceWith("<tr>"+
                                "<td></td>"+
                                "<td><input type='text' id=''/></td>"+
                                "<td><input type='text' id=''/></td>"+
                                "<td><input type='text' id=''/></td>"+
                                "<td><input type='text' id=''/></td>"+
                                "<td><input type='text' id=''/></td>"+
                                "<td><input type='text' id=''/></td>"+
                                "<td><input type='text' id=''/></td>"+
                                "<td><select class='select1'><option>Select</option></select></td>"+
                                "<td><input type='text' id=''/></td>"+
                                "</tr>");
    }
    else{
        $("#parameterTable tbody").prepend("<tr>"+
                "<td></td>"+
                "<td><input type='text' id=''/></td>"+
                "<td><input type='text' id=''/></td>"+
                "<td><input type='text' id=''/></td>"+
                "<td><input type='text' id=''/></td>"+
                "<td><input type='text' id=''/></td>"+
                "<td><input type='text' id=''/></td>"+
                "<td><input type='text' id=''/></td>"+
                "<td><select class='select1'><option>Select</option></select></td>"+
                "<td><input type='text' id=''/></td>"+
                "</tr>");
        }
    $("select").uniform();
    
    
} */
$(function(){
	
	
    $("input[name='specAnticoagulantsAdditives']").click(function(){
     	var checkradio=$("input[name='specAnticoagulantsAdditives']:checked").val();
     	//alert("checkradio++++"+checkradio);
    	 if(checkradio!=1){
    		 $("#anticogulants_view").hide();
    	 }
     	else {
     		anticoagulantsPopup(donorValue,donorplannedprocedure);
    		 $("#anticogulants_view").show();
    	 }
    	    $("select").uniform();

     });
    
   /*  $('#anticogulants_view').live('click', function (e) {
    	
    	showPopUp('open','anticoagulants','Anticogulants and Additives','850','550');
    }); */
  });
  
/*   function addnewanticoagulants(){
	var newRow;
	 		newRow =  "<tr><td></td>"+
		              "<td><input type='checkbox' id='anticoagulantsrow' name='anticoagulantsrow' value='0'><input type='hidden' id='antigulants' name='antigulants' value='0'/></td>"+
		              "<td><select class='select1'  id='antigulantsName' name='antigulantsName' onchange='javascript:loadDropdown(this,\"antigulantsLot\");'>"+$("#reagent_hidden").html() + "</select></td>"+
		              "<td><div><select class='select1' id='antigulantsLot' name='antigulantsLot' onchange='javascript:loadDropdown(this,\"expiryDate\");' ></select></div></td>"+
		              "<td><input type='text' class='dateEntry dateclass'id='expiryDate' name='expiryDate'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/></td>"+
		              "<td><input type='text' id='antigulantsVolume' name='antigulantsVolume'/></td>"+
		              "<td><input type='text' id='antigulantsAdditionDate' name='antigulantsAdditionDate' class='dateEntry'/></td>"+
		              "<td><input type='hidden' id='antigulantsAdditionTime' name='antigulantsAdditionTime'/><input type='text' id='tempantigulantsAdditionTime' name='tempantigulantsAdditionTime' class='antitimepicker' onchange='setanticoagulantstime()'/></td></td>"+
		              "<td><input type='text' id='antigulantsComments' name='antigulantsComments'/></td>"+
		              "</tr>";
		             
	$("#anticoagulantsTable tbody").prepend(newRow);
	
	$("#anticoagulantsTable .ColVis_MasterButton").triggerHandler("click");
	$('.ColVis_Restore:visible').triggerHandler("click");
	$("div.ColVis_collectionBackground").trigger("click");
	
	$("select").uniform();
	$(".dateEntry").datepicker({
			dateFormat: 'M dd, yy',
			changeMonth: true,
			changeYear: true, 
			yearRange: "c-50:c+45"
		});
	$(".antitimepicker").timepicker({});
  }
  function addnewadditives(){
	var newRow;
 		newRow =  "<tr><td></td>"+
	              "<td><input type='checkbox' id='additivesrow' name='additivesrow' value='0'><input type='hidden' id='additives' name='additives' value='0'/></td>"+
	              "<td><select class='select1'  id='additivesName' name='additivesName' onchange='javascript:loadDropdown(this,\"additivesLot\");'>"+$("#reagent_hidden").html() + "</select></td>"+
	              "<td><div><select class='select1' id='additivesLot' name='additivesLot' onchange='javascript:loadDropdown(this,\"additivesExpiryDate\");' ></select></div></td>"+
	              "<td><input type='text' class='dateEntry dateclass'id='additivesExpiryDate' name='additivesExpiryDate'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/></td>"+
	              "<td><input type='text' id='additivesVolume' name='additivesVolume'/></td>"+
	              "<td><input type='text' id='additivesAdditionDate' name='additivesAdditionDate' class='dateEntry'/></td>"+
	              "<td><input type='hidden' id='additivesAdditionTime' name='additivesAdditionTime'/><input type='text' id='tempadditivesAdditionTime' name='tempadditivesAdditionTime' class='antitimepicker' onchange='setadditivetime()'/></td></td>"+
	              "<td><input type='text' id='additivesComments' name='additivesComments'/></td>"+
	              "</tr>";
	                    
	$("#additiveTable tbody").prepend(newRow);
	
	$("#anticoagulantsTable .ColVis_MasterButton").triggerHandler("click");
	$('.ColVis_Restore:visible').triggerHandler("click");
	$("div.ColVis_collectionBackground").trigger("click");
	
	$("select").uniform();
	$(".dateEntry").datepicker({
			dateFormat: 'M dd, yy',
			changeMonth: true,
			changeYear: true, 
			yearRange: "c-50:c+45"
		});
	$(".antitimepicker").timepicker({});
 }
  function setadditivetime(){
	  $('#additivesAdditionTime').val($('#additivesAdditionDate').val() +" "+ $('#tempadditivesAdditionTime').val());
  }
  function setanticoagulantstime(){
	  $('#antigulantsAdditionTime').val($('#antigulantsAdditionDate').val() +" "+ $('#tempantigulantsAdditionTime').val());
  } */
  
  function savePostCollection(mode){
	  //alert($("#flowNextFlag").val());
		$("#postCollectionForm").find("#flowNextFlag").val($("#loginForm #flowNextFlag").val());
	  	$("#refDonor").val($("#donor").val());
	  	
	  	//checkNA('plasmaVerify','plasmaLabelVerified');
	  	//checkNA('specProductLVN2','productLabelVerified');
	  	//checkNA('weightVerifiedNurse2','weightVerifiedN2');
	  	
	  	if(mode=="next" && ( !checkCVC() || !checkDonorStatus())){
  			return false;
  		}
	  	
	  	
		var url='savePostCollection';
		var response = ajaxCall(url,"postCollectionForm");
		//alert("Data saved succesfully");
		saveQuestions('postassessment');
		saveQuestions('postcollinformation');
		saveQuestions('productQCTable');
		stafaAlert('Data saved Successfully','');
		//?BB $("#main").html(response);
		if($("#flowNextFlag").val()!="true"){
			var postCollection = $("#postCollection").val();
			if(postCollection ==0){
				var donor=$("#donor").val();
				var donorProcedure=$("#donorProcedure").val();
				var url = "loadPostCollection.action?donorProcedure="+donorProcedure +"&donor="+donor;
				loadAction(url);
			}
		}
		return true;
	}
  function checkNA(fieldId,domId){
	 /*  if($("#"+fieldId).val()=="Yes"){
	  		$("#"+domId).val("1");
	  	}else if($("#"+fieldId).val()=="No"){
	  		$("#"+domId).val("0");
	  	}else */ 
	  	if($("#"+fieldId).val()=="N/A"){
	  		$("#"+domId).val("-1");
	  	}
  }
  function setvalues(id){
	    var s=$('#'+id).attr('checked');
	    if(s=='checked'){
	        $('#'+id).val(1);
	    }else{
	        $('#'+id).val(0);
	    }
	}
 function setCollectionStartTime(){
	  $('#collectionStartTime').val($('#collectionDate').val() +" "+ $('#tempCollectionStartTime').val());
  }
  function setCollectionEndTime(){
	  $('#collectionEndTime').val($('#collectionDate').val() +" "+ $('#tempCollectionEndTime').val());
  }
  function setPlasmaCollStartTime(){
	  $('#plasmaCollectionStartTime').val($('#plasmaCollectionDate').val() +" "+ $('#tempPlasmaCollStartTime').val());
  }
  function setValueNA(cBoxId,textId){
	    var s=$('#'+cBoxId).attr('checked');
	    if(s=='checked'){
	        $('#'+textId).val("N/A");
	    }else {
	    	$('#'+textId).val("");
	    }
	}

  function constructLab(flag){
	  var addLabColDef = [
	  					 {
	  							"aTargets": [0], "mDataProp": function ( source, type, val ) { 
	  							    return "<input type='checkbox' id='pkCodelst' name='pkCodelst' value='"+source[1] +"'/><input type='hidden' id='subType' name='subType' value='"+source[2] +"'/>";
	  					     }},
	  	                  {"sTitle":"Code",
	  							"aTargets": [1], "mDataProp": function ( source, type, val ) {
	  								return source[0];
	  	                	 }},
	  					  {	"sTitle":"Description",
	  	                		 "aTargets": [2], "mDataProp": function ( source, type, val ) { 
	  	                			 return "";
	  	                	  }}
	  					]; 
	  var addLabColumns = [{"bSortable": false},
	  		               null,
	  		               null
	  		               ];
	  var addLabServerParams = function ( aoData ) {
	  											aoData.push( { "name": "application", "value": "stafa"});
	  											aoData.push( { "name": "module", "value": "add_Lab_Test"});	
	  											
	  											aoData.push( {"name": "col_1_name", "value": "lower(codelst_desc)" } );
	  											aoData.push( { "name": "col_1_column", "value": "lower(codelst_desc)"});
	  											
	  											aoData.push( { "name": "col_2_name", "value": ""});
	  											aoData.push( { "name": "col_2_column", "value": ""});
	  										};
	  		constructTableWithoutColManager(flag,'addLabTable',addLabServerParams,addLabColDef,addLabColumns);
	  						
	  }
  function addLab(){
		var splitVal =  new Array();
		var finalVal = new Array();
		var hiddenVal;
		if($("#postCollLabTest").val()==""){
			showPopUp("open","addLabTest","Add Lab Test","600","550");
			constructLab(false);
		}else{
			hiddenVal = $("#postCollLabTest").val();
			splitVal = (hiddenVal.split(","));
			for(s=0;s<splitVal.length;s++){
				$("#tempLabTest option").each(function(i){
					if($(this).val()==splitVal[s]){
						finalVal.push($(this).text());
					}
				});
			}
			showPopUp("open","addLabTest","Add Lab Test","600","550");
			constructLab(true);
		}
	}	
	function closePopup(){
		try{
	    		$("#addLabTest").dialog("close");
	  		}catch(err){
				alert("err" + err);
			}
	}
	var subTypVal = [];
	var nextTD = [];
	function saveLabTest(){
		var chkValues = "";
		$('[name="pkCodelst"]:checked').each(function(i,e) { 
				 chkValues += $(this).val() + ",";
				 subTypVal.push($(this).siblings('input:hidden').val());
				 nextTD.push($(this).parent().siblings('td').html());
		});
		chkValues = chkValues.substring(0, chkValues.length - 1);
		$("#postCollLabTest").val(chkValues);
		displayLabTest(subTypVal,nextTD);
		closePopup();
	}
	function displayLabTest(subTypVal,nextTD){
		var hiddenVal = $("#postCollLabTest").val();
		var splitVal = (hiddenVal.split(","));
		var listValue;
		var pkValue;
		var rHtml="<tr>";
		for(s=0;s<splitVal.length;s++){
			
			$("#tempLabTest option").each(function(i){
				listValue = $(this).val();
				pkValue = (listValue.split("~-~"));
			if(pkValue[0] == "" || pkValue[0] == undefined){
				return;
			}else if(pkValue[0]== splitVal[s]){
						rHtml+="<td class=\"tablewapper\">";
						rHtml+="<input type=\"hidden\" name='P"+pkValue[1]+"_id' id='P"+pkValue[1]+"_id' value=\"0\"><input type=\"hidden\" name=\"questionsCode\" id=\"questionsCode\" value='P"+pkValue[1]+"'/>";
						rHtml+="<input type=\"checkbox\" name='P"+pkValue[1]+"' id='P"+pkValue[1]+"' checked=\"checked\" value='1'/>&nbsp;"+$(this).text()+"</td>";
						
						rHtml+="<td class=\"tablewapper1\"><input type=\"hidden\" name='P"+pkValue[1]+"ref_id' id='P"+pkValue[1]+"ref_id' value=\"0\">";
						rHtml+="<input type=\"hidden\" name=\"questionsCode\" id=\"questionsCode\" value='P"+pkValue[1]+"ref'/>";
						rHtml+="Ref #:&nbsp;<input type='text' id='P"+pkValue[1]+"ref' name='P"+pkValue[1]+"ref'/></td></tr>";
					}
			});
		}
		rHtml+="</tr>";
			$('#productQCTable > tbody > tr:eq(0)').before(rHtml);
	}
 var checknurseflag=false;
 function checknurseid(fieldId){
		 var nurseentered=$('input[name="'+fieldId+'"]').val();
		 var url="verifyCustomerId";
			 var jsonData = "{customerId:'"+nurseentered+"'}";
			 var response = jsonDataCall(url,"jsonData="+jsonData);
		 if(response.verifyStatus=="true"){
	    	checknurseflag=true;
		 }
	}
 function productLabelVerification(fieldId){
	/*//?BB checknurseid(fieldId);
	if(checknurseflag==true){
		//showPopUp("open","labelverified","Label Verfication","850","520");
		addNewPopup("Label Verification","labelverified","loadPostLabelVerf","700","1050");
	}else{
		alert("Please Verify Nurse ID");
	}
	*/
	 if($("#productLabelVerifiedNurse2").val().length>0){
			//showPopUp("open","labelverified","Label Verfication","850","520");
			addNewPopup("Label Verification","labelverified","loadPostLabelVerf","700","1050");
		}else{
			alert("Please Verify Nurse ID");
		}
 }
 function plasmaLabelVerification(fieldId){
		/*//?BB checknurseid(fieldId);
		if(checknurseflag==true){
			//showPopUp("open","labelverified","Label Verfication","850","520");
			 addNewPopup("Plasma Label Verification","plasmaLabelVerification","loadPostPlasmaLabelVerf","700","1050");
		}else{
			alert("Please Verify Nurse ID");
		}
		*/
		if($("#plasmaLabelVerifiedNurse2").val().length>0){
			//showPopUp("open","labelverified","Label Verfication","850","520");
			 addNewPopup("Plasma Label Verification","plasmaLabelVerification","loadPostPlasmaLabelVerf","700","1050");
		}else{
			alert("Please Verify Nurse ID");
		}
	 }
 /* function showAnticogulants(){
 	showPopUp('open','anticoagulants','Anticogulants and Additives','850','550');
 } 

  function saveAnticoagulants(){
		var rowData = "";
		var donorVal=$("#donor").val();
		var donorProcedure=$("#donorProcedure").val();
		 $("#anticoagulantsTable").find("#antigulantsName").each(function (row){
			 rowHtml =  $(this).closest("tr");	 
			antiname = "";
			antilot="";
			antiexpdate="";
			antivolume="";
			antiadmdate="";
			antiadmtime="";
			anticomments="";
			antiname=$(rowHtml).find("#antigulantsName").val();
			antiid=$(rowHtml).find("#antigulants").val()
		 		antilot=$(rowHtml).find("#antigulantsLot").val();
				antiexpdate=$(rowHtml).find("#expiryDate").val();
		 		antivolume=$(rowHtml).find("#antigulantsVolume").val()
		 		antiadmdate=$(rowHtml).find("#antigulantsAdditionDate").val();
		 		antiadmtime=$(rowHtml).find("#antigulantsAdditionTime").val()
		 		anticomments=$(rowHtml).find("#antigulantsComments").val();
		 		
			 	  	rowData+="{antigulants:"+antiid+",antigulantsVolume:'"+antivolume+"',antigulantsName:'"+antiname+"',antigulantsLot:'"+antilot+"',expiryDate:'"+antiexpdate+"',antigulantsAdditionDate:'"+antiadmdate+"',antigulantsAdditionTime:'"+antiadmtime+"',antigulantsComments:'"+anticomments+"',donor:'"+donorVal+"',donorProcedure:'"+donorProcedure+"'},";
		 		
		 });
		 
		alert("row data " + rowData)
		 if(rowData.length >0){
	            rowData = rowData.substring(0,(rowData.length-1));
	        }
		
		var rowData1="";
		var adname;
		var adlot;
		var adexpdate;
		var advolume;
		var addiadmdate;
		var addiadmtime;
		var adcomments;
		$("#additiveTable").find("#additivesName").each(function (row){
			 rowHtml =  $(this).closest("tr");	 
			 	adname="";
				adlot="";
				adexpdate="";
				advolume="";
				addiadmdate="";
				addiadmtime="";
				adcomments="";
				adname=$(rowHtml).find("#additivesName").val();
				addid=$(rowHtml).find("#additives").val()
		 		adlot=$(rowHtml).find("#additivesLot").val();
				adexpdate=$(rowHtml).find("#additivesExpiryDate").val();
				advolume=$(rowHtml).find("#additivesVolume").val()
		 		addiadmdate=$(rowHtml).find("#additivesAdditionDate").val();
				addiadmtime=$(rowHtml).find("#additivesAdditionTime").val()
		 		adcomments=$(rowHtml).find("#additivesComments").val();
		 		
			 	  	rowData1+="{additives:"+addid+",additivesName:'"+adname+"',additivesLot:'"+adlot+"',additivesExpiryDate:'"+adexpdate+"',additivesVolume:'"+advolume+"',additivesAdditionDate:'"+addiadmdate+"',additivesAdditionTime:'"+addiadmtime+"',additivesComments:'"+adcomments+"',donor:'"+donorVal+"',donorProcedure:'"+donorProcedure+"'},";
		 		
		 });
		 
		//alert("row data " + rowData1)
		 if(rowData1.length >0){
	            rowData1 = rowData1.substring(0,(rowData1.length-1));
	        }
		
		
		 url = "saveAnticoagulants";
		 var jsonData = "jsonData={anticoagulantsData:[" + rowData + "],additivesData:["+ rowData1 + "]}";
	alert("jsonData====>"+jsonData);
	$('.progress-indicator').css('display', 'block');

	response = jsonDataCall(url, jsonData);
	constructAnticoagulantsTable(true,donorVal,donorProcedure);

	$('.progress-indicator').css('display', 'none');
	//alert("Data saved Successfully");
	stafaAlert('Data saved Successfully','');
}
  
 function deleteanticoagulantsData(){
		// alert("Delete Rows");
	    var donorVal=$("#donor").val();
		var donorProcedure=$("#donorProcedure").val();
		var yes=confirm(confirm_deleteMsg);
		if(!yes){
			return;
		}
		var deletedIds = ""
		$("#anticoagulantsTable #antigulants:checked").each(function (row){
			deletedIds += $(this).val() + ",";
		});
		if(deletedIds.length >0){
		  deletedIds = deletedIds.substr(0,deletedIds.length-1);
		}
		jsonData = "{deletedAnticoagulantsResult:'"+deletedIds+"'}";
		url="deleteAnticoagulantsData";
	    response = jsonDataCall(url,"jsonData="+jsonData);
		constructAnticoagulantsTable(true,donorVal,donorProcedure);
		
	}
function deleteadditivesData(){
		//alert("Delete Rows");
		var donorVal=$("#donor").val();
		var donorProcedure=$("#donorProcedure").val();
	    var yes=confirm(confirm_deleteMsg);
		if(!yes){
			return;
		}
		var deletedIds = ""
		$("#additiveTable #additives:checked").each(function (row){
			deletedIds += $(this).val() + ",";
		});
		if(deletedIds.length >0){
		  deletedIds = deletedIds.substr(0,deletedIds.length-1);
		}
		jsonData = "{deletedAdditivesResult:'"+deletedIds+"'}";
		url="deleteAdditivesData";
	    response = jsonDataCall(url,"jsonData="+jsonData);
	    constructAnticoagulantsTable(true,donorVal,donorProcedure);
	}
function editAnticoagulants(){
	 // alert("edit Rows");
	 var colObj;
	  $("#anticoagulantsTable tbody tr").each(function (row){
		 
		 if($(this).find("#antigulants").is(":checked")){
			 $(this).find("td").each(function (col){
				 if(col ==2 ){
					 if ($(this).find("#antigulantsName").val()== undefined){
						 tmpValue = $(this).text();
						$(this).html("<select class='select1'  id='antigulantsName' name='antigulantsName' onchange='javascript:loadDropdown(this,\"antigulantsLot\");'>"+$("#reagent_hidden").html() + "</select>");
						$(this).find("#reagentsname").val(tmpValue);
						colObj = $(this).find("#antigulantsName");
					 }
				 }else if(col ==3){
					 if ($(this).find("#antigulantsLot").val()== undefined){
						 tmpValue = $(this).text();
					 	$(this).html("<div><select class='select1' id='antigulantsLot' name='antigulantsLot' onchange='javascript:loadDropdown(this,\"expiryDate\");' ></select></div>");
					 	loadDropdown($(colObj),"antigulantsLot");
					 	$(this).find("#antigulantsLot").val(tmpValue);
					 	colObj = $(this).find("#antigulantsLot");
					 }
				 }else if(col ==4){
					 if ($(this).find("#expiryDate").val()== undefined){
						 tmpValue = $(this).text();
						 	$(this).html("<input type='text' class='dateEntry dateclass'id='expiryDate' name='expiryDate' value='"+$(this).text()+ "'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/>");
						 	$(this).find("#expiryDate").val(tmpValue);
						 //	colObj = $(this).find("#expiryDate");
						 }
				}else if(col ==5){
					 if ($(this).find("#antigulantsVolume").val()== undefined){
						 	tmpValue = $(this).text();
						 	$(this).html("<input type='text' id='antigulantsVolume' name='antigulantsVolume' value='"+$(this).text()+ "'/>");
						 	$(this).find("#antigulantsVolume").val(tmpValue);
						 	//colObj = $(this).find("#antigulantsVolume");
						 }
				}else if(col ==6){
					 if ($(this).find("#antigulantsAdditionDate").val()== undefined){
						 	tmpValue = $(this).text();
						 	$(this).html("<input type='text' id='antigulantsAdditionDate' name='antigulantsAdditionDate' class='dateEntry'value='"+$(this).text()+ "'/>");
						 	$(this).find("#antigulantsAdditionDate").val(tmpValue);
						 }
				}else if(col ==7){   
					 if ($(this).find("#antigulantsAdditionTime").val()== undefined){
						 	tmpValue = $(this).text();
						 	$(this).html("<input type='hidden' id='antigulantsAdditionTime' name='antigulantsAdditionTime' value='"+$(this).text()+ "'/><input type='text' id='tempantigulantsAdditionTime' name='tempantigulantsAdditionTime' class='antitimepicker' onchange='setanticoagulantstime()'/>");
						 	$(this).find("#antigulantsAdditionTime").val(tmpValue);
						 }
				}else if(col ==8){   
					 if ($(this).find("#antigulantsComments").val()== undefined){
						 	tmpValue = $(this).text();
						 	$(this).html("<input type='text' id='antigulantsComments' name='antigulantsComments' value='"+$(this).text()+ "'/>");
						 	$(this).find("#antigulantsComments").val(tmpValue);
						 }
				}
			 });
		 }
	  });
      $.uniform.restore('select');
      $("select").uniform();
      $(".dateEntry").datepicker({
			dateFormat: 'M dd, yy',
			changeMonth: true,
			changeYear: true, 
			yearRange: "c-50:c+45"
		});
	  $(".antitimepicker").timepicker({});
     
  } 
function editAdditives(){
	 // alert("edit Rows");
	 var colObj;
	  $("#additiveTable tbody tr").each(function (row){
		 
		 if($(this).find("#additives").is(":checked")){
			 $(this).find("td").each(function (col){
				 if(col ==2 ){
					 if ($(this).find("#additivesName").val()== undefined){
						 tmpValue = $(this).text();
						$(this).html("<select class='select1'  id='additivesName' name='additivesName' onchange='javascript:loadDropdown(this,\"additivesLot\");'>"+$("#reagent_hidden").html() + "</select>");
						$(this).find("#additivesName").val(tmpValue);
						colObj = $(this).find("#additivesName");
					 }
				 }else if(col ==3){
					 if ($(this).find("#additivesLot").val()== undefined){
						 tmpValue = $(this).text();
					 	$(this).html("<div><select class='select1' id='additivesLot' name='additivesLot' onchange='javascript:loadDropdown(this,\"additivesExpiryDate\");' ></select></div>");
					 	loadDropdown($(colObj),"additivesLot");
					 	$(this).find("#additivesLot").val(tmpValue);
					 	colObj = $(this).find("#additivesLot");
					 }
				 }else if(col ==4){
					 if ($(this).find("#additivesExpiryDate").val()== undefined){
						 tmpValue = $(this).text();
						 	$(this).html("<input type='text' class='dateEntry dateclass'id='additivesExpiryDate' name='additivesExpiryDate' value='"+$(this).text()+ "'><input type='hidden' name='reagentsupplies' id='reagentsupplies' value='0'/>");
						 	$(this).find("#additivesExpiryDate").val(tmpValue);
						 }
				}else if(col ==5){
					 if ($(this).find("#additivesVolume").val()== undefined){
						 	tmpValue = $(this).text();
						 	$(this).html("<input type='text' id='additivesVolume' name='additivesVolume' value='"+$(this).text()+ "'/>");
						 	$(this).find("#additivesVolume").val(tmpValue);
						 }
				}else if(col ==6){
					 if ($(this).find("#additivesAdditionDate").val()== undefined){
						 	tmpValue = $(this).text();
						 	$(this).html("<input type='text' id='additivesAdditionDate' name='additivesAdditionDate' class='dateEntry' value='"+$(this).text()+ "'/>");
						 	$(this).find("#additivesAdditionDate").val(tmpValue);
						 }
				}else if(col ==7){   
					 if ($(this).find("#additivesAdditionTime").val()== undefined){
						 	tmpValue = $(this).text();
						 	$(this).html("<input type='hidden' id='additivesAdditionTime' name='additivesAdditionTime' value='"+$(this).text()+ "'/><input type='text' id='tempadditivesAdditionTime' name='tempadditivesAdditionTime' class='antitimepicker' onchange='setadditivetime()'/>");
						 	$(this).find("#additivesAdditionTime").val(tmpValue);
						 }
				}else if(col ==8){   
					 if ($(this).find("#additivesComments").val()== undefined){
						 	tmpValue = $(this).text();
						 	$(this).html("<input type='text' id='additivesComments' name='additivesComments' value='"+$(this).text()+ "'/>");
						 	$(this).find("#additivesComments").val(tmpValue);
						 }
				}
			 });
		 }
	  });
     $.uniform.restore('select');
     $("select").uniform();
     $(".dateEntry").datepicker({
			dateFormat: 'M dd, yy',
			changeMonth: true,
			changeYear: true, 
			yearRange: "c-50:c+45"
		});
	 $(".antitimepicker").timepicker({});
 }
 function closeAnticoagulants(){
		try{
	    	showPopUp('close','anticoagulants','Anticogulants and Additives','850','550');
	  	}catch(err){
			alert("err" + err);
		}
	} */
</script>

<div  id="slidecontrol" style="display:none;">
	<ul class="ulstyle">
	<span>
		<li class="listyle print_label"><img src = "images/icons/Bar_Code_32.png"  onclick="loadBarcodePage();" style="width:30px;height:30px;cursor:pointer;"/></li>
		<li class="listyle print_label" ><b><s:text name="stafa.label.label"/></b> </li>
	</span>
	<span>
		<li class="listyle notes_icon"  style="margin-left:17%"><img src = "images/icons/notes.png" onclick="notes(donormrn,'donor',this);"  style="width:30px;height:30px;cursor:pointer;"/><sup style="margin-left:2%"><span class='subspan'></span></sup></li>
		<li class="listyle notes_icon"><b><s:text name="stafa.label.notes"/></b> </li>
	</span>
	<span>
		<li class="listyle print_label"><img src = "images/icons/labresults.png"  style="width:30px;height:30px;cursor:pointer;" onclick='addNewPopup("Complete Lab Result","labResults","loadLabResult","700","1050");'/></li>
		<li class="listyle print_label" ><b><s:text name="stafa.label.labresult"/></b> </li>
	</span>
	<span>
		<li class="listyle medication_icon"><img src = "images/icons/capsule1.png" onclick="medicationpopup(donormrn,donorValue,donorplannedprocedure);" style="width:30px;height:30px;cursor:pointer;"/></li>
		<li class="listyle medication_icon"><b><s:text name="stafa.label.medication"/></b></li>
	</span>
	<span>
			<li class="listyle transfer_icon"><img src = "images/icons/transfer.png" onclick="taskAction(donorplannedprocedure,'transfer')"  style="width:30px;height:30px;cursor:pointer;"/></li>
			<li class="listyle transfer_icon" id="transfer_icon"><b><s:text name="stafa.label.transfer"/></b></li>
			
		</span>
	<span>
			<li class="listyle terminate_icon"><img src = "images/icons/terminated.png"  onclick="taskAction(donorplannedprocedure,'terminate')" style="width:30px;height:30px;cursor:pointer;"/></li>
			<li class="listyle terminate_icon"><b><s:text name="stafa.label.terminate"/></b></li>
	</span>
		<%-- <li class="listyle complete_icon"><b><s:text name="stafa.label.complete"/></b></li>
		<li class="listyle complete_icon"><img src = "images/icons/task_completed_icon.PNG"  style="width:30px;height:30px;"/></li> --%>
	</ul>
</div>
<div id="nurse_slidewidget" style="display:none;">
				
<!-- Patient Info start-->
			<div class="column">
				<div  class="portlet">
					<div class="portlet-header "><s:text name="stafa.label.donorinformation"/></div>
						<div class="portlet-content">
							<table width="100%" border="0">
							  <tr>
								<td width="50%"><s:text name="stafa.label.donorid"/></td>
								<td width="50%" id="slide_donorId" name="slide_donorId"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.name"/></td>
								<td id="slide_donorName" name="slide_donorName"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.dob"/></td>
								<td id="slide_donorDOB" name="slide_donorDOB"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.donoraborh"/></td>
								<td id="slide_donorABORH" name="slide_donorABORH"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.procedure"/></td>
								<td id="slide_procedure" name="slide_procedure"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.donation"/></td>
								<td id="slide_donation" name="slide_donation"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.weights"/></td>
								<td id="slide_donorWeight" name="slide_donorWeight"></td>
							  </tr>
							  <tr>
								<td>Height</td>
								<td id="slide_donorHeight" name="slide_donorHeight"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.targetvolume"/></td>
								<td id="slide_targetVol" name="slide_targetVol"></td>
							  </tr>
							</table>
						</div>
				</div>
			</div>
				
<!-- Patient Info-->

<!-- Lab Results start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header "><s:text name="stafa.label.donorlab"/></div>
						<div class="portlet-content">
							<table width="100%" border="0">
								<tr>
									<td width="50%"><s:text name="stafa.label.idms"/></td>
									<td width="50%" id="idms"></td>
								</tr>
								<tr>
									<td width="50%"><s:text name="stafa.label.westnile"/></td>
									<td width="50%" id="westnile"> </td>
								</tr>
								<tr>
									<td width="50%"><span class="cd34"><s:text name="stafa.label.donorcd34"/></span>
									<span class="cd3"><s:text name="stafa.label.donorcd3"/></span></td>
									<td width="50%" id="donorcd34"> </td>
								</tr>
							</table>
						</div>
				</div>
				</div>
				
<!-- Lab Results-->
			<div class="column">
					<div class="portlet">
						<div class="portlet-header "><s:text name="stafa.label.productlab"/></div>
							<div class="portlet-content">
							<table width="100%" border="0">
								<tr>
									<td width="50%">
									<span class="cd34"><s:text name="stafa.label.cd34_day1"/></span>
									<span class="cd3"><s:text name="stafa.label.cd3_day1"/></span>
									</td>
									<td width="50%" id="cd34_day1"></td>
								</tr>
								<tr>
									<td width="50%">
									<span class="cd34"><s:text name="stafa.label.cum_CD34"/></span>
									<span class="cd3"><s:text name="stafa.label.cum_CD3"/></span>
									</td>
									<td width="50%" id="cum_CD34"></td>
								</tr>
							</table>
							</div>
					</div>
				</div>
<!-- Recipient Info start-->
				<div class="column">
					<div class="portlet">
						<div class="portlet-header "><s:text name="stafa.label.recipientinformation"/></div>
							<div class="portlet-content">
							<table width="100%" border="0">
							<tr>
								<td width="50%"><s:text name="stafa.label.recipientmrn"/></td>
								<td width="50%" id="recipient_id"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.name"/></td>
								<td id="recipient_name"></td>
							  </tr>
							  <tr>
								<td><s:text name="stafa.label.dob"/></td>
								<td id="recipient_dob"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.aborh"/></td>
								<td id="recipient_aborh"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.diagnosis"/></td>
								<td id="recipient_diagnosis"></td>
							  </tr>
							   <tr>
								<td><s:text name="stafa.label.weights"/></td>
								<td id="recipient_weight"></td>
							  </tr>
							  </table>
							</div>
							 
					</div>
				</div>
				
<!--Recipient Info end-->
				</div>
<div class="cleaner"></div>
<div class="hidden">
	<select name="tmpSelect" id="tmpSelect" ><option value="">Select</option></select>	
  <s:hidden id="donor" name="donor" value="%{donor}"/>

  
			<select  name="equipment_hidden" id="equipment_hidden" >
				<option value="">Select</option>
				<s:iterator value="equipmentList" var="rowVal" status="row"> 
					<option value='<s:property value="#rowVal"/>'><s:property value="#rowVal"/> </option>
				</s:iterator> 		
			</select>
			
			<select name="reagent_hidden" id="reagent_hidden" >
				<option value="">Select</option>
			<s:iterator value="reagentsnameList" var="rowVal" status="row">
				<option value='<s:property value="#rowVal"/>'> <s:property value="#rowVal"/> </option>
			</s:iterator> 		
			</select>
		
</div>	
<form action="#" id="postCollectionForm" onsubmit="return avoidFormSubmitting();">
<input type="hidden" id="scanType" name="scanType" value=""/>
<s:hidden id="donor" name="donor" value="%{donor}"/>
<s:hidden id="collectPlasma" name="collectPlasma" value="%{collectPlasma}"/>
<s:hidden id="donorProcedure" name="donorProcedure" value="%{donorProcedure}"/>
<s:hidden id="preparation" name="preparation" value="0"/>
<s:hidden id="aphCollection" name="aphCollection" value="0"/>
<s:hidden id="postCollection" name="postCollection" value="0"/>
<s:hidden id="productId" name="productId" />
<s:hidden id="postCollLabTest" name="postCollLabTest" value=""/>
<s:hidden id="donorId" name="donorId" value="%{donor}"/>
<%-- 
<s:textfield id="donor" name="donor" value="%{donor}"/>
<s:textfield   id="donorProcedure" name="donorProcedure" value="%{donorProcedure}"/>
<s:textfield   id="aphCollection" name="aphCollection" value="0"/>
<s:textfield   id="postCollection" name="postCollection" value="0"/>
<s:textfield   id="refDonor" name="refDonor"/>
<s:textfield   id="productId" name="productId" />
<s:textfield   id="postCollLabTest" name="postCollLabTest" value=""/> 
 --%>
<span class='hidden'><s:select id="tempLabTest" style='display:none;' name="tempLabTest" list="postCollectionLabList" listKey="pkCodelst+'~-~'+subType+'~-~'+type" listValue="description"  headerKey="" headerValue="Select"/></span>
<input type="hidden" name="flowNextFlag" id="flowNextFlag" value=""/>
<input type="hidden" name="productFlag" id="productFlag" value="false"/>
<input type="hidden" name="scanFlag" id="scanFlag" value="false"/>
<div class="column">       
	<div  class="portlet">
	    <div class="portlet-header ">Post-Collection Information</div>
	    <div class="portlet-content" >
			<table  id="postcollinformation" cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Blood Warmer Ending Temp. (C)</td>
					<td class="tablewapper1"><input type="hidden" name="bloodwarmertemp_id" id="bloodwarmertemp_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="bloodwarmertemp" /><input type="text" id="bloodwarmertemp" name="bloodwarmertemp" class="numbersOnly"/></td>
					<td class="tablewapper">Activase 2mg/2mL Instilled</td>
					<td class="tablewapper1"><input type="hidden" name="activaseinstilled_id" id="activaseinstilled_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="activaseinstilled" /><input type="radio" id="activaseinstilled_yes" name="activaseinstilled" value="1"/>Yes&nbsp;<input type="radio" id="activaseinstilled_no" name="activaseinstilled" value="0" />No</td>
					
				</tr>
				<tr>
					<td class="tablewapper">Catheter Flush</td>
					<td class="tablewapper1"><input type="hidden" name="catheterflush_id" id="catheterflush_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="catheterflush" /><input type="radio" id="catheterflush_yes" name="catheterflush" value="1"/>Yes&nbsp;<input type="radio" name="catheterflush" id="catheterflush_no" value="0"/>No</td>
					<td class="tablewapper">Injection Ports Changed</td>
					<td class="tablewapper1"><input type="hidden" name="injectionport_id" id="injectionport_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="injectionport" /><s:select id="injectionport"  name="injectionport" list="lstInjectionPort" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
				
                </tr>
                <tr>
					<td class="tablewapper">Heparin 1000 units instilled</td>
					<td class="tablewapper1"><input type="hidden" name="heparininstilled_id" id="heparininstilled_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="heparininstilled" /><input type="radio" id="heparininstilled_yes" name="heparininstilled" value="1"/>Yes&nbsp;<input type="radio" id="heparininstilled_no" name="heparininstilled" value="0"/>No</td>
					<td class="tablewapper">Dressing Changed</td>
					<td class="tablewapper1"><input type="hidden" name="dressingchanged_id" id="dressingchanged_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="dressingchanged" /><input type="radio" id="dressingchanged_yes" name="dressingchanged" value="1"/>Yes&nbsp;<input type="radio" id="dressingchanged_no" name="dressingchanged" value="0"/>No&nbsp;<input type="radio" name="dressingchanged" id="dressingchanged_na" value="-1"/>N/A</td>
					
				</tr>
				<tr>
					<td class="tablewapper">Heparin (mL/Lumen)</td>
					<td class="tablewapper1"><input type="hidden" name="heparinlumen_id" id="heparinlumen_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="heparinlumen" /><input type="text" id="heparinlumen" name="heparinlumen" class="numbersOnly"/></td>
					<td class="tablewapper">Neupogen Admin. to Donor</td>
					<td class="tablewapper1"><input type="hidden" name="neupogen_id" id="neupogen_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="neupogen" /><input type="radio" id="neupogen_yes" name="neupogen" value="1"/>Yes&nbsp;<input type="radio" id="neupogen_no" name="neupogen" value="0"/>No</td>
				</tr>
	            <tr>
	                 <td  colspan="3"style="padding-left:5%" width="75%" height="40px">Donor Advised of Potential Apheresis Procedure Low Platelet Issues</td>
	                 <td class="tablewapper1"><input type="hidden" name="donorpotential_id" id="donorpotential_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="donorpotential" /><input type="radio" id="donorpotential_yes" name="donorpotential" value="1"/>Yes&nbsp;<input type="radio" id="donorpotential_no" name="donorpotential" value="0"/>No</td>
	            </tr>
	       </table>
		
		</div>
	</div>
</div>


<div class="cleaner"></div>

<div class="column">       
	<div  class="portlet">
    	<div class="portlet-header ">Post Assessment</div>
		<div class="portlet-content">
			<table id="postassessment" cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Blood Pressure (mmHg)</td>
					<td class="tablewapper1"><input type="hidden" name="bloodpressure_id" id="bloodpressure_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="bloodpressure" /><input type="text" id="bloodpressure" name="bloodpressure" class="numbersOnly" style="width:35% !important"/>
					<input type="hidden" name="bloodpressure1_id" id="bloodpressure1_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="bloodpressure1" /><input type="text" id="bloodpressure1" name="bloodpressure1" class="numbersOnly" style="width:35% !important"/>
					</td>
					<td class="tablewapper">Temp.(F)</td>
					<td class="tablewapper1"><input type="hidden" name="bloodtemp_id" id="bloodtemp_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="bloodtemp" /><input type="text" id="bloodtemp" name="bloodtemp" class="numbersOnly" /></td>
					
				</tr>
				<tr>
					<td class="tablewapper">Heart Rate (bpm)</td>
					<td class="tablewapper1"><input type="hidden" name="heartrate_id" id="heartrate_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="heartrate" /><input type="text" id="heartrate" name="heartrate" class="numbersOnly" /></td>
					<td class="tablewapper">Peripheral Access Status</td>
					<td class="tablewapper1"><input type="hidden" name="peripheralaccess_id" id="peripheralaccess_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="peripheralaccess"/><s:select id="peripheralaccess"  name="peripheralaccess" list="lstPeripheralAccess" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
                </tr>
                <tr>
					<td class="tablewapper">CVC Dressing/Site Appearance<label class="mandatory">*</label></td>
					<td class="tablewapper1"><div class="checkboxrequired" id="cvcrequired" style="display:none;">CVC is Required</div><input type="hidden" name="cvcdry_id" id="cvcdry_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="cvcdry"/><input type="checkbox" id="cvcdry" name="cvcdry" value="1" onclick="checkCVC()"/>Dry&nbsp;<input type="hidden" name="cvcclear_id" id="cvcclear_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="cvcclear"/><input type="checkbox" id="cvcclear" name="cvcclear" value="1" onclick="checkCVC()"/>Clean&nbsp;<input type="hidden" name="cvcintact_id" id="cvcintact_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="cvcintact"/><input type="checkbox" id="cvcintact" name="cvcintact" value="1" onclick="checkCVC()"/>Intact&nbsp;<input type="hidden" name="cvcsoiled_id" id="cvcsoiled_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="cvcsoiled"/><input type="checkbox" name="cvcsoiled" id="cvcsoiled" value="1" onclick="checkCVC()"/>Soiled&nbsp;<input type="hidden" name="cvcna_id" id="cvcna_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="cvcna"/><input type="checkbox" name="cvcna" id="cvcna" value="1" onclick="checkCVC()"/>N/A</td>
					<td class="tablewapper">Comments</td>
					<td class="tablewapper1"><input type="hidden" name="cvccomments_id" id="cvccomments_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="cvccomments"/><input type="text" id="cvccomments" name="cvccomments"/></td>
				</tr>
				 <tr>
					<td class="tablewapper">Donor Status<label class="mandatory">*</label></td>
					<!-- <td width="26%"><input type="hidden" name="donornone_id" id="donornone_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="donornone"/><input type="checkbox" id="donornone" name="donornone" value="1"/>No Complains&nbsp;<input type="hidden" name="donornause_id" id="donornause_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="donornause"/><input type="checkbox" id="donornause" name="donornause" value="1"/>Nausea</td> -->
					<td class="tablewapper1"><div class="checkboxrequired" id="donorrequired" style="display:none;">Donor Status is Required</div><input type="hidden" name="donornone_id" id="donornone_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="donornone"/><input type="checkbox" id="donornone" name="donornone" value="1" onclick="checkDonorStatus()"/>No Complaints&nbsp;<input type="hidden" name="donorpain_id" id="donorpain_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="donorpain"/><input type="checkbox" id="donorpain" name="donorpain" value="1" onclick="checkDonorStatus()"/>Pain&nbsp;<input type="hidden" name="donornause_id" id="donornause_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="donornause"/><input type="checkbox" id="donornause" name="donornause" value="1" onclick="checkDonorStatus()"/>Nausea&nbsp;<input type="hidden" name="donordiarrhea_id" id="donordiarrhea_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="donordiarrhea"/><input type="checkbox" id="donordiarrhea" name="donordiarrhea" value="1" onclick="checkDonorStatus()"/>Diarrhea&nbsp;<input type="hidden" name="donorfebrile_id" id="donorfebrile_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="donorfebrile"/><input type="checkbox" id="donorfebrile" name="donorfebrile" value="1" onclick="checkDonorStatus()"/>Febrile</td>
					<td class="tablewapper">Comments</td>
					<td class="tablewapper1"><input type="hidden" name="donorcomments_id" id="donorcomments_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="donorcomments"/><input type="text" id="donorcomments" name="donorcomments"/></td>
				</tr>
				<tr>
					<td class="tablewapper">POC Discussed with Donor</td>
					<td class="tablewapper1"><input type="hidden" name="pocdonor_id" id="pocdonor_id" value="0"><input type="hidden" name="questionsCode" id="questionsCode" value="pocdonor"/><input type="radio" name="pocdonor" id="pocdonor_yes" value="1"/>Yes&nbsp;<input type="radio" id="pocdonor_no" name="pocdonor" value="1"/>No</td>
				</tr>
			</table>
        </div>
    </div>
</div>

<div class="cleaner"></div>

<div class="column">       
	<div  class="portlet">
	<div class="portlet-header">Product Information</div>
    	<div class="portlet-content">
	    	<div id="button_wrapper">
				<div id="divProductScan" style="margin-left:40%;">
       				<input class="scanboxSearch barcodeSearch"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;" onkeyup="setScanFlag('PRODUCT');performScanData(event,this);" />
					<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" onfocus="changeType(this)"/>
	 			</div>
			</div> 
			<table id="productInformation" cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Product ID</td>
					<td class="tablewapper1"><s:textfield id="specimen" name="specimen" value="" /></td>
					<td class="tablewapper">Weight Verified: Nurse 2</td>
					<td class="tablewapper1"><input type="hidden" id="weightVerifiedN2" name="weightVerifiedN2" /><input type="text" onfocus="showVerifyUserPopup(this);" name="weightVerifiedNurse2" id="weightVerifiedNurse2" placeholder="Enter/Scan ID"/>&nbsp;<input type="checkbox" id="specWeightVerifiedNurseNA" name="specWeightVerifiedNurseNA"  onclick="setValueNA('specWeightVerifiedNurseNA','weightVerifiedNurse2')"/>N/A</td>
				</tr>
				<tr>
					<td class="tablewapper">Product Type</td>
					<td class="tablewapper1"><s:select id="productType"  name="productType" list="lstProductType" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select"/></td>
					<td class="tablewapper">Product Scale Volume (mL)</td>
					<td class="tablewapper1"><input type="text" id="productScaleVolume" name="productScaleVolume" /></td>
				</tr>
				<tr>
					<td class="tablewapper">Collection Date</td>
					<td class="tablewapper1"><input type="text" id="collectionDate" name="collectionDate" class="dateEntry dateclass"/></td>
					<td class="tablewapper"><a href="#" onclick="anticoagulantsPopup(donorValue,donorplannedprocedure);">Anticoagulants & Additives</a></td>
					<td class="tablewapper1"><s:radio name="specAnticoagulantsAdditives" id="specAnticoagulantsAdditives" list="#{'1':'Yes','0':'No'}"/><a href="#" onclick="anticoagulantsPopup(donorValue,donorplannedprocedure)" id="anticogulants_view" style="color:blue;display:none;">&nbsp;View</a></td>
	               </tr>
	               <tr>
					<td class="tablewapper">Collection Start Time</td>
					<td class="tablewapper1"><s:hidden  id="collectionStartTime" name="collectionStartTime" Class="" /><s:textfield id="tempCollectionStartTime" name="tempCollectionStartTime" Class="" onchange="setCollectionStartTime();"/></td>
					<td class="tablewapper">Product Appearance OK</td>
					<td class="tablewapper1"><s:radio name="specProductAppearanceOk" id="specProductAppearanceOk" list="#{'1':'Yes','0':'No'}"/></td>
				</tr>
				<!-- <tr>
					<td class="tablewapper">Collection Start Time</td>
					<td class="tablewapper1"><span type="text" class='jclock' id="collectionStartTime" style="width:100px; font-size:1.0em;"></span>&nbsp;<span id="timeZone"></span></td>
					<td class="tablewapper">Product Container Intact</td>
					<td class="tablewapper1"><input type="radio"/>Yes&nbsp;<input type="radio"/>No</td>
				</tr> -->
				<tr>
					<td class="tablewapper">Collection End Time</td>
					<td class="tablewapper1"><s:hidden  id="collectionEndTime" name="collectionEndTime" Class="" /><s:textfield id="tempCollectionEndTime" name="tempCollectionEndTime" Class="" onchange="setCollectionEndTime();"/></td>
					<!-- <td class="tablewapper"><span id="spanProductLink" class="hidden"><a href="#" onclick="plasmaLabelVerification();">Product Label</a></span><span id="spanProduct">Product Label</span> Verified: Nurse2</td>
					<td class="tablewapper1">
					<input type="hidden" name="productLabelVerified" id="productLabelVerified"/>
					<input type="text" name="specProductLVN2" id="specProductLVN2"/>&nbsp;
					<input type="checkbox" id="productLabelVerifiedNA" name="productLabelVerifiedNA"  onclick="setValueNA('productLabelVerifiedNA','specProductLVN2')"/>N/A
					</td> -->
					<td class="tablewapper"><a href="#" onclick="productLabelVerification('specProductLVN2');">Product Label</a> Verified: Nurse2</td>
					<td class="tablewapper1">
					<input type="hidden" name="productLabelVerified" id="productLabelVerified"/>
					<input type="text" onfocus="showVerifyUserPopup(this);" name="productLabelVerifiedNurse2" id="productLabelVerifiedNurse2"/>&nbsp;
					<input type="checkbox" id="productLabelVerifiedNA" name="productLabelVerifiedNA"  onclick="setValueNA('productLabelVerifiedNA','productLabelVerifiedNurse2')"/>N/A
					</td>
				</tr>
				<tr>
					<td class="tablewapper">Product Bag Tared</td>
					<td class="tablewapper1"><s:checkbox id="specProductBagTarred" name="specProductBagTarred" fieldValue="" onclick="setvalues('specProductBagTarred')"/>&nbsp;Yes</td>
					<td class="tablewapper">Mid-Count WBC</td>
					<td class="tablewapper1"><input type="text" id="midCountWbc" name="midCountWbc"/></td>
				</tr>
				<tr>
					<td class="tablewapper">Product Weight: Nurse 1 (g)</td>
					<td class="tablewapper1"><input type="text" id="specProductWeightNurse1" name="specProductWeightNurse1"/></td>
					<td class="tablewapper">Quarantine Product</td>
					<td class="tablewapper1"><s:radio name="specQuarantineProduct" id="specQuarantineProduct" list="#{'1':'Yes','0':'No'}"/></td>
				</tr>
			</table>
		</div>
    </div>
</div>

<div class="cleaner"></div>
   
<div class="column">       
	<div  class="portlet" id="plasmaInfo">
	<div class="portlet-header ">Plasma Information</div>
		<div class="portlet-content">
		   <div id="button_wrapper">
                <div id="divPlasmaScan" style="margin-left:40%;">
			       	<input class="scanboxSearch recipientscan multiple"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Plasma ID"  style="width:210px;" onkeyup="setScanFlag('PLASMA');performScanData(event,this)" />
					<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Plasma ID"  style="width:210px;display:none;" onfocus="changeType(this)" />
				</div>
            </div>
			<table id="plasmaInformation" cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Plasma ID</td>
					<td class="tablewapper1"><input type="text" id="plasmaId" name="plasmaId"/></td>
					<td class="tablewapper">PlasmaVolume (mL)</td>
					<td class="tablewapper1"><input type="text" id="proplasmaVolume" name="proplasmaVolume"/></td>
				</tr>
				<tr>
					<td class="tablewapper">Plasma Collection Date</td>
					<td class="tablewapper1"><input type="text" id="plasmaCollectionDate" name="plasmaCollectionDate" class="dateEntry dateclass"/></td>
					<td class="tablewapper">Plasma Appearance OK</td>
					<td class="tablewapper1"><s:radio name="specPlasmaAppearanceOk" id="specPlasmaAppearanceOk" list="#{'1':'Yes','0':'No'}"/></td>
	               </tr>
	               <tr>
					<td class="tablewapper">Plasma Collection Start Time</td>
					<td class="tablewapper1"><s:hidden  id="plasmaCollectionStartTime" name="plasmaCollectionStartTime" Class="" /><s:textfield id="tempPlasmaCollStartTime" name="tempPlasmaCollStartTime" Class="" onchange="setPlasmaCollStartTime();"/></td>
					<td class="tablewapper"><a href="#" onclick="plasmaLabelVerification('plasmaVerify');">Plasma Label</a> Verified: Nurse 2</td>
					<td class="tablewapper1"><input type="hidden" id="plasmaLabelVerified" name="plasmaLabelVerified" /><input type="text" onfocus="showVerifyUserPopup(this);" id="plasmaLabelVerifiedNurse2" name="plasmaLabelVerifiedNurse2"/>&nbsp;<input type="checkbox" id="plasmaLabelVerifiedNA" name="plasmaLabelVerifiedNA"  onclick="setValueNA('plasmaLabelVerifiedNA','plasmaLabelVerifiedNurse2')"/>N/A</td>
				</tr>
			</table>
		</div>   
	</div>
</div>

<div class="cleaner"></div>

<div id='productQC' class="column">       
	<div  class="portlet">
	<div class="portlet-header ">Product Lab Test</div>
		<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%" id="productQCTable">
				<%-- <tr>
					<td class="tablewapper"><s:checkbox id="scpp" name="scpp" fieldValue="" onclick="setvalues('scpp')"/>&nbsp;SCPP</td>
					<td class="tablewapper1">Ref #:&nbsp;<input type="text" name="scppRefNumber" id="scppRefNumber"/>&nbsp;</td>
					<td class="tablewapper"><s:checkbox id="cd3" name="cd3" fieldValue="" onclick="setvalues('cd3')"/>&nbsp;CD3</td>
					<td class="tablewapper1">Ref #:&nbsp;<input type="text" name="cd3RefNumber" id="cd3RefNumber"/></td>
				</tr>
				<tr>
					<td class="tablewapper"><s:checkbox id="typeCheck" name="typeCheck" fieldValue="" onclick="setvalues('typeCheck')"/>&nbsp;Type Check</td>
				</tr> --%>
			</table>
			<!-- <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper"><img onclick="addLab();" style="width:16;height:16;cursor:pointer;" src="images/icons/addnew.jpg">&nbsp;&nbsp;Add QC Test</td>
					<td width="23%" height="24px"><select><option>Select...</option></select></td>
					<td class="tablewapper"></td>
					<td class="tablewapper1"></td>
				</tr>
			</table> -->
		</div>
	</div>
</div>

<div class="cleaner"></div>

<div class="column">   
	<div  class="portlet">
	<div class="portlet-header notes">Post-Cleaning</div>
		<div class="portlet-content">    
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
	            <tr>
	                <td class="questionsAlign">Biohazard Waste Disposed of Post Procedure</td>
	                <td width="11%" height="40px"><s:checkbox id="biohazardWasteDisposed" name="biohazardWasteDisposed" fieldValue="" onclick="setvalues('biohazardWasteDisposed')"/>&nbsp;Yes</td>
	            </tr>
	            <tr>
	                <td class="questionsAlign">Equipment Cleaning Post Procedure</td>
	                <td width="11%" height="40px"><s:checkbox id="equipmentCleaning" name="equipmentCleaning" fieldValue="" onclick="setvalues('equipmentCleaning')"/>&nbsp;Yes</td>
	            </tr>
	        </table>
		</div>
	</div>
</div>

<%--  <div class="floatright_right">
    <jsp:include page="sidecontrols.jsp"></jsp:include>
 </div> --%>
 </form>
<div class="cleaner"></div>
 <form onsubmit="return avoidFormSubmitting()"> 
<div align="left" style="float:right;">
	<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
	    <tr>
	        <td><span class='nextContainer'>Next:
	        <select name="nextOption" id="nextOption" ></select>
	        <input type="password" style="width:80px;" size="5" value="" id="eSignNext" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
			<input type="button" value="Next" onclick="verifyeSign('eSignNext','next')"/></span></td>
	    </tr>
	</table>
</div>
</form>

<div id="labelverified" style="display:none;">
	<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
		<tr>
			<td class="isbtlabel">ISBT Label Attached/Affixed</td>
			<td><input type="radio" id="isbtlabel_yes" name="isbtlabel" value="1"/>Yes&nbsp;<input type="radio" name="isbtlabel" id="" value="0" />No</td>
			
		</tr>
		<tr class="isbtlabelshow" style="display:none;">
		
			<td align=""  style="padding-left:3%;">ISBT Label Complete and Legible</td>
			<td><input type="radio" id="" />Yes&nbsp;<input type="radio" id="" />No</td>
			
		</tr>	
		<tr>
			<td >Warning Label Attached/Affixed</td>
			<td><input type="radio" id="" name="warninglabelfixed"  value="1"/>Yes&nbsp;<input type="radio" id=""  name="warninglabelfixed" value="0"/>No&nbsp;<input type="radio" id="" />N/A</td>
			
		</tr>
		<tr class="warninglabel" style="display:none;">
		
			<td align="" style="padding-left:3%;">Warning:Reactive Test Results for</td>
			<td><input type="radio" id="warninglabelid" name="warninglabelid"/>Yes&nbsp;<input type="radio" name="warninglabelid" id="" />No&nbsp;<input type="radio" id="" />N/A</td>
			
		</tr>	
		<tr class="warninglabel" style="display:none;">
		
			<td align="" style="padding-left:3%;">Warning:Advise Patient of Communicable Disease Risk</td>
			<td><input type="radio" id="" />Yes&nbsp;<input type="radio" id="" />No&nbsp;<input type="radio" id="" />N/A</td>
			
		</tr>	
		<tr class="warninglabel" style="display:none;">
		
			<td align="" style="padding-left:3%;">Not Evaluated For Infectious Substances</td>
			<td><input type="radio" id="" />Yes&nbsp;<input type="radio" id="" />No&nbsp;<input type="radio" id="" />N/A</td>
			
		</tr>	
		<tr class="warninglabel" style="display:none;">
		
			<td align=""style="padding-left:3%;">Biohazard</td>
			<td><input type="radio" id="" />Yes&nbsp;<input type="radio" id="" />No&nbsp;<input type="radio" id="" />N/A</td>
			
		</tr>	
		<tr>
			<td>AUTO only Label Attached/Affixed</td>
			<td><input type="radio" id="" />Yes&nbsp;<input type="radio" id="" />No&nbsp;<input type="radio" id="" />N/A</td>
			
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
		
		<tr>
			<td>Comments</td>
			<td><textarea id="" rows="4" cols="50" > </textarea></td>
			
		</tr>
	</table>

  	<div align="right" style="float:right;">
		<form>
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td><input type="button" value="Save" onclick=""/></td>
					<td><input type="button" value="Cancel" onclick=""/></td>
				</tr>
			</table>
		</form>
	</div>  
</div>

<!-- <div id="addLabTest" class="model" title="addNew" style="display: none">
	<form action="#" id="addLabTestForm">
		<table cellpadding="0" cellspacing="0" border="0" id="addLabTable" class="display">
			<thead>
				<tr>
					<th><input type='checkbox'onclick="checkall('addLabTable',this)"/>Check All</th>
					<th>Code</th>
					<th>Description</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<div align='right' style='float:right;'>
			<table cellpadding='0' cellspacing='0' class='' align='' border='0'>
				<tr>
					<td><input type='button' onclick='saveLabTest();' value='Save'/></td>
					<td><input type='button' onclick='closePopup();' value='Cancel'/></td>
				</tr>
			</table>
		</div>
	</form>
</div> -->
<!-- <div id="anticoagulants" style="display:none;">
<form>

<div class="cleaner"></div> -->
<%-- 
<div class="column">
	<div  class="portlet">
	<div class="portlet-header notes"><s:text name="stafa.label.equipment"/></div>
		<div class="portlet-content" >
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewequipment(false);"/>&nbsp;&nbsp;<label  class="cursor" onclick="addnewequipment(false);"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteEquimentData()"/>&nbsp;&nbsp;<label class="cursor"  onclick="deleteEquimentData()"><b>Delete</b></label>&nbsp;</div></td>
			    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" onclick="editEquimentsRows()"/>&nbsp;&nbsp;<label  class="cursor" onclick="editEquimentsRows()"><b>Edit</b></label>&nbsp;</div></td>
				</tr>
			</table>
			<div style="overflow-y:auto;overflow-y:hidden;">
			<table cellpadding="0" cellspacing="0" border="0" id="equipmentTable" class="display">
			<thead>
				<tr>
					<th width="4%" id="equipmentTableColumn"></th>
					<th colspan='1'><s:text name="stafa.label.equipment"/></th>
					<th colspan='1'><s:text name="stafa.label.manufacturer"/></th>
					<th colspan='1'><s:text name="stafa.label.model"/></th>
					<th colspan='1'><s:text name="stafa.label.mhs"/></th>
					<th colspan='1'><s:text name="stafa.label.maintainance_Duedate"/></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			</table>
			</div>
		</div>
	</div>
</div>

<div class="cleaner"></div>

<div class="column">
		
	<div  class="portlet">
	<div class="portlet-header notes"><s:text name="stafa.label.regandandsupplies"/></div>
		<div class="portlet-content">
		
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewRegents(false)"/>&nbsp;&nbsp;<label  class="cursor" onclick="addnewRegents(false)"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteReagentsData()"/>&nbsp;&nbsp;<label class="cursor"  onclick="deleteReagentsData()"><b>Delete</b></label>&nbsp;</div></td>
			    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;"onclick="editReagentsRows()"/>&nbsp;&nbsp;<label  class="cursor" onclick="editReagentsRows()"><b>Edit</b></label>&nbsp;</div></td>
				</tr>
			</table>
			<div style="overflow-y:auto;overflow-y:hidden;">
			<table cellpadding="0" cellspacing="0" border="0" id="reagenTable" width="100%"class="display">
				<thead>
					<tr>
						<th width="4%" id="reagenColumn"></th>
						<th colspan="1"><s:text name="stafa.label.regandandsupplies"/></th>
						<th colspan="1"><s:text name="stafa.label.quantity"/></th>
						<th colspan="1"><s:text name="stafa.label.manufacturer"/></th>
						<th colspan="1"><s:text name="stafa.label.lot"/></th>
						<th colspan="1"><s:text name="stafa.label.expirationdate"/></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
			</div>
		</div>
	</div>
</div> --%>

<!-- <div class="cleaner"></div>

<div  class="portlet">
	<div class=" "><b>Anticoagulants</b></div>
		<div class="portlet-content" style="overflow-x:auto">
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewanticoagulants();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteanticoagulantsData()"/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
			    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" onclick="editAnticoagulants();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="anticoagulantsTable" class="display">
			<thead>
				<tr>
					<th width="4%" id="anticoagulantsColumn"></th>
					<th>Check all <input type="checkbox" id="checkall"/></th>
					<th colspan='1'>Name</th>
					<th colspan='1'>Lot #</th>
					<th colspan='1'>Exp.Date</th>
					<th colspan='1'>Volume</th>
					<th colspan='1'>Date of Addition</th>
					<th colspan='1'>Time of Addition</th>
					<th colspan='1'>Comment</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			</table>
		</div>
</div>
<br>
<br>
<br>
<br>
<div  class="portlet">
	<div class=" "><b>Other Additives</b></div>
		<div class="portlet-content" style="overflow-x:auto">
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewadditives();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" " onclick="deleteadditivesData()"/>&nbsp;&nbsp;<label class="cursor"  onclick="deleteadditivesData();"><b>Delete</b></label>&nbsp;</div></td>
			    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" onclick="editAdditives();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" id="additiveTable" width="100%" class="display">
			<thead>
				<tr>
					<th width="4%" id="additivesColumn"></th>
					<th>Check all <input type="checkbox" id="checkall"/></th>
					<th>Name</th>
					<th>Lot #</th>
					<th>Exp.Date</th>
					<th>Volume</th>
					<th>Date of Addition</th>
					<th>Time of Addition</th>
					<th>Comment</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			</table>
		</div>
</div> 
<div align="right" style="float:right;">
<table>

         <tr>
           <td width="" align="" >
           <input type="password" style="width:80px;" size="5" value="" id="postPrepEsign" placeholder="e-Sign"/>&nbsp;<input type="button" name="Save" onclick="verifyeSign('postPrepEsign','popup');" value="Save"/>&nbsp;<input type="button" name="Cancel" value="Cancel" onclick="closeAnticoagulants();"/>
           </td>
         </tr>
</table>
</div>
</form>

</div> -->
<!-- Barcode Starts -->		
<div  id ="mainbarcodediv" style="background-color:white; display:none; height:655px;overflow:auto;margin-top:1%;">  <!-- added by mohan j -->
<div id="maindiv"><div id="barimageTables" style="float:right;display:none; width:44%;height:40%;">
			<!-- From Template Start -->
<div id="barcodeMainDiv" >
		</div>
			<!-- From Template Ends -->
</div></div>
	
</div>
<!-- Barcode Ends -->
<script>

$(function() {
    $('.jclock').jclock();
    $( ".column" ).sortable().sortable({
        connectWith: ".column"
    });
   
   



    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

    $('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});   
});
</script>












