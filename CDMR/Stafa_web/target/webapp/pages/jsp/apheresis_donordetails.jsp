<%@ taglib prefix="s" uri="/struts-tags"%>
<script>
$(document).ready(function(){
	show_slidecontrol('sidecont');
	hide_slidewidgets();
var oTable1=$("#donorLabResultTable").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null
											             
											             ],
											             "sScrollX": "100%",
				 		 								 'sScrollXInner': '110%',
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#donorLabResultColumn").html($('.ColVis:eq(0)'));
												}
		});
			var oTable2=$("#productLabResultTable").dataTable({
												"sPaginationType": "full_numbers",
												"sDom": 'C<"clear">Rlfrtip',
												"aoColumns": [ { "bSortable":false},
												               null,
												               null,
												               null,
												               null,
												               null,
												               null,
												               null,
												               null,
												               null,
												               null,
												               null
												             ],
												             "sScrollX": "100%",
															'sScrollXInner': '110%',
													"oColVis": {
														"aiExclude": [ 0 ],
														"buttonText": "&nbsp;",
														"bRestore": true,
														"sAlign": "left"
													},
													"fnInitComplete": function () {
												        $("#productLabResultColumn").html($('.ColVis:eq(1)'));
													}
			});
			
			var oTable3=$("#documentreviewTable").dataTable({
				"sPaginationType": "full_numbers",
				"sDom": 'C<"clear">Rlfrtip',
				"aoColumns": [ { "bSortable":false},
				               null,
				               null,
				               null,
				               null
				             ],
				             "sScrollX": "100%",
							'sScrollXInner': '110%',
					"oColVis": {
						"aiExclude": [ 0 ],
						"buttonText": "&nbsp;",
						"bRestore": true,
						"sAlign": "left"
					},
					"fnInitComplete": function () {
				        $("#documentreviewColumn").html($('.ColVis:eq(2)'));
					}
				});
		

			$("select").uniform(); 

});

function add_donorLabResults(){
	var runningTabClass= $('#donorLabResultTable tbody tr td:nth-child(1)').attr("class");
	if(runningTabClass=="dataTables_empty"){
		$("#donorLabResultTable tbody").replaceWith("<tr>"+
								"<td><input type='checkbox' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"</tr>");
	}
	else{
		$("#donorLabResultTable tbody").prepend("<tr>"+
				"<td><input type='checkbox' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"</tr>");
		}

} 
function add_productLabResults(){
	var runningTabClass= $('#productLabResultTable tbody tr td:nth-child(1)').attr("class");
	if(runningTabClass=="dataTables_empty"){
		$("#productLabResultTable tbody").replaceWith("<tr>"+
								"<td><input type='checkbox' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"<td><input type='text' id=''/></td>"+
								"</tr>");
	}
	else{
		$("#productLabResultTable tbody").prepend("<tr>"+
				"<td><input type='checkbox' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"<td><input type='text' id=''/></td>"+
				"</tr>");
		}

} 

</script>
<div id="sidecont" style="display:none">
<div class="" >
<ul class="ulstyle">
<!-- <li class="listyle print_label" ><b>Label</b> </li>
<li class="listyle print_label"><img src = "images/icons/print_small.png"  style="width:30px;height:30px;"/></li> -->
<li class="listyle print_label" ><b>Calendar</b> </li>
<li class="listyle print_label"><img src = "images/icons/calendar.png"  style="width:30px;height:30px;"/></li>
<li class="listyle notes_icon"><b>Notes</b> </li>
<li class="listyle notes_icon"><img src = "images/icons/notes.png"  style="width:30px;height:30px;"/></li>

</ul>
</div>
</div>
<form>
<div class="column">       
	<div  class="portlet">
    	<div class="portlet-header ">Donor Details</div>
		<div class="portlet-content">
		<div id="button_wrapper">
				<div style="margin-left:40%;">
					<input class="scanboxSearch barcodeSearch"  type="text" size="15" value="" name="productSearch" id="conformProductSearch"  placeholder="Scan/Enter Product ID"  style="width:210px;"/>
					<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" />
				</div>
			</div>
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Donor Id</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">Donor Gender</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'Male','2':'Female'}" name="" value="" /></td>
				</tr>
				<tr>
					<td class="tablewapper">Donor MRN</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">Donor Weight</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
                </tr>
                <tr>
					<td class="tablewapper">Donor Name</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">Donor Height</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
				</tr>
				<tr>
					<td class="tablewapper">Donor DOB</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">Donor Physician</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'Mari','2':'Muthu'}" name="" value="" /></td>
					
				</tr>
				<tr>
					<td class="tablewapper">Donor ABO/Rh</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">Language Spoken</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'Select','2':'Select'}" name="" value="" /></td>
					
				</tr>
				<tr>
					<td class="tablewapper">Donor Type</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">Translator Needed</td>
					<td class="tablewapper1"><input type="radio"/>Yes&nbsp;<input type="radio"/>No</td>
					
				</tr>
			</table>
        </div>
    </div>
</div>
</form>

<form>
<div class="column">       
	<div  class="portlet">
    	<div class="portlet-header ">Donor Screening Results</div>
		<div class="portlet-content">
						
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">HIV Type-1</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'Select','2':'Non-Reactive'}" name="" value="" /></td>
					<td class="tablewapper">Human T Cells Lymphotrophic Virus 1</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'Select','2':'Non-Reactive'}" name="" value="" /></td>
				</tr>
				<tr>
					<td class="tablewapper">HIV Type-2</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'Select','2':'Non-Reactive'}" name="" value="" /></td>
					<td class="tablewapper">Human T Cells Lymphotrophic Virus 2</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'Select','2':'Non-Reactive'}" name="" value="" /></td>
                </tr>
                <tr>
					<td class="tablewapper">Hepatitis B Virus</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'Select','2':'Non-Reactive'}" name="" value="" /></td>
					<td class="tablewapper">West Nile Virus</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'Select','2':'Non-Reactive'}" name="" value="" /></td>
				</tr>
				<tr>
					<td class="tablewapper">Hepatitis C Virus</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'Select','2':'Non-Reactive'}" name="" value="" /></td>
					<td class="tablewapper">Typonosoma Cruzi:Chagas'Disease</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'Select','2':'Non-Reactive'}" name="" value="" /></td>
					
				</tr>
				<tr>
					<td class="tablewapper">Treponema Pallidum (Syphilis)</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'Select','2':'Non-Reactive'}" name="" value="" /></td>
					<td class="tablewapper">CMV</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'Select','2':'Non-Reactive'}" name="" value="" /></td>
					
			</table>
        </div>
    </div>
</div>
</form>

<div class="column">		
		<div  class="portlet">
			<div class="portlet-header ">Donor Lab Results</div>
				<div class="portlet-content">
				<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="add_donorLabResults()"/>&nbsp;&nbsp;<label  class="cursor" onclick="add_donorLabResults()"><b>Add </b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id =""/>&nbsp;&nbsp;<label class="cursor" ><b>Delete</b></label>&nbsp;</div></td>
				</tr>
				</table>
				<form>
				<table cellpadding="0" cellspacing="0" border="0" id="donorLabResultTable" class="display">
					<thead>
						<tr>
							<th width="4%" id="donorLabResultColumn"></th>
							<th>Collection Day</th>
							<th>WBC</th>
							<th>CD34</th>
							<th>%CD34</th>
							<th>HCT</th>
							<th>PLT</th>
							<th>%MNC</th>
						</tr>
						
					</thead>
					<tbody>
												
					</tbody>
				</table>
				</form>		
				</div>	
			</div>
</div>

<div class="column">		
		<div  class="portlet">
			<div class="portlet-header ">Product Lab Results</div>
				<div class="portlet-content">
				<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="add_productLabResults()"/>&nbsp;&nbsp;<label  class="cursor" onclick="add_productLabResults()"><b>Add </b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id =""/>&nbsp;&nbsp;<label class="cursor" ><b>Delete</b></label>&nbsp;</div></td>
				</tr>
				</table>
				<form>
				<table cellpadding="0" cellspacing="0" border="0" id="productLabResultTable" class="display">
					<thead>
						<tr>
							<th width="4%" id="productLabResultColumn"></th>
							<th>Collection Day</th>
							<th>WBC</th>
							<th>HCT</th>
						    <th>PLT</th>
						    <th>%MNC</th>
							<th>CD34</th>
							<th>CD Yield</th>
							<th>Liter Processed(L)</th>
						    <th>Machine Volume(L)</th>
						    <th>Scale Volume(L)</th>
							<th># of Bags</th>
						</tr>
					</thead>
					<tbody>
												
					</tbody>
				</table>
				</form>		
				</div>	
			</div>
</div>

<div class="column">		
		<div  class="portlet">
			<div class="portlet-header ">Document Review</div>
				<div class="portlet-content">
				<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="add_documentreview()"/>&nbsp;&nbsp;<label  class="cursor" onclick="add_documentreview()"><b>Add </b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id =""/>&nbsp;&nbsp;<label class="cursor" ><b>Delete</b></label>&nbsp;</div></td>
				</tr>
				</table>
				<form>
				<table cellpadding="0" cellspacing="0" border="0" id="documentreviewTable" class="display">
					<thead>
						<tr>
							<th width="4%" id="documentreviewColumn"></th>
							<th>Document Title</th>
							<th>Document Date</th>
							<th>Attachment</th>
							<th>Reviewed</th>
						</tr>
					</thead>
					<tbody>
				 	<tr>
						<td><input type='checkbox' id=''/> </td>	
						<td>Donor Eligibility</td>
						<td> Aug 05,1987</td>
						<td><img src="images/icons/Black_Paperclip.jpg"></td>
						<td> <input type='checkbox' id=''/> </td>
								
					</tr>
					<tr>
						<td><input type='checkbox' id=''/> </td>	
						<td> Donor Consent</td>
						<td> Aug 05,1987 </td>
						<td><img src="images/icons/Black_Paperclip.jpg"></td>
						<td> <input type='checkbox' id=''/> </td>
									
					</tr>
					<tr>
						<td><input type='checkbox' id=''/> </td>	
						<td> Collection</td>
						<td> Aug 05,1987 </td>
						<td><a href="#" style="">View</a></td>
						<td> <input type='checkbox' id=''/> </td>
								
					</tr>
					<tr>
						<td><input type='checkbox' id=''/> </td>	
						<td> IDM Verification</td>
						<td> <input type='text' class='dateEntry' id=''/></td>
						<td> <input type='file' class='' id=''/></td>
						<td><input type='checkbox' id=''/>  </td>
									
					</tr>
					
				</tbody>					
				</table>
				</form>		
				</div>	
			</div>
</div>
 
 <form>
 <div class="column">       
	<div  class="portlet">
    	<div class="portlet-header ">Collection Order</div>
		<div class="portlet-content">
		
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Collection Order Issued By</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'yes','2':'No'}" name="" value="" /></td>
					<td class="tablewapper">Target CD34 Counts</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					
				</tr>
				<tr>
					<td class="tablewapper">Target Processing Volume(L)</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper"></td>
					<td class="tablewapper1"></td>
                </tr>
               
			</table>
        </div>
    </div>
</div>
</form>
<form>
 <div class="column">       
	<div  class="portlet">
    	<div class="portlet-header ">Most Recent Mobilization Information</div>
		<div class="portlet-content">
		
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Mobilization Order Issued By</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'yes','2':'No'}" name="" value="" /></td>
					<td class="tablewapper">Mobilization Nurse</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'yes','2':'No'}" name="" value="" /></td>
					
				</tr>
				<tr>
					<td class="tablewapper">Mobilization Date</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper">Mobilization Method</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'yes','2':'No'}" name="" value="" /></td>
					
				</tr>
				<tr>
					<td class="tablewapper">Time of Mobilization</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					<td class="tablewapper"></td>
					<td class="tablewapper1"></td>
                </tr>
               
			</table>
			<hr>
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">Mobilizer Name</td>
					<td class="tablewapper1"><s:select headerKey="" headerValue="Select" list="#{'1':'yes','2':'No'}" name="" value="" />+   </td>
					<td class="tablewapper">Mobilizer Dose</td>
					<td class="tablewapper1"><input type="text" id="" name=""/></td>
					
				</tr>
				
               
			</table>
        </div>
    </div>
</div>
</form>

<div align="right" style="float:right;">
    <form onsubmit="return avoidFormSubmitting()">
        <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
            <tr>
                <td>Next:</td>
                <td><div class="mainselection"><select name="" id="" ><option value="">Select</option><option>Donor Assignment</option><option>Log Out</option></select></div></td>
                <td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;&nbsp;</td>
                <td><input type="button" value="Next" onclick="()"/></td>
            </tr>
        </table>
    </form>
</div>
<script>
$(function() {

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	

	/**For Notes**/
	/* 
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='ui-addnew'></span>");
	
	$( ".portlet-header .ui-addnew" ).click(function() {
		addNewPopup("Add New Product","SupervisoraddNewDialog","addnewAcquisition","700","1100");
	});
	 */
	
	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});
</script>