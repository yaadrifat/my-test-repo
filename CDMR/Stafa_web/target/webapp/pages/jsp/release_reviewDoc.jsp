<%@ taglib prefix="s" uri="/struts-tags"%>
<script>
$(document).ready(function(){
	oTable=$("#documentReviewTable").dataTable({
		 										"bJQueryUI": true,
		 										"sDom": 'C<"clear">Rlfrtip',
		 										"aoColumns": [ { "bSortable":false},
		 										               null,
		 										               null,
		 										               null,
		 										               null
		 										               ],
		 										"oColVis": {
		 											"aiExclude": [ 0 ],
		 											"buttonText": "&nbsp;",
		 											"bRestore": true,
		 											"sAlign": "left"
		 										},
		 										"fnInitComplete": function () {
		 									        $("#documentReviewColumn").html($('.ColVis:eq(0)'));
		 										}
												});
		oTable.thDatasorting('documentReviewTable');
		oTable=$("#disposalTable").dataTable({
										"bJQueryUI": true,
										"sDom": 'C<"clear">Rlfrtip',
										"aoColumns": [ { "bSortable":false},
 										               null,
 										               null,
 										               null,
 										               null,
 										               null,
 										               null,
 										               null,
 										               null,
 										               null,
 										               null,
 										               null
 										               ],
										"oColVis": {
											"aiExclude": [ 0 ],
											"buttonText": "&nbsp;",
											"bRestore": true,
											"sAlign": "left"
										},
										"fnInitComplete": function () {
									        $("#disposalColumn").html($('.ColVis:eq(1)'));
										}
			});
	    $("select").uniform(); 
	    
	    oTable.thDatasorting('disposalTable');
  });
</script>
<div class="cleaner"></div>

<div class="cleaner"></div>
 
<div class="column">		
	<div  class="portlet">
		<div class="portlet-header notes">Documents to be Reviewed</div>
			<div class="portlet-content">
				<table cellpadding="0" cellspacing="0" border="0" id="documentReviewTable" class="display">
				<thead>
					   <tr>
					   		<th width="4%" id="documentReviewColumn"></th>
							<th><s:text name="stafa.label.document_title"/></th>
							<th><s:text name="stafa.label.issue_date"/></th>
							<th><s:text name="stafa.label.attachment"/></th>
							<th><s:text name="stafa.label.reviewed"/></th>
					   </tr>
					   <tr>
					   		<th width="4%" id="documentReviewColumn"></th>
							<th><s:text name="stafa.label.document_title"/></th>
							<th><s:text name="stafa.label.issue_date"/></th>
							<th><s:text name="stafa.label.attachment"/></th>
							<th><s:text name="stafa.label.reviewed"/></th>
					   </tr>
				</thead>
				<tbody>
						<tr>
							<td></td>
							<td>Approval forDisposal-Expired Patient</td>
							<td>07-09-2012</td>
							<td><img src="images/icons/Black_Paperclip.jpg"></td>
							<td><input type="checkbox"></td>
						</tr>
				</tbody>
				</table>
		</div>
	</div>	
</div>	

<div class="cleaner"></div>
 
<div class="column">		
	<div  class="portlet">
		<div class="portlet-header notes">Select Products for Disposal</div>
			<div class="portlet-content">
					<div id="button_wrapper" align="center">
						<input class="scanboxSearch" type="text" size="15" value="" name="productSearch" id="productSearch" onkeyup=""  placeholder="Scan/Enter Product ID" style="width:200px;"/>
					</div>
					<table cellpadding="0" cellspacing="0" border="0" id="disposalTable" class="display">
						<thead>
							<tr>
								<th width="4%" id="disposalColumn"></th>
								<th><s:text name="stafa.label.recipientid"/></th>
								<th><s:text name="stafa.label.recipientname"/></th>
								<th><s:text name="stafa.label.donorid"/></th>
								<th><s:text name="stafa.label.donorname"/></th>
								<th><s:text name="stafa.label.productid"/></th>
								<th><s:text name="stafa.label.producttype"/></th>
								<th><s:text name="stafa.label.collectiondate"/></th>
								<th><s:text name="stafa.label.vessel"/></th>
								<th><s:text name="stafa.label.slotlocation"/></th>
								<th><s:text name="stafa.label.verification"/></th>
								<th><s:text name="stafa.label.documents"/></th>
							</tr>
							<tr>
								<th width="4%" id="disposalColumn"></th>
								<th><s:text name="stafa.label.recipientid"/></th>
								<th><s:text name="stafa.label.recipientname"/></th>
								<th><s:text name="stafa.label.donorid"/></th>
								<th><s:text name="stafa.label.donorname"/></th>
								<th><s:text name="stafa.label.productid"/></th>
								<th><s:text name="stafa.label.producttype"/></th>
								<th><s:text name="stafa.label.collectiondate"/></th>
								<th><s:text name="stafa.label.vessel"/></th>
								<th><s:text name="stafa.label.slotlocation"/></th>
								<th><s:text name="stafa.label.verification"/></th>
								<th><s:text name="stafa.label.documents"/></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td>MDA789565</td>
								<td>John Wilson</td>
								<td>MDA789783</td>
								<td>Ashley Wilson</td>
								<td>BMT123-2</td>
								<td>HPC-M</td>
								<td>Jul 1,2012</td>
								<td>100ml Bag</td>
								<td>T20-2-b</td>
								<td><input type="checkbox"></td>
								<td><input type="checkbox"></td>
							</tr>
							<tr>
								<td></td>
								<td>MDA789565</td>
								<td>John Wilson</td>
								<td>MDA789783</td>
								<td>Ashley Wilson</td>
								<td>BMT123-3</td>
								<td>HPC-M</td>
								<td>Jul 1,2012</td>
								<td>100ml Bag</td>
								<td>T20-2-b</td>
								<td><input type="checkbox"></td>
								<td><input type="checkbox"></td>
							</tr>
						</tbody>
					</table>
			</div>
	</div>
</div>	

<div class="cleaner"></div>

<div class="column">		
	<div  class="portlet">
		<div class="portlet-header notes">Release Sign-Off</div>
			<div class="portlet-content">
				<form>
					<table cellpadding="0" cellspacing="0" border="0" id="disposalTable" >
						<tbody>
							<tr>
								<td>
									<table>
										<tr>
											<td>Lab Tech1 &nbsp;</td>
											<td><input type="text" value="S.G."/>&nbsp;</td>
											<td>Sign-off Time &nbsp;</td>
											<td><span class="jclock"></span>&nbsp;&nbsp;</td>
										</tr>
									</table>
								</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>
									<table>
										<tr>
											<td>Lab Tech2 &nbsp;</td>
											<td><input type="text" value="S.T."/>&nbsp;</td>
											<td>Sign-off Time &nbsp;</td>
											<td><span class="jclock"></span>&nbsp;&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
	</div>
</div>	
<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td>Next:</td>
				<td><div><div class="mainselection"><select name="" id=""><option value="">Select</option><option>Document Review</option><option>Preparation</option></select></div></div></td>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="nextProcessPreProduct();" value="Next"/></td>
				</tr>
				</table>
				</form>
		</div>

<script>

$(function() {
	$('.jclock').jclock();
	
	$( ".column" ).sortable({
		connectWith: ".column"
			/*,
        handle: '.portlet-header',
        cursor: 'move',
        forcePlaceholderSize: true*/
			
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});

	/**For Notes**/
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span> ");

	/* $( ".portlet-header .ui-addnew" ).click(function() {
		showPopUp("open","addNewDialog","Add New Acquision","900","450")
	}); */
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");
	$( ".portlet-header .ui-reset " ).click(function() {
		var formName = document.getElementById("login");
		//clearForm(formName);
		clear();
	});

// 	$( ".portlet-header .ui-notes " ).click(function() {
// 		showNotes('open');
// 	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});


var now = new Date();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivalDate').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new Date()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};



$("#timeZone").html(timezones[-offset / 60]);

</script>
 