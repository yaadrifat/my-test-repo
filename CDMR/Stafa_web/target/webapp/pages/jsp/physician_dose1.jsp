<%@ include file="common/includes.jsp" %>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />

<script>
$(document).ready(function() {
//$('#table111').dataTable({"aaSorting": [[ 1, "desc" ]]});
	$('#previouse').attr("disabled", false);
	$('#next').attr("disabled", false);
	$('#currentPage').val("s5");
	oTable = $('#availableProductsTable').dataTable({
		"bJQueryUI": true
	});
	oTable.thDatasorting('availableProductsTable');
	oTable = $('#totalDoseTable').dataTable({
		"bJQueryUI": true
	});	
	oTable.thDatasorting('totalDoseTable');
	$("select").uniform();
});
$(document).ready(function() {
    $('#blind').html("<<");
      var $box = $('#box').wrap('<div id="box-outer"></div>');
      $('#blind').click(function() {
        $box.blindToggle('fast');
      });
    });


jQuery.fn.blindToggle = function(speed, easing, callback) {
var h = this.width() + parseInt(this.css('paddingLeft')) +  parseInt(this.css('paddingRight'));
if(parseInt(this.css('marginLeft'))<0){
$('#forfloat_right').animate( { width: "79%" }, { queue: false, duration: 200 });	
 $('#blind').html("<<");
 return this.animate({marginLeft:0},speed, easing, callback);
 }
else{
$( "#forfloat_right" ).animate( { width: "95%" }, { queue: false, duration: 250 });
 	$('#blind').html(">>");
 	return this.animate({marginLeft:-h},speed, easing, callback);
   }
};
function calculateTotalDose(){
	var rowDose = document.getElementById("totalDoseTable").rows[1];
	var productsTable = document.getElementById("availableProductsTable");
	var tnc=0;
	var cd34=0;
	var cd3=0;
	for(i=1;i<productsTable.rows.length;i++){
		var row = productsTable.rows[i];
		if(productsTable.rows[i].cells[8].childNodes[0].checked){
			if(i!=5){
			tnc+=Number($.trim(row.cells[4].innerHTML));
			cd34+=Number($.trim(row.cells[5].innerHTML));
			cd3+=Number($.trim(row.cells[6].innerHTML));
			}
		}
	}
	rowDose.cells[0].innerHTML = tnc;
	rowDose.cells[1].innerHTML = cd34;
	rowDose.cells[2].innerHTML = cd3;
}

</script>

<div class="cleaner"></div>
<!--float window-->

<div id="floatMenu">
<div id="forfloat_wind">
<div id="box-outer">
<a href="#" style="text-decoration:none" id="blind">&lt;&lt; </a>
	<div id="box">
		<div id="leftnav" >
		
<!-- recipient start-->
		<div  class="portlet">
			<div class="portlet-header notes">Recipient</div>
				<div class="portlet-content">
				<table width="225" border="0">
				  <tr>
					<td width="89">ID:</td>
					<td width="120">MDA789564</td>
				  </tr>
				  <tr>
					<td>Name:</td>
					<td>John Wilson</td>
				  </tr>
				  <tr>
					<td>ABO/Rh:</td>
					<td>O/Pos</td>
				  </tr>
				  <tr>
					<td>Diagnosis:</td>
					<td>Leukemia</td>
				  </tr>
				  <tr>
					<td>Planned Protocol:</td>
					<td>SCT-Allo</td>
				  </tr>
</table>

			</div>
			</div>
		
<!-- recipient-->

<!-- Donor start-->
			<div class="portlet">
				<div class="portlet-header notes">Donor</div>
				<div class="portlet-content">
					<table width="225" border="0">
						<tr>
							<td width="89">ID:</td>
							<td width="120">MDA789569</td>
						</tr>
						<tr>
							<td>Name:</td>
							<td>Mary Wilson</td>
						</tr>
						<tr>
							<td>ABO/Rh:</td>
							<td>O/Pos</td>
						</tr>
						<tr>
							<td>Donor Type:</td>
							<td>ALLO-related</td>
						</tr>
					</table>


				</div>
			</div>
<!-- Donor-->
		</div>
		</div>
	
	</div>
</div>
</div>

<!--float window end-->


<div class="cleaner"></div>
<!--right window start -->	
<div id="forfloat_right">
	<section>
 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes next previous">Total Dose</div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="totalDoseTable" class="display">
					<thead>
						<tr>
							<th colspan="1">TNC/kg(x10^8/kg)</th>
							<th colspan="1">CD34/kg(x10^6/kg)</th>
							<th colspan="1">CD3/kg(x10^6/kg)</th>
						</tr>
						<tr>
						<th></th>
						<th></th>
						<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td align="center">25</td>
							<td align="center">20</td>
							<td align="center">35</td>
						</tr>
					</tbody>
			</table>
	</div>
	</div>	
</div>
	
 <div class="cleaner"></div>
 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header">Available Products</div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="availableProductsTable" class="display">
					<thead>
						<tr>
							<th colspan="1">Product Type</th>
							<th colspan="1">Collectioned Date</th>
							<th colspan="1">Processing</th>
							<th colspan="1">Sterility</th>				
							<th colspan="1">TNC/kg(x10^8/kg)</th>
							<th colspan="1">CD34/kg(x10^6/kg)</th>
							<th colspan="1">CD3/kg(x10^6/kg)</th>
							<th colspan="1">Volume</th>
							<th colspan="1">Select</th>
						</tr>
						<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						</tr>
					</thead>
					<tbody>
						<tr >
							<td>HPC-A</td>
							<td>Aug 02, 2008</td>
							<td>Vol.Red.</td>
							<td>Pending</td>
							<td>8</td>
							<td>0.8</td>
							<td>0.7</td>
							<td>100ml</td>
							<td align="center"><input id="check" onclick="calculateTotalDose(this)" type="checkbox" /></td>
						</tr>
						<tr onclick="">
							<td>HPC-A</td>
							<td>Aug 02, 2008</td>
							<td></td>
							<td>Pending</td>
							<td>4</td>
							<td>0.2</td>
							<td>0.3</td>
							<td>100ml</td>
							<td align="center"><input onclick="calculateTotalDose(this)"  type="checkbox" /></td>
						</tr>
						<tr>
							<td>HPC-A</td>
							<td>Mar 02, 2011</td>
							<td></td>
							<td>Pending</td>
							<td>5</td>
							<td>0.5</td>
							<td>0.3</td>
							<td>100ml</td>
							<td align="center"><input onclick="calculateTotalDose(this)" type="checkbox" /></td>
						</tr>
						<tr>
							<td>HPC-A</td>
							<td>Mar 02, 2011</td>
							<td></td>
							<td>Pending</td>
							<td>7</td>
							<td>0.2</td>
							<td>0.2</td>
							<td>100ml</td>
							<td align="center"><input onclick="calculateTotalDose(this)"  type="checkbox" /></td>
						</tr>
						<tr>
							<td>HPC-A</td>
							<td>Aug 02, 2008</td>
							<td>Vol.Red.</td>
							<td></td>
							<td onclick="showPopUp('open','sterility','Positive Sterility');" style="color:red">Positive</a></td>
							<td></td>
							<td></td>
							<td>100ml</td>
							<td align="center"><input type="checkbox" /></td>
						</tr>
						<tr>
							<td>HPC-A</td>
							<td>Aug 02, 2008</td>
							<td></td>
							<td>Pending</td>
							<td>3</td>
							<td>0.5</td>
							<td>0.2</td>
							<td>100ml</td>
							<td align="center"><input onclick="calculateTotalDose(this)"  type="checkbox" /></td>
						</tr>
						<tr>
							<td>HPC-A</td>
							<td>Mar 02, 2011</td>
							<td></td>
							<td>Pending</td>
							<td>6</td>
							<td>0.5</td>
							<td>0.5</td>
							<td>100ml</td>
							<td align="center"><input onclick="calculateTotalDose(this)"  type="checkbox" /></td>
						</tr>
						<tr>
							<td>HPC-A</td>
							<td>Mar 02, 2011</td>
							<td></td>
							<td>Negative</td>
							<td>3</td>
							<td>0.7</td>
							<td>0.5</td>
							<td>100ml</td>
							<td align="center"><input onclick="calculateTotalDose(this)"  type="checkbox" /></td>
						</tr>
						<tr>
							<td>HPC-A</td>
							<td>Aug 02, 2008</td>
							<td>Vol.Red.</td>
							<td>Pending</td>
							<td>8</td>
							<td>0.2</td>
							<td>0.6</td>
							<td>100ml</td>
							<td align="center"><input onclick="calculateTotalDose(this)"  type="checkbox" /></td>
						</tr>
						<tr>
							<td>HPC-A</td>
							<td>Aug 02, 2008</td>
							<td></td>
							<td>Negative</td>
							<td>4</td>
							<td>0.2</td>
							<td>0.3</td>
							<td>100ml</td>
							<td align="center"><input onclick="calculateTotalDose(this)"  type="checkbox" /></td>
						</tr>
						<tr>
							<td>HPC-A</td>
							<td>Mar 02, 2011</td>
							<td></td>
							<td>Negative</td>
							<td>5</td>
							<td>0.4</td>
							<td>0.2</td>
							<td>100ml</td>
							<td align="center"><input onclick="calculateTotalDose(this)"  type="checkbox" /></td>
						</tr>
						<tr>
							<td>HPC-A</td>
							<td>Mar 02, 2011</td>
							<td></td>
							<td>Negative</td>
							<td>9</td>
							<td>0.9</td>
							<td>0.9</td>
							<td>100ml</td>
							<td align="center"><input onclick="calculateTotalDose(this)"  type="checkbox"/></td>
						</tr>
					</tbody>
			</table>
	</div>
	</div>	
</div>		
		
</section>	

<div class="cleaner"></div>

<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
				Page:&nbsp;&nbsp;<u>1</u>&nbsp;&nbsp;&nbsp;2&nbsp;
			<input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>
			<input type="button" value="Select"/>
		</form>
		</div>
		<div class="cleaner"></div>
</div>
<script>
$(function() {	
	
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open');
	});
	
	

	/**for Next Button Start**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".next" )
	.append("<span style=\"float:right;\" class='ui-next'></span>");
	
	$( ".portlet-header .ui-next " ).click(function() {
		loadPage('jsp/physician_product1.jsp');	
	});
	/**for Next Button End**/
	
	/**for Previous Button Start**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".previous" )
	.append("<span style=\"float:right;\" class='ui-previous'></span>");
	
	$( ".portlet-header .ui-previous " ).click(function() {
		loadPage('jsp/physician_dose.jsp');	
	});
	/**for Previous Button End**/
	
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");
	

	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});

</script>

<div id="sterility" title="sterility" style="display: none">
		<b>Proceed ?</b>&nbsp;<input type="checkbox" value=""/>Y&nbsp;<input type="checkbox" value=""/>N
    </div>