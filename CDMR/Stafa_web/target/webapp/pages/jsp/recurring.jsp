<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="common/includes.jsp" %>
<script type="text/javascript">


$(document).ready(function() {
	hide_slidewidget();
	$( ".task" ).css("width","20%");
	$( ".startDateStr" ).datepicker({dateFormat: 'M dd, yy'});
	$( ".dueDateStr" ).datepicker({dateFormat: 'M dd, yy'});
	$( "#recurringDateStr2" ).datepicker({dateFormat: 'M dd, yy'});
	$( "#recurringDateStr3" ).datepicker({dateFormat: 'M dd, yy'});

	oTable = $('#pendingTable').dataTable({
		"bJQueryUI": true
	});
	oTable.thDatasorting('pendingTable');
	$("select").uniform();	
});



</script>
<div class="cleaner"></div>

<section>

 <div class="cleaner"></div>
 
 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header addnew">Recurring Tasks</div>
			<div class="portlet-content">
			<table cellpadding="0" cellspacing="0" border="0" id="pendingTable" class="display" >
			<thead>
				<tr>
					<th colspan="1">Task</th>
					<th colspan="1">Description</th>		
					<th style="width:25%;" colspan="1">User Assignment </th>
					<th colspan="1">Start Date</th>
					<th colspan="1">Due Date</th>
					<th colspan="1">Task Status</th>
				</tr>
				 
                      <tr>
                      <th></th>
					<th></th>		
					<th></th>
					<th></th>
					<th></th>
					<th></th>
                      </tr>
			</thead>
				<tr>
					<td><select>
							<option>Select</option>
							<option>QC Test Result</option>
							<option>Vol.REd.</option>
							<option>Cryogene liason</option>
							<option>Rountine QC</option>
							<option> Tank Maintainence</option>
							<option> Reagents & Consumable</option></select></td>
					<td>Routine Maintenance and quality control</td>
					<td><input type="text" id="" class="task" value="Task1" />
						<select>
							<option>Select</option>
							<option>Primary</option>
							<!-- <option>Tech 3</option>
							<option>Tech 4</option> -->
						</select><img src = "images/icons/addnew.jpg" style="cursor:pointer;"/>
					</td>
					<td>
					<input type="text" id="" class="dateEntry startDateStr" />
					</td> 
					<td>
					<input type="text" id="" class="dateEntry dueDateStr" />
					</td>
					<td>Assigned</td>
				</tr>
				
				<tr>
					<td><select>
							<option>Select</option>
							<option>QC Test Result</option>
							<option>Vol.REd.</option>
							<option>Cryogene liason</option>
							<option>Rountine QC</option>
							<option> Tank Maintainence</option>
							<option> Reagents & Consumable</option>
							</select></td>
					<td>Validate Stability of all Cryo Tanks</td>
					<td><input type="text" id="" class="task" value="Task2"/>
						<select>
							<option>Select</option>
							<option>Primary</option>
							<!-- <option>Tech 3</option>
							<option>Tech 4</option> -->
						</select><img src = "images/icons/addnew.jpg" style="cursor:pointer;"/>
					</td>
					<td>
					<input type="text" id="" class="dateEntry startDateStr" />
					</td> 
					<td>
					<input type="text" id="" class="dateEntry dueDateStr" />
					</td>
					<td>Assigned</td> 
					
				</tr>
				
				<tr>
					<td><select>
							<option>Select</option>
							<option>QC Test Result</option>
							<option>Vol.REd.</option>
							<option>Cryogene liason</option>
							<option>Rountine QC</option>
							<option> Tank Maintainence</option>
							<option> Reagents & Consumable</option>
							</select></td>
					<td>Send, order, receive products from cryogene</td>
					<td ><input type="text" id="" class="task" value="Task3"/>
						<select>
							<option>Select</option>
							<option>Primary</option>
							<!-- <option></option>
							<option></option> -->
						</select><img src = "images/icons/addnew.jpg" style="cursor:pointer;"/> 
					</td>
					<td>
					<input type="text" id="" class="dateEntry startDateStr" />
					</td> 
					<td>
					<input type="text" id="" class="dateEntry dueDateStr" />
					</td>
					<td>Assigned</td>
				</tr>
				
				<tr>
					<td><select>
							<option>Select</option>
							<option>QC Test Result</option>
							<option>Vol.REd.</option>
							<option>Cryogene liason</option>
							<option>Rountine QC</option>
							<option> Tank Maintainence</option>
							<option> Reagents & Consumable</option>
						</select></td>
					<td>Make required reagents and stock consumables in each workstation</td>
					<td><input type="text" id="" class="task" value="Task4"/>
						<select>
							<option>Select</option>
							<option>Primary</option>
							<!--  <option></option>
							<option></option>  -->
						</select> 
						<img src = "images/icons/addnew.jpg" style="cursor:pointer"/>
					</td>
					<td>
					<input type="text" id="" class="dateEntry startDateStr" />
					</td> 
					<td>
					<input type="text" id="" class="dateEntry dueDateStr" />
					</td>
					<td>Assigned</td>
				</tr>
			
			</table>
	</div>
	</div>	
</div>

</section>
<script type="text/javascript">

$(function(){
	
	
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open');
	});

	/**For Notes**/
	
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
	.append("<span style=\"float:right;\" class='ui-addnew'></span>");
	
	$( ".portlet-header .ui-addnew" ).click(function() {
/* 		$.uniform.restore("Select"); */
		$("#pendingTable").append("<tr>"+
								  "<td><select class='select1'><option>QC Test Results</option><option>Vol.Red.</option><option>Cryogene liason</option><option>Routine QC</option><option>Cryoprep</option></select></td>"+
								  "<td></td>"+
								  "<td><select class='select1'><option>None</option></select></td>"+
								  "<td><input type='text' id='recurringDateStr4' class='dateEntry'/></td>"+
								  "</tr>");
		$("select").uniform();
		$( "#recurringDateStr4" ).datepicker({dateFormat: 'M dd, yy'});
	});
	
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});


	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.pendingTable').dataTable({"aaSorting": [[ 1, "asc" ]]});	
});


var now = new Date();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivalDate').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new Date()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};
$("#timeZone").html(timezones[-offset / 60]);

</script>
