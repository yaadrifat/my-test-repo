<%@ taglib prefix="s" uri="/struts-tags"%>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script>
$(document).ready(function() {
	//getProcessingData();
//$('#table111').dataTable({"aaSorting": [[ 1, "desc" ]]});
	oTable = $('#equipmentsTable').dataTable({
		"bJQueryUI": true
		//"sPaginationType": "full_numbers"
	});
	oTable = $('#reagentsTable').dataTable({
		"bJQueryUI": true
		//"sPaginationType": "full_numbers"
	});
	$("select").uniform();	  
	document.getElementById('equipmentsTable_next').setAttribute("onClick","removeTableClass()");
	document.getElementById('equipmentsTable_previous').setAttribute("onClick","removeTableClass()");
	document.getElementById('reagentsTable_next').setAttribute("onClick","removeTableClass()");
	document.getElementById('reagentsTable_previous').setAttribute("onClick","removeTableClass()");
	
	removeTableClass();
	removeTableClassOnChange("equipmentsTable_length");
	removeTableClassOnChange("reagentsTable_length");
	
	 
});
$("#entirevol").click(function() {
	$("#resportion").find("tr").show();
	});
$("#entirevolno").click(function(){
	$("#resportion").find("tr").hide();
});
$(document).ready(function() {
	$("#resportion").find("tr").hide();
    show_slidewidgets('rel_infusionwidget');

	
             });


/* 
       jQuery.fn.blindToggle = function(speed, easing, callback) {
               var h = this.width() + parseInt(this.css('paddingLeft')) +  parseInt(this.css('paddingRight'));
               if(parseInt(this.css('marginLeft'))<0){
			   	   $('#forfloat_right').animate( { width: "79%" }, { queue: false, duration: 200 });	
                   $('#blind').html("<<");
                   return this.animate({marginLeft:0},speed, easing, callback);
                   }
               else{
    			  $( "#forfloat_right" ).animate( { width: "95%" }, { queue: false, duration: 250 });
     
                   $('#blind').html(">>");
                   return this.animate({marginLeft:-h},speed, easing, callback);
				   
                     }
				
             }; */
             function generateTableFromJsnList(response){
            		$.each(response.recipientDonorList, function(i,v) {
            			//alert(i+"=-="+v[0]);
            			$("#recipientName").html(v[0]);
            			$("#recipientId").html(v[1]);
            			$("#recipientDiagnosis").html(v[2]);
            			$("#recipientAboRh").html(v[3]);
            			$("#donorName").html(v[4]);
            			$("#donorId").html(v[5]);
            			$("#donorAboRh").html(v[6]);
            			$("#productId").html(v[7]);
            			$("#productType").html(v[9]);
            		 	});
            	}
            	function getProcessingData(){
            		//alert("s");
            		var url = "getOriginalProductData";
            		var productSearch = $("#productSearch").val();
            		//if(e.keyCode==13 && productSearch!=""){
            			//alert("s : "+e.keyCode);
            			var response = ajaxCall(url,"infusionReleaseForm");
            			//alert(response);
            			//alert(response.qcList);
            			//response = jQuery.parseJSON(response.recipientDonorList);
            			//alert(response);	
            			//alert(response.returnJsonStr);
            			generateTableFromJsnList(response);
            		//}
            		//response = jQuery.parseJSON(response.recipientDonorList);

            	}

function removeTableClass(){
	
	$('#equipmentsTable_next').removeClass("ui-state-default");
	$('#equipmentsTable_previous').removeClass("ui-state-default");
	$('#reagentsTable_next').removeClass("ui-state-default");
	$('#reagentsTable_previous').removeClass("ui-state-default");

}

</script>

<div class="cleaner"></div>

<!--float window-->

<!-- <div id="floatMenu">
<div id="forfloat_wind">
	<div id="box-outer">
		
		<a href="#" style="text-decoration:none" id="blind">&lt;&lt; </a>
			<div id="box"> -->
				<div id="rel_infusionwidget" >
				
<!-- recipient start-->
					<div class="column">
					<div  class="portlet">
					<div class="portlet-header notes"><s:hidden id="moduleName" value="releaseinfusion"/><s:hidden id="widgetName" value="recipient"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.recipient"/></div>
						<div class="portlet-content">
						<table width="100%" border="0">
						  <tr>
							<td width="50%"><s:text name="stafa.label.id"/></td>
							<td width="50%"><div id="recipientId">789564</div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.name"/></td>
							<td><div id="recipientName">John Wilson</div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientaborh"/></td>
							<td><div id="recipientAboRh">A Positive</div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientdiagnosis"/></td>
							<td><div id="recipientDiagnosis"></div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.recipientrid"/></td>
							<td><div id="recipientRID"></div></td>
						  </tr>
						</table>
					</div>
					</div>
					</div>
				
<!-- recipient-->

<!-- Product start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header notes"><s:hidden id="moduleName" value="releaseinfusion"/><s:hidden id="widgetName" value="product"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.product"/></div>
						<div class="portlet-content">
						<table width="100%" border="0">
							<tr>
							<td width="50%"><s:text name="stafa.label.id"/></td>
							<td width="50%"><div id="productId">BMT123-3-1</div></td>
						  </tr>
						  <tr>
							<td><s:text name="stafa.label.type"/></td>
							<td><div id="productType">HPC-A</div></td>
						  </tr>
						</table>	
					</div>
				</div>
				</div>
				
<!-- Product-->

<!-- Donor start-->
					<div class="column">
					<div class="portlet">
						<div class="portlet-header notes"><s:hidden id="moduleName" value="releaseinfusion"/><s:hidden id="widgetName" value="donor"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.donor"/></div>
						<div class="portlet-content">
							<table width="100%" border="0">
								<tr>
									<td width="50%"><s:text name="stafa.label.id"/></td>
									<td width="50%"><div id="donorId">546743</div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.name"/></td>
									<td><div id="donorName">Johnson</div></td>
								</tr>
								<tr>
									<td><s:text name="stafa.label.donoraborh"/></td>
									<td><div id="donorAboRh">A Positive</div></td>
								</tr>
							</table>
						</div>
					</div>
					</div>
					<!-- Donor-->
				</div>
				<!-- </div>
			</div>
		</div>
</div> -->
<!--float window end-->

<!--right window start -->	
<!-- <div id="forfloat_right"> -->
<form action="#" id="infusionReleaseForm">
<input type="hidden" name="productSearch" id="productSearch" value="BMT4657"/>	
 <div class="column">		
		<div  class="portlet">
			<div class="portlet-header"><s:text name="stafa.label.prodinfusion"/></div>
			<div id="button_wrapper">Product Type : &nbsp;&nbsp;HPC-A&nbsp;&nbsp;&nbsp;Release Quantity : &nbsp;&nbsp;2 Bags&nbsp;&nbsp;&nbsp;Total Volume : &nbsp;&nbsp;200 ml&nbsp;&nbsp;&nbsp;</div>
			<div class="portlet-content">

			<table cellpadding="0" cellspacing="0" border="0" id="reagentsTable" class="display">
					<thead>
						<tr>
							<th colspan="1"><s:text name="stafa.label.bagid"/></th>
							<th colspan="1"><s:text name="stafa.label.volume"/></th>				
							<th colspan="1"><s:text name="stafa.label.tnc/kg(x10^8/kg)"/></th>
							<th colspan="1"><s:text name="stafa.label.cde34/kg(x10^6/kg)"/></th>
							<th colspan="1"><s:text name="stafa.label.cde3/kg(x10^6/kg)"/></th>
							<th colspan="1"><s:text name="stafa.label.enterid"/></th>
						</tr>
						<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>BMT123-3-1</td>
							<td>100ml</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><img src="images/icons/ok.png" width="20" height="20" border="0" /></td>
						</tr>
						<tr>
							<td>BMT123-3-2</td>
							<td>100ml</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><img src="images/icons/button_delete_blue.png" width="20" height="20" border="0" /></td>
						</tr>
					</tbody>
			</table>
		</div>
		</div>
		</div>
		</form>
	
	<form action="#" id="login">
	<div class="column">		
	<div  class="portlet">
		<div class="portlet-header notes"><s:hidden id="moduleName" value="releaseinfusion"/><s:hidden id="widgetName" value="infusioninfo"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.infusioninfo"/></div>
		<div class="portlet-content">
		<div id="table_inner_wrapper">
			<div id="left_table_wrapper">
				<div id="table_container_input">
					<div class="table_row_input1">
						<div class="table_cell_input1"><s:text name="stafa.label.labtechnician"/></div>
						<div class="table_cell_input2"><input type="text" name="" id="" placeholder="Lab Technician1"></div>												
					</div>		
					
					<div class="table_row_input2">
						<div class="table_cell_input1"><s:text name="stafa.label.datetime"/></div>
						<div class="table_cell_input2">Aug 02 2011, 11:00 CST</div>	
					</div>						
				</div>
			</div>			
			
			<div id="right_table_wrapper">
				<div id="table_container_input">
					<div class="table_row_input1">
						<div class="table_cell_input1"><s:text name="stafa.label.labtechnician"/></div>
						<div class="table_cell_input2"><input type="text" name="" id="" placeholder="Lab Manager1"></div>												
					</div>		
					
					<div class="table_row_input2">
						<div class="table_cell_input1"><s:text name="stafa.label.datetime"/></div>
						<div class="table_cell_input2">Aug 02 2011, 11:00 CST</div>
					</div>	
					
				</div>			
				
			</div>
		</div>
		
		<div id="table_inner_wrapper">
			<div id="left_table_wrapper">
				<div id="table_container_input">
					<div class="table_row_input1">
						<div class="table_cell_input1"><s:text name="stafa.label.infusingpersonnel"/></div>
						<div class="table_cell_input2"><input type="text" name="" id="" placeholder="Phyician 1"></div>												
					</div>		
					
					<div class="table_row_input2">
						<div class="table_cell_input1"><s:text name="stafa.label.datetime"/></div>
						<div class="table_cell_input2">Aug 02 2011, 00:00 CST</div>	
					</div>						
				</div>
			</div>			
			
			<div id="right_table_wrapper">
				<div id="table_container_input">
					<div class="table_row_input1">
						<div class="table_cell_input1"><s:text name="stafa.label.infusingpersonnel"/></div>
						<div class="table_cell_input2"><input type="text" name="" id="" placeholder="Nurse 1"></div>												
					</div>		
					
					<div class="table_row_input2">
						<div class="table_cell_input1"><s:text name="stafa.label.datetime"/></div>
						<div class="table_cell_input2">Aug 02 2011, 11:00 CST</div>
					</div>	
					
				</div>			
				
			</div>
		</div>
		</div>
		</div>
		</div>
	</form>
	<form>
	<div class="column">		
	<div  class="portlet">
		<div class="portlet-header notes"><s:hidden id="" value="releaseinfusion"/>Infustion Information<s:hidden id="pkNotes" value=""/></div>
		<div class="portlet-content">
		<div id="table_inner_wrapper">
			<div id="left_table_wrapper">
				<div id="table_container_input">
					<div class="table_row_input1">
					<table width="100%" border="0">
						<tr>
						<td width="50%" height="40px"><div class="txt_align_label" align="left">Route of Infusion</div></td>
						<td width="50%" height="40px"><div  class="mainselection" align="left"><select name="" class="" id=""><option value="1">Select</option><option>Frozen Gel Pack</option><option>Frozen Cord Blood Unit(s)</option><option>Room Temp per Transplantation</option></select></div></td>
					</tr>	
					<tr>
						<td width="50%" height="40px"><div class="txt_align_label" align="left">Was the entire Vol of product infused</div></td>
						<td width="50%" height="40px"><div align="left"><input type="radio" id="entirevol" name="proinfused" class="selectDeselect"/></a>Y &nbsp; <input type="radio" checked="checked" id="entirevolno" name="proinfused" class="selectDeselect"/> N</div></td>
					</tr>	
					</table>											
					</div>			
					<%-- <div class="table_row_input2">
						<table width="100%" border="0">
						<tr>
						<td width="50%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.datetime"/></div></td>
						<td width="48%" height="40px"><div class="txt_align_label" align="left">Aug 02 2011, 11:00 CST</div></td>	
						</tr>
						</table>
					</div>		 --%>				
				</div>
			</div>			
			
			<div id="right_table_wrapper">
				<div id="table_container_input">
					<div class="table_row_input1">
						<table width="100%" border="0" id="resportion">
						<tr>
						<td width="50%" height="40px"><div class="txt_align_label" align="left"></div></td>
						<td width="50%" height="40px"><div  class="mainselection" align="left"></div></td>
					</tr>
						<tr>
						<td width="50%" height="40px"><div class="txt_align_label" align="left">Fate of Reserved Portion</div></td>
						<td width="50%" height="40px"><div  class="mainselection" align="left"><select name="" class="" id=""><option value="1">Select</option><option>Discarded</option><option>Cryopreserved for future use</option></select></div></td>
						
					</tr>	
					</table>											
					
				</div>			
				
			</div>
		</div>	
	</div>	
</div>
</div>	
</form>	
</section>

<section>

<div class="cleaner"></div>
		<div  style="float:right;">
		<form>
			Next Process:&nbsp;&nbsp;<select name="" id="" class=""><option>Wash COBE 2991</option><option>Wash Manual</option><option>Infusion</option><option>Culture</option></select>&nbsp;
			<input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>
			<input type="button" value="Next"/>
		</form>
		</div>
		<div class="cleaner"></div>
</section>
<!-- </div> -->
			
<script>
$(function() {
	
	$( ".column" ).sortable({
		connectWith: ".column"
	});

	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
	.addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
	
	/**For Notes**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".notes" )
	.append("<span style=\"float:right;\" class='ui-notes'></span>");

	$( ".portlet-header .ui-notes " ).click(function() {
		showNotes('open',this);
	});
	/**For Refresh**/
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".reset" )
	.append("<span style=\"float:right;\" class='ui-reset'></span>");

	$( ".portlet-header .ui-icon" ).click(function() {
		$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
		$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
	});

	$('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});
	//$('.sortable').columnManager({listTargetID:'sortable', onClass: 'advon', offClass: 'advoff', hideInList: [1,3],
     //   saveState: true, colsHidden: [2,4]});
	//$( ".column" ).disableSelection();
});

</script>