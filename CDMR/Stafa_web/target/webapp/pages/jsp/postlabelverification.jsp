<%@ include file="common/includes.jsp" %>

<script>
function savePopup(){
	try{
		if(saveQuestions('postlabelverification')){
			alert("Data Saved Successfully");
			$("#labelverified").dialog('close');
		}
		
	}catch(e){
		alert("exception " + e);
	}
	return true;
}


$(function() {
	$('input[name=postisbtlblatt]').live("change",function(){
    	var isbtval=$(this).val();
    	if(isbtval==1){
    		    $('.isbtlabelshow').show();
    	}
    	if(isbtval==0){
		    $('.isbtlabelshow').hide();
	}
    });
    $('input[name=postwarnlblatt]').live("change",function(){
    	var isbtval=$(this).val();
    	if(isbtval==1){
    		    $('.warninglabel').show();
    	}
    	if(isbtval==0){
		    $('.warninglabel').hide();
	}
    });
    loadquestions();
});

function closepostlabelverification(){
	$("#labelverified").dialog('close');
}
</script>

<form id="postlabelverificationform" onsubmit="return avoidFormSubmitting()">
<%-- <s:hidden id="donor" name="donor" value="%{donor}"/>
<s:hidden id="donorProcedure" name="donorProcedure" value="%{donorProcedure}"/> --%>
	<table  id="postlabelverification" cellpadding="0"  cellspacing="0" class="" align="center" border="0" width="100%">
		<tr>
			<td class="isbtlabel">ISBT Label Attached/Affixed</td>
			<td><input type="hidden" name="postisbtlblatt_id" id="postisbtlblatt_id" value="0"><input type="radio" id="postisbtlblatt_yes" name="postisbtlblatt" value="1" class="comclass"/>Yes&nbsp;<input type="radio" name="postisbtlblatt" id="postisbtlblatt_no" value="0" class="comclass" />No<input type="hidden" name="questionsCode" id="questionsCode" value="postisbtlblatt" /></td>
			
		</tr>
		<tr class="isbtlabelshow" style="display:none;">
		
			<td align=""  style="padding-left:3%;">ISBT Label Complete and Legible</td>
			<td><input type="hidden" name="postisbtlblcom_id" id="postisbtlblcom_id" value="0"><input type="radio" id="postisbtlblcom_yes" name="postisbtlblcom" value="1" class="comclass"/>Yes&nbsp;<input type="radio" id="postisbtlblcom_no" name="postisbtlblcom" value="0" class="isbtcom"/>No<input type="hidden" name="questionsCode" id="questionsCode" value="postisbtlblcom" /></td>
			
		</tr>	
		<tr>
			<td >Warning Label Attached/Affixed</td>
			<td><input type="hidden" name="postwarnlblatt_id" id="postwarnlblatt_id" value="0"><input type="radio" id="postwarnlblatt_yes" name="postwarnlblatt"  value="1"/>Yes&nbsp;<input type="radio" id="postwarnlblatt_no"  name="postwarnlblatt" value="0"/>No&nbsp;<input type="radio" id="postwarnlblatt" name="postwarnlblatt" value="-1"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="postwarnlblatt" /></td>
			
		</tr>
		<tr class="warninglabel" style="display:none;">
		
			<td align="" style="padding-left:3%;">Warning:Reactive Test Results for</td>
			<td><input type="hidden" name="postwarnrecative_id" id="postwarnrecative_id" value="0"><input type="radio" id="postwarnrecative_yes" name="postwarnrecative" value="1"/>Yes&nbsp;<input type="radio" name="postwarnrecative" id="postwarnrecative_no" value="0"/>No&nbsp;<input type="radio" id="postwarnrecative_na" name="postwarnrecative" value="-1"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="postwarnrecative" /></td>
			
		</tr>	
		<tr class="warninglabel" style="display:none;">
		
			<td align="" style="padding-left:3%;">Warning:Advise Patient of Communicable Disease Risk</td>
			<td><input type="hidden" name="postwarndisease_id" id="postwarndisease_id" value="0"><input type="radio" id="postwarndisease_yes" name="postwarndisease" value="1"/>Yes&nbsp;<input type="radio" id="postwarndisease_no" name="postwarndisease" value="0"/>No&nbsp;<input type="radio" id="postwarndisease_na" name="postwarndisease" value="-1"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="postwarndisease" /></td>
			
		</tr>	
		<tr class="warninglabel" style="display:none;">
		
			<td align="" style="padding-left:3%;">Not Evaluated For Infectious Substances</td>
			<td><input type="hidden" name="postinfectioussubst_id" id="postinfectioussubst_id" value="0"><input type="radio" id="postinfectioussubst_yes" name="postinfectioussubst" value="1"/>Yes&nbsp;<input type="radio" id="postinfectioussubst_no" name="postinfectioussubst" value="0"/>No&nbsp;<input type="radio" id="postinfectioussubst_na" name="postinfectioussubst" value="-1"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="postinfectioussubst" /></td>			
		</tr>	
		<tr class="warninglabel" style="display:none;">
		
			<td align=""style="padding-left:3%;">Biohazard</td>
			<td><input type="hidden" name="postbiohazard_id" id="postbiohazard_id" value="0"><input type="radio" id="postbiohazard_yes" name="postbiohazard" value="1" />Yes&nbsp;<input type="radio" id="postbiohazard_no" name="postbiohazard" value="0"/>No&nbsp;<input type="radio" id="postbiohazard_na" name="postbiohazard" value="-1" />N/A<input type="hidden" name="questionsCode" id="questionsCode" value="postbiohazard" /></td>
			
		</tr>	
		<tr>
			<td>AUTO only Label Attached/Affixed</td>
			<td><input type="hidden" name="postautolblatt_id" id="postautolblatt_id" value="0"><input type="radio" id="postautolblatt_yes" name="postautolblatt" value="1"/>Yes&nbsp;<input type="radio" id="postautolblatt_no" name="postautolblatt" value="0" />No&nbsp;<input type="radio" id="postautolblatt_na" name="postautolblatt" value="-1"/>N/A<input type="hidden" name="questionsCode" id="questionsCode" value="postautolblatt" /></td>
			
		</tr>
	<!-- </table> -->
	<!-- <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%"> -->
		
		<tr>
			<td>Comments</td>
			<td><input type="hidden" name="postquestionsComments_id" id="postquestionsComments_id" value="0"><textarea id="postquestionsComments" name="postquestionsComments" rows="4" cols="50" > </textarea><input type="hidden" name="questionsCode" id="questionsCode" value="postquestionsComments" /></td>
			
		</tr>
	</table> 
</form>
  	<div align="right" style="float:right;">
		<form>
			<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td><span><input type="password" style="width:80px;" size="5" value="" id="eSignLabelVerification" name="eSignNext" placeholder="e-Sign"/>&nbsp;&nbsp;
					<input type="button" value="Save" onclick="verifyeSign('eSignLabelVerification','popup')"/>
					<input type="button" value="Cancel" onclick="closepostlabelverification()"/></span></td>
				</tr>
			</table>
		</form>
	</div>  
