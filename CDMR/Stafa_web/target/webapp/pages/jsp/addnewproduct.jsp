<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript">
var personFlag = false;
function getPersonName(personObj,response,id,name){
	if(response.acquisitionList.length<=0){
		personFlag=false;
		}
	$.each(response.acquisitionList, function(i) {
		var answer = confirm("Person Id is already exist. Do you want to use same person?");
        if (!answer){
        	if(this.recipientId==id){
        		$(personObj).val("");
    			$("#acquisitionNewForm #"+name).val("");
    			$("#acquisitionNewForm #"+name).removeAttr("readonly");
    			$("#acquisitionNewForm #recipientId").val("");
    			personFlag=false;
    		}
    		if(this.donorId==id){
    			$(personObj).val("");
    			$("#acquisitionNewForm #"+name).val("");
    			$("#acquisitionNewForm #donorId").val("");
    			$("#acquisitionNewForm #"+name).removeAttr("readonly");
    			personFlag=false;
    		}
            return false;
        }else{
        	if(this.recipientId==id){
    			$("#acquisitionNewForm #"+name).val(this.recipientName);
    			$("#acquisitionNewForm #"+name).attr("readonly","readonly");
    			personFlag=true;
    		}
    		if(this.donorId==id){
    			$("#acquisitionNewForm #"+name).val(this.donorName);
    			$("#acquisitionNewForm #"+name).attr("readonly","readonly");
    			personFlag=true;
    			
    		}
        	return false;
            }
	});
	//alert(personFlag);
	if(!personFlag){
		$("#acquisitionNewForm #"+name).removeAttr("readonly");
	}
}

$(document).ready(function() {
	$( "#Collectdate" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	        changeYear: true, yearRange: "c-50:c+45"});
    $("select").uniform();
    
    $("#acquisitionNewForm #recipientId" ).blur(function(){
		var url = "fetchAvailablePerson";
		var id = "personId="+$(this).val(); 
		result = jsonDataCall(url,id);
		getPersonName(this,result,$(this).val(),"recipientName");
     });
    $("#acquisitionNewForm #donorId" ).blur(function(){
		var url = "fetchAvailablePerson";
		var data = "personId="+$(this).val(); 
		result = jsonDataCall(url,data);
		getPersonName(this,result,$(this).val(),"donorName");
     });
    
});

$(function(){
    $("#acquisitionNewForm").validate({
    	rules:{
            "recipientId":"required",
             "recipientName":"required", 
            "donorId":"required",
             "donorName":"required", 
            "productId":"required"
            },
            messages:{
                "recipientId":"Please enter Recipient ID",
                 "recipientName":"Please enter Recipient Name", 
                "donorId":"Please enter Donor ID",
                 "donorName":"Please enter Donor Name", 
                "productId":"Please enter Product ID",
                	
                
            }
       });
});
	
	</script>
<form id="acquisitionNewForm">
			<table border="0" >
				<tr>
					<td  style="font-weight:bold"><s:text name="stafa.label.recipient" /></td>
				</tr>
				<tr>
					<td>
					<table width="110%" bgcolor="#ffffff" style="border-radius:10px;margin-left:5%;">
					
					<tr>
						<td style="padding:0% 0% 0% 2%" width="25%"><s:text name="stafa.label.recipientid" /></td>
						<td style="padding:1% 0% 0% 0%" width="25%"><input type="text" id="recipientId" value ="" name="recipientId"/></td>
						<td style="padding:0% 0% 0% 2%" width="25%"><s:text name="stafa.label.recipientname" /></td>
						<td style="padding:1% 0% 0% 0%" width="25%"><input type="text" id="recipientName" name="recipientName" placeholder="Last,First"/></td> 
					</tr>
					
					</table>
					</td>
				</tr>
				
				<tr>
					<td  style="font-weight:bold"><s:text name="stafa.label.donor" /></td>
				</tr>
				<tr>
					<td>
					<table width="110%" bgcolor="#ffffff" style="border-radius:10px; margin-left:5%;">
					
					<tr>
						<td style="padding:0% 0% 0% 2%" width="25%"><s:text name="stafa.label.donorid" /></td>
						 <td style="padding:1% 0% 0% 0%" width="25%"><input type="text" id="donorId" name="donorId"/></td>
						<td style="padding:1% 0% 0% 2%" width="25%"><s:text name="stafa.label.donorname" /></td>
						<td style="padding:1% 1% 0% 0%" width="25%"><input type="text" id="donorName" name="donorName" placeholder="Last,First" /></td>
						
					</tr>
					
	
					<tr>
						<td style="padding:0% 0% 0% 2%" width="25%"><s:text name="stafa.label.donorlocation" /></td>
						<td style="padding:1% 0% 0% 0%" width="25%">
				       		 <div class="mainselection"><s:select  headerKey="" headerValue="Select" list="donorLocList" listKey="description" listValue="description" name="donorLocation" id="fkCodelstPersonlocation"/></div>
				   		</td>
						<td style="padding:1% 0% 0% 2%" width="25%"><s:text name="stafa.label.collectsection" /></td>
						<td style="padding:1% 1% 0% 0%" width="25%"><input type="text" id="CollectSection" name="collectSection"/></td>
						 
					</tr>
					<tr>
						<td style="padding:1% 0% 0% 2%" width="25%"><s:text name="stafa.label.physicalname" /></td>
						<td style="padding:1% 1% 0% 0%" width="25%"><input type="text" id="Physicianname" name="physicianname"/></td>
						<td style="padding:1% 0% 0% 2%" width="25%"><s:text name="stafa.label.collectiondate" /></td>
					 <td style="padding:1% 0% 0% 0%" width="25%"> <input type="Text" id="Collectdate" name="collectionDate" size="30" class="dateEntry"></td>
					</tr>
					</table>
					</td>
				</tr>
				
				<tr>
					<td  style="font-weight:bold"><s:text name="stafa.label.product" /></td>
				</tr>
				<tr>
					<td>
					<table width="110%" bgcolor="#ffffff" style="border-radius:10px;margin-left:5%;">
					
					 <tr>
						<td style="padding:0% 0% 0% 2%" width="25%"><s:text name="stafa.label.productsource" /></td>
						<td style="padding:1% 0% 0% 0%" width="25%">
				       	 <div class="mainselection"><s:select  headerKey="" headerValue="Select" list="productSourceList" listKey="description" listValue="description" name="ProductType" id="fkCodelstProductsource"/></div>
				       	 <div style="display:none">
				         	<s:select headerKey="0" headerValue="Select" list="productSourceList" listKey="pkCodelst" listValue="description" name="fkCodelstProductsource1" id="fkCodelstProductsource1"/>
				         </div>
						</td>
						<td style="padding:1% 0% 0% 2%" width="25%"><s:text name="stafa.label.producttype" /></td>
						<td style="padding:1% 1% 0% 0%" width="25%">
				      	 <div class="mainselection"> <s:select  headerKey="" headerValue="Select"  list="productTypeList" listKey="description" listValue="description" name="ProductSource" id="fkCodelstProductType"/></div>
				      	 	<div style="display:none">
				        		<s:select headerKey="0" headerValue="Select" list="productTypeList" listKey="pkCodelst" listValue="description" name="fkCodelstProductType1" id="fkCodelstProductType1"/>
				        	</div>
						</td>
					</tr>
					
					<tr>
						<td style="padding:0% 0% 0% 2%" width="25%"><s:text name="stafa.label.proposedfinaldisposition" /></td>
						<td style="padding:1% 0% 0% 0%" width="25%">
				   <div class="mainselection"><s:select  headerKey="" headerValue="Select" list="finalDispList" listKey="description" listValue="description" name="ProposedFinalDisposition" id="fkCodelstFinalDisposition"/></div>
				    	</td>
				    	<td style="padding:1% 0% 0% 2%" width="25%"><s:text name="stafa.label.processingfacility" /></td>
						<td style="padding:1% 1% 0% 0%" width="25%"><input type="text" id="ProcessingFacility" name="processingFacility"/></td>
					</tr> 
					
					<tr>
						<td style="padding:0% 0% 0% 2%" width="25%"><s:text name="stafa.label.productid" /></td>
						<td style="padding:1% 0% 0% 0%" width="25%"><input type="text" id="productId" name="productId"/></td>
						 <td style="padding:1% 0% 0% 2%" width="25%"><s:text name="stafa.label.productappearance" /></td>
						<td style="padding:1% 1% 0% 0%" width="25%"><input name="productAppearance" type="Radio" id="Yes"/> Yes <input type="Radio" name="productAppearance"  id="NO"/> No <label id="error" class="error" for="productAppearance" generated="true"></label></td> 
					</tr>
					
					 <tr>
						<td style="padding:0% 0% 0% 2%" width="25%"><s:text name="stafa.label.productFrom" /></td>
						<td style="padding:1% 0% 0% 0%" width="25%"><div class="mainselection"><select name="fkCodelstProductFrom" id="fkCodelstProductFrom">
						<option value="0">Internal</option>
						<option value="1">External</option>
				       		 </select></div></td>
						<td style="padding:1% 0% 0% 2%" width="25%"><s:hidden name="workflow3" value="getAccession"></s:hidden>&nbsp;</td>
						<td style="padding:1% 1% 0% 0%" width="25%">&nbsp;</td>
					</tr> 
					</table>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr><td width="200%" align="right" ><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;<input type="button" name="Save" onclick="addNewProduct();" value="Save"/>&nbsp;<input type="button" name="Cancel" value="Cancel" onclick="closeNewAcquisition();"/></td></tr>
			</table>
			</form>