<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page import="com.velos.stafa.util.VelosStafaConstants"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script>
$(function() {
    $('.jclock').jclock();
    $("select").uniform(); 
  });
</script>
<form>
<s:hidden id="pkTxWorkflow_id" name="transplantId" /> 
<s:hidden id="recipientId" name="recipient" /> 
<s:hidden id="visitId" name="visitId" /> 
<input type="hidden" id="entityTypeId" value='<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_RECIPIENT"/>' />
<s:hidden id="personId" name="personId" />
<div class="column">
<div class="portlet">
<div class="portlet-header" style="font-size: large"><s:text
	name="stafa.label.stafa.label.Phisician-Task" /></div>
<div class="portlet-content">
<table cellpadding="0" cellspacing="0" border="1" id="insuranceapproval"
	class="display">
	<thead class="portlet-header">
		<tr>
			<th><s:text name="stafa.label.stafa.label.Item" /></th>
			<th><s:text name="stafa.label.stafa.label.Link" /></th>
			<th><s:text name="stafa.label.stafa.label.CompleteBefore" /></th>
		</tr>
	</thead>
	<s:iterator value="#request.physicianTasks" var="physicianTask">		
		<tbody class="taskTable">
			<tr>
				<td class="tablewapper1"><s:property
					value="%{#physicianTask[1]}" /></td>
				<td style="text-align: center;" class="tablewapper1"><a
					href="#">Link</a></td>
				<td class="tablewapper1"><s:property value="%{#physicianTask[3]}"/></td>
			</tr>
		</tbody>
	</s:iterator>
</table>

</div>
</div>
</div>
</form>





