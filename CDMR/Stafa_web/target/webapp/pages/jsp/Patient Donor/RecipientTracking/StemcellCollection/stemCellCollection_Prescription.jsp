<%-- <%@ include file="common/includes.jsp" %> --%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script>

$(function() {
    $('.jclock').jclock();
    //alert($(".portlet").find( ".portlet-header" ).find("span").length);
  });
function add_DonorRows(){
	  $("#recipientdids").prepend("<tr>"+
	            "<td><input type='checkbox' id=''/></td>"+
	            "<td><input type='text' id=''/></td>"+
	            "<td><input type='text' id=''/></td>"+
	            "<td><input type='text' id=''/></td>"+
	            "</tr>");
	}
  function showHiddenDiv(obj){
	  var donorType=$(obj).find("option:selected").text();
	 /*  if(donorType=="Allo"){
		  $("#recipientBasicInfo").css("display","block");
	  }else{
		  $("#recipientBasicInfo").css("display","none");				  
	  } */
	  
  }
  function   showFewDiv(obj){
	  var donorType=$(obj).find("option:selected").text();
	 if(donorType=="HPC-A"){
		  $("#hpc-A").css("display","block");
	  }else{
		  $("#hpc-A").css("display","none");
	  } 
	 if(donorType=="HPC-M"){
		  $("#hpc-M").css("display","block");
	  }else{
		  $("#hpc-M").css("display","none");
	  } 
	 if(donorType=="HPC-TC"){
		  $("#hpc-TC").css("display","block");
	  }else{
		  $("#hpc-TC").css("display","none");
	  } 
	 if(donorType=="HPC-Single"){
		  $("#physicianOff").css("display","none");
		  $("#mobilizationWidget").css("display","none");
		  $("#cellularInfo").css("display","none");
		  $("#DonorBasicInfo").css("display","none");
	  }else{
		  $("#physicianOff").css("display","block");
		  $("#mobilizationWidget").css("display","block");
		  $("#cellularInfo").css("display","block");
		  $("#DonorBasicInfo").css("display","block");;
	  } 
	  
  }
  function showDonorID(){
	    showPopUp('open','addmodal','Donor IDs','800','300');
	}

$(document).ready(function(){
	portletMinus();
 	$("#subDIvTitle").html("Stem-Cell Collection - Prescription and Orders");
    //$("select").uniform(); 
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"});
      	//show_eastslidewidget("slidedivs");
		 		
		 			var  oTable=$("#mobilizationCollection").dataTable({
						"sPaginationType": "full_numbers",
						"sDom": 'C<"clear">Rlfrtip',
						"aoColumns": [ { "bSortable":false},
							               null,
							               null,
							               null,
							               null,
							               null
							               ],
							"oColVis": {
								"aiExclude": [ 0 ],
								"buttonText": "&nbsp;",
								"bRestore": true,
								"sAlign": "left"
							},
							"fnInitComplete": function () {
						        $("#mobilizationCollectionColumn").html($('#mobilizationCollection_wrapper .ColVis'));
							}
				});
					    
		 		   
});
$("select").uniform(); 
</script>
<div class="cleaner"></div>
<form>
<div class="cleaner"></div>
<div class="column">
    <div  class="portlet">
    <div class="portlet-header">Stem Cell Prescription</div>
        <div class="portlet-content">
       
             <table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%">
				<tr>
					<td>Donor Type</td>
					<td ><select id='donorType' onchange="showHiddenDiv(this)"><option>Select</option><option>Auto</option><option>Allo</option></select></td>
				</tr>
				<tr>
					<td>Product Type</td>
					<td><select id='productType' onchange="showFewDiv(this)"><option>Select</option><option>HPC-A</option><option>HPC-M</option><option>HPC-TC</option><option>HPC-Single</option></select></td>
				</tr>
            </table><br>
            <div id="hpc-A" style="display:none">
            	<table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%">
            	<tr>
            		<td>Desired CD34/kg:</td>
            		<td class="tablewapper"><input type="checkbox"><label>2-5x10^6/kg</label></td>
            		<td class="tablewapper"><input type="checkbox"><label>>5x10^6/kg</label></td>
            		<td class="tablewapper"><input type="checkbox"><label>>10x10^6/kg</label></td>
            		<td class="tablewapper"><input type="checkbox"><label>Other</label></td>
            	</tr>
            	<tr>
            		<td></td>
            		<td></td>
            		<td></td>
            		<td>Specify Other:</td>
            		<td><input type="text"></td>
            	</tr>
            	<tr>
            		<td>Minimum Number of Bags:</td>
            		<td class="tablewapper"><input type="text"></td>
            		<td></td>
            		<td></td>
            		<td></td>
            	</tr>
            	</table>
            </div>
            <div id="hpc-M"  style="display:none">
            	<table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%">
            	<tr>
            		<td>Desired Total Nucleated Cells:</td>
            		<td><input type="text" name="" id="" style="width:70px;"><label> x10^8 Cells/kg x Recipient wt </label><input type="text" name="" id="" style="width:70px;"><label> kg = </label><input type="text" name="" id="" style="width:70px;"><label> x 10^8 cells</</label></td>
               	</tr>
            	</table>
            </div>
             <div id="hpc-TC"  style="display:none">
            	<table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%">
            	<tr>
            		<td width="28%"></td>
            		<td><input type="checkbox"><label>GSF mobilized T-cells</label></td>
            		<td><input type="checkbox"><label>No mobilization</label></td>
            		<td><input type="checkbox"><label>Other</label></td>
            	</tr>
            	<tr>
            		<td></td>
            		<td></td>
            		<td></td>
            		<td>Specify Other:</td>
            		<td><input type="text"></td>
            	</tr>
            	</table>
            	<table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%">
	            	<tr>
            		<td>Desired Total Nucleated Cells:</td>
            		<td><input type="text" name="" id="" style="width:70px;"><label> x10^8 Cells/kg x Recipient wt </label><input type="text" name="" id="" style="width:70px;"><label> kg = </label><input type="text" name="" id="" style="width:70px;"><label> x 10^8 cells</</label></td>
               	</tr>	
            	</table>
                    <table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%">
                    	<tr>
                    		<td>Minimum Number of Bags</td>
                    		<td><input type="text" name="" id=""></td>
                    		<td>Maximum CD3 cells/bag x 10^7/kg</td>
                    		<td><input type="text" name="" id=""></td>
                    	</tr>
                    </table>
            
            </div>
        </div>
    </div>
    </div>
    <div class="cleaner"></div>
    <div class="column">
    <div  class="portlet"  id="DonorBasicInfo">
        <div class="portlet-content">
       
             <table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%">
                <tr>
	                <td>Date of Donor Infectious Disease Markers:</td>
	                <td><input type="text" class="dateEntry"></td>
                </tr>
                <tr>
	                <td>Quarentine</td>
	                <td><input type="radio"  id="yes" name="qut">yes<input type="radio"  id="" name="qut">No</td>
                </tr>
                 <tr>
                <td>Reason</td>
                <td colspan="3"><textarea rows="3" cols="70"></textarea></td>
                </tr>
                <tr>
                    <td  width="15%" class="" rowspan="3">Collection Date(s)</td>
                    <td>
                     <div align="left"><img src = "images/icons/addnew.jpg" class="cursor"  onclick=""/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;&nbsp;&nbsp;
                       <img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label></div>
                    </td>
                    <td></td>
                    <td></td>
                <tr>   
                    <tr>
                    <td class="" colspan="3">
                        <table id="" cellpadding="0" cellspacing="0" class="display" align="center" border="1" width="80%">
                            <thead>
                            <tr>
                                <th>Select</th>
                                <th>Date</th>
                                <th>Status</th>
                            </tr>   
                            </thead>
                           
                            <tbody align="center">
                            <tr>
                                <td> <input type="checkbox" id="" ></td>
                                <td><input type="text" class="dateEntry"></td>
                                <td ><select id='donorType' onchange="showHiddenDiv(this)"><option>Select</option><option>Auto</option><option>Allo</option></select></td>
                            </tr>
                            <tr>
                                <td> <input type="checkbox" id="" ></td>
                                <td><input type="text" class="dateEntry"></td>
               					<td ><select id='donorType' onchange="showHiddenDiv(this)"><option>Select</option><option>Auto</option><option>Allo</option></select></td>

                            </tr>
                             </tbody>
                       
                        </table>

                    </td>
                    
                     </tr>
                     <tr>
	                <td>Site of Collection</td>
	                <td><input type="radio"  id="yes" name="qut">MASC<input type="radio"  id="" name="qut">apheresis Unit<input type="radio"  id="yes" name="qut">children's OR'<input type="radio"  id="" name="qut">other<input type="Text"></td>
                </tr>
	            </table>
        </div>
    </div>
    </div>
    <div class="cleaner"></div>
    <div class="column"> 
		<div class="portlet" id="cellularInfo">
			<div class="portlet-header">Cellular Theraphy Laboratory Product  Processing  Order(All products are tested for Sterility)</div>
			<div class="portlet-content">
				<table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%">
					<tr>
						<td><input type="radio" name=""><label>1. CryoPreservation</label></td>
					</tr>		
					<tr>
						<td><input type="radio" name=""><label>2. Fresh Infusion</label></td>
					</tr>
					<tr>
						<td><input type="radio" name=""><label>3.Therapeutic Cells(i.e donor Lymphocyte infusion)</label></td>
					</tr>
					<tr>
						<td><input type="radio" name=""><label>4. NMDP Collection</label></td>
					</tr>
					<tr>
						<td><input type="radio" name=""><label>5. Other</label></td>
					</tr>
				</table>
					<table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%">
					<tr>
							<td>Specify Other:</td>
							<td><input type="text" name="" id=""></td>
					</tr>
					<tr>
						     <td colspan="2"><textarea rows="3" cols="70" Placeholder="Comments"></textarea></td>
					</tr>
					</table>
			</div>
		</div>
		</div>
		<div class="cleaner"></div>
	<div class="column">   
            <div  class="portlet" id="mobilizationWidget">
                <div class="portlet-header ">Mobilization and Collection Orders</div>
                        <div class="portlet-content">
                        <table>
                            <tr>
                                <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                                <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                              
                            </tr>
                            </table>
                   	 <table cellpadding="0" cellspacing="0" border="0" id="mobilizationCollection" class="display">
                        <thead>
                            <tr>
                                <th width="4%" id="mobilizationCollectionColumn"></th>
                                <th colspan='1'>Select</th>
                                <th colspan='1'>Document Name</th>
                                <th colspan='1'>Document Complete</th>
                                <th colspan='1'>Attachment</th>
                                <th colspan='1'>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                        	<td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>Collection Orders</td>
                            <td><input type="text" id="" value="" class="dateEntry"/></td>
                            <td><img src="images/icons/attach.png"></td>
                            <td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
                        </tr>
                          <tr>
                          <td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>Mobilization Orders</td>
                            <td><input type="text" id="" value="" class	="dateEntry"/></td>
                            <td><img src="images/icons/attach.png"></td>
                            <td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
                        </tr>
                          <tr>
                          <td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>Line Placement Orders</td>
                            <td><input type="text" id="" value="" class="dateEntry"/></td>
                            <td><img src="images/icons/attach.png"></td>
                            <td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
                        </tr>
                        </tbody>
                        </table>
            </div>
        </div>
        </div>
        <div class="cleaner"></div>
        <div class="column">      
            <div  class="portlet"  id="physicianOff">
                        <div class="portlet-content">
                        <table cellpadding="0" cellspacing="0" border="0" id="" class="display">
                        	<tr>
                        		<td>Physician Sign Off</td>
                        		<td><input type="text" name="" id="">
                        		<td>Date</td>
                        		<td><input type="text" name="" id="" class="dateEntry">
                        </table>
            </div>
        </div>
</div>
<div class="cleaner"></div>
 <div  style="float:right;">
            <input type="button" value="Save"/>
        </div>
	</form>
<div class="cleaner"></div>