<%-- <%@ include file="common/includes.jsp" %> --%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script>

$(function() {
    $('.jclock').jclock();
    $("select").uniform(); 
    //alert($(".portlet").find( ".portlet-header" ).find("span").length);
    $( "#tabNew" ).tabs();
  });
  function showHiddenDiv(obj){
	  var donorType=$(obj).find("option:selected").text();
	 /*  if(donorType=="Allo"){
		  $("#recipientBasicInfo").css("display","block");
	  }else{
		  $("#recipientBasicInfo").css("display","none");				  
	  } */
  }
 /* function   showFewDiv(obj){
	  var donorType=$(obj).find("option:selected").text();
	 if(donorType=="HPC-M"){
		  $("#hpc-MSub").css("display","block");
		  $("#linePlacementdiv").css("display","none");
		  $("#mobilizationWidget").css("display","none");
		  $("#preCollectionLabs").css("display","none");
		  $("#verfication").css("display","none");
	  }else if(donorType=="HPC-Single"){
			 $("#hpc-MSub").css("display","none");
			  $("#linePlacementdiv").css("display","none");
			  $("#mobilizationWidget").css("display","none");
			  $("#preCollectionLabs").css("display","none");
			  $("#verfication").css("display","none");
			  $("#precollectionWidget").css("display","none");
		 }else{
			  $("#hpc-MSub").css("display","none");
			  $("#linePlacementdiv").css("display","block");
			  $("#mobilizationWidget").css("display","block");
			  $("#preCollectionLabs").css("display","block");
			  $("#verfication").css("display","block");
			  $("#precollectionWidget").css("display","block");
		  }   
	
	  
  }  */
  function showDonorID(){
	    showPopUp('open','addmodal','Donor IDs','800','300');
	}

$(document).ready(function(){
	portletMinus();
 	$("#subDIvTitle").html("Stem-Cell Collection  - Pre-Collection details");
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"});
//	show_eastslidewidget("slidedivs");
		 	/*	
		 			var  oTable=$("#mobilizationDetails").dataTable({
						"sPaginationType": "full_numbers",
						"sDom": 'C<"clear">Rlfrtip',
						"aoColumns": [ { "bSortable":false},
							               null,
							               null,
							               null,
							               null,
							               null
							               ],
							"oColVis": {
								"aiExclude": [ 0 ],
								"buttonText": "&nbsp;",
								"bRestore": true,
								"sAlign": "left"
							},
							"fnInitComplete": function () {
						        $("#mobilizationDetailsColumn").html($('#mobilizationDetails_wrapper .ColVis'));
							}
				});
		 			var  oTable=$("#preCollectionLabsTable").dataTable({
						"sPaginationType": "full_numbers",
						"sDom": 'C<"clear">Rlfrtip',
						"aoColumns": [ { "bSortable":false},
							               null,
							               null,
							               null,
							               null,
							               null,
							               null,
							               null
							               ],
							"oColVis": {
								"aiExclude": [ 0 ],
								"buttonText": "&nbsp;",
								"bRestore": true,
								"sAlign": "left"
							},
							"fnInitComplete": function () {
						        $("#preCollectionLabsColumn").html($('#preCollectionLabsTable_wrapper .ColVis'));
							}
				});    */
					    
		 		   
});

var preCollection={
		
		donorDetail:function(divId){
				var url = 'loadPreCollDonor.action';
				LoadedPage.url=url;
				LoadedPage.div='preCollectionDiv';
				this.loader(url, divId);
				
		},
		financial:function(divId){
			var url = 'loadFinancialAppoval.action';
			LoadedPage.url=url;
			LoadedPage.div='consultEval';
			url +='?personId=<s:property value="personId"/>';
			this.loader(url, divId);
			
		},
		preColDetail:function(divId){
			var recipientId=$("#recipientId").val();	
			var transplantID = $("#pkTxWorkflow_id").val();
			var url = "loadHPCAPage.action?recipientId="+recipientId+"&transplantId="+transplantID;
			LoadedPage.url=url;
			LoadedPage.div='consultEval';
			this.loader(url, divId);
			
		},
		 nextStep:function(divId){
			   var discCode ='preCollectionDisclaimer';
				var url = 'loadConsult_NextStepPage.action?transplantId='+'<s:property value="#request.transplantId"/>'+'&disc_code='+discCode;
				
				 openUrl(url,'','<s:property value="#request.visitId"/>','preCollectionDiv');
		   },
		   
		loader:function(url, divId){			
			$("#"+divId).html(ajaxCall(url,''));
		}
		
};

preCollection.donorDetail('preCollectionDiv');

/* function loadHPCAPage()
{
	var form="";
	
	var recipientId=$("#recipientId").val();	
	var transplantID = $("#pkTxWorkflow_id").val();
	var url = "loadHPCAPage.action?recipientId="+recipientId+"&transplantID="+transplantID;
	var result = ajaxCall(url,form);
	//alert(result);
	$("#mainDiv").html(result);
	
	//loadAction('loadHPCAPage.action?recipientId="+recipientId+"&transplantID="+transplantID);	
}

function loadFinancialApproval()
{
	var form="";
	var recipientId=$("#recipientId").val();
	var transplantID = $("#pkTxWorkflow_id").val();
	var url = "loadFinancialAppoval.action?recipientId="+recipientId+"&transplantID="+transplantID;
	var result = ajaxCall(url,form);
	//alert(result);
	$("#mainDiv").html(result);
}

function loadNextPage(){
	var form="";
	var url = "loadPreCollNext.action";
	var result = ajaxCall(url,form);
	$("#mainDiv").html(result);
}

function loadDonorMatching(){
	var form="";
	var url = "loadPreCollDonor.action";
	var result = ajaxCall(url,form);
	$("#mainDiv").html(result);
} */

</script>

<div class="cleaner"></div>
<form>
<s:hidden id="pkTxWorkflow_id" name="transplantId"/>
<s:hidden id="recipientId" name="recipient"/>
<s:hidden id="personId" name="personId"/>
<div id="tabNew">
		<div id="subDIvTitle"></div>
			 <ul>
				<li><a id="tabs-1" href="javascript:void(0);" onclick="preCollection.donorDetail('preCollectionDiv');">Donor Details</a></li>
			   	<li><a id="tabs-2" href="javascript:void(0);" onclick="preCollection.preColDetail('preCollectionDiv');">Pre-Collection Details</a></li>
			   	<li><a id="tabs-3" href="javascript:void(0);" onclick="preCollection.financial('preCollectionDiv');">Financial Approvals</a></li>
			   	<li><a id="tabs-4" href="javascript:void(0);" onclick="preCollection.nextStep('preCollectionDiv');">Next Step</a></li>
			</ul>
		</div>
 </form>
<div class="cleaner"></div>
<div id="preCollectionDiv"></div>


<!--

<div class="cleaner"></div>
<form>
  <div class="cleaner"></div>
 <div class="column">
    <div  class="portlet">
    <div class="portlet-header">Collection Type</div>
        <div class="portlet-content">
             <table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%">
				<tr>
					<td>Donor Type</td>
					<td ><select id='donorType' onchange="showHiddenDiv(this)"><option>Select</option><option>Auto</option><option>Allo</option></select></td>
				</tr>
				<tr>
					<td>Product Type</td>
					<td><select id='productType' onchange="showFewDiv(this)"><option>Select</option><option>HPC-A</option><option>HPC-M</option><option>HPC-TC</option><option>HPC-Single</option></select></td>
				</tr>
            </table><br>
       </div>
    </div>
</div>
  <div class="cleaner"></div>
<div class="column">
	<div class="portlet" id="linePlacementdiv">
		<div class="portlet-header">Line Placement Details</div>
			<div class="portlet-content">
				<table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%">
					<tr>
						<td>Central Line Placed?</td>
						<td><input type="radio" name="" id="">Yes<input type="radio" name="" id="">No</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Site of Central Line Placement:</td>
						<td><select id='siteCentralLine' onchange=""><option>Select</option><option>Femoral</option><option>Subclavin</option><option>Internal Jugular</option><option>Other</option></select></td>
						<td>Specify Other</td>
						<td><input type="text" name="" id=""></td>
					</tr>
					<tr>
						<td>Line Placement Date:</td>
						<td><input type="text" name="" id="" class="dateEntry"></td>
						<td>	Status</td>
						<td><select id='siteCentralLine' onchange=""><option>Select</option><option>Conformed</option></select></td>
					</tr>
					<tr>
						<td>Line Placement Nurse:</td>
						<td><input type="Text" name="" id=""></td>
						<td></td>
						<td></td> 
					</tr>
					<tr>
						<td>Line Placement Location:</td>
						<td><input type="Text" name="" id=""></td>
						<td></td>
						<td></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	  <div class="cleaner"></div>
		<div class="column">   
            <div  class="portlet" id="mobilizationWidget">
                <div class="portlet-header ">Mobilization Details</div>
                        <div class="portlet-content">
                        	<table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%">
                        	<tr>
                        		<td>Mobilization Start Date</td>
                        		<td><input type="text" id="" name="" class="dateEntry">
                        		<td>Status</td>
								<td><select id='siteCentralLine' onchange=""><option>Select</option><option>Femoral</option><option>Subclavin</option><option>Internal Jugular</option><option>Other</option></select></td>
                        	</tr>
                        	<tr>
                        		<td>Mobilization Schedule:</td>
                        		<td></td>
                        		<td></td>
                        		<td></td>
                        	</tr>
                        </table>
                        <table>
                            <tr>
                                <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                                <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                              
                            </tr>
                            </table>
                   	 <table cellpadding="0" cellspacing="0" border="0" id="mobilizationDetails" align="center" class="display">
                        <thead>
                            <tr>
                                <th width="4%" id="mobilizationDetailsColumn"></th>
                                <th colspan='1'>Select</th>
                                <th colspan='1'>Document Name</th>
                                <th colspan='1'>Document Complete</th>
                                <th colspan='1'>Attachment</th>
                                <th colspan='1'>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                        	<td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>Collection Orders</td>
                            <td><input type="text" id="" value="" class="dateEntry"/></td>
                            <td><img src="images/icons/attach.png"></td>
                            <td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
                        </tr>
                          <tr>
                          <td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>Mobilization Orders</td>
                            <td><input type="text" id="" value="" class	="dateEntry"/></td>
                            <td><img src="images/icons/attach.png"></td>
                            <td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
                        </tr>
                          <tr>
                          <td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>Line Placement Orders</td>
                            <td><input type="text" id="" value="" class="dateEntry"/></td>
                            <td><img src="images/icons/attach.png"></td>
                            <td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
                        </tr>
                        </tbody>
                        </table><br><br>
                        <div id="Notes"  style="margin-left:0%">
                        	<table cellpadding="0" cellspacing="8" class="" align="" border="0" width="85%">
                        		<tr>
                        			<td>MobilizationNotes:</td>	
                        		</tr>
                        		<tr>
                        			<td>
                        				<div id="mobNotes" style="padding:2%;width:85%;background-color:white;" >
                        						<table>
                        									<tr>
                        										<td><span style="font-weight:bold;text-decoration:underline" >Sunday, April 8, 2012</span></td>
                        									</tr>
                        									<tr>
                        										<td>11:00am- Go to 4th floor, BMT unit, Methodist Children's 	Hospital for labs'</td>
                        									</tr>
                        									<tr>
                        										<td><span style="font-weight:bold;" >* Patient should have nothing to eat or drink after midnight on Sunday </span></td>
                        									</tr>
                        									<tr><td></td></tr>
                        									<tr><td></td></tr>
                        									
                        									<tr>
                        										<td><span style="font-weight:bold;text-decoration:underline" >Monday, April 9, 2012</span></td>
                        									</tr>
                        									<tr>
                        										<td>7:30am- Go to Children's Radiology to register for GFR and line Placement . Take Gold elevator's 'on the first floor.</td>
                        									</tr>
                        									<tr>
                        										<td>8:00am- GFR  in Nuclear  Medicine, labs will be drawn ar 9:00am to 11:00 am.</td>
                        									</tr>
                        									<tr>
                        										<td>10:00am- Line Placement in Interventional Radiology.</td>
                        									</tr>
                        									<tr>
                        										<td>12:00am- Collection on the 10th floor..</td>
                        									</tr>
                        						</table>
                        					
                        				</div>
                        			</td>
                        		</tr>
                        	</table>
                        	
                        </div>
            </div>
   
        </div>
</div>
  <div class="cleaner"></div>
<div class="column">   
            <div  class="portlet" id="preCollectionLabs">
                <div class="portlet-header ">Pre-Collection Labs</div>
                        <div class="portlet-content">
                        <table>
                            <tr>
                                <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                                <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                              
                            </tr>
                            </table>
                   	 <table cellpadding="0" cellspacing="0" border="0" id="preCollectionLabsTable" class="display">
                        <thead>
                            <tr>
                                <th width="4%" id="preCollectionLabsColumn"></th>
                                <th colspan='1'>Select</th>
                                <th colspan='1'>Test</th>
                                <th colspan='1'>Day of Mobilization</th>
                                <th colspan='1'>Date</th>
                                <th colspan='1'>Result</th>
                                <th colspan='1'>Status</th>
                                <th colspan='1'>Flag</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                        	<td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>CBC</td>
                            <td>Day 4</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><input type="radio" name="" id=""></td>
                        </tr>
                          <tr>
                         <td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>Peripheral CD34 Count</td>
                            <td>Day 4</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><input type="radio" name="" id=""></td>
                        </tr>
                          <tr>
                          	<td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>CBC</td>
                            <td>Day 5</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><input type="radio" name="" id=""></td>
                        </tr>
                        <tr>
                        	<td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>Peripheral CD34 Count</td>
                            <td>Day 5</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><input type="radio" name="" id=""></td>
                          </tr>
                        </tbody>
                        </table>
            </div>
        </div>
</div>
  <div class="cleaner"></div>
   <div class="column">
<div class="portlet" id="verfication">
	<div class="portlet-header"></div>
	<div class="portlet-content">
		<table cellpadding="0" cellspacing="8" class="" align="" border="0" width="85%">
			<tr>
				<td><input type="checkbox" id="" class=""><label> Donor Ready for Apheresis </label></td>
				<td><label> Date </label><input type="Text" name="" id="" class="dateEntry"></td>
			</tr>
		</table>
	</div>
</div>
</div>
  <div class="cleaner"></div>
   <div class="column">
<div class="portlet" id="precollectionWidget">
	<div class="portlet-header" >Pre-Collection Details</div>
	<div class="portlet-conent">
	<table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%" id="hpc-MSub" style="display:none">
		<tr>
			<td>Admission Date:</td>
			<td><input type="text" name="" id="" class="dateEntry">
			<td>Status</td>
            <td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
		</tr>
		<tr>
			<td>Anesthesia Location:</td>
			<td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
			<td></td>
			<td></td>
		</tr>
	</table>
		<table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%">
                <tr>
                    <td  width="15%" class="" rowspan="3">Colection  Date(s)</td>
                    <td>
                     <div align="left"><img src = "images/icons/addnew.jpg" class="cursor"  onclick=""/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;&nbsp;&nbsp;
                       <img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label></div>
                    </td>
                    <td></td>
                    <td></td>
                    <tr>
                    <tr>
                    <td class="" colspan="3">
                        <table id="" cellpadding="0" cellspacing="0" class="display" align="center" border="1" width="80%">
                            <thead>
                            <tr>
                                <th>Select</th>
                                <th>Dates</th>
                                <th>Status</th>
                            </tr>   
                            </thead>
                            <tbody align="center">
                            <tr>
                                <td> <input type="checkbox" id="" ></td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td> <input type="checkbox" id="" ></td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td> <input type="checkbox" id="" ></td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                     <td></td>
                   </tr>
                  
            </table>
	</div>
</div>
</div>
 </form>
<div class="cleaner"></div>
-->