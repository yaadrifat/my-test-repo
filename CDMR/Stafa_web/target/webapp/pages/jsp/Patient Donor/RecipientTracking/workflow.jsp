<%-- <%@ include file="common/includes.jsp" %> --%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/workFlow.js"></script>
<link type="text/css" href="css/rec_workFlow.css" rel="stylesheet" />
<style>
.fontRed a{
color:red !important;
}
.workFlowContainer{
background-color:	#FFFFFF;;
border-radius:5px;
}
.subLinkInsurance{
border:1px solid white;
font-weight:bold;
width:60%;
text-align:center;
line-height:1.3 em;
padding:2%;
background-color:#DADADA;
}
#subMenuWrapper{
background-color:	#B5B5B5;
border-radius:5px;
}
.subDivPortlet{
background-color:	#DADADA;
border-radius:5px;
}
.Selected{
background-color:yellow;
}
</style>

<script>
var LoadedPage = {url:'',workflow:'',visitId:'', div:''};

function openUrl(url,workflow,visitId,div){
	LoadedPage.url = url;
	LoadedPage.workflow = workflow;
	LoadedPage.visitId = visitId;
	LoadedPage.div = div;
	$("#dynamicheader").html(workflow);
	if(url.indexOf("#1a")!=-1)
	{
      url = url.replace('#1a','<s:property value="#request.transplantId"/>');      
	}
	if(url.indexOf("transplantId")==-1){
      url = url+"?transplantId="+'<s:property value="#request.transplantId"/>';
	}
	url = url + "&recipient="+'<s:property value="%{recipientD.pkRecipient}"/>'+"&personId="+'<s:property value="%{recipientD.person.pkPerson}"/>'+"&visitId="+visitId;
	//alert(url);
	//var url = "loadRecipientDetails.action?recipient="+<s:property value="%{recipient.pkRecipient}"/>;
	var form="";	
	var result = ajaxCall(url,form);	
	$("#"+div).html(result);
	
}

function checkTabHighlight(){
	$(".subLinkInsurance").each(function(){
		if($(this).hasClass("selected")){
			$(this).removeClass("selected");
		}	
	});
}
function consult(){
	$("#dynamicheader").html("Consult/Eval");
	var form="";
	var url = "loadConsultPage";
	var result = ajaxCall(url,form);
	$("#subdiv").html(result);
}
function preTransplant(){
	$("#dynamicheader").html("Pre-Transplant Visit");
	var form="";
	var url = "loadPreTransplantPage";
	var result = ajaxCall(url,form);
	$("#subdiv").html(result);
}
function stemCellCollection(){
	$("#dynamicheader").html("Stem Cell Collection");
	var form="";
	var url = "loadStemCellCollectionPage";
	var result = ajaxCall(url,form);
	$("#subdiv").html(result);
}
/* function show_eastslidewidget(divid){
    // to Hide the left side widget
	innerLayout.hide('west');
    innerLayout.show('south',true);
    innerLayout.show('north',true);
    innerLayout.show('east',false);
    var src = $("#"+divid).html();
    $("#innereast").html(src);
    $("#"+divid).html("slidedivs");
   
}  */
$(function() {
    $('.jclock').jclock();
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

  });
$(document).ready(function(){
	calculateHeight();
    $(window).resize(calculateHeight);
	show_slidewidgets("donorInfo",true);
	show_slidecontrol("slidedivs");
    //$("select").uniform(); 
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"});
      $(window).resize();
      show_innernorth();
	//show_eastslidewidget("slidedivs");
	});
/* $(window).resize(function(){
    var height;
    var screenheight=screen.height;
    var innerheight=$("#innercenter").height();
    var innerheight=594;
    var constanthgt=$("#constantdiv").height();
     if(innerheight) 
    if(screenheight<= 768){
       height =innerheight -(constanthgt+240);
    }else if(screenheight>=1024){
           height =innerheight -(constanthgt+40);

    }else if(screenheight>=800 && screenheight < 900){
           height =innerheight -(constanthgt+240);
    }else if(screenheight==900){
           height =innerheight -(constanthgt+130);
    }else if(screenheight==960){
           height =innerheight -(constanthgt+80);
    }
       $('#subdiv').height(height);
    }); */
    
    function ShowdonorTraker(){
    	//alert($("#donorMatching").attr("class"));
    /* 	if($("#donorMatching").hasClass("fontBlue")){
    		$("#donorMatching").removeClass("fontBlue");
    		$("#donorMatching").addClass("fontRed");
    	}else{
    		$("#donorMatching").removeClass("fontRed");
    		$("#donorMatching").addClass("fontBlue");
    	} */ 	
    	//alert("after===="+	$("#donorMatching").attr("class"));
    	$("#dynamicheader").html("Donor Matching");
    	checkTabHighlight();
    	//$("#donorMatching").addClass("Selected");
    	var form="";
    	var url = "loadDonorMatching";
    	var result = ajaxCall(url,form);
    	$("#subdiv").html(result);
    }
    
function add_recipientsrows(){
	   //alert("!");
    $("#recipiendids").prepend("<tr>"+
            "<td><input type='checkbox' id=''/></td>"+
            "<td><input type='text' id=''/></td>"+
            "<td><input type='text' id=''/></td>"+
            "<td><input type='text' id=''/></td>"+
            "</tr>");
   
}
function showRecipientID(){
    showPopUp('open','addmodal','Recipient IDs','800','300');
}
$("select").uniform(); 

var contentheight;
function calculateHeight(){
    mainheight=$("#innercenter").height();
    var titlediv=$("#workflowcontainerID").height()
    var titlediv=$("#workflowcontainerID").height();
	// var contentdivheight=$("#subdiv").height();
    contentheight=(mainheight-titlediv)/16;
    //alert("contentheight"+contentheight);
    $("#subdiv").css("height",contentheight +"em");
}
var mainheight;

function loadRecipientProfile(){

	var url = "loadRecipientProfile.action?personId="+'<s:property value="%{recipientD.person.pkPerson}"/>'+"&recipient="+'<s:property value="%{recipientD.pkRecipient}"/>'+"&transplantId="+'<s:property value="#request.transplantId"/>';
	LoadedPage.url = url;
	LoadedPage.workflow = '';
	var result = ajaxCall(url);
	$("#main").html(result); 	
}

function loadPhysicianTask(){

	var url = "loadPhysicianTaskPage.action?personId="+'<s:property value="%{recipientD.person.pkPerson}"/>'+"&recipient="+'<s:property value="%{recipientD.pkRecipient}"/>'+"&transplantId="+'<s:property value="#request.transplantId"/>';
	LoadedPage.url = url;
	LoadedPage.workflow = '';
	var result = ajaxCall(url);
	$("#main").html(result); 	
}

var  personIdOnIns=" ";
function loadRecipientInsurancePage(){
        personIdOnIns=<s:property value="%{recipientD.person.pkPerson}"/>;
		var url = "loadRecipientInsurance.action?transplantId="+'<s:property value="#request.transplantId"/>'+"&personInsId="+personIdOnIns;
		LoadedPage.url = url;
		LoadedPage.workflow = '';
		var result = ajaxCall(url);
		$("#main").html(result); 
	}

var RefreshElement={
		firstName:'firstName',
		lastName:'lastName',
		mrn:'mrn',
		dob:'dob',
		age:'age',
		registryorCBU:'registryorCBU',
		aboRh:'aboRh',
		department:'department'
};


var Refreshner=function(eleObj, dataObj){
	 for(var prop in eleObj) {
		    if(eleObj.hasOwnProperty(prop) && dataObj[prop]!=null && dataObj[prop]!=='' && dataObj[prop]!= undefined){
			    $('#'+eleObj[prop]).html(dataObj[prop]);
		        }
		}
}
</script>

<div class="cleaner"></div>

<!--right window start -->	
<div  id="slidedivs"  style="display:none">
<div id="uldiv">
<ul class="ulstyle">
<li class="listyle print_label"><img src = "images/icons/calendar.png"  style="width:30px;height:30px;"/></li>
<li class="listyle print_label" ><b>Calendar</b> </li>
<li class="listyle print_label"><img src = "images/icons/plan.png"  style="width:30px;height:30px;"/></li>
<li class="listyle notes_icon"><b>Plan</b> </li>
<li class="listyle print_label"><img src = "images/icons/notes.png"  style="width:30px;height:30px;"/></li>
<li class="listyle print_label" ><b>Notes</b> </li>
</ul>
</div>
</div>
<div class="column">
<div  id="donorInfo"  style="display:none">
<div class="portlet">
	<div class="portlet-header">Recipient Info</div>
		<div class="portlet-content">
									<table width="100%" border="0">
									  <tr>
										<td width="50%" style="padding:2%">First Name:</td>
										<td width="50%" id="firstName" name="firstName" style="padding:2%"><s:property value="%{recipientD.person.personFname}"/></td>
									  </tr>
									  <tr>
										<td style="padding:2%">Last Name:</td>
										<td id="lastName" name="lastName" style="padding:2%"><s:property value="%{recipientD.person.personLname}"/></td>
									  </tr>									  
									  <tr>
										<td style="padding:2%">MRN:</td>
										<td id="mrn" name="mrn" style="padding:2%"><s:property value="%{recipientD.person.personMRN}"/></td>
									  </tr>
									  <tr>
										<td style="padding:2%">DOB:</td>
										<td id="dob" name="dob" style="padding:2%">
										   <s:date name="%{recipientD.person.personDob}" id="dob" format="MMM dd, yyyy" />
										   <s:property value="%{dob}"/>
										</td>
									  </tr>
									   <tr>
										<td style="padding:2%">Age:</td>
										<td id="age" name="age" style="padding:2%"><s:property value="%{recipientD.age}"/></td>
									  </tr>
									   <tr>
										<td style="padding:2%">Registry or CBU:</td><s:property value="%{recipientD.person.personMRN}"/>
										<td id="registryorCBU" name="registryorCBU" style="padding:2%">N/A</td>
									  </tr>
									   <tr>
										<td style="padding:2%">ABO/Rh:</td>
										<td id="aboRh" name="abo/Rh" style="padding:2%"><s:property value="getCodeListDescById(recipientD.fkCodelstAborh)" /></td>
									  </tr>
									  <tr>
										<td style="padding:2%">Department:</td>
										<td id="department" name="department" style="padding:2%">Adults</td>
									  </tr>
									  <tr>
										<td style="padding:2%">Quicklinks:</td>
										<td id="recipientProfile" name="recipientProfile" style="padding:2%"><a href="#" onclick="javascript:loadRecipientProfile();" src="" id="" name="" style="color:blue">Recipient Profile</a></td>
									  </tr>
									  <tr>
										<td></td>
										<td id="donorMatching"  name="donormatching" style="padding:2%" class="fontBlue"><a href="#" src="" id="" name="" style="color:blue" onclick="ShowdonorTraker()">Donor Matching</a></td>
									  </tr>
									   <tr>
										<td></td>
										<td id="insurance"  name="insurance" style="padding:2%" class="fontBlue"><a href="#" src="" id="" name="" style="color:blue" onclick="javascript:loadRecipientInsurancePage();">Insurance</a></td>
									  </tr>
									  <tr>
										<td></td>
										<td id="availableProducts"  name="availableProducts" style="padding:2%" class="fontBlue"><a href="#" src="" id="" name="" style="color:blue" onclick="()">Available Products</a></td>
									  </tr>
									  <tr>
										<td></td>
										<td id="physicianTask"  name="physicianTask" style="padding:2%" class="fontBlue"><a href="#" src="" id="" name="" style="color:blue" onclick="avascript:loadPhysicianTask();">Physician Tasks</a></td>
									  </tr>
									</table>
								</div>
						</div>
						</div>
						
<!-- <div  class="portlet"> -->
<!-- 	<div class="portlet-header" id="dynamicheader">Referral</div> -->
		<div class="portlet-content"  id="workflowcontainerID" style="height:50px;">
			<!-- <div id="subMenuWrapper" >
				<table cellpadding="0" cellspacing="0" border="0" id="" class="display">
						<tr>
							<td style="padding-right:20px"><div class="subLinkInsurance" id="recipientWorkflow">Recipient Workflow</div></td>
							<td style="padding-right:20px"><div class="subLinkInsurance" id="recipientProfile">Recipient Profile</div></td>
							<td style="padding-right:20px"><div class="subLinkInsurance" id="insurance">Insurance</div></td>
							<td style="padding-right:20px"><div class="subLinkInsurance" onclick="ShowdonorTraker()" id="donorMatching">Donor Matching</div></td>
							<td style="padding-right:20px"><div class="subLinkInsurance" id="availableProducts">Available Products</div></td>
						</tr>
			</table>
			</div><br> -->
				<!-- <div class="subDivPortlet">
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
							<tr>
								<td align="right" style="padding-top:1%">Recipient First Name : </td>
								<td  align="left" style="padding-top:1%">&nbsp;Renee</td>
								<td align="right" style="padding-top:1%">Diagnosis :</td>
								<td  align="left" style="padding-top:1%">&nbsp;AML</td>
								<td  align="right" style="padding-top:1%">Age :</td>
								<td  align="left" style="padding-top:1%">&nbsp;24</td>
								<td  align="right" style="padding-top:1%">Diagnosis :</td>
								<td  align="left" style="padding-top:1%">&nbsp;Peds</td>
						</tr>
						<tr>
								<td  align="right" style="padding-top:1%">Recipient Last Name : </td>
								<td  align="left" style="padding-top:1%">&nbsp;Branofsky</td>
								<td  align="right" style="padding-top:1%">Diagnosis Date:</td>
								<td  align="left" style="padding-top:1%">&nbsp;Jan 6, 2012</td>
								<td  align="right" style="padding-top:1%">SSN:</td>
								<td  align="left" style="padding-top:1%">&nbsp;465983875</td>
								<td  align="right">ABO/Rh:</td>
								<td  align="left">&nbsp;A+</td>
						</tr>
						<tr>
							<td  align="right" style="padding-top:1%">Status:</td>
							<td style="padding-top:1%">&nbsp;Pre-Transplant</td>
							<td  align="right" style="padding-top:1%">DOB :</td>
							<td style="padding-top:1%">&nbsp;Jan 19, 1988</td>
							<td  align="right" style="padding-top:1%">MRN :</td>
							<td style="padding-top:1%">&nbsp;586790</td>
						</tr>
							
					</table>
				</div> -->  
		<div class="workFlowContainer">
			 <div class="workFlow">	
			      <s:set name="process" value="0" />
			      <s:iterator value="#request.workflowList" var="row" >			       
			         <div class='workflowContent' >			        
				 		<div 
				 		  <s:if test="%{#row[1]==0 && #process==0}">class='processstarted'</s:if> 
				 		  <s:if test="%{#row[1]==0 && #process==1}">class='processinitial'</s:if>
				 		  <s:if test="%{#row[1]==1 && #process==0}">class='processinitial'</s:if>
				 		  <s:if test="%{#row[1]==1 && #process==1}">class='processinitial'</s:if>
				 		onclick=''></div>
				 		<div 
				 		   <s:if test="%{#row[1]==0 && #process==0}">class='onprogress' onclick="openUrl('<s:property value="%{#row[2]}"/>','<s:property value="%{#row[0]}"/>','<s:property value="%{#row[3]}"/>','subdiv')"</s:if> 
				 		  <s:if test="%{#row[1]==0 && #process==1}">class='initial' onclick="openUrl('<s:property value="%{#row[2]}"/>','<s:property value="%{#row[0]}"/>','<s:property value="%{#row[3]}"/>','subdiv')" </s:if>
				 		  <s:if test="%{#row[1]==1 && #process==1}">class='completed' onclick="openUrl('<s:property value="%{#row[2]}"/>','<s:property value="%{#row[0]}"/>','<s:property value="%{#row[3]}"/>','subdiv')"</s:if>
				 		  <s:if test="%{#row[1]==1 && #process==0}">class='completed' onclick="openUrl('<s:property value="%{#row[2]}"/>','<s:property value="%{#row[0]}"/>','<s:property value="%{#row[3]}"/>','subdiv')"</s:if>
				 		><p class='samplet-text'><s:property value="%{#row[0]}"/></p></div>
				     </div>
				     <s:if test="%{#row[1]==0 && #process==0}">
				       <s:set name="process" value="1" />	
				       <script>
				         LoadedPage.url = '<s:property value="%{#row[2]}"/>';
				         LoadedPage.workflow = '<s:property value="%{#row[0]}"/>';
				         LoadedPage.visitId = '<s:property value="%{#row[3]}"/>';
				         LoadedPage.div = 'subdiv';
				       </script>			     
				     </s:if>
			      </s:iterator>		    
				 <!-- div class='workflowContent' >
				 		<div class='processstarted' id="referalProcess" onclick=''></div>
				 		<div class='onprogress' id="referalID" onclick='check(this)'><p class='samplet-text'>Referal</p></div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' id="consultProcess" onclick=''></div>
				 	<div class='initial'  id="consultID" onclick='check(this),consult(),checkBox(this)'>
				 		<p class='samplet-text'>Consult/Eval</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' id="preTransplantProcess"'></div>
				 	<div class='initial' id="preTransplantID" onclick='check(this),checkBox(this),preTransplant()'>
				 		<p class='samplet-text'>Pre-Transplant Visit</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' id="stemCellProcess"onclick=''></div>
				 	<div class='initial'  id="stemCellCollectionID" onclick='check(this),checkBox(this),stemCellCollection()'>
				 		<p class='samplet-text'>Stem Cell Collection</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' id="PreparativeProcess" onclick=''></div>
				 	<div class='initial'  id=" preprativeRegimenID" onclick=''>
				 		<p class='samplet-text'>Preparative Regimen</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' id="selectProductProcess" onclick=''></div>
				 	<div class='initial'  id="selectProductID" onclick=''>
				 		<p class='samplet-text'>	Selected Products</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' id="infusionProcess" onclick=''></div>
				 	<div class='initial'  id="infusuionID" onclick=''>
				 		<p class='samplet-text'>Infusion</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
					 <div class='processinitial' id="followupProcess"onclick=''></div>
					 <div class='initial' id="followUpID" onclick=''>
					 	<p class='samplet-text'>Followup</p>
					 </div>
					 </div-->
					 
			</div>
		</div>
<!-- 		</div> -->
<!-- 	</div> -->
</div>
<div class="cleaner"></div>
<div id="subdiv" style="overflow-y:auto">

   
</div>
</div>

<script>

   openUrl(LoadedPage.url,LoadedPage.workflow,LoadedPage.visitId,LoadedPage.div);
</script>
<!-- <script type="text/javascript">



var now = new dateEntry();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivaldateEntry').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new dateEntry()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};
$("#timeZone").html(timezones[-offset / 60]);

</script>
 -->













