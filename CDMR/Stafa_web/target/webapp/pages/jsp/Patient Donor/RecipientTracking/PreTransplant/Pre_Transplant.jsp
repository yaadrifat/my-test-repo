<%-- <%@ include file="common/includes.jsp" %> --%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/workFlow.js"></script>
<link type="text/css" href="css/workFlow.css" rel="stylesheet" />
<style>
/* .workFlowContainer{
background-color:	#FFFFFF;;
border-radius:5px;
}
.subLinkInsurance{
border:1px solid white;
font-weight:bold;
width:60%;
text-align:center;
line-height:1.3 em;
padding:2%;
background-color:#DADADA;
}
.subMenuWrapper{
background-color:	#B5B5B5;
border-radius:5px;
}
.subDivPortlet{
background-color:	#DADADA;
border-radius:5px;
}
#box-outer {
	width:18%;
}
#subDivLeftWrapper{
border-radius:5px;
}
.subDivLeftContent{
border:1px solid black;
font-weight:bold;
text-align:center;
line-height:1.3 em;
padding:2%;
background-color:#FFFFFF;
border-radius:5px;
} */
#subDIvTitle{
height:30px;
font-weight:bold;
padding-top:6px;
padding-left:2px;
font-size:1.2em;
text-decoration:underline
}
</style>

<script>
//var entryContext = "PRETRANSPLANT";
//var updateContext = "PRETRANSPLANT";

function showPreTransplant_VisitPage(){
	checkHighlight();
	$("#preTransplantVisit").addClass("selectedDiv");
	$("#subDIvTitle").html("Pre-Transplant Visit - Visit");
	var form1="";
	var url = "loadPreTransplant_VisitPage";
	var result = ajaxCall(url,form1);
	$("#forfloat_right").html(result);
	portletMinus();
}
function showPreTransplant_LabPage(){
	checkHighlight();
	$("#preTransplantLab").addClass("selectedDiv");
	$("#subDIvTitle").html("Pre-Transplant Visit - Labs/Tests");
	var form="";
	var url = "loadConsult_LabPage";
	var result = ajaxCall(url,form);
	$("#forfloat_right").html(result);
	portletMinus();
}
function showPreTransplant_PhysicianPage(){
	checkHighlight();
	$("#preTransplantPhysicianLab").addClass("selectedDiv");
	$("#subDIvTitle").html("Pre-Transplant Visit - Plan");
	var form="";
	var url = "loadConsult_PhysicianPage";
	var result = ajaxCall(url,form);
	$("#forfloat_right").html(result);
	portletMinus();
	}
function showPreTransplant_InsurancePage(){
	checkHighlight();
	$("#preTransplantInsurance").addClass("selectedDiv");
	$("#subDIvTitle").html("Pre-Transplant Visit- Insurance Approvals");
	var form="";
	var url = "loadConsult_InsurancePage";
	var result = ajaxCall(url,form);
	$("#forfloat_right").html(result);
	portletMinus();
}
function showPreTransplant_NextStepPage(){
	checkHighlight();
	$("#preTransplantNextStep").addClass("selectedDiv");
	$("#subDIvTitle").html("Pre-Transplant Visit- Next Step");
	var form="";
	var url = "loadConsult_NextStepPage";
	var result = ajaxCall(url,form);
	$("#forfloat_right").html(result);
	portletMinus();
}
function showPreTransplant_RecipientSuitablity(){
	checkHighlight();
	$("#preTransplantRecipientSuitability").addClass("selectedDiv");
	$("#subDIvTitle").html("Pre-Transplant Visit-Recipient Donor Suitablity");
	var form="";
	var url = "loadPreTransplant_RecipientSuitablity";
	var result = ajaxCall(url,form);
	$("#forfloat_right").html(result);
	portletMinus();
}

var contentheight;
function calculateHeight(){
    mainheight=$("#innercenter").height();
    var titlediv=$("#workflowcontainerID").height()
    //;alert(" mainheight"+ mainheight);
    //alert("titlediv"+ titlediv);
    //var contentdivheight=$("#contentdiv").height();
    contentheight=433.8
    //alert("contentheight"+ contentheight);
    $("#subdiv").height(contentheight);
}
var mainheight;
$(function() {
    $('.jclock').jclock();
  });
$(document).ready(function(){
	portletMinus();
	$( "#tabNew" ).tabs();
	//calculateHeight();
    $(window).resize(calculateHeight);
   // showPreTransplant_VisitPage();
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"});
      //$(window).resize();
	show_eastslidewidget("slidedivs");
		 		/* 	var  oTable3=$("#consultEvalChecklistDetails").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null
	 										             ],
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#consultEvalChecklistColumn").html($('.ColVis:eq(0)'));
												}
									});
		 			var  oTable=$("#consultEvalEducationDetails").dataTable({
						"sPaginationType": "full_numbers",
						"sDom": 'C<"clear">Rlfrtip',
						"aoColumns": [ { "bSortable":false},
							               null,
							               null,
							               null,
							               null
							               ],
							"oColVis": {
								"aiExclude": [ 0 ],
								"buttonText": "&nbsp;",
								"bRestore": true,
								"sAlign": "left"
							},
							"fnInitComplete": function () {
						        $("#consultEvalEducationColumn").html($('.ColVis:eq(1)'));
							}
				}); */
					    
});
$("select").uniform(); 

var preTransVisit={
		visit:function(divId){
		
		var url = 'getpreTransplantVisit.action';
		openUrl(url,'','<s:property value="#request.visitId"/>',divId);
			
	   },
	   lab:function(divId){
			$("#subDIvTitle").html("Lab");	
			$("#preTransLoadDiv").html("Lab");
			var url = 'loadPreTransplant_LabPage.action';
			LoadedPage.url=url;
			LoadedPage.div='preTransLoadDiv';
			this.loader(url, divId);
	   },
	   
	   phyPlan:function(divId){
			$("#subDIvTitle").html("Lab");	
			$("#preTransLoadDiv").html("Physician's Plan");
		
	   },
	   
	   donorSuitabilty:function(divId){
			$("#subDIvTitle").html("Recipient - Donor Suitablity");	
			$("#preTransLoadDiv").html("Lab");
			var url = 'loadPreTransplant_RecipientSuitablity.action';
			LoadedPage.url=url;
			LoadedPage.div='preTransLoadDiv';
			this.loader(url, divId);
	   },
	   
		financial:function(divId){
			var url = 'loadFinancialAppoval.action';
			openUrl(url,'','<s:property value="#request.visitId"/>',divId);
			
			
			
	   },
	   
	   next:function(divId){
		   var discCode ='preTransplantDisclaimer';
			var url = 'loadConsult_NextStepPage.action?transplantId='+'<s:property value="#request.transplantId"/>'+'&disc_code='+discCode;
			
			 openUrl(url,'','<s:property value="#request.visitId"/>','preTransLoadDiv');
			
	    },
	   
		loader:function(url, divId){			
			$("#"+divId).html(ajaxCall(url,''));
		}
		
		
};
preTransVisit.visit('preTransLoadDiv');

</script>
<form>
<div class="cleaner"></div>

<!--right window start -->	
<div class="cleaner"></div>
	<!-- <div class="subMenuWrapper" id="sub">
		<div id="subDIvTitle">Pre-TransplantVisit</div>
	</div> -->
	<br>
<!-- <div id="floatSubDivMenu">
<div id="forfloat_wind">
	<div id="box-outer">
		
 <a href="#" style="text-decoration:none" id="blind">&lt;&lt; </a> 
			<div id="box">
				<div id="leftnav" >
				
	recipient start
				
					<div  class="portlet">
					<div class="portlet-header"></div>
						<div class="portlet-content">
						<div id="subDivLeftWrapper" >
				<table cellpadding="0" cellspacing="0" border="0" id="" class="display">
						<tr>
							<td ><div class="subDivLeftContent" onclick="showPreTransplant_VisitPage()" id="preTransplantVisit">Visit</div></td></tr>
							<tr><td><div class="subDivLeftContent" onclick="showPreTransplant_LabPage()" id="preTransplantLab">Labs/Tests</div></td></tr>
							<tr><td><div class="subDivLeftContent" onclick="showPreTransplant_PhysicianPage()" id="preTransplantPhysicianLab">Physician's Plan</div></td></tr>
							<tr><td><div class="subDivLeftContent" onclick="showPreTransplant_RecipientSuitablity()" id="preTransplantRecipientSuitability">Recipient - Donor Suitablity</div></td></tr>
							<tr><td ><div class="subDivLeftContent" onclick="showPreTransplant_InsurancePage()" id="preTransplantInsurance">Insurance Approvals</div></td>
							<tr><td ><div class="subDivLeftContent" onclick="showPreTransplant_NextStepPage()" id="preTransplantNextStep">Next Step</div></td>
						</tr>
			</table>
			</div>
			</div>
			</div>
			</div>
			</div>
			</div>
		</div>
</div> -->
<s:hidden id="pkTxWorkflow_id" name="transplantId"/>
<s:hidden id="recipientId" name="recipient"/>
<s:hidden id="personId" name="personId"/>
<div id="subdiv">
<div id="tabNew">
	      <ul> 
			<li><a id="preTransVisitTab" href="#preTransLoadDiv" onclick="preTransVisit.visit('preTransLoadDiv');" >Visit</a></li>
			<li><a id="preTransLabTab" href="#preTransLoadDiv" onclick="preTransVisit.lab('preTransLoadDiv')">Labs/Tests</a></li>
   			<li><a id="preTransPhyTab" href="#preTransLoadDiv" onclick="preTransVisit.phyPlan('preTransLoadDiv')">Physician's Plan</a></li>
   			<li><a id="preTransSuitTab" href="#preTransLoadDiv" onclick="preTransVisit.donorSuitabilty('preTransLoadDiv')">Recipient - Donor Suitablity</a></li>
   			<li><a id="preTransInsTab" href="#preTransLoadDiv" onclick="preTransVisit.financial('preTransLoadDiv')">Financial Approvals</a></li>
   			<li><a id="preTransNextTab" href="#preTransLoadDiv" onclick="preTransVisit.next('preTransLoadDiv')">Next Step</a></li>
		</ul>
		<div id="preTransLoadDiv"></div>
	</div>
</div>
</form>

<!-- <script type="text/javascript">



var now = new dateEntry();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivaldateEntry').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new dateEntry()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};
$("#timeZone").html(timezones[-offset / 60]);

</script>
 -->













