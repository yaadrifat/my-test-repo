<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page import="com.velos.stafa.util.VelosStafaConstants"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript">
function savePre_CollectionDetails(){
	try{
		var pkPreCollectionDetails=''				
		var entityId =$("#recipientId").val();
		var entityType = $("#entityTypeId").val();			
		var fkTxWorkflow = $("#pkTxWorkflow_id").val();		
		//var visitId= $("#visitId").val();
		var visitId=0; 
		var preCollectionData = "{entityId:'"+entityId+ "',entityType:'"+entityType+"',fkTxWorkflow:'"+fkTxWorkflow+"',visitId:'"+visitId+"',";
		if($("#pkPreCollectionDetails").val()!=null)
		  {
			pkPreCollectionDetails=$('#pkPreCollectionDetails').val();
			preCollectionData += "pkPreCollectionDetails:'"+pkPreCollectionDetails+ "',";
		 }	
		var isAnesthesiaSchedule=$('#isAnesthesiaSchedule_id').is(':checked');
		preCollectionData += "anesthesiaSchedule:'"+isAnesthesiaSchedule+ "',";	
		var spokeTo =  $('#spokeTo_id').val();
		preCollectionData += "spokeTo:'"+spokeTo+ "',";
		var anesthesiaGroup = $('#anesthesiaGroup_id').val();
		preCollectionData += "anesthesiaGroup:'"+anesthesiaGroup+ "',";
		if($("#fkCodeAnesthesiaLocation_id option:selected").text() != 'Select')
		 {
			fkCodeAnesthesiaLocation=$("#fkCodeAnesthesiaLocation_id").val(); 
	 		preCollectionData += "fkCodeAnesthesiaLocation:'"+fkCodeAnesthesiaLocation+ "',";
		 }	 	
		var isAutologousCollected=$('#isAutologousCollected_id').is(':checked');
		preCollectionData += "autologousCollected:'"+isAutologousCollected+"'}";	
		var url = "savePreCollectionDetails";
		response = jsonDataCall(url,"jsonData={data:["+preCollectionData+"]}");	
		return true; 
		}
	catch(e){
		alert("Error : "+e);
		}
	 return true;
} 	
function savePages(mode){
	if(checkModuleRights(STAFA_RECIPIENTTRACKERS, APPMODE_SAVE)){
				try{							
							if(savePre_CollectionDetails()){
								
							}
							jAlert("  Data Saved Successfully !", "Information ...", function () { 
								openUrl(LoadedPage.url,LoadedPage.workflow,LoadedPage.visitId,LoadedPage.div);
								if(RefreshData!=undefined){
								  Refreshner(RefreshElement,RefreshData);
								}
				            }); 

			                return true;	
				
			}catch(e){
			alert("exception " + e);
		}
	}
}</script>
<s:hidden id="pkPreCollectionDetails"
	value="%{preCollectionDetailsData.pkPreCollectionDetails}" />
<s:hidden id="pkTxWorkflow_id" name="transplantId" />
<s:hidden id="recipientId" name="recipient" />
<s:hidden id="visitId" name="visitId" />
<input type="hidden" id="entityTypeId"
	value='<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_RECIPIENT"/>' />
<s:hidden id="personId" name="personId" />
<table cellpadding="0" cellspacing="0" class="" align="center"
	border="0" width="100%">
	<tr>
		<td class="tablewapper"><s:checkbox name="isAnesthesiaSchedule"
			value="%{preCollectionDetailsData.anesthesiaSchedule}"
			id="isAnesthesiaSchedule_id" /> <s:text
			name="stafa.label.HPCMPre-CollectionDetails.isAneSch" /></td>
		<td class="tablewapper1"><s:text
			name="stafa.label.HPCMPre-CollectionDetails.spokeTo" />: <s:textfield
			name="spokeTo" id="spokeTo_id"
			value="%{preCollectionDetailsData.spokeTo}" /></td>
	</tr>
	<tr>
		<td class="tablewapper"><s:text
			name="stafa.label.HPCMPre-CollectionDetails.AneGroup" />: <s:textfield
			name="anesthesiaGroup" id="anesthesiaGroup_id"
			value="%{preCollectionDetailsData.anesthesiaGroup}" /></td>
	</tr>
	<tr>
		<td class="tablewapper"><s:text
			name="stafa.label.HPCMPre-CollectionDetails.AneLocatiOn" />: <s:select
			cssStyle="width:auto" id="fkCodeAnesthesiaLocation_id"
			value="%{preCollectionDetailsData.fkCodeAnesthesiaLocation}"
			name="fkCodeAnesthesiaLocation"
			list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@TYPE_ANELOC]"
			listKey="pkCodelst" listValue="description" headerKey=""
			headerValue="Select" /></td>
	</tr>
	<tr>
		<td class="tablewapper"><s:checkbox name="isAutologousCollected"
			id="isAutologousCollected_id"
			value="%{preCollectionDetailsData.autologousCollected}" /> <s:text
			name="stafa.label.HPCMPre-CollectionDetails.isRBSCollected" /></td>
	</tr>
</table>