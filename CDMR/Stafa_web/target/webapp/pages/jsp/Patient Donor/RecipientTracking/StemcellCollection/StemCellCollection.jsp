<%-- <%@ include file="common/includes.jsp" %> --%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/workFlow.js"></script>
<% 
	String path = request.getContextPath()+"/pages/";
	System.out.println("path:::"+path);
%>
<link type="text/css" href="css/workFlow.css" rel="stylesheet" />
<style>
/* .workFlowContainer{
background-color:	#FFFFFF;;
border-radius:5px;
}
.subLinkInsurance{
border:1px solid white;
font-weight:bold;
width:60%;
text-align:center;
line-height:1.3 em;
padding:2%;
background-color:#DADADA;
}
.subMenuWrapper{
background-color:	#B5B5B5;
border-radius:5px;
}
.subDivPortlet{
background-color:	#DADADA;
border-radius:5px;
}
#box-outer {
	width:18%;
}
#subDivLeftWrapper{
border-radius:5px;
}
.subDivLeftContent{
border:1px solid black;
font-weight:bold;
text-align:center;
line-height:1.3 em;
padding:2%;
background-color:#FFFFFF;
border-radius:5px;
} */
#subDIvTitle {
	height: 30px;
	font-weight: bold;
	padding-top: 6px;
	padding-left: 2px;
	font-size: 1.2em;
	text-decoration: underline
}
</style>

<script>

function showCollection_DonorDetails(){
	checkHighlight();
	$("#collection_Donor").addClass("selectedDiv");
	$("#subDIvTitle").html("Stem-Cell Collection  - Donor Details");
	var form1="";
	var url = "loadCollection_DonorDetailsPage";
	var result = ajaxCall(url,form1);
	$("#forfloat_right").html(result);
	portletMinus();
}
function showPrescriptionPage(){
	checkHighlight();
	$("#collection_Prescription").addClass("selectedDiv");
	$("#subDIvTitle").html("Stem-Cell Collection - Prescription and Orders");
	var form="";
	var url = "loadCollection_PrescriptionPage";
	var result = ajaxCall(url,form);
	$("#forfloat_right").html(result);
	portletMinus();
}
function showPreCollectionPage(){
	checkHighlight();
	$("#collection_PreCollection").addClass("selectedDiv");
	$("#subDIvTitle").html("Stem-Cell Collection  - Pre-Collection details");
	var form="";
	var url = "loadPreCollectionPage";
	var result = ajaxCall(url,form);
	$("#forfloat_right").html(result);
	portletMinus();
}
function showConsult_InsurancePage(){
	checkHighlight();
	$("#collection_Insurance").addClass("selectedDiv");
	$("#subDIvTitle").html("Stem-Cell Collection - Financial Approvals");
	$("#forfloat_right").load('<%=path%>jsp/Consult_InsurancePage.jsp');
	portletMinus();
}
function showConsult_NextStepPage(){
	checkHighlight();
	$("#collection_NextStep").addClass("selectedDiv");
	$("#subDIvTitle").html("Stem-Cell Collection - Next Step");
	$("#forfloat_right").load('<%=path%>jsp/Consult_NextStepPage.jsp');
	portletMinus();
	}
$(function() {
    $('.jclock').jclock();
  });
var contentheight;var mainheight;
function calculateHeight(){
    mainheight=$("#innercenter").height();
    var titlediv=$("#workflowcontainerID").height()
    //;alert(" mainheight"+ mainheight);
    //alert("titlediv"+ titlediv);
    //var contentdivheight=$("#contentdiv").height();
    contentheight=433.8;
    //alert("contentheight"+ contentheight);
    $("#subdiv").height(contentheight);
}

$(document).ready(function(){
	portletMinus();
	$( "#tabNew" ).tabs();
	//calculateHeight();
    //$(window).resize(calculateHeight);
//    showCollection_DonorDetails();
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"});
    //  $(window).resize();
					    
});
$(window).resize(function(){
    var height;
    var screenheight=screen.height;
    var innerheight=$("#innercenter").height();
    var innerheight=594;
    var constanthgt=$("#constantdiv").height();
    /* if(innerheight) */
    if(screenheight<= 768){
       height =innerheight -(constanthgt+240);
    }else if(screenheight>=1024){
           height =innerheight -(constanthgt+40);

    }else if(screenheight>=800 && screenheight < 900){
           height =innerheight -(constanthgt+240);
    }else if(screenheight==900){
           height =innerheight -(constanthgt+130);
    }else if(screenheight==960){
           height =innerheight -(constanthgt+80);
    }
       $('#subdiv').height(height);
    });
$("select").uniform(); 
//load StemCellCollection
var StemCellCollection={	
		//load donorDetails page
		donorDetails:function(divId){
					var url = 'getdonorDetails.action';
					openUrl(url,'','<s:property value="#request.visitId"/>',divId);			
		},
		//load prescriptionAndOrders page
		prescriptionAndOrders:function(divId){
			var url = '.action';			
			openUrl(url,'','','stemCellCollection');			
	},
	//load preCollectionDetails page
	preCollectionDetails:function(divId){
			var url = 'loadHPCMPreCollection.action';
			openUrl(url,'','<s:property value="0"/>','stemCellCollection');			
	    },
	    financialApprovals:function(divId){
			var url = '.action';			
			openUrl(url,'','','stemCellCollection');			
	},
	nextStep:function(divId){
		var url = '.action';		
		openUrl(url,'','','stemCellCollection');
}			
};
StemCellCollection.donorDetails('stemCellCollection');
</script>
<div class="cleaner"></div>

<!--right window start -->
<div id="slidedivs" style="display: none">
<div id="uldiv">
<ul class="ulstyle">
	<li class="listyle print_label"><img
		src="images/icons/calendar.png" style="width: 30px; height: 30px;" /></li>
	<li class="listyle print_label"><b>Calendar</b></li>
	<li class="listyle print_label"><img src="images/icons/plan.png"
		style="width: 30px; height: 30px;" /></li>
	<li class="listyle notes_icon"><b>Plan</b></li>
	<li class="listyle print_label"><img
		src="images/icons/Notification.png" style="width: 30px; height: 30px;" /></li>
	<li class="listyle print_label"><b>Notification</b></li>
</ul>
</div>
</div>
<form>
<div class="cleaner"></div>
<div id="subdiv">
<div id="tabNew">
		<ul>
			<li><a id="def" href="#stemCellCollection" onclick="StemCellCollection.donorDetails('stemCellCollection');">Donor	Details</a></li>
			<li><a id="def" href="jsp/Patient Donor/RecipientTracking/StemcellCollection/stemCellCollection_Prescription.jsp">Prescription & Orders</a></li>
			<li><a id="def" href="#stemCellCollection" onclick="StemCellCollection.preCollectionDetails('stemCellCollection')">Pre-Collection Details</a></li>
			<li><a id="def" href="jsp/Patient Donor/RecipientTracking/Consult/Consult_InsurancePage.jsp">Financial 	Approvals</a></li>
			<li><a id="def" href="jsp/Patient Donor/RecipientTracking/Consult/Consult_NextStepPage.jsp">Next Step</a></li>
		</ul>
		 <div id="stemCellCollection"></div>
		</div>
		</div>
</form>














