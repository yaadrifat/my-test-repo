<%-- <%@ include file="common/includes.jsp" %> --%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<h1>Interim Visit</h1>

<script>

function calculateHeight(){
	   var  mainheight=$("#innercenter").height();
	    var titlediv=$("#workflowcontainerID").height();
		// var contentdivheight=$("#subdiv").height();
	    contentheight=(mainheight-titlediv)/16;
	    //alert("contentheight"+contentheight);
	    $("#subdiv").css("height",contentheight +"em");
	}

$(document).ready(function(){	
	portletMinus();
	$( "#tabNew" ).tabs();
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"});
	show_eastslidewidget("slidedivs");					    
	$("select").uniform(); 
});

var InterimVisit={
		visit:function(divId){
			//$("#subDIvTitle").html("Visit");
			//$("#interimVisitDiv").html("Visit");
			var url = 'getinterimVisit.action';
			openUrl(url,'','<s:property value="#request.visitId"/>',divId);
	   },
	   
	   lab:function(divId){
			$("#subDIvTitle").html("Lab");	
			$("#interimVisitDiv").html("Lab");
	   },
	   
		financial:function(divId){
			var url = 'loadFinancialAppoval.action';
			//LoadedPage.url=url;
			//LoadedPage.div='interimVisitDiv';
			//url +='?personId=<s:property value="personId"/>';
			//this.loader(url, divId);
			openUrl(url,'','<s:property value="#request.visitId"/>',divId);
			
	   },
	   
	   nextStep:function(divId){
		   var discCode ='interimVistDiscliamer';
			var url = 'loadConsult_NextStepPage.action?transplantId='+'<s:property value="#request.transplantId"/>'+'&disc_code='+discCode;
			
			 openUrl(url,'','<s:property value="#request.visitId"/>','interimVisitDiv');
	   },
	   
		loader:function(url, divId){			
			$("#"+divId).html(ajaxCall(url,''));
		}
		
		
};
InterimVisit.visit('interimVisitDiv');
</script>



<s:hidden id="pkTxWorkflow_id" name="transplantId"/>
<s:hidden id="recipientId" name="recipient"/>
<s:hidden id="personId" name="personId"/>
<form>
<div class="cleaner"></div>
<div id="subdiv">
<div id="tabNew">		
			 <ul>
				<li><a  href="#interimVisitDiv" onclick="InterimVisit.visit('interimVisitDiv');">Visit</a></li>
			   	<li><a id="def" href="#interimVisitDiv" onclick="InterimVisit.lab('interimVisitDiv')">Lab</a></li>
			   	<li><a  id="def" href="#interimVisitDiv" onclick="InterimVisit.financial('interimVisitDiv');">Financial Approvals</a></li>
			   	 <li><a id="def" href="#interimVisitDiv" onclick="InterimVisit.nextStep('interimVisitDiv');">Next Step</a></li>
			   
			</ul>
			<div id="interimVisitDiv">				 
				 			</div>	
		</div>
		
				 		
		</div>
 </form>
