<%-- <%@ include file="common/includes.jsp" %> --%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/workFlow.js"></script>
<link type="text/css" href="css/workFlow.css" rel="stylesheet" />
<style>
.fontRed a{
color:red !important;
}
.workFlowContainer{
background-color:	#FFFFFF;;
border-radius:5px;
}
.subLinkInsurance{
border:1px solid white;
font-weight:bold;
width:60%;
text-align:center;
line-height:1.3 em;
padding:2%;
background-color:#DADADA;
}
#subMenuWrapper{
background-color:	#B5B5B5;
border-radius:5px;
}
.subDivPortlet{
background-color:	#DADADA;
border-radius:5px;
}
.Selected{
background-color:yellow;
}
</style>

<script>
function checkTabHighlight(){
	$(".subLinkInsurance").each(function(){
		if($(this).hasClass("selected")){
			$(this).removeClass("selected");
		}	
	});
}
function consult(){
	$("#dynamicheader").html("Consult/Eval");
	var form="";
	var url = "loadConsultPage";
	var result = ajaxCall(url,form);
	$("#subdiv").html(result);
}
function preTransplant(){
	$("#dynamicheader").html("Pre-Transplant Visit");
	var form="";
	var url = "loadPreTransplantPage";
	var result = ajaxCall(url,form);
	$("#subdiv").html(result);
}
function stemCellCollection(){
	$("#dynamicheader").html("Stem Cell Collection");
	var form="";
	var url = "loadStemCellCollectionPage";
	var result = ajaxCall(url,form);
	$("#subdiv").html(result);
}
/* function show_eastslidewidget(divid){
    // to Hide the left side widget
	innerLayout.hide('west');
    innerLayout.show('south',true);
    innerLayout.show('north',true);
    innerLayout.show('east',false);
    var src = $("#"+divid).html();
    $("#innereast").html(src);
    $("#"+divid).html("slidedivs");
   
}  */
$(function() {
    $('.jclock').jclock();
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

  });
$(document).ready(function(){
	calculateHeight();
    $(window).resize(calculateHeight);
	show_slidewidgets("donorInfo",true);
	show_slidecontrol("slidedivs");
    //$("select").uniform(); 
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"});
      $(window).resize();
	//show_eastslidewidget("slidedivs");
	var oTable=$("#referralDetails").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null
											             ],
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#referralDetailsColumn").html($('.ColVis:eq(0)'));
												}
		});
		 var oTable1=$("#referralChecklist").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null
	 										             ],
	 										"oColVis": {
	 											"aiExclude": [ 0 ],
	 											"buttonText": "&nbsp;",
	 											"bRestore": true,
	 											"sAlign": "left"
	 										},
	 										"fnInitComplete": function () {
	 									        $("#referralChecklistColumn").html($('.ColVis:eq(1)'));
	 										}
									});
	 var oTable2=$("#referralDocuments").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
	 										               null,
	 										               null,
	 										               null,
	 										               null
	 										             ],
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#referralDocumentColumn").html($('.ColVis:eq(2)'));
												}
									});
					var  oTable3=$("#insuranceApprovals").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null
	 										             ],
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#insuranceApprovalsColumn").html($('.ColVis:eq(3)'));
												}
									});
					    
});
/* $(window).resize(function(){
    var height;
    var screenheight=screen.height;
    var innerheight=$("#innercenter").height();
    var innerheight=594;
    var constanthgt=$("#constantdiv").height();
     if(innerheight) 
    if(screenheight<= 768){
       height =innerheight -(constanthgt+240);
    }else if(screenheight>=1024){
           height =innerheight -(constanthgt+40);

    }else if(screenheight>=800 && screenheight < 900){
           height =innerheight -(constanthgt+240);
    }else if(screenheight==900){
           height =innerheight -(constanthgt+130);
    }else if(screenheight==960){
           height =innerheight -(constanthgt+80);
    }
       $('#subdiv').height(height);
    }); */
    
    function ShowdonorTraker(){
    	//alert($("#donorMatching").attr("class"));
    /* 	if($("#donorMatching").hasClass("fontBlue")){
    		$("#donorMatching").removeClass("fontBlue");
    		$("#donorMatching").addClass("fontRed");
    	}else{
    		$("#donorMatching").removeClass("fontRed");
    		$("#donorMatching").addClass("fontBlue");
    	} */ 	
    	//alert("after===="+	$("#donorMatching").attr("class"));
    	$("#dynamicheader").html("Donor Matching");
    	checkTabHighlight();
    	//$("#donorMatching").addClass("Selected");
    	var form="";
    	var url = "loadDonorMatching";
    	var result = ajaxCall(url,form);
    	$("#subdiv").html(result);
    }
    
function add_recipientsrows(){
	   //alert("!");
    $("#recipiendids").prepend("<tr>"+
            "<td><input type='checkbox' id=''/></td>"+
            "<td><input type='text' id=''/></td>"+
            "<td><input type='text' id=''/></td>"+
            "<td><input type='text' id=''/></td>"+
            "</tr>");
   
}
function showRecipientID(){
    showPopUp('open','addmodal','Recipient IDs','800','300');
}
$("select").uniform(); 

var contentheight;
function calculateHeight(){
    mainheight=$("#innercenter").height();
    var titlediv=$("#workflowcontainerID").height()
    var titlediv=$("#workflowcontainerID").height();
	// var contentdivheight=$("#subdiv").height();
    contentheight=(mainheight-titlediv)/16;
    //alert("contentheight"+contentheight);
    $("#subdiv").css("height",contentheight +"em");
}
var mainheight;
    
</script>

<div class="cleaner"></div>

<!--right window start -->	
<div  id="slidedivs"  style="display:none">
<div id="uldiv">
<ul class="ulstyle">
<li class="listyle print_label"><img src = "images/icons/calendar.png"  style="width:30px;height:30px;"/></li>
<li class="listyle print_label" ><b>Calendar</b> </li>
<li class="listyle print_label"><img src = "images/icons/plan.png"  style="width:30px;height:30px;"/></li>
<li class="listyle notes_icon"><b>Plan</b> </li>
<li class="listyle print_label"><img src = "images/icons/Notification.png"  style="width:30px;height:30px;"/></li>
<li class="listyle print_label" ><b>Notification</b> </li>
</ul>
</div>
</div>
<form>
<div class="column">
<div  id="donorInfo"  style="display:none">
<div class="portlet">
	<div class="portlet-header">Donor Info</div>
		<div class="portlet-content">
									<table width="100%" border="0">
									  <tr>
										<td width="50%" style="padding:2%">First Name:</td>
										<td width="50%" id="firstName" name="firstName" style="padding:2%">Sarah</td>
									  </tr>
									  <tr>
										<td style="padding:2%">Last Name:</td>
										<td id="lastName" name="lastName" style="padding:2%">Branofsky</td>
									  </tr>
									  <tr>
										<td style="padding:2%">Recipient ID:</td>
										<td id="recipientId" name="recipientId" style="padding:2%">HCA897695</td>
									  </tr>
									  <tr>
										<td style="padding:2%">MRN:</td>
										<td id="mrn" name="mrn" style="padding:2%">398708</td>
									  </tr>
									  <tr>
										<td style="padding:2%">DOB:</td>
										<td id="dob" name="dob" style="padding:2%"> May 20, 1985</td>
									  </tr>
									   <tr>
										<td style="padding:2%">Age:</td>
										<td id="age" name="age" style="padding:2%">27</td>
									  </tr>
									   <tr>
										<td style="padding:2%">Registry or CBU:</td>
										<td id="registryorCBU" name="registryorCBU" style="padding:2%">N/A</td>
									  </tr>
									   <tr>
										<td style="padding:2%">ABO/Rh:</td>
										<td id="abo/Rh" name="abo/Rh" style="padding:2%">A+</td>
									  </tr>
									  <tr>
										<td style="padding:2%">Department:</td>
										<td id="department" name="department" style="padding:2%">Adults</td>
									  </tr>
									  <tr>
										<td style="padding:2%">Quicklinks:</td>
										<td id="recipientProfile" name="recipientProfile" style="padding:2%"><a href="#" src="" id="" name="" style="color:blue">Recipient Profile</a></td>
									  </tr>
									  <tr>
										<td></td>
										<td id="donorMatching"  name="donormatching" style="padding:2%" class="fontBlue"><a href="#" src="" id="" name="" style="color:blue" onclick="ShowdonorTraker()">Donor Matching</a></td>
									  </tr>
									   <tr>
										<td></td>
										<td id="insurance"  name="insurance" style="padding:2%" class="fontBlue"><a href="#" src="" id="" name="" style="color:blue" onclick="()">Insurance</a></td>
									  </tr>
									  <tr>
										<td></td>
										<td id="availableProducts"  name="availableProducts" style="padding:2%" class="fontBlue"><a href="#" src="" id="" name="" style="color:blue" onclick="()">Available Products</a></td>
									  </tr>
									</table>
								</div>
						</div>
						</div>
						
<!-- <div  class="portlet"> -->
<!-- 	<div class="portlet-header" id="dynamicheader">Referral</div> -->
		<div class="portlet-content"  id="workflowcontainerID" >
			<!-- <div id="subMenuWrapper" >
				<table cellpadding="0" cellspacing="0" border="0" id="" class="display">
						<tr>
							<td style="padding-right:20px"><div class="subLinkInsurance" id="recipientWorkflow">Recipient Workflow</div></td>
							<td style="padding-right:20px"><div class="subLinkInsurance" id="recipientProfile">Recipient Profile</div></td>
							<td style="padding-right:20px"><div class="subLinkInsurance" id="insurance">Insurance</div></td>
							<td style="padding-right:20px"><div class="subLinkInsurance" onclick="ShowdonorTraker()" id="donorMatching">Donor Matching</div></td>
							<td style="padding-right:20px"><div class="subLinkInsurance" id="availableProducts">Available Products</div></td>
						</tr>
			</table>
			</div><br> -->
				<!-- <div class="subDivPortlet">
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
							<tr>
								<td align="right" style="padding-top:1%">Recipient First Name : </td>
								<td  align="left" style="padding-top:1%">&nbsp;Renee</td>
								<td align="right" style="padding-top:1%">Diagnosis :</td>
								<td  align="left" style="padding-top:1%">&nbsp;AML</td>
								<td  align="right" style="padding-top:1%">Age :</td>
								<td  align="left" style="padding-top:1%">&nbsp;24</td>
								<td  align="right" style="padding-top:1%">Diagnosis :</td>
								<td  align="left" style="padding-top:1%">&nbsp;Peds</td>
						</tr>
						<tr>
								<td  align="right" style="padding-top:1%">Recipient Last Name : </td>
								<td  align="left" style="padding-top:1%">&nbsp;Branofsky</td>
								<td  align="right" style="padding-top:1%">Diagnosis Date:</td>
								<td  align="left" style="padding-top:1%">&nbsp;Jan 6, 2012</td>
								<td  align="right" style="padding-top:1%">SSN:</td>
								<td  align="left" style="padding-top:1%">&nbsp;465983875</td>
								<td  align="right">ABO/Rh:</td>
								<td  align="left">&nbsp;A+</td>
						</tr>
						<tr>
							<td  align="right" style="padding-top:1%">Status:</td>
							<td style="padding-top:1%">&nbsp;Pre-Transplant</td>
							<td  align="right" style="padding-top:1%">DOB :</td>
							<td style="padding-top:1%">&nbsp;Jan 19, 1988</td>
							<td  align="right" style="padding-top:1%">MRN :</td>
							<td style="padding-top:1%">&nbsp;586790</td>
						</tr>
							
					</table>
				</div> -->  
		<div class="workFlowContainer">
			 <div class="workFlow">
				 <div class='workflowContent' >
				 		<div class='processstarted' id="referalProcess" onclick=''></div>
				 		<div class='onprogress' id="referalID" onclick='check(this)'><p class='samplet-text'>Referal</p></div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' id="consultProcess" onclick=''></div>
				 	<div class='initial'  id="consultID" onclick='check(this),consult(),checkBox(this)'>
				 		<p class='samplet-text'>Consult/Eval</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' id="preTransplantProcess"'></div>
				 	<div class='initial' id="preTransplantID" onclick='check(this),checkBox(this),preTransplant()'>
				 		<p class='samplet-text'>Pre-Transplant Visit</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' id="stemCellProcess"onclick=''></div>
				 	<div class='initial'  id="stemCellCollectionID" onclick='check(this),checkBox(this),stemCellCollection()'>
				 		<p class='samplet-text'>Stem Cell Collection</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' id="PreparativeProcess" onclick=''></div>
				 	<div class='initial'  id=" preprativeRegimenID" onclick=''>
				 		<p class='samplet-text'>Preparative Regimen</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' id="selectProductProcess" onclick=''></div>
				 	<div class='initial'  id="selectProductID" onclick=''>
				 		<p class='samplet-text'>	Selected Products</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
				 	<div class='processinitial' id="infusionProcess" onclick=''></div>
				 	<div class='initial'  id="infusuionID" onclick=''>
				 		<p class='samplet-text'>Infusion</p>
				 	</div>
				 </div>
				 <div class='workflowContent'>
					 <div class='processinitial' id="followupProcess"onclick=''></div>
					 <div class='initial' id="followUpID" onclick=''>
					 	<p class='samplet-text'>Followup</p>
					 </div>
					 </div>
			</div>
		</div>
<!-- 		</div> -->
<!-- 	</div> -->
</div>
<div class="cleaner"></div>
<div id="subdiv" style="overflow-y:auto">
<div class="column">
	<div  class="portlet">
	<div class="portlet-header">Recipient Registration</div>
		<div class="portlet-content">
		 	<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td class="tablewapper">First Name</td>
					<td class="tablewapper1"><input type="text" id="firstName"  name=""/></td>
					<td class="tablewapper">Diagnosis at the time of Referral</td>
					<td class="tablewapper1"><select id='diagnosis'><option>Select</option></select></td>
				</tr>
				<tr>
					<td class="tablewapper">Middle Name</td>
					<td class="tablewapper1"><input type="text" id="middleName"  name=""/></td>
					<td class="tablewapper">Diagnosis Date</td>
					<td class="tablewapper1"><input type="text" id="companyPhoneNumber"  name="" class="dateEntry"/></td>
                </tr>
                <tr>
					<td class="tablewapper">LastName</td>
					<td class="tablewapper1"><input type="text" id="lastName"  name=""/></td>
					<td class="tablewapper">SSN</td>
					<td class="tablewapper1"><input type="text" id="companyPhoneNumber"  name="" class="dateEntry"/></td>
                </tr>
                <tr>
					<td class="tablewapper">Maiden Name</td>
					<td class="tablewapper1"><select id="SubscriberId"><option>Select</option></select></td>
					<td class="tablewapper">Date of Birth</td>
					<td class="tablewapper1"><input type="text" id="dateOfBirth" name="" class="dateEntry"/></td>
				</tr>
				<tr>
					<td class="tablewapper">Address Line 1</td>
					<td class="tablewapper1"><input type="text" id="addressLine1" name=""/></td>
					<td class="tablewapper">Age</td>
					<td class="tablewapper1"><input type="text" id="age" name=""/></td>
				</tr>
				<tr>
					<td class="tablewapper">Address Line 2</td>
					<td class="tablewapper1"><input type="text" id="addressLine2" name=""/></td>
					<td class="tablewapper">Gender</td>
					<td class="tablewapper1"><select id="gender"><option>Select</option></select></td>
				</tr>
				<tr>
					<td class="tablewapper">City</td>
					<td class="tablewapper1"><input type="text" id="city" name=""/></td>
					<td class="tablewapper">ABO/Rh</td>
					<td class="tablewapper1"><select id="abo/rh"><option>Select</option></select></td>
				</tr>
				<tr>
					<td class="tablewapper">State</td>
					<td class="tablewapper1"><select id="state"><option>Select</option></select></td>
					<td class="tablewapper">MRN #</td>
					<td class="tablewapper1"><input type="text" id="mrn" name=""/></td>
				</tr>
				<tr>
					<td class="tablewapper">Country</td>
					<td class="tablewapper1"><select id="country"><option>Select</option></select></td>
					<td class="tablewapper"></td>
					<td class="tablewapper1"></td>
				</tr>
				<tr>
					<td class="tablewapper">ZipCode</td>
					<td class="tablewapper1"><input type="text" id="zipCode" name=""/></td>
					<td class="tablewapper"></td>
					<td class="tablewapper1"></td>
				</tr>
				<tr>
					<td class="tablewapper">Primary Phone #</td>
					<td class="tablewapper1"><input type="text" id="primaryPhone" name=""/></td>
					<td class="tablewapper"></td>
					<td class="tablewapper1"></td>
				</tr>
				<tr>
					<td  class="tablewapper">Home Phone #</td>
					<td class="tablewapper1"><input type="text" id="homePhone" name=""/></td>
					<td class="tablewapper"></td>
					<td class="tablewapper1"></td>
				</tr>
				<tr>
					<td class="tablewapper">Work Phone 3</td>
					<td class="tablewapper1"><input type="text" id="workPhone" name=""/></td>
					<td class="tablewapper"></td>
					<td class="tablewapper1"></td>
				</tr>
			</table>
		</div>
	</div>
</div>
	<div class="column">
	<div  class="portlet">
				<div class="portlet-header ">Referral Details</div>
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
					<td width="12%" style="padding:2%;">Referral Date</td>
					<td><input type="text" id="workPhone" name="" class="dateEntry"/></td>
					<td></td>
					<td></td>
				</tr>
				</table>
						<div class="portlet-content">
						<table>
							<tr>
								<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
								<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
						    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
							</tr>
						</table>
						<div id='referralDetailsTable' style="overflow-x:auto;overflow-y:hidden;">
						<table cellpadding="0" cellspacing="0" border="0" id="referralDetails" class="display">
						<thead>
							<tr>
								<th width="4%" id="referralDetailsColumn"></th>
								<th colspan='1'>Select</th>
								<th colspan='1'>Last Name</th>
								<th colspan='1'>First Name</th>
								<th colspan='1'>Type</th>
								<th colspan='1'>Institution</th>
								<th colspan='1'>Dept</th>
								<th colspan='1'>Address</th>
								<th colspan='1'>Phone Number</th>
								<th colspan='1'>Fax Number</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="recipientInsuranceVerified" onclick=""></td>
								<td></td>
								<td></td>
								<td>	</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
						</table>
						</div>
			</div>
		</div>
	</div>
<div class="column">       
    <div  class="portlet">
    <div class="portlet-header">Registration Details</div><br>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Department</td>
					<td class="tablewapper1"><select id="State"><option>Select</option></select></td>
                    <td class="tablewapper">Assigned Transplant MD</td>
                    <td class="tablewapper1"><input type="text" id="assignedTransplant" name=""/></td>
                </tr>
                <tr>
                    <td class="tablewapper">Recipient IDs</td>
                    <td class="tablewapper1"><input type="button" id="referralRecipientView" value="View"  onclick="showRecipientID()"/></td>
                    <td class="tablewapper">Assigned Transplant Coordinator</td>
                    <td class="tablewapper1"><input type="text" id="assignedTransplantCoordinator" name=""/></td>
                </tr>
                <tr>
                    <td class="tablewapper">Consult/Eval Date</td>
                        <td class="tablewapper1"><input type="text" id="ConsultEval Date" class="dateEntry"/></td>
                    <td class="tablewapper">Status</td>
					<td class="tablewapper1"><select id="Status"><option>Select</option></select></td>
                </tr>
            </table>
        </div>
    </div>
</div>
			
			
<div class="column">
	<div  class="portlet">
				<div class="portlet-header ">Referral Checklist</div>
						<div class="portlet-content">
						<table>
							<tr>
								<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
								<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
						    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
							</tr>
						</table>
						 <div id='accessTable' style="overflow-x:auto;overflow-y:hidden;">
						<table cellpadding="0" cellspacing="0" border="0" id="referralChecklist" class="display">
						<thead>
							<tr>
								<th width="4%" id="referralChecklistColumn"></th>
								<th colspan='1'>Select</th>
								<th colspan='1'>Item</th>
								<th colspan='1'>Date</th>
								<th colspan='1'>Associated Document</th>
								<th colspan='1'>Attachment</th>
								<th colspan='1'>Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="recipientInsuranceVerified" onclick=""></td>
								<td>Medicaid</td>
								<td><select><option>Select</option><option>Active</option><option>Expired</option></select></td>
								<td><input type="text"  class="dateEntry"></td>
								<td><select><option>Select</option><option>Active</option><option>Expired</option></select></td>
								<td><select><option>Select</option><option>Active</option><option>Expired</option></select></td>
							</tr>
						</tbody>
						</table>
						</div>
			</div>
		</div>
	</div>			
	
	
	<div class="column">
	<div  class="portlet">
				<div class="portlet-header ">Referral Documents</div>
						<div class="portlet-content">
						<table>
							<tr>
								<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
								<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
						    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
							</tr>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" id="referralDocuments" class="display">
						<thead>
							<tr>
								<th width="4%" id="referralDocumentColumn"></th>
								<th colspan='1'>Select</th>
								<th colspan='1'>Document Name</th>
								<th colspan='1'>Date</th>
								<th colspan='1'>Attachment</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="referralDocumentVerified" onclick=""></td>
								<td><select><option>Select</option><option>SingleCase Agreement</option><option>Private Pay</option><option>Charity</option><option>Limited Coverage</option><option>Not Covered- Rejected</option></select></td>
								<td><input type="text"  class="dateEntry"></td>
								<td></td>
							</tr>
						</tbody>
						</table>
			</div>
		</div>
	</div>			
	
	
	<div class="column">
	<div  class="portlet">
				<div class="portlet-header ">Insurance Approvals</div>
						<div class="portlet-content">
						<table>
							<tr>
								<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
								<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
						    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
							</tr>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" id="insuranceApprovals" class="display">
						<thead>
							<tr>
								<th width="4%" id="insuranceApprovalsColumn"></th>
								<th colspan='1'>Select</th>
								<th colspan='1'>Approval Name</th>
								<th colspan='1'>I-Plan Code</th>
								<th colspan='1'>Auth Date</th>
								<th colspan='1'>Auth Code</th>
								<th colspan='1'>Eff Date</th>
								<th colspan='1'>End Date</th>
								<th colspan='1'>Auth Attachment</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="insuranceApprovalsVerified" onclick=""></td>
								<td><select><option>Select</option><option>Consult</option></select></td>
								<td><select><option>Select</option></select></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td><img src="images/icons/attach.png"></td>
						
							</tr>
						</tbody>
						</table>
			</div>
		</div>
	</div>			
			
			
			
				
<div class="column">
	<div  class="portlet">
	<div class="portlet-header ">Next Step</div>
		<div class="portlet-content">
		 	<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%"  id="nextStepTable">
				<tr>
					<td class="tablewapper">Recipient Status</td>
					<td class="tablewapper1"><select id="recipient Status"><option>Select</option><option>Medical Mnanagement</option><option>Sent out for more treatment</option><option>Ineligible for Transplant</option></select></td>
					<td class="tablewapper">&nbsp;</td>
					<td class="tablewapper">&nbsp;</td>
				</tr>
				<tr>
					<td class="tablewapper">Next Step</td>
					<td class="tablewapper1"><select id="nextStep"><option>Select</option><option>N/A</option></select></td>
					<td class="tablewapper">&nbsp;</td>
					<td class="tablewapper">&nbsp;</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<div id="addmodal" style="display:none;">
 <div align="left">
            <table width="28%">
                <tr>
                    <td style="width:5%"><div><img src = "images/icons/addnew.jpg" class="cursor" id="" onclick="add_recipientsrows()"/>&nbsp;&nbsp;<label  class="cursor" onclick="add_donorLabResults()"><b>Add </b></label>&nbsp;</div></td>
                    <td style="width:5%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id =""/>&nbsp;&nbsp;<label class="cursor" ><b>Delete</b></label>&nbsp;</div></td>
                </tr>
            </table>
</div>   <br>        
            <table cellpadding="0" cellspacing="0" border="1" id="recipiendids" class="display">
                <thead>
                    <tr>
                        <th>Select</th>
                        <th>Recipient ID</th>
                        <th>ID Source</th>
                        <th>ID Type</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input type="checkbox" id="" class=""/></td>
                        <td></td>
                        <td><input type="text" id="" value="NMDP"/></td>
                        <td><input type="text" value="RID"/></td>
                    </tr>
                </tbody>
            </table>


</div>
 
<div class="cleaner"></div>
		
<div align="right" style="float:right;">

		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
			<tr>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;&nbsp;</td>
				<td><input type="button" value="Save" onclick=""/></td>
			</tr>
		</table>

</div>
</div>
</div>
</form>

<!-- <script type="text/javascript">



var now = new dateEntry();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivaldateEntry').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new dateEntry()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};
$("#timeZone").html(timezones[-offset / 60]);

</script>
 -->













