<%-- <%@ include file="common/includes.jsp" %> --%>
<!-- <script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" /> -->
<!-- <script type="text/javascript" src="jsp/Patient Donor/RecipientTracking/Consult/Consult.js"></script> -->
<script>
$(function() {
    $('.jclock').jclock();
    $("select").uniform();
  });
$(document).ready(function(){
	portletMinus();
	$("#subDIvTitle").html("Labs/Tests");
     var oTable=$("#preTestTable").dataTable({
                                            "sPaginationType": "full_numbers",
                                            "sDom": 'C<"clear">Rlfrtip',
                                            "aoColumns": [ { "bSortable":false},
                                                           null,
                                                           null,
                                                           null,
                                                           null,
                                                           null,
                                                           null
                                                         ],
                                            "oColVis": {
                                                    "aiExclude": [ 0 ],
                                                    "buttonText": "&nbsp;",
                                                    "bRestore": true,
                                                    "sAlign": "left"
                                                },
                                                "fnInitComplete": function () {
                                                    $("#preTestTableColumn").html($('#preTestTable_wrapper .ColVis'));
                                                }
                                    });
        oTable=$("#otherTestTable").dataTable({
                                            "sPaginationType": "full_numbers",
                                            "sDom": 'C<"clear">Rlfrtip',
                                            "aoColumns": [ { "bSortable":false},
                                                            null,
                                                            null,
                                                            null,
                                                            null,
                                                            null,
                                                            null
                                                          ],
                                             "oColVis": {
                                                 "aiExclude": [ 0 ],
                                                 "buttonText": "&nbsp;",
                                                 "bRestore": true,
                                                 "sAlign": "left"
                                             },
                                             "fnInitComplete": function () {
                                                 $("#otherTestTableColumn").html($('#otherTestTable_wrapper .ColVis'));
                                             }
                                    });
        oTable=$("#recipientIDMTable").dataTable({
                                            "sPaginationType": "full_numbers",
                                            "sDom": 'C<"clear">Rlfrtip',
                                            "aoColumns": [ { "bSortable":false},
                                                            null,
                                                            null,
                                                            null,
                                                            null,
                                                            null
                                                          ],
                                                "oColVis": {
                                                    "aiExclude": [ 0 ],
                                                    "buttonText": "&nbsp;",
                                                    "bRestore": true,
                                                    "sAlign": "left"
                                                },
                                                "fnInitComplete": function () {
                                                    $("#recipientIDMColumn").html($('#recipientIDMTable_wrapper .ColVis'));
                                                }
                                    }); 
    $("select").uniform();
});
</script>
<div class="cleaner"></div>
<form>
<div class="cleaner"></div>
<div class="column">       
    <div  class="portlet">
        <div class="portlet-header">Pre-Tests</div>
        <div class="portlet-content">
            <table>
                <tr>
                    <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" id="preTestTable" class="display">
                <thead>
                    <tr>
                        <th width="4%" id="preTestTableColumn"></th>
                        <th>Test</th>
                        <th>Date</th>
                        <th>Result</th>
                        <th>Attachment</th>
                        <th>Status</th>
                        <th>Flag</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>CBC w/diff</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><select><option>Complete</option><option>N/A</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>BMP /L ft/Mag</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>PT/PTT</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>Type / Screen</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>Echocardiogram</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>Pregnancy Test</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>UA</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>CT:Head</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                   
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>CT:Sinus</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>CT:Chest</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>CT:Abdomen</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>CXR</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>Bone Marrow / BX</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>Pshycosocial Evaluation</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>EKG</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>O2 sat</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>Muga</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>Chest X-ray</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>Dental Evaluation</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>H&P Dictation</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="cleaner"></div>
<div class="column">       
    <div  class="portlet">
        <div class="portlet-header">Other-tests</div>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="0" border="0" id="otherTestTable" class="display">
                <thead>
                    <tr>
                        <th width="4%" id="otherTestTableColumn"></th>
                        <th>Test</th>
                        <th>Date</th>
                        <th>Result</th>
                        <th>Attachment</th>
                        <th>Status</th>
                        <th>Flag</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>LP</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>PFTs/ABGs</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>Colonoscopy</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>PSA</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>Mammogram</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>Hgb A1C</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>24 hr urine for creatnine Clearance</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>Nuclear GFR</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type='checkbox'/></td>
                        <td>Neuropsych</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><input type="radio"/></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="cleaner"></div>
<div class="column">
    <div  class="portlet">
        <div class="portlet-header">Recipient IDMs</div>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="0" border="0" id="recipientIDMTable" class="display">
                <thead>
                    <tr>
                        <th width="4%" id="recipientIDMColumn"></th>
                        <th>Test</th>
                        <th>Date</th>
                        <th>Result</th>
                        <th>Status</th>
                        <th>Flag</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>RPR</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>EBV lgM</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>EBV lgG</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>CMV Total</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>Toxoplasmosis lgG</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>VZV lgG</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>HSV 1 lgG</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>HSV 2 lgG</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>HSV 1&2 lgM</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>Hep A lgG</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>Hep A lgM</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>Hep B surf.Ab</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>Hep B surf.Ag</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>Hep C Ab</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>HIV 1&2 Ab Screen</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>HIV RNA Copy</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>HTLV 1&2</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>West Nile</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"/></td>
                        <td>Chagas</td>
                        <td>Dec 4,2012</td>
                        <td>Negative</td>
                        <td><select><option>Complete</option></select></td>
                        <td><input type="radio"/></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
 <div class="cleaner"></div>
<div class="column">
    <div  class="portlet">
        <div class="portlet-header">Recipient HLA</div>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="" class="display" align="" border="0" width="100%">
                <thead>
                <tr>
                <th colspan=3>Recipient Initial HLA Typing</th>
                <th colspan=2>Recipient Confirmatory HLA Typing</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Resolution</td>
                    <td colspan="2"><div style="padding-left:13%;"><select><option>select</option><option>High Resolution</option></select></div></td>
                    <td colspan="2"><div style="padding-left:35%;"><select><option>select</option><option>High Resolution</option></select></div></td>
                </tr>
                <tr>
                    <td>HLA-A</td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                </tr>
                 <tr>
                    <td>HLA-B</td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                </tr>
                  <tr>
                    <td>HLA-C</td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                </tr>
                 <tr>
                    <td>HLA-DR</td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                </tr>
                 <tr>
                    <td>HLA-DQ</td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                </tr>
                  <tr>
                    <td>Date Tested</td>
                    <td colspan="2"><div style="padding-left:13%;"><input type="text" id="" class="dateEntry"/></div></td>
                    <td colspan="2"><div style="padding-left:35%;"><input type="text" id="" class="dateEntry"/></div></td>
                </tr>
                 <tr>
                    <td>Test Location</td>
                    <td colspan="2"><div style="padding-left:13%;"><select><option>select</option><option>High Resolution</option></select></div></td>
                    <td colspan="2"><div style="padding-left:35%;"><select><option>select</option><option>High Resolution</option></select></div></td>
                </tr>
                 <tr>
                    <td>Attachment</td>
                    <td colspan="2"><div style="padding-left:13%;"><input id="recipientHLAattach" class="attach" type="file" onclick=""  style="display:none;"/><a id="recipientHLAbrowse" href="#"> Browse</a> </div></td>
                    <td colspan="2"><div style="padding-left:35%;"><input id="recipientHLAattach1" class="attach" type="file"  onclick="" style="display:none;"/><a id="recipientHLAbrowse1" href="#">Browse</a> </div></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</form>
<div class="cleaner"></div>
<div align="right" style="float:right;">
    <form>
        <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
            <tr>
                <td>Next:</td>
                <td><div class="mainselection"><select name="" id="" ><option value="">Select</option><option>Preparation</option><option>Verification</option><option>Collection</option><option>Post-Collection</option></select></div></td>
                <td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;&nbsp;</td>
                <td><input type="button" value="Next" onclick="saveOvernightStorage()"/></td>
            </tr>
        </table>
    </form>
</div>
<div class="cleaner"></div>




<!-- <script type="text/javascript">



var now = new Date();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivalDate').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new Date()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};
$("#timeZone").html(timezones[-offset / 60]);

</script>
 -->