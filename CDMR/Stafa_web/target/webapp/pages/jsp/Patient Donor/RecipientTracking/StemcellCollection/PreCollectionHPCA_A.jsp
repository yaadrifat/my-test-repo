<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page import="com.velos.stafa.util.VelosStafaConstants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script>
$(function() {
    $('.jclock').jclock();
    $("select").uniform(); 
  });
$(document).ready(function(){
	$("#subDIvTitle").html("Pre Collection");
     portletMinus();
});

function saveLinePlacementDetail()
	 {
	 var entityId = $("#recipientId").val();
	 var entityType = "RECIPIENT";
	 var fkTxWorkflow = $("#pkTxWorkflow_id").val();
	 var isCentralPlaced="";
	 var fkCentralPlaceSite,fkLinePlaceLocation;
	 if($('input[name=centralLinePlaceQues]:radio:checked').val()=='true')
	 	{
		 isCentralPlaced=true;
	 	}
	 else if($('input[name=centralLinePlaceQues]:radio:checked').val()=='false')
	 	{
		 isCentralPlaced=false;
	 	}
	 
	 var linePlaceData = "{isCentralPlaced:'"+isCentralPlaced+ "',";
	  if($("#pkLinrPlacement").val()!=null)
	  {
	  var pkLinePlacement = $("#pkLinrPlacement").val();
		linePlaceData += "pkLinePlacement:'"+pkLinePlacement+ "',";
	  }
 
	 if($("#centralSiteSelectId option:selected").text() == 'Other')
	 {
	 var centralPlaceSiteOther = $("#centralLineSiteOtherId").val();
	  fkCentralPlaceSite = $("#centralSiteSelectId").val(); 
	 linePlaceData += "fkCentralPlaceSite:"+fkCentralPlaceSite+",centralPlaceSiteOther:'"+centralPlaceSiteOther+"',";
	 }
 	else
	 {	
	 fkCentralPlaceSite = $("#centralSiteSelectId").val(); 
	 linePlaceData += "fkCentralPlaceSite:'"+fkCentralPlaceSite+ "',";
	 }
 
 	if($("#lineLocSelectId option:selected").text() == 'Other')
	 {
		var linePlaceLocationOther = $("#lineLocOtherId").val();
			fkLinePlaceLocation = $("#lineLocSelectId").val();
		linePlaceData += "fkLinePlaceLocation:"+fkLinePlaceLocation+",linePlaceLocationOther:'"+linePlaceLocationOther+"',";
	 }
	 else
	 {
	 	fkLinePlaceLocation = $("#lineLocSelectId").val();
		linePlaceData += "fkLinePlaceLocation:'"+fkLinePlaceLocation+ "',";
	 }
	 
	 linePlaceData +="entityId:"+entityId+",";
	 linePlaceData += "entityType:'"+entityType+ "',";
	 linePlaceData += "fkTxWorkflow:'"+fkTxWorkflow+ "'}";
 	 //alert($("#centralSiteSelectId option:selected").text()); 
	 //alert(linePlaceData);
	 var url = "saveLinePlacementDetail";
	 response = jsonDataCall(url,"jsonData={data:["+linePlaceData+"]}");
	 return true; 
	 }

	 
	
function savePages(mode){
	
	if(checkModuleRights(STAFA_RECIPIENTTRACKERS, APPMODE_SAVE)){
				try{
							if(saveLinePlacementDetail()){
								
							}							
							jAlert("  Data Saved Successfully !", "Information ...", function () { 
								openUrl(LoadedPage.url,LoadedPage.workflow,LoadedPage.visitId,LoadedPage.div);
								if(RefreshData!=undefined){
								  Refreshner(RefreshElement,RefreshData);
								}
				            }); 

			                return true;	
				
			}catch(e){
			alert("exception " + e);
		}
	}
}
	
</script>

<form>
<s:hidden id="pkLinrPlacement"  value="%{linePlacementData.pkLinePlacement}"/> 
 <div class="column">      
            <div  class="portlet">
                <div class="portlet-header ">Collection Orders</div>
                        <div class="portlet-content">
						<s:hidden id="pkTxWorkflow_id" name="transplantId" /> 
<s:hidden id="recipientId" name="recipient" />
<s:hidden id="visitId" name="visitId"/> 
<input type="hidden"  id="entityTypeId" value='<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_RECIPIENT"/>'/>
<s:hidden id="personId" name="personId"/>
<div class="column">
<div class="portlet">
<div class="portlet-header "><s:text
	name="stafa.label.HPCMPre-CollectionDetails.preColleDet" /></div>
<div class="portlet-content">
<table cellpadding="0" cellspacing="0" class="" align="center"
	border="0" width="100%">
	<!--<tr>
		<td class="tablewapper"><s:text
			name="stafa.label.centralLinePlaceSite" /></td>
		<td class="tablewapper1"><s:select cssStyle="width:auto"
			id="centralSiteSelectId"
			value="%{linePlacementData.fkCentralPlaceSite}"
			name="centralLineSiteName"
			list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@TYPE_ANELOC]"
			listKey="pkCodelst" listValue="description" headerKey=""
			headerValue="Select" /></td>

		<td class="tablewapper"><s:text name="stafa.label.specifyOther" /></td>
		<td class="tablewapper1"><s:textfield name=""
			value="%{linePlacementData.centralPlaceSiteOther}"
			id="centralLineSiteOtherId" /></td>
	</tr>
	<tr>
		<td class="tablewapper"><s:text
			name="stafa.label.linePlacementLoc" /></td>
		<td class="tablewapper1"><s:select cssStyle="width:auto"
			id="lineLocSelectId" value="%{linePlacementData.fkLinePlaceLocation}"
			name=""
			list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@LINE_PLACEMENT_LOCATION]"
			listKey="pkCodelst" listValue="description" headerKey="0"
			headerValue="Select" /></td>

		<td class="tablewapper" style="width: 15%"><s:text
			name="stafa.label.specifyOther" /></td>
		<td class="tablewapper1" style="width: 27%"><s:textfield name=""
			value="%{linePlacementData.linePlaceLocationOther}"
			id="lineLocOtherId" /></td>
			</tr>

--></table>



</div>
</div>
</div>
		    </div>
    </div>
</div>
	
		
	 <div class="column">      
            <div  class="portlet">
                <div class="portlet-header "><s:text name= "stafa.label.linePlacementDetail"/></div>
                        <div class="portlet-content">	
        

	<jsp:include page="LinePlacementDetail.jsp"></jsp:include>



     </div>
    </div>
</div>
  <div class="cleaner"></div>
  
   <div class="column">      
            <div  class="portlet">
                <div class="portlet-header "><s:text name= " stafa.label.mobilizationdetail"/></div>
                        <div class="portlet-content">	
        

	<jsp:include page="PreColMobilizationDetail.jsp"></jsp:include>



     </div>
    </div>
</div>
  <div class="cleaner"></div>
  </form>
  
  
  
  
  
  