
<%-- <%@ include file="common/includes.jsp" %> --%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script>

$(document).ready(function(){
    var oTable;
     portletMinus();
 	$("#subDIvTitle").html("Insurance Approvals");
     oTable=$("#Insurancestatus").dataTable({
         "sPaginationType": "full_numbers",
         "sDom": 'C<"clear">Rlfrtip',
         "aoColumns": [ { "bSortable":false},
                         null,
                         null,
                         null
                       ],
             "oColVis": {
                 "aiExclude": [ 0 ],
                 "buttonText": "&nbsp;",
                 "bRestore": true,
                 "sAlign": "left"
             },
             "fnInitComplete": function () {
                 $("#InsurancestatusCol").html($('#Insurancestatus_wrapper .ColVis'));
             }
 });
     
     
       oTable=$("#insuranceapproval").dataTable({
            "sPaginationType": "full_numbers",
            "sDom": 'C<"clear">Rlfrtip',
            "aoColumns": [ { "bSortable":false},
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null
                          ],
                "oColVis": {
                    "aiExclude": [ 0 ],
                    "buttonText": "&nbsp;",
                    "bRestore": true,
                    "sAlign": "left"
                },
                "fnInitComplete": function () {
                    $("#insuranceapprovalColumn").html($('#insuranceapproval_wrapper .ColVis'));
                }
    });
      

});

$(function() {
    $('.jclock').jclock();
    $("select").uniform();
  });
  </script>
 
        <div class="column">      
            <div  class="portlet">
                <div class="portlet-header ">Insurance Status</div>
                        <div class="portlet-content">
                        <table>
                            <tr>
                                <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                                <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                              
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" id="Insurancestatus" class="display">
                        <thead>
                            <tr>
                                <th width="4%" id="InsurancestatusCol"></th>
                                <th colspan='1'>Select</th>
                                <th colspan='1'>Insurance Status</th>
                                <th colspan='1'>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                        </table>
            </div>
        </div>
</div>
 
  <div class="cleaner"></div>

 <div class="column">   
        <form>
            <div  class="portlet">
                <div class="portlet-header ">Insurance Approvals</div>
                        <div class="portlet-content">
                        <table>
                            <tr>
                                <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                                <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                              
                            </tr>
                            </table>
                     <table cellpadding="0" cellspacing="0" border="0" id="insuranceapproval" class="display">
                            <thead>
                                <tr>
                                    <th width="4%" id="insuranceapprovalColumn"></th>
                                    <th>Select</th>
                                    <th >Approval Name</th>
                                    <th >I-Plan Code</th>
                                    <th >Auth Date</th>
                                    <th >Auth Code</th>
                                    <th >Eff Date</th>
                                    <th >End Date</th>
                                    <th >Auth Attachment</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                    </table>
            </div>
        </div>
        </form>
</div>
<div class="cleaner"></div>
        <div  style="float:right;">
        <form>
            <input type="button" value="Save"/>
        </form>
        </div>
        <div class="cleaner"></div>
