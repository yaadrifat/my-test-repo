<%@ taglib prefix="s" uri="/struts-tags"%>
<%-- <%@ include file="common/includes.jsp" %> --%>
<!-- <script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" /> -->
<!-- <script type="text/javascript" src="jsp/Patient Donor/RecipientTracking/Consult/Consult.js"></script>
 -->
<script>
$(function() {
    $('.jclock').jclock();
    $("select").uniform();
  });
$(document).ready(function(){
	portletMinus();
	$("#subDIvTitle").html("Consult/Eval");
	var  oTable=$("#consultEvalChecklistDetails").dataTable({
		"sPaginationType": "full_numbers",
		"sDom": 'C<"clear">Rlfrtip',
		"aoColumns": [ { "bSortable":false},
			               null,
			               null,
			               null,
			               null,
			               null,
			               null
			             ],
			"oColVis": {
				"aiExclude": [ 0 ],
				"buttonText": "&nbsp;",
				"bRestore": true,
				"sAlign": "left"
			},
			"fnInitComplete": function () {
		        $("#consultEvalChecklistColumn").html($('.ColVis:eq(0)'));
			}
	}); 
	var  oTable=$("#consultEvalEducationDetails").dataTable({
		"sPaginationType": "full_numbers",
		"sDom": 'C<"clear">Rlfrtip',
		"aoColumns": [ { "bSortable":false},
			               null,
			               null,
			               null,
			               null
			               ],
			"oColVis": {
				"aiExclude": [ 0 ],
				"buttonText": "&nbsp;",
				"bRestore": true,
				"sAlign": "left"
			},
			"fnInitComplete": function () {
		        $("#consultEvalEducationColumn").html($('.ColVis:eq(1)'));
			}
	}); 
    $("select").uniform();
});

var paramObj ={
		entityid:'<s:property value="personId"/>',
		entityType:'<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_PERSON"/>',
		txid:'<s:property value="transplantId"/>',	
		visitId:'<s:property value="visitId"/>'
};

function save_txCoordinator()
{
	var personId = <s:property value="personId"/>;
	var pkRecipient=<s:property value ="recipient"/>;
var transplantId =<s:property value ="transplantId"/>;
var entityType = "recipient";
var transplantData ="{entityId:"+pkRecipient+",";
transplantData += "pkTxWorkflow:'"+transplantId+ "',";
transplantData += "entityType:'"+entityType+ "',";
transplantData += "fkTxCoordinator:'"+$("#txCoordinatorName_id").val()+"'}";

var url = "saveTxCoordinator";
response = jsonDataCall(url,"jsonData={data:["+transplantData+"]}");
return true; 
}


function savePages(mode){
	
	if(checkModuleRights(STAFA_RECIPIENTTRACKERS, APPMODE_SAVE)){
		try{
			if(saveCheckList()){
			}
			if(save_ClinicalInfo(paramObj)){
			}
			if(Documents.saveDoc(ConsultEvalVisitDocParam, ConsultEvalVisitCurrentDoc)){
			}		
			if(save_txCoordinator()){				
				}
			jAlert("  Data Saved Successfully !", "Information ...", function () { 
				openUrl(LoadedPage.url,LoadedPage.workflow,LoadedPage.visitId,LoadedPage.div);
				/*if(RefreshData!=undefined){
				  Refreshner(RefreshElement,RefreshData);
				}*/
            }); 
		}catch(e){
			alert("exception " + e);
		}
	}
}
</script>
		<form>
		
		 <div class="column">       
    <div  class="portlet">
    <div class="portlet-header"><s:text name ="stafa.label.coordinatorAssignment"/></div><br>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
               <tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientTransplantCoordinator"/></td>
       		            		<td class="tablewapper1"><td class="tablewapper1"><s:select id="txCoordinatorName_id" name="txCoordinatorName" value="txCoordinator" list="txCoordinatorName"  headerKey="" headerValue="Select"/></td>
       		            	</tr>
                </table>
        </div>
    </div>
</div>	 
		
				<jsp:include page="../../../basic_clinicalInfo.jsp"></jsp:include>
					<%--<div class="column">       
					    <div  class="portlet">
					    			<div class="portlet-header">Basic Clinical Info</div>
					        			<div class="portlet-content">
								             <table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%">
					            			    <tr>
					                    				<td  width="15%" class="" rowspan="3">Recipient Weight</td>
					                    				<td>
					                    							<div align="left"><img src = "images/icons/addnew.jpg" class="cursor"  onclick=""/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;&nbsp;&nbsp;
					                       														<img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label></div>
					                    				</td>
					                    				<td></td>
					                   					 <td></td>
					               				 </tr>   
					                    		<tr>
									                    <td class="" colspan="3">
					                				        <table id="" cellpadding="0" cellspacing="0" class="display" align="center" border="1" width="80%">
						                            			<thead>
						                            			<tr>
										                                <th>Select</th>
										                                <th>Weight</th>
										                                <th>Unit</th>
										                                <th>Date</th>
						                           			 </tr>   
						                            		</thead>
								                            <tbody align="center">
						        		                    <tr>
								                                <td> <input type="checkbox" id="" ></td>
								                                <td> </td>
								                                <td> </td>
								                                <td> </td>
								                            </tr>
								                            <tr>
								                                <td> <input type="checkbox" id="" ></td>
								                                <td> </td>
								                                <td> </td>
								                                <td> </td>
								                            </tr>
								                            <tr>
								                                <td> <input type="checkbox" id="" ></td>
								                                <td> </td>
								                                <td> </td>
								                                <td> </td>
								                            </tr>
						                           		 </tbody>
						                        </table>
					                    	</td>
					                     	<td></td>
					                   </tr>
					                   <tr>
						                   <td>Recipient Height(cm)</td>
						                   <td><input type="text"></td>
						                   <td>Recipient Height(cm)</td>
						                   <td><input type="text"></td>
					                   </tr>
					                <tr>
					                <td>Allergies</td>
					                <td colspan="3"><textarea rows="3" cols="70"></textarea></td>
					                </tr>
					            </table>
					        </div>
					    </div>
					</div> --%>
					<div class="cleaner"></div>
					
					
					<s:set var="id" value="%{'visitDocuments'}" />  
						  <div class="column">
							<div  class="portlet">
										<div class="portlet-header "><s:text name = "stafa.label.visitdocument"/></div><br>
										<span class="hidden">
											<s:select id="%{#attr['id']+'List'}" name="visitDocuments1" list="visitDocuments"  headerKey="" headerValue="Select"/>
										</span>
										<jsp:include page="../../../doc_activity.jsp"></jsp:include>					
							</div>
						  </div>					
					
					<div class="column">       
					    <div  class="portlet">
									<div class="portlet-header ">Consult/Eval Checklist</div><br>
										<div class="portlet-content">
											<table>
												<tr>
													<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
													<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
											    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
												</tr>
											</table>
											<table cellpadding="0" cellspacing="0" border="0" id="consultEvalChecklistDetails" class="display">
											<thead>
												<tr>
													<th width="4%" id="consultEvalChecklistColumn"></th>
													<th colspan='1'>Select</th>
													<th colspan='1'>Items</th>
													<th colspan='1'>Date</th>
													<th colspan='1'>Document Name</th>
													<th colspan='1'>Attachment</th>
													<th colspan='1'>Status</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td></td>
													<td><input type='checkbox'  id="consultEvalChecklistVerified" onclick=""></td>
													<td>Eval / Assesment By MD</td>
													<td></td>
													<td>	</td>
													<td><input type="button" id="" value="Browse" /></td>
													<td></td>
											</tr>
											</tbody>
											</table>
								</div>
							</div>
					</div>
					   
					<div class="cleaner"></div>
					
					<div class="column">
					    <div  class="portlet">
									<div class="portlet-header ">Consult/Eval Education</div><br>
										<div class="portlet-content">
											<table>
												<tr>
													<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
													<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
											    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
												</tr>
											</table>
											<table cellpadding="0" cellspacing="0" border="0" id="consultEvalEducationDetails" class="display">
											<thead>
												<tr>
													<th width="4%" id="consultEvalEducationColumn"></th>
													<th colspan='1'>Select</th>
													<th colspan='1'>Items</th>
													<th colspan='1'>Date</th>
													<th colspan='1'>Status</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td></td>
													<td><input type='checkbox'  id="consultEvalDocumentVerified" onclick=""></td>
													<td>Patient info Packet</td>
													<td></td>
													<td>	<select id="ABO/Rh"><option>Select</option></select></td>
												</tr>
											</tbody>
											</table>
								</div>
							</div>
					
					</div>
									
					<jsp:include page="../../../visit_checklist.jsp"/>
	
	
					<div align="right" style="float:right;">
					
					        <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
					            <tr>
					                <td>Next:</td>
					                <td><div class="mainselection"><select name="" id="" ><option value="">Select</option><option>Preparation</option><option>Verification</option><option>Collection</option><option>Post-Collection</option></select></div></td>
					                <td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;&nbsp;</td>
					                <td><input type="button" value="Next" onclick="saveOvernightStorage()"/></td>
					            </tr>
					        </table>
					
					</div>
					
 
		
		</form>
<%-- <div class="floatright_right">
    <jsp:include page="sidecontrols.jsp"></jsp:include>
 </div> --%>
 
<div class="cleaner"></div>
       



 <script type="text/javascript">

var ConsultEvalVisitDocParam={		 
		
			 widgetType : <s:property value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@TYPE_DOCTYPE, @com.velos.stafa.util.VelosStafaConstants@SUBTYPE_VISITDOCUMENT)"/>,
			 entityId : '<s:property value="recipient"/>',
			 entityType : '<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_RECIPIENT"/>',
			 txId : paramObj.txid,
			 visitId : paramObj.visitId  

 };

var ConsultEvalVisitCurrentDoc={

		     TABLE: '<s:property value="id"/>CurrTable',
			 TABLECol : '<s:property value="id"/>CurrColumn',
			 TABLEIdPrefix : '<s:property value="id"/>'

 };

 var ConsultEvalVisitHistDoc={

		     TABLE: '<s:property value="id"/>HistTable',
			 TABLECol : '<s:property value="id"/>HistColumn',
			 TABLEIdPrefix : '<s:property value="id"/>'
 };

 
 var docTableHeaders = new Array();
			 docTableHeaders[0]='<s:text name = "stafa.lable.documentTitle"/>';
			 docTableHeaders[1]='<s:text name = "stafa.lable.documentVersion"/>';
			 docTableHeaders[2]='<s:text name = "stafa.lable.documentDate"/>';
			 docTableHeaders[3]='<s:text name = "stafa.lable.Attachment"/>';
			 docTableHeaders[4]='Status';
 
 $('#<s:property value="id"/>Tabs' ).tabs();
 
function constructDocTable(table){
	if(table==ConsultEvalVisitCurrentDoc.TABLE){
		
		DocTable.currentTable(true,ConsultEvalVisitDocParam.entityId,ConsultEvalVisitDocParam.entityType, ConsultEvalVisitDocParam.txId, ConsultEvalVisitDocParam.visitId, table, docTableHeaders, ConsultEvalVisitCurrentDoc.TABLECol, ConsultEvalVisitCurrentDoc.TABLEIdPrefix, ConsultEvalVisitDocParam.widgetType);
				
	}
}

//preparing document widget
DocTable.currentTable(false, ConsultEvalVisitDocParam.entityId, ConsultEvalVisitDocParam.entityType, ConsultEvalVisitDocParam.txId, ConsultEvalVisitDocParam.visitId, ConsultEvalVisitCurrentDoc.TABLE, docTableHeaders, ConsultEvalVisitCurrentDoc.TABLECol, ConsultEvalVisitCurrentDoc.TABLEIdPrefix, ConsultEvalVisitDocParam.widgetType);
DocTable.historyTable(false, ConsultEvalVisitDocParam.entityId, ConsultEvalVisitDocParam.entityType, ConsultEvalVisitDocParam.txId, ConsultEvalVisitDocParam.visitId, ConsultEvalVisitHistDoc.TABLE, docTableHeaders, ConsultEvalVisitHistDoc.TABLECol, ConsultEvalVisitHistDoc.TABLEIdPrefix, ConsultEvalVisitDocParam.widgetType);

</script>

 