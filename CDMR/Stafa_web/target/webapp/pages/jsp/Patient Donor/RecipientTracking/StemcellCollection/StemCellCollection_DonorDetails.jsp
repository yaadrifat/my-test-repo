<%-- <%@ include file="common/includes.jsp" %> --%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script>

$(function() {
    $('.jclock').jclock();
    //alert($(".portlet").find( ".portlet-header" ).find("span").length);
  });
function add_DonorRows(){
	  $("#recipientids").prepend("<tr>"+
	            "<td><input type='checkbox' id=''/></td>"+
	            "<td><input type='text' id=''/></td>"+
	            "<td><input type='text' id=''/></td>"+
	            "<td><input type='text' id=''/></td>"+
	            "</tr>");
	}
  function showHiddenDiv(obj){
	  var donorType=$(obj).find("option:selected").text();
	  if(donorType=="Allo"){
		  $("#recipientBasicInfo").css("display","block");
	  }else{
		  $("#recipientBasicInfo").css("display","none");				  
	  }
  }
  function   showFewDiv(obj){
	  var donorType=$(obj).find("option:selected").text();
	  if(donorType=="HPC-Single"||donorType=="HPC-Multiple"){
		  $("#DonorBasicInfo").css("display","none");
		  $("#preCollectionChecklist").css("display","none");
		  $("#recipientBasicInfo").css("display","none");
	  }else{
		  $("#DonorBasicInfo").css("display","block");
		  $("#preCollectionChecklist").css("display","block");
	  }
	  
  }
  function showDonorID(){
	    showPopUp('open','addmodal','Donor IDs','800','300');
	}

$(document).ready(function(){
    //$("select").uniform(); 
    portletMinus();
 	$("#subDIvTitle").html("Stem-Cell Collection - Donor Details");
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"});
     // $(window).resize();
	//show_eastslidewidget("slidedivs");
		 			var  oTable3=$("#collectionChecklist").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										               null
	 										             ],
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#collectionChecklistColumn").html($('#collectionChecklist_wrapper .ColVis'));
												}
									});
		 			var  oTable=$("#reviewConsents").dataTable({
						"sPaginationType": "full_numbers",
						"sDom": 'C<"clear">Rlfrtip',
						"aoColumns": [ { "bSortable":false},
							               null,
							               null,
							               null,
							               null,
							               null
							               ],
							"oColVis": {
								"aiExclude": [ 0 ],
								"buttonText": "&nbsp;",
								"bRestore": true,
								"sAlign": "left"
							},
							"fnInitComplete": function () {
						        $("#reviewConsentsColumn").html($('#reviewConsents_wrapper .ColVis'));
							}
				});
					    
		 		   
});
$("select").uniform(); 
</script>
<div class="column">
<form>
    <div  class="portlet">
    <div class="portlet-header">Donor Details</div>
        <div class="portlet-content">
       
             <table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%">
				<tr>
					<td>Donor Type</td>
					<td ><select id='donorType' onchange="showHiddenDiv(this)"><option>Select</option><option>Auto</option><option>Allo</option></select></td>
				</tr>
				<tr>
					<td>Product Type</td>
					<td><select id='productType' onchange="showFewDiv(this)"><option>Select</option><option>HPC-A</option><option>HPC-Single</option><option>HPC-Multiple</option></select></td>
				</tr>
				<tr>
					<td>Donor IDS</td>
					<td><input type="button"  name="" id="" value="view" onclick="showDonorID()"></td>
				</tr>                  
          		<tr>
					<td>Donor Gender</td>
					<td><select id='diagnosis'><option>Select</option><option>Male</option><option>Female</option></select></td>
				</tr>     
				<tr>
					<td>Donor Age</td>
					<td><input type="text" name=""  id="" value="view" onclick=""></td>
				</tr>           
            </table>
        </div>
    </div>
    <div  class="portlet"  id="DonorBasicInfo">
    <div class="portlet-header">Donor Basic Clinical Info</div>
        <div class="portlet-content">
       
             <table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%">
                <tr>
                    <td  width="15%" class="" rowspan="3">Donor Weight(Kg)</td>
                    <td>
                     <div align="left"><img src = "images/icons/addnew.jpg" class="cursor"  onclick=""/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;&nbsp;&nbsp;
                       <img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label></div>
                    </td>
                    <td></td>
                    <td></td>
                <tr>   
                    <tr>
                  
                    <td class="" colspan="3">
                        <table id="" cellpadding="0" cellspacing="0" class="display" align="center" border="1" width="80%">
                            <thead>
                            <tr>
                                <th>Select</th>
                                <th>Weight</th>
                                <th>Unit</th>
                                <th>Date</th>
                            </tr>   
                            </thead>
                           
                            <tbody align="center">
                            <tr>
                                <td> <input type="checkbox" id="" ></td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td> <input type="checkbox" id="" ></td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td> <input type="checkbox" id="" ></td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            </tbody>
                       
                        </table>

                    </td>
                     <td></td>
                   </tr>
                 <tr>
                  	 <td>Donor Height(cm)</td>
                   	<td><input type="text"></td>
                 </tr>
                <tr>
                <td>Allergies</td>
                <td colspan="3"><textarea rows="3" cols="70"></textarea></td>
                </tr>
              	<tr>
              		<td>ABO/Rh</td>
              		<td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
              		<td>AntiBody Screen</td>
              		<td><input type="text"></td>
              	</tr>
            </table>
        </div>
    </div>
		<div class="portlet" id="recipientBasicInfo" style="display: none">
			<div class="portlet-header">Recipient Basic Clinical Info</div>
			<div class="portlet-content">

				<table cellpadding="0" cellspacing="8" class="" align="center"
					border="0" width="85%">
					<tr>
						<td width="15%" class="" rowspan="3">Recipient Weight(Kg)</td>
						<td>
							<div align="left">
								<img src="images/icons/addnew.jpg" class="cursor" onclick="" />&nbsp;&nbsp;<label
									class="cursor" onclick=""><b>Add</b>
								</label>&nbsp;&nbsp;&nbsp; <img src="images/icons/no.png"
									style="width: 16; height: 16;" class="cursor" id="" "/>&nbsp;&nbsp;<label
									class="cursor" onclick=""><b>Delete</b>
								</label>
							</div></td>
						<td></td>
						<td></td>
					<tr>
					<tr>

						<td class="" colspan="3">
							<table id="" cellpadding="0" cellspacing="0" class="display"
								align="center" border="1" width="80%">
								<thead>
									<tr>
										<th>Select</th>
										<th>Weight</th>
										<th>Unit</th>
										<th>Date</th>
									</tr>
								</thead>

								<tbody align="center">
									<tr>
										<td><input type="checkbox" id="">
										</td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td><input type="checkbox" id="">
										</td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td><input type="checkbox" id="">
										</td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</tbody>

							</table></td>
						<td></td>
					</tr>
					<tr>
						<td>Recipient Height(cm)</td>
						<td><input type="text">
						</td>
					</tr>
					<tr>
						<td>Allergies</td>
						<td colspan="3"><textarea rows="3" cols="70"></textarea>
						</td>
					</tr>
					<tr>
						<td>ABO/Rh</td>
						<td><select id='diagnosis'><option>Select</option>
								<option>HPC-A</option>
								<option>HPC-C</option>
						</select>
						</td>
						<td>AntiBody Screen</td>
						<td><input type="text">
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div id="addmodal" style="display:none;">
 			<div align="left">
           	 <table width="28%">
                <tr>
                    <td style="width:5%"><div><img src = "images/icons/addnew.jpg" class="cursor" id=""  onclick="add_DonorRows()"/>&nbsp;&nbsp;<label  class="cursor" ><b>Add </b></label>&nbsp;</div></td>
                    <td style="width:5%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id =""/>&nbsp;&nbsp;<label class="cursor" ><b>Delete</b></label>&nbsp;</div></td>
                </tr>
            </table>
			</div>   <br>        
            <table cellpadding="0" cellspacing="0" border="1" id="recipientids" class="display">
                <thead>
                    <tr>
                        <th>Select</th>
                        <th>Donor ID</th>
                        <th>ID Source</th>
                        <th>ID Type</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input type="checkbox" id="" class=""/></td>
                        <td><input type="text" id="" value=""/></td>
                        <td><input type="text" id="" value=""/></td>
                        <td><input type="text" id="" value=""/></td>
                    </tr>
                </tbody>
            </table>
</div>
        <div class="column">      
            <div  class="portlet" id="preCollectionChecklist">
                <div class="portlet-header ">Pre-Collection Checklist</div>
                        <div class="portlet-content">
                        <table>
                            <tr>
                                <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                                <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                              
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" id="collectionChecklist" class="display">
                        <thead>
                            <tr>
                                <th width="4%" id="collectionChecklistColumn"></th>
                                <th colspan='1'>Select</th>
                                <th colspan='1'>items</th>
                                <th colspan='1'>Date</th>
                                <th colspan='1'>Document Name</th>
                                <th colspan='1'>Attachment</th>
                                <th colspan='1'>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                        	<td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>Verify IDMs are Current (within 30 days of Collection)</td>
                            <td><input type="text" id="" value="" class="dateEntry"/></td>
                              <td><input type="text" id="" value=""/></td>
                            <td><img src="images/icons/attach.png"></td>
                            <td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
                        </tr>
                          <tr>
                          <td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>Verify Pregnancy Test is Current (within 7 days Prior to mobilization)</td>
                            <td><input type="text" id="" value="" class="dateEntry"/></td>
                              <td><input type="text" id="" value=""/></td>
                            <td><img src="images/icons/attach.png"></td>
                            <td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
                        </tr>
                          <tr>
                          <td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>Review Donor Eligiblity</td>
                            <td><input type="text" id="" value="" class="dateEntry"/></td>
                              <td><input type="text" id="" value=""/></td>
                            <td><img src="images/icons/attach.png"></td>
                            <td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
                        </tr>
                          <tr>
                            <td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>Verify 	Donor card is Complete and signed off</td>
                            <td><input type="text" id="" value="" class="dateEntry"/></td>
                              <td><input type="text" id="" value=""/></td>
                            <td><img src="images/icons/attach.png"></td>
                            <td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
                        </tr>
                          <tr>
                          <td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>Compete BSA form</td>
                            <td><input type="text" id="" value="" class="dateEntry"/></td>
                              <td><input type="text" id="" value=""/></td>
                            <td><img src="images/icons/attach.png"></td>
                            <td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
                        </tr>
                        </tbody>
                        </table>
            </div>
        </div>
</div>
 
  <div class="cleaner"></div>

 <div class="column">   
        
            <div  class="portlet">
                <div class="portlet-header ">ReviewConsents</div>
                        <div class="portlet-content">
                        <table>
                            <tr>
                                <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                                <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                              
                            </tr>
                            </table>
                   	 <table cellpadding="0" cellspacing="0" border="0" id="reviewConsents" class="display">
                        <thead>
                            <tr>
                                <th width="4%" id="reviewConsentsColumn"></th>
                                <th colspan='1'>Select</th>
                                <th colspan='1'>Document Name</th>
                                <th colspan='1'>Document Complete</th>
                                <th colspan='1'>Attachment</th>
                                <th colspan='1'>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                        	<td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>Donor Consent for  Collection</td>
                            <td><input type="text" id="" value="" class="dateEntry"/></td>
                            <td><img src="images/icons/attach.png"></td>
                            <td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
                        </tr>
                          <tr>
                          <td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>Informed Consent for Exceptional Use</td>
                            <td><input type="text" id="" value="" class	="dateEntry"/></td>
                            <td><img src="images/icons/attach.png"></td>
                            <td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
                        </tr>
                          <tr>
                          <td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>Donor Parent's or Legal Guardian Consent</td>
                            <td><input type="text" id="" value="" class="dateEntry"/></td>
                            <td><img src="images/icons/attach.png"></td>
                            <td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
                        </tr>
                          <tr>
                            <td></td>
                            <td><input type="checkbox" id="" class=""/></td>
                            <td>Donor Consent to release Donor's Health Info to the Transplant Physician and Recipient'</td>
                            <td><input type="text" id="" value="" class="dateEntry"/></td>
                            <td><img src="images/icons/attach.png"></td>
                            <td><select id='diagnosis'><option>Select</option><option>HPC-A</option><option>HPC-C</option></select></td>
                        </tr>
                        </tbody>
                        </table>
            </div>
        </div>
        
</div>
  <div class="cleaner"></div>

	</form>
</div>
	
