<%@ taglib prefix="s" uri="/struts-tags"%>

		<jsp:include page="../../../basic_clinicalInfo.jsp"></jsp:include>

		<div class="cleaner"></div>

		<s:set var="id" value="%{'visitDocuments'}" />  
			 <div class="column">
				<div  class="portlet">
					<div class="portlet-header "><s:text name = "stafa.label.visitdocument"/></div><br>
						<span class="hidden">
							<s:select id="%{#attr['id']+'List'}" name="visitDocuments1" list="visitDocuments"  headerKey="" headerValue="Select"/>
						</span>
								<jsp:include page="../../../doc_activity.jsp"></jsp:include>					
							</div>
		</div>
		
		<div class="cleaner"></div>			
			
<script type="text/javascript">


	var InterimVisitDocParam={		 
			
				 widgetType : <s:property value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@TYPE_DOCTYPE, @com.velos.stafa.util.VelosStafaConstants@SUBTYPE_VISITDOCUMENT)"/>,
				 entityId  	: '<s:property value="recipient"/>',
				 entityType : '<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_RECIPIENT"/>',
				 txId 		: '<s:property value="transplantId"/>',
				 visitId 	: '<s:property value="visitId"/>'  
	
	 };
	
	var InterimVisitCurrentDoc={
	
			     TABLE			: '<s:property value="id"/>CurrTable',
				 TABLECol 		: '<s:property value="id"/>CurrColumn',
				 TABLEIdPrefix	: '<s:property value="id"/>'
	
	 };
	
	 var InterimVisitHistDoc={
	
			     TABLE			: '<s:property value="id"/>HistTable',
				 TABLECol 		: '<s:property value="id"/>HistColumn',
				 TABLEIdPrefix 	: '<s:property value="id"/>'
	 };
	
	 
	 var docTableHeaders = new Array();
				 docTableHeaders[0]='<s:text name = "stafa.lable.documentTitle"/>';
				 docTableHeaders[1]='<s:text name = "stafa.lable.documentVersion"/>';
				 docTableHeaders[2]='<s:text name = "stafa.lable.documentDate"/>';
				 docTableHeaders[3]='<s:text name = "stafa.lable.Attachment"/>';
				 docTableHeaders[4]='Status';
	 
	 $('#<s:property value="id"/>Tabs' ).tabs();
	 
	function constructDocTable(table){
		if(table==InterimCurrentDoc.TABLE){
			
			DocTable.currentTable(true,InterimVisitDocParam.entityId,InterimVisitDocParam.entityType, InterimVisitDocParam.txId, InterimVisitDocParam.visitId, table, docTableHeaders, InterimVisitCurrentDoc.TABLECol, InterimVisitCurrentDoc.TABLEIdPrefix, InterimVisitDocParam.widgetType);
					
		}else if(table==ReciepCurrentDoc.TABLE){
			
			DocTable.currentTableExHead(false, ReciepDocParam.entityId, ReciepDocParam.entityType, ReciepDocParam.txId, ReciepDocParam.visitId, table, docTableHeaders, ReciepCurrentDoc.TABLECol, ReciepCurrentDoc.TABLEIdPrefix, ReciepDocParam.widgetType);
			
		}
	}
	
	//preparing document widget
	DocTable.currentTable(false, InterimVisitDocParam.entityId, InterimVisitDocParam.entityType, InterimVisitDocParam.txId, InterimVisitDocParam.visitId, InterimVisitCurrentDoc.TABLE, docTableHeaders, InterimVisitCurrentDoc.TABLECol, InterimVisitCurrentDoc.TABLEIdPrefix, InterimVisitDocParam.widgetType);
	DocTable.historyTable(false, InterimVisitDocParam.entityId, InterimVisitDocParam.entityType, InterimVisitDocParam.txId, InterimVisitDocParam.visitId, InterimVisitHistDoc.TABLE, docTableHeaders, InterimVisitHistDoc.TABLECol, InterimVisitHistDoc.TABLEIdPrefix, InterimVisitDocParam.widgetType);
</script>

	<s:set var="id" value="%{'referralDocuments'}" />   
	<s:set var="column" value="%{'false'}" />   
	<div class="column">
		<div  class="portlet">
					<div class="portlet-header "><s:text name = "stafa.label.recipientConsents"/></div>		
					<span class="hidden">
					  <s:select id="%{#attr['id']+'List'}" name="recipientConsents1" list="recipientConsents"  headerKey="" headerValue="Select"/>
					</span>
					<jsp:include page="../../../doc_activity.jsp"></jsp:include>					
			</div>
		</div>

	<jsp:include page="../../../visit_checklist.jsp"/>
	
<script type="text/javascript">

		//To int the portlet border
		$('#preTransLoadDiv div.column')
		.find('div.portlet')
		.attr({	class:'portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'});


	var ReciepDocParam ={
	
			 widgetType : <s:property value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@TYPE_REFERRAL, @com.velos.stafa.util.VelosStafaConstants@SUBTYPE_REFERRALDOC)"/>,
			 entityId 	: '<s:property value="recipient"/>',
			 entityType : '<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_RECIPIENT"/>',
			 txId 		: '<s:property value="transplantId"/>',
			 visitId 	: '<s:property value="visitId"/>'
			
	};
	
	var ReciepCurrentDoc={
	
		     TABLE: '<s:property value="id"/>CurrTable',
			 TABLECol : '<s:property value="id"/>CurrColumn',
			 TABLEIdPrefix : '<s:property value="id"/>'
	
	};
	
	var ReciepHistDoc={
	
		     TABLE: '<s:property value="id"/>HistTable',
			 TABLECol : '<s:property value="id"/>HistColumn',
			 TABLEIdPrefix : '<s:property value="id"/>'
	};

	$('#<s:property value="id"/>Tabs' ).tabs();
	DocTable.currentTableExHead(false, ReciepDocParam.entityId, ReciepDocParam.entityType, ReciepDocParam.txId, ReciepDocParam.visitId, ReciepCurrentDoc.TABLE, docTableHeaders, ReciepCurrentDoc.TABLECol, ReciepCurrentDoc.TABLEIdPrefix, ReciepDocParam.widgetType);
	DocTable.historyTableExHead(false, ReciepDocParam.entityId, ReciepDocParam.entityType, ReciepDocParam.txId, ReciepDocParam.visitId, ReciepHistDoc.TABLE, docTableHeaders, ReciepHistDoc.TABLECol, ReciepHistDoc.TABLEIdPrefix, ReciepDocParam.widgetType);
	

	var paramObj ={
			entityid:'<s:property value="personId"/>',
			entityType:'<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_PERSON"/>',
			txid:'<s:property value="transplantId"/>',	
			visitId:'<s:property value="visitId"/>'
	};

	   
   function savePages(mode){
		
		if(checkModuleRights(STAFA_RECIPIENTTRACKERS, APPMODE_SAVE)){
			try{
				if(saveCheckList()){
				}
				if(save_ClinicalInfo(paramObj)){
				}
				if(Documents.saveDocs(InterimVisitDocParam, ReciepDocParam, InterimVisitCurrentDoc, ReciepCurrentDoc)){
				}	
			
				jAlert("  Data Saved Successfully !", "Information ...", function () { 
					openUrl(LoadedPage.url,LoadedPage.workflow,LoadedPage.visitId,LoadedPage.div);
					
	            }); 
			}catch(e){
				alert("exception " + e);
			}
		}
	}
</script>

		