<%-- <%@ include file="jsp/common/includes.jsp"%>  --%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/workFlow.js"></script>
<script type="text/javascript" src="js/dataSearch.js"></script>
<link type="text/css" href="css/workFlow.css" rel="stylesheet" />
<script>
$(function() {
	 $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" ) 
    .append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" ></span>"); 
    $('.jclock').jclock();
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });
  /*  $( ".portlet-header .ui-addnew" ).click(function() {
		 showPopUp("open","addRecipient","Add New Recipient","500","280");

    });  */
    hide_innernorth()
  });

function add_Recipient(){
	//?BB $("#donorNewForm").reset();
	//if(checkModuleRights(STAFA_DONORLIST,APPMODE_ADD)){
		addNewRecipient();
//	}
	// showPopUp("open","addNewDonor","Add New Donor / Collection Cycle","1200","550");
	

	//alert("test");
	/*//?BB 
	  stafaPopUp("open","addNewDonor","Add New Donor / Collection Cycle","900","600");
	  //$("#addNewDonor").dialog("open");
	   $.uniform.restore('select');
		$("select").uniform();
		addDatepicker();
	*/
}

function constructRecipientTrackerList(flag){
	
	
	var recipientListTable_serverParam = function ( aoData ) {

		aoData.push( { "name": "application", "value": "stafa"});
		aoData.push( { "name": "module", "value": "aphresis_recipient"});	

		aoData.push( {"name": "col_0_name", "value": "pkRec" } );
		aoData.push( { "name": "col_0_column", "value": "0"});
		
		aoData.push( {"name": "col_1_name", "value": "lower(mrn)" } );
		aoData.push( { "name": "col_1_column", "value": "1"});
		
		aoData.push( { "name": "col_2_name", "value": "lower(lname)"});
		aoData.push( { "name": "col_2_column", "value": "2"});
		
		aoData.push( { "name": "col_3_name", "value": "lower(fname)"});
		aoData.push( { "name": "col_3_column", "value": "3"});
		
		aoData.push( { "name": "col_4_name", "value": "lower(dob)"});
		aoData.push( { "name": "col_4_column", "value": "4"});
		
		aoData.push( { "name": "col_5_name", "value": "age"});
		aoData.push( { "name": "col_5_column", "value": "5"});
		
		aoData.push( { "name": "col_6_name", "value": "lower(diagnosis)"});
		aoData.push( { "name": "col_6_column", "value": "6"});
		
		aoData.push( { "name": "col_7_name", "value": "age"});
		aoData.push( { "name": "col_7_column", "value": "7"});

		aoData.push( { "name": "col_8_name", "value": "age"});
		aoData.push( { "name": "col_8_column", "value": "8"});
		aoData.push( { "name": "col_9_name", "value": "age"});
		aoData.push( { "name": "col_9_column", "value": "9"});
		aoData.push( { "name": "col_10_name", "value": "age"});
		aoData.push( { "name": "col_10_column", "value": "10"});
		aoData.push( { "name": "col_11_name", "value": "age"});
		aoData.push( { "name": "col_11_column", "value": "11"});
		aoData.push( { "name": "col_12_name", "value": "age"});
		aoData.push( { "name": "col_12_column", "value": "12"});
		aoData.push( { "name": "col_13_name", "value": "age"});
		aoData.push( { "name": "col_13_column", "value": "13"});
		aoData.push( { "name": "col_14_name", "value": "age"});
		aoData.push( { "name": "col_14_column", "value": "14"});
		aoData.push( { "name": "col_15_name", "value": "age"});
		aoData.push( { "name": "col_15_column", "value": "15"});
		aoData.push( { "name": "col_16_name", "value": "age"});
		aoData.push( { "name": "col_16_column", "value": "16"});
		aoData.push( { "name": "col_17_name", "value": "age"});
		aoData.push( { "name": "col_17_column", "value": "17"});
		aoData.push( { "name": "col_18_name", "value": "age"});
		aoData.push( { "name": "col_18_column", "value": "18"});
		aoData.push( { "name": "col_19_name", "value": "age"});
		aoData.push( { "name": "col_19_column", "value": "19"});

		aoData.push( { "name": "col_20_name", "value": "age"});
		aoData.push( { "name": "col_20_column", "value": "20"});

		aoData.push( { "name": "col_21_name", "value": "age"});
		aoData.push( { "name": "col_21_column", "value": "21"});

		aoData.push( { "name": "col_22_name", "value": "age"});
		aoData.push( { "name": "col_22_column", "value": "22"});

		aoData.push( { "name": "col_23_name", "value": "age"});
		aoData.push( { "name": "col_23_column", "value": "23"});

		aoData.push( { "name": "col_24_name", "value": "age"});
		aoData.push( { "name": "col_24_column", "value": "24"});

		aoData.push( { "name": "col_25_name", "value": "age"});
		aoData.push( { "name": "col_25_column", "value": "25"});

		aoData.push( { "name": "col_26_name", "value": "age"});
		aoData.push( { "name": "col_26_column", "value": "26"});

		aoData.push( { "name": "col_27_name", "value": "age"});
		aoData.push( { "name": "col_27_column", "value": "27"});
		
		
		/*aoData.push( { "name": "col_7_name", "value": "depid"});
		aoData.push( { "name": "col_7_column", "value": "7"});
		
		aoData.push( { "name": "col_8_name", "value": "lower(recip_blood)"});
		aoData.push( { "name": "col_8_column", "value": "14"});
		
		aoData.push( { "name": "col_9_name", "value": "lower(diagnosis)"});
		aoData.push( { "name": "col_9_column", "value": "15"});
		
		aoData.push( { "name": "col_10_name", "value": "lower(labresult)"});
		aoData.push( { "name": "col_10_column", "value": "5"});
		
		aoData.push( { "name": "col_11_name", "value": "lower(documents)"});
		aoData.push( { "name": "col_11_column", "value": "6"});
		
		aoData.push( { "name": "col_12_name", "value": "lower(donationtype)"});
		aoData.push( { "name": "col_12_column", "value": "7"});
		
		aoData.push( { "name": "col_13_name", "value": "lower(planprocedure)"});
		aoData.push( { "name": "col_13_column", "value": "8"});
		
		aoData.push( { "name": "col_14_name", "value": "lower(nurse)"});
		aoData.push( { "name": "col_14_column", "value": "9"});
		
		aoData.push( { "name": "col_15_name", "value": "lower(status)"});
		aoData.push( { "name": "col_15_column", "value": "16"}); */

													};

													
	var recipientListTable_aoColumnDef = [
			                         {"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
			                             return "<input name='recipeintId" + source[0] +"' id= 'recipeintId" + source[0] +"' type='checkbox' value='" + source[0] +"' onclick='openReferal("+source[0]+")' />";
			                             }},
			                          {    "sTitle":'<s:text name="stafa.label.recipientid"/>',
			                                 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
			                                	 //?BB link = "<a href='#' onclick='javascript:getDonorDetails(\""+source[0] +"\");'>"+source[2]+"</a>";
			                                	 link = source[1];			                                	
			                             return link;
			                             }},
			                          {    "sTitle":'<s:text name="stafa.label.lastName"/>',
			                                 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
			                                	 //?BB link = "<a href='#' onclick='javascript:getDonorDetails(\""+source[0] +"\");'>"+source[3] +"</a>";
			                                	 link = source[2] ;
					                             return link;
			                                 
			                                    }},
			                          {    "sTitle":'<s:text name="stafa.label.firstName"/>',
			                                        "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
			                                    return source[3];
			                                  }},
			                                  {    "sTitle":'<s:text name="stafa.label.dob"/>',
				                                  "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
				                                      return source[4];
				                              }}, 
				                              {    "sTitle":'<s:text name="stafa.label.age"/>',
				                                  "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
				                                      return source[5];
				                              }}, 
				                              
			                          {    "sTitle":'<s:text name="stafa.label.diagnosis"/>',
			                                        "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
			                                    return source[6];
			                                  }},    
			                                  {    "sTitle":'Donor Type',
			                                        "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
			                                    return "";
			                                  }},    
			                                  {    "sTitle":'Referral Date',
			                                        "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
			                                    return "";
			                                  }},  
			                                  {    "sTitle":'Transplant Physician',
			                                        "aTargets": [ 9], "mDataProp": function ( source, type, val ) {
			                                    return "";
			                                  }},    
			                          {    "sTitle":'Transplant Coordinator',
			                                        "aTargets": [ 10], "mDataProp": function ( source, type, val ) {
			                                    return "";
			                                  }},    
			                          {    "sTitle":'Dept',
			                                        "aTargets": [ 11], "mDataProp": function ( source, type, val ) {
			                                        	return "";
			                                  }},       
			                                  
			                          {   "sTitle":'Plan',
			                                  "aTargets": [ 12], "mDataProp": function ( source, type, val ) {
			                                      return "";
			                              }},
			                          {    "sTitle":'Status',
			                                  "aTargets": [ 13], "mDataProp": function ( source, type, val ) {
			                                	  return "";
			                              }},
			                          {    "sTitle":'Primary Insurance',
			                                  "aTargets": [ 14], "mDataProp": function ( source, type, val ) {
			                                      return "";
			                              }},
			                              {    "sTitle":'Secondary Insurance',
			                                  "aTargets": [ 15], "mDataProp": function ( source, type, val ) {
			                                	  return "";
			                                	 
			                              }},
			                              {    "sTitle":'Consult/Eval Date',
			                                  "aTargets": [ 16], "mDataProp": function ( source, type, val ) {
			                                	  return "";
			                                	 
			                              }},
			                              {    "sTitle":'Pre Transplant Visit Date',
			                                  "aTargets": [ 17], "mDataProp": function ( source, type, val ) {
			                                	  return "";
			                                	 
			                              }},
			                              {    "sTitle":'Collection Date',
			                                  "aTargets": [ 18], "mDataProp": function ( source, type, val ) {
			                                	  return "";
			                                	 
			                              }} ,
			                              {    "sTitle":'Admission Date',
			                                  "aTargets": [ 19], "mDataProp": function ( source, type, val ) {
			                                	  return "";
			                                	 
			                              }} ,
			                              {    "sTitle":'Treatment Location',
			                                  "aTargets": [ 20], "mDataProp": function ( source, type, val ) {
			                                	  return "";
			                                	 
			                              }},
			                              {    "sTitle":'Transplant Date',
			                                  "aTargets": [ 21], "mDataProp": function ( source, type, val ) {
			                                	  return "";
			                                	 
			                              }} ,
			                              {    "sTitle":'Pre Protocol',
			                                  "aTargets": [ 22], "mDataProp": function ( source, type, val ) {
			                                	  return "";
			                                	 
			                              }} ,
			                              {    "sTitle":'Comorbidity Index',
			                                  "aTargets": [ 23], "mDataProp": function ( source, type, val ) {
			                                	  return "";
			                                	 
			                              }} ,
			                              {    "sTitle":'Karnofsky',
			                                  "aTargets": [ 24], "mDataProp": function ( source, type, val ) {
			                                	  return "";
			                                	 
			                              }}  ,
			                              {    "sTitle":'Lansky',
			                                  "aTargets": [ 25], "mDataProp": function ( source, type, val ) {
			                                	  return "";
			                                	 
			                              }} ,
			                              {    "sTitle":'Inter-Disciplinary Meeting Date',
			                                  "aTargets": [ 26], "mDataProp": function ( source, type, val ) {
			                                	  return "";
			                                	 
			                              }}  
			                         
			                        ];
   
	var recipientListTable_aoColumn = [ { "bSortable": false},
	                               	  null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               null,
						               { "bVisible": false},
						               { "bVisible": false},
						               { "bVisible": false},
						               { "bVisible": false},
						               { "bVisible": false},
						               { "bVisible": false},
						               { "bVisible": false},
						               { "bVisible": false}
			             ];
	//?BB var column_sort = [[ 1, "asc" ]];
	var column_sort = [];
	//var reagentsTable_MergeCols = mergedCols;
	var recipientListTable_ColManager = function(){
										$("#recipientTrackerColumn").html($('.ColVis:eq(0)'));
									 };
	var sDomVal = 'C<"clear">Rlfrtip';
									//constructTable(flag,'recipientTracker',donorListTable_ColManager,donorListTable_serverParam,donorListTable_aoColumnDef,donorListTable_aoColumn);
									// constructTableWithMerge(flag,"recipientTracker",recipientListTable_serverParam,recipientListTable_aoColumnDef,recipientListTable_aoColumn,column_sort,reagentsTable_MergeCols,recipientListTable_ColManager,sDomVal);
									
									 constructTable(flag,"recipientTracker",recipientListTable_ColManager,recipientListTable_serverParam,recipientListTable_aoColumnDef,recipientListTable_aoColumn);
}

function openReferal(id){
	var form="";
//	var url = "loadRecipientDetails.action?recipient="+id;
var url = "loadWorkflow.action?recipientD.pkRecipient="+id
	var result = ajaxCall(url,form);
	$("#main").html(result);
}



$(document).ready(function(){
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"}); 
	  $("select").uniform(); 
      $(window).resize();
      hide_slidewidgets();
      hide_slidecontrol();      
      constructRecipientTrackerList(false);
      /*var oTable=$("#recipientTracker").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											             ],
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#recipientTrackerColumn").html($('.ColVis:eq(0)'));
												}
		});*/
	/*$('#recipientTracker tbody[role="alert"] tr').bind('click',function(){
		var form="";
		var url = "loadReferralPage";
		var result = ajaxCall(url,form);
		$("#main").html(result);
		}); */
	/*	oTable=$("#recipientInsuranceDocuments").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										             ],
	 										"oColVis": {
	 											"aiExclude": [ 0 ],
	 											"buttonText": "&nbsp;",
	 											"bRestore": true,
	 											"sAlign": "left"
	 										},
	 										"fnInitComplete": function () {
	 									        $("#recipientInsuranceDocumentsColumn").html($('.ColVis:eq(1)'));
	 										}
									});
	 oTable=$("#patientBenefitsDocuments").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
	 										               null,
	 										               null,
	 										               null,
	 										               null,
	 										             ],
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#patientBenefitsDocumentsColumn").html($('.ColVis:eq(2)'));
												}
									});*/
	 
    $("select").uniform(); 
});

</script>

<div class="cleaner"></div>
<form>

<!--right window start -->	
<div  id="slidedivs"  style="display:none">
<div id="uldiv">
<ul class="ulstyle">
<li class="listyle print_label"><img src = "images/icons/calendar.png"  style="width:30px;height:30px;"/></li>
<li class="listyle print_label" ><b>Calendar</b> </li>
<li class="listyle print_label"><img src = "images/icons/plan.png"  style="width:30px;height:30px;"/></li>
<li class="listyle notes_icon"><b>Plan</b> </li>
<li class="listyle print_label"><img src = "images/icons/Notification.png"  style="width:30px;height:30px;"/></li>
<li class="listyle print_label" ><b>Notification</b> </li>
</ul>
</div>
</div>
<div class="cleaner"></div>
		<div class="column">		
			<div  class="portlet">
				  <div class="portlet-header addnew">Recipient Tracker</div>
						<div class="portlet-content">
						
						<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  style="width:16;height:16;cursor:pointer;" onclick="add_Recipient('recipientTracker');"/>&nbsp;&nbsp;<label class="cursor"  onclick="add_Recipient('recipientTracker');;"><b>Add New Recipient</b></label>&nbsp;</div></td>
					<!-- td style="width:20%"><div><img src = "images/icons/no.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="delete_Recipient('recipientTracker');"/>&nbsp;&nbsp;<label class="cursor" onclick="delete_Donor('donordetailsTable');"><b>Delete</b></label>&nbsp;</div></td>
				    <td style="width:20%"><div><img src = "images/icons/edit.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="edit_Recipient('recipientTracker');"/>&nbsp;&nbsp;<label class="cursor" onclick="edit_Donor('donordetailsTable');"><b>Edit</b></label>&nbsp;</div></td>
				    <td style="width:20%"><div><img src = "images/icons/save3.png" class="cursor" style="width:16;height:16;" class="cursor" onclick="save_Recipient('recipientTracker');" >&nbsp;&nbsp;<label class="cursor" onclick="save_Donor('donordetailsTable')"><b>Save</b></label></div></td-->
				</tr>
			</table>
						
						<table cellpadding="0" cellspacing="0" border="0" id="recipientTracker" class="display">
						<thead>
							<tr>
								<th width="4%" id="recipientTrackerColumn"></th>
								<th colspan='1'>RecipientId</th>
								<th colspan='1'>Last Name</th>
								<th colspan='1'>First Name</th>
								<th colspan='1'>DOB</th>
								<th colspan='1'>Age</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>
								<th colspan='1'>Diagnosis</th>								
								<!--<th colspan='1'>Type of BMT</th>
								<th colspan='1'>BMT MD</th>
								<th colspan='1'>Plan</th>
								<th colspan='1'>Dept</th>
								<th colspan='1'>Status</th>
							--></tr>
						</thead>
						<tbody>
							<!--<tr>
								<td></td>
								<td><input type='checkbox'  id="recipientInsuranceVerified" onclick=""></td>
								<td>HCA423479</td>
								<td>Branofsky</td>
								<td>Renee</td>
								<td>Jan 19, 1988</td>
								<td>24</td>
								<td>AML</td>
								<td>Allo</td>
								<td>GR</td>
								<td >New Referral. Patient has a matched Sibling. Awaiting approval for Consult.</td>
								<td>Adults</td>
								<td>Pre-Transplant</td>
							</tr>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="recipientInsuranceVerified" onclick=""></td>
								<td>HCA877665</td>
								<td>Solikov</td>
								<td>Dori</td>
								<td>Aug 19, 1963</td>
								<td>49</td>
								<td>MM</td>
								<td>Auto</td>
								<td>TS</td>
								<td>New Referral</td>
								<td>Adults</td>
								<td>Pre-Transplant</td>
							</tr>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="recipientInsuranceVerified" onclick=""></td>
								<td>HCA098968</td>
								<td>Jaurez</td>
								<td>Maria</td>
								<td>Sep 4, 1958</td>
								<td>54</td>
								<td>MDS</td>
								<td>Allo</td>
								<td>TS</td>
								<td>Patient was med management 11/12. Sister did not match. Good NMDP Search.</td>
								<td>Adults</td>
								<td>Med Management</td>
							</tr>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="recipientInsuranceVerified" onclick=""></td>
								<td>HCA786555</td>
								<td>Geromi</td>
								<td>Nina</td>
								<td>Dec 23, 1995</td>
								<td>17</td>
								<td>AML</td>
								<td>Allo</td>
								<td>DC</td>
								<td>Discussing Immunotheraphy instead of/prior to allo transplant</td>
								<td>Peds</td>
								<td>Sent out for more treatment</td>
							</tr><tr>
								<td></td>
								<td><input type='checkbox'  id="recipientInsuranceVerified" onclick=""></td>
								<td>HCA897667</td>
								<td>Trumbold</td>
								<td>Robert</td>
								<td>Jan 14, 1949</td>
								<td>64</td>
								<td>MDS</td>
								<td>Allo</td>
								<td>IF</td>
								<td>On track. Scheduled for Prep on 01/05</td>
								<td>Adults</td>
								<td>Transplant</td>
							</tr>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="recipientInsuranceVerified" onclick=""></td>
								<td>HCA087765</td>
								<td>Ole</td>
								<td>Tyler</td>
								<td>Jun 12, 1999</td>
								<td>13</td>
								<td>ALL</td>
								<td>Allo</td>
								<td>FD</td>
								<td>GVHD followup</td>
								<td>Peds</td>
								<td>Follow-Up</td>
							</tr>
						--></tbody>
						</table>
			</div>
		</div>
</div>
<div class="cleaner"></div>
<div id="addRecipient" style="display:none">
	<div id="addRecipientconttent" style="background:white;height:150px">
             	<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%"  id="addNewRecipient">
				<tr>
					<td class="tablewapper">First Name</td>
					<td class="tablewapper1"><input type="text" name="" id=""/></td>
				<tr>
					<td class="tablewapper">Last Name</td>
					<td class="tablewapper1"><input type="text" name="" id=""/></td>
				</tr>
				<tr>
					<td class="tablewapper">SSN</td>
					<td class="tablewapper1"><input type="text" name="" id=""/></td>
				</tr>
			</table>
	</div>
	<br><br>
	<div align="right">
		<input type="button" value="save"/>
	</div>
</div>
<div class="cleaner"></div>
	
			

<%-- <div class="floatright_right">
	<jsp:include page="sidecontrols.jsp"></jsp:include>
 </div> --%>
 
<div class="cleaner"></div>
		
<!-- div align="right" style="float:right;">

		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
			<tr>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;&nbsp;</td>
				<td><input type="button" value="Next" onclick="saveOvernightStorage()"/></td>
			</tr>
		</table>

</div-->
</form>


<!-- <script type="text/javascript">



var now = new dateEntry();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivaldateEntry').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new dateEntry()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};
$("#timeZone").html(timezones[-offset / 60]);

</script>
 -->













