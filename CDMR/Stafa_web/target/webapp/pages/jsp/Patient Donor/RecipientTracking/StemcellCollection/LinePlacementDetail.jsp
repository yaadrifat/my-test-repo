<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page import="com.velos.stafa.util.VelosStafaConstants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
       	<tr>
       		<td class="tablewapper"><s:text name = "stafa.label.centralLinePlaceQues"/></td>
       		<td class="tablewapper1">
       		    <s:radio name="centralLinePlaceQues" value ="%{linePlacementData.isCentralPlaced}" id="centralLinePlaceQuesId" 					  
					list="#{'true':'Yes','false':'NO'}" ></s:radio>
       	</tr>
		<tr>
       		<td class="tablewapper"><s:text name = "stafa.label.centralLinePlaceSite"/></td>
       		<td class="tablewapper1"><s:select cssStyle = "width:auto" id="centralSiteSelectId" value = "%{linePlacementData.fkCentralPlaceSite}"  name="centralLineSiteName" list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@CENTRAL_LINE_PLACEMENT]" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
								
			<td class="tablewapper"><s:text name = "stafa.label.specifyOther"/></td>
       		<td class="tablewapper1"><s:textfield name="" value="%{linePlacementData.centralPlaceSiteOther}" id="centralLineSiteOtherId"/></td>
       	</tr>
       	<tr>
       		<td class="tablewapper"><s:text name = "stafa.label.linePlacementLoc"/></td>
       		<td class="tablewapper1"><s:select cssStyle = "width:auto" id="lineLocSelectId" value = "%{linePlacementData.fkLinePlaceLocation}"  name="" list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@LINE_PLACEMENT_LOCATION]" listKey="pkCodelst" listValue="description"  headerKey="0" headerValue="Select" /></td>
								
			<td class="tablewapper" style="width:15%"><s:text name = "stafa.label.specifyOther"/></td>
       		<td class="tablewapper1" style="width:27%"><s:textfield name="" value="%{linePlacementData.linePlaceLocationOther}" id="lineLocOtherId"/></td>
       	<tr>
	
		<!-- <td style="width:20%"><div><img src = "images/icons/save3.png" class="cursor" style="width:16;height:16;" class="cursor" onclick="saveLinePlacementDetail();" >&nbsp;&nbsp;<label class="cursor" onclick="saveLinePlacementDetail()"><b>Save</b></label></div></td> --> 
</table>