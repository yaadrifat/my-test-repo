
<%-- <%@ include file="common/includes.jsp" %> --%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script>


$(document).ready(function(){
    portletMinus();
 	$("#subDIvTitle").html("Next Step");

});
$(function() {
    $('.jclock').jclock();
    $("select").uniform();

  });



</script>
<form>
<div class="column">

    <div  class="portlet">
    <div class="portlet-header">Next Step</div>
         <div  class="portlet">
         <s:hidden id=""/>
        <div class="portlet-header"><s:text name= "stafa.label.recipientDisclaimers"/></div>
    		<jsp:include page="../../../disclaimer.jsp">
						<jsp:param value="referralDisclaimer" name="disclaimerType"/>
						</jsp:include>		
    </div>

 
    </div>
    
    <div  class="portlet">
        <div class="portlet-header"><s:text name= "stafa.label.recipientStatus"/></div>
        <div class="portlet-content">
        
        <jsp:include page="../../../recipient_status.jsp"></jsp:include>
       		    
        </div>
    </div>

</div>
<div class="cleaner"></div>
        <div  style="float:right;">

            <input type="button" value="Save"/>
      
        </div>
          </form>
<div class="cleaner"></div>