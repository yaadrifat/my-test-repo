<%--  <%@ include file="jsp/common/includes.jsp" %>  --%>
<!-- <script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script>
<link type="text/css" href="css/floatmenu.css" rel="stylesheet" /> -->
<%@ taglib prefix="s" uri="/struts-tags"%>
<style>
.textboxsizing{
width:42% !important;
}

</style>
<script>

$(function() {
    $('.jclock').jclock();
    $("select").uniform();

  });
$(document).ready(function(){
	portletMinus();
	$("#subDIvTitle").html("Pre-Transplant Visit-Recipient Donor Suitablity");
    $("#recipentbrowse").live("click",function(){
        $("#recipentattach").trigger('click');
    });
    $("#recipientHLAbrowse").live("click",function(){
        $("#recipientHLAattach").trigger('click');
    });
    $("#recipientHLAbrowse1").live("click",function(){
        $("#recipientHLAattach1").trigger('click');
    });
    $("#donorHLAbrowse").live("click",function(){
        $("#donorHLAattach").trigger('click');
    });
    $("#donorHLAbrowse1").live("click",function(){
        $("#donorHLAattach1").trigger('click');
    });
    $("#donorHLAbrowse2").live("click",function(){
        $("#donorHLAattach2").trigger('click');
    });
    $("#donareligibilitybrowse").live("click",function(){
        $("#donareligibilityattach").trigger('click');
    });
   
    $("select").uniform();
});

/*
function commonattach(attid){
    $(".attach").hide();
    var id1=0;
    $(".attach").after("<a href='#' id='"+attid+"' style=''>Browse</a>");                
        $("#"+attid).click(function() {           
        $(".attach").click();
        $("#maxSize").after("<div id='"+id1+"'><span id='attachedFile'></span><a href='#' id='remove' name='"+id1+"'>Remove</a></div>"); 
           id1=id1+1;
    $("#remove").hide();
        $('.attach').change(function(){
               $("#"+attid).attr("enabled","enabled");         
              $("#attachedFile").html($(this).val());
        $("#remove").show();
        });       
        $("#remove").click(function(){
            var id;
            id=$(this).attr("name");  
              $("div[id='"+id+"']").remove();
        });
        });
   
   
}  */





</script>
<div class="cleaner"></div>
<!--right window start --> 
<form>
<div class="cleaner"></div>
<div class="column">     
    <div  class="portlet">
        <div class="portlet-header">Recipient - Donor Suitability Documents</div>
        <div class="portlet-content">
            <table>
                <tr>
                    <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick=""/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                    <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                </tr>
            </table>
            <table width="100%">
            <tr>
              <td width="90%">
                    <table cellpadding="0" cellspacing="0" border="1" id="donorSuitTable" class="display" >
                        <thead>
                            <tr>
                                <th>Select</th>
                                <th>Document Name</th>
                                <th>Date</th>
                                <th>Attachment</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type='checkbox' id='' name=''/></td>
                                <td>Allogeneic Donor/Recipient Transplant Checklist (Peds)</td>
                                <td>Dec 10,2012</td>
                                <td><img src="images/icons/Black_Paperclip.jpg"></td>
                               </tr>
                        </tbody>
                    </table>
               </td>
               <td >
                <input id="recipentattach" type="file"  class="attach" onclick="" style="display:none;"/><a id="recipentbrowse" href="#">Browse</a>
               </td>
             </tr>      
            </table>
        </div>
    </div>
</div>

<div class="cleaner"></div>

<div class="column">     
    <div  class="portlet">
        <div class="portlet-header">Recipient - Donor Suitability</div>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td style="padding:2% 0% 1% 15%"><b>Recipient</b></td>
                    <td style="padding:2% 0% 1% 15%"><b>Donor</b></td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                       <td class="tablewapper">Recipient ID</td>
                    <td class="tablewapper1"><input type="text"/></td>
                    <td class="tablewapper">Donor ID</td>
                    <td class="tablewapper1"><input type="text"/></td>
                </tr>
                <tr>
                       <td class="tablewapper">ABO/Rh</td>
                    <td class="tablewapper1"><input type="text"/></td>
                    <td class="tablewapper">ABO/Rh</td>
                    <td class="tablewapper1"><input type="text"/></td>
                </tr>
                <tr>
                       <td class="tablewapper">CMV</td>
                    <td class="tablewapper1"><input type="text"/></td>
                    <td class="tablewapper">CMV</td>
                    <td class="tablewapper1"><input type="text"/></td>
                </tr>
                <tr>
                       <td class="tablewapper">Weight</td>
                    <td class="tablewapper1">
                        <table>
                            <tr>
                                <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick=""/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                                <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                               </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" id="indsideWeight_left" class="display">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Weight(kg)</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type='checkbox' id='' name=''/></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td class="tablewapper">Weight</td>
                    <td class="tablewapper1">
                        <table>
                            <tr>
                                <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick=""/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                                <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                               </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" id="indsideWeight_right" class="display">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Weight(kg)</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type='checkbox' id='' name=''/></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
 
<div class="cleaner"></div>

<div class="column">
    <div  class="portlet">
        <div class="portlet-header">Recipient HLA</div>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="" class="display" align="" border="0" width="100%">
                <thead>
                <tr>
                <th colspan=3>Recipient Initial HLA Typing</th>
                <th colspan=2>Recipient Confirmatory HLA Typing</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Resolution</td>
                    <td colspan="2"><div style="padding-left:13%;"><select><option>select</option><option>High Resolution</option></select></div></td>
                    <td colspan="2"><div style="padding-left:35%;"><select><option>select</option><option>High Resolution</option></select></div></td>
                </tr>
                <tr>
                    <td>HLA-A</td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                </tr>
                 <tr>
                    <td>HLA-B</td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                </tr>
                  <tr>
                    <td>HLA-C</td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                </tr>
                 <tr>
                    <td>HLA-DR</td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                </tr>
                 <tr>
                    <td>HLA-DQ</td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                </tr>
                  <tr>
                    <td>Date Tested</td>
                    <td colspan="2"><div style="padding-left:13%;"><input type="text" id="" class="dateEntry"/></div></td>
                    <td colspan="2"><div style="padding-left:35%;"><input type="text" id="" class="dateEntry"/></div></td>
                </tr>
                 <tr>
                    <td>Test Location</td>
                    <td colspan="2"><div style="padding-left:13%;"><select><option>select</option><option>High Resolution</option></select></div></td>
                    <td colspan="2"><div style="padding-left:35%;"><select><option>select</option><option>High Resolution</option></select></div></td>
                </tr>
                 <tr>
                    <td>Attachment</td>
                    <td colspan="2"><div style="padding-left:13%;"><input id="recipientHLAattach" class="attach" type="file" onclick=""  style="display:none;"/><a id="recipientHLAbrowse" href="#"> Browse</a> </div></td>
                    <td colspan="2"><div style="padding-left:35%;"><input id="recipientHLAattach1" class="attach" type="file"  onclick="" style="display:none;"/><a id="recipientHLAbrowse1" href="#">Browse</a> </div></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="column">
    <div  class="portlet">
        <div class="portlet-header">Donor HLA</div>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="" class="display" align="" border="0" width="100%">
                <thead>
                <tr>
                <th></th>
                <th colspan=2>Donor Initial HLA Typing 1</th>
                <th colspan=2>Donor Initial HLA Typing 3</th>
                <th colspan=2>Donor Confirmatory HLA Typing</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Resolution</td>
                    <td colspan="2"><div style="padding-left:13%"><select><option>select</option><option>High Resolution</option></select></div></td>
                    <td colspan="2"><div style="padding-left:24%"><select><option>select</option><option>High Resolution</option></select></div></td>
                    <td colspan="2"><div style="padding-left:22%"><select><option>select</option><option>High Resolution</option></select></div></td>
                </tr>
                <tr>
                    <td>HLA-A</td>
                    <td colspan="2"><input type="text" id="" class="textboxsizing"/>&nbsp;<input type="text" id="" class="textboxsizing"/></td>
                    <td colspan="2"><input type="text" id="" class="textboxsizing"/>&nbsp;<input type="text" id="" class="textboxsizing" /></td>
                    <td colspan="2"><input type="text" id="" class="textboxsizing"/>&nbsp;<input type="text" id="" class="textboxsizing" /></td>
                </tr>
                 <tr>
                    <td>HLA-B</td>
                    <td colspan="2"><input type="text" id="" class="textboxsizing"/>&nbsp;<input type="text" id="" class="textboxsizing"/></td>
                    <td colspan="2"><input type="text" id="" class="textboxsizing"/>&nbsp;<input type="text" id="" class="textboxsizing"/></td>
                    <td colspan="2"><input type="text" id="" class="textboxsizing"/>&nbsp;<input type="text" id="" class="textboxsizing"/></td>
                </tr>
                  <tr>
                    <td>HLA-C</td>
                      <td colspan="2"><input type="text" id="" class="textboxsizing"/>&nbsp;<input type="text" id="" class="textboxsizing"/></td>
                    <td colspan="2"><input type="text" id="" class="textboxsizing"/>&nbsp;<input type="text" id="" class="textboxsizing"/></td>
                    <td colspan="2"><input type="text" id="" class="textboxsizing"/>&nbsp;<input type="text" id="" class="textboxsizing"/></td>
                </tr>
                 <tr>
                    <td>HLA-DR</td>
                     <td colspan="2"><input type="text" id="" class="textboxsizing"/>&nbsp;<input type="text" id="" class="textboxsizing"/></td>
                    <td colspan="2"><input type="text" id="" class="textboxsizing"/>&nbsp;<input type="text" id="" class="textboxsizing"/></td>
                    <td colspan="2"><input type="text" id="" class="textboxsizing"/>&nbsp;<input type="text" id="" class="textboxsizing"/></td>
                </tr>
                 <tr>
                    <td>HLA-DQ</td>
                    <td colspan="2"><input type="text" id="" class="textboxsizing"/>&nbsp;<input type="text" id="" class="textboxsizing"/></td>
                    <td colspan="2"><input type="text" id="" class="textboxsizing"/>&nbsp;<input type="text" id="" class="textboxsizing"/></td>
                    <td colspan="2"><input type="text" id="" class="textboxsizing"/>&nbsp;<input type="text" id="" class="textboxsizing"/></td>
                </tr>
                <tr>
                    <td>Date Tested</td>
                    <td colspan="2"><div style="padding-left:13%;"><input type="text" id="" class="dateEntry"/></div></td>
                    <td colspan="2"><div style="padding-left:24%;"><input type="text" id="" class="dateEntry"/></div></td>
                    <td colspan="2"><div style="padding-left:22%;"><input type="text" id="" class="dateEntry"/></div></td>
                </tr>
                 <tr>
                    <td>Test Location</td>
                    <td colspan="2"><div style="padding-left:13%;"><select><option>select</option><option>High Resolution</option></select></div></td>
                    <td colspan="2"><div style="padding-left:24%;"><select><option>select</option><option>High Resolution</option></select></div></td>
                    <td colspan="2"><div style="padding-left:22%;"><select><option>select</option><option>High Resolution</option></select></div></td>
                </tr>
                 <tr>
                    <td>Attachment</td>
                    <td colspan="2"><div style="padding-left:13%;"><input id="donorHLAattach" class="attach" type="file" onclick=""  style="display:none"/><a id="donorHLAbrowse" href="#">Browse</a> </div></td>
                    <td colspan="2"><div style="padding-left:24%;"><input id="donorHLAattach1" class="attach" type="file"  onclick="" style="display:none"/><a id="donorHLAbrowse1" href="#">Browse</a> </div></td>
                    <td colspan="2"><div style="padding-left:22%;"><input id="donorHLAattach2" class="attach" type="file" onclick=""  style="display:none"/><a id="donorHLAbrowse2" href="#"> Browse</a> </div></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="cleaner"></div>

<div class="column">
    <div  class="portlet">
        <div class="portlet-header">HLA Antigen Mismatch</div>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">HLA Antigen Mismatch</td>
                    <td class="tablewapper"><s:select headerKey="" headerValue="Select" list="#{'1':'10/10','2':'9/10','3':'8/10','4':'7/10','5':'Cord-6/6','6':'Cord-5/6','7':'Cord-4/6','8':'Haplo-5/10'}" name="" value="" /></td>
                </tr>
            </table>
        </div>
    </div>
</div>
             
<div class="column">
    <div  class="portlet">
    <div class="portlet-header"></div>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td style="padding:2% 0% 1% 35%"><b>Pre-Collection</b></td>
                    <td style="padding:2% 0% 1% 32%"><b>Pre-Prep Transplant Regiment</b></td>
                </tr>
                <tr>
                    <td style="padding:0% 0% 1% 36%"><b>Acceptable</b></td>
                    <td style="padding:0% 0% 1% 37%"><b>Acceptable</b></td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                       <td class="tablewapper">History and Physical</td>
                    <td class="tablewapper1"><s:radio name="" id="" list="#{'1':'Yes','0':'No','2':'N/A'}" value="" /></td>
                    <td class="tablewapper1"><s:radio name="" id="" list="#{'1':'Yes','0':'No','2':'N/A'}" value="" /></td>
                </tr>
                <tr>
                       <td class="tablewapper">General Anesthesia Risk</td>
                    <td class="tablewapper1"><s:radio name="" id="" list="#{'1':'Yes','0':'No','2':'N/A'}" value="" /></td>
                    <td class="tablewapper1"><s:radio name="" id="" list="#{'1':'Yes','0':'No','2':'N/A'}" value="" /></td>
                </tr>
                <tr>
                       <td class="tablewapper">HLA(Initial and Confirmatory)</td>
                    <td class="tablewapper1"><s:radio name="" id="" list="#{'1':'Yes','0':'No','2':'N/A'}" value="" /></td>
                    <td class="tablewapper1"><s:radio name="" id="" list="#{'1':'Yes','0':'No','2':'N/A'}" value="" /></td>
                </tr>
                <tr>
                       <td class="tablewapper">Infectious Disease Testing</td>
                    <td class="tablewapper1"><s:radio name="" id="" list="#{'1':'Yes','0':'No','2':'N/A'}" value="" /></td>
                    <td class="tablewapper1"><s:radio name="" id="" list="#{'1':'Yes','0':'No','2':'N/A'}" value="" /></td>
                </tr>
                <tr>
                       <td class="tablewapper">Other Lab Test</td>
                    <td class="tablewapper1"><s:radio name="" id="" list="#{'1':'Yes','0':'No','2':'N/A'}" value="" /></td>
                    <td class="tablewapper1"><s:radio name="" id="" list="#{'1':'Yes','0':'No','2':'N/A'}" value="" /></td>
                </tr>
                <tr>
                       <td class="tablewapper">Stem Cell Infusion Orders Complete</td>
                    <td class="tablewapper1"><s:radio name="" id="" list="#{'1':'Yes','0':'No','2':'N/A'}" value="" /></td>
                    <td class="tablewapper1"><s:radio name="" id="" list="#{'1':'Yes','0':'No','2':'N/A'}" value="" /></td>
                </tr>
                <tr>
                       <td class="tablewapper">Donor Cleared</td>
                    <td class="tablewapper1"><s:radio name="" id="" list="#{'1':'Yes','0':'No','2':'N/A'}" value="" /></td>
                    <td class="tablewapper1"><s:radio name="" id="" list="#{'1':'Yes','0':'No','2':'N/A'}" value="" /></td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Pre-Collection Comments</td>
                    <td class="tablewapper1">Pre-Prep Comments</td>
                </tr>
                <tr>
                    <td class="tablewapper"><textarea rows="4" cols="40"></textarea></td>
                    <td class="tablewapper1"><textarea rows="4" cols="40"></textarea></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="cleaner"></div>

<div class="column">
    <div  class="portlet">
        <div class="portlet-header">Donor Eligibility</div>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Donor Eligible for Collection?</td>
                    <td class="tablewapper"><s:radio name="" id="" list="#{'1':'Yes','0':'No'}" value="" /></td>
                </tr>
            </table>
            <table width="100%">
            <tr>
            <td width="90%">
            <table cellpadding="0" cellspacing="0" border="1" id="indsideWeight_left" class="display" >
                <thead>
                    <tr>
                        <th></th>
                        <th>Document Name</th>
                        <th>Date Complete</th>
                        <th>Attachment</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input type='checkbox' id='' name=''/></td>
                        <td>Donor Card</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            </td>
            <td>
             <input id="donareligibilityattach" class="attach" type="file" onclick="" style="display:none" /><a id="donareligibilitybrowse" href="#"> Browse</a>
            </td>
            </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                <tr>
                    <td class="tablewapper">Ineligible due to:</td>
                </tr>
                <tr>
                    <td class="tablewapper" style="padding-left:10%"><input type="radio" id='' name=''/></td>
                    <td class="tablewapper1">Infectious Disease Testing</td>
                    <td class="tablewapper1" style="padding-left:10%"><input type="radio" id='' name=''/></td>
                    <td class="tablewapper1">Donor Screening Results</td>
                </tr>
                <tr>
                    <td class="tablewapper" style="padding-left:10%"><input type="checkbox" id='' name=''/></td>
                    <td class="tablewapper1" style="width:28%">Some or all of the infectious disease testing were not performed in a CLIA lab and/or the appropriate FDA approved test kits were not used</td>
                    <td class="tablewapper1" style="padding-left:10%"><input type="checkbox" id='' name=''/></td>
                    <td class="tablewapper1">Donor health history screening indicate the presence of risk factors of relevant communicable diseases.</td>
                </tr>
                 <tr>
                    <td class="tablewapper" style="padding-left:10%"><input type="checkbox" id='' name=''/></td>
                    <td class="tablewapper1">Some or all of the infectious disease testing were not performed within the required time frame</td>
                    <td class="tablewapper1" style="padding-left:10%"><input type="checkbox" id='' name=''/></td>
                    <td class="tablewapper1">Donor physical assessment indicates physical evidence of risk factors of relevant communicable diseases.</td>
                </tr>
                <tr>
                    <td class="tablewapper" style="padding-left:10%"><input type="checkbox" id='' name=''/></td>
                    <td class="tablewapper1">Some or all of the infectious disease testing indicated positive test results</td>
                    <td class="tablewapper1" style="padding-left:10%"><input type="checkbox" id='' name=''/></td>
                    <td class="tablewapper1">Donor relevant medical records indicate risk factor for relevant communicable diseases.</td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                 <tr>
                    <td><input type="checkbox" id='' name=''/></td>
                    <td>Due to the urgent medical need for transplantation it is  acceptible to transplant cells from the above done. An Exceptional Use Consent must be signed by donor and recipient proir to transplant.
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                    <tr>
                        <td class="tablewapper">Transplant Physician Name:</td>
                        <td class="tablewapper1"><input type='text' id='' name=''/></td>
                    </tr>
                    <tr>
                        <td class="tablewapper">Sign Off:</td>
                        <td class="tablewapper1"><input type='text' id='' name=''/></td>
                        <td class="tablewapper">Date:</td>
                        <td class="tablewapper1"><input type='text' id='' name=''/></td>
                    </tr>
            </table>
        </div>
    </div>
</div>

<div class="cleaner"></div>

<div class="column">
    <div  class="portlet">
        <div class="portlet-header">Physician Sign Off</div>
        <div class="portlet-content">
            <div style="border:solid 1px #000;border-radius:2px;">
                <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                    <tr>
                        <td style="padding:1% 0% 5% 8%">I have Reviewed thes evaluations and approve the collection of Hematopoetic Stem Cells from the above named donor for the use by the above named recipient.</td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                    <tr>
                        <td class="tablewapper">Transplant Physician Signature:</td>
                        <td class="tablewapper1"><input type='text' id='' name=''/></td>
                        <td class="tablewapper">Date:</td>
                        <td class="tablewapper1"><input type='text' id='' name=''/></td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                     <tr>
                        <td class="tablewapper">Transplant Physician Name:</td>
                        <td style="padding:2%"><textarea rows="1" cols="60"></textarea></td>
                    </tr>
                </table>
            </div>
            <div style="border:solid 1px #000;border-radius:2px;">
                <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                    <tr>
                        <td style="padding:1% 0% 5% 8%">I have Reviewed thes evaluations and approve the start of the transplant for the above named recipient.</td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                    <tr>
                        <td class="tablewapper">Transplant Physician Signature:</td>
                        <td class="tablewapper1"><input type='text' id='' name=''/></td>
                        <td class="tablewapper">Date:</td>
                        <td class="tablewapper1"><input type='text' id='' name=''/></td>
                    </tr>
                </table>
                <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
                     <tr>
                        <td class="tablewapper">Transplant Physician Name:</td>
                        <td style="padding:2%"><textarea rows="1" cols="60"></textarea></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
 
<div class="cleaner"></div>
     
<div align="right" style="float:right;">
            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
            <tr>
                <td><div class="mainselection"><select name="" id="" ><option value="">Select</option><option>Preparation</option><option>Verification</option><option>Collection</option><option>Post-Collection</option></select></div></td>
                <td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;&nbsp;</td>
                <td><input type="button" value="save" onclick=""/></td>
            </tr>
        </table>

</div>
    </form>
    <div class="cleaner"></div>
    