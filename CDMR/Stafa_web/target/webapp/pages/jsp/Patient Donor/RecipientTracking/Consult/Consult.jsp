<%-- <%@ include file="common/includes.jsp" %> --%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/workFlow.js"></script>
<link rel="stylesheet" type="text/css" href="css/ui/base/jquery.ui.tabs.css" media="screen" />
<style>
/* .workFlowContainer{
background-color:	#FFFFFF;;
border-radius:5px;
}
.subLinkInsurance{
border:1px solid white;
font-weight:bold;
width:60%;
text-align:center;
line-height:1.3 em;
padding:2%;
background-color:#DADADA;
}
.subMenuWrapper{
background-color:	#B5B5B5;
border-radius:5px;
}
.subDivPortlet{
background-color:	#DADADA;
border-radius:5px;
}
#box-outer {
	width:18%;
}
#subDivLeftWrapper{
border-radius:5px;
}
.subDivLeftContent{
border:1px solid black;
font-weight:bold;
text-align:center;
line-height:1.3 em;
padding:2%;
background-color:#FFFFFF;
border-radius:5px;
} */
#subDIvTitle{
height:30px;
font-weight:bold;
padding-top:6px;
padding-left:2px;
font-size:1.2em;
text-decoration:underline;
}
</style>

<script>
function calculateHeight(){
   var  mainheight=$("#innercenter").height();
    var titlediv=$("#workflowcontainerID").height();
	// var contentdivheight=$("#subdiv").height();
    contentheight=(mainheight-titlediv)/16;
    //alert("contentheight"+contentheight);
    $("#subdiv").css("height",contentheight +"em");
}
/* function showConsult_VisitPage(){
	checkHighlight();
	$("#consultVisit").addClass("selectedDiv");
	$("#subDIvTitle").html("Consult/Eval - Visit");
	var form1="";
	var url = "loadConsultVisitPage";
	var result = ajaxCall(url,form1);
	alert(result);
	$("#forfloat_right").html(result);
	portletMinus();
}
function showConsult_LabPage(){
	checkHighlight();
	$("#consultLab").addClass("selectedDiv");
	$("#subDIvTitle").html("Consult/Eval - Labs/Tests");
	var form="";
	var url = "loadConsult_LabPage";
	var result = ajaxCall(url,form);
	$("#forfloat_right").html(result);
	portletMinus();
}
function showConsult_PhysicianPage(){
	checkHighlight();
	$("#consultPhysician").addClass("selectedDiv");
	$("#subDIvTitle").html("Consult/Eval - Plan");
	var form="";
	var url = "loadConsult_PhysicianPage";
	var result = ajaxCall(url,form);
	$("#forfloat_right").html(result);
	portletMinus();
}
function showConsult_InsurancePage(){
	checkHighlight();
	$("#consultInsurance").addClass("selectedDiv");
	$("#subDIvTitle").html("Consult/Eval - Insurance Approvals");
	var form="";
	var url = "loadConsult_InsurancePage";
	var result = ajaxCall(url,form);
	$("#forfloat_right").html(result);
	portletMinus();
}
function showConsult_NextStepPage(){
	checkHighlight();
	$("#consultNextStep").addClass("selectedDiv");
	$("#subDIvTitle").html("Consult/Eval - Next Step");
	var form="";
	var url = "loadConsult_NextStepPage";
	var result = ajaxCall(url,form);
	$("#forfloat_right").html(result);
	portletMinus();
} */
/* function show_eastslidewidget(divid){
    // to Hide the left side widget
	innerLayout.hide('west');
    innerLayout.show('south',true);
    innerLayout.show('north',true);
    innerLayout.show('east',false);
    var src = $("#"+divid).html();
    $("#innereast").html(src);
    $("#"+divid).html("slidedivs");
       }  */
       var contentheight;
$(function() {
    $('.jclock').jclock();
    
	$( "#tabNew" ).tabs();

  });
$(document).ready(function(){	
	portletMinus();
//	calculateHeight();
    //$(window).resize(calculateHeight)
    //$("select").uniform(); 
    //showConsult_VisitPage();
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"});
	show_eastslidewidget("slidedivs");					    
	$("select").uniform(); 
});
//loadConsultVisitPage?personId=<s:property value="personId"/>

var ConsultEval={
		
		visit:function(divId){
				var url = 'loadConsultVisitPage.action';
				//LoadedPage.url=url;
				//LoadedPage.div='consultEval';
				//LoadedPage.visitId = '<s:property value="#request.visitId"/>';
				//url +='?personId=<s:property value="personId"/>';
				//this.loader(url, divId);
				openUrl(url,'','<s:property value="#request.visitId"/>',divId);
				
		},labs:function(divId){
			var url = 'loadLabs.action';
			openUrl(url,'','<s:property value="#request.visitId"/>',divId);
			
	   },
		financial:function(divId){
			var url = 'loadFinancialAppoval.action';
			//LoadedPage.url=url;
			//LoadedPage.div='consultEval';
			//LoadedPage.visitId = '<s:property value="#request.visitId"/>';
			//url +='?personId=<s:property value="personId"/>';
			openUrl(url,'','<s:property value="#request.visitId"/>',divId);
			
	  },next:function(divId){
		  
		  	var discCode ='consultEvalDiscliamer';
		  	
			var url = 'loadConsult_NextStepPage.action?transplantId='+'<s:property value="#request.transplantId"/>'+'&disc_code='+discCode;
			openUrl(url,'','<s:property value="#request.visitId"/>',divId);
			
	    },
		loader:function(url, divId){			
			$("#"+divId).html(ajaxCall(url,''));
		}
		
		
};

ConsultEval.visit('consultEval');
</script>
<s:hidden id="pkTxWorkflow_id" name="transplantId"/>
<s:hidden id="recipientId" name="recipient"/>
<s:hidden id="personId" name="personId"/>
		<div class="cleaner"></div>
			<!--right window start -->	
				<div  id="slidedivs"  style="display:none">
					<div id="uldiv">
						<ul class="ulstyle">
							<li class="listyle print_label"><img src = "images/icons/calendar.png"  style="width:30px;height:30px;"/></li>
							<li class="listyle print_label" ><b>Calendar</b> </li>
							<li class="listyle print_label"><img src = "images/icons/plan.png"  style="width:30px;height:30px;"/></li>
							<li class="listyle notes_icon"><b>Plan</b> </li>
							<li class="listyle print_label"><img src = "images/icons/Notification.png"  style="width:30px;height:30px;"/></li>
							<li class="listyle print_label" ><b>Notification</b> </li>
						</ul>
					</div>
				</div>
		<form>
				<div class="cleaner"></div>
					<div id="subdiv">
			<div id="tabNew">			
							    <ul>
							      <li><a href="#consultEval" onclick="ConsultEval.visit('consultEval');">Visit</a></li>
							      <li><a href="#consultEval" onclick="ConsultEval.labs('consultEval')">Labs/Tests</a></li>			   				      
			   				      <li><a href="#consultEval" onclick="ConsultEval.financial('consultEval');">Financial Approvals</a></li>
			   				      <li><a href="#consultEval" onclick="ConsultEval.next('consultEval');">Next Step</a></li>
							    </ul>
							<div id="consultEval">				 
				 			</div>
				</div>
				
				 
		<!-- <div id="floatSubDivMenu">
		<div id="forfloat_wind">
			<div id="box-outer">
				
		 <a href="#" style="text-decoration:none" id="blind">&lt;&lt; </a> 
					<div id="box">
						<div id="leftnav" >
						
			recipient start
						
							<div  class="portlet">
							<div class="portlet-header"></div>
								<div class="portlet-content">
								<div id="subDivLeftWrapper" >
						<table cellpadding="0" cellspacing="0" border="0" id="" class="display">
								<tr>
									<td ><div class="subDivLeftContent" onclick="showConsult_VisitPage()" id="consultVisit"'>Visit</div></td></tr>
									<tr><td><div class="subDivLeftContent" onclick="showConsult_LabPage()" id="consultLab">Labs/Tests</div></td></tr>
									<tr><td><div class="subDivLeftContent" onclick="showConsult_PhysicianPage()" id="consultPhysician">Physician's Plan</div></td></tr>
									<tr><td><div class="subDivLeftContent" onclick="showConsult_InsurancePage()" id="consultInsurance">Insurance Approvals</div></td></tr>
									<tr><td ><div class="subDivLeftContent" onclick="showConsult_NextStepPage()" id="consultNextStep">Next Step</div></td>
								</tr>
					</table>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
				</div>
		</div> -->
		
		
		<%-- <div class="column">       
		    <div  class="portlet">
		    <div class="portlet-header">Registration Details</div><br>
		        <div class="portlet-content">
		            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
		                <tr>
		                    <td class="tablewapper">Recipient Weight(Kg)</td>
							<td>
								<div  class="portlet11">
									<div class="portlet-content">
									<table>
									<tr>
										<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
										<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
								    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
									</tr>
								</table>
								<table cellpadding="0" cellspacing="0" border="1" id="referralDetails" class="display">
								<thead>
									<tr>
										<th colspan='1'>Select</th>
										<th colspan='1'>Weight</th>
										<th colspan='1'>unit</th>
										<th colspan='1'>Date</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><input type='checkbox'  id="recipientInsuranceVerified" onclick=""></td>
										<td></td>
										<td></td>
										<td>	</td>
										</tr>
								</tbody>
								</table>
					</div>
				</div>
							</td>
		                </tr>
		                </table>
		                 <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
		                <tr>
		                    <td class="tablewapper">Recipient Height(cm)</td>
							<td class="tablewapper1"><input type="text" id="recipientHeight" name=""/></td>
		                    <td class="tablewapper">ABO/Rh</td>
							<td class="tablewapper1"><select id="ABO/Rh"><option>Select</option></select></td>
		                </tr>
		                 <tr>
		                    <td class="tablewapper">Recipient Height(cm)</td>
							<td colspan=3><textarea cols="80" rows="4" id="rxBenefitPlan"></textarea></td>
		                 </tr>
		                </table>
		        </div>
		    </div>
		</div>	 --%>
		<!-- <div class="column">
			<div  class="portlet">
						<div class="portlet-header ">Consult/Eval Checklist</div>
							<div class="portlet-content">
								<table>
									<tr>
										<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
										<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
								    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
									</tr>
								</table>
								<table cellpadding="0" cellspacing="0" border="0" id="consultEvalChecklistDetails" class="display">
								<thead>
									<tr>
										<th width="4%" id="consultEvalChecklistColumn"></th>
										<th colspan='1'>Select</th>
										<th colspan='1'>Items</th>
										<th colspan='1'>Date</th>
										<th colspan='1'>Document Name</th>
										<th colspan='1'>Attachment</th>
										<th colspan='1'>Status</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<td><input type='checkbox'  id="consultEvalChecklistVerified" onclick=""></td>
										<td>Eval / Assesment By MD</td>
										<td></td>
										<td>	</td>
										<td><input type="button" id="" value="Browse" /></td>
										<td></td>
								</tr>
								</tbody>
								</table>
					</div>
				</div>
			</div>	 -->
			
			<%-- <div class="column">
			<div  class="portlet">
						<div class="portlet-header ">Consult/Eval Education</div>
							<div class="portlet-content">
								<table>
									<tr>
										<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewMachine();"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
										<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
								    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
									</tr>
								</table>
								<table cellpadding="0" cellspacing="0" border="0" id="consultEvalEducationDetails" class="display">
								<thead>
									<tr>
										<th width="4%" id="consultEvalEducationColumn"></th>
										<th colspan='1'>Select</th>
										<th colspan='1'>Items</th>
										<th colspan='1'>Date</th>
										<th colspan='1'>Status</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<td><input type='checkbox'  id="consultEvalDocumentVerified" onclick=""></td>
										<td>Patient info Packet</td>
										<td></td>
										<td>	<select id="ABO/Rh"><option>Select</option></select></td>
									</tr>
								</tbody>
								</table>
					</div>
				</div>
			</div>	 --%>
		
		</div>
		</form>
		
		<!-- <script type="text/javascript">
		
		
		
		var now = new dateEntry();
		
		var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;
		
		var lastLog = "" + now.format("h:MM:ss ");
		
		var userDetails = lastLog;
		
		
		$('#arrivaldateEntry').html(lastLog);
		
		
		
		/**************For Time Zone Start****************************/
		 var offset = (new dateEntry()).getTimezoneOffset();
		
		 var timezones = {
		 '-12': 'PST',
		 '-11': 'PST',
		 '-10': 'PST',
		 '-9': 'CST',
		 '-8': 'CST',
		 '-7': 'CST',
		 '-6': 'CST',
		 '-5': 'CST',
		 '-4': 'CST',
		 '-3.5': 'America/St_Johns',
		 '-3': 'America/Argentina/Buenos_Aires',
		 '-2': 'Atlantic/Azores',
		 '-1': 'Atlantic/Azores',
		 '0': 'Europe/London',
		 '1': 'Europe/Paris',
		 '2': 'Europe/Helsinki',
		 '3': 'Europe/Moscow',
		 '3.5': 'Asia/Tehran',
		 '4': 'Asia/Baku',
		 '4.5': 'Asia/Kabul',
		 '5': 'Asia/Karachi',
		 '5.5': 'IST',
		 '6': 'Asia/Colombo',
		 '7': 'Asia/Bangkok',
		 '8': 'Asia/Singapore',
		 '9': 'Asia/Tokyo',
		 '9.5': 'Australia/Darwin',
		 '10': 'PST',
		 '11': 'Asia/Magadan',
		 '12': 'Asia/Kamchatka'
		};
		$("#timeZone").html(timezones[-offset / 60]);
		
		</script>
		 -->













