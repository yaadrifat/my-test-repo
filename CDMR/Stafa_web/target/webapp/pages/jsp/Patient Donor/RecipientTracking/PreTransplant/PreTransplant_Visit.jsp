<%-- <%@ include file="common/includes.jsp" %> --%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script>
$(function() {
    $('.jclock').jclock();
  });
$(document).ready(function(){
	portletMinus();
	$("#subDIvTitle").html("Pre-Transplant Visit - Visit");
    //$("select").uniform(); 
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"});
      $(window).resize();
	//show_eastslidewidget("slidedivs");
		 	var  oTable=$("#EducationChecklist").dataTable({
						"sPaginationType": "full_numbers",
						"sDom": 'C<"clear">Rlfrtip',
						"aoColumns": [ { "bSortable":false},
							               null,
							               null,
							               null
							               ],
							"oColVis": {
								"aiExclude": [ 0 ],
								"buttonText": "&nbsp;",
								"bRestore": true,
								"sAlign": "left"
							},
							"fnInitComplete": function () {
						        $("#EducationChecklistColumn").html($('#EducationChecklist_wrapper .ColVis'));
							}
				}); 
});
$("select").uniform(); 
</script>
 <div class="cleaner"></div>
<form>
<div class="column">
<div class="cleaner"></div>
    <div  class="portlet">
    <div class="portlet-header">Basic Clinical Info</div>
        <div class="portlet-content">
       
             <table cellpadding="0" cellspacing="8" class="" align="center" border="0" width="85%">
                <tr>
                    <td  width="15%" class="" rowspan="3">Recipient Weight</td>
                    <td>
                     <div align="left"><img src = "images/icons/addnew.jpg" class="cursor"  onclick=""/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;&nbsp;&nbsp;
                       <img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label></div>
                    </td>
                    <td></td>
                    <td></td>
                <tr>   
                    <tr>
                  
                    <td class="" colspan="3">
                        <table id="" cellpadding="0" cellspacing="0" class="display" align="center" border="1" width="80%">
                            <thead>
                            <tr>
                                <th>Select</th>
                                <th>Weight</th>
                                <th>Unit</th>
                                <th>Date</th>
                            </tr>   
                            </thead>
                           
                            <tbody align="center">
                            <tr>
                                <td> <input type="checkbox" id="" ></td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td> <input type="checkbox" id="" ></td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            <tr>
                                <td> <input type="checkbox" id="" ></td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                            </tbody>
                       
                        </table>

                    </td>
                     <td></td>
                   </tr>
                   <tr>
                   <td>Recipient Height(cm)</td>
                   <td><input type="text"></td>
                   <td>Recipient Height(cm)</td>
                   <td><input type="text"></td>
                 
                   </tr>
                <tr>
                <td>Allergies</td>
                <td colspan="3"><textarea rows="3" cols="70"></textarea></td>
                </tr>
                  
                              
            </table>
        </div>
    </div>
</div>
<div class="cleaner"></div>
<div class="column">
	<div  class="portlet">
				<div class="portlet-header ">Pre-Transplant Visit Checklist</div><br>
					<div class="portlet-content">
						<table cellpadding="0" cellspacing="0" border="1" id="preTransplantVisitChecklist" class="display">
						<thead>
							<tr>
								<th width="4%" id="preTransplantVisitChecklistColumn"></th>
								<th colspan='1'>Select</th>
								<th colspan='1'>Items</th>
								<th colspan='1'>Date</th>
								<th colspan='1'>Document Name</th>
								<th colspan='1'>Attachment</th>
								<th colspan='1'>Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="consultEvalChecklistVerified" onclick=""></td>
								<td width="20%">Schedule Donor Consent Appointment</td>
								<td></td>
								<td>	</td>
								<td></td>
								<td></td>
						</tr>
						<tr>
								<td></td>
								<td><input type='checkbox'  id="consultEvalChecklistVerified" onclick=""></td>
								<td>Schedule Transplant Date</td>
								<td></td>
								<td>	</td>
								<td></td>
								<td></td>
						</tr>
						<tr>
								<td></td>
								<td><input type='checkbox'  id="consultEvalChecklistVerified" onclick=""></td>
								<td>Schedule Donor Collection - OR or Apheresis</td>
								<td></td>
								<td>	</td>
								<td></td>
								<td></td>
						</tr>
						<tr>
								<td></td>
								<td><input type='checkbox'  id="consultEvalChecklistVerified" onclick=""></td>
								<td>If BM Collection - Schedule Anesthesia</td>
								<td></td>
								<td>	</td>
								<td></td>
								<td></td>
						</tr>
						<tr>
								<td></td>
								<td><input type='checkbox'  id="consultEvalChecklistVerified" onclick=""></td>
								<td>Schedule CVL Placement (prior to TBI if applicable)</td>
								<td></td>
								<td>	</td>
								<td></td>
								<td></td>
						</tr>
						<tr>
								<td></td>
								<td><input type='checkbox'  id="consultEvalChecklistVerified" onclick=""></td>
								<td>If patient  will not be admitted after line Placement, have case manager arrange home care /line Supplies</td>
								<td></td>
								<td>	</td>
								<td></td>
								<td></td>
						</tr>
						<tr>
								<td></td>
								<td><input type='checkbox'  id="consultEvalChecklistVerified" onclick=""></td>
								<td>Notify PBMT Unit of Admission and Transplant Date</td>
								<td></td>
								<td>	</td>
								<td></td>
								<td></td>
						</tr>
						<tr>
								<td></td>
								<td><input type='checkbox'  id="consultEvalChecklistVerified" onclick=""></td>
								<td>Verify Patient has loding and travel arrangements</td>
								<td></td>
								<td>	</td>
								<td></td>
								<td></td>
						</tr>
						<tr>
								<td></td>
								<td><input type='checkbox'  id="consultEvalChecklistVerified" onclick=""></td>
								<td>Verify patient has a companion/Caregiver- Complete care Contract</td>
								<td></td>
								<td>	</td>
								<td></td>
								<td></td>
						</tr>
						<tr>
								<td></td>
								<td><input type='checkbox'  id="consultEvalChecklistVerified" onclick=""></td>
								<td>FC/CM notify insurance company of admission and transplant date</td>
								<td></td>
								<td>	</td>
								<td></td>
								<td></td>
						</tr>
						<tr>
								<td></td>
								<td><input type='checkbox'  id="consultEvalChecklistVerified" onclick=""></td>
								<td>Complete Collection Orders</td>
								<td></td>
								<td>	</td>
								<td></td>
								<td></td>
						</tr>
						<tr>
								<td></td>
								<td><input type='checkbox'  id="consultEvalChecklistVerified" onclick=""></td>
								<td>	Complete infusion Orders</td>
								<td></td>
								<td>	</td>
								<td></td>
								<td></td>
						</tr>
						<tr>
								<td></td>
								<td><input type='checkbox'  id="consultEvalChecklistVerified" onclick=""></td>
								<td>	Notify Referring Hospital/MD of Plan of Care for transplant</td>
								<td></td>
								<td>	</td>
								<td></td>
								<td></td>
						</tr>
						</tbody>
						</table>
			</div>
		</div>
	</div>	 
	<div class="cleaner"></div>
	
<div class="column">
       
    <div  class="portlet">
    <div class="portlet-header notes">Pre-Transplant Visit Education Checklist</div><br>
        <div class="portlet-content">
        <div style="">   
       
        </div>
            <table>
                <tr>
                    <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewDisposable(false)"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                    <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                    <td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" id="EducationChecklist" class="display">
                <thead>
                    <tr>
                        <th width="4%" id="EducationChecklistColumn">Select</th>
                        <th colspan="1">Item</th>
                        <th colspan="1">Date</th>
                        <th colspan="1">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <td><input type="checkbox" id="" class=""></td>
                    <td>Apheresis Teaching</td>
                    <td> <input type="text" id="" class=""></td>
                    <td><Select><option>Complete</option><option>N/A</option></Select> </td>
                     
                    </tr>
                    <tr>
                    <td><input type="checkbox" id="" class=""></td>
                    <td>Apheresis Tour</td>
                    <td> <input type="text" id="" class=""></td>
                    <td><Select><option>Complete</option><option>N/A</option></Select> </td>
                     
                    </tr>
                    <tr>
                    <td><input type="checkbox" id="" class=""></td>
                    <td>Line Teaching(peds)</td>
                    <td> <input type="text" id="" class=""></td>
                    <td><Select><option>Complete</option><option>N/A</option></Select> </td>
                     
                    </tr>
                    <tr>
                    <td><input type="checkbox" id="" class=""></td>
                    <td>BMT Education Binder</td>
                    <td> <input type="text" id="" class=""></td>
                    <td><Select><option>Complete</option><option>N/A</option></Select> </td>
                     
                    </tr>
                    <tr>
                    <td><input type="checkbox" id="" class=""></td>
                    <td>Chemo Education Sheets</td>
                    <td> <input type="text" id="" class=""></td>
                    <td><Select><option>Complete</option><option>N/A</option></Select> </td>
                     
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>	
<div class="cleaner"></div>
<div class="column">
    <div  class="portlet">
    <div class="portlet-header notes">TBI Checklist</div><br>
        <div class="portlet-content">
                <div id="button_wrapper">TBI Patient?&nbsp;&nbsp;<input type="radio" name="" id="">&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="patients" id="">&nbsp;&nbsp;No</div><br>
       
            <table>
                <tr>
                    <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewDisposable(false)"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                    <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                    <td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" id="tbichecklist" class="display">
                <thead>
                    <tr>
                        <th width="4%" id="">Select</th>
                        <th colspan="1">Task/Event</th>
                        <th colspan="1">Date</th>
                        <th colspan="1">Attachment</th>
                        <th colspan="1">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <td><input type="checkbox" id="" class=""></td>
                    <td> Have TBI Order from MD</td>
                    <td> <input type="text" id="" class="dateEntry"></td>
                    <td> </td>
                    <td></td>
                     
                    </tr>
                        <tr>
                    <td><input type="checkbox" id="" class=""></td>
                    <td> Schedule Radiation Consult/Simulation</td>
                    <td> <input type="text" id="" class="dateEntry"></td>
                    <td> </td>
                    <td></td>
                     
                    </tr>
                        <tr>
                    <td><input type="checkbox" id="" class=""></td>
                    <td> Schedule Radiation Dates-Notify if Patients Needs Sedation</td>
                    <td> <input type="text" id="" class="dateEntry"></td>
                    <td> </td>
                    <td></td>
                     
                    </tr>
                    <tr>
                    <td><input type="checkbox" id="" class=""></td>
                    <td>Set up Clinic Visit day prior to starting TBI appointment</td>
                    <td> <input type="text" id="" class="dateEntry"></td>
                    <td> </td>
                    <td></td>
                     
                    </tr>
                    <tr>
                    <td><input type="checkbox" id="" class=""></td>
                    <td> Arrage transportation to/from TBI if needed </td>
                    <td> <input type="text" id="" class="dateEntry"></td>
                    <td> </td>
                    <td></td>
                     
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="cleaner"></div>
<div class="column">
    <div  class="portlet">
    <div class="portlet-header notes">Consents</div><br>
        <div class="portlet-content">
            <table>
                <tr>
                    <td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addnewDisposable(false)"/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
                    <td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
                    <td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" id="consentsTable" class="display">
                <thead>
                    <tr>
                        <th id="consentsTableColumn">Select</th>
                        <th>Consent Type</th>
                        <th>Date</th>
                        <th>Document Name</th>
                        <th>Attachment</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
	                    <td><input type="checkbox" id="" class=""></td>
	                    <td></td>
	                    <td><input type="text" id="" class="dateEntry"></td>
	                    <td> </td>
	                    <td></td>
	                    <td></td>
                    </tr>
                     <tr>
	                    <td><input type="checkbox" id="" class=""></td>
	                    <td></td>
	                    <td><input type="text" id="" class="dateEntry"></td>
	                    <td> </td>
	                    <td></td>
	                     <td></td>
                    </tr>
                        <tr>
	                    <td><input type="checkbox" id="" class=""></td>
	                    <td></td>
	                    <td> <input type="text" id="" class="dateEntry"></td>
	                    <td> </td>
	                    <td></td>
	                     <td></td>
                    </tr>
                    <tr>
                    <td><input type="checkbox" id="" class=""></td>
                    <td></td>
                    <td> <input type="text" id="" class="dateEntry"></td>
                    <td> </td>
                    <td></td>
                    <td></td>
                    </tr>
                    <tr>
                    <td><input type="checkbox" id="" class=""></td>
                    <td>  </td>
                    <td> <input type="text" id="" class="dateEntry"></td>
                    <td> </td>
                    <td></td>
                     <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="cleaner"></div>
  </form>