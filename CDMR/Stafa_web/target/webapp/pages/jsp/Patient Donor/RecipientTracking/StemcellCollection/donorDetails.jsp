<%@ taglib prefix="s" uri="/struts-tags"%>
<form>
		<jsp:include page="../../../basic_clinicalInfo.jsp"></jsp:include>

		<div class="cleaner"></div>


	<s:set var="id" value="%{'referralDocuments'}" />   
	<s:set var="column" value="%{'false'}" />   
	<div class="column">
		<div  class="portlet">
					<div class="portlet-header "><s:text name = "stafa.label.recipientConsents"/></div>		
					<span class="hidden">
					  <s:select id="%{#attr['id']+'List'}" name="recipientConsents1" list="recipientConsents"  headerKey="" headerValue="Select"/>
					</span>
					<jsp:include page="../../../doc_activity.jsp"></jsp:include>					
			</div>
		</div>
<s:set var="preCollection" value="%{'true'}" />
	<jsp:include page="../../../visit_checklist.jsp"/>
</form>	
<script type="text/javascript">

		//To int the portlet border
		$('#stemCellCollection div.column')
		.find('div.portlet')
		.attr({	class:'portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'});


	var ReciepDocParam ={
	
			 widgetType : <s:property value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@TYPE_REFERRAL, @com.velos.stafa.util.VelosStafaConstants@SUBTYPE_REFERRALDOC)"/>,
			 entityId 	: '<s:property value="recipient"/>',
			 entityType : '<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_RECIPIENT"/>',
			 txId 		: '<s:property value="transplantId"/>',
			 visitId 	: '<s:property value="visitId"/>'
			
	};
	
	var ReciepCurrentDoc={
	
		     TABLE: '<s:property value="id"/>CurrTable',
			 TABLECol : '<s:property value="id"/>CurrColumn',
			 TABLEIdPrefix : '<s:property value="id"/>'
	
	};
	
	var ReciepHistDoc={
	
		     TABLE: '<s:property value="id"/>HistTable',
			 TABLECol : '<s:property value="id"/>HistColumn',
			 TABLEIdPrefix : '<s:property value="id"/>'
	};

	 
	function constructDocTable(table){
		 if(table==ReciepCurrentDoc.TABLE){
			
			DocTable.currentTableExHead(false, ReciepDocParam.entityId, ReciepDocParam.entityType, ReciepDocParam.txId, ReciepDocParam.visitId, table, docTableHeaders, ReciepCurrentDoc.TABLECol, ReciepCurrentDoc.TABLEIdPrefix, ReciepDocParam.widgetType);
			
		}
	}
	
	$('#<s:property value="id"/>Tabs' ).tabs();
	DocTable.currentTableExHead(false, ReciepDocParam.entityId, ReciepDocParam.entityType, ReciepDocParam.txId, ReciepDocParam.visitId, ReciepCurrentDoc.TABLE, docTableHeaders, ReciepCurrentDoc.TABLECol, ReciepCurrentDoc.TABLEIdPrefix, ReciepDocParam.widgetType);
	DocTable.historyTableExHead(false, ReciepDocParam.entityId, ReciepDocParam.entityType, ReciepDocParam.txId, ReciepDocParam.visitId, ReciepHistDoc.TABLE, docTableHeaders, ReciepHistDoc.TABLECol, ReciepHistDoc.TABLEIdPrefix, ReciepDocParam.widgetType);
	

	var paramObj ={
			entityid:'<s:property value="personId"/>',
			entityType:'<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_PERSON"/>',
			txid:'<s:property value="transplantId"/>',	
			visitId:'<s:property value="visitId"/>'
	};

	   
   function savePages(mode){
		
		if(checkModuleRights(STAFA_RECIPIENTTRACKERS, APPMODE_SAVE)){
			try{
				if(saveCheckList()){
				}
				if(save_ClinicalInfo(paramObj)){
				}
				if(Documents.saveDocs(ReciepDocParam, ReciepCurrentDoc)){
				}	
			
				jAlert("  Data Saved Successfully !", "Information ...", function () { 
					openUrl(LoadedPage.url,LoadedPage.workflow,LoadedPage.visitId,LoadedPage.div);
					
	            }); 
			}catch(e){
				alert("exception " + e);
			}
		}
	}
</script>

		