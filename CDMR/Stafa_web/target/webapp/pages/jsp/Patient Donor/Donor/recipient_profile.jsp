
<%-- <%@ include file="jsp/common/includes.jsp"%>  --%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />
<script type="text/javascript" src="js/dataSearch.js"></script>
<link rel="stylesheet" href="css/cssmenuhorizontal.css" type="text/css" />

<style>
#insuranceStatusTable
{
	 width:100% !important;
}
#insuranceApprovalTable
{
	 width:100% !important;
}

</style>

<script language="javascript">

function saveAccountStatus()
{
	var personId = '<s:property value="personId"/>';
	var pkRecipient='<s:property value ="recipient"/>';
	var transplantId ='<s:property value ="transplantId"/>';
	var entityType ="recipient";


var transplantData ="{pkTxWorkflow:"+transplantId+",";
transplantData += "entityId:'"+pkRecipient+ "',";
transplantData += "accountStatusComment:'"+$("#accountStatusComment").val()+ "',";
transplantData += "entityType:"+entityType+",fkAccountStatus:'"+$("#accountStatus").val()+"'}";
alert("transplantData"+transplantData);
var url = "saveAccountStatus";

response = jsonDataCall(url,"jsonData={data:["+transplantData+"]}");



return true;  
}

function savePopup(){
	//saveDonor();
	 try{
		//var donorMrnLength=$("#donorMRN").val().length;
	
			 if($("#accountStatusComment").val()!="") 
				 {
				if(saveUpdateAccountStatus()){
					return true;
					}
				 }
			 else
				 {
				 alert("Please enter Reason for Update");
				 return false;
				 }
			
	}catch(e){
		alert("exception " + e);
	} 
}
function saveUpdateAccountStatus()
{
	var personId = '<s:property value="personId"/>';
	var pkRecipient='<s:property value ="recipient"/>';
	var transplantId ='<s:property value ="transplantId"/>';
	var entityType ="recipient";


var transplantData ="{pkTxWorkflow:"+transplantId+",";
transplantData += "entityId:'"+pkRecipient+ "',";
transplantData += "accountStatusComment:'"+$("#accountStatusComment").val()+ "',";
transplantData += "entityType:"+entityType+",fkAccountStatus:'"+$("#accountStatus1").val()+"'}";

var url = "saveAccountStatus";

response = jsonDataCall(url,"jsonData={data:["+transplantData+"]}");
if(response.status !=""){
	alert("Data Saved Successfully");
	closePopup();
	constructAccountSummaryTable(true);	
	loaddiv('refreshAccountStatus?accountStatus='+$("#accountStatus1").val(),'accountStatusdDiv');
	return true;
	}

}

var seqNo = 0;
function removeProduct(){
   var deleteIds = [];
   $("#insuranceStatusTable #productId:checked").each(function(i){
        deleteIds.push($(this).val());
        deleteProduct($(this).val());
   });
}

function deleteProduct(deleteId){
   var temp=$("#transProducts").val();
   var splitArray = temp.split(",");
   var selectedIndex = splitArray.indexOf(deleteId);
   if(selectedIndex!=-1){
        splitArray.splice($.inArray(deleteId,splitArray),1);
   }
   $("#transProducts").val(splitArray);
   criteriaValue ="'"+splitArray+"'";
   constructTrasportTable(true,'insuranceStatusTable');
   if(criteriaValue == "''"){
	   $("#transProducts").val(",");
   }
}


function loadWorkFlow(){
	var recipientId = <s:property value ="recipient"/>
	var transplantId = <s:property value="#request.transplantId"/>
	var form="";
//	var url = "loadRecipientDetails.action?recipient="+id;
	var url = "loadWorkflowReadOnly.action?recipientD.pkRecipient="+recipientId+"&transplantId="+transplantId
	var result = ajaxCall(url,form);
	$("#main").html(result);
}

function constructAccountSummaryTable(flag){
	var recipientId = '<s:property value="#request.recipient"/>';

    var accDetailsColDef = [                  
                             {	 "aTargets": [0], "mDataProp": function ( source, type, val ) {
                                        return "<input  type='hidden' id='refPkDetailHidddenId' name='' value='"+source[0]+"' >";
                                 }},
                           /*    {"sTitle":'<s:text name ="stafa.label.accountSummary"/>',
                                    "aTargets": [1], "mDataProp": function ( source, type, val ) {
                                        return source[1];	
		                                    
                                 }}, */
                              {    "sTitle":'<s:text name ="stafa.label.dateAccClose"/>',
                                     "aTargets": [1], "mDataProp": function ( source, type, val ) {
                                    	 return source[1];	
                                   }},
							  {    "sTitle":'<s:text name="stafa.label.lastCompStep"/>',
                                     "aTargets": [2], "mDataProp": function ( source, type, val ) {
                                    	 return source[2];
                                   }},
                                   {    "sTitle":'<s:text name="stafa.label.reasonToInactivate"/>',
                                       "aTargets": [3], "mDataProp": function ( source, type, val ) {
                                      	 return source[3];
                                  }} ,
                              {    "sTitle":'<s:text name="stafa.label.transPhy"/>',
                                      "aTargets": [4], "mDataProp": function ( source, type, val ) {
                                     	 return source[4];
                                   }},
                             
                              {    "sTitle":'<s:text name="stafa.label.summary"/>',
                                         "aTargets": [5], "mDataProp": function ( source, type, val ) {
                                        	
                                      		   link ="<button type='button' id='view' onclick='loadWorkFlow()' ><s:text
                       						name='View' ></s:text></button>";  
                                      	    
                                      	   return link;
                                    }} 
                            
                            ];
    var accDetailsColumns = [{"bSortable": false},
                               null,
                               null,
                               null,
                               null,
                               null
                              
                               ];
    var accDetailsColManager = function () {
            $("#accountSummaryMatchColumn").html($('#accountSummaryTable_wrapper .ColVis'));
    };

    var accDetailsServerParams = function ( aoData ) {
                                                    aoData.push( { "name": "application", "value": "stafa"});
                                                    aoData.push( { "name": "module", "value": "account_summary"}); 
                                                    
                                                    aoData.push( { "name": "param_count", "value":"1"});
                                        			aoData.push( { "name": "param_1_name", "value":":ENTITYID:"});
                                        			aoData.push( { "name": "param_1_value", "value":"'"+recipientId+"'"});
                                                  
                                                    aoData.push( {"name": "col_0_name", "value": "lower(pkInsStatus))" } );
                                                    aoData.push( { "name": "col_0_column", "value": "0"});
                                                  
                                                    aoData.push( { "name": "col_1_name", "value": "lower()"});
                                                    aoData.push( { "name": "col_1_column", "value": "1"});
                                                  
                                                    aoData.push( {"name": "col_2_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_2_column", "value": "2"});
                                                    
                                                    aoData.push( {"name": "col_3_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_3_column", "value": "3"});
                                                    
                                                    aoData.push( {"name": "col_4_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_4_column", "value": "4"});
                                                    
                                                  
                                                    
                                                    
                                                                                 
                                                };
    constructTable(flag,"accountSummaryTable",accDetailsColManager,accDetailsServerParams,accDetailsColDef,accDetailsColumns);                                          
}


function constructTransplantHistoryTable(flag){
	var criteria="";           
	criteria = $("#personID").val();
    var transDetailsColDef = [                  
                             {	 "aTargets": [0], "mDataProp": function ( source, type, val ) {
                                        return "<input  type='hidden' id='refPkDetailHidddenId' name='' value='"+source[10]+"' >";
                                 }},
                              {"sTitle":'<s:text name ="stafa.label.transDate"/>',
                                    "aTargets": [1], "mDataProp": function ( source, type, val ) {
                                        return source[1];	
		                                    
                                 }},
                              {    "sTitle":'<s:text name ="stafa.label.diagTime"/>',
                                     "aTargets": [2], "mDataProp": function ( source, type, val ) {
                                    	 return source[2];	
                                   }},
							  {    "sTitle":'<s:text name="stafa.label.productType"/>',
                                     "aTargets": [3], "mDataProp": function ( source, type, val ) {
                                    	 return source[3];
                                   }},
                              {    "sTitle":'<s:text name="stafa.label.transPhy"/>',
                                      "aTargets": [4], "mDataProp": function ( source, type, val ) {
                                     	 return source[4];
                                   }},
                              {    "sTitle":'<s:text name="stafa.label.donorType"/>',
                                       "aTargets": [5], "mDataProp": function ( source, type, val ) {
                                      	 return source[5];
                                  }} ,
                             
                              {    "sTitle":'<s:text name="stafa.label.summary"/>',
                                         "aTargets": [6], "mDataProp": function ( source, type, val ) {
                                        	 return source[6];
                                    }} 
                            ];
    var transDetailsColumns = [{"bSortable": false},
                               null,
                               null,
                               null,
                               null,
                               null,
                              null
                               ];
    var transDetailsColManager = function () {
            $("#transplantHistoryMatchColumn").html($('#transplantHistoryTable_wrapper .ColVis'));
    };

    var transDetailsServerParams = function ( aoData ) {
                                                    aoData.push( { "name": "application", "value": "stafa"});
                                                    aoData.push( { "name": "module", "value": "referral_detail"});  
                                                    aoData.push( { "name": "criteria", "value": criteria});  
                                                    
                                                    /* aoData.push( { "name": "param_count", "value":"1"});
            										aoData.push( { "name": "param_1_name", "value":":SEARCH:"});
            										aoData.push( { "name": "param_1_value", "value":search}); */
                                                  
                                                    aoData.push( {"name": "col_0_name", "value": "lower(pkInsStatus))" } );
                                                    aoData.push( { "name": "col_0_column", "value": "0"});
                                                  
                                                    aoData.push( { "name": "col_1_name", "value": "lower()"});
                                                    aoData.push( { "name": "col_1_column", "value": "1"});
                                                  
                                                    aoData.push( {"name": "col_2_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_2_column", "value": "2"});
                                                    
                                                    aoData.push( {"name": "col_3_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_3_column", "value": "3"});
                                                    
                                                    aoData.push( {"name": "col_4_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_4_column", "value": "4"});
                                                    
                                                    aoData.push( {"name": "col_5_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_5_column", "value": "5"});
                                                    
                                                    aoData.push( {"name": "col_6_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_6_column", "value": "6"});
                                                    
                                                    aoData.push( {"name": "col_7_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_7_column", "value": "6"});
                                                    
                                                    aoData.push( {"name": "col_8_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_8_column", "value": "6"});
                                                    
                                                    aoData.push( {"name": "col_9_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_9_column", "value": "6"});
                                                                                 
                                                };
    constructTable(flag,"transplantHistoryTable",transDetailsColManager,transDetailsServerParams,transDetailsColDef,transDetailsColumns);                                          
}

function constructInsuranceStatusTable(flag){
	var recipient="";           
	recipient = $("#personId").val();
    var insStatusColDef = [                  
                             {	 "aTargets": [0], "mDataProp": function ( source, type, val ) {
                                        return "<input  type='checkbox' id='insStatusID"+source[0] +"' name='insStatusCheck' value='" + source[0] +"' >";
                                 }},
                              {"sTitle":'<s:text name="stafa.label.insurance_Status"/>',
                                    "aTargets": [1], "mDataProp": function ( source, type, val ) {
                                        return source[3];	
		                                    
                                 },
								   "sClass" : "alignCenter"
								 },
                              {    "sTitle":'<s:text name="stafa.label.date"/>',
                                     "aTargets": [2], "mDataProp": function ( source, type, val ) {
                                    	 var startDate = ($.datepicker.formatDate('M dd, yy', new Date(source[2])));
    									    return startDate;
                                  }}
                            
                            ];
    var insStatusColumns = [{"bSortable": false},
                               null,
                               null
                               ];
    var insStatusColManager = function () {
            $("#insuranceStatusColumn").html($('#insuranceStatusTable_wrapper .ColVis'));
    };

    var insStatusServerParams = function ( aoData ) {
    	
			 aoData.push( { "name": "application", "value": "stafa"});
			 aoData.push( { "name": "module", "value": "insurance_status_detail"});  
			 aoData.push( { "name": "criteria", "value": recipient});									
                     
			 aoData.push( { "name": "col_1_name", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_INSURANCESTATUS))"});
			 aoData.push( { "name": "col_1_column", "value": "4"});

			 aoData.push( {"name": "col_2_name", "value": "lower(NVL(TO_CHAR(INSURANCESTATUS_DATE,'Mon dd, yyyy'),''))" });
			 aoData.push( { "name": "col_2_column", "value": "3"});
    
	}
    constructTable(flag,"insuranceStatusTable",insStatusColManager,insStatusServerParams,insStatusColDef,insStatusColumns);                                          
}


function constructInsuranceApprovalTable(flag){
	var criteria="";           
	criteria = $("#personId").val();
    var insApprovalColDef = [
                                 {
                                    "aTargets": [0], "mDataProp": function ( source, type, val ) {
                                    	 return "<input  type='checkbox' id='insApp"+source[0] +"' name='insApprovalCheck' value='" + source[0] +"' >";
                                 }},
                                 {"sTitle":"<s:text name="stafa.label.approval_Name"/>",
                                    "aTargets": [1], "mDataProp": function ( source, type, val ) {
                                        return source[9];
                                 }},
                                 {    "sTitle":"<s:text name="stafa.label.datesubmittedForApproval"/>",
                                     "aTargets": [2], "mDataProp": function ( source, type, val ) {
                                    	 var	submitDate = ($.datepicker.formatDate('M dd, yy', new Date(source[2])));
 									    return submitDate;
                                  }},
                                 {    "sTitle":"<s:text name="stafa.label.approvalStatus"/>",
                                     "aTargets": [3], "mDataProp": function ( source, type, val ) {
                                         return source[10];
                                  }},
                                 {    "sTitle":"<s:text name="stafa.label.i-Plan_Code"/>",
                                     "aTargets": [4], "mDataProp": function ( source, type, val ) {
                                         return source[4];
                                  }},
                                 {    "sTitle":"<s:text name="stafa.label.auth_Date"/>",
                                     "aTargets": [5], "mDataProp": function ( source, type, val ) {
                                    	 var authDate = ($.datepicker.formatDate('M dd, yy', new Date(source[5])));
  									    return authDate;
                                  }},
                                 {    "sTitle":"<s:text name="stafa.label.auth_Code"/>",
                                     "aTargets": [6], "mDataProp": function ( source, type, val ) {
                                         return source[6];
                                  }},
                                 {    "sTitle":"<s:text name="stafa.label.eff_Date"/>",
                                     "aTargets": [7], "mDataProp": function ( source, type, val ) {
                                    	 var effDate = ($.datepicker.formatDate('M dd, yy', new Date(source[7])));
   									    return effDate;
                                  }},
                                 {    "sTitle":"<s:text name="stafa.label.end_Date"/>",
                                     "aTargets": [8], "mDataProp": function ( source, type, val ) {
                                    	 var endDate = ($.datepicker.formatDate('M dd, yy', new Date(source[8])));
   									    return endDate;
                                  }}
                            ];
    var insApprovalColumns = [{"bSortable": false},
					   null,
                       null,
                       null,
					   null,
					   null,
					   null,
					   null,
					   null ];
    
	var insApprovalColManager = function () {
            $("#insuranceApprovalColumn").html($('#insuranceApprovalTable_wrapper .ColVis'));
    };
    var insApprovalServerParams = function ( aoData ) {
        aoData.push( { "name": "application", "value": "stafa"});
        aoData.push( { "name": "module", "value": "insurance_approval_detail"});  
        aoData.push( { "name": "criteria", "value": criteria});                           
      
        aoData.push( {"name": "col_1_name", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE pk_codelst=FK_CODELST_APPROVAL_NAME))" } );
        aoData.push( { "name": "col_1_column", "value": "10"});
      
        aoData.push( { "name": "col_2_name", "value": "lower(NVL(TO_CHAR(APPROVAL_SUBMISSION_DATE,'Mon dd, yyyy'),''))"});
        aoData.push( { "name": "col_2_column", "value": "3"});
      
        aoData.push( {"name": "col_3_name", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE pk_codelst=FK_CODELST_APPROVAL_STATUS))" } );
        aoData.push( { "name": "col_3_column", "value": "11"});
        
        aoData.push( {"name": "col_4_name", "value": "lower(IPLAN_CODE)" } );
        aoData.push( { "name": "col_4_column", "value": "5"});
        
        aoData.push( {"name": "col_5_name", "value": "lower(NVL(TO_CHAR(AUTHORIZATION_DATE,'Mon dd, yyyy'),''))" } );
        aoData.push( { "name": "col_5_column", "value": "6"});
        
        aoData.push( {"name": "col_6_name", "value": "lower(AUTHORIZATION_CODE)" } );
        aoData.push( { "name": "col_6_column", "value": "7"});
        
        aoData.push( {"name": "col_7_name", "value": "lower(NVL(TO_CHAR(EFFECTIVE_DATE,'Mon dd, yyyy'),''))" } );
        aoData.push( { "name": "col_7_column", "value": "8"});
        
        aoData.push( {"name": "col_8_name", "value": "lower(NVL(TO_CHAR(END_DATE,'Mon dd, yyyy'),''))" } );
        aoData.push( { "name": "col_8_column", "value": "9"});

                                     
    };
    constructTable(flag,"insuranceApprovalTable",insApprovalColManager,insApprovalServerParams,insApprovalColDef,insApprovalColumns);                                          
}


$(document).ready(function() {
    //clearTracker();
    //show_innernorth();
    constructTransplantHistoryTable(true);
    constructAccountSummaryTable(true);
    constructInsuranceStatusTable(true);
	constructInsuranceApprovalTable(true);
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"});
    var param ="{module:'APHERESIS',page:'TRANSPORT'}";
	markValidation(param);
	showBelowWidgets();
	
});
/* Scrolling Method Starts*/
$("#insuranceStatusDiv").scroll(function(){
    var newMargin = $(this).scrollLeft();
    $('#insuranceStatusTable_length').css('marginLeft', newMargin);
    $('#insuranceStatusTable_length').css('width', '20%');
    $('#insuranceStatusTable_filter').css('width', '20%');
    $('#insuranceStatusTable_info').css('marginLeft', newMargin);
    $('#insuranceStatusTable_info').css('width', '40%');
});

$("#insuranceApprovalDiv").scroll(function(){
    var newMargin = $(this).scrollLeft();
    $('#insuranceApprovalTable_length').css('marginLeft', newMargin);
    $('#insuranceApprovalTable_length').css('width', '20%');
    $('#insuranceApprovalTable_filter').css('width', '20%');
    $('#insuranceApprovalTable_info').css('marginLeft', newMargin);
    $('#insuranceApprovalTable_info').css('width', '40%');
});
	

function addInsuranceStatus()
{
var htmlString =$("#insuranceStatusSpan").html();
var newRow = "<tr class='odd'>";
seqNo = seqNo+1;
newRow+="<td><input type='checkbox' name='insStatusCheck' Value='0' /></td>";
newRow+="<td align='center'>"+htmlString+"</td>";
newRow+="<td><input type='text' name='selectDate' id='in_date"+seqNo+"' class='startDate dateEntry dateclass'/></td>";
newrow="</tr>";
  $("#insuranceStatusTableTbody").prepend(newRow);
		 $( 'input[name="selectDate"]').datepicker({
                                        dateFormat: 'M dd, yy',
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "c-50:c+45"
                                    });
		$.uniform.restore('select');
		$("select").uniform();
}  


function save_Insurance_Status(){
		
		var row, codelistnsuranceStatus, insuranceDate, insuranceStatusId,entityId,entityType;	
		var rowData = "";
		$("#insuranceStatusTable").find("#insuranceStatusSelect").each(
		 function(row) {
			rowHtml=$(this).closest("tr");
			insuranceStatusId = $(rowHtml).find('input[name="insStatusCheck"]').val();
			codelistnsuranceStatus = $(rowHtml).find("#insuranceStatusSelect").val();
			insuranceDate = $(rowHtml).find('input[name="selectDate"]').val();	
			entityId = $("#personId").val();
			entityType="Person";
			  rowData+= "{insuranceStatusId:'"+insuranceStatusId+"',codelistnsuranceStatus:'"+codelistnsuranceStatus+"',insuranceDate:'"+insuranceDate+"',entityId:'"+entityId+"',entityType:'"+entityType+"'},";
				
		});
		if(rowData.length >0){
	        rowData = rowData.substring(0,(rowData.length-1));
			//alert(rowData);
	 	var url="saveInsuranceStatus";
		   response = jsonDataCall(url,"jsonData={data:["+rowData+"]}");
		    //   alert("Data Saved Successfully");
		    // constructInsuranceStatusTable(true);
		}
		return true;
	}

var deletedflag=false;
function deleteInsuranceStatus(){
   deletedflag=false;
	var deletedIds = "";
		try{
			$("input[name='insStatusCheck']:checked").each(function(){
				row=$(this).closest("tr");
				if($(row).find('input[name="insStatusCheck"]').val() != 0)
			     deletedIds += $(row).find('input[name="insStatusCheck"]').val() + ",";
				 deletedflag=true;
			
		});
		if(deletedIds.length >0){
		  deletedIds = deletedIds.substr(0,deletedIds.length-1);
		}
		if(deletedflag!=false){
			 var yes=confirm(confirm_deleteMsg);
				if(!yes){
					return;
				}
				//alert(deletedIds);
		
		jsonData = "{deletedIds:'"+deletedIds+"'}";
		url="deleteInsuranceStatus";
	   response = jsonDataCall(url,"jsonData="+jsonData);
	   constructInsuranceStatusTable(true);
		}
		if(deletedflag==false){
			alert("Please Select any Record");
		}
		deletedflag=false;
	   
		}catch(error){
			alert("error " + error);
		}

}  

function addInsuranceApproval()
{
var approvalNamehtmlString =$("#approvalNameSpan").html();
var approvalStatushtmlString =$("#approvalStatusSpan").html();
var newRow = "<tr class='odd'>";
newRow+="<td><input type='checkbox' name='insApprovalCheck' Value='0' /></td>";
newRow+="<td align='center'>"+approvalNamehtmlString+"</td>";
newRow+="<td><input type='text' name='insSubDate' id='insAppDateSub' class='startDate dateEntry dateclass'/></td>";
newRow+="<td align='center'>"+approvalStatushtmlString+"</td>";
newRow+="<td><input type='text' name='insAppIPlanCode' id='insAppIPlanCode'/></td>";
newRow+="<td><input type='text' name='insAuthDate' id='insAuthDate' class='startDate dateEntry dateclass'/></td>";
newRow+="<td><input type='text' name='insAuthCode' id='insAuthCode'/></td>";
newRow+="<td><input type='text' name='insEffDate' id='insEffDate' class='startDate dateEntry dateclass'/></td>";
newRow+="<td><input type='text' name='insEndDate' id='insEndDate' class='startDate dateEntry dateclass'/></td>";
newrow="</tr>";
  $("#insuranceApprovalTableTbody").prepend(newRow);
		 $( 'input[name="insSubDate"]').datepicker({
                                        dateFormat: 'M dd, yy',
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "c-50:c+45"
                                    });
		 $( 'input[name="insAuthDate"]').datepicker({
                                        dateFormat: 'M dd, yy',
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "c-50:c+45"
                                    });							
		 $( 'input[name="insEffDate"]').datepicker({
                                        dateFormat: 'M dd, yy',
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "c-50:c+45"
                                    });
		 $( 'input[name="insEndDate"]').datepicker({
                                        dateFormat: 'M dd, yy',
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "c-50:c+45"
                                    });
		$.uniform.restore('select');
		$("select").uniform();
}  
 
 
function save_Insurance_Approval(){
		var row, insuranceApprovalId, entityId, codelistApprovalName, submissionDate, codelistApprovalStatus, iPlanCode, authorizationDate, authorizationCode, effectiveDate, endDate, entityType;	
		var rowData = "";
		 
		$("#insuranceApprovalTable").find("#approvalNameSelect").each(
			function(row) {
			rowHtml=$(this).closest("tr");
			insuranceApprovalId = $(rowHtml).find('input[name="insApprovalCheck"]').val();
			codelistApprovalName = $(rowHtml).find("#approvalNameSelect").val();
			submissionDate = $(rowHtml).find('input[name="insSubDate"]').val();	
			codelistApprovalStatus = $(rowHtml).find("#approvalStatusSelect").val(); 
			iPlanCode =  $(rowHtml).find('input[name="insAppIPlanCode"]').val();	
			authorizationDate = $(rowHtml).find('input[name="insAuthDate"]').val();
			authorizationCode = $(rowHtml).find('input[name="insAuthCode"]').val();
 			effectiveDate = $(rowHtml).find('input[name="insEffDate"]').val();
			endDate = $(rowHtml).find('input[name="insEndDate"]').val();
			entityId = $("#personId").val();
			entityType = "Person";
			  rowData+= "{insuranceApprovalId:'"+insuranceApprovalId+"',codelistApprovalName:'"+codelistApprovalName+"',submissionDate:'"+submissionDate+"',codelistApprovalStatus:'"+codelistApprovalStatus+"',iPlanCode:'"+iPlanCode+"',authorizationDate:'"+authorizationDate+"',authorizationCode:'"+authorizationCode+"',effectiveDate:'"+effectiveDate+"',endDate: '"+endDate+"',entityId:'"+entityId+"',entityType:'"+entityType+"'},";		
		});
		if(rowData.length >0){
	        rowData = rowData.substring(0,(rowData.length-1));
			//alert(rowData);
	 	var url="saveInsuranceApproval";
		  response = jsonDataCall(url,"jsonData={data:["+rowData+"]}");
		      // alert("Data Saved Successfully");
		      // constructInsuranceApprovalTable(true);
		}
		return true;
	}

function deleteInsuranceApproval(){
	   deletedflag=false;
		var deletedIds = "";
			try{
				$("input[name='insApprovalCheck']:checked").each(function(){
					row=$(this).closest("tr");
					if($(row).find('input[name="insApprovalCheck"]').val() != 0)
					{
				     deletedIds += $(row).find('input[name="insApprovalCheck"]').val() + ",";
					 deletedflag=true;
				    }
			});
			if(deletedIds.length >0){
			  deletedIds = deletedIds.substr(0,deletedIds.length-1);
			}
			if(deletedflag!=false){
				 var yes=confirm(confirm_deleteMsg);
					if(!yes){
						return;
					}
					//alert(deletedIds);
			
			jsonData = "{deletedIds:'"+deletedIds+"'}";
			url="deleteInsuranceApproval";
		   response = jsonDataCall(url,"jsonData="+jsonData);
		   constructInsuranceApprovalTable(true);
			}
			if(deletedflag==false){
				alert("Please Select any Record");
			}
			deletedflag=false;
		   
			}catch(error){
				alert("error " + error);
			}

	}

function editInsuranceStatus()
{			
	var editFlag=false;
			$("#insuranceStatusTable tbody tr").each(function (i,row){
				 if($(this).find('input[name="insStatusCheck"]').is(":checked")){
					 $(this).find("td").each(function (col){
						 if(col ==0){
			  	 		  }else if(col ==1 ){			
			  	 			 if ($(this).find("#insuranceStatusSelect").val()== undefined){
								 	var insStatusSpan = $(this).html();
								 	$(this).html($("#insuranceStatusSpan").html());
								 	$(this).find("#insuranceStatusSelect option:contains("+insStatusSpan+")").attr('selected', 'selected');
									$.uniform.restore($(this).find("#insuranceStatusSelect"));
									$(this).find("#insuranceStatusSelect").uniform(); 	
							 }   
						 }else if(col ==2){
							 if ($(this).find(".startDate").val()== undefined){
								 	$(this).html("<input type='text' id='selectDate"+i+"' name='selectDate' class='startDate dateEntry dateclass' value='"+$(this).text()+ "'/>")
								  	$("#selectDate"+i).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
								        changeYear: true, yearRange: "c-50:c+45"});
								 }
						 }
					 });
					 editFlag=true;
				 }
			  });
			if(editFlag==false){
				alert("Please Select any Record");
			}
}

function editInsuranceApproval()
{
	var editFlag=false;
	$("#insuranceApprovalTable tbody tr").each(function (i,row){
		 if($(this).find('input[name="insApprovalCheck"]').is(":checked")){
			 $(this).find("td").each(function (col){
				 if(col ==0){
					
	  	 		  }else if(col ==1 ){			
	  	 			 if ($(this).find("#approvalNameSelect").val()== undefined){
						 	var insApprovalCurrentVal = $(this).html();
							
						 	$(this).html($("#approvalNameSpan").html());
						 	$(this).find("#approvalNameSelect option:contains("+insApprovalCurrentVal+")").attr('selected', 'selected');
							$.uniform.restore($(this).find("#approvalNameSelect"));
							$(this).find("#approvalNameSelect").uniform(); 	
					 }   
				 }else if(col ==2){
					 if ($(this).find(".startDate").val()== undefined){
						 	$(this).html("<input type='text' id='insSubDate"+i+"' name='insSubDate' class='startDate dateEntry dateclass' value='"+$(this).text()+ "'/>")
						  	$("#insSubDate"+i).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
						        changeYear: true, yearRange: "c-50:c+45"});
						 }
				 }
				 else if(col ==3){
					 if ($(this).find("#approvalStatusSelect").val()== undefined){
						 	var insApprovalStausVal = $(this).html();
						 	$(this).html($("#approvalStatusSpan").html());
						 	$(this).find("#approvalStatusSelect option:contains("+insApprovalStausVal+")").attr('selected', 'selected');
							$.uniform.restore($(this).find("#approvalStatusSelect"));
							$(this).find("#approvalStatusSelect").uniform(); 	
					 }
				 }           
				 else if(col ==4){
					 if ($(this).find('input[name="insAppIPlanCode"]').val()== undefined){
					 	$(this).html("<input type='text' name='insAppIPlanCode' id='' value='"+$(this).text()+ "'/>");
					 }
				 }
				 
				 else if(col ==5){
					 if ($(this).find('input[name="insAuthDate"]').val()== undefined){
						 	$(this).html("<input type='text' id='insAuthDate"+i+"' name='insAuthDate' class='startDate dateEntry dateclass' value='"+$(this).text()+ "'/>")
						  	$("#insAuthDate"+i).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
						        changeYear: true, yearRange: "c-50:c+45"});
						 }
				 } 
				 else if(col ==6){
					 if ($(this).find('input[name="insAuthCode"]').val()== undefined){
					 	$(this).html("<input type='text' name='insAuthCode' id='' value='"+$(this).text()+ "'/>");
					 }
				 }
				 else if(col ==7){
					 if ($(this).find('input[name="insEffDate"]').val()== undefined){
						 	$(this).html("<input type='text' id='insEffDate"+i+"' name='insEffDate' class='startDate dateEntry dateclass' value='"+$(this).text()+ "'/>")
						  	$("#insEffDate"+i).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
						        changeYear: true, yearRange: "c-50:c+45"});
						 }
				 }
				 
				 else if(col ==8){
					 if ($(this).find('input[name="insEndDate"]').val()== undefined){
						 	$(this).html("<input type='text' id='insEndDate"+i+"' name='insEndDate' class='startDate dateEntry dateclass' value='"+$(this).text()+ "'/>")
						  	$("#insEndDate"+i).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
						        changeYear: true, yearRange: "c-50:c+45"});
						 }
				 }				 
			 });
			 editFlag=true;
		 }
	  });
	if(editFlag==false){
		alert("Please Select any Record");
	}
}

 


function savePages(mode){


if(checkModuleRights(STAFA_RECIPIENTTRACKERS, APPMODE_SAVE)){
		
		
		try{
				/*Save Documents*/
				if(Documents.saveDocs(ReferralInsApprovDocParam, ReferralRecipConsDocParam, ReferralInsApprovDocCurrentDoc, ReferralRecipConsCurrentDoc)){								
				}
				if(save_Insurance_Status()){
					//alert("save_Insurance_Status()");
				}
				
				if(save_Insurance_Approval()){
					//alert("save_Insurance_Approval()");
				}
				if(saveAccountStatus())
					{
					
					}
				jAlert("  Data Saved Successfully !", "Information ...", function () { 
					     openUrl(LoadedPage.url,LoadedPage.workflow,LoadedPage.visitId,LoadedPage.div);
                }); 
                return true;			
	
			}catch(e){
			alert("exception " + e);
		}
	}
	
}
</script>

<div class="cleaner"></div>
<s:hidden id="sampleLocationVal" value="%{lstInsuranceStatus}" />
<s:hidden id="recipientId_Id" name="recipient"/>
<input name="personId" id="personId" type ="hidden" value="<s:property value="personId"/>"/>
<input name="transplantId" id="transplantId" type ="hidden" value="<s:property value="transplantId"/>"/>
<span id="hiddendropdowns" class="hidden">
 <span id="insuranceStatusSpan" style="none">
 <s:select id="insuranceStatusSelect"  name="insuranceStatus" list="lstInsuranceStatus" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" />
</span>

<span id="approvalNameSpan">
 <s:select id="approvalNameSelect"  name="approvalName" list="lstApprovalName" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" />
</span>

<span id="approvalStatusSpan">
 <s:select id="approvalStatusSelect"  name="approvalStatus" list="lstApprovalStatus" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" />
</span>
 </span>
 



<s:hidden id="personId" name="personId" value="%{personId}"/>

	<div class="column">
    <div  class="portlet">
    <div class="portlet-header"><s:text name="stafa.label.insurance_Status"/></div><br>
        <div class="portlet-content">
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  style="width:16;height:16;cursor:pointer;" onclick="addInsuranceStatus();"/>&nbsp;&nbsp;<label class="cursor"  onclick="addInsuranceStatus();"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="deleteInsuranceStatus();"/>&nbsp;&nbsp;<label class="cursor" onclick="deleteInsuranceStatus();"><b>Delete</b></label>&nbsp;</div></td>
				    <td style="width:20%"><div><img src = "images/icons/edit.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="editInsuranceStatus();"/>&nbsp;&nbsp;<label class="cursor" onclick="editInsuranceStatus();"><b>Edit</b></label>&nbsp;</div></td>
				    <!-- <td style="width:20%"><div><img src = "images/icons/save3.png" class="cursor" style="width:16;height:16;" class="cursor" onclick="save_Insurance_Status();" >&nbsp;&nbsp;<label class="cursor" onclick="save_Insurance_Status()"><b>Save</b></label></div></td> -->
				</tr>
			</table>
            
            <div id="insuranceStatusDiv" style="overflow-x:auto;overflow-y:hidden;">
            <table cellpadding="0" cellspacing="0" border="0" id="insuranceStatusTable" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th width="4%" id="insuranceStatusColumn"></th> 
                        <th><s:text name="stafa.label.insurance_Status"/></th>
                        <th><s:text name="stafa.label.date"/></th>
                       
                    </tr>
                </thead>
                <tbody id="insuranceStatusTableTbody">
				
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

    <div class="cleaner"></div>

<div class="column">
    <div  class="portlet">
    <div class="portlet-header"><s:text name="stafa.label.insurance_Approvals"/></div>
        <div class="portlet-content">
           <table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  style="width:16;height:16;cursor:pointer;" onclick="addInsuranceApproval();"/>&nbsp;&nbsp;<label class="cursor"  onclick="addInsuranceApproval();"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="deleteInsuranceApproval();"/>&nbsp;&nbsp;<label class="cursor" onclick="deleteInsuranceApproval();"><b>Delete</b></label>&nbsp;</div></td>
				    <td style="width:20%"><div><img src = "images/icons/edit.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="editInsuranceApproval();"/>&nbsp;&nbsp;<label class="cursor" onclick="editInsuranceApproval();"><b>Edit</b></label>&nbsp;</div></td>
				    <!-- <td style="width:20%"><div><img src = "images/icons/save3.png" class="cursor" style="width:16;height:16;" class="cursor" onclick="save_Insurance_Approval();" >&nbsp;&nbsp;<label class="cursor" onclick="save_Insurance_Approval();"><b>Save</b></label></div></td> -->
				</tr>
			</table>
            <div id="insuranceApprovalDiv" style="overflow-x:auto;overflow-y:hidden;">
            <table style="width:100%" cellpadding="0" cellspacing="0" border="0" id="insuranceApprovalTable" class="display" align="center" >
                <thead>
                    <tr>
                        <th width="4%" id="insuranceApprovalColumn"></th>
                        <th><s:text name="stafa.label.approval_Name"/></th>
                        <th><s:text name="stafa.label.datesubmittedForApproval"/></th>
						<th><s:text name="stafa.label.approvalStatus"/></th>
                        <th><s:text name="stafa.label.i-Plan_Code"/></th>
					    <th><s:text name="stafa.label.auth_Date"/></th>
						<th><s:text name="stafa.label.auth_Code"/></th>
						<th><s:text name="stafa.label.eff_Date"/></th>
						<th><s:text name="stafa.label.end_Date"/></th>	
                    </tr>
                </thead>
                <tbody id="insuranceApprovalTableTbody">
                  
                </tbody>
            </table>
        </div>
    </div>  
</div>
    <div class="cleaner"></div>
</div>
 
 
<s:set var="id" value="%{'insuranceApprovalsDocuments'}" />  
  <div class="column">
	<div  class="portlet">
				<div class="portlet-header "><s:text name = "stafa.label.recipientInsuranceApprovalDocument"/></div><br>
				<span class="hidden">
					<s:select id="%{#attr['id']+'List'}" name="insuranceApprovDocList1" list="insuranceApprovDocList"  headerKey="" headerValue="Select"/>
				</span>
				<jsp:include page="../../doc_activity.jsp"></jsp:include>					
	</div>
  </div>	 
 <script>

//Documents Scripts

/* Category mean widget id */

 var ReferralInsApprovDocParam={		 
		
			 widgetType : <s:property value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@TYPE_WIDGET, @com.velos.stafa.util.VelosStafaConstants@SUBTYPE_INSAPPROVDOCS)"/>,
			 entityId   : '<s:property value="personId"/>',
			 entityType : '<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_PERSON"/>',
			 txId       : '<s:property value="transplantId"/>',
			 visitId    : ''//'<s:property value="visitId"/>'  

 };

 var ReferralInsApprovDocCurrentDoc={

	     TABLE: '<s:property value="id"/>CurrTable',
		 TABLECol : '<s:property value="id"/>CurrColumn',
		 TABLEIdPrefix : '<s:property value="id"/>'

 };

 var ReferralInsApprovDocHistDoc={

	     TABLE: '<s:property value="id"/>HistTable',
		 TABLECol : '<s:property value="id"/>HistColumn',
		 TABLEIdPrefix : '<s:property value="id"/>'
}; 
 
		/*var TABLE_1= '<s:property value="id"/>CurrTable';
		var TABLE_1Column= '<s:property value="id"/>CurrTable';
		var TABLE_1IdPrefix = '<s:property value="id"/>';
		var widgetType=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@TYPE_REFERRAL, @com.velos.stafa.util.VelosStafaConstants@SUBTYPE_REFERRALDOC)"/>;
		var entityId = $('#recipientId_Id').val();
		var entityType='<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_RECIPIENT"/>';*/
      
 var docTableHeaders = new Array();
	    docTableHeaders[0]='<s:text name = "stafa.lable.documentTitle"/>';
	    docTableHeaders[1]='<s:text name = "stafa.lable.documentVersion"/>';
	    docTableHeaders[2]='<s:text name = "stafa.lable.documentDate"/>';
	    docTableHeaders[3]='<s:text name = "stafa.lable.Attachment"/>';
	    docTableHeaders[4]='Status';
	    
 $('#<s:property value="id"/>Tabs' ).tabs();


function constructDocTable(table){
	if(table == ReferralInsApprovDocCurrentDoc.TABLE){
		DocTable.currentTable(true,ReferralInsApprovDocParam.entityId, ReferralInsApprovDocParam.entityType, ReferralInsApprovDocParam.txId, ReferralInsApprovDocParam.visitId, table, docTableHeaders, ReferralInsApprovDocCurrentDoc.TABLECol, ReferralInsApprovDocCurrentDoc.TABLEIdPrefix, ReferralInsApprovDocParam.widgetType);
		
		
	}else if(table==ReferralRecipConsCurrentDoc.TABLE){
		
		DocTable.currentTable(true, ReferralRecipConsDocParam.entityId, ReferralRecipConsDocParam.entityType, ReferralRecipConsDocParam.txId, ReferralRecipConsDocParam.visitId, table, docTableHeaders, ReferralRecipConsCurrentDoc.TABLECol, ReferralRecipConsCurrentDoc.TABLEIdPrefix, ReferralRecipConsDocParam.widgetType);
			
	}
	
}

//preparing document widget
DocTable.currentTable(false, ReferralInsApprovDocParam.entityId, ReferralInsApprovDocParam.entityType,ReferralInsApprovDocParam.txId, ReferralInsApprovDocParam.visitId, ReferralInsApprovDocCurrentDoc.TABLE, docTableHeaders, ReferralInsApprovDocCurrentDoc.TABLECol, ReferralInsApprovDocCurrentDoc.TABLEIdPrefix, ReferralInsApprovDocParam.widgetType);
DocTable.historyTable(false,ReferralInsApprovDocParam.entityId, ReferralInsApprovDocParam.entityType,ReferralInsApprovDocParam.txId, ReferralInsApprovDocParam.visitId, ReferralInsApprovDocHistDoc.TABLE, docTableHeaders, ReferralInsApprovDocHistDoc.TABLECol, ReferralInsApprovDocHistDoc.TABLEIdPrefix, ReferralInsApprovDocParam.widgetType);
</script>
 
<s:set var="id" value="%{'referralDocuments'}" />   
<s:set var="column" value="%{'false'}" />   
<div class="column">
	<div  class="portlet">
				<div class="portlet-header "><s:text name = "stafa.label.recipientConsents"/></div>		
				<span class="hidden">
				  <s:select id="%{#attr['id']+'List'}" name="recipientConsents1" list="recipientConsents"  headerKey="" headerValue="Select"/>
				</span>
				<%--  <input type="button" name="Save Me" onclick="save_documentReview(entityId, 'Recipient', widgetType, '<s:property value="id"/>CurrTable', '<s:property value="id"/>')"/> --%>
				<jsp:include page="../../doc_activity.jsp"></jsp:include>					
		</div>
	</div>
<div class="column">
	<div  class="portlet">
				<div class="portlet-header "><s:text name = "stafa.label.transHistory"/></div>		
				
				<jsp:include page="../../trans_history.jsp"></jsp:include>					
		</div>
	</div>		
<div class="column">
	<div  class="portlet">
				<div class="portlet-header "><s:text name = "stafa.label.accountSummary"/></div>		
				<div style="overflow-x:auto;overflow-y:hidden;">
				<jsp:include page="../../account_summary.jsp"></jsp:include>				
				</div>	
		</div>
	</div>	
<div class="column">
	<div  class="portlet">
				<div class="portlet-header "><s:text name = "stafa.label.accountStatus"/></div>		
				<div style="overflow-x:auto;overflow-y:hidden;" id="accountStatusdDiv">
				<jsp:include page="../../account_status.jsp"></jsp:include>		
				</div>			
		</div>
	</div>	
	

<script>

$(function() {
 
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
 



    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

});
var ReferralRecipConsDocParam={		 
		
		 widgetType : <s:property value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@TYPE_DOCTYPE, @com.velos.stafa.util.VelosStafaConstants@TYPE_RECIPIENTCONSENT)"/>,
		 entityId : $('#recipientId_Id').val(),
		 entityType : '<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_RECIPIENT"/>',
		 txId : '<s:property value="transplantId"/>',
		 visitId : '<s:property value="visitId"/>'  

};

var ReferralRecipConsCurrentDoc={

     TABLE: '<s:property value="id"/>CurrTable',
	 TABLECol : '<s:property value="id"/>CurrColumn',
	 TABLEIdPrefix : '<s:property value="id"/>'

};

var ReferralRecipConsHistDoc={

     TABLE: '<s:property value="id"/>HistTable',
	 TABLECol : '<s:property value="id"/>HistColumn',
	 TABLEIdPrefix : '<s:property value="id"/>'
}; 

/*var TABLE_2= '<s:property value="id"/>CurrTable';
var TABLE_2Column= '<s:property value="id"/>CurrTable';
var TABLE_2IdPrefix = '<s:property value="id"/>';
var widgetType1=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@TYPE_REFERRAL, @com.velos.stafa.util.VelosStafaConstants@SUBTYPE_INSUAPPROVREFERRALDOC)"/>;
*/
$('#<s:property value="id"/>Tabs' ).tabs();
// Preparing reciepient Consent
DocTable.currentTable(false, ReferralRecipConsDocParam.entityId, ReferralRecipConsDocParam.entityType, ReferralRecipConsDocParam.txId, ReferralRecipConsDocParam.visitId, ReferralRecipConsCurrentDoc.TABLE, docTableHeaders, ReferralRecipConsCurrentDoc.TABLECol, ReferralRecipConsCurrentDoc.TABLEIdPrefix, ReferralRecipConsDocParam.widgetType);
DocTable.historyTable(false, ReferralRecipConsDocParam.entityId, ReferralRecipConsDocParam.entityType, ReferralRecipConsDocParam.txId, ReferralRecipConsDocParam.visitId, ReferralRecipConsHistDoc.TABLE, docTableHeaders, ReferralRecipConsHistDoc.TABLECol, ReferralRecipConsHistDoc.TABLEIdPrefix, ReferralRecipConsDocParam.widgetType);
  

</script>