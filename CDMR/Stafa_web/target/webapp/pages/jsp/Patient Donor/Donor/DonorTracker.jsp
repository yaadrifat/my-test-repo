<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/workFlow.js"></script>
<link type="text/css" href="css/workFlow.css" rel="stylesheet" />
<script>
$(function() {
	$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
    .append("<span style=\"float:right;\" class='spanspace'></span><span style=\"float:right;\" class='ui-addnew'></span>");
    $('.jclock').jclock();
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });
  });
$(document).ready(function(){
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"}); 
	  $("select").uniform(); 
      $(window).resize();
	var oTable=$("#donorTracker").dataTable({
											"sPaginationType": "full_numbers",
											"sDom": 'C<"clear">Rlfrtip',
											"aoColumns": [ { "bSortable":false},
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null,
											               null
											             ],
												"oColVis": {
													"aiExclude": [ 0 ],
													"buttonText": "&nbsp;",
													"bRestore": true,
													"sAlign": "left"
												},
												"fnInitComplete": function () {
											        $("#donorTrackerColumn").html($('.ColVis:eq(0)'));
												}
		});
	
    $("select").uniform(); 
});
</script>

<div class="cleaner"></div>

<!--right window start -->	
<div  id="slidedivs"  style="display:none">
<div id="uldiv">
<ul class="ulstyle">
<li class="listyle print_label"><img src = "images/icons/calendar.png"  style="width:30px;height:30px;"/></li>
<li class="listyle print_label" ><b>Calendar</b> </li>
<li class="listyle print_label"><img src = "images/icons/plan.png"  style="width:30px;height:30px;"/></li>
<li class="listyle notes_icon"><b>Plan</b> </li>
<li class="listyle print_label"><img src = "images/icons/Notification.png"  style="width:30px;height:30px;"/></li>
<li class="listyle print_label" ><b>Notification</b> </li>
</ul>
</div>
</div>
<form>
<div class="cleaner"></div>
		<div class="column">		
			<div  class="portlet">
				  <div class="portlet-header addnew">DonorTracker</div>
						<div class="portlet-content">
						<table cellpadding="0" cellspacing="0" border="0" id="donorTracker" class="display">
						<thead>
							<tr>
								<th width="4%" id="donorTrackerColumn"></th>
								<th colspan='1'>Select</th>
								<th colspan='1'>DonorId</th>
								<th colspan='1'>Donor Last Name</th>
								<th colspan='1'>Donor First Name</th>
								<th colspan='1'>DOB</th>
								<th colspan='1'>Age</th>
								<th colspan='1'>MD</th>
								<th colspan='1'>Donor Type</th>
								<th colspan='1'>Product Type</th>
								<th colspan='1'>Relationship to Recipient</th>
								<th colspan='1'>Registry or CBU Name</th>
								<th colspan='1'>Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="" onclick=""></td>
								<td>HCA423479</td>
								<td>Branofsky</td>
								<td>Renee</td>
								<td>Jan 19, 1988</td>
								<td>24</td>
								<td>AML</td>
								<td>Allo</td>
								<td>GR</td>
								<td >New Referral. Patient has a matched Sibling. Awaiting approval for Consult.</td>
								<td>Adults</td>
								<td>Pre-Transplant</td>
							</tr>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="" onclick=""></td>
								<td>HCA877665</td>
								<td>Solikov</td>
								<td>Dori</td>
								<td>Aug 19, 1963</td>
								<td>49</td>
								<td>MM</td>
								<td>Auto</td>
								<td>TS</td>
								<td>New Referral</td>
								<td>Adults</td>
								<td>Pre-Transplant</td>
							</tr>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="" onclick=""></td>
								<td>HCA098968</td>
								<td>Jaurez</td>
								<td>Maria</td>
								<td>Sep 4, 1958</td>
								<td>54</td>
								<td>MDS</td>
								<td>Allo</td>
								<td>TS</td>
								<td>Patient was med management 11/12. Sister did not match. Good NMDP Search.</td>
								<td>Adults</td>
								<td>Med Management</td>
							</tr>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="" onclick=""></td>
								<td>HCA786555</td>
								<td>Geromi</td>
								<td>Nina</td>
								<td>Dec 23, 1995</td>
								<td>17</td>
								<td>AML</td>
								<td>Allo</td>
								<td>DC</td>
								<td>Discussing Immunotheraphy instead of/prior to allo transplant</td>
								<td>Peds</td>
								<td>Sent out for more treatment</td>
							</tr><tr>
								<td></td>
								<td><input type='checkbox'  id="" onclick=""></td>
								<td>HCA897667</td>
								<td>Trumbold</td>
								<td>Robert</td>
								<td>Jan 14, 1949</td>
								<td>64</td>
								<td>MDS</td>
								<td>Allo</td>
								<td>IF</td>
								<td>On track. Scheduled for Prep on 01/05</td>
								<td>Adults</td>
								<td>Transplant</td>
							</tr>
							<tr>
								<td></td>
								<td><input type='checkbox'  id="" onclick=""></td>
								<td>HCA087765</td>
								<td>Ole</td>
								<td>Tyler</td>
								<td>Jun 12, 1999</td>
								<td>13</td>
								<td>ALL</td>
								<td>Allo</td>
								<td>FD</td>
								<td>GVHD followup</td>
								<td>Peds</td>
								<td>Follow-Up</td>
							</tr>
						</tbody>
						</table>
			</div>
		</div>
</div>
<div class="cleaner"></div>
<div class="cleaner"></div>
	
			

<%-- <div class="floatright_right">
	<jsp:include page="sidecontrols.jsp"></jsp:include>
 </div> --%>
 
<div class="cleaner"></div>
		
<div align="right" style="float:right;">

		<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
			<tr>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/>&nbsp;&nbsp;</td>
				<td><input type="button" value="Next" onclick="saveOvernightStorage()"/></td>
			</tr>
		</table>

</div>
</div>
</form>

