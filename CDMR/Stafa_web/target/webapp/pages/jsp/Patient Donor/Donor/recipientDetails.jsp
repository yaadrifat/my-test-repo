
 <%-- <%@ include file="common/includes.jsp" %> --%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page import="com.velos.stafa.util.VelosStafaConstants" %>

<script type="text/javascript" src="js/ajaxfileupload.js"></script>


<style>

.subLinkInsurance{
border:1px solid white;
font-weight:bold;
width:60%;
text-align:center;
line-height:1.3 em;
padding:2%;
background-color:#DADADA;
}
#subMenuWrapper{
background-color:	#B5B5B5;
border-radius:5px;
}
.subDivPortlet{
background-color:	#DADADA;
border-radius:5px;
}
.Selected{
background-color:yellow;
}
</style>

<script>
  
 var seqNo = 0;
var seqNoInsApp= 0; 

function checkTabHighlight(){
	$(".subLinkInsurance").each(function(){
		if($(this).hasClass("selected")){
			$(this).removeClass("selected");
		}	
	});
}

function searchPhysician(e)
{  
	//alert("filet");
    var evt = e || window.event;
    
    var keyPressed = evt.which || evt.keyCode;
	if (keyPressed == 13){
	//	$("#recipientMatchTable").css( 'display', 'block' );
		constructReferringPhysicanList(false);
	
	return false;
	}
	return true;
}


//function constructReferringPhysicanList(flag){
	 
	/* var searchMrn = $("#mrnForSearch").val();
		var referringPhysicianServerParam = function ( aoData ) {
			aoData.push( { "name": "application", "value": "stafa"});
			aoData.push( { "name": "module", "value": "aphresis_referring_search"});	
			
			
			aoData.push( { "name": "param_count", "value":"1"});
			aoData.push( { "name": "param_1_name", "value":":PERSONMRN:"});
			aoData.push( { "name": "param_1_value", "value":"'"+searchMrn+"'"});
			
			aoData.push( {"name": "col_0_name", "value": "lower(tempRecipientMrn)" } );
			aoData.push( { "name": "col_0_column", "value": "0"});
			
			
			aoData.push( { "name": "col_1_name", "value": "lower(fname)"});
			aoData.push( { "name": "col_1_column", "value": "1"});
			
			aoData.push( { "name": "col_2_name", "value": "lower(lname)"});
			aoData.push( { "name": "col_2_column", "value": "2"});
			
			aoData.push( {"name": "col_3_name", "value": "lower(dob)" } );
			aoData.push( { "name": "col_3_column", "value": "3"});
			
			aoData.push( {"name": "col_4_name", "value": "lower(mrn)" } );
			aoData.push( { "name": "col_4_column", "value": "4"});
			
			aoData.push( {"name": "col_5_name", "value": "lower(ssn)" } );
			aoData.push( { "name": "col_5_column", "value": "5"});
			
			aoData.push( {"name": "col_6_name", "value": "lower(bloodgrp)" } );
			aoData.push( { "name": "col_6_column", "value": "6"});
			
		}; */
		/* var referringPhysicianTable_aoColumnDef = [
		 			                         {"aTargets": [ 0], "mDataProp": function ( source, type, val ) {
		 			                            
		 			                             
		 			                           return "<input name='recipeintId" + source[0] +"' id= 'recipeintId" + source[0] +"' type='checkbox' value='" + source[0] +"' onclick='populateRecipientData(this.value)' />"; 
		 			                             
		 			                             }},
		 			                             
		 			                            {    "sTitle":'<s:text name="stafa.label.lastName"/>',
		 			                                 "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
		 			                                	 //?BB link = "<a href='#' onclick='javascript:getDonorDetails(\""+source[0] +"\");'>"+source[2]+"</a>";
		 			                                	 link = source[1];			                                	
		 			                             return link;
		 			                             }},
		 			                          {    "sTitle":'<s:text name="stafa.label.firstName"/>',
		 			                                 "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
		 			                                	 //?BB link = "<a href='#' onclick='javascript:getDonorDetails(\""+source[0] +"\");'>"+source[2]+"</a>";
		 			                                	 link = source[2];			                                	
		 			                             return link;
		 			                             }},
		 			                    
		 			                          {    "sTitle":'<s:text name="stafa.esecurity.label.org.type"/>',
		 			                                 "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
		 			                                	 //?BB link = "<a href='#' onclick='javascript:getDonorDetails(\""+source[0] +"\");'>"+source[3] +"</a>";
		 			                                	 link = source[3] ;
		 					                             return link;
		 			                                 
		 			                                    }},
		 			                          {    "sTitle":'<s:text name="stafa.label.institution"/>',
		 			                                        "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
		 			                                    return source[4];
		 			                                  }},
		 			                                  {    "sTitle":'<s:text name="stafa.label.recipientPhysicianPractice"/>',
		 				                                  "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
		 				                                      return source[5];
		 				                              }},
		 			                                  
		 			                                 {    "sTitle":'<s:text name="stafa.label.address"/>',
		 				                                  "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
		 				                                      return source[6];
		 				                              }},
		 				                             {    "sTitle":'<s:text name="stafa.label.phoneNumber"/>',
		 				                                  "aTargets": [ 7], "mDataProp": function ( source, type, val ) {
		 				                                      return source[7];
		 				                              }},
		 				                             {    "sTitle":'<s:text name="stafa.label.faxNumber"/>',
		 				                                  "aTargets": [ 8], "mDataProp": function ( source, type, val ) {
		 				                                      return source[8];
		 				                              }},
		 				                             {    "sTitle":'<s:text name="stafa.label.recipientPrimaryReferring"/>',
		 				                            	  return "<input name='recipeintId" + source[0] +"' id= 'recipeintId" + source[0] +"' type='radio' value='" + source[0] +"' onclick='populateRecipientData(this.value)' />"; 
		 				                              }}
		 				                             */
		 			                   /*        ,{    "sTitle":'<s:text name="stafa.label.diagnosis"/>',
		 			                                        "aTargets": [ 6], "mDataProp": function ( source, type, val ) {
		 			                                    return source[6];
		 			                                  }}    
		 			                                   */
	/* 	 			                        ];
		var referringPhysicianColManager = function () {
			$("#referringPhysicianMatchColumn").html($('.ColVis:eq(1)'));
		};
		var referringPhysicianCol = [{"bSortable": false},
			               null,
			               null,
			               null,
			               null,
			               null,
			               null
			               ];
		 constructTable(flag,'referringPhysicianTable',referringPhysicianColManager,referringPhysicianServerParam,referringPhysicianTable_aoColumnDef,referringPhysicianCol);

}
 */

$(function() {
    $('.jclock').jclock();
      show_innernorth();
  /*   $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    }); */

  });
function addRows(){
	
	  $("#donorRegistrationChecklistDetails").prepend("<tr>"+
			  "<td width='4%'></td>"+
			  "<td><input type='checkbox' id=''/></td>"+
	            "<td><input type='text' id=''/></td>"+
	            "<td><input type='text' id=''/ class='dateEntry'></td>"+
	            "<td><input type='text' id=''/></td>"+
	            "<td><input type='button' id='' value='Browse'/></td>"+
	            "<td><select><option>Select</option><option>Active</option><option>Expired</option></select></td>"+
	            "</tr>");
  $("select").uniform();
}

$(document).ready(function(){
//show_slidewidgets("recipientInfo_slidewidget");
	portletMinus();
	constructInsuranceStatusTable(true);
	constructInsuranceApprovalTable(true);
	constructReferralDetailTable(true);
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"});
      $(window).resize();
	//show_eastslidewidget("slidedivs");
	var  oTable3=$("#donorRegistrationChecklistDetails").dataTable({
		"sPaginationType": "full_numbers",
		"sDom": 'C<"clear">Rlfrtip',
		"aoColumns": [ { "bSortable":false},
			               null,
			               null,
			               null,
			               null,
			               null,
			               null
			             ],
			"oColVis": {
				"aiExclude": [ 0 ],
				"buttonText": "&nbsp;",
				"bRestore": true,
				"sAlign": "left"
			},
			"fnInitComplete": function () {
		        $("#donorRegistrationChecklistColumn").html($('.ColVis:eq(0)'));
			}
});

/* var  oTable=$("#consultEvalEducationDetails").dataTable({
"sPaginationType": "full_numbers",
"sDom": 'C<"clear">Rlfrtip',
"aoColumns": [ { "bSortable":false},
       null,
       null,
       null,
       null
       ],
"oColVis": {
"aiExclude": [ 0 ],
"buttonText": "&nbsp;",
"bRestore": true,
"sAlign": "left"
},
"fnInitComplete": function () {
$("#consultEvalEducationColumn").html($('.ColVis:eq(1)'));
}
}); */
$("select").uniform();		 
					    
});
$(window).resize(function(){
    var height;
    var screenheight=screen.height;
    var innerheight=$("#innercenter").height();
    var innerheight=594;
    var constanthgt=$("#constantdiv").height();
    /* if(innerheight) */
    if(screenheight<= 768){
       height =innerheight -(constanthgt+240);
    }else if(screenheight>=1024){
           height =innerheight -(constanthgt+40);

    }else if(screenheight>=800 && screenheight < 900){
           height =innerheight -(constanthgt+240);
    }else if(screenheight==900){
           height =innerheight -(constanthgt+130);
    }else if(screenheight==960){
           height =innerheight -(constanthgt+80);
    }
       $('#subdiv').height(height);
    });
function donorEval(){
//	show_slidewidgets("donorInfo",true);
	//$("#dynamicheader").html("Donor Eval");
//	$("#donorMatching").addClass("Selected")
	var form="";
	var url = "loadDonorEvalPage";
	var result = ajaxCall(url,form);
//	alert(result);
	$("#subdiv").html(result);
	portletMinus();
}

function constructInsuranceStatusTable(flag){
	var recipient="";           
	recipient = $("#personID").val();
    var insStatusColDef = [                  
                             {	 "aTargets": [0], "mDataProp": function ( source, type, val ) {
                                        return "<input  type='checkbox' id='insStatusID"+source[0] +"' name='insStatusCheck' value='" + source[0] +"' >";
                                 }},
                              {"sTitle":'<s:text name="stafa.label.insurance_Status"/>',
                                    "aTargets": [1], "mDataProp": function ( source, type, val ) {
                                        return source[3];	
		                                    
                                 },
								   "sClass" : "alignCenter"
								 },
                              {    "sTitle":'<s:text name="stafa.label.date"/>',
                                     "aTargets": [2], "mDataProp": function ( source, type, val ) {
                                    	 var startDate = ($.datepicker.formatDate('M dd, yy', new Date(source[2])));
    									    return startDate;
                                  }}
                            
                            ];
    var insStatusColumns = [{"bSortable": false},
                               null,
                               null
                               ];
    var insStatusColManager = function () {
            $("#insuranceStatusColumn").html($('#insuranceStatusTable_wrapper .ColVis'));
    };

    var insStatusServerParams = function ( aoData ) {
    	
       									 aoData.push( { "name": "application", "value": "stafa"});
       									 aoData.push( { "name": "module", "value": "insurance_status_detail"});  
        								 aoData.push( { "name": "criteria", "value": recipient});									
                                                      
        								 aoData.push( { "name": "col_1_name", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE  pk_codelst=FK_CODELST_INSURANCESTATUS))"});
       									 aoData.push( { "name": "col_1_column", "value": "4"});
      
       									 aoData.push( {"name": "col_2_name", "value": "lower(NVL(TO_CHAR(INSURANCESTATUS_DATE,'Mon dd, yyyy'),''))" });
      									 aoData.push( { "name": "col_2_column", "value": "3"});
                                     
    }
    constructTable(flag,"insuranceStatusTable",insStatusColManager,insStatusServerParams,insStatusColDef,insStatusColumns);                                          
}

function saveRecipientStatus()
{
var personId = $("#personID").val();
var pkRecipient = $("#pkRecipient").val();
var transplantId = <s:property value ="transplantId"/>;
var entityType ="recipient";
var recipientData ="{fkPerson:"+personId+",";
recipientData += "recipientStatus:'"+$("#recipientStatus").val()+ "',";
recipientData += "pkRecipient:"+pkRecipient+",comments:'"+$("#recipientStatusComments").val()+"'}";
var transplantData ="{pkTxWorkflow:"+transplantId+",";
transplantData += "entityId:'"+pkRecipient+ "',";
transplantData += "entityType:"+entityType+",fkRecipientStatus:'"+$("#recipientStatus").val()+"'}";

var url = "saveRecipientStatus";

var jsonData = "transplantData:"+transplantData+",recipientData:"+recipientData;
response = jsonDataCall(url,"jsonData={"+jsonData+"}");

return true; 
}

function savePages(mode){

	if(checkModuleRights(STAFA_RECIPIENTTRACKERS, APPMODE_SAVE)){
		
		var recipientMrnLength=$("#recipientMrn").val().length;
		try{
			if(  recipientMrnLength >0){
					
							if(saveReferralDetails()){
								//alert("saveDonorDetails");
							}
							//Save Disclaimer
							if(saveDisclaimer()){								
							  }
							//Save Recipient Status
							if(saveRecipientStatus()){								
							  }
							
							/*Save Documents*/
							if(Documents.saveDocs(ReferralDocParam, RefInsApprovDocParam, ReferralCurrentDoc, RefInsApprovCurrentDoc)){								
							}
							
							/*SaveRegDetailAndRefCheckLst*/
							if(SaveRegDetailAndRefCheckLst()){
							}
							
							if(save_Insurance_Status()){
								//alert("save_Insurance_Status()");
							}
							
							if(save_Insurance_Approval()){
								//alert("save_Insurance_Approval()");
							}
							
							if(saveReferringPhysician()){
								//alert("saveReferringPhysician()");
							}
							jAlert("  Data Saved Successfully !", "Information ...", function () { 
								openUrl(LoadedPage.url,LoadedPage.workflow,LoadedPage.visitId,LoadedPage.div);
								if(RefreshData!=undefined){
								  Refreshner(RefreshElement,RefreshData);
								}
			                }); 

			                return true;
						
					}else{
					   
							alert("Please Enter MRN");
								
						}
			}catch(e){
			alert("exception " + e);
		}
	}
}

function saveReferralDetails()
{
var personId = $("#personID").val();
var pkRecipient = $("#pkRecipient").val();
var transReq="";
if($('input[name=isTranslaortneed]:radio:checked').val()=='true')
	{
	transReq=true;
	}
else if($('input[name=isTranslaortneed]:radio:checked').val()=='false')
	{
	transReq=false;
	}
var recipientData ="{fkPerson:"+personId+",";
recipientData += "fkCodelstCountry:'"+$("#recipientCountry").val()+ "',";
recipientData += "fkCodelstState:'"+$("#recipientState").val()+ "',";
recipientData += "recipientPrimaryPhone:'"+$("#recipientPrimaryPhone").val()+ "',";
recipientData += "fkCodelstBirthCountry:'"+$("#countryOfBirth").val()+ "',";
recipientData += "recipientPrimaryContact:'"+$("#primContact").val()+ "',";
recipientData += "recipientPrimaryContPhone:'"+$("#primCntactPhne").val()+ "',";
recipientData += "recipientKinLname:'"+$("#kinLastName").val()+ "',";
recipientData += "recipientKinFname:'"+$("#kinFirstName").val()+ "',";
recipientData += "fkCodelstKinRelation:'"+$("#kinRelationship").val()+ "',";
recipientData += "recipientKinAdd1:'"+$("#kinAddressLine1").val()+ "',";
recipientData += "recipientKinAdd2:'"+$("#kinAddressLine2").val()+ "',";
recipientData += "recipientKinCity:'"+$("#kinCity").val()+ "',";
recipientData += "fkCodelstKinState:'"+$("#kinState").val()+ "',";
recipientData += "recipientKinZip:'"+$("#kinZip").val()+ "',";
recipientData += "recipientKinHphone:'"+$("#kinHomePhone").val()+ "',";
recipientData += "recipientDiagnosisDate:'"+$("#dateOfRefferal").val()+ "',";
recipientData += "fkCodelstAborh:'"+$("#recipientabo").val()+"',";
recipientData += "other:'"+$("#other").val()+"',";
recipientData += "recipientTransReq:'"+transReq+"',";
recipientData += "fkPrimaryContactRelation:'"+$("#primCntactRelation").val()+"',";
recipientData += "pkRecipient:"+pkRecipient+",recipientKinWphone:'"+$("#kinWorkPhone").val()+"'}";

var personData = "{pkPerson:"+personId+",";
personData +="personLname:'"+ $("#recipientLastName").val()+ "',";
personData +="fkCodelstRace:'" + $("#race").val()+"',";
personData +="personFname:'"+ $("#recipientFirstName").val()+ "',";
personData +="personMname:'"+ $("#recipientMiddleName").val()+ "',";
personData +="personDob:'" + $("#recipientDob").val()+"',";
personData +="fkCodelstGender:'" + $("#gender").val()+"',";
personData +="personSsn:'" + $("#recipientSsn").val()+ "',";
personData +="personMRN:'" + $("#recipientMrn").val()+"',";
//personData +="personMRN:'" + $("#recipientMrn").val()+"',";
personData +="personAddress1:'" + $("#recipientAddressLine1").val()+"',";
personData +="personAddress2:'" + $("#recipientAddressLine2").val()+"',";
personData +="personCity:'" + $("#recipientCity").val()+"',";
personData +="personZip:'" + $("#recipientZipCode").val()+"',";
personData +="fkCodelstEthnicity:'" + $("#ethnicity").val()+"',";
personData +="personEmail:'" + $("#recipientEmail").val()+"',";
//personData +="fkCodelstEthnicity:'" + $("#ethnicity").val()+"',";
personData +="personHphone:'" + $("#recipientHomePhone").val()+"',";
personData +="personBphone:'" + $("#recipinetWorkPhone").val()+"',";
personData +="fkCodelst_MaritaL:'" + $("#maritialStatus").val()+"',";
personData +="fkCodelstPrilang:'" + $("#primaryLanguge").val()+"'}";
//personData +="fkCodelstAbs:'" + $("#recipientabo").val()+"',";
//personData += "fkCodelstAbs:'"+$("#recipientabo").val()+"'}";
var url = "saveReferralDetails";
var jsonData = "personData:"+personData+",recipientData:"+recipientData;
response = jsonDataCall(url,"jsonData={"+jsonData+"}");
return true; 
}

function saveDisclaimer()
{
	var question1="";
if($('input[name=disclaimerQuestion1]:radio:checked').val()=='true')
	{
	question1=true;
	}
else if($('input[name=disclaimerQuestion1]:radio:checked').val()=='false')
	{
	question1=false;
	}
var personId = $("#personID").val();
var pkRecipient = $("#pkRecipient").val();
var diclaimerData ="{entityId:"+personId+",";
 if($("#pkDisclamier").val()!=null)
	{
	diclaimerData += "pkDisclaimer:'"+$("#pkDisclamier").val()+ "',";
	} 
diclaimerData += "disclaimerCode:'"+$("#disclaimerCode").val()+ "',";
diclaimerData += "entityType:'"+$("#entityType").val()+ "',";
diclaimerData += "question1:"+question1+",comment1:'"+$("#disclaimerComment1").val()+"'}";

var url = "saveDisclaimer";
response = jsonDataCall(url,"jsonData={data:["+diclaimerData+"]}");
return true; 
}
function constructInsuranceApprovalTable(flag){
	var criteria="";           
	criteria = $("#personID").val();
    var insApprovalColDef = [
                                 {
                                    "aTargets": [0], "mDataProp": function ( source, type, val ) {
                                    	 return "<input  type='checkbox' id='insApp"+source[0] +"' name='insApprovalCheck' value='" + source[0] +"' >";
                                 }},
                                 {"sTitle":"<s:text name="stafa.label.approval_Name"/>",
                                    "aTargets": [1], "mDataProp": function ( source, type, val ) {
                                        return source[9];
                                 }},
                                 {    "sTitle":"<s:text name="stafa.label.datesubmittedForApproval"/>",
                                     "aTargets": [2], "mDataProp": function ( source, type, val ) {
                                    	 var	submitDate = ($.datepicker.formatDate('M dd, yy', new Date(source[2])));
 									    return submitDate;
                                  }},
                                 {    "sTitle":"<s:text name="stafa.label.approvalStatus"/>",
                                     "aTargets": [3], "mDataProp": function ( source, type, val ) {
                                         return source[10];
                                  }},
                                 {    "sTitle":"<s:text name="stafa.label.i-Plan_Code"/>",
                                     "aTargets": [4], "mDataProp": function ( source, type, val ) {
                                         return source[4];
                                  }},
                                 {    "sTitle":"<s:text name="stafa.label.auth_Date"/>",
                                     "aTargets": [5], "mDataProp": function ( source, type, val ) {
                                    	 var authDate = ($.datepicker.formatDate('M dd, yy', new Date(source[5])));
  									    return authDate;
                                  }},
                                 {    "sTitle":"<s:text name="stafa.label.auth_Code"/>",
                                     "aTargets": [6], "mDataProp": function ( source, type, val ) {
                                         return source[6];
                                  }},
                                 {    "sTitle":"<s:text name="stafa.label.eff_Date"/>",
                                     "aTargets": [7], "mDataProp": function ( source, type, val ) {
                                    	 var effDate = ($.datepicker.formatDate('M dd, yy', new Date(source[7])));
   									    return effDate;
                                  }},
                                 {    "sTitle":"<s:text name="stafa.label.end_Date"/>",
                                     "aTargets": [8], "mDataProp": function ( source, type, val ) {
                                    	 var endDate = ($.datepicker.formatDate('M dd, yy', new Date(source[8])));
   									    return endDate;
                                  }}
                            ];
    var insApprovalColumns = [{"bSortable": false},
					   null,
                       null,
                       null,
					   null,
					   null,
					   null,
					   null,
					   null ];
    
	var insApprovalColManager = function () {
            $("#insuranceApprovalColumn").html($('#insuranceApprovalTable_wrapper .ColVis'));
    };
    var insApprovalServerParams = function ( aoData ) {
        aoData.push( { "name": "application", "value": "stafa"});
        aoData.push( { "name": "module", "value": "insurance_approval_detail"});  
        aoData.push( { "name": "criteria", "value": criteria});                           
      
        aoData.push( {"name": "col_1_name", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE pk_codelst=FK_CODELST_APPROVAL_NAME))" } );
        aoData.push( { "name": "col_1_column", "value": "10"});
      
        aoData.push( { "name": "col_2_name", "value": "lower(NVL(TO_CHAR(APPROVAL_SUBMISSION_DATE,'Mon dd, yyyy'),''))"});
        aoData.push( { "name": "col_2_column", "value": "3"});
      
        aoData.push( {"name": "col_3_name", "value": "lower((SELECT codelst_desc FROM er_codelst WHERE pk_codelst=FK_CODELST_APPROVAL_STATUS))" } );
        aoData.push( { "name": "col_3_column", "value": "11"});
        
        aoData.push( {"name": "col_4_name", "value": "lower(IPLAN_CODE)" } );
        aoData.push( { "name": "col_4_column", "value": "5"});
        
        aoData.push( {"name": "col_5_name", "value": "lower(NVL(TO_CHAR(AUTHORIZATION_DATE,'Mon dd, yyyy'),''))" } );
        aoData.push( { "name": "col_5_column", "value": "6"});
        
        aoData.push( {"name": "col_6_name", "value": "lower(AUTHORIZATION_CODE)" } );
        aoData.push( { "name": "col_6_column", "value": "7"});
        
        aoData.push( {"name": "col_7_name", "value": "lower(NVL(TO_CHAR(EFFECTIVE_DATE,'Mon dd, yyyy'),''))" } );
        aoData.push( { "name": "col_7_column", "value": "8"});
        
        aoData.push( {"name": "col_8_name", "value": "lower(NVL(TO_CHAR(END_DATE,'Mon dd, yyyy'),''))" } );
        aoData.push( { "name": "col_8_column", "value": "9"});

                                     
    };
    constructTable(flag,"insuranceApprovalTable",insApprovalColManager,insApprovalServerParams,insApprovalColDef,insApprovalColumns);                                          
}

function constructReferralDetailTable(flag){
	var criteria="";           
	criteria = $("#personID").val();
    var refDetailsColDef = [                  
                             {	 "aTargets": [0], "mDataProp": function ( source, type, val ) {
                                        return "<input  type='hidden' id='refPkDetailHidddenId' name='' value='"+source[10]+"' >";
                                 }},
                              {"sTitle":'<s:text name ="stafa.label.lastName"/>',
                                    "aTargets": [1], "mDataProp": function ( source, type, val ) {
                                        return source[1];	
		                                    
                                 }},
                              {    "sTitle":'<s:text name ="stafa.label.firstName"/>',
                                     "aTargets": [2], "mDataProp": function ( source, type, val ) {
                                    	 return source[2];	
                                   }},
							  {    "sTitle":'<s:text name="stafa.esecurity.label.org.type"/>',
                                     "aTargets": [3], "mDataProp": function ( source, type, val ) {
                                    	 return source[3];
                                   }},
                              {    "sTitle":'<s:text name="stafa.label.institution"/>',
                                      "aTargets": [4], "mDataProp": function ( source, type, val ) {
                                     	 return source[4];
                                   }},
                              {    "sTitle":'<s:text name="stafa.label.recipientPhysicianPractice"/>',
                                       "aTargets": [5], "mDataProp": function ( source, type, val ) {
                                      	 return source[5];
                                   }},
                              {    "sTitle":'<s:text name="stafa.label.address"/>',
                                        "aTargets": [6], "mDataProp": function ( source, type, val ) {
                                       	 return source[6];
                                   }},
                              {    "sTitle":'<s:text name="stafa.label.phoneNumber"/>',
                                         "aTargets": [7], "mDataProp": function ( source, type, val ) {
                                        	 return source[7];
                                    }},
                              {    "sTitle":'<s:text name="stafa.label.faxNumber"/>',
                                        "aTargets": [8], "mDataProp": function ( source, type, val ) {
                                       	 return source[8];
                                   }},
                              {    "sTitle":'<s:text name="stafa.label.recipientPrimaryReferring"/>',
                                       "aTargets": [9], "mDataProp": function ( source, type, val ) {
                                    	   if(source[9]=="true")
                                    	   {
                                    		   link ="<input  type='radio' id='refPhysicianIsPrimaryRadio"+source[0]+"' checked='checked' name='refPhysicianIsPrimary' value='"+source[0]+"'></input>";  }
                                    	   else
                                    	{
                                    		   link= "<input  type='radio' id='refPhysicianIsPrimaryRadio"+source[0]+"' name='refPhysicianIsPrimary' value='"+source[0]+"'></input>";  }
                                    	   return link;
                                       }}, 
                            ];
    var refDetailsColumns = [{"bSortable": false},
                               null,
                               null,
                               null,
                               null,
                               null,
                               null,
                               null,
                               null,
                               null
                               ];
    var refDetailsColManager = function () {
            $("#referringPhysicianMatchColumn").html($('#referringPhysicianTable_wrapper .ColVis'));
    };

    var refDetailsServerParams = function ( aoData ) {
                                                    aoData.push( { "name": "application", "value": "stafa"});
                                                    aoData.push( { "name": "module", "value": "referral_detail"});  
                                                    aoData.push( { "name": "criteria", "value": criteria});  
                                                    
                                                    /* aoData.push( { "name": "param_count", "value":"1"});
            										aoData.push( { "name": "param_1_name", "value":":SEARCH:"});
            										aoData.push( { "name": "param_1_value", "value":search}); */
                                                  
                                                    aoData.push( {"name": "col_0_name", "value": "lower(pkInsStatus))" } );
                                                    aoData.push( { "name": "col_0_column", "value": "0"});
                                                  
                                                    aoData.push( { "name": "col_1_name", "value": "lower()"});
                                                    aoData.push( { "name": "col_1_column", "value": "1"});
                                                  
                                                    aoData.push( {"name": "col_2_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_2_column", "value": "2"});
                                                    
                                                    aoData.push( {"name": "col_3_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_3_column", "value": "3"});
                                                    
                                                    aoData.push( {"name": "col_4_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_4_column", "value": "4"});
                                                    
                                                    aoData.push( {"name": "col_5_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_5_column", "value": "5"});
                                                    
                                                    aoData.push( {"name": "col_6_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_6_column", "value": "6"});
                                                    
                                                    aoData.push( {"name": "col_7_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_7_column", "value": "6"});
                                                    
                                                    aoData.push( {"name": "col_8_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_8_column", "value": "6"});
                                                    
                                                    aoData.push( {"name": "col_9_name", "value": "lower()" } );
                                                    aoData.push( { "name": "col_9_column", "value": "6"});
                                                                                 
                                                };
    constructTable(flag,"referringPhysicianTable",refDetailsColManager,refDetailsServerParams,refDetailsColDef,refDetailsColumns);                                          
}

$("#insuranceStatusDiv").scroll(function(){
    var newMargin = $(this).scrollLeft();
    $('#insuranceStatusTable_length').css('marginLeft', newMargin);
    $('#insuranceStatusTable_length').css('width', '20%');
    $('#insuranceStatusTable_filter').css('width', '20%');
    $('#insuranceStatusTable_info').css('marginLeft', newMargin);
    $('#insuranceStatusTable_info').css('width', '40%');
});

$("#insuranceApprovalDiv").scroll(function(){
    var newMargin = $(this).scrollLeft();
    $('#insuranceApprovalTable_length').css('marginLeft', newMargin);
    $('#insuranceApprovalTable_length').css('width', '20%');
    $('#insuranceApprovalTable_filter').css('width', '20%');
    $('#insuranceApprovalTable_info').css('marginLeft', newMargin);
    $('#insuranceApprovalTable_info').css('width', '40%');
});

$("#referralDetailsDiv").scroll(function(){
    var newMargin = $(this).scrollLeft();
    $('#referringPhysicianTable_length').css('marginLeft', newMargin);
    $('#referringPhysicianTable_length').css('width', '20%');
    $('#referringPhysicianTable_filter').css('width', '20%');
    $('#referringPhysicianTable_info').css('marginLeft', newMargin);
    $('#referringPhysicianTable_info').css('width', '40%');
});

function saveReferringPhysician()
{	
	var row,entityId,entityType,fkRefPhysician,isPrimary,refPhysicianId;
	var rowHtml = "";
	var rowData = "";
	entityId = $("#personID").val();
	entityType = "Person";
	
	$("#referringPhysicianTable").find('input[name="refPhysicianIsPrimary"]').each(
	  function(row) {
	  var refDate,accCreatedDate;
	     rowHtml=$(this).closest("tr");
			
		if($(this).is(':checked'))
		{
			refDate = $("#referralDateId").val();
			accCreatedDate = $("#accCreatedDateId").val();
			fkRefPhysician = $(this).val();
			isPrimary = "true";
			refPhysicianId = $(rowHtml).find("#refPkDetailHidddenId").val();
			rowData+= "{refDate:'"+refDate+"',accCreatedDate:'"+accCreatedDate+"',entityId:'"+entityId+"',entityType:'"+entityType+"',fkRefPhysician:'"+fkRefPhysician+"',isPrimary:'"+isPrimary+"',refPhysicianId:'"+refPhysicianId+"'},";
		}
		else
		{
			 if($(rowHtml).find("#refPkDetailHidddenId").val() == 0)
			 {
				 //alert("0");
			 }
			 else
			 {
				 fkRefPhysician = $(this).val();
				 isPrimary = "false";
				 refPhysicianId = $(rowHtml).find("#refPkDetailHidddenId").val();
				 rowData+= "{refDate:'"+refDate+"',accCreatedDate:'"+accCreatedDate+"',entityId:'"+entityId+"',entityType:'"+entityType+"',fkRefPhysician:'"+fkRefPhysician+"',isPrimary:'"+isPrimary+"',refPhysicianId:'"+refPhysicianId+"'},";
			 }
		}
		
	  }); 
	
	   if(rowData.length >0){
	        rowData = rowData.substring(0,(rowData.length-1));
			//alert(rowData);
	 	var url="saveReferralPhysician";
		  response = jsonDataCall(url,"jsonData={data:["+rowData+"]}");
		       //alert("Data Saved Successfully");
		     //constructReferralDetailTable(true);
		} 
	   $('#physicianChoice').val("");
	   $('#physicianChoice').blur();
	return true;
}

$('#physicianChoice').bind('keyup', function(e) {

    if ( e.keyCode === 13 ) { // 13 is enter key
    	constructReferralDetailTable(true);
    	var form="";
        var searchParameter = $("#physicianChoice").val();
        var searchName = searchParameter.toUpperCase();
        var personId = $("#personID").val();
    	var url = "loadAllPhysician.action?searchName="+searchName+"&personId="+personId;
    	//alert(url);
    	var result = ajaxCall(url,form);  
		//alert(result.physicianDetailList.length);	
		if(result.physicianDetailList!=null && result.physicianDetailList.length>0) {
			$.each(result.physicianDetailList,function(){
			
		
		var newRow = "<tr class='odd'>";
		newRow+="<td><input type='hidden' id='refPkDetailHidddenId' Value='0' /></td>";
		newRow+="<td>"+this.personLname+"</td>";
		newRow+="<td>"+this.personFname+"</td>";
		newRow+="<td>"+this.physicianType+"</td>";
		newRow+="<td>"+this.physicianInstitute+"</td>";
		newRow+="<td>"+this.physianPractice+"</td>";
		newRow+="<td>"+this.personAddress1+"</td>";
		newRow+="<td>"+this.personHphone+"</td>";
		newRow+="<td>"+this.personBphone+"</td>";
		newRow+="<td><input type='radio' id='refPhysicianIsPrimaryRadio"+this.phyMaintId+"' name='refPhysicianIsPrimary' value='"+this.phyMaintId+"'></input></td>"
		newRow+="</tr>";
		//alert(newRow);
		$("#referringPhysicianTableBodyId").append(newRow);
			});
			
		}	
    }
});

function addInsuranceStatus()
{
var htmlString =$("#insuranceStatusSpan").html();
var newRow = "<tr class='odd'>";
seqNo = seqNo+1;
newRow+="<td><input type='checkbox' name='insStatusCheck' Value='0' /></td>";
newRow+="<td align='center'>"+htmlString+"</td>";
newRow+="<td><input type='text' name='selectDate' id='in_date"+seqNo+"' class='startDate dateEntry dateclass'/></td>";
newrow="</tr>";
  $("#insuranceStatusTableTbody").prepend(newRow);
		 $( 'input[name="selectDate"]').datepicker({
                                        dateFormat: 'M dd, yy',
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "c-50:c+45"
                                    });
		$.uniform.restore('select');
		$("select").uniform();
}  


function save_Insurance_Status(){
		
		var row, codelistnsuranceStatus, insuranceDate, insuranceStatusId,entityId,entityType;	
		var rowData = "";
		$("#insuranceStatusTable").find("#insuranceStatusSelect").each(
		 function(row) {
			rowHtml=$(this).closest("tr");
			insuranceStatusId = $(rowHtml).find('input[name="insStatusCheck"]').val();
			codelistnsuranceStatus = $(rowHtml).find("#insuranceStatusSelect").val();
			insuranceDate = $(rowHtml).find('input[name="selectDate"]').val();	
			entityId = $("#personID").val();
			entityType="Person";
			  rowData+= "{insuranceStatusId:'"+insuranceStatusId+"',codelistnsuranceStatus:'"+codelistnsuranceStatus+"',insuranceDate:'"+insuranceDate+"',entityId:'"+entityId+"',entityType:'"+entityType+"'},";
				
		});
		if(rowData.length >0){
	        rowData = rowData.substring(0,(rowData.length-1));
			//alert(rowData);
	 	var url="saveInsuranceStatus";
		   response = jsonDataCall(url,"jsonData={data:["+rowData+"]}");
		       //alert("Data Saved Successfully");
		     //constructInsuranceStatusTable(true);
		}
		return true;
	}

var deletedflag=false;
function deleteInsuranceStatus(){
   deletedflag=false;
	var deletedIds = "";
		try{
			$("input[name='insStatusCheck']:checked").each(function(){
				row=$(this).closest("tr");
				if($(row).find('input[name="insStatusCheck"]').val() != 0)
			     deletedIds += $(row).find('input[name="insStatusCheck"]').val() + ",";
				 deletedflag=true;
			
		});
		if(deletedIds.length >0){
		  deletedIds = deletedIds.substr(0,deletedIds.length-1);
		}
		if(deletedflag!=false){
			 var yes=confirm(confirm_deleteMsg);
				if(!yes){
					return;
				}
				//alert(deletedIds);
		
		jsonData = "{deletedIds:'"+deletedIds+"'}";
		url="deleteInsuranceStatus";
	   response = jsonDataCall(url,"jsonData="+jsonData);
	   constructInsuranceStatusTable(true);
		}
		if(deletedflag==false){
			alert("Please Select any Record");
		}
		deletedflag=false;
	   
		}catch(error){
			alert("error " + error);
		}

}  

function addInsuranceApproval()
{
var approvalNamehtmlString =$("#approvalNameSpan").html();
var approvalStatushtmlString =$("#approvalStatusSpan").html();
var newRow = "<tr class='odd'>";
seqNoInsApp = seqNoInsApp+1;
newRow+="<td><input type='checkbox' name='insApprovalCheck' Value='0' /></td>";
newRow+="<td align='center'>"+approvalNamehtmlString+"</td>";
newRow+="<td><input type='text' name='insSubDate' id='insAppDateSub"+seqNoInsApp+"' class='startDate dateEntry dateclass'/></td>";
newRow+="<td align='center'>"+approvalStatushtmlString+"</td>";
newRow+="<td><input type='text' name='insAppIPlanCode' id='insAppIPlanCode"+seqNoInsApp+"'/></td>";
newRow+="<td><input type='text' name='insAuthDate' id='insAuthDate"+seqNoInsApp+"' class='startDate dateEntry dateclass'/></td>";
newRow+="<td><input type='text' name='insAuthCode' id='insAuthCode"+seqNoInsApp+"'/></td>";
newRow+="<td><input type='text' name='insEffDate' id='insEffDate"+seqNoInsApp+"' class='startDate dateEntry dateclass'/></td>";
newRow+="<td><input type='text' name='insEndDate' id='insEndDate"+seqNoInsApp+"' class='startDate dateEntry dateclass'/></td>";
newrow="</tr>";
  $("#insuranceApprovalTableTbody").prepend(newRow);
		 $( 'input[name="insSubDate"]').datepicker({
                                        dateFormat: 'M dd, yy',
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "c-50:c+45"
                                    });
		 $( 'input[name="insAuthDate"]').datepicker({
                                        dateFormat: 'M dd, yy',
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "c-50:c+45"
                                    });							
		 $( 'input[name="insEffDate"]').datepicker({
                                        dateFormat: 'M dd, yy',
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "c-50:c+45"
                                    });
		 $( 'input[name="insEndDate"]').datepicker({
                                        dateFormat: 'M dd, yy',
                                        changeMonth: true,
                                        changeYear: true,
                                        yearRange: "c-50:c+45"
                                    });
		$.uniform.restore('select');
		$("select").uniform();
}  
 
 
function save_Insurance_Approval(){
		var row, insuranceApprovalId, entityId, codelistApprovalName, submissionDate, codelistApprovalStatus, iPlanCode, authorizationDate, authorizationCode, effectiveDate, endDate, entityType;	
		var rowData = "";
		 
		$("#insuranceApprovalTable").find("#approvalNameSelect").each(
			function(row) {
			rowHtml=$(this).closest("tr");
			insuranceApprovalId = $(rowHtml).find('input[name="insApprovalCheck"]').val();
			codelistApprovalName = $(rowHtml).find("#approvalNameSelect").val();
			submissionDate = $(rowHtml).find('input[name="insSubDate"]').val();	
			codelistApprovalStatus = $(rowHtml).find("#approvalStatusSelect").val(); 
			iPlanCode =  $(rowHtml).find('input[name="insAppIPlanCode"]').val();	
			authorizationDate = $(rowHtml).find('input[name="insAuthDate"]').val();
			authorizationCode = $(rowHtml).find('input[name="insAuthCode"]').val();
 			effectiveDate = $(rowHtml).find('input[name="insEffDate"]').val();
			endDate = $(rowHtml).find('input[name="insEndDate"]').val();
			entityId = $("#personID").val();
			entityType = "Person";
			  rowData+= "{insuranceApprovalId:'"+insuranceApprovalId+"',codelistApprovalName:'"+codelistApprovalName+"',submissionDate:'"+submissionDate+"',codelistApprovalStatus:'"+codelistApprovalStatus+"',iPlanCode:'"+iPlanCode+"',authorizationDate:'"+authorizationDate+"',authorizationCode:'"+authorizationCode+"',effectiveDate:'"+effectiveDate+"',endDate: '"+endDate+"',entityId:'"+entityId+"',entityType:'"+entityType+"'},";		
		});
		if(rowData.length >0){
	        rowData = rowData.substring(0,(rowData.length-1));
			//alert(rowData);
	 	var url="saveInsuranceApproval";
		  response = jsonDataCall(url,"jsonData={data:["+rowData+"]}");
		       //alert("Data Saved Successfully");
		       //constructInsuranceApprovalTable(true);
		}
	}

function deleteInsuranceApproval(){
	   deletedflag=false;
		var deletedIds = "";
			try{
				$("input[name='insApprovalCheck']:checked").each(function(){
					row=$(this).closest("tr");
					if($(row).find('input[name="insApprovalCheck"]').val() != 0)
					{
				     deletedIds += $(row).find('input[name="insApprovalCheck"]').val() + ",";
					 deletedflag=true;
				    }
			});
			if(deletedIds.length >0){
			  deletedIds = deletedIds.substr(0,deletedIds.length-1);
			}
			if(deletedflag!=false){
				 var yes=confirm(confirm_deleteMsg);
					if(!yes){
						return;
					}
					//alert(deletedIds);
			
			jsonData = "{deletedIds:'"+deletedIds+"'}";
			url="deleteInsuranceApproval";
		   response = jsonDataCall(url,"jsonData="+jsonData);
		   constructInsuranceApprovalTable(true);
			}
			if(deletedflag==false){
				alert("Please Select any Record");
			}
			deletedflag=false;
		   
			}catch(error){
				alert("error " + error);
			}

	}



function editInsuranceStatus()
{			
	var editFlag=false;
			$("#insuranceStatusTable tbody tr").each(function (i,row){
				 if($(this).find('input[name="insStatusCheck"]').is(":checked")){
					 $(this).find("td").each(function (col){
						 if(col ==0){
			  	 		  }else if(col ==1 ){			
			  	 			 if ($(this).find("#insuranceStatusSelect").val()== undefined){
								 	var insStatusSpan = $(this).html();
								 	$(this).html($("#insuranceStatusSpan").html());
								 	$(this).find("#insuranceStatusSelect option:contains("+insStatusSpan+")").attr('selected', 'selected');
									$.uniform.restore($(this).find("#insuranceStatusSelect"));
									$(this).find("#insuranceStatusSelect").uniform(); 	
							 }   
						 }else if(col ==2){
							 if ($(this).find(".startDate").val()== undefined){
								 	$(this).html("<input type='text' id='selectDate"+i+"' name='selectDate' class='startDate dateEntry dateclass' value='"+$(this).text()+ "'/>")
								  	$("#selectDate"+i).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
								        changeYear: true, yearRange: "c-50:c+45"});
								 }
						 }
					 });
					 editFlag=true;
				 }
			  });
			if(editFlag==false){
				alert("Please Select any Record");
			}
}

function editInsuranceApproval()
{
	var editFlag=false;
	$("#insuranceApprovalTable tbody tr").each(function (i,row){
		 if($(this).find('input[name="insApprovalCheck"]').is(":checked")){
			 $(this).find("td").each(function (col){
				 if(col ==0){
					
	  	 		  }else if(col ==1 ){			
	  	 			 if ($(this).find("#approvalNameSelect").val()== undefined){
						 	var insApprovalCurrentVal = $(this).html();
							
						 	$(this).html($("#approvalNameSpan").html());
						 	$(this).find("#approvalNameSelect option:contains("+insApprovalCurrentVal+")").attr('selected', 'selected');
							$.uniform.restore($(this).find("#approvalNameSelect"));
							$(this).find("#approvalNameSelect").uniform(); 	
					 }   
				 }else if(col ==2){
					 if ($(this).find(".startDate").val()== undefined){
						 	$(this).html("<input type='text' id='insSubDate"+i+"' name='insSubDate' class='startDate dateEntry dateclass' value='"+$(this).text()+ "'/>")
						  	$("#insSubDate"+i).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
						        changeYear: true, yearRange: "c-50:c+45"});
						 }
				 }
				 else if(col ==3){
					 if ($(this).find("#approvalStatusSelect").val()== undefined){
						 	var insApprovalStausVal = $(this).html();
						 	$(this).html($("#approvalStatusSpan").html());
						 	$(this).find("#approvalStatusSelect option:contains("+insApprovalStausVal+")").attr('selected', 'selected');
							$.uniform.restore($(this).find("#approvalStatusSelect"));
							$(this).find("#approvalStatusSelect").uniform(); 	
					 }
				 }           
				 else if(col ==4){
					 if ($(this).find('input[name="insAppIPlanCode"]').val()== undefined){
					 	$(this).html("<input type='text' name='insAppIPlanCode' id='' value='"+$(this).text()+ "'/>");
					 }
				 }
				 
				 else if(col ==5){
					 if ($(this).find('input[name="insAuthDate"]').val()== undefined){
						 	$(this).html("<input type='text' id='insAuthDate"+i+"' name='insAuthDate' class='startDate dateEntry dateclass' value='"+$(this).text()+ "'/>")
						  	$("#insAuthDate"+i).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
						        changeYear: true, yearRange: "c-50:c+45"});
						 }
				 } 
				 else if(col ==6){
					 if ($(this).find('input[name="insAuthCode"]').val()== undefined){
					 	$(this).html("<input type='text' name='insAuthCode' id='' value='"+$(this).text()+ "'/>");
					 }
				 }
				 else if(col ==7){
					 if ($(this).find('input[name="insEffDate"]').val()== undefined){
						 	$(this).html("<input type='text' id='insEffDate"+i+"' name='insEffDate' class='startDate dateEntry dateclass' value='"+$(this).text()+ "'/>")
						  	$("#insEffDate"+i).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
						        changeYear: true, yearRange: "c-50:c+45"});
						 }
				 }
				 
				 else if(col ==8){
					 if ($(this).find('input[name="insEndDate"]').val()== undefined){
						 	$(this).html("<input type='text' id='insEndDate"+i+"' name='insEndDate' class='startDate dateEntry dateclass' value='"+$(this).text()+ "'/>")
						  	$("#insEndDate"+i).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
						        changeYear: true, yearRange: "c-50:c+45"});
						 }
				 }
				 
				 
				 
				 
			 });
			 editFlag=true;
		 }
	  });
	if(editFlag==false){
		alert("Please Select any Record");
	}
}

	 
	

	var SaveRegDetailAndRefCheckLst = function(){

       
		var regDetailAndRefCkeckLstData;
		var physician = $('#txPhysiansName_id').val();
		var depId = $('#department_id').val();

		var medRecObtained = $('#isMedRecObtained_id').is(':checked');
		var dateRecObtained = $('#recipientDateRecordObtain_id').val();
		var pkTxWorkflow = $('#pkTxWorkflow_id').val();
		var entityType = $('#entityType_id').val();
		
		    regDetailAndRefCkeckLstData = "{pkTxWorkflow:'"+pkTxWorkflow+"',entityType:'"+entityType+"',fkTxPhysician:'"+physician+"',fkCodelstDepid:'"+depId+"',isMedicalRecordObtain:"+medRecObtained+",recordObtainDate:'"+dateRecObtained+"'}";

		if(regDetailAndRefCkeckLstData.length >0 ){	
			var url="saveRegDetailAndRefCheckList";
			response = jsonDataCall(url,"regDetailAndRefCkeckLstJsonData={regDetailAndRefCkeckLst:["+regDetailAndRefCkeckLstData+"]}");
		
			$('.progress-indicator').css( 'display', 'none' );
			
		}
		return true;
	};


	 var  RefreshData={
			  firstName:'<s:property value="recipientDetails.recipientFirstName"/>',
			  lastName:'<s:property value="recipientDetails.recipientLastName"/>',
			  dob:'<s:property value="recipientDetails.recipientDob"/>',
			  age:'<s:property value="recipientDetails.recipientAge"/>',
			  aboRh:'<s:property value="recipientDetails.recipientAbo"/>'
	  };



</script>
<span id="hiddendropdowns" class="hidden">
 <span id="insuranceStatusSpan" style="none">
 <s:select id="insuranceStatusSelect"  name="insuranceStatus" list="lstInsuranceStatus" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" />
</span>

<span id="approvalNameSpan">
 <s:select id="approvalNameSelect"  name="approvalName" list="lstApprovalName" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" />
</span>

<span id="approvalStatusSpan">
 <s:select id="approvalStatusSelect"  name="approvalStatus" list="lstApprovalStatus" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" />
</span>
 </span>
<!-- div id="recipientInfo_slidewidget" style="display:none;">

	
	<div class="column">
		<div class="portlet">
			<div class="portlet-header ">
				<s:text name="stafa.label.recipientinformation" />
			</div>
			<div class="portlet-content">
				<table width="100%" border="0">
					<tr>
						<td width="50%"><s:text name="stafa.label.firstName" />:</td>
						<td width="50%" id=""><s:property value = "%{recipientDetails.recipientFirstName}" /></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.lastName" />:</td>
						<td id=""><s:property value = "%{recipientDetails.recipientLastName}"/></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.recipientmrn" />:</td>
						<td id=""><s:property value = "%{recipientDetails.recipientMrn}" /></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.dob" />:</td>
						<td id=""><s:property value = "%{recipientDetails.recipientDob}" /></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.diagnosis_at_transplant" />:</td>
						<td id=""></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.diagnosis_at_transplant" />:</td>
						<td id=""></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.diagnosis_Date" />:</td>
						<td id=""></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.recipientaborh" />:</td>
						<td id=""><s:property  value="%{recipientDetails.recipientAbo}"/></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.donortype" />:</td>
						<td id=""></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.producttype" />:</td>
						<td id=""></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.department" />:</td>
						<td id=""></td>
					</tr>
					<tr>
						<td><s:text name="stafa.label.quicklinks" />:</td>
						<td id=""><a href="#" onclick="javascript:loadRecipientProfile();">Recipient Profile</a></br></td>
					</tr>
					<tr>
					<td></td>
					<td><a href="#" onclick="javascript:loadRecipientInsurancePage();">Insurance</a></br>
						<a href="#">Donor Matching</a></br>
						<a href="#">Availabel product</a></br>
						<a href="#">Physician Tasks</a></td>
					</tr>
				</table>
			</div>

		</div>
	</div>
	
</div-->	
<form id="recipientDetailForm">
<s:hidden id="recipient" name="recipient" value="%{recipient}"/>
<s:hidden id="entityType_id" name="entityType"/>
<s:hidden id="pkRecipient" name="pkRecipient" value="%{recipientDetails.pkRecipient}"/>
<s:hidden id="personID" name="personID" value="%{recipientDetails.fkPerson}"/>
<s:hidden id="pkTxWorkflow_id" name="transplantId"/>
	<div class="column">       
	<div  class="portlet">
    	<div class="portlet-header "><s:text  name = "stafa.lable.recipientRegistration"></s:text></div>
		<div class="portlet-content">
       		            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.firstName"></s:text></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.recipientFirstName}" id="recipientFirstName"  name="recipientFirstName"></s:textfield></td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.mrn"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.recipientMrn}" id="recipientMrn"  name="recipientMrn"></s:textfield></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.middleName"/></td>
       		            		<td class="tablewapper1"><s:textfield id="recipientMiddleName" value = "%{recipientDetails.recipientMiddleName}" name="recipientMiddleName"></s:textfield></td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.preliminaryDiagnosis"/></td>
       		            		<td class="tablewapper1"> <s:textfield  disabled ="disabled" id="recipientAge"  name="recipientAge"></s:textfield></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.lastName"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.recipientLastName}" id="recipientLastName"  name="recipientLastName"></s:textfield></td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.dateOfRefferal"/></td>
       		            		<td class="tablewapper1">
       		            		<s:date   id="diagDate"  name="diagDate" format="MMM dd, yyyy" />
	       		            		<s:textfield id="dateOfRefferal" name="dateOfRefferal" cssClass="dateEntry calDate" value="%{recipientDetails.dateOfRefferal}"/>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.addressLine1"/></td>
       		            		<td class="tablewapper1"><s:textfield id="recipientAddressLine1" value = "%{recipientDetails.recipientAddressLine1}"  name="recipientAddressLine1"></s:textfield></td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.ssn"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.recipientSsn}" id="recipientSsn"  name="recipientSsn"></s:textfield></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.addressLine2"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.recipientAddressLine2}" id="recipientAddressLine2"  name="recipientAddressLine2"></s:textfield></td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.dob"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.recipientDob}" id="recipientDob" name="recipientDob" class="dateEntry calDate" /></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.city"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.recipientCity}" id="recipientCity"  name="recipientCity"></s:textfield></td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.age"/></td>
       		            		<td class="tablewapper1"><s:textfield  value = "%{recipientDetails.recipientAge}" id="recipientAge"  name="recipientAge"></s:textfield></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.state"/></td>
       		            		<td class="tablewapper1"> <s:select value = "%{recipientDetails.recipientState}" id="recipientState"  name="recipientState" list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@STATE]" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /> </td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.gender"/></td>
       		            		<td class="tablewapper1"> <s:select cssStyle = "width:auto"   id="gender" value="%{recipientDetails.gender}" name="gender" list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@GENDER]" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.country"/></td>
       		            		<td class="tablewapper1"><s:select cssStyle = "width:auto"   id="recipientCountry" value="%{recipientDetails.recipientCountry}" name="country" list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@COUNTRY]" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.aboRH"/></td>
       		            		<td class="tablewapper1"><s:select cssStyle = "width:auto"  id="recipientabo" value="%{recipientDetails.recipientAbo}"  name="recipientabo" list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@ABO]" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /> </td>
       		            	</tr>
       		            	
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.zipCode"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.recipientZipCode}" id="recipientZipCode" name="recipientZipCode"></s:textfield></td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.primaryLanguge"/></td>
       		            		<td class="tablewapper1"><s:select cssStyle = "width:auto"  id="primaryLanguge" value="%{recipientDetails.primaryLanguge}"  name="primaryLanguge" list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@LANGUAGE]" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
       		            	</tr>
       		            	
       		            	
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.primaryPhone"/></td>
       		            		<td class="tablewapper1"><s:textfield id="recipientPrimaryPhone" value = "%{recipientDetails.recipinetPrimaryPhone}" name="recipientPrimaryPhone"></s:textfield></td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.transRequired"/></td>
       		            		<td class="tablewapper1"><s:radio name="isTranslaortneed" value ="%{recipientDetails.isTranslaortneed}" id="isTranslaortneed" list="#{'true':'Yes','false':'NO'}" ></s:radio>
       		            	</tr>
       		            	
       		            		
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.homePhone"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.recipinetHomePhone}" id="recipientHomePhone" name="recipientHomePhone"></s:textfield></td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.maritalStatus"/></td>
       		            		<td class="tablewapper1"> 
       		            		<s:select cssStyle = "width:auto" id="maritialStatus" value = "%{recipientDetails.maritialStatus}"  name="" list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@MARITIAL_STATUS]" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" />
       		            		
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.workPhone"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.recipinetWorkPhone}" id="recipinetWorkPhone" name="recipientWorkPhone"></s:textfield></td>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientSpouseOther"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.other}" id="other" name="other"></s:textfield> </td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.emailAddress"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.recipientEmail}" id="recipientEmail"  name="recipientEmail"></s:textfield> </td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.kinLastName"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.kinLastName}" id="kinLastName" name="kinLastName"></s:textfield></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.race"/></td>
       		            		<td class="tablewapper1"> <s:select cssStyle = "width:auto" id="race" value = "%{recipientDetails.race}"  name="race" list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@RACE]" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.kinFirstName"/></td>
       		            		<td class="tablewapper1"><s:textfield id="kinFirstName" value = "%{recipientDetails.kinFirstName}" name="kinFirstName"></s:textfield></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.ethnicity"/></td>
       		            		<td class="tablewapper1"> <s:select cssStyle = "width:auto" id="ethnicity" value = "%{recipientDetails.ethnicity}"  name="ethnicity" list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@ETHINICITY]" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /> </td> 
       		            		<td class="tablewapper"><s:text  name = "stafa.label.kinRelationship"/></td>
       		            		<td class="tablewapper1"><s:select cssStyle = "width:auto" id="kinRelationship" value = "%{recipientDetails.kinRelation}" name="kinRelationship" list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@RELATIONSHIP]" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /> </td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.countryOfBirth"/></td>
       		            		<td class="tablewapper1"><s:select cssStyle = "width:auto" id="countryOfBirth" value = "%{recipientDetails.countryOfBirth}"  name="countryOfBirth" list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@COUNTRY]" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /> </td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.kinAddressLine1"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.kinAddressLine1}" id="kinAddressLine1" name="kinAddressLine1"></s:textfield></td>
       		            	</tr>
       		        
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.primContact"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.primContact}" id="primContact" name="primContact"></s:textfield></td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.kinAddressLine2"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.kinAddressLine2}" id="kinAddressLine2" name="kinAddressLine2"></s:textfield></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.primCntactRelation"/></td>
       		            		<td class="tablewapper1"><s:select cssStyle = "width:auto" id="primCntactRelation" value = "%{recipientDetails.primCntactRelation}"  name="" list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@PRIM_CNT_RELATION]" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.city"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.kinCity}" id="kinCity" name="kinCity"></s:textfield></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.primCntactPhne"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.primCntactPhne}" id="primCntactPhne" name="primCntactPhne"></s:textfield></td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.state"/></td>
       		            		<td class="tablewapper1"><s:select cssStyle = "width:auto" id="kinState" value = "%{recipientDetails.kinState}"  name="" list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@STATE]" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td>
       		            	</tr>
       		            	<tr>
       		            	<td></td><td></td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.zipCode"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.kinZip}" id="kinZip" name="kinZip"></s:textfield></td>
       		            		
       		            	</tr>
       		            	<tr>
       		            	<td></td><td></td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.kinHomePhone"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.kinHomePhone}" id="kinHomePhone" name="kinHomePhone"></s:textfield></td>
       		            		
       		            	</tr>
       		           <tr>
       		            	<td></td><td></td>
       		            		<td class="tablewapper"><s:text  name = "stafa.label.kinWorkPhone"/></td>
       		            		<td class="tablewapper1"><s:textfield value = "%{recipientDetails.kinWorkPhone}" id="kinWorkPhone" name="kinWorkPhone"></s:textfield></td>
       		            		
       		            	</tr>
       		            </table>
        </div>
    </div>
    	<div class="column">
      <div  class="portlet">
        <div class="portlet-header"><s:text name= "stafa.label.recipientReferralDetails"/></div>
        <div class="portlet-content">
       		            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="85%">
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientReferralDate"/></td>
       		            		<td class="tablewapper1"><input type='text' name='referralDate' id='referralDateId' class='startDate dateEntry dateclass'/></td>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientDateOfAccount"/></td>
       		            		<td class="tablewapper1"><input type='text' name='accCreatedDate' id='accCreatedDateId' class='startDate dateEntry dateclass'/></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientReferringPhysician"/></td>
       		            		<td class="tablewapper1">
								<s:textfield id="physicianChoice" name="physicianChoiceListName" class="" />
       		            		</td>
       		            	</tr>
       		            	
       		            </table>
       		          <div id="referralDetailsDiv" style="overflow-x:auto;overflow-y:hidden;"> 
       		           <table cellpadding="0" cellspacing="0" border="0" id="referringPhysicianTable"  class="display">
						<thead>
							<tr>
								<th width="4%" id="referringPhysicianMatchColumn"></th>
								<th colspan='1'><s:text name ="stafa.label.lastName"/></th>
								<th colspan='1'><s:text name ="stafa.label.firstName"/></th>
								<th colspan='1'><s:text name ="stafa.esecurity.label.org.type"/></th>
								<th colspan='1'><s:text name ="stafa.label.institution"/></th>
								<th colspan='1'><s:text name ="stafa.label.recipientPhysicianPractice"/></th>
								<th colspan='1'><s:text name ="stafa.label.address"/></th>
								<th colspan='1'><s:text name ="stafa.label.phoneNumber"/></th>
								<th colspan='1'><s:text name ="stafa.label.faxNumber"/></th>
								<th colspan='1'><s:text name ="stafa.label.recipientPrimaryReferring"/></th>
								
							</tr>
						</thead>
						<tbody id="referringPhysicianTableBodyId">
						</tbody>
						</table>
					</div>	
        </div>
    </div>
    </div>
    <s:set var="id" value="%{'referralDocuments'}" />   
    <s:set var="column" value="%{'false'}" />   
<div class="column">
	<div  class="portlet">
				<div class="portlet-header "><s:text name = "stafa.label.recipientReferralDocuments"/></div>		
				<span class="hidden">
				 <s:select id="%{#attr['id']+'List'}" name="referralDocList1" list="referralDocList"  headerKey="" headerValue="Select"/>
				</span>
				 <%-- <input type="button" name="Save Me" onclick="save_documentReview(entityId, 'Recipient', widgetType, '<s:property value="id"/>CurrTable', '<s:property value="id"/>')"/>--%>
				<jsp:include page="../../doc_activity.jsp"></jsp:include>
					<%-- <div class="portlet-content">
						<table>
							<tr>
								<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addRows();"/>&nbsp;&nbsp;<label  class="cursor" onclick="addRows"><b><s:text name = "stafa.lable.add"/></b></label>&nbsp;</div></td>
								<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b><s:text name = "stafa.lable.delete"/></b></label>&nbsp;</div></td>
							</tr>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" id="donorRegistrationChecklistDetails" class="display">
						<thead>
							<tr>
								<th width="4%" id="donorRegistrationChecklistColumn"></th>
								<th colspan='1'><s:text name = "stafa.lable.documentTitle"/></th>
								<th colspan='1'><s:text name = "stafa.lable.documentVersion"/></th>
								<th colspan='1'><s:text name = "stafa.lable.documentDate"/></th>
								<th colspan='1'><s:text name = "stafa.lable.Attachment"/></th>
								
							</tr>
						</thead>
						<tbody>
						</tbody>
						</table>
			</div>--%>
		</div>
	</div>	
		 
	
<script>/*Category mean widget id*/

var ReferralDocParam ={

		 widgetType : <s:property value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@TYPE_REFERRAL, @com.velos.stafa.util.VelosStafaConstants@SUBTYPE_REFERRALDOC)"/>,
		 entityId 	: $('#recipient').val(),
		 entityType : '<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_RECIPIENT"/>',
		 txId 		: '<s:property value="transplantId"/>',
		 visitId 	: '<s:property value="visitId"/>'
		
};

var ReferralCurrentDoc={

	     TABLE: '<s:property value="id"/>CurrTable',
		 TABLECol : '<s:property value="id"/>CurrColumn',
		 TABLEIdPrefix : '<s:property value="id"/>'

};

var ReferralHistDoc={

	     TABLE: '<s:property value="id"/>HistTable',
		 TABLECol : '<s:property value="id"/>HistColumn',
		 TABLEIdPrefix : '<s:property value="id"/>'
};

	/*	var TABLE_1= '<s:property value="id"/>CurrTable';
		var TABLE_1IdPrefix = '<s:property value="id"/>';
		var widgetType=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@TYPE_REFERRAL, @com.velos.stafa.util.VelosStafaConstants@SUBTYPE_REFERRALDOC)"/>;
		var entityId = $('#recipient').val();
		var entityType='<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_RECIPIENT"/>';
        
        */
var docTableHeaders = new Array();
	    docTableHeaders[0]='<s:text name = "stafa.lable.documentTitle"/>';
	    docTableHeaders[1]='<s:text name = "stafa.lable.documentVersion"/>';
	    docTableHeaders[2]='<s:text name = "stafa.lable.documentDate"/>';
	    docTableHeaders[3]='<s:text name = "stafa.lable.Attachment"/>';
	    docTableHeaders[4]='Status';

$('#<s:property value="id"/>Tabs' ).tabs();

function constructDocTable(table){
	if(table == ReferralCurrentDoc.TABLE){
		DocTable.currentTable(true, ReferralDocParam.entityId, ReferralDocParam.entityType, ReferralDocParam.txId, ReferralDocParam.visitId, table, docTableHeaders, ReferralCurrentDoc.TABLECol, ReferralCurrentDoc.TABLEIdPrefix, ReferralDocParam.widgetType);
		
		
	}else if(table==RefInsApprovCurrentDoc.TABLE){
		
		DocTable.currentTable(false, RefInsApprovDocParam.entityId, RefInsApprovDocParam.entityType, RefInsApprovDocParam.txId, RefInsApprovDocParam.visitId, table, docTableHeaders, RefInsApprovCurrentDoc.TABLECol, RefInsApprovCurrentDoc.TABLEIdPrefix, RefInsApprovDocParam.widgetType);
			
	}
	
}

 //preparing document widget
DocTable.currentTable(false,ReferralDocParam.entityId, ReferralDocParam.entityType, ReferralDocParam.txId, ReferralDocParam.visitId, ReferralCurrentDoc.TABLE, docTableHeaders, ReferralCurrentDoc.TABLECol, ReferralCurrentDoc.TABLEIdPrefix, ReferralDocParam.widgetType);
DocTable.historyTable(false,ReferralDocParam.entityId, ReferralDocParam.entityType, ReferralDocParam.txId, ReferralDocParam.visitId,ReferralHistDoc.TABLE, docTableHeaders, ReferralHistDoc.TABLECol, ReferralHistDoc.TABLEIdPrefix, ReferralDocParam.widgetType);





</script>
	
		 <div  class="portlet">
		 <%-- <input type="button" onclick="SaveRegDetailAndRefCheckLst();" value="Save"/> --%>
        <div class="portlet-header"><s:text name= "stafa.label.recipientRegistrationDetail"/></div>
        <div class="portlet-content">
       		            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientTransplantPhysician"/></td>
       		            		<td class="tablewapper1"><s:select id="txPhysiansName_id" name="txPhysiansName" list="txPhysName"  headerKey="" headerValue="Select"/></td>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientDepartment"/></td>
       		            		<td class="tablewapper1"><s:select id="department_id" name="department" list="departments"  headerKey="" headerValue="Select"/></td>
       		            		
       		            	</tr>
       		            	
       		            </table>
        </div>
    	</div>
	
		<div  class="portlet">
        <div class="portlet-header"><s:text name= "stafa.label.recipientReferralCheckList"/></div>
        <div class="portlet-content">
       		            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientMedicalRecordObtain"/></td>
       		            		<td class="tablewapper1"><s:checkbox name="medRecObtained" id="isMedRecObtained_id"/></td>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientDateRecordObtain"/></td>
       		            		<td class="tablewapper1">
	       		            		<s:date   id="medRecObtainedDate_id"  name="medRecObtainedDate" format="MMM dd, yyyy" />
	       		            		<s:textfield id="recipientDateRecordObtain_id" name="medRecObtainedDate" cssClass="dateEntry calDate" value="%{medRecObtainedDate_id}"/>
       		            		</td>
       		            		
       		            	</tr>
       		            	
       		            </table>
        </div>
    	</div>

	<div class="column">
    <div  class="portlet">
    <div class="portlet-header"><s:text name="stafa.label.insurance_Status"/></div><br>
        <div class="portlet-content">
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  style="width:16;height:16;cursor:pointer;" onclick="addInsuranceStatus();"/>&nbsp;&nbsp;<label class="cursor"  onclick="addInsuranceStatus();"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="deleteInsuranceStatus();"/>&nbsp;&nbsp;<label class="cursor" onclick="deleteInsuranceStatus();"><b>Delete</b></label>&nbsp;</div></td>
				    <td style="width:20%"><div><img src = "images/icons/edit.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="editInsuranceStatus();"/>&nbsp;&nbsp;<label class="cursor" onclick="editInsuranceStatus();"><b>Edit</b></label>&nbsp;</div></td>
				    <!-- <td style="width:20%"><div><img src = "images/icons/save3.png" class="cursor" style="width:16;height:16;" class="cursor" onclick="save_Insurance_Status();" >&nbsp;&nbsp;<label class="cursor" onclick="save_Insurance_Status()"><b>Save</b></label></div></td> -->
				</tr>
			</table>
            
            <div id="insuranceStatusDiv" style="overflow-x:auto;overflow-y:hidden;">
            <table cellpadding="0" cellspacing="0" border="0" id="insuranceStatusTable" class="display">
                <thead>
                    <tr>
                        <th width="4%" id="insuranceStatusColumn"></th> 
                        <th><s:text name="stafa.label.insurance_Status"/></th>
                        <th><s:text name="stafa.label.date"/></th>
                       
                    </tr>
                </thead>
                <tbody id="insuranceStatusTableTbody">
				
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

	
<div class="column">
    <div  class="portlet">
    <div class="portlet-header"><s:text name="stafa.label.insurance_Approvals"/></div>
        <div class="portlet-content">
           <table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  style="width:16;height:16;cursor:pointer;" onclick="addInsuranceApproval();"/>&nbsp;&nbsp;<label class="cursor"  onclick="addInsuranceApproval();"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="deleteInsuranceApproval();"/>&nbsp;&nbsp;<label class="cursor" onclick="deleteInsuranceApproval();"><b>Delete</b></label>&nbsp;</div></td>
				    <td style="width:20%"><div><img src = "images/icons/edit.png" class="cursor" style="width:16;height:16;cursor:pointer;"  onclick="editInsuranceApproval();"/>&nbsp;&nbsp;<label class="cursor" onclick="editInsuranceApproval();"><b>Edit</b></label>&nbsp;</div></td>
				    <!-- <td style="width:20%"><div><img src = "images/icons/save3.png" class="cursor" style="width:16;height:16;" class="cursor" onclick="save_Insurance_Approval();" >&nbsp;&nbsp;<label class="cursor" onclick="save_Insurance_Approval();"><b>Save</b></label></div></td> --> 
				</tr>
			</table>
            <div id="insuranceApprovalDiv" style="overflow-x:auto;overflow-y:hidden;">
            <table cellpadding="0" cellspacing="0" border="0" id="insuranceApprovalTable" class="display">
                <thead>
                    <tr>
                        <th width="4%" id="insuranceApprovalColumn"></th>
                        <th><s:text name="stafa.label.approval_Name"/></th>
                        <th><s:text name="stafa.label.datesubmittedForApproval"/></th>
						<th><s:text name="stafa.label.approvalStatus"/></th>
                        <th><s:text name="stafa.label.i-Plan_Code"/></th>
					    <th><s:text name="stafa.label.auth_Date"/></th>
						<th><s:text name="stafa.label.auth_Code"/></th>
						<th><s:text name="stafa.label.eff_Date"/></th>
						<th><s:text name="stafa.label.end_Date"/></th>	
                    </tr>
                </thead>
                <tbody id="insuranceApprovalTableTbody">
                  
                </tbody>
            </table>
        </div>
    </div>  
</div>
    <div class="cleaner"></div>
</div>


	 
		<s:set var="id" value="%{'insuranceApprovalsDocuments'}" />  
		<div class="column">
	<div  class="portlet">
				<div class="portlet-header "><s:text name = "stafa.label.recipientInsuranceApprovalDocument"/></div><br>
				<span class="hidden">
					<s:select id="%{#attr['id']+'List'}" name="insuranceApprovDocList1" list="insuranceApprovDocList"  headerKey="" headerValue="Select"/>
				</span>
				<jsp:include page="../../doc_activity.jsp"></jsp:include>
					<%-- <div class="portlet-content">
						<table>
							<tr>
								<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addRows();"/>&nbsp;&nbsp;<label  class="cursor" onclick="addRows"><b><s:text name = "stafa.lable.add"/></b></label>&nbsp;</div></td>
								<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b><s:text name = "stafa.lable.delete"/></b></label>&nbsp;</div></td>
							</tr>
						</table>
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
						<thead>
							<tr>
								<th width="4%" id="donorRegistrationChecklistColumn"></th>
								<th colspan='1'><s:text name = "stafa.lable.documentTitle"/></th>
								<th colspan='1'><s:text name = "stafa.lable.documentVersion"/></th>
								<th colspan='1'><s:text name = "stafa.lable.documentDate"/></th>
								<th colspan='1'><s:text name = "stafa.lable.Attachment"/></th>
								
							</tr>
						</thead>
						<tbody>
						</tbody>
						</table>
			</div>--%>
		</div>
	</div>	 
	<script>

	var RefInsApprovDocParam ={

			 widgetType : <s:property value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@TYPE_WIDGET, @com.velos.stafa.util.VelosStafaConstants@SUBTYPE_INSAPPROVDOCS)"/>,
			 entityId 	: '<s:property value="personId"/>',
			 entityType : '<s:property value="@com.velos.stafa.util.VelosStafaConstants@ENTITY_TYPE_PERSON"/>',
			 txId 		: '<s:property value="transplantId"/>',
			 visitId 	: ''//'<s:property value="visitId"/>'
			
	};

	var RefInsApprovCurrentDoc={

		     TABLE: '<s:property value="id"/>CurrTable',
			 TABLECol : '<s:property value="id"/>CurrColumn',
			 TABLEIdPrefix : '<s:property value="id"/>'

	};

	var RefInsApprovHistDoc={

		     TABLE: '<s:property value="id"/>HistTable',
			 TABLECol : '<s:property value="id"/>HistColumn',
			 TABLEIdPrefix : '<s:property value="id"/>'
	};
	
	/*var TABLE_2= '<s:property value="id"/>CurrTable';
	var TABLE_2IdPrefix = '<s:property value="id"/>';
	var widgetType1=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@TYPE_REFERRAL, @com.velos.stafa.util.VelosStafaConstants@SUBTYPE_INSUAPPROVREFERRALDOC)"/>;
	*/
	$('#<s:property value="id"/>Tabs' ).tabs();
	DocTable.currentTable(false, RefInsApprovDocParam.entityId, RefInsApprovDocParam.entityType, RefInsApprovDocParam.txId, RefInsApprovDocParam.visitId, RefInsApprovCurrentDoc.TABLE, docTableHeaders, RefInsApprovCurrentDoc.TABLECol, RefInsApprovCurrentDoc.TABLEIdPrefix, RefInsApprovDocParam.widgetType);
	DocTable.historyTable(false, RefInsApprovDocParam.entityId, RefInsApprovDocParam.entityType, RefInsApprovDocParam.txId, RefInsApprovDocParam.visitId, RefInsApprovHistDoc.TABLE, docTableHeaders, RefInsApprovHistDoc.TABLECol, RefInsApprovHistDoc.TABLEIdPrefix, RefInsApprovDocParam.widgetType);
	  
	</script>
	 <div  class="portlet">
        <div class="portlet-header"><s:text name= "stafa.label.recipientDisclaimers"/></div>
    		<jsp:include page="../../disclaimer.jsp">
						<jsp:param value="referralDisclaimer" name="disclaimerType"/>
						</jsp:include>		
    </div>

 <div  class="portlet">
        <div class="portlet-header"><s:text name= "stafa.label.recipientStatus"/></div>
        <div class="portlet-content">
        
        <jsp:include page="../../recipient_status.jsp"></jsp:include>
       		    
        </div>
    </div>
	
	</div>
	
	
	 </form>
	 <s:set name="Contant" value="getCodeListDescByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@REC_WORKFLOW,@com.velos.stafa.util.VelosStafaConstants@REC_WORKFLOW_CONSLUT)" scope="request"/>
	 <s:set name="ConstantVal" value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@REC_WORKFLOW_STATUS,@com.velos.stafa.util.VelosStafaConstants@REC_WORKFLOW_STATUS_CONSLUT_PAND)" scope="request"/>
	 <s:set name="CurrentVal" value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@REC_WORKFLOW_STATUS,@com.velos.stafa.util.VelosStafaConstants@REC_WORKFLOW_STATUS_REFERRAL_PAND)" scope="request"/>
	 <s:set name="UpdateVal" value="getCodeListPkByTypeAndSubtype(@com.velos.stafa.util.VelosStafaConstants@REC_WORKFLOW_STATUS,@com.velos.stafa.util.VelosStafaConstants@REC_WORKFLOW_STATUS_REFERRAL_COMP)" scope="request"/>
	 <jsp:include page="../../common_next_page.jsp">
			<jsp:param name="Contant" value="<%=request.getAttribute(\"Contant\")%>" />
			<jsp:param name="ConstantVal" value="<%=request.getAttribute(\"ConstantVal\")%>" />
			<jsp:param name="CurrentVal" value="<%=request.getAttribute(\"CurrentVal\")%>" />
			<jsp:param name="UpdateVal" value="<%=request.getAttribute(\"UpdateVal\")%>" />
     </jsp:include>
<script type="text/javascript">

$("#recipientDateRecordObtain").datepicker({
											   dateFormat: 'M dd, yy',
											   changeMonth: true,
											   changeYear: true,
											   yearRange: '1900:' + new Date().getFullYear()
											});
</script>
