
 <%-- <%@ include file="common/includes.jsp" %> --%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script type="text/javascript" src="js/workFlow.js"></script>
<link type="text/css" href="css/workFlow.css" rel="stylesheet" />
<style>
.workFlowContainer{
background-color:	#FFFFFF;;
border-radius:5px;
}
.subLinkInsurance{
border:1px solid white;
font-weight:bold;
width:60%;
text-align:center;
line-height:1.3 em;
padding:2%;
background-color:#DADADA;
}
#subMenuWrapper{
background-color:	#B5B5B5;
border-radius:5px;
}
.subDivPortlet{
background-color:	#DADADA;
border-radius:5px;
}
.Selected{
background-color:yellow;
}
</style>

<script>
function checkTabHighlight(){
	$(".subLinkInsurance").each(function(){
		if($(this).hasClass("selected")){
			$(this).removeClass("selected");
		}	
	});
}
$(function() {
    $('.jclock').jclock();
  /*   $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    }); */

  });
function addRows(){
	
	  $("#donorRegistrationChecklistDetails").prepend("<tr>"+
			  "<td width='4%'></td>"+
			  "<td><input type='checkbox' id=''/></td>"+
	            "<td><input type='text' id=''/></td>"+
	            "<td><input type='text' id=''/ class='dateEntry'></td>"+
	            "<td><input type='text' id=''/></td>"+
	            "<td><input type='button' id='' value='Browse'/></td>"+
	            "<td><select><option>Select</option><option>Active</option><option>Expired</option></select></td>"+
	            "</tr>");
  $("select").uniform();
}

$(document).ready(function(){
	portletMinus();
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"});
      $(window).resize();
	//show_eastslidewidget("slidedivs");
	var  oTable3=$("#donorRegistrationChecklistDetails").dataTable({
		"sPaginationType": "full_numbers",
		"sDom": 'C<"clear">Rlfrtip',
		"aoColumns": [ { "bSortable":false},
			               null,
			               null,
			               null,
			               null,
			               null,
			               null
			             ],
			"oColVis": {
				"aiExclude": [ 0 ],
				"buttonText": "&nbsp;",
				"bRestore": true,
				"sAlign": "left"
			},
			"fnInitComplete": function () {
		        $("#donorRegistrationChecklistColumn").html($('.ColVis:eq(0)'));
			}
});

/* var  oTable=$("#consultEvalEducationDetails").dataTable({
"sPaginationType": "full_numbers",
"sDom": 'C<"clear">Rlfrtip',
"aoColumns": [ { "bSortable":false},
       null,
       null,
       null,
       null
       ],
"oColVis": {
"aiExclude": [ 0 ],
"buttonText": "&nbsp;",
"bRestore": true,
"sAlign": "left"
},
"fnInitComplete": function () {
$("#consultEvalEducationColumn").html($('.ColVis:eq(1)'));
}
}); */
$("select").uniform();		 
					    
});
$(window).resize(function(){
    var height;
    var screenheight=screen.height;
    var innerheight=$("#innercenter").height();
    var innerheight=594;
    var constanthgt=$("#constantdiv").height();
    /* if(innerheight) */
    if(screenheight<= 768){
       height =innerheight -(constanthgt+240);
    }else if(screenheight>=1024){
           height =innerheight -(constanthgt+40);

    }else if(screenheight>=800 && screenheight < 900){
           height =innerheight -(constanthgt+240);
    }else if(screenheight==900){
           height =innerheight -(constanthgt+130);
    }else if(screenheight==960){
           height =innerheight -(constanthgt+80);
    }
       $('#subdiv').height(height);
    });
function donorEval(){
//	show_slidewidgets("donorInfo",true);
	//$("#dynamicheader").html("Donor Eval");
//	$("#donorMatching").addClass("Selected")
	var form="";
	var url = "loadDonorEvalPage";
	var result = ajaxCall(url,form);
//	alert(result);
	$("#subdiv").html(result);
	portletMinus();
}
</script>

<div class="cleaner"></div>
<!--right window start -->	
<div  id="slidedivs"  style="display:none">
	<div id="uldiv">
			<ul class="ulstyle">
				<li class="listyle print_label"><img src = "images/icons/calendar.png"  style="width:30px;height:30px;"/></li>
				<li class="listyle print_label" ><b>Calendar</b> </li>
				<li class="listyle print_label"><img src = "images/icons/plan.png"  style="width:30px;height:30px;"/></li>
				<li class="listyle notes_icon"><b>Plan</b> </li>
				<li class="listyle print_label"><img src = "images/icons/Notification.png"  style="width:30px;height:30px;"/></li>
				<li class="listyle print_label" ><b>Notification</b> </li>
			</ul>
	</div>
</div>
<form>
<!-- <div class="portlet"> -->
<!-- 	<div class="portlet-header" id="dynamicheader">Donor Registration</div> -->
		<div class="portlet-content">
				<div class="workFlowContainer">
					 <div class="workFlow">
						 <div class='workflowContent' >
						 		<div class='processstarted' id="referalProcess" onclick=''></div>
						 		<div class='onprogress' id="referalID" onclick='check(this)'><p class='samplet-text'>Registration</p></div>
						 </div>
						 <div class='workflowContent'>
						 	<div class='processinitial' id="consultProcess" onclick=''></div>
						 	<div class='initial'  id="consultID" onclick='check(this),donorEval(),checkBox(this)'>
						 		<p class='samplet-text'>Donor Evaluation</p>
						 	</div>
						 </div>
						 <div class='workflowContent'>
						 	<div class='processinitial' id="preTransplantProcess"'></div>
						 	<div class='initial' id="preTransplantID" onclick='check(this),checkBox(this)'>
						 		<p class='samplet-text'>Donor Suitability</p>
						 	</div>
						 </div>
						 <div class='workflowContent'>
						 	<div class='processinitial' id="stemCellProcess"onclick=''></div>
						 	<div class='initial'  id="stemCellCollectionID" onclick='check(this),checkBox(this)'>
						 		<p class='samplet-text'>Collection</p>
						 	</div>
						 </div>
						 <div class='workflowContent'>
							 <div class='processinitial' id="followupProcess"onclick=''></div>
							 <div class='initial' id="followUpID" onclick=''>
							 	<p class='samplet-text'>Followup</p>
							 </div>
							 </div>
					</div>
			</div>
		</div>
<!-- </div> -->
<!-- <div  id="donorInfo"  style="display:none">
<div class="portlet">
	<div class="portlet-header">Donor Info</div>
		<div class="portlet-content">
									<table width="100%" border="0">
									  <tr>
										<td width="50%" style="padding:2%">First Name:</td>
										<td width="50%" id="firstName" name="firstName" style="padding:2%">Sarah</td>
									  </tr>
									  <tr>
										<td style="padding:2%">Last Name:</td>
										<td id="lastName" name="lastName" style="padding:2%">Branofsky</td>
									  </tr>
									  <tr>
										<td style="padding:2%">Recipient ID:</td>
										<td id="recipientId" name="recipientId" style="padding:2%">HCA897695</td>
									  </tr>
									  <tr>
										<td style="padding:2%">MRN:</td>
										<td id="mrn" name="mrn" style="padding:2%">398708</td>
									  </tr>
									  <tr>
										<td style="padding:2%">DOB:</td>
										<td id="dob" name="dob" style="padding:2%"> May 20, 1985</td>
									  </tr>
									   <tr>
										<td style="padding:2%">Age:</td>
										<td id="age" name="age" style="padding:2%">27</td>
									  </tr>
									   <tr>
										<td style="padding:2%">Registry or CBU:</td>
										<td id="registryorCBU" name="registryorCBU" style="padding:2%">N/A</td>
									  </tr>
									   <tr>
										<td style="padding:2%">ABO/Rh:</td>
										<td id="abo/Rh" name="abo/Rh" style="padding:2%">A+</td>
									  </tr>
									  <tr>
										<td style="padding:2%">Department:</td>
										<td id="department" name="department" style="padding:2%">Adults</td>
									  </tr>
									  <tr>
										<td style="padding:2%">Quicklinks:</td>
										<td id="donorprofile" name="donorprofile" style="padding:2%"><a href="#" src="" id="" name="" style="color:blue">Donor Profile</a></td>
									  </tr>
									  <tr>
										<td></td>
										<td id="donorMatching" name="donormatching" style="padding:2%"><a href="#" src="" id="" name="" style="color:blue">Donor Matching</a></td>
									  </tr>
									</table>
								</div>
						</div>
						</div> -->
<div id="subdiv" style="height:15em;overflow-y:auto;">
    <div  class="portlet">
    <div class="portlet-header">Donor Registration</div>
        <div class="portlet-content">
       		            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="85%">
       		            	<tr>
       		            		<td class="tablewapper">First Name</td>
       		            		<td class="tablewapper1"><input type="text" id=""  name=""></td>
       		            		<td class="tablewapper">Relationship to Recipient</td>
       		            		<td class="tablewapper1"><select><option>select</option><option>Sibling</option></select></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper">Middle Name</td>
       		            		<td class="tablewapper1"><input type="text" id=""  name=""></td>
       		            		<td class="tablewapper">SSN</td>
       		            		<td class="tablewapper1"><input type="text" id="" name=""></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper">Last Name</td>
       		            		<td class="tablewapper1"><input type="text" id=""  name=""></td>
       		            		<td class="tablewapper">Relationship to Recipient</td>
       		            		<td class="tablewapper1"><input type="text'"id="" name="" class="dateEntry"></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper">Maiden Name</td>
       		            		<td class="tablewapper1"><input type="text" id=""  name=""></td>
       		            		<td class="tablewapper">Age</td>
       		            		<td class="tablewapper1"><input type="text" id=""  name=""></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper">Address Line 1</td>
       		            		<td class="tablewapper1"><input type="text" id=""  name=""></td>
       		            		<td class="tablewapper">Gender</td>
       		            		<td class="tablewapper1"><select><option>select</option><option>Male</option><option>Female</option></select></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper">Address Line 2</td>
       		            		<td class="tablewapper1"><input type="text" id=""  name=""></td>
       		            		<td class="tablewapper">ABO/Rh</td>
       		            		<td class="tablewapper1"><select><option>select</option><option>A+</option><option>A-</option></select></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper">City</td>
       		            		<td class="tablewapper1"><input type="text" id=""  name=""></td>
       		            		<td class="tablewapper">Race</td>
       		            		<td class="tablewapper1"><select><option>select</option></select></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper">State</td>
       		            		<td class="tablewapper1"><select><option>select</option><option>Texas</option></select></td>
       		            		<td class="tablewapper">Ethnicity</td>
       		            		<td class="tablewapper1"><select><option>select</option></select></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper">Country</td>
       		            		<td class="tablewapper1"><select><option>select</option><option>USA</option></select></td>
       		            		<td class="tablewapper">Primary Language</td>
       		            		<td class="tablewapper1"><select><option>select</option></select></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper">Zipcode</td>
       		            		<td class="tablewapper1"><input type="text" id=""name=""> </td>
       		            		<td class="tablewapper">Requires a Translator</td>
       		            		<td class="tablewapper1"><input type="Radio" name="1"  id="">yes<input type="Radio" name="1" id="">No </td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper">Primary Phone #</td>
       		            		<td class="tablewapper1"><input type="text" id=""name=""> </td>
       		            		<td class="tablewapper">Highest Level of Education</td>
       		            		<td class="tablewapper1"><select><option>select</option></select></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper">Home Phone #</td>
       		            		<td class="tablewapper1"><input type="text" id=""name=""> </td>
       		            		<td class="tablewapper">Martial Status</td>
       		            		<td class="tablewapper1"><select><option>select</option></select></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper">Work Phone #</td>
       		            		<td class="tablewapper1"><input type="text" id=""name=""> </td>
       		            		<td class="tablewapper">Spouse/Significant Other</td>
       		            		<td class="tablewapper1"><select><option>select</option></select></td>
       		            	</tr>
       		            </table>
        </div>
    </div>
      <div  class="portlet">
        <div class="portlet-header">Donor Registration Details</div>
        <div class="portlet-content">
       		            <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="85%">
       		            	<tr>
       		            		<td class="tablewapper">Department</td>
       		            		<td class="tablewapper1"><select><option>select</option><option>Peds</option></select></td>
       		            		<td class="tablewapper">Donor Type</td>
       		            		<td class="tablewapper1"><select><option>select</option><option>Allo Rel</option></select></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper">Assigned Physician</td>
       		            		<td class="tablewapper1"><select><option>select</option></select></td>
       		            		<td class="tablewapper">Product Type</td>
       		            		<td class="tablewapper1"><select><option>select</option><option>HPC-A</option></select></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper">MRN #</td>
       		            		<td class="tablewapper1"><input type="text" id=""  name=""></td>
       		            		<td class="tablewapper">ABO/RH</td>
       		            		<td class="tablewapper1"><select><option>select</option><option>A+</option></select></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper">Donor Alias</td>
       		            		<td class="tablewapper1"><input type="button" id=""  name="" value="View"></td>
       		            		<td class="tablewapper"></td>
       		            		<td class="tablewapper1"></td>
       		            	</tr>
       		            </table>
        </div>
    </div>
    <div class="column">
    <div  class="portlet"  style="overflow-x:auto;overflow-Y:-moz-hidden-unscrollable">
        <div class="portlet-header">Donor HLA</div>
        <div class="portlet-content">
            <table cellpadding="0" cellspacing="" class="display" align="" border="0" width="100%">
                <thead>
                <tr>
                <th colspan=3>Donor Initial HLA Typing1</th>
                <th colspan=2>Donor Initial HLA Typing2</th>
                <th colspan=2>Donor Confirmatory HLA Typing</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Resolution</td>
                    <td colspan="2"><div style="padding-left:13%;"><select><option>select</option><option>High Resolution</option></select></div></td>
                    <td colspan="2"><div style="padding-left:35%;"><select><option>select</option><option>High Resolution</option></select></div></td>
                    <td colspan="2"><div style="padding-left:35%;"><select><option>select</option><option>High Resolution</option></select></div></td>
                </tr>
                <tr>
                    <td>HLA-A</td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                     <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                </tr>
                 <tr>
                    <td>HLA-B</td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                     <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                </tr>
                  <tr>
                    <td>HLA-C</td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                     <td><input type="text" id="" class=""/></td>
                     <td><input type="text" id="" class=""/></td>
                </tr>
                 <tr>
                    <td>HLA-DR</td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                     <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                </tr>
                 <tr>
                    <td>HLA-DQ</td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                     <td><input type="text" id="" class=""/></td>
                    <td><input type="text" id="" class=""/></td>
                </tr>
                  <tr>
                    <td>Date Tested</td>
                    <td colspan="2"><div style="padding-left:13%;"><input type="text" id="" class="dateEntry"/></div></td>
                    <td colspan="2"><div style="padding-left:35%;"><input type="text" id="" class="dateEntry"/></div></td>
                    <td colspan="2"><div style="padding-left:35%;"><input type="text" id="" class="dateEntry"/></div></td>
                </tr>
                 <tr>
                    <td>Test Location</td>
                    <td colspan="2"><div style="padding-left:13%;"><select><option>select</option><option>High Resolution</option></select></div></td>
                    <td colspan="2"><div style="padding-left:35%;"><select><option>select</option><option>High Resolution</option></select></div></td>
                    <td colspan="2"><div style="padding-left:35%;"><select><option>select</option><option>High Resolution</option></select></div></td>
                </tr>
                 <tr>
                    <td>Attachment</td>
                    <td colspan="2"><div style="padding-left:13%;"><input id="recipientHLAattach" class="attach" type="file" onclick=""  style="display:none;"/><a id="recipientHLAbrowse" href="#"> Browse</a> </div></td>
                    <td colspan="2"><div style="padding-left:35%;"><input id="recipientHLAattach1" class="attach" type="file"  onclick="" style="display:none;"/><a id="recipientHLAbrowse1" href="#">Browse</a> </div></td>
                    <td colspan="2"><div style="padding-left:35%;"><input id="recipientHLAattach1" class="attach" type="file"  onclick="" style="display:none;"/><a id="recipientHLAbrowse1" href="#">Browse</a> </div></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="column">
	<div  class="portlet">
				<div class="portlet-header ">Donor Registration Checklist</div><br>
					<div class="portlet-content">
						<table>
							<tr>
								<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addRows();"/>&nbsp;&nbsp;<label  class="cursor" onclick="addRows"><b>Add</b></label>&nbsp;</div></td>
								<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
						    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
							</tr>
						</table>
						<table cellpadding="0" cellspacing="0" border="0" id="donorRegistrationChecklistDetails" class="display">
						<thead>
							<tr>
								<th width="4%" id="donorRegistrationChecklistColumn"></th>
								<th colspan='1'>Select</th>
								<th colspan='1'>Items</th>
								<th colspan='1'>Date</th>
								<th colspan='1'>Document Name</th>
								<th colspan='1'>Attachment</th>
								<th colspan='1'>Status</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						</table>
			</div>
		</div>
	</div>	 
	</div>
	 </form>

<!-- <script type="text/javascript">



var now = new dateEntry();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivaldateEntry').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new dateEntry()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};
$("#timeZone").html(timezones[-offset / 60]);

</script>
 -->













 












 