<%@ taglib prefix="s" uri="/struts-tags"%>
<style>
.highlighting{
background-color:#33ffff;
}
</style>
<script>
function ckj(This){
	  var flag=$(This).closest("tr").attr("class");
	    $(This).closest("tr").removeClass(flag);
	    $(This).closest("tr").addClass("highlighting");
	    $(This).closest("tr").attr("id",flag);
}
$(function() {
    $('.jclock').jclock();
    //alert($(".portlet").find( ".portlet-header" ).find("span").length);
    $( ".portlet").each(function(){
		if(!$(this).find( ".portlet-header" ).find("span").length > 0){
			   $(this ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
			    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
			   $(this).find(".portlet-header .ui-icon").click(function() {
			       $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
			        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
			    });
		}
});
   
  });
function addRows(){
	  //alert("!");
	  $("#unrelatedDonorSearch tbody ").prepend("<tr>"+
			  "<td width='4%'></td>"+
			  "<td><input type='checkbox' id=''/></td>"+
	            "<td><input type='text' id=''/></td>"+
	            "<td><input type='text' id=''/></td>"+
	            "<td><select><option>Select</option><option>Active</option><option>Expired</option></select></td>"+
	            "<td><select><option>Select</option><option>Active</option><option>Expired</option></select></td>"+
	            "<td><select><option>Select</option><option>Active</option><option>Expired</option></select></td>"+
	            "<td><input type='text' id=''/ class='dateEnry'></td>"+
	            "</tr>");
	       $("select").uniform();
	}
  function showDonorID(){
	    showPopUp('open','addmodal','Donor IDs','800','300');
	}
function donorRegistration(){
	var form="";
	var url = "loadDonorRegistrationPage";
	var result = ajaxCall(url,form);
	$("#main").html(result);
//	portletMinus();
}
$(document).ready(function(){
	portletMinus();	
    //$("select").uniform(); 
    $("#recipientProfile").replaceWith("<td id='donorProfile' name='donorProfile' style='padding:2%'><a href='#' src='' id='' name='' style='color:blue'>Donor Profile</a></td>");;
	$( ".dateEntry").datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	       changeYear: true, yearRange: "c-50:c+45"});
     // $(window).resize();
	//show_eastslidewidget("slidedivs");
		 		
		 			var  oTable2=$("#potentialDonors").dataTable({
						"sPaginationType": "full_numbers",
						"sDom": 'C<"clear">Rlfrtip',
						"aoColumns": [ { "bSortable":false},
							               null,
							               null,
							               null,
							               null,
							               null,
							               null,
							               null,
							               null,
							               null,
							               null,
							               null
							               ],
							"oColVis": {
								"aiExclude": [ 0 ],
								"buttonText": "&nbsp;",
								"bRestore": true,
								"sAlign": "left"
							},
							"fnInitComplete": function () {
						        $("#potentialDonorsColumn").html($('.ColVis:eq(0)'));
							}
				});

		 			var  oTable=$("#unrelatedDonorSearch").dataTable({
						"sPaginationType": "full_numbers",
						"sDom": 'C<"clear">Rlfrtip',
						"aoColumns": [ { "bSortable":false},
						               { "bSortable":false},
							               null,
							               null,
							               null,
							               null,
							               null,
							               null,
								               ],
							"oColVis": {
								"aiExclude": [ 0 ],
								"buttonText": "&nbsp;",
								"bRestore": true,
								"sAlign": "left",
							},
							"fnInitComplete": function () {
						        $("#unrelatedDonorSearchColumn").html($('.ColVis:eq(1)'));
							}
				});
		 			var  oTable1=$("#unrelatedDonorSearchResult").dataTable({
						"sPaginationType": "full_numbers",
						"sDom": 'C<"clear">Rlfrtip',
						"aoColumns": [ { "bSortable":false},
							               null,
							               null,
							               null,
							               null,
							               null,
							               null,
							               null,
							               null
							               ],
							"oColVis": {
								"aiExclude": [ 0 ],
								"buttonText": "&nbsp;",
								"bRestore": true,
								"sAlign": "left"
							},
							"fnInitComplete": function () {
						        $("#unrelatedDonorSearchResultColumn").html($('.ColVis:eq(2)'));
							}
				});
					    
		 		   
});
$("select").uniform(); 
</script>
<div class="column">	
	<div  class="portlet">
			<div class="portlet-header ">Potential Donors</div>
			<div class="portlet-content">
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="donorRegistration()"/>&nbsp;&nbsp;<label  class="cursor" onclick="donorRegistration()"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
			    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" id="potentialDonors" class="display">
			<thead>
				<tr>
					<th width="4%" id="potentialDonorsColumn"></th>
					<th colspan='1'>Select</th>
					<th colspan='1'>Last Name</th>
					<th colspan='1'>First Name</th>
					<th colspan='1'>Date of Birth</th>
					<th colspan='1'>Age</th>
					<th colspan='1'>Gender</th>
					<th colspan='1'>Relationship to Recipient</th>
					<th colspan='1'>Donor Type</th>
					<th colspan='1'>Product Type</th>
					<th colspan='1'>Donor Status</th>
					<th colspan='1'>Selected Donor</th>	
				</tr>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td><input type='checkbox'  id="DocumentVerified" onclick=""></td>
					<td>Barnosfky</td>
					<td>Sarah</td>
					<td>May 7,1985</td>
					<td>27</td>
					<td>Female</td>
					<td>Sibling</td>
					<td>Allo Related</td>
					<td>HPC - A</td>
					<td>Registration</td>
					<td><input type='checkbox'  id="selectedDonor" onclick="ckj(this)"></td>
				</tr>
				<tr>
				<td></td>
					<td><input type='checkbox'  id="DocumentVerified" onclick=""></td>
					<td>Barnosfky</td>
					<td>Sarah</td>
					<td>May 7,1985</td>
					<td>27</td>
					<td>Female</td>
					<td>Sibling</td>
					<td>Allo Related</td>
					<td>HPC - A</td>
					<td>Registration</td>
					<td><input type='checkbox'  id="selectedDonor" onclick="ckj(this)"></td>
				</tr>
			</tbody>
			</table>
		</div>
	</div>
</div>
<div class="column">	
	<div  class="portlet">
			<div class="portlet-header ">Unrelated Donor Searches - Registries and Cord Blood Banks</div>
			<div class="portlet-content">
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick="addRows()"/>&nbsp;&nbsp;<label  class="cursor" onclick="addRows"><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
			    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
				</tr>
			</table>
			<div style="overflow-x:auto;overflow-y:hidden;">
			<table cellpadding="0" cellspacing="0" border="0" id="unrelatedDonorSearch" class="display">
				<thead>
					<tr>
						<th width="4%" id="unrelatedDonorSearchColumn"></th>
						<th>Select</th>
						<th>Registry or UCB</th>
						<th>Registry or UCB Bank ID</th>
						<th>Donor Type</th>
						<th>Product Type</th>
						<th>Search Status</th>
						<th>Date of Last status Change</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td></td>
						<td><input type='checkbox'  id="DocumentVerified" onclick=""></td>
						<td>Barnosfky</td>
						<td>Sarah</td>
						<td>May 7,1985</td>
						<td>27</td>
						<td>Female</td>
						<td>Sibling</td>
					</tr>
					<tr>
						<td></td>
						<td><input type='checkbox'  id="DocumentVerified" onclick=""></td>
						<td>Barnosfky</td>
						<td>Sarah</td>
						<td>May 7,1985</td>
						<td>27</td>
						<td>Female</td>
						<td>Sibling</td>
					</tr>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<div class="column">	
	<div  class="portlet">
			<div class="portlet-header ">Unrelated Donor Search Results</div>
			<div class="portlet-content">
			<table>
				<tr>
					<td style="width:20%"><div><img src = "images/icons/addnew.jpg" class="cursor"  onclick=""/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Add</b></label>&nbsp;</div></td>
					<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" "/>&nbsp;&nbsp;<label class="cursor"  onclick=""><b>Delete</b></label>&nbsp;</div></td>
			    	<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;cursor:pointer;" "/>&nbsp;&nbsp;<label  class="cursor" onclick=""><b>Edit</b></label>&nbsp;</div></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" id="unrelatedDonorSearchResult" class="display">
			<thead>
				<tr>
					<th width="4%" id="unrelatedDonorSearchResultColumn"></th>
					<th colspan='1'>Select</th>
					<th colspan='1'>Registry or UCB</th>
					<th colspan='1'>Unrelated Donor or Cord Blood Unit ID</th>
					<th colspan='1'>Product Type</th>
					<th colspan='1'>Selected for Typing</th>
					<th colspan='1'>Selected for Eval</th>
					<th colspan='1'>Selected Donor</th>
					<th colspan='1'>Donor Status</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td><input type='checkbox'  id="DocumentVerified" onclick=""></td>
					<td>NMDP</td>
					<td>0768899</td>
					<td>HPC-A</td>
					<td><input type='checkbox'  id="" onclick=""></td>
					<td><input type='checkbox'  id="" onclick=""></td>
					<td><input type='checkbox'  id="" onclick=""></td>
					<td></td>
				</tr>
				<tr>
				<td></td>
					<td><input type='checkbox'  id="DocumentVerified" onclick=""></td>
					<td>NMDP</td>
					<td>0876568</td>
					<td>HPC-A</td>
					<td><input type='checkbox'  id="" onclick=""></td>
					<td><input type='checkbox'  id="" onclick=""></td>
					<td><input type='checkbox'  id="" onclick=""></td>
					<td></td>
				</tr>
			</tbody>
			</table>
		</div>
	</div>
</div>