<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="common/includes.jsp" %>
<script type="text/javascript" src="js/dataSearch.js"></script>

<style type="text/css">
.highlighting{
/*background-color:#33ffff;*/
}
</style>

<script type="text/javascript">

function savePages(){
	try{
		if(saveDataFiled()){
			return true;
		}
	}catch(e){
		alert("exception " + e);
	}
}

function deleteRows(){
	//  alert("Delete Rows");
	
	  var deletedIds = "";
	  try{
	  		$("#dataLibraryTable tbody tr").each(function (row){
	  			 if($(this).find("#pkDataQc").is(":checked")){
					//alert($(this).val());
					deletedIds += $(this).find("#pkDataQc").val() + ",";
	  			 }
		 });
		if(deletedIds.length<=0){
			alertbox(err_delete,"ERROR");
			return;
			}
		  var yes=confirm(confirm_deleteMsg);
			if(!yes){
				return;
			}
		  if(deletedIds.length >0){
			  deletedIds = deletedIds.substr(0,deletedIds.length-1);
		  }
		  var url = "deleteDataField";
		  jsonData = "deleteDataFieldIds="+deletedIds;
		//  alert("deletedIds " + deletedIds);
		  ajaxresult = jsonDataCall(url,jsonData);
	        //alert("json data " + jsonData);
          constructTable(true,'dataLibraryTable',datafield_ColManager,datafield_serverParam,datafield_aoColumnDef,datafield_aoColumn);
	  }catch(error){
			alert("error " + error);
	  }
}
function editRows(){
	//  alert("edit Rows");
	
	  $("#dataLibraryTable tbody tr").each(function (row){
		 if($(this).find("#pkDataQc").is(":checked")){
			 var applock = $(this).find("#applock").val();
			 $(this).find("td").each(function (col){
				 
				 if(col ==1 ){
					 if ($(this).find("#codeListQcName").val()== undefined){
						$(this).html("<input type='text' id='codeListQcName' name='codeListQcName' value='"+$(this).text()+ "' />");
					 }
				 }else if(col ==2){
					 if(applock!=1){
						 if ($(this).find("#codeListQcCode").val()== undefined){
						 	$(this).html("<input type='text' class='codeListCode' id='codeListQcCode' name='codeListQcCode' value='"+$(this).text()+ "' />");
						 }
					 }
				 }else if(col ==3){
					  if ($(this).find("#codeListQcModule").val()== undefined){
					 	//$(this).html("<input type='text' id='codeListQcModule' name='codeListQcModule' value='"+$(this).text()+ "' />");
					 	var oldModuleVal = $(this).text();
					 	$(this).html("<div><select name='codeListQcModule' id='codeListQcModule'>"+$.trim($("#codeListModule").html())+"</select></div>");
					 	//alert(oldModuleVal);
					 	$(this).find("#codeListQcModule").val(oldModuleVal);
					 	//alert("sss");
					 	$.uniform.restore('select');
						 $("select").uniform();
						 $("#uniform-codeListModule").hide();
					 } 
					 //?BB $(this).html("stafa");
				 }else if(col ==4){
					 if ($(this).find("#codeListQcDesc").val()== undefined){
						 	$(this).html("<input type='text' id='codeListQcDesc' name='codeListQcDesc' value='"+$(this).text()+ "' />");
						 }
				}
					
			 });
		 }
	  });
  }


function saveDataFiled(){

	var rowData = "";
	 var pkDataQc;
	 var applock;
	 var codeListQcName;
	 var codeListQcCode;
	 var codeListQcModule;
	 var codeListQcDesc ;
	 var qcIsHide ;
	 var type="stafa";
	 isActivate =0;
	 
	$("#dataLibraryTable tbody tr").each(function (row){
		  codeListQcName="";
 		  codeListQcCode="";
 		  codeListQcModule="";
 		  codeListQcDesc ="";
 		  qcIsHide ="";
	 	$(this).find("td").each(function (col){
	 		 
	 		
		 		if(col ==0){
		 			pkDataQc= $(this).find("#pkDataQc").val();
		 			applock= $(this).find("#applock").val();
			 	 }else if(col ==1){
			 		codeListQcName=$(this).find("#codeListQcName").val();
			 	 }else if(col ==2){
			 		
				 	 if($(this).find(".codeListCode").hasClass("codeListCode")){
			 			codeListQcCode=$(this).find("#codeListQcCode").val();
				 	 }else if($.trim($(this).text())==""){
						 	 if(codeListQcName!=undefined){
					 			//var randomCode = codeListQcName.replace('',' ');
					 			var randomCode = codeListQcName.replace(/\s/g, '').toLowerCase();
					 			//alert("randomCode : "+randomCode);
					 			codeListQcCode=$.trim(randomCode);
						 	 }
					 }else{
						 codeListQcCode = $(this).text();
							}
			 	}else if(col ==3){
				 	//alert("-->"+$(this).html())
				 	codeListQcModule=$(this).find("#codeListQcModule").val();
			 		//?BB codeListQcModule="stafa";
			 	 }else if(col ==4){
			 		codeListQcDesc=$(this).find("#codeListQcDesc").val();
			 	 }else if(col ==5){
			 		qcIsHide = "N";
			 	 }
		  });
	 		if(codeListQcName != undefined && codeListQcName!=""){
		 	  rowData+= "{pkCodelst:"+pkDataQc+",isHide ="+qcIsHide+",description:'"+codeListQcName+"',subType:'"+codeListQcCode+"',comments:'"+codeListQcDesc+"',type:'"+codeListQcModule+"',module:'"+codeListQcModule+"',applock:'"+applock+"'},";
	 	//?BB rowData+= "{pkCodelst:"+pkDataQc+",isHide ="+qcIsHide+",description:'"+codeListQcName+"',subType:'"+codeListQcCode+"',comments:'"+codeListQcDesc+"',type:'stafa',module:'"+codeListQcModule+"',applock:'"+applock+"'},";
	 		}
	 		
	 });

	if(rowData.length <=0){
		return;
		}
	 if(rowData.length >0){
            rowData = rowData.substring(0,(rowData.length-1));
        }
        url="saveCodeListQc";
        //alert(rowData);
	    response = jsonDataCall(url,"jsonData={protocolData:["+rowData+"]}");

	    //$("#panelMaintenanceDiv").replaceWith($('#panelMaintenanceDiv', $(response)));
	    //BB$("select").uniform();
	    /* alert("Data Saved Sucessfully"); */
	  /*   $.blockUI({
            message:"Data Saved Sucessfully",
            centerY: 0,
            css: { top:'40%',background:'#A4A6A5',height:'8%',},
            timeout: 1000

        });  */
        alertbox(save_success_msg,'SUCCESS');
    	constructTable(true,'dataLibraryTable',datafield_ColManager,datafield_serverParam,datafield_aoColumnDef,datafield_aoColumn);
    	 $("#uniform-codeListModule").hide();
    	return true;
} 

function addRows(tableId){
	 //restoreAll('#targetall1 tbody tr:first td');
	 elementcount +=1;
	 //?Mari Autogenerate code and apply dropdown for module
	 //$('#dataLibraryTable tbody[role="alert"]').prepend("<tr><td> <input type='checkbox' id='pkDataQc' name='pkDataQc' value='0'/></td> <td><input type='text' id='codeListQcName' name='codeListQcName' /></td><td><input type='text' id='codeListQcCode' name='codeListQcCode' /></td><td><input type='text' id='codeListQcModule' name='codeListQcModule' /></td><td><input type='text' id='codeListQcDesc' name='codeListQcDesc' /></td><td>N</td></tr>");  
	 //<input type='text' id='codeListQcName' name='codeListQcName' />
	 //<input type='text' id='codeListQcModule' name='codeListQcModule' />
	  var moduleDrop = "<select name='codeListQcModule' id='codeListQcModule'>"+$.trim($("#codeListModule").html())+"</select>";
	//?BB var moduleDrop = "stafa";
	 //alert(moduleDrop);
	 $('#dataLibraryTable tbody[role="alert"]').prepend("<tr><td> <input type='checkbox' id='pkDataQc' name='pkDataQc' value='0'/><input type='hidden' id='applock' name='applock' value='0'/></td> <td><input type='text' id='codeListQcName' name='codeListQcName' /></td><td>&nbsp;</td><td><div>"+moduleDrop+"</div></td><td><input type='text' id='codeListQcDesc' name='codeListQcDesc' /></td><td>N</td></tr>");
	 
	 $("#dataLibraryTable .ColVis_MasterButton").triggerHandler("click");
	 $('.ColVis_Restore:visible').triggerHandler("click");
	 $("div.ColVis_collectionBackground").trigger("click");
	 $.uniform.restore('select');
	 $("select").uniform();
	//?BB $("#uniform-codeListModule").hide(); 
		var param ="{module:'APHERESIS',page:'DATAFIELDLIBRARY'}";
		markValidation(param);
	 
}

function setTableEdit(id){
    var tables=document.getElementById(id);
    var rowsLen = tables.rows.length;
    try{
    for(var i=0;i<rowsLen;i++){

        $(tables.rows[i].cells[2]).click(function(event) {
            if(!(this.getAttribute('textFlag')=="true")){
            insertTextBox(this);
            cellTextFlag=true;
            }
        });

        $(tables.rows[i].cells[3]).click(function(event) {
            if(!(this.getAttribute('textFlag')=="true")){
            insertTextBox(this);
            cellTextFlag=true;
            }
        });

        $(tables.rows[i].cells[4]).click(function(event) {
                if(!(this.getAttribute('textFlag')=="true")){
                insertTextBox(this);
                cellTextFlag=true;
                }
        });
        $(tables.rows[i].cells[5]).click(function(event) {
                if(!(this.getAttribute('textFlag')=="true")){
                insertTextBox(this);
                cellTextFlag=true;
                }
        });

        }
    }catch(e){alert(e);}
}
/*DataField Library Table Server Parameters Starts*/

	var datafield_aoColumn = [ { "bSortable": false},
				               null,
				               null,
				               null,
				               null,
				               null
				               ];
	var datafield_aoColumnDef = [
				                  {	"sTitle":"",
				                	  "aTargets": [ 0], "mDataProp": function ( source, type, val ) { 
				                	 		return "<input type='checkbox' id='pkDataQc' name='pkDataQc' value='"+source[0]+"' /><input type='hidden' id='applock' name='applock' value='"+source[7]+"' />";
				                	 		}},
				                	 		//SELECT PK_CODELST, CODELST_DESC, CODELST_TYPE, CODELST_SUBTYP, CODELST_HIDE, LEVEL,CODELST_COMMENTS
				                  {	"sTitle":"Field Name",
				                	 			"aTargets": [ 1], "mDataProp": function ( source, type, val ) {
				                	  var link = "<a href='#' onclick='javascript:loadMaintenanceDetails(\""+source[0]+"\",\""+source[2]+"\",\""+source[3]+"\",\""+source[4]+"\",\""+source[1]+"\",\""+source[6]+"\",this,\""+source[8]+"\");'>"+source[1]+"</a>";
				                	  		return link;
				                	 		}},
				                {	"sTitle":"Code",
				                	 			"aTargets": [ 2], "mDataProp": function ( source, type, val ) { 
											    return source[3];
											    }},
								  {	"sTitle":"Module",
											    	"aTargets": [ 3], "mDataProp": function ( source, type, val ) { 
										    	return source[8];
										    }},
								  {	"sTitle":"Comments",
										    	"aTargets": [ 4], "mDataProp": function ( source, type, val ) {
									  var comments = "";
									  if(source[6]!=null){
										  comments = source[6];
									  }
									 	   return comments;
								  		}},
								  {	"sTitle":"Deactivate?",
								  			"aTargets": [ 5], "mDataProp": function ( source, type, val ) { 
									  		return source[4];
									  }}
								];
	var datafield_serverParam = function ( aoData ) {
														aoData.push( { "name": "application", "value": "stafa"});
														aoData.push( { "name": "module", "value": "maintenance_datafieldconfig"});	
														
														aoData.push( {"name": "col_1_name", "value": "lower(CODELST_DESC)" } );
														aoData.push( { "name": "col_1_column", "value": "lower(CODELST_DESC)"});
														
														aoData.push( {"name": "col_5_name", "value": "lower(CODELST_HIDE)" } );
														aoData.push( { "name": "col_5_column", "value": "lower(CODELST_HIDE)"});
														
														aoData.push( { "name": "col_2_name", "value": "lower(CODELST_SUBTYP)" } );
														aoData.push( { "name": "col_2_column", "value": "lower(CODELST_SUBTYP)"});
														
														aoData.push( {"name": "col_3_name", "value":"lower(CODELST_MODULE)" } );
														aoData.push( { "name": "col_3_column", "value": "lower(CODELST_MODULE)"});
														
														aoData.push( {"name": "col_4_name", "value":"lower(CODELST_COMMENTS)" } );
														aoData.push( { "name": "col_4_column", "value": "lower(CODELST_COMMENTS)"});
														//SELECT PK_CODELST, CODELST_DESC, CODELST_TYPE, CODELST_SUBTYP, CODELST_HIDE, LEVEL,CODELST_COMMENTS
												};
	var datafield_ColManager = function () {
        $("#dataLibraryColumn").html($('.ColVis:eq(0)'));
	};
/*DataField Library Table Server Parameters Ends*/

$(document).ready(function(){
	hide_slidewidgets();
	hide_slidecontrol();
	clearTracker();
	show_innernorth();
	constructTable(false,'dataLibraryTable',datafield_ColManager,datafield_serverParam,datafield_aoColumnDef,datafield_aoColumn);
	//setColumnManager('dataLibraryTable');
    $(".validvalues").hide();
$("select").uniform();
$("#uniform-codeListModule").hide();

});

function displayResult(id)
{
    //alert(id);
    //alert(rowsLen);
var tables=document.getElementById(id);
var rowsLen = tables.rows.length;
var dataFieldType = $("#dataFieldType").val();
//alert(rowsLen);
//rowsLen=rowsLen-1;
//alert(rowsLen);
var row=tables.insertRow(rowsLen);
var cell0=row.insertCell(0);
var cell1=row.insertCell(1);
var cell2=row.insertCell(2);
var cell3=row.insertCell(3);
var cell4=row.insertCell(4);
var cell5=row.insertCell(5);
var cell6=row.insertCell(6);
var cell7=row.insertCell(7);
$(cell0).attr('style','width:12%');
$(cell1).attr('style','width:32%');
$(cell2).attr('style','width:32%');
$(cell3).attr('style','width:32%');
$(cell4).attr('style','width:32%');
$(cell5).attr('style','width:32%');
$(cell6).attr('style','width:32%');
$(cell7).attr('style','width:32%');

$(cell2).click(function(event) {
    if(!(this.getAttribute('textFlag')=="true")){
    insertTextBox(this);
    cellTextFlag=true;
    }
});
$(cell3).click(function(event) {
    if(!(this.getAttribute('textFlag')=="true")){
    insertTextBox(this);
    cellTextFlag=true;
    }
});

$(cell4).click(function(event) {
        if(!(this.getAttribute('textFlag')=="true")){
        insertTextBox(this);
        cellTextFlag=true;
        }
});
    $(cell5).click(function(event) {
        if(!(this.getAttribute('textFlag')=="true")){
        insertTextBox(this);
        cellTextFlag=true;
        }
});
cell0.innerHTML="&nbsp;";
cell1.innerHTML="<input type='checkbox' class='deleterows' id=''>";
cell2.innerHTML="&nbsp;";
cell3.innerHTML="&nbsp;"
cell4.innerHTML=dataFieldType;
cell5.innerHTML="&nbsp;";
/* $.uniform.restore("select"); */
cell6.innerHTML="<div class='smallSelector'><select id='defID'><option>No</option><option>Yes</option></select></div>";
cell7.innerHTML="<div class='smallSelector'><select id='defID'><option>No</option><option>Yes</option></select></div>";
$("select").uniform();
$("#uniform-codeListModule").hide();
cell0.setAttribute('class','hidden');
cell1.setAttribute('textFlag',false);
cell2.setAttribute('textFlag',false);
cell3.setAttribute('textFlag',false);
cell4.setAttribute('textFlag',true);
cell5.setAttribute('textFlag',false);
cell6.setAttribute('textFlag',false);
cell7.setAttribute('textFlag',false);
}
// additives data
var oldCellObj;
var textCellFlag=false;
function insertTextBox(cellObj){
    //alert(cellObj.getAttribute('textFlag'))

    if(oldCellObj){
        ///alert("11-->"+oldCellObj.getAttribute('textFlag'));
        }
    if(cellObj.getAttribute('textFlag')=="false" && oldCellObj && (oldCellObj.getAttribute('textFlag')=="true")){
        //alert("-->"+cellObj.getAttribute('textFlag'))
        var tempValue = $("#tempId").val();
        //alert("-->tempValue"+tempValue)
        cellObj.innerHTML="<input type='text' id='tempId' style='width:95%;padding-left:1%;'  onblur='setCellValue(this)' value='"+cellObj.innerHTML+"'/>";
        cellObj.setAttribute('textFlag',true);
        oldCellObj.innerHTML=tempValue;
        oldCellObj.setAttribute('textFlag',false);
        oldCellObj=cellObj;
        textCellFlag=true;
        /*$('#tempId').blur(function() {
            setCellValue(this);
            });*/
        $('#tempId').focus();
        return;
    }

    if(cellObj.getAttribute('textFlag')=="false"){
        //alert("=====>"+cellObj.getAttribute('textFlag'))

        cellObj.innerHTML="<input type='text' id='tempId'style='width:99%;padding-left:2%;'  onblur='setCellValue(this)' value='"+cellObj.innerHTML+"'/>";
        cellObj.setAttribute('textFlag',true);
        /*$('#tempId').blur(function() {
              setCellValue(this);
            });*/
        oldCellObj=cellObj;
        textCellFlag=true;
        $('#tempId').focus();
    }
}

function setCellValue(This){
    //alert(This.value);
    //alert(oldCellObj.innerHTML);

    oldCellObj.innerHTML=This.value;
    oldCellObj.setAttribute('textFlag',false);
    oldCellObj = null;
}




function loadMaintenanceDetails(pkId,type,subType,isDeactivate,Desc,comments,This,module){
    //alert(Desc);
    var flag=$(This).attr("class");
    $(This).removeClass(flag);
    $(This).addClass("highlighting");
    $(This).attr("id",flag);
    $("#portletsubtitlediv").html(Desc);
    var codelstparam = ["subType","description","pageUrl","isHide","maintainable","pkCodelst","type","defaultValue","codelstvalue","sequence"];
    //alert(pkId+"--"+type+"--"+subType+"--"+isDeactivate+"->-"+Desc);
    var maintenable= "";
    $('.progress-indicator').css( 'display', 'block' );
     $.ajax({
            type: "GET",
            url : "loadMaintenance.action?type="+subType,
            async:false,
            dataType: 'json',

            success: function (result){
                var treelst = result.mainTreelst;
                $("#fieldNameID").val(Desc);
                if(isDeactivate=='Y'){
                        $("#deActivateNo").attr('checked',false);
                        $("#deActivateYes").attr('checked',true);
                    }else if(isDeactivate=='N'){
                        $("#deActivateYes").attr('checked',false);
                        $("#deActivateNo").attr('checked',true);
                        }
                var constructHTML="<thead><tr><th class='hidden'></th><th>Check All<br/><input type='checkbox'id='deleteallchild' name='ValidName' onclick='checkall()'></th><th>value</th><th>Value Code</th>";
                constructHTML+="<th>Code Source</th><th>Sequence</th><th>Default</th><th>DeActivate</th></tr></thead>";
                $.uniform.restore('select');
                //alert(result.mainTreelst);
                $.each(result.mainTreelst, function(i) {
                    //var codelstparam = ["subType","description","pageUrl","isHide","maintainable","pkCodelst","type","defaultValue","codelstvalue"];
                    var maintlink = 'javascript:loadcalist("'+eval("this."+codelstparam[0])+'","Y","'+eval("this."+codelstparam[1])+'");';
                    constructHTML += "<tr id="+i+" ><td class='hidden'>"+eval("this."+codelstparam[5])+"</td><td><input type='checkbox'class='deleterows' name='ValidName'></td>";
                    constructHTML += "<td textFlag='false'>"+eval("this."+codelstparam[1])+"</td><td textFlag='false'>"+eval("this."+codelstparam[0])+"</td><td textFlag='false'>"+eval("this."+codelstparam[6])+"</td>";
                    constructHTML += "<td textFlag='false'>"+eval("this."+codelstparam[9])+"</td>";
                    //IS Default
                    if(eval("this."+codelstparam[7]) == "1"){
                        constructHTML += "<td><div class='smallSelector'><select name='isDefault' id='defID'><option selected='true'>Yes</option><option>No</option></select></div></td>";
                    }else{
                        constructHTML += "<td><div class='smallSelector'><select name='isDefault' id='defID'><option>Yes</option><option selected='true'>No</option></select></div></td>";
                    }
                    //DeActive
                    if(eval("this."+codelstparam[3]) == "Y"){
                        constructHTML += "<td><div class='smallSelector'><select name='isDeActive' id='isDeActive'><option selected='true'>Yes</option><option>No</option></select></div></td>";
                    }else{
                        constructHTML += "<td><div class='smallSelector'><select name='isDeActive' id='isDeActive'><option>Yes</option><option selected='true'>No</option></select></div></td>";
                    }
                    constructHTML += "</tr>";

                });
                //alert(constructHTML);
                $("#dataFieldLibValueTable").html("");
                 $("#dataFieldLibValueTable").append(constructHTML);
                 setTableEdit("dataFieldLibValueTable");
                }
     });
    $(".validvalues").show();
    $("#addDataField #dataFieldType").val(subType);
    $("#addDataField #dataFieldModule").val(module);
    //$.uniform.restore('select');
    $("select").uniform();
    $("#uniform-codeListModule").hide();
    $('.progress-indicator').css( 'display', 'none' );

    showPopUp("open","addDataField",$("#portletsubtitlediv").html(),"1130","660");

}

function closeDataFiledPopup(){
    jQuery("#addDataField").dialog("close");
    /* if($("#dataLibraryTbody").children().hasClass("highlighting")){
        var id=$(this).attr("id");
        alert(id);
    } */
    hideHighting('dataLibraryTbody');
}
function checkall() {
    var isChecked = $('#deleteallchild').attr('checked');
     if(isChecked)
         {
     $('.deleterows').attr('checked', true);
         }
     else
         {
         $('.deleterows').attr('checked', false);
         }
 }

function saveDataFiledValues(){
	//alert("BB save");
    //alert(tableToJson(document.getElementById('dataFieldLibValueTable')));
    //var codelstparam = ["subType","description","pageUrl","isHide","maintainable","pkCodelst","type","defaultValue","codelstvalue"];
    var columns = ["pkCodelst","checkBox","description","subType","type","sequence","defaultValue1","isHide1"];
    var parentData="{'fieldType':'"+$("#fieldType").val()+ "','subtype':'"+ $("#addDatafieldForm #dataFieldType").val() + "','module':'" +  $("#addDatafieldForm #dataFieldModule").val() +"'}";
    	
   
    var obj = getJsonFromHtmlTable(document.getElementById('dataFieldLibValueTable'),columns);
    var jsonData = "{panelData:" + JSON.stringify(obj) +",parentData:" +parentData +"}";

    $.ajax({
        type: "POST",
        url: "saveDataFiledValues.action",
        async:false,
        data :{jsonData:jsonData},
        dataType: 'json',
        success: function (result){
            //$('#recipientAlias').dialog('close');
            //jQuery("#"+Id).dialog("close");
            /* alert("Saved"); */
            alertbox('saved');
        }
    });
    jQuery("#addDataField").dialog("close");
    //alert(tableToJson($('#dataFieldLibValueTable')));
    hideHighting('dataLibraryTbody');
}
</script>
<div class="cleaner"></div>

<form>
<span class="hidden">
<s:select name="codeListModule" id="codeListModule" list="moduleList" headerKey="" headerValue="Select" listKey="subType" listValue="description"/>
</span>
<div class="column">
<div  class="portlet">
<div class="portlet-header"><s:text name="stafa.label.datafieldlibrary"/></div>
<div class="portlet-content">
	<table>
		<tr>
			<td style="width:20%"><div onclick="addRows(0,'','','',0,false);"><img src = "images/icons/addnew.jpg" class="cursor"  />&nbsp;&nbsp;<label id="adchild" class="cursor"><b>Add</b></label>&nbsp;</div></td>
			<td style="width:20%"><div><img src = "images/icons/no.png" style="width:16;height:16;" class="cursor" id ="" onclick="deleteRows();"/>&nbsp;&nbsp;<label class="cursor"  onclick="deleteRows();"><b>Delete</b></label>&nbsp;</div></td>
			<td style="width:20%"><div><img src = "images/icons/edit.png" style="width:16;height:16;" class="cursor" id ="" onclick="editRows();"/>&nbsp;&nbsp;<label id="dchild" class="cursor" onclick="editRows();"><b>Edit</b></label>&nbsp;</div></td>
			<!-- <td style="width:20%"><div><img src = "images/icons/save3.png" style="width:16;height:16;" class="cursor" id ="" onclick="saveDataFiled();" >&nbsp;&nbsp;<label id="schild" class="cursor" onclick="saveDataFiled();"><b>Save</b></label>&nbsp;</div></td> -->
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" id="dataLibraryTable" class=display>
	<thead>
         <tr>
            	<th  width="4%" id="dataLibraryColumn"></th>
				<th colspan="1"><s:text name="stafa.label.fieldname"/></th>
				<th colspan="1"><s:text name="stafa.label.code"/></th>
				<th colspan="1"><s:text name="stafa.label.module"/></th>
				<th colspan="1"><s:text name="stafa.label.comment"/></th>
				<th colspan="1"><s:text name="stafa.label.deactivate"/></th>
		</tr>
	</thead>
</table>
</div>
</div>
</div>
</form>

<!-- NEW POP FOR DATAFIELD CONFIG -->

<div class="portlet-header" id="addDataField"  style="display:none"><span id="portletsubtitlediv" style="display:none"></span>
<hr>
		<form id="addDatafieldForm">
		<s:hidden name="dataFieldModule" id="dataFieldModule" />
		<s:hidden name="dataFieldType" id="dataFieldType"/>
				<table border="0">
					<tr>
						<td style="font-weight:bold"><s:text name="stafa.label.datafielddetails"/></td>
					</tr>
				</table>
			<div class="addContentTable">
				<table width="98%" cellpadding="0" cellspacing="0" border="0" style="border-radius:10px;">
					<tr>
						<td style="padding:0% 0% 0% 2%"><s:text name="stafa.label.fieldname"/></td>
						<td style="padding:2% 0% 0% 2%"><input type="text" id="fieldNameID" name="fieldNameID"/></td>
						<td ><s:text name="stafa.label.deactivate"/></td>
						<td><input name="deActivate" type="Radio" id="deActivateYes"/> Yes <input type="Radio" name="deActivate"  id="deActivateNo"/> No
					</tr>
				</table>
			<div class="Scrolll" style="height:90%;overflow:auto;">
				<table>
				
					<tr>
					<td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td>
					</tr>
					<tr>
					<td></td>
					<td></td>
					<td> <img src = "images/icons/addnew.jpg">&nbsp;&nbsp;<img src = "images/icons/mini_del.png"></td>
					</tr>
					<tr>
						<td style="padding:0% 0% 0% 2%"> <s:text name="stafa.label.fieldcode"/></td>
						<td style="padding:0% 0% 0% 2%">
						<table cellpadding="0" cellspacing="0" border="1" id="fieldCodeID" class="createchilddisplay" width="115%">
						<thead>
						<tr>
						<th></th>
						<th><s:text name="stafa.label.code" /></th>
						<th><s:text name="stafa.label.codeSource"/></th>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td><input type="checkbox" name="fieldCodeName"></td>
						<td>abc</td>
						<td>Stafa</td>
						</tr>
						</tbody>
						</table>
						</td>
					</tr>
				<tr><td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td></tr>
				<tr>
					<td></td>
					<td></td>
					<td> <img src = "images/icons/addnew.jpg">&nbsp;&nbsp;<img src = "images/icons/mini_del.png"></td>
				</tr>
				<tr>
				<td style="padding:0% 0% 0% 2%"><s:text name="stafa.label.fieldid"/></td>
				<td style="padding:0% 0% 0% 2%">
				<table cellpadding="0" cellspacing="0" border="1" id="fieldID" class="createchilddisplay" width="115%">
				<thead>
				<tr>
				<th></th>
				<th><s:text name="stafa.label.id"/></th>
				<th><s:text name="stafa.label.idSource"/></th>
				</tr>
				</thead>
				<tbody>
				<tr>
				<td><input type="checkbox" name="FieldIDName"></td>
				<td>123</td>
				<td>Stafa</td>
				</tr>
				</tbody>
				</table>
				</td>
				</tr>
				<tr>
				<td style="padding:0% 0% 0% 2%"><br><s:text name="stafa.label.fielddescription"/></td>
				<td style="padding:0% 0% 0% 2%"> <br><textarea rows="3" cols="40" id="fielddesc"></textarea></td>
				</tr>
				<tr>
				<td style="padding:0% 0% 0% 2%"><br><s:text name="stafa.label.fieldtype"/></td>
				<td style="padding:0% 0% 0% 2%"><br> <select id="fieldType" name="fieldType" class="fieldType"  style=\"width:50%\"  onchange = "showboxes(this)"><option value="" >Select</option><option value="TextBOX" >TextBox</option><option value="DROPDOWN">DropDown</option><option value="RADIO">RadioButton</option><option value="CHECKBOX">CheckBox</option></select></td>
				</tr>
				</table>

			</div>
		<br></div>
		<div class="addContentTable validValues" style="margin-top:-1%;">
		<table width="100%">
				<tr>
					<td rowspan="4">Valid Values</td>
					<td colspan="9">
					<div align="right">
					<img src = "images/icons/addnew.jpg" onclick="displayResult('dataFieldLibValueTable')">&nbsp;&nbsp;<img src = "images/icons/mini_del.png"  onclick="deletechildrows('dataFieldLibValueTable')">
					</div>
					</td>
					</tr>
				<tr>
				
					<td colspan="9" width="88%">
					 <table width="100%" cellpadding="0" cellspacing="0" class="createchilddisplay" border="1" id="dataFieldLibValueTable">
								
					</table>
					</td>
		      </tr>
		</table>
		</div>
		<table width="95%">
		 <tr ><td align="right"><input type="button" name="Save" onclick="saveDataFiledValues();" value="Save"/>&nbsp;<input type="button" name="Cancel" value="Cancel" onclick="closeDataFiledPopup()"/></td></tr>
		 </table>
		</form>
</div>

<!-- END OF DATAFIELD CONFIG -->






<script>

function deletechildrows(id) {
    var deleteDataFieldIds = "";
    var deLim = "";
    try {
           var table = document.getElementById(id);
           var isChecked = $('#'+id).find('tr').length;
          var isCheckedall = $('#deleteallchild').attr('checked');
           if(isChecked>1 && $('.deleterows').is(':checked')){
           if(isCheckedall){
               var yes=confirm('Do you want delete all products ?');
           }
           else{var yes=confirm('Do you want delete selected products?');
           }
           var rowCount = table.rows.length;
        if(yes){
        for(var i=1; i<rowCount; i++) {
            var row = table.rows[i];
            var cbox = row.cells[1].childNodes[0];
            if(null != cbox && true == cbox.checked) {
                deleteDataFieldIds+=deLim+row.cells[0].innerHTML;
                table.deleteRow(i);
                rowCount--;
                i--;
                deLim=",";
                }

            }
        }
    }
         rowCount = table.rows.length;

    //To delete Data Field in the Database

    deleteDataField(deleteDataFieldIds);

    }catch(e) {
        alert(e);
    }
    $('#deleteallchild').attr('checked', false);
    }

//To delete Data Field in the Database
function deleteDataField(deleteDataFieldIds){
    var url = "deleteDataField";
    jsonData = "deleteDataFieldIds="+deleteDataFieldIds;
    ajaxresult = jsonDataCall(url,jsonData);

}

function  showboxes(This){
var ans=$(This).val();

if((ans=="DropDown")||(ans=="RadioButton")||(ans=="CheckBox")){
    $(".validvalues").show();
}else
    {
    $(".validvalues").hide();
    }
    $(".validvalues").focus();
}
$(function(){
    $( ".column" ).sortable({
        connectWith: ".column"
            /*,
        handle: '.portlet-header',
        cursor: 'move',
        forcePlaceholderSize: true*/

    });

    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");



    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".addnew" )
    .append("<span style=\"float:right;\" class='ui-addnew'></span>");

    $( ".portlet-header .ui-addNew " ).click(function() {
        //showPopUp("open","addDataField","Add New Data Field","900","660");
    	addRows();
    });

    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });


});


var now = new Date();

var divHtml = "" + now.format("dddd, mmmm dS, yyyy ") ;

var lastLog = "" + now.format("h:MM:ss ");

var userDetails = lastLog;


$('#arrivalDate').html(lastLog);



/**************For Time Zone Start****************************/
 var offset = (new Date()).getTimezoneOffset();

 var timezones = {
 '-12': 'PST',
 '-11': 'PST',
 '-10': 'PST',
 '-9': 'CST',
 '-8': 'CST',
 '-7': 'CST',
 '-6': 'CST',
 '-5': 'CST',
 '-4': 'CST',
 '-3.5': 'America/St_Johns',
 '-3': 'America/Argentina/Buenos_Aires',
 '-2': 'Atlantic/Azores',
 '-1': 'Atlantic/Azores',
 '0': 'Europe/London',
 '1': 'Europe/Paris',
 '2': 'Europe/Helsinki',
 '3': 'Europe/Moscow',
 '3.5': 'Asia/Tehran',
 '4': 'Asia/Baku',
 '4.5': 'Asia/Kabul',
 '5': 'Asia/Karachi',
 '5.5': 'IST',
 '6': 'Asia/Colombo',
 '7': 'Asia/Bangkok',
 '8': 'Asia/Singapore',
 '9': 'Asia/Tokyo',
 '9.5': 'Australia/Darwin',
 '10': 'PST',
 '11': 'Asia/Magadan',
 '12': 'Asia/Kamchatka'
};
$("#timeZone").html(timezones[-offset / 60]);

</script>



