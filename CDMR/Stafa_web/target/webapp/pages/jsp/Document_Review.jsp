<%@ include file="common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/stafaQC.js"></script>

<link type="text/css" href="css/floatmenu.css" rel="stylesheet" />


<script language="javascript">
$(document).ready(function() {
	//alert("hi");
	//getProcessingData();
 
	oTable = $('#equipmentsTable').dataTable({
		"bJQueryUI": true,
		 "bRetrieve": true,
		 
		//"sPaginationType": "full_numbers"
	});
	oTable = $('#documentConformation').dataTable({
													"bJQueryUI": true,
													"bRetrieve": true,
													"bPaginate":true,
													"sDom": 'C<"clear">Rlfrtip',
													"aoColumns": [ { "bSortable":false},
			 										               null,
			 										               null,
			 										               null,
			 										               null
			 										             ],
			 										"oColVis": {
			 											"aiExclude": [ 0 ],
			 											"buttonText": "&nbsp;",
			 											"bRestore": true,
			 											"sAlign": "left"
			 										},
			 										"fnInitComplete": function () {
			 									        $("#documentColumn").html($('.ColVis:eq(0)'));
			 										}
													//"sPaginationType": "full_numbers"
	});
	oTable.thDatasorting('documentConformation');
	
	var url = "getQc";
	var response = ajaxCall(url,"processingForm");
	generateTableFromJsnList(response);
	
	
	
	

	// check with Mari
	/*
	document.getElementById('equipmentsTable_next').setAttribute("onClick","removeTableClass()");
	document.getElementById('equipmentsTable_previous').setAttribute("onClick","removeTableClass()");
	document.getElementById('reagentsTable_next').setAttribute("onClick","removeTableClass()");
	document.getElementById('reagentsTable_previous').setAttribute("onClick","removeTableClass()");
	
	removeTableClass();
	
	
	//removeTableClassOnChange("equipmentsTable_length");
	//removeTableClassOnChange("reagentsTable_length");
	*/

	// calculations
	var rbcv = round2Decimal((Number($("#tmptotalVolume").val()) * Number($("#tmphct").val()))/100); 
	var maxPVR = Number($("#tmptotalVolume").val() - Number(rbcv));
	var minPVR = round2Decimal(Number(maxPVR)*0.8);
	$("#maxPlasma").val(maxPVR);
	$("#minPlasma").val(minPVR);
	
	
});



function generateTableFromJsnList(response){
	var arr = [];
	var pkCodeList = [];
	$.each(response.qcList, function(i,v) {
		pkCodeList.push(v[0]);
		arr.push(v[3]);		
	 });
	var tab="<table style='width:100%'><tr>"
		 $.each(arr, function(i,val){
		var k=i+1;
		if((i%4==0)&&(i!=0)){
					
					tab+="</tr><tr><td>";	
					tab+="<input type='checkbox'  name='ABO' id='"+pkCodeList[i]+"' onclick=\"javascript:qcCalculation('processingForm');\">&nbsp;<a href='#' onclick=\"javascript:showQcDetails('"+pkCodeList[i]+"','"+arr[i]+"')\">"+arr[i]+"</a></td>";
			
		}
			 else{
				 
					tab+="<td>";			
					tab+="<input type='checkbox'  name='ABO' id='"+pkCodeList[i]+"' onclick=\"javascript:qcCalculation('processingForm');\">&nbsp;<a href='#' onclick=\"javascript:showQcDetails('"+pkCodeList[i]+"','"+arr[i]+"')\">"+arr[i]+"</a></td>";
		
			 }

		 });
	$("#qcContent").html(tab);
}

function generateTableFromJsnList1(response){
	$.each(response.recipientDonorList, function(i,v) {
		//alert(i+"=-="+v[0]);
		$("#recipientName").html(v[0]);
		$("#recipientId").html(v[1]);
		$("#recipientDiagnosis").html(v[2]);
		$("#recipientAboRh").html(v[3]);
		$("#donorName").html(v[4]);
		$("#donorId").html(v[5]);
		$("#donorAboRh").html(v[6]);
		$("#productId").html(v[7]);
		$("#productType").html(v[9]);
		$("#fkProtocol").val(v[11]);
	 	});
	//var qcCheckList = [];
	var qcCheckList = Array(2); 
	$.each(response.protocolQcList, function(i,v) {
		qcCheckList.push(v[2])
	 	});
 	
	var chk_arr = $("#qcContent input[type=checkbox]");
	var chklength = chk_arr.length;             
	//alert("qcCheckList : "+qcCheckList);
	for(k=0;k< chklength;k++)
	{	

		var id = $(chk_arr[k]).attr("id");
		//alert("id : "+id);
		if (Search_Array(qcCheckList, id)){
			//alert("Found : "+id);
			chk_arr[k].checked = true;
			$(chk_arr[k]).trigger('click');
			$("#"+id).attr("checked","checked");
			}
			else{
				//alert("Not Found");
				chk_arr[k].checked = false;
			}
	} 
	var mappedProtocol = $("#fkProtocol").val();
	//alert("mappedProtocol "+mappedProtocol);
	getProtocolCalculation(mappedProtocol,"calculationDiv");
}
function Search_Array(ArrayObj, SearchFor){
	  var Found = false;
	  //alert(ArrayObj+"--"+SearchFor);
	  for (var i = 0; i < ArrayObj.length; i++){
	    if (ArrayObj[i] == SearchFor){
	      return true;
	      var Found = true;
	      break;
	    }
	    else if ((i == (ArrayObj.length - 1)) && (!Found)){
	      if (ArrayObj[i] != SearchFor){
	        return false;
	      }
	    }
	  }
	}
$(function(){
    $("select").uniform();
  });




function getProcessingData(e){
	var url = "getOriginalProductData";
	var productSearch = $("#productSearch").val();
	if(e.keyCode==13 && productSearch!=""){
		//alert("s : "+e.keyCode);
		var url = "getOriginalProductData";
		var response = ajaxCall(url,"processingForm");
		generateTableFromJsnList1(response);
	}
}



/*function getProcessingData(){
	//alert("s11111111");
	var url = "getOriginalProductData";
	var productSearch = $("#productSearch").val();
		var response = ajaxCall(url,"processingForm");
		generateTableFromJsnList1(response);
}*/
function removeTableClass(){

	$('#equipmentsTable_next').removeClass("ui-state-default");
	$('#equipmentsTable_previous').removeClass("ui-state-default");
	$('#reagentsTable_next').removeClass("ui-state-default");
	$('#reagentsTable_previous').removeClass("ui-state-default");

}

</script>

<div class="cleaner"></div>

<!--float window-->

<div id="floatMenu">
<div id="forfloat_wind">
	<div id="box-outer">
		
		<a href="#" style="text-decoration:none" id="blind">&lt;&lt; </a>
			<div id="box">
				<div id="leftnav" >
				
<!-- Patient start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header"><s:hidden id="moduleName" value=""/><s:hidden id="widgetName" value="patient"/><s:hidden id="pkNotes" value=""/><%-- <s:text name="stafa.label.patientinfo"/> --%>Patient Info</div>
						<div class="portlet-content">
						<table width="225" border="0">
						  <tr>
							<td width="25"><%-- <s:text name="stafa.label.id"/> --%> ID</td>
							<td width="135"><div id="patientId">MDA12983483</div></td>
						  </tr>
						  <tr>
							<td><%-- <s:text name="stafa.label.name"/> --%>Name</td>
							<td><div id="PatientName"></div>Mare Lee</td>
						  </tr>
						  <tr>
						  	<td><%-- <s:text name="stafa.label.dob"/> --%>DOB</td>
						  	<td><div id="patientDob"></div>Feb 15 1963</td>
						  </tr>
						  <tr>
							<td><%-- <s:text name="stafa.label.abo"/> --%>ABO/Rh</td>
							<td><div id="patientAboRh">O</div></td>
						  </tr>
						  <tr>
							<td><%-- <s:text name="stafa.label.procedure"/> --%>Procedure</td>
							<td><div id="procedure"></div>ApheresisDay2</td>
						  </tr>
						  <tr>
							<td><%-- <s:text name="stafa.label.donation"/> --%>Donation</td>
							<td><div id="donation"></div>AUTO</td>
						  </tr>
						  <tr>
							<td><%-- <s:text name="stafa.label.bsa"/> --%>BSA</td>
							<td><div id="bsa"></div>4M^2</td>
						  </tr>
						  <tr>
							<td><%-- <s:text name="stafa.label.bv"/> --%>BV</td>
							<td><div id="bv"></div> 3l</td>
						  </tr>
						</table>

					</div>
					</div>
					</div>
				
<!-- recipient-->
<!-- Lab Results-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header"><s:hidden id="moduleName" value=""/><s:hidden id="widgetName" value="Lab"/><s:hidden id="pkNotes" value=""/><%-- <s:text name="stafa.label.lab"/> --%>Lab Results</div>
						<div class="portlet-content">
						<table width="225" border="0">
							<tr>
							<td width="89"><%-- <s:text name="stafa.label.cd34Day1"/> --%>CD34 Day1</td>
							<td width="120"><div id="cd3day1">1.5</div></td>
						  </tr>
						  <tr>
							<td><%-- <s:text name="stafa.label.cum.cd34"/> --%>CUM.CD34</td>
							<td><div id="comdate"></div>1.5</td>
						  </tr>
						</table>						
					</div>
				</div>
				</div>
				
<!-- Product-->

<!-- Recipient info-->
					<div class="column">
					<div class="portlet">
						<div class="portlet-header"><s:hidden id="moduleName" value=""/><s:hidden id="widgetName" value="recipient"/><s:hidden id="pkNotes" value=""/><%-- <s:text name="stafa.label.recipient"/> --%>Recipient Info</div>
						<div class="portlet-content">
							<table width="225" border="0">
						  <tr>
							<td width="25"><%-- <s:text name="stafa.label.id"/> --%> ID</td>
							<td width="135"><div id="patientId">MDA12983483</div></td>
						  </tr>
						  <tr>
							<td><%-- <s:text name="stafa.label.name"/> --%>Name</td>
							<td><div id="PatientName"></div>Mare Lee</td>
						  </tr>
						  <tr>
						  	<td><%-- <s:text name="stafa.label.dob"/> --%>DOB</td>
						  	<td><div id="patientDob"></div>Feb 15 1963</td>
						  </tr>
						  <tr>
							<td><%-- <s:text name="stafa.label.abo"/> --%>ABO/Rh</td>
							<td><div id="patientAboRh">O</div></td>
						  </tr>
						  <tr>
							<td><%-- <s:text name="stafa.label.procedure"/> --%>Diagnosis</td>
							<td><div id="procedure">Leukemia</div></td>
						  </tr>
						</table>
						</div>
					</div>
					</div>
				</div>
				</div>
			
			</div>
		</div>

</div><!--float window end-->

<!--right window start -->	
<div id="forfloat_right">
	<div id="" class="floatright_left">
<div class="column">		
		<div  class="portlet">
			<div class="portlet-header"><s:text name="stafa.label.barcodescanning"/></div><br>
		<div class="portlet-content">
		<form action="#" id="">
	<%-- 			<div id="table_inner_wrapper">
					<div id="left_table_wrapper">
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
							<tr>
								<td width="52%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.processingstation"/></div></td>
								<td width="48%" height="40px"><div align="left" class="mainselection"><select name="" id="" class=""><option>Select</option><option>Scan/Enter ID</option></select></div></td>
							</tr>
							<tr>
								<td width="52%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.procedure"/></div></td>
								<td width="48%" height="40px"><div align="left" class="mainselection"><select name="" id="" class=""><option>Select</option><option>Vol.Red.</option></select></div></td>
							</tr>							
						</table>
					</div>
					
					<div id="right_table_wrapper">
						<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
							<tr>
								<td width="52%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.processinghood"/></div></td>
								<td width="48%" height="40px"><div align="left" class="mainselection"><select name="" id="" class=""><option>Select</option><option>Scan/Enter ID</option></select></div></td>
							</tr>
							<tr>
								<td width="52%" height="40px"><div class="txt_align_label" align="left"><s:text name="stafa.label.method"/></div></td>
								<td width="48%" height="40px"><div align="left" class="mainselection"><select name="" id="" class=""><option>Select</option><option>COBE 2991</option></select></div></td>
							</tr>							
						</table>						
					</div>
				</div> --%>
				<div id="barcodeScanning">
					<table cellpadding="0" cellspacing="0" class="" align="center" border="1" width="100%">
						<tr>
							<td><div class="txt_align_label" style="font-weight:bold;"><s:text name="stafa.label.patientid"/></div></td>
							<td><div align="center" style="padding:1%;"><input type="text" id="patentID" name="patentID" placeholder="Scan/Enter Product ID"/></div></td>
							<td><div align="center"><input type='checkbox'  id="patientVerified" onclick="">Verified</td></div>
						</tr>
						<tr>
							<td><div class="txt_align_label" style="font-weight:bold;"><s:text name="stafa.label.nurseid"/></div></td>
							<td><div align="center" style="padding:1%;"><input type="text" id="nurseID" name="nurseID" placeholder="Scan/Enter Nurse ID"/></div></td>
							<td><div align="center"><input type='checkbox'  id="nurseVerified" onclick="">Verified</td></div>
						</tr>
						<tr>
							<td><div class="txt_align_label" style="font-weight:bold;"><s:text name="stafa.label.bedid"/></div></td>
							<td><div align="center" style="padding:1%;"><input type="text" id="bedID" name="bedID" placeholder="Scan/Enter Bed ID"/></div></td>
							<td><div align="center"><input type='checkbox'  id="bedVerified" onclick="">Verified</td></div>
						</tr>
					</table>
					<br><br>
					<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
						<tr>
							<td><div  style="font-weight:bold;"><s:text name="stafa.label.machine"/></div></td>
						</tr>
						<tr>
						<td><div class="txt_align_label" style="font-weight:bold;"><s:text name="stafa.label.machineid"/></div></td>
						<td><div align="center" style="padding:1%;"><input type="text" id="machineID" name="machineID" placeholder="Scan/Enter Machine ID"/></div></td>
						<td><div class="txt_align_label" style="font-weight:bold;"><s:text name="stafa.label.machinename"/></div></td>
						<td><div align="center" style="padding:1%;"><input type="text" id="machineName" name="machineName"/></div></td>
						</tr>	
					</table>
				</div>
				
				
				</form>			
			</div>
		</div>
	</div>	
	
<div class="cleaner"></div>

 <div class="cleaner"></div>
 <form>
 <div class="column">
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value=""/><s:hidden id="widgetName" value="donorconformation"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.donorconformation"/></div>
			<div class="portlet-content">
			<div>
			<table>
				
				<tr>
					<td>
							<img onclick="addtxt();" style="width:16;height:16;cursor:pointer;" src="images/icons/addnew.jpg">
							<label id=""><b>Add Document</b></label>
					</td>
												
				</tr>
			</table>
			</div>
			<div id="documentConformationdiv">
			<table cellpadding="0" cellspacing="0" border="0" id="documentConformation" class="display">
					<thead>
						<tr>
							<th width="4%" id="documentColumn"></th>
							<th colspan="1"><s:text name="stafa.label.document_title"/></th>
							<th colspan="1"><s:text name="stafa.label.issue_date"/></th>
							<th colspan="1"><s:text name="stafa.label.attachment"/></th>
							<th colspan="1"><s:text name="stafa.label.reviewed"/></th>
						</tr>
						<tr>
							<th width="4%" id="documentColumn"></th>
							<th><s:text name="stafa.label.document_title"/></th>
							<th><s:text name="stafa.label.issue_date"/></th>
							<th><s:text name="stafa.label.attachment"/></th>
							<th><s:text name="stafa.label.reviewed"/></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td></td>
							<td> Donor Certification</td>
							<td> Jul 10, 2012</td>
							<td><img src="images/icons/Black_Paperclip.jpg"></td>
							<td><input type="checkbox"></td>
						</tr>					
						<tr>
							<td></td>
							<td> Donor Questionnaire</td>
							<td> Jul 9, 2012</td>
							<td><img src="images/icons/Black_Paperclip.jpg"></td>
							<td><input type="checkbox"></td>
						</tr>
						<tr>
							<td></td>
							<td> Donor Medical History</td>
							<td> Jul 9, 2012</td>
							<td><img src="images/icons/Black_Paperclip.jpg"></td>
							<td><input type="checkbox"></td>
						</tr>
						<tr>
							<td></td>
							<td> Informed  Conset</td>
							<td> Jul 9, 2012</td>
							<td><img src="images/icons/Black_Paperclip.jpg"></td>
							<td><input type="checkbox"></td>
						</tr>
						<tr>
							<td></td>
							<td> <a href="#">Certificate of Analysis Day2</a></td>
							<td> Aug 8, 2012</td>
							<td></td>	
							<td><input type="checkbox"></td>
						</tr>
					</tbody>
			</table>
			</div> 
	</div>
	</div>	
</div>		
</form>		
</div>
<div class="floatright_right">
<jsp:include page="sidecontrols.jsp"></jsp:include>
 </div>
<!-- popup starts -->
	<div id="transfer" title="addNew" style="display:none;">
		<form id="">
			<table border="0" >
				<tr>
					<td  style="font-weight:bold"><s:text name="stafa.label.pre_collection_therapy" /></td>
				</tr>
				<tr>
					<td></td>
				</tr>
				<tr>
					<td><img onclick="addrows_therapy('pre-collectionTherapy')" style="width:16;height:16;cursor:pointer;" src="images/icons/addnew.jpg">
							<label id=""><b>Add Medication</b></label>
					</td><td> <img onclick="addtxt();" style="width:16;height:16;cursor:pointer;" src="images/icons/no.png">
							<label id=""><b>Delete</b></label>
					</td>
				</tr>
			</table>
			 <table cellpadding="0" cellspacing="0" border="0" id="pre-collectionTherapy" class="display createchilddisplay" >
			 	<thead>
			 		<tr>
			 			<th>Thearpy Name</th>
			 			<th>Volume</th>
			 			<th>Dosage</th>
			 			<th>Date of Therapy</th>
			 			<th>Time of Therapy</th>
			 			<th>Reason for Therapy</th>
			 		</tr>	
			 		<tr>
			 			<th></th>
			 			<th></th>
			 			<th></th>
			 			<th></th>
			 			<th></th>
			 			<th></th>
			 		</tr>		 		
			 	</thead>
			 	<tbody>
			 	</tbody>
			 
			 </table>
			 <br>
			 <div style="height:14px;width:2%;"></div>
			 <table border="0">
				<tr>
					<td  style="font-weight:bold"><s:text name="stafa.label.medication" /></td>
				</tr>
				<tr>
					<td></td>
				</tr>
				<tr>
					<td><img onclick="addrows_therapy('medicationTable')" style="width:16;height:16;cursor:pointer;" src="images/icons/addnew.jpg">
							<label id=""><b>Add Medication</b></label>
					</td><td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img onclick="addtxt();" style="width:16;height:16;cursor:pointer;" src="images/icons/no.png">
							<label id=""><b>Delete</b></label>
					</td>
				</tr>
			</table>
			 <table cellpadding="0" cellspacing="0" border="0" id="medicationTable" class="display createchilddisplay" height="auto">
			 	<thead>
			 		<tr>
			 			<th>Medication Name</th>
			 			<th>Volume</th>
			 			<th>Dosage</th>
			 			<th>Date of Medication</th>
			 			<th>Time of Medication</th>
			 			<th>Reason for Medication</th>
			 		</tr>	
			 		<tr>
			 			<th></th>
			 			<th></th>
			 			<th></th>
			 			<th></th>
			 			<th></th>
			 			<th></th>
			 		</tr>				 		
			 	</thead>
			 	<tbody>
			 	</tbody>
			 
			 </table>
				<div style="float:right">
				<input type="button" onclick="save()" value=Save>
				<input type="button" onclick="cancel()" value="Cancel">
				</div>
			 
			</form>
			</div>

<!-- end of popup -->




<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
				<table cellpadding="0" cellspacing="0" class="" align="" border="0">
				<tr>
				<td>Next:</td>
				<td>
				<select name="" id="" class=""><option>Select</option><option>PreParation</option><option>Document Review</option><option>Collection</option></select>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" onclick="" value="Next"/></td>
				</tr>
				</table>
		</form>
		</div>
		
		<div class="cleaner"></div>
</section>
