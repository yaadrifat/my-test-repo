<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<link rel="stylesheet" type="text/css" href="css/notes.css" /> 
<style>
/* .chk_content_no_bg {
    background: none repeat scroll 0 0 #F5F5F5;
    height: 12px;
    padding: 10px;
    text-align: center;
}
.larfont {
    font-size: 1.3em;
    font-weight:bold;
}
#notes a {
    color: #0088CC;
 text-decoration: none;
 font-weight:bold;
 font-size:1.2em;
}
.extraComnt{
background:none repeat scroll 0 0 #F5F5F5
}
#noteView{
    margin-top: 1%;
}
.reply_content{
	clear:both;
	margin-left:2%;
} */
</style>
<script>
$(document).ready(function(){
});
function hideExtraComments(obj){
	    $(obj).parent().next( ".extraComnt" ).toggle();
}

function viewNoteMenu(obj){
	if($(obj).next("#menulist").is(":visible")){
	$(obj).next("#menulist").hide();
	}else{
	$(".noteMenu").hide();
	$(obj).next("#menulist").show();
	}
	}
function deleteNoteData(obj){
    var data =$(obj).attr("id");
    var entityId=$("#tempEntityId").val();
    var donor="donor";
    var parameters="{entityId:'"+entityId+"',entityType:'"+donor+"',data:'"+data+"',flag:'"+0+"'}";    
    //var parameters="{entityId:'"+id+"',entityType:'"+donor+"',flag:'"+falg+"'}";
	$.ajax({
        type: "POST",
        url: "deleteNoteData.action",
        async:false,
        data:"jsonData="+parameters,
        success:function (result){
        	//alert("result " + result );
        	//alert("Notes Saved Successfully");
        	stafaAlert('Notes Deleted  Successfully','');
        	$(".ui-dialog").find("#notesContainer").html(" test ");
        	//alert("trace 1");
        	$(".ui-dialog").find("#notesContainer").html(result);
        	//clearForm("notesForm");
        	},
        error: function (request, status, error) {
            alert("Error " + error);
        }
    });
	} 
</script>
<input type="hidden" id="tempEntityId" value='<s:property value="entityId"/>'>
<input type="hidden" id="tempEntityType" value='<s:property value="entityType"/>'>
<input type="hidden"  id="listSize" value='<s:property value="%{notesList.size()}"/>'/>
<section id="notes"> 
	<a id="newNotes" style="text-decoration:none" onclick="newNote(this)" href="javascript:;" >
		<img title="New Note" style="cursor: pointer; width: 19px;" src="images/icons/pencilicon.png">
 				Create a New Note 
	</a>
	<div id="noteView">
		<div id="newNoteCreate"></div>
				<s:if test="notesList!=null && notesList.size()>0" >
				<s:iterator value="notesList"   var="rowVal">
					<hr id="hr" width="100%">
					<div id="mainnote" class="chk_content_no_bg" >
						<div align="left" style="float:left;" onclick="hideExtraComments(this)">
							<!-- <input id="1638" type="checkbox" value="false" title="Check to Delete"> -->
							<span class="larfont">
								<s:property  value="%{#rowVal[3]}" />
							</span>
						</div>
						<div align="right" style="float:right;">
							<span>
							<s:property  value="%{#rowVal[1]}" /></span>
								 <img id ='<s:property value="%{#rowVal[4]}" />' onclick="viewNoteMenu(this);" title="View Menu" style="cursor:pointer;" src="images/icons/arrowDown.png">
									<ul id="menulist" class="noteMenu" style="display: none;">
										<li id ='<s:property value="%{#rowVal[4]}" />' onclick="deleteNoteData(this)" title="Delete">
											<img id="detNotes" src="images/icons/delete.png">&nbsp;Delete
										</li>
									</ul>
						</div>
					</div>
					<div id="extraComments" class="extraComnt">
						<div class="reply_content">
							<table align="center" width="96%" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td align="left" width="2%" style="vertical-align:top">
											<!-- <input id="1638" type="checkbox" disabled="" value="false" title="Check to Delete"> -->
										</td>
										<td class="norfont" align="left" width="95%">
										<s:if test="#rowVal[5]==1">
											<span class="deleteNote" style="margin-left:15px">
												<b> <s:property  value="%{#rowVal[0]}" /> on <s:property  value="%{#rowVal[1]}" /> said: </b>
											</span>
											</s:if>
											<s:else>
												<span  style="margin-left:15px">
													<b> <s:property  value="%{#rowVal[0]}" /> on <s:property  value="%{#rowVal[1]}" /> said: </b>
												</span>
											</s:else>
											<br>
											<s:if test="#rowVal[5]==1">
												<span class="deleteNote" style="margin-left:15px">
													<s:property  value="%{#rowVal[2]}" escape="false" />
												</span>
											</s:if>
											<s:else>
												<span  style="margin-left:15px">
													<s:property  value="%{#rowVal[2]}" escape="false" />
												</span>
											</s:else>
										</td>
										<td align="right" width="15%">
											<img src="pages/images/icons/reply.png" onclick="fn_Replydiv(1638);" style="width: 12px;float:right">
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</s:iterator>
			</s:if>
		</div>
	</section>