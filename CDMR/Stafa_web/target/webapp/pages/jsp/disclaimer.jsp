<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	
	String disclaimerCode ="";
	if(request.getParameter("disclaimerCode")!=null){
		disclaimerCode = request.getParameter("disclaimerCode");
	}
	else if(request.getAttribute("disclaimerCode")!=null){
		disclaimerCode = (String)request.getAttribute("disclaimerCode");
	}
	request.setAttribute("disclaimerCode",disclaimerCode);
%>
<s:if test="(#request.disclaimerCode == 'referralDisclaimer')">
    <div class="portlet-content">
       		           <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
       		           <input type ="hidden" id="disclaimerCode" name = "disclaimerCode" value="<s:property value="#request.disclaimerCode"/>"/>
       		           <input type ="hidden" id ="entityType" name ="entityType" value ="recipient"/>
       		           <input type ="hidden" id ="pkDisclamier" name ="pkDisclamier" value ="<s:property value="recipientDetails.pkDisclamier"/>"/>
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientInsuranceQues"/></td>
       		            		<td class="tablewapper1">
       		            		
       		            		<s:radio name="disclaimerQuestion1" value ="%{recipientDetails.disclaimerQuestion1}" id="disclaimerQuestion1" list="#{'true':'Yes','false':'NO'}" ></s:radio>
       		            		<!-- <input name ="disclaimerQuestion1" id ="disclaimerQuestion1" type="Radio" name="yes"  id="">Yes<input type="Radio" name="No" id="">No </td> -->
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientComments"/></td>
       		            		<td class="tablewapper1"><s:textarea value ="%{recipientDetails.comment1}" id="disclaimerComment1" name="comment1"  rows="5" cols="50" wrap="hard" style="width:800;height:190;" ></s:textarea></td>
       		            		
       		            	</tr>
       		            	
       		            </table>
        </div>
</s:if>

<s:elseif test="(#request.disclaimerCode == 'consultEvalDiscliamer')">
  <div class="portlet-content">
       		           <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
       		             <input type ="hidden" id="disclaimerCode" name = "disclaimerCode" value="<s:property value="#request.disclaimerCode"/>"/>
       		           <input type ="hidden" id ="entityType" name ="entityType" value ="recipient"/>
       		           <input type ="hidden" id ="pkDisclamier" name ="pkDisclamier" value ="<s:property value="disclaimerDetails.pkDisclaimer"/>"/>
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientDisclaimersConsultEval1"/></td>
       		            		<td class="tablewapper1"><s:radio name="disclaimerQuestion1" value ="%{disclaimerDetails.question1}" id="disclaimerQuestion1" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientDisclaimersConsultEval2"/></td>
       		            		<td class="tablewapper1"><s:radio name="disclaimerQuestion2" value ="%{disclaimerDetails.question2}" id="disclaimerQuestion2" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientComments"/></td>
       		            		<td class="tablewapper1"><s:textarea value ="%{disclaimerDetails.comment1}" id="disclaimerComment1" name="comment1"  rows="5" cols="50" wrap="hard" style="width:800;height:190;" ></s:textarea></td>
       		            		
       		            	</tr>
       		            	
       		            </table>
        </div>
        </s:elseif >
<s:elseif test="(#request.disclaimerCode == 'interimVistDiscliamer')">
         <div class="portlet-content">
       		           <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
       		            	<input type ="hidden" id="disclaimerCode" name = "disclaimerCode" value="<s:property value="#request.disclaimerCode"/>"/>
       		           	<input type ="hidden" id ="entityType" name ="entityType" value ="recipient"/>
       		          	 <input type ="hidden" id ="pkDisclamier" name ="pkDisclamier" value ="<s:property value="disclaimerDetails.pkDisclaimer"/>"/>
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientDisclaimersIntrimVisitQues1"/></td>
       		            		<td class="tablewapper1"><s:radio name="disclaimerQuestion1" value ="%{disclaimerDetails.question1}" id="disclaimerQuestion1" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientDisclaimersIntrimVisitQues2"/></td>
       		            		<td class="tablewapper1"><s:radio name="disclaimerQuestion2" value ="%{disclaimerDetails.question2}" id="disclaimerQuestion2" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientComments"/></td>
       		            		<td class="tablewapper1"><s:textarea value ="%{disclaimerDetails.comment1}" id="disclaimerComment1" name="comment1"  rows="5" cols="50" wrap="hard" style="width:800;height:190;" ></s:textarea></td>
       		            		
       		            	</tr>
       		            	
       		            </table>
        </div>
        </s:elseif>
   <s:elseif test="(#request.disclaimerCode == 'preTransplantDisclaimer')">
	 <div class="portlet-content">
       		           <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
       		            	<input type ="hidden" id="disclaimerCode" name = "disclaimerCode" value="<s:property value="#request.disclaimerCode"/>"/>
       		           	<input type ="hidden" id ="entityType" name ="entityType" value ="recipient"/>
       		          	 <input type ="hidden" id ="pkDisclamier" name ="pkDisclamier" value ="<s:property value="disclaimerDetails.pkDisclaimer"/>"/>
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientDisclaimersPreTransplant"/></td>
       		            		<td class="tablewapper1"><s:radio name="disclaimerQuestion1" value ="%{disclaimerDetails.question1}" id="disclaimerQuestion1" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientComments"/></td>
       		            		<td class="tablewapper1"><s:textarea value ="%{disclaimerDetails.comment1}" id="disclaimerComment1" name="comment1"  rows="5" cols="50" wrap="hard" style="width:800;height:190;" ></s:textarea></td>
       		            		
       		            	</tr>
       		            	
       		            </table>
        </div>
        </s:elseif>
          <s:elseif test="(#request.disclaimerCode == 'preCollectionDisclaimer')">
	 <div class="portlet-content">
       		           <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
       		            	<input type ="hidden" id="disclaimerCode" name = "disclaimerCode" value="<s:property value="#request.disclaimerCode"/>"/>
       		           	<input type ="hidden" id ="entityType" name ="entityType" value ="recipient"/>
       		          	 <input type ="hidden" id ="pkDisclamier" name ="pkDisclamier" value ="<s:property value="disclaimerDetails.pkDisclaimer"/>"/>
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientDisclaimersPreCollection"/></td>
       		            		<td class="tablewapper1"><s:radio name="disclaimerQuestion1" value ="%{disclaimerDetails.question1}" id="disclaimerQuestion1" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientComments"/></td>
       		            		<td class="tablewapper1"><s:textarea value ="%{disclaimerDetails.comment1}" id="disclaimerComment1" name="comment1"  rows="5" cols="50" wrap="hard" style="width:800;height:190;" ></s:textarea></td>
       		            		
       		            	</tr>
       		            	
       		            </table>
        </div>
        </s:elseif>
        <s:else>
         <div class="portlet-content">
       		           <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientDisclaimersPreTransplantAllo"/></td>
       		            		<td class="tablewapper1"><input type="Radio" name="yes"  id="">Yes<input type="Radio" name="No" id="">No </td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientComments"/></td>
       		            		<td class="tablewapper1"><s:textarea id="recipientComments" name="recipientComments"  rows="5" cols="50" wrap="hard" style="width:800;height:190;" ></s:textarea></td>
       		            		
       		            	</tr>
       		            	
       		            </table>
        </div>
         <div class="portlet-content">
       		           <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientDisclaimersPreTransplantAutoAdult"/></td>
       		            		<td class="tablewapper1"><input type="Radio" name="yes"  id="">Yes<input type="Radio" name="No" id="">No </td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientComments"/></td>
       		            		<td class="tablewapper1"><s:textarea id="recipientComments" name="recipientComments"  rows="5" cols="50" wrap="hard" style="width:800;height:190;" ></s:textarea></td>
       		            		
       		            	</tr>
       		            	
       		            </table>
        </div>
          <div class="portlet-content">
       		           <table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientDisclaimersPreTransplantAutoPeds"/></td>
       		            		<td class="tablewapper1"><input type="Radio" name="yes"  id="">Yes<input type="Radio" name="No" id="">No </td>
       		            	</tr>
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.recipientComments"/></td>
       		            		<td class="tablewapper1"><s:textarea id="recipientComments" name="recipientComments"  rows="5" cols="50" wrap="hard" style="width:800;height:190;" ></s:textarea></td>
       		            		
       		            	</tr>
       		            	
       		            </table>
        </div>
</s:else>