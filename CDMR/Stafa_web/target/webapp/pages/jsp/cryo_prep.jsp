<%@ include file="common/includes.jsp" %>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/placeholder.js"></script>

    <script type="text/javascript" src="js/jquery/graph/jquery.jqplot.min.js"></script>
    <script type="text/javascript" src="js/jquery/graph/shCore.min.js"></script>
    <script type="text/javascript" src="js/jquery/graph/shBrushJScript.min.js"></script>
    <script type="text/javascript" src="js/jquery/graph/shBrushXml.min.js"></script>
  
   <script type="text/javascript" src="js/jquery/graph/jqplot.cursor.min.js"></script>
  <link type="text/css" href="css/jquery.jqplot.css" rel="stylesheet" />
  <script type="text/javascript" src="js/ajaxfileupload.js"></script>
<!-- <link type="text/css" href="css/floatmenu.css" rel="stylesheet" /> -->

<style type="text/css">
	  .jqplot-cursor-legend {
	    width: 160px;
	    font-family: "Courier New";
	    font-size: 0.8em;
	  }
	  
	  td.jqplot-cursor-legend-swatch {
	    width: 1.3em;
	  }
	  
	  div.jqplot-cursor-legend-swatch {
/*      width: 15px;*/
	  }
	</style>
<script>

$( ".column" ).sortable({
	connectWith: ".column",
    cursor: 'move',
    cancel:	".portlet-content"
});

function resetPage(){
	//alert("side");
$("#recipientName").html("");
$("#recipientId").html("");
$("#recipientDiagnosis").html("");
$("#recipientAboRh").html("");
$("#donorName").html("");
$("#donorId").html("");
$("#donorAboRh").html("");
$("#productId").html("");
$("#productType").html("");	

//alert("cry");
$("#cryoBagVolume").val("");
$("#referenceVials").val("");
$("#totalVolume").val("");
$("#noOfBags").val("");
$("#hsaAdded").val("");

//alert("chil");
$("#childNo").trigger("click");
//alert("gr");
$("#startingTemp").text("");       
$("#finalTemp").text("");

$("#chart1").html("");
$("#chart2").html("");
$("#resetGraph").hide();

$('#childcreation tbody tr').each(function(i,v){
	$(this).remove();
});
$("#childNo").attr('checked', 'checked');
$("#childNo").trigger('click');

//alert("qc");
$.uniform.restore('select');
$("select").uniform();
}

function showHide(This){
	$("#addtext").hide();
	//$("#thSelectColumn").css('display: block');
	if(This.id=="childYes"){
		$("#childDiv").show();
		// $('#createchild'). find( ".portlet-header .ui-icon" ).removeClass('ui-icon-minusthick')addclass('ui-icon-plusthick');
		nchildrens();
		//$( ".portlet-header .ui-icon" ).trigger("click");
		}else{deleteallchild();
			//$("#childDiv").hide();
			//$( ".portlet-header .ui-icon" ).trigger("click");
	}
}

	var name = "#floatMenu";
	var menuYloc = null;
	

$(document).ready(function() {
	show_slidecontrol('sidecont');
	show_slidewidgets('cryo_slidewidget');
	$("#childDiv").hide();
	//getChildcreationTable();   
	//alert($("#newProductId").val());
	//$('#nextProtocolList').append("<option>Next Product</option><option>Logout</option>");
	oTable =$('#childcreation').dataTable({
		 //"bJQueryUI": true,
			"bRetrieve": true,
			"sPaginationType": "full_numbers",
			"bFilter":false,
			"bPaginate":false,
			"oColVis": {
					"aiExclude": [ 0 ],
					"buttonText": "&nbsp;",
					"bRestore": true,
					"sAlign": "left",
					"iOverlayFade": 25 
				},
				"sDom": 'C<"clear">Rlfrtip',
			//sorting the input fields
		 "aoColumns": [{ "bSortable":false },
		               { "bSortable":false },
			   			null,
			   			null,
			   			{ "sSortDataType": "dom-select" },
			   			{ "sSortDataType": "dom-select" },
			   			{ "sSortDataType": "dom-text", "sType": "numeric" },
			   			{ "sSortDataType": "dom-text", "sType": "numeric" },
			   			{ "sSortDataType": "dom-text", "sType": "numeric" },
			   			{ "sSortDataType": "dom-text", "sType": "numeric" },
			   			{ "sSortDataType": "dom-text", "sType": "numeric" },
			   			{ "sSortDataType": "dom-text", "sType": "numeric" },
			   			{ "sSortDataType": "dom-select" }
		   		],
		   		"fnInitComplete": function () {
			        $("#childColumn").html($('.ColVis:eq(0)'));
				}
		
	});
	 //oTable.thDatasorting('childcreation'); 
		
	/*menuYloc = parseInt($(name).css("top").substring(0,$(name).css("top").indexOf("px")))
	$(window).scroll(function () { 
		offset = menuYloc+$(document).scrollTop()+"px";
		$(name).animate({top:offset},{duration:500,queue:false});
	});*/
	//to get Qc list 
	/*//? Mari var url = "getQc";
	var response = ajaxCall(url,"originalProductForm");
	generateTableFromJsnList(response);*/

	var product = $("#product").val();
	//BB testing 
	//product="bmt1"
	 //alert("product : "+product);
	 if(product !=""){
		 //alert(product);
		 $("#fake_conformProductSearch").val(product);
		 $("#conformProductSearch").val(product);
		 var keyEvent = jQuery.Event("keyup");
		 keyEvent.keyCode=$.ui.keyCode.RIGHT;
		 $("#conformProductSearch").focus();
		 $(".barcodeSearch").trigger(keyEvent);
		 
		 var keyEvent1 = jQuery.Event("keyup");
		 keyEvent1.keyCode=$.ui.keyCode.ENTER;
		 $(".barcodeSearch").trigger(keyEvent1);

		 $("#productSearch").val(product);
		 $("#productSearch").trigger(keyEvent1);
	}

	//to get calculation list 
	//var mappedProtocol = $("#fkProtocol").val();
	//alert("mappedProtocol "+mappedProtocol);
	//loadCalculationDiv("calculationDiv",mappedProtocol);
	//getCal
});
/* Create an array with the values of all the select options in a column */
$.fn.dataTableExt.afnSortData['dom-select'] = function  ( oSettings, iColumn )
{
	var aData = [];
	$( 'td:eq('+iColumn+') select', oSettings.oApi._fnGetTrNodes(oSettings) ).each( function () {
		aData.push( $(this).val() );
	} );
	return aData;
}

/* Create an array with the values of all the input boxes in a column */
$.fn.dataTableExt.afnSortData['dom-text'] = function  ( oSettings, iColumn )
{
	var aData = [];
	$( 'td:eq('+iColumn+') input', oSettings.oApi._fnGetTrNodes(oSettings) ).each( function () {
		aData.push( this.value );
	} );
	return aData;
}

/* function columnMaager(){
	var showFlag1 = false;
	$('#targetall').hide();
	$('#tableall').columnManager({listTargetID:'targetall', onClass: 'advon', offClass: 'advoff', hideInList: [1,4,5], 
									saveState: true, colsHidden: [1,3]});
	$('#ulSelectColumn').click( function(){
		  if(showFlag1){
			$('#targetall').fadeOut();
			showFlag1 = false;
		  }else{
			  $('#targetall').fadeIn();
			  showFlag1 = true;
		  }
		});
} */

function setTmpValues(){
	// set tmp values
	$("#tmptotalVolume").val( $("#totalVolume").val());
	 $("#tmpdilution").val($("#dilution").val());
	 $("#tmpweight").val($("#weight").val());
	 $("#tmphct").val($("#hct").val());
	 $("#tmpsample1").val($("#sample1").val());
	 $("#tmpsample2").val($("#sample2").val());
	 $("#tmphgb").val($("#hgb").val());
	 $("#tmptnc").val($("#tnc").text());
}
 $(function(){
     $("select").uniform(); 
  }); 
function setBufferValues(){
	$("#totalVolume").val( $("#tmptotalVolume").val());
	 $("#dilution").val($("#tmpdilution").val());
	 $("#weight").val($("#tmpweight").val());
	 $("#hct").val($("#tmphct").val());
	 $("#sample1").val($("#tmpsample1").val());
	 $("#sample2").val($("#tmpsample2").val());
	 $("#hgb").val($("#tmphgb").val());
	 fn_calculation();
	}

function addtxt(){$('#deleteallchild').attr('checked', false);

	$("#addtext").html('# of Children &nbsp;<input type=text  id=norow  onkeypress="createcols(event);"/>');
	$("#addtext").show();
		}
 function createcols(e){	
	 if(e.keyCode==13){
		// $.uniform.restore('select');	
		Createchildrows();			
		return false;
		}
	} 
 function checkall() { 
	var isChecked = $('#deleteallchild').attr('checked');
 	if(isChecked)
 		{
	 $('.deleterows').attr('checked', true);
 		}
 	else
 		{
 		$('.deleterows').attr('checked', false);	
 		}
 }
 var dynoptions = Array();
 var header = Array()
/*function Createchildrows(){ 
	var i=0; 
	var n=$("#norow").val();
	var dynrow=$('#childgeneration').find('tr').length;
	if(dynrow!=1){
		dynrow++;
	}

	var componentH_1 = $('#componentH_1').html();
	var containerH_1 = $('#containerH_1').html(); 
	var intendedProcessH_1 = $('#intendedProcessH_1').html();
	alert(componentH_1);
	for(i;i<n;i++){		
	oTable=$('#childcreation').dataTable().fnAddData(
									[
									 "","<input type='checkbox' class='deleterows'/>",
									 "<label>BMT123-"+dynrow+"</label>",
									 "<div class='selector_2'><select  style=\"width:50%\"></select></div>",
									 "<div class='selector_2'><select  style=\"width:50%\"></select></div>",
									 "<input type=text  id=volid_"+dynrow+" style=\"width:95%\"/>",
									 "<input type=text  id=wgtid_"+dynrow+" style=\"width:95%\"/>",
									 "<input type=text  id=tncid_"+dynrow+" style=\"width:95%\"/>",
									 "<input type=text  id=tncids_"+dynrow+" style=\"width:95%\"/>",
									 "<input type=text  id=cdeid_"+dynrow+" style=\"width:95%\"/>",
									 "<input type=text  id=cdekg_"+dynrow+" style=\"width:95%\"/>",
									 "<div class='selector_2'><select style=\"width:50%\"><option>Select</option><option>Vol.Red.</option><option>Plasma Depletion</option><option>MNC</option><option>Wash</option><option>Buffy Coat</option><option>Cryo-prep</option><option>Release</option></select></div>"
									]);
					dynoptions[i]='BMT123-'+dynrow;	
					dynrow=dynrow+1;
						
		}
		$("#childcreation thead tr th").each(function(i, v){
        header[i] = $(this).attr('style');
        	if(header[i].indexOf("display: none") >0){
    	   $('.tbody').find('tr').children('td:nth-child('+(i+1)+')').hide();
       }         	        
	 }); 		
	$("select").uniform();  
	$("#addtext").hide();
   	nchildrens();
   	nextprotopt();
	}
	//add to nextProtocolList
	function nextprotopt(){
		var dynrow=$('#childcreation').find('tr').length;
		var i=0;
		var txtval=$('#norow').val();
		i=dynrow;	
		for(i=0;i<txtval;i++){
			$('#nextProtocolList').append("<option title='newadd'>"+dynoptions[i]+"</option>");
		}	
	} 
	var oTable1= $('#childcreation').dataTable();
	function deleteallchild(){
		 var dynrow=$('#childcreation').find('tr').length;
    	 if($('#childNo').attr('checked')){
    		 if(dynrow>1){  
    			 $('#deleteallchild').attr('checked', true);
    		checkall();
    		deletechildrows('childcreation');
        	 //$('.deleterows').attr('checked', false);
        	 $('#createchild'). find( ".portlet-header .ui-icon" ).trigger("click");
        	 $("#childDiv").hide();
    		 return;   		 
    		 }
    	 }
        else{
        		 $('input[name=Yeschild]').attr('checked', false);
        		 $('#childYes').attr('checked', true);
        		 $("#childDiv").show();
        		 return;
        }
    		 $("#childDiv").hide();
    		 $("#nextProtocolList option[title='newadd']").remove(); 
        	 $("#nextdropdown").find("tbody").find("tr").find("td").find("#uniform-nextProtocolList").find("span").text("Select");
    }
*/
 function deletechildrows(id) {
	 var table = document.getElementById(id);
		try {
		   	var isChecked = $('#childgeneration').find('tr').length;
		   	var isCheckedall = $('#deleteallchild').attr('checked');
		   	if(isChecked>=1 && $('.deleterows').is(':checked')){
		   	if(isCheckedall){
		   		var yes=confirm('Do you want delete all products ?');	
		   	}
		   	else{var yes=confirm('Do you want delete selected products?');
		   	}
		   	var rowCount = $('#childgeneration').find('tr').length;
		    if(yes){
		    	$('#childcreation .tbody tr').each(function(i,v){
						var cbox=$(v).children("td:eq(1)").find(':checkbox').is(":checked");
						if(cbox){
							oTable1.fnDeleteRow(this);
						}
				});
			 $('.deleterows').attr('checked', false);
			$("#nextProtocolList option[title='newadd']").remove();
			$("#nextdropdown").find("tbody").find("tr").find("td").find("#uniform-nextProtocolList").find("span").text("Select");
		    }
		}
		     rowCount = table.rows.length;
		     for(var i=1; i<rowCount; i++) {
					var row = table.rows[i];
			        var cbox = row.cells[2];
			        var labelText = $(cbox).find('label').text();
			        if(labelText!="" && labelText!=null && labelText!="undefined"){
			        	var indexL = labelText.indexOf('-');
			        	labelText = labelText.substr(0,indexL+1);
			        	$('#nextProtocolList').append("<option title='newadd'>"+labelText+i+"</option>"); 
			        	labelText= labelText+i;
			        	$(cbox).find('label').text(labelText);
			        }   
		    }
	    }catch(e) {
	        alert(e);
	    }
	    $('#deleteallchild').attr('checked', false);
	    nchildrens();
	 }  
 var dynrows;
 function nchildrens(){
	 dynrows=$('#childgeneration').find('tr').length;
	 var len=$("#childcreation").find("tbody").find("tr").find("td").text();
	 if(len=='No data available in table')
		 {dynrows=0;
		 $("#nchild").html('<b>Total No of Children</b> &nbsp;'+'<b>'+dynrows+'</b>');
		 }
	  if(dynrows>=2)
		 {
		  $("#nchild").html('<b>Total No of Children</b> &nbsp;'+'<b>'+dynrows+'</b>');
		 } else {
	 $("#nchild").html('<b>Total No of Children</b> &nbsp;'+'<b>'+dynrows+'</b>');
		 }
 }

function drawGraph(){
	//alert("drawgraph");
	//alert($("#uploadFrame").html());
	//alert($('iframe').contents().find("#strCurve1").val());
	$("#chart1").html("");
	$("#chart2").html("");
	var startingTemp, finalTemp;

	var cur1 = eval($('iframe').contents().find("#strCurve1").val());
	var cur2 = eval($('iframe').contents().find("#strCurve2").val());
	var cur3 = eval($('iframe').contents().find("#strCurve3").val());
	var documentId =eval($('iframe').contents().find("#documentId").val());
	//alert("trace 1");
	$("#cryoCurve1").val($('iframe').contents().find("#strCurve1").val());
	$("#cryoCurve2").val($('iframe').contents().find("#strCurve2").val());
	$("#cryoCurve3").val($('iframe').contents().find("#strCurve3").val());
	$("#documentId").val(documentId);
	//alert( " cur3 " + cur3);
	     if(cur3 != undefined &&  cur3.length > 0){   
	    	 startingTemp = cur3[0][1];
	    	 finalTemp = cur3[cur3.length-1][1];
	    	// alert(" 1"+startingTemp + " / " +finalTemp );
	          targetPlot = $.jqplot('chart1', [cur1, cur2, cur3], {
			    seriesColors: [ "#1430e3", "#b00ef0", "#078c10"],
		        cursor:{
		        	show:true,
		        	  zoom:true,
		        	  showTooltip:true,
		      		  followMouse: true,
		        }
		       }); 
	            
	            
	            controllerPlot = $.jqplot('chart2', [cur1, cur2, cur3], {
                    seriesColors: [ "#1430e3", "#b00ef0", "#078c10"],
                    cursor:{
                    	show:true,
                      showTooltip: false,
                      zoom:true,
                      constrainZoomTo: 'x'
                    }
                   
                  });
	     }else{
	    	 startingTemp = cur2[0][1];
	    	 finalTemp = cur2[cur2.length-1][1];
	    	// alert(" 2 "+startingTemp + " / " +finalTemp );
	    	 targetPlot = $.jqplot('chart1', [cur1, cur2], {
	    		 seriesColors: [ "#1430e3", "#b00ef0"],
			        cursor:{
			        	show:true,
			        	 zoom:true,
			        	 showTooltip:true,
			      		  followMouse: true,
			        }
			       }); 
		            
		            controllerPlot = $.jqplot('chart2', [cur1, cur2], {
	                    seriesDefaults:{neighborThreshold:0, showMarker: false},
	                    
	                    seriesColors: [ "#1430e3", "#b00ef0"],
	                    cursor:{
	                    	show:true,
	                      showTooltip: false,
	                      zoom:true,
	                      constrainZoomTo: 'x'
	                    },
	                    axesDefaults:{tickOptions:{formatString:"%d"}, autoscale:false, useSeriesColor:true}
	                  });
	 
	     }    
	  //   alert("trace 2");
	     $("#startingTemp").text(startingTemp);       
	     $("#finalTemp").text(finalTemp);
	     
	            
                  $.jqplot.Cursor.zoomProxy(targetPlot, controllerPlot);
                  $("#resetGraph").removeClass("hidden");
                 
}

function nextCryoPrep(){

	var recipientId = $("#leftnav #recipientId").val();
	var productSearch = $("#fake_conformProductSearch").val();
	var validProduct = $("#validProduct").val();
	if( productSearch==null || productSearch=="" || validProduct==1){
		alert("Please Scan/Enter valid Product Id");
		return;
		}
	//alert($("#cryoPrep").val());
	if($("#cryoPrep").val()>0){
		alert("Product is already Cryo-Prepared.");
		return;
	}
	//alert("s");
	//return;
	
var nextToCryo = $("#nextToCryo").val();
var productId;
var componenet;
var container;
var volume;
var weight;
var TNC;
var TNCkg;
var CD34;
var CD34Kg;
var intendedProcess;
var comments;
var rowData = "";

    $("#childcreation tbody tr").each(function (row){
       // alert("re"+row);
    	$(this).find("td").each(function (col){
			if(col ==2){
				productId= $(this).text();
			}else if(col==3){
				component=$(this).find("#component_"+(row+1)).val();	
			}else if(col ==4){
				container=$(this).find("#container_"+(row+1)).val();
			}else if(col ==5){
				volume=$(this).find("#volid_"+(row+1)).val();
			}else if(col ==6){
				weight=$(this).find("#wgtid_"+(row+1)).val();
			}else if(col ==7){
				TNC=$(this).find("#tncid_"+(row+1)).val();
			}else if(col ==8){
				TNCkg=$(this).find("#tncids_"+(row+1)).val();
			}else if(col ==9){
				CD34=$(this).find("#cdeid_"+(row+1)).val();
			}else if(col ==10){
				CD34kg=$(this).find("#cdekg_"+(row+1)).val();
			}else if(col ==11){
				intendedProcess=$(this).find("#ide__"+(row+1)).val();
			}else if(col ==12){
				comments=$(this).find("#comment_"+(row+1)).val();
			}
    	});

    	
		if(productId!= undefined){
		 	  rowData+= "{cryoChilds=0,refCryoPrep:0,productSearch:'"+productId+"',refCodelstComponent:'"+component+"',refCodelstContainer:'"+container+"',volume:'"+volume+"',weight:'"+weight+"',tnc10_8:'"+TNC+"',tncKg10_6:'"+TNCkg+"',cd34_10_6:'"+CD34+"',cd34Kg_10_6:'"+CD34kg+"',refCodelstIntended:'"+intendedProcess+"',comments:'"+comments+"'},";
	 		}
	 		//alert("rowData : "+rowData);
    });

    if(rowData.length >0){
		 rowData = rowData.substring(0,(rowData.length-1));
    }
    var cryoData = JSON.stringify(serializeFormObject($('#cryoPreparationForm')));
  
    cryoData = cryoData.substring(1,(cryoData.length-1));
    //var graphData = JSON.stringify(serializeFormObject($('#frmGraph')));
    //graphData = graphData.substring(1,(graphData.length-1));
  //  alert(" cryoData " + cryoData);
  var nextOption = $("#nextOption").val();
    var jsonData = "jsonData={childs:["+rowData+"],cryoData:{"+cryoData+"},nextOption:'"+nextOption+"'}";
    var url = "saveCryoPreparation";
    response = jsonDataCall(url,jsonData);
    
/*//?BB remove later
    $.ajax({
    type: "POST",
    url: "saveCryoPreparation.action?jsonData="+jsonData,
    async:false,
    data : $("#uploadForm").serialize(),
    success: function (result){
        alert(result);
     }
	});
*/
    
    

    alert("Data Saved Successfully");
    
    var res = response.returnMessage;
    if(res !=""){
   		 var url = res;
   		 loadAction(url);
    }
     /*     if(nextToCryo=="Storage")
		{
        	javascript:loadPage('html/stafa/storage_storeProduct.html');
		}
        
        if(nextToCryo=="Production")
		{
			javascript:loadAction('getOriginalProduct.action?newProductId='+productId);
		} */
}

//
$(document).ready(function() {
	//getProcessingData();
	try{
		//alert(" ready");
	
	
	//??BB check with mari
	/*
	document.getElementById('equipmentsTable_next').setAttribute("onClick","removeTableClass()");
	document.getElementById('equipmentsTable_previous').setAttribute("onClick","removeTableClass()");
	document.getElementById('reagentsTable_next').setAttribute("onClick","removeTableClass()");
	document.getElementById('reagentsTable_previous').setAttribute("onClick","removeTableClass()");
	
	removeTableClass();
	removeTableClassOnChange("equipmentsTable_length");
	removeTableClassOnChange("reagentsTable_length");
	*/
	//calculation
	//alert(" trace 1")
	$("#cryoTV").val($("#tmptv2").val());
	fn_cryoCalculation();
	}catch( err){
		alert(err);
	}
	$("select").uniform();
});

$('#totalVolume').keyup(function() {
	fn_calcBagCount();
});

function fn_calcBagCount(){
	//alert("test");
	if($("#totalVolume").val() !="" && $("#cryoBagVolume").val()!=""){
		var bagVol = eval($("#cryoBagVolume option:selected").text() * .60);
		//alert("bagVol" +bagVol);
		var bagNo = ($("#totalVolume").val() / bagVol );
		//alert("bagNo " + bagNo);
		$("#noOfBags").val(Math.ceil(bagNo));
	}else{
		$("#noOfBags").val("");
	}
}

function fn_cryoCalculation(){
	//alert(" trace 2")
	var cryoBV = $("#cryoBV").val();
	var x1 = 60;
	var x2=30000;
	var x3 = 35;
	var x4=5;

	if(cryoBV ==60){
		x1=36;
		x2=18000;
		x3=21;
		x4=3;
		
	}
	//alert((Number($("#tmptnc2").val()) + " / " + x2));
	var nob1 = Math.ceil((Number($("#cryoTV").val())/x1 ));
	var nob2 = Math.ceil((Number($("#tmptnc2").val())/x2 ));
	var cyroNob = nob1;
	//alert( nob1 + "<" + nob2);
	if(nob1 < nob2){
		cyroNob = nob2;
	}
	$("#cyroNoB").val(cyroNob);
	
	$("#hsaCell").val( ((cyroNob*x1)-Number($("#cryoTV").val())));
	
	
	var hsan = (cyroNob+2)* x3;
	var dmson = (cyroNob+2)* x4;
	var ffs = hsan + dmson;
	$("#hsan").text(hsan);
	$("#dmson").text(dmson);
	$("#ffs").text(ffs);
	
	//construct product table
	//constructProductTable();
	
	
	
}


function getScanData(event,obj){
	if(event.keyCode ==13){
		getProcessingData(event);
	}
}
function removeTableClass(){
	
	$('#equipmentsTable_next').removeClass("ui-state-default");
	$('#equipmentsTable_previous').removeClass("ui-state-default");
	$('#reagentsTable_next').removeClass("ui-state-default");
	$('#reagentsTable_previous').removeClass("ui-state-default");

}

function generateTableFromJsnList(response){
	$.each(response.recipientDonorList, function(i,v) {
		//alert(i+"=-="+v[0]);
		$("#recipientName").html(v[0]);
		$("#recipientId").html(v[1]);
		$("#recipientDiagnosis").html(v[2]);
		$("#recipientAboRh").html(v[3]);
		$("#donorName").html(v[4]);
		$("#donorId").html(v[5]);
		$("#donorAboRh").html(v[6]);
		$("#productId").html(v[7]);
		
		$("#productType").html(v[9]);
		
		
	 	});
}
function getProcessingData(e){
	//alert("s");
	//?BB var url = "getCryoPrepData";
	
	var searchUrl = "checkCryoPreparationData"
	var productSearch = $("#fake_conformProductSearch").val();
	if(e.keyCode==13 && productSearch!=""){
		$('.progress-indicator').css( 'display', 'block' );
		var result = jsonDataCall(searchUrl,"productId="+productSearch);
	    var listLength=result.cryoPrepList.length;
	    	//alert(listLength);
	        if(listLength <= 0){
	        	alert("Product ID is not Found");
	        	$("#validProduct").val(1);
	        	resetPage();
	        	$('.progress-indicator').css( 'display', 'none' );
	        	return;
	        }else{
	        	$("#validProduct").val(0);
	        	generateTableFromJsnList(result);
	        	$("#product").val(productSearch);
	        	//alert( "product id " + result.cryoPrepList[0]);
	        	$("#refProductId").val(result.cryoPrepList[0]);
	        	var jsonResult = jQuery.parseJSON(result.cryoData);
	        	$("#totalVolume").val(jsonResult.totalVolume);
	        	//alert("document" + jsonResult.documentId);
	        	$("#cryoPrep").val(jsonResult.cryoPrep);
	        	if($("#cryoPrep").val()=="" || $("#cryoPrep").val() ==" "){
	        		$("#cryoPrep").val(0);
	        	}
        		$("#hsaAdded").val(jsonResult.hsaAdded);
        		$("#cryoBagVolume").val(jsonResult.cryoBagVolume);
        		$("#referenceVials").val(jsonResult.referenceVials);
        		$("#noOfBags").val(jsonResult.noOfBags);
				if(jsonResult.curve1!=undefined ){
	        		$('iframe').contents().find("#strCurve1").val(jsonResult.curve1);
	        		$('iframe').contents().find("#strCurve2").val(jsonResult.curve2);
	        		$('iframe').contents().find("#strCurve3").val(jsonResult.curve3);
	        		$('iframe').contents().find("#documentId").val(jsonResult.documentId);
	        		drawGraph();
	        		$("#resetGraph").show();
				}else{
					$("#chart1").html("");
					$("#chart2").html("");
					$("#startingTemp").text("");       
					$("#finalTemp").text("");
					$("#resetGraph").hide();
					}

				$('#childcreation tbody tr').each(function(i,v){
					$(this).remove();
				});
				$("#childNo").attr('checked', 'checked');
				$("#childNo").trigger('click');
				var childLength=result.childList.length;
				if( childLength > 0){
					//alert("childLength " + childLength);
					
					
					$("#childYes").attr('checked', 'checked');
					$("#childYes").trigger('click');
					$("#norow").val(childLength);
					if(Createchildrows()){
						//$("#norow").val("");
						var i;
						$.each(result.childList, function(row,v) {
							i = row + 1;
							$("#prodcut_"+i).html(v[3]);
							$("#component_"+i).val(v[4]);
							
							$("#container_"+i).val(v[5]);
							$("#volid_"+i).val(v[6]);
							$("#wgtid_"+i).val(v[7]);
							$("#tncid_"+i).val(v[8]);
							$("#tncids_"+i).val(v[9]);
							$("#cdeid_"+i).val(v[10]);
							$("#cdekg_"+i).val(v[11]);
							$("#ide__"+i).val(v[12]);
							$("#comment_"+i).val(v[13]);
							
						});
						$.uniform.restore('select');
						$("select").uniform(); 
						
					}
					
					
				}
	        }
	        
	  /*      
		
		//alert("s : "+e.keyCode);
		var response = ajaxCall(url,"cryoPreparationForm");
		//alert(response);
		//alert(response.qcList);
		//response = jQuery.parseJSON(response.recipientDonorList);
		//alert(response);	
		//alert(response.returnJsonStr);
		generateTableFromJsnList(response);
		$('.progress-indicator').css( 'display', 'none' );
		*/
	    $('.progress-indicator').css( 'display', 'none' );
	}
	//response = jQuery.parseJSON(response.recipientDonorList);
	
}
/*Child Creation Starts*/
function showHide(This){
	$("#addtxt").hide();
	//$("#thSelectColumn").css('display: block');
	if(This.id=="childYes"){
		$("#childDiv").show();
		// $('#createchild'). find( ".portlet-header .ui-icon" ).removeClass('ui-icon-minusthick')addclass('ui-icon-plusthick');
		nchildrens();
		//$( ".portlet-header .ui-icon" ).trigger("click");
		}else{deleteallchild();
			//$("#childDiv").hide();
			//$( ".portlet-header .ui-icon" ).trigger("click");
	}
}
function addtxt(){
	$('#deleteallchild').attr('checked', false);
	$("#addtxt").show();
	//?BB $("#addtxt").html('<table><tr><td># of Children</td> &nbsp;&nbsp;&nbsp;<td><input type=text  id=norow style=\"width:40%\"  onkeypress="createcols(event);"/></td></tr></table>');
		}
 function createcols(e){	
	 if(e.keyCode==13){
		Createchildrows();
		e.returnValue = false;
		return false;
		}
	} 
 function checkall() { 
	var isChecked = $('#deleteallchild').attr('checked');
 	if(isChecked)
 		{
	 $('.deleterows').attr('checked', true);
 		}
 	else
 		{
 		$('.deleterows').attr('checked', false);	
 		}
 }
 var dynoptions = Array();
 //var header = Array()
function Createchildrows(){
	// alert("Createchildrows");
	 var Somewar="";
	var i=0; 
	var n=$("#norow").val();
	var dynrow=$('#childcreation tbody').find('tr').length;
	//alert("dynrow" + dynrow);
	var runningTabClass= $('#childcreation tbody tr td:nth-child(1)').attr("class");
	//alert("dynrow"+dynrow+"runningTabClass :"+runningTabClass);
	if(runningTabClass!="dataTables_empty"){
		dynrow++;
	}
	//alert("dynrow" + dynrow);
	var componentH_1 = $('#componentH_1').html();
	var containerH_1 = $('#containerH_1').html(); 
	var intendedProcessH_1 = $('#intendedProcessH_1').html();
	//alert(i + " / " + n);
	
	for(i;i<n;i++){
		Somewar+=("<tr>"+ 
				"<td style=\"width:7%\">&nbsp;&nbsp;</td>"+
		        "<td style=\"width:4%\"><input type='checkbox' class='deleterows'/></td>"+
		        "<td style=\"width:6%\"><label id=prodcut_"+dynrow+">"+$("#product").val()+"-"+dynrow+"</label></td>"+
		        "<td style=\"width:6%\"><div><select id=component_"+dynrow+" style=\"width:50%\">"+componentH_1+"</div></td>"+
				"<td style=\"width:6%\"><div><select id=container_"+dynrow+" style=\"width:50%\">"+containerH_1+"</select></div></td>"+
				"<td style=\"width:6%\"><input type=text  id=volid_"+dynrow+" style=\"width:75%\"/></td>"+
				"<td style=\"width:6%\"><input type=text  id=wgtid_"+dynrow+" style=\"width:75%\"/></td>"+
				"<td style=\"width:6%\"><input type=text  id=tncid_"+dynrow+" style=\"width:75%\"/></td>"+
				"<td style=\"width:6%\"><input type=text  id=tncids_"+dynrow+" style=\"width:75%\"/></td>"+
				"<td style=\"width:6%\"><input type=text  id=cdeid_"+dynrow+" style=\"width:75%\"/></td>"+
			    "<td style=\"width:6%\"><input type=text  id=cdekg_"+dynrow+" style=\"width:75%\"/></td>"+ 
				"<td style=\"width:6%\"><div><select id=ide__"+dynrow+" style=\"width:50%\">"+intendedProcessH_1+"</select></div></td>"+
				"<td style=\"width:6%\"><input type=text  id=comment_"+dynrow+" style=\"width:75%\"/></td>"+
				"</tr>");
		dynrow++;
		//alert("inside loop " + Somewar);
					}
				/* $("#childcreation thead tr th").each(function(i, v){
		        header[i] = $(this).attr('style');
		        	if(header[i].indexOf("display: none") >0){
		    	   $('.tbody').find('tr').children('td:nth-child('+(i+1)+')').hide();
		       }         	        
			 });  */
			  if(runningTabClass=="dataTables_empty"){
				 $('#childcreation tbody tr:first').remove(); 
			  }
	//	alert("Somewar " + Somewar);	 
				 $('#childcreation tbody').append(Somewar); 
				 $("select").uniform(); 
	$("#addtxt").hide();
    nchildrens();
    nextprotopt();
    return true;
 }
	//add to nextProtocolList
	function nextprotopt(){
		var dynrow=$('#childcreation tbody').find('tr').length;
		var i=0;
		var txtval=$('#norow').val();
		i=dynrow;	
		for(i=0;i<txtval;i++){
			$('#nextProtocolList').append("<option title='newadd'>"+dynoptions[i]+"</option>");
		}	
	} 
	var oTable1= $('#childcreation').dataTable();
	function deleteallchild(){
		 var dynrow=$('#childcreation tbody').find('tr').length;
    	 if($('#childNo').attr('checked')){
    		 if(dynrow>1){  
    			 $('#deleteallchild').attr('checked', true);
    		checkall();
    		deletechildrows('childcreation');
        	 //$('.deleterows').attr('checked', false);
        	 $('#createchild'). find( ".portlet-header .ui-icon" ).trigger("click");
        	 $("#childDiv").hide();
    		 return;   		 
    		 }
    	 }
        else{
        		 $('input[name=Yeschild]').attr('checked', false);
        		 $('#childYes').attr('checked', true);
        		 $("#childDiv").show();
        		 return;
        }
    		 $("#childDiv").hide();
    		 $("#nextProtocolList option[title='newadd']").remove(); 
        	 $("#nextdropdown").find("tbody").find("tr").find("td").find("#uniform-nextProtocolList").find("span").text("Select");
    }
    	
 function deletechildrows(id) {
	 var table = document.getElementById(id);
		try {
		   	var isChecked = $('#childcreation tbody').find('tr').length;
		   	var isCheckedall = $('#deleteallchild').attr('checked');
		   	if(isChecked>=1 && $('.deleterows').is(':checked')){
		   	if(isCheckedall){
		   		var yes=confirm('Do you want delete all products ?');	
		   	}
		   	else{var yes=confirm('Do you want delete selected products?');
		   	}
		   	var rowCount = $('#childcreation').find('tr').length;
		    if(yes){
		    	$('#childcreation tbody tr').each(function(i,v){
						var cbox=$(v).children("td:eq(1)").find(':checkbox').is(":checked");
						if(cbox){
							//oTable1.fnDeleteRow(this);
							$(this).remove();
						}
				});
			 $('.deleterows').attr('checked', false);
			$("#nextProtocolList option[title='newadd']").remove();
			$("#nextdropdown").find("tbody").find("tr").find("td").find("#uniform-nextProtocolList").find("span").text("Select");
		    }
		}
		     //rowCount = table.rows.length;
		     for(var i=1; i<rowCount; i++) {
					var row = table.rows[i];
			        var cbox = row.cells[2];
			        var labelText = $(cbox).find('label').text();
			        if(labelText!="" && labelText!=null && labelText!="undefined"){
			        	var indexL = labelText.indexOf('-');
			        	labelText = labelText.substr(0,indexL+1);
			        	var temp=i;
			        	temp=temp-1;
			        	$('#nextProtocolList').append("<option title='newadd'>"+labelText+temp+"</option>"); 
			        	labelText= labelText+temp;
			        	$(cbox).find('label').text(labelText);
			        }   
		    }
	    }catch(e) {
	       // alert(e);
	    }
	    $('#deleteallchild').attr('checked', false);
	    nchildrens();
	 }  
 var dynrows;
 function nchildrens(){
	 dynrows=$('#childcreation tbody').find('tr').length;
	 var len=$("#childcreation").find("tbody").find("tr").find("td").text();
	 if(len=='No data available in table')
		 {dynrows=0;
		 $("#nchild").html('<b>Total No of Children</b> &nbsp;'+'<b>'+dynrows+'</b>');
		 }
	  if(dynrows>=2)
		 {
		  $("#nchild").html('<b>Total No of Children</b> &nbsp;'+'<b>'+dynrows+'</b>');
		 } else {
	 $("#nchild").html('<b>Total No of Children</b> &nbsp;'+'<b>'+dynrows+'</b>');
		 }
 }
 /*Child Creation Ends*/

</script>

<div class="cleaner"></div>

<!--float window-->

	<div id="cryo_slidewidget"  style="display:none;">
				
<!-- recipient start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header "><s:hidden id="moduleName" value="cryoPrep"/><s:hidden id="widgetName" value="recipient"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.recipient"/></div>
						<div class="portlet-content">
						<table width="225" border="0">
						  <tr>
							<td width="89">ID:</td>
							<td width="120"><div id="recipientId"></div></td>
						  </tr>
						  <tr>
							<td>Name:</td>
							<td><div id="recipientName"></div></td>
						  </tr>
						  <tr>
							<td>ABO/Rh:</td>
							<td><div id="recipientAboRh"></div></td>
						  </tr>
						  <tr>
							<td>Diagnosis:</td>
							<td><div id="recipientDiagnosis"></div></td>
						  </tr>
						  <tr>
							<td>RID:</td>
							<td><div id="recipientRID"></div></td>
						  </tr>
					</table>
					</div>
					</div>
					</div>
				
<!-- recipient-->


<!-- Product start-->
				<div class="column">
				<div  class="portlet">
					<div class="portlet-header "><s:hidden id="moduleName" value="cryoPrep"/><s:hidden id="widgetName" value="product"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.product"/></div>
						<div class="portlet-content">
						<table width="225" border="0">
							<tr>
							<td width="89">ID:</td>
							<td width="120"><div id="productId"></div></td>
						  </tr>
						  <tr>
							<td>Type:</td>
							<td><div id="productType"></div></td>
						  </tr>
						</table>					
					</div>
				</div>
				</div>
<!-- Product-->

<!-- Donor start-->
					<div class="column">
					<div class="portlet">
						<div class="portlet-header "><s:hidden id="moduleName" value="cryoPrep"/><s:hidden id="widgetName" value="donor"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.donor"/></div>
						<div class="portlet-content">
							<table width="225" border="0">
								<tr>
									<td width="89">ID:</td>
									<td width="120"><div id="donorId"></div></td>
								</tr>
								<tr>
									<td>Name:</td>
									<td><div id="donorName"></div></td>
								</tr>
								<tr>
									<td>ABO/Rh:</td>
									<td><div id="donorAboRh"></div></td>
								</tr>
							</table>
						</div>
					</div>
					</div>
<!-- Donor-->
				
				</div>
			
<!--float window end-->
<div id="sidecont" style="display:none">
<div class="" >
<ul class="ulstyle">
<li class="listyle print_label" ><b>Label</b> </li>
<li class="listyle print_label"><img src = "images/icons/print_small.png"  style="width:30px;height:30px;"/></li>
<li class="listyle print_label" ><b>Calendar</b> </li>
<li class="listyle print_label"><img src = "images/icons/calendar.png"  style="width:30px;height:30px;"/></li>
<li class="listyle notes_icon"><b>Notes</b> </li>
<li class="listyle notes_icon"><img src = "images/icons/notes.png"  style="width:30px;height:30px;"/></li>

</ul>
</div>
</div>
<!--right window start -->	
<form action="#" id="cryoPreparationForm">
	<input type="hidden"name="cryoPrep" id="cryoPrep" value="0"/>
	<input type="hidden"name="validProduct" id="validProduct" value="0"/>
	<s:hidden name="product" id="product" value="%{product}"/>
	 <input type="hidden" name="refProductId" id="refProductId"  value='' />
	
	 <input type="hidden" name="cryoCurve1" id="cryoCurve1"  value='' />
     <input type="hidden" name="cryoCurve2" id="cryoCurve2"  value='' />
     <input type="hidden" name="cryoCurve3" id="cryoCurve3"  value='' />
     <input type="hidden" name="documentId" id="documentId"  value='' />
   
	
  <div class="column">	
		<div  class="portlet" id="barcodeScan">
			<div class="portlet-header">Barcode Scanning</div>
			<div class="portlet-content" id="scan">
				<div  id="button_wrapper">
					<div style="margin-left:40%;">
							<input class="scanboxSearch barcodeSearch" type="text" size="15" value="" name="productSearch" id="conformProductSearch" placeholder="Scan/Enter Product ID"/>
							<input class="scanboxSearch"  type="password" size="15" value="" name="fake_productSearch" id="fake_conformProductSearch" autocomplete="off" placeholder="Scan/Enter Product ID"  style="width:210px;display:none;" />
					</div>
				</div>
			</div>
	</div>	
</div>

<div class="cleaner"></div>



<div class="cleaner"></div>

<!-- <input type="hidden" name="productSearch" id="productSearch" value="BMT4657"/>  -->
<div class="column">		
		<div  class="portlet">
			<div class="portlet-header"><s:hidden id="moduleName" value="cryoPrep"/><s:hidden id="widgetName" value="cryoCalculations"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.cryocalculations"/></div>
			<!--<div id="button_wrapper"><input class="scanboxSearch" type="text" size="25" placeholder="Scan and Enter ID" name="search" /></div>-->
		<div class="portlet-content">
				<div id="table_inner_wrapper">
				<div style="display:none">	<!-- for Child creation table -->
					<s:select style="display:none" headerKey="" headerValue="Select"  headerKey="" headerValue="Select"  list="componentList" listKey="pkCodelst" listValue="description" name="componentH_1" id="componentH_1" />
					<s:select style="display:none" headerKey="" headerValue="Select"  headerKey="" headerValue="Select"  list="containerList" listKey="pkCodelst" listValue="description" name="containerH_1" id="containerH_1" />
					<s:select style="display:none" headerKey="" headerValue="Select"  headerKey="" headerValue="Select"  list="intendedProcessList" listKey="pkCodelst" listValue="description" name="intendedProcessH_1" id="intendedProcessH_1" />
				</div>
					<div id="left_table_wrapper">
						<div id="table_container_input">
							<div class="table_row_input1">
								<div class="table_cell_input1">Total Volume (ml):</div>
								<div class="table_cell_input2"><input type="text" id="totalVolume" name="totalVolume"  value="" placeholder="" /></div>												
							</div>		
							
							<div class="table_row_input2">
								<div class="table_cell_input1">Cryo Bag Volume (ml):</div>
								<div class="table_cell_input2">
								<s:select  headerKey="" headerValue="Select"  headerKey="" headerValue="Select"  list="cryoBagVolumeList" listKey="pkCodelst" listValue="description" name="cryoBagVolume" id="cryoBagVolume" onchange="javascript:fn_calcBagCount();"/>
								<!-- <select name="cryoBagVolume" id="cryoBagVolume" onchange="javascript:fn_cryoCalculation();"><option value="100" >100</option><option value="60">60</option></select> -->
							</div>	
							</div>
							
							<div class="table_row_input3">
								<div class="table_cell_input1">No.of.Bags:</div>
								<div class="table_cell_input2"><input type="text" id="noOfBags" name="noOfBags"  value="" placeholder=""/></div>
							</div>						
																
						</div>
					</div>			
					
					<div id="right_table_wrapper">
						<div id="table_container_input">
							<div class="table_row_input1">
								<div class="table_cell_input1">5% HSA Added to cell (ml):</div>
								<div class="table_cell_input2"><input type="text" id="hsaAdded" name="hsaAdded"  value="" placeholder=""/></div>
							</div>		
							
							<div class="table_row_input1">
								<div class="table_cell_input1">Reference Vials:</div>
								<div class="table_cell_input2">
								<s:select  headerKey="" headerValue="Select"  headerKey="" headerValue="Select"  list="referenceVialsList" listKey="pkCodelst" listValue="description" name="referenceVials" id="referenceVials" onchange="javascript:constructProductTable();"/>
							</div>												
							</div>	
						
							
						</div>
						
					</div>
				</div>

			
			</div>
		</div>
	</div>
</form>		
	
<div class="cleaner"></div>



<div class="cleaner"></div>
<form>
  <div class="column">	
		<div  class="portlet" id="createchild">
			<div class="portlet-header" ><s:hidden id="moduleName" value="originalProduct"/><s:hidden id="widgetName" value="childgeneration"/><s:hidden id="pkNotes" value=""/>Child Generation &nbsp;<input type="radio" id="childYes" name="Yeschild" onclick="showHide(this);" class=""/> Y &nbsp; <input type="radio" id="childNo" name="Yeschild" class="" onclick="showHide(this);" />N</div>
			<div class="portlet-content" id="childDiv">
			<table><tr>
			<td><img src = "images/icons/addnew.jpg" style="width:16;height:16;cursor:pointer;" onclick="addtxt();"/>&nbsp;&nbsp;<label id=""><b>Add Child</b></label></td>
			<td><div id="addtxt" style="width:180px;"><table><tr><td># of Children</td> &nbsp;&nbsp;&nbsp;<td><input type=text  id=norow style="width:30%"  onkeypress="createcols(event);"/></td></tr></table></div></td>&nbsp;&nbsp;<td><div><img src = "images/icons/no.png" style="width:16;height:16;cursor:pointer;" id ="" onclick="deletechildrows('childcreation');"/>&nbsp;&nbsp;<label id=""><b>Delete Child</b></label></div></td>
			<td width="27%" height="40px"></td>
			<td width="25%" height="40px"><div id="nchild" align="right"></div></td>
			</tr></table>
			<div style="overflow-x:auto;overflow-y:hidden;">
			<table cellpadding="0" cellspacing="0" border="1" id="childcreation" class="display ">
			<thead>
			<tr>	
							<th width="4%" id="childColumn"></th>
							<th colspan="1">Check all<input type="checkbox" name="" id="deleteallchild" onclick="checkall();"/></th>
							<th colspan="1">ProductId</th>
							<th colspan="1">Component</th>
							<th colspan="1">Container</th>
							<th colspan="1">Volume (ml)</th>
							<th colspan="1">Weight (kg)</th>
							<th colspan="1">TNC (*10^8)</th>
							<th colspan="1">TNC/kg (10^6/kg)</th>				
							<th colspan="1">CD34 (*10^6)</th>
							<th colspan="1">CD34/kg (*10^6/kg)</th>
							<th colspan="1">Intended Process</th>
							<th colspan="1">Comments</th>
			</tr>
				</thead>			
			</table>
			</div>
	</div>
	</div>	
</div>
</form>
<%--  <div class="column">		
		<div  class="portlet">
			<div class="portlet-header"><s:text name="stafa.label.productsummary"/></div>
			<div class="portlet-content">

			<table cellpadding="0" cellspacing="0" border="0" id="productTable" class="display">
					<thead>
						<tr>
							<th>Bag/Vial</th>
							<th>Volume</th>
							<th>TNC/kg</th>
							<th>CD34/kg</th>				
							<th>CD3/kg</th>
							<th>MNC/kg</th>
							<th>RBC/kg</th>
							<th>Stored</th>
						</tr>
					</thead>
					<tbody>
							
					
						
					</tbody>
			</table>
	</div>
	</div>	
</div>



<form action="#" id="login">
<div class="column">		
		<div  class="portlet">
			<div class="portlet-header notes"><s:hidden id="moduleName" value="cryoPrep"/><s:hidden id="widgetName" value="freezesolutionPrep"/><s:hidden id="pkNotes" value=""/><s:text name="stafa.label.freezesolutionprep"/></div>
		<div class="portlet-content">
			<div id="table_inner_wrapper">
			<div id="left_inner_wrapper">
				<div id="table_container_input">
					<div class="table_row_input1">
						<div class="table_cell_input1">HSA Needed (ml):</div>
						<div class="table_cell_input1" id="hsan" name="hsan"></div>
					</div>					
				</div>	
			</div>
			
			<div id="middle_line_2"></div>
			
			<div id="middle_inner_wrapper">
				<div id="table_container_input">
					<div class="table_row_input1">
						<div class="table_cell_input1">DMSO Needed (ml):</div>
						<div class="table_cell_input1" id="dmson" name="dmson"></div>
					</div>								
				</div>
			</div>
			
			<div id="middle_line_2"></div>
			
			<div id="right_inner_wrapper">
				<div id="table_container_input">
					<div class="table_row_input1">
						<div class="table_cell_input1">Final Freeze Solution (ml):</div>
						<div class="table_cell_input1" id="ffs" name="ffs"></div>
					</div>									
				</div>
			</div>
		</div>
			
			</div>
		</div>
	</div>
</form>	 --%>



<div class="column">		
		<div  class="portlet">
			<div class="portlet-header"><s:text name="stafa.label.freeze"/></div><br>
		<div class="portlet-content">
		
				<div id="table_inner_wrapper">
					<div id="left_table_wrapper">
						<div id="table_container_input">
							<div class="table_row_input1">
								<div class="table_cell_input1">Dummy Bag Test</div>
								<div class="table_cell_input2"><input type="checkbox" name=""></div>
							</div>	
							<div class="table_row_input1">
								<div class="table_cell_input1">CRF Serial #</div>
								<div class="table_cell_input2"><div class="mainselection"><select name="" id=""><option>abc123</option></select></div>
								</div>
							</div>	
							<div class="table_row_input1">
								<div class="table_cell_input1">Program</div>
								<div class="table_cell_input2"><div class="mainselection"><select name="" id=""><option>CTL-F1</option></select></div>
							</div>
							</div>	
							<div class="table_row_input1">
								<div class="table_cell_input1">Starting Temp.(C)</div>
								<div class="table_cell_input2"><span id="startingTemp"></span></div>
							</div>
							<div class="table_row_input1">
								<div class="table_cell_input1">Final Temp.(C)</div>
								<div class="table_cell_input2"><span id="finalTemp"></span></div>
							</div>
							<div class="table_row_input1">
								<div class="table_cell_input1">File : </div>
								<div class="table_cell_input2">
								<iframe  src="jsp/uploadFile.jsp" style="border: medium none;" id="uploadFrame" ></iframe>							
								</div>
							</div>
						</div>
						
						

					</div>
						
						
					</div>
					
					<div id="right_table_wrapper">
						
						<div id="table_container_input">
						
							<div class="table_row_input1">
								<div class="table_cell_input1 hidden">
								Time : <input type="text" id="time" style="width:100px"/><br/>
								Temp : <input type="text" id="temp" style="width:100px"/><br/>
								<input type="button" value="Update Graph" onclick="generateGraph1();"/>
								</div>
								
				
						<div class="table_cell_input2" align="left">

						 <div class="jqPlot" id="chart1" style="height:320px; width:600px;"></div>
						<div class="jqPlot" id="chart2" style="margin-top: 30px; height:100px; width:600px;"></div>
							<button class="hidden"  id="resetGraph" value="reset" onclick="controllerPlot.resetZoom();">Reset</button>
						 
							</div>						
						</div>
						
						
						
					</div>
					
					
				</div>
		   </form>			
			</div>
		</div>
	</div>	
	
<div class="cleaner"></div>

<section>

<div class="cleaner"></div>
		<div align="right" style="float:right;">
		<form>
				<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
				<tr>
				<td>Next:</td>
				<td><div> <select name="nextOption" id="nextOption">
                 	<option value="select">Select</option>
               		<s:iterator value="nextOptions" status="row" var="cellValue">  
	               		<s:if test="%{#cellValue[2] == 0}">
	                 		<option value="<s:property value="#cellValue[1]"/>"> <s:property value="#cellValue[0]"/></option>
	                 	</s:if>

	                 </s:iterator>
                 </select></div></td>
				<td><input type="text" style="width:80px;" size="5" value="" placeholder="e-Sign"/></td>
				<td><input type="button" value="Next" onClick="nextCryoPrep()"/></td>
				</tr>
				</table>
		</form>
		</div>
</section>
<script>
$(function() {
    $('.jclock').jclock();
    $( ".column" ).sortable().sortable({
        connectWith: ".column"
    });
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( ".portlet-header" )
    .addClass( "ui-widget-header ui-corner-all" ).prepend( "<span class='ui-icon ui-icon-minusthick'></span>");
   



    $( ".portlet-header .ui-icon" ).click(function() {
        $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
        $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });

    $('.searchtable').dataTable({"aaSorting": [[ 1, "asc" ]]});   
});
</script>

