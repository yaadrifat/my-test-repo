<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page import="com.velos.stafa.util.VelosStafaConstants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script>



function closePopup(){
	try{
		
		$("#recipientNewForm").reset();
		$("#modalpopup").dialog('close');
    	//jQuery("#addNewDonor").dialog("destroy");
    	
		//?BB stafaPopUp("close");
		
  	}catch(err){
		alert("err" + err);
	}
}

function updateAccountStatus(){
	//showPopUp("open","","","950","520");

	url="updateAccountStatus.action?accountStatus="+$("#accountStatus").val();

	$("#modalpopup").dialog({
		   autoOpen: false,
		   title: "Account Status",
		   modal: true, width:800, height:550,
		   close: function() {
			  jQuery("#modalpopup").dialog("destroy");
			  //?BB hideHighting('dataLibraryTbody');
		   }
		}); 
	$.ajax({
        type: "POST",
        url: url,
       // async:false,
        success: function (result, status, error){
        	//$('.progress-indicator').css( 'display', 'block' );	
        	$("#modalpopup").html(result);
        	//$('.progress-indicator').css( 'display', 'none' );
               $("select").uniform();
                      
        },
        error: function (request, status, error) {
            alert("Error " + error);
           // alert(request.responseText);
    }

});
	
	$("#modalpopup").dialog("open");
	$("#modalpopup").css("width","95%");
	$('.ui-dialog select').uniform();
	$("#error").hide();
}
</script>

<table cellpadding="0" cellspacing="0" class="" align="center" border="0" width="100%">
       		            	<tr>
       		            		<td class="tablewapper"><s:text name = "stafa.label.accountStatus"/></td>
       		            		<td class="tablewapper1"><s:select cssStyle = "width:auto" id="accountStatus" disabled="true"  value = "accountStatus"  name="accountStatus" list="#application.codeListValues[@com.velos.stafa.util.VelosStafaConstants@ACCOUNT_STATUS]" listKey="pkCodelst" listValue="description"  headerKey="" headerValue="Select" /></td> 
       		            		
       		            	</tr>
       		            	<tr align="center">
       		            		<td><input type="button"  value="Update" onclick="updateAccountStatus()"/></td>
       		            	</tr>
       		            	
       		            </table>