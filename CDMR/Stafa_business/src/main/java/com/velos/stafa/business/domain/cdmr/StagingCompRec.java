package com.velos.stafa.business.domain.cdmr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.velos.stafa.business.domain.BaseDomainObject1;

/**
 * @summary     StagingMain
 * @description Mapping class of CDM_STAGINGCOMPREC
 * @version     1.0
 * @file        StagingMain.java
 * @author      Lalit Chattar
 */

@Entity
@Table(name="CDM_STAGINGCOMPREC")
@javax.persistence.SequenceGenerator(name="comprec", sequenceName="SEQ_CDM_STAGINGCOMPREC")
public class StagingCompRec extends BaseDomainObject1{
	
	private Long pkStagingComRec;
	private Long fkStagingMain;
	private Long fkStagingDetail;
	private String instCode;
	private String compCol1;
	private String compCol2;
	private String compCol3;
	private String compCol4;
	private String compCol5;
	private String compCol6;
	private String compCol7;
	private String compCol8;
	private String compCol9;
	private String compCol10;
	private String compCol11;
	private String compCol12;
	private String compCol13;
	private String compCol14;
	private String compCol15;
	private String compCol16;
	private String compCol17;
	private String compCol18;
	private String compCol19;
	private String compCol20;
	private String compCol21;
	private String compCol22;
	private String compCol23;
	private String compCol24;
	private String compCol25;
	private String compCol26;
	private String compCol27;
	private String compCol28;
	private String compCol29;
	private String compCol30;
	private String compCol31;
	private String compCol32;
	private String compCol33;
	private String compCol34;
	private String compCol35;
	private String compCol36;
	private String compCol37;
	private String compCol38;
	private String compCol39;
	private String compCol40;
	private String comColExt;
	private Long curStatus;
	
	@Id
	@GeneratedValue(generator="comprec",strategy=GenerationType.AUTO)
	@Column(name="PK_CDM_STAGINGCOMPREC")
	public Long getPkStagingComRec() {
		return pkStagingComRec;
	}
	public void setPkStagingComRec(Long pkStagingComRec) {
		this.pkStagingComRec = pkStagingComRec;
	}
	
	@Column(name="FK_CDM_STAGINGMAIN")
	public Long getFkStagingMain() {
		return fkStagingMain;
	}
	public void setFkStagingMain(Long fkStagingMain) {
		this.fkStagingMain = fkStagingMain;
	}
	
	@Column(name="FK_CDM_STAGINGDETAIL")
	public Long getFkStagingDetail() {
		return fkStagingDetail;
	}
	public void setFkStagingDetail(Long fkStagingDetail) {
		this.fkStagingDetail = fkStagingDetail;
	}
	
	@Column(name="INST_CODE")
	public String getInstCode() {
		return instCode;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}
	
	@Column(name="COMP_COL1")
	public String getCompCol1() {
		return compCol1;
	}
	public void setCompCol1(String compCol1) {
		this.compCol1 = compCol1;
	}
	
	@Column(name="COMP_COL2")
	public String getCompCol2() {
		return compCol2;
	}
	public void setCompCol2(String compCol2) {
		this.compCol2 = compCol2;
	}
	
	@Column(name="COMP_COL3")
	public String getCompCol3() {
		return compCol3;
	}
	public void setCompCol3(String compCol3) {
		this.compCol3 = compCol3;
	}
	
	@Column(name="COMP_COL4")
	public String getCompCol4() {
		return compCol4;
	}
	public void setCompCol4(String compCol4) {
		this.compCol4 = compCol4;
	}
	
	@Column(name="COMP_COL5")
	public String getCompCol5() {
		return compCol5;
	}
	public void setCompCol5(String compCol5) {
		this.compCol5 = compCol5;
	}
	
	@Column(name="COMP_COL6")
	public String getCompCol6() {
		return compCol6;
	}
	public void setCompCol6(String compCol6) {
		this.compCol6 = compCol6;
	}

	@Column(name="COMP_COL7")
	public String getCompCol7() {
		return compCol7;
	}
	public void setCompCol7(String compCol7) {
		this.compCol7 = compCol7;
	}
	
	@Column(name="COMP_COL8")
	public String getCompCol8() {
		return compCol8;
	}
	public void setCompCol8(String compCol8) {
		this.compCol8 = compCol8;
	}
	
	@Column(name="COMP_COL9")
	public String getCompCol9() {
		return compCol9;
	}
	public void setCompCol9(String compCol9) {
		this.compCol9 = compCol9;
	}
	
	@Column(name="COMP_COL10")
	public String getCompCol10() {
		return compCol10;
	}
	public void setCompCol10(String compCol10) {
		this.compCol10 = compCol10;
	}
	
	@Column(name="COMP_COL11")
	public String getCompCol11() {
		return compCol11;
	}
	public void setCompCol11(String compCol11) {
		this.compCol11 = compCol11;
	}
	
	@Column(name="COMP_COL12")
	public String getCompCol12() {
		return compCol12;
	}
	public void setCompCol12(String compCol12) {
		this.compCol12 = compCol12;
	}
	
	@Column(name="COMP_COL13")
	public String getCompCol13() {
		return compCol13;
	}
	public void setCompCol13(String compCol13) {
		this.compCol13 = compCol13;
	}
	
	@Column(name="COMP_COL14")
	public String getCompCol14() {
		return compCol14;
	}
	public void setCompCol14(String compCol14) {
		this.compCol14 = compCol14;
	}
	
	@Column(name="COMP_COL15")
	public String getCompCol15() {
		return compCol15;
	}
	public void setCompCol15(String compCol15) {
		this.compCol15 = compCol15;
	}
	
	@Column(name="COMP_COL16")
	public String getCompCol16() {
		return compCol16;
	}
	public void setCompCol16(String compCol16) {
		this.compCol16 = compCol16;
	}
	
	@Column(name="COMP_COL17")
	public String getCompCol17() {
		return compCol17;
	}
	public void setCompCol17(String compCol17) {
		this.compCol17 = compCol17;
	}
	
	@Column(name="COMP_COL18")
	public String getCompCol18() {
		return compCol18;
	}
	public void setCompCol18(String compCol18) {
		this.compCol18 = compCol18;
	}
	
	@Column(name="COMP_COL19")
	public String getCompCol19() {
		return compCol19;
	}
	public void setCompCol19(String compCol19) {
		this.compCol19 = compCol19;
	}
	
	@Column(name="COMP_COL20")
	public String getCompCol20() {
		return compCol20;
	}
	public void setCompCol20(String compCol20) {
		this.compCol20 = compCol20;
	}
	
	@Column(name="COMP_COL21")
	public String getCompCol21() {
		return compCol21;
	}
	public void setCompCol21(String compCol21) {
		this.compCol21 = compCol21;
	}
	
	@Column(name="COMP_COL22")
	public String getCompCol22() {
		return compCol22;
	}
	public void setCompCol22(String compCol22) {
		this.compCol22 = compCol22;
	}
	
	@Column(name="COMP_COL23")
	public String getCompCol23() {
		return compCol23;
	}
	public void setCompCol23(String compCol23) {
		this.compCol23 = compCol23;
	}
	
	@Column(name="COMP_COL24")
	public String getCompCol24() {
		return compCol24;
	}
	public void setCompCol24(String compCol24) {
		this.compCol24 = compCol24;
	}
	
	@Column(name="COMP_COL25")
	public String getCompCol25() {
		return compCol25;
	}
	public void setCompCol25(String compCol25) {
		this.compCol25 = compCol25;
	}
	
	@Column(name="COMP_COL26")
	public String getCompCol26() {
		return compCol26;
	}
	public void setCompCol26(String compCol26) {
		this.compCol26 = compCol26;
	}
	
	@Column(name="COMP_COL27")
	public String getCompCol27() {
		return compCol27;
	}
	public void setCompCol27(String compCol27) {
		this.compCol27 = compCol27;
	}
	
	@Column(name="COMP_COL28")
	public String getCompCol28() {
		return compCol28;
	}
	public void setCompCol28(String compCol28) {
		this.compCol28 = compCol28;
	}
	
	@Column(name="COMP_COL29")
	public String getCompCol29() {
		return compCol29;
	}
	public void setCompCol29(String compCol29) {
		this.compCol29 = compCol29;
	}
	
	@Column(name="COMP_COL30")
	public String getCompCol30() {
		return compCol30;
	}
	public void setCompCol30(String compCol30) {
		this.compCol30 = compCol30;
	}
	
	@Column(name="COMP_COL31")
	public String getCompCol31() {
		return compCol31;
	}
	public void setCompCol31(String compCol31) {
		this.compCol31 = compCol31;
	}
	
	@Column(name="COMP_COL32")
	public String getCompCol32() {
		return compCol32;
	}
	public void setCompCol32(String compCol32) {
		this.compCol32 = compCol32;
	}
	
	@Column(name="COMP_COL33")
	public String getCompCol33() {
		return compCol33;
	}
	public void setCompCol33(String compCol33) {
		this.compCol33 = compCol33;
	}
	
	@Column(name="COMP_COL34")
	public String getCompCol34() {
		return compCol34;
	}
	public void setCompCol34(String compCol34) {
		this.compCol34 = compCol34;
	}
	
	@Column(name="COMP_COL35")
	public String getCompCol35() {
		return compCol35;
	}
	public void setCompCol35(String compCol35) {
		this.compCol35 = compCol35;
	}
	
	@Column(name="COMP_COL36")
	public String getCompCol36() {
		return compCol36;
	}
	public void setCompCol36(String compCol36) {
		this.compCol36 = compCol36;
	}
	
	@Column(name="COMP_COL37")
	public String getCompCol37() {
		return compCol37;
	}
	public void setCompCol37(String compCol37) {
		this.compCol37 = compCol37;
	}
	
	@Column(name="COMP_COL38")
	public String getCompCol38() {
		return compCol38;
	}
	public void setCompCol38(String compCol38) {
		this.compCol38 = compCol38;
	}
	
	@Column(name="COMP_COL39")
	public String getCompCol39() {
		return compCol39;
	}
	public void setCompCol39(String compCol39) {
		this.compCol39 = compCol39;
	}
	
	@Column(name="COMP_COL40")
	public String getCompCol40() {
		return compCol40;
	}
	public void setCompCol40(String compCol40) {
		this.compCol40 = compCol40;
	}
	
	@Column(name="COMP_COLEXT")
	public String getComColExt() {
		return comColExt;
	}
	public void setComColExt(String comColExt) {
		this.comColExt = comColExt;
	}
	
	@Column(name="CUR_STATUS")
	public Long getCurStatus() {
		return curStatus;
	}
	public void setCurStatus(Long curStatus) {
		this.curStatus = curStatus;
	}
	
	
}