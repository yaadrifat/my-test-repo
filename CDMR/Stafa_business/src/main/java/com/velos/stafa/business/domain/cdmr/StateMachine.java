package com.velos.stafa.business.domain.cdmr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.velos.stafa.business.domain.BaseDomainObject;

@Entity
@Table(name="VCDM_STATEMACHINE")
@javax.persistence.SequenceGenerator(name="statemachine", sequenceName="SEQ_VCDM_STATEMACHINE")
public class StateMachine extends BaseDomainObject{
	
	private Long pkStateMachine;
	private Long fkAccount;
	private String entityType;
	private String stateType;
	private String curStatus;
	private String nxtPossibleState;
	private String actionCodeUrl;
	private Integer statePref;
	private Long rid;
	
	@Id
	@GeneratedValue(generator="statemachine",strategy=GenerationType.AUTO)
	@Column(name="PK_STATEMACHINE")
	public Long getPkStateMachine() {
		return pkStateMachine;
	}
	public void setPkStateMachine(Long pkStateMachine) {
		this.pkStateMachine = pkStateMachine;
	}
	
	@Column(name="FK_ACCOUNT")
	public Long getFkAccount() {
		return fkAccount;
	}
	public void setFkAccount(Long fkAccount) {
		this.fkAccount = fkAccount;
	}
	
	@Column(name="ENTITY_TYPE")
	public String getEntityType() {
		return entityType;
	}
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
	
	@Column(name="STATE_TYPE")
	public String getStateType() {
		return stateType;
	}
	public void setStateType(String stateType) {
		this.stateType = stateType;
	}
	
	@Column(name="CUR_STATE")
	public String getCurStatus() {
		return curStatus;
	}
	public void setCurStatus(String curStatus) {
		this.curStatus = curStatus;
	}
	
	@Column(name="NXT_POSS_STATE")
	public String getNxtPossibleState() {
		return nxtPossibleState;
	}
	public void setNxtPossibleState(String nxtPossibleState) {
		this.nxtPossibleState = nxtPossibleState;
	}
	
	@Column(name="ACTIONCODE_URL")
	public String getActionCodeUrl() {
		return actionCodeUrl;
	}
	public void setActionCodeUrl(String actionCodeUrl) {
		this.actionCodeUrl = actionCodeUrl;
	}
	
	@Column(name="STATE_PREF")
	public Integer getStatePref() {
		return statePref;
	}
	public void setStatePref(Integer statePref) {
		this.statePref = statePref;
	}
	
	@Column(name="RID")
	public Long getRid() {
		return rid;
	}
	public void setRid(Long rid) {
		this.rid = rid;
	}
	
	
}