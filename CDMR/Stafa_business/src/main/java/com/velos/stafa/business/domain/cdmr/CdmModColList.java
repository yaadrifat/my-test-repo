package com.velos.stafa.business.domain.cdmr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CDM_MOD_COLS")
@javax.persistence.SequenceGenerator(name="cdmmodcols_sqnc", sequenceName="SEQ_CDM_MOD_COLS")
public class CdmModColList {
	private Long pkCdmModCol;
	private Long fkCdmModule;
	private String columnName;
	private String columnProp;
	private String columnPropVal;
	private Integer colSeq;
	private Integer dispStatus;
	
	@Id
	@GeneratedValue(generator="cdmmodcols_sqnc",strategy=GenerationType.AUTO)
	@Column(name="PK_CDM_MOD_COLS")
	public Long getPkCdmModCol() {
		return pkCdmModCol;
	}
	public void setPkCdmModCol(Long pkCdmModCol) {
		this.pkCdmModCol = pkCdmModCol;
	}
	@Column(name="FK_CDM_MODULE")
	public Long getFkCdmModule() {
		return fkCdmModule;
	}
	public void setFkCdmModule(Long fkCdmModule) {
		this.fkCdmModule = fkCdmModule;
	}
	@Column(name="COLUMN_NAME")
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	@Column(name="COLUMN_PROP")
	public String getColumnProp() {
		return columnProp;
	}
	public void setColumnProp(String columnProp) {
		this.columnProp = columnProp;
	}
	@Column(name="COLUMN_PROP_VAL")
	public String getColumnPropVal() {
		return columnPropVal;
	}
	public void setColumnPropVal(String columnPropVal) {
		this.columnPropVal = columnPropVal;
	}
	@Column(name="COL_SEQ")
	public Integer getColSeq() {
		return colSeq;
	}
	public void setColSeq(Integer colSeq) {
		this.colSeq = colSeq;
	}
	@Column(name="DISP_STATUS")
	public Integer getDispStatus() {
		return dispStatus;
	}
	public void setDispStatus(Integer dispStatus) {
		this.dispStatus = dispStatus;
	}
	
	
	
}
