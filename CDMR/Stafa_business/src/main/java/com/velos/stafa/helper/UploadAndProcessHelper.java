package com.velos.stafa.helper;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import atg.taglib.json.util.JSONObject;

import com.velos.stafa.business.domain.cdmr.CodeList;
import com.velos.stafa.business.domain.cdmr.MapVersion;
import com.velos.stafa.business.domain.cdmr.StagingMaps;

import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.CDMRConstant;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.util.CDMRConstant;

public class UploadAndProcessHelper{
	
	
	/**
	 * @description Store file information in json object
	 * @param jsonObject
	 * @return jsonObject
	 * @throws Exception
	 */
	public static JSONObject getFileInfo(JSONObject jsonObject, HttpSession httpSession) throws Exception{
		
		List<Object> codeList = (List<Object>)httpSession.getAttribute("codelist");
		Map<Integer, String> criteria = new HashMap<Integer, String>();
		criteria.put(CDMRConstant.CODELST_TYPE, "filetgtloc");
		criteria.put(CDMRConstant.CODELST_SUBTYP, "defl");
		CodeList codeListObj = CDMRUtil.fetchFromCodeList(codeList, criteria);
		String dirPath = codeListObj.getCodeListCustomCol();
		File file = new File(dirPath);
		if(!file.exists()){
			file.mkdirs();
		}
		
		criteria.put(CDMRConstant.CODELST_TYPE, "filestatus");
		criteria.put(CDMRConstant.CODELST_SUBTYP, "new");
		codeListObj = CDMRUtil.fetchFromCodeList(codeList, criteria);
		jsonObject.put("fileDest", dirPath);
		jsonObject.put("filename", jsonObject.getString("filename"));
		jsonObject.put("fileDes", jsonObject.getString("filename"));
		jsonObject.put("fileCurrentStatus", codeListObj.getPkCodeList());
		jsonObject.put("comment", codeListObj.getCodeListDesc());
		jsonObject.put("filelocation", dirPath + "/" + jsonObject.getString("filename"));
		jsonObject.put("fileid", "abc");
		return jsonObject;
	}
	
	public static JSONObject getMapVersion(JSONObject jsonObject, String fileId, 
			CDMRController cdmrController) throws Exception{
	
		//String query = CDMRSqlConstants.ACTIVE_MAP_VERSION + " and FILE_ID = '" + fileID + "'";
		String query = "select * from CDM_MAPSVERSION where IS_ACTIVE=1 and DELETEDFLAG = 0 and FILE_ID = '" + fileId + "'";
		jsonObject.put("query", query);
		jsonObject.put("SQLQuery", true);
		jsonObject = cdmrController.getActiveMapVesion(jsonObject);
		
		List<Map> list = (List<Map>)jsonObject.get("mapversion");
		for (Map map : list) {
			
			Iterator it = map.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pairs = (Map.Entry)it.next();
		        if(!(pairs.getValue() == null)){
		        	jsonObject.put(pairs.getKey().toString(), pairs.getValue().toString());
		        }
		    }
		}
		return jsonObject;
		
	}
	

	/**
	 * @description Store file information in jsonObject
	 * @param row
	 * @param jsonObject
	 * @return JSONObject
	 * @throws Exception
	 */
	public static JSONObject saveStagingInfo(JSONObject jsonObject,  HttpSession httpSession
			,CDMRController cdmrController) throws Exception{
		List<Object> codeList = (List<Object>)httpSession.getAttribute("codelist");
		Map<Integer, String> criteria = new HashMap<Integer, String>();
		criteria.put(CDMRConstant.CODELST_TYPE, "stagingtype");
		criteria.put(CDMRConstant.CODELST_SUBTYP, "file");
		CodeList codeListObj = CDMRUtil.fetchFromCodeList(codeList, criteria);
		jsonObject.put("processedBy", jsonObject.getInt("createdBy"));
		jsonObject.put("processedDate", jsonObject.getString("createdOn"));
		jsonObject.put("stagingType", codeListObj.getPkCodeList());
		jsonObject.put("stagingID", getStagingID(cdmrController));
		jsonObject.put("fkMapVersion", jsonObject.getString("MAPSVERSION"));
		jsonObject.put("fkAccount", CDMRUtil.getFkAccount());
		return jsonObject;
	}
	
	public static Long getStagingID(CDMRController cdmrController) throws Exception{
		JSONObject jsonObject = new JSONObject();
		String query = CDMRSqlConstants.MAX_STAGING_ID;
		jsonObject.put("query", query);
		jsonObject.put("SQLQuery", true);
		jsonObject = cdmrController.getStagingID(jsonObject);
		
		Map obj = ((List<Map>)jsonObject.get("staginid")).get(0);
		if((obj.get("STAGINGID")) == null)
			return Long.parseLong("1");
		else
			return Long.valueOf(obj.get("STAGINGID").toString()) + 1;

	}
	
	public static JSONObject getCompReqColumnMapDetail(JSONObject jsonObject,CDMRController cdmrController) throws Exception{
		String query = CDMRSqlConstants.GET_STAGING_COLUMN_MAP + " where fkMapVersion = " + jsonObject.getString("MAPSVERSION");
		jsonObject.put("query", query);
		jsonObject.put("SQLQuery", false);
		jsonObject = cdmrController.getCompReqColumnMapDetail(jsonObject);
		List<StagingMaps> mapsObjList = (List<StagingMaps>)jsonObject.get("compreccolumnmap");
		Map mappingObj = new HashMap();
		for (StagingMaps stagingMaps : mapsObjList) {
			mappingObj.put(stagingMaps.getCompColID(), getColumns(stagingMaps.getPullFromCols()));
		}
		jsonObject.put("comprMAP", mappingObj);
		return jsonObject;
	}
	
	public static List getColumns(String colunNames) throws Exception{
		
		String names[] = colunNames.split("\\+");
		String cname = "";
		List<String> mappingList = new ArrayList<String>();
		if(names.length > 1){
			for (int i = 0; i < names.length; i++) {
				if(names[i].contains("DATA_COL")){
					mappingList.add(names[i].replace("DATA_COL", "").toString());
				}else if(names[i].equals("DDE")){
					mappingList.add("DDE");
				}else if(names[i].endsWith("INST")){
					mappingList.add("INST");
				}
			}
		
		}else if(names[0].contains("DATA_COL")){
			mappingList.add(names[0].replace("DATA_COL", "").toString());
		}
		else if(names[0].contains("DDE")){
			mappingList.add("DDE");
		}else if(names[0].contains("INST")){
			mappingList.add("INST");
		}
		
		return mappingList;
	}
	
	public static JSONObject getStagingColumnMapDetail(JSONObject jsonObject, CDMRController cdmrController) throws Exception{
		String query = CDMRSqlConstants.GET_STAGING_COLUMN_MAP + " where fkMapVersion = " + jsonObject.getString("MAPSVERSION");
		jsonObject.put("query", query);
		jsonObject.put("SQLQuery", false);
		jsonObject = cdmrController.getStagingColumnMapDetail(jsonObject);
		return jsonObject;
	}
	
	public static JSONObject createDynamicQuery(JSONObject jsonObject) throws Exception{
		List<StagingMaps> mapsObjList = (List<StagingMaps>)jsonObject.get("stagingcolumnmap");
		StringBuilder query = new StringBuilder();
		StringBuilder insertquery = new StringBuilder();
		int index = 1;
		query.append("select (SEQ_CDM_STAGINGCOMPREC.NEXTVAL) as pk, FK_CDM_STAGINGMAIN, PK_CDM_STAGINGDETAIL, ");
		insertquery.append("insert into CDM_STAGINGCOMPREC( PK_CDM_STAGINGCOMPREC, FK_CDM_STAGINGMAIN, FK_CDM_STAGINGDETAIL, ");
		
		for (StagingMaps stagingMaps : mapsObjList) {
			//System.out.println(stagingMaps.getCompColName());
			//System.out.println(stagingMaps.getCompColID());
			insertquery.append(stagingMaps.getEqCompReqCol() + ",");
			query.append("("+getColumnsNames(stagingMaps.getPullFromCols())).append(") as COL"+index+",");
			index++;
		}
		query = query.deleteCharAt(query.length()-1);
		insertquery = insertquery.deleteCharAt((insertquery.length()-1));
		insertquery.append(")");
		//int iDisLen = Integer.parseInt(jsonObject.getString("iDisplayLength")) * Integer.parseInt(jsonObject.getString("sEcho"));
		query.append(" from CDM_STAGINGDETAIL WHERE RECORD_TYPE = 'D' and FK_CDM_STAGINGMAIN = " + jsonObject.getString("stagingMainID"));
		jsonObject.put("query", insertquery + " "+ query);
		System.out.println("query"+ insertquery + " "+ query);
		return jsonObject;
	}
	
	public static String getColumnsNames(String colunNames) throws Exception{
		String cname = "";
		String names[];
		String $names[];
		try{
			names = colunNames.split("\\+");
			$names = colunNames.split("\\$");
		}catch (Exception e) {
			cname = "' '";
			return cname;
		}
		
		
		if($names.length > 1){
			if($names[0].equals("SUBSTR")){
				cname = "SUBSTR("+$names[1]+"," + $names[2] + "," + $names[3] + ")";
				return cname;
			}
		}
		
		if(names.length > 1){
			for (int i = 0; i < names.length; i++) {
				cname = cname + names[i] + " ||" ;
			}
			cname = cname.substring(0, cname.length()-3);
			cname = cname ;
			return cname;
		
		}
		else if(names[0].equals("INST")){
			cname = "INST_CODE";
			return cname;
		}else if(names[0].equals("DDE") || names[0].equals("") || names[0] == null){
			cname = "' '";
			return cname;
		}else if(names[0].equals("DDEI")){
			cname = "0";
			return cname;
		}else{
			return names[0];
		}
		
		
	}
	
	
}