/**
 * 
 */
package com.velos.stafa.business.util;

/**
 * @author akajeera
 *
 */
public interface VelosConstants {
	public static final String STAFA_PROPERTYFILE="Stafa";
	public static final String ESECURITY_URL="ESECURITY_URL";
	
	public static final String CODELST_PRODUCT_STATUS_TYPE="productstatus";
	public static final String PRODUCT_STATUS_ACTIVE="active";
	public static final String PRODUCT_STATUS_INACTIVE="inactive";
	public static final String PRODUCT_STATUS_ACQUIRED="acquired";
	public static final String PRODUCT_STATUS_ACCESSED="accessed";
	public static final String PRODUCT_STATUS_PROCESSED="processed";
	public static final String PRODUCT_STATUS_CRYOPREP="cryoprepared";
	public static final String PRODUCT_STATUS_STORED="stored";
	public static final String PRODUCT_STATUS_TRANSFERED="transfered";
	
	//public static final String CODELST_DOMAIN="com.velos.stafa.business.domain.CodeList";
	public static final String CODELST_ID = "pkCodelst"; 
	public static final String ACQUISITION_DOMAIN="com.velos.stafa.business.domain.Acquisition";
	public static final String ACQUISITION_ID = "pkAcquisition";
	public static final String PRODUCT_DOMAIN="com.velos.stafa.business.domain.Product";
	public static final String PRODUCT_ID = "pkProduct";
	public static final String PERSONPRODUCT_DOMAIN="com.velos.stafa.business.domain.PersonProduct";
	public static final String PERSONPRODUCT_ID = "pkPersonProduct";
	public static final String SUCCESS_MESSAGE = "Data Saved Successfully";
	public static final String SPECIMEN_DOMAIN="com.velos.stafa.business.domain.Specimen";
	public static final String SPECIMEN_ID = "pkSpecimen";
	public static final String SPECIMEN_FKID = "fkSpecimen";
	public static final String SPECIMEN_STATUS = "specStatus";
	public static final String ERTEMPPRODUCT_DOMAIN="com.velos.stafa.business.domain.ErTempProduct";//ErTempProduct
	public static final String ERTEMPPRODUCT_ID = "personproductid";
	public static final String ACCESSION_DOMAIN="com.velos.stafa.business.domain.Accession";
	public static final String ACCESSSION_ID = "pkAccession";
	public static final String PERSON_DOMAIN="com.velos.stafa.business.domain.Person";
	public static final String PERSON_ID = "pkPerson";
	public static final String PERSON_DOMAIN_NAME = "Person";
	public static final String COLLECTION_DOMAIN="com.velos.stafa.business.domain.Collection";
	public static final String COLLECTION_ID = "pkCollection";
	public static final String DOCUMENTS_DOMAIN="com.velos.stafa.business.domain.Documents";
	public static final String DOCUMENTS_ID = "pkDocumentId";
	public static final String FK_DONOR_ID = "fkDonorId";
	public static final String FK_RECIPIENT_ID = "fkRecipient";
	public static final String ACQUIREDSTATUS = "Acquired";
	public static final String ALIAS_DOMAIN="com.velos.stafa.business.domain.Alias";
	public static final String NOTES_DOMAIN="com.velos.stafa.business.domain.Notes";
	public static final String HQLSTR="HQL";
	public static final String DOMAIN_DONOR="com.velos.stafa.business.domain.apheresis.Donor";
	public static final String DONOR_ID = "donorId";
	public static final String DONOR = "personId";
	public static final String PRODUCT = "productId";
	public static final String RECIPIENT = "recipient";
	
	/************************** CDMR SPECIFIC CONSTANT************/
	public static final String CDMR_FILE_DOMAIN = "com.velos.stafa.business.domain.cdmr.FilesLibrary";
	public static final String CDMR_STAGING_MAIN = "com.velos.stafa.business.domain.cdmr.StagingMain";
	public static final String CDMR_STAGING_DETAIL = "com.velos.stafa.business.domain.cdmr.StagingDetail";
	public static final String CDMR_COMPREC = "com.velos.stafa.business.domain.cdmr.StagingCompRec";
	public static final String CDM_VERSION_DEF = "com.velos.stafa.business.domain.cdmr.CdmVersionDef";
	public static final String CDM_SSLINK = "com.velos.stafa.business.domain.cdmr.CdmssLink";
	public static final String CDM_MAIN = "com.velos.stafa.business.domain.cdmr.CdmMain";
	public static final String CDMR_GRUP_DOMAIN ="com.velos.stafa.business.domain.cdmr.GrpCode";
	public static final String CDMR_NOTIFICATION_DOMAIN ="com.velos.stafa.business.domain.cdmr.CdmNotifycnf";
	public static final String CDMR_ERROR_LOG ="com.velos.stafa.business.domain.cdmr.ErrorLog";
	public static final String CODELST_DOMAIN="com.velos.stafa.business.domain.cdmr.CodeList";
	public static final String CDMR_NOTIFICATION="com.velos.stafa.business.domain.cdmr.CdmNotification";
	public static final String CDM_OBI_SVCBFR="com.velos.stafa.business.domain.cdmr.ObiSvcBufr";
	public static final String CDMR_STAGING_TEMP = "com.velos.stafa.business.domain.cdmr.StagingTemp";
	public static final String CDMR_FILTER_MAINT = "com.velos.stafa.business.domain.cdmr.CdmFilterMaint";
	public static final String CDMR_FILTER_PARAM = "com.velos.stafa.business.domain.cdmr.CdmFilterParam";	
}
