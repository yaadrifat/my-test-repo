package com.velos.stafa.business.domain.cdmr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.velos.stafa.business.domain.BaseDomainObject;

@Entity
@Table(name="CDM_SSLINK")
@javax.persistence.SequenceGenerator(name="sslink_sqnc", sequenceName="SEQ_CDM_SSLINK")
public class CdmssLink {
	
	private Long pkCdmssLink;
	private String ssID;
	private String ssChildId;
	private Integer isChildSS;
	private Integer isInclude;
	private Integer deletedFlag;
	private Long rid;
	private String eventlibid;
	
	
	@Id
	@GeneratedValue(generator="sslink_sqnc",strategy=GenerationType.AUTO)
	@Column(name="PK_CDM_SSLINK")
	public Long getPkCdmssLink() {
		return pkCdmssLink;
	}
	public void setPkCdmssLink(Long pkCdmssLink) {
		this.pkCdmssLink = pkCdmssLink;
	}
	
	@Column(name="SS_ID")
	public String getSsID() {
		return ssID;
	}
	public void setSsID(String ssID) {
		this.ssID = ssID;
	}
	
	@Column(name="SS_CHILD_ID")
	public String getSsChildId() {
		return ssChildId;
	}
	public void setSsChildId(String ssChildId) {
		this.ssChildId = ssChildId;
	}
	
	@Column(name="IS_CHILD_SS")
	public Integer getIsChildSS() {
		return isChildSS;
	}
	public void setIsChildSS(Integer isChildSS) {
		this.isChildSS = isChildSS;
	}
	
	@Column(name="IS_INCLUDE")
	public Integer getIsInclude() {
		return isInclude;
	}
	
	
	public void setIsInclude(Integer isInclude) {
		this.isInclude = isInclude;
	}
	
	@Column(name="DELETEDFLAG")
	public Integer getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(Integer deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	@Column(name="RID")
	public Long getRid() {
		return rid;
	}
	public void setRid(Long rid) {
		this.rid = rid;
	}
	@Column(name="EVENT_LIB_ID")
	public String getEventlibid() {
		return eventlibid;
	}
	public void setEventlibid(String eventlibid) {
		this.eventlibid = eventlibid;
	}
	
}

