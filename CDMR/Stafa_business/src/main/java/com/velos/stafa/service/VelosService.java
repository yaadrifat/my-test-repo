package com.velos.stafa.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import com.velos.stafa.helper.VelosHelper;
/*import com.velos.stafa.pojo.EmploymentInfoDetailsObject;
import com.velos.stafa.pojo.PersonFundCaseManagementObjects;
import com.velos.stafa.pojo.RecipientInsViewDetailsObject;
*/
import atg.taglib.json.util.JSONArray;
import atg.taglib.json.util.JSONObject;


public interface VelosService {
	
	public JSONObject saveJSONObjects (JSONObject jsonObject)throws Exception;
	
	public JSONObject saveJSONObject (JSONObject jsonObject)throws Exception;
		
	
	public JSONObject saveJSONData (JSONObject jsonObject)throws Exception;
	
	public JSONObject executeSQLQueries(JSONObject jsonObject)throws Exception;
	
	
	
	public int findCount(String objectName,String property,String value)throws Exception;
	public JSONObject saveMultipleBaseAndSubObjects(JSONObject jsonObject) throws Exception;
	public JSONObject saveReferenceSubObjects(JSONObject jsonObject) throws Exception;
	
		
	public JSONObject saveJSONDataObject(JSONObject jsonObject) throws Exception;
	
	public List fetchNextTask(String entityId, String entityType, Long userId)throws Exception;
	
	
	
	public JSONObject saveObject(JSONObject jsonObject) throws Exception;
	
	public JSONArray saveObject(JSONArray jsonArray) throws Exception;
	
	public List getObjectList(JSONObject jsonObject)throws Exception;
	public List getObjectList(JSONArray jsonArray)throws Exception;
	public JSONObject saveDataFiledValues(JSONObject jsonObject)throws Exception;
	
	public JSONObject getObjectListByCriteria(JSONObject jsonObject)throws Exception;


	
	
	
	
	
	
	public Object getObject(String objectName,Long id) throws Exception;
	
	public List getDataFromSQLQuery(String mainQuery) throws Exception;

	/*public void saveInsEmploymentInfo(
			EmploymentInfoDetailsObject insEmploymentDetails)throws Exception;

	public void saveCompleteInsuranceInfo(
			RecipientInsViewDetailsObject recipientInsViewDetails, EmploymentInfoDetailsObject employmentDetails)throws Exception;

	public void savePersonFundCaseManagement(
			PersonFundCaseManagementObjects personFundCaseMgmtDetails)throws Exception;
*/
    public void saveDomainFromPojoList(Object domainObject, String keyId, List savePojoList);
	
	public List<Object> getPojoList(String domainName, Object pojoObject, String criteria, String type);
	
	/*********** CDMR SPECIFIC METHODS***********/
	public JSONObject saveFileInfo(JSONObject jsonObject)throws Exception;
	public JSONObject getFilesInfo(JSONObject jsonObject)throws Exception;
	public JSONObject saveStagingMainInfo(JSONObject jsonObject)throws Exception;
	public JSONObject saveStagingDetailInfo(JSONObject jsonObject)throws Exception;
	public JSONObject getCodeList(JSONObject jsonObject)throws Exception;
	public JSONObject getStagingID(JSONObject jsonObject)throws Exception;
	public JSONObject getActiveMapVesion(JSONObject jsonObject)throws Exception;
	public JSONObject getStagingMaps(JSONObject jsonObject)throws Exception;
	public JSONObject getStagingPrimaryID(JSONObject jsonObject)throws Exception;
	public JSONObject getStagingDetailData(JSONObject jsonObject)throws Exception;
	public JSONObject getCompReqColumnMapDetail(JSONObject jsonObject)throws Exception;
	public JSONObject saveCompRecInfo(JSONObject jsonObject)throws Exception;
	public JSONObject getStagingColumnMapDetail(JSONObject jsonObject)throws Exception;
	public JSONObject saveInStagingCompReq(JSONObject jsonObject)throws Exception;
	public JSONObject getCompRecData(JSONObject jsonObject)throws Exception;
	public JSONObject countRecord(JSONObject jsonObject)throws Exception;
	public JSONObject getGroups(JSONObject jsonObject)throws Exception;
	public JSONObject updateComrec(JSONObject jsonObject)throws Exception;
	public JSONObject getGroupCodeValue(JSONObject jsonObject)throws Exception;
	public JSONObject updateFileStatus(JSONObject jsonObject)throws Exception;
	public JSONObject getNextPossibleStatus(JSONObject jsonObject)throws Exception;
	public JSONObject insertInCDMMain(JSONObject jsonObject)throws Exception;
	public JSONObject getMatchedData(JSONObject jsonObject)throws Exception;
	public JSONObject saveInMainCDM(JSONObject jsonObject)throws Exception;
	public JSONObject getCDMVersionDetail(JSONObject jsonObject)throws Exception;
	public JSONObject saveCdmVersionDef(JSONObject jsonObject)throws Exception;
	public JSONObject updateCdmVersionDef(JSONObject jsonObject)throws Exception;
	public JSONObject getFromCdmMain(JSONObject jsonObject)throws Exception;
	public JSONObject saveInSSLink(JSONObject jsonObject)throws Exception;
	public JSONObject getFromCdmSSLink(JSONObject jsonObject)throws Exception;
	public JSONObject saveNewServiceSet(JSONObject jsonObject)throws Exception;
	public JSONObject getMaxGrupCode(JSONObject jsonObject)throws Exception;
	public JSONObject saveInCdmGrupCode(JSONObject jsonObject)throws Exception;
	public JSONObject updateInCdmGrupCode(JSONObject jsonObject)throws Exception;
	public JSONObject getFromCDMGroupCode(JSONObject jsonObject)throws Exception;
	public JSONObject getTechnicalCharges(JSONObject jsonObject)throws Exception;
	public JSONObject getProfessionalCharges(JSONObject jsonObject)throws Exception;
	public JSONObject insertInStagingTemp(JSONObject jsonObject)throws Exception;
	public JSONObject truncateStagingTemp(JSONObject jsonObject)throws Exception;
	public JSONObject getMappingForSQLBinding(JSONObject jsonObject)throws Exception;
	public JSONObject getFromStagingTemp(JSONObject jsonObject)throws Exception;
	public  JSONObject getMapVersion(JSONObject jsonObject) throws Exception;
	public JSONObject getCdmMain(JSONObject jsonObject) throws Exception;
	public JSONObject saveStagingDetail(JSONObject jsonObject) throws Exception;
	public JSONObject getChagIndc(JSONObject jsonObject) throws Exception;
	public JSONObject updateFilesLibForRemoteData(JSONObject jsonObject) throws Exception;
	public JSONObject editServiceSection(JSONObject jsonObject) throws Exception;
	public JSONObject editServiceSet(JSONObject jsonObject) throws Exception;
	public JSONObject getUserDetail(JSONObject jsonObject) throws Exception;
	public JSONObject getFromNotifycnf(JSONObject jsonObject) throws Exception;
	public JSONObject getFromEsec(JSONObject jsonObject) throws Exception;
	public JSONObject saveNotificationInfo(JSONObject jsonObject) throws Exception;
	public JSONObject getSchedulerTime(JSONObject jsonObject) throws Exception;
	public JSONObject updateSchdulerTime(JSONObject jsonObject) throws Exception;
	public JSONObject deleteNotifiedUser(JSONObject jsonObject) throws Exception;
	public JSONObject loadAllNotification(JSONObject jsonObject) throws Exception;
	public JSONObject deleteNotification(JSONObject jsonObject) throws Exception;
	public JSONObject saveErrorLog(JSONObject jsonObject) throws Exception;
	public JSONObject updateErrorLog(JSONObject jsonObject) throws Exception;
	public JSONObject getMailConfigurationInfo(JSONObject jsonObject) throws Exception;
	public JSONObject getNotificationMessages(JSONObject jsonObject) throws Exception;
	public JSONObject updateNotification(JSONObject jsonObject) throws Exception;
	public JSONObject updateEmailSettings(JSONObject jsonObject) throws Exception;
	public JSONObject getEmailSettings(JSONObject jsonObject) throws Exception;
	public JSONObject updateNoOfFiles(JSONObject jsonObject) throws Exception;
	public JSONObject getNumberOfFiles(JSONObject jsonObject) throws Exception;
	public JSONObject getSuperAdmin(JSONObject jsonObject) throws Exception;
	public JSONObject saveForgotPasswordRequest(JSONObject jsonObject) throws Exception;
	public JSONObject getDefaultPassword(JSONObject jsonObject) throws Exception;
	public JSONObject getUserEmail(JSONObject jsonObject) throws Exception;
	public JSONObject updateNotificationStatus(JSONObject jsonObject) throws Exception;
	public JSONObject saveInObsSvcBfr(JSONObject jsonObject) throws Exception;
	public List<Map> fetchColumnForDataTable(JSONObject jsonObject)throws Exception;
	public void saveColumnState(JSONObject jsonObject)throws Exception;
	public void saveColumnOrder(JSONObject jsonObject)throws Exception;
	public JSONObject getFilePkForStatus(JSONObject jsonObject)throws Exception;
	public JSONObject getFileSourceLocation(JSONObject jsonObject)throws Exception;
	public JSONObject getFileInformation(JSONObject jsonObject)throws Exception;
	public  boolean checkForExistingFile(JSONObject jsonObject)throws Exception;
	public JSONObject getPullMethod(JSONObject jsonObject)throws Exception;
	public List<String> getInterfaceList(JSONObject jsonObject)throws Exception;
	public JSONObject getChgIndc(JSONObject jsonObject)throws Exception;
	public JSONObject getProcessedDirLocation(JSONObject jsonObject)throws Exception;
	public JSONObject getFileName(JSONObject jsonObject)throws Exception;
	public JSONObject getFileID(JSONObject jsonObject)throws Exception;
	public JSONObject getModulename(JSONObject jsonObject)throws Exception;
	public JSONObject getMatchingCriteria(JSONObject jsonObject)throws Exception;
	public JSONObject getEventLibId(JSONObject jsonObject)throws Exception;
	public JSONObject getchgtype(JSONObject jsonObject)throws Exception;
	public JSONObject getChgIndcBySeq(JSONObject jsonObject)throws Exception;
	public JSONObject fetchEventLibId(JSONObject jsonObject)throws Exception;
	public JSONObject fetchVersionID(JSONObject jsonObject)throws Exception;
	public JSONObject getActiveRule(JSONObject jsonObject)throws Exception;
	public JSONObject fetchRTRule(JSONObject jsonObject)throws Exception;
	public JSONObject fetchcolTypeMapDetail(JSONObject jsonObject)throws Exception;
	
	
	/*Advanced search methods start*/
	public JSONObject getAdvsearchCols(JSONObject jsonObject)throws Exception;
	public JSONObject checkForFilterName(JSONObject jsonObject)throws Exception;
	public JSONObject saveNewFilter(JSONObject jsonObject)throws Exception;
	public JSONObject loadFilters(JSONObject jsonObject)throws Exception;
	public JSONObject saveFilterParam(JSONObject jsonObject)throws Exception;
	public JSONObject updateCdmFilMaint(JSONObject jsonObject)throws Exception;
	public JSONObject getSavedFilters(JSONObject jsonObject)throws Exception;
	public JSONObject getCdmFilParam(JSONObject jsonObject)throws Exception;
	public JSONObject getPkforFilter(JSONObject jsonObject)throws Exception;
	public JSONObject updateCdmParam(JSONObject jsonObject)throws Exception;
	/*crms DATA TRANSFER STARTS HERE*/
	public JSONObject dataTransferStatus(JSONObject jsonObject)throws Exception;
	public JSONObject getexportDatacount(JSONObject jsonObject)throws Exception;
	public JSONObject transferToCtms()throws Exception;
	public JSONObject updateTransferstatus(JSONObject jsonObject)throws Exception;
	public JSONObject saveNewNotification(JSONObject jsonObject)throws Exception;
	public JSONObject getPasswordAuthFlag(JSONObject jsonObject)throws Exception;
	public JSONObject getLibraryName(JSONObject jsonObject)throws Exception;
}
