package com.velos.stafa.business.domain.cdmr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.velos.stafa.business.domain.BaseDomainObject;
/**
 * @summary     StagingDetail
 * @description Mapping class of CDM_STAGINGTemp
 * @version     1.0
 * @file        StagingDetail.java
 * @author      Shiv Murti Pal
 */

@Entity
@Table(name="CDM_STAGING_TEMP")
@javax.persistence.SequenceGenerator(name="staging_temp_sqnc", sequenceName="SEQ_CDM_STAGING_TEMP")
public class StagingTemp{

	private Long pkStagingTemp;
	private Long fkStagingMain;
	private Long recordSeqNo;
	private String recordType;
	private String dataCol1;
	private String dataCol2;
	private String dataCol3;
	private String dataCol4;
	private String dataCol5;
	private String dataCol6;
	private String dataCol7;
	private String dataCol8;
	private String dataCol9;
	private String dataCol10;
	private String dataCol11;
	private String dataCol12;
	private String dataCol13;
	private String dataCol14;
	private String dataCol15;
	private String dataCol16;
	private String dataCol17;
	private String dataCol18;
	private String dataCol19;
	private String dataCol20;
	private String dataCol21;
	private String dataCol22;
	private String dataCol23;
	private String dataCol24;
	private String dataCol25;
	private String dataColExt;
	private Integer deletedFlag;
	private String instCode;
	private String eventLibid;
	
	
	

	@Id
	@GeneratedValue(generator="staging_temp_sqnc",strategy=GenerationType.AUTO)
	@Column(name="PK_CDM_STAGING_TEMP")
	public Long getPkStagingTemp() {
		return pkStagingTemp;
	}

	public void setPkStagingTemp(Long pkStagingTemp) {
		this.pkStagingTemp = pkStagingTemp;
	}

	@Column(name="FK_CDM_STAGINGMAIN")
	public Long getFkStagingMain() {
		return fkStagingMain;
	}

	public void setFkStagingMain(Long fkStagingMain) {
		this.fkStagingMain = fkStagingMain;
	}

	@Column(name="RECORD_SEQNO")
	public Long getRecordSeqNo() {
		return recordSeqNo;
	}

	public void setRecordSeqNo(Long recordSeqNo) {
		this.recordSeqNo = recordSeqNo;
	}
	
	@Column(name="RECORD_TYPE")
	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	@Column(name="DATA_COL1")
	public String getDataCol1() {
		return dataCol1;
	}

	public void setDataCol1(String dataCol1) {
		this.dataCol1 = dataCol1;
	}

	@Column(name="DATA_COL2")
	public String getDataCol2() {
		return dataCol2;
	}

	public void setDataCol2(String dataCol2) {
		this.dataCol2 = dataCol2;
	}

	@Column(name="DATA_COL3")
	public String getDataCol3() {
		return dataCol3;
	}

	public void setDataCol3(String dataCol3) {
		this.dataCol3 = dataCol3;
	}

	@Column(name="DATA_COL4")
	public String getDataCol4() {
		return dataCol4;
	}

	public void setDataCol4(String dataCol4) {
		this.dataCol4 = dataCol4;
	}

	@Column(name="DATA_COL5")
	public String getDataCol5() {
		return dataCol5;
	}

	public void setDataCol5(String dataCol5) {
		this.dataCol5 = dataCol5;
	}

	@Column(name="DATA_COL6")
	public String getDataCol6() {
		return dataCol6;
	}

	public void setDataCol6(String dataCol6) {
		this.dataCol6 = dataCol6;
	}

	@Column(name="DATA_COL7")
	public String getDataCol7() {
		return dataCol7;
	}

	public void setDataCol7(String dataCol7) {
		this.dataCol7 = dataCol7;
	}

	@Column(name="DATA_COL8")
	public String getDataCol8() {
		return dataCol8;
	}

	public void setDataCol8(String dataCol8) {
		this.dataCol8 = dataCol8;
	}

	@Column(name="DATA_COL9")
	public String getDataCol9() {
		return dataCol9;
	}

	public void setDataCol9(String dataCol9) {
		this.dataCol9 = dataCol9;
	}

	@Column(name="DATA_COL10")
	public String getDataCol10() {
		return dataCol10;
	}

	public void setDataCol10(String dataCol10) {
		this.dataCol10 = dataCol10;
	}

	@Column(name="DATA_COL11")
	public String getDataCol11() {
		return dataCol11;
	}

	public void setDataCol11(String dataCol11) {
		this.dataCol11 = dataCol11;
	}

	@Column(name="DATA_COL12")
	public String getDataCol12() {
		return dataCol12;
	}

	public void setDataCol12(String dataCol12) {
		this.dataCol12 = dataCol12;
	}

	@Column(name="DATA_COL13")
	public String getDataCol13() {
		return dataCol13;
	}

	public void setDataCol13(String dataCol13) {
		this.dataCol13 = dataCol13;
	}

	@Column(name="DATA_COL14")
	public String getDataCol14() {
		return dataCol14;
	}

	public void setDataCol14(String dataCol14) {
		this.dataCol14 = dataCol14;
	}

	@Column(name="DATA_COL15")
	public String getDataCol15() {
		return dataCol15;
	}

	public void setDataCol15(String dataCol15) {
		this.dataCol15 = dataCol15;
	}

	@Column(name="DATA_COL16")
	public String getDataCol16() {
		return dataCol16;
	}

	public void setDataCol16(String dataCol16) {
		this.dataCol16 = dataCol16;
	}

	@Column(name="DATA_COL17")
	public String getDataCol17() {
		return dataCol17;
	}

	public void setDataCol17(String dataCol17) {
		this.dataCol17 = dataCol17;
	}

	@Column(name="DATA_COL18")
	public String getDataCol18() {
		return dataCol18;
	}

	public void setDataCol18(String dataCol18) {
		this.dataCol18 = dataCol18;
	}

	@Column(name="DATA_COL19")
	public String getDataCol19() {
		return dataCol19;
	}

	public void setDataCol19(String dataCol19) {
		this.dataCol19 = dataCol19;
	}

	@Column(name="DATA_COL20")
	public String getDataCol20() {
		return dataCol20;
	}

	public void setDataCol20(String dataCol20) {
		this.dataCol20 = dataCol20;
	}

	@Column(name="DATA_COL21")
	public String getDataCol21() {
		return dataCol21;
	}

	public void setDataCol21(String dataCol21) {
		this.dataCol21 = dataCol21;
	}

	@Column(name="DATA_COL22")
	public String getDataCol22() {
		return dataCol22;
	}

	public void setDataCol22(String dataCol22) {
		this.dataCol22 = dataCol22;
	}

	@Column(name="DATA_COL23")
	public String getDataCol23() {
		return dataCol23;
	}

	public void setDataCol23(String dataCol23) {
		this.dataCol23 = dataCol23;
	}

	@Column(name="DATA_COL24")
	public String getDataCol24() {
		return dataCol24;
	}

	public void setDataCol24(String dataCol24) {
		this.dataCol24 = dataCol24;
	}

	@Column(name="DATA_COL25")
	public String getDataCol25() {
		return dataCol25;
	}

	public void setDataCol25(String dataCol25) {
		this.dataCol25 = dataCol25;
	}

	@Column(name="DATA_COLEXT")
	@Lob
	public String getDataColExt() {
		return dataColExt;
	}

	public void setDataColExt(String dataColExt) {
		this.dataColExt = dataColExt;
	}

	@Column(name="DELETEDFLAG")
	public Integer getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(Integer deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	
	@Column(name="INST_CODE")
	public String getInstCode() {
		return instCode;
	}

	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}
	@Column(name="EVENT_LIB_ID")
	public String getEventLibid() {
		return eventLibid;
	}

	public void setEventLibid(String eventLibid) {
		this.eventLibid = eventLibid;
	}
	
}