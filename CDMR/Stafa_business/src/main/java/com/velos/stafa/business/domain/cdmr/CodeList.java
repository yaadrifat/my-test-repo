package com.velos.stafa.business.domain.cdmr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.velos.stafa.business.domain.BaseDomainObject;

/**
 * @summary     CodeList
 * @description Mapping class of CDM_CODELST Table
 * @version     1.0
 * @file        CodeList.java
 * @author      Lalit Chattar
 */

@Entity
@Table(name="VBAR_CODELST")
@javax.persistence.SequenceGenerator(name="codelist_sqnc", sequenceName="SEQ_VBAR_CODELST")
public class CodeList extends BaseDomainObject {
	
	private Long pkCodeList;
	private Long fkAccount;
	private String codeListType;
	private String codeListSubType;
	private String codeListDesc;
	private Character codeListHide;
	private Long codeListSeq;
	private Character codeListMaint;
	private Long rid;
	private String codeListCustomCol;
	private String codeListCustomCol1;
	private Integer fkCodeListOrgId;
	private String codeListPageUrl;
	private Integer codeListDefaultValue;
	private String codeListModule;
	private String codeListComments;
	private String codeListValue;
	private Long fkCopyCodeList;
	private Integer appLock;
	private String codeListFieldType;
	
	@Id
	@GeneratedValue(generator="codelist_sqnc",strategy=GenerationType.AUTO)
	@Column(name="PK_CODELST")
	public Long getPkCodeList() {
		return pkCodeList;
	}
	public void setPkCodeList(Long pkCodeList) {
		this.pkCodeList = pkCodeList;
	}
	
	@Column(name="FK_ACCOUNT")
	public Long getFkAccount() {
		return fkAccount;
	}
	public void setFkAccount(Long fkAccount) {
		this.fkAccount = fkAccount;
	}
	
	@Column(name="CODELST_TYPE")
	public String getCodeListType() {
		return codeListType;
	}
	public void setCodeListType(String codeListType) {
		this.codeListType = codeListType;
	}

	@Column(name="CODELST_SUBTYP")
	public String getCodeListSubType() {
		return codeListSubType;
	}
	public void setCodeListSubType(String codeListSubType) {
		this.codeListSubType = codeListSubType;
	}

	@Column(name="CODELST_DESC")
	public String getCodeListDesc() {
		return codeListDesc;
	}
	public void setCodeListDesc(String codeListDesc) {
		this.codeListDesc = codeListDesc;
	}

	@Column(name="CODELST_HIDE")
	public Character getCodeListHide() {
		return codeListHide;
	}
	public void setCodeListHide(Character codeListHide) {
		this.codeListHide = codeListHide;
	}
	
	@Column(name="CODELST_SEQ")
	public Long getCodeListSeq() {
		return codeListSeq;
	}
	public void setCodeListSeq(Long codeListSeq) {
		this.codeListSeq = codeListSeq;
	}
	
	@Column(name="CODELST_MAINT")
	public Character getCodeListMaint() {
		return codeListMaint;
	}
	public void setCodeListMaint(Character codeListMaint) {
		this.codeListMaint = codeListMaint;
	}

	@Column(name="RID")
	public Long getRid() {
		return rid;
	}
	public void setRid(Long rid) {
		this.rid = rid;
	}
	
	@Column(name="CODELST_CUSTOM_COL")

	public String getCodeListCustomCol() {
		return codeListCustomCol;
	}
	public void setCodeListCustomCol(String codeListCustomCol) {
		this.codeListCustomCol = codeListCustomCol;
	}

	@Column(name="CODELST_CUSTOM_COL1")
	public String getCodeListCustomCol1() {
		return codeListCustomCol1;
	}
	public void setCodeListCustomCol1(String codeListCustomCol1) {
		this.codeListCustomCol1 = codeListCustomCol1;
	}

	@Column(name="FK_CODELST_ORGID")
	public Integer getFkCodeListOrgId() {
		return fkCodeListOrgId;
	}
	public void setFkCodeListOrgId(Integer fkCodeListOrgId) {
		this.fkCodeListOrgId = fkCodeListOrgId;
	}

	@Column(name="CODELST_PAGEURL")
	public String getCodeListPageUrl() {
		return codeListPageUrl;
	}
	public void setCodeListPageUrl(String codeListPageUrl) {
		this.codeListPageUrl = codeListPageUrl;
	}

	@Column(name="CODELST_DEFAULTVALUE")
	public Integer getCodeListDefaultValue() {
		return codeListDefaultValue;
	}
	public void setCodeListDefaultValue(Integer codeListDefaultValue) {
		this.codeListDefaultValue = codeListDefaultValue;
	}

	@Column(name="CODELST_MODULE")
	public String getCodeListModule() {
		return codeListModule;
	}
	public void setCodeListModule(String codeListModule) {
		this.codeListModule = codeListModule;
	}

	@Column(name="CODELST_COMMENTS")
	public String getCodeListComments() {
		return codeListComments;
	}
	public void setCodeListComments(String codeListComments) {
		this.codeListComments = codeListComments;
	}

	@Column(name="CODELST_VALUE")
	public String getCodeListValue() {
		return codeListValue;
	}
	public void setCodeListValue(String codeListValue) {
		this.codeListValue = codeListValue;
	}

	@Column(name="FK_COPY_CODELST")
	public Long getFkCopyCodeList() {
		return fkCopyCodeList;
	}
	public void setFkCopyCodeList(Long fkCopyCodeList) {
		this.fkCopyCodeList = fkCopyCodeList;
	}

	@Column(name="APPLOCK")
	public Integer getAppLock() {
		return appLock;
	}
	public void setAppLock(Integer appLock) {
		this.appLock = appLock;
	}

	@Column(name="CODELST_FIELDTYPE")
	public String getCodeListFieldType() {
		return codeListFieldType;
	}
	public void setCodeListFieldType(String codeListFieldType) {
		this.codeListFieldType = codeListFieldType;
	}

}