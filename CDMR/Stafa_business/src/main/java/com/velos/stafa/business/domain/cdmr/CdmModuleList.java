package com.velos.stafa.business.domain.cdmr;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.velos.stafa.business.domain.BaseDomainObject;

@Entity
@Table(name="CDM_MODULE_LIST")
@javax.persistence.SequenceGenerator(name="cdmmodlist_sqnc", sequenceName="SEQ_CDM_MODULE_LIST")
public class CdmModuleList extends BaseDomainObject {
	
	private Long pkCdmModule;
	private String modName;
	private String modDesc;
	@Id
	@GeneratedValue(generator="cdmmodlist_sqnc",strategy=GenerationType.AUTO)
	@Column(name="PK_CDM_MODULE")
	public Long getPkCdmModule() {
		return pkCdmModule;
	}
	public void setPkCdmModule(Long pkCdmModule) {
		this.pkCdmModule = pkCdmModule;
	}
	@Column(name="MODULE_NAME")
	public String getModName() {
		return modName;
	}
	public void setModName(String modName) {
		this.modName = modName;
	}
	@Column(name="MODULE_DESC")
	public String getModDesc() {
		return modDesc;
	}
	public void setModDesc(String modDesc) {
		this.modDesc = modDesc;
	}
	

	
	
}

