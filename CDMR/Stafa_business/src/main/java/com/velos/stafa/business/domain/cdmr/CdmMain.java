package com.velos.stafa.business.domain.cdmr;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.velos.stafa.business.domain.BaseDomainObject;


@Entity
@Table(name="CDM_MAIN")
@javax.persistence.SequenceGenerator(name="cdmmain", sequenceName="SEQ_CDM_MAIN")
public class CdmMain extends BaseDomainObject{
	
	private Long pkCdmMain;
	private Long fkAccount;
	private String lServiceCode;
	private String serviceCode;
	private String cptCode;
	private String prsCode;
	private String cptMdfr;
	private Date effFromDate;
	private Date effToDate;
	private Integer freezainVersion;
	private Integer currentVersion;
	private Integer isThisSS;
	private String serviceDesc1;
	private String serviceDesc2;
	private String sponsorRef1;
	private String sponsorRef2;
	private String serviceUnit;
	private Integer unitSize;
	private String revCode;
	private String revClass;
	private String revSubDept;
	private String revTestCode;
	private Integer hccTechCharge;
	private Integer prsPhyCharge;
	private Integer customCharge1;
	private Integer customCharge2;
	private Integer isIncludeInTotal;
	private Integer isBillable;
	private String icd9Code;
	private String icd10Code;
	private String customCode1;
	private String customCode2;
	private Integer fkCdmGrpCode;
	private String notes;
	private Integer applyUptoRules;
	private Integer isActivated;
	private Integer curStatus;
	private Date dateUpdated;
	private String updateSrc;
	private String updateComments1;
	private Integer fkFacility;
	private Integer fkInsurance;
	private String facId;
	private String inscoId;
	private String instCode;
	private Long rid;
	private String eventlibid;
	
	
	@Id
	@GeneratedValue(generator="cdmmain",strategy=GenerationType.AUTO)
	@Column(name="PK_CDM_MAIN")
	public Long getPkCdmMain() {
		return pkCdmMain;
	}
	public void setPkCdmMain(Long pkCdmMain) {
		this.pkCdmMain = pkCdmMain;
	}
	
	@Column(name="FK_ACCOUNT")
	public Long getFkAccount() {
		return fkAccount;
	}
	public void setFkAccount(Long fkAccount) {
		this.fkAccount = fkAccount;
	}
	
	@Column(name="LSERVICE_CODE")
	public String getlServiceCode() {
		return lServiceCode;
	}
	public void setlServiceCode(String lServiceCode) {
		this.lServiceCode = lServiceCode;
	}
	
	@Column(name="SERVICE_CODE")
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	
	@Column(name="CPT_CODE")
	public String getCptCode() {
		return cptCode;
	}
	public void setCptCode(String cptCode) {
		this.cptCode = cptCode;
	}
	
	@Column(name="PRS_CODE")
	public String getPrsCode() {
		return prsCode;
	}
	public void setPrsCode(String prsCode) {
		this.prsCode = prsCode;
	}
	
	
	@Column(name="CPT_MDFR")
	public String getCptMdfr() {
		return cptMdfr;
	}
	public void setCptMdfr(String cptMdfr) {
		this.cptMdfr = cptMdfr;
	}
	
	
	
	@Column(name="EFF_FROM_DATE")
	public Date getEffFromDate() {
		return effFromDate;
	}
	public void setEffFromDate(Date effFromDate) {
		this.effFromDate = effFromDate;
	}
	
	@Column(name="EFF_TO_DATE")
	public Date getEffToDate() {
		return effToDate;
	}
	public void setEffToDate(Date effToDate) {
		this.effToDate = effToDate;
	}
	
	@Column(name="FREEZEIN_VERSION")
	public Integer getFreezainVersion() {
		return freezainVersion;
	}
	public void setFreezainVersion(Integer freezainVersion) {
		this.freezainVersion = freezainVersion;
	}
	
	@Column(name="CUR_VERSION")
	public Integer getCurrentVersion() {
		return currentVersion;
	}
	public void setCurrentVersion(Integer currentVersion) {
		this.currentVersion = currentVersion;
	}
	
	@Column(name="IS_THIS_SS")
	public Integer getIsThisSS() {
		return isThisSS;
	}
	public void setIsThisSS(Integer isThisSS) {
		this.isThisSS = isThisSS;
	}
	
	@Column(name="SERVICE_DESC1")
	public String getServiceDesc1() {
		return serviceDesc1;
	}
	public void setServiceDesc1(String serviceDesc1) {
		this.serviceDesc1 = serviceDesc1;
	}
	
	@Column(name="SERVICE_DESC2")
	public String getServiceDesc2() {
		return serviceDesc2;
	}
	public void setServiceDesc2(String serviceDesc2) {
		this.serviceDesc2 = serviceDesc2;
	}
	
	@Column(name="SPONSOR_REF1")
	public String getSponsorRef1() {
		return sponsorRef1;
	}
	public void setSponsorRef1(String sponsorRef1) {
		this.sponsorRef1 = sponsorRef1;
	}
	
	@Column(name="SPONSOR_REF2")
	public String getSponsorRef2() {
		return sponsorRef2;
	}
	public void setSponsorRef2(String sponsorRef2) {
		this.sponsorRef2 = sponsorRef2;
	}
	
	@Column(name="SERVICE_UNIT")
	public String getServiceUnit() {
		return serviceUnit;
	}
	public void setServiceUnit(String serviceUnit) {
		this.serviceUnit = serviceUnit;
	}
	
	@Column(name="UNIT_SIZE")
	public Integer getUnitSize() {
		return unitSize;
	}
	public void setUnitSize(Integer unitSize) {
		this.unitSize = unitSize;
	}
	
	@Column(name="REV_CODE")
	public String getRevCode() {
		return revCode;
	}
	public void setRevCode(String revCode) {
		this.revCode = revCode;
	}
	
	@Column(name="REV_CLASS")
	public String getRevClass() {
		return revClass;
	}
	public void setRevClass(String revClass) {
		this.revClass = revClass;
	}
	
	@Column(name="REV_SUB_DEPT")
	public String getRevSubDept() {
		return revSubDept;
	}
	public void setRevSubDept(String revSubDept) {
		this.revSubDept = revSubDept;
	}
	
	@Column(name="REV_TEST_CODE")
	public String getRevTestCode() {
		return revTestCode;
	}
	public void setRevTestCode(String revTestCode) {
		this.revTestCode = revTestCode;
	}
	
	@Column(name="HCC_TECH_CHG")
	public Integer getHccTechCharge() {
		return hccTechCharge;
	}
	public void setHccTechCharge(Integer hccTechCharge) {
		this.hccTechCharge = hccTechCharge;
	}
	
	@Column(name="PRS_PHY_CHG")
	public Integer getPrsPhyCharge() {
		return prsPhyCharge;
	}
	public void setPrsPhyCharge(Integer prsPhyCharge) {
		this.prsPhyCharge = prsPhyCharge;
	}
	
	@Column(name="CUSTOM_CHG1")
	public Integer getCustomCharge1() {
		return customCharge1;
	}
	public void setCustomCharge1(Integer customCharge1) {
		this.customCharge1 = customCharge1;
	}
	
	@Column(name="CUSTOM_CHG2")
	public Integer getCustomCharge2() {
		return customCharge2;
	}
	public void setCustomCharge2(Integer customCharge2) {
		this.customCharge2 = customCharge2;
	}
	

	
	@Column(name="IS_INCLUDE_INTOTAL")
	public Integer getIsIncludeInTotal() {
		return isIncludeInTotal;
	}
	public void setIsIncludeInTotal(Integer isIncludeInTotal) {
		this.isIncludeInTotal = isIncludeInTotal;
	}
	
	@Column(name="IS_BILLABLE")
	public Integer getIsBillable() {
		return isBillable;
	}
	public void setIsBillable(Integer isBillable) {
		this.isBillable = isBillable;
	}
	
	@Column(name="ICD9_CODE")
	public String getIcd9Code() {
		return icd9Code;
	}
	public void setIcd9Code(String icd9Code) {
		this.icd9Code = icd9Code;
	}
	
	@Column(name="ICD10_CODE")
	public String getIcd10Code() {
		return icd10Code;
	}
	public void setIcd10Code(String icd10Code) {
		this.icd10Code = icd10Code;
	}
	
	@Column(name="CUSTOM_CODE1")
	public String getCustomCode1() {
		return customCode1;
	}
	public void setCustomCode1(String customCode1) {
		this.customCode1 = customCode1;
	}
	
	@Column(name="CUSTOM_CODE2")
	public String getCustomCode2() {
		return customCode2;
	}
	public void setCustomCode2(String customCode2) {
		this.customCode2 = customCode2;
	}
	
	
	@Column(name="FK_CDM_GRPCODE")
	public Integer getFkCdmGrpCode() {
		return fkCdmGrpCode;
	}
	public void setFkCdmGrpCode(Integer fkCdmGrpCode) {
		this.fkCdmGrpCode = fkCdmGrpCode;
	}
	
	@Column(name="NOTES")
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	@Column(name="APPLY_UPTO_RULE")
	public Integer getApplyUptoRules() {
		return applyUptoRules;
	}
	public void setApplyUptoRules(Integer applyUptoRules) {
		this.applyUptoRules = applyUptoRules;
	}
	
	@Column(name="IS_ACTIVE")
	public Integer getIsActivated() {
		return isActivated;
	}
	public void setIsActivated(Integer isActivated) {
		this.isActivated = isActivated;
	}
	
	@Column(name="CUR_STATUS")
	public Integer getCurStatus() {
		return curStatus;
	}
	public void setCurStatus(Integer curStatus) {
		this.curStatus = curStatus;
	}
	
	@Column(name="DATE_UPDATED")
	public Date getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	
	@Column(name="UPDATE_SRC")
	public String getUpdateSrc() {
		return updateSrc;
	}
	public void setUpdateSrc(String updateSrc) {
		this.updateSrc = updateSrc;
	}
	
	
	@Column(name="FK_FACILITY")
	public Integer getFkFacility() {
		return fkFacility;
	}
	public void setFkFacility(Integer fkFacility) {
		this.fkFacility = fkFacility;
	}
	
	@Column(name="FK_INSURANCE")
	public Integer getFkInsurance() {
		return fkInsurance;
	}
	public void setFkInsurance(Integer fkInsurance) {
		this.fkInsurance = fkInsurance;
	}
	
	@Column(name="FAC_ID")
	public String getFacId() {
		return facId;
	}
	public void setFacId(String facId) {
		this.facId = facId;
	}
	
	@Column(name="INSCO_ID")
	public String getInscoId() {
		return inscoId;
	}
	public void setInscoId(String inscoId) {
		this.inscoId = inscoId;
	}
	
	@Column(name="INST_CODE")
	public String getInstCode() {
		return instCode;
	}
	public void setInstCode(String instCode) {
		this.instCode = instCode;
	}
	
	@Column(name="RID")
	public Long getRid() {
		return rid;
	}
	public void setRid(Long rid) {
		this.rid = rid;
	}
	@Column(name="EVENT_LIB_ID")
	public String getEventlibid() {
		return eventlibid;
	}
	public void setEventlibid(String eventlibid) {
		this.eventlibid = eventlibid;
	}
}