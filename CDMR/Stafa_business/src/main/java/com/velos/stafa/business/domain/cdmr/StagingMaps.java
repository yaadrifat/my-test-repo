package com.velos.stafa.business.domain.cdmr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.velos.stafa.business.domain.BaseDomainObject;


/**
 * @summary     StagingMaps
 * @description Mapping class of CDM_STAGINGMAIN
 * @version     1.0
 * @file        StagingMaps.java
 * @author      Lalit Chattar
 */

@Entity
@Table(name="CDM_STAGINGMAPS")
@javax.persistence.SequenceGenerator(name="staging_maps", sequenceName="SEQ_CDM_STAGINGMAPS")
public class StagingMaps extends BaseDomainObject{
	
	private Long pkStagingMap;
	private Long fkMapVersion;
	private Long compSeqNum;
	private String compColID;
	private String compColName;
	private String pullFromCols;
	private String eqMainCdmCol;
	private String eqCompReqCol;
	private Integer postToCDM;
	private Integer dispInStaging;
	private String mainCdmColType;
	private Long rid;
	
	
	@Id
	@GeneratedValue(generator="staging_maps",strategy=GenerationType.AUTO)
	@Column(name="PK_STAGINGMAPS")
	public Long getPkStagingMap() {
		return pkStagingMap;
	}
	public void setPkStagingMap(Long pkStagingMap) {
		this.pkStagingMap = pkStagingMap;
	}
	
	@Column(name="FK_CDM_MAPSVERSION")
	public Long getFkMapVersion() {
		return fkMapVersion;
	}
	public void setFkMapVersion(Long fkMapVersion) {
		this.fkMapVersion = fkMapVersion;
	}
	
	@Column(name="COMP_SEQNO")
	public Long getCompSeqNum() {
		return compSeqNum;
	}
	public void setCompSeqNum(Long compSeqNum) {
		this.compSeqNum = compSeqNum;
	}
	
	@Column(name="COMPCOL_ID")
	public String getCompColID() {
		return compColID;
	}
	public void setCompColID(String compColID) {
		this.compColID = compColID;
	}
	
	@Column(name="COMPCOL_NAME")
	public String getCompColName() {
		return compColName;
	}
	public void setCompColName(String compColName) {
		this.compColName = compColName;
	}
	
	@Column(name="PULLFROM_COLS")
	public String getPullFromCols() {
		return pullFromCols;
	}
	public void setPullFromCols(String pullFromCols) {
		this.pullFromCols = pullFromCols;
	}
	
	@Column(name="EQMAINCDM_COL")
	public String getEqMainCdmCol() {
		return eqMainCdmCol;
	}
	public void setEqMainCdmCol(String eqMainCdmCol) {
		this.eqMainCdmCol = eqMainCdmCol;
	}
	
	@Column(name="EQCOMPREC_COL")
	public String getEqCompReqCol() {
		return eqCompReqCol;
	}
	public void setEqCompReqCol(String eqCompReqCol) {
		this.eqCompReqCol = eqCompReqCol;
	}
	
	@Column(name="POST_TOCDM")
	public Integer getPostToCDM() {
		return postToCDM;
	}
	public void setPostToCDM(Integer postToCDM) {
		this.postToCDM = postToCDM;
	}
	
	@Column(name="DISP_INSTAGING")
	public Integer getDispInStaging() {
		return dispInStaging;
	}
	public void setDispInStaging(Integer dispInStaging) {
		this.dispInStaging = dispInStaging;
	}
	
	@Column(name="RID")
	public Long getRid() {
		return rid;
	}
	public void setRid(Long rid) {
		this.rid = rid;
	}
	
	@Column(name="MAINCDM_COLTYPE")
	public String getMainCdmColType() {
		return mainCdmColType;
	}

	public void setMainCdmColType(String mainCdmColType) {
		this.mainCdmColType = mainCdmColType;
	}
	
	
}