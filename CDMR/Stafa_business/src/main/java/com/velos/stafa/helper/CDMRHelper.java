package com.velos.stafa.helper;

import java.io.InputStream;
import java.sql.CallableStatement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javassist.convert.Transformer;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.hql.ast.QuerySyntaxException;
import org.hibernate.transform.Transformers;

import atg.taglib.json.util.JSONObject;

import com.sun.corba.se.pept.transport.Connection;
import com.velos.stafa.business.domain.cdmr.CdmMain;
import com.velos.stafa.business.domain.cdmr.CdmModColList;
import com.velos.stafa.business.domain.cdmr.CdmNotification;
import com.velos.stafa.business.domain.cdmr.CdmNotifycnf;
import com.velos.stafa.business.domain.cdmr.CdmUserView;
import com.velos.stafa.business.domain.cdmr.CdmVersionDef;
import com.velos.stafa.business.domain.cdmr.CdmssLink;
import com.velos.stafa.business.domain.cdmr.ErrorLog;
import com.velos.stafa.business.domain.cdmr.FilesLibrary;
import com.velos.stafa.business.domain.cdmr.GrpCode;
import com.velos.stafa.business.domain.cdmr.ObiSvcBufr;
import com.velos.stafa.business.domain.cdmr.StagingCompRec;
import com.velos.stafa.business.domain.cdmr.StagingDetail;
import com.velos.stafa.business.domain.cdmr.StagingMain;
import com.velos.stafa.business.util.HibernateRemoteUtil;
import com.velos.stafa.business.util.HibernateUtil;
import com.velos.stafa.business.util.ObjectTransfer;
import com.velos.stafa.business.util.VelosConstants;
import com.velos.stafa.controller.CDMRController;
import com.velos.stafa.helper.VelosHelper;
import com.velos.stafa.util.CDMRUtil;
import com.velos.stafa.util.CDMRSqlConstants;
import com.velos.stafa.business.domain.cdmr.CdmFilterMaint;
import com.velos.stafa.business.domain.cdmr.CdmFilterParam;

/**
 * @summary CDMRHelper
 * @description Helper Class of CDM implements business logic
 * @version 1.0
 * @file CDMRHelper.java
 * @author Lalit Chattar
 */
public class CDMRHelper {

	/**
	 * @description store file information in DB
	 * @param jsonObject
	 * @return JSONObject
	 * @throws Exception
	 */

	private static CDMRController cdmrController = new CDMRController();

	public static JSONObject saveFileInfo(JSONObject jsonObject)
			throws Exception {
		Session session = null;

		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String cdmrFileDomain = VelosConstants.CDMR_FILE_DOMAIN;
			Object fileObj = VelosHelper.getNewInstance(cdmrFileDomain);

			fileObj = ObjectTransfer.tranferJSONObjecttoObject(jsonObject,
					fileObj);

			FilesLibrary fileLibrary = (FilesLibrary) fileObj;
			session.save(fileLibrary);
			
			jsonObject.put("pkfileID", fileLibrary.getPkFilelib().toString().trim());
			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::saveFileInfo", null);
			e.printStackTrace();

			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while saving the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	/**
	 * @description Fetch file information from DB
	 * @param jsonObject
	 * @return JSONObject
	 * @throws Exception
	 */
	public static JSONObject getFilesInfo(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		List<Object> returnList = new ArrayList();
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("filesinfo", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("filesinfo", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getFilesInfo", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	/**
	 * @description store staging main information in DB
	 * @param jsonObject
	 * @return JSONObject
	 * @throws Exception
	 */
	public static JSONObject saveStagingMainInfo(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String cdmrStagingMainInfo = VelosConstants.CDMR_STAGING_MAIN;
			Object stagingMainObj = VelosHelper
					.getNewInstance(cdmrStagingMainInfo);
			stagingMainObj = ObjectTransfer.tranferJSONObjecttoObject(
					jsonObject, stagingMainObj);
			StagingMain stagingMain = (StagingMain) stagingMainObj;
			if (jsonObject.getString("pkfileID") != null) {
				stagingMain.setFkFilelib(jsonObject.getLong("pkfileID"));
			}

			session.save(stagingMain);
			jsonObject.put("stagingMainID", stagingMain.getPkStagingMain());
			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::saveStagingMainInfo", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while saving the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	/**
	 * @description store staging detail information in DB
	 * @param jsonObject
	 * @return JSONObject
	 * @throws Exception
	 */
	public static JSONObject saveStagingDetailInfo(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String cdmrStagingDetailInfo = VelosConstants.CDMR_STAGING_DETAIL;
			Object stagingDetailObj = VelosHelper
					.getNewInstance(cdmrStagingDetailInfo);
			stagingDetailObj = ObjectTransfer.tranferJSONObjecttoObject(
					jsonObject, stagingDetailObj);
			StagingDetail stagingDetail = (StagingDetail) stagingDetailObj;
			if (jsonObject.getString("stagingMainID") != null) {
				stagingDetail.setFkStagingMain(jsonObject
						.getLong("stagingMainID"));
			}
			session.save(stagingDetail);
			jsonObject.put("stagingDetailPk", stagingDetail
					.getPkStagingDetail());
			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::saveStagingDetailInfo", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while saving the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getCodeList(JSONObject jsonObject)	throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("codelist", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("codelist", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getCodeList", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getStagingID(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("staginid", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("staginid", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getStagingID", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getActiveMapVesion(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("mapversion", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("mapversion", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getActiveMapVesion", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getStagingMaps(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("mapversion", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("mapversion", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getStagingMaps", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getStagingPrimaryID(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("stagingpk", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("stagingpk", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getStagingPrimaryID", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getStagingDetailData(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper
						.getDataFromSQLQueryCDMR(query, session);
				jsonObject.put("stagingdetail", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("stagingdetail", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getStagingDetailData", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getCompReqColumnMapDetail(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("compreccolumnmap", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("compreccolumnmap", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getCompReqColumnMapDetail", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	/**
	 * @description store staging detail information in DB
	 * @param jsonObject
	 * @return JSONObject
	 * @throws Exception
	 */
	public static JSONObject saveCompRecInfo(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String comprec = VelosConstants.CDMR_COMPREC;
			Object compRecObj = VelosHelper.getNewInstance(comprec);
			compRecObj = ObjectTransfer.tranferJSONObjecttoObject(jsonObject,
					compRecObj);
			StagingCompRec comprecDetail = (StagingCompRec) compRecObj;
			session.save(comprecDetail);
			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::saveCompRecInfo", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while saving the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getStagingColumnMapDetail(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("stagingcolumnmap", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("stagingcolumnmap", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getStagingColumnMapDetail", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject saveInStagingCompReq(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeQueryString(query, session);
				jsonObject.put("stagingcolumnmap", updateflag);
			} else {
				updateflag = executeQueryString(query, session);
				jsonObject.put("stagingcolumnmap", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::saveInStagingCompReq", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {

			if (session.isOpen()) {
				session.close();

			}
		}
		return jsonObject;
	}

	public static JSONObject getCompRecData(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("comprecdata", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("comprecdata", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getCompRecData", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject countRecord(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("recordcount", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("recordcount", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::countRecord", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getGroups(JSONObject jsonObject) throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("groupcode", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("groupcode", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getGroups", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject updateComrec(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeQueryString(query, session);
				jsonObject.put("updateflag", updateflag);
			} else {
				updateflag = executeQueryString(query, session);
				jsonObject.put("updateflag", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::updateComrec", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	// public static JSONObject updateFileStatus(JSONObject jsonObject) throws
	// Exception{
	// Session session = null;
	// try{
	// int updateflag = 0;
	// session = HibernateUtil.getSessionFactory().getCurrentSession();
	// session.beginTransaction();
	// String query = jsonObject.getString("query");
	// if(jsonObject.getBoolean("SQLQuery")){
	// updateflag = executeQueryString(query, session);
	// jsonObject.put("updateflag", updateflag);
	// }else{
	// updateflag = executeQueryString(query, session);
	// jsonObject.put("updateflag", updateflag);
	// }
	//			
	// session.getTransaction().commit();
	// }catch (Exception e) {
	// CDMRUtil.errorLogUtil(e, "", cdmrController,
	// "CDMRHelper::deleteNotifiedUser");e.printStackTrace();
	// jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
	// "Error while getting the data.");
	// }finally{
	// if(session.isOpen()){
	// session.close();
	// }
	// }
	// return jsonObject;
	// }

	public static int executeQueryString(String queryBuffer, Session session)
			throws Exception {
		int updateflag = 0;
		try {

			System.out.println("queryBuffer  " + queryBuffer);
			Query query = session.createQuery(queryBuffer);

			updateflag = query.executeUpdate();
		} catch (QuerySyntaxException qse) {
			try {
				Query query = session.createSQLQuery(queryBuffer);

				updateflag = query.executeUpdate();
			} catch (Exception e) {
				sendErrorOnMail(e);
				CDMRUtil.errorLogUtil(e, "", cdmrController,
						"CDMRHelper::executeQueryString", null);
				e.printStackTrace();
			}
		} catch (Exception e) {
			sendErrorOnMail(e);
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::executeQueryString", null);
			e.printStackTrace();

		}
		return updateflag;
	}

	public static void sendErrorOnMail(Exception e) throws Exception {
		if (e instanceof org.hibernate.SessionException) {
			return;
		}
		JSONObject jsonObject = CDMRUtil.getMailConfiguration();
		jsonObject.put("BODY",
				"There is an error occured while processing file.");
		jsonObject = CDMRUtil.getUsernameAndEmailAddress(jsonObject);
		List<Map> mailObjList = (List<Map>) jsonObject.get("notifycnf");
		if (mailObjList.size() != 0) {
			for (Map map : mailObjList) {
				String to = map.get("USER_EMAIL") == null ? null : map.get(
						"USER_EMAIL").toString();

				jsonObject.put("TO", to);
				if (to != null)
					CDMRUtil.sendNotificationMail(jsonObject);
			}
		}
	}

	public static int executeSQLQueryString(String queryBuffer, Session session)
			throws Exception {
		int updateflag = 0;
		try {

			Query query = session.createSQLQuery(queryBuffer);

			updateflag = query.executeUpdate();
		} catch (QuerySyntaxException qse) {

			try {
				Query query = session.createSQLQuery(queryBuffer);

				updateflag = query.executeUpdate();
			} catch (Exception e) {
				sendErrorOnMail(e);
				CDMRUtil.errorLogUtil(e, "", cdmrController,
						"CDMRHelper::executeSQLQueryString", null);
				e.printStackTrace();
			}
		} catch (Exception e) {
			sendErrorOnMail(e);
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::executeSQLQueryString", null);
			e.printStackTrace();
		}
		return updateflag;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject getGroupCodeValue(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("groupcodename", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("groupcodename", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getGroupCodeValue", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject getNextPossibleStatus(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("possiblestatus", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("possiblestatus", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getNextPossibleStatus", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject insertInCDMMain(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeQueryString(query, session);
				jsonObject.put("inserted", updateflag);
			} else {
				updateflag = executeQueryString(query, session);
				jsonObject.put("inserted", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::insertInCDMMain", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getMatchedData(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("searched", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("searched", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getMatchedData", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject saveInMainCDM(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeQueryString(query, session);
				jsonObject.put("updateflag", updateflag);
			} else {
				updateflag = executeQueryString(query, session);
				jsonObject.put("updateflag", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::saveInMainCDM", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getCDMVersionDetail(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("versioninfo", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("versioninfo", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getCDMVersionDetail", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject saveCdmVersionDef(JSONObject jsonObject)
			throws Exception {

		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String cdmrVersionDomain = VelosConstants.CDM_VERSION_DEF;
			Object versionObj = VelosHelper.getNewInstance(cdmrVersionDomain);
			versionObj = ObjectTransfer.tranferJSONObjecttoObject(jsonObject,
					versionObj);
			CdmVersionDef versionDef = (CdmVersionDef) versionObj;
			session.save(versionDef);
			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::saveCdmVersionDef", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while saving the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject updateCdmVersionDef(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeQueryString(query, session);
				jsonObject.put("inserted", updateflag);
			} else {
				updateflag = executeQueryString(query, session);
				jsonObject.put("inserted", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::updateCdmVersionDef", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getFromCdmMain(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper
						.getDataFromSQLQueryCDMR(query, session);
				jsonObject.put("cdmmain", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("cdmmain", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getFromCdmMain", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject saveInSSLink(JSONObject jsonObject)
			throws Exception {

		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String cdmSSLink = VelosConstants.CDM_SSLINK;
			Object ssLinkObj = VelosHelper.getNewInstance(cdmSSLink);
			ssLinkObj = ObjectTransfer.tranferJSONObjecttoObject(jsonObject,
					ssLinkObj);
			CdmssLink cdmSSLinkObj = (CdmssLink) ssLinkObj;
			session.save(cdmSSLinkObj);
			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::saveInSSLink", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while saving the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject getFromCdmSSLink(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("cdmsslink", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("cdmsslink", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getFromCdmSSLink", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject saveNewServiceSet(JSONObject jsonObject)
			throws Exception {

		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String cdmmain = VelosConstants.CDM_MAIN;
			Object cdmmainObj = VelosHelper.getNewInstance(cdmmain);
			cdmmainObj = ObjectTransfer.tranferJSONObjecttoObject(jsonObject,
					cdmmainObj);
			CdmMain cdmMainObj = (CdmMain) cdmmainObj;
			session.save(cdmMainObj);
			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::saveNewServiceSet", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while saving the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject getMaxGrupCode(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				Query sqlquery = session.createSQLQuery(query);
				returnList = sqlquery.list();
				jsonObject.put("MaxGrupCode", returnList);
			} else {
				Query sqlquery = session.createSQLQuery(query);
				returnList = sqlquery.list();
				jsonObject.put("MaxGrupCode", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getMaxGrupCode", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject saveInCdmGrupCode(JSONObject jsonObject)
			throws Exception {

		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String grpDomain = VelosConstants.CDMR_GRUP_DOMAIN;
			Object grpObj = VelosHelper.getNewInstance(grpDomain);
			grpObj = ObjectTransfer.tranferJSONObjecttoObject(jsonObject,
					grpObj);
			GrpCode grpLib = (GrpCode) grpObj;

			session.save(grpLib);
			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::saveInCdmGrupCode", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while saving the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject updateInCdmGrupCode(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeQueryString(query, session);
				jsonObject.put("updateflag", updateflag);
			} else {
				updateflag = executeQueryString(query, session);
				jsonObject.put("updateflag", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::updateInCdmGrupCode", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getFromCDMGroupCode(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("cdmgrpcode", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("cdmgrpcode", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getFromCDMGroupCode", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getTechnicalCharges(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateRemoteUtil.getSessionFactory()
					.getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("remotedata", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("remotedata", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getTechnicalCharges", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
			throw e;
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getProfessionalCharges(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateRemoteUtil.getSessionFactory()
					.getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("remotedata", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("remotedata", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getProfessionalCharges", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
			throw e;
		} finally {
			if (session.isOpen()) {
				session.close();

			}
		}
		return jsonObject;
	}

	public static JSONObject insertInStagingTemp(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeQueryString(query, session);
				jsonObject.put("inserted", updateflag);
			} else {
				updateflag = executeQueryString(query, session);
				jsonObject.put("inserted", updateflag);
			}

			// session.flush();
			// session.clear();
			session.getTransaction().commit();
			// session.close();

		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::insertInStagingTemp", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
			throw e;
		} finally {

			if (session.isOpen()) {
				session.close();

			}
		}
		return jsonObject;
	}

	public static JSONObject truncateStagingTemp(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("truncated", updateflag);
			} else {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("truncated", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::truncateStagingTemp", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getMappingForSQLBinding(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("sqlbindingmapdata", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("sqlbindingmapdata", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getMappingForSQLBinding", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getFromStagingTemp(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				// String cdmrStagingDetailInfo =
				// VelosConstants.CDMR_STAGING_DETAIL;
				// Object stagingDetailObj =
				// VelosHelper.getNewInstance(cdmrStagingDetailInfo);
				// stagingDetailObj =
				// ObjectTransfer.tranferJSONObjecttoObject(jsonObject,
				// stagingDetailObj);
				// StagingDetail stagingDetail =
				// (StagingDetail)stagingDetailObj;
				jsonObject.put("stagingTemp", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("stagingTemp", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getFromStagingTemp", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject saveStagingDetail(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		int updateflag = 0;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {

				updateflag = executeQueryString(query, session);
				jsonObject.put("inserted", updateflag);

			} else {
				updateflag = executeQueryString(query, session);
				jsonObject.put("inserted", updateflag);

			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::saveStagingDetail", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getChagIndc(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("chrgindc", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("chrgindc", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getChagIndc", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject updateFilesLibForRemoteData(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			} else {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::updateFilesLibForRemoteData", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject editServiceSection(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			} else {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::editServiceSection", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject editServiceSet(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			} else {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::editServiceSet", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getUserDetail(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("userdetail", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("userdetail", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getUserDetail", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getFromNotifycnf(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("notifycnf", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("notifycnf", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getFromNotifycnf", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getFromEsec(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("esecdata", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("esecdata", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getFromEsec", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getSchedulerTime(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("scheduler", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("scheduler", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getSchedulerTime", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject saveNotificationInfo(JSONObject jsonObject)
			throws Exception {

		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String notifyDomain = VelosConstants.CDMR_NOTIFICATION_DOMAIN;
			Object notifyObj = VelosHelper.getNewInstance(notifyDomain);
			notifyObj = ObjectTransfer.tranferJSONObjecttoObject(jsonObject,
					notifyObj);
			CdmNotifycnf notification = (CdmNotifycnf) notifyObj;
			session.save(notification);
			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::saveNotificationInfo", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while saving the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject updateFileStatus(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			FilesLibrary filesLibrary = (FilesLibrary) session.get(
					FilesLibrary.class, Long.parseLong(jsonObject
							.getString("fileid")));
			filesLibrary.setFileDes(jsonObject.getString("desc"));
			filesLibrary.setFileCurrentStatus(jsonObject.getInt("newStatus"));
			filesLibrary.setLastModifiedBy(Long.parseLong(jsonObject
					.getString("lastModifiedBy")));
			filesLibrary.setLastModifiedOn((Date) jsonObject
					.get("lastModifiedOn"));
			filesLibrary.setComment(jsonObject.getString("desc"));
			session.update(filesLibrary);
			Criteria criteria = session.createCriteria(StagingMain.class);
			criteria.add(Restrictions.eq("fkFilelib", Long.parseLong(jsonObject
					.getString("fileid"))));
			StagingMain main = (StagingMain) criteria.uniqueResult();
			main.setFileCurrentStatus(jsonObject.getInt("newStatus"));
			main.setComment(jsonObject.getString("desc"));
			main.setLastModifiedBy(Long.parseLong(jsonObject
					.getString("lastModifiedBy")));
			main.setLastModifiedOn((Date) jsonObject.get("lastModifiedOn"));
			session.update(main);
			// String query = jsonObject.getString("query");
			// if(jsonObject.getBoolean("SQLQuery")){
			// updateflag = executeQueryString(query, session);
			// jsonObject.put("updateflag", updateflag);
			// }else{
			// updateflag = executeQueryString(query, session);
			// jsonObject.put("updateflag", updateflag);
			// }
			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::updateFileStatus", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject updateSchdulerTime(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			} else {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::updateSchdulerTime", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject deleteNotifiedUser(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			} else {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::deleteNotifiedUser", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject deleteNotification(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			} else {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::deleteNotification", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject loadAllNotification(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("notification", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("notification", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::loadAllNotification", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject saveErrorLog(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String cdmrErrorLog = VelosConstants.CDMR_ERROR_LOG;
			Object errorLogObj = VelosHelper.getNewInstance(cdmrErrorLog);
			errorLogObj = ObjectTransfer.tranferJSONObjecttoObject(jsonObject,
					errorLogObj);
			ErrorLog errorLog = (ErrorLog) errorLogObj;
			session.save(errorLog);
			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::saveErrorLog", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while saving the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject updateErrorLog(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			} else {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::updateErrorLog", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getMailConfigurationInfo(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("mailconfig", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("mailconfig", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getMailConfigurationInfo", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getNotificationMessages(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("notimesg", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("notimesg", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getMailConfigurationInfo", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject updateNotification(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			} else {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::updateNotification", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject updateEmailSettings(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			} else {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::updateEmailSettings", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getEmailSettings(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("mailsetting", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("mailsetting", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getEmailSettings", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject updateNoOfFiles(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			} else {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::updateNoOfFiles", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getNumberOfFiles(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("nooffiles", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("nooffiles", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getNumberOfFiles", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getSuperAdmin(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("superadmin", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("superadmin", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getSuperAdmin", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject saveForgotPasswordRequest(JSONObject jsonObject)
			throws Exception {

		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String cdmrNotification = VelosConstants.CDMR_NOTIFICATION;
			Object notifObj = VelosHelper.getNewInstance(cdmrNotification);
			notifObj = ObjectTransfer.tranferJSONObjecttoObject(jsonObject,
					notifObj);
			CdmNotification notification = (CdmNotification) notifObj;
			session.save(notification);
			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::saveForgotPasswordRequest", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while saving the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject getDefaultPassword(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("defaultpassword", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("defaultpassword", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getDefaultPassword", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getUserEmail(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("usermail", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("usermail", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getUserEmail", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject updateNotificationStatus(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			} else {
				updateflag = executeSQLQueryString(query, session);
				jsonObject.put("updated", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::updateNotificationStatus", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject saveInObsSvcBfr(JSONObject jsonObject)
			throws Exception {

		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String cdm_obi_svc = VelosConstants.CDM_OBI_SVCBFR;
			Object obi_svc = VelosHelper.getNewInstance(cdm_obi_svc);
			obi_svc = ObjectTransfer.tranferJSONObjecttoObject(jsonObject,
					obi_svc);
			ObiSvcBufr obiSvcBufr = (ObiSvcBufr) obi_svc;
			session.save(obiSvcBufr);
			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::saveInObsSvcBfr", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while saving the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static List<Map> fetchColumnForDataTable(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		List<Map> data = new ArrayList<Map>();
		try {
			String modName = jsonObject.getString("MODULENAME");
			Long userid = jsonObject.getLong("user");
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String subsql1 = "(select pkCdmModule from CdmModuleList where modName =:moduleName)";
			String subsql2 = "select cdm.pkCdmModCol from CdmModColList cdm where cdm.fkCdmModule="
					+ subsql1;
			String sql = "select cdmview.pkCdmUserView from CdmUserView cdmview where fkCdmModCol in("
					+ subsql2 + ") " + "and userID=:userid";
			Query query = session.createQuery(sql);
			query.setParameter("moduleName", modName);
			query.setParameter("userid", userid);
			List listResult = query.list();
			Iterator it;
			if (listResult.size() == 0) {
				String savesubsql = "(select pkCdmModule from CdmModuleList where modName =:moduleName)";
				String savesql = "select cdm.columnName as columnName,cdm.columnProp as columnProp,"
						+ "cdm.columnPropVal as columnPropVal ,cdm.dispStatus as dispStatus from CdmModColList cdm "
						+ "where cdm.fkCdmModule="
						+ savesubsql
						+ " order by cdm.colSeq ";
				Query savequery = session.createQuery(savesql);
				savequery.setParameter("moduleName", modName);
				savequery.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				List savelist = savequery.list();
				session.getTransaction().commit();
				it = savelist.iterator();

			} else {
				String getSql = "SELECT cdm.pkCdmModCol as pkCdmModCol,cdm.columnName as columnName,cdm.columnProp as columnProp,cdm.columnPropVal as columnPropVal,cdmu.dispStatus as dispStatus "
						+ "FROM CdmModColList cdm,CdmUserView cdmu,CdmModuleList module where cdm.pkCdmModCol=cdmu.fkCdmModCol "
						+ "and module.pkCdmModule=cdm.fkCdmModule and cdmu.userID=:userid and module.modName=:moduleName order by (cdmu.colSeq)";
				Query savequery = session.createQuery(getSql);
				savequery.setParameter("moduleName", modName);
				savequery.setParameter("userid", userid);
				savequery.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				List savelist = savequery.list();
				session.getTransaction().commit();
				it = savelist.iterator();

			}
			while (it.hasNext()) {
				Map m = (Map) it.next();
				String colname = (String) m.get("columnName");
				String colProp = (String) m.get("columnProp");
				String colPropVal = (String) m.get("columnPropVal");
				String colPArr[] = colProp.split("-");
				String colPVal[] = colPropVal.split("-");
				int disStatus = (Integer) m.get("dispStatus");
				Map<String, Object> test = new HashMap<String, Object>();
				test.put("sTitle", colname);
				if (disStatus == 1)
					test.put("bVisible", true);
				else
					test.put("bVisible", false);
				if (colPArr.length > 0) {
					for (int i = 0; i < colPArr.length; i++) {
						if (colPVal[i].equalsIgnoreCase("true")
								|| colPVal[i].equalsIgnoreCase("false"))
							test.put(colPArr[i], Boolean.valueOf(colPVal[i]));
						else
							test.put(colPArr[i], colPVal[i]);
					}
				} else {
					if ((colPropVal != null) && (colProp != null)) {
						if (colPropVal.equalsIgnoreCase("true")
								|| colPropVal.equalsIgnoreCase("false"))
							test.put(colProp, Boolean.valueOf(colPropVal));
						else
							test.put(colProp, colPropVal);
					}
				}
				data.add(test);
			}
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::fetchColumnForDataTable", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while fetching the column info.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return data;
	}

	public static void saveColumnState(JSONObject jsonObject) throws Exception {
		Session session = null;
		try {
			String modName = jsonObject.getString("module");
			Long userid = jsonObject.getLong("user");
			String colValue = jsonObject.getString("colvalue");
			JSONObject myjsonObject = new JSONObject(colValue);
			HashMap<String, Boolean> map = myjsonObject.toHashMap();
			// Set<Integer> keys=map.keySet();
			// Set<String> keys=jsonObject.keySet();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String subsql1 = "(select pkCdmModule from CdmModuleList where modName =:moduleName)";
			String subsql2 = "select cdm.pkCdmModCol from CdmModColList cdm where cdm.fkCdmModule="
					+ subsql1;
			String sql = "select cdmview.pkCdmUserView as pkCdmUserView,cdmview.colSeq as colSeq  from CdmUserView cdmview where cdmview.fkCdmModCol in("
					+ subsql2 + ") " + "and cdmview.userID=:userid";
			Query query = session.createQuery(sql);
			query.setParameter("moduleName", modName);
			query.setParameter("userid", userid);
			query.setResultTransformer(Transformers
					.aliasToBean(CdmUserView.class));
			List listResult = query.list();

			if (listResult.size() == 0) {
				// session.beginTransaction();
				String ressql = "select cdm.pkCdmModCol as pkCdmModCol,cdm.colSeq as colSeq from CdmModColList cdm "
						+ "where cdm.fkCdmModule="
						+ subsql1
						+ " order by cdm.colSeq  ";
				Query query1 = session.createQuery(ressql);
				query1.setParameter("moduleName", modName);
				query1.setResultTransformer(Transformers
						.aliasToBean(CdmModColList.class));
				List list = query1.list();
				Iterator it = list.iterator();
				while (it.hasNext()) {
					CdmUserView cdmview = new CdmUserView();
					CdmModColList m = (CdmModColList) it.next();

					Boolean val = map.get(m.getColSeq().toString());

					cdmview.setUserID(userid);
					cdmview.setFkCdmModCol(m.getPkCdmModCol());
					cdmview.setColSeq(m.getColSeq());
					if (val)
						cdmview.setDispStatus(1);
					else
						cdmview.setDispStatus(0);
					session.save(cdmview);
				}
				session.getTransaction().commit();
			} else {
				Iterator it = listResult.iterator();
				while (it.hasNext()) {
					CdmUserView cdmview = (CdmUserView) it.next();
					String updatesql = "update CdmUserView cdmview set cdmview.dispStatus=:dispStatus where cdmview.pkCdmUserView=:pkcdmuser";
					Query updateQuery = session.createQuery(updatesql);
					System.out.println(cdmview.getColSeq());
					Boolean dssstatus = map.get(Integer.toString(cdmview
							.getColSeq()));
					if (dssstatus)
						updateQuery.setParameter("dispStatus", 1);
					else
						updateQuery.setParameter("dispStatus", 0);
					updateQuery.setParameter("pkcdmuser", cdmview
							.getPkCdmUserView());
					updateQuery.executeUpdate();
				}
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::fetchColumnForDataTable", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while fetching the column info.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
	}

	public static void saveColumnOrder(JSONObject jsonObject) throws Exception {
		Session session = null;
		try {
			String modName = jsonObject.getString("module");
			Long userid = jsonObject.getLong("user");
			String colValue = jsonObject.getString("colvalue");
			JSONObject myjsonObject = new JSONObject(colValue);
			HashMap<String, Integer> map = myjsonObject.toHashMap();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String subsql1 = "(select pkCdmModule from CdmModuleList where modName =:moduleName)";
			String subsql2 = "select cdm.pkCdmModCol from CdmModColList cdm where cdm.fkCdmModule="
					+ subsql1;
			String sql = "select cdmview.pkCdmUserView as pkCdmUserView,cdmview.colSeq as colSeq  from CdmUserView cdmview where cdmview.fkCdmModCol in("
					+ subsql2 + ") " + "and cdmview.userID=:userid";
			Query query = session.createQuery(sql);
			query.setParameter("moduleName", modName);
			query.setParameter("userid", userid);
			query.setResultTransformer(Transformers
					.aliasToBean(CdmUserView.class));
			List listResult = query.list();

			if (listResult.size() == 0) {
				// session.beginTransaction();
				String ressql = "select cdm.pkCdmModCol as pkCdmModCol,cdm.columnName as columnName,cdm.dispStatus as dispStatus from CdmModColList cdm "
						+ "where cdm.fkCdmModule=" + subsql1;
				Query query1 = session.createQuery(ressql);
				query1.setParameter("moduleName", modName);
				query1.setResultTransformer(Transformers
						.aliasToBean(CdmModColList.class));
				List list = query1.list();
				Iterator it = list.iterator();
				while (it.hasNext()) {
					CdmUserView cdmview = new CdmUserView();
					CdmModColList m = (CdmModColList) it.next();
					int seq = (Integer) map.get(m.getColumnName());
					cdmview.setUserID(userid);
					cdmview.setFkCdmModCol(m.getPkCdmModCol());
					cdmview.setColSeq(seq);
					cdmview.setDispStatus(m.getDispStatus());
					session.save(cdmview);
				}
				session.getTransaction().commit();
			} else {
				String ressql = "select cdm.pkCdmModCol as pkCdmModCol,cdm.columnName as columnName from CdmModColList cdm "
						+ "where cdm.fkCdmModule=" + subsql1;
				Query query1 = session.createQuery(ressql);
				query1.setParameter("moduleName", modName);
				query1.setResultTransformer(Transformers
						.aliasToBean(CdmModColList.class));
				List list = query1.list();
				Iterator it = list.iterator();
				while (it.hasNext()) {
					String updatesql = "update CdmUserView cdmview set cdmview.colSeq=:colseq where cdmview.fkCdmModCol=:fkcdmuser and userID=:userid";
					Query updateQuery = session.createQuery(updatesql);
					CdmModColList m = (CdmModColList) it.next();
					int seq = (Integer) map.get(m.getColumnName());
					updateQuery.setParameter("colseq", seq);
					updateQuery.setParameter("fkcdmuser", m.getPkCdmModCol());
					updateQuery.setParameter("userid", userid);
					updateQuery.executeUpdate();
				}
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::fetchColumnForDataTable", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while fetching the column info.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
	}

	public static JSONObject getFilePkForStatus(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("errorStatuspk", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("errorStatuspk", returnList);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getMailConfigurationInfo", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getFileSourceLocation(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQueryWithoutEntityMap(
						query, session);

				jsonObject.put("file_source_location", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("file_source_location", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject getFileInformation(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {

				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("fileinfo", returnList);
			} else {

				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("fileinfo", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static boolean checkForExistingFile(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {

				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				if (returnList.size() == 0)
					return false;
				else
					return true;
			} else {

				returnList = VelosHelper.getDataFromHQLQuery(query, session);

				if (returnList.size() == 0)
					return false;
				else
					return true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return true;

	}

	public static JSONObject getPullMethod(JSONObject jsonObject)
			throws Exception {
		Session session = null;

		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("pullmethod", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("pullmethod", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject getChgIndc(JSONObject jsonObject) throws Exception {
		Session session = null;
		String chgIndc = "";
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("chgind", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("chgind", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static List getInterfaceList(JSONObject jsonObject) throws Exception {
		Session session = null;
		List returnList = new ArrayList();
		try {

			// List<String> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("interfacelist", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("interfacelist", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return returnList;

	}

	public static JSONObject getProcessedDirLocation(JSONObject jsonObject) {
		Session session = null;
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("processedDir", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("processedDir", returnList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getFileName(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {

				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("file_name", returnList);
			} else {

				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("file_name", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getFileID(JSONObject jsonObject) throws Exception {
		Session session = null;
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {

				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("file_id", returnList);
			} else {

				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("file_id", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getModulename(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {

				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("module_name", returnList);

			} else {

				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("module_name", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getMatchingCriteria(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {

				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("file_key", returnList);

			} else {

				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("file_key", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getEventLibId(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {

				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("eventLibID", returnList);
			} else {

				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("eventLibID", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject getchgtype(JSONObject jsonObject) throws Exception {
		Session session = null;
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {

				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("chg_type", returnList);
			} else {

				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("chg_type", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject getChgIndcBySeq(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		String chgIndc = "";
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("chgseqind", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("chgseqind", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject fetchEventLibId(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		String chgIndc = "";
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("eventlid", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("eventlid", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject fetchVersionID(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		String chgIndc = "";
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("verid", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("verid", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject getActiveRule(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		String chgIndc = "";
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("activerule", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("activerule", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject fetchRTRule(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		String chgIndc = "";
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("rtyperule", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("rtyperule", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject fetchcolTypeMapDetail(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		String chgIndc = "";
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("coltypemap", returnList);
			} else {
				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("coltypemap", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject getAdvsearchCols(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {

				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("advncesrch_cols", returnList);

			} else {

				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("advncesrch_cols", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject checkForFilterName(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {

				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("filternamelist", returnList);
			} else {

				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("filternamelist", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject saveNewFilter(JSONObject jsonObject)
			throws Exception {

		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String cdmfiltermaint = VelosConstants.CDMR_FILTER_MAINT;
			Object cdmfilterobj = VelosHelper.getNewInstance(cdmfiltermaint);
			cdmfilterobj = ObjectTransfer.tranferJSONObjecttoObject(jsonObject,
					cdmfilterobj);
			CdmFilterMaint cdmFilObj = (CdmFilterMaint) cdmfilterobj;
			session.save(cdmFilObj);
			jsonObject.put("filterID", cdmFilObj.getPkFiltermaint());

			session.getTransaction().commit();

		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::saveNewFilter", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while saving the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject loadFilters(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {

				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("filternames", returnList);
			} else {

				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("filternames", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject saveFilterParam(JSONObject jsonObject)
			throws Exception {

		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String cdmfilterparam = VelosConstants.CDMR_FILTER_PARAM;
			Object cdmparamobj = VelosHelper.getNewInstance(cdmfilterparam);
			cdmparamobj = ObjectTransfer.tranferJSONObjecttoObject(jsonObject,
					cdmparamobj);
			CdmFilterParam cdmFilObj = (CdmFilterParam) cdmparamobj;
			session.save(cdmFilObj);
			jsonObject.put("filterparamID", cdmFilObj.getPkFilterparam());
			session.getTransaction().commit();

		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::saveFilterParam", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while saving the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject updateCdmParam(JSONObject jsonObject)
			throws Exception {

		Session session = null;
		try {
			int updateflag = 0;
			
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeQueryString(query, session);
				jsonObject.put("updateflag", updateflag);
			} else {
				updateflag = executeQueryString(query, session);
				jsonObject.put("updateflag", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,"CDMRHelper::updateFilterParam", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,"Error while saving the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject updateCdmFilMaint(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {
			int updateflag = 0;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				updateflag = executeQueryString(query, session);
				jsonObject.put("updateflag", updateflag);
			} else {
				updateflag = executeQueryString(query, session);
				jsonObject.put("updateflag", updateflag);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::updateCdmFilMaint", null);
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getCdmFilParam(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {

				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("savedfiltersdetails", returnList);
			} else {

				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("savedfiltersdetails", returnList);
			}

		} catch (Exception e) {
			CDMRUtil.errorLogUtil(e, "", cdmrController,
					"CDMRHelper::getCdmFilParam", null);
			e.printStackTrace();
			jsonObject.put(VelosConstants.SUCCESS_MESSAGE,
					"Error while getting the data.");
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getSavedFilters(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {

				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("savedfilters", returnList);
			} else {

				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("savedfilters", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject getPkforFilter(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		try {

			List<Object> returnList = new ArrayList();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {

				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("savedfilternamebypk", returnList);
			} else {

				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("savedfilternamebypk", returnList);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}

	public static JSONObject dataTransferStatus(JSONObject jsonObject)
			throws Exception {

		List<Map> returnList = new ArrayList();
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			returnList = VelosHelper.getDataFromSQLQuery(query, session);
			jsonObject.put("datatransferflag", returnList.get(0).get("CODELST_VALUE"));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}
	public static JSONObject getexportDatacount(JSONObject jsonObject)
			throws Exception {
		List<Map> returnList = new ArrayList();
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");

			if (jsonObject.getBoolean("SQLQuery")) {

				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("exportcount", returnList);
			} else {

				returnList = VelosHelper.getDataFromHQLQuery(query, session);
				jsonObject.put("exportcount", returnList);
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;

	}

	public static JSONObject transferToCtms()throws Exception {
		Session session = null;
		JSONObject jsonObject = new JSONObject();
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			Properties prop = new Properties();
			
            prop.load(CDMRUtil.class.getClassLoader().getResourceAsStream("/cdmr.properties"));
            String url = prop.getProperty("URL");
            String userName=prop.getProperty("USERNAME");
            String password=prop.getProperty("PASSWORD");
           
			java.sql.Connection con = HibernateUtil.getDBConnection(url,userName, password);
			
			CallableStatement cs = con.prepareCall("{call CDMR_CTMS_OBI(?)}");
			cs.registerOutParameter(1, java.sql.Types.VARCHAR);
			cs.executeQuery();
			String result = cs.getString(1);
			jsonObject.put("CRMSDataTransferResult",result);
			session.flush();
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
		

	}

	public static JSONObject updateTransferstatus(JSONObject jsonObject) {
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			String query = jsonObject.getString("query");
			if (jsonObject.getBoolean("SQLQuery")) {
				executeQueryString(query, session);
			}
			else{
				executeQueryString(query, session);
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}
	public static JSONObject saveNewNotification(JSONObject jsonObject) throws Exception {
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			
			String cdmrNotification = VelosConstants.CDMR_NOTIFICATION;
			Object notifObj = VelosHelper.getNewInstance(cdmrNotification);
			notifObj = ObjectTransfer.tranferJSONObjecttoObject(jsonObject,	notifObj);
			CdmNotification notification = (CdmNotification) notifObj;
			session.save(notification);
			session.getTransaction().commit();
			
		}
		catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return jsonObject;
	}
		public static JSONObject getPasswordAuthFlag(JSONObject jsonObject)	throws Exception {
	
			List<Map> returnList = new ArrayList();
			Session session = null;
			try {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
				String query = jsonObject.getString("query");
				returnList = VelosHelper.getDataFromSQLQuery(query, session);
				jsonObject.put("passwordauthflag", returnList.get(0).get("PARAM_VAL"));
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (session.isOpen()) {
					session.close();
				}
	}
	return jsonObject;
	
	}

	public static JSONObject getLibraryName(JSONObject jsonObject) throws Exception {
	Session session = null;
	try {

		List<Object> returnList = new ArrayList();
		session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		String query = jsonObject.getString("query");

		if (jsonObject.getBoolean("SQLQuery")) {

			returnList = VelosHelper.getDataFromSQLQuery(query, session);
			jsonObject.put("lib_name", returnList);

		} else {

			returnList = VelosHelper.getDataFromHQLQuery(query, session);
			jsonObject.put("lib_name", returnList);
		}

	} catch (Exception e) {
		e.printStackTrace();

	} finally {
		if (session.isOpen()) {
			session.close();
		}
	}
	return jsonObject;
}
}