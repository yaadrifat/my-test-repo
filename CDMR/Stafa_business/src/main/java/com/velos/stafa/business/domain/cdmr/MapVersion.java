package com.velos.stafa.business.domain.cdmr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.velos.stafa.business.domain.BaseDomainObject;

/**
 * @summary     MapVersion
 * @description Mapping class of MAPSVERSION Table
 * @version     1.0
 * @file        MapVersion.java
 * @author      Lalit Chattar
 */

@Entity
@Table(name="CDM_MAPSVERSION")
@javax.persistence.SequenceGenerator(name="mapversion_sqnc", sequenceName="SEQ_CDM_MAPSVERSION")
public class MapVersion extends BaseDomainObject {
	
	
	private Long pkMapVersion;
	private Long fkAccount;
	private String mapVersion;
	private String versionDesc;
	private Integer sheetNo;
	private Integer instColPos;
	private String addInst;
	private String editInst;
	private String dactInst;
	private String ractInst;
	private String delInst;
	private Integer thRow;
	private Integer lwCols;
	private String fileID;
	private String fileExt;
	private Integer procRem;
	private String srchRem;
	private String srchLinkFld;
	private Integer isActive;
	private Integer colLimit;
	private Long rid;
	
	@Id
	@GeneratedValue(generator="mapversion_sqnc",strategy=GenerationType.AUTO)
	@Column(name="PK_CDM_MAPSVERSION")
	public Long getPkMapVersion() {
		return pkMapVersion;
	}
	public void setPkMapVersion(Long pkMapVersion) {
		this.pkMapVersion = pkMapVersion;
	}
	
	@Column(name="FK_ACCOUNT")
	public Long getFkAccount() {
		return fkAccount;
	}
	public void setFkAccount(Long fkAccount) {
		this.fkAccount = fkAccount;
	}
	
	@Column(name="MAPSVERSION")
	public String getMapVersion() {
		return mapVersion;
	}
	public void setMapVersion(String mapVersion) {
		this.mapVersion = mapVersion;
	}
	
	@Column(name="VERSIONDESC")
	public String getVersionDesc() {
		return versionDesc;
	}
	public void setVersionDesc(String versionDesc) {
		this.versionDesc = versionDesc;
	}
	
	@Column(name="SHEET_NOS")
	public Integer getSheetNo() {
		return sheetNo;
	}
	public void setSheetNo(Integer sheetNo) {
		this.sheetNo = sheetNo;
	}
	
	@Column(name="INST_COLPOS")
	public Integer getInstColPos() {
		return instColPos;
	}
	public void setInstColPos(Integer instColPos) {
		this.instColPos = instColPos;
	}
	
	@Column(name="ADD_INST")
	public String getAddInst() {
		return addInst;
	}
	public void setAddInst(String addInst) {
		this.addInst = addInst;
	}
	
	@Column(name="EDIT_INST")
	public String getEditInst() {
		return editInst;
	}
	public void setEditInst(String editInst) {
		this.editInst = editInst;
	}
	
	@Column(name="DACT_INST")
	public String getDactInst() {
		return dactInst;
	}
	public void setDactInst(String dactInst) {
		this.dactInst = dactInst;
	}
	
	@Column(name="RACT_INST")
	public String getRactInst() {
		return ractInst;
	}
	public void setRactInst(String ractInst) {
		this.ractInst = ractInst;
	}
	
	@Column(name="DEL_INST")
	public String getDelInst() {
		return delInst;
	}
	public void setDelInst(String delInst) {
		this.delInst = delInst;
	}
	
	@Column(name="TH_ROWS")
	public Integer getThRow() {
		return thRow;
	}
	public void setThRow(Integer thRow) {
		this.thRow = thRow;
	}
	
	@Column(name="LM_COLS")
	public Integer getLwCols() {
		return lwCols;
	}
	public void setLwCols(Integer lwCols) {
		this.lwCols = lwCols;
	}
	
	@Column(name="FILE_EXT")
	public String getFileExt() {
		return fileExt;
	}
	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}
	
	@Column(name="PROC_REM")
	public Integer getProcRem() {
		return procRem;
	}
	public void setProcRem(Integer procRem) {
		this.procRem = procRem;
	}
	
	@Column(name="SRCH_REM")
	public String getSrchRem() {
		return srchRem;
	}
	public void setSrchRem(String srchRem) {
		this.srchRem = srchRem;
	}
	
	@Column(name="SRCH_LINK_FLD")
	public String getSrchLinkFld() {
		return srchLinkFld;
	}
	public void setSrchLinkFld(String srchLinkFld) {
		this.srchLinkFld = srchLinkFld;
	}
	
	@Column(name="COLS_LIMIT")
	public Integer getColLimit() {
		return colLimit;
	}
	public void setColLimit(Integer colLimit) {
		this.colLimit = colLimit;
	}
	
	@Column(name="RID")
	public Long getRid() {
		return rid;
	}
	public void setRid(Long rid) {
		this.rid = rid;
	}
	
	@Column(name="FILE_ID")
	public String getFileID() {
		return fileID;
	}

	public void setFileID(String fileID) {
		this.fileID = fileID;
	}

	@Column(name="IS_ACTIVE")
	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
}