package com.velos.stafa.business.util;

import java.io.InputStream;
import java.sql.Clob;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


/**
 * Class used for having utility methods for the whole application.
 *
 * @author bbabu
 * @version 1.0
 */

public class Utilities { 

	private static String DEF_DATE_FORMAT = "MM/dd/yyyy HH:mm";
	private static String TIMESTAMP_FORMAT ="MM/dd/yyyy HH:mm";
	/**
	 * Responsible for returning the current date
	 *
	 * @return Date
	 * @throws Exception
	 */
	public static Date getCurrentDate() throws Exception {
		try {
			long ts = System.currentTimeMillis();
			Date utilDate = new java.util.Date(ts);
			return utilDate;
		} catch (Exception e) {
			throw e;
		}
	}


	/** Method used to get current timestamp value
	 *
	 * @return Timestamp
	 */

	public static Timestamp getCurrentTimeStamp(){
	       Date d=new Date();
	       return new java.sql.Timestamp(d.getTime());
	   }



	/**
	 * Method used to validate the given date as String.
	 *
	 * @param dateStr -
	 * @param format
	 * @return Date
	 * @throws Exception
	 */
	public static Date getDate(String dateStr, String format) throws Exception {
		if (format == null) {
			format = DEF_DATE_FORMAT;
		}
		try {
			SimpleDateFormat df = new SimpleDateFormat(format);
			Date date = df.parse(dateStr);
			return date;
		} catch (Exception e) {
			throw e;
		}
	}



	/**
	 * Method used to validate the given date as String.
	 *
	 * @param dateStr -
	 * @param format
	 * @return Date
	 * @throws Exception
	 */
	public static Date getDate(String dateStr) throws Exception {
		String format = DEF_DATE_FORMAT;
		return getDate(dateStr, format);
	}


	/**
	 * Responsible for retrieving LeonyFormatted parameter Date
	 *
	 * @param d
	 * @return
	 * @throws Exception
	 */

	public static String getFormattedDate(Date date) throws Exception{
		return getFormattedDate(date, DEF_DATE_FORMAT).toString();
	}

	/**
	 * Method used to get the formatted date for given date in the given format. If format is not
	 * given, the date is rendered in the default format.
	 *
	 * @param date
	 * @param format
	 * @return String
	 * @throws Exception
	 */
	public static String getFormattedDate(Date date, String format) throws Exception {
		String fmt = format;
		try {
			if (fmt == null) {
				fmt = DEF_DATE_FORMAT;
			}
			SimpleDateFormat sdf = new SimpleDateFormat(fmt);
			
			return sdf.format(date);
		} catch (Exception e) {
			//throw new RDSApplicationException("invalid Date:" + date, e);
			throw e;
		}
	}

	 public static Timestamp convertstringtotimestamp(String date,String hrs,String mins) throws Exception{
		   SimpleDateFormat formatter;
			formatter = new SimpleDateFormat(TIMESTAMP_FORMAT);
			/*formatter.setTimeZone(TimeZone.getTimeZone("UTC"));*/
			String hours="";
			String strmins="";
			if(hrs.length()!=0){

				hours=hrs;

			}else{

			hours="00";
			}
			if(mins.length()!=0){
				strmins=mins;
			}else{
				strmins="00";
			}
		String time =hours + ":" + strmins ;
		String strdate = date + " " + time;
		Date date1=formatter.parse(strdate);
	//	String s = formatter.format(date1);
		return new java.sql.Timestamp(date1.getTime());
	}

	 public static byte[] covertClobToByte(Clob clob) throws Exception{
		 if(clob != null){
			 byte[] thedata=new byte[(int)clob.length()];
			 InputStream is=clob.getAsciiStream();
			 is.read(thedata);
			// System.out.println(thedata.toString());
			 return thedata;
		 }else{
			 return null;
		 }
	 }

	 public static String getDateFormatDate(String fromDate){
		 String returnDate = "";
		 	try{
				DateFormat fromFormat=new SimpleDateFormat("MMM dd,yyyy");
			//?BB	fromFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
				Date utilDate=fromFormat.parse(fromDate);
	
				DateFormat toFormat=new SimpleDateFormat("MM/dd/yyyy");
			//?BB	toFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
				returnDate=toFormat.format(utilDate);
	
				//System.out.println(returnDate);
		 	}catch(Exception e){
		 		e.printStackTrace();
		 	}
		 	
		 	return returnDate;
	 }
	 
	 public static Long getDaysBetweenTwoDates(int frmYear,int frmMonth,int frmDay,int toYear,int toMonth,int toDay)throws Exception{
		 //
	        // Creates two calendars instances
	        //
	        Calendar cal1 = Calendar.getInstance();
	        Calendar cal2 = Calendar.getInstance();
	 
	        //
	        // Set the date for both of the calendar instance
	        //
	        
	        System.out.println(frmYear);
	        System.out.println(frmMonth);
	        System.out.println(frmDay);
	        
	        System.out.println("----------------------------------------------------------------------");
	        
	        System.out.println(toYear);
	        System.out.println(toMonth);
	        System.out.println(toDay);
	        
	        
	        cal1.set(frmYear,frmMonth,frmDay);
	        cal2.set(toYear,toMonth,toDay);
	 
	        //
	        // Get the represented date in milliseconds
	        //
	        long milis1 = cal1.getTimeInMillis();
	        long milis2 = cal2.getTimeInMillis();
	 
	        //
	        // Calculate difference in milliseconds
	        //
	        long diff = milis2 - milis1;
	 
	 
	        //
	        // Calculate difference in days
	        //
	        long diffDays = diff / (24 * 60 * 60 * 1000);
	 
	        System.out.println("In milliseconds: " + diff + " milliseconds.");
	        System.out.println("In days: " + diffDays + " days.");
	        return diffDays;
	 }
	 
	 public static int getYearBetweenTwoDates(Date oldDate,Date newDate){
		 try{
			    Calendar c1 = Calendar.getInstance();
		        Calendar c2 = Calendar.getInstance();
		        if (oldDate.compareTo(newDate) < 0) {
		            c1.setTime(oldDate);
		            c2.setTime(newDate);
		        } else {		            
		            return 0;
		        }
		        int year = 0;
		        int month = 0;
		        int days = 0;
		        boolean doneMonth = false;
		        boolean doneYears = false;
		        while (c1.before(c2)) {
		            //log.debug("Still in Loop");
		            if (!doneYears) {
		                c1.add(Calendar.YEAR, 1);
		                year++;
		            }
		            if (c1.after(c2) || doneYears) {
		                if (!doneYears) {
		                    doneYears = true;
		                    year--;
		                    c1.add(Calendar.YEAR, -1);
		                }   
		                if (!doneMonth) {
		                    c1.add(Calendar.MONTH, 1);
		                    month++;
		                }
		                if (c1.after(c2) || doneMonth) {
		                    if (!doneMonth) {
		                        doneMonth = true;
		                        month--;
		                        c1.add(Calendar.MONTH, -1);
		                    }

		                    c1.add(Calendar.DATE, 1);
		                    days++;
		                    if (c1.after(c2)) {
		                        days--;
		                    }
		                    // this will not be executed
		                    if (days == 31 || month==12) {
		                        break;
		                    }
		                }
		            }
		        }		        
		        return year;  
		 }catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	 }
	 
}


