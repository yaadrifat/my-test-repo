package com.velos.stafa.business.domain.usertracking;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
* This class made for logging the user session.
* @author Mohiuddin Ali Ahmed 
* @version 1
*/
@Entity
@Table(name="ER_USERSESSIONLOG")
@SequenceGenerator(name="SEQ_ER_USERSESSIONLOG",sequenceName="SEQ_ER_USERSESSIONLOG")
public class UserSessionLog {

	private Long pkUsl;
	private Long fkUserSession;
	private String uslUrl;
	private Timestamp uslAccessTime;
	private String uslUrlParam;
	private String uslModuleName;
	private Long uslEntityId;
	private String uslEntityType;
	private String uslEntityIdentifier;
	private String uslEntityEventCode;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_ER_USERSESSIONLOG")
	@Column(name="PK_USL")
	public Long getPkUsl() {
		return pkUsl;
	}
	public void setPkUsl(Long pkUsl) {
		this.pkUsl = pkUsl;
	}
	@Column(name="FK_USERSESSION")
	public Long getFkUserSession() {
		return fkUserSession;
	}
	public void setFkUserSession(Long fkUserSession) {
		this.fkUserSession = fkUserSession;
	}
	@Column(name="USL_URL")
	public String getUslUrl() {
		return uslUrl;
	}
	public void setUslUrl(String uslUrl) {
		this.uslUrl = uslUrl;
	}
	@Column(name="USL_ACCESSTIME")
	public Timestamp getUslAccessTime() {
		return uslAccessTime;
	}
	public void setUslAccessTime(Timestamp uslAccessTime) {
		this.uslAccessTime = uslAccessTime;
	}
	@Column(name="USL_URLPARAM")
	public String getUslUrlParam() {
		return uslUrlParam;
	}
	public void setUslUrlParam(String uslUrlParam) {
		this.uslUrlParam = uslUrlParam;
	}
	@Column(name="USL_MODNAME")
	public String getUslModuleName() {
		return uslModuleName;
	}
	public void setUslModuleName(String uslModuleName) {
		this.uslModuleName = uslModuleName;
	}
	@Column(name="USL_ENTITY_ID")
	public Long getUslEntityId() {
		return uslEntityId;
	}
	public void setUslEntityId(Long uslEntityId) {
		this.uslEntityId = uslEntityId;
	}
	@Column(name="USL_ENTITY_TYPE")
	public String getUslEntityType() {
		return uslEntityType;
	}
	public void setUslEntityType(String uslEntityType) {
		this.uslEntityType = uslEntityType;
	}
	@Column(name="USL_ENTITY_IDENTIFIER")
	public String getUslEntityIdentifier() {
		return uslEntityIdentifier;
	}
	public void setUslEntityIdentifier(String uslEntityIdentifier) {
		this.uslEntityIdentifier = uslEntityIdentifier;
	}
	@Column(name="USL_ENTITY_EVENT_CODE")
	public String getUslEntityEventCode() {
		return uslEntityEventCode;
	}
	public void setUslEntityEventCode(String uslEntityEventCode) {
		this.uslEntityEventCode = uslEntityEventCode;
	}
	
	
}
