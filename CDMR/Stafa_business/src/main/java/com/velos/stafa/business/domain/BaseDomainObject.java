/**
 * 
 */
package com.velos.stafa.business.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * @author akajeera
 *
 */
@MappedSuperclass
public class BaseDomainObject implements Serializable{

	public Long createdBy;
	public Date createdOn;
	public Long lastModifiedBy;
	public Date lastModifiedOn;
	public String ipAddress;
	public Integer deletedFlag;
	public Long rid;
	//private Long organizationId;
	
	@Column(updatable = false, name = "CREATOR")
	
	public Long getCreatedBy() {
		return createdBy;
	}
	
	@Column(updatable = false, name = "CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}
	@Column(insertable = false, name = "LAST_MODIFIED_BY")
	public Long getLastModifiedBy() {
		return lastModifiedBy;
	}
	
	@Column(insertable = true, name = "LAST_MODIFIED_DATE")
	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}
	@Column(name = "IP_ADD")
	public String getIpAddress() {
		return ipAddress;
	}
	@Column(name="DELETEDFLAG")
	public Integer getDeletedFlag() {
		return deletedFlag;
	}
	/*@Column(name = "FK_CODELST_ORGID")
	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}*/
	public void setDeletedFlag(Integer deletedFlag) {
		this.deletedFlag = deletedFlag;
	}	
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public void setLastModifiedBy(Long lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
}
