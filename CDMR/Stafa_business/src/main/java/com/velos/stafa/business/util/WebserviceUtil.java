package com.velos.stafa.business.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class WebserviceUtil {
	
	
	/*static {
	    HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier()
	        {
	            public boolean verify(String hostname, SSLSession session)
	            {
	                // ip address of the service URL(like.23.28.244.244)
	                if (hostname.equals("172.16.3.95") || hostname.equals("50.197.161.47"))
	                    return true;
	                return false;
	            }
	        });
	}*/

	public static StringBuffer invokeService(String urlString,StringBuffer inputXml){	
		StringBuffer buffer = new StringBuffer();
		try{
			//java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "true");
			System.out.println("  url " + urlString);
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			System.out.println("request " + inputXml.toString());
			connection.setRequestProperty("Content-Type", "text/xml");
			connection.setDoOutput(true);
			PrintWriter pw = new PrintWriter(connection.getOutputStream());
			pw.write(inputXml.toString());
			pw.println();
			pw.flush();
		//	System.out.println("trace 1");
			connection.connect();
		//	System.out.println("trace 2");
			InputStream wsin = connection.getInputStream();
		//	System.out.println("trace 3");
			BufferedReader in = new BufferedReader(new InputStreamReader(wsin));
			String line = "";
			while ((line = in.readLine()) != null) {
				buffer.append(line);
			}
			in.close();
		//	System.out.println(" Result data::: " + buffer.toString());
		}catch(Exception e){
			e.printStackTrace();
		}
		return buffer;
	}
	
	public static List getUsersByRole(String esecurityURL,String roleName){
		List lstResult = new ArrayList();
		try{
			
			System.out.println("eSecurity URL :::::::::::::  " + esecurityURL);
			StringBuffer xmlIp = new StringBuffer();
			xmlIp.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.aithent.com/\">");
			xmlIp.append("<soapenv:Header/><soapenv:Body> <ser:getRoleUsers>");
			xmlIp.append("<user> <application>Stafa</application>");
			xmlIp.append("<roleCode>"+roleName+"</roleCode>");
			xmlIp.append("</user> </ser:getRoleUsers> </soapenv:Body></soapenv:Envelope>");
			StringBuffer result = WebserviceUtil.invokeService(esecurityURL, xmlIp);
			String[] stringNodes = {"userId","lastName","firstName"};
			lstResult = getArrayToList(result,"user",stringNodes);
		}catch(Exception e){
			System.out.println("Exception in getUsersByRole" + e);
			e.printStackTrace();
		}
		
		return lstResult;
	}


	public static List<String[]> getArrayToList(StringBuffer xmlRecords,String arrayObj,String[] valueTags){
		List<String[]> result = new ArrayList<String[]>();
		try{
		//	System.out.println(" get array to map****************************************");
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlRecords.toString()));

			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName(arrayObj);
			Node keyNode;
			NodeList tmpNodelst;
			String[] strArray;
			//System.out.println( " length " + nodes.getLength());
		   String tmpValue;
			  for (int i = 0; i < nodes.getLength(); i++) {
			      Element element = (Element) nodes.item(i);
			      strArray = new String[valueTags.length];
			      for(int j=0;j<valueTags.length;j++){
			    	  tmpValue ="";
			    	  if(valueTags[j]!=null && !valueTags[j].equals("")){
					      tmpNodelst = element.getElementsByTagName(valueTags[j]);
					      if(tmpNodelst !=null && tmpNodelst.getLength() >0){
						      keyNode = tmpNodelst.item(0);
						      if(keyNode != null){
						    	  tmpValue = keyNode.getTextContent();
						      }
					      }
			    	  }
			    	  strArray[j]= tmpValue;
			      }
				     // System.out.println( " node " + strArray );
				  result.add(strArray);
			  }
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result;
	}

	
	public static Map<String,String> getArrayToMap(StringBuffer xmlRecords,String arrayObj,String keyTag,String valueTag){
		Map<String,String> result = new HashMap<String,String>();
		try{
		//	System.out.println(" get array to map****************************************");
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlRecords.toString()));

			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName(arrayObj);
			Node keyNode;
			//System.out.println( " length " + nodes.getLength());
		   String strKey,strValue;
			  for (int i = 0; i < nodes.getLength(); i++) {
			      Element element = (Element) nodes.item(i);
			    //  System.out.println("element -->" + element.getTextContent() + " / " );
			    //  System.out.println(" node name " + element.getNodeName() + " / value " + element.getTextContent());
			       NodeList childList = element.getChildNodes();
			       strKey = "";
			       strValue="";
			       for(int j=0;j<childList.getLength();j++){
			    	   keyNode = childList.item(j);
			    	  
			    	   if(keyNode.getNodeName().equalsIgnoreCase(keyTag)){
			    		   strKey = keyNode.getTextContent();
			    	   }else if(keyNode.getNodeName().equalsIgnoreCase(valueTag)){
			    		   strValue = keyNode.getTextContent();
			    	   }
			       }
			      //System.out.println( " node " + strKey + " --> " + strValue);
			      result.put(strKey,strValue);
			    }
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result;
	}
	
public static String getXmlValue(StringBuffer xmlRecords,String resultNodeName,String nodeName){
		
		String returnValue="";
		try{
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xmlRecords.toString()));

			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName(resultNodeName);

			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);

				NodeList name = element.getElementsByTagName(nodeName);
				Element line = (Element) name.item(0);
				returnValue = getCharacterDataFromElement(line);
				//System.out.println("Name: " + returnValue);


			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return returnValue;
	}
public static String getCharacterDataFromElement(Element e) {
	if (e != null){
	    Node child = e.getFirstChild();
	    if (child instanceof CharacterData) {
	      CharacterData cd = (CharacterData) child;
	      return cd.getData();
	    }
	}
    return "";
  }
	
}
