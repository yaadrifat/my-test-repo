package com.velos.stafa.business.domain.cdmr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.velos.stafa.business.domain.BaseDomainObject;

/**
 * @summary     FilesLibrary
 * @description Mapping class of CDM_FILESLIB Table
 * @version     1.0
 * @file        FilesLibrary.java
 * @author      Lalit Chattar
 */

@Entity
@Table(name="CDM_FILESLIB")
@javax.persistence.SequenceGenerator(name="fileslib_sqnc", sequenceName="SEQ_CDM_FILESLIB")
public class FilesLibrary extends BaseDomainObject {
	
	private Long pkFilelib;
	private Long fkAccount;
	private String fileId;
	private String fileName;
	private String fileDes;
	private String fileLocation;
	private Integer fileCurrentStatus;
	private String comment;
	private Long rid;
	
	@Id
	@GeneratedValue(generator="fileslib_sqnc",strategy=GenerationType.AUTO)
	@Column(name="PK_CDM_FILESLIB")
	public Long getPkFilelib() {
		return pkFilelib;
	}
	public void setPkFilelib(Long pkFilelib) {
		this.pkFilelib = pkFilelib;
	}
	
	@Column(name="FK_ACCOUNT")
	public Long getFkAccount() {
		return fkAccount;
	}
	public void setFkAccount(Long fkAccount) {
		this.fkAccount = fkAccount;
	}
	
	@Column(name="FILE_ID")
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	
	@Column(name="FILE_NAME")
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	@Column(name="FILE_DESC")
	public String getFileDes() {
		return fileDes;
	}
	public void setFileDes(String fileDes) {
		this.fileDes = fileDes;
	}
	
	@Column(name="FILE_LOCATION")
	public String getFileLocation() {
		return fileLocation;
	}
	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
	@Column(name="FILE_CUR_STATUS")
	public Integer getFileCurrentStatus() {
		return fileCurrentStatus;
	}
	public void setFileCurrentStatus(Integer fileCurrentStatus) {
		this.fileCurrentStatus = fileCurrentStatus;
	}
	
	@Column(name="COMMENTS")
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	@Column(name="RID")
	public Long getRid() {
		return rid;
	}
	public void setRid(Long rid) {
		this.rid = rid;
	}
	

}
