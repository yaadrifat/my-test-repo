package com.velos.stafa.business.domain.cdmr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.Date;

import com.velos.stafa.business.domain.BaseDomainObject1;

@Entity
@Table(name="CDM_NOTIFICATION")
@javax.persistence.SequenceGenerator(name="notification", sequenceName="SEQ_CDM_NOTIFICATION")
public class CdmNotification implements Serializable {
	
	private Long pkCdmNotification;
	private Long pkCdmNotifycnf;
	private Integer userId;
	private String msgTxt;
	private Date sentOn;
	private String sentStatus;
	private Integer isRead;
	private Integer deletedFalg;
	private String loginName;
	private String userEmail;

	
	@Id
	@GeneratedValue(generator="notification",strategy=GenerationType.AUTO)
	@Column(name="PK_CDM_NOTIFICATION")
	public Long getPkCdmNotification() {
		return pkCdmNotification;
	}
	public void setPkCdmNotification(Long pkCdmNotification) {
		this.pkCdmNotification = pkCdmNotification;
	}
	
	@Column(name="FK_CDM_NOTIFYCNF")
	public Long getPkCdmNotifycnf() {
		return pkCdmNotifycnf;
	}
	public void setPkCdmNotifycnf(Long pkCdmNotifycnf) {
		this.pkCdmNotifycnf = pkCdmNotifycnf;
	}
	
	@Column(name="USER_ID")
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	@Column(name="MSG_TEXT")
	public String getMsgTxt() {
		return msgTxt;
	}
	public void setMsgTxt(String msgTxt) {
		this.msgTxt = msgTxt;
	}
	
	@Column(name="SENT_ON")
	public Date getSentOn() {
		return sentOn;
	}
	public void setSentOn(Date sentOn) {
		this.sentOn = sentOn;
	}
	
	@Column(name="SENT_STATUS")
	public String getSentStatus() {
		return sentStatus;
	}
	public void setSentStatus(String sentStatus) {
		this.sentStatus = sentStatus;
	}
	
	@Column(name="IS_READ")
	public Integer getIsRead() {
		return isRead;
	}
	public void setIsRead(Integer isRead) {
		this.isRead = isRead;
	}
	
	@Column(name="DELETEDFLAG")
	public Integer getDeletedFalg() {
		return deletedFalg;
	}
	public void setDeletedFalg(Integer deletedFalg) {
		this.deletedFalg = deletedFalg;
	}
	
	@Column(name="LOGIN_NAME")
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	
	@Column(name="USER_EMAIL")
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	
	
	
	
}

