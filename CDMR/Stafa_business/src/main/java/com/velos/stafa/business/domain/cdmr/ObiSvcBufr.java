package com.velos.stafa.business.domain.cdmr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.Date;

import com.velos.stafa.business.domain.BaseDomainObject1;

@Entity
@Table(name="CDM_OBISVCBFR")
@javax.persistence.SequenceGenerator(name="obisvcbfr", sequenceName="SEQ_CDM_OBISVCBFR")
public class ObiSvcBufr implements Serializable {
	
	private Integer pkObcsvcBfr;
	private String ssId;
	private String ssNameOrg;
	private String serviceName;
	private String ssNameNew;
	private String opCode;
	private String chargeType;
	private String eventLibid;
	@Id
	@GeneratedValue(generator="obisvcbfr",strategy=GenerationType.AUTO)
	@Column(name="PK_CDM_OBISVCBFR")
	public Integer getPkObcsvcBfr() {
		return pkObcsvcBfr;
	}

	public void setPkObcsvcBfr(Integer pkObcsvcBfr) {
		this.pkObcsvcBfr = pkObcsvcBfr;
	}

	@Column(name="SS_ID")
	public String getSsId() {
		return ssId;
	}

	public void setSsId(String ssId) {
		this.ssId = ssId;
	}

	@Column(name="SS_NAME_ORG")
	public String getSsNameOrg() {
		return ssNameOrg;
	}

	public void setSsNameOrg(String ssNameOrg) {
		this.ssNameOrg = ssNameOrg;
	}

	@Column(name="SERVICE_NAME")
	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	@Column(name="SS_NAME_NEW")
	public String getSsNameNew() {
		return ssNameNew;
	}

	public void setSsNameNew(String ssNameNew) {
		this.ssNameNew = ssNameNew;
	}

	@Column(name="OP_CODE")
	public String getOpCode() {
		return opCode;
	}

	public void setOpCode(String opCode) {
		this.opCode = opCode;
	}
	
	@Column(name="CHG_IND")
	public String getChargeType(){
		return this.chargeType;
	}
	
	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}
	@Column(name="EVENT_LIB_ID")
	public String getEventLibid() {
		return eventLibid;
	}

	public void setEventLibid(String eventLibid) {
		this.eventLibid = eventLibid;
	}
	
	
}

