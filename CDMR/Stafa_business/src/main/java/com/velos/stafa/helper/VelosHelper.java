/**
 * 
 */
package com.velos.stafa.helper;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.xwork.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.hql.ast.QuerySyntaxException;

import atg.taglib.json.util.JSONArray;
import com.velos.stafa.util.CDMRUtil;
import atg.taglib.json.util.JSONObject;

import com.opensymphony.xwork2.Result;
import com.velos.stafa.business.domain.CodeList;
import com.velos.stafa.business.util.HibernateUtil;
import com.velos.stafa.business.util.ObjectTransfer;
import com.velos.stafa.business.validation.ValidateEntity;

import com.velos.stafa.controller.CDMRController;

/**
 * @author Babu
 * 
 */
public class VelosHelper {
	public static final Log log = LogFactory.getLog(VelosHelper.class);
	private static CDMRController cdmrController = new CDMRController();

	// Commo method for all type of save

	public static JSONObject saveJSONObjects(JSONObject jsonObject)
			throws Exception {
		log.debug("Methods Start ");
		Session session = null;
		try {

			HashMap domainKeyMap = new HashMap();
			String domainKey = null;
			String strKeyValue = null;
			String domainName;
			JSONObject domainData;
			JSONArray domainDataArray;
			JSONArray domainArray;
			String aliasName;
			String query;
			JSONObject tmpObject;
			JSONObject domain;
			JSONObject queryObject;
			JSONObject queryDetails;
			JSONArray queryArray;
			JSONArray referenceArray;
			JSONObject referenceObject;
			String referenceFieldName, referenceKeyName;
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			if (jsonObject.has("baseDomain")) {
				tmpObject = jsonObject.optJSONObject("baseDomain");
				if (tmpObject == null) {
					domainArray = jsonObject.getJSONArray("baseDomain");
				} else {
					domainArray = new JSONArray();
					domainArray.put(jsonObject.getJSONObject("baseDomain"));
				}
				for (int i = 0; i < domainArray.length(); i++) {
					domain = domainArray.getJSONObject(i);
					System.out.println("domain " + domain);

					// Before Queries
					if (domain.has("queries")) {
						queryObject = domain.getJSONObject("queries");
						if (queryObject.has("beforeQueries")) {
							tmpObject = queryObject
									.optJSONObject("beforeQueries");
							if (tmpObject == null) {
								queryArray = queryObject
										.getJSONArray("beforeQueries");
							} else {
								queryArray = new JSONArray();
								queryArray.put(queryObject
										.getJSONObject("beforeQueries"));
							}

							// beforeQueries ->no reference values
							if (queryArray != null && queryArray.length() > 0) {
								for (int q = 0; q < queryArray.length(); q++) {
									query = queryArray.getString(q);
									executeQueryString(query, session);
								}

							}
						}
					}

					domainName = domain.getString("domain");
					domainKey = domain.getString("domainKey");

					tmpObject = domain.optJSONObject("data");

					if (tmpObject == null) {
						domainDataArray = domain.getJSONArray("data");
					} else {
						domainDataArray = new JSONArray();
						domainDataArray.put(domain.getJSONObject("data"));
					}
					for (int j = 0; j < domainDataArray.length(); j++) {
						domainData = domainDataArray.getJSONObject(j);
						domainData.put("domain", domainName);
						domainData.put("domainKey", domainKey);
						// set user details

						System.out
								.println("domain before save -trace*********-main method---------- "
										+ domainData);
						domainData = ObjectTransfer.setJSONUserDetails(
								jsonObject, domainData);
						/*
						 * //?BB move to top //Before Queries
						 * if(domain.has("queries") ){ queryObject =
						 * domain.getJSONObject("queries");
						 * if(queryObject.has("beforeQueries")){
						 * tmpObject=queryObject.optJSONObject("beforeQueries");
						 * if(tmpObject == null){ queryArray =
						 * queryObject.getJSONArray("beforeQueries"); }else{
						 * queryArray=new JSONArray();
						 * queryArray.put(queryObject
						 * .getJSONObject("beforeQueries")); }
						 * 
						 * // beforeQueries ->no reference values
						 * if(queryArray!=null && queryArray.length()>0 ){
						 * for(int q=0;q<queryArray.length();q++){ query =
						 * queryArray.getString(q); executeQueryString(query,
						 * session); }
						 * 
						 * } } }
						 */
						// domain save
						domainData = saveJSONObject(domainData, session);
						System.out
								.println("domain after save --main method---------- "
										+ domainData);

						if (domain.has("aliasName")) {
							aliasName = domain.getString("aliasName");
							strKeyValue = domainData.getString(domainKey);
							domainKeyMap.put(aliasName + "." + domainKey,
									strKeyValue);
							System.out.println("value====>" + aliasName + "."
									+ domainKey + " / " + strKeyValue);
						}

						// ?BB check
						if (domainData.has("aliasName")) {
							aliasName = domainData.getString("aliasName");
							strKeyValue = domainData.getString(domainKey);
							domainKeyMap.put(aliasName + "." + domainKey,
									strKeyValue);
							System.out.println("value====>" + aliasName + "."
									+ domainKey + " / " + strKeyValue);
						}

						/*
						 * //?bb move to end After queries
						 * if(domain.has("queries") ){ queryObject =
						 * domain.getJSONObject("queries");
						 * if(queryObject.has("afterQueries")){
						 * tmpObject=queryObject.optJSONObject("afterQueries");
						 * if(tmpObject == null){ queryArray =
						 * queryObject.getJSONArray("afterQueries"); }else{
						 * queryArray=new JSONArray();
						 * queryArray.put(queryObject
						 * .getJSONObject("afterQueries")); } //?BB
						 * session.beginTransaction().commit();
						 * if(queryArray!=null && queryArray.length()>0 ){
						 * for(int q=0;q<queryArray.length();q++){ queryDetails
						 * = queryArray.getJSONObject(q); referenceArray=new
						 * JSONArray(); query = queryDetails.getString("query");
						 * if(queryDetails.has("reference")){
						 * tmpObject=queryDetails.optJSONObject("reference");
						 * if(tmpObject == null){ referenceArray =
						 * queryDetails.getJSONArray("reference"); }else{
						 * referenceArray
						 * .put(queryDetails.getJSONObject("reference")); }
						 * System.out.println("After update Query " + query);
						 * for(int ref=0;ref<referenceArray.length();ref++){
						 * referenceObject = referenceArray.getJSONObject(ref);
						 * referenceFieldName=referenceKeyName="";
						 * if(referenceObject.has("referenceFieldName")){
						 * referenceFieldName
						 * =referenceObject.getString("referenceFieldName"); }
						 * if(referenceObject.has("referenceKeyName")){
						 * referenceKeyName
						 * =referenceObject.getString("referenceKeyName"); }
						 * System.out.println(" referenceKeyName " +
						 * referenceKeyName + " / " + referenceFieldName +
						 * " / value " + domainKeyMap.get(referenceKeyName));
						 * System.out.println(referenceFieldName + " / "
						 * +referenceKeyName );
						 * query=query.replaceAll(referenceFieldName,
						 * domainKeyMap.get(referenceKeyName).toString()); } }
						 * System.out.println("After ref replae " + query);
						 * executeQueryString(query, session); } } } } // end of
						 * after query
						 */}

					// After queries
					if (domain.has("queries")) {
						queryObject = domain.getJSONObject("queries");
						if (queryObject.has("afterQueries")) {
							tmpObject = queryObject
									.optJSONObject("afterQueries");
							if (tmpObject == null) {
								queryArray = queryObject
										.getJSONArray("afterQueries");
							} else {
								queryArray = new JSONArray();
								queryArray.put(queryObject
										.getJSONObject("afterQueries"));
							}
							// ?BB session.beginTransaction().commit();
							if (queryArray != null && queryArray.length() > 0) {
								for (int q = 0; q < queryArray.length(); q++) {
									queryDetails = queryArray.getJSONObject(q);
									referenceArray = new JSONArray();
									query = queryDetails.getString("query");
									if (queryDetails.has("reference")) {
										tmpObject = queryDetails
												.optJSONObject("reference");
										if (tmpObject == null) {
											referenceArray = queryDetails
													.getJSONArray("reference");
										} else {
											referenceArray
													.put(queryDetails
															.getJSONObject("reference"));
										}
										System.out
												.println("After update Query "
														+ query);
										for (int ref = 0; ref < referenceArray
												.length(); ref++) {
											referenceObject = referenceArray
													.getJSONObject(ref);
											referenceFieldName = referenceKeyName = "";
											if (referenceObject
													.has("referenceFieldName")) {
												referenceFieldName = referenceObject
														.getString("referenceFieldName");
											}
											if (referenceObject
													.has("referenceKeyName")) {
												referenceKeyName = referenceObject
														.getString("referenceKeyName");
											}
											System.out
													.println(" referenceKeyName "
															+ referenceKeyName
															+ " / "
															+ referenceFieldName
															+ " / value "
															+ domainKeyMap
																	.get(referenceKeyName));
											System.out
													.println(referenceFieldName
															+ " / "
															+ referenceKeyName);
											query = query.replaceAll(
													referenceFieldName,
													domainKeyMap.get(
															referenceKeyName)
															.toString());
										}
									}
									System.out.println("After ref replae "
											+ query);
									executeQueryString(query, session);
								}
							}
						}
					} // end of after query

				}
			}// end of base domain

			// sub domains
			if (jsonObject.has("subDomains")) {
				JSONObject data;
				JSONArray subDomainArray;
				// Class toClass;
				Long keyValue = null;
				Object obj;
				JSONArray dataArray = new JSONArray();

				String tmpString;
				long referenceValue;

				tmpObject = jsonObject.optJSONObject("subDomains");
				if (tmpObject == null) {
					subDomainArray = jsonObject.getJSONArray("subDomains");
				} else {
					subDomainArray = new JSONArray();
					subDomainArray.put(jsonObject.getJSONObject("subDomains"));
				}

				JSONObject subDomain;

				for (int i = 0; i < subDomainArray.length(); i++) {
					referenceKeyName = "";
					referenceFieldName = "";
					referenceValue = 0;
					subDomain = subDomainArray.getJSONObject(i);
					System.out.println(" subdomain " + subDomain.toString());
					// Before Queries
					if (subDomain.has("queries")) {
						queryObject = subDomain.getJSONObject("queries");
						if (queryObject.has("beforeQueries")) {
							tmpObject = queryObject
									.optJSONObject("beforeQueries");
							if (tmpObject == null) {
								queryArray = queryObject
										.getJSONArray("beforeQueries");
							} else {
								queryArray = new JSONArray();
								queryArray.put(queryObject
										.getJSONObject("beforeQueries"));
							}

							// beforeQueries ->no reference values
							if (queryArray != null && queryArray.length() > 0) {
								for (int q = 0; q < queryArray.length(); q++) {
									query = queryArray.getString(q);
									executeQueryString(query, session);
								}
							}
						}
					}
					// domain save

					domainName = subDomain.getString("domain");
					if (subDomain.has("domainKey")) {
						domainKey = subDomain.getString("domainKey");
					}
					referenceArray = new JSONArray();
					if (subDomain.has("reference")) {
						tmpObject = subDomain.optJSONObject("reference");
						if (tmpObject == null) {
							referenceArray = subDomain
									.getJSONArray("reference");
						} else {
							referenceArray.put(subDomain
									.getJSONObject("reference"));
						}
					}

					if (subDomain.has("data")) {
						tmpObject = subDomain.optJSONObject("data");
						if (tmpObject == null) {
							dataArray = subDomain.getJSONArray("data");
						} else {
							dataArray = new JSONArray();
							dataArray.put(subDomain.getJSONObject("data"));
						}
						// dataArray = subDomain.getJSONArray("data");
						for (int j = 0; j < dataArray.length(); j++) {
							data = dataArray.getJSONObject(j);
							strKeyValue = null;
							keyValue = null;
							if (data.has(domainKey)) {
								strKeyValue = data.getString(domainKey);
							}
							if (strKeyValue != null && !strKeyValue.equals("")) {
								keyValue = Long.parseLong(strKeyValue);
							}

							for (int ref = 0; ref < referenceArray.length(); ref++) {
								referenceObject = referenceArray
										.getJSONObject(ref);

								if (referenceObject.has("referenceFieldName")) {
									referenceFieldName = referenceObject
											.getString("referenceFieldName");
								}
								if (referenceObject.has("referenceKey")) {
									referenceKeyName = referenceObject
											.getString("referenceKey");
								}
								System.out.println(" referenceKeyName "
										+ referenceKeyName + " / "
										+ referenceFieldName + " / value "
										+ domainKeyMap.get(referenceKeyName));
								tmpString = (String) domainKeyMap
										.get(referenceKeyName);
								if (tmpString != null) {
									referenceValue = Long.valueOf(tmpString);
								}
								data.remove(referenceFieldName);
								data.put(referenceFieldName, referenceValue);
							}

							if (data.has("referenceFieldName")) {
								referenceFieldName = data
										.getString("referenceFieldName");
								if (data.has("referenceKey")) {
									referenceKeyName = data
											.getString("referenceKey");
								}
								tmpString = (String) domainKeyMap
										.get(referenceKeyName);
								System.out.println(" referenceKeyName "
										+ referenceKeyName + " / "
										+ referenceFieldName + " / value "
										+ tmpString);

								data.remove(referenceFieldName);
								data.put(referenceFieldName, tmpString);
							}
							data.put("domain", domainName);
							data.put("domainKey", domainKey);
							data = ObjectTransfer.setJSONUserDetails(
									jsonObject, data);
							System.out.println("sub object before save  "
									+ data);
							data = saveJSONObject(data, session);
							// user details obj =
							// ObjectTransfer.tranferJSONObjecttoObject(jsonObject,
							// obj);
							System.out.println(" sub obj after save value====>"
									+ data);
							if (subDomain.has("aliasName")) {
								aliasName = subDomain.getString("aliasName");
								strKeyValue = data.getString(domainKey);
								domainKeyMap.put(aliasName + "." + domainKey,
										strKeyValue);
								System.out
										.println("value====>" + aliasName + "."
												+ domainKey + " / "
												+ strKeyValue);
							}
						}
					} // end of sub domain data

					// subdomain after query
					// After queries
					if (subDomain.has("queries")) {
						queryObject = subDomain.getJSONObject("queries");
						if (queryObject.has("afterQueries")) {
							tmpObject = queryObject
									.optJSONObject("afterQueries");
							if (tmpObject == null) {
								queryArray = queryObject
										.getJSONArray("afterQueries");
							} else {
								queryArray = new JSONArray();
								queryArray.put(queryObject
										.getJSONObject("afterQueries"));
							}
							if (queryArray != null && queryArray.length() > 0) {
								for (int q = 0; q < queryArray.length(); q++) {
									queryDetails = queryArray.getJSONObject(q);
									query = queryDetails.getString("query");
									System.out.println("After update Query "
											+ query);

									if (queryDetails.has("reference")) {
										tmpObject = queryDetails
												.optJSONObject("reference");
										if (tmpObject == null) {
											referenceArray = queryDetails
													.getJSONArray("reference");
										} else {
											referenceArray = new JSONArray();
											referenceArray
													.put(queryDetails
															.getJSONObject("reference"));
										}

										for (int ref = 0; ref < referenceArray
												.length(); ref++) {
											referenceObject = referenceArray
													.getJSONObject(ref);

											if (referenceObject
													.has("referenceFieldName")) {
												referenceFieldName = referenceObject
														.getString("referenceFieldName");
											}
											if (referenceObject
													.has("referenceKeyName")) {
												referenceKeyName = referenceObject
														.getString("referenceKeyName");
											}
											System.out
													.println(" referenceKeyName= "
															+ referenceKeyName
															+ " /referenceFieldName= "
															+ referenceFieldName
															+ " / value "
															+ domainKeyMap
																	.get(referenceKeyName));
											System.out
													.println(referenceFieldName
															+ " / "
															+ referenceKeyName);
											query = query.replaceAll(
													referenceFieldName,
													domainKeyMap.get(
															referenceKeyName)
															.toString());
										}
									}
									System.out.println("After ref replae "
											+ query);
									executeQueryString(query, session);
								}
							}
						}
					} // end of after query

				}

			}

			// queries //?BB to do before query after query

			session.getTransaction().commit();

		} catch (Exception e) {
			log.debug("Exception ::" + e);
			log.error("Error :: ", e);
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}

		return jsonObject;
	}

	public static JSONObject executeSQLQueries(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		System.out.println(" Save method in velos helper *******");

		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();

			// queries
			String query;
			if (jsonObject.has("queries")) {
				JSONArray queryArray = jsonObject.getJSONArray("queries");
				if (queryArray != null && queryArray.length() > 0) {
					for (int i = 0; i < queryArray.length(); i++) {
						query = queryArray.getString(i);
						System.out.println(" query in helper " + query);
						Query sqlQuery = session.createSQLQuery(query);
						int count = sqlQuery.executeUpdate();
						System.out.println(" query execute count " + count);
					}
				}
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}

		return jsonObject;
	}

	// *****************old methods

	public static JSONObject saveJSONData(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		System.out
				.println(" Save method in velos helper ********************************");
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			if (jsonObject.has("domains")) {
				JSONArray domains = jsonObject.getJSONArray("domains");
				JSONObject domain, data;
				String domainName, domainKey = "", strKeyValue;
				Long keyValue;
				JSONArray dataArray;
				Class toClass;
				Object obj;
				for (int i = 0; i < domains.length(); i++) {
					domain = domains.getJSONObject(i);
					System.out.println(" subdomain " + domain.toString());
					domainName = domain.getString("domain");

					if (domain.has("domainKey")) {
						domainKey = domain.getString("domainKey");
						// strKeyValue = subDomain.getString(domainKey);
					}
					if (domain.has("data")) {
						// check is it array of data or not
						JSONObject category = domain.optJSONObject("data");
						System.out.println(" category " + category);
						if (category == null) {
							dataArray = domain.getJSONArray("data");
						} else {
							dataArray = new JSONArray();
							dataArray.put(domain.getJSONObject("data"));
						}
						for (int j = 0; j < dataArray.length(); j++) {
							data = dataArray.getJSONObject(j);
							strKeyValue = null;
							keyValue = null;
							if (data.has(domainKey)) {
								strKeyValue = data.getString(domainKey);
							}
							if (strKeyValue != null && !strKeyValue.equals("")) {
								keyValue = Long.parseLong(strKeyValue);
							}
							System.out.println("Domain " + domainName
									+ " / domainKey " + domainKey + " key "
									+ keyValue);
							toClass = Class.forName(domainName);
							obj = toClass.newInstance();
							// ?BB need to set user details
							data = ObjectTransfer.setJSONUserDetails(
									jsonObject, data);
							if (keyValue != null && keyValue > 0) {
								obj = getObject(domainName, keyValue,
										domainKey, session);
							}

							obj = ObjectTransfer.tranferJSONObjecttoObject(
									data, obj);
							// ?BB obj =
							// ObjectTransfer.tranferJSONObjecttoObject(jsonObject,
							// obj);
							saveObject(obj, keyValue, session);

						}
					}
				}
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}

		}
		return jsonObject;
	}

	public static int findCount(String objectName, String property, String value)
			throws Exception {
		log.debug("VelosHelper : findCount Method Start ");
		String criteria = property + " = " + value;
		List<Object> lstResult = getObjectList(objectName, criteria);
		log.debug("VelosHelper : findCount Method End ");
		if (lstResult != null && lstResult.size() > 0) {
			return lstResult.size();
		} else {
			return 0;
		}

	}

	// ?BB not in use remove
	/*public static JSONObject saveCryoJSON(JSONObject jsonObject, Session session)
			throws Exception {
		System.out.println(" inside save cryo json");
		boolean sessionFlag = false;
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
				sessionFlag = true;
			}
			if (jsonObject.has("domain")) {

				String domainKey = null, strKeyValue = null;
				long keyValue = 0;
				if (jsonObject.has("domainKey")) {
					domainKey = jsonObject.getString("domainKey");
				}
				if (domainKey != null && jsonObject.has("domainKey")) {
					strKeyValue = jsonObject.getString(domainKey);
					keyValue = Long.parseLong(strKeyValue);
				} else {
					jsonObject.put(domainKey, 0);
				}
				// System.out.println("jsonObject " + jsonObject);
				System.out
						.println("Domain cryp data ----=====================---->  domainKey "
								+ domainKey + " key " + strKeyValue);
				CryoPrep obj = new CryoPrep();

				// ? BB TODO change to multiple
				obj = (CryoPrep) ObjectTransfer.tranferJSONObjecttoObject(
						jsonObject, obj);
				System.out.println(" before save key value " + keyValue);
				// set blob data
				Clob clob1 = Hibernate.createClob(jsonObject
						.getString("cryoCurve1"));
				obj.setCurve1(clob1);
				System.out.println("test  == "
						+ jsonObject.getString("cryoCurve1"));
				Clob clob2 = Hibernate.createClob(jsonObject
						.getString("cryoCurve2"));
				obj.setCurve2(clob2);
				Clob clob3 = Hibernate.createClob(jsonObject
						.getString("cryoCurve3"));
				obj.setCurve3(clob3);
				System.out.println("length " + clob3.length());
				saveObject(obj, keyValue, session);
				if (keyValue == 0) {
					strKeyValue = ObjectTransfer.getObjectIdValue(obj,
							domainKey);
					System.out.println(" STR key value ===================>"
							+ strKeyValue);
					jsonObject.remove(domainKey);
					jsonObject.put(domainKey, strKeyValue);
				}
				// System.out.println("domain after save -------------- " +
				// jsonObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sessionFlag) {
				// ?BB session.getTransaction().commit();

				if (session != null && session.isOpen()) {
					session.close();
				}
			}
		}
		return jsonObject;
	}*/

	public static JSONObject saveJSONObject(JSONObject jsonObject,
			Session session) throws Exception {
		boolean sessionFlag = false;
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();
				sessionFlag = true;
			}
			if (jsonObject.has("domain")) {
				String domainName = jsonObject.getString("domain");
				String domainKey = null, strKeyValue = null;
				long keyValue = 0;
				if (jsonObject.has("domainKey")) {
					domainKey = jsonObject.getString("domainKey");
				}
				if (domainKey != null && jsonObject.has(domainKey)) {
					strKeyValue = jsonObject.getString(domainKey);
					keyValue = Long.parseLong(strKeyValue);
				} else {
					jsonObject.put(domainKey, 0);
				}
				// System.out.println("jsonObject " + jsonObject);
				System.out.println("Domain " + domainName + " / domainKey "
						+ domainKey + " key " + strKeyValue);
				Class toClass = Class.forName(domainName);
				Object obj = toClass.newInstance();
				// ?BB added for update new values only
				if (keyValue > 0) {
					obj = getObject(domainName, keyValue, domainKey, session);
				}
				// ? BB TODO change to multiple

				obj = ObjectTransfer.tranferJSONObjecttoObject(jsonObject, obj);
				System.out.println(" before save key value " + keyValue);

				// ?BB test will return long object and obj will return
				// corresponding domain object
				// Object test = saveObject(obj,keyValue,session);
				// System.out.println(
				// " TEst for save object return ------------" +
				// test.getClass().toString() + " /============ " +
				// obj.getClass().toString());
				saveObject(obj, keyValue, session);

				if (keyValue == 0) {
					strKeyValue = ObjectTransfer.getObjectIdValue(obj,
							domainKey);
					System.out.println(" STR key value ===================>"
							+ strKeyValue);
					jsonObject.remove(domainKey);
					jsonObject.put(domainKey, strKeyValue);
				}
				// System.out.println("domain after save -------------- " +
				// jsonObject);
			}
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			if (sessionFlag) {
				session.getTransaction().commit();

				if (session != null && session.isOpen()) {

					session.close();
				}
			}
		}
		return jsonObject;
	}

	public static JSONObject saveReferenceSubObjects(JSONObject jsonObject)
			throws Exception {
		log.debug("VelosHelper saveReferenceSubObjects Methods Start ");
		Session session = null;
		try {

			HashMap domainKeyMap = new HashMap();
			String domainKey = null;
			String strKeyValue = null;
			String domainName;
			JSONObject domainData;
			String aliasName;
			String query;
			if (jsonObject.has("baseDomain")) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				session.beginTransaction();

				JSONObject domain = jsonObject.getJSONObject("baseDomain");
				System.out.println("domain " + domain);
				domainName = domain.getString("domain");
				domainKey = domain.getString("domainKey");

				domainData = domain.getJSONObject("data");
				domainData.put("domain", domainName);
				domainData.put("domainKey", domainKey);
				// set user details

				System.out
						.println("domain before save -trace*********-main method---------- "
								+ domainData);
				System.out.println("domainData.has(hasCryoData) "
						+ domainData.has("hasCryoData"));
				if (domainData.has("hasCryoData") || domain.has("hasCryoData")) {
					System.out.println(" Inside if");
					//domainData = saveCryoJSON(domainData, session);
				} else {
					domainData = ObjectTransfer.setJSONUserDetails(jsonObject,
							domainData);
					domainData = saveJSONObject(domainData, session);
				}
				System.out.println("domain after save --main method---------- "
						+ domainData);
				if (domain.has("aliasName")) {
					aliasName = domain.getString("aliasName");

					strKeyValue = domainData.getString(domainKey);

					domainKeyMap.put(aliasName + "." + domainKey, strKeyValue);
					System.out.println("value====>" + aliasName + "."
							+ domainKey + " / " + strKeyValue);

				}

				// sub domains
				if (jsonObject.has("subDomains")) {
					JSONObject data;
					Class toClass;
					Long keyValue = null;
					Object obj;
					JSONArray dataArray = new JSONArray();
					String referenceKeyName;
					String referenceFieldName;
					String tmpString;
					long referenceValue;
					JSONArray subDomainArray = jsonObject
							.getJSONArray("subDomains");
					if (subDomainArray != null && subDomainArray.length() > 0) {
						JSONObject subDomain;

						for (int i = 0; i < subDomainArray.length(); i++) {
							referenceKeyName = "";
							referenceFieldName = "";
							referenceValue = 0;
							subDomain = subDomainArray.getJSONObject(i);
							System.out.println(" subdomain "
									+ subDomain.toString());
							if (subDomain.has("queries")) {
								JSONArray queryArray = subDomain
										.getJSONArray("queries");
								if (queryArray != null
										&& queryArray.length() > 0) {
									for (int j = 0; j < queryArray.length(); j++) {
										query = queryArray.getString(j);
										executeQueryString(query, session);
									}
								}
							}

							domainName = subDomain.getString("domain");

							if (subDomain.has("domainKey")) {
								domainKey = subDomain.getString("domainKey");
								// strKeyValue = subDomain.getString(domainKey);
							}
							if (subDomain.has("referenceFieldName")) {
								referenceFieldName = subDomain
										.getString("referenceFieldName");
							}
							if (subDomain.has("referenceKey")) {
								referenceKeyName = subDomain
										.getString("referenceKey");

							}
							System.out.println(" referenceKeyName "
									+ referenceKeyName + " / "
									+ referenceFieldName + " / value "
									+ domainKeyMap.get(referenceKeyName));

							if (subDomain.has("data")) {
								dataArray = subDomain.getJSONArray("data");
								for (int j = 0; j < dataArray.length(); j++) {
									data = dataArray.getJSONObject(j);
									strKeyValue = null;
									keyValue = null;
									if (data.has(domainKey)) {
										strKeyValue = data.getString(domainKey);
									}
									if (strKeyValue != null
											&& !strKeyValue.equals("")) {
										keyValue = Long.parseLong(strKeyValue);
									}
									if (!referenceFieldName.equals("")
											&& !(referenceKeyName.equals(""))) {
										System.out
												.println("reference value -------> "
														+ domainKeyMap
																.get(referenceKeyName));
										tmpString = (String) domainKeyMap
												.get(referenceKeyName);
										if (tmpString != null) {
											referenceValue = Long
													.valueOf(tmpString);
										}
										data.remove(referenceFieldName);
										data.put(referenceFieldName,
												referenceValue);
									}
									data.put("domain", domainName);
									data.put("domainKey", domainKey);
									data = ObjectTransfer.setJSONUserDetails(
											jsonObject, data);
									System.out
											.println("sub object before save  "
													+ data);
									data = saveJSONObject(data, session);
									// user details obj =
									// ObjectTransfer.tranferJSONObjecttoObject(jsonObject,
									// obj);
									System.out
											.println(" sub obj after save value====>"
													+ data);
									if (subDomain.has("aliasName")) {
										aliasName = subDomain
												.getString("aliasName");
										strKeyValue = data.getString(domainKey);
										domainKeyMap.put(aliasName + "."
												+ domainKey, strKeyValue);
										System.out.println("value====>"
												+ aliasName + "." + domainKey
												+ " / " + strKeyValue);
									}
								}
							}
						}
					}
				}
				// queries //?BB to do before query after query

				if (jsonObject.has("queries")) {
					JSONArray queryArray = jsonObject.getJSONArray("queries");
					if (queryArray != null && queryArray.length() > 0) {
						for (int i = 0; i < queryArray.length(); i++) {
							query = queryArray.getString(i);
							executeQueryString(query, session);
						}
					}
				}
				session.getTransaction().commit();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return null;
	}

	public static JSONObject saveJSONDataObject(JSONObject jsonObject)
			throws Exception {
		Session session = null;
		System.out.println(" helper --------------");

		System.out.println(jsonObject);
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();

			// queries
			String query;
			if (jsonObject.has("queries")) {
				JSONArray queryArray = jsonObject.getJSONArray("queries");
				if (queryArray != null && queryArray.length() > 0) {
					for (int i = 0; i < queryArray.length(); i++) {
						query = queryArray.getString(i);
						executeQueryString(query, session);
					}
				}
			}
			// sub domains
			if (jsonObject.has("subDomains")) {
				JSONArray subDomainArray = jsonObject
						.getJSONArray("subDomains");
				if (subDomainArray != null && subDomainArray.length() > 0) {
					JSONObject subDomain;
					JSONObject data;
					String domainName;
					String domainKey = null;
					String strKeyValue = null;
					Class toClass;
					Long keyValue = null;
					Object obj;
					JSONArray dataArray = new JSONArray();
					for (int i = 0; i < subDomainArray.length(); i++) {
						subDomain = subDomainArray.getJSONObject(i);
						System.out
								.println(" subdomain " + subDomain.toString());
						domainName = subDomain.getString("domain");

						if (subDomain.has("domainKey")) {
							domainKey = subDomain.getString("domainKey");
							// strKeyValue = subDomain.getString(domainKey);
						}
						if (subDomain.has("data")) {
							dataArray = subDomain.getJSONArray("data");
							for (int j = 0; j < dataArray.length(); j++) {
								data = dataArray.getJSONObject(j);
								strKeyValue = null;
								keyValue = null;
								if (data.has(domainKey)) {
									strKeyValue = data.getString(domainKey);
								}
								if (strKeyValue != null
										&& !strKeyValue.equals("")) {
									keyValue = Long.parseLong(strKeyValue);
								}
								System.out.println("Domain " + domainName
										+ " / domainKey " + domainKey + " key "
										+ keyValue);
								toClass = Class.forName(domainName);
								obj = toClass.newInstance();

								obj = ObjectTransfer.tranferJSONObjecttoObject(
										data, obj);
								obj = ObjectTransfer.tranferJSONObjecttoObject(
										jsonObject, obj);
								saveObject(obj, keyValue, session);

							}
						}

					}
				}
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}

		return jsonObject;
	}

	// /--------------------old methods

	public static List fetchNextTask(String entityId, String entityType,
			Long userId) throws Exception {

		List tasklst = new ArrayList();
		CallableStatement cSt = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();

			cSt = session.connection().prepareCall(
					"{call sp_workflow(?,?,?,?)}");
			cSt.setString(1, entityId);
			cSt.setString(2, entityType);
			if (userId != null) {
				cSt.setInt(3, userId.intValue());
			} else {
				cSt.setNull(3, Types.NULL);
			}
			cSt.setString(4, " ");
			cSt.registerOutParameter(4, Types.CHAR);
			cSt.execute();
			String strAction = cSt.getString(4);
			System.out.println(" strAction flush");
			System.out.println(" strAction " + strAction);
			String[] strArray = new String[1];
			strArray[0] = strAction;
			tasklst.add(strArray);
			System.out.println("End of helper");
			session.flush();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return tasklst;
	}

	public static JSONArray saveObject(JSONArray jsonArray) throws Exception {
		System.out.println("VelosHelper : saveObject(jsonArray) Method Start");
		JSONObject jsonObject = new JSONObject();
		try {
			if (jsonArray != null && jsonArray.length() > 0) {
				for (int i = 0; i < jsonArray.length(); i++) {
					jsonObject = new JSONObject();
					jsonObject = jsonArray.getJSONObject(i);
					jsonObject = saveObject(jsonObject);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("VelosHelper : saveObject(jsonArray) Method End");
		return jsonArray;
	}

	public static JSONObject saveObject(JSONObject jsonObject) throws Exception {
		System.out.println("VelosHelper saveObject Methods Start ");
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			System.out.println("****************************" + jsonObject);
			jsonObject = saveObject(jsonObject, session);

			if (jsonObject.has("domainKey") && jsonObject.has("subjson")) {
				String domainKey = jsonObject.getString("domainKey");
				String domainKeyValue = jsonObject.getString(domainKey);
				System.out
						.println("***************************************************************************************************");

				System.out
						.println("---------------------------------domainKey : "
								+ domainKey);

				System.out
						.println("---------------------------------domainKeyValue : "
								+ domainKeyValue);

				JSONArray subJSONArray = new JSONArray();
				subJSONArray = jsonObject.getJSONArray("subjson");
				JSONObject subJSON = new JSONObject();
				for (Iterator subIter = subJSONArray.iterator(); subIter
						.hasNext();) {
					subJSON = new JSONObject();
					subJSON = (JSONObject) subIter.next();
					subJSON.put(domainKey, domainKeyValue);
					System.out
							.println("---------------------------------subJSON : "
									+ subJSON);
					System.out
							.println("***************************************************************************************************");
					saveObject(subJSON, session);
				}
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		System.out.println("VelosHelper saveObject Methods End ");
		return jsonObject;
	}

	public static JSONObject saveObject(JSONObject jsonObject, Session session)
			throws Exception {
		System.out
				.println("VelosHelper saveObject with Session Methods Start ");
		String domainName = jsonObject.getString("Domain");
		String domainKey = "";
		String keyValueStr = "";
		if (jsonObject.has("domainKey")) {
			domainKey = jsonObject.getString("domainKey");
			keyValueStr = jsonObject.getString(domainKey);
		}
		Long keyValue = null;

		if (keyValueStr != null && !keyValueStr.equals("")) {
			keyValue = Long.parseLong(keyValueStr);
		}
		System.out.println("Domain " + domainName + " key " + keyValue);
		Class toClass = Class.forName(domainName);
		Object obj = toClass.newInstance();

		// Business Logic Check
		if (jsonObject.has("domainKey")) {
			jsonObject = (JSONObject) ValidateEntity.validateEntity(domainName,
					jsonObject);
			// jsonObject = ValidateEntity.validateEntity(jsonObject,
			// VelosConstants.CODELST_DOMAIN);

		}
		// End
		// System.out.println("After Validdate");
		if (keyValue != null && keyValue != 0) {
			obj = getObject(domainName, keyValue, domainKey, session);
		}
		String domainKeyValue = "";
		if (jsonObject.has("domainKey")) {
			obj = ObjectTransfer.tranferJSONObjecttoObject(jsonObject, obj);

			System.out.println("prototocol : " + keyValue);
			if (obj != null) {
				// CodeList codeList = (CodeList)obj;
				// System.out.println("=======================>"+codeList.getDeletedFlag());

				if (jsonObject.has("maintDeletedFlag")) {
					// codeList.setDeletedFlag(Integer.parseInt(jsonObject.get("maintDeletedFlag").toString()));
				}

				// System.out.println("=======================>"+codeList.getDeletedFlag());
				// obj = codeList;
			}
			saveObject(obj, keyValue, session);
			domainKeyValue = ObjectTransfer.getObjectIdValue(obj, domainKey);
			jsonObject.remove(domainKey);
			jsonObject.put(domainKey, domainKeyValue);
			// Roles for After Save
			jsonObject = (JSONObject) ValidateEntity.validateAfterSave(
					domainName, jsonObject);
		}
		// Query execution
		if (jsonObject.has("query")) {
			String query = jsonObject.getString("query");
			String[] queryBuffer = query.split(";");
			for (int i = 0; i < queryBuffer.length; i++) {
				if (queryBuffer[i] != null && !queryBuffer[i].equals("")) {
					executeQueryString(queryBuffer[i], session);
				}
			}

		}
		System.out.println("VelosHelper saveObject with Session Methods End ");
		return jsonObject;
	}

	public static int executeQueryString(String queryBuffer, Session session)
			throws Exception {
		int updateflag = 0;
		try {
			// System.out.println(" execute query");
			Query query = session.createQuery(queryBuffer);
			// System.out.println("Query : " + query);
			updateflag = query.executeUpdate();
		} catch (QuerySyntaxException qse) {
			// System.out.println(" inside  query exception");
			try {
				Query query = session.createSQLQuery(queryBuffer);
				// System.out.println("SQL Query : " + query);
				updateflag = query.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			// System.out.println(" main catch " + e);
			e.printStackTrace();
		}
		return updateflag;
	}

	public static Object saveObject(Object object, Long pkId, Session session)
			throws Exception {
		System.out.println("VelosHelper saveObject Start");
		try {
			if (pkId != null && pkId != 0) {
				session.merge(object);

			} else {
				object = session.save(object);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("VelosHelper saveObject End");
		return object;
	}

	public static List getObjectList(JSONArray jsonArray) throws Exception {
		List objlst = new ArrayList();
		try {
			List returnlst = new ArrayList();
			for (int i = 0; i < jsonArray.length(); i++) {
				if (!jsonArray.isNull(i)) {
					returnlst = getObjectList(jsonArray.getJSONObject(i));
					objlst.add(returnlst);
				} else {
					objlst.add(null);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return objlst;
	}

	public static List getObjectList(JSONObject jsonObject) throws Exception {
		System.out.println("Inside getObject List ");
		List objlst = new ArrayList();
		try {
			String type = null;

			if (jsonObject.has("type")) {
				type = jsonObject.getString("type");
			}
			if (type != null && type.equals("SQL")) {
				String sql = jsonObject.getString("Query");
				objlst = getDataFromSQLQuery(sql, null);
			} else if (type != null && type.equals("HSQL")) {
				String sql = jsonObject.getString("Query");
				objlst = getDataFromHQLQuery(sql, null);
			} else {
				String objectName = jsonObject.getString("Domain");
				Class toClass = Class.forName(objectName);
				Object obj = toClass.newInstance();

				String criteria = jsonObject.getString("criteria");
				objlst = getObjectList(objectName, criteria);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objlst;
	}

	public static Object getObject(String objectName, Long id,
			String fieldName, Session session) throws Exception {
		log.debug("VelosHelper : getObject Method Start ");
		Object returnObject = new Object();
		try {

			String criteria = fieldName + " = " + id;
			List<Object> objList = getObjectList(objectName, criteria, session);
			System.out.println("-------------------");
			if (objList != null && objList.size() > 0) {
				System.out
						.println("(Object)objList.get(0) : " + objList.get(0));
				returnObject = (Object) objList.get(0);
			}

		} catch (Exception e) {
			log.error("Exception in VelosHelper : getObject() :: " + e);
			e.printStackTrace();
		}
		log.debug("VelosHelper : getObject Method End ");
		return returnObject;
	}

	public static List<Object> getObjectList(String objectName, String criteria)
			throws Exception {
		log.debug("VelosHelper : getObjectList Method ");
		return getObjectList(objectName, criteria, null);
	}

	public static List<Object> getObjectList(String objectName,
			String criteria, Session session) throws Exception {
		log.debug("VelosHelper : getObjectList Method Start ");
		System.out.println("objectName : " + objectName);
		System.out.println("criteria : " + criteria);
		boolean sessionFlag = false;
		List<Object> returnList = null;
		 try{
		 // check session
		 if(session== null){
		 // create new session
		 session = HibernateUtil.getSessionFactory().getCurrentSession();
		 sessionFlag = true;
		 session.beginTransaction();
		 }
		 String query = " from " + objectName + " where deletedFlag = 0 ";
		 if(criteria!=null && !criteria.trim().equals("")){
		 query += " and " + criteria;
		 }
		 System.out.println("------ trace query " + query);
		 returnList = (List<Object>)session.createQuery(query).list();
					
		 //System.out.println("returnList : "+returnList);
		 if(returnList!=null){
		 System.out.println("returnList.size : "+returnList.size());
		 }
		 }catch(Exception e){
		 log.error("Exception in VelosHelper : getObjectList() :: " + e);
		 e.printStackTrace();
					
		 }finally{
		 if(sessionFlag){
		 session.close();
		 }
		 }
		 log.debug("VelosHelper : getObjectList Method End ");
		return returnList;
	}

	public static Object getNewInstance(String domainName) {
		Object obj = null;
		try {
			Class toClass = Class.forName(domainName);
			obj = toClass.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;
	}

	public static List getDataFromSQLQuery(String mainQuery, Session session)
			throws Exception {
		//System.out.println("VelosHelper.getDataFromSQLQuery() start");
		List returnList = new ArrayList();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			System.out.println("mainQuery :: " + mainQuery);
			if (mainQuery != null && mainQuery.length() > 0) {
				SQLQuery query = session.createSQLQuery(mainQuery);
				query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				returnList = query.list();
			}

		} catch (Exception e) {
			System.out
					.println("Exception in VelosHelper.getDataFromSQLQuery() :: "
							+ e);
			
			 CDMRUtil.errorLogUtil(e, mainQuery, cdmrController, "VelosHelper::getDataFromSQLQuery", null);
			e.printStackTrace();
		} finally {
			if (sessionFlag) {
				session.close();
			}
		}
		//System.out.println("VelosHelper.getDataFromSQLQuery() end");
		return returnList;
	}

	public static List getDataFromSQLQueryWithoutEntityMap(String mainQuery, Session session)
			throws Exception {
		//System.out.println("VelosHelper.getDataFromSQLQuery() start");
		List returnList = new ArrayList();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			System.out.println("mainQuery :: " + mainQuery);
			if (mainQuery != null && mainQuery.length() > 0) {
				SQLQuery query = session.createSQLQuery(mainQuery);
				//query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				returnList = query.list();
			}

		} catch (Exception e) {
			System.out
					.println("Exception in VelosHelper.getDataFromSQLQuery() :: "
							+ e);
			
			 CDMRUtil.errorLogUtil(e, mainQuery, cdmrController, "VelosHelper::getDataFromSQLQuery", null);
			e.printStackTrace();
		} finally {
			if (sessionFlag) {
				session.close();
			}
		}
		//System.out.println("VelosHelper.getDataFromSQLQuery() end");
		return returnList;
	}
	

	public static List getDataFromHQLQuery(String mainQuery, Session session)
			throws Exception {
		System.out.println("VelosHelper.getDataFromHQLQuery() start");
		List returnList = new ArrayList();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			if (mainQuery != null && mainQuery.length() > 0) {
				Query query = session.createQuery(mainQuery);
				returnList = query.list();
			}

		} catch (Exception e) {
			System.out
					.println("Exception in VelosHelper.getDataFromHQLQuery() :: "
							+ e);
			
			 CDMRUtil.errorLogUtil(e, mainQuery, cdmrController, "VelosHelper::getDataFromHQLQuery", null);
			e.printStackTrace();
		} finally {
			if (sessionFlag) {
				session.close();
			}
		}
		System.out.println("VelosHelper.getDataFromHQLQuery() end");
		return returnList;
	}

/*	public static Documents addDocument(Documents velosDocument, Session session)
			throws Exception {
		log.debug("VelosStudyHelper : addDocument Method Start ");
		boolean sessionFlag = false;
		try {
			// check session
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}

			if (velosDocument.getPkDocumentId() != null) {
				session.update(velosDocument);
			} else {
				session.save(velosDocument);
			}
			if (sessionFlag) {
				// close session, if session created in this method
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			log.error("Exception in VelosStudyHelper : addDocument() :: " + e);
			e.printStackTrace();
		}
		log.debug("VelosStudyHelper : addDocument Method End ");
		return velosDocument;
	}*/

	public static JSONObject saveDataFiledValues(JSONObject jsonObject)
			throws Exception {
		JSONObject jsonNewObj = new JSONObject();
		Session session = null;
		try {
			JSONArray jsonArray = jsonObject.getJSONArray("panelData");
			System.out.println("recs.length() ::-------------------- "
					+ jsonArray.length() + "jsonObject : " + jsonObject);
			Iterator it = jsonArray.iterator();
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			while (it.hasNext()) {
				System.out.println("value*************----------************");
				CodeList codeLst = new CodeList();
				jsonNewObj = (JSONObject) it.next();
				codeLst = (CodeList) ObjectTransfer.tranferJSONObjecttoObject(
						jsonNewObj, codeLst);
				// for User Details
				codeLst = (CodeList) ObjectTransfer.tranferJSONObjecttoObject(
						jsonObject, codeLst);
				Long pkCodelst = null;
				if (jsonNewObj.has("pkCodelst")
						&& jsonNewObj.get("pkCodelst") != null
						&& !jsonNewObj.get("pkCodelst").equals("")) {
					System.out.println("value*************************"
							+ jsonNewObj.get("pkCodelst"));
					codeLst.setPkCodelst(Long.valueOf(jsonNewObj.get(
							"pkCodelst").toString()));
					pkCodelst = codeLst.getPkCodelst();
				}
				if (jsonNewObj.has("defaultValue1")
						&& jsonNewObj.get("defaultValue1") != null
						&& !jsonNewObj.get("defaultValue1").equals("")) {
					System.out.println("value*************************"
							+ jsonNewObj.get("defaultValue1"));
					String defualt = jsonNewObj.get("defaultValue1").toString();
					if (defualt.substring(0, 2).equalsIgnoreCase("Yes")) {
						codeLst.setDefaultValue(Long.valueOf("1"));
					} else {
						codeLst.setDefaultValue(Long.valueOf("0"));
					}
				}
				if (jsonNewObj.has("isHide1")
						&& jsonNewObj.get("isHide1") != null
						&& !jsonNewObj.get("isHide1").equals("")) {
					System.out.println("value*************************"
							+ jsonNewObj.get("isHide1"));
					String isHide = jsonNewObj.get("isHide1").toString();
					char c = isHide.charAt(0);
					System.out
							.println("--------------------------------------------------"
									+ c);
					if (isHide.substring(0, 2).equalsIgnoreCase("Yes")) {
						codeLst.setIsHide(c);
					} else {
						codeLst.setIsHide(c);
					}
				}
				/*
				 * codeLst.setAliasId(jsonNewObj.get("aliasId").toString());
				 * codeLst
				 * .setAliasSource(jsonNewObj.get("aliasSource").toString());
				 * codeLst.setAliasType(jsonNewObj.get("aliasType").toString());
				 * codeLst
				 * .setFkProduct(Long.valueOf(jsonObject.get("pkSpecimenProduct"
				 * ).toString()));
				 * codeLst.setFkCodelstAliasType(Long.valueOf(jsonObject
				 * .get("fkCodelstAlisType").toString()));
				 */

				System.out.println("Before save ");
				VelosHelper.saveObject(codeLst, pkCodelst, session);
			}
			if (jsonObject.has("parentData")) {
				jsonNewObj = jsonObject.getJSONObject("parentData");
				String query = "update  ER_CODELST  set CODELST_FIELDTYPE ='"
						+ jsonNewObj.getString("fieldType")
						+ "' where CODELST_MODULE='"
						+ jsonNewObj.getString("module")
						+ "' and CODELST_SUBTYP ='"
						+ jsonNewObj.getString("subtype") + "' ";
				executeQueryString(query, session);
			}
			if (session.isOpen()) {
				session.getTransaction().commit();
			}

			// jsonObject = getAlias(jsonObject,null);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}

	// Save Multiple Base domain and Refer corresponding domain pKey to
	// corresponding sub domain as fKey
	// When the base domain no need to save at the time no need to send
	// domainKey,aliasName,rererenceFieldName and referenceKey
	public static JSONObject saveMultipleBaseAndSubObjects(JSONObject jsonObject)
			throws Exception {
		log.debug("VelosHelper saveMultipleBaseAndSubObjects Methods Start ");
		Session session = null;
		try {

			HashMap domainKeyMap = new HashMap();
			String domainKey = null;
			String strKeyValue = null;
			String domainName;
			JSONObject domainData;
			String aliasName;
			String query;
			JSONObject tmpObject = new JSONObject();
			JSONArray domainArray = new JSONArray();
			if (jsonObject.has("baseDomain")) {
				tmpObject = jsonObject.optJSONObject("baseDomain");
				if (tmpObject == null) {
					domainArray = jsonObject.getJSONArray("baseDomain");
				} else {
					domainArray = new JSONArray();
					domainArray.put(jsonObject.getJSONObject("baseDomain"));
				}
				for (int m = 0; m < domainArray.length(); m++) {

					// if(jsonObject.has("baseDomain")){
					session = HibernateUtil.getSessionFactory()
							.getCurrentSession();
					session.beginTransaction();

					// JSONObject domain =
					// jsonObject.getJSONObject("baseDomain");
					JSONObject domain = (JSONObject) (domainArray.get(m));
					System.out.println("domain " + domain);
					domainName = domain.getString("domain");
					if (domain.has("domainKey")) {
						domainKey = domain.getString("domainKey");
					}

					domainData = domain.getJSONObject("data");
					domainData.put("domain", domainName);
					domainData.put("domainKey", domainKey);
					// set user details

					System.out
							.println("domain before save -trace*********-main method---------- "
									+ domainData);
					System.out.println("domainData.has(hasCryoData) "
							+ domainData.has("hasCryoData"));
					if (domainData.has("hasCryoData")
							|| domain.has("hasCryoData")) {
						System.out.println(" Inside if");
						//domainData = saveCryoJSON(domainData, session);
					} else {
						if (domainData.has(domainKey)) {
							domainData = ObjectTransfer.setJSONUserDetails(
									jsonObject, domainData);
							domainData = saveJSONObject(domainData, session);
							System.out.println("After saved the domainData : "
									+ domainData);
						}
					}
					System.out
							.println("domain after save --main method---------- "
									+ domainData);
					if (domain.has("aliasName")) {
						aliasName = domain.getString("aliasName");
						strKeyValue = domainData.getString(domainKey);
						domainKeyMap.put(aliasName + "." + domainKey,
								strKeyValue);
						System.out
								.println("value================================>"
										+ aliasName
										+ "."
										+ domainKey
										+ " / "
										+ strKeyValue);
						System.out.println("******************************"
								+ domainKeyMap.get(aliasName + "." + domainKey)
								+ "*******************************");
					}

					System.out
							.println("******************************Base Domain Saved*******************************");
					System.out.println("******************************"
							+ domainKeyMap + "*******************************");
					// sub domains
					if (jsonObject.has("subDomains")) {
						JSONObject data;
						Class toClass;
						Long keyValue = null;
						Object obj;
						JSONArray dataArray = new JSONArray();
						String referenceKeyName;
						String referenceFieldName;
						String tmpString;
						long referenceValue;
						JSONArray subDomainArray = jsonObject
								.getJSONArray("subDomains");
						if (subDomainArray != null
								&& subDomainArray.length() > 0) {
							JSONObject subDomain;
							// for(int i=0;i<subDomainArray.length();i++){
							// for(int k=m;i<=m;i++){
							referenceKeyName = "";
							referenceFieldName = "";
							referenceValue = 0;
							subDomain = subDomainArray.getJSONObject(m);
							System.out.println(" subdomain "
									+ subDomain.toString());
							if (subDomain.has("queries")) {
								JSONArray queryArray = subDomain
										.getJSONArray("queries");
								if (queryArray != null
										&& queryArray.length() > 0) {
									for (int j = 0; j < queryArray.length(); j++) {
										query = queryArray.getString(j);
										executeQueryString(query, session);
									}
								}
							}

							domainName = subDomain.getString("domain");

							if (subDomain.has("domainKey")) {
								domainKey = subDomain.getString("domainKey");
								// strKeyValue = subDomain.getString(domainKey);
							}
							if (subDomain.has("referenceFieldName")) {
								referenceFieldName = subDomain
										.getString("referenceFieldName");
							}
							if (subDomain.has("referenceKey")) {
								referenceKeyName = subDomain
										.getString("referenceKey");

							}
							System.out.println(" referenceKeyName "
									+ referenceKeyName + " / "
									+ referenceFieldName + " / value "
									+ domainKeyMap.get(referenceKeyName));

							if (subDomain.has("data")) {
								dataArray = subDomain.getJSONArray("data");
								for (int j = 0; j < dataArray.length(); j++) {
									data = dataArray.getJSONObject(j);
									strKeyValue = null;
									keyValue = null;
									if (data.has(domainKey)) {
										strKeyValue = data.getString(domainKey);
									}
									if (strKeyValue != null
											&& !strKeyValue.equals("")) {
										keyValue = Long.parseLong(strKeyValue);
									}
									if (!referenceFieldName.equals("")
											&& !(referenceKeyName.equals(""))) {
										System.out
												.println("reference value -------> "
														+ domainKeyMap
																.get(referenceKeyName));
										tmpString = (String) domainKeyMap
												.get(referenceKeyName);
										if (tmpString != null) {
											referenceValue = Long
													.valueOf(tmpString);
										}
										data.remove(referenceFieldName);
										data.put(referenceFieldName,
												referenceValue);
									}
									data.put("domain", domainName);
									data.put("domainKey", domainKey);
									System.out
											.println("************jsonObject******************"
													+ jsonObject
													+ "*******************************");
									System.out
											.println("************data******************"
													+ data
													+ "*******************************");
									data = ObjectTransfer.setJSONUserDetails(
											jsonObject, data);
									System.out
											.println("sub object before save  "
													+ data);
									data = saveJSONObject(data, session);
									// user details obj =
									// ObjectTransfer.tranferJSONObjecttoObject(jsonObject,
									// obj);
									System.out
											.println(" sub obj after save value====>"
													+ data);
									if (subDomain.has("aliasName")) {
										aliasName = subDomain
												.getString("aliasName");
										strKeyValue = data.getString(domainKey);
										domainKeyMap.put(aliasName + "."
												+ domainKey, strKeyValue);
										System.out.println("value====>"
												+ aliasName + "." + domainKey
												+ " / " + strKeyValue);
									}
								}
							}
							// }
						}
					}
					// queries //?BB to do before query after query

					if (jsonObject.has("queries")) {
						JSONArray queryArray = jsonObject
								.getJSONArray("queries");
						if (queryArray != null && queryArray.length() > 0) {
							for (int i = 0; i < queryArray.length(); i++) {
								query = queryArray.getString(i);
								executeQueryString(query, session);
							}
						}
					}
					session.getTransaction().commit();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return null;
	}

	public static Object getObject(String objectName, Long id) throws Exception {
		Session session = null;
		boolean sessionFlag = false;
		Object object = null;
		object = Class.forName(objectName).newInstance();
		try {
			if (session == null) {
				// create new session
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			object = session.get(object.getClass(), id);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
		return object;
	}

	/*// Added By Anuj
	// Insurance save objects methods Start
	public static void saveInsEmploymentInfo(
			EmploymentInfoDetailsObject insEmploymentDetails, Session session,
			long Id) {
		log.debug("VelosStudyHelper: save InsEmployment details method start");
		boolean sessionFlag = false;
		try {

			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}

			EmploymentInfo employmentInfoSaveObject = (EmploymentInfo) ObjectTransfer
					.transferObjects(insEmploymentDetails, new EmploymentInfo());
			if (Id == 0) {
				session.save(employmentInfoSaveObject);
			} else {
				session.update(employmentInfoSaveObject);
			}
			if (sessionFlag) {
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			log
					.error("Exception in VelosStudyHelper : saveInsEmploymentInfo()::"
							+ e);
			e.printStackTrace();
		}
		log.debug("VelosStudyHelper :save InsEmployment  details method  end ");

	}

	public static void saveCompleteInsuranceInfo(
			RecipientInsViewDetailsObject recipientInsViewDetails,
			EmploymentInfoDetailsObject employmentDetails, Session session) {
		log
				.debug("VelosStudyHelper:saveCompleteInsuranceInfo details method start");
		boolean sessionFlag = false;
		try {

			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			RecipientInsurance recipientAllSaveObjects = (RecipientInsurance) ObjectTransfer
					.transferObjects(recipientInsViewDetails,
							new RecipientInsurance());
			if (recipientInsViewDetails.getEntityId() != null
					&& recipientInsViewDetails.getPkPersonIns() == null) {
				long flagSaveEmplInfo = 0;
				session.save(recipientAllSaveObjects);
				employmentDetails.setRefPersonIns(recipientAllSaveObjects
						.getPkPersonIns());
				saveInsEmploymentInfo(employmentDetails, session,
						flagSaveEmplInfo);
			} else if (recipientInsViewDetails.getPkPersonIns() != null
					&& recipientInsViewDetails.getPkPersonIns() != 0) {
				session.update(recipientAllSaveObjects);
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			log
					.error("Exception in VelosStudyHelper : saveCompleteInsuranceInfo()::"
							+ e);
			e.printStackTrace();
		}
		log
				.debug("VelosStudyHelper :saveCompleteInsuranceInfo  details method  end ");
	}

	public static void savePersonFundCaseManagement(
			PersonFundCaseManagementObjects personFundCaseMgmtDetails,
			Session session) {
		log
				.debug("VelosStudyHelper:savePersonFundCaseManagement details method start");
		boolean sessionFlag = false;
		try {

			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}

			PersonFundCaseManagement perCaseManagement = (PersonFundCaseManagement) ObjectTransfer
					.transferObjects(personFundCaseMgmtDetails,
							new PersonFundCaseManagement());

			Criteria criteria = session.createCriteria(
					PersonFundCaseManagement.class).setResultTransformer(
					Criteria.DISTINCT_ROOT_ENTITY).add(
					Restrictions.eq("entityId", personFundCaseMgmtDetails
							.getEntityId())).add(
					Restrictions.eq("deletedFlag", 0));
			if (perCaseManagement.getEntityId() != null
					&& perCaseManagement.getEntityId() > 0
					&& criteria.list() != null && criteria.list().size() > 0) {
				session.update(perCaseManagement);
			} else if (perCaseManagement.getEntityId() != null
					&& perCaseManagement.getEntityId() > 0) {
				session.save(perCaseManagement);
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			log
					.error("Exception in VelosStudyHelper : savePersonFundCaseManagement()::"
							+ e);
			e.printStackTrace();
		}
		log
				.debug("VelosStudyHelper :savePersonFundCaseManagement  details method  end ");

	}

	// Insurance Save Objects Methods End
*/
	public static List<Object> getPojoList(String domainName,
			Object pojoObject, String criteria, String type) {
		return getPojoList(domainName, pojoObject, criteria, type, null);
	}

	public static List<Object> getPojoList(String domainName,
			Object pojoObject, String criteria, String type, Session session) {
		System.out.println("VelosHelper.getDataFromSQLQuery() start");
		List<Object> returnList = new ArrayList();
		List<Object> returnPojoList = new ArrayList();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			String hql = "";
			if (!StringUtils.isEmpty(domainName)) {
				hql += !StringUtils.isEmpty(type)
						&& type.equalsIgnoreCase("hql") ? " From " + domainName
						: " Select * from ";
			}
			if (!StringUtils.isEmpty(criteria)) {
				hql += " where " + criteria;
			}
			if (!StringUtils.isEmpty(type) && type.equalsIgnoreCase("hql")) {
				Query query = session.createQuery(hql);
				returnList = query.list();
			} else {
				Query query = session.createSQLQuery(hql);
				returnList = query.list();
			}
			if (returnList != null && pojoObject != null
					&& !StringUtils.isEmpty(type)
					&& type.equalsIgnoreCase("hql")) {
				returnPojoList = ObjectTransfer.transferObjectsInsideList(
						returnList, null, pojoObject);
			} else {
				returnPojoList = returnList;
			}
		} catch (Exception e) {
			System.out
					.println("Exception in VelosHelper.getDataFromSQLQuery() :: "
							+ e);
			e.printStackTrace();
		} finally {
			if (sessionFlag) {
				session.close();
			}
		}
		System.out.println("VelosHelper.getDataFromSQLQuery() end");
		return returnPojoList;
	}

	public static void saveDomainFromPojoList(Object domainObject,
			String keyId, List savePojoList) {
		saveDomainFromPojoList(domainObject, keyId, savePojoList, null);
	}

	public static void saveDomainFromPojoList(Object domainObject,
			String keyId, List savePojoList, Session session) {
		boolean sessionFlag = false;
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			List list = ObjectTransfer.transferObjectsInsideList(savePojoList,
					null, domainObject);
			if (list != null && list.size() > 0) {
				for (Object o : list) {
					Object keyValue = ObjectTransfer.getObjectIdValue(o, keyId);
					if (keyValue == null) {
						saveObject(o, 1l, null);
					} else {
						saveObject(o, 0l, null);
					}
				}
			}
		} catch (Exception e) {
			System.out
					.println("Exception in VelosHelper.getDataFromSQLQuery() :: "
							+ e);
			e.printStackTrace();
		} finally {
			if (sessionFlag) {
				session.getTransaction().commit();
				session.close();
			}
		}
	}

	// ///////////////////////////////////////////////////////

	public static List getDataFromSQLQueryCDMR(String mainQuery, Session session)
			throws Exception {
		//System.out.println("VelosHelper.getDataFromSQLQuery() start");
		List returnList = new ArrayList();
		boolean sessionFlag = false;
		try {
			if (session == null) {
				session = HibernateUtil.getSessionFactory().getCurrentSession();
				sessionFlag = true;
				session.beginTransaction();
			}
			//System.out.println("mainQuery :: " + mainQuery);
			if (mainQuery != null && mainQuery.length() > 0) {
				SQLQuery query = session.createSQLQuery(mainQuery);
				query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				returnList = query.list();
			}

		} catch (Exception e) {
			System.out
					.println("Exception in VelosHelper.getDataFromSQLQuery() :: "
							+ e);
			e.printStackTrace();
		} finally {
			if (sessionFlag) {
				session.close();
			}
		}
		//System.out.println("VelosHelper.getDataFromSQLQuery() end");
		return returnList;
	}

}
