package com.velos.stafa.business.domain.cdmr;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;

import com.velos.stafa.business.domain.BaseDomainObject;

/**
 * @summary     StagingMain
 * @description Mapping class of CDM_STAGINGMAIN
 * @version     1.0
 * @file        StagingMain.java
 * @author      Lalit Chattar
 */

@Entity
@Table(name="CDM_STAGINGMAIN")
@javax.persistence.SequenceGenerator(name="staging_sqnc", sequenceName="SEQ_CDM_STAGINGMAIN")
public class StagingMain extends BaseDomainObject{
	
	
	private Long pkStagingMain;
	private Long fkAccount;
	private Long stagingID;
	private Integer stagingType;
	private Long fkFilelib;
	private String fileName;
	private Long processedBy;
	private Date processedDate;
	private Long reviewedBy;
	private Date reviewedDate;
	private Long approvedBy;
	private Date approvedDate;
	private Long submittedBy;
	private Date submittedDate;
	private Integer fileCurrentStatus;
	private String comment;
	private Integer fkMapVersion;
	private String stagingMapUsed;
	private Integer isSubmitted;
	private Long rid;
	
	@Id
	@GeneratedValue(generator="staging_sqnc",strategy=GenerationType.AUTO)
	@Column(name="PK_CDM_STAGINGMAIN")
	public Long getPkStagingMain() {
		return pkStagingMain;
	}
	public void setPkStagingMain(Long pkStagingMain) {
		this.pkStagingMain = pkStagingMain;
	}
	
	@Column(name="FK_ACCOUNT")
	public Long getFkAccount() {
		return fkAccount;
	}
	public void setFkAccount(Long fkAccount) {
		this.fkAccount = fkAccount;
	}
	
	@Column(name="STAGING_ID")
	public Long getStagingID() {
		return stagingID;
	}
	public void setStagingID(Long stagingID) {
		this.stagingID = stagingID;
	}
	
	@Column(name="STAGING_TYPE")
	public Integer getStagingType() {
		return stagingType;
	}
	public void setStagingType(Integer stagingType) {
		this.stagingType = stagingType;
	}
	
	@Column(name="FK_CDM_FILESLIB")
	public Long getFkFilelib() {
		return fkFilelib;
	}
	public void setFkFilelib(Long fkFilelib) {
		this.fkFilelib = fkFilelib;
	}
	
	@Column(name="FILE_NAME")
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	@Column(name="PROCESSED_BY")
	public Long getProcessedBy() {
		return processedBy;
	}
	public void setProcessedBy(Long processedBy) {
		this.processedBy = processedBy;
	}
	
	@Column(name="PROCESSED_DATE")
	public Date getProcessedDate() {
		return processedDate;
	}
	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}
	
	@Column(name="REVIEWED_BY")
	public Long getReviewedBy() {
		return reviewedBy;
	}
	public void setReviewedBy(Long reviewedBy) {
		this.reviewedBy = reviewedBy;
	}
	
	@Column(name="REVEIWED_DATE")
	public Date getReviewedDate() {
		return reviewedDate;
	}
	public void setReviewedDate(Date reviewedDate) {
		this.reviewedDate = reviewedDate;
	}
	
	@Column(name="APPROVED_BY")
	public Long getApprovedBy() {
		return approvedBy;
	}
	public void setApprovedBy(Long approvedBy) {
		this.approvedBy = approvedBy;
	}
	
	@Column(name="APPROVED_DATE")
	public Date getApprovedDate() {
		return approvedDate;
	}
	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}
	
	@Column(name="SUBMITTED_BY")
	public Long getSubmittedBy() {
		return submittedBy;
	}
	public void setSubmittedBy(Long submittedBy) {
		this.submittedBy = submittedBy;
	}
	
	@Column(name="SUBMITTED_DATE")
	public Date getSubmittedDate() {
		return submittedDate;
	}
	public void setSubmittedDate(Date submittedDate) {
		this.submittedDate = submittedDate;
	}
	
	@Column(name="FILE_CUR_STATUS")
	public Integer getFileCurrentStatus() {
		return fileCurrentStatus;
	}
	public void setFileCurrentStatus(Integer fileCurrentStatus) {
		this.fileCurrentStatus = fileCurrentStatus;
	}
	
	@Column(name="COMMENTS")
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	@Column(name="FK_CDM_MAPSVERSION")
	public Integer getFkMapVersion() {
		return fkMapVersion;
	}
	public void setFkMapVersion(Integer fkMapVersion) {
		this.fkMapVersion = fkMapVersion;
	}
	
	@Column(name="STAGINGMAPS_USED")
	public String getStagingMapUsed() {
		return stagingMapUsed;
	}
	public void setStagingMapUsed(String stagingMapUsed) {
		this.stagingMapUsed = stagingMapUsed;
	}
	
	@Column(name="IS_SUBMITTED")
	public Integer getIsSubmitted() {
		return isSubmitted;
	}
	public void setIsSubmitted(Integer isSubmitted) {
		this.isSubmitted = isSubmitted;
	}
	
	@Column(name="RID")
	public Long getRid() {
		return rid;
	}
	public void setRid(Long rid) {
		this.rid = rid;
	}
	
}