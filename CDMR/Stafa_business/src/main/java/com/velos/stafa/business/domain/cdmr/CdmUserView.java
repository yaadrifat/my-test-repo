package com.velos.stafa.business.domain.cdmr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CDM_USER_VIEW")
@javax.persistence.SequenceGenerator(name="cdmuserview_sqnc", sequenceName="SEQ_CDM_USER_VIEW")
public class CdmUserView {
	private Long pkCdmUserView;
	private Long fkCdmModCol;
	private Long userID;
	private Integer colSeq;
	private Integer dispStatus;
	@Id
	@GeneratedValue(generator="cdmuserview_sqnc",strategy=GenerationType.AUTO)
	@Column(name="PK_CDM_USER_VIEW")
	public Long getPkCdmUserView() {
		return pkCdmUserView;
	}
	public void setPkCdmUserView(Long pkCdmUserView) {
		this.pkCdmUserView = pkCdmUserView;
	}
	@Column(name="FK_CDM_MOD_COLS")
	public Long getFkCdmModCol() {
		return fkCdmModCol;
	}
	public void setFkCdmModCol(Long fkCdmModCol) {
		this.fkCdmModCol = fkCdmModCol;
	}
	@Column(name="USER_ID")
	public Long getUserID() {
		return userID;
	}
	public void setUserID(Long userID) {
		this.userID = userID;
	}
	@Column(name="COL_SEQ")
	public Integer getColSeq() {
		return colSeq;
	}
	public void setColSeq(Integer colSeq) {
		this.colSeq = colSeq;
	}
	@Column(name="DISP_STATUS")
	public Integer getDispStatus() {
		return dispStatus;
	}
	public void setDispStatus(Integer dispStatus) {
		this.dispStatus = dispStatus;
	}
	

}
