package com.velos.stafa.business.domain.cdmr;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @summary     ErrorLog
 * @description Log Error in Table
 * @version     1.0
 * @file        FilesLibrary.java
 * @author      Lalit Chattar
 */

@Entity
@Table(name="CDM_ERRORLOG")
@javax.persistence.SequenceGenerator(name="errorlog_sqnc", sequenceName="SEQ_CDM_ERRORLOG")
public class ErrorLog implements Serializable {
	
	private Long pkCdmErrorLog;
	private String errorArea;
	private String errorName;
	private String errorDesc;
	private Date errorRepOn;
	private Integer deletedFalg;
	private Integer temIndc;
	private String fileName;
	
	@Id
	@GeneratedValue(generator="errorlog_sqnc",strategy=GenerationType.AUTO)
	@Column(name="PK_CDM_ERRORLOG")
	public Long getPkCdmErrorLog() {
		return pkCdmErrorLog;
	}
	public void setPkCdmErrorLog(Long pkCdmErrorLog) {
		this.pkCdmErrorLog = pkCdmErrorLog;
	}
	
	@Column(name="ERR_AREA")
	public String getErrorArea() {
		return errorArea;
	}
	public void setErrorArea(String errorArea) {
		this.errorArea = errorArea;
	}
	
	@Column(name="ERR_NAME")
	public String getErrorName() {
		return errorName;
	}
	public void setErrorName(String errorName) {
		this.errorName = errorName;
	}
	
	@Column(name="ERR_DESC")
	public String getErrorDesc() {
		return errorDesc;
	}
	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}
	
	@Column(name="ERR_REP_ON")
	public Date getErrorRepOn() {
		return errorRepOn;
	}
	public void setErrorRepOn(Date errorRepOn) {
		this.errorRepOn = errorRepOn;
	}
	
	@Column(name="DELETEDFLAG")
	public Integer getDeletedFalg() {
		return deletedFalg;
	}
	public void setDeletedFalg(Integer deletedFalg) {
		this.deletedFalg = deletedFalg;
	}
	
	@Column(name="TMP_IND")
	  public Integer getTemIndc() {
			return temIndc;
	  }
		public void setTemIndc(Integer temIndc) {
			this.temIndc = temIndc;
		}
		@Column(name="FILE_NAME")
		public String getFileName() {
			return fileName;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}

}
