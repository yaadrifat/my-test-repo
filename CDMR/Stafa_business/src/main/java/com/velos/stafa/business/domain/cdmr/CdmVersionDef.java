package com.velos.stafa.business.domain.cdmr;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.velos.stafa.business.domain.BaseDomainObject;


@Entity
@Table(name="CDM_VERSIONDEF")
@javax.persistence.SequenceGenerator(name="versiondef", sequenceName="SEQ_CDM_VERSIONDEF")
public class CdmVersionDef extends BaseDomainObject{
	
	private Long pkCdmVersionDef;
	private Long fkAccount;
	private Integer versionId;
	private String versionName;
	private String versionNo;
	private String versionDesc;
	private Integer versionStatus;
	private Date creationDate;
	private Integer isCurrent;
	private Integer isClosed;
	private Long rid;
	private String file_id;
		
	@Id
	@GeneratedValue(generator="versiondef",strategy=GenerationType.AUTO)
	@Column(name="PK_CDM_VERSIONDEF")
	public Long getPkCdmVersionDef() {
		return pkCdmVersionDef;
	}
	public void setPkCdmVersionDef(Long pkCdmVersionDef) {
		this.pkCdmVersionDef = pkCdmVersionDef;
	}
	
	@Column(name="FK_ACCOUNT")
	public Long getFkAccount() {
		return fkAccount;
	}
	public void setFkAccount(Long fkAccount) {
		this.fkAccount = fkAccount;
	}
	
	@Column(name="VERSION_ID")
	public Integer getVersionId() {
		return versionId;
	}
	public void setVersionId(Integer versionId) {
		this.versionId = versionId;
	}
	
	@Column(name="VERSION_NAME")
	public String getVersionName() {
		return versionName;
	}
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
	
	@Column(name="VERSION_NO")
	public String getVersionNo() {
		return versionNo;
	}
	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}
	
	@Column(name="VERSION_DESC")
	public String getVersionDesc() {
		return versionDesc;
	}
	public void setVersionDesc(String versionDesc) {
		this.versionDesc = versionDesc;
	}
	
	@Column(name="VERSION_STATUS")
	public Integer getVersionStatus() {
		return versionStatus;
	}
	public void setVersionStatus(Integer versionStatus) {
		this.versionStatus = versionStatus;
	}
	
	@Column(name="CREATION_DATE")
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@Column(name="IS_CURRENT")
	public Integer getIsCurrent() {
		return isCurrent;
	}
	public void setIsCurrent(Integer isCurrent) {
		this.isCurrent = isCurrent;
	}
	
	@Column(name="IS_CLOSED")
	public Integer getIsClosed() {
		return isClosed;
	}
	public void setIsClosed(Integer isClosed) {
		this.isClosed = isClosed;
	}
	
	@Column(name="RID")
	public Long getRid() {
		return rid;
	}
	public void setRid(Long rid) {
		this.rid = rid;
	}
	
	@Column(name="FILE_ID")

	public String getFile_id() {
		return file_id;
	}
	public void setFile_id(String fileId) {
		file_id = fileId;
	}
}