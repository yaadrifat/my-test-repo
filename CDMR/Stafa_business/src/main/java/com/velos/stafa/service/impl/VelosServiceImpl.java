package com.velos.stafa.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import atg.taglib.json.util.JSONArray;
import atg.taglib.json.util.JSONObject;

import com.velos.stafa.helper.VelosHelper;
/*import com.velos.stafa.pojo.EmploymentInfoDetailsObject;
import com.velos.stafa.pojo.PersonFundCaseManagementObjects;
import com.velos.stafa.pojo.RecipientInsViewDetailsObject;*/
import com.velos.stafa.service.VelosService;
import com.velos.stafa.helper.CDMRHelper;

public class VelosServiceImpl implements VelosService{
	public static VelosService velosService;

	private VelosServiceImpl(){
		
	} 
	public static VelosService getService() throws Exception{
		try{
			if(velosService == null){
				velosService =(VelosService)new VelosServiceImpl(); 
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return velosService;
	}
	public JSONObject executeSQLQueries(JSONObject jsonObject)throws Exception{
		return VelosHelper.executeSQLQueries(jsonObject);
	}
	public JSONObject saveJSONObjects (JSONObject jsonObject)throws Exception{
		return VelosHelper.saveJSONObjects(jsonObject);
	}
	public JSONObject saveJSONObject (JSONObject jsonObject)throws Exception{
		return VelosHelper.saveJSONObject(jsonObject,null);
	}	
	public JSONObject saveJSONData (JSONObject jsonObject)throws Exception{
		return VelosHelper.saveJSONData(jsonObject);
	}
	
	public JSONObject getObjectListByCriteria (JSONObject jsonObject)throws Exception{
		//return VelosHelper.getObjectListByCriteria(jsonObject);
		return jsonObject;
	} 
	
	
	public JSONObject saveMultipleBaseAndSubObjects(JSONObject jsonObject) throws Exception{
		return VelosHelper.saveMultipleBaseAndSubObjects(jsonObject);
	}
	public JSONObject saveReferenceSubObjects(JSONObject jsonObject) throws Exception{
		return VelosHelper.saveReferenceSubObjects(jsonObject);
	}
	public int findCount(String objectName,String property,String value)throws Exception{
		return VelosHelper.findCount(objectName, property, value);
	}
	
	public JSONObject saveJSONDataObject(JSONObject jsonObject) throws Exception{
		return VelosHelper.saveJSONDataObject(jsonObject);
	}
	public List fetchNextTask(String entityId, String entityType, Long userId)throws Exception {
		return VelosHelper.fetchNextTask(entityId, entityType, userId);
	}
	
	
	public JSONObject saveObject(JSONObject jsonObject) throws Exception{
		return VelosHelper.saveObject(jsonObject);
	}
	
	public JSONArray saveObject(JSONArray jsonArray) throws Exception{
		return VelosHelper.saveObject(jsonArray);
	}
	
	public List getObjectList(JSONObject jsonObject)throws Exception{
		return VelosHelper.getObjectList(jsonObject);
	}
	
	public JSONObject saveDataFiledValues(JSONObject jsonObject)throws Exception{
		return VelosHelper.saveDataFiledValues(jsonObject);
	}
	public List getObjectList(JSONArray jsonArray)throws Exception{
		return VelosHelper.getObjectList(jsonArray);
	}

	
	public Object getObject(String objectName, Long id) throws Exception {
		// TODO Auto-generated method stub
		return VelosHelper.getObject(objectName, id);
	}
	public List getDataFromSQLQuery(String mainQuery) throws Exception {
		return VelosHelper.getDataFromSQLQuery(mainQuery,null);
	}
	/*@Override
	public void saveInsEmploymentInfo(
			EmploymentInfoDetailsObject insEmploymentDetails) throws Exception {
		 VelosHelper.saveInsEmploymentInfo(insEmploymentDetails,null,(Long) null);
	}
	@Override
	public void saveCompleteInsuranceInfo(
			RecipientInsViewDetailsObject recipientInsViewDetails,
			EmploymentInfoDetailsObject employmentDetails) throws Exception {
		 VelosHelper.saveCompleteInsuranceInfo(recipientInsViewDetails,employmentDetails,null);
		
	}
	
	@Override
	public void savePersonFundCaseManagement(
			PersonFundCaseManagementObjects personFundCaseMgmtDetails)
			throws Exception {
		 VelosHelper.savePersonFundCaseManagement(personFundCaseMgmtDetails,null);
		
	}*/
	public List<Object> getPojoList(String domainName, Object pojoObject,
			String criteria, String type) {		
		return VelosHelper.getPojoList(domainName, pojoObject, criteria, type);
	}
	public void saveDomainFromPojoList(Object domainObject, String keyId,
			List savePojoList) {
		VelosHelper.saveDomainFromPojoList(domainObject, keyId, savePojoList);		
	}
	
	/******************* CDMR SPECIFIC CODE********************/
	
	public JSONObject saveFileInfo(JSONObject jsonObject)throws Exception{
		return CDMRHelper.saveFileInfo(jsonObject);
	}
	
	public JSONObject getFilesInfo(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getFilesInfo(jsonObject);
	}
	
	public JSONObject saveStagingMainInfo(JSONObject jsonObject)throws Exception{
		return CDMRHelper.saveStagingMainInfo(jsonObject);
	}
	
	public JSONObject saveStagingDetailInfo(JSONObject jsonObject)throws Exception{
		return CDMRHelper.saveStagingDetailInfo(jsonObject);
	}
	
	public JSONObject getCodeList(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getCodeList(jsonObject);
	}
	
	public JSONObject getStagingID(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getStagingID(jsonObject);
	}
	
	public JSONObject getActiveMapVesion(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getActiveMapVesion(jsonObject);
	}
	
	public JSONObject getStagingMaps(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getStagingMaps(jsonObject);
	}
	
	public JSONObject getStagingPrimaryID(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getStagingPrimaryID(jsonObject);
	}
	
	public JSONObject getStagingDetailData(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getStagingDetailData(jsonObject);
	}
	
	public JSONObject getCompReqColumnMapDetail(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getCompReqColumnMapDetail(jsonObject);
	}
	
	public JSONObject saveCompRecInfo(JSONObject jsonObject)throws Exception{
		return CDMRHelper.saveCompRecInfo(jsonObject);
	}
	
	public JSONObject getStagingColumnMapDetail(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getStagingColumnMapDetail(jsonObject);
	}
	
	public JSONObject saveInStagingCompReq(JSONObject jsonObject)throws Exception{
		return CDMRHelper.saveInStagingCompReq(jsonObject);
	}
	
	public JSONObject getCompRecData(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getCompRecData(jsonObject);
	}
	
	public JSONObject countRecord(JSONObject jsonObject)throws Exception{
		return CDMRHelper.countRecord(jsonObject);
	}
	
	public JSONObject getGroups(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getGroups(jsonObject);
	}
	
	public JSONObject updateComrec(JSONObject jsonObject)throws Exception{
		return CDMRHelper.updateComrec(jsonObject);
	}
	public JSONObject getGroupCodeValue(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getGroupCodeValue(jsonObject);
	}
	
	public JSONObject updateFileStatus(JSONObject jsonObject)throws Exception{
		return CDMRHelper.updateFileStatus(jsonObject);
	}
	
	public JSONObject getNextPossibleStatus(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getNextPossibleStatus(jsonObject);
	}
	
	public JSONObject insertInCDMMain(JSONObject jsonObject)throws Exception{
		return CDMRHelper.insertInCDMMain(jsonObject);
	}
	public JSONObject getMatchedData(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getMatchedData(jsonObject);
	}
	public JSONObject saveInMainCDM(JSONObject jsonObject)throws Exception{
		return CDMRHelper.saveInMainCDM(jsonObject);
	}
	public JSONObject getCDMVersionDetail(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getCDMVersionDetail(jsonObject);
	}
	public JSONObject saveCdmVersionDef(JSONObject jsonObject)throws Exception{
		return CDMRHelper.saveCdmVersionDef(jsonObject);
	}
	public JSONObject updateCdmVersionDef(JSONObject jsonObject)throws Exception{
		return CDMRHelper.updateCdmVersionDef(jsonObject);
	}
	public JSONObject getFromCdmMain(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getFromCdmMain(jsonObject);
	}
	public JSONObject saveInSSLink(JSONObject jsonObject)throws Exception{
		return CDMRHelper.saveInSSLink(jsonObject);
	}
	public JSONObject getFromCdmSSLink(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getFromCdmSSLink(jsonObject);
	}
	public JSONObject saveNewServiceSet(JSONObject jsonObject)throws Exception{
		return CDMRHelper.saveNewServiceSet(jsonObject);
	}
	public JSONObject getMaxGrupCode(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getMaxGrupCode(jsonObject);
	}
	public JSONObject saveInCdmGrupCode(JSONObject jsonObject)throws Exception{
		return CDMRHelper.saveInCdmGrupCode(jsonObject);
	}
	public JSONObject updateInCdmGrupCode(JSONObject jsonObject)throws Exception{
		return CDMRHelper.updateInCdmGrupCode(jsonObject);
	}
	public JSONObject getFromCDMGroupCode(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getFromCDMGroupCode(jsonObject);
	}
	public JSONObject getTechnicalCharges(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getTechnicalCharges(jsonObject);
	}
	public JSONObject getProfessionalCharges(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getProfessionalCharges(jsonObject);
	}
	public JSONObject insertInStagingTemp(JSONObject jsonObject)throws Exception{
		return CDMRHelper.insertInStagingTemp(jsonObject);
	}
	public JSONObject truncateStagingTemp(JSONObject jsonObject)throws Exception{
		return CDMRHelper.truncateStagingTemp(jsonObject);
	}
	public JSONObject getMappingForSQLBinding(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getMappingForSQLBinding(jsonObject);
	}
	
	public JSONObject getFromStagingTemp(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getFromStagingTemp(jsonObject);
	}
	public  JSONObject getMapVersion(JSONObject jsonObject) throws Exception{
		return CDMRHelper.getActiveMapVesion(jsonObject);
	}
	public JSONObject getCdmMain(JSONObject jsonObject) throws Exception{
		return CDMRHelper.getFromCdmMain(jsonObject);
	}
	public JSONObject saveStagingDetail(JSONObject jsonObject) throws Exception{
		return CDMRHelper.saveStagingDetail(jsonObject);
	}
	public JSONObject getChagIndc(JSONObject jsonObject) throws Exception{
		return CDMRHelper.getChagIndc(jsonObject);
	}
	public JSONObject updateFilesLibForRemoteData(JSONObject jsonObject) throws Exception{
		return CDMRHelper.updateFilesLibForRemoteData(jsonObject);
	}
	
	public JSONObject editServiceSection(JSONObject jsonObject) throws Exception{
		return CDMRHelper.editServiceSection(jsonObject);
	}
	public JSONObject editServiceSet(JSONObject jsonObject) throws Exception{
		return CDMRHelper.editServiceSection(jsonObject);
	}
	public JSONObject getUserDetail(JSONObject jsonObject) throws Exception{
		return CDMRHelper.getUserDetail(jsonObject);
	}
	public JSONObject getFromNotifycnf(JSONObject jsonObject) throws Exception{
		return CDMRHelper.getFromNotifycnf(jsonObject);
	}
	public JSONObject getFromEsec(JSONObject jsonObject) throws Exception{
		return CDMRHelper.getFromEsec(jsonObject);
	}
	public JSONObject saveNotificationInfo(JSONObject jsonObject) throws Exception{
		return CDMRHelper.saveNotificationInfo(jsonObject);
	}
	
	public JSONObject getSchedulerTime(JSONObject jsonObject) throws Exception{
		return CDMRHelper.getSchedulerTime(jsonObject);
	}
	
	public JSONObject updateSchdulerTime(JSONObject jsonObject) throws Exception{
		return CDMRHelper.updateSchdulerTime(jsonObject);
	}
	public JSONObject deleteNotifiedUser(JSONObject jsonObject) throws Exception{
		return CDMRHelper.deleteNotifiedUser(jsonObject);
	}
	
	public JSONObject deleteNotification(JSONObject jsonObject) throws Exception{
		return CDMRHelper.deleteNotification(jsonObject);
	}
	
	public JSONObject loadAllNotification(JSONObject jsonObject) throws Exception{
		return CDMRHelper.loadAllNotification(jsonObject);
	}
	
	public JSONObject saveErrorLog(JSONObject jsonObject) throws Exception{
		return CDMRHelper.saveErrorLog(jsonObject);
	}
	public JSONObject updateErrorLog(JSONObject jsonObject) throws Exception{
		return CDMRHelper.updateErrorLog(jsonObject);
	}
	
	public JSONObject getMailConfigurationInfo(JSONObject jsonObject) throws Exception{
		return CDMRHelper.getMailConfigurationInfo(jsonObject);
	}
	
	public JSONObject getNotificationMessages(JSONObject jsonObject) throws Exception{
		return CDMRHelper.getNotificationMessages(jsonObject);
	}
	public JSONObject updateNotification(JSONObject jsonObject) throws Exception{
		return CDMRHelper.updateNotification(jsonObject);
	}
	
	public JSONObject updateEmailSettings(JSONObject jsonObject) throws Exception{
		return CDMRHelper.updateEmailSettings(jsonObject);
	}
	
	public JSONObject getEmailSettings(JSONObject jsonObject) throws Exception{
		return CDMRHelper.getEmailSettings(jsonObject);
	}
	
	public JSONObject updateNoOfFiles(JSONObject jsonObject) throws Exception{
		return CDMRHelper.updateNoOfFiles(jsonObject);
	}
	
	public JSONObject getNumberOfFiles(JSONObject jsonObject) throws Exception{
		return CDMRHelper.getNumberOfFiles(jsonObject);
	}
	public JSONObject getSuperAdmin(JSONObject jsonObject) throws Exception{
		return CDMRHelper.getSuperAdmin(jsonObject);
	}
	
	public JSONObject saveForgotPasswordRequest(JSONObject jsonObject) throws Exception{
		return CDMRHelper.saveForgotPasswordRequest(jsonObject);
	}
	
	public JSONObject getDefaultPassword(JSONObject jsonObject) throws Exception{
		return CDMRHelper.getDefaultPassword(jsonObject);
		//
	}
	
	public JSONObject getUserEmail(JSONObject jsonObject) throws Exception{
		return CDMRHelper.getUserEmail(jsonObject);
	}
	
	public JSONObject updateNotificationStatus(JSONObject jsonObject) throws Exception{
		return CDMRHelper.updateNotificationStatus(jsonObject);
	}
	
	public JSONObject saveInObsSvcBfr(JSONObject jsonObject) throws Exception{
		return CDMRHelper.saveInObsSvcBfr(jsonObject);
	}
	public List<Map> fetchColumnForDataTable(JSONObject jsonObject)throws Exception{
		return CDMRHelper.fetchColumnForDataTable(jsonObject);
	}
	public void saveColumnState(JSONObject jsonObject)throws Exception{
		CDMRHelper.saveColumnState(jsonObject);
	}
	public void saveColumnOrder(JSONObject jsonObject)throws Exception{
		CDMRHelper.saveColumnOrder(jsonObject);
	}
	
	public JSONObject getFilePkForStatus(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getFilePkForStatus(jsonObject);
	}
	public JSONObject getFileSourceLocation(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getFileSourceLocation(jsonObject);
	}
	public JSONObject getFileInformation(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getFileInformation(jsonObject);
	}
	public boolean checkForExistingFile(JSONObject jsonObject)throws Exception{
		return CDMRHelper.checkForExistingFile(jsonObject);
	}
	public JSONObject getPullMethod(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getPullMethod(jsonObject);
	}
	public JSONObject getChgIndc(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getChgIndc(jsonObject);
	}
	public List<String> getInterfaceList(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getInterfaceList(jsonObject);
	}
	public JSONObject getProcessedDirLocation(JSONObject jsonObject)
	throws Exception {
       return CDMRHelper.getProcessedDirLocation(jsonObject);
    }
	public JSONObject getFileName(JSONObject jsonObject)
	throws Exception {
     return CDMRHelper.getFileName(jsonObject);
    }
	public JSONObject getFileID(JSONObject jsonObject)
	throws Exception {
     return CDMRHelper.getFileID(jsonObject);
    }
	public JSONObject getModulename(JSONObject jsonObject)
	throws Exception {
     return CDMRHelper.getModulename(jsonObject);
    }
	public JSONObject getMatchingCriteria(JSONObject jsonObject)
	throws Exception {
     return CDMRHelper.getMatchingCriteria(jsonObject);
    }
	public JSONObject getEventLibId	(JSONObject jsonObject)
	throws Exception {
     return CDMRHelper.getEventLibId(jsonObject);
    }
	public JSONObject getchgtype	(JSONObject jsonObject)
	throws Exception {
     return CDMRHelper.getchgtype(jsonObject);
    }
	
	public JSONObject getChgIndcBySeq(JSONObject jsonObject)
	throws Exception {
     return CDMRHelper.getChgIndcBySeq(jsonObject);
    }
	public JSONObject fetchEventLibId(JSONObject jsonObject)
	throws Exception {
     return CDMRHelper.fetchEventLibId(jsonObject);
    }
	public JSONObject fetchVersionID(JSONObject jsonObject)
	throws Exception {
     return CDMRHelper.fetchVersionID(jsonObject);
    }
	public JSONObject getActiveRule(JSONObject jsonObject)
	throws Exception {
     return CDMRHelper.getActiveRule(jsonObject);
    }
	
	public JSONObject fetchRTRule(JSONObject jsonObject)
	throws Exception {
     return CDMRHelper.fetchRTRule(jsonObject);
    }
	
	public JSONObject fetchcolTypeMapDetail(JSONObject jsonObject)
	throws Exception {
     return CDMRHelper.fetchcolTypeMapDetail(jsonObject);
    }
	
	/*Advanced search methods start*/
	public JSONObject getAdvsearchCols(JSONObject jsonObject)
	throws Exception {
     return CDMRHelper.getAdvsearchCols(jsonObject);
    }
	public JSONObject checkForFilterName(JSONObject jsonObject)
	throws Exception {
     return CDMRHelper.checkForFilterName(jsonObject);
    }
	public JSONObject saveNewFilter(JSONObject jsonObject)
	throws Exception {
	     return CDMRHelper.saveNewFilter(jsonObject);
	    }
	public JSONObject loadFilters(JSONObject jsonObject)
	throws Exception {
	     return CDMRHelper.loadFilters(jsonObject);
	    }
	public JSONObject saveFilterParam(JSONObject jsonObject)
	throws Exception {
	     return CDMRHelper.saveFilterParam(jsonObject);
	    }
	public JSONObject getCdmFilParam(JSONObject jsonObject)
	throws Exception {
	     return CDMRHelper.getCdmFilParam(jsonObject);
	    }
	public JSONObject updateCdmFilMaint(JSONObject jsonObject)
	throws Exception {
	     return CDMRHelper.updateCdmFilMaint(jsonObject);
	    }
	public JSONObject getSavedFilters(JSONObject jsonObject)
	throws Exception {
	     return CDMRHelper.getSavedFilters(jsonObject);
	    }
	public JSONObject getPkforFilter(JSONObject jsonObject)
	throws Exception {
	     return CDMRHelper.getPkforFilter(jsonObject);
	    }
	public JSONObject updateCdmParam(JSONObject jsonObject)
	throws Exception {
	     return CDMRHelper.updateCdmParam(jsonObject);
	    }
	/*transfer to ctms*/
	public JSONObject dataTransferStatus(JSONObject jsonObject)throws Exception{
		return CDMRHelper.dataTransferStatus(jsonObject);
	}
	public JSONObject getexportDatacount(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getexportDatacount(jsonObject);
	}
	public JSONObject transferToCtms()throws Exception{
		return CDMRHelper.transferToCtms();
	}
	public JSONObject updateTransferstatus(JSONObject jsonObject)throws Exception{
		return CDMRHelper.updateTransferstatus(jsonObject);
	}
	public JSONObject saveNewNotification(JSONObject jsonObject)throws Exception{
		return CDMRHelper.saveNewNotification(jsonObject);
	}
	public JSONObject getPasswordAuthFlag(JSONObject jsonObject)throws Exception{
		return CDMRHelper.getPasswordAuthFlag(jsonObject);
	}
	public JSONObject getLibraryName(JSONObject jsonObject)	throws Exception {
     return CDMRHelper.getLibraryName(jsonObject);
    }
}

