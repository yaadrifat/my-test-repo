package com.velos.stafa.business.domain.cdmr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.velos.stafa.business.domain.BaseDomainObject1;

@Entity
@Table(name="CDM_NOTIFYCNF")
@javax.persistence.SequenceGenerator(name="notify", sequenceName="SEQ_CDM_NOTIFYCNF")
public class CdmNotifycnf extends BaseDomainObject1 {
	
	private Long pkCdmnotifycnf;
	private String stateFrom;
	private String stateTo;
	private Integer userId;
	private String userName;
	private String userEmail;
	private String messageTemp;
	private Integer deletedFalg;
	private String fileDesc;
	private Long rid;
	
	@Id
	@GeneratedValue(generator="notify",strategy=GenerationType.AUTO)
	@Column(name="PK_CDM_NOTIFYCNF")
	public Long getPkCdmnotifycnf() {
		return pkCdmnotifycnf;
	}
	public void setPkCdmnotifycnf(Long pkCdmnotifycnf) {
		this.pkCdmnotifycnf = pkCdmnotifycnf;
	}
	
	@Column(name="STATE_FROM")
	public String getStateFrom() {
		return stateFrom;
	}
	public void setStateFrom(String stateFrom) {
		this.stateFrom = stateFrom;
	}
	
	@Column(name="STATE_TO")
	public String getStateTo() {
		return stateTo;
	}
	public void setStateTo(String stateTo) {
		this.stateTo = stateTo;
	}
	
	@Column(name="USER_ID")
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	@Column(name="USER_NAME")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name="USER_EMAIL")
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	
	@Column(name="MSG_TEMPLATE")
	public String getMessageTemp() {
		return messageTemp;
	}
	public void setMessageTemp(String messageTemp) {
		this.messageTemp = messageTemp;
	}
	
	@Column(name="RID")
	public Long getRid() {
		return rid;
	}
	public void setRid(Long rid) {
		this.rid = rid;
	}
	@Column(name="DELETEDFLAG")
	public Integer getDeletedFalg() {
		return deletedFalg;
	}

	public void setDeletedFalg(Integer deletedFalg) {
		this.deletedFalg = deletedFalg;
	}
	
	@Column(name="FILE_DESC")
	public String getFileDesc() {
		return fileDesc;
	}

	public void setFileDesc(String fileDesc) {
		this.fileDesc = fileDesc;
	}
	
}

