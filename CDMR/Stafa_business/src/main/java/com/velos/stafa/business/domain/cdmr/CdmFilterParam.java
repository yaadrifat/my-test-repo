package com.velos.stafa.business.domain.cdmr;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.velos.stafa.business.domain.BaseDomainObject;

/**
 * @summary     CdmFilterParam
 * @description Mapping class of CDM_FILTER_PARAM Table
 * @version     1.0
 * @file        CdmFilterParam.java
 * @author      Shikha Srivastva
 */

@Entity
@Table(name="CDM_FILTER_PARAM")
@javax.persistence.SequenceGenerator(name="filterparam_sqnc", sequenceName="SEQ_CDM_FILTER_PARAM")
public class CdmFilterParam extends BaseDomainObject {
	
	private long pkFilterparam;
	private long fkpkFiltermaint;
	private String cdmcolsname;
	private String filterColName;
	private String filterval;
	private String upflag;
	
	@Id
	@GeneratedValue(generator="filterparam_sqnc",strategy=GenerationType.AUTO)
	@Column(name="PK_FILTER_PARAM")
	public long getPkFilterparam() {
		return pkFilterparam;
	}
	public void setPkFilterparam(long pkFilterparam) {
		this.pkFilterparam = pkFilterparam;
	}
	@Column(name="FK_FILTER_MAINT")
	public long getFkpkFiltermaint() {
		return fkpkFiltermaint;
	}
	public void setFkpkFiltermaint(long fkpkFiltermaint) {
		this.fkpkFiltermaint = fkpkFiltermaint;
	}
	@Column(name="CDM_COLS_NAME")
	public String getCdmcolsname() {
		return cdmcolsname;
	}
	public void setCdmcolsname(String cdmcolsname) {
		this.cdmcolsname = cdmcolsname;
	}
	@Column(name="FILTER_COL")
	public String getFilterColName() {
		return filterColName;
	}
	public void setFilterColName(String filterColName) {
		this.filterColName = filterColName;
	}
	@Column(name="FILTER_VAL")
	public String getFilterval() {
		return filterval;
	}
	public void setFilterval(String filterval) {
		this.filterval = filterval;
	}
	@Column(name="FLAG")
	public String getUpflag() {
		return upflag;
	}
	public void setUpflag(String upflag) {
		this.upflag = upflag;
	}
	
	
	
}
