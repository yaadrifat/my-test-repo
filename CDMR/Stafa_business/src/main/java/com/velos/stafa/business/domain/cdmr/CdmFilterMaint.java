package com.velos.stafa.business.domain.cdmr;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.velos.stafa.business.domain.BaseDomainObject;

/**
 * @summary     CdmFilterMaint
 * @description Mapping class of CDM_FILTER_MAINT Table
 * @version     1.0
 * @file        CdmFilterMaint.java
 * @author      Shikha Srivastva
 */

@Entity
@Table(name="CDM_FILTER_MAINT")
@javax.persistence.SequenceGenerator(name="filtermaint_sqnc", sequenceName="SEQ_CDM_FILTER_MAINT")
public
class CdmFilterMaint extends BaseDomainObject {
	
	private long pkFiltermaint;
	private String filterName;
	private long userId;
	private String eventLibid;
	
	@Id
	@GeneratedValue(generator="filtermaint_sqnc",strategy=GenerationType.AUTO)
	@Column(name="PK_FILTER_MAINT")
	public long getPkFiltermaint() {
		return pkFiltermaint;
	}
	public void setPkFiltermaint(long pkFiltermaint) {
		this.pkFiltermaint = pkFiltermaint;
	}
	
	@Column(name="FILTER_NAME")
	public String getFilterName() {
		return filterName;
	}
	
	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}
	
	@Column(name="USER_ID")
	public long getUserId() {
		return userId;
	}
		
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	@Column(name="EVENT_LIB_ID")
	public String getEventLibid() {
		return eventLibid;
	}
		
	public void setEventLibid(String eventLibid) {
		this.eventLibid = eventLibid;
	}
	
		

}
