package com.velos.stafa.business.domain.usertracking;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
* This class made for tracking the user login and logout .
* @author Mohiuddin Ali Ahmed 
* @version 1
*/
@Entity
@Table(name="USERSESSION")
@SequenceGenerator(name="SEQ_USERSESSION",sequenceName="SEQ_USERSESSION")
public class UserSession {

	private Long pkUserSession;
	private Long user;
	private Timestamp loginTime;
	private Timestamp logoutTime;
	private Boolean successFlag;
	private String ipAddress;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_USERSESSION")
	@Column(name="PK_USERSESSION")
	public Long getPkUserSession() {
		return pkUserSession;
	}
	public void setPkUserSession(Long pkUserSession) {
		this.pkUserSession = pkUserSession;
	}
	
	@Column(name="FK_USER")
	public Long getUser() {
		return user;
	}
	public void setUser(Long user) {
		this.user = user;
	}
	public void setLoginTime(Timestamp loginTime) {
		this.loginTime = loginTime;
	}
	@Column(name="LOGIN_TIME",updatable=false)
	public Timestamp getLoginTime() {
		return loginTime;
	}		
	@Column(name="LOGOUT_TIME",insertable=false)
	public Timestamp getLogoutTime() {
		return logoutTime;
	}
	public void setLogoutTime(Timestamp logoutTime) {
		this.logoutTime = logoutTime;
	}
	@Column(name="SUCCESS_FLAG")
	public Boolean getSuccessFlag() {
		return successFlag;
	}
	public void setSuccessFlag(Boolean successFlag) {
		this.successFlag = successFlag;
	}
	@Column(name="IP_ADD")
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
}
