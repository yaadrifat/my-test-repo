package com.velos.stafa.business.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.taglib.json.util.JSONObject;


public class ObjectTransfer {

	

	private static final String SET="set";
	private static final String GET="get";

	
	public static String generateIDs(List<Object> lstObjectList,String fieldName){
		StringBuffer returnValue = new StringBuffer();
		String methodName ="";
		Object valueObject;
		Object fromObject = new Object();
		int methodNum = 0;
		if(lstObjectList != null && lstObjectList.size() > 0){
			returnValue.append(",");
			fromObject = (Object) lstObjectList.get(0);
			
			Method[] codeMethod = fromObject.getClass().getMethods();
			for(int m =0;m<codeMethod.length;m++){
				if(codeMethod[m].getName().startsWith(GET)){
					methodName = codeMethod[m].getName().substring(3);
					 if(fieldName.equalsIgnoreCase(methodName)){
						 methodNum = m;
					 }
				}
			}
			//System.out.println(" method num " + methodNum);
			
			for(int i=0;i<lstObjectList.size();i++){
				fromObject = (Object) lstObjectList.get(i);
				
				 codeMethod = fromObject.getClass().getMethods();
							  try{	
								  valueObject = codeMethod[methodNum].invoke(fromObject, null);
								  if(valueObject != null && !valueObject.toString().equalsIgnoreCase("null")){
									   returnValue.append(valueObject.toString() + ",");
								  }
							  }catch(Exception e){
								  System.out.println(" Exception in generateIDs() :: " + e);
								   e.printStackTrace();
							  }
						  }
						}
					
		return returnValue.toString();
	}
	
	public static Map<String,String> transferListToMap(List<Object> lstObjectList,String[] fields){
		Map<String,String> lstResult = new HashMap<String,String>();
	
		String[] strArray;
		String methodName ="";
		Object valueObject;
		Object fromObject = new Object();
		if(lstObjectList != null && lstObjectList.size() > 0){
			for(int i=0;i<lstObjectList.size();i++){
				fromObject = (Object) lstObjectList.get(i);
				strArray = new String[fields.length];
				Method[] codeMethod = fromObject.getClass().getMethods();
				for(int m =0;m<codeMethod.length;m++){
					if(codeMethod[m].getName().startsWith("get")){
						methodName = codeMethod[m].getName().substring(3);
						for(int f=0;f<fields.length;f++){
						  if(fields[f].equalsIgnoreCase(methodName)){
							  try{
								  valueObject = codeMethod[m].invoke(fromObject, null);
								  if(valueObject == null|| valueObject.toString().equalsIgnoreCase("null")){
									  strArray[f] = "";
								  }else{
									  strArray[f] = valueObject.toString();
								  }
							  }catch(Exception e){
								  System.out.println(" Exception in transferCodeListValue() :: " + e);
								   e.printStackTrace();
							  }
						  }
						}
					}
				}
				//System.out.println(" Map transfer  " + strArray[0] + " / " +  strArray[1] );
				lstResult.put(strArray[0], strArray[1]);
			}
		}

		return lstResult;
	}
	
	
	
	
	
	// to be optimize using haspmap
	// transfer one object values to other object retun toObject
	public static Object transferObjects(Object fromObject, Object toObject){
		Object returnObject = new Object();
		HashMap toFieldTypes = new HashMap();
	//	System.out.println(" from obj " + fromObject + " / to obj " + toObject);
		if(fromObject!= null && toObject!=null){

			Method[] fromMethods = fromObject.getClass().getMethods();
			Method[] toMethods = toObject.getClass().getMethods();
			Field[] declareFields = toObject.getClass().getDeclaredFields();
			Field[] fields = toObject.getClass().getFields();
			int fieldSize = declareFields.length + fields.length;

			Field[] toFields = new Field[fieldSize];
			int i =0;

			for( ;i<declareFields.length;i++){
				toFields[i] = declareFields[i];
			}

			for(int j=0;j<fields.length;j++,i++){
		//		System.out.println(" i == " + i);
				toFields[i] = fields[j];
			}
		//	System.out.println("to length " + toFields.length);
			for (int count =0;count<toFields.length;count++){
				toFieldTypes.put(toFields[count].getName().toUpperCase(), toFields[count].getType());
		//		System.out.println(count + " / " +toFields[count].getName().toUpperCase() + " /type "+  toFields[count].getType() );
			}
		//	System.out.println("length ~~~~~~~~~~~~~~~~~~~" + fromMethods.length + " / " + toMethods.length);
			String fromMethodName="";
			String toMethodName ="";
			Object[] param = null;
			for( i=0;i<fromMethods.length;i++){
				//System.out.println(" method name " + fromMethods[i].getName());
			   if(fromMethods[i].getName().indexOf(GET) == 0 ){ // only get methods
				   fromMethodName = fromMethods[i].getName();
				   fromMethodName= fromMethodName.substring(3,fromMethodName.length() );
		//		   System.out.println(" method name " + fromMethods[i].getName());
				   if(declareFields.length < 2 || toFieldTypes.containsKey(fromMethodName.toUpperCase())){
					   // field length for extend class
					   for(int j =0;j<toMethods.length;j++){
						   if(toMethods[j].getName().indexOf(SET) == 0){ // only set methods
							   toMethodName = toMethods[j].getName();
		//				   System.out.println(" to method " + toMethodName);
							   toMethodName = toMethodName.substring(3, toMethodName.length());
					//		   System.out.println( fromMethodName + "=====" + toMethodName);
							   if(fromMethodName.equals(toMethodName)){
		//						   System.out.println(" equal methods " + fromMethodName);
								   try{
									   Object obj = fromMethods[i].invoke(fromObject, param);
		//							   System.out.println(" obj **************" + obj);
									   if(obj != null ){
										   //--------- conversions -----------
										  // System.out.println(" trace ***********" +toMethodName + " /1 " + obj.getClass() + " /3 " + toFieldTypes.get(toMethodName.toUpperCase()));
										   if(declareFields.length > 1) { // for dto transfer
											   if(toFieldTypes.get(toMethodName.toUpperCase()).equals(new Date().getClass()) && (obj instanceof String)  ){
												   // string to Date
											//	   System.out.println(" inside if 1");
												   if(!((String)obj).trim().equals("")){
													   obj = Utilities.getDate((String)obj);
												   }else{
													   continue;
												   }
											   }else  if( (obj instanceof java.util.Date) && (toFieldTypes.get(toMethodName.toUpperCase()).equals(new String().getClass())) ){
												   // Date to string
												//   System.out.println(" inside if 2");
												   obj = Utilities.getFormattedDate((Date)obj);

											   }else if(toFieldTypes.get(toMethodName.toUpperCase()).equals(new Date().getClass()) && (obj instanceof String)  ){
												   // String to Timestamp
												//   System.out.println(" inside if 3");
												   if(!((String)obj).trim().equals("")){
														obj = Utilities.convertstringtotimestamp((String)obj,"0","0");
													}else{
														continue;
													}

											   }else if((obj instanceof java.sql.Timestamp) && (toFieldTypes.get(toMethodName.toUpperCase()).equals(new String().getClass())) ){
												   // Timestamp to string
												   //System.out.println(" inside if Timestamp ");
												   if(!((String)obj).trim().equals("")){
														obj = Utilities.convertstringtotimestamp((String)obj,"0","0");
													}else{
														continue;
													}

												   //obj = Utilities.getALICEFormattedDate((Date)obj);
												  // System.out.println(" end of sql timestamp ");
											   }else if ((obj instanceof String ) && (toFieldTypes.get(toMethodName.toUpperCase()) instanceof java.sql.Timestamp ) ){
												//   System.out.println( " String to time stamp ");
												   if( obj != null && !((String)obj).trim().equals("")){
													   obj = Utilities.convertstringtotimestamp((String)obj,"0","0");
												   }else{
													 //  System.out.println("else");
													   continue;
												   }
											   }
										   }
										  //========== end of conversion =======\
											   toMethods[j].invoke(toObject, obj);
									   }
								   }catch(Exception e){

									   System.out.println(fromMethodName + " -> Data type mismatch check data types in  (" + fromObject.getClass().getName() + " , " + toObject.getClass().getName()+")");
									   System.out.println(" Exception in transferObjects() :: " + e);
									  // e.printStackTrace();
								   }
								   break;
							   }
						   }
					   }// end of for
				   }//end of key check
			   }
			}
		}
		returnObject = toObject;
		//System.out.println("End of object transfer ");
		return returnObject;
	}


	// Method use to set the set values in list
	//public static List transferSetValues(List setObject,String[] fields){ // testing
	public static List transferSetValues(Set setObject,String[] fields){
		List<String[]> lstResult = new ArrayList<String[]>();
		String[] strValues;
		String subClassName="";

		Object obj ;
		//System.out.println(" transfer set values ");
		if(setObject != null && setObject.size() > 0 && fields != null && fields.length>0) {
				for(Iterator it=setObject.iterator();it.hasNext();){
					strValues = new String[fields.length];
					obj = (Object)it.next();

					Object valueObject;
					Method[] method = obj.getClass().getMethods();
					String methodName = "";
					String subMethodName ="";
					for(int m =0;m<method.length;m++){
						if(method[m].getName().startsWith("get")){
							//System.out.println("method name " + method[m].getName());
							methodName = method[m].getName().substring(3);
							for(int f=0;f<fields.length;f++){
								try{
									if(fields[f].equalsIgnoreCase(methodName)){
										valueObject = method[m].invoke(obj, null);
										if(valueObject == null || valueObject.toString().equalsIgnoreCase("null")){
											strValues[f] = "";
										}else{
											if((valueObject instanceof java.sql.Timestamp) || (valueObject instanceof java.util.Date) ){
												valueObject = Utilities.getFormattedDate((Date)valueObject);
											}
											strValues[f] = valueObject.toString();
										}
									}else if(fields[f].indexOf(".")>0){
										// get the subclass values
										subClassName = fields[f].substring(0,fields[f].indexOf("."));
										subMethodName = fields[f].substring(fields[f].indexOf(".")+1);
									//	System.out.println(" subclass name " + subClassName + " method " + methodName + " / " + subMethodName );

										Object subClass = new Object();
										if(subClassName.equalsIgnoreCase(methodName)){
											subClass = method[m].invoke(obj, null);
											if(subClass != null){
												Method[] subMethods = subClass.getClass().getMethods();
													for(int sm=0;sm<subMethods.length;sm++){
														if(subMethods[sm].getName().startsWith("get")){
															//System.out.println(" sub method name " + subMethods[sm].getName());
															if(subMethodName.equalsIgnoreCase(subMethods[sm].getName().substring(3))){
																valueObject = subMethods[sm].invoke(subClass, null);
																if(valueObject == null || valueObject.toString().equalsIgnoreCase("null")){
																	strValues[f] = "";
																}else{

																	strValues[f] = valueObject.toString();
																}
															}
														}
													}
											}
										}
										//System.out.println("end of sub class methods ");

									}
								}catch(Exception e){
									System.out.println(" Exception CAObjectTransfer.transferSetValues() :: " + e);
									e.printStackTrace();
								}
							}
						}
					}
					lstResult.add(strValues);
				}
		}
		//System.out.println("CAObjectTransfer end");
		return lstResult;
	}

	// Method to use transfer QueryList to List
	public static List transferQueryListToList(List lstQueryList,String[] fields){
		//System.out.println("transfer query list ");
		List<String[]> lstResult = new ArrayList<String[]>();
		String[] strValues;
		String subClassName="";

		Object obj ;
		//System.out.println(" transfer set values ");
		if(lstQueryList != null && lstQueryList.size() > 0 && fields != null && fields.length>0) {
				for(Iterator it=lstQueryList.iterator();it.hasNext();){
					strValues = new String[fields.length];
					obj = (Object)it.next();

					Object valueObject;
					Method[] method = obj.getClass().getMethods();
					String methodName = "";
					String subMethodName ="";
					for(int m =0;m<method.length;m++){
						if(method[m].getName().startsWith("get")){
							//System.out.println("method name " + method[m].getName());
							methodName = method[m].getName().substring(3);
							for(int f=0;f<fields.length;f++){
								try{
									if(fields[f].equalsIgnoreCase(methodName)){
										valueObject = method[m].invoke(obj, null);
										if(valueObject == null || valueObject.toString().equalsIgnoreCase("null")){
											strValues[f] = "";
										}else{
											if((valueObject instanceof java.sql.Timestamp) || (valueObject instanceof java.util.Date) ){
												valueObject = Utilities.getFormattedDate((Date)valueObject);
											}
											strValues[f] = valueObject.toString();

										}
									}else if(fields[f].indexOf(".")>0){
										// get the subclass values
										subClassName = fields[f].substring(0,fields[f].indexOf("."));
										subMethodName = fields[f].substring(fields[f].indexOf(".")+1);
									//	System.out.println(" subclass name " + subClassName + " method " + methodName + " / " + subMethodName );

										Object subClass = new Object();
										if(subClassName.equalsIgnoreCase(methodName)){
											subClass = method[m].invoke(obj, null);
											if(subClass != null){
												Method[] subMethods = subClass.getClass().getMethods();
													for(int sm=0;sm<subMethods.length;sm++){
														if(subMethods[sm].getName().startsWith("get")){
															//System.out.println(" sub method name " + subMethods[sm].getName());
															if(subMethodName.equalsIgnoreCase(subMethods[sm].getName().substring(3))){
																valueObject = subMethods[sm].invoke(subClass, null);
																if(valueObject == null || valueObject.toString().equalsIgnoreCase("null")){
																	strValues[f] = "";
																}else{
																	if((valueObject instanceof java.sql.Timestamp) || (valueObject instanceof java.util.Date) ){
																		valueObject = Utilities.getFormattedDate((Date)valueObject);
																	}
																	strValues[f] = valueObject.toString();
																}
															}
														}
													}
											}
										}
										//System.out.println("end of sub class methods ");

									}
								}catch(Exception e){
									System.out.println(" Exception CAObjectTransfer.transferQueryListValues() :: " + e);
									e.printStackTrace();
								}
							}
						}
					}
					lstResult.add(strValues);
				}
		}
		//System.out.println("Transfer query list end");
		return lstResult;

	}


	public static List transferObjectListToList(List lstObjectList,String[] fields){
		List<String[]> lstResult = new ArrayList<String[]>();
	//	CodeList codeList = new CodeList();
		String[] strArray;
		String methodName ="";
		Object valueObject;
		Object fromObject = new Object();
		if(lstObjectList != null && lstObjectList.size() > 0){
			for(int i=0;i<lstObjectList.size();i++){
				fromObject = (Object) lstObjectList.get(i);
				strArray = new String[fields.length];
				Method[] codeMethod = fromObject.getClass().getMethods();
				for(int m =0;m<codeMethod.length;m++){
					if(codeMethod[m].getName().startsWith("get")){
						methodName = codeMethod[m].getName().substring(3);
						for(int f=0;f<fields.length;f++){
						  if(fields[f].equalsIgnoreCase(methodName)){
							  try{
								  valueObject = codeMethod[m].invoke(fromObject, null);
								  if(valueObject == null|| valueObject.toString().equalsIgnoreCase("null")){
									  strArray[f] = "";
								  }else{
									  strArray[f] = valueObject.toString();
								  }
							  }catch(Exception e){
								  System.out.println(" Exception in transferCodeListValue() :: " + e);
								   e.printStackTrace();
							  }
						  }
						}
					}
				}
				lstResult.add(strArray);
			}
		}

		return lstResult;
	}

	public static List transferObjectsInsideList(List srcList, Object fromObject,Object toObject){
		List<Object> lstResult = new ArrayList<Object>();

		if(srcList != null && srcList.size()>0){
			Object tmpObject;
			try{
				for(int i=0;i<srcList.size();i++){
					tmpObject = toObject.getClass().newInstance();
					lstResult.add(transferObjects((Object)srcList.get(i), tmpObject));
				}
			}catch(Exception e){
				System.out.println(" Exception in transfer " + e);
				e.printStackTrace();
			}
		}

		return lstResult;
	}
	

	public static Object tranferJSONObjecttoObject(JSONObject jsonObject, Object toObject){
		
		Object returnObject = new Object();;
		HashMap toFieldTypes = new HashMap();
		//	System.out.println(" from obj " + fromObject + " / to obj " + toObject);
			if(jsonObject!= null && toObject!=null){

				
				Iterator iter = jsonObject.keys();
				
				
				Method[] toMethods = toObject.getClass().getMethods();
				Field[] declareFields = toObject.getClass().getDeclaredFields();
				Field[] fields = toObject.getClass().getFields();
				int fieldSize = declareFields.length + fields.length;

				Field[] toFields = new Field[fieldSize];
				int i =0;

				for( ;i<declareFields.length;i++){
					toFields[i] = declareFields[i];
				}

				for(int j=0;j<fields.length;j++,i++){
			//		System.out.println(" i == " + i);
					toFields[i] = fields[j];
				}
			//	System.out.println("to length " + toFields.length);
				for (int count =0;count<toFields.length;count++){
					toFieldTypes.put(toFields[count].getName().toUpperCase(), toFields[count].getType());
					//System.out.println(count + " / " +toFields[count].getName().toUpperCase() + " /type "+  toFields[count].getType() );
				}
			//	System.out.println("length ~~~~~~~~~~~~~~~~~~~" + fromMethods.length + " / " + toMethods.length);
				String fromMethodName="";
				String toMethodName ="";
				while(iter.hasNext()){
					//System.out.println(" method name " + fromMethods[i].getName());
					
					
					   fromMethodName= (String)iter.next();
			//		   System.out.println(" method name " + fromMethods[i].getName());
					   if(declareFields.length < 2 || toFieldTypes.containsKey(fromMethodName.toUpperCase())){
						   // field length for extend class
						   for(int j =0;j<toMethods.length;j++){
							   if(toMethods[j].getName().indexOf(SET) == 0){ // only set methods
								   toMethodName = toMethods[j].getName();
			//				   System.out.println(" to method " + toMethodName);
								   toMethodName = toMethodName.substring(3, toMethodName.length());
							   //System.out.println( fromMethodName + "=====" + toMethodName);
								   if(fromMethodName.toLowerCase().equals(toMethodName.toLowerCase())){
									   //System.out.println(" equal methods " + fromMethodName);
									   try{
										  // Object obj = fromMethods[i].invoke(fromObject, param);
										   Object obj = (Object)jsonObject.get(fromMethodName); 
										   
										  // System.out.println(" obj **************" + obj);
										   if(obj != null){
											   //--------- conversions -----------va.lang.Long
											  // System.out.println(" trace ***********" +toMethodName + " /1 " + obj.getClass() + " /3 " + toFieldTypes.get(toMethodName.toUpperCase()) + " / " +obj);
											   if(declareFields.length > 1) { // for dto transfer
												   if(toFieldTypes.get(toMethodName.toUpperCase()).equals(new Date().getClass())){
													   // Date to string
													 //  System.out.println(" obj " + obj);
													 //  System.out.println(" Date>" + obj.toString() + "<");
													   if(!obj.toString().equals("") && obj.toString().trim().length() >1  ){
													//	   System.out.println(" inside if--->");
														   obj = new Date(Utilities.getFormattedDate(new Date(obj.toString())));
													   }else{
														  //?BB continue;
														   obj = null;
													   }

												   }else if(toFieldTypes.get(toMethodName.toUpperCase()).equals(java.lang.Long.class)){
													   // String to Timestamp
													   //System.out.println(" inside if Long ");
													   if(!obj.toString().equals("")){
														   obj = new Long(obj.toString());
													   }else{
														   continue;
													   }

												   }else if(toFieldTypes.get(toMethodName.toUpperCase()).equals(java.lang.Integer.class)){
													  // System.out
															//.println("Inside Integer");
													   if(!obj.toString().equals("")){
														   obj = new Integer(obj.toString());
													   }else{
														   continue;
													   }
													   
												   }else if ((toFieldTypes.get(toMethodName.toUpperCase()).equals(java.sql.Timestamp.class)) ){
													  // System.out.println( " String to time stamp ");
													   if( obj != null && !(obj.toString()).trim().equals("")){
														   obj = Utilities.convertstringtotimestamp(obj.toString(),"0","0");
													   }else{
														 //  System.out.println("else");
														   continue;
													   }
												   }else if ((toFieldTypes.get(toMethodName.toUpperCase()).equals(java.lang.Character.class)) ){
													  // System.out.println( " String to Character ");
													   if( obj != null && !((String)obj).trim().equals("")){
														   obj = (Character)obj.toString().charAt(0);
													   }else{
														 //  System.out.println("else");
														   continue;
													   }
												   }else if(toFieldTypes.get(toMethodName.toUpperCase()).equals(java.lang.Double.class)){
														  // System.out
														//.println("Inside Doluble");
													   if(!obj.toString().equals("")){
														   obj = new Double(obj.toString());
													   }else{
														   continue;
													   }
												   
													   
												   }else if(toFieldTypes.get(toMethodName.toUpperCase()).equals(java.lang.Boolean.class)){
														  // System.out
														//.println("Inside Doluble");
													   if(obj!=null && obj.toString().equals("true")){
														   obj = new Boolean(true);
													   }else if(obj!=null && obj.toString().equals("false")){
														   obj = new Boolean(false);
													   }else{
														   continue;
													   }
												   
													   
												   }
												   
											   }
											  //========== end of conversion =======\
											 // System.out.println("Object : " + obj);
											  toMethods[j].invoke(toObject,  obj);
										   }
									   }catch(Exception e){
										   //	System.out.println("========== " + toMethodName + "/" +toMethodName.getClass() + " ");	
										   System.out.println(fromMethodName + " -> Data type mismatch check data types in  (" + jsonObject.getClass().getName() + " , " + toObject.getClass().getName()+")");
										   System.out.println(" Exception in transferObjects() :: " + e);
										   e.printStackTrace();
									   }
									   break;
								   }
							   }
						   }// end of for
					   }//end of key check
				   
				}
			}
			returnObject = toObject;
		
		return returnObject;
	}
	
	public static String getObjectIdValue(Object object, String keyId){
		String keyValue ="";
		Method[] fromMethods = object.getClass().getMethods();
		String methodName = "";
		Object[] param = null;
		try{
			for(int i=0;i<fromMethods.length;i++){
				if(fromMethods[i].getName().indexOf(GET) == 0 ){ // only get methods
					methodName = fromMethods[i].getName();
					methodName= methodName.substring(3,methodName.length() );
					if(methodName.toLowerCase().equals(keyId.toLowerCase())){
						keyValue = fromMethods[i].invoke(object, param).toString();
					}
				}	   
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return keyValue;
		
	}
	public static JSONObject setJSONUserDetails(JSONObject fromObject,JSONObject toObject){
		if(toObject == null){
			toObject = new JSONObject();
		}
		String[] userDetails = {"organizationId","createdBy","createdOn","lastModifiedBy","lastModifiedOn","ipAddress","deletedFlag"};
		try{
		for(int i=0;i<userDetails.length;i++){
			if(fromObject.has(userDetails[i])){
				toObject.remove(userDetails[i]);
				toObject.put(userDetails[i],fromObject.get(userDetails[i]));
			}
		}
		}catch(Exception e){
			e.printStackTrace();
		}

		return toObject;
	}
	public static JSONObject transferObjecttoJSONObject(Object fromObject, JSONObject toObject){
		try{
			if(toObject == null){
				toObject = new JSONObject();
			}
			
			if(fromObject !=null){
				Method[] fromMethods = fromObject.getClass().getMethods();
				String fromMethodName="";
				Object[] param = null;
				for(int i=0;i<fromMethods.length;i++){
					 if(fromMethods[i].getName().indexOf(GET) == 0 ){ // only get methods
						 fromMethodName = fromMethods[i].getName();
						 fromMethodName= fromMethodName.substring(3,fromMethodName.length() );
						 fromMethodName = fromMethodName.substring(0,1).toLowerCase() + fromMethodName.substring(1);
						 System.out.println("From Method Name " + fromMethodName);
						 Object obj = fromMethods[i].invoke(fromObject, param);
						 toObject.put(fromMethodName, obj);
						 
					 }
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return toObject;
	}
	
	public static List transferObjectlsttoJsonlist(List fromlst,Object fromObject, List tolst){
		 List list = new ArrayList();

		JSONObject jsonObject = new JSONObject();
		if(fromlst != null && !fromlst.isEmpty()){
			for(Iterator iter = fromlst.iterator();iter.hasNext();){
				fromObject = (Object)iter.next();
				jsonObject = new JSONObject();
				jsonObject = (JSONObject)transferObjecttoJSONObject(fromObject,jsonObject );
				list.add(jsonObject);
			}
		}
		return list;
	}
	

}
