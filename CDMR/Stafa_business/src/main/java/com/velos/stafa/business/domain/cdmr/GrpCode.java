package com.velos.stafa.business.domain.cdmr;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.velos.stafa.business.domain.BaseDomainObject;

@Entity
@Table(name="CDM_GRPCODE")
@javax.persistence.SequenceGenerator(name="grpcode_sqnc", sequenceName="SEQ_CDM_GRPCODE")
public class GrpCode extends BaseDomainObject {
	
	private Long pkGrpCode;
	private Long fkAccount;
	private String grpCode;
	private String grpName;
	private String grpDispName;
	private Integer grpLevel;
	private Integer parentNodeId;
	private Long rid;
	private String eventlibid;

	@Id
	@GeneratedValue(generator="grpcode_sqnc",strategy=GenerationType.AUTO)
	@Column(name="PK_CDM_GRPCODE")
	public Long getPkGrpCode() {
		return pkGrpCode;
	}
	public void setPkGrpCode(Long pkGrpCode) {
		this.pkGrpCode = pkGrpCode;
	}
	
	@Column(name="FK_ACCOUNT")
	public Long getFkAccount() {
		return fkAccount;
	}
	public void setFkAccount(Long fkAccount) {
		this.fkAccount = fkAccount;
	}
	
	@Column(name="GRP_CODE")
	public String getGrpCode() {
		return grpCode;
	}
	public void setGrpCode(String grpCode) {
		this.grpCode = grpCode;
	}
	
	@Column(name="GRP_NAME")
	public String getGrpName() {
		return grpName;
	}
	public void setGrpName(String grpName) {
		this.grpName = grpName;
	}
	
	@Column(name="GRP_DISP_NAME")
	public String getGrpDispName() {
		return grpDispName;
	}
	public void setGrpDispName(String grpDispName) {
		this.grpDispName = grpDispName;
	}
	
	@Column(name="GRP_LEVEL")
	public Integer getGrpLevel() {
		return grpLevel;
	}
	public void setGrpLevel(Integer grpLevel) {
		this.grpLevel = grpLevel;
	}
	
	@Column(name="PARENT_NODE_ID")
	public Integer getParentNodeId() {
		return parentNodeId;
	}
	public void setParentNodeId(Integer parentNodeId) {
		this.parentNodeId = parentNodeId;
	}
	
	@Column(name="RID")
	public Long getRid() {
		return rid;
	}
	public void setRid(Long rid) {
		this.rid = rid;
	}
	@Column(name="EVENT_LIB_ID")
	public String getEventlibid() {
		return eventlibid;
	}
	public void setEventlibid(String eventlibid) {
		this.eventlibid = eventlibid;
	}
}

