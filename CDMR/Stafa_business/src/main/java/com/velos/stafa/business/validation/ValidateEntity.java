/**
 * 
 */
package com.velos.stafa.business.validation;

import org.hibernate.Query;
import org.hibernate.Session;

import atg.taglib.json.util.JSONObject;

import com.velos.stafa.business.util.HibernateUtil;

/**
 * @author akajeera
 *
 */
public class ValidateEntity {
	
	private static Class c;
	private static Class[] parameterTypes;
	private static Object returnObject;
	
	private static String classname = "com.velos.stafa.business.validation.ValidateEntity";
	private static String CODELST_OBJECT = "com.velos.stafa.business.domain.CodeList";
	private static String SEQUENCE_PROPERTY ="sequence";
	
	public static JSONObject validateAfterSave(String objectName,JSONObject jsonObject) throws Exception{
		Long count = null;
		
		if(objectName.equals(CODELST_OBJECT)){
			String domainKey = jsonObject.getString("domainKey");
			Long keyValue = null;
			String keyValueStr = jsonObject.getString(domainKey);
			if(keyValueStr != null && !keyValueStr.equals("")){
				keyValue = Long.parseLong(keyValueStr);
			}

			String defaultValue = jsonObject.getString("defaultValue");
			if(defaultValue != null && defaultValue.equals("1")){
				String queryBuffer = "update " + objectName + " set defaultValue=0 where "+domainKey + " != " +  keyValue;
				String query = "";
				if(jsonObject.has("query")){
					query = jsonObject.getString("query");
					query += queryBuffer + ";";
				}else{
					query = queryBuffer;
				}
				jsonObject.put("query",query);
			}
		}
		return jsonObject;
	}
	
	public static JSONObject validateEntity(String objectName,JSONObject jsonObject) throws Exception{
		Long count = null;
		
		if(objectName.equals(CODELST_OBJECT)){
			String domainKey = jsonObject.getString("domainKey");
			Long keyValue = null;
			String keyValueStr = jsonObject.getString(domainKey);
			if(keyValueStr != null && !keyValueStr.equals("")){
				keyValue = Long.parseLong(keyValueStr);
			}
			System.out.println("Validate Entity :  keyValue  " + keyValue);
			if(keyValue == null || keyValue == 0){
				count = getCodelstSeq(jsonObject);
				System.out.println("Count : " + count);
				jsonObject.put(SEQUENCE_PROPERTY,(count+1));
			}	
		}
		return jsonObject;
	}
	/*public static List validateEntity(Object obj,String objectName) throws Exception{


		List errorList=new ArrayList();

		c=Class.forName(classname);
		boolean status;
		parameterTypes=new Class[1];
		//argList=new Object[1];
		Object argList[]=new Object[1];
		try{

			argList[0]=obj;
			HashMap hashMap= ReadXML.getInstance().getEntity(objectName);
			HashMap errorMap = ReadXML.getInstance().getErrorMap();
			for(Iterator iter=hashMap.entrySet().iterator();iter.hasNext();){
				Map.Entry entry=(Map.Entry)iter.next();
				String key=(String)entry.getKey();
				String[] value = (String[]) entry.getValue();
				parameterTypes = new Class[value.length];
				//argList = new Object[value.length];

				for (int i = 0; i < value.length; i++) {
					parameterTypes[i] = Class.forName(value[i]);
				}

				if (value != null) {
					Method method = c.getMethod(key, parameterTypes);
					//returnObject = method.invoke(method, argList);
					returnObject = method.invoke(method, argList);
					status = new Boolean(returnObject.toString()).booleanValue();
					////System.out.println(" Mapping ========"+ errorMap);
					if (status == false) {
						////System.out.println("Error Map" + errorMap);
						if (errorMap.get(key) != null) {
							errorList.add(errorMap.get(key).toString());
						}
					}

				} else {
					Method method = c.getMethod(key);
					returnObject = method.invoke(method);
					status = new Boolean(returnObject.toString()).booleanValue();
					if (status == false) {
						
						if (errorMap.get(key) != null) {
							errorList.add(errorMap.get(key).toString());
						}
					}
				}

			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return errorList;

	}*/
	public static Long getCodelstSeq(JSONObject jsonObject){
		Long seqvalue = null;
		Session session = null;
		try{
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			
			String queryBuffer = "select count(*) from CodeList where type = '"+jsonObject.getString("type") + "' and deletedFlag=0";
			Query query = session.createQuery(queryBuffer);
			
			System.out.println("In Validate Entity count " + query);
			seqvalue = (Long)query.list().get(0);
		}catch(Exception e){
			e.printStackTrace();
			
		}
		return seqvalue;
	}
}
