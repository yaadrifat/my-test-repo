/**
 * 
 */
package com.velos.stafa.business.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * @author akajeera
 *
 */
@MappedSuperclass
public class BaseDomainObject1 implements Serializable{

	public Long lastModifiedBy;
	public Date lastModifiedOn;
	public String ipAddress;
	public Long rid;
	//private Long organizationId;
	
	@Column(insertable = false, name = "LAST_MODIFIED_BY")
	public Long getLastModifiedBy() {
		return lastModifiedBy;
	}
	
	@Column(insertable = false, name = "LAST_MODIFIED_DATE")
	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}
	
	@Column(name = "IP_ADD")
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	
	public void setLastModifiedBy(Long lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}
	
	
}
