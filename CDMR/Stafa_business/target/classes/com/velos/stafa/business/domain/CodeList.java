/**
 * 
 */
package com.velos.stafa.business.domain;

/**
 * @author akajeera
 *
 */
/**
 * 
 */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author akajeera
 *
 */

@Entity
@Table(name="ER_CODELST")
@SequenceGenerator(sequenceName="SEQ_ER_CODELST",name="SEQ_ER_CODELST")
public class CodeList extends BaseDomainObject implements Serializable{

	private Long pkCodelst;
	private String type;
	private String subType;
	private String description;
	private Long sequence;
	private Character isHide;
	private Integer maintainable;
	private String pageUrl;
	private Long defaultValue;
	private String module;
	private String codelstvalue;
	private String comments;
	private Long copyCodelst;
	private Long applock;
	private String fieldType;
	private String customColumn1;
	private String customColumn2;
	public CodeList(){
		
	}	
	

	@Id
	@GeneratedValue(generator="SEQ_ER_CODELST",strategy=GenerationType.AUTO)
	@Column(name="PK_CODELST")
	public Long getPkCodelst() {
		return pkCodelst;
	}

	
	@Column(name="CODELST_TYPE")
	public String getType() {
		return type;
	}
	@Column(name="CODELST_SUBTYP")
	public String getSubType() {
		return subType;
	}
	@Column(name="CODELST_DESC")
	public String getDescription() {
		return description;
	}
	@Column(name="CODELST_SEQ")
	public Long getSequence() {
		return sequence;
	}
	@Column(name="CODELST_HIDE")
	public Character getIsHide() {
		return isHide;
	}
	@Column(name="CODELST_MAINT")
	public Integer getMaintainable() {
		return maintainable;
	}
	
	@Column(name="CODELST_PAGEURL")
	public String getPageUrl() {
		return pageUrl;
	}
	@Column(name="CODELST_DEFAULTVALUE")
	public Long getDefaultValue() {
		return defaultValue;
	}
	
	@Column(name="CODELST_MODULE")
	public String getModule() {
		return module;
	}
	@Column(name="CODELST_VALUE")
	public String getCodelstvalue() {
		return codelstvalue;
	}

	public void setCodelstvalue(String codelstvalue) {
		this.codelstvalue = codelstvalue;
	}
	@Column(name="CODELST_COMMENTS")
	public String getComments() {
		return comments;
	}
	@Column(name="FK_COPY_CODELST")
	public Long getCopyCodelst() {
		return copyCodelst;
	}

	@Column(name="APPLOCK")
	public Long getApplock() {
		return applock;
	}
	
	@Column(name="CODELST_FIELDTYPE")
	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public void setApplock(Long applock) {
		this.applock = applock;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public void setDefaultValue(Long defaultValue) {
		this.defaultValue = defaultValue;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	public void setPkCodelst(Long pkCodelst) {
		this.pkCodelst = pkCodelst;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}
	public void setIsHide(Character isHide) {
		this.isHide = isHide;
	}
	public void setMaintainable(Integer maintainable) {
		this.maintainable = maintainable;
	}
	public void setCopyCodelst(Long copyCodelst) {
		this.copyCodelst = copyCodelst;
	}
	@Column(name = "CODELST_CUSTOM_COL")
	public String getCustomColumn1() {
		return customColumn1;
	}
	@Column(name = "CODELST_CUSTOM_COL1")
	public String getCustomColumn2() {
		return customColumn2;
	}
	public void setCustomColumn1(String customColumn1) {
		this.customColumn1 = customColumn1;
	}
	public void setCustomColumn2(String customColumn2) {
		this.customColumn2 = customColumn2;
	}
	
}
