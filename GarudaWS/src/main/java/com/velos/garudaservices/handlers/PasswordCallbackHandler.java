package com.velos.garudaservices.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.commons.lang.StringUtils;
import org.apache.ws.security.WSPasswordCallback;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.context.SecurityContextHolder;

import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.web.grpRights.GrpRightsJB;
import com.velos.eres.web.user.UserJB;
import com.velos.garudaservices.security.ApplicationRightsConstant;
import com.velos.garudaservices.security.WsAuthenticationManager;
import com.velos.garudaservices.service.VelosService;
import com.velos.ordercomponent.business.pojoobjects.GrpsPojo;
/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
public class PasswordCallbackHandler implements CallbackHandler,ApplicationRightsConstant {
	
	private VelosService velosService;	
	
	private Properties properties = new Properties();
	
	private Map<String,List<String>> roleRightsMap = new HashMap<String,List<String>>();
	
	public void setVelosService(VelosService velosService) {
		this.velosService = velosService;
	}	
	
	public void setRoleRightsMap(Map<String, List<String>> roleRightsMap) {
		this.roleRightsMap = roleRightsMap;
	}	
	
	private void loadProps(){
		try{
		  properties.load(this.getClass().getClassLoader().getResourceAsStream("webservice_rights.properties"));
		  if(!properties.isEmpty()){
			  for(Object o : properties.keySet()){
				  String key = o.toString();
				  Object values = properties.get(key);
				  List<String> groupAccess = null;
				  if(values!=null){
					  String[] groups = values.toString().split(",");
					  groupAccess = Arrays.asList(groups);
				  }
				  key = "ROLE_" + key;
				  roleRightsMap.put(key, groupAccess);
			  }
		  }
		  
		}catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {
		UserJB userJB = new UserJB();
		UserBean user = null;
		GrpRightsJB rights = null;
		/*
		 * below code commented for JMS
		 */
		/*Sender sender = new Sender();
		sender.send();
		Reciever reciever = new Reciever();
		reciever.recieve();*/
		if(!(callbacks[0] instanceof WSPasswordCallback)){
			throw new IOException("Unsupported Authentication Type");
		}
		WSPasswordCallback callback = (WSPasswordCallback)callbacks[0];
		if(callback.getUsage()!=WSPasswordCallback.USERNAME_TOKEN_UNKNOWN)
			throw new IOException("Unsupported password type");
		try{
		AuthenticationManager am = new WsAuthenticationManager();
		String username = callback.getIdentifier();
		String password = callback.getPassword();
		user = userJB.validateUser(username, password);
		if(user==null){
			throw new IOException("User not authenticated");
		}
		List<GrpsPojo> grps = velosService.getUserGroups(Long.parseLong(user.getUserSiteId()),Long.parseLong(user.getUserId().toString()));
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		GrantedAuthority authority = null;
		loadProps();
		/*
		 rights = getGroupRights(grps, Long.parseLong(user.getUserSiteId()),Long.parseLong(user.getUserId().toString()));
		 String s = "ROLE_";		
		 if(rights!=null){
			if(rights.getFtrRightsByValue(RIGHTS_UPDATE_CBB_GUID)!=null && Integer.parseInt(rights.getFtrRightsByValue(RIGHTS_UPDATE_CBB_GUID))==6){
				authority = new GrantedAuthorityImpl(roleRightsMap.get(RIGHTS_UPDATE_CBB_GUID));
				authorities.add(authority);
			}
		}*/
		if(grps!=null){
			for(Map.Entry<String, List<String>> mapEntry : roleRightsMap.entrySet()){
				Boolean flag = false;
				for(String access : mapEntry.getValue()){
					for(GrpsPojo grp : grps){
						if(StringUtils.equalsIgnoreCase(access,grp.getGrpName())){
							authority = new GrantedAuthorityImpl(mapEntry.getKey());
							authorities.add(authority);
							flag = true;
							break;
						}
					}
					if(flag)
						break;
				}
			}
		}
		Authentication request = new UsernamePasswordAuthenticationToken(username, password,authorities);
		/*Authenticathttp://172.16.2.60:8080/GarudaWebservices/studentInfoService?wsdlion request = null;
		if(username.equalsIgnoreCase("irshad")){
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			authorities.add(new GrantedAuthorityImpl("ROLE_USER"));
			request = new UsernamePasswordAuthenticationToken(username, password, authorities);
		}else{
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			authorities.add(new GrantedAuthorityImpl("ROLE_IS_CDR_USER"));
			request = new UsernamePasswordAuthenticationToken(username, password, authorities);
		}*/		
		SecurityContextHolder.getContext().setAuthentication(request);
		}catch (AuthenticationException e) {
			throw new IOException("Authentication Failed");
		}
        
	}
	
	public GrpRightsJB getGroupRights(List<GrpsPojo> grpList,Long userId,Long siteId){
		GrpRightsJB grpRights = new GrpRightsJB();
		if(grpList!=null && grpList.size()==1){
			 grpRights.setId(Integer.parseInt(grpList.get(0).getPkGrp().toString()));
			 grpRights.getGrpRightsDetails();				 				
		 }else if(grpList!=null && grpList.size()>1){
			 grpRights.getMaxGrpRights(userId,siteId);				 
		 }
		return grpRights;
	}

}