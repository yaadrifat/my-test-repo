package com.velos.garudaservices.daos;

import java.util.List;

import com.velos.garudaservices.model.UpdateCBB;
import com.velos.ordercomponent.business.pojoobjects.GrpsPojo;
/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
public interface CommonDaoI {
	
	public List<GrpsPojo> getUserGroups(Long siteId,Long userId);
	
	public Long getEntityData(String entityName, String entityField , String entityValue);
	
	public Long getEntityData(String entityName, String entityField , Long entityValue);

	public Long getEntityData(String entityName, String entityField , String entityValue, String entityFieldA , String entityValueA, String constraint);
	
	public void updateCBUGUID(UpdateCBB cbb);
}
