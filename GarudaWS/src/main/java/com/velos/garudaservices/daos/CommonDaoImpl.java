package com.velos.garudaservices.daos;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.velos.garudaservices.domain.Grps;
import com.velos.garudaservices.model.UpdateCBB;
import com.velos.ordercomponent.business.pojoobjects.GrpsPojo;
import com.velos.ordercomponent.business.util.ObjectTransfer;
/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
public class CommonDaoImpl implements CommonDaoI {
	
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	private Session session = null;
	
	public List<GrpsPojo> getUserGroups(Long siteId,Long userId){
	    	List<Grps> grpsDomain= new ArrayList<Grps>();
	    	List<GrpsPojo> grpsPojos= new ArrayList<GrpsPojo>();
	    	Grps grps= new Grps();
	    	GrpsPojo grpsPojo = null;
		    try{		    		
		      session = sessionFactory.getCurrentSession();
			  session.beginTransaction();
				
		      String sql = "select {g.*} from er_grps g left join er_usrsite_grp u on u.FK_GRP_ID=g.PK_GRP left join er_usersite eu on eu.PK_USERSITE=u.FK_USER_SITE where u.fk_user = ? and eu.fk_site = ?";
		      SQLQuery query = session.createSQLQuery(sql);
		      query.addEntity("g", Grps.class);
		      query.setLong(0,userId);
		      query.setLong(1, siteId);
		      grpsDomain = query.list();
				for(Grps g : grpsDomain){
					grpsPojo = new GrpsPojo();
					grpsPojo=(GrpsPojo)ObjectTransfer.transferObjects(g, grpsPojo);
					grpsPojos.add(grpsPojo);	
				}
		
		    }catch (Exception e) {
		    		e.printStackTrace();
		    }finally{
		    		if(session.isOpen()){
		    			session.close();
		    		}
		    }
	    	return grpsPojos; 	
	    }
	 
	 public Long getEntityData(String entityName, String entityField , String entityValue){
			Long count=null;
			try {		
				session = sessionFactory.getCurrentSession();
				session.beginTransaction();
				
				String sql = "select count(*) From "+entityName+" where Replace("+entityField+",'-','') ='"+entityValue+"' ";
				count = (Long)session.createQuery(sql).uniqueResult();
				
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (session.isOpen()) {
					session.close();
				}
			}
			
			return count; 
		}
	 
	 public Long getEntityData(String entityName, String entityField , Long entityValue){
			Long count=null;
			try {		
				session = sessionFactory.getCurrentSession();
				session.beginTransaction();
				
				String sql = "select count(*) From "+entityName+" where "+entityField+" ="+entityValue;
				count = (Long)session.createQuery(sql).uniqueResult();
				
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (session.isOpen()) {
					session.close();
				}
			}
			
			return count; 
	}

	 
	 public Long getEntityData(String entityName, String entityField , String entityValue, String entityFieldA , String entityValueA, String constraint){
		 Long count=null;
			try {		
				session = sessionFactory.getCurrentSession();
				session.beginTransaction();
				
				String sql = "select count(*) From "+entityName+" where "+entityField+" ='"+entityValue+"'";
				if(StringUtils.equalsIgnoreCase(constraint, "notequals")){
					sql += " and "+ entityFieldA +" <>'"+entityValueA+"'";
				}else{
					sql += " and "+ entityFieldA +" ='"+entityValueA+"'";
				}
				count = (Long)session.createQuery(sql).uniqueResult();
				
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (session.isOpen()) {
					session.close();
				}
			}
			
			return count;
	 }

	@Override
	public void updateCBUGUID(UpdateCBB cbb) {
		try {		
			session = sessionFactory.getCurrentSession();
			session.beginTransaction();
			
			String sql = "Update Site set guid='"+cbb.getCBBGUID()+"' where siteIdentifier='"+cbb.getCBBID()+"'";
			session.createQuery(sql).executeUpdate();
			session.beginTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session.isOpen()) {
				session.close();
			}
		}
	}
}
