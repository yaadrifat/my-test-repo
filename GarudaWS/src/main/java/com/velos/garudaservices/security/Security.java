package com.velos.garudaservices.security;

import java.util.ArrayList;
import java.util.List;
/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
public abstract class Security {

	private List<String> userRights;
	private List<String> organizationRights;
	public List<String> getUserRights() {
		return userRights;
	}
	public List<String> getOrganizationRights() {
		return organizationRights;
	}
	
	
	public final void setRights(){
		userRights = new ArrayList<String>();
		userRights.add("Add_CBB");
		organizationRights = new ArrayList<String>();
		organizationRights.add("ROLE_IS_CDR_USER");
	}
	
}
