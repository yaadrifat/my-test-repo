package com.velos.garudaservices.security;
/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
public interface ApplicationRightsConstant {
	/*****Roles****/
	String ROLE_UPDATE_CBB_GUID = "ROLE_UPDATE_CBB_GUID";

	
	
	/******Rights******/
	String RIGHTS_UPDATE_CBB_GUID = "CB_CBBPROFILE";
}
