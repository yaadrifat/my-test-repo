package com.velos.garudaservices.security;
/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ElementType.TYPE,ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy=CrossFieldValidator.class)
@Documented
public @interface CrossFieldData {

	String message() default "{com.velos.garudaservices.security.crossfielddata}";

	 Class<?>[] groups() default {};

	 Class<? extends Payload>[] payload() default {};
	 
	 String entity();
	 
	 String[] fields();
	 
	 String crossFieldValidation();
}
