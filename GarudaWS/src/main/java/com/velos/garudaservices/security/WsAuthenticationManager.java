package com.velos.garudaservices.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
public class WsAuthenticationManager implements AuthenticationManager {

	public static List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
	static {
		authorities.add(new GrantedAuthorityImpl("ROLE_USER"));
	}
	public Authentication authenticate(Authentication auth)
			throws AuthenticationException {
		if(auth.getName().equals(auth.getCredentials())){
			return new UsernamePasswordAuthenticationToken(auth.getName(), auth.getCredentials(),authorities);
		}
		throw new BadCredentialsException("Bad Credentials");
	}

}
