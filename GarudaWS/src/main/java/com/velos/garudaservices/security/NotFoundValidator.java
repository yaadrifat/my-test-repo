package com.velos.garudaservices.security;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.velos.garudaservices.service.VelosService;
/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
public class NotFoundValidator implements ConstraintValidator<NotFoundData,Object> {

	private VelosService velosService;	
	
	private String entity;
	
	private String field;
	
	private String message;
	
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setVelosService(VelosService velosService) {
		this.velosService = velosService;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	@Override
	public void initialize(NotFoundData obj) {
		this.entity = obj.entity();
		this.field = obj.field();
		this.message = obj.message();
	}

	@Override
	public boolean isValid(Object object, ConstraintValidatorContext constraintContext) {
		// TODO Auto-generated method stub
		if(object==null)
			return true;
		String entity = getEntity();
		String field = getField();
		Long count = null;
		Boolean flag = false;
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-context.xml");
		velosService = (VelosService)ctx.getBean("service");
		if(object.getClass()==Long.class && !StringUtils.isEmpty(entity) && !StringUtils.isEmpty(field))
		{
			count = velosService.getEntityData(entity, field,(Long)object);
		}else if(object.getClass()==String.class && !StringUtils.isEmpty(entity) && !StringUtils.isEmpty(field)){
			count = velosService.getEntityData(entity, field, ((String)object).replaceAll("-", ""));
		}
		flag = (count!=null && count==1)?true:false;
		if(!flag) {
			constraintContext.disableDefaultConstraintViolation();
            String pattern = count==0 || count==null?" record is not found ":" multiple records found ";
            message = message.replaceAll("\\?", pattern);
            constraintContext.buildConstraintViolationWithTemplate(message).addConstraintViolation();
        }
		return flag;
	}

	

}
