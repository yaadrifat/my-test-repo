package com.velos.garudaservices.security;
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({METHOD,FIELD,ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy=NotFoundValidator.class)
@Documented
/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
public @interface NotFoundData {

	 String message() default "{com.velos.garudaservices.security.notfounddata}";

	 Class<?>[] groups() default {};

	 Class<? extends Payload>[] payload() default {};
	 
	 String entity();
	 
	 String field();
}
