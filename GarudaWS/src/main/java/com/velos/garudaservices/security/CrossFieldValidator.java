package com.velos.garudaservices.security;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.velos.garudaservices.model.UpdateCBB;
import com.velos.garudaservices.service.VelosService;
/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
public class CrossFieldValidator implements ConstraintValidator<CrossFieldData, Object> {
	
	private static final String CROSS_FIELD_GUID_VALIDATION_FOR_CBB_CONTSANT = "CBBGUIDCROSSFIELD";

    private VelosService velosService;	
	
	private String entity;
	
	private String[] fields;
	
	private String message;
	
	private String crossFieldValidation;	
	
	public VelosService getVelosService() {
		return velosService;
	}

	public void setVelosService(VelosService velosService) {
		this.velosService = velosService;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String[] getFields() {
		return fields;
	}

	public void setFields(String[] fields) {
		this.fields = fields;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCrossFieldValidation() {
		return crossFieldValidation;
	}

	public void setCrossFieldValidation(String crossFieldValidation) {
		this.crossFieldValidation = crossFieldValidation;
	}

	@Override
	public void initialize(CrossFieldData crossFieldData) {
		// TODO Auto-generated method stub
		this.entity = crossFieldData.entity();
		this.fields = crossFieldData.fields();
		this.message = crossFieldData.message();
		this.crossFieldValidation = crossFieldData.crossFieldValidation();
	}

	@Override
	public boolean isValid(Object obj, ConstraintValidatorContext constraintContext) {
		if(obj==null)
			return true;
		Long count = null;
		Boolean flag = false;
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-context.xml");
		velosService = (VelosService)ctx.getBean("service");
		if(obj.getClass()==UpdateCBB.class && !StringUtils.isEmpty(getEntity()) && !StringUtils.isEmpty(getMessage())
				&& getFields()!=null && getFields().length>0 && !StringUtils.isEmpty(getCrossFieldValidation())){
			if(StringUtils.equalsIgnoreCase(getCrossFieldValidation(), CROSS_FIELD_GUID_VALIDATION_FOR_CBB_CONTSANT)){
				count = velosService.getEntityData(entity, getFields()[0],((UpdateCBB)obj).getCBBGUID(),getFields()[1],((UpdateCBB)obj).getCBBID(),"notequals");
				flag = (count>0)?false:true;
			}
		}
		if(!flag) {
			constraintContext.disableDefaultConstraintViolation();
            constraintContext.buildConstraintViolationWithTemplate(getMessage()).addConstraintViolation();
        }
		return flag;
	}

}
