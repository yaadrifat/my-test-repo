package com.velos.garudaservices.security;

import java.util.Collection;

import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
public class SecurityAccessManager extends Security implements AccessDecisionVoter{

	public boolean supports(ConfigAttribute arg0) {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return true;
	}

	public int vote(Authentication authentication, Object object,
			Collection<ConfigAttribute> attributes) {
		// TODO Auto-generated method stub
		//setRights();
		Boolean flag = false;
		for(GrantedAuthority c : authentication.getAuthorities()){
			if(attributes!=null){
				for(ConfigAttribute att : attributes){
					if(c.getAuthority().equalsIgnoreCase(att.getAttribute()))
						flag = true;
				}
			}
		}
		if(flag)
			return ACCESS_GRANTED;
		else
		    return ACCESS_DENIED;
	}

	

}
