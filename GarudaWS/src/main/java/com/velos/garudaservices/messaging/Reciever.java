package com.velos.garudaservices.messaging;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
public class Reciever {

	public void recieve(){
		try{
			Connection connection = MessagingUtility.getConnection();
			Session session = MessagingUtility.getSession();
			Destination destination = MessagingUtility.getDestination("/queue/DanishQueue");
			MessageConsumer consumer = session.createConsumer(destination);
			consumer.setMessageListener(new RecieverListner());
			connection.start();			
			try {
			    Thread.sleep(3000);
			} catch (InterruptedException e) {
			    e.printStackTrace();
			}
			consumer.close();
			session.close();
			connection.close();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
