package com.velos.garudaservices.messaging;

import java.util.Hashtable;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
public class MessagingUtility {

	private static ConnectionFactory connectionFactory = null;
	private static Connection connection = null;
	private static Session session = null;
	private static MessageConsumer consumer = null;
	private static MessageProducer producer = null;
	private static Context context = null;
	private static Destination destination = null;
	private static Hashtable environment = new Hashtable();
	private static final String connectionFactoryName = "ConnectionFactory";
	
	static{
		environment.put("java.naming.provider.url", "localhost:1099");
		environment.put("java.naming.factory.initial","org.jboss.naming.NamingContextFactory");
		environment.put("java.naming.factory.url.pkgs","org.jboss.naming:org.jnp.interfaces");
	}
	
	public static ConnectionFactory getConnectionFactory(){
		try{
			//context = new InitialContext(environment);
			context = new InitialContext();
			connectionFactory = (ConnectionFactory) context.lookup(connectionFactoryName);
		} catch (NamingException e) {
			e.printStackTrace();
		}finally{
			
		}
		return connectionFactory;
	}
	
	public static Connection getConnection(){
		ConnectionFactory connectionFactory = getConnectionFactory();
		try {
			connection = connectionFactory.createConnection();
		} catch (JMSException e) {
			e.printStackTrace();
		}
		return connection;
	}
	
	public static Session getSession(){
		Connection connection = getConnection();
		try {
			session = connection.createSession(false,Session.AUTO_ACKNOWLEDGE);
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return session;
	}
	
	public static void closeConnection(Connection connection){
		try {
			connection.stop();
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void startConnection(Connection connection){
		try {
			connection.start();
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void stopSession(Session session){
	  try {
		session.close();
	  } catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	  }	
	}
	
	public static Destination getDestination(String destinationName){
		try {
			//context = new InitialContext(environment);
			context = new InitialContext();
			destination = (Destination) context.lookup(destinationName);			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return destination;
	}
}
