package com.velos.garudaservices.messaging;

import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;

/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
public class Sender {
    
	
	public void send(){
		try{
			Session session = MessagingUtility.getSession();
			Destination destination = MessagingUtility.getDestination("/queue/DanishQueue");
			MessageProducer producer = session.createProducer(destination);
			Message message = session.createTextMessage("Danish message");
			producer.send(message);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}
