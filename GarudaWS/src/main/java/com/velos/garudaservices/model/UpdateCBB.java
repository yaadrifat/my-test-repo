package com.velos.garudaservices.model;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.velos.garudaservices.security.CrossFieldData;
import com.velos.garudaservices.security.NotFoundData;
/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
@XmlRootElement(name="UpdateCBBGUID")
@CrossFieldData(entity="Site",fields={"guid","siteIdentifier"},crossFieldValidation = "CBBGUIDCROSSFIELD"
	,message="CBB GUID is not updated because CBBGUID already exists for another CBB.")
public class UpdateCBB {
	
	private String AppUserName;	
	private String CBBName;
	private String CBBID;	
	private String CBBGUID;
	
	@Size(min=1,max=32 ,message="CBB GUID is not updated because Application User Name is blank.")
	public String getAppUserName() {
		return AppUserName;
	}
	public void setAppUserName(String appUserName) {
		AppUserName = appUserName;
	}
	@Size(min=1,max=32 ,message="CBB GUID is not updated because both CBB Name and ID are blank.")
	@NotFoundData(entity="Site",field="siteName",message="CBB GUID is not updated because CBB ? for entered CBB Name .")
	public String getCBBName() {
		return CBBName;
	}
	public void setCBBName(String cBBName) {
		CBBName = cBBName;
	}
	@Size(min=1,max=32 ,message="CBB GUID is not updated because both CBB Name and ID are blank.")
	@NotFoundData(entity="Site",field="siteIdentifier",message="CBB GUID is not updated because CBB ? for entered CBB ID .")
	public String getCBBID() {
		return CBBID;
	}
	public void setCBBID(String cBBID) {
		CBBID = cBBID;
	}
	@Size(min=32,max=32 ,message="CBB GUID is not updated because CBB GUID is not equal to 32 characters.")
	@Pattern(regexp="([A-Za-z0-9]{32})" ,message="CBB GUID is not updated because CBB GUID value has some invalid characters(Other than A-F and 0-9).")
	public String getCBBGUID() {
		return CBBGUID;
	}
	public void setCBBGUID(String cBBGUID) {
		CBBGUID = cBBGUID;
	}
	
	

}
