package com.velos.garudaservices.domain;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
@Entity
@Table(name="ER_GRPS")
@SequenceGenerator(sequenceName="SEQ_ER_GRPS",name="SEQ_ER_GRPS",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class Grps extends Auditable{
	
	private Long pkGrp;
	private String grpName;
	private String grpDesc;
	private Long fkAccount;
	private String grpRights;
	private String grpSupUsrRights;
	private Long grpSupUsrFlag;
	private Long grpHidden;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_ER_GRPS")
	@Column(name="PK_GRP")
	public Long getPkGrp() {
		return pkGrp;
	}
	public void setPkGrp(Long pkGrp) {
		this.pkGrp = pkGrp;
	}
	@Column(name="GRP_NAME")
	public String getGrpName() {
		return grpName;
	}
	public void setGrpName(String grpName) {
		this.grpName = grpName;
	}
	@Column(name="GRP_DESC")
	public String getGrpDesc() {
		return grpDesc;
	}
	public void setGrpDesc(String grpDesc) {
		this.grpDesc = grpDesc;
	}
	@Column(name="FK_ACCOUNT")
	public Long getFkAccount() {
		return fkAccount;
	}
	public void setFkAccount(Long fkAccount) {
		this.fkAccount = fkAccount;
	}
	@Column(name="GRP_RIGHTS")
	public String getGrpRights() {
		return grpRights;
	}
	public void setGrpRights(String grpRights) {
		this.grpRights = grpRights;
	}
	
	@Column(name="GRP_SUPUSR_RIGHTS")
	public String getGrpSupUsrRights() {
		return grpSupUsrRights;
	}
	public void setGrpSupUsrRights(String grpSupUsrRights) {
		this.grpSupUsrRights = grpSupUsrRights;
	}
	
	@Column(name="GRP_SUPUSR_FLAG")
	public Long getGrpSupUsrFlag() {
		return grpSupUsrFlag;
	}
	public void setGrpSupUsrFlag(Long grpSupUsrFlag) {
		this.grpSupUsrFlag = grpSupUsrFlag;
	}
	@Column(name="GRP_HIDDEN")
	public Long getGrpHidden() {
		return grpHidden;
	}
	public void setGrpHidden(Long grpHidden) {
		this.grpHidden = grpHidden;
	}
	}
	
