package com.velos.garudaservices.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.velos.ordercomponent.business.util.VelosUtil;
/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
@Entity
@Table(name = "er_site")
@SequenceGenerator(sequenceName="SEQ_ER_SITE",name="SEQ_ER_SITE",allocationSize=1)
@org.hibernate.annotations.Entity(dynamicInsert = true,dynamicUpdate=true)
public class Site extends Auditable {

   private static final long serialVersionUID = 3834024775256652086L;

   private Long siteId;
   private Long siteCodelstType;
   private Long siteAccountId;
   private Long sitePerAdd;
   private String siteName;
   private String siteInfo;
   private String siteNotes;
   private String siteStatus;
   private Long siteParent;
   private String siteIdentifier;
   private Long siteSequence;    
   private String siteHidden; 
   private String siteAltId;
   private String siteRestrict; 
   private String guid;
	
   
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="SEQ_ER_SITE")
	@Column(name="PK_SITE")
	public Long getSiteId() {
		return siteId;
	}
	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}
	@Column(name="FK_CODELST_TYPE")
	public Long getSiteCodelstType() {
		return siteCodelstType;
	}
	public void setSiteCodelstType(Long siteCodelstType) {
		this.siteCodelstType = siteCodelstType;
	}
	@Column(name="FK_ACCOUNT")
	public Long getSiteAccountId() {
		return siteAccountId;
	}
	public void setSiteAccountId(Long siteAccountId) {
		this.siteAccountId = siteAccountId;
	}
	@Column(name="FK_PERADD")
	public Long getSitePerAdd() {
		return sitePerAdd;
	}
	public void setSitePerAdd(Long sitePerAdd) {
		this.sitePerAdd = sitePerAdd;
	}
	@Column(name="SITE_NAME")
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	@Column(name="SITE_INFO")
	public String getSiteInfo() {
		return siteInfo;
	}
	public void setSiteInfo(String siteInfo) {
		this.siteInfo = siteInfo;
	}
	@Column(name="SITE_NOTES")
	public String getSiteNotes() {
		return siteNotes;
	}
	public void setSiteNotes(String siteNotes) {
		this.siteNotes = siteNotes;
	}
	@Column(name="SITE_STAT")
	public String getSiteStatus() {
		return siteStatus;
	}
	public void setSiteStatus(String siteStatus) {
		this.siteStatus = siteStatus;
	}
	@Column(name="SITE_PARENT")
	public Long getSiteParent() {
		return siteParent;
	}
	public void setSiteParent(Long siteParent) {
		this.siteParent = siteParent;
	}
	@Column(name="SITE_ID")
	public String getSiteIdentifier() {
		return siteIdentifier;
	}
	public void setSiteIdentifier(String siteIdentifier) {
		this.siteIdentifier = siteIdentifier;
	}
	@Column(name="SITE_SEQ")
	public Long getSiteSequence() {
		return siteSequence;
	}
	public void setSiteSequence(Long siteSequence) {
		this.siteSequence = siteSequence;
	}
	@Column(name="SITE_HIDDEN")
	public String getSiteHidden() {
		return siteHidden;
	}
	public void setSiteHidden(String siteHidden) {
		this.siteHidden = siteHidden;
	}
	
	@Column(name="SITE_RESTRICT")
	public String getSiteRestrict() {
		return siteRestrict;
	}
	public void setSiteRestrict(String siteRestrict) {
		this.siteRestrict = siteRestrict;
	}
	@Column(name="SITE_ALTID")
	public String getSiteAltId() {
		return siteAltId;
	}
	public void setSiteAltId(String siteAltId) {
		this.siteAltId = siteAltId;
	}
	@Column(name="GUID")
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	
	
}