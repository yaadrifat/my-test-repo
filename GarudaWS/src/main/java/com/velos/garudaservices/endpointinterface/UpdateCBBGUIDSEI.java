package com.velos.garudaservices.endpointinterface;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.velos.garudaservices.exception.Response;
import com.velos.garudaservices.model.UpdateCBB;

/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
@WebService
public interface UpdateCBBGUIDSEI {

	@WebMethod(operationName="updateCBUGUID")
	public Response updateCBBGUID(@WebParam(name ="UpdateCBB")UpdateCBB updateCBB);		
	
}
