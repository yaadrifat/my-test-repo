package com.velos.garudaservices.endpointinterface;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.velos.garudaservices.exception.Response;
import com.velos.garudaservices.model.UpdateCBB;

/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
@WebService
public interface StudentInfoSEI {

	@WebMethod(operationName="getStudentName")
	public String getStudentName(@WebParam(name="StudentName")String name);
	
	@WebMethod(operationName="getIssues")	
	public List<String> getIssues();
	
	@WebMethod(operationName="updateCBUGUID")
	public Response updateCBBGUID(@WebParam(name ="UpdateCBB")UpdateCBB updateCBB);		
	
}
