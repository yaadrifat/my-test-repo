package com.velos.garudaservices.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.velos.garudaservices.daos.CommonDaoI;
import com.velos.garudaservices.model.UpdateCBB;
import com.velos.ordercomponent.business.pojoobjects.GrpsPojo;


/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
@Transactional(readOnly=true)
public class VelosServiceImpl implements VelosService {
	
	private CommonDaoI commonDao;

	public void setCommonDao(CommonDaoI commonDao) {
		this.commonDao = commonDao;
	}
	
	public List<GrpsPojo> getUserGroups(Long siteId,Long userId){
		return commonDao.getUserGroups(siteId, userId);
	}

	@Transactional(readOnly=false)
	public void updateCBUGUID(UpdateCBB cbb) {		
		commonDao.updateCBUGUID(cbb);
	}

	@Override
	public Long getEntityData(String entityName, String entityField,
			String entityValue) {
		return commonDao.getEntityData(entityName, entityField, entityValue);
	}

	@Override
	public Long getEntityData(String entityName, String entityField,
			Long entityValue) {
		return commonDao.getEntityData(entityName, entityField, entityValue);
	}

	@Override
	public Long getEntityData(String entityName, String entityField,
			String entityValue, String entityFieldA, String entityValueA,
			String constraint) {
		return commonDao.getEntityData(entityName, entityField, entityValue, entityFieldA, entityValueA, constraint);
	}
	
		
}
