package com.velos.garudaservices.webservice;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.jws.WebService;
import javax.validation.Constraint;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;

import com.velos.garudaservices.endpointinterface.StudentInfoSEI;
import com.velos.garudaservices.exception.Response;
import com.velos.garudaservices.model.UpdateCBB;
import com.velos.garudaservices.service.VelosService;
import com.velos.ordercomponent.business.domain.CBB;


/**
 * @author Mohiuddin 
 * @version 1.0
 * @since 4 April,2013
 * 
 */
@WebService(endpointInterface="com.velos.garudaservices.endpointinterface.StudentInfoSEI",serviceName="StudentInfo")
public class StudentInfoWS implements StudentInfoSEI {

	private VelosService velosService;	
	
	
	public void setVelosService(VelosService velosService) {
		this.velosService = velosService;
	}

	public String getStudentName(String name) {
		return name;
	}
	
	public List<String> getIssues(){
		List<String> list = new ArrayList<String>();
		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		return list;
	}

	@Override
	public Response updateCBBGUID(UpdateCBB updateCBB) {
		// TODO Auto-generated method stub
		Response response = new Response();
		List<String> issues = new ArrayList<String>();
		Validator validation = Validation.buildDefaultValidatorFactory().getValidator();
		Set<ConstraintViolation<UpdateCBB>> updateCbbGuidValidator = validation.validate(updateCBB);
		if(updateCbbGuidValidator!=null && updateCbbGuidValidator.size()>0){
			for(ConstraintViolation v : updateCbbGuidValidator){
				issues.add(v.getMessage());
			}
		}
		if(issues!=null && issues.size()>0)
		{
			response.setIssues(issues);			
		}else{
		 CBB cbb = new CBB();
		// velosService.updateCBUGUID(cbb);
		}
		return response;
	}
	
}
