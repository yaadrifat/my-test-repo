CREATE OR REPLACE FUNCTION        "F_GET_ENROLL_FORWARD_PAGE" (p_enroll_fwd VARCHAR2)
RETURN VARCHAR
IS
v_desc VARCHAR2(4000);
vpos NUMBER;
v_code VARCHAR2(200);
/* Returns the enrollforward page from study settings value*/
BEGIN
	vpos := instr(p_enroll_fwd,'[VELCHAR]');
	IF (vpos > 0) THEN -- core forwwarding pages
		v_code 	:= substr(p_enroll_fwd,(vpos+9));

		select decode(v_code,'ADV','Adverse Events','LAB','Labs','PATSTAT','Patient Status','DEMO','Demographics')
		into v_desc
		from dual;

	ELSE -- its a form or its null
		BEGIN
			SELECT form_name
			INTO v_desc FROM ER_FORMLIB
			WHERE pk_formlib = p_enroll_fwd;
		EXCEPTION WHEN NO_DATA_FOUND THEN
			v_desc := '';
		END;

	END IF;
	 RETURN v_desc ;
END ;
/


