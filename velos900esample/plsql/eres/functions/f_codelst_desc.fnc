CREATE OR REPLACE FUNCTION        "F_CODELST_DESC" (p_id VARCHAR2)
RETURN VARCHAR2
IS
  v_codelst_desc VARCHAR2(200);
BEGIN
	 SELECT codelst_desc INTO v_codelst_desc FROM ER_CODELST WHERE pk_codelst = p_id;
  RETURN v_codelst_desc ;
END ;
/


