CREATE OR REPLACE FUNCTION      f_getLatestPatientStudyStatus(p_study NUMBER,p_per in number) RETURN NUMBER
  IS
  v_stat NUMBER;
  BEGIN
   BEGIN

    SELECT fk_codelst_stat
    INTO     v_stat
    FROM ER_PATSTUDYSTAT WHERE pk_patstudystat =  (SELECT   MAX (pk_patstudystat)
                          FROM   er_patstudystat o
                         WHERE   o.fk_study = p_study and o.fk_per = p_per and o.patstudystat_date =
                                                                        (SELECT   MAX (i.patstudystat_date)
                                          FROM   er_patstudystat i
                                         WHERE   i.fk_per = o.fk_per
                                                 AND i.fk_study = o.fk_study
                                                 ));

         EXCEPTION WHEN NO_DATA_FOUND THEN

          v_stat := 0;

     END;

     RETURN v_stat ;
  END;
/


