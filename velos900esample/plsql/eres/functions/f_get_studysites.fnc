CREATE OR REPLACE FUNCTION        "F_GET_STUDYSITES" (p_study NUMBER)
RETURN VARCHAR2
AS
v_retval VARCHAR2(32000);

BEGIN

	 select Pkg_Util.f_join(cursor(SELECT site_name FROM ER_STUDYSITES , ER_SITE WHERE fk_study = p_study  and pk_site = fk_site
	 order by site_name ),',')
	 into v_retval
	 from dual;

	return v_retval;
END ;
/


