CREATE OR REPLACE FUNCTION        "F_REP_EMSI" (p_pk_person NUMBER,p_param4 VARCHAR)
RETURN NUMBER
IS
v_count NUMBER;
BEGIN

IF p_param4 = '1 Month' THEN
   SELECT COUNT(*) INTO v_count FROM er_formslinear WHERE fk_form = 434 AND ID = p_pk_person;
ELSE
	SELECT COUNT(*) INTO v_count FROM er_formslinear WHERE fk_form = 588 AND ID = p_pk_person;
END IF;

RETURN v_count;
END ;
/


