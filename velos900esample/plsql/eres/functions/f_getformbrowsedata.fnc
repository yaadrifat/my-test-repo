CREATE OR REPLACE FUNCTION        "F_GETFORMBROWSEDATA" (p_form NUMBER, p_filledform NUMBER)
RETURN VARCHAR
IS
v_selectcol VARCHAR2(1000);
v_counter NUMBER := 0;
v_retval VARCHAR2(4000);
BEGIN
  FOR i IN (SELECT mp_mapcolname, mp_dispname,mp_flddatatype FROM ER_MAPFORM WHERE fk_form = p_form AND mp_browser = 1 ORDER BY mp_sequence)
  LOOP
  	  v_counter := v_counter + 1;
	  IF v_counter = 1 THEN
--  	  	 v_selectcol := v_selectcol || '''' ||i.mp_dispname || ' : '' || ' || 'substr(' || i.mp_mapcolname || ',1,instr(' ||  i.mp_mapcolname || ',[VELSEP1]))' || '|| ''X@@@X''' ;
			 IF i.mp_flddatatype = 'MC' OR  i.mp_flddatatype = 'MR' OR  i.mp_flddatatype = 'MD' THEN
  	  		 	 v_selectcol := v_selectcol || '''' ||i.mp_dispname || ' : '' || substr(' || i.mp_mapcolname || ',1,instr('|| i.mp_mapcolname || ',''[VELSEP1'')-1) || ''X@@@X''' ;
			ELSE
				 v_selectcol := v_selectcol || '''' ||i.mp_dispname || ' : '' || ' || i.mp_mapcolname || ' || ''X@@@X''' ;
			END IF;
	  ELSE
			 IF i.mp_flddatatype = 'MC' OR  i.mp_flddatatype = 'MR' OR  i.mp_flddatatype = 'MD' THEN
  	  		 	 v_selectcol := v_selectcol || ' || ''' || i.mp_dispname || ' : '' || substr(' || i.mp_mapcolname || ',1,instr('|| i.mp_mapcolname || ',''[VELSEP1'')-1) || ''X@@@X''' ;
			ELSE
				 v_selectcol := v_selectcol || ' || ''' ||i.mp_dispname || ' : '' || ' || i.mp_mapcolname || ' || ''X@@@X''' ;
			END IF;
	  END IF;
  END LOOP;
  v_selectcol := 'select ' || v_selectcol || ' from er_formslinear where form_type = ''P'' and fk_filledform = :1';
  EXECUTE IMMEDIATE v_selectcol INTO v_retval USING p_filledform;
  RETURN v_retval ;
END;
/


CREATE SYNONYM ESCH.F_GETFORMBROWSEDATA FOR F_GETFORMBROWSEDATA;


CREATE SYNONYM EPAT.F_GETFORMBROWSEDATA FOR F_GETFORMBROWSEDATA;


GRANT EXECUTE, DEBUG ON F_GETFORMBROWSEDATA TO EPAT;

GRANT EXECUTE, DEBUG ON F_GETFORMBROWSEDATA TO ESCH;

