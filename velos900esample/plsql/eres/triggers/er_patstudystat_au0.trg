CREATE OR REPLACE TRIGGER "ER_PATSTUDYSTAT_AU0"
  AFTER UPDATE
  ON er_patstudystat
  FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := getuser(:NEW.last_modified_by);



  audit_trail.record_transaction
    (raid, 'ER_PATSTUDYSTAT', :OLD.rid, 'U', usr);

  IF NVL(:OLD.pk_patstudystat,0) !=
     NVL(:NEW.pk_patstudystat,0) THEN
     audit_trail.column_update
       (raid, 'PK_PATSTUDYSTAT',
       :OLD.pk_patstudystat, :NEW.pk_patstudystat);
  END IF;
  IF NVL(:OLD.fk_codelst_stat,0) !=
     NVL(:NEW.fk_codelst_stat,0) THEN
    BEGIN
	SELECT trim(codelst_desc) INTO old_codetype
	  FROM er_codelst
       WHERE pk_codelst =  :OLD.fk_codelst_stat 	;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_codetype := NULL;
    END ;
    BEGIN
     SELECT trim(codelst_desc) INTO new_codetype
	 FROM er_codelst
      WHERE pk_codelst =  :NEW.fk_codelst_stat ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_codetype := NULL;
    END ;

     audit_trail.column_update
       (raid, 'FK_CODELST_STAT',
       old_codetype, new_codetype);

  END IF;
  IF NVL(:OLD.fk_per,0) !=
     NVL(:NEW.fk_per,0) THEN
     audit_trail.column_update
       (raid, 'FK_PER',
       :OLD.fk_per, :NEW.fk_per);
  END IF;
  IF NVL(:OLD.fk_study,0) !=
     NVL(:NEW.fk_study,0) THEN
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :OLD.fk_study, :NEW.fk_study);
  END IF;
  IF NVL(:OLD.patstudystat_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.patstudystat_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'PATSTUDYSTAT_DATE',
       to_char(:OLD.patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.patstudystat_endt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.patstudystat_endt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'PATSTUDYSTAT_ENDT',
       to_char(:OLD.patstudystat_endt,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.patstudystat_endt,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.patstudystat_note,' ') !=
     NVL(:NEW.patstudystat_note,' ') THEN
     audit_trail.column_update
       (raid, 'PATSTUDYSTAT_NOTE',
       :OLD.patstudystat_note, :NEW.patstudystat_note);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL;
    END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
             to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;
  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
              to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;

    IF NVL(:OLD.PATSTUDYSTAT_REASON,0) !=
     NVL(:NEW.PATSTUDYSTAT_REASON,0) THEN
    BEGIN
	SELECT trim(codelst_desc) INTO old_codetype
	  FROM er_codelst
       WHERE pk_codelst =  :OLD.PATSTUDYSTAT_REASON 	;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_codetype := NULL;
    END ;
    BEGIN
     SELECT trim(codelst_desc) INTO new_codetype
	 FROM er_codelst
      WHERE pk_codelst =  :NEW.PATSTUDYSTAT_REASON ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_codetype := NULL;
    END ;

     audit_trail.column_update
       (raid, 'PATSTUDYSTAT_REASON',
       old_codetype, new_codetype);

  END IF;
  IF NVL(:OLD.SCREEN_NUMBER,' ') !=
     NVL(:NEW.SCREEN_NUMBER,' ') THEN
     audit_trail.column_update
       (raid, 'SCREEN_NUMBER',
       :OLD.SCREEN_NUMBER, :NEW.SCREEN_NUMBER);
  END IF;
  IF NVL(:OLD.SCREENED_BY,0) !=
     NVL(:NEW.SCREENED_BY,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.SCREENED_BY ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.SCREENED_BY ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL;
    END ;
     audit_trail.column_update
       (raid, 'SCREENED_BY',
       old_modby, new_modby);
  END IF;
  IF NVL(:OLD.SCREENING_OUTCOME,0) !=
     NVL(:NEW.SCREENING_OUTCOME,0) THEN
    BEGIN
	SELECT trim(codelst_desc) INTO old_codetype
	  FROM er_codelst
       WHERE pk_codelst =  :OLD.SCREENING_OUTCOME 	;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_codetype := NULL;
    END ;
    BEGIN
     SELECT trim(codelst_desc) INTO new_codetype
	 FROM er_codelst
      WHERE pk_codelst =  :NEW.SCREENING_OUTCOME ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_codetype := NULL;
    END ;
     audit_trail.column_update
       (raid, 'SCREENING_OUTCOME',
       old_codetype, new_codetype);

  END IF;
  --Added by Manimaran for the September Enhancement s8.
  if nvl(:old.current_stat,0) !=
     NVL(:new.current_stat,0) then
     audit_trail.column_update
       (raid, 'CURRENT_STAT',
       :old.current_stat, :new.current_stat);
   end if;


END;
/


