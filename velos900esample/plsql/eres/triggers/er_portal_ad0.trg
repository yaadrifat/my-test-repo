CREATE OR REPLACE TRIGGER "ER_PORTAL_AD0" AFTER DELETE ON ER_PORTAL
FOR EACH ROW
declare
  raid number(10);
  deleted_data clob;

begin
  select seq_audit.nextval into raid from dual;
audit_trail.record_transaction
    (raid, 'ER_PORTAL', :old.rid, 'D');
--Created by Gopu for Audit delete
deleted_data :=
to_char(:old.PK_PORTAL) || '|' ||
:old.PORTAL_NAME || '|' ||
to_char(:old.FK_ACCOUNT) || '|' ||
to_char(:old.PORTAL_CREATED_BY) || '|' ||
:old.PORTAL_DESC || '|' ||
:old.PORTAL_LEVEL || '|' ||
to_char(:old.PORTAL_NOTIFICATION_FLAG) || '|' ||
:old.PORTAL_STATUS || '|' ||
:old.PORTAL_DISP_TYPE || '|' ||
:old.PORTAL_BGCOLOR || '|' ||
:old.PORTAL_TEXTCOLOR || '|' ||
to_char(:old.PORTAL_AUDITUSER) || '|' ||
to_char(:old.RID) || '|' ||
to_char(:old.CREATOR) || '|' ||
to_char(:old.LAST_MODIFIED_BY) || '|' ||
to_char(:old.LAST_MODIFIED_DATE) || '|' ||
to_char(:old.CREATED_ON) || '|' ||
:old.IP_ADD|| '|' ||
to_char(:old.FK_STUDY) || to_char(:old.portal_selflogout)|| '|' || to_char(:new.portal_createlogins) || '|' || :new.portal_defaultpass
 || '|' || :new.portal_consenting_form;

insert into AUDIT_DELETE
(raid, row_data) values (raid, SUBSTR(deleted_data,1,4000));--KM-to fix the bug3038
end;
/


