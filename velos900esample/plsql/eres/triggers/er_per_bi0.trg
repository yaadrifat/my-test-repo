CREATE OR REPLACE TRIGGER "ER_PER_BI0" BEFORE INSERT ON ER_PER
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;

  audit_trail.record_transaction(raid, 'ER_PER',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_PER||'|'||:NEW.PER_CODE||'|'|| :NEW.FK_ACCOUNT||'|'||
    :NEW.FK_SITE||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
    TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||
    :NEW.IP_ADD||'|'|| :NEW.RID||'|'|| :NEW.FK_TIMEZONE;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END;
/


