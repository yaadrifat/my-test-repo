CREATE OR REPLACE TRIGGER "ERES"."ER_STUDYTEAM_AD0"
AFTER DELETE
ON ER_STUDYTEAM
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);
  usr VARCHAR(2000);


begin

  usr := Getuser(:OLD.last_modified_by);

  select seq_audit.nextval into raid from dual;
  audit_trail.record_transaction
    (raid, 'ER_STUDYTEAM', :old.rid, 'D', usr);
  deleted_data :=
  to_char(:old.pk_studyteam) || '|' ||
  to_char(:old.fk_codelst_tmrole) || '|' ||
  to_char(:old.fk_user) || '|' ||
  to_char(:old.fk_study) || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.study_team_rights || '|' ||
  :old.ip_add || '|' ||
  :old.study_team_usr_type || '|' ||
  -- Added the column studyteam status for july-august enhancement
  :old.studyteam_status;
insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/