CREATE OR REPLACE TRIGGER ER_STUDYSTAT_BI0 BEFORE INSERT ON ER_STUDYSTAT
FOR EACH ROW
DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
    usr := getuser(:NEW.creator);
                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'ER_STUDYSTAT',erid, 'I',usr);
    --Modified By Amarnadh
    -- Added by Ganapathy on 06/21/05 for Audit inserting
    insert_data:= :NEW.FK_SITE||'|'|| :NEW.PK_STUDYSTAT||'|'||
    :NEW.FK_USER_DOCBY||'|'|| :NEW.FK_CODELST_STUDYSTAT||'|'||
    :NEW.FK_STUDY||'|'|| TO_CHAR(:NEW.STUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
    :NEW.STUDYSTAT_HSPN||'|'||:NEW.STUDYSTAT_NOTE||'|'||
    TO_CHAR(:NEW.STUDYSTAT_ENDT,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| :NEW.FK_CODELST_APRSTAT||'|'||
    :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
    TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
    :NEW.FK_CODELST_APRNO||'|'||:NEW.IP_ADD||'|'|| TO_CHAR(:NEW.STUDYSTAT_VALIDT,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
    :NEW.CURRENT_STAT ||'|'||
    :NEW.OUTCOME   ||'|'|| :NEW.STATUS_TYPE ||'|'||TO_CHAR(:NEW.STUDYSTAT_MEETDT,PKG_DATEUTIL.F_GET_DATEFORMAT)
    ||'|'|| :NEW.fk_codelst_revboard ||'|'|| :NEW.studystat_assignedto;

    INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
END ;
/


