CREATE OR REPLACE TRIGGER "ER_STUDY_BI1" 
BEFORE INSERT
ON ER_STUDY
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
begin
  if :new.study_pubflag is null then
    :new.study_pubflag := 'Y' ;
  end if;
end;
/


