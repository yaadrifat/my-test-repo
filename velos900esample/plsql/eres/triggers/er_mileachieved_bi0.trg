CREATE OR REPLACE TRIGGER "ER_MILEACHIEVED_BI0" BEFORE INSERT ON ER_MILEACHIEVED
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
 erid NUMBER(10);
 usr VARCHAR(2000);
 raid NUMBER(10);
 insert_data CLOB;
BEGIN
    usr := Getuser(:NEW.creator);


 SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  Audit_Trail.record_transaction(raid, 'ER_MILEACHIEVED',erid, 'I',usr);

 insert_data:= :NEW.pk_mileachieved ||'|'|| :NEW.fk_milestone ||'|'|| :NEW.fk_per ||'|'|| :NEW.fk_study ||'|'|| :NEW.fk_patprot
 ||'|'|| TO_CHAR(:NEW.ach_date  ,PKG_DATEUTIL.f_get_dateformat) ||'|'|| :NEW.table_pk  ||'|'|| :NEW.table_name
 ||'|'|| :NEW.CREATOR ||'|'|| :NEW.IP_ADD ||'|'|| :NEW.RID   ||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)    ;

 INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);

  END ;
/


