CREATE OR REPLACE TRIGGER "ER_STUDYVER_AD0" 
  after delete
  on er_studyver
  for each row
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'ER_STUDYVER', :old.rid, 'D');

  deleted_data :=
  to_char(:old.pk_studyver) || '|' ||
  to_char(:old.fk_study) || '|' ||
  :old.studyver_number || '|' ||
  :old.studyver_status || '|' ||
  :old.studyver_notes || '|' ||
  to_char(:old.orig_study) || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.ip_add;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


