CREATE OR REPLACE TRIGGER "CB_FUNDING_GUIDELINES_AU1"
AFTER UPDATE ON CB_FUNDING_GUIDELINES REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE 
    v_rowid NUMBER(10);/*variable used to fetch value of sequence */
    NEW_FK_FUND_CATEG VARCHAR(200); 
    OLD_FK_FUND_CATEG  VARCHAR(200);
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
    
    
    IF NVL(:OLD.FK_FUND_CATEG,0) !=     NVL(:NEW.FK_FUND_CATEG,0) THEN
   BEGIN
           SELECT F_Codelst_Desc(:new.FK_FUND_CATEG)        INTO NEW_FK_FUND_CATEG  from dual;
   EXCEPTION WHEN OTHERS THEN
                NEW_FK_FUND_CATEG := '';
   END;  
   BEGIN
           SELECT F_Codelst_Desc(:OLD.FK_FUND_CATEG)        INTO OLD_FK_FUND_CATEG  from dual;
   EXCEPTION WHEN OTHERS THEN
                OLD_FK_FUND_CATEG := '';
   END;
  END IF; 
    
    
    
    IF :NEW.DELETEDFLAG = 'Y' THEN
      
    PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'CB_FUNDING_GUIDELINES',:OLD.rid,:OLD.PK_FUNDING_GUIDELINE,'D',:NEW.LAST_MODIFIED_BY);    
  
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','PK_FUNDING_GUIDELINE',:OLD.PK_FUNDING_GUIDELINE,NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','FK_CBB_ID',:OLD.FK_CBB_ID,NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','FUNDING_ID',:OLD.FUNDING_ID,NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','FK_FUND_CATEG',OLD_FK_FUND_CATEG,NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','FUNDING_BDATE', to_char(:OLD.FUNDING_BDATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','DESCRIPTION',:OLD.DESCRIPTION,NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','FUNDING_STATUS',:OLD.FUNDING_STATUS,NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','CREATOR',:OLD.CREATOR,NULL,NULL,NULL);    
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','CREATED_ON', to_char(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','IP_ADD',:OLD.IP_ADD,NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','LAST_MODIFIED_DATE', to_char(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);    
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','RID',:OLD.RID,NULL,NULL,NULL);    
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','DELETEDFLAG',:OLD.DELETEDFLAG,NULL,NULL,NULL);  
        
    ELSE
    
    PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'CB_FUNDING_GUIDELINES',:OLD.RID,:OLD.PK_FUNDING_GUIDELINE,'U',:NEW.LAST_MODIFIED_BY); 
    
    IF NVL(:OLD.PK_FUNDING_GUIDELINE,0) != NVL(:NEW.PK_FUNDING_GUIDELINE,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','PK_FUNDING_GUIDELINE', :OLD.PK_FUNDING_GUIDELINE, :NEW.PK_FUNDING_GUIDELINE,NULL,NULL);
  END IF;  
    IF NVL(:OLD.FK_CBB_ID,0) != NVL(:NEW.FK_CBB_ID,0) THEN
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','FK_CBB_ID',:OLD.FK_CBB_ID, :NEW.FK_CBB_ID,NULL,NULL);
  END IF;  
  IF NVL(:OLD.FUNDING_ID,' ') != NVL(:NEW.FUNDING_ID,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','FUNDING_ID',:OLD.FUNDING_ID, :NEW.FUNDING_ID,NULL,NULL);
  END IF;  
   IF NVL(:OLD.FK_FUND_CATEG,0) != NVL(:NEW.FK_FUND_CATEG,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','FK_FUND_CATEG',OLD_FK_FUND_CATEG,NEW_FK_FUND_CATEG,NULL,NULL);
  END IF;
  IF NVL(:OLD.FUNDING_BDATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.FUNDING_BDATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','FUNDING_BDATE',
       TO_CHAR(:OLD.FUNDING_BDATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.FUNDING_BDATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;    
  IF NVL(:OLD.DESCRIPTION,' ') != NVL(:NEW.DESCRIPTION,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','DESCRIPTION',:OLD.DESCRIPTION, :NEW.DESCRIPTION,NULL,NULL);
  END IF;    
  IF NVL(:OLD.FUNDING_STATUS,' ') != NVL(:NEW.FUNDING_STATUS,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','FUNDING_STATUS',:OLD.FUNDING_STATUS, :NEW.FUNDING_STATUS,NULL,NULL);
  END IF;  
  IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES', 'CREATOR',:OLD.CREATOR, :NEW.CREATOR,NULL,NULL);
  END IF;
  IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','CREATED_ON',
       TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
  IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','IP_ADD',:OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);
  END IF;     
    IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','LAST_MODIFIED_BY', :OLD.LAST_MODIFIED_BY, :NEW.LAST_MODIFIED_BY,NULL,NULL);
	END IF;   
	IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','LAST_MODIFIED_DATE',
       TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;      
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES', 'RID', :OLD.RID, :NEW.RID,NULL,NULL);     
    END IF;    
    IF NVL(:OLD.DELETEDFLAG,' ') != NVL(:NEW.DELETEDFLAG,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_FUNDING_GUIDELINES','DELETEDFLAG', :OLD.DELETEDFLAG, :NEW.DELETEDFLAG,NULL,NULL);     
    END IF;     
  END IF;  
END ;
/

