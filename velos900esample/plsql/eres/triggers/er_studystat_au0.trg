CREATE OR REPLACE TRIGGER ER_STUDYSTAT_AU0 AFTER UPDATE ON ER_STUDYSTAT FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := getuser(:NEW.CREATOR);--28/03/11 @Ankit #5396
  audit_trail.record_transaction
    (raid, 'ER_STUDYSTAT', :OLD.rid, 'U', usr);
  IF NVL(:OLD.pk_studystat,0) !=
     NVL(:NEW.pk_studystat,0) THEN
     audit_trail.column_update
       (raid, 'PK_STUDYSTAT',
       :OLD.pk_studystat, :NEW.pk_studystat);
  END IF;
  IF NVL(:OLD.fk_user_docby,0) !=
     NVL(:NEW.fk_user_docby,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.fk_user_docby ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.fk_user_docby ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'FK_USER_DOCBY',
       old_modby, new_modby);
  END IF;
  IF NVL(:OLD.fk_codelst_studystat,0) !=
     NVL(:NEW.fk_codelst_studystat,0) THEN
    BEGIN
	SELECT trim(codelst_desc) INTO old_codetype
	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_studystat ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_codetype := NULL ;
    END ;
    BEGIN
	SELECT trim(codelst_desc) INTO new_codetype
	FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_studystat ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_codetype := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'FK_CODELST_STUDYSTAT',
       old_codetype, new_codetype);
  END IF;
  IF NVL(:OLD.fk_study,0) !=
     NVL(:NEW.fk_study,0) THEN
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :OLD.fk_study, :NEW.fk_study);
  END IF;
  IF NVL(:OLD.studystat_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.studystat_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'STUDYSTAT_DATE',
       to_char(:OLD.studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.studystat_hspn,' ') !=
     NVL(:NEW.studystat_hspn,' ') THEN
     audit_trail.column_update
       (raid, 'STUDYSTAT_HSPN',
       :OLD.studystat_hspn, :NEW.studystat_hspn);
  END IF;
  IF NVL(:OLD.studystat_note,' ') !=
     NVL(:NEW.studystat_note,' ') THEN
     audit_trail.column_update
       (raid, 'STUDYSTAT_NOTE',
       :OLD.studystat_note, :NEW.studystat_note);
  END IF;
  IF NVL(:OLD.studystat_endt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.studystat_endt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'STUDYSTAT_ENDT',
       to_char(:OLD.studystat_endt,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.studystat_endt,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.fk_codelst_aprstat,0) !=
     NVL(:NEW.fk_codelst_aprstat,0) THEN
    BEGIN
	SELECT trim(codelst_desc) INTO old_codetype
	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_aprstat ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_codetype := NULL ;
    END ;
    BEGIN
	SELECT trim(codelst_desc) INTO new_codetype
	FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_aprstat ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_codetype := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'FK_CODELST_APRSTAT',
       old_codetype, new_codetype);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;

  IF NVL(:OLD.last_modified_by,0) !=  NVL(:NEW.last_modified_by,0) THEN

  	BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL ;
    END ;

     audit_trail.column_update (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;

  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN

     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;


  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
              to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
    IF NVL(:OLD.fk_codelst_aprno,0) !=
     NVL(:NEW.fk_codelst_aprno,0) THEN
     audit_trail.column_update
       (raid, 'FK_CODELST_APRNO',
       :OLD.fk_codelst_aprno, :NEW.fk_codelst_aprno);
  END IF;
  IF NVL(:OLD.IP_ADD,' ') !=
     NVL(:NEW.IP_ADD,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.IP_ADD, :NEW.IP_ADD);
  END IF;
 --Added by Manimaran for the September Enhancement s8.
 IF NVL(:OLD.current_stat,0) !=
     NVL(:NEW.current_stat,0) THEN
     audit_trail.column_update
       (raid, 'CURRENT_STAT',
       :OLD.current_stat, :NEW.current_stat);
  END IF;

  --Added by Amarnadh for December Enhancement

    IF NVL(:OLD.studystat_meetdt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.studystat_meetdt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'STUDYSTAT_MEETDT',
       to_char(:OLD.studystat_meetdt,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.studystat_meetdt,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

   IF NVL(:OLD.outcome,0) !=
     NVL(:NEW.outcome,0) THEN
    BEGIN
	SELECT trim(codelst_desc) INTO old_codetype
	FROM ER_CODELST WHERE pk_codelst =  :OLD.outcome ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_codetype := NULL ;
    END ;
    BEGIN
	SELECT trim(codelst_desc) INTO new_codetype
	FROM ER_CODELST WHERE pk_codelst =  :NEW.outcome ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_codetype := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'OUTCOME',
       old_codetype, new_codetype);
  END IF;

   IF NVL(:OLD.status_type,0) !=
     NVL(:NEW.status_type,0) THEN
    BEGIN
	SELECT trim(codelst_desc) INTO old_codetype
	FROM ER_CODELST WHERE pk_codelst =  :OLD.status_type ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_codetype := NULL ;
    END ;
    BEGIN
	SELECT trim(codelst_desc) INTO new_codetype
	FROM ER_CODELST WHERE pk_codelst =  :NEW.status_type ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_codetype := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'STATUS_TYPE',
       old_codetype, new_codetype);
  END IF;

  IF NVL(:OLD.studystat_assignedto,0) !=  NVL(:NEW.studystat_assignedto,0) THEN

  	BEGIN

     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM ER_USER
     WHERE pk_user = :OLD.studystat_assignedto ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL ;
    END ;

    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM ER_USER
     WHERE pk_user = :NEW.studystat_assignedto ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL ;
    END ;

     audit_trail.column_update (raid, 'STUDYSTAT_ASSIGNEDTO',
       old_modby, new_modby);
  END IF;


   IF NVL(:OLD.fk_codelst_revboard,0) !=
     NVL(:NEW.fk_codelst_revboard,0) THEN
    BEGIN

	SELECT trim(codelst_desc) INTO old_codetype
	FROM ER_CODELST WHERE pk_codelst =  :OLD.fk_codelst_revboard ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_codetype := NULL ;
    END ;
    BEGIN

		SELECT trim(codelst_desc) INTO new_codetype
		FROM ER_CODELST WHERE pk_codelst =  :NEW.fk_codelst_revboard ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_codetype := NULL ;
    END ;

     audit_trail.column_update
       (raid, 'FK_CODELST_REVBOARD',
       old_codetype, new_codetype);
  END IF;


END;
/


