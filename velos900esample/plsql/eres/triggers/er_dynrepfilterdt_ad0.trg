CREATE OR REPLACE TRIGGER "ER_DYNREPFILTERDT_AD0" AFTER DELETE ON ER_DYNREPFILTERDT
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'ER_DYNREPFILTERDT', :old.rid, 'D');

  deleted_data :=
  to_char(:old.PK_DYNREPFILTERDT) || '|' ||
  :old.DYNREPFILTERDT_COL || '|' ||
  :old.DYNREPFILTERDT_QF || '|' ||
  :old.DYNREPFILTERDT_DATA || '|' ||
  :old.DYNREPFILTERDT_EXCLUDE || '|' ||
  :old.DYNREPFILTERDT_BBRAC || '|' ||
  :old.DYNREPFILTERDT_EBRAC || '|' ||
  :old.DYNREPFILTERDT_OPER || '|' ||
  to_char(:old.FK_DYNREPFILTER) || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.ip_add || '|' ||
  :old.DYNREPFILTERDT_NAME;

  insert into audit_delete(raid, row_data) values (raid, deleted_data);
end;
/


