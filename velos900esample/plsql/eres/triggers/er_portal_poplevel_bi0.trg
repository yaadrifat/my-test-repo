CREATE OR REPLACE TRIGGER "ER_PORTAL_POPLEVEL_BI0" BEFORE INSERT ON ER_PORTAL_POPLEVEL
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN
   BEGIN
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname INTO usr FROM er_user
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   audit_trail.record_transaction(raid, 'ER_PORTAL_POPLEVEL',erid, 'I',usr);

   insert_data:= :NEW.PK_PORTAL_POPLEVEL||'|'||:NEW.FK_PORTAL||'|'|| :NEW.PP_OBJECT_TYPE||'|'||:NEW.PP_OBJECT_ID||'|'||:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)
   ||'|'||:NEW.IP_ADD;

   INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END ;
/


