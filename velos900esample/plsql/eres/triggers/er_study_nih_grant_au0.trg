CREATE OR REPLACE TRIGGER ERES.ER_STUDY_NIH_GRANT_AU0
AFTER UPDATE OF PK_STUDY_NIH_GRANT,FK_STUDY,FK_CODELST_FUNDMECH,FK_CODELST_INSTCODE,NIH_GRANT_SERIAL,FK_CODELST_PROGRAM_CODE,
IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE
ON ERES.ER_STUDY_NIH_GRANT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
	raid NUMBER(10);
	usr VARCHAR2(100);
	old_modby VARCHAR2(100);
	new_modby VARCHAR2(100);
BEGIN
	SELECT seq_audit.NEXTVAL INTO raid FROM dual;
	usr := getuser(:NEW.last_modified_by);
	audit_trail.record_transaction
	(raid, 'ER_STUDY_NIH_GRANT', :OLD.rid, 'U', usr);

	IF NVL(:OLD.PK_STUDY_NIH_GRANT,0) != NVL(:NEW.PK_STUDY_NIH_GRANT,0) then
		audit_trail.column_update (raid, 'PK_STUDY_NIH_GRANT',:OLD.PK_STUDY_NIH_GRANT,:NEW.PK_STUDY_NIH_GRANT);
	end if;
	IF NVL(:OLD.FK_STUDY,0) != NVL(:NEW.FK_STUDY,0) then
		audit_trail.column_update (raid, 'FK_STUDY',:OLD.FK_STUDY,:NEW.FK_STUDY);
	end if;
	IF NVL(:OLD.FK_CODELST_FUNDMECH,0) != NVL(:NEW.FK_CODELST_FUNDMECH,0) then
		audit_trail.column_update (raid, 'FK_CODELST_FUNDMECH',:OLD.FK_CODELST_FUNDMECH,:NEW.FK_CODELST_FUNDMECH);
	end if;
	IF NVL(:OLD.FK_CODELST_INSTCODE,0) != NVL(:NEW.FK_CODELST_INSTCODE,0) then
		audit_trail.column_update (raid, 'FK_CODELST_INSTCODE',:OLD.FK_CODELST_INSTCODE,:NEW.FK_CODELST_INSTCODE);
	end if;
	IF NVL(:OLD.NIH_GRANT_SERIAL,' ') != NVL(:NEW.NIH_GRANT_SERIAL,' ') then
		audit_trail.column_update (raid, 'NIH_GRANT_SERIAL',:OLD.NIH_GRANT_SERIAL,:NEW.NIH_GRANT_SERIAL);
	end if;
	IF NVL(:OLD.FK_CODELST_PROGRAM_CODE,0) != NVL(:NEW.FK_CODELST_PROGRAM_CODE,0) then
		audit_trail.column_update (raid, 'FK_CODELST_PROGRAM_CODE',:OLD.FK_CODELST_PROGRAM_CODE,:NEW.FK_CODELST_PROGRAM_CODE);
	end if;
	IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') then
		audit_trail.column_update (raid, 'IP_ADD',:OLD.IP_ADD,:NEW.IP_ADD);
	end if;

	IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) then
		BEGIN
			SELECT  TO_CHAR(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
			INTO old_modby FROM ER_USER  WHERE pk_user = :OLD.LAST_MODIFIED_BY ;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				old_modby := NULL;
		END ;
		BEGIN
			SELECT  TO_CHAR(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
			INTO new_modby   FROM ER_USER   WHERE pk_user = :NEW.LAST_MODIFIED_BY ;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				new_modby := NULL;
		 END ;
		audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
	end if;
	IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
		audit_trail.column_update (raid, 'LAST_MODIFIED_DATE',to_char(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
	end if;
END;
/