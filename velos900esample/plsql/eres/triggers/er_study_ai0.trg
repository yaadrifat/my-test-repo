CREATE OR REPLACE TRIGGER "ER_STUDY_AI0" 
AFTER INSERT
ON ER_STUDY
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE

v_stat NUMBER(10);
v_site NUMBER;
o_success NUMBER;
v_stattype number;

BEGIN


--insert a default status for a new study

	SELECT pk_codelst INTO v_stat
     FROM ER_CODELST
	WHERE codelst_type='studystat'
	AND codelst_subtyp = 'not_active';

	begin

		SELECT pk_codelst INTO v_stattype
	     FROM ER_CODELST
		WHERE codelst_type='studystat_type'
		AND codelst_subtyp = 'default';

	exception when no_data_found then
		v_stattype := null;
	end;

  SELECT FK_SITEID INTO v_site
	FROM
	ER_USER
	WHERE pk_user =:NEW.FK_AUTHOR;

-- current_stat is added by Manimaran for September Enhancement S8.

INSERT INTO ER_STUDYSTAT
(pk_studystat,fk_user_docby,
fk_codelst_studystat,
fk_study,
fk_site,
studystat_date,
creator,
created_on,
ip_add,
current_stat,status_type)
VALUES
(seq_er_studystat.NEXTVAL, :NEW.creator,
v_stat,
:NEW.pk_study,
 v_site,
 --JM: Jan242006 for not active study to have only date part, not time part
 TO_DATE(TO_CHAR(SYSDATE,'mm/dd/yyyy'),'mm/dd/yyyy')
,

:NEW.creator,

SYSDATE,

:NEW.ip_add, 1,v_stattype);


-- will be called from java
--Pkg_Supuser.SP_GRANT_SUPUSER_FORSTUDY(:NEW.fk_account, :NEW.pk_study, :NEW.creator , :NEW.ip_add, o_success);


END;
/


