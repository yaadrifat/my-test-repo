CREATE OR REPLACE TRIGGER "ER_STORAGE_KIT_BI0" BEFORE INSERT ON ER_STORAGE_KIT
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN
   BEGIN
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname INTO usr FROM er_user
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL) INTO erid FROM dual;
   :NEW.rid := erid ;

   SELECT seq_audit.NEXTVAL INTO raid FROM dual;
   audit_trail.record_transaction(raid, 'ER_STORAGE_KIT',erid, 'I',usr);


insert_data:= :NEW.PK_STORAGE_KIT
||'|'||:NEW.FK_STORAGE
||'|'||:NEW.DEF_SPECIMEN_TYPE
||'|'||:NEW.DEF_PROCESSING_TYPE
||'|'||:NEW.DEF_SPEC_PROCESSING_SEQ
||'|'||:NEW.DEF_SPEC_QUANTITY
||'|'||:NEW.DEF_SPEC_QUANTITY_UNIT
||'|'||:NEW.KIT_SPEC_DISPOSITION
||'|'||:NEW.KIT_SPEC_ENVT_CONS
||'|'||:NEW.RID
||'|'||:NEW.CREATOR
||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)
||'|'||:NEW.LAST_MODIFIED_BY
||'|'||TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)
||'|'||:NEW.IP_ADD;

INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);

END;
/


