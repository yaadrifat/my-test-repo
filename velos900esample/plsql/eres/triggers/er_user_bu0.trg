CREATE OR REPLACE TRIGGER "ER_USER_BU0" 
BEFORE UPDATE
ON ER_USER
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
new.last_modified_by is not null
      )
BEGIN
  :NEW.last_modified_date := SYSDATE ;
END;
/


