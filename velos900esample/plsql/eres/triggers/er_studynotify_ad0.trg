CREATE OR REPLACE TRIGGER "ER_STUDYNOTIFY_AD0" 
AFTER DELETE
ON ER_STUDYNOTIFY
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);
begin
  select seq_audit.nextval into raid from dual;
  audit_trail.record_transaction
    (raid, 'ER_STUDYNOTIFY', :old.rid, 'D');
  deleted_data :=
  to_char(:old.pk_studynotify) || '|' ||
  to_char(:old.fk_codelst_tarea) || '|' ||
  to_char(:old.fk_user) || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.STUDYNOTIFY_TYPE || '|' ||
  :old.STUDYNOTIFY_KEYWRDS || '|' ||
  :old.IP_ADD ;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


