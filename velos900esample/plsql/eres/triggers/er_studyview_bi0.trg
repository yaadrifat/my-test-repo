CREATE OR REPLACE TRIGGER "ER_STUDYVIEW_BI0" BEFORE INSERT ON ER_STUDYVIEW
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
 erid NUMBER(10);
 usr VARCHAR(2000);
 raid NUMBER(10);
 insert_data CLOB;
     BEGIN
    usr := getuser(:NEW.creator);
                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction(raid, 'ER_STUDYVIEW',erid, 'I', usr);
  -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_STUDYVIEW||'|'|| :NEW.FK_USER||'|'||
     :NEW.FK_STUDY||'|'||:NEW.STUDYVIEW_NUM||'|'||:NEW.STUDYVIEW_TITLE||'|'|| :NEW.RID||'|'||
  :NEW.CREATOR||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
  :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD||'|'||
  :NEW.STUDYVIEW_VERNO;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END;
/


