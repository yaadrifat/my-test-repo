CREATE OR REPLACE TRIGGER ER_GRPS_BI0 BEFORE INSERT ON ER_GRPS
FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
 BEGIN
  SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' ||usr_firstname
  INTO usr FROM er_user WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
   audit_trail.record_transaction(raid, 'ER_GRPS',erid, 'I',usr);
     -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:=:NEW.GRP_SUPBUD_RIGHTS||'|'|| :NEW.GRP_SUPBUD_FLAG||'|'||
   :NEW.PK_GRP||'|'||:NEW.GRP_NAME||'|'||:NEW.GRP_DESC||'|'|| :NEW.RID||'|'||
   :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
   TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
   TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'|| :NEW.FK_ACCOUNT||'|'||
   :NEW.GRP_RIGHTS||'|'||:NEW.IP_ADD||'|'||:NEW.GRP_SUPUSR_RIGHTS||'|'||
   :NEW.GRP_SUPUSR_FLAG||'|'||:NEW.GRP_HIDDEN||'|'||:new.FK_CODELST_ST_ROLE;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END ;
/


