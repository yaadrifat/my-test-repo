create or replace TRIGGER "ER_FLDVALIDATE_BI0" BEFORE INSERT ON ER_FLDVALIDATE
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(100);
  insert_data CLOB;
 BEGIN
 BEGIN
 --KM-#3635
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;


  SELECT TRUNC(seq_rid.NEXTVAL) INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'ER_FLDVALIDATE', erid, 'I', USR );
-- Added by JMajumdar on 23-06-05 for Audit Insert
insert_data:= :NEW.PK_FLDVALIDATE||'|'|| :NEW.FK_FLDLIB||'|'|| :NEW.FLDVALIDATE_OP1||'|'||
     :NEW.FLDVALIDATE_VAL1||'|'||:NEW.FLDVALIDATE_LOGOP1||'|'|| :NEW.FLDVALIDATE_OP2||'|'||
     :NEW.FLDVALIDATE_VAL2||'|'|| :NEW.FLDVALIDATE_LOGOP2||'|'||:NEW.FLDVALIDATE_JAVASCR||'|'||
     :NEW.RECORD_TYPE||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
      TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'|| TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.f_get_dateformat)||'|'||
      :NEW.IP_ADD||'|'|| :NEW.RID;

INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/


