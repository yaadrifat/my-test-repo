CREATE OR REPLACE TRIGGER "ER_FORMSLINEAR_AI1"
AFTER INSERT
ON ER_FORMSLINEAR
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

usrName VARCHAR2(500);


 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'ER_FL2_TRIGGER', pLEVEL  => Plog.LFATAL);
/******************************************************************************
   NAME:       ER_FORMSLINEAR_AI1
   PURPOSE:    After insert of er_formslinear for forms audit trail

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        06/16/2006             1. Created this trigger.
******************************************************************************/
BEGIN

   BEGIN


        SELECT Getuser(:NEW.creator) INTO usrName FROM dual;

		SELECT SUBSTR(usrName,INSTR(usrName,',')+1) INTO usrName FROM dual;


    EXCEPTION WHEN OTHERS THEN
   			   usrName :=  '';
   END;

INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,
fa_oldvalue,fa_newvalue,fa_modifiedby_name)
VALUES (seq_er_formauditcol.NEXTVAL,:NEW.fk_filledform,:NEW.fk_form,:NEW.form_type,'created_by',NULL,'Created By',
'Not Applicable',  usrName, 'Not Applicable');

 INSERT INTO ER_FORMAUDITCOL(pk_formauditcol,fk_filledform,fk_form,fa_formtype,fa_systemid,fa_datetime,fa_fldname,
fa_oldvalue,fa_newvalue,fa_modifiedby_name)
VALUES (seq_er_formauditcol.NEXTVAL,:NEW.fk_filledform,:NEW.fk_form,:NEW.form_type,'created_on',NULL,'Created On',
'Not Applicable', TO_CHAR(SYSDATE,PKG_DATEUTIL.f_get_datetimeformat)  ,'Not Applicable');


      EXCEPTION
     WHEN OTHERS THEN
	 	  plog.DEBUG(pCTX,'Exception in ER_FORMSLINEAR_AI1 :' || SQLERRM);
       -- Consider logging the error and then re-raise
       RAISE;
END ;
/


