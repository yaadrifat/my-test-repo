CREATE OR REPLACE TRIGGER "ER_ORDER_USERS_AU1" AFTER UPDATE ON eres.ER_ORDER_USERS 
referencing OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE 
    v_rowid NUMBER(10);/*variable used to fetch value of sequence */
BEGIN
    SELECT seq_audit_row_module.nextval INTO v_rowid FROM dual;
    
    pkg_audit_trail_module.sp_row_insert (v_rowid,'ER_ORDER_USERS',:OLD.rid,:OLD.PK_ORDER_USER,'U',:NEW.LAST_MODIFIED_BY);
    
     IF NVL(:OLD.PK_ORDER_USER,0) != NVL(:NEW.PK_ORDER_USER,0) THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'PK_ORDER_USER',:OLD.PK_ORDER_USER, :NEW.PK_ORDER_USER,NULL,NULL);
  END IF;
 IF NVL(:OLD.USER_LOGINID,' ') != NVL(:NEW.USER_LOGINID,' ') THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'USER_LOGINID',:OLD.USER_LOGINID, :NEW.USER_LOGINID,NULL,NULL);
  END IF; 
  IF NVL(:OLD.USER_FNAME,' ') != NVL(:NEW.USER_FNAME,' ') THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'USER_FNAME',:OLD.USER_FNAME, :NEW.USER_FNAME,NULL,NULL);
  END IF; 
  IF NVL(:OLD.USER_LNAME,' ') != NVL(:NEW.USER_LNAME,' ') THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'USER_LNAME',:OLD.USER_LNAME, :NEW.USER_LNAME,NULL,NULL);
  END IF;
  IF NVL(:OLD.USER_MAILID,' ') != NVL(:NEW.USER_MAILID,' ') THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'USER_MAILID',:OLD.USER_MAILID, :NEW.USER_MAILID,NULL,NULL);
  END IF;
  IF NVL(:OLD.USER_CONTACTNO,' ') != NVL(:NEW.USER_CONTACTNO,' ') THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'USER_CONTACTNO',:OLD.USER_CONTACTNO, :NEW.USER_CONTACTNO,NULL,NULL);
  END IF;
  IF NVL(:OLD.USER_COMMENTS,' ') != NVL(:NEW.USER_COMMENTS,' ') THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'USER_COMMENTS',:OLD.USER_COMMENTS, :NEW.USER_COMMENTS,NULL,NULL);
  END IF;
   IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'CREATOR',:OLD.CREATOR, :NEW.CREATOR,NULL,NULL);
  END IF;  
   IF NVL(:OLD.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=  NVL(:NEW.CREATED_ON,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS', 'CREATED_ON',
       TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
  IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'IP_ADD',:OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);     
    END IF;
    IF nvl(:OLD.last_modified_by,0) != nvl(:NEW.last_modified_by,0) THEN
        pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS','LAST_MODIFIED_BY',:OLD.last_modified_by,:NEW.last_modified_by,NULL,NULL);
      END IF;   	
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS', 'LAST_MODIFIED_DATE',
       TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
    IF NVL(:OLD.DELETEDFLAG,0) != NVL(:NEW.DELETEDFLAG,0) THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'DELETEDFLAG',:OLD.DELETEDFLAG, :NEW.DELETEDFLAG,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'RID',:OLD.RID, :NEW.RID,NULL,NULL);     
    END IF;
  END;
/

