create or replace TRIGGER ER_MILESTONE_AU0 AFTER UPDATE OF RID,FK_CAL,IP_ADD,CREATOR,FK_STUDY,CREATED_ON,PK_MILESTONE,FK_EVENTASSOC,MILESTONE_TYPE,FK_CODELST_RULE,MILESTONE_COUNT,MILESTONE_LIMIT,MILESTONE_VISIT,MILESTONE_AMOUNT,MILESTONE_PAYFOR,MILESTONE_STATUS,MILESTONE_DELFLAG,MILESTONE_PAYTYPE,MILESTONE_USERSTO,MILESTONE_PAYDUEBY,MILESTONE_PAYBYUNIT,MILESTONE_EVENTSTATUS ON ERES.ER_MILESTONE FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr varchar2(2000);
BEGIN
  --KM-#3634
  usr := getuser(:new.last_modified_by);
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  
  if (:new.last_modified_by is not null) then
  Audit_Trail.record_transaction
    (raid, 'ER_MILESTONE', :OLD.rid, 'U', usr);
  end if;
  
  IF NVL(:OLD.pk_milestone,0) !=
     NVL(:NEW.pk_milestone,0) THEN
     Audit_Trail.column_update
       (raid, 'PK_MILESTONE',
       :OLD.pk_milestone, :NEW.pk_milestone);
  END IF;
  IF NVL(:OLD.fk_study,0) !=
     NVL(:NEW.fk_study,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_STUDY',
       :OLD.fk_study, :NEW.fk_study);
  END IF;
  IF NVL(:OLD.milestone_type,' ') !=
     NVL(:NEW.milestone_type,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_TYPE',
       :OLD.milestone_type, :NEW.milestone_type);
  END IF;
  IF NVL(:OLD.fk_cal,0) !=
     NVL(:NEW.fk_cal,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_CAL',
       :OLD.fk_cal, :NEW.fk_cal);
  END IF;
  IF NVL(:OLD.fk_codelst_rule,0) !=
     NVL(:NEW.fk_codelst_rule,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_CODELST_RULE',
       :OLD.fk_codelst_rule, :NEW.fk_codelst_rule);
  END IF;
  IF NVL(:OLD.milestone_amount,0) !=
     NVL(:NEW.milestone_amount,0) THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_AMOUNT',
       :OLD.milestone_amount, :NEW.milestone_amount);
  END IF;
  IF NVL(:OLD.milestone_visit,0) !=
     NVL(:NEW.milestone_visit,0) THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_VISIT',
       :OLD.milestone_visit, :NEW.milestone_visit);
  END IF;
  IF NVL(:OLD.fk_eventassoc,0) !=
     NVL(:NEW.fk_eventassoc,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_EVENTASSOC',
       :OLD.fk_eventassoc, :NEW.fk_eventassoc);
  END IF;
  IF NVL(:OLD.milestone_count,0) !=
     NVL(:NEW.milestone_count,0) THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_COUNT',
       :OLD.milestone_count, :NEW.milestone_count);
  END IF;
  IF NVL(:OLD.milestone_delflag,' ') !=
     NVL(:NEW.milestone_delflag,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_DELFLAG',
       :OLD.milestone_delflag, :NEW.milestone_delflag);
  END IF;
  IF NVL(:OLD.milestone_usersto,' ') !=
     NVL(:NEW.milestone_usersto,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_USERSTO',
       :OLD.milestone_usersto, :NEW.milestone_usersto);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     Audit_Trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     Audit_Trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     Audit_Trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
      to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     Audit_Trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;

  IF NVL(:OLD.milestone_limit ,0) !=
     NVL(:NEW.milestone_limit ,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_limit',
       :OLD.milestone_limit , :NEW.milestone_limit );
  END IF;

  IF NVL(:OLD.milestone_status,0) !=
     NVL(:NEW.milestone_status ,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_status',
       :OLD.milestone_status , :NEW.milestone_status);
  END IF;
  IF NVL(:OLD.milestone_paytype,0) !=
     NVL(:NEW.milestone_paytype,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_paytype',
       :OLD.milestone_paytype, :NEW.milestone_paytype);
  END IF;
   IF NVL(:OLD.milestone_paydueby,0) !=
     NVL(:NEW.milestone_paydueby,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_paydueby',
       :OLD.milestone_paydueby, :NEW.milestone_paydueby);
  END IF;

  IF NVL(:OLD.milestone_paybyunit,' ') !=
     NVL(:NEW.milestone_paybyunit,' ') THEN
     Audit_Trail.column_update
       (raid, 'milestone_paybyunit',
       :OLD.milestone_paybyunit, :NEW.milestone_paybyunit);
  END IF;
   IF NVL(:OLD.milestone_payfor,0) !=
     NVL(:NEW.milestone_payfor,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_payfor',
       :OLD.milestone_payfor, :NEW.milestone_payfor);
  END IF;
     IF NVL(:OLD.milestone_eventstatus,0) !=
     NVL(:NEW.milestone_eventstatus,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_eventstatus',
       :OLD.milestone_eventstatus, :NEW.milestone_eventstatus);
  END IF;

  /*IF NVL(:OLD.milestone_achievedcount,0) !=
     NVL(:NEW.milestone_achievedcount,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_achievedcount',
       :OLD.milestone_achievedcount, :NEW.milestone_achievedcount);
  END IF;*/

  --Added by Manimaran for milestone_description field.
  IF NVL(:OLD.milestone_description,' ') !=
     NVL(:NEW.milestone_description,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_DESCRIPTION',
       :OLD.milestone_description, :NEW.milestone_description);
  END IF;

  --Added by Manimaran for D-FIN7
  IF NVL(:OLD.fk_codelst_milestone_stat,0) !=
     NVL(:NEW.fk_codelst_milestone_stat,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_CODELST_MILESTONE_STAT',
       :OLD.fk_codelst_milestone_stat, :NEW.fk_codelst_milestone_stat);
  END IF;

END;
/