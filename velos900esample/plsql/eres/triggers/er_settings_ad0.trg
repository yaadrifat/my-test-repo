CREATE OR REPLACE TRIGGER "ER_SETTINGS_AD0" 
AFTER DELETE
ON ER_SETTINGS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
--Created by Manimaran for Audit delete.
declare
  raid number(10);
  deleted_data varchar2(2000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'ER_SETTINGS', :old.rid, 'D');

  deleted_data :=
  to_char(:old.SETTINGS_PK) || '|' ||
  to_char(:old.SETTINGS_MODNUM) || '|' ||
  to_char(:old.SETTINGS_MODNAME) || '|' ||
  :old.SETTINGS_KEYWORD || '|' ||
  :old.SETTINGS_VALUE || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on) || '|' ||
  :old.ip_add;
  insert into audit_delete(raid, row_data) values (raid, deleted_data);
end;
/


