CREATE OR REPLACE TRIGGER "ER_LINKEDFORMS_AD0"
  AFTER UPDATE-- delete
  ON er_linkedforms
  FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

rtype CHAR;
BEGIN
  rtype:=:NEW.record_type;
-- Added by Ganapathy on 28/06/05 for Audit Delete
 IF(rtype='D') THEN


  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'ER_LINKEDFORMS', :OLD.rid, 'D');

  deleted_data :=
  TO_CHAR(:OLD.pk_lf) || '|' ||
  TO_CHAR(:OLD.fk_formlib) || '|' ||
  :OLD.lf_displaytype || '|' ||
  :OLD.lf_entrychar || '|' ||
  TO_CHAR(:OLD.fk_account) || '|' ||
  TO_CHAR(:OLD.fk_study) || '|' ||
  :OLD.lf_lnkfrom || '|' ||
  TO_CHAR(:OLD.fk_calendar) || '|' ||
  TO_CHAR(:OLD.fk_event) || '|' ||
  TO_CHAR(:OLD.fk_crf) || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  :OLD.record_type || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add || '|' ||
  :OLD.lf_isirb || '|' ||
  :OLD.lf_submission_type;

INSERT INTO audit_delete
(raid, row_data) VALUES (raid, deleted_data);
END IF;
END;
/


