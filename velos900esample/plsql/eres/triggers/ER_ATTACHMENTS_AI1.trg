--When records are inserted in ER_ATTACHMENTS table, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ERES"."ER_ATTACHMENTS_AI1"
AFTER INSERT ON ERES.ER_ATTACHMENTS 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
  NEW_FK_ATTACHMENT_TYPE  VARCHAR2(200);
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM Dual;
    IF :NEW.FK_ATTACHMENT_TYPE is not null  THEN
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_ATTACHMENT_TYPE)		INTO NEW_FK_ATTACHMENT_TYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_ATTACHMENT_TYPE := '';
   END; 
   END IF;
	
	  --Inserting row in AUDIT_ROW_MODULE table.
	  PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid, 'ER_ATTACHMENTS',:NEW.rid,:NEW.PK_ATTACHMENT,'I',:NEW.Creator);
    
	IF :NEW.PK_ATTACHMENT IS NOT NULL THEN
	  --Inserting rows in AUDIT_COLUMN_MODULE table.
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ER_ATTACHMENTS','PK_ATTACHMENT',NULL,:NEW.PK_ATTACHMENT,NULL,NULL);
    END IF;
    IF :NEW.ENTITY_ID IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ER_ATTACHMENTS','ENTITY_ID',NULL,:NEW.ENTITY_ID,NULL,NULL);
    END IF;
    IF :NEW.ENTITY_TYPE IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ER_ATTACHMENTS','ENTITY_TYPE',NULL,:NEW.ENTITY_TYPE,NULL,NULL);
    END IF;
    IF :NEW.FK_ATTACHMENT_TYPE IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ER_ATTACHMENTS','FK_ATTACHMENT_TYPE',NULL,NEW_FK_ATTACHMENT_TYPE,NULL,NULL);
    END IF;
    IF :NEW.DCMS_FILE_ATTACHMENT_ID IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ER_ATTACHMENTS','DCMS_FILE_ATTACHMENT_ID',NULL,:NEW.DCMS_FILE_ATTACHMENT_ID,NULL,NULL);
    END IF;
    IF :NEW.CREATOR IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ER_ATTACHMENTS','CREATOR',NULL,:NEW.CREATOR,NULL,NULL);
    END IF;
	IF :NEW.CREATED_ON IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ER_ATTACHMENTS','CREATED_ON',NULL,TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
	IF :NEW.IP_ADD IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ER_ATTACHMENTS','IP_ADD',NULL,:NEW.IP_ADD,NULL,NULL);
    END IF;
    IF :NEW.RID IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ER_ATTACHMENTS','RID',NULL,:NEW.RID,NULL,NULL);
    END IF;
    IF :NEW.CREATOR IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ER_ATTACHMENTS','CREATOR',NULL,:NEW.CREATOR,NULL,NULL);
    END IF;
    IF :NEW.LAST_MODIFIED_BY IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ER_ATTACHMENTS','LAST_MODIFIED_BY',NULL,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF :NEW.LAST_MODIFIED_DATE IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ER_ATTACHMENTS','LAST_MODIFIED_DATE',NULL,TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF :NEW.RID IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ER_ATTACHMENTS','RID',NULL,:NEW.RID,NULL,NULL);
    END IF;
    IF :NEW.ATTACHMENTS_TYPE_REM IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ER_ATTACHMENTS','ATTACHMENTS_TYPE_REM',NULL,:NEW.ATTACHMENTS_TYPE_REM,NULL,NULL);
    END IF;
    IF :NEW.DOCUMENT_TYPE IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ER_ATTACHMENTS','DOCUMENT_TYPE',NULL,:NEW.DOCUMENT_TYPE,NULL,NULL);
    END IF;
    IF :NEW.DELETEDFLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ER_ATTACHMENTS','DELETEDFLAG',NULL,:NEW.DELETEDFLAG,NULL,NULL);
    END IF;
    IF :NEW.ATTACHMENT_FILE_NAME IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'ER_ATTACHMENTS','ATTACHMENT_FILE_NAME',NULL,:NEW.ATTACHMENT_FILE_NAME,NULL,NULL);
    END IF;
END;
/