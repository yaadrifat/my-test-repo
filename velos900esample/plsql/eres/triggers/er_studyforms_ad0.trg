CREATE OR REPLACE TRIGGER "ER_STUDYFORMS_AD0" 
AFTER UPDATE--DELETE
ON ER_STUDYFORMS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);
  usr VARCHAR(2000);
rtype CHAR;
BEGIN
  rtype:=:NEW.record_type;
-- Added by Ganapathy on 28/06/05 for Audit Delete
 IF(rtype='D') THEN


  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  	usr := Getuser(:new.last_modified_by);

  Audit_Trail.record_transaction
    (raid, 'ER_STUDYFORMS', :OLD.rid, 'D',usr);

  deleted_data :=
  TO_CHAR(:OLD.pk_studyforms) || '|' ||
  TO_CHAR(:OLD.fk_formlib) || '|' ||
  TO_CHAR(:OLD.fk_study) || '|' ||
  TO_CHAR(:OLD.studyforms_filldate) || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.form_completed) || '|' ||
  TO_CHAR(:OLD.isvalid) || '|' ||
  TO_CHAR(:OLD.notification_sent) || '|' ||
  TO_CHAR(:OLD.process_data) || '|' ||
  TO_CHAR(:OLD.fk_formlibver) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  :OLD.record_type || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add;

INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, deleted_data);
END IF;
END;
/


