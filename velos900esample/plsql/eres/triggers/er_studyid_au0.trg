CREATE OR REPLACE TRIGGER ER_STUDYID_AU0
AFTER UPDATE
ON ER_STUDYID REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  old_study VARCHAR2(1000) ;
  new_study VARCHAR2(1000) ;
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;

  usr VARCHAR2(100);
BEGIN

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'ER_STUDYID', :OLD.rid, 'U', usr);

  IF NVL(:OLD.PK_STUDYID,0) !=
     NVL(:NEW.PK_STUDYID,0) THEN
     audit_trail.column_update
       (raid, 'PK_STUDYID',
       :OLD.PK_STUDYID, :NEW.PK_STUDYID);
  END IF;

  IF NVL(:OLD.fk_study,0) !=
     NVL(:NEW.fk_study,0) THEN
    BEGIN
     SELECT study_title INTO old_study
     FROM er_study WHERE pk_study = :OLD.fk_study ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_study := NULL ;
     END ;
     BEGIN
     SELECT study_title INTO new_study
     FROM er_study WHERE pk_study = :NEW.fk_study ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_study := NULL ;
     END ;
     audit_trail.column_update
       (raid, 'FK_STUDY',
       old_study, new_study);
  END IF;


  IF NVL(:OLD.FK_CODELST_IDTYPE,0) !=
     NVL(:NEW.FK_CODELST_IDTYPE,0) THEN

   SELECT trim(codelst_desc) INTO old_codetype
	FROM er_codelst WHERE pk_codelst =  :OLD.FK_CODELST_IDTYPE ;
	SELECT trim(codelst_desc) INTO new_codetype
	FROM er_codelst WHERE pk_codelst =  :NEW.FK_CODELST_IDTYPE ;
     audit_trail.column_update
       (raid, 'FK_CODELST_IDTYPE',old_codetype, new_codetype);

  END IF;

  IF NVL(:OLD.STUDYID_ID ,' ') !=
     NVL(:NEW.STUDYID_ID ,' ') THEN
     audit_trail.column_update
       (raid, 'STUDYID_ID',
       :OLD.STUDYID_ID, :NEW.STUDYID_ID);
  END IF;


  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;

  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
        to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.IP_ADD,' ') !=
     NVL(:NEW.IP_ADD,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.IP_ADD, :NEW.IP_ADD);
  END IF;
END;
/


