create or replace TRIGGER "ERES"."ER_MILEPAYMENT_BI0" BEFORE INSERT ON ER_MILEPAYMENT
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW    WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  erid NUMBER(10);
  raid NUMBER(10);
  insert_data CLOB;
  usr varchar(2000);
     BEGIN
   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:new.creator);
  Audit_Trail.record_transaction  (raid, 'ER_MILEPAYMENT', erid, 'I', usr );
  insert_data:= :NEW.PK_MILEPAYMENT||'|'|| :NEW.FK_STUDY||'|'||:NEW.FK_MILESTONE||'|'|| TO_CHAR(:NEW.MILEPAYMENT_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.MILEPAYMENT_AMT||'|'||:NEW.MILEPAYMENT_DESC||'|'||:NEW.MILEPAYMENT_DELFLAG||'|'||:NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat) ||'|'||  :NEW.IP_ADD ||'|'||  :NEW.milepayment_comments ||'|'||  :NEW.milepayment_type;
 INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
   END ;
/


