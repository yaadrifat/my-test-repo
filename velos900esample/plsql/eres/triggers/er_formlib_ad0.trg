CREATE OR REPLACE TRIGGER "ER_FORMLIB_AD0" 
  AFTER UPDATE--delete
  ON ER_FORMLIB
  FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

rtype CHAR;
BEGIN
  rtype:=:NEW.record_type;
-- Added by Ganapathy on 28/06/05 for Audit Delete
 IF(rtype='D') THEN


  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'ER_FORMLIB', :OLD.rid, 'D');

  deleted_data :=
  TO_CHAR(:OLD.pk_formlib) || '|' ||
  TO_CHAR(:OLD.fk_catlib) || '|' ||
  TO_CHAR(:OLD.fk_account) || '|' ||
  :OLD.form_name || '|' ||
  :OLD.form_desc || '|' ||
  :OLD.form_sharedwith || '|' ||
  TO_CHAR(:OLD.form_status) || '|' ||
  :OLD.form_linkto || '|' ||
  TO_CHAR(:OLD.form_next) || '|' ||
  :OLD.form_nextmode || '|' ||
  TO_CHAR(:OLD.form_bold) || '|' ||
  TO_CHAR(:OLD.form_italics) || '|' ||
  :OLD.form_align || '|' ||
  TO_CHAR(:OLD.form_underline) || '|' ||
  :OLD.form_color || '|' ||
  :OLD.form_bgcolor || '|' ||
  :OLD.form_font || '|' ||
  TO_CHAR(:OLD.fld_fontsize) || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.form_fillflag) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.form_xslrefresh) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  :OLD.record_type || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add || '|' || :old.FORM_ESIGNREQ;

INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, deleted_data);
END IF;
END;
/


