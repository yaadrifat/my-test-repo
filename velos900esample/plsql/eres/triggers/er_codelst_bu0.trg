create or replace
TRIGGER "ERES"."ER_CODELST_BU0" 
BEFORE UPDATE
ON ERES.ER_CODELST
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW   WHEN ( NEW.last_modified_by is not null) BEGIN
  :NEW.last_modified_date := SYSDATE ;
END;
