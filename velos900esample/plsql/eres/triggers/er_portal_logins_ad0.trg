CREATE OR REPLACE TRIGGER "ER_PORTAL_LOGINS_AD0" AFTER DELETE ON ER_PORTAL_LOGINS
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);

begin
  select seq_audit.nextval into raid from dual;
audit_trail.record_transaction
    (raid, 'ER_PORTAL_LOGINS', :old.rid, 'D');

deleted_data := :OLD.PK_PORTAL_LOGIN||'|'|| :OLD.PL_ID||'|'||
     :OLD.PL_ID_TYPE||'|'|| :OLD.PL_LOGIN||'|'|| :OLD.PL_PASSWORD||'|'||
     :OLD.PL_STATUS||'|'||:OLD.PL_LOGOUT_TIME||'|'|| :OLD.FK_PORTAL||'|'||
     :OLD.RID||'|'|| :OLD.CREATOR||'|'|| :OLD.LAST_MODIFIED_BY||'|'||
     TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
     TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:OLD.IP_ADD;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


