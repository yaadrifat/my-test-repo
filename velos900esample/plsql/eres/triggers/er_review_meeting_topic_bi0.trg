CREATE OR REPLACE TRIGGER "ER_REVIEW_MEETING_TOPIC_BI0"
  BEFORE INSERT ON ER_REVIEW_MEETING_TOPIC  REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
 erid NUMBER(10);
 usr VARCHAR(2000);
 raid NUMBER(10);
 insert_data CLOB;
BEGIN

  BEGIN
    usr := getuser(:NEW.creator);
  EXCEPTION WHEN NO_DATA_FOUND THEN
    usr := 'New User' ;
  END ;

SELECT TRUNC(seq_rid.NEXTVAL) INTO erid FROM dual;
:NEW.rid := erid;
SELECT seq_audit.NEXTVAL INTO raid FROM dual;

audit_trail.record_transaction(raid, 'ER_REVIEW_MEETING_TOPIC', erid, 'I', usr);

insert_data:=:NEW.FK_REVIEW_MEETING||'|'||:NEW.TOPIC_NUMBER||'|'||:NEW.MEETING_TOPIC||'|'||
  TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.CREATOR||'|'||
  TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
  :NEW.LAST_MODIFIED_BY||'|'|| :NEW.IP_ADD||'|'||:NEW.RID;

 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);

END;
/


