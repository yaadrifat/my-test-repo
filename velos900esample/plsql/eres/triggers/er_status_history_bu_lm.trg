CREATE OR REPLACE TRIGGER "ER_STATUS_HISTORY_BU_LM" BEFORE UPDATE ON ER_STATUS_HISTORY
FOR EACH ROW
BEGIN :NEW.LAST_MODIFIED_DATE := SYSDATE;
END;
/


