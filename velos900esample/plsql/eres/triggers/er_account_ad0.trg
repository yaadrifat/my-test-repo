CREATE OR REPLACE TRIGGER "ER_ACCOUNT_AD0" AFTER DELETE ON ER_ACCOUNT
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);
begin
  select seq_audit.nextval into raid from dual;
  audit_trail.record_transaction
    (raid, 'ER_ACCOUNT', :old.rid, 'D');
  deleted_data :=
  to_char(:old.pk_account) || '|' ||
  :old.ac_type || '|' ||
  :old.ac_name || '|' ||
  to_char(:old.ac_strtdt) || '|' ||
  to_char(:old.ac_endt) || '|' ||
  :old.ac_stat || '|' ||
  :old.ac_mailflag || '|' ||
  :old.ac_pubflag || '|' ||
  :old.ac_note || '|' ||
  to_char(:old.ac_usrcreator) || '|' ||
  to_char(:old.rid) || '|' ||
  to_char(:old.creator) || '|' ||
  to_char(:old.last_modified_by) || '|' ||
  to_char(:old.last_modified_date) || '|' ||
  to_char(:old.created_on)  || '|' ||
  to_char(:old.FK_CODELST_ROLE)   || '|' ||
  :old.IP_ADD  || '|' ||
  to_char(:old.AC_AUTOGEN_STUDY) || '|' || --KM-032108
  to_char(:old.AC_AUTOGEN_PATIENT) ;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


