CREATE OR REPLACE TRIGGER "ER_PATFORMS_BI1"
BEFORE INSERT
ON ER_PATFORMS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
new.PATFORMS_FILLDATE is null
      )
declare

  datestr Varchar2(20);
  v_form_xml  sys.xmlType;
  i   NUMBER := 1;
 begin
  v_form_xml := :new.PATFORMS_XML;


          datestr := v_form_xml.extract('/rowset/er_def_date_01/text()').getStringVal();

       if datestr is not null then
       :new.PATFORMS_FILLDATE  := to_date(datestr,PKG_DATEUTIL.f_get_dateformat);
    end if ;

 if :new.PATFORMS_FILLDATE is null then
   :new.PATFORMS_FILLDATE  := sysdate;
     END IF;
 end;
/


