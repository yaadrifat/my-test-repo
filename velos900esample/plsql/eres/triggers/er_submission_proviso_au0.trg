CREATE OR REPLACE TRIGGER ER_SUBMISSION_PROVISO_AU0
AFTER UPDATE
ON ER_SUBMISSION_PROVISO REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
 
  usr VARCHAR2(100);
BEGIN

  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'ER_SUBMISSION_PROVISO', :OLD.rid, 'U', usr);
 
 
 IF NVL(:OLD.FK_SUBMISSION,0) !=
     NVL(:NEW.FK_SUBMISSION,0) THEN
     audit_trail.column_update
       (raid, 'FK_SUBMISSION',
       :OLD.FK_SUBMISSION, :NEW.FK_SUBMISSION);
  END IF;



 IF NVL(:OLD.FK_SUBMISSION_BOARD,0) !=
     NVL(:NEW.FK_SUBMISSION_BOARD,0) THEN
     audit_trail.column_update
       (raid, 'FK_SUBMISSION_BOARD',
       :OLD.FK_SUBMISSION_BOARD, :NEW.FK_SUBMISSION_BOARD);
  END IF;
  
  
  
 IF NVL(:OLD.FK_USER_ENTEREDBY,0) !=
     NVL(:NEW.FK_USER_ENTEREDBY,0) THEN
    
 begin 
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.FK_USER_ENTEREDBY ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.FK_USER_ENTEREDBY ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     
     audit_trail.column_update
       (raid, 'FK_USER_ENTEREDBY',
       old_modby, new_modby);
       
  END IF;
  

  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  
  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  
  IF NVL(:OLD.IP_ADD,' ') !=
     NVL(:NEW.IP_ADD,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.IP_ADD, :NEW.IP_ADD);
  END IF;
  
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     old_modby := NULL ;
    END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.last_modified_by ;
    EXCEPTION WHEN NO_DATA_FOUND THEN
     new_modby := NULL ;
    END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;
  
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
        to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  
 
  IF NVL(:OLD.PROVISO_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.PROVISO_DATE,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'PROVISO_DATE',
        to_char(:OLD.PROVISO_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.PROVISO_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

 if  :old.PROVISO !=  :new.PROVISO then
      audit_trail.column_update  (raid, 'PROVISO',    dbms_lob.substr(:OLD.PROVISO ,4000,1), dbms_lob.substr(:NEW.PROVISO,4000,1));
  end if;

 if nvl(:old.fk_codelst_provisotype,0) !=
     NVL(:new.fk_codelst_provisotype,0) then
     audit_trail.column_update
       (raid, 'FK_CODELST_PROVISOTYPE',
       :old.fk_codelst_provisotype, :new.fk_codelst_provisotype);
  end if;
  
END;
/

ALTER TRIGGER ER_SUBMISSION_PROVISO_AU0 ENABLE
/

