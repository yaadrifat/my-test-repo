CREATE OR REPLACE TRIGGER ER_CRFFORMS_AU0
AFTER UPDATE OF RID,FK_CRF,IP_ADD,ISVALID,FK_FORMLIB,PK_CRFFORMS,RECORD_TYPE,FK_SCHEVENT1,FORM_COMPLETED,LAST_MODIFIED_BY,CRFFORMS_FILLDATE,NOTIFICATION_SENT,LAST_MODIFIED_DATE
ON ER_CRFFORMS REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  usr varchar2(100);
  old_modby varchar2(100) ;
  new_modby varchar2(100) ;

begin
  select seq_audit.nextval into raid from dual;

   usr := getuser(:new.last_modified_by);

  audit_trail.record_transaction
    (raid, 'ER_CRFFORMS', :old.rid, 'U', usr);

  if nvl(:old.pk_crfforms,0) !=
     NVL(:new.pk_crfforms,0) then
     audit_trail.column_update
       (raid, 'PK_CRFFORMS',
       :old.pk_crfforms, :new.pk_crfforms);
  end if;
  if nvl(:old.fk_formlib,0) !=
     NVL(:new.fk_formlib,0) then
     audit_trail.column_update
       (raid, 'FK_FORMLIB',
       :old.fk_formlib, :new.fk_formlib);
  end if;
  if nvl(:old.fk_schevent1,0) !=
     NVL(:new.fk_schevent1,0) then
     audit_trail.column_update
       (raid, 'FK_SCHEVENT1',
       :old.fk_schevent1, :new.fk_schevent1);
  end if;
  if nvl(:old.fk_crf,0) !=
     NVL(:new.fk_crf,0) then
     audit_trail.column_update
       (raid, 'FK_CRF',
       :old.fk_crf, :new.fk_crf);
  end if;
  if nvl(:old.crfforms_filldate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.crfforms_filldate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'CRFFORMS_FILLDATE',
       to_char(:old.crfforms_filldate, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.crfforms_filldate, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.record_type,' ') !=
     NVL(:new.record_type,' ') then
     audit_trail.column_update
       (raid, 'RECORD_TYPE',
       :old.record_type, :new.record_type);
  end if;
  if nvl(:old.form_completed,0) !=
     NVL(:new.form_completed,0) then
     audit_trail.column_update
       (raid, 'FORM_COMPLETED',
       :old.form_completed, :new.form_completed);
  end if;
  if nvl(:old.isvalid,0) !=
     NVL(:new.isvalid,0) then
     audit_trail.column_update
       (raid, 'ISVALID',
       :old.isvalid, :new.isvalid);
  end if;
  if nvl(:old.notification_sent,0) !=
     NVL(:new.notification_sent,0) then
     audit_trail.column_update
       (raid, 'NOTIFICATION_SENT',
       :old.notification_sent, :new.notification_sent);
  end if;


 if nvl(:old.process_data,0) !=

     NVL(:new.process_data,0) then

     audit_trail.column_update

       (raid, 'PROCESS_DATA',

       :old.process_data, :new.process_data);

 end if;


 if nvl(:old.fk_formlibver,0) !=

     NVL(:new.fk_formlibver,0) then

     audit_trail.column_update

       (raid, 'FK_FORMLIBVER',

       :old.fk_formlibver, :new.fk_formlibver);

  end if;


  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
   Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into old_modby
     from er_user
     where pk_user = :old.last_modified_by ;
     Exception When NO_DATA_FOUND then
      old_modby := null ;
     End ;
    Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.last_modified_by ;
     Exception When NO_DATA_FOUND then
      new_modby := null ;
     End ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  end if;
  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;

end;
/


