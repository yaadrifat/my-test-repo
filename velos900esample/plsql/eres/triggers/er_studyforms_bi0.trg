create or replace TRIGGER "ERES"."ER_STUDYFORMS_BI0" BEFORE INSERT ON ER_STUDYFORMS
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
 WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      ) DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
   BEGIN
 --KM-#3635 
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM ER_USER
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'ER_STUDYFORMS',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
      insert_data:= :NEW.PK_STUDYFORMS||'|'|| :NEW.FK_FORMLIB||'|'||
    :NEW.FK_STUDY||'|'||
    TO_CHAR(:NEW.STUDYFORMS_FILLDATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| :NEW.RID||'|'||
     :NEW.FORM_COMPLETED||'|'|| :NEW.ISVALID||'|'|| :NEW.CREATOR||'|'||
   :NEW.LAST_MODIFIED_BY||'|'||:NEW.RECORD_TYPE||'|'||
    TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
     TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD||'|'||
      :NEW.NOTIFICATION_SENT||'|'|| :NEW.PROCESS_DATA||'|'|| :NEW.FK_FORMLIBVER || '|' || :NEW.FK_SPECIMEN;
 INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
   END ;
/


