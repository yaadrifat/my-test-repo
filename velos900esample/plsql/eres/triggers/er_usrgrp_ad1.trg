CREATE OR REPLACE TRIGGER "ER_USRGRP_AD1" 
AFTER DELETE
ON ER_USRGRP
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
BEGIN

/* Author: Sonika on Nov 25, 03
 Purpose : Remove the deleted user from the Form
*/

/*DELETE FROM ER_FORMSHAREWITH
WHERE fk_formlib IN
(SELECT fk_formlib
 FROM ER_FORMSHAREWITH
 WHERE fsw_type = 'G' AND fsw_id = :OLD.fk_grp)
 AND fsw_type = 'U'
AND fsw_id = :OLD.fk_user ;*/

/*
 May 03, 04
 Purpose : Remove the deleted user from the Object share
*/

/* Modified by Sonia Abrol, 06/02/05 to delete user rows only if user record that belongs to the group's object share*/

DELETE FROM ER_OBJECTSHARE
WHERE fk_object IN
	  			(SELECT fk_object
				 FROM ER_OBJECTSHARE
				  WHERE objectshare_type = 'G' AND fk_objectshare_id = :OLD.fk_grp)
 AND objectshare_type = 'U'
AND fk_objectshare_id = :OLD.fk_user AND
fk_objectshare IN (SELECT pk_objectshare FROM ER_OBJECTSHARE i
				  WHERE i.objectshare_type = 'G' AND i.fk_objectshare_id = :OLD.fk_grp AND i.fk_object = fk_object)  ;


END;
/


