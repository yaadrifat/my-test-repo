CREATE OR REPLACE TRIGGER "ER_STORAGE_KIT_AD0" AFTER DELETE ON ER_STORAGE_KIT
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(2000);
begin
  select seq_audit.nextval into raid from dual;
audit_trail.record_transaction
    (raid, 'ER_STORAGE', :old.rid, 'D');

deleted_data :=
to_char(:old.PK_STORAGE_KIT)||'|'||
to_char(:old.FK_STORAGE)||'|'||
to_char(:old.DEF_SPECIMEN_TYPE)||'|'||
:old.DEF_PROCESSING_TYPE||'|'||
:old.DEF_SPEC_PROCESSING_SEQ||'|'||
to_char(:old.DEF_SPEC_QUANTITY)||'|'||
to_char(:old.DEF_SPEC_QUANTITY_UNIT)||'|'||
to_char(:old.KIT_SPEC_DISPOSITION)||'|'||
to_char(:old.KIT_SPEC_ENVT_CONS)||'|'||
to_char(:old.RID)||'|'||
to_char(:old.CREATOR)||'|'||
to_char(:old.CREATED_ON)||'|'||
to_char(:old.LAST_MODIFIED_BY)||'|'||
to_char(:old.LAST_MODIFIED_DATE)||'|'||
:old.IP_ADD;

insert into AUDIT_DELETE
(raid, row_data) values (raid, deleted_data);
end;
/


