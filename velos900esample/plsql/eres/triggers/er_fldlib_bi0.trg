create or replace TRIGGER "ER_FLDLIB_BI0" BEFORE INSERT ON ER_FLDLIB
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW   WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
 BEGIN

 --KM-#3635
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

                         SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction(raid, 'ER_FLDLIB',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.FLD_OVERRIDE_FORMAT||'|'||
    :NEW.FLD_OVERRIDE_RANGE||'|'||
    :NEW.FLD_OVERRIDE_DATE||'|'||
    :NEW.FLD_HIDERESPLABEL||'|'||
    :NEW.FLD_DISPLAY_WIDTH||'|'|| :NEW.FLD_LINKEDFORM||'|'||
    :NEW.FLD_RESPALIGN||'|'||:NEW.FLD_SORTORDER||'|'||
    :NEW.FLD_HIDELABEL||'|'||:NEW.FLD_NAME_FORMATTED||'|'||
    :NEW.PK_FIELD||'|'|| :NEW.FK_CATLIB||'|'|| :NEW.FK_ACCOUNT||'|'||
    :NEW.FLD_LIBFLAG||'|'||:NEW.FLD_NAME||'|'||:NEW.FLD_DESC||'|'||
    :NEW.FLD_UNIQUEID||'|'||:NEW.FLD_SYSTEMID||'|'||:NEW.FLD_KEYWORD||'|'||
    :NEW.FLD_TYPE||'|'||:NEW.FLD_DATATYPE||'|'||:NEW.FLD_INSTRUCTIONS||'|'||
    :NEW.FLD_LENGTH||'|'|| :NEW.FLD_DECIMAL||'|'|| :NEW.FLD_LINESNO||'|'||
    :NEW.FLD_CHARSNO||'|'||:NEW.FLD_DEFRESP||'|'|| :NEW.FK_LOOKUP||'|'||
    :NEW.FLD_ISUNIQUE||'|'|| :NEW.FLD_ISREADONLY||'|'|| :NEW.FLD_ISVISIBLE||'|'||
    :NEW.FLD_COLCOUNT||'|'||:NEW.FLD_FORMAT||'|'|| :NEW.FLD_REPEATFLAG||'|'|| :NEW.FLD_BOLD||'|'||
    :NEW.FLD_ITALICS||'|'|| :NEW.FLD_SAMELINE||'|'||:NEW.FLD_ALIGN||'|'||
    :NEW.FLD_UNDERLINE||'|'||:NEW.FLD_COLOR||'|'||:NEW.FLD_FONT||'|'||:NEW.RECORD_TYPE||'|'||
    :NEW.FLD_FONTSIZE||'|'||:NEW.FLD_LKPDATAVAL||'|'|| :NEW.RID||'|'||:NEW.FLD_LKPDISPVAL||'|'||
    :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
    TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD||'|'||
    :NEW.FLD_TODAYCHECK||'|'|| :NEW.FLD_OVERRIDE_MANDATORY||'|'||:NEW.FLD_LKPTYPE||'|'||
    :NEW.FLD_EXPLABEL;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END ;
/


