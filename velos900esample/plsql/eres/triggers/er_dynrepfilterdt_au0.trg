CREATE OR REPLACE TRIGGER ER_DYNREPFILTERDT_AU0 AFTER UPDATE ON ER_DYNREPFILTERDT FOR EACH ROW
declare
   raid number(10);
   usr varchar2(100);
   old_modby varchar2(100);
   new_modby varchar2(100);
begin
  select seq_audit.nextval into raid from dual;
  usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction(raid, 'ER_DYNREPFILTERDT', :old.rid, 'U', usr);

  if nvl(:old.PK_DYNREPFILTERDT,0) != NVL(:new.PK_DYNREPFILTERDT,0) then
     audit_trail.column_update(raid, 'PK_DYNREPFILTERDT',:old.PK_DYNREPFILTERDT, :new.PK_DYNREPFILTERDT);
  end if;

  if nvl(:old.DYNREPFILTERDT_COL,' ') !=
     NVL(:new.DYNREPFILTERDT_COL,' ') then
     audit_trail.column_update
       (raid, 'DYNREPFILTERDT_COL',
       :old.DYNREPFILTERDT_COL, :new.DYNREPFILTERDT_COL);
  end if;

  if nvl(:old.DYNREPFILTERDT_QF,' ') !=
     NVL(:new.DYNREPFILTERDT_QF,' ') then
     audit_trail.column_update
       (raid, 'DYNREPFILTERDT_QF',
       :old.DYNREPFILTERDT_QF, :new.DYNREPFILTERDT_QF);
  end if;

  if nvl(:old.DYNREPFILTERDT_DATA,' ') !=
     NVL(:new.DYNREPFILTERDT_DATA,' ') then
     audit_trail.column_update
       (raid, 'DYNREPFILTERDT_DATA',
       :old.DYNREPFILTERDT_DATA, :new.DYNREPFILTERDT_DATA);
  end if;

  if nvl(:old.DYNREPFILTERDT_EXCLUDE,' ') !=
     NVL(:new.DYNREPFILTERDT_EXCLUDE,' ') then
     audit_trail.column_update
       (raid, 'DYNREPFILTERDT_EXCLUDE',
       :old.DYNREPFILTERDT_EXCLUDE, :new.DYNREPFILTERDT_EXCLUDE);
  end if;

  if nvl(:old.DYNREPFILTERDT_BBRAC,' ') !=
     NVL(:new.DYNREPFILTERDT_BBRAC,' ') then
     audit_trail.column_update
       (raid, 'DYNREPFILTERDT_BBRAC',
       :old.DYNREPFILTERDT_BBRAC, :new.DYNREPFILTERDT_BBRAC);
  end if;

  if nvl(:old.DYNREPFILTERDT_EBRAC,' ') !=
     NVL(:new.DYNREPFILTERDT_EBRAC,' ') then
     audit_trail.column_update
       (raid, 'DYNREPFILTERDT_EBRAC',
       :old.DYNREPFILTERDT_EBRAC, :new.DYNREPFILTERDT_EBRAC);
  end if;

  if nvl(:old.DYNREPFILTERDT_OPER,' ') !=
     NVL(:new.DYNREPFILTERDT_OPER,' ') then
     audit_trail.column_update
       (raid, 'DYNREPFILTERDT_OPER',
       :old.DYNREPFILTERDT_OPER, :new.DYNREPFILTERDT_OPER);
  end if;


  if nvl(:old.DYNREPFILTERDT_SEQ,0) !=
     NVL(:new.DYNREPFILTERDT_SEQ,0) then
     audit_trail.column_update
       (raid, 'DYNREPFILTERDT_SEQ',
       :old.DYNREPFILTERDT_SEQ, :new.DYNREPFILTERDT_SEQ);
  end if;

  if nvl(:old.FK_DYNREPFILTER,0) !=
     NVL(:new.FK_DYNREPFILTER,0) then
     audit_trail.column_update
       (raid, 'FK_DYNREPFILTER',
       :old.FK_DYNREPFILTER, :new.FK_DYNREPFILTER);
  end if;

  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;

  if nvl(:old.LAST_MODIFIED_BY,0) != NVL(:new.LAST_MODIFIED_BY,0) then
     Begin
	Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
	into old_modby from er_user  where pk_user = :old.last_modified_by ;
     Exception When NO_DATA_FOUND then
	old_modby := null;
     End ;
     Begin
	Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
	into new_modby   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
     Exception When NO_DATA_FOUND then
	new_modby := null;
     End ;
     audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
  end if;

  if nvl(:old.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:new.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) then
     audit_trail.column_update(raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;

  if nvl(:old.ip_add,' ') != NVL(:new.ip_add,' ') then
     audit_trail.column_update(raid, 'IP_ADD',:old.ip_add, :new.ip_add);
  end if;

  if nvl(:old.DYNREPFILTERDT_NAME,' ') !=
     NVL(:new.DYNREPFILTERDT_NAME,' ') then
     audit_trail.column_update
       (raid, 'DYNREPFILTERDT_NAME',
       :old.DYNREPFILTERDT_NAME, :new.DYNREPFILTERDT_NAME);
  end if;
end;
/


