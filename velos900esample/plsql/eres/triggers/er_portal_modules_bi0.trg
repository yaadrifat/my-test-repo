CREATE OR REPLACE TRIGGER "ER_PORTAL_MODULES_BI0" BEFORE INSERT ON ER_PORTAL_MODULES
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN
   BEGIN
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname INTO usr FROM er_user
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   audit_trail.record_transaction(raid, 'ER_PORTAL_MODULES',erid, 'I',usr);

   insert_data:= :NEW.PK_PORTAL_MODULES||'|'||:NEW.FK_PORTAL||'|'||:NEW.FK_ID||'|'|| :NEW.PM_TYPE||'|'||:NEW.PM_ALIGN||'|'||:NEW.PM_FROM||'|'||
   :NEW.PM_TO||'|'||:NEW.PM_FROM_UNIT||'|'||:NEW.PM_TO_UNIT||'|'||:NEW.PM_FORM_AFTER_RESP||'|'||:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||
   TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)
   ||'|'||:NEW.IP_ADD;

   INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/


