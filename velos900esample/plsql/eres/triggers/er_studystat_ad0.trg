CREATE OR REPLACE TRIGGER ER_STUDYSTAT_AD0 AFTER DELETE ON ER_STUDYSTAT
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);
  usr VARCHAR(2000);
BEGIN

  usr := Getuser(:OLD.last_modified_by);


  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction
    (raid, 'ER_STUDYSTAT', :OLD.rid, 'D',usr);


  deleted_data :=
  TO_CHAR(:OLD.pk_studystat) || '|' ||
  TO_CHAR(:OLD.fk_user_docby) || '|' ||
  TO_CHAR(:OLD.fk_codelst_studystat) || '|' ||
  TO_CHAR(:OLD.fk_study) || '|' ||
  TO_CHAR(:OLD.studystat_date) || '|' ||
  :OLD.studystat_hspn || '|' ||
  :OLD.studystat_note || '|' ||
  TO_CHAR(:OLD.studystat_endt) || '|' ||
  TO_CHAR(:OLD.fk_codelst_aprstat) || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  TO_CHAR(:OLD.fk_codelst_aprno) || '|' ||
  :OLD.ip_add || '|' ||
  TO_CHAR(:OLD.current_stat)  || '|' ||
  TO_CHAR(:OLD.STUDYSTAT_VALIDT,PKG_DATEUTIL.F_GET_DATEFORMAT) || '|' ||
  TO_CHAR(:OLD.FK_SITE) || '|' ||
  TO_CHAR(:OLD.outcome) || '|' ||
  TO_CHAR(:OLD.status_type) || '|' ||
  TO_CHAR(:OLD.studystat_meetdt) || '|' ||
  :OLD.fk_codelst_revboard ||'|'|| :OLD.studystat_assignedto;

INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, deleted_data);
END;
/


