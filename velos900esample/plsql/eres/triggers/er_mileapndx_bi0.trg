create or replace TRIGGER "ERES"."ER_MILEAPNDX_BI0" BEFORE INSERT ON ER_MILEAPNDX
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW   WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  erid NUMBER(10);
  raid NUMBER(10);
  insert_data CLOB;
  usr VARCHAR(2000);
     BEGIN

                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  --KM-#3634
 usr := getuser(:new.creator);
audit_trail.record_transaction(raid, 'ER_MILEAPNDX',erid, 'I',  usr);
       insert_data:= :NEW.PK_MILEAPNDX||'|'|| :NEW.FK_STUDY||'|'||
     :NEW.FK_MILESTONE||'|'||:NEW.MILEAPNDX_DESC||'|'||:NEW.MILEAPNDX_URI||'|'||
  :NEW.MILEAPNDX_TYPE||'|'|| :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
   TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
   TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD;

 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);

   END;
/


