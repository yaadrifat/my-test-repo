CREATE OR REPLACE PROCEDURE        "SP_DELETE_STORAGES" (p_ids ARRAY_STRING,o_ret OUT NUMBER)
IS
   v_cnt NUMBER;
   i NUMBER;
   v_id_str VARCHAR2(100);
   v_inStr VARCHAR2(500);
   v_temp VARCHAR2(100);
   v_sql VARCHAR2(1000);
   v_sql1 VARCHAR2(1000);
   v_sql2 VARCHAR2(1000);
BEGIN

   v_cnt := p_ids.COUNT; --get the # of elements in array

   i:=1;
	WHILE i <= v_cnt LOOP
 	  v_temp:='to_number('''||p_ids(i)||''')';

	IF (LENGTH(v_inStr) > 0) THEN
	v_inStr:=v_inStr||', '||v_temp ;
	ELSE
	v_inStr:= '('||v_temp ;
	END IF;

	i := i+1;
 END LOOP;

	v_inStr:=v_inStr||')' ;


	BEGIN
          v_sql2:= 'delete from er_allowed_items  where  fk_storage in ' || v_inStr;
  	  EXECUTE IMMEDIATE v_sql2 ;
	  commit;
          v_sql1:= 'delete from er_storage_status  where  fk_storage in ' || v_inStr;
  	  EXECUTE IMMEDIATE v_sql1 ;
	  commit;
          v_sql:='delete from er_storage where pk_storage in ' || v_inStr;
	  EXECUTE IMMEDIATE v_sql ;
	  COMMIT;
      EXCEPTION  WHEN OTHERS THEN
        P('ERROR');
        o_ret:=-1;
	   RETURN;
      END ;--end of insert begin
   o_ret:=0;

 END;
/


