CREATE OR REPLACE PROCEDURE "SP_GET_PAGE_RECORDS"
 (
  p_page NUMBER,
  p_recs_per_page NUMBER,
  p_sql LONG, -- changed varchar2 to clob and clob to long to accomodate the need of huge sqls
  p_countsql LONG,
  o_res OUT Gk_Cv_Types.GenericCursorType,
  o_rows OUT NUMBER
 )
IS
-- Find out the first and last record we want
FirstRec NUMBER;
LastRec NUMBER;
V_POS      NUMBER         := 0;
V_FROM VARCHAR2(500);
--V_CTSELECT Varchar2(4000);
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'Sp_Get_Page_Records', pLEVEL  => Plog.LFATAL);

BEGIN

SELECT (p_page - 1) * p_recs_per_page
INTO FirstRec
FROM dual;

SELECT p_page * p_recs_per_page + 1
INTO LastRec
FROM dual;

-- Now, return the set of paged records
BEGIN

OPEN o_res FOR TO_CHAR('select * from ( select a.*, rownum rnum
      FROM (' || p_sql ||') a
      WHERE ROWNUM < ' || TO_CHAR(LastRec) || ' )
      WHERE rnum > ' || TO_CHAR(FirstRec)) ;
--Commented by Sonika on Feb 25,03
--V_CTSELECT := p_countsql ;

  EXECUTE IMMEDIATE to_char(p_countsql) INTO o_rows;

  EXCEPTION WHEN OTHERS THEN
      PLOG.FATAL(PCTX,'Sp_Get_Page_Records SQLERRM :' || SQLERRM ||':');
END     ;

END Sp_Get_Page_Records;
/


CREATE SYNONYM ESCH.SP_GET_PAGE_RECORDS FOR SP_GET_PAGE_RECORDS;


CREATE SYNONYM EPAT.SP_GET_PAGE_RECORDS FOR SP_GET_PAGE_RECORDS;


GRANT EXECUTE, DEBUG ON SP_GET_PAGE_RECORDS TO EPAT;

GRANT EXECUTE, DEBUG ON SP_GET_PAGE_RECORDS TO ESCH;

