CREATE OR REPLACE PACKAGE BODY        "PKG_PM_RULE"
AS
 PROCEDURE SP_CHK_PAT_RULE_ENR_ALL (
    p_study        NUMBER,
    p_org_id       NUMBER,
    p_milcount NUMBER,
    p_stdate DATE,
    p_enddate DATE,
    o_result OUT  NUMBER,
    o_date OUT DATE
   )
   /* Author: Sonia Sahni
   Date: 3rd June 2002
   Modified on 26th June
    Purpose - Returns 0 if Patient Type Milestone  - pm_2 is not met
              Returns 1 if Patient Type Milestone  - pm_2 is met
		    Returns the date of achievement of milestone
		    Returns Null date if milestone is not achieved
     PM_2 - Enrolled patients, any status   */
   AS
    v_count NUMBER;
    v_min NUMBER;
    v_max NUMBER;
    v_pat NUMBER;
    v_ach_date DATE;
    v_rnum NUMBER;
    V_CUR   TYPES.cursorType;
    BEGIN
     o_result := 0;
     v_count := 0;
	v_min := p_milcount - 1;
	v_max  := p_milcount +  1;
    BEGIN
    --date comparison changed by Sonika on Sep 13, 2002

   IF P_ORG_ID > 0 THEN
     OPEN V_CUR FOR SELECT  FK_PER,PATSTUDYSTAT_DATE,rnum
             FROM ( SELECT a.*, ROWNUM rnum
                FROM (SELECT   DISTINCT  FK_PER, PATSTUDYSTAT_DATE
                       FROM ERV_ALLPATSTUDYSTAT_ALLSTAT ov WHERE FK_STUDY = p_study AND
				   per_site = p_org_id AND
                       TO_DATE(TO_CHAR( PATSTUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) >= p_stdate AND
                       TO_DATE(TO_CHAR( PATSTUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) <= p_enddate AND
                      PATSTUDYSTAT_DATE =  (SELECT MAX(PATSTUDYSTAT_DATE)
				  			  FROM  ERV_ALLPATSTUDYSTAT_ALLSTAT ev
							  WHERE ev.FK_STUDY = ov.fk_study AND
                               TO_DATE(TO_CHAR( ev.PATSTUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  >= p_stdate  AND
                               TO_DATE(TO_CHAR( ev.PATSTUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)   <= p_enddate AND
                         ev.fk_per = ov.fk_per ) ORDER BY PATSTUDYSTAT_DATE ) a
			  WHERE ROWNUM < v_max )
	 WHERE rnum > v_min ;
  ELSE
     OPEN V_CUR FOR SELECT  FK_PER,PATSTUDYSTAT_DATE,rnum
           FROM ( SELECT a.*, ROWNUM rnum
                FROM (SELECT   DISTINCT  FK_PER, PATSTUDYSTAT_DATE
                       FROM ERV_ALLPATSTUDYSTAT_ALLSTAT ov WHERE FK_STUDY = p_study AND
                       TO_DATE(TO_CHAR( PATSTUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) >= p_stdate AND
                       TO_DATE(TO_CHAR( PATSTUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) <= p_enddate AND
                      PATSTUDYSTAT_DATE =  (SELECT MAX(PATSTUDYSTAT_DATE)
				  			  FROM  ERV_ALLPATSTUDYSTAT_ALLSTAT ev
							  WHERE ev.FK_STUDY = ov.fk_study AND
                               TO_DATE(TO_CHAR( ev.PATSTUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)  >= p_stdate  AND
                               TO_DATE(TO_CHAR( ev.PATSTUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)   <= p_enddate AND
                         ev.fk_per = ov.fk_per ) ORDER BY PATSTUDYSTAT_DATE ) a
			  WHERE ROWNUM < v_max )
	 WHERE rnum > v_min ;
  END IF;

  LOOP
    FETCH V_CUR INTO V_PAT, v_ach_date, V_RNUM;
    EXIT WHEN V_CUR%NOTFOUND;
    IF v_ach_date IS NULL THEN
            o_result := 0	;
    	  ELSE
            o_result := 1	;
	  END IF;
	  o_date := v_ach_date ;

   END LOOP;
   CLOSE V_CUR;

   EXCEPTION WHEN NO_DATA_FOUND THEN  o_result := 0	;
    END;
   END SP_CHK_PAT_RULE_ENR_ALL;
--------------------------------------------------
    PROCEDURE SP_CHK_PAT_RULE_ENR_ACT (
     p_study        NUMBER,
	p_org_id       NUMBER,
     p_milcount NUMBER,
     p_stdate DATE,
     p_enddate DATE,
     o_result OUT  NUMBER,
     o_date OUT DATE
   )
    /* Author: Sonia Sahni
    Date: 3rd June 2002
    Modified on: 27 Jun 2002
    Purpose - Returns 0 if Patient Type Milestone  - pm_1 is not met
              Returns 1 if Patient Type Milestone  - pm_1 is met
    PM_1 -  Enrolled patients, active status
   */
     AS
    v_count NUMBER;
    v_min NUMBER;
    v_max NUMBER;
    v_pat NUMBER;
    v_ach_date DATE;
    v_rnum NUMBER;
    V_CUR   TYPES.cursorType;
    BEGIN
     o_result := 0;
     v_count := 0;
	v_min := p_milcount - 1;
	v_max  := p_milcount +  1;
    BEGIN
    --date comparison changed by Sonika on Sep 13, 2002
   IF P_ORG_ID > 0 THEN
       OPEN V_CUR FOR SELECT  FK_PER,PATSTUDYSTAT_DATE,rnum
           FROM ( SELECT a.*, ROWNUM rnum
                FROM (SELECT   DISTINCT  FK_PER, PATSTUDYSTAT_DATE
                       FROM ERV_ALLPATSTUDYSTAT_ALLSTAT ov WHERE FK_STUDY = p_study AND
				    per_site = p_org_id AND
                        TO_DATE(TO_CHAR( PATSTUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) >= p_stdate AND
                        TO_DATE(TO_CHAR( PATSTUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) <= p_enddate AND
				   status_codelst_subtyp = 'active' AND
                       PATSTUDYSTAT_DATE =  (SELECT MAX(PATSTUDYSTAT_DATE) FROM
                               ERV_ALLPATSTUDYSTAT_ALLSTAT ev
						 WHERE ev.FK_STUDY = ov.fk_study AND
                               TO_DATE(TO_CHAR( ev.PATSTUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) >= p_stdate  AND
                               TO_DATE(TO_CHAR( ev.PATSTUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) <= p_enddate AND
                         ev.fk_per = ov.fk_per ) ORDER BY PATSTUDYSTAT_DATE ) a
			  WHERE ROWNUM < v_max )
	 WHERE rnum > v_min ;
 ELSE
       OPEN V_CUR FOR SELECT  FK_PER,PATSTUDYSTAT_DATE,rnum
           FROM ( SELECT a.*, ROWNUM rnum
                FROM (SELECT   DISTINCT  FK_PER, PATSTUDYSTAT_DATE
                       FROM ERV_ALLPATSTUDYSTAT_ALLSTAT ov WHERE FK_STUDY = p_study AND
                        TO_DATE(TO_CHAR( PATSTUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) >= p_stdate AND
                        TO_DATE(TO_CHAR( PATSTUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) <= p_enddate AND
				   status_codelst_subtyp = 'active' AND
                       PATSTUDYSTAT_DATE =  (SELECT MAX(PATSTUDYSTAT_DATE) FROM
                               ERV_ALLPATSTUDYSTAT_ALLSTAT ev
						 WHERE ev.FK_STUDY = ov.fk_study AND
                               TO_DATE(TO_CHAR( ev.PATSTUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) >= p_stdate  AND
                               TO_DATE(TO_CHAR( ev.PATSTUDYSTAT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) <= p_enddate AND
                         ev.fk_per = ov.fk_per ) ORDER BY PATSTUDYSTAT_DATE ) a
			  WHERE ROWNUM < v_max )
	 WHERE rnum > v_min ;
 END IF;

  LOOP
     FETCH V_CUR INTO v_pat, v_ach_date, v_rnum ;
    EXIT WHEN V_CUR%NOTFOUND;
       IF v_ach_date IS NULL THEN
            o_result := 0	;
    	  ELSE
            o_result := 1	;
	  END IF;

	  o_date := v_ach_date ;

   END LOOP;
   CLOSE V_CUR;
      EXCEPTION WHEN NO_DATA_FOUND THEN  o_result := 0	;
    END;

   END SP_CHK_PAT_RULE_ENR_ACT;
---------------------------------------------------------------------------------------------------------------

 PROCEDURE SP_RULE_CHK_VIS_DONE_ONE (
   p_study        NUMBER,
   p_org_id       NUMBER,
   p_milcount        NUMBER,
   p_visit        NUMBER,
   p_stdate DATE,
   p_enddate DATE,
   o_result OUT  NUMBER,
   o_date OUT DATE
 )
   /* Author: Sonia Sahni
    Date: 17th June 2002
    Modified on 21st June 2002
    Purpose - Returns 1 if Patient Type Milestone  - pm_4 is completed
              Returns 1 if Patient Type Milestone  - pm_4 is not completed
		    Returns the date of achievement of milestone
		    Returns Null date if milestone is not achieved
    PM_4 - Patient's first visit (at least one event) is marked as 'Done'
    IN USE!!
   */
   AS
    v_count NUMBER;
    v_min NUMBER;
    v_max NUMBER;
    v_patprot NUMBER;
    v_ach_date DATE;
    v_rnum NUMBER;
    V_CUR   TYPES.cursorType;
    BEGIN
     v_count := 0;
	v_min := p_milcount - 1;
	v_max  := p_milcount +  1;

     o_result := 0;
    BEGIN
    --date comparison changed by Sonika on Sep 13, 2002
   IF P_ORG_ID > 0 THEN
       OPEN V_CUR FOR SELECT  pk_patprot,EVENT_EXEON,rnum
             FROM ( SELECT pk_patprot, TO_DATE(TO_CHAR(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) EVENT_EXEON , ROWNUM rnum
                                FROM (SELECT pk_patprot, MIN(TO_DATE(TO_CHAR(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)) EVENT_EXEON
						  FROM ERV_PATSTUDY_EVEALL a
                              WHERE FK_STUDY= p_study AND visit = p_visit AND
						per_site = p_org_id AND
					     TO_DATE(TO_CHAR( EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) BETWEEN p_stdate AND  p_enddate AND
	                        EVENT_STATUS_CODELST_SUBTYP = 'ev_done' AND
                           PK_EVENTSTAT = (SELECT MAX(PK_EVENTSTAT)
                           FROM ERV_PATSTUDY_EVEALL b
                           WHERE b.SCH_EVENTS1_PK = a.SCH_EVENTS1_PK AND
     	                 TO_DATE(TO_CHAR( EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) >= p_stdate
					  AND   TO_DATE(TO_CHAR( EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) <= p_enddate
                             ) GROUP BY pk_patprot ORDER BY 2  ) a
			  WHERE ROWNUM < v_max )
 	 WHERE rnum > v_min ;
  ELSE
     OPEN V_CUR FOR SELECT  pk_patprot,EVENT_EXEON,rnum
             FROM ( SELECT pk_patprot, TO_DATE(TO_CHAR(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) EVENT_EXEON , ROWNUM rnum
                                FROM (SELECT pk_patprot, MIN(TO_DATE(TO_CHAR(EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)) EVENT_EXEON
						  FROM ERV_PATSTUDY_EVEALL a
                              WHERE FK_STUDY= p_study AND visit = p_visit AND
					     TO_DATE(TO_CHAR( EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) BETWEEN p_stdate AND  p_enddate AND
	                        EVENT_STATUS_CODELST_SUBTYP = 'ev_done' AND
                           PK_EVENTSTAT = (SELECT MAX(PK_EVENTSTAT)
                           FROM ERV_PATSTUDY_EVEALL b
                           WHERE b.SCH_EVENTS1_PK = a.SCH_EVENTS1_PK AND
     	                 TO_DATE(TO_CHAR( EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) >= p_stdate
					  AND   TO_DATE(TO_CHAR( EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) <= p_enddate
                             ) GROUP BY pk_patprot ORDER BY 2  ) a
			  WHERE ROWNUM < v_max )
 	 WHERE rnum > v_min ;
  END IF;

  LOOP
	  FETCH V_CUR INTO v_patprot, v_ach_date, v_rnum;
       EXIT WHEN V_CUR%NOTFOUND;
       IF v_ach_date IS NULL THEN
            o_result := 0	;
    	  ELSE
            o_result := 1	;
	  END IF;
	  o_date := v_ach_date ;

   END LOOP;
    CLOSE V_CUR;

     EXCEPTION WHEN NO_DATA_FOUND THEN  o_result := 0	;
    END;



   END SP_RULE_CHK_VIS_DONE_ONE;
----------------------------------------------------------------------------------

PROCEDURE SP_RULE_CHK_VIS_DONE_COMPLETE (
   p_study        NUMBER,
   p_org_id       NUMBER,
   p_milcount        NUMBER,
   p_stdate DATE,
   p_enddate DATE,
   o_result OUT  NUMBER,
   o_date OUT DATE
   )
   /* Author: Sonia Sahni
    Date: 19th June 2002
    Modified on: 25 June 2002
    Purpose - Returns 1 if Patient Type Milestone  - pm_5 is completed
              Returns 1 if Patient Type Milestone  - pm_5 is not completed
		    Returns the date of achievement of milestone
		    Returns Null date if milestone is not achieved
        PM_5 - Patient's first visit (all events) is marked as 'Done'
	   IN USE!!
   */
   AS
   v_count NUMBER;
    v_min NUMBER;
    v_max NUMBER;
    v_patprot NUMBER;
    v_ach_date DATE := NULL ;
    v_rnum NUMBER ;
    v_tmpdate VARCHAR2(10) := NULL;
    V_CUR   TYPES.cursorType;
    BEGIN
     v_count := 0;
	v_min := p_milcount - 1;
	v_max  := p_milcount +  1;
	o_result := 0;
	BEGIN
     --date comparison changed by Sonika on Sep 13, 2002
     IF P_ORG_ID > 0 THEN
       OPEN V_CUR FOR SELECT  pk_patprot ,event_exeon
             FROM ( SELECT t_ev.pk_patprot , TO_DATE(TO_CHAR(t_ev.event_exeon,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) event_exeon, ROWNUM rnum
             FROM (
		        SELECT v.pk_patprot , MAX(TO_DATE(TO_CHAR(ev.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT))  event_exeon
                  FROM ER_PATPROT v, SCH_EVENTS1 s, SCH_CODELST c,
			   sch_eventstat ev, ER_PER P
                  WHERE v.fk_study = p_study AND
                  v.PK_PATPROT = s.fk_patprot AND
			   v.fk_per = P.pk_per AND
			   P.fk_site = p_org_id AND
                  s.EVENT_ID = ev.FK_EVENT AND
                  c.pk_codelst = ev.EVENTSTAT AND
                  c.codelst_type = 'eventstatus'
                  AND trim(c.codelst_subtyp) = 'ev_done' AND
                  s.visit = 1
                  AND
                  v.pk_patprot IN
                        (SELECT b.pk_patprot FROM ( SELECT PK_PATPROT, TOT_EVENT_COUNT                                                                            FROM ERV_VISIT1_EVCOUNT WHERE
                                                  fk_study = p_study ) a ,
										 (SELECT PK_PATPROT, TOT_DONE_EVENT_COUNT
                                                    FROM ERV_VISIT1_DONE_EVCOUNT
                                                   WHERE fk_study = p_study) b
                          WHERE a.pk_patprot=b.pk_patprot
                          AND a.TOT_EVENT_COUNT = b.TOT_DONE_EVENT_COUNT
                          )
                 GROUP BY v.pk_patprot
                 HAVING MAX(TO_DATE(TO_CHAR(ev.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) ) >= p_stdate AND
                 MAX(TO_DATE(TO_CHAR(ev.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) ) <= p_enddate
                 ORDER BY 2
                 ) t_ev
	  WHERE ROWNUM < v_max )
  	 WHERE rnum > v_min ;
    ELSE
       OPEN V_CUR FOR SELECT  pk_patprot ,event_exeon
             FROM ( SELECT t_ev.pk_patprot , TO_DATE(TO_CHAR(t_ev.event_exeon,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) event_exeon, ROWNUM rnum
             FROM (
		        SELECT v.pk_patprot , MAX(TO_DATE(TO_CHAR(ev.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT))  event_exeon
                  FROM ER_PATPROT v, SCH_EVENTS1 s, SCH_CODELST c,
			   sch_eventstat ev
                  WHERE v.fk_study = p_study AND
                  v.PK_PATPROT = s.fk_patprot AND
                  s.EVENT_ID = ev.FK_EVENT AND
                  c.pk_codelst = ev.EVENTSTAT AND
                  c.codelst_type = 'eventstatus'
                  AND trim(c.codelst_subtyp) = 'ev_done' AND
                  s.visit = 1
                  AND
                  v.pk_patprot IN
                        (SELECT b.pk_patprot FROM ( SELECT PK_PATPROT, TOT_EVENT_COUNT                                                                            FROM ERV_VISIT1_EVCOUNT WHERE
                                                  fk_study = p_study ) a ,
										 (SELECT PK_PATPROT, TOT_DONE_EVENT_COUNT
                                                    FROM ERV_VISIT1_DONE_EVCOUNT
                                                   WHERE fk_study = p_study) b
                          WHERE a.pk_patprot=b.pk_patprot
                          AND a.TOT_EVENT_COUNT = b.TOT_DONE_EVENT_COUNT
                          )
                 GROUP BY v.pk_patprot
                 HAVING MAX(TO_DATE(TO_CHAR(ev.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) ) >= p_stdate AND
                 MAX(TO_DATE(TO_CHAR(ev.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) ) <= p_enddate
                 ORDER BY 2
                 ) t_ev
	  WHERE ROWNUM < v_max )
  	 WHERE rnum > v_min ;
    END IF;

    LOOP
	  FETCH V_CUR INTO v_patprot  , v_ach_date;
       EXIT WHEN V_CUR%NOTFOUND;
       IF  v_ach_date IS NULL THEN
            o_result := 0	;
    	  ELSE
            o_result := 1	;
	  END IF;
	  o_date := v_ach_date ;

    END LOOP;
    CLOSE V_CUR;

     EXCEPTION WHEN NO_DATA_FOUND THEN  o_result := 0	;
    END;
   END SP_RULE_CHK_VIS_DONE_COMPLETE;

--------------------------------------------------------------------------------------------------------------------------

 PROCEDURE SP_FORECAST_VIS_EVENT_DONE_ONE (
   p_study        NUMBER,
   p_org_id       NUMBER,
   p_milcount        NUMBER,
   p_visit        NUMBER,
   p_stdate DATE,
   p_enddate DATE,
   o_result OUT  NUMBER,
   o_date OUT DATE
 )
 /*Author: Sonika Talwar
 * Date: June 03, 2004
 * Procedure to get the forecast for Patient's first visit (at least one event) is marked as 'Done', pm_4
 * and Procedure to get the forecast for Patient's first visit (all events) is marked as 'Done', pm_5
 * Returns the date of achievement of milestone
 */
   AS
    v_count NUMBER;
    v_min NUMBER;
    v_max NUMBER;
    v_patprot NUMBER;
    v_ach_date DATE;
    v_rnum NUMBER;

    BEGIN
     v_count := 0;
     o_result := 0;
	v_min := p_milcount - 1;
	v_max  := p_milcount +  1;
    BEGIN
   SELECT  pk_patprot, sch_on ,rnum
     INTO v_patprot, v_ach_date, v_rnum
             FROM ( SELECT pk_patprot, TO_DATE(TO_CHAR(SCH_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) SCH_ON , ROWNUM rnum
                              FROM (SELECT pk_patprot, MAX(TO_DATE(TO_CHAR(ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT)) SCH_ON
						  FROM SCH_EVENTS1 s, SCH_CODELST c ,  ERV_ALLPATSTUDYSTAT v
                              WHERE v.FK_STUDY= p_study AND s.visit = p_visit AND
					     TO_DATE(TO_CHAR( ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) BETWEEN p_stdate AND  p_enddate AND
	                           c.codelst_subtyp  <> 'ev_done' AND
                               v.PK_PATPROT = s.fk_patprot AND c.pk_codelst = s.isconfirmed AND
					      c.codelst_type = 'eventstatus'
					    GROUP BY pk_patprot ORDER BY 2 DESC ) a
			  WHERE ROWNUM < v_max )
        	 WHERE rnum > v_min ;

     IF v_ach_date IS NULL THEN
            o_result := 0	;
    	  ELSE
            o_result := 1	;
	  END IF;
	 o_date := v_ach_date ;
    P('sonika o_result ' || o_result);
             P('sonika v_ach_date ' || v_ach_date);

     EXCEPTION WHEN NO_DATA_FOUND THEN  o_result := 0	;
    END;
   END SP_FORECAST_VIS_EVENT_DONE_ONE ;
----------------------------------------------------------------------------------

   END Pkg_Pm_Rule;
/


CREATE SYNONYM ESCH.PKG_PM_RULE FOR PKG_PM_RULE;


CREATE SYNONYM EPAT.PKG_PM_RULE FOR PKG_PM_RULE;


GRANT EXECUTE, DEBUG ON PKG_PM_RULE TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_PM_RULE TO ESCH;

