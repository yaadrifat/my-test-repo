CREATE OR REPLACE PACKAGE        "PKG_IMPEXLOGGING" 
AS
/****************************************************************************************************
 ** ** Author: Sonia Sahni
 ** Purpose ; Contains procedures for database logging
 ********************************************************************************************************/

PROCEDURE SP_RECORDLOG (p_expreqid NUMBER, p_logdate DATE, p_severity VARCHAR2, p_messagecode VARCHAR2, p_logdesc VARCHAR2, p_module VARCHAR2, o_success OUT NUMBER );

PROCEDURE SP_RECORDLOG (p_expreqid NUMBER, p_severity VARCHAR2, p_messagecode VARCHAR2, p_logdesc VARCHAR2, p_module VARCHAR2, o_success OUT NUMBER );

PROCEDURE SP_RECORDLOG (p_expreqid NUMBER, p_severity VARCHAR2, p_messagecode VARCHAR2, p_logdesc VARCHAR2, o_success OUT NUMBER );

PROCEDURE SP_RECORDLOG (p_expreqid NUMBER,p_logdesc VARCHAR2, p_module VARCHAR2, o_success OUT NUMBER );

PROCEDURE SP_RECORDLOG (p_expreqid NUMBER,p_logdesc VARCHAR2, o_success OUT NUMBER );

PROCEDURE SP_RECORDLOG (p_logdesc VARCHAR2, o_success OUT NUMBER );

PROCEDURE SP_RECORDLOG (p_expreqid NUMBER, p_severity VARCHAR2, p_messagecode VARCHAR2, p_logdesc VARCHAR2, p_module VARCHAR2, p_sitecode VARCHAR2, o_success OUT NUMBER );

PROCEDURE SP_RECORDLOGSITE (p_expreqid NUMBER,p_logdesc VARCHAR2, p_module VARCHAR2, p_sitecode VARCHAR2, o_success OUT NUMBER );

PROCEDURE SP_RECORDLOGSITE (p_expreqid NUMBER,p_logdesc VARCHAR2, p_sitecode VARCHAR2, o_success OUT NUMBER );

PROCEDURE SP_RECORDLOGSITE (p_expreqid NUMBER, p_severity VARCHAR2, p_messagecode VARCHAR2, p_logdesc VARCHAR2, p_sitecode VARCHAR2, o_success OUT NUMBER );

PROCEDURE SP_UPDATE_LOGFILE (LOCATION VARCHAR2,filename VARCHAR2, DATA VARCHAR2);

/*
change pLEVEL  => PLOG.LDEBUG  with the required setting. Possible values:
-- The OFF has the highest possible rank and is intended to turn off logging.
LOFF
LFATAL
LERROR
LWARN
LINFO
LDEBUG
-- The ALL has the lowest possible rank and is intended to turn on all logging.
LALL */

pCTXA2A PLOG.LOG_CTX := PLOG.init (pSECTION => 'A2A', pLEVEL  => PLOG.LDEBUG );

END PKG_IMPEXLOGGING;
/


CREATE SYNONYM ESCH.PKG_IMPEXLOGGING FOR PKG_IMPEXLOGGING;


CREATE SYNONYM EPAT.PKG_IMPEXLOGGING FOR PKG_IMPEXLOGGING;


GRANT EXECUTE, DEBUG ON PKG_IMPEXLOGGING TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_IMPEXLOGGING TO ESCH;

