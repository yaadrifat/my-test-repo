CREATE OR REPLACE PACKAGE BODY      PKG_SUBMISSION_LOGIC AS
/******************************************************************************
   NAME:       PKG_SUBMISSION_LOGIC
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        5/28/2009   Sonia Abrol          1. Created this package.

   This package is designed to club together custom logic needed to validate submissions
   There can be one or more functions written to perform validation checks. All functions defined in this package will
   follow following conventions:

   1. Pls see template function - f_check_cro(p_study IN NUMBER) as an example.
   2. The Function will take one parameter : PK of the study
   3. The function will return a table - t_nested_results_table
   4. The table will contain following information:
          result: Pass/Fail information.Possible values:
                  0 : Pass
                 -1 : Fail
                 -2 : Not Required

          result_text : Text to be displayed for results
          TestText - Descriptive text (if any) for the checks performed
   5. The table can return one or more rows to send check results


******************************************************************************/

Function f_check_irb(p_study IN NUMBER) return t_nested_results_table
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);
    v_doc_count number;
    v_testText varchar2(1000);
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Check for Uploaded Document in Protocol Category';

  select count(*)
  into v_doc_count
  from er_studyver v,er_studyapndx x
  where v.fk_study = p_study and v.studyver_category = (select pk_codelst from er_codelst where codelst_type = 'studyvercat'
   and codelst_subtyp = 'protocol') and x.fk_studyver = pk_studyver and studyapndx_type = 'file';

   if v_doc_count > 0 then
        v_result := 0;
        v_result_text := 'Pass';
    else
        v_result := -1;
        v_result_text := 'Fail: No Document Uploaded in Protocol Category';
    end if;

  v_tab_submission_results.extend;
  v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );

--------------

      v_testText := 'Check for Uploaded Document in Informed Consent Category';

  select count(*)
  into v_doc_count
  from er_studyver v,er_studyapndx x
  where v.fk_study = p_study and v.studyver_category = (select pk_codelst from er_codelst where codelst_type = 'studyvercat'
   and codelst_subtyp = 'inf_consent') and x.fk_studyver = pk_studyver and studyapndx_type = 'file';

   if v_doc_count > 0 then
        v_result := 0;
        v_result_text := 'Pass';
    else
        v_result := -1;
        v_result_text := 'Fail: No Document Uploaded in Informed Consent Category';
    end if;

  v_tab_submission_results.extend;
  v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );

    return v_tab_submission_results;

end;

Function f_check_cro(p_study IN NUMBER) return t_nested_results_table
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);

    v_testText varchar2(1000);
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Check for Central Research Office';

    v_result := 0;
    v_result_text := 'Pass';

    v_tab_submission_results.extend;
    v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );


    return v_tab_submission_results;

end;



Function f_check_rsc(p_study IN NUMBER) return t_nested_results_table
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);

    v_testText varchar2(1000);
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Radiation Safety Committee';

    v_result := 0;
    v_result_text := 'Pass';

    v_tab_submission_results.extend;
    v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );


    return v_tab_submission_results;

end;

Function f_check_cic(p_study IN NUMBER) return t_nested_results_table
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);

    v_testText varchar2(1000);
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Conflict of Interest Committee';

    v_result := 0;
    v_result_text := 'Pass';

    v_tab_submission_results.extend;
    v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );


    return v_tab_submission_results;

end;


Function f_check_src(p_study IN NUMBER) return t_nested_results_table
AS
    v_tab_submission_results t_nested_results_table ;

    v_result number;
    v_result_text varchar2(1000);

    v_testText varchar2(1000);
begin

    v_tab_submission_results := t_nested_results_table();

    v_testText := 'Scientific Review Committee';

    v_result := 0;
    v_result_text := 'Pass';

    v_tab_submission_results.extend;
    v_tab_submission_results(v_tab_submission_results.count) := tab_submission_results(v_result, v_result_text,v_testText );


    return v_tab_submission_results;

end;

END PKG_SUBMISSION_LOGIC;
/


