CREATE OR REPLACE PACKAGE BODY PKG_INVENTORY
AS
  function f_get_storageSpecCount(p_pk_storage Number) return number
  is
  v_specCnt number;
  begin
          if (nvl(p_pk_storage,0) = 0) then
                  return '';
          end if;

          SELECT count(*) into v_specCnt FROM er_specimen WHERE fk_storage = p_pk_storage;

          for i in(SELECT PK_STORAGE FROM ER_STORAGE a WHERE
          a.fk_STORAGE =p_pk_storage ORDER BY STORAGE_NAME)
          loop
              v_specCnt := v_specCnt + pkg_INVENTORY.f_get_storageSpecCount(i.pk_STORAGE);
          end loop;
          return v_specCnt ;
  end;
------------------------------------------------------------------------------------------------
  function f_get_storagecapacity(p_pk_storage Number) return number
  is
  v_capacity number;
  begin
          if (nvl(p_pk_storage,0) = 0) then
                  return '';
          end if;

          SELECT count(*) into v_capacity FROM ER_STORAGE p WHERE p.fk_STORAGE = p_pk_storage;

          for i in(SELECT PK_STORAGE FROM ER_STORAGE a WHERE
          a.fk_STORAGE =p_pk_storage ORDER BY STORAGE_NAME)
          loop
              v_capacity := v_capacity + pkg_INVENTORY.f_get_storagecapacity(i.pk_STORAGE);
          end loop;
          return v_capacity ;
  end;
------------------------------------------------------------------------------------------------
  function f_get_storageinfo(p_pk_storage Number,p_RowCnt number,p_Level number) return clob
    is
    v_storeInfo clob;
        v_Count number;
        v_Level number;
        v_capacity number;
        v_specCnt number;
    begin
        if (nvl(p_pk_storage,0) = 0) then
            return '';
        end if;
                v_Count := p_RowCnt;
                v_Level := p_Level;

                select
                '<PK>'||p_pk_storage||'</PK>'||
                '<ParentPK>'||c.fk_STORAGE||'</ParentPK>'||
                '<PSTORE_ID>'||(SELECT p.STORAGE_ID FROM ER_STORAGE p WHERE p.pk_STORAGE = c.fk_STORAGE)||'</PSTORE_ID>'||
                '<ChildCnt>'||(SELECT count(*) FROM ER_STORAGE p WHERE p.fk_STORAGE = p_pk_storage)||'</ChildCnt>'||
                '<STORE_ID>'||STORAGE_ID||'</STORE_ID>'||
                '<STORE_NAME>'||STORAGE_NAME||'</STORE_NAME>'||
                '<STORE_ALTERNATE_ID>'||STORAGE_ALTERNALEID||'</STORE_ALTERNATE_ID>'||
                '<STORE_TYPE>'||(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = FK_CODELST_STORAGE_TYPE)||'</STORE_TYPE>'||
                '<STORE_CAP_SPEC>'||STORAGE_CAP_NUMBER||'</STORE_CAP_SPEC>'||
                '<STORE_CAP_DIMEN>'||(case when STORAGE_DIM1_CELLNUM is not null and STORAGE_DIM2_CELLNUM is not null then STORAGE_DIM1_CELLNUM||'*'||STORAGE_DIM2_CELLNUM
                else '' end)||'</STORE_CAP_DIMEN>'||
                '<STORE_STATUS>'||(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = s1.FK_CODELST_STORAGE_STATUS)||'</STORE_STATUS>'||
                '<STORE_STAT_STUDY>'||(case when s1.FK_STUDY is null then '[No Study Specified]'
                else (SELECT study_number FROM er_study WHERE pk_study = s1.FK_STUDY) end)||'</STORE_STAT_STUDY>'||
                '<STORE_SPECCNT>'||(SELECT count(*) FROM er_specimen WHERE fk_storage = c.pk_storage)||'</STORE_SPECCNT>'
                as STOREInfo INTO v_StoreInfo
                from ER_STORAGE c, er_storage_status s1 where c.pk_STORAGE = p_pk_storage
                AND s1.fk_storage = c.pk_storage
                AND s1.PK_STORAGE_STATUS = (SELECT MAX(s2.PK_STORAGE_STATUS) FROM er_storage_status s2
                WHERE s2.fk_storage = c.pk_storage
                AND s2.SS_START_DATE = (SELECT MAX(s3.SS_START_DATE) FROM er_storage_status s3 WHERE s3.fk_storage = c.pk_storage));

                v_capacity := pkg_INVENTORY.f_get_storagecapacity(p_pk_storage)+1;
                v_specCnt := pkg_INVENTORY.f_get_storageSpecCount(p_pk_storage);

                v_StoreInfo :=v_StoreInfo || '<STORE_CAP_ACT>'||v_capacity||'</STORE_CAP_ACT>';
                v_StoreInfo :=v_StoreInfo || '<STORE_SPEC_ACT>'||v_specCnt||'</STORE_SPEC_ACT>';
                v_StoreInfo :=v_StoreInfo || '<STORE_Occupancy>'||TO_CHAR((v_specCnt/v_capacity)*100,999.99)||'%</STORE_Occupancy>';

                v_StoreInfo := '<ROW'||to_char(v_Level)||' num="'||TO_CHAR(v_Count)||'">'|| v_StoreInfo;
                v_Level := v_Level +1;

                for i in(SELECT PK_STORAGE FROM ER_STORAGE a WHERE
                fk_STORAGE =p_pk_storage
                ORDER BY STORAGE_NAME)
                loop
                    v_Count := v_Count+1;
                    v_StoreInfo := v_StoreInfo || pkg_INVENTORY.f_get_storageinfo(i.pk_STORAGE, v_count, v_Level);
                end loop;
                v_STOREInfo := v_STOREInfo ||'</ROW'||to_char(v_Level-1)||'>'|| CHR(13);
        return v_STOREInfo ;
    end;
------------------------------------------------------------------------------------------------
        function f_get_storage_trail(p_accId number, p_storageId varchar) return clob
    is
        tab_storage split_tbl;

        v_repXML clob;
    v_pk_parentSp number;

        v_count number default 0;
        v_Level number;
    begin
        if (nvl(p_accId,0) = 0) then
            return '';
        end if;

                SELECT Pkg_Util.f_split(p_storageId) INTO tab_storage FROM dual;

                v_repXML := '<?xml version="1.0" encoding="UTF-8"?>'|| CHR(13);
                v_repXML := v_repXML || '<ROWSET>'|| CHR(13);

                FOR S IN(SELECT PK_STORAGE FROM ER_STORAGE a WHERE a.fk_account = p_accId
                AND PK_STORAGE in (SELECT COLUMN_VALUE AS stor FROM TABLE(tab_storage))
                --AND fk_STORAGE is null
                ORDER BY STORAGE_NAME)
                loop
                    v_Level := 1;
                    v_repXML := v_repXML || pkg_INVENTORY.f_get_STORAGEinfo(S.pk_STORAGE, v_count, v_Level);
                    v_count := v_count+1;
                end loop;
                v_repXML := v_repXML || '</ROWSET>'|| CHR(13);
        return v_repXML;
    end;
------------------------------------------------------------------------------------------------
        function f_get_specimeninfo(p_pk_specimen Number,p_RowCnt number,p_Level number) return clob
    is
    v_specInfo clob;
        v_Count number;
        v_Level number;
    begin
        if (nvl(p_pk_specimen,0) = 0) then
            return '';
        end if;
                v_Count := p_RowCnt;
                v_Level := p_Level;

                select
                '<PK>'||p_pk_specimen||'</PK>'||
                '<ParentPK>'||c.fk_specimen||'</ParentPK>'||
                '<PSPEC_ID>'||(SELECT p.SPEC_ID FROM ER_SPECIMEN p WHERE p.pk_specimen = c.fk_specimen)||'</PSPEC_ID>'||
                '<ChildCnt>'||(SELECT count(*) FROM ER_SPECIMEN p WHERE p.fk_specimen = p_pk_specimen)||'</ChildCnt>'||
                '<SPEC_ID>'||SPEC_ID||'</SPEC_ID>'||
                '<SPEC_ALTERNATE_ID>'||SPEC_ALTERNATE_ID||'</SPEC_ALTERNATE_ID>'||
                '<STUDY_NUMBER>'||(case when fk_study is null then '[No Study Specified]'
                else (SELECT study_number FROM er_study WHERE pk_study = fk_study) end)||'</STUDY_NUMBER>'||
                '<SPEC_TYPE>'||(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type)||'</SPEC_TYPE>'||
                '<SPEC_COLLECTION_DATE>'||SPEC_COLLECTION_DATE||'</SPEC_COLLECTION_DATE>'||
                '<SPEC_ANATOMIC_SITE>'||(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_anatomic_site)||'</SPEC_ANATOMIC_SITE>'||
                '<SPEC_ORIGINAL_QUANTITY>'||SPEC_ORIGINAL_QUANTITY||'</SPEC_ORIGINAL_QUANTITY>'||
                '<SPEC_QUANTITY_UNITS>'||(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units)||'</SPEC_QUANTITY_UNITS>' as specInfo
                INTO v_specInfo
                from ER_SPECIMEN c where c.pk_specimen = p_pk_specimen;

                v_specInfo := '<ROW'||to_char(v_Level)||' num="'||TO_CHAR(v_Count)||'">'|| v_specInfo;
                v_Level := v_Level +1;

                for i in(SELECT PK_SPECIMEN FROM ER_SPECIMEN a WHERE
                fk_specimen =p_pk_specimen
                ORDER BY SPEC_COLLECTION_DATE)
                loop
                    v_Count := v_Count+1;
                    v_specInfo := v_specInfo || pkg_INVENTORY.f_get_specimeninfo(i.pk_specimen, v_count, v_Level);
                end loop;
                v_specInfo := v_specInfo ||'</ROW'||to_char(v_Level-1)||'>'|| CHR(13);
        return v_specInfo ;
    end;
------------------------------------------------------------------------------------------------
        function f_get_specimen_trail(p_userId number, p_studyId varchar, p_fromDate date, p_toDate date) return clob
    is
        tab_study split_tbl;

        v_repXML clob;
    v_pk_parentSp number;

        v_count number default 0;
        v_Level number;
    begin
        if (nvl(p_userId,0) = 0) then
            return '';
        end if;

          v_repXML := '<?xml version="1.0" encoding="UTF-8"?>'|| CHR(13);
          v_repXML := v_repXML || '<ROWSET>'|| CHR(13);

          if (p_studyId = 'NonStudy') then
                FOR S IN(SELECT PK_SPECIMEN FROM ER_SPECIMEN a
                WHERE (a.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account = a.fk_account
                and usr.fk_user = p_userId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
                AND ((a.fk_study is null AND a.fk_account = (SELECT u.fk_account from er_user u where pk_user = p_userId)))
                AND fk_specimen is null
                AND (spec_collection_date is null or spec_collection_date BETWEEN p_fromDate AND p_toDate)
                ORDER BY SPEC_COLLECTION_DATE)
                loop
                    v_Level := 1;
                    v_repXML := v_repXML || pkg_INVENTORY.f_get_specimeninfo(S.pk_specimen, v_count, v_Level);
                    v_count := v_count+1;
                end loop;
          else
                SELECT Pkg_Util.f_split(p_studyId) INTO tab_study FROM dual;

                FOR S IN(SELECT PK_SPECIMEN FROM ER_SPECIMEN a
                WHERE (a.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account = a.fk_account
                and usr.fk_user = p_userId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
                AND ((a.fk_study in (SELECT COLUMN_VALUE AS study FROM TABLE(tab_study))
                AND a.fk_study in (SELECT pk_study FROM ER_STUDY WHERE EXISTS (SELECT * FROM ER_STUDYTEAM t WHERE t.fk_user = p_userId AND
                t.fk_study = pk_study AND t.study_team_usr_type <> 'X' ) OR Pkg_Superuser.F_Is_Superuser(p_userId, pk_study) = 1)))
                AND fk_specimen is null
                AND (spec_collection_date is null or spec_collection_date BETWEEN p_fromDate AND p_toDate)
                ORDER BY SPEC_COLLECTION_DATE)
                loop
                    v_Level := 1;
                    v_repXML := v_repXML || pkg_INVENTORY.f_get_specimeninfo(S.pk_specimen, v_count, v_Level);
                    v_count := v_count+1;
                end loop;
          end if;
        v_repXML := v_repXML || '</ROWSET>'|| CHR(13);
        return v_repXML;
    end;
END PKG_INVENTORY;
/


