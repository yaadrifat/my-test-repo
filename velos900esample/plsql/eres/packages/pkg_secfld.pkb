create or replace PACKAGE BODY        "PKG_SECFLD" 
AS


/****************************************************************************************************
   ** Procedure to update all the sequences (when a duplicate seq is entered) by incrementing the
   ** formsec_seq of the duplicate and all other section sequences whose seq are greater than the duplicate seq
   ** Author: Anu Khanna 20/May/04.
   ** Input parameter: formLibId
   **/
PROCEDURE SP_UPDATE_SECTION_SEQ(p_formlib_id Number,ret out number)
 as

 str varchar2(4000);
 queryStr varchar2(4000);

 Begin

	/* str contains all ids whise duplicate sequence number exists*/
	for i in(select a.PK_FORMSEC
                            from er_formsec a
                            where a.fk_formlib = p_formlib_id
                            and a.PK_FORMSEC  > any (select b.PK_FORMSEC
                            from er_formsec b
                            where a.fk_formlib = b.fk_formlib
                            and a.formsec_seq = b.formsec_seq))
  loop
            if str is null then
                    str := to_char(i.pk_formsec);
            else
                    str := str||','||to_char(i.pk_formsec);
    end if;
  end loop ;

 /* Query to update all duplicate seq and seq greater than the duplicate seq*/
 queryStr := 'update er_formsec set formsec_seq = formsec_seq + 1
         where pk_formsec not in ('|| str || ')
         and fk_formlib = '|| p_formlib_id ||'
         and formsec_seq >= :1';

  for i in (select a.PK_FORMSEC  , a.FORMSEC_SEQ
            from er_formsec a
            where a.fk_formlib = p_formlib_id
            and a.PK_FORMSEC  > any (select b.PK_FORMSEC
                    from er_formsec b
                    where a.fk_formlib = b.fk_formlib
            and a.formsec_seq = b.formsec_seq)
            order by a.FORMSEC_SEQ)
 Loop

     execute immediate queryStr using i.formsec_seq;
  end Loop;
 commit;
 ret := 0;

	EXCEPTION
	WHEN OTHERS THEN
		ret := -1;
 end;


 /*****************************/

PROCEDURE SP_UPDATE_FIELD_SEQ(p_formsec_id Number,p_formfld_id Number,p_fld_seq Number, p_usr Number, ret out number)
  as

  /****************************************************************************************************
   ** Procedure to update all the sequences (when a duplicate seq is entered) by incrementing the
   ** formfld_seq of the duplicate and all other field sequences in a section of a form whose seq are greater than the duplicate seq
   ** Author: Anu Khanna 20/May/04.
   ** Input parameter: formSexId
   ** Input parameter: formFldId
   ** Input parameter: fldSeq
   ** Modified by Anu on 14/June/04: Added two new parameter p_formfld_id, p_fld_seq.Changed the sql, as earlier it used to
   ** give formFldId which had greater value out of the formFldIds with same field sequence.
   ** Modified by Sonika on 17/June/04: Set the last_modified_date of er_formfld so that the trigger is executed to update the repeated fields
   **/

  str varchar2(4000);
  queryStr varchar2(4000);
  v_formfld Number;
  Begin

--v_formfld contains formFldId whose duplicate sequence number exists

  

  /* Query to update all seq greater than and equal to the duplicate seq
     change the last modified date so that er_formfld_au3 is triggered and the sequences of repeated fields are also modified
  */
     --KM-#3634
 	    update er_formfld
	     set formfld_seq = formfld_seq + 1	,
		    last_modified_date = sysdate, last_modified_by = p_usr
          where pk_formfld <>  p_formfld_id
          and fk_formsec =  p_formsec_id
          and formfld_seq >= p_fld_seq;


  commit;
  ret := 0;
  EXCEPTION
  WHEN OTHERS THEN
   ret := -1;
  end;
    END PKG_SECFLD; 

/


CREATE SYNONYM ESCH.PKG_SECFLD FOR PKG_SECFLD;


CREATE SYNONYM EPAT.PKG_SECFLD FOR PKG_SECFLD;


GRANT EXECUTE, DEBUG ON PKG_SECFLD TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_SECFLD TO ESCH;

