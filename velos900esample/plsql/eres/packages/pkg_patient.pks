CREATE OR REPLACE PACKAGE        "PKG_PATIENT" AS
/******************************************************************************
   NAME:       PKG_PATIENT
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        9/30/2005 Sonia Abrol             1. Created this package.
******************************************************************************/

/* returns a comma separated list of sites to which the patient is registered. Each site name is also followed by the respective facility id of the patient in that site*/
  FUNCTION f_get_patsites(p_per IN NUMBER,p_site IN NUMBER) RETURN VARCHAR2;


  /* returns a comma separated list of all patient codes along with the sites to which the patient is registered. */
  FUNCTION f_get_patcodes(p_per IN NUMBER) RETURN VARCHAR2 ;

    /* returns 1 if patient's enrolling site is the p_site, if enrolling site is null, checks for patient's default site. */
  FUNCTION f_is_enrollingsite(p_per IN NUMBER, p_site IN NUMBER, p_study NUMBER) RETURN NUMBER ;

     /* returns patient's enrolling site , if enrolling site is null, returns patient's default site. */
  FUNCTION f_get_enrollingsite(p_per IN NUMBER,  p_study NUMBER) RETURN NUMBER ;

    /* Sonia Abrol, 03/07/06
  generates a  apatient code using ac_key (er_account and patient PK*/
   Function f_generate_CCID ( p_per IN NUMBER) RETURN VARCHAR2;


   pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_PATIENT', pLEVEL  => Plog.LFATAL);

  PROCEDURE SP_PATENRMAIL (
   p_per Number,
   p_patstudystatsubtype   VARCHAR2,
   p_study Number,
   p_statdate Varchar2
   );

      /* return patient enrollment date in a study*/
   function f_get_enrollment_date(p_per IN NUMBER, p_study NUMBER) RETURN DATE;

     /* return patient study linking date*/
   function f_get_study_linking_date(p_per IN NUMBER, p_study NUMBER) RETURN DATE;

   /* returns 1 if p_site is patient facility site, return 0 if not*/
  FUNCTION f_is_patfacilitysite(p_per IN NUMBER, p_site IN NUMBER) RETURN NUMBER ;

   /* returns 1 if p_code is a patient facility ID , return 0 if not*/
  FUNCTION f_is_patfacilityid(p_per IN NUMBER, p_code IN Varchar2) RETURN NUMBER ;

	  PROCEDURE sp_notify_portal_patient (
   p_per Number,
   p_loginname VARCHAR2,
   p_loginpass Varchar2,
   p_usr Number,
   o_out OUT Varchar2
   );

 /* returns patprot for latest patient schedule for a calendar in a study*/
  function fget_latest_sch_patprot(p_per IN NUMBER, p_protocol NUMBER) RETURN Number;

END Pkg_Patient;
/


CREATE SYNONYM ESCH.PKG_PATIENT FOR PKG_PATIENT;


CREATE SYNONYM EPAT.PKG_PATIENT FOR PKG_PATIENT;


GRANT EXECUTE, DEBUG ON PKG_PATIENT TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_PATIENT TO ESCH;

