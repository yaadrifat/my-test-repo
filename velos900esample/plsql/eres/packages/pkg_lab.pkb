CREATE OR REPLACE PACKAGE BODY        "PKG_LAB"
IS
PROCEDURE SP_INSERT_PATLABS(p_pat_id VARCHAR2,p_site_id VARCHAR2,p_patprot_id VARCHAR2, p_lab_date ARRAY_STRING, p_lab_id ARRAY_STRING,
              p_accn_num ARRAY_STRING,p_lab_result ARRAY_STRING,p_test_type ARRAY_STRING, p_lab_unit ARRAY_STRING,p_lab_lln ARRAY_STRING,
			  p_lab_uln ARRAY_STRING,p_lab_status ARRAY_STRING,p_lab_abnr ARRAY_STRING,p_lab_cat ARRAY_STRING,p_lab_advname ARRAY_STRING,
			  p_lab_grade ARRAY_STRING,p_lab_notes ARRAY_CLOB,p_lab_lresults ARRAY_CLOB, p_user VARCHAR2, p_ipadd VARCHAR2,p_stdPhase ARRAY_STRING,p_studyId VARCHAR2, p_specId varchar2, o_ret OUT NUMBER)
IS


   v_cnt NUMBER;
   i NUMBER;
   v_patient NUMBER;
   v_site NUMBER;
   v_patprot_id NUMBER;
   v_lab_date DATE;
   v_lab_pk NUMBER;
   v_accn_num VARCHAR2(20);
   v_lab_result VARCHAR2(100);
   v_test_type CHAR(2);
   v_lab_unit VARCHAR2(20);
   v_lab_lln VARCHAR2(20);
   v_lab_uln VARCHAR2(20);
   v_lab_status NUMBER;
   v_lab_abnr NUMBER;
   v_lab_cat NUMBER;
   v_advname VARCHAR2(500);
   v_grade NUMBER;
   v_note CLOB;
   v_lresult CLOB;
   v_stdPhase NUMBER;
   v_studyId NUMBER;
   v_specId NUMBER;
BEGIN



	 v_patient:=TO_NUMBER(p_pat_id);
--	 v_stdPhase:=to_number(p_stdPhase);
	 v_studyId:=TO_NUMBER(p_studyId);
         v_specId:=TO_NUMBER(p_specId); --//JM: 07Jul2009: #INVP2.11
   v_cnt := p_lab_id.COUNT(); --get the # of elements in array
   i:=1;

   WHILE i <= v_cnt LOOP
    IF (LENGTH(p_lab_id(i)) > 0) THEN
      v_lab_pk := TO_NUMBER(p_lab_id(i));

	  v_lab_date:=TO_DATE(p_lab_date(i),PKG_DATEUTIL.F_GET_DATEFORMAT);

	  v_accn_num:=p_accn_num(i);

	  v_lab_result:=p_lab_result(i);

	  v_test_type:= p_test_type(i);

	  v_lab_unit:=p_lab_unit(i);

	  v_lab_lln:= p_lab_lln(i);

	  v_lab_uln:= p_lab_uln(i);

	  v_lab_status:=TO_NUMBER(p_lab_status(i));

	  v_lab_abnr:=TO_NUMBER(p_lab_abnr(i));

	  v_lab_cat:=TO_NUMBER(p_lab_cat(i));

	  v_advname:=p_lab_advname(i);

	  v_grade:=TO_NUMBER(p_lab_grade(i));

	  v_note:=p_lab_notes(i);
  	  v_lresult:=p_lab_lresults(i);
	  v_stdPhase:=TO_NUMBER(p_stdPhase(i));


      BEGIN
	  INSERT INTO ER_PATLABS (PK_PATLABS ,FK_PER ,
    FK_TESTID ,TEST_DATE ,TEST_RESULT ,TEST_TYPE ,
    TEST_UNIT ,TEST_LLN ,TEST_ULN ,FK_SITE ,
    FK_CODELST_TSTSTAT ,ACCESSION_NUM ,FK_TESTGROUP ,
    FK_CODELST_ABFLAG ,FK_PATPROT ,CREATOR ,CREATED_ON ,IP_ADD ,ER_NAME ,
    ER_GRADE,TEST_RESULT_TX,NOTES,fk_codelst_stdphase,fk_study, fk_specimen )
    VALUES (seq_er_patlabs.NEXTVAL,v_patient,v_lab_pk,v_lab_date,
		   v_lab_result,v_test_type,v_lab_unit,v_lab_lln,v_lab_uln,TO_NUMBER(p_site_id),v_lab_status,
		   v_accn_num,v_lab_cat,v_lab_abnr,TO_NUMBER(p_patprot_id),p_user,SYSDATE,p_ipadd,v_advname,v_grade,v_lresult,v_note,v_stdPhase,v_studyId, v_specId);

      EXCEPTION  WHEN OTHERS THEN
        P('ERROR');
RAISE_APPLICATION_ERROR(-20000, 'Unknown Exception Raised: '||SQLCODE||' '||SQLERRM);
		COMMIT;

        o_ret:=-1;
	   RETURN;
      END ;--end of insert begin

	 i := i+1;
    ELSE
	    i := i+1; --do not insert null values
    END IF;
   END LOOP; --v_cnt loop

   COMMIT;
   o_ret:=0;



END;

PROCEDURE SP_DELETE_PATLABS(p_ids ARRAY_STRING,p_tablename VARCHAR2,p_col VARCHAR2,p_datatype VARCHAR2,o_ret OUT NUMBER)
IS
   v_cnt NUMBER;
   i NUMBER;
   v_id NUMBER;
   v_id_str VARCHAR2(100);
   v_date DATE;
   v_whereStr VARCHAR2(500);
   v_inStr VARCHAR2(500);
   v_temp VARCHAR2(100);
   v_sql VARCHAR2(1000);


BEGIN

   v_cnt := p_ids.COUNT; --get the # of elements in array


   i:=1;
	WHILE i <= v_cnt LOOP

	IF LOWER(p_datatype)= 'string' THEN
		v_temp:= p_ids(i);
	END IF;
	IF LOWER(p_datatype)= 'number' THEN
		 v_temp:='to_number('''||p_ids(i)||''')';


	 END IF;


	 IF LOWER(p_datatype)= 'date' THEN
		 --JM: Modified 19Mar2009
		 v_temp:='to_date('||p_ids(i)||','''||PKG_DATEUTIL.F_GET_DATEFORMAT||''')';

	END IF;

	IF (LENGTH(v_inStr) > 0) THEN
	v_inStr:=v_inStr||', '||v_temp ;
	ELSE
	v_inStr:= '('||v_temp ;
	END IF;

	i := i+1;
 END LOOP;

	v_inStr:=v_inStr||')' ;


	BEGIN
	  v_sql:='delete from '||p_tablename|| ' where ' || p_col||' in '|| v_inStr ;


	  EXECUTE IMMEDIATE v_sql ;

	  COMMIT;
      EXCEPTION  WHEN OTHERS THEN
        P('ERROR');
        o_ret:=-1;
	   RETURN;
      END ;--end of insert begin
   o_ret:=0;

 END;




-----------------------------------------------------------------------------------------------------


END Pkg_Lab;
/


CREATE SYNONYM ESCH.PKG_LAB FOR PKG_LAB;


CREATE SYNONYM EPAT.PKG_LAB FOR PKG_LAB;


GRANT EXECUTE, DEBUG ON PKG_LAB TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_LAB TO ESCH;

