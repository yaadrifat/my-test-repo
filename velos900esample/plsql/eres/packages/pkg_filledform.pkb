CREATE OR REPLACE PACKAGE BODY ERES."PKG_FILLEDFORM"
AS
PROCEDURE SP_GENERATE_PRINTXSL(p_formid NUMBER)
 /****************************************************************************************************
   ** Procedure to create xsl for printer friendly format, called when the form status is changed to active
   ** When the user makes the form Active, we traverse through all the field xsls, same way as we do in Form Preview.
   ** We change each xsl to make it a label and then club that xsl.
   ** Input parameter: formid - PK of the form
   ** Output parameter: formxsl
   ** Author: Sonika Talwar Dec 24, 2003
   ** Modified By: Sonika Talwar on April 19, 04
   **/
  AS
   v_formnametag VARCHAR2(500);
   v_fldxsl VARCHAR2(32000);
   v_clubxsl CLOB;
   v_formxsl CLOB;
   v_xslstarttags VARCHAR2(4000);
   v_xslmidtags VARCHAR2(4000);
   v_xslendtags VARCHAR2(4000);
   v_fldsameline NUMBER;
   v_fldseq NUMBER;
   v_section_seq NUMBER;
   v_secname VARCHAR2(250);
   v_formname VARCHAR2(250);
   v_pksec NUMBER;
   v_prevsec NUMBER := 0;
   v_fldname VARCHAR2(2000);
   v_fldkeyword VARCHAR2(255);
   v_flduniqueid VARCHAR2(250);
   v_fldsystemid VARCHAR2(50);
   v_pk_field NUMBER;
   v_fldtype CHAR(1);
   v_fld_datatype VARCHAR2(2);
   v_formstarttag VARCHAR2(500);
   v_formendtag VARCHAR2(100);
   v_xslns VARCHAR2(1000);
   v_colcount NUMBER;
   --salil
   v_fldlinesno NUMBER ;
   v_count NUMBER := 1;
   v_sec_count NUMBER := 1;
   v_secrepno NUMBER := 0;
   v_secformat CHAR(1) ;
   v_prev_sec_format CHAR(1) ;
   v_repfldxsl VARCHAR2(32000);
   v_pkformfld NUMBER;
   v_repfldsameline NUMBER;
   v_repfldname VARCHAR2(2000);
   v_repfldkeyword VARCHAR2(255);
   v_repflduniqueid VARCHAR2(50);
   v_repfldsystemid VARCHAR2(50);
   v_rep_pk_field NUMBER;
   v_repfldtype CHAR(1);
   v_rep_fld_datatype VARCHAR2(2);
   v_rep_fld_defresp VARCHAR2(500);
   v_repcolcount NUMBER;
   --salil
   v_repfldlinesno NUMBER ;
   v_prev_sec_repno NUMBER :=0 ;
   v_prevset NUMBER := 0;
   v_repfld_set NUMBER := 0;
   v_repfld_count NUMBER := 1;
   v_strlen NUMBER;
   v_pos_1 NUMBER := 0;
   v_pos_2 NUMBER := 0;
   v_pos_endtd NUMBER :=0;
   len NUMBER := 0;
   v_repfld_xsl_2 VARCHAR2(4000);
   v_repfld_xsl_3 VARCHAR2(4000);
   v_grid_title_xsl VARCHAR2(100);
   v_sec_grid_header VARCHAR2(4000);
   v_counter NUMBER;
   v_tempstr VARCHAR2(4000);
   v_tempclubxsl CLOB;
   v_origsystemid VARCHAR2(50);
   v_reporigsystemid VARCHAR2(50);
   v_valuestr VARCHAR2(2000);
   v_replacestr VARCHAR2(2000);
   v_interfld_before VARCHAR2(4000);
   v_activation_js VARCHAR2(4000) := ' ';
   v_interfld_after VARCHAR2(4000);
   v_custom_js VARCHAR2(4000);
   v_fld_align VARCHAR2(20);
   v_form_preserve_formatting number;

BEGIN

        BEGIN
             SELECT NVL(FORM_SAVEFORMAT,0), FORM_ACTIVATION_JS INTO v_form_preserve_formatting, v_activation_js
             FROM ER_FORMLIB  WHERE PK_FORMLIB = p_formid ;
        EXCEPTION WHEN NO_DATA_FOUND THEN
                v_form_preserve_formatting := 0;

        END;

        v_activation_js := replace(v_activation_js, '<script>', '');
        v_activation_js := replace(v_activation_js, '</script>', '');
        v_activation_js := replace(v_activation_js, 'changeFieldStateMultiple', 'changeFieldStateMultiplePrint');
        v_activation_js := regexp_replace(v_activation_js,
          '(fld = formobj\.fld)([0-9_]+)(\s)?;',
          '<xsl:for-each select = "fld'||'\2'||'/resp">'||
          '<xsl:if test="@selected=''1''">fld = &quot;<xsl:value-of select="@dataval"/>&quot;;</xsl:if>'||
          '<xsl:if test="@checked=''1''">fld += &quot;<xsl:value-of select="@dataval"/>|&quot;;</xsl:if>'||
          '</xsl:for-each>'||
          ' if (!fld) { fld = &quot;<xsl:value-of select="fld'||'\2'||'"/>&quot; ; }'
        );
        v_activation_js := regexp_replace(v_activation_js, '(formobj\.fld)([0-9_]+)',
          '{ name:&quot;fld'||'\2'||'&quot; } ;'
        );


    --KM -#6060
    if (v_form_preserve_formatting = 0 or v_form_preserve_formatting = 1) then
        --add xsl namespace tags
        v_xslns := '<?xml version="1.0" ?> <xsl:stylesheet version="1.0" ';
        v_xslns := v_xslns || 'xmlns:xsl="http://www.w3.org/1999/XSL/Transform" ';
        v_xslns := v_xslns || 'xmlns:xdb="http://xmlns.oracle.com/xdb" ';
        v_xslns := v_xslns || 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> ' || CHR(13);
        v_xslstarttags := ' <xsl:template match="/"> ';
        v_xslstarttags :=  v_xslstarttags  || '<HTML> ' || CHR(13) || '<HEAD> ' || CHR(13);
        v_xslstarttags :=  v_xslstarttags  || '<meta http-equiv="Pragma" content="no-cache"></meta>' || CHR(13);
        v_xslstarttags :=  v_xslstarttags  || '<meta http-equiv="Expires" content="-1"></meta>' || CHR(13);
        v_xslstarttags :=  v_xslstarttags  || '<meta http-equiv="Cache-Control" content="no-cache"></meta>' || CHR(13);
        v_xslmidtags := ' </HEAD> ' || CHR(13) || '<BODY id="forms"> ' || CHR(13) || '<xsl:apply-templates select="rowset"/> ';
        v_xslmidtags := v_xslmidtags || CHR(13) || '</BODY> ' || CHR(13) ||' </HTML> ' || CHR(13) || '</xsl:template> ' || CHR(13) || '<xsl:template match="rowset"> ';
        v_xslendtags := '</xsl:template> ' || CHR(13) ||' </xsl:stylesheet> ';
        v_interfld_before := '<script src="js/jquery/jquery-1.4.4.js"></script><script src="formjs.js"></script>' || CHR(13);
        v_interfld_before := v_interfld_before || '<script> var $j = jQuery; function callActionScript() { ' || CHR(13);
        v_interfld_after := '}' || CHR(13);
        v_interfld_after := v_interfld_after || '$j(document).ready(function() { callActionScript(); });' || CHR(13);
        v_interfld_after := v_interfld_after || '</script>';

        /************Sonia - 16th June for field Actions ********************************/
         -- create function for custom js
          v_custom_js := ' <script> ' || CHR(13) ||' function performAction(formobj, currentfld)' ||
                          CHR(13) || '{' || CHR(13) ||'}' || CHR(13) ||'</script>'  ;
         /************Sonia - 16th June for field Actions *********************************/
        v_formstarttag := '<Form name="er_fillform1" method="post" >';
        v_formendtag := '</Form>';
        v_clubxsl :=  v_clubxsl  || '<table class="dynFormTable" width="100%" border="0"><tr>';
        FOR i IN (SELECT pk_formfld,form_name , pk_formsec, formsec_name,formsec_seq ,formfld_seq ,NVL(fld_sameline,0) fld_sameline,formfld_xsl ,
                    NVL(formfld_javascr,' ') formfld_javascr,fld_name,fld_keyword,fld_uniqueid,
                  fld_systemid, pk_field,fld_type,fld_datatype, fld_defresp, fld_colcount , fld_linesno , formsec_fmt,NVL(formsec_repno,0) formsec_repno,
                  NVL(fld_align,'left') fld_Align
                   FROM erv_formflds
                  WHERE  pk_formlib = p_formid
                  AND NVL(fld_isvisible,0) <> 1
                  AND NVL(fld_datatype,'Z') <> 'ML' AND
                  NVL(fld_type,'Z') <> 'F'
                  UNION
                  SELECT NULL, NULL , pk_formsec, formsec_name,formsec_seq ,NULL ,NULL,NULL ,
                    NULL,NULL,NULL,NULL,
                  NULL, NULL,NULL,NULL, NULL, NULL,NULL,NULL,NULL,NULL
                  FROM ER_FORMSEC
                  WHERE fk_formlib = p_formid
                  AND NVL(record_type,'Z') <> 'D'
                  AND pk_formsec NOT IN (SELECT pk_formsec
                  FROM erv_formflds WHERE pk_formlib = p_formid)
                  ORDER BY formsec_seq,pk_formsec,formfld_seq
                  )
        LOOP
            IF (i.form_name IS NOT NULL) THEN
              v_formname := i.form_name;
           END IF;
           v_pksec := i.pk_formsec;
           v_secname := i.formsec_name;
           v_section_seq := i.formsec_seq;
           v_fldseq := i.formfld_seq;
           v_fldsameline := i.fld_sameline;
           v_fldxsl := i.formfld_xsl;
            v_fldname := i.fld_name;
           v_fldkeyword := i.fld_keyword;
           v_flduniqueid := i.fld_uniqueid;
           v_fldsystemid := i.fld_systemid;
           v_pk_field := i.pk_field;
           v_fldtype :=  i.fld_type;
           v_fld_datatype := i.fld_datatype;
           v_colcount := i.fld_colcount;
           v_fldlinesno := i.fld_linesno;
           v_secformat := i.formsec_fmt;
           v_secrepno :=  i.formsec_repno;
           v_pkformfld := i.pk_formfld;
           v_origsystemid := i.fld_systemid;
           v_fld_align := i.fld_align;
           --get the changed field xsl
           IF v_fldtype = 'S' THEN
              v_fldxsl := v_fldxsl; --no change in xsl
           ELSE
                  SP_GETFLDPRINTXSL(v_fld_datatype, v_fldsystemid, v_fldlinesno, v_fldxsl);
           END IF;
             --cut label for grid
             IF v_secformat = 'T' THEN
             -------------------------------
             -- if condition to hide horizontal line for tabular section
                 IF v_fldtype = 'H' THEN
                     v_fldxsl := '&#xa0;';
                 END IF;
                           --modified by sonia abrol, 03/16/05 to use a function to cut the label
                SELECT  Pkg_Form.f_cut_fieldlabel(v_fldxsl,v_fldtype,v_fld_datatype,v_fld_align,v_pk_field)
                INTO v_fldxsl FROM dual;
                     /* IF v_fldtype <> 'C' AND v_fldtype <> 'H' THEN
                    --modified by sonika on Nov 21, 03 search only for <td width=''15%'' as align option has also been added
                    --modified by sonika on April 19, 04 search only for <td
                        v_pos_1 := INSTR( v_fldxsl, '<td') ;
                       v_repfld_xsl_2 := SUBSTR(v_fldxsl, 0 ,v_pos_1-1);
                        len := LENGTH(v_fldxsl);
                       v_pos_2 := INSTR(v_fldxsl , '</label>');
                       --v_strlen := length( '</label>  &#xa0;&#xa0;&#xa0;</td>' );
                           --Modified by Sonika Talwar on April 22, 04
                            v_pos_endtd := INSTR(v_fldxsl , '</td>',v_pos_2);
                           --# of characters between </label> and </td>
                        v_strlen := LENGTH('</td>');
                        v_strlen := (v_pos_endtd - v_pos_2) + v_strlen;
                          v_repfld_xsl_3 := SUBSTR( v_fldxsl , v_pos_2+v_strlen , len-1 ) ;
                          v_fldxsl := v_repfld_xsl_2 || v_repfld_xsl_3 ;
                      END IF ;
                      */
                    v_fldsameline := 1;
             END IF; --if v_secformat = 'T'
           IF v_count > 1 AND v_fldsameline = 0 AND v_fldtype <> 'H' THEN
                 v_fldxsl := '</tr><tr>' || v_fldxsl ;
           ELSIF v_count > 1 AND v_fldtype = 'H' AND v_secformat <> 'T' THEN
                    v_fldxsl := '</tr></table>' || v_fldxsl || '<table class="dynFormTable" width="100%"><tr>' ;
           END IF;
           IF v_prevsec <> v_pksec THEN -- if form section changes, create section tag
                        -- for section repeat fields of the previous section
                  IF v_prev_sec_repno  > 0 THEN
                      v_repfld_count := 1;
                        FOR x IN (SELECT repfrmfld.PK_REPFORMFLD, repfrmfld.FK_FORMFLD,repfrmfld.FK_FORMSEC,repfrmfld.FK_FIELD,repfrmfld.REPFORMFLD_SEQ,
                                       repfrmfld.REPFORMFLD_XSL,repfrmfld.REPFORMFLD_JAVASCR,repfrmfld.REPFORMFLD_SET, NVL(rep.FLD_SAMELINE,0) FLD_SAMELINE,rep.FLD_NAME,
                                 rep.FLD_KEYWORD, rep.FLD_UNIQUEID,rep.FLD_SYSTEMID,rep.FLD_TYPE,  rep.FLD_DATATYPE,
                                 rep.FLD_DEFRESP ,rep.FLD_COLCOUNT,rep.FLD_LINESNO, orig.FLD_SYSTEMID orig_systemid --salil
                                 FROM ER_REPFORMFLD repfrmfld, ER_FLDLIB rep, ER_FORMFLD, ER_FLDLIB orig
                                 WHERE repfrmfld.FK_FORMSEC = v_prevsec AND
                                 rep.PK_FIELD = repfrmfld.FK_FIELD AND
                                 NVL(rep.fld_isvisible,0) <> 1 AND
                                 NVL(rep.fld_datatype,'Z') <> 'ML' AND
                                  NVL(rep.fld_type,'Z') <> 'F'  AND
                                 NVL(repfrmfld.record_type,'Z') <> 'D' AND
                                 ER_FORMFLD.PK_FORMFLD = repfrmfld.fk_formfld AND
                                 orig.PK_FIELD = ER_FORMFLD.fk_field
                                 ORDER BY REPFORMFLD_SET,REPFORMFLD_SEQ
                                 )
                      LOOP
                          v_rep_pk_field := x.FK_FIELD;
                          v_repfldxsl := x.REPFORMFLD_XSL;
                         v_repfldsameline  :=x.FLD_SAMELINE;
                         v_repfldname := x.FLD_NAME;
                         v_repfldkeyword := x.FLD_KEYWORD;
                         v_repflduniqueid :=  x.FLD_UNIQUEID ;
                         v_repfldsystemid := x.FLD_SYSTEMID;
                         v_repfldtype := x.FLD_TYPE ;
                         v_rep_fld_datatype := x.FLD_DATATYPE ;
                         v_rep_fld_defresp := x.FLD_DEFRESP ;
                         v_repcolcount := x.FLD_COLCOUNT;
                         v_repfld_set := x.REPFORMFLD_SET;
                         v_repfldlinesno := x.FLD_LINESNO ;
                         v_reporigsystemid := x.orig_systemid;
                         --get the changed field xsl
                                IF v_repfldtype = 'S' THEN
                                         v_repfldxsl := v_repfldxsl; --no change in xsl
                                 ELSE
                                       SP_GETFLDPRINTXSL(v_rep_fld_datatype, v_repfldsystemid, v_repfldlinesno, v_repfldxsl);
                                END IF;
                         -- add xsl
                        IF v_prev_sec_format = 'T' THEN
                        -- if condition to hide horizontal line for tabular section
                           IF v_repfldtype = 'H' THEN
                              v_repfldxsl := '&#xa0;';
                           END IF;
                           IF v_prevset <> v_repfld_set AND v_repfld_count = 1 THEN
                             v_repfldsameline := 0;
                           ELSIF v_repfld_count > 1 AND v_prevset = v_repfld_set THEN
                             v_repfldsameline := 1;
                            ELSE
                                 v_repfldsameline := 0;
                           END IF;
                        END IF;
                        v_repfldxsl := REPLACE(v_repfldxsl,'&', '&amp;');
                         IF  v_repfldsameline = 0  THEN
                              IF v_prev_sec_format = 'T' THEN -- sonia 16th june for tabular alignment
                                 v_clubxsl := v_clubxsl || '</tr><tr>' || v_repfldxsl ; -- sonia 16th june for tabular alignment
                               ELSE -- sonia 16th june for tabular alignment
                                    --changed by Sonika on April 19, 04
                               v_clubxsl := v_clubxsl || '</tr> </table> <table class="dynFormTable" width="100%" border="0"> <tr>' || v_repfldxsl ;
                               END IF; -- sonia 16th june for tabular alignment
                          ELSE
                            v_clubxsl := v_clubxsl || v_repfldxsl ;
                        END IF;
                       v_prevset := v_repfld_set ;
                       v_repfld_count := v_repfld_count + 1;
                      END LOOP ;
                  END IF;--if v_prev_sec_repno  > 0
              v_sec_count := 0;
               IF v_secformat = 'T' THEN
                   v_fldxsl := REPLACE(v_fldxsl,'&', '&amp;');
                  SP_GET_PRN_SECTION_HEADER (p_formid, v_pksec , v_sec_grid_header);
                  --remove tooltip overlib script from section header
                     v_pos_1 := INSTR( v_sec_grid_header, 'onmouseover="return overlib') ;
                  WHILE (v_pos_1 > 0)
                  LOOP
                       v_strlen := LENGTH( 'return nd();"');
                        v_pos_2 := INSTR( v_sec_grid_header, 'return nd();"',v_pos_1) ;
                      v_replacestr := SUBSTR(v_sec_grid_header, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
                      v_valuestr := '';
                       v_sec_grid_header := REPLACE(v_sec_grid_header,v_replacestr,v_valuestr);
                    v_pos_1 := INSTR( v_sec_grid_header, 'onmouseover="return overlib',v_pos_1+1) ;
                  END LOOP;
                    --sonia 21st June, changed border=0 for tabular, revreted back the change for tabular grid
                               v_secname:=Pkg_Util.f_escapeSpecialCharsForXML(v_secname);
                        v_clubxsl := v_clubxsl || '</tr></table><P class="dynSection" > ' || v_secname || '</P><table class="dynFormTable" width="100%" border="0"><tr>' || v_sec_grid_header || '</tr><tr>' || v_fldxsl ;
             ELSE
                       v_secname:=Pkg_Util.f_escapeSpecialCharsForXML(v_secname);
                   v_clubxsl := v_clubxsl || '</tr></table><P class="dynSection" > ' || v_secname || '</P><table class="dynFormTable" width="100%" border="0"><tr>' || v_fldxsl ;
             END IF;
           ELSE
                 v_sec_count := v_sec_count + 1;
             --replace special characters
              v_fldxsl := REPLACE(v_fldxsl,'&', '&amp;');
              IF  v_fldsameline = 0  THEN
                     v_clubxsl := v_clubxsl || '</tr> </table> <table class="dynFormTable" width="100%" border="0"> <tr>' || v_fldxsl ;
              ELSE
                       v_clubxsl := v_clubxsl || v_fldxsl ;
              END IF;
           END IF;
            v_prevsec := v_pksec;
            v_prev_sec_repno := v_secrepno;
            v_prev_sec_format := v_secformat;
            v_count := v_count + 1;
        END LOOP ;
        -------------------------------------------------------------------
              IF v_prev_sec_repno > 0 AND v_prevsec > 0 THEN -- for last section
                      v_repfld_count := 1;
                      v_prevset := 0;
                        FOR y IN (SELECT repfrmfld.PK_REPFORMFLD, repfrmfld.FK_FORMFLD,repfrmfld.FK_FORMSEC,repfrmfld.FK_FIELD,repfrmfld.REPFORMFLD_SEQ,
                                       repfrmfld.REPFORMFLD_XSL,repfrmfld.REPFORMFLD_JAVASCR,repfrmfld.REPFORMFLD_SET, NVL(rep.FLD_SAMELINE,0) FLD_SAMELINE,rep.FLD_NAME,
                                 rep.FLD_KEYWORD, rep.FLD_UNIQUEID,rep.FLD_SYSTEMID,rep.FLD_TYPE,  rep.FLD_DATATYPE,
                                 rep.FLD_DEFRESP ,rep.FLD_COLCOUNT,rep.FLD_LINESNO, orig.FLD_SYSTEMID orig_systemid --salil
                                 FROM ER_REPFORMFLD repfrmfld, ER_FLDLIB rep, ER_FORMFLD, ER_FLDLIB orig
                                 WHERE repfrmfld.FK_FORMSEC = v_prevsec AND
                                 rep.PK_FIELD = repfrmfld.FK_FIELD AND
                                  NVL(rep.fld_isvisible,0) <> 1 AND
                                 NVL(rep.fld_datatype,'Z') <> 'ML' AND
                                 NVL(rep.fld_type,'Z') <> 'F'  AND
                                 NVL(repfrmfld.record_type,'Z') <> 'D' AND
                                 ER_FORMFLD.PK_FORMFLD = repfrmfld.fk_formfld AND
                                 orig.PK_FIELD = ER_FORMFLD.fk_field
                                 ORDER BY REPFORMFLD_SET,REPFORMFLD_SEQ
                                 )
                      LOOP
                          v_rep_pk_field := y.FK_FIELD;
                         v_repfldxsl := y.REPFORMFLD_XSL;
                         v_repfldsameline  := y.FLD_SAMELINE;
                         v_repfldname := y.FLD_NAME;
                         v_repfldkeyword := y.FLD_KEYWORD;
                         v_repflduniqueid :=  y.FLD_UNIQUEID ;
                         v_repfldsystemid := y.FLD_SYSTEMID;
                         v_repfldtype := y.FLD_TYPE ;
                         v_rep_fld_datatype := y.FLD_DATATYPE ;
                         v_rep_fld_defresp := y.FLD_DEFRESP ;
                         v_repcolcount := y.FLD_COLCOUNT;
                         v_repfldlinesno := y.FLD_LINESNO ; --salil
                          v_repfld_set := y.REPFORMFLD_SET;
                         v_reporigsystemid := y.orig_systemid;
                          --get the changed field xsl
                         IF v_repfldtype = 'S' THEN
                                         v_repfldxsl := v_repfldxsl; --no change in xsl
                                 ELSE
                                          SP_GETFLDPRINTXSL(v_rep_fld_datatype, v_repfldsystemid, v_repfldlinesno, v_repfldxsl);
                            END IF;
                         -- add xsl
                        IF v_prev_sec_format = 'T' THEN
                         -- if condition to hide horizontal line for tabular section
                         IF v_repfldtype = 'H' THEN
                              v_repfldxsl := '&#xa0;';
                         END IF;
                           IF v_prevset <> v_repfld_set AND v_repfld_count = 1 THEN
                             v_repfldsameline := 0;
                           ELSIF v_repfld_count > 1 AND v_prevset = v_repfld_set THEN
                             v_repfldsameline := 1;
                           ELSE
                                 v_repfldsameline := 0;
                           END IF;
                        END IF;
                         v_repfldxsl := REPLACE(v_repfldxsl,'&', '&amp;');
                         IF  v_repfldsameline = 0  THEN
                            v_clubxsl := v_clubxsl || '</tr>  <tr>' || v_repfldxsl ;
                          ELSE
                            v_clubxsl := v_clubxsl || v_repfldxsl ;
                        END IF;
                       v_prevset := v_repfld_set ;
                       v_repfld_count := v_repfld_count + 1;
                      END LOOP ;
                  END IF;
        --------------------------------------------------------------------
         v_clubxsl := v_clubxsl || '</tr></table>';
          v_formname:=Pkg_Util.f_escapeSpecialCharsForXML(v_formname);
         v_formnametag := '<table width="100%"><tr><td align="center" class="reportName">'||v_formname||'</td></tr></table>';
          --combine the xsl namespace tags, xsl start tags, form tags ,club xsl, xsl end tags to make the entire form xsl
         --sonia 16th June, added v_custom_js
         v_formxsl := v_xslns || v_xslstarttags ||v_xslmidtags || v_interfld_before || v_activation_js || v_interfld_after || v_custom_js || v_formstarttag || v_formnametag || v_clubxsl || v_formendtag || v_xslendtags ;
         --remove mandatory *
          v_formxsl := REPLACE(v_formxsl,'<font class = "mandatory">*</font>',' ');
         -- INSERT INTO T VALUES (v_formxsl,'VerXSL');
          --COMMIT;
         UPDATE ER_FORMLIB
            SET form_viewxsl = v_formxsl
            WHERE pk_formlib = p_formid;
     COMMIT;
   end if;-- if v_form_preserve_formatting = 0

END; -- end of sp_generate_printxsl
------------------------------------------------------------------------------------------------------------
PROCEDURE SP_GETFLDPRINTXSL(p_datatype VARCHAR2, p_fldsystemid VARCHAR2, p_fldlinesno NUMBER, o_fldxsl IN OUT VARCHAR2)
 /****************************************************************************************************
   ** Procedure to change field xsl to make it a label instead of editable
   ** Author: Sonika Talwar Dec 24, 2003
   ** Input parameter: datatype of field
   ** Output parameter: fldxsl
   ** Modified by Sonika Talwar on April 20, 04 to remove the help icon
   ** Modified by Sonika Talwar on May 20, 04 to display radio buttons and checkboxes as such in printout
   ** Modified by Sonika Talwar on June 04, 04 to add line ------- in xsl if there's no value in xml
 **/
AS
 v_fldxsl VARCHAR2(4000);
 v_strlen NUMBER;
 v_pos_1 NUMBER := 0;
 v_pos_2 NUMBER := 0;
 v_valuestr VARCHAR2(2000);
 v_casestr VARCHAR2(2000);
 v_replacestr VARCHAR2(2000);
 v_fldsystemid VARCHAR2(50);
 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_GETFLDPRINTXSL', pLEVEL  => Plog.LFATAL);
BEGIN
        v_fldxsl := o_fldxsl;
       --remove the help icon
        v_pos_1 := INSTR( v_fldxsl, '<img src="../images/jpg/help.jpg"') ;
        IF (v_pos_1 > 0) THEN
          v_strlen := LENGTH( '</img>');
           v_pos_2 := INSTR( v_fldxsl, '</img>',v_pos_1) ;
          v_replacestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
          v_valuestr := '';
            v_fldxsl := REPLACE(v_fldxsl,v_replacestr,v_valuestr);
        END IF;
           --remove script for tooltip overlib
        v_pos_1 := INSTR( v_fldxsl, 'onmouseover="return overlib') ;
        IF (v_pos_1 > 0) THEN
          v_strlen := LENGTH( 'return nd();"');
           v_pos_2 := INSTR( v_fldxsl, 'return nd();"',v_pos_1) ;
          v_replacestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
          v_valuestr := '';
            v_fldxsl := REPLACE(v_fldxsl,v_replacestr,v_valuestr);
        END IF;
       --its an edit type date or number field, replace input tag
       IF NVL(p_datatype,'Z') = 'ED' OR NVL(p_datatype,'Z') = 'EN' THEN
           v_pos_1 := INSTR( v_fldxsl, '<xsl:value-of select') ;
           v_strlen := LENGTH( '/>');
           v_pos_2 := INSTR( v_fldxsl, '/>',v_pos_1) ;
           v_valuestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
         v_casestr := '<xsl:choose> <xsl:when test="string-length('||p_fldsystemid || ') =0"><xsl:text>_______________</xsl:text></xsl:when> ';
           v_valuestr := v_casestr || '<xsl:otherwise> <U>' || v_valuestr || '</U> </xsl:otherwise></xsl:choose>';
           v_pos_1 := INSTR( v_fldxsl, '<input ') ;
          v_strlen := LENGTH( '</input>');
           v_pos_2 := INSTR( v_fldxsl, '</input>') ;
           v_replacestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
        END IF; --ED and EN (edit box date, edit box number)


          --if its an edit type text or textarea, replace input tag or textarea
          IF NVL(p_datatype,'Z') = 'ET' THEN
         IF (NVL(p_fldlinesno,0) <=1) THEN --text
           v_pos_1 := INSTR( v_fldxsl, '<xsl:value-of select') ;
           v_strlen := LENGTH( '/>');
           v_pos_2 := INSTR( v_fldxsl, '/>',v_pos_1) ;
           v_valuestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
           v_pos_1 := INSTR( v_fldxsl, '<input ') ;
          v_strlen := LENGTH( '</input>');
           v_pos_2 := INSTR( v_fldxsl, '</input>') ;
           v_replacestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
         v_casestr := '<xsl:choose> <xsl:when test="string-length('||p_fldsystemid || ') =0"><xsl:text>_______________</xsl:text></xsl:when> ';
           v_valuestr := v_casestr || '<xsl:otherwise> <U>' || v_valuestr || '</U> </xsl:otherwise></xsl:choose>';
         ELSE --textarea
         v_pos_1 := INSTR( v_fldxsl, '<xsl:value-of select') ;
           v_strlen := LENGTH( '/>');
           v_pos_2 := INSTR( v_fldxsl, '/>',v_pos_1) ;
           v_valuestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
           v_pos_1 := INSTR( v_fldxsl, '<textarea ') ;
          v_strlen := LENGTH( '</textarea>');
           v_pos_2 := INSTR( v_fldxsl, '</textarea>') ;
           v_replacestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
          v_casestr := '<xsl:choose> <xsl:when test="'||p_fldsystemid || '= ''er_textarea_tag'' "><xsl:text>_______________</xsl:text></xsl:when> ';
           v_valuestr := v_casestr || '<xsl:otherwise> <U>' || v_valuestr || '</U> </xsl:otherwise></xsl:choose>';
         END IF;
        END IF; --ET (edit box text and textareaa)
           --if its dropdown, replace select
       IF NVL(p_datatype,'Z') = 'MD' THEN
          v_valuestr := '<U> <xsl:for-each select = "' || p_fldsystemid || '/resp"> <xsl:if test="@selected=''1''">  <xsl:value-of select="@dispval"/> </xsl:if> </xsl:for-each></U>';
           v_pos_1 := INSTR( v_fldxsl, '<select ') ;
          v_strlen := LENGTH( '</select>');
           v_pos_2 := INSTR( v_fldxsl, '</select>') ;
           v_replacestr := SUBSTR(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
         v_casestr := '<xsl:choose> <xsl:when test="string-length('||p_fldsystemid || '/@checkboxesdata) =0"><xsl:text>_______________</xsl:text></xsl:when> ';
           v_valuestr := v_casestr || '<xsl:otherwise> <U>' || v_valuestr || '</U> </xsl:otherwise></xsl:choose>';
        END IF; --MD (dropdown)
       --let the checkboxes and radios display as such in printout
           --if its checkbox or radio button, replace <xsl:for-each select
/*       if nvl(p_datatype,'Z') = 'MC' or  nvl(p_datatype,'Z') = 'MR' then
         if nvl(p_datatype,'Z') = 'MC' then
             v_valuestr := '<xsl:for-each select = "' || p_fldsystemid || '/resp"> <xsl:if test="@checked=''1''"> [<xsl:value-of select="@dispval"/>] </xsl:if> </xsl:for-each>';
        elsif nvl(p_datatype,'Z') = 'MR' then
             v_valuestr := '<xsl:for-each select = "' || p_fldsystemid || '/resp"> <xsl:if test="@checked=''1''"> <xsl:value-of select="@dispval"/> </xsl:if> </xsl:for-each>';
        end if;
         v_pos_1 := instr( v_fldxsl, '<xsl:for-each select ') ;
          v_strlen := length( '</xsl:for-each>');
           v_pos_2 := instr( v_fldxsl, '</xsl:for-each>') ;
           v_replacestr := substr(v_fldxsl, v_pos_1, v_pos_2 - v_pos_1 +v_strlen);
        end if; --MC (checkbox)*/
          v_valuestr := v_valuestr || '&#xa0;&#xa0;';
       v_fldxsl := REPLACE(v_fldxsl,v_replacestr,v_valuestr);

          --added by sonia, 11/03/05remove the calendar picture, now calendar picture is added after the input tag
           IF NVL(p_datatype,'Z') = 'ED'  THEN

           --find the calendar link
          -- plog.fatal(pctx,'v_fldxsl1' || v_fldxsl);
                   --v_pos_1 := INSTR( v_fldxsl, '<A onClick="javascript:return fnShowCalendar') ;
                --v_pos_2 := INSTR( v_fldxsl, '</img></A>') ;
                --v_fldxsl := SUBSTR(v_fldxsl,0,v_pos_1) || SUBSTR(v_fldxsl,(v_pos_2 + 11));
                v_fldxsl := replace(v_fldxsl,'<A onClick=','<A style="display:none;" onClick=');
            --    plog.fatal(pctx,'v_fldxsl2' || v_fldxsl);

        END IF;

       o_fldxsl := v_fldxsl;
END; --end of sp_getfldprintxsl
-----------------------------------------------------------------------------------------------

  PROCEDURE SP_GETPRINTFORM_HTML(p_formid NUMBER, p_filledform_id NUMBER, p_linkfrom STRING, o_xml OUT CLOB,o_xsl OUT CLOB)
 /****************************************************************************************************
   ** Procedure to generate printer friendly html
   ** Author: Sonika Talwar Dec 24, 2003
   ** Modified by: Sonika Talwar March 10, 2004, to generate the printxsl if it does not exist
   ** Modified by: Sonika Talwar May 12, 2004, to read the xsl according to form version
   ** Input parameter: formid - PK of the form
   ** Input parameter: filledformid - PK of the filled form
   ** Input parameter: linkfrom
   ** Output parameter: form xml
   ** Output parameter: form xsl

   ** Modified by Sonia Abrol, 11/02/05,  to return xml and xsl, tranformation will be done in java
   **/
AS
   v_html sys.XMLTYPE;
   v_len NUMBER;
   i NUMBER;
   v_returnhtml CLOB;
   tempstr VARCHAR2(4000);
   viewstr VARCHAR2(10);

   v_xmlclob CLOB;
   v_xslClob CLOB;

BEGIN
   --check if the printxsl has been generated or not
   BEGIN
   SELECT 'nullview' INTO viewstr
   FROM ER_FORMLIB
   WHERE  pk_formlib = p_formid
   AND form_viewxsl IS NULL ;

   IF (viewstr='nullview') THEN
      --generate the print xsl
     SP_GENERATE_PRINTXSL(p_formid);
   END IF;

   EXCEPTION WHEN NO_DATA_FOUND THEN
     P('no data');
   END;
   BEGIN


   IF (p_filledform_id > 0) THEN  --the user has filled the form
    IF (p_linkfrom = 'A') THEN

      SELECT e.ACCTFORMS_XML.getClobVal(), formlibver_viewxsl
        INTO V_XMLCLOB , V_XSLCLOB
        FROM ER_ACCTFORMS e, ER_FORMLIBVER
        WHERE PK_ACCTFORMS = p_filledform_id
        AND PK_FORMLIBVER = FK_FORMLIBVER ;

   ELSIF (p_linkfrom = 'S') THEN

   SELECT e.STUDYFORMS_XML.getClobval(), formlibver_viewxsl
        INTO V_XMLCLOB, V_XSLCLOB
        FROM ER_STUDYFORMS e, ER_FORMLIBVER
        WHERE PK_STUDYFORMS = p_filledform_id
        AND PK_FORMLIBVER = FK_FORMLIBVER ;

   ELSIF (p_linkfrom = 'P') THEN

          SELECT     e.PATFORMS_XML.getclobval(), formlibver_viewxsl
        INTO V_XMLCLOB,  V_XSLCLOB
        FROM ER_PATFORMS e, ER_FORMLIBVER
        WHERE PK_PATFORMS = p_filledform_id
        AND PK_FORMLIBVER = FK_FORMLIBVER ;
   ELSIF (p_linkfrom = 'C')THEN

     SELECT e.CRFFORMS_XML.getclobval(),formlibver_viewxsl
        INTO V_XMLCLOB, V_XSLCLOB
        FROM ER_CRFFORMS e, ER_FORMLIBVER
        WHERE PK_CRFFORMS = p_filledform_id
        AND PK_FORMLIBVER = FK_FORMLIBVER ;

   END IF;
 ELSE --user hasn't entered any data

  SELECT a.form_xml.getClobVal() ,a.form_viewxsl
        INTO V_XMLCLOB,  V_XSLCLOB
      FROM     ER_FORMLIB a WHERE pk_formlib = p_formid ;
 END IF;

/*        -- Return the transformed XML instance as a CLOB value.
     o_html := v_html.getClobVal();
     v_len := LENGTH(o_html);
     i:=1;
      WHILE i<=v_len
      LOOP
          tempstr := dbms_lob.SUBSTR(o_html,3000,i);
           tempstr := REPLACE(tempstr,'&quot;','''');
          tempstr := REPLACE(tempstr,'&apos;','''');
          tempstr := REPLACE(tempstr,'&amp;','&'); --replace an ampersand
          tempstr := REPLACE(tempstr, '&lt;' , '<');
           tempstr := REPLACE(tempstr, '&gt;', '>');
            tempstr := REPLACE(tempstr, '\\', '\');
          tempstr := REPLACE(tempstr,'er_textarea_tag<','<');
          tempstr := REPLACE(tempstr, '&#xa0;' , ' ');
          v_returnhtml := v_returnhtml || tempstr;
           i := i + 3000 ;
      END LOOP;
        o_html := v_returnhtml;
        o_html := REPLACE(o_html,'&amp;','&');
        o_html := REPLACE(o_html,'&quot;','''');
        o_html := REPLACE(o_html,'&apos;','''');
        o_html := REPLACE(o_html,'&lt;', '<');
        o_html := REPLACE(o_html,'&gt;', '>');
        o_html := REPLACE(o_html,'\\', '\');
        o_html := REPLACE(o_html, '&#xa0;' , ' ');
        -- replace the er_textarea_tag added to the text area field
        -- "er_textarea_tag<"is being replaced with "<" since replace  didn't work  when the string to be replaced is blank
        o_html := REPLACE(o_html,'er_textarea_tag<','<'); */

        o_xml := V_XMLCLOB;
        o_xsl :=   V_XSLCLOB;

  EXCEPTION  WHEN OTHERS THEN
       RAISE_APPLICATION_ERROR (-20102, 'Exception occurred in SP_GET_FORM_HTML :'||SQLERRM);
  END;
END; --end of sp_getprintform_html
------------------
PROCEDURE SP_GET_PRN_SECTION_HEADER (p_formid NUMBER, p_section NUMBER, o_xsl OUT VARCHAR2)
AS
 /****************************************************************************************************
   ** Procedure to get XSl for printing tabular type section's header
   ** Author: Anu Khanna 3 June 2004
   ** Input parameter: formid - PK of the form
   ** Input parameter: section id  - PK of the section
   ** Output parameter: headerXSL
   **/
   v_fldxsl VARCHAR2(4000);
   v_fld_esign_validate VARCHAR2(4000);
   v_sec_grid_label_beg_pos NUMBER;
   v_sec_grid_label_end_pos  NUMBER;
   v_sec_grid_header_label VARCHAR2(1000);
   v_help_beg_pos NUMBER;
   v_help_end_pos NUMBER;
   v_help_icon VARCHAR2(1000);
   v_sec_grid_header VARCHAR2(4000);
   v_fld_type CHAR(1) ;
   v_fld_datatype VARCHAR2(3);
BEGIN
    FOR i IN (SELECT  formfld_seq , formfld_xsl , fld_type , fld_datatype, fld_systemid
          FROM ER_FORMFLD, ER_FLDLIB
          WHERE  fk_formsec = p_section AND
          pk_field = fk_field AND
          NVL(fld_isvisible,0) <> 1 AND
          ER_FORMFLD.record_type <> 'D' AND fld_type <> 'H' AND NVL(fld_datatype,'Z') <> 'ML'  AND NVL(fld_type,'Z') <> 'F'
          ORDER BY formfld_seq  )
    LOOP
       v_fldxsl := i.formfld_xsl;
       v_fld_type := i.fld_type;
       v_fld_datatype := i.fld_datatype;
        IF v_fld_type = 'S' THEN
           v_sec_grid_header_label := '<td>Space Break</td>';
       ELSIF v_fld_datatype = 'ML' THEN
              v_sec_grid_header_label := '<td>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;</td>';
       --added by Sonika as per issue # 984, they want Comments as header in case of Comments type field
       ELSIF v_fld_type = 'C' THEN
              v_sec_grid_header_label := '<td>Comments</td>';
       ELSE
           v_sec_grid_label_beg_pos := INSTR(v_fldxsl, '<label');
           v_sec_grid_label_end_pos  := INSTR(v_fldxsl, '</label>');
              v_sec_grid_label_end_pos := v_sec_grid_label_end_pos -  v_sec_grid_label_beg_pos + 8;
           v_sec_grid_header_label := '<td>' || SUBSTR(v_fldxsl,v_sec_grid_label_beg_pos,v_sec_grid_label_end_pos ) || '</td>';
        END IF;
         v_sec_grid_header := v_sec_grid_header || v_sec_grid_header_label  ;
    END LOOP;
    --p(substr(v_sec_grid_header,1,1000));
    o_xsl := v_sec_grid_header;
END; -- end of SP_GET_PRN_SECTION_HEADER
------------
--------------------------------anu----------
/****************************************************************************************************
   ** Procedure insert records in query and query status.
   ** Author: Anu Khanna 4th Jan 2005
   ** Input parameter: p_filledform_id - PK of the filled form
   ** Input parameter: p_called_from - module linked to (Stores 1- for patient forms, 2- for study forms, 3- for account  forms)
   ** Input parameter: p_query_type - (Stores 1- for System Query ,  2- for Manual Query , 3- for Response)
   ** Input parameter: p_query_type_id - Stores codelst id for a query type
   ** Input parameter: p_fldsys_ids - field system id for which query is entered
   ** Input parameter: p_fld_notes - notes for the query entered
    ** Input parameter: p_query_status - query status
  ** Input parameter: p_creator  - creator of the query
   ** Input parameter: p_ip_Add - ip address
   ** Output parameter: o_ret_number
   **/
PROCEDURE SP_INSERT_FORM_QUERY(p_filledform_id IN NUMBER, p_form_id IN NUMBER, p_called_from NUMBER, p_query_type NUMBER, p_query_type_id IN ARRAY_STRING,
p_fldsys_ids IN ARRAY_STRING,  p_fld_notes IN ARRAY_STRING,
p_query_status IN ARRAY_STRING, p_query_comments IN ARRAY_STRING,
p_mode IN ARRAY_STRING, p_pk_query_status IN ARRAY_STRING,
p_creator IN NUMBER,  p_ip_Add IN VARCHAR2,o_ret_number OUT NUMBER)
AS
v_count NUMBER;
v_pk_form_query NUMBER;
v_pk_form_query_status NUMBER;
i NUMBER;
mod_counter NUMBER;
v_fldsys_id VARCHAR2(50);
v_reason NUMBER;
v_note VARCHAR2(4000);
v_query_open_status NUMBER;
v_query_status NUMBER;
v_query_type_id NUMBER;
v_systemQueryId NUMBER;
v_query_comments VARCHAR2(4000);
v_mode VARCHAR2(50);
v_pk_query_status NUMBER;--KM
v_system_note VARCHAR2(4000);

 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_INSERT_FORM_QUERY', pLEVEL  => Plog.LDEBUG);
BEGIN
v_count := p_fldsys_ids.COUNT();
i := 1;
mod_counter := 1;
 SELECT pk_codelst INTO v_systemQueryId
     FROM ER_CODELST
     WHERE codelst_type ='form_query'
     AND codelst_subtyp = 'sys_gen_val';
     SELECT pk_codelst INTO v_query_open_status
     FROM ER_CODELST WHERE codelst_type = 'query_status' AND
     codelst_subtyp= 'open';
WHILE (i<= v_count) LOOP
     v_fldsys_id := p_fldsys_ids(i);
     v_note := p_fld_notes(i);
     v_query_status := TO_NUMBER(p_query_status(i));
     v_query_comments := trim(p_query_comments(i));
     plog.debug(pctx, v_query_comments);

     v_query_type_id := TO_NUMBER(p_query_type_id(i));
     v_mode :=  trim(p_mode(i));
     IF (v_mode = 'M') THEN
     v_pk_query_status := TO_NUMBER(p_pk_query_status(mod_counter)); --KM
     mod_counter := mod_counter+1;
     END IF;


	IF (v_mode = 'N') THEN
		SELECT seq_er_form_query.NEXTVAL
		INTO v_pk_form_query FROM dual ;

		INSERT INTO ER_FORMQUERY
		(PK_FORMQUERY ,FK_QUERYMODULE,
		QUERYMODULE_LINKEDTO, FK_FIELD,
		CREATOR,IP_ADD, created_on
		)
		VALUES
		(v_pk_form_query,p_filledform_id,
		p_called_from,
		(select MP_PKFLD from ER_MAPFORM where FK_FORM = p_form_id and MP_SYSTEMID = v_fldsys_id),
		p_creator, p_ip_Add,SYSDATE);

		SELECT seq_er_form_query_status.NEXTVAL
		INTO v_pk_form_query_status FROM dual ;
		-- insert the system generated query for the fields
		INSERT INTO ER_FORMQUERYSTATUS
		(PK_FORMQUERYSTATUS, FK_FORMQUERY,
		FORMQUERY_TYPE, FK_CODELST_QUERYTYPE,
		QUERY_NOTES, ENTERED_ON, ENTERED_BY,
		FK_CODELST_QUERYSTATUS,
		CREATOR, IP_ADD, created_on)
		VALUES
		(v_pk_form_query_status, v_pk_form_query,
		1, v_systemQueryId ,
		v_note, SYSDATE, 0,
		v_query_open_status,
		0, p_ip_Add, SYSDATE
		);

		SELECT seq_er_form_query_status.NEXTVAL
		INTO v_pk_form_query_status FROM dual ;

		-- enter the response for the query
		INSERT INTO ER_FORMQUERYSTATUS
		(PK_FORMQUERYSTATUS, FK_FORMQUERY,
		FORMQUERY_TYPE, FK_CODELST_QUERYTYPE,
		QUERY_NOTES, ENTERED_ON, ENTERED_BY,
		FK_CODELST_QUERYSTATUS,
		CREATOR, IP_ADD, created_on)
		VALUES
		(v_pk_form_query_status, v_pk_form_query,
		3, v_query_type_id,
		v_query_comments, SYSDATE, p_creator,
		v_query_status,
		p_creator, p_ip_Add, SYSDATE);
	ELSE
		SELECT PK_FORMQUERY, prim.QUERY_NOTES
    INTO v_pk_form_query, v_system_note
    FROM ER_FORMQUERY q, ER_FORMQUERYSTATUS prim
		where prim.FK_FORMQUERY = PK_FORMQUERY
    and prim.PK_FORMQUERYSTATUS = (select min(sub.PK_FORMQUERYSTATUS)
    from ER_FORMQUERYSTATUS sub
    where sub.FK_FORMQUERY = PK_FORMQUERY)
		and FK_QUERYMODULE = p_filledform_id
		AND QUERYMODULE_LINKEDTO = p_called_from
		AND FK_FIELD = (select MP_PKFLD from ER_MAPFORM where FK_FORM = p_form_id and MP_SYSTEMID = v_fldsys_id)
    	and prim.QUERY_NOTES = v_note;

    if (v_system_note <> v_note) then
      --dependentQuery;
      BEGIN
        SELECT seq_er_form_query.NEXTVAL
        INTO v_pk_form_query FROM dual ;

        INSERT INTO ER_FORMQUERY
        (PK_FORMQUERY ,FK_QUERYMODULE,
        QUERYMODULE_LINKEDTO, FK_FIELD,
        CREATOR,IP_ADD, created_on
        )
        VALUES
        (v_pk_form_query,p_filledform_id,
        p_called_from,
        (select MP_PKFLD from ER_MAPFORM where FK_FORM = p_form_id and MP_SYSTEMID = v_fldsys_id),
        p_creator, p_ip_Add,SYSDATE);

        SELECT seq_er_form_query_status.NEXTVAL
        INTO v_pk_form_query_status FROM dual ;
        -- insert the system generated query for the fields
        INSERT INTO ER_FORMQUERYSTATUS
        (PK_FORMQUERYSTATUS, FK_FORMQUERY,
        FORMQUERY_TYPE, FK_CODELST_QUERYTYPE,
        QUERY_NOTES, ENTERED_ON, ENTERED_BY,
        FK_CODELST_QUERYSTATUS,
        CREATOR, IP_ADD, created_on)
        VALUES
        (v_pk_form_query_status, v_pk_form_query,
        1, v_systemQueryId ,
        v_note, SYSDATE, 0,
        v_query_open_status,
        0, p_ip_Add, SYSDATE
        );

        SELECT seq_er_form_query_status.NEXTVAL
        INTO v_pk_form_query_status FROM dual ;

        -- enter the response for the query
        INSERT INTO ER_FORMQUERYSTATUS
        (PK_FORMQUERYSTATUS, FK_FORMQUERY,
        FORMQUERY_TYPE, FK_CODELST_QUERYTYPE,
        QUERY_NOTES, ENTERED_ON, ENTERED_BY,
        FK_CODELST_QUERYSTATUS,
        CREATOR, IP_ADD, created_on)
        VALUES
        (v_pk_form_query_status, v_pk_form_query,
        3, v_query_type_id,
        v_query_comments, SYSDATE, p_creator,
        v_query_status,
        p_creator, p_ip_Add, SYSDATE);
      END;
    else
      BEGIN
        SELECT seq_er_form_query_status.NEXTVAL
        INTO v_pk_form_query_status FROM dual ;

        -- enter the response for the query
        INSERT INTO ER_FORMQUERYSTATUS
        (PK_FORMQUERYSTATUS, FK_FORMQUERY,
        FORMQUERY_TYPE, FK_CODELST_QUERYTYPE,
        QUERY_NOTES, ENTERED_ON, ENTERED_BY,
        FK_CODELST_QUERYSTATUS,
        CREATOR, IP_ADD, created_on)
        VALUES
        (v_pk_form_query_status, v_pk_form_query,
        3, v_query_type_id,
        v_query_comments, SYSDATE, p_creator,
        v_query_status,
        p_creator, p_ip_Add, SYSDATE);

        --JM: 17Aug2010, #4848
        update ER_FORMQUERY set
        LAST_MODIFIED_BY = p_creator
        where PK_FORMQUERY = v_pk_form_query;
      END;
    end if;
	END IF;

i := i+1;
END LOOP;--while
COMMIT;
o_ret_number := v_pk_form_query;
RETURN;
END;
/****************************************************************************************************
   ** Procedure insert records in query and query status for a query or response
   ** Author: Anu Khanna 4th Jan 2005
   ** Input parameter: p_filledform_id - PK of the filled form
   ** Input parameter: p_called_from - module linked to (Stores 1- for patient forms, 2- for study forms, 3- for account  forms)
   ** Input parameter: p_query_type - (Stores 1- for System Query ,  2- for Manual Query , 3- for Response)
   ** Input parameter: p_query_type_id - Stores codelst id for a query type
   ** Input parameter: p_fldsys_ids - field system id for which query is entered
   ** Input parameter: p_fld_notes - notes for the query entered
    ** Input parameter: p_query_status - query status
  ** Input parameter: p_creator  - creator of the query
   ** Input parameter: p_ip_Add - ip address
   ** Output parameter: o_ret_number
   ** Modified by Jnanamay Majumdar on 12Mar2009: PKG_DATEUTIL.F_GET_DATEFORMAT added in place of hard coded date format
   **/
PROCEDURE SP_INSERT_QUERY_RESPONSE(p_filledform_id IN NUMBER, p_called_from NUMBER, p_fld_id NUMBER,
                                                 p_query_type NUMBER, p_query_type_id NUMBER,
                                                 p_query_status_id NUMBER,  p_fld_comments VARCHAR2,
                                               p_entered_on VARCHAR2, p_entered_by NUMBER,
                                               p_creator IN NUMBER,  p_ip_Add IN VARCHAR2,o_ret_number OUT NUMBER)
AS
v_count NUMBER;
v_pk_form_query NUMBER;
v_pk_form_query_status NUMBER;
i NUMBER;
v_fldsys_id VARCHAR2(50);
v_reason NUMBER;
v_note VARCHAR2(4000);
v_query_status NUMBER;
v_query_type_id NUMBER;
v_systemQueryId NUMBER;
BEGIN
       SELECT seq_er_form_query.NEXTVAL
       INTO v_pk_form_query FROM dual ;
    INSERT INTO ER_FORMQUERY
     (PK_FORMQUERY ,FK_QUERYMODULE,
     QUERYMODULE_LINKEDTO, FK_FIELD,
     CREATOR,IP_ADD, created_on
      )
     VALUES
      (v_pk_form_query,p_filledform_id,
       p_called_from, p_fld_id,
      p_creator, p_ip_Add,SYSDATE);
      SELECT seq_er_form_query_status.NEXTVAL
       INTO v_pk_form_query_status FROM dual ;
       -- insert
       INSERT INTO ER_FORMQUERYSTATUS
       (PK_FORMQUERYSTATUS, FK_FORMQUERY,
       FORMQUERY_TYPE, FK_CODELST_QUERYTYPE,
       QUERY_NOTES, ENTERED_ON, ENTERED_BY,
       FK_CODELST_QUERYSTATUS,
       CREATOR, IP_ADD, created_on)
       VALUES
       (v_pk_form_query_status, v_pk_form_query,
       p_query_type, p_query_type_id ,
       p_fld_comments, sysdate, p_entered_by,
       p_query_status_id,
        p_creator, p_ip_Add, SYSDATE
       );
COMMIT;
o_ret_number := v_pk_form_query;
RETURN;
END SP_INSERT_QUERY_RESPONSE;
--------------------------------anu----------

/* returns 1: if user has acccess to form's filled responses, 0: if user does not have access to form's filled responses
p_form : the form
p_user : the loggedin user
 p_filledform_creator : the creator of filled form record
by Sonia Abrol, 12/14/05, to use in AdHoc reports
*/

FUNCTION fn_getUserAccess (p_form NUMBER,p_user NUMBER,p_filledform_creator NUMBER) RETURN NUMBER
IS
v_ct NUMBER;
v_pk_lf NUMBER;
BEGIN

-- check if the linked form has 'keep response to private' defined

SELECT COUNT(*)
INTO v_pk_lf
FROM ER_LINKEDFORMS , ER_OBJECTSHARE a
WHERE fk_formlib = p_form AND a.fk_object = pk_lf AND
a.object_number=6  ;

IF v_pk_lf = 0 THEN
RETURN 1;
END IF;

SELECT DISTINCT pk_lf
INTO v_pk_lf
FROM ER_LINKEDFORMS , ER_OBJECTSHARE a
WHERE fk_formlib = p_form AND a.fk_object = pk_lf AND
a.object_number=6  ;

SELECT COUNT( *)
INTO v_ct
FROM ER_OBJECTSHARE a
WHERE fk_objectshare_id=p_user AND fk_object = v_pk_lf AND
(
( ( a.object_number=4 OR a.object_number=5 OR a.object_number=6) AND a.objectshare_type='U' )
OR p_user =p_filledform_creator ) ;

IF v_ct <= 0 THEN
RETURN 0;
ELSE
RETURN 1;
END IF;

END;

FUNCTION fn_formfld_validate_partial(p_form_id NUMBER, p_system_id VARCHAR2, p_study_id NUMBER, p_patient_id NUMBER, p_account_id NUMBER, p_datavalue VARCHAR2) RETURN VARCHAR2 IS
v_retData NUMBER :=1;
v_targetForm NUMBER;
v_targetFld VARCHAR2(250);
v_operator VARCHAR2(5);
v_message VARCHAR2(4000);
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'fn_formfld_validate_partial', pLEVEL  => Plog.LDEBUG);
BEGIN
Plog.DEBUG(pCTX,'Start Request with p_form_id='||p_form_id|| '  p_system_id'||p_system_id );
SELECT formaction_targetform,formaction_targetfld,formaction_operator,formaction_msg INTO v_targetForm,v_targetFld,v_operator,v_message FROM ER_FORMACTION
WHERE FORMACTION_SOURCEFORM=p_form_id AND FORMACTION_SOURCEFLD=p_system_id;

Plog.DEBUG(pCTX,'v_targetform=' ||v_targetForm || 'v_targetFld' || v_targetfld || 'p_datavalue '||p_datavalue|| 'p_account_id' ||p_account_id);

v_retData:=fn_formfld_validate(v_targetForm, 0, v_targetFld, p_study_id , p_patient_id , p_account_id , p_datavalue ,v_operator );
IF (v_retData>0) THEN
IF (LENGTH(v_message)>0) THEN
RETURN v_message;
ELSE
RETURN 'Validation Successful';
END IF;
END IF;

RETURN '';

EXCEPTION WHEN OTHERS THEN
 RAISE_APPLICATION_ERROR (-20102, 'fn_formfld_validate_partial:'||SQLERRM);
END;

FUNCTION fn_formfld_validate(p_form_id NUMBER, p_response_id NUMBER, p_system_id VARCHAR2, p_study_id NUMBER, p_patient_id NUMBER, p_account_id NUMBER, p_datavalue VARCHAR2,p_operator VARCHAR2 ) RETURN NUMBER IS


v_retdata NUMBER :=1;
v_condition VARCHAR2(8000);
v_condition_expanded VARCHAR2(8000);
v_colname VARCHAR2(1000);
v_formtype_query_final VARCHAR2(1000);
v_colDataQuery VARCHAR2(1000);
v_formtype_query VARCHAR2(1000);
v_formtype VARCHAR2(10);
v_datacond VARCHAR2(1000);
v_fldType VARCHAR2(50);
v_date DATE;

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'fn_formfld_validate', pLEVEL  => Plog.LDEBUG);
BEGIN
     v_condition:='';
     v_colname:='';

        v_colDataQuery:='SELECT count(*) FROM er_formslinear ';

        v_condition:=' WHERE fk_form='||p_form_id|| ' ';

              Plog.DEBUG(pCTX,'1....p_response_id' ||p_response_id || 'length(p_response_id)' || LENGTH(p_response_id)|| 'p_form_id is '||p_form_id);

        IF LENGTH(p_response_id)>0 AND p_response_id<>0  THEN
           v_condition:= v_condition||' and fk_filledform='||p_response_id;
        END IF;

         SELECT    mp_formtype, mp_mapcolname,mp_flddatatype  INTO v_formtype,v_colname,v_fldType FROM ER_MAPFORM WHERE
        mp_systemid=p_system_id AND fk_form=p_form_id;

        Plog.DEBUG(pCTX,'from type & Column Name  '|| v_formtype || 'and' ||v_colname||' and Field Type' ||v_fldType);

         IF(v_formtype='A')  THEN
                 v_condition :=v_condition|| ' and ID = '||p_account_id;
             ELSIF (v_formtype='P') AND LENGTH(LTRIM(RTRIM(p_patient_id)))>0 THEN
                  IF(LENGTH(LTRIM(RTRIM(p_study_id)))>0) AND (p_study_id<>0) THEN
                v_condition :=v_condition|| ' and ID = '||p_patient_id||' and fk_patprot =(select pk_patprot from er_patprot where fk_per='||p_patient_id||'and'||
                'fk_study='||p_study_id||')';
                ELSE
                v_condition :=v_condition|| ' and ID = '||p_patient_id;
                END IF;

                Plog.DEBUG(pCTX,'v_formtype and p_study_id' ||v_formtype||'and'||p_study_id);
             ELSIF(v_formtype='S') AND LENGTH(LTRIM(RTRIM(p_study_id)))>0 THEN
                         v_condition :=v_condition|| ' and ID = '||p_study_id;
         END IF;


             IF(v_fldType='EN') THEN
               Plog.DEBUG(pCTX,'responseID' ||p_response_id);
                  IF (p_response_id>0) THEN
                           v_condition:=v_condition||' and pkg_util.f_to_number('''||p_datavalue||''')'||p_operator||'pkg_util.f_to_number('||v_colname||')';
                       ELSE

                         IF (p_operator='>') THEN
                             v_condition_expanded:=v_condition||' and pkg_util.f_to_number('''||p_datavalue||''')'||p_operator||'(select max(pkg_util.f_to_number('||v_colname||')) from er_formslinear ' || v_condition||')';
                             ELSIF (p_operator='<') THEN
                             v_condition_expanded:=v_condition||' and pkg_util.f_to_number('''||p_datavalue||''')'||p_operator||'(select min(pkg_util.f_to_number('||v_colname||')) from er_formslinear ' || v_condition||')';
                              ELSE
                              v_condition_expanded:=v_condition||' and pkg_util.f_to_number('''||p_datavalue||''')'||p_operator||'pkg_util.f_to_number('||v_colname||')';
                            END IF;--for p_opeator
                        v_condition:=v_condition_expanded;
                    END IF;-- for p_response

              ELSIF (v_fldType='ED') THEN
               IF (p_response_id>0) THEN
                  v_condition:=v_condition||' and f_to_date('''||p_datavalue||''')'||p_operator||'f_to_date('||v_colname||')';

                     ELSE
                  IF (p_operator='>') THEN
                       v_condition_expanded:=v_condition||' and f_to_date('''||p_datavalue||''')'||p_operator||'(select max(f_to_date('||v_colname||')) from er_formslinear '||v_condition||')';
                      ELSIF(p_operator='<') THEN
                       v_condition_expanded:=v_condition||' and f_to_date('''||p_datavalue||''')'||p_operator||'(select min(f_to_date('||v_colname||')) from er_formslinear '||v_condition||')';
                  ELSE
                      v_condition_expanded:=v_condition||' and f_to_date('''||p_datavalue||''')'||p_operator||'f_to_date('||v_colname||')';

                    END IF;
                  v_condition:=v_condition_expanded;
               END IF; -- for p_response_id
              END IF;

              Plog.DEBUG(pCTX,'Final condition' ||v_condition);
              IF (LENGTH(v_condition)>0) THEN
                 v_coldataquery:=v_coldataquery||v_condition;
              END IF;


         Plog.DEBUG(pCTX,'Final Query'||v_coldataquery);

         EXECUTE IMMEDIATE v_coldataquery INTO v_retdata;

         Plog.DEBUG(pCTX,'Final Return value'|| v_retdata);

    RETURN v_retdata ;
    EXCEPTION WHEN OTHERS THEN
 RAISE_APPLICATION_ERROR (-20102, 'fn_formfld_validate:'||SQLERRM);
END;


END Pkg_Filledform;
/


CREATE OR REPLACE SYNONYM ESCH.PKG_FILLEDFORM FOR PKG_FILLEDFORM;


CREATE OR REPLACE SYNONYM EPAT.PKG_FILLEDFORM FOR PKG_FILLEDFORM;


GRANT EXECUTE, DEBUG ON PKG_FILLEDFORM TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_FILLEDFORM TO ESCH;

