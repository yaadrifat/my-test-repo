/* Formatted on 4/6/2009 3:17:38 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_USER_MORE_DETAILS
(
   PK_MOREDETAILS,
   FIELD_NAME,
   FIELD_VALUE,
   CREATED_ON,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   RESPONSE_ID,
   USER_NAME,
   FK_ACCOUNT
)
AS
   SELECT   pk_moredetails,
            (SELECT   codelst_desc
               FROM   er_codelst
              WHERE   pk_codelst = MD_MODELEMENTPK)
               field_name,
            MD_MODELEMENTDATA AS field_value,
            a.CREATED_ON CREATED_ON,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            a.LAST_MODIFIED_DATE LAST_MODIFIED_DATE,
            A.RID,
            PK_MOREDETAILS RESPONSE_ID,
            u.USR_FIRSTNAME || ' ' || u.USR_LASTNAME AS USER_NAME,
            u.fk_account AS fk_account
     FROM   er_moredetails a, er_user u
    WHERE   a.MD_MODNAME = 'user' AND fk_modpk = u.pk_user;


