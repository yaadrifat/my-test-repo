/* Formatted on 2/9/2010 1:39:55 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_LIB_FORM
(
   LBFRM_TYPE,
   LBFRM_NAME,
   LBFRM_DESC,
   LBFRM_STATUS,
   LBFRM_SHARED_WITH,
   FK_ACCOUNT,
   LBFRM_RESPONSE_ID,
   RID,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON
)
AS
   SELECT   (SELECT   CATLIB_NAME
               FROM   ER_CATLIB
              WHERE   PK_CATLIB = FK_CATLIB)
               LBFRM_TYPE,
            FORM_NAME LBFRM_NAME,
            FORM_DESC LBFRM_DESC,
            F_CODELST_DESC (FORM_STATUS) LBFRM_STATUS,
            F_GET_SHAREDWITH (FORM_SHAREDWITH) LBFRM_SHARED_WITH,
            FK_ACCOUNT,
            PK_FORMLIB LBFRM_RESPONSE_ID,
            RID,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = ER_FORMLIB.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = ER_FORMLIB.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            LAST_MODIFIED_DATE,
            CREATED_ON
     FROM   ER_FORMLIB
    WHERE   form_linkto = 'L';


