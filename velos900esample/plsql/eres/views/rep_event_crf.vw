/* Formatted on 2/9/2010 1:39:52 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_EVENT_CRF
(
   EVCRF_EVENT,
   EVCRF_NUMBER,
   EVCRF_NAME,
   FK_ACCOUNT,
   EVCRF_RESPONSE_ID,
   RID,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   CREATED_ON,
   EVENT_CATEGORY_NAME,
   EVENT_LIBRARY_TYPE,
   BUDGET_CATEGORY
)
AS
   SELECT   NAME EVCRF_EVENT,
            c.CRFLIB_NUMBER EVCRF_NUMBER,
            c.CRFLIB_NAME EVCRF_NAME,
            USER_ID FK_ACCOUNT,
            c.PK_CRFLIB EVCRF_RESPONSE_ID,
            c.RID,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = c.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = c.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            c.LAST_MODIFIED_DATE,
            c.CREATED_ON,
            (SELECT A.NAME 
              FROM EVENT_DEF A 
              WHERE a.event_type = 'L'  AND A.EVENT_ID = EVENT_DEF.CHAIN_ID) EVENT_CATEGORY_NAME,
            (SELECT CODELST_DESC 
	     FROM SCH_CODELST , EVENT_DEF B   
	     WHERE  B.event_type = 'L'  AND B.EVENT_ID = EVENT_DEF.CHAIN_ID AND 
	     SCH_CODELST.PK_CODELST = EVENT_DEF.EVENT_LIBRARY_TYPE) EVENT_LIBRARY_TYPE,
	    (SELECT CODELST_DESC 
	     FROM SCH_CODELST , EVENT_DEF B   
	     WHERE  B.event_type = 'L'  AND B.EVENT_ID = EVENT_DEF.CHAIN_ID AND 
	     SCH_CODELST.PK_CODELST = EVENT_DEF.EVENT_LINE_CATEGORY) BUDGET_CATEGORY
     FROM   SCH_CRFLIB c, EVENT_DEF
    WHERE   EVENT_ID = FK_EVENTS AND event_type = 'E'
   UNION
   SELECT   NAME EVCRF_EVENT,
            NULL EVCRF_NUMBER,
            DECODE (fk_form,
                    0, other_links,
                    (SELECT   form_name
                       FROM   ER_FORMLIB
                      WHERE   pk_formlib = fk_form))
               EVCRF_NAME,
            USER_ID FK_ACCOUNT,
            c.PK_EVENTCRF EVCRF_RESPONSE_ID,
            c.RID,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = c.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = c.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            c.LAST_MODIFIED_DATE,
            c.CREATED_ON,
	    (SELECT A.NAME 
              FROM EVENT_DEF A 
              WHERE a.event_type = 'L'  AND A.EVENT_ID = EVENT_DEF.CHAIN_ID) EVENT_CATEGORY_NAME,
            (SELECT CODELST_DESC 
	     FROM SCH_CODELST , EVENT_DEF B   
	     WHERE  B.event_type = 'L'  AND B.EVENT_ID = EVENT_DEF.CHAIN_ID AND 
	     SCH_CODELST.PK_CODELST = EVENT_DEF.EVENT_LIBRARY_TYPE) EVENT_LIBRARY_TYPE,
	    (SELECT CODELST_DESC 
	     FROM SCH_CODELST , EVENT_DEF B   
	     WHERE  B.event_type = 'L'  AND B.EVENT_ID = EVENT_DEF.CHAIN_ID AND 
	     SCH_CODELST.PK_CODELST = EVENT_DEF.EVENT_LINE_CATEGORY) BUDGET_CATEGORY
     FROM   ESCH.SCH_EVENT_CRF c, EVENT_DEF
    WHERE   EVENT_ID = FK_EVENT AND event_type = 'E';


