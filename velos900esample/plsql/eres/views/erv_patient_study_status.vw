/* Formatted on 4/6/2009 4:13:57 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERES.ERV_PATIENT_STUDY_STATUS
(
   PATIENT_ID,
   PATIENT_STUDY_ID,
   STUDY_NUMBER,
   STUDY_TITLE,
   STATUS,
   STATUS_DATE,
   REASON,
   NOTES,
   ASSIGNED_TO,
   PHYSICIAN,
   TREATMENT_LOCATION,
   RAND_NUMBER,
   ENROLLED_BY,
   SCREEN_NUMBER,
   SCREENED_BY,
   SCREENING_OUTCOME,
   FK_PER,
   PK_STUDY,
   CREATED_ON,
   FK_CODELST_PTST_EVAL_FLAG,
   FK_CODELST_PTST_EVAL,
   FK_CODELST_PTST_INEVAL,
   FK_CODELST_PTST_SURVIVAL,
   FK_CODELST_PTST_DTH_STDREL,
   DEATH_STD_RLTD_OTHER,
   DATE_OF_DEATH,
   FK_CODELST_PAT_DTH_CAUSE,
   CAUSE_OF_DEATH_OTHER,
   FK_ACCOUNT,
   PSTAT_ENROLL_SITE,
   PSTAT_NEXT_FU_DATE,
   PSTAT_IC_VERS_NUM,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   PSTAT_RESPONSE_ID,
   CURRENT_STAT,
   PATPROT_TREATINGORG,
   FK_SITE_ENROLLING
)
AS
   SELECT   per_code patient_id,
            patprot_patstdid patient_study_id,
            study_number,
            study_title,
            f_get_codelstdesc (fk_codelst_stat) status,
            TRUNC (patstudystat_date) status_date,
            f_get_codelstdesc (patstudystat_reason) reason,
            patstudystat_note notes,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = fk_userassto)
               assigned_to,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = patprot_physician)
               physician,
            f_get_codelstdesc (fk_codelstloc) treatment_location,
            patprot_random rand_number,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = fk_user)
               enrolled_by,
            screen_number,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = screened_by)
               screened_by,
            f_get_codelstdesc (screening_outcome) screening_outcome,
            y.fk_per,
            pk_study,
            y.created_on,
            f_get_codelstdesc (x.fk_codelst_ptst_eval_flag)
               fk_codelst_ptst_eval_flag,
            f_get_codelstdesc (x.fk_codelst_ptst_eval) fk_codelst_ptst_eval,
            f_get_codelstdesc (x.fk_codelst_ptst_ineval)
               fk_codelst_ptst_ineval,
            f_get_codelstdesc (x.fk_codelst_ptst_survival)
               fk_codelst_ptst_survival,
            f_get_codelstdesc (x.fk_codelst_ptst_dth_stdrel)
               fk_codelst_ptst_dth_stdrel,
            x.death_std_rltd_other,
            x.date_of_death,
            f_get_codelstdesc ( (SELECT   fk_codelst_pat_dth_cause
                                   FROM   person
                                  WHERE   pk_person = pk_per))
               fk_codelst_pat_dth_cause,
            (SELECT   cause_of_death_other
               FROM   person
              WHERE   pk_person = pk_per)
               cause_of_death_other,
            er_per.fk_account,
            (SELECT   site_name
               FROM   er_site
              WHERE   pk_site = fk_site_enrolling)
               pstat_enroll_site,
            y.next_followup_on pstat_next_fu_date,
            y.inform_consent_ver pstat_ic_vers_num,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = y.creator)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = y.last_modified_by)
               last_modified_by,
            y.last_modified_date,
            y.rid,
            pk_patstudystat pstat_response_id,
            f_get_yesno (current_stat) current_stat,
            patprot_treatingorg,
            fk_site_enrolling
     FROM   er_per,
            er_patprot x,
            er_study,
            er_patstudystat y
    WHERE       pk_per = x.fk_per
            AND x.patprot_stat = 1
            AND pk_study = x.fk_study
            AND pk_per = x.fk_per
            AND pk_study = y.fk_study
            AND pk_per = y.fk_per;


CREATE OR REPLACE SYNONYM ESCH.ERV_PATIENT_STUDY_STATUS FOR ERES.ERV_PATIENT_STUDY_STATUS;

CREATE OR REPLACE  SYNONYM EPAT.ERV_PATIENT_STUDY_STATUS FOR ERES.ERV_PATIENT_STUDY_STATUS;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERES.ERV_PATIENT_STUDY_STATUS TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERES.ERV_PATIENT_STUDY_STATUS TO ESCH;
