/* Formatted on 6/1/2011 2:16:01 PM (QP5 v5.115.810.9015) */
CREATE or replace VIEW ERES.ERV_STUDYPAT_ALLVISITS
(
   PATSTUDYSTAT_ID,
   PATSTUDYSTAT_DESC,
   FK_PER,
   PER_CODE,
   FK_STUDY,
   STUDY_NUMBER,
   FK_ACCOUNT,
   PATSTUDYSTAT_DATE,
   PATSTUDYSTAT_NOTE,
   PATSTUDYSTAT_REASON,
   PATPROT_ENROLDT,
   PK_PATPROT,
   PK_PATSTUDYSTAT,
   PER_SITE,
   PATSTUDYSTAT_SUBTYPE,
   EVENT_PROTOCOL_ID,
   SCH_EVENTS1_PK,
   EVENT_STATUS_ID,
   EVENT_START_DATE,
   EVENT_ID_ASSOC,
   EVENT_EXEON,
   EVENT_EXEBY,
   EVENT_ACTUAL_SCHDATE,
   VISIT,
   EVENT_STATUS_DESC,
   EVENT_STATUS_CODELST_SUBTYP,
   STUDY_TITLE,
   PATPROT_PATSTDID,
   FK_USERASSTO,
   PI,
   ASSIGNEDTO_NAME,
   PATPROT_PHYSICIAN,
   PHYSICIAN_NAME,
   FK_USER,
   ENROLLEDBY_NAME,
   PATPROT_TREATINGORG,
   TREATINGORG_NAME,
   FK_VISIT,STATUS_BROWSER_FLAG
)
AS
     SELECT   b.fk_codelst_stat status_id,                                 --1
              f.codelst_desc status_desc,                                  --2
              b.fk_per,
              d.per_code,                                                  --4
              b.fk_study,                                                  --5
              e.study_number,                                              --6
              e.fk_account,                                                --7
              b.patstudystat_date,                                         --8
              b.patstudystat_note,
              (select codelst_desc from er_codelst where pk_codelst = b.patstudystat_reason) patstudystat_reason,
              c.patprot_enroldt,                                           --9
              c.pk_patprot,                                               --10
              b.pk_patstudystat,                                          --11
              c.fk_site_enrolling,                                        --12
              f.codelst_subtyp,                                           --13
              c.fk_protocol,                                              --14
              s.event_id,
              s.isconfirmed,                                              --16
              s.start_date_time,                                          --17
              s.fk_assoc,                                                 --18
              s.event_exeon,                                              --19
              s.event_exeby,                                              --20
              s.actual_schdate,                                           --21
              s.visit,                                                    --22
              (SELECT   c.codelst_desc
                 FROM   sch_codelst c
                WHERE   pk_codelst = s.isconfirmed),                      --23
              (SELECT   c.codelst_subtyp
                 FROM   sch_codelst c
                WHERE   pk_codelst = s.isconfirmed),                      --24
              e.study_title,                                              --25
              c.patprot_patstdid,
              c.fk_userassto,
              (SELECT   usr_firstname || ' ' || usr_lastname
                 FROM   er_user
                WHERE   pk_user = e.STUDY_PRINV)
                 AS PI,
              (SELECT   usr_firstname || ' ' || usr_lastname
                 FROM   er_user
                WHERE   pk_user = c.fk_userassto)
                 AS assignedto_name,
              c.patprot_physician,
              (SELECT   usr_firstname || ' ' || usr_lastname
                 FROM   er_user
                WHERE   pk_user = c.patprot_physician)
                 AS physician,
              c.fk_user,
              (SELECT   usr_firstname || ' ' || usr_lastname
                 FROM   er_user
                WHERE   pk_user = c.fk_user)
                 AS enrolledby,
              c.patprot_treatingorg,
              (SELECT   site_name
                 FROM   er_site
                WHERE   pk_site = c.patprot_treatingorg)
                 AS treatingorg_name,
              fk_visit,decode(nvl(f.codelst_custom_col,''),'browser',1,0) as STATUS_BROWSER_FLAG 
       FROM   er_patstudystat b,
              er_patprot c,
              er_per d,
              er_study e,
              er_codelst f,
              sch_events1 s
      WHERE    c.patprot_stat = 1
              AND c.pk_patprot = s.fk_patprot
              AND b.fk_per = c.fk_per
              AND b.fk_study = c.fk_study
              AND b.pk_patstudystat = f_getLatestPatStudyStatPK(c.fk_study,c.fk_per)
              AND b.fk_per = d.pk_per
              AND b.fk_study = e.pk_study
              AND e.study_actualdt IS NOT NULL
              AND b.fk_codelst_stat = f.pk_codelst;



CREATE SYNONYM ESCH.ERV_STUDYPAT_ALLVISITs FOR ERV_STUDYPAT_ALLVISITS;


CREATE SYNONYM EPAT.ERV_STUDYPAT_ALLVISITs FOR ERV_STUDYPAT_ALLVISITS;


