
SELECT disease_site, total,
	(SELECT COUNT(*) FROM person a, pat_perid, ER_SITE b  WHERE a.fk_account = ~1 AND pk_person = fk_per AND pk_site = fk_site AND site_stat = 'Y' AND
	person_regdate BETWEEN '~2' AND  '~3' AND
	perid_id = disease_site_subtyp) AS new_reg
FROM (
SELECT disease_site, disease_site_subtyp, SUM(cnt) AS total FROM (
(SELECT disease_site, disease_site_subtyp, COUNT(DISTINCT fk_per || '*' ||fk_study) AS CNT FROM (
SELECT DECODE(INSTR(study_disease_site,','),0,(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = a.study_disease_site),'MULTIPLE') AS disease_site,
	   DECODE(INSTR(study_disease_site,','),0,(SELECT codelst_subtyp FROM ER_CODELST WHERE pk_codelst = a.study_disease_site),'MULTIPLE') AS disease_site_subtyp,
	    b.fk_per,b.fk_study
FROM ER_STUDY a,ER_PATSTUDYSTAT b,ER_STUDYID c
WHERE fk_account = ~1 AND
		pk_study = b.fk_study  AND pk_study = c.fk_study AND
		fk_codelst_idtype = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_subtyp = 'sum4_agent') AND studyId_id='Y'	AND
	    fk_codelst_stat = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'patStatus' AND codelst_subtyp = 'active') AND
		patstudystat_date BETWEEN '~2' AND  '~3'
) GROUP BY disease_site, disease_site_subtyp)
UNION
(SELECT codelst_desc AS disease_site,codelst_subtyp AS disease_site_subtyp,0 AS total FROM ER_CODELST WHERE  codelst_type ='disease_site')
) GROUP BY disease_site, disease_site_subtyp ORDER BY disease_site, disease_site_subtyp
)
