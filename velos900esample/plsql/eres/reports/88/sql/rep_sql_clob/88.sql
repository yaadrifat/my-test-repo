select  b.PAT_STUDYID , b.FK_STUDY, b.STUDY_NUMBER,b.SITE_NAME, b.STUDY_TITLE, to_char(b.STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,   b.PER_CODE,
ad.PK_ADVEVE, ad.AE_TYPE_DESC,to_char(ad.AE_STDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_STDATE,
to_char(ad.AE_ENDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_ENDDATE,  ad.AE_SEVERITY,  ad.AE_DESC,   ad.AE_RELATIONSHIP_DESC,
ad.AE_BDSYSTEM_AFF,  ad.AE_RECOVERY_DESC, 'O' REC_TYPE,    ev.info_desc OUTCOME, null ADVNOTIFY_DATE,  ad.AE_OUTNOTES,
(Select Decode(ADVINFO_VALUE,0,'No',1,'Yes') from ESCH.EV_ADV_CHKINFO
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = 'A' and trim(codelst_subtyp) = 'adv_unexp')  unexpected ,
(Select  Decode(ADVINFO_VALUE,0,'No',1,'Yes') from ESCH.EV_ADV_CHKINFO
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = 'A' and trim(codelst_subtyp) = 'adv_dopped') adv_dropped,
(Select  Decode(ADVINFO_VALUE,0,'No',1,'Yes') from ESCH.EV_ADV_CHKINFO
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = 'A' and trim(codelst_subtyp) = 'adv_violation') adv_violation ,
(select count(*) from ESCH.sch_Adverseve e
where e.fk_study in (:studyId)  and e.fk_per in (:patientId) and trunc(ad.AE_STDATE) between TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)) adv_cnt,
(ad.AE_ADV_GRADE) adv_grade
from  ESCH.ev_adv_type ad, erv_patstudy b, ESCH.EV_ADV_CHKINFO_CHECKED ev
where  ad.fk_study = b.fk_study and   ad.fk_per =  b.fk_per and  ad.PK_ADVEVE = ev.fk_adverse(+) and
ad.fk_study in (:studyId) and
b.fk_per in (:patientId) and  trunc(ad.AE_STDATE) between TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
UNION
select b.PAT_STUDYID, b.FK_STUDY, b.STUDY_NUMBER, b.SITE_NAME, b.STUDY_TITLE, to_char(b.STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,
b.PER_CODE,   ad.PK_ADVEVE, ad.AE_TYPE_DESC,  to_char(ad.AE_STDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_STDATE,
to_char(ad.AE_ENDDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AE_ENDDATE, ad.AE_SEVERITY,  ad.AE_DESC,    ad.AE_RELATIONSHIP_DESC,
ad.AE_BDSYSTEM_AFF,    ad.AE_RECOVERY_DESC,  'N' REC_TYPE,  adnot.ADV_NOT_SENT_TO, to_char(adnot.ADVNOTIFY_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) ADVNOTIFY_DATE, ad.AE_OUTNOTES,
(Select Decode(ADVINFO_VALUE,0,'No',1,'Yes') from ESCH.EV_ADV_CHKINFO
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = 'A' and trim(codelst_subtyp) = 'adv_unexp') unexpected ,
(Select  Decode(ADVINFO_VALUE,0,'No',1,'Yes') from ESCH.EV_ADV_CHKINFO
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = 'A' and trim(codelst_subtyp) = 'adv_dopped') adv_dropped,
(Select  Decode(ADVINFO_VALUE,0,'No',1,'Yes') from ESCH.EV_ADV_CHKINFO
Where fk_adverse = ad.pk_adveve and trim(ADVINFO_TYPE) = 'A' and trim(codelst_subtyp) = 'adv_violation') adv_violation ,
(select count(*) from ESCH.sch_Adverseve e
where e.fk_study in (:studyId)  and e.fk_per in (:patientId)      and trunc(e.AE_STDATE) between TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT)and TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)) adv_cnt,
(ad.AE_ADV_GRADE) adv_grade
from ESCH. ev_adv_type ad, erv_patstudy b, ERV_SCH_ADVNOTIY_CHECKED adnot
where ad.fk_study = b.fk_study and ad.fk_per =  b.fk_per and
ad.pk_adveve = adnot.pk_adveve(+) and
ad.fk_study in (:studyId) and  b.fk_per in (:patientId) and
trunc(ad.AE_STDATE) between TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)