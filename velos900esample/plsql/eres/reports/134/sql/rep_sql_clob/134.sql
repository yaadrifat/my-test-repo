SELECT (SELECT study_number
                    FROM er_study
                   WHERE pk_study = ad.fk_study) "STUDY_NUMBER",
                 (SELECT PATPROT_PATSTDID from er_patprot
                    WHERE fk_per = ad.fk_per and fk_study=ad.fk_study and PATPROT_STAT=1 ) "PAT_STUDY_ID",
                 trim(to_char(ad.ae_stdate,PKG_DATEUTIL.F_GET_DATEFORMAT)) AS ae_date, ad.ae_desc AS ae_desc, column_name,
                 CASE
                    WHEN (column_name = 'AE_OUTTYPE')
                       THEN pkg_util.f_decipher_codelist
                                             (ac.old_value,
                                              'outcome',
                                              'esch.sch_codelst'
                                             )
                    WHEN (column_name = 'AE_ADDINFO')
                       THEN pkg_util.f_decipher_codelist (ac.old_value,
                                                          'adve_info',
                                                          'esch.sch_codelst'
                                                         )
                    WHEN (column_name = 'AE_REPORTEDBY')
                       THEN (SELECT usr_lastname || ', ' || usr_firstname
                               FROM er_user
                              WHERE pk_user = old_value
                                       )
                    WHEN (column_name = 'AE_ENTERBY')
                       THEN (SELECT usr_lastname || ', ' || usr_firstname
                               FROM er_user
                              WHERE pk_user = old_value
                                       ) WHEN (column_name = 'AE_RELATIONSHIP')
                       THEN (SELECT codelst_desc
                               FROM esch.sch_codelst
                              WHERE pk_codelst = ac.old_value)
                    WHEN (column_name='AE_RECOVERY_DESC')
                       THEN ( SELECT codelst_desc
                               FROM esch.sch_codelst
                              WHERE pk_codelst = ac.old_value
                       )
                    WHEN (column_name='AE_LOGGEDDATE')
                       THEN old_value
                    WHEN (column_name='AE_DISCVRY')
                       THEN old_value
                    ELSE old_value
                 END AS old_value,
                 CASE
                    WHEN (column_name = 'AE_OUTTYPE')
                       THEN pkg_util.f_decipher_codelist
                                             (ac.new_value,
                                              'outcome',
                                              'esch.sch_codelst'
                                             )
                    WHEN (column_name = 'AE_ADDINFO')
                       THEN pkg_util.f_decipher_codelist (ac.new_value,
                                                          'adve_info',
                                                          'esch.sch_codelst '
                                                         )
                    WHEN (column_name = 'AE_REPORTEDBY')
                       THEN (SELECT usr_lastname || ', ' || usr_firstname
                               FROM er_user
                              WHERE pk_user = new_value
                                       )
                     WHEN (column_name = 'AE_ENTERBY')
                       THEN (SELECT usr_lastname || ', ' || usr_firstname
                               FROM er_user
                              WHERE pk_user = new_value
                                       )
                    WHEN (column_name = 'AE_RELATIONSHIP')
                       THEN (SELECT codelst_desc
                               FROM esch.sch_codelst
                              WHERE pk_codelst = ac.new_value)
                     WHEN (column_name='AE_RECOVERY_DESC')
                       THEN ( SELECT codelst_desc
                               FROM esch.sch_codelst
                              WHERE pk_codelst = ac.new_value
                       )
                       WHEN (column_name='AE_LOGGEDDATE')
                       THEN new_value
                       WHEN (column_name='AE_DISCVRY')
                       THEN new_value
                    ELSE new_value
                 END AS new_value,
                 Decode(pkg_util.f_to_number(user_name), 0, user_name,
                (SELECT usr_lastname  || ', ' || usr_firstname FROM ER_USER 	WHERE pk_user = user_name)
               ) AS "USER",
                 trim(to_char(ar.TIMESTAMP,PKG_DATEUTIL.F_GET_DATEFORMAT)) as TIMESTAMP
    FROM sch_adverseve ad, esch.audit_row ar, esch.audit_column ac
   WHERE ad.rid = ar.rid
     AND ar.action = 'U'
     AND (ar.TIMESTAMP BETWEEN TO_DATE (':fromDate', PKG_DATEUTIL.F_GET_DATEFORMAT)
                           AND TO_DATE (':toDate', PKG_DATEUTIL.F_GET_DATEFORMAT)
         )
     AND ac.raid = ar.raid
     AND ad.fk_study IN (:studyId)
     AND ad.fk_per IN (:patientId)
     AND column_name NOT IN ('LAST_MODIFIED_BY',
'LAST_MODIFIED_DATE','IP_ADD','FK_ACCOUNT')
ORDER BY ac.raid ASC
