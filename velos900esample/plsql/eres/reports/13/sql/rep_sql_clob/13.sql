select
   STUDY_NUMBER           ,
   STUDY_TITLE            ,
    TO_CHAR(STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) STUDY_ACTUALDT,
    SITE_NAME,
    PROTOCOLID   ,
   PROTOCOL_NAME          ,
   (SELECT usr_firstname || ' ' || usr_lastname FROM ER_USER WHERE FK_USERASSTO = ER_USER.pk_user) AS FK_USERASSTO,
   PERSON_CODE            ,
   PAT_STUDYID,
   PERSON_NAME            ,
   EVENT_NAME             ,
   TO_CHAR(PATPROT_ENROLDT,PKG_DATEUTIL.F_GET_DATEFORMAT) ENROLDT ,
   nvl(MONTH,'No Interval Defined ') MONTH                  ,
   TO_CHAR(EVENT_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT)    st     ,
   EVENT_STATUS_DESC AS EVENT_STATUS, VISIT_name AS visit,
   TO_CHAR(EVENT_EXEON ,PKG_DATEUTIL.F_GET_DATEFORMAT) exe_dt,notes
   FROM   erv_patsch
  WHERE  fk_study IN (:studyId)  AND
         fk_site IN (:orgId) AND
         fk_per IN (:patientId)
                 AND protocolId IN (:protCalId)
         AND fk_codelst_stat IN (:patStatusId)
         AND (fk_userassto IN (:userId) OR fk_userassto IS NULL) AND
NVL(EVENT_SCHDATE, TO_DATE('01/01/1900',PKG_DATEUTIL.F_GET_DATEFORMAT)) BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
AND patprot_stat = 1 ORDER BY sort_date,EVENT_SCHDATE,study_number,site_name,person_code
