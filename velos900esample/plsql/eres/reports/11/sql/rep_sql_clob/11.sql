SELECT   v.fk_per,
         v.fk_study,
    v.PER_CODE,
    v.STUDY_NUMBER,
    (SELECT site_name FROM ER_SITE WHERE pk_site = v.PER_SITE) AS site,
    (SELECT patprot_patstdid FROM ER_PATPROT WHERE v.pk_patprot= pk_patprot) AS patstudy_id,
        TO_CHAR(ev.EVENTSTAT_DT,PKG_DATEUTIL.F_GET_DATEFORMAT) EVENTSTAT_DT,
    NVL((SELECT usr_firstname || usr_lastname FROM ER_USER WHERE pk_user = s.EVENT_EXEBY) , (SELECT usr_firstname || usr_lastname FROM ER_USER WHERE pk_user = s.creator))AS entered_by,
    TO_CHAR(s.ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) ACTUAL_SCHDATE,
    (select visit_name from sch_protocol_visit where pk_protocol_visit = s.FK_VISIT) visit,
    c.CODELST_DESC,
    s.DESCRIPTION,
       b.COST AS event_cost,
    b.EVENT_RES,
    ev.created_on,ev.last_modified_date
FROM    ERV_ALLPATSTUDYSTAT v, SCH_EVENTS1 s, SCH_CODELST c, sch_eventstat ev, EVENT_ASSOC b
WHERE  v.PK_PATPROT = s.fk_patprot AND
s.EVENT_ID = ev.FK_EVENT AND
s.FK_ASSOC = b.EVENT_ID AND
c.pk_codelst = ev.EVENTSTAT AND
c.codelst_type = 'eventstatus'
AND
v.fk_per IN (:patientId)  AND
v.fk_study IN (:studyId)  AND
v.per_site IN (:orgId) AND
v.fk_protocol IN (:protCalId) AND
NVL(s.ACTUAL_SCHDATE,TO_DATE('01/01/1900',PKG_DATEUTIL.F_GET_DATEFORMAT)) BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT)  AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
 ORDER BY v.PER_CODE, s.ACTUAL_SCHDATE, s.DESCRIPTION, ev.EVENTSTAT_DT