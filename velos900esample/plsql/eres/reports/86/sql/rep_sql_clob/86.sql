select f.person_code as patientId,
b.CODELST_DESC as advType,
c.STUDY_NUMBER as studyNumber,
c.STUDY_TITLE as studyTitle,
to_char(c. STUDY_ACTUALDT,PKG_DATEUTIL.F_GET_DATEFORMAT) study_start_dt,
g.site_name site_name,
a.AE_DESC as advDesc,
to_char(a.AE_STDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) as advSdate,
to_char(a.AE_OUTDATE,PKG_DATEUTIL.F_GET_DATEFORMAT) as advOutdate,
(select codelst_desc from ESCH.sch_codelst where pk_codelst = a.AE_RELATIONSHIP) as advRelation,
(select distinct patprot_patstdid from er_patprot where fk_study=c.pk_study and fk_per = a.fk_per) as pat_studyid
from ESCH.sch_adverseve a,
ESCH.sch_codelst b,
er_study c,
ESCH.sch_advInfo d,
ESCH.sch_codelst e,
EPAT.person f,
er_site g
where  a.FK_CODLST_AETYPE=b.pk_codelst
and d.FK_CODELST_INFO=e.pk_codelst
and a.FK_PER=f.pk_person
and d.FK_ADVERSE = a.PK_ADVEVE
and a.FK_STUDY = c.pk_study
and e.CODELST_SUBTYP = 'al_death'
and d.ADVINFO_VALUE = 1
and f.FK_SITE in (:orgId)
and c.PK_STUDY in (:studyId)
and a.AE_OUTDATE between TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) and TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
and g.pk_site in (:orgId)