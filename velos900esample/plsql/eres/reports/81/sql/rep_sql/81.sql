select std.study_number study_number, std.study_title study_title, to_char(std.study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt,
st.site_name site_name,st.pk_site,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and b.adv_type ='al_adve' and
AE_STDATE between '~2'and '~3'),0) ADV_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and b.adv_type = 'al_sadve' and
AE_STDATE between '~2'and '~3'),0) SADV_COUNT,
nvl((select count(PK_ADVEVE)
from ESCH.EV_ADV_SITE_INFO b
where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
b.ADVINFO_TYPE   = 'O' and
b.CODELST_SUBTYP = 'al_death' and
b.ADVINFO_VALUE = 1 and
b.AE_STDATE between '~2'and '~3'
),0) DEATH_COUNT,
nvl((Select count(PK_ADVEVE)
from ESCH.EV_ADV_SITE_INFO b
where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
CODELST_SUBTYP = 'adv_unexp'and
ADVINFO_TYPE   = 'A' and
ADVINFO_VALUE = 1 and
b.AE_STDATE between '~2'and '~3'),0) UNEXP_COUNT,
nvl((Select count(PK_ADVEVE)
from ESCH.EV_ADV_SITE_INFO b
where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
CODELST_SUBTYP = 'adv_violation'and
ADVINFO_TYPE   = 'A' and
ADVINFO_VALUE = 1 and
b.AE_STDATE between '~2'and '~3'),0) VIO_COUNT,
nvl((Select count(PK_ADVEVE)
from ESCH.EV_ADV_SITE_INFO b
where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
CODELST_SUBTYP = 'adv_dopped'and
ADVINFO_TYPE   = 'A' and
ADVINFO_VALUE = 1 and
b.AE_STDATE between '~2'and '~3' ),0) DROP_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
b.REL_TYPE = 'ad_def' and
AE_STDATE between '~2' and '~3' ),0) R_DEF_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
b.REL_TYPE = 'ad_nr' and
AE_STDATE between '~2'and '~3' ),0) R_NR_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
b.REL_TYPE = 'ad_pos' and
AE_STDATE between '~2' and '~3' ),0)
R_POS_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
b.REL_TYPE = 'ad_prob' and
AE_STDATE between '~2'and '~3' ),0)
R_PROB_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
b.REL_TYPE = 'ad_unknown' and
AE_STDATE between '~2'and '~3'),0) R_UNK_COUNT,
nvl((Select count(PK_ADVEVE) from ESCH.EV_ADV_TYPE b
Where st.pk_site = b.pk_site and b.fk_study = std.pk_study and
b.REL_TYPE = 'ad_unlike' and
AE_STDATE between '~2'and '~3' ),0) R_UNL_COUNT
from er_site st, er_study std
where pk_study = ~1 and
pk_site in (Select distinct er_user.FK_SITEID
from er_studyteam team, er_user
Where team.fk_study = ~1 and
er_user.pk_user = team.fk_user and
nvl(study_team_usr_type,'D') = 'D' )