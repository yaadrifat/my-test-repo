SELECT
form_name ,
formsec_name AS section_name,
formfld_seq AS SEQUENCE,
fld_name AS field_name,
fld_uniqueid AS field_id,
fld_desc  AS field_description,
fld_instructions  AS field_instructions,
DECODE(fld_datatype,'ED','DATE','ET','Text','EN','NUMBER','MD','DROP down','MC','Checkbox','MR','Radio Button','ML','Lookup',DECODE(fld_type,'C','Comments','H','Line break','S','SPACE break')) AS field_datatype,
DECODE(formfld_mandatory,1,'Yes',0,'NO') AS mandatory,
DECODE(fld_override_mandatory,1,'Yes') AS override_mandatory,
DECODE(fld_isreadonly,1,'READ ONLY',DECODE(fld_isvisible,0,'Visible',1,'Hide',2,'DISABLE')) AS display_attributes,
fld_length  AS field_length,
fld_charsno  AS max_chars,
fld_defresp AS default_response,
fld_format  AS number_format,
DECODE(fld_override_format,1,'Yes') AS override_number_format,
DECODE(fld_todaycheck,1,'Yes') AS date_validation,
DECODE(fld_override_date,1,'Yes') AS override_date_validation,
(SELECT fldvalidate_op1 || ' ' || fldvalidate_val1 || ' ' || fldvalidate_logop1 || ' ' || fldvalidate_op2 || ' ' || fldvalidate_val2 FROM ER_FLDVALIDATE WHERE fk_fldlib = a.fk_field) AS number_range_validation,
DECODE(fld_override_range,1,'Yes') AS override_range_validation,
fldresp_seq,fldresp_dispval,fldresp_dataval,fldresp_score
FROM ER_FORMFLD a,ER_FORMSEC b,ER_FORMLIB c,ER_FLDRESP d, ER_FLDLIB e
WHERE pk_formsec = fk_formsec  AND pk_formlib = fk_formlib AND pk_field = a.fk_field AND pk_field = d.fk_field(+) AND e.fld_type!='C' and
pK_FORMLIB = ~2 AND c.fk_account = ~1 and a.record_type<> 'D' and e.record_type <> 'D'
ORDER BY section_name,SEQUENCE,fldresp_seq
