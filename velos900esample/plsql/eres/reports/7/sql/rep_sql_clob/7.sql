select  distinct c.USR_LASTNAME,
  c.USR_FIRSTNAME, ER_STUDY.pk_study,
  ER_STUDY.STUDY_TITLE, TO_CHAR(study_actualdt,PKG_DATEUTIL.F_GET_DATEFORMAT) study_actualdt ,
  ER_STUDY.STUDY_NUMBER,   a.CODELST_DESC tarea,
  b.CODELST_DESC team_role, f.codelst_desc study_stat,
 d.site_name, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) AS division,
 (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ER_STUDY.fk_codelst_phase) AS study_Phase,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ER_STUDY.fk_codelst_restype) AS Research_Type,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = ER_STUDY.fk_codelst_type) AS Study_Type
 FROM  ER_STUDYTEAM,   ER_USER c,
  ER_STUDY,   ER_CODELST a,
  ER_CODELST b, ER_CODELST f,
  ER_SITE d ,ER_STUDYSTAT e
 WHERE  c.fk_siteid IN (:orgId)
 AND  TRUNC(ER_STUDY.created_on) BETWEEN TO_DATE(':fromDate',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(':toDate',PKG_DATEUTIL.F_GET_DATEFORMAT)
 AND  er_studyteam.fk_user in (:userId) and ER_STUDYTEAM.FK_USER = c.pk_user
 AND c.fk_siteid = d.pk_site
 AND  ER_STUDY.pk_study = ER_STUDYTEAM.FK_STUDY
 AND  ER_STUDY.FK_CODELST_TAREA = a.pk_codelst
 AND  ER_STUDYTEAM.FK_CODELST_TMROLE = b.pk_codelst
AND e.fk_study = ER_STUDY.pk_study
AND e.FK_CODELST_STUDYSTAT = f.pk_codelst
AND f.codelst_type = 'studystat' AND e.STUDYSTAT_ENDT IS NULL
 AND study_verparent IS NULL
 AND fk_codelst_tarea IN (:tAreaId)
 AND pk_study IN (:studyId)
 AND (study_division IN (:studyDivId) OR study_division IS NULL)
     AND EXISTS  (SELECT fk_study FROM ER_STUDYSITES WHERE fk_study=pk_study AND fk_site IN (:orgId) )
ORDER BY tarea, pk_study  
