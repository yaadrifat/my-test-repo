CREATE OR REPLACE TRIGGER PAT_PERID_AU0
AFTER UPDATE OF RID,FK_PER,IP_ADD,CREATOR,PERID_ID,PK_PERID,CREATED_ON,FK_CODELST_IDTYPE
ON PAT_PERID REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
   usr VARCHAR2(100);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'PAT_PERID', :OLD.rid, 'U', usr);

  IF NVL(:OLD.pk_perid,0) !=
     NVL(:NEW.pk_perid,0) THEN
     audit_trail.column_update
       (raid, 'PK_PERID',
       :OLD.pk_perid, :NEW.pk_perid);
  END IF;
  IF NVL(:OLD.fk_per,0) !=
     NVL(:NEW.fk_per,0) THEN
     audit_trail.column_update
       (raid, 'FK_PER',
       :OLD.fk_per, :NEW.fk_per);
  END IF;
  IF NVL(:OLD.fk_codelst_idtype,0) !=
     NVL(:NEW.fk_codelst_idtype,0) THEN
     SELECT trim(codelst_desc) INTO old_codetype
       FROM er_codelst WHERE pk_codelst =  :OLD.FK_CODELST_IDTYPE ;
     SELECT trim(codelst_desc) INTO new_codetype
       FROM er_codelst WHERE pk_codelst =  :NEW.FK_CODELST_IDTYPE ;
     audit_trail.column_update
       (raid, 'FK_CODELST_IDTYPE',old_codetype, new_codetype);
  END IF;
  IF NVL(:OLD.perid_id,' ') !=
     NVL(:NEW.perid_id,' ') THEN
     audit_trail.column_update
       (raid, 'PERID_ID',
       :OLD.perid_id, :NEW.perid_id);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
   BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL ;
     END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL ;
     END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;

  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
            to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;

END;
/


