CREATE OR REPLACE TRIGGER "PAT_PERAPNDX_BI0"
BEFORE INSERT
ON PAT_PERAPNDX REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(100);
  insert_data CLOB;


 BEGIN
BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;

 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

  SELECT TRUNC(seq_rid.NEXTVAL) INTO erid  FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction    (raid, 'PAT_PERAPNDX', erid, 'I', USR );
-- Added by Ganapathy on 23/06/05 for Audit Insert
   insert_data:=:NEW.pk_perapndx||'|'|| :NEW.fk_per||'|'||TO_CHAR(:NEW.perapndx_date,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
    :NEW.perapndx_desc||'|'|| :NEW.perapndx_uri||'|'|| :NEW.perapndx_type||'|'|| :NEW.rid||'|'||
 :NEW.creator||'|'|| :NEW.last_modified_by||'|'|| TO_CHAR(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
  TO_CHAR(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| :NEW.ip_add;

 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
 END ;
/


