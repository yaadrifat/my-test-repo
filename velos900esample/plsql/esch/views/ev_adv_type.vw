/* Formatted on 2/9/2010 1:41:14 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW EV_ADV_TYPE
(
   PK_ADVEVE,
   FK_CODLST_AETYPE,
   AE_TYPE_DESC,
   AE_DESC,
   AE_STDATE,
   AE_ENDDATE,
   FK_STUDY,
   FK_PER,
   AE_OUTNOTES,
   AE_NOTES,
   AE_ADV_GRADE,
   AE_SEVERITY,
   AE_RELATIONSHIP,
   AE_RELATIONSHIP_DESC,
   AE_BDSYSTEM_AFF,
   PK_SITE,
   SITE_NAME,
   ADV_TYPE,
   REL_TYPE,
   AE_RECOVERY_DESC,
   FK_ACCOUNT
)
AS
   SELECT   ad.PK_ADVEVE,
            ad.FK_CODLST_AETYPE,
            cd.codelst_desc,
            ad.AE_DESC,
            ad.AE_STDATE,
            ad.AE_ENDDATE,
            ad.FK_STUDY,
            ad.FK_PER,
            ad.AE_OUTNOTES,
            ad.AE_NOTES,
            ('Grade: ' || ad.AE_GRADE || ' ' || ad.AE_NAME) AE_ADV_GRADE,
            (SELECT   codelst_desc
               FROM   sch_codelst
              WHERE   codelst_type = 'adve_severity'
                      AND pk_codelst = ad.AE_SEVERITY)
               AE_SEVERITY,
            ad.AE_RELATIONSHIP,
            rel.codelst_desc,
            (SELECT   codelst_desc
               FROM   sch_codelst
              WHERE   codelst_type = 'adve_bdsystem'
                      AND pk_codelst = ad.AE_BDSYSTEM_AFF)
               AE_BDSYSTEM_AFF,
            st.pk_site,
            st.site_name,
            cd.codelst_subtyp adv_type,
            rel.codelst_subtyp rel_type,
            (SELECT   codelst_desc
               FROM   sch_codelst
              WHERE   codelst_type = 'adve_recovery'
                      AND pk_codelst = ad.AE_RECOVERY_DESC)
               AE_RECOVERY_DESC,
            per.fk_account
     FROM   sch_adverseve ad,
            ER_SITE st,
            ER_PER per,
            sch_codelst cd,
            sch_codelst rel
    WHERE       per.pk_per = ad.fk_per
            AND st.pk_site = per.fk_site
            AND cd.pk_codelst = ad.FK_CODLST_AETYPE
            AND ad.AE_RELATIONSHIP = rel.pk_codelst(+);


CREATE SYNONYM ERES.EV_ADV_TYPE FOR EV_ADV_TYPE;


CREATE SYNONYM EPAT.EV_ADV_TYPE FOR EV_ADV_TYPE;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON EV_ADV_TYPE TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON EV_ADV_TYPE TO ERES;

