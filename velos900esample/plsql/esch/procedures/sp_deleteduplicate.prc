CREATE OR REPLACE PROCEDURE        "SP_DELETEDUPLICATE" (
p_protocol_id IN NUMBER
)
AS
/*
This procedure deletes the duplicate events, and the deselected events
*/
BEGIN
 /*
 Set the flags of the old events which have again been inserted during the edit ptotocol process
 */
 UPDATE EVENT_DEF
    SET EVENT_FLAG = 0
  WHERE event_id IN (
        SELECT de.evt FROM
         (SELECT   COST  , DISPLACEMENT  , COUNT(*) , MIN(event_id) evt,fk_visit
            FROM EVENT_DEF
           WHERE chain_id = p_protocol_id
             AND event_type = 'A'
          HAVING COUNT(*) > 1
           GROUP BY COST   , DISPLACEMENT,fk_visit,event_id
         ) de
                  )
 ;
 /*
 This will delete the new events which already existed in the protocol before the edit
 protocol process was started.
 */
 DELETE FROM EVENT_DEF
  WHERE event_id IN
      (
        SELECT de.evt
	     FROM (SELECT   COST  , DISPLACEMENT  , COUNT(*) , MAX(event_id) evt,fk_visit
                  FROM EVENT_DEF
                 WHERE chain_id = p_protocol_id
                   AND event_type = 'A'
                HAVING COUNT(*) > 1
                 GROUP BY COST   , DISPLACEMENT,fk_visit,event_id
               ) de
       )
 ;
/*
 This will DELETE THE OLD EVENTS which have been deselected during THE edit protocol process
 We will know about these EVENTS BY THE event flag, which was SET TO 1 INITIALLY AND THEN THE
 AFTER ALL THE EVENTS FROM THE edit screen are inserted. FROM THE dulicate EVENTS THE OLD EVENTS
 get THE flag SET AS 0. Similary THE NEW EVENTS which FOR which an OLD RECORD IS FOUND are deleted
 Lastly THE EVENTS which had been deselected, AND still have their flags SET TO 1 are deleted.
 */
 DELETE
   FROM EVENT_DEF
  WHERE EVENT_FLAG = 1
    AND chain_id= p_protocol_id
    AND event_type='A'
    AND displacement <> 0;
 COMMIT;
END;
/


CREATE SYNONYM ERES.SP_DELETEDUPLICATE FOR SP_DELETEDUPLICATE;


CREATE SYNONYM EPAT.SP_DELETEDUPLICATE FOR SP_DELETEDUPLICATE;


GRANT EXECUTE, DEBUG ON SP_DELETEDUPLICATE TO EPAT;

GRANT EXECUTE, DEBUG ON SP_DELETEDUPLICATE TO ERES;

