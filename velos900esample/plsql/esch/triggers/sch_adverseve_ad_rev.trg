CREATE OR REPLACE TRIGGER "SCH_ADVERSEVE_AD_REV" 
AFTER DELETE
ON SCH_ADVERSEVE
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE

/* **********************************
   **
   ** Author: Sonia Abrol 08/09/2007
   ** for reverse A2A
   ** Insert a record in er_rev_pendingdata for adverse event deletion
   *************************************
*/

v_key NUMBER;
v_pk NUMBER;
v_tabname VARCHAR2(50);
v_account NUMBER;
v_site_code VARCHAR2(255);
v_sponsor_study NUMBER;
v_err VARCHAR2(4000);
v_module_name VARCHAR2(20);
v_sponsor_account NUMBER;

v_count NUMBER;
BEGIN

 -- get account information and site code

  PKG_IMPEX_REVERSE.SP_GET_STUDYSPONSORINFO(:OLD.FK_STUDY, v_sponsor_study , v_site_code, v_sponsor_account, v_err);

  IF LENGTH(trim(v_site_code)) >  0 THEN

  IF v_sponsor_study > 0 THEN

  --insert data for patient status

   v_tabname := 'sch_adverseve';
   v_module_name := 'pat_adverse';

   v_pk := :OLD.PK_ADVEVE ;

   SELECT SEQ_ER_REV_PENDINGDATA.NEXTVAL
   INTO v_key FROM dual;

   INSERT INTO ER_REV_PENDINGDATA (pk_rp, rp_tablename, rp_tablepk, RP_SITECODE,FK_STUDY,RP_MODULE,RP_RECORD_TYPE)
   VALUES (v_key ,v_tabname,v_pk,v_site_code,:OLD.FK_STUDY,v_module_name,'D');

   END IF; -- for if v_sponsor_study > 0
 END IF; -- for v_site_code


END;
/


