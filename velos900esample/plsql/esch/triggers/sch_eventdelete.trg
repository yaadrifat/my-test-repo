CREATE OR REPLACE TRIGGER "SCH_EVENTDELETE"
AFTER DELETE
ON EVENT_DEF
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare

status  NUMBER(1);

BEGIN



--modified by Sonika Talwar on June 10, 03

delete from sch_eventcost where fk_event = :old.event_id;

delete from sch_eventusr where fk_event = :old.event_id;



--delete documents

for i in (select pk_docs

          from sch_eventdoc

	     where fk_event = :old.event_id)

loop



  delete from sch_eventdoc

  where pk_docs = i.pk_docs ;



   delete sch_docs

   where sch_docs.pk_docs = i.pk_docs;



end loop;



--delete crf

  delete from sch_crflib

  where fk_events = :old.event_id;

  -- KM-#3491
  delete from sch_event_crf where fk_event = :old.event_id;

  delete from sch_protocol_visit where fk_protocol = :old.event_id;

END;
/


