CREATE OR REPLACE TRIGGER "SCH_CRFSTAT_BI0"
BEFORE INSERT
ON SCH_CRFSTAT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(70);
  v_new_creator NUMBER;
  insert_data CLOB;
BEGIN
 BEGIN
   v_new_creator := :NEW.creator;
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' ||usr_firstname
   INTO usr FROM er_user WHERE pk_user = v_new_creator;
  EXCEPTION WHEN NO_DATA_FOUND THEN
  USR := 'New User' ;
 END ;
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_CRFSTAT', erid, 'I', USR );



      --   Added by Ganapathy on 06/23/05 for Audit insert
  insert_data:= :NEW.PK_CRFSTAT||'|'|| :NEW.FK_CRF||'|'|| :NEW.FK_CODELST_CRFSTAT||'|'||
        :NEW.CRFSTAT_ENTERBY||'|'|| :NEW.CRFSTAT_REVIEWBY||'|'||
        TO_CHAR(:NEW.CRFSTAT_REVIEWON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.CRFSTAT_SENTTO||'|'||
      :NEW.CRFSTAT_SENTFLAG||'|'|| :NEW.CRFSTAT_SENTBY||'|'||
      TO_CHAR(:NEW.CRFSTAT_SENTON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| :NEW.RID||'|'||
      :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
      TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD;

     INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/


