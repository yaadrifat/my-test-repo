CREATE OR REPLACE TRIGGER "SCH_PROTSTAT_AD0" 
AFTER DELETE
ON SCH_PROTSTAT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_PROTSTAT', :old.rid, 'D');

  deleted_data :=
   to_char(:old.pk_protstat) || '|' ||
   to_char(:old.fk_event) || '|' ||
   --:old.protstat || '|' ||
   to_char(:old.protstat_dt) || '|' ||
   to_char(:old.protstat_by) || '|' ||
   :old.protstat_note || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   to_char(:old.created_on) || '|' ||
   :old.ip_add|| '|' ||
   to_char(:old.FK_CODELST_CALSTAT) ;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


