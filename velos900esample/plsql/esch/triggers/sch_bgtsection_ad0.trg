CREATE OR REPLACE TRIGGER "SCH_BGTSECTION_AD0" 
AFTER DELETE
ON SCH_BGTSECTION
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  deleted_data VARCHAR2(4000);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_BGTSECTION', :OLD.rid, 'D');

  deleted_data :=
  TO_CHAR(:OLD.pk_budgetsec) || '|' ||
  TO_CHAR(:OLD.fk_bgtcal) || '|' ||
  :OLD.bgtsection_name || '|' ||
  :OLD.bgtsection_visit || '|' ||
  :OLD.bgtsection_delflag || '|' ||
  TO_CHAR(:OLD.bgtsection_sequence) || '|' ||
  TO_CHAR(:OLD.bgtsection_patno) || '|' ||
  TO_CHAR(:OLD.bgtsection_personlflag) || '|' ||
  :OLD.bgtsection_type || '|' ||
  TO_CHAR(:OLD.rid) || '|' ||
  TO_CHAR(:OLD.creator) || '|' ||
  TO_CHAR(:OLD.last_modified_by) || '|' ||
  TO_CHAR(:OLD.last_modified_date) || '|' ||
  TO_CHAR(:OLD.created_on) || '|' ||
  :OLD.ip_add || '|' || TO_CHAR(:OLD.FK_VISIT);



INSERT INTO AUDIT_DELETE
(raid, row_data) VALUES (raid, deleted_data);
END;
/


