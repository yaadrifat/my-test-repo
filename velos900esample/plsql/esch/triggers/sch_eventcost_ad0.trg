CREATE OR REPLACE TRIGGER "SCH_EVENTCOST_AD0" 
AFTER DELETE
ON SCH_EVENTCOST
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_EVENTCOST', :old.rid, 'D');

  deleted_data :=
   to_char(:old.pk_eventcost) || '|' ||
   to_char(:old.eventcost_value) || '|' ||
   to_char(:old.fk_event) || '|' ||
   to_char(:old.fk_cost_desc) || '|' ||
   to_char(:old.fk_currency) || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   to_char(:old.created_on) || '|' ||
   :old.ip_add;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


