CREATE OR REPLACE TRIGGER "SCH_ALERTNOTIFY_AD0" 
AFTER DELETE
ON SCH_ALERTNOTIFY
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_ALERTNOTIFY', :old.rid, 'D');

  deleted_data :=
   to_char(:old.pk_alnot) || '|' ||
   to_char(:old.fk_patprot) || '|' ||
   to_char(:old.fk_codelst_an) || '|' ||
   :old.alnot_type || '|' ||
   :old.alnot_users || '|' ||
   :old.alnot_mobeve || '|' ||
   to_char(:old.alnot_flag) || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   to_char(:old.created_on) || '|' ||
   :old.ip_add || '|' ||
   to_char(:old.fk_study) || '|' ||
   to_char(:old.fk_protocol) || '|' ||
   :old.alnot_globalflag ;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/


