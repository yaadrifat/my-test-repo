SET DEFINE OFF;

DECLARE 
  table_count NUMBER; 
BEGIN 
  select count(*) into table_count from all_tables where 
  upper(owner) = 'ERES' and table_name = 'ER_LKPFILTER';

  if (table_count < 1) then 
    execute immediate
'CREATE TABLE ERES.ER_LKPFILTER '||
'( '||
'  PK_LKPFILTER           NUMBER                 NOT NULL, '||
'  LKPFILTER_KEY          VARCHAR2(256)          NOT NULL, '||
'  LKPFILTER_SQL          CLOB, '||
'  LKPFILTER_PARAM_TYPE   VARCHAR2(2000), '||
'  LKPFILTER_COMMENT         VARCHAR2(2000) '||
') '||
'LOGGING  '||
'NOCOMPRESS  '||
'NOCACHE '||
'NOPARALLEL '||
'NOMONITORING';

    execute immediate
'ALTER TABLE ERES.ER_LKPFILTER ADD ( '||
'  CONSTRAINT ER_LKPFILTER_PK '||
' PRIMARY KEY '||
' (PK_LKPFILTER), '||
'  CONSTRAINT ER_LKPFILTER_KEY_UNIQ '||
' UNIQUE (LKPFILTER_KEY))';
  end if;
END;
/

DECLARE 
  seq_count NUMBER; 
BEGIN 
  select count(*) into seq_count from ALL_SEQUENCES where 
  upper(SEQUENCE_OWNER) = 'ERES' and SEQUENCE_NAME = 'SEQ_ER_LKPFILTER';

  if (seq_count < 1) then 
    execute immediate
    'CREATE SEQUENCE ERES.SEQ_ER_LKPFILTER START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCACHE  NOCYCLE NOORDER';
  end if;
END;
/

-- Comment operations can be executed multiple times

COMMENT ON TABLE ERES.ER_LKPFILTER IS 'Table to register all SQL fragments of the filters which are used in getlookup.jsp and multilookup.jsp.';

COMMENT ON COLUMN ERES.ER_LKPFILTER.PK_LKPFILTER IS 'Primary key of the table.';

COMMENT ON COLUMN ERES.ER_LKPFILTER.LKPFILTER_KEY IS 'Key of the filter as string, which is also used in Java/JSP. Must be unique. Case-sensitive.';

COMMENT ON COLUMN ERES.ER_LKPFILTER.LKPFILTER_SQL IS 'SQL fragment of the filter; for parameters, use ?1, ?2, etc. in the SQL and specify types below.';

COMMENT ON COLUMN ERES.ER_LKPFILTER.LKPFILTER_PARAM_TYPE IS 'Ordered list of comma-separated types chosen from: NUMBER, NUMBER_LIST, CHAR2, VARCHAR, VARCHAR_NO_SPACES. Leave as null if no parameter. E.g. for two parameters, enter: NUMBER,VARCHAR';

COMMENT ON COLUMN ERES.ER_LKPFILTER.LKPFILTER_COMMENT IS 'Descriptions for DBA; not used by the application.';


