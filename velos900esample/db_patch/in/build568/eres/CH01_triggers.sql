SET DEFINE OFF;
create or replace TRIGGER "ERES"."ER_ER_ACCOUNT_BU_LM" 
BEFORE UPDATE ON ER_ACCOUNT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW WHEN (NEW.last_modified_by IS NOT NULL)  --KM-3634
begin
:new.last_modified_date := sysdate ;
 end;
/

create or replace
TRIGGER ER_MILEPAYMENT_AU0
AFTER UPDATE OF CREATED_ON,CREATOR,FK_MILESTONE,FK_STUDY,IP_ADD,MILEPAYMENT_AMT,MILEPAYMENT_DATE,MILEPAYMENT_DELFLAG,MILEPAYMENT_DESC,PK_MILEPAYMENT,RID
ON ER_MILEPAYMENT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr varchar2(2000);
BEGIN
  --KM-#3634
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.LAST_MODIFIED_BY);
 
  --KM-#3634
   IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     Audit_Trail.record_transaction
    (raid, 'ER_MILEPAYMENT', :OLD.rid, 'U', usr);
  END IF;
  
 
  IF NVL(:OLD.pk_milepayment,0) !=
     NVL(:NEW.pk_milepayment,0) THEN
     Audit_Trail.column_update
       (raid, 'PK_MILEPAYMENT',
       :OLD.pk_milepayment, :NEW.pk_milepayment);
  END IF;
  IF NVL(:OLD.fk_study,0) !=
     NVL(:NEW.fk_study,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_STUDY',
       :OLD.fk_study, :NEW.fk_study);
  END IF;
  IF NVL(:OLD.fk_milestone,0) !=
     NVL(:NEW.fk_milestone,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_MILESTONE',
       :OLD.fk_milestone, :NEW.fk_milestone);
  END IF;
  IF NVL(:OLD.milepayment_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.milepayment_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     Audit_Trail.column_update
       (raid, 'MILEPAYMENT_DATE',
       to_char(:OLD.milepayment_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.milepayment_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.milepayment_amt,0) !=
     NVL(:NEW.milepayment_amt,0) THEN
     Audit_Trail.column_update
       (raid, 'MILEPAYMENT_AMT',
       :OLD.milepayment_amt, :NEW.milepayment_amt);
  END IF;
  IF NVL(:OLD.milepayment_desc,' ') !=
     NVL(:NEW.milepayment_desc,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILEPAYMENT_DESC',
       :OLD.milepayment_desc, :NEW.milepayment_desc);
  END IF;
  IF NVL(:OLD.milepayment_delflag,' ') !=
     NVL(:NEW.milepayment_delflag,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILEPAYMENT_DELFLAG',
       :OLD.milepayment_delflag, :NEW.milepayment_delflag);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     Audit_Trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     Audit_Trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     Audit_Trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     Audit_Trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
   IF NVL(:OLD.milepayment_comments,' ') !=
     NVL(:NEW.milepayment_comments,' ') THEN
     Audit_Trail.column_update
       (raid, 'milepayment_comments',
       :OLD.milepayment_comments, :NEW.milepayment_comments);
  END IF;
  IF NVL(:OLD.milepayment_type,0) !=
     NVL(:NEW.milepayment_type,0) THEN
     Audit_Trail.column_update
       (raid, 'milepayment_type',
       :OLD.milepayment_type, :NEW.milepayment_type);
  END IF;

END;
/

create or replace
TRIGGER ERES.ER_STUDYSEC_AU0
AFTER UPDATE
ON ERES.ER_STUDYSEC REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  old_modby varchar2(100) ;
  new_modby varchar2(100) ;
  old_study varchar2(4000) ;
  new_study varchar2(4000) ;
  usr varchar2(100);
begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(:new.last_modified_by);
  
  --KM-#3634
   IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     audit_trail.record_transaction
    (raid, 'ER_STUDYSEC', :old.rid, 'U', usr);
  END IF;
  
  if nvl(:old.pk_studysec,0) !=
     NVL(:new.pk_studysec,0) then
     audit_trail.column_update
       (raid, 'PK_STUDYSEC',
       :old.pk_studysec, :new.pk_studysec);
  end if;

  if nvl(:old.fk_study,0) !=
     NVL(:new.fk_study,0) then
   Begin
     select study_title into old_study
     from er_study where pk_study = :old.fk_study ;
     Exception When NO_DATA_FOUND then
      old_study := null ;
    End ;
    Begin
     select study_title into new_study
     from er_study where pk_study = :new.fk_study ;
     Exception When NO_DATA_FOUND then
      new_study := null ;
    End ;
     audit_trail.column_update
       (raid, 'FK_STUDY',
       old_study, new_study);
  end if;
  if nvl(:old.studysec_name,' ') !=
     NVL(:new.studysec_name,' ') then
     audit_trail.column_update
       (raid, 'STUDYSEC_NAME',
       :old.studysec_name, :new.studysec_name);
  end if;

  if nvl(:old.studysec_num,' ') !=
     NVL(:new.studysec_num,' ') then
     audit_trail.column_update
       (raid, 'STUDYSEC_NUM',
       :old.studysec_num, :new.studysec_num);
  end if;
  if nvl(:old.studysec_pubflag,' ') !=
     NVL(:new.studysec_pubflag,' ') then
     audit_trail.column_update
       (raid, 'STUDYSEC_PUBFLAG',
       :old.studysec_pubflag, :new.studysec_pubflag);
  end if;
  if nvl(:old.studysec_seq,0) !=
     NVL(:new.studysec_seq,0) then
     audit_trail.column_update
       (raid, 'STUDYSEC_SEQ',
       :old.studysec_seq, :new.studysec_seq);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
   Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into old_modby
     from er_user
     where pk_user = :old.last_modified_by ;
     Exception When NO_DATA_FOUND then
      old_modby := null ;
    End ;
    Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.last_modified_by ;
     Exception When NO_DATA_FOUND then
      new_modby := null ;
    End ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  end if;
  if nvl(:old.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
  end if;


  if  :old.STUDYSEC_text  !=  :new.STUDYSEC_text then
--JM: commented and modified in the new line

     --audit_trail.column_update  (raid, 'STUDYSEC_TEXT',    dbms_lob.substr(:old.STUDYSEC_CONTENTS ,1,4000), dbms_lob.substr(:new.STUDYSEC_CONTENTS,1,4000));
     audit_trail.column_update  (raid, 'STUDYSEC_TEXT',    dbms_lob.substr(:OLD.STUDYSEC_text ,4000,1), dbms_lob.substr(:NEW.STUDYSEC_text,4000,1));
  end if;

end;
/