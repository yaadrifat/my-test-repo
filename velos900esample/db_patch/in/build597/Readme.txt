-- Build 597
=========================================================================================================================

eResearch enhancement INF-18183:

We have released the last set of JSP files w.r.t. INF-18183 enhancement.

Please refer to INF18183impactJsps.xls file for more details.


Internal Release:

As per the directions from Velos, we have included " method="POST" " in Form tag  across all Jsp files in the application. 

=========================================================================================================================

=========================================================================================================================
eResearch Localization
----------------------

Entire Application JSPs : For implementing UTF-8 char. set

Bug fixed : #7140
1.   irbongoing.jsp

Others For Localization :
2	addspecimenurl.jsp
3	adminauditreports.jsp
4	adminreports.jsp
5	adminrepRetrieve.jsp
6	adminusersearchdetails.jsp
7	budgetapndxdelete.jsp
8	dataRecvd.jsp
9	editmultiplespecimenstatus.jsp
10	irbactionwin.jsp
11	irbhistory.jsp
12	irbReview.jsp
13	login.jsp
14	menusiemac.jsp
15	milestonetabs.jsp
16	milewait.jsp
17	myHome.jsp
18	myHome1.jsp
19	patientreports.jsp
20	patientstudies.jsp
21	patientStudyFormBrowser.jsp
22	patstudyhistory.jsp
23	preparationCart.jsp
24	printMultiLabel.jsp
25	register.jsp
26	register1.jsp
27	revSetup.jsp
28	rolerights.jsp
29	searchLocation.jsp
30	sendNotification.jsp
31	specimenapndxdelete.jsp
32	specimenapndxfile.jsp
33	specimenbrowser.jsp
34	specimenurlsave.jsp
35	studyFormDD.jsp
36	studynumexists.jsp
37	studyschedule.jsp
38	studysearchresults.jsp
39	studysearchviewresults.jsp
40	studytabs.jsp
41	studyteamrolebr.jsp
42	studyversiondelete.jsp
43	submissionBrowser.jsp
44	testJMSConnection.jsp
45	timeout_adminchild.jsp
46	transformAdHoc.jsp
47	underConstruction.jsp
48	updateaccountuser.jsp
49	UpdateActualDate.jsp
50	updateaddnewquery.jsp
51	updateadvevent.jsp
52	updateadvevent_new.jsp
53	updatealertnotify.jsp
54	updatealertnotify_arvind.jsp
55	updatealertnotifysettings.jsp
56	updateallpatientdetails.jsp
57	updatebgtnewsection.jsp
58	updatecategory.jsp
59	updatecategoryselectus.jsp
60	updateCopyStorage.jsp
61	updateCoverage.jsp
62	updatecrfnotifyglobal.jsp
63	updatedynadvance.jsp
64	updatedynfilter.jsp
65	updatedynrep.jsp
66	updateeventfile.jsp
67	updateEvtVisitPatSched.jsp
68	updateEvtVisits.jsp
69	updateFormAction.jsp
70	updateFormData.jsp
71	updateInvDetails.jsp
72	updatelab.jsp
73	updateManageCart.jsp
74	updatemilepayments.jsp
75	updateMilestone.jsp
76	updatemilestonerule.jsp
77	updateMorePerDetails.jsp
78	updatemsgcntr.jsp
79	updatemsgcntrbak.jsp
80	updatemultipleschedules.jsp
81	updatemultiplespecimens.jsp
82	updatemultiplestorage.jsp
83	updatenewcrfnotify.jsp
84	updatenewcrfstatus.jsp
85	updateNewSubmission.jsp
86	updateNonSystemUser.jsp
87	updatenotification.jsp
88	updatenotificationglobal.jsp
89	updatepatenrolldetails.jsp
90	updatepatientdetails.jsp
91	updatepatlogin.jsp
92	updatepatstatmilestone.jsp
93	updateportal.jsp
94	updatePortalDesign.jsp
95	updateportalformaccesspg.jsp
96	updatePortalStatus.jsp
97	updatepref.jsp
98	updatepref2.jsp
99	updatePrepareSpecimen.jsp
100	updateprotocolstatus.jsp
101	updateProviso.jsp
102	updateReason.jsp
103	updaterolerights.jsp
104	updateSCIVisits.jsp
105	updatesiterights.jsp
106	updatesnapshot.jsp
107	updatespcimenstatus.jsp
108	updatespecimendetails.jsp
109	updatestoragekit.jsp
110	updatestoragestatus.jsp
111	updatestorageunitdetails.jsp
112	updatestudyalnot.jsp
113	updatestudyalnotettings.jsp
114	updatestudycentricenroll.jsp
115	updatestudydates.jsp
116	updatestudyeventnot.jsp
117	updatestudyrights.jsp
118	updatestudyteam.jsp
119	updateSubmission.jsp
120	updateSubmissionStatus.jsp
121	updatesupbudrights.jsp
122	updatesupuserrights.jsp
123	updateTeam.jsp
124	updatstudycrfnot.jsp
125	uploadmileapndx.jsp
126	user.jsp
127	userbak.jsp
128	userSearchByPin.jsp
129	userStudies.jsp
130	userStudiesforpage.jsp
131	userStudiesforpagecs.jsp
132	veloshome.jsp
133	veloslogin.jsp
134	velosmsgs.jsp
135	viepanel.jsp
136	viewSubmissionResponses.jsp
137	activateuser.jsp
138	AddCost.jsp
139	addformtostudyacc.jsp
140	attributesForStudyAccountSubmit.jsp
141	attributesForStudyForms.jsp
142	bottompanel.jsp
143	budgetdelete.jsp
144	budgetfileupdate.jsp
145	budgetsectiondelete.jsp
146	calendarstatus.jsp
147	calLibraryDelete.jsp
148	caltolibrary.jsp
149	copyFormForStudy.jsp
150	copystudy.jsp
151	deletepatstudy.jsp
152	deleteportal.jsp
153	deletestorages.jsp
154	deleteStudySite.jsp
155	delMulLineitemsSubmit.jsp
156	editBudgetCalAttributes.jsp
157	editmilestonestatus.jsp
158	editPersonnelCostSubmit.jsp
159	editRepeatLineItemsSubmit.jsp
160	editStudyMulEventDetails.jsp
161	eventdelete.jsp
162	formprint.jsp
163	Formrespdelete.jsp
164	linkFormType.jsp
165	linkFormTypesubmit.jsp
166	loggedon_err.jsp
167	managesites.jsp
168	milestonedelete.jsp
169	nonSystemUserDetails.jsp
170	patlogin.jsp
171	patOrgview.jsp
172	protocolhistory.jsp
173	refreshProtocolMessages.jsp
174	refreshstudyalnot.jsp
175	savecaltolibrary.jsp
176	savecopystudy.jsp
177	savegrouplist.jsp
178	saveMultiMilestones.jsp
179	saveNewOrganization.jsp
180	savesubscribe.jsp
181	selectprotocol.jsp
182	specimenstatus.jsp
183	specimenstatusdelete.jsp
184	storagestatus.jsp
185	storagestatusdelete.jsp
186	studyalnotsettings.jsp
187	studycalendardelete.jsp
188	studyeventnot.jsp
189	studyeventnotbrowse.jsp
190	Studyformdetails.jsp
191	suprightfilter.jsp
192	supuserstudyrights.jsp
=========================================================================================================================