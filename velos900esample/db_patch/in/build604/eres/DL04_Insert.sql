DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM ER_LABTEST WHERE LABTEST_NAME = 'HIV I/II';
  
  IF (index_count = 0) THEN
    INSERT INTO ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC)
    VALUES(SEQ_ER_LABTEST.NEXTVAL,'HIV I/II','HIVINII',null,null,null);
    dbms_output.put_line('One row created');
  ELSE
    dbms_output.put_line('LABTEST_NAME: "HIV I/II" is already exists');
  END IF;
END;
/
COMMIT;

--1 record is created

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM ER_LABTEST WHERE LABTEST_NAME = 'HBsAg';
  
  IF (index_count = 0) THEN
    INSERT INTO ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC)
    VALUES(SEQ_ER_LABTEST.NEXTVAL,'HBsAg','HB',null,null,null);
    dbms_output.put_line('One row created');
  ELSE
    dbms_output.put_line('LABTEST_NAME: "HBsAg" is already exists');
  END IF;
END;
/
COMMIT;

--2 record is created


DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM ER_LABTEST WHERE LABTEST_NAME = 'HCV';
  
  IF (index_count = 0) THEN
    INSERT INTO ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC)
    VALUES(SEQ_ER_LABTEST.NEXTVAL,'HCV','HCV',null,null,null);
    dbms_output.put_line('One row created');
  ELSE
    dbms_output.put_line('LABTEST_NAME: "HCV" is already exists');
  END IF;
END;
/
COMMIT;

--3 record is created


DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM ER_LABTEST WHERE LABTEST_NAME = 'T cruzi / Chagas';
  
  IF (index_count = 0) THEN
    INSERT INTO ER_LABTEST (PK_LABTEST,LABTEST_NAME,LABTEST_SHORTNAME,LABTEST_CPT,FK_LKP_ADV_NCI,GRADE_CALC)
    VALUES(SEQ_ER_LABTEST.NEXTVAL,'T cruzi / Chagas','TCRUZI',null,null,null);
    dbms_output.put_line('One row created');
  ELSE
    dbms_output.put_line('LABTEST_NAME: "T cruzi / Chagas" is already exists');
  END IF;
END;
/
COMMIT;

--4 record is created



DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM ER_LABTESTGRP WHERE FK_TESTID = (SELECT PK_LABTEST FROM ER_LABTEST WHERE LABTEST_NAME='HIV I/II');
  
  IF (index_count = 0) THEN
    INSERT INTO ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP)
    VALUES(SEQ_ER_LABTESTGRP.NEXTVAL,(SELECT PK_LABTEST FROM ER_LABTEST WHERE LABTEST_NAME='HIV I/II'),(SELECT PK_LABGROUP FROM ER_LABGROUP WHERE GROUP_TYPE='I'));
    dbms_output.put_line('One row created');
  ELSE
    dbms_output.put_line('LABTEST_NAME: "HIV I/II" MAPPING is already exists');
  END IF;
END;
/
COMMIT;
--1 RECORD  IS INSERTED



DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM ER_LABTESTGRP WHERE FK_TESTID = (SELECT PK_LABTEST FROM ER_LABTEST WHERE LABTEST_NAME='HBsAg');
  
  IF (index_count = 0) THEN
    INSERT INTO ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP)
    VALUES(SEQ_ER_LABTESTGRP.NEXTVAL,(SELECT PK_LABTEST FROM ER_LABTEST WHERE LABTEST_NAME='HBsAg'),(SELECT PK_LABGROUP FROM ER_LABGROUP WHERE GROUP_TYPE='I'));
    dbms_output.put_line('One row created');
  ELSE
    dbms_output.put_line('LABTEST_NAME: "HBsAg" MAPPING is already exists');
  END IF;
END;
/
COMMIT;
--2 RECORD  IS INSERTED



DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM ER_LABTESTGRP WHERE FK_TESTID = (SELECT PK_LABTEST FROM ER_LABTEST WHERE LABTEST_NAME='HCV');
  
  IF (index_count = 0) THEN
    INSERT INTO ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP)
    VALUES(SEQ_ER_LABTESTGRP.NEXTVAL,(SELECT PK_LABTEST FROM ER_LABTEST WHERE LABTEST_NAME='HCV'),(SELECT PK_LABGROUP FROM ER_LABGROUP WHERE GROUP_TYPE='I'));
    dbms_output.put_line('One row created');
  ELSE
    dbms_output.put_line('LABTEST_NAME: "HCV" MAPPING is already exists');
  END IF;
END;
/
COMMIT;
--3 RECORD  IS INSERTED



DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM ER_LABTESTGRP WHERE FK_TESTID = (SELECT PK_LABTEST FROM ER_LABTEST WHERE LABTEST_NAME='T cruzi / Chagas');
  
  IF (index_count = 0) THEN
    INSERT INTO ER_LABTESTGRP (PK_LABTESTGRP,FK_TESTID,FK_LABGROUP)
    VALUES(SEQ_ER_LABTESTGRP.NEXTVAL,(SELECT PK_LABTEST FROM ER_LABTEST WHERE LABTEST_NAME='T cruzi / Chagas'),(SELECT PK_LABGROUP FROM ER_LABGROUP WHERE GROUP_TYPE='I'));
    dbms_output.put_line('One row created');
  ELSE
    dbms_output.put_line('LABTEST_NAME: "T cruzi / Chagas" MAPPING is already exists');
  END IF;
END;
/
COMMIT;
--4 RECORD  IS INSERTED


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'entity_type'
    AND codelst_subtyp = 'ORDER';
  if (v_record_exists = 0) then

     INSERT INTO
 ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD)
  VALUES(SEQ_ER_CODELST.nextval,'entity_type','ORDER','ORDER','N',NULL,SYSDATE,SYSDATE,NULL);

    commit;

  end if;
end;
/
--END--


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'minimum_decl','meets','Meets minimum requirements','N',null,null,null,null,null,null,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'minimum_decl','doesntmeets','Does not meets minimum requirements','N',null,null,null,null,null,null,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'minimum_field','yes','Yes','N',null,null,null,null,null,null,sysdate,null,null,null,null);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'minimum_field','no','No','N',null,null,null,null,null,null,sysdate,null,null,null,null);



commit;


