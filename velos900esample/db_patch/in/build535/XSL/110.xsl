<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
<xsl:key name="RecordsByCategory" match="ROW" use="BGTSECTION_NAME" />
<xsl:key name="RecordsByCategorySOC" match="ROW" use="concat(BGTSECTION_NAME, ' ', STANDARD_OF_CARE)" /> 
<xsl:key name="RecordsBySOC" match="ROW" use="STANDARD_OF_CARE" /> 
<xsl:key name="RecordsByPersonnel" match="ROW" use="CATEGORY_SUBTYP" />
<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>
<xsl:param name="mode"/>
<xsl:param name="budgetTemplate"/>
<xsl:param name="pkBgtCal"/>
<xsl:param name="budgetStatus"/>
<xsl:param name="pkBudget"/>
<xsl:param name="pageRight"/>
<xsl:param name="includedIn"/>
<xsl:param name="from"/>
<xsl:param name="ddList"/>
<xsl:template match="/">
<HTML>
<HEAD><TITLE> <xsl:value-of select="$repName" /> </TITLE>
<link rel="stylesheet" href="./styles/common.css" type="text/css"/>
</HEAD>
<BODY class="repBody">
<input>
	<xsl:attribute name="type">hidden</xsl:attribute>
	<xsl:attribute name="id">fromWhere</xsl:attribute>
	<xsl:attribute name="name">fromWhere</xsl:attribute>
	<xsl:attribute name="value"><xsl:value-of select="$from"/></xsl:attribute>
</input>
        
<xsl:if test="$cond='T'">
    <xsl:if test="$mode='V'">
        <table width="100%" >
        <tr class="reportGreyRow">
        <td class="reportPanel"> 
        Download the report in: 
        <A href='{$wd}' >
        Word Format
        </A> 
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <A href='{$xd}' >
        Excel Format
        </A>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <xsl:text>&#xa0;</xsl:text>
        <A href='{$hd}' >
        Printer Friendly Format
        </A> 
        </td>
        </tr>
        </table>
    </xsl:if>
</xsl:if>
<xsl:apply-templates select="ROWSET"/>
</BODY>
</HTML>
</xsl:template> 

<xsl:template match="ROWSET">
<xsl:variable name="excludeSOC_flag">
<xsl:value-of select="//BGTCAL_EXCLDSOCFLAG"/>
</xsl:variable>

<xsl:if test="$mode='V'">
    <TABLE WIDTH="100%" >
    <xsl:if test="$hdrflag='1'">
    <TR>
    <TD WIDTH="100%" ALIGN="CENTER">
    <img src="{$hdrFileName}"/>
    </TD>
    </TR>
    </xsl:if>
    <TR>
    <TD class="reportName" WIDTH="100%" ALIGN="CENTER">
    <xsl:value-of select="$repName" />
    </TD>
    </TR>
    </TABLE>
    <hr class="thickLine" />

    <TABLE WIDTH="100%" >
	    <TR>
		    <TD class="reportGrouping" ALIGN="Left" width="22%">
		    Budget Name: </TD><TD class="reportData"><b><xsl:value-of select="//BUDGET_NAME" /></b>
		    </TD>
	    </TR>
	    <TR>
		    <TD class="reportGrouping" ALIGN="Left" width="22%">
		    	Budget Version: </TD><TD class="reportData" align="left"><b><xsl:value-of select="//BUDGET_VERSION" /></b>
		    </TD>
	    </TR>
	    <TR>
		    <TD class="reportGrouping" ALIGN="Left" width="22%">
		    	Budget Description: </TD><TD class="reportData"><b><xsl:value-of select="//BUDGET_DESC" /></b>
		    </TD>
	    </TR>
	    <TR>
		    <TD class="reportGrouping" ALIGN="left" width="22%">
		    	Study Number: </TD><TD class="reportData"><b><xsl:value-of select="//STUDY_NUMBER" /></b>
		    </TD>
	    </TR>
	    <TR>
		    <TD class="reportGrouping" ALIGN="LEFT" width="22%">
		    	Study Title: </TD><TD class="reportData"><b><xsl:value-of select="//STUDY_TITLE" /></b>
		    </TD>
	    </TR>
	    <TR>
		    <TD class="reportGrouping" ALIGN="LEFT" width="22%">
		    	Protocol Calendar: </TD><TD class="reportData"><b><xsl:value-of select="//PROT_CALENDAR" /></b>
		    </TD>
	    </TR>
	    <TR>
		    <TD class="reportGrouping" ALIGN="left" width="22%">
		    	Organization: </TD><TD class="reportData"><b><xsl:value-of select="//SITE_NAME" /></b>
		    </TD>
	    </TR>
    </TABLE>
    <hr class="thinLine" />
</xsl:if> 
<xsl:variable name="divHt">
	<xsl:choose>
		<xsl:when test="$budgetTemplate='C'">400</xsl:when>
		<xsl:otherwise>300</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<div id="TableContainer" class="TableContainer" style="height:320px;">
    <TABLE class="scrollTable" WIDTH="100%" style="background-color:black;" border="1" CELLSPACING="1">
	<thead class="fixedHeader headerFormat">
        <TR class="title">
			<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">Event</TH>
			<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">Category</TH>
			<TH class="reportHeading" WIDTH="6%" ALIGN="CENTER">Cost Type</TH>
			<TH class="reportHeading" WIDTH="5%" ALIGN="CENTER">Unit Cost</TH>
			<TH class="reportHeading" WIDTH="5%" ALIGN="CENTER" title="Number of Units">Units</TH>
			<TH class="reportHeading" WIDTH="5%" ALIGN="CENTER" title="Apply Discount OR Markup">D/M</TH>
			<TH class="reportHeading" WIDTH="5%" ALIGN="CENTER">Direct Cost/Patient</TH>
			<TH class="reportHeading" WIDTH="5%" ALIGN="CENTER">I/D Applied</TH>
			<TH class="reportHeading" WIDTH="8%" ALIGN="CENTER">Total Cost/Patient</TH>
			<TH class="reportHeading" WIDTH="8%" ALIGN="CENTER">Cost/ All Patients</TH>
			<xsl:if test="$budgetTemplate='C'">
				<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">Sponsor Amount</TH>
				<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">Variance</TH>
			</xsl:if>
		</TR>
	</thead>
	
    <tbody class="scrollContent bodyFormat" style="height:280px;">
	<xsl:variable name="colsp">
		<xsl:choose>
			<xsl:when test="$budgetTemplate='C'">12</xsl:when>
			<xsl:otherwise>10</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:if test="$from='study'">
	<TR style="background-color:white;">
	<TD>
		<xsl:attribute name="colspan"><xsl:value-of select="$colsp"/></xsl:attribute>
		<table width="100%" CELLSPACING="1">
			<tr>
				<TD class="reportData"><b><font color="red"><xsl:value-of select="//PROT_CALENDAR" /> </font></b>&#xa0;&#xa0;
				<xsl:if test="$mode='M'">
					<a>
						<xsl:attribute name="href">javascript:void(0);</xsl:attribute>
						<xsl:attribute name="onClick">openBudgetCalAttributes(<xsl:value-of select="$pkBgtCal"/>,
						<xsl:value-of select="$pkBudget"/>,<xsl:value-of select="$pageRight"/>,'<xsl:value-of select="$budgetStatus"/>')</xsl:attribute>
						Edit Calculation Attributes
					</a>
				</xsl:if>&#xa0;&#xa0;&#xa0;&#xa0;
				</TD>
				<xsl:variable name="cur" select='position()' />
				<TD align="right" valign="top" width="60%">
					<table CELLSPACING="1">
						<tr id="calIcons" name="calIcons" style="display:block">
							<TD valign="top" width="15%">
								<font size ="-2" color="#696969"><b>Reports</b></font><BR/>
								<xsl:value-of select="$ddList" disable-output-escaping="yes"/>
								<A><xsl:attribute name="href">javascript:void(0);</xsl:attribute>
									<xsl:attribute name="onClick">openCBBudReport(<xsl:value-of select="$cur"/>,<xsl:value-of select="$pkBudget"/>,<xsl:value-of select="$pkBgtCal"/>)</xsl:attribute>
									<img src="./FCKeditor/editor/skins/silver/toolbar/preview.gif" alt="Display Report" width="16" height="18" border="0" align="absbotton"/></A>
							</TD>
							<TD align="center" valign="top" width="7%">
								<font size="-2" color="#696969"><b>Sections</b></font><BR/>
								<A><xsl:attribute name="href">javascript:void(0);</xsl:attribute>
									<xsl:attribute name="onClick">openCBSections(<xsl:value-of select="$pkBudget"/>,<xsl:value-of select="$pkBgtCal"/>,'<xsl:value-of select="$budgetTemplate"/>')</xsl:attribute>
									<img src="./images/section.gif" alt="Add and Edit Sections" width="16" height="18" border="1" align="absbotton"/></A>
							</TD>
							<TD align="center" valign="top" width="9%">
								<font size="-2" color="#696969"><b>Repeating</b></font><BR/>
								<A><xsl:attribute name="href">javascript:void(0);</xsl:attribute>
									<xsl:attribute name="onClick">openCBRepeatLineItems(<xsl:value-of select="$pkBudget"/>,<xsl:value-of select="$pkBgtCal"/>,'<xsl:value-of select="$budgetTemplate"/>')</xsl:attribute>
									<img src="./images/note.gif" alt="Add and Edit Repeating Line Items" width="16" height="18" border="1" align="absbotton"/></A>
							</TD>
							<TD align="center" valign="top" width="10%">
								<font size ="-2" color="#696969"><b>Personnel Costs</b></font><BR/>
								<A><xsl:attribute name="href">javascript:void(0);</xsl:attribute>
								<xsl:attribute name="onClick">openCBEditPersonnelCost(<xsl:value-of select="$pkBudget"/>,<xsl:value-of select="$pkBgtCal"/>,'<xsl:value-of select="$budgetTemplate"/>')</xsl:attribute>
								<img src="./images/patientsearch.jpg" alt="Add and Edit Personnel Costs" width="16" height="18" border="1" align="absbotton"/></A>
							</TD>
							<TD align="center" valign="top" width="10%">
								<font size="-2" color="#696969"><b>Exclude Multiple</b></font><BR/>
								<A><xsl:attribute name="href">javascript:void(0);</xsl:attribute>
								<xsl:attribute name="onClick">openCBDelete(<xsl:value-of select="$pkBgtCal"/>)</xsl:attribute>
								<img src="./images/delete.gif" alt="Exclude Multiple" width="16" height="18" border="1" align="absbotton"/></A>
							</TD>
						</tr>
					</table>
				</TD>	
			</tr>
		</table>
	</TD>
	</TR>
	</xsl:if>
    <xsl:for-each select="ROW[count(. | key('RecordsByCategory', BGTSECTION_NAME)[1])=1]">
<TR style="background-color:white;" >

<xsl:variable name="sectionType">
<xsl:value-of select="BGTSECTION_TYPE"/>
</xsl:variable>

<xsl:variable name="sectionTypeText">
<xsl:choose>
<xsl:when test="$sectionType='P'">Per Patient Fees</xsl:when> 
<xsl:otherwise>One Time Fees</xsl:otherwise>
</xsl:choose> 
</xsl:variable>


<TD class="reportData" colspan="6">
<b><xsl:value-of select="BGTSECTION_NAME" /></b>

&#xa0;&#xa0;<i>(<xsl:value-of select="$sectionTypeText"/>)</i> &#xa0;&#xa0;
</TD>


<TD class="reportData" colspan="2" align="right">
	<xsl:if test="($sectionType='P')">
		<b>Number of Patients: <xsl:value-of select="BGTSECTION_PATNO" /></b>
	</xsl:if>&#xa0;
</TD>

<TD colspan="4">

<xsl:variable name="visit">
<xsl:value-of select="BUDGETSEC_FKVISIT"/>
</xsl:variable>

<xsl:variable name="allowSubmitSection">
<xsl:choose>
<xsl:when test="(($includedIn='P' and $visit = 0) or $includedIn='')">1</xsl:when> 
<xsl:otherwise>0</xsl:otherwise>
</xsl:choose> 
</xsl:variable>

<xsl:if test="($mode='M')">
    <a>
        <xsl:attribute   name="href">javascript:void(0);</xsl:attribute>  <xsl:attribute name="onClick">openLineItem(<xsl:value-of select="$pkBgtCal"/>,<xsl:value-of select="$pkBudget"/>,<xsl:value-of select="PK_BUDGETSEC" />,'<xsl:value-of select="BGTSECTION_NAME"/>','<xsl:value-of select="$budgetStatus"/>','M','<xsl:value-of select="$pageRight"/>','','','','<xsl:value-of select="$allowSubmitSection"/>','<xsl:value-of select="$budgetTemplate"/>')</xsl:attribute> Edit Section Lineitems</a>
 </xsl:if>       

   <xsl:if test="($mode='M' and $allowSubmitSection=1)">
        &#xa0;&#xa0;
 
        <a>
        <xsl:attribute     name="href">javascript:void(0);</xsl:attribute>
        <xsl:attribute name="onClick">openLineItem(<xsl:value-of select="$pkBgtCal"/>,<xsl:value-of select="$pkBudget"/>,<xsl:value-of select="PK_BUDGETSEC" />,'<xsl:value-of select="BGTSECTION_NAME"/>','<xsl:value-of select="$budgetStatus"/>','N','<xsl:value-of select="$pageRight"/>','','','',1,'<xsl:value-of select="$budgetTemplate"/>')
            </xsl:attribute>
        Add New Lineitem</a>
  </xsl:if>            
</TD>
</TR>

<xsl:variable name="str" select="key('RecordsByCategory', BGTSECTION_NAME)" />
<xsl:for-each select="key('RecordsByCategory', BGTSECTION_NAME)">



<xsl:variable name="vcost_custom">
<xsl:value-of select="STANDARD_OF_CARE"/>
</xsl:variable>


<xsl:variable name="class">
	<xsl:if test="$excludeSOC_flag='1' and $vcost_custom='Yes'">
		pastSchRow
	</xsl:if>            

<xsl:if test="( $excludeSOC_flag='1' and $vcost_custom != 'Yes') or $excludeSOC_flag='0'">
	<xsl:choose>
	<xsl:when test="number(position() mod 2)=0" >reportEvenRow</xsl:when> 
	<xsl:otherwise>reportOddRow</xsl:otherwise>
	</xsl:choose> 
</xsl:if>      

</xsl:variable>
 

<xsl:variable name="visitLine">
<xsl:value-of select="BUDGETSEC_FKVISIT"/>
</xsl:variable>

<xsl:variable name="allowSubmitLine">
<xsl:choose>
<xsl:when test="(($includedIn='P' and $visitLine = 0) or $includedIn='')">1</xsl:when> 
<xsl:otherwise>0</xsl:otherwise>
</xsl:choose> 
</xsl:variable>

<TR> 
<xsl:attribute name="class">
	<xsl:choose>
		<xsl:when test="($class='')">reportOddRow</xsl:when> 
		<xsl:otherwise><xsl:value-of select="$class"/></xsl:otherwise>
	</xsl:choose> 
</xsl:attribute>
<xsl:variable name="lineitem"><xsl:value-of select="LINEITEM_NAME"/></xsl:variable>
<xsl:choose>
<xsl:when test="$mode='M'">
    <TD class="reportData" >
    	<xsl:attribute name="title"><xsl:value-of select="$lineitem"/></xsl:attribute>
	    <xsl:variable name="vlineitem_name_len">
			<xsl:value-of select="string-length(LINEITEM_NAME)"/>
		</xsl:variable>
		<xsl:variable name="vlineitem_name">
			<xsl:choose>
				<xsl:when test="($vlineitem_name_len &gt; 50)"><xsl:value-of select="concat(substring(LINEITEM_NAME,1,50),' ','...')"/></xsl:when> 
				<xsl:otherwise><xsl:value-of select="LINEITEM_NAME"/></xsl:otherwise>
			</xsl:choose> 
		</xsl:variable>
    	<a>
        <xsl:attribute name="href">javascript:void(0);</xsl:attribute>
		<xsl:attribute name="onClick">openLineItem(<xsl:value-of select="$pkBgtCal"/>,<xsl:value-of select="$pkBudget"/>,<xsl:value-of select="PK_BUDGETSEC" />,'<xsl:value-of select="BGTSECTION_NAME"/>','<xsl:value-of select="$budgetStatus"/>','M','<xsl:value-of select="$pageRight"/>','<xsl:value-of select="PK_LINEITEM"/>','<xsl:value-of select="LINEITEM_NAME"/>',<xsl:value-of select="LINEITEM_INPERSEC"/>,<xsl:value-of select="$allowSubmitLine"/>,'<xsl:value-of select="$budgetTemplate"/>')</xsl:attribute>
        <xsl:value-of select="$vlineitem_name" /></a>
    </TD>
</xsl:when> 
<xsl:otherwise>
    <TD class="reportData" >
    	<xsl:attribute name="title"><xsl:value-of select="$lineitem"/></xsl:attribute>
    	<xsl:value-of select="LINEITEM_NAME" />
    </TD>
</xsl:otherwise>    
</xsl:choose> 

            
<TD class="reportData" >
 
<xsl:value-of select="CATEGORY" /></TD>
<TD class="reportData" ><xsl:value-of select="COST_TYPE_DESC" /></TD>
<TD class="reportData" ALIGN="right"><xsl:value-of select="UNIT_COST" /></TD>
<TD class="reportData" ALIGN="right"><xsl:value-of select="NUMBER_OF_UNIT" /></TD>
<TD class="reportData" ALIGN="center"><xsl:value-of select="COST_DISCOUNT_ON_LINE_ITEM" /></TD>
<TD class="reportData" ALIGN="right"><xsl:value-of select="format-number(LINEITEM_DIRECT_PERPAT,'##,###,###,###,###,##0.00')" /></TD>
<TD class="reportData" ALIGN="center"><xsl:value-of select="LINE_ITEM_INDIRECTS_FLAG"/></TD>
<TD class="reportData" ALIGN="right"><xsl:value-of select="format-number(TOTAL_COST_PER_PAT,'##,###,###,###,###,##0.00')" /></TD>
<TD class="reportData" ALIGN="right"><xsl:value-of select="format-number(TOTAL_COST_ALL_PAT,'##,###,###,###,###,##0.00')" /></TD>


<xsl:if test="$budgetTemplate='C'">
     
    <TD class="reportData" WIDTH="10%" ALIGN="right"><xsl:value-of select="SPONSOR_AMOUNT" /></TD>
    <TD class="reportData" WIDTH="10%" ALIGN="right"><xsl:value-of select="L_VARIANCE" /></TD>
</xsl:if>

</TR>

</xsl:for-each>


<TR style="background-color:white;">
<td>&#xa0;</td>
<td colspan="5" align="left" class="reportGrouping">Section Research Total </td>


<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','No'))/LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></td>
<td>&#xa0;</td>

<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','No'))/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','No'))/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></td>

<xsl:if test="$budgetTemplate='C'">
    
     
    <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','No'))/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></td>
    <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','No'))/SPONSOR_AMOUNT) - ( sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','No'))/TOTAL_COST_ALL_PAT)   ),'##,###,###,###,###,##0.00')"/></td>
</xsl:if>

</TR>


<TR style="background-color:white;">
<td>&#xa0;</td>
<td colspan="5" align="left" class="reportGrouping">Section SOC Total </td>

<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Yes'))/LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></td>
<td>&#xa0;</td>

<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Yes'))/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Yes'))/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></td>
 
<xsl:if test="$budgetTemplate='C'">

 
    <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Yes'))/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></td>
    <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Yes'))/SPONSOR_AMOUNT) - ( sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Yes'))/TOTAL_COST_ALL_PAT)  ) ,'##,###,###,###,###,##0.00')"/></td>

</xsl:if>

</TR>


<TR style="background-color:white;">
<td>&#xa0;</td>
<td colspan="5" align="left" class="reportGrouping">Section Other Cost Total </td>


<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Other'))/LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></td>
<td>&#xa0;</td>

<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Other'))/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Other'))/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></td>
 <xsl:if test="$budgetTemplate='C'">
 
     
    <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Other'))/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></td>
    <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Other'))/SPONSOR_AMOUNT) - ( sum(key('RecordsByCategorySOC', concat(BGTSECTION_NAME,' ','Other'))/TOTAL_COST_ALL_PAT)  ) ,'##,###,###,###,###,##0.00')"/></td>

</xsl:if>

</TR>


</xsl:for-each>

<TR style="background-color:white;">
<td>&#xa0;</td>
<td colspan="5" align="left" class="reportGrouping"><font color="red">Grand Research Total </font></td>


<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'No')/LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></font></td>
<td>&#xa0;</td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'No')/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'No')/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
 
<xsl:if test="$budgetTemplate='C'">
    
    
    <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'No')/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></font></td>
    <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'No')/SPONSOR_AMOUNT) - ( sum(key('RecordsBySOC', 'No')/TOTAL_COST_ALL_PAT)  ) ,'##,###,###,###,###,##0.00')"/></font></td>
</xsl:if>

</TR>

<TR style="background-color:white;">
<td>&#xa0;</td>
<td colspan="5" align="left" class="reportGrouping"><font color="red">Grand SOC Total </font></td>

<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Yes')/LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></font></td>
<td>&#xa0;</td>

<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Yes')/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Yes')/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
 
<xsl:if test="$budgetTemplate='C'">

    
    <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Yes')/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></font></td>
    <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Yes')/SPONSOR_AMOUNT) - ( sum(key('RecordsBySOC', 'Yes')/TOTAL_COST_ALL_PAT)  )   ,'##,###,###,###,###,##0.00')"/></font></td>
</xsl:if>

</TR>

<TR style="background-color:white;">
<td>&#xa0;</td>
<td colspan="5" align="left" class="reportGrouping"><font color="red">Grand Other Cost Total </font></td>

<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Other')/LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></font></td>
<td>&#xa0;</td>

<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Other')/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Other')/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
 
<xsl:if test="$budgetTemplate='C'">

    
    <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Other')/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></font></td>
    <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Other')/SPONSOR_AMOUNT) - ( sum(key('RecordsBySOC', 'Other')/TOTAL_COST_ALL_PAT)  )   ,'##,###,###,###,###,##0.00')"/></font></td>
</xsl:if>

</TR>

<TR style="background-color:white;">
<td>&#xa0;</td>
<td colspan="5" align="left" class="reportGrouping"><font color="red">Grand Total </font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></font></td>
<td>&#xa0;</td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
 

<xsl:if test="$budgetTemplate='C'">

    <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></font></td>
    <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//SPONSOR_AMOUNT) - ( sum(//TOTAL_COST_ALL_PAT) ) ,'##,###,###,###,###,##0.00')"/></font></td>

</xsl:if>

</TR>
<xsl:variable name="colsp">
	<xsl:choose>
		<xsl:when test="$budgetTemplate='C'">12</xsl:when>
		<xsl:otherwise>10</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<TR style="background-color:white;" >
	<td><xsl:attribute name="colspan"><xsl:value-of select="$colsp"/></xsl:attribute>&#xa0;</td>
</TR>

<xsl:variable name="colsp">
	<xsl:choose>
		<xsl:when test="$budgetTemplate='C'">9</xsl:when>
		<xsl:otherwise>7</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:for-each select="ROW[count(. | key('RecordsByPersonnel', CATEGORY_SUBTYP)[1])=1]">
<xsl:if test="CATEGORY_SUBTYP='ctgry_per'">
<TR style="background-color:white;" >
	<td><xsl:attribute name="colspan"><xsl:value-of select="$colsp"/></xsl:attribute>&#xa0;</td>
	<td align="left" class="reportGrouping">Total Salary </td>
	<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByPersonnel', CATEGORY_SUBTYP)/TOTAL_COST_PER_PAT),'##,###,###,###,###,##0.00')"/></td>
	<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByPersonnel', CATEGORY_SUBTYP)/TOTAL_COST_ALL_PAT),'##,###,###,###,###,##0.00')"/></td>
</TR>
</xsl:if>
</xsl:for-each>
<xsl:variable name="fringe_flag">
<xsl:value-of select="//FRINGE_FLAG"/>
</xsl:variable>

<xsl:variable name="colsp">
	<xsl:choose>
		<xsl:when test="$budgetTemplate='C'">12</xsl:when>
		<xsl:otherwise>10</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:choose>
<xsl:when test="$from='study'">
</xsl:when>

<xsl:otherwise>

<xsl:if test="$mode='M'">
<TR style="background-color:white;" >
	<TD class="reportGrouping">
		<xsl:attribute name="colspan"><xsl:value-of select="$colsp"/></xsl:attribute>
		<a>
			<xsl:attribute name="href">javascript:void(0);</xsl:attribute>
			<xsl:attribute name="onClick">openBudgetCalAttributes(<xsl:value-of select="$pkBgtCal"/>,
			<xsl:value-of select="$pkBudget"/>,<xsl:value-of select="$pageRight"/>,'<xsl:value-of select="$budgetStatus"/>')</xsl:attribute>
			Edit Calculation Attributes
		</a>&#xa0;&#xa0;&#xa0;&#xa0;
	</TD>
</TR>
</xsl:if>
</xsl:otherwise>

</xsl:choose>


<xsl:variable name="colsp">
	<xsl:choose>
		<xsl:when test="$budgetTemplate='C'">9</xsl:when>
		<xsl:otherwise>7</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<TR style="background-color:white;" >
	<td class="reportGrouping">
		<xsl:attribute name="colspan"><xsl:value-of select="$colsp"/></xsl:attribute>
		Fringe benefit of <xsl:value-of select="format-number(//FRINGE_BENEFIT,'##0.00')" />% 
		<xsl:choose>
			<xsl:when test="$fringe_flag='1'">Applied</xsl:when> 
			<xsl:otherwise>Not Applied</xsl:otherwise>
		</xsl:choose> 
	 	to all lineitems with Personnel category
	</td>

	<td class="reportGrouping">Total Fringe</td>
	<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//PER_PATIENT_LINE_FRINGE),'##,###,###,###,###,##0.00')"/></td>
	<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_LINE_FRINGE),'##,###,###,###,###,##0.00')"/></td>
</TR>


<xsl:variable name="discount_flag">
<xsl:value-of select="//BUDGET_DISCOUNT_FLAG"/>
</xsl:variable>

<xsl:variable name="discMText">
	<xsl:choose>
		<xsl:when test="$discount_flag='Discount'">Discount</xsl:when> 
		<xsl:when test="$discount_flag='Markup'">Markup</xsl:when> 
		<xsl:otherwise>Discount/Markup</xsl:otherwise>
	</xsl:choose> 
</xsl:variable>



<xsl:variable name="excludeSOC_flag_text">
	<xsl:choose>
		<xsl:when test="$excludeSOC_flag='1'">SOC Line Items excluded from the Totals </xsl:when> 
		<xsl:when test="$excludeSOC_flag='0'">SOC Line Items not excluded from the Totals </xsl:when> 
		<xsl:otherwise>SOC Line Items not excluded from the Totals </xsl:otherwise>
	</xsl:choose> 
</xsl:variable>


<TR style="background-color:white;" >
	<td class="reportGrouping">
		<xsl:attribute name="colspan"><xsl:value-of select="$colsp"/></xsl:attribute>
		Cost <xsl:value-of select="$discMText" /> of <xsl:value-of select="format-number(//BUDGET_DISCOUNT,'##0.00')" />% 
		<xsl:choose>
			<xsl:when test="$discount_flag='Discount'">Applied</xsl:when> 
			<xsl:when test="$discount_flag='Markup'">Applied</xsl:when> 
			<xsl:otherwise>Not Applied</xsl:otherwise>
		</xsl:choose> 
		 to all selected line items
	</td>
	<td class="reportGrouping">Total Cost <xsl:value-of select="$discMText" /></td>
	<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//PER_PAT_LINE_ITEM_DISCOUNT),'##,###,###,###,###,##0.00')"/></td>
	<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_COST_DISCOUNT),'##,###,###,###,###,##0.00')"/></td>
</TR>


<xsl:variable name="indirect_flag">
<xsl:value-of select="//BUDGET_INDIRECT_FLAG"/>
</xsl:variable>


<TR style="background-color:white;" >
	<td class="reportGrouping">
		<xsl:attribute name="colspan"><xsl:value-of select="$colsp"/></xsl:attribute>
		Indirects of <xsl:value-of select="format-number(//INDIRECTS,'##0.00')" />% 
		<xsl:choose>
			<xsl:when test="$indirect_flag='Y'">Applied</xsl:when> 
			<xsl:otherwise>Not Applied</xsl:otherwise>
		</xsl:choose> 
		 to selected Total Cost/Patient
	</td>

	<td class="reportGrouping">Total Indirects</td>
	<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//PERPAT_INDIRECT),'##,###,###,###,###,##0.00')"/></td>
	<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_COST_INDIRECT),'##,###,###,###,###,##0.00')"/></td>
</TR>

<xsl:variable name="colsp">
	<xsl:choose>
		<xsl:when test="$budgetTemplate='C'">12</xsl:when>
		<xsl:otherwise>10</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<TR style="background-color:white;" >
	<td class="reportGrouping"> 
		<xsl:attribute name="colspan"><xsl:value-of select="$colsp"/></xsl:attribute>
		<xsl:value-of select="$excludeSOC_flag_text" />
	</td>
</TR>
<tr style="background-color:white;">
	<td><xsl:attribute name="colspan"><xsl:value-of select="$colsp"/></xsl:attribute><br/></td>
</tr>
<tr style="background-color:white;">
	<td><xsl:attribute name="colspan"><xsl:value-of select="$colsp"/></xsl:attribute><br/></td>
</tr>
</tbody>
</TABLE>
</div>
<hr class="thickLine" />
<xsl:if test="$mode='V'">
    <TABLE WIDTH="100%" >
        <TR>
        <TD class="reportFooter" WIDTH="50%" ALIGN="LEFT">Last Modified by: <xsl:value-of select="//LAST_MODIFIED_BY"/></TD>
        <TD class="reportFooter" WIDTH="50%" ALIGN="RIGHT">Date Last Modified: <xsl:value-of select="//LAST_MODIFIED_DATE"/></TD>
        </TR>
        <TR>
        <TD class="reportFooter" WIDTH="50%" ALIGN="LEFT">Report By:<xsl:value-of select="$repBy" /></TD>
        <TD class="reportFooter" WIDTH="50%" ALIGN="RIGHT">Date:<xsl:value-of select="$repDate" /></TD>
        </TR>
    </TABLE>
    <xsl:if test="$ftrflag='1'">
    <TABLE>
        <TR>
        <TD WIDTH="100%" ALIGN="CENTER">
        <img src="{$ftrFileName}"/>
        </TD>
        </TR>
    </TABLE>
    </xsl:if>
</xsl:if>
</xsl:template> 
</xsl:stylesheet>