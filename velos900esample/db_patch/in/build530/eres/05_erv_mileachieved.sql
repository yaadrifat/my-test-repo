CREATE OR REPLACE FORCE VIEW "ERES"."ERV_MILEACHIEVED" ("PK_MILEACHIEVED", "FK_MILESTONE", "FK_PER", "FK_STUDY", "FK_PATPROT", "ACH_DATE", "MILESTONE_TYPE", "MILESTONE_AMOUNT", "PATFACILITYIDS", "MILEDESC", "MILESTONE_COUNT", "FK_SITE", "MILESTONE_PAYTYPE", "FK_ACCOUNT", "STUDY_DIVISION", "STUDY_DIVISION_DESC", "FK_CODELST_TAREA", "FK_CODELST_TAREA_DESC", "FK_CODELST_RESTYPE", "FK_CODELST_RESTYPE_DESC", "STUDY_SPONSOR", "STUDY_PRINV", "STUDY_PRINV_NAME", "PK_STUDYSTAT", "STUDY_NUMBER") AS 
  SELECT pk_MILEACHIEVED, fk_milestone,NVL(fk_per,0) fk_per,a.fk_study,NVL(fk_patprot,0)fk_patprot,ach_date,milestone_type,
DECODE(milestone_count,0,milestone_amount,1,milestone_amount,ABS(TRUNC(milestone_amount/milestone_count,2) )) milestone_amount,
DECODE(milestone_type,'SM','',NVL(pkg_patient.f_get_patcodes(fk_per),' ')) patfacilityIds,
Pkg_Milestone_New.f_getMilestoneDesc( fK_MILESTONE) mileDesc, milestone_count,
DECODE(milestone_type,'SM',(SELECT fk_site FROM ER_STUDYSTAT WHERE pk_studystat = pkg_studystat.f_getLatestStudyStatusPK(m.fk_study) ),pkg_patient.f_get_enrollingsite(fk_per,a.fk_study)) fk_site,
milestone_paytype,s.fk_account, s.study_division, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = study_division) study_division_desc,
fk_codelst_tarea, (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_tarea) fk_codelst_tarea_desc,
fk_codelst_restype,  (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_restype) fk_codelst_restype_desc,
DECODE(fk_codelst_sponsor,NULL,study_sponsor,( SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_sponsor)) study_sponsor,
study_prinv,usr_lst(study_prinv) study_prinv_name,
pkg_studystat.f_getLatestStudyStatus(m.fk_study) pk_studystat,STUDY_NUMBER
FROM ER_MILEACHIEVED A,ER_MILESTONE m ,ER_STUDY s
WHERE m.pk_milestone = fk_milestone AND fk_codelst_milestone_stat = (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='A') AND pk_study = m.fk_study AND is_complete = 1;