
  CREATE OR REPLACE FORCE VIEW "ERES"."ERV_LIBRARY_SCI" ("PK_SUBCOST_ITEM", "CALENDAR_NAME", "CALENDAR_STATUS", "SUBCOST_ITEM_NAME", "SUBCOST_ITEM_COST", "SUBCOST_ITEM_UNIT", "SUBCOST_ITEM_SEQ", "COST_CATEGORY", "FK_CODELST_COST_TYPE", "FK_ACCOUNT", "VISITS", "RID", "CREATOR", "CREATED_ON", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "IP_ADD") AS 
  SELECT  a.PK_SUBCOST_ITEM,
   b.name as CALENDAR_NAME,
   (select CODELST_DESC from SCH_CODELST where PK_CODELST=b.FK_CODELST_CALSTAT ) AS CALENDAR_STATUS,
   a.SUBCOST_ITEM_NAME,a.SUBCOST_ITEM_COST,
   a.SUBCOST_ITEM_UNIT,
   a.SUBCOST_ITEM_SEQ,
   (select CODELST_DESC from SCH_CODELST where PK_CODELST=a.FK_CODELST_CATEGORY ) AS COST_CATEGORY,
   a.FK_CODELST_COST_TYPE,-- YK 09May2011 Bug# 5821 $ #5818
   b.user_id as FK_ACCOUNT,
   F_GET_VISITS(a.PK_SUBCOST_ITEM) as VISITS,
   a.RID,
   (SELECT USR_FIRSTNAME || ' ' || USR_LASTNAME FROM ER_USER
   WHERE PK_USER = a.CREATOR) CREATOR,
   a.CREATED_ON,
   (SELECT USR_FIRSTNAME || ' ' || USR_LASTNAME FROM ER_USER
   WHERE PK_USER = a.LAST_MODIFIED_BY) LAST_MODIFIED_BY,
   a.LAST_MODIFIED_DATE,
   a.IP_ADD
   FROM
   SCH_SUBCOST_ITEM a, EVENT_DEF b
   where
   EVENT_ID=a.FK_CALENDAR;
 
