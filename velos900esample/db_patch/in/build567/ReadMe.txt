For D-FIN22, 22a, and 22b:

D-FIN22b Configurable columns in the Patient Schedule use ER_OBJECT_SETTINGS. Due to this implementation, D-FIN22 (3.a) was also changed to use ER_OBJECT_SETTINGS instead of resource bundle. Please retest D-FIN22 (3.a). D-FIN22 Req. was updated for this.

The alert messages still use the Resource Bundle; therefore, there could be a potential discrepancy of the display text between the Resource Bundle and ER_OBJECT_SETTINGS. However, we will make this a training issue for the administrator by documenting this point in the How-To document to ensure the consistent use of display text.

By the way, D-FIN22a Req. was also updated per QA request.
