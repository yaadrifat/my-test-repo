Declare 


id_F_def number;
id_W_def number;

gen_id number;

Begin 
 


select pk_codelst into id_F_def from sch_codelst where codelst_type='calStatLib' and CODELST_SUBTYP='F';
select pk_codelst into id_W_def from sch_codelst where codelst_type='calStatLib' and CODELST_SUBTYP='W';


For i in (select EVENT_ID, status from EVENT_DEF where UPPER(EVENT_TYPE)='P' and status is not null)
loop

	if    i.status = 'F' then gen_id:=id_F_def; 
	elsif i.status = 'W' then gen_id:=id_W_def;
	
	end if;

	update EVENT_DEF set fk_codelst_calstat = gen_id where EVENT_ID = i.EVENT_ID;
	

End loop;

commit;
End;
/

    
    