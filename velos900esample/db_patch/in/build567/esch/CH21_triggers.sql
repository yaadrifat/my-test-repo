drop trigger SCH_PORTAL_FORMS_BI;

drop trigger SCH_PORTAL_FORMS_AD;


CREATE OR REPLACE TRIGGER "ESCH"."SCH_PORTAL_FORMS_BI0" BEFORE INSERT ON SCH_PORTAL_FORMS
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
erid NUMBER(10);
usr VARCHAR(2000);
raid NUMBER(10);
insert_data CLOB;

BEGIN

 BEGIN
  usr := getuser(:NEW.creator);
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'SCH_PORTAL_FORMS',erid, 'I',usr);
insert_data:=:NEW.PK_PF ||'|'|| :NEW.FK_PORTAL ||'|'|| :NEW.FK_CALENDAR ||'|'||
:NEW.FK_EVENT ||'|'|| :NEW.FK_FORM
||'|'|| :NEW.RID ||'|'|| :NEW.CREATOR
||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)
||'|'|| :NEW.IP_ADD;
INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);

END;

/


CREATE OR REPLACE TRIGGER "ESCH"."SCH_PORTAL_FORMS_AD0" 
AFTER DELETE
ON SCH_PORTAL_FORMS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_PORTAL_FORMS', :old.rid, 'D');

  deleted_data :=
   :old.PK_PF || '|' ||
   :old.FK_PORTAL || '|' ||
   :old.FK_CALENDAR || '|' ||
   :old.FK_EVENT || '|' ||
   :old.FK_FORM || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.created_on) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   :old.ip_add;

insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end;
/