<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList,java.util.HashMap,org.json.*"%>
<%@page import="java.util.Date"%>
<%@page import="com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<%@page import="com.velos.eres.business.common.StudyDao"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.velos.eres.business.common.CodeDao"%>
<%@page import="com.velos.eres.business.common.CtrlDao"%>
<%@page import="com.velos.eres.business.common.UserSiteDao,com.velos.eres.web.user.UserJB"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id ="usrSite" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />  
<%@ page language = "java" import = "com.velos.eres.business.common.PatientDao"%>

<%
    request.setCharacterEncoding("UTF-8");
    response.setContentType("application/json");
    HttpSession tSession = request.getSession(true);
	String userId="";
    int mandtry = Integer.parseInt(request.getParameter("mandtry"));
  	userId = (String) tSession.getValue("userId");
    JSONObject jsObj = new JSONObject();
    if (!sessionmaint.isValidSession(tSession)) {
        response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
        
        jsObj.put("error", new Integer(-1));
        jsObj.put("errorMsg", MC.M_UsrNot_LoggedIn);/*jsObj.put("errorMsg", "User is not logged in."); *****/
	    out.println(jsObj.toString());
        return;
    }
    String userIdFromSession = (String) tSession.getValue("userId");
	String accId = (String) tSession.getValue("accountId");

    /// Patient Autocomplete Values
    PatientDao pat= new PatientDao();
    pat.getPatientsRows(userIdFromSession,accId);
    ArrayList patNames= pat.getPatNames();
    ArrayList patIds= pat.getPatIds();
    jsObj.put("patIds",patIds);
    jsObj.put("patNames",patNames);
   
    /// Study AutoCompletes values
    StudyDao study= new StudyDao(); //AK:fixed BUG#6714
    study.getStudyAutoRows(userIdFromSession,accId);
    ArrayList stdIds =study.getStudyAutoIds();
	ArrayList stdNums = study.getStudyAutoNames();
	jsObj.put("studyIds",stdIds);
	jsObj.put("studyNames",stdNums);
    //BT:Code added for INV21680
	UserJB user = (UserJB) tSession.getValue("currentUser");
    String accountId = user.getUserAccountId();
	UserSiteDao usd = new UserSiteDao ();
	ArrayList arSiteid = new ArrayList();
	ArrayList arSitedesc = new ArrayList();
	usd = usrSite.getSitesWithViewRight(EJBUtil.stringToNum(accountId),EJBUtil.stringToNum(userId));
	arSiteid = usd.getUserSiteSiteIds();
	arSitedesc = usd.getUserSiteNames();
	arSiteid.add(0,"0");
    arSitedesc.add(0,LC.L_Select_AnOption/*Select an Option*****/);    
    jsObj.put("orgMdtryId",arSiteid);
    jsObj.put("orgMdtry",arSitedesc);
    
	//AK:Removed code for BUG#6714
	CodeDao codeLstSpecType = new CodeDao();
    ArrayList ids = new ArrayList();
    ArrayList descs = new ArrayList();
    ArrayList descsSubTypes = new ArrayList();
    
    codeLstSpecType.getCodeValues("specimen_type");
    ids = codeLstSpecType.getCId();
    descs = codeLstSpecType.getCDesc();   
    ids.add(0,"0");   
    descs.add(0,LC.L_Select_AnOption/*Select an Option*****/);   
    jsObj.put("specTypeId",ids);
    jsObj.put("specType",descs);
    
    codeLstSpecType.resetDao();  
    
    codeLstSpecType.getCodeValues("specimen_stat");
    ids = codeLstSpecType.getCId();
    descs = codeLstSpecType.getCDesc();   
    ids.add(0,"0");
    descs.add(0,LC.L_Select_AnOption/*Select an Option*****/);    
    jsObj.put("specStatId",ids);
    jsObj.put("specStat",descs);
    codeLstSpecType.resetDao();
    codeLstSpecType.getCodeValues("spec_q_unit");
    ids = codeLstSpecType.getCId();
    descs = codeLstSpecType.getCDesc();   
    ids.add(0,"0");
    descs.add(0,LC.L_Select_AnOption/*Select an Option*****/);    
    jsObj.put("specQUnitId",ids);
    jsObj.put("specQUnit",descs);
    
    /// SpecimensID Readonly Flag, Bug#4142 Date:05-June-2012 Ankit  
    CtrlDao ctrl = new CtrlDao();
    String isSpecReadonly = ctrl.getControlValue("specid_readonly");
	isSpecReadonly=(isSpecReadonly==null)?"0":isSpecReadonly;
	jsObj.put("isSpecReadonly",isSpecReadonly);

  
    JSONArray jsColArray = new JSONArray();
    {
    	
    	JSONObject jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "specSeq");
    	jsObjTemp1.put("label",LC.L_SpecSeq/*specSeq*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray.put(jsObjTemp1);
    	
        jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "recNum");
    	jsObjTemp1.put("label",LC.L_Serial/*Serial*****/+" #");
    	jsObjTemp1.put("type", "integer");
    	jsObjTemp1.put("hidden", "true");
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
	   	jsObjTemp1.put("key", "delete");
    	jsObjTemp1.put("label", LC.L_Delete/*Delete*****/);
    	jsObjTemp1.put("action", "delete");
    	jsColArray.put(jsObjTemp1);
    	
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "specimenId");
    	jsObjTemp1.put("label", LC.L_Specimen_Id/*Specimen ID*****/);
    	jsObjTemp1.put("type", "varchar");
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "specType");
    	jsObjTemp1.put("label", LC.L_Specimen_Type/*Specimen Type*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");
    	jsColArray.put(jsObjTemp1);
     
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "specTypeId");
    	jsObjTemp1.put("label",LC.L_SpecTypeID/*specTypeId*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "description");
    	jsObjTemp1.put("label", LC.L_Description/*Description*****/); 
    	jsObjTemp1.put("type", "varchar");  
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "selStudyIds");
    	jsObjTemp1.put("label", LC.L_Study/*Study*****/);
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "tempStudyDesc");
    	jsObjTemp1.put("label",LC.L_Temp_StudyDesc/*Temp Study Desc*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "tempStudyIds");
    	jsObjTemp1.put("label",LC.L_TempStudy_ID/*Temp Study Ids*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray.put(jsObjTemp1);
    	
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patientIds");
    	jsObjTemp1.put("label", LC.L_Patient_Id/*Patient ID*****/);
    	jsColArray.put(jsObjTemp1);
    	
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "tempPatientDesc");
    	jsObjTemp1.put("label",LC.L_Temp_PatientDesc/*Temp Patient Desc*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "tempPatientIds");
    	jsObjTemp1.put("label",LC.L_TempPatient_ID/*Temp Patient Ids*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "specStat");
    	jsObjTemp1.put("label", LC.L_Status/*Status*****/);	
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "specStatId");
    	jsObjTemp1.put("label",LC.L_SpecStatID/*specStatId*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "statDt");
    	jsObjTemp1.put("label", LC.L_Status_Date/*Status Date*****/);
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "tempStatDt");
    	jsObjTemp1.put("label",LC.L_TempStatus_Dt/*Temp Status Date*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "quantity");
    	jsObjTemp1.put("label", LC.L_Quantity/*Quantity*****/);
    	jsObjTemp1.put("type", "number");
    	jsObjTemp1.put("formatter", "customNumber");
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "unit");
    	jsObjTemp1.put("label",LC.L_Unit/*Unit*****/); 	
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "unitId");
    	jsObjTemp1.put("label",LC.L_UnitID/*UnitId*****/); 
    	jsObjTemp1.put("hidden", "true");
    	jsColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "altId");
    	jsObjTemp1.put("label",LC.L_Alternate_Id/*Alternate ID*****/);
    	jsObjTemp1.put("type", "varchar");
    	jsColArray.put(jsObjTemp1);
		
		//BT:Code added for INV21680
		jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "orgMdtry");
        if(mandtry==1){jsObjTemp1.put("label",LC.L_Organization/*Alternate ID*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");}
	    else{jsObjTemp1.put("label",LC.L_Organization);}
    	jsColArray.put(jsObjTemp1);
    	
		jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "orgMdtryId");
    	jsObjTemp1.put("label","OrganizationID"); 
    	jsObjTemp1.put("hidden", "true");
    	jsColArray.put(jsObjTemp1);
    }
    jsObj.put("colArray", jsColArray);
    JSONArray jsParentColArray = new JSONArray();
    {
    	
    	JSONObject jsObjTemp1 = new JSONObject();  	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "specType");
    	jsObjTemp1.put("label",LC.L_Specimen_Type/*Specimen Type*****/);//AK:Fixed BUG#6795(10thAug11)
    	jsParentColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();    
    	jsObjTemp1.put("key", "specTypeId");
    	jsObjTemp1.put("label", LC.L_SpecTypeID/*specTypeId*****/);
    	jsObjTemp1.put("hidden", "true");
    	jsParentColArray.put(jsObjTemp1); //AK:Fixed BUG#6676
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "description");
    	jsObjTemp1.put("label", LC.L_Description/*Description*****/); 
    	jsObjTemp1.put("type", "varchar");
    	jsParentColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "selStudyIds");
    	jsObjTemp1.put("label",LC.L_Study/*Study*****/);
    	jsParentColArray.put(jsObjTemp1);
    	    	
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "patientIds");
    	jsObjTemp1.put("label", LC.L_Patient_Id/*Patient ID*****/);
    	jsParentColArray.put(jsObjTemp1);
    	
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "specStat");
    	jsObjTemp1.put("label", LC.L_Status/*Status*****/);	
    	jsParentColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "statDt");
    	jsObjTemp1.put("label", LC.L_Status_Date/*Status Date*****/);
    	jsParentColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "specStatId");
    	jsObjTemp1.put("label", LC.L_SpecStatID/*specStatId*****/);
        jsObjTemp1.put("hidden", "true");
    	jsParentColArray.put(jsObjTemp1);
    	 	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "quantity");
    	jsObjTemp1.put("label", LC.L_Quantity/*Quantity*****/);
    	jsObjTemp1.put("type", "number");
    	jsObjTemp1.put("formatter", "customNumber");
    	jsParentColArray.put(jsObjTemp1);
    	
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "unit");
    	jsObjTemp1.put("label",LC.L_Unit/*Unit*****/); 	
    	jsParentColArray.put(jsObjTemp1);
		
		//BT:Code added for INV21680
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "orgMdtry");
    	if(mandtry==1){jsObjTemp1.put("label",LC.L_Organization/*Alternate ID*****/+" <FONT class='mandatory'><sup>*</sup></FONT>");}
	    else{jsObjTemp1.put("label",LC.L_Organization);}
    	jsParentColArray.put(jsObjTemp1);
		
		jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "orgMdtryId");
    	jsObjTemp1.put("label","OrganizationID"); 
    	jsObjTemp1.put("hidden", "true");
    	jsColArray.put(jsObjTemp1);
		
    	jsObjTemp1 = new JSONObject();
    	jsObjTemp1.put("key", "unitId");
    	jsObjTemp1.put("label", LC.L_UnitID/*UnitId*****/); 
    	jsObjTemp1.put("hidden", "true");
    	jsParentColArray.put(jsObjTemp1);

    }
    jsObj.put("parentColArray", jsParentColArray);
    ArrayList dataList = new ArrayList();
    JSONArray jsSpecimens = new JSONArray();
    int childRecNum=0;
    for (int iX=0; iX<dataList.size(); iX++) {
    	jsSpecimens.put(EJBUtil.hashMapToJSON((HashMap)dataList.get(iX)));
    }
    jsObj.put("childrecCount",childRecNum);
    
    JSONArray jsParSpecimens = new JSONArray();

    JSONObject jObj = new JSONObject();
    jObj.put("specType","");
    jObj.put("specTypeId","0");
    jObj.put("description","");
    jObj.put("selStudyIds","");
    jObj.put("patientIds","");
    jObj.put("specStat","");
    jObj.put("specStatId","0");
    jObj.put("quantity","");
    jObj.put("unit","");
	jObj.put("orgMdtry","");
	jObj.put("orgMdtryId","0");
    jObj.put("unitId","0");
    jsParSpecimens.put(jObj);
    
    jsObj.put("parentDataArray", jsParSpecimens);
    jsObj.put("dataArray", jsSpecimens);
     
    out.println(jsObj.toString());
 
%>
