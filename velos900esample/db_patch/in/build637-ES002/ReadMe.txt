/* This readMe is specific to eSample (version 9.0.0 build #637-ES002) */

=====================================================================================================================================
eSample requirements covered in the build:
	INV 21680

Following existing files have been modified:

	1. fetchMultiSpecimensJSON.jsp 		(...\deploy\velos.ear\velos.war\jsp)
	2. addmultiplespecimens.jsp			(...\deploy\velos.ear\velos.war\jsp)
	3. LC.jsp							(...\deploy\velos.ear\velos.war\jsp)
	4. specimendetails.jsp				(...\deploy\velos.ear\velos.war\jsp)
	5. updatemultiplespecimens.jsp		(...\deploy\velos.ear\velos.war\jsp)
	6. multiSpecimens.js				(...\deploy\velos.ear\velos.war\jsp\js\velos)
=====================================================================================================================================