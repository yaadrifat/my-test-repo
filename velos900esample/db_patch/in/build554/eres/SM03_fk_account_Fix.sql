
Declare
	lookupID INTEGER DEFAULT 0;
BEGIN

SELECT PK_LKPLIB INTO lookupID
FROM ER_LKPLIB 
WHERE LKPTYPE_NAME = 'dynReports' AND LKPTYPE_DESC = 'Study Summary';

Update ER_LKPVIEW set LKPVIEW_FILTER ='erv_study_dyn.fk_account= [:ACCID] and (pk_study in (select FK_STUDY from er_studyteam where fk_user = [:USERID] and study_team_usr_type <> ''X'' ) OR  pkg_superuser.F_Is_Superuser([:USERID], pk_study) = 1 )'
where FK_LKPLIB = lookupID AND LKPVIEW_NAME='Study Lookup';

commit;

END;
/
