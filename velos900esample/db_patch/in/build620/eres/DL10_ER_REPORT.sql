set define off;

-- INSERTING into ER_REPORT


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_report
    where pk_report = 167;
  if (v_record_exists = 0) then
	Insert into ER_REPORT (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) 
	values (167,'Detail CBU Report','Detail CBU Report','',0,null,null,'Y',null,null,1,'rep_cbu','',null,null);
	COMMIT;	
	
	Update ER_REPORT set REP_SQL = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
idnotes, cbbid, storage_loc, cbu_collection_site, cbu_info_notes, eligible_status, eligibility_notes,
prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, lab_sum_notes, proc_name, proc_start_date, proc_termi_date, processing, automated_type,
other_processing, product_modification, other_product_modi, storage_method, freezer_manufact, other_freezer_manufact,
frozen_in, other_frozen_cont, no_of_bags, cryobag_manufac, bagtype, bag1type, bag2type,   other_bag,
storage_temperature, max_vol, ctrl_rate_freezing, individual_frac,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,
no_of_oth_rep_alliquots_f_prod, no_of_oth_rep_alliq_alter_cond, no_of_serum_mater_aliquots, no_of_plasma_mater_aliquots,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots  FROM rep_cbu_details where pk_cord = ~1' where pk_report = 167;
	COMMIT;

	Update ER_REPORT set REP_SQL_CLOB = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
idnotes, cbbid, storage_loc, cbu_collection_site, cbu_info_notes, eligible_status, eligibility_notes,
prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, lab_sum_notes, proc_name, proc_start_date, proc_termi_date, processing, automated_type,
other_processing, product_modification, other_product_modi, storage_method, freezer_manufact, other_freezer_manufact,
frozen_in, other_frozen_cont, no_of_bags, cryobag_manufac, bagtype, bag1type, bag2type,   other_bag,
storage_temperature, max_vol, ctrl_rate_freezing, individual_frac,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,
no_of_oth_rep_alliquots_f_prod, no_of_oth_rep_alliq_alter_cond, no_of_serum_mater_aliquots, no_of_plasma_mater_aliquots,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots  FROM rep_cbu_details where pk_cord = ~1' where pk_report = 167;
	COMMIT;

  end if;
end;
/

Update ER_REPORT set REP_SQL = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
idnotes, cbbid, storage_loc, cbu_collection_site, cbu_info_notes_visible_tc, eligible_status, eligibility_notes_visible_tc,
prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, lab_sum_notes_visible_tc, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,
no_of_oth_rep_alliquots_f_prod, no_of_oth_rep_alliq_alter_cond, no_of_serum_mater_aliquots, no_of_plasma_mater_aliquots,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots FROM rep_cbu_details where pk_cord = ~1' where pk_report = 161;

COMMIT;

Update ER_REPORT set REP_SQL_CLOB = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
idnotes, cbbid, storage_loc, cbu_collection_site, cbu_info_notes_visible_tc, eligible_status, eligibility_notes_visible_tc,
prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, lab_sum_notes_visible_tc, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,
no_of_oth_rep_alliquots_f_prod, no_of_oth_rep_alliq_alter_cond, no_of_serum_mater_aliquots, no_of_plasma_mater_aliquots,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots FROM rep_cbu_details where pk_cord = ~1' where pk_report = 161;

COMMIT;