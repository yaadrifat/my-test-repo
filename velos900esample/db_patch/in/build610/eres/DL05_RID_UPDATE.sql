


set define off;
DECLARE
  COUNTFLAG NUMBER(5);  
BEGIN
    SELECT COUNT(1) INTO COUNTFLAG FROM  ER_ATTACHMENTS WHERE RID IS NULL;    
    IF (COUNTFLAG > 0) THEN
     update ER_ATTACHMENTS  set RID=SEQ_RID.NEXTVAL WHERE RID IS NULL;       
commit;
end if;
END;
/


set define off;
DECLARE
  COUNTFLAG NUMBER(5);  
BEGIN
    SELECT COUNT(1) INTO COUNTFLAG FROM  CB_UPLOAD_INFO WHERE RID IS NULL;    
    IF (COUNTFLAG > 0) THEN
     update CB_UPLOAD_INFO  set RID=SEQ_RID.NEXTVAL WHERE RID IS NULL;       
commit;
end if;
END;
/

set define off;
DECLARE
  COUNTFLAG NUMBER(5);  
BEGIN
    SELECT COUNT(1) INTO COUNTFLAG FROM  CBB_PROCESSING_PROCEDURES_INFO WHERE RID IS NULL;    
    IF (COUNTFLAG > 0) THEN
     update CBB_PROCESSING_PROCEDURES_INFO  set RID=SEQ_RID.NEXTVAL WHERE RID IS NULL;       
commit;
end if;
END;
/

set define off;
DECLARE
  COUNTFLAG NUMBER(5);  
BEGIN
    SELECT COUNT(1) INTO COUNTFLAG FROM  CBB_PROCESSING_PROCEDURES WHERE RID IS NULL;    
    IF (COUNTFLAG > 0) THEN
     update CBB_PROCESSING_PROCEDURES  set RID=SEQ_RID.NEXTVAL WHERE RID IS NULL;       
commit;
end if;
END;
/
