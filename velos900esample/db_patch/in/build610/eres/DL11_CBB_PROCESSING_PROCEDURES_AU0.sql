CREATE OR REPLACE TRIGGER "ERES".CBB_PROCESSING_PROCEDURES_AU0 AFTER  UPDATE OF
PK_PROC,
CREATOR,
CREATED_ON,
IP_ADD,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
PROC_NO,
PROC_START_DATE,
PROC_TERMI_DATE,
PROC_VERSION,
RID,
FK_PROCESSING_ID,
FK_SITE ON CBB_PROCESSING_PROCEDURES   FOR EACH ROW 
DECLARE 
raid NUMBER(10);
  usr                                         VARCHAR2(100);
  old_modby                                   VARCHAR2(100);
  new_modby                                   VARCHAR2(100);
  BEGIN
    SELECT seq_audit.nextval INTO raid FROM dual;
    usr := getuser(:new.last_modified_by);
    audit_trail.record_transaction (raid, 'CBB_PROCESSING_PROCEDURES', :old.rid, 'U', usr);
IF NVL(:old.PK_PROC,0) != NVL(:new.PK_PROC,0) THEN
      audit_trail.column_update (raid, 'PK_PROC', :OLD.PK_PROC, :NEW.PK_PROC);
    END IF;
IF NVL(:old.CREATOR,0) != NVL(:new.CREATOR,0) THEN
      audit_trail.column_update (raid, 'CREATOR', :OLD.CREATOR, :NEW.CREATOR);
    END IF;
	IF NVL(:old.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'CREATED_ON', TO_CHAR(:old.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:new.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
 IF NVL(:old.ip_add,' ') != NVL(:new.ip_add,' ') THEN
      audit_trail.column_update (raid, 'IP_ADD', :old.ip_add, :new.ip_add);
    END IF;
 IF NVL(:old.LAST_MODIFIED_BY,0) != NVL(:new.LAST_MODIFIED_BY,0) THEN
      BEGIN
        SELECT TO_CHAR(pk_user)
          || ','
          || usr_lastname
          ||', '
          || usr_firstname
        INTO old_modby
        FROM er_user
        WHERE pk_user = :old.last_modified_by ;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        old_modby := NULL;
      END ;
      BEGIN
        SELECT TO_CHAR(pk_user)
          || ','
          || usr_lastname
          ||', '
          || usr_firstname
        INTO new_modby
        FROM er_user
        WHERE pk_user = :new.LAST_MODIFIED_BY ;
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        new_modby := NULL;
      END ;
      audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modby, new_modby);
    END IF;
	IF NVL(:old.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
	 IF NVL(:old.PROC_NO,0.0) != NVL(:new.PROC_NO,0.0) THEN
      audit_trail.column_update (raid, 'PROC_NO', :OLD.PROC_NO, :NEW.PROC_NO);
    END IF;
	 IF NVL(:old.PROC_START_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.PROC_START_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'PROC_START_DATE', TO_CHAR(:OLD.PROC_START_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.PROC_START_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
 IF NVL(:old.PROC_TERMI_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.PROC_TERMI_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update (raid, 'PROC_TERMI_DATE', TO_CHAR(:OLD.PROC_TERMI_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.PROC_TERMI_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
	  IF NVL(:old.PROC_VERSION,' ') != NVL(:new.PROC_VERSION,' ') THEN
      audit_trail.column_update (raid, 'PROC_VERSION', :OLD.PROC_VERSION, :NEW.PROC_VERSION);
    END IF;
 IF NVL(:old.rid,0) != NVL(:new.rid,0) THEN
      audit_trail.column_update (raid, 'RID', :OLD.rid, :NEW.rid);
    END IF;
	IF NVL(:old.FK_PROCESSING_ID,0) != NVL(:new.FK_PROCESSING_ID,0) THEN
      audit_trail.column_update (raid, 'FK_PROCESSING_ID', :OLD.FK_PROCESSING_ID, :NEW.FK_PROCESSING_ID);
    END IF;
IF NVL(:old.FK_SITE,0) != NVL(:new.FK_SITE,0) THEN
      audit_trail.column_update (raid, 'FK_SITE', :OLD.FK_SITE, :NEW.FK_SITE);
    END IF;
END;
/