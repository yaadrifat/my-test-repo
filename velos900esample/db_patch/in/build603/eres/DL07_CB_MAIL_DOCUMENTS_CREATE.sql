set define off;

Create table CB_MAIL_DOCUMENTS
(
PK_MAIL_DOCUMENTS NUMBER(10) Primary Key,
DOCUMENT_FILE NUMBER(10),
DOCUMENT_CONTENTTYPE	VARCHAR2(50 BYTE),
DOCUMENT_FILENAME	VARCHAR2(100 BYTE),
ATTACHMENT_TYPE VARCHAR2(50 BYTE),
ENTITY_ID number(10),
ENTITY_TYPE varchar2(50 BYTE)
);
COMMENT ON TABLE "CB_MAIL_DOCUMENTS" IS 'Table to EMail ATTACHEMNTS';
COMMENT ON COLUMN "CB_MAIL_DOCUMENTS"."PK_MAIL_DOCUMENTS" IS 'Primary key of the table';
COMMENT ON COLUMN "CB_MAIL_DOCUMENTS"."DOCUMENT_FILE" IS 'This column store the content of the email attachment';
COMMENT ON COLUMN "CB_MAIL_DOCUMENTS"."DOCUMENT_CONTENTTYPE" IS 'This column store the content type of the email attachment';
COMMENT ON COLUMN "CB_MAIL_DOCUMENTS"."DOCUMENT_FILENAME" IS 'This column store the name of the email attachment';
COMMENT ON COLUMN "CB_MAIL_DOCUMENTS"."ATTACHMENT_TYPE" IS 'This column store the type of the email attachment';
COMMENT ON COLUMN "CB_MAIL_DOCUMENTS"."ENTITY_ID" IS 'This column store the entity id related to the email attachment';
COMMENT ON COLUMN "CB_MAIL_DOCUMENTS"."ENTITY_TYPE" IS 'This column store the entity type related to the email attachment';
CREATE SEQUENCE SEQ_CB_MAIL_DOCUMENTS MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;

commit;