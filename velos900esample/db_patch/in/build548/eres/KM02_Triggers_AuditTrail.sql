set define off;

create or replace
TRIGGER ER_ACCOUNT_AU0 AFTER UPDATE ON ER_ACCOUNT FOR EACH ROW
declare
  raid number(10);
  old_modby varchar2(100) ;
  new_modby varchar2(100) ;
  usr varchar2(100);

begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(:new.last_modified_by);
  
  if (:new.last_modified_by is not null) then
  audit_trail.record_transaction
    (raid, 'ER_ACCOUNT', :old.rid, 'U', usr);
  end if;  
  if nvl(:old.pk_account,0) !=
     NVL(:new.pk_account,0) then
     audit_trail.column_update
       (raid, 'PK_ACCOUNT',
       :old.pk_account, :new.pk_account);
  end if;
  if nvl(:old.ac_type,' ') !=
     NVL(:new.ac_type,' ') then
     audit_trail.column_update
       (raid, 'AC_TYPE',
       :old.ac_type, :new.ac_type);
  end if;
  if nvl(:old.ac_name,' ') !=
     NVL(:new.ac_name,' ') then
     audit_trail.column_update
       (raid, 'AC_NAME',
       :old.ac_name, :new.ac_name);
  end if;
  if nvl(:old.ac_strtdt,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.ac_strtdt,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'AC_STRTDT',
       to_char(:old.ac_strtdt, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ac_strtdt, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ac_endt,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.ac_endt,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'AC_ENDT',
       to_char(:old.ac_endt, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.ac_endt, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ac_stat,' ') !=
     NVL(:new.ac_stat,' ') then
     audit_trail.column_update
       (raid, 'AC_STAT',
       :old.ac_stat, :new.ac_stat);
  end if;
  if nvl(:old.ac_mailflag,' ') !=
     NVL(:new.ac_mailflag,' ') then
     audit_trail.column_update
       (raid, 'AC_MAILFLAG',
       :old.ac_mailflag, :new.ac_mailflag);
  end if;
  if nvl(:old.ac_pubflag,' ') !=
     NVL(:new.ac_pubflag,' ') then
     audit_trail.column_update
       (raid, 'AC_PUBFLAG',
       :old.ac_pubflag, :new.ac_pubflag);
  end if;
  if nvl(:old.ac_note,' ') !=
     NVL(:new.ac_note,' ') then
     audit_trail.column_update
       (raid, 'AC_NOTE',
       :old.ac_note, :new.ac_note);
  end if;
  if nvl(:old.ac_usrcreator,0) !=
     NVL(:new.ac_usrcreator,0) then
     audit_trail.column_update
       (raid, 'AC_USRCREATOR',
       :old.ac_usrcreator, :new.ac_usrcreator);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
      Begin
       select to_char(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
       into old_modby
       from er_user
       where pk_user = :old.last_modified_by ;
       Exception When no_data_found then
        old_modby := NULL;
      End ;
    Begin
     select to_char(pk_user) ||', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.last_modified_by ;
       Exception When no_data_found then
        new_modby := NULL;
    End ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  end if;
  if nvl(:old.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:old.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.created_on, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.FK_CODELST_ROLE,0) !=
     NVL(:new.FK_CODELST_ROLE,0) then
     audit_trail.column_update
       (raid, 'FK_CODELST_ROLE',
       :old.FK_CODELST_ROLE, :new.FK_CODELST_ROLE);
  end if;
  if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD, ' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
  end if;

 --KM-032108
 if nvl(:old.AC_AUTOGEN_STUDY,0) !=
     NVL(:new.AC_AUTOGEN_STUDY,0) then
     audit_trail.column_update
       (raid, 'AC_AUTOGEN_STUDY',
       :old.AC_AUTOGEN_STUDY, :new.AC_AUTOGEN_STUDY);
  end if;

  if nvl(:old.AC_AUTOGEN_PATIENT,0) !=
     NVL(:new.AC_AUTOGEN_PATIENT,0) then
     audit_trail.column_update
       (raid, 'AC_AUTOGEN_PATIENT',
       :old.AC_AUTOGEN_PATIENT, :new.AC_AUTOGEN_PATIENT);
  end if;


end;
/

create or replace TRIGGER "ERES"."ER_MILEAPNDX_BI0" BEFORE INSERT ON ER_MILEAPNDX
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW   WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  erid NUMBER(10);
  raid NUMBER(10);
  insert_data CLOB;
  usr VARCHAR(2000);
     BEGIN

                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  --KM-#3634
 usr := getuser(:new.creator);
audit_trail.record_transaction(raid, 'ER_MILEAPNDX',erid, 'I',  usr);
       insert_data:= :NEW.PK_MILEAPNDX||'|'|| :NEW.FK_STUDY||'|'||
     :NEW.FK_MILESTONE||'|'||:NEW.MILEAPNDX_DESC||'|'||:NEW.MILEAPNDX_URI||'|'||
  :NEW.MILEAPNDX_TYPE||'|'|| :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
   TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
   TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD;

 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);

   END;
/

create or replace TRIGGER "ERES"."ER_MILEPAYMENT_BI0" BEFORE INSERT ON ER_MILEPAYMENT
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW    WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  erid NUMBER(10);
  raid NUMBER(10);
  insert_data CLOB;
  usr varchar(2000);
     BEGIN
   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:new.creator);
  Audit_Trail.record_transaction  (raid, 'ER_MILEPAYMENT', erid, 'I', usr );
  insert_data:= :NEW.PK_MILEPAYMENT||'|'|| :NEW.FK_STUDY||'|'||:NEW.FK_MILESTONE||'|'|| TO_CHAR(:NEW.MILEPAYMENT_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.MILEPAYMENT_AMT||'|'||:NEW.MILEPAYMENT_DESC||'|'||:NEW.MILEPAYMENT_DELFLAG||'|'||:NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat) ||'|'||  :NEW.IP_ADD ||'|'||  :NEW.milepayment_comments ||'|'||  :NEW.milepayment_type;
 INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
   END ;
/

create or replace TRIGGER "ERES".ER_MILESTONE_BI0 BEFORE INSERT ON ER_MILESTONE
FOR EACH ROW
WHEN (NEW.rid IS NULL OR NEW.rid = 0)
DECLARE
 erid NUMBER(10);
 raid NUMBER(10);
 insert_data CLOB;
 usr varchar2(2000);
     BEGIN

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:new.creator);
  Audit_Trail.record_transaction    (raid, 'ER_MILESTONE', erid, 'I', usr );
  -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.FK_VISIT||'|'|| :NEW.PK_MILESTONE||'|'||
    :NEW.FK_STUDY||'|'||:NEW.MILESTONE_TYPE||'|'|| :NEW.FK_CAL||'|'||
    :NEW.FK_CODELST_RULE||'|'|| :NEW.MILESTONE_AMOUNT||'|'|| :NEW.MILESTONE_VISIT||'|'||
    :NEW.FK_EVENTASSOC||'|'|| :NEW.MILESTONE_COUNT||'|'||:NEW.MILESTONE_DELFLAG||'|'||
    :NEW.MILESTONE_USERSTO||'|'|| :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
  TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
  TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD   ||'|'||:NEW.milestone_limit ||'|'||:NEW.milestone_status
  ||'|'||:NEW.milestone_paytype ||'|'||:NEW.milestone_paydueby ||'|'||:NEW.milestone_paybyunit   ||'|'||:NEW.milestone_payfor ||'|'||:NEW.milestone_eventstatus
  ||'|'||:NEW.milestone_achievedcount
  ||'|'||TO_CHAR(:NEW.last_checked_on ,PKG_DATEUTIL.f_get_dateformat) ||'|'||:NEW.milestone_description
  ||'|'|| :NEW.fk_codelst_milestone_stat; --KM


 INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);

 END ;
/

create or replace TRIGGER "ER_SPECIMEN_APPNDX_BIO" BEFORE INSERT ON ER_SPECIMEN_APPNDX
FOR EACH ROW
 WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  insert_data CLOB;
  usr varchar2(2000);
BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  --KM-#3634
  usr := getuser(:new.creator);
  Audit_Trail.record_transaction
    (raid, 'ER_SPECIMEN_APPNDX', erid, 'I', usr );

insert_data:= :NEW.PK_SPECIMEN_APPNDX||'|'||
	      :NEW.FK_SPECIMEN||'|'|| :NEW.SA_DESC||'|'||
              :NEW.SA_URI||'|'||:NEW.SA_TYPE||'|'||
	      :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
	      TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
              TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD;
              INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
END;
/

create or replace TRIGGER ER_USER_BI0 BEFORE INSERT ON ER_USER
FOR EACH ROW
WHEN (NEW.rid IS NULL OR NEW.rid = 0)
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  usr VARCHAR2(2000);
  insert_data CLOB;
BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  usr := getuser(:new.creator);
  audit_trail.record_transaction
    (raid, 'ER_USER', erid, 'I', usr );

--Added by Ganapathy on 24-06-05 for Audit Inserting

insert_data:= :NEW.FK_TIMEZONE||'|'|| :NEW.USR_ATTEMPTCNT||'|'|| :NEW.USR_LOGGEDINFLAG||'|'||
:NEW.USR_SITEFLAG||'|'||:NEW.USR_TYPE||'|'|| :NEW.PK_USER||'|'|| :NEW.FK_CODELST_JOBTYPE||'|'||
 :NEW.FK_ACCOUNT||'|'|| :NEW.FK_SITEID||'|'||:NEW.USR_LASTNAME||'|'||:NEW.USR_FIRSTNAME||'|'||
 :NEW.USR_MIDNAME||'|'||:NEW.USR_WRKEXP||'|'||:NEW.USR_PAHSEINV||'|'|| :NEW.USR_SESSTIME||'|'||
 :NEW.USR_LOGNAME||'|'||:NEW.USR_PWD||'|'||:NEW.USR_SECQUES||'|'||:NEW.USR_ANS||'|'||
 :NEW.USR_STAT||'|'||:NEW.FK_CODELST_SPL||'|'|| :NEW.RID||'|'|| :NEW.CREATOR||'|'||
  :NEW.FK_GRP_DEFAULT||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
   TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
    :NEW.FK_PERADD||'|'||:NEW.USER_CODE||'|'||:NEW.USR_CODE||'|'||
 TO_CHAR(:NEW.USR_PWDXPIRY,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| :NEW.USR_PWDDAYS||'|'||
 TO_CHAR(:NEW.USR_PWDREMIND,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.USR_ES||'|'|| TO_CHAR(:NEW.USR_ESXPIRY,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
 :NEW.USR_ESDAYS||'|'|| TO_CHAR(:NEW.USR_ESREMIND,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD||'|'||:NEW.USER_HIDDEN;--KM

 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/

create or replace TRIGGER ER_MILEPAYMENT_AU0
AFTER UPDATE OF CREATED_ON,CREATOR,FK_MILESTONE,FK_STUDY,IP_ADD,MILEPAYMENT_AMT,MILEPAYMENT_DATE,MILEPAYMENT_DELFLAG,MILEPAYMENT_DESC,PK_MILEPAYMENT,RID
ON ER_MILEPAYMENT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr varchar2(2000);
BEGIN
  --KM-#3634
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.LAST_MODIFIED_BY);
  Audit_Trail.record_transaction
    (raid, 'ER_MILEPAYMENT', :OLD.rid, 'U', usr);
  IF NVL(:OLD.pk_milepayment,0) !=
     NVL(:NEW.pk_milepayment,0) THEN
     Audit_Trail.column_update
       (raid, 'PK_MILEPAYMENT',
       :OLD.pk_milepayment, :NEW.pk_milepayment);
  END IF;
  IF NVL(:OLD.fk_study,0) !=
     NVL(:NEW.fk_study,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_STUDY',
       :OLD.fk_study, :NEW.fk_study);
  END IF;
  IF NVL(:OLD.fk_milestone,0) !=
     NVL(:NEW.fk_milestone,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_MILESTONE',
       :OLD.fk_milestone, :NEW.fk_milestone);
  END IF;
  IF NVL(:OLD.milepayment_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.milepayment_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     Audit_Trail.column_update
       (raid, 'MILEPAYMENT_DATE',
       to_char(:OLD.milepayment_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.milepayment_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.milepayment_amt,0) !=
     NVL(:NEW.milepayment_amt,0) THEN
     Audit_Trail.column_update
       (raid, 'MILEPAYMENT_AMT',
       :OLD.milepayment_amt, :NEW.milepayment_amt);
  END IF;
  IF NVL(:OLD.milepayment_desc,' ') !=
     NVL(:NEW.milepayment_desc,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILEPAYMENT_DESC',
       :OLD.milepayment_desc, :NEW.milepayment_desc);
  END IF;
  IF NVL(:OLD.milepayment_delflag,' ') !=
     NVL(:NEW.milepayment_delflag,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILEPAYMENT_DELFLAG',
       :OLD.milepayment_delflag, :NEW.milepayment_delflag);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     Audit_Trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     Audit_Trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     Audit_Trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     Audit_Trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
   IF NVL(:OLD.milepayment_comments,' ') !=
     NVL(:NEW.milepayment_comments,' ') THEN
     Audit_Trail.column_update
       (raid, 'milepayment_comments',
       :OLD.milepayment_comments, :NEW.milepayment_comments);
  END IF;
  IF NVL(:OLD.milepayment_type,0) !=
     NVL(:NEW.milepayment_type,0) THEN
     Audit_Trail.column_update
       (raid, 'milepayment_type',
       :OLD.milepayment_type, :NEW.milepayment_type);
  END IF;

END;
/

create or replace TRIGGER ER_MILESTONE_AU0 AFTER UPDATE OF RID,FK_CAL,IP_ADD,CREATOR,FK_STUDY,CREATED_ON,PK_MILESTONE,FK_EVENTASSOC,MILESTONE_TYPE,FK_CODELST_RULE,MILESTONE_COUNT,MILESTONE_LIMIT,MILESTONE_VISIT,MILESTONE_AMOUNT,MILESTONE_PAYFOR,MILESTONE_STATUS,MILESTONE_DELFLAG,MILESTONE_PAYTYPE,MILESTONE_USERSTO,MILESTONE_PAYDUEBY,MILESTONE_PAYBYUNIT,MILESTONE_EVENTSTATUS ON ERES.ER_MILESTONE FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr varchar2(2000);
BEGIN
  --KM-#3634
  usr := getuser(:new.last_modified_by);
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  
  if (:new.last_modified_by is not null) then
  Audit_Trail.record_transaction
    (raid, 'ER_MILESTONE', :OLD.rid, 'U', usr);
  end if;
  
  IF NVL(:OLD.pk_milestone,0) !=
     NVL(:NEW.pk_milestone,0) THEN
     Audit_Trail.column_update
       (raid, 'PK_MILESTONE',
       :OLD.pk_milestone, :NEW.pk_milestone);
  END IF;
  IF NVL(:OLD.fk_study,0) !=
     NVL(:NEW.fk_study,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_STUDY',
       :OLD.fk_study, :NEW.fk_study);
  END IF;
  IF NVL(:OLD.milestone_type,' ') !=
     NVL(:NEW.milestone_type,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_TYPE',
       :OLD.milestone_type, :NEW.milestone_type);
  END IF;
  IF NVL(:OLD.fk_cal,0) !=
     NVL(:NEW.fk_cal,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_CAL',
       :OLD.fk_cal, :NEW.fk_cal);
  END IF;
  IF NVL(:OLD.fk_codelst_rule,0) !=
     NVL(:NEW.fk_codelst_rule,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_CODELST_RULE',
       :OLD.fk_codelst_rule, :NEW.fk_codelst_rule);
  END IF;
  IF NVL(:OLD.milestone_amount,0) !=
     NVL(:NEW.milestone_amount,0) THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_AMOUNT',
       :OLD.milestone_amount, :NEW.milestone_amount);
  END IF;
  IF NVL(:OLD.milestone_visit,0) !=
     NVL(:NEW.milestone_visit,0) THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_VISIT',
       :OLD.milestone_visit, :NEW.milestone_visit);
  END IF;
  IF NVL(:OLD.fk_eventassoc,0) !=
     NVL(:NEW.fk_eventassoc,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_EVENTASSOC',
       :OLD.fk_eventassoc, :NEW.fk_eventassoc);
  END IF;
  IF NVL(:OLD.milestone_count,0) !=
     NVL(:NEW.milestone_count,0) THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_COUNT',
       :OLD.milestone_count, :NEW.milestone_count);
  END IF;
  IF NVL(:OLD.milestone_delflag,' ') !=
     NVL(:NEW.milestone_delflag,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_DELFLAG',
       :OLD.milestone_delflag, :NEW.milestone_delflag);
  END IF;
  IF NVL(:OLD.milestone_usersto,' ') !=
     NVL(:NEW.milestone_usersto,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_USERSTO',
       :OLD.milestone_usersto, :NEW.milestone_usersto);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     Audit_Trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
     Audit_Trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :OLD.last_modified_by, :NEW.last_modified_by);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
     Audit_Trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
      to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     Audit_Trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;

  IF NVL(:OLD.milestone_limit ,0) !=
     NVL(:NEW.milestone_limit ,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_limit',
       :OLD.milestone_limit , :NEW.milestone_limit );
  END IF;

  IF NVL(:OLD.milestone_status,0) !=
     NVL(:NEW.milestone_status ,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_status',
       :OLD.milestone_status , :NEW.milestone_status);
  END IF;
  IF NVL(:OLD.milestone_paytype,0) !=
     NVL(:NEW.milestone_paytype,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_paytype',
       :OLD.milestone_paytype, :NEW.milestone_paytype);
  END IF;
   IF NVL(:OLD.milestone_paydueby,0) !=
     NVL(:NEW.milestone_paydueby,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_paydueby',
       :OLD.milestone_paydueby, :NEW.milestone_paydueby);
  END IF;

  IF NVL(:OLD.milestone_paybyunit,' ') !=
     NVL(:NEW.milestone_paybyunit,' ') THEN
     Audit_Trail.column_update
       (raid, 'milestone_paybyunit',
       :OLD.milestone_paybyunit, :NEW.milestone_paybyunit);
  END IF;
   IF NVL(:OLD.milestone_payfor,0) !=
     NVL(:NEW.milestone_payfor,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_payfor',
       :OLD.milestone_payfor, :NEW.milestone_payfor);
  END IF;
     IF NVL(:OLD.milestone_eventstatus,0) !=
     NVL(:NEW.milestone_eventstatus,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_eventstatus',
       :OLD.milestone_eventstatus, :NEW.milestone_eventstatus);
  END IF;

  /*IF NVL(:OLD.milestone_achievedcount,0) !=
     NVL(:NEW.milestone_achievedcount,0) THEN
     Audit_Trail.column_update
       (raid, 'milestone_achievedcount',
       :OLD.milestone_achievedcount, :NEW.milestone_achievedcount);
  END IF;*/

  --Added by Manimaran for milestone_description field.
  IF NVL(:OLD.milestone_description,' ') !=
     NVL(:NEW.milestone_description,' ') THEN
     Audit_Trail.column_update
       (raid, 'MILESTONE_DESCRIPTION',
       :OLD.milestone_description, :NEW.milestone_description);
  END IF;

  --Added by Manimaran for D-FIN7
  IF NVL(:OLD.fk_codelst_milestone_stat,0) !=
     NVL(:NEW.fk_codelst_milestone_stat,0) THEN
     Audit_Trail.column_update
       (raid, 'FK_CODELST_MILESTONE_STAT',
       :OLD.fk_codelst_milestone_stat, :NEW.fk_codelst_milestone_stat);
  END IF;

END;
/

create or replace TRIGGER ER_SPECIMEN_APPNDX_AUO AFTER UPDATE OF RID,IP_ADD,CREATOR,FK_SPECIMEN,CREATED_ON,PK_SPECIMEN_APPNDX,SA_URI,SA_DESC,SA_TYPE ON ERES.ER_SPECIMEN_APPNDX FOR EACH ROW
declare
  raid number(10);
  usr varchar2(2000);
begin
  --KM-#3634
  usr := getuser(:new.last_modified_by);
  select seq_audit.nextval into raid from dual;
  audit_trail.record_transaction
    (raid, 'ER_SPECIMEN_APPNDX', :old.rid, 'U', usr);
  if nvl(:old.pk_specimen_appndx,0) !=
     NVL(:new.pk_specimen_appndx,0) then
     audit_trail.column_update
       (raid, 'PK_SPECIMEN_APPNDX',
       :old.pk_specimen_appndx, :new.pk_specimen_appndx);
  end if;

  if nvl(:old.fk_specimen,0) !=
     NVL(:new.fk_specimen,0) then
     audit_trail.column_update
       (raid, 'FK_SPECIMEN',
       :old.fk_specimen, :new.fk_specimen);
  end if;

  if nvl(:old.sa_desc,' ') !=
     NVL(:new.sa_desc,' ') then
     audit_trail.column_update
       (raid, 'SA_DESC',
       :old.sa_desc, :new.sa_desc);
  end if;

  if nvl(:old.sa_uri,' ') !=
     NVL(:new.sa_uri,' ') then
     audit_trail.column_update
       (raid, 'SA_URI',
       :old.sa_uri, :new.sa_uri);
  end if;

  if nvl(:old.sa_type,' ') !=
     NVL(:new.sa_type,' ') then
     audit_trail.column_update
       (raid, 'SA_TYPE',
       :old.sa_type, :new.sa_type);
  end if;

  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;

  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :old.last_modified_by, :new.last_modified_by);
  end if;

  if nvl(:old.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;

  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;
end;
/

create or replace TRIGGER ER_STUDYSITES_APNDX_AU
AFTER UPDATE
ON ER_STUDYSITES_APNDX REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  usr VARCHAR2(100);
BEGIN
    select seq_audit.nextval into raid from dual;
    usr := getuser(:new.last_modified_by);
   

  --KM-#3635-To avoid insertion of Update records in audit_row table while handling clob column in new and edit mode
  IF NVL(:OLD.last_modified_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:NEW.last_modified_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
    audit_trail.record_transaction
    (raid, 'ER_STUDYSITES_APNDX', :OLD.rid, 'U', usr);
 end if;

if nvl(:old.PK_STUDYSITES_APNDX,0) !=
     NVL(:new.PK_STUDYSITES_APNDX,0) then
     audit_trail.column_update
       (raid, 'PK_STUDYSITES_APNDX',
       :old.PK_STUDYSITES_APNDX, :new.PK_STUDYSITES_APNDX);
  end if;

if nvl(:old.FK_STUDYSITES,0) !=
     NVL(:new.FK_STUDYSITES,0) then
     audit_trail.column_update
       (raid, 'FK_STUDYSITES',
       :old.FK_STUDYSITES, :new.FK_STUDYSITES);
  end if;

if nvl(:old.APNDX_TYPE,' ') !=
     NVL(:new.APNDX_TYPE,' ') then
     audit_trail.column_update
       (raid, 'APNDX_TYPE',
       :old.APNDX_TYPE, :new.APNDX_TYPE);
  end if;


if nvl(:old.APNDX_NAME,' ') !=
     NVL(:new.APNDX_NAME,' ') then
     audit_trail.column_update
       (raid, 'APNDX_NAME',
       :old.APNDX_NAME, :new.APNDX_NAME);
  end if;

if nvl(:old.APNDX_DESCRIPTION,' ') !=
     NVL(:new.APNDX_DESCRIPTION,' ') then
     audit_trail.column_update
       (raid, 'APNDX_DESCRIPTION',
       :old.APNDX_DESCRIPTION, :new.APNDX_DESCRIPTION);
  end if;

if nvl(:old.APNDX_FILESIZE,0) !=
     NVL(:new.APNDX_FILESIZE,0) then
     audit_trail.column_update
       (raid, 'APNDX_FILESIZE',
       :old.APNDX_FILESIZE, :new.APNDX_FILESIZE);
  end if;

if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;

if nvl(:old.CREATOR,0) !=
     NVL(:new.CREATOR,0) then
     audit_trail.column_update
       (raid, 'CREATOR',
       :old.CREATOR, :new.CREATOR);
   end if;

if nvl(:old.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;

if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
   Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into old_modby
     from er_user
     where pk_user = :old.last_modified_by ;
     Exception When NO_DATA_FOUND then
      old_modby := null ;
    End ;
    Begin
     select to_char(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     into new_modby
     from er_user
     where pk_user = :new.last_modified_by ;
     Exception When NO_DATA_FOUND then
      new_modby := null ;
    End ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  end if;

if nvl(:old.last_modified_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.last_modified_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_ON',
       to_char(:OLD.last_modified_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_on,PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;

   if nvl(:old.IP_ADD,' ') !=
     NVL(:new.IP_ADD,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.IP_ADD, :new.IP_ADD);
  end if;
END;
/

create or replace TRIGGER "ERES"."ER_STUDYSITES_APNDX_BU_LM" 
BEFORE UPDATE
ON ER_STUDYSITES_APNDX
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW when (new.last_modified_by is not null)
BEGIN
:NEW.LAST_MODIFIED_ON := SYSDATE ;
END;
/

create or replace TRIGGER ER_MILEAPNDX_AU0
  after update of
  pk_mileapndx,
  fk_study,
  fk_milestone,
  mileapndx_desc,
  mileapndx_uri,
  mileapndx_type,
  creator,
  created_on,
  ip_add,
  rid
  on er_mileapndx
  for each row
declare
  raid number(10);
  usr varchar2(2000);
begin
  select seq_audit.nextval into raid from dual;

  usr := getuser(:new.last_modified_by);
  audit_trail.record_transaction
    (raid, 'ER_MILEAPNDX', :old.rid, 'U', usr);

  if nvl(:old.pk_mileapndx,0) !=
     NVL(:new.pk_mileapndx,0) then
     audit_trail.column_update
       (raid, 'PK_MILEAPNDX',
       :old.pk_mileapndx, :new.pk_mileapndx);
  end if;
  if nvl(:old.fk_study,0) !=
     NVL(:new.fk_study,0) then
     audit_trail.column_update
       (raid, 'FK_STUDY',
       :old.fk_study, :new.fk_study);
  end if;
  if nvl(:old.fk_milestone,0) !=
     NVL(:new.fk_milestone,0) then
     audit_trail.column_update
       (raid, 'FK_MILESTONE',
       :old.fk_milestone, :new.fk_milestone);
  end if;
  if nvl(:old.mileapndx_desc,' ') !=
     NVL(:new.mileapndx_desc,' ') then
     audit_trail.column_update
       (raid, 'MILEAPNDX_DESC',
       :old.mileapndx_desc, :new.mileapndx_desc);
  end if;
  if nvl(:old.mileapndx_uri,' ') !=
     NVL(:new.mileapndx_uri,' ') then
     audit_trail.column_update
       (raid, 'MILEAPNDX_URI',
       :old.mileapndx_uri, :new.mileapndx_uri);
  end if;
  if nvl(:old.mileapndx_type,' ') !=
     NVL(:new.mileapndx_type,' ') then
     audit_trail.column_update
       (raid, 'MILEAPNDX_TYPE',
       :old.mileapndx_type, :new.mileapndx_type);
  end if;
  if nvl(:old.rid,0) !=
     NVL(:new.rid,0) then
     audit_trail.column_update
       (raid, 'RID',
       :old.rid, :new.rid);
  end if;
  if nvl(:old.last_modified_by,0) !=
     NVL(:new.last_modified_by,0) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       :old.last_modified_by, :new.last_modified_by);
  end if;
  if nvl(:old.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
     NVL(:new.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) then
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
       to_char(:old.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:new.last_modified_date, PKG_DATEUTIL.F_GET_DATEFORMAT));
  end if;
  if nvl(:old.ip_add,' ') !=
     NVL(:new.ip_add,' ') then
     audit_trail.column_update
       (raid, 'IP_ADD',
       :old.ip_add, :new.ip_add);
  end if;

end;
/

create or replace TRIGGER "ER_DYNREPVIEW_BI0" BEFORE INSERT ON ER_DYNREPVIEW
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN
   BEGIN
      --KM-#3635
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM er_user
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;
   audit_trail.record_transaction(raid, 'ER_DYNREPVIEW',erid, 'I',usr);
   insert_data:= :NEW.PK_DYNREPVIEW||'|'|| :NEW.FK_DYNREP||'|'||:NEW.DYNREPVIEW_COL||'|'||:NEW.DYNREPVIEW_SEQ||'|'||:NEW.DYNREPVIEW_WIDTH||'|'||:NEW.DYNREPVIEW_FORMAT||'|'||:NEW.DYNREPVIEW_DISPNAME||'|'||:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||
   TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||:NEW.IP_ADD||'|'||:NEW.DYNREPVIEW_COLNAME||'|'||:NEW.DYNREPVIEW_COLDATATYPE||'|'||:NEW.FK_FORM||'|'||:NEW.FORM_TYPE||'|'||:NEW.USEUNIQUEID||'|'||:NEW.USEDATAVAL||'|'||:NEW.CALCPER||'|'||:NEW.CALCSUM ||'|'||:NEW.REPEAT_NUMBER;

   INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END ;
/

create or replace TRIGGER "ER_FLDLIB_BI0" BEFORE INSERT ON ER_FLDLIB
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW   WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
 BEGIN

 --KM-#3635
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

                         SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction(raid, 'ER_FLDLIB',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.FLD_OVERRIDE_FORMAT||'|'||
    :NEW.FLD_OVERRIDE_RANGE||'|'||
    :NEW.FLD_OVERRIDE_DATE||'|'||
    :NEW.FLD_HIDERESPLABEL||'|'||
    :NEW.FLD_DISPLAY_WIDTH||'|'|| :NEW.FLD_LINKEDFORM||'|'||
    :NEW.FLD_RESPALIGN||'|'||:NEW.FLD_SORTORDER||'|'||
    :NEW.FLD_HIDELABEL||'|'||:NEW.FLD_NAME_FORMATTED||'|'||
    :NEW.PK_FIELD||'|'|| :NEW.FK_CATLIB||'|'|| :NEW.FK_ACCOUNT||'|'||
    :NEW.FLD_LIBFLAG||'|'||:NEW.FLD_NAME||'|'||:NEW.FLD_DESC||'|'||
    :NEW.FLD_UNIQUEID||'|'||:NEW.FLD_SYSTEMID||'|'||:NEW.FLD_KEYWORD||'|'||
    :NEW.FLD_TYPE||'|'||:NEW.FLD_DATATYPE||'|'||:NEW.FLD_INSTRUCTIONS||'|'||
    :NEW.FLD_LENGTH||'|'|| :NEW.FLD_DECIMAL||'|'|| :NEW.FLD_LINESNO||'|'||
    :NEW.FLD_CHARSNO||'|'||:NEW.FLD_DEFRESP||'|'|| :NEW.FK_LOOKUP||'|'||
    :NEW.FLD_ISUNIQUE||'|'|| :NEW.FLD_ISREADONLY||'|'|| :NEW.FLD_ISVISIBLE||'|'||
    :NEW.FLD_COLCOUNT||'|'||:NEW.FLD_FORMAT||'|'|| :NEW.FLD_REPEATFLAG||'|'|| :NEW.FLD_BOLD||'|'||
    :NEW.FLD_ITALICS||'|'|| :NEW.FLD_SAMELINE||'|'||:NEW.FLD_ALIGN||'|'||
    :NEW.FLD_UNDERLINE||'|'||:NEW.FLD_COLOR||'|'||:NEW.FLD_FONT||'|'||:NEW.RECORD_TYPE||'|'||
    :NEW.FLD_FONTSIZE||'|'||:NEW.FLD_LKPDATAVAL||'|'|| :NEW.RID||'|'||:NEW.FLD_LKPDISPVAL||'|'||
    :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
    TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD||'|'||
    :NEW.FLD_TODAYCHECK||'|'|| :NEW.FLD_OVERRIDE_MANDATORY||'|'||:NEW.FLD_LKPTYPE||'|'||
    :NEW.FLD_EXPLABEL;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END ;
/

create or replace TRIGGER "ER_FORMQUERY_BI0" BEFORE INSERT ON ER_FORMQUERY
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW   WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
   erid NUMBER(10);
   usr VARCHAR(2000);
   raid NUMBER(10);
   insert_data CLOB;
     BEGIN
   BEGIN
 --KM-#3635
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;


                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction(raid, 'ER_FORMQUERY',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_FORMQUERY||'|'|| :NEW.FK_QUERYMODULE||'|'||
     :NEW.QUERYMODULE_LINKEDTO||'|'|| :NEW.FK_FIELD||'|'||:NEW.QUERY_NAME||'|'||
  :NEW.QUERY_DESC||'|'|| :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
  TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
  TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD;

 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END ;
/

create or replace TRIGGER "ER_FORMQUERYSTATUS_BI0" BEFORE INSERT ON ER_FORMQUERYSTATUS
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW   WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
 BEGIN
 --KM-#3635
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
   audit_trail.record_transaction(raid, 'ER_FORMQUERYSTATUS',erid, 'I',usr);
     -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_FORMQUERYSTATUS||'|'|| :NEW.FK_FORMQUERY||'|'||
    :NEW.FORMQUERY_TYPE||'|'|| :NEW.FK_CODELST_QUERYTYPE||'|'||
    :NEW.QUERY_NOTES||'|'|| :NEW.ENTERED_BY||'|'|| TO_CHAR(:NEW.ENTERED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||
    :NEW.FK_CODELST_QUERYSTATUS||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
  TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
   TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD||'|'|| :NEW.RID;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END ;
/

create or replace TRIGGER "ER_FORMSTAT_BI0" BEFORE INSERT ON ER_FORMSTAT
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW   WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
 BEGIN

 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction(raid, 'ER_FORMSTAT',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_FORMSTAT||'|'|| :NEW.FK_FORMLIB||'|'||
    :NEW.FK_CODELST_STAT||'|'|| :NEW.FORMSTAT_CHANGEDBY||'|'||
    TO_CHAR(:NEW.FORMSTAT_STDATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
    TO_CHAR(:NEW.FORMSTAT_ENDDATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
    :NEW.FORMSTAT_NOTES||'|'|| :NEW.RID||'|'||:NEW.RECORD_TYPE||'|'|| :NEW.CREATOR||'|'||
    :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
    TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD;

 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END ;
/

create or replace TRIGGER ER_SPECIMEN_BI0 BEFORE INSERT ON ER_SPECIMEN
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN
   BEGIN
      --KM-#3635
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM ER_USER
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   audit_trail.record_transaction(raid, 'ER_SPECIMEN',erid, 'I',usr);

insert_data:=

:NEW.PK_SPECIMEN || '|' ||
:NEW.SPEC_ID|| '|' ||
:NEW.SPEC_ALTERNATE_ID || '|' ||
:NEW.SPEC_DESCRIPTION || '|' ||
:NEW.SPEC_ORIGINAL_QUANTITY || '|' ||
:NEW.SPEC_QUANTITY_UNITS || '|' ||
:NEW.SPEC_TYPE || '|' ||
:NEW.FK_STUDY || '|' ||
TO_CHAR(:NEW.SPEC_COLLECTION_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)|| '|' ||
:NEW.FK_PER || '|' ||
:NEW.SPEC_OWNER_FK_USER || '|' ||
:NEW.FK_STORAGE || '|' ||
:NEW.FK_SITE || '|' ||
:NEW.FK_VISIT || '|' ||
:NEW.FK_SCH_EVENTS1 || '|' ||
:NEW.FK_SPECIMEN || '|' ||
:NEW.SPEC_STORAGE_TEMP || '|' ||
:NEW.SPEC_STORAGE_TEMP_UNIT || '|' ||
:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)
||'|'||:NEW.IP_ADD||'|'||:NEW.FK_ACCOUNT||'|'||:NEW.SPEC_PROCESSING_TYPE||'|'||:NEW.FK_STORAGE_KIT_COMPONENT  --KM -013008
||'|'||:NEW.FK_USER_PATH||'|'||:NEW.FK_USER_SURG||'|'||:NEW.FK_USER_COLLTECH||'|'||:NEW.FK_USER_PROCTECH
||'|'||TO_CHAR(:NEW.SPEC_REMOVAL_TIME,PKG_DATEUTIL.F_GET_DATETIMEFORMAT) ||'|'|| TO_CHAR(:NEW.SPEC_FREEZE_TIME,PKG_DATEUTIL.F_GET_DATETIMEFORMAT)||'|'||
:NEW.spec_anatomic_site||'|'||
:NEW.spec_tissue_side||'|'||
:NEW.spec_pathology_stat||'|'||
:NEW.fk_storage_kit||'|'||
:NEW.spec_expected_quantity||'|'||
:NEW.spec_base_orig_quantity||'|'||
:NEW.spec_expectedq_units||'|'||
:NEW.spec_base_origq_units;

INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
END;
/

create or replace TRIGGER "ER_SPECIMEN_STATUS_BIO" BEFORE INSERT ON ER_SPECIMEN_STATUS
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN

   BEGIN
   --KM-#3635
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM ER_USER
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   audit_trail.record_transaction(raid, 'ER_SPECIMEN_STATUS',erid, 'I',usr);

insert_data:= :NEW.PK_SPECIMEN_STATUS||'|'||:NEW.FK_SPECIMEN||'|'||TO_CHAR(:NEW.SS_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.FK_CODELST_STATUS||'|'||
:NEW.SS_QUANTITY||'|'||:NEW.SS_QUANTITY_UNITS||'|'||:NEW.SS_ACTION||'|'||:NEW.FK_STUDY||'|'||:NEW.FK_USER_RECEPIENT||'|'||
:NEW.SS_TRACKING_NUMBER||'|'||:NEW.SS_STATUS_BY||'|'||
:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD||'|'||
:NEW.SS_PROC_TYPE||'|'||
TO_CHAR(:NEW.SS_HAND_OFF_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
TO_CHAR(:NEW.SS_PROC_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT);

INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
END ;
/

create or replace TRIGGER "ER_STORAGE_BI0" BEFORE INSERT ON ER_STORAGE
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN
--Created by Manimaran for Audit Insert
   BEGIN
      --KM-#3635
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM ER_USER
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   audit_trail.record_transaction(raid, 'ER_STORAGE',erid, 'I',usr);
   --KM-Template field added.
   insert_data:= :NEW.PK_STORAGE||'|'||:NEW.STORAGE_ID||'|'||:NEW.STORAGE_NAME||'|'||
   :NEW.FK_CODELST_STORAGE_TYPE||'|'||:NEW.FK_STORAGE||'|'||:NEW.STORAGE_CAP_NUMBER||'|'||
   :NEW.FK_CODELST_CAPACITY_UNITS||'|'||--:NEW.STORAGE_STATUS||'|'||--KM
   :NEW.STORAGE_DIM1_CELLNUM||'|'||:NEW.STORAGE_DIM1_NAMING||'|'||:NEW.STORAGE_DIM1_ORDER||'|'||
   :NEW.STORAGE_DIM2_CELLNUM||'|'||:NEW.STORAGE_DIM2_NAMING||'|'||:NEW.STORAGE_DIM1_ORDER||'|'||
   :NEW.STORAGE_AVAIL_UNIT_COUNT||'|'||:NEW.STORAGE_COORDINATE_X||'|'||
   :NEW.STORAGE_COORDINATE_Y||'|'||:NEW.STORAGE_LABEL||'|'||
   :NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)
   ||'|'||:NEW.IP_ADD||'|'||:NEW.FK_ACCOUNT||'|'||:NEW.STORAGE_ISTEMPLATE|| '|' ||
   :NEW.STORAGE_TEMPLATE_TYPE|| '|' ||:NEW.STORAGE_ALTERNALEID|| '|' ||:NEW.STORAGE_UNIT_CLASS|| '|' ||:NEW.STORAGE_KIT_CATEGORY;

   INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
END ;
/

create or replace TRIGGER "ER_ALLOWED_ITEMS_BIO" BEFORE INSERT ON ER_ALLOWED_ITEMS
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN
   BEGIN
      --KM-#3635
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM ER_USER
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;

SELECT seq_audit.NEXTVAL INTO raid FROM dual;

 Audit_Trail.record_transaction(raid, 'ER_ALLOWED_ITEMS',erid, 'I',usr);

 insert_data:=:NEW.PK_AI||'|'||:NEW.FK_STORAGE||'|'||:NEW.FK_CODELST_STORAGE_TYPE||'|'||:NEW.FK_CODELST_SPECIMEN_TYPE||'|'||:NEW.AI_ITEM_TYPE||'|'|| :NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat);

 INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);


END ;
/

create or replace TRIGGER "ER_STORAGE_STATUS_BIO" BEFORE INSERT ON ER_STORAGE_STATUS
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN

   BEGIN
      --KM-#3635
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM er_user
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   audit_trail.record_transaction(raid, 'ER_STORAGE_STATUS',erid, 'I',usr);

insert_data:= :NEW.PK_STORAGE_STATUS||'|'||:NEW.FK_STORAGE||'|'||TO_CHAR(:NEW.SS_START_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
:NEW.FK_CODELST_STORAGE_STATUS||'|'||
--:NEW.SS_NOTES||'|'|| --KM-to fix the issue3171
:NEW.FK_USER||'|'||:NEW.SS_TRACKING_NUMBER||'|'||:NEW.FK_STUDY||'|'||
:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD;

INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END ;
/

create or replace TRIGGER "ER_STUDYSEC_BI0"
BEFORE INSERT
ON ER_STUDYSEC
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
 WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      ) DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(100);
  insert_data CLOB;


BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
  INTO usr FROM ER_USER WHERE pk_user =:NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

SELECT TRUNC(seq_rid.NEXTVAL) INTO erid FROM dual;
	   :NEW.rid := erid ;
	   SELECT seq_audit.NEXTVAL INTO raid FROM dual;

Audit_Trail.record_transaction(raid, 'ER_STUDYSEC', erid, 'I', USR );
-- Added by JMajumdar on 23-06-05 for Audit Insert

--modified by Sonia Abrol, change dthe field to studysec_text
insert_data := :NEW.PK_STUDYSEC||'|'||:NEW.FK_STUDY||'|'||:NEW.STUDYSEC_NAME||'|';

insert_data := insert_data || :NEW.STUDYSEC_PUBFLAG||'|'||:NEW.STUDYSEC_SEQ||'|'||
			  :NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||
			  TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
			  TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
			  :NEW.IP_ADD||'|'|| :NEW.STUDYSEC_NUM||'|'||
			  :NEW.FK_STUDYVER ||'|';

--JM: commented and modified in the new line
--insert_data := insert_data || 	 dbms_lob.substr(:NEW.STUDYSEC_TEXT,1,4000) ||'|';
insert_data := insert_data || 	 dbms_lob.SUBSTR(:NEW.STUDYSEC_TEXT,4000,1);



INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
END;
/

create or replace TRIGGER "ER_OBJECTSHARE_BI0" BEFORE INSERT ON ER_OBJECTSHARE
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW    WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
 erid NUMBER(10);
 usr VARCHAR(2000);
 raid NUMBER(10);
 insert_data CLOB;
     BEGIN
   BEGIN

 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

                SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'ER_OBJECTSHARE',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.FK_OBJECTSHARE||'|'|| :NEW.PK_OBJECTSHARE||'|'||
    :NEW.OBJECT_NUMBER||'|'|| :NEW.FK_OBJECT||'|'|| :NEW.FK_OBJECTSHARE_ID||'|'||
    :NEW.OBJECTSHARE_TYPE||'|'|| :NEW.RID||'|'|| :NEW.CREATOR||'|'||
    :NEW.RECORD_TYPE||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
    TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
    TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END;
/

create or replace TRIGGER "ER_ADD_BI0" BEFORE INSERT ON ER_ADD
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  usr VARCHAR(2000);
      insert_data CLOB;
     BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.add_creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;

                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;


  audit_trail.record_transaction(raid, 'ER_ADD',erid, 'I',usr);
  -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_ADD||'|'|| :NEW.FK_CODELST_ADDTYPE||'|'||
    :NEW.ADDRESS||'|'||:NEW.ADD_CITY||'|'||:NEW.ADD_STATE||'|'||:NEW.ADD_ZIPCODE||'|'||
    :NEW.ADD_COUNTRY||'|'||:NEW.ADD_COUNTY||'|'||
    TO_CHAR(:NEW.ADD_EFF_DT,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.ADD_PHONE||'|'||:NEW.ADD_EMAIL||'|'||
    :NEW.ADD_PAGER||'|'||:NEW.ADD_MOBILE||'|'||:NEW.ADD_FAX||'|'|| :NEW.RID||'|'||
     :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
   TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||
   :NEW.IP_ADD;
    INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END;
/

create or replace TRIGGER "ER_FORMLIBVER_BI0" BEFORE INSERT ON ER_FORMLIBVER
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW   WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
 erid NUMBER(10);
 usr VARCHAR(2000);
 raid NUMBER(10);
 insert_data CLOB;
     BEGIN
 BEGIN

 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

            SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;


  audit_trail.record_transaction(raid, 'ER_FORMLIBVER',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||
    :NEW.IP_ADD||'|'|| :NEW.PK_FORMLIBVER||'|'|| :NEW.FK_FORMLIB||'|'||
    :NEW.FORMLIBVER_NUMBER||'|'||:NEW.FORMLIBVER_NOTES||'|'||
    TO_CHAR(:NEW.FORMLIBVER_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
    :NEW.RID||'|'|| :NEW.CREATOR||'|'||:NEW.RECORD_TYPE||'|'||
    :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat);
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END;
/

create or replace TRIGGER "ER_FLDRESP_BI0" 
  before insert
  on er_fldresp
  for each row
   WHEN (new.rid is null or new.rid = 0) declare
  raid number(10);
  erid number(10);
  USR varchar2(100);

 begin
 begin

 Select to_char(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 into usr from er_user
 where pk_user = :new.creator ;
 Exception When NO_DATA_FOUND then
 USR := 'New User' ;
 End ;
  select trunc(seq_rid.nextval)
  into erid
  from dual;
  :new.rid := erid ;
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction

    (raid, 'ER_FLDRESP', erid, 'I', USR );
end;
/

create or replace TRIGGER "ER_FLDVALIDATE_BI0" BEFORE INSERT ON ER_FLDVALIDATE
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(100);
  insert_data CLOB;
 BEGIN
 BEGIN
 --KM-#3635
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;


  SELECT TRUNC(seq_rid.NEXTVAL) INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'ER_FLDVALIDATE', erid, 'I', USR );
-- Added by JMajumdar on 23-06-05 for Audit Insert
insert_data:= :NEW.PK_FLDVALIDATE||'|'|| :NEW.FK_FLDLIB||'|'|| :NEW.FLDVALIDATE_OP1||'|'||
     :NEW.FLDVALIDATE_VAL1||'|'||:NEW.FLDVALIDATE_LOGOP1||'|'|| :NEW.FLDVALIDATE_OP2||'|'||
     :NEW.FLDVALIDATE_VAL2||'|'|| :NEW.FLDVALIDATE_LOGOP2||'|'||:NEW.FLDVALIDATE_JAVASCR||'|'||
     :NEW.RECORD_TYPE||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
      TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'|| TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.f_get_dateformat)||'|'||
      :NEW.IP_ADD||'|'|| :NEW.RID;

INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END;
/

create or replace TRIGGER "ER_FORMFLD_BI0" BEFORE INSERT ON ER_FORMFLD
       REFERENCING OLD AS OLD NEW AS NEW
  FOR EACH ROW
  WHEN (NEW.rid IS NULL OR NEW.rid = 0) DECLARE
  erid NUMBER(10);
  usr VARCHAR(2000);
  raid NUMBER(10);
  insert_data CLOB;
     BEGIN
   BEGIN
 --KM-#3635
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

                  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;


  audit_trail.record_transaction(raid, 'ER_FORMFLD',erid, 'I',usr);
    -- Added by Ganapathy on 06/21/05 for Audit inserting
       insert_data:= :NEW.PK_FORMFLD||'|'|| :NEW.FK_FORMSEC||'|'||
   :NEW.FK_FIELD||'|'|| :NEW.FORMFLD_SEQ||'|'|| :NEW.FORMFLD_MANDATORY||'|'||
   :NEW.FORMFLD_BROWSERFLG||'|'|| :NEW.RID||'|'||:NEW.FORMFLD_XSL||'|'||
   :NEW.FORMFLD_JAVASCR||'|'|| :NEW.CREATOR||'|'||
   :NEW.LAST_MODIFIED_BY||'|'||:NEW.RECORD_TYPE||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||
    TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||'|'||:NEW.IP_ADD||'|'||
   :NEW.FORMFLD_OFFLINE;
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END ;
/

create or replace TRIGGER "ER_FLDACTIONINFO_BI0" BEFORE INSERT ON ER_FLDACTIONINFO
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN
   BEGIN
      --KM-#3635
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM er_user
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;
   audit_trail.record_transaction(raid, 'ER_FLDACTIONINFO',erid, 'I',usr);
   insert_data:= :NEW.PK_FLDACTIONINFO||'|'||:NEW.FK_FLDACTION||'|'||:NEW.INFO_TYPE||'|'||:NEW.INFO_VALUE||'|'||:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat)||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||:NEW.IP_ADD;

   INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END ;
/

create or replace TRIGGER "ER_FLDACTION_BI0" BEFORE INSERT ON ER_FLDACTION
FOR EACH ROW
DECLARE
   raid NUMBER(10);
   erid NUMBER(10);
   usr VARCHAR(2000);
   insert_data CLOB;
BEGIN
   BEGIN
      --KM-#3635
      SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM er_user
      WHERE pk_user = :NEW.creator ;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         USR := 'New User' ;
   END ;

   SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
   :NEW.rid := erid ;
   SELECT seq_audit.NEXTVAL INTO raid FROM dual;
   audit_trail.record_transaction(raid, 'ER_FLDACTION',erid, 'I',usr);
   insert_data:= :NEW.PK_FLDACTION||'|'||:NEW.FK_FORM||'|'||:NEW.FLDACTION_TYPE||'|'||:NEW.FK_FIELD||'|'||:NEW.FLDACTION_CONDITION||'|'||:NEW.RID||'|'||:NEW.CREATOR||'|'||:NEW.LAST_MODIFIED_BY||'|'||TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.f_get_dateformat)||:NEW.IP_ADD;

   INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END ;
/