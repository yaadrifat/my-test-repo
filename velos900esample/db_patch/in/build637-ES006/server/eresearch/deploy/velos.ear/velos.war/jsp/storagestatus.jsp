<!--

	Project Name:	Velos eResearch

	Author:	Jnanamay Majumdar

	Created on Date:	05Sept2007

	Purpose:	Gui for Storage Status

	File Name:	storagestatus.jsp

-->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Storage_StatusDets%><%--Storage Status Details*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>


<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>

<SCRIPT language="JavaScript">

 function  validate(formobj) {

	 if(formobj.storageStatus) {
  	      if (!(validate_col('Status',formobj.storageStatus))) return false;
	 }

	 if(formobj.statDate) {
	  	   if (!(validate_col('Status Date',formobj.statDate))) return false;
  	   if (!(validate_date(formobj.statDate))) return false;
	 }


     if (!(validate_col('e-Signature',formobj.eSign))) return false


     if(isNaN(formobj.eSign.value) == true) {

		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

		formobj.eSign.focus();

		return false;

	   }

 }



function openUserWindow(frm) {

		windowName = window.open("usersearchdetails.jsp?fname=&lname=&from="+ frm,"TEST","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
		windowName.focus();
}


function openStudyWindow(formobj) {
	formobj.target="Lookup";
	formobj.method="post";
	formobj.action="multilookup.jsp?viewId=6013&form=storagestat&seperator=,"+
                  "&keyword=selStudy|STUDY_NUMBER~selStudyIds|LKP_PK|[VELHIDE]&maxselect=1";

	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');

	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="_self";
	formobj.action="updatestoragestatus.jsp";
	void(0);
}




</SCRIPT>



<%@ page language = "java" import = "com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.business.common.*,com.velos.eres.web.user.*,com.velos.esch.business.common.EventAssocDao"%>

<jsp:useBean id="StorageStatJB" scope="request" class="com.velos.eres.web.storageStatus.StorageStatusJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="stdJB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="usrJB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:include page="include.jsp" flush="true"/>

<% String src;
String from = "status";
src= request.getParameter("srcmenu");

%>


<body>
<br>
<DIV class="popDefault" id="div1">

<%

   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
	{


	String userIdFromSession = (String) tSession.getValue("userId");
	UserJB user = (UserJB) tSession.getValue("currentUser");
	String userId = (String) tSession.getValue("userId");

	String uName = (String) tSession.getValue("userName");
	String acc = (String) tSession.getValue("accountId");


	//Storage PK
	String fkstorage = request.getParameter("pkStorage");
	fkstorage = (fkstorage==null)?"":fkstorage;

	//Storage Status PK
	String pkStorStat = request.getParameter("pkStorageStatus");
	pkStorStat = (pkStorStat==null)?"":pkStorStat;



    //codelst sub type value of the latest storage status
	String latest_Stat_sub_typ = request.getParameter("latestStatSubTye");
	latest_Stat_sub_typ = (latest_Stat_sub_typ==null)?"":latest_Stat_sub_typ;

	//JM: 23Jul2009, #4147(part-2)
	boolean isSpecimenLocated = StorageStatJB.isStorageAllocated(EJBUtil.stringToNum(fkstorage), EJBUtil.stringToNum(acc));



	String mode =  request.getParameter("mode");

		if (mode==null) mode="N";




String selStat = request.getParameter("storageStatus");
selStat = (selStat==null)?"":selStat;

String dstorageStatus = "";


 int defaultStat = 0;

 CodeDao cd3 = new CodeDao();
 cd3.getCodeValues("storage_stat");

 CodeDao cd5 =new CodeDao();
 defaultStat = cd5.getCodeId("storage_stat","Available");

 if (mode.equals("N")) {
	 dstorageStatus=cd3.toPullDown("storageStatus",defaultStat);
 }


	String storStat = "";
 	String  startDt = "";
	String  studyId = "", studyNumber = "";
	String usrId = "", usrName ="";
	String statNotes = "";


	StringBuffer stStatbuffer = new StringBuffer();

	 if (mode.equals("M")) {

		StorageStatJB.setPkStorageStat(EJBUtil.stringToNum(pkStorStat));
		StorageStatJB.getStorageStatusDetails();

		storStat = StorageStatJB.getFkCodelstStorageStat();
		dstorageStatus=cd3.toPullDown("storageStatus",EJBUtil.stringToNum(storStat));


		//JM: 18Sep2007: added the following block: disable the storage status drop down in the modify mode
		int len = dstorageStatus.length();
		int strIndex = dstorageStatus.indexOf(">");

		String strSel =  dstorageStatus.substring(0, strIndex);
		strSel = strSel + " "+ "disabled>";
		stStatbuffer.append(strSel);

		String strApp = dstorageStatus.substring(strIndex+1, len);

		stStatbuffer.append(strApp);
		dstorageStatus = stStatbuffer.toString();
		//JM: 18Sep2007:


		startDt  = StorageStatJB.getSsStartDate();
		startDt=(startDt==null)?"":startDt;



		studyId =   StorageStatJB.getFkStudy();
		studyId=(studyId==null)?"":studyId;


		//get the study number to be displyed
		stdJB.setId(EJBUtil.stringToNum(studyId));
		stdJB.getStudyDetails();
		studyNumber = stdJB.getStudyNumber();
		studyNumber=(studyNumber==null)?"":studyNumber;


		usrId = StorageStatJB.getFkUser();


		statNotes = StorageStatJB.getSsNotes();
		statNotes=(statNotes==null)?"":statNotes;
	 }
		//get the user name to be displayed
		if (mode.equals("N")) {

		usrId=userIdFromSession;

		}else{

		usrId=(usrId==null)?"":usrId;

		}


		usrJB.setUserId(EJBUtil.stringToNum(usrId));
		usrJB.getUserDetails();

		String fName=usrJB.getUserFirstName();
			fName=(fName==null)?"":fName;

		String lName=usrJB.getUserLastName();
			lName=(lName==null)?"":lName;

		usrName =  fName + " " + lName ;



//JM: 19JAN2010, #4638
		if (mode.equals("N")) {
			studyId =  request.getParameter("def_study_Id");
			if (studyId.equals("0")) studyId = "";

			if (!studyId.equals("")){
				stdJB.setId(EJBUtil.stringToNum(studyId));
				stdJB.getStudyDetails();
				studyNumber = stdJB.getStudyNumber();
				studyNumber=(studyNumber==null)?"":studyNumber;
			}
		}



%>




<Form name="storagestat" id="storagestatfrm" method="post" action="updatestoragestatus.jsp" onSubmit ="if (validate(document.storagestat)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">


<P class = "defComments">
<%=MC.M_Etr_StorStatDet%><%--Please enter storage status details*****--%>:
</P>


<input type="hidden" name="srcmenu" Value="<%=src%>">

<input type="hidden" name="pkstorage" Value="<%=fkstorage%>">
<input type="hidden" name="pkStorageStat" Value="<%=pkStorStat%>">
<input type="hidden" name="mode" Value="<%=mode%>">
<input type="hidden" name="storStatDisabled" Value="<%=storStat%>">




<table width="500">


<table width="100%">
<tr>
<td><%=LC.L_Status%><%--Status*****--%><FONT class="Mandatory">* </FONT></td><td width="30%">
<%=dstorageStatus%>
</td>


<td><%=LC.L_Status_Date%><%--Status Date*****--%><FONT class="Mandatory">* </FONT></td>
<td class=tdDefault>
<%-- INF-20084 Datepicker-- AGodara --%>
<% if (latest_Stat_sub_typ.equals("Occupied") && isSpecimenLocated){%>

<Input type = "text" name="statDate"  value="<%=startDt%>"  size=25 MAXLENGTH =20 readonly>
<%}else{%>
<Input type = "text" name="statDate" class="datefield" value="<%=startDt%>"  size=25 MAXLENGTH =20>
<%}%>
</td>
</tr>
<tr>
<td><%=LC.L_For_Study%><%--For <%=LC.Std_Study%>*****--%></td>

<input type="hidden" name="selStudyIds" value="<%=studyId%>">
				<td class=tdDefault width="30%"><Input type = "text" name="selStudy"  value="<%=studyNumber%>" size=25 readonly >

				<A href="#" onClick="openStudyWindow(document.storagestat)" ><%=LC.L_Select_Study%><%--Select <%=LC.Std_Study%>*****--%></A>
				</td>

<td><%=LC.L_For_User%><%--For User*****--%></td>
<input type="hidden" name="creatorId" value="<%=usrId%>">
<td class=tdDefault >
				<Input type = "text" name="createdBy"    value="<%=usrName%>"  size=25 align = right readonly>
				<A href="#" onClick="return openUserWindow('storageStatus')"><%=LC.L_Select_User%><%--Select User*****--%></A></td>

</tr>
<tr><td><%=LC.L_Status_Notes%><%--Status Notes*****--%></td><td><textarea name="statusNotes" rows=4 cols=35 MAXLENGTH = 20000><%=statNotes%></textarea></td></tr>
</table>
<br>

<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="storagestatfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

</Form>

<%

}//end of if body for session

else

{

%>

<jsp:include page="timeout.html" flush="true"/>

<%

}

%>

<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</div>

<DIV class="mainMenu" id = "emenu">

  <jsp:include page="getmenu.jsp" flush="true"/>

</DIV>

</body>

</html>



