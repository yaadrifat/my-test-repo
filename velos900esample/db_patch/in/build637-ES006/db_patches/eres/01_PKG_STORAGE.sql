CREATE OR REPLACE PACKAGE ERES."PKG_STORAGE" AS
/******************************************************************************
   NAME:       PKG_STORAGE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        10/11/2007   Khader              1. Created this package for storage specific Pl/SQL.
******************************************************************************/

   v_id NUMBER;
   indx NUMBER := 1;
   cntr NUMBER := 1;
   x    NUMBER := 0;
   y    NUMBER := 0;

   v_childFetchedList ARRAY_STRING     :=  ARRAY_STRING();
   v_finalDelIdList   ARRAY_STRING     :=  ARRAY_STRING();
   childFetchFlag boolean := true;
   finalDelFlag boolean :=true;
   childFetchedFlag boolean := true;
   /* deletes the  child records recursively for the parent key passed*/
  PROCEDURE sp_delete_storages_parent_chld(p_ids ARRAY_STRING,del_flag in NUMBER, usr in number, o_ret OUT NUMBER);--KM
  /* deletes records in all parent and child tables based on the primary key */
  PROCEDURE SP_DELETE_STORAGES(p_ids ARRAY_STRING, usr in number, o_ret OUT NUMBER);
  /* get all the child and sub sub child storage ids*/
  PROCEDURE sp_find_all_child_storage_ids(storageId IN VARCHAR2, o_return OUT VARCHAR); --JM:28Dec2007
  /* to take required number of copies of storage unit/template  */
  PROCEDURE sp_copy_storages(strg_pks array_string,copy_counts array_string,strg_ids array_string, strg_names array_string, templates array_string, usr varchar2, ipAdd varchar2,o_ret OUT NUMBER); --KM:25Jan2008

  /* copies children / frandchildren of a storage. uses recursion */
  procedure sp_copy_child_storage(p_pk_storge_old Number,p_pk_storage_new Number,p_usr varchar2, p_ipAdd varchar2,o_ret_new OUT NUMBER);

  procedure sp_create_default_storagestat(p_pk_storge_old Number,p_pk_storage_new Number,p_usr varchar2, p_ipAdd varchar2,o_ret_new OUT NUMBER);

  /* copies allowed contents of a storage*/
  procedure sp_copy_allowed_contents(p_pk_storge_old Number,p_pk_storage_new Number,p_template_flag Number, p_usr varchar2, p_ipAdd varchar2,o_ret_new OUT NUMBER);

  /* calculate the number of children and follow the first child's children all the way down */
  procedure sp_get_firstborn_descendants(i_pk_storage IN Number,o_level OUT NUMBER,o_pk_down OUT VARCHAR2);

  /* procedure to be used recursively by sp_num_firstborn_desc */
  procedure sp_get_firstborn_recursive(i_pk_storage IN Number,o_next_pk OUT NUMBER,o_ret_number OUT NUMBER);


  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'Pkg_storage', pLEVEL  => Plog.LFATAL);

  END Pkg_Storage;
/

CREATE OR REPLACE PACKAGE BODY ERES.PKG_STORAGE AS
 counter NUMBER:= 0;

PROCEDURE sp_delete_storages_parent_chld(p_ids ARRAY_STRING,del_flag in NUMBER, usr in number, o_ret OUT NUMBER)
IS
v_childlist  ARRAY_STRING     :=null;
 flag number  :=0;
 specimencount number;
 checkFlag boolean := false;
 v_checkId number;
BEGIN

 v_finalDelIdList := ARRAY_STRING();
 FOR j IN 1..p_ids.COUNT
 LOOP
 -- Modified by Isaac to fix the bug that not all descendants are being picked up
    -- First add the parent (each of p_ids) to the final list
    v_finalDelIdList.EXTEND;
    v_finalDelIdList(v_finalDelIdList.count) := p_ids(j);

    -- Then find all descendants using start with...connect by prior
    v_childlist := ARRAY_STRING();
    for m in(select pk_storage from er_storage  start with fk_storage = p_ids(j) connect by prior pk_storage = fk_storage)
    loop
      v_childlist.EXTEND;
      v_childlist(v_childlist.count) := m.pk_storage ;
    end loop;

    -- Finally add the descendants to the final list
    for k in 1..v_childlist.count
    loop
      v_finalDelIdList.EXTEND;
      v_finalDelIdList(v_finalDelIdList.count) := v_childlist(k);
    end loop ;

--Added by Manimaran to check spcimen association with storage unit records.
  for p in 1..v_finalDelIdList.count
  loop
    v_checkId := TO_NUMBER(v_finalDelIdList(p));
    select count(*) into specimencount from er_specimen where fk_storage = v_checkId;
    if specimencount > 0 then
      checkFlag := true;
    exit;
    end if;
  end loop;

 END LOOP; -- End of p_ids loop

 if checkFlag = true then
   o_ret := -4;

 elsif del_flag = 1 then
   o_ret := -3;
 else
   sp_delete_storages(v_finalDelIdList,usr, Y);
 end if;

END;


PROCEDURE SP_DELETE_STORAGES(p_ids ARRAY_STRING, usr in number, o_ret OUT NUMBER)
IS
   v_cnt NUMBER;
   i NUMBER;
   v_inStr long;
   v_temp VARCHAR2(1000);
   v_sql long;
   v_sql1 long;
   v_sql2 long;
   v_total_count number;
   v_maincounter number;

--JM: 18Sep2009: 4317
   v_sql3 long;

   v_sql4 long;
   v_sql5 long;


BEGIN


   i:=1;
   v_maincounter := 1;
   v_cnt := 500;

   v_total_count  := p_ids.COUNT; --get the # of elements in array


--plog.fatal(pctx,'v_total_count'|| v_total_count);
  While v_maincounter <=  v_total_count loop

    i := 1;
    v_inStr := '';

    --plog.fatal(pctx,'v_maincounter'|| v_maincounter);

    WHILE (i <= v_cnt and v_maincounter <= v_total_count) LOOP

    v_temp:='to_number('''||p_ids( v_maincounter )||''')';

    --plog.fatal(pctx,'v_temp'|| v_temp);

    IF (LENGTH(v_inStr) > 0) THEN
    v_inStr:=v_inStr||', '||v_temp ;

    ELSE
    v_inStr:= '('||v_temp ;

    END IF;

    i := i+1;

    v_maincounter := v_maincounter+1;
    END LOOP;

    v_inStr:=v_inStr||')' ;


    --plog.fatal(pctx,'v_inStr'|| v_inStr);

    BEGIN
          v_sql2:= 'delete from er_allowed_items  where  fk_storage in ' || v_inStr;
        EXECUTE IMMEDIATE v_sql2 ;
      commit;

        v_sql4:= 'update er_storage_status set last_modified_by='||usr||'where fk_storage in ' || v_inStr;
      EXECUTE IMMEDIATE v_sql4 ;
      COMMIT;

          v_sql1:= 'delete from er_storage_status  where  fk_storage in ' || v_inStr;
        EXECUTE IMMEDIATE v_sql1 ;
      commit;

 --JM: 18Sep2009: 4317
          v_sql3:='delete from er_storage_kit where fk_storage in ' || v_inStr;
      EXECUTE IMMEDIATE v_sql3 ;
      COMMIT;

          v_sql5:= 'update er_storage set last_modified_by='||usr||'where pk_storage in ' || v_inStr;
      EXECUTE IMMEDIATE v_sql5 ;
      COMMIT;


          v_sql:='delete from er_storage where pk_storage in ' || v_inStr;
      EXECUTE IMMEDIATE v_sql ;
      COMMIT;
      EXCEPTION  WHEN OTHERS THEN
        Plog.fatal(pctx, 'exception' || sqlerrm);
        o_ret:=-1;
       RETURN;
      END ;--end of insert begin

   end loop; --main while
   o_ret:=0;

 END;

 ---------------------------------------------------------------------------------------------
 PROCEDURE sp_find_all_child_storage_ids(storageId IN VARCHAR2, o_return OUT VARCHAR)
 is

   v_totalChildlist ARRAY_STRING     := ARRAY_STRING();

   v_cnt NUMBER;
   i NUMBER;
   v_inStr long; --KM
   v_temp VARCHAR2(500);

BEGIN

       for j in(select pk_storage from er_storage  start with fk_storage = storageId connect by prior pk_storage = fk_storage)
        loop

           v_totalChildlist.EXTEND;
           v_totalChildlist(indx) := j.pk_storage ;
           indx := indx +1;

        end loop;

        v_cnt := v_totalChildlist.COUNT;

        i:=1;
        WHILE i <= v_cnt LOOP
      v_temp:=v_totalChildlist(i);

    IF (LENGTH(v_inStr) > 0) THEN
     v_inStr:=v_inStr||','||v_temp ;
    ELSE
     v_inStr:= v_temp ;
    END IF;

     i := i+1;
        END LOOP;

    v_inStr:=v_inStr;
    o_return := v_inStr;

 end;


 /* To take required number of copies of storage unit/template */ --KM-25Jan2008

 procedure sp_copy_storages(strg_pks array_string,copy_counts array_string,strg_ids array_string, strg_names array_string, templates array_string, usr varchar2, ipAdd varchar2,o_ret OUT NUMBER)
 is
 v_strgpk number;
 v_count number;
 v_strgid varchar2(500);
 v_strgname varchar2(500);
 v_template number;
 v_pkstrg number;
 v_newpk number;
 v_newfk number;
 v_strname varchar2(500);


 BEGIN

 FOR i IN 1..strg_pks.COUNT
     LOOP
        v_strgpk := TO_NUMBER(strg_pks(i));
    v_count := to_number(copy_counts(i));
    --v_strgid := strg_ids(i);
    v_strgname := strg_names(i);
    v_template := templates(i);

        for j in 1..v_count
       loop

             for m in ( select pk_storage,storage_id,storage_name, fk_codelst_storage_type,storage_cap_number,
                   fk_codelst_capacity_units,storage_dim1_cellnum, storage_dim1_naming, storage_dim1_order, storage_dim2_cellnum,
                   storage_dim2_naming,storage_dim2_order,storage_avail_unit_count,storage_coordinate_x,storage_coordinate_y,
                   fk_codelst_child_stype,fk_account, storage_template_type, storage_unit_class, storage_notes, storage_kit_category, storage_multi_specimen
                   from er_storage  where pk_storage = v_strgpk   )
             loop

                   select seq_er_storage.nextval into v_newpk  from dual;
                   v_newfk := null;
                   v_strname := v_strgname;

                   select seq_er_storage_id.nextval into v_strgid from dual;

                insert into er_storage(    pk_storage,storage_id,storage_name,fk_codelst_storage_type,fk_storage,storage_cap_number,
                fk_codelst_capacity_units,storage_dim1_cellnum,    storage_dim1_naming,storage_dim1_order,    storage_dim2_cellnum,
                storage_dim2_naming,storage_dim2_order,    storage_avail_unit_count,storage_coordinate_x,    storage_coordinate_y,
                creator, created_on,fk_codelst_child_stype,ip_add,fk_account,storage_istemplate, storage_template_type, storage_unit_class , storage_notes, storage_kit_category,storage_multi_specimen)
                values (v_newpk,v_strgid,v_strname,m.fk_codelst_storage_type,
                v_newfk,m.storage_cap_number,m.fk_codelst_capacity_units,m.storage_dim1_cellnum,m.storage_dim1_naming,m.storage_dim1_order,m.storage_dim2_cellnum,
                m.storage_dim2_naming,m.storage_dim2_order,m.storage_avail_unit_count,m.storage_coordinate_x,m.storage_coordinate_y,
                usr,sysdate,m.fk_codelst_child_stype,ipAdd,m.fk_account,v_template, m.storage_template_type, m.storage_unit_class, m.storage_notes, m.storage_kit_category,m.storage_multi_specimen);


               sp_create_default_storagestat(m.pk_storage,v_newpk,usr,ipAdd ,o_ret );
                sp_copy_child_storage(m.pk_storage,v_newpk,usr , ipAdd ,o_ret);
                sp_copy_allowed_contents(m.pk_storage,v_newpk,v_template,  usr , ipAdd ,o_ret);

           end loop;
         end loop;
    END LOOP;
 o_ret := 0;

 commit;

 END;

procedure sp_copy_child_storage(p_pk_storge_old Number,p_pk_storage_new Number,p_usr varchar2, p_ipAdd varchar2,o_ret_new OUT NUMBER)
 is
 v_newpk number;
 v_strgid varchar2(500);
 begin
     --copy children of old storage PK into new storage PK

     --plog.fatal(pctx,'copying child...' ||p_pk_storge_old);

        for m in ( select pk_storage, storage_id,storage_name, fk_codelst_storage_type,storage_cap_number,
                   fk_codelst_capacity_units,storage_dim1_cellnum, storage_dim1_naming, storage_dim1_order, storage_dim2_cellnum,
                   storage_dim2_naming,storage_dim2_order,storage_avail_unit_count,storage_coordinate_x,storage_coordinate_y,
                   fk_codelst_child_stype,fk_account,storage_template_type, storage_unit_class, storage_notes,  storage_kit_category,storage_multi_specimen,(select count(*) from er_storage i where i.fk_storage = o.pk_storage ) child_count
                   from er_storage o where fk_storage = p_pk_storge_old    )
       loop
               -- copy to new storage

                 select seq_er_storage.nextval into v_newpk  from dual;
                  select seq_er_storage_id.nextval into v_strgid from dual;

               insert into er_storage(    pk_storage,storage_id,storage_name,fk_codelst_storage_type,fk_storage,storage_cap_number,
            fk_codelst_capacity_units,storage_dim1_cellnum,    storage_dim1_naming,storage_dim1_order,    storage_dim2_cellnum,
            storage_dim2_naming,storage_dim2_order,    storage_avail_unit_count,storage_coordinate_x,    storage_coordinate_y,
            creator, created_on,fk_codelst_child_stype,ip_add,fk_account,storage_istemplate, storage_template_type, storage_unit_class, storage_notes, storage_kit_category,storage_multi_specimen)
            values (v_newpk,v_strgid,m.storage_name,m.fk_codelst_storage_type,
                p_pk_storage_new,m.storage_cap_number,m.fk_codelst_capacity_units,m.storage_dim1_cellnum,m.storage_dim1_naming,m.storage_dim1_order,m.storage_dim2_cellnum,
                m.storage_dim2_naming,m.storage_dim2_order,m.storage_avail_unit_count,m.storage_coordinate_x,m.storage_coordinate_y,
                p_usr,sysdate,m.fk_codelst_child_stype,p_ipAdd,m.fk_account,0, m.storage_template_type, m.storage_unit_class, m.storage_notes, m.storage_kit_category,m.storage_multi_specimen);

                sp_create_default_storagestat(m.pk_storage,v_newpk,p_usr, p_ipAdd ,o_ret_new );
                sp_copy_allowed_contents(m.pk_storage,v_newpk,0,  p_usr , p_ipAdd ,o_ret_new );

                if m.child_count > 0 then
                    sp_copy_child_storage( m.pk_storage,v_newpk,p_usr , p_ipAdd ,o_ret_new);
                end if;

       end loop;
     o_ret_new := 0;

 end;

procedure sp_create_default_storagestat(p_pk_storge_old Number,p_pk_storage_new Number,p_usr varchar2, p_ipAdd varchar2,o_ret_new OUT NUMBER)
as
v_def_stat number;
v_Occ_stat number;
v_ss_notes clob;
v_fk_study number;
v_storage_cap_num number;
v_storage_multi_specimen number;
v_codelst_capacity_units number;
v_fk_codelst_item number;
v_multistoragespecimen_count number;
begin

    select pkg_util.f_getcodepk ('Available', 'storage_stat'),pkg_util.f_getcodepk ('Occupied', 'storage_stat'),pkg_util.f_getcodepk ('Items', 'cap_unit')
    into v_def_stat,v_Occ_stat,v_fk_codelst_item
    from dual;
    SELECT  COUNT(*) INTO v_multistoragespecimen_count FROM er_storage WHERE FK_STORAGE =p_pk_storge_old;
    if(v_multistoragespecimen_count>0) then
    select NVL(storage_cap_number,0),NVL(storage_multi_specimen,0),NVL(fk_codelst_capacity_units,0) 
    into v_storage_cap_num,v_storage_multi_specimen,v_codelst_capacity_units from er_storage where PK_STORAGE=p_pk_storge_old;
    
    IF (v_storage_multi_specimen=1 and v_codelst_capacity_units=v_fk_codelst_item and v_storage_cap_num=v_multistoragespecimen_count) then
    insert into er_storage_status(pk_storage_status,
    fk_storage,
    ss_start_date,
    fk_codelst_storage_status,
    creator,
    created_on,
    LAST_MODIFIED_BY,
    LAST_MODIFIED_DATE,
    ip_add)
    values ( seq_er_storage_stat.nextval,p_pk_storage_new,trim(sysdate),v_Occ_stat,
    p_usr,sysdate,p_usr,sysdate,p_ipAdd );

    o_ret_new := 0;
    else
    insert into er_storage_status(pk_storage_status,
    fk_storage,
    ss_start_date,
    fk_codelst_storage_status,
    creator,
    created_on,
    LAST_MODIFIED_BY,
    LAST_MODIFIED_DATE,
    ip_add)
    values ( seq_er_storage_stat.nextval,p_pk_storage_new,trim(sysdate),v_def_stat,
    p_usr,sysdate,p_usr,sysdate,p_ipAdd );

    o_ret_new := 0;
end if;
else
    insert into er_storage_status(pk_storage_status,
    fk_storage,
    ss_start_date,
    fk_codelst_storage_status,
    creator,
    created_on,
    LAST_MODIFIED_BY,
    LAST_MODIFIED_DATE,
    ip_add)
    values ( seq_er_storage_stat.nextval,p_pk_storage_new,trim(sysdate),v_def_stat,
    p_usr,sysdate,p_usr,sysdate,p_ipAdd );

    o_ret_new := 0;
    end if;
end;

/* copies allowed contents of a storage*/
  procedure sp_copy_allowed_contents(p_pk_storge_old Number,p_pk_storage_new Number,p_template_flag Number,
  p_usr varchar2, p_ipAdd varchar2,o_ret_new OUT NUMBER)
  as
  v_sql varchar2(4000);

  begin
      -- if template flag is 1, do not copy allowed contents for specimens

      v_sql  := 'Insert into ER_ALLOWED_ITEMS  (PK_AI, FK_STORAGE, FK_CODELST_STORAGE_TYPE, FK_CODELST_SPECIMEN_TYPE, AI_ITEM_TYPE,
             CREATOR, CREATED_ON, IP_ADD)
             Select seq_er_allowed_items.nextval,:1,FK_CODELST_STORAGE_TYPE, FK_CODELST_SPECIMEN_TYPE, AI_ITEM_TYPE,
             :2,:3,:4
             from ER_ALLOWED_ITEMS where fk_storage = :5';

         -- get the     FK_CODELST_STORAGE_TYPE for storage, when template=1, only storage type will be copied
         --JM: 16NOV2009, #4434 modified the foll. v_sql
      if p_template_flag = 1 then
              v_sql  := v_sql  || ' and (FK_CODELST_STORAGE_TYPE in (select pk_codelst from er_codelst where codelst_type in (''store_type''))
              OR FK_CODELST_SPECIMEN_TYPE in (select pk_codelst from er_codelst where codelst_type in (''specimen_type'')))';

      end if;

         execute immediate v_sql using p_pk_storage_new,p_usr,sysdate,p_ipAdd,p_pk_storge_old;

  end;

  /* calculate the number of children and follow the first child's children and so on all the way down */
  procedure sp_get_firstborn_descendants(i_pk_storage IN Number,
      o_level OUT NUMBER, o_pk_down OUT VARCHAR2)
  as
  v_next_pk number := i_pk_storage;
  v_ret_number number := 0;
  v_level number := 0;
  begin
      o_pk_down := '(';
      while v_next_pk > 0 loop
          o_pk_down := o_pk_down || v_next_pk || ',';
          sp_get_firstborn_recursive(v_next_pk, v_next_pk, v_ret_number);
          exit when v_ret_number = 0;
      end loop;
      o_pk_down := o_pk_down || '0)';
      dbms_output.put_line('o_pk_down=' || o_pk_down);

      for m in (
          select level lvl into v_level from er_storage e
          start with pk_storage = i_pk_storage
          connect by prior fk_storage = pk_storage
      )
      loop
          dbms_output.put_line('v_level=' || v_level);
          if m.lvl > v_level then
                v_level := m.lvl;
          end if;
      end loop;

      o_level := v_level;
  exception
      when NO_DATA_FOUND then
          o_level := 0;
          o_pk_down := '(0)';
  end;

  /* procedure to be used recursively by sp_get_firstborn_descendants */
  procedure sp_get_firstborn_recursive(i_pk_storage IN Number,o_next_pk OUT NUMBER,o_ret_number OUT NUMBER)
  as
  v_next_pk number := 0;
  v_ret_number number := 0;
  begin
      o_ret_number := -1;
      select pk_storage, storage_cap_number into v_next_pk, v_ret_number
      from (select pk_storage, storage_cap_number from er_storage where
      fk_storage = i_pk_storage
      order by fk_storage,storage_coordinate_y,storage_coordinate_x) where
      rownum <= 1;
      if v_ret_number = 1 then
          v_ret_number := 0;
      end if;
      o_next_pk := v_next_pk;
      o_ret_number := v_ret_number;
  exception
      when NO_DATA_FOUND then
          v_next_pk := 0;
          v_ret_number := 0;
  end;
END PKG_STORAGE;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,186,1,'01_PKG_STORAGE.sql',sysdate,'9.0.0 B#637-ES006');

COMMIT;
