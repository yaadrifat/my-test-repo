--STARTS INSERTING RECORD INTO CB_FORMS TABLE--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'MRQ'
    AND VERSION = 'GFA';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'MRQ','GFA','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'MRQ'
    AND VERSION = 'GFB';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'MRQ','GFB','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'MRQ'
    AND VERSION = 'GFC';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'MRQ','GFC','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'MRQ'
    AND VERSION = 'N2';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'MRQ','N2','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'MRQ'
    AND VERSION = 'N2B';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'MRQ','N2B','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'MRQ'
    AND VERSION = 'N2C';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'MRQ','N2C','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'MRQ'
    AND VERSION = 'N2D';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'MRQ','N2D','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'MRQ'
    AND VERSION = 'N2E';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'MRQ','N2E','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'MRQ'
    AND VERSION = 'N2F';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'MRQ','N2F','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'FMHQ'
    AND VERSION = 'GFA';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'FMHQ','GFA','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'FMHQ'
    AND VERSION = 'N2';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'FMHQ','N2','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'FMHQ'
    AND VERSION = 'N2B';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'FMHQ','N2B','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORMS
    where FORMS_DESC = 'FMHQ'
    AND VERSION = 'N2C';
  if (v_record_exists = 0) then
	Insert into CB_FORMS(PK_FORM,FORMS_DESC,VERSION,IS_CURRENT_FLAG,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_FORMS.nextval,'FMHQ','N2C','0',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_QUESTION_GROUP TABLE--

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GROUP
    where QUESTION_GRP_DESC = 'E. Addendum';
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GROUP(PK_QUESTION_GROUP,QUESTION_GRP_DESC,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GROUP.NEXTVAL,'E. Addendum',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GROUP
    where QUESTION_GRP_DESC = 'Have You Section';
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GROUP(PK_QUESTION_GROUP,QUESTION_GRP_DESC,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GROUP.NEXTVAL,'Have You Section',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GROUP
    where QUESTION_GRP_DESC = 'In the Last 12 Months, Have You Section';
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GROUP(PK_QUESTION_GROUP,QUESTION_GRP_DESC,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GROUP.NEXTVAL,'In the Last 12 Months, Have You Section',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GROUP
    where QUESTION_GRP_DESC = 'Additional Questions Section';
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GROUP(PK_QUESTION_GROUP,QUESTION_GRP_DESC,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GROUP.NEXTVAL,'Additional Questions Section',null,sysdate,null,null,null,null,null);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_FORMS TABLE--
--Query For N2F Form --
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'Hep_B_imm_glob_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG, RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Hepatitis B Immune Globulin in the last 12 months?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0',(select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'Hep_B_imm_glob_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 3.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'evr_tkn_hmn_pit_gh_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Growth hormone from human pituitary glands ever?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0',(select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'evr_tkn_hmn_pit_gh_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'shots_vacc_12wk_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 12 weeks, have you had any shots or vaccinations?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'shots_vacc_12wk_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'shots_vacc_12wk_desc';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'If yes, please describe:', NULL, '1', '0', '1', '0', '0', NULL, '1', '0',(select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'shots_vacc_12wk_desc',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/


--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cntc_smallpox_vaccine_8wk_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 8 weeks, have you had contact with someone who has received the smallpox vaccine?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'cntc_smallpox_vaccine_8wk_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'pst_4mo_illness_symptom_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 4 months, have you experienced two or more of the following: a fever (>100.5ºF or 38.0ºC), headache, muscle weakness, skin rash on trunk of the body, swollen lymph glands?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'pst_4mo_illness_symptom_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'major_ill_srgry_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 12 months, have you had a major illness or surgery?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'major_ill_srgry_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/


--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'major_ill_desc';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'If yes, please describe:', NULL, '1', '0', '1', '0', '0', NULL, '1', '0',(select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'major_ill_desc',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'bld_disease_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Have you ever had a bleeding condition or blood disease, including sickle cell disease?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'bld_disease_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/


--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'bld_prblm_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Have you ever had a bleeding problem, such as hemophilia or other clotting factor deficiencies, and received human-derived clotting factor concentrates?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'bld_prblm_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/


--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'jaund_liver_hep_pos_tst_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Have you ever had yellow jaundice, liver disease, viral hepatitis, or a positive test (including screening tests) for hepatitis?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'jaund_liver_hep_pos_tst_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'para_chagas_babesiosis_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Have you ever had a parasitic blood disease such as Chagas’ disease or Babesiosis?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'para_chagas_babesiosis_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/


--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cjd_diag_neuro_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Have you been diagnosed with Creutzfeldt-Jakob Disease (CJD) or variant CJD or do you have a degenerative neurological condition such as dementia where the cause has not been identified?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'cjd_diag_neuro_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'blood_transf_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 12 months, have you had a blood transfusion?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'blood_transf_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 22--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tx_stemcell_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 12 months, have you had a transplant such as organ, bone marrow, stem cell, cornea, bone or other tissue?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'tx_stemcell_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 23--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tissue_graft_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 12 months, have you had a tissue graft such as bone or skin?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'tissue_graft_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 24--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tattoo_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 12 months, have you had a tattoo?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'tattoo_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 25--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tattoo_shared_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Were shared instruments, needles or inks used for the tattoo?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'tattoo_shared_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 26--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'piercing_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 12 months, have you had an ear, skin, or body piercing using shared instruments or needles?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'piercing_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 28--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'had_treat_syph_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 12 months, have you had or been treated for a sexually transmitted disease, including syphilis?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'had_treat_syph_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 29--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'give_mn_dr_pmt_for_sex_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 12 months have you given money, drugs, or other payment to anyone to have sex with you?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'give_mn_dr_pmt_for_sex_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 30--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_tk_mn_dr_pmt_sex_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 12 months have you had sex with anyone who has taken money, drugs, or other payment in exchange for sex in the past 5 years?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'sex_w_tk_mn_dr_pmt_sex_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 31--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_cntc_liv_jaund_hep_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 12 months, have you had sexual contact or lived with a person who has active or chronic viral hepatitis or yellow jaundice?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'sex_cntc_liv_jaund_hep_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 37--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'pst_5yr_tk_mn_dr_pmt_sex_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 5 years, have you received money, drugs, or other payment for sex?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'pst_5yr_tk_mn_dr_pmt_sex_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 40.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'unexpln_blue_prpl_spot';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Unexplained blue or purple spots on or under the skin or mucous membranes?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'unexpln_blue_prpl_spot',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 40.h--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'unexpln_1m_lump_nk_apit_grn';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Lumps in your neck, armpits, or groin lasting longer than one month?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'unexpln_1m_lump_nk_apit_grn',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 41--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'htlv_incl_screen_test_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Have you ever tested positive for HTLV (including screening tests)?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'htlv_incl_screen_test_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 42--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_contag_understand_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Do you understand that if you have the AIDS virus, you can give it to someone else even though you may feel well and have a negative AIDS test?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'hiv_contag_understand_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/


--Question 43--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'live_travel_europe_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Since 1980, have you ever lived in or traveled to Europe?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'live_travel_europe_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 46--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'spent_europe_ge_5y_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Since 1980, have you spent time that adds up to 5 years or more in Europe, including time spent in the UK between 1980 and 1996?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'spent_europe_ge_5y_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/


--Question 47--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'us_military_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'From 1980 through 1996, were you a member of the U.S. military, a civilian military employee, or a dependent of a member of the U.S. military?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'us_military_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 53--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'addendum_questions_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Were NMDP addendum questions asked?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'addendum_questions_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--END--
--Query For N2E Form --
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'propecia_last_month_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG, RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Propecia© (finasteride) in the last month?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0',(select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'propecia_last_month_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 3.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'accutane_last_month_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG, RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Accutane© (isotretinoin) in the last month?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0',(select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'accutane_last_month_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 3.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'soriatane_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG, RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Soriatane© (acitretin) in the last 12 months?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0',(select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'soriatane_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 3.f--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'ever_tkn_tegison_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG, RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Tegison© (etretinate) ever?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0',(select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'ever_tkn_tegison_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'pst_4wk_illness_symptom_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG, RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 4 weeks, have you experienced two or more of the following: a fever (>100.5ºF or 38.0ºC), headache, muscle weakness, skin rash on trunk of the body, swollen lymph glands?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0',(select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'pst_4wk_illness_symptom_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'west_nile_preg_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG, RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Have you been diagnosed with West Nile Virus during your pregnancy?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0',(select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'west_nile_preg_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'chagas_dises_babesiosis_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG, RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Have you ever had Chagas’ disease or Babesiosis?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0',(select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'chagas_dises_babesiosis_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cjd_diagnosis_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG, RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Have you or any of your blood relatives ever been diagnosed with Creutzfeldt-Jakob Disease (CJD)?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0',(select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'cjd_diagnosis_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tx_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG, RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 12 months, have you had a transplant such as organ, tissue, or bone marrow?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0',(select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'tx_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 26--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'acc_ns_stk_cntc_bld_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG, RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 12 months, have you had an accidental needle stick or have you come into contact with someone else’s blood through an open wound, non-intact skin, or mucous membrane (for example, into your eye, mouth, etc.)?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0',(select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'acc_ns_stk_cntc_bld_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 27--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'had_treat_syph_gono_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG, RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 12 months, have you had or been treated for a sexually transmitted disease, including syphilis or gonorrhea?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0',(select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'had_treat_syph_gono_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 33--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_clotting_factor_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG, RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'In the past 12 months, have you had sex, even once, with anyone who has taken clotting factor concentrates for a bleeding problem such as hemophilia?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0',(select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'sex_w_clotting_factor_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 43--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'recv_transfusion_uk_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG, RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Since 1980, have you received a transfusion of blood, platelets, plasma, cryoprecipitate, or granulocytes while in the UK?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0',(select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'recv_transfusion_uk_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--END--
--END--
