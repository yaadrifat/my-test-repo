set define off;

CREATE INDEX ERES.C_SPST_FKSPEC ON ERES.ER_SPECIMEN_STATUS
(FK_SPECIMEN)
LOGGING
TABLESPACE ERES_USER
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,187,1,'01_C_SPST_FKSPEC.sql',sysdate,'9.0.0 B#637-ES007');

COMMIT;
