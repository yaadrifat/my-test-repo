/* This readMe is specific to eSample (version 9.0.0 build #637-ES007) */

=====================================================================================================================================
eSample bugs covered in the build:

1) Bug #5314 - Performance botteneck : eSample-Specimens
2) Bug #12227 - INV-21679 :User is "NOT" able to search the specimens, when "User's Primary Organization" is REVOKED
3) Bug #12537 - INV 21675:"P" link is not getting displayed for parent storage
4) Bug #12713 - Update Multiple Specimen Status page fields are not fit into outer container on "editmultiplespecimenstatus.jsp" 

Following existing files have been modified:

	1. specimenbrowser.jsp					(...\deploy\velos.ear\velos.war\jsp)
	2. getStorageGrid.jsp					(...\deploy\velos.ear\velos.war\jsp)
=====================================================================================================================================