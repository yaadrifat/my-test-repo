set define off;

update ER_CODELST set CODELST_CUSTOM_COL = 'IDE' where 
CODELST_TYPE = 'INDIDEGrantor' and CODELST_SUBTYP = 'CDRH';

update ER_CODELST set CODELST_CUSTOM_COL = 'IND' where 
CODELST_TYPE = 'INDIDEGrantor' and CODELST_SUBTYP = 'CDER';

update ER_CODELST set CODELST_CUSTOM_COL = 'IND' where 
CODELST_TYPE = 'INDIDEGrantor' and CODELST_SUBTYP = 'CBER';

commit;
