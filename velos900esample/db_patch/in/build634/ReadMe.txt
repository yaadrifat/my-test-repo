/* This readMe is specific to Velos eResearch version 9.0 build #634 */

=====================================================================================================================================
Garuda :


	     
=====================================================================================================================================
eResearch:

1. EMail issue - There is an old version of mail.jar which is causing trouble for eResearch in sending out emails. Please remove mail.jar (and also activation.jar if it exists in the same folder) from the following location:
[eresearch_dir]\server\eresearch\deploy\velos.ear\velos.war\WEB-INF\lib\


=====================================================================================================================================
eResearch Localization:

Following 208 triggers have been modified 

ERES schema (153)
---------------------
1	CB_ASSESSMENT_AU0
2	CB_ASSESSMENT_AU1
3	CB_BEST_HLA_AU0
4	CB_BEST_HLA_AU1
5	CB_CBU_STATUS_AU0
6	CB_CBU_STATUS_AU1
7	CB_CORD_AU0
8	CB_CORD_AU1
9	CB_CORD_COMPLETE_REQ_INFO_AU0
10	CB_CORD_COMPLETE_REQ_INFO_AU1
11	CB_CORD_FINAL_REVIEW_AU0
12	CB_CORD_FINAL_REVIEW_AU1
13	CB_DCMS_LOG_AU0
14	CB_DCMS_LOG_AU1
15	CB_DUMN_AU0
16	CB_DUMN_AU1
17	CB_ENTITY_SAMPLES_AU1
18	CB_ENTITY_STATUS_AU0
19	CB_ENTITY_STATUS_AU1
20	CB_ENTITY_STATUS_REASON_AU0
21	CB_ENTITY_STATUS_REASON_AU1
22	CB_FUNDING_GUIDELINES_AU0
23	CB_FUNDING_GUIDELINES_AU1
24	CB_FUNDING_SCHEDULE_AU0
25	CB_FUNDING_SCHEDULE_AU1
26	CB_HLA_AU0
27	CB_HLA_AU1
28	CB_IDM_AU0
29	CB_IDM_AU1
30	CB_MULTIPLE_VALUES_AU0
31	CB_MULTIPLE_VALUES_AU1
32	CB_NOTES_AU0
33	CB_NOTES_AU1
34	CB_QUESTION_GRP_AU0
35	CB_QUESTION_GRP_AU1
36	CB_QUESTION_RESPONSES_AU0
37	CB_QUESTION_RESPONSES_AU1
38	CB_QUESTIONS_AU0
39	CB_QUESTIONS_AU1
40	CB_SHIPMENT_AU0
41	CB_SHIPMENT_AU1
42	CB_UPLOAD_INFO_AU0
43	CB_UPLOAD_INFO_AU1
44	CB_USER_ALERTS_AU0
45	CB_USER_ALERTS_AU1
46	CBB_AU0
47	CBB_AU1
48	CBB_PROCESSING_PROC_INFO_AU0
49	CBB_PROCESSING_PROC_INFO_AU1
50	CBB_PROCESSING_PROCEDURES_AU0
51	CBB_PROCESSING_PROCEDURES_AU1
52	CBU_UNIT_REPORT_HLA_AU1
53	er_account_au0
54	er_acctforms_au0
55	er_add_au0
56	ER_ATTACHMENTS_AU0
57	ER_ATTACHMENTS_AU1
58	er_catlib_au0
59	ER_CB_ENTITY_SAMPLES_AU0
60	er_cb_notes_au0
61	er_cb_upload_info_au0
62	er_codelst_au0
63	er_crfforms_au0
64	er_ctrp_documents_au0
65	er_ctrp_draft_au0
66	er_dynrep_au0
67	er_dynrepfilter_au0
68	er_dynrepfilterdt_au0
69	er_dynrepview_au0
70	er_er_attachments_au0
71	er_fldaction_au0
72	er_fldactioninfo_au0
73	er_fldlib_au0
74	er_fldresp_au0
75	er_fldvalidate_au0
76	er_formfld_au0
77	er_formgrpacc_au0
78	er_formlib_au0
79	er_formlibver_au0
80	er_formnotify_au0
81	er_formorgacc_au0
82	er_formsec_au0
83	er_formsharewith_au0
84	er_formstat_au0
85	er_grps_au0
86	er_invoice_au0
87	er_invoice_detail_au0
88	er_linkedforms_au0
89	er_mileachieved_au0
90	er_mileapndx_au0
91	er_milepayment_au0
92	er_milepayment_details_au0
93	er_milestone_au0
94	er_moredetails_au0
95	er_msgcntr_au0
96	er_objectshare_au0
97	ER_ORDER_ATTACHMENTS_AU0
98	ER_ORDER_ATTACHMENTS_AU1
99	ER_ORDER_AU0
100	ER_ORDER_AU1
101	ER_ORDER_HEADER_AU0
102	ER_ORDER_HEADER_AU1
103	ER_ORDER_USERS_AU0
104	ER_ORDER_USERS_AU1
105	er_patfacility_au0
106	er_patforms_au0
107	er_patlabs_au0
108	ER_PATLABS_AU1
109	er_patprot_au0
110	er_patstudystat_au0
111	er_pattxarm_au0
112	er_portal_au0
113	er_portal_logins_au0
114	er_portal_modules_au0
115	er_portal_poplevel_au0
116	er_repfldvalidate_au0
117	er_review_meeting_topic_au0
118	er_savedrep_au0
119	er_settings_au0
120	er_site_au0
121	er_specimen_appndx_auo
122	er_specimen_au0
123	ER_SPECIMEN_AU2
124	er_specimen_status_au0
125	er_status_history_au0
126	er_storage_au0
127	er_storage_kit_au0
128	er_storage_status_au0
129	er_study_au0
130	er_study_indide_au0
131	er_study_nih_grant_au0
132	er_study_site_add_info_au0
133	er_study_site_rights_au0
134	er_studyapndx_au0
135	er_studyforms_au0
136	er_studyid_au0
137	er_studynotify_au0
138	er_studysec_au0
139	er_studysites_apndx_au
140	er_studysites_au
141	er_studystat_au0
142	er_studyteam_au0
143	er_studytxarm_au0
144	er_studyver_au0
145	er_submission_au0
146	er_submission_board_au0
147	er_submission_proviso_au0
148	er_submission_status_au0
149	er_user_au0
150	er_usersite_au0
151	er_usr_lnks_au0
152	er_usrgrp_au0
153	er_xusright_au0


ESCH schema (52)
---------------------
1	event_assoc_au0
2	event_assoc_au2
3	event_def_au0
4	event_def_au1
5	sch_adverseve_au0
6	sch_advinfo_au0
7	sch_advnotify_au0
8	sch_alertnotify_au0
9	sch_bgtapndx_au0
10	sch_bgtapndx_au1
11	sch_bgtcal_au0
12	sch_bgtcal_au2
13	sch_bgtsection_au0
14	sch_bgtsection_au1
15	sch_bgtusers_au0
16	sch_bgtusers_au1
17	sch_budget_au0
18	sch_budget_au3
19	sch_codelst_au0
20	sch_crf_au0
21	sch_crflib_au0
22	sch_crflib_au2
23	sch_crfnotify_au0
24	sch_crfstat_au0
25	sch_dispatchmsg_au0
26	sch_docs_au0
27	sch_docs_au1
28	sch_event_crf_au0
29	sch_event_crf_au1
30	sch_event_kit_au0
31	sch_event_kit_au1
32	sch_eventcost_au0
33	sch_eventcost_au2
34	sch_eventdoc_au0
35	sch_eventdoc_au1
36	sch_events1_au0
37	sch_events1_au2
38	sch_eventstat_au0
39	sch_eventstat_au1
40	sch_eventusr_au0
41	sch_eventusr_au1
42	sch_evres_track_au0
43	sch_lineitem_au0
44	sch_lineitem_au1
45	SCH_PORTAL_FORMS_AU
46	SCH_PORTAL_FORMS_AU0
47	sch_protocol_visit_au2
48	sch_protocol_visit_auo
49	sch_protstat_au0
50	sch_protstat_au1
51	sch_subcost_item_au0
52	sch_subcost_item_visit_au0


EPAT schema (3)
---------------------
1	pat_perapndx_au0
2	pat_perid_au0
3	person_au0

=====================================================================================================================================
