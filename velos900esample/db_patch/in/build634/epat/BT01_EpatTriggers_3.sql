CREATE OR REPLACE TRIGGER PAT_PERAPNDX_AU0
AFTER UPDATE OF RID,FK_PER,IP_ADD,CREATOR,CREATED_ON,PK_PERAPNDX,PERAPNDX_URI,PERAPNDX_DATE,PERAPNDX_DESC,PERAPNDX_TYPE
ON PAT_PERAPNDX REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
   usr := getuser(:NEW.last_modified_by);
  audit_trail.record_transaction
    (raid, 'PAT_PERAPNDX', :OLD.rid, 'U', usr);
  IF NVL(:OLD.pk_perapndx,0) !=
     NVL(:NEW.pk_perapndx,0) THEN
     audit_trail.column_update
       (raid, 'PK_PERAPNDX',
       :OLD.pk_perapndx, :NEW.pk_perapndx);
  END IF;
  IF NVL(:OLD.fk_per,0) !=
     NVL(:NEW.fk_per,0) THEN
     audit_trail.column_update
       (raid, 'FK_PER',
       :OLD.fk_per, :NEW.fk_per);
  END IF;
  IF NVL(:OLD.perapndx_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.perapndx_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'PERAPNDX_DATE',
       to_char(:OLD.perapndx_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.perapndx_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.perapndx_desc,' ') !=
     NVL(:NEW.perapndx_desc,' ') THEN
     audit_trail.column_update
       (raid, 'PERAPNDX_DESC',
       :OLD.perapndx_desc, :NEW.perapndx_desc);
  END IF;
  IF NVL(:OLD.perapndx_uri,' ') !=
     NVL(:NEW.perapndx_uri,' ') THEN
     audit_trail.column_update
       (raid, 'PERAPNDX_URI',
       :OLD.perapndx_uri, :NEW.perapndx_uri);
  END IF;
  IF NVL(:OLD.perapndx_type,' ') !=
     NVL(:NEW.perapndx_type,' ') THEN
     audit_trail.column_update
       (raid, 'PERAPNDX_TYPE',
       :OLD.perapndx_type, :NEW.perapndx_type);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
      BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL ;
     END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL ;
     END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;
  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
     to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;
END;
/
CREATE OR REPLACE TRIGGER PAT_PERID_AU0
AFTER UPDATE OF RID,FK_PER,IP_ADD,CREATOR,PERID_ID,PK_PERID,CREATED_ON,FK_CODELST_IDTYPE
ON PAT_PERID REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
   usr VARCHAR2(100);
  old_modby VARCHAR2(100) ;
  new_modby VARCHAR2(100) ;
  old_codetype VARCHAR2(200) ;
  new_codetype VARCHAR2(200) ;

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

   usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'PAT_PERID', :OLD.rid, 'U', usr);

  IF NVL(:OLD.pk_perid,0) !=
     NVL(:NEW.pk_perid,0) THEN
     audit_trail.column_update
       (raid, 'PK_PERID',
       :OLD.pk_perid, :NEW.pk_perid);
  END IF;
  IF NVL(:OLD.fk_per,0) !=
     NVL(:NEW.fk_per,0) THEN
     audit_trail.column_update
       (raid, 'FK_PER',
       :OLD.fk_per, :NEW.fk_per);
  END IF;
  IF NVL(:OLD.fk_codelst_idtype,0) !=
     NVL(:NEW.fk_codelst_idtype,0) THEN
     SELECT trim(codelst_desc) INTO old_codetype
       FROM er_codelst WHERE pk_codelst =  :OLD.FK_CODELST_IDTYPE ;
     SELECT trim(codelst_desc) INTO new_codetype
       FROM er_codelst WHERE pk_codelst =  :NEW.FK_CODELST_IDTYPE ;
     audit_trail.column_update
       (raid, 'FK_CODELST_IDTYPE',old_codetype, new_codetype);
  END IF;
  IF NVL(:OLD.perid_id,' ') !=
     NVL(:NEW.perid_id,' ') THEN
     audit_trail.column_update
       (raid, 'PERID_ID',
       :OLD.perid_id, :NEW.perid_id);
  END IF;
  IF NVL(:OLD.rid,0) !=
     NVL(:NEW.rid,0) THEN
     audit_trail.column_update
       (raid, 'RID',
       :OLD.rid, :NEW.rid);
  END IF;
  IF NVL(:OLD.last_modified_by,0) !=
     NVL(:NEW.last_modified_by,0) THEN
   BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO old_modby
     FROM er_user
     WHERE pk_user = :OLD.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      old_modby := NULL ;
     END ;
    BEGIN
     SELECT TO_CHAR(pk_user) || ', '|| usr_lastname ||', '||usr_firstname
     INTO new_modby
     FROM er_user
     WHERE pk_user = :NEW.last_modified_by ;
     EXCEPTION WHEN NO_DATA_FOUND THEN
      new_modby := NULL ;
     END ;
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_BY',
       old_modby, new_modby);
  END IF;

  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN
     audit_trail.column_update
       (raid, 'LAST_MODIFIED_DATE',
            to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;
  IF NVL(:OLD.ip_add,' ') !=
     NVL(:NEW.ip_add,' ') THEN
     audit_trail.column_update
       (raid, 'IP_ADD',
       :OLD.ip_add, :NEW.ip_add);
  END IF;

END;
/
CREATE OR REPLACE TRIGGER PERSON_AU0
AFTER UPDATE
ON PERSON REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

  raid NUMBER(10);

  old_codetype VARCHAR2(200) ;

  new_codetype VARCHAR2(200) ;

  USR VARCHAR2(70);

  v_new_last_modified_by NUMBER;

  v_new_fk_codelst_gender NUMBER;

  v_old_fk_codelst_gender NUMBER;

  v_new_fk_codelst_race NUMBER;

  v_old_fk_codelst_race NUMBER;

  v_new_fk_codelst_prilang NUMBER;

  v_old_fk_codelst_prilang NUMBER;

  v_new_fk_codelst_marital NUMBER;

  v_old_fk_codelst_marital NUMBER;

  v_new_person_ethgrp NUMBER;

  v_old_person_ethgrp NUMBER;

  v_new_fk_codelst_citizen NUMBER;

  v_old_fk_codelst_citizen NUMBER;

  v_new_fk_codelst_nationality NUMBER;

  v_old_fk_codelst_nationality NUMBER;

  v_new_fk_codelst_employment NUMBER;

  v_old_fk_codelst_employment NUMBER;

  v_new_fk_codelst_edu NUMBER;

  v_old_fk_codelst_edu NUMBER;

  v_new_fk_codelst_pstat NUMBER;

  v_old_fk_codelst_pstat NUMBER;

  v_new_fk_codelst_bloodgrp NUMBER;

  v_old_fk_codelst_bloodgrp NUMBER;

  v_new_fk_codlst_ntype NUMBER;

  v_old_fk_codlst_ntype NUMBER;

  v_new_fk_codelst_religion NUMBER;

  v_old_fk_codelst_religion NUMBER;

  v_new_fk_timezone NUMBER;

  v_old_fk_timezone NUMBER;

  v_new_fk_codelst_pat_dth_cause NUMBER;

  v_old_fk_codelst_pat_dth_cause NUMBER;




  v_new_fk_codelst_ethnicity NUMBER;
  v_old_fk_codelst_ethnicity NUMBER;




BEGIN

 v_new_last_modified_by := :NEW.last_modified_by;

 BEGIN

   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' ||usr_firstname

   INTO usr FROM er_user WHERE pk_user = v_new_last_modified_by ;

  EXCEPTION WHEN NO_DATA_FOUND THEN

  USR := 'New User' ;

 END ;



  SELECT seq_audit.NEXTVAL INTO raid FROM dual;



  Audit_Trail.record_transaction

    (raid, 'PERSON', :OLD.rid, 'U', USR);



  IF NVL(:OLD.pk_person,0) !=

     NVL(:NEW.pk_person,0) THEN

     Audit_Trail.column_update

       (raid, 'PK_PERSON',

       :OLD.pk_person, :NEW.pk_person);

  END IF;

  IF NVL(:OLD.person_code,' ') !=

     NVL(:NEW.person_code,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_CODE',

       :OLD.person_code, :NEW.person_code);

  END IF;

  IF NVL(:OLD.person_altid,' ') !=

     NVL(:NEW.person_altid,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_ALTID',

       :OLD.person_altid, :NEW.person_altid);

  END IF;

  IF NVL(:OLD.person_lname,' ') !=

     NVL(:NEW.person_lname,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_LNAME',

       :OLD.person_lname, :NEW.person_lname);

  END IF;

  IF NVL(:OLD.person_fname,' ') !=

     NVL(:NEW.person_fname,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_FNAME',

       :OLD.person_fname, :NEW.person_fname);

  END IF;

  IF NVL(:OLD.person_mname,' ') !=

     NVL(:NEW.person_mname,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_MNAME',

       :OLD.person_mname, :NEW.person_mname);

  END IF;

  IF NVL(:OLD.person_suffix,' ') !=

     NVL(:NEW.person_suffix,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_SUFFIX',

       :OLD.person_suffix, :NEW.person_suffix);

  END IF;

  IF NVL(:OLD.person_prefix,' ') !=

     NVL(:NEW.person_prefix,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_PREFIX',

       :OLD.person_prefix, :NEW.person_prefix);

  END IF;

  IF NVL(:OLD.person_degree,' ') !=

     NVL(:NEW.person_degree,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_DEGREE',

       :OLD.person_degree, :NEW.person_degree);

  END IF;

  IF NVL(:OLD.person_dob,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=

     NVL(:NEW.person_dob,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN

     Audit_Trail.column_update

       (raid, 'PERSON_DOB',

       to_char(:OLD.person_dob,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.person_dob,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;

  IF NVL(:OLD.fk_codelst_gender,0) !=

     NVL(:NEW.fk_codelst_gender,0) THEN

     v_new_fk_codelst_gender := :NEW.fk_codelst_gender;

    BEGIN

    SELECT trim(codelst_desc) INTO new_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_new_fk_codelst_gender ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL;

    END ;

    BEGIN

     v_old_fk_codelst_gender := :OLD.fk_codelst_gender;

    SELECT trim(codelst_desc) INTO old_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_old_fk_codelst_gender ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_GENDER',

       old_codetype, new_codetype);

  END IF;

  IF NVL(:OLD.person_aka,' ') !=

     NVL(:NEW.person_aka,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_AKA',

       :OLD.person_aka, :NEW.person_aka);

  END IF;



  IF NVL(:OLD.fk_codelst_race,0) !=

     NVL(:NEW.fk_codelst_race,0) THEN

    BEGIN

     v_new_fk_codelst_race := :NEW.fk_codelst_race;

    SELECT trim(codelst_desc) INTO new_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_new_fk_codelst_race ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL;

    END ;

    BEGIN

     v_old_fk_codelst_race := :OLD.fk_codelst_race;

    SELECT trim(codelst_desc) INTO old_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_old_fk_codelst_race ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_RACE',

       old_codetype, new_codetype);

  END IF;



  IF NVL(:OLD.fk_codelst_prilang,0) !=

     NVL(:NEW.fk_codelst_prilang,0) THEN

    BEGIN

     v_new_fk_codelst_prilang := :NEW.fk_codelst_prilang;

    SELECT trim(codelst_desc) INTO new_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_new_fk_codelst_prilang ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL;

    END ;

    BEGIN

     v_old_fk_codelst_prilang := :OLD.fk_codelst_prilang;

    SELECT trim(codelst_desc) INTO old_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_old_fk_codelst_prilang ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_PRILANG',

       old_codetype, new_codetype);

  END IF;



  IF NVL(:OLD.fk_codelst_marital,0) !=

     NVL(:NEW.fk_codelst_marital,0) THEN

    BEGIN

     v_new_fk_codelst_marital := :NEW.fk_codelst_marital;

    SELECT trim(codelst_desc) INTO new_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_new_fk_codelst_marital ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL;

    END ;

    BEGIN

     v_old_fk_codelst_marital := :OLD.fk_codelst_marital;

    SELECT trim(codelst_desc) INTO old_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_old_fk_codelst_marital ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_MARITAL',

       old_codetype, new_codetype);

  END IF;



  IF NVL(:OLD.fk_account,0) !=

     NVL(:NEW.fk_account,0) THEN

     Audit_Trail.column_update

       (raid, 'FK_ACCOUNT',

       :OLD.fk_account, :NEW.fk_account);

  END IF;

  IF NVL(:OLD.person_ssn,' ') !=

     NVL(:NEW.person_ssn,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_SSN',

       :OLD.person_ssn, :NEW.person_ssn);

  END IF;

  IF NVL(:OLD.person_driv_lic,' ') !=

     NVL(:NEW.person_driv_lic,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_DRIV_LIC',

       :OLD.person_driv_lic, :NEW.person_driv_lic);

  END IF;

  IF NVL(:OLD.person_ethgrp,0) !=

     NVL(:NEW.person_ethgrp,0) THEN

    BEGIN

     v_new_person_ethgrp := :NEW.person_ethgrp;

    SELECT trim(codelst_desc) INTO new_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_new_person_ethgrp;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL;

    END ;

    BEGIN

     v_old_person_ethgrp := :OLD.person_ethgrp;

    SELECT trim(codelst_desc) INTO old_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_old_person_ethgrp ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL;

    END ;

     Audit_Trail.column_update

       (raid, 'PERSON_ETHGRP',

       old_codetype, new_codetype);

  END IF;



  IF NVL(:OLD.person_birth_place,' ') !=

     NVL(:NEW.person_birth_place,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_BIRTH_PLACE',

       :OLD.person_birth_place, :NEW.person_birth_place);

  END IF;

  IF NVL(:OLD.person_multi_birth,0) !=

     NVL(:NEW.person_multi_birth,0) THEN

     Audit_Trail.column_update

       (raid, 'PERSON_MULTI_BIRTH',

       :OLD.person_multi_birth, :NEW.person_multi_birth);

  END IF;

  IF NVL(:OLD.fk_codelst_citizen,0) !=

     NVL(:NEW.fk_codelst_citizen,0) THEN

    BEGIN

     v_new_fk_codelst_citizen := :NEW.fk_codelst_citizen ;

    SELECT trim(codelst_desc) INTO new_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_new_fk_codelst_citizen;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL;

    END ;

    BEGIN

     v_old_fk_codelst_citizen := :OLD.fk_codelst_citizen ;

    SELECT trim(codelst_desc) INTO old_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_old_fk_codelst_citizen ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_CITIZEN',

       old_codetype, new_codetype);

  END IF;



  IF NVL(:OLD.person_milvet,' ') !=

     NVL(:NEW.person_milvet,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_MILVET',

       :OLD.person_milvet, :NEW.person_milvet);

  END IF;

  IF NVL(:OLD.fk_codelst_nationality,0) !=

     NVL(:NEW.fk_codelst_nationality,0) THEN

    BEGIN

     v_new_fk_codelst_nationality := :NEW.fk_codelst_nationality;

    SELECT trim(codelst_desc) INTO new_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_new_fk_codelst_nationality ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL;

    END ;

    BEGIN

     v_old_fk_codelst_nationality := :OLD.fk_codelst_nationality;

    SELECT trim(codelst_desc) INTO old_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_old_fk_codelst_nationality ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_NATIONALITY',

       old_codetype, new_codetype);

  END IF;



  IF NVL(:OLD.person_deathdt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=

     NVL(:NEW.person_deathdt,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN

     Audit_Trail.column_update

       (raid, 'PERSON_DEATHDT',

       to_char(:OLD.person_deathdt,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.person_deathdt,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;

  IF NVL(:OLD.fk_site,0) !=

     NVL(:NEW.fk_site,0) THEN

     Audit_Trail.column_update

       (raid, 'FK_SITE',

       :OLD.fk_site, :NEW.fk_site);

  END IF;

  IF NVL(:OLD.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=

     NVL(:NEW.created_on,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN

     Audit_Trail.column_update

       (raid, 'CREATED_ON',

            to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));


  END IF;

  IF NVL(:OLD.fk_codelst_employment,0) !=

     NVL(:NEW.fk_codelst_employment,0) THEN

    BEGIN

     v_new_fk_codelst_employment := :NEW.fk_codelst_employment ;

    SELECT trim(codelst_desc) INTO new_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_new_fk_codelst_employment ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL;

    END ;

    BEGIN

     v_old_fk_codelst_employment := :OLD.fk_codelst_employment ;

    SELECT trim(codelst_desc) INTO old_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_old_fk_codelst_employment;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_EMPLOYMENT',

       old_codetype, new_codetype);

  END IF;



  IF NVL(:OLD.fk_codelst_edu,0) !=

     NVL(:NEW.fk_codelst_edu,0) THEN

    BEGIN

     v_new_fk_codelst_edu := :NEW.fk_codelst_edu ;

     SELECT trim(codelst_desc) INTO new_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_new_fk_codelst_edu;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL;

    END ;

    BEGIN

     v_old_fk_codelst_edu := :OLD.fk_codelst_edu ;

    SELECT trim(codelst_desc) INTO old_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_old_fk_codelst_edu;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_EDU',

       old_codetype, new_codetype);

  END IF;

/*

  IF NVL(:OLD.person_notes,' ') !=

     NVL(:NEW.person_notes,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_NOTES',

       :OLD.person_notes, :NEW.person_notes);

  END IF;
  */

  IF NVL(:OLD.fk_codelst_pstat,0) !=

     NVL(:NEW.fk_codelst_pstat,0) THEN

    BEGIN

     v_new_fk_codelst_pstat := :NEW.fk_codelst_pstat ;

     SELECT trim(codelst_desc) INTO new_codetype

    FROM ER_CODELST WHERE pk_codelst = v_new_fk_codelst_pstat ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL;

    END ;

    BEGIN

     v_old_fk_codelst_pstat := :OLD.fk_codelst_pstat ;

    SELECT trim(codelst_desc) INTO old_codetype

    FROM ER_CODELST WHERE pk_codelst = v_old_fk_codelst_pstat ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_PSTAT',

       old_codetype, new_codetype);

 END IF;



  IF NVL(:OLD.fk_codelst_bloodgrp,0) !=

     NVL(:NEW.fk_codelst_bloodgrp,0) THEN

    BEGIN

     v_new_fk_codelst_bloodgrp := :NEW.fk_codelst_bloodgrp;

     SELECT trim(codelst_desc) INTO new_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_new_fk_codelst_bloodgrp ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL;

    END ;

    BEGIN

     v_old_fk_codelst_bloodgrp := :OLD.fk_codelst_bloodgrp;

    SELECT trim(codelst_desc) INTO old_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_old_fk_codelst_bloodgrp ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_BLOODGRP',

       old_codetype, new_codetype);

  END IF;



  IF NVL(:OLD.person_address1,' ') !=

     NVL(:NEW.person_address1,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_ADDRESS1',

       :OLD.person_address1, :NEW.person_address1);

  END IF;

  IF NVL(:OLD.person_address2,' ') !=

     NVL(:NEW.person_address2,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_ADDRESS2',

       :OLD.person_address2, :NEW.person_address2);

  END IF;

  IF NVL(:OLD.person_city,' ') !=

     NVL(:NEW.person_city,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_CITY',

       :OLD.person_city, :NEW.person_city);

  END IF;

  IF NVL(:OLD.person_state,' ') !=

     NVL(:NEW.person_state,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_STATE',

       :OLD.person_state, :NEW.person_state);

  END IF;

  IF NVL(:OLD.person_zip,' ') !=

     NVL(:NEW.person_zip,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_ZIP',

       :OLD.person_zip, :NEW.person_zip);

  END IF;

  IF NVL(:OLD.person_country,' ') !=

     NVL(:NEW.person_country,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_COUNTRY',

       :OLD.person_country, :NEW.person_country);

  END IF;

  IF NVL(:OLD.person_hphone,' ') !=

     NVL(:NEW.person_hphone,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_HPHONE',

       :OLD.person_hphone, :NEW.person_hphone);

  END IF;

  IF NVL(:OLD.person_bphone,' ') !=

     NVL(:NEW.person_bphone,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_BPHONE',

       :OLD.person_bphone, :NEW.person_bphone);

  END IF;

  IF NVL(:OLD.person_email,' ') !=

     NVL(:NEW.person_email,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_EMAIL',

       :OLD.person_email, :NEW.person_email);

  END IF;

  IF NVL(:OLD.person_county,' ') !=

     NVL(:NEW.person_county,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_COUNTY',

       :OLD.person_county, :NEW.person_county);

  END IF;

  IF NVL(:OLD.person_regdate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=

     NVL(:NEW.person_regdate,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN

     Audit_Trail.column_update

       (raid, 'PERSON_REGDATE',

       to_char(:OLD.person_regdate,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.person_regdate,PKG_DATEUTIL.F_GET_DATEFORMAT));

  END IF;

  IF NVL(:OLD.person_regby,0) !=

     NVL(:NEW.person_regby,0) THEN

     Audit_Trail.column_update

       (raid, 'PERSON_REGBY',

       :OLD.person_regby, :NEW.person_regby);

  END IF;

  IF NVL(:OLD.fk_codlst_ntype,0) !=

     NVL(:NEW.fk_codlst_ntype,0) THEN

    BEGIN

     v_new_fk_codlst_ntype := :NEW.fk_codlst_ntype;

     SELECT trim(codelst_desc) INTO new_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_new_fk_codlst_ntype ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL;

    END ;

    BEGIN

     v_old_fk_codlst_ntype := :OLD.fk_codlst_ntype;

    SELECT trim(codelst_desc) INTO old_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_old_fk_codlst_ntype ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODLST_NTYPE',

       old_codetype, new_codetype);

  END IF;



  IF NVL(:OLD.person_mother_name,' ') !=

     NVL(:NEW.person_mother_name,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_MOTHER_NAME',

       :OLD.person_mother_name, :NEW.person_mother_name);

  END IF;

  IF NVL(:OLD.fk_person_mother,0) !=

     NVL(:NEW.fk_person_mother,0) THEN

     Audit_Trail.column_update

       (raid, 'FK_PERSON_MOTHER',

       :OLD.fk_person_mother, :NEW.fk_person_mother);

  END IF;

  IF NVL(:OLD.fk_codelst_religion,0) !=

     NVL(:NEW.fk_codelst_religion,0) THEN

    BEGIN

     v_new_fk_codelst_religion := :NEW.fk_codelst_religion ;

     SELECT trim(codelst_desc) INTO new_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_new_fk_codelst_religion ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL;

    END ;

    BEGIN

     v_old_fk_codelst_religion := :OLD.fk_codelst_religion ;

    SELECT trim(codelst_desc) INTO old_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_old_fk_codelst_religion ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_RELIGION',

       old_codetype, new_codetype);

  END IF;



  IF NVL(:OLD.rid,0) !=

     NVL(:NEW.rid,0) THEN

     Audit_Trail.column_update

       (raid, 'RID',

       :OLD.rid, :NEW.rid);

  END IF;

  IF NVL(:OLD.last_modified_by,0) !=

     NVL(:NEW.last_modified_by,0) THEN

     Audit_Trail.column_update

       (raid, 'LAST_MODIFIED_BY',

       :OLD.last_modified_by, :NEW.last_modified_by);

  END IF;

  IF NVL(:OLD.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) !=

     NVL(:NEW.last_modified_date,TO_DATE(eres.pkg_dateutil.f_get_future_null_date_str,eres.pkg_dateutil.f_get_dateformat)) THEN

     Audit_Trail.column_update

       (raid, 'LAST_MODIFIED_DATE',

    to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));


  END IF;

  IF NVL(:OLD.ip_add,' ') !=

     NVL(:NEW.ip_add,' ') THEN

     Audit_Trail.column_update

       (raid, 'IP_ADD',

       :OLD.ip_add, :NEW.ip_add);

  END IF;

  IF NVL(:OLD.fk_timezone,0) !=

     NVL(:NEW.fk_timezone,0) THEN

    BEGIN

     v_new_fk_timezone := :NEW.fk_timezone ;

     SELECT trim(tz_description) INTO new_codetype

    FROM sch_timezones WHERE pk_tz =  v_new_fk_timezone ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL;

    END ;

    BEGIN

     v_old_fk_timezone := :OLD.fk_timezone ;

     SELECT trim(tz_description) INTO old_codetype

    FROM sch_timezones WHERE pk_tz = v_old_fk_timezone ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_TIMEZONES',

       old_codetype, new_codetype);

  END IF;

--JM: 07Jun2007
  IF NVL(:OLD.person_phyother,' ') !=

     NVL(:NEW.person_phyother,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_PHYOTHER',

       :OLD.person_phyother, :NEW.person_phyother);

  END IF;

  IF NVL(:OLD.person_orgother,' ') !=

     NVL(:NEW.person_orgother,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_ORGOTHER',

       :OLD.person_orgother, :NEW.person_orgother);

  END IF;

  IF NVL(:OLD.person_splaccess,' ') !=

     NVL(:NEW.person_splaccess,' ') THEN

    BEGIN

    select F_GETSPL_ACCESS(:NEW.person_splaccess) INTO new_codetype  from dual;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL;

    END;

    BEGIN

    select F_GETSPL_ACCESS(:OLD.person_splaccess) INTO old_codetype  from dual;

    EXCEPTION WHEN NO_DATA_FOUND THEN

    old_codetype := NULL;

    END;

    Audit_Trail.column_update

      (raid, 'PERSON_SPLACCESS',

       old_codetype, new_codetype);

    END IF;


    IF NVL(:OLD.fk_codelst_ethnicity,0) !=

     NVL(:NEW.fk_codelst_ethnicity,0) THEN

    BEGIN

     v_new_fk_codelst_ethnicity := :NEW.fk_codelst_ethnicity ;

     select codelst_desc INTO new_codetype

     FROM er_codelst where pk_codelst=  v_new_fk_codelst_ethnicity;


    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL;

    END ;

    BEGIN

    v_old_fk_codelst_ethnicity := :OLD.fk_codelst_ethnicity ;

    select codelst_desc INTO old_codetype

    FROM er_codelst where pk_codelst=   v_old_fk_codelst_ethnicity ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

    old_codetype := NULL;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_ETHNICITY',

       old_codetype, new_codetype);

    END IF;

 -- Added by Gopu to fix the issue #2957
 --JM: modified
  IF NVL(:OLD.PERSON_ADD_RACE,' ') !=

    NVL(:NEW.PERSON_ADD_RACE,' ') THEN

    BEGIN

    select F_GETSPL_ACCESS(:NEW.person_add_race) INTO new_codetype  from dual;


    EXCEPTION WHEN NO_DATA_FOUND THEN

    new_codetype := NULL;

    END ;

    BEGIN

    select F_GETSPL_ACCESS(:OLD.person_add_race) INTO old_codetype  from dual;

    EXCEPTION WHEN NO_DATA_FOUND THEN

    old_codetype := NULL;

    END ;



     Audit_Trail.column_update

       (raid, 'PERSON_ADD_RACE',

       old_codetype, new_codetype);

    END IF;



    IF NVL(:OLD.person_add_ethnicity,' ') !=

    NVL(:NEW.person_add_ethnicity,' ') THEN

    BEGIN


    select F_GETSPL_ACCESS(:NEW.person_add_ethnicity) INTO new_codetype  from dual;


    EXCEPTION WHEN NO_DATA_FOUND THEN

    new_codetype := NULL;

    END ;

    BEGIN


    select F_GETSPL_ACCESS(:OLD.person_add_ethnicity) INTO old_codetype  from dual;

    EXCEPTION WHEN NO_DATA_FOUND THEN

    old_codetype := NULL;

    END ;

    Audit_Trail.column_update

       (raid, 'PERSON_ADD_ETHNICITY',

       old_codetype, new_codetype);

    END IF;



  IF NVL(:OLD.fk_codelst_pat_dth_cause,0) !=

     NVL(:NEW.fk_codelst_pat_dth_cause,0) THEN

    BEGIN

     v_new_fk_codelst_pat_dth_cause := :NEW.fk_codelst_pat_dth_cause ;

     SELECT trim(codelst_desc) INTO new_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_new_fk_codelst_pat_dth_cause ;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     new_codetype := NULL;

    END ;

    BEGIN

     v_old_fk_codelst_pat_dth_cause := :OLD.fk_codelst_pat_dth_cause ;

    SELECT trim(codelst_desc) INTO old_codetype

    FROM ER_CODELST WHERE pk_codelst =  v_old_fk_codelst_pat_dth_cause;

    EXCEPTION WHEN NO_DATA_FOUND THEN

     old_codetype := NULL;

    END ;

     Audit_Trail.column_update

       (raid, 'FK_CODELST_PAT_DTH_CAUSE',

       old_codetype, new_codetype);

  END IF;


IF NVL(:OLD.cause_of_death_other,' ') !=

     NVL(:NEW.cause_of_death_other,' ') THEN

     Audit_Trail.column_update

       (raid, 'CAUSE_OF_DEATH_OTHER',

       :OLD.cause_of_death_other, :NEW.cause_of_death_other);

  END IF;

 --JM:
 --Commented by Manimaran to fix the Bug 2743.
/*IF NVL(:OLD.person_notes_clob,' ') !=

     NVL(:NEW.person_notes_clob,' ') THEN

     Audit_Trail.column_update

       (raid, 'PERSON_NOTES_CLOB',

       dbms_lob.substr(:OLD.person_notes_clob,4000), dbms_lob.substr(:NEW.person_notes_clob,4000));

  END IF;
*/

  IF NVL(:OLD.pat_facilityid,' ') !=

     NVL(:NEW.pat_facilityid,' ') THEN

     Audit_Trail.column_update

       (raid, 'PAT_FACILITYID',

       :OLD.pat_facilityid, :NEW.pat_facilityid);

  END IF;

END;
/
