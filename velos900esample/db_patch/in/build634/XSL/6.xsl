<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>

<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>


<xsl:template match="/">

<HTML>
<HEAD><TITLE> <xsl:value-of select="$repName" /> </TITLE>

<link rel="stylesheet" href="./styles/common.css" type="text/css"/>

</HEAD>

<BODY class="repBody">
<xsl:if test="$cond='T'">
<table width="100%" >

<tr class="reportGreyRow">
<td class="reportPanel"> 

VELMESSGE[M_Download_ReportIn]:<!-- Download the report in -->  
<A href='{$wd}' >
<img border="0" title="VELLABEL[L_Word_Format]" alt="VELLABEL[L_Word_Format]" src="./images/word.GIF" ></img><!-- Word Format -->
</A> 
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$xd}' >
<img border="0" title="VELLABEL[L_Excel_Format]" alt="VELLABEL[L_Excel_Format]" src="./images/excel.GIF" ></img><!-- Excel Format -->
</A>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$hd}' >
VELLABEL[L_Printer_FriendlyFormat]<!-- Printer Friendly Format -->
</A> 
</td>

</tr>
</table>
</xsl:if>
<table class="reportborder" width="100%">
<tr><td>
<xsl:apply-templates select="ROWSET"/>
</td></tr>
</table>
</BODY>
</HTML>
</xsl:template> 

<xsl:template match="ROWSET">
<TABLE WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<TR>
<TD WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</TD>
</TR>
</xsl:if>

<TR>
<TD class="reportName" WIDTH="100%" ALIGN="CENTER">
<xsl:value-of select="$repName" />
</TD>
</TR>
</TABLE>


<TABLE WIDTH="100%">
<TR>
<TD class="reportData" WIDTH="100%" ALIGN="CENTER">
VELLABEL[L_Selected_Filter_S]<!-- Selected Filter(s) -->: <xsl:value-of select="$argsStr" />
</TD>
</TR>
<tr><td>&#xa0;</td></tr> 
<tr>
	<td class="reportLabel" ALIGN="CENTER"><b>VELLABEL[L_Total_MatchingRows]<!-- Total Matching Rows -->: <xsl:value-of select="count(//ROW)" /></b></td>
</tr>
</TABLE>


<hr class="thinLine" />

<TABLE WIDTH="100%" BORDER="1">
<TR>
<TH class="reportHeading"  WIDTH="15%" ALIGN="CENTER">
VELLABEL[L_Number]<!-- Number -->
</TH>

<TH class="reportHeading"  WIDTH="15%" ALIGN="CENTER">
VELLABEL[L_Std_StartDate]<!-- Study Start Date -->
</TH>


<TH class="reportHeading"  WIDTH="35%" ALIGN="CENTER">
VELLABEL[L_Title]<!-- Title -->
</TH>
<TH class="reportHeading"  WIDTH="15%" ALIGN="CENTER">
VELLABEL[L_Therapeutic_Area]<!-- Therapeutic Area --> 

</TH>
<TH class="reportHeading"  WIDTH="20%" ALIGN="CENTER">
VELLABEL[L_Data_Manager]<!-- Data Manager -->

</TH>
</TR>
<xsl:apply-templates select="ROW"/>
</TABLE>

<hr class="thinLine" />

<TABLE WIDTH="100%" >
<TR>
<TD class="reportFooter" WIDTH="50%" ALIGN="LEFT">
VELLABEL[L_Report_By]<!-- Report By -->:<xsl:value-of select="$repBy" />
</TD>
<TD class="reportFooter" WIDTH="50%" ALIGN="RIGHT">
VELLABEL[L_Date]<!-- Date -->:<xsl:value-of select="$repDate" />
</TD>
</TR>
</TABLE>
<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDHT="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 

<xsl:template match="ROW">

<xsl:choose>
<xsl:when test="number(position() mod 2)=0" >
<TR class="reportEvenRow" VALIGN="TOP">
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="STUDY_NUMBER" />
</TD>

<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="STUDY_ACTUALDT" />
</TD>





<TD class="reportData" WIDTH="35%" >
<xsl:value-of select="STUDY_TITLE" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="TA" />
</TD>
<TD class="reportData" WIDTH="20%" >
<xsl:value-of select="STUDY_AUTHOR" />
</TD>
</TR>
</xsl:when> 
<xsl:otherwise>
<TR class="reportOddRow" VALIGN="TOP">
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="STUDY_NUMBER" />
</TD>

<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="STUDY_ACTUALDT" />
</TD>



<TD class="reportData" WIDTH="35%" >
<xsl:value-of select="STUDY_TITLE" />
</TD>
<TD class="reportData" WIDTH="15%" >
<xsl:value-of select="TA" />
</TD>
<TD class="reportData" WIDTH="20%" >
<xsl:value-of select="STUDY_AUTHOR" />
</TD>
</TR>
</xsl:otherwise>
</xsl:choose> 
</xsl:template> 
</xsl:stylesheet>
