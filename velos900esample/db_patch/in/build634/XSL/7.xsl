<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>

<xsl:key name="RecordsByStudy" match="ROW" use="PK_STUDY" />
<xsl:key name="RecordsByResType" match="ROW" use="RESEARCH_TYPE" />
<xsl:key name="RecordsByStudyStat" match="ROW" use="STUDY_STAT" />
<xsl:key name="RecordsByPhase" match="ROW" use="PHASE" />
<xsl:key name="RecordsByTA" match="ROW" use="TAREA" />

<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>


<xsl:template match="/">
<HTML>
<HEAD><TITLE> <xsl:value-of select="$repName" /> </TITLE>

<link rel="stylesheet" href="./styles/common.css" type="text/css"/>

	</HEAD>
<BODY class="repBody">
<xsl:if test="$cond='T'">
<table width="100%" >
<tr class="reportGreyRow">
<td class="reportPanel"> 
VELMESSGE[M_Download_ReportIn]:<!-- Download the report in --> 
<A href='{$wd}' >
<img border="0" title="VELLABEL[L_Word_Format]" alt="VELLABEL[L_Word_Format]" src="./images/word.GIF" ></img><!-- Word Format -->
</A> 
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$xd}' >
<img border="0" title="VELLABEL[L_Excel_Format]" alt="VELLABEL[L_Excel_Format]" src="./images/excel.GIF" ></img><!-- Excel Format -->
</A>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$hd}' >
VELLABEL[L_Printer_FriendlyFormat]<!-- Printer Friendly Format -->
</A> 
</td>
</tr>
</table>
</xsl:if>
<table class="reportborder">
<tr><td>
<xsl:apply-templates select="ROWSET"/>
</td></tr>
</table>
</BODY>
</HTML>
</xsl:template> 

<xsl:template match="ROWSET">
<TABLE WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<TR>
<TD WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</TD>
</TR>
</xsl:if>
<TR>
<TD class="reportName" WIDTH="100%" ALIGN="CENTER" >
<xsl:value-of select="$repName" /> : <xsl:value-of select="ROW/SITE_NAME" />
</TD>
</TR>
<tr><td class="reportFooter" align="center">
VELMESSGE[M_RptInclStd_AccruingStat]<!-- This report includes all studies, irrespective of accruing status -->
</td></tr>
<tr><td>&#xa0;
</td></tr>
<TR>
<TD class="reportData"  ALIGN="Center">
VELMESSGE[M_Selected_DtRangeFilter]<!-- Selected Date Range Filter -->: <xsl:value-of select="$argsStr" />
</TD>
</TR>
</TABLE>

<!--
<table border ="0" width ="100%">
	   <tr class="reportGraphRow">
	   	   
		   <td class="reportData" WIDTH="20%" ALIGN="LEFT" valign="top">
		   	   
			   <table WIDTH="100%" BORDER="0" cellpadding ="0" cellspacing = "0">
			   		  <tr>
			   		  <td class="reportGraphRow" ALIGN="CENTER" colspan = "3"> <b>By Research Type </b>
			   		  </td>
			   		  </tr>
			   		  <tr>
			   		  <td>&#xa0;
			   		  </td>
			   		  </tr>
			   		  <xsl:for-each select="ROW[count(. | key('RecordsByResType', RESEARCH_TYPE)[1])=1]">
			   		  <xsl:variable name="str" select="key('RecordsByResType', RESEARCH_TYPE)" />	
			   		  <tr>
			   		  <xsl:variable name="v_restype">
			   		  <xsl:value-of select="RESEARCH_TYPE" /> 
			   		  </xsl:variable>
			   		  <xsl:if test="$v_restype!= ''">
			   		  <td class="reportGraphRow" ALIGN="LEFT">
			   	   	  	  <table width="100%" border="0">
			   	   		  <tr>
			   		  		 <xsl:variable name="restype_count" select="format-number(((count($str[RESEARCH_TYPE=$v_restype]) div count(//ROW))*100),'###.00')"/>
				   			<td width = "70%" class="reportGraphRow" ALIGN="LEFT">
				   		  <xsl:value-of select="$v_restype" />&#xa0;:
				   		  </td>
						<td class="reportGraphRow" ALIGN="LEFT" width="30%">
				   		  <xsl:value-of select="count($str[RESEARCH_TYPE=$v_restype])" />
				   		  </td>
						  
						</tr></table></td>
						</xsl:if>
						</tr>
						</xsl:for-each>
						</table>
				</td>
				
		   
				<td class="reportData" WIDTH="30%" ALIGN="LEFT" valign="top">
		   	   
			   <table WIDTH="100%" BORDER="0" cellpadding ="0" cellspacing = "0">
			   		  <tr>
			   		  <td class="reportGraphRow" ALIGN="CENTER" colspan = "3"> <b>By VELLABEL[Std_Study] Status </b>
			   		  </td>
			   		  </tr>
			   		  <tr>
			   		  <td>&#xa0;
			   		  </td>
			   		  </tr>
			   		  <xsl:for-each select="ROW[count(. | key('RecordsByStudyStat', STUDY_STAT)[1])=1]">
			   		  <xsl:variable name="str" select="key('RecordsByStudyStat', STUDY_STAT)" />	
			   		  <tr>
			   		  <xsl:variable name="v_studystat">
			   		  <xsl:value-of select="STUDY_STAT" /> 
			   		  </xsl:variable>
			   		  <xsl:if test="$v_studystat!= ''">
			   		  <td width = "60%" class="reportGraphRow" ALIGN="RIGHT">
							   <xsl:value-of select="$v_studystat" />&#xa0; :
				   		  </td>
						  <td class="reportGraphRow" ALIGN="CENTER" width="10%">
				   		  <xsl:value-of select="count($str[STUDY_STAT=$v_studystat])" />
				   		  </td>
					  <td class="reportGraphRow" ALIGN="LEFT">
			   	   	  	  <table width="100%" border="0">
			   	   		  <tr><td width = "30%" class="reportGraphRow" ALIGN="LEFT">
			   		  		  <xsl:variable name="studystat_count" select="format-number(((count($str[STUDY_STAT=$v_studystat]) div count(//ROW[STUDY_STAT!='']))*100),'###.00')"/>
				   			  <table width="{$studystat_count}" border="0">
								 <tr class="reportGraphBar">
								 <td >
								 <xsl:text>&#xa0; </xsl:text>
								 </td> 
								 </tr> 
								 </table> </td>
							  
							  
						  </tr></table></td>
						</xsl:if>
						</tr>
						</xsl:for-each>
						</table>
				</td>
				
				
				
				
			<td class="reportData" width="30%" valign="top">
	
				<table WIDTH="100%" BORDER="0" cellpadding ="0" cellspacing = "0">
				<tr><td class="reportGraphRow" ALIGN="CENTER" colspan = "3"> <b>By Therapeutic Area</b>
				</td></tr>
				<tr>
				<td>&#xa0;</td></tr>
				<xsl:for-each select="ROW[count(. | key('RecordsByTA', TAREA)[1])=1]">
				<xsl:variable name="str" select="key('RecordsByTA', TAREA)" />	
				<tr>
				<xsl:variable name="v_ta">
				<xsl:value-of select="TAREA" /> 
				</xsl:variable>
				<xsl:if test="$v_ta!= ''">
				<td class="reportGraphRow" ALIGN="LEFT">
					<table width="100%" border="0">
					<tr>
					<td width = "60%" class="reportGraphRow" ALIGN="RIGHT">
					<xsl:value-of select="$v_ta" />&#xa0;: 
					</td>
					<td class="reportGraphRow" ALIGN="CENTER" width="10%">
					<xsl:value-of select="count($str[TAREA=$v_ta])" />
					</td>
					<td class="reportGraphRow" ALIGN="LEFT" width="30%">
					<xsl:variable name="ta_count" select="format-number(((count($str[TAREA=$v_ta]) div count(//ROW[TAREA!='']))*100),'###.00')"/>
								  <table width="{$ta_count}%" border="0">
								  <tr class="reportGraphBar">
								  <td >
								  <xsl:text>&#xa0; </xsl:text>
								  </td> 
								  </tr> 
								  </table> 
					</td>
					</tr></table>
				</td>
			</xsl:if>
			</tr>
		</xsl:for-each>

		</table>

</td>
					
					<td class="reportData" WIDTH="15%" ALIGN="LEFT" valign="top">
		   	   
			   <table WIDTH="100%" BORDER="0" cellpadding ="0" cellspacing = "0">
			   		  <tr>
			   		  <td class="reportGraphRow" ALIGN="CENTER" colspan = "3"> <b>By Phase </b>
			   		  </td>
			   		  </tr>
			   		  <tr>
			   		  <td>&#xa0;
			   		  </td>
			   		  </tr>
			   		  <xsl:for-each select="ROW[count(. | key('RecordsByPhase', PHASE)[1])=1]">
			   		  <xsl:variable name="str" select="key('RecordsByPhase', PHASE)" />	
			   		  <tr>
			   		  <xsl:variable name="v_phase">
			   		  <xsl:value-of select="PHASE" /> 
			   		  </xsl:variable>
			   		  <xsl:if test="$v_phase!= ''">
			   		  <td class="reportGraphRow" ALIGN="RIGHT">
			   	   	  	  <table width="100%" border="0">
			   	   		  <tr>
			   		  		  <xsl:variable name="phase_count" select="format-number(((count($str[PHASE=$v_phase]) div count(//ROW))*100),'###.00')"/>
				   			  <td width = "70%" class="reportGraphRow" ALIGN="LEFT">
				   		  <xsl:value-of select="$v_phase" />&#xa0;:
				   		  </td>
						  <td class="reportGraphRow" ALIGN="LEFT" width="20%">
				   		  <xsl:value-of select="count($str[PHASE=$v_phase])" />
				   		  </td>
						</tr></table></td>
						</xsl:if>
						</tr>
						</xsl:for-each>
						</table>
				</td>
				
					

</tr>
<tr><td>&#xa0;</td></tr> 
</table>
--> 
<!-- <tr>	<td class="reportLabel" ALIGN="CENTER" colspan="4"><b>Total Matching Rows: <xsl:value-of select="count($str[PK_STUDY=$v_study])" /></b></td>
			</tr>	-->			
			



<TABLE WIDTH="100%" ALIGN="CENTER" BORDER="1">
<TR>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">
VELLABEL[L_Study_Number]<!-- Study Number -->
</TH>

<TH class="reportHeading" WIDTH="8%" ALIGN="CENTER">
VELLABEL[L_Std_StartDate]<!-- Study Start Date -->
</TH>

<TH class="reportHeading" WIDTH="20%" ALIGN="CENTER">
VELLABEL[L_Study_Title]<!-- Study Title -->
</TH>

<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">
VELLABEL[L_Research_Type]<!-- Research Type -->
</TH>

<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">
VELLABEL[L_Study_Type]<!-- Study Type -->
</TH>

<TH class="reportHeading" WIDTH="8%" ALIGN="CENTER">
VELLABEL[L_Phase]<!-- Phase -->
</TH>

<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">
VELLABEL[L_Division]<!-- Division -->
</TH>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">
VELLABEL[L_Therapeutic_Area]<!-- Therapeutic Area -->
</TH>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">
VELLABEL[L_Status]<!-- Status -->
</TH>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">
VELMESSGE[M_User_NameOrRole]<!-- User Name / Role -->
</TH>
</TR>
<!-- </TABLE> --> 

<xsl:for-each select="ROW[count(. | key('RecordsByStudy', PK_STUDY)[1])=1]">
<xsl:text>
</xsl:text>
<!-- <TABLE WIDTH="100%" BORDER="1"> --> 

<xsl:choose>
<xsl:when test="number(position() mod 2)=0" >

<TR class="reportEvenRow" valign = "top">

<!--TR class="reportGreyRow"-->


<TD class="reportData" ALIGN="left" >
<xsl:value-of select="STUDY_NUMBER" />
</TD>

<TD class="reportData" ALIGN="left" >
<xsl:value-of select="STUDY_ACTUALDT" />
</TD>


<TD class="reportData" ALIGN="LEFT" >
<xsl:value-of select="STUDY_TITLE" />
</TD>

<TD class="reportData" ALIGN="LEFT" >
<xsl:value-of select="RESEARCH_TYPE" />
</TD>

<TD class="reportData" ALIGN="LEFT" >
<xsl:value-of select="STUDY_TYPE" />
</TD>

<TD class="reportData" ALIGN="LEFT" >
<xsl:value-of select="STUDY_PHASE" />
</TD>

<TD class="reportData" ALIGN="left">
<xsl:value-of select="DIVISION" />
</TD>
<TD class="reportData" ALIGN="left">
<xsl:value-of select="TAREA" />
</TD>
<TD class="reportData" ALIGN="left" >
<xsl:value-of select="STUDY_STAT" />
</TD>

<TD class="reportData" width="20%">

<TABLE width="100%" BORDER="1">
<xsl:for-each select="key('RecordsByStudy', PK_STUDY)">

<TR>
<TD class="reportData" WIDTH="50%"  ALIGN="left">

<xsl:value-of select="USR_FIRSTNAME" />
<xsl:text>&#xa0;</xsl:text>
<xsl:value-of select="USR_LASTNAME" />

</TD>
<TD class="reportData" WIDTH="50%"  ALIGN="left">
<xsl:value-of select="TEAM_ROLE" />
</TD>
</TR>
</xsl:for-each>

</TABLE>
</TD>
</TR>
</xsl:when> 

<xsl:otherwise>

<TR class="reportOddRow" valign = "top">

<TD class="reportData" ALIGN="left" >
<xsl:value-of select="STUDY_NUMBER" />
</TD>

<TD class="reportData" ALIGN="left" >
<xsl:value-of select="STUDY_ACTUALDT" />
</TD>


<TD class="reportData" ALIGN="LEFT" >
<xsl:value-of select="STUDY_TITLE" />
</TD>

<TD class="reportData" ALIGN="LEFT" >
<xsl:value-of select="RESEARCH_TYPE" />
</TD>

<TD class="reportData" ALIGN="LEFT" >
<xsl:value-of select="STUDY_TYPE" />
</TD>

<TD class="reportData" ALIGN="LEFT" >
<xsl:value-of select="STUDY_PHASE" />
</TD>
<TD class="reportData" ALIGN="left">
<xsl:value-of select="DIVISION" />
</TD>
<TD class="reportData" ALIGN="left">
<xsl:value-of select="TAREA" />
</TD>
<TD class="reportData" ALIGN="left" >
<xsl:value-of select="STUDY_STAT" />
</TD>


<TD class="reportData" width="20%">

<TABLE width="100%" BORDER="1">
<xsl:for-each select="key('RecordsByStudy', PK_STUDY)">

<TR>
<TD class="reportData" WIDTH="50%"  ALIGN="left">

<xsl:value-of select="USR_FIRSTNAME" />
<xsl:text>&#xa0;</xsl:text>
<xsl:value-of select="USR_LASTNAME" />

</TD>
<TD class="reportData" WIDTH="50%"  ALIGN="left">
<xsl:value-of select="TEAM_ROLE" />
</TD>
</TR>
</xsl:for-each>

</TABLE>
</TD>
</TR>

</xsl:otherwise>
</xsl:choose> 

</xsl:for-each>
</TABLE>


<TABLE WIDTH="100%" >
<TR>
<TD class="reportFooter" WIDTH="50%" ALIGN="LEFT">
VELLABEL[L_Report_By]<!-- Report By -->:<xsl:value-of select="$repBy" />
</TD>
<TD class="reportFooter" WIDTH="50%" ALIGN="RIGHT">
VELLABEL[L_Date]<!-- Date -->:<xsl:value-of select="$repDate" />
</TD>
</TR>
</TABLE>
<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDHT="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 
</xsl:stylesheet>
