<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>

<xsl:key name="cordRegId" match="ROWSET" use="concat(CORD_REGISTRY_ID,' ')"/>
<xsl:key name="cbbId" match="ROWSET" use="concat(CBBID,' ')"/>


<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="pd"/>

<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>

<xsl:template match="/">
<HTML>
<HEAD>
<link rel="stylesheet" href="./styles/common.css" type="text/css"/>
</HEAD>
<BODY class="repBody">
<table class="reportborder" width ="100%">
<tr><td>
<xsl:apply-templates select="ROWSET"/>
</td></tr>
</table>
</BODY>
</HTML>
</xsl:template> 




<xsl:template match="ROWSET">

<table WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</td>
</tr>
</xsl:if>
<tr>
<td WIDTH="100%" ALIGN="CENTER">
<b><!--xsl:value-of select="$repName" /--> <font size ="4"> National Marrow Donor Program® </font>
</b>
</td>
</tr>
<tr>
<td class="reportName" WIDTH="100%" ALIGN="CENTER">
<b><!--xsl:value-of select="$repName" /--><font size ="4"> CBU Detail Report for HPC, Cord Blood  </font>
</b>
</td>
</tr>
</table>
<br></br>
<br></br>

<table>
<tr>
<td align="left"> Cord Blood Bank / Registry Name: 
<xsl:value-of select="ROW/CORD_REGISTRY_ID"/>
</td>
<td align="right"> Cord Blood Bank ID: 
<xsl:value-of select="ROW/CBB_ID"/>
</td>

</tr>
<tr>
<td align="left"> CBU Storage Location: 
<xsl:value-of select="ROW/STORAGE_LOC"/>
</td>
<td align="right"> 
</td>

</tr>


</table>
<hr class="thickLine" width="100%"/>
<xsl:apply-templates select="ROW"/>

<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDHT="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 


<xsl:template match="ROW">
<xsl:for-each select="//ROW">

 <b><font size="3">IDS </font></b> 

<table>
<tr>
<TD> CBU Registry ID: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="CORD_REGISTRY_ID" /> 
</TD>
<TD> Maternal Registry ID: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="REGISTRY_MATERNAL_ID" />
</TD>
</tr>

<tr>
<TD> CBU Local ID: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="CORD_LOCAL_CBU_ID" />
</TD>
<TD> Maternal Local ID: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="MATERNAL_LOCAL_ID" />
</TD>
</tr>

<tr>
<TD> ISBT Donation Identification Number:  </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="CORD_ISBI_DIN_CODE" />
</TD>
<TD> </TD> 
<TD class="reportData" align="left" >
</TD>
</tr>
</table>

<br></br>
<p>Notes</p>

<br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center">Posted By</TD>  
<TD align="center">Category</TD>
<TD align="center">Posted On</TD>
<TD align="center">Notes</TD>
<TD align="center">Assessment</TD>
<TD align="center">Reason</TD>
<TD align="center">Send To</TD>
</tr>
<xsl:for-each select="//IDS_NOTES/IDS_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" />:CN<xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
</table>

<br></br>
<table>
<tr>
<td  align="left" colspan="2"> 
Name of Person Completing Form:
<xsl:choose>
<xsl:when test="CREATEDBY=''">
	<xsl:value-of select="LASTMODIFIEDBY" />
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="CREATEDBY" /> 
</xsl:otherwise>
</xsl:choose>
</td>

</tr>
<tr>
<td align="left" colspan="2"> 
E-SIGN PROVIDED :
<xsl:choose>
<xsl:when test="LASTMODIFIEDBY=''">
	<xsl:value-of select="CREATEDBY" /> 
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="LASTMODIFIEDBY" /> 
</xsl:otherwise>

</xsl:choose>
</td>
</tr>
<tr>
<td align="left" colspan="2"> 
Actual Date of Completion of Form:
<xsl:choose>
<xsl:when test="LAST_MODIFIED_DT=''">
	 <xsl:value-of select="CREATEDON" />
</xsl:when>
<xsl:otherwise>
	 <xsl:value-of select="LAST_MODIFIED_DT" />
</xsl:otherwise>

</xsl:choose>
</td>

</tr>
</table>



<br></br> <br></br>
<b><font size="3">CBU Information </font></b> 

<table>
<tr>
<TD> Cord Blood Bank ID: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="CBB_ID" /> 
</TD>
<TD> CBU Storage Location: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="STORAGE_LOC" />
</TD>
</tr>

<tr>
<TD> CBU Collection Site: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="CBU_COLLECTION_SITE" />
</TD>
<TD></TD> 
<TD class="reportData" align="left" >
</TD>
</tr>

</table>

<br></br>
<p>Notes</p>

<br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center">Posted By</TD>  
<TD align="center">Category</TD>
<TD align="center">Posted On</TD>
<TD align="center">Notes</TD>
<TD align="center">Assessment</TD>
<TD align="center">Reason</TD>
<TD align="center">Send To</TD>
</tr>
<xsl:for-each select="//CBU_INFO_NOTES/CBU_INFO_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" />:CN<xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
</table>
<br></br>

<font size="3">Document</font>

<br></br><br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center">Category</TD>  
<TD align="center">File name</TD>
<TD align="center">Created on</TD>
</tr>
<xsl:for-each select="//CBU_ATTACHMENTS/CBU_ATTACHMENTS_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="CATEGORYVAL" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="FILENAME" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="CREATEDON" /></TD>
</tr>
</xsl:for-each>

</table>

<br></br>
<table>
<tr>
<td  align="left" colspan="2"> 
Name of Person Completing Form:
<xsl:choose>
<xsl:when test="CREATEDBY=''">
	<xsl:value-of select="LASTMODIFIEDBY" />
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="CREATEDBY" /> 
</xsl:otherwise>
</xsl:choose>
</td>

</tr>
<tr>
<td align="left" colspan="2"> 
E-SIGN PROVIDED :
<xsl:choose>
<xsl:when test="LASTMODIFIEDBY=''">
	<xsl:value-of select="CREATEDBY" /> 
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="LASTMODIFIEDBY" /> 
</xsl:otherwise>

</xsl:choose>
</td>
</tr>
<tr>
<td align="left" colspan="2"> 
Actual Date of Completion of Form:
<xsl:choose>
<xsl:when test="LAST_MODIFIED_DT=''">
	 <xsl:value-of select="CREATEDON" />
</xsl:when>
<xsl:otherwise>
	 <xsl:value-of select="LAST_MODIFIED_DT" />
</xsl:otherwise>

</xsl:choose>
</td>

</tr>
</table>

<br></br> <br></br>
<b><font size="3">HLA</font></b> 

<br></br> <br></br>

<table border="1" bordercolor="black">
<tr>
<td align="center"><b>HLA Type</b></td>
<td align="center"><b>HLA-Type-A</b></td>
<td align="center"><b>HLA-Type-B</b></td>
</tr>
<xsl:for-each select="//HLA/HLA_ROW">

<tr>
<td class="reportData" align="left" >
<xsl:value-of select="SOURCEVAL" /> 
</td>
<td class="reportData" align="left" >
<xsl:value-of select="HLA_VALUE_TYPE1" /> 
</td>
<td class="reportData" align="left" >
<xsl:value-of select="HLA_VALUE_TYPE2" /> 
</td>
</tr>

</xsl:for-each>
</table>

<br></br>
<p>Notes</p>

<br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center">Posted By</TD>  
<TD align="center">Category</TD>
<TD align="center">Posted On</TD>
<TD align="center">Notes</TD>
<TD align="center">Assessment</TD>
<TD align="center">Reason</TD>
<TD align="center">Send To</TD>
</tr>
<xsl:for-each select="//HLA_NOTES/HLA_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" />:CN<xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
</table>

<br></br>
<font size="3">Document</font>
<br></br><br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center">Category</TD>  
<TD align="center">File name</TD>
<TD align="center">Created on</TD>
</tr>
<xsl:for-each select="//HLA_ATTACHMENTS/HLA_ATTACHMENTS_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="CATEGORYVAL" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="FILENAME" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="CREATEDON" /></TD>
</tr>
</xsl:for-each>
</table>



<br></br> <br></br>
<b><font size="3">Licensure and Eligibility </font></b>

<br></br><br></br> 


<table>

<tr>
<TD><p>As of <xsl:value-of select="LIC_MODI_DT" />, the licensure status of this HPC(CB) was reported as:</p><xsl:value-of select="LIC_STATUS" /> 
</TD>
</tr>

<xsl:if test="LIC_FLAG='1'">
<tr>
<TD class="reportData"><p>Unlicensed reason(s):</p><xsl:value-of select="UNLICENSEREASON" /> 
</TD>
</tr>
</xsl:if>


<tr>
<TD><p>As of <xsl:value-of select="ELGIBLE_MODI_DT" />, the Eligibility Determination of this HPC(CB) was reported as:</p><xsl:value-of select="ELIGIBLE_STATUS" /> 
</TD>
</tr>

<xsl:if test="ELI_FLAG='1'">
<tr>
<TD class="reportData">
<p>Ineligible Reason(s):</p><xsl:value-of select="INELIGIBLEREASON" /> 
</TD>
</tr>
</xsl:if>
<xsl:if test="ELI_FLAG='2'">
<tr>
<TD class="reportData">
<p>Incomplete Reason(s):</p><xsl:value-of select="INELIGIBLEREASON" /> 
</TD>
</tr>
</xsl:if>

<xsl:if test="ELI_FLAG!='0'">
<tr>
<TD class="reportData"><p>Comments:<xsl:value-of select="ELIGCOMMENTS" /></p>
</TD>
</tr>
</xsl:if>


</table>
<br></br>
<p>Notes</p>

<br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center">Posted By</TD>  
<TD align="center">Category</TD>
<TD align="center">Posted On</TD>
<TD align="center">Notes</TD>
<TD align="center">Assessment</TD>
<TD align="center">Reason</TD>
<TD align="center">Send To</TD>
</tr>
<xsl:for-each select="//ELIGIBILITY_NOTES/ELIGIBILITY_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" />:CN<xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
</table>
<br></br>

<font size="3">Document</font> 
<br></br><br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center">Category</TD> 
<TD align="center">File name</TD>
<TD align="center">Created on</TD> 
</tr>
<xsl:for-each select="//ELIGIBLE_ATTACHMENTS/ELIGIBLE_ATTACHMENTS_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="CATEGORYVAL" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="FILENAME" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="CREATEDON" /></TD>
</tr>
</xsl:for-each>
</table>
<br></br> <br></br>


<br></br>
<table>
<tr>
<td  align="left" colspan="2"> 
Name of Person Completing Form:
<xsl:choose>
<xsl:when test="CREATEDBY=''">
	<xsl:value-of select="LASTMODIFIEDBY" />
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="CREATEDBY" /> 
</xsl:otherwise>
</xsl:choose>
</td>

</tr>
<tr>
<td align="left" colspan="2"> 
E-SIGN PROVIDED :
<xsl:choose>
<xsl:when test="LASTMODIFIEDBY=''">
	<xsl:value-of select="CREATEDBY" /> 
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="LASTMODIFIEDBY" /> 
</xsl:otherwise>

</xsl:choose>
</td>
</tr>
<tr>
<td align="left" colspan="2"> 
Actual Date of Completion of Form:
<xsl:choose>
<xsl:when test="LAST_MODIFIED_DT=''">
	 <xsl:value-of select="CREATEDON" />
</xsl:when>
<xsl:otherwise>
	 <xsl:value-of select="LAST_MODIFIED_DT" />
</xsl:otherwise>

</xsl:choose>
</td>

</tr>
</table>

<br></br> <br></br>
<b><font size="3">Lab Summary </font></b> 

<table>
<tr>
<TD> Processing Start Date: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="PRCSNG_START_DATE" /> 
</TD>
<TD> Bacterial Culture: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="BACT_CULTURE" />
</TD>
<TD class="reportData" align="left">
<xsl:value-of select="BACT_COMMENT" />
</TD>
</tr>

<tr>
<TD> Freeze Date: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="FRZ_DATE" />
</TD>
<TD> Fungal Culture: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="FUNGAL_CULTURE" />
</TD>
<TD class="reportData" align="left">
<xsl:value-of select="FUNG_COMMENT" />
</TD>
</tr>

<tr>
<TD> ABO Blood Type: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="ABO_BLOOD_TYPE" /> 
</TD>
<TD> Hemoglobinopathy Testing: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="HEMOGLOBIN_SCRN" />
</TD>
</tr>

<tr>
<TD> Rh Type: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="RH_TYPE" /> 
</TD>
<TD> Notes: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="LAB_SUM_NOTES" />
</TD>
</tr>
</table>



<br></br> <br></br>


<table>
<tr>
<td><b><font size="3">Test</font></b></td>
<td align ="center"><b><font size="3">Pre-Processing</font></b></td>
<td><b><font size="3">Post Processing/Pre-Cryopreservation </font></b></td>
</tr>
</table>

<br></br> <br></br>

<table>
<xsl:for-each select="//LAB_SUMMARY/LAB_SUMMARY_ROW">


<tr>

<td class="reportData" align="left" >
<xsl:value-of select="LABTEST_NAME" /> 
</td>

<td class="reportData" align="center" >
<xsl:value-of select="PRE_TEST_RESULT" /> 
</td>

<td class="reportData" align="center" >
<xsl:value-of select="POST_TEST_RESULT" /> 
</td>

</tr>
</xsl:for-each>
</table>


 <br></br>
<p>Notes</p>

<br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center">Posted By</TD>  
<TD align="center">Category</TD>
<TD align="center">Posted On</TD>
<TD align="center">Notes</TD>
<TD align="center">Assessment</TD>
<TD align="center">Reason</TD>
<TD align="center">Send To</TD>
</tr>
<xsl:for-each select="//LABSUMMARY_NOTES/LABSUMMARY_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" />:CN<xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
</table>

<br> </br> 
<table>
<tr>
<TD><font size="3"> Document </font></TD> 
<TD></TD> 
<TD></TD> 
</tr>
</table>

<br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center">Category</TD>  
<TD align="center">File name</TD>
<TD align="center">Created on</TD>
</tr>
<xsl:for-each select="../LABSUM_ATTACHMENTS/LABSUM_ATTACHMENTS_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="CATEGORYVAL" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="FILENAME" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="CREATEDON" /></TD>
</tr>
</xsl:for-each>
</table>



<br></br> <br></br>

<b><font size="3">Processing Information </font></b> 

<table>
<tr>
<TD> Procedure Name: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="PROC_NAME" /> 
</TD> <td> </td>
</tr>
<tr>
<TD> Procedure Start Date: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="PROC_START_DATE" /> 
</TD>
<TD> Procedure Termination Date: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="PROC_TERMI_DATE" />
</TD>
</tr>
</table>
<br></br> <br></br>
<b><font size="3">Product Modification </font></b>
<table>
<tr>
<TD> Processing: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="PROCESSING" /> 
</TD>
<TD> Type of automated process used to modify the CBU: </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="AUTOMATED_TYPE" />
</TD>
</tr>

<tr>
<TD> Please Specify: </TD> 
<TD class="reportData" align="left" >
<xsl:value-of select="OTHER_PROCESSING" />
</TD>
<TD> Product Modification:  </TD> 
<TD class="reportData" align="left">
<xsl:value-of select="PRODUCT_MODIFICATION" />
</TD>
</tr>

<tr>
<TD> Please Specify: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="OTHER_PRODUCT_MODI" /> 
</TD>
<TD></TD> <td> </td>

</tr>

</table>

<br></br> <br></br>
<b><font size="3">Storage Conditions </font></b> 
<table>
<tr>
<TD> Storage Method: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="STORAGE_METHOD" /> 
</TD>

<TD> Freezer Manufacturer: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="FREEZER_MANUFACT" /> 
</TD>
</tr>
<tr>
<TD> Please Specify: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="OTHER_FREEZER_MANUFACT" /> 
</TD> <td></td> <td> </td>
</tr>
<tr>
<TD> Frozen in vials, tubes, bags or others: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="FROZEN_IN" /> 
</TD>

<TD> Please Specify: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="OTHER_FROZEN_CONT" /> 
</TD>
</tr>
<tr>
<TD> Number of bags: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_BAGS" /> 
</TD>

<TD> CryoBag Manufacturer: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="CRYOBAG_MANUFAC" /> 
</TD>
</tr>
<tr>
<TD> Bag Type:</TD> <TD class="reportData" align="left" >
<xsl:value-of select="BAGTYPE" /> 
</TD> <td> </td> <td></td>
</tr>
<tr>
<TD> Bag 1 Type: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="BAG1TYPE" /> 
</TD>

<TD> Bag 2 Type: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="BAG2TYPE" /> 
</TD>
</tr>
<tr>
<TD> Please Specify </TD> <TD class="reportData" align="left" >
<xsl:value-of select="OTHER_BAG" /> 
</TD> <td> </td> <td></td>
</tr>
<tr>
<TD> Storage Temperature: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="STORAGE_TEMPERATURE" /> 
</TD>

<TD> Maximum Volume(ml): </TD> <TD class="reportData" align="left" >
<xsl:value-of select="MAX_VOL" /> 
</TD>
</tr>
<tr>
<TD> Controlled Rate Freezing: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="CTRL_RATE_FREEZING" /> 
</TD>

<TD> Number Of Individual Fractions that can be Thawed: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="INDIVIDUAL_FRAC" /> 
</TD>
</tr>
</table>

<br></br> <br></br>

<b><font size="3">CBU Samples </font></b> 
<table>
<tr>
<TD> Number of Filter Paper Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="FILTER_PAPER" /> 
</TD>

<TD> Number of RBC Pellets : </TD> <TD class="reportData" align="left" >
<xsl:value-of select="RPC_PELLETS" /> 
</TD>
</tr>
<tr>
<TD> Number of Extracted DNA Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="EXTR_DNA" /> 
</TD>

<TD> Number of Serum Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="SERUM_ALIQUOTES" /> 
</TD>
</tr>
<tr>
<TD> Number of Plasma Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="PLASMA_ALIQUOTES" /> 
</TD>

<TD> Number of Non-Viable Cell samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NONVIABLE_ALIQUOTES" /> 
</TD>
</tr>
<tr>
<TD> Number of Segments : </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_SEGMENTS" /> 
</TD>
<TD> Number of other representative aliquots
stored under conditions consistent
with those of the final product:</TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_OTH_REP_ALLIQUOTS_F_PROD" /> 
</TD>
</tr>
<tr>
<TD> Number of other representative
aliquots stored under alternate conditions: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_OTH_REP_ALLIQ_ALTER_COND" /> 
</TD>
</tr>

</table>


<br></br> <br></br>
<b><font size="3"> Maternal Samples </font></b> 
<table>
<tr>
<TD> Number of Maternal Serum Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_SERUM_MATER_ALIQUOTS" /> 
</TD>

<TD> Number of Maternal Plasma Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_PLASMA_MATER_ALIQUOTS" /> 
</TD>
</tr>

<tr>
<TD> Number of Maternal Extracted DNA Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_EXTR_DNA_MATER_ALIQUOTS" /> 
</TD>

<TD> Number of Miscellaneous Maternal Samples: </TD> <TD class="reportData" align="left" >
<xsl:value-of select="NO_OF_CELL_MATER_ALIQUOTS" /> 
</TD>
</tr>

<tr>
<TD><p> Documents </p></TD> 
<TD></TD> 
<TD></TD> 
<TD></TD> 
</tr>
</table>

<br></br>
<p>Notes</p>

<br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center">Posted By</TD>  
<TD align="center">Category</TD>
<TD align="center">Posted On</TD>
<TD align="center">Notes</TD>
<TD align="center">Assessment</TD>
<TD align="center">Reason</TD>
<TD align="center">Send To</TD>
</tr>
<xsl:for-each select="//PROCESSINFO_NOTES/PROCESSINFO_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" />:CN<xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
</table>

<br></br>


<font size="3">Document</font>

<br></br><br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center">Category</TD>  
<TD align="center">File name</TD>
<TD align="center">Created on</TD>
</tr>
<xsl:for-each select="//PROINFO_ATTACHMENTS/PROINFO_ATTACHMENTS_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="CATEGORYVAL" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="FILENAME" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="CREATEDON" /></TD>
</tr>
</xsl:for-each>

</table>

<br></br>
<table>
<tr>
<td  align="left" colspan="2"> 
Name of Person Completing Form:

<xsl:choose>
<xsl:when test="SAMPLE_CREATOR!=''">
	<xsl:value-of select="SAMPLE_CREATOR" /> 
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="SAMPLE_LAST_MODBY" /> 
	
</xsl:otherwise>
</xsl:choose>
</td>

</tr>
<tr>
<td align="left" colspan="2"> 
E-SIGN PROVIDED :
<xsl:choose>
<xsl:when test="SAMPLE_LAST_MODBY!=''">
	<xsl:value-of select="SAMPLE_LAST_MODBY" /> 
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="SAMPLE_CREATOR" /> 

</xsl:otherwise>

</xsl:choose>
</td>
</tr>

<tr>
<td align="left" colspan="2"> 
Actual Date of Completion of Form:
<xsl:choose>
<xsl:when test="SAMPLE_LAST_MODDT!=''">
	 <xsl:value-of select="SAMPLE_LAST_MODDT" />
</xsl:when>
<xsl:otherwise>
	  <xsl:value-of select="SAMPLE_CREATED_ON" />
</xsl:otherwise>

</xsl:choose>
</td>

</tr>
</table>

<br></br> <br></br>
<b><font size="3"> IDM </font></b> 


<br></br> <br></br>

<table border="1" bordercolor="black">
<xsl:for-each select="//IDMDETAILS/IDMDETAILS_ROW">
<tr>
<TD align="left" colspan="2">
<xsl:value-of select="TESTDATEVAL" /> 
</TD>

<TD align="left" colspan="4">
<xsl:value-of select="LABTESTNAME" /> 
</TD>


<xsl:if test="position()!=17">
<TD align="left" colspan="2">
<xsl:value-of select="FK_TEST_OUTCOME" /> 
</TD>
</xsl:if>

<xsl:if test="position()=17">
<TD align="left" colspan="2">
<xsl:value-of select="LASTVAL" /> 
</TD>
</xsl:if>


<!--
<TD align="left" >
<xsl:value-of select="ASSESSMENT_REMARKS" /> 
</TD>

<TD align="left" >
<xsl:value-of select="ASSESSMENT_POSTEDON" /> 
</TD>


<TD align="left" >
<xsl:value-of select="ASSESSMENT_CONSULTON" /> 
</TD>

<TD align="left" >
<xsl:value-of select="ASSESSMENT_POSTEDBY" /> 
</TD>
-->
</tr>
</xsl:for-each>
</table>

<br></br>
<p>Notes</p>

<br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center">Posted By</TD>  
<TD align="center">Category</TD>
<TD align="center">Posted On</TD>
<TD align="center">Notes</TD>
<TD align="center">Assessment</TD>
<TD align="center">Reason</TD>
<TD align="center">Send To</TD>
</tr>
<xsl:for-each select="//IDM_NOTES/IDM_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" />:CN<xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
</table>

<br></br>

<font size="3">Document</font> 
<br></br><br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center">Category</TD>
<TD align="center">File name</TD>
<TD align="center">Created on</TD>
</tr>
<xsl:for-each select="//IDM_ATTACHMENTS/IDM_ATTACHMENTS_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="CATEGORYVAL" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="FILENAME" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="CREATEDON" /></TD>
</tr>
</xsl:for-each>
</table>



<br></br>
<br></br> <br></br>
<b><font size="3"> MRQ </font></b> 

<xsl:for-each select="//ROW/GRP/GRP_ROW">
	
	<table border="0" ><tr><td colspan="4"><xsl:value-of select="QUESTION_GRP_NAME" />&#160; </td></tr></table>


		<xsl:variable name="PKGRPVAL" select="PK_QUESTION_GRP" /> 
		<xsl:for-each select="//ROW/MRQ/MRQ_ROW">
			<xsl:if test="$PKGRPVAL=FK_QUES_GRP">

					<xsl:variable name="FKDEPEN_PKVAL" select="DEPENT_QUES_PK" /> 
					<xsl:variable name="VAR_DEPEN_QUES_PK" select="DEPENT_QUES_PK"/> 
					<xsl:variable name="VAR_DEPEN_VALUE" select="DEPENT_QUES_VALUE"/> 
					
						<xsl:variable name="VAR_TEMP_MULTI">
						  <xsl:choose>
							    <xsl:when test="DEPENT_QUES_PK!='0' and contains(DEPENT_QUES_VALUE, ',') and preceding-sibling::*[1]/DEPENT_QUES_PK='0'">
							      <xsl:value-of select="preceding-sibling::*[1]/RESPONSE_CODE" />
							    </xsl:when>
							    <xsl:when test="DEPENT_QUES_PK!='0' and contains(DEPENT_QUES_VALUE, ',') and preceding-sibling::*[2]/DEPENT_QUES_PK='0'">
							      <xsl:value-of select="preceding-sibling::*[2]/RESPONSE_CODE" />
							    </xsl:when>
							     <xsl:when test="DEPENT_QUES_PK!='0' and contains(DEPENT_QUES_VALUE, ',') and preceding-sibling::*[3]/DEPENT_QUES_PK='0'">
							      <xsl:value-of select="preceding-sibling::*[3]/RESPONSE_CODE" />
							    </xsl:when>
						 </xsl:choose>
						</xsl:variable>

						


					<xsl:if test="$FKDEPEN_PKVAL!='0' and contains($VAR_DEPEN_VALUE, ',')">
						 
						<xsl:variable name="VALUE1" select="substring-before($VAR_DEPEN_VALUE,',')"/>
						
						<xsl:if test="$VALUE1=$VAR_TEMP_MULTI">
							<table border="1" bordercolor="black">
							<tr>
							<td><xsl:value-of select="QUES_SEQ" /></td>
							<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
							<td>
							<xsl:if test="RESPONSE_TYPE='dropdown'">
								<xsl:value-of select="DROPDOWN_VALUE" />
							</xsl:if>
							<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
								<xsl:value-of select="RESPONSE_VAL" />
							</xsl:if>
							</td>
							<td><xsl:value-of select="ASSES_RESPONSE" /></td>
							<td><xsl:value-of select="ASSES_NOTES" /></td>
							</tr>
							</table>
						</xsl:if>

						<xsl:variable name="TEMPSTR2" select="substring-after($VAR_DEPEN_VALUE,',')"/>

						<xsl:if test="contains($TEMPSTR2, ',')">
							<xsl:variable name="VALUE2" select="substring-before($TEMPSTR2,',')"/>
							<xsl:if test="$VALUE2=$VAR_TEMP_MULTI">
								<table border="1" bordercolor="black">
								<tr>
								<td><xsl:value-of select="QUES_SEQ" /></td>
								<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
								<td>
								<xsl:if test="RESPONSE_TYPE='dropdown'">
									<xsl:value-of select="DROPDOWN_VALUE" />
								</xsl:if>
								<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
									<xsl:value-of select="RESPONSE_VAL" />
								</xsl:if>
								</td>
								<td><xsl:value-of select="ASSES_RESPONSE" /></td>
								<td><xsl:value-of select="ASSES_NOTES" /></td>
								</tr>
								</table>
							</xsl:if>
						</xsl:if>

						<xsl:variable name="TEMPSTR3" select="substring-after($TEMPSTR2,',')"/>
						
						<xsl:if test="not(contains($TEMPSTR3, ','))">
							<xsl:variable name="VALUE3" select="$TEMPSTR3"/>
							<xsl:if test="$VALUE3=$VAR_TEMP_MULTI">
								<table border="1" bordercolor="black">
								<tr>
								<td><xsl:value-of select="QUES_SEQ" /></td>
								<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
								<td>
								<xsl:if test="RESPONSE_TYPE='dropdown'">
									<xsl:value-of select="DROPDOWN_VALUE" />
								</xsl:if>
								<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
									<xsl:value-of select="RESPONSE_VAL" />
								</xsl:if>
								</td>
								<td><xsl:value-of select="ASSES_RESPONSE" /></td>
								<td><xsl:value-of select="ASSES_NOTES" /></td>
								</tr>
								</table>
							</xsl:if>
						</xsl:if>						
						
					</xsl:if>




					<xsl:variable name="VAR_TEMP">
						  <xsl:choose>
							    <xsl:when test="DEPENT_QUES_PK!='0' and not(contains(DEPENT_QUES_VALUE, ',')) and preceding-sibling::*[1]/DEPENT_QUES_PK='0'">
							      <xsl:value-of select="preceding-sibling::*[1]/RESPONSE_CODE" />
							    </xsl:when>
							    <xsl:when test="DEPENT_QUES_PK!='0' and not(contains(DEPENT_QUES_VALUE, ',')) and preceding-sibling::*[2]/DEPENT_QUES_PK='0'">
							      <xsl:value-of select="preceding-sibling::*[2]/RESPONSE_CODE" />
							    </xsl:when>
							     <xsl:when test="DEPENT_QUES_PK!='0' and not(contains(DEPENT_QUES_VALUE, ',')) and preceding-sibling::*[3]/DEPENT_QUES_PK='0'">
							      <xsl:value-of select="preceding-sibling::*[3]/RESPONSE_CODE" />
							    </xsl:when>
						 </xsl:choose>
					</xsl:variable>




							
					<xsl:if test="$FKDEPEN_PKVAL!='0' and not(contains($VAR_DEPEN_VALUE, ','))">
						
						<xsl:if test="$VAR_DEPEN_VALUE=$VAR_TEMP">
							<table border="1" bordercolor="black">
							<tr>
							<td><xsl:value-of select="QUES_SEQ" /></td>
							<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
							<td>
							<xsl:if test="RESPONSE_TYPE='dropdown'">
								<xsl:value-of select="DROPDOWN_VALUE" />
							</xsl:if>
							<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
								<xsl:value-of select="RESPONSE_VAL" />
							</xsl:if>
							</td>
							<td><xsl:value-of select="ASSES_RESPONSE" /></td>
							<td><xsl:value-of select="ASSES_NOTES" /></td>
							</tr>
							</table>
						</xsl:if>
					</xsl:if>
					
					<xsl:if test="$FKDEPEN_PKVAL='0'">
					<table border="1" bordercolor="black">
					<tr>
					
			
								<td><xsl:value-of select="QUES_SEQ" /></td>
								<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
								<xsl:if test="RESPONSE_TYPE='dropdown'">
									<td><xsl:value-of select="DROPDOWN_VALUE" /></td>
								</xsl:if>
								<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
									<td><xsl:value-of select="RESPONSE_VAL" /></td>
								</xsl:if>
								<td><xsl:value-of select="ASSES_RESPONSE" /></td>
								<td><xsl:value-of select="ASSES_NOTES" /></td>
				
		
					</tr>
					</table>
					</xsl:if>

					
					
					
					
					
					
				
			</xsl:if>
		</xsl:for-each>
		

	<br></br>
	<br></br>	
		
		
	

	
</xsl:for-each>

<br></br>

<xsl:for-each select="//ROW/HISTORY/HISTORY_ROW">
<table>
<tr>
<td  align="left" colspan="2"> 
Name of Person Completing Form:

<xsl:choose>
<xsl:when test="CREATOR!=''">
	<xsl:value-of select="CREATOR" /> 
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="MODIFIED_BY" /> 
	
</xsl:otherwise>
</xsl:choose>
</td>

</tr>
<tr>
<td align="left" colspan="2"> 
E-SIGN PROVIDED :
<xsl:choose>
<xsl:when test="MODIFIED_BY!=''">
	<xsl:value-of select="MODIFIED_BY" /> 
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="CREATOR" /> 

</xsl:otherwise>

</xsl:choose>
</td>
</tr>

<tr>
<td align="left" colspan="2"> 
Actual Date of Completion of Form:
<xsl:choose>
<xsl:when test="MODIFIED_ON!=''">
	 <xsl:value-of select="MODIFIED_ON" />
</xsl:when>
<xsl:otherwise>
	  <xsl:value-of select="CREATED_ON" />
</xsl:otherwise>

</xsl:choose>
</td>

</tr>
</table>
</xsl:for-each>



<br></br>
<br></br> <br></br>
<b><font size="3"> FMHQ </font></b> 

<xsl:for-each select="//ROW/FMHQ_GRP/FMHQ_GRP_ROW">
	
	<table border="0" ><tr><td colspan="4"><xsl:value-of select="QUESTION_GRP_NAME" />&#160; </td></tr></table>


		<xsl:variable name="PKGRPVAL" select="PK_QUESTION_GRP" /> 
		<xsl:for-each select="//ROW/FMHQ/FMHQ_ROW">
			<xsl:if test="$PKGRPVAL=FK_QUES_GRP">

					<xsl:variable name="FKDEPEN_PKVAL" select="DEPENT_QUES_PK" /> 
					<xsl:variable name="VAR_DEPEN_QUES_PK" select="DEPENT_QUES_PK"/> 
					<xsl:variable name="VAR_DEPEN_VALUE" select="DEPENT_QUES_VALUE"/> 
					<xsl:variable name="VAR_RESPONSEVAL" select="RESPONSE_VAL"/>

										
					<xsl:variable name="VAR_PRE_RESTYPE" select="preceding-sibling::*[1]/RESPONSE_TYPE"/> 
					<xsl:variable name="VAR_PRE_RESTYPE1" select="preceding-sibling::*[2]/RESPONSE_TYPE"/> 
					<xsl:variable name="VAR_PRE_RESVAL" select="preceding-sibling::*[1]/RESPONSE_VAL12"/> 

					



					<xsl:if test="$FKDEPEN_PKVAL!='0' and $VAR_PRE_RESTYPE='multiselect' and $VAR_PRE_RESTYPE1='dropdown' and RESPONSE_TYPE='multiselect'">
					
							<xsl:if test="$VAR_PRE_RESVAL!='0' and contains($VAR_PRE_RESVAL, ',')">
								
								<xsl:variable name="VAR1"/>
								<xsl:variable name="VAR1" select="substring-before($VAR_PRE_RESVAL,',')"/>
								<xsl:variable name="SPLTD_STR1" select="substring-after($VAR_PRE_RESVAL,', ')"/>
								

								<xsl:variable name="VAR2">
								    <xsl:choose>
									 <xsl:when test="contains($SPLTD_STR1,',')">
									    	<xsl:value-of select="substring-before($SPLTD_STR1,',')" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="substring-after($VAR_PRE_RESVAL,', ')"/>
									</xsl:otherwise>
								    </xsl:choose>
								</xsl:variable>

								
								<xsl:variable name="SPLTD_STR2" select="substring-after($SPLTD_STR1,', ')"/>

								<xsl:variable name="VAR3">
								    <xsl:choose>
									 <xsl:when test="contains($SPLTD_STR2,',')">
									    	<xsl:value-of select="substring-before($SPLTD_STR2,',')" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$SPLTD_STR2"/>
									</xsl:otherwise>
								    </xsl:choose>
								</xsl:variable>
							
							
								<xsl:variable name="SPLTD_STR3" select="substring-after($SPLTD_STR2,', ')"/>

								<xsl:variable name="VAR4">
								    <xsl:choose>
									 <xsl:when test="contains($SPLTD_STR3,',')">
									    	<xsl:value-of select="substring-before($SPLTD_STR3,',')" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$SPLTD_STR3"/>
									</xsl:otherwise>
								    </xsl:choose>
								</xsl:variable>
						
								<xsl:variable name="SPLTD_STR4" select="substring-after($SPLTD_STR3,', ')"/>

								<xsl:variable name="VAR5">
								    <xsl:choose>
									 <xsl:when test="contains($SPLTD_STR4,',')">
									    	<xsl:value-of select="substring-before($SPLTD_STR4,',')" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$SPLTD_STR4"/>
									</xsl:otherwise>
								    </xsl:choose>
								</xsl:variable>

								
								<xsl:variable name="SPLTD_STR5" select="substring-after($SPLTD_STR4,', ')"/>
								
								<xsl:variable name="VAR6">
								    <xsl:choose>
									 <xsl:when test="contains($SPLTD_STR5,',')">
									    	<xsl:value-of select="substring-before($SPLTD_STR5,',')" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$SPLTD_STR5"/>
									</xsl:otherwise>
								    </xsl:choose>
								</xsl:variable>
				
								
								<xsl:variable name="FINDMATCH">
								    <xsl:choose>
									 <xsl:when test="$VAR1=$VAR_DEPEN_VALUE">
									    <xsl:value-of select="'matched'" />
									</xsl:when>
									 <xsl:when test="$VAR2=$VAR_DEPEN_VALUE">
									     <xsl:value-of select="'matched'" />
									</xsl:when>
									<xsl:when test="$VAR3=$VAR_DEPEN_VALUE">
									     <xsl:value-of select="'matched'" />
									</xsl:when>
									<xsl:when test="$VAR4=$VAR_DEPEN_VALUE">
									     <xsl:value-of select="'matched'" />
									</xsl:when>
									<xsl:when test="$VAR4=$VAR_DEPEN_VALUE">
									     <xsl:value-of select="'matched'" />
									</xsl:when>
									<xsl:when test="$VAR6=$VAR_DEPEN_VALUE">
									     <xsl:value-of select="'matched'" />
									</xsl:when>
								    </xsl:choose>
							      </xsl:variable>

							     <xsl:if test="$FINDMATCH='matched'">
									<table border="1" bordercolor="black">
									<tr>
									<td><xsl:value-of select="QUES_SEQ" /></td>
									<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
									<td>
									<xsl:if test="RESPONSE_TYPE='dropdown'">
										<xsl:value-of select="DROPDOWN_VALUE" />
									</xsl:if>
									<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
										<xsl:value-of select="RESPONSE_VAL" />
									</xsl:if>
									<xsl:if test="RESPONSE_TYPE='multiselect' and RESPONSE_VAL!='0'">
										<xsl:value-of select="RESPONSE_VAL" />
									</xsl:if>
									</td>
									<td><xsl:value-of select="ASSES_RESPONSE" /></td>
									<td><xsl:value-of select="ASSES_NOTES" /></td>
									</tr>
									</table>
							    </xsl:if>

								
						</xsl:if>
						<xsl:if test="$VAR_PRE_RESVAL!='0' and not(contains($VAR_PRE_RESVAL, ','))">
								<xsl:variable name="FINDMATCH">
								    <xsl:choose>
									 <xsl:when test="$VAR_PRE_RESVAL=$VAR_DEPEN_VALUE">
									    <xsl:value-of select="'matched'" />
									</xsl:when>
								    </xsl:choose>
								</xsl:variable>
								<xsl:if test="$FINDMATCH='matched'">
									<table border="1" bordercolor="black">
									<tr>
									<td><xsl:value-of select="QUES_SEQ" /></td>
									<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
									<td>
									<xsl:if test="RESPONSE_TYPE='dropdown'">
										<xsl:value-of select="DROPDOWN_VALUE" />
									</xsl:if>
									<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
										<xsl:value-of select="RESPONSE_VAL" />
									</xsl:if>
									<xsl:if test="RESPONSE_TYPE='multiselect' and RESPONSE_VAL!='0'">
										<xsl:value-of select="RESPONSE_VAL" />
									</xsl:if>
									</td>
									<td><xsl:value-of select="ASSES_RESPONSE" /></td>
									<td><xsl:value-of select="ASSES_NOTES" /></td>
									</tr>
									</table>
							    </xsl:if>
						</xsl:if>
			</xsl:if>
				

							<xsl:variable name="VAR_TEMP">
								  <xsl:choose>
									    <xsl:when test="DEPENT_QUES_PK!='0' and not(contains(DEPENT_QUES_VALUE, ',')) and preceding-sibling::*[1]/DEPENT_QUES_PK='0' and preceding-sibling::*[1]/RESPONSE_TYPE='dropdown'">
									      <xsl:value-of select="preceding-sibling::*[1]/RESPONSE_CODE" />
									    </xsl:when>
									    <xsl:when test="DEPENT_QUES_PK!='0' and not(contains(DEPENT_QUES_VALUE, ',')) and preceding-sibling::*[2]/DEPENT_QUES_PK='0' and preceding-sibling::*[2]/RESPONSE_TYPE='dropdown'">
									      <xsl:value-of select="preceding-sibling::*[2]/RESPONSE_CODE" />
									    </xsl:when>
									     <xsl:when test="DEPENT_QUES_PK!='0' and not(contains(DEPENT_QUES_VALUE, ',')) and preceding-sibling::*[3]/DEPENT_QUES_PK='0' and preceding-sibling::*[3]/RESPONSE_TYPE='dropdown'">
									      <xsl:value-of select="preceding-sibling::*[3]/RESPONSE_CODE" />
									    </xsl:when>
									    <xsl:when test="DEPENT_QUES_PK!='0' and not(contains(DEPENT_QUES_VALUE, ',')) and preceding-sibling::*[4]/DEPENT_QUES_PK='0' and preceding-sibling::*[4]/RESPONSE_TYPE='dropdown'">
									      <xsl:value-of select="preceding-sibling::*[4]/RESPONSE_CODE" />
									    </xsl:when>
								 </xsl:choose>
							</xsl:variable>




							
							<xsl:if test="$FKDEPEN_PKVAL!='0' and not(contains($VAR_DEPEN_VALUE, ','))">
						
								<xsl:if test="$VAR_DEPEN_VALUE=$VAR_TEMP">
									<table border="1" bordercolor="black">
									<tr>
									<td><xsl:value-of select="QUES_SEQ" /></td>
									<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
									<td>
									<xsl:if test="RESPONSE_TYPE='dropdown'">
										<xsl:value-of select="DROPDOWN_VALUE" />
									</xsl:if>
									<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
										<xsl:value-of select="RESPONSE_VAL" />
									</xsl:if>
									<xsl:if test="RESPONSE_TYPE='multiselect' and RESPONSE_VAL!='0'">
										<xsl:value-of select="RESPONSE_VAL" />
									</xsl:if>
									</td>
									<td><xsl:value-of select="ASSES_RESPONSE" /></td>
									<td><xsl:value-of select="ASSES_NOTES" /></td>
									</tr>
									</table>
								</xsl:if>
							</xsl:if>

					
					<xsl:if test="$FKDEPEN_PKVAL='0' or (DEPENT_QUES_PK!='0' and preceding-sibling::*[1]/DEPENT_QUES_PK=DEPENT_QUES_PK and RESPONSE_VAL12!='0')">
					<table border="1" bordercolor="black">
					<tr>
					
			
								<td><xsl:value-of select="QUES_SEQ" /></td>
								<td colspan="3"><xsl:value-of select="QUES_DESC" /></td>
								<xsl:if test="RESPONSE_TYPE='dropdown'">
									<td><xsl:value-of select="DROPDOWN_VALUE" /></td>
								</xsl:if>
								<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
									<td><xsl:value-of select="RESPONSE_VAL" /></td>
								</xsl:if>
								<xsl:if test="RESPONSE_TYPE='multiselect' and RESPONSE_VAL!='0'">
									<td><xsl:value-of select="RESPONSE_VAL" /></td>
								</xsl:if>
								<td><xsl:value-of select="ASSES_RESPONSE" /></td>
								<td><xsl:value-of select="ASSES_NOTES" /></td>
				
		
					</tr>
					</table>
					</xsl:if>

					
					
					
					
					
					
				
			</xsl:if>
		</xsl:for-each>
		

	<br></br>
	<br></br>	
		
		
	

	
</xsl:for-each>

<br></br>

<xsl:for-each select="//ROW/FMHQ_HISTORY/FMHQ_HISTORY_ROW">
<table>
<tr>
<td  align="left" colspan="2"> 
Name of Person Completing Form:

<xsl:choose>
<xsl:when test="CREATOR!=''">
	<xsl:value-of select="CREATOR" /> 
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="MODIFIED_BY" /> 
</xsl:otherwise>
</xsl:choose>
</td>

</tr>
<tr>
<td align="left" colspan="2"> 
E-SIGN PROVIDED :
<xsl:choose>
<xsl:when test="MODIFIED_BY!=''">
	<xsl:value-of select="MODIFIED_BY" /> 
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="CREATOR" /> 

</xsl:otherwise>

</xsl:choose>
</td>
</tr>

<tr>
<td align="left" colspan="2"> 
Actual Date of Completion of Form:
<xsl:choose>
<xsl:when test="MODIFIED_ON!=''">
	 <xsl:value-of select="MODIFIED_ON" />
</xsl:when>
<xsl:otherwise>
	  <xsl:value-of select="CREATED_ON" />
</xsl:otherwise>

</xsl:choose>
</td>

</tr>
</table>
</xsl:for-each>



<xsl:if test="ORDERTYPEDATA=1">
<br></br> <br></br>
<b><font size="3"> CBU Request History</font></b> 


<br></br> <br></br>

<table border="1" bordercolor="black">
<tr>
<TD align="center"><b>Date</b></TD>
<TD align="center"><b>Cord Event</b></TD>
<TD align="center"><b>User</b></TD>
<TD align="center"><b>Request Type</b></TD>
</tr>
<xsl:for-each select="//CBUREQHISTORY/CBUREQHISTORY_ROW">
<tr>
<TD align="left">
<xsl:value-of select="DATEVAL" /> 
</TD>

<TD align="left">


<xsl:if test="TYPECODE='cord_status'">
Local Cord Status is <xsl:value-of select="DESCRIPTION1" /> 
</xsl:if>

<xsl:if test="TYPECODE='Cord_Nmdp_Status'">
NMDP Registry Status is <xsl:value-of select="DESCRIPTION1" /> 
</xsl:if>


<xsl:if test="TYPECODE='order_status'">
Order Status is <xsl:value-of select="DESCRIPTION2" /> 
</xsl:if>


</TD>


<TD align="left">
<xsl:value-of select="CREATOR" /> 
</TD>

<TD align="left" >
<xsl:value-of select="ORDERTYPE" /> 
</TD>


</tr>
</xsl:for-each>
</table>
</xsl:if>


<xsl:if test="ORDERTYPEDATA=0">
<br></br> <br></br>
<b><font size="3"> CBU History </font></b> 


<br></br> <br></br>

<table border="1" bordercolor="black">
<tr>
<TD align="center"><b>Date</b></TD>
<TD align="center"><b>Cord Event</b></TD>
<TD align="center"><b>User</b></TD>
<TD align="center"><b>Request Type</b></TD>
</tr>
<xsl:for-each select="//CBUHISTORY/CBUHISTORY_ROW">
<tr>
<TD align="left">
<xsl:value-of select="DATEVAL" /> 
</TD>

<TD align="left">


<xsl:if test="TYPECODE='cord_status'">
Local Cord Status is <xsl:value-of select="DESCRIPTION1" /> 
</xsl:if>

<xsl:if test="TYPECODE='Cord_Nmdp_Status'">
NMDP Registry Status is <xsl:value-of select="DESCRIPTION1" /> 
</xsl:if>


<xsl:if test="TYPECODE='order_status'">
Order Status is <xsl:value-of select="DESCRIPTION2" /> 
</xsl:if>


</TD>


<TD align="left">
<xsl:value-of select="CREATOR" /> 
</TD>

<TD align="left" >
<xsl:value-of select="ORDERTYPE" /> 
</TD>


</tr>
</xsl:for-each>
</table>
</xsl:if>



<br></br> <br></br>
<b><font size="3"> Notes </font></b> 
<br></br><br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center">Posted By</TD>  
<TD align="center">Category</TD>
<TD align="center">Posted On</TD>
<TD align="center">Notes</TD>
<TD align="center">Assessment</TD>
<TD align="center">Reason</TD>
<TD align="center">Send To</TD>
</tr>

<xsl:for-each select="//IDS_NOTES/IDS_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" />:CN<xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
<xsl:for-each select="//CBU_INFO_NOTES/CBU_INFO_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" />:CN<xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
<xsl:for-each select="//HLA_NOTES/HLA_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" />:CN<xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
<xsl:for-each select="//ELIGIBILITY_NOTES/ELIGIBILITY_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" />:CN<xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
<xsl:for-each select="//LABSUMMARY_NOTES/LABSUMMARY_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" />:CN<xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>

<xsl:for-each select="//PROCESSINFO_NOTES/PROCESSINFO_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" />:CN<xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
<xsl:for-each select="//IDM_NOTES/IDM_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" />:CN<xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
</table>





</xsl:for-each>
</xsl:template> 

</xsl:stylesheet>