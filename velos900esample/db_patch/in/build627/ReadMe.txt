/* This readMe is specific to Velos eResearch version 9.0 build #627 */

=====================================================================================================================================
Garuda :
DB Patch :- 
The following DB patches are specific to Garuda.
	1.DL14_ER_CODELST_UPDATE.sql
	2.DL15_ER_CODELST_INSERT.sql
	   
=====================================================================================================================================
eResearch:



=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1. labelBundle.properties
2. LC.java
3. MC.java
4. messageBundle.properties
5. patientschedule.jsp
6. reportsinstudy.jsp
=====================================================================================================================================
