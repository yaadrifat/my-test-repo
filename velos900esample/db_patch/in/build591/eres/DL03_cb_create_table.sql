 -------------------------cb_upload_info Table-------------------------------------------------

CREATE TABLE "CB_UPLOAD_INFO"
  (
    "PK_UPLOAD_INFO" NUMBER(10,0),
    "DESCRIPTION"    VARCHAR2(1000 BYTE),
    "COMPLETION_DATE" DATE,
    "TEST_DATE" DATE,
    "PROCESS_DATE" DATE,
    "VERIFICATION_TYPING" VARCHAR2(1 BYTE),
    "RECEIVED_DATE" DATE,
    "FK_CATEGORY"     NUMBER(10,0),
    "FK_SUBCATEGORY"  NUMBER(10,0),
    "FK_ATTACHMENTID" NUMBER(10,0),
    "CREATOR"         NUMBER(10,0),
    "CREATED_ON" DATE,
    "LAST_MODIFIED_BY" NUMBER(10,0),
    "LAST_MODIFIED_DATE" DATE,
    "IP_ADD"      VARCHAR2(15 BYTE),
    "RID"         NUMBER(10,0),
    "DELETEDFLAG" VARCHAR2(1 BYTE),
    PRIMARY KEY ("PK_UPLOAD_INFO") USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT) TABLESPACE "ERES_USER" ENABLE
  )
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (
    INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )
  TABLESPACE "ERES_USER" ;
COMMENT ON COLUMN "CB_UPLOAD_INFO"."PK_UPLOAD_INFO"
IS
  'Primary Key of the table. ';
  COMMENT ON COLUMN "CB_UPLOAD_INFO"."DESCRIPTION"
IS
  'Remarks/Comments given for each upload.';
  COMMENT ON COLUMN "CB_UPLOAD_INFO"."COMPLETION_DATE"
IS
  'Completion date for lab test';
  COMMENT ON COLUMN "CB_UPLOAD_INFO"."TEST_DATE"
IS
  'Test date for lab test if the upload is done for a lab test.';
  COMMENT ON COLUMN "CB_UPLOAD_INFO"."PROCESS_DATE"
IS
  'Test processing date';
  COMMENT ON COLUMN "CB_UPLOAD_INFO"."VERIFICATION_TYPING"
IS
  'Verification type of upload made for any category';
  COMMENT ON COLUMN "CB_UPLOAD_INFO"."RECEIVED_DATE"
IS
  'Upload received date';
  COMMENT ON COLUMN "CB_UPLOAD_INFO"."FK_CATEGORY"
IS
  'Upload category src - ER_CODELST';
  COMMENT ON COLUMN "CB_UPLOAD_INFO"."FK_SUBCATEGORY"
IS
  'Sub category of upload src - ER_CODELST';
  COMMENT ON COLUMN "CB_UPLOAD_INFO"."FK_ATTACHMENTID"
IS
  'Reference to Attachment';
  COMMENT ON COLUMN "CB_UPLOAD_INFO"."CREATOR"
IS
  'This column is used for Audit Trail. The Creator identifies the user who created this row.';
  COMMENT ON COLUMN "CB_UPLOAD_INFO"."CREATED_ON"
IS
  'This column is used for Audit Trail. Stores the date on which this row was created.';
  COMMENT ON COLUMN "CB_UPLOAD_INFO"."LAST_MODIFIED_BY"
IS
  'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';
  COMMENT ON COLUMN "CB_UPLOAD_INFO"."LAST_MODIFIED_DATE"
IS
  'This column is used for Audit Trail. Stores the date on which this row was last modified.';
  COMMENT ON COLUMN "CB_UPLOAD_INFO"."IP_ADD"
IS
  'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
  COMMENT ON COLUMN "CB_UPLOAD_INFO"."RID"
IS
  'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
  COMMENT ON COLUMN "CB_UPLOAD_INFO"."DELETEDFLAG"
IS
  'This column denotes whether record is deleted. ';
  COMMENT ON TABLE "CB_UPLOAD_INFO"
IS
  'This table is being used to store the additional details for uploads. These uploads are stored in ER_ATTACHMENT table and subsequent information is stored in this table based on Category/Subcategory.';

--------------------------------cb_upload_info Table Ends Here-----------------------------

--------------------------------cb_upload_info Sequence----------------------------------------

CREATE SEQUENCE "ERES"."SEQ_CB_UPLOAD_INFO" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE ;

--------------------------------cb_upload_info Sequence Ends Here----------------------------------------


--------------------------------CB_CORD_CT_STATUS Table-------------------------------------


CREATE TABLE "CB_CORD_CT_STATUS"
  (
    "PK_CORD_CT_STATUS" NUMBER(10,0),
    "ENTITY_ID"         NUMBER(10,0),
    "ENTITY_TYPE"       NUMBER(10,0),
    "HLA_TYPING_STATUS" VARCHAR2(1 BYTE),
    "HLA_TYPING_COMPLETION_DATE" DATE,
    "COMPLETED_BY"     VARCHAR2(100 BYTE),
    "IP_ADD"           VARCHAR2(15 BYTE),
    "RID"              NUMBER(10,0),
    "LAST_MODIFIED_BY" NUMBER(10,0),
    "LAST_MODIFIED_DATE" DATE,
    "DELETEDFLAG" VARCHAR2(1 BYTE),
    "CREATOR"     NUMBER(10,0),
    "CREATED_ON" DATE,
    PRIMARY KEY ("PK_CORD_CT_STATUS") USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT) TABLESPACE "ERES_USER" ENABLE
  )
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (
    INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )
  TABLESPACE "ERES_USER" ;
COMMENT ON COLUMN "CB_CORD_CT_STATUS"."PK_CORD_CT_STATUS"
IS
  'Primary Key';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."ENTITY_ID"
IS
  'Id of the entity like ERU, Doner etc';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."ENTITY_TYPE"
IS
  'Type of entity';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."IP_ADD"
IS
  'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."RID"
IS
  'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."LAST_MODIFIED_BY"
IS
  'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."LAST_MODIFIED_DATE"
IS
  'This column is used for Audit Trail. Stores the date on which this row was last modified.';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."DELETEDFLAG"
IS
  'This column denotes whether record is deleted. ';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."CREATOR"
IS
  'This column is used for Audit Trail. The Creator identifies the user who created this row.';
  COMMENT ON COLUMN "CB_CORD_CT_STATUS"."CREATED_ON"
IS
  'This column is used for Audit Trail. Stores the date on which this row was created.';


--------------------------------CB_CORD_CT_STATUS Table Ends Here-------------------------------------

--------------------------------CB_CORD_CT_STATUS Sequence----------------------------------------

CREATE SEQUENCE "SEQ_CB_CORD_CT_STATUS" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE ;

--------------------------------CB_CORD_CT_STATUS Sequence Ends Here----------------------------------------

------------------------------------CB_DCMS_LOG Table --------------------------------------

CREATE TABLE "CB_DCMS_LOG"
  (
    "PK_DCMS_LOG" NUMBER(19,0) NOT NULL ENABLE,
    "CREATOR"     NUMBER(19,0),
    "CREATED_ON" TIMESTAMP (6),
    "IP_ADD"           VARCHAR2(255 CHAR),
    "LAST_MODIFIED_BY" NUMBER(19,0),
    "LAST_MODIFIED_DATE" TIMESTAMP (6),
    "ATTACHMENT_ID" NUMBER(19,0),
    "ACTION"        VARCHAR2(255 CHAR),
    "ACTION_DATE" TIMESTAMP (6),
    "USER_ID" NUMBER(19,0),
    PRIMARY KEY ("PK_DCMS_LOG") USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT) TABLESPACE "ERES_USER" ENABLE
  )
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (
    INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )
  TABLESPACE "ERES_USER" ;
COMMENT ON COLUMN "CB_DCMS_LOG"."PK_DCMS_LOG"
IS
  'Primary Key';
  COMMENT ON COLUMN "CB_DCMS_LOG"."CREATOR"
IS
  'This column is used for Audit Trail. The Creator identifies the user who created this row.';
  COMMENT ON COLUMN "CB_DCMS_LOG"."CREATED_ON"
IS
  'This column is used for Audit Trail. Stores the date on which this row was created.';
  COMMENT ON COLUMN "CB_DCMS_LOG"."IP_ADD"
IS
  'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
  COMMENT ON COLUMN "CB_DCMS_LOG"."LAST_MODIFIED_BY"
IS
  'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';
  COMMENT ON COLUMN "CB_DCMS_LOG"."LAST_MODIFIED_DATE"
IS
  'This column is used for Audit Trail. Stores the date on which this row was last modified.';
  COMMENT ON COLUMN "CB_DCMS_LOG"."ATTACHMENT_ID"
IS
  'Attachment ID which is stored in DCMS';
  COMMENT ON COLUMN "CB_DCMS_LOG"."ACTION"
IS
  'INSERT/FETCH';
  COMMENT ON COLUMN "CB_DCMS_LOG"."ACTION_DATE"
IS
  'Action Date';
  COMMENT ON COLUMN "CB_DCMS_LOG"."USER_ID"
IS
  'User id';

-------------------------------CB_DCMS_LOG Table Ends Here----------------------------------

-------------------------------SEQ_DCMS_LOG Sequence---------------------------------------

CREATE SEQUENCE "SEQ_DCMS_LOG" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE ;

-------------------------------SEQ_DCMS_LOG Sequence Ends Here ---------------------------------------

