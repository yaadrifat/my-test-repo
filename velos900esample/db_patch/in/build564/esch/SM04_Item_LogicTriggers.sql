set define off;

create or replace TRIGGER ESCH."SCH_SUBCOST_ITEM_AU1"
AFTER UPDATE
ON ESCH.SCH_SUBCOST_ITEM REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
   raid number(10);
--Created by Sampada
BEGIN
  if nvl(:old.SUBCOST_ITEM_DELFLAG,'N') = 'N' AND nvl(:new.SUBCOST_ITEM_DELFLAG,'N') = 'Y' then
     DELETE FROM SCH_SUBCOST_ITEM_VISIT WHERE FK_SUBCOST_ITEM = :NEW.PK_SUBCOST_ITEM;
  end if;  
END;
/ 


create or replace TRIGGER ESCH."SCH_SUBCOST_ITEM_AD1"
AFTER UPDATE
ON ESCH.SCH_SUBCOST_ITEM REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
   raid number(10);
--Created by Sampada
BEGIN
     DELETE FROM SCH_SUBCOST_ITEM_VISIT WHERE FK_SUBCOST_ITEM = :old.PK_SUBCOST_ITEM;
END;
/ 

CREATE OR REPLACE TRIGGER esch."EVENT_ASSOC_AD2" AFTER DELETE ON EVENT_ASSOC
REFERENCING OLD AS OLD NEW AS a
FOR EACH ROW
WHEN ( old.event_type='P')
declare
	pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'EVENT_ASSOC_AD2', pLEVEL  => Plog.LFATAL);
begin
	update SCH_SUBCOST_ITEM SET SUBCOST_ITEM_DELFLAG ='Y' where FK_CALENDAR = :old.event_id;
	--DELETE FROM SCH_SUBCOST_ITEM where FK_CALENDAR = :old.event_id;
	exception when others then
		plog.fatal(pctx,'Exception:'||sqlerrm);
end;
/


CREATE OR REPLACE TRIGGER esch."EVENT_DEF_AD2" AFTER DELETE ON EVENT_DEF
REFERENCING OLD AS OLD NEW AS a
FOR EACH ROW
WHEN ( old.event_type='P')
declare
	pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'EVENT_DEF_AD2', pLEVEL  => Plog.LFATAL);
begin
	update SCH_SUBCOST_ITEM SET SUBCOST_ITEM_DELFLAG ='Y' where FK_CALENDAR = :old.event_id;
	--DELETE FROM SCH_SUBCOST_ITEM where FK_CALENDAR = :old.event_id;
	exception when others then
		plog.fatal(pctx,'Exception:'||sqlerrm);
end;
/


CREATE OR REPLACE TRIGGER esch."SCH_PROTOCOL_VISIT_AD1" AFTER DELETE ON SCH_PROTOCOL_VISIT REFERENCING OLD AS OLD NEW AS a
FOR EACH ROW
declare
 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SCH_PROTOCOL_VISIT_AD1', pLEVEL  => Plog.LFATAL);
 begin

 --Delete the event from calendar's default budget
 begin
 	 DELETE FROM SCH_SUBCOST_ITEM_VISIT WHERE FK_PROTOCOL_VISIT = :old.pk_protocol_visit;

     delete from sch_lineitem l
     where l.fk_bgtsection in ( select pk_budgetsec from sch_bgtsection where fk_visit = :old.pk_protocol_visit and fk_bgtcal =
                 (select pk_bgtcal from sch_bgtcal where bgtcal_protid = :old.fk_protocol and fk_budget = (
                 select  pk_budget from sch_budget where budget_calendar = :old.fk_protocol)
                 )
             );

     delete from sch_bgtsection where fk_visit = :old.pk_protocol_visit and fk_bgtcal =
                 (select pk_bgtcal from sch_bgtcal where bgtcal_protid = :old.fk_protocol and fk_budget = (
                 select  pk_budget from sch_budget where budget_calendar = :old.fk_protocol)
                 );

 exception when others then
    plog.fatal(pctx,'Exception:'||sqlerrm);
 end;
end;
/


