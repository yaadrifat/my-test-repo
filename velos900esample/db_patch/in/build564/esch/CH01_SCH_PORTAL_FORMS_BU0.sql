drop trigger "ESCH"."SCH_PORTAL_FORMS_B0";

create or replace TRIGGER "ESCH"."SCH_PORTAL_FORMS_BU0"
BEFORE UPDATE
ON SCH_PORTAL_FORMS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
when (NEW.last_modified_by IS NOT NULL)
begin
	:new.last_modified_date := sysdate;
end;
/