set define off;

--STARTS INSERTING RECORD INTO ER_MAILCONFIG TABLE--

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_MAILCONFIG
    where MAILCONFIG_MAILCODE = 'Assessment';

  if (v_record_exists = 0) then
     INSERT INTO 
ER_MAILCONFIG(PK_MAILCONFIG,MAILCONFIG_MAILCODE,MAILCONFIG_SUBJECT,MAILCONFIG_CONTENT,MAILCONFIG_CONTENTTYPE,MAILCONFIG_FROM,RID,CREATED_ON,CREATOR,LAST_MODIFIED_DATE,LAST_MODIFIED_BY) 
  VALUES(SEQ_ER_MAILCONFIG.nextval,'Assessment','IDM Assessment Notification','Please Review the following assessment in the [IDM] form for CBU ID # [CBUID]<br/><a href="[URL]">[URL]</a>','text/html','admin@aithent.com',1,SYSDATE,NULL,SYSDATE,NULL);
	commit;
  end if;
end;
/

--END--
