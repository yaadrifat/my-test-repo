CREATE OR REPLACE FORCE VIEW REP_ADV_MORE_DETAILS
(
   PK_MOREDETAILS,
   FIELD_NAME,
   FIELD_VALUE,
   FK_PER,
   CREATED_ON,
   FK_ACCOUNT,
   PAT_ID,
   PAT_NAME,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   RESPONSE_ID,
   ADVE_TYPE,
   ADVE_NAME,
   AE_STDATE,
   STUDY_NUMBER,
   FK_STUDY
)
AS
   SELECT   PK_MOREDETAILS,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = MD_MODELEMENTPK)
               field_name,
            MD_MODELEMENTDATA AS field_value,
            pk_person,
            to_date(pkg_util.date_to_char_with_time (a.CREATED_ON) , PKG_DATEUTIL.f_get_datetimeformat) CREATED_ON,
            fk_account,
            PERson_CODE PAT_ID,
            (PERSON_FNAME || ' ' || PERSON_LNAME) PAT_NAME,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            to_date(pkg_util.date_to_char_with_time (a.LAST_MODIFIED_DATE) , PKG_DATEUTIL.f_get_datetimeformat) LAST_MODIFIED_DATE,
            A.RID,
            PK_MOREDETAILS RESPONSE_ID,
            (SELECT   codelst_desc
               FROM   SCH_CODELST
              WHERE   pk_codelst = fk_codlst_aetype)
               adve_type,
            ae_name,
            ae_stdate,
            (SELECT   study_number
               FROM   er_study
              WHERE   pk_study = fk_study)
               study_number,
            fk_study
     FROM   er_moredetails a, person, sch_adverseve
    WHERE       a.MD_MODNAME = 'advtype'
            AND FK_MODPK = pk_adveve
            AND pk_person = fk_per;