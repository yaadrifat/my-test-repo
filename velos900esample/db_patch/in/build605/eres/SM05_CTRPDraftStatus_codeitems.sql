set define off;

DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpDraftStatus' and codelst_subtyp = 'wip';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL) 
		values (SEQ_ER_CODELST.nextval,null,'ctrpDraftStatus','wip','Work In Progress','N',1, null);
		dbms_output.put_line('Code-list item Type:ctrpDraftStatus Subtype:wip inserted');
	else 
		dbms_output.put_line('Code-list item Type:ctrpDraftStatus Subtype:wip already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpDraftStatus' and codelst_subtyp = 'readySubmission';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpDraftStatus','readySubmission','Ready For Submission','N',2, null);
		dbms_output.put_line('Code-list item Type:ctrpDraftStatus Subtype:readySubmission inserted');
	else 
		dbms_output.put_line('Code-list item Type:ctrpDraftStatus Subtype:readySubmission already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'ctrpDraftStatus' and codelst_subtyp = 'submitted';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL)
		values (SEQ_ER_CODELST.nextval,null,'ctrpDraftStatus','submitted','Submitted','N',3, null);
		dbms_output.put_line('Code-list item Type:ctrpDraftStatus Subtype:submitted inserted');
	else 
		dbms_output.put_line('Code-list item Type:ctrpDraftStatus Subtype:submitted already exists');
	end if;

	COMMIT;

end;
/
