set define off;

UPDATE er_codelst
SET CODELST_SUBTYP='pi'
WHERE CODELST_TYPE='add_type'
AND CODELST_SUBTYP='PI' ;

UPDATE er_codelst
SET CODELST_SUBTYP='submittingOrg'
WHERE CODELST_TYPE='add_type'
AND CODELST_SUBTYP='SubmittingOrg';

commit;

DECLARE
  v_item_exists number := 0;  
BEGIN
	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'add_type' and codelst_subtyp = 'sponsor';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL) 
		values (SEQ_ER_CODELST.nextval,null,'add_type','sponsor','Sponsor','N',1, null);
		dbms_output.put_line('Code-list item Type:add_type Subtype:sponsor inserted');
	else 
		dbms_output.put_line('Code-list item Type:add_type Subtype:sponsor already exists');
	end if;

	select count(*) into v_item_exists from ER_CODELST where codelst_type = 'add_type' and codelst_subtyp = 'sponsorContact';
	if (v_item_exists = 0) then
		Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP, CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL) 
		values (SEQ_ER_CODELST.nextval,null,'add_type','sponsorContact','Sponsor Contact','N',1, null);
		dbms_output.put_line('Code-list item Type:add_type Subtype:sponsorContact inserted');
	else 
		dbms_output.put_line('Code-list item Type:add_type Subtype:sponsorContact already exists');
	end if;

	COMMIT;
end;
/

UPDATE er_codelst
SET CODELST_CUSTOM_COL='industrial,nonIndustrial'
WHERE CODELST_TYPE='ctrpSubmType'
AND (CODELST_SUBTYP='O' OR CODELST_SUBTYP='U');

UPDATE er_codelst
SET CODELST_CUSTOM_COL='industrial'
WHERE CODELST_TYPE='ctrpSubmType'
AND CODELST_SUBTYP='A' ;

commit;