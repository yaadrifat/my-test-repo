set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = '8.10.0 Build#555' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,98,0,'00_er_version.sql',sysdate,'8.10.0 Build#555');

commit;
