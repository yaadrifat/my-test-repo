--Update sequence numbers of existing skins.
Update er_codelst set CODELST_SEQ = 1 where CODELST_TYPE='skin' and CODELST_SUBTYP = 'vel_default'
Update er_codelst set CODELST_SEQ = 2 where CODELST_TYPE='skin' and CODELST_SUBTYP = 'vel_salmon';
Update er_codelst set CODELST_SEQ = 3 where CODELST_TYPE='skin' and CODELST_SUBTYP = 'vel_green';
Update er_codelst set CODELST_SEQ = 4 where CODELST_TYPE='skin' and CODELST_SUBTYP = 'vel_purple';
Update er_codelst set CODELST_SEQ = 5 where CODELST_TYPE='skin' and CODELST_SUBTYP = 'vel_violet';
Update er_codelst set CODELST_SEQ = 6 where CODELST_TYPE='skin' and CODELST_SUBTYP = 'vel_skyblue';
Update er_codelst set CODELST_SEQ = 7 where CODELST_TYPE='skin' and CODELST_SUBTYP = 'vel_teal';
Update er_codelst set CODELST_SEQ = 8 where CODELST_TYPE='skin' and CODELST_SUBTYP = 'vel_violet';
Update er_codelst set CODELST_SEQ = 9 where CODELST_TYPE='skin' and CODELST_SUBTYP = 'vel_defaultCSS3';

COMMIT;

--INSERTING skins into ER_CODELST
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ) 
values (SEQ_ER_CODELST.nextval,null,'skin','vel_bethematch','Be The Match','N',10);

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ) 
values (SEQ_ER_CODELST.nextval,null,'skin','vel_redmond','Redmond','N',11);

COMMIT;

