eResearch v9.0 build #590
===============================================================================================================

eResearch
In build #589, newSkin folder was accidently released to QA. Please delete this folder from folder-
\server\eresearch\deploy\velos.ear\velos.war\jsp\styles

===============================================================================================================

GARUDA
In this build, the DB objects for the Garuda component are released as the initial step of Garuda integration. 
These DB objects are all under ERES schema and consist of tables that begin with CB_ and UI_ and some ER_ plus their sequence objects. 
However, no Garuda UI component has been added to eResearch yet. Therefore, there is nothing to test for the Garuda component in this build.
===============================================================================================================