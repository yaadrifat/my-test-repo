<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
            xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
            xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:key name="a" match="TNC_ROW" use="concat(A,' ')"/>
  <xsl:key name="oct" match="TNC_ROW" use="concat(OCT,' ')"/>
  <xsl:key name="nov" match="TNC_ROW" use="concat(NOV,' ')"/>
  <xsl:key name="dec" match="TNC_ROW" use="concat(DEC,' ')"/>
  <xsl:key name="jan" match="TNC_ROW" use="concat(JAN,' ')"/>
  <xsl:key name="feb" match="TNC_ROW" use="concat(FEB,' ')"/>
  <xsl:key name="mar" match="TNC_ROW" use="concat(MAR,' ')"/>
  <xsl:key name="apr" match="TNC_ROW" use="concat(APR,' ')"/>
  <xsl:key name="may" match="TNC_ROW" use="concat(MAY,' ')"/>
  <xsl:key name="jun" match="TNC_ROW" use="concat(JUN,' ')"/>
  <xsl:key name="jul" match="TNC_ROW" use="concat(JUL,' ')"/>
  <xsl:key name="aug" match="TNC_ROW" use="concat(AUG,' ')"/>
  <xsl:key name="sep" match="TNC_ROW" use="concat(SEP,' ')"/>
  <xsl:key name="pyear" match="TNC_ROW" use="concat(PREVYEAR,' ')"/>


  <xsl:param name="repName"/>
  <xsl:param name="repBy"/>
  <xsl:param name="repDate"/>
  <xsl:param name="selYear"/>
	  
   <xsl:template match="/">
  <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <fo:layout-master-set>
      <fo:simple-page-master master-name="pdf" page-height="11in" page-width="10.5in" 
          margin-top="0.5in" margin-bottom="0.5in" margin-left="0.2in" margin-right="1.5in">
	<fo:region-body margin-top="0.6in" margin-bottom="1.0in"/>
	<fo:region-before extent="0.5in"/>
        <fo:region-after extent="0.5in"/>
      </fo:simple-page-master>
    </fo:layout-master-set>
      <xsl:apply-templates select="ROWSET/ROW/TNC"/>
  </fo:root>
  </xsl:template>
  
  
  <xsl:template match="ROWSET/ROW/TNC">

  <fo:page-sequence master-reference="pdf" initial-page-number="1" font-family="serif">
  <fo:static-content flow-name="xsl-region-before">
  <fo:block font-size="9pt"> Report Date: <xsl:value-of select="$repDate" /> </fo:block>

  </fo:static-content>
   <fo:static-content flow-name="xsl-region-after">
   <fo:table>
   <fo:table-body>
   
   <fo:table-row>
	<fo:table-cell>  <fo:block text-align="left" font-size="10pt" font-weight="bold"> NMDP Proprietory and Confidential</fo:block>	</fo:table-cell>
	<fo:table-cell> <fo:block  text-align="left" font-size="10pt">Page <fo:page-number/> of  <fo:page-number-citation ref-id="theEnd"/></fo:block>	</fo:table-cell>
   </fo:table-row>
   </fo:table-body>
   </fo:table>
   </fo:static-content>


   <fo:flow flow-name="xsl-region-body">
	<fo:block font-family="Helvetica" text-align="center" font-size="14pt" font-weight="bold">
	National Marrow Donor Program 
	</fo:block>
	
	<fo:block font-family="Helvetica" text-align="center" font-size="13pt" font-weight="bold">
	Analysis of the TNC Count of NMDP Cords Shipped (NCBI and Non-NCBI)
	</fo:block>

	
	<fo:block font-family="Helvetica" text-align="center" font-size="13pt" font-weight="bold">
	For Fiscal Year <xsl:value-of select="$selYear" /> (10/1/<xsl:value-of select="($selYear) - 1 " /> - 9/30/<xsl:value-of select="$selYear" /> )
	</fo:block>

	<fo:block font-family="Helvetica" text-align="center" font-size="13pt" font-weight="bold"  space-after.optimum="45pt">
	As of September 30, <xsl:value-of select="$selYear" /> 
	</fo:block>


	<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
	</fo:block>

	<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
	</fo:block>


	<fo:table width="680pt" table-layout="fixed"  border-width="0.0mm" border-style="solid">
		<fo:table-column column-width="60pt" column-number="1"/>
		<fo:table-column column-width="40pt" column-number="2"/>
		<fo:table-column column-width="40pt" column-number="3"/>
		<fo:table-column column-width="40pt" column-number="4"/>
		<fo:table-column column-width="40pt" column-number="5"/>
		<fo:table-column column-width="40pt" column-number="6"/>
		<fo:table-column column-width="40pt" column-number="7"/>
		<fo:table-column column-width="40pt" column-number="8"/>
		<fo:table-column column-width="40pt" column-number="9"/>
		<fo:table-column column-width="40pt" column-number="10"/>
		<fo:table-column column-width="40pt" column-number="11"/>
		<fo:table-column column-width="40pt" column-number="12"/>
		<fo:table-column column-width="40pt" column-number="13"/>
		<fo:table-column column-width="45pt" column-number="14"/>
		<fo:table-column column-width="45pt" column-number="15"/>
		<fo:table-column column-width="45pt" column-number="16"/>
		<fo:table-column column-width="45pt" column-number="17"/>

	
	 <fo:table-body start-indent="0pt" text-align="start"> 
	
	
	   <fo:table-row>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid" number-columns-spanned="2">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">FY <xsl:value-of select="$selYear" />  YTD</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid" number-columns-spanned="2">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">Entire FY <xsl:value-of select="($selYear) - 1" /> </fo:block>
	  </fo:table-cell>

	</fo:table-row> 
	   

	   
	   <fo:table-row>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid" number-columns-spanned="2">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">-------------------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid" number-columns-spanned="2">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------------------- </fo:block>
	  </fo:table-cell>

	</fo:table-row> 
	   
	   <fo:table-row>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">TNC x 10^7</fo:block>
	  </fo:table-cell>
	  
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">Oct</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">Nov</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">Dec</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">Jan</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">Feb</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">Mar</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">Apr</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">May</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">Jun</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">Jul</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">Aug</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">Sep</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">Vol</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">%</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">Vol</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">%</fo:block>
	  </fo:table-cell>

	</fo:table-row> 
	  
	  <fo:table-row height="7mm">
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">---------------</fo:block>
	  </fo:table-cell>
	  
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>

	</fo:table-row> 
	 
	
	<xsl:for-each select="TNC_ROW[count(. | key('a', concat(A,' '))[1])=1]">
	<fo:table-row height="7mm">
	
	<fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	<fo:block font-size="11pt" text-align="center" >
	 <xsl:variable name="b" select="B" />
         <xsl:value-of select="$b"/>
        </fo:block>
        </fo:table-cell>
	
	<fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	<fo:block font-size="11pt" text-align="center" >
	 <xsl:variable name="oct" select="OCT" />
         <xsl:value-of select="$oct"/>
        </fo:block>
        </fo:table-cell>

	<fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	      <fo:block font-size="11pt" text-align="center">
                   <xsl:variable name="nov" select="NOV" />
                   <xsl:value-of select="$nov"/>
		 </fo:block>
        </fo:table-cell> 	

	 <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" text-align="center">
                   <xsl:variable name="dec" select="DEC" />
                   <xsl:value-of select="$dec"/>
	       </fo:block>
         </fo:table-cell>
	
	<fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" text-align="center">
                   <xsl:variable name="jan" select="JAN" />
                   <xsl:value-of select="$jan"/>
               </fo:block>
         </fo:table-cell>	
	
	<fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" text-align="center">
                   <xsl:variable name="feb" select="FEB" />
                   <xsl:value-of select="$feb"/>
              </fo:block>
            </fo:table-cell>
	    <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" text-align="center">
                   <xsl:variable name="mar" select="MAR" />
                   <xsl:value-of select="$mar"/>
               </fo:block>
            </fo:table-cell>
	    <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" text-align="center">
                   <xsl:variable name="apr" select="APR" />
                   <xsl:value-of select="$apr"/>
              </fo:block>
            </fo:table-cell>
	    <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" text-align="center">
                   <xsl:variable name="may" select="MAY" />
                   <xsl:value-of select="$may"/>
               </fo:block>
            </fo:table-cell> 
	    <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" text-align="center">
                   <xsl:variable name="jun" select="JUN" />
                   <xsl:value-of select="$jun"/>
               </fo:block>
            </fo:table-cell> 

	     <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" text-align="center">
                   <xsl:variable name="jul" select="JUL" />
                   <xsl:value-of select="$jul"/>
               </fo:block>
            </fo:table-cell> 
	    
	     <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" text-align="center">
                   <xsl:variable name="aug" select="AUG" />
                   <xsl:value-of select="$aug"/>
               </fo:block>
            </fo:table-cell> 

	     <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" text-align="center">
                   <xsl:variable name="sep" select="SEP" />
                   <xsl:value-of select="$sep"/>
               </fo:block>
            </fo:table-cell> 

	    
	    <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" text-align="center" font-weight="bold">
		<xsl:variable name="oct" select="OCT" /> <xsl:variable name="nov" select="NOV" />
		    <xsl:variable name="dec" select="DEC" /><xsl:variable name="jan" select="JAN" />
		    <xsl:variable name="feb" select="FEB" /><xsl:variable name="mar" select="MAR" />
		    <xsl:variable name="apr" select="APR" /> <xsl:variable name="may" select="MAY" /> 
		    <xsl:variable name="jun" select="JUN" /> <xsl:variable name="jul" select="JUL" /> 
		    <xsl:variable name="aug" select="AUG" /> <xsl:variable name="sep" select="SEP" /> 
		 
		 <xsl:value-of select="$oct + $nov +$dec + $jan + $feb + $mar + $apr + $may + $jun + $jul + $aug + $sep"/>
               </fo:block>
            </fo:table-cell> 	

	 
	    <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" text-align="center" font-weight="bold">
		<xsl:variable name="oct" select="OCT" /> <xsl:variable name="nov" select="NOV" />
		    <xsl:variable name="dec" select="DEC" /><xsl:variable name="jan" select="JAN" />
		    <xsl:variable name="feb" select="FEB" /><xsl:variable name="mar" select="MAR" />
		    <xsl:variable name="apr" select="APR" /> <xsl:variable name="may" select="MAY" /> 
		    <xsl:variable name="jun" select="JUN" /> <xsl:variable name="jul" select="JUL" /> 
		    <xsl:variable name="aug" select="AUG" /> <xsl:variable name="sep" select="SEP" /> 
		 
		 <xsl:value-of select="format-number((($oct + $nov +$dec + $jan + $feb + $mar + $apr + $may + $jun + $jul + $aug + $sep) div (sum(//OCT) + sum(//NOV) + sum(//DEC) + sum(//JAN) + sum(//FEB) + sum(//MAR) + sum(//APR) + sum(//MAY) +  sum(//JUN) + sum(//JUL) + sum(//AUG) + sum(//SEP)) * 100),'##0.0') "/> %
               </fo:block>
            </fo:table-cell> 	

	     <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" text-align="center">
                   <xsl:variable name="pyear" select="PREVYEAR" />
                   <xsl:value-of select="$pyear"/>
               </fo:block>
            </fo:table-cell> 

	    <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" text-align="center">
                   <xsl:variable name="pyear" select="PREVYEAR" />
                   <xsl:value-of select="format-number((($pyear) div (sum(//PREVYEAR)) * 100),'##0.0') "/>%
               </fo:block>
            </fo:table-cell> 


        </fo:table-row>
	</xsl:for-each>

	  <fo:table-row>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">------</fo:block>
	  </fo:table-cell>

	</fo:table-row> 
	 
	<fo:table-row>
	<fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
		<fo:block font-family="Helvetica" text-align="center" font-size="11pt" font-weight="bold">
		 Total	
		</fo:block>
        </fo:table-cell>
	<fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//OCT)"/>
	       </fo:block>
         </fo:table-cell>
	<fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//NOV)"/>
	       </fo:block>
         </fo:table-cell>
	 <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//DEC)"/>
	       </fo:block>
         </fo:table-cell>
	 <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//JAN)"/>
	       </fo:block>
         </fo:table-cell>
	 <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//FEB)"/>
	       </fo:block>
         </fo:table-cell>
	 <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//MAR)"/>
	       </fo:block>
         </fo:table-cell>
	 <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//APR)"/>
	       </fo:block>
         </fo:table-cell>

	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//MAY)"/>
	       </fo:block>
         </fo:table-cell>

	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//JUN)"/>
	       </fo:block>
         </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//JUL)"/>
	       </fo:block>
         </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//AUG)"/>
	       </fo:block>
         </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//SEP)"/>
	       </fo:block>
         </fo:table-cell>

	 <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//OCT) + sum(//NOV) + sum(//DEC) + sum(//JAN) + sum(//FEB) + sum(//MAR) + sum(//APR) + sum(//MAY) +  sum(//JUN) + sum(//JUL) + sum(//AUG) + sum(//SEP) "/>
	       </fo:block>
         </fo:table-cell>

	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" font-weight="bold" text-align="center" >
	        <xsl:value-of select="format-number((sum(//OCT) + sum(//NOV) + sum(//DEC) + sum(//JAN) + sum(//FEB) + sum(//MAR) + sum(//APR) + sum(//MAY) +  sum(//JUN) + sum(//JUL) + sum(//AUG) + sum(//SEP)) div (sum(//OCT) + sum(//NOV) + sum(//DEC) + sum(//JAN) + sum(//FEB) + sum(//MAR) + sum(//APR) + sum(//MAY) +  sum(//JUN) + sum(//JUL) + sum(//AUG) + sum(//SEP)) * 100,'##0.0') "/> %
	       </fo:block>
         </fo:table-cell>

	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//PREVYEAR)"/>
	       </fo:block>
         </fo:table-cell>

	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
              <fo:block font-size="11pt" font-weight="bold" text-align="center" >
	      <xsl:value-of select="format-number((sum(//PREVYEAR)) div  (sum(//PREVYEAR)) * 100, '###0.0')"/> %
	       </fo:block>
         </fo:table-cell>

	</fo:table-row>
	 
	  <fo:table-row>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center"></fo:block>
	  </fo:table-cell>
	  
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">====</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">====</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">====</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">====</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">====</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">====</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">====</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">====</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">====</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">====</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">====</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">====</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">====</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">====</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">====</fo:block>
	  </fo:table-cell>
	   <fo:table-cell padding="0.4mm" border-width="0.0mm" border-style="solid">
	    <fo:block font-size="11pt" font-weight="bold" text-align="center">====</fo:block>
	  </fo:table-cell>

	</fo:table-row> 
	 
	 </fo:table-body>
        </fo:table>
	<fo:block id="theEnd"/>	
   </fo:flow>
 </fo:page-sequence>  
</xsl:template>
</xsl:stylesheet>
	  
	 