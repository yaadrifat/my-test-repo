update er_browserconf 
set BROWSERCONF_SETTINGS = '{"key":"NEXT_VISIT_DATESORT", "label":"Visit Date", "sortable":true, "resizeable":true,"hideable":true}',
BROWSERCONF_EXPLABEL = 'Visit Date'
where fk_browser in 
(select pk_browser from ER_BROWSER where BROWSER_MODULE ='allSchedules' and BROWSER_NAME='Patient Schedule Browser')
and BROWSERCONF_EXPLABEL = 'Next due'
and BROWSERCONF_COLNAME = 'NEXT_VISIT_DATESORT';

commit;

CREATE OR REPLACE FORCE VIEW "ERES"."ERV_STUDYPAT_UPCOMING_VISIT" ("FK_PER", "PER_SITE", "STUDY_NUMBER", "STUDY_TITLE", "PK_PATPROT", "FK_STUDY", "PER_CODE", "PATSTUDYSTAT_DESC", "PATSTUDYSTAT_SUBTYPE", "PATSTUDYSTAT_ID", "PK_PATSTUDYSTAT", "PATSTUDYSTAT_DATE", "PATSTUDYSTAT_NOTE", "PATSTUDYSTAT_REASON", "PATPROT_ENROLDT", "PATPROT_PATSTDID", "FK_VISIT", "EVENT_ACTUAL_SCHDATE", "NEXT_VISIT", "NEXT_VISIT_NAME", "NEXT_VISIT_NO", "PI", "ASSIGNEDTO_NAME", "PHYSICIAN_NAME", "ENROLLEDBY_NAME", "TREATINGORG_NAME")
AS
  SELECT DISTINCT fk_per,
    per_site,
    study_number,
    study_title,
    pk_patprot,
    fk_study,
    per_code,
    patstudystat_desc,
    patstudystat_subtype,
    patstudystat_id,
    pk_patstudystat,
    patstudystat_date,
    patstudystat_note,
    patstudystat_reason,
    patprot_enroldt,
    patprot_patstdid,
    fk_visit,
    EVENT_ACTUAL_SCHDATE,
    (EVENT_ACTUAL_SCHDATE) next_visit,
    (SELECT visit_name FROM sch_protocol_visit WHERE pk_protocol_visit = fk_visit
    ) next_visit_name,
    fk_visit next_visit_no,
    PI,
    assignedto_name,
    physician_name,
    enrolledby_name,
    treatingorg_name
  FROM erv_patstudy_latest_eve o;
