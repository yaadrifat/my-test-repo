--Rename Top Menu 'Accounts' as 'Personalize'
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Personalize', OBJECT_SUBTYPE='personalize_menu'
WHERE OBJECT_TYPE ='TM'
AND OBJECT_SUBTYPE='accnt_menu' AND OBJECT_NAME='top_menu';

Commit;

--Hide Top Menu 'Homepage'
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_VISIBLE = '0' 
WHERE OBJECT_SUBTYPE='homepage_menu' AND OBJECT_NAME='top_menu';

--Hide Sub Menu 'Personalize' as now it is a Top Menu
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_VISIBLE = '0' 
WHERE OBJECT_SUBTYPE='personal_menu' AND OBJECT_NAME='accnt_menu';

--Repoint the 'Personalize' Sub-Menus to the new 'Personalize' Top Menu.
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_NAME='personalize_menu'
WHERE OBJECT_NAME='personal_menu';

Commit;

--Rename 'Accounts' Sub-Menu to 'Application'
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Application', OBJECT_SUBTYPE = 'application_menu' 
WHERE OBJECT_TYPE ='M' and OBJECT_NAME ='accnt_menu';

Commit;

--Update Sub-menu 'Application' to appear under 'Manage'
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_NAME = 'manage_acct' 
WHERE OBJECT_TYPE ='M' AND OBJECT_NAME ='accnt_menu' and OBJECT_SUBTYPE ='acct_menu';

Commit;

--Update all old 'Accounts' Sub-menus to appear under 'Application'
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_NAME = 'application_menu' 
WHERE object_name = 'acct_menu';

Commit;

--Re-sequence 'Manage' Sub-menus
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_SEQUENCE = '2' 
WHERE OBJECT_TYPE ='M' AND OBJECT_NAME ='manage_accnt' AND OBJECT_SUBTYPE = 'application_menu';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_SEQUENCE = '1', OBJECT_DISPLAYTEXT='Studies'
WHERE OBJECT_TYPE ='M' AND OBJECT_NAME ='manage_accnt' AND OBJECT_SUBTYPE = 'study_menu';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_SEQUENCE = '3', OBJECT_DISPLAYTEXT ='Patients'
WHERE OBJECT_TYPE ='M' AND OBJECT_NAME ='manage_accnt' AND OBJECT_SUBTYPE = 'mgpat_menu';

Commit;

--Update Sub-menu 'Milestones' to appear under 'Manage'
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_TYPE = 'M', OBJECT_NAME = 'manage_accnt', 
OBJECT_SUBTYPE = 'milestone_menu', OBJECT_DISPLAYTEXT ='Milestones'
WHERE OBJECT_SUBTYPE = 'mile_menu' AND OBJECT_NAME = 'mile_menu';

Commit;

--Update Top Menu 'Regulatory Reporting' to appear under 'Reports' as a Sub-menu
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_TYPE = 'M', OBJECT_NAME = 'report_menu' 
WHERE OBJECT_TYPE ='TM' AND OBJECT_SUBTYPE = 'regreporting_menu';

Commit;

--Re-sequence 'Reports' Sub-menus
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_SEQUENCE = (select MAX(OBJECT_SEQUENCE)+1 
from "ERES"."ER_OBJECT_SETTINGS" where OBJECT_NAME = 'report_menu' and OBJECT_SUBTYPE <> 'regreporting_menu') 
WHERE OBJECT_SUBTYPE = 'regreporting_menu';

commit;

--Rename Top Menu 'Research Compliance' to be 'eCompliance'
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'eCompliance' 
WHERE OBJECT_TYPE = 'TM' AND OBJECT_SUBTYPE = 'irb_menu';

Commit;

--Rename Top Menu 'Manage Inventory' to be 'eSample'
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'eSample' 
WHERE OBJECT_TYPE = 'TM' AND OBJECT_SUBTYPE = 'inv_menu';

Commit;

--Rename existing Sub-menus
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Links' 
WHERE OBJECT_TYPE ='M' and OBJECT_NAME ='application_menu' AND OBJECT_SUBTYPE = 'acclinks_menu';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Forms' 
WHERE OBJECT_TYPE ='M' and OBJECT_NAME ='application_menu' AND OBJECT_SUBTYPE = 'formmgmt_menu';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Portals' 
WHERE OBJECT_TYPE ='M' and OBJECT_NAME ='application_menu' AND OBJECT_SUBTYPE = 'portal_menu';

Commit;

/*
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Profile' 
WHERE OBJECT_TYPE ='M' and OBJECT_NAME ='application_menu' AND OBJECT_SUBTYPE = 'my_prof';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Links' 
WHERE OBJECT_TYPE ='M' and OBJECT_NAME ='application_menu' AND OBJECT_SUBTYPE = 'my_links';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Subscriptions' 
WHERE OBJECT_TYPE ='M' and OBJECT_NAME ='application_menu' AND OBJECT_SUBTYPE = 'new_trials';

Commit;*/

--Rename existing Sub-menus
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Search' 
WHERE OBJECT_TYPE ='M' and OBJECT_TYPE='open_menu' and OBJECT_NAME='study_menu';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Search' 
WHERE OBJECT_TYPE ='M' and OBJECT_TYPE='open_menu' and OBJECT_NAME='mgpat_menu';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Search' 
WHERE OBJECT_TYPE ='M' and OBJECT_TYPE='open_menu' and OBJECT_NAME='budget_menu';

Commit;

--Adding a new Search Milestones submenu
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'open_menu' and OBJECT_NAME = 'milestone_menu' ;
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
     (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
      OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
     (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'open_menu', 'milestone_menu', 1, 
      1, 'Search', 0);
    dbms_output.put_line('One row created');
  else
	UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Search' 
	WHERE OBJECT_TYPE ='M' and OBJECT_TYPE='open_menu' and OBJECT_NAME='milestone_menu';

    dbms_output.put_line('Object setting open_menu for milestone_menu already exists');
  end if;
END;
/
Commit;

--Adding a new submenus to Patient search
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'enrolled_menu' and OBJECT_NAME = 'mgpat_menu' ;
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
     (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
      OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
     (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'enrolled_menu', 'mgpat_menu', 3, 
      1, 'Enrolled', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting enrolled_menu for mgpat_menu already exists');
  end if;
  
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'schedule_menu' and OBJECT_NAME = 'mgpat_menu' ;
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
     (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
      OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
     (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'schedule_menu', 'mgpat_menu', 4, 
      1, 'Schedule', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting schedule_menu for mgpat_menu already exists');
  end if;
  
END;
/
Commit;

--Rename existing Sub-menus
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Libraries' 
WHERE OBJECT_TYPE ='TM' and OBJECT_SUBTYPE = 'lib_menu';

Commit;

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Calendars' 
WHERE OBJECT_TYPE ='M' and OBJECT_SUBTYPE = 'callib_menu';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Fields' 
WHERE OBJECT_TYPE ='M' and OBJECT_SUBTYPE = 'fldlib_menu';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Forms' 
WHERE OBJECT_TYPE ='M' and OBJECT_SUBTYPE = 'formlib_menu';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Events' 
WHERE OBJECT_TYPE ='M' and OBJECT_SUBTYPE = 'evtlib_menu';

Commit;

--Rename and re-sequence existing Sub-menus
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Search' 
WHERE OBJECT_TYPE ='M' and OBJECT_NAME = 'callib_menu' and OBJECT_SUBTYPE ='srch_cal';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Add Calendar', OBJECT_SEQUENCE = 3
WHERE OBJECT_TYPE ='M' and OBJECT_NAME = 'callib_menu' and OBJECT_SUBTYPE ='add_newcal';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Add Type', OBJECT_SEQUENCE = 2
WHERE OBJECT_TYPE ='M' and OBJECT_NAME = 'callib_menu' and OBJECT_SUBTYPE ='newcal_type';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Copy' 
WHERE OBJECT_TYPE ='M' and OBJECT_NAME = 'callib_menu' and OBJECT_SUBTYPE ='copy_cal';

Commit;

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Search' 
WHERE OBJECT_TYPE ='M' and OBJECT_NAME = 'formlib_menu' and OBJECT_SUBTYPE ='srch_form';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Add Form', OBJECT_SEQUENCE = 3
WHERE OBJECT_TYPE ='M' and OBJECT_NAME = 'formlib_menu' and OBJECT_SUBTYPE ='add_newform';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Add Type', OBJECT_SEQUENCE = 2
WHERE OBJECT_TYPE ='M' and OBJECT_NAME = 'formlib_menu' and OBJECT_SUBTYPE ='form_type';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Copy' 
WHERE OBJECT_TYPE ='M' and OBJECT_NAME = 'formlib_menu' and OBJECT_SUBTYPE ='copy_form';

Commit;

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Search' 
WHERE OBJECT_TYPE ='M' and OBJECT_NAME = 'fldlib_menu' and OBJECT_SUBTYPE ='srch_fld';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Add Edit Box', OBJECT_SEQUENCE = 3
WHERE OBJECT_TYPE ='M' and OBJECT_NAME = 'fldlib_menu' and OBJECT_SUBTYPE ='add_fld';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Add Multiple Choice', OBJECT_SEQUENCE = 4
WHERE OBJECT_TYPE ='M' and OBJECT_NAME = 'fldlib_menu' and OBJECT_SUBTYPE ='add_mulfld';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Add Category' , OBJECT_SEQUENCE = 2
WHERE OBJECT_TYPE ='M' and OBJECT_NAME = 'fldlib_menu' and OBJECT_SUBTYPE ='fld_cat';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Copy' 
WHERE OBJECT_TYPE ='M' and OBJECT_NAME = 'fldlib_menu' and OBJECT_SUBTYPE ='copy_fld';

Commit;

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Search' 
WHERE OBJECT_TYPE ='M' and OBJECT_NAME = 'evtlib_menu' and OBJECT_SUBTYPE ='srch_evnt';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Add Event', OBJECT_SEQUENCE = 3
WHERE OBJECT_TYPE ='M' and OBJECT_NAME = 'evtlib_menu' and OBJECT_SUBTYPE ='add_evnt';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Add Category', OBJECT_SEQUENCE = 2
WHERE OBJECT_TYPE ='M' and OBJECT_NAME = 'evtlib_menu' and OBJECT_SUBTYPE ='evnt_cat';

Commit;