create or replace
TRIGGER "SCH_PROTOCOL_VISIT_AD0" 
AFTER DELETE
ON SCH_PROTOCOL_VISIT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data varchar2(4000);

begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_PROTOCOL_VISIT', :old.rid, 'D');

  deleted_data :=
   to_char(:old.pk_protocol_visit) || '|' ||
   to_char(:old.fk_protocol) || '|' ||
   to_char(:old.visit_no) || '|' ||
   :old.visit_name || '|' ||
   :old.description || '|' ||
   to_char(:old.displacement) || '|' ||
   to_char(:old.num_months) || '|' ||
   to_char(:old.num_weeks) || '|' ||
   to_char(:old.num_days) || '|' ||
   to_char(:old.insert_after) || '|' ||  
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.last_modified_by) || '|' ||   
   to_char(:old.last_modified_date) || '|' ||
   to_char(:old.created_on) || '|' ||
   :old.ip_add || '|' ||
   to_char(:old.insert_after_interval) || '|' ||  
   :OLD.insert_after_interval_unit || '|' ||
   :OLD.protocol_type || '|' ||
   :OLD.visit_type || '|' ||
   to_char(:old.offline_flag) || '|' ||
   to_char(:old.hide_flag) || '|' ||
   to_char(:old.no_interval_flag) || '|' ||
   to_char(:old.win_before_number) || '|' ||
   :OLD.win_before_unit || '|' ||
   to_char(:old.win_after_number) || '|' ||
   :OLD.win_after_unit;


insert into audit_delete
(raid, row_data) values (raid, deleted_data);
end; 
/