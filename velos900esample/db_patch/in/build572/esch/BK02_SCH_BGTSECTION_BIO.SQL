create or replace
TRIGGER "ESCH"."SCH_BGTSECTION_BI0"
BEFORE INSERT
ON SCH_BGTSECTION
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  insert_data CLOB; 
  maxsection  NUMBER(38,0); --ADDED BY BIKASH
BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction
    (raid, 'SCH_BGTSECTION', erid, 'I', :NEW.LAST_MODIFIED_BY );
    if :NEW.FK_VISIT is not null  --ADDED BY BIKASH
   then   
   maxsection := GETMAXSECTION(:NEW.FK_BGTCAL) + 1; --BK-MAR-22-11 FIX 5888   
   :NEW.BGTSECTION_SEQUENCE := :NEW.BGTSECTION_SEQUENCE + maxsection;
end if;
  --   Added by Ganapathy on 06/23/05 for Audit insert

  insert_data:=:NEW.BGTSECTION_PATNO||'|'||:NEW.BGTSECTION_NOTES||'|'|| :NEW.BGTSECTION_PERSONLFLAG||'|'||
       :NEW.PK_BUDGETSEC||'|'|| :NEW.FK_BGTCAL||'|'||:NEW.BGTSECTION_NAME||'|'||
      :NEW.BGTSECTION_VISIT||'|'||:NEW.BGTSECTION_DELFLAG||'|'|| :NEW.BGTSECTION_SEQUENCE||'|'||
      :NEW.RID||'|'|| :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
      TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
      :NEW.IP_ADD||'|'||:NEW.BGTSECTION_TYPE ||'|'||
--JM: added the following columns
      :NEW.SRT_COST_TOTAL ||'|'|| :NEW.SRT_COST_GRANDTOTAL ||'|'|| :NEW.SOC_COST_TOTAL ||'|'||
      :NEW.SOC_COST_GRANDTOTAL ||'|'|| :NEW.SRT_COST_SPONSOR ||'|'|| :NEW.SRT_COST_VARIANCE ||'|'||
      :NEW.SOC_COST_SPONSOR ||'|'|| :NEW.SOC_COST_VARIANCE || :NEW.FK_VISIT;



  INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
END;