Declare

	lkpID INTEGER DEFAULT 0;
	lkpvwID INTEGER DEFAULT 0;
	lkpcolID INTEGER DEFAULT 0;
	lkpvwcolID INTEGER DEFAULT 0;


BEGIN

	SELECT MAX(PK_LKPCOL)+1 INTO lkpcolID FROM ER_LKPCOL;
	SELECT MAX(PK_LKPVIEWCOL)+1 INTO lkpvwcolID FROM ER_LKPVIEWCOL;

	select PK_LKPLIB into lkpID  from ER_LKPLIB where LKPTYPE_TYPE='dyn_s' and LKPTYPE_NAME='dynReports' and LKPTYPE_DESC='Milestone Rules';

	select PK_LKPVIEW into lkpvwID from ER_LKPVIEW where LKPVIEW_NAME='Milestone Rules';

	INSERT INTO ER_LKPCOL ( PK_LKPCOL, FK_LKPLIB, LKPCOL_NAME, LKPCOL_DISPVAL, LKPCOL_DATATYPE, LKPCOL_LEN, LKPCOL_TABLE, LKPCOL_KEYWORD )
	VALUES (lkpcolID, lkpID, 'EVENT_NAME', 'Event Name', 'varchar', NULL, 'REP_MILESTONE_RULES', 'EVENT_NAME');

	INSERT INTO ER_LKPVIEWCOL ( PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY )
	VALUES (lkpvwcolID, lkpcolID, NULL, 24, lkpvwID, '10%', 'Y');

COMMIT;
END;
/






