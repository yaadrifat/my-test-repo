/* This readMe is specific to Velos eResearch version 9.0 build #624 */

=====================================================================================================================================
Garuda :
DB Patch:-
	   i) DL04_MENU.sql  :- This SQL contains the Manage Link sub-menu of CBU Tools. These are specific to Garuda Project.
	   
=====================================================================================================================================
eResearch:

INF-22311 enhancement is released in this build.Please find the .xls file released with this build. The .xls file contains 3 sheets and 
as per the communication done with velos only two sheets i.e Sheet1 and Sheet2 are needed to be released in this version9.0. There are places
in sheet1 and sheet2 which are not done and marked as "Not Done" in Bold in the .xls file and will be released in upcoming builds.

=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1	budgetAuditReport.jsp
2	budrepretrieve.jsp
3	calendarAuditReport.jsp
4	calrepretrieve.jsp
5	labelBundle.properties
6	LC.java
7	MC.java
8	messageBundle.properties
9	protocoltabs.jsp
10	updateReasonModule.jsp 
=====================================================================================================================================
