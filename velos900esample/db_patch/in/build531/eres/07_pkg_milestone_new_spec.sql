create or replace
PACKAGE      Pkg_Milestone_New
AS

 PROCEDURE SP_TRANSFER_ACH_PM;
 PROCEDURE SP_TRANSFER_ACH_VMEM;
 PROCEDURE   SP_TRANSFER_ACH_SM;
 
 FUNCTION f_getMilestoneDesc(p_milestoneid IN NUMBER) RETURN varchar2; 

 PROCEDURE   sp_process_milestones;

pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'PKG_MILESTONE_NEW', pLEVEL  => Plog.LFATAL);

FUNCTION F_GET_MILE_DESC_SQL(P_FROM_DATE IN VARCHAR2,  P_TO_DATE IN VARCHAR2, p_site IN VARCHAR2,p_study IN VARCHAR2, p_Division IN VARCHAR2,
p_tarea IN VARCHAR2, p_restype IN VARCHAR2, p_Sponsor IN VARCHAR2, p_studystatus IN VARCHAR2, p_user IN VARCHAR2,p_Patient IN VARCHAR2,
p_budget IN VARCHAR2,p_account IN NUMBER,p_loggedinuser IN NUMBER) RETURN CLOB;

FUNCTION F_GET_MILE_INTERVAL_XML(P_FROM_DATE IN VARCHAR2,  P_TO_DATE IN VARCHAR2,p_study IN VARCHAR2, p_Division IN VARCHAR2,
p_tarea IN VARCHAR2, p_restype IN VARCHAR2, p_Sponsor IN VARCHAR2, p_studystatus IN VARCHAR2, p_user IN VARCHAR2,
p_account IN NUMBER ,p_loggedinuser IN NUMBER) RETURN CLOB;

FUNCTION F_GET_MILE_ARAP_XML(P_FROM_DATE IN VARCHAR2,  P_TO_DATE IN VARCHAR2,p_study IN VARCHAR2, p_Division IN VARCHAR2,
p_tarea IN VARCHAR2, p_restype IN VARCHAR2, p_Sponsor IN VARCHAR2, p_studystatus IN VARCHAR2, p_user IN VARCHAR2,
p_account IN NUMBER ,p_loggedinuser IN NUMBER) RETURN CLOB;

FUNCTION getMileInvoiceAmount(p_pkmilestone NUMBER, p_fromdate VARCHAR2, p_to_date VARCHAR2) RETURN NUMBER;

FUNCTION getMilePaymentAmount(p_pkmilestone NUMBER, p_fromdate VARCHAR2, p_to_date VARCHAR2 ,p_payment_type VARCHAR2) RETURN NUMBER;

FUNCTION F_GET_MILE_PAY_DISCREPANCY_XML(P_FROM_DATE IN VARCHAR2,  P_TO_DATE IN VARCHAR2,p_study IN VARCHAR2, p_Division IN VARCHAR2,
p_tarea IN VARCHAR2, p_restype IN VARCHAR2, p_Sponsor IN VARCHAR2, p_studystatus IN VARCHAR2, p_user IN VARCHAR2,
p_account IN NUMBER ,p_loggedinuser IN NUMBER) RETURN CLOB;

FUNCTION F_GET_MILE_FORECAST_XML(P_FROM_DATE IN VARCHAR2, P_TO_DATE IN VARCHAR2,p_study IN VARCHAR2, p_Division IN VARCHAR2,
p_tarea IN VARCHAR2, p_restype IN VARCHAR2, p_Sponsor IN VARCHAR2, p_studystatus IN VARCHAR2, p_user IN VARCHAR2,
p_account IN NUMBER ,p_loggedinuser IN NUMBER) RETURN CLOB;

FUNCTION F_GET_MILE_FOREDTL_XML(P_FROM_DATE IN VARCHAR2, P_TO_DATE IN VARCHAR2,
p_site IN VARCHAR2, p_study IN VARCHAR2, p_Division IN VARCHAR2,
p_tarea IN VARCHAR2, p_restype IN VARCHAR2, p_Sponsor IN VARCHAR2, p_studystatus IN VARCHAR2, p_user IN VARCHAR2,
p_patient IN VARCHAR2,p_account IN NUMBER ,p_loggedinuser IN NUMBER) RETURN CLOB;

FUNCTION F_GET_MILE_FORECAST_COUNT(p_study IN NUMBER, p_rule IN VARCHAR2, p_count IN NUMBER,p_user IN NUMBER,p_fromdate IN VARCHAR2,
 p_todate IN VARCHAR2 ,p_event IN VARCHAR2, p_visit IN NUMBER,p_cal IN NUMBER) RETURN NUMBER;

 PROCEDURE   SP_SYNCH_INV_DETAILS(p_inv IN NUMBER, p_user IN NUMBER, p_ipAdd IN VARCHAR2, o_ret OUT NUMBER);

 PROCEDURE   SP_CREATE_VM_MILESTONES(p_budget IN NUMBER, p_study IN NUMBER, p_user IN NUMBER, p_ipAdd IN VARCHAR2, p_bgtcalId  IN NUMBER,
 p_rule IN NUMBER, p_eventstatus IN NUMBER,p_count IN NUMBER, p_patstatus IN NUMBER,p_limit IN NUMBER, p_payment_type IN NUMBER,
 p_paymentfor IN NUMBER,  o_ret OUT NUMBER);

 PROCEDURE   SP_CREATE_EM_MILESTONES(p_budget IN NUMBER, p_study IN NUMBER, p_user IN NUMBER, p_ipAdd IN VARCHAR2, p_bgtcalId  IN NUMBER,
 p_rule IN NUMBER, p_eventstatus IN NUMBER,p_count IN NUMBER, p_patstatus IN NUMBER,p_limit IN NUMBER, p_payment_type IN NUMBER,
 p_paymentfor IN NUMBER,  o_ret OUT NUMBER);

PROCEDURE   SP_CREATE_AM_MILESTONES(p_budget IN NUMBER, p_study IN NUMBER, p_user IN NUMBER, p_ipAdd IN VARCHAR2, p_payment_type IN NUMBER,
 p_paymentfor IN NUMBER,  o_ret OUT NUMBER,p_bgtcalid NUMBER);

END Pkg_Milestone_New;
/