CREATE OR REPLACE TRIGGER "ER_SUBMISSION_PROVISO_AD0" 
AFTER DELETE
ON ER_SUBMISSION_PROVISO
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
--  deleted_data varchar2(2000);
  deleted_data clob;
begin

  select seq_audit.nextval into raid from dual;
  audit_trail.record_transaction
    (raid, 'ER_SUBMISSION_PROVISO', :old.rid, 'D');

  deleted_data :=
            :OLD.PK_SUBMISSION_PROVISO||'|'||:OLD.FK_SUBMISSION||'|'||
              :OLD.FK_SUBMISSION_BOARD||'|'||:OLD.FK_USER_ENTEREDBY||'|'||:OLD.rid||'|'||
              TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
              :OLD.CREATOR ||'|'||
              :OLD.IP_ADD||'|'|| TO_CHAR(:OLD.LAST_MODIFIED_BY) ||'|'||
              TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) ||'|'
              || TO_CHAR(:OLD.PROVISO_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)
              || '|' || dbms_lob.SUBSTR(:OLD.PROVISO,3500,1) || '|' || :old.fk_codelst_provisotype;

  
insert into audit_delete
(raid, row_data) values (raid, dbms_lob.substr(deleted_data,4000,1));
end;
/

