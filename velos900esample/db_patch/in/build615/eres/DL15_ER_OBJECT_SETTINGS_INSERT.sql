set define off;

DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'publishrep_menu' and OBJECT_NAME = 'report_menu' ;
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
     (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
      OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
     (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'publishrep_menu', 'report_menu',14, 0, 'Published Reports', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting publishrep_menu for report_menu already exists');
  end if;

  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'chart_menu' and OBJECT_NAME = 'report_menu' ;
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
     (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
      OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
     (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'chart_menu', 'report_menu', 15, 0, 'Chart', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting chart_menu for report_menu already exists');
  end if;
END;
/ 
commit;