set define off;
Declare

lkpID INTEGER DEFAULT 0;
lkpvwID INTEGER DEFAULT 0;
lkpcolID INTEGER DEFAULT 0;
lkpvwcolID INTEGER DEFAULT 0;
lkpvwcolSeq INTEGER DEFAULT 0;


BEGIN
	
			
			SELECT MAX(PK_LKPLIB)+1 INTO lkpID FROM ER_LKPLIB;

			SELECT MAX(PK_LKPVIEW)+1 INTO lkpvwID FROM ER_LKPVIEW;

			SELECT max(pk_LKPCOL)+1 into lkpcolID FROM er_lkpcol;

			SELECT MAX(PK_LKPVIEWCOL)+1 INTO lkpvwcolID FROM ER_LKPVIEWCOL;
			lkpvwcolSeq :=1; 		
			
			-- INSERTING into ER_LKPLIB
			INSERT INTO ER_LKPLIB (PK_LKPLIB,LKPTYPE_NAME,LKPTYPE_VERSION,LKPTYPE_DESC,LKPTYPE_TYPE,FK_ACCOUNT,FK_SITE) 
			VALUES (lkpID,'dynReports',null,'Library SCI','dyn_a',null,null);			
			
			-- INSERTING into ER_LKPVIEW
			INSERT INTO ER_LKPVIEW (PK_LKPVIEW,LKPVIEW_NAME,LKPVIEW_DESC,FK_LKPLIB,LKPVIEW_RETDATA,LKPVIEW_RETDISP,LKPVIEW_ISACTIVE,LKPVIEW_IGNOREFILTER,LKPVIEW_FILTER,LKPVIEW_KEYWORD) 
			VALUES (lkpvwID,'Library SCI',null,lkpID,null,null,null,null,'fk_account=[:ACCID]',null); 

			-- INSERTING into ER_LKPCOL and ER_LKPVIEWCOL
			INSERT INTO ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) 
			VALUES (lkpcolID, lkpID,'CALENDAR_NAME','Calendar Name','number',null,'ERV_LIBRARY_SCI','CALENDAR_NAME');
			INSERT INTO ER_LKPVIEWCOL ( PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY ) 
			VALUES ( lkpvwcolID, lkpcolID, 'Y', lkpvwcolSeq, lkpvwID, '10%', 'Y');
			lkpvwcolID := lkpvwcolID+1; 
			lkpvwcolSeq := lkpvwcolSeq +1;
			lkpcolID := lkpcolID+1;

			INSERT INTO ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) 
			VALUES (lkpcolID, lkpID,'CALENDAR_STATUS','Calendar Status','number',null,'ERV_LIBRARY_SCI','CALENDAR_STATUS');
			INSERT INTO ER_LKPVIEWCOL ( PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY ) 
			VALUES ( lkpvwcolID, lkpcolID, 'Y', lkpvwcolSeq, lkpvwID, '10%', 'Y');
			lkpvwcolID := lkpvwcolID+1; 
			lkpvwcolSeq := lkpvwcolSeq +1;
			lkpcolID := lkpcolID+1;

			INSERT INTO ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) 
			VALUES (lkpcolID, lkpID,'COST_CATEGORY','Cost category','number',null,'ERV_LIBRARY_SCI','COST_CATEGORY');
			INSERT INTO ER_LKPVIEWCOL ( PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY ) 
			VALUES ( lkpvwcolID, lkpcolID, 'Y', lkpvwcolSeq, lkpvwID, '10%', 'Y');
			lkpvwcolID := lkpvwcolID+1; 
			lkpvwcolSeq := lkpvwcolSeq +1;			
			lkpcolID := lkpcolID+1;

			INSERT INTO ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) 
			VALUES (lkpcolID, lkpID,'SUBCOST_ITEM_NAME','Cost Item Name','varchar2',null,'ERV_LIBRARY_SCI','SUBCOST_ITEM_NAME');
			INSERT INTO ER_LKPVIEWCOL ( PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY ) 
			VALUES ( lkpvwcolID, lkpcolID, 'Y', lkpvwcolSeq, lkpvwID, '10%', 'Y');
			lkpvwcolID := lkpvwcolID+1; 
			lkpvwcolSeq := lkpvwcolSeq +1;			
			lkpcolID := lkpcolID+1;

			INSERT INTO ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) 
			VALUES (lkpcolID, lkpID,'SUBCOST_ITEM_COST','Unit Cost','number',null,'ERV_LIBRARY_SCI','SUBCOST_ITEM_COST');
			INSERT INTO ER_LKPVIEWCOL ( PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY ) 
			VALUES ( lkpvwcolID, lkpcolID, 'Y', lkpvwcolSeq, lkpvwID, '10%', 'Y');
			lkpvwcolID := lkpvwcolID+1; 
			lkpvwcolSeq := lkpvwcolSeq +1;
			lkpcolID := lkpcolID+1;
			
			INSERT INTO ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) 
			VALUES (lkpcolID, lkpID,'SUBCOST_ITEM_UNIT','Number of Unit','number',null,'ERV_LIBRARY_SCI','SUBCOST_ITEM_UNIT');
			INSERT INTO ER_LKPVIEWCOL ( PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY ) 
			VALUES ( lkpvwcolID, lkpcolID, 'Y', lkpvwcolSeq, lkpvwID, '10%', 'Y');
			lkpvwcolID := lkpvwcolID+1; 
			lkpvwcolSeq := lkpvwcolSeq +1;
			lkpcolID := lkpcolID+1;
			
			INSERT INTO ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) 
			VALUES (lkpcolID, lkpID,'FK_ACCOUNT','FK_ACCOUNT','number',null,'ERV_LIBRARY_SCI','LKP_PK');
			INSERT INTO ER_LKPVIEWCOL ( PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY ) 
			VALUES ( lkpvwcolID, lkpcolID, 'Y', lkpvwcolSeq, lkpvwID, '10%', 'N');
			lkpvwcolID := lkpvwcolID+1; 
			lkpvwcolSeq := lkpvwcolSeq +1;
			lkpcolID := lkpcolID+1;
			
			INSERT INTO ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD) 
			VALUES (lkpcolID, lkpID,'VISITS','Visits','varchar2',null,'ERV_LIBRARY_SCI','VISITS');
			INSERT INTO ER_LKPVIEWCOL ( PK_LKPVIEWCOL, FK_LKPCOL, LKPVIEW_IS_SEARCH, LKPVIEW_SEQ, FK_LKPVIEW, LKPVIEW_DISPLEN, LKPVIEW_IS_DISPLAY ) 
			VALUES ( lkpvwcolID, lkpcolID, 'Y', lkpvwcolSeq, lkpvwID, '10%', 'Y');				
			

			commit;				
			
			
			
END;
/ 