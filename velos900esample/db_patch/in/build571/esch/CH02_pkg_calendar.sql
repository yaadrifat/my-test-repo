set define off;

create or replace
PACKAGE BODY      pkg_calendar AS
 PROCEDURE sp_delete_event_visit (p_eventOrVisitIds IN ARRAY_STRING , tname IN varchar2, p_flags IN ARRAY_STRING) as
/*
   Date : 10Apr2008
   Author: Jnanamay
   Purpose: Delete Visits, delete Event(s) and Visit(s) associated to a calendar
*/

 v_cnt NUMBER;
 v_eventOrVisitId NUMBER;
 v_flag number;
 i NUMBER;
 v_visit_string varchar2(10000);
 v_event_string varchar2(10000);

 v_flagDelVis number;
 v_flagDelEvt number;

 Begin


v_cnt := p_eventOrVisitIds.COUNT;

   i:=1;
   v_flagDelVis := 0;
   v_flagDelEvt := 0;

  WHILE i <= v_cnt LOOP


      v_eventOrVisitId := TO_NUMBER(p_eventOrVisitIds(i));
      v_flag := TO_NUMBER(p_flags(i));

      if (v_flag=1 ) then
          v_visit_string := v_visit_string || v_eventOrVisitId || ',';
          v_flagDelVis := v_flagDelVis + 1 ;
      else
          v_event_string := v_event_string || v_eventOrVisitId || ',';
          v_flagDelEvt := v_flagDelEvt + 1 ;
      end if;

      i := i+1;
  END LOOP;


  if (v_flagDelVis>0 ) then
    v_visit_string := substr(v_visit_string,1,length(v_visit_string)-1);
    execute immediate 'delete from sch_protocol_visit where pk_protocol_visit in (' || v_visit_string || ') and (OFFLINE_FLAG <>1 or OFFLINE_FLAG is null)';
    --KM- Modified for Not Defined Visits and Events
    if (lower(tname) = 'event_def') then
            execute immediate 'delete from event_def where fk_visit in (' || v_visit_string || ') and (DISPLACEMENT <> 0 or DISPLACEMENT IS NULL) ';
    else
            execute immediate 'delete from event_assoc where fk_visit in (' || v_visit_string || ') and (DISPLACEMENT <> 0 or DISPLACEMENT IS NULL) and (not offline_flag >0 or offline_flag is null)';
    end if;

  end if;

  if (v_flagDelEvt>0) then
    v_event_string := substr(v_event_string,1,length(v_event_string)-1);

    if (lower(tname) = 'event_def') then
             execute immediate 'delete from event_def where event_id in ('||v_event_string || ') and (DISPLACEMENT <> 0 or DISPLACEMENT IS NULL) ';
    else
             execute immediate 'delete from event_assoc where event_id in ('||v_event_string || ') and (DISPLACEMENT <> 0 or DISPLACEMENT IS NULL) and (not offline_flag >0 or offline_flag is null)';
    end if;

  end if;

  --JM: 09Mar2011: #D-FIN26
  if (v_flagDelVis>0 ) then
    execute immediate 'Delete from er_milestone where FK_VISIT in (' || v_visit_string || ' ) ' ;

    Delete from SCH_SUBCOST_ITEM_VISIT where FK_PROTOCOL_VISIT in (v_visit_string);      

  elsif (v_flagDelEvt>0) then
    execute immediate 'Delete from er_milestone where FK_EVENTASSOC in (' || v_event_string || ' ) ' ;
  end if;



 END sp_delete_event_visit;



 FUNCTION f_get_event_sequence(p_visit_id NUMBER, p_event_name varchar2, p_table varchar2)
 RETURN Number AS
/*
  Date : 10Apr2008
  Author: Jnanamay
  Purpose:
            a) to stop duplicate Event entry for a visit
            b) returns the maximum sequence number for the existing Events under a visit

*/

 v_retval number;
 v_count number;
 max_seq number;
 v_ret number;


 BEGIN

   execute immediate 'select count(*) from ' ||p_table || ' where trim(lower(name))= '''||trim(lower(p_event_name))||''' and fk_visit= ' || p_visit_id
   into v_count ;

   if (v_count>0) then
    v_ret := -3;
   RETURN v_ret;

   else
    --KM-09Oct09
    execute immediate 'select max(EVENT_SEQUENCE) from ' ||p_table|| ' where fk_visit = ' ||p_visit_id ||' and (DISPLACEMENT <> 0 or DISPLACEMENT IS NULL) '
    into max_seq ;

    v_ret := max_seq;

   end if;

    return v_ret;


 END f_get_event_sequence;



PROCEDURE sp_updteOflgEventsVisits(p_eventId IN number , p_offline_flag IN varchar2) as

/*
  Date : 24Apr2008
  Author: Jnanamay
  Purpose:
            a) to update the offline flag for a caledar, its visits and events

*/

BEGIN

    update EVENT_ASSOC set OFFLINE_FLAG = p_offline_flag where event_id= p_eventId;
    update EVENT_ASSOC set OFFLINE_FLAG = p_offline_flag where CHAIN_ID= p_eventId ;--and DISPLACEMENT <> 0;
    update sch_protocol_visit set OFFLINE_FLAG = p_offline_flag where fk_protocol = p_eventId;

END sp_updteOflgEventsVisits;


PROCEDURE sp_hide_unhide_EventsVisits(p_eventOrVisitIds IN ARRAY_STRING,  p_hide_flag IN varchar2, p_visit_flags IN ARRAY_STRING) as


/*
  Date : 25Apr2008
  Author: Jnanamay
  Purpose:
            a) to update the hide flag for a caledar, its visits and events

*/

v_count_eventsOrVisits number;
j number;
v_eventOrVisit number;
v_visit_flag number;
v_visit_ids varchar2(10000);
v_event_ids varchar2(10000);
v_count_VisIds number;
v_count_EvtIds number;

BEGIN



v_count_eventsOrVisits := p_eventOrVisitIds.COUNT;

j := 1;
v_count_VisIds := 0;
v_count_EvtIds := 0;

while j <= v_count_eventsOrVisits loop

    v_eventOrVisit    := to_number(p_eventOrVisitIds(j));
    v_visit_flag := to_number(p_visit_flags(j)) ;


    if (v_visit_flag=1) then

        v_visit_ids := v_visit_ids ||  v_eventOrVisit || ',';
        v_count_VisIds := v_count_VisIds+1;




    else
        v_event_ids := v_event_ids||  v_eventOrVisit || ',';
        v_count_EvtIds := v_count_EvtIds+1;



    end if;
        j := j + 1;


end loop;

if (v_count_VisIds > 0) then

     v_visit_ids := substr(v_visit_ids,1,length(v_visit_ids)-1);
     execute immediate 'update sch_protocol_visit set HIDE_FLAG = ' ||p_hide_flag||' where PK_PROTOCOL_VISIT in (' || v_visit_ids || ') ';
         execute immediate 'update event_assoc set HIDE_FLAG = '||p_hide_flag|| ' where fk_visit in (' || v_visit_ids || ') and DISPLACEMENT <> 0';

end if;

if (v_count_EvtIds > 0) then

     v_event_ids := substr(v_event_ids,1,length(v_event_ids)-1);
     execute immediate 'update EVENT_ASSOC set HIDE_FLAG = '||p_hide_flag|| ' where event_id in ('|| v_event_ids||')';

end if;


END sp_hide_unhide_EventsVisits;

PROCEDURE sp_update_Pat_Schedules_now(p_calId in number, p_chkVal in number, p_statId in number, p_date IN DATE, p_user in number, p_ipAdd in varchar2) as
 /*
   Date : 30Apr2008
   Author: Jnanamay
   Purpose: update patient schedule when calendar status changes from Offline for editing to Active
   status: undegoing change
   MOdified by Sonia to remove redundant code and to fix the slowness problem
*/

v_event_id number;
v_event_flag number;
v_newevt number;
v_notdone number;

v_fk_per number;
v_patProt number;
v_calassoc         CHAR (1);
v_protocol_study   NUMBER;

--v_ptprot_st_dt date;
v_ptprot_st_dt char(10);
v_seq_num number;
 --i NUMBER;
v_days NUMBER;
v_patprot_schday number;
v_mindisp number;

v_cnt number;

v_sql              VARCHAR2 (2000);
v_study number;

v_curr_disc_status number;

v_option1 Number;
v_option2 Number;


BEGIN

begin



      SELECT pk_codelst INTO v_notdone FROM SCH_CODELST WHERE codelst_subtyp = 'ev_notdone';



      SELECT event_calassocto, chain_id
          INTO v_calassoc, v_protocol_study
           FROM EVENT_ASSOC
          WHERE event_id = p_calId AND UPPER (event_type) = 'P';

 --KM-No interval defined visits
  for m in ( select event_id, offline_flag from event_assoc where CHAIN_ID = p_calId and (displacement<>0 or displacement is null)
    and (offline_flag in (0,2) or offline_flag is null) order  by trim(lower(name)) asc )

  loop


----------JM: 05May2008-----------------------------------------------------------------------

      if ( p_chkVal = 1 ) then

       for n in (select fk_per, pk_patprot, PATPROT_START, patprot_schday, patprot_stat
       from er_patprot a where  fk_protocol = p_calId
        )
       loop


        --JM: 30Jun2008: #3498
        if (n.patprot_stat = 0) then
                  v_curr_disc_status := 5;
        else
                  v_curr_disc_status := 0;

        end if;


        if (m.offline_flag = 0 or m.offline_flag is null) then

            sp_insert_event2SCH(p_calId  ,n.pk_patprot,m.event_id,n.PATPROT_START,
            n.fk_per,p_user ,p_ipadd ,v_notdone ,v_curr_disc_status );


        end if;

       end loop;

      end if; --end of p_chkVal = 1------------JM: 05May2008

    ----------------------------------------------------------------------------------------------

      if  ( p_chkVal = 2 ) then


        for n in ( select fk_per, pk_patprot, PATPROT_START, patprot_schday --into v_fk_per, v_patProt, v_ptprot_st_dt, v_patprot_schday
         from er_patprot a where fk_protocol = p_calId and patprot_stat = 1
          )
         loop





        if (m.offline_flag = 0 or m.offline_flag is null) then

            sp_insert_event2SCH(p_calId  ,n.pk_patprot,m.event_id,n.PATPROT_START,
            n.fk_per,p_user ,p_ipadd ,v_notdone ,0);


        end if;


 end loop;--2


end if; ---if 2


     if ( p_chkVal = 3 ) then

       --JM: 16Jun2008: modified for #3502
       for n in (
           select fk_per, pk_patprot, PATPROT_START, patprot_schday
           from er_patprot a where fk_protocol = p_calId and patprot_stat = 1
           and exists ( select * from er_patstudystat b
           where b.fk_per = a.fk_per and b.fk_study = a.fk_study and
           fk_codelst_stat = p_statId and patstudystat_date >= to_date(p_date, 'dd/mm/yy')
            )
       )
       loop


        if (m.offline_flag = 0 or m.offline_flag is null) then

            sp_insert_event2SCH(p_calId  ,n.pk_patprot,m.event_id,n.PATPROT_START,
            n.fk_per,p_user ,p_ipadd ,v_notdone ,0);


        end if;


 end loop; --3

 end if;--3


 end loop;


 ---JM: 15Jan2009, #3517 event name propagation----------------
--KM: #4370
 for outer_loop in (
          select event_id, name from event_assoc
          where CHAIN_ID = p_calId and (displacement<>0 or displacement is null) and EVENT_TYPE = 'A'
          and (HIDE_FLAG is null or HIDE_FLAG <>1)
          and offline_flag is null order  by trim(lower(name)) asc )

    loop

      for inner_loop in (
          select event_id, name from event_assoc where CHAIN_ID = p_calId and (displacement<>0 or displacement is null)
          and EVENT_TYPE = 'A' and (HIDE_FLAG is null or HIDE_FLAG <>1)
          and offline_flag=3 order  by trim(lower(name)) asc )
        loop

          if (outer_loop.name = inner_loop.name) then

             update sch_events1 set DESCRIPTION = outer_loop.name where fk_assoc = inner_loop.event_id;

          end if;

        end loop;
    end loop;

 -------------------

-- modification by sonia for all patients---------------

if ( p_chkVal = 1 or  p_chkVal = 2 ) then


    if (p_chkVal = 1 ) then
      v_option1 := 0;
      v_option2 := 1;
    else
      v_option1 := 1;
      v_option2 := 1;
    end if;

    for k in (select fk_per, pk_patprot, PATPROT_START, patprot_schday,patprot_stat
           from er_patprot a where fk_protocol = p_calId  and
        (patprot_stat = v_option1 or  patprot_stat =v_option2 )
    )
        loop

       sp_update_sch_eventseq(p_calId ,k.pk_patprot , p_user , p_ipAdd );

       sp_regenerate_sch_mails(p_calId ,k.pk_patprot , p_user , p_ipAdd ,
       k.patprot_stat ,k.fk_per,'P',v_protocol_study  );
     ---------
    end loop;--for patprot
elsif (p_chkval = 3) then

    for k in ( select fk_per, pk_patprot, PATPROT_START, patprot_schday
           from er_patprot a where fk_protocol = p_calId and patprot_stat = 1
           and exists ( select * from er_patstudystat b
           where b.fk_per = a.fk_per and b.fk_study = a.fk_study and
           fk_codelst_stat = p_statId and patstudystat_date >= to_date(p_date, 'dd/mm/yy')

    ))
        loop

       sp_update_sch_eventseq(p_calId ,k.pk_patprot , p_user , p_ipAdd );

       sp_regenerate_sch_mails(p_calId ,k.pk_patprot , p_user , p_ipAdd ,1 ,
       k.fk_per,'P',v_protocol_study );
     ---------
    end loop;--for patprot

end if;

COMMIT;

------------end of change sonia

end;

 end sp_update_Pat_Schedules_now;


--PROCEDURE sp_updt_unsch_evts1(p_calId in number, p_user in number, p_ipAdd in varchar2, p_evtIds IN ARRAY_STRING) as
PROCEDURE sp_updt_unsch_evts1(p_patId in number, p_patProtId in number, p_calId in number, p_user in number, p_ipAdd in varchar2, p_evtIds IN ARRAY_STRING) as

/*
   Date : 06May2008
   Author: Jnanamay
   Purpose: insert new records in the sch_events1 table for the event_type='U' i.e.; Unscheduled events
   status: undegoing change

   --JM: 21May2008: changed for issue #3531
   JM: 26Nov2009: code modified for START_DATE_TIME, END_DATE_TIME, ACTUAL_SCHDATE for no interval visit #4371
*/

v_event_id number;
v_newevt number;
v_notdone number;

v_fk_per number;
v_patProt number;
v_calassoc         CHAR (1);
v_protocol_study   NUMBER;


v_ptprot_st_dt char(20);
v_seq_num number;
v_days NUMBER;
v_patprot_schday number;
v_mindisp number;

v_cnt number;
v_dupCnt number;
v_mainCnt number;
v_eventId NUMBER;

 j NUMBER;


v_sql              VARCHAR2 (4000);
v_study number;
v_rows number;
v_seq number;
v_error varchar2(4000);
v_patprot_stat number;
v_curr_disc_status number;


BEGIN




      --SELECT SCH_EVENTS1_SEQ1.NEXTVAL INTO v_newevt FROM DUAL;
      SELECT pk_codelst INTO v_notdone FROM SCH_CODELST WHERE codelst_subtyp = 'ev_notdone';

      SELECT event_calassocto, chain_id
          INTO v_calassoc, v_protocol_study
           FROM EVENT_ASSOC
          WHERE event_id = p_calId AND UPPER (event_type) = 'P';

      --select max(event_sequence) into v_seq_num from sch_events1 where session_id = p_calId;


  --for m in (select event_id, fk_visit from event_assoc where chain_id = p_calId and event_type='U' order by trim(lower(name)) asc)
  -- LOOP --success

  v_mainCnt := p_evtIds.COUNT;
  j := 1;

  WHILE j <= v_mainCnt LOOP


  --for j in 1..p_evtIds.COUNT

 --LOOP
  v_eventId := TO_NUMBER(p_evtIds(j));


--#3531 added 'and a.fk_per=p_patId and a.pk_patprot = p_patProtId' in the query
         select fk_per, pk_patprot, to_char(PATPROT_START,PKG_DATEUTIL.F_GET_DATEFORMAT), patprot_schday, patprot_stat
                into v_fk_per, v_patProt, v_ptprot_st_dt, v_patprot_schday, v_patprot_stat
         from er_patprot a where a.pk_patprot = p_patProtId and fk_protocol = p_calId;



 --JM: 30Jun2008: #3514
    if (v_patprot_stat = 0) then
              v_curr_disc_status := 5;
    else
              v_curr_disc_status := 0;

    end if;

       if v_patprot_schday =1 then
            SELECT MIN(displacement) INTO v_mindisp FROM EVENT_ASSOC WHERE chain_id=p_calId AND UPPER(event_type)='A';
            IF (v_mindisp<0) THEN
              v_days:=v_mindisp;
            ELSE
              v_days:=v_patprot_schday;
            END IF;
        else
            v_days := v_patprot_schday;
        end if;



--begin

select SCH_EVENTS1_SEQ1.NEXTVAL into v_seq from dual;

INSERT INTO SCH_EVENTS1  ( EVENT_ID, LOCATION_ID, NOTES, DESCRIPTION, BOOKED_BY,
START_DATE_TIME, END_DATE_TIME, PATIENT_ID, OBJECT_ID, STATUS, TYPE_ID,
SESSION_ID, VISIT_TYPE_ID, FK_ASSOC,  ISCONFIRMED, BOOKEDON, CREATOR , LAST_MODIFIED_BY ,
LAST_MODIFIED_DATE ,CREATED_ON, IP_ADD, FK_PATPROT,ACTUAL_SCHDATE, VISIT,
FK_VISIT,event_notes, SERVICE_SITE_ID, FACILITY_ID, FK_CODELST_COVERTYPE,
fk_study, EVENT_SEQUENCE )
((SELECT lpad (to_char(v_seq), 10, '0'), '173', null,
NAME, '0000000525', --((to_date(v_ptprot_st_dt,PKG_DATEUTIL.F_GET_DATEFORMAT)  + DISPLACEMENT) - 1) ,
case when (select no_interval_flag FROM SCH_PROTOCOL_VISIT
where fk_protocol =  p_calId and pk_protocol_visit = fk_visit) = 1
then (to_date(v_ptprot_st_dt,PKG_DATEUTIL.F_GET_DATEFORMAT) )
else ((to_date(v_ptprot_st_dt,PKG_DATEUTIL.F_GET_DATEFORMAT) + nvl(DISPLACEMENT,0)) - 1)
end ,
(( to_date(v_ptprot_st_dt,PKG_DATEUTIL.F_GET_DATEFORMAT)+ DISPLACEMENT) - 1) + DURATION,
lpad (to_char (v_fk_per), 10, '0'), '0000001476', v_curr_disc_status, '165',
lpad (to_char (p_calId), 10, '0'), FUZZY_PERIOD, EVENT_ID,v_notdone,
to_date(v_ptprot_st_dt,PKG_DATEUTIL.F_GET_DATEFORMAT) ,
p_user, NULL, NULL , SYSDATE ,p_ipAdd, p_patProtId,--v_patprot
--((to_date(v_ptprot_st_dt,PKG_DATEUTIL.F_GET_DATEFORMAT)+ DISPLACEMENT) - 1),
case when (select no_interval_flag  FROM SCH_PROTOCOL_VISIT
where fk_protocol =  p_calId and pk_protocol_visit = fk_visit ) = 1
then null
else ((to_date(v_ptprot_st_dt,PKG_DATEUTIL.F_GET_DATEFORMAT) + nvl(DISPLACEMENT,0)) - 1)
end ,
(select act_num from(select rownum act_num,a.*    from
((SELECT pk_protocol_visit, visit_no, displacement
FROM sch_protocol_visit
where fk_protocol = p_calId order by   displacement asc, visit_no)) a)b    where pk_protocol_visit = fk_visit) visit
, fk_visit ,notes, assoc.SERVICE_SITE_ID, assoc.FACILITY_ID, assoc.FK_CODELST_COVERTYPE,
0, assoc.EVENT_SEQUENCE
FROM EVENT_ASSOC  assoc, (select min(nvl(displacement,0)) mindisp from  event_assoc
where chain_id  =p_calId
and EVENT_TYPE = 'U'
and (displacement <> 0 or displacement = 0) and (DISPLACEMENT >= v_days or displacement=0))d
WHERE CHAIN_ID = p_calId
and event_id = v_eventId
and EVENT_TYPE = 'U'
and (displacement <> 0 or displacement = 0)
and (DISPLACEMENT >=v_days or displacement = 0)
));

--modified by Sonia Abrol, 08/20/08 to
--create additional milestone for this event
sp_add_additionaMS(v_eventId);

commit;

--select count(*) into v_rows from event_assoc where event_Id = v_eventId ;

-- insert into t (c) values ('ROWS IN ASSOC:'|| v_rows);
--insert into t (c) values (v_fk_per||'='|| v_rows||'='||v_protocol_study||'='||v_patProt);
--- commit;

 --exception when others then
 --v_error := sqlerrm;
 --insert into t (c) values ('ERROR:'|| v_error);
 --commit;
 --end;

    --pkg_gensch.sp_addmsg_to_tran (v_fk_per, v_protocol_study, p_user, p_ipAdd, v_patProt );
    --for selected event(s) JM: 20Jun2008, #3540
      pkg_gensch.sp_addmsg_to_tran_per_event (v_eventId, v_fk_per, v_protocol_study, p_user, p_ipAdd, v_patProt);

    --pkg_gensch.sp_setmailstat (v_patprot, p_user, p_ipAdd);
    -- JM: 20Jun2008, #3540
      pkg_gensch.sp_setmailstat_per_event (v_eventId, v_patprot, p_user, p_ipAdd);



            IF (NVL (v_calassoc, 'P') = 'P')
            THEN
               Sp_Copyprotcrf (v_patProt, p_user, p_ipAdd);
            END IF;
             COMMIT;


 j := j+1;
 end loop;

   COMMIT;


end sp_updt_unsch_evts1;


--------------------------------------------------------

FUNCTION givemeevent_count(p_event_id in number)
 RETURN Number AS
/*
  Date : 10Apr2008
  Author: Jnanamay
  Purpose:
            a) to stop duplicate Event entry for a visit
            b) returns the maximum sequence number for the existing Events under a visit

*/

cnt number;
 v_ret number;

 BEGIN


   execute immediate 'select count(*) from event_assoc where event_Id =' || p_event_id
   into cnt;

   v_ret := cnt;



return v_ret;


END givemeevent_count;


----------------------------------------------------------

/* procedure to add additional milestones when anunscheduled event is created
Sonia Abrol
08/20/08
*/

PROCEDURE sp_add_additionaMS(p_event in number)
as
   v_study NUMBER;
   v_res_codeid number;
   v_code_stdcare number;
  v_res_cost_amount number;
  v_eventid number;
  v_ORG_eventid number;
  v_soc_cost_amount number;
  v_amount number := 0;
  v_status Number;
  v_milepaytype number;
  v_protocol number;
  v_visitpk number;
  v_visit_name varchar2(1000);
  v_eventname varchar2(4000);--KM-16Sep09
  v_miledesc varchar2(4000);
  v_creator number;
  v_ipadd varchar2(15);
  v_protocol_name varchar2(1000);

BEGIN
  /* added by Sonia Abrol 08/20/08 - to create an additional milestone for an unscheduled event*/

      select CHAIN_ID, org_id, fk_visit,name,creator,ip_add
      into v_protocol,v_ORG_eventid ,v_visitpk,v_eventname,v_creator,v_ipadd
      from event_assoc where event_id = p_event;


    v_eventid := p_event;

    select visit_name
    into v_visit_name
    from sch_protocol_visit
    where pk_protocol_visit = v_visitpk;

    -- get study, protocol

    select chain_id,name
    into v_study,v_protocol_name
    from event_assoc
    where event_id = v_protocol and event_type = 'P';



    --get milestone status

    begin
        select pk_codelst
        into v_status
        from er_codelst
        where codelst_type = 'milestone_stat' and codelst_subtyp = 'WIP';
      exception when no_data_found then
          v_status := null;
      end;


    begin
        select pk_codelst
        into v_milepaytype
        from er_codelst
        where codelst_type = 'milepaytype' and codelst_subtyp = 'rec';
      exception when no_data_found then

      v_milepaytype := null;
    end;



    --get amount from event cost
    begin
       SELECT pk_codelst INTO v_res_codeid FROM SCH_CODELST WHERE  LOWER(codelst_type)='cost_desc' AND LOWER(codelst_subtyp)='res_cost';

           -- get costs from original ev since this ev is still being created
         select nvl(max(eventcost_value) ,0)
         into v_res_cost_amount
         from sch_eventcost where fk_event=v_ORG_eventid and fk_cost_desc =  v_res_codeid ;

         v_amount := v_res_cost_amount;
    exception when no_data_found then
         v_res_codeid := 0;
         v_res_cost_amount := 0;
         v_amount := 0;
     end;

      if v_res_cost_amount = 0 then

          begin
           SELECT pk_codelst INTO v_code_stdcare FROM SCH_CODELST WHERE LOWER(codelst_type)='cost_desc'  AND LOWER(codelst_subtyp)='stdcare_cost';

               select max(eventcost_value)
             into v_soc_cost_amount
             from sch_eventcost where fk_event=v_ORG_eventid and fk_cost_desc =  v_code_stdcare ;

             v_amount := v_soc_cost_amount;

           exception when no_data_found then
             v_code_stdcare := 0;
             v_soc_cost_amount := 0;
             v_amount := 0;
         end;
    end if;


 -- create additional milestone
 -- KM-16Sep09
 v_eventname := substr(v_eventname,1,500) || '...';
 v_miledesc := 'Unscheduled Event - ' || v_protocol_name  || ', ' || v_visit_name || ',' || v_eventname;
--KM-#D-FIN7
Insert into ER_MILESTONE
   (PK_MILESTONE, FK_STUDY, MILESTONE_TYPE,
    MILESTONE_AMOUNT,  MILESTONE_DELFLAG,
      CREATOR,
    CREATED_ON, IP_ADD,  MILESTONE_STATUS,
    MILESTONE_PAYTYPE, FK_CODELST_MILESTONE_STAT , MILESTONE_DESCRIPTION)
 Values
   (seq_er_milestone.nextval, v_study, 'AM', nvl(v_amount,0),  'N', v_creator, sysdate,v_IPADD,  v_status ,  v_milepaytype,
   (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='WIP'), v_miledesc );



END ;

 PROCEDURE sp_updateCurrentStatus(p_eventId IN number) as

/*
  Date : 28Jul2008
  Author: Jnanamay
  Purpose:
            a) to update the latest event status, end date of the history record to null after histroy row is deleted
  Modified: Manimaran - 26Nov09
  As per the issue 3818, the status should be updated based on the latest date not on latest pk.
  ---modified by Sonia 04/29/10 - rollback fix for 3818
*/


BEGIN



  Update SCH_EVENTSTAT
  set  EVENTSTAT_ENDDT = null
    where fk_event = lpad(to_char(p_eventId),10,'0')
  and PK_EVENTSTAT = (select max(PK_EVENTSTAT) from SCH_EVENTSTAT where fk_event = lpad(to_char(p_eventId),10,'0'));




  update SCH_EVENTS1
  set ISCONFIRMED = (
    select EVENTSTAT from SCH_EVENTSTAT
    WHERE PK_EVENTSTAT = ( select max(PK_EVENTSTAT) from SCH_EVENTSTAT where FK_EVENT = lpad(to_char(p_eventId),10,'0'))
    ) ,
      notes = (select EVENTSTAT_NOTES from   SCH_EVENTSTAT
      WHERE PK_EVENTSTAT = ( select max(PK_EVENTSTAT) from SCH_EVENTSTAT where FK_EVENT = lpad(to_char(p_eventId),10,'0'))
    ),
       event_exeon = (select EVENTSTAT_DT from   SCH_EVENTSTAT
      WHERE PK_EVENTSTAT = ( select max(PK_EVENTSTAT) from SCH_EVENTSTAT where FK_EVENT = lpad(to_char(p_eventId),10,'0'))
    )
   where event_id = lpad(to_char(p_eventId),10,'0');




END sp_updateCurrentStatus;


procedure sp_regenerate_sch_mails(p_calId IN NUMBER,p_patprot in number, p_user IN NUMBER, p_ipAdd IN VARCHAR2,p_patprot_stat in number,p_per in number,
p_calassoc Varchar2,p_protocol_study number )
as
begin
    --- all other mail related functions

         DELETE FROM  SCH_DISPATCHMSG
         WHERE fk_schevent IS NOT NULL AND fk_patprot = p_patprot AND NVL(msg_status,0) = 0;

      --update flag for 'transient emails' so that they don't get sent in clubbed email
         UPDATE SCH_MSGTRAN
         SET msg_status= 2
         WHERE fk_patprot=p_patprot AND fk_schevent IS NOT NULL;

      -------

          IF (NVL (p_calassoc, 'P') = 'P')
            THEN
                pkg_gensch.sp_addmsg_to_tran (p_per, p_protocol_study, p_user, p_ipAdd, p_patprot);
           END IF;



--JM: 30Jun2008: #3497
    if (p_patprot_stat = 0) then
         UPDATE SCH_MSGTRAN
         SET msg_status= 2
         WHERE fk_patprot= p_patprot AND fk_schevent IS NOT NULL;


    end if;
  --change status of mails according to current alert/notify setting
        pkg_gensch.sp_setmailstat (p_patprot , p_user, p_ipAdd);

        IF (NVL (p_calassoc, 'P') = 'P')
        THEN
           Sp_Copyprotcrf (p_patprot, p_user, p_ipAdd);

        END IF;




end;


procedure sp_update_sch_eventseq(p_calId IN NUMBER,p_patprot in number, p_user IN NUMBER, p_ipAdd IN VARCHAR2 )
as
begin
--KM-Modified for Not define visits
 for ul in (select event_id,fk_visit, EVENT_SEQUENCE
    from event_assoc where CHAIN_ID = p_calId and (displacement<>0 or displacement is null)
        order  by trim(lower(name)) asc)

  loop

    UPDATE SCH_EVENTS1
      SET  visit = (SELECT act_num FROM(SELECT ROWNUM act_num,a.*
      FROM( (SELECT pk_protocol_visit, visit_no, displacement  FROM SCH_PROTOCOL_VISIT
      WHERE fk_protocol =  p_calId  ORDER BY   displacement ASC, visit_no)) a)b
      WHERE pk_protocol_visit =ul.fk_visit)
       , EVENT_SEQUENCE = ul.EVENT_SEQUENCE, --JM: 07Jul2008, #3514------------############################-------------
      last_modified_by = p_user,
      ip_add = p_ipAdd
      WHERE fk_patprot = p_patprot
      AND fk_assoc = ul.event_id;


  end loop;

end;

procedure sp_insert_event2SCH(p_calId in number,p_patprot in number,p_eventid in number,p_PATPROT_START in date,
p_per in number,p_dmlby in number,p_ipadd in varchar2,p_notdone in number,p_curr_disc_status in number )
as
begin


     INSERT INTO SCH_EVENTS1  ( EVENT_ID, LOCATION_ID, NOTES, DESCRIPTION, BOOKED_BY,
      START_DATE_TIME, END_DATE_TIME, PATIENT_ID, OBJECT_ID, STATUS, TYPE_ID,
        SESSION_ID, VISIT_TYPE_ID, FK_ASSOC,  ISCONFIRMED, BOOKEDON, CREATOR , LAST_MODIFIED_BY ,
           LAST_MODIFIED_DATE ,CREATED_ON, IP_ADD, FK_PATPROT,ACTUAL_SCHDATE, VISIT,
         FK_VISIT,event_notes  ,fk_study ,EVENT_SEQUENCE)
     SELECT lpad (to_char (SCH_EVENTS1_SEQ1.NEXTVAL), 10, '0'), '173', null,NAME,  '0000000525' ,
       --((p_PATPROT_START + DISPLACEMENT) - 1) ,--KM-#4370
       case when (select no_interval_flag FROM SCH_PROTOCOL_VISIT
               where fk_protocol =  p_calId and pk_protocol_visit = fk_visit) = 1 then (p_PATPROT_START+ (select max(nvl(displacement,0)) from sch_protocol_visit where fk_protocol= p_calId) ) else ((p_PATPROT_START + nvl(DISPLACEMENT,0)) - 1) end ,

       (( p_PATPROT_START + nvl(DISPLACEMENT,0) ) - 1) + DURATION,
                    lpad (to_char (p_per), 10, '0'), '0000001476', p_curr_disc_status, '165',
                    lpad (to_char (p_calId), 10, '0'), FUZZY_PERIOD, EVENT_ID,
                    p_notdone , p_PATPROT_START , p_dmlby , NULL, NULL , SYSDATE ,  p_ipadd , p_patprot,
                    --((p_PATPROT_START + DISPLACEMENT) - 1),--KM
             case when (select no_interval_flag  FROM SCH_PROTOCOL_VISIT
               where fk_protocol =  p_calId and pk_protocol_visit = fk_visit ) = 1 then null else ((p_PATPROT_START + nvl(DISPLACEMENT,0)) - 1) end ,
              (select act_num from(select rownum act_num,a.*
               from(    (SELECT pk_protocol_visit, visit_no, displacement  FROM SCH_PROTOCOL_VISIT
               where fk_protocol =  p_calId    order by   displacement asc, visit_no)) a)b
                 where pk_protocol_visit = fk_visit) visit
                       , fk_visit ,notes,
                        0,assoc.EVENT_SEQUENCE
           FROM EVENT_ASSOC  assoc
         WHERE CHAIN_ID = p_calId and event_id = p_eventid
        and EVENT_TYPE = 'A' and  (displacement <> 0 or displacement is null)
        and (HIDE_FLAG is null or HIDE_FLAG <>1)   ;


end;

end pkg_calendar;
/