/* This readMe is specific to eSample (version 9.0.0 build #637-ES009 HotFix #2) */

=====================================================================================================================================
eSample bugs covered in the build:

1) Bug#13438 - Specimen getting saved with different amount �UNIT� on �specimendetails.jsp�.

Following existing files have been modified:

	1. specimendetails.jsp					(...\deploy\velos.ear\velos.war\jsp)
=====================================================================================================================================