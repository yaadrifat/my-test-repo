UPDATE event_assoc e 
SET e.BUDGET_TEMPLATE = (SELECT b.BUDGET_TEMPLATE
FROM sch_budget b
WHERE e.EVENT_ID = b.budget_calendar 
AND e.chain_id = b.fk_study
AND b.FK_STUDY IS NOT NULL AND NVL(b.BUDGET_DELFLAG,'Z') !='Y')
WHERE EVENT_TYPE='P' ;

commit;
