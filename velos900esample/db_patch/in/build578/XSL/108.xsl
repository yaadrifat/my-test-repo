<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>

<xsl:key name="RecordsByCategory" match="ROW" use="CATEGORY" />
<xsl:key name="RecordsByCategoryLineitem" match="ROW" use="concat(CATEGORY, ' ', LINEITEM_NAME)" />
    
    <xsl:key name="RecordsByCategorySOC" match="ROW" use="concat(CATEGORY, ' ', STANDARD_OF_CARE)" /> 
    
    <xsl:key name="RecordsBySOC" match="ROW" use="STANDARD_OF_CARE" /> 

<xsl:param name="hdrFileName" />
<xsl:param name="ftrFileName" />
<xsl:param name="repTitle"/>
<xsl:param name="repName"/>
<xsl:param name="repBy"/>
<xsl:param name="repDate"/>
<xsl:param name="argsStr"/>
<xsl:param name="wd"/>
<xsl:param name="xd"/>
<xsl:param name="hd"/>
<xsl:param name="cond"/>
<xsl:param name="hdrflag"/>
<xsl:param name="ftrflag"/>
<xsl:param name="budgetTemplate"/>

<xsl:template match="/">
<HTML>
<HEAD><TITLE> <xsl:value-of select="$repName" /> </TITLE>

<link rel="stylesheet" href="./styles/common.css" type="text/css"/>

    </HEAD>
<BODY class="repBody">
<xsl:if test="$cond='T'">
<table width="100%" >
<tr class="reportGreyRow">
<td class="reportPanel"> 
Download the report in: 
<A href='{$wd}' >
Word Format
</A> 
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$xd}' >
Excel Format
</A>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<xsl:text>&#xa0;</xsl:text>
<A href='{$hd}' >
Printer Friendly Format
</A> 
</td>
</tr>
</table>
</xsl:if>
<xsl:apply-templates select="ROWSET"/>
</BODY>
</HTML>
</xsl:template> 

<xsl:template match="ROWSET">
<TABLE WIDTH="100%" >
<xsl:if test="$hdrflag='1'">
<TR>
<TD WIDTH="100%" ALIGN="CENTER">
<img src="{$hdrFileName}"/>
</TD>
</TR>
</xsl:if>
<TR>
<TD class="reportName" WIDTH="100%" ALIGN="CENTER">
<xsl:value-of select="$repName" />
</TD>
</TR>
</TABLE>
<hr class="thickLine" />


<TABLE WIDTH="100%" >
<TR>
<TD class="reportGrouping" ALIGN="Left" width="22%">
Budget Name: </TD><TD class="reportData"><b><xsl:value-of select="//BUDGET_NAME" /></b>
</TD>
</TR>
<TR>
<TD class="reportGrouping" ALIGN="Left" width="22%">
Budget Version: </TD><TD class="reportData" align="left"><b><xsl:value-of select="//BUDGET_VERSION" /></b>
</TD>
</TR>

<TR>
<TD class="reportGrouping" ALIGN="Left" width="22%">
Budget Description: </TD><TD class="reportData"><b><xsl:value-of select="//BUDGET_DESC" /></b>
</TD>
</TR>
<TR>
<TD class="reportGrouping" ALIGN="left" width="22%">
VELLABEL[Std_Study] Number: </TD><TD class="reportData"><b><xsl:value-of select="//STUDY_NUMBER" /></b>
</TD>
</TR>
<TR>
<TD class="reportGrouping" ALIGN="LEFT" width="22%">
VELLABEL[Std_Study] Title: </TD><TD class="reportData"><b><xsl:value-of select="//STUDY_TITLE" /></b>
</TD>
</TR>

<TR>
<TD class="reportGrouping" ALIGN="LEFT" width="22%">
Protocol Calendar: </TD><TD class="reportData"><b><xsl:value-of select="//PROT_CALENDAR" /></b>
</TD>
</TR>

<TR>
<TD class="reportGrouping" ALIGN="left" width="22%">
Organization: </TD><TD class="reportData"><b><xsl:value-of select="//SITE_NAME" /></b>
</TD>
</TR>
</TABLE>
<hr class="thinLine" />

<TABLE WIDTH="100%" BORDER="1">
<TR>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">Category</TH>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">Section</TH>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">Event</TH>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">CPT Code</TH>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">Direct Cost/VELLABEL[Pat_Patient]</TH>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">Total Cost/VELLABEL[Pat_Patient]</TH>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">Cost/All VELLABEL[Pat_Patients]</TH>
<TH class="reportHeading" WIDTH="5%" ALIGN="CENTER">Standard of Care</TH>
<TH class="reportHeading" WIDTH="5%" ALIGN="CENTER">Apply Indirects</TH>
<TH class="reportHeading" WIDTH="5%" ALIGN="CENTER">Apply Discount/Markup</TH>
<xsl:if test="$budgetTemplate='C'">
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">Sponsor Amount</TH>
<TH class="reportHeading" WIDTH="10%" ALIGN="CENTER">Variance</TH>
</xsl:if>
</TR>
<xsl:for-each select="ROW[count(. | key('RecordsByCategory', CATEGORY)[1])=1]">


<TR>
<TD class="reportGrouping" WIDTH="10%" colspan="8">
<xsl:value-of select="CATEGORY" />
</TD>
</TR>

<xsl:variable name="str" select="key('RecordsByCategory', CATEGORY)" />
<xsl:for-each select="key('RecordsByCategory', CATEGORY)">




<xsl:variable name="class">
<xsl:choose>
<xsl:when test="number(position() mod 2)=0" >reportEvenRow</xsl:when> 
<xsl:otherwise>reportOddRow</xsl:otherwise>
</xsl:choose> 
</xsl:variable>

<TR> <xsl:attribute name="class"><xsl:value-of select="$class"/></xsl:attribute>

<TD WIDTH="10%">&#xa0;</TD>
<TD class="reportData" WIDTH="10%" ><xsl:value-of select="BGTSECTION_NAME" /></TD>
<TD class="reportData" WIDTH="10%" ><xsl:value-of select="LINEITEM_NAME" /></TD>
<TD class="reportData" WIDTH="10%" ><xsl:value-of select="CPT_CODE" /></TD>
<TD class="reportData" WIDTH="10%" ALIGN="right"><xsl:value-of select="format-number(LINEITEM_DIRECT_PERPAT,'##,###,###,###,###,##0.00')" /></TD>
<TD class="reportData" WIDTH="10%" ALIGN="right"><xsl:value-of select="format-number(TOTAL_COST_PER_PAT,'##,###,###,###,###,##0.00')" /></TD>
<TD class="reportData" WIDTH="10%" ALIGN="right"><xsl:value-of select="format-number(TOTAL_COST_ALL_PAT,'##,###,###,###,###,##0.00')" /></TD>
<TD class="reportData" WIDTH="5%" ALIGN="center"><xsl:value-of select="STANDARD_OF_CARE" /></TD>
<TD class="reportData" WIDTH="5%" ALIGN="center"><xsl:value-of select="LINE_ITEM_INDIRECTS_FLAG" /></TD>
<TD class="reportData" WIDTH="5%" ALIGN="center"><xsl:value-of select="COST_DISCOUNT_ON_LINE_ITEM" /></TD>
<xsl:if test="$budgetTemplate='C'">
<TD class="reportData" WIDTH="10%" ALIGN="center"><xsl:value-of select="SPONSOR_AMOUNT" /></TD>
<TD class="reportData" WIDTH="10%" ALIGN="center"><xsl:value-of select="L_VARIANCE" /></TD>
</xsl:if>
</TR>

</xsl:for-each>



<tr>
<td>&#xa0;</td>
<td colspan="3" align="left" class="reportGrouping">Sub Total </td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategory', CATEGORY)/LINEITEM_DIRECT_PERPAT),'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategory', CATEGORY)/TOTAL_COST_PER_PAT),'##,###,###,###,###,##0.00')"/></td>

<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategory', CATEGORY)/TOTAL_COST_ALL_PAT),'##,###,###,###,###,##0.00')"/></td>

<td></td>
<td></td>
<td></td>
<xsl:if test="$budgetTemplate='C'">
<td width="10%" align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategory', CATEGORY)/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></td>

<td width="10%" align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategory', CATEGORY)/L_VARIANCE),'##,###,###,###,###,##0.00')"/></td>
</xsl:if>
</tr>

<tr>
<td>&#xa0;</td>
<td colspan="3" align="left" class="reportData">Category Fringe </td>
<td align="right" class="reportData"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategory', CATEGORY)/PER_PATIENT_LINE_FRINGE),'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportData"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategory', CATEGORY)/TOTAL_LINE_FRINGE),'##,###,###,###,###,##0.00')"/></td>
</tr>

<tr>
<td>&#xa0;</td>
<td colspan="3" align="left" class="reportData">Category Discount/Markup </td>
<td align="right" class="reportData"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategory', CATEGORY)/PER_PAT_LINE_ITEM_DISCOUNT),'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportData"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategory', CATEGORY)/TOTAL_COST_DISCOUNT),'##,###,###,###,###,##0.00')"/></td>
</tr>

<tr>
<td>&#xa0;</td>
<td colspan="3" align="left" class="reportData">Category Indirect </td>
<td align="right" class="reportData"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategory', CATEGORY)/PERPAT_INDIRECT),'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportData"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategory', CATEGORY)/TOTAL_COST_INDIRECT),'##,###,###,###,###,##0.00')"/></td>
</tr>
    
    <tr>
        <td>&#xa0;</td>
        
        
        <td colspan="3" align="left" class="reportGrouping">Category Research Total </td>
        <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(CATEGORY,' ','No'))/LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></td>
        <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(CATEGORY,' ','No'))/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></td>
        <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(CATEGORY,' ','No'))/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></td>
        <td></td>
        <td></td>
        <td></td>
	<xsl:if test="$budgetTemplate='C'">
        <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(CATEGORY,' ','No'))/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></td>
        <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(CATEGORY,' ','No'))/L_VARIANCE),'##,###,###,###,###,##0.00')"/></td>
        </xsl:if>
    </tr>
    
    <tr>
        <td>&#xa0;</td>
        <td colspan="3" align="left" class="reportGrouping">Category SOC Total </td>
        <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(CATEGORY,' ','Yes'))/LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></td>
        <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(CATEGORY,' ','Yes'))/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></td>
        <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(CATEGORY,' ','Yes'))/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></td>
        
        <td></td>
        <td></td>
        <td></td>
	<xsl:if test="$budgetTemplate='C'">
        <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(CATEGORY,' ','Yes'))/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></td>
        <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(CATEGORY,' ','Yes'))/L_VARIANCE),'##,###,###,###,###,##0.00')"/></td>
	</xsl:if>
        
    </tr>


    <tr>
        <td>&#xa0;</td>
        
        
        <td colspan="3" align="left" class="reportGrouping">Category Other Cost Total </td>
        <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(CATEGORY,' ','Other'))/LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></td>
        <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(CATEGORY,' ','Other'))/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></td>
        <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(CATEGORY,' ','Other'))/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></td>
        <td></td>
        <td></td>
        <td></td>
	<xsl:if test="$budgetTemplate='C'">
        <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(CATEGORY,' ','Other'))/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></td>
        <td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategorySOC', concat(CATEGORY,' ','Other'))/L_VARIANCE),'##,###,###,###,###,##0.00')"/></td>
        </xsl:if>
    </tr>
    
</xsl:for-each>

<tr>
<td>&#xa0;</td>
<td colspan="3" align="left" class="reportGrouping"><font color="red">Grand Total </font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//LINEITEM_DIRECT_PERPAT) ,'##,###,###,###,###,##0.00')"/></font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
<td></td>
<td></td>
<td></td>
<xsl:if test="$budgetTemplate='C'">
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></font></td>
<td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//L_VARIANCE),'##,###,###,###,###,##0.00')"/></font></td>
</xsl:if>
</tr>
 
    <tr>
        <td>&#xa0;</td>
        <td colspan="3" align="left" class="reportGrouping"><font color="red">Grand Research Total </font></td>
        <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'No')/LINEITEM_DIRECT_PERPAT),'##,###,###,###,###,##0.00')"/></font></td>
        <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'No')/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
        <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'No')/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
        <td></td>
        <td></td>
        <td></td>
	<xsl:if test="$budgetTemplate='C'">
        <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'No')/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></font></td>
        <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'No')/L_VARIANCE),'##,###,###,###,###,##0.00')"/></font></td>
        </xsl:if>
    </tr>
    
    <tr>
        <td>&#xa0;</td>
        <td colspan="3" align="left" class="reportGrouping"><font color="red">Grand SOC Total </font></td>
                <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Yes')/LINEITEM_DIRECT_PERPAT),'##,###,###,###,###,##0.00')"/></font></td>
        <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Yes')/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
        <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Yes')/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
        
        <td></td>
        <td></td>
        <td></td>
	<xsl:if test="$budgetTemplate='C'">
           <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Yes')/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></font></td>
        <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Yes')/L_VARIANCE),'##,###,###,###,###,##0.00')"/></font></td>
	 </xsl:if>
    </tr>
    
    <tr>
        <td>&#xa0;</td>
        <td colspan="3" align="left" class="reportGrouping"><font color="red">Grand Other Cost Total </font></td>
        <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Other')/LINEITEM_DIRECT_PERPAT),'##,###,###,###,###,##0.00')"/></font></td>
        <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Other')/TOTAL_COST_PER_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
        <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Other')/TOTAL_COST_ALL_PAT) ,'##,###,###,###,###,##0.00')"/></font></td>
        <td></td>
        <td></td>
        <td></td>
	<xsl:if test="$budgetTemplate='C'">
        <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Other')/SPONSOR_AMOUNT),'##,###,###,###,###,##0.00')"/></font></td>
        <td align="right" class="reportGrouping"><font color="red"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsBySOC', 'Other')/L_VARIANCE),'##,###,###,###,###,##0.00')"/></font></td>
         </xsl:if>
    </tr>
    

<xsl:for-each select="ROW[count(. | key('RecordsByCategory', CATEGORY)[1])=1]">
<tr>
<td>&#xa0;</td>
<td colspan="3" align="left" class="reportGrouping">&#xa0;&#xa0;&#xa0;&#xa0;Total <xsl:value-of select="CATEGORY"/></td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategory', CATEGORY)/TOTAL_PAT_COST_AFTER),'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(key('RecordsByCategory', CATEGORY)/TOTAL_COST_AFTER),'##,###,###,###,###,##0.00')"/></td>
</tr>
</xsl:for-each>



<xsl:variable name="fringe_flag">
<xsl:value-of select="//FRINGE_FLAG"/>
</xsl:variable>

<tr>
<td class="reportGrouping" colspan="3">Fringe benefit of <xsl:value-of select="format-number(//FRINGE_BENEFIT,'##0.00')" />% 
<xsl:choose>
<xsl:when test="$fringe_flag='1'">Applied</xsl:when> 
<xsl:otherwise>Not Applied</xsl:otherwise>
</xsl:choose> 
 to all Personnel Costs
</td>
<td class="reportGrouping">Total Fringe</td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//PER_PATIENT_LINE_FRINGE),'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_LINE_FRINGE),'##,###,###,###,###,##0.00')"/></td>
</tr>

<xsl:variable name="discount_flag">
<xsl:value-of select="//BUDGET_DISCOUNT_FLAG"/>
</xsl:variable>


<xsl:variable name="discMText">
<xsl:choose>
<xsl:when test="$discount_flag='Discount'">Discount</xsl:when> 
<xsl:when test="$discount_flag='Markup'">Markup</xsl:when> 
<xsl:otherwise>Discount/Markup</xsl:otherwise>
</xsl:choose> 
</xsl:variable>

<tr>
<td class="reportGrouping" colspan="3">Cost <xsl:value-of select="$discMText" /> of <xsl:value-of select="format-number(//BUDGET_DISCOUNT,'##0.00')" />% 
<xsl:choose>
<xsl:when test="$discount_flag='Discount'">Applied</xsl:when> 
<xsl:when test="$discount_flag='Markup'">Applied</xsl:when> 
<xsl:otherwise>Not Applied</xsl:otherwise>
</xsl:choose> 
 to all selected line items
</td>
<td class="reportGrouping">Total Cost <xsl:value-of select="$discMText" /></td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//PER_PAT_LINE_ITEM_DISCOUNT),'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_COST_DISCOUNT),'##,###,###,###,###,##0.00')"/></td>
</tr>

<xsl:variable name="indirect_flag">
<xsl:value-of select="//BUDGET_INDIRECT_FLAG"/>
</xsl:variable>

<tr>
<td class="reportGrouping" colspan="3">Indirects of <xsl:value-of select="format-number(//INDIRECTS,'##0.00')" />% 
<xsl:choose>
<xsl:when test="$indirect_flag='Y'">Applied</xsl:when> 
<xsl:otherwise>Not Applied</xsl:otherwise>
</xsl:choose> 
 to selected Total Cost/VELLABEL[Pat_Patient]
</td>
<td class="reportGrouping">Total Indirects</td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//PERPAT_INDIRECT),'##,###,###,###,###,##0.00')"/></td>
<td align="right" class="reportGrouping"><xsl:value-of select="//BUDGET_CURRENCY"/><xsl:value-of select="format-number(sum(//TOTAL_COST_INDIRECT),'##,###,###,###,###,##0.00')"/></td>
</tr>

</TABLE>

<hr class="thickLine" />
<TABLE WIDTH="100%" >
<TR>
<TD class="reportFooter" WIDTH="50%" ALIGN="LEFT">
Report By:<xsl:value-of select="$repBy" />
</TD>
<TD class="reportFooter" WIDTH="50%" ALIGN="RIGHT">
Date:<xsl:value-of select="$repDate" />
</TD>
</TR>
</TABLE>
<xsl:if test="$ftrflag='1'">
<TABLE>
<TR>
<TD WIDTH="100%" ALIGN="CENTER">
<img src="{$ftrFileName}"/>
</TD>
</TR>
</TABLE>
</xsl:if>
</xsl:template> 
</xsl:stylesheet>