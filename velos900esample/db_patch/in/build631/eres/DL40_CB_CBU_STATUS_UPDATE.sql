--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='HE' AND 
	CODE_TYPE='Response';
  if (v_record_exists = 1) then
      UPDATE CB_CBU_STATUS SET CBU_STATUS_DESC='HE Requested' where CBU_STATUS_CODE='HE' AND CODE_TYPE='Response';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='CT' AND 
	CODE_TYPE='Response';
  if (v_record_exists = 1) then
      UPDATE CB_CBU_STATUS SET CBU_STATUS_DESC='CT Requested' where CBU_STATUS_CODE='CT' AND CODE_TYPE='Response';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE='alert_resol' AND 
	CODELST_SUBTYP='alert_33';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='No Results Received. Lab has not reported results' where CODELST_TYPE='alert_resol' AND 
	CODELST_SUBTYP='alert_33';
	commit;
  end if;
end;
/
--END--
