create or replace
PROCEDURE SP_RESULTS_RECEIVED(V_ORDER_ID NUMBER) AS
  V_SERVICE_CNT NUMBER;
  V_RESULTS_REC_CNT NUMBER;
  V_RES_REC_DATE DATE;
BEGIN
  SELECT COUNT(*) INTO V_SERVICE_CNT FROM ER_ORDER_SERVICES WHERE FK_ORDER_ID=V_ORDER_ID AND CANCELLED_DATE IS NULL AND CANCELLED_FLAG IS NULL AND RESULTS_REC_DATE IS NULL AND RESULTS_REC_FLAG IS NULL;
  SELECT COUNT(*) INTO V_RESULTS_REC_CNT FROM ER_ORDER_SERVICES WHERE FK_ORDER_ID=V_ORDER_ID AND RESULTS_REC_DATE IS NOT NULL AND RESULTS_REC_FLAG IS NOT NULL;
  SELECT MAX(RESULTS_REC_DATE) INTO V_RES_REC_DATE FROM ER_ORDER_SERVICES WHERE FK_ORDER_ID=V_ORDER_ID;
  IF V_SERVICE_CNT=0 AND V_RESULTS_REC_CNT>0 THEN
    UPDATE ER_ORDER SET RESULT_REC_DATE=V_RES_REC_DATE,ORDER_STATUS=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_status' and CODELST_SUBTYP='HLA_RES_REC'),ORDER_STATUS_DATE=SYSDATE WHERE PK_ORDER=V_ORDER_ID;
    COMMIT;
  END IF;
   EXCEPTION
 WHEN NO_DATA_FOUND THEN
  DBMS_OUTPUT.PUT_LINE('Data Not Found in sp_results received procedure');
END;
/



create or replace
procedure SP_RESOLVE_ORDERS(V_ORDERID NUMBER,V_ORDERTYPE VARCHAR2)
AS
V_CORDID NUMBER;
V_PATIENTID NUMBER;
V_ORDER_ID NUMBER;
V_ORDER_TYPE VARCHAR2(250 CHAR);
V_PK_ORDER_TYE NUMBER;
BEGIN
SELECT ORDHDR.ORDER_ENTITYID,PAT.FK_PERSON_ID INTO V_CORDID,V_PATIENTID FROM ER_ORDER ORD,ER_ORDER_HEADER ORDHDR,CB_RECEIPANT_INFO PAT WHERE ORD.FK_ORDER_HEADER=ORDHDR.PK_ORDER_HEADER AND PAT.FK_ORDER_ID=ORD.PK_ORDER AND ORD.PK_ORDER=V_ORDERID AND ORDHDR.ORDER_ENTITYTYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='CBU');
dbms_output.put_line('CORD ID :::'||V_CORDID);
dbms_output.put_line('PATIENT ID :::'||V_PATIENTID);
FOR ORDERS IN(SELECT ORD.PK_ORDER ORDER_ID,ORD.ORDER_TYPE ORDER_TYPE FROM ER_ORDER ORD,ER_ORDER_HEADER ORDHDR,CB_RECEIPANT_INFO PAT WHERE ORD.FK_ORDER_HEADER=ORDHDR.PK_ORDER_HEADER AND PAT.FK_ORDER_ID=ORD.PK_ORDER AND ORDHDR.ORDER_ENTITYID=V_CORDID AND ORDHDR.ORDER_ENTITYTYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='CBU') AND PAT.FK_PERSON_ID=V_PATIENTID AND ORD.PK_ORDER<>V_ORDERID AND (ORD.FK_ORDER_RESOL_BY_TC IS NULL OR ORD.FK_ORDER_RESOL_BY_CBB IS NULL))
LOOP
dbms_output.put_line('ORDER ID :::'||ORDERS.ORDER_ID);
V_ORDER_ID:=ORDERS.ORDER_ID;
dbms_output.put_line('ORDER TYPE :::'||ORDERS.ORDER_TYPE);
V_ORDER_TYPE:=ORDERS.ORDER_TYPE;
IF V_ORDERTYPE='CTORDER' THEN
SELECT PK_CODELST INTO V_PK_ORDER_TYE FROM ER_CODELST WHERE CODELST_TYPE='order_type' AND CODELST_SUBTYP='CT';
  IF V_ORDER_TYPE=V_PK_ORDER_TYE THEN
    UPDATE ER_ORDER SET FK_ORDER_RESOL_BY_CBB=(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='RCT'),ORDER_RESOL_DATE=SYSDATE WHERE PK_ORDER=V_ORDER_ID;
  ELSE
    UPDATE ER_ORDER SET FK_ORDER_RESOL_BY_CBB=(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='CT'),ORDER_RESOL_DATE=SYSDATE WHERE PK_ORDER=V_ORDER_ID;
  END IF;
END IF;
IF V_ORDERTYPE='HEORDER' THEN
SELECT PK_CODELST INTO V_PK_ORDER_TYE FROM ER_CODELST WHERE CODELST_TYPE='order_type' AND CODELST_SUBTYP='HE';
  IF V_ORDER_TYPE=V_PK_ORDER_TYE THEN
    UPDATE ER_ORDER SET FK_ORDER_RESOL_BY_CBB=(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='RHE'),ORDER_RESOL_DATE=SYSDATE WHERE PK_ORDER=V_ORDER_ID;
  ELSE
    UPDATE ER_ORDER SET FK_ORDER_RESOL_BY_CBB=(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='HE'),ORDER_RESOL_DATE=SYSDATE WHERE PK_ORDER=V_ORDER_ID;
  END IF;
END IF;
IF V_ORDERTYPE='ORORDER' THEN
  UPDATE ER_ORDER SET FK_ORDER_RESOL_BY_CBB=(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='OR'),ORDER_RESOL_DATE=SYSDATE WHERE PK_ORDER=V_ORDER_ID;
END IF;
END LOOP;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
  DBMS_OUTPUT.PUT_LINE('Data Not Found in sp_resolve_order');
END;
/

create or replace
PROCEDURE SP_ALERTS(entity_id number,entity_type number) AS
TYPE CurTyp IS REF CURSOR;  -- define weak REF CURSOR type
cur_cv   CurTyp;  -- declare cursor variable
v_alertId number;
v_alert_title varchar2(4000);
v_alert_desc varchar2(4000);
v_alert_wording varchar2(4000);
v_alert_precondition varchar2(4000) :='';
v_col_name varchar2(4000);
v_alert_precond_cnt number :=0;
v_query varchar2(4000);
sql_fieldval varchar2(4000);
var_instance_cnt number;
v_userId number;
v_cordregid varchar2(255);
V_ORDERTYPE VARCHAR2(255);
v_sampleatlab varchar2(1);
V_ALERTNAME VARCHAR2(255);
v_resol number:=0;
v_resoldesc VARCHAR2(50 CHAR):='N/A';
v_orderId number;
v_schshipdate varchar2(50);
v_temp number;
V_ALERT_ID_TEMP NUMBER;
v_entity_order number;
v_entity_cord number;
BEGIN
FOR CUR_ALERTS IN(select pk_codelst,CODELST_SUBTYP,codelst_desc,codelst_custom_col1,codelst_custom_col from er_codelst where codelst_type in('alert','alert_resol') order by pk_codelst)
LOOP
  v_alertId := cur_alerts.pk_codelst;
  V_ALERTNAME:=cur_alerts.codelst_subtyp;
	v_alert_title := cur_alerts.codelst_desc;
	v_alert_desc := cur_alerts.codelst_custom_col1;
	v_alert_wording :=cur_alerts.codelst_custom_col;
  v_alert_precondition := getalertcondition(v_alertid,'PRE');
if v_alert_precondition<>' ' then
    dbms_output.put_line('alert Id :::'||v_alertid);
    dbms_output.put_line('alert pre condition is::'||v_alert_precondition);

    if v_alertname in('alert_01','alert_02','alert_03','alert_04','alert_15','alert_27','alert_20','alert_16','alert_29','alert_30','alert_31','alert_32','alert_33','alert_34','alert_35','alert_36','alert_37','alert_38','alert_39') then
      v_query := 'select count(*) cnt from er_order where '||v_alert_precondition;
    elsif v_alertname in('alert_05','alert_06') then
      v_query := 'select count(*) cnt from er_order,er_order_header where er_order.fk_order_header=er_order_header.pk_order_header and '||v_alert_precondition;
    elsif v_alertname in('alert_07','alert_08','alert_09','alert_10','alert_11') then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_shipment where er_order.fk_order_header=er_order_header.pk_order_header and cb_shipment.fk_order_id=er_order.pk_order and '||v_alert_precondition;
    elsif v_alertname='alert_19' then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_cord where er_order.fk_order_header=er_order_header.pk_order_header and er_order_header.order_entityid=cb_cord.pk_cord and '||v_alert_precondition;
    elsif v_alertname IN('alert_23','alert_24','alert_25','alert_26') then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_cord,er_attachments,cb_upload_info where er_order.fk_order_header=er_order_header.pk_order_header and er_order_header.order_entityid=cb_cord.pk_cord and er_attachments.entity_id=cb_cord.pk_cord and er_attachments.entity_type=(select pk_codelst from er_codelst where codelst_type=''entity_type'' and codelst_subtyp=''CBU'') and er_attachments.pk_attachment=cb_upload_info.fk_attachmentid and '||v_alert_precondition;
    end if;
    v_query:=replace(v_query,'#keycolumn',entity_id);
     dbms_output.put_line('^^^^^^^^^^^'||v_query);
    open cur_cv for v_query;
     LOOP
          FETCH cur_cv INTO sql_fieldval ;
          EXIT WHEN cur_cv%NOTFOUND;
           dbms_output.put_line('display result  ' || sql_fieldval );
           if sql_fieldval>0 then
           select pk_codelst into v_entity_order from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER';
           select pk_codelst into v_entity_cord from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU';
           if entity_type=v_entity_order then
              select nvl(assigned_to,0),nvl(crd.cord_registry_id,'N/A'),nvl(ord.order_sample_at_lab,'-'),case when ord.fk_order_resol_by_tc is null and ord.fk_order_resol_by_cbb is null then 0 when ord.fk_order_resol_by_tc is null and ord.fk_order_resol_by_cbb is not null then ord.fk_order_resol_by_cbb when ord.fk_order_resol_by_tc is not null and ord.fk_order_resol_by_cbb is null then ord.fk_order_resol_by_tc else 0 end into v_userid,v_cordregid,v_sampleatlab,v_resol from er_order ord left outer join er_order_header ordhdr on(ord.fk_order_header=ordhdr.pk_order_header) left outer join cb_cord crd on(ordhdr.order_entityid=crd.pk_cord) where pk_order=entity_id;
              dbms_output.put_line('v_resol............'||v_resol);
              select ord.pk_order,nvl(to_char(ship.sch_shipment_date,'Mon DD, YYYY'),'N/A') into v_temp,v_schshipdate from er_order ord left outer join (select fk_order_id,sch_shipment_date from cb_shipment where fk_order_id=entity_id and fk_shipment_type=(select pk_codelst from er_codelst where codelst_type='shipment_type' and codelst_subtyp='CBU Shipment')) ship on(ship.fk_order_id=ord.pk_order) where ord.pk_order=entity_id;
            end if;
            if entity_type=v_entity_cord then
              select nvl(cord_registry_id,'N/A') into v_cordregid from cb_cord where pk_cord=entity_id;
            end if;
           if v_resol<>0 then
           dbms_output.put_line('before getting resolution desc...............');
            select cbu_status_desc into v_resoldesc from cb_cbu_status where pk_cbu_status=v_resol;
            dbms_output.put_line('after getting resolution desc...............'||v_resoldesc);
           end if;
           
           v_alert_wording:=replace(v_alert_wording,'<CBU Registry ID>',v_cordregid);
           v_alert_wording:=replace(v_alert_wording,'<Request Type>',v_ordertype);
           v_alert_wording:=replace(v_alert_wording,'<Resolution Code>',v_resoldesc);
           v_alert_wording:=replace(v_alert_wording,'<Shipment Scheduled Date>',v_schshipdate);
           if v_sampleatlab='Y' then
           v_alert_wording:=replace(v_alert_wording,v_ordertype,'CT Lab');
           elsif v_sampleatlab='N' then
           v_alert_wording:=replace(v_alert_wording,v_ordertype,'CT Ship');
           end if;
           select pk_codelst into v_alert_id_temp from er_codelst where codelst_type='alert' and codelst_subtyp='alert_20';
           dbms_output.put_line('v_alert_id_temp::::::::'||v_alert_id_temp);
           select count(*) into var_instance_cnt from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=entity_id and alrt_inst.entity_type=entity_type and alrt_inst.fk_userid=v_userid and alrt_inst.alert_type=(select pk_codelst from er_codelst where codelst_type='alert_type' and codelst_subtyp='notify');
                dbms_output.put_line('var_instance_cnt:::::'||var_instance_cnt);
            if var_instance_cnt=0 or v_alert_id_temp=v_alertid then
             
               insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,ENTITY_ID,ENTITY_TYPE,FK_USERID,ALERT_TYPE,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,entity_id,entity_type,v_userid,(select pk_codelst from er_codelst where codelst_type='alert_type' and codelst_subtyp='notify'),v_alert_wording);
             
            end if;
            for usrs in(select altr.FK_USER,altr.EMAIL_FLAG email_flag,altr.NOTIFY_FLAG notify_flag,altr.PROMPT_FLAG prompt_flag,addr.add_email mail from cb_user_alerts altr,er_user usr,er_add addr where altr.fk_user=usr.pk_user and usr.fk_peradd=addr.pk_add and fk_codelst_alert=v_alertid)
            loop
              dbms_output.put_line('users that has assigned alert is:::'||usrs.fk_user);
              if usrs.email_flag=1 then
              select count(*) into var_instance_cnt from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=entity_id and alrt_inst.entity_type=entity_type and alrt_inst.entity_type=(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER') and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=(select pk_codelst from er_codelst where codelst_type='alert_type' and codelst_subtyp='mail');
              if var_instance_cnt=0 or v_alert_id_temp=v_alertid then
                insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,ENTITY_ID,ENTITY_TYPE,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,entity_id,(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER'),usrs.fk_user,(select pk_codelst from er_codelst where codelst_type='alert_type' and codelst_subtyp='mail'),usrs.mail,v_alert_wording);
              dbms_output.put_line('Email alerts implemented');
                insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT) values(SEQ_ER_NOTIFICATION.nextval,'admin@aithent.com',usrs.mail,v_alert_title,v_alert_wording,0);
              end if;
              end if;
              end loop;
              commit;
           end if;
      END LOOP;
    dbms_output.put_line('alert wording is::'||v_alert_wording);
end if;
END LOOP;
if entity_type=v_entity_order then
  update er_order set data_modified_flag='' where pk_order=entity_id;
end if;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
  dbms_output.put_line ('A SELECT...INTO did not return any row.');
end;
/





create or replace
PROCEDURE        "CB_ALERTS_MAINTENANCE_PROC" AS
  V_ENTITY_ID NUMBER;
  V_ENTITY_TYPE NUMBER;
  V_ENTITY_CORD NUMBER;
  V_ENTITY_ORDER NUMBER;
  v_ord_cnt number;
BEGIN
FOR ALERTS IN(SELECT ENTITY_ID,ENTITY_TYPE FROM CB_ALERTS_MAINTENANCE)
LOOP
V_ENTITY_TYPE:=ALERTS.ENTITY_TYPE;
V_ENTITY_ID:=ALERTS.ENTITY_ID;
dbms_output.put_line('V_ENTITY_TYPE:::'||V_ENTITY_TYPE);
dbms_output.put_line('V_ENTITY_ID:::'||V_ENTITY_ID);
SELECT PK_CODELST INTO V_ENTITY_ORDER FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='ORDER';
SELECT PK_CODELST INTO V_ENTITY_CORD FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='CBU';
IF V_ENTITY_TYPE=V_ENTITY_ORDER THEN
dbms_output.put_line('order id:::'||V_ENTITY_ID);
  SP_ALERTS(V_ENTITY_ID,v_entity_order);
  delete from cb_alerts_maintenance where entity_id=V_ENTITY_ID;
END IF;
IF V_ENTITY_TYPE=V_ENTITY_CORD THEN
dbms_output.put_line('cord id:::'||V_ENTITY_ID);
SELECT count(PK_ORDER) into v_ord_cnt FROM ER_ORDER ORD,ER_ORDER_HEADER ORDHDR WHERE ORD.FK_ORDER_HEADER=ORDHDR.PK_ORDER_HEADER AND ORDHDR.ORDER_ENTITYID=V_ENTITY_ID AND ORDHDR.ORDER_ENTITYTYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='CBU');
if v_ord_cnt<>0 then
  FOR ORDERS IN(SELECT PK_ORDER FROM ER_ORDER ORD,ER_ORDER_HEADER ORDHDR WHERE ORD.FK_ORDER_HEADER=ORDHDR.PK_ORDER_HEADER AND ORDHDR.ORDER_ENTITYID=V_ENTITY_ID AND ORDHDR.ORDER_ENTITYTYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='CBU'))
  LOOP
  dbms_output.put_line('ORDERS.PK_ORDER:::'||ORDERS.PK_ORDER);
    SP_ALERTS(ORDERS.PK_ORDER,v_entity_order);
  END LOOP;
else
  dbms_output.put_line('in cord id:::');
   SP_ALERTS(V_ENTITY_ID,v_entity_cord);
end if;
  delete from cb_alerts_maintenance where entity_id=V_ENTITY_ID; 
END IF;
end loop;
EXCEPTION
WHEN NO_DATA_FOUND THEN
  DBMS_OUTPUT.PUT_LINE('NO DATA FOUND');
END CB_ALERTS_MAINTENANCE_PROC;
/

create or replace
PROCEDURE SP_WORKFLOW_TEMP IS
OUTSTR VARCHAR2(100 BYTE);
var_order_id varchar2(10 CHAR);
var_order_type varchar2(10 CHAR);
v_entity_order number;
BEGIN
select pk_codelst into v_entity_order from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER';
for ORDERS in (SELECT ORDER_ID,ORDER_TYPE FROM ER_ORDER_TEMP)
loop
  var_order_id :=orders.order_id;
  var_order_type :=orders.order_type;
  SP_RESULTS_RECEIVED(VAR_ORDER_ID);
  sp_resolve_orders(var_order_id,var_order_type);
  sp_alerts(var_order_id,v_entity_order);
 sp_workflow(var_order_id,var_order_type,1,outstr);
 END LOOP;
 delete from er_order_temp;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
  DBMS_OUTPUT.PUT_LINE('Data Not Found');
END SP_WORKFLOW_TEMP;
/








create or replace
TRIGGER CB_UPLOAD_INFO_ALERTS AFTER INSERT OR UPDATE ON CB_UPLOAD_INFO
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
V_ENTITY_ID NUMBER;
V_ENTITY_TYPE NUMBER;
BEGIN
SELECT ENTITY_ID,ENTITY_TYPE INTO V_ENTITY_ID,V_ENTITY_TYPE FROM ER_ATTACHMENTS WHERE PK_ATTACHMENT=:NEW.FK_ATTACHMENTID;
INSERT INTO CB_ALERTS_MAINTENANCE(ENTITY_ID,ENTITY_TYPE) VALUES(V_ENTITY_ID,V_ENTITY_TYPE);
EXCEPTION 
  WHEN NO_DATA_FOUND THEN
  DBMS_OUTPUT.PUT_LINE('NO DATA FOUND IN CB_UPLOAD_INFO_ALERTS TRIGGER');
end;
/
-----------------------------------------------------------------------------------------

