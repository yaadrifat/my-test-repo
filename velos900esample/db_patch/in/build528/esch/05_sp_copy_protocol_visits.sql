create or replace PROCEDURE Sp_Copy_Protocol_Visits(
   P_OLD_PROTOCOL_ID  IN       NUMBER,
   P_NEW_PROTOCOL_ID	IN	NUMBER,
   p_user  IN NUMBER,
   p_ip IN VARCHAR2,
   p_ret OUT NUMBER)
  IS
v_VISIT_NAME VARCHAR2(50);
v_VISIT_NO NUMBER;
v_DESCRIPTION VARCHAR2(200);
v_DISPLACEMENT NUMBER;
v_NUM_MONTHS NUMBER;
v_NUM_WEEKS NUMBER;
v_NUM_DAYS NUMBER;
v_INSERT_AFTER NUMBER;
v_INSERT_AFTER_INTERVAL NUMBER;
v_INSERT_AFTER_INTERVAL_UNIT CHAR(1);
v_new_visit_id NUMBER;
v_count NUMBER;
V_OLD_PK_PROTOCOL_VISIT NUMBER;
v_oldid NUMBER;
v_newid NUMBER;
v_hide_flag number; --JM: 30Jun2008, #3543
v_nointerval_flag number;
v_win_before_number number; ---KM:29Apr10,#4871
v_win_before_unit char(1);
v_win_after_number number;
v_win_after_unit char(1);


v_rec_count NUMBER := 0;
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_COPYPROTOCOL', pLEVEL  => Plog.LDEBUG);

 v_rec_mapping rec_mapping   := rec_mapping(NULL,NULL);
v_table_keys tab_mapping := tab_mapping(); --index-by table typ


CURSOR C1 IS
SELECT VISIT_NO,
   VISIT_NAME, DESCRIPTION, DISPLACEMENT,
   NUM_MONTHS, NUM_WEEKS, NUM_DAYS,
   INSERT_AFTER, INSERT_AFTER_INTERVAL,INSERT_AFTER_INTERVAL_UNIT,PK_PROTOCOL_VISIT, hide_flag, no_interval_flag, 
   win_before_number, win_before_unit, win_after_number, win_after_unit
FROM SCH_PROTOCOL_VISIT
   WHERE fk_protocol = p_old_protocol_id
ORDER BY VISIT_NO;


/******************************************************************************
   NAME:       sp_copy_prot_visits
   PURPOSE:    To copy protocol visits from one calendar to another


   PARAMETERS:
   INPUT:
   OUTPUT:
   RETURNED VALUE:
   CALLED BY:
   CALLS:
   EXAMPLE USE:     sp_copy_prot_visits;
   ASSUMPTIONS:
   LIMITATIONS:
   ALGORITHM:
   NOTES:

      Object Name:     sp_copy_prot_visit
      Date/Time:       9/29/2004 11:43:47 PM
      Username:         Sam Varadarajan
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        9/30/2004    Sam V         1. Created this procedure.
   1.1       9/9/2005        Sonia Abrol    Added logic to update insert_after with new ids for the copied visits

******************************************************************************/
BEGIN
   	v_count := 0;

	OPEN  c1;

	LOOP

	FETCH c1
	INTO v_VISIT_NO,
	   v_VISIT_NAME, v_DESCRIPTION, v_DISPLACEMENT,
	   v_NUM_MONTHS, v_NUM_WEEKS, v_NUM_DAYS,
	   v_INSERT_AFTER, v_INSERT_AFTER_INTERVAL,v_INSERT_AFTER_INTERVAL_UNIT,V_OLD_PK_PROTOCOL_VISIT, v_hide_flag, v_nointerval_flag,
     v_win_before_number, v_win_before_unit, v_win_after_number, v_win_after_unit ;
	-- SV, 09/30, copy the visits, from old protocol to new.
	IF (c1%NOTFOUND) THEN
	   EXIT;
	END IF;

	SELECT SCH_PROT_VISIT_SEQ.NEXTVAL
		INTO v_new_visit_id
	FROM dual;

	IF (v_INSERT_AFTER = NULL) THEN
	   v_INSERT_AFTER := 0;
	END IF;

	IF (v_INSERT_AFTER_INTERVAL = NULL) THEN
	   v_INSERT_AFTER_INTERVAL := 0;
	END IF;

	IF (v_INSERT_AFTER_INTERVAL_UNIT = NULL) THEN
	   v_INSERT_AFTER_INTERVAL := 'D';
	END IF;

/* JM: 07Feb2008, #3209: removed LAST_MODIFIED_BY col. from the insert query below  -----------------------------*/
/* KM: interval_flag added for no interval visit */
	INSERT INTO SCH_PROTOCOL_VISIT (
	   PK_PROTOCOL_VISIT, FK_PROTOCOL, VISIT_NO,
	   VISIT_NAME, DESCRIPTION, DISPLACEMENT,
	   NUM_MONTHS, NUM_WEEKS, NUM_DAYS,
	   INSERT_AFTER, INSERT_AFTER_INTERVAL, INSERT_AFTER_INTERVAL_UNIT, CREATOR,
	   CREATED_ON,IP_ADD, hide_flag, no_interval_flag, win_before_number, win_before_unit, win_after_number, win_after_unit )
	VALUES ( v_new_visit_id, p_new_protocol_id, v_VISIT_NO,
	   v_VISIT_NAME, v_DESCRIPTION, v_DISPLACEMENT,
	   v_NUM_MONTHS, v_NUM_WEEKS, v_NUM_DAYS,
	   v_INSERT_AFTER, v_INSERT_AFTER_INTERVAL, v_INSERT_AFTER_INTERVAL_UNIT, p_user,
	   SYSDATE,
	   p_IP, v_hide_flag, v_nointerval_flag, v_win_before_number, v_win_before_unit, v_win_after_number, v_win_after_unit );

       v_count := v_count + 1;


        v_rec_count := v_rec_count + 1;
   		v_rec_mapping.old_id := V_OLD_PK_PROTOCOL_VISIT ;
		v_rec_mapping.new_id := v_new_visit_id;
		v_table_keys.EXTEND;
		v_table_keys(v_rec_count ) := v_rec_mapping;
		v_rec_count  := v_table_keys.COUNT();
		-- Plog.DEBUG(pCTX,'mapping one old:' || V_OLD_PK_PROTOCOL_VISIT || ': new ' || v_new_visit_id);


	END LOOP;

	CLOSE c1;

	-- return value
	p_ret := v_count;

  -- update the values of insert_after


FOR i IN 1..v_table_keys.COUNT
	LOOP
          v_oldid := (v_table_keys(i).old_id);
		  v_newid := (v_table_keys(i).new_id);

		  UPDATE SCH_PROTOCOL_VISIT
		  SET insert_after = v_newid WHERE fk_protocol = P_NEW_PROTOCOL_ID AND insert_after = v_oldid;

	END LOOP;


END Sp_Copy_Protocol_Visits;


/**********************************
--TEST
set serveroutput on
declare

begin
sp_copy_protocol_visits(52, 40027, 40028, 1523, '66.237.42.110', v_ret) ;
end ;
**************************************/
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,71,5,'05_sp_copy_protocol_visits.sql',sysdate,'8.9.0 Build#528');

commit;
