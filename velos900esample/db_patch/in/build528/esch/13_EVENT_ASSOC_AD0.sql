CREATE OR REPLACE TRIGGER "ESCH"."EVENT_ASSOC_AD0" AFTER DELETE ON EVENT_ASSOC
FOR EACH ROW
declare
  raid number(10);
  deleted_data clob; --KM

begin
	select seq_audit.nextval into raid from dual;

	audit_trail.record_transaction
		(raid, 'EVENT_ASSOC', :old.rid, 'D');

	deleted_data :=
		to_char(:old.event_id) || '|' ||
		to_char(:old.chain_id) || '|' ||
		:old.event_type || '|' ||
		:old.name || '|' ||
		:old.notes || '|' ||
		to_char(:old.cost) || '|' ||
		:old.cost_description || '|' ||
		to_char(:old.duration) || '|' ||
		to_char(:old.user_id) || '|' ||
		:old.linked_uri || '|' ||
		:old.fuzzy_period || '|' ||
		:old.msg_to || '|' ||
		:old.status || '|' ||
		:old.description || '|' ||
		to_char(:old.displacement) || '|' ||
		to_char(:old.org_id) || '|' ||
		:old.event_msg || '|' ||
		:old.event_res || '|' ||
		to_char(:old.created_on) || '|' ||
		to_char(:old.event_flag) || '|' ||
		to_char(:old.pat_daysbefore) || '|' ||
		to_char(:old.pat_daysafter) || '|' ||
		to_char(:old.usr_daysbefore) || '|' ||
		to_char(:old.usr_daysafter) || '|' ||
		:old.pat_msgbefore || '|' ||
		:old.pat_msgafter || '|' ||
		:old.usr_msgbefore || '|' ||
		:old.usr_msgafter || '|' ||
		to_char(:old.status_dt) || '|' ||
		to_char(:old.status_changeby) || '|' ||
		to_char(:old.rid) || '|' ||
		to_char(:old.creator) || '|' ||
		to_char(:old.last_modified_by) || '|' ||
		to_char(:old.last_modified_date) || '|' ||
		:old.ip_add  || '|' ||
		:old.orig_study ||'|'||
		:old.orig_cal ||'|'||
		:old.orig_event ||'|'||
		:old.duration_unit || '|' ||
		to_char(:old.fk_visit) || '|' ||
		:old.event_cptcode || '|' ||
		:old.event_fuzzyafter || '|' ||
		:old.event_durationafter || '|' ||
		:old.event_durationbefore || '|' ||
		:old.fk_catlib || '|' ||
		:old.event_calassocto || '|' ||
		to_char(:old.event_calschdate) || '|' ||
		--KM-28Mar08
		:old.event_category || '|' ||
		to_char(:old.EVENT_LIBRARY_TYPE) || '|' ||
		to_char(:old.EVENT_LINE_CATEGORY) || '|' ||--KM: 21Aug09
		to_char(:old.SERVICE_SITE_ID) ||'|'|| to_char(:old.FACILITY_ID) ||'|'|| to_char(:old.FK_CODELST_COVERTYPE); 

	insert into audit_delete
	(raid, row_data) values (raid, SUBSTR(deleted_data,1,4000));
end; 
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,71,13,'13_EVENT_ASSOC_AD0.sql',sysdate,'8.9.0 Build#528');

commit;
