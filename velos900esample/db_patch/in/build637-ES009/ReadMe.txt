/* This readMe is specific to eSample (version 9.0.0 build #637-ES009) */

=====================================================================================================================================
eSample bugs covered in the build:

1) Bug#12838 - UI issue on "Specimens" and "Storage Search"
2) Bug#12839 - INV 21675: Status of the storage do not changed ,when storage have specimen
3) Bug#12817 - UI issue's on different browser

Following existing files have been modified:

	1. specimendetails.jsp					(...\deploy\velos.ear\velos.war\jsp)
	2. storageunitdetails.jsp				(...\deploy\velos.ear\velos.war\jsp)
	3. addstoragekit.jsp					(...\deploy\velos.ear\velos.war\jsp)
	4. editstoragekit.jsp					(...\deploy\velos.ear\velos.war\jsp)
	5. labbrowser.jsp						(...\deploy\velos.ear\velos.war\jsp)
	6. storageadminbrowser.jsp				(...\deploy\velos.ear\velos.war\jsp)
	7. ManageCart.class						(...\deploy\velos.ear\velos-main.jar\com\velos\eres\web\specimen)
=====================================================================================================================================