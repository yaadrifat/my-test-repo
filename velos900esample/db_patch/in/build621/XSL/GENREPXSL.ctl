LOAD DATA
INFILE *
INTO TABLE er_repxsl
APPEND
FIELDS TERMINATED BY ','
(PK_REPXSL,
 FK_REPORT,
 FK_ACCOUNT,
 REPXSL_NAME,
 xsl_file filler char,
"REPXSL" LOBFILE (xsl_file) TERMINATED BY EOF NULLIF XSL_FILE = 'NONE',
 xsl_file2 filler char,
"REPXSL_XSL" LOBFILE (xsl_file2) TERMINATED BY EOF NULLIF XSL_FILE2 = 'NONE'
)
BEGINDATA
115,115,0,Adverse Event Summary,115.xsl,115.xsl
168,168,0,CBU Comprehensive Report,168.xsl,168.xsl