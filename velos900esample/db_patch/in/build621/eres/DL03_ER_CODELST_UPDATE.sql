set define off;

update ER_CODELST set CODELST_SUBTYP='post_proc_thaw' where CODELST_TYPE='timing_of_test' and codelst_desc='Post-Cryopreservation/Thaw on';

--STARTS UPDATE RECORD INTO INTO ER_CODELST TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from ER_CODELST
    where  CODELST_TYPE='minimum_decl' AND CODELST_SUBTYP = 'meets';

  if (v_column_exists = 1) then
      execute immediate 'UPDATE ER_CODELST SET CODELST_DESC = ''Meets minimum requirements (or appropriate approval has been obtained)'' where  CODELST_TYPE = ''minimum_decl'' AND CODELST_SUBTYP = ''meets''';
  end if;
end;
/
--END--

commit;