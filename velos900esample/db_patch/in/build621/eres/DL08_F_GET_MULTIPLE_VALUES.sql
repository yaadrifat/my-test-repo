create or replace
FUNCTION F_GET_MULTIPLE_VALUES (ENTITYID NUMBER, ENTITYTYPE VARCHAR2, CODETYPE VARCHAR2)
RETURN VARCHAR2
AS
V_MULTI_VALUES VARCHAR2(1000);
Begin
	FOR i IN  (Select f_codelst_desc(multi.FK_CODELST) as multiple_values from cb_multiple_values multi
  where multi.entity_id = ENTITYID and
  Multi.Entity_Type = ENTITYTYPE And
  Multi.Fk_Codelst_Type = CODETYPE
  )
	LOOP
	    V_MULTI_VALUES := V_MULTI_VALUES || i.multiple_values || ', ';
	END LOOP;
    RETURN SUBSTR(V_MULTI_VALUES,1,LENGTH(V_MULTI_VALUES)-2);
END;
/