<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>
<title> <%=LC.L_Add_MultiStorage%><%--Add Multiple Storage*****--%></title>

<%@ page import ="java.util.*, java.io.*, org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.*"  %>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<script Language="javascript">

//KM-23Dec2008
function validateme(formObj){

	counter =0;
	for(i=0;i<10;i++){
	   id = formObj.id[i].value;
	   name = formObj.name[i].value;
	   strType = formObj.dStore[i].value;

	   if ((name == '' && strType == ''  )){
        counter++;
			if (counter == 10){
					alert("<%=MC.M_EtrAtleastOne_Resp%>");/*alert("Please enter at least one Response");*****/
	    			return false;
			}
		}

		if ((name !='' || strType !='')) {

			if(name==''){alert("<%=MC.M_Etr_StorName%>");/*alert("Please enter Storage Name");*****/ return false;}
			if(strType==''){alert("<%=MC.M_Etr_StorType%>");/*alert("Please enter Storage Type");*****/ return false;}
		}

		if ((name !='' && strType !='')) {
			if (formObj.cells1[i].value.indexOf('+') != -1 || isNaN(formObj.cells1[i].value) == true || formObj.cells1[i].value < 1 || formObj.cells1[i].value =='') {
				alert("<%=MC.M_Valid_PositiveNum%>");/*alert("Please enter a Valid Positive Number");*****/
				formObj.cells1[i].focus();
				return false;
			}

			if(formObj.cells2[i].value.indexOf('+') != -1 || isNaN(formObj.cells2[i].value) == true || formObj.cells2[i].value < 1  || formObj.cells2[i].value =='' ) {
				alert("<%=MC.M_Valid_PositiveNum%>");/*alert("Please enter a Valid Positive Number");*****/
				formObj.cells2[i].focus();
				return false;
			}
		}


    }

	for(count=0;count<10;count++)
	{
		for(count1=count+1;count1<10;count1++){	if((fnTrimSpaces(formObj.id[count].value.toLowerCase())==fnTrimSpaces(formObj.id[count1].value.toLowerCase()))&&(formObj.id[count].value!="")){
			alert("<%=MC.M_StorId_CntSimilar%>");/*alert("Two Storage IDs cannot be similar");*****/
			formObj.id[count1].focus();
			return false;
			}
		}
	}

	if(!(validate_col('e-Signature',formObj.eSign))) return false

	if (isNaN(formObj.eSign.value) == true){
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formObj.eSign.focus();
		return false;
	}

}

</script>
<jsp:useBean id="sess" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="include.jsp" flush="true"/>
<BODY>
<%
HttpSession tSession = request.getSession(true);
if (sess.isValidSession(tSession)){
 String dStore = "";
 CodeDao cd1 = new CodeDao();

 //JM: 28Sep2009: #4302
	//cd1.getCodeValues("store_type");
	  cd1.getCodeValuesSaveKit("store_type");

 dStore = cd1.toPullDown("dStore");

 //JM: 04Aug2009: #INVP2.15-(part-1)
 CodeDao cdTmplt = new CodeDao();
 cdTmplt.getCodeValues("templatetype");
 String dTemplateType = cdTmplt.toPullDown("templatetype");

 CodeDao cdStorUnitClass = new CodeDao();
 cdStorUnitClass.getCodeValues("stor_unit_class");
 String dStorageUnitClass = cdStorUnitClass.toPullDown("storageunitclass");

%>

<p class = "sectionHeadings" ><%=LC.L_Add_MultiStorage%><%--Add Multiple Storage*****--%></P>
<br>

<form name="storage" id="multstoragefrm" method=post action="updatemultiplestorage.jsp" onsubmit="if (validateme(document.storage)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<table class="basetbl outline midalign"><tr><td>
<table width="100%" >
	<tr>
	<td colspan="3" align = "left" ><p class="defComments"><%=MC.M_LveStrIdBlank_ToAutoId%><%--Please leave 'Storage ID' blank to auto generate the ID*****--%>
		</p></td>
	</tr>
	<tr>
	<th> <%=LC.L_Id_Upper%><%--ID*****--%> </th>
	<th> <%=LC.L_Storage_Name%><%--Storage Name*****--%> <FONT class="Mandatory">* </FONT></th>
	<th> <%=LC.L_Storage_Type%><%--Storage Type*****--%>	<FONT class="Mandatory">* </FONT> </th>
	<th> <%=MC.M_Dim1_OfCell%><%--Dimension 1 # of Cells*****--%> </th>
	<th> <%=LC.L_Naming%><%--Naming*****--%> </th>
	<th> <%=LC.L_Positioning%><%--Positioning*****--%> </th>
	<th> <%=MC.M_Dim2_OfCell%><%--Dimension 2 # of Cells*****--%> </th>
	<th> <%=LC.L_Naming%><%--Naming*****--%> </th>
	<th > <%=LC.L_Positioning%><%--Positioning*****--%> </th>
	<th> <%=LC.L_Template_Type%><%--Template Type*****--%> </th>
	<th> <%=LC.L_Alternate_Id%><%--Alternate ID*****--%> </th>
	<th > <%=LC.L_Storage_UnitClass%><%--Storage Unit Class*****--%> </th>
	</tr>

<br>
<%
	int numrows = 10;

	for (int i=0; i <  numrows; i++){ %>

	<tr width = "100%">

	<td width="15%">
	<input type="text" name="id" maxlength=100 size=15>
	</td>

	<td width="15%">
	<input type="text" name="name" maxlength=250 size=15>
	</td>

	<td width="15%">
	<%=dStore%>
	</td>



	<td width="20%"><Input type = "text" name="cells1" size=15 align = right value="1">
	</td>
    <td>
	<SELECT NAME=namingConv id="nameConvA" style="width: 100px">
	<OPTION value ="azz" selected><%=LC.L_AGTZz%><%--A>ZZ*****--%></OPTION>
	<OPTION value = "zza"><%=LC.L_ZzGTA%><%--ZZ>A*****--%></OPTION>
	</SELECT>
	</td>

	<td>
    <SELECT NAME=positioning id="posA" style="width: 120px">
	<OPTION value ="LR" selected><%=LC.L_Left_ToRight%><%--Left to Right*****--%></OPTION>
	<OPTION value ="RL"><%=LC.L_Right_ToLeft%><%--Right to Left*****--%></OPTION>

	</SELECT>
    </td>


	<td width="20%"><Input type = "text" name="cells2" size=15 align = right value="1">
	</td>
	<td>
	<SELECT NAME=namingConvn id="nameConvB" style="width: 100px">
	<OPTION value ="1n" selected><%=LC.L_1To_N%><%--1 to n*****--%></OPTION>
	<OPTION value = "n1"><%=LC.L_N_To1%><%--n to 1*****--%></OPTION>

	</SELECT>
    </td>
	<td>
	<SELECT NAME=positioningn id="posB" style="width: 120px">
	<OPTION value ="BF" selected><%=LC.L_Back_ToFront%><%--Back to Front*****--%></OPTION>
	<OPTION value ="FB" ><%=LC.L_Front_ToBack%><%--Front to Back*****--%></OPTION>
	</SELECT>
	</td>

	<td>
	<%=dTemplateType%>
	</td>

	<td width="20%"><Input type = "text" name="alternateId" size=15 align = right value="">
	</td>
	<td>
	<%=dStorageUnitClass%>
	</td>

<%} %>
</table>

<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="multstoragefrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

</td></tr></table>

</form>

<%

}
//end of session
else{
%>

<jsp:include page="timeout.html" flush='true'></jsp:include>
<%}%>

	<div class = "myHomebottomPanel">
	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</BODY>

</html>