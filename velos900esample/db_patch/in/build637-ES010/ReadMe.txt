/* This readMe is specific to eSample (version 9.0.0 build #637-ES010) */

=====================================================================================================================================
Following eSample requirements are covered in the build:
	1. INV-22458	Icons and Child Specimen indicator in specimen tabs
	2. INV-22459	More Specimen Details
	3. INV-22460	Relationship between fields Anatomic Side and Tissue Side


Following existing files have been modified:
	1. updatespecimendetails.jsp				(...\deploy\velos.ear\velos.war\jsp)
	2. inventorytabs.jsp						(...\deploy\velos.ear\velos.war\jsp)
	3. specimendetails.jsp						(...\deploy\velos.ear\velos.war\jsp)
	4. MC.class									(...\deploy\velos.ear\velos-main.jar\com\velos\eres\service\util)
	5. messageBundle.properties					(...\eResearch_jboss510\conf)
	6. Org.png									(...\deploy\velos.ear\velos.war\jsp\images)
	
Property file changes (Do not replace the files):
	Copy the KEY(s) given in the file "changes_messageBundle.properties" to the existing file "messageBundle.properties" under the path:  ...\eResearch_jboss510\conf
=====================================================================================================================================