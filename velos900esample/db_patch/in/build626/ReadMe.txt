/* This readMe is specific to Velos eResearch version 9.0 build #626 */

=====================================================================================================================================
Garuda :

	   
=====================================================================================================================================
eResearch:

For Bug#9306, An excel file(Field Label Redmond Checklist For INF-22323.xls)is released along with this build.

=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1. datagrid.js
2. reportcentral.jsp
3. labelBundle.properties
4. LC.java
5. MC.java
6. messageBundle.properties
7. reportsinstudy.jsp
=====================================================================================================================================
