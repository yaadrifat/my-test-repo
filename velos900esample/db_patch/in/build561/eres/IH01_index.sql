DECLARE 
  index_count INTEGER; 
BEGIN 
  SELECT COUNT(*) into index_count FROM user_indexes WHERE
    index_name = 'IDX_PATFORMS_SCH_EVENTS1' and upper(table_owner) = 'ERES';
  if (index_count = 0) then
    execute immediate 'CREATE INDEX ERES.IDX_PATFORMS_SCH_EVENTS1 ON ERES.ER_PATFORMS
      (FK_SCH_EVENTS1)
       LOGGING
       NOPARALLEL';
  end if;
END;
/
