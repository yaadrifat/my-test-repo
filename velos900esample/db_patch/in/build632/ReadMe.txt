/* This readMe is specific to Velos eResearch version 9.0 build #632 */

=====================================================================================================================================
Garuda :
Following scripts are specific to NMDP.
DL05_ER_CODELST_INSERT.SQL
DL06_ER_CODELST_UPDATE.SQL 

DL11_UPDATE_CORD_STATUS_JOB.SQL - Becuase of some job scheduling one Alert message will be shown on the console repeatedly. The message is
'Alert condition message executed....'

	
	     
=====================================================================================================================================
eResearch:

#For Bug#10023 and 10027:-
An excel file(INF-22311-MouseoverText (QA).xls) sheet4 mentioning all the changes required for Image and mouseover text is released
 along with this build. Please find the "Developer Comments" column in excel sheet to check the status of issues mentioned.

#For INF-22324 New Icons and mouseover implementation:-
An excel file(INF-22324-moreIcons.xlsx) mentioning the places required for Image and mouseover text change is released along with 
this build. Two zip files named :
1)INF-22324-screenshots.zip and
2)Latest 14 Icons.zip       are also released along with this build.
Please refer to "INF-22324-screenshots.zip" file to know the places where changes are made and "Latest 14 Icons.zip" file for new icons. 
Please find the "Developer Comment" column in "INF-22324-moreIcons.xlsx" excel file sheet to check the status of issues mentioned.

=====================================================================================================================================
eResearch Localization:


=====================================================================================================================================
