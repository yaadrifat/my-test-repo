begin
 DBMS_SCHEDULER.drop_job (job_name => 'Update_cord_status_job');
 end;
 /

begin
DBMS_SCHEDULER.create_job (
    job_name        => 'UPDATE_CORD_STATUS_JOB',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'DECLARE V_ENTITY_TYPE NUMBER;begin SELECT PK_CODELST INTO V_ENTITY_TYPE FROM ER_CODELST WHERE CODELST_TYPE=''entity_type'' and CODELST_SUBTYP=''ORDER'';SP_UPDATE_CORD_STATUS;FOR ORDERS IN(SELECT PK_ORDER FROM ER_ORDER ORD WHERE ORD.ORDER_STATUS NOT IN(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''order_status'' and CODELST_SUBTYP IN(''close_ordr'',''CANCELLED''))) LOOP SP_ALERTS(ORDERS.PK_ORDER,V_ENTITY_TYPE);END LOOP;end;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'freq=daily;byhour=00;byminute=00;bysecond=1',
    end_date        => NULL,
    enabled         => TRUE,
    comments        => 'Job to update cord status.');
end;
/

begin
dbms_scheduler.run_job('UPDATE_CORD_STATUS_JOB');
end;
/

