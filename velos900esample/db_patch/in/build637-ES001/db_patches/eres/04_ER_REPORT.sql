set define off;

-- UPDATE into ER_REPORT

Update ER_REPORT set REP_SQL_CLOB = 'SELECT a.fk_account, a.fk_study, spec_id, spec_alternate_id,
case when a.fk_study is null then ''[No Study Specified]''
else (SELECT study_number FROM er_study WHERE pk_study = a.fk_study) end study_number,
case when FK_USER_PATH is null then ''[Not specified]''
else (select usr_firstName || '' '' || usr_lastname from er_user where pk_user = FK_USER_PATH) end pathologist,
case when FK_USER_SURG is null then ''[Not specified]''
else (select usr_firstName || '' '' || usr_lastname from er_user where pk_user = FK_USER_SURG) end Surgeon,
(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) spec_type,
TO_CHAR(spec_collection_date,PKG_DATEUTIL.F_GET_DATEFORMAT) spec_collection_date,
(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_anatomic_site) spec_anatomic_site,
spec_original_quantity,
(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) spec_quantity_units
FROM er_specimen a, er_specimen_status b
WHERE (('':specTyp''=''NonStudy'' AND a.fk_study is null AND a.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND a.fk_study in (:studyId)))
AND (a.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
and usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
AND b.fk_specimen = a.pk_specimen
AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
AND b.PK_SPECIMEN_STATUS = (SELECT MAX(c.PK_SPECIMEN_STATUS) FROM er_specimen_status c
WHERE c.fk_specimen = a.pk_specimen AND c.SS_DATE = (SELECT MAX(d.SS_DATE) FROM er_specimen_status d WHERE d.fk_specimen = a.pk_specimen ))
and a.FK_SITE IN (:orgId)
ORDER BY spec_collection_date' where pk_report = 143;

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,181,4,'04_ER_REPORT.sql',sysdate,'9.0.0 B#637-ES001');

COMMIT;
