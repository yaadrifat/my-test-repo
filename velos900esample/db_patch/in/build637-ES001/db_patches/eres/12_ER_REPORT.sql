set define off;

-- UPDATE into ER_REPORT

UPDATE er_report SET REP_SQL_CLOB ='SELECT pkg_inventory.f_get_specimen_trail(:sessUserId,''NonStudy'', TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),'':orgId'') from dual' WHERE pk_report = 154;

COMMIT;

INSERT INTO er_repfiltermap (pk_repfiltermap,fk_repfilter,repfiltermap_repcat,repfiltermap_seq,repfiltermap_mandatory,repfiltermap_dispdiv) VALUES (127,2, 'inventory',5,'Y','orgDIV,orgdataDIV');

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,181,12,'12_ER_REPORT.sql',sysdate,'9.0.0 B#637-ES001');

COMMIT;
