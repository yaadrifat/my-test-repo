--INSERTING DATA INTO CB_FORM_QUESTIONS TABLE FOR MRQ FORM --

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='donate_cb_prev_ind'),null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='refused_bld_dnr_ind'),null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='refused_bld_dnr_ind_desc'),null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tkn_medications_ind'),null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bovine_insulin_since_1980_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='evr_tkn_pit_gh_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rabies_vacc_pst_year_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='shots_vacc_pst_8wk_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='shots_vacc_pst_8wk_desc'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cntc_smallpox_vaccine_12wk_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_4mth_illness_symptom_ind'), null,sysdate,null,null,null,null,null);


Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='symptom_desc'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cncr_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bld_prblm_5yr_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='wnv_preg_diag_pos_test_ind'),null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='viral_hepatitis_age_11_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='para_chagas_babesiosis_leish_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cjd_diag_neuro_unk_ind'),null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bld_rlt_diag_rsk_cjd'),null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='dura_mater_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_xeno_tx_med_procedure_ind'),null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='liv_cntc_xeno_tx_med_proc_ind'),null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='malaria_antimalr_drug_3yr_ind'),null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='outside_us_or_canada_3yr_ind'),null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='outside_canada_us_desc'),null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='blood_transf_b4coll_12m_ind'),null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tx_stemcell_12m_oth_self_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tattoo_ear_skin_pierce_12m_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tattoo_shared_nonsterile_12m_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acc_ns_stk_cut_bld_12m_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='had_treat_syph_12m_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='given_mn_dr_for_sex_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_tk_mn_dr_pmt_sex_12m_5yr_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_cntc_liv_hep_b_c_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_dr_in_pst_5yr_12m_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_man_oth_pst_5yr_12m_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_clot_fact_bld_12m_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_hiv_aids_12m_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='prison_jail_contin_72hrs_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_5yr_tk_mn_dr_sex_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_5yr_iv_drug_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aids_hiv_screen_test_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_night_sweat'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_blue_prpl_spot_kaposi_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_weight_loss'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_persist_diarrhea'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_cough_short_breath'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_temp_over_ten_days'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_mouth_sores_wht_spt'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_1m_lump_nk_apit_grn_mult_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inf_dur_preg_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='htlv_incl_screen_test_para_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_contag_person_understand_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cjd_live_travel_country_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_uk_ge_3m_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_trnsfsn_uk_france_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_europe_ge_5y_cjd_cnt_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='us_military_dep_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mil_base_europe_1_ge_6m_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mil_base_europe_2_ge_6m_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_1_2_o_performed_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='born_live_cam_afr_nig_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='trav_ctry_rec_bld_or_med_ind'), null,sysdate,null,null,null,null,null);

Insert into CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG='1'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_born_live_in_ctry_ind'), null,sysdate,null,null,null,null,null);

-- END --
commit;
