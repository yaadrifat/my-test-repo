CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTS1_AD0" AFTER DELETE ON SCH_EVENTS1
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data clob;--KM
begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_EVENTS1', :old.rid, 'D', :old.last_modified_by);

  deleted_data :=
   :old.event_id || '|' ||
   :old.object_id || '|' ||
   :old.visit_type_id || '|' ||
   :old.child_service_id || '|' ||
   :old.session_id || '|' ||
   :old.occurence_id || '|' ||
   :old.location_id || '|' ||
   :old.notes || '|' ||
   :old.type_id || '|' ||
   to_char(:old.roles) || '|' ||
   :old.svc_type_id || '|' ||
   to_char(:old.status) || '|' ||
   :old.service_id || '|' ||
   :old.booked_by || '|' ||
   to_char(:old.isconfirmed) || '|' ||
   :old.description || '|' ||
   to_char(:old.attended) || '|' ||
   to_char(:old.start_date_time) || '|' ||
   to_char(:old.end_date_time) || '|' ||
   to_char(:old.bookedon) || '|' ||
   to_char(:old.iswaitlisted) || '|' ||
   :old.object_name || '|' ||
   :old.location_name || '|' ||
   :old.session_name || '|' ||
   :old.user_name || '|' ||
   :old.patient_id || '|' ||
   :old.svc_type_name || '|' ||
   to_char(:old.event_num) || '|' ||
   :old.visit_type_name || '|' ||
   :old.obj_location_id || '|' ||
   to_char(:old.fk_assoc) || '|' ||
   to_char(:old.event_exeon) || '|' ||
   to_char(:old.event_exeby) || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   to_char(:old.created_on) || '|' ||
   :old.ip_add || '|' ||
   to_char(:old.fk_patprot) || '|' ||
   to_char(:old.actual_schdate) || '|' ||
   to_char(:old.adverse_count) || '|' ||
   to_char(:old.visit) || '|' ||
   to_char(:old.alnot_sent) || '|' ||
   to_char(:old.fk_visit) || '|' ||
   to_char(:old.fk_study) || '|' ||
   :old.event_notes || '|' ||
   to_char(:old.SERVICE_SITE_ID) ||'|'|| 
   to_char(:old.FACILITY_ID) ||'|'|| 
   to_char(:old.FK_CODELST_COVERTYPE) ||'|'|| 
   :old.reason_for_coveragechange; 

	insert into audit_delete
	(raid, row_data) values (raid, SUBSTR(deleted_data,1,4000));
end; 
/