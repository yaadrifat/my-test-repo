<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="java.sql.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.StringTokenizer"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<script>
function CloseSelfWin(url)
{
	window.opener.location.href=url;
	window.close();
}
</script>
<%
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
	{	
		String TableStart="<table border='1' width='100%' height='100%' style='border-collapse: collapse; border-color: #800000' bordercolordark='#800000'>";
		TableStart=TableStart+"<tr><td height='100%' width='100%' valign='top'><b><font face='Verdana' size='2' color='#800000'>"+LC.L_Specimen_Id+":</font></b><p><b><font face='Verdana' size='2' color='#0000FF'>";
		String TableEnd = "</font></b></td></tr></table>";
		String pk_storage_id = request.getParameter("strg_pk");
		
		StringTokenizer st = new StringTokenizer(pk_storage_id, ",");
		while (st.hasMoreElements())
		{
			String specIDval = String.valueOf(st.nextElement());
			TableStart = TableStart + "<a href='#' onclick=CloseSelfWin('specimendetails.jsp?mode=M&pkId="+specIDval+"'); style='text-decoration: none'>"+specIDval+ "</a><br>";
		}
		out.print(TableStart+TableEnd);
	}
	else
	{
		%><jsp:include page="timeout.html" flush="true"/><%
	} //end of else body for session time out
%>

