set define off;

insert into ER_LKPLIB(PK_LKPLIB,
LKPTYPE_NAME,
LKPTYPE_VERSION,
LKPTYPE_DESC,
LKPTYPE_TYPE,
FK_ACCOUNT,
FK_SITE) 
Values
((select MAX(PK_LKPLIB)+1 FROM ER_LKPLIB),'dynReports',Null,'CB HLA INFO','dyn_a',Null,Null);



Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Cord_Registry_Id','CBU Registry ID','varchar',50,'REP_CB_HLA_VIEW','Cord_Registry_Id' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Cord_Local_Cbu_Id','CBU Local ID','varchar',50,'REP_CB_HLA_VIEW','Cord_Local_Cbu_Id' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Registry_Maternal_Id','Maternal Registry ID','varchar',50,'REP_CB_HLA_VIEW','Registry_Maternal_Id' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Maternal_Local_Id','Maternal Local ID','varchar',50,'REP_CB_HLA_VIEW','Maternal_Local_Id' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Created_On','Created On','date',null,'REP_CB_HLA_VIEW','Created_On' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Creator','Created By','varchar',61,'REP_CB_HLA_VIEW','Creator' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Last_Modified_By','Last Modified By','number',10,'REP_CB_HLA_VIEW','Last_Modified_By' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Last_Modified_Date','Last Modified Date','DATE',Null,'REP_CB_HLA_VIEW','Last_Modified_Date' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Status_Reasons','Status Reasons','varchar',4000,'REP_CB_HLA_VIEW','Status_Reasons' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Entity_Type','Entity Type','number',20,'REP_CB_HLA_VIEW','Entity_Type' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Fk_Hla_Antigeneid1','Antigen Id 1','number',10,'REP_CB_HLA_VIEW','Fk_Hla_Antigeneid1' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Fk_Hla_Antigeneid2','Antigen Id 2','number',10,'REP_CB_HLA_VIEW','Fk_Hla_Antigeneid2' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Fk_Hla_Code_Id','HLA Code ID','varchar',4000,'REP_CB_HLA_VIEW','Fk_Hla_Code_Id' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Fk_Hla_Method_Id','HLA Method ID','varchar',4000,'REP_CB_HLA_VIEW','Fk_Hla_Method_Id' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Fk_Source','Source','varchar',4000,'REP_CB_HLA_VIEW','Fk_Source' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Fk_Spec_Type','Specimen Type','varchar',4000,'REP_CB_HLA_VIEW','Fk_Spec_Type' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Hla_Value_Type1','HLA Value Type 1','varchar',20,'REP_CB_HLA_VIEW','Hla_Value_Type1' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Hla_Value_Type2','HLA Value Type 2','varchar',20,'REP_CB_HLA_VIEW','Hla_Value_Type2' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Hla_Received_Date','HLA Received Date','date',null,'REP_CB_HLA_VIEW','Hla_Received_Date' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Hla_Typing_Date','HLA Typing Date','date',Null,'REP_CB_HLA_VIEW','Hla_Typing_Date' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'Site_Name','Site Name','varchar',50,'REP_CB_HLA_VIEW','Site_Name' );


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((Select Max(Pk_Lkpcol)+1 From Er_Lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'FK_ACCOUNT','Account ID','number',null,'REP_CB_HLA_VIEW','lkp_pk');



Insert Into Er_Lkpview 
(Pk_Lkpview,Lkpview_Name,Lkpview_Desc,Fk_Lkplib,Lkpview_Filter)
Values((Select Max(Pk_Lkpview)+1 From Er_Lkpview),'CB HLA INFO',Null,(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='CB HLA INFO'),'fk_account=[:ACCID]');

 


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Cord_Registry_Id' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,1,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Cord_Local_Cbu_Id' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,2,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Registry_Maternal_Id' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,3,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Maternal_Local_Id' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,4,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Created_On' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,5,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Creator' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,6,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Last_Modified_By' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,7,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Last_Modified_Date' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,8,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Status_Reasons' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,9,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Entity_Type' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,10,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Fk_Hla_Antigeneid1' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,11,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Fk_Hla_Antigeneid2' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,12,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Fk_Hla_Code_Id' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,13,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Fk_Hla_Method_Id' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,14,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Fk_Source' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,15,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Fk_Spec_Type' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,16,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Hla_Value_Type1' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,17,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Hla_Value_Type2' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,18,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Hla_Received_Date' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,19,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Hla_Typing_Date' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,20,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='Site_Name' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,21,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_ACCOUNT' And Lkpcol_Table='REP_CB_HLA_VIEW'),
Null,22,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CB HLA INFO'),'10%','N' );


commit;

----------------------CB HLA INFO-------------------------------------------



---------------CBB PROCESSING PROCEDURE-----------------------


  
  insert into ER_LKPLIB(PK_LKPLIB,
LKPTYPE_NAME,
LKPTYPE_VERSION,
LKPTYPE_DESC,
LKPTYPE_TYPE,
FK_ACCOUNT,
FK_SITE) values((select MAX(PK_LKPLIB)+1 FROM ER_LKPLIB),'dynReports',null,'CBU CBB PROCESSING PROCEDURE','dyn_a',null,null);


insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'CORD_LOCAL_CBU_ID','CBU LOCAL ID','VARCHAR',50,'REP_CBU_CBB_PROCESSING_PROC','CORD_LOCAL_CBU_ID'); 
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'CORD_REGISTRY_ID','CBU REGISTRY ID','VARCHAR',50,'REP_CBU_CBB_PROCESSING_PROC','CORD_REGISTRY_ID'); 

 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'REGISTRY_MATERNAL_ID','MATERNAL REGISTRY ID','VARCHAR',50,'REP_CBU_CBB_PROCESSING_PROC','REGISTRY_MATERNAL_ID'); 
   
   insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'MATERNAL_LOCAL_ID','MATERNAL LOCAL ID','VARCHAR',50,'REP_CBU_CBB_PROCESSING_PROC','MATERNAL_LOCAL_ID');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_ACCOUNT','ACCOUNT ID ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','lkp_pk');
 

 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'PROC_NAME','Procedure Name','VARCHAR',50,'REP_CBU_CBB_PROCESSING_PROC','PROC_NAME');
  

   insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'PROC_START_DATE','Procedure Start Date','DATE',NULL,'REP_CBU_CBB_PROCESSING_PROC','PROC_START_DATE');
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'PRODUCT_CODE','Product Code','VARCHAR',50,'REP_CBU_CBB_PROCESSING_PROC','PRODUCT_CODE');
  

insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'PROC_TERMI_DATE','Procedure Termination Date','DATE',NULL,'REP_CBU_CBB_PROCESSING_PROC','PROC_TERMI_DATE');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values(SEQ_ER_LKPCOL.nextval,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_PROC_METH_ID','Processing','VARCHAR',50,'REP_CBU_CBB_PROCESSING_PROC','FK_PROC_METH_ID');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_IF_AUTOMATED','Automated process to modify CBU ','NUMBER',20,'REP_CBU_CBB_PROCESSING_PROC','FK_IF_AUTOMATED');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTHER_PROCESSING','Please Specify ','VARCHAR',1020,'REP_CBU_CBB_PROCESSING_PROC','OTHER_PROCESSING');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_PRODUCT_MODIFICATION','Product Modification ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','FK_PRODUCT_MODIFICATION');
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTHER_PROD_MODI','Please Specify ','VARCHAR',1020,'REP_CBU_CBB_PROCESSING_PROC','OTHER_PROD_MODI');
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_STOR_METHOD','Storage Method','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','FK_STOR_METHOD');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_FREEZ_MANUFAC','Freezer Manufacture','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','FK_FREEZ_MANUFAC');
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTHER_FREEZ_MANUFAC','Please Specify','VARCHAR',1020,'REP_CBU_CBB_PROCESSING_PROC','OTHER_FREEZ_MANUFAC');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_FROZEN_IN','Frozen vials,tubes,bags or others','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','FK_FROZEN_IN');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTHER_FROZEN_CONT','Please Specify','VARCHAR',1020,'REP_CBU_CBB_PROCESSING_PROC','OTHER_FROZEN_CONT');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_NUM_OF_BAGS','Number of bags','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','FK_NUM_OF_BAGS');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'CRYOBAG_MANUFAC','CryoBag Manufacturer','VARCHAR',1020,'REP_CBU_CBB_PROCESSING_PROC','CRYOBAG_MANUFAC');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_BAG_TYPE','Bag Type','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','FK_BAG_TYPE');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_BAG_1_TYPE','Bag 1 Type','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','FK_BAG_1_TYPE');
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_BAG_2_TYPE','Bag 2 Type','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','FK_BAG_2_TYPE');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTHER_BAG_TYPE','Please Specify','VARCHAR',120,'REP_CBU_CBB_PROCESSING_PROC','OTHER_BAG_TYPE');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_STOR_TEMP','Storage Temperature','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','FK_STOR_TEMP');
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'MAX_VALUE','Maximum Volume(ml)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','MAX_VALUE');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FK_CONTRL_RATE_FREEZING','Controlled Rate Freezing','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','FK_CONTRL_RATE_FREEZING');
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_INDI_FRAC','Number OF Individual Fractions','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','NO_OF_INDI_FRAC');
   

 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'THOU_UNIT_PER_ML_HEPARIN_PER','1000 unit/ml Heparin(%)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','THOU_UNIT_PER_ML_HEPARIN_PER');
   
  insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'THOU_UNIT_PER_ML_HEPARIN','1000 unit/ml Heparin(ML)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','THOU_UNIT_PER_ML_HEPARIN');
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FIVE_UNIT_PER_ML_HEPARIN_PER','5000 unit/ml Heparin(%)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','FIVE_UNIT_PER_ML_HEPARIN_PER');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FIVE_UNIT_PER_ML_HEPARIN','5000 unit/ml Heparin(ML)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','FIVE_UNIT_PER_ML_HEPARIN');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'TEN_UNIT_PER_ML_HEPARIN_PER','10,000 unit/ml Heparin(%)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','TEN_UNIT_PER_ML_HEPARIN_PER');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'TEN_UNIT_PER_ML_HEPARIN','10,000 unit/ml Heparin(ML)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','TEN_UNIT_PER_ML_HEPARIN');
    
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'SIX_PER_HYDRO_STARCH_PER','6% Hydroxyethyl Starch( %)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','SIX_PER_HYDRO_STARCH_PER');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'SIX_PER_HYDROXYETHYL_STARCH','6% Hydroxyethyl Starch( ML)','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','SIX_PER_HYDROXYETHYL_STARCH');
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'CPDA_PER','CPDA( % ) ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','CPDA_PER');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'CPDA','CPDA( ML ) ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','CPDA');
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'CPD_PER','CPD( % ) ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','CPD_PER');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'CPD','CPD( ML ) ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','CPD');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'ACD_PER','ACD( % ) ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','ACD_PER');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'ACD','ACD( ML ) ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','ACD');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTHER_ANTICOAGULANT_PER','Other Anticoagulant( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','OTHER_ANTICOAGULANT_PER');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTHER_ANTICOAGULANT','Other Anticoagulant( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','OTHER_ANTICOAGULANT');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'SPECIFY_OTH_ANTI','Specify Other Anticoagulant','VARCHAR',1020,'REP_CBU_CBB_PROCESSING_PROC','SPECIFY_OTH_ANTI');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'HUN_PER_DMSO_PER','100% DMSO( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','HUN_PER_DMSO_PER');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'HUN_PER_DMSO','100% DMSO( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','HUN_PER_DMSO');
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'HUN_PER_GLYCEROL_PER','100% Glycerol( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','HUN_PER_GLYCEROL_PER');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'HUN_PER_GLYCEROL','100% Glycerol( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','HUN_PER_GLYCEROL');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'TEN_PER_DEXTRAN_40_PER','10% Dextran-40( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','TEN_PER_DEXTRAN_40_PER');
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'TEN_PER_DEXTRAN_40','10% Dextran-40( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','TEN_PER_DEXTRAN_40');
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FIVE_PER_HUMAN_ALBU_PER','5% Human Albumin( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','FIVE_PER_HUMAN_ALBU_PER');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FIVE_PER_HUMAN_ALBU','5% Human Albumin( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','FIVE_PER_HUMAN_ALBU');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'TWEN_FIVE_HUM_ALBU_PER','25% Human Albumin( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','TWEN_FIVE_HUM_ALBU_PER');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'TWEN_FIVE_HUM_ALBU','25% Human Albumin( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','TWEN_FIVE_HUM_ALBU');
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'PLASMALYTE_PER','Plasmalyte( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','PLASMALYTE_PER');
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'PLASMALYTE','Plasmalyte( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','PLASMALYTE');
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTH_CRYOPROTECTANT_PER','Other Cryoprotectant( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','OTH_CRYOPROTECTANT_PER');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTH_CRYOPROTECTANT','Other Cryoprotectant( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','OTH_CRYOPROTECTANT');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'SPEC_OTH_CRYOPRO','Specify Other Cryoprotectant','VARCHAR',1024,'REP_CBU_CBB_PROCESSING_PROC','SPEC_OTH_CRYOPRO');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FIVE_PER_DEXTROSE_PER','5% Dextrose( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','FIVE_PER_DEXTROSE_PER');
   
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FIVE_PER_DEXTROSE','5% Dextrose( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','FIVE_PER_DEXTROSE');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'POINNT_NINE_PER_NACL_PER','0.9% NaCl( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','POINNT_NINE_PER_NACL_PER');
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'POINNT_NINE_PER_NACL','0.9% NaCl( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','POINNT_NINE_PER_NACL');
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTH_DILUENTS_PER','Other Diluents( % )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','OTH_DILUENTS_PER');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'OTH_DILUENTS','Other Diluents( ML )','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','OTH_DILUENTS');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'SPEC_OTH_DILUENTS','Specify Other Diluents','VARCHAR',1020,'REP_CBU_CBB_PROCESSING_PROC','SPEC_OTH_DILUENTS');
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'FILTER_PAPER','Number of Filter Paper Samples','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','FILTER_PAPER');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'RBC_PALLETS','Number of RBC Pellets ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','RBC_PALLETS');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_EXTR_DNA_ALIQUOTS','Number of Extracted DNA Samples ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','NO_OF_EXTR_DNA_ALIQUOTS');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_SERUM_ALIQUOTS','Number of Serum Samples ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','NO_OF_SERUM_ALIQUOTS');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_PLASMA_ALIQUOTS','Number of Plasma Samples ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','NO_OF_PLASMA_ALIQUOTS');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_NONVIABLE_ALIQUOTS','Number of Non-Viable Cell samples ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','NO_OF_NONVIABLE_ALIQUOTS');
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_VIABLE_SMPL_FINAL_PROD','NO. OF viable samples not represent final product ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','NO_OF_VIABLE_SMPL_FINAL_PROD');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_SEGMENTS','Number of Segments ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','NO_OF_SEGMENTS');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_OTH_REP_ALLIQUOTS_F_PROD','No. of other representative aliquots ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','NO_OF_OTH_REP_ALLIQUOTS_F_PROD');
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_OTH_REP_ALLIQ_ALTER_COND','Number of Maternal Serum Samples ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','NO_OF_OTH_REP_ALLIQ_ALTER_COND');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_SERUM_MATER_ALIQUOTS','Number of Maternal Plasma Samples ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','NO_OF_SERUM_MATER_ALIQUOTS');
  
  insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_PLASMA_MATER_ALIQUOTS','Number of Maternal Extracted DNA Samples ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','NO_OF_PLASMA_MATER_ALIQUOTS');
  
 insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_EXTR_DNA_MATER_ALIQUOTS','Number of Miscellaneous Maternal Samples ','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','NO_OF_EXTR_DNA_MATER_ALIQUOTS');
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'NO_OF_CELL_MATER_ALIQUOTS','NO. of Miscellaneous Maternal Samples','NUMBER',19,'REP_CBU_CBB_PROCESSING_PROC','NO_OF_CELL_MATER_ALIQUOTS');
  


insert into ER_LKPVIEW 
(PK_LKPVIEW,LKPVIEW_NAME,LKPVIEW_DESC,FK_LKPLIB,LKPVIEW_FILTER)
values((select MAX(PK_LKPVIEW)+1 FROM ER_LKPVIEW),'CBU CBB PROCESSING PROCEDURE',null,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='CBU CBB PROCESSING PROCEDURE'),'fk_account=[:ACCID]');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CORD_LOCAL_CBU_ID' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,1,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CORD_REGISTRY_ID' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,2,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='REGISTRY_MATERNAL_ID' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,3,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='MATERNAL_LOCAL_ID' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,4,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_ACCOUNT' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,5,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','N');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='PROC_NAME' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,6,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='PROC_START_DATE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,7,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='PRODUCT_CODE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,8,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='PROC_TERMI_DATE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,9,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_PROC_METH_ID' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,10,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_IF_AUTOMATED' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,11,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTHER_PROCESSING' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,12,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_PRODUCT_MODIFICATION' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,13,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTHER_PROD_MODI' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,14,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_STOR_METHOD' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,15,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_FREEZ_MANUFAC' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,16,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTHER_FREEZ_MANUFAC' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,17,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_FROZEN_IN' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,18,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTHER_FROZEN_CONT' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,19,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_NUM_OF_BAGS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,20,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CRYOBAG_MANUFAC' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,21,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_BAG_TYPE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,22,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_BAG_1_TYPE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,23,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_BAG_2_TYPE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,24,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTHER_BAG_TYPE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,25,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_STOR_TEMP' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,26,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='MAX_VALUE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,27,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_CONTRL_RATE_FREEZING' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,28,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_INDI_FRAC' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,29,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='THOU_UNIT_PER_ML_HEPARIN_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,30,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='THOU_UNIT_PER_ML_HEPARIN' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,31,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FIVE_UNIT_PER_ML_HEPARIN_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,32,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FIVE_UNIT_PER_ML_HEPARIN' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,33,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='TEN_UNIT_PER_ML_HEPARIN_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,34,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='TEN_UNIT_PER_ML_HEPARIN' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,35,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='SIX_PER_HYDRO_STARCH_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,36,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='SIX_PER_HYDROXYETHYL_STARCH' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,37,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CPDA_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,38,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CPDA' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,39,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CPD_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,40,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CPD' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,41,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='ACD_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,42,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='ACD' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,43,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTHER_ANTICOAGULANT_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,44,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTHER_ANTICOAGULANT' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,45,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='SPECIFY_OTH_ANTI' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,46,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='HUN_PER_DMSO_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,47,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='HUN_PER_DMSO' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,48,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='HUN_PER_GLYCEROL_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,49,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='HUN_PER_GLYCEROL' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,50,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='TEN_PER_DEXTRAN_40_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,51,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='TEN_PER_DEXTRAN_40' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,52,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FIVE_PER_HUMAN_ALBU_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,53,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FIVE_PER_HUMAN_ALBU' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,54,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='TWEN_FIVE_HUM_ALBU_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,55,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='TWEN_FIVE_HUM_ALBU' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,56,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='PLASMALYTE_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,57,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='PLASMALYTE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,58,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTH_CRYOPROTECTANT_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,59,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTH_CRYOPROTECTANT' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,60,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='SPEC_OTH_CRYOPRO' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,61,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FIVE_PER_DEXTROSE_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,62,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FIVE_PER_DEXTROSE' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,63,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='POINNT_NINE_PER_NACL_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,64,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='POINNT_NINE_PER_NACL' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,65,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTH_DILUENTS_PER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,66,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='OTH_DILUENTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,67,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='SPEC_OTH_DILUENTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,68,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FILTER_PAPER' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,69,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='RBC_PALLETS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,70,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_EXTR_DNA_ALIQUOTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,71,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_SERUM_ALIQUOTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,72,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_PLASMA_ALIQUOTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,73,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_NONVIABLE_ALIQUOTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,74,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_VIABLE_SMPL_FINAL_PROD' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,75,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_SEGMENTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,76,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_OTH_REP_ALLIQUOTS_F_PROD' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,77,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_OTH_REP_ALLIQ_ALTER_COND' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,78,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_SERUM_MATER_ALIQUOTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,79,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_PLASMA_MATER_ALIQUOTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,80,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_EXTR_DNA_MATER_ALIQUOTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,81,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='NO_OF_CELL_MATER_ALIQUOTS' And Lkpcol_Table='REP_CBU_CBB_PROCESSING_PROC'),
Null,82,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='CBU CBB PROCESSING PROCEDURE'),'10%','Y');

commit;

-----------------Eligibility View--------------

      /* ************************************                  INSERT INTO      ER_LKPLIB  TABLE                    *************************************/
   
insert into ER_LKPLIB(PK_LKPLIB,
LKPTYPE_NAME,
LKPTYPE_VERSION,
LKPTYPE_DESC,
LKPTYPE_TYPE,
FK_ACCOUNT,
FK_SITE) values((select MAX(PK_LKPLIB)+1 FROM ER_LKPLIB),'dynReports',null,'ELIGIBILITY DETERMINATION','dyn_a',null,null);


       /* ************************************                  INSERT INTO      ER_LKPCOL TABLE                    *************************************/
	   
  insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='ELIGIBILITY DETERMINATION'),'CORD_REGISTRY_ID','CBU REGISTRY ID','VARCHAR',50,'REP_ELIGIBLE_VIEW','CORD_REGISTRY_ID' ); 

  insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='ELIGIBILITY DETERMINATION'),'CORD_LOCAL_CBU_ID','CBU LOCAL ID','VARCHAR',50,'REP_ELIGIBLE_VIEW','CORD_LOCAL_CBU_ID' );
   
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='ELIGIBILITY DETERMINATION'),'REGISTRY_MATERNAL_ID','MATERNAL REGISTRY ID','VARCHAR',50,'REP_ELIGIBLE_VIEW','REGISTRY_MATERNAL_ID' ); 
   
   insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='ELIGIBILITY DETERMINATION'),'MATERNAL_LOCAL_ID','MATERNAL LOCAL ID','VARCHAR',50,'REP_ELIGIBLE_VIEW','MATERNAL_LOCAL_ID' );
  
  insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='ELIGIBILITY DETERMINATION'),'STATUS','Status','VARCHAR',4000,'REP_ELIGIBLE_VIEW','STATUS' );
  
   insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='ELIGIBILITY DETERMINATION'),'STATUS_REASONS','Status Reasons','VARCHAR',4000,'REP_ELIGIBLE_VIEW','STATUS_REASONS' );
   
  insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='ELIGIBILITY DETERMINATION'),'STATUS_DATE','Status Date','DATE',null,'REP_ELIGIBLE_VIEW','STATUS_DATE' );
  
   insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='ELIGIBILITY DETERMINATION'),'STATUS_REMARKS','Status Remarks','VARCHAR',200,'REP_ELIGIBLE_VIEW','STATUS_REMARKS' );
  
  insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='ELIGIBILITY DETERMINATION'),'CBB_NAME','Cbb Name','VARCHAR',50,'REP_ELIGIBLE_VIEW','CBB_NAME' );
 
  
insert into ER_LKPCOL (PK_LKPCOL,FK_LKPLIB,LKPCOL_NAME,LKPCOL_DISPVAL,LKPCOL_DATATYPE,LKPCOL_LEN,LKPCOL_TABLE,LKPCOL_KEYWORD)
values((select MAX(PK_LKPCOL)+1 FROM ER_LKPCOL),(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='ELIGIBILITY DETERMINATION'),'FK_ACCOUNT','ACCOUNT ID ','NUMBER',19,'REP_ELIGIBLE_VIEW','lkp_pk' );
 
           /* ************************************                  INSERT  INTO      ER_LKPVIEW    TABLE                    *************************************/
		   
insert into ER_LKPVIEW 
(PK_LKPVIEW,LKPVIEW_NAME,LKPVIEW_DESC,FK_LKPLIB,LKPVIEW_FILTER)
values((select MAX(PK_LKPVIEW)+1 FROM ER_LKPVIEW),'ELIGIBILITY DETERMINATION',null,(select PK_LKPLIB from ER_LKPLIB where LKPTYPE_DESC ='ELIGIBILITY DETERMINATION'),'fk_account=[:ACCID]');

          /* ************************************                  INSERT  INTO      ER_LKPVIEWCOL   TABLE                    *************************************/
		  
insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CORD_REGISTRY_ID' And Lkpcol_Table='REP_ELIGIBLE_VIEW'),
Null,1,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='ELIGIBILITY DETERMINATION'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CORD_LOCAL_CBU_ID' And Lkpcol_Table='REP_ELIGIBLE_VIEW'),
Null,2,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='ELIGIBILITY DETERMINATION'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='REGISTRY_MATERNAL_ID' And Lkpcol_Table='REP_ELIGIBLE_VIEW'),
Null,3,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='ELIGIBILITY DETERMINATION'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='MATERNAL_LOCAL_ID' And Lkpcol_Table='REP_ELIGIBLE_VIEW'),
Null,4,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='ELIGIBILITY DETERMINATION'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='STATUS' And Lkpcol_Table='REP_ELIGIBLE_VIEW'),
Null,5,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='ELIGIBILITY DETERMINATION'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='STATUS_REASONS' And Lkpcol_Table='REP_ELIGIBLE_VIEW'),
Null,6,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='ELIGIBILITY DETERMINATION'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='STATUS_DATE' And Lkpcol_Table='REP_ELIGIBLE_VIEW'),
Null,7,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='ELIGIBILITY DETERMINATION'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='STATUS_REMARKS' And Lkpcol_Table='REP_ELIGIBLE_VIEW'),
Null,8,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='ELIGIBILITY DETERMINATION'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='CBB_NAME' And Lkpcol_Table='REP_ELIGIBLE_VIEW'),
Null,9,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='ELIGIBILITY DETERMINATION'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
Lkpview_Is_Display) Values((Select Max(Pk_Lkpviewcol)+1 From Er_Lkpviewcol),(Select Pk_Lkpcol From Er_Lkpcol Where Lkpcol_Name ='FK_ACCOUNT' And Lkpcol_Table='REP_ELIGIBLE_VIEW'),
Null,10,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='ELIGIBILITY DETERMINATION'),'10%','N' );

commit;


--------------------------Prepost Test---------------


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'CORD_REGISTRY_ID','Cord Registry ID','VARCHAR2',50,'REP_CBU_PREPOST_INFO','CORD_REGISTRY_ID' );
 
 Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'CORD_LOCAL_CBU_ID','Cord Local ID','VARCHAR2',50,'REP_CBU_PREPOST_INFO','CORD_LOCAL_CBU_ID' );
   
Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'REGISTRY_MATERNAL_ID','Registry Maternal ID','VARCHAR2',50,'REP_CBU_PREPOST_INFO','REGISTRY_MATERNAL_ID' );
       
	   Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'MATERNAL_LOCAL_ID','Maternal Local ID','VARCHAR2',50,'REP_CBU_PREPOST_INFO','MATERNAL_LOCAL_ID' );
 
Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'TEST_NAME','Test Name','VARCHAR2',200,'REP_CBU_PREPOST_INFO','TEST_NAME' );
  
  Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'TIMING_OF_TEST','Timing of Test','VARCHAR2',4000,'REP_CBU_PREPOST_INFO','TIMING_OF_TEST' );
 
 Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'TEST_RESULT','Test Result','VARCHAR2',100,'REP_CBU_PREPOST_INFO','TEST_RESULT' );
 
Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'TEST_METHOD','Test Method','VARCHAR2',4000,'REP_CBU_PREPOST_INFO','TEST_METHOD' );
  
Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'TEST_COMMENTS','Test Comments','VARCHAR2',2000,'REP_CBU_PREPOST_INFO','TEST_COMMENTS' );
  
  Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'SAMPLE_TYPE','Sample Type','VARCHAR2',4000,'REP_CBU_PREPOST_INFO','SAMPLE_TYPE' );
  
Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'TEST_DATE','Test Date','DATE',null,'REP_CBU_PREPOST_INFO','TEST_DATE' );
  



 Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'CBB_NAME','CBB Name','VARCHAR2',50,'REP_CBU_PREPOST_INFO','CBB_NAME' );
        
    Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'CREATED_ON','Created On','DATE',null,'REP_CBU_PREPOST_INFO','CREATED_ON' );
         
    Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'CREATOR','Created By','VARCHAR2',61,'REP_CBU_PREPOST_INFO','CREATOR' );
      
	  Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'LAST_MODIFIED_DATE','Last Modified Date','DATE',null,'REP_CBU_PREPOST_INFO','LAST_MODIFIED_DATE' );
  
  Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'LAST_MODIFIED_BY','Last Modified By','VARCHAR2',61,'REP_CBU_PREPOST_INFO','LAST_MODIFIED_BY' );

    Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'FK_ACCOUNT','FK Account','NUMBER',null,'REP_CBU_PREPOST_INFO','lkp_pk' );
        
    Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'),'TEST_NOTES','Test Notes','CLOB',null,'REP_CBU_PREPOST_INFO','TEST_NOTES' );
        
      insert into ER_LKPVIEW 
(Pk_Lkpview,Lkpview_Name,Lkpview_Desc,Fk_Lkplib)
Values((select max(pk_lkpview)+1 from er_lkpview),'Pre Post Test',Null,(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='Pre Post Test'));


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='CORD_REGISTRY_ID' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,1,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='CORD_LOCAL_CBU_ID' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,2,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='REGISTRY_MATERNAL_ID' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,3,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','Y' );


     
  
insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='MATERNAL_LOCAL_ID' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,4,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','Y' );

 
insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='TEST_NAME' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,5,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','Y' );


     
insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='TIMING_OF_TEST' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,6,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','Y' );


     
insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='TEST_RESULT' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,7,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='TEST_METHOD' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,8,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='TEST_COMMENTS' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,9,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','Y' );
 

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='SAMPLE_TYPE' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,10,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='TEST_DATE' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,11,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='CBB_NAME' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,12,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='CREATED_ON' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,13,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='CREATOR' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,14,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','Y' );


    
insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='LAST_MODIFIED_DATE' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,15,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='LAST_MODIFIED_BY' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,16,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='FK_ACCOUNT' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,17,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','N' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='TEST_NOTES' and LKPCOL_TABLE='REP_CBU_PREPOST_INFO'),
Null,18,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='Pre Post Test'),'10%','Y' );

commit;


--------------------------Lab Summary-------------




insert into ER_LKPLIB(PK_LKPLIB,
LKPTYPE_NAME,
LKPTYPE_VERSION,
LKPTYPE_DESC,
LKPTYPE_TYPE,
FK_ACCOUNT,
FK_SITE) 
values
(Seq_Er_Lkplib.Nextval,'dynReports',Null,'LAB SUMMARY INFO','dyn_a',Null,Null);


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'CORD_REGISTRY_ID','Cord Registry ID','VARCHAR2',50,'REP_CBU_LAB_SUMMARY','CORD_REGISTRY_ID' );

  
Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'CORD_LOCAL_CBU_ID','Cord Local CBU ID','VARCHAR2',50,'REP_CBU_LAB_SUMMARY','CORD_LOCAL_CBU_ID' );
 
Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'REGISTRY_MATERNAL_ID','Registry Maternal ID','VARCHAR2',50,'REP_CBU_LAB_SUMMARY','REGISTRY_MATERNAL_ID' );
   
Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'MATERNAL_LOCAL_ID','Maternal Local ID','VARCHAR2',50,'REP_CBU_LAB_SUMMARY','MATERNAL_LOCAL_ID' );
  

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'PROCESS_DATE','Process Date','DATE',null,'REP_CBU_LAB_SUMMARY','PROCESS_DATE' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'BACTERIAL_CULTURE','Bacterial Culture','VARCHAR2',4000,'REP_CBU_LAB_SUMMARY','BACTERIAL_CULTURE' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'BACT_COMMENT','Bacterial Comments','VARCHAR2',255,'REP_CBU_LAB_SUMMARY','BACT_COMMENT' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'FREEZE_DATE','Freeze Date','DATE',null,'REP_CBU_LAB_SUMMARY','FREEZE_DATE' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'FUNGAL_CULTURE','Fungal Culture','VARCHAR2',4000,'REP_CBU_LAB_SUMMARY','FUNGAL_CULTURE' );


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'FUNG_COMMENT','Fungal Comment','VARCHAR2',255,'REP_CBU_LAB_SUMMARY','FUNG_COMMENT' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'BACT_CULT_DATE','Bacterial Date','DATE',null,'REP_CBU_LAB_SUMMARY','BACT_CULT_DATE' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'RH_TYPE','Rh Type','VARCHAR2',4000,'REP_CBU_LAB_SUMMARY','RH_TYPE' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'FUNG_CULT_DATE','Fung Cult Date','DATE',null,'REP_CBU_LAB_SUMMARY','FUNG_CULT_DATE' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'ABO_TYPE','ABO Type','VARCHAR2',4000,'REP_CBU_LAB_SUMMARY','ABO_TYPE' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'HMGLBN_TSTNG','Hemoglobin Testing','VARCHAR2',4000,'REP_CBU_LAB_SUMMARY','HMGLBN_TSTNG' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'FK_ACCOUNT','FK Account','NUMBER',null,'REP_CBU_LAB_SUMMARY','lkp_pk' );

insert into ER_LKPVIEW 
(Pk_Lkpview,Lkpview_Name,Lkpview_Desc,Fk_Lkplib)
Values((select max(pk_lkpview)+1 from er_lkpview),'LAB SUMMARY INFO',Null,(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'));


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'CREATOR','Creator','VARCHAR2',61,'REP_CBU_LAB_SUMMARY','CREATOR' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'CREATED_ON','Created on','DATE',null,'REP_CBU_LAB_SUMMARY','CREATED_ON' );


Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'LAST_MODIFIED_BY','Last Modified By','VARCHAR2',61,'REP_CBU_LAB_SUMMARY','LAST_MODIFIED_BY' );

Insert Into Er_Lkpcol (Pk_Lkpcol,Fk_Lkplib,Lkpcol_Name,Lkpcol_Dispval,Lkpcol_Datatype,Lkpcol_Len,Lkpcol_Table,Lkpcol_Keyword)
Values((select max(pk_lkpcol)+1 from er_lkpcol),(Select Pk_Lkplib From Er_Lkplib Where Lkptype_Desc ='LAB SUMMARY INFO'),'LAST_MODIFIED_DATE','Last Modified Date','DATE',null,'REP_CBU_LAB_SUMMARY','LAST_MODIFIED_DATE' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='CORD_REGISTRY_ID' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,1,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='CORD_LOCAL_CBU_ID' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,2,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='REGISTRY_MATERNAL_ID' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,3,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='MATERNAL_LOCAL_ID' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,4,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='PROCESS_DATE' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,5,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='BACTERIAL_CULTURE' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,6,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='BACT_COMMENT' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,7,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='FREEZE_DATE' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,8,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='FUNGAL_CULTURE' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,9,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );



insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='FUNG_COMMENT' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,10,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='ABO_TYPE' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,11,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='HMGLBN_TSTNG' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,12,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='RH_TYPE' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,13,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='BACT_CULT_DATE' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,14,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='FUNG_CULT_DATE' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,15,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='CREATOR' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,16,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='CREATED_ON' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,17,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='LAST_MODIFIED_BY' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,18,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );

insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='LAST_MODIFIED_DATE' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,19,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','Y' );


insert into ER_LKPVIEWCOL (PK_LKPVIEWCOL,
FK_LKPCOL,
LKPVIEW_IS_SEARCH,
LKPVIEW_SEQ,
FK_LKPVIEW,
Lkpview_Displen,
LKPVIEW_IS_DISPLAY) values((select max(pk_lkpviewcol)+1 from er_lkpviewcol),(select PK_LKPCOL from ER_LKPCOL where LKPCOL_NAME ='FK_ACCOUNT' and LKPCOL_TABLE='REP_CBU_LAB_SUMMARY'),
Null,20,(Select Pk_Lkpview From Er_Lkpview Where Lkpview_Name='LAB SUMMARY INFO'),'10%','N' );


commit;










