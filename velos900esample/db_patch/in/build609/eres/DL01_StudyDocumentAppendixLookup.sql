set define off;

DECLARE
  v_item_exists NUMBER :=0;
  v_Pk_Lib NUMBER :=0;
  v_pk_View NUMBER :=0;
  
  
begin

select count(*) into v_item_exists from er_lkplib where LKPTYPE_NAME='studyDocAppx';
if (v_item_exists = 0) then
-- Insert the row in er_lkplib for new Study Doc lookup.
INSERT
INTO er_lkplib VALUES
  (
    6500,
    'studyDocAppx',
    '1.1',
    'Study Appendix',
    'studydocs',
    '',
    ''
  );
  


    dbms_output.put_line('Lookup :studyDocAppx inserted');
  else 
    dbms_output.put_line('Lookup :studyDocAppx already exists');
  end if;
  SELECT PK_LKPLIB into v_Pk_Lib FROM er_lkplib WHERE LKPTYPE_NAME='studyDocAppx';
  select count(*) into v_item_exists from ER_LKPVIEW where LKPVIEW_NAME='Study Appendix';
  if (v_item_exists = 0) then 
                -- Insert the row in ER_LKPVIEW for new Study Doc lookup.
              INSERT
              INTO ER_LKPVIEW VALUES
                (
                  6500,
                  'Study Appendix',
                  'Study Appendix',
                  v_Pk_Lib,
                  '',
                  '',
                  '',
                  '',
                  'FK_STUDY=[:PROTID]',
                  ''
                );
            dbms_output.put_line('Lookup :studyDocAppx inserted');
          else 
            dbms_output.put_line('Lookup :studyDocAppx already exists');
          end if;     
              select PK_LKPVIEW into v_pk_View from ER_LKPVIEW where FK_LKPLIB =v_Pk_Lib;
              select count(*) into v_item_exists from er_ctrltab where CTRL_KEY='studyDocAppx' and CTRL_VALUE=v_pk_View;
              if (v_item_exists = 0) then   
              -- Insert the row in er_ctrltab for new Study Doc lookup.
                INSERT
                INTO er_ctrltab 
                (PK_CTRLTAB,
                CTRL_VALUE,
                CTRL_DESC,
                CTRL_KEY
                )VALUES
                  (
                    SEQ_er_ctrltab.nextval ,
                   v_pk_View,
                    'Study Appendix',
                    'studyDocAppx'
                  );
                
                dbms_output.put_line('Lookup :studyDocAppx in CTRLTAB inserted');
              else 
                dbms_output.put_line('Lookup :studyDocAppx in CTRLTAB already exists');
              end if;   
          
          select count(*) into v_item_exists from er_lkpcol where LKPCOL_NAME='PK_STUDYAPNDX' and FK_LKPLIB=v_Pk_Lib;
              if (v_item_exists = 0) then
              -- Insert the row in er_lkpcol for new Study Doc lookup.
              INSERT
              INTO er_lkpcol VALUES
                (
                  (SELECT (MAX(PK_LKPCOL)+1) FROM er_lkpcol),
                  v_Pk_Lib,
                  'PK_STUDYAPNDX',
                  'PK_STUDYAPNDX',
                  'number',
                  '',
                  'ER_STUDYAPNDX',
                  'PK_STUDYAPNDX'
                );
              dbms_output.put_line('Lookup :PK_STUDYAPNDX in er_lkpcol inserted');
            else 
              dbms_output.put_line('Lookup :PK_STUDYAPNDX in er_lkpcol already exists');
            end if;  
            
             select count(*) into v_item_exists from er_lkpcol where LKPCOL_NAME='FK_STUDY' and FK_LKPLIB=v_Pk_Lib;
              if (v_item_exists = 0) then
                INSERT
              INTO er_lkpcol VALUES
                (
                  (SELECT (MAX(PK_LKPCOL)+1) FROM er_lkpcol),
                  v_Pk_Lib,
                  'FK_STUDY',
                  'FK_STUDY',
                  'number',
                  '',
                  'ER_STUDYAPNDX',
                  'LKP_PK'
                );
                 dbms_output.put_line('Lookup :FK_STUDY in er_lkpcol inserted');
            else 
              dbms_output.put_line('Lookup :FK_STUDY in er_lkpcol already exists');
            end if;
            
             select count(*) into v_item_exists from er_lkpcol where LKPCOL_NAME='STUDYAPNDX_TYPE' and FK_LKPLIB=v_Pk_Lib;
              if (v_item_exists = 0) then
              INSERT
              INTO er_lkpcol VALUES
                (
                  (SELECT (MAX(PK_LKPCOL)+1) FROM er_lkpcol),
                  v_Pk_Lib,
                  'STUDYAPNDX_TYPE',
                  'Document Type',
                  'varchar2',
                  '',
                  'ER_STUDYAPNDX',
                  'STUDYAPNDX_TYPE'
                );
                   dbms_output.put_line('Lookup :STUDYAPNDX_TYPE in er_lkpcol inserted');
            else 
              dbms_output.put_line('Lookup :STUDYAPNDX_TYPE in er_lkpcol already exists');
            end if;
            
              select count(*) into v_item_exists from er_lkpcol where LKPCOL_NAME='STUDYAPNDX_URI' and FK_LKPLIB=v_Pk_Lib;
              if (v_item_exists = 0) then
              INSERT
              INTO er_lkpcol VALUES
                (
                  (SELECT (MAX(PK_LKPCOL)+1) FROM er_lkpcol),
                  v_Pk_Lib,
                  'STUDYAPNDX_URI',
                  'Document URI',
                  'varchar2',
                  '',
                  'ER_STUDYAPNDX',
                  'STUDYAPNDX_URI'
                );
                     dbms_output.put_line('Lookup :STUDYAPNDX_URI in er_lkpcol inserted');
            else 
              dbms_output.put_line('Lookup :STUDYAPNDX_URI in er_lkpcol already exists');
            end if;
            
             select count(*) into v_item_exists from er_lkpcol where LKPCOL_NAME='STUDYAPNDX_DESC' and FK_LKPLIB=v_Pk_Lib;
              if (v_item_exists = 0) then
              INSERT
              INTO er_lkpcol VALUES
                (
                  (SELECT (MAX(PK_LKPCOL)+1) FROM er_lkpcol),
                  v_Pk_Lib,
                  'STUDYAPNDX_DESC',
                  'Document Description',
                  'varchar2',
                  '',
                  'ER_STUDYAPNDX',
                  'STUDYAPNDX_DESC'
                );
                 dbms_output.put_line('Lookup :STUDYAPNDX_DESC in er_lkpcol inserted');
            else 
              dbms_output.put_line('Lookup :STUDYAPNDX_DESC in er_lkpcol already exists');
            end if;
            
             select count(*) into v_item_exists from er_lkpcol where LKPCOL_NAME='STUDYAPNDX_FILE' and FK_LKPLIB=v_Pk_Lib;
              if (v_item_exists = 0) then
              INSERT
              INTO er_lkpcol VALUES
                (
                  (SELECT (MAX(PK_LKPCOL)+1) FROM er_lkpcol),
                 v_Pk_Lib,
                  'STUDYAPNDX_FILE',
                  'Document File',
                  'varchar2',
                  '',
                  'ER_STUDYAPNDX',
                  'STUDYAPNDX_FILE'
                );
               dbms_output.put_line('Lookup :STUDYAPNDX_FILE in er_lkpcol inserted');
            else 
              dbms_output.put_line('Lookup :STUDYAPNDX_FILE in er_lkpcol already exists');
            end if;
   
    -- Insert the row in ER_LKPVIEWCOL for new Study Doc lookup.           
           select count(*) into v_item_exists from ER_LKPVIEWCOL where FK_LKPCOL=(SELECT PK_LKPCOL FROM ER_LKPCOL WHERE LKPCOL_NAME='PK_STUDYAPNDX' and FK_LKPLIB=v_Pk_Lib) and FK_LKPVIEW=v_pk_View;
              if (v_item_exists = 0) then   
              INSERT
              INTO ER_LKPVIEWCOL VALUES
               (
                  (SELECT (MAX(PK_LKPVIEWCOL)+1) FROM ER_LKPVIEWCOL),
                  (SELECT PK_LKPCOL FROM ER_LKPCOL WHERE LKPCOL_NAME='PK_STUDYAPNDX' and FK_LKPLIB =v_Pk_Lib),
                  'N',
                  1,
                 v_pk_View,
                  '10%',
                  'N'   
                );
                  dbms_output.put_line('Lookup :PK_STUDYAPNDX in ER_LKPVIEWCOL inserted');
            else 
              dbms_output.put_line('Lookup :PK_STUDYAPNDX in ER_LKPVIEWCOL already exists');
            end if; 
             select count(*) into v_item_exists from ER_LKPVIEWCOL where FK_LKPCOL=(SELECT PK_LKPCOL FROM ER_LKPCOL WHERE LKPCOL_NAME='FK_STUDY' and FK_LKPLIB=v_Pk_Lib) and FK_LKPVIEW=v_pk_View;
              if (v_item_exists = 0) then 
                INSERT
              INTO ER_LKPVIEWCOL VALUES
               (
                  (SELECT (MAX(PK_LKPVIEWCOL)+1) FROM ER_LKPVIEWCOL),
                  (SELECT PK_LKPCOL FROM ER_LKPCOL WHERE LKPCOL_NAME='FK_STUDY' and FK_LKPLIB =v_Pk_Lib),
                  'N',
                  2,
                  v_pk_View,
                  '10%',
                  'N'   
                );
                       dbms_output.put_line('Lookup :FK_STUDY in ER_LKPVIEWCOL inserted');
            else 
              dbms_output.put_line('Lookup :FK_STUDY in ER_LKPVIEWCOL already exists');
            end if; 
              select count(*) into v_item_exists from ER_LKPVIEWCOL where FK_LKPCOL=(SELECT PK_LKPCOL FROM ER_LKPCOL WHERE LKPCOL_NAME='STUDYAPNDX_DESC' and FK_LKPLIB=v_Pk_Lib) and FK_LKPVIEW=v_pk_View;
              if (v_item_exists = 0) then
                INSERT
              INTO ER_LKPVIEWCOL VALUES
               (
                  (SELECT (MAX(PK_LKPVIEWCOL)+1) FROM ER_LKPVIEWCOL),
                  (SELECT PK_LKPCOL FROM ER_LKPCOL WHERE LKPCOL_NAME='STUDYAPNDX_DESC' and FK_LKPLIB =v_Pk_Lib),
                  'Y',
                  3,
                  v_pk_View,
                  '30%',
                  'Y'   
                );
                               dbms_output.put_line('Lookup :STUDYAPNDX_DESC in ER_LKPVIEWCOL inserted');
            else 
              dbms_output.put_line('Lookup :STUDYAPNDX_DESC in ER_LKPVIEWCOL already exists');
            end if; 
                  select count(*) into v_item_exists from ER_LKPVIEWCOL where FK_LKPCOL=(SELECT PK_LKPCOL FROM ER_LKPCOL WHERE LKPCOL_NAME='STUDYAPNDX_FILE' and FK_LKPLIB=v_Pk_Lib) and FK_LKPVIEW=v_pk_View;
              if (v_item_exists = 0) then
                  INSERT
              INTO ER_LKPVIEWCOL VALUES
               (
                  (SELECT (MAX(PK_LKPVIEWCOL)+1) FROM ER_LKPVIEWCOL),
                  (SELECT PK_LKPCOL FROM ER_LKPCOL WHERE LKPCOL_NAME='STUDYAPNDX_FILE' and FK_LKPLIB =v_Pk_Lib),
                  'Y',
                  4,
                  v_pk_View,
                  '20%',
                  'Y'   
                );
            dbms_output.put_line('Lookup :STUDYAPNDX_FILE in ER_LKPVIEWCOL inserted');
            else 
              dbms_output.put_line('Lookup :STUDYAPNDX_FILE in ER_LKPVIEWCOL already exists');
            end if;
            
      select count(*) into v_item_exists from ER_LKPVIEWCOL where FK_LKPCOL=(SELECT PK_LKPCOL FROM ER_LKPCOL WHERE LKPCOL_NAME='STUDYAPNDX_TYPE' and FK_LKPLIB=v_Pk_Lib) and FK_LKPVIEW=v_pk_View;
              if (v_item_exists = 0) then
                INSERT
              INTO ER_LKPVIEWCOL VALUES
               (
                  (SELECT (MAX(PK_LKPVIEWCOL)+1) FROM ER_LKPVIEWCOL),
                  (SELECT PK_LKPCOL FROM ER_LKPCOL WHERE LKPCOL_NAME='STUDYAPNDX_TYPE' and FK_LKPLIB =v_Pk_Lib),
                  'Y',
                  5,
                  v_pk_View,
                  '10%',
                  'Y'   
                );
            dbms_output.put_line('Lookup :STUDYAPNDX_TYPE in ER_LKPVIEWCOL inserted');
            else 
              dbms_output.put_line('Lookup :STUDYAPNDX_TYPE in ER_LKPVIEWCOL already exists');
            end if;
                  select count(*) into v_item_exists from ER_LKPVIEWCOL where FK_LKPCOL=(SELECT PK_LKPCOL FROM ER_LKPCOL WHERE LKPCOL_NAME='STUDYAPNDX_URI' and FK_LKPLIB=v_Pk_Lib) and FK_LKPVIEW=v_pk_View;
              if (v_item_exists = 0) then
                INSERT
              INTO ER_LKPVIEWCOL VALUES
               (
                  (SELECT (MAX(PK_LKPVIEWCOL)+1) FROM ER_LKPVIEWCOL),
                  (SELECT PK_LKPCOL FROM ER_LKPCOL WHERE LKPCOL_NAME='STUDYAPNDX_URI' and FK_LKPLIB =v_Pk_Lib),
                  'N',
                  6,
                  v_pk_View,
                  '30%',
                  'Y'   
                );
                dbms_output.put_line('Lookup :STUDYAPNDX_URI in ER_LKPVIEWCOL inserted');
            else 
              dbms_output.put_line('Lookup :STUDYAPNDX_URI in ER_LKPVIEWCOL already exists');
            end if;

  COMMIT;

end;
/