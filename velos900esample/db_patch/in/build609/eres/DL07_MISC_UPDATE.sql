set define off;

update er_codelst set codelst_seq = 5,codelst_maint = 'Y' where codelst_type='bag_type' and codelst_subtyp = 'other';

----------TO UPDATE FK_CBU_STATUS


--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'DD'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 1) then
	SELECT PK_CBU_STATUS INTO V_CBU_STATUS_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE = 'DD' AND CODE_TYPE = 'Resolution';
     UPDATE CB_CBU_STATUS SET FK_CBU_STATUS=V_CBU_STATUS_CODE WHERE CBU_STATUS_CODE = 'DD' AND CODE_TYPE = 'Response';
	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'QR'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 1) then
	SELECT PK_CBU_STATUS INTO V_CBU_STATUS_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE = 'QR' AND CODE_TYPE = 'Resolution';
     UPDATE CB_CBU_STATUS SET FK_CBU_STATUS=V_CBU_STATUS_CODE WHERE CBU_STATUS_CODE = 'QR' AND CODE_TYPE = 'Response';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'RO'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 1) then
	SELECT PK_CBU_STATUS INTO V_CBU_STATUS_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE = 'RO' AND CODE_TYPE = 'Resolution';
     UPDATE CB_CBU_STATUS SET FK_CBU_STATUS=V_CBU_STATUS_CODE WHERE CBU_STATUS_CODE = 'RO' AND CODE_TYPE = 'Response';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'SO'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 1) then
	SELECT PK_CBU_STATUS INTO V_CBU_STATUS_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE = 'SO' AND CODE_TYPE = 'Resolution';
     UPDATE CB_CBU_STATUS SET FK_CBU_STATUS=V_CBU_STATUS_CODE WHERE CBU_STATUS_CODE = 'SO' AND CODE_TYPE = 'Response';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'OT'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 1) then
	SELECT PK_CBU_STATUS INTO V_CBU_STATUS_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE = 'OT' AND CODE_TYPE = 'Resolution';
     UPDATE CB_CBU_STATUS SET FK_CBU_STATUS=V_CBU_STATUS_CODE WHERE CBU_STATUS_CODE = 'OT' AND CODE_TYPE = 'Response';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'CD'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 1) then
	SELECT PK_CBU_STATUS INTO V_CBU_STATUS_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE = 'CD' AND CODE_TYPE = 'Resolution';
     UPDATE CB_CBU_STATUS SET FK_CBU_STATUS=V_CBU_STATUS_CODE WHERE CBU_STATUS_CODE = 'CD' AND CODE_TYPE = 'Response';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'AG'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 1) then
	SELECT PK_CBU_STATUS INTO V_CBU_STATUS_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE = 'AG' AND CODE_TYPE = 'Resolution';
     UPDATE CB_CBU_STATUS SET FK_CBU_STATUS=V_CBU_STATUS_CODE WHERE CBU_STATUS_CODE = 'AG' AND CODE_TYPE = 'Response';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'DD'
    AND CODE_TYPE = 'Resolution';
  if (v_record_exists = 1) then
	SELECT PK_CBU_STATUS INTO V_CBU_STATUS_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE = 'DD' AND CODE_TYPE = 'Response';
     UPDATE CB_CBU_STATUS SET FK_CBU_STATUS=V_CBU_STATUS_CODE WHERE CBU_STATUS_CODE = 'DD' AND CODE_TYPE = 'Resolution';
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'QR'
    AND CODE_TYPE = 'Resolution';
  if (v_record_exists = 1) then
	SELECT PK_CBU_STATUS INTO V_CBU_STATUS_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE = 'QR' AND CODE_TYPE = 'Response';
     UPDATE CB_CBU_STATUS SET FK_CBU_STATUS=V_CBU_STATUS_CODE WHERE CBU_STATUS_CODE = 'QR' AND CODE_TYPE = 'Resolution';
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'RO'
    AND CODE_TYPE = 'Resolution';
  if (v_record_exists = 1) then
	SELECT PK_CBU_STATUS INTO V_CBU_STATUS_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE = 'RO' AND CODE_TYPE = 'Response';
     UPDATE CB_CBU_STATUS SET FK_CBU_STATUS=V_CBU_STATUS_CODE WHERE CBU_STATUS_CODE = 'RO' AND CODE_TYPE = 'Resolution';
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'SO'
    AND CODE_TYPE = 'Resolution';
  if (v_record_exists = 1) then
	SELECT PK_CBU_STATUS INTO V_CBU_STATUS_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE = 'SO' AND CODE_TYPE = 'Response';
     UPDATE CB_CBU_STATUS SET FK_CBU_STATUS=V_CBU_STATUS_CODE WHERE CBU_STATUS_CODE = 'SO' AND CODE_TYPE = 'Resolution';
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'OT'
    AND CODE_TYPE = 'Resolution';
  if (v_record_exists = 1) then
	SELECT PK_CBU_STATUS INTO V_CBU_STATUS_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE = 'OT' AND CODE_TYPE = 'Response';
     UPDATE CB_CBU_STATUS SET FK_CBU_STATUS=V_CBU_STATUS_CODE WHERE CBU_STATUS_CODE = 'OT' AND CODE_TYPE = 'Resolution';
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'CD'
    AND CODE_TYPE = 'Resolution';
  if (v_record_exists = 1) then
	SELECT PK_CBU_STATUS INTO V_CBU_STATUS_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE = 'CD' AND CODE_TYPE = 'Response';
     UPDATE CB_CBU_STATUS SET FK_CBU_STATUS=V_CBU_STATUS_CODE WHERE CBU_STATUS_CODE = 'CD' AND CODE_TYPE = 'Resolution';
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'AG'
    AND CODE_TYPE = 'Resolution';
  if (v_record_exists = 1) then
	SELECT PK_CBU_STATUS INTO V_CBU_STATUS_CODE FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE = 'AG' AND CODE_TYPE = 'Response';
     UPDATE CB_CBU_STATUS SET FK_CBU_STATUS=V_CBU_STATUS_CODE WHERE CBU_STATUS_CODE = 'AG' AND CODE_TYPE = 'Resolution';
	commit;
  end if;
end;
/
--END--

------------TO UPDATE DELETEDFLAG

--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'XP'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 1) then
     UPDATE CB_CBU_STATUS SET DELETEDFLAG=1 WHERE CBU_STATUS_CODE = 'XP' AND CODE_TYPE = 'Response';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'EE'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 1) then
     UPDATE CB_CBU_STATUS SET DELETEDFLAG=1 WHERE CBU_STATUS_CODE = 'EE' AND CODE_TYPE = 'Response';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'RO'
    AND CODE_TYPE = 'Response';
  if (v_record_exists = 1) then
     UPDATE CB_CBU_STATUS SET IS_LOCAL_REG=1, USA=1 WHERE CBU_STATUS_CODE = 'RO' AND CODE_TYPE = 'Response';
	commit;
  end if;
end;
/
--END--


commit;
