set define off;
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB_PROCESSING_PROCEDURES' AND column_name = 'PROC_NAME'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB_PROCESSING_PROCEDURES add PROC_NAME VARCHAR2(50 CHAR)');
  end if;
end;
/
COMMENT ON COLUMN CBB_PROCESSING_PROCEDURES.PROC_NAME IS 'Procedure Name.'; 
commit;

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB_PROCESSING_PROCEDURES_INFO' AND column_name = 'FK_NUM_OF_BAGS'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB_PROCESSING_PROCEDURES_INFO add FK_NUM_OF_BAGS NUMBER(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CBB_PROCESSING_PROCEDURES_INFO.FK_NUM_OF_BAGS IS 'Number of Bags.'; 
commit;

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB_PROCESSING_PROCEDURES_INFO' AND column_name = 'FK_BAGTYPE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB_PROCESSING_PROCEDURES_INFO add FK_BAGTYPE NUMBER(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CBB_PROCESSING_PROCEDURES_INFO.FK_BAGTYPE IS 'Bag Type'; 
commit;

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB_PROCESSING_PROCEDURES_INFO' AND column_name = 'FK_BAG_1_TYPE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB_PROCESSING_PROCEDURES_INFO add FK_BAG_1_TYPE NUMBER(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CBB_PROCESSING_PROCEDURES_INFO.FK_BAG_1_TYPE IS 'Bag Type 1'; 
commit;

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB_PROCESSING_PROCEDURES_INFO' AND column_name = 'FK_BAG_2_TYPE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB_PROCESSING_PROCEDURES_INFO add FK_BAG_2_TYPE NUMBER(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CBB_PROCESSING_PROCEDURES_INFO.FK_BAG_2_TYPE IS 'Bag Type 2'; 
commit;

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB_PROCESSING_PROCEDURES_INFO' AND column_name = 'NO_OF_VIABLE_SMPL_FINAL_PROD'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB_PROCESSING_PROCEDURES_INFO add NO_OF_VIABLE_SMPL_FINAL_PROD NUMBER(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CBB_PROCESSING_PROCEDURES_INFO.NO_OF_VIABLE_SMPL_FINAL_PROD IS 'No of viable samples not representative of the final product'; 
commit;

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB_PROCESSING_PROCEDURES_INFO' AND column_name = 'NO_OF_OTH_REP_ALLIQUOTS_F_PROD'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB_PROCESSING_PROCEDURES_INFO add NO_OF_OTH_REP_ALLIQUOTS_F_PROD NUMBER(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CBB_PROCESSING_PROCEDURES_INFO.NO_OF_OTH_REP_ALLIQUOTS_F_PROD IS 'No of other representative aliquots of the final product'; 
commit;

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CBB_PROCESSING_PROCEDURES_INFO' AND column_name = 'NO_OF_OTH_REP_ALLIQ_ALTER_COND'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CBB_PROCESSING_PROCEDURES_INFO add NO_OF_OTH_REP_ALLIQ_ALTER_COND NUMBER(19,0)');
  end if;
end;
/
COMMENT ON COLUMN CBB_PROCESSING_PROCEDURES_INFO.NO_OF_OTH_REP_ALLIQ_ALTER_COND IS 'No of other rep aliquots stored under alternate conditions'; 
commit;


set define off;
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 
'CB_CBU_STATUS' AND column_name = 'DELETEDFLAG'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_CBU_STATUS add DELETEDFLAG VARCHAR2(1BYTE)');
  end if;
end;
/
COMMENT ON COLUMN CB_CBU_STATUS.DELETEDFLAG IS 'DELETED FLAG'; 
commit;

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD'
    AND column_name = 'CORD_DONOR_IDENTI_NUM';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD ADD(CORD_DONOR_IDENTI_NUM  VARCHAR(50))';
  end if;
end;
/
COMMENT ON COLUMN ERES.CB_CORD.CORD_DONOR_IDENTI_NUM   IS 'DONOR IDENTIFICATION NUMBER (DIN)'; 

commit;
