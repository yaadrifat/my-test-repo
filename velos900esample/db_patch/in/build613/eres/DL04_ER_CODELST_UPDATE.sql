set define off;

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_status'
    AND codelst_subtyp = 'NEW';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='New' where codelst_type = 'order_status' AND codelst_subtyp = 'NEW';
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_status'
    AND codelst_subtyp = 'RESERVED';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Reserved' where codelst_type = 'order_status' AND codelst_subtyp = 'RESERVED';
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_status'
    AND codelst_subtyp = 'close_ordr';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Closed' where codelst_type = 'order_status' AND codelst_subtyp = 'close_ordr';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_status'
    AND codelst_subtyp = 'CANCELLED';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Cancelled' where codelst_type = 'order_status' AND codelst_subtyp = 'CANCELLED';
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_status'
    AND codelst_subtyp = 'PENDING';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Pending' where codelst_type = 'order_status' AND codelst_subtyp = 'PENDING';
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_status'
    AND codelst_subtyp = 'open_ordr';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Open' where codelst_type = 'order_status' AND codelst_subtyp = 'open_ordr';
	commit;
  end if;
end;
/
--END--

commit;