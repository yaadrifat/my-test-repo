set define off;

--Sql for Table CBB to add column PICKUP_ADD_CONTACT_LASTNAME, PICKUP_ADD_CONTACT_FIRSTNAME .
--Sql for Table ER_ADD to add column Add_EXT.

DECLARE
  v_column_one_exists number := 0;  
  v_column_two_exists number := 0;  
  v_column_three_exists number := 0;  
BEGIN
  Select count(*) into v_column_one_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'PICKUP_ADD_CONTACT_LASTNAME'; 
 Select count(*) into v_column_two_exists from user_tab_cols where TABLE_NAME = 
'CBB' AND column_name = 'PICKUP_ADD_CONTACT_FIRSTNAME'; 
Select count(*) into v_column_three_exists from user_tab_cols where TABLE_NAME =
'ER_ADD' AND column_name ='ADD_EXT';
  if (v_column_one_exists = 0) then
      execute immediate ('alter table cbb add PICKUP_ADD_CONTACT_LASTNAME varchar2(30)');
      
  end if;
   if (v_column_two_exists = 0) then
      execute immediate ('alter table cbb add PICKUP_ADD_CONTACT_FIRSTNAME varchar2(30)');
  end if;
   if (v_column_three_exists = 0) then
      execute immediate ('alter table ER_ADD add ADD_EXT varchar2(20)');
  end if;
end;
/
COMMENT ON COLUMN CBB.PICKUP_ADD_CONTACT_LASTNAME IS 'Stores Last name of Contact.'; 
COMMENT ON COLUMN CBB.PICKUP_ADD_CONTACT_FIRSTNAME IS 'Stores First name of Contact.'; 
COMMENT ON COLUMN ER_ADD.Add_EXT IS 'Stores Extention of phone.'; 
commit;

--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS  where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_19')
    AND CONDITION_TYPE = 'PRE' AND TABLE_NAME='CB_CORD' AND COLUMN_NAME='CORD_NMDP_STATUS';
  if (v_record_exists = 1) then
     UPDATE CB_ALERT_CONDITIONS SET END_BRACE='',LOGICAL_OPERATOR='OR' WHERE FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_19') AND CONDITION_TYPE = 'PRE' AND TABLE_NAME='CB_CORD' AND COLUMN_NAME='CORD_NMDP_STATUS';
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_19')
    AND CONDITION_TYPE = 'PRE'
    AND TABLE_NAME='CB_CORD'
    AND COLUMN_NAME='INFUSION_FLAG';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,KEY_CONDITION,END_BRACE) 
  VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_19'),'PRE','CB_CORD','INFUSION_FLAG','''Y''','=','PK_ORDER = #keycolumn',')');
	commit;
  end if;
end;
/
--END--


delete from cb_alert_conditions where pk_alert_conditions in( select max(pk_alert_conditions) from cb_alert_conditions group by fk_codelst_alert,condition_type,table_name,column_name,condition_value having count(pk_alert_conditions)>1);
commit;


--STARTS ADDING COLUMN DATA_MODIFIED_FLAG TO ER_ORDER TABLE
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'ER_ORDER' AND COLUMN_NAME = 'DATA_MODIFIED_FLAG';
IF (v_column_exists = 0) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE ER_ORDER ADD(DATA_MODIFIED_FLAG VARCHAR2(2 BYTE))';
END IF;
END;
/
--END


COMMENT ON COLUMN ER_ORDER.DATA_MODIFIED_FLAG IS 'Stores ''Y'' every time er_order is updated through ESB';


--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_20')
    AND CONDITION_TYPE = 'PRE'
    AND TABLE_NAME='ER_ORDER'
    AND COLUMN_NAME='DATA_MODIFIED_FLAG';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,KEY_CONDITION) 
  VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_20'),'PRE','ER_ORDER','DATA_MODIFIED_FLAG','''Y''','=','PK_ORDER = #keycolumn');
	commit;
  end if;
end;
/
--END--



