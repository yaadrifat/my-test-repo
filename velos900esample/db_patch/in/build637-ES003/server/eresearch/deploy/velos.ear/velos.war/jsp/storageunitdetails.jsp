<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true" />
<html>
<head>
<title><%=MC.M_MngInv_StrgUnitDets%><%--Manage Inventory >> Storage Unit Details*****--%></title>
<SCRIPT LANGUAGE="JavaScript">
	    var specimenValues ;
	    var storageValues ;
        function SpecExistCkeck(formobj,total)
		{
            var paramArray = [total];
            alert(getLocalizedMessageString("M_StrgSpec_CntChg",paramArray));/*alert("Storage contains ("+total+") specimens and can't be change to single storage");*****/
			document.getElementById('vmultiSpecimen').checked=true;
		}

		function validateName(storageName)
		{
			if(storageName.value == ''||storageName.value==null )
			{
				alert("<%=MC.M_StorName_CntBlank%>");/*alert("Storage Name cannot be left blank");*****/
				storageName.focus();
				return false;
			}
			if (isWhitespace(storageName.value))
			{
				alert("<%=MC.M_Etr_ValidData%>");/*alert("Please enter valid data");*****/
				storageName.focus();
				return false;
			}
		}

       function setSelAllValues() {
            var disFlag = false;
            specimenValues = new Array(storageunit.aiSpecimenType.length);
	        for (var x=0;x<document.storageunit.aiSpecimenType.length;x++) {
	             specimenValues[x] = document.storageunit.aiSpecimenType[x].checked ;
	         }

	 // alert("storageunit.aiStorageType.length" + storageunit.aiStorageType.length);
	       storageValues = new Array(storageunit.aiStorageType.length);
	       for (var x=0;x<document.storageunit.aiStorageType.length;x++) {

	            //alert("storageunit.a" +  document.storageunit.aiStorageType[x].checked);

	            storageValues[x] = document.storageunit.aiStorageType[x].checked ;

	       }

	       for (var x=0;x<document.storageunit.aiStorageType.length;x++) {
	          if(document.storageunit.aiSpecimenType[x].checked == false ){
	             disFlag = true;
	             break;
	          }
	      }
	      if(disFlag == true) {
	          document.storageunit.All.checked = false;
	       }
	       else {
	          document.storageunit.All.checked = true;
	       }

	     //JM: 21Jan2008: Parent storage is 'Kit' so children should not be created manually by changing the dimension

      		var storParentType = document.storageunit.storageParentType.value;

			if (storParentType=='Kit'){

				document.storageunit.dimA.disabled = true
				document.storageunit.dimB.disabled = true
			}
}

function setValue(formobj) {
	formobj.location.value = "";
	formobj.mainFKStorage.value ="";
}

 function f_edit(selctedbox,formobj,childpk)
{
	if (selctedbox.checked)
	{
		document.getElementById('name_'+childpk).style.display='none';
		document.getElementById(childpk + '_name_e').style.display='block';
		document.getElementById('type_'+childpk).style.display='none';
		document.getElementById(childpk + '_type_e').style.display='block';
	}
	else
	{

		document.getElementById('name_'+childpk).style.display='block';
		document.getElementById(childpk + '_name_e').style.display='none';
		document.getElementById('type_'+childpk).style.display='block';
		document.getElementById(childpk + '_type_e').style.display='none';
	}
}

function openLocLookup(formobj) {

	//KM-#3279
	mode = formobj.mode.value;
	if (formobj.template.checked == true &&  mode=='M') {
			alert("<%=MC.M_CntSelLoc_ForTemplate%>");/*alert("You cannot select a location for a Template");*****/
			return false;
	 }

	/*formobj.target="Lookup";
	formobj.method="post";
	formobj.action="multilookup.jsp?viewId=6050&form=storageunit&seperator=,"+
                  "&keyword=location|storage_name~strCoordinateX|storage_coordinate_x|[VELHIDE]~strCoordinateY|storage_coordinate_y|[VELHIDE]~mainFKStorage|pk_storage|[VELHIDE]&maxselect=1";

	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="_self";
	formobj.action="updatestorageunitdetails.jsp";
	void(0);*/

	windowName= window.open("searchLocation.jsp?gridFor=ST&form=storageunit&locationName=location&coordx=strCoordinateX&coordy=strCoordinateY&storagepk=mainFKStorage","Grid","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=900,height=650");
	windowName.focus();
}

function openGridView(formobj, pkS) {
	windowName= window.open("searchLocation.jsp?gridFor=ST&form=storageunit&locationName=location&coordx=strCoordinateX&coordy=strCoordinateY&storagepk=mainFKStorage&explorer=1&pkStorage="+pkS,"Grid","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=900,height=650");
	windowName.focus();
}

</SCRIPT>
</head>
<body>
<SCRIPT language="javascript">

function checkAll(formobj) {


   if(formobj.All.checked == true ){
     	for (var x=0;x<formobj.aiSpecimenType.length;x++){
	         formobj.aiSpecimenType[x].checked = true;
	    }
	    for (var x=0;x<formobj.aiStorageType.length;x++){
	        formobj.aiStorageType[x].checked = true;
	 }
   }
    else{
	    for (var x=0;x<formobj.aiSpecimenType.length;x++){
	       formobj.aiSpecimenType[x].checked = false;
	    }
	   for (var x=0;x<formobj.aiStorageType.length;x++){
	      formobj.aiStorageType[x].checked = false;
	    }
      }
   }


//KM-to fix the issue 3049
function change(formobj) {

	//KM-3944
	if (formobj.dimA.value.indexOf('+') != -1 || isNaN(formobj.dimA.value) ==true || fnTrimSpaces(formobj.dimA.value)=='' || (formobj.dimA.value < 1) ) {
		alert("<%=MC.M_Valid_PositiveNum%>");/*alert("Please enter a Valid Positive Number");*****/
	    formobj.dimA.focus();
		return false;
	}

	if (formobj.dimB.value.indexOf('+') != -1 || isNaN(formobj.dimB.value) ==true || fnTrimSpaces(formobj.dimB.value)=='' || (formobj.dimB.value < 1)) {
		alert("<%=MC.M_Valid_PositiveNum%>");/*alert("Please enter a Valid Positive Number");*****/
	    formobj.dimB.focus();
		return false;
	}

	//formobj.storageUnitCap.value = formobj.dimB.value * formobj.dimA.value;

}


function openGrid(location)
{
dimA=document.getElementById('dimA').value;
dimB=document.getElementById('dimB').value;
nameConvA=document.getElementById('nameConvA').value;
nameConvB=document.getElementById('nameConvB').value;
posA=document.getElementById('posA').value;
posB=document.getElementById('posB').value;
width="550";
height="560";
if (dimA>25) width="800";
if (dimB>25) height="800";
windowName= window.open("specgrid.jsp?dimA="+dimA+"&dimB="+dimB+"&nameConvA=" +nameConvA + "&nameConvB=" + nameConvB+"&posA="+posA+"&posB="+posB+"&location="+location,"Grid","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width="+width+",height="+height);
windowName.focus();
}

function  validate(formobj){


	    var disFlagsp = false;
        var disFlagst = false;

      for (var x=0;x<formobj.aiSpecimenType.length;x++){
	     if (formobj.aiSpecimenType[x].checked == false) {
	      disFlagsp = true;
	     }
	   else {
	       disFlagsp = false;
	       break;
	    }
	 }

     for (var x=0;x<formobj.aiStorageType.length;x++) {
	  if (formobj.aiStorageType[x].checked == false) {
	   disFlagst = true;
	 }
	  else {
	    disFlagst = false;
	    break;
	  }
	}
       if(disFlagsp == true && disFlagst == true) {
          alert("<%=MC.M_SelAtleast_OneContents%>");/*alert("Please select atleast one allowed contents");*****/
	      return false;
       }

	 if (!(validate_col('Storage Name',formobj.storageName))) return false;

     if (!(validate_col('Storage Type',formobj.storetype))) return false;


	 if(formobj.storageStatus) {
  	      if (!(validate_col('Status',formobj.storageStatus))) return false;
	 }

	 if(formobj.statDate) {
	  	    if (!(validate_col('Status Date',formobj.statDate))) return false;
  	    if (!(validate_date(formobj.statDate))) return false;
	 }

  if((fnTrimSpaces(formobj.storageUnitCap.value) == '') && (document.getElementById('vmultiSpecimen').checked==true) )
  {
	if (confirm("<%=MC.M_Unlimited_Storage%>"))/*if (confirm("Storage will be unlimited as the capacity is not defined"))*****/
	   {
		formobj.submit();
	   } else
	   {
		formobj.storageUnitCap.focus();
		 return false;
	   }
	}
	else if(formobj.storageUnitCap.value.indexOf('+') != -1 || isNaN(formobj.storageUnitCap.value) == true || fnTrimSpaces(formobj.storageUnitCap.value) == '' || (formobj.storageUnitCap.value < 1) ){
		alert("<%=MC.M_Valid_PositiveNum%>");/*alert("Please enter a Valid Positive Number");*****/
		formobj.storageUnitCap.focus();
		return false;
	}
	if (formobj.dimA.value.indexOf('+') != -1 || isNaN(formobj.dimA.value) ==true || fnTrimSpaces(formobj.dimA.value)=='' || (formobj.dimA.value < 1) ) {
		alert("<%=MC.M_Valid_PositiveNum%>");/*alert("Please enter a Valid Positive Number");*****/
		formobj.dimA.focus();
		return false;
	}

	if (formobj.dimB.value.indexOf('+') != -1 || isNaN(formobj.dimB.value) ==true || fnTrimSpaces(formobj.dimB.value)=='' || (formobj.dimB.value < 1)) {
		alert("<%=MC.M_Valid_PositiveNum%>");/*alert("Please enter a Valid Positive Number");*****/
		formobj.dimB.focus();
		return false;
	}

	//KM-091307
	 mode = formobj.mode.value;

	if (mode=='N')
	{
		if (parseInt(formobj.dimB.value) * parseInt(formobj.dimA.value ) > 1)
		{
		    	if (!(validate_col('Child Storage Type',formobj.childstoretype)))
		    	{
		    		return false;
		    	}
		}
	}

     if (!(validate_col('e-Signature',formobj.eSign))) return false;

	 if(mode=='M') {//KM-Confirmation message change.
        // IH for 3943
        if((formobj.oldDimA.value !=formobj.dimA.value) || (formobj.oldDimB.value!=formobj.dimB.value) ||
        		(formobj.oldNameConvA.value != formobj.nameConvA.value) || (formobj.oldNameConvB.value != formobj.nameConvB.value) ||
                (formobj.oldChildstoretype.value != formobj.childstoretype.value) ) {
            var msg=MC.M_ChgStrg_DelChildStrgCont;/*var msg="You have changed the Storage Grid details. This may delete the child storages linked with the selected storage unit. Do you want to continue?";*****/
		    if (!confirm(msg)) {
		        return false;
		    }
	    	if (!(validate_col('Child Storage Type',formobj.childstoretype)))
	    	{
	    		return false;
	    	}
	    }
	 }

 	//Added by Manimaran for location checking for template.

	var loc = formobj.location.value;//KM
	if (formobj.template.checked == true) {
		if(loc !='') {
			alert("<%=MC.M_TemplateCntAssoc_WithLoc%>");/*alert("A Template cannot be associated with Location");*****/
			formobj.location.focus();
			return false;
		}
	}

  		var selectedItem;
	 	var subtyp;

	 	selectedItem  =  formobj.storetype.selectedIndex;
		subtyp = formobj.codeSubTyp[selectedItem].value;

	//JM: 22Jan2008: INV # 3.2.8 Manage Inventory > Storage Kits :  A Kit cannot have a location.
	 	if(loc !='') {

			if (subtyp=='Kit'){
				alert("<%=MC.M_StorKitCnt_HaveLoc%>");/*alert("A Storage Kit cannot have a location");*****/
				formobj.location.focus();
				return false;
			}
		}

		if (formobj.template.checked == false && subtyp=='Kit' )
		{
		  alert("<%=MC.M_PlsChkStrUnits_StrKit%>");/*alert("Please Check 'This Storage Unit is a Template' for the Storage Kit");*****/
		  return false;
		}

	 if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
     }
	 
	 // BT: For INV 21675

	  if(mode=='N' || mode=='M'){
 	if(document.getElementById('vmultiSpecimen').checked == true && (formobj.capunit.options[formobj.capunit.selectedIndex].text =='Items')) 
	{
		var StrgCnt=0;
		var StrgCld=0;
		StrgCld=document.getElementById('StorageChildCount').value;
		StrgCnt = document.getElementById('StorageCount').value;
		var UnitCap = formobj.storageUnitCap.value;
		UnitCap = UnitCap.trim();
			if(UnitCap!='') // something is input
			{
			 var total=parseInt(StrgCnt)+parseInt(StrgCld);
			 var Multiple = formobj.dimB.value * formobj.dimA.value;
			 	if(UnitCap<(total)){
			 		var paramArray = [StrgCnt,StrgCld,total,total];
		            alert(getLocalizedMessageString("M_PrevStrg_CapGtr",paramArray));/*alert("Previous Storage contains ("+StrgCnt+") specimens and ("+StrgCld+") childs. Please assign capacity ("+total+") or greater than ("+total+")");*****/
				
					formobj.storageUnitCap.focus();
					return false;	
				}
				if(UnitCap<Multiple) // 4<3x3
				{
					var paramArray = [UnitCap,formobj.dimA.value,formobj.dimB.value,Multiple];
		            alert(getLocalizedMessageString("M_ParentStrg_ChldStrgDims",paramArray));/*alert("Parent storage capacity("+UnitCap+") cannot be less then child storage dimensions ("+formobj.dimA.value+" x "+formobj.dimB.value+" = "+Multiple+")");*****/
					
					formobj.storageUnitCap.focus();
					return false;
				}
				else if(UnitCap==Multiple) // is same 9/3x3
				{
					var ddEle = document.getElementById('storageStatus');
					for ( var i = 0; i < ddEle.options.length; i++ ) 
					{	
						if ( ddEle.options[i].text == 'Occupied' ) 
						{
							ddEle.options[i].selected = true;
							break;
						}
					}
				}
			}
	}
	else
	{
		formobj.storageUnitCap.value = formobj.dimB.value * formobj.dimA.value;
	}
	}
}

function openUserWindow(frm) {
		windowName = window.open("usersearchdetails.jsp?fname=&lname=&from="+ frm,"TEST","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
		windowName.focus();
}

function openStudyWindow(formobj) {
	formobj.target="Lookup";
	formobj.method="post";
	formobj.action="multilookup.jsp?viewId=6013&form=storageunit&seperator=,"+
                  "&keyword=selStudy|STUDY_NUMBER~selStudyIds|LKP_PK|[VELHIDE]&maxselect=1";

	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');

	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="_self";
	formobj.action="updatestorageunitdetails.jsp";
	void(0);
}


function openWin(pkStorage,defaultStudyID,pkStorageStatus,mode,pgright, latestStatSubTye)
{
		if (!f_check_perm(pgright,'E'))
		{
			return false;
		}

     	param = "storagestatus.jsp?pkStorage="+pkStorage+"&def_study_Id="+defaultStudyID+"&pkStorageStatus="+pkStorageStatus+"&mode="+mode+"&latestStatSubTye="+latestStatSubTye;
		windowName= window.open(param,"_new","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=false,width=800,height=350");
		windowName.focus();
}


//Added by Manimaran for deletion of Storage status.
function f_delete(pkStorStat,len,pgright)
{
	if (!f_check_perm(pgright,'E'))
		{
			return false;
		}

	if (len >1) {
	   if (confirm("<%=MC.M_DoWantDel_StrgStat%>"))/*if (confirm("Do you want to delete this storage status?"))*****/
	   {
			windowName= window.open("storagestatusdelete.jsp?pkStorStat="+pkStorStat,"statdel","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=650,height=350");
			windowName.focus();
	   } else
	   {
		 return false;
	   }
    }
	else
	{
		alert("<%=MC.M_CntDel_LastStat%>");/*alert("You can not delete the last/only status for a Storage Unit");*****/
	}
}

</SCRIPT>
<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="srctdmenubaritem5" />
</jsp:include>

<%@ page language="java"
	import="java.util.*,com.velos.eres.service.util.StringUtil,com.velos.eres.business.common.*,com.velos.eres.web.storageAllowedItems.* ,com.velos.eres.web.grpRights.GrpRightsJB"%>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*"%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="storageB" scope="request" class="com.velos.eres.web.storage.StorageJB" />
<jsp:useBean id="StorageStatJB" scope="request" class="com.velos.eres.web.storageStatus.StorageStatusJB" />
<jsp:useBean id="stdJB" scope="request" class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="usrJB" scope="request" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="codeLstB" scope="page" class="com.velos.eres.web.codelst.CodelstJB" />


<div class="tabDefTopN" id="div1">
<%

HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {

	 int pageRight = 0;

	 GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

	 pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSTORAGE"));

String mode= "";
String storageId = "";
String storageName = "";
String storageType = "";
String notes ="";
String location = "";
String pkStorage ="";
String dim1Naming ="";
String dim1Order ="";
String dim2Naming ="";
String dim2Order ="";
String capNum ="1";
String capUnit ="";
String dim1CellNum="";
String dim2CellNum="";
String strCoordinateX ="";
String strCoordinateY ="";
String childStorageType ="";
String  stdy = "", studyNumber = "";
String  user = "", userName ="";
String  modFlg = "";
String capUnitDesc ="";
String template = "";
String multiSpecimen="";
String templateTyp = "", alternateId="", storageUnitClas="";
int  StorageCount = 0,StorageChildCount=0;
String storageParentIsKit = "";
String disableDim="";
ArrayList<Integer> fkCodelstStorageType  = new ArrayList<Integer>();
ArrayList<Integer> fkCodelstSpecimenType = new ArrayList<Integer>();
ArrayList allowedItemTypes  = new ArrayList();
ArrayList pkAllowedItems    = new ArrayList();
ArrayList fkStorages        = new ArrayList();

String mainFKStorage = "";
String mainParentStorageName = "";

mode = request.getParameter("mode");
String acc = (String) tSession.getValue("accountId");

   if(mode.equals("M")) {
	pkStorage = request.getParameter("pkStorage");
	storageB.setPkStorage(EJBUtil.stringToNum(pkStorage));
	storageB.getStorageDetails();
	StorageCount=storageB.getStorageCount(pkStorage);
	StorageChildCount=storageB.getChildCount(pkStorage);
	storageId = storageB.getStorageId();
	storageName = storageB.getStorageName();
	storageType = storageB.getFkCodelstStorageType();
	capNum = storageB.getStorageCapNum();
	capUnit = storageB.getFkCodelstCapUnits();
	capUnitDesc = codeLstB.getCodeDescription(EJBUtil.stringToNum(capUnit));
	dim1CellNum = storageB.getStorageDim1CellNum();
	dim1Naming = storageB.getStorageDim1Naming();
	dim1Order = storageB.getStorageDim1Order();
	dim2CellNum = storageB.getStorageDim2CellNum();
	dim2Naming = storageB.getStorageDim2Naming();
	dim2Order = storageB.getStorageDim2Order();
	notes = storageB.getStorageNotes();
	strCoordinateX = storageB.getStorageCoordinateX();
	strCoordinateY = storageB.getStorageCoordinateY();
	childStorageType = storageB.getFkCodelstChildStype();
	childStorageType = (childStorageType==null)?"":childStorageType;


	mainFKStorage = storageB.getFkStorage();

	mainParentStorageName = storageB.getParentstorageName();

	template = storageB.getTemplate();//KM
	multiSpecimen = storageB.getMultiSpecimenStorage();

	if (StringUtil.isEmpty(template))
	{
		template = "0";
	}
	if (StringUtil.isEmpty(multiSpecimen))
	{

		multiSpecimen = "0";
	}


 		//JM: 21Jan2008: find out if the parent type is a 'Kit'
		storageParentIsKit = storageB.findParentStorageType(pkStorage);
		if (storageParentIsKit==null)
		storageParentIsKit = "";

		if (storageParentIsKit.equals("Kit"))
		{
			disableDim = " disabled ";
		}

	capNum = (capNum == null)?"":(capNum);
	capUnit = (capUnit == null)?"":(capUnit);
	dim1CellNum = (dim1CellNum == null)?"":(dim1CellNum);
	dim1Naming = (dim1Naming == null)?"azz":(dim1Naming);
	dim1Order = (dim1Order == null)?"":(dim1Order);
	dim2CellNum = (dim2CellNum == null)?"":(dim2CellNum);
	dim2Naming = (dim2Naming == null)?"1n":(dim2Naming);
	dim2Order = (dim2Order == null)?"":(dim2Order);
	notes = (notes == null)?"":(notes);

  	 if (StringUtil.isEmpty(strCoordinateX))
  	 {
  	 	strCoordinateX = "";
  	 }


  	 if (StringUtil.isEmpty(strCoordinateY))
  	 {
  	 	strCoordinateY = "";
  	 }

	if(dim1CellNum.equals(""))
		dim1CellNum="1";
	if(dim2CellNum.equals(""))
		dim2CellNum="1";
	//if(capNum.equals(""))
	//	capNum="1";


 	 StorageAllowedItemsJB storageAllowedItemJB  = new StorageAllowedItemsJB();
     StorageAllowedItemsDao strAllowedItemsdao=storageAllowedItemJB.getStorageAllowedValue(EJBUtil.stringToNum(pkStorage));

	 pkAllowedItems = strAllowedItemsdao.getPkAllowedItems();
	 fkStorages = strAllowedItemsdao.getFkStorages();
	 fkCodelstSpecimenType=strAllowedItemsdao.getFkCodelstSpecimenTypes();
	 fkCodelstStorageType=strAllowedItemsdao.getFkCodelstStorageTypes();
	 allowedItemTypes=strAllowedItemsdao.getAllowedItemTypes();

	 tSession.setAttribute("specimenlist",fkCodelstSpecimenType);
	 tSession.setAttribute("storagelist",fkCodelstStorageType);

	 //JM: 30Jul2009: #INVP2.15
	 templateTyp = storageB.getStorageTemplateType();

	 alternateId = storageB.getStorageAlternateId();
	 alternateId = (alternateId == null)?"":alternateId;

	 storageUnitClas = storageB.getStorageUnitClass();

}

 String dStore = "", dTemplateType="", dStorageUnitClass="";
 String dCapUnit ="";
 String dstorageStatus ="";
 String dChildStore ="";
 CodeDao cd1 = new CodeDao();
 CodeDao cd2 = new CodeDao();
 CodeDao cd3 = new CodeDao();
 CodeDao cd4 = new CodeDao();

 //JM: 28Sep2009: #4302
	//cd1.getCodeValues("store_type");
	  cd1.getCodeValuesSaveKit("store_type");

 dStore = cd1.toPullDown("storetype");
 cd2.getCodeValues("cap_unit");
 cd3.getCodeValues("storage_stat");

 //JM: 28Sep2009: #4302
	//cd4.getCodeValues("store_type");
	  cd4.getCodeValuesSaveKit("store_type");

 dChildStore = cd4.toPullDown("childstoretype");

 int defaultItem = 0;
 int defaultStat =0;
 CodeDao cd = new CodeDao();
 defaultItem = cd.getCodeId("cap_unit","Items");
 dCapUnit = cd2.toPullDown("capunit",defaultItem);

 CodeDao cd5 =new CodeDao();
 defaultStat = cd5.getCodeId("storage_stat","Available");
 dstorageStatus=cd3.toPullDown("storageStatus",defaultStat);

 //JM: 30Jul2009: #INVP2.15
 CodeDao cdTmplt = new CodeDao();
 cdTmplt.getCodeValues("templatetype");
 dTemplateType = cdTmplt.toPullDown("templatetype");
 CodeDao cdStorUnitClass = new CodeDao();
 cdStorUnitClass.getCodeValues("stor_unit_class");
 dStorageUnitClass = cdStorUnitClass.toPullDown("storageunitclass");

//JM: 19JAN2010, #4638
 int def_studyId=0;
 def_studyId= StorageStatJB.getDefStatStudyId(EJBUtil.stringToNum(pkStorage));

%>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td colspan="4"><jsp:include page="inventorytabs.jsp" flush="true">
			<jsp:param name="selectedTab" value="2" />
		</jsp:include></td>
	</tr>
</table>

</div>
<div class="tabDefBotN" id="div2">

<Form name="storageunit" id="storageunitdet" method="post" action="updatestorageunitdetails.jsp" onSubmit="if (validate(document.storageunit)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<input type="hidden" name="mode" value=<%=mode%>> 
<input type="hidden" name="defStatusCode" value=<%=defaultStat%>> 
<input type="hidden" name="pkStorage" value=<%=pkStorage%>> 
<input type="hidden" name="storageParentType" value="<%=storageParentIsKit%>">
<input type="hidden" id="StorageCount" value=<%=StorageCount%>>
<input type="hidden" id="StorageChildCount" value=<%=StorageChildCount%>>

 <%
	//JM: 22Jan2008: INV # 3.2.8 Manage Inventory > Storage Kits: h. A Kit cannot have a location.

		ArrayList codeSubTypes = new ArrayList();
		codeSubTypes =  cd1.getCSubType();
		// added by sonia, if storageType is empty, then add a dummy row to balance code subtypes

		if (EJBUtil.stringToNum(storageType) == 0 && mode.equals("M"))
		{
			%> <input type=hidden name=codeSubTyp value=''> <%
		}

		if (codeSubTypes != null )
		{
			for ( int k=0; k < codeSubTypes.size(); k++)
			{
			 	%> <input type=hidden name=codeSubTyp value='<%=codeSubTypes.get(k)%>'> <%
			}
		}

  %>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td colspan="2"><%=MC.M_LveStorIDBlk_ForSysID%><%--Leave 'Storage ID' field blank for system auto-generated ID*****--%></td>
	</tr>

	<tr>
		<td width="68%">
		<table class="basetbl" width="100%" border="0" cellspacing="0"
			cellpadding="0">
			<tr>
				<td width="40%"><b><%=LC.L_Storage_Id%><%--Storage ID*****--%></b></td>
				<td><Input type="text" name="storageId" value="<%=storageId%>" size=15 MAXLENGTH=100></td>
			</tr>
			<tr>
				<td align="center"><%=LC.L_Storage_Name%><%--Storage Name*****--%><FONT class="Mandatory">* </FONT></td>
				<td><Input type="text" name="storageName" value="<%=storageName%>" size=25 MAXLENGTH=250 align=right></td>
			</tr>

			<tr>

				<% if (mode.equals("N")){ %>

				<td align="center"><%=LC.L_Storage_Type%><%--Storage Type*****--%><FONT class="Mandatory">* </FONT></td>
				<td><%=dStore%></td>

				<%}else {
				dStore = cd1.toPullDown("storetype",EJBUtil.stringToNum(storageType));
				%>
				<td align="center"><%=LC.L_Storage_Type%><%--Storage Type*****--%><FONT class="Mandatory">* </FONT></td>
				<td><%=dStore%></td>
				<%}%>
			</tr>

			<!--JM: 31Jul2009: Enh #INVP2.15-part1-->
			<tr>

				<% if (mode.equals("N")){ %>

				<td align="center"><%=LC.L_Template_Type%><%--Template Type*****--%>
				</td>
				<td><%=dTemplateType%></td>

				<%}else {
			dTemplateType= cdTmplt.toPullDown("templatetype",EJBUtil.stringToNum(templateTyp));

		%>
				<td align="center"><%=LC.L_Template_Type%><%--Template Type*****--%>
				</td>
				<td><%=dTemplateType%></td>
				<%}%>
			</tr>
			<tr>
				<td align="center"><%=LC.L_Alternate_Id%><%--Alternate ID*****--%></td>
				<td><Input type="text" name="alternate_Id" value="<%=alternateId%>" size=25 MAXLENGTH=500 align=right></td>
			</tr>
			<tr>

				<% if (mode.equals("N")){ %>
				<td align="center"><%=LC.L_Storage_UnitClass%><%--Storage Unit Class*****--%>
				</td>
				<td><%=dStorageUnitClass%></td>
				<% }else {
			dStorageUnitClass = cdStorUnitClass.toPullDown("storageunitclass",EJBUtil.stringToNum(storageUnitClas));	%>
				<td align="center"><%=LC.L_Storage_UnitClass%><%--Storage Unit Class*****--%>
				</td>
				<td><%=dStorageUnitClass%></td>
				<%}%>

			</tr>
			<!--JM: 31Jul2009: Enh #INVP2.15-part1-->
		</table>
		</td>
		<td width="50%">
		<table>
			<%
        StorageDao strageDaoForSpec = storageB.getSpecimensAndPatients(EJBUtil.stringToNum(pkStorage),
        EJBUtil.stringToNum(acc));
        ArrayList specimenList = strageDaoForSpec.getSpecimens();
        ArrayList specimenPatientList = strageDaoForSpec.getSpecimenPatients();

        boolean hasContent = false;
        if (specimenList != null && specimenList.size() > 0) {
            hasContent = true;
        }
        %>

			<% if (mode.equals("M")) { %>
			<tr>
				<td width="30%"><b><%=LC.L_Content%><%--Content*****--%></b></td>
				<td>

				<%   if (hasContent) { %> <%  if (specimenList.size() == 1) {  %> 
				<a href="specimendetails.jsp?mode=M&pkId=<%=specimenList.get(0)%>"><%=LC.L_Specimen%><%--Specimen*****--%></a>

				<%       if (specimenPatientList != null && specimenPatientList.size() == 1 &&
                 ((Integer)specimenPatientList.get(0)).intValue() > 0) {  %>
				&nbsp;<%=LC.L_Of_Lower%><%--of*****--%>&nbsp; <a href="patientdetails.jsp?mode=M&srcmenu=tdmenubaritem5&selectedTab=1&page=patient&pkey=<%=specimenPatientList.get(0)%>"><%=LC.L_Patient%><%--<%=LC.Pat_Patient%>*****--%></a>
				<% } %> <% } else { %> 
				<a href="specimenbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=1&mainFKStorage=<%=EJBUtil.stringToNum(pkStorage)%>"><%=LC.L_Specimen%><%--Specimen*****--%></a>
				<% } %>
				<% } else { %> 
				<%=LC.L_None%><%--None*****--%> <% }%>
				</td>
			</tr>
			<% }  %>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="30%"><b><%=LC.L_Notes%><%--Notes*****--%> </b></td>
				<td><textarea name="notes" rows=4 cols=30 MAXLENGTH=20000><%=notes%></textarea></td>
			</tr>
		</table>
		</td>
	</tr>

	<tr>
		<td width="50%">
		<table>
			<tr>
				<td width="30%"><b><%=LC.L_Location%><%--Location*****--%>
				</b></td>
				<td><Input type="text" name="location" value="<%=mainParentStorageName%>" size=25 MAXLENGTH=250 READONLY>
				<Input type="hidden" name="mainFKStorage" value="<%=mainFKStorage == null ? "" : mainFKStorage%>" size=25
					MAXLENGTH=250 align=right> <Input type="hidden" name="strCoordinateX" value="<%=strCoordinateX%>" size=25
					MAXLENGTH=250 align=right> <Input type="hidden" name="strCoordinateY" value="<%=strCoordinateY%>" size=25
					MAXLENGTH=250 align=right> <A href="javascript:void(0);" onClick="return openLocLookup(document.storageunit)"><%=LC.L_Select%><%--Select*****--%></A>&nbsp;
				<A href="#" onClick="setValue(document.storageunit)"> <%=LC.L_Remove%><%--Remove*****--%></A>
				</td>

			</tr>

			<% if ("M".equals(mode)) { %>

			<tr>
				<td colspan="2"><A href="javascript:void(0);" onClick="return openGridView(document.storageunit,<%=EJBUtil.stringToNum(pkStorage)%>)"><%=LC.L_Storage_HierarchyView%><%--Storage Hierarchy View*****--%></A></td>
			</tr>
			<% } %>

		</table>
		</td>
		<td width="50%">

		<table>
			<tr>
				<td width="40%"><b><%=LC.L_Storage_Capacity%><%--Storage Capacity*****--%>&nbsp;&nbsp;</b><Input
					type="text" id="storageUnitCap" name="storageUnitCap" size=8 value="<%=capNum%>"> <% if (mode.equals("N")) {%> <%=dCapUnit%>

				<%} else {
	dCapUnit = cd2.toPullDown("capunit",EJBUtil.stringToNum(capUnit));

	%> <%=dCapUnit%> <% } %> <%

		   //Panjvir:multispecimen field added.
		   String SpeccheckStr ="";
		   if(multiSpecimen.equals("1") && mode.equals("M")) {
			   SpeccheckStr ="checked";
		   }
		   else
			   SpeccheckStr ="";
			%> <% if (mode.equals("N")) {%>
				</td>
				<td width="40%" align="right"><input type="checkbox" name="multiSpecimen" id="vmultiSpecimen" <%=SpeccheckStr%>><%=MC.M_CanStore_MultiSpmen%></td>
			</tr>
			<%} else 
	{
	if(SpeccheckStr.equals("checked")){
	if(StorageCount>1){
	%>
			</td>
			<td width="40%" align="right"><input type="checkbox" name="multiSpecimen" id="vmultiSpecimen" <%=SpeccheckStr%>	onchange="SpecExistCkeck(document.storageunit,<%=StorageCount%>)">Can Store Multiple Specimen</td></tr>
			<% }else{%>
			</td>
			<td width="40%" align="right"><input type="checkbox" name="multiSpecimen" id="vmultiSpecimen" <%=SpeccheckStr%>><%=MC.M_CanStore_MultiSpmen%></td>
			</tr>
			<%}%>
			<%}else{%>
			</td>
			<td width="40%" align="right"><input type="checkbox" name="multiSpecimen" id="vmultiSpecimen" <%=SpeccheckStr%>><%=MC.M_CanStore_MultiSpmen%></td>
			</tr>
	<%}%>
<%}%>

	<%
		   //KM:Template field added.
		   String checkStr ="";
		   if(template.equals("1") && mode.equals("M")) {
			   checkStr ="checked";
		   }
		   else
			   checkStr ="";
			%>
			<tr>
				<td><input type="checkbox" name="template" <%=checkStr%>>
				<%=MC.M_StorUnit_IsTemplate%><%--This Storage Unit is a Template*****--%>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

			</tr>
		</table>
		</td>
	</tr>
</table>

<table width="99%">
	<tr>
		<td colspan="2">
		<table width="99%" border="1">
			<tr>
				<td width="25%" align="right"><b><%=LC.L_Allowed_Contents%><%--Allowed Contents*****--%></b></td>
				<td>(<input type="checkbox" name="All" onClick="checkAll(document.storageunit)"> <%=LC.L_Select_All%><%--Select All*****--%>)</td>
			</tr>
			<tr>
				<td width="25%" align="right"><%=LC.L_Specimen_Type%><%--Specimen Type*****--%>:&nbsp;</td>
				<td>
				<%
		String specDesc = "";
		CodeDao cd6 = new CodeDao();
		cd6.getCodeValues("specimen_type");
		ArrayList specimenCodeListValues = cd6.getCId();
		ArrayList specimenCodeListValuesDesc = cd6.getCDesc();

%> <input type="hidden" name="specCount" value="<%=specimenCodeListValues.size()%>"> <!--KM--> 
<%
if (mode.equals("N")) {
  for(int i = 0;i<specimenCodeListValues.size(); i++) { %>
  <input type="checkbox" name=aiSpecimenType value=<%=specimenCodeListValues.get(i)%> checked><%=specimenCodeListValuesDesc.get(i)%>
<%
 }
}
else {
for(int i = 0;i<specimenCodeListValues.size(); i++) {
   boolean equalFlag = false;
	if(fkCodelstSpecimenType.contains(specimenCodeListValues.get(i))) {
    equalFlag = true;
}

if(equalFlag) {
%> <input type="checkbox" name=aiSpecimenType value=<%=specimenCodeListValues.get(i)%> checked><%=specimenCodeListValuesDesc.get(i)%>
<% } else {
%> <input type="checkbox" name=aiSpecimenType value=<%=specimenCodeListValues.get(i)%>><%=specimenCodeListValuesDesc.get(i)%>
<%  }
 }
}
%>
</td>
		<tr>
		<td width="25%" align="right"><%=LC.L_Storage_Type%><%--Storage Type*****--%>:&nbsp;</td>
	<td>
<%
	CodeDao cd9 = new CodeDao();
	//JM: 28Sep2009: #4302
	//cd9.getCodeValues("store_type");
	cd9.getCodeValuesSaveKit("store_type");
	ArrayList storageCodeListValues = cd9.getCId();
	ArrayList storageCodeListValuesDesc = cd9.getCDesc();

if (mode.equals("N")) {
for(int i = 0;i<storageCodeListValues.size(); i++) {

%> <input type="checkbox" name=aiStorageType value=<%=storageCodeListValues.get(i)%> checked><%=storageCodeListValuesDesc.get(i)%>
<%
 }
}
else {
for(int i = 0;i<storageCodeListValues.size(); i++) {
   boolean equalFlag = false;

if(fkCodelstStorageType.contains(storageCodeListValues.get(i))) {
     equalFlag = true;
}

if(equalFlag) {
%> <input type="checkbox" name=aiStorageType value=<%=storageCodeListValues.get(i)%> checked><%=storageCodeListValuesDesc.get(i)%>
<% } else { %> 
<input type="checkbox" name=aiStorageType value=<%=storageCodeListValues.get(i)%>><%=storageCodeListValuesDesc.get(i)%>
<% }
 }
}

%>
</td>
		</tr>
</table>
</td>
</tr>
</table>

<%if (mode.equals("N")) { %>

<table width="99%">
	<tr colspan="2">
		<td>
		<table>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="30%"><b><%=LC.L_Status%><%--Status*****--%></b><FONT class="Mandatory">* </FONT></td>
				<td><%=dstorageStatus%></td>
			</tr>
			<%-- INF-20084 Datepicker-- AGodara --%>
			<tr>
				<td align="right"><%=LC.L_Status_Date%><%--Status Date*****--%>
				<FONT class="Mandatory">* </FONT>&nbsp;</td>
				<td><Input type="text" name="statDate" value="" size=25 MAXLENGTH=20 class="datefield"></td>
			</tr>
			<tr>
				<td align="right"><%=LC.L_For_Study%><%--For <%=LC.Std_Study%>*****--%>
				&nbsp;</td>
				<td><input type="hidden" name="selStudyIds" value="<%=stdy%>">
				<Input type="text" name="selStudy" value="<%=studyNumber%>" size=25 readonly> <A href="#"
					onClick="openStudyWindow(document.storageunit)"><%=LC.L_Select_Study%><%--Select <%=LC.Std_Study%>*****--%></A>
				</td>
			</tr>
			<tr>

				<td align="right"><%=LC.L_For_User%><%--For User--%> &nbsp;</td>
				<input type="hidden" name="creatorId" value="<%=user%>">
				<td><Input type="text" name="createdBy" value="<%=userName%>" size=25 align=right readonly> 
					<A href="#" onClick="return openUserWindow('storageUser')"><%=LC.L_Select_User%><%--Select User*****--%></A>
				</td>

			</tr>
			<tr>
				<td align="right"><%=LC.L_Status_Notes%><%--Status Notes*****--%>
				&nbsp;</td>
				<td><textarea name="statusNotes" rows=4 cols=35 MAXLENGTH=250></textarea></td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<%}%> <%

if(mode.equals("M")) {

	//JM:06Sep2007: added, applied StorageStatusDao to retrieve storage status data
	StorageStatusDao strStatDao = StorageStatJB.getStorageStausValues(EJBUtil.stringToNum(pkStorage));

	ArrayList pkStorStats = strStatDao.getPkStorageStatuses();
	ArrayList fkStors =	strStatDao.getFkStorages();
	ArrayList stDates = strStatDao.getStorageStartDates();
	ArrayList fkCodeLstStorStats = strStatDao.getFkCodelstStorageStatuses();
	ArrayList storNotes = strStatDao.getStorageNotes();
	ArrayList storUsers = strStatDao.getStorageUsers();
	ArrayList storSsTracNums = strStatDao.getStorageSsTrackingNumbers();
	ArrayList storFkStudys= strStatDao.getStorageFkStudys();

	int loopLen = pkStorStats.size();

		String pkStorStat = "";
		String fkStor = "";
		String stDate = "";
		String fkCodeLstStorStat = "";
		String indvFkCodeLstStorStat = "";
		String storNote = "";
		String storUser = "";
		String storSsTracNum = "";
		String storFkStudy = "";

		//JM: 20Sep2007: #3150
		String maxDatePk = "";
		String maxDate = "";

		if (loopLen >0){
		maxDatePk = ((pkStorStats.get(0))==null)?"":(pkStorStats.get(0)).toString();
		maxDate = ((stDates.get(0))==null)?"":(stDates.get(0)).toString();

		int maxDateLen = maxDate.length();
		 if(maxDateLen > 1){
		    try {
				maxDate = DateUtil.dateToString(java.sql.Date.valueOf(maxDate.toString().substring(0,10)));
		    } catch(Exception e) {
			    String tmp1 = DateUtil.prepadYear(maxDate.toString().substring(0,10).trim()).substring(0, 10);
			    maxDate = DateUtil.dateToString(java.sql.Date.valueOf(tmp1));
		    }
		 }
		}

//JM: 01Jul2009: #INVP2.7.3 d)
 	CodeDao cdStoageStat = new CodeDao();

	String latestStoerageCode = cdStoageStat.getCodeSubtype(EJBUtil.stringToNum(fkCodeLstStorStat = ((fkCodeLstStorStats.get(0))==null)?"":(fkCodeLstStorStats.get(0)).toString()));
	latestStoerageCode = (latestStoerageCode==null)?"":latestStoerageCode;

//JM: 23Jul2009, #4147(part-2)
	boolean isSpecimenLocated ;
	isSpecimenLocated = StorageStatJB.isStorageAllocated(EJBUtil.stringToNum(pkStorage), EJBUtil.stringToNum(acc));
%>

<table width="99%">
	<tr>
		<td><b>
		<p class="defcomments"><%=LC.L_Status_Dets%><%--Status Details*****--%></p>
		</b></td>
		<% if (latestStoerageCode.equals("Occupied")&& isSpecimenLocated){}else{ %>
		<td align="right" colspan=4><A href="#"
			onclick="openWin('<%=pkStorage%>',<%=def_studyId%>,'','N',<%=pageRight%>,'')">
		<%=LC.L_Add_NewStatus_Upper%><%--ADD NEW STATUS*****--%> </A></td>
		<%}%>
	</tr>
	<tr>
		<th width="15%"><%=LC.L_Status_Date%><%--Status Date*****--%></th>
		<th width="25%"><%=LC.L_Status%><%--Status*****--%></th>
		<th width="15%"><%=LC.L_For_Study%><%--For <%=LC.Std_Study%>*****--%></th>
		<th width="15%"><%=LC.L_For_User%><%--For User--%></th>
		<th width="20%"><%=LC.L_Notes%><%--Notes*****--%></th>
		<th width="10%"><%=LC.L_Delete%><%--Delete*****--%></th>
	</tr>
	<!--JM: 06Sep2007: added the following loop	-->
	<%

	  int cntr = 0;
	  String storageStatDesc="";
	  CodeDao statCodeDao =new CodeDao();
	  for(cntr = 0;cntr<loopLen;cntr++)
  	  {
		pkStorStat = ((pkStorStats.get(cntr))==null)?"":(pkStorStats.get(cntr)).toString();
		fkStor = ((fkStors.get(cntr))==null)?"":(fkStors.get(cntr)).toString();
		stDate = ((stDates.get(cntr))==null)?"":(stDates.get(cntr)).toString();
 		 int dateLen = stDate.length();
		 if(dateLen > 1){
		    try {
				stDate = DateUtil.dateToString(java.sql.Date.valueOf(stDate.toString().substring(0,10)));
		    } catch(Exception e) {
			    String tmp1 = DateUtil.prepadYear(stDate.toString().substring(0,10).trim()).substring(0, 10);
			    stDate = DateUtil.dateToString(java.sql.Date.valueOf(tmp1));
		    }
		 }
		 
		fkCodeLstStorStat = ((fkCodeLstStorStats.get(cntr))==null)?"":(fkCodeLstStorStats.get(cntr)).toString();
		storageStatDesc = statCodeDao.getCodeDescription(EJBUtil.stringToNum(fkCodeLstStorStat));
		if(storageStatDesc ==null)
			storageStatDesc ="";

		storNote = ((storNotes.get(cntr))==null)?"":(storNotes.get(cntr)).toString();
		if(storNote.length()>100)
			storNote = storNote.substring(0,100) +"...";

		storUser  = ((storUsers.get(cntr))==null)?"":(storUsers.get(cntr)).toString();

		//get the user name to be displyed
		String usrName = "";
		if (!StringUtil.isEmpty(storUser)){
		usrJB.setUserId(EJBUtil.stringToNum(storUser));
		usrJB.getUserDetails();
		String fName=usrJB.getUserFirstName();
			fName=(fName==null)?"":fName;
		String lName=usrJB.getUserLastName();
			lName=(lName==null)?"":lName;
		usrName  =  fName + " " + lName ;
		}

		storSsTracNum = ((storSsTracNums.get(cntr))==null)?"":(storSsTracNums.get(cntr)).toString();
		storFkStudy = ((storFkStudys.get(cntr))==null)?"":(storFkStudys.get(cntr)).toString();
		if (storFkStudy.equals("0") || storFkStudy.equals("null")) storFkStudy="";

		//get the study number to be displyed
		String studyNum = "";
		if (!StringUtil.isEmpty(storFkStudy)){
		stdJB.setId(EJBUtil.stringToNum(storFkStudy));
		stdJB.getStudyDetails();
		studyNum = stdJB.getStudyNumber();
		studyNum=(studyNum==null)?"":studyNum;
		}

		if ((cntr%2)!=0) {
     %>

	<tr style="background-color: #d7d7d7;">
		<% } else { %>
		<tr style="background-color: #f7f7f7;">
			<% } %>

			<td width="15%"><%=stDate%></td>
			<td width="25%"><A href="#" onclick="openWin('<%=pkStorage%>', '','<%=pkStorStat%>', 'M',<%=pageRight%>, '<%=latestStoerageCode%>')"><%=storageStatDesc%></A></td>
			<td width="15%"><%=studyNum%></td>
			<td width="15%"><%=usrName%></td>
			<td width="20%"><%=storNote%></td>
			<td width="10%">
			<% if (latestStoerageCode.equals("Occupied") && cntr==0 && isSpecimenLocated){}else{%>
			<A href="#" onClick="return f_delete('<%=pkStorStat%>',<%=loopLen%>,<%=pageRight%>)">
				<img src="./images/delete.gif" border="0" align="left" alt="<%=LC.L_Delete%>" title="<%=LC.L_Delete%>" /></A> <%}%>
			</td>
		</tr>

		<%} //end of the for loop, storage status%>

		<!--JM: 06Sep2007: added above, starting from the loop for retrieving storage status data-->
</table>

<%}%>

<table width="100%">
	<tr>
		<td><br>
		<b>
		<p class="defcomments"><%=MC.M_StrgGridDets_ChildStrg%><%--Storage Grid Details : Child Storage Units*****--%></p>
		</b></td>
	</tr>
	<tr>
		<td><%=LC.L_Child_StorageType%><%--Child Storage Type*****--%>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <% if(mode.equals("N")) {%>
		<%=dChildStore%> <%} else {
			dChildStore = cd4.toPullDown("childstoretype",EJBUtil.stringToNum(childStorageType));
		%> <%=dChildStore%> <%}%>
		</td>
	</tr>
	<tr>
		<td width="40%"><%=MC.M_Dim1_OfCells%><%--Dimension 1 &nbsp;&nbsp;&nbsp;&nbsp; #of Cells*****--%>

		<% if(mode.equals("N")) { %> <Input <%=disableDim %> type="text" name="dimA" id="dimA" size=10 align=right value="1" onBlur="change(document.storageunit)"></td>
		<% } else if(mode.equals("M")) {%>
		<Input <%=disableDim %> type="text" name="dimA" id="dimA" size=10 align=right value="<%=dim1CellNum%>" onBlur="change(document.storageunit)">
		</td>
		<input type="hidden" name="oldDimA" value="<%=dim1CellNum%>">
		<input type="hidden" name="oldNameConvA" value="<%=dim1Naming%>">
		<!--
<input type = "hidden" name="oldChildstoretype" value="<%--=dChildStore--%>" >
-->
		<!--JM: 03Sep2009: 4205--->
		<input type="hidden" name="oldChildstoretype" value="<%=childStorageType%>">

		<%}%>

		<td><%=LC.L_Naming_Convention%><%--Naming Convention*****--%> <% if(mode.equals("N")) { %>

		<SELECT NAME=namingConv id="nameConvA" style="width: 100px">
			<OPTION value="azz" SELECTED><%=LC.L_AGTZz%><%--A>ZZ*****--%></OPTION>
			<OPTION value="zza"><%=LC.L_ZzGTA%><%--ZZ>A*****--%></OPTION>
		</SELECT> <%} else if(dim1Naming.equals("azz")) {%> <SELECT NAME=namingConv id="nameConvA" style="width: 100px">
			<OPTION value="azz" SELECTED><%=LC.L_AGTZz%><%--A>ZZ*****--%></OPTION>
			<OPTION value="zza"><%=LC.L_ZzGTA%><%--ZZ>A*****--%></OPTION>
		</SELECT> <%} else if(dim1Naming.equals("zza")) {%> <SELECT NAME=namingConv id="nameConvA" style="width: 100px">
			<OPTION value="azz"><%=LC.L_AGTZz%><%--A>ZZ*****--%></OPTION>
			<OPTION value="zza" SELECTED><%=LC.L_ZzGTA%><%--ZZ>A*****--%></OPTION>
		</SELECT> <%} else { %> <SELECT NAME=namingConv id="nameConvA" style="width: 100px">
			<OPTION value="azz" SELECTED><%=LC.L_AGTZz%><%--A>ZZ*****--%></OPTION>
			<OPTION value="zza"><%=LC.L_ZzGTA%><%--ZZ>A*****--%></OPTION>
		</SELECT> <%}%>
		</td>

		<td><%=LC.L_Positioning%><%--Positioning*****--%> <% if (mode.equals("N")) { %>

		<SELECT NAME=positioning id="posA" style="width: 120px">
			<OPTION value="LR" SELECTED><%=LC.L_Left_ToRight%><%--Left to Right*****--%></OPTION>
			<OPTION value="RL"><%=LC.L_Right_ToLeft%><%--Right to Left*****--%></OPTION>
		</SELECT> 
		<%
		} else if(dim1Order.equals("LR")) {%> <SELECT NAME=positioning id="posA"
			style="width: 120px" disabled>
			<OPTION value="LR" SELECTED><%=LC.L_Left_ToRight%><%--Left to Right*****--%></OPTION>
			<OPTION value="RL"><%=LC.L_Right_ToLeft%><%--Right to Left*****--%></OPTION>
		</SELECT> <input type=hidden name=positioning value=<%=dim1Order%>> 
		<%
		} else if(dim1Order.equals("RL")) {%> <SELECT NAME=positioning id="posA"
			style="width: 120px" disabled>
			<OPTION value="LR"><%=LC.L_Left_ToRight%><%--Left to Right*****--%></OPTION>
			<OPTION value="RL" SELECTED><%=LC.L_Right_ToLeft%><%--Right to Left*****--%></OPTION>
		</SELECT> <input type=hidden name=positioning value=<%=dim1Order%>> <%} else {%>

		<SELECT NAME=positioning id="posA" style="width: 120px" disabled>
			<OPTION value="LR" SELECTED><%=LC.L_Left_ToRight%><%--Left to Right*****--%></OPTION>
			<OPTION value="RL"><%=LC.L_Right_ToLeft%><%--Right to Left*****--%></OPTION>
		</SELECT> <input type="hidden" name=positioning value="LR"> <%}%>
		</td>
	</tr>

	<tr>
		<td><%=MC.M_Dim2_OfCells%><%--Dimension 2 &nbsp;&nbsp;&nbsp;&nbsp; #of Cells*****--%>

		<% if(mode.equals("N")) {%> <Input <%=disableDim %> type="text" name="dimB" id="dimB" size=10 align=right value="1" onBlur="change(document.storageunit)"></td>

		<%}else if(mode.equals("M")) {%>

		<Input <%=disableDim %> type="text" name="dimB" id="dimB" size=10 align=right value="<%=dim2CellNum%>" onBlur="change(document.storageunit)">
		</td>
		<input type="hidden" name="oldDimB" value="<%=dim2CellNum%>">
		<input type="hidden" name="oldNameConvB" value="<%=dim2Naming%>">
		<%}%>

		<td><%=LC.L_Naming_Convention%><%--Naming Convention*****--%> <% if(mode.equals("N")) {%>

		<SELECT NAME=namingConvn id="nameConvB" style="width: 100px">
			<OPTION value="1n" SELECTED><%=LC.L_1To_N%><%--1 to n*****--%></OPTION>
			<OPTION value="n1"><%=LC.L_N_To1%><%--n to 1*****--%></OPTION>
		</SELECT> <% } else if(dim2Naming.equals("1n")) {%> 
		<SELECT NAME=namingConvn id="nameConvB" style="width: 100px">
			<OPTION value="1n" SELECTED><%=LC.L_1To_N%><%--1 to n*****--%></OPTION>
			<OPTION value="n1"><%=LC.L_N_To1%><%--n to 1*****--%></OPTION>

		</SELECT> <% } else if(dim2Naming.equals("n1")) {%> 
		<SELECT NAME=namingConvn id="nameConvB" style="width: 100px">
			<OPTION value="1n"><%=LC.L_1To_N%><%--1 to n*****--%></OPTION>
			<OPTION value="n1" SELECTED><%=LC.L_N_To1%><%--n to 1*****--%></OPTION>

		</SELECT> <% } else { %> <SELECT NAME=namingConvn id="nameConvB" style="width: 100px">
			<OPTION value="1n" SELECTED><%=LC.L_1To_N%><%--1 to n*****--%></OPTION>
			<OPTION value="n1"><%=LC.L_N_To1%><%--n to 1*****--%></OPTION>
		</SELECT> <%}%>

		</td>

		<td><%=LC.L_Positioning%><%--Positioning*****--%> <% if(mode.equals("N")) {%>
		<SELECT NAME=positioningn id="posB" style="width: 120px">
			<OPTION value="BF" SELECTED><%=LC.L_Back_ToFront%><%--Back to Front*****--%></OPTION>
			<OPTION value="FB"><%=LC.L_Front_ToBack%><%--Front to Back*****--%></OPTION>

		</SELECT> <% } else if(dim2Order.equals("BF")){ %> <SELECT NAME=positioningn id="posB" style="width: 120px" disabled>
			<OPTION value="BF" SELECTED><%=LC.L_Back_ToFront%><%--Back to Front*****--%></OPTION>
			<OPTION value="FB"><%=LC.L_Front_ToBack%><%--Front to Back*****--%></OPTION>
		</SELECT> <input type=hidden name=positioningn value=<%=dim2Order%>> <% } else if(dim2Order.equals("FB")){ %>

		<SELECT NAME=positioningn id="posB" style="width: 120px" disabled>
			<OPTION value="BF"><%=LC.L_Back_ToFront%><%--Back to Front*****--%></OPTION>
			<OPTION value="FB" SELECTED><%=LC.L_Front_ToBack%><%--Front to Back*****--%></OPTION>
		</SELECT> <input type=hidden name=positioningn value=<%=dim2Order%>> <%} else {%>

		<SELECT NAME=positioningn id="posB" style="width: 120px" disabled>
			<OPTION value="BF" SELECTED><%=LC.L_Back_ToFront%><%--Back to Front*****--%></OPTION>
			<OPTION value="FB"><%=LC.L_Front_ToBack%><%--Front to Back*****--%></OPTION>
		</SELECT> <input type="hidden" name=positioningn value="BF"> <%}%>

		</td>
	</tr>

</table>

<%

	String pkStge="";
	String stgeId="";
	String stgeName="";
	String stgeType ="";
	String stgeCox="";
	String stgeCoy="";
	String stgeLoc="";
	String stgeCap="";
	String stgeStat="";
	String stgeCapUnit="";
	String stgeCapAndUnit="";

if (mode.equals("M")) {
	StorageDao strdao = storageB.getStorageValue(EJBUtil.stringToNum(pkStorage));
	ArrayList pkStorages = strdao.getPkStorages();
	ArrayList strIds = strdao.getStorageIds();
	ArrayList strNames = strdao.getStorageNames();
	ArrayList strTypes = strdao.getFkCodelstStorageTypes();
	ArrayList strCoXs = strdao.getStorageCoordinateXs();
	ArrayList strCoYs = strdao.getStorageCoordinateYs();
	ArrayList strCapacities = strdao.getStorageCapNums();
	ArrayList strCapUnits = strdao.getStorageCapUnits();
	ArrayList strStats = strdao.getStorageStatus();
	int len = pkStorages.size();

	%> <b>
<p class="defcomments"><%=MC.M_Existing_StorUnits%><%--Existing Child Storage Units*****--%>:</p>
</b> <input type=hidden name="childcount" value=<%=len%>>

<table width="100%" border="0">
	<tr>
		<th width="5%"><%=LC.L_Edit%><%--Edit*****--%></th>
		<th width="10%"><%=LC.L_Id_Upper%><%--ID*****--%></th>
		<th width="20%"><%=LC.L_Name%><%--Name*****--%></th>
		<th width="8%"><%=LC.L_Type%><%--Type*****--%></th>
		<th width="23%"><%=MC.M_Grid_CoordColRow%><%--Grid Coordinates (column, row)*****--%></th>
		<th width="10%"><%=LC.L_Capacity%><%--Capacity*****--%></th>
		<th width="10%"><%=LC.L_Status%><%--Status*****--%></th>
		<th width="14%"><%=LC.L_Content%><%--Content*****--%></th>
	</tr>

	<%
	  if(len >0){
	  int counter = 0;
	  String stgeTypeDesc="";
	  String stgeStatDesc="";
	  CodeDao cdDao =new CodeDao();
	  for(counter = 0;counter<len;counter++)
  	  {
		pkStge = ((pkStorages.get(counter))==null)?"-":(pkStorages.get(counter)).toString();
	    stgeId = ((strIds.get(counter))==null)?"-":(strIds.get(counter)).toString();
        stgeName = ((strNames.get(counter))==null)?"":(strNames.get(counter)).toString();
	    stgeType = ((strTypes.get(counter))==null)?"":(strTypes.get(counter)).toString();
		stgeTypeDesc = cdDao.getCodeDescription(EJBUtil.stringToNum(stgeType));
		if(stgeTypeDesc ==null)
			stgeTypeDesc ="";

		String dChildStorageType = cd4.toPullDown(pkStge+"_type",EJBUtil.stringToNum(stgeType));
	    stgeCox = ((strCoXs.get(counter))==null)?"":(strCoXs.get(counter)).toString();
	    stgeCoy = ((strCoYs.get(counter))==null)?"":(strCoYs.get(counter)).toString();
	    stgeCap = ((strCapacities.get(counter))==null)?"":(strCapacities.get(counter)).toString();
		stgeCapUnit = ((strCapUnits.get(counter))==null)?"":(strCapUnits.get(counter)).toString();
		
		String strCapacityUnitsDesc = "" ;

		if(stgeCapUnit != "")
			strCapacityUnitsDesc =  cdDao.getCodeDescription(EJBUtil.stringToNum(stgeCapUnit));
		else
			strCapacityUnitsDesc = "";

		stgeCapAndUnit = stgeCap+" "+strCapacityUnitsDesc;
        stgeStat = ((strStats.get(counter))==null)?"":(strStats.get(counter)).toString();

		//KM-to fix the issue 3153
		stgeStatDesc = cdDao.getCodeDescription(EJBUtil.stringToNum(stgeStat));

		if(stgeStatDesc ==null)
			stgeStatDesc ="";

		if((stgeCox !="") && (stgeCoy!=""))
		   stgeLoc = "("+stgeCox +","+stgeCoy+")";

		if ((counter%2)!=0) { %>

	<tr style="background-color: #d7d7d7;">
		<% } else { %>
		<tr style="background-color: #f7f7f7;">
		<% }

        StorageDao strageDaoForSpec1 = storageB.getSpecimensAndPatients(EJBUtil.stringToNum(pkStge), EJBUtil.stringToNum(acc));
        ArrayList specimenList1 = strageDaoForSpec1.getSpecimens();
        ArrayList specimenPatientList1 = strageDaoForSpec1.getSpecimenPatients();
        boolean hasContent1 = false;

        if (specimenList1 != null && specimenList1.size() > 0) {
            hasContent1 = true;
        }

        String spLink1 = "";
        String pLink1 = "";

		if (hasContent1) {
            if (specimenList1.size() == 1) {
                spLink1 = "<a href='specimendetails.jsp?mode=M&pkId="+specimenList1.get(0)+"'>Specimen</a>";
                if (specimenPatientList1 != null && specimenPatientList1.size() == 1 &&
                        ((Integer)specimenPatientList1.get(0)).intValue() > 0) {
                    pLink1 = "&nbsp;of&nbsp" + "<a href='patientdetails.jsp?mode=M&srcmenu=tdmenubaritem5&selectedTab=1&page=patient&pkey="+specimenPatientList1.get(0)+"'>"+LC.L_Patient/*LC.Pat_Patient*****/+"</a>";
                } 
            } else {
                spLink1 = "<a href='specimenbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=1&mainFKStorage"+EJBUtil.stringToNum(pkStge)+"'>"+LC.L_Specimen/*Specimen*****/+"</a>";
            }
        } else {
            spLink1 = LC.L_None;/*spLink1 = "None";*****/
        }
  %>
			<td width="5%"><input type="checkbox" value=<%=pkStge%> name="selectEdit"
				onClick="f_edit(this,document.storageunit,<%=pkStge%>)" /></td>
			<td width="10%"><A href="storageunitdetails.jsp?srcmenu=tdmenubaritem6&selectedTab=2&pkStorage=<%=pkStge%>&mode=M"><%=stgeId%>
			</A></td>
			<td width="20%"><span id="name_<%=pkStge%>"><%=stgeName%></span>
			<span id="<%=pkStge%>_name_e" style="display: none;">
			<input onBlur="return validateName(this);" name="<%=pkStge%>_name_e" type="text" value='<%=stgeName%>' /></span></td>
			<td width="8%"><span id="type_<%=pkStge%>"><%=stgeTypeDesc%></span>
			<span id="<%=pkStge%>_type_e" style="display: none;"><%=dChildStorageType%></span></td>
			<td width="23%"><%=stgeLoc%></td>
			<td width="10%"><%=stgeCapAndUnit%></td>
			<td width="10%"><%=stgeStatDesc%></td>
			<td width="10%"><%=spLink1%><%=pLink1%></td>
		</tr>
		<%}
	  } else {
	  %>
		<tr>
			<td colspan="5" align="center"><b> <%=MC.M_NoChildStorages%><%--No child storages*****--%>
		</b> <td> </tr>
		<%
 }
%>
	
</table>

<% } if ( pageRight >=5  ) {%> <BR>
<jsp:include page="submitBar.jsp" flush="true">
	<jsp:param name="displayESign" value="Y" />
	<jsp:param name="formID" value="storageunitdet" />
	<jsp:param name="showDiscard" value="N" />
</jsp:include> <% } %>
</Form>
<%
}//end of if body for session
	else {
%> <jsp:include page="timeout.html" flush="true" /> <% } %>
<div class="myHomebottomPanel"><jsp:include page="bottompanel.jsp" flush="true" /></div>
</div>
</body>

</html>
