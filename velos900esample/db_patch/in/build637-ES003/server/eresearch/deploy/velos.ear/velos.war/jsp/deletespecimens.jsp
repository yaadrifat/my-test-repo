<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true" />
<%@ page language="java"
	import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils,com.velos.eres.web.storageStatus.StorageStatusJB"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<jsp:useBean id="specimenJB" scope="request" class="com.velos.eres.web.specimen.SpecimenJB" />
<jsp:useBean id="storageB" scope="request" class="com.velos.eres.web.storage.StorageJB" />
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint" />

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

<%
	String src;
	src = request.getParameter("srcmenu");
%>
<jsp:include page="skinChoser.jsp" flush="true" />
<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>" />
</jsp:include>

<BODY>
<br>

<DIV class="formDefault" id="div1">
<%
	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession)) {
		String delMode = request.getParameter("delMode");
		String selStrs = request.getParameter("selStrs");

		if (delMode == null) {
			delMode = "final";
%>
<FORM name="specimendel" id="specdelfrm" method="post"
	action="deletespecimens.jsp?srcmenu=<%=src%>"
	onSubmit="if (validate(document.specimendel)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<br>
<br>

<TABLE width="98%" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center"><b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%>
		</b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>

<jsp:include page="submitBar.jsp" flush="true">
	<jsp:param name="displayESign" value="Y" />
	<jsp:param name="formID" value="specdelfrm" />
	<jsp:param name="showDiscard" value="N" />
</jsp:include> 

<input type="hidden" name="delMode" value="<%=delMode%>"> 
<input type="hidden" name="selStrs" value="<%=selStrs%>">

</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");
			String oldESign = (String) tSession.getValue("eSign");
			if (!oldESign.equals(eSign)) {
%> <jsp:include page="incorrectesign.jsp" flush="true" /> <%
 	} else {

 				String fkStorage = "";
 				StringTokenizer idStrs = new StringTokenizer(selStrs,
 						",");
 				int numIds = idStrs.countTokens();
 				int i = 1;
 				String[] strArrStrs = new String[numIds];

 				for (int cnt = 0; cnt < numIds; cnt++) {
 					strArrStrs[cnt] = idStrs.nextToken();
 					specimenJB.setPkSpecimen(EJBUtil.stringToNum(strArrStrs[cnt]));
 					specimenJB.getSpecimenDetails();
 					fkStorage = specimenJB.getFkStorage();
 					String[] strArrStrs2 = new String[1];
 					strArrStrs2[0] = strArrStrs[cnt];
 					if (fkStorage != null) {
 						int sret = 0, storageCount = 0;
 						Integer partOccupiedStat = 0, availableStat = 0;
 						String multiSpecimen = "", startDate = "", fkStudy = "", ipAdd = "", user = "";
 						Date dt = new java.util.Date();
 						startDate = DateUtil.dateToString(dt);
 						ipAdd = (String) tSession.getValue("ipAdd");
 						user = (String) tSession.getValue("userId");
 						fkStudy = specimenJB.getFkStudy();
						CodeDao cd5 = new CodeDao();
 						partOccupiedStat = cd5.getCodeId("storage_stat", "PartOccupied");
 						availableStat = cd5.getCodeId("storage_stat","Available");
 						storageB.setPkStorage(EJBUtil.stringToNum(fkStorage));
 						storageB.getStorageDetails();
 						multiSpecimen = storageB.getMultiSpecimenStorage();
 						storageCount = storageB.getStorageCount(fkStorage);
						StorageStatusJB storageStatB = new StorageStatusJB();
 						i = specimenJB.deleteSpecimens(strArrStrs2,AuditUtils.createArgs(tSession,"", ""));
 						if (i == 0) {
						 	if (EJBUtil.stringToNum(multiSpecimen) == 0	|| multiSpecimen == "")
 							storageStatB.setFkCodelstStorageStat(EJBUtil.integerToString(availableStat));
 						else {
 							if (storageCount == 1)
 								storageStatB.setFkCodelstStorageStat(EJBUtil.integerToString(availableStat));
 							else if (storageCount > 1)
 								storageStatB.setFkCodelstStorageStat(EJBUtil.integerToString(partOccupiedStat));
 						}
 							storageStatB.setFkStorage(fkStorage);
 							storageStatB.setSsStartDate(startDate);
 							storageStatB.setSsNotes("");
 							storageStatB.setFkUser(user);
 							storageStatB.setSsTrackingNum("");
 							storageStatB.setFkStudy(fkStudy);
 							storageStatB.setIpAdd(ipAdd);
 							storageStatB.setCreator(user);
 							sret = storageStatB.setStorageStatusDetails();
 						}
 					} else
 						i = specimenJB.deleteSpecimens(strArrStrs2,AuditUtils.createArgs(tSession,"", ""));

 				}
 				// Modified for INF-18183 ::: AGodara	
 				//int i = specimenJB.deleteSpecimens(strArrStrs,AuditUtils.createArgs(tSession, "", ""));
 %> <br>
<br>
<br>
<br>
<%
	if (i == 0) {
%>
<p class="successfulmsg" align=center><%=MC.M_Del_Succ%><%--Deleted successfully.*****--%></p>
<%
	} else {
%>
<p class="successfulmsg" align=center><%=MC.M_DataCnt_BeDel%><%--Data could not be deleted.*****--%>
</p>
<%
	}
%>

<META HTTP-EQUIV=Refresh CONTENT="1; URL=specimenbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=1&searchFrom=search">
<%
	} //end esign
		} //end of delMode
	}//end of if body for session
	else {
%> <jsp:include page="timeout.html" flush="true" /> <%
 	}
 %>

<div class="myHomebottomPanel"><jsp:include page="bottompanel.jsp" flush="true" /></div>

</DIV>

<div class="mainMenu" id="emenu"><jsp:include page="getmenu.jsp" flush="true" /></div>

</BODY>
</HTML>
