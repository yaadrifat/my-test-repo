set define off;

DROP TRIGGER "ERES"."ER_SPECIMEN_AD1";
DROP TRIGGER "ERES"."ER_SPECIMEN_AI0";
DROP TRIGGER "ERES"."ER_SPECIMEN_AU1";

COMMIT;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,183,2,'02_DROP_TRIGGERS.sql',sysdate,'9.0.0 B#637-ES003');

COMMIT;
