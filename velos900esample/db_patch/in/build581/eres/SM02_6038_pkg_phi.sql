CREATE OR REPLACE PACKAGE BODY      "PKG_PHI" AS

     FUNCTION f_get_patientright(p_user NUMBER, p_group NUMBER, p_patient NUMBER ) RETURN NUMBER
     AS
      v_grpright_seq NUMBER;
      v_grpright NUMBER;
      v_studyright_seq NUMBER;
      v_studyright NUMBER;
      v_enrollcount NUMBER;
      v_supusr_flag NUMBER;
      v_studyright_str VARCHAR2(100);
      v_superuser_studRights VARCHAR2(100);
      o_right NUMBER;
    BEGIN
         -- get group right sequence for  'View complete patient details' for the user
         SELECT ctrl_seq
         INTO v_grpright_seq
         FROM ER_CTRLTAB
         WHERE ctrl_key = 'app_rights' AND ctrl_value = 'VIEWPATCOM';
           -- get group right for  'View complete patient details' for the user
         SELECT SUBSTR(grp_rights,v_grpright_seq,1) , grp_supusr_flag,grp_supusr_rights
         INTO v_grpright, v_supusr_flag,v_superuser_studRights
         FROM ER_GRPS,ER_USER
         WHERE pk_grp = p_group
         AND pk_grp=fk_grp_default
         AND ER_USER.pk_user=p_user;
              -- get study right sequence for  'View complete patient details'
         SELECT ctrl_seq
         INTO v_studyright_seq
         FROM ER_CTRLTAB
         WHERE ctrl_key = 'study_rights' AND  ctrl_value='STUDYVPDET' ;
         -- find if patient is currently enrolled in a protocol

         SELECT COUNT(*)
         INTO v_enrollcount
         FROM erv_patstudystat a
         WHERE fk_per = p_patient
         AND  status_codelst_subtyp <> 'lockdown'
        AND  ( EXISTS (SELECT * FROM ER_STUDYTEAM b WHERE b.fk_study = a.fk_study AND  fk_user = p_user AND study_team_usr_type = 'D'  )
                  OR Pkg_Phi.F_Is_Superuser(p_user, a.fk_study) = 1 ) ;

         IF  v_enrollcount > 0 THEN
             -- get min. of 'View complete patient details'' for all studies in which patient is enrolled.

                   SELECT MIN (NVL(TO_NUMBER(SUBSTR(rights,v_studyright_seq,1 )),0 ))
                   INTO v_studyright
                   FROM (
                   SELECT (NVL((SELECT study_team_rights FROM ER_STUDYTEAM x WHERE x.fk_user = p_user AND  x.fk_study = c.fk_study
                    AND study_team_usr_type = 'D'), v_superuser_studRights ) ) RIGHTS
                        FROM ER_PATPROT C, ER_PATSTUDYSTAT B
                 WHERE C.fk_per = p_patient  AND
                  C.PATPROT_STAT = 1 AND
                  C.PATPROT_ENROLDT IS NOT NULL AND
                  B.FK_PER = C.FK_PER AND
                  B.FK_STUDY = C.FK_STUDY  AND
                  B.PATSTUDYSTAT_ENDT IS NULL AND
                 ( EXISTS (SELECT * FROM ER_STUDYTEAM a WHERE a.fk_study = c.fk_study AND  fk_user = p_user AND study_team_usr_type = 'D'  )
                  OR Pkg_Phi.F_Is_Superuser(p_user, c.fk_study) = 1 ) )   ;


         ELSE
              v_studyright := 4;
            --Plog.DEBUG(pCTX,'hardcode');
         END IF;
         IF v_studyright >= 4 AND v_grpright >= 4 THEN
             o_right := 4;
         ELSE
             o_right := 0;
         END IF;
            --Plog.DEBUG(pCTX,'hardcode');
         RETURN o_right;

    END; -- end of function


  FUNCTION F_Is_Superuser(p_user  NUMBER, p_study NUMBER) RETURN NUMBER
  IS
  v_group NUMBER;
  v_rights VARCHAR2(100) := '';
  v_flag NUMBER;

   v_acc_study NUMBER :=0;
   v_settings_count NUMBER :=0;
   v_usr_account NUMBER ;

  BEGIN
      -- get user group and see if its super user

      SELECT fk_grp_default , NVL(grp_supusr_rights,''),NVL(grp_supusr_flag,0),ER_USER.fk_account
      INTO v_group,v_rights,v_flag,v_usr_account
      FROM ER_USER,ER_GRPS
      WHERE pk_user = p_user AND pk_grp = fk_grp_default ;

      IF v_flag = 1 THEN

     SELECT COUNT (*)
     INTO v_settings_count
     FROM
          ER_SETTINGS
     WHERE settings_modname=4 AND settings_modnum=v_group
     AND settings_keyword IN ('STUDY_TAREA' ,'STUDY_DIVISION','STUDY_DISSITE') ;

    BEGIN
      IF v_settings_count > 0 THEN

           SELECT pk_study
          INTO v_acc_study
        FROM ER_STUDY
          WHERE pk_study = p_study AND fk_account = v_usr_account
          AND (
          (((SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum=v_group
     AND settings_keyword = 'STUDY_TAREA') LIKE '%'||TO_CHAR(fk_codelst_tarea)||'%') AND (fk_codelst_tarea IS NOT NULL))
     OR
     (((SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum=v_group
     AND settings_keyword = 'STUDY_DIVISION') LIKE '%'||TO_CHAR(study_division)||'%') AND (study_division IS NOT NULL))
               OR
    (1=(SELECT  Pkg_Util.f_compare('SELECT (settings_value) FROM ER_SETTINGS WHERE settings_modname=4 AND settings_modnum='||v_group||' AND settings_keyword = ''STUDY_DISSITE''',
        study_disease_site,',',',','SQL','') FROM dual) AND (study_disease_site IS NOT NULL) )
                       );

             RETURN 1 ;
        ELSE

            SELECT COUNT(pk_study)
              INTO v_acc_study
            FROM ER_STUDY
              WHERE pk_study = p_study AND fk_account = v_usr_account ;

            RETURN v_acc_study;

        END IF;

        EXCEPTION WHEN NO_DATA_FOUND THEN

                  plog.fatal(pctx,'psuper' || SQLERRM);
                  RETURN 0;
        END;


    ELSE
        RETURN 0;
      END IF;



  END; -- for function

FUNCTION F_Gen_PatDump_phi(p_study VARCHAR2,p_site VARCHAR2,p_patient VARCHAR2,p_txarm VARCHAR2,
p_startdate VARCHAR2, p_enddate VARCHAR2,p_user NUMBER ,p_pi in varchar2)
RETURN  CLOB
AS
v_xml CLOB := EMPTY_CLOB();
v_studynum VARCHAR2(100);
v_studytitle VARCHAR2(4000);
v_site VARCHAR2(100);
v_count NUMBER;
v_temp VARCHAR2(30);
tab_study split_tbl;
tab_org split_tbl;
v_study VARCHAR2(2000);
v_org VARCHAR2(20);
v_counter NUMBER :=1;
v_perCode VARCHAR2(20);
v_perLName VARCHAR2(200);
v_perFName VARCHAR2(200);
v_initials VARCHAR2(10);
v_gender VARCHAR2(50);
v_enrollDt VARCHAR2(20);
v_patStudyId VARCHAR2(20);
v_race VARCHAR2(50);
v_ethnicity VARCHAR2(50);
v_deathDate VARCHAR2(20);
v_enrollPhysn VARCHAR2(200);
v_assndTo VARCHAR2(200);
v_sql VARCHAR2(4000);
v_cur Gk_Cv_Types.GenericCursorType;
v_statcur Gk_Cv_Types.GenericCursorType;
v_statSQL VARCHAR2(4000);
v_perPk NUMBER;
v_statDate VARCHAR2(20);
v_statReason VARCHAR2(4000);
v_statStr CLOB;
v_headerStr VARCHAR2(4000);
v_txarm VARCHAR2(100);
v_where VARCHAR2(4000);
v_add CHAR(1);
v_infVer VARCHAR2(250);
v_txPK NUMBER;
v_off_study_stat NUMBER;
v_off_treat_stat NUMBER;
v_off_study_count NUMBER;
v_off_treat_count NUMBER;
v_tx_desc VARCHAR2(4000);
v_tempStr VARCHAR2(32000);
v_finalclob CLOB := EMPTY_CLOB();
v_group NUMBER;
v_pat_access NUMBER;
v_study_filter varchar2(32000);
pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'f_gen_patdump', pLEVEL  => Plog.LFATAL);

v_filterstudycur Gk_Cv_Types.GenericCursorType;
 generateList boolean := false;
 v_prinv_name varchar2(2000);
BEGIN



 if (p_study is null or p_study = ':studyId' or nvl(p_study,'') = '' or length(trim(p_study)) = 0 ) then --filter not replaced

     --get the sql for 'ALL' studies
     select repfilter_valuesql
     into v_study_filter
     from er_repfilter
     where  repfilter_keyword = 'studyId';

     v_study_filter := replace(v_study_filter,':sessUserId',p_user);

     generateList := true;

 else -- p_study has study selected

     v_study_filter := p_study;

  end if;

  --if PI filter is supplied append that to it
      if (instr(lower(p_pi),'pk_user') <= 0) then
          generateList := true;
      end if;

  if generateList = true then
          v_study_filter :=  'select a.pk_study from er_study a where a.pk_study in (' || v_study_filter || ' )' ;

         if (instr(lower(p_pi),'pk_user') <= 0) then
             v_study_filter :=  v_study_filter || '  and study_prinv in (' || p_pi ||')';
         end if;



      begin
           open v_filterstudycur for v_study_filter;

             select nvl(Pkg_Util.f_join(v_filterstudycur ,','),'0')
             into v_study_filter
             from dual;
      exception when others then
            v_study_filter := '0';
      end ;

      if v_study_filter is null then
            v_study_filter := '0';
      end if;


 end if;


    DBMS_LOB.CREATETEMPORARY(v_xml,TRUE);
    DBMS_LOB.CREATETEMPORARY(v_finalclob,TRUE);

--v_xml := v_xml || '<ROWSET>';
--Create header String

SELECT fk_grp_default INTO v_group FROM ER_USER WHERE pk_user = p_user;

v_add:='Y';
   FOR k IN (SELECT pk_codelst,codelst_subtyp,codelst_desc FROM ER_CODELST WHERE codelst_type='patStatus' ORDER BY codelst_seq )
   LOOP
   IF (LENGTH(v_headerStr)>0)  THEN
   v_headerStr:=v_headerStr||'<name>'||k.codelst_desc||'</name>';
   ELSE
--   v_headerStr:='<'||k.codelst_subtyp||'>'||k.codelst_desc||'</'||k.codelst_subtyp||'>';
   v_headerStr:='<name>'||k.codelst_desc||'</name>';
   END IF;
   END LOOP;
 v_headerStr:='<HEADER>'||v_headerStr||'</HEADER>';
--End header

SELECT Pkg_Util.f_split(v_study_filter) INTO tab_study FROM dual;
plog.DEBUG(pCTX,'v_xml2-'||v_xml);
SELECT Pkg_Util.f_split(p_site) INTO tab_org FROM dual;

 -- get off treatment code
 SELECT Pkg_Util.f_getcodepk ('offtreat', 'patStatus')
 INTO v_off_treat_stat FROM dual;

 -- get off study code
 SELECT Pkg_Util.f_getcodepk ('offstudy', 'patStatus')
 INTO v_off_study_stat FROM dual;

 ----------


 FOR i IN (SELECT COLUMN_VALUE AS study FROM TABLE(tab_study))
 LOOP
 v_study:=i.study;

     if v_study > 0 then

     plog.DEBUG(pCTX,'study-'||v_study);

     -- get offtreatment count
     --Bug Fix for # 3604-1 SM 06/26/2008 TO_DATE() to TO_DATE(, 'mm/dd/yyyy')
     SELECT COUNT(DISTINCT fk_per)
     INTO v_off_treat_count
     FROM ER_PATPROT pp, ER_PER WHERE fk_study = v_study AND
       pk_per = fk_per AND fk_site IN (SELECT COLUMN_VALUE AS org FROM TABLE(tab_org))
     AND  patprot_stat=1
     AND  ( (patprot_enroldt BETWEEN  TO_DATE(p_startdate, PKG_DATEUTIL.F_GET_DATEFORMAT)  AND TO_DATE(p_enddate, PKG_DATEUTIL.F_GET_DATEFORMAT)) OR patprot_enroldt IS NULL) AND
     EXISTS (SELECT * FROM ER_PATSTUDYSTAT P WHERE P.fk_per = pp.fk_per AND P.fk_study = pp.fk_study
     AND fk_codelst_stat = v_off_treat_stat);

     -- get off study count  Select count(distinct fk_per)
     --Bug Fix for # 3604-1 SM 06/26/2008 TO_DATE() to TO_DATE(, 'mm/dd/yyyy')
     SELECT COUNT(DISTINCT fk_per)
     INTO v_off_study_count
     FROM ER_PATPROT pp, ER_PER WHERE fk_study = v_study
     AND  pk_per = fk_per AND fk_site IN (SELECT COLUMN_VALUE AS org FROM TABLE(tab_org))
     AND  patprot_stat=1
     AND  ( (patprot_enroldt BETWEEN  TO_DATE(p_startdate, PKG_DATEUTIL.F_GET_DATEFORMAT)  AND TO_DATE(p_enddate, PKG_DATEUTIL.F_GET_DATEFORMAT)) OR patprot_enroldt IS NULL) AND
     EXISTS (SELECT * FROM ER_PATSTUDYSTAT P WHERE P.fk_per = pp.fk_per AND P.fk_study = pp.fk_study
     AND fk_codelst_stat = v_off_study_stat);



     -----------------------
     FOR j IN (SELECT COLUMN_VALUE AS org FROM TABLE(tab_org))
     LOOP
     v_org:=j.org;
     plog.DEBUG(pCTX,'site-'||v_org);


    SELECT study_number, study_title , USR_LST(study_prinv) INTO v_studynum, v_studytitle ,v_prinv_name FROM ER_STUDY WHERE pk_study  = v_study;

    -- Bug fix for #3526 CL#7815 By SM 06/02/08
    SELECT  REPLACE(site_name, '&', '&amp;') INTO v_site FROM ER_SITE WHERE pk_site =  v_org;

    plog.DEBUG(pCTX,'TXARM'||p_txarm||'date'||p_startdate||'date1'||p_enddate
    );
    --Bug Fix for # 3604-1 SM 06/26/2008 TO_DATE() to TO_DATE(, 'mm/dd/yyyy')
    v_sql:= ' select * from (SELECT  pkg_phi.f_get_patientright(' || p_user || ',' || v_group || ', fk_per ) AS pat_access,fk_per,person_code AS patient_id,patprot_patstdid AS patient_study_id,person_lname AS patient_lname , person_fname AS patient_fname,SUBSTR(Person_fname,1,1) || SUBSTR(person_mname,1,1) || SUBSTR(person_lname,1,1) AS initials,F_Get_Codelstdesc(fk_codelst_gender) AS gender,(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_race) AS race,(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_ethnicity) AS ethnicity,
    TO_CHAR(person_deathdt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS death_date,(SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = patprot_physician) AS enroll_physn,(SELECT usr_firstname || '' '' || usr_lastname FROM ER_USER WHERE pk_user = fk_userassto) AS usr_assdto,TO_CHAR(patprot_enroldt,PKG_DATEUTIL.F_GET_DATEFORMAT) AS enrolled_on ,'
    || '(SELECT REPLACE(tx_name, ''&'', ''&amp;'') FROM ER_STUDYTXARM, ER_PATTXARM WHERE pk_studytxarm = fk_studytxarm AND fk_patprot = pk_patprot AND tx_start_date = (SELECT MIN(tx_start_date) FROM ER_PATTXARM WHERE fk_patprot = pk_patprot ) AND ROWNUM = 1) AS txarm,'
    || '(SELECT fk_studytxarm FROM ER_STUDYTXARM, ER_PATTXARM WHERE pk_studytxarm = fk_studytxarm AND fk_patprot = pk_patprot AND tx_start_date = (SELECT MIN(tx_start_date) FROM ER_PATTXARM WHERE fk_patprot = pk_patprot ) AND ROWNUM = 1) AS fk_studytxarm'
    ||'  FROM ER_PATPROT ,epat.PERSON  WHERE fk_study='||v_study
    || '  AND pk_person=fk_per AND  fk_site='||v_org || '  AND  patprot_stat=1 and  (patprot_enroldt BETWEEN  to_date('''||p_startdate||''',PKG_DATEUTIL.F_GET_DATEFORMAT)  AND to_date(''' ||p_enddate||''', PKG_DATEUTIL.F_GET_DATEFORMAT) or patprot_enroldt is null))';

    --Put txarm filter
    v_where:='';

    /*
    IF (LENGTH(p_txarm) >0) THEN
       IF (LENGTH(v_where)>0) THEN
       v_where:=v_where|| '  and fk_studytxarm  in ('||p_txarm||')';
    --      v_where:=v_where|| '  and (fk_studytxarm is null or fk_studytxarm  in ('||p_txarm||'))';
     ELSE
     v_where:='fk_studytxarm  in ('||p_txarm||')';
    --   v_where:='fk_studytxarm is null or fk_studytxarm  in ('||p_txarm||')';
     END IF;
    END IF;
    */

    plog.DEBUG(pCTX,'v_where'||v_where||'v_Add'||v_Add);
    --IF ((LENGTH(v_where)>0) AND (v_add='Y') ) THEN
    IF ((LENGTH(v_where)>0) ) THEN
    v_sql:=v_sql||' where  '||v_where;
    --v_add:='N';
    END IF;
    v_sql:=v_sql|| '  order by enrolled_on desc';

    plog.DEBUG(pCTX,'SQL'||v_sql);
    OPEN v_cur FOR v_sql;
     LOOP
     FETCH v_cur INTO v_pat_access,v_perPk,v_perCode,v_patStudyId,v_perLName,v_perFName,v_initials,v_gender,v_race,v_ethnicity,v_deathDate,v_enrollPhysn,v_assndTo,v_enrollDt,v_txarm,v_txPk;
     EXIT WHEN v_cur %NOTFOUND;
    --   v_xml := v_xml || '<ROW num="'||v_counter||'">';

     -- get TX description

     --    v_tempclob :=  empty_clob();
    v_tempStr := '';

         IF (NVL(v_txPk,0) > 0) THEN
             SELECT  NVL(REPLACE(tx_desc,'&','&amp;'),'&#xa0;') INTO v_tx_desc FROM ER_STUDYTXARM WHERE pk_studytxarm = v_txPk;
        ELSE
            v_tx_desc  := '&#xa0;';
        END IF;

    v_tempStr:= v_tempStr || '<ROW num="'||TO_CHAR(v_counter)||'">';
       v_tempStr := v_tempStr  ||'<PRINV_NAME>'|| NVL(v_prinv_name, '&#xa0;')||'</PRINV_NAME><STUDY_NUM>' || v_studynum ||  '</STUDY_NUM><OFFTREAT_CT>'||v_off_treat_count ||'</OFFTREAT_CT><OFFSTUDY_CT>'||v_off_study_count ||'</OFFSTUDY_CT><SITE>' || v_site || '</SITE><PATIENT_ID>'||v_perCode||'</PATIENT_ID><PATIENT_STUDY_ID>'||v_patStudyId||'</PATIENT_STUDY_ID>';
        IF v_pat_access < 4 THEN
          v_tempStr :=v_tempStr ||'<PATIENT_LNAME>***</PATIENT_LNAME><PATIENT_FNAME>***</PATIENT_FNAME><INITIALS>***</INITIALS>';
        ELSE
          v_tempStr :=v_tempStr ||'<PATIENT_LNAME>'|| NVL(v_perLName, '&#xa0;') ||'</PATIENT_LNAME><PATIENT_FNAME>'|| NVL(v_perFName,'&#xa0;') ||'</PATIENT_FNAME><INITIALS>'|| NVL(v_initials, '&#xa0;') || '</INITIALS>';
          END IF;
      v_tempStr :=v_tempStr || '<GENDER>'|| NVL(v_gender, '&#xa0;') ||'</GENDER><TXARM>'||NVL(v_txarm,'&#xa0;')||'</TXARM><TXDESC>'||  v_tx_desc  || '</TXDESC><RACE>'||NVL(v_race,'&#xa0;') ||'</RACE><ETHNICITY>'|| NVL(v_ethnicity,'&#xa0;')||'</ETHNICITY><DEATH_DATE>'|| NVL(v_deathDate, '&#xa0;') ||'</DEATH_DATE><ENROLL_PHYSN>'|| NVL(v_enrollPhysn, '&#xa0;') ||'</ENROLL_PHYSN><USR_ASSDTO>'|| NVL(v_assndTo, '&#xa0;') ||'</USR_ASSDTO>';
       v_counter:=v_counter+1;

       FOR i IN (SELECT pk_codelst,codelst_subtyp,codelst_desc FROM ER_CODELST WHERE codelst_type='patStatus'  ORDER BY codelst_seq)
       LOOP
          v_statStr:='';
       v_statSQL:= 'SELECT  to_char(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) as statDate , (select codelst_desc from er_codelst where pk_codelst=patstudystat_reason) as reason, inform_consent_ver  FROM ER_PATSTUDYSTAT WHERE fk_per='||v_perPk||' and fk_study='||v_study|| ' and fk_codelst_stat='||i.pk_codelst||'  order by patstudystat_date desc'   ;
    --   OPEN v_statcur FOR v_statSQL;
       FOR x IN (SELECT  TO_CHAR(patstudystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) AS statDate , (SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst=patstudystat_reason) AS reason, inform_consent_ver  FROM ER_PATSTUDYSTAT WHERE fk_per=v_perPk AND fk_study=v_study AND fk_codelst_stat= i.pk_codelst  ORDER BY patstudystat_date DESC)
       LOOP
    --   FETCH v_statcur INTO  v_statDate,v_statReason, v_infVer;
    --    EXIT WHEN v_statcur %NOTFOUND;
    v_statDate:=x.statDate;
     IF (i.codelst_subtyp='infConsent') THEN
     IF (LENGTH(v_statStr)>0) THEN
    -- v_statStr:=v_statStr||' , '||CHR(13)||v_statDate;
     v_statStr:=v_statStr||' , '||CHR(13)|| x.statDate;
     ELSE
    -- v_statStr:=v_statDate||' Ver. '||v_infVer||'';
     v_statStr:=v_statDate||' Ver. '|| x.inform_consent_ver||'';
     END IF;
     ELSE
     IF (LENGTH(v_statStr)>0) THEN
    -- v_statStr:=v_statStr||' , '||CHR(13)||v_statDate||' '||v_statReason;
     v_statStr:=v_statStr||' , '||CHR(13)||v_statDate||' '||x.reason;
     ELSE
    -- v_statStr:=v_statDate||' '||v_statReason;
     v_statStr:=v_statDate||' '||x.reason;
     END IF;
    END IF;
       END LOOP; --end loop for status SQL

     v_tempStr  := v_tempStr  ||'<data>'|| NVL(v_statStr,'&#xa0;') ||'</data>';

      END LOOP; -- loop for codelst types

      v_tempStr  :=  v_tempStr || '</ROW>';

       dbms_lob.writeappend( v_xml, LENGTH(v_tempStr), v_tempStr );

     --  plog.DEBUG(pCTX,'v_xml-final'||v_xml);


     END LOOP;

    END LOOP;

end if; -- for v_study > 0
END LOOP;



   --v_xml :='<ROWSET>' ||v_headerStr|| v_xml || '</ROWSET>';

   dbms_lob.WRITEAPPEND (  v_finalclob, 8, '<ROWSET>' );
   dbms_lob.WRITEAPPEND (  v_finalclob, LENGTH(v_headerStr), v_headerStr );
   dbms_lob.APPEND (  v_finalclob, v_xml);
    dbms_lob.WRITEAPPEND (  v_finalclob, 9, '</ROWSET>' );


  -- plog.DEBUG(pCTX,'v_xml-final2'||v_finalclob);
--    plog.DEBUG(pCTX,'final-'||v_xml);



  RETURN v_finalclob;
END;

END Pkg_Phi;
/


