set define off;

DECLARE
 col_count NUMBER;
BEGIN
 select count(*) into col_count
 from all_tab_columns where owner = 'ESCH' and
 table_name = 'SCH_LINEITEM' and column_name = 'SUBCOST_ITEM_FLAG';
 if (col_count == 0) then
   execute immediate 'alter table esch.SCH_LINEITEM add SUBCOST_ITEM_FLAG NUMBER DEFAULT 0';
 end if;
END;
/

