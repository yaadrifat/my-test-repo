1) #D-FIN25, Day-0 implementation
Currently the eResearch application does not support Day 0 when the user creates a calendar.
This enhancement is all about to make existing calendar consistent with the application as new retrieval criteria for displaying and identifying events/visits for calendar.
This phase-1 of the enhancement released in build # 566 should be understood as the groundwork for the next phase which will allow application accepting day 0 as visit interval for a calendar.

Following are the changes done to the application, explained in 2 cases below.
=============================================================================
Case 1. While retrieving default events in "Select Events" tab of calendar application now consider retrieving only those events for which FK_VISIT is null(Table name:EVENT_DEF OR EVENT_ASSOC) .
Earlier we used to filter it out on the basis of displacement equal to 0. We did it because after phase 2 release we will allow the application
to accept day 0 as visit interval hence displacement will be 0 for visit linked events too.

Case 2. While retrieving visit linked events in "schedule and customize" tab of calendar, application now consider retrieving only those events for which FK_VISIT(Table name:EVENT_DEF OR EVENT_ASSOC) is not null (instead of how it used do before i.e. for retrieving visit linked events, application used to consider displacement NOT equal to 0 or displacement equal to NULL ).

Similar implementation has been done for 'Add Events to visit' link under schedule and customize tab,Coverage analysis and event-visit grid tab of calendar.
 

So,We need to test the following sections in order to make the existing calendar functionlity consistent with the application, making sure all works both for the exisiting calendar and new calendars, exisiting schedule and new schedule etc.
1.Calendar library.
2.Protocol Calendar.
3.Patient Schedule
4.Calendar reports.
5.Patient schedulecalendar report.
6.visit calendar report.


2) Edit Visit dialog in Patient Schedule
In D-FIN22a 4.c., it is mentioned that the Coverage Type can be configured. This is implemented using velosConfig.js which is in [eresearch_dir]\server\eresearch\deploy\eres.war\jsp\js\velos\

A new variable called PAT_SCHED_EDIT_COVERAGE_MANDATORY is added to velosConfig.js. In the Edit Visit dialog of Patient Schedule, this makes the Coverage type entry for an event mandatory or optional.
  Possible values:
    1: Makes Coverage Type entry mandatory for an event that is being edited.
    0: Makes Coverage Type entry optional.
By default, it is 0, that is, optional.

To customize this value, use velosConfigCustom.js as mentioned previously.
