<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page language = "java" import="com.aithent.audittrail.reports.EJBUtil,com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.LC"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>

  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "java.util.*,java.text.*,com.velos.eres.service.util.*,com.velos.eres.web.storage.*,com.velos.eres.web.storageStatus.*,com.velos.eres.business.common.*,com.velos.eres.web.storageAllowedItems.*"%>
  <jsp:useBean id="storageB" scope="page" class="com.velos.eres.web.storage.StorageJB"/>
   <jsp:useBean id="storageBEmpty" scope="page" class="com.velos.eres.web.storage.StorageJB"/>
  <jsp:useBean id="storageStatB" scope="page" class="com.velos.eres.web.storageStatus.StorageStatusJB"/>

   <%!
    public void storeAllowedContentDetails(String[] specimenList,String[] storageList , int fkStorage,String usr ,String ipAdd) {

         if(specimenList != null ) {
	        CodeDao cd = new CodeDao();
	        int pkCodelst = cd.getCodeId("item_type","specimen");
		     storeDatafronList(specimenList,"specimen",ipAdd,usr,fkStorage,pkCodelst);
	   }
	   if(storageList != null ) {
	        CodeDao cd = new CodeDao();
	        int pkCodelst = cd.getCodeId("item_type","storage_u");
		    storeDatafronList(storageList,"storage",ipAdd,usr,fkStorage,pkCodelst);
	   }

	 }
  public void storeDatafronList(String[] dataList, String listType,String ipAdd,String usr,int fkStorage,int pkCodelst) {
               int sret = 0;

	         for(int i = 0 ; i<dataList.length;i++) {

	           StorageAllowedItemsJB storageAllowedItemJB  = new StorageAllowedItemsJB();
	           storageAllowedItemJB.setIpAdd(ipAdd);
	           storageAllowedItemJB.setCreator(usr);
	           storageAllowedItemJB.setFkStorage(fkStorage+"");

		       if(listType.equals("specimen")){
		        storageAllowedItemJB.setFkCodelstSpecimenType(dataList[i]);
		      }
		     else {
		       storageAllowedItemJB.setFkCodelstStorageType(dataList[i]);

		     }
		       storageAllowedItemJB.setAllowedItemType(pkCodelst+"");
	           sret=storageAllowedItemJB.setStorageAllowedItemsDetails();
	      }

	  }

  %>

  <%

	String src = null;
    String storageCoordinateX="";
	String storageCoordinateY="";

	src = request.getParameter("srcmenu");
	String eSign = request.getParameter("eSign");
	String mode = request.getParameter("mode");
	String pkStorage = request.getParameter("pkStorage");

    String storageId = request.getParameter("storageId").trim();
	storageId=(storageId==null)?"":storageId;
    String storageName = request.getParameter("storageName").trim();
	String storeType = request.getParameter("storetype");
	String childStoreType = request.getParameter("childstoretype");
	String storageUnitCap = request.getParameter("storageUnitCap");
	String capUnit = request.getParameter("capunit");
	if(capUnit.equals("")) capUnit = null;

	String dimA = request.getParameter("dimA");

	if (! StringUtil.isEmpty(dimA))
	{
			dimA = dimA.trim();
	}
	String namingConv = request.getParameter("namingConv");
	namingConv = (namingConv  == null)?"":(namingConv );
	String positioning = request.getParameter("positioning");
	positioning = (positioning  == null)?"":(positioning);

	String dimB = request.getParameter("dimB");


	if (! StringUtil.isEmpty(dimB))
	{
			dimB = dimB.trim();
	}

	String namingConvn = request.getParameter("namingConvn");
	namingConvn = (namingConvn  == null)?"":(namingConvn);
	String positioningn = request.getParameter("positioningn");
	positioningn = (positioningn  == null)?"":(positioningn);
	String notes = request.getParameter("notes");
	notes	 = (notes  == null)?"":(notes);

	String oldLocation = "";

	//String modFlag = request.getParameter("modFlag");

	String template = request.getParameter("template");
	String multiSpecimen = request.getParameter("multiSpecimen");
	//Added for Template records insertion.
	if (template != null)
	    template = "1";
	else
	    template ="0";
		
	if (multiSpecimen != null)
	    multiSpecimen = "1";
	else
	    multiSpecimen ="0";
		
	int retVal =0;//KM

    storageCoordinateX = request.getParameter("strCoordinateX");
    storageCoordinateY = request.getParameter("strCoordinateY");
    String mainFKStorage = request.getParameter("mainFKStorage");

    if (StringUtil.isEmpty(mainFKStorage))
	{
		mainFKStorage = "";
	}

	if (StringUtil.isEmpty(storageCoordinateX))
	{
		storageCoordinateX = "";
	}

	if (StringUtil.isEmpty(storageCoordinateY))
	{
		storageCoordinateY = "";
	}

	String storageStatusCode = null;
	if(mode.equals("N")) {
		storageStatusCode = request.getParameter("storageStatus");
	}
	else
	{
		StorageStatusDao strStatDao = storageStatB.getStorageStausValues(EJBUtil.stringToNum(pkStorage));
		ArrayList fkCodeLstStorStats = strStatDao.getFkCodelstStorageStatuses();

		if (fkCodeLstStorStats.size()>0){
		storageStatusCode = ((fkCodeLstStorStats.get(0)) == null)?"":(fkCodeLstStorStats.get(0)).toString();
		}
	}


	String statDate = request.getParameter("statDate");
	String statusNotes = request.getParameter("statusNotes");

	String stdy = request.getParameter("selStudyIds");
	stdy = (stdy==null)?"":stdy;

	String user = request.getParameter("creatorId");
    user = (user==null)?"":user;
          String specimenList[] = null ;
	  String storageList[] = null;
	  int blankStPK = 0;


	//JM: 31Jul2009: #invp2.15

	String templateTyp = request.getParameter("templatetype");
	String alternateId = request.getParameter("alternate_Id");
	String storUnitclass = request.getParameter("storageunitclass");

	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

   {
     %>
     <jsp:include page="sessionlogging.jsp" flush="true"/>
     <%

   	String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign))
	{
%>
  	   <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	}
	else
  	{

		String ipAdd = (String) tSession.getValue("ipAdd");
		String accId = (String) tSession.getValue("accountId");
    		String usr = null;
		String oldDimA ="";
        	String oldDimB ="";
		String oldNamingConv ="";
		String oldNamingConvn ="";
		String oldChildStrType = "";
		usr = (String) tSession.getValue("userId");
		int ret=0;
		int sret=0;
		int cret=0;

		int dim1 =EJBUtil.stringToNum(dimA);
		int dim2 =EJBUtil.stringToNum(dimB);
		int cellCap = dim1*dim2;



	//JM: 25Jan2008: find out if the parent type is a 'Kit'
		String storageParentIsKit = storageB.findParentStorageType(mainFKStorage);
		if (storageParentIsKit==null)
		storageParentIsKit = "";

		if (storageParentIsKit.equals("Kit")){

			ret = -5;

		}
		else{

		if ((mode.equals("N") || mode.equals("M")) && storageId.equals("")) {	//KM
		storageId = storageB.getStorageIdAuto(storeType, EJBUtil.stringToNum(accId));
		storageId=(storageId==null)?"":storageId;

		}
		else{
		int count=0;
		count = storageB.getCountId(storageId,accId);
		if(count ==0)
			storageB.getStorageIdAuto();
		}


		if(mode.equals("M")) {
			storageB.setPkStorage(EJBUtil.stringToNum(pkStorage));
			storageB.getStorageDetails();
			oldDimA = storageB.getStorageDim1CellNum();
			oldDimB = storageB.getStorageDim2CellNum();
			oldNamingConv = storageB.getStorageDim1Naming();
			oldNamingConvn = storageB.getStorageDim2Naming();
			oldChildStrType = storageB.getFkCodelstChildStype();
			if(oldChildStrType ==null)
				oldChildStrType ="";

			oldLocation = 	storageB.getFkStorage();



			if(! StringUtil.isEmpty(oldLocation))
			{
			  if (! oldLocation.equals(mainFKStorage))
			  { // create a blank srorage for original location

				  //storageBEmpty.setPkStorage(EJBUtil.stringToNum(pkStorage));
				  //storageBEmpty.getStorageDetails();


				  storageBEmpty.setStorageName("Empty Location");
				  storageBEmpty.setCreator(usr);
				  storageBEmpty.setIpAdd(ipAdd);
				  storageBEmpty.setStorageId(
				          storageBEmpty.getStorageIdAuto(storeType, EJBUtil.stringToNum(accId)));

				  storageBEmpty.setFkCodelstStorageType( storageB.getFkCodelstStorageType());

				  storageBEmpty.setStorageCoordinateX(storageB.getStorageCoordinateX());
				  storageBEmpty.setStorageCoordinateY(storageB.getStorageCoordinateY());

				  storageBEmpty.setAccountId(accId);
		   		  storageBEmpty.setFkStorage(storageB.getFkStorage());
		  		  storageBEmpty.setTemplate(storageB.getTemplate());
				  storageBEmpty.setMultiSpecimenStorage(storageB.getMultiSpecimenStorage());	


				  //System.out.println("blankStPK" + storageBEmpty.getId());
				   blankStPK=storageBEmpty.setStorageDetails();

				   System.out.println("blankStPK" + blankStPK);

				   specimenList = request.getParameterValues("aiSpecimenType");
				   storageList  = request.getParameterValues("aiStorageType");
				   storeAllowedContentDetails(specimenList,storageList,blankStPK,usr,ipAdd) ;

				   //add storage status
				   	StorageStatusJB newStatB = new StorageStatusJB();
					newStatB.setFkStorage(String.valueOf(blankStPK));

			         String dummystatDate = "";
					Date dt = new java.util.Date();
					dummystatDate = DateUtil.dateToString(dt);

					newStatB.setSsStartDate(dummystatDate);

		         	newStatB.setFkCodelstStorageStat(request.getParameter("defStatusCode"));

					newStatB.setFkUser(user);
					newStatB.setIpAdd(ipAdd);
					newStatB.setCreator(usr);
					newStatB.setStorageStatusDetails();

				   /////////////
			  }
			}

		}

		storageB.setStorageId(storageId);
		storageB.setStorageName(storageName);
		storageB.setFkCodelstStorageType(storeType);
		storageB.setStorageCapNum(storageUnitCap);
		storageB.setFkCodelstCapUnits(capUnit);
		//storageB.setStorageStatus(subTypeStatus);//KM

		storageB.setStorageDim1CellNum(dimA);
		storageB.setStorageDim1Naming(namingConv);
		storageB.setStorageDim1Order(positioning);
		storageB.setStorageDim2CellNum(dimB);
		storageB.setStorageDim2Naming(namingConvn);
		storageB.setStorageDim2Order(positioningn);
		storageB.setStorageNotes(notes);
		storageB.setStorageAvailUnitCnt(cellCap+"");
		storageB.setStorageCoordinateX(storageCoordinateX);
		storageB.setStorageCoordinateY(storageCoordinateY);
		//storageB.setStorageLabel();
		storageB.setIpAdd(ipAdd);
		storageB.setFkCodelstChildStype(childStoreType);//KM
		storageB.setAccountId(accId);//KM-AccountId added as per the new requirment.

		storageB.setFkStorage(mainFKStorage);//for parent storage

		storageB.setTemplate(template);//KM
		storageB.setMultiSpecimenStorage(multiSpecimen);
    //JM: 31Jul2009: invp2.15 (part-1)
		storageB.setStorageTemplateType(templateTyp);
		storageB.setStorageAlternateId(alternateId);
		storageB.setStorageUnitClass(storUnitclass);


		if(mode.equals("N")) {
			storageB.setCreator(usr);
			ret=storageB.setStorageDetails();

			pkStorage = String.valueOf(ret);

			specimenList = request.getParameterValues("aiSpecimenType");
			storageList  = request.getParameterValues("aiStorageType");
			storeAllowedContentDetails(specimenList,storageList,ret,usr,ipAdd) ;

              }

		if(oldDimA == null) oldDimA="";
		if(oldDimB == null) oldDimB="";

		if(mode.equals("M")) {
			storageB.setModifiedBy(usr);
			//ret=storageB.updateStorage();
			//KM-to fix the issue 3138
			if( !((oldDimA.equals(dimA)) && (oldDimB.equals(dimB)) && (oldNamingConv.equals(namingConv)) && (oldNamingConvn.equals(namingConvn)) && (oldChildStrType.equals(childStoreType)))) {
			ArrayList pkStorages = new ArrayList();
			StorageDao strdao = storageB.getStorageValue(EJBUtil.stringToNum(pkStorage));
			pkStorages = strdao.getPkStorages();
			String strArrStrs[] = new String[pkStorages.size()];

			for(int cnt=0;cnt<pkStorages.size();cnt++)
			{
					strArrStrs[cnt] = pkStorages.get(cnt).toString();

			}
			int flag = 2;

			if (strArrStrs.length > 0)//KM
				//Modified for INF-18183 ::: Akshi
				retVal = storageB.deleteStorages(strArrStrs,flag,EJBUtil.stringToNum(usr),AuditUtils.createArgs(session,"",LC.L_Manage_Invent));

		 }

		   ArrayList<Integer> specimenChckBoxList = (ArrayList<Integer>)tSession.getAttribute("specimenlist");
		   ArrayList<Integer> storageChckBoxList = (ArrayList<Integer>)tSession.getAttribute("storagelist");
		   boolean specFlag = true;
		   boolean storFlag = true;

		   if(specimenList != null) {
		    for(int i = 0;i<specimenList.length; i++) {
                    specFlag = false;
		    for(int j = 0;j<specimenChckBoxList.size();j++){
		       if(new String(specimenList[i]).equals( (specimenChckBoxList.get(j).toString()))){
		         specFlag = true;
		          break;
		     }
		  }
		  if(!specFlag)
		    break;
	          }
                 }

		if(storageList != null) {
 		 for(int i = 0;i<storageList.length; i++) {
                   storFlag = false;
		   for(int j = 0;j<storageChckBoxList.size();j++){
		   if(new String(storageList[i]).equals( (storageChckBoxList.get(j).toString()))){
		     storFlag = true;
		     break;

		   }
 	       }

  	        if(!storFlag)
		    break;

                 }
               }
			if(specFlag || storFlag){
			
		    StorageAllowedItemsDao  storageAllowedItemsDao  = new StorageAllowedItemsDao();
		    specimenList = request.getParameterValues("aiSpecimenType");
			storageList  = request.getParameterValues("aiStorageType");
		   	//Modified for Bug #7267 AGodara
		   	storageAllowedItemsDao.deleteStorageAllowedItems(Integer.parseInt(pkStorage),specimenChckBoxList,specimenList,storageChckBoxList,storageList,AuditUtils.createArgs(tSession,"",LC.L_Manage_Invent));
			storeAllowedContentDetails(specimenList,storageList,Integer.parseInt(pkStorage),usr,ipAdd) ;
		  }
	      if(retVal != -4) {
	          ret = storageB.updateStorage();

		  }

		  //update the child storage Names

		  String arSelectedChild[] = null;

		  String childStrpk = "";
		  String childStrName = "";
		  String childStrType = "";
		  int childcount = 0;

		  childcount =  EJBUtil.stringToNum(request.getParameter("childcount"));

		  //System.out.println("childcount " + childcount );

		  if (childcount > 1)
		  {
		  	arSelectedChild = request.getParameterValues("selectEdit");
		  } else if (childcount == 1)
		  {

		  	childStrpk = request.getParameter("selectEdit");

		  	if (! StringUtil.isEmpty(childStrpk))
		  	{
		  	arSelectedChild = new String[1];
		  	arSelectedChild[0] = childStrpk;
		  	}

		  //	System.out.println("childStrpk " + childStrpk );
		  	//System.out.println("arSelectedChild.length " + arSelectedChild.length );
		  }

		  if (arSelectedChild != null)
		  {
			  for (int o = 0; o< arSelectedChild.length; o++)
				{
					childStrpk = arSelectedChild[o];

					//System.out.println("childStrpk -arr " + childStrpk );

					childStrName = request.getParameter(childStrpk + "_name_e");
					childStrType = request.getParameter(childStrpk + "_type");

					StorageJB sjbc = new StorageJB();

					sjbc.setPkStorage(EJBUtil.stringToNum(childStrpk));
					sjbc.getStorageDetails();

					sjbc.setStorageName(childStrName);
					sjbc.setFkCodelstStorageType(childStrType);
					//sjbc.setMultiSpecimenStorage(multiSpecimen);
					sjbc.setModifiedBy(usr);
					sjbc.setIpAdd(ipAdd);
					sjbc.updateStorage();

				}
			}

		}

		//KM-to fix the issue 3138
		if(ret>=0 && (!(mode.equals("M") && (oldDimA.equals(dimA)) &&(oldDimB.equals(dimB)) && (oldNamingConv.equals(namingConv))      && (oldNamingConvn.equals(namingConvn)) &&(oldChildStrType.equals(childStoreType)) ))   &&(retVal!=-4)    ) {

		String a[]=new String[cellCap];

		int m=0;
		int n=0;
		int r=0;

		if(namingConv.equals("azz")) {
		for (int k=1;k<=dim1;k++) {
		     for (int l=1;l<=dim2;l++) {
				if(k > 26) {
				   n=Math.abs(k/26);
                   r = k%26;
		   	       a[m] = storageName+"-"+(char)(n+64)+""+(char)(r+64)+""+l;//KM-100307
				}
				else {
				  a[m] = storageName+"-"+(char)(k+64)+""+l;
				}
			    m++;
			}
	    }
		}

      if(namingConv.equals("zza")) {
		for (int k=1;k<=dim1;k++) {
		     for (int l=1;l<=dim2;l++) {
				n=Math.abs(k/26);
				if(k>26)
				   r = k%26;
				else
				   r=k;
				a[m] = storageName+"-"+(char)(90-n)+""+(char)(91-r)+""+l;//KM-100307
				m++;
			}
	      }
	   }

		int s=0;
		int t=0;

		if (((positioning.equals("LR") && namingConv.equals("azz")) ||((positioning.equals("LR") && namingConv.equals("zza")))) &&
			((positioningn.equals("BF")  && namingConvn.equals("1n")) || (positioningn.equals("FB")  && namingConvn.equals("n1")))) {
		   s=1;
		   t=1;
		}

		if (((positioning.equals("LR") && namingConv.equals("azz")) || ((positioning.equals("LR") && namingConv.equals("zza")))) &&
			((positioningn.equals("FB")  && namingConvn.equals("1n")) || (positioningn.equals("BF")  && namingConvn.equals("n1")))) {
		   s=1;
		   t=dim2;
		}

		if (((positioning.equals("RL") && namingConv.equals("azz")) ||  ((positioning.equals("RL") && namingConv.equals("zza"))))  &&
			((positioningn.equals("FB")  && namingConvn.equals("1n")) || (positioningn.equals("BF")  && namingConvn.equals("n1")))) {
		   s=dim1;
		   t=dim2;
		}


		if (((positioning.equals("RL") && namingConv.equals("azz")) || ((positioning.equals("RL") && namingConv.equals("zza")))) &&
			((positioningn.equals("FB")  && namingConvn.equals("n1")) || (positioningn.equals("BF")  && namingConvn.equals("1n")))) {
		   s=dim1;
		   t=1;

		}

		if (cellCap > 1) {
		//KM-122407
		  for (int j=0;j<cellCap;j++) {
			StorageJB strB = new StorageJB();

			if (mode.equals("N") || mode.equals("M")) {
				// storageId = storageB.getStorageIdAuto();
				// Child storage ID will have the same format as parent storage ID; not the case for specimen
				storageId = storageB.getStorageIdAuto(childStoreType, EJBUtil.stringToNum(accId));
				storageId=(storageId==null)?"":storageId;
				strB.setCreator(usr);
			}

			strB.setStorageId(storageId);
			strB.setStorageName(a[j]);
			//strB.setStorageStatus(subTypeStatus);	//KM
			if(mode.equals("N")){
			   strB.setFkStorage(ret+"");

			}
			if(mode.equals("M"))
			   strB.setFkStorage(pkStorage);

			strB.setFkCodelstStorageType(childStoreType);
			//strB.setStorageStatus(subTypeStatus);//KM
			strB.setStorageCapNum("1");//KM-100307
			strB.setFkCodelstCapUnits(capUnit);
			strB.setIpAdd(ipAdd);
			strB.setAccountId(accId);
			//strB.setTemplate(template);


		   if (((positioning.equals("LR") && namingConv.equals("azz")) ||((positioning.equals("LR") && namingConv.equals("zza")))) &&
			((positioningn.equals("BF")  && namingConvn.equals("1n")) || (positioningn.equals("FB")  && namingConvn.equals("n1")))) {
			  strB.setStorageCoordinateX(s+"");
			  strB.setStorageCoordinateY(t+"");
			if(t<=dim2)
			   t=t+1;

			if(t>dim2){
			   t=1;
			   s=s+1;
		    }
			}


			if (((positioning.equals("LR") && namingConv.equals("azz")) || ((positioning.equals("LR") &&namingConv.equals("zza"))))&&
			((positioningn.equals("FB")  && namingConvn.equals("1n")) || (positioningn.equals("BF")  && namingConvn.equals("n1")))) {

			  strB.setStorageCoordinateX(s+"");
			  strB.setStorageCoordinateY(t+"");
			if(t<=dim2)
			   t=t-1;
			if(t<1){
			   t=dim2;
			   s=s+1;
		    }
			}

		  if(((positioning.equals("RL") && namingConv.equals("azz")) || ((positioning.equals("RL") && namingConv.equals("zza")))) &&
			((positioningn.equals("FB")  && namingConvn.equals("1n")) || (positioningn.equals("BF")  && namingConvn.equals("n1")))) {
			  strB.setStorageCoordinateX(s+"");
			  strB.setStorageCoordinateY(t+"");

			if(t<=dim2)
			   t=t-1;
			if(t<1){
			   t=dim2;
			   s=s-1;
		    }
			}


			if(((positioning.equals("RL") && namingConv.equals("azz")) || ((positioning.equals("RL") && namingConv.equals("zza")))) &&
			((positioningn.equals("FB")  && namingConvn.equals("n1")) || (positioningn.equals("BF")  && namingConvn.equals("1n")))) {
			  strB.setStorageCoordinateX(s+"");
			  strB.setStorageCoordinateY(t+"");
			if(t<=dim2)
			   t=t+1;
			if(t>dim2){
			   t=1;
			   s=s-1;
		    }
			}

			cret = strB.setStorageDetails();
			storageList = null;
			storeAllowedContentDetails(specimenList,storageList,cret,usr,ipAdd) ;

			StorageStatusJB strStatB = new StorageStatusJB();
			strStatB.setFkStorage(cret+"");
			if(statDate == null) {
				Date dt = new java.util.Date();
				statDate = DateUtil.dateToString(dt);

			}

			strStatB.setSsStartDate(statDate);
			strStatB.setFkCodelstStorageStat(request.getParameter("defStatusCode"));
			strStatB.setSsNotes(statusNotes);
			strStatB.setFkUser(user);
			//strStatB.setSsTrackingNum("varchar");
			strStatB.setFkStudy(stdy);
			strStatB.setIpAdd(ipAdd);
			strStatB.setCreator(usr);
			sret=strStatB.setStorageStatusDetails();

		  }
		}

		if(mode.equals("N")) {
		storageStatB.setFkStorage(ret+"");
		storageStatB.setSsStartDate(statDate);
		storageStatB.setFkCodelstStorageStat(storageStatusCode);
		storageStatB.setSsNotes(statusNotes);
		storageStatB.setFkUser(user);
		//storageStatB.setSsTrackingNum("varchar");
		storageStatB.setFkStudy(stdy);
		storageStatB.setIpAdd(ipAdd);
		storageStatB.setCreator(usr);
		sret=storageStatB.setStorageStatusDetails();
		}
		}
		if(mode.equals("M")) {
					Date dt1 = new java.util.Date();
					statDate = DateUtil.dateToString(dt1);
					int codelstItem = 0, storageCount = 0, storageChildCount = 0;
					Integer partOccupiedStat = 0, occupiedStat = 0, availableStat = 0;
					CodeDao cd5 = new CodeDao();
					partOccupiedStat = cd5.getCodeId("storage_stat","PartOccupied");
					occupiedStat = cd5.getCodeId("storage_stat", "Occupied");
					availableStat = cd5.getCodeId("storage_stat", "Available");
					codelstItem = cd5.getCodeId("cap_unit", "Items");
					storageCount = storageB.getStorageCount(pkStorage);
					storageChildCount = storageB.getChildCount(pkStorage);
					if (EJBUtil.stringToNum(multiSpecimen) == 0) {
						if(storageCount==1)
						storageStatB.setFkCodelstStorageStat(EJBUtil.integerToString(occupiedStat));
						else
						storageStatB.setFkCodelstStorageStat(EJBUtil.integerToString(availableStat));
					} else {
					if(storageCount==0){
					if (EJBUtil.stringToNum(capUnit) == codelstItem){
					if(EJBUtil.stringToNum(storageUnitCap)>storageChildCount || StringUtil.isEmpty(storageUnitCap))
					storageStatB.setFkCodelstStorageStat(EJBUtil.integerToString(availableStat));
					else if(EJBUtil.stringToNum(storageUnitCap)==storageChildCount)
					storageStatB.setFkCodelstStorageStat(EJBUtil.integerToString(occupiedStat));
					}
					else
					storageStatB.setFkCodelstStorageStat(EJBUtil.integerToString(availableStat));
					}
					else
					{
						if (EJBUtil.stringToNum(capUnit) == codelstItem) {
							if (((EJBUtil.stringToNum(storageUnitCap) - storageChildCount) > storageCount)	|| (StringUtil.isEmpty(storageUnitCap)))
								storageStatB.setFkCodelstStorageStat(EJBUtil.integerToString(partOccupiedStat));
							else if ((EJBUtil.stringToNum(storageUnitCap) - storageChildCount) == (storageCount))
								storageStatB.setFkCodelstStorageStat(EJBUtil.integerToString(occupiedStat));
						} else
							storageStatB.setFkCodelstStorageStat(EJBUtil.integerToString(partOccupiedStat));
					}
					}
				storageStatB.setFkStorage(pkStorage);
				storageStatB.setSsStartDate(statDate);
				storageStatB.setSsNotes(statusNotes);
				storageStatB.setFkUser(request.getParameter("creatorId"));
				storageStatB.setFkStudy(stdy);
				storageStatB.setIpAdd(ipAdd);
				storageStatB.setCreator((String)tSession.getValue("userId"));
				sret=storageStatB.setStorageStatusDetails();
		 }

	}//kit else part...

%>

	  <br>
      <br>
      <br>
      <br>
      <br>
	  <%


	  if(retVal != -4) {

	  if (ret >=0){
	  %>
      <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
	  <META HTTP-EQUIV=Refresh CONTENT="1; URL=storageunitdetails.jsp?srcmenu=tdmenubaritem6&selectedTab=2&pkStorage=<%=pkStorage%>&mode=M">
	  <%} else if(ret == -2) {%>
	  <p class = "successfulmsg" align = center> <%=MC.M_DataNotSvd_Succ%><%--Data was not saved successfully*****--%> </p>
	  <META HTTP-EQUIV=Refresh CONTENT="1; URL=storageadminbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=2&searchFrom=search">
	  <%} else if(ret == -3) {%>
	  <table align="center">
	  <tr>
	  <td align ="center">
  	  <p class = "successfulmsg" align = center> <%=MC.M_StrNameExst_EtrAgain%><%--The Storage Name you have entered already exists. Please click on Back button and enter it again*****--%> </p>
	  </td>
	  </tr>
	  <tr height=20></tr>
	  <tr>
	  <td align="center">
	  <button onclick="history.go(-1);"><%=LC.L_Back%></button>
	  </td>
	  <tr>
	  </table>
      <%} else if(ret == -4){%>
	  <table align="center">
	  <tr>
	  <td align ="center">
  	  <p class = "successfulmsg" align = center> <%=MC.M_StrIdExst_EtrAgain%><%--The Storage ID you have entered already exists. Please click on Back button and enter it again*****--%> </p>
	  </td>
	  </tr>
	  <tr height=20></tr>
	  <tr>
	  <td align="center">
	  <button name="back" onClick="window.history.back();"><%=LC.L_Back%></button>
	  </td>
	  <tr>
	  </table>
	  <%}

	  else if(ret == -5){%>
	  <table align="center">
	  <tr>
	  <td align ="center">
  	  <p class = "successfulmsg" align = center> <%=MC.M_StrKitCnt_CreateManually%><%--Parent storage is a 'Kit', so Storage children can not be created manually by changing the location. Please click on Back button to go to the parent page*****--%></p>
	  </td>
	  </tr>
	  <tr height=20></tr>
	  <tr>
	  <td align="center">
	  <button onclick="history.go(-1);"><%=LC.L_Back%></button>
	  </td>
	  <tr>
	  </table>
	  <%}


	  } //KM-Added to check child records link with specimens.

	  else if(retVal == -4)
		{%>
 	  <table align="center">
	  <tr>
	  <td align ="center">
		<p class = "successfulmsg" align = center> <%=MC.M_StrDimCntChg_TryAgain%><%--Storage Dimensions cannot be changed. There are one or more child storage records linked with specimen(S).<br>Please remove the association with the related specimen records and try again later.*****--%> </p>
      </td>
	  </tr>
	  <tr height=20></tr>
	  <tr>
	  <td align="center">
	  <button onclick="history.go(-1);"><%=LC.L_Back%></button>
	  </td>
	  <tr>
	  </table>

	  <%	}
	  %>

<%

}//end of if for eSign check

}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
	<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>

</BODY>
</HTML>
