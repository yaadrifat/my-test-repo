/* This readMe is specific to eSample (version 9.0.0 build #637-ES005) */

=====================================================================================================================================
eSample bugs covered in the build:

1) Bug 12314 - INV-21675:If storage have multiple specimen ,show specimen ID instead of "PK"
2) Bug 12284 - Issue: "Status" of the storage admin got changed
3) Bug 12283 - Issue: Functionality of "copy storage unit /Template" is not working
4) Bug 12239 - INV-21675 Issue: Storage storing specimen beyond capacity.
5) Bug 12142 - If user "deletes" the parent specimen, storage "status" still remains "occupied"

Following existing files have been modified:
	
	1. ajaxGetSpecimenList.jsp				(...\deploy\velos.ear\velos.war\jsp)
	2. deletespecimens.jsp					(...\deploy\velos.ear\velos.war\jsp)
	3. storageunitdetails.jsp				(...\deploy\velos.ear\velos.war\jsp)
	4. updatespecimendetails.jsp			(...\deploy\velos.ear\velos.war\jsp)
	5. updatestorageunitdetails.jsp			(...\deploy\velos.ear\velos.war\jsp)
=====================================================================================================================================