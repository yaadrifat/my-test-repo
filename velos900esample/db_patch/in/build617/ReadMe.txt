/* This readMe is specific to Velos eResearch version 9.0 build #617 */

=====================================================================================================================================
Garuda :
	ResquestSigning-1.0.jar has been modified in this build.Also we modified the jar file in CVS/doc/tools/maven/jars-garuda path. 
	Please take the new jar file from the CVS/doc/tools/maven/jars-garuda path and run the storeJarsInMavenForGaruda.bat file again.

	Imp:- Please delete the garuda_buisiness exploded folder from the path "server\eresearch\deploy\velos.ear\velos.war\WEB-INF\lib" before build.

	Imp:- For development purpose, add garuda1_business-1.0.jar to build path after building project. You can find this jar in \GarudaMissingJar folder. 
	This will be available after building your project.
=====================================================================================================================================
eResearch:
1. Please refer to enclosed Type_Category.xlsx file for the list of all the places where Label text 'Type' is replaced 
   by text 'Category'. 

=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1	confirm.js
2	groupRights.jsp
3	invbrowsercommon.jsp
4	labelBundle.properties
5	LC.java
6	LC.jsp
7	MC.java
8	MC.jsp
9	mdynpreview.jsp
10	messageBundle.properties
11	ofcal.js
12	patientschedule.jsp
13	previewPortal.jsp
14	reportcentral.jsp
15	ui-include.jsp
16	updatemilestonerule.jsp
17	updateNewStudy.jsp
18	updatequickpatientdetails.jsp
19	viewevent.jsp
=====================================================================================================================================
