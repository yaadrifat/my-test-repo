set define off;
begin
  update ER_CODELST set CODELST_CUSTOM_COL1='../images/jpg/room.jpg' where CODELST_TYPE='store_type' and CODELST_SUBTYP='Room';
  update ER_CODELST set CODELST_CUSTOM_COL1='../images/jpg/building.jpg' where CODELST_TYPE='store_type' and CODELST_SUBTYP='Building';
  commit;
end;
/