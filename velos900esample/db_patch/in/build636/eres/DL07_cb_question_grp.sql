--INSERTING DATA INTO CB_QUESTION_GRP--

--IDM N2F FORM--

--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC is null),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cms_cert_lab_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fda_lic_mfg_inst_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC is null),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='fda_lic_mfg_inst_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_cms'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_fda'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hbc_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hbc_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hbc_cms'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hbc_fda'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_cms'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_fda'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_o_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_o_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_o_cms'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_o_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_o_fda'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_cms'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_1_2_fda'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv1_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv1_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv1_cms'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv1_fda'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_total_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_total_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_total_cms'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cmv_total_fda'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_1_hcv_hbv_mpx_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_1_hcv_hbv_mpx_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_1_hcv_hbv_mpx_cms'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_1_hcv_hbv_mpx_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_1_hcv_hbv_mpx_fda'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_react_cms'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_react_fda'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_rslt'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_cms'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_fda'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hcv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hcv_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hcv_react_cms'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hcv_react_fda'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hbv_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hbv_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hbv_react_cms'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_hbv_react_fda'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='syphilis_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='syphilis_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='syphilis_react_cms'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_react_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='syphilis_react_fda'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_west_nile_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_west_nile_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_west_nile_react_cms'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='nat_west_nile_react_fda'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='chagas_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='chagas_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='chagas_react_cms'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.c--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_fda')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Screening Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='chagas_react_fda'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_ct_sup_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='syphilis_ct_sup_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_ct_sup_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='syphilis_ct_sup_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='syphilis_ct_sup_react_cms')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='syphilis_ct_sup_react_cms'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='treponemal_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='treponemal_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 20--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Additional / Miscellaneous IDM Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='other_idm_1_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_1_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Additional / Miscellaneous IDM Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='other_idm_1_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_2_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Additional / Miscellaneous IDM Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='other_idm_2_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_2_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Additional / Miscellaneous IDM Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='other_idm_2_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 22--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_3_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Additional / Miscellaneous IDM Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='other_idm_3_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 22.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_3_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Additional / Miscellaneous IDM Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='other_idm_3_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_4_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Additional / Miscellaneous IDM Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='other_idm_4_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_4_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Additional / Miscellaneous IDM Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='other_idm_4_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_5_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Additional / Miscellaneous IDM Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='other_idm_5_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_5_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Additional / Miscellaneous IDM Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='other_idm_5_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 25--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_6_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Additional / Miscellaneous IDM Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='other_idm_6_react_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 25.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_idm_6_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Additional / Miscellaneous IDM Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='other_idm_6_react_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
