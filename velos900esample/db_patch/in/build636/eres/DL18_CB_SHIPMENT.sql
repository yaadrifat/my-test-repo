--STARTS ADDING COLUMN FROM CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;  
  v_flag number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_SHIPMENT' AND COLUMN_NAME = 'TEMP_COL';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_SHIPMENT ADD TEMP_COL NUMBER(10,0)';
		v_flag := 1;
    commit;
  end if;
 if (v_flag = 1) then
	 execute immediate 'UPDATE CB_SHIPMENT SET TEMP_COL=SHIPMENT_TRACKING_NO';
	commit;
end if;
end;
/

--STARTS MODIFYING COLUMN FROM CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;  
   v_flag number:= 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_SHIPMENT' AND COLUMN_NAME = 'SHIPMENT_TRACKING_NO';
  if (v_column_exists = 1) then 
      UPDATE CB_SHIPMENT SET SHIPMENT_TRACKING_NO=NULL;
	commit;
      execute immediate 'alter table CB_SHIPMENT MODIFY SHIPMENT_TRACKING_NO varchar2(100)';
      v_flag := 1;
      commit;
  end if;
   if (v_flag = 1) then
  execute immediate 'UPDATE CB_SHIPMENT SET SHIPMENT_TRACKING_NO=TEMP_COL';
       commit;
  end if;
end;
/


--STARTS MODIFYING COLUMN FROM CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_SHIPMENT' AND COLUMN_NAME = 'TEMP_COL';
  if (v_column_exists = 1) then
       execute immediate 'ALTER TABLE CB_SHIPMENT DROP COLUMN TEMP_COL';
	commit;
  end if;	
end;
/



--STARTS ADDING COLUMN FROM CB_PACKING_SLIP TABLE--
DECLARE
  v_column_exists number := 0;  
  v_flag number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_PACKING_SLIP' AND COLUMN_NAME = 'TEMP_COL';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_PACKING_SLIP ADD TEMP_COL NUMBER(10,0)';
		v_flag := 1;
    commit;
  end if;
 if (v_flag = 1) then
	 execute immediate 'UPDATE CB_PACKING_SLIP SET TEMP_COL=SHIPMENT_TRACKING_NO';
	commit;
end if;
end;
/

--STARTS MODIFYING COLUMN FROM CB_PACKING_SLIP TABLE--
DECLARE
  v_column_exists number := 0;  
   v_flag number:= 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_PACKING_SLIP' AND COLUMN_NAME = 'SHIPMENT_TRACKING_NO';
  if (v_column_exists = 1) then 
      UPDATE CB_PACKING_SLIP SET SHIPMENT_TRACKING_NO=NULL;
	commit;
      execute immediate 'alter table CB_PACKING_SLIP MODIFY SHIPMENT_TRACKING_NO varchar2(100)';
      v_flag := 1;
      commit;
  end if;
   if (v_flag = 1) then
  execute immediate 'UPDATE CB_PACKING_SLIP SET SHIPMENT_TRACKING_NO=TEMP_COL';
       commit;
  end if;
end;
/


--STARTS MODIFYING COLUMN FROM CB_PACKING_SLIP TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_PACKING_SLIP' AND COLUMN_NAME = 'TEMP_COL';
  if (v_column_exists = 1) then
       execute immediate 'ALTER TABLE CB_PACKING_SLIP DROP COLUMN TEMP_COL';
	commit;
  end if;	
end;
/




create or replace
TRIGGER "ERES"."CB_SHIPMENT_AU1"
AFTER UPDATE ON ERES.CB_SHIPMENT
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
	NEW_FK_SHIPMENT_TYPE VARCHAR(200);
	OLD_FK_SHIPMENT_TYPE  VARCHAR(200);
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;


	IF NVL(:OLD.FK_SHIPMENT_TYPE,0) !=     NVL(:NEW.FK_SHIPMENT_TYPE,0) THEN
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_SHIPMENT_TYPE)		INTO NEW_FK_SHIPMENT_TYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_SHIPMENT_TYPE := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:OLD.FK_SHIPMENT_TYPE)		INTO OLD_FK_SHIPMENT_TYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_SHIPMENT_TYPE  := '';
   END;
  END IF;



	IF :NEW.DELETEDFLAG = 'Y' THEN

	PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'CB_SHIPMENT',:OLD.rid,:OLD.PK_SHIPMENT,'D',:NEW.LAST_MODIFIED_BY);

	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','PK_SHIPMENT',:OLD.PK_SHIPMENT,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_ORDER_ID',:OLD.FK_ORDER_ID,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_DATE', to_char(:OLD.SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ADDI_SHIPMENT_DATE', to_char(:OLD.ADDI_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_TRACKING_NO',:OLD.SHIPMENT_TRACKING_NO,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ADDI_SHIPMENT_TRACKING_NO',:OLD.ADDI_SHIPMENT_TRACKING_NO,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','CBB_ID',:OLD.CBB_ID,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','CREATOR',:OLD.CREATOR,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','CREATED_ON', to_char(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','IP_ADD',:OLD.IP_ADD,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','LAST_MODIFIED_DATE', to_char(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','RID',:OLD.RID,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','DELETEDFLAG',:OLD.DELETEDFLAG,NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_CORD_ID',:OLD.FK_CORD_ID,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','PROP_SHIPMENT_DATE', to_char(:OLD.PROP_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ATTN_NAME',:OLD.ATTN_NAME,NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_DELIVERY_ADD',:OLD.FK_DELIVERY_ADD,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ADDI_INFO',:OLD.ADDI_INFO,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SCH_SHIPMENT_DATE', to_char(:OLD.SCH_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_SHIPPING_CONT',:OLD.FK_SHIPPING_CONT,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_TEMPERATURE_MONITER',:OLD.FK_TEMPERATURE_MONITER,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','EXPECT_ARR_DATE', to_char(:OLD.EXPECT_ARR_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_CONFIRM',:OLD.SHIPMENT_CONFIRM,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_CONFIRM_BY',:OLD.SHIPMENT_CONFIRM_BY,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_CONFIRM_ON', to_char(:OLD.SHIPMENT_CONFIRM_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_SHIP_COMPANY_ID',:OLD.FK_SHIP_COMPANY_ID,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','PADLOCK_COMBINATION',:OLD.PADLOCK_COMBINATION,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','PAPERWORK_LOCATION',:OLD.PAPERWORK_LOCATION,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ADDITI_SHIPPER_DET',:OLD.ADDITI_SHIPPER_DET,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_SHIPMENT_TYPE',OLD_FK_SHIPMENT_TYPE,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','NMDP_SHIP_EXCUSE_FLAG',:OLD.NMDP_SHIP_EXCUSE_FLAG,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','DATA_MODIFIED_FLAG',:OLD.DATA_MODIFIED_FLAG,NULL,NULL,NULL);

	ELSE

	PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'CB_SHIPMENT',:OLD.RID,:OLD.PK_SHIPMENT,'U',:NEW.LAST_MODIFIED_BY);

	IF NVL(:OLD.PK_SHIPMENT,0) != NVL(:NEW.PK_SHIPMENT,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','PK_SHIPMENT', :OLD.PK_SHIPMENT, :NEW.PK_SHIPMENT,NULL,NULL);
  END IF;
	IF NVL(:OLD.FK_ORDER_ID,0) != NVL(:NEW.FK_ORDER_ID,0) THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_ORDER_ID',:OLD.FK_ORDER_ID, :NEW.FK_ORDER_ID,NULL,NULL);
  END IF;
  IF NVL(:OLD.SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_DATE',
       TO_CHAR(:OLD.SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
 IF NVL(:OLD.ADDI_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.ADDI_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ADDI_SHIPMENT_DATE',
       TO_CHAR(:OLD.ADDI_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.ADDI_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
  IF NVL(:OLD.SHIPMENT_TRACKING_NO,'') != NVL(:NEW.SHIPMENT_TRACKING_NO,'') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_TRACKING_NO',:OLD.SHIPMENT_TRACKING_NO, :NEW.SHIPMENT_TRACKING_NO,NULL,NULL);
  END IF;
   IF NVL(:OLD.ADDI_SHIPMENT_TRACKING_NO,'') != NVL(:NEW.ADDI_SHIPMENT_TRACKING_NO,'') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ADDI_SHIPMENT_TRACKING_NO',:OLD.ADDI_SHIPMENT_TRACKING_NO, :NEW.ADDI_SHIPMENT_TRACKING_NO,NULL,NULL);
  END IF;
   IF NVL(:OLD.CBB_ID,0) != NVL(:NEW.CBB_ID,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','CBB_ID',:OLD.CBB_ID, :NEW.CBB_ID,NULL,NULL);
  END IF;
  IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT', 'CREATOR',:OLD.CREATOR, :NEW.CREATOR,NULL,NULL);
  END IF;
  IF NVL(:OLD.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','CREATED_ON',
       TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
  IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','IP_ADD',:OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);
  END IF;
	IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','LAST_MODIFIED_BY', :OLD.LAST_MODIFIED_BY, :NEW.LAST_MODIFIED_BY,NULL,NULL);
	END IF;
	IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','LAST_MODIFIED_DATE',
       TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT', 'RID', :OLD.RID, :NEW.RID,NULL,NULL);
    END IF;
	IF NVL(:OLD.DELETEDFLAG,' ') != NVL(:NEW.DELETEDFLAG,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','DELETEDFLAG', :OLD.DELETEDFLAG, :NEW.DELETEDFLAG,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_CORD_ID,0) != NVL(:NEW.FK_CORD_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_CORD_ID', :OLD.FK_CORD_ID, :NEW.FK_CORD_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.PROP_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.PROP_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','PROP_SHIPMENT_DATE',
       TO_CHAR(:OLD.PROP_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.PROP_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.ATTN_NAME,' ') != NVL(:NEW.ATTN_NAME,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ATTN_NAME', :OLD.ATTN_NAME, :NEW.ATTN_NAME,NULL,NULL);
	  END IF;
    IF NVL(:OLD.FK_DELIVERY_ADD,0) != NVL(:NEW.FK_DELIVERY_ADD,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_SHIPMENT','FK_DELIVERY_ADD', :OLD.FK_DELIVERY_ADD, :NEW.FK_DELIVERY_ADD,NULL,NULL);
    END IF;
    IF NVL(:OLD.ADDI_INFO,' ') != NVL(:NEW.ADDI_INFO,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ADDI_INFO', :OLD.ADDI_INFO, :NEW.ADDI_INFO,NULL,NULL);
	  END IF;
     IF NVL(:OLD.SCH_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.SCH_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SCH_SHIPMENT_DATE',
       TO_CHAR(:OLD.SCH_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.SCH_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_SHIPPING_CONT,0) != NVL(:NEW.FK_SHIPPING_CONT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_SHIPPING_CONT', :OLD.FK_SHIPPING_CONT, :NEW.FK_SHIPPING_CONT,NULL,NULL);
	  END IF;
	   IF NVL(:OLD.FK_TEMPERATURE_MONITER,0) != NVL(:NEW.FK_TEMPERATURE_MONITER,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_TEMPERATURE_MONITER', :OLD.FK_TEMPERATURE_MONITER, :NEW.FK_TEMPERATURE_MONITER,NULL,NULL);
	  END IF;
	  IF NVL(:OLD.EXPECT_ARR_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.EXPECT_ARR_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','EXPECT_ARR_DATE',
       TO_CHAR(:OLD.EXPECT_ARR_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.EXPECT_ARR_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
	IF NVL(:OLD.SHIPMENT_CONFIRM,' ') != NVL(:NEW.SHIPMENT_CONFIRM,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_CONFIRM', :OLD.SHIPMENT_CONFIRM, :NEW.SHIPMENT_CONFIRM,NULL,NULL);
	  END IF;
	 IF NVL(:OLD.SHIPMENT_CONFIRM_BY,0) != NVL(:NEW.SHIPMENT_CONFIRM_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_CONFIRM_BY', :OLD.SHIPMENT_CONFIRM_BY, :NEW.SHIPMENT_CONFIRM_BY,NULL,NULL);
	  END IF;
	IF NVL(:OLD.SHIPMENT_CONFIRM_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.SHIPMENT_CONFIRM_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_CONFIRM_ON',
       TO_CHAR(:OLD.SHIPMENT_CONFIRM_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.SHIPMENT_CONFIRM_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
	IF NVL(:OLD.FK_SHIP_COMPANY_ID,0) != NVL(:NEW.FK_SHIP_COMPANY_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_SHIP_COMPANY_ID', :OLD.FK_SHIP_COMPANY_ID, :NEW.FK_SHIP_COMPANY_ID,NULL,NULL);
	  END IF;
	IF NVL(:OLD.PADLOCK_COMBINATION,' ') != NVL(:NEW.PADLOCK_COMBINATION,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','PADLOCK_COMBINATION', :OLD.PADLOCK_COMBINATION, :NEW.PADLOCK_COMBINATION,NULL,NULL);
	  END IF;
	IF NVL(:OLD.PAPERWORK_LOCATION,0) != NVL(:NEW.PAPERWORK_LOCATION,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','PAPERWORK_LOCATION', :OLD.PAPERWORK_LOCATION, :NEW.PAPERWORK_LOCATION,NULL,NULL);
	  END IF;
	IF NVL(:OLD.ADDITI_SHIPPER_DET,' ') != NVL(:NEW.ADDITI_SHIPPER_DET,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ADDITI_SHIPPER_DET', :OLD.ADDITI_SHIPPER_DET, :NEW.ADDITI_SHIPPER_DET,NULL,NULL);
	  END IF;
    IF NVL(:OLD.FK_SHIPMENT_TYPE,0) != NVL(:NEW.FK_SHIPMENT_TYPE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_SHIPMENT_TYPE',OLD_FK_SHIPMENT_TYPE,NEW_FK_SHIPMENT_TYPE,NULL,NULL);
	  END IF;
    IF NVL(:OLD.NMDP_SHIP_EXCUSE_FLAG,' ') != NVL(:NEW.NMDP_SHIP_EXCUSE_FLAG,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','NMDP_SHIP_EXCUSE_FLAG', :OLD.NMDP_SHIP_EXCUSE_FLAG, :NEW.NMDP_SHIP_EXCUSE_FLAG,NULL,NULL);
	  END IF;
    IF NVL(:OLD.DATA_MODIFIED_FLAG,' ') != NVL(:NEW.DATA_MODIFIED_FLAG,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','DATA_MODIFIED_FLAG', :OLD.DATA_MODIFIED_FLAG, :NEW.DATA_MODIFIED_FLAG,NULL,NULL);
	  END IF;
  END IF;
END;
/

create or replace
TRIGGER CB_SHIPMENT_AU0  AFTER UPDATE OF
PK_SHIPMENT,
FK_ORDER_ID,
SHIPMENT_DATE,
ADDI_SHIPMENT_DATE,
SHIPMENT_TRACKING_NO,
ADDI_SHIPMENT_TRACKING_NO,
CBB_ID,
CREATOR,
CREATED_ON,
IP_ADD,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
RID,
DELETEDFLAG,
FK_CORD_ID,
PROP_SHIPMENT_DATE,
ATTN_NAME,
FK_DELIVERY_ADD,
ADDI_INFO,
SCH_SHIPMENT_DATE,
SHIPMENT_TIME,
FK_SHIPPING_CONT,
FK_TEMPERATURE_MONITER,
EXPECT_ARR_DATE,
SHIPMENT_CONFIRM,
SHIPMENT_CONFIRM_BY,
SHIPMENT_CONFIRM_ON,
FK_SHIP_COMPANY_ID,
PADLOCK_COMBINATION,
PAPERWORK_LOCATION,
ADDITI_SHIPPER_DET,
FK_SHIPMENT_TYPE,
NMDP_SHIP_EXCUSE_FLAG,
DATA_MODIFIED_FLAG  ON CB_SHIPMENT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
   RAID NUMBER(10);
   USR VARCHAR(200);
   OLD_MODIFIED_BY VARCHAR2(100);
   NEW_MODIFIED_BY VARCHAR2(100);

    NEW_FK_SHIPMENT_TYPE VARCHAR(200);
	OLD_FK_SHIPMENT_TYPE VARCHAR(200) ;
BEGIN
  SELECT SEQ_AUDIT.NEXTVAL INTO RAID FROM DUAL;

   USR := GETUSER(:NEW.LAST_MODIFIED_BY);


   IF NVL(:OLD.FK_SHIPMENT_TYPE,0) != NVL(:NEW.FK_SHIPMENT_TYPE,0) THEN
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_SHIPMENT_TYPE)		INTO NEW_FK_SHIPMENT_TYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_SHIPMENT_TYPE := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:OLD.FK_SHIPMENT_TYPE)		INTO OLD_FK_SHIPMENT_TYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_SHIPMENT_TYPE := '';
   END;
  END IF;

  AUDIT_TRAIL.RECORD_TRANSACTION
    (RAID, 'CB_SHIPMENT', :OLD.RID, 'U', USR);

  IF NVL(:OLD.PK_SHIPMENT,0) != NVL(:NEW.PK_SHIPMENT,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'PK_SHIPMENT',:OLD.PK_SHIPMENT, :NEW.PK_SHIPMENT);
  END IF;

    IF NVL(:OLD.FK_ORDER_ID,0) != NVL(:NEW.FK_ORDER_ID,0) THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'FK_ORDER_ID', :OLD.FK_ORDER_ID, :NEW.FK_ORDER_ID);
    END IF;

  IF NVL(:OLD.SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     AUDIT_TRAIL.COLUMN_UPDATE
       (RAID, 'SHIPMENT_DATE',
       TO_CHAR(:OLD.SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.ADDI_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.ADDI_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     AUDIT_TRAIL.COLUMN_UPDATE
       (RAID, 'ADDI_SHIPMENT_DATE',
       TO_CHAR(:OLD.ADDI_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.ADDI_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

   IF NVL(:OLD.SHIPMENT_TRACKING_NO,'') != NVL(:NEW.SHIPMENT_TRACKING_NO,'') THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'SHIPMENT_TRACKING_NO', :OLD.SHIPMENT_TRACKING_NO, :NEW.SHIPMENT_TRACKING_NO);
    END IF;

 IF NVL(:OLD.ADDI_SHIPMENT_TRACKING_NO,'') != NVL(:NEW.ADDI_SHIPMENT_TRACKING_NO,'') THEN
   AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'ADDI_SHIPMENT_TRACKING_NO', :OLD.ADDI_SHIPMENT_TRACKING_NO, :NEW.ADDI_SHIPMENT_TRACKING_NO);
    END IF;

  IF NVL(:OLD.CBB_ID,0) != NVL(:NEW.CBB_ID,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'CBB_ID', :OLD.CBB_ID, :NEW.CBB_ID);
  END IF;

  IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'CREATOR',:OLD.CREATOR, :NEW.CREATOR);
  END IF;

   IF NVL(:OLD.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     AUDIT_TRAIL.COLUMN_UPDATE
       (RAID, 'CREATED_ON',
       TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

  IF NVL(:OLD.IP_ADD,' ') !=   NVL(:NEW.IP_ADD,' ') THEN
     AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'IP_ADD', :OLD.IP_ADD, :NEW.IP_ADD);
  END IF;

    IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
       Begin
      Select  to_char(pk_user) || ',' ||  usr_lastname ||', ' || usr_firstname
      into old_modified_by from er_user  where pk_user = :old.last_modified_by ;
      Exception When NO_DATA_FOUND then
			old_modified_by := null;
    End ;
    Begin
      Select  to_char(pk_user) || ',' || usr_lastname ||', ' || usr_firstname
      into new_modified_by   from er_user   where pk_user = :new.LAST_MODIFIED_BY ;
      Exception When NO_DATA_FOUND then
			new_modified_by := null;
    End ;
      audit_trail.column_update (raid,'LAST_MODIFIED_BY' , old_modified_by, new_modified_by);
    END IF;

  IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     AUDIT_TRAIL.COLUMN_UPDATE
       (RAID, 'LAST_MODIFIED_DATE',
       TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'RID',:OLD.RID, :NEW.RID);
    END IF;

	IF NVL(:OLD.DELETEDFLAG,' ') != NVL(:NEW.DELETEDFLAG,' ') THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'DELETEDFLAG', :OLD.DELETEDFLAG, :NEW.DELETEDFLAG);
	END IF;

	IF NVL(:OLD.FK_CORD_ID,0) !=  NVL(:NEW.FK_CORD_ID,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'FK_CORD_ID', :OLD.FK_CORD_ID, :NEW.FK_CORD_ID);
  END IF;

    IF NVL(:OLD.PROP_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.PROP_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     AUDIT_TRAIL.COLUMN_UPDATE
       (RAID, 'PROP_SHIPMENT_DATE',
       TO_CHAR(:OLD.PROP_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.PROP_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;

   IF NVL(:OLD.ATTN_NAME,' ') != NVL(:NEW.ATTN_NAME,' ') THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'ATTN_NAME', :OLD.ATTN_NAME, :NEW.ATTN_NAME);
  END IF;

 IF NVL(:OLD.FK_DELIVERY_ADD,0) != NVL(:NEW.FK_DELIVERY_ADD,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'FK_DELIVERY_ADD',:OLD.FK_DELIVERY_ADD, :NEW.FK_DELIVERY_ADD);
  END IF;
  IF NVL(:OLD.ADDI_INFO,' ') != NVL(:NEW.ADDI_INFO,' ') THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'ADDI_INFO', :OLD.ADDI_INFO, :NEW.ADDI_INFO);
  END IF;
  IF NVL(:OLD.SCH_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.SCH_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     AUDIT_TRAIL.COLUMN_UPDATE
       (RAID, 'SCH_SHIPMENT_DATE',
       TO_CHAR(:OLD.SCH_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.SCH_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.FK_SHIPPING_CONT,0) != NVL(:NEW.FK_SHIPPING_CONT,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'FK_SHIPPING_CONT',:OLD.FK_SHIPPING_CONT, :NEW.FK_SHIPPING_CONT);
  END IF;
   IF NVL(:OLD.FK_TEMPERATURE_MONITER,0) != NVL(:NEW.FK_TEMPERATURE_MONITER,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'FK_TEMPERATURE_MONITER',:OLD.FK_TEMPERATURE_MONITER, :NEW.FK_TEMPERATURE_MONITER);
    END IF;
    IF NVL(:OLD.EXPECT_ARR_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.EXPECT_ARR_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     AUDIT_TRAIL.COLUMN_UPDATE
       (RAID, 'EXPECT_ARR_DATE',
       TO_CHAR(:OLD.EXPECT_ARR_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.EXPECT_ARR_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
  IF NVL(:OLD.SHIPMENT_CONFIRM,' ') !=   NVL(:NEW.SHIPMENT_CONFIRM,' ') THEN
     AUDIT_TRAIL.COLUMN_UPDATE(RAID, 'SHIPMENT_CONFIRM', :OLD.SHIPMENT_CONFIRM, :NEW.SHIPMENT_CONFIRM);
  END IF;

   IF NVL(:OLD.SHIPMENT_CONFIRM_BY,0) != NVL(:NEW.SHIPMENT_CONFIRM_BY,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'SHIPMENT_CONFIRM_BY', :OLD.SHIPMENT_CONFIRM_BY, :NEW.SHIPMENT_CONFIRM_BY);
	END IF;

  IF NVL(:OLD.SHIPMENT_CONFIRM_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.SHIPMENT_CONFIRM_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     AUDIT_TRAIL.COLUMN_UPDATE
       (RAID, 'SHIPMENT_CONFIRM_ON',
       TO_CHAR(:OLD.SHIPMENT_CONFIRM_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.SHIPMENT_CONFIRM_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
  END IF;
    IF NVL(:OLD.FK_SHIP_COMPANY_ID,0) != NVL(:NEW.FK_SHIP_COMPANY_ID,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'FK_SHIP_COMPANY_ID',:OLD.FK_SHIP_COMPANY_ID, :NEW.FK_SHIP_COMPANY_ID);
    END IF;
	IF NVL(:OLD.PADLOCK_COMBINATION,' ') != NVL(:NEW.PADLOCK_COMBINATION,' ') THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'PADLOCK_COMBINATION', :OLD.PADLOCK_COMBINATION, :NEW.PADLOCK_COMBINATION);
	END IF;
	IF NVL(:OLD.PAPERWORK_LOCATION,0) != NVL(:NEW.PAPERWORK_LOCATION,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'PAPERWORK_LOCATION',:OLD.PAPERWORK_LOCATION, :NEW.PAPERWORK_LOCATION);
    END IF;
	IF NVL(:OLD.ADDITI_SHIPPER_DET,' ') != NVL(:NEW.ADDITI_SHIPPER_DET,' ') THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'ADDITI_SHIPPER_DET', :OLD.ADDITI_SHIPPER_DET, :NEW.ADDITI_SHIPPER_DET);
	END IF;
	IF NVL(:OLD.FK_SHIPMENT_TYPE,0) != NVL(:NEW.FK_SHIPMENT_TYPE,0) THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'FK_SHIPMENT_TYPE',OLD_FK_SHIPMENT_TYPE,NEW_FK_SHIPMENT_TYPE);
    END IF;
	IF NVL(:OLD.NMDP_SHIP_EXCUSE_FLAG,' ') != NVL(:NEW.NMDP_SHIP_EXCUSE_FLAG,' ') THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'NMDP_SHIP_EXCUSE_FLAG', :OLD.NMDP_SHIP_EXCUSE_FLAG, :NEW.NMDP_SHIP_EXCUSE_FLAG);
	END IF;
	IF NVL(:OLD.DATA_MODIFIED_FLAG,' ') != NVL(:NEW.DATA_MODIFIED_FLAG,' ') THEN
     AUDIT_TRAIL.COLUMN_UPDATE (RAID, 'DATA_MODIFIED_FLAG', :OLD.DATA_MODIFIED_FLAG, :NEW.DATA_MODIFIED_FLAG);
	END IF;

END;
/


create or replace
TRIGGER CB_SHIPMENT_AU
AFTER UPDATE ON CB_SHIPMENT REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
pragma autonomous_transaction;
 outstr varchar2(100);
 workflowType VARCHAR2(100);
 entityType_CT NUMBER;
 entityType NUMBER;

BEGIN

 select pk_codelst into entityType_CT from er_codelst where codelst_type='order_type' and codelst_subtyp='CT';

 select ord.order_type into entityType from ER_ORDER ord where ord.PK_ORDER = :new.FK_ORDER_ID;

  workflowType := null;
  if(entityType_CT = entityType) then
      workflowType := 'CTORDER';
  end if;

  if(workflowType is not null) then
       SP_WORKFLOW(:new.FK_ORDER_ID,'CTORDER',1,outstr);
  end if;
COMMIT;  
END;
/

