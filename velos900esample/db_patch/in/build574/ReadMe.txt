
eSample enhancements:
1)Storage type 'kit' has been removed from storage kit browser.
2)Status column removal from storage kit browser 'Storagekitbrowser' as there is no need of status for storage kit templates.
3)Columns 'storageId' and 'name' has been aligned on a single line in storage kit browser.
4)Records on preparation area browser has been sorted on the basis of scheduled date.
5)While Creating Specimen from prepration area browser,specimen collected date is set to the patient scheduled date.


