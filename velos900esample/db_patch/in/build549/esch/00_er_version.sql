set define off;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,92,0,'00_er_version.sql',sysdate,'8.10.0 Build#549');

commit;


Update track_patches set APP_VERSION = '8.10.0 Build#548' 
where DB_VER_MJR = 91 and APP_VERSION = '8.9.0 Build#548';

commit;