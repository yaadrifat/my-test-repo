set define off;

DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where  CODELST_TYPE = 'cbu_ids' AND CODELST_SUBTYP = 'cbu_reg_id';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'cbu_ids','cbu_reg_id','CBU Registry ID','N',1,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/


DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where  CODELST_TYPE = 'cbu_ids' AND CODELST_SUBTYP = 'cbu_loc_id';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'cbu_ids','cbu_loc_id','CBU Local ID','N',2,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/


DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where  CODELST_TYPE = 'cbu_ids' AND CODELST_SUBTYP = 'add_id';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'cbu_ids','add_id','Addtional ID','N',3,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/

DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where  CODELST_TYPE = 'maternal_ids' AND CODELST_SUBTYP = 'mat_reg_id';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'maternal_ids','mat_reg_id','Maternal Registry ID','N',1,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/


DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where  CODELST_TYPE = 'maternal_ids' AND CODELST_SUBTYP = 'mat_loc_id';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'maternal_ids','mat_loc_id','Maternal Local ID','N',2,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/


DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where  CODELST_TYPE = 'maternal_ids' AND CODELST_SUBTYP = 'mat_add_id';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'maternal_ids','mat_add_id','Additional ID','N',3,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/



commit;

