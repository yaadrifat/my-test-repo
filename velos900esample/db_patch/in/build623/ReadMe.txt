/* This readMe is specific to Velos eResearch version 9.0 build #623 */

=====================================================================================================================================
Garuda :
DB Patch:-
	   i) DL18_ASSESS_RIGHT.sql  :- This SQL contains the CBU specific group access rights for Reports. These are specific to Garuda Project.
	   
=====================================================================================================================================
eResearch:


=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1. messageBundle.properties
2. labelBundle.properties
3. LC.java
4. MC.java
5. patientschedule.jsp
=====================================================================================================================================
