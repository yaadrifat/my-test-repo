--STARTS ADD THE COLUMN CORD_HISTORIC_CBU_ID TO CB_CORD--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD'
    AND COLUMN_NAME = 'CORD_HISTORIC_CBU_ID'; 

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_CORD ADD CORD_HISTORIC_CBU_ID VARCHAR2(50 BYTE)';
  end if;
end;
/
commit;
--END--
--starts comment for THE COLUMN CORD_HISTORIC_CBU_ID TO CB_ASSESSMENT TABLE--
comment on column CB_CORD.CORD_HISTORIC_CBU_ID  is 'Storing the historic Cbu Local id information from ESB messages.';
--end--
commit;



