create or replace
FUNCTION        "F_GET_MISACTIVE" (V_VALUE number)
RETURN VARCHAR2 AS V_RETVAL VARCHAR2(50);
v_milestone_stat varchar2(50);
BEGIN
  --Modified by Manimaran to replace milestone_isactive column and get the milestone status-- #D-FIN7
  select codelst_desc into v_milestone_stat from er_codelst where pk_codelst = v_value;
  return v_milestone_stat;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,73,7,'07_f_get_misactive.sql',sysdate,'8.9.0 Build#530');

commit;