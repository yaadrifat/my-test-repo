--DELETE SCRIPT FOR ER_CODELST

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'hla_meth' and codelst_subtyp='molecular' ;
  if (v_record_exists > 0) then
      DELETE FROM ER_CODELST WHERE codelst_type = 'hla_meth' and codelst_subtyp='molecular' ;
	commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,173,24,'24_ER_CODELST_DELETE.sql',sysdate,'9.0.0 Build#630');

commit;