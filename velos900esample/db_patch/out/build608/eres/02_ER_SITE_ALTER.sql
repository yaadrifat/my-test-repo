set define off;

--Sql for Table ER_SITE to add column GUID.'; 

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'ER_SITE' AND column_name = 'GUID'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table ER_SITE add (GUID VARCHAR2(50))');
  end if;
end;
/
COMMENT ON COLUMN ER_SITE.GUID IS 'System generated GUID.'; 
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,151,2,'02_ER_SITE_ALTER.sql',sysdate,'9.0.0 Build#608');

commit;
