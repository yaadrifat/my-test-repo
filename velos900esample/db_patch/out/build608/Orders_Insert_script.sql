--To Insert CT Order


DECLARE
	v_orderId number := 0;
BEGIN
       
Insert into ER_ORDER (PK_ORDER,ORDER_NUMBER,ORDER_ENTITYID,ORDER_ENTITYTYP,ORDER_DATE,FK_ORDER_STATUS,FK_ORDER_TYPE)
values (SEQ_ER_ORDER.nextval,'ORD223',<PK_CORD>,(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU'),sysdate,(select pk_codelst from er_codelst where codelst_type='order_status' and codelst_subtyp='NEW'),(select pk_codelst from er_codelst where codelst_type='order_type' and codelst_subtyp='CT'));
       
Select SEQ_ER_ORDER.currval into v_orderId from dual;
       
       
insert into ER_ORDER_TEMP(ORDER_ID,ORDER_TYPE) values(v_orderId,'CTORDER');
       
insert into cb_shipment(pk_shipment,fk_order_id) values(seq_cb_shipment.nextval,v_orderId);
commit;
       
end;
/

--To Insert OR Order

DECLARE
	v_orderId number := 0;
BEGIN
       
Insert into ER_ORDER (PK_ORDER,ORDER_NUMBER,ORDER_ENTITYID,ORDER_ENTITYTYP,ORDER_DATE,FK_ORDER_STATUS,FK_ORDER_TYPE)
values (SEQ_ER_ORDER.nextval,'ORD222',<PK_CORD>,(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU'),sysdate,(select pk_codelst from er_codelst where codelst_type='order_status' and codelst_subtyp='NEW'),(select pk_codelst from er_codelst where codelst_type='order_type' and codelst_subtyp='OR'));
       
Select SEQ_ER_ORDER.currval into v_orderId from dual;
       
       
insert into ER_ORDER_TEMP(ORDER_ID,ORDER_TYPE) values(v_orderId,'ORORDER');
       
insert into cb_shipment(pk_shipment,fk_order_id) values(seq_cb_shipment.nextval,v_orderId);
commit;
       
end;
/

--To Enter HE Order:

DECLARE
          v_orderId number := 0;
BEGIN
       
Insert into ER_ORDER (PK_ORDER,ORDER_NUMBER,ORDER_ENTITYID,ORDER_ENTITYTYP,ORDER_DATE,FK_ORDER_STATUS,FK_ORDER_TYPE)
values (SEQ_ER_ORDER.nextval,'ORD222',<PK_CORD>,(select pk_codelst from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU'),sysdate,(select pk_codelst from er_codelst where codelst_type='order_status' and codelst_subtyp='NEW'),(select pk_codelst from er_codelst where codelst_type='order_type' and codelst_subtyp='HE'));
       
Select SEQ_ER_ORDER.currval into v_orderId from dual;
       
       
insert into ER_ORDER_TEMP(ORDER_ID,ORDER_TYPE) values(v_orderId,'HEORDER');
       
commit;
       
end;
/


