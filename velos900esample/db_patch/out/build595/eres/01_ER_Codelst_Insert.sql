Set Define Off;

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hla_meth','serology','Serology','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

Commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,138,1,'01_ER_Codelst_Insert.sql',sysdate,'9.0.0 Build#595');

commit;

