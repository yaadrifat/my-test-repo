Set Define Off;

ALTER TABLE  CB_CORD ADD CORD_DONOR_IDENTI_NUM VARCHAR2(255);

COMMENT ON COLUMN "CB_CORD"."CORD_DONOR_IDENTI_NUM" IS 'Donor Identification number part of product code validation.';

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,138,3,'03_CB_Alter_table.sql',sysdate,'9.0.0 Build#595');

commit;

