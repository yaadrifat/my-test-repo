create or replace
function f_hla_cord(pk_cord number) return SYS_REFCURSOR
IS
p_hla_refcur SYS_REFCURSOR;
BEGIN
    OPEN p_hla_refcur FOR select hla_value_type1, hla_value_type2, F_CODELST_DESC(FK_SOURCE) as sourceval from cb_hla where ENTITY_ID = pk_cord order by FK_SOURCE;
RETURN P_HLA_REFCUR;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,164,32,'32_HotFix1.sql',sysdate,'9.0.0 Build#621');

commit;