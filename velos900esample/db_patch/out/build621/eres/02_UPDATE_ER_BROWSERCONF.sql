set define off;
--STARTS UPDATING RECORD INTO ER_BROWSERCONF TABLE--
DECLARE
  v_record_exists number := 0;
  v_record_exist number := 0;
BEGIN
  Select count(*) into v_record_exists from ER_BROWSERCONF
    where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='studyPatient')
    AND BROWSERCONF_COLNAME='MASK_PERSON_FNAME';
  if (v_record_exists = 1) then
	update ER_BROWSERCONF set BROWSERCONF_SETTINGS = '{"key":"MASK_PERSON_FNAME", "label":"First Name", "sortable":true, "resizeable":true,"hideable":true}' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='studyPatient') and BROWSERCONF_COLNAME='MASK_PERSON_FNAME';
	commit;
  end if;
  Select count(*) into v_record_exist from ER_BROWSERCONF
    where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='studyPatient')
    AND BROWSERCONF_COLNAME='MASK_PERSON_LNAME';
  if (v_record_exist = 1) then
	update ER_BROWSERCONF set BROWSERCONF_SETTINGS = '{"key":"MASK_PERSON_LNAME", "label":"Last Name", "sortable":true, "resizeable":true,"hideable":true}' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='studyPatient') and BROWSERCONF_COLNAME='MASK_PERSON_LNAME';
	commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,164,2,'02_UPDATE_ER_BROWSERCONF.sql',sysdate,'9.0.0 Build#621');

commit;