CREATE OR REPLACE TRIGGER "ESCH"."SCH_PROTOCOL_VISIT_BU1"
BEFORE UPDATE
ON SCH_PROTOCOL_VISIT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
ntpd Number;
BEGIN
ntpd :=0;
Select F_SCH_CODELST_ID('timepointtype','NTPD') into ntpd from dual;
IF (NVL(:NEW.NO_INTERVAL_FLAG,0) = 1) OR (NVL(:NEW.NO_INTERVAL_FLAG,0)=ntpd) THEN
   :NEW.NO_INTERVAL_FLAG :=1;
    :NEW.displacement := null;
ELSIF (:NEW.INSERT_AFTER <0) THEN
   :NEW.NO_INTERVAL_FLAG:=1;
   :NEW.displacement := null;
   :NEW.NUM_DAYS :=null;
   :NEW.INSERT_AFTER := 0;
   :NEW.INSERT_AFTER_INTERVAL :=0;
   :NEW.INSERT_AFTER_INTERVAL_UNIT :=null;
   :NEW.WIN_BEFORE_NUMBER :=null;
   :NEW.WIN_AFTER_NUMBER :=null;
ELSE
  :NEW.NO_INTERVAL_FLAG :=0;
    IF NVL(:NEW.displacement ,0) = 0 THEN
		:NEW.displacement := 1;
    END IF;
  END IF;
END;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,171,2,'02_SCH_PROTOCOL_VISIT_BU1.sql',sysdate,'9.0.0 Build#628');

commit;