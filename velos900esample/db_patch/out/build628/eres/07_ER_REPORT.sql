set define off;


UPDATE ER_CODELST  SET CODELST_DESC='Other' WHERE CODELST_TYPE='shipper' AND CODELST_SUBTYP='others'; 
commit;

delete er_repxsl where pk_repxsl in(161,167,168,170);
commit;

update er_report set rep_sql='SELECT
  '''' TESTDATEVAL,
  '''' LABTESTNAME,
  '''' FK_TEST_OUTCOME,
  1 CMS_APPROVED_LAB,
  1 FDA_LICENSED_LAB,
  '''' ASSESSMENT_REMARKS,
  '''' FLAG_FOR_LATER,
  1  ASSESSMENT_FOR_RESPONSE,
  ''''  TC_VISIBILITY_FLAG,
  ''''  CONSULTE_FLAG,
  ''''  ASSESSMENT_POSTEDBY,
  ''''  ASSESSMENT_POSTEDON,
  ''''  ASSESSMENT_CONSULTBY,
  '''' ASSESSMENT_CONSULTON,
  CRD.CORD_ISBI_DIN_CODE ISTBTDIN,
  CRD.CORD_REGISTRY_ID REGID,
  CRD.CORD_LOCAL_CBU_ID LOCALCBUID,
  F_CORD_ADD_IDS(~1) ADDIDS,
  F_CORD_ID_ON_BAG(~1) IDSONBAG,
  SITE.SITE_NAME SITENAME,
  SITE.SITE_ID SITEID,
  '''' SOURCEVAL,
  1 LAB_TEST,
  '''' lastval
FROM
  CB_CORD CRD,
  er_site site
WHERE
  CRD.FK_CBB_ID=SITE.PK_SITE AND CRD.PK_CORD=~1
union all
SELECT
        PL.TESTDATE,
        ltest.labtest_name,
        f_codelst_desc(pl.FK_TEST_OUTCOME),
        pl.CMS_APPROVED_LAB,
        pl.FDA_LICENSED_LAB,
        pl.ASSESSMENT_REMARKS,
        PL.FLAG_FOR_LATER,
        pl.ASSESSMENT_FOR_RESPONSE,
        PL.TC_VISIBILITY_FLAG,
        PL.CONSULTE_FLAG,
        F_GETUSER(PL.ASSESSMENT_POSTEDBY),
        to_char(PL.ASSESSMENT_POSTEDON,''Mon DD, YYYY''),
        F_GETUSER(PL.ASSESSMENT_CONSULTBY),
        to_char(PL.ASSESSMENT_CONSULTON,''Mon DD, YYYY''),
        '''',
        '''',
        '''',
        F_CORD_ADD_IDS(~1) ADDIDS,
        F_CORD_ID_ON_BAG(~1) IDSONBAG,
        '''',
        '''',
        PL.SOURCEVAL,
        ltest.pk_labtest lab_test,
        DECODE(PL.FK_TEST_OUTCOME,
                ''1'',''Yes'',
                ''0'',''No'',
                null,'''')
    from
        er_labtest ltest
    left outer join
        (
            select
                plabs.fk_testid fk_testid,
                plabs.fk_test_outcome fk_test_outcome,
                plabs.ft_test_source ft_test_source,
                plabs.cms_approved_lab cms_approved_lab,
                plabs.fda_licensed_lab fda_licensed_lab,
                f_codelst_desc(PLABS.FT_TEST_SOURCE) sourceval,
                to_char(plabs.test_date,
                ''Mon DD, YYYY'') testDate,
                plabs.custom003 CUSTOM003,
                ass.ASSESSMENT_REMARKS ASSESSMENT_REMARKS,
                ass.FLAG_FOR_LATER FLAG_FOR_LATER,
                ass.ASSESSMENT_FOR_RESPONSE ASSESSMENT_FOR_RESPONSE,
                ass.TC_VISIBILITY_FLAG TC_VISIBILITY_FLAG,
                ass.SENT_TO_REVIEW_FLAG SENT_TO_REVIEW_FLAG,
                ass.CONSULTE_FLAG CONSULTE_FLAG,
                ass.ASSESSMENT_POSTEDBY ASSESSMENT_POSTEDBY,
                ass.ASSESSMENT_POSTEDON ASSESSMENT_POSTEDON,
                ass.ASSESSMENT_CONSULTBY ASSESSMENT_CONSULTBY,
                ass.ASSESSMENT_CONSULTON ASSESSMENT_CONSULTON
            from
                er_patlabs plabs
            left outer join
                cb_assessment ass
                    on(
                        ass.sub_entity_id=plabs.pk_patlabs
                    )
            where
                plabs.pk_patlabs in(
                    select
                        pk_patlabs
                    from
                        er_patlabs
                    WHERE
                        fk_specimen=(select CORD.FK_SPECIMEN_ID from cb_cord cord where cord.pk_cord=~1)
                )
            ) pl
                on(
                    ltest.pk_labtest=pl.fk_testid
                )
        left outer join
            er_labtestgrp ltestgrp
                on(
                    ltestgrp.fk_testid=ltest.pk_labtest
                )
        where
            ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    lg.group_type=''I''
            )
            or ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    LG.GROUP_TYPE=''IOG''
            )AND PL.FK_TESTID=LTEST.PK_LABTEST ORDER BY LAB_TEST' where pk_report=172;
	    
commit;
update er_report set rep_sql_clob='SELECT
  '''' TESTDATEVAL,
  '''' LABTESTNAME,
  '''' FK_TEST_OUTCOME,
  1 CMS_APPROVED_LAB,
  1 FDA_LICENSED_LAB,
  '''' ASSESSMENT_REMARKS,
  '''' FLAG_FOR_LATER,
  1  ASSESSMENT_FOR_RESPONSE,
  ''''  TC_VISIBILITY_FLAG,
  ''''  CONSULTE_FLAG,
  ''''  ASSESSMENT_POSTEDBY,
  ''''  ASSESSMENT_POSTEDON,
  ''''  ASSESSMENT_CONSULTBY,
  '''' ASSESSMENT_CONSULTON,
  CRD.CORD_ISBI_DIN_CODE ISTBTDIN,
  CRD.CORD_REGISTRY_ID REGID,
  CRD.CORD_LOCAL_CBU_ID LOCALCBUID,
  F_CORD_ADD_IDS(~1) ADDIDS,
  F_CORD_ID_ON_BAG(~1) IDSONBAG,
  SITE.SITE_NAME SITENAME,
  SITE.SITE_ID SITEID,
  '''' SOURCEVAL,
  1 LAB_TEST,
  '''' lastval
FROM
  CB_CORD CRD,
  er_site site
WHERE
  CRD.FK_CBB_ID=SITE.PK_SITE AND CRD.PK_CORD=~1
union all
SELECT
        PL.TESTDATE,
        ltest.labtest_name,
        f_codelst_desc(pl.FK_TEST_OUTCOME),
        pl.CMS_APPROVED_LAB,
        pl.FDA_LICENSED_LAB,
        pl.ASSESSMENT_REMARKS,
        PL.FLAG_FOR_LATER,
        pl.ASSESSMENT_FOR_RESPONSE,
        PL.TC_VISIBILITY_FLAG,
        PL.CONSULTE_FLAG,
        F_GETUSER(PL.ASSESSMENT_POSTEDBY),
        to_char(PL.ASSESSMENT_POSTEDON,''Mon DD, YYYY''),
        F_GETUSER(PL.ASSESSMENT_CONSULTBY),
        to_char(PL.ASSESSMENT_CONSULTON,''Mon DD, YYYY''),
        '''',
        '''',
        '''',
        F_CORD_ADD_IDS(~1) ADDIDS,
        F_CORD_ID_ON_BAG(~1) IDSONBAG,
        '''',
        '''',
        PL.SOURCEVAL,
        ltest.pk_labtest lab_test,
        DECODE(PL.FK_TEST_OUTCOME,
                ''1'',''Yes'',
                ''0'',''No'',
                null,'''')
    from
        er_labtest ltest
    left outer join
        (
            select
                plabs.fk_testid fk_testid,
                plabs.fk_test_outcome fk_test_outcome,
                plabs.ft_test_source ft_test_source,
                plabs.cms_approved_lab cms_approved_lab,
                plabs.fda_licensed_lab fda_licensed_lab,
                f_codelst_desc(PLABS.FT_TEST_SOURCE) sourceval,
                to_char(plabs.test_date,
                ''Mon DD, YYYY'') testDate,
                plabs.custom003 CUSTOM003,
                ass.ASSESSMENT_REMARKS ASSESSMENT_REMARKS,
                ass.FLAG_FOR_LATER FLAG_FOR_LATER,
                ass.ASSESSMENT_FOR_RESPONSE ASSESSMENT_FOR_RESPONSE,
                ass.TC_VISIBILITY_FLAG TC_VISIBILITY_FLAG,
                ass.SENT_TO_REVIEW_FLAG SENT_TO_REVIEW_FLAG,
                ass.CONSULTE_FLAG CONSULTE_FLAG,
                ass.ASSESSMENT_POSTEDBY ASSESSMENT_POSTEDBY,
                ass.ASSESSMENT_POSTEDON ASSESSMENT_POSTEDON,
                ass.ASSESSMENT_CONSULTBY ASSESSMENT_CONSULTBY,
                ass.ASSESSMENT_CONSULTON ASSESSMENT_CONSULTON
            from
                er_patlabs plabs
            left outer join
                cb_assessment ass
                    on(
                        ass.sub_entity_id=plabs.pk_patlabs
                    )
            where
                plabs.pk_patlabs in(
                    select
                        pk_patlabs
                    from
                        er_patlabs
                    WHERE
                        fk_specimen=(select CORD.FK_SPECIMEN_ID from cb_cord cord where cord.pk_cord=~1)
                )
            ) pl
                on(
                    ltest.pk_labtest=pl.fk_testid
                )
        left outer join
            er_labtestgrp ltestgrp
                on(
                    ltestgrp.fk_testid=ltest.pk_labtest
                )
        where
            ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    lg.group_type=''I''
            )
            or ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    LG.GROUP_TYPE=''IOG''
            )AND PL.FK_TESTID=LTEST.PK_LABTEST ORDER BY LAB_TEST' where pk_report=172;
commit;

create or replace
function f_getIdmDetails(cordId number) return SYS_REFCURSOR
IS
c_IdmDetails SYS_REFCURSOR;
BEGIN
open C_IDMDETAILS for
SELECT
        PL.TESTDATE TESTDATEVAL,
        LTEST.LABTEST_NAME LABTESTNAME,
        F_CODELST_DESC(PL.FK_TEST_OUTCOME) FK_TEST_OUTCOME,
        PL.CMS_APPROVED_LAB CMS_APPROVED_LAB,
        PL.FDA_LICENSED_LAB FDA_LICENSED_LAB,
        PL.ASSESSMENT_REMARKS ASSESSMENT_REMARKS,
        PL.FLAG_FOR_LATER FLAG_FOR_LATER,
        PL.ASSESSMENT_FOR_RESPONSE ASSESSMENT_FOR_RESPONSE,
        PL.TC_VISIBILITY_FLAG TC_VISIBILITY_FLAG,
        PL.CONSULTE_FLAG CONSULTE_FLAG,
        F_GETUSER(PL.ASSESSMENT_POSTEDBY) ASSESSMENT_POSTEDBY,
        TO_CHAR(PL.ASSESSMENT_POSTEDON,'Mon DD, YYYY') ASSESSMENT_POSTEDON,
        F_GETUSER(PL.ASSESSMENT_CONSULTBY) ASSESSMENT_CONSULTBY,
        TO_CHAR(PL.ASSESSMENT_CONSULTON,'Mon DD, YYYY') ASSESSMENT_CONSULTON,
        PL.SOURCEVAL SOURCEVALS,
        LTEST.PK_LABTEST LAB_TEST,
        DECODE(PL.FK_TEST_OUTCOME,
                '1','Yes',
                '0','No',
                null,'') lastval
    from
        er_labtest ltest
    left outer join
        (
            select
                plabs.fk_testid fk_testid,
                plabs.fk_test_outcome fk_test_outcome,
                plabs.ft_test_source ft_test_source,
                plabs.cms_approved_lab cms_approved_lab,
                plabs.fda_licensed_lab fda_licensed_lab,
                f_codelst_desc(PLABS.FT_TEST_SOURCE) sourceval,
                to_char(plabs.test_date,
                'Mon DD, YYYY') testDate,
                plabs.custom003 CUSTOM003,
                ass.ASSESSMENT_REMARKS ASSESSMENT_REMARKS,
                ass.FLAG_FOR_LATER FLAG_FOR_LATER,
                ass.ASSESSMENT_FOR_RESPONSE ASSESSMENT_FOR_RESPONSE,
                ass.TC_VISIBILITY_FLAG TC_VISIBILITY_FLAG,
                ass.SENT_TO_REVIEW_FLAG SENT_TO_REVIEW_FLAG,
                ass.CONSULTE_FLAG CONSULTE_FLAG,
                ass.ASSESSMENT_POSTEDBY ASSESSMENT_POSTEDBY,
                ass.ASSESSMENT_POSTEDON ASSESSMENT_POSTEDON,
                ASS.ASSESSMENT_CONSULTBY ASSESSMENT_CONSULTBY,
                ass.ASSESSMENT_CONSULTON ASSESSMENT_CONSULTON
            from
                er_patlabs plabs
            left outer join
                cb_assessment ass
                    on(
                        ass.sub_entity_id=plabs.pk_patlabs
                    )
            where
                plabs.pk_patlabs in(
                    select
                        pk_patlabs
                    from
                        er_patlabs
                    WHERE
                        fk_specimen=(select CORD.FK_SPECIMEN_ID from cb_cord cord where cord.pk_cord=CORDID)
                )
            ) pl
                on(
                    ltest.pk_labtest=pl.fk_testid
                )
        left outer join
            er_labtestgrp ltestgrp
                on(
                    ltestgrp.fk_testid=ltest.pk_labtest
                )
        where
            ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    lg.group_type='I'
            )
            or ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    LG.GROUP_TYPE='IOG'
            )AND PL.FK_TESTID=LTEST.PK_LABTEST
            ORDER BY LAB_TEST;
RETURN C_IDMDETAILS;
END;
/
commit;

CREATE OR REPLACE FORCE VIEW "ERES"."REP_CBU_DETAILS" ("PK_CORD", "CORD_REGISTRY_ID", "REGISTRY_MATERNAL_ID", "CORD_LOCAL_CBU_ID", "MATERNAL_LOCAL_ID", "CORD_ISBI_DIN_CODE", "CBBID", "CBB_ID", "STORAGE_LOC", "CBU_COLLECTION_SITE", "ELIGIBLE_STATUS", "LIC_STATUS", "PRCSNG_START_DATE", "BACT_CULTURE", "BACT_COMMENT", "FRZ_DATE", "FUNGAL_CULTURE", "BACT_CUL_STRT_DT", "FUNG_CUL_STRT_DT", "FUNG_COMMENT", "ABO_BLOOD_TYPE", "HEMOGLOBIN_SCRN", "RH_TYPE", "PROC_NAME", "PROC_START_DATE", "PROC_TERMI_DATE", "PROCESSING", "AUTOMATED_TYPE", "OTHER_PROCESSING", "PRODUCT_MODIFICATION", "OTHER_PRODUCT_MODI", "STORAGE_METHOD", "FREEZER_MANUFACT", "OTHER_FREEZER_MANUFACT", "FROZEN_IN", "OTHER_FROZEN_CONT", "NO_OF_BAGS", "CRYOBAG_MANUFAC", "BAGTYPE", "BAG1TYPE", "BAG2TYPE", "OTHER_BAG", "HEPARIN_THOU_PER", "HEPARIN_THOU_ML", "HEPARIN_FIVE_PER", "HEPARIN_FIVE_ML", "HEPARIN_TEN_PER", "HEPARIN_TEN_ML", "HEPARIN_SIX_PER", "HEPARIN_SIX_ML", "CPDA_PER", "CPDA_ML", "CPD_PER", "CPD_ML", "ACD_PER", "ACD_ML",
  "OTHR_ANTI_PER", "OTHR_ANTI_ML", "SPECI_OTHR_ANTI", "HUN_DMSO_PER", "HUN_DMSO_ML", "HUN_GLYCEROL_PER", "HUN_GLYCEROL_ML", "TEN_DEXTRAN_40_PER", "TEN_DEXTRAN_40_ML", "FIVE_HUMAN_ALBU_PER", "FIVE_HUMAN_ALBU_ML", "TWENTYFIVE_HUMAN_ALBU_PER", "TWENTYFIVE_HUMAN_ALBU_ML", "PLASMALYTE_PER", "PLASMALYTE_ML", "OTHR_CRYOPROTECTANT_PER", "OTHR_CRYOPROTECTANT_ML", "SPEC_OTHR_CRYOPROTECTANT", "FIVE_DEXTROSE_PER", "FIVE_DEXTROSE_ML", "POINT_NINE_NACL_PER", "POINT_NINE_NACL_ML", "OTHR_DILUENTS_PER", "OTHR_DILUENTS_ML", "SPEC_OTHR_DILUENTS", "PRODUCTCODE", "STORAGE_TEMPERATURE", "MAX_VOL", "CTRL_RATE_FREEZING", "INDIVIDUAL_FRAC", "VIABLE_SAMP_FINAL_PROD", "FILTER_PAPER", "RPC_PELLETS", "EXTR_DNA", "SERUM_ALIQUOTES", "PLASMA_ALIQUOTES", "NONVIABLE_ALIQUOTES", "NO_OF_SEGMENTS", "NO_OF_OTH_REP_ALLIQ_ALTER_COND", "NO_OF_OTH_REP_ALLIQUOTS_F_PROD", "NO_OF_SERUM_MATER_ALIQUOTS", "NO_OF_PLASMA_MATER_ALIQUOTS", "NO_OF_EXTR_DNA_MATER_ALIQUOTS", "NO_OF_CELL_MATER_ALIQUOTS", "INELIGIBLEREASON",
  "UNLICENSEREASON", "ELIGCOMMENTS", "ELGIBLE_MODI_DT", "LIC_MODI_DT", "ELI_FLAG", "LIC_FLAG")
AS
  SELECT pk_cord,
    cord_registry_id,
    registry_maternal_id,
    cord_local_cbu_id,
    maternal_local_id,
    CORD_ISBI_DIN_CODE,
    b.site_name cbbid,
    b.site_id cbb_id,
    f_codelst_desc(fk_cbu_stor_loc) storage_loc,
    c.site_name cbu_collection_site,
    f_codelst_desc(fk_cord_cbu_eligible_status) eligible_status,
    f_codelst_desc(fk_cord_cbu_lic_status) lic_status,
    TO_CHAR(prcsng_start_date,'Mon DD,YYYY') prcsng_start_date,
    f_codelst_desc(fk_cord_bact_cul_result) bact_culture,
    bact_comment,
    TO_CHAR(frz_date,'Mon DD,YYYY')frz_date,
    f_codelst_desc(fk_cord_fungal_cul_result) fungal_culture,
    TO_CHAR(a.bact_cult_date,'Mon DD,YYYY') bact_cul_strt_dt,
    TO_CHAR(a.fung_cult_date,'Mon DD,YYYY') fung_cul_strt_dt,
    fung_comment,
    f_codelst_desc(fk_cord_abo_blood_type) abo_blood_type,
    f_codelst_desc(hemoglobin_scrn) hemoglobin_scrn,
    f_codelst_desc(fk_cord_rh_type) rh_type,
    e.proc_name proc_name,
    TO_CHAR(e.proc_start_date,'Mon DD, YYYY') proc_start_date,
    TO_CHAR(e.proc_termi_date,'Mon DD, YYYY') proc_termi_date,
    f_codelst_desc(d.fk_proc_meth_id) processing,
    f_codelst_desc(d.fk_if_automated) automated_type,
    d.other_processing other_processing,
    f_codelst_desc(d.fk_product_modification) product_modification,
    d.other_prod_modi other_product_modi,
    f_codelst_desc(d.fk_stor_method) storage_method,
    f_codelst_desc(d.fk_freez_manufac) freezer_manufact,
    d.other_freez_manufac other_freezer_manufact,
    f_codelst_desc(d.fk_frozen_in) frozen_in,
    d.other_frozen_cont other_frozen_cont,
    f_codelst_desc(d.fk_num_of_bags) no_of_bags,
    d.cryobag_manufac cryobag_manufac,
    f_codelst_desc(d.fk_bagtype) bagtype,
    f_codelst_desc(d.fk_bag_1_type) bag1type,
    f_codelst_desc(d.fk_bag_2_type) bag2type,
    d.other_bag_type other_bag,
    d.THOU_UNIT_PER_ML_HEPARIN_PER heparin_thou_per,
    d.THOU_UNIT_PER_ML_HEPARIN heparin_thou_ml,
    d.FIVE_UNIT_PER_ML_HEPARIN_PER heparin_five_per,
    d.FIVE_UNIT_PER_ML_HEPARIN heparin_five_ml,
    d.TEN_UNIT_PER_ML_HEPARIN_PER heparin_ten_per,
    d.TEN_UNIT_PER_ML_HEPARIN heparin_ten_ml,
    d.SIX_PER_HYDRO_STARCH_PER heparin_six_per,
    d.SIX_PER_HYDROXYETHYL_STARCH heparin_six_ml,
    d.CPDA_PER cpda_per,
    d.CPDA cpda_ml,
    d.CPD_PER cpd_per,
    d.CPD cpd_ml,
    d.ACD_PER acd_per,
    d.ACD acd_ml,
    d.OTHER_ANTICOAGULANT_PER othr_anti_per,
    d.OTHER_ANTICOAGULANT othr_anti_ml,
    d.SPECIFY_OTH_ANTI speci_othr_anti,
    d.HUN_PER_DMSO_PER hun_dmso_per,
    d.HUN_PER_DMSO hun_dmso_ml,
    d.HUN_PER_GLYCEROL_PER hun_glycerol_per,
    d.HUN_PER_GLYCEROL hun_glycerol_ml,
    d.TEN_PER_DEXTRAN_40_PER ten_dextran_40_per,
    d.TEN_PER_DEXTRAN_40 ten_dextran_40_ml,
    d.FIVE_PER_HUMAN_ALBU_PER five_human_albu_per,
    d.FIVE_PER_HUMAN_ALBU five_human_albu_ml,
    d.TWEN_FIVE_HUM_ALBU_PER twentyfive_human_albu_per,
    d.TWEN_FIVE_HUM_ALBU twentyfive_human_albu_ml,
    d.PLASMALYTE_PER plasmalyte_per,
    d.PLASMALYTE plasmalyte_ml,
    d.OTH_CRYOPROTECTANT_PER othr_cryoprotectant_per,
    d.OTH_CRYOPROTECTANT othr_cryoprotectant_ml,
    d.SPEC_OTH_CRYOPRO spec_othr_cryoprotectant,
    d.FIVE_PER_DEXTROSE_PER five_dextrose_per,
    d.FIVE_PER_DEXTROSE five_dextrose_ml,
    d.POINNT_NINE_PER_NACL_PER point_nine_nacl_per,
    d.POINNT_NINE_PER_NACL point_nine_nacl_ml,
    d.OTH_DILUENTS_PER othr_diluents_per,
    d.OTH_DILUENTS othr_diluents_ml,
    d.SPEC_OTH_DILUENTS spec_othr_diluents,
    a.PRODUCT_CODE productcode,
    f_codelst_desc(d.fk_stor_temp) storage_temperature,
    d.max_value max_vol,
    f_codelst_desc(d.fk_contrl_rate_freezing) ctrl_rate_freezing,
    D.NO_OF_INDI_FRAC INDIVIDUAL_FRAC,
    D.NO_OF_VIABLE_SMPL_FINAL_PROD VIABLE_SAMP_FINAL_PROD,
    f.filt_pap_avail filter_paper,
    f.RBC_PEL_AVAIL rpc_pellets,
    f.NO_EXT_DNA_ALI extr_dna,
    f.NO_SER_ALI serum_aliquotes,
    F.NO_PLAS_ALI PLASMA_ALIQUOTES,
    F.NO_NON_VIA_ALI NONVIABLE_ALIQUOTES,
    F.NO_OF_SEG_AVAIL NO_OF_SEGMENTS,
    f.CBU_NO_REP_ALT_CON no_of_oth_rep_alliq_alter_cond,
    F.CBU_OT_REP_CON_FIN NO_OF_OTH_REP_ALLIQUOTS_F_PROD,
    F.NO_SER_MAT_ALI NO_OF_SERUM_MATER_ALIQUOTS,
    F.NO_PLAS_MAT_ALI NO_OF_PLASMA_MATER_ALIQUOTS,
    F.NO_EXT_DNA_MAT_ALI NO_OF_EXTR_DNA_MATER_ALIQUOTS,
    f.SI_NO_MISC_MAT NO_OF_CELL_MATER_ALIQUOTS,
    ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_INELIGIBLE_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD in ('
    ||a.pk_cord
    ||')',',') INELIGIBLEREASON,
    ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_UNLICENSED_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD in ('
    ||a.pk_cord
    ||')',',') UNLICENSEREASON,
    a.CORD_ELIGIBLE_ADDITIONAL_INFO ELIGCOMMENTS,
    f_lic_eligi_moddt(a.pk_cord, 'eligibility') ELGIBLE_MODI_DT,
    f_lic_eligi_moddt(a.pk_cord, 'licence') LIC_MODI_DT,
    DECODE(a.FK_CORD_CBU_ELIGIBLE_STATUS, (f_codelst_id('eligibility','eligible')),'0', (f_codelst_id('eligibility','ineligible')),'1', (f_codelst_id('eligibility','incomplete')),'2', (f_codelst_id('eligibility','not_appli_prior')),'3') ELI_FLAG,
    DECODE(a.FK_CORD_CBU_LIC_STATUS, (f_codelst_id('licence','licensed')),'0', (f_codelst_id('licence','unlicensed')),'1') LIC_FLAG
  FROM cb_cord a
  LEFT OUTER JOIN er_site b
  ON (a.fk_cbb_id = b.pk_site)
  LEFT OUTER JOIN er_site c
  ON (a.fk_cbu_coll_site = c.pk_site)
  LEFT OUTER JOIN cbb_processing_procedures_info d
  ON (a.fk_cbb_procedure = d.fk_processing_id)
  LEFT OUTER JOIN CBB_PROCESSING_PROCEDURES E
  ON (A.FK_CBB_PROCEDURE = E.PK_PROC )
  LEFT OUTER JOIN CB_ENTITY_SAMPLES F
  ON (f.entity_id=a.pk_cord);

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,171,7,'07_ER_REPORT.sql',sysdate,'9.0.0 Build#628');

commit;