set define off;

create or replace function f_tnc_count_ncbi(selYear varchar2) return SYS_REFCURSOR
IS
p_emp_refcur SYS_REFCURSOR;
BEGIN
    OPEN p_emp_refcur FOR select 'A' AS A, '90-124' as B,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 90 and 124 )
and to_char(b.shipment_date,'MON')='OCT' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id ) as Oct,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 90 and 124 )
and to_char(b.shipment_date,'MON')='NOV' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id ) as Nov,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 90 and 124 )
and to_char(b.shipment_date,'MON')='DEC' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id ) as Dec,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 90 and 124 )
and to_char(b.shipment_date,'MON')='JAN' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id  ) as Jan,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 90 and 124 )
and to_char(b.shipment_date,'MON')='FEB' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id  ) as Feb,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 90 and 124 )
and to_char(b.shipment_date,'MON')='MAR' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id  ) as Mar,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 90 and 124 )
and to_char(b.shipment_date,'MON')='APR' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id  ) as Apr,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 90 and 124 )
and to_char(b.shipment_date,'MON')='MAY' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id  ) as May,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 90 and 124 )
and to_char(b.shipment_date,'MON')='JUN' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id  ) as Jun,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 90 and 124 )
and to_char(b.shipment_date,'MON')='JUL' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id  ) as Jul,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 90 and 124 )
and to_char(b.shipment_date,'MON')='AUG' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id  ) as Aug,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 90 and 124 )
and to_char(b.shipment_date,'MON')='SEP' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id ) as Sep,
(select count(*) from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 90 and 124 ) and b.shipment_date >= to_date('10/01/'||(selYear-2),'MM/dd/yyyy') 
and  b.shipment_date <= to_date('09/30/'||(selYear-1),'MM/dd/yyyy') and a.pk_cord = b.fk_cord_id) as PrevYear
from dual
union
select 'B', '125-149',
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 125 and 149 )
and to_char(b.shipment_date,'MON')='OCT' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id ) as Oct,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 125 and 149 )
and to_char(b.shipment_date,'MON')='NOV' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id) as Nov,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 125 and 149 )
and to_char(b.shipment_date,'MON')='DEC' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id) as Dec,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 125 and 149 )
and to_char(b.shipment_date,'MON')='JAN' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id) as Jan,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 125 and 149 )
and to_char(b.shipment_date,'MON')='FEB' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id ) as Feb,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 125 and 149 )
and to_char(b.shipment_date,'MON')='MAR' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id) as Mar,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 125 and 149 )
and to_char(b.shipment_date,'MON')='APR' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id) as Apr,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 125 and 149 )
and to_char(b.shipment_date,'MON')='MAY' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id) as May,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 125 and 149 )
and to_char(b.shipment_date,'MON')='JUN' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id) as Jun,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 125 and 149 )
and to_char(b.shipment_date,'MON')='JUL' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id) as Jul,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 125 and 149 )
and to_char(b.shipment_date,'MON')='AUG' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id) as Aug,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 125 and 149 )
and to_char(b.shipment_date,'MON')='SEP' and to_char(b.shipment_date,'YYYY') = (selYear) and a.pk_cord = b.fk_cord_id) as Sep,
(select count(*) from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 125 and 149 ) and b.shipment_date >= to_date('10/01/'||(selYear-2),'MM/dd/yyyy') 
and  b.shipment_date <= to_date('09/30/'||(selYear-1),'MM/dd/yyyy') and a.pk_cord = b.fk_cord_id) as PrevYear
from dual
union
select 'C', '150-199',
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 150 and 199 )
and to_char(b.shipment_date,'MON')='OCT' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id ) as Oct,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 150 and 199 )
and to_char(b.shipment_date,'MON')='NOV' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id ) as Nov,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 150 and 199 )
and to_char(b.shipment_date,'MON')='DEC' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id) as Dec,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 150 and 199 )
and to_char(b.shipment_date,'MON')='JAN' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Jan,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 150 and 199 )
and to_char(b.shipment_date,'MON')='FEB' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Feb,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 150 and 199 )
and to_char(b.shipment_date,'MON')='MAR' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Mar,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 150 and 199 )
and to_char(b.shipment_date,'MON')='APR' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Apr,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 150 and 199 )
and to_char(b.shipment_date,'MON')='MAY' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as May,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 150 and 199 )
and to_char(b.shipment_date,'MON')='JUN' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Jun,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 150 and 199 )
and to_char(b.shipment_date,'MON')='JUL' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Jul,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 150 and 199 )
and to_char(b.shipment_date,'MON')='AUG' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Aug,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 150 and 199 )
and to_char(b.shipment_date,'MON')='SEP' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Sep,
(select count(*) from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 150 and 199 ) and b.shipment_date >= to_date('10/01/'||(selYear-2),'MM/dd/yyyy') 
and  b.shipment_date <= to_date('09/30/'||(selYear-1),'MM/dd/yyyy') and a.pk_cord = b.fk_cord_id) as PrevYear
from dual
union
select 'D', '200-249',
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 200 and 249 )
and to_char(b.shipment_date,'MON')='OCT' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id) as Oct,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 200 and 249 )
and to_char(b.shipment_date,'MON')='NOV' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id) as Nov,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 200 and 249 )
and to_char(b.shipment_date,'MON')='DEC' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id) as Dec,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 200 and 249 )
and to_char(b.shipment_date,'MON')='JAN' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Jan,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 200 and 249 )
and to_char(b.shipment_date,'MON')='FEB' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Feb,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 200 and 249 )
and to_char(b.shipment_date,'MON')='MAR' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Mar,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 200 and 249 )
and to_char(b.shipment_date,'MON')='APR' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Apr,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 200 and 249 )
and to_char(b.shipment_date,'MON')='MAY' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as May,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 200 and 249 )
and to_char(b.shipment_date,'MON')='JUN' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Jun,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 200 and 249 )
and to_char(b.shipment_date,'MON')='JUL' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Jul,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 200 and 249 )
and to_char(b.shipment_date,'MON')='AUG' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Aug,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 200 and 249 )
and to_char(b.shipment_date,'MON')='SEP' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Sep,
(select count(*) from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 200 and 249 ) and b.shipment_date >= to_date('10/01/'||(selYear-2),'MM/dd/yyyy') and  
b.shipment_date <= to_date('09/30'||(selYear-1),'MM/dd/yyyy') and a.pk_cord = b.fk_cord_id) as PrevYear
from dual
union
select 'E', '250-300',
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 250 and 300 )
and to_char(b.shipment_date,'MON')='OCT' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id) as Oct,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 250 and 300 )
and to_char(b.shipment_date,'MON')='NOV' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id) as Nov,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 250 and 300 )
and to_char(b.shipment_date,'MON')='DEC' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id) as Dec,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 250 and 300 )
and to_char(b.shipment_date,'MON')='JAN' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Jan,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 250 and 300 )
and to_char(b.shipment_date,'MON')='FEB' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Feb,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 250 and 300 )
and to_char(b.shipment_date,'MON')='MAR' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Mar,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 250 and 300 )
and to_char(b.shipment_date,'MON')='APR' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Apr,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 250 and 300 )
and to_char(b.shipment_date,'MON')='MAY' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as May,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 250 and 300 )
and to_char(b.shipment_date,'MON')='JUN' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Jun,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 250 and 300 )
and to_char(b.shipment_date,'MON')='JUL' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Jul,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 250 and 300 )
and to_char(b.shipment_date,'MON')='AUG' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Aug,
(select count(*)  from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 250 and 300 )
and to_char(b.shipment_date,'MON')='SEP' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Sep,
(select count(*) from cb_cord a, cb_shipment b where (a.cord_tnc_frozen between 250 and 300 ) and b.shipment_date >= to_date('10/01'||(selYear-2),'MM/dd/yyyy') and  
b.shipment_date <= to_date('09/30/'||(selYear-1),'MM/dd/yyyy') and a.pk_cord = b.fk_cord_id) as PrevYear
from dual
union
select  'F', '>300',
(select count(*)  from cb_cord a, cb_shipment b where a.cord_tnc_frozen > 300
and to_char(b.shipment_date,'MON')='OCT' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id) as Oct,
(select count(*)  from cb_cord a, cb_shipment b where a.cord_tnc_frozen > 300
and to_char(b.shipment_date,'MON')='NOV' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id) as Nov,
(select count(*)  from cb_cord a, cb_shipment b where a.cord_tnc_frozen > 300
and to_char(b.shipment_date,'MON')='DEC' and to_char(b.shipment_date,'YYYY') = (selYear-1) and a.pk_cord = b.fk_cord_id) as Dec,
(select count(*)  from cb_cord a, cb_shipment b where a.cord_tnc_frozen > 300
and to_char(b.shipment_date,'MON')='JAN' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Jan,
(select count(*)  from cb_cord a, cb_shipment b where a.cord_tnc_frozen > 300
and to_char(b.shipment_date,'MON')='FEB' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Feb,
(select count(*)  from cb_cord a, cb_shipment b where a.cord_tnc_frozen > 300
and to_char(b.shipment_date,'MON')='MAR' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Mar,
(select count(*)  from cb_cord a, cb_shipment b where a.cord_tnc_frozen > 300
and to_char(b.shipment_date,'MON')='APR' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Apr,
(select count(*)  from cb_cord a, cb_shipment b where a.cord_tnc_frozen > 300
and to_char(b.shipment_date,'MON')='MAY' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as May,
(select count(*)  from cb_cord a, cb_shipment b where a.cord_tnc_frozen > 300
and to_char(b.shipment_date,'MON')='JUN' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Jun,
(select count(*)  from cb_cord a, cb_shipment b where a.cord_tnc_frozen > 300
and to_char(b.shipment_date,'MON')='JUL' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Jul,
(select count(*)  from cb_cord a, cb_shipment b where a.cord_tnc_frozen > 300
and to_char(b.shipment_date,'MON')='AUG' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Aug,
(select count(*)  from cb_cord a, cb_shipment b where a.cord_tnc_frozen > 300
and to_char(b.shipment_date,'MON')='SEP' and to_char(b.shipment_date,'YYYY') = selYear and a.pk_cord = b.fk_cord_id) as Sep,
(select count(*) from cb_cord a, cb_shipment b where a.cord_tnc_frozen > 300 and b.shipment_date >= to_date('10/01'||(selYear-2),'MM/dd/yyyy') 
and  b.shipment_date <= to_date('09/30/'||(selYear-1),'MM/dd/yyyy') and a.pk_cord = b.fk_cord_id) as PrevYear
from dual;
return p_emp_refcur;
END;
/

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,159,23,'23_CREATE_F_TNC_COUNT_NCBI.sql',sysdate,'9.0.0 Build#616');

commit;