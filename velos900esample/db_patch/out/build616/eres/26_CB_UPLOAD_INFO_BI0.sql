create or replace TRIGGER ERES.CB_UPLOAD_INFO_BI0 BEFORE INSERT ON CB_UPLOAD_INFO
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW

DECLARE
  ERID NUMBER(10);
  USR VARCHAR(200); 
  RAID NUMBER(10);
  INSERT_DATA CLOB;
  
BEGIN
  SELECT TRUNC(SEQ_RID.NEXTVAL)  INTO ERID FROM DUAL;
  :NEW.RID :=ERID;
  USR := getuser(:NEW.LAST_MODIFIED_BY);
  SELECT SEQ_AUDIT.NEXTVAL INTO RAID FROM DUAL;
 
  AUDIT_TRAIL.RECORD_TRANSACTION(RAID, 'CB_UPLOAD_INFO', ERID, 'I',USR);
  INSERT_DATA := :NEW.PK_UPLOAD_INFO ||'|'|| to_char(:NEW.DESCRIPTION) ||'|'|| to_char(:NEW.COMPLETION_DATE) ||'|'||  
to_char(:NEW.TEST_DATE) ||'|'|| to_char(:NEW.PROCESS_DATE) ||'|'|| to_char(:NEW.VERIFICATION_TYPING) ||'|'|| to_char(:NEW.RECEIVED_DATE) ||'|'||  
to_char(:NEW.FK_CATEGORY) ||'|'|| to_char(:NEW.FK_SUBCATEGORY) ||'|'|| to_char(:NEW.FK_ATTACHMENTID) ||'|'|| to_char(:NEW.CREATOR) ||'|'|| 
to_char(:NEW.CREATED_ON) ||'|'|| to_char(:NEW.LAST_MODIFIED_BY) ||'|'|| to_char(:NEW.LAST_MODIFIED_DATE) ||'|'|| to_char(:NEW.IP_ADD) ||'|'||  
to_char(:NEW.RID) ||'|'|| to_char(:NEW.DELETEDFLAG) ||'|'|| to_char(:NEW.REPORT_DATE);                
                
                 INSERT INTO AUDIT_INSERT(RAID, ROW_DATA) VALUES (RAID, INSERT_DATA);                 
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,159,26,'26_CB_UPLOAD_INFO_BI0.sql',sysdate,'9.0.0 Build#616');

commit;