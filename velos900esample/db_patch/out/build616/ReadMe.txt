/* This readMe is specific to Velos eResearch version 9.0 build #616 */

=====================================================================================================================================
Garuda :

=====================================================================================================================================
eResearch:


=====================================================================================================================================
eResearch Localization:

1.Copy files -> messageBundle.properties and labelBundle.properties 
in ereshome\ and paste it to the folder specified by ERES_HOME 
(where eresearch.xml is also hosted), over-writing the existing files.


Following Files have been Modified:

1	activateuservel.jsp
2	copyevent.jsp
3	crflib.jsp
4	crflibbrowser.jsp
5	db_datamanager.jsp
6	db_financial.jsp
7	deleteStudySite.jsp
8	editmilestonestatus.jsp
9	eventappendix.jsp
10	getmultilookup.jsp
11	importReverseFile.jsp
12	irbsendback.jsp
13	labelBundle.properties
14	LC.java
15	LC.jsp
16	MC.java
17	MC.jsp
18	mdynfilterview.jsp
19	messageBundle.properties
20	myhomepanel.jsp
21	neweventcost.jsp
22	patientbudgetprint.jsp
23	patientdetailsquick.jsp
24	popupaddcrf.jsp
25	popuplnkfrmstudy.jsp
26	protocolcalendar.jsp
27	sectionBrowserNew.jsp
28	selectDOW.jsp
29	selectevent.jsp
30	signerror.jsp
31	specimenapndx.jsp
32	studyKeywordSearch.jsp
33	studySearch.jsp
34	studystatusbrowser.jsp
35	updateaccountuser.jsp
36	updatecategoryselectus.jsp
37	updateNewUser.jsp
38	updateresetpass.jsp
39	updateSequenceAndDisplay.jsp
40	updatestudystatus.jsp
41	updateviesettings.jsp
42	viewInvoice.jsp
=====================================================================================================================================
