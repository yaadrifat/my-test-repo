CREATE OR REPLACE TRIGGER "ER_SUBMISSION_PROVISO_BI0" 
BEFORE INSERT
ON ER_SUBMISSION_PROVISO REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(100);
  insert_data CLOB;


BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||',' || usr_firstname
  INTO usr FROM ER_USER WHERE pk_user =:NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
 USR := 'New User' ;
 END ;

SELECT TRUNC(seq_rid.NEXTVAL) INTO erid FROM dual;
       :NEW.rid := erid ;
       SELECT seq_audit.NEXTVAL INTO raid FROM dual;

Audit_Trail.record_transaction(raid, 'ER_SUBMISSION_PROVISO', erid, 'I', USR );

insert_data := :NEW.PK_SUBMISSION_PROVISO||'|'||:NEW.FK_SUBMISSION||'|'||
              :NEW.FK_SUBMISSION_BOARD||'|'||:NEW.FK_USER_ENTEREDBY||'|'||:NEW.rid||'|'||
              TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
              :NEW.CREATOR ||'|'||
              :NEW.IP_ADD||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_BY) ||'|'||
              TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) ||'|'
              || TO_CHAR(:NEW.PROVISO_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)
              || '|' || dbms_lob.SUBSTR(:NEW.PROVISO,3500,1) || '|' || :new.fk_codelst_provisotype;


INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, insert_data);
END;
/



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,74,11,'11_ER_SUBMISSION_PROVISO_BI0.sql',sysdate,'8.9.0 Build#531');

commit;
