update er_lkpcol set lkpcol_name = 'STORAGE_ALTERNATEID', lkpcol_keyword = 'STORAGE_ALTERNATEID'
where upper(lkpcol_name) = 'STORAGE_ALTERNALEID' and lkpcol_table = 'ERV_SPECIMEN_STORAGE';

commit;

update er_lkpcol set lkpcol_name = 'STORAGE_ALTERNATEID', lkpcol_keyword = 'STORAGE_ALTERNATEID'
where upper(lkpcol_name) = 'STORAGE_ALTERNALEID' and lkpcol_table = 'ERV_STORAGE';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,117,1,'01_AlternateID_fix.sql',sysdate,'8.10.0 Build#574');

commit;
 