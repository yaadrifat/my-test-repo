create or replace
TRIGGER "ESCH"."SCH_PROTOCOL_VISIT_BU0" 
BEFORE UPDATE
ON ESCH.SCH_PROTOCOL_VISIT
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW BEGIN
  :NEW.last_modified_date := SYSDATE ;  
  -- Requirement PCAL-20354
   IF NVL(:OLD.HIDE_FLAG ,0) <> NVL(:NEW.HIDE_FLAG ,0) THEN
		update event_assoc set HIDE_FLAG = :NEW.HIDE_FLAG WHERE FK_VISIT = :OLD.PK_PROTOCOL_VISIT;
    END IF;
  
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,140,1,'01_SCH_PROTOCOL_VISIT_BU0_TRIGGER.sql',sysdate,'9.0.0 Build#597');

commit;