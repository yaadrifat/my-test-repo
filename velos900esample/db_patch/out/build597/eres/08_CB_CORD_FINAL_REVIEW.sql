CREATE TABLE CB_CORD_FINAL_REVIEW(PK_CORD_FINAL_REVIEW NUMBER(10,0) PRIMARY KEY,
FK_CORD_ID NUMBER(10,0),
LICENSURE_CONFIRM_FLAG VARCHAR2(1),
ELIGIBLE_CONFIRM_FLAG VARCHAR2(1),
FINAL_REVIEW_DATE DATE,
CREATOR NUMBER(10),
CREATED_ON DATE,
IP_ADD VARCHAR2(15),
LAST_MODIFIED_BY NUMBER(10),
LAST_MODIFIED_ON DATE,
RID NUMBER(10),
DELETEDFLAG VARCHAR2(1)
);

COMMENT ON TABLE "CB_CORD_FINAL_REVIEW" IS 'Table to store CORD FINAL REVIEW information';
COMMENT ON COLUMN "CB_CORD_FINAL_REVIEW"."PK_CORD_FINAL_REVIEW" IS 'Primary key column';
COMMENT ON COLUMN "CB_CORD_FINAL_REVIEW"."FK_CORD_ID" IS 'Foreign key column for CB_CORD TABLE ';
COMMENT ON COLUMN "CB_CORD_FINAL_REVIEW"."LICENSURE_CONFIRM_FLAG" IS 'LICENSURE flag column';
COMMENT ON COLUMN "CB_CORD_FINAL_REVIEW"."ELIGIBLE_CONFIRM_FLAG" IS 'ELIGIBILITY flag column';
COMMENT ON COLUMN "CB_CORD_FINAL_REVIEW"."FINAL_REVIEW_DATE" IS 'Final review date and time';
COMMENT ON COLUMN "CB_CORD_FINAL_REVIEW"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row. '; 
COMMENT ON COLUMN "CB_CORD_FINAL_REVIEW"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.'; 
COMMENT ON COLUMN "CB_CORD_FINAL_REVIEW"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine. '; 
COMMENT ON COLUMN "CB_CORD_FINAL_REVIEW"."LAST_MODIFIED_BY" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_CORD_FINAL_REVIEW"."LAST_MODIFIED_ON" IS 'This column stores the name of user who last modified the row in the table.'; 
COMMENT ON COLUMN "CB_CORD_FINAL_REVIEW"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database. '; 
COMMENT ON COLUMN "CB_CORD_FINAL_REVIEW"."DELETEDFLAG" IS 'This column denotes whether record is deleted.';

CREATE  SEQUENCE SEQ_CB_CORD_FINAL_REVIEW MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,140,8,'08_CB_CORD_FINAL_REVIEW.sql',sysdate,'9.0.0 Build#597');

commit;
