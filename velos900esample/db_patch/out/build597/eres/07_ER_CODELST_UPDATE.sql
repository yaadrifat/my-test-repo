
Update er_codelst set codelst_desc='US-Ineligible'  where  codelst_type='unlicensed' and codelst_subtyp = 'u.s_inel';

Update er_codelst set codelst_desc='US-Incomplete'  where  codelst_type='unlicensed' and codelst_subtyp = 'u.s_inel_test';

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,140,7,'07_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 Build#597');

commit;
