set define off;

update er_fldactionlib set fldactionlib_name='Change State to Disabled/Read Only/Hide'
where fldactionlib_type='tempGrey';

commit;

DELETE FROM ER_REPXSL WHERE FK_REPORT in (109,108,124,160);

commit;

--FIX FOR BUG#6062
SET SCAN OFF;
UPDATE "ERES"."ER_REPFILTERMAP" SET REPFILTERMAP_COLUMN = '<td><DIV id="studyDIV"><Font class=comments><A href="javascript:openLookup(document.reports,''viewId=6013&form=reports&seperator=,&defaultvalue=&keyword=selstudyId|STUDY_NUMBER~paramstudyId|LKP_PK|[VELHIDE]'','''')">Select Study</Font></DIV></td><td><DIV id="studydataDIV"> <input TYPE="text" NAME="selstudyId" readonly value="[SELSTUDYNUMBER]"><input TYPE="hidden" NAME="paramstudyId" value="[SELSTUDYPK]"></DIV></td>'  
WHERE FK_REPFILTER = (Select PK_REPFILTER FROM ER_REPFILTER WHERE REPFILTER_COLDISPNAME = 'Study' and REPFILTER_KEYWORD = 'studyId') 
and repfiltermap_repcat= 'study';

commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,121,1,'01_data.sql',sysdate,'8.10.0 Build#578');

commit;
