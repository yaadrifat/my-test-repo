--Setting correct datatype for clob type columns
--Bug #2989, #4884, #4883 related to ad-hoc queries

Update er_lkpcol set LKPCOL_DATATYPE='clob' where lower(LKPCOL_NAME) = lower('STSEC_SECTN_TEXT')
and lower(LKPCOL_TABLE)=lower('rep_study_sections');

commit;

Update er_lkpcol set LKPCOL_DATATYPE='clob' where lower(LKPCOL_NAME) = lower('PTDEM_NOTES')
and lower(LKPCOL_TABLE)=lower('ERV_PERSON_COMPLETE');

commit;

update er_report set rep_sql = 
'SELECT
PROT_NAME,
PROT_DESC,
PROT_DURATION,
VISIT_NAME,
VISIT_DESC,
nvl(VISIT_INTERVAL,''No Interval Defined'') as VISIT_INTERVAL,
EVENT_NAME,
EVENT_DESC,
DECODE(trim(vwindow_bef),''(BEFORE)'','''',vwindow_bef) AS vwindow_bef,
DECODE(trim(vwindow_aft),''(AFTER)'','''',vwindow_aft) AS vwindow_aft,
EVENT_COST ,
RESOURCES,
APPENDIX,
MESSAGES,
FORMS,insert_after,decode(nvl(hide_flag,0),1,''Yes'',''No'')
 is_hidden,
 COVERAGE_TYPE,
EVENT_CPTCODE
,visit_win_before, visit_win_after
FROM  erv_calendar_template
WHERE prot_id = ~1 and nvl(EVENT_TYPE,''A'') <>''U''
ORDER BY displacement,PK_PROTOCOL_VISIT' 
where pk_report = 106; 

update er_report set rep_sql_clob = 
'SELECT
PROT_NAME,
PROT_DESC,
PROT_DURATION,
VISIT_NAME,
VISIT_DESC,
nvl(VISIT_INTERVAL,''No Interval Defined'') as VISIT_INTERVAL,
EVENT_NAME,
EVENT_DESC,
DECODE(trim(vwindow_bef),''(BEFORE)'','''',vwindow_bef) AS vwindow_bef,
DECODE(trim(vwindow_aft),''(AFTER)'','''',vwindow_aft) AS vwindow_aft,
EVENT_COST ,
RESOURCES,
APPENDIX,
MESSAGES,
FORMS,insert_after,decode(nvl(hide_flag,0),1,''Yes'',''No'')
 is_hidden,
 COVERAGE_TYPE,
EVENT_CPTCODE
,visit_win_before, visit_win_after
FROM  erv_calendar_template
WHERE prot_id = ~1 and nvl(EVENT_TYPE,''A'') <>''U''
ORDER BY displacement,PK_PROTOCOL_VISIT' 
where pk_report = 106; 


commit;

update er_report set rep_sql =
'SELECT
PROT_ID,
PROT_NAME,
PROT_DESC,
PROT_DURATION,
nvl(sch_date_grp, ''No Interval'') as sch_date_grp,
nvl(sch_date_grp_disp, ''No Interval Defined'' ) as sch_date_grp_disp,
pkg_dateutil.f_get_first_dayofyear_str AS cal_start_date,
TO_CHAR(SCH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AS sch_date,
TO_CHAR((sch_date - FUZZY_DURATION_bef),PKG_DATEUTIL.F_GET_DATEFORMAT) || '' - '' || TO_CHAR((sch_date + FUZZY_DURATION_aft),PKG_DATEUTIL.F_GET_DATEFORMAT) AS fuzzy_dates,
VISIT_NAME,
VISIT_DESC,
VISIT_INTERVAL,
EVENT_NAME,
EVENT_DESC,
FORMS,
nvl(displacement,0) as displacement,
COVERAGE_TYPE, visit_win_before, visit_win_after
FROM erv_calendar_template
WHERE prot_id = ~1 and nvl(EVENT_TYPE,''A'') <>''U''  and
nvl(HIDE_FLAG,0)=0
ORDER BY SCH_DATE,PK_PROTOCOL_VISIT'
where pk_report = 107;

update er_report set rep_sql_clob =
'SELECT
PROT_ID,
PROT_NAME,
PROT_DESC,
PROT_DURATION,
nvl(sch_date_grp, ''No Interval'') as sch_date_grp,
nvl(sch_date_grp_disp, ''No Interval Defined'' ) as sch_date_grp_disp,
pkg_dateutil.f_get_first_dayofyear_str AS cal_start_date,
TO_CHAR(SCH_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT) AS sch_date,
TO_CHAR((sch_date - FUZZY_DURATION_bef),PKG_DATEUTIL.F_GET_DATEFORMAT) || '' - '' || TO_CHAR((sch_date + FUZZY_DURATION_aft),PKG_DATEUTIL.F_GET_DATEFORMAT) AS fuzzy_dates,
VISIT_NAME,
VISIT_DESC,
VISIT_INTERVAL,
EVENT_NAME,
EVENT_DESC,
FORMS,
nvl(displacement,0) as displacement,
COVERAGE_TYPE, visit_win_before, visit_win_after
FROM erv_calendar_template
WHERE prot_id = ~1 and nvl(EVENT_TYPE,''A'') <>''U''  and
nvl(HIDE_FLAG,0)=0
ORDER BY SCH_DATE,PK_PROTOCOL_VISIT'
where pk_report = 107;
commit;



UPDATE ER_REPORT SET REP_NAME = 'By Coverage Type' WHERE pk_report = 160;


update er_report set REP_SQL='SELECT budgetsection_sequence, category_seq,BUDGET_NAME,PROT_CALENDAR,BUDGET_VERSION,BUDGET_DESC,STUDY_NUMBER,STUDY_TITLE,SITE_NAME, NVL(COVERAGE_TYPE,''Coverage Type NOT specified'') AS COVERAGE_TYPE,
BGTSECTION_NAME,
LINEITEM_NAME,lineitem_seq,
CPT_CODE,
NVL(COST_PER_PATIENT,0) AS COST_PER_PATIENT,
NVL(TOTAL_COST,0) AS TOTAL_COST,
DECODE(LINE_ITEM_INDIRECTS_FLAG,1,''Yes'','''') AS LINE_ITEM_INDIRECTS_FLAG,
DECODE(COST_DISCOUNT_ON_LINE_ITEM,1,''Yes'','''') AS COST_DISCOUNT_ON_LINE_ITEM,
INDIRECTS,
BUDGET_INDIRECT_FLAG,
FRINGE_BENEFIT,
FRINGE_FLAG,
PER_PATIENT_LINE_FRINGE,
TOTAL_LINE_FRINGE,
BUDGET_DISCOUNT,
DECODE(BUDGET_DISCOUNT_FLAG,1,''Discount'',2,''Markup'','''') AS BUDGET_DISCOUNT_FLAG,
TOTAL_COST_AFTER,
TOTAL_PAT_COST_AFTER,
PER_PAT_LINE_ITEM_DISCOUNT,
TOTAL_COST_DISCOUNT,
PERPAT_INDIRECT,
TOTAL_COST_INDIRECT,
BUDGET_CURRENCY,
DECODE(COST_CUSTOMCOL,''research'',''No'',''soc'',''Yes'',''Other'') AS standard_of_care,
LINEITEM_SPONSORAMOUNT AS  SPONSOR_AMOUNT,
LINEITEM_VARIANCE AS  L_VARIANCE,LINEITEM_DIRECT_PERPAT,TOTAL_COST_PER_PAT,TOTAL_COST_ALL_PAT
FROM erv_budget
WHERE pk_budget =  ~1  AND
pk_bgtcal = ~2
ORDER BY COVERAGE_TYPE, budgetsection_sequence,lineitem_seq, lower(LINEITEM_NAME)' 
where pk_report = 160;

commit;

update er_report set REP_SQL_CLOB='SELECT budgetsection_sequence, category_seq,BUDGET_NAME,PROT_CALENDAR,BUDGET_VERSION,BUDGET_DESC,STUDY_NUMBER,STUDY_TITLE,SITE_NAME, NVL(COVERAGE_TYPE,''Coverage Type NOT specified'') AS COVERAGE_TYPE,
BGTSECTION_NAME,
LINEITEM_NAME,lineitem_seq,
CPT_CODE,
NVL(COST_PER_PATIENT,0) AS COST_PER_PATIENT,
NVL(TOTAL_COST,0) AS TOTAL_COST,
DECODE(LINE_ITEM_INDIRECTS_FLAG,1,''Yes'','''') AS LINE_ITEM_INDIRECTS_FLAG,
DECODE(COST_DISCOUNT_ON_LINE_ITEM,1,''Yes'','''') AS COST_DISCOUNT_ON_LINE_ITEM,
INDIRECTS,
BUDGET_INDIRECT_FLAG,
FRINGE_BENEFIT,
FRINGE_FLAG,
PER_PATIENT_LINE_FRINGE,
TOTAL_LINE_FRINGE,
BUDGET_DISCOUNT,
DECODE(BUDGET_DISCOUNT_FLAG,1,''Discount'',2,''Markup'','''') AS BUDGET_DISCOUNT_FLAG,
TOTAL_COST_AFTER,
TOTAL_PAT_COST_AFTER,
PER_PAT_LINE_ITEM_DISCOUNT,
TOTAL_COST_DISCOUNT,
PERPAT_INDIRECT,
TOTAL_COST_INDIRECT,
BUDGET_CURRENCY,
DECODE(COST_CUSTOMCOL,''research'',''No'',''soc'',''Yes'',''Other'') AS standard_of_care,
LINEITEM_SPONSORAMOUNT AS  SPONSOR_AMOUNT,
LINEITEM_VARIANCE AS  L_VARIANCE,LINEITEM_DIRECT_PERPAT,TOTAL_COST_PER_PAT,TOTAL_COST_ALL_PAT
FROM erv_budget
WHERE pk_budget =  ~1  AND
pk_bgtcal = ~2
ORDER BY COVERAGE_TYPE, budgetsection_sequence,lineitem_seq, lower(LINEITEM_NAME)' where pk_report = 160;

commit;

DELETE FROM ER_REPXSL WHERE FK_REPORT in (106, 107);

commit;

DELETE FROM ER_REPXSL WHERE FK_REPORT in (110, 111,112,159);

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,78,1,'01_data.sql',sysdate,'8.9.0 Build#535');

commit;