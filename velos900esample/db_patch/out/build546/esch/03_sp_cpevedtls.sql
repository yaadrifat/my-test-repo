CREATE OR REPLACE PROCEDURE SP_CPEVEDTLS (
   P_OLDEVENTID   IN       NUMBER,
   P_NEWEVENTID   IN       NUMBER,
   P_CRFINFO      IN       CHAR,
   P_STATUS       OUT      NUMBER,
   p_user  IN NUMBER,
   p_ip IN VARCHAR2,
   p_copy_propagateflag IN NUmber Default 1

)
AS



  MDMODNAME VARCHAR2 (20);--KM
  MDMODELEMENTPK NUMBER;
  MDMODELEMENTDATA VARCHAR2(4000);

   DOCID     NUMBER;
   DOCNAME   VARCHAR2 (255);
   DOCDESC   VARCHAR2 (2000);
   DOCTYPE   CHAR (1);
   o_ret_number NUMBER;

   v_PK_EVENTDOC Number;

   v_pkeventcost Number;

   v_oldevent_type Varchar2(1);
   v_isMainEvent Number  ;
 V_PROPAGATE_FROM Number;
 v_docsize Number;
 v_docblob  BLOB;

 v_studyId number;--JM:

   pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SP_CPEVEDTLS ', pLEVEL  => Plog.LFATAL);

   CURSOR C1
   IS
      SELECT DOC_NAME, DOC_DESC, DOC_TYPE, SCH_EVENTDOC.PK_DOCS, PK_EVENTDOC, PROPAGATE_FROM, doc, DOC_SIZE
        FROM SCH_DOCS, SCH_EVENTDOC
       WHERE FK_EVENT = P_OLDEVENTID
         AND SCH_DOCS.PK_DOCS = SCH_EVENTDOC.PK_DOCS;



    CURSOR C2
   IS
      SELECT MD_MODNAME ,MD_MODELEMENTPK, MD_MODELEMENTDATA
        FROM ER_MOREDETAILS
       WHERE FK_MODPK = P_OLDEVENTID and md_modname  = 'evtaddlcode';

BEGIN


      -- the following sqsl should return only one row, but made it a for loop just to beon safe side
      if   p_copy_propagateflag = 1 then
            for k  in (SELECT  event_type FROM EVENT_DEF e1
             WHERE e1.event_id = P_OLDEVENTID
         UNION
                SELECT event_type  FROM EVENT_ASSOC e1
             WHERE e1.event_id =P_OLDEVENTID  )
         loop
                   v_oldevent_type := k.event_type ;

            end loop;

     -- if v_new_event_displacement  is 0, that means this information is added for the main event row and not any customized event

         if  trim(v_oldevent_type)  = 'E'  then
           v_isMainEvent := 1; -- the record is copied from event library
          else
          v_isMainEvent := 0; -- the record is a customized event
        end if;
    else
        v_isMainEvent := -1 ; -- the record is a 'copy' as a result of copy protocol, add to study
    end if;

 plog.debug(pctx,'v_isMainEvent' || v_isMainEvent);

  INSERT INTO SCH_EVENTCOST
               (PK_EVENTCOST,
                EVENTCOST_VALUE,
                FK_EVENT,
                FK_COST_DESC,
                FK_CURRENCY, propagate_from
               )
           SELECT SCH_EVENTCOST_SEQ.NEXTVAL, EVENTCOST_VALUE, P_NEWEVENTID,
                  FK_COST_DESC, FK_CURRENCY, decode( v_isMainEvent,0,nvl(propagate_from,PK_EVENTCOST),1, SCH_EVENTCOST_SEQ.CURRVAL,-1,nvl(propagate_from, PK_EVENTCOST))--insert old id
             FROM SCH_EVENTCOST
            WHERE FK_EVENT = P_OLDEVENTID;

-- added on 8th feb 2002 to add crflib along with event details -dk
--copy only free text crfs, crf_libflag=0

  INSERT INTO SCH_CRFLIB
              (PK_CRFLIB,
               CRFLIB_NUMBER,
               CRFLIB_NAME,
               FK_EVENTS,
               CRFLIB_FLAG,
              CRFLIB_FORMFLAG,
            CREATOR,
             CREATED_ON,
            IP_ADD,propagate_from
              )
  SELECT SEQ_SCH_CRFLIB.NEXTVAL,
           CRFLIB_NUMBER,
         CRFLIB_NAME,
           P_NEWEVENTID,
         P_CRFinfo,
         CRFLIB_FORMFLAG,
         p_user,
         SYSDATE,
         p_ip,  decode( v_isMainEvent,0,nvl(propagate_from,PK_CRFLIB),1, SEQ_SCH_CRFLIB.CURRVAL,-1,nvl(propagate_from,PK_CRFLIB))  --insert old id
   FROM SCH_CRFLIB
   WHERE FK_EVENTS = P_OLDEVENTID
        AND CRFLIB_FORMFLAG = 0;



--modified by sonia sahni 19 May 2003, include new columns for table sch_eventusr - eventusr_notes and eventusr_duration

   INSERT INTO SCH_EVENTUSR
               (PK_EVENTUSR, EVENTUSR, EVENTUSR_TYPE, FK_EVENT, EVENTUSR_DURATION,EVENTUSR_NOTES,propagate_from  )
           SELECT SCH_EVENTUSR_SEQ.NEXTVAL, EVENTUSR, EVENTUSR_TYPE, P_NEWEVENTID, EVENTUSR_DURATION,
           EVENTUSR_NOTES, decode( v_isMainEvent,0,nvl(propagate_from,PK_EVENTUSR),1, SCH_EVENTUSR_SEQ.CURRVAL,-1,nvl(propagate_from,PK_EVENTUSR))  ---insert old id
             FROM SCH_EVENTUSR
            WHERE FK_EVENT = P_OLDEVENTID;

-- modified by Sonia Abrol, 08/20/06 ...to include CRF forms (new implementation)


---**********************************------JM: 31Jul2008, #2978--


if (P_CRFINFO = 'S') then

--JM: 27May2008, added for, issue #2978 ---and FORM_TYPE <> 'SP'
INSERT INTO SCH_EVENT_CRF ( pk_eventcrf,  fk_form,form_type, other_links,
                               FK_EVENT,   CREATOR, CREATED_ON,
                               IP_ADD, PROPAGATE_FROM)
    SELECT SEQ_SCH_EVENT_CRF.nextval,fk_form,form_type, other_links,
                               P_NEWEVENTID, p_user,sysdate,p_ip,
                decode( v_isMainEvent,0,nvl(propagate_from,PK_EVENTCRF),1, SEQ_SCH_EVENT_CRF.CURRVAL,-1,nvl(propagate_from,PK_EVENTCRF))  ---insert old id
            from SCH_EVENT_CRF    WHERE FK_EVENT = P_OLDEVENTID ;

else

--JM: 27May2008, added for, issue #2978 ---and FORM_TYPE <> 'SP'
INSERT INTO SCH_EVENT_CRF ( pk_eventcrf,  fk_form,form_type, other_links,
                               FK_EVENT,   CREATOR, CREATED_ON,
                               IP_ADD, PROPAGATE_FROM)
    SELECT SEQ_SCH_EVENT_CRF.nextval,fk_form,form_type, other_links,
                               P_NEWEVENTID, p_user,sysdate,p_ip,
                decode( v_isMainEvent,0,nvl(propagate_from,PK_EVENTCRF),1, SEQ_SCH_EVENT_CRF.CURRVAL,-1,nvl(propagate_from,PK_EVENTCRF))  ---insert old id
            from SCH_EVENT_CRF    WHERE FK_EVENT = P_OLDEVENTID and FORM_TYPE <> 'SP';




end if;
---****************************************************************************------------


--modified by sonia abrol , 05/16/08 -- to include storage kits

INSERT INTO SCH_EVENT_KIT(  PK_EVENTKIT,  FK_EVENT   ,  FK_STORAGE  ,  PROPAGATE_FROM    , CREATOR, CREATED_ON,
                               IP_ADD)
    select seq_SCH_EVENT_KIT.nextval,P_NEWEVENTID,fk_storage,
decode( v_isMainEvent,0,nvl(propagate_from,PK_EVENTKIT),1, seq_SCH_EVENT_KIT.CURRVAL,-1,nvl(propagate_from,PK_EVENTKIT)),
p_user,sysdate,p_ip
from  SCH_EVENT_KIT where  FK_EVENT = P_OLDEVENTID;

/**********************
INSERT INTO sch_eventdoc (
   PK_EVENTDOC    ,
   PK_DOCS        ,
   FK_EVENT       )
Select sch_eventdoc_seq.nextval  ,
       pk_docs ,
       p_newEventId
From sch_eventdoc
Where fk_event = p_oldEventId
And   doc_type = 'F'  ;
*/








   OPEN C1;


   LOOP

      FETCH C1 INTO DOCNAME, DOCDESC, DOCTYPE, DOCID,  v_PK_EVENTDOC ,V_PROPAGATE_FROM,v_docblob,v_docsize;
      EXIT WHEN C1%NOTFOUND;


    --  IF (DOCTYPE = 'U') THEN


         INSERT INTO SCH_DOCS
                     (PK_DOCS, DOC_NAME, DOC_DESC, DOC_TYPE,doc, DOC_SIZE)
              VALUES(SCH_DOCS_SEQ.NEXTVAL, DOCNAME, DOCDESC, DOCTYPE , v_docblob,v_docsize);


         INSERT INTO SCH_EVENTDOC
                     (PK_EVENTDOC, PK_DOCS, FK_EVENT,propagate_from )
              VALUES(
                 SCH_EVENTDOC_SEQ.NEXTVAL,
                 SCH_DOCS_SEQ.CURRVAL,
                 P_NEWEVENTID,  decode( v_isMainEvent,0,nvl(v_propagate_from,v_PK_EVENTDOC ) ,1, SCH_EVENTDOC_SEQ.CURRVAL,-1,nvl(v_propagate_from,v_PK_EVENTDOC ))    --insert old id


              );

    /*  ELSE

         INSERT INTO SCH_EVENTDOC
                     (PK_EVENTDOC, PK_DOCS, FK_EVENT,propagate_from )
              VALUES(SCH_EVENTDOC_SEQ.NEXTVAL, DOCID, P_NEWEVENTID,  decode( v_isMainEvent,0,nvl(v_propagate_from,v_PK_EVENTDOC )  ,1, SCH_EVENTDOC_SEQ.CURRVAL,-1,nvl(v_propagate_from,v_PK_EVENTDOC ))
              ); --insert old id
      END IF; */

   END LOOP;

   CLOSE C1;

   --KM- #4279
    OPEN C2;
    LOOP
      FETCH C2 INTO MDMODNAME, MDMODELEMENTPK, MDMODELEMENTDATA;
      EXIT WHEN C2%NOTFOUND;

      INSERT INTO ER_MOREDETAILS (PK_MOREDETAILS,FK_MODPK,MD_MODNAME,MD_MODELEMENTPK,MD_MODELEMENTDATA, CREATOR, IP_ADD)
      VALUES(ERES.SEQ_ER_MOREDETAILS.NEXTVAL,P_NEWEVENTID,MDMODNAME,MDMODELEMENTPK,MDMODELEMENTDATA,P_USER,P_IP);
  END LOOP;
  CLOSE C2;



/**********************************
TEST
set serveroutput on
declare
stat number;
begin

sp_cpevedtls(4954,12345,stat) ;
dbms_output.put_line(to_char(stat) ) ;
end ;
**************************************/

 --  COMMIT;
END;
/


CREATE or replace SYNONYM ERES.SP_CPEVEDTLS FOR SP_CPEVEDTLS;


CREATE or replace SYNONYM EPAT.SP_CPEVEDTLS FOR SP_CPEVEDTLS;


GRANT EXECUTE, DEBUG ON SP_CPEVEDTLS TO EPAT;

GRANT EXECUTE, DEBUG ON SP_CPEVEDTLS TO ERES;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,89,3,'03_sp_cpevedtls.sql',sysdate,'8.9.0 Build#546');

commit;
