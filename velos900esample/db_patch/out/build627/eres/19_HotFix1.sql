--STARTS UPDATING RECORD INTO ER_BROWSERCONF TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_BROWSERCONF
    where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch')
    AND BROWSERCONF_COLNAME='PK_STUDY';
  if (v_record_exists = 1) then
	update ER_BROWSERCONF set BROWSERCONF_SETTINGS='{"key":"PK_STUDY", "label":"Study Summary","resizeable":true,"format":"studyLink"}' , BROWSERCONF_EXPLABEL = 'Study Summary' where FK_BROWSER=(select PK_BROWSER from ER_BROWSER where BROWSER_MODULE='advSearch') and BROWSERCONF_COLNAME='PK_STUDY';
	commit;
  end if;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,170,19,'19_HotFix1.sql',sysdate,'9.0.0 Build#627');

commit;
--END--
