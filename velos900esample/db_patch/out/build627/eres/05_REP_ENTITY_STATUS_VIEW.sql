CREATE OR REPLACE FORCE VIEW "ERES"."REP_ENTITY_STATUS_VIEW" ("CORD_REGISTRY_ID", "CORD_LOCAL_CBU_ID", "REGISTRY_MATERNAL_ID", "MATERNAL_LOCAL_ID", "CREATED_ON", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "STATUS_TYPE", "STATUS_VALUE", "STATUS_REMARKS", "STATUS_DATE", "IP_ADD", "PK_ROW_ID", "MODULE_ID")
AS
  SELECT cord.Cord_Registry_Id,
    cord.Cord_Local_Cbu_Id,
    cord.Registry_Maternal_Id,
    cord.maternal_local_id,
    cb.Created_On ,
    (SELECT ER_USER.USR_FIRSTNAME
      || ' '
      || ER_USER.USR_LASTNAME
    FROM Er_User
    WHERE Pk_User = cb.Creator
    ) Creator,
    (SELECT ER_USER.USR_FIRSTNAME
      || ' '
      || ER_USER.USR_LASTNAME
    FROM Er_User
    WHERE Pk_User = cb.Last_Modified_By
    ) Last_Modified_By,
    cb.Last_Modified_Date,
    cb.STATUS_TYPE_CODE                AS STATUS_TYPE,
    f_codelst_desc(cb.FK_STATUS_VALUE) AS STATUS_VALUE ,
    cb.status_remarks,
    cb.status_date,
    cb.IP_ADD,
    arm.PK_ROW_ID,
    arm.module_id
  FROM AUDIT_ROW_MODULE arm,
    Cb_Cord cord,
    CB_ENTITY_STATUS cb
  WHERE cb.Entity_Id      =cord.Pk_Cord
  AND cb.PK_ENTITY_STATUS = arm.module_id
  AND arm.table_name      ='CB_ENTITY_STATUS';
  
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,170,5,'05_REP_ENTITY_STATUS_VIEW.sql',sysdate,'9.0.0 Build#627');

commit;