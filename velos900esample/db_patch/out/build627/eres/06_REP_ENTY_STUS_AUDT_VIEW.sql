CREATE OR REPLACE FORCE VIEW "ERES"."REP_ENTY_STUS_AUDT_VIEW" ("CORD_REGISTRY_ID", "CORD_LOCAL_CBU_ID", "REGISTRY_MATERNAL_ID", "MATERNAL_LOCAL_ID", "CREATED_ON", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "COLUMN_NAME", "COLUMN_DISPLAY_NAME", "OLD_VALUE", "NEW_VALUE", "REASON", "REMARKS", "FK_ROW_ID")
AS
  SELECT status.CORD_REGISTRY_ID,
    status.Cord_Local_Cbu_Id,
    status.Registry_Maternal_Id,
    status.Registry_Maternal_Id,
    status.Created_On ,
    status.Creator,
    status.Last_Modified_By,
    status.Last_Modified_Date,
    acm.column_name,
    acm.column_display_name,
    acm.old_value,
    acm.new_value,
    f_codelst_desc(acm.FK_CODELST_REASON) AS REASON,
    acm.REMARKS,
    acm.fk_row_id
  FROM REP_ENTITY_STATUS_VIEW status,
    AUDIT_COLUMN_MODULE acm
  WHERE status.pk_row_id = acm.fk_row_id;
  
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,170,6,'06_REP_ENTY_STUS_AUDT_VIEW.sql',sysdate,'9.0.0 Build#627');

commit;