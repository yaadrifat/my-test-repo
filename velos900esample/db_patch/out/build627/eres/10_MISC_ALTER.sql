set define off;

--SQL for the Cord Entry ->Proceesing Start Time
DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'CB_CORD'  AND column_name = 'PRCSNG_START_TIME';
   if (countFlage = 0) then
 execute immediate ('alter table CB_CORD add PRCSNG_START_TIME VARCHAR2(50 CHAR)');
  end if;
end;
/
 COMMENT ON COLUMN CB_CORD.PRCSNG_START_TIME IS 'This column will store Processing Start Time.'; 
commit;

--SQL for the Cord Entry ->Freeze Time
DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'CB_CORD'  AND column_name = 'FRZ_TIME';
   if (countFlage = 0) then
 execute immediate ('alter table CB_CORD add FRZ_TIME VARCHAR2(50 CHAR)');
  end if;
end;
/
 COMMENT ON COLUMN CB_CORD.FRZ_TIME IS 'This column will store Freeze Time.'; 
commit;

--STARTS ADDING COLUMN ALERT_TYPE TO CB_ALERT_INSTANCES TABLE
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'CB_ALERT_INSTANCES' AND COLUMN_NAME = 'ALERT_TYPE';
IF (v_column_exists = 0) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE CB_ALERT_INSTANCES ADD(ALERT_TYPE NUMBER(10,0))';
END IF;
END;
/
--END


COMMENT ON COLUMN CB_ALERT_INSTANCES.ALERT_TYPE IS 'Stores reference to er_codelst where codelst_type=alert_type';

--STARTS ADDING COLUMN MAIL_ID TO CB_ALERT_INSTANCES TABLE
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'CB_ALERT_INSTANCES' AND COLUMN_NAME = 'MAIL_ID';
IF (v_column_exists = 0) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE CB_ALERT_INSTANCES ADD(MAIL_ID VARCHAR2(255 CHAR))';
END IF;
END;
/
--END


COMMENT ON COLUMN CB_ALERT_INSTANCES.MAIL_ID IS 'Stores mail id where alert to be send';


--STARTS ADDING COLUMN ALERT_WORDING TO CB_ALERT_INSTANCES TABLE
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'CB_ALERT_INSTANCES' AND COLUMN_NAME = 'ALERT_WORDING';
IF (v_column_exists = 0) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE CB_ALERT_INSTANCES ADD(ALERT_WORDING VARCHAR2(4000 BYTE))';
END IF;
END;
/
--END


COMMENT ON COLUMN CB_ALERT_INSTANCES.ALERT_WORDING IS 'Stores what data should be display as alert';


--STARTS ADD THE COLUMN CORD_HISTORIC_CBU_ID TO CB_CORD--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD'
    AND COLUMN_NAME = 'CORD_HISTORIC_CBU_ID'; 

  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE CB_CORD modify CB_RHTYPE_OTHER_SPEC VARCHAR2(30 BYTE)';
  end if;
end;
/
commit;



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,170,10,'10_MISC_ALTER.sql',sysdate,'9.0.0 Build#627');

commit;