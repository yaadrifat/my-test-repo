set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v9.0.0 Build#637-ES007'
WHERE CTRL_KEY = 'app_version';

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,187,0,'00_er_version.sql',sysdate,'9.0.0 B#637-ES007');

COMMIT;
