<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.StringTokenizer"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="storageB" scope="request" class="com.velos.eres.web.storage.StorageJB"/>
<jsp:useBean id="codeB" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="StorageStatJB" scope="request" class="com.velos.eres.web.storageStatus.StorageStatusJB" />
<jsp:useBean id="codeLstB" scope="page" class="com.velos.eres.web.codelst.CodelstJB" />

<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*"%>
<head>
<style type="text/css">
 table.sample {
	border-width: medium;
	border-style: groove;
	border-color: gray;
	border-collapse: separate;
	background-color: white;
}
table.sample th {
	border-width: 1px;
	padding: 1px;
	border-style: solid;
	border-color: gray;
	background-color: white;

}
table.sample td {
	border-width: medium;
	padding: 1px;
	border-style: groove;
	border-color: gray;
	border-collapse: separate;
	}

.tableTd {
    font-family: Verdana, Arial, Helvetica,  sans-serif;
    color:black;
    font-size:10pt;
    text-align:center;
}
 </style>

</script>
<%!
// Declare variables and methods
final int totalWidth = 650;
final int totalWidth2D = 630;
final int labelWidth = 100;
final int labelWidth2D = 100;
final String imgHeight = "\"20\"";
final String imgWidth  = "\"20\"";

String formTypeDescriptionPerLevel(String inValue, ArrayList typeCodepks, ArrayList typeCodeDescs) {
    StringBuffer descripSB = new StringBuffer();
    String[] combo = inValue.split(",");
    for (int iX=0; iX<combo.length; iX++) {
        String parts[] = combo[iX].split("@");
        descripSB.append(parts[0]).append(" ").append(getPluralizedDescrip(parts[0], parts[1],
                typeCodepks, typeCodeDescs)).append("<BR />");
    }
    descripSB.delete(descripSB.length()-6, descripSB.length());
    return descripSB.toString();
}
String getPluralizedDescrip(String inNum, String inValue, ArrayList typeCodepks, ArrayList typeCodeDescs) {
    String storageSubtype = (String)typeCodeDescs.get(typeCodepks.indexOf(new Integer(EJBUtil.stringToNum(inValue))));
    if ("1".equals(inNum)) {
        return storageSubtype;
    }
    if ("Box".equals(storageSubtype)) return "Boxes";
    if ("Shelf".equals(storageSubtype)) return "Shelves";
    return storageSubtype+"s";
}
String convertIntToAlphaLabel(int in) {
    if (in < 1 || in > 702) return "";
    int quotient = (in-1) / 26 - 1;
    int remainder = (in-1) % 26;
    char cQuotient = (char)(65 + quotient);
    char cRemainder = (char)(65 + remainder);
    StringBuffer sb = new StringBuffer();
    if (quotient < 0) {
        sb.append(cRemainder);
    } else {
        sb.append(cQuotient).append(cRemainder);
    }
    return sb.toString();
}
// End of declaration
%>

<%
 HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession)) {

    String name = "";
    String id = "";
    // gridFor values => ST=storage; BR=browser (list of top pk's); SP=specimen; EM=embedded
    String gridFor = "";
	int cellWidth = 0;
		String viewSpecimen="";
		String seltPatient="";
	int cellWidthRemaining = 0;
	int numCellRemaining = 0;
	String availImagePath ="";
	boolean tableIs2D = false;
	String child = request.getParameter("child");
	String parent = request.getParameter("parent");
	name  = request.getParameter("name");
	id = request.getParameter("storageId");
	gridFor = request.getParameter("gridFor");
	String topStorageName = request.getParameter("topStorageName");
	String pkTopStorage = request.getParameter("pkTopStorage");
	String userStorageType = request.getParameter("storageType"); // user-specified storage type for search parameter
	String browserType = request.getParameter("browser");
	String pkStorageForExplorer = request.getParameter("pkStorage");
	String explorer = request.getParameter("explorer");

	String spSrchLocFlag = request.getParameter("srchLocFlag");
	spSrchLocFlag = (spSrchLocFlag==null)?"":spSrchLocFlag;

	if (StringUtil.isEmpty(browserType))
	{
	    browserType = "0";
	}

	if (StringUtil.isEmpty(gridFor))
	{
		gridFor = "SP"; //storage
	}

	if (!StringUtil.isEmpty(topStorageName))
	{
	    topStorageName = StringUtil.htmlEncode(topStorageName);
	}

	if (!StringUtil.isEmpty(pkTopStorage))
	{
	    pkTopStorage = StringUtil.htmlEncode(pkTopStorage);
	}

	int accId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));

	CodeDao cdStatus = new CodeDao();
	CodeDao cdStorageType = new CodeDao();

	ArrayList codepks = new ArrayList();
	ArrayList codeSubtypes = new ArrayList();
	ArrayList codeCustom1s = new ArrayList();
	ArrayList codeDescs = new ArrayList();


	ArrayList typeCodepks = new ArrayList();
	ArrayList typeCodeSubtypes = new ArrayList();
	ArrayList typeCodeCustom1s = new ArrayList();
	int typePos = 0;


	//cdStatus = codeB.getCodelstData(accId,"storage_stat");

	cdStatus.getCodeValues("storage_stat");
	cdStorageType.getCodeValues("store_type");


	codeSubtypes =  cdStatus.getCSubType();
	codeCustom1s = cdStatus.getCodeCustom1();
	codepks = cdStatus.getCId();
	codeDescs  = cdStatus.getCDesc();

	typeCodepks = cdStorageType.getCId();
	typeCodeSubtypes = cdStorageType.getCSubType();
	typeCodeCustom1s = cdStorageType.getCodeCustom1();
	ArrayList typeCodeDescs = cdStorageType.getCDesc();

	//get PK for KIT
	int kitLoc = 0;
	int kitCodePk = -1;

	kitLoc = typeCodeSubtypes.indexOf("Kit");

	if (kitLoc > -1) //get PK
	{
		kitCodePk = ((Integer)typeCodepks.get(kitLoc)).intValue();
	}

	Hashtable htArgs = new Hashtable();

	if (!StringUtil.isEmpty(parent))
	{
		htArgs.put("parent",parent);
	}

	if (!StringUtil.isEmpty(id))
	{
		htArgs.put("id",id);
	}


	if (!StringUtil.isEmpty(name))
	{
		htArgs.put("name",name);
	}


	// dispType: B = browser view (list of top pk's); G = grid veiw
	String dispType = "";

	dispType = request.getParameter("dispType");
	//System.out.println("dispType" + dispType);
	//System.out.println("parent" + parent+"*");


	if (StringUtil.isEmpty(dispType))
	{
		dispType = "G"; //grid
	}
	//52

	if (dispType.equals("B"))
	{
		htArgs.put("orderby"," lower_parent_name, lower_storage_name ");
	    htArgs.remove("gridView");

		if ( gridFor.equals("SP") || gridFor.equals("EM"))
		{
			htArgs.put("ignoreTemplate","1");
		}
		if ( !StringUtil.isEmpty(userStorageType) ) {
		    htArgs.put("storageType", userStorageType);
		} else {
		    htArgs.remove("storageType");
		}
	}  else {
		htArgs.put("gridView", "Y");
	}

	if (explorer != null)
	{
	    if (child != null) {
		    htArgs.put("gridView", "Y");
	        htArgs.put("searchUpward", "Y");
		    htArgs.remove("ignoreTemplate");
		    htArgs.put("child", String.valueOf(EJBUtil.stringToNum(child))); // sanitize child
	    } else {
	        htArgs.remove("searchUpward");
	        htArgs.remove("child");
	    }
	}


	StorageDao sdao = storageB.searchStorage(htArgs,accId);
	ArrayList sChain = sdao.getStorageChain();
	ArrayList tdColors = new ArrayList();

	ArrayList pkStorages = new ArrayList();
	ArrayList fkStorages = new ArrayList();
    ArrayList storageIds = new ArrayList();
    ArrayList storageNames = new ArrayList();
    ArrayList coordxs = new ArrayList();
    ArrayList coordys = new ArrayList();
    ArrayList statusCodes = new ArrayList();
    ArrayList storageDim1 =  new ArrayList();
    ArrayList storageDim2 =  new ArrayList();
    ArrayList specAllocationCount = new ArrayList();
    ArrayList childAllocationCount = new ArrayList();
    ArrayList parentStorageName = new ArrayList();
    ArrayList storageTypes = new ArrayList();
    ArrayList storageLevel = new ArrayList();

    int len = 0;
    int cX = 0;
    int oldCY = -1;
    int cY = 0;
    int pkStorage = 0;
    String storageName = "";
    String storageId = "";
    Integer statusCode = null;
    String codeSubType = "";
    String codeColor = "";
    String statusDesc = "";
    int codePos = 0;
    int dim1 = 0;
    int dim2 = 0;
    int specAllocation = 0;
    int childAllocation = 0;
    String parentLocation = "";
    String selectLocationStr="";
    String selectLocationScript = "";
    String selectSpecimen = "&nbsp;&nbsp;&nbsp;";
    String selectPatient = "";
    Integer storageType=null;      // storage type for search result
    String typeImagePath = "";

    pkStorages = sdao.getPkStorages();
    fkStorages = sdao.getFkStorages();
    storageIds =  sdao.getStorageIds();
    storageNames = sdao.getStorageNames();

    coordxs =	sdao.getStorageCoordinateXs();
    coordys =	sdao.getStorageCoordinateYs();
    statusCodes = sdao.getStorageStatus();
    storageDim1 = sdao.getStorageDim1();
    storageDim2 = sdao.getStorageDim2();
    specAllocationCount = sdao.getSpecAllocationCount();
    childAllocationCount = sdao.getChildAllocationCount();
    parentStorageName = sdao.getStorageParent();
    storageTypes=sdao.getFkCodelstStorageTypes();
    storageLevel = sdao.getStorageLevel();
    int intLevel = 0;
    int oldIntLevel = -1;
    int numLevelsDisplayed = 0;
    int vBorderW = "1".equals(browserType) ? 3 : 5; // 3 pts for IE; 5 pts for FireFox
    if (!"EM".equals(gridFor)) vBorderW = 6;
    len = pkStorages.size();

    if (explorer != null) {
        topStorageName = sdao.getTopStorageNameForExplorer();
    }

    //loop
    String tabclassname = "";

    if (dispType.equals("G"))
    {
    	tabclassname = "tableDefault";
    }
    else
    {
    	tabclassname="tableDefault";
    }

    if (! StringUtil.isEmpty(topStorageName) && dispType.equals("G")) {
        if((!StringUtil.isEmpty(parent) && !StringUtil.isEmpty(pkTopStorage) &&
                parent.equals(pkTopStorage)) || explorer != null)
    	{
		if(gridFor.equals("EM")){
				String fkCodeLstStorStat="";
				String storId="";
				String storName="";
				String storSubType="";
				String storDesc="";
				String addSpecimen="";
				String addPat="";
				String latestStoerageCode="";
				String paravailImagePath="";
				ArrayList specList=null;
				ArrayList patList=null;
				ArrayList fkCodeLstStorStats=null;
				StorageStatusDao strStatDao = StorageStatJB.getStorageStausValues(EJBUtil.stringToNum(parent));
				fkCodeLstStorStats = strStatDao.getFkCodelstStorageStatuses();
				storageB.setPkStorage(EJBUtil.stringToNum(parent));
				storageB.getStorageDetails();
				storName=storageB.getStorageName();
				storSubType=storageB.getFkCodelstStorageType();
				storDesc=codeLstB.getCodeDescription(EJBUtil.stringToNum(storSubType));
				storId=storageB.getStorageId();
				CodeDao cdStoageStat = new CodeDao();
				latestStoerageCode = cdStoageStat.getCodeSubtype(EJBUtil.stringToNum(fkCodeLstStorStat = ((fkCodeLstStorStats.get(0))==null)?"":(fkCodeLstStorStats.get(0)).toString()));
				sdao.getSpecimensAndPatients(EJBUtil.stringToNum(parent),accId);
				specList=sdao.getSpecimens();
				patList=sdao.getSpecimenPatients();
				if(!specList.isEmpty()){
				for(int i=0;i<specList.size();i++)
				{
				if(!StringUtil.isEmpty(addSpecimen))
				addSpecimen=addSpecimen+","+specList.get(i);
				else		
				addSpecimen=specList.get(i).toString();
				}
				}
				if(!patList.isEmpty()){
				if(EJBUtil.stringToNum(patList.get(0).toString())==0 && patList.size()==1){}
				else{
				for(int i=0;i<patList.size();i++)
				{
				if(!StringUtil.isEmpty(addPat))
				addPat=addPat+","+patList.get(i);
				else
				addPat=patList.get(i).toString();
				}
				}
				}
				selectLocationScript = "\"goToStorageDetail("+parent+")\"";
				if(!StringUtil.isEmpty(addSpecimen)){
				 if("PartOccupied".equals(latestStoerageCode) || ("Occupied".equals(latestStoerageCode) && (specList.size())>1))
				  viewSpecimen = " <A href=\"#\" onClick=processAjaxCall('"+addSpecimen+"'); onmouseout=return nd();>"+LC.L_View_Specimens/*View Specimens*****/+"</A>";
				 else
				 viewSpecimen = " <A href=\"#\" onClick=goToSpecimenDetails("+EJBUtil.stringToNum(addSpecimen)+")>"+LC.L_Sp/*SP*****/+"</A>";
				      if (!StringUtil.isEmpty(addPat)) {
		             if (!addPat.contains(","))
					 seltPatient = "&nbsp;<A href=\"#\" onClick=goToPatientDetails("+addPat+")>"+LC.L_P/*P*****/+"</A>";
		         } else {
		             seltPatient = "";
		         }
				 }
				if ("Occupied".equals(latestStoerageCode)) paravailImagePath = "images/occupied.jpg";
                    else if ("PartOccupied".equals(latestStoerageCode)) paravailImagePath = "images/part_occ.jpg";
                     else paravailImagePath = "images/available.jpg";
    		%>
			
    			<table class="<%=tabclassname%>"><TH><%= topStorageName%></TH> <tr><td onmouseover="return callOverlib('<%=storId%>','<%=storName%>',
				'<%=storDesc%>','<%=latestStoerageCode%>');" onmouseout="return nd();">
             <input type=image name="availabilityIcon" id="availabilityIcon" onClick="<%=selectLocationScript.replaceAll("^\"", "").replaceAll("\"$", "")%>;return false" src="<%=paravailImagePath%>"/>
			<%
			//specAllocation = EJBUtil.stringToNum((String)specAllocationCount.get(k));
			if("PartOccupied".equals(latestStoerageCode) || ("Occupied".equals(latestStoerageCode) && (specList.size())>1)){%>
			 <font size=1>
	    			 	<%=viewSpecimen%>
                        </font>
						<%}
						else{%>
						<font size=1>
	    			 	<%=viewSpecimen%><%=seltPatient%>
                        </font>
						<%}%>
			 </tr><td>
			   </table>
    		<%
			}
    	}
    }
    if(dispType.equals("B"))
    	{ %>
    <table class="<%=tabclassname%> basetbl" border = "0" width="100%" cellspacing="0" cellpadding="0">
    		<TH><%=LC.L_Storage_Name%><%--Storage Name*****--%></TH><TH><%=LC.L_Id_Upper%><%--ID*****--%></TH>
            <!--  <tr><td></td> commented this line for the bug fix 9106 -->
     <% }   %>
    <%
    // Gather cell information for display
    Hashtable tableInfo = new Hashtable();
    HashMap rowsPerLevel = new HashMap();
    for (int k = 0;k<len;k++)
    {
    	if (storageLevel.size() > 0) {
    	    // Collect storage type info and cell counts
        	storageType = new Integer(EJBUtil.stringToNum((String)storageTypes.get(k)));
        	Integer numThisType = (Integer)tableInfo.get(storageLevel.get(k)+"-"+storageType);
        	int numThisTypeInt = 0;
        	if (numThisType == null) { numThisTypeInt = 0; }
        	else { numThisTypeInt = numThisType.intValue(); }
    	    numThisTypeInt++;
        	tableInfo.put(storageLevel.get(k)+"-"+storageType, new Integer(numThisTypeInt));
        	rowsPerLevel.put(storageLevel.get(k), coordys.get(k));
    	}
    	if (sChain.contains(pkStorages.get(k))) {
    	    tdColors.add(k, "#FDFDDE");
    	} else {
        	cY = EJBUtil.stringToNum((String) coordys.get(k));
        	if (cY %2 == 1) tdColors.add(k, "#DADADA");
        	else tdColors.add(k, "#F7F7F7");
    	}
    }

    // Decipher storage types and cell count per level
    Enumeration enum1 = tableInfo.keys();
    HashMap subtypesPerLevel = new HashMap();
    HashMap countPerLevel = new HashMap();
    while(enum1.hasMoreElements()) {
        String sKey = (String)enum1.nextElement();
        String[] parts = sKey.split("-"); // parts[0] is level; parts[1] is type
        String descripPerLevel = (String)subtypesPerLevel.get(parts[0]);
        if (descripPerLevel == null) {
            descripPerLevel = tableInfo.get(sKey)+"@"+parts[1];
        } else {
            descripPerLevel = descripPerLevel+","+tableInfo.get(sKey)+"@"+parts[1];
        }
        subtypesPerLevel.put(parts[0], descripPerLevel);

        Integer thisTypeCount = (Integer) countPerLevel.get(parts[0]);
        if (thisTypeCount == null) {
            thisTypeCount = (Integer)tableInfo.get(sKey);
        } else {
            int tmpCount = thisTypeCount.intValue();
            tmpCount += ((Integer)tableInfo.get(sKey)).intValue();
            thisTypeCount = Integer.valueOf(tmpCount);
        }
        countPerLevel.put(parts[0], thisTypeCount);
    }

    Iterator keyIter = subtypesPerLevel.keySet().iterator();
    String sKey = null;
    while(keyIter.hasNext()) {
        sKey = (String)keyIter.next();
        String typeDescript = formTypeDescriptionPerLevel((String)subtypesPerLevel.get(sKey),
                typeCodepks, typeCodeDescs);
        subtypesPerLevel.put(sKey, typeDescript);
    }

    // Now display all the cells
    for (int k = 0;k<len;k++)
    {
    	cX = EJBUtil.stringToNum( (String) coordxs.get(k));
    	cY = EJBUtil.stringToNum( (String) coordys.get(k)) ;


    	pkStorage = ((Integer) pkStorages.get(k)).intValue();
    	storageName = (String) storageNames.get(k);
    	storageId = (String) storageIds.get(k);

    	statusCode = Integer.valueOf((String)statusCodes.get(k));
    	dim1 = EJBUtil.stringToNum((String)storageDim1.get(k));
    	dim2 = EJBUtil.stringToNum((String)storageDim2.get(k));
    	childAllocation = EJBUtil.stringToNum((String)childAllocationCount.get(k));
    	parentLocation = (String) parentStorageName.get(k);

    	storageType = new Integer(EJBUtil.stringToNum((String)storageTypes.get(k)));

    	if (storageLevel.size() > 0) {
    	    intLevel = EJBUtil.stringToNum((String)storageLevel.get(k));
    	}

    	if(StringUtil.isEmpty(parentLocation))
    	{
    		parentLocation = "-";
    	}

    	 codePos = codepks.indexOf(statusCode);

    	 if (codePos >=0)
    	 {
    	   codeSubType = (String)codeSubtypes.get(codePos);
    	   //codeColor = (String)codeCustom1s.get(codePos);
    	   statusDesc = (String)codeDescs.get(codePos);

    	   if (StringUtil.isEmpty(codeColor))
    	   {
    	   	//codeColor = "";
    	   }
    	 }
    	 /////////////

 			 typePos = typeCodepks.indexOf(storageType);
	      if (typePos >=0)
    	 {

    	   typeImagePath = (String)typeCodeCustom1s.get(typePos);


    	   if (StringUtil.isEmpty(typeImagePath))
    	   {
    	   	typeImagePath = "";
    	   }
    	 }else
    	 {
    	 	typeImagePath ="";
    	 }

	     if ("1".equals(explorer))
 		 {
 		     selectLocationScript = "\"goToDetailInParent("+pkStorage+")\"";
 		     selectLocationStr = "<A href=\"#\" onClick="+selectLocationScript+">"+LC.L_JumpTo/*JumpTo*****/+"</A>";
 		 }
	     // gridFor values => ST=storage; BR=browser (list of top pk's); SP=specimen; EM=embedded
	     else if (gridFor.equals("ST"))
		 {
    	     if ( ("Available".equals(codeSubType))  && childAllocation==0)
		     {
	     	     selectLocationScript = "\"selectStorage('"+storageName+"', 1 , 1 ,"+pkStorage+")\"";
		         selectLocationStr = "<A href=\"#\" onClick="+selectLocationScript+">"+LC.L_Select/*Select*****/+"</A>";
		     }
			  else if ("PartOccupied".equals(codeSubType))
		     {
		         selectLocationScript = "\"selectStorage('"+storageName+"', 1 , 1 ,"+pkStorage+")\"";
		         selectLocationStr = "<A href=\"#\" onClick="+selectLocationScript+">"+LC.L_Select/*Select*****/+"</A>";
		     }
		     else
		     {
		         selectLocationStr = selectLocationScript = "";
		     }
		 } else if (gridFor.equals("BR"))
		 {
		     selectLocationScript = "\"selectStorage('"+storageName+"', "+cX+" , "+cY+" ,"+pkStorage+")\"";
		     selectLocationStr = "<A href=\"#\" onClick="+selectLocationScript+">"+LC.L_Select/*Select*****/+"</A>";
		 } else if (gridFor.equals("EM"))
		 {
		     selectLocationScript = "\"goToStorageDetail("+pkStorage+")\"";
		     selectLocationStr = "<A href=\"storageunitdetails.jsp?srcmenu=tdmenubaritem6&selectedTab=2&mode=M&pkStorage="+pkStorage+"\">"+LC.L_Select/*Select*****/+"</A>";
		     if (sdao.getSpecimenPks(pkStorage) != null) {
		        if("PartOccupied".equals(codeSubType)|| ("Occupied".equals(codeSubType) && sdao.getSpecimenPks(pkStorage).contains(",")))
				 {
				  selectSpecimen = " <A href=\"#\" onClick=processAjaxCall('"+sdao.getSpecimenPks(pkStorage)+"'); onmouseout=return nd();>"+LC.L_View_Specimens/*View Specimens*****/+"</A>";
		         }
				 else
				 {
				 selectSpecimen = " <A href=\"#\" onClick=goToSpecimenDetails("+sdao.getSpecimenPks(pkStorage)+")>"+LC.L_Sp/*SP*****/+"</A>";
				 }
		         if (sdao.getSpecimenPatientFks(pkStorage) != null) {
		             if (sdao.getSpecimenPatientFks(pkStorage).contains(",")) {
		                 //selectPatient = "&nbsp;<A href=\"#\" onClick=goToSpecimenBrowser("+pkStorage+")>SP</A>";
		             } else {
		                 selectPatient = "&nbsp;<A href=\"#\" onClick=goToPatientDetails("+sdao.getSpecimenPatientFks(pkStorage)+")>"+LC.L_P/*P*****/+"</A>";
		             }
		         } else {
		             selectPatient = "";
		         }
		     } else {
		         selectSpecimen = "&nbsp;&nbsp;&nbsp;";
		         selectPatient = "";
		     }
		 } else //for Specimen
		 {

		     specAllocation = EJBUtil.stringToNum((String)specAllocationCount.get(k));
			
		     if ("Y".equals(spSrchLocFlag) || ("Available".equals(codeSubType) && specAllocation == 0) || ("PartOccupied".equals(codeSubType) && specAllocation>=1))
		     {
		         selectLocationScript = "\"selectStorage4Specimen('"+storageName +"', "+pkStorage +")\"";
		         selectLocationStr = "<A href=\"#\" onClick="+selectLocationScript+">"+LC.L_Select/*Select*****/+"</A> ";
		     }
		     else
		     {
		         selectLocationStr = selectLocationScript = "";
		     }
		 }

    	  /////////////

    	if(dispType.equals("G"))
    	{
	    	//create GRID!!!

	    	if (oldIntLevel != intLevel)
	    	{
	    	    // Level has changed; if the last level was 2D, close the table tag
	    	    if (tableIs2D) {
	    	    %>
                    </td></tr></table>
	    	    <%
	    	    }

	    	    // Calculate cell widths
	    	    String tmpLevel = "" + intLevel;
	    	    Integer tmpInteger = (Integer)countPerLevel.get(tmpLevel);
	    	    String tmpRows = (String)rowsPerLevel.get(tmpLevel);
	    	    int numRows = 1;
	    	    if (tmpRows == null) {
	    	        numRows = 1;
	    	    } else {
	    	        try {
	    	            numRows = Integer.parseInt(tmpRows);
	    	        } catch(Exception e) {
	    	            numRows = 1;
	    	        }
	    	    }
		    	int numPerLevel = tmpInteger == null ? 1 : tmpInteger.intValue();
		    	if (numRows > 1) {
		    	    numPerLevel /= numRows;
		    	    tableIs2D = true;
		    	} else {
		    	    tableIs2D = false;
		    	}
	    	    numCellRemaining = numPerLevel;
	    	    int borderWidth = vBorderW * (numPerLevel+2); // (num+1) is # cells (incl. label); add 1 to get # v-border
	    	    int totalWidthTmp = tableIs2D ? totalWidth2D : totalWidth;
	    	    int labelWidthTmp = tableIs2D ? labelWidth2D : labelWidth;
	    	    double tmpDouble = (double)(totalWidthTmp - labelWidthTmp - borderWidth);
	    	    cellWidth = (int)Math.round( tmpDouble / ((double)numPerLevel) );
	    	    cellWidthRemaining = totalWidthTmp - labelWidthTmp - borderWidth;
	        	%>
	        	<%
		    	numLevelsDisplayed++;
	    	    if (numLevelsDisplayed > 1) {
	    		%>
                </tr></table>
                <%
	    	    }
                %>
                <span id="span_storagechild<%=intLevel%>" style="float:left;">
                <%
                // if this table is 2D, add a grid label (which is also a table);
                // also make it a table within a table
                if (numRows > 1) {
                %>
                <table class="<%=tabclassname%>"><TH><%= parentStorageName.get(k)+" Grid View"%></TH></table>
                <table class="<%=tabclassname%>" >
                <tr><td>
                    <table class="<%=tabclassname%>" border = "1">
                    <tr class="browserOddRow">
	    			<td class=tdDefault width=<%=labelWidth2D%>>
                    <% if (! StringUtil.isEmpty(typeImagePath)) { %>
    				    <img src="<%=typeImagePath%>" width=<%=imgWidth%> height=<%=imgHeight%>></img>
    				<% } %>
	    			<%=subtypesPerLevel.get(Integer.toString(intLevel)) %>
	    			</td>
                    <%
                       for(int iX=1; iX<numPerLevel+1; iX++) {
                    %>
                        <td align="center"><%=convertIntToAlphaLabel(iX)%></td>
                    <%
                        }
                    %>
                    </tr>
                    <tr class="browserEvenRow">
	    			<td class=tdDefault align="center" width=<%=labelWidth2D%>><%=cY %></td>
                <%
                } else { // Not 2D => add type description label
                %>
                <table class="<%=tabclassname%>" border = "1">
                    <tr>
	    			<td class=tdDefault width=<%=labelWidth%>>
                    <table><tr>
                    <td class=tdDefault>
    				<% if (! StringUtil.isEmpty(typeImagePath)) { %>
    						<img src="<%=typeImagePath%>" width=<%=imgWidth%> height=<%=imgHeight%>></img>
    				<% } %>
                    </td><td class=tdDefault>
	    			<%=subtypesPerLevel.get(Integer.toString(intLevel))%>
                    </td></tr></table>
	    			</td>
             <% } %>

	    		<%
	    	}
	    	// IH for 3906 - Fixed col vs. row problem in Grid; swap X and Y
	    	else if (oldCY != cY)
	    	{
	    	    String trClassLabel = (cY%2)==0 ? "browserOddRow" : "browserEvenRow";
	    		%>
	    			<tr class=<%=trClassLabel %>>
	    			<td class=tdDefault align="center" width=<%=labelWidth2D%>><%=cY%></td>
	    		<%
	    	}
	    	%>

                        <%
                        if (numCellRemaining == 1) {
                            cellWidth = cellWidthRemaining;
                        } else {
                            cellWidthRemaining -= cellWidth;
                        }
                        numCellRemaining--;
                        String styleBG = "style=\"background-color:"+tdColors.get(k)+"\"";
        	    	    String storageSubtype = (String)typeCodeDescs.get(typeCodepks.indexOf(storageType));
                        %>
	    				<td id="td_level<%=intLevel+"_col"+cX+"_row"+cY%>" <%=styleBG%> class=tableTd width=<%=cellWidth%>
                        onmouseover="return callOverlib('<%=storageIds.get(k)%>','<%=storageNames.get(k)%>',
                            '<%=storageSubtype%>','<%=statusDesc%>');"
                        onmouseout="return nd();"
                        <% if (childAllocation > 0 && "1".equals(browserType)) { %>
                        onClick="callAjaxGetStorageChildren(<%=pkStorage%>,'<%=storageName%>','<%=gridFor%>',<%=parent%>,<%=pkTopStorage%>,
                            '<%=topStorageName%>',<%=intLevel%>,<%=cX%>,<%=cY%>,<%=explorer%>);"
                        <% } %>>
	    				<% if (childAllocation > 0) {
							 availImagePath = null;
                           if ("Occupied".equals(codeSubType)) availImagePath = "images/occupied.jpg";
                           else if ("PartOccupied".equals(codeSubType)) availImagePath = "images/part_occ.jpg";
                           else availImagePath = "images/available.jpg";
						%>
	    				<A href="#" onClick="callAjaxGetStorageChildren(<%=pkStorage%>,'<%=storageName%>','<%=gridFor%>',<%=parent%>,<%=pkTopStorage%>,
                            '<%=topStorageName%>',<%=intLevel%>,<%=cX%>,<%=cY%>,<%=explorer%>)"><%if(tableIs2D)out.print(convertIntToAlphaLabel(cX)+cY);else out.print(cX);%>
	    				</A>
	    				<BR>
                        <font size=1>
	    			  <input type=image name="availabilityIcon" id="availabilityIcon" onClick="<%=selectLocationScript.replaceAll("^\"", "").replaceAll("\"$", "")%>;return false"
                            src="<%=availImagePath%>"/>
							
                        </font>
                        <% } else if (tableIs2D) {
                           availImagePath = null;
                           if ("Occupied".equals(codeSubType)) availImagePath = "images/occupied.jpg";
                           else if ("PartOccupied".equals(codeSubType)) availImagePath = "images/part_occ.jpg";
                           else availImagePath = "images/available.jpg";
                        %>
                        <input type=image name="availabilityIcon" id="availabilityIcon"
                            onClick="<%=selectLocationScript.replaceAll("^\"", "").replaceAll("\"$", "")%>;return false"
                            src="<%=availImagePath%>"/>
	    				<% } else // No child and not 2D
	    				 {
						 availImagePath = null;
                           if ("Occupied".equals(codeSubType)) availImagePath = "images/occupied.jpg";
                           else if ("PartOccupied".equals(codeSubType)) availImagePath = "images/part_occ.jpg";
                           else availImagePath = "images/available.jpg";	
						
						 %>
						 <%=cX%>
						 <br>
						 <input type=image name="availabilityIcon" id="availabilityIcon"
                            onClick="<%=selectLocationScript.replaceAll("^\"", "").replaceAll("\"$", "")%>;return false"
                            src="<%=availImagePath%>"/>
	    				 	
	    				
	    				 <%
	    				 }%>
						 <%
						 specAllocation = EJBUtil.stringToNum((String)specAllocationCount.get(k));
						 if("PartOccupied".equals(codeSubType) || "Occupied".equals(codeSubType) && specAllocation>1){%>
                        <font size=1>
	    			 	<%=selectSpecimen%>
                        </font>
						<%}
						else{%>
						<font size=1>
	    			 	<%=selectSpecimen%><%=selectPatient%>
                        </font>
						<%}%>
						
	    				</td>
	    	<%

	    	oldCY = cY;
	    	oldIntLevel = intLevel;
    	}
    	else //browser mode
    	{
    	    String explorerParameter = "null";
    	    if (!"0".equals(pkStorageForExplorer)) {
    	        explorer = "1";
    	        explorerParameter = "'1'";
    	    }
    		%>
    				</tr>

					   <%if ((k%2)!=0) {
					    %>

					      <tr class="browserEvenRow">

					   <%

					   }
					   else {

					   %>

					      <tr class="browserOddRow">

					 <%
					    }
					 %>

    				<td onClick="callAjaxGetStorageChildren(<%=pkStorage%>,'<%=storageName%>','<%=gridFor%>',<%=parent%>,<%=pkStorage%>,'<%=storageName%>',null,null,null,<%=explorer%>)">
    				<% if (childAllocation > 0) {  %>
    				 <A href="#" onClick="callAjaxGetStorageChildren(<%=pkStorage%>,'<%=storageName%>','<%=gridFor%>',<%=parent%>,<%=pkStorage%>,'<%=storageName%>',null,null,null,<%=explorer%>)"><%=storageName%></A>
    				 <% } else {
    				 %>
    				 	<%=storageName%>
    				 <%
    				  }
    				 %>
    				 <BR><%=selectLocationStr %>
    				 </td>


    				<td> <%=storageId%></td>
    		<%
    	}
    }
	%>
    <%
    if (tableIs2D) {
    %>
        </td></tr></table>
    <%
    }
    %>
	    </tr>
	    </table>
<%
for(int iX=0; iX < numLevelsDisplayed; iX++) {
    %>
</span>
    <%
}
    %>
	<%
}//session
%>
