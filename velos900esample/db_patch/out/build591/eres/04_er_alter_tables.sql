 ------------------------Alter er_add---------------------------------------------------------


alter table eres.er_add add address2 VARCHAR2(50 BYTE);

COMMENT ON COLUMN "er_add"."address2" IS 'Stores Address line 2, follows column Address';

--------------------------------------------------------------------------------------------

INSERT INTO eres.track_patches
VALUES(eres.seq_track_patches.nextval,134,4,'04_er_alter_tables.sql',sysdate,'9.0.0 Build#591');

commit;
