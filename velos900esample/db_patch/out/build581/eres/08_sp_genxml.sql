SET DEFINE OFF;

create or replace
PROCEDURE      "SP_GENXML" (
   P_REPID          NUMBER,
   P_ACCID          NUMBER,
   P_PARAMS         VARCHAR,
   P_SXML     OUT   CLOB,
   P_SXML1    OUT   CLOB,
   P_SXML2    OUT   CLOB,
   P_SXML3    OUT   CLOB,
   P_SXML4    OUT   CLOB,
   P_SXML5    OUT   CLOB,
   P_HDR      OUT   CLOB,
   P_FTR      OUT   CLOB
)
IS
 /***************************************************************************************************
   **
   ** Author: Charanjiv S Kalha 05/30/2001
   ** This procedure generates the XML from SQL's for the reports module. The SQLs are stored
   ** in the ER_REPORT.REP_SQL column. The PK_REP is passed as a parameter (p_repid), the parameters
   ** to the SQL (stored as ~1, ~2 and so on) are passed in colon (:) delimited string p_params.
   ** The procedure parses the string p_params and replaces the parameters in the SQL with the values.
   ** The XML is created with xmlgen.getXML procedure. and passed out in the p_sxml parameter.
   ** The other parameters p_sxml1..5 and the hdr/ftr variables are for futurer use.
   **
   ** Modification History
   **
   ** Modified By         Date         Remarks
   ** Charanjiv           13th July         For the report 21 a Global Temp table had to be created cos of the
   **                                       complex nature of the report.
   ** Charanjiv           07th Sept         Added the Currancy of the cost value in the SQL
   ** Charanjiv           12th Sept         Added report 44 for Protocol Calender Template
   ** Charanjiv           24th Sept    Appended the where caluse for displacement > 0 in rep 21
   ** Sonika Talwar       15th May     Changed the parameter separator from comma(,) to colon(:)
   **************************************************************************************************
   ** Known Bug List
   **************************************************************************************************
   ** Error handling      if the Report sql does not return any rows the procedure still sends a XML
   **                     back with just a header row in it.
   **
   */
   V_STR      VARCHAR2 (2000)DEFAULT P_PARAMS || ':';
   V_SQLSTR   VARCHAR2 (4000);
   V_PARAMS   VARCHAR2 (2000)DEFAULT P_PARAMS || ':';
   V_POS      NUMBER         := 0;
   V_CNT      NUMBER         := 0;
   V_SQLXML   CLOB;
   V_INS      VARCHAR2 (4000);
   V_CID      NUMBER;
   V_CID2      NUMBER;

   V_GENXML NUMBER;

BEGIN
/*
Resets any XML options set before
*/
   Xmlgen.RESETOPTIONS;
   V_CID := SUBSTR (V_STR, 1, INSTR (V_STR, ':') - 1);
   v_cid2 := v_cid ;
   -- For RepId 21va global temp table is populated and then the SQL executed on the temp table.
   IF P_REPID = 21 THEN
   DELETE FROM ER_TMPREP;
   V_INS :=
'insert into er_tmprep (
     	EVENT_ID               , --1
	CHAIN_ID               , --2
	EVENT_TYPE             , -- 3
	NAME                   , --4
	NOTES                  , --5
	COST                   , --6
	COST_DESCRIPTION       , --7
	DURATION               , --8
	USER_ID                , --9
	LINKED_URI             , --10
	FUZZY_PERIOD           , --11
	MSG_TO                 , --12
	STATUS                 , -- 13
	DESCRIPTION            , --14
	DISPLACEMENT           , --15
	ORG_ID                 , --16
	EVENT_MSG              , --17
	EVENT_RES              , --18
	CREATED_ON             , --19
	EVENT_FLAG             , --20
	EVENTCOST_DESC         , --21
	EVENTCOST_CURR         , --22
	EVE_VALUE          , --23
	EVENTCOST_VALUE        , --24
	FK_EVENT               , --25
	SUM_EVENT              )   --26
      (
       SELECT
	a.EVENT_ID               ,
        a.CHAIN_ID               ,
        a.EVENT_TYPE             ,
        a.NAME                   ,
        a.NOTES                  , --5
        a.COST                   ,
        a.COST_DESCRIPTION       ,
        a.DURATION               ,
        a.USER_ID                ,
        a.LINKED_URI             , --10
        a.FUZZY_PERIOD           ,
        a.MSG_TO                 ,
        a.STATUS                 ,
        a.DESCRIPTION            ,
        a.DISPLACEMENT           , --15
        a.ORG_ID                 ,
        a.EVENT_MSG              ,
        a.EVENT_RES              ,
        a.CREATED_ON             ,
        a.EVENT_FLAG             , --20
        b.EVENTCOST_DESC ,
	b.EVENTCOST_CURR ,
	b.EVENTCOST_VALUE  EVE_VALUE         , --23
        trim(b.EVENTCOST_CURR ||'' '' ||  b.EVENTCOST_VALUE) EVENTCOST_VALUE , --24
        b.FK_EVENT , --25
        (SELECT      MIN(c.eventcost_curr) || '' '' || SUM(c.EVENTCOST_VALUE)
           FROM erv_evecost c
          WHERE c.fk_event = a.event_id   ) sum_event --25
  FROM erv_eveassoc a , erv_evecost b
 WHERE a.event_id = b.fk_event
   AND a.chain_id =  :1
   AND a.DISPLACEMENT > 0  )';
 EXECUTE IMMEDIATE V_INS
   USING V_CID;
 -- If the report is the protocol calender template
 ELSIF p_repid = 44 THEN
  v_ins := 'insert into er_tmprep
 ( event_id , chain_id , displacement , NAME , duration , fuzzy_period  )
   SELECT a.event_id ,
	  a.chain_id ,
	  a.displacement ,
	  a.NAME ,
	  a.duration ,
	  a.FUZZY_PERIOD
     FROM erv_eveassoc a
    WHERE a.chain_id = :1 '  ;
-- DBMS_OUTPUT.PUT_LINE (substr(v_ins,-200)) ;
 EXECUTE IMMEDIATE V_INS
  USING v_cid ;
  END IF;
   -- Get the Report SQL from the ER_REPORT Table for the preport id p_repid
   SELECT REP_SQL,NVL(GENERATE_XML,1)
     INTO V_SQLSTR,V_GENXML
     FROM ER_REPORT
    WHERE PK_REPORT = P_REPID;
   -- Parse the parameter string passed and separate the comma delimited paramters.
   -- Replace the variables in the SQL with the parameters
   LOOP
      EXIT WHEN V_PARAMS IS NULL;
      V_CNT := V_CNT + 1;
      V_POS := INSTR (V_STR, ':');
      V_PARAMS := SUBSTR (V_STR, 1, V_POS - 1);
      V_SQLSTR := REPLACE (V_SQLSTR, '~' || V_CNT, V_PARAMS);
      V_STR := SUBSTR (V_STR, V_POS + 1);
 	  DBMS_OUTPUT.PUT_LINE (SUBSTR(v_sqlstr,1,200)) ;
   	  DBMS_OUTPUT.PUT_LINE (SUBSTR(v_sqlstr,-200)) ;
   END LOOP;
   --DBMS_OUTPUT.PUT_LINE (substr(v_sqlstr,1,255)) ;
   -- DBMS_OUTPUT.PUT_LINE (substr(v_sqlstr,245,255)) ;
   -- Execute the SQL created, pass it as a variable in the XMLGEN.getXML procedure
   -- This will return the XML for the SQL. Even if the SQL returns no rows the XML will be created
   -- INSERT INTO T (c) VALUES ('v_sqlstr'||p_repid||'  -  '||v_sqlstr);
			 

     IF V_GENXML = 1 THEN
	    P_SXML := F_GETXML (V_SQLSTR);
	ELSE

		EXECUTE IMMEDIATE V_SQLSTR INTO P_SXML;
		
	END IF;
     DBMS_OUTPUT.PUT_LINE ('XML ' || DBMS_LOB.SUBSTR (P_SXML, 200, 200));


END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,124,8,'08_sp_genxml.sql',sysdate,'8.10.0 Build#581');

commit;