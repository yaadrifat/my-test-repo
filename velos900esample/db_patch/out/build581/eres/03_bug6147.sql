set define off;

update er_browserconf set browserconf_seq = browserconf_seq+1 where fk_browser =
(select pk_browser from er_browser where BROWSER_MODULE='studyPatient' and BROWSER_NAME ='Enrolled Patient Browser');

commit;


DELETE FROM ER_REPXSL WHERE FK_REPORT in (110);

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,124,3,'03_bug6147.sql',sysdate,'8.10.0 Build#581');

commit;