set define off;
--------------------------------------------------------Themes------------------------------------------------------------------

insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','redmond','redmond','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','base','base','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','black-tie','black-tie','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','blitzer','blitzer','N',4,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','cupertino','cupertino','N',5,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','dark-hive','dark-hive','N',6,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','dot-luv','dot-luv','N',7,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','eggplant','eggplant','N',8,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','excite-bike','excite-bike','N',9,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','hot-sneaks','hot-sneaks','N',10,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','humanity','humanity','N',11,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','le-frog','le-frog','N',12,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','mint-choc','mint-choc','N',13,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','overcast','overcast','N',14,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','pepper-grinder','pepper-grinder','N',15,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','smoothness','smoothness','N',16,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','south-street','south-street','N',17,'Y',null,
	null,null,sysdate,sysdate,'N',null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','start','start','N',18,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','sunny','sunny','N',19,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','swanky-purse','swanky-purse','N',20,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','trontastic','trontastic','N',21,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','ui-darkness','ui-darkness','N',22,'N',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','ui-lightness','ui-lightness','N',23,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'themes','vader','vader','N',24,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
---------------------------------------------------------Themes End Here ----------------------------------

---------------------------------------------Bacterial Culture--------------------------------------------

insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'bact_cul','posi','Positive','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'bact_cul','negi','Negative','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'bact_cul','not_done','Not Done','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
--------------------------------------------- Bacterial Culture End Here --------------------------------------------

----------------------------------------------Fungal Culture---------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'fung_cul','posi','Positive','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'fung_cul','negi','Negative','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'fung_cul','not_done','Not Done','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

----------------------------------------------Fungal Culture Ends Here---------------------------------------------

----------------------------------------------HLA---------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hla_locus','hla_a','HLA-A','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hla_locus','hla_b','HLA-B','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hla_locus','hla_c','HLA-C','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hla_locus','hla_drb1','HLA-DRB1','N',4,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hla_locus','hla_drb2','HLA-DRB2','N',5,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hla_locus','hla_drb3','HLA-DRB3','N',6,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hla_locus','hla_drb4','HLA-DRB4','N',7,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hla_locus','hla_drb5','HLA-DRB5','N',8,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hla_locus','hla_dqb1','HLA-DQB1','N',9,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hla_locus','hla_dpb1','HLA-DPB1','N',10,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
		insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hla_locus','hla_dqa1','HLA-DQA1','N',11,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hla_locus','hla_dpa1','HLA-DPA1','N',12,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

----------------------------------------------HLA Ends Here---------------------------------------------

----------------------------------------------Clicnical Status---------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'clini_stat','accept','Temporary Unavailable','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'clini_stat','defer','Medically Deferred','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Clicnical Status Ends Here---------------------------------------------

---------------------------------------------Rh Type---------------------------------------------	
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'rh_type','posi','Positive','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'rh_type','negi','Negative','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Rh Type Ends Here---------------------------------------------

---------------------------------------------HLA Method---------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hla_meth','molecular','Molecular','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------HLA Method Ends Here---------------------------------------------
	
---------------------------------------------Cord Viab Method---------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'viab_meth','7-AAD','7-AAD','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'viab_meth','acri_org','Acridine Orange','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'viab_meth','ethi_bromi','Ethidinum Bromide','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'viab_meth','try_blue','Trypan Blue','N',4,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'viab_meth','other','Other','N',5,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Cord Viab Method Ends Here---------------------------------------------
	
	
---------------------------------------------Cord CFU Post Method---------------------------------------------

	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'cfu_post_meth','gm','GM','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'cfu_post_meth','gemm','GEMM','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Cord CFU Post Method Ends Here---------------------------------------------

---------------------------------------------HEMOGLOBINOPATHy SCREENING---------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hem_path_scrn','negi','Negative','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hem_path_scrn','homozygous','Homozygous','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hem_path_scrn','hetrozygous �','Hetrozygous �','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'hem_path_scrn','not_done','Not Done','N',4,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

-------------------------------------------HEMOGLOBINOPATHy SCREENING Ends Here---------------------------------
	
---------------------------------------------Process Method---------------------------------------------

	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'pro_meth','automated','Automated','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'pro_meth','manual','Manual','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'pro_meth','none','None','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Process Method Ends Here---------------------------------------------

---------------------------------------------Bag Type-----------------------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'bag_type','sing_frac','Single Fraction','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'bag_type','80/20','80/20','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'bag_type','60/40','60/40','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'bag_type','50/50','50/50','N',4,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'bag_type','2_bags','2 Bags','N',5,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);


---------------------------------------------Bag Type Ends Here -----------------------------------------------------------
	
	
---------------------------------------------Product Modification-----------------------------------------------------------

	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'prod_modi','red_cell_redu','Red Cell Reduction','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'prod_modi','plasma_redu','Plasma Reduction','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'prod_modi','both','Both','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'prod_modi','none','None','N',4,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'prod_modi','other','Other','N',5,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Product Modification Ends Here--------------------------------------------

	
---------------------------------------------Shipment Segment--------------------------------------------

	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ship_seg','0','0','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ship_seg','1','1','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ship_seg','2','2','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ship_seg','>2','>2','N',4,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Shipment Segment Ends Here--------------------------------------------
	
---------------------------------------------Entity Type--------------------------------------------

	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'entity_type','CBU','CBU','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'entity_type','CBB','CBB','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Entity Type Ends Here --------------------------------------------
	
---------------------------------------------Attachment Type--------------------------------------------	
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'attach_type','unit_rprt','Unit Report','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'attach_type','test_rprt','Test Report','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Attachment Type Ends Here--------------------------------------------

---------------------------------------------Order Type--------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'order_type','lab_ordr','Lab Order','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'order_type','med_ordr','Medical Order','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'order_type','non_clic_ordr','Non Clinical Order','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Order Type Ends Here--------------------------------------------

---------------------------------------------Order Status--------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'order_status','open_ordr','Open Orders','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'order_status','close_ordr','Close Orders','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);


---------------------------------------------Order Status Ends Here--------------------------------------------
	

---------------------------------------------Eligibility--------------------------------------------

	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,' eligibility',' eligible',' Eligible','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,' eligibility','ineligible','Ineligible','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Eligibility Ends Here--------------------------------------------

	
---------------------------------------------Ineligibile Reason--------------------------------------------

	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ineligible','cjd_risk','Health History- CJD Risk','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ineligible','HIV/HEP/HTL','Health History- HIV/Hepatitis/HTLV related risk','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ineligible','small_vacc','Small Pox/ Vaccine/ West Nile/ Chagas related risk ','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ineligible','othr_med_rec','Other Medical Records','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ineligible','phy_find','Physical Findings','N',4,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ineligible','idm_res','Reactive / Positive IDM results','N',5,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ineligible','idm_tst','"IDM test performed  
at non-CLIA/CMS lab, not using FDA kits and/ or expired"','N',6,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Ineligibile Reason Ends Here --------------------------------------------
	

---------------------------------------------Unlicensed Reason--------------------------------------------

	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'unlicensed','international','International','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'unlicensed','u.s_prior','U.S. prior to May 25, 2005','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'unlicensed','u.s_inel','U.S. ineligible-donor screening','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'unlicensed','u.s_inel_test','U.S. ineligible-donor testing and screening','N',4,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'unlicensed','pre_lice','U.S. eligible-pre-licensure','N',5,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'unlicensed','post_lice','U.S. eligible-post-licensure','N',6,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Unlicensed Reason Ends Here----------------------------------

---------------------------------------------Address------------------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'add_type','business','Business','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'add_type','mailing','Mailing','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'add_type','residence','Residence','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Address Ends Here--------------------------------------------

---------------------------------------------Clinical Category--------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'clinical_cat','laboratory','Laboratory','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'clinical_cat','medical','Medical','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Clinical Category Ends Here--------------------------------------------

---------------------------------------------Active--------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'active','act','Active','N',null,'Y',1,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'active','not_act','Not Active','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Active Ends Here--------------------------------------------

---------------------------------------------Licence Status--------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'licence','licensed','Licensed','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'licence','unlicensed','UnLicensed','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Licence Status Ends Here--------------------------------------------

---------------------------------------------Data Entry --------------------------------------------
	
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'Data Entry','user_type','Data Entry Specialist ','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'Data Entry','task_type','Data Entry','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);


---------------------------------------------Data Entry Ends Here--------------------------------------------


---------------------------------------------Application Version--------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'appl_ver','subappver','Ver 1.0.0 Build#104','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Application Version Ends Here-----------------------------------

---------------------------------------------IND Sponser--------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ind_spon','NMDP','NMDP','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ind_spon','other','Other','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------IND Sponser Ends Here--------------------------------------------

---------------------------------------------IND Sposnser Name--------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ind_spon_name','NMDP02','NMDP02','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ind_spon_name','NMDP01','NMDP01','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ind_spon_name','NMDP03','NMDP03','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------IND Sponser Name Ends Here----------------------------------------
	
---------------------------------------------IND Sponser Number--------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ind_spon_number','01NMDP_NUM','01NMDP_NUM','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ind_spon_number','02NMDP_NUM','02NMDP_NUM','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ind_spon_number','03NMDP_NUM','03NMDP_NUM','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------IND Sponser Number Ends Here--------------------------------------------

---------------------------------------------IND Sponser NMDP--------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ind_spon_nmdp','NMDP','NMDP','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------IND Sponser NMDP Ends Here--------------------------------------------

---------------------------------------------IND Sponser Other--------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'ind_spon_other','other','Other','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------IND Sponser Other Ends Here--------------------------------------------

---------------------------------------------Shipper---------------------------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'shipper','fedex','FedEx','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'shipper','ups','UPS','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'shipper','quick','Quick','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'shipper','world_cour','World Courier','N',4,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'shipper','others','Others','N',5,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);


---------------------------------------------Shipper Ends Here----------------------------------------------


---------------------------------------------Alert---------------------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'alert','new_req','New Request','N',1,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'alert','ass_to_chg','Assigned to Changed','N',2,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'alert','HLA_uploads','HLA Uploads','N',3,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Alert Ends Here---------------------------------------------------------

	
---------------------------------------------Storage Method---------------------------------------------------------
		
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'storage_meth','under_liq_nit','Under Liquid Nitrogen(LN2)','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'storage_meth','vpr_phse',' Vapor phase (LN)','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'storage_meth','ele_freezer','Electric Freezer','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);


---------------------------------------------Storage Method Ends Here--------------------------------------


---------------------------------------------Freezer Manufacturer---------------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'freezer_manu','bioarchive','BioArchive','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'freezer_manu','MVE','MVE','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'freezer_manu','tay_whar','Taylor-Wharton','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'freezer_manu','forma_scienti','Forma Scientific','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'freezer_manu','other','Other','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);


---------------------------------------------Freezer Manufacturer Ends Here----------------------------------------------
	
	
---------------------------------------------Storage Temperature---------------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'storage_temp','<-150','<-150','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'storage_temp','>=-150<=-135','>=-150 to <=-135','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'storage_temp','>-135<=80','>-135 to <=80','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'storage_temp','>-135<=80','>-135 to <=80','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'storage_temp','>-80','>-80','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Storage Temperature Ends Here -------------------------------------------

--------------------------------------------- contr_rate_freez ---------------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'contr_freez','yes','Yes','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'contr_freez','no','No','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

--------------------------------------------- contr_rate_freez Ends Here ---------------------------------------------------

--------------------------------------------- Frozen In ---------------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'frozen_in','vials','Vials','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);


	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'frozen_in','tubes','Tubes','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'frozen_in','bags','Bags','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'frozen_in','other','Other','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

--------------------------------------------- Frozen In Ends Here -------------------------------------------

--------------------------------------------- Processing ---------------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'processing','manual','Manual','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'processing','automated','Automated','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------Processing Ends Here ---------------------------------------------------


---------------------------------------------If Automated ---------------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'if_automated','SEPAX','SEPAX','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'if_automated','AXP','AXP','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'if_automated','other','Other','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

---------------------------------------------If Automated Ends Here ---------------------------------------------------

---------------------------------------------Product Modification---------------------------------------------------
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'product_modi','red_cell_only','Red Cell Depletion Only','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'product_modi','plas_rbc_red','Plasma and RBC Reduced','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'product_modi','buff_coat_prep','Buffy Coat Preparation','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'product_modi','vol_redu_only','Volume Reduction Only','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'product_modi','dens_sep','Density Separation','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'product_modi','none','None','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);
	
	insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'product_modi','other','None','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);


---------------------------------------------Product Modification Ends Here------------------------------------------

---------------------------------------------Specimen Type-----------------------------------------------------------

insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'specimen_type','cbu','CBU','N',null,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

-----------------------------------------Specimen Type Ends Here----------------------------------------------------

Commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,133,2,'02_ER_CodeList_Insert.sql',sysdate,'9.0.0 Build#590');

commit;