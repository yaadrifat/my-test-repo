/* This readMe is specific to eSample (version 9.0.0 build #637-ES009 HotFix #1) */

=====================================================================================================================================
eSample bugs covered in the build:

1) Bug#13336 - In quick edit, it is best, not allow user to leave it blank

Following existing files have been modified:

	1. specimendetails.jsp					(...\deploy\velos.ear\velos.war\jsp)
	2. MC.jsp								(...\deploy\velos.ear\velos.war\jsp)
	3. ajaxValidateSpecimen.jsp				(...\deploy\velos.ear\velos.war\jsp)
	4. MC.class								(...\deploy\velos.ear\velos-common.jar\com\velos\eres\service\util)
	
Property file changes (Do not replace the files):
	1. Copy the KEY(s) given in the file "changes_messageBundle.properties" to the existing file "messageBundle.properties" under the path:  ...\eResearch_jboss510\conf
=====================================================================================================================================