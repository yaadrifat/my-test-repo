Set define off;

DECLARE
  v_column_value_update number := 0;
BEGIN
  Select count(*) into v_column_value_update
    from user_tab_cols
    where TABLE_NAME = 'ER_CTRLTAB'
    AND column_name = 'CTRL_DESC';
  if (v_column_value_update =1) then
   UPDATE ER_CTRLTAB SET CTRL_DESC='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cord Processing Procedure'
		WHERE CTRL_VALUE='CB_CORDPP' AND CTRL_KEY='app_rights' AND CTRL_DESC='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cord Precessing Procedure';
  end if;
end;
/


Set define off;

DECLARE
  v_column_value_update number := 0;
BEGIN
  Select count(*) into v_column_value_update
    from user_tab_cols
    where TABLE_NAME = 'ER_OBJECT_SETTINGS'
    AND column_name = 'OBJECT_SUBTYPE';
  if (v_column_value_update =1) then
   UPDATE ER_OBJECT_SETTINGS SET OBJECT_SUBTYPE='cinfo_menu'
   WHERE OBJECT_NAME='top_menu' AND OBJECT_TYPE='TM' AND OBJECT_SUBTYPE='con_info_menu' AND OBJECT_DISPLAYTEXT='Contact Info';
  end if;
end;
/

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,149,6,'06_ER_OBJECT_SETTING_UPDATES.sql',sysdate,'9.0.0 Build#606');

commit;
