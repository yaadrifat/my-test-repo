/* This readMe is specific to eSample (version 9.0.0 build #637-ES008) */

=====================================================================================================================================
eSample bugs covered in the build:

1) Bug#12713 - Update Multiple Specimen Status page fields are not fit into outer container on "editmultiplespecimenstatus.jsp"
2) Bug#10502 - UI: Add Multiple Storage page fields are not fit into outer container in Firefox browser
3) Bug#12536 - INV-21675:If the storage have multiple specimen, on hover it should display the list of specimens in the storage
4) Bug#12784 - UI issue on "labdata.jsp" on different browser's

Following existing files have been modified:

	1. labdata.jsp							(...\deploy\velos.ear\velos.war\jsp)
	2. storageunitdetails.jsp				(...\deploy\velos.ear\velos.war\jsp)
	3. specimenbrowser.jsp					(...\deploy\velos.ear\velos.war\jsp)
	4. editmultiplespecimenstatus.jsp		(...\deploy\velos.ear\velos.war\jsp)
	5. storageadminbrowser.jsp				(...\deploy\velos.ear\velos.war\jsp)
	6. addmultiplestorage.jsp				(...\deploy\velos.ear\velos.war\jsp)
	7. ajaxGetSpecimenList.jsp				(...\deploy\velos.ear\velos.war\jsp)
	8. getStorageGrid.jsp					(...\deploy\velos.ear\velos.war\jsp)
=====================================================================================================================================