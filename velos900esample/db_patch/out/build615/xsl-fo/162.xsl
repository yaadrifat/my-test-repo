<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
            xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
            xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:key name="cbbCode" match="ROW" use="concat(FK_CBB_ID,' ')"/>
  <xsl:key name="cbbName" match="ROW" use="concat(CBB_NAME,' ')"/>
  <xsl:key name="oneYr" match="ROW" use="concat(ONE_YR,' ')"/>
  <xsl:key name="twoYr" match="ROW" use="concat(TWO_YR,' ')"/>
  <xsl:key name="threeYr" match="ROW" use="concat(THREE_YR,' ')"/>
  <xsl:key name="fourYr" match="ROW" use="concat(FOUR_YR,' ')"/>
  <xsl:key name="fiveYr" match="ROW" use="concat(FIVE_YR,' ')"/>
  <xsl:key name="fiveYr" match="ROW" use="concat(SIX_YR,' ')"/>
  <xsl:key name="sevenYrPlus" match="ROW" use="concat(SEVEN_YR_PLUS,' ')"/>
  <xsl:param name="repName"/>
  <xsl:param name="repBy"/>
  <xsl:param name="repDate"/>
  <xsl:param name="selDate"/>
	  
   <xsl:template match="/">
  <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <fo:layout-master-set>
      <fo:simple-page-master master-name="pdf" page-height="11in" page-width="10in" 
          margin-top="0.5in" margin-bottom="0.5in" margin-left="0.5in" margin-right="0.5in">
	<fo:region-body margin-top="1.0in" margin-bottom="1.0in"/>
	<fo:region-before extent="1.0in"/>
        <fo:region-after extent="0.5in"/>
      </fo:simple-page-master>
    </fo:layout-master-set>
      <xsl:apply-templates select="ROWSET"/>
  </fo:root>
  </xsl:template>
  
  
  <xsl:template match="ROWSET">

  <fo:page-sequence master-reference="pdf" initial-page-number="1" font-family="serif">
  <fo:static-content flow-name="xsl-region-before">
  <fo:block font-size="10pt" font-weight="bold"> Report Date:  <xsl:value-of select="$repDate" /> </fo:block>

  </fo:static-content>
   <fo:static-content flow-name="xsl-region-after">
   <fo:table>
   <fo:table-body>
   <fo:table-row>
    <fo:table-cell>  <fo:block text-align="left" font-size="10pt"> *The age of the CBU is determined based on the CBU birth date. </fo:block></fo:table-cell>	
   </fo:table-row>
   <fo:table-row>
    <fo:table-cell>  <fo:block text-align="left" font-size="10pt"> **Months are calculated based on a 30 day month. </fo:block></fo:table-cell>	
   </fo:table-row>
   <fo:table-row>
	<fo:table-cell>  <fo:block text-align="left" font-size="10pt" font-weight="bold"> NMDP Proprietory and Confidential</fo:block>	</fo:table-cell>
	<fo:table-cell> <fo:block  text-align="left" font-size="10pt" font-weight="bold">Page <fo:page-number/> of  <fo:page-number-citation ref-id="theEnd"/></fo:block>	</fo:table-cell>
   </fo:table-row>
   </fo:table-body>
   </fo:table>
   </fo:static-content>


   <fo:flow flow-name="xsl-region-body">
	<fo:block font-family="Helvetica" text-align="center" font-size="13pt" font-weight="bold">
	National Marrow Donor Program 
	</fo:block>
	
	<fo:block font-family="Helvetica" text-align="center" font-size="11pt" font-weight="bold"  space-after.optimum="12pt">
	Total Member Cord Blood Bank Inventory by Age of CBU* (NCBI and Non-NCBI)
	</fo:block>

	
	<fo:block font-family="Helvetica" text-align="center" font-size="11pt" font-weight="bold"  space-after.optimum="25pt">
	As of <xsl:value-of select="$selDate" />
	</fo:block>
	
	<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
	</fo:block>

	<fo:block font-family="Helvetica" text-align="center" font-size="14pt">
	</fo:block>

	<fo:block font-family="Helvetica" text-align="right" font-size="10pt" font-weight="bold">
	Age of CBU in Months**
	</fo:block>

	<fo:table width="640pt" table-layout="fixed"  border-width="0.4mm" border-style="solid">
		<fo:table-column column-width="60pt" column-number="1"/>
		<fo:table-column column-width="300pt" column-number="2"/>
		<fo:table-column column-width="40pt" column-number="3"/>
		<fo:table-column column-width="40pt" column-number="4"/>
		<fo:table-column column-width="40pt" column-number="5"/>
		<fo:table-column column-width="40pt" column-number="6"/>
		<fo:table-column column-width="40pt" column-number="7"/>
		<fo:table-column column-width="40pt" column-number="8"/>
		<fo:table-column column-width="40pt" column-number="9"/>
		<fo:table-column column-width="40pt" column-number="10"/>

	 <fo:table-header>
	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">CBB Code</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">CBB Name</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">0-11</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">12-23</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">24-35</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">36-47</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">48-59</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">60-72</fo:block>
	  </fo:table-cell>
	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">72+</fo:block>
	  </fo:table-cell>

	  <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	    <fo:block font-size="10pt" font-weight="bold" text-align="center">Grand Total</fo:block>
	  </fo:table-cell>

	</fo:table-header> 
	 
	 
	 <fo:table-body start-indent="0pt" text-align="start"> 
	
	
	<xsl:for-each select="ROW[count(. | key('cbbCode', concat(FK_CBB_ID,' '))[1])=1]">
	<fo:table-row>
	
	<fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	<fo:block font-size="10pt" text-align="center" >
	 <xsl:variable name="cbb_Code" select="FK_CBB_ID" />
         <xsl:value-of select="$cbb_Code"/>
        </fo:block>
        </fo:table-cell>

	<fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
	      <fo:block font-size="10pt" text-align="left">
                   <xsl:variable name="cbb_name" select="CBB_NAME" />
                   <xsl:value-of select="$cbb_name"/>
		 </fo:block>
        </fo:table-cell> 	

	 <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:variable name="one_yr" select="ONE_YR" />
                   <xsl:value-of select="$one_yr"/>
	       </fo:block>
         </fo:table-cell>
	
	<fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:variable name="two_yr" select="TWO_YR" />
                   <xsl:value-of select="$two_yr"/>
               </fo:block>
         </fo:table-cell>	
	
	<fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:variable name="three_yr" select="THREE_YR" />
                   <xsl:value-of select="$three_yr"/>
              </fo:block>
            </fo:table-cell>
	    <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:variable name="four_yr" select="FOUR_YR" />
                   <xsl:value-of select="$four_yr"/>
               </fo:block>
            </fo:table-cell>
	    <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:variable name="five_yr" select="FIVE_YR" />
                   <xsl:value-of select="$five_yr"/>
              </fo:block>
            </fo:table-cell>
	    <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:variable name="six_yr" select="SIX_YR" />
                   <xsl:value-of select="$six_yr"/>
               </fo:block>
            </fo:table-cell> 
	    <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:variable name="seven_yr_plus" select="SEVEN_YR_PLUS" />
                   <xsl:value-of select="$seven_yr_plus"/>
               </fo:block>
            </fo:table-cell> 

	    <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
		    <xsl:variable name="one_yr" select="ONE_YR" /> <xsl:variable name="two_yr" select="TWO_YR" />
		    <xsl:variable name="three_yr" select="THREE_YR" /><xsl:variable name="four_yr" select="FOUR_YR" />
		    <xsl:variable name="five_yr" select="FIVE_YR" /><xsl:variable name="six_yr" select="SIX_YR" />
		    <xsl:variable name="seven_yr_plus" select="SEVEN_YR_PLUS" /> <xsl:value-of select="$one_yr + $two_yr +$three_yr + $four_yr + $five_yr + $six_yr + $seven_yr_plus"/>
               </fo:block>
            </fo:table-cell>

        </fo:table-row>
	</xsl:for-each>
	
	<fo:table-row>
	<fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
		<fo:block font-family="Helvetica" text-align="right" font-size="12pt">

		</fo:block>
        </fo:table-cell>
	<fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
		<fo:block text-align="right" font-size="10pt" font-weight="bold" >
		Grand Total
		</fo:block>
        </fo:table-cell>
	<fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//ONE_YR)"/>
	       </fo:block>
         </fo:table-cell>
	<fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//TWO_YR)"/>
	       </fo:block>
         </fo:table-cell>
	 <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//THREE_YR)"/>
	       </fo:block>
         </fo:table-cell>
	 <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//FOUR_YR)"/>
	       </fo:block>
         </fo:table-cell>
	 <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//FIVE_YR)"/>
	       </fo:block>
         </fo:table-cell>
	 <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//SIX_YR)"/>
	       </fo:block>
         </fo:table-cell>
	 <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//SEVEN_YR_PLUS)"/>
	       </fo:block>
         </fo:table-cell>

	 <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" font-weight="bold" text-align="center">
                   <xsl:value-of select="sum(//ONE_YR) + sum(//TWO_YR) + sum(//THREE_YR) + sum(//FOUR_YR) + sum(//FIVE_YR) + sum(//SIX_YR) + sum(//SEVEN_YR_PLUS)"/>
	       </fo:block>
         </fo:table-cell>
	</fo:table-row>
	<fo:table-row>
	<fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
		<fo:block font-family="Helvetica" text-align="right" font-size="12pt">
		</fo:block>
        </fo:table-cell>
	<fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
		<fo:block text-align="right" font-size="10pt" font-weight="bold" >
		Percentage of Total Cords
		</fo:block>
        </fo:table-cell>
	 <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:value-of select="format-number((sum(//ONE_YR) div (sum(//ONE_YR) + sum(//TWO_YR) + sum(//THREE_YR) + sum(//FOUR_YR) + sum(//FIVE_YR) + sum(//SIX_YR) + sum(//SEVEN_YR_PLUS))*100),'##.#') "/> %
	       </fo:block>
         </fo:table-cell>
	<fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:value-of select="format-number((sum(//TWO_YR) div (sum(//ONE_YR) + sum(//TWO_YR) + sum(//THREE_YR) + sum(//FOUR_YR) + sum(//FIVE_YR) + sum(//SIX_YR) + sum(//SEVEN_YR_PLUS))*100),'##.#') "/> %
	       </fo:block>
         </fo:table-cell>
	 <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:value-of select="format-number((sum(//THREE_YR) div (sum(//ONE_YR) + sum(//TWO_YR) + sum(//THREE_YR) + sum(//FOUR_YR) + sum(//FIVE_YR) + sum(//SIX_YR) + sum(//SEVEN_YR_PLUS))*100),'##.#') "/> %
	       </fo:block>
         </fo:table-cell>
	 <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:value-of select="format-number((sum(//FOUR_YR) div (sum(//ONE_YR) + sum(//TWO_YR) + sum(//THREE_YR) + sum(//FOUR_YR) + sum(//FIVE_YR) + sum(//SIX_YR) + sum(//SEVEN_YR_PLUS))*100),'##.#') "/> %
	       </fo:block>
         </fo:table-cell>
	 <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:value-of select="format-number((sum(//FIVE_YR) div (sum(//ONE_YR) + sum(//TWO_YR) + sum(//THREE_YR) + sum(//FOUR_YR) + sum(//FIVE_YR) + sum(//SIX_YR) + sum(//SEVEN_YR_PLUS))*100),'##.#') "/> %
	       </fo:block>
         </fo:table-cell>
	 <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:value-of select="format-number((sum(//SIX_YR) div (sum(//ONE_YR) + sum(//TWO_YR) + sum(//THREE_YR) + sum(//FOUR_YR) + sum(//FIVE_YR) + sum(//SIX_YR) + sum(//SEVEN_YR_PLUS))*100),'##.#') "/> %
	       </fo:block>
         </fo:table-cell>
	 <fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block font-size="10pt" text-align="center">
                   <xsl:value-of select="format-number((sum(//SEVEN_YR_PLUS) div (sum(//ONE_YR) + sum(//TWO_YR) + sum(//THREE_YR) + sum(//FOUR_YR) + sum(//FIVE_YR) + sum(//SIX_YR) + sum(//SEVEN_YR_PLUS))*100),'##.#') "/> %
	       </fo:block>
         </fo:table-cell>
	
	<fo:table-cell padding="0.4mm" border-width="0.4mm" border-style="solid">
              <fo:block>
	       </fo:block>
         </fo:table-cell>
	</fo:table-row>

	 </fo:table-body>
        </fo:table>

	<fo:block id="theEnd"/>	

   </fo:flow>
 </fo:page-sequence>  
</xsl:template>
</xsl:stylesheet>
	  
	 