CREATE OR REPLACE TRIGGER "ERES".CB_BEST_HLA_AU0 AFTER
  UPDATE OF PK_BEST_HLA,
FK_HLA_CODE_ID,
FK_HLA_METHOD_ID,
HLA_VALUE_TYPE1,
HLA_VALUE_TYPE2,
CREATOR,
CREATED_ON,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
IP_ADD,
DELETEDFLAG,
RID,
ENTITY_ID,
ENTITY_TYPE,
FK_CORD_CDR_CBU_ID,
FK_HLA_OVERCLS_CODE,
HLA_CRIT_OVER_DATE,
HLA_OVER_IND_ANTGN1,
HLA_OVER_IND_ANTGN2,
FK_HLA_PRIM_DNA_ID,
FK_HLA_PROT_DIPLOID,
FK_HLA_ANTIGENEID1,
FK_HLA_ANTIGENEID2,
HLA_RECEIVED_DATE,
HLA_TYPING_DATE,
FK_HLA_REPORTING_LAB,
FK_HLA_REPORT_METHOD,
FK_ORDER_ID,
FK_CORD_CT_STATUS,
FK_SPEC_TYPE,
FK_SOURCE  ON CB_BEST_HLA FOR EACH ROW DECLARE raid NUMBER(10);
  usr                                                                  VARCHAR2(100);
  old_modby                                                            VARCHAR2(100);
  new_modby                                                            VARCHAR2(100);
  BEGIN
    SELECT seq_audit.nextval INTO raid FROM dual;
    usr := getuser(:new.last_modified_by);
    audit_trail.record_transaction (raid, 'CB_BEST_HLA', :old.rid, 'U', usr);
	
    IF NVL(:old.PK_BEST_HLA,0) != NVL(:new.PK_BEST_HLA,0) THEN
      audit_trail.column_update (raid, 'PK_BEST_HLA', :OLD.PK_BEST_HLA, :NEW.PK_BEST_HLA);
    END IF;
	IF NVL(:old.FK_HLA_CODE_ID,0) != NVL(:new.FK_HLA_CODE_ID,0) THEN
      audit_trail.column_update (raid, 'FK_HLA_CODE_ID', :OLD.FK_HLA_CODE_ID, :NEW.FK_HLA_CODE_ID);
    END IF;
	IF NVL(:old.FK_HLA_METHOD_ID,0) != NVL(:new.FK_HLA_METHOD_ID,0) THEN
      audit_trail.column_update (raid, 'FK_HLA_METHOD_ID', :OLD.FK_HLA_METHOD_ID, :NEW.FK_HLA_METHOD_ID);
    END IF;
	IF NVL(:old.HLA_VALUE_TYPE1,' ') != NVL(:new.HLA_VALUE_TYPE1,' ') THEN
      audit_trail.column_update (raid, 'HLA_VALUE_TYPE1', :old.HLA_VALUE_TYPE1, :new.HLA_VALUE_TYPE1);
    END IF;
	IF NVL(:old.HLA_VALUE_TYPE2,' ') != NVL(:new.HLA_VALUE_TYPE2,' ') THEN
      audit_trail.column_update (raid, 'HLA_VALUE_TYPE2', :old.HLA_VALUE_TYPE2, :new.HLA_VALUE_TYPE2);
    END IF;	
	
	IF NVL(:old.CREATOR,0) != NVL(:new.CREATOR,0) THEN
      audit_trail.column_update (raid, 'CREATOR', :OLD.CREATOR, :NEW.CREATOR);
    END IF;
	IF NVL(:old.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.CREATED_ON,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
    audit_trail.column_update (raid, 'CREATED_ON', TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;	
	IF NVL(:old.LAST_MODIFIED_BY,0) != NVL(:new.LAST_MODIFIED_BY,0) THEN
      audit_trail.column_update (raid, 'LAST_MODIFIED_BY', :OLD.LAST_MODIFIED_BY, :NEW.LAST_MODIFIED_BY);
    END IF;	
	IF NVL(:old.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.LAST_MODIFIED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
    audit_trail.column_update (raid, 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;	
	IF NVL(:old.IP_ADD,' ') != NVL(:new.IP_ADD,' ') THEN
      audit_trail.column_update (raid, 'IP_ADD', :old.IP_ADD, :new.IP_ADD);
    END IF;
	IF NVL(:old.DELETEDFLAG,0) != NVL(:new.DELETEDFLAG,0) THEN
      audit_trail.column_update (raid, 'DELETEDFLAG', :OLD.DELETEDFLAG, :NEW.DELETEDFLAG);
    END IF;
	IF NVL(:old.RID,0) != NVL(:new.RID,0) THEN
      audit_trail.column_update (raid, 'RID', :OLD.RID, :NEW.RID);
    END IF;
   IF NVL(:old.ENTITY_ID,0) != NVL(:new.ENTITY_ID,0) THEN
      audit_trail.column_update (raid, 'ENTITY_ID', :OLD.ENTITY_ID, :NEW.ENTITY_ID);
    END IF;
	 IF NVL(:old.ENTITY_TYPE,0) != NVL(:new.ENTITY_TYPE,0) THEN
      audit_trail.column_update (raid, 'ENTITY_TYPE', :OLD.ENTITY_TYPE, :NEW.ENTITY_TYPE);
    END IF;
	 IF NVL(:old.FK_CORD_CDR_CBU_ID,' ') != NVL(:new.FK_CORD_CDR_CBU_ID,' ') THEN
      audit_trail.column_update (raid, 'FK_CORD_CDR_CBU_ID', :old.FK_CORD_CDR_CBU_ID, :new.FK_CORD_CDR_CBU_ID);
    END IF;
	IF NVL(:old.FK_HLA_OVERCLS_CODE,0) != NVL(:new.FK_HLA_OVERCLS_CODE,0) THEN
      audit_trail.column_update (raid, 'FK_HLA_OVERCLS_CODE', :OLD.FK_HLA_OVERCLS_CODE, :NEW.FK_HLA_OVERCLS_CODE);
    END IF;
	IF NVL(:old.HLA_CRIT_OVER_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.HLA_CRIT_OVER_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
    audit_trail.column_update (raid, 'HLA_CRIT_OVER_DATE', TO_CHAR(:OLD.HLA_CRIT_OVER_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.HLA_CRIT_OVER_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
	IF NVL(:old.HLA_OVER_IND_ANTGN1,' ') != NVL(:new.HLA_OVER_IND_ANTGN1,' ') THEN
      audit_trail.column_update (raid, 'HLA_OVER_IND_ANTGN1', :old.HLA_OVER_IND_ANTGN1, :new.HLA_OVER_IND_ANTGN1);
    END IF;
	IF NVL(:old.HLA_OVER_IND_ANTGN2,' ') != NVL(:new.HLA_OVER_IND_ANTGN2,' ') THEN
      audit_trail.column_update (raid, 'HLA_OVER_IND_ANTGN2', :old.HLA_OVER_IND_ANTGN2, :new.HLA_OVER_IND_ANTGN2);
    END IF;
    IF NVL(:old.FK_HLA_PRIM_DNA_ID,0) != NVL(:new.FK_HLA_PRIM_DNA_ID,0) THEN
      audit_trail.column_update (raid, 'FK_HLA_PRIM_DNA_ID', :OLD.FK_HLA_PRIM_DNA_ID, :NEW.FK_HLA_PRIM_DNA_ID);
    END IF;
	IF NVL(:old.FK_HLA_PROT_DIPLOID,0) != NVL(:new.FK_HLA_PROT_DIPLOID,0) THEN
      audit_trail.column_update (raid, 'FK_HLA_PROT_DIPLOID', :OLD.FK_HLA_PROT_DIPLOID, :NEW.FK_HLA_PROT_DIPLOID);
    END IF;
	IF NVL(:old.FK_HLA_ANTIGENEID1,0) != NVL(:new.FK_HLA_ANTIGENEID1,0) THEN
      audit_trail.column_update (raid, 'FK_HLA_ANTIGENEID1', :OLD.FK_HLA_ANTIGENEID1, :NEW.FK_HLA_ANTIGENEID1);
    END IF;
	IF NVL(:old.FK_HLA_ANTIGENEID2,0) != NVL(:new.FK_HLA_ANTIGENEID2,0) THEN
      audit_trail.column_update (raid, 'FK_HLA_ANTIGENEID2', :OLD.FK_HLA_ANTIGENEID2, :NEW.FK_HLA_ANTIGENEID2);
    END IF;   
	IF NVL(:old.HLA_RECEIVED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.HLA_RECEIVED_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
    audit_trail.column_update (raid, 'HLA_RECEIVED_DATE', TO_CHAR(:OLD.HLA_RECEIVED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.HLA_RECEIVED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
	IF NVL(:old.HLA_TYPING_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) != NVL(:new.HLA_TYPING_DATE,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
    audit_trail.column_update (raid, 'HLA_TYPING_DATE', TO_CHAR(:OLD.HLA_TYPING_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.HLA_TYPING_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT));
    END IF;
	IF NVL(:old.FK_HLA_REPORTING_LAB,0) != NVL(:new.FK_HLA_REPORTING_LAB,0) THEN
      audit_trail.column_update (raid, 'FK_HLA_REPORTING_LAB', :OLD.FK_HLA_REPORTING_LAB, :NEW.FK_HLA_REPORTING_LAB);
    END IF;
	IF NVL(:old.FK_HLA_REPORT_METHOD,0) != NVL(:new.FK_HLA_REPORT_METHOD,0) THEN
      audit_trail.column_update (raid, 'FK_HLA_REPORT_METHOD', :OLD.FK_HLA_REPORT_METHOD, :NEW.FK_HLA_REPORT_METHOD);
    END IF;
	IF NVL(:old.FK_ORDER_ID,0) != NVL(:new.FK_ORDER_ID,0) THEN
      audit_trail.column_update (raid, 'FK_ORDER_ID', :OLD.FK_ORDER_ID, :NEW.FK_ORDER_ID);
    END IF;
	IF NVL(:old.FK_CORD_CT_STATUS,0) != NVL(:new.FK_CORD_CT_STATUS,0) THEN
      audit_trail.column_update (raid, 'FK_CORD_CT_STATUS', :OLD.FK_CORD_CT_STATUS, :NEW.FK_CORD_CT_STATUS);
    END IF;
	IF NVL(:old.FK_SPEC_TYPE,0) != NVL(:new.FK_SPEC_TYPE,0) THEN
      audit_trail.column_update (raid, 'FK_SPEC_TYPE', :OLD.FK_SPEC_TYPE, :NEW.FK_SPEC_TYPE);
    END IF;	    
    IF NVL(:old.FK_SOURCE,' ') != NVL(:new.FK_SOURCE,' ') THEN
      audit_trail.column_update (raid, 'FK_SOURCE', :old.FK_SOURCE, :new.FK_SOURCE);
    END IF;
  END;
  /
  
  
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,158,18,'18_CB_BEST_HLA_AU0.sql',sysdate,'9.0.0 Build#615');

commit;