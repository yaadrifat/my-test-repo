SET DEFINE OFF;

DELETE FROM ER_REPXSL WHERE FK_REPORT = 162;

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,158,13,'13_ER_REPXSL_DELETE.sql',sysdate,'9.0.0 Build#615');

commit;