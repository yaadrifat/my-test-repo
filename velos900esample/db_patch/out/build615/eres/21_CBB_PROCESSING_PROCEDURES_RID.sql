set define off;
DECLARE
  COUNTFLAG NUMBER(5);  
BEGIN
    SELECT COUNT(1) INTO COUNTFLAG FROM  CBB_PROCESSING_PROCEDURES WHERE RID IS NULL;    
    IF (COUNTFLAG > 0) THEN
     update CBB_PROCESSING_PROCEDURES  set RID=SEQ_RID.NEXTVAL WHERE RID IS NULL;       
commit;
end if;
END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,158,21,'21_CBB_PROCESSING_PROCEDURES_RID.sql',sysdate,'9.0.0 Build#615');

commit;