--ALTER THE CB_MAIL_DOCUMENTS TABLE--

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_MAIL_DOCUMENTS'
    AND column_name = 'DOCUMENT_FILES';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_MAIL_DOCUMENTS ADD(DOCUMENT_FILES BLOB)';
  end if;
end;
/

COMMENT ON COLUMN "CB_MAIL_DOCUMENTS"."DOCUMENT_FILES" IS 'Document File.';

--STARTS ADDING COLUMN NMDP_SHIP_EXCUSE_FLAG TO CB_SHIPMENT TABLE
DECLARE
 v_column_exists NUMBER :=0;
BEGIN
 SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'CB_SHIPMENT' AND COLUMN_NAME = 'NMDP_SHIP_EXCUSE_FLAG';
IF (v_column_exists = 0) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE CB_SHIPMENT ADD(NMDP_SHIP_EXCUSE_FLAG VARCHAR2(2 BYTE))';
END IF;
END;
/
--END

COMMENT ON COLUMN CB_SHIPMENT.NMDP_SHIP_EXCUSE_FLAG IS 'Stores ''Y'' if excuse form is submitted else stores null or ''N''';

--STARTS ADDING COLUMN INFUSION_FLAG TO CB_RECEIPANT_INFO TABLE
DECLARE
 v_column_exists NUMBER :=0;
BEGIN
 SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'CB_RECEIPANT_INFO' AND COLUMN_NAME = 'INFUSION_FLAG';
IF (v_column_exists = 0) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE CB_RECEIPANT_INFO ADD(INFUSION_FLAG VARCHAR2(2 BYTE))';
END IF;
END;
/
--END

COMMENT ON COLUMN CB_RECEIPANT_INFO.INFUSION_FLAG IS 'Stores ''Y'' if cord is infused else stores null or ''N''';



--STARTS ADDING COLUMN INFUSION_FLAG TO CB_CORD TABLE
DECLARE
 v_column_exists NUMBER :=0;
BEGIN
 SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'CB_CORD' AND COLUMN_NAME = 'INFUSION_FLAG';
IF (v_column_exists = 0) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE CB_CORD ADD(INFUSION_FLAG VARCHAR2(2 BYTE))';
END IF;
END;
/
--END

COMMENT ON COLUMN CB_CORD.INFUSION_FLAG IS 'Stores ''Y'' if cord is infused else stores null or ''N''';



--STARTS ADDING COLUMN INFUSION_DATE TO CB_CORD TABLE
DECLARE
 v_column_exists NUMBER :=0;
BEGIN
 SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'CB_CORD' AND COLUMN_NAME = 'INFUSION_DATE';
IF (v_column_exists = 0) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE CB_CORD ADD(INFUSION_DATE DATE)';
END IF;
END;
/
--END

COMMENT ON COLUMN CB_CORD.INFUSION_DATE IS 'Date entered by the CM once the CBU has been infused (indication by the TC)';


--STARTS ADDING COLUMN SERVICE_CODE TO ER_ORDER TABLE
DECLARE
 v_column_exists NUMBER :=0;
BEGIN
 SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'ER_ORDER' AND COLUMN_NAME = 'SERVICE_CODE';
IF (v_column_exists = 0) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE ER_ORDER ADD(SERVICE_CODE VARCHAR2(50 BYTE))';
END IF;
END;
/
--END

COMMENT ON COLUMN ER_ORDER.SERVICE_CODE IS 'Stores service code received from ESB message';

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,158,4,'04_MISC_ALTER.sql',sysdate,'9.0.0 Build#615');

commit;

