create or replace
PACKAGE BODY Pkg_User
AS
PROCEDURE SP_SITETREE (p_account IN NUMBER, p_user IN NUMBER, p_site_cursor OUT Types.cursorType)
AS
  /***************************************************************************************************
   ** Procedure to generate a tree structure for all the organizations(sites) in an account
   ** Author: Sonika Talwar 30th April 2003
   ** Input parameter: Account Id
   ** Input parameter: User Id
   ** Output parameter: index-by table of records , p_siterows OUT TYPES.SITEROWS_TABLE
   **/
   v_site_rec SITEREC_RECORD := SITEREC_RECORD(0,0,'0',0,0,0); --record type
   v_site_table SITEROWS_TABLE := SITEROWS_TABLE(); --index-by table type
   v_cnt NUMBER;
   i NUMBER;
   N NUMBER;
BEGIN
v_cnt := 0;
--get all the main parent sites
FOR i IN
	(SELECT pk_site, pk_usersite, site_name, usersite_right
		FROM erv_usersites
		WHERE site_parent IS NULL
		AND fk_account=p_account
		AND fk_user=p_user
		ORDER BY LOWER(site_name))
LOOP
BEGIN
      v_cnt := v_cnt + 1;
--store each column in a record
      v_site_rec.siteid :=  i.pk_usersite;
      v_site_rec.usersiteid := i.pk_site; --fk_site
      v_site_rec.sitename := i.site_name;
      v_site_rec.siteright := i.usersite_right;
      v_site_rec.indent := 0;
	 v_site_rec.parentid := i.pk_site;
--store each record in a table
      v_site_table.EXTEND;
	  v_site_table(v_cnt) := v_site_rec;
--call function to get children sites
	v_site_table := F_GETCHILDSITES(i.pk_site, v_site_table, p_account, p_user, 0,i.pk_site);
	v_cnt := v_site_table.COUNT();
END;
END LOOP;
OPEN p_site_cursor
    FOR
     SELECT *
       FROM TABLE( CAST(v_site_table AS SITEROWS_TABLE));
END; --end of SP_SITETREE
----------------------------------------------------------------------------------------------------
FUNCTION F_GETCHILDSITES
(p_parentsite NUMBER, p_site_table SITEROWS_TABLE, p_account NUMBER, p_user NUMBER, p_indent NUMBER, p_main_parent NUMBER)
    RETURN  SITEROWS_TABLE
AS
 /****************************************************************************************************
  **Function to get all the child organizations(sites) of a given site
  **Function is called recursively to get child sites up to any level
  **Author Sonika Talwar April 30, 2003
  **Input parameter: p_parentsite parent site
  **Input parameter: p_site_table index-by table
  **Input parameter: p_account account_id
  **Input parameter: p_site user_id
  **Input parameter: p_indent indentation number
  **Input parameter: p_main_parent this is id of the main head parent site whose sub sites have been formed
  **                 eg if B is child of A and C is child of B then the main parent is A for both
  **Output parameter: index-by table
  */
  v_childno NUMBER;
  v_childsite_table SITEROWS_TABLE := SITEROWS_TABLE();
  v_childsite_rec SITEREC_RECORD :=  SITEREC_RECORD(0,0,'0',0,0,0);
  v_count NUMBER;
  v_indent NUMBER;
  v_loopcnt NUMBER;
   BEGIN
	      v_indent := p_indent + 1;
	      v_count := p_site_table.COUNT() ;
           v_childsite_table := p_site_table;
		 SELECT COUNT(*)
		    INTO v_loopcnt
              FROM erv_usersites
		    WHERE site_parent = p_parentsite
		    AND fk_account = p_account
		    AND fk_user = p_user;
	   --get child sites for the given parent site
	      FOR i IN
               (SELECT pk_site, pk_usersite, site_name, usersite_right, site_parent
  		         FROM erv_usersites
			    WHERE site_parent = p_parentsite
			    AND fk_account = p_account
			    AND fk_user = p_user
				ORDER BY LOWER(site_name))
		 LOOP
		 BEGIN
		     v_loopcnt := v_loopcnt -1;
               v_count := v_childsite_table.COUNT() + 1;
               --store each column in a record
               v_childsite_rec.siteid := i.pk_usersite;
			v_childsite_rec.usersiteid := i.pk_site; --fk_site
               v_childsite_rec.sitename := i.site_name;
               v_childsite_rec.siteright := i.usersite_right;
               v_childsite_rec.indent := v_indent ;
			v_childsite_rec.parentid := p_main_parent ;
               --store each record in a table
			v_childsite_table.EXTEND;
               v_childsite_table(v_count) := v_childsite_rec;
		     SELECT COUNT(*)
			    INTO v_childno
			    FROM ER_SITE
			    WHERE site_parent = i.pk_site;
			IF (v_childno > 0) THEN
			  --call the function recursively if child sites exist, else exit
                   v_childsite_table := F_GETCHILDSITES(i.pk_site, v_childsite_table, p_account, p_user, v_indent,p_main_parent);
			ELSIF (v_childno > 0 AND v_loopcnt = 0) THEN --no child left
                   EXIT;
			END IF;
		 END;
		 END LOOP;
        RETURN v_childsite_table;
   END; -- end of F_GETCHILDSITES
----------------------------------------------------------------------------------------------------
PROCEDURE SP_UPDATESITERIGHTS(p_pkuser_sites IN ARRAY_STRING,p_rights IN ARRAY_STRING,p_user IN VARCHAR2,p_ipadd IN VARCHAR2,o_ret OUT NUMBER)
AS
  /****************************************************************************************************
   ** Procedure to update rights for all the organizations(sites) for an account user,
   ** multiple records are updated
   ** Author: Sonika Talwar 6th May 2003
   ** Input parameter: Array of pkUserSites
   ** Input parameter: Array of corresponding rights of each pkUserSites
   ** Input parameter: creator of record (userId)
   ** Input parameter: IP Address
   ** Output parameter: 0 for successful update, -1 for error
   **/
   v_cnt NUMBER;
   i NUMBER;
   v_pkusersite NUMBER;
   v_right NUMBER;
BEGIN
   v_cnt := p_pkuser_sites.COUNT();
   i:=1;
   WHILE i <= v_cnt LOOP
      v_pkusersite := TO_NUMBER(p_pkuser_sites(i));
      v_right := TO_NUMBER(p_rights(i));

	 UPDATE ER_USERSITE
	    SET usersite_right = v_right,
             last_modified_by = p_user,
		   last_modified_date = SYSDATE,
		   ip_add = p_ipadd
	    WHERE pk_usersite = v_pkusersite ;
	 i := i+1;
   END LOOP;
   COMMIT;
   o_ret:=0;
END; --end of SP_UPDATESITERIGHTS
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
PROCEDURE SP_UPDATESITERIGHTS(p_pkuser_sites IN ARRAY_STRING,p_rights IN ARRAY_STRING,p_user IN VARCHAR2,p_userId VARCHAR2,p_ipadd IN VARCHAR2,o_ret OUT NUMBER)
AS
  /****************************************************************************************************
   ** Procedure to update rights for all the organizations(sites) for an account user,
   ** multiple records are updated
   ** Author: Sonika Talwar 6th May 2003
   ** Input parameter: Array of pkUserSites
   ** Input parameter: Array of corresponding rights of each pkUserSites
   ** Input parameter: creator of record (userId)
   ** Input parameter: IP Address
   ** Output parameter: 0 for successful update, -1 for error
   **/
   v_cnt NUMBER;
   i NUMBER;
   v_pkusersite NUMBER;
   v_right NUMBER;
   v_ifRem Number;
BEGIN
   v_cnt := p_pkuser_sites.COUNT();
   i:=1;
   WHILE i <= v_cnt LOOP
      v_pkusersite := TO_NUMBER(p_pkuser_sites(i));
      v_right := TO_NUMBER(p_rights(i));
      IF(v_right = 0) THEN
        --Delete user-site-group from ER_USRSITE_GRP when removed
      plog.DEBUG(pctx,'>> SELECT COUNT(*) INTO v_ifRem FROM ER_USRSITE_GRP WHERE FK_USER_SITE='||v_pkusersite||' AND FK_USER='||p_userId);
        SELECT COUNT(*) INTO v_ifRem FROM ER_USRSITE_GRP WHERE FK_USER_SITE=v_pkusersite AND FK_USER=p_userId;
        IF(v_ifRem>0) THEN
        DELETE FROM ER_USRSITE_GRP WHERE FK_USER_SITE=v_pkusersite AND FK_USER=p_userId;
        COMMIT;
      END IF;
     END IF; 

	 UPDATE ER_USERSITE
	    SET usersite_right = v_right,
             last_modified_by = p_user,
		   last_modified_date = SYSDATE,
		   ip_add = p_ipadd
	    WHERE pk_usersite = v_pkusersite ;
	 i := i+1;
   END LOOP;
   COMMIT;
   o_ret:=0;
END; --end of SP_UPDATESITERIGHTS
----------------------------------------------------------------------------------------------------
  PROCEDURE SP_CREATE_USERSITE_DATA
   (
      p_user IN NUMBER,
	 p_account IN NUMBER,
	 p_def_group IN NUMBER,
	 p_old_site IN NUMBER,
	 p_def_site IN NUMBER,
	 p_creator IN NUMBER,
	 p_ip IN VARCHAR2,
	 p_mode IN VARCHAR2,
	 o_success OUT NUMBER
   )
AS
  /****************************************************************************************************
   ** Procedure to create er_usersite data for all the organizations(sites) for user's account
   ** The default site gets rights same as user's default group's Manage Patients rights
   ** Author: Sonia Sahni 12th May 2003
   ** p_user : Pk_user
   ** p_account : pk of user account
   ** p_def_group : pk of user default group
   ** p_old_site : pk of previous user site
   ** p_def_site : pk of new user site
   ** p_creator : pk of creator (fk_user)
   ** p_ip IN : ip add of client machine
   ** p_mode: indicates mode, N- new, M- Modify.
   ** o_success : Output parameter: 0 for successful update, -1 for error
   ** Modified by Anu Khanna on Sep 18, 03 - when organization of already existing user is changed then the rights for its
   ** primary organization are set corresponding to its group rights and the rest organization's rights are set to 0
   ** Modified by Sonika on Dec 17, 03 to update/create form share with data for the user when the primary organization s changed
   ** calls another SP for this.
   **/
   v_right_seq NUMBER;
BEGIN
/**Insert values into er_userdite table when new user is created*/
   --KM -#3634
   IF p_mode = 'N' THEN
	   INSERT INTO ER_USERSITE (PK_USERSITE,FK_USER,FK_SITE,USERSITE_RIGHT,CREATED_ON,CREATOR,IP_ADD  )
	   SELECT SEQ_ER_USERSITE.NEXTVAL,p_user,s.pk_site,0,SYSDATE,p_creator,p_ip
	   FROM ER_SITE s
	   WHERE s.fk_Account = p_account  ;
/**update data in er_usersite when user deatails page is opened in modified mode*/
   ELSIF p_mode  = 'M' THEN
	   UPDATE ER_USERSITE
	   SET USERSITE_RIGHT =0, last_modified_by = p_creator, last_modified_date = sysdate, ip_add = p_ip
	   WHERE fk_user=p_user;
   END IF;
-- get sequence number of Manage Patients Right
   SELECT  CTRL_SEQ
   INTO v_right_seq
   FROM ER_CTRLTAB
   WHERE  CTRL_KEY       = 'app_rights' AND
   CTRL_VALUE     = 'MPATIENTS';
 ---
  UPDATE ER_USERSITE a
  SET USERSITE_RIGHT = (
  SELECT TO_NUMBER(SUBSTR(GRP_RIGHTS,v_right_seq,1))
  FROM ER_GRPS
  WHERE  PK_GRP = p_def_group), last_modified_by = p_creator, last_modified_date = sysdate, ip_add = p_ip
  WHERE fk_user = p_user AND
  fk_site = p_def_site;
  o_success := 0;
  --added by Sonika on Dec 17, 03 to update the user form share with data when primary organization is changed
  IF p_mode  = 'M' THEN
    SP_CREATE_USERFORMSHARE_DATA (p_user , p_old_site, p_def_site , p_creator , p_ip , o_success );
  ELSIF p_mode = 'N' THEN
    SP_CREATE_FORMSHAREWITH_DATA(p_user, p_account, p_def_site, p_creator, p_ip, o_success);
  END IF;
  COMMIT;
END; --end of SP_CREATE_USERSITE_DATA
---------------------------------------------------------------------------------------------------------------------
PROCEDURE SP_CREATE_FORMSHAREWITH_DATA (p_user IN NUMBER, p_account NUMBER, p_site IN NUMBER, p_creator IN NUMBER, p_ip IN VARCHAR2, o_success OUT NUMBER)
AS
  /****************************************************************************************************
   ** Procedure to create er_formsharewith data for all the forms who have the user primary organization in sharewith
   ** Author: Sonika Talwar Nov 25, 2003
   ** Modified by: Sonika Talwar May 03, 04 to create object share data
   ** p_user : Pk_user
   ** p_account : User Account
   ** p_site : fk user site, user primary organization
   ** p_creator : pk of creator (fk_user)
   ** p_ip IN : ip add of client machine
   ** o_success : Output parameter: 0 for successful update, -1 for error
   **/
   v_formid NUMBER;
   v_object_number NUMBER;
   v_fk_object NUMBER;
   v_parent NUMBER;
BEGIN
--insert records for new user's account
-- now we are using er_objectshare table
/*for i in (select FK_FORMLIB
          from er_formsharewith
          where FSW_ID = p_account
          and FSW_TYPE = 'A')
Loop
    v_formid := i.fk_formlib;
    INSERT INTO ER_FORMSHAREWITH(PK_FSW,FK_FORMLIB,FSW_ID ,FSW_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD)
		 VALUES (SEQ_ER_FORMSHAREWITH.nextval,v_formid,p_user,'U',p_creator ,'N',sysdate,p_ip);
end Loop;
--insert records for new user's primary organization
for i in (select FK_FORMLIB
          from er_formsharewith
          where FSW_ID = p_site
          and FSW_TYPE = 'O')
Loop
    v_formid := i.fk_formlib;
    INSERT INTO ER_FORMSHAREWITH(PK_FSW,FK_FORMLIB,FSW_ID ,FSW_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD)
		 VALUES (SEQ_ER_FORMSHAREWITH.nextval,v_formid,p_user,'U',p_creator ,'N',sysdate,p_ip);
end Loop;
*/
--insert records for new user's account in object share
FOR i IN (SELECT pk_objectshare,fk_object, object_number
          FROM ER_OBJECTSHARE
          WHERE fk_objectshare_id = p_account
          AND objectshare_type = 'A')
LOOP
	v_parent := i.pk_objectshare;
    v_fk_object := i.fk_object;
    v_object_number := i.object_number;
    INSERT INTO ER_OBJECTSHARE(PK_OBJECTSHARE,OBJECT_NUMBER,FK_OBJECT ,FK_OBJECTSHARE_ID,OBJECTSHARE_TYPE, CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD,fk_objectshare)
		 VALUES (SEQ_ER_OBJECTSHARE.NEXTVAL,v_object_number, v_fk_object,p_user,'U',p_creator,'N',SYSDATE,p_ip,v_parent);
END LOOP;
--insert records for new user's primary organization in object share
FOR i IN (SELECT pk_objectshare, fk_object, object_number
          FROM ER_OBJECTSHARE
          WHERE fk_objectshare_id = p_site
          AND objectshare_type = 'O')
LOOP
	v_parent := i.pk_objectshare;
    v_fk_object := i.fk_object;
    v_object_number := i.object_number;
    INSERT INTO ER_OBJECTSHARE(PK_OBJECTSHARE,OBJECT_NUMBER,FK_OBJECT ,FK_OBJECTSHARE_ID,OBJECTSHARE_TYPE, CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD,fk_objectshare)
		 VALUES (SEQ_ER_OBJECTSHARE.NEXTVAL,v_object_number, v_fk_object,p_user,'U',p_creator,'N',SYSDATE,p_ip,v_parent);
END LOOP;
END; -- end of SP_CREATE_FORMSHAREWITH_DATA
------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE SP_CREATE_USERFORMSHARE_DATA (p_user IN NUMBER, p_oldsite IN NUMBER, p_newsite IN NUMBER, p_creator IN NUMBER, p_ip IN VARCHAR2, o_success OUT NUMBER )
AS
  /****************************************************************************************************
   ** Procedure to update er_formsharewith data according to the changed organization
   ** Remove the deleted user from the Form in which the old primary organization has been selected in Form share with
   ** to insert the user in the Form share with so that the user is able to access the Forms available to this Primary Organization
   ** Author: Sonika Talwar Dec 17, 2003
   ** Modified by: Sonika Talwar May 03, 04 to set object share with data
   ** p_user : Pk_user
   ** p_site : fk user site, user primary organization
   ** p_creator : pk of creator (fk_user)
   ** p_ip IN : ip add of client machine
   ** o_success : Output parameter: 0 for successful update, -1 for error
   **/
  v_formid NUMBER;
  v_object_number NUMBER;
  v_fk_object NUMBER;
  v_parent NUMBER;
BEGIN
 BEGIN

 /* DELETE FROM ER_FORMSHAREWITH
  WHERE fk_formlib IN
 (SELECT fk_formlib
  FROM ER_FORMSHAREWITH
  WHERE fsw_type = 'O' AND fsw_id = p_oldsite)
  AND fsw_type = 'U'
  AND fsw_id = p_user ;

 FOR i IN (SELECT FK_FORMLIB
          FROM ER_FORMSHAREWITH
          WHERE FSW_ID = p_newsite
          AND FSW_TYPE = 'O')
 LOOP
    v_formid := i.fk_formlib;
    INSERT INTO ER_FORMSHAREWITH(PK_FSW,FK_FORMLIB,FSW_ID ,FSW_TYPE,CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD)
		 VALUES (SEQ_ER_FORMSHAREWITH.NEXTVAL,v_formid,p_user,'U',p_creator ,'N',SYSDATE,p_ip);
 END LOOP; */



  DELETE FROM ER_OBJECTSHARE
  WHERE fk_object IN
 (SELECT fk_object
  FROM ER_OBJECTSHARE
  WHERE objectshare_type = 'O' AND fk_objectshare_id = p_oldsite)
  AND objectshare_type = 'U'
  AND fk_objectshare_id = p_user  AND
fk_objectshare IN (SELECT pk_objectshare FROM ER_OBJECTSHARE i
				  WHERE i.objectshare_type = 'O' AND i.fk_objectshare_id = p_oldsite AND i.fk_object = fk_object)  ;


 FOR i IN (SELECT pk_objectshare,fk_object, object_number
          FROM ER_OBJECTSHARE
          WHERE fk_objectshare_id = p_newsite
          AND objectshare_type = 'O')
 LOOP
    v_parent := i.pk_objectshare;
    v_fk_object := i.fk_object;
    v_object_number := i.object_number ;

    INSERT INTO ER_OBJECTSHARE(PK_OBJECTSHARE,OBJECT_NUMBER,FK_OBJECT ,FK_OBJECTSHARE_ID,OBJECTSHARE_TYPE, CREATOR,RECORD_TYPE,CREATED_ON,IP_ADD,fk_objectshare)
		 VALUES (SEQ_ER_OBJECTSHARE.NEXTVAL,v_object_number, v_fk_object,p_user,'U',p_creator ,'N',SYSDATE,p_ip,v_parent);
 END LOOP;
 EXCEPTION WHEN OTHERS THEN
   o_success := -1;
   RETURN;
 END;
 o_success:=0;
 COMMIT;
END; -- end of SP_CREATE_USERFORMSHARE_DATA
-----------------------------------------
PROCEDURE SP_CREATE_STUDYSITE_DATA
   (
     p_user IN NUMBER,
	 p_account IN NUMBER,
	 p_study IN NUMBER,
	 p_creator IN NUMBER,
	 p_ip IN VARCHAR2,
	 o_success OUT NUMBER
   )
AS
  /****************************************************************************************************
   ** Procedure to create er_study_site_rights data for users of organizations corresponding to a study
   ** The default site gets rights same as user's default group's Manage Patients rights
   ** Author: Anu Khanna 18th Nov 2004
   ** p_user : user Id
   ** p_account : pk of user account
   ** p_study : study Id
   ** p_creator : pk of creator (fk_user)
   ** p_ip IN : ip add of client machine
   ** o_success : Output parameter: 0 for successful update, -1 for error
   **/
   v_right_seq NUMBER;
   v_nonsys_site NUMBER;
   v_count NUMBER;
   v_pk_studysite_rights NUMBER;
BEGIN
/**Insert values into er_study_site_rights table when new user is added to a study*/
--Check whether it is a non system user.
SELECT COUNT(*) INTO v_count FROM ER_USER WHERE usr_type='N' AND
	   pk_user = p_user;
	   IF(v_count = 1) THEN
	    SELECT fk_siteid INTO v_nonsys_site FROM ER_USER WHERE usr_type='N' AND
	   	pk_user = p_user;
	   INSERT INTO ER_STUDY_SITE_RIGHTS (PK_STUDY_SITE_RIGHTS,FK_SITE,FK_STUDY,FK_USER,USER_STUDY_SITE_RIGHTS,CREATOR,CREATED_ON,IP_ADD)
	   SELECT SEQ_ER_STUDYSITE_RIGHTS.NEXTVAL,FK_SITEID,p_study,p_user,1,p_creator,SYSDATE,p_ip
	   FROM ER_USER
	   WHERE pk_user = p_user
	   AND fk_account = p_account
	   AND fk_siteid = v_nonsys_site;
	   	   FOR i IN (--Select distinct FK_SITE from er_usersite s,er_user u  where s.fk_site = u.fk_siteid
					   --and fk_user = pk_user  and fk_account = p_account  and fk_site <> v_nonsys_site
					   SELECT DISTINCT PK_SITE FROM erv_usersites WHERE fk_account = p_account AND
					   pk_site <> v_nonsys_site)
			LOOP
		    	SELECT  SEQ_ER_STUDYSITE_RIGHTS.NEXTVAL INTO v_pk_studysite_rights FROM dual;
			    INSERT INTO ER_STUDY_SITE_RIGHTS (PK_STUDY_SITE_RIGHTS,FK_SITE,FK_STUDY,FK_USER,USER_STUDY_SITE_RIGHTS,CREATOR,CREATED_ON,IP_ADD)
			    VALUES(v_pk_studysite_rights,i.PK_SITE,p_study,p_user,0,p_creator,SYSDATE,p_ip);
		   END LOOP ;
	   ELSE
	   INSERT INTO ER_STUDY_SITE_RIGHTS (PK_STUDY_SITE_RIGHTS,FK_SITE,FK_STUDY,FK_USER,USER_STUDY_SITE_RIGHTS,CREATOR,CREATED_ON,IP_ADD)
	   SELECT SEQ_ER_STUDYSITE_RIGHTS.NEXTVAL,FK_SITE,p_study,p_user,1,p_creator,SYSDATE,p_ip
	   FROM ER_USERSITE s,ER_USER u
	   WHERE s.fk_user = u.pk_user
	   AND s.fk_user = p_user
	   AND fk_account = p_account
	   AND USERSITE_RIGHT >= 4;
	    INSERT INTO ER_STUDY_SITE_RIGHTS (PK_STUDY_SITE_RIGHTS,FK_SITE,FK_STUDY,FK_USER,USER_STUDY_SITE_RIGHTS,CREATOR,CREATED_ON,IP_ADD)
	   SELECT SEQ_ER_STUDYSITE_RIGHTS.NEXTVAL,FK_SITE,p_study,p_user,0,p_creator,SYSDATE,p_ip
	   FROM ER_USERSITE s,ER_USER u
	   WHERE s.fk_user = u.pk_user
	   AND s.fk_user = p_user
	   AND fk_account = p_account
	   AND USERSITE_RIGHT < 4 ;
	   END IF;
	 EXCEPTION WHEN OTHERS THEN
   	   o_success := -1;
   RETURN;
 o_success:=0;
 COMMIT;
END; --end of SP_CREATE_STUDYSITE_DATA
--------------------------------------------
PROCEDURE SP_STUDYSITETREE (p_account IN NUMBER, p_user IN NUMBER,p_study IN NUMBER, p_site_cursor OUT Types.cursorType)
AS
  /***************************************************************************************************
   ** Procedure to generate a tree structure for all the organizations in an account of a particular user
   ** Author: Anu Khanna 18th Nov 2004
   ** Input parameter: Account Id
   ** Input parameter: User Id
   ** Input parameter: Study Id
   ** Output parameter: index-by table of records , p_siterows OUT TYPES.SITEROWS_TABLE
   **/
   v_site_rec SITEREC_RECORD := SITEREC_RECORD(0,0,'0',0,0,0); --record type
   v_site_table SITEROWS_TABLE := SITEROWS_TABLE(); --index-by table type
   v_cnt NUMBER;
   i NUMBER;
   N NUMBER;
BEGIN
v_cnt := 0;
--get all the main parent sites
FOR i IN
/*	( 	select pk_site, pk_study_site_rights, site_name, user_study_site_rights
		from er_site, er_study_site_rights
		where pk_site = fk_site
		and site_parent is null
		and fk_account=p_account
		and fk_user=p_user
		and fk_study = p_study )*/
		(SELECT pk_site, pk_study_site_rights, site_name, LOWER(site_name) sName, user_study_site_rights
		FROM ER_SITE, ER_STUDY_SITE_RIGHTS
		WHERE pk_site = fk_site
		AND site_parent IS NULL
		AND fk_account=p_account
		AND fk_user=p_user
		AND fk_study = p_study
		UNION SELECT pk_site, 0, site_name, LOWER(site_name) sName, 0
		FROM ER_SITE , ER_USERSITE
		WHERE pk_site = fk_site
		AND site_parent IS NULL
		AND fk_account=p_account
		AND fk_user=p_user
		AND pk_site NOT IN(SELECT pk_site
		FROM ER_SITE, ER_STUDY_SITE_RIGHTS
		WHERE pk_site = fk_site
		AND site_parent IS NULL
		AND fk_account=p_account
		AND fk_user=p_user
		AND fk_study = p_study )
		ORDER BY sName )
LOOP
BEGIN
      v_cnt := v_cnt + 1;
--store each column in a record
      v_site_rec.siteid :=  i.pk_study_site_rights;
	  v_site_rec.usersiteid := i.pk_site;
      v_site_rec.sitename := i.site_name;
      v_site_rec.siteright := i.user_study_site_rights;
      v_site_rec.indent := 0;
	 v_site_rec.parentid := i.pk_site;
--store each record in a table
      v_site_table.EXTEND;
	  v_site_table(v_cnt) := v_site_rec;
--call function to get children sites
	v_site_table := F_GETCHILDSTUDYSITES(i.pk_site, v_site_table, p_account, p_user,p_study,0,i.pk_site);
	v_cnt := v_site_table.COUNT();
END;
END LOOP;
OPEN p_site_cursor
    FOR
     SELECT *
       FROM TABLE( CAST(v_site_table AS SITEROWS_TABLE));
END; --end of SP_SITETREE
----------------------------------------------------------------------------------------------------
FUNCTION F_GETCHILDSTUDYSITES
(p_parentsite NUMBER, p_site_table SITEROWS_TABLE, p_account NUMBER, p_user NUMBER,p_study NUMBER, p_indent NUMBER, p_main_parent NUMBER)
    RETURN  SITEROWS_TABLE
AS
 /****************************************************************************************************
  **Function to get all the child organizations(sites) of a given site
  **Function is called recursively to get child sites up to any level
  **Author Anu Khanna 18th Nov, 2004
  **Input parameter: p_parentsite parent site
  **Input parameter: p_site_table index-by table
  **Input parameter: p_account account_id
  **Input parameter: p_site user_id
  **Input parameter: p_study study_id
  **Input parameter: p_indent indentation number
  **Input parameter: p_main_parent this is id of the main head parent site whose sub sites have been formed
  **                 eg if B is child of A and C is child of B then the main parent is A for both
  **Output parameter: index-by table
  */
  v_childno NUMBER;
  v_childsite_table SITEROWS_TABLE := SITEROWS_TABLE();
  v_childsite_rec SITEREC_RECORD :=  SITEREC_RECORD(0,0,'0',0,0,0);
  v_count NUMBER;
  v_indent NUMBER;
  v_loopcnt NUMBER;
   BEGIN
	      v_indent := p_indent + 1;
	      v_count := p_site_table.COUNT() ;
           v_childsite_table := p_site_table;
		 SELECT COUNT(*)
		    INTO v_loopcnt
              FROM erv_usersites
		    WHERE site_parent = p_parentsite
		    AND fk_account = p_account
		    AND fk_user = p_user;
	   --get child sites for the given parent site
	      FOR i IN
               /*(select pk_site, pk_study_site_rights, site_name, user_study_site_rights, site_parent
				from er_site, er_study_site_rights
				where pk_site = fk_site
				and site_parent = p_parentsite
				and fk_account=p_account
				and fk_user=p_user
				and fk_study = p_study)*/
				(SELECT pk_site, pk_study_site_rights, site_name, LOWER(site_name) sName, user_study_site_rights, site_parent
				FROM ER_SITE, ER_STUDY_SITE_RIGHTS
				WHERE pk_site = fk_site
				AND site_parent = p_parentsite
				AND fk_account=p_account
				AND fk_user=p_user
				AND fk_study = p_study
				UNION SELECT pk_site, 0, site_name, LOWER(site_name) sName , 0, site_parent
				FROM ER_SITE , ER_USERSITE
				WHERE pk_site = fk_site
				AND site_parent = p_parentsite
				AND fk_account=p_account
				AND fk_user=p_user
				AND pk_site NOT IN(SELECT pk_site
				FROM ER_SITE, ER_STUDY_SITE_RIGHTS
				WHERE pk_site = fk_site
				AND site_parent = p_parentsite
				AND fk_account=p_account
				AND fk_user=p_user
				AND fk_study = p_study)
				ORDER BY sName)
		 LOOP
		 BEGIN
		     v_loopcnt := v_loopcnt -1;
               v_count := v_childsite_table.COUNT() + 1;
               --store each column in a record
			   v_childsite_rec.siteid := i.pk_study_site_rights;
			   v_childsite_rec.usersiteid := i.pk_site;
               v_childsite_rec.sitename := i.site_name;
			   v_childsite_rec.siteright := i.user_study_site_rights;
               v_childsite_rec.indent := v_indent ;
			   v_childsite_rec.parentid := p_main_parent ;
               --store each record in a table
			v_childsite_table.EXTEND;
               v_childsite_table(v_count) := v_childsite_rec;
		     SELECT COUNT(*)
			    INTO v_childno
			    FROM ER_SITE
			    WHERE site_parent = i.pk_site;
			IF (v_childno > 0) THEN
			  --call the function recursively if child sites exist, else exit
                   v_childsite_table := F_GETCHILDSTUDYSITES(i.pk_site, v_childsite_table, p_account, p_user, p_study, v_indent,p_main_parent);
			ELSIF (v_childno > 0 AND v_loopcnt = 0) THEN --no child left
                   EXIT;
			END IF;
		 END;
		 END LOOP;
        RETURN v_childsite_table;
   END; -- end of F_GETCHILDSTUDYSITES
   --------------------------
   PROCEDURE SP_UPDATESTUDYSITERIGHTS(p_pk_study_sites IN ARRAY_STRING,p_site_ids IN ARRAY_STRING, p_rights IN ARRAY_STRING,p_study_id IN NUMBER,p_user IN VARCHAR2,p_ipadd IN VARCHAR2,o_ret OUT NUMBER
   ,p_creator IN Varchar2)
AS
  /****************************************************************************************************
   ** Procedure to update rights for all the organizations(sites) for a study for an account user,
   ** multiple records are updated
   ** Author: Anu Khanna 18th Nov 2004
   ** Input parameter: Array of pkStudySites
   ** Input parameter: Array of corresponding rights of each pkStudySites
   ** Input parameter: creator of record (userId)
   ** Input parameter: IP Address
   ** Output parameter: 0 for successful update, -1 for error
   **/
   v_cnt NUMBER;
   i NUMBER;
   v_pkstudysite NUMBER;
   v_right NUMBER;
   v_study_id NUMBER;
   v_site_id NUMBER;
   v_tot NUMBER;
   v_active_cnt NUMBER;
   v_compl_cnt NUMBER;
   v_pk_studysite_rights NUMBER;
BEGIN
   v_cnt := p_pk_study_sites.COUNT();
   i:=1;
   WHILE i <= v_cnt LOOP
      v_pkstudysite := TO_NUMBER(p_pk_study_sites(i));
	  v_site_id := TO_NUMBER(p_site_ids(i));
	  v_right := TO_NUMBER(p_rights(i));
	 --if right is revoked for an org and that org has status to it. Then delete those status
	/*if(v_right = 0) then
	 select fk_site, fk_study  into v_site_id ,
	 v_study_id from
	 er_study_site_rights
	 where PK_STUDY_SITE_RIGHTS = v_pkstudysite ;
	select count(*) into v_tot from er_studystat
	 where fk_study = v_study_id
	 and fk_site = v_site_id;
	 if(v_tot>0) then
	  delete from er_studystat
	  where fk_study = v_study_id
	 and fk_site = v_site_id;
	 end if;--cnt>0*/
	  --find the count for active enrolling status
			/* select count(*) into v_active_cnt
				 from er_studystat
				 where fk_study=   v_study_id and
				 FK_CODELST_STUDYSTAT =
				(select pk_codelst
				 from er_codelst where codelst_type ='studystat'  and codelst_subtyp='active' );
				 if(v_active_cnt = 0) then
				 update er_study set STUDY_ACTUALDT = null
				 where pk_study =  v_study_id;
				 else
				  update er_study set STUDY_ACTUALDT =
				  (select min(STUDYSTAT_DATE)
				  from er_studystat 	where  fk_study =  v_study_id
				 and FK_CODELST_STUDYSTAT = 	 (select pk_codelst  from er_codelst where codelst_type ='studystat'  and codelst_subtyp='active' ))
				 where pk_study =  v_study_id;
				 end if;
				  --find the count for study completed status
			 select count(*) into v_compl_cnt
				 from er_studystat
				 where fk_study=   v_study_id and
				  FK_CODELST_STUDYSTAT =
				(select pk_codelst
				 from er_codelst where codelst_type ='studystat'  and codelst_subtyp='prmnt_cls' );
				 if(v_compl_cnt = 0) then
				 update er_study set STUDY_END_DATE =  null
				  where pk_study =  v_study_id;
				  else
				   update er_study set STUDY_END_DATE  =
				  (select min(STUDYSTAT_DATE)
				  from er_studystat 	where  fk_study =  v_study_id
				 and FK_CODELST_STUDYSTAT = 	 (select pk_codelst  from er_codelst where codelst_type ='studystat'  and codelst_subtyp='prmnt_cls' ))
				 where pk_study =  v_study_id;
				 end if;
end if;-- right =0 ;*/
IF(v_pkstudysite = 0 ) THEN
 SELECT  SEQ_ER_STUDYSITE_RIGHTS.NEXTVAL INTO v_pk_studysite_rights FROM dual;
	INSERT INTO ER_STUDY_SITE_RIGHTS (PK_STUDY_SITE_RIGHTS,FK_SITE,FK_STUDY,FK_USER,USER_STUDY_SITE_RIGHTS,CREATOR,CREATED_ON,IP_ADD)
	VALUES(v_pk_studysite_rights,v_site_id,p_study_id,p_user,v_right,p_creator,SYSDATE,p_ipadd);
ELSE
        UPDATE ER_STUDY_SITE_RIGHTS
	    SET USER_STUDY_SITE_RIGHTS = v_right,
             last_modified_by = p_creator,
		   last_modified_date = SYSDATE,
		   ip_add = p_ipadd
	    WHERE PK_STUDY_SITE_RIGHTS = v_pkstudysite ;
END IF;
	 i := i+1;
   END LOOP;
   COMMIT;
   o_ret:=0;
END; --end of SP_UPDATESTUDYSITERIGHTS
------------------------
PROCEDURE SP_DELETE_SITE_AND_USERS(p_study_id NUMBER , p_acc_id NUMBER, p_site_id NUMBER, p_user number)
AS
/****************************************************************************************************
   ** Procedure delete study site and corresponding
   ** multiple records are updated
   ** Author: Anu Khanna 18th Nov 2004
   ** Input parameter: Array of pkStudySites
   ** Input parameter: Array of corresponding rights of each pkStudySites
   ** Input parameter: creator of record (userId)
   ** Input parameter: IP Address
   ** Output parameter: 0 for successful update, -1 for error
   **/
   cnt NUMBER;
   v_active_cnt NUMBER;
   v_compl_cnt NUMBER;
  BEGIN
  BEGIN
  	   	  plog.DEBUG(pctx,'SP_DELETE_SITE_AND_USERS - here to delete');

	  SELECT COUNT(*) INTO cnt
				 FROM ER_STUDYTEAM
	--JM: 08Nov2006
	--			 WHERE FK_STUDY = p_study_id AND NVL(STUDY_TEAM_USR_TYPE,'D') = 'D'
				 			 WHERE FK_STUDY = p_study_id AND STUDY_TEAM_USR_TYPE IN ('D','X')
				 AND fk_user IN (SELECT pk_user FROM ER_USER WHERE fk_account =  p_acc_id
				 AND fk_siteid = p_site_id);
				 plog.DEBUG(pctx,'SP_DELETE_SITE_AND_USERS - cnt' || cnt);

				 IF(cnt > 0) THEN

                                         --JM: 30Aug2010, #3765
                                         update ER_STUDY_SITE_RIGHTS set LAST_MODIFIED_BY = p_user
						 WHERE fk_user IN ( SELECT fk_user
						 FROM ER_STUDYTEAM
						 WHERE FK_STUDY = p_study_id AND STUDY_TEAM_USR_TYPE IN ('D','X')
						 AND fk_user IN (SELECT pk_user FROM ER_USER WHERE fk_account =  p_acc_id
						 AND fk_siteid = p_site_id))
						 AND fk_study = p_study_id;



                                                 -- delete all rights for that user
						 DELETE FROM ER_STUDY_SITE_RIGHTS
						 WHERE fk_user IN ( SELECT fk_user
						 FROM ER_STUDYTEAM
							--JM: 08Nov2006
							--			 WHERE FK_STUDY = p_study_id AND NVL(STUDY_TEAM_USR_TYPE,'D') = 'D'
						 WHERE FK_STUDY = p_study_id AND STUDY_TEAM_USR_TYPE IN ('D','X')
						 AND fk_user IN (SELECT pk_user FROM ER_USER WHERE fk_account =  p_acc_id
						 AND fk_siteid = p_site_id))
						 AND fk_study = p_study_id;
				 END IF;
				 -- delete site from study site


				 --JM: 30Aug2010, #3765
                                 Update ER_STUDYSITES set LAST_MODIFIED_BY = p_user
				 WHERE fk_study = p_study_id
				 AND fk_site = p_site_id;


                                 DELETE FROM ER_STUDYSITES
				 WHERE fk_study = p_study_id
				 AND fk_site = p_site_id;
				 plog.DEBUG(pctx,'SP_DELETE_SITE_AND_USERS - deleted study site stuyd' || p_study_id || 'site' || p_site_id);


				 	 IF(cnt > 0) THEN


                                --JM: 30Aug2010, #3765
                                Update ER_STUDYTEAM set LAST_MODIFIED_BY = p_user
				 WHERE fk_study = p_study_id
				 AND fk_user  IN(SELECT fk_user
				 FROM ER_STUDYTEAM
				 WHERE FK_STUDY = p_study_id AND STUDY_TEAM_USR_TYPE IN ('D','X')
				 AND fk_user IN (SELECT pk_user FROM ER_USER WHERE fk_account =  p_acc_id
				 AND fk_siteid = p_site_id))
				 AND STUDY_TEAM_USR_TYPE IN ('D','X');


                                 -- delete users from study team

				DELETE FROM ER_STUDYTEAM
				 WHERE fk_study = p_study_id
				 AND fk_user  IN(SELECT fk_user
				 FROM ER_STUDYTEAM
	--JM: 08Nov2006
	--			 WHERE FK_STUDY = p_study_id AND NVL(STUDY_TEAM_USR_TYPE,'D') = 'D'
				 WHERE FK_STUDY = p_study_id AND STUDY_TEAM_USR_TYPE IN ('D','X')
				 AND fk_user IN (SELECT pk_user FROM ER_USER WHERE fk_account =  p_acc_id
				 AND fk_siteid = p_site_id))
	--JM:
	--			 AND NVL(STUDY_TEAM_USR_TYPE, 'T') = 'D';
				 AND STUDY_TEAM_USR_TYPE IN ('D','X');
				  END IF;

                                 --JM: 30Aug2010, #3765
                                 Update ER_STUDYSTAT  set LAST_MODIFIED_BY = p_user
				 WHERE fk_study = p_study_id
				 AND fk_site = p_site_id;

                                 -- delete all status entered for that organization
				 DELETE FROM ER_STUDYSTAT
				 WHERE fk_study = p_study_id
				 AND fk_site = p_site_id;
				 --find the count for active enrolling status
				 SELECT COUNT(*) INTO v_active_cnt
					 FROM ER_STUDYSTAT
					 WHERE fk_study=  p_study_id AND
					 FK_CODELST_STUDYSTAT =
					(SELECT pk_codelst
					 FROM ER_CODELST WHERE codelst_type ='studystat'  AND codelst_subtyp='active' );
					 IF(v_active_cnt = 0) THEN
					 UPDATE ER_STUDY SET STUDY_ACTUALDT = NULL
					 WHERE pk_study = p_study_id;
					 ELSE
					  UPDATE ER_STUDY SET STUDY_ACTUALDT =
					  (SELECT MIN(STUDYSTAT_DATE)
					  FROM ER_STUDYSTAT 	WHERE  fk_study = p_study_id
					 AND FK_CODELST_STUDYSTAT = 	 (SELECT pk_codelst  FROM ER_CODELST WHERE codelst_type ='studystat'  AND codelst_subtyp='active' ))
					 WHERE pk_study = p_study_id;
					 END IF;
					  --find the count for study completed status
				 SELECT COUNT(*) INTO v_compl_cnt
					 FROM ER_STUDYSTAT
					 WHERE fk_study=  p_study_id AND
					  FK_CODELST_STUDYSTAT =
					(SELECT pk_codelst
					 FROM ER_CODELST WHERE codelst_type ='studystat'  AND codelst_subtyp='prmnt_cls' );
					 IF(v_compl_cnt = 0) THEN
					 UPDATE ER_STUDY SET STUDY_END_DATE =  NULL
					  WHERE pk_study = p_study_id;
					  ELSE
					   UPDATE ER_STUDY SET STUDY_END_DATE  =
					  (SELECT MIN(STUDYSTAT_DATE)
					  FROM ER_STUDYSTAT 	WHERE  fk_study = p_study_id
					 AND FK_CODELST_STUDYSTAT = 	 (SELECT pk_codelst  FROM ER_CODELST WHERE codelst_type ='studystat'  AND codelst_subtyp='prmnt_cls' ))
					 WHERE pk_study = p_study_id;
					 END IF;

					 plog.DEBUG(pctx,'SP_DELETE_SITE_AND_USERS - about to commit');
				 COMMIT;
		EXCEPTION WHEN OTHERS THEN
				  plog.fatal(pctx,'exception in deleting study site' || SQLERRM);
		END;
END;

-- Query modified by gopu to fix the bugzilla issue # 2609
PROCEDURE SP_UPDATE_START_END_DATES(p_study_id NUMBER, userId NUMBER )
AS
/****************************************************************************************************
   ** Procedure to update study end date and actual date
   ** multiple records are updated
   ** Author: Anu Khanna 7th Jan 05
   ** Input parameter: Pk of study
   **/
v_active_cnt NUMBER;
v_compl_cnt NUMBER;
BEGIN
  --find the count for active enrolling status
			 SELECT COUNT(*) INTO v_active_cnt
				 FROM ER_STUDYSTAT
				 WHERE fk_study=   p_study_id AND
				 FK_CODELST_STUDYSTAT =
				(SELECT pk_codelst
				 FROM ER_CODELST WHERE codelst_type ='studystat'  AND codelst_subtyp='active' );
				 IF(v_active_cnt = 0) THEN
				 -- Query modified by gopu to fix the bugzilla issue # 2609
				 UPDATE ER_STUDY SET STUDY_ACTUALDT = NULL, LAST_MODIFIED_BY = userId
				 WHERE pk_study =  p_study_id;
				  COMMIT;
				 ELSE
				  UPDATE ER_STUDY SET STUDY_ACTUALDT =
				  (SELECT MIN(STUDYSTAT_DATE)
				  FROM ER_STUDYSTAT 	WHERE  fk_study =  p_study_id
				 AND FK_CODELST_STUDYSTAT = 	 (SELECT pk_codelst  FROM ER_CODELST WHERE codelst_type ='studystat'  AND codelst_subtyp='active' )),
				 LAST_MODIFIED_BY = userId
				 WHERE pk_study =  p_study_id;
				  COMMIT;
				 END IF;
				  --find the count for study completed status
			 SELECT COUNT(*) INTO v_compl_cnt
				 FROM ER_STUDYSTAT
				 WHERE fk_study=   p_study_id AND
				  FK_CODELST_STUDYSTAT =
				(SELECT pk_codelst
				 FROM ER_CODELST WHERE codelst_type ='studystat'  AND codelst_subtyp='prmnt_cls' );
				-- if(v_compl_cnt = 0) then
				 /*update er_study set STUDY_END_DATE =  null
				  where pk_study =  v_study_id;		*/
				   IF(v_compl_cnt > 0) THEN
				   UPDATE ER_STUDY SET LAST_MODIFIED_BY = userId,STUDY_END_DATE  =
				  (SELECT MIN(STUDYSTAT_DATE)
				  FROM ER_STUDYSTAT 	WHERE  fk_study =  p_study_id
				 AND FK_CODELST_STUDYSTAT = 	 (SELECT pk_codelst  FROM ER_CODELST WHERE codelst_type ='studystat'  AND codelst_subtyp='prmnt_cls' ))
				 WHERE pk_study =  p_study_id;
				  COMMIT;
				 END IF;
				COMMIT;
				 END;

	 		FUNCTION f_chk_right_for_studysite(p_study NUMBER,p_user NUMBER,p_site NUMBER) RETURN NUMBER
					AS
					/* Function to check if user has a right in a site for a given study,
					   p_study study pk
					   p_user user pk
					   p_site site pk
					    returns 1 is user has rights
					    returns 0 if user does not have rights
						date - 03/29/05, by Sonia Abrol
					*/
					v_studyteam_count NUMBER;
					v_site_accesscount NUMBER := 0;
					v_account NUMBER;
					BEGIN


					    --main logic from StudySiteDao.getSitesForStatAndEnrolledPat()

					   SELECT COUNT(DISTINCT ss.fk_site )
					   INTO v_site_accesscount
			             FROM ER_STUDYSITES ss, ER_SITE
			             WHERE ss.fk_study = p_study AND ss.fk_site = p_site AND pk_site = ss.fk_site
			             AND ( EXISTS ( SELECT * FROM ER_STUDYTEAM T,ER_STUDY_SITE_RIGHTS r
			             			 WHERE T.fk_study = p_study AND T.fk_user = p_user AND NVL(study_team_usr_type,'Y') = 'D'
			             			 AND r.fk_study = T.fk_study AND
			             			 r.fk_user = T.fk_user AND   USER_STUDY_SITE_RIGHTS = 1 AND ss.fk_site = r.fk_site )
			             	OR ( pkg_superuser.F_Is_Superuser(p_user, p_study) = 1 )
			             	 AND EXISTS (SELECT * FROM ER_USERSITE u WHERE fk_user = p_user
			             	  AND u.fk_site = ss.fk_site AND USERSITE_RIGHT >= 4 AND NOT EXISTS (SELECT * FROM ER_STUDYTEAM TT
			             	  WHERE TT.fk_study = p_study AND TT.fk_user = p_user))
			             			 );

					       RETURN  v_site_accesscount;
					END;
	FUNCTION f_chk_right_for_patprotsite(p_patprot NUMBER,p_user NUMBER) RETURN NUMBER
					AS
					/* Function to check if user has a right in a patient's site for a given study patient enrollment,
					   p_patprot  patient enrollment pk
					   p_user user pk
					    returns 1 is user has rights
					    returns 0 if user does not have rights
						date - 03/29/05, by Sonia Abrol

		Modified by sonia abrol, 10/07/05, check patient facilities access rights too
					*/
					v_study NUMBER;
					v_pk_per NUMBER;
					v_pat_site  NUMBER;
					v_right NUMBER := 0;
	BEGIN
	BEGIN
			SELECT fk_study ,fk_per
			INTO v_study,v_pk_per
			FROM ER_PATPROT WHERE pk_patprot = p_patprot;

			/*SELECT fk_site
			INTO v_pat_site
			FROM ER_PER WHERE pk_per =v_pk_per;*/


			--get maximum right for each site patient is registered to
			SELECT MAX(f_chk_right_for_studysite(v_study ,p_user ,fk_site))
			INTO v_right
			FROM ER_PATFACILITY WHERE fk_per = v_pk_per AND patfacility_accessright > 0;


			 RETURN   v_right;
		EXCEPTION WHEN OTHERS THEN
				  RETURN 0;
		END;
	END;

	FUNCTION f_chk_studyright_using_pat(p_pat NUMBER,p_study NUMBER , p_user NUMBER) RETURN NUMBER
	AS
					/* Function to check if user has a right in a patient's site for a given study
					   p_patprot  patient enrollment pk
					   p_user user pk
						    returns a value greater than 0 if user has rights
					    returns 0 if user does not have rights
						date - 06/13/05, by Sonia Abrol
					*/
					v_pat_site  NUMBER;
					v_right NUMBER := 0;
	BEGIN
	BEGIN

			/*SELECT fk_site
			INTO v_pat_site
			FROM ER_PER WHERE pk_per =p_pat; */


			/**Modified by sonia abrol , 10/07/05, check for all patient sites*/

			SELECT MAX(f_chk_right_for_studysite(p_study ,p_user ,fk_site))
			INTO v_right
			FROM ER_PATFACILITY WHERE fk_per = p_pat AND patfacility_accessright > 0;


			/*SELECT f_chk_right_for_studysite(p_study ,p_user ,v_pat_site )
			INTO v_right
			FROM dual; */

		   RETURN   v_right;
		EXCEPTION WHEN OTHERS THEN
				  RETURN 0;
		END;
	END;


	FUNCTION f_right_forpatsites(p_pat NUMBER,p_user NUMBER) RETURN NUMBER
	AS
		/* Function to check if user has a right for a patient's site
			   p_pat patient pk
			   p_user user pk
				    returns a value greater than 0 if user has rights
			    returns 0 if user does not have rights
				date - 08/04/06, by Sonia Abrol
			*/
				v_right NUMBER := 0;
	BEGIN
		  BEGIN

		  	      SELECT MAX(u.usersite_right)
			   INTO v_right
			   FROM ER_PATFACILITY P , ER_USERSITE u
			   WHERE P.fk_per = p_pat AND P.patfacility_accessright > 0 AND
			   P.FK_SITE = u.fk_site AND u.fk_user = p_user AND
			   usersite_right > 0;


 		    RETURN   v_right;
			EXCEPTION WHEN NO_DATA_FOUND THEN
				   RETURN 0;

		END;


	END;

	/* added by sonia abrol, 12/21/06, to get user's right for anAdHoc query*/
FUNCTION f_getUserAdHocRight(p_user  NUMBER , p_pk_dynrep NUMBER) RETURN NUMBER
AS
  v_count NUMBER;
BEGIN

 SELECT COUNT(b.fk_object) INTO v_count
  FROM ER_OBJECTSHARE b
  WHERE b.object_number=2 AND  b.fk_object = p_pk_dynrep AND ( ( b.objectshare_type = 'U' AND (b.fk_objectshare_id= p_user)
 OR ( b.fk_objectshare_id IN ( SELECT pk_site FROM erv_usersites WHERE fk_user = p_user AND
 usersite_right > 0 UNION SELECT fk_siteid FROM ER_USER WHERE pk_user = p_user ) AND
 b.objectshare_type = 'O') ));

RETURN v_count;
END;

	PROCEDURE SP_deactivate_user
	(p_user IN NUMBER,
	p_creator IN VARCHAR2,
	p_ipadd IN VARCHAR2,
	o_ret OUT NUMBER)
	AS
	 V_DEACTIVE_STATUS NUMBER;
	 v_date DATE;
	BEGIN
	  -- get studyteam records

	 BEGIN

	 	SELECT pk_codelst INTO V_DEACTIVE_STATUS
	 	FROM ER_CODELST
	 	WHERE codelst_type = 'teamstatus'  AND codelst_subtyp = 'Deactivated';

	 	SELECT SYSDATE INTO v_date FROM dual;

	 	UPDATE ER_STATUS_HISTORY
	 	SET STATUS_end_DATE = v_date,last_modified_by = p_creator,last_modified_date = v_date ,ip_add=p_ipadd,STATUS_ISCURRENT=0
	 	WHERE STATUS_MODTABLE= 'er_studyteam' AND STATUS_end_DATE IS NULL AND
	 	EXISTS ( SELECT * FROM ER_STUDYTEAM WHERE fk_user = p_user AND  pk_studyteam = STATUS_MODPK AND STUDY_TEAM_USR_TYPE = 'D'
	    	AND NVL(studyteam_status,'Active') = 'Active' ) ;

        -- SA - to handle latest status and iscurrent discrepencies
         UPDATE ER_STATUS_HISTORY
	 	SET last_modified_by = p_creator,last_modified_date = v_date ,ip_add=p_ipadd,STATUS_ISCURRENT=0
	 	WHERE STATUS_MODTABLE= 'er_studyteam' AND STATUS_end_DATE IS NOT NULL AND STATUS_ISCURRENT = 1 AND
	 	EXISTS ( SELECT * FROM ER_STUDYTEAM WHERE fk_user = p_user AND  pk_studyteam = STATUS_MODPK AND STUDY_TEAM_USR_TYPE = 'D'
	    	AND NVL(studyteam_status,'Active') = 'Active' ) ;


	  	INSERT INTO ER_STATUS_HISTORY
		   (PK_STATUS, STATUS_MODPK,
		   STATUS_MODTABLE,
		   FK_CODELST_STAT,
		   STATUS_DATE,
		   status_notes,
		    CREATOR,
		    RECORD_TYPE,
		    CREATED_ON, IP_ADD, STATUS_ISCURRENT)
		 SELECT
		   seq_er_STATUS_HISTORY.NEXTVAL, pk_studyteam,
		    'er_studyteam',
		     V_DEACTIVE_STATUS,v_date, 'User Deactivated',p_creator, 'N', v_date, p_ipadd, '1'
		     FROM ER_STUDYTEAM WHERE fk_user = p_user AND  STUDY_TEAM_USR_TYPE = 'D'
	    	AND NVL(studyteam_status,'Active') = 'Active';

	    	UPDATE ER_STUDYTEAM
	    	SET studyteam_status = 'Deactivated',last_modified_by = p_creator,last_modified_date = v_date ,ip_add=p_ipadd,
	    	study_team_usr_type= 'X'
	    	WHERE fk_user = p_user AND  STUDY_TEAM_USR_TYPE = 'D'
	    	AND NVL(studyteam_status,'Active')  = 'Active';



     o_ret := 0;
      EXCEPTION WHEN OTHERS THEN
        o_ret := -1;
       -- raise ;
      END;
	  COMMIT;
	END;

PROCEDURE SP_NOTIFY_USER_INFO(p_word VARCHAR2,p_url VARCHAR2,p_eSign VARCHAR2,userId NUMBER,userLogin VARCHAR2,infoFlag VARCHAR2,o_ret OUT NUMBER)
IS

v_msgtemplate VARCHAR2(4000);
v_mail VARCHAR2(4000);
v_fparam VARCHAR2(1000);


BEGIN

begin

 IF(infoFlag = 'R') THEN

       v_msgtemplate   :=    Pkg_Common.SCH_GETMAILMSG('user_reset');

     v_fparam :=   p_word ||'~'|| p_eSign||'~'|| p_url ;

     v_mail :=    Pkg_Common.SCH_GETMAIL(v_msgtemplate,v_fparam);
       --plog.DEBUG(pCTX,'user_notify_reset_7thapril mail----'||v_mail);

    INSERT INTO SCH_DISPATCHMSG (PK_MSG ,MSG_SENDON ,MSG_STATUS  ,
                        MSG_TYPE    ,
                        MSG_TEXT,FK_PAT) values(SCH_DISPATCHMSG_SEQ1.NEXTVAL,SYSDATE ,0,'UR',
                        v_mail,userId);

   ELSIF(infoFlag = 'RP') THEN
            v_msgtemplate   :=    Pkg_Common.SCH_GETMAILMSG('reset_pass');

           v_fparam :=   p_word ||'~'|| p_url ;

        v_mail :=    Pkg_Common.SCH_GETMAIL(v_msgtemplate,v_fparam);
      -- plog.DEBUG(pCTX,'user_notify_reset_pass mail----'||v_mail);

        INSERT INTO SCH_DISPATCHMSG (PK_MSG ,MSG_SENDON ,MSG_STATUS  ,
                        MSG_TYPE    ,
                        MSG_TEXT,FK_PAT) values(SCH_DISPATCHMSG_SEQ1.NEXTVAL,SYSDATE ,0,'UR',
                        v_mail,userId);

   ELSIF(infoFlag = 'RE') THEN
            v_msgtemplate   :=    Pkg_Common.SCH_GETMAILMSG('user_esign');

           v_fparam :=  p_eSign||'~'|| p_url ;

        v_mail :=    Pkg_Common.SCH_GETMAIL(v_msgtemplate,v_fparam);
       --plog.DEBUG(pCTX,'user_notify_reset_esign mail----'||v_mail);

        INSERT INTO SCH_DISPATCHMSG (PK_MSG ,MSG_SENDON ,MSG_STATUS  ,
                        MSG_TYPE    ,
                        MSG_TEXT,FK_PAT) values(SCH_DISPATCHMSG_SEQ1.NEXTVAL,SYSDATE ,0,'UR',
                        v_mail,userId);

   ELSIF(infoFlag = 'P') THEN
            v_msgtemplate   :=    Pkg_Common.SCH_GETMAILMSG('user_pass');

           v_fparam :=   p_word ||'~'|| p_eSign||'~'|| p_url ;

        v_mail :=    Pkg_Common.SCH_GETMAIL(v_msgtemplate,v_fparam);
--plog.DEBUG(pCTX,'user_notify_reset_7thapril mail----'||v_mail);

        INSERT INTO SCH_DISPATCHMSG (PK_MSG ,MSG_SENDON ,MSG_STATUS  ,
                        MSG_TYPE    ,
                        MSG_TEXT,FK_PAT) values(SCH_DISPATCHMSG_SEQ1.NEXTVAL,SYSDATE ,0,'UN',
                        v_mail,userId);
   ELSE

             v_msgtemplate   :=    Pkg_Common.SCH_GETMAILMSG('user_id');

           v_fparam :=   userLogin ||'~'|| p_url ;

        --plog.DEBUG(pCTX,'user_notify_loginid_8thapril param----'||v_fparam);
        v_mail :=    Pkg_Common.SCH_GETMAIL(v_msgtemplate,v_fparam);
       --plog.DEBUG(pCTX,'user_notify_reset_7thapril mail----'||v_mail);

        INSERT INTO SCH_DISPATCHMSG (PK_MSG ,MSG_SENDON ,MSG_STATUS  ,
                        MSG_TYPE    ,
                        MSG_TEXT,FK_PAT) values(SCH_DISPATCHMSG_SEQ1.NEXTVAL,SYSDATE ,0,'UN',
                        v_mail,userId);

 END IF ;
  COMMIT;
  o_ret:=0;


 exception when others then

       o_ret:=-1;
       plog.fatal(pctx,'SP_BROADCAST_STUDY.SP_BROADCAST_STUDY:'||sqlerrm);


  end;

END SP_NOTIFY_USER_INFO;
----------------------------------------------------------------------------------------------------
PROCEDURE SP_ATTACH_USER_SITE_GROUPS(
    p_userid IN NUMBER ,
    p_fkSiteIds ARRAY_STRING,
    p_fkUserGrpIds ARRAY_STRING,
    p_pkDel ARRAY_STRING,
    p_ipadd   IN VARCHAR2,
    p_creator IN VARCHAR2,
    o_ret OUT NUMBER
  )
  AS
   v_cnt NUMBER;
   v_grpCnt NUMBER;
   v_pkDelCnt NUMBER;
   v_isGrpCnt NUMBER;
   i NUMBER;
   j NUMBER;
   v_pkusersite NUMBER;
   v_fkusergroup NUMBER;
   v_delPkValue NUMBER;
   v_fkusergroupArr split_tbl;
BEGIN
 v_cnt := p_fkSiteIds.COUNT();
 v_pkDelCnt := p_pkDel.COUNT();
-- Deletes the entry of the group that is removed from selection for a site.
  IF  (v_pkDelCnt > 0) THEN
   j:=1;
    WHILE j <= v_pkDelCnt LOOP
       v_delPkValue := TO_NUMBER(p_pkDel(j));
       DELETE FROM ER_USRSITE_GRP WHERE PK_USR_SITE_GRP=v_delPkValue;
      j := j+1;
    END LOOP;
  END IF;
 
   i:=1;
   WHILE i <= v_cnt LOOP
      v_pkusersite := TO_NUMBER(p_fkSiteIds(i));
	    SELECT pkg_util.f_split(p_fkUserGrpIds(i),',') into v_fkusergroupArr from dual;
      v_grpCnt := v_fkusergroupArr.COUNT();
        FOR j IN (SELECT COLUMN_VALUE AS fkGroupId FROM TABLE(v_fkusergroupArr))
         LOOP
             v_fkusergroup := j.fkGroupId;
             SELECT COUNT(*) INTO v_isGrpCnt FROM ER_USRSITE_GRP WHERE FK_USER=p_userid AND FK_USER_SITE=v_pkusersite AND FK_GRP_ID=v_fkusergroup;
             IF(v_isGrpCnt = 0) THEN
              if(v_fkusergroup > 0) then
              insert into ER_USRSITE_GRP(PK_USR_SITE_GRP,FK_GRP_ID,FK_USER_SITE,CREATOR,CREATED_ON,IP_ADD,FK_USER) values(SEQ_ER_USRSITE_GRP.nextval,v_fkusergroup,v_pkusersite,p_creator,sysdate,p_ipadd,p_userid);
              end if;
             END IF;
        END LOOP;
	 i := i+1;
   END LOOP;
 COMMIT;
 o_ret:=0;
  
END SP_ATTACH_USER_SITE_GROUPS;

----------------------------------------------------------------------------------------------------
END Pkg_User;
/


CREATE OR REPLACE SYNONYM ESCH.PKG_USER FOR PKG_USER;


CREATE OR REPLACE SYNONYM EPAT.PKG_USER FOR PKG_USER;


GRANT EXECUTE, DEBUG ON PKG_USER TO EPAT;

GRANT EXECUTE, DEBUG ON PKG_USER TO ESCH;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,158,2,'02_PKG_USER_BODY.sql',sysdate,'9.0.0 Build#615');

commit;