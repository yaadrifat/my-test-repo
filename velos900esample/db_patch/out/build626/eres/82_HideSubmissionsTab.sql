set define off;

update er_object_settings set object_visible = 0 where 
object_name like 'ctrp_tab' and object_subtype = 'ctrp_subm_tab';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,169,82,'82_HideSubmissionsTab.sql',sysdate,'9.0.0 Build#626');

commit;