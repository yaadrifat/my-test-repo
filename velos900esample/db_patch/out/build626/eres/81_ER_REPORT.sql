set define off;

delete from er_repxsl where pk_repxsl in (161,167,168,171,172);
commit;

 create or replace function f_lic_eligi_moddt(cordid number,status_type varchar2) 
  return varchar2
  is
  v_modi_dt varchar2(50);
  begin
   select TO_CHAR(MAX(CREATED_ON),'Mon DD, YYYY') into v_modi_dt FROM CB_ENTITY_STATUS WHERE ENTITY_ID = cordid AND STATUS_TYPE_CODE= status_type;
   return v_modi_dt;
end;
/

create or replace function f_getIdmDetails(cordId number) return SYS_REFCURSOR
IS
c_IdmDetails SYS_REFCURSOR;
BEGIN
open C_IDMDETAILS for
SELECT
        PL.TESTDATE TESTDATEVAL,
        LTEST.LABTEST_NAME LABTESTNAME,
        F_CODELST_DESC(PL.FK_TEST_OUTCOME) FK_TEST_OUTCOME,
        PL.CMS_APPROVED_LAB CMS_APPROVED_LAB,
        PL.FDA_LICENSED_LAB FDA_LICENSED_LAB,
        PL.ASSESSMENT_REMARKS ASSESSMENT_REMARKS,
        PL.FLAG_FOR_LATER FLAG_FOR_LATER,
        PL.ASSESSMENT_FOR_RESPONSE ASSESSMENT_FOR_RESPONSE,
        PL.TC_VISIBILITY_FLAG TC_VISIBILITY_FLAG,
        PL.CONSULTE_FLAG CONSULTE_FLAG,
        F_GETUSER(PL.ASSESSMENT_POSTEDBY) ASSESSMENT_POSTEDBY,
        TO_CHAR(PL.ASSESSMENT_POSTEDON,'Mon DD, YYYY') ASSESSMENT_POSTEDON,
        F_GETUSER(PL.ASSESSMENT_CONSULTBY) ASSESSMENT_CONSULTBY,
        TO_CHAR(PL.ASSESSMENT_CONSULTON,'Mon DD, YYYY') ASSESSMENT_CONSULTON,
        PL.SOURCEVAL SOURCEVALS,
        LTEST.PK_LABTEST LAB_TEST,
        DECODE(PL.FK_TEST_OUTCOME,
                '0','YES',
                '1','No',
                null,'') lastval
    from
        er_labtest ltest
    left outer join
        (
            select
                plabs.fk_testid fk_testid,
                plabs.fk_test_outcome fk_test_outcome,
                plabs.ft_test_source ft_test_source,
                plabs.cms_approved_lab cms_approved_lab,
                plabs.fda_licensed_lab fda_licensed_lab,
                f_codelst_desc(PLABS.FT_TEST_SOURCE) sourceval,
                to_char(plabs.test_date,
                'Mon DD, YYYY') testDate,
                plabs.custom003 CUSTOM003,
                ass.ASSESSMENT_REMARKS ASSESSMENT_REMARKS,
                ass.FLAG_FOR_LATER FLAG_FOR_LATER,
                ass.ASSESSMENT_FOR_RESPONSE ASSESSMENT_FOR_RESPONSE,
                ass.TC_VISIBILITY_FLAG TC_VISIBILITY_FLAG,
                ass.SENT_TO_REVIEW_FLAG SENT_TO_REVIEW_FLAG,
                ass.CONSULTE_FLAG CONSULTE_FLAG,
                ass.ASSESSMENT_POSTEDBY ASSESSMENT_POSTEDBY,
                ass.ASSESSMENT_POSTEDON ASSESSMENT_POSTEDON,
                ASS.ASSESSMENT_CONSULTBY ASSESSMENT_CONSULTBY,
                ass.ASSESSMENT_CONSULTON ASSESSMENT_CONSULTON
            from
                er_patlabs plabs
            left outer join
                cb_assessment ass
                    on(
                        ass.sub_entity_id=plabs.pk_patlabs
                    )
            where
                plabs.pk_patlabs in(
                    select
                        pk_patlabs
                    from
                        er_patlabs
                    WHERE
                        fk_specimen=(select CORD.FK_SPECIMEN_ID from cb_cord cord where cord.pk_cord=CORDID)
                )
            ) pl
                on(
                    ltest.pk_labtest=pl.fk_testid
                )
        left outer join
            er_labtestgrp ltestgrp
                on(
                    ltestgrp.fk_testid=ltest.pk_labtest
                )
        where
            ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    lg.group_type='I'
            )
            or ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    LG.GROUP_TYPE='IOG'
            )AND PL.FK_TESTID=LTEST.PK_LABTEST
            ORDER BY LAB_TEST;
RETURN C_IDMDETAILS;
END;
/

 
 CREATE OR REPLACE FORCE VIEW "ERES"."REP_CBU_DETAILS" AS 
  SELECT pk_cord,
    cord_registry_id,
    registry_maternal_id,
    cord_local_cbu_id,
    maternal_local_id,
    CORD_ISBI_DIN_CODE,
    b.site_name cbbid,
    b.site_id cbb_id,
    f_codelst_desc(fk_cbu_stor_loc) storage_loc,
    c.site_name cbu_collection_site,
    f_codelst_desc(fk_cord_cbu_eligible_status) eligible_status,
    f_codelst_desc(fk_cord_cbu_lic_status) lic_status,
    TO_CHAR(prcsng_start_date,'Mon DD,YYYY') prcsng_start_date,
    f_codelst_desc(fk_cord_bact_cul_result) bact_culture,
    bact_comment,
    TO_CHAR(frz_date,'Mon DD,YYYY')frz_date,
    f_codelst_desc(fk_cord_fungal_cul_result) fungal_culture,
    TO_CHAR(a.bact_cult_date,'Mon DD,YYYY') bact_cul_strt_dt,
    TO_CHAR(a.fung_cult_date,'Mon DD,YYYY') fung_cul_strt_dt,
    fung_comment,
    f_codelst_desc(fk_cord_abo_blood_type) abo_blood_type,
    f_codelst_desc(hemoglobin_scrn) hemoglobin_scrn,
    f_codelst_desc(fk_cord_rh_type) rh_type,  
    e.proc_name proc_name,
    to_char(e.proc_start_date,'Mon DD, YYYY') proc_start_date,
    to_char(e.proc_termi_date,'Mon DD, YYYY') proc_termi_date,
    f_codelst_desc(d.fk_proc_meth_id) processing,
    f_codelst_desc(d.fk_if_automated) automated_type,
    d.other_processing other_processing,
    f_codelst_desc(d.fk_product_modification) product_modification,
    d.other_prod_modi other_product_modi,  
    f_codelst_desc(d.fk_stor_method) storage_method,
    f_codelst_desc(d.fk_freez_manufac) freezer_manufact,
    d.other_freez_manufac other_freezer_manufact,
    f_codelst_desc(d.fk_frozen_in) frozen_in,
    d.other_frozen_cont other_frozen_cont,
    f_codelst_desc(d.fk_num_of_bags) no_of_bags,
    d.cryobag_manufac cryobag_manufac,
    f_codelst_desc(d.fk_bagtype) bagtype,
    f_codelst_desc(d.fk_bag_1_type) bag1type,
    f_codelst_desc(d.fk_bag_2_type) bag2type,
    d.other_bag_type other_bag,
    d.THOU_UNIT_PER_ML_HEPARIN_PER heparin_thou_per,
    d.THOU_UNIT_PER_ML_HEPARIN heparin_thou_ml,
    d.FIVE_UNIT_PER_ML_HEPARIN_PER heparin_five_per,
    d.FIVE_UNIT_PER_ML_HEPARIN heparin_five_ml,
    d.TEN_UNIT_PER_ML_HEPARIN_PER heparin_ten_per,
    d.TEN_UNIT_PER_ML_HEPARIN heparin_ten_ml,
    d.SIX_PER_HYDRO_STARCH_PER heparin_six_per,
    d.SIX_PER_HYDROXYETHYL_STARCH heparin_six_ml,
    d.CPDA_PER cpda_per,
    d.CPDA cpda_ml, 
    d.CPD_PER cpd_per,
    d.CPD cpd_ml,
    d.ACD_PER acd_per,
    d.ACD acd_ml,
    d.OTHER_ANTICOAGULANT_PER othr_anti_per,
    d.OTHER_ANTICOAGULANT othr_anti_ml,
    d.SPECIFY_OTH_ANTI speci_othr_anti,
    d.HUN_PER_DMSO_PER hun_dmso_per,
    d.HUN_PER_DMSO hun_dmso_ml,
    d.HUN_PER_GLYCEROL_PER hun_glycerol_per,
    d.HUN_PER_GLYCEROL hun_glycerol_ml,
    d.TEN_PER_DEXTRAN_40_PER ten_dextran_40_per,
    d.TEN_PER_DEXTRAN_40 ten_dextran_40_ml,
    d.FIVE_PER_HUMAN_ALBU_PER five_human_albu_per,
    d.FIVE_PER_HUMAN_ALBU five_human_albu_ml,  
    d.TWEN_FIVE_HUM_ALBU_PER  twentyfive_human_albu_per,
    d.TWEN_FIVE_HUM_ALBU twentyfive_human_albu_ml,
    d.PLASMALYTE_PER plasmalyte_per,
    d.PLASMALYTE plasmalyte_ml,
    d.OTH_CRYOPROTECTANT_PER othr_cryoprotectant_per,
    d.OTH_CRYOPROTECTANT othr_cryoprotectant_ml,
    d.SPEC_OTH_CRYOPRO spec_othr_cryoprotectant,
    d.FIVE_PER_DEXTROSE_PER five_dextrose_per,
    d.FIVE_PER_DEXTROSE five_dextrose_ml, 
    d.POINNT_NINE_PER_NACL_PER point_nine_nacl_per,
    d.POINNT_NINE_PER_NACL point_nine_nacl_ml,
    d.OTH_DILUENTS_PER othr_diluents_per, 
    d.OTH_DILUENTS othr_diluents_ml,
    d.SPEC_OTH_DILUENTS spec_othr_diluents,    
    PRODUCT_CODE productcode,
    f_codelst_desc(d.fk_stor_temp) storage_temperature, 
    d.max_value max_vol,
    f_codelst_desc(d.fk_contrl_rate_freezing) ctrl_rate_freezing,
    d.no_of_indi_frac individual_frac,
    d.filter_paper filter_paper,
    d.rbc_pallets rpc_pellets,
    d.no_of_extr_dna_aliquots extr_dna,
    d.no_of_serum_aliquots serum_aliquotes,
    d.no_of_plasma_aliquots plasma_aliquotes,
    d.no_of_nonviable_aliquots nonviable_aliquotes,
    d.no_of_viable_smpl_final_prod viable_samp_final_prod,
    d.no_of_segments no_of_segments,
    d.no_of_oth_rep_alliquots_f_prod no_of_oth_rep_alliquots_f_prod,
    d.no_of_oth_rep_alliq_alter_cond no_of_oth_rep_alliq_alter_cond,
    d.no_of_serum_mater_aliquots no_of_serum_mater_aliquots,
    d.no_of_plasma_mater_aliquots no_of_plasma_mater_aliquots,
    d.no_of_extr_dna_mater_aliquots no_of_extr_dna_mater_aliquots,
    d.no_of_cell_mater_aliquots NO_OF_CELL_MATER_ALIQUOTS,
    ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_INELIGIBLE_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD in ('
    ||a.pk_cord
    ||')',',') INELIGIBLEREASON,
    ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_UNLICENSED_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD in ('
    ||a.pk_cord
    ||')',',') UNLICENSEREASON,
    a.CORD_ELIGIBLE_ADDITIONAL_INFO ELIGCOMMENTS,
    f_lic_eligi_moddt(a.pk_cord, 'eligibility') ELGIBLE_MODI_DT,
    f_lic_eligi_moddt(a.pk_cord, 'licence') LIC_MODI_DT,
     DECODE(a.FK_CORD_CBU_ELIGIBLE_STATUS,
    (f_codelst_id('eligibility','eligible')),'0',
    (f_codelst_id('eligibility','ineligible')),'1',
    (f_codelst_id('eligibility','incomplete')),'2',
    (f_codelst_id('eligibility','not_appli_prior')),'3') ELI_FLAG,
    DECODE(a.FK_CORD_CBU_LIC_STATUS,
    (f_codelst_id('licence','licensed')),'0',
    (f_codelst_id('licence','unlicensed')),'1') LIC_FLAG
  FROM cb_cord a 
  left outer join er_site b on (a.fk_cbb_id = b.pk_site)
  left outer join er_site c on (a.fk_cbu_coll_site = c.pk_site)
  left outer join cbb_processing_procedures_info d on (a.fk_cbb_procedure = d.fk_processing_id)
  left outer join cbb_processing_procedures e on (a.fk_cbb_procedure = e.pk_proc );
commit; 

Update ER_REPORT set REP_SQL = 'SELECT PK_CORD, CORD_REGISTRY_ID, REGISTRY_MATERNAL_ID, CORD_LOCAL_CBU_ID, MATERNAL_LOCAL_ID, CORD_ISBI_DIN_CODE,
CBBID, STORAGE_LOC, CBU_COLLECTION_SITE,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''cbu_info''),''1'')cbu_info_notes, ELIGIBLE_STATUS,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''eligiblity''),''1'') eligibility_notes,
prcsng_start_date, bact_culture,bact_cul_strt_dt, bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''lab_sum''),''1'') labsummary_notes, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
HEPARIN_THOU_PER,HEPARIN_THOU_ML,HEPARIN_FIVE_PER,HEPARIN_FIVE_ML,HEPARIN_TEN_PER,HEPARIN_TEN_ML,HEPARIN_SIX_PER,HEPARIN_SIX_ML,CPDA_PER,
cpda_ml,cpd_per,cpd_ml,acd_per,acd_ml,othr_anti_per,othr_anti_ml,speci_othr_anti,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''id''),''1'')ids_notes,
hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,
FIVE_HUMAN_ALBU_ML,TWENTYFIVE_HUMAN_ALBU_PER,TWENTYFIVE_HUMAN_ALBU_ML, PLASMALYTE_PER,PLASMALYTE_ML,OTHR_CRYOPROTECTANT_PER,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hla''),''1'') hla_notes,(select f_lab_summary(~1) from dual) lab_summary,
five_dextrose_ml,filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,
OTHR_DILUENTS_ML,SPEC_OTHR_DILUENTS,PRODUCTCODE,NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
NO_OF_EXTR_DNA_MATER_ALIQUOTS, NO_OF_CELL_MATER_ALIQUOTS, (SELECT F_HLA_CORD(~1) FROM DUAL) HLA,BACT_CUL_STRT_DT,FUNG_CUL_STRT_DT,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''cbu_info_cat''))) CBU_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''hla''))) HLA_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''eligibile_cat''))) ELIGIBLE_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''lab_sum_cat''))) LABSUMMARY_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''process_info''))) PROCESSINFO_ATTACHMENTS,
F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''infect_mark''))) IDM_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''hlth_scren''))) HHS_ATTACHMENTS,CBB_ID,F_CORD_ID_ON_BAG(~1) IDONBAG, F_CORD_ADD_IDS(~1) ADDIDS,
INELIGIBLEREASON,UNLICENSEREASON,ELGIBLE_MODI_DT,LIC_MODI_DT,LIC_FLAG,ELI_FLAG,ELIGCOMMENTS,F_GETIDMDETAILS(~1) IDMDETAILS,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1'') IDM_NOTES,F_CBU_HISTORY(~1) CBUHISTORY,F_CBU_REQ_HISTORY(~1,~2) CBUREQHISTORY,
DECODE((SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),
        NULL,''0'',
        (SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),''1'')ordertypedata
FROM
 REP_CBU_DETAILS
WHERE
 PK_CORD = ~1' where pk_report = 168;
	COMMIT;

Update ER_REPORT set REP_SQL_CLOB = 'SELECT PK_CORD, CORD_REGISTRY_ID, REGISTRY_MATERNAL_ID, CORD_LOCAL_CBU_ID, MATERNAL_LOCAL_ID, CORD_ISBI_DIN_CODE,
CBBID, STORAGE_LOC, CBU_COLLECTION_SITE,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''cbu_info''),''1'')cbu_info_notes, ELIGIBLE_STATUS,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''eligiblity''),''1'') eligibility_notes,
prcsng_start_date, bact_culture,bact_cul_strt_dt, bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''lab_sum''),''1'') labsummary_notes, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
HEPARIN_THOU_PER,HEPARIN_THOU_ML,HEPARIN_FIVE_PER,HEPARIN_FIVE_ML,HEPARIN_TEN_PER,HEPARIN_TEN_ML,HEPARIN_SIX_PER,HEPARIN_SIX_ML,CPDA_PER,
cpda_ml,cpd_per,cpd_ml,acd_per,acd_ml,othr_anti_per,othr_anti_ml,speci_othr_anti,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''id''),''1'')ids_notes,
hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,
FIVE_HUMAN_ALBU_ML,TWENTYFIVE_HUMAN_ALBU_PER,TWENTYFIVE_HUMAN_ALBU_ML, PLASMALYTE_PER,PLASMALYTE_ML,OTHR_CRYOPROTECTANT_PER,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hla''),''1'') hla_notes,(select f_lab_summary(~1) from dual) lab_summary,
five_dextrose_ml,filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,
OTHR_DILUENTS_ML,SPEC_OTHR_DILUENTS,PRODUCTCODE,NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
NO_OF_EXTR_DNA_MATER_ALIQUOTS, NO_OF_CELL_MATER_ALIQUOTS, (SELECT F_HLA_CORD(~1) FROM DUAL) HLA,BACT_CUL_STRT_DT,FUNG_CUL_STRT_DT,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''cbu_info_cat''))) CBU_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''hla''))) HLA_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''eligibile_cat''))) ELIGIBLE_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''lab_sum_cat''))) LABSUMMARY_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''process_info''))) PROCESSINFO_ATTACHMENTS,
F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''infect_mark''))) IDM_ATTACHMENTS,F_GET_ATTACHMENTS(~1,(F_CODELST_ID(''doc_categ'',''hlth_scren''))) HHS_ATTACHMENTS,CBB_ID,F_CORD_ID_ON_BAG(~1) IDONBAG, F_CORD_ADD_IDS(~1) ADDIDS,
INELIGIBLEREASON,UNLICENSEREASON,ELGIBLE_MODI_DT,LIC_MODI_DT,LIC_FLAG,ELI_FLAG,ELIGCOMMENTS,F_GETIDMDETAILS(~1) IDMDETAILS,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1'') IDM_NOTES,F_CBU_HISTORY(~1) CBUHISTORY,F_CBU_REQ_HISTORY(~1,~2) CBUREQHISTORY,
DECODE((SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),
        NULL,''0'',
        (SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),''1'')ordertypedata
FROM
 REP_CBU_DETAILS
WHERE
 PK_CORD = ~1' where pk_report = 168;
	COMMIT;

Update ER_REPORT set REP_SQL = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''id''),''1,0'')ids_notes, cbbid, storage_loc, cbu_collection_site, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''cbu_info''),''1,0'')cbu_info_notes,
(select f_hla_cord(~1) from dual) hla, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hla''),''1,0'') hla_notes, eligible_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''eligiblity''),''1,0'') eligibility_notes,
ineligiblereason,unlicensereason,elgible_modi_dt,lic_modi_dt,lic_flag,eli_flag,eligcomments,prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''lab_sum''),''1,0'') labsummary_notes, (select f_lab_summary(~1) from dual) lab_summary, proc_name, proc_start_date, proc_termi_date, processing, automated_type,
other_processing, product_modification, other_product_modi, storage_method, freezer_manufact, other_freezer_manufact,
FROZEN_IN, OTHER_FROZEN_CONT, NO_OF_BAGS, CRYOBAG_MANUFAC, BAGTYPE, BAG1TYPE, BAG2TYPE,   OTHER_BAG,
storage_temperature, max_vol, ctrl_rate_freezing, individual_frac,CBB_ID,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,
NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots, f_getidmdetails(~1) idmdetails,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1,0'') idm_notes, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''cbu_info_cat''))) cbu_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''hla''))) hla_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''eligibile_cat''))) eligible_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''infect_mark''))) idm_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''process_info''))) proinfo_attachments,f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsummary_attachments,
F_CBU_HISTORY(~1) CBUHISTORY,F_CBU_REQ_HISTORY(~1,~2) CBUREQHISTORY, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''process_info''),''1,0'') processinfo_notes,
DECODE((SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),NULL,''0'',(SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),''1'') ordertypedata
FROM rep_cbu_details where pk_cord = ~1' where pk_report = 167;
	COMMIT;

Update ER_REPORT set REP_SQL_CLOB = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''id''),''1,0'')ids_notes, cbbid, storage_loc, cbu_collection_site, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''cbu_info''),''1,0'')cbu_info_notes,
(select f_hla_cord(~1) from dual) hla, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hla''),''1,0'') hla_notes, eligible_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''eligiblity''),''1,0'') eligibility_notes,
ineligiblereason,unlicensereason,elgible_modi_dt,lic_modi_dt,lic_flag,eli_flag,eligcomments,prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''lab_sum''),''1,0'') labsummary_notes, (select f_lab_summary(~1) from dual) lab_summary, proc_name, proc_start_date, proc_termi_date, processing, automated_type,
other_processing, product_modification, other_product_modi, storage_method, freezer_manufact, other_freezer_manufact,
FROZEN_IN, OTHER_FROZEN_CONT, NO_OF_BAGS, CRYOBAG_MANUFAC, BAGTYPE, BAG1TYPE, BAG2TYPE,   OTHER_BAG,
storage_temperature, max_vol, ctrl_rate_freezing, individual_frac,CBB_ID,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,
NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots, f_getidmdetails(~1) idmdetails,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1,0'') idm_notes, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''cbu_info_cat''))) cbu_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''hla''))) hla_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''eligibile_cat''))) eligible_attachments,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''infect_mark''))) idm_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''process_info''))) proinfo_attachments,f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsummary_attachments,
F_CBU_HISTORY(~1) CBUHISTORY,F_CBU_REQ_HISTORY(~1,~2) CBUREQHISTORY, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''process_info''),''1,0'') processinfo_notes,
DECODE((SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),NULL,''0'',(SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),''1'') ordertypedata
FROM rep_cbu_details where pk_cord = ~1' where pk_report = 167;
	COMMIT;

Update ER_REPORT set REP_SQL ='SELECT
  '''' TESTDATEVAL,
  '''' LABTESTNAME,
  '''' FK_TEST_OUTCOME,
  1 CMS_APPROVED_LAB,
  1 FDA_LICENSED_LAB,
  '''' ASSESSMENT_REMARKS,
  '''' FLAG_FOR_LATER,
  1  ASSESSMENT_FOR_RESPONSE,
  ''''  TC_VISIBILITY_FLAG,
  ''''  CONSULTE_FLAG,
  ''''  ASSESSMENT_POSTEDBY,
  ''''  ASSESSMENT_POSTEDON,
  ''''  ASSESSMENT_CONSULTBY,
  '''' ASSESSMENT_CONSULTON,
  CRD.CORD_ISBI_DIN_CODE ISTBTDIN,
  CRD.CORD_REGISTRY_ID REGID,
  CRD.CORD_LOCAL_CBU_ID LOCALCBUID,
  F_CORD_ADD_IDS(~1) ADDIDS,
  F_CORD_ID_ON_BAG(~1) IDSONBAG,
  SITE.SITE_NAME SITENAME,
  SITE.SITE_ID SITEID,
  '''' SOURCEVAL,
  1 LAB_TEST,
  '''' lastval
FROM
  CB_CORD CRD,
  er_site site
WHERE
  CRD.FK_CBB_ID=SITE.PK_SITE AND CRD.PK_CORD=~1
union all
SELECT
        PL.TESTDATE,
        ltest.labtest_name,
        f_codelst_desc(pl.FK_TEST_OUTCOME),
        pl.CMS_APPROVED_LAB,
        pl.FDA_LICENSED_LAB,
        pl.ASSESSMENT_REMARKS,
        PL.FLAG_FOR_LATER,
        pl.ASSESSMENT_FOR_RESPONSE,
        PL.TC_VISIBILITY_FLAG,
        PL.CONSULTE_FLAG,
        F_GETUSER(PL.ASSESSMENT_POSTEDBY),
        to_char(PL.ASSESSMENT_POSTEDON,''Mon DD, YYYY''),
        F_GETUSER(PL.ASSESSMENT_CONSULTBY),
        to_char(PL.ASSESSMENT_CONSULTON,''Mon DD, YYYY''),
        '''',
        '''',
        '''',
        F_CORD_ADD_IDS(~1) ADDIDS,
        F_CORD_ID_ON_BAG(~1) IDSONBAG,
        '''',
        '''',
        PL.SOURCEVAL,
        ltest.pk_labtest lab_test,
        DECODE(PL.FK_TEST_OUTCOME,
                ''0'',''YES'',
                ''1'',''No'',
                null,'''')
    from
        er_labtest ltest
    left outer join
        (
            select
                plabs.fk_testid fk_testid,
                plabs.fk_test_outcome fk_test_outcome,
                plabs.ft_test_source ft_test_source,
                plabs.cms_approved_lab cms_approved_lab,
                plabs.fda_licensed_lab fda_licensed_lab,
                f_codelst_desc(PLABS.FT_TEST_SOURCE) sourceval,
                to_char(plabs.test_date,
                ''Mon DD, YYYY'') testDate,
                plabs.custom003 CUSTOM003,
                ass.ASSESSMENT_REMARKS ASSESSMENT_REMARKS,
                ass.FLAG_FOR_LATER FLAG_FOR_LATER,
                ass.ASSESSMENT_FOR_RESPONSE ASSESSMENT_FOR_RESPONSE,
                ass.TC_VISIBILITY_FLAG TC_VISIBILITY_FLAG,
                ass.SENT_TO_REVIEW_FLAG SENT_TO_REVIEW_FLAG,
                ass.CONSULTE_FLAG CONSULTE_FLAG,
                ass.ASSESSMENT_POSTEDBY ASSESSMENT_POSTEDBY,
                ass.ASSESSMENT_POSTEDON ASSESSMENT_POSTEDON,
                ass.ASSESSMENT_CONSULTBY ASSESSMENT_CONSULTBY,
                ass.ASSESSMENT_CONSULTON ASSESSMENT_CONSULTON
            from
                er_patlabs plabs
            left outer join
                cb_assessment ass
                    on(
                        ass.sub_entity_id=plabs.pk_patlabs
                    )
            where
                plabs.pk_patlabs in(
                    select
                        pk_patlabs
                    from
                        er_patlabs
                    WHERE
                        fk_specimen=(select CORD.FK_SPECIMEN_ID from cb_cord cord where cord.pk_cord=~1)
                )
            ) pl
                on(
                    ltest.pk_labtest=pl.fk_testid
                )
        left outer join
            er_labtestgrp ltestgrp
                on(
                    ltestgrp.fk_testid=ltest.pk_labtest
                )
        where
            ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    lg.group_type=''I''
            )
            or ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    LG.GROUP_TYPE=''IOG''
            )AND PL.FK_TESTID=LTEST.PK_LABTEST
            ORDER BY LAB_TEST' where pk_report = 172;
commit;

Update ER_REPORT set REP_SQL_CLOB ='SELECT
  '''' TESTDATEVAL,
  '''' LABTESTNAME,
  '''' FK_TEST_OUTCOME,
  1 CMS_APPROVED_LAB,
  1 FDA_LICENSED_LAB,
  '''' ASSESSMENT_REMARKS,
  '''' FLAG_FOR_LATER,
  1  ASSESSMENT_FOR_RESPONSE,
  ''''  TC_VISIBILITY_FLAG,
  ''''  CONSULTE_FLAG,
  ''''  ASSESSMENT_POSTEDBY,
  ''''  ASSESSMENT_POSTEDON,
  ''''  ASSESSMENT_CONSULTBY,
  '''' ASSESSMENT_CONSULTON,
  CRD.CORD_ISBI_DIN_CODE ISTBTDIN,
  CRD.CORD_REGISTRY_ID REGID,
  CRD.CORD_LOCAL_CBU_ID LOCALCBUID,
  F_CORD_ADD_IDS(~1) ADDIDS,
  F_CORD_ID_ON_BAG(~1) IDSONBAG,
  SITE.SITE_NAME SITENAME,
  SITE.SITE_ID SITEID,
  '''' SOURCEVAL,
  1 LAB_TEST,
  '''' lastval
FROM
  CB_CORD CRD,
  er_site site
WHERE
  CRD.FK_CBB_ID=SITE.PK_SITE AND CRD.PK_CORD=~1
union all
SELECT
        PL.TESTDATE,
        ltest.labtest_name,
        f_codelst_desc(pl.FK_TEST_OUTCOME),
        pl.CMS_APPROVED_LAB,
        pl.FDA_LICENSED_LAB,
        pl.ASSESSMENT_REMARKS,
        PL.FLAG_FOR_LATER,
        pl.ASSESSMENT_FOR_RESPONSE,
        PL.TC_VISIBILITY_FLAG,
        PL.CONSULTE_FLAG,
        F_GETUSER(PL.ASSESSMENT_POSTEDBY),
        to_char(PL.ASSESSMENT_POSTEDON,''Mon DD, YYYY''),
        F_GETUSER(PL.ASSESSMENT_CONSULTBY),
        to_char(PL.ASSESSMENT_CONSULTON,''Mon DD, YYYY''),
        '''',
        '''',
        '''',
        F_CORD_ADD_IDS(~1) ADDIDS,
        F_CORD_ID_ON_BAG(~1) IDSONBAG,
        '''',
        '''',
        PL.SOURCEVAL,
        ltest.pk_labtest lab_test,
        DECODE(PL.FK_TEST_OUTCOME,
                ''0'',''YES'',
                ''1'',''No'',
                null,'''')
    from
        er_labtest ltest
    left outer join
        (
            select
                plabs.fk_testid fk_testid,
                plabs.fk_test_outcome fk_test_outcome,
                plabs.ft_test_source ft_test_source,
                plabs.cms_approved_lab cms_approved_lab,
                plabs.fda_licensed_lab fda_licensed_lab,
                f_codelst_desc(PLABS.FT_TEST_SOURCE) sourceval,
                to_char(plabs.test_date,
                ''Mon DD, YYYY'') testDate,
                plabs.custom003 CUSTOM003,
                ass.ASSESSMENT_REMARKS ASSESSMENT_REMARKS,
                ass.FLAG_FOR_LATER FLAG_FOR_LATER,
                ass.ASSESSMENT_FOR_RESPONSE ASSESSMENT_FOR_RESPONSE,
                ass.TC_VISIBILITY_FLAG TC_VISIBILITY_FLAG,
                ass.SENT_TO_REVIEW_FLAG SENT_TO_REVIEW_FLAG,
                ass.CONSULTE_FLAG CONSULTE_FLAG,
                ass.ASSESSMENT_POSTEDBY ASSESSMENT_POSTEDBY,
                ass.ASSESSMENT_POSTEDON ASSESSMENT_POSTEDON,
                ass.ASSESSMENT_CONSULTBY ASSESSMENT_CONSULTBY,
                ass.ASSESSMENT_CONSULTON ASSESSMENT_CONSULTON
            from
                er_patlabs plabs
            left outer join
                cb_assessment ass
                    on(
                        ass.sub_entity_id=plabs.pk_patlabs
                    )
            where
                plabs.pk_patlabs in(
                    select
                        pk_patlabs
                    from
                        er_patlabs
                    WHERE
                        fk_specimen=(select CORD.FK_SPECIMEN_ID from cb_cord cord where cord.pk_cord=~1)
                )
            ) pl
                on(
                    ltest.pk_labtest=pl.fk_testid
                )
        left outer join
            er_labtestgrp ltestgrp
                on(
                    ltestgrp.fk_testid=ltest.pk_labtest
                )
        where
            ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    lg.group_type=''I''
            )
            or ltestgrp.fk_labgroup = (
                select
                    lg.pk_labgroup
                from
                    er_labgroup lg
                where
                    LG.GROUP_TYPE=''IOG''
            )AND PL.FK_TESTID=LTEST.PK_LABTEST
            ORDER BY LAB_TEST' where pk_report = 172;
commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,169,81,'81_ER_REPORT.sql',sysdate,'9.0.0 Build#626');

commit;
