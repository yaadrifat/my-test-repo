Set Define Off;

Update er_object_settings set object_displaytext = 'CBB Processing Procedures' where object_name = 'cbb' and object_subtype='cbb_proc';

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,139,4,'04_ER_OBJECT_SETTINGS_UPDATE.sql',sysdate,'9.0.0 Build#596');

commit;