Create table CB_CORD_IMPORT_MESSAGES
(
PK_CORD_IMPORT_MESSAGES NUMBER(10) Primary Key,
MESSAGE_ID NUMBER(10),
MESSAGE_TYPE varchar2(50 BYTE),
MESSAGE_TEXT varchar2(200 CHAR),
CBU_REGISTRY_ID varchar2(50 BYTE),
FK_CORD_IMPORT_HISTORY_ID NUMBER(10),
FK_CORD_ID NUMBER(10),
MESSAGE_LEVEL varchar2(50 BYTE),
CORD_PROGRESS NUMBER(5),
CREATOR NUMBER(10),
CREATED_ON DATE,
IP_ADD VARCHAR2(15),
LAST_MODIFIED_BY NUMBER(10),
LAST_MODIFIED_DATE DATE,
RID NUMBER(10),
DELETEDFLAG VARCHAR2(1)
);

 COMMENT ON COLUMN "CB_CORD_IMPORT_MESSAGES"."PK_CORD_IMPORT_MESSAGES" IS 'Primary Key';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_MESSAGES"."MESSAGE_ID" IS 'Message id of the cord import process';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_MESSAGES"."MESSAGE_TYPE" IS 'type of the message for mentaining the cord import process';
  
 COMMENT ON COLUMN "CB_CORD_IMPORT_MESSAGES"."MESSAGE_TEXT" IS 'message description of the cord import';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_MESSAGES"."CBU_REGISTRY_ID" IS 'cbu registry id of the processed cord during cord import';
   
 COMMENT ON COLUMN "CB_CORD_IMPORT_MESSAGES"."FK_CORD_IMPORT_HISTORY_ID" IS 'fk of the cord import history';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_MESSAGES"."FK_CORD_ID" IS 'store the pk of cord';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_MESSAGES"."MESSAGE_LEVEL" IS 'Message level of the cord import history id ';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_MESSAGES"."CORD_PROGRESS" IS 'Store the cord progress';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_MESSAGES"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_MESSAGES"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_MESSAGES"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_MESSAGES"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_MESSAGES"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_MESSAGES"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_MESSAGES"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';

 COMMENT ON TABLE "CB_CORD_IMPORT_MESSAGES"  IS 'This table stores message of the processed cord during cord import.';
 
 CREATE SEQUENCE SEQ_CB_CORD_IMPORT_MESSAGES MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;
 
 Create table CB_CORD_IMPORT_HISTORY
(
PK_CORD_IMPORT_HISTORY NUMBER(10) Primary Key,
IMPORT_HISTORY_ID varchar2(50 BYTE),
START_TIME TIMESTAMP,
END_TIME TIMESTAMP,
IMPORT_HISTORY_DESC varchar2(200 CHAR),
IMPORT_FILE BLOB,
IMPORT_FILE_NAME VARCHAR2(50 char),
CREATOR NUMBER(10),
CREATED_ON DATE,
IP_ADD VARCHAR2(15),
LAST_MODIFIED_BY NUMBER(10),
LAST_MODIFIED_DATE DATE,
RID NUMBER(10),
DELETEDFLAG VARCHAR2(1)
);

 COMMENT ON COLUMN "CB_CORD_IMPORT_HISTORY"."PK_CORD_IMPORT_HISTORY" IS 'Primary Key';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_HISTORY"."IMPORT_HISTORY_ID" IS 'History id of the cord import process';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_HISTORY"."START_TIME" IS 'stores the start time of cord import';
  
 COMMENT ON COLUMN "CB_CORD_IMPORT_HISTORY"."END_TIME" IS 'store the end time of the cord import';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_HISTORY"."IMPORT_HISTORY_DESC" IS 'store the description of the cord import';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_HISTORY"."IMPORT_FILE" IS 'store the cord import file';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_HISTORY"."IMPORT_FILE_NAME" IS 'store the cord import file NAME';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_HISTORY"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_HISTORY"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_HISTORY"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_HISTORY"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_HISTORY"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_HISTORY"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';
 
 COMMENT ON COLUMN "CB_CORD_IMPORT_HISTORY"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';

 COMMENT ON TABLE "CB_CORD_IMPORT_HISTORY"  IS 'This table stores history of cord import.';
 
 CREATE SEQUENCE SEQ_CB_CORD_IMPORT_HISTORY MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1;
 
 INSERT INTO track_patches
 VALUES(seq_track_patches.nextval,176,11,'11_cord_import.sql',sysdate,'9.0.0 Build#633');

 commit;
