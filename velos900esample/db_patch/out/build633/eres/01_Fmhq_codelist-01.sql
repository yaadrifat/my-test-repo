-- INSERTING CODE LIST VALUES FOR FMHQ FORM RESPONSE TYPE 1--

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res1','yes','Yes','N',1,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res1','no','No','N',2,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res1','unknown','Unknown','N',3,null,null,null,null,sysdate,sysdate,null,null,null,null);

--END--

-- INSERTING CODE LIST VALUES FOR FMHQ FORM UNEXPECTED RESPONSE TYPE 1--

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_unres1','no','No','N',1,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_unres1','unknown','Unknown','N',2,null,null,null,null,sysdate,sysdate,null,null,null,null);

--END--

-- INSERTING CODE LIST VALUES FOR FMHQ FORM UNEXPECTED RESPONSE TYPE 2--

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_unres2','yes','Yes','N',1,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_unres2','unknown','Unknown','N',2,null,null,null,null,sysdate,sysdate,null,null,null,null);

--END--

-- INSERTING CODE LIST VALUES FOR FMHQ FORM UNEXPECTED RESPONSE TYPE 3--

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_unres3','unknown','Unknown','N',1,null,null,null,null,sysdate,sysdate,null,null,null,null);

--END--

-- INSERTING CODE LIST VALUES FOR FMHQ FORM RESPONSE TYPE 2--

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res2','babymom','Baby''s Mother','N',1,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res2','babydad','Baby''s Father','N',2,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res2','babysib','Baby''s Sibling','N',3,null,null,null,null,sysdate,sysdate,null,null,null,null);

--END--

-- INSERTING CODE LIST VALUES FOR FMHQ FORM  CANCER TYPE --

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'cncr_type','brnevcncr','Brain or other nervous system cancer','N',1,null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'cncr_type','brncncr','Bone or joint cancer','N',2,null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'cncr_type','kdycncr','Kidney (incl. renal pelvic) cancer','N',3,null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'cncr_type','thycncr','Thyroid cancer','N',4,null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'cncr_type','hodgcncr','Hodgkin''s lymphoma','N',5,null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'cncr_type','nonhodgcncr','Non-Hodgkin''s lymphoma','N',6,null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'cncr_type','acmleuk','Acute or chronic myelongenous/myeloid leukemia', 'N',7,null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'cncr_type','aclleuk','Acute or chronic lymphocytic/lymphoblastic leukemia', 'N',8,null,null,null,null,sysdate, sysdate,null,null,null,null);


Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'cncr_type','skincncr','Skin Cancer', 'N',9,null,null,null,null,sysdate, sysdate,null,null,null,null);


Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'cncr_type','otskincncr','Other cancer/leukemia', 'N',10,null,null,null,null,sysdate, sysdate,null,null,null,null);

--END--

-- INSERTING CODE LIST VALUES FOR FMHQ FORM RESPONSE TYPE 3--

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res3','babymom','Baby''s Mother','N',1,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res3','babydad','Baby''s Father','N',2,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res3','babysib','Baby''s Sibling','N',3,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res3','babygpa','Baby''s Grandparent','N',4,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res3','babymomsib','Baby''s Mother''s Sibling','N',5,null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res3','babydadsib','Baby''s Father''s Sibling','N',6,null,null,null,null,sysdate, sysdate,null,null,null,null);

--END--

-- INSERTING CODE LIST VALUES FOR FMHQ FORM White Blood Cell Disease--

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'wbc_dis','chonicdis','Chronic Granulomatous Disease','N',1,null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'wbc_dis','kostsyn','Kostmann Syndrome','N',2,null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'wbc_dis','schdiasyn','Schwachman-Diamond Syndrome','N',3,null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'wbc_dis','lad','Leukocyte Adhesion Deficiency (LAD)','N',4,null,null,null,null,sysdate, sysdate,null,null,null,null);

--END--


-- INSERTING CODE LIST VALUES FOR FMHQ FORM RED Blood Cell Disease--

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'rbc_dis','diablsyn','Diamond-Blackfan Syndrome','N',1,null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'rbc_dis','ellipto','Elliptocytosis','N',2,null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'rbc_dis','gpdrc','G6PD or other red cell enzyme deficiency','N',3,null,null,null,null, sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'rbc_dis','sphero','Spherocytosis','N',4,null,null,null,null,sysdate, sysdate,null,null,null,null);

--END--

-- INSERTING CODE LIST VALUES FOR FMHQ FORM RESPONSE TYPE 4--

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res4','yes','Yes','N',1,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res4','no','No','N',2,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res4','notask','Not Asked','N',3,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res4','unknown','Unknown','N',4,null,null,null,null,sysdate,sysdate,null,null,null,null);

--END--

-- INSERTING CODE LIST VALUES FOR FMHQ FORM IMMUNE DEFICIENCY --

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'immune_def','adaorp','ADA or PNP Deficiency','N',1,null,null,null,null,sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'immune_def','cis','Combined Immundodeficiency Syndrome (CIS), Common Variable ','N',2, null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'immune_def','cvid','Immun odeficiency Disease (CVID)','N',3,null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'immune_def','digsyn','DiGeorge Syndrome','N',4,null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'immune_def','hleInfel','Hereditary Hemophagocytic Lymphohistiocytosis (HLH) incl FEL','N',5, null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'immune_def','hypoglob','Hypoglobulinemia','N',6, null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'immune_def','nezelhoff','Nezelhoff Syndrome','N',7, null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'immune_def','scid','SCID','N',8, null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'immune_def','wisAld','Wiskott-Aldrich','N',9, null,null,null,null,sysdate, sysdate,null,null,null,null);

-- END --

-- INSERTING CODE LIST VALUES FOR FMHQ FORM Platelet Disease --

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'plate_dis','amega','Amegakaryocytic Thrombocytopenia','N',1, null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'plate_dis','glanz','Glanzmann Thrombasthenia','N',2, null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'plate_dis','heredi','Hereditary Thrombocytopenia','N',3, null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'plate_dis','pspdis','Platelet Storage Pool Disease','N',4, null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'plate_dis','tar','Thrombocytopenia with absent radii (TAR)','N',5, null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'plate_dis','ataTelan','Ataxia-Telangiectasia','N',6, null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'plate_dis','fanAne','Fanconi Anemia','N',6, null,null,null,null,sysdate, sysdate,null,null,null,null);
-- END --

-- INSERTING CODE LIST VALUES FOR FMHQ FORM RESPONSE TYPE 5--

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res5','yes','Yes','N',1,null,null,null,null,sysdate,sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_res5','no','No','N',2,null,null,null,null,sysdate,sysdate,null,null,null,null);

-- END --

-- INSERTING CODE LIST VALUES FOR FMHQ FORM UNEXPECTED RESPONSE TYPE 4--

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'fmhq_unres4','yes','Yes','N',1,null,null,null,null,sysdate,sysdate,null,null,null,null);

-- END --

-- INSERTING CODE LIST VALUES FOR FMHQ FORM Metabolic/Storage Disease--

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'metab_dis','mps1','Hurler Syndrome (MPS I)','N',1,null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'metab_dis','mps2','Hurler-Scheie Syndrome (MPS I H-S)','N',2,null,null,null,null,sysdate, sysdate,null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'metab_dis','hunSYn','Hunter Syndrome (MPS II)','N',3,null,null,null,null,sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'metab_dis','mps3','Sanfilippo Syndrome (MPS III)','N',4,null,null,null,null,sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'metab_dis','mps4','Morquio Syndrome (MPS IV)','N',5,null,null,null,null,sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'metab_dis','mps6','Maroteaux-Larny Syndrome (MPS VI)','N',6,null,null,null,null,sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'metab_dis','mps7','Sly Syndrome (MPS VII)','N',7,null,null,null,null,sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'metab_dis','icell','I-cell disease','N',8,null,null,null,null,sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'metab_dis','globoid','Globoid Leukodystrophy (Krabbe Disease)','N',9,null,null,null,null, sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'metab_dis','mld','Metachromatic Leukodystrophy (MLD)','N',10,null,null,null,null, sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'metab_dis','ald','Adrenoleukodystrophy (ALD)','N',11,null,null,null,null, sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'metab_dis','sandhoff','Sandhoff Disease','N',12,null,null,null,null, sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'metab_dis','taysachs','Tay-Sachs Disease','N',13,null,null,null,null, sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'metab_dis','gaucher','Gaucher Disease','N',14,null,null,null,null, sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'metab_dis','niemann','Niemann-Pick Disease','N',15,null,null,null,null, sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'metab_dis','porphy','Porphyria','N',16,null,null,null,null, sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'metab_dis','storgdis','Metabolic/Storage Disease (other or unknown type)','N',17,null,null, null,null,sysdate,sysdate, null,null,null,null);


-- END --

-- INSERTING CODE LIST VALUES FOR FMHQ FORM SEVERE AUTOIMMUNE DISORDER --

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'serv_dis','crohndis','Crohn''s Disease or Ulcerative Colitis','N',1,null,null,null,null, sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'serv_dis','lupus','Lupus','N',2,null,null,null,null, sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'serv_dis','ms','Multiple Sclerosis (MS)','N',3,null,null,null,null, sysdate,sysdate, null,null,null,null);

Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'serv_dis','rheumatoid','Rheumatoid Arthritis','N',4,null,null,null,null, sysdate,sysdate, null,null,null,null);

-- END --
 
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,176,1,'01_Fmhq_codelist-01.sql',sysdate,'9.0.0 Build#633');

commit;