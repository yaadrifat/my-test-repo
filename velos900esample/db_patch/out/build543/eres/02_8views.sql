
--1st View--

DROP VIEW ERV_ALL_PATIENT_LABS;

/* Formatted on 2/9/2010 1:39:10 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_ALL_PATIENT_LABS
(
   PATIENT_ID,
   PATIENT_STUDY_ID,
   LAB_DATE,
   LAB_NAME,
   RESULT,
   LONG_TEST_RESULT,
   UNIT,
   LLN,
   ULN,
   ACCESSION_NUM,
   TOXICITY,
   LAB_STATUS,
   RESULT_TYPE,
   STUDY_NUMBER,
   STUDY_PHASE,
   NOTES,
   FK_PER,
   CREATED_ON,
   FK_ACCOUNT,
   PTLAB_PAT_NAME,
   PTLAB_CATEGORY,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   PTLAB_RESPONSE_ID,
   FK_STUDY
)
AS
   SELECT   per_code AS patient_id,
            patprot_patstdid AS patient_study_id,
            TRUNC (test_date) AS lab_date,
            labtest_name AS lab_name,
            test_result AS result,
            test_result_tx AS long_test_result,
            test_unit AS unit,
            test_lln AS lln,
            test_uln AS uln,
            accession_num,
            er_name || NVL2 (er_name, ' - ', '') || er_grade AS toxicity,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_tststat)
               AS lab_status,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_abflag)
               AS result_type,
            NVL ( (SELECT   study_number
                     FROM   ER_STUDY
                    WHERE   pk_study = a.fk_study), 'No Study Specified')
               AS study_number,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_stdphase)
               AS study_phase,
            notes,
            a.fk_per,
            a.created_on,
            fk_account,
            (SELECT   PERSON_FNAME || ' ' || PERSON_LNAME
               FROM   PERSON
              WHERE   PK_PERSON = PK_PER)
               PTLAB_PAT_NAME,
            (SELECT   GROUP_NAME
               FROM   ER_LABGROUP
              WHERE   PK_LABGROUP = FK_TESTGROUP)
               PTLAB_CATEGORY,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            A.LAST_MODIFIED_DATE,
            A.RID,
            PK_PATLABS PTLAB_RESPONSE_ID,
            pp.fk_study
     FROM   ER_PATPROT pp,
            ER_PATLABS a,
            ER_LABTEST,
            ER_PER
    WHERE       patprot_stat = 1
            AND pk_per = pp.fk_per
            AND pp.fk_per = a.fk_per
            AND Pk_labtest = fk_testid;
COMMENT ON TABLE ERV_ALL_PATIENT_LABS IS 'view for patient labs data';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.PATIENT_ID IS 'Patient code for a facility';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.PATIENT_STUDY_ID IS 'Patients study id';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.LAB_DATE IS 'Date of the lab test';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.LAB_NAME IS 'lab test name';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.RESULT IS 'Lab result';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.LONG_TEST_RESULT IS 'Lab test result in LONG format';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.UNIT IS 'lab test unit';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.LLN IS 'lab test LLN';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.ULN IS 'lab test ULN';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.ACCESSION_NUM IS 'Accession number';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.TOXICITY IS 'Toxicity linked with the test';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.LAB_STATUS IS 'lab test status';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.RESULT_TYPE IS 'lab test result type';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.STUDY_NUMBER IS 'Study number of the study linked with the lab test (if any)';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.STUDY_PHASE IS 'Study Phase of the study linked with the lab test (if any)';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.NOTES IS 'Notes for patient labs';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.FK_PER IS 'Patient Primary Key';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.CREATED_ON IS 'The date patient lab test record was created';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.FK_ACCOUNT IS 'Primary key of account labtest is linked with';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.PTLAB_PAT_NAME IS 'Patient Name';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.PTLAB_CATEGORY IS 'Lab test category';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.CREATOR IS 'The user who created the patient labs record';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.LAST_MODIFIED_BY IS 'The user who last modified the patient labs record';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.LAST_MODIFIED_DATE IS 'The date patient lab test record was last modified';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.RID IS 'Database internal RID (used for audit trail)';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.PTLAB_RESPONSE_ID IS 'Primary key of er_patlabs';

COMMENT ON COLUMN ERV_ALL_PATIENT_LABS.FK_STUDY IS 'Study PK of the study linked with the lab test (if any)';


--2nd View--

DROP VIEW ERES.ERV_PATIENT_LABS;

/* Formatted on 4/6/2009 4:21:30 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERES.ERV_PATIENT_LABS
(
   PATIENT_ID,
   PATIENT_STUDY_ID,
   LAB_DATE,
   LAB_NAME,
   RESULT,
   LONG_TEST_RESULT,
   UNIT,
   LLN,
   ULN,
   ACCESSION_NUM,
   TOXICITY,
   LAB_STATUS,
   RESULT_TYPE,
   STUDY_NUMBER,
   STUDY_PHASE,
   NOTES,
   FK_PER,
   CREATED_ON,
   FK_ACCOUNT,
   PTLAB_PAT_NAME,
   PTLAB_CATEGORY,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   PTLAB_RESPONSE_ID,
   FK_STUDY
)
AS
   SELECT   per_code AS patient_id,
            (SELECT   patprot_patstdid
               FROM   ER_PATPROT pp
              WHERE       pp.fk_study = a.fk_study
                      AND pp.fk_per = a.fk_per
                      AND patprot_stat = 1
                      AND ROWNUM < 2)
               AS patient_study_id,
            TRUNC (test_date) AS lab_date,
            labtest_name AS lab_name,
            test_result AS result,
            test_result_tx AS long_test_result,
            test_unit AS unit,
            test_lln AS lln,
            test_uln AS uln,
            accession_num,
            er_name || NVL2 (er_name, ' - ', '') || er_grade AS toxicity,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_tststat)
               AS lab_status,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_abflag)
               AS result_type,
            (SELECT   study_number
               FROM   ER_STUDY
              WHERE   pk_study = a.fk_study)
               AS study_number,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_stdphase)
               AS study_phase,
            notes,
            a.fk_per,
            a.created_on,
            fk_account,
            (SELECT   PERSON_FNAME || ' ' || PERSON_LNAME
               FROM   PERSON
              WHERE   PK_PERSON = PK_PER)
               PTLAB_PAT_NAME,
            (SELECT   GROUP_NAME
               FROM   ER_LABGROUP
              WHERE   PK_LABGROUP = FK_TESTGROUP)
               PTLAB_CATEGORY,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            A.LAST_MODIFIED_DATE,
            A.RID,
            PK_PATLABS PTLAB_RESPONSE_ID,
            a.fk_study
     FROM   ER_PATLABS a, ER_LABTEST, ER_PER
    WHERE   pk_per = fk_per AND Pk_labtest = fk_testid;


CREATE OR REPLACE SYNONYM ESCH.ERV_PATIENT_LABS FOR ERES.ERV_PATIENT_LABS;


CREATE OR REPLACE SYNONYM EPAT.ERV_PATIENT_LABS FOR ERES.ERV_PATIENT_LABS;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERES.ERV_PATIENT_LABS TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERES.ERV_PATIENT_LABS TO ESCH;

--3rd View--

DROP VIEW ERES.ERV_PATIENT_MORE_DETAILS;

/* Formatted on 4/6/2009 4:17:11 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERES.ERV_PATIENT_MORE_DETAILS
(
   FIELD_NAME,
   FIELD_VALUE,
   FK_PER,
   CREATED_ON,
   FK_ACCOUNT,
   PTMPD_PAT_ID,
   PTMPD_PAT_NAME,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   PTMPD_RESPONSE_ID
)
AS
   SELECT   (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_idtype)
               AS field_name,
            perid_id AS field_value,
            fk_per,
            a.CREATED_ON,
            fk_account,
            PER_CODE PTMPD_PAT_ID,
            (SELECT   PERSON_FNAME || ' ' || PERSON_LNAME
               FROM   PERSON
              WHERE   PK_PER = PK_PERSON)
               PTMPD_PAT_NAME,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            a.LAST_MODIFIED_DATE,
            A.RID,
            PK_PERID PTMPD_RESPONSE_ID
     FROM   pat_perid a, ER_PER
    WHERE   pk_per = fk_per;


CREATE OR REPLACE SYNONYM ESCH.ERV_PATIENT_MORE_DETAILS FOR ERES.ERV_PATIENT_MORE_DETAILS;


CREATE OR REPLACE SYNONYM EPAT.ERV_PATIENT_MORE_DETAILS FOR ERES.ERV_PATIENT_MORE_DETAILS;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERES.ERV_PATIENT_MORE_DETAILS TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERES.ERV_PATIENT_MORE_DETAILS TO ESCH;


--4th View--

DROP VIEW ERES.ERV_PATIENT_PROTOCOLS;

/* Formatted on 4/6/2009 3:03:58 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERES.ERV_PATIENT_PROTOCOLS
(
   PATIENT_ID,
   PATIENT_STUDY_ID,
   STUDY_NUMBER,
   STUDY_TITLE,
   PATPROT_ENROLDT,
   FK_PER,
   CREATED_ON,
   FK_ACCOUNT,
   PTPRO_AGE,
   PTPRO_GENDER,
   PTPRO_PAT_NAME,
   PTPRO_SITE,
   PTPRO_ASSTO,
   PTPRO_TX_LOCAT,
   PTPRO_RANDOM_NUM,
   PTPRO_ENROL_BY,
   PTPRO_PHYSICIAN,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   PTPRO_RESPONSE_ID,
   FK_STUDY,
   ENROLLING_SITE,
   TREATING_ORG,
   EVAL_FLAG,
   EVAL_STATUS,
   INEVAL_STATUS,
   SURVIVAL_STATUS,
   PERSON_DEATHDT,
   DEATH_CAUSE,
   DEATH_CAUSE_OTHER,
   DEATH_STUDY_RELATED,
   DEATH_STUDY_RELATED_OTHER
)
AS
   SELECT   person_code patient_id,
            patprot_patstdid patient_study_id,
            study_number,
            study_title,
            patprot_enroldt,
            fk_per,
            ER_PATPROT.CREATED_ON,
            PERSON.fk_account,
            ROUND ( (ROUND (SYSDATE - PERSON_DOB)) / 365) AS PTPRO_AGE,
            F_GET_CODELSTDESC (FK_CODELST_GENDER) PTPRO_GENDER,
            PERSON_FNAME || ' ' || PERSON_LNAME PTPRO_PAT_NAME,
            (SELECT   SITE_NAME
               FROM   ER_SITE
              WHERE   PK_SITE = FK_SITE)
               PTPRO_SITE,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = FK_USERASSTO)
               PTPRO_ASSTO,
            F_GET_CODELSTDESC (FK_CODELSTLOC) PTPRO_TX_LOCAT,
            PATPROT_RANDOM PTPRO_RANDOM_NUM,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = FK_USER)
               PTPRO_ENROL_BY,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = PATPROT_PHYSICIAN)
               PTPRO_PHYSICIAN,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_PATPROT.CREATOR)
               creator,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = ER_PATPROT.LAST_MODIFIED_BY)
               LAST_MODIFIED_BY,
            ER_PATPROT.LAST_MODIFIED_DATE,
            ER_PATPROT.RID,
            PK_PATPROT PTPRO_RESPONSE_ID,
            fk_study,
            (SELECT   SITE_NAME
               FROM   ER_SITE
              WHERE   PK_SITE = FK_SITE_ENROLLING)
               ENROLLING_SITE,
            (SELECT   SITE_NAME
               FROM   ER_SITE
              WHERE   PK_SITE = patprot_treatingorg)
               TREATING_ORG,
            F_GET_CODELSTDESC (FK_CODELST_PTST_EVAL_FLAG) EVAL_FLAG,
            F_GET_CODELSTDESC (FK_CODELST_PTST_EVAL) EVAL_STATUS,
            F_GET_CODELSTDESC (FK_CODELST_PTST_INEVAL) INEVAL_STATUS,
            F_GET_CODELSTDESC (FK_CODELST_PTST_SURVIVAL) SURVIVAL_STATUS,
            person_deathdt,
            F_GET_CODELSTDESC (FK_CODELST_PAT_DTH_CAUSE) DEATH_CAUSE,
            CAUSE_OF_DEATH_OTHER DEATH_CAUSE_OTHER,
            F_GET_CODELSTDESC (FK_CODELST_PTST_DTH_STDREL)
               AS DEATH_STUDY_RELATED,
            DEATH_STD_RLTD_OTHER AS DEATH_STUDY_RELATED_OTHER
     FROM   PERSON, ER_PATPROT, ER_STUDY
    WHERE   PK_PERSON = fk_per AND pk_study = fk_study AND patprot_stat = 1;


CREATE OR REPLACE SYNONYM ESCH.ERV_PATIENT_PROTOCOLS FOR ERES.ERV_PATIENT_PROTOCOLS;




CREATE OR REPLACE SYNONYM EPAT.ERV_PATIENT_PROTOCOLS FOR ERES.ERV_PATIENT_PROTOCOLS;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERES.ERV_PATIENT_PROTOCOLS TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERES.ERV_PATIENT_PROTOCOLS TO ESCH;


--5th View--


DROP VIEW ERES.ERV_PATIENT_STUDY_STATUS;

/* Formatted on 4/6/2009 4:13:57 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERES.ERV_PATIENT_STUDY_STATUS
(
   PATIENT_ID,
   PATIENT_STUDY_ID,
   STUDY_NUMBER,
   STUDY_TITLE,
   STATUS,
   STATUS_DATE,
   REASON,
   NOTES,
   ASSIGNED_TO,
   PHYSICIAN,
   TREATMENT_LOCATION,
   RAND_NUMBER,
   ENROLLED_BY,
   SCREEN_NUMBER,
   SCREENED_BY,
   SCREENING_OUTCOME,
   FK_PER,
   PK_STUDY,
   CREATED_ON,
   FK_CODELST_PTST_EVAL_FLAG,
   FK_CODELST_PTST_EVAL,
   FK_CODELST_PTST_INEVAL,
   FK_CODELST_PTST_SURVIVAL,
   FK_CODELST_PTST_DTH_STDREL,
   DEATH_STD_RLTD_OTHER,
   DATE_OF_DEATH,
   FK_CODELST_PAT_DTH_CAUSE,
   CAUSE_OF_DEATH_OTHER,
   FK_ACCOUNT,
   PSTAT_ENROLL_SITE,
   PSTAT_NEXT_FU_DATE,
   PSTAT_IC_VERS_NUM,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   PSTAT_RESPONSE_ID,
   CURRENT_STAT,
   PATPROT_TREATINGORG,
   FK_SITE_ENROLLING
)
AS
   SELECT   per_code patient_id,
            patprot_patstdid patient_study_id,
            study_number,
            study_title,
            f_get_codelstdesc (fk_codelst_stat) status,
            TRUNC (patstudystat_date) status_date,
            f_get_codelstdesc (patstudystat_reason) reason,
            patstudystat_note notes,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = fk_userassto)
               assigned_to,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = patprot_physician)
               physician,
            f_get_codelstdesc (fk_codelstloc) treatment_location,
            patprot_random rand_number,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = fk_user)
               enrolled_by,
            screen_number,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = screened_by)
               screened_by,
            f_get_codelstdesc (screening_outcome) screening_outcome,
            y.fk_per,
            pk_study,
            y.created_on,
            f_get_codelstdesc (x.fk_codelst_ptst_eval_flag)
               fk_codelst_ptst_eval_flag,
            f_get_codelstdesc (x.fk_codelst_ptst_eval) fk_codelst_ptst_eval,
            f_get_codelstdesc (x.fk_codelst_ptst_ineval)
               fk_codelst_ptst_ineval,
            f_get_codelstdesc (x.fk_codelst_ptst_survival)
               fk_codelst_ptst_survival,
            f_get_codelstdesc (x.fk_codelst_ptst_dth_stdrel)
               fk_codelst_ptst_dth_stdrel,
            x.death_std_rltd_other,
            x.date_of_death,
            f_get_codelstdesc ( (SELECT   fk_codelst_pat_dth_cause
                                   FROM   person
                                  WHERE   pk_person = pk_per))
               fk_codelst_pat_dth_cause,
            (SELECT   cause_of_death_other
               FROM   person
              WHERE   pk_person = pk_per)
               cause_of_death_other,
            er_per.fk_account,
            (SELECT   site_name
               FROM   er_site
              WHERE   pk_site = fk_site_enrolling)
               pstat_enroll_site,
            y.next_followup_on pstat_next_fu_date,
            y.inform_consent_ver pstat_ic_vers_num,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = y.creator)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   er_user
              WHERE   pk_user = y.last_modified_by)
               last_modified_by,
            y.last_modified_date,
            y.rid,
            pk_patstudystat pstat_response_id,
            f_get_yesno (current_stat) current_stat,
            patprot_treatingorg,
            fk_site_enrolling
     FROM   er_per,
            er_patprot x,
            er_study,
            er_patstudystat y
    WHERE       pk_per = x.fk_per
            AND x.patprot_stat = 1
            AND pk_study = x.fk_study
            AND pk_per = x.fk_per
            AND pk_study = y.fk_study
            AND pk_per = y.fk_per;


CREATE OR REPLACE SYNONYM ESCH.ERV_PATIENT_STUDY_STATUS FOR ERES.ERV_PATIENT_STUDY_STATUS;

CREATE OR REPLACE  SYNONYM EPAT.ERV_PATIENT_STUDY_STATUS FOR ERES.ERV_PATIENT_STUDY_STATUS;


GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERES.ERV_PATIENT_STUDY_STATUS TO EPAT;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON ERES.ERV_PATIENT_STUDY_STATUS TO ESCH;


--6th View--

DROP VIEW REP_ADV_MORE_DETAILS;

/* Formatted on 4/6/2009 3:43:59 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_ADV_MORE_DETAILS
(
   PK_MOREDETAILS,
   FIELD_NAME,
   FIELD_VALUE,
   FK_PER,
   CREATED_ON,
   FK_ACCOUNT,
   PAT_ID,
   PAT_NAME,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   RESPONSE_ID,
   ADVE_TYPE,
   ADVE_NAME,
   AE_STDATE,
   STUDY_NUMBER,
   FK_STUDY
)
AS
   SELECT   PK_MOREDETAILS,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = MD_MODELEMENTPK)
               field_name,
            MD_MODELEMENTDATA AS field_value,
            pk_person,
            a.CREATED_ON,
            fk_account,
            PERson_CODE PAT_ID,
            (PERSON_FNAME || ' ' || PERSON_LNAME) PAT_NAME,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            a.LAST_MODIFIED_DATE,
            A.RID,
            PK_MOREDETAILS RESPONSE_ID,
            (SELECT   codelst_desc
               FROM   SCH_CODELST
              WHERE   pk_codelst = fk_codlst_aetype)
               adve_type,
            ae_name,
            ae_stdate,
            (SELECT   study_number
               FROM   er_study
              WHERE   pk_study = fk_study)
               study_number,
            fk_study
     FROM   er_moredetails a, person, sch_adverseve
    WHERE       a.MD_MODNAME = 'advtype'
            AND FK_MODPK = pk_adveve
            AND pk_person = fk_per;
COMMENT ON TABLE REP_ADV_MORE_DETAILS IS 'view for adverse events more details';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.PAT_ID IS 'Patient Code for a facility';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.PAT_NAME IS 'Patient Name';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.CREATOR IS 'The user who created the record';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.LAST_MODIFIED_BY IS 'The user who last modified the record';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.LAST_MODIFIED_DATE IS 'The date record was last modified on';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.RID IS 'Database internal RID (used for audit trail)';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.RESPONSE_ID IS 'Primary Key of er_moredetails';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.ADVE_TYPE IS 'Adverse Event type';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.ADVE_NAME IS 'Adverse Event name';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.AE_STDATE IS 'Adverse Event Start Date';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.STUDY_NUMBER IS 'Study number of the study linked with the lab test (if any)';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.FK_STUDY IS 'Study PK of the study linked with the lab test (if any)';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.PK_MOREDETAILS IS 'Primary Key of er_moredetails';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.FIELD_NAME IS 'More Details Field Name';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.FIELD_VALUE IS 'More Details Field Value';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.FK_PER IS 'FK to ER_PER. Identifies the patient';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.CREATED_ON IS 'The date record was created';

COMMENT ON COLUMN REP_ADV_MORE_DETAILS.FK_ACCOUNT IS 'FK to er_account. Identifies the account the record is linked with';


--7th View--


DROP VIEW REP_USER_MORE_DETAILS;

/* Formatted on 4/6/2009 3:17:38 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW REP_USER_MORE_DETAILS
(
   PK_MOREDETAILS,
   FIELD_NAME,
   FIELD_VALUE,
   CREATED_ON,
   CREATOR,
   LAST_MODIFIED_BY,
   LAST_MODIFIED_DATE,
   RID,
   RESPONSE_ID,
   USER_NAME,
   FK_ACCOUNT
)
AS
   SELECT   pk_moredetails,
            (SELECT   codelst_desc
               FROM   er_codelst
              WHERE   pk_codelst = MD_MODELEMENTPK)
               field_name,
            MD_MODELEMENTDATA AS field_value,
            a.CREATED_ON CREATED_ON,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.CREATOR)
               creator,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = A.LAST_MODIFIED_BY)
               last_modified_by,
            a.LAST_MODIFIED_DATE LAST_MODIFIED_DATE,
            A.RID,
            PK_MOREDETAILS RESPONSE_ID,
            u.USR_FIRSTNAME || ' ' || u.USR_LASTNAME AS USER_NAME,
            u.fk_account AS fk_account
     FROM   er_moredetails a, er_user u
    WHERE   a.MD_MODNAME = 'user' AND fk_modpk = u.pk_user;


--8th View--


DROP VIEW ERV_SPECIMEN_STORAGE;

/* Formatted on 2/9/2010 1:39:33 PM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW ERV_SPECIMEN_STORAGE
(
   FK_ACCOUNT,
   PK_STORAGE,
   STORAGE_ID,
   STORAGE_NAME,
   STORAGE_ALTERNATEID,
   STORAGE_TYPE,
   CHILD_STORAGE_TYPE,
   PARENT_STORAGE_ID,
   PARENT_STORAGE,
   STORAGE_FK_STUDY,
   STORAGE_STUDY_NUMBER,
   STORAGE_STATUS,
   STORAGE_STATUS_DATE,
   STORAGE_TRACKING_USER,
   STORAGE_TRACKING_NO,
   STORAGE_RID,
   STORAGE_CREATOR,
   STORAGE_CREATED_ON,
   STORAGE_LAST_MODIFIED_BY,
   STORAGE_LAST_MODIFIED_DATE,
   PK_SPECIMEN,
   SPEC_ID,
   SPEC_ALTERNATE_ID,
   SPEC_DESCRIPTION,
   SPEC_TYPE,
   SPEC_COLLECTION_DATE,
   EXP_SPEC_QTY,
   EXP_QTY_UNITS,
   COLL_SPEC_QTY,
   COLL_QTY_UNITS,
   SPEC_QTY,
   SPEC_QTY_UNITS,
   OWNER,
   SPEC_FK_STUDY,
   SPEC_STUDY_NUMBER,
   SPEC_ORGN,
   SPEC_ANATOMIC_SITE,
   SPEC_TISSUE_SIDE,
   SPEC_PATHOLOGY_STAT,
   PARENT_SPEC_ID,
   SPEC_STATUS,
   SPEC_STATUS_DATE,
   SPEC_STATUS_QTY,
   SPEC_STATUS_QTY_UNITS,
   SPEC_STATUS_ACTION,
   SPEC_PROCESSING_TYPE,
   SPEC_REMOVAL_TIME,
   SPEC_FREEZE_TIME,
   RECEPIENT,
   PATHOLOGIST,
   SURGEON,
   COLLECTING_TECHNICIAN,
   PROCESSING_TECHNICIAN,
   SPEC_TRACKING_NO,
   SPEC_CREATOR,
   SPEC_CREATED_ON,
   SPEC_LAST_MODIFIED_BY,
   SPEC_LAST_MODIFIED_DATE,
   CREATED_ON
)
AS
   SELECT   a.fk_account,
            pk_storage,
            a.storage_id,
            storage_name,
            STORAGE_ALTERNALEID AS STORAGE_ALTERNATEID,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_storage_type)
               storage_type,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_child_stype)
               child_storage_type,
            (SELECT   aa.storage_id
               FROM   ER_STORAGE aa
              WHERE   aa.pk_storage = a.fk_storage)
               parent_storage_id,
            (SELECT   aa.storage_name
               FROM   ER_STORAGE aa
              WHERE   aa.pk_storage = a.fk_storage)
               parent_storage,
            b.fk_study storage_fk_study,
            (SELECT   study_number
               FROM   ER_STUDY
              WHERE   pk_study = b.fk_study)
               storage_study_number,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = fk_codelst_storage_status)
               storage_status,
            ss_start_date storage_status_date,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = b.fk_user)
               AS Storage_tracking_user,
            b.ss_tracking_number Storage_tracking_no,
            a.rid storage_rid,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = a.creator)
               storage_creator,
            a.created_on storage_created_on,
            (SELECT   usr_firstname || ' ' || usr_lastname
               FROM   ER_USER
              WHERE   pk_user = a.last_modified_by)
               storage_last_modified_by,
            a.last_modified_date storage_last_modified_date,
            s.pk_specimen pk_specimen,
            SPEC_ID,
            SPEC_ALTERNATE_ID,
            SPEC_DESCRIPTION,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_TYPE)
               SPEC_TYPE,
            SPEC_COLLECTION_DATE,
            SPEC_EXPECTED_QUANTITY exp_spec_qty,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_EXPECTEDQ_UNITS)
               exp_qty_units,
            SPEC_BASE_ORIG_QUANTITY coll_spec_qty,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_BASE_ORIGQ_UNITS)
               coll_qty_units,
            SPEC_ORIGINAL_QUANTITY spec_qty,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_QUANTITY_UNITS)
               spec_qty_units,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = SPEC_OWNER_FK_USER)
               AS Owner,
            s.FK_STUDY spec_fk_study,
            (SELECT   study_number
               FROM   ER_STUDY
              WHERE   pk_study = s.FK_STUDY)
               spec_study_number,
            (SELECT   site_name
               FROM   ER_SITE
              WHERE   pk_site = s.FK_SITE)
               AS spec_orgn,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_ANATOMIC_SITE)
               SPEC_ANATOMIC_SITE,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_TISSUE_SIDE)
               SPEC_TISSUE_SIDE,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SPEC_PATHOLOGY_STAT)
               SPEC_PATHOLOGY_STAT,
            (SELECT   aa.SPEC_ID
               FROM   ER_SPECIMEN aa
              WHERE   aa.pk_specimen = s.FK_SPECIMEN)
               parent_spec_id,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = ss.FK_CODELST_STATUS)
               spec_status,
            SS_DATE spec_status_date,
            SS_QUANTITY spec_status_qty,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SS_QUANTITY_UNITS)
               spec_status_qty_units,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = SS_ACTION)
               spec_status_action,
            (SELECT   codelst_desc
               FROM   ER_CODELST
              WHERE   pk_codelst = ss.SS_PROC_TYPE)
               spec_processing_type,
            SPEC_REMOVAL_TIME,
            SPEC_FREEZE_TIME,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = FK_USER_RECEPIENT)
               AS RECEPIENT,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = FK_USER_PATH)
               AS Pathologist,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = FK_USER_SURG)
               AS Surgeon,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = FK_USER_COLLTECH)
               AS Collecting_Technician,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = FK_USER_PROCTECH)
               AS Processing_Technician,
            ss.SS_TRACKING_NUMBER spec_tracking_no,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = s.CREATOR)
               spec_creator,
            s.CREATED_ON spec_created_on,
            (SELECT   USR_FIRSTNAME || ' ' || USR_LASTNAME
               FROM   ER_USER
              WHERE   PK_USER = s.LAST_MODIFIED_BY)
               spec_LAST_MODIFIED_BY,
            s.LAST_MODIFIED_DATE spec_last_modified_date,
			a.created_on
     FROM   ER_STORAGE a,
            ER_STORAGE_STATUS b,
            ER_SPECIMEN s,
            ER_SPECIMEN_STATUS ss
    WHERE       a.FK_ACCOUNT = s.FK_ACCOUNT
            AND a.pk_storage = b.fk_storage
            AND a.pk_storage = s.fk_storage
            AND b.pk_storage_status =
                  (SELECT   MAX (c.pk_storage_status)
                     FROM   ER_STORAGE_STATUS c
                    WHERE   a.pk_storage = c.fk_storage
                            AND c.ss_start_date =
                                  (SELECT   MAX (d.ss_start_date)
                                     FROM   ER_STORAGE_STATUS d
                                    WHERE   a.pk_storage = d.fk_storage))
            AND ss.pk_specimen_status =
                  (SELECT   MAX (c.pk_specimen_status)
                     FROM   ER_SPECIMEN_STATUS c
                    WHERE   s.pk_specimen = c.fk_specimen
                            AND c.ss_date =
                                  (SELECT   MAX (d.ss_date)
                                     FROM   ER_SPECIMEN_STATUS d
                                    WHERE   s.pk_specimen = d.fk_specimen));


--End Views--


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,86,2,'02_8views.sql',sysdate,'8.9.0 Build#543');

commit;