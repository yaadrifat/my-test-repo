-- INSERTING into ER_CODELST
Insert into ER_CODELST (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CREATED_ON) 
values (SEQ_ER_CODELST.nextval,'site_type','service','Site of Service','N',4,sysdate);

commit;

 
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,71,7,'07_data.sql',sysdate,'8.9.0 Build#528');

commit;
