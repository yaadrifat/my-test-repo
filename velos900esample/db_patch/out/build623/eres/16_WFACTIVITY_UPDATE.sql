set define off;

--STARTS UPDATING RECORD FROM WFACTIVITY_CONDITION TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from WFACTIVITY_CONDITION
    where EA_TYPE = 'POST'
    AND EA_TABLENAME = 'ER_ORDER' AND EA_COLUMNNAME='NMDP_SAMPLE_SHIPPED_FLAG';
  if (v_record_exists > 1) then
      UPDATE WFACTIVITY_CONDITION SET EA_VALUE='select ''''''Y'''''' from dual' where EA_TYPE = 'POST'
    AND EA_TABLENAME = 'ER_ORDER' AND EA_COLUMNNAME='NMDP_SAMPLE_SHIPPED_FLAG';
	commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,166,16,'16_WFACTIVITY_UPDATE.sql',sysdate,'9.0.0 Build#623');

commit;
