--STARTS ADD THE COLUMN STATUS_CODE_SEQ TO CB_CBU_STATUS--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CBU_STATUS'
    AND COLUMN_NAME = 'STATUS_CODE_SEQ';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_CBU_STATUS add (STATUS_CODE_SEQ  NUMBER(10,0))';
      commit;
  end if;
end;
/
--END--
--starts comment for THE COLUMN STATUS_CODE_SEQ TO CB_CBU_STATUS TABLE--
comment on column CB_CBU_STATUS.STATUS_CODE_SEQ  is 'Storing the STATUS CODE seqence.';
commit;
--end--




--STARTS UPDATING RECORD INTO CB_CBU_STATUS--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from CB_CBU_STATUS
    where  CBU_STATUS_CODE = 'AG' and CODE_TYPE='Response'; 

  if (v_column_exists > 0) then
      UPDATE CB_CBU_STATUS SET STATUS_CODE_SEQ = 1 WHERE CBU_STATUS_CODE = 'AG' and CODE_TYPE='Response';
      commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO CB_CBU_STATUS--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from CB_CBU_STATUS
    where  CBU_STATUS_CODE = 'CD' and CODE_TYPE='Response'; 

  if (v_column_exists > 0) then
      UPDATE CB_CBU_STATUS SET STATUS_CODE_SEQ = 2 WHERE CBU_STATUS_CODE = 'CD' and CODE_TYPE='Response';
      commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO CB_CBU_STATUS--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from CB_CBU_STATUS
    where  CBU_STATUS_CODE = 'DD' and CODE_TYPE='Response'; 

  if (v_column_exists > 0) then
      UPDATE CB_CBU_STATUS SET STATUS_CODE_SEQ = 3 WHERE CBU_STATUS_CODE = 'DD' and CODE_TYPE='Response';
      commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO CB_CBU_STATUS--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from CB_CBU_STATUS
    where  CBU_STATUS_CODE = 'OT' and CODE_TYPE='Response'; 

  if (v_column_exists > 0) then
      UPDATE CB_CBU_STATUS SET STATUS_CODE_SEQ = 4 WHERE CBU_STATUS_CODE = 'OT' and CODE_TYPE='Response';
      commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO CB_CBU_STATUS--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from CB_CBU_STATUS
    where  CBU_STATUS_CODE = 'QR' and CODE_TYPE='Response'; 

  if (v_column_exists > 0) then
      UPDATE CB_CBU_STATUS SET STATUS_CODE_SEQ = 5 WHERE CBU_STATUS_CODE = 'QR' and CODE_TYPE='Response';
      commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO CB_CBU_STATUS--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from CB_CBU_STATUS
    where  CBU_STATUS_CODE = 'RO' and CODE_TYPE='Response'; 

  if (v_column_exists > 0) then
      UPDATE CB_CBU_STATUS SET STATUS_CODE_SEQ = 6 WHERE CBU_STATUS_CODE = 'RO' and CODE_TYPE='Response';
      commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO CB_CBU_STATUS--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from CB_CBU_STATUS
    where  CBU_STATUS_CODE = 'SO' and CODE_TYPE='Response'; 

  if (v_column_exists > 0) then
      UPDATE CB_CBU_STATUS SET STATUS_CODE_SEQ = 7 WHERE CBU_STATUS_CODE = 'SO' and CODE_TYPE='Response';
      commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO CB_CBU_STATUS--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from CB_CBU_STATUS
    where  CBU_STATUS_CODE = 'XP' and CODE_TYPE='Response'; 

  if (v_column_exists > 0) then
      UPDATE CB_CBU_STATUS SET STATUS_CODE_SEQ = 8 WHERE CBU_STATUS_CODE = 'XP' and CODE_TYPE='Response';
      commit;
  end if;
end;
/
--END--

--STARTS ADDING COLUMN TO CB_CORD_MINIMUM_CRITERIA--

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'CT_COMPLETED_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(CT_COMPLETED_FLAG VARCHAR2(1))';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'UNIT_RECP_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(UNIT_RECP_FLAG VARCHAR2(1))';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'PRIOR_TO_PREP_START';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(PRIOR_TO_PREP_START VARCHAR2(1))';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'PAT_TYPING';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(PAT_TYPING VARCHAR2(1))';
  end if;
end;
/

COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."CT_COMPLETED_FLAG" IS 'This is the column to store the flag of CT (A, B, DRB1) completed';

COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."UNIT_RECP_FLAG" IS 'This is the column to store the flag of Unit and recipient at least 3/6 antigen match';

COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."PRIOR_TO_PREP_START" IS 'This is the column to store the flag of CBU planned arrival is prior to prep start';

COMMENT ON COLUMN "CB_CORD_MINIMUM_CRITERIA"."PAT_TYPING" IS 'This is the column to store the flag of Patient typing reported';

--END--

--START ALTERING TABLE CB_QUESTIONS--

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_QUESTIONS'
    AND column_name = 'REF_CHART';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_QUESTIONS ADD(REF_CHART VARCHAR2(15))';
  end if;
end;
/
--END--
COMMENT ON COLUMN "CB_QUESTIONS"."REF_CHART" IS 'This is the column to store the codelsttype of Reference Chart';

commit;



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,174,3,'03_MISC_ALTER.sql',sysdate,'9.0.0 Build#631');

commit;