--STARTS DELETING RECORDS FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_25')
    AND CONDITION_TYPE = 'PRE';
  if (v_record_exists > 1) then
      DELETE FROM CB_ALERT_CONDITIONS where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_25')AND CONDITION_TYPE = 'PRE';
	commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,174,38,'38_CB_ALERT_CONDITIONS_DELETE.sql',sysdate,'9.0.0 Build#631');

commit;