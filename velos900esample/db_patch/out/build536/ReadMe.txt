This build involves an upgrade of Yahoo UI (YUI) library from v2.4.0 to v.2.8.1. Before copying the deploy foler, be sure to remove the old YUI library. It is located in
[eresearch_install_dir]\server\eresearch\deploy\eres.war\jsp\js\yui\

Remove the yui folder first. Then copy the contents of the deploy folder of the build, as usual, to
[eresearch_install_dir]\server\eresearch\deploy\

