CREATE OR REPLACE FUNCTION ERES.f_getLatestPatStudyStatPK(p_study NUMBER,p_per in number) RETURN NUMBER
  IS
  v_stat NUMBER;
  BEGIN
   BEGIN

         SELECT   MAX (pk_patstudystat)
         into v_stat 
         FROM   er_patstudystat
          WHERE   fk_study = p_study  and fk_per = p_per
           and patstudystat_date =
           (
            SELECT   MAX (i.patstudystat_date)
            FROM   er_patstudystat i
            WHERE   i.fk_study = p_study and i.fk_per = p_per
                        
           );
               
    EXCEPTION WHEN NO_DATA_FOUND THEN

          v_stat := 0;

    END;

    RETURN v_stat ;
  END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,125,6,'06_f_getLatestPatStudyStatPK.sql',sysdate,'8.10.0 Build#582');

commit;