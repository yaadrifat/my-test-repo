DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) into index_count FROM ER_OBJECT_SETTINGS WHERE
    OBJECT_SUBTYPE = 'mng_ling_menu' and OBJECT_NAME = 'cbu_tools_menu' ;
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
     (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
      OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
     (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'mng_ling_menu', 'cbu_tools_menu', 5, 1, 'Manage Link', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting mng_ling_menu for cbu_tools_menu already exists');
  end if;
END;
/ 

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,167,4,'04_MENU.sql',sysdate,'9.0.0 Build#624');


commit;
