set define off;

--Rename existing Sub-menus
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Search' 
WHERE OBJECT_TYPE ='M' and OBJECT_SUBTYPE='open_menu' and OBJECT_NAME='study_menu';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Search' 
WHERE OBJECT_TYPE ='M' and OBJECT_SUBTYPE='open_menu' and OBJECT_NAME='mgpat_menu';

UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_DISPLAYTEXT = 'Search' 
WHERE OBJECT_TYPE ='M' and OBJECT_SUBTYPE='open_menu' and OBJECT_NAME='budget_menu';

Commit;

--Renaming the hidden Sub Menu 'Personalize' subtype
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_SUBTYPE = 'personal_menu'
WHERE OBJECT_NAME='accnt_menu' AND OBJECT_SUBTYPE = 'application_menu' AND OBJECT_VISIBLE = '0' ;

Commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,155,10,'10_menuChanges.sql',sysdate,'9.0.0 Build#612');

commit;