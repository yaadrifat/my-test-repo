--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='ind_protocol'
    AND CODELST_SUBTYP = 'yes';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'ind_protocol','yes','YES','N',NULL,SYSDATE,SYSDATE,NULL,1);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='ind_protocol'
    AND CODELST_SUBTYP = 'lic_crd';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'ind_protocol','lic_crd','N/A - Licensed Cord','N',NULL,SYSDATE,SYSDATE,NULL,2);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='ind_protocol'
    AND CODELST_SUBTYP = 'non-us';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'ind_protocol','non-us','N/A - Non-US TC','N',NULL,SYSDATE,SYSDATE,NULL,3);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='ind_protocol'
    AND CODELST_SUBTYP = 'no';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'ind_protocol','no','NO','N',NULL,SYSDATE,SYSDATE,NULL,4);
	commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,175,5,'05_ER_CODELST_INSERT.sql',sysdate,'9.0.0 Build#632');

commit;