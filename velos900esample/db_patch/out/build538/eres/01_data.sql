set define off;

declare
v_ct number;
begin

select count(*) into v_ct from ER_OBJECT_SETTINGS where OBJECT_NAME = 'protocol_tab' and
OBJECT_SUBTYPE = '7';

if (v_ct = 0) then

Insert into ER_OBJECT_SETTINGS
   (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, OBJECT_VISIBLE, 
    OBJECT_DISPLAYTEXT, FK_ACCOUNT)
 Values
   (SEQ_ER_OBJECT_SETTINGS.nextval, 'T', '7', 'protocol_tab', 5, 1, 
    'Coverage Analysis', 0);

COMMIT;

end if;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,81,1,'01_data.sql',sysdate,'8.9.0 Build#538');

commit;
