set define off;

create or replace FUNCTION "F_GET_VISITS" (paramid NUMBER) 
RETURN VARCHAR2
IS
  v_retval VARCHAR2(4000);
  
BEGIN

FOR i IN (select VISIT_NAME from SCH_PROTOCOL_VISIT where  PK_PROTOCOL_VISIT in (select FK_PROTOCOL_VISIT from SCH_SUBCOST_ITEM_VISIT where FK_SUBCOST_ITEM =paramid) )
LOOP
	 
	 v_retval := v_retval || i.VISIT_NAME || ',';
END LOOP;

	v_retval := substr(v_retval, 1, LENGTH(v_retval)-1);
	

RETURN v_retval ;
END ;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,112,2,'02_F_GET_VISITS.sql',sysdate,'8.10.0 Build#569');

commit;