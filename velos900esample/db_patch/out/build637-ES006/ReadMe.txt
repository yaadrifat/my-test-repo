/* This readMe is specific to eSample (version 9.0.0 build #637-ES006) */

=====================================================================================================================================
eSample bugs covered in the build:

1) Bug 12369 - INV 21675: In storage admin if parent storage has multiple specimens, user is not able to see the tabular view for parent storages 
2) Bug 12370 - INV-21675: Image is not getting displayed for parent specimen, whether it is Occupied or partially occupied.
3) Bug 12371 - INV-21675: Alert message validation is not working for Child storage specimen's 
4) Bug 12468 - If a storage location stores multiple specimens and all of them belong to diff/more than one patient, then we should not show P link
5) Bug 10112 - UI-1-Skin: Font Size and Font Color are inconsistent with eSample Module

Following existing files have been modified:

	1. deletespecimens.jsp					(...\deploy\velos.ear\velos.war\jsp)
	2. deletestorages.jsp					(...\deploy\velos.ear\velos.war\jsp)
	3. getStorageGrid.jsp					(...\deploy\velos.ear\velos.war\jsp)
	4. specimenstatus.jsp					(...\deploy\velos.ear\velos.war\jsp)
	5. storagestatus.jsp					(...\deploy\velos.ear\velos.war\jsp)
	6. storageunitdetails.jsp				(...\deploy\velos.ear\velos.war\jsp)
	7. updatestorageunitdetails.jsp			(...\deploy\velos.ear\velos.war\jsp)
=====================================================================================================================================