set define off;
CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENT_CRF_AD1" 
AFTER DELETE
ON SCH_EVENT_CRF
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW

BEGIN

  --delete the forms/events combination from the sch_portal_forms table 

  DELETE from SCH_PORTAL_FORMS 
  WHERE 
  FK_EVENT = :OLD.FK_EVENT and FK_FORM = :OLD.FK_FORM;

END;
/

  CREATE OR REPLACE TRIGGER "ESCH"."EVENT_ASSOC_AD3" AFTER DELETE ON EVENT_ASSOC
REFERENCING OLD AS OLD NEW AS a
FOR EACH ROW
BEGIN
 
--delete records with matching events from the sch_portal_forms table 

  DELETE from SCH_PORTAL_FORMS 
  WHERE 
  FK_EVENT = :OLD.EVENT_ID;

END;
/

create or replace TRIGGER esch."SCH_PROTOCOL_VISIT_AD1" AFTER DELETE ON SCH_PROTOCOL_VISIT REFERENCING OLD AS OLD NEW AS a
FOR EACH ROW
declare
 pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'SCH_PROTOCOL_VISIT_AD1', pLEVEL  => Plog.LFATAL);
 begin

 --Delete the event from calendar's default budget
 begin
     --KM-#5949
     --DELETE FROM SCH_SUBCOST_ITEM_VISIT WHERE FK_PROTOCOL_VISIT = :old.pk_protocol_visit;

     delete from sch_lineitem l
     where l.fk_bgtsection in ( select pk_budgetsec from sch_bgtsection where fk_visit = :old.pk_protocol_visit and fk_bgtcal =
                 (select pk_bgtcal from sch_bgtcal where bgtcal_protid = :old.fk_protocol and fk_budget = (
                 select  pk_budget from sch_budget where budget_calendar = :old.fk_protocol)
                 )
             );

     delete from sch_bgtsection where fk_visit = :old.pk_protocol_visit and fk_bgtcal =
                 (select pk_bgtcal from sch_bgtcal where bgtcal_protid = :old.fk_protocol and fk_budget = (
                 select  pk_budget from sch_budget where budget_calendar = :old.fk_protocol)
                 );

 exception when others then
    plog.fatal(pctx,'Exception:'||sqlerrm);
 end;
end;
/

create or replace TRIGGER ESCH.SCH_SUBCOST_ITEM_VISIT_AD0 AFTER DELETE ON SCH_SUBCOST_ITEM_VISIT
FOR EACH ROW
declare
  raid number(10);
  deleted_data clob;
  USR VARCHAR2(2000);
begin
	--BUG 5818
	BEGIN
		--KM-#5949
    USR := getuser(:old.last_modified_by);
    --USR := getuser(NVL(:NEW.LAST_MODIFIED_BY, :NEW.CREATOR));
		EXCEPTION WHEN NO_DATA_FOUND THEN
		USR := 'New User' ;
	END ;

select seq_audit.nextval into raid from dual;
audit_trail.record_transaction
    (raid, 'SCH_SUBCOST_ITEM_VISIT', :old.rid, 'D', USR);
--Created by Manimaran for Audit delete
deleted_data :=
to_char(:old.PK_SUBCOST_ITEM_VISIT) || '|' ||
to_char(:old.FK_SUBCOST_ITEM) || '|' ||
to_char(:old.FK_PROTOCOL_VISIT) || '|' ||
to_char(:old.RID) || '|' ||
to_char(:old.CREATOR) || '|' ||
to_char(:old.CREATED_ON) || '|' ||
to_char(:old.LAST_MODIFIED_BY) || '|' ||
to_char(:old.LAST_MODIFIED_DATE) || '|' ||
:old.IP_ADD;

insert into AUDIT_DELETE
(raid, row_data) values (raid, deleted_data);
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,115,3,'03_esch_trigger.sql',sysdate,'8.10.0 Build#572');

commit;