set define off;

--Renaming the hidden Sub Menu 'Personalize' subtype
UPDATE "ERES"."ER_OBJECT_SETTINGS" SET OBJECT_SUBTYPE = 'personal_menu'
WHERE OBJECT_NAME='manage_accnt' AND OBJECT_SUBTYPE = 'application_menu' AND OBJECT_VISIBLE = '0' ;

Commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,157,2,'02_menuChanges.sql',sysdate,'9.0.0 Build#614');

commit;