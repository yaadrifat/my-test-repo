set define off;

delete from er_repxsl where fk_report in (160);

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,122,1,'01_data.sql',sysdate,'8.10.0 Build#579');

commit;
