/* This readMe is specific to eSample (version 9.0.0 build #637-ES001) */

=====================================================================================================================================
eSample requirements covered in the build:
	INV 21676
	INV 21677
	INV 21679

Following new files have been added:

	1. ajaxGetSpecimenLocation.jsp		(...\deploy\velos.ear\velos.war\jsp)

Following existing files have been modified:

	2. specimenbrowser.jsp 				(...\deploy\velos.ear\velos.war\jsp)
	3. updatespecimendetails.jsp		(...\deploy\velos.ear\velos.war\jsp)
	4. SpecimenJB.class					(...\deploy\velos.ear\velos-main.jar\com\velos\eres\web\specimen)
	5. SpecimenAgentRObj.class			(...\deploy\velos.ear\velos-ejb-eres.jar\com\velos\eres\service\specimenAgent)
	6. SpecimenAgentBean.class			(...\deploy\velos.ear\velos-ejb-eres.jar\com\velos\eres\service\specimenAgent\impl)
	7. SpecimenDao.class				(...\deploy\velos.ear\velos-ejb-eres.jar\com\velos\eres\business\common)
	8. LC.class							(...\deploy\velos.ear\velos-common.jar\com\velos\eres\service\util)
	9. labelBundle.properties			(...\eResearch_jboss510\conf)

Property file changes (Do not replace the files):
	Copy the KEY(s) given in the file "changes_labelBundle.properties" to the existing file "labelBundle.properties" under the path:  ...\eResearch_jboss510\conf
=====================================================================================================================================
