<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="java.sql.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*"%>

<%
	ResultSet rsResults = null;
	Statement stmt = null;
	Connection conn = null;
    String specimenName=null;
    String temp=null;
	int fk_specimen=0;
	
	CommonDAO cd = new CommonDAO();
	int pk_storage_id = Integer.parseInt(request.getParameter("strg_pk"));
    try
	{
		do
		{
			conn = cd.getConnection();
			stmt=conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT fk_storage,storage_name FROM er_storage WHERE pk_storage="+pk_storage_id); 
            temp=specimenName;
            while (rs.next())
			{
            	fk_specimen =  rs.getInt(1);
            	specimenName =  rs.getString(2);
            } 
			pk_storage_id=fk_specimen;
			
            if(temp!=null) 
            specimenName=specimenName+" >> "+temp; 	
            conn.close();
        }
		while(fk_specimen!=0);			
    }
	catch (Exception ex)
	{
		Rlog.fatal("specimen","In SpecimenDao, EXCEPTION IN FETCHING FROM fk_storage,storage_name"+ ex);
    }
	finally
	{
        try
		{
			if (conn != null) { conn.close(); }
        }
		catch (Exception e){ }
    }
	out.print(specimenName);
%>

