set define off;

-- UPDATE into ER_REPORT

Update ER_REPORT set REP_SQL_CLOB = 'SELECT pk_specimen, spec_id, spec_alternate_id, a.fk_study,
(SELECT p.SPEC_ID FROM ER_SPECIMEN p WHERE p.pk_specimen = a.fk_specimen) PSEC_ID,
(SELECT count(*) FROM ER_SPECIMEN p WHERE p.fk_specimen = a.pk_specimen) ChildCnt,
case when a.fk_study is null then ''[No Study Specified]''
else (SELECT study_number FROM er_study WHERE pk_study = a.fk_study) end study_number,
b.SS_DATE,
(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst=b.FK_CODELST_STATUS) as Spec_Staus,
case when (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) is null then ''-''
else (SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_type) end spec_type,
spec_collection_date,
(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_anatomic_site) spec_anatomic_site,
spec_original_quantity,
(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = spec_quantity_units) spec_quantity_units,
(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst=b.SS_PROC_TYPE) as SS_PROC_TYPE,
SS_HAND_OFF_DATE, SS_PROC_DATE, SS_QUANTITY,
(SELECT CODELST_DESC FROM er_codelst WHERE pk_codelst = SS_QUANTITY_UNITS) SS_QUANTITY_UNITS,
(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=b.FK_USER_RECEPIENT ) AS SS_RECEPIENT_NAME,
(SELECT USR_FIRSTNAME ||''  ''|| USR_LASTNAME FROM ER_USER where pk_user=b.SS_STATUS_BY ) AS SS_STATUSBY_NAME,
(SELECT STUDY_NUMBER FROM ER_STUDY where pk_study=b.FK_STUDY) AS SS_STUDY_NUMBER
FROM er_specimen a, er_specimen_status b
WHERE (('':specTyp''=''NonStudy'' AND a.fk_study is null AND a.fk_account = :sessAccId) or ('':specTyp''=''Study'' AND a.fk_study in (:studyId)))
AND (a.fk_per is null OR exists (select * from epat.person per, ER_PATFACILITY fac, er_usersite usr  WHERE pk_person = a.fk_per and per.fk_account =:sessAccId
and usr.fk_user =:sessUserId AND usr.usersite_right>=4 AND usr.fk_site = fac.fk_site  AND fac.patfacility_accessright > 0  and per.pk_person = fac.fk_per))
AND b.fk_specimen = a.pk_specimen
AND b.FK_CODELST_STATUS IN (SELECT pk_codeLst FROM er_codelst WHERE codelst_custom_col1=''process'')
AND (spec_collection_date is null or spec_collection_date BETWEEN TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT))
AND a.FK_SITE IN (:orgId)
ORDER BY pk_specimen, spec_collection_date' where pk_report = 138;

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,181,7,'07_ER_REPORT.sql',sysdate,'9.0.0 B#637-ES001');

COMMIT;
