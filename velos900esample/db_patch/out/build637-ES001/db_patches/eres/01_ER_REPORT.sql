set define off;

-- UPDATE into ER_REPORT

UPDATE er_report SET REP_FILTERAPPLICABLE = REP_FILTERAPPLICABLE || ':orgId'
WHERE pk_report IN (128,130,136,137,138,142,143,145,154,155);

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,181,1,'01_ER_REPORT.sql',sysdate,'9.0.0 B#637-ES001');

COMMIT;