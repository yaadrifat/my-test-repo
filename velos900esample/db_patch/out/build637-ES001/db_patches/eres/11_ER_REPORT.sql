set define off;

-- UPDATE into ER_REPORT

UPDATE er_report SET REP_SQL_CLOB ='SELECT case when '':specTyp''=''Study'' then
pkg_inventory.f_get_specimen_trail(:sessUserId, '':studyId'', TO_DATE('':fromDate'',PKG_DATEUTIL.F_GET_DATEFORMAT), TO_DATE('':toDate'',PKG_DATEUTIL.F_GET_DATEFORMAT),'':orgId'')
end from dual' WHERE pk_report = 137;

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,181,11,'11_ER_REPORT.sql',sysdate,'9.0.0 B#637-ES001');

COMMIT;
