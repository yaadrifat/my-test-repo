set define off;



--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_15';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL1='Alert new user if a request has been re-assigned to them.' WHERE codelst_type = 'alert' AND codelst_subtyp = 'alert_15';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_11';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL1='Alert CBB if  a CT sample has not been shipped 2 days after the request date (not ship date entered).  Per Cord Manual of Operations.' WHERE codelst_type = 'alert' AND codelst_subtyp = 'alert_11';
	commit;
  end if;
end;
/
--END--


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,165,8,'08_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 Build#622');

commit;