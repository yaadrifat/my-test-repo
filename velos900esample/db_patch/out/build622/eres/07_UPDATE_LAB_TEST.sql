set define off;


    UPDATE ER_LABTEST SET LABTEST_SEQ=1 WHERE LABTEST_NAME='CBU volume (without anticoagulant/additives)'and LABTEST_SHORTNAME='CBU_VOL';
        
    UPDATE ER_LABTEST SET LABTEST_SEQ=2 WHERE LABTEST_NAME='Total CBU Nucleated Cell Count' and LABTEST_SHORTNAME='TCNCC';
        
    UPDATE ER_LABTEST SET LABTEST_SEQ=3 WHERE LABTEST_NAME='CBU Nucleated Cell Count Concentration' and LABTEST_SHORTNAME='CBUNCCC';
        
    UPDATE ER_LABTEST SET LABTEST_SEQ=4 WHERE LABTEST_NAME='Final Product Volume' and LABTEST_SHORTNAME='FNPV';
         
    UPDATE ER_LABTEST SET LABTEST_SEQ=5 WHERE LABTEST_NAME='Total CBU Nucleated Cell Count (unknown if uncorrected)' and LABTEST_SHORTNAME='TCBUUN';
         
    UPDATE ER_LABTEST SET LABTEST_SEQ=6 WHERE LABTEST_NAME='Total CBU Nucleated Cell Count (uncorrected)' and LABTEST_SHORTNAME='UNCRCT';

   UPDATE ER_LABTEST SET LABTEST_SEQ=7 WHERE LABTEST_NAME='Total CD34+ Cell Count' and LABTEST_SHORTNAME='TCDAD';
        
   UPDATE ER_LABTEST SET LABTEST_SEQ=8 WHERE LABTEST_NAME='% of CD34+  Viable' and LABTEST_SHORTNAME='PERCD';
        
   UPDATE ER_LABTEST SET LABTEST_SEQ=9 WHERE LABTEST_NAME='% of CD34+ Cells in Total Monunuclear Cell Count' and LABTEST_SHORTNAME='PERTMON';
        
   UPDATE ER_LABTEST SET LABTEST_SEQ=10 WHERE LABTEST_NAME=' % of Mononuclear Cells in Total Nucleated Cell Count' and LABTEST_SHORTNAME='PERTNUC';

   UPDATE ER_LABTEST SET LABTEST_SEQ=11 WHERE LABTEST_NAME='Total CD3 Cell Count' and LABTEST_SHORTNAME='TOTCD' and LABTEST_SHORTNAME='TOTCD';        
       
   UPDATE ER_LABTEST SET LABTEST_SEQ=12 WHERE LABTEST_NAME='% CD3 Cells in Total Mononuclear Cell Count' and LABTEST_SHORTNAME='PERCD3'; 
        
   UPDATE ER_LABTEST SET LABTEST_SEQ=13 WHERE LABTEST_NAME='CBU nRBC Absolute Number' and LABTEST_SHORTNAME='CNRBC'; 
        
   UPDATE ER_LABTEST SET LABTEST_SEQ=14 WHERE LABTEST_NAME='CBU nRBC %' and LABTEST_SHORTNAME='PERNRBC';
         
   UPDATE ER_LABTEST SET LABTEST_SEQ=15 WHERE LABTEST_NAME='CFU Count' and LABTEST_SHORTNAME='CFUCNT';
        
   UPDATE ER_LABTEST SET LABTEST_SEQ=16 WHERE LABTEST_NAME='Viability' and LABTEST_SHORTNAME='VIAB'; 
   
   commit;
   
   
   INSERT INTO track_patches
VALUES(seq_track_patches.nextval,165,7,'07_UPDATE_LAB_TEST.sql',sysdate,'9.0.0 Build#622');

commit;