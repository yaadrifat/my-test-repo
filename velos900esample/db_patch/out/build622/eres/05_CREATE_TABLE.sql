CREATE TABLE CB_IDM(
	PK_IDM NUMBER(10,0),
	FK_TESTGROUP NUMBER(10,0),
	IDM_FORM_FILLED_BY NUMBER(10,0),
	IDM_FORM_FILLED_DATE DATE,
	BLOOD_COLLECTION_DATE DATE,
	CMS_APPROVED_LAB NUMBER(10,0),
	FDA_LICENSED_LAB NUMBER(10,0),
	FK_SPECIMEN NUMBER(10,0),
	CREATOR	NUMBER(10,0),
	CREATED_ON DATE,
	LAST_MODIFIED_BY NUMBER(10,0),
	LAST_MODIFIED_DATE DATE,
	IP_ADD VARCHAR2(15 BYTE),
	DELETEDFLAG NUMBER(10,0),
	RID NUMBER(10,0)
);

COMMENT ON TABLE CB_IDM IS 'Stores additional information of IDM form whcih we can not store in er_patlabs table';
COMMENT ON COLUMN CB_IDM.PK_IDM IS 'Stores sequence generated unique value';
COMMENT ON COLUMN CB_IDM.FK_TESTGROUP IS 'Stores foreign key reference to ER_LABGROUP';
COMMENT ON COLUMN CB_IDM.IDM_FORM_FILLED_BY IS 'Stores user id who filled the data in IDM form';
COMMENT ON COLUMN CB_IDM.IDM_FORM_FILLED_DATE IS 'Stores date when IDM form filled';
COMMENT ON COLUMN CB_IDM.BLOOD_COLLECTION_DATE IS 'Stores date when blood collected';
COMMENT ON COLUMN CB_IDM.CMS_APPROVED_LAB IS 'Stores reference to er_codelst where codelst_type=idmtest_subques';
COMMENT ON COLUMN CB_IDM.FDA_LICENSED_LAB IS 'Stores reference to er_codelst where codelst_type=idmtest_subques';
COMMENT ON COLUMN CB_IDM.FK_SPECIMEN IS 'Stores foreign key reference to er_specimen table';
COMMENT ON COLUMN CB_IDM.CREATOR IS 'Uses for Audit trail. Stores who created the record';
COMMENT ON COLUMN CB_IDM.LAST_MODIFIED_BY IS 'Uses for Audit trail. Stores who modified the record recently';
COMMENT ON COLUMN CB_IDM.LAST_MODIFIED_DATE IS 'Uses for Audit trail. Stores when modified the record recently';
COMMENT ON COLUMN CB_IDM.CREATED_ON IS 'Uses for Audit trail. Stores when created the record';
COMMENT ON COLUMN CB_IDM.IP_ADD IS 'Uses for Audit trail. Stores from whcih IP Address action is performed';
COMMENT ON COLUMN CB_IDM.DELETEDFLAG IS 'Uses for Audit trail. Stores 1 if record is deleted,else stores 0 or null';
COMMENT ON COLUMN CB_IDM.RID IS 'Uses for Audit trail. Stores sequence generated unique value';



CREATE SEQUENCE SEQ_CB_IDM MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,165,5,'05_CREATE_TABLE.sql',sysdate,'9.0.0 Build#622');

commit;