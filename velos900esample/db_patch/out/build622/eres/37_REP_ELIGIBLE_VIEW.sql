CREATE OR REPLACE FORCE VIEW "ERES"."REP_ELIGIBLE_VIEW" 
(
"CORD_REGISTRY_ID", 
"CORD_LOCAL_CBU_ID",
"REGISTRY_MATERNAL_ID",
"MATERNAL_LOCAL_ID",
"STATUS",
"STATUS_REASONS",
"STATUS_DATE",
"STATUS_REMARKS",
"CBB_NAME",
"FK_ACCOUNT"
)
AS
  SELECT Cb_Cord.CORD_REGISTRY_ID,
    Cb_Cord.CORD_LOCAL_CBU_ID,
    Cb_Cord.REGISTRY_MATERNAL_ID,
    Cb_Cord.MATERNAL_LOCAL_ID,
    F_Codelst_Desc(Cb_Entity_Status.Fk_Status_Value) AS STATUS,       
    f_get_multiple_reasons(Cb_Entity_Status.Entity_Id)AS STATUS_REASONS,
    Cb_Entity_Status.STATUS_DATE,
    CB_ENTITY_STATUS.STATUS_REMARKS,
    Er_Site.Site_Name AS CBB_NAME,
    ER_SITE.FK_ACCOUNT
  FROM Cb_Cord,
    Er_Site,
    cb_entity_status
  WHERE Cb_Cord.Fk_Cbb_Id             =Er_Site.Pk_Site
  AND Cb_Cord.Pk_Cord                 =Cb_Entity_Status.Entity_Id
  AND Cb_Entity_Status.Fk_Entity_Type =
    (SELECT Pk_Codelst
    FROM Er_Codelst
    WHERE Lower(Codelst_Type) = 'entity_type'
    AND Lower(Codelst_Subtyp) ='cbu'
    )
  AND CB_ENTITY_STATUS.STATUS_TYPE_CODE ='eligibility';
  
  

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,165,37,'37_REP_ELIGIBLE_VIEW.sql',sysdate,'9.0.0 Build#622');

commit;

           


