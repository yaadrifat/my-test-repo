/* This readMe is specific to Velos eResearch version 9.0 build #622 */

=====================================================================================================================================
Garuda :

=====================================================================================================================================
eResearch:
1. Please refer to "UI Framework Standards and Guidelines.docx" while testing the INF-22323 enhancement.
2. Please refer to "INF-22237-Reason-for-change SQLs for testing.xls" while testing the INF-22237 related bugs.

=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1. messageBundle.properties
2. labelBundle.properties
3. LC.java
4. LC.jsp
5. MC.java
6. MC.jsp
=====================================================================================================================================
