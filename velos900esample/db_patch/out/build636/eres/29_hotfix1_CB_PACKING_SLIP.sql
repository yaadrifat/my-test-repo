set define off;
--STARTS ADDING COLUMN FROM CB_PACKING_SLIP TABLE--
DECLARE
  v_column_exists number := 0;  
  v_flag number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_PACKING_SLIP' AND COLUMN_NAME = 'TEMP_COL';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_PACKING_SLIP ADD TEMP_COL NUMBER(10,0)';
		v_flag := 1;
    commit;
  end if;
 if (v_flag = 1) then
	 execute immediate 'UPDATE CB_PACKING_SLIP SET TEMP_COL=SHIPMENT_TRACKING_NO';
	commit;
end if;
end;
/

--STARTS MODIFYING COLUMN FROM CB_PACKING_SLIP TABLE--
DECLARE
  v_column_exists number := 0;  
   v_flag number:= 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_PACKING_SLIP' AND COLUMN_NAME = 'SHIPMENT_TRACKING_NO';
  if (v_column_exists = 1) then 
      UPDATE CB_PACKING_SLIP SET SHIPMENT_TRACKING_NO=NULL;
	commit;
      execute immediate 'alter table CB_PACKING_SLIP MODIFY SHIPMENT_TRACKING_NO varchar2(100)';
      v_flag := 1;
      commit;
  end if;
   if (v_flag = 1) then
  execute immediate 'UPDATE CB_PACKING_SLIP SET SHIPMENT_TRACKING_NO=TEMP_COL';
       commit;
  end if;
end;
/


--STARTS MODIFYING COLUMN FROM CB_PACKING_SLIP TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_PACKING_SLIP' AND COLUMN_NAME = 'TEMP_COL';
  if (v_column_exists = 1) then
       execute immediate 'ALTER TABLE CB_PACKING_SLIP DROP COLUMN TEMP_COL';
	commit;
  end if;	
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,179,29,'29_hotfix1_CB_PACKING_SLIP.sql',sysdate,'9.0.0 Build#636');

commit;
