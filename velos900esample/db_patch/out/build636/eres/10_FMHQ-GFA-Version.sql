--Cb_QUESTION table--
--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'fam_med_hist_cmplt';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Was a Family Medical History completed?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'fam_med_hist_cmplt',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'response_med_hist_issue';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Are there any responses indicating a significant medical history issue?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='mulselc'), 'response_med_hist_issue',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 2.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'response_med_hist_dis';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Biological Relations', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'response_med_hist_dis',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 2.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'response_med_hist_rel';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Disease', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'response_med_hist_rel',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--END--
--CB_question_grp--
--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fam_med_hist_cmplt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='GFA');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC is null),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='fam_med_hist_cmplt'),
(select pk_form from cb_forms where forms_desc='FMHQ' and version='GFA'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='response_med_hist_issue')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='GFA');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC is null),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='response_med_hist_issue'),
(select pk_form from cb_forms where forms_desc='FMHQ' and version='GFA'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='response_med_hist_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='GFA');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC is null),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='response_med_hist_rel'),
(select pk_form from cb_forms where forms_desc='FMHQ' and version='GFA'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='response_med_hist_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='GFA');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC is null),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='response_med_hist_dis'),
(select pk_form from cb_forms where forms_desc='FMHQ' and version='GFA'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--cb_form_question--
--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fam_med_hist_cmplt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='GFA');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='GFA'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fam_med_hist_cmplt'),null,null,null,'1',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='fmhq_res5'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='response_med_hist_issue')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='GFA');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='GFA'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='response_med_hist_issue'),null,null,null,'2',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='fmhq_res5'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='response_med_hist_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='GFA');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='GFA'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='response_med_hist_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='response_med_hist_issue'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='response_med_hist_issue'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res5' and CODELST_SUBTYP='yes'),'2.a',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='fmhq_res3'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='response_med_hist_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='GFA');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='GFA'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='response_med_hist_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='response_med_hist_issue'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='response_med_hist_issue'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res5' and CODELST_SUBTYP='yes'),'2.b',NULL,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,179,10,'10_FMHQ-GFA-Version.sql',sysdate,'9.0.0 Build#636');

commit;