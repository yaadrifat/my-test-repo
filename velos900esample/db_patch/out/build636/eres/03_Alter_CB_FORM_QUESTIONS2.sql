
--STARTS ADDING COLUMN TO CB_FORM_QUESTIONS--

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_FORM_QUESTIONS'
    AND column_name = 'RESPONSE_VALUE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_FORM_QUESTIONS ADD(RESPONSE_VALUE VARCHAR2(255 BYTE))';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_FORM_QUESTIONS'
    AND column_name = 'UNEXPECTED_RESPONSE_VALUE';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_FORM_QUESTIONS ADD(UNEXPECTED_RESPONSE_VALUE VARCHAR2(255 BYTE))';
  end if;
end;
/


COMMENT ON COLUMN CB_FORM_QUESTIONS.RESPONSE_VALUE IS 'Stores value from er_codelst.codelst_type where codelst_type like responses';

COMMENT ON COLUMN CB_FORM_QUESTIONS.UNEXPECTED_RESPONSE_VALUE IS 'Stores value from er_codelst.codelst_type where codelst_type like unexpected_responses';

commit;
--END--

 --START CPOYING DATA FROM CB_QUESTION_RESPONSES TABLE TO CB_FORM_QUESTIONS TABLE--

  update cb_form_questions frmques set frmques.response_value=(select quesres.response_value from cb_question_responses quesres where frmques.fk_question=quesres.fk_question);
  
  update cb_form_questions frmques set frmques.unexpected_response_value=(select quesres.unexpected_response_value from cb_question_responses quesres where frmques.fk_question=quesres.fk_question);

--END--



--END--
--INSERTING DATA INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_unres1'
    and CODELST_SUBTYP = 'none';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_unres1','none','None','N',1,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'test_outcome1'
    and CODELST_SUBTYP = 'posi';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'test_outcome1','posi','Reactive/Positive','N',1,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'test_outcome1'
    and CODELST_SUBTYP = 'negi';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'test_outcome1','negi','Non-Reactive/Negative','N',2,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'test_outcome1'
    and CODELST_SUBTYP = 'not_done';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'test_outcome1','not_done','Not Done','N',3,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_unres2'
    and CODELST_SUBTYP = 'posi';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_unres2','posi','Reactive/Positive','N',1,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_unres2'
    and CODELST_SUBTYP = 'negi';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_unres2','negi','Non-Reactive/Negative','N',2,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_unres3'
    and CODELST_SUBTYP = 'not_done';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_unres3','not_done','Not Done','N',1,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'test_outcome2'
    and CODELST_SUBTYP = 'no';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'test_outcome2','no','No','N',1,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'test_outcome2'
    and CODELST_SUBTYP = 'yes';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'test_outcome2','yes','Yes','N',2,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_unres4'
    and CODELST_SUBTYP = 'no';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_unres4','no','No','N',1,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_unres5'
    and CODELST_SUBTYP = 'posi';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_unres5','posi','Reactive/Positive','N',1,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_unres5'
    and CODELST_SUBTYP = 'indeter';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_unres5','indeter','Indeterminate','N',2,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
--END--



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,179,3,'03_Alter_CB_FORM_QUESTIONS2.sql',sysdate,'9.0.0 Build#636');

commit;