set define off;
ALTER TRIGGER CBB_AU0 DISABLE;
--STARTS ADDING COLUMN FROM CBB TABLE--
DECLARE
  v_column_exists number := 0;  
  v_flag number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CBB' AND COLUMN_NAME = 'TEMP_COL';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CBB ADD TEMP_COL varchar2(1)';
		v_flag := 1;
    commit;
  end if;
 if (v_flag = 1) then
	 execute immediate 'UPDATE CBB SET TEMP_COL=MEMBER';
	commit;
end if;
end;
/

--STARTS MODIFYING COLUMN FROM CBB TABLE--
DECLARE
  v_column_exists number := 0;  
   v_flag number:= 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CBB' AND COLUMN_NAME = 'MEMBER';
  if (v_column_exists = 1) then 
      UPDATE CBB SET MEMBER=NULL;
	commit;
      execute immediate 'alter table CBB MODIFY MEMBER varchar2(1)';
      v_flag := 1;
      commit;
  end if;
   if (v_flag = 1) then
  execute immediate 'UPDATE CBB SET MEMBER=TEMP_COL';
       commit;
  end if;
end;
/


--STARTS MODIFYING COLUMN FROM CBB TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CBB' AND COLUMN_NAME = 'TEMP_COL';
  if (v_column_exists = 1) then
       execute immediate 'ALTER TABLE CBB DROP COLUMN TEMP_COL';
	commit;
  end if;	
end;
/
ALTER TRIGGER CBB_AU0 ENABLE;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,179,31,'31_hotfix2_CBB.sql',sysdate,'9.0.0 Build#636');

commit;
