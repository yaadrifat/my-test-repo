/* This readMe is specific to Velos eResearch version 9.0 build #636 */

=====================================================================================================================================
Garuda :
#Login Page Bug 10304
To resolve the BUG 10304 we have to make changes in messageBundle.properties variable M_FfoxIe_ScrnReso which is at line no 5344
from M_FfoxIe_ScrnReso=Browser: Internet Explorer 8 and 9 and Firefox 12 | Screen Resolution: 1280x1024
to M_FfoxIe_ScrnReso= Browser: Internet Explorer 8 | Screen Resolution: 1280x1024. This is NMDP specific so we can't commit from our end
	   To make this change go to eresHome->  messageBundle.properties
	   Then at line number 5344 set variable M_FfoxIe_ScrnReso to
	   M_FfoxIe_ScrnReso = Browser: Internet Explorer 8 | Screen Resolution: 1280x1024
#Spell Checker issue
ie8 does not have its own spell checker. To resolve the spell checker issue in editor of clinical note, download
Speckie 4.0 from internet. - http://www.softpedia.com/get/Internet/Internet-Applications-Addons/Speckie.shtml
To enable spell Checking in editor write the text then select the entire text and click on to the spell checker button
from the menu of the clinical note editor. 


#15_ASSESS_RIGHT_UPDATE.sql - This script is NMDP Specific

#16_ER_OBJECT_SETTING_UPDATE.sql - This script is NMDP Specific. (Funding menu option is no more required as this is out of scope for Rel 1.)

#18_CB_SHIPMENT.SQL - While executing this script various application messages are appearing over command prompt. Please ignore these messages as these
are part of database process executed on update of columns.


=====================================================================================================================================
eResearch:

#Bug10364
Total 107 number of xsl files are modified to resolve this issue. All the xsl files are released along with this version. These xsl files
can also be found under corresponding xsl file directory under plsql folder.

=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1. labelBundle.properties
2. LC.java
3. dashboardcentral.jsp
=====================================================================================================================================
