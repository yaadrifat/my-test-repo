--STARTS UPDATING RECORD INTO ER_CODELST TABLE -
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'note_assess'
    AND codelst_subtyp = 'no_cause';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='No Cause For Deferral' where codelst_type ='note_assess'AND codelst_subtyp = 'no_cause';
	commit;
  end if;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,178,3,'03_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 Build#635');

commit;