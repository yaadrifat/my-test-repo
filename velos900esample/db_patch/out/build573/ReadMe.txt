1) Copy messageBundle.properties in ereshome\ and paste it to the 
folder specified by ERES_HOME (where eresearch.xml is also hosted), 
overriding the existing file.

2) For all configurable items in Javascript going forward, 
the list will be maintained in js\velos\velosConfig.js.
This file is part of eResearch and will be continually updated.
It will list configurable items with default values. 
The meaning of the values will be documented in the comments.

Customizations of these configurable items are to be done in
velosCofigCustom.js (not in velosConfig.js). This custom file
is released only once in this build (Build #563) as a blank file
and will not be released again. To customize, simply copy the
appropriate line or block from velosConfig.js to velosConfigCustom.js
and make the change in velosConfigCustom.js.

For example, to restrict future date from being entered in 
Patient Study Status Date, add this line in velosConfigCustom.js:

var VAL_PATSTUDYSTAT_RESTRICT_FUTURE_STATDATE = 1;

