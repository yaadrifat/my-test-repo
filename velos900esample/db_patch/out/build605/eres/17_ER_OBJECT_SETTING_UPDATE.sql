UPDATE ER_OBJECT_SETTINGS SET OBJECT_DISPLAYTEXT='Product Fulfillment'
WHERE OBJECT_TYPE='TM' AND OBJECT_NAME='top_menu' AND OBJECT_SUBTYPE='pf_menu';

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,148,17,'17_ER_OBJECT_SETTING_UPDATE.sql',sysdate,'9.0.0 Build#605');

commit;