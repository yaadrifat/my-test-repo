--Workflow related functions------------
create or replace
FUNCTION "KEYCONDITIONSTR" (strfield varchar, strvalue varchar  ) Return Varchar As

	
	resultstr varchar2(1000);
        tmp_value varchar2(1000);
        tmp_keycolumn varchar2(1000);
Begin
    resultstr := '';
	tmp_value := strvalue || ',';
             tmp_keycolumn := strfield || ',' ;
             if(instr(tmp_keycolumn,',') > 0) then 
                 while ( instr(tmp_keycolumn,',') > 0)
                 loop
                  --BB   resultstr:= resultstr || ' and (' || (substr(tmp_keycolumn,0,instr(tmp_keycolumn,',')-1)) || '=' || (substr(tmp_value,0,instr(tmp_value,',')-1)) || ')';
                  resultstr:= resultstr || ' and (' || (substr(tmp_keycolumn,0,instr(tmp_keycolumn,',')-1)) || ')';
                     tmp_value := substr(tmp_value,instr(tmp_value,',')+1);
                     tmp_keycolumn:= substr(tmp_keycolumn,instr(tmp_keycolumn,',')+1);
                 end loop;
             end if; 
             resultstr:=replace(resultstr,'#keycolumn',strvalue);
	Return resultstr;

End;
/


create or replace
function populatesql(strsql varchar, strvalue varchar  ) Return Varchar As

	resultstr varchar2(1000);
        tmp_value varchar2(1000);
        tmp_repkey varchar2(10);
        tmp_repval varchar2(1000);
        tmp_count number;

Begin
    resultstr := strsql;
	tmp_value := strvalue || ',';
             tmp_count:=1;
             if(instr(tmp_value,',') > 0) then
                 while ( instr(tmp_value,',') > 0)
                 loop

                    tmp_repval := substr(tmp_value,0,instr(tmp_value,',')-1);
                    tmp_repkey := '#' || tmp_count || 'a';
                    resultstr := replace(resultstr,tmp_repkey,tmp_repval) ;
                    tmp_value := substr(tmp_value,instr(tmp_value,',')+1);
                    tmp_count := tmp_count + 1;
                 end loop;
              end if;
	Return resultstr;

End;
/



create or replace
FUNCTION ROWTOCOL ( p_slct IN VARCHAR2,p_dlmtr IN VARCHAR2 DEFAULT ',' ) RETURN VARCHAR2 AUTHID CURRENT_USER AS
     TYPE c_refcur IS REF CURSOR;
     lc_str VARCHAR2(4000);
     lc_colval VARCHAR2(4000);
     c_dummy c_refcur;
     l number;
     BEGIN
     OPEN c_dummy FOR p_slct;
     LOOP
     FETCH c_dummy INTO lc_colval;
     EXIT WHEN c_dummy%NOTFOUND;
     lc_str := lc_str || p_dlmtr || lc_colval;
     END LOOP;
     CLOSE c_dummy;
     RETURN SUBSTR(lc_str,2);
     EXCEPTION
     WHEN OTHERS THEN
     lc_str := SQLERRM;
     IF c_dummy%ISOPEN THEN
     CLOSE c_dummy;
     END IF;
     RETURN lc_str;
     END; 
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,148,12,'12_Functions.sql',sysdate,'9.0.0 Build#605');

commit;