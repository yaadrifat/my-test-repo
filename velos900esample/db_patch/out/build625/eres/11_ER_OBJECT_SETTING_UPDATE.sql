set define off;

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_OBJECT_SETTINGS
    where OBJECT_SUBTYPE = 'mng_ling_menu'
    AND OBJECT_NAME = 'cbu_tools_menu';
  if (v_record_exists = 0) then
      UPDATE ER_OBJECT_SETTINGS SET OBJECT_VISIBLE= 1 WHERE OBJECT_SUBTYPE = 'mng_ling_menu' AND OBJECT_NAME = 'cbu_tools_menu';
	commit;
  end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,168,11,'11_ER_OBJECT_SETTING_UPDATE.sql',sysdate,'9.0.0 Build#625');

commit;