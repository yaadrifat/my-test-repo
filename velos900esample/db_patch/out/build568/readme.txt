1) #D-FIN25, Day-0 implementation
Currently the eResearch application does not support Day 0 when the user creates a calendar.

This phase-2 of the enhancement released in build # 568 will allow the application accepting day 0 as visit interval for a calendar.

Following are the changes done to the application.
=============================================================================
1.Now User can enter the visit interval as dayo.Remember day 0 will be equivalent to month1.week1.day0 so day 0 can not be entered for any other week except first.And also we need to verify that if day7 has been entered for week 1 then interval day 0 should not be allowed and vice versa.

2.Schedule will be genereated accordingly.please go through those three cases mentioned in the requirement
document.

3.Similar changes has been done for mock schedule report. 

4.Test this scenario for the new calendar first then go for existing calendar as old data patch has
been released for existing calendars.

5.�Interval Column of calendar tab will show the interval information similar to protocol calendar view report.(requirement document updated)

So,We need to test the following sections making sure all works.
1.Calendar library.
2.Protocol Calendar.
3.Patient Schedule
4.Calendar reports.
5.Patient schedulecalendar report.
6.visit calendar report.
