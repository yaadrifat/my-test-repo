SET DEFINE OFF;

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_OBJECT_SETTINGS where OBJECT_SUBTYPE = 'pat_sched_forms' and 
  OBJECT_NAME = 'pat_sched_col';
  if (row_count < 1) then 
   Insert into ER_OBJECT_SETTINGS
    (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
     OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
   Values
    (SEQ_ER_OBJECT_SETTINGS.nextval, 'C', 'pat_sched_forms', 'pat_sched_col', 6, 
     1, 'Linked Forms', 0);
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_OBJECT_SETTINGS where OBJECT_SUBTYPE = 'pat_sched_site' and 
  OBJECT_NAME = 'pat_sched_col';
  if (row_count < 1) then 
   Insert into ER_OBJECT_SETTINGS
    (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
     OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
   Values
    (SEQ_ER_OBJECT_SETTINGS.nextval, 'C', 'pat_sched_site', 'pat_sched_col', 7, 
     1, 'Site of Service', 0);
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_OBJECT_SETTINGS where OBJECT_SUBTYPE = 'pat_sched_cov' and 
  OBJECT_NAME = 'pat_sched_col';
  if (row_count < 1) then 
   Insert into ER_OBJECT_SETTINGS
    (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
     OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
   Values
    (SEQ_ER_OBJECT_SETTINGS.nextval, 'C', 'pat_sched_cov', 'pat_sched_col', 8, 
     1, 'Coverage Type', 0);
  end if;
END;
/

DECLARE 
  row_count NUMBER; 
BEGIN 
  select count(*) into row_count from
  ER_OBJECT_SETTINGS where OBJECT_SUBTYPE = 'pat_sched_addl' and 
  OBJECT_NAME = 'pat_sched_col';
  if (row_count < 1) then 
   Insert into ER_OBJECT_SETTINGS
    (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
     OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
   Values
    (SEQ_ER_OBJECT_SETTINGS.nextval, 'C', 'pat_sched_addl', 'pat_sched_col', 9, 
     1, 'Additional Information', 0);
  end if;
END;
/


COMMENT on COLUMN ER_OBJECT_SETTINGS.OBJECT_TYPE is 
'Possible values: T:Tab, M:Menu, TM:Top Menu, C:Column of HTML table';

COMMENT on TABLE ER_OBJECT_SETTINGS is
'Table containing configuration data of all tabs, all menus, and some HTML columns. See how-to doc for details.';

COMMIT;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,110,6,'06_data.sql',sysdate,'8.10.0 Build#567');

commit;

