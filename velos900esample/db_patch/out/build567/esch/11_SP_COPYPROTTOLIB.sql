set define off;
create or replace
PROCEDURE        "SP_COPYPROTTOLIB" (
p_protocol IN NUMBER,
p_studyId IN NUMBER,
p_protname IN VARCHAR,
p_protdesc IN VARCHAR,
p_prottype IN NUMBER,
p_protsharewith VARCHAR,
p_userid IN NUMBER ,
P_newProtocol OUT NUMBER,
p_ip IN VARCHAR
)
AS


/****************************************************************************************************
** Procedure to copy a protocol from a study back to library


** Parameter Description

** p_protocol event id of the protocol that is to be copied in a protocol
** p_studyId study id of the study in which protocol is to be copied
** P_newProtocol protocol id of the new copied protocol

**Modified by		Date		Reason
** Sonia Abrol		06/06/06	do not send propagate_from while copying events
** Manimaran			04/18/08	to add new columns
** Sammie			04/30/2010	added new columns SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE
** Bikash      		29jul2010   SW-FIN5b added new column COVERAGE_NOTES
** JM: 				09FEB2011,  #D-FIN9: added FK_CODELST_CALSTAT and removed STATUS
*****************************************************************************************************
*/

event_id NUMBER;
chain_id NUMBER;
event_type CHAR(1);
NAME VARCHAR2(4000);--KM-08Sep09
notes VARCHAR2(1000);
COST NUMBER;
cost_description VARCHAR2(200);
duration NUMBER;
user_id NUMBER;
linked_uri VARCHAR2(200);
fuzzy_period VARCHAR2(10);
msg_to CHAR(1);
v_codestat number; --JM: 04FEB2011
v_codelst_pk number;
description VARCHAR2(1000);
displacement NUMBER;
eventMsg VARCHAR2(2000);
eventRes CHAR(1);
eventFlag NUMBER;

pat_daysbefore NUMBER(3);
pat_daysafter NUMBER(3);
usr_daysbefore NUMBER(3);
usr_daysafter NUMBER(3);
pat_msgbefore VARCHAR2(4000);
pat_msgafter VARCHAR2(4000);
usr_msgbefore VARCHAR2(4000);
usr_msgafter VARCHAR2(4000);

p_name VARCHAR2(4000);--KM-08Sep09
org_id NUMBER;
p_chain_id NUMBER;
p_currval NUMBER;
p_status NUMBER;
p_count NUMBER;

v_duration_unit CHAR(1); -- SV, 09/30/04, added to copy visits from old protocol to new study protocol.
v_event_visit_id NUMBER; -- SV, 09/30/04, fk_visit
v_ret NUMBER; -- SV, 09/30/04,
v_name VARCHAR2(4000);--KM-08Sep09
v_desc VARCHAR2(2000);
v_catlib NUMBER;
v_type NUMBER;
v_visit_num NUMBER;
--JM: 13Dec added
v_event_cptcode VARCHAR2(255);
v_event_fuzzyafter VARCHAR2(10);
v_event_durationafter CHAR(1);
v_event_durationbefore CHAR(1);
v_event_category varchar2(100); --KM
v_event_sequence number;

v_account number;
v_event_library_type number;
v_event_line_category number;

v_SERVICE_SITE_ID Number;
v_FACILITY_ID Number;
v_FK_CODELST_COVERTYPE Number;
v_COVERAGE_NOTES varchar2(4000);

-- The new protocol id generated from the EVENT_ASSOCIATION_SEQ sequence. This is stored in the -- p_chain_id
-- This variable is an OUT parameter

-- SV, 09/30/04, cal-enh- added code to copy duration_unit (type) and fk_visit (visits in event) to new protocol.
-- JM: 13Dec added EVENT_CPTCODE,EVENT_FUZZYAFTER, EVENT_DURATIONAFTER, EVENT_DURATIONBEFORE
CURSOR C1 IS SELECT EVENT_ID,
	CHAIN_ID,
	EVENT_TYPE,
	NAME,
	NOTES,
	COST,
	COST_DESCRIPTION,
	DURATION,
	USER_ID,
	LINKED_URI,
	FUZZY_PERIOD,
	MSG_TO,
	DESCRIPTION,
	DISPLACEMENT,
	ORG_ID ,
	EVENT_MSG,
	EVENT_RES,
	EVENT_FLAG,
	PAT_DAYSBEFORE,
	PAT_DAYSAFTER ,
	USR_DAYSBEFORE,
	USR_DAYSAFTER ,
	PAT_MSGBEFORE ,
	PAT_MSGAFTER ,
	USR_MSGBEFORE ,
	USR_MSGAFTER,
	DURATION_UNIT,
	EVENT_CPTCODE,EVENT_FUZZYAFTER, EVENT_DURATIONAFTER, EVENT_DURATIONBEFORE,
	FK_VISIT,FK_CATLIB,EVENT_CATEGORY,EVENT_SEQUENCE,event_library_type,event_line_category,
	SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES, fk_codelst_calstat
	FROM EVENT_ASSOC WHERE (chain_id = p_protocol OR event_id=p_protocol) ORDER BY event_type DESC ,COST,displacement;
BEGIN

	-- to check if the protocol name already exists in the study

	SELECT NAME ,fk_account INTO p_name ,v_account FROM EVENT_ASSOC ,er_study WHERE event_id=p_protocol and pk_study=chain_id;
	--KM--to fix the Bug1756
	SELECT COUNT(event_id) INTO p_count FROM EVENT_DEF WHERE LOWER(trim(NAME)) = LOWER(trim(p_protname)) AND EVENT_TYPE='P'
	and user_id = v_account ;

	IF p_count > 0 THEN
		p_newProtocol:=-1;
		RETURN;
	END IF;

	OPEN C1;

	LOOP
		--SV, 09/30/04, ++ duration_unit, fk_vist
		--JM: added v_event_cptcode,v_event_fuzzyafter,v_event_durationafter,v_event_durationbefore,
		--KM: added v_event_category,v_event_sequence
		FETCH C1 INTO event_id, chain_id , event_type, NAME, notes, COST, cost_description,
		duration, user_id, linked_uri, fuzzy_period, msg_to, description, displacement,
		org_id,eventMsg,eventRes, eventFlag,pat_daysbefore,pat_daysafter,usr_daysbefore,
		usr_daysafter,pat_msgbefore,pat_msgafter,usr_msgbefore,usr_msgafter, v_duration_unit,
		v_event_cptcode,v_event_fuzzyafter,v_event_durationafter,v_event_durationbefore,
		v_event_visit_id,v_catlib,v_event_category,v_event_sequence ,v_event_library_type,v_event_line_category,
		v_SERVICE_SITE_ID,v_FACILITY_ID,v_FK_CODELST_COVERTYPE,v_coverage_notes, v_codestat;

		SELECT EVENT_DEFINITION_SEQ.NEXTVAL INTO p_currval FROM dual;

		IF C1%ROWCOUNT = 1 THEN
			p_chain_id:=p_currval;
			p_newProtocol:=p_currval;
			--status:='W';
     select pk_codelst into v_codelst_pk from sch_codelst where codelst_type='calStatLib' and CODELST_SUBTYP='W';	
			v_codestat:=v_codelst_pk;
      
      ELSE
			p_chain_id:=p_newProtocol;
		END IF;

		IF (event_type='P') THEN
			v_name:=p_protname;
			v_desc:=p_protdesc;
			v_type:=p_prottype;
		ELSE
			v_name:=NAME;
			v_desc:=description;
			v_type:=v_catlib;
		END IF;

		--event id of the the protocol generated is the chain id of all the child events

		EXIT WHEN C1%NOTFOUND;
		--SV, ++ duration unit. visits handled separately below.
		--JM: added EVENT_CPTCODE,EVENT_FUZZYAFTER, EVENT_DURATIONAFTER, EVENT_DURATIONBEFORE,
		--JM: added v_event_cptcode,v_event_fuzzyafter,v_event_durationafter,v_event_durationbefore,
		INSERT INTO EVENT_DEF(
			EVENT_ID,
			CHAIN_ID,
			EVENT_TYPE,
			NAME,
			NOTES,
			COST,
			COST_DESCRIPTION,
			DURATION,
			USER_ID,
			LINKED_URI,
			FUZZY_PERIOD,
			MSG_TO,			
			DESCRIPTION,
			DISPLACEMENT,
			ORG_ID ,
			EVENT_MSG,
			EVENT_RES,
			EVENT_FLAG,
			PAT_DAYSBEFORE,
			PAT_DAYSAFTER ,
			USR_DAYSBEFORE,
			USR_DAYSAFTER ,
			PAT_MSGBEFORE ,
			PAT_MSGAFTER ,
			USR_MSGBEFORE ,
			USR_MSGAFTER ,
			CREATOR,
			DURATION_UNIT,
			EVENT_CPTCODE,EVENT_FUZZYAFTER, EVENT_DURATIONAFTER, EVENT_DURATIONBEFORE,
			calendar_sharedwith,fk_catlib,event_category,event_sequence, event_library_type,event_line_category,
			SERVICE_SITE_ID,FACILITY_ID,FK_CODELST_COVERTYPE,COVERAGE_NOTES, FK_CODELST_CALSTAT
		) VALUES(
			p_currval,
			p_chain_id ,
			event_type,
			v_name,
			notes,
			COST,
			cost_description,
			duration,
			user_id,
			linked_uri,
			fuzzy_period,
			msg_to,
			v_desc,
			displacement,
			org_id,
			eventMsg,
			eventRes,
			eventFlag,
			pat_daysbefore,
			pat_daysafter,
			usr_daysbefore,
			usr_daysafter,
			pat_msgbefore,
			pat_msgafter ,
			usr_msgbefore,
			usr_msgafter ,
			p_userid,
			v_duration_unit,
			v_event_cptcode,v_event_fuzzyafter,v_event_durationafter,v_event_durationbefore,
			p_protsharewith
			,v_type,
			v_event_category,
			v_event_sequence,v_event_library_type, v_event_line_category,
			v_SERVICE_SITE_ID,v_FACILITY_ID,v_FK_CODELST_COVERTYPE,v_coverage_notes,v_codestat
		);

		IF C1%ROWCOUNT = 1 THEN
			Sp_Copy_Protocol_Visits(P_PROTOCOL, P_NEWPROTOCOL, P_USERID, P_IP, V_RET); --SV, 09/30, copy "all" the visits from old to new calendar.
		END IF;

		IF C1%ROWCOUNT != 1 THEN

		 	IF (v_event_visit_id > 0) THEN
				--Get the visit no to differentiate between 2 events with same displacement
				SELECT visit_no INTO v_visit_num FROM SCH_PROTOCOL_VISIT WHERE pk_protocol_visit=v_event_visit_id;

				Sp_Get_Prot_Visit(P_NEWPROTOCOL,DISPLACEMENT, P_USERID, P_IP, v_ret,v_visit_num);

				IF (v_ret > 0) THEN
					UPDATE EVENT_DEF
					SET FK_VISIT = v_ret
					WHERE EVENT_ID = P_CURRVAL;
				END IF;
			END IF;

			Sp_Cpevedtls(event_id,p_currval,'s',p_status,TO_CHAR(p_userid),p_ip, 0);
		END IF;
	END LOOP;
	CLOSE C1;

	/**********************************
	TEST
	set serveroutput on
	declare
	newProtId number ;
	begin
	sp_addprottostudy(7739,1850,newProtId) ;
	dbms_output.put_line(to_char(newProtId) ) ;
	end ;
	**************************************/
	COMMIT;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,110,11,'11_SP_COPYPROTTOLIB.sql',sysdate,'8.10.0 Build#567');

commit;