Declare 


id_A_assoc number;
id_D_assoc number;
id_O_assoc number;
id_W_assoc number;
id_R_assoc number;

gen_id number;

Begin 

 

select pk_codelst into id_A_assoc from sch_codelst where codelst_type='calStatStd' and CODELST_SUBTYP='A';
select pk_codelst into id_D_assoc from sch_codelst where codelst_type='calStatStd' and CODELST_SUBTYP='D';
select pk_codelst into id_O_assoc from sch_codelst where codelst_type='calStatStd' and CODELST_SUBTYP='O';
select pk_codelst into id_W_assoc from sch_codelst where codelst_type='calStatStd' and CODELST_SUBTYP='W';
select pk_codelst into id_R_assoc from sch_codelst where codelst_type='calStatStd' and CODELST_SUBTYP='R';


For i in (select PK_PROTSTAT, PROTSTAT from SCH_PROTSTAT where Protstat is not null)
loop

	if    i.PROTSTAT = 'A' then gen_id:=id_A_assoc; 
	elsif i.PROTSTAT = 'D' then gen_id:=id_D_assoc; 
	elsif i.PROTSTAT = 'O' then gen_id:=id_O_assoc; 
	elsif i.PROTSTAT = 'W' then gen_id:=id_W_assoc;
	elsif i.PROTSTAT = 'R' then gen_id:=id_R_assoc;
	
	end if;

	update SCH_PROTSTAT set fk_codelst_calstat = gen_id where PK_PROTSTAT = i.PK_PROTSTAT;
	

End loop;

commit;
End;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,110,4,'04_old_data_patch_sch_protstat.sql',sysdate,'8.10.0 Build#567');

commit;