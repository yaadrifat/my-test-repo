set define off;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,93,0,'00_er_version.sql',sysdate,'8.10.0 Build#550');

commit;


Update track_patches set APP_VERSION = '8.10.0 Build#549' 
where DB_VER_MJR = 92 and APP_VERSION = '8.9.0 Build#549';

commit;