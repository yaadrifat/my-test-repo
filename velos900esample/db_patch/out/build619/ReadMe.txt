/* This readMe is specific to Velos eResearch version 9.0 build #619 */

=====================================================================================================================================
Garuda :

    We have released one Report (XSL Based) in this build. This is NMDP Specific Report.
    itext-2.1.7.jar has been included to create the report.

    For development team only: Please go  to  doc\tools\maven\Jars-garuda\ in cvs  and after taking a  CVS update, double-click on storeJarsInMavenForGaruda.bat
=====================================================================================================================================
eResearch:

=====================================================================================================================================
eResearch Localization:

Following Files have been Modified:

1	accountbrowser.jsp
2	acctformdetails.jsp
3	addeventuser.jsp
4	addevtvisits.jsp
5	addFieldToForm.jsp
6	displayDOW.jsp
7	dynadvanced.jsp
8	dynfilter.jsp
9	dynpreview.jsp
10	dynrepbrowse.jsp
11	editMulEventDetails.jsp
12	editStudyMulEventDetails.jsp
13	enrolledpatient.jsp
14	enrollpatientsearch.jsp
15	eventlibrary.jsp
16	eventlibrarycstest.jsp
17	eventmilestone.jsp
18	eventusersearch.jsp
19	fetchProt.jsp
20	fieldLibraryBrowser.jsp
21	formfilledaccountbrowser.jsp
22	formfilledcrfbrowser.jsp
23	formfilledpatbrowser.jsp
24	formfilledstdpatbrowser.jsp
25	formfilledstudybrowser.jsp
26	formusersearch.jsp
27	getlookupmodal.jsp
28	labelBundle.properties
29	LC.java
30	LC.jsp
31	MC.java
32	MC.jsp
33	mdynexport.jsp
34	mdynfilter.jsp
35	mdynpreview.jsp
36	messageBundle.properties
37	milestone.jsp
38	milestonehome.jsp
39	modifyTeam.jsp
40	modifyTeam1.jsp
41	modifyTeam2.jsp
42	multipleusersearchdetails.jsp
43	multipleusersearchdetails1.jsp
44	multipleusersearchdetailswithsave.jsp
45	myHome.jsp
46	mySubscriptions.jsp
47	notify.jsp
48	ongoingBrowser.jsp
49	patformdetails.jsp
50	patientbudget.jsp
51	patientbudget_bak.jsp
52	patientbudgetView.jsp
53	patientsearch.jsp
54	patienttabs.jsp
55	protocollist.jsp
56	searchLocation.jsp
57	sectionBrowserNew.jsp
58	selectDateRangeGeneric.jsp
59	selectDOW.jsp
60	selectevent.jsp
61	selecteventus.jsp
62	selectremoveusers.jsp
63	specimenbrowser.jsp
64	storageadminbrowser.jsp
65	storagekitbrowser.jsp
66	studybrowserpg.jsp
67	studyformdetails.jsp
68	studypatients.jsp
69	studyschedule.jsp
70	studysearchbrowser.jsp
71	studystatusbrowser.jsp
72	studyVerBrowser.jsp
73	submissionBrowser.jsp
74	teamBrowser.jsp
75	updatemultschedules.jsp
76	viemsgbrowse.jsp
77	viemsglist.jsp
78	wsPatientSearch.jsp
=====================================================================================================================================
