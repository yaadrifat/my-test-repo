SET DEFINE OFF;

DELETE FROM ER_REPXSL WHERE FK_REPORT = 161;

COMMIT;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,162,18,'18_ER_REPXSL_DELETE.sql',sysdate,'9.0.0 Build#619');

commit;