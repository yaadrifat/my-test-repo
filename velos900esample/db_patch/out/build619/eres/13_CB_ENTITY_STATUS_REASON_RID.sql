set define off;
DECLARE
  COUNTFLAG NUMBER(5);  
BEGIN
    SELECT COUNT(1) INTO COUNTFLAG FROM  CB_ENTITY_STATUS_REASON WHERE RID IS NULL;    
    IF (COUNTFLAG > 0) THEN
     update CB_ENTITY_STATUS_REASON  set RID=SEQ_RID.NEXTVAL WHERE RID IS NULL;       
commit;
end if;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,162,13,'13_CB_ENTITY_STATUS_REASON_RID.sql',sysdate,'9.0.0 Build#619');

commit;
