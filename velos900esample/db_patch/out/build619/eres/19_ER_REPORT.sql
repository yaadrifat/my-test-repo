set define off;

Update ER_REPORT set REP_SQL = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
idnotes, cbbid, storage_loc, cbu_collection_site, cbu_info_notes, eligible_status, eligibility_notes,
prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, lab_sum_notes, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,
no_of_oth_rep_alliquots_f_prod, no_of_oth_rep_alliq_alter_cond, no_of_serum_mater_aliquots, no_of_plasma_mater_aliquots,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots FROM rep_cbu_details where pk_cord = ~1' where pk_report = 161;
COMMIT;

Update ER_REPORT set REP_SQL_CLOB = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
idnotes, cbbid, storage_loc, cbu_collection_site, cbu_info_notes, eligible_status, eligibility_notes,
prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status, lab_sum_notes, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,
no_of_oth_rep_alliquots_f_prod, no_of_oth_rep_alliq_alter_cond, no_of_serum_mater_aliquots, no_of_plasma_mater_aliquots,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots FROM rep_cbu_details where pk_cord = ~1' where pk_report = 161;
COMMIT;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,162,19,'19_ER_REPORT.sql',sysdate,'9.0.0 Build#619');

commit;
