SET DEFINE OFF;

CREATE OR REPLACE TRIGGER "ERES"."ER_STUDY_NIH_GRANT_AD0"
AFTER  DELETE
ON ER_STUDY_NIH_GRANT
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
    raid NUMBER(10);
    deleted_data VARCHAR2(4000);
BEGIN

    SELECT seq_audit.NEXTVAL INTO raid FROM dual;
    audit_trail.record_transaction (raid, 'ER_STUDY_NIH_GRANT', :OLD.rid, 'D');

    deleted_data := TO_CHAR(:OLD.PK_STUDY_NIH_GRANT) ||'|'||
    TO_CHAR(:OLD.FK_STUDY) ||'|'||
    TO_CHAR(:OLD.FK_CODELST_FUNDMECH) ||'|'||
    TO_CHAR(:OLD.FK_CODELST_INSTCODE) ||'|'||
    TO_CHAR(:OLD.NIH_GRANT_SERIAL) ||'|'||
    TO_CHAR(:OLD.FK_CODELST_PROGRAM_CODE) ||'|'||
    TO_CHAR(:OLD.RID) ||'|'||
    TO_CHAR(:OLD.IP_ADD) ||'|'||
    TO_CHAR(:OLD.CREATOR) ||'|'||
    TO_CHAR(:OLD.CREATED_ON,PKG_DATEUTIL.f_get_dateformat) ||'|'||
    TO_CHAR(:OLD.LAST_MODIFIED_BY) ||'|'||
    TO_CHAR(:OLD.LAST_MODIFIED_DATE,PKG_DATEUTIL.f_get_dateformat);

    INSERT INTO audit_delete(raid, row_data) VALUES (raid, deleted_data);
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,150,14,'14_ER_STUDY_NIH_GRANT_AD0.sql',sysdate,'9.0.0 Build#607');

commit;
