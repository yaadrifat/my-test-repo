set define off;

alter table cb_antigen drop column FK_HLA_CODE_ID ;
alter table cb_antigen drop column FK_HLA_METHOD_ID;
drop table cb_search_typing;


commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,150,9,'09_Drop_CB_Antigen_Cols.sql',sysdate,'9.0.0 Build#607');

commit;
