set define off;

DECLARE
  v_tab_exists number := 0;  
BEGIN
	
	Select count(*) into v_tab_exists
    from tabs
    where TABLE_NAME = 'ER_RESP_COMPLOG';

	if v_tab_exists = 0 then
	
			execute immediate 'CREATE TABLE ERES.ER_RESP_COMPLOG (
				pk_complog number,	fk_form number, form_type varchar2(3),records_processed number,
				comp_start_date date default sysdate,comp_end_date date 	)';

			execute immediate 'COMMENT ON TABLE ERES.ER_RESP_COMPLOG IS ''This table stores form response comparison utility execution log to find discrepancies between data in er_formslinear and respective XML tables for form responses''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOG.pk_complog IS ''Primary Key''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOG.fk_form IS ''This column stores the FK to er_formlib. Identifies the form with the discrepancy''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOG.form_type IS ''This column stores the form type.Possible values: P - patient form, S: Study Form, A: account form''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOG.records_processed IS ''This column stores the total form response records processed. If the utility is running for a large number of responses, it will also show progress incrementally''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOG.comp_start_date IS ''This column stores the date/time this log record was created for the form''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOG.comp_end_date IS ''This column stores the end date/time when the utility finished for the form''';


			execute immediate 'CREATE TABLE ERES.ER_RESP_COMPLOGDET ( pk_complogdet number,	fk_resp_complog number, 
				fk_per number,	fk_patprot number,fk_study number,	fk_account number,fk_filledform number,
				field_name_section varchar2(250),field_uid varchar2(250),xml_val varchar2(4000),
				lin_val varchar2(4000), fill_date_xml date,	formresp_lmd date,formresp_created_on date)';

			execute immediate 'COMMENT ON TABLE ERES.ER_RESP_COMPLOGDET IS ''This table stores the detailed log of discrepancies between data in er_formslinear and respective XML tables for form responses''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOGDET.pk_complogdet IS ''Primary Key''';
			
			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOGDET.fk_resp_complog IS ''FK to er_resp_complog''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOGDET.fk_per IS ''This column stores the FK to er_per. Identifies the patient related with form response (if applicable)''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOGDET.fk_per IS ''This column stores the FK to ER_RESP_COMPLOG. Identifies the log original record when the utility was ran''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOGDET.fk_patprot IS ''This column stores the FK to fk_patprot. Identifies the patient-enrollment related with form response (if applicable)''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOGDET.fk_study IS ''This column stores the FK to fk_study. Identifies the study related with form response (if applicable)''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOGDET.fk_account IS ''This column stores the FK to fk_account. Identifies the account related with form response (if applicable)''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOGDET.fk_filledform IS ''This column stores the FK to fk_filledform. Identifies the PK of the response related with form response''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOGDET.field_name_section IS ''This column stores the form field name and sectiona name of the form.''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOGDET.field_uid IS ''This column stores the field_id of the form field''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOGDET.xml_val IS ''This column stores the field value stored in form xml''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOGDET.lin_val IS ''This column stores the field value stored in er_formslinear''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOGDET.fill_date_xml IS ''This column stores the form fill date value stored in form xml''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOGDET.formresp_lmd IS ''This column stores the last modified date of the form response in the XML table''';

			execute immediate 'COMMENT ON COLUMN ERES.ER_RESP_COMPLOGDET.formresp_created_on IS ''This column stores the created on date of the form response in the XML table''';

			execute immediate ' CREATE SEQUENCE ERES.SEQ_ER_RESP_COMPLOGDET   START WITH 1
			  MAXVALUE 999999999999999999999999999 	  MINVALUE 1 	  NOCYCLE   CACHE 20   NOORDER';

			 execute immediate 'CREATE SEQUENCE ERES.SEQ_ER_RESP_COMPLOG   START WITH 1  MAXVALUE 999999999999999999999999999
			  MINVALUE 1  NOCYCLE   CACHE 20  NOORDER';
			  
			 execute immediate ' ALTER TABLE ERES.ER_RESP_COMPLOG ADD (  CONSTRAINT XPKER_RESP_COMPLOG
			 PRIMARY KEY (PK_COMPLOG))';
			 
			 execute immediate ' ALTER TABLE ERES.ER_RESP_COMPLOGDET ADD (   CONSTRAINT XPKER_ER_RESP_COMPLOGDET
			 PRIMARY KEY  (pk_complogdet))';
	else
		p('no action needed. Form response Compare utility patch exists');
	end if;	
end; 
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,150,1,'01_create_table.sql',sysdate,'9.0.0 Build#607');

commit;

