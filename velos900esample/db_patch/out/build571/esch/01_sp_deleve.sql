set define off;

create or replace PROCEDURE        "SP_DELEVE" (p_eventids ARRAY_STRING , tname varchar2)
as
sqlstr varchar2(2000) ;
sqlstrMs varchar2(2000) ;

v_chain_id NUMBER;
v_cost NUMBER;
v_org_id NUMBER;
v_cnt NUMBER; --KM
v_temp NUMBER;
i NUMBER;


Begin

--SV, 11/1, to allow the fix for 1827, in we need to change this:
-- the event id passed in is the id of the calendar event whose disp = 0
-- In the fix for bug 1827, to allow prop'gation from this event to all schedules for this, the org id of the successive
-- scheduled events will point to this event id. Thus, this routine has to look for the event id and delete it and also, those records
-- (only) in this calendar and delete the schedule records with org_id = p_eventid.


--Modified by Manimaran to delete multiple events at a time
v_cnt := p_eventids.COUNT; --get the # of elements in array

   i:=1;
   WHILE i <= v_cnt LOOP


      v_temp:=TO_NUMBER(p_eventids(i));


if (lower(tname) = 'event_def') then
	  SELECT chain_id, org_id, cost
	  INTO v_chain_id, v_org_id, v_cost
	  FROM event_def WHERE event_id = v_temp;


else
	  SELECT chain_id, org_id, cost
	  INTO v_chain_id, v_org_id, v_cost
	  FROM event_assoc WHERE event_id = v_temp;
end if;


/*sqlstr := 'delete from ' || tname ||
	   	  ' WHERE chain_id = :a  and ' ||
		  ' org_id =  :b and ' ||
		  ' cost =  :c and ' ||
		  ' event_id = :x '  ;

execute immediate sqlstr using v_chain_id, v_org_id, v_cost, p_eventid;
*/

sqlstr := 'delete from ' ||tname ||
 ' where (CHAIN_ID, ORG_ID, cost ) =
    (select CHAIN_ID, ORG_ID, cost
       from ' ||tname ||
      ' where EVENT_ID = :x) ' ;

--	  insert into tmp values (sqlstr || 'USING' ||p_eventid);
--	  commit;
execute immediate sqlstr using v_temp ;



sqlstr := 'delete from ' || tname ||
	   	  ' WHERE chain_id = :a and ' ||
/** This line is commented to remove only by Cost. During copy of a calendar
/** ORG_ID does not get the correct value,so Removed.*/
--		  ' org_id = :x and ' ||
		  ' cost = :c ' ;


 -- insert into tmp values ('2 - '||sqlstr || 'USING  eventid'||p_eventid||'v_chain_id' || v_chain_id || ' v_cost ' ||v_cost);
 -- commit;
execute immediate sqlstr using v_chain_id, v_cost;


--JM: 08MAR2011: #D-FIN26
sqlstrMs := ' Delete from er_milestone where FK_EVENTASSOC= :j ' ; 
execute immediate sqlstrMs using v_temp ;


  i := i+1;
END LOOP;


end ;
/



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,114,1,'01_sp_deleve.sql',sysdate,'8.10.0 Build#571');

commit;