/* This readMe is specific to eSample (version 9.0.0 build #637-ES003) */

=====================================================================================================================================
eSample requirements covered in the build:
	INV 21675
	INV 22442

Following existing files have been modified:

	1. ajaxGetSpecimenList.jsp				(...\deploy\velos.ear\velos.war\jsp)
	2. ajaxValidateSpecimen.jsp				(...\deploy\velos.ear\velos.war\jsp)
	3. deletespecimens.jsp					(...\deploy\velos.ear\velos.war\jsp)
	4. getStorageGrid.jsp					(...\deploy\velos.ear\velos.war\jsp)
	5. MC.jsp								(...\deploy\velos.ear\velos.war\jsp)
	6. specimendetails.jsp					(...\deploy\velos.ear\velos.war\jsp)
	7. storageadminbrowser.jsp				(...\deploy\velos.ear\velos.war\jsp)
	8. storageunitdetails.jsp				(...\deploy\velos.ear\velos.war\jsp)
	9. updatespecimendetails.jsp			(...\deploy\velos.ear\velos.war\jsp)
	10. updatestorageunitdetails.jsp		(...\deploy\velos.ear\velos.war\jsp)
	11. LC.class							(...\deploy\velos.ear\velos-common.jar\com\velos\eres\service\util)
	12. MC.class							(...\deploy\velos.ear\velos-common.jar\com\velos\eres\service\util)
	13. StorageAgentBean.class				(...\deploy\velos.ear\velos-ejb-eres.jar\com\velos\eres\service\storageAgent\impl)
	14. StorageAgentRObj.class				(...\deploy\velos.ear\velos-ejb-eres.jar\com\velos\eres\service\storageAgent)
	15. StorageDao.class					(...\deploy\velos.ear\velos-ejb-eres.jar\com\velos\eres\business\common)
	16. StorageBean.class					(...\deploy\velos.ear\velos-ejb-eres.jar\com\velos\eres\business\storage\impl)
	17. StorageJB.class						(...\deploy\velos.ear\velos-main.jar\com\velos\eres\web\storage)
	
Property file changes (Do not replace the files):
	1. Copy the KEY(s) given in the file "changes_labelBundle.properties" to the existing file "labelBundle.properties" under the path:  ...\eResearch_jboss510\conf
	2. Copy the KEY(s) given in the file "changes_messageBundle.properties" to the existing file "messageBundle.properties" under the path:  ...\eResearch_jboss510\conf
=====================================================================================================================================