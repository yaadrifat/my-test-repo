set define off;

-- Insert into ER_CODELST

INSERT INTO ER_CODELST (pk_codelst,codelst_type,codelst_subtyp,codelst_desc,codelst_hide,codelst_seq)
values (seq_er_codelst.nextval,'storage_stat','PartOccupied','PartOccupied','N',9);

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,183,3,'03_ER_CODELST.sql',sysdate,'9.0.0 B#637-ES003');

COMMIT;
