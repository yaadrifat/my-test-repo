set define off;

-- Add column into ER_STORAGE

alter table ER_STORAGE 
add Storage_Multi_Specimen NUMBER(22) DEFAULT 0;
COMMENT ON COLUMN ERES.ER_STORAGE.Storage_Multi_Specimen IS 'Flag to indicate the storage is a Multi Specimen or not. Defaults 0, Possible values : 0:NO,1:YES.';

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,183,1,'01_ALTER_ER_STORAGE.sql',sysdate,'9.0.0 B#637-ES003');

COMMIT;
