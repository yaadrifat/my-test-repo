<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<!-- Commented panel.jsp as it is not required Bug No:4609 -->
<%/*  
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="srctdmenubaritem6"/> 
</jsp:include> 
*/
%>

<body>
  
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.specimen.impl.*,com.velos.eres.business.common.CodeDao, java.text.SimpleDateFormat, java.text.Format
  , com.velos.eres.web.specimen.SpecimenJB, com.velos.eres.web.specimenStatus.SpecimenStatusJB,com.velos.eres.business.common.SpecimenStatusDao"%>
  <jsp:useBean id ="specJB" scope="request" class="com.velos.eres.web.specimen.SpecimenJB"/>
  <jsp:useBean id ="qtySpecJB" scope="request" class="com.velos.eres.web.specimen.SpecimenJB"/>
  <jsp:useBean id ="specStatB" scope="request" class="com.velos.eres.web.specimenStatus.SpecimenStatusJB"/>
  <jsp:useBean id ="specJBBatch" scope="request" class="com.velos.eres.web.specimen.SpecimenJB"/>
  <jsp:useBean id ="specStatBUpd" scope="request" class="com.velos.eres.web.specimenStatus.SpecimenStatusJB"/>
  <jsp:useBean id="storageStatB" scope="page" class="com.velos.eres.web.storageStatus.StorageStatusJB" />
<jsp:useBean id="oldStorageStatB" scope="page" class="com.velos.eres.web.storageStatus.StorageStatusJB" />
<jsp:useBean id="storageB" scope="request" class="com.velos.eres.web.storage.StorageJB" />

  <%

	String mode = request.getParameter("mode");
	if (EJBUtil.isEmpty(mode))
	mode="N";

	String eSign = request.getParameter("eSign");
	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))

   {


   	String user = null;
	user = (String) tSession.getValue("userId");

	String accId = (String) tSession.getValue("accountId");

	String ipAdd = null;
	ipAdd = (String) tSession.getValue("ipAdd");

     %>
     <jsp:include page="sessionlogging.jsp" flush="true"/>

     <%

   	String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign))
	{
%>
  	   <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	}
	else
  	{

		int printme = 0;
		int printmeParent = 0;	String avQtyParent ="", parentPk ="", old_avQty ="";
		int printmeChild = 0;
		int specStatRet = 0;
		int specStatRetChild = 0;

		String  pkId = request.getParameter("specimenPkId");

		String specimenId = request.getParameter("specimenId");
		specimenId = (specimenId==null)?"":specimenId;


		String stdy = request.getParameter("selStudyIds");
		stdy = (stdy==null)?"":stdy;

		String pat = request.getParameter("patientIds");
		pat = (pat==null)?"":pat;


		//JM: 29Aug2007: added
		String  specId = "";
		String SeqFlag = "0";
		if ( (mode.equals("N") || mode.equals("M")) && specimenId.equals("")) {
			// specId = specJB.getSpecimenIdAuto();
			specJB.setFkAccount(accId);
			specId = specJB.getSpecimenIdAuto(stdy, pat);
			specId=(specId==null)?"":specId;
			SeqFlag = "1";
		}
		if (specimenId.equals("")){
		specimenId = specId ;
		}


		//today
		Date date = new java.util.Date();
		String sysDate = DateUtil.dateToString(date);
		sysDate = sysDate + " " + "00:00:00";


		//pk for default Specimen Status
		CodeDao cd = new CodeDao();
		int fkCodelstSpecimenStat = cd.getCodeId("specimen_stat", "Collected");

		int defaultProc_Stat = cd.getCodeId("specimen_stat","Processed");

		int depleted_Stat = cd.getCodeId("specimen_stat","Depleted");


		String descn = request.getParameter("description");
				descn = (descn==null)?"":descn;


		String type = request.getParameter("specType");
				type = (type==null)?"":type;


		String collDt = request.getParameter("colDate");
				collDt = (collDt==null)?"":collDt;

		String procDt = request.getParameter("procDate");
				procDt = (procDt==null)?"":procDt;

		String eProcDt = request.getParameter("eProcDate");
				eProcDt = (eProcDt==null)?"":eProcDt;


		String ownr = request.getParameter("creatorId");
				      ownr = (ownr==null)?"":ownr;

		String locn = request.getParameter("storageLoc");
				locn = (locn==null)?"":locn;

String cLocn = request.getParameter("storageLocation");
				cLocn = (cLocn==null)?"":cLocn;

String ecLocn = request.getParameter("eStorageLocation");
				ecLocn = (ecLocn==null)?"":ecLocn;


		String alterId = request.getParameter("altId");
				alterId = (alterId==null)?"":alterId;


		String avQty = request.getParameter("avlQuantity");
				avQty = (avQty==null)?"":avQty;

		String units = request.getParameter("specQuantityUnit");
				units = (units==null)?"":units;


		String site = request.getParameter("dPatSite");
				site = (site==null)?"":site;


		String visitEvt = request.getParameter("visitOrEvent");
			visitEvt = (visitEvt==null)?"":visitEvt;


		String note = request.getParameter("note");
			note = (note==null)?"":note;



		//JM: 29Jan2008: added

			String numOfChild = request.getParameter("noOfChild");
			numOfChild = (numOfChild==null)?"":numOfChild;

		String childQty = request.getParameter("quantity");
			childQty = (childQty==null)?"":childQty;

		//Parent processing type
		String pProcTyp = request.getParameter("pProcType");
			pProcTyp = (pProcTyp==null)?"":pProcTyp;

		//Child processing type
		String cProcTyp = request.getParameter("cProcType");
			cProcTyp = (cProcTyp==null)?"":cProcTyp;

	    //processing type for the existing Child
		String ecProcTyp = request.getParameter("ecProcType");
			ecProcTyp = (ecProcTyp==null)?"":ecProcTyp;


		//old Quantity
		String oldQty = request.getParameter("oldQuantity");
			oldQty = (oldQty==null)?"":oldQty;

		String fkstorage = "";

		fkstorage = request.getParameter("mainFKStorage");
		if (StringUtil.isEmpty(fkstorage))
		{
			fkstorage = "";
		}


	String selAnatomicSite =	request.getParameter("anatomic_site_dd");
	if (selAnatomicSite ==null) selAnatomicSite ="";

	String selTissueSide =	request.getParameter("tissue_site_dd");
	if (selTissueSide ==null) selTissueSide ="";

	String selPathologyStat =	request.getParameter("pathology_stat_dd");
	if (selPathologyStat ==null) selPathologyStat ="";

	String specExptdQntUnit =	request.getParameter("specExptdQntUnit_dd");
	if (specExptdQntUnit ==null) specExptdQntUnit ="";

	String specBaseOrigQntUnit =	request.getParameter("specBaseOrigQuUnit_dd");
	if (specBaseOrigQntUnit ==null) specBaseOrigQntUnit ="";

	String exptdQty = request.getParameter("expQuantity");
	exptdQty = (exptdQty==null)?"":exptdQty;

	String origQty = request.getParameter("origQuantity");
	origQty = (origQty==null)?"":origQty;

	//JM: 11Jan2010: #4614
	String copyParentDet =request.getParameter("cpParentDet");
	copyParentDet = (copyParentDet==null)?"":copyParentDet;


	String TOFreezeSS = "00";
	String TOFreezeHH = "00";
	String TOFreezeMM = "00";

	String TORemovalSS = "00";
	String TORemovalMM = "00";
	String TORemovalHH = "00";

	String collectionHH = "00";
	String collectionMM = "00";
	String collectionSS = "00";

	String procDtHH = "00";
	String procDtMM = "00";
	String procDtSS = "00";


	String eprocDtHH = "00";
	String eprocDtMM = "00";
	String eprocDtSS = "00";




	String userProcTechId = "";
	String userCollTechId = "";
	String userSurgeonId = "";
	String userPathologistId ="";
	String removalTime ="";
	String freezeTime = "";

	String collectionTime = "";
	String procDateTime = "";
	String eprocDateTime = "";


	//Child processing type
	String total_Processing_Stat = request.getParameter("num_Processing_Stat");
	//total_Processing_Stat = (total_Processing_Stat==null)?"":total_Processing_Stat;


	TOFreezeSS =  request.getParameter("TOFreezeSS");
	 if (StringUtil.isEmpty(TOFreezeSS))
	 {
	 	TOFreezeSS = "00";
	 }

	TOFreezeHH =  request.getParameter("TOFreezeHH");
	 if (StringUtil.isEmpty(TOFreezeHH))
	 {
	 	TOFreezeHH = "00";
	 }

	TOFreezeMM =  request.getParameter("TOFreezeMM");
	 if (StringUtil.isEmpty(TOFreezeMM))
	 {
	 	TOFreezeMM = "00";
	 }

	 TORemovalMM =  request.getParameter("TORemovalMM");
	 if (StringUtil.isEmpty(TORemovalMM))
	 {
	 	TORemovalMM = "00";
	 }

	 TORemovalSS =  request.getParameter("TORemovalSS");
	 if (StringUtil.isEmpty(TORemovalSS))
	 {
	 	TORemovalSS = "00";
	 }

	 TORemovalHH =  request.getParameter("TORemovalHH");
	 if (StringUtil.isEmpty(TORemovalHH))
	 {
	 	TORemovalHH = "00";
	 }

	 /* JM: 20May2009: added, enhacement #INVP2.7.1 */
	 collectionHH =  request.getParameter("collectionHH");
	 if (StringUtil.isEmpty(collectionHH))
	 {
	 	collectionHH = "00";
	 }
	 collectionMM =  request.getParameter("collectionMM");
	 if (StringUtil.isEmpty(collectionMM))
	 {
	 	collectionMM = "00";
	 }
	 collectionSS =  request.getParameter("collectionSS");
	 if (StringUtil.isEmpty(collectionSS))
	 {
	 	collectionSS = "00";
	 }


	  /* JM: 09Jun2009: added, enhacement #INVP2.9 (6) */

	 procDtHH =  request.getParameter("processHH");
	 procDtHH = (procDtHH==null)?"00":procDtHH;
	 procDtMM =  request.getParameter("processMM");
	 procDtMM = (procDtMM==null)?"00":procDtMM;
	 procDtSS =  request.getParameter("processSS");
	 procDtSS = (procDtSS==null)?"00":procDtSS;


	 eprocDtHH =  request.getParameter("eprocessHH");
	 eprocDtHH = (eprocDtHH==null)?"00":eprocDtHH;
	 eprocDtMM =  request.getParameter("eprocessMM");
	 eprocDtMM = (eprocDtMM==null)?"00":eprocDtMM;
	 eprocDtSS =  request.getParameter("eprocessSS");
	 eprocDtSS = (eprocDtSS==null)?"00":eprocDtSS;



	 userProcTechId =  request.getParameter("userProcTechId");
	 if (StringUtil.isEmpty(userProcTechId))
	 {
	 	userProcTechId = "";
	 }

	 userCollTechId =  request.getParameter("userCollTechId");
	 if (StringUtil.isEmpty(userCollTechId))
	 {
	 	userCollTechId = "";
	 }

	 userSurgeonId =  request.getParameter("userSurgeonId");
	 if (StringUtil.isEmpty(userSurgeonId))
	 {
	 	userSurgeonId = "";
	 }

	  userPathologistId =  request.getParameter("userPathologistId");
	 if (StringUtil.isEmpty(userPathologistId))
	 {
	 	userPathologistId = "";
	 }



 	 String checkedVal =  request.getParameter("checkme");



	 removalTime= TORemovalHH+ ":"+TORemovalMM + ":" + TORemovalSS;
	 freezeTime = TOFreezeHH+ ":"+TOFreezeMM + ":" + TOFreezeSS;
	 collectionTime = collectionHH+ ":"+collectionMM + ":" + collectionSS;
	 procDateTime = procDtHH+ ":"+ procDtMM + ":" + procDtSS;
	 eprocDateTime = eprocDtHH+ ":"+ eprocDtMM + ":" + eprocDtSS;






	if (mode.equals("M")){
		specJB.setPkSpecimen(EJBUtil.stringToNum(pkId));
		specJB.getSpecimenDetails();

		old_avQty = specJB.getSpecOrgQnty();
		old_avQty = (old_avQty==null)?"":old_avQty;
	}


	specJB.setSpecimenId(specimenId);
	specJB.setSpecAltId(alterId);
	specJB.setSpecDesc(descn);
	specJB.setSpecType(type);

	/* JM: 20May2009: added, enhacement #INVP2.7.1 */

	//specJB.setSpecCollDate(collDt);


	specJB.setSpecAnatomicSite(selAnatomicSite);
	specJB.setSpecTissueSide(selTissueSide);
	specJB.setSpecPathologyStat(selPathologyStat);

	specJB.setSpecExpectQuant(exptdQty);
	specJB.setSpecBaseOrigQuant(origQty);
	specJB.setSpecExpectedQUnits(specExptdQntUnit);
	specJB.setSpecBaseOrigQUnit(specBaseOrigQntUnit);


	/** #INVP2.7.1: fk_storageKit  number, hidden from front end. FK to er_storage. If specimen is stored in a kit, this column will store the PK of the top level storage of the specimen's kit instance*/
	//if (kit){
		//specJB.setFkStorageId(fkstorage);
	//}




	specJB.setSpecOwnerFkUser(ownr);

	specJB.setFkStorage(fkstorage);

	if (!numOfChild.equals("")){
		avQty = "" + (EJBUtil.stringToFloat(avQty) - (EJBUtil.stringToNum(numOfChild) * EJBUtil.stringToFloat(childQty)));
	}
	specJB.setSpecOrgQnty(avQty);



	if (mode.equals("M")){
		parentPk = qtySpecJB.getParentSpecimenId(EJBUtil.stringToNum(pkId));

		qtySpecJB.setPkSpecimen(EJBUtil.stringToNum(parentPk));
		qtySpecJB.getSpecimenDetails();

		avQtyParent = qtySpecJB.getSpecOrgQnty();
		avQtyParent = "" + ((EJBUtil.stringToFloat(avQtyParent)) + ( EJBUtil.stringToFloat(oldQty) - EJBUtil.stringToFloat(avQty)));

		qtySpecJB.setSpecOrgQnty(avQtyParent);
		qtySpecJB.setModifiedBy(user);
		printmeParent = qtySpecJB.updateSpecimen();
	}
	if (mode.equals("N")){
		specJB.setCreator(user);
	}

	specJB.setFkStudy(stdy);
	specJB.setFkPer(pat);
	specJB.setFkSite(site);
	specJB.setSpectQuntyUnits(units);
	specJB.setSpecNote(note);
	//JM: 29Jan2008
	//specJB.setSpecProcType(pProcTyp);//JM: 03June2009: #INV 2.29 (2)
	//specJB.setFkStrgKitComp("");

	specJB.setIpAdd(ipAdd);

	//JM: 19Sep2007: added fk_account, issue #3157
	specJB.setFkAccount(accId);

	//new user fields

	 specJB.setSpecUserProcessingTech(userProcTechId );
	 specJB.setSpecUserSurgeon(userSurgeonId);
	 specJB.setSpecUserPathologist(userPathologistId);
	 specJB.setSpecUserCollectingTech(userCollTechId);


	 /* JM: 20May2009: added, enhacement #INVP2.7.1 */

	 String collDtVar = "";
	 String procDtVar = "";
	 String eprocDtVar = "";
	 String collDtRemovalVar = "";
	 String collDtFreezeVar = "";



	 	if (! StringUtil.isEmpty(collDt))
	 	{
	 		collDtVar = collDt + " "+ collectionTime;
	 	}else{
	 		collDtVar = "";
	 	}
     	specJB.setSpecCollDate(collDtVar);


//Processing Date and Time for child specimen
     	if (! StringUtil.isEmpty(procDt))
	 	{
	 		procDtVar = procDt + " "+ procDateTime;
	 	}else{
	 		procDtVar = "";
	 	}



//Processing Date and Time for existing child specimen
     	if (! StringUtil.isEmpty(eProcDt))
	 	{
	 		eprocDtVar = eProcDt + " "+ eprocDateTime;
	 	}else{
	 		eprocDtVar = "";
	 	}



     	if (! removalTime.equals("00:00:00"))
	 	{
	 	if (! StringUtil.isEmpty(collDt))
	 	{
	 		collDtRemovalVar = collDt + " "+ removalTime;
	 	}
	 	else
	 	{
	 		collDtRemovalVar = DateUtil.getFromDateStringForNullDate() + " " +removalTime;

	 	}
    	specJB.setSpecRemovalDatetime(collDtRemovalVar);
     }

     if (! freezeTime.equals("00:00:00"))
	 {

	 	if (! StringUtil.isEmpty(collDt))
	 	{
	 		collDtFreezeVar = collDt + " "+ freezeTime;
	 	}
	 	else
	 	{
	 		collDtFreezeVar = DateUtil.getFromDateStringForNullDate() + " " + freezeTime;
	 	}

     	specJB.setSpecFreezeDatetime(collDtFreezeVar);
     }
     String startDate = "", storageCapacity = "", multiSpecimen = "", capacityUnit = "";
		int codelstItem = 0, storageCount = 0, storageChildCount = 0, sret = 0;
		Integer partOccupiedStat = 0, occupiedStat = 0, availableStat = 0;
		CodeDao cd5 = new CodeDao();
		partOccupiedStat = cd5.getCodeId("storage_stat",
				"PartOccupied");
		occupiedStat = cd5.getCodeId("storage_stat", "Occupied");
		availableStat = cd5.getCodeId("storage_stat", "Available");
		codelstItem = cd5.getCodeId("cap_unit", "Items");
		Date dt = new java.util.Date();
		startDate = DateUtil.dateToString(dt);
		if (mode.equals("N")) {
			if (fkstorage != null) {
				storageB.setPkStorage(EJBUtil.stringToNum(fkstorage));
				storageB.getStorageDetails();
				storageCapacity = storageB.getStorageCapNum();
				multiSpecimen = storageB.getMultiSpecimenStorage();
				capacityUnit = storageB.getFkCodelstCapUnits();
				storageCount = storageB.getStorageCount(fkstorage);
				storageChildCount = storageB.getChildCount(fkstorage);
				if (EJBUtil.stringToNum(multiSpecimen) == 0
						|| multiSpecimen == "") {
					storageStatB.setFkCodelstStorageStat(EJBUtil.integerToString(occupiedStat));
				} else {
					if (EJBUtil.stringToNum(capacityUnit) == codelstItem) {
						if (((EJBUtil.stringToNum(storageCapacity) - storageChildCount) > storageCount + 1)
								|| ((EJBUtil
										.stringToNum(storageCapacity) == 0) || storageCapacity == ""))
							storageStatB
									.setFkCodelstStorageStat(EJBUtil
											.integerToString(partOccupiedStat));
						else if ((EJBUtil
								.stringToNum(storageCapacity) - storageChildCount) == (storageCount + 1))
							storageStatB
									.setFkCodelstStorageStat(EJBUtil
											.integerToString(occupiedStat));
					} else
						storageStatB
								.setFkCodelstStorageStat(EJBUtil
										.integerToString(partOccupiedStat));
				}
				storageStatB.setFkStorage(fkstorage);
				storageStatB.setSsStartDate(startDate);
				storageStatB.setSsNotes("");
				storageStatB.setFkUser(user);
				storageStatB.setSsTrackingNum("");
				storageStatB.setFkStudy(stdy);
				storageStatB.setIpAdd(ipAdd);
				storageStatB.setCreator(user);
				sret = storageStatB.setStorageStatusDetails();

			}
		}
	if (mode.equals("M")){
		String oldFkStorage = "";
		oldFkStorage = request.getParameter("oldFKStorage");
		if (fkstorage != oldFkStorage) {
			if (fkstorage != "") {
				storageB.setPkStorage(EJBUtil
						.stringToNum(fkstorage));
				storageB.getStorageDetails();
				storageCapacity = storageB.getStorageCapNum();
				multiSpecimen = storageB
						.getMultiSpecimenStorage();
				capacityUnit = storageB.getFkCodelstCapUnits();
				storageCount = storageB
						.getStorageCount(fkstorage);
				storageChildCount = storageB
						.getChildCount(fkstorage);
				if (EJBUtil.stringToNum(multiSpecimen) == 0
						|| multiSpecimen == "") {
					storageStatB
							.setFkCodelstStorageStat(EJBUtil
									.integerToString(occupiedStat));
				} else {
					if (EJBUtil.stringToNum(capacityUnit) == codelstItem) {
						if (((EJBUtil
								.stringToNum(storageCapacity) - storageChildCount) > storageCount + 1)
								|| ((EJBUtil
										.stringToNum(storageCapacity) == 0) || storageCapacity == ""))
							storageStatB
									.setFkCodelstStorageStat(EJBUtil
											.integerToString(partOccupiedStat));
						else if ((EJBUtil
								.stringToNum(storageCapacity) - storageChildCount) == (storageCount + 1))
							storageStatB
									.setFkCodelstStorageStat(EJBUtil
											.integerToString(occupiedStat));
					} else
						storageStatB
								.setFkCodelstStorageStat(EJBUtil
										.integerToString(partOccupiedStat));
				}
				storageStatB.setFkStorage(fkstorage);
				storageStatB.setSsStartDate(startDate);
				storageStatB.setSsNotes("");
				storageStatB.setFkUser(user);
				storageStatB.setSsTrackingNum("");
				storageStatB.setFkStudy(stdy);
				storageStatB.setIpAdd(ipAdd);
				storageStatB.setCreator(user);
				sret = storageStatB.setStorageStatusDetails();
			}
			if (oldFkStorage != "") {
				storageB.setPkStorage(EJBUtil
						.stringToNum(oldFkStorage));
				storageB.getStorageDetails();
				multiSpecimen = storageB
						.getMultiSpecimenStorage();
				storageCount = storageB
						.getStorageCount(oldFkStorage);
				if (EJBUtil.stringToNum(multiSpecimen) == 0
						|| multiSpecimen == "")
					oldStorageStatB
							.setFkCodelstStorageStat(EJBUtil
									.integerToString(availableStat));
				else {
					if (storageCount == 1)
						oldStorageStatB
								.setFkCodelstStorageStat(EJBUtil
										.integerToString(availableStat));
					else if (storageCount > 1)
						oldStorageStatB
								.setFkCodelstStorageStat(EJBUtil
										.integerToString(partOccupiedStat));
				}
				oldStorageStatB.setFkStorage(oldFkStorage);
				oldStorageStatB.setSsStartDate(startDate);
				oldStorageStatB.setSsNotes("");
				oldStorageStatB.setFkUser(user);
				oldStorageStatB.setSsTrackingNum("");
				oldStorageStatB.setFkStudy(stdy);
				oldStorageStatB.setIpAdd(ipAdd);
				oldStorageStatB.setCreator(user);
				sret = oldStorageStatB
						.setStorageStatusDetails();

			}
		}
		specJB.setModifiedBy(user);
		printme = specJB.updateSpecimen();


		/////////////////////////////////

		  //update the child spec data

		  String arSelectedChild[] = null;

		  String childpk = "";

		  String eColldate = "";
		  String eType = "";

		  String eChildid="";

		  String eQuan = "";
		  String eLocation = "";

		  String eProcType = "";
		  String  eStatus = "";
		  String  eStatusDate = "";

//JM: #INV 2.30, 12Jun2009
		  String allPkValue[] = null;
		  if (!StringUtil.isEmpty(checkedVal) && checkedVal.equals("allChild")){

			 allPkValue = request.getParameterValues("childSpecIds");

		  }


		  int childcount = 0;

		  childcount =  EJBUtil.stringToNum(request.getParameter("childcount"));

		  //System.out.println("childcount " + childcount );

		  if (childcount > 1)
		  {
		  	arSelectedChild = request.getParameterValues("selectEdit");
		  } else if (childcount == 1)
		  {

		  	childpk = request.getParameter("selectEdit");

		  	if (! StringUtil.isEmpty(childpk))
		  	{
		  	arSelectedChild = new String[1];
		  	arSelectedChild[0] = childpk;
		  	}

		  //	System.out.println("childpk " + childpk );
		  	//System.out.println("arSelectedChild.length " + arSelectedChild.length );
		  }

		  double oldQuantity = 0.0;
		  double newQuantity = 0.0;

		  CodeDao statCodeDao =new CodeDao();

		  if (arSelectedChild != null)
		  {
			  for (int o = 0; o< arSelectedChild.length; o++)
				{
					childpk = arSelectedChild[o];

					//System.out.println("childpk -arr " + childpk );

					eColldate = request.getParameter( "colldate_e_"+childpk);
//JM: 22Sept2009: #4127 (Comment #12)
					eColldate = eColldate + " " + "00:00:00";

					eType = request.getParameter( "specType_"+childpk);

					eChildid = request.getParameter( "childid_"+childpk);

					eQuan = request.getParameter( "specqnty_e_"+childpk);

					eLocation = request.getParameter( "childlocation_pk_"+childpk);
					eProcType = request.getParameter( "specproctype_"+childpk);

					eStatus = request.getParameter( "specstatus_"+childpk);
					eStatusDate = request.getParameter( "specstatusdate_e_"+childpk);
					eStatusDate = eStatusDate + " " + "00:00:00";
					String oldStatus = request.getParameter( "old_status_pk_"+childpk);


		 			SpecimenJB sjbc = new SpecimenJB();

					sjbc.setPkSpecimen(EJBUtil.stringToNum(childpk));
					sjbc.getSpecimenDetails();

					sjbc.setSpecType(eType);
		            sjbc.setSpecCollDate(eColldate);
		            sjbc.setSpecimenId(eChildid);

		            oldQuantity = oldQuantity + EJBUtil.stringToFloat(sjbc.getSpecOrgQnty());
		            newQuantity = newQuantity +  EJBUtil.stringToFloat(eQuan);

		            sjbc.setSpecOrgQnty(eQuan);


		            sjbc.setFkStorage(eLocation);
		            
		            if (!eProcType.equals("") && ("process").equals(statCodeDao.getCodeCustomCol1(EJBUtil.stringToNum(eStatus)))){
		            sjbc.setSpecProcType(eProcType);
		            }else if(checkedVal == null){
		            	sjbc.setSpecProcType("");
		            }

					sjbc.setModifiedBy(user);
					sjbc.setIpAdd(ipAdd);
					sjbc.updateSpecimen();

					//new status???
					if (!StringUtil.isEmpty(eStatus) && (oldStatus!=null && !oldStatus.equals(eStatus)))
					{
						SpecimenStatusJB specCSB = new SpecimenStatusJB();

						specCSB.setFkSpecimen(childpk);
						specCSB.setSsDate(eStatusDate);
						specCSB.setFkCodelstSpecimenStat(eStatus);

//JM: 23Sep2009: #4124
						if (!eProcType.equals("") && ("process").equals(statCodeDao.getCodeCustomCol1(EJBUtil.stringToNum(eStatus)))){
							specCSB.setSsProcType(eProcType);
							specCSB.setSsProcDate(sysDate);
						}

						specCSB.setCreator(user);
						specCSB.setIpAdd(ipAdd);
						specCSB.setSpecimenStatusDetails();
					}
					
					//Ankit: Bug#9971 Date: 27-June-2012
					if ((!eProcType.equals("")) && (checkedVal == null) && ("process").equals(statCodeDao.getCodeCustomCol1(EJBUtil.stringToNum(eStatus))) && (oldStatus!=null && oldStatus.equals(eStatus)))
					{
						ArrayList pkSpecSta = new ArrayList();
						SpecimenStatusDao speStaDao = new SpecimenStatusDao();
						SpecimenStatusJB specCSB = new SpecimenStatusJB();
						
						speStaDao.getLatestSpecimenStaus(Integer.parseInt(childpk));
						pkSpecSta  = speStaDao.getPkSpecimenStats();
						if(pkSpecSta != null && pkSpecSta.size()>0 )
							specCSB.setPkSpecimenStat((Integer)pkSpecSta.get(0));
						
						specCSB.setSsProcType(eProcType);
						specCSB.setSsProcDate(sysDate);
						specCSB.updateSpecimenProcessType();
					}

				}
				//update main record for quantity
				specJB.setSpecOrgQnty(String.valueOf((EJBUtil.stringToFloat(specJB.getSpecOrgQnty()) + oldQuantity) - newQuantity));
			    printme = specJB.updateSpecimen();

			}

//JM: #INV 2.30, 12Jun2009, Apply to Selected Child Specimens: On Submit, if a process type / location is selected: Add a new 'Processed' status ...

			if ((!StringUtil.isEmpty(ecProcTyp) || !StringUtil.isEmpty(ecLocn) ) && arSelectedChild != null && !StringUtil.isEmpty(checkedVal) && checkedVal.equals("selChild")){




				for (int o = 0; o< arSelectedChild.length; o++){

					childpk = arSelectedChild[o];
					SpecimenStatusJB specExtChildStatAdd = new SpecimenStatusJB();

					specExtChildStatAdd.setFkSpecimen(childpk);
					specExtChildStatAdd.setSsDate(sysDate);
					specExtChildStatAdd.setFkCodelstSpecimenStat(defaultProc_Stat + ""); // add default processed stat.
					specExtChildStatAdd.setSsProcType(ecProcTyp);
					specExtChildStatAdd.setSsQuantity("");
					specExtChildStatAdd.setSsQuantityUnits("");
					specExtChildStatAdd.setSsAction("");
					specExtChildStatAdd.setFkStudy(stdy);
					specExtChildStatAdd.setFkUserRecpt("");
					specExtChildStatAdd.setSsTrackingNum("");
					specExtChildStatAdd.setSsNote("");
					specExtChildStatAdd.setSsStatusBy(user);
					specExtChildStatAdd.setCreator(user);
					specExtChildStatAdd.setIpAdd(ipAdd);
					specExtChildStatAdd.setSsProcDate(eprocDtVar); //Processing date and time
					//Location ...
					specStatRetChild=specExtChildStatAdd.setSpecimenStatusDetails();

					//Update the specimen processing type to the related specimen..
					 specJBBatch.setPkSpecimen(EJBUtil.stringToNum(childpk));
					 specJBBatch.getSpecimenDetails();
					 specJBBatch.setSpecProcType(ecProcTyp);
					 specJBBatch.updateSpecimen();


				}//end for

//JM: #INV 2.30, 12Jun2009,  Apply to All Child Specimens
			}else if((!StringUtil.isEmpty(ecProcTyp) || !StringUtil.isEmpty(ecLocn) ) && !StringUtil.isEmpty(checkedVal) && checkedVal.equals("allChild")){

				for (int o = 0; o< allPkValue.length; o++){


					childpk = allPkValue[o];
					SpecimenStatusJB specExtChildStatAdd = new SpecimenStatusJB();

					specExtChildStatAdd.setFkSpecimen(childpk);
					specExtChildStatAdd.setSsDate(sysDate);
					specExtChildStatAdd.setFkCodelstSpecimenStat(defaultProc_Stat + ""); // add default processed stat.
					specExtChildStatAdd.setSsProcType(ecProcTyp);
					specExtChildStatAdd.setSsQuantity("");
					specExtChildStatAdd.setSsQuantityUnits("");
					specExtChildStatAdd.setSsAction("");
					specExtChildStatAdd.setFkStudy(stdy);
					specExtChildStatAdd.setFkUserRecpt("");
					specExtChildStatAdd.setSsTrackingNum("");
					specExtChildStatAdd.setSsNote("");
					specExtChildStatAdd.setSsStatusBy(user);
					specExtChildStatAdd.setCreator(user);
					specExtChildStatAdd.setIpAdd(ipAdd);
					specExtChildStatAdd.setSsProcDate(eprocDtVar); //Processing date and time
					//Location *****************??
					specStatRetChild=specExtChildStatAdd.setSpecimenStatusDetails();

					//Update the specimen processing type to the related specimen..
					 specJBBatch.setPkSpecimen(EJBUtil.stringToNum(childpk));
					 specJBBatch.getSpecimenDetails();
					 specJBBatch.setSpecProcType(ecProcTyp);
					 specJBBatch.updateSpecimen();

				}//end for

			}

		/////////////////////////////////

	}else{

	printme = specJB.setSpecimenDetails();
	pkId = String.valueOf(printme);

	//a new record should also be entered into the er_specimen_status table as soon as a specimen added
	specStatB.setFkSpecimen(printme+"");
	specStatB.setSsDate(sysDate);
	specStatB.setFkCodelstSpecimenStat(fkCodelstSpecimenStat + "");
	specStatB.setSsQuantity("");
	specStatB.setSsQuantityUnits("");
	specStatB.setSsAction("");
	specStatB.setFkStudy(stdy);
	specStatB.setFkUserRecpt("");
	specStatB.setSsTrackingNum("");
	specStatB.setSsNote("");
	specStatB.setSsStatusBy(user);
	specStatB.setCreator(user);
	specStatB.setIpAdd(ipAdd);
	specStatRet=specStatB.setSpecimenStatusDetails();

	}


//JM: 11Sep2009: Issue No- 4289
//JM: 17Jul2009: #4128
	if ( mode.equals("M")&& Float.parseFloat(avQty)==0 && Float.parseFloat(old_avQty)> 0){

	specStatBUpd.setFkSpecimen(printme+"");
	specStatBUpd.setSsDate(sysDate);
	specStatBUpd.setFkCodelstSpecimenStat(depleted_Stat + "");
	specStatBUpd.setSsQuantity("");
	specStatBUpd.setSsQuantityUnits("");
	specStatBUpd.setSsAction("");
	specStatBUpd.setFkStudy(stdy);
	specStatBUpd.setFkUserRecpt("");
	specStatBUpd.setSsTrackingNum("");
	specStatBUpd.setSsNote("");
	specStatBUpd.setSsStatusBy(user);
	specStatBUpd.setCreator(user);
	specStatBUpd.setIpAdd(ipAdd);
	specStatRet=specStatBUpd.setSpecimenStatusDetails();

	}

	//JM: adding the child Specimens
	if (!numOfChild.equals("") && printme>0 ){

		//JM: 11Jan2009: #4614
		SpecimenJB specCretedJB = new SpecimenJB();

		specCretedJB.setPkSpecimen(printme);
		specCretedJB.getSpecimenDetails();

		HashSet <String>specimenList = new HashSet <String>();
		String pkSpecId = request.getParameter("specimenPkId");
		for (int j = 1; j <= EJBUtil.stringToNum(numOfChild); j++) {
			specimenList = specJB.getChildSpecimenId(EJBUtil
					.stringToNum(pkSpecId));
			SpecimenJB specChildJB = new SpecimenJB();
			SpecimenStatusJB specChildStatB = new SpecimenStatusJB();
			Iterator iterator = specimenList.iterator();
			if (!(specimenList.isEmpty())) {
				int big = 0;
				while (iterator.hasNext()) {
					String test = (String) iterator.next();
					int index;
					do {
						index = test.indexOf("-");
						if (index != -1) {
							test = test.substring(index + 1);
						}
					} while (index != -1);
					if (EJBUtil.stringToNum(test) >= big) 
						big = EJBUtil.stringToNum(test) + 1;
				}
				specChildJB.setSpecimenId(specimenId + "-" + big);
				specimenList.clear();
			} else {
				specChildJB.setSpecimenId(specimenId + "-" + j);
			}

			specChildJB.setSpecAltId("");

		//copy the Parent Specimen Details if checked
		if (copyParentDet.equals("on")){

		specChildJB.setSpecAnatomicSite(specCretedJB.getSpecAnatomicSite());
		specChildJB.setSpecTissueSide(specCretedJB.getSpecTissueSide());
		specChildJB.setSpecPathologyStat(specCretedJB.getSpecPathologyStat());
		specChildJB.setFkStudy(specCretedJB.getFkStudy());
		specChildJB.setFkPer(specCretedJB.getFkPer());
		specChildJB.setFkSite(specCretedJB.getFkSite());
		specChildJB.setSpecNote(specCretedJB.getSpecNote());

		specChildJB.setFkSchEvents(specCretedJB.getFkSchEvents());
		specChildJB.setFkVisit(specCretedJB.getFkVisit());

		specChildJB.setSpecDesc(specCretedJB.getSpecDesc());
		specChildJB.setSpecUserProcessingTech(specCretedJB.getSpecUserProcessingTech());
		specChildJB.setSpecUserSurgeon(specCretedJB.getSpecUserSurgeon());
		specChildJB.setSpecUserPathologist(specCretedJB.getSpecUserPathologist());
		specChildJB.setSpecUserCollectingTech(specCretedJB.getSpecUserCollectingTech());


		}else{
		specChildJB.setSpecDesc("");
		specChildJB.setFkStudy(stdy);
		specChildJB.setFkPer(pat);
		specChildJB.setFkSite(site);
		specChildJB.setSpecNote("");
		}

		specChildJB.setSpecType(type);
		specChildJB.setSpecCollDate(collDtVar);
		specChildJB.setSpecOwnerFkUser(ownr);
		specChildJB.setSpecOrgQnty(childQty);
		specChildJB.setSpectQuntyUnits(units);

//JM: 02Sep2009: #4222- point(6)
		specChildJB.setSpecBaseOrigQuant(childQty);
		specChildJB.setSpecBaseOrigQUnit(units);
		specChildJB.setSpecExpectQuant(childQty);
		specChildJB.setSpecExpectedQUnits(units);

		specChildJB.setFkSpec(""+printme); //parent child relation...
		//specChildJB.setSpecProcType(cProcTyp);
		specChildJB.setCreator(user);
		specChildJB.setIpAdd(ipAdd);
		specChildJB.setFkAccount(accId);
		printmeChild = specChildJB.setSpecimenDetails();



		//a new record should also be entered into the er_specimen_status table for each child Specimen added
		specChildStatB.setFkSpecimen(printmeChild+"");
		specChildStatB.setSsDate(sysDate);
		specChildStatB.setFkCodelstSpecimenStat(fkCodelstSpecimenStat + "");
		specChildStatB.setSsQuantity("");
		specChildStatB.setSsQuantityUnits("");
		specChildStatB.setSsAction("");
		specChildStatB.setFkStudy(stdy);
		specChildStatB.setFkUserRecpt("");
		specChildStatB.setSsTrackingNum("");
		specChildStatB.setSsNote("");
		specChildStatB.setSsStatusBy(user);
		specChildStatB.setCreator(user);
		specChildStatB.setIpAdd(ipAdd);
		specStatRetChild=specChildStatB.setSpecimenStatusDetails();

	//JM: 09Jun2009: #invp 2.9(6): Besides adding 'checked in' status, also add 'Processed' status (read using code_subtyp) along with processing date time values
	if(!cProcTyp.equals("") || !procDt.equals("")) {

			SpecimenStatusJB specChildStatAdd = new SpecimenStatusJB();

			specChildStatAdd.setFkSpecimen(printmeChild+"");
			specChildStatAdd.setSsDate(sysDate);
			specChildStatAdd.setFkCodelstSpecimenStat(defaultProc_Stat + ""); // add default processed stat.
			specChildStatAdd.setSsProcType(cProcTyp);//
			specChildStatAdd.setSsQuantity("");
			specChildStatAdd.setSsQuantityUnits("");
			specChildStatAdd.setSsAction("");
			specChildStatAdd.setFkStudy(stdy);
			specChildStatAdd.setFkUserRecpt("");
			specChildStatAdd.setSsTrackingNum("");
			specChildStatAdd.setSsNote("");
			specChildStatAdd.setSsStatusBy(user);
			specChildStatAdd.setCreator(user);
			specChildStatAdd.setIpAdd(ipAdd);
			specChildStatAdd.setSsProcDate(procDtVar); //Processing date and time
			//Location *****************??
			specStatRetChild=specChildStatAdd.setSpecimenStatusDetails();

		}



		}//end of for loop for child

	} //end of if st/ numOfChild


	if(printme>0){

	//JM: 31Aug2007: need to get the sequence generated...when a new specimen added
	if ( mode.equals("N") && !specimenId.equals("") && !SeqFlag.equals("1")) {

		    specJB.setFkAccount(accId);
		    specId = specJB.getSpecimenIdAuto(stdy, pat);
			specId=(specId==null)?"":specId;

		}



%>

      <br>
      <br>
      <br>
      <br>
      <br>
      <p class = "successfulmsg" align = "center" > <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>

      <META HTTP-EQUIV=Refresh CONTENT="1; URL=specimendetails.jsp?mode=M&pkId=<%=pkId%>&pkey=<%=pat%>&studyId=<%=stdy%>">


<%}
	else if(printme==-3){
	%>

	  <br>
      <br>
      <br>
      <br>
      <br>
      <p class = "successfulmsg" align = "center" > <%=MC.M_SpmenIdExst_GiveDiff%><%--The Specimen ID already exists, please give a different ID*****--%></p>
      <table width="70%" >
      <tr>
      <td width="10%"></td>
      <td width="60%" align="center">&nbsp;&nbsp;
      <button onclick="history.go(-1);"><%=LC.L_Back%></button>
</td></tr>	</table>

		<%

       }else{%>

      <br>
      <br>
      <br>
      <br>
      <br>
      <p class = "successfulmsg" align = "center" > <%=MC.M_DataCnt_Svd%><%--Data could not be saved*****--%></p>
      <table width="70%" >
      <tr>
      <td width="10%"></td>
      <td width="60%" align="center">&nbsp;&nbsp;
      <button onclick="history.go(-1);"><%=LC.L_Back%></button>
</td></tr>	</table>

<%}
}//end of eSign check

}//end session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

	<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>



</BODY>

</HTML>
