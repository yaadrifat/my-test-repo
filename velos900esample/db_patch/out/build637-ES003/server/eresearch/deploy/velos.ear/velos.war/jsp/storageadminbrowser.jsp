<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>

<title> <%=MC.M_MngInv_StrgSrch%><%--Manage Inventory >> Storage Search*****--%></title>
<!--  <style type="text/css">
.tableTd {
    font-family: Verdana, Arial, Helvetica,  sans-serif;
    color:black;
    font-size:8pt;
    text-align:center;
    border-style:solid;
}
</style> -->
</head>
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/prototype.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="js/dojo/dojo.js"></SCRIPT>
<script type="text/javascript" src="js/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<SCRIPT LANGUAGE="JavaScript" SRC="js/velos/ajaxengine.js"></SCRIPT>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<SCRIPT language="javascript">
function callAjaxGetStorageGrid(parent,name){
    span_legend.style.visibility = "visible";
	var agt=navigator.userAgent.toLowerCase();
	var browser = 0;
	if (agt.indexOf("msie") != -1) browser = 1;
	else if (agt.indexOf("firefox") != -1) browser = 2;
    span_storagechild.innerHTML = "";
    new VELOS.ajaxObject("getStorageGrid.jsp", {
        urlData:"parent="+parent+"&pkTopStorage="+parent+"&topStorageName="+name+
            "&gridFor=EM&dispType=G"+"&browser="+browser,
        reqType:"POST",
        outputElement: "span_storagechild" }
    ).startRequest();
}
function callAjaxGetStorageChildren(parent,parentname,gridFor,grandparent,
		   topStorage,topStorageName,level,column,row){
	   callAjaxGetStorageChildren(parent,parentname,gridFor,grandparent,
			   topStorage,topStorageName,level,column,row, null);
}
function callAjaxGetStorageChildren(parent,parentname,gridFor,grandparent,
		   topStorage,topStorageName,level,column,row, explorer){
	   var curdivs;
    //alert('parent='+parent+' gridFor='+gridFor+' grandparent='+grandparent+' topStorage='+topStorage+' topStorageName='+topStorageName+
    //       ' level='+level+' column='+column);
    span_legend.style.visibility = "visible";
    for (var iY=(level+1); iY<702; iY++) {
	       var elem = document.getElementById("span_storagechild"+iY);
	       if (elem == null) { break; }
		   elem.innerHTML = "";
    }
    var childspanname = "span_storagechild" + (level+1);

	   if (parent == topStorage) {
		   storage_path.innerHTML = "";
		   span_storagechild.innerHTML = "";
    }
    oldchHTML = span_storagechild.innerHTML;
	   // span_storagechild.innerHTML = oldchHTML + "<span ID=\""+childspanname+"\" style=\"float:left;position:relative;margin-left: -20px;\"></span>";
	   span_storagechild.innerHTML = oldchHTML + "<span ID=\""+childspanname+"\" style=\"float:left;\"></span>";

		var agt=navigator.userAgent.toLowerCase();
		var browser = 0;
		if (agt.indexOf("msie") != -1) browser = 1;
		else if (agt.indexOf("firefox") != -1) browser = 2;

		var explorerParameter = explorer == null ? "" : "&explorer="+explorer;
	   new VELOS.ajaxObject("getStorageGrid.jsp", {
		urlData:"parent="+parent+"&dispType=G&gridFor="+gridFor+"&parentName="+parentname+
		        "&pkTopStorage="+topStorage+"&topStorageName="+topStorageName+"&browser="+browser+explorerParameter,
		   reqType:"POST",
		   outputElement: childspanname }
    ).startRequest();
	   var eachTd = null;
    for (var iY=1; iY<702; iY++) {
	       eachTd = document.getElementById("td_level"+level+"_col1_row"+iY);
	       if (eachTd == null) { break; }
        for (var iX=1; iX<702; iX++) {
  	       eachTd = document.getElementById("td_level"+level+"_col"+iX+"_row"+iY);
  	       if (eachTd == null) { break; }
  	       if (iY %2 == 1) eachTd.style.backgroundColor = "#DADADA";
  	       else eachTd.style.backgroundColor = "#F7F7F7";
        }
    }
    if (row == null) row = 1;
    var td = document.getElementById("td_level"+level+"_col"+column+"_row"+row);
    if (td != null) {
        td.style.backgroundColor = "#FDFDDE";
	}
}
function callOverlib(id,name,subtype,status) {
	if (id == null || name == null || subtype == null || status == null) { return; }
	return overlib(
			'<tr><td><font size=2><%=LC.L_Storage_Id%><%--Storage ID*****--%>: </b></font></td>'+
            '<td><font size=2>'+id+'</font></td></tr>'+
            '<tr><td><font size=2><%=LC.L_Name%><%--Name*****--%>: </b></font></td>'+
            '<td><font size=2>'+name+'</font></td></tr>'+
            '<tr><td><font size=2><%=LC.L_Type%><%--Type*****--%>: </b></font></td>'+
            '<td><font size=2>'+subtype+'</font></td></tr>'+
            '<tr><td><font size=2><%=LC.L_Status%><%--Status*****--%>: </b></font></td>'+
            '<td><font size=2>'+status+'</font></td></tr>');
}
function goToStorageDetail(pkStorage) {
	location.href = 'storageunitdetails.jsp?srcmenu=tdmenubaritem6&selectedTab=2&mode=M&pkStorage='+pkStorage;
}
function goToSpecimenBrowser(pkStorage) {
	location.href = 'specimenbrowser.jsp?srcmenu=tdmenubaritem6&selectedTab=1&mainFKStorage='+pkStorage;
}
function goToSpecimenDetails(pkSpecimen) {
	location.href = 'specimendetails.jsp?mode=M&pkId='+pkSpecimen;
}
function goToPatientDetails(fkPer) {
	location.href = 'patientdetails.jsp?mode=M&srcmenu=tdmenubaritem5&selectedTab=1&page=patient&pkey='+fkPer;
}
// Added By Amarnadh for Study search Nov 16th 07
function openStudyWindow(formobj) {
	formobj.target="Lookup";
	formobj.method="post";
	formobj.action="multilookup.jsp?viewId=6013&form=storage&seperator=,"+
                  "&keyword=selStudy|STUDY_NUMBER~selStudyIds|LKP_PK|[VELHIDE]&maxselect=1";

	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');

	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="_self";
	formobj.action="storageadminbrowser.jsp";
	void(0);
}


function setFilterText(form) {

       form.storageId.value=form.storageID.value;
       form.storageTyp.value=form.storageType[form.storageType.selectedIndex].text ;
       form.specimenTyp.value=form.specimenType[form.specimenType.selectedIndex].text
       form.storageLocation.value = form.storageLoc.value;
       form.storageName.value = form.storageName.value;
       form.storageStat.value = form.storageStatus[form.storageStatus.selectedIndex].text;

	   form.dstudytxt.value=form.selStudy.value;
	   form.scannerRead.value="0"; //YK 25MAY2011 Bug#4340

       var loc = form.storageLocation.value ;



}
function setOrder(formObj,orderBy,pgRight) { //orderBy column number

    var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";
	}
	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	lselectedtab= formObj.selectedTab.value;

	formObj.action="storageadminbrowser.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType+"&selectedTab="+lselectedtab;
	setFilterText(formObj)
	formObj.submit();
}
function openwindow(formobj,pgright){

	if (! f_check_perm(pgright,'N') )
	{
		return false;
	}
     windowName =window.open('addmultiplestorage.jsp?srcmenu=tdmenubaritem6&selectedTab=1' , 'Information', 'toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=1100,height=500');
     windowName.focus();

}

function printLabelWin(formobj,pageRight)
{
    if (! f_check_perm(pageRight,'V') ) {
        return false;
    }

	var j=0;
	var cnt = 0;
	selStrs = new Array();
	totcount = formobj.totcount.value;
	var selPks = "";

	if(totcount==0) {
	    alert("<%=MC.M_SelStorUnits_ToPrint%>");<%--/* alert("Please select Storage Unit(s) to be Printed");*****/--%>
	    return false;
	}

	if (totcount==1){
		if (formobj.Del.checked==true) {
			cnt++;
			selPks = "," +  formobj.Del.value;
	  	}
	} else {
	    for(i=0;i<totcount;i++) {
	 	    if (formobj.Del[i].checked) {
			    selPks = selPks + "," + formobj.Del[i].value;
		  	    j++;
		  	    cnt++;
	 	    }
	 	}
	}
	if(cnt>0) {
		selPks = selPks + ",";
	    windowName =window.open("printMultiLabel.jsp?&labelType=storage&selPks="+selPks, "Information", "toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=700,height=450, top=100, left=90");
	    if (windowName != null) { windowName.focus(); }
	}

//JM: 14Dec2009: #4343
	  if(cnt==0)
		 {
		   alert("<%=MC.M_SelStorUnits_ToPrint%>");<%--/* alert("Please select Storage Unit(s) to be Printed");*****/--%>
		   return false;
		 }


}

function checkAll(formobj){
    act="check";
 	totcount=formobj.totcount.value;
    if (formobj.All.value=="checked")
		act="uncheck" ;
    if (totcount==1){
       if (act=="uncheck")
		   formobj.Del.checked =false ;
       else
		   formobj.Del.checked =true ;
	}
    else {
         for (i=0;i<totcount;i++){
             if (act=="uncheck")
				 formobj.Del[i].checked=false;
             else
				 formobj.Del[i].checked=true;
         }
    }


    if (act=="uncheck")
		formobj.All.value="unchecked";
    else
		formobj.All.value="checked";
}



	 function selectStorages(formobj,selVal,pgright){

	 if (! f_check_perm(pgright,'E') )
	{
		return false;
	}

	 var j=0;
	 var cnt = 0;
	 selStrs = new Array();
	 totcount = formobj.totcount.value;
	 srcmenu = formobj.srcmenu.value;

	 submit="no";
	 if(totcount==0)
	 {

	   if(selVal == 'D')
	     alert("<%=MC.M_SelStorUnits_ToDel%>");<%--/*alert("Please select Storage Unit(s) to be Deleted");*****/--%>
	   else
         alert("<%=MC.M_SelStorUnits_ToCopy%>");<%--/*alert("Please select Storage Unit(s) to be Copied");*****/--%>

	   return false;
	 }

	 if (totcount==1){
		 if (formobj.Del.checked==true){//KM-Confirmation message change.
			  if(selVal =='D')
			    msg="<%=MC.M_DelChildStr_LnkStrUnits%>";<%--/*msg="This action will also delete all the child storages linked with the selected storage unit(s). Are you sure you want to continue?";*****/--%>
			   else
				msg="<%=MC.M_ActCopyChildLnk_StrUnit%>";<%--/*msg="This action will also copy all the child storages linked with the selected storage unit(s). Are you sure you want to continue?";*****/--%>


			 if (confirm(msg))
			{
			   cnt++;

			   if(selVal =='D') {
			     // window.open("deletestorages.jsp?searchFrom=search&srcmenu="+srcmenu+"&selStrs="+formobj.Del.value,"_self");
			     formobj.action="deletestorages.jsp?searchFrom=search&srcmenu="+srcmenu+"&selStrs="+formobj.Del.value,"_self";
			     formobj.submit();
			   }
			   else
				{
				 windowName =window.open('copyStorage.jsp?srcmenu=tdmenubaritem6&selectedTab=1&selStrs='+formobj.Del.value, 'Information', 'toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=850,height=500');
			     windowName.focus();
				}
		    }
		    else
				{
					return false;
				}
		 }

	 }else{
	  for(i=0;i<totcount;i++){

	  if (formobj.Del[i].checked){
		  selStrs[j] = formobj.Del[i].value;
		  j++;
		  cnt++;
			}
		}
		if(cnt>0){

			  if(selVal == 'D')
				  msg="<%=MC.M_DelChildStr_LnkStrUnits%>";<%--/*msg="This action will also delete all the child storages linked with the selected storage unit(s). Are you sure you want to continue?";*****/--%>
			  else
				  msg="<%=MC.M_ActCopyChildLnk_StrUnit%>";<%--/*msg="This action will also copy all the child storages linked with the selected storage unit(s). Are you sure you want to continue?";*****/--%>

		if(confirm(msg)) {

			if(selVal == 'D') {
			// window.open("deletestorages.jsp?searchFrom=search&srcmenu="+srcmenu+"&selStrs="+selStrs,"_self");
			formobj.action="deletestorages.jsp?searchFrom=search&srcmenu="+srcmenu+"&selStrs="+selStrs,"_self";
			formobj.submit();
			}
			else
			{
			windowName =window.open('copyStorage.jsp?srcmenu=tdmenubaritem6&selectedTab=1&selStrs='+selStrs, 'Information', 'toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=850,height=500');
		     windowName.focus();

			}

	    }
			  else
				{
				  return false;
				}
		}

	  }
	  if(cnt==0)
		 {

		   if(selVal == 'D')
		       alert("<%=MC.M_SelStorUnits_ToDel%>");<%--/*alert("Please select Storage Unit(s) to be Deleted");*****/--%>
		   else
			   alert("<%=MC.M_SelStorUnits_ToCopy%>");<%--/*alert("Please select Storage Unit(s) to be Copied");*****/--%>

		   return false;
		 }

	  formobj.selStrs.value=selStrs;

 }


//JM: added: 14Nov2007:
 function editStatusWindow(formobj,pgright){

 	if (! f_check_perm(pgright,'E') )
	{
		return false;
	}

	 var j=0;
	 var cnt = 0;
	 selStrs = new Array();
	 totcount = formobj.totcount.value;
	 srcmenu  = formobj.srcmenu.value;

	 submit="no";
	 if(totcount==0)
	 {
	   alert("<%=MC.M_SelStorUnits_ToUpdt%>");<%--/*alert("Please select Storage Unit(s) to be updated");*****/--%>
	   return false;
	 }

	 if (totcount==1){
		 if (formobj.Del.checked==true){
			cnt++;
			//KM-Modified
			windowName =window.open("editmultiplestoragestatus.jsp?selStrs="+formobj.Del.value, "Information", "toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=1000,height=450, top=100, left=90");
			windowName.focus();
	  	 }
	  	 else{
	  	  alert("<%=MC.M_SelStorUnit_ToUpdt%>");<%--/*alert("Please select the Storage Unit(s) to be updated");*****/--%>
	   	  return false;
	  	 }

	 }else{

	  for(i=0;i<totcount;i++){

	 	if (formobj.Del[i].checked){

			selStrs[j] = formobj.Del[i].value;
		  	j++;
		  	cnt++;
		 }
		}
		if(cnt>0){
		//KM-Modified
		windowName =window.open("editmultiplestoragestatus.jsp?selStrs="+selStrs, "Information", "toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=1000,height=450, top=100, left=90");
		windowName.focus();
      }

	  if(cnt==0)
		 {
		   alert("<%=MC.M_SelStorUnits_ToUpdt%>");<%--/*alert("Please select Storage Unit(s) to be Updated");*****/--%>
		   return false;
		 }

 }

 }

function openLocLookup(formobj) {
 	/*formobj.target="Lookup";
	formobj.method="post";
	formobj.action="multilookup.jsp?viewId=6050&form=storage&seperator=,"+
                  "&keyword=storageLoc|storage_name~strCoordinateX|storage_coordinate_x|[VELHIDE]~strCoordinateY|storage_coordinate_y|[VELHIDE]~mainFKStorage|pk_storage|[VELHIDE]&maxselect=1";

	formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');
	if (formWin && !formWin.closed) formWin.focus();
	formobj.submit();
	formobj.target="_self";
	formobj.action="storageadminbrowser.jsp?page=1";
	void(0); */
	windowName= window.open("searchLocation.jsp?gridFor=BR&form=storage&locationName=storageLoc&coordx=strCoordinateX&coordy=strCoordinateY&storagepk=mainFKStorage","Grid","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=900,height=650");
	windowName.focus();
}

function setValue(formobj) {
	formobj.storageLoc.value = "";
	formobj.mainFKStorage.value ="";
}

function initEvents() {
	if (!document || !document.forms[0]) { return; }
	if (document.forms[0].barcode) {
        document.forms[0].barcode.value = "";
	}
    document.onkeypress = readScanner;
}
function readScanner(evt) {
	evt = (evt) || window.event;
    if (evt) {
        var code = evt.charCode || evt.keyCode;
        var elem = (evt.target) ? evt.target :
            ((evt.srcElement) ? evt.srcElement : null);
        var target = "";
        if (elem) {
        	if (elem.nodeType == 1) {
                // for W3C DOM property
                if (evt.relatedTarget) {
                    if (evt.relatedTarget != elem.firstChild) {
                    	target = (evt.relatedTarget.firstChild) ?
                            evt.relatedTarget.firstChild.nodeValue : null;
                    }
                // for IE DOM property
                } else if (elem.name) {
                	target = elem.name;
                }
        	}
        }
        if (!target) {
            document.storage.barcode.value = document.storage.barcode.value + String.fromCharCode(code);
            document.storage.storageID.value = document.storage.barcode.value;
            uncheckGrid();
            if (document.storage.storageID.value != '') {
                document.storage.storageId.value = document.storage.barcode.value;
                document.storage.scannerRead.value="1"; //YK 25MAY2011 Bug#4340
                setTimeout("document.storage.submit()", 1000);
            }
        }
    }
}
function uncheckGrid() {
	
    if (document.forms[1] != null) { //AK:Fixed BUG#6056 26Apr2011
    	chkShowGridView(document.forms[1]);
    }
}
function checkBarcode() {
	if (document.storage.barcode.value != '') {
		document.storage.storageID.value = document.storage.barcode.value;
		document.storage.storageId.value = document.storage.barcode.value;
	}
}



//JM: 27Aug2009: #4242
function chkShowGridView(formObj){

	if (formObj.showGridView.checked){
		formObj.showGridView.value="on";
		formObj.chkflg.value="1";

	}else{
		formObj.showGridView.value="";
		formObj.chkflg.value="0";
	}

}

//PS 17Aug2012 INV 21675 : Pritam Singh

this.name="parentWindow";

function callOverlibNew(fullTableContent) 
{
//return overlib(fullTableContent);
myWin = window.open("","spec_win", "top=350,left=350,width=200,height=100,status=no,toolbar=no,menubar=no,scrollbars=yes,addressbar=no");
myWin.document.write("<head><title>Specimen ID List</title></head>");
myWin.document.write ("<BODY>"+fullTableContent);
myWin.document.write ("</BODY></HTML>");
myWin.focus();
myWin.document.close();
}

//  AJAX function to fetch specimen ID List - Requirement No: INV 21675
function processAjaxCall(pk_storage_id) 
{
	xmlHttp = getXmlHttpObject();
	var urlParameters = "ajaxGetSpecimenList.jsp?strg_pk="+pk_storage_id;
	xmlHttp.onreadystatechange = stateChanged;
	xmlHttp.open("GET", urlParameters, true);
	xmlHttp.send(null);
}	

function getXmlHttpObject()
{
	var xmlHttp = null;
	try {
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	} catch (e) { //Internet Explorer
		try {
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function stateChanged() 
{
	if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
		if (xmlHttp.status == 200) 
		{
			showDataChild = xmlHttp.responseText;
			callOverlibNew(showDataChild);
		}
		else 
		{
			alert("Error occured in processing the data, Please try again.");
		}
	}
}

</SCRIPT>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="tdmenubaritem6"/>
</jsp:include>
<script>
window.onload = initEvents;
</script>
<body>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.StringUtil"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.*,com.velos.eres.web.user.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/> <!--Amarnadh -->

<DIV class="tabDefTopN" id="div1">
<jsp:include page="inventorytabs.jsp" flush="true">
<jsp:param name="selectedTab" value="2"/>
</jsp:include>

<%
    HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession)) {

	 int pageRight = 0;

	 GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

	 pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MSTORAGE"));

	 if (pageRight > 0 )

	{

		String accId = (String) tSession.getValue("accountId");//KM
		String src               = request.getParameter("srcmenu");
		String storageId         = request.getParameter("storageId");
		String storageStat       = request.getParameter("storageStatus");

		String storageNme        = request.getParameter("storageName");
		String storageTyp        = request.getParameter("storageType");
		String specimenTyp       = request.getParameter("specimenType");

	    String dstudytxt		 =  "" ; //Amar

		String storageLocation   = request.getParameter("storageLoc");
		String selectedTab       = request.getParameter("selectedTab") ;
		String searchFrom        = request.getParameter("searchFrom");
		String orderBy           = request.getParameter("orderBy");
		String orderType         = request.getParameter("orderType");
		String pagenum 	         = request.getParameter("page");
		String storageTypTxt     = request.getParameter("storageType");
		String dstorageType      = "";
		String dspecimenType     = "";
		String dstorageStatus    = "";
		String where             = "where ";
		String strCoordinateX = "";
		String strCoordinateY = "";
		String mainFKStorage = "";

		String templateTxt =request.getParameter("template");


		String inActiveFlag = request.getParameter("chkflg");
		if (inActiveFlag==null) inActiveFlag="1";

		String gridViewTxt =request.getParameter("showGridView");
//JM: 27Aug2009: #4242
		if (gridViewTxt==null && inActiveFlag.equals("1")){
			gridViewTxt="on";
		}
		if (gridViewTxt==null && inActiveFlag.equals("0")){
			gridViewTxt="";
		}







		strCoordinateX = request.getParameter("strCoordinateX");

		strCoordinateY = request.getParameter("strCoordinateY");
		mainFKStorage= request.getParameter("mainFKStorage");

		if (StringUtil.isEmpty(mainFKStorage))
		{
			mainFKStorage = "";

		}

		if (StringUtil.isEmpty(strCoordinateX))
		{
			strCoordinateX = "";

		}

		if (StringUtil.isEmpty(strCoordinateY))
		{
			strCoordinateY = "";

		}


		String study =  request.getParameter("dStudy");
	    if (( request.getParameter("dstudytxt"))!= null &&  (request.getParameter("dstudytxt").length() > 0))dstudytxt= request.getParameter("dstudytxt");

		CodeDao cd = new CodeDao();

		if(storageId == null )
	    	   storageId = "";

		if(study == null)
			study = "";

		if(storageNme == null )
	  	   storageNme = "";

	   if(storageLocation == null )
	      storageLocation = "";

//JM: 28Sep2009: #4302
		//cd.getCodeValues("store_type");
		cd.getCodeValuesSaveKit("store_type");


		if (templateTxt == null || templateTxt =="")
			templateTxt ="";
		else
			templateTxt ="1";


		if (storageTyp==null)
		    storageTyp="";


		if(specimenTyp ==null)
		   specimenTyp="";


		if (storageTyp.equals(""))
	 	    dstorageType=cd.toPullDown("storageType");

		else
	       dstorageType=cd.toPullDown("storageType",EJBUtil.stringToNum(storageTyp),true);


		  cd = new CodeDao();
		  cd.getCodeValues("specimen_type");



		if (specimenTyp.equals(""))
	 	    dspecimenType=cd.toPullDown("specimenType");
		else
	       dspecimenType=cd.toPullDown("specimenType",EJBUtil.stringToNum(specimenTyp),true);


		cd = new CodeDao();
		cd.getCodeValues("storage_stat");


		if (storageStat==null)
		    storageStat="";

		if (storageStat.equals(""))
	 	    dstorageStatus=cd.toPullDown("storageStatus");

		else
	        dstorageStatus=cd.toPullDown("storageStatus",EJBUtil.stringToNum(storageStat),true);

	    if (storageTypTxt==null || storageTypTxt=="")
		    storageTypTxt="All";
	    // Added By Amarnadh for Study search Nov 16th 07
		 String studyId = request.getParameter("selStudyIds");
			if (studyId==null) studyId="";

		 String studyNumber = request.getParameter("selStudy");
			if (studyNumber==null) studyNumber="";

		String tableName = "ER_STORAGE a,ER_STORAGE_STATUS b " ;
		       if(specimenTyp  != "") {
		            tableName =  tableName + ", ER_ALLOWED_ITEMS ";
		        }


		// Amarnadh Nov 16th
		String studyIdStr = "";
		studyIdStr = request.getParameter("selStudyIds");
		studyIdStr=(studyIdStr==null)?"":studyIdStr;

		//IH-Latest status fetched.
		StringBuffer sqlBuffer   = new StringBuffer();
		if ("on".equals(gridViewTxt)) {
		    sqlBuffer.append("SELECT distinct STORAGE_ID,STORAGE_NAME,PK_STORAGE ");
		    sqlBuffer.append(" ");
		    sqlBuffer.append(" FROM ").append(tableName).append(" ");
		} else {
		    sqlBuffer.append("SELECT distinct STORAGE_ID,(select codelst_desc from er_codelst where pk_codelst = a.fk_codelst_storage_type)storage_type,STORAGE_COORDINATE_X,STORAGE_COORDINATE_Y,	 ");
		    sqlBuffer.append(" nvl((select codelst_desc from er_codelst where pk_codelst = ");
		    sqlBuffer.append("   nvl((select FK_CODELST_STORAGE_STATUS from er_storage_status where FK_STORAGE = PK_STORAGE and ");
		    sqlBuffer.append("   pk_storage_status=(select max(pk_storage_status)from er_storage_status where fk_storage=PK_STORAGE and ");
		    sqlBuffer.append("   ss_start_date =(select max(ss_start_date) from er_storage_status where fk_storage =PK_STORAGE))),0) ) ");
		    sqlBuffer.append(" ,' ') as STORAGE_STATUS, ");
		    sqlBuffer.append(" STORAGE_NAME,STORAGE_CAP_NUMBER, (select codelst_desc from er_codelst where pk_codelst = fk_codelst_capacity_units) as capacity_units,PK_STORAGE,STORAGE_ISTEMPLATE, (select c.storage_name ");
		    sqlBuffer.append(" from er_storage c  where c.pk_storage = a.fk_storage) parent_storage FROM ").append(tableName).append(" ");
		}
		StringBuffer whereBuffer = new StringBuffer(where);

		// Modified By Amarnadh for Bugzilla issue #3199
	    boolean whereFlag = true;//KM-Account Id added for new requirement.
	            whereBuffer.append(" a.pk_storage = b.fk_storage AND a.FK_CODELST_STORAGE_TYPE not in (select pk_codelst from er_codelst where codelst_type='store_type' and codelst_subtyp='Kit' ) and pk_storage ");
//JM: 04Nov2009: #4433
	            whereBuffer.append(" not in ( select pk_storage from er_Storage start with fk_storage in  (select pk_storage from er_storage where FK_CODELST_STORAGE_TYPE = (select pk_codelst from er_codelst where codelst_type='store_type' and codelst_subtyp='Kit' ))connect by prior pk_storage = fk_storage) ");

	     if ("on".equals(gridViewTxt)) {
	            whereBuffer.append(" and a.fk_storage is null ");
	     }

	 	 if(storageId != "" ){
	  	   whereBuffer.append(" AND ( LOWER(STORAGE_ID) LIKE LOWER(");
		   whereBuffer.append("'%"+storageId+"%')");
	  	   whereBuffer.append("     OR LOWER(STORAGE_ALTERNALEID) LIKE LOWER(");
		   whereBuffer.append("'%"+storageId+"%') ) ");
	           whereFlag = true;
	     }



	     if( EJBUtil.stringToNum(mainFKStorage) > 0 ){
	  	   whereBuffer.append(" AND a.fk_storage = "+mainFKStorage );
		         whereFlag = true;
	     }




	     //Modified By  Amarnadh for Bugzilla issue #3199
 	     if(studyIdStr != "" ){
	            whereBuffer.append(" AND FK_STUDY = "+studyIdStr);
	     }
	     else{
	            whereBuffer.append(" and FK_ACCOUNT = '").append(accId).append("'");
	     }

		 if(storageStat != "" ){
	          if(whereFlag){
	            whereBuffer.append(" AND b.FK_CODELST_STORAGE_STATUS = ")
		                    .append("'"+storageStat+"'");//KM

	       }
	        else
	         whereBuffer.append(" b.FK_CODELST_STORAGE_STATUS = ")
		                .append("'"+storageStat+"'");//KM
		       whereFlag = true;
	       }
	      if(storageNme != "" ){
	       if(whereFlag){
	        whereBuffer.append(" AND  LOWER(STORAGE_NAME) LIKE LOWER(")
		               .append("'%"+storageNme+"%')");

	      }
	      else
	       whereBuffer.append("  LOWER(STORAGE_NAME) LIKE LOWER(")
		              .append("'%"+storageNme+"%')");
		    whereFlag = true;
	      }
	     if(storageTyp != "" ){

	      if(whereFlag){
	         whereBuffer.append(" AND  a.FK_CODELST_STORAGE_TYPE =")
		                .append(storageTyp);
	     }
	     else
	        whereBuffer.append("  a.FK_CODELST_STORAGE_TYPE =")
		               .append(storageTyp);
	        whereFlag = true;
	    }


	    if(specimenTyp  != ""){
	       if(whereFlag){
	          whereBuffer.append(" AND FK_CODELST_SPECIMEN_TYPE  = " )
		      .append(specimenTyp)
			  .append(" AND a.PK_STORAGE = ER_ALLOWED_ITEMS.FK_STORAGE");

	      }
	       else{
	          whereBuffer.append(" FK_CODELST_SPECIMEN_TYPE  = ")
		      .append(specimenTyp)
			  .append(" AND a.PK_STORAGE = ER_ALLOWED_ITEMS.FK_STORAGE");

	        }
	        whereFlag = true;

	    }

		// Added by Manimaran for Template records.

		 if(templateTxt != "" ){
	        if(whereFlag){

	       whereBuffer.append(" AND STORAGE_ISTEMPLATE = ")
		             .append(templateTxt);

	    }
	    else
	       whereBuffer.append(" STORAGE_ISTEMPLATE = ")
		              .append(templateTxt);
	        whereFlag = true;
	    }

		// Add order by
		if ("on".equals(gridViewTxt)) {

			//JM: 16Jul2009: #4118
			//whereBuffer.append(" order by a.storage_name ");
			orderBy="lower(a.STORAGE_NAME)";
			orderType = "asc";
		}

	     if(whereFlag)
	        sqlBuffer.append(whereBuffer);

	     String sql = sqlBuffer.toString();

	     //out.println(sql);




		long rowsPerPage    = 0;
		long totalPages     = 0;
		long rowsReturned   = 0;
		long showPages      = 0;
		long totalRows      = 0;
		long firstRec       = 0;
		long lastRec        = 0;
		boolean hasMore     = false;
		boolean hasPrevious = false;


		String storageTypTxtDesc = "";
		String countsql = " Select count(*) from ( " + sql + " )";

		   if (EJBUtil.isEmpty(orderBy))
			orderBy = "lower(STORAGE_ID)";

		   if (orderType == null)
		        orderType = "asc";

		   //KM-#3853
		   if (orderBy.equals("LOWER(CAPACITY_UNITS)"))
			   orderType = orderType + ", storage_cap_number " +orderType;

		   rowsPerPage =   Configuration.ROWSPERBROWSERPAGE ;
		   totalPages  =   Configuration.PAGEPERBROWSER ;
		   int curPage = 0;
		   long startPage = 1;
		   long cntr = 0;
		   //if (pagenum == null)
		       //pagenum = "1";	//JM: 19Nov2009: #4344


		  curPage        = EJBUtil.stringToNum(pagenum);
//JM: 23Jul2009: issue no-4118(Comment #5)
		  curPage = (curPage==0)?1:curPage;

		  BrowserRows br = new BrowserRows();


		  if (pagenum != null) {
		      if ("on".equals(gridViewTxt)) {
		          rowsPerPage = totalPages = 1000;
		      }
	  	      br.getPageRows(curPage,rowsPerPage,sql,totalPages,countsql,orderBy,orderType);
		  }


		  rowsReturned   = br.getRowReturned();
		  showPages      = br.getShowPages();
		  startPage      = br.getStartPage();
		  hasMore        = br.getHasMore();
		  hasPrevious    = br.getHasPrevious();
		  totalRows      = br.getTotalRows();
		  firstRec       = br.getFirstRec();
		  lastRec        = br.getLastRec();

		  if(orderType.indexOf(",")!= -1) {
			  int val = orderType.indexOf(",");
			  orderType = orderType.substring(0,val);
		  }

	%>

	<form name="storage" method="post" action="storageadminbrowser.jsp?page=1" onSubmit="checkBarcode();return    setFilterText(document.storage)">
	<input type="hidden" name="searchFrom" Value="search">
	<Input type="hidden" name="page" value="<%=curPage%>">
	<Input type="hidden" name="orderBy" Value="<%=orderBy%>">
	<Input type="hidden" name="orderType" Value="<%=orderType%>">
	<input type="hidden" name="storageId">
	<input type="hidden" name="dstudytxt">
	<input type="hidden" name="storageTyp">
	<input type="hidden" name="specimenTyp">
	<input type="hidden" name="storageLocation">
	<input type="hidden" name="storageStat">
	<input type="hidden" name="srcmenu" Value="<%=src%>">
	<input type="hidden" name="selectedTab" Value=<%=selectedTab%>>
	<input type="hidden" name="totcount" Value="<%=rowsReturned%>">
	<input type="hidden" name="selStrs" >



<input type="hidden" name="checkedVal" value="<%=gridViewTxt%>">
<input type="hidden" name="chkflg" value="<%=inActiveFlag%>">
<input type="hidden" name="scannerRead" value="0"> <!-- YK 25MAY2011 Bug#4340 -->



	</DIV>

	<DIV class="tabFormTopN tabFormTopN_PAS"  id="div2">

	<table class="basetbl outline midAlign lhsFont" width="100%" cellspacing="0" cellpadding="0" border="0"  >
	<tr >
		<td><%=LC.L_Storage_Id%><%--Storage ID*****--%>:</td><td><Input type = "text" name="storageID"  value = "<%=storageId%>" size=15 align = right>
		</td>

	        <td><%=LC.L_Storage_Type%><%--Storage Type*****--%>:</td>
		<td ><%=dstorageType%></td>

		<td><%=LC.L_Location%><%--Location*****--%>:</td><td><Input readonly type = "text" name="storageLoc" value =  "<%=storageLocation%>" size=15 align = right>

			<Input type = "hidden" name="mainFKStorage" value = "<%=mainFKStorage%>" size=25 MAXLENGTH = 250 align = right>

			<Input type = "hidden" name="strCoordinateX" value = "<%=strCoordinateX%>" size=25 MAXLENGTH = 250 align = right>
			<Input type = "hidden" name="strCoordinateY" value = "<%=strCoordinateY%>" size=25 MAXLENGTH = 250 align = right>

			<A  href="javascript:void(0);" onClick="return openLocLookup(document.storage)" ><%=LC.L_Select%><%--Select*****--%></A>&nbsp;
			<A href="#" onClick="setValue(document.storage)"> <%=LC.L_Remove%><%--Remove*****--%></A>

		</td>

	</tr>

	<tr >
		<td><%=LC.L_Name%><%--Name*****--%>:</td><td><Input type = "text" name="storageName" value = "<%=storageNme%>" size=15>
		</td>

		<td><%=LC.L_Allowed_Specimens%><%--Allowed Specimens*****--%>:</td>
		<td ><%=dspecimenType%></td>

	    <%
		String checkGridView="";

		if (gridViewTxt == null || "".equals(gridViewTxt)){
		    checkGridView ="";


		}else{
		    checkGridView ="checked";

		}
		%>

	<!--
		<td colspan=2> <input type="checkbox" name="showGridView" <%=checkGridView%> > Show in Grid View
		</td>


	-->



<% if ("on".equals(gridViewTxt)){%>
<td colspan=2> <input type="checkbox" name="showGridView" onclick=" return chkShowGridView(document.storage)" checked> <%=MC.M_Show_InGridView%><%--Show in Grid View*****--%>
</td>
<%}else if(gridViewTxt==null && inActiveFlag.equals("1")){%>
<td colspan=2> <input type="checkbox" name="showGridView" onclick=" return chkShowGridView(document.storage)" checked> <%=MC.M_Show_InGridView%><%--Show in Grid View*****--%>
</td>
<%}else if(!("on".equals(gridViewTxt)) && inActiveFlag.equals("0")){%>
<td colspan=2> <input type="checkbox" name="showGridView" onclick=" return chkShowGridView(document.storage)" > <%=MC.M_Show_InGridView%><%--Show in Grid View*****--%>
</td>
<%}%>


	<tr >
		<td ><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%>:</td>
		<input type="hidden" name="selStudyIds" value="<%=studyId%>">

		<td ><Input type = "text" name="selStudy"  value="<%=studyNumber%>" size=15 readonly >
		<A href="#" onClick="openStudyWindow(document.storage)" ><%=LC.L_Select%><%--Select*****--%></A>
		</td>

		<td><%=LC.L_Status%><%--Status*****--%>:</td>
		<td ><%=dstorageStatus%></td>
	<!-- Code Changed By Amarnadh for Study search Nov 16th 07  -->



	<%
		//KM
		String checkStr="";
		if (templateTxt.equals("1"))
			checkStr ="checked";
		else
			checkStr ="";
		%>

	    	 <td colspan="2" align="left"> <input type="checkbox" name="template" <%=checkStr%> > <%=LC.L_Show_TemplatesOnly%><%--Show Templates Only*****--%>
		   	&nbsp;<button type="submit"><%=LC.L_Search%></button>
		 </td>

	</tr>
	</table>
<div class="tmpHeight"></div>
	<table width="100%" cellspacing="0" cellpadding="0" border="0" class="searchBg">
    <tr  height="35">
	<td width=24%>
    <%=LC.L_Barcode%><%--Barcode*****--%>:&nbsp;
    <input id="barcode" name="barcode" type="text" size=12 onKeyPress="uncheckGrid()">
	</td>
 	<td >
	<td width=13%>
	<A href="storageunitdetails.jsp?srcmenu=tdmenubaritem6&selectedTab=2&mode=N" onClick="return f_check_perm(<%=pageRight%>,'N') ;"><%=LC.L_Add_New%><%--ADD NEW*****--%></A>
	</td>
	<td width=13%>
	<A href="#" onClick="return openwindow(document.storage,<%=pageRight%>);"  ><%=LC.L_Add_Multi%><%--ADD MULTIPLE*****--%></A>
	</td>
	<td width=13%>
	<% if (!"on".equals(gridViewTxt)) { %>
	<A href="#" onClick="return editStatusWindow(document.storage,<%=pageRight%>);" ><%=LC.L_Update_Status%><%--UPDATE STATUS*****--%></A>
	<% } %>
	</td>
	<td width=10%>
	<% if (!"on".equals(gridViewTxt)) { %>
	<A href="#" onClick="return printLabelWin(document.storage,<%=pageRight%>);"><%=LC.L_Print_Label%><%--PRINT LABEL*****--%></A>
	<% } %>
	</td>
	<td width=10%>
	<% if (!"on".equals(gridViewTxt)) { %>
	<A href="#" onClick ="return selectStorages(document.storage,'D',<%=pageRight%>)" ><%=LC.L_Delete%><%--DELETE*****--%></A>
	<% } %>
	</td>
	<td width=17%> <!--KM -to copy storageunit/template -->
	<% if (!"on".equals(gridViewTxt)) { %>
	<A href="#" onClick ="return selectStorages(document.storage,'C',<%=pageRight%>)" ><%=MC.M_CopyStor_UntOrTpl%><%--COPY STORAGE UNIT/TEMPLATE*****--%></A>
	<% } %>
	</td>
	</tr>
	</table>

	</DIV>
	<DIV class="tabFormBotN tabFormBotN_MI_1" id="div3">

	<%
	/*//YK 25MAY2011 Bug#4340*/
	 String scannerReader = request.getParameter("scannerRead");
	 scannerReader=(scannerReader==null)?"0":scannerReader;
	   // Embedded Grid Viewer
	   if ("on".equals(gridViewTxt)) {
	%>
    <span ID="storage_path" ></span>
    <div ID="span_storage" style="float:left;overflow:auto;width:200px;height:100%;" >
    <table class="basetbl outline" width="100%" cellspacing="0" cellpadding="0" border="0">
	<%
	       String resultMsg = "";
	       if (rowsReturned < 1) {
	           resultMsg = MC.M_NoRec_MdfCrit;/*resultMsg = "No matching records found. Please modify your criteria and try again.";*****/
	       } else {   
	    	   String gPkStorage, gStrId, gStrName;/*YK 25MAY2011 Bug#4340*/
	    	   if ( scannerReader.equalsIgnoreCase("0") || (scannerReader.equalsIgnoreCase("1") && rowsReturned>1)){
 	%>
               <TH><%=LC.L_Storage_Name%><%--Storage Name*****--%></TH><TH><%=LC.L_Id_Upper%><%--ID*****--%></TH>
    <%
	    	   for(int i = 1 ; i <=rowsReturned ; i++) {
	    	       gPkStorage = br.getBValues(i,"PK_STORAGE");
	    	       gStrId     = br.getBValues(i,"STORAGE_ID");
	    		   if(gStrId.length() > 35) gStrId = gStrId.substring(0,35) + "...";
	    		   gStrName   = br.getBValues(i,"STORAGE_NAME");
	    		   if(gStrName.length() > 35) gStrName = gStrName.substring(0,35) + "...";
	%>
	<%             if ((i%2)!=0) {    %>
				<tr class="browserEvenRow">
    <%             } else {           %>
				<tr class="browserOddRow">
    <%             }                  %>
               <td>
                   <A href="#" onClick="callAjaxGetStorageGrid(<%=gPkStorage%>,'<%=gStrName%>');"><%=gStrName%></A><br/>
                   <A href="#" onClick="goToStorageDetail(<%=gPkStorage%>);"><%=LC.L_Select%><%--Select*****--%></A></td><td><%=gStrId%></td>
               </tr>
	<%
	    	   } 
	       }else if (rowsReturned==1){ /*//YK 25MAY2011 Bug#4340*/
	    	   gPkStorage = br.getBValues(1,"PK_STORAGE");
	    		%>
	    		<script type="text/javascript">
	    		goToStorageDetail(<%=gPkStorage%>);
	    		</script>
	  <% } 
	 } %>
    </table></div>
  	<P class="defComments_txt" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=resultMsg%></P>
    <span ID="span_storagechild" style="overflow:auto;float:left;width:680px;height:100%;"></span>
    <span ID="span_legend" style="visibility: hidden">
    <br/>
    <table align="left">
    <tr align="left">
    <td class="tdDefault" width="30"><img src="images/occupied.jpg"></img></td>
    <td class="tdDefault" width="60"><%=LC.L_Occupied%><%--Occupied*****--%></td>
    </tr>
    <tr align="left">
    <td class="tdDefault" width="30"><img src="images/part_occ.jpg"></img></td>
    <td class="tdDefault" width="60"><%=LC.L_Partially_Occupied%><%--Partially Occupied*****--%></td>
    </tr>
    <tr align="left">
    <td class="tdDefault" width="30"><img src="images/available.jpg"></img></td>
    <td class="tdDefault" width="60"><%=LC.L_Available%><%--Available*****--%></td>
    </tr>
    </table>
    </span>
    <div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
    </DIV>
</body>
</html>
    <%
           return;
	   } %>


	<%

	   if(rowsReturned > 0) {
		   String pkStorage;
		   //String pkStorageStat;
		   String strId ="";
		   String strName = "";
		   String studynum = ""; //Amar
		   String strType ="";
		   String strCO_ORDINATE_X ="";
		   String strCO_ORDINATE_Y ="";
		   String strCapacityNumber = "";
		   String strCapacityUnits = " ";
		   String strStatus = "";
		   String rNum = "";
		   String strlocation;
		   String strTypeDesc ="";
		   String strStatusDesc = "";
		   String template ="";
		   CodeDao cdDesc = new CodeDao();
		   
		   /*//YK 25MAY2011 Bug#4340*/
		   if ( scannerReader.equalsIgnoreCase("0") || (scannerReader.equalsIgnoreCase("1") && rowsReturned>1)){
	%>



	<table class="basetbl outline" width="100%" cellspacing="0" cellpadding="0" border="0">

	<TR>
	     <th width=10% onClick="setOrder(document.storage,'LOWER(STORAGE_ID)')" align =center><%=LC.L_Id_Upper%><%--ID*****--%> &loz;</th>
	     <th width=14% onClick="setOrder(document.storage,'LOWER(STORAGE_NAME)')" align =center><%=LC.L_Name%><%--Name*****--%>  &loz;</th>
	     <th width=14% onClick="setOrder(document.storage,'LOWER(STORAGE_TYPE)')"align =center><%=LC.L_Type%><%--Type*****--%>  &loz;</th> <!--KM-->
	     <th width=14% onClick="setOrder(document.storage,'LOWER(PARENT_STORAGE)')" align =center><%=LC.L_Location%><%--Location*****--%> &loz;</th>
	     <th width=10% onClick="setOrder(document.storage,'LOWER(CAPACITY_UNITS)')" align =center><%=LC.L_Capacity%><%--Capacity*****--%> &loz;</th>
	     <th width=12% onClick="setOrder(document.storage,'LOWER(STORAGE_STATUS)')" salign =center ><%=LC.L_Status%><%--Status*****--%>  &loz;</th>
	     <!-- <th width=10%  align =center >Print</th> -->
	     <th width=10%  align =center ><%=LC.L_Select%><%--Select*****--%><input type="checkbox" name="All" value="" onClick="checkAll(document.storage)"></th>
	</TR>


<%
	 
	   for(int i = 1 ; i <=rowsReturned ; i++) {
	    strlocation="";
	    pkStorage = br.getBValues(i,"PK_STORAGE");
		template = br.getBValues(i,"STORAGE_ISTEMPLATE");

	    strId     = br.getBValues(i,"STORAGE_ID");
	    if(strId.length() > 35)
	      strId = strId.substring(0,35) + "...";

		studynum = br.getBValues(i,"studynum");
	    studynum=(studynum==null)?"":studynum;

		//KM-Modified for 3853
		strTypeDesc   = br.getBValues(i,"storage_type");


		strCO_ORDINATE_X = br.getBValues(i,"STORAGE_COORDINATE_X");
	    strCO_ORDINATE_Y = br.getBValues(i,"STORAGE_COORDINATE_Y");


		strStatusDesc = br.getBValues(i,"STORAGE_STATUS");//KM


	      strName   = br.getBValues(i,"STORAGE_NAME");
	      if(strName.length() > 35)
	      strName = strName.substring(0,35) + "...";
	      strCapacityNumber = br.getBValues(i,"STORAGE_CAP_NUMBER");

		  strCapacityUnits  = br.getBValues(i,"CAPACITY_UNITS");
	      //strCapacityUnits  = br.getBValues(i,"CAPACITY_UNITS");
	      /*String strCapacityUnitsDesc = "" ;

	     if(strCapacityUnits != null)
	        strCapacityUnitsDesc =  cd.getCodeDescription(Integer.parseInt(strCapacityUnits));
	     else
	        strCapacityUnitsDesc = "";
		*/
		 String strCapacityNumberAndUnits = "";


	    if(strCapacityUnits == null || strCapacityNumber == null)
	      strCapacityNumberAndUnits = "";

	      else
	      strCapacityNumberAndUnits = strCapacityNumber+" "+strCapacityUnits ;



		strlocation =  br.getBValues(i,"parent_storage");

		if (StringUtil.isEmpty(strlocation))
		{
			strlocation= "";
		}

		rNum = br.getBValues(i,"RNUM");

		if (template == null)
			template ="";
		//KM-010408
		if(template.equals("1"))
			template = "[Template]";
		else
			template="";

	    if ((i%2)!=0) {
	    %>

	      <tr class="browserEvenRow">

	   <%

	   }
	   else {

	   %>

	      <tr class="browserOddRow">

	 <%
	    }
	 %>
	    <td width =10%><A href="storageunitdetails.jsp?srcmenu=tdmenubaritem6&selectedTab=2&pkStorage=<%=pkStorage%>&mode=M"><%=strId%></A></td>
	     <td width =14%><%=strName%> </td>
	     <td width =14%><%=strTypeDesc%> &nbsp;&nbsp;&nbsp; <%=template%> </td> <!--KM-->
	     <td width =14%><%=strlocation%></td>
	     <td width =10%><%=strCapacityNumberAndUnits%></td>
	     <td width =12%><%=strStatusDesc%></td>
	     <!-- <td width =10% align="center"><A href="#"> <img src="./images/printer.gif" border="0" align="center"/></A></td> -->
	     <td width =10% align="center"><input type="checkbox" name="Del" value="<%=pkStorage%>"></td>


	 <%
	  }
	%>
	</table>
<%}else if (rowsReturned==1){ /*//YK 25MAY2011 Bug#4340*/
	pkStorage = br.getBValues(1,"PK_STORAGE");
	
	%>
	<script type="text/javascript">
	goToStorageDetail(<%=pkStorage%>);
	</script>
	<%}%>
	<P class="defComments"><%=MC.M_Your_SelcFiltersAre%><%--Your selected filters are*****--%>:

	        <%
		if(storageId.equals("All") || storageId == null || storageId.equals("") ) {
		%>
		<%=LC.L_Storage_Id%><%--Storage ID*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Storage_Id%><%--Storage ID*****--%>:&nbsp;<B><I><u><%=storageId%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>


		<%
		if(storageTypTxt.equals("All") || storageTypTxt == null) {
		%>
		<%=LC.L_Storage_Type%><%--Storage
		 type*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Storage_Type%><%--Storage
		 type*****--%>:&nbsp;<B><I><u><%=cd.getCodeDescription(Integer.parseInt(storageTypTxt))%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		 <%
		if(storageLocation.equals("All") || storageLocation == null || storageLocation.equals("")) {
		%>
		 <%=LC.L_Location%><%--Location*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Location%><%--Location*****--%>:&nbsp;<B><I><u><%=storageLocation%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		 <%
		if(storageNme.equals("All") || storageNme == null || storageNme.equals("")) {
		%>
		<%=LC.L_Name%><%--Name*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Name%><%--Name*****--%>:&nbsp;<B><I><u><%=storageNme%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		 <!--Added By Amarnadh For  Bugzilla issue 3200 Nov 19th 2007 -->
		 <%
		if(specimenTyp.equals("All") || specimenTyp == null || specimenTyp.equals("")) {
		%>
		<%=LC.L_Allowed_Specimens%><%--Allowed Specimens*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
	    <%=LC.L_Allowed_Specimens%><%--Allowed Specimens*****--%>:&nbsp;<B><I><u><%=cd.getCodeDescription(Integer.parseInt(specimenTyp))%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		<%
		if(storageStat.equals("All") || storageStat == null || storageStat.equals("")) {
		%>
		<%=LC.L_Status%><%--Status*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Status%><%--Status*****--%>:&nbsp;<B><I><u><%=cd.getCodeDescription(Integer.parseInt(storageStat))%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		 <!--Modified By Amarnadh  for Bugzilla issue #3200 Nov 18th 2007-->
		 <%
		if(dstudytxt.equals("All") || dstudytxt == null || dstudytxt.equals("")) {
		%>
		<%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%>:&nbsp;<B><I><u><%=dstudytxt%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		</P>

	<table class="tableDefault" align=left  >
			<tr valign="top"><td>

		<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
			<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<%} %>	</td></tr></table>

		<table class="tableDefault" align=center height="50px">
			<tr valign="top">

	      <%
	       if (curPage==1) startPage=1;

	        for (int count = 1; count <= showPages;count++) {
	        cntr = (startPage - 1) + count;
	        if ((count == 1) && (hasPrevious)) {

	       %>

	  	<td> <A href="storageadminbrowser.jsp?selectedTab=1&searchFrom=initials&srcmenu=<%=src%>&selStudyIds=<%=studyIdStr%>&storageId=<%=storageId %>&storageName=<%=storageNme%>&storageType=<%=storageTyp%>&specimenType=<%=specimenTyp%>&storageLoc=<%=storageLocation %>&storageStatus=<%=storageStat%>&dstudytxt=<%=dstudytxt%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&template=<%=templateTxt%>&chkflg=<%=inActiveFlag%>&showGridView=<%=gridViewTxt%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<%
	  	}
		%>
		<td>
		<%

		 if (curPage  == cntr)
		 {
	        %>
			<FONT class = "pageNumber"><%= cntr %></Font>
	       <%
	       }
	      else
	        {
	       %>
		<A href="storageadminbrowser.jsp?selectedTab=1&searchFrom=initials&srcmenu=<%=src%>&selStudyIds=<%=studyIdStr%>&storageId=<%=storageId %>&storageName=<%=storageNme%>&storageType=<%=storageTyp%>&specimenType=<%=specimenTyp%>&storageLoc=<%=storageLocation %>&storageStatus=<%=storageStat%>&dstudytxt=<%=dstudytxt%>&orderBy=<%=orderBy%>&page=<%=cntr%>&orderType=<%=orderType%>&template=<%=templateTxt%>&chkflg=<%=inActiveFlag%>&showGridView=<%=gridViewTxt%>"><%=cntr%></A>
	       <%
	    	}
		 %>
		</td>
		<%
		  }

		if (hasMore)
		{

	     %>
	     <td colspan = 3 align = center>
	  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="storageadminbrowser.jsp?selectedTab=1&searchFrom=initials&srcmenu=<%=src%>&selStudyIds=<%=studyIdStr%>&storageId=<%=storageId %>&storageName=<%=storageNme%>&storageType=<%=storageTyp%>&specimenType=<%=specimenTyp%>&storageLoc=<%=storageLocation %>&storageStatus=<%=storageStat%>&dstudytxt=<%=dstudytxt%>&orderBy=<%=orderBy%>&page=<%=cntr+1%>&orderType=<%=orderType%>&template=<%=templateTxt%>&chkflg=<%=inActiveFlag%>&showGridView=<%=gridViewTxt%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%> ></A></td>
		<%
	  	}
		%>
	   </tr>
	  </table>
	 <%
	   }
	   else {
	     %>
	      <P class="defComments"><%=MC.M_Your_SelcFiltersAre%><%--Your selected filters are*****--%>:

	        <%
		if(storageId.equals("All") || storageId == null || storageId.equals("") ) {
		%>
		<%=LC.L_Storage_Id%><%--Storage ID*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Storage_Id%><%--Storage ID*****--%>:&nbsp;<B><I><u><%=storageId%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>


		<%
		if(storageTypTxt.equals("All") || storageTypTxt == null) {
		%>
		<%=LC.L_Storage_Type%><%--Storage
		 type*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Storage_Type%><%--Storage
		 type*****--%>:&nbsp;<B><I><u><%=cd.getCodeDescription(Integer.parseInt(storageTypTxt))%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		 <%
		if(storageLocation.equals("All") || storageLocation == null || storageLocation.equals("")) {
		%>
		 <%=LC.L_Location%><%--Location*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Location%><%--Location*****--%>:&nbsp;<B><I><u><%=storageLocation%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		 <%
		if(storageNme.equals("All") || storageNme == null || storageNme.equals("")) {
		%>
		<%=LC.L_Name%><%--Name*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Name%><%--Name*****--%>:&nbsp;<B><I><u><%=storageNme%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		 <!--Added By Amarnadh For  Bugzilla issue 3200 Nov 19th 2007 -->
		 <%
		if(specimenTyp.equals("All") || specimenTyp == null || specimenTyp.equals("")) {
		%>
		<%=LC.L_Allowed_Specimens%><%--Allowed Specimens*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
	    <%=LC.L_Allowed_Specimens%><%--Allowed Specimens*****--%>:&nbsp;<B><I><u><%=cd.getCodeDescription(Integer.parseInt(specimenTyp))%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		<%
		if(storageStat.equals("All") || storageStat == null || storageStat.equals("")) {
		%>
		<%=LC.L_Status%><%--Status*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Status%><%--Status*****--%>:&nbsp;<B><I><u><%=cd.getCodeDescription(Integer.parseInt(storageStat))%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		 <!--Modified By Amarnadh  for Bugzilla issue #3200 Nov 18th 2007-->
		 <%
		if(dstudytxt.equals("All") || dstudytxt == null || dstudytxt.equals("")) {
		%>
		<%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%>:&nbsp;<B><I><u><%=LC.L_All%><%--All*****--%></u></I></B>&nbsp;
		 <%
		 }
		 else {
		 %>
		 <%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%>:&nbsp;<B><I><u><%=dstudytxt%></u></I></B>&nbsp;&nbsp;
		 <%
		 }
		 %>
		</P>
	 <% if(pagenum != null) { // When the page opens the first time, do no search and don't display the following msg %>
		<P class="defComments" align="center"><b><%=MC.M_NoRec_MdfCrit%><%--No matching records found. Please modify your criteria and try again.*****--%></b></P>
     <% } %>
	       <!-- <table>
	<tr>
	<td width=30%>
	</td>
	<td width=20%>
	</td>
	<td >
	<td width=10%>
	<A href="storageunitdetails.jsp?srcmenu=tdmenubaritem6&selectedTab=2&mode=N" >Add new</A>
	</td>
	<td width=11%>
	<A href="#" onClick="openwindow(document.storage);" >Add Multiple</A>
	</td>
	</tr>
	</table>-->
	       <%
	  }
	%>



	</form>
<%
 } //pageright
 else
 {
 	%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%
 }
}//end of if body for session
	else {
%>
  <jsp:include page="timeout.html" flush="true"/>
<% } %>

<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>


</DIV>
</body>

</html>

