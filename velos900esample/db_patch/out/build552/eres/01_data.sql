
Declare
	browserID INTEGER DEFAULT 0;
BEGIN

SELECT PK_BROWSER INTO browserID
FROM ER_BROWSER 
WHERE BROWSER_MODULE = 'allSchedules' AND BROWSER_NAME = 'Patient Schedule Browser';

Update ER_BROWSERCONF set BROWSERCONF_EXPLABEL ='Assigned To' where FK_BROWSER = browserID AND BROWSERCONF_COLNAME='ASSIGNEDTO_NAME';

Update ER_BROWSERCONF set BROWSERCONF_EXPLABEL ='Physician' where FK_BROWSER = browserID AND BROWSERCONF_COLNAME='PHYSICIAN_NAME';

commit;

END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,95,1,'01_data.sql',sysdate,'8.10.0 Build#552');

commit;