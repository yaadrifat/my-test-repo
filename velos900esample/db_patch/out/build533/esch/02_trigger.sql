CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTS1_BI0" BEFORE INSERT ON ESCH.SCH_EVENTS1
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  USR VARCHAR2(70);
  v_new_creator NUMBER;
  insert_data CLOB;
BEGIN
 BEGIN
   v_new_creator := :NEW.creator;
   SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' ||usr_firstname
   INTO usr FROM er_user WHERE pk_user = v_new_creator ;
  EXCEPTION WHEN NO_DATA_FOUND THEN
  USR := 'New User' ;
 END ;
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;

  audit_trail.record_transaction
    (raid, 'SCH_EVENTS1', erid, 'I', USR );

      --   Added by Ganapathy on 06/23/05 for Audit insert

   insert_data:= :NEW.FK_VISIT||'|'||:NEW.OBJ_LOCATION_ID||'|'|| :NEW.FK_ASSOC||'|'||
	TO_CHAR(:NEW.EVENT_EXEON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| :NEW.EVENT_EXEBY||'|'|| :NEW.RID||'|'||
	:NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'|| TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
	TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||:NEW.IP_ADD||'|'|| :NEW.FK_PATPROT||'|'||
	TO_CHAR(:NEW.ACTUAL_SCHDATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| :NEW.ADVERSE_COUNT||'|'|| :NEW.VISIT||'|'||
	:NEW.ALNOT_SENT||'|'||:NEW.EVENT_ID||'|'||:NEW.OBJECT_ID||'|'||:NEW.VISIT_TYPE_ID||'|'||
	:NEW.CHILD_SERVICE_ID||'|'||:NEW.SESSION_ID||'|'||:NEW.OCCURENCE_ID||'|'||:NEW.LOCATION_ID||'|'||
	:NEW.NOTES||'|'||:NEW.TYPE_ID||'|'|| :NEW.ROLES||'|'||:NEW.SVC_TYPE_ID||'|'||
	:NEW.STATUS||'|'||:NEW.SERVICE_ID||'|'||:NEW.BOOKED_BY||'|'|| :NEW.ISCONFIRMED||'|'||
	:NEW.DESCRIPTION||'|'|| :NEW.ATTENDED||'|'|| TO_CHAR(:NEW.START_DATE_TIME,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
	TO_CHAR(:NEW.END_DATE_TIME,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| TO_CHAR(:NEW.BOOKEDON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
	:NEW.ISWAITLISTED||'|'||:NEW.OBJECT_NAME||'|'||:NEW.LOCATION_NAME||'|'||:NEW.SESSION_NAME||'|'||
	:NEW.USER_NAME||'|'||:NEW.PATIENT_ID||'|'||:NEW.SVC_TYPE_NAME||'|'|| :NEW.EVENT_NUM||'|'||
	:NEW.VISIT_TYPE_NAME ||'|'|| :NEW.FK_STUDY ||'|'|| :NEW.EVENT_NOTES ||'|'|| 
    :NEW.SERVICE_SITE_ID ||'|'|| :NEW.FACILITY_ID ||'|'|| :NEW.FK_CODELST_COVERTYPE||'|'|| :NEW.REASON_FOR_COVERAGECHANGE; 

     INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
END; 
/


CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTS1_AU0" AFTER UPDATE ON ESCH.SCH_EVENTS1 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
  raid NUMBER(10);
  usr VARCHAR2(100);

BEGIN
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  usr := getuser(:NEW.last_modified_by);

  audit_trail.record_transaction
    (raid, 'SCH_EVENTS1', :OLD.rid, 'U', usr );

   IF NVL(:OLD.event_id,' ') !=
      NVL(:NEW.event_id,' ') THEN
      audit_trail.column_update
        (raid, 'EVENT_ID',
        :OLD.event_id, :NEW.event_id);
   END IF;
   IF NVL(:OLD.object_id,' ') !=
      NVL(:NEW.object_id,' ') THEN
      audit_trail.column_update
        (raid, 'OBJECT_ID',
        :OLD.object_id, :NEW.object_id);
   END IF;
   IF NVL(:OLD.visit_type_id,' ') !=
      NVL(:NEW.visit_type_id,' ') THEN
      audit_trail.column_update
        (raid, 'VISIT_TYPE_ID',
        :OLD.visit_type_id, :NEW.visit_type_id);
   END IF;
   IF NVL(:OLD.child_service_id,' ') !=
      NVL(:NEW.child_service_id,' ') THEN
      audit_trail.column_update
        (raid, 'CHILD_SERVICE_ID',
        :OLD.child_service_id, :NEW.child_service_id);
   END IF;
   IF NVL(:OLD.session_id,' ') !=
      NVL(:NEW.session_id,' ') THEN
      audit_trail.column_update
        (raid, 'SESSION_ID',
        :OLD.session_id, :NEW.session_id);
   END IF;
   IF NVL(:OLD.occurence_id,' ') !=
      NVL(:NEW.occurence_id,' ') THEN
      audit_trail.column_update
        (raid, 'OCCURENCE_ID',
        :OLD.occurence_id, :NEW.occurence_id);
   END IF;
   IF NVL(:OLD.location_id,' ') !=
      NVL(:NEW.location_id,' ') THEN
      audit_trail.column_update
        (raid, 'LOCATION_ID',
        :OLD.location_id, :NEW.location_id);
   END IF;
   IF NVL(:OLD.notes,' ') !=
      NVL(:NEW.notes,' ') THEN
      audit_trail.column_update
        (raid, 'NOTES',
        :OLD.notes, :NEW.notes);
   END IF;
   IF NVL(:OLD.type_id,' ') !=
      NVL(:NEW.type_id,' ') THEN
      audit_trail.column_update
        (raid, 'TYPE_ID',
        :OLD.type_id, :NEW.type_id);
   END IF;
   IF NVL(:OLD.ROLES,0) !=
      NVL(:NEW.ROLES,0) THEN
      audit_trail.column_update
        (raid, 'ROLES',
        :OLD.ROLES, :NEW.ROLES);
   END IF;
   IF NVL(:OLD.svc_type_id,' ') !=
      NVL(:NEW.svc_type_id,' ') THEN
      audit_trail.column_update
        (raid, 'SVC_TYPE_ID',
        :OLD.svc_type_id, :NEW.svc_type_id);
   END IF;
   IF NVL(:OLD.status,0) !=
      NVL(:NEW.status,0) THEN
      audit_trail.column_update
        (raid, 'STATUS',
        :OLD.status, :NEW.status);
   END IF;
   IF NVL(:OLD.service_id,' ') !=
      NVL(:NEW.service_id,' ') THEN
      audit_trail.column_update
        (raid, 'SERVICE_ID',
        :OLD.service_id, :NEW.service_id);
   END IF;
   IF NVL(:OLD.booked_by,' ') !=
      NVL(:NEW.booked_by,' ') THEN
      audit_trail.column_update
        (raid, 'BOOKED_BY',
        :OLD.booked_by, :NEW.booked_by);
   END IF;
   IF NVL(:OLD.isconfirmed,0) !=
      NVL(:NEW.isconfirmed,0) THEN
      audit_trail.column_update
        (raid, 'ISCONFIRMED',
        :OLD.isconfirmed, :NEW.isconfirmed);
   END IF;
   IF NVL(:OLD.description,' ') !=
      NVL(:NEW.description,' ') THEN
      audit_trail.column_update
        (raid, 'DESCRIPTION',
        :OLD.description, :NEW.description);
   END IF;
   IF NVL(:OLD.attended,0) !=
      NVL(:NEW.attended,0) THEN
      audit_trail.column_update
        (raid, 'ATTENDED',
        :OLD.attended, :NEW.attended);
   END IF;
   IF NVL(:OLD.start_date_time,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.start_date_time,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'START_DATE_TIME',
        to_char(:OLD.start_date_time,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.start_date_time,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.end_date_time,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.end_date_time,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'END_DATE_TIME',
        to_char(:OLD.end_date_time,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.end_date_time,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.bookedon,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.bookedon,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'BOOKEDON',
        to_char(:OLD.bookedon,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.bookedon,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.iswaitlisted,0) !=
      NVL(:NEW.iswaitlisted,0) THEN
      audit_trail.column_update
        (raid, 'ISWAITLISTED',
        :OLD.iswaitlisted, :NEW.iswaitlisted);
   END IF;
   IF NVL(:OLD.object_name,' ') !=
      NVL(:NEW.object_name,' ') THEN
      audit_trail.column_update
        (raid, 'OBJECT_NAME',
        :OLD.object_name, :NEW.object_name);
   END IF;
   IF NVL(:OLD.location_name,' ') !=
      NVL(:NEW.location_name,' ') THEN
      audit_trail.column_update
        (raid, 'LOCATION_NAME',
        :OLD.location_name, :NEW.location_name);
   END IF;
   IF NVL(:OLD.session_name,' ') !=
      NVL(:NEW.session_name,' ') THEN
      audit_trail.column_update
        (raid, 'SESSION_NAME',
        :OLD.session_name, :NEW.session_name);
   END IF;
   IF NVL(:OLD.user_name,' ') !=
      NVL(:NEW.user_name,' ') THEN
      audit_trail.column_update
        (raid, 'USER_NAME',
        :OLD.user_name, :NEW.user_name);
   END IF;
   IF NVL(:OLD.patient_id,' ') !=
      NVL(:NEW.patient_id,' ') THEN
      audit_trail.column_update
        (raid, 'PATIENT_ID',
        :OLD.patient_id, :NEW.patient_id);
   END IF;
   IF NVL(:OLD.svc_type_name,' ') !=
      NVL(:NEW.svc_type_name,' ') THEN
      audit_trail.column_update
        (raid, 'SVC_TYPE_NAME',
        :OLD.svc_type_name, :NEW.svc_type_name);
   END IF;
   IF NVL(:OLD.event_num,0) !=
      NVL(:NEW.event_num,0) THEN
      audit_trail.column_update
        (raid, 'EVENT_NUM',
        :OLD.event_num, :NEW.event_num);
   END IF;
   IF NVL(:OLD.visit_type_name,' ') !=
      NVL(:NEW.visit_type_name,' ') THEN
      audit_trail.column_update
        (raid, 'VISIT_TYPE_NAME',
        :OLD.visit_type_name, :NEW.visit_type_name);
   END IF;
   IF NVL(:OLD.obj_location_id,' ') !=
      NVL(:NEW.obj_location_id,' ') THEN
      audit_trail.column_update
        (raid, 'OBJ_LOCATION_ID',
        :OLD.obj_location_id, :NEW.obj_location_id);
   END IF;
   IF NVL(:OLD.fk_assoc,0) !=
      NVL(:NEW.fk_assoc,0) THEN
      audit_trail.column_update
        (raid, 'FK_ASSOC',
        :OLD.fk_assoc, :NEW.fk_assoc);
   END IF;
   IF NVL(:OLD.event_exeon,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.event_exeon,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'EVENT_EXEON',
        to_char(:OLD.event_exeon,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.event_exeon,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.event_exeby,0) !=
      NVL(:NEW.event_exeby,0) THEN
      audit_trail.column_update
        (raid, 'EVENT_EXEBY',
        :OLD.event_exeby, :NEW.event_exeby);
   END IF;
   IF NVL(:OLD.rid,0) !=
      NVL(:NEW.rid,0) THEN
      audit_trail.column_update
        (raid, 'RID',
        :OLD.rid, :NEW.rid);
   END IF;
   IF NVL(:OLD.last_modified_by,0) !=
      NVL(:NEW.last_modified_by,0) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_BY',
        :OLD.last_modified_by, :NEW.last_modified_by);
   END IF;
   IF NVL(:OLD.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.last_modified_date,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'LAST_MODIFIED_DATE',
    to_char(:OLD.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.last_modified_date,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.created_on,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'CREATED_ON',
       to_char(:OLD.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.created_on,PKG_DATEUTIL.F_GET_DATEFORMAT));

   END IF;
   IF NVL(:OLD.ip_add,' ') !=
      NVL(:NEW.ip_add,' ') THEN
      audit_trail.column_update
        (raid, 'IP_ADD',
        :OLD.ip_add, :NEW.ip_add);
   END IF;
   IF NVL(:OLD.fk_patprot,0) !=
      NVL(:NEW.fk_patprot,0) THEN
      audit_trail.column_update
        (raid, 'FK_PATPROT',
        :OLD.fk_patprot, :NEW.fk_patprot);
   END IF;
   IF NVL(:OLD.actual_schdate,TO_DATE('31-dec-9595','DD-MON-YYYY')) !=
      NVL(:NEW.actual_schdate,TO_DATE('31-dec-9595','DD-MON-YYYY')) THEN
      audit_trail.column_update
        (raid, 'ACTUAL_SCHDATE',
        to_char(:OLD.actual_schdate,PKG_DATEUTIL.F_GET_DATEFORMAT), to_char(:NEW.actual_schdate,PKG_DATEUTIL.F_GET_DATEFORMAT));
   END IF;
   IF NVL(:OLD.adverse_count,0) !=
      NVL(:NEW.adverse_count,0) THEN
      audit_trail.column_update
        (raid, 'ADVERSE_COUNT',
        :OLD.adverse_count, :NEW.adverse_count);
   END IF;
   IF NVL(:OLD.visit,0) !=
      NVL(:NEW.visit,0) THEN
      audit_trail.column_update
        (raid, 'VISIT',
        :OLD.visit, :NEW.visit);
   END IF;

   IF NVL(:OLD.alnot_sent,0) !=
      NVL(:NEW.alnot_sent,0) THEN
      audit_trail.column_update
        (raid, 'ALNOT_SENT',
        :OLD.alnot_sent, :NEW.alnot_sent);
   END IF;

   IF NVL(:OLD.fk_visit,0) !=
      NVL(:NEW.fk_visit,0) THEN
      audit_trail.column_update
        (raid, 'FK_VISIT',
        :OLD.fk_visit, :NEW.fk_visit);
   END IF;

  --JM: 02/16/2007
  IF NVL(:OLD.fk_study,0) !=
      NVL(:NEW.fk_study,0) THEN
      audit_trail.column_update
        (raid, 'FK_STUDY',
        :OLD.fk_study, :NEW.fk_study);
   END IF;

   IF NVL(:OLD.event_notes,0) !=
      NVL(:NEW.event_notes,0) THEN
      audit_trail.column_update
        (raid, 'EVENT_NOTES',
        :OLD.event_notes, :NEW.event_notes);
   END IF;

   IF NVL(:OLD.SERVICE_SITE_ID,0) !=
      NVL(:NEW.SERVICE_SITE_ID,0) THEN
      audit_trail.column_update
	(raid, 'SERVICE_SITE_ID',:OLD.SERVICE_SITE_ID, :NEW.SERVICE_SITE_ID);
   END IF;

   IF NVL(:OLD.FACILITY_ID,0) !=
      NVL(:NEW.FACILITY_ID,0) THEN
      audit_trail.column_update
	(raid, 'FACILITY_ID',:OLD.FACILITY_ID, :NEW.FACILITY_ID);
   END IF;

   IF NVL(:OLD.FK_CODELST_COVERTYPE,0) !=
      NVL(:NEW.FK_CODELST_COVERTYPE,0) THEN
      audit_trail.column_update
	(raid, 'FK_CODELST_COVERTYPE',:OLD.FK_CODELST_COVERTYPE, :NEW.FK_CODELST_COVERTYPE);
   END IF;


   IF NVL(:OLD.REASON_FOR_COVERAGECHANGE,0) !=
      NVL(:NEW.REASON_FOR_COVERAGECHANGE,0) THEN
      audit_trail.column_update
	(raid, 'REASON_FOR_COVERAGECHANGE',:OLD.REASON_FOR_COVERAGECHANGE, :NEW.REASON_FOR_COVERAGECHANGE);
   END IF;


END; 
/

CREATE OR REPLACE TRIGGER "ESCH"."SCH_EVENTS1_AD0" AFTER DELETE ON SCH_EVENTS1
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
declare
  raid number(10);
  deleted_data clob;--KM
begin
  select seq_audit.nextval into raid from dual;

  audit_trail.record_transaction
    (raid, 'SCH_EVENTS1', :old.rid, 'D');

  deleted_data :=
   :old.event_id || '|' ||
   :old.object_id || '|' ||
   :old.visit_type_id || '|' ||
   :old.child_service_id || '|' ||
   :old.session_id || '|' ||
   :old.occurence_id || '|' ||
   :old.location_id || '|' ||
   :old.notes || '|' ||
   :old.type_id || '|' ||
   to_char(:old.roles) || '|' ||
   :old.svc_type_id || '|' ||
   to_char(:old.status) || '|' ||
   :old.service_id || '|' ||
   :old.booked_by || '|' ||
   to_char(:old.isconfirmed) || '|' ||
   :old.description || '|' ||
   to_char(:old.attended) || '|' ||
   to_char(:old.start_date_time) || '|' ||
   to_char(:old.end_date_time) || '|' ||
   to_char(:old.bookedon) || '|' ||
   to_char(:old.iswaitlisted) || '|' ||
   :old.object_name || '|' ||
   :old.location_name || '|' ||
   :old.session_name || '|' ||
   :old.user_name || '|' ||
   :old.patient_id || '|' ||
   :old.svc_type_name || '|' ||
   to_char(:old.event_num) || '|' ||
   :old.visit_type_name || '|' ||
   :old.obj_location_id || '|' ||
   to_char(:old.fk_assoc) || '|' ||
   to_char(:old.event_exeon) || '|' ||
   to_char(:old.event_exeby) || '|' ||
   to_char(:old.rid) || '|' ||
   to_char(:old.creator) || '|' ||
   to_char(:old.last_modified_by) || '|' ||
   to_char(:old.last_modified_date) || '|' ||
   to_char(:old.created_on) || '|' ||
   :old.ip_add || '|' ||
   to_char(:old.fk_patprot) || '|' ||
   to_char(:old.actual_schdate) || '|' ||
   to_char(:old.adverse_count) || '|' ||
   to_char(:old.visit) || '|' ||
   to_char(:old.alnot_sent) || '|' ||
   to_char(:old.fk_visit) || '|' ||
   to_char(:old.fk_study) || '|' ||
   :old.event_notes || '|' ||
   to_char(:old.SERVICE_SITE_ID) ||'|'|| 
   to_char(:old.FACILITY_ID) ||'|'|| 
   to_char(:old.FK_CODELST_COVERTYPE) ||'|'|| 
   :old.reason_for_coveragechange; 

	insert into audit_delete
	(raid, row_data) values (raid, SUBSTR(deleted_data,1,4000));
end; 
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,76,2,'02_trigger.sql',sysdate,'8.9.0 Build#533');

commit;
