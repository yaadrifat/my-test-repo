declare
v_cnt number;
begin

select count(*) into v_cnt from ER_OBJECT_SETTINGS
where object_name = 'irb_subm_tab'  and OBJECT_SUBTYPE = 'irb_pend_rev' and OBJECT_TYPE = 'T';

if v_cnt = 0 then

	update er_object_settings
	set object_Sequence = object_Sequence +1 where 
	object_name = 'irb_subm_tab' and object_Sequence >= 5 and nvl(fk_Account,0)= 0 ;

	Insert into ER_OBJECT_SETTINGS
	   (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
	    OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
	 Values
	   (seq_ER_OBJECT_SETTINGS.nextval, 'T', 'irb_pend_rev', 'irb_subm_tab', 5, 
	    1, 'Pending Review', 0);

	COMMIT;

end if;

end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,77,3,'03_data.sql',sysdate,'8.9.0 Build#534');

commit;
