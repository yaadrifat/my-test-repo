create or replace TRIGGER SCH_BUDGET_BI0
BEFORE INSERT
ON SCH_BUDGET
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
WHEN (
NEW.rid IS NULL OR NEW.rid = 0
      )
DECLARE
  raid NUMBER(10);
  erid NUMBER(10);
  insert_data CLOB;
BEGIN
  SELECT TRUNC(seq_rid.NEXTVAL)
  INTO erid
  FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction
    (raid, 'SCH_BUDGET', erid, 'I', :NEW.LAST_MODIFIED_BY );
    --   Added by Ganapathy on 06/23/05 for Audit insert

    insert_data:= :NEW.PK_BUDGET||'|'||:NEW.BUDGET_NAME||'|'||:NEW.BUDGET_VERSION||'|'||
          substr(:NEW.BUDGET_DESC,1,2500) ||'|'|| :NEW.BUDGET_CREATOR||'|'||:NEW.BUDGET_TYPE||'|'||
         :NEW.BUDGET_STATUS||'|'|| :NEW.BUDGET_CURRENCY||'|'||:NEW.BUDGET_SITEFLAG||'|'||
         :NEW.BUDGET_CALFLAG||'|'|| :NEW.FK_STUDY||'|'|| :NEW.FK_ACCOUNT||'|'||
         :NEW.FK_SITE||'|'||:NEW.BUDGET_RIGHTSCOPE||'|'||:NEW.BUDGET_RIGHTS||'|'|| :NEW.RID||'|'||
         :NEW.CREATOR||'|'|| :NEW.LAST_MODIFIED_BY||'|'||
         TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'|| TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT)||'|'||
        :NEW.IP_ADD||'|'|| :NEW.BUDGET_TEMPLATE||'|'||:NEW.BUDGET_DELFLAG || '|' || :NEW.FK_CODELST_STATUS || '|' || 
        :NEW.BUDGET_COMBFLAG;
     
     INSERT INTO AUDIT_INSERT(raid, row_data) VALUES (raid, substr(insert_data,1,4000) );
     
END; 
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,70,5,'05_Trigger SCH_BUDGET_BI0.sql',sysdate,'8.9.0 Build#527');

commit;