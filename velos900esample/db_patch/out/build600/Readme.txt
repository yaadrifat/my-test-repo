* This readMe is specific to Velos eResearch version 9.0 build #599 */
=======================================================================================================================

Hibernate configuration XML file requires manual configuration.

1. After deploying the 'server' package, go to
[eresearch_dir]\server\eresearch\deploy\velos.ear\velos.war\WEB-INF\lib\garuda1_business-1.0.jar\
2. Open hibernate.cfg.xml in an editor.
3. Replace DB_IP, DB_PORT, DB_SID, ERES_PWD with appropriate values for DB connection.
4. Save and close the file.
=======================================================================================================================

For Localization 

1. Copy files -> messageBundle.properties and labelBundle.properties 
in ereshome\ and paste it to the folder specified by ERES_HOME 
(where eresearch.xml is also hosted), over-writing the existing files.

2. Application server restart is needed. The change is not immediate.

=========================================================================================================================
eResearch Localization:
 
Following Files have been Modified:

1	formfldbrowser.jsp 
2	patientschedule.jsp
3	patientschedulebyvisit.jsp
4	updateNonSystemuser.jsp
5	nonSystemUserDetails.jsp
6	patientdetailsquick.jsp
7	manageVisits.jsp
8	labelBundle.properties
9	messageBundle.properties
10	LC.java
11	MC.java
12	LC.jsp
13	MC.jsp
=========================================================================================================================