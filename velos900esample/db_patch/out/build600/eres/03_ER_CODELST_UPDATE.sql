Set Define Off;

Update er_codelst set codelst_subtyp='red_cell_redu',codelst_desc='Red Cell Depletion Only' where codelst_desc='Red Cell Reduction' and codelst_type='prod_modi';

Update er_codelst set codelst_subtyp='red_cell_redu',codelst_desc='Plasma and RBC Reduced' where codelst_desc='Plasma Reduction' and codelst_type='prod_modi';

Update er_codelst set codelst_subtyp='buffy_coat_prep',codelst_desc='Buffy Coat Preparation' where codelst_desc='Both' and codelst_type='prod_modi';

Update er_codelst set codelst_seq='6' where codelst_desc='None' and codelst_type='prod_modi';

Update er_codelst set codelst_seq='7' where codelst_desc='Other' and codelst_type='prod_modi';

Commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,143,3,'03_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 Build#600');

commit;