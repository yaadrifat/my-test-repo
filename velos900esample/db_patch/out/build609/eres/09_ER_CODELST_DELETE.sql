set define off;

delete from er_codelst where codelst_type='bag_type' and codelst_subtyp = 'sing_frac';
delete from er_codelst where codelst_type='bag_type' and codelst_subtyp = '2_bags';
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,152,9,'09_ER_CODELST_DELETE.sql',sysdate,'9.0.0 Build#609');

commit;
