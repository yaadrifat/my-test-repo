/* This readMe is specific to Velos eResearch version 9.0 build #589 */

=====================================================================================
For Localization 

1. Copy files -> messageBundle.properties and labelBundle.properties 
in ereshome\ and paste it to the folder specified by ERES_HOME 
(where eresearch.xml is also hosted), over-writing the existing files.

2. Application server restart is needed. The change is not immediate.

For Localization on Aug 10,2011:
###### Start
Basic Files - 04
1.  messageBundle.properties
2.  labelBundle.properties
3.  LC.java
4.  MC.java
JSP Files - 20 	
5.  emailuser.jsp
6.  portaldetails.jsp
7.  accountbrowser.jsp
8.  accountcreation.jsp
9.  accountdelete.jsp
10. accountFormBrowser.jsp
11. accountforms.jsp
12. accountfreeze.jsp
13. accountlinkbrowser.jsp
14. accountreports.jsp
15. accountsave.jsp
16. accountUsers.jsp
17. accrepfilter.jsp
18. acctformdetails.jsp
19. addUsersToMultipleStudies.jsp
20. adminSettings.jsp
21. assignuserstogroupusrsearch.jsp
22. attributesForStudyAccount.jsp
23. editPortalStatus.jsp
24. copyFormForAccount.jsp
JS Files - 15
25. budget.js
26. cal.js
27. calendar.js
28. calendar_witheditbox.js
29. confirm.js
30. conJS.js
31. formjs.js
32. multisearchJS.js
33. ofcal.js
34. ofcalendar.js
35. overlib_hideform.js
36. overlib_shadow.js
37. SearchJS.js
38. validations.js
39. website.js
###### end
=====================================================================================

***** QA FYI *****
*Datepicker year range configuration.
For the new JQuery Datepicker, year range configuration is now introduced. Two new 
configuration variables for setting up year range into datepicker has been introduced 
in file velosConfig.js. If not configued the default year range is -30 to +30.

1.YEAR_UPPER_BOUND - upper bound used in date picker.It is added to the current year 
	to set the upper limit of year range. 
2.YEAR_LOWER_BOUND - lower bound used in date picker.It is subtracted from the current 
	year to set the lower limit of year range. 
=====================================================================================

***** QA FYI *****
Misplaced header-
List of Files chnaged for Bug# 6728, #6766, #6767
Files Changed by Yogesh/Ashwani

1	activateuser.jsp
2	addeventcost.jsp
3	addeventfile.jsp
4	addeventrole.jsp
5	addeventurl.jsp
6	addspecimenurl.jsp
7	budgetapndxdelete.jsp
8	budgetdelete.jsp
9	budgetsectiondelete.jsp
10	budgetusersdelete.jsp
11	calLibraryDelete.jsp
12	copyevent.jsp
13	costsave.jsp
14	createversion.jsp
15	crflib.jsp
16	crflibbrowser.jsp
17	deletecrf.jsp
18	deleteInvoice.jsp
19	deleteMeetingTopic.jsp
20	deleventstatus.jsp
21	dynfilter.jsp
22	dynorder.jsp
23	dynrepcopy.jsp
24	dynrepdelete.jsp
25	editeventfile.jsp
26	emailuser.jsp
27	eventappendix.jsp
28	eventcost.jsp
29	eventdetails.jsp
30	eventmessage.jsp
31	eventresource.jsp
32	eventuserdelete.jsp
33	formdelete.jsp
34	formrespdelete.jsp
35	lineitemdelete.jsp
36	mdynfilter.jsp
37	mdynorder.jsp
38	mileapndxdelete.jsp
39	milepaymentdelete.jsp
40	neweventcost.jsp
41	patientsearch.jsp
42	perapndxdelete.jsp
43	portaldetails.jsp
44	protocollist.jsp
45	refreshProtocolMessages.jsp
46	savedrepdelete.jsp
47	specimenapndxdelete.jsp
48	specimenapndxfile.jsp
49	specimenbrowser.jsp
50	specimendetails.jsp
51	specimendetails.jsp
52	studycalendardelete.jsp
53	studynotification.jsp
54	studysectiondelete.jsp
55	studystatusdelete.jsp
56	updatespecimendetails.jsp
57	updatespecimendetails.jsp
58	urlsave.jsp
59	visitdelete.jsp
60	popupJS.js

##############################

=====================================================================================


