Set Define off;

UPDATE ER_CODELST SET CODELST_CUSTOM_COL1= 'bladder,blad_peritoneum,blad_periSerosa,corpus,cervix,cul-de-sac,diaphragm,fallopiantube,omentum,paracolic_gutt,pelvic_wall,perit_uteri_ser,vagina,ovary,mainstem_bronch,carina,UL,LL,ML,ureter,renal_pelvis,endometrium,lymph_node,other_specify,res_organ-spc,bone,brain' 
WHERE CODELST_TYPE = 'tissue_side' AND CODELST_SUBTYP = 'notSpecified';

UPDATE ER_CODELST SET CODELST_CUSTOM_COL1= 'bladder,blad_peritoneum,blad_periSerosa,corpus,cervix,cul-de-sac,diaphragm,fallopiantube,omentum,paracolic_gutt,pelvic_wall,perit_uteri_ser,vagina,ovary,mainstem_bronch,carina,UL,LL,ML,ureter,renal_pelvis,endometrium,lymph_node,other_specify,res_organ-spc,bone,brain' 
WHERE CODELST_TYPE = 'tissue_side' AND CODELST_SUBTYP = 'right';

UPDATE ER_CODELST SET CODELST_CUSTOM_COL1= 'bladder,blad_peritoneum,blad_periSerosa,corpus,cervix,cul-de-sac,diaphragm,fallopiantube,omentum,paracolic_gutt,pelvic_wall,perit_uteri_ser,vagina,ovary,mainstem_bronch,carina,UL,LL,ML,ureter,renal_pelvis,endometrium,lymph_node,other_specify,res_organ-spc,bone,brain' 
WHERE CODELST_TYPE = 'tissue_side' AND CODELST_SUBTYP = 'left';

UPDATE ER_CODELST SET CODELST_CUSTOM_COL1= 'bladder,blad_peritoneum,blad_periSerosa,corpus,cervix,cul-de-sac,diaphragm,fallopiantube,omentum,paracolic_gutt,pelvic_wall,perit_uteri_ser,vagina,ovary,mainstem_bronch,carina,UL,LL,ML,ureter,renal_pelvis,endometrium,lymph_node,other_specify,res_organ-spc,bone,brain' 
WHERE CODELST_TYPE = 'tissue_side' AND CODELST_SUBTYP = 'notApplicable';

COMMIT;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,190,2,'02_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 B#637-ES010');

COMMIT;
