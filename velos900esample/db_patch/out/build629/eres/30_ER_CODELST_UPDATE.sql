set define off;


--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_temp'
    AND codelst_subtyp = '>-135';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='>-135' where codelst_type = 'storage_temp'AND codelst_subtyp = '>-135';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_temp'
    AND codelst_subtyp = '>=-150to<=-135';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=2 where codelst_type = 'storage_temp'AND codelst_subtyp = '>=-150to<=-135';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_temp'
    AND codelst_subtyp = '<-150';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=1 where codelst_type = 'storage_temp'AND codelst_subtyp = '<-150';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD INTO ER_CODELST TABLE Storage Method--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_temp'
    AND codelst_subtyp = '>-135';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SEQ=3 where codelst_type = 'storage_temp'AND codelst_subtyp = '>-135';
	commit;
  end if;
end;
/
--END--


set define off;

--STARTS UPDATING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'eligibility'
    AND codelst_subtyp = 'not_appli_prior';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='Not Applicable - collected prior to 05/25/2005' where codelst_type = 'eligibility'AND codelst_subtyp = 'not_appli_prior';
	commit;
  end if;
end;
/
--END--

update er_codelst set codelst_desc='Maternal Extracted DNA' where codelst_type='aliquots_type' and codelst_subtyp='MDNA';
commit;



update er_codelst set codelst_desc='Aliquots' where codelst_type='cbu_sample_type' and codelst_subtyp='ALQT';
commit;



update er_codelst set codelst_desc='OTHER' where codelst_type='shipper' and codelst_subtyp='others';
commit;





update er_codelst set codelst_seq=1 where codelst_type='aliquots_type' and codelst_subtyp='EDNA';
update er_codelst set codelst_seq=2 where codelst_type='aliquots_type' and codelst_subtyp='NVC';
update er_codelst set codelst_seq=3 where codelst_type='aliquots_type' and codelst_subtyp='SER';
update er_codelst set codelst_seq=4 where codelst_type='aliquots_type' and codelst_subtyp='PLA';
update er_codelst set codelst_seq=5 where codelst_type='aliquots_type' and codelst_subtyp='VCEL';
update er_codelst set codelst_seq=6 where codelst_type='aliquots_type' and codelst_subtyp='MDNA';
update er_codelst set codelst_seq=7 where codelst_type='aliquots_type' and codelst_subtyp='MCEL';
update er_codelst set codelst_seq=8 where codelst_type='aliquots_type' and codelst_subtyp='MPLA';
update er_codelst set codelst_seq=9 where codelst_type='aliquots_type' and codelst_subtyp='MSER';
commit;





update er_codelst set codelst_desc='Dry Shipper(default)',codelst_subtyp='dry_ship',codelst_seq='1' where codelst_type='ship_cont' and codelst_subtyp='SHIP_CONT1';
update er_codelst set codelst_desc='Other',codelst_subtyp='other',codelst_seq='2' where codelst_type='ship_cont' and codelst_subtyp='SHIP_CONT2';

commit;




INSERT INTO track_patches
VALUES(seq_track_patches.nextval,172,30,'30_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 Build#629');

commit;