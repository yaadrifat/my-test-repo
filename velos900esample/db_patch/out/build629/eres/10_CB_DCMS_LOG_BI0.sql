create or replace TRIGGER CB_DCMS_LOG_BI0 BEFORE INSERT ON CB_DCMS_LOG     REFERENCING OLD AS OLD NEW AS NEW
	FOR EACH ROW
	DECLARE
	raid NUMBER(10);
	erid NUMBER(10);
	usr VARCHAR(2000);
	insert_data VARCHAR2(4000);
	 BEGIN
 BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;
  SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'CB_DCMS_LOG',erid, 'I',usr);
       insert_data:=:NEW.PK_DCMS_LOG || '|' ||
to_char(:NEW.ATTACHMENT_ID) || '|' ||
to_char(:NEW.ACTION) || '|' ||
to_char(:NEW.ACTION_DATE) || '|' ||
to_char(:NEW.USER_ID) || '|' ||
to_char(:NEW.CREATOR) || '|' ||
to_char(:NEW.CREATED_ON) || '|' ||
To_Char(:New.IP_ADD) || '|' ||
to_char(:NEW.RID) || '|' ||
to_char(:NEW.LAST_MODIFIED_BY) || '|' ||
to_char(:NEW.LAST_MODIFIED_DATE);      
    INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,172,10,'10_CB_DCMS_LOG_BI0.sql',sysdate,'9.0.0 Build#629');

commit;
