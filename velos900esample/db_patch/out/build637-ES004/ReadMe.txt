/* This readMe is specific to eSample (version 9.0.0 build #637-ES004) */

=====================================================================================================================================
eSample bugs covered in the build:

1) Bug 5219 - Always default color schema is applied in editmultiplestoragestatus.jsp 
2) Bug 10502 - UI: Add Multiple Storage page fields are not fit into outer container in Firefox browser 
3) Bug 12001 - Revised-INV-21675:Alert message not working in "Internet explorer" 
4) Bug 11895 - INV-21679: User is not able search the data in "Manage Inventory >> Specimens" 
5) Bug 10694 - ISSUE: LOCL:Text "Content" need to be Localized on storageunitdetails.jsp 

Following existing files have been modified:
	
	1. specimenbrowser.jsp				(...\deploy\velos.ear\velos.war\jsp)
	2. storageunitdetails.jsp			(...\deploy\velos.ear\velos.war\jsp)
	3. getStorageGrid.jsp				(...\deploy\velos.ear\velos.war\jsp)
	4. storageadminbrowser.jsp			(...\deploy\velos.ear\velos.war\jsp)
	5. ajaxGetSpecimenList.jsp			(...\deploy\velos.ear\velos.war\jsp)
=====================================================================================================================================