set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = '9.0.0 Build#593' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(eres.seq_track_patches.nextval,136,0,'00_er_version.sql',sysdate,'9.0.0 Build#593');

commit;

