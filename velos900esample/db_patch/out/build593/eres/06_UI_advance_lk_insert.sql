Insert into UI_WIDGET
(PK_WIDGET,
HELP_URL,
DESCRIPTION,
IS_AUTHENTICATED,
IS_MINIZABLE,
IS_CLOSABLE,
IS_RESIZEABLE,	IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,
CREATED_ON,
LAST_MODIFIED_DATE,
VERSION_NO,
WIDGET_DIV_ID,
NAME,
RESIZE_MIN_HEIGHT,
RESIZE_MIN_WIDTH,
RESIZE_MAX_HEIGHT,
RESIZE_MAX_WIDTH)
VALUES
(
SEQ_UI_WIDGET.nextval,NULL,'Advance Lookup',1,1,1,1,1,1,NULL,NULL,NULL,'advancelookupdiv','ADVANCE LOOKUP',300,400,1024,1024
);


Insert into UI_PAGE (PK_PAGE,HELP_URL,PAGE_CODE,PAGE_NAME,SEQ) values (SEQ_UI_PAGE.nextval,null,22,'ADVANCE LOOKUP',null);

Insert into UI_WIDGET_CONTAINER (PK_CONTAINER,CONTAINER_NAME,FK_PAGE_ID) values (SEQ_UI_WIDGET_CONTAINER.nextval,'Advance Lookup',(select pk_page from ui_page where PAGE_NAME='ADVANCE LOOKUP'));

Insert into UI_CONTAINER_WIDGET_MAP (PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) values (SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(select PK_CONTAINER from UI_WIDGET_CONTAINER where container_name='Advance Lookup'),(select PK_WIDGET from ui_widget where name='ADVANCE LOOKUP'));

commit;

INSERT INTO track_patches
VALUES(eres.seq_track_patches.nextval,136,6,'06_UI_advance_lk_insert.sql',sysdate,'9.0.0 Build#593');

commit;