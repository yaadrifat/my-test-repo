CREATE TABLE "ERES"."ER_OBJECT_MAP"
  (
    "SYSTEM_ID" VARCHAR2(255 CHAR) NOT NULL,
    "TABLE_NAME"  VARCHAR2(255 CHAR) NOT NULL ,
    "TABLE_PK"    NUMBER(10,0) NOT NULL,
    PRIMARY KEY ("SYSTEM_ID") 
  )
  TABLESPACE "ERES_HUGE";
  
  COMMENT ON TABLE "ERES"."ER_OBJECT_MAP"  IS 'Stores mappings for the outside world to reference objects in eresearch';
  COMMENT ON COLUMN "ERES"."ER_OBJECT_MAP"."SYSTEM_ID" IS 'Stores an id for an object available to the external world' ;
  COMMENT ON COLUMN "ERES"."ER_OBJECT_MAP"."TABLE_NAME" IS 'Stores the database table name where the mapping points to' ;
  COMMENT ON COLUMN "ERES"."ER_OBJECT_MAP"."TABLE_PK" IS 'Stores the database primary key where the mapping points to' ;
  
  
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,82,2,'02_ALTER.sql',sysdate,'8.9.0 Build#539');

commit;