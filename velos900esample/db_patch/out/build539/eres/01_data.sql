delete from er_repxsl where fk_report=129;
commit;

delete from er_repxsl where fk_report in (110,159);

commit;

update er_report 
set REP_SQL_CLOB='SELECT pk_storage, storage_id,
decode(s.fk_storage,null,(SELECT b.storage_name FROM ER_STORAGE b WHERE s.pk_storage = b.pk_storage),
(SELECT b.storage_name FROM ER_STORAGE b WHERE s.fk_storage = b.pk_storage)) as parent, storage_name,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_storage_type AND codelst_type = ''store_type'') AS st_type,
decode(s.fk_storage,null,pk_storage,s.fk_storage) as fk_storage, storage_cap_number AS cap,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_capacity_units AND codelst_type = ''cap_unit'')  AS unit,
DECODE((storage_dim1_cellnum ||'' x ''|| storage_dim2_cellnum),'' x '','''',(storage_dim1_cellnum ||'' x ''|| storage_dim2_cellnum)) AS grid,
(SELECT COUNT(*) FROM ER_STORAGE WHERE fk_storage = s.pk_storage) AS child_storage,
(SELECT COUNT(*) FROM ER_SPECIMEN WHERE pk_storage = ER_SPECIMEN.fk_storage) AS num_sample,
((SELECT b.storage_name FROM ER_STORAGE b WHERE s.fk_storage = b.pk_storage) || '''' || storage_name) AS sort_crit,
ss_start_date,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = fk_codelst_storage_status AND codelst_type = ''storage_stat'')  AS status,
Decode((SELECT usr_firstname ||'' ''|| usr_lastname FROM ER_USER WHERE pk_user = fk_user),null,''No'',(SELECT usr_firstname ||'' ''|| usr_lastname FROM ER_USER WHERE pk_user = fk_user)) AS res_user,
Decode((SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study),null,''No'',(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study)) AS res_study
FROM ER_STORAGE s, ER_STORAGE_STATUS ss
WHERE s.pk_storage = ss.fk_storage and s.fk_storage is not null
and s.fk_account =:sessAccId and s.pk_storage in (:storageId)
AND ss.PK_STORAGE_STATUS in (SELECT max(ss1.PK_STORAGE_STATUS) FROM er_storage_status ss1
WHERE ss1.FK_STORAGE = s.PK_STORAGE
AND ss1.SS_START_DATE in (select max(ss2.SS_START_DATE) from er_storage_status ss2
where ss2.FK_STORAGE = s.PK_STORAGE))
order by sort_crit' where pk_report = 129;

commit;



INSERT INTO track_patches
VALUES(seq_track_patches.nextval,82,1,'01_data.sql',sysdate,'8.9.0 Build#539');

commit;