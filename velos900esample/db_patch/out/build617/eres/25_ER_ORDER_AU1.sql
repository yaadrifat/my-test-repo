--When ER_ORDER table is updated, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ERES"."ER_ORDER_AU1" 
AFTER UPDATE ON eres.er_order 
referencing OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
BEGIN
    SELECT seq_audit_row_module.nextval INTO v_rowid FROM dual;
	
	pkg_audit_trail_module.sp_row_insert (v_rowid,'ER_ORDER',:OLD.rid,:OLD.PK_ORDER,'U',:NEW.creator);
	
IF nvl(:OLD.pk_order,0) != nvl(:NEW.pk_order,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'PK_ORDER',:OLD.pk_order, :NEW.pk_order,NULL,NULL);
  END IF;
  IF nvl(:OLD.order_type,0) != nvl(:NEW.order_type,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_TYPE',:OLD.order_type, :NEW.order_type,NULL,NULL);
  END IF;
 IF nvl(:OLD.fk_order_header,0) != nvl(:NEW.fk_order_header,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'FK_ORDER_HEADER',:OLD.fk_order_header, :NEW.fk_order_header,NULL,NULL);
  END IF; 
 IF nvl(:OLD.order_status,0) != nvl(:NEW.order_status,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_STATUS',:OLD.order_status, :NEW.order_status,NULL,NULL);
  END IF;
  IF nvl(:OLD.recent_hla_entered_flag,' ') !=  nvl(:NEW.recent_hla_entered_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'RECENT_HLA_ENTERED_FLAG',:OLD.recent_hla_entered_flag, :NEW.recent_hla_entered_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.additi_typing_flag,' ') !=  nvl(:NEW.additi_typing_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ADDITI_TYPING_FLAG',:OLD.additi_typing_flag, :NEW.additi_typing_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.cbu_avail_confirm_flag,' ') !=  nvl(:NEW.cbu_avail_confirm_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'CBU_AVAIL_CONFIRM_FLAG',:OLD.cbu_avail_confirm_flag, :NEW.cbu_avail_confirm_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.is_cord_avail_for_nmdp,' ') !=  nvl(:NEW.is_cord_avail_for_nmdp,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'IS_CORD_AVAIL_FOR_NMDP',:OLD.is_cord_avail_for_nmdp, :NEW.is_cord_avail_for_nmdp,NULL,NULL);
  END IF;
  IF NVL(:OLD.CORD_AVAIL_CONFIRM_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) != NVL(:NEW.CORD_AVAIL_CONFIRM_DATE,TO_DATE('31-DEC-9595','DD-MON-YYYY')) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CORD_AVAIL_CONFIRM_DATE',TO_CHAR(:OLD.CORD_AVAIL_CONFIRM_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CORD_AVAIL_CONFIRM_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
  IF nvl(:OLD.shipment_sch_flag,' ') !=  nvl(:NEW.shipment_sch_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'SHIPMENT_SCH_FLAG',:OLD.shipment_sch_flag, :NEW.shipment_sch_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.cord_shipped_flag,' ') !=  nvl(:NEW.cord_shipped_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'CORD_SHIPPED_FLAG',:OLD.cord_shipped_flag, :NEW.cord_shipped_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.package_slip_flag,' ') !=  nvl(:NEW.package_slip_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'PACKAGE_SLIP_FLAG',:OLD.package_slip_flag, :NEW.package_slip_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.nmdp_sample_shipped_flag,' ') !=  nvl(:NEW.nmdp_sample_shipped_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'NMDP_SAMPLE_SHIPPED_FLAG',:OLD.nmdp_sample_shipped_flag, :NEW.nmdp_sample_shipped_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.complete_req_info_task_flag,' ') !=  nvl(:NEW.complete_req_info_task_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'COMPLETE_REQ_INFO_TASK_FLAG',:OLD.complete_req_info_task_flag, :NEW.complete_req_info_task_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.final_review_task_flag,' ') !=  nvl(:NEW.final_review_task_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'FINAL_REVIEW_TASK_FLAG',:OLD.final_review_task_flag, :NEW.final_review_task_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.order_last_viewed_by,0) !=  nvl(:NEW.order_last_viewed_by,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_LAST_VIEWED_BY',:OLD.order_last_viewed_by, :NEW.order_last_viewed_by,NULL,NULL);
  END IF;
  IF nvl(:OLD.order_last_viewed_date,to_date('31-dec-9595','DD-MON-YYYY')) !=  nvl(:NEW.order_last_viewed_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_LAST_VIEWED_DATE',
       to_char(:OLD.order_last_viewed_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.order_last_viewed_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF;
  IF nvl(:OLD.order_view_confirm_flag,' ') !=  nvl(:NEW.order_view_confirm_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_VIEW_CONFIRM_FLAG',:OLD.order_view_confirm_flag, :NEW.order_view_confirm_flag,NULL,NULL);
  END IF;
  IF nvl(:OLD.order_view_confirm_date,to_date('31-dec-9595','DD-MON-YYYY')) !=  nvl(:NEW.order_view_confirm_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_VIEW_CONFIRM_DATE',
       to_char(:OLD.order_view_confirm_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.order_view_confirm_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF;
  IF nvl(:OLD.order_priority,0) != nvl(:NEW.order_priority,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_PRIORITY',:OLD.order_priority, :NEW.order_priority,NULL,NULL);
  END IF;
  IF nvl(:OLD.fk_current_hla,0) != nvl(:NEW.fk_current_hla,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'FK_CURRENT_HLA',:OLD.fk_current_hla, :NEW.fk_current_hla,NULL,NULL);
  END IF;
  IF nvl(:OLD.creator,0) !=    nvl(:NEW.creator,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'CREATOR',:OLD.creator, :NEW.creator,NULL,NULL);
  END IF;
   IF nvl(:OLD.created_on,to_date('31-dec-9595','DD-MON-YYYY')) !=
     nvl(:NEW.created_on,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'CREATED_ON',to_char(:OLD.created_on, pkg_dateutil.f_get_dateformat), to_char(:NEW.created_on, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF;
 IF nvl(:OLD.last_modified_by,0) != nvl(:NEW.last_modified_by,0) THEN
        pkg_audit_trail_module.sp_column_insert (v_rowid,'LAST_MODIFIED_BY',:OLD.last_modified_by,:NEW.last_modified_by,NULL,NULL);
      END IF;
      IF nvl(:OLD.last_modified_date,to_date('31-dec-9595','DD-MON-YYYY')) != nvl(:NEW.last_modified_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
        pkg_audit_trail_module.sp_column_insert (v_rowid,'LAST_MODIFIED_DATE',to_char(:OLD.last_modified_date, pkg_dateutil.f_get_dateformat),to_char(:NEW.last_modified_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
      END IF;  
   IF nvl(:OLD.ip_add,' ') !=  nvl(:NEW.ip_add,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'IP_ADD',:OLD.ip_add, :NEW.ip_add,NULL,NULL);
  END IF;  
  IF nvl(:OLD.deletedflag,0) !=  nvl(:NEW.deletedflag,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'DELETEDFLAG', :OLD.deletedflag, :NEW.deletedflag,NULL,NULL);
  END IF;
  IF nvl(:OLD.rid,0) !=   nvl(:NEW.rid,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'RID',:OLD.rid, :NEW.rid,NULL,NULL);
  END IF;  
  IF nvl(:OLD.task_id,0) !=   nvl(:NEW.task_id,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'TASK_ID',:OLD.task_id, :NEW.task_id,NULL,NULL);
  END IF;
    IF nvl(:OLD.task_name,' ') != nvl(:NEW.task_name,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'TASK_NAME',:OLD.task_name, :NEW.task_name,NULL,NULL);
  END IF;
  IF nvl(:OLD.assigned_to,' ') != nvl(:NEW.assigned_to,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ASSIGNED_TO', :OLD.assigned_to, :NEW.assigned_to,NULL,NULL);
  END IF;
  IF nvl(:OLD.order_status_date,to_date('31-dec-9595','DD-MON-YYYY')) !=
     nvl(:NEW.order_status_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_STATUS_DATE',
       to_char(:OLD.order_status_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.order_status_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF;
  IF nvl(:OLD.order_reviewed_by,0) !=  nvl(:NEW.order_reviewed_by,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_REVIEWED_BY',
       :OLD.order_reviewed_by, :NEW.order_reviewed_by,NULL,NULL);
  END IF;
  IF nvl(:OLD.order_reviewed_date,to_date('31-dec-9595','DD-MON-YYYY')) !=
     nvl(:NEW.order_reviewed_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_REVIEWED_DATE',
       to_char(:OLD.order_reviewed_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.order_reviewed_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF;
   IF nvl(:OLD.order_assigned_date,to_date('31-dec-9595','DD-MON-YYYY')) !=
     nvl(:NEW.order_assigned_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_ASSIGNED_DATE',
       to_char(:OLD.order_assigned_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.order_assigned_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF;  
   IF nvl(:OLD.fk_order_resol_by_cbb,0) !=     nvl(:NEW.fk_order_resol_by_cbb,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'FK_ORDER_RESOL_BY_CBB', :OLD.fk_order_resol_by_cbb, :NEW.fk_order_resol_by_cbb,NULL,NULL);
  END IF;  
   IF nvl(:OLD.fk_order_resol_by_tc,0) !=     nvl(:NEW.fk_order_resol_by_tc,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'FK_ORDER_RESOL_BY_TC',
       :OLD.fk_order_resol_by_tc, :NEW.fk_order_resol_by_tc,NULL,NULL);
  END IF;  
    IF nvl(:OLD.order_resol_date,to_date('31-dec-9595','DD-MON-YYYY')) !=
     nvl(:NEW.order_resol_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_RESOL_DATE',
       to_char(:OLD.order_resol_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.order_resol_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF; 
  IF nvl(:OLD.order_ack_flag,' ') !=     nvl(:NEW.order_ack_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_ACK_FLAG',
       :OLD.order_ack_flag, :NEW.order_ack_flag,NULL,NULL);
  END IF;
   IF nvl(:OLD.order_ack_date,to_date('31-dec-9595','DD-MON-YYYY')) !=
     nvl(:NEW.order_ack_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_ACK_DATE',
       to_char(:OLD.order_ack_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.order_ack_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF; 
   IF nvl(:OLD.order_ack_by,0) !=     nvl(:NEW.order_ack_by,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_ACK_BY',
       :OLD.order_ack_by, :NEW.order_ack_by,NULL,NULL);
  END IF; 
   IF nvl(:OLD.result_rec_date,to_date('31-dec-9595','DD-MON-YYYY')) !=
     nvl(:NEW.result_rec_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'RESULT_REC_DATE',
       to_char(:OLD.result_rec_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.result_rec_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF; 
  IF nvl(:OLD.accpt_to_cancel_req,' ') !=     nvl(:NEW.accpt_to_cancel_req,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ACCPT_TO_CANCEL_REQ',
       :OLD.accpt_to_cancel_req, :NEW.accpt_to_cancel_req,NULL,NULL);
  END IF;
   IF nvl(:OLD.cancel_conform_date,to_date('31-dec-9595','DD-MON-YYYY')) !=
     nvl(:NEW.cancel_conform_date,to_date('31-dec-9595','DD-MON-YYYY')) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'CANCEL_CONFORM_DATE',
       to_char(:OLD.cancel_conform_date, pkg_dateutil.f_get_dateformat), to_char(:NEW.cancel_conform_date, pkg_dateutil.f_get_dateformat),NULL,NULL);
  END IF;   
   IF nvl(:OLD.canceled_by,0) !=     nvl(:NEW.canceled_by,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'CANCELED_BY',:OLD.canceled_by, :NEW.canceled_by,NULL,NULL);
  END IF;   
   IF nvl(:OLD.clinic_info_checklist_stat,' ') !=     nvl(:NEW.clinic_info_checklist_stat,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'CLINIC_INFO_CHECKLIST_STAT',:OLD.clinic_info_checklist_stat, :NEW.clinic_info_checklist_stat,NULL,NULL);
  END IF;
  IF nvl(:OLD.recent_hla_typing_avail,' ') !=     nvl(:NEW.recent_hla_typing_avail,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'RECENT_HLA_TYPING_AVAIL',   :OLD.recent_hla_typing_avail, :NEW.recent_hla_typing_avail,NULL,NULL);
  END IF;
  IF nvl(:OLD.addi_test_result_avail,' ') !=     nvl(:NEW.addi_test_result_avail,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ADDI_TEST_RESULT_AVAIL',     :OLD.addi_test_result_avail, :NEW.addi_test_result_avail,NULL,NULL);
  END IF;
  IF nvl(:OLD.order_sample_at_lab,' ') !=     nvl(:NEW.order_sample_at_lab,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_SAMPLE_AT_LAB',    :OLD.order_sample_at_lab, :NEW.order_sample_at_lab,NULL,NULL);
  END IF;
   IF nvl(:OLD.fk_case_manager,0) !=     nvl(:NEW.fk_case_manager,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'FK_CASE_MANAGER',  :OLD.fk_case_manager, :NEW.fk_case_manager,NULL,NULL);
  END IF; 
   IF nvl(:OLD.case_manager,' ') !=     nvl(:NEW.case_manager,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'CASE_MANAGER',  :OLD.case_manager, :NEW.case_manager,NULL,NULL);
  END IF;
   IF nvl(:OLD.trans_center_id,0) != nvl(:NEW.trans_center_id,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'TRANS_CENTER_ID',   :OLD.trans_center_id, :NEW.trans_center_id,NULL,NULL);
  END IF; 
    IF nvl(:OLD.trans_center_name,' ') !=     nvl(:NEW.trans_center_name,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'TRANS_CENTER_NAME',   :OLD.trans_center_name, :NEW.trans_center_name,NULL,NULL);
  END IF;
    IF nvl(:OLD.sec_trans_center_name,' ') !=     nvl(:NEW.sec_trans_center_name,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'SEC_TRANS_CENTER_NAME',:OLD.sec_trans_center_name, :NEW.sec_trans_center_name,NULL,NULL);
  END IF;
 IF nvl(:OLD.order_physician,0) !=     nvl(:NEW.order_physician,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'ORDER_PHYSICIAN',   :OLD.order_physician, :NEW.order_physician,NULL,NULL);
  END IF; 
   IF nvl(:OLD.sec_trans_center_id,' ') !=     nvl(:NEW.sec_trans_center_id,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'SEC_TRANS_CENTER_ID',       :OLD.sec_trans_center_id, :NEW.sec_trans_center_id,NULL,NULL);
  END IF;
IF nvl(:OLD.cm_mail_id,' ') !=     nvl(:NEW.cm_mail_id,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'CM_MAIL_ID',       :OLD.cm_mail_id, :NEW.cm_mail_id,NULL,NULL);
  END IF;
IF nvl(:OLD.cm_contact_no,' ') !=     nvl(:NEW.cm_contact_no,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'CM_CONTACT_NO',       :OLD.cm_contact_no, :NEW.cm_contact_no,NULL,NULL);
  END IF;
IF nvl(:OLD.req_clin_info_flag,' ') !=     nvl(:NEW.req_clin_info_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'REQ_CLIN_INFO_FLAG',       :OLD.req_clin_info_flag, :NEW.req_clin_info_flag,NULL,NULL);
  END IF;
IF nvl(:OLD.resol_ack_flag,' ') !=     nvl(:NEW.resol_ack_flag,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'RESOL_ACK_FLAG',  :OLD.resol_ack_flag, :NEW.resol_ack_flag,NULL,NULL);
  END IF; 
   IF nvl(:OLD.fk_sample_type_avail,0) !=     nvl(:NEW.fk_sample_type_avail,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'FK_SAMPLE_TYPE_AVAIL',       :OLD.fk_sample_type_avail, :NEW.fk_sample_type_avail,NULL,NULL);
  END IF;
   IF nvl(:OLD.fk_aliquots_type,0) !=     nvl(:NEW.fk_aliquots_type,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid, 'FK_ALIQUOTS_TYPE', :OLD.fk_aliquots_type, :NEW.fk_aliquots_type,NULL,NULL);
  END IF;
  END;
  /
  INSERT INTO track_patches
VALUES(seq_track_patches.nextval,160,25,'25_ER_ORDER_AU1.sql',sysdate,'9.0.0 Build#617');

commit;