set define off;

Insert into ER_BROWSER (PK_BROWSER,BROWSER_MODULE,BROWSER_NAME,FK_ACCOUNT) 
values (SEQ_ER_BROWSER.nextval,'preparea','Preparation Area',null);

commit;

Declare
	browserID INTEGER DEFAULT 0;
BEGIN

SELECT PK_BROWSER INTO browserID
FROM ER_BROWSER 
WHERE BROWSER_MODULE = 'preparea' AND BROWSER_NAME = 'Preparation Area';


Insert into ER_BROWSERCONF (PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values (SEQ_ER_BROWSERCONF.nextval,browserID,'PERSON_CODE',1,'{"key":"PERSON_CODE", "label":"Patient ID", "sortable":true, "resizeable":true,"hideable":true}','Patient Id');
Insert into ER_BROWSERCONF (PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values (SEQ_ER_BROWSERCONF.nextval,browserID,'STUDY_NUMBER',2,'{"key":"STUDY_NUMBER", "label":"Study Number", "sortable":true, "resizeable":true,"hideable":true}','Study Number');
Insert into ER_BROWSERCONF (PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values (SEQ_ER_BROWSERCONF.nextval,browserID,'PROTOCOL_NAME',3,'{"key":"PROTOCOL_NAME", "label":"Calendar", "sortable":true, "resizeable":true,"hideable":true}','Calender');
Insert into ER_BROWSERCONF (PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values (SEQ_ER_BROWSERCONF.nextval,browserID,'VISIT_NAME',4,'{"key":"VISIT_NAME", "label":"Visit", "sortable":true, "resizeable":true,"hideable":true}','Visit');
Insert into ER_BROWSERCONF (PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values (SEQ_ER_BROWSERCONF.nextval,browserID,'EVENT_SCHDATE',5,'{"key":"EVENT_SCHDATE", "label":"Scheduled Date", "resizeable":true,"hideable":true}','Scheduled Date');
Insert into ER_BROWSERCONF (PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values (SEQ_ER_BROWSERCONF.nextval,browserID,'EVENT_NAME',6,'{"key":"EVENT_NAME", "label":"Events", "sortable":true, "resizeable":true,"hideable":true}','Events');
Insert into ER_BROWSERCONF (PK_BROWSERCONF,FK_BROWSER,BROWSERCONF_COLNAME,BROWSERCONF_SEQ,BROWSERCONF_SETTINGS,BROWSERCONF_EXPLABEL) 
values (SEQ_ER_BROWSERCONF.nextval,browserID,'STORAGE_KIT',7,'{"key":"STORAGE_KIT", "label":"Storage kit",  "resizeable":true,"hideable":true}','Storage Kit');

commit;

END;
/

Insert into ER_OBJECT_SETTINGS (PK_OBJECT_SETTINGS,OBJECT_TYPE,OBJECT_SUBTYPE,OBJECT_NAME,OBJECT_SEQUENCE,OBJECT_VISIBLE,OBJECT_DISPLAYTEXT,FK_ACCOUNT) 
values (SEQ_ER_OBJECT_SETTINGS.nextval,'M','prep_menu','inv_menu',4,0,'Preparation Area',0);

Insert into ER_OBJECT_SETTINGS (PK_OBJECT_SETTINGS,OBJECT_TYPE,OBJECT_SUBTYPE,OBJECT_NAME,OBJECT_SEQUENCE,OBJECT_VISIBLE,OBJECT_DISPLAYTEXT,FK_ACCOUNT) 
values (SEQ_ER_OBJECT_SETTINGS.nextval,'T','4','inv_tab',4,0,'Preparation Area',0);

COMMIT;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,100,1,'01_InvBrowser.sql',sysdate,'8.10.0 Build#557');

commit;
