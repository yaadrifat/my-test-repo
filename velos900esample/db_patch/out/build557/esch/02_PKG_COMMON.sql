set define off;

create or replace
PACKAGE        "PKG_COMMON" as

FUNCTION SCH_GETPATCODE  (p_patprot NUMBER)
    RETURN  varchar2 ;

FUNCTION VALIDATENAME (
   EVENTID     NUMBER,
   USERID      NUMBER,
   EVENTNAME   VARCHAR2,
   EVENTTYPE   VARCHAR2, CHAINID VARCHAR2, CALASSOCTO VARCHAR2 )
   RETURN varchar2 ;


/*
** The function checks if the event name is already existing or not. It returns a -1 if a duplicate
** is found. -1 is passed in the EVENTID parameter if the name is to be checked during an Insert.
** To check during an UPDATE the event_id is passed in the EVENTID parameter. This is used to check the name
** in all other events other than this particular EVENT_ID
**
** Return Value
** -1 on finding a duplicate
**  0 on not finding a duplicate
*/

FUNCTION SCH_GETMAIL (p_message varchar2, p_params varchar2)
    RETURN  varchar2 ;

FUNCTION SCH_GETCELLMAILMSG (p_mesgtype varchar2)
    RETURN  varchar2 ;

FUNCTION SCH_GETMAILMSG (p_mesgtype varchar2)
    RETURN  varchar2 ;

end pkg_common ;
/

set define off

create or replace
PACKAGE BODY        "PKG_COMMON" AS

FUNCTION SCH_GETPATCODE  (p_patprot NUMBER)
    RETURN  VARCHAR2
IS
       patcode VARCHAR2 (20);
        BEGIN
             SELECT P.per_code
             INTO patcode
             FROM ER_PATPROT e , ER_PER P
             WHERE e.pk_patprot = p_patprot AND
              P.pk_per = e.fk_per;
            P(patcode);
             RETURN patcode ;
  END SCH_GETPATCODE;

FUNCTION Validatename (
   EVENTID     NUMBER,
   USERID      NUMBER,
   EVENTNAME   VARCHAR2,
   EVENTTYPE   VARCHAR2, 
   CHAINID VARCHAR2, 
   CALASSOCTO VARCHAR2
)
   RETURN VARCHAR2
IS
/*
** The function checks if the event name is already existing or not. It returns a -1 if a duplicate
** is found. -1 is passed in the EVENTID parameter if the name is to be checked during an Insert.
** To check during an UPDATE the event_id is passed in the EVENTID parameter. This is used to check the name
** in all other events other than this particular EVENT_ID
**
** Return Value
** -1 on finding a duplicate
**  0 on not finding a duplicate
** JM: 02DEC2010: #4538 and side affects 5590, 5593
** Validatename() made generalized for checking duplicate Calendar name on Study and Library module... 
*/
   EVENTCOUNT   NUMBER;
   v_tablename VARCHAR2(30);
   v_addStr VARCHAR2(1000);
   
   
BEGIN
   if (CHAINID >0) then
	v_tablename := 'EVENT_ASSOC';
	v_addStr := 'AND CHAIN_ID = '||CHAINID||' AND EVENT_CALASSOCTO='''||CALASSOCTO||'''';
   else
	v_tablename := 'EVENT_DEF';
	v_addStr :='  ';

   end if;

 -- If EVENTID is -1 then its a check during a INSERT --JM: and it happens currently for library only....
    IF (EVENTID = -1) THEN
      IF ((EVENTTYPE = 'P') OR (EVENTTYPE = 'E')OR (EVENTTYPE = 'L')) THEN
         execute immediate 'SELECT COUNT (EVENT_ID) 
	 FROM EVENT_DEF 
          WHERE USER_ID ='|| USERID||
            ' AND EVENT_TYPE ='''|| EVENTTYPE||''' AND trim(LOWER(NAME)) = trim(LOWER('''||EVENTNAME||''')) '
	    INTO EVENTCOUNT;

         IF (EVENTCOUNT > 0) THEN
            RETURN (-1);
         END IF;
      END IF;
   --
   -- ELSE if the EVENTID is not equal to -1, then its a check during an UPDATE.
   -- The EVENTID contains the event is of the event whoes name has to be checked
   --
   ELSE
      IF ((EVENTTYPE = 'P') OR (EVENTTYPE = 'E') OR (EVENTTYPE = 'L')) THEN
         execute immediate 'SELECT COUNT (EVENT_ID) FROM ' 
         ||v_tablename|| 
         ' WHERE USER_ID ='|| USERID||
         ' AND EVENT_TYPE ='''|| EVENTTYPE||''' AND trim(LOWER(NAME)) = trim(LOWER('''||EVENTNAME||''')) '||v_addStr||  
         ' AND EVENT_ID !='|| EVENTID
	     INTO EVENTCOUNT;
            
         IF (EVENTCOUNT > 0) THEN
            RETURN (-1);
         END IF;
      END IF;
   END IF;
    

   RETURN (0);
END Validatename;





FUNCTION SCH_GETMAIL (p_message VARCHAR2, p_params VARCHAR2)
    RETURN  VARCHAR2
IS
       v_msg  VARCHAR2 (4000);
       V_POS      NUMBER         := 0;
       V_CNT      NUMBER         := 0;
       V_PARAMS   VARCHAR2 (2000)DEFAULT p_params || '~';
       V_STR      VARCHAR2 (2000)DEFAULT P_PARAMS || '~';
        BEGIN
         v_msg :=  p_message;
     /*replace the placeholders in message string in order of parameters*/
            LOOP
               EXIT WHEN V_PARAMS IS NULL;
               V_CNT := V_CNT + 1;
               V_POS := INSTR (V_STR, '~');
               V_PARAMS := SUBSTR (V_STR, 1, V_POS - 1);
               v_msg := REPLACE (v_msg, '~' || V_CNT || '~', V_PARAMS);
               V_STR := SUBSTR (V_STR, V_POS + 1);
         END LOOP;

            /*Test*/
      --p(v_msg);
              RETURN v_msg;
END SCH_GETMAIL;

FUNCTION SCH_GETCELLMAILMSG (p_mesgtype VARCHAR2)
    RETURN  VARCHAR2
IS
       v_msg  VARCHAR2 (4000);
        BEGIN
             SELECT msgtxt_short
             INTO v_msg
             FROM sch_msgtxt
             WHERE UPPER(msgtxt_type) = UPPER(p_mesgtype) ;
          --  p(v_msg);
             RETURN v_msg;
END SCH_GETCELLMAILMSG;

FUNCTION SCH_GETMAILMSG (p_mesgtype VARCHAR2)
    RETURN  VARCHAR2
IS
       v_msg  VARCHAR2 (4000);
        BEGIN
             SELECT dbms_lob.SUBSTR(msgtxt_long,4000,1)
             INTO v_msg
             FROM sch_msgtxt
             WHERE UPPER(msgtxt_type) = UPPER(p_mesgtype) ;
          --  p(v_msg);
             RETURN v_msg;
END SCH_GETMAILMSG ;

END Pkg_Common ;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,100,2,'02_pkg_common.sql',sysdate,'8.10.0 Build#557');

commit;