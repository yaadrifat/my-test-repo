* This readMe is specific to Velos eResearch version 9.0 build #611 */
=====================================================================================================================================
=====================================================================================================================================
Garuda :

We have released one Report (XSL Based) in this build. This is Garuda Specific Report.
So The 161.XSL and it's related script (07_ER_REPORT) are Specific to Garuda.

=====================================================================================================================================
eResearch:

For CTRP Non-industrial Draft, the validation and saving operations have been changed according to CTRP-20579-6 and CTRP-20576-8. The validation of mandatory fields takes place when the user clicks the 'Validate Draft' button or link. It happens without screen transition. If there is a validation error, a dialog opens to inform the user of an error, and when the user closes the dialog, the focus takes the user to an error with a message indicating why it failed. If the validation succeeds, another dialog opens for the next user action. The user will have the option to save the draft as 'Ready For Submission'. But saving as 'Ready For Submission' is still under construction.

The workflow surrounding the dialog is mentioned in CTRP-20579-6.

The saving operation also has been changed. The 'Submit' button has been changed to 'Save' button. The user can now save a draft even with missing mandatory values. This is a new concept. The reason is that the user wants to save it as 'Work In Progress' and then come back to it to complete it later. For the save operation, eResearch still has to do a basic validation to make sure the incomplete draft can be saved in database especially for data type compatibility.  This is especially true if the user enters an incorrect date format in a date field. In that case, the draft cannot be saved; therefore, the UI will focus back on the errored field and show a message. We will call this kind of checks 'basic validations'. The mandatory field checks mentioned above will be called 'business validations' because they are from the business requirements.

All this will be implemented in the Industrial Draft in the same way in a future build.

=====================================================================================================================================
For Localization:

Following Files have been Modified:

1	appendix_url.jsp
2	calMilestoneSetup.js
3	coverage.js
4	datagrid.js
5	labelBundle.properties
6	LC.java
7	LC.jsp
8	link.jsp
9	linklist.jsp
10	manageVisitsGrid.js
11	manageVisitsGrid.js
12	MC.java
13	MC.jsp
14	messageBundle.properties
15	milestonegrid.js
16	milestonegrid.js
17	multiSpecimens.js
18	multiSpecimens.js
19	myhome.jsp
20	study.jsp
21  ereslogin.jsp
22  login.css

=====================================================================================================================================
