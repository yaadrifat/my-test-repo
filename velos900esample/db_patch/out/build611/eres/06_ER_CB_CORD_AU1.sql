CREATE OR REPLACE TRIGGER ER_CB_CORD_AU1
AFTER UPDATE OF CORD_NMDP_STATUS 
ON CB_CORD 
FOR EACH ROW 
DECLARE
v_entity_type NUMBER :=0;
BEGIN 
select pk_codelst into v_entity_type from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU';

INSERT INTO CB_ENTITY_STATUS(PK_ENTITY_STATUS,ENTITY_ID,FK_ENTITY_TYPE,STATUS_TYPE_CODE,FK_STATUS_VALUE,STATUS_DATE,LAST_MODIFIED_BY,LAST_MODIFIED_DATE)
VALUES(SEQ_CB_ENTITY_STATUS.nextval,:old.pk_cord,v_entity_type,'Cord_Nmdp_Status',:new.CORD_NMDP_STATUS,sysdate,:new.LAST_MODIFIED_BY,sysdate);

END; 
/ 

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,154,6,'06_ER_CB_CORD_AU1.sql',sysdate,'9.0.0 Build#611');

commit;

