begin
 DBMS_SCHEDULER.drop_job (job_name => 'CREATE_WORKFLOW_JOB');
 end;
 /


begin
DBMS_SCHEDULER.create_job (
    job_name        => 'Create_Workflow_job',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'begin sp_workflow_temp;end;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'freq=SECONDLY',
    end_date        => NULL,
    enabled         => TRUE,
    comments        => 'Job to create workflow.');
end;
/


begin
dbms_scheduler.run_job('CREATE_WORKFLOW_JOB');
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,154,8,'08_RECREATE_JOB.sql',sysdate,'9.0.0 Build#611');

commit;