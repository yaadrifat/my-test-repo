drop trigger "ESCH"."SCH_PORTAL_FORMS_B0";

create or replace TRIGGER "ESCH"."SCH_PORTAL_FORMS_BU0"
BEFORE UPDATE
ON SCH_PORTAL_FORMS
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
when (NEW.last_modified_by IS NOT NULL)
begin
	:new.last_modified_date := sysdate;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,107,7,'07_SCH_PORTAL_FORMS_BU0.sql',sysdate,'8.10.0 Build#564');

commit;