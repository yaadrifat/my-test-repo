set define off;
DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'ER_STUDY' AND COLUMN_NAME = 'FDA_REGULATED_STUDY';
IF (v_column_exists = 0) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE ER_STUDY ADD(FDA_REGULATED_STUDY NUMBER)';
END IF;
END;
/
COMMENT ON COLUMN ER_STUDY.FDA_REGULATED_STUDY IS 'Identifies FDA regulated study flag 1-Yes, 0-No';
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,161,28,'28_ER_STUDY_ALTER.sql',sysdate,'9.0.0 Build#618');

commit;