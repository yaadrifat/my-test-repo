Set Define off;


--Sql for the table ER_CODELST where CODELST_TYPE is Test Outcome 


   Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'test_outcome','posi','Positive','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   
   Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'test_outcome','negi','Negative','N',2,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   
   
   Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD, CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'test_outcome','not_done','Not Done','N',3,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
   

   
   
--Sql for the table ER_CODELST where CODELST_TYPE is Test Reason 
   
    Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'test_reason','test_incon','Test Inconclusive','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   
    Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'test_reason','QA/QC','QA/QC','N',2,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
   
    Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'test_reason','add_samp_test','Additional Sample Testing','N',3,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   
    Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'test_reason','research','Research','N',4,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   
   
    Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'test_reason','other','Other','N',5,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
   
  
      
       --Sql for the table ER_CODELST where CODELST_TYPE is Test Method 
   
    Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'test_method','tot','Total(BFUE+BM+DEMM)','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'test_method','gm','GM','N',2,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   
   
   
     --Sql for the table ER_CODELST where CODELST_TYPE is Test Source   
   
    Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'test_source','cbu','CBU','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   
     Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'test_source','maternal','MATERNAL','N',2,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


-----------------Association Status-----------------------------------
   
  Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'assocition_stat','current','Current','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);



Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'assocition_stat','closed','Closed','N',2,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


--------------------------------------------------------

--STARTS INSERT ROW IN ER_CODELST 
DECLARE
 v_record_exits number := 0;  
BEGIN
  Select count(*) into v_record_exits
    from er_codelst
    where  CODELST_SUBTYP = 'resolved' and codelst_type='order_status'; 

  if (v_record_exits = 0) then
      Insert into ER_CODELST 
(PK_CODELST,
FK_ACCOUNT,
CODELST_TYPE,
CODELST_SUBTYP,
CODELST_DESC,
CODELST_HIDE,
CODELST_SEQ,
CODELST_MAINT,
RID,
CREATOR,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
CREATED_ON,
IP_ADD,
CODELST_CUSTOM_COL,
CODELST_CUSTOM_COL1,
CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,
null,
'order_status',
'resolved',
'Resolved',
'N',null,null,null,null,null,null,sysdate,null,null,null,null);

  end if;
end;
/
--END--


------------------------Eligibile_cat----------------------------------
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
	CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
	CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
	(SEQ_ER_CODELST.nextval,null,'eligibile_cat','cbu_assessment','Notification Of CBU Assessment','N',8,'Y',null,
	null,null,sysdate,sysdate,null,null,null,null);

------------------------------------------------------------------------

   --Sql for the table ER_CODELST where CODELST_TYPE is TNC SAMPLE TESTING   inlcude in specimen_type
   
   Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'specimen_type','seg','Segment','N',7,null,null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   
   
    Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'specimen_type','filt_pap','Filter','N',8,null,null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   
   
   Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'specimen_type','rbc_pell','Rbc pellet','N',9,null,null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'specimen_type','ext_dna_ali','Extracted Dna Aliquot','N',10,null,null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   
   Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'specimen_type','non_viacel','Non-Viable Cell Aliquot','N',11,null,null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'specimen_type','pla_ali','Plasma Aliquot','N',12,null,null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   
   Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'specimen_type','ser_ali','Serum Aliquot','N',13,null,null,
   null,null,sysdate,sysdate,null,null,null,null);
   
   
   
    Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'specimen_type','viacel_ali','Viable Cell Aliquot','N',14,null,null,
   null,null,sysdate,sysdate,null,null,null,null);


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'ineligible','mtrn_rsk_ques','Maternal Risk Questionnaire','N',6,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'ineligible','phy_find_ass','Physical Findings_Assessment','N',6,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'ineligible','oth_med_rec','Other Medical Record','N',6,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,null,'ineligible','infe_desc_mrk','Infectious Disease Marker Testing','N',6,'Y',null,null,null,sysdate,sysdate,null,null,null,null);
   

Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,
   CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,
   CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
   values(SEQ_ER_CODELST.nextval,null,'theme','th_cdr_home','CDR Home','N',11,null,null,null,null,sysdate,sysdate,
   null,'cdrhome',null,null);
   


Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'source_categ','mbr','MBR','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);



Insert into ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
   CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
   CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
   (SEQ_ER_CODELST.nextval,null,'race_rollup','cau','CAU','N',1,'Y',null,
   null,null,sysdate,sysdate,null,null,null,null);


--STARTS INSERT ROW IN ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where  CODELST_SUBTYP = 'HLA_RES_REC' and codelst_type='order_status';

  if (v_record_exists = 0) then
      Insert into ER_CODELST
(PK_CODELST,
FK_ACCOUNT,
CODELST_TYPE,
CODELST_SUBTYP,
CODELST_DESC,
CODELST_HIDE,
CODELST_SEQ,
CODELST_MAINT,
RID,
CREATOR,
LAST_MODIFIED_BY,
LAST_MODIFIED_DATE,
CREATED_ON,
IP_ADD,
CODELST_CUSTOM_COL,
CODELST_CUSTOM_COL1,
CODELST_STUDY_ROLE) values (SEQ_ER_CODELST.nextval,
null,
'order_status',
'HLA_RES_REC',
'HLA Result Received',
'N',null,null,null,null,null,null,sysdate,null,null,null,null);
commit;
  end if;
end;
/
--END-- 


Commit;
   
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,145,6,'06_ER_CODELST_INSERT.sql',sysdate,'9.0.0 Build#602');

commit;