Set Define off;

alter table CB_ENTITY_STATUS_REASON add NOT_APP_PRIOR_FLAG varchar2(1 BYTE);

COMMENT ON COLUMN "CB_ENTITY_STATUS_REASON"."NOT_APP_PRIOR_FLAG" IS 'This column will store the eligibility reason not applicbale collected to prior is checked or not'; 

Alter table CB_CORD add  (FK_SOURCE_CATEG NUMBER(10),FK_FUND_CATEG NUMBER(10), FK_FUND_RACE_ROLLUP NUMBER(10));

COMMENT ON COLUMN "CB_CORD"."FK_SOURCE_CATEG" IS 'This 
column will store the eligibility reason not applicbale collected to prior is 
checked or not';

COMMENT ON COLUMN "CB_CORD"."FK_FUND_CATEG" IS 'This column stores the funding category code possible values are defined in code list';


COMMENT ON COLUMN "CB_CORD"."FK_FUND_CATEG" IS 'This column stores the fund race rollup possible values are defined in code list';


alter table er_site modify SITE_ID varchar2(50 BYTE);

--Sql for the table ER_PATLABS to add the column FK_TEST_OUTCOME

DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'ER_PATLABS'  AND column_name = 'FK_TEST_OUTCOME';
   if (countFlage = 0) then
 execute immediate ('alter table ER_PATLABS add FK_TEST_OUTCOME number(10)');
  end if;
end;
/
 COMMENT ON COLUMN ER_PATLABS.FK_TEST_OUTCOME IS 'Code for test_outcome referenced from Code List(ER_CODELST).'; 
commit;



--Sql for the table ER_PATLABS and Column CMS APPROVED LAB
DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'ER_PATLABS'  AND column_name = 'CMS_APPROVED_LAB';
   if (countFlage = 0) then
 execute immediate ('alter table ER_PATLABS add CMS_APPROVED_LAB number(1)');
  end if;
end;
/
 COMMENT ON COLUMN ER_PATLABS.CMS_APPROVED_LAB IS 'This column is for the CMS APPROVED LAB.'; 
commit;


--Sql for the table ER_PATLABS and Column FDA LICENSED LAB
DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'ER_PATLABS'  AND column_name = 'FDA_LICENSED_LAB';
   if (countFlage = 0) then
 execute immediate ('alter table ER_PATLABS add FDA_LICENSED_LAB number(1)');
  end if;
end;
/
 COMMENT ON COLUMN ER_PATLABS.FDA_LICENSED_LAB IS 'This column is for the FDA LICENSED LAB.'; 
commit;


--Sql for the table ER_PATLABS and Column FK_TEST_REASON 

DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'ER_PATLABS'  AND column_name = 'FK_TEST_REASON';
   if (countFlage = 0) then
 execute immediate ('alter table ER_PATLABS add FK_TEST_REASON number(10)');
  end if;
end;
/
 COMMENT ON COLUMN ER_PATLABS.FK_TEST_REASON IS 'This column is for the FK TEST REASON.'; 
commit;

--Sql for the table ER_PATLABS and Column FK_TEST_METHOD 

DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'ER_PATLABS'  AND column_name = 'FK_TEST_METHOD';
   if (countFlage = 0) then
 execute immediate ('alter table ER_PATLABS add FK_TEST_METHOD number(10)');
  end if;
end;
/
 COMMENT ON COLUMN ER_PATLABS.FK_TEST_METHOD IS 'This column is for the FK TEST METHOD.'; 
commit;


--Sql for the table ER_PATLABS and Column FT_TEST_SOURCE

DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'ER_PATLABS'  AND column_name = 'FT_TEST_SOURCE';
   if (countFlage = 0) then
 execute immediate ('alter table ER_PATLABS add FT_TEST_SOURCE number(10)');
  end if;
end;
/
 COMMENT ON COLUMN ER_PATLABS.FT_TEST_SOURCE IS 'This column is for the FT TEST SOURCE.'; 
commit;

--Sql for the table ER_SPECIMEN to add the column sequence 

DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'ER_SPECIMEN'  AND column_name = 'SPEC_SEQUENCE';
   if (countFlage = 0) then
 execute immediate ('alter table ER_SPECIMEN add SPEC_SEQUENCE number(10)');
  end if;
end;
/
 COMMENT ON COLUMN ER_SPECIMEN.SPEC_SEQUENCE IS 'This column will store specimen Sequence.'; 
commit;



--Sql for the table ER_SPECIMEN to add the column qcSampleInd 

DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'ER_SPECIMEN'  AND column_name = 'SPEC_QCSAMPLEIND';
   if (countFlage = 0) then
 execute immediate ('alter table ER_SPECIMEN add SPEC_QCSAMPLEIND varchar2(1)');
  end if;
end;
/
 COMMENT ON COLUMN ER_SPECIMEN.SPEC_QCSAMPLEIND IS 'This column will store QCSAMPLEIND.'; 
commit;




--Sql for the table ER_SPECIMEN to add the column dnaSamplesAvailableCode  

DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'ER_SPECIMEN'  AND column_name = 'SPEC_DNA_SAM_AVAILCODE';
   if (countFlage = 0) then
 execute immediate ('alter table ER_SPECIMEN add SPEC_DNA_SAM_AVAILCODE number(10)');
  end if;
end;
/
 COMMENT ON COLUMN ER_SPECIMEN.SPEC_DNA_SAM_AVAILCODE IS 'This column will store DNA SAMPLES AVAILABLE CODE.'; 
commit;




--Sql for the table ER_SPECIMEN to add the column otherSamplesAvailableCode   

DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'ER_SPECIMEN'  AND column_name = 'SPEC_OTH_SAM_AVAILCODE';
   if (countFlage = 0) then
 execute immediate ('alter table ER_SPECIMEN add SPEC_OTH_SAM_AVAILCODE number(10)');
  end if;
end;
/
 COMMENT ON COLUMN ER_SPECIMEN.SPEC_OTH_SAM_AVAILCODE IS 'This column will store OTHER SAMPLES AVAILABLE CODE.'; 
commit;



--STARTS MODIFY THE COLUMN THOU_UNIT_PER_ML_HEPARIN FROM CBB_PROCESSING_PROCEDURES_INFO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'THOU_UNIT_PER_ML_HEPARIN';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(THOU_UNIT_PER_ML_HEPARIN Number(5,2))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN THOU_UNIT_PER_ML_HEPARIN_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'THOU_UNIT_PER_ML_HEPARIN_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(THOU_UNIT_PER_ML_HEPARIN_PER Number(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.THOU_UNIT_PER_ML_HEPARIN_PER IS 'Stores 1,000 Unit/ml Heparin percentage value';


--STARTS MODIFY THE COLUMN FIVE_UNIT_PER_ML_HEPARIN FROM 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'FIVE_UNIT_PER_ML_HEPARIN';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(FIVE_UNIT_PER_ML_HEPARIN NUMBER(5,2))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN FIVE_UNIT_PER_ML_HEPARIN_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'FIVE_UNIT_PER_ML_HEPARIN_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(FIVE_UNIT_PER_ML_HEPARIN_PER NUMBER(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.FIVE_UNIT_PER_ML_HEPARIN_PER IS 'Stores 5,000 Unit/ml Heparin percentage value';


--STARTS MODIFY THE COLUMN TEN_UNIT_PER_ML_HEPARIN FROM 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'TEN_UNIT_PER_ML_HEPARIN';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(TEN_UNIT_PER_ML_HEPARIN NUMBER(5,2))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN TEN_UNIT_PER_ML_HEPARIN_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'TEN_UNIT_PER_ML_HEPARIN_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(TEN_UNIT_PER_ML_HEPARIN_PER NUMBER(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.TEN_UNIT_PER_ML_HEPARIN_PER IS 'Stores 10,000 Unit/ml Heparin percentage value';


--STARTS MODIFY THE COLUMN SIX_PER_HYDROXYETHYL_STARCH FROM 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'SIX_PER_HYDROXYETHYL_STARCH';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(SIX_PER_HYDROXYETHYL_STARCH Number(5,2))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN SIX_PER_HYDRO_STARCH_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'SIX_PER_HYDRO_STARCH_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(SIX_PER_HYDRO_STARCH_PER Number(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.SIX_PER_HYDRO_STARCH_PER IS 'Stores 6% Hydroxyethyl Starch percentage value';


--STARTS MODIFY THE COLUMN CPDA FROM
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'CPDA';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(CPDA Number(5,2))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN CPDA_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'CPDA_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(CPDA_PER Number(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.CPDA_PER IS 'Stores cpda percentage value';


--STARTS MODIFY THE COLUMN CPD FROM 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'CPD';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(CPD Number(5,2))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN CPD_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'CPD_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(CPD_PER Number(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.CPD_PER IS 'Stores cpd percentage value';


--STARTS MODIFY THE COLUMN ACD FROM 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'ACD';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(ACD Number(5,2))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN ACD_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'ACD_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(ACD_PER Number(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.ACD_PER IS 'Stores acd percentage value';


--STARTS MODIFY THE COLUMN OTHER_ANTICOAGULANT FROM 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'OTHER_ANTICOAGULANT';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(OTHER_ANTICOAGULANT Number(5,2))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN OTHER_ANTICOAGULANT_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'OTHER_ANTICOAGULANT_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(OTHER_ANTICOAGULANT_PER Number(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.OTHER_ANTICOAGULANT_PER IS 'Stores other anticoagulant percentage value';


--STARTS MODIFY THE COLUMN HUN_PER_DMSO FROM 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'HUN_PER_DMSO';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(HUN_PER_DMSO Number(5,2))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN HUN_PER_DMSO_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'HUN_PER_DMSO_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(HUN_PER_DMSO_PER Number(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.HUN_PER_DMSO_PER IS 'Stores 100% DMSO percentage value';


--STARTS MODIFY THE COLUMN HUN_PER_GLYCEROL FROM 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'HUN_PER_GLYCEROL';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(HUN_PER_GLYCEROL Number(5,2))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN HUN_PER_GLYCEROL_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'HUN_PER_GLYCEROL_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(HUN_PER_GLYCEROL_PER Number(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.HUN_PER_GLYCEROL_PER IS 'Stores 100% glycerol percentage value';


--STARTS MODIFY THE COLUMN TEN_PER_DEXTRAN_40 FROM 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'TEN_PER_DEXTRAN_40';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(TEN_PER_DEXTRAN_40 Number(5,2))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN TEN_PER_DEXTRAN_40_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'TEN_PER_DEXTRAN_40_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(TEN_PER_DEXTRAN_40_PER Number(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.TEN_PER_DEXTRAN_40_PER IS 'Stores 10% dextran 40 percentage value';



--STARTS MODIFY THE COLUMN FIVE_PER_HUMAN_ALBU FROM 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'FIVE_PER_HUMAN_ALBU';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(FIVE_PER_HUMAN_ALBU Number(5,2))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN FIVE_PER_HUMAN_ALBU_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'FIVE_PER_HUMAN_ALBU_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(FIVE_PER_HUMAN_ALBU_PER Number(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.FIVE_PER_HUMAN_ALBU_PER IS 'Stores 5% human albumin percentage value';


--STARTS MODIFY THE COLUMN TWEN_FIVE_HUM_ALBU FROM 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'TWEN_FIVE_HUM_ALBU';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(TWEN_FIVE_HUM_ALBU Number(5,2))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN TWEN_FIVE_HUM_ALBU_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'TWEN_FIVE_HUM_ALBU_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(TWEN_FIVE_HUM_ALBU_PER Number(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.TWEN_FIVE_HUM_ALBU_PER IS 'Stores 25% human albumin percentage value';


--STARTS MODIFY THE COLUMN PLASMALYTE FROM 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'PLASMALYTE';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(PLASMALYTE Number(5,2))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN PLASMALYTE_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'PLASMALYTE_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(PLASMALYTE_PER Number(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.PLASMALYTE_PER IS 'Stores plasmalyte percentage value';


--STARTS MODIFY THE COLUMN OTH_CRYOPROTECTANT FROM 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'OTH_CRYOPROTECTANT';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(OTH_CRYOPROTECTANT Number(5,2))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN OTH_CRYOPROTECTANT_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'OTH_CRYOPROTECTANT_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(OTH_CRYOPROTECTANT_PER Number(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.OTH_CRYOPROTECTANT_PER IS 'Stores other cryoprotectant percentage value';


--STARTS MODIFY THE COLUMN FIVE_PER_DEXTROSE FROM 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'FIVE_PER_DEXTROSE';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(FIVE_PER_DEXTROSE NUMBER(4,1))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN FIVE_PER_DEXTROSE_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'FIVE_PER_DEXTROSE_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(FIVE_PER_DEXTROSE_PER Number(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.FIVE_PER_DEXTROSE_PER IS 'Stores 5% dextrose percentage value';


--STARTS MODIFY THE COLUMN POINNT_NINE_PER_NACL FROM 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'POINNT_NINE_PER_NACL';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(POINNT_NINE_PER_NACL NUMBER(4,1))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN POINNT_NINE_PER_NACL_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'POINNT_NINE_PER_NACL_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(POINNT_NINE_PER_NACL_PER Number(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.POINNT_NINE_PER_NACL_PER IS 'Stores 0.9% nacl percentage value';


--STARTS MODIFY THE COLUMN OTH_DILUENTS FROM 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'OTH_DILUENTS';
  if (v_column_exists != 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO MODIFY(OTH_DILUENTS NUMBER(4,1))';
  end if;
end;
/
--END--




--STARTS ADDING COLUMN OTH_DILUENTS_PER TO 
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CBB_PROCESSING_PROCEDURES_INFO'
    AND column_name = 'OTH_DILUENTS_PER';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CBB_PROCESSING_PROCEDURES_INFO ADD(OTH_DILUENTS_PER Number(5,2))';
  end if;
end;
/
--END--

COMMENT ON COLUMN ERES.CBB_PROCESSING_PROCEDURES_INFO.OTH_DILUENTS_PER IS 'Stores other diluents percentage value';


DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'CB_ENTITY_SAMPLES'  AND column_name = 'FK_SPECIMEN';
   if (countFlage = 0) then
 execute immediate ('alter table CB_ENTITY_SAMPLES add FK_SPECIMEN number(10)');
  end if;
end;
/
 COMMENT ON COLUMN CB_ENTITY_SAMPLES.FK_SPECIMEN IS 'This column will store FK SPECIMEN.'; 
commit;

DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'ER_PATLABS' AND column_name = 'FK_TEST_SPECIMEN';
   if (countFlage = 0) then
 execute immediate ('alter table ER_PATLABS add FK_TEST_SPECIMEN number(10)');
  end if;
end;
/
 COMMENT ON COLUMN ER_PATLABS.FK_TEST_SPECIMEN IS 'This column will store FK TEST SAMPLE.'; 
commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,145,5,'05_MISC_ALTER.sql',sysdate,'9.0.0 Build#602');

commit;