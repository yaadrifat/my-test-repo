package com.velos.validator;

import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.web.user.UserJB;

public class DefaultValidator extends Validator
{
  

public DefaultValidator()
 {
  
 }
 public DefaultValidator(String validateField,String data,String targetVal)
 {
    super(validateField,data,targetVal);
	 
     
 }
 public boolean validate()
 {
     
   if ((validateFld.toLowerCase()).equals("esign"))
       return validateESig();
    
     return false;
     
 }
 private boolean validateESig()
 {
     UserJB userB =new UserJB();
     userB.setUserId(EJBUtil.stringToNum(dataArray[0]));
     userB.getUserDetails();
     String eSign = userB.getUserESign();
      if ((targetValue != null) && eSign.equals(targetValue)) {
    	  validationMessage="E-sign Successful";
         return true;
                             
     } else {
         return false;
     }   
 }
 
 
 
 public String getDataSet() {
     return dataSet;
 }
 public void setDataSet(String dataSet) {
     this.dataSet = dataSet;
 }
 public String getValidateFld() {
     return validateFld;
 }
 public void setValidateFld(String validateFld) {
     this.validateFld = validateFld;
 }
 
}