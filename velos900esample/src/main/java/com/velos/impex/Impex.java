///__astcwz_model_idt#(0) 
///__astcwz_maxim_idt#(2102) 
///__astcwz_packg_idt#(1046,1047,1048~,,)
package com.velos.impex;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import oracle.jdbc.driver.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.CLOB;
import oracle.xml.parser.v2.DOMParser;
import oracle.xml.parser.v2.XMLDocument;
import oracle.xml.parser.v2.XMLElement;
import oracle.xml.parser.v2.XMLNode;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.service.util.BrowserRows;
import com.velos.eres.service.util.EJBUtil;

///__astcwz_class_idt#(1010!n)

/**
 * Impex is a Wrapper class for A2A Export Import. The class extends java's
 * Thread class so that export and import methods can be called asynchronously.
 * 
 * @author Sonia Sahni
 * @version %I%, %G%
 * 
 */
public class Impex extends Thread {

    // /__astcwz_asscn_idt#(1767|1768%contains:ASSOCIATION^AGGREGATION!0..n)

    /**
     * ImpexRequest object containg the Import-Export request information
     */
    private ImpexRequest impexRequest;

    /**
     * Type of request. Possible Values:<BR>
     * E - Export, I - Import, ER - Reverse Export and IR - Reverse Import
     */
    private String requestType;

    /**
     * Path to the export file - a '.req' file <BR>
     * Required for import
     */
    private String importPath;

    /**
     * Hashtable for parameters that may be required for an import. the
     * key-value pairs are added to the Import Request XML and thus can be used
     * by the Import procedures
     */
    private Hashtable importParam;

    /**
     * Flag to indicate whether temporary import tables will be droppped after
     * an Import. The value is controlled from the database. Possible values:
     * 0-No, 1-Yes
     */
    private static String dropTableFlag = "0";

    private ArrayList exportData;

    /**
     * Returns the value of exportData.
     */
    public ArrayList getExportData() {
        return exportData;
    }

    /**
     * Sets the value of exportData.
     * 
     * @param exportData
     *            The value to assign exportData.
     */
    public void setExportData(ArrayList exportData) {
        this.exportData = exportData;
    }

    /**
     * Returns the value of dropTableFlag.
     */
    public static String getDropTableFlag() {
        return dropTableFlag;
    }

    /**
     * Logs an Export Request in the database.
     * 
     * @return Returns an ID for the Export Request
     */
    public int recordExportRequest() {
        /* Records export Request */

        CallableStatement cstmt = null;
        Connection conn = null;
        int intOUT = -1;
        java.sql.Timestamp tStamp = null;
        String strTimeStamp = null;

        Object[] siteArray = null;
        CLOB clValue = null;

        try {

            // DBLogger.log2DB ("debug", "MSG", "IMPEX -->
            // Impex.recordExportRequest started .. ");

            CommonDAO cDao = new CommonDAO();

            conn = cDao.getConnection();

            cstmt = conn
                    .prepareCall("{call pkg_impexcommon.SP_CREATE_EXPREQUEST( ? , ? , ? ,? , ? , ?,?) } ");

            strTimeStamp = impexRequest.getReqDateTime();

            // DBLogger.log2DB ("debug", "MSG", "IMPEX -->
            // Impex.recordExportRequest strTimeStamp : " + strTimeStamp);

            if ((strTimeStamp != null) && strTimeStamp.length() > 0) {
                tStamp = Timestamp.valueOf(strTimeStamp);
            }

            siteArray = impexRequest.getExpSitesArray();

            // DBLogger.log2DB ("debug", "MSG", "IMPEX -->
            // Impex.recordExportRequest impexRequest.getExpSites() : " +
            // impexRequest.getExpSites());

            ArrayDescriptor adArr = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ARRAY siteARR = new ARRAY(adArr, conn, siteArray);

            clValue = DBUtil.getCLOB(impexRequest.getReqDetails(), conn);

            // DBLogger.log2DB ("debug", "MSG", "IMPEX -->
            // Impex.recordExportRequest impexRequest.getExpSites() : " +
            // impexRequest.getExpSites());

            cstmt.setString(1, impexRequest.getReqBy());
            cstmt.setTimestamp(2, tStamp);
            cstmt.setArray(3, siteARR);
            cstmt.setClob(4, clValue);
            cstmt.setString(5, impexRequest.getReqStatus());
            cstmt.setString(6, impexRequest.getMsgStatus());
            cstmt.registerOutParameter(7, OracleTypes.INTEGER);

            // execute the procedure after setting the input and output
            // parameters
            cstmt.execute();

            System.out
                    .println("IMPEX --> Impex.recordExportRequest  : procedure executed");

            // if there are output parameters, read the parameters
            intOUT = cstmt.getInt(7); // returns the expId

            impexRequest.setReqId(intOUT);

            return intOUT;

        }// end of try
        catch (Exception ex) {
            System.out
                    .println("IMPEX --> exception in Impex.recordExportRequest  "
                            + ex.toString());
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> exception in Impex.recordExportRequest  "
                            + ex.toString());
            return intOUT;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    // /__astcwz_opern_idt#(1016)

    /**
     * Starts export process using the ImpexRequest object.
     * 
     * @param irObj
     *            ImpexRequest object with Export Details
     */
    public synchronized static void startExport(ImpexRequest irObj) {
        // will work on the export details

        String expDetails;

        String oldExpDetails;
        String newExpDetails;

        int expReqId = 0;
        String moduleName;
        int attrLength = 0;
        String filepath;
        int systemType;
        String logStr;

        String[] siteArray = null;

        ArrayList arrAttr = new ArrayList();

        ArrayList arrAttrValues = new ArrayList();
        ArrayList arrSites = new ArrayList();

        expReqId = irObj.getReqId();

        DBLogger.impexId = expReqId;

        DBLogger.log2DB("debug", "MSG", "IMPEX --> Starting Export........");

        Impex.updateExportStatus(expReqId, 1); // export started

        expDetails = irObj.getReqDetails();
        oldExpDetails = expDetails;

        siteArray = irObj.getExpSitesArray();

        systemType = EJBUtil.getSystemType();

        // populate arrSites to hold Site Information
        for (int ctr = 0; ctr < siteArray.length; ctr++) {
            A2ASite site = new A2ASite();
            site.setId(Integer.parseInt(siteArray[ctr]));
            site.getA2ASiteDetails();
            arrSites.add(site);

        }

        // all modules exported,persist the ImpexRequest object

        for (int ctr = 0; ctr < arrSites.size(); ctr++) {
            A2ASite site = new A2ASite();
            FTPInfo ftp = new FTPInfo();

            site = (A2ASite) arrSites.get(ctr);
            ftp = site.getFtpInfo();
            filepath = ftp.getFtpFolder();

            filepath = Impex.getFilePath(filepath, String.valueOf(irObj
                    .getReqId()));

            newExpDetails = Impex.addXMLElement(irObj.getReqDetails(),
                    "targetSite", site.getCode());

            if (!EJBUtil.isEmpty(filepath)) {
                irObj.setReqDetails(newExpDetails);
                Impex.persistObject(irObj, filepath, "eresearch.req");
                irObj.setReqDetails(oldExpDetails);
            }
        }
        //

        // parse this expDetails to get the list of modules to export

        arrAttr.add("name");

        arrAttrValues = Impex.getXMLElementAttributes(expDetails, "module",
                arrAttr);

        attrLength = arrAttrValues.size();

        for (int i = 0; i < attrLength; i++) {

            Hashtable ht = new Hashtable();

            ht = (Hashtable) arrAttrValues.get(i);

            moduleName = (String) ht.get("name");

            Module md = new Module();

            md.setName(moduleName);
            md.setType("E");
            md.readData(expReqId);

            // Persist module for each participating site

            for (int ctr = 0; ctr < arrSites.size(); ctr++) {
                A2ASite site = new A2ASite();
                FTPInfo ftp = new FTPInfo();

                site = (A2ASite) arrSites.get(ctr);
                ftp = site.getFtpInfo();
                filepath = ftp.getFtpFolder();

                filepath = Impex.getFilePath(filepath, String.valueOf(irObj
                        .getReqId()));

                if (!EJBUtil.isEmpty(filepath)) {
                    Module.persistModuleData(md, filepath);
                }
            }

        } // end of for - for modules

        DBLogger.log2DB("debug", "MSG", "IMPEX --> Finished Export........");

        // generate Log Files
        logStr = DBLogger.generateLogFileData(irObj.getReqId());

        System.out.println("logStr************" + logStr);

        for (int ctr = 0; ctr < arrSites.size(); ctr++) {
            A2ASite site = new A2ASite();
            FTPInfo ftp = new FTPInfo();

            site = (A2ASite) arrSites.get(ctr);
            ftp = site.getFtpInfo();
            filepath = ftp.getFtpFolder();

            filepath = Impex.getFilePath(filepath, String.valueOf(irObj
                    .getReqId()));
            System.out.println("Persisting logStr************ filepath"
                    + filepath);

            if (!EJBUtil.isEmpty(filepath)) {

                Impex
                        .persistObject(logStr, filepath, irObj.getReqId()
                                + ".txt");
            }
        }
        //
        Impex.updateExportStatus(expReqId, 2);

    }

    // /__astcwz_opern_idt#(1077)
    /**
     * Starts Import process.
     * 
     * @param reqPath
     *            Path to the export file - a '.req' file <BR>
     *            The .req file contains information required for Import.
     * @param importId
     *            Id for the Import Request
     * @param htImportParam
     *            Hashtable for parameters that may be required for an import.
     *            <BR>
     *            The key-value pairs are added to the Import Request XML and
     *            thus can be used by the Import procedures
     * 
     */
    public static synchronized void startImport(String reqPath, int importId,
            Hashtable htImportParam) {
        ImpexRequest impexR = new ImpexRequest();
        ObjectInputStream in = null;
        FileInputStream fis = null;

        ArrayList arrAttr = new ArrayList();
        ArrayList arrAttrValues = new ArrayList();
        String expDetails;
        String expDetailsForImp;
        int attrLength = 0;
        String moduleName;

        String folderPath;
        String filePath;
        String logStr;

        Enumeration paramNames = null;
        String param = null;
        String paramValue = null;
        String importCategory = null;
        int status = 0;

        try {

            folderPath = Impex.getFolderPath(reqPath);
            
            System.out.println("IMPEX---------------->folderPath" + folderPath);
            System.out.println("IMPEX---------------->reqPath" + reqPath);
            
            fis = new FileInputStream(reqPath);
            in = new ObjectInputStream(fis);
            impexR = (ImpexRequest) in.readObject();

            in.close();

            // this.setImpexRequest(impex);

            expDetails = impexR.getReqDetails();
            expDetailsForImp = impexR.getReqDetails();

            DBLogger.impexId = impexR.getReqId();

            // add ImportParams to the exp Details

            paramNames = htImportParam.keys();

            while (paramNames.hasMoreElements()) {
                param = (String) paramNames.nextElement();
                paramValue = (String) htImportParam.get(param);

                // append this to the expDetails

                expDetailsForImp = Impex.addXMLElement(expDetailsForImp, param,
                        paramValue);

                if (param.equalsIgnoreCase("importCategory"))
                    importCategory = paramValue;

            }

            Impex.updateImportRequest(importId, expDetailsForImp, 1, impexR
                    .getReqId());

            DBLogger
                    .log2DB("debug", "MSG", "IMPEX --> Starting Import........");

            if (!EJBUtil.isEmpty(importCategory)) {
                status = Impex.alterTrigger(importCategory, "D");
            }

            if (status >= 0) {
                // parse this expDetails to get the list of modules to export

                arrAttr.add("name");
                arrAttrValues = Impex.getXMLElementAttributes(expDetails,
                        "module", arrAttr);
                attrLength = arrAttrValues.size();

                // read each imported module and save data
                for (int i = 0; i < attrLength; i++) {
                    Module md = new Module();

                    Hashtable ht = new Hashtable();
                    ht = (Hashtable) arrAttrValues.get(i);
                    moduleName = (String) ht.get("name");

                    fis = new FileInputStream(Impex.getFilePath(folderPath,
                            moduleName + ".vel"));

                    in = new ObjectInputStream(fis);
                    md = (Module) in.readObject();

                    md.setType("I"); // import type module
                    DBLogger.log2DB("debug", "MSG",
                            "IMPEX -->  Import........getDropTableFlag"
                                    + Impex.getDropTableFlag());
                    md.setDropModuleTableFlag(Impex.getDropTableFlag());
                    md.saveData(importId);
                    in.close();

                }

                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> Finished Import........");

                // generate Log Files
                logStr = DBLogger.generateLogFileData(impexR.getReqId());

                // filePath =
                // Impex.getFilePath(folderPath,String.valueOf(impexRequest.getReqId()));

                Impex.persistObject(logStr, folderPath, "import_"
                        + impexR.getReqId() + ".txt");

                if (!EJBUtil.isEmpty(importCategory)) {
                    Impex.alterTrigger(importCategory, "E");
                }

            } // end of status
            Impex.updateImportStatus(importId, 2);

        } catch (Exception ex) {
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> exception in Impex.startImport   "
                            + ex.toString());
        }

    }

    /*
     * public void readRequestDetails (){ }
     * 
     * public void updateReqStatus (){ }
     */

    /**
     * Gets ImpexRequest object
     * 
     * @return ImpexRequest object
     */
    public ImpexRequest getImpexRequest() {
        return this.impexRequest;
    }

    /**
     * Sets ImpexRequest object
     * 
     * @param impexRequest
     *            An ImpexRequest object
     */
    public void setImpexRequest(ImpexRequest impexRequest) {
        this.impexRequest = impexRequest;
    }

    /**
     * Searches an XML string for an element and its attributes <BR>
     * and returns an ArrayList of Hashtable objects with key value pairs for
     * the element's attributes and <BR>
     * their respective values
     * 
     * @param xmlTypeStr
     *            XML string to search
     * @param elementName
     *            Element Name to search in the XML
     * @param arAttributes
     *            ArrayList of attributes whose values are required
     * @return an ArrayList of Hashtable objects with key value pairs for the
     *         element's attributes and <BR>
     *         their respective values
     * 
     */
    public static ArrayList getXMLElementAttributes(String xmlTypeStr,
            String elementName, ArrayList arAttributes) {

        int attrCt = 0;

        attrCt = arAttributes.size();
        String attrStr = null;
        String attrVal = null;

        ArrayList arrReturn = new ArrayList();

        try {
            DOMParser parser = new DOMParser();
            parser.setPreserveWhitespace(true);
            parser.parse(new StringReader(xmlTypeStr));
            XMLDocument doc = parser.getDocument();

            NodeList nl = doc.getElementsByTagName(elementName);

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> getXMLElementAttributes........");

            for (int n = 0; n < nl.getLength(); n++) {
                Hashtable htAtt = new Hashtable();

                XMLElement elem = (XMLElement) nl.item(n);
                XMLNode textNode = (XMLNode) elem.getFirstChild();

                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> getXMLElementAttributes........ element :"
                                + n);

                for (int i = 0; i < attrCt; i++) // iterate for every
                // parameter
                {
                    attrStr = (String) arAttributes.get(i);
                    attrVal = elem.getAttribute(attrStr);

                    htAtt.put(attrStr, attrVal);

                }

                arrReturn.add(htAtt);

            }

        } // end of try
        catch (Exception e) {
            e.printStackTrace(System.out);
        }
        // System.out.println("outXML" + outXML);
        return arrReturn;
    }

    /**
     * Extracts path till the last folder from a String containing a a folder
     * path
     * 
     * @param path
     *            String containing a folder path
     * @return path till the last folder
     */
    public static String getFolderPath(String path) {
        // extract path till the last folder

        int sType = 0;
        int delimitPos = 0;

        sType = EJBUtil.getSystemType();
        if (sType == 0) {
            delimitPos = path.lastIndexOf("\\");
        } else {
            delimitPos = path.lastIndexOf("//");
        }

        if (delimitPos > 0) {
            path = path.substring(0, delimitPos);
        }

        return path;

    }

    /**
     * Appends a file name to a folder path according to the Application
     * Server's OS
     * 
     * @param folderPath
     *            Folder Path to the file
     * @param fileName
     *            Name of the file
     * @return full path to the file
     */
    public static String getFilePath(String folderPath, String fileName) {

        int sType = 0;

        sType = EJBUtil.getSystemType();

        String path = folderPath;

        if (sType == 0) {
            path = folderPath + "\\" + fileName;

        } else {
            path = folderPath + "//" + fileName;
        }

        return path;

    }

    /**
     * Persists an Object to secondary storage.
     * 
     * @param obj
     *            Object to be persisted
     * @param filePath
     *            File Location
     * @param fileName
     *            Name of the file for the persisted object
     * @return Success Flag. Possible Values: 0-ifSuccessful, -1 - if
     *         unsuccessful
     */
    public static int persistObject(Object obj, String filePath, String fileName) {
        // serialize the object

        int sType = 0;
        int ret = 0;
        boolean success;

        sType = EJBUtil.getSystemType();
        try {

            success = (new File(filePath)).mkdirs();

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> Impex.persistObject success : " + success);

            filePath = Impex.getFilePath(filePath, fileName);

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> Impex.persistObject filePath : " + filePath);

            FileOutputStream fos = null;
            ObjectOutputStream out1 = null;
            fos = new FileOutputStream(filePath);
            out1 = new ObjectOutputStream(fos);
            out1.writeObject(obj);
            out1.close();
            return ret;

        } catch (Exception ex) {
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> exception in Impex.persistObject(): "
                            + ex.toString());
            ret = -1;
            return ret;
        }
        // end of serialize

        // end of method
    }

    /**
     * Overrides Thread's run method.
     */
    public void run() {
        Impex.impexWrapper(this);
    }

    /**
     * A Wrapper method to call appropriate Impex method according to the
     * requestType of the Impex Object. Sets an passed Impex objects attribute -
     * exportData with export date with Export data if requestType is "ER" (for
     * reverse Export)
     * 
     * @param impx
     *            Impex Object
     * 
     */
    public static synchronized void impexWrapper(Impex impx) {
        // get setting for A2Adroptable

        CtrlDao ctrlDao = new CtrlDao();
        ctrlDao.getControlValues("A2Adroptable");

        ArrayList arVal = new ArrayList();
        arVal = ctrlDao.getCValue();

        String ctrlDropTableFlag = "";

        DBLogger.log2DB("debug", "MSG", "IMPEX --> DropTableFlag arsize: "
                + arVal.size());

        if (arVal.size() > 0) {
            ctrlDropTableFlag = (String) arVal.get(0);
        }

        dropTableFlag = ctrlDropTableFlag;

        DBLogger.log2DB("debug", "MSG", "IMPEX --> DropTableFlag : "
                + dropTableFlag);

        // Hashtable htData = new Hashtable();
        ArrayList arData = new ArrayList();

        System.out.println("Starting Request: " + impx.requestType
                + impx.toString());
        if (impx.requestType.equals("E")) {
            Impex.startExport(impx.impexRequest);

        } else if (impx.requestType.equals("I")) {
            Impex.startImport(impx.getImportPath(), impx.impexRequest
                    .getReqId(), impx.getImportParam());

        } else if (impx.requestType.equals("ER")) // reverse export
        {
            arData = Impex.getReverseExportData(impx.impexRequest);
            impx.setExportData(arData);

        } else if (impx.requestType.equals("IR")) // reverse import
        {
            Impex.startReverseImport(impx.impexRequest);

        }

    }

    /**
     * Gets request type
     * 
     * @return request type
     */
    public String getRequestType() {
        return this.requestType;
    }

    /**
     * Sets request type
     * 
     * @param requestType
     *            request type
     */

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    /**
     * Gets Import path
     * 
     * @return Import path
     */

    public String getImportPath() {
        return this.importPath;
    }

    /**
     * Sets Import path
     * 
     * @param importPath
     *            Import Path
     */

    public void setImportPath(String importPath) {
        this.importPath = importPath;
    }

    /**
     * update export request status
     * 
     * @param expId
     *            Export Id
     * @param status
     *            Status flag
     * @return -1 in case of any error
     */

    public static int updateExportStatus(int expId, int status) {
        /* Records export Request */

        PreparedStatement pstmt = null;
        Connection conn = null;

        int intOUT = -1;

        try {
            DBLogger
                    .log2DB("debug", "MSG", "IMPEX --> Impex.updateImpexStatus");

            CommonDAO cDao = new CommonDAO();
            conn = cDao.getConnection();

            pstmt = conn
                    .prepareStatement("Update er_expreqlog set EXP_STATUS = ? where PK_EXPREQLOG = ? ");

            pstmt.setInt(1, status);
            pstmt.setInt(2, expId);

            // execute the statement
            intOUT = pstmt.executeUpdate();

            if (!conn.getAutoCommit()) {
                conn.commit();
            }
            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> Impex.updateImpexStatus : statement executed");

            return intOUT;
        }// end of try
        catch (Exception ex) {
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> exception in Impex.updateImpexStatus  "
                            + ex.toString());
            return intOUT;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * Appends an element and its value to an XML string. Returns the new XML.
     * 
     * @param xmlTypeStr
     *            XML String
     * @param elementName
     *            Name of the element to be appended
     * @param value
     *            Value of the element
     * @return New XML string
     */
    public static String addXMLElement(String xmlTypeStr, String elementName,
            String value) {
        String outXML = xmlTypeStr;

        try {

            DOMParser parser = new DOMParser();
            parser.setPreserveWhitespace(true);
            parser.parse(new StringReader(outXML));
            XMLDocument doc = parser.getDocument();

            // rootElement = doc.getDocumentElement();
            NodeList nl = doc.getElementsByTagName("exp");

            // System.out.println("nl.getLength(): " + nl.getLength());

            for (int n = 0; n < nl.getLength(); n++) {
                Element element = doc.createElement(elementName);
                Text txt = doc.createTextNode(value);
                element.appendChild(txt);

                XMLElement rootelem = (XMLElement) nl.item(n);

                rootelem.appendChild(element);

            }

            StringWriter sw = new StringWriter();
            doc.print(new PrintWriter(sw));

            outXML = sw.toString();

        } // end of try
        catch (Exception e) {
            e.printStackTrace(System.out);
            System.out.println("Exception in addXMLElement " + e);
        }

        // System.out.println("outXML" + outXML);
        return outXML;
    }

    /**
     * Logs an Import Request to the database
     * 
     * @return Import Id for the request if successful, -1 if unsuccessful
     */
    // /__astcwz_opern_idt#(1078)
    public int recordImportRequest() {
        /* Records export Request */

        CallableStatement cstmt = null;
        Connection conn = null;
        int intOUT = -1;
        java.sql.Timestamp tStamp = null;
        String strTimeStamp = null;

        CLOB clValue = null;

        try {

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> Impex.recordImportRequeststarted .. ");

            CommonDAO cDao = new CommonDAO();

            conn = cDao.getConnection();

            cstmt = conn
                    .prepareCall("{call pkg_impexcommon.SP_CREATE_IMPREQUEST( ? , ? , ? ,? , ?, ?) } ");

            strTimeStamp = impexRequest.getReqDateTime();

            if ((strTimeStamp != null) && strTimeStamp.length() > 0) {
                tStamp = Timestamp.valueOf(strTimeStamp);
            }

            cstmt.setString(1, impexRequest.getReqBy());
            cstmt.setTimestamp(2, tStamp);

            if (!EJBUtil.isEmpty(impexRequest.getReqDetails())) {
                clValue = DBUtil.getCLOB(impexRequest.getReqDetails(), conn);

            }

            cstmt.setClob(3, clValue);
            cstmt.setString(4, impexRequest.getReqStatus());
            cstmt.setString(5, impexRequest.getSiteCode());
            cstmt.registerOutParameter(6, OracleTypes.INTEGER);

            // execute the procedure after setting the input and output
            // parameters
            cstmt.execute();

            DBLogger
                    .log2DB("debug", "MSG",
                            "IMPEX --> Impex.recordImportRequest  : procedure executed");

            // if there are output parameters, read the parameters
            intOUT = cstmt.getInt(6); // returns the expId
            impexRequest.setReqId(intOUT);

            return intOUT;

        }// end of try
        catch (Exception ex) {
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> exception in Impex.recordImportRequest  "
                            + ex.toString());
            return intOUT;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * Updates an Import Request
     * 
     * @param impId
     *            Import id
     * @param expDetails
     *            Export details (xml)
     * @param status
     *            Import status flag
     * @param expId
     *            Export Id
     * @return Success flag: -1 if unsuccessful
     */
    public static int updateImportRequest(int impId, String expDetails,
            int status, int expId) {
        /* Updated Import Status */

        CallableStatement cstmt = null;

        Connection conn = null;

        int intOUT = -1;
        CLOB clValue = null;

        try {

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> Impex.updateImportRequest");

            CommonDAO cDao = new CommonDAO();
            conn = cDao.getConnection();

            cstmt = conn
                    .prepareCall("{call pkg_impexcommon.SP_UPDATE_IMPREQUEST( ? , ? , ? ,? , ?) } ");

            clValue = DBUtil.getCLOB(expDetails, conn);

            cstmt.setInt(1, impId);
            cstmt.setInt(2, expId);
            cstmt.setInt(3, status);

            cstmt.setClob(4, clValue);

            cstmt.registerOutParameter(5, OracleTypes.INTEGER);

            // execute the statement
            cstmt.execute();

            intOUT = cstmt.getInt(5); // returns the expId

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> Impex.updateImportRequest : statement executed");

            return intOUT;
        }// end of try
        catch (Exception ex) {
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> exception in Impex.updateImportRequest  "
                            + ex.toString());
            return intOUT;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * Updates status of an Import Request
     * 
     * @param impId
     *            Import id
     * @param status
     *            Import status flag
     * @return Success flag: -1 if unsuccessful
     */
    public static int updateImportStatus(int impId, int status) {
        /* Updated Import Status */

        PreparedStatement pstmt = null;
        Connection conn = null;

        int intOUT = -1;

        try {

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> Impex.updateImportStatus");

            CommonDAO cDao = new CommonDAO();
            conn = cDao.getConnection();

            pstmt = conn
                    .prepareStatement("Update er_impreqlog set IMP_STATUS = ? Where  PK_IMPREQLOG = ? ");

            pstmt.setInt(1, status);
            pstmt.setInt(2, impId);

            // execute the statement
            intOUT = pstmt.executeUpdate();

            if (!conn.getAutoCommit()) {
                conn.commit();
            }

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> Impex.updateImportStatus : statement executed");

            return intOUT;
        }// end of try
        catch (Exception ex) {
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> exception in Impex.updateImportStatus  "
                            + ex.toString());
            return intOUT;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
            
        }

    }

    /**
     * Gets a Hashtable of Import parameters
     * 
     * @return a Hashtable of Import parameters
     */

    public Hashtable getImportParam() {
        return this.importParam;
    }

    /**
     * Sets a Hashtable of Import parameters
     * 
     * @param importParam
     *            a Hashtable of Import parameters
     */

    public void setImportParam(Hashtable importParam) {
        this.importParam = importParam;
    }

    /**
     * Alter Trigger status for an Import. (Before import)
     * 
     * @param impId
     *            Import id
     * @param action
     *            Action to be performed, E: Enable, D: Disable
     * @param Success
     *            flag, -1 if unsuccessful
     * 
     */
    public static int alterTrigger(int impId, String action) {
        /* Updated Import Status */

        CallableStatement cstmt = null;
        CallableStatement schCstmt = null;

        Connection conn = null;
        Connection schConn = null;

        int intOUT = -1;
        CLOB clValue = null;

        try {

            DBLogger.log2DB("debug", "MSG", "IMPEX --> Impex.alterTrigger");

            CommonDAO cDao = new CommonDAO();
            conn = cDao.getConnection();

            schConn = cDao.getSchConnection();

            cstmt = conn
                    .prepareCall("{call pkg_impex.SP_ALTER_TRIGGER( ? , ? , ? ) } ");
            schCstmt = schConn
                    .prepareCall("{call pkg_schimpex.SP_ALTER_TRIGGER( ? , ? , ? ) } ");

            cstmt.setInt(1, impId);
            cstmt.setString(2, action);
            cstmt.registerOutParameter(3, OracleTypes.INTEGER);

            // execute the statement
            cstmt.execute();

            intOUT = cstmt.getInt(3); // returns the expId

            schCstmt.setInt(1, impId);
            schCstmt.setString(2, action);
            schCstmt.registerOutParameter(3, OracleTypes.INTEGER);

            // execute the statement
            schCstmt.execute();

            intOUT = schCstmt.getInt(3); // returns the expId

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> Impex.alterTrigger : statement executed");

            return intOUT;
        }// end of try
        catch (Exception ex) {
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> exception in Impex.alterTrigger()  "
                            + ex.toString());
            return intOUT;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
                if (schCstmt != null)
                    schCstmt.close();

                if (conn != null)
                    conn.close();
                if (schConn != null)
                    schConn.close();
            }

            catch (Exception e) {
            }
        }

    }

    /**
     * Alter Trigger status for an Import category. (Before import)
     * 
     * @param category
     *            Import category
     * @param action
     *            Action to be performed, E: Enable, D: Disable
     * @param Success
     *            flag, -1 if unsuccessful
     * 
     */
    public static int alterTrigger(String category, String action) {

        String nameSQL = "Select ti_user, ti_name from er_triggerinfo where ti_category = ? ";

        String actionName = null;

        if (action.equals("E"))
            actionName = "ENABLE";
        else
            actionName = "DISABLE";

        String alterSQL = " ALTER TRIGGER ? " + " " + actionName;

        PreparedStatement pstmtTriggerName = null;

        PreparedStatement pstmtAlterERTrigger = null;
        PreparedStatement pstmtAlterSCHTrigger = null;

        Statement stmtER = null;
        Statement stmtSCH = null;
        Statement stmtct = null;

        Connection conn = null;
        Connection schConn = null;

        String tiName = null;
        String tiUser = null;
        int retVal = -1;
        int updateStatus = 0;

        try {

            CommonDAO cDao = new CommonDAO();

            conn = cDao.getConnection();
            schConn = cDao.getSchConnection();

            pstmtTriggerName = conn.prepareStatement(nameSQL);

            pstmtAlterERTrigger = conn.prepareStatement(alterSQL);
            pstmtAlterSCHTrigger = schConn.prepareStatement(alterSQL);

            stmtER = conn.createStatement();
            stmtSCH = schConn.createStatement();
            stmtct = conn.createStatement();

            pstmtTriggerName.setString(1, category);

            ResultSet rs = pstmtTriggerName.executeQuery();

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> Impex.alterTrigger for : category " + category);

            try {

                while (rs.next()) {

                    tiUser = rs.getString("ti_user");
                    tiName = rs.getString("ti_name");

                    if (tiUser.equalsIgnoreCase("ER")) {
                        // pstmtAlterERTrigger.setString(1,tiName);
                        // pstmtAlterERTrigger.execute();
                        DBLogger.log2DB("debug", "MSG",
                                "IMPEX --> Impex.alterTrigger alter trigger "
                                        + tiName + " " + actionName);
                        stmtER.addBatch("alter trigger " + tiName + " "
                                + actionName);

                    } else {
                        // pstmtAlterSCHTrigger.setString(1,tiName);
                        // pstmtAlterSCHTrigger.execute();
                        DBLogger.log2DB("debug", "MSG",
                                "IMPEX --> Impex.alterTrigger alter trigger "
                                        + tiName + " " + actionName);
                        stmtSCH.addBatch("alter trigger " + tiName + " "
                                + actionName);
                    }

                }

                ResultSet rs2 = stmtct
                        .executeQuery("select count(*) from user_triggers");

                while (rs2.next()) {
                    DBLogger.log2DB("debug", "MSG",
                            "IMPEX --> Impex.alterTrigger  trigger count"
                                    + rs2.getString(1) + " " + actionName);
                }

                stmtER.executeBatch();
                stmtSCH.executeBatch();

                retVal = 0;
                return retVal;
            } catch (SQLException sqlEx) {
                retVal = -1;
                DBLogger.log2DB("fatal", "Exception",
                        "SQLException in Impex.alterTrigger --> "
                                + sqlEx.toString()
                                + " Could not alter trigger " + tiName);
                return retVal;
            }

        } catch (Exception ex) {
            // Debug.println("EXCEPTION IN FETCHING FROM SITE TABLE " + ex);
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> Impex.alterTrigger:  " + ex.toString());
            return -1;
        } finally {
            try {
                if (pstmtTriggerName != null)
                    pstmtTriggerName.close();
                if (pstmtAlterSCHTrigger != null)
                    pstmtAlterSCHTrigger.close();
                if (pstmtAlterERTrigger != null)
                    pstmtAlterERTrigger.close();
                if (stmtER != null)
                    stmtER.close();
                if (stmtSCH != null)
                    stmtSCH.close();

            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
                if (schConn != null)
                    schConn.close();
            } catch (Exception e) {
            }
        }

        // end of method
    }

    /**
     * Returns a BrowersRows object with information about studies/data that is
     * ready for export to respective sponsor
     * 
     * @return a BrowserRows object with following data keys: FK_STUDY FK_MODULE
     *         MODULE_SUBTYPE PK_RS DATA_COUNT RS_INTERVAL RS_INTERVAL_UNIT
     *         SITE_CODE BACKUP_MSGFOLDER PK_REV_STUDYSPONSOR
     */
    public static BrowserRows getExpStudiesInfo() {
        int rows = 0;
        Hashtable ht = new Hashtable();

        BrowserRows br = new BrowserRows();
        Connection conn = null;
        CallableStatement cstmt = null;
        ResultSet rsOUT = null;

        String study;
        String module;
        String moduleSubtype;
        String pkRS;
        String dataCount;
        String interval;
        String intervalUnit;
        String siteCode;
        String msgFolder;
        String pk_rev_studysponsor;

        try {
            CommonDAO cDao = new CommonDAO();
            conn = cDao.getConnection();

            if (conn != null) {
                System.out
                        .println("DBUtil.getExpStudiesInfo() --> Got connection");
            }

            cstmt = conn
                    .prepareCall("{call PKG_IMPEX_REVERSE.SP_GET_EXPSTUDIES( ? ) } ");
            cstmt.registerOutParameter(1, OracleTypes.CURSOR);
            // execute the statement
            cstmt.execute();

            rsOUT = (ResultSet) cstmt.getObject(1);// returns the cursor

            if (rsOUT != null) {
                System.out
                        .println("DBUtil.getExpStudiesInfo() --> rsOUT is not null");
                br.setBColumns("FK_STUDY");
                br.setBColumns("FK_MODULE");
                br.setBColumns("MODULE_SUBTYPE");
                br.setBColumns("PK_RS");
                br.setBColumns("DATA_COUNT");
                br.setBColumns("RS_INTERVAL");
                br.setBColumns("RS_INTERVAL_UNIT");
                br.setBColumns("SITE_CODE");
                br.setBColumns("BACKUP_MSGFOLDER");
                br.setBColumns("PK_REV_STUDYSPONSOR");

                while (rsOUT.next()) {

                    study = rsOUT.getString("FK_STUDY");
                    module = rsOUT.getString("FK_MODULE");
                    moduleSubtype = rsOUT.getString("MODULE_SUBTYPE");
                    pkRS = rsOUT.getString("PK_RS");
                    dataCount = rsOUT.getString("DATA_COUNT");
                    interval = rsOUT.getString("RS_INTERVAL");
                    intervalUnit = rsOUT.getString("RS_INTERVAL_UNIT");
                    siteCode = rsOUT.getString("SITE_CODE");
                    msgFolder = rsOUT.getString("BACKUP_MSGFOLDER");
                    pk_rev_studysponsor = rsOUT
                            .getString("PK_REV_STUDYSPONSOR");

                    ArrayList ar = new ArrayList();

                    System.out.println("Study *** : " + study
                            + "dataCount *** : " + dataCount);
                    System.out.println("*************");

                    ar.add(study);
                    ar.add(module);
                    ar.add(moduleSubtype);
                    ar.add(pkRS);
                    ar.add(dataCount);
                    ar.add(interval);
                    ar.add(intervalUnit);
                    ar.add(siteCode);
                    ar.add(msgFolder);
                    ar.add(pk_rev_studysponsor);
                    rows++;
                    ht.put(new Integer(rows), ar);

                }

                br.setBValues(ht);
                br.setHasPrevious(false);
                br.setHasMore(false);
                br.setRowReturned(rows);
                br.setShowPages(1);
                br.setStartPage(1);
                br.setFirstRec(1);
                br.setLastRec(rows);

            }

            System.out.println("DBUtil.getExpStudiesInfo() --> Total Rows"
                    + rows);
            return br;

        } catch (Exception ex) {
            System.out.println("DBUtil.getExpStudiesInfo() --> "
                    + ex.toString());
            return br;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * Updates er_revmodexp_setup data for next export schedule and sets
     * rp_processedflag = 2 for exported data in er_rev_pendingdata
     * 
     * @param arStudySponsor
     *            A String Array of fk_rev_studysponsor
     * @param arModule
     *            A String Array of module sub_type
     * @param arInterval
     *            A String Array of Interval between exports
     * @param arInterval
     *            A String Array of Interval Units
     * @return Error string if there is any errror
     * 
     */
    public static String updateRevNextExportData(String[] arStudySponsor,
            String[] arModule, String[] arInterval, String[] intervalUnit) {
        Connection conn = null;
        CallableStatement cstmt = null;
        String strOUT;

        try {
            CommonDAO cDao = new CommonDAO();

            conn = cDao.getConnection();

            ArrayDescriptor adStudySponsor = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ArrayDescriptor adModule = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ArrayDescriptor adInterval = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);
            ArrayDescriptor adIntervalUnit = ArrayDescriptor.createDescriptor(
                    "ARRAY_STRING", conn);

            ARRAY oArStudySponsor = new ARRAY(adStudySponsor, conn,
                    arStudySponsor);
            ARRAY oArModule = new ARRAY(adModule, conn, arModule);
            ARRAY oArInterval = new ARRAY(adInterval, conn, arInterval);
            ARRAY oArIntervalUnit = new ARRAY(adIntervalUnit, conn,
                    intervalUnit);

            cstmt = conn
                    .prepareCall("{call PKG_IMPEX_REVERSE.SP_SET_NEXT_EXPDATA( ?,?,?,?,? ) } ");

            cstmt.setArray(1, oArStudySponsor);
            cstmt.setArray(2, oArModule);
            cstmt.setArray(3, oArInterval);
            cstmt.setArray(4, oArIntervalUnit);

            cstmt.registerOutParameter(5, java.sql.Types.VARCHAR);
            // execute the statement
            cstmt.execute();

            strOUT = cstmt.getString(5);// returns the err string if any

            return strOUT;
        } catch (Exception ex) {
            System.out.println("Impex.updateRevNextExportData --> "
                    + ex.toString());
            return ex.toString();
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * Returns Reverse Export data for an ImpexRequest object
     * 
     * @param irObj
     *            ImpexRequest object
     * @return ArrayList of Module objects
     */
    public synchronized static ArrayList getReverseExportData(ImpexRequest irObj) {
        // will work on the export details

        String expDetails;

        int expReqId = 0;
        String moduleName;

        int attrLength = 0;
        String filepath;
        int systemType;
        String logStr;

        ArrayList arrAttr = new ArrayList();
        ArrayList arrAttrValues = new ArrayList();
        ArrayList arFinal = new ArrayList();

        expReqId = irObj.getReqId();

        DBLogger.impexId = expReqId;
        DBLogger.log2DB("debug", "MSG", "IMPEX --> Starting Export........");

        Impex.updateExportStatus(expReqId, 1); // export started

        expDetails = irObj.getReqDetails();

        // systemType = EJBUtil.getSystemType();
        // parse this expDetails to get the list of modules to export

        arrAttr.add("name");
        arrAttrValues = Impex.getXMLElementAttributes(expDetails, "module",
                arrAttr);

        attrLength = arrAttrValues.size();

        for (int i = 0; i < attrLength; i++) {

            Hashtable ht = new Hashtable();
            ht = (Hashtable) arrAttrValues.get(i);

            moduleName = (String) ht.get("name");
            Module md = new Module();

            md.setName(moduleName);
            md.setType("E");
            md.readData(expReqId);

            // append the module to the final Hashtable
            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> Finished Export of moduleName:" + moduleName);

            // htFinal.put(moduleName,md);
            arFinal.add(md);

        } // end of for - for modules

        DBLogger.log2DB("debug", "MSG", "IMPEX --> Finished Export........");
        Impex.updateExportStatus(expReqId, 2);

        return arFinal;
        // end of method
    }

    public static synchronized void startReverseImport(ImpexRequest impexR) {

        Hashtable htImpData = new Hashtable();
        int importId = 0;
        int exportId = 0;
        ArrayList arData = new ArrayList();
        try {

            importId = impexR.getReqId();
            exportId = impexR.getDataExpId();

            DBLogger.impexId = exportId;
            DBLogger.siteCode = impexR.getSiteCode();

            Impex.updateImportRequest(importId, "", 1, exportId);

            DBLogger
                    .log2DB("debug", "MSG", "IMPEX --> Starting Import........");

            // iterate through the hashtable data
            htImpData = impexR.getHtData();

            arData = (ArrayList) htImpData.get("data");

            // for (Enumeration dataEnum = htImpData.keys();
            // dataEnum.hasMoreElements() ;)
            for (int i = 0; i < arData.size(); i++) {

                String moduleName = new String();

                // moduleName = (String) dataEnum.nextElement();

                Module mdImport = new Module();

                // mdImport = (Module) htImpData.get(moduleName);

                mdImport = (Module) arData.get(i);
                System.out.println("Read Message Object, got module: "
                        + mdImport.getName() + "..starting Import");

                mdImport.setType("I"); // import type module
                mdImport.setDropModuleTableFlag(Impex.getDropTableFlag());
                mdImport.saveData(importId);

                System.out
                        .println(".........Finished Importing to transitional tables");

            }

            Impex.updateImportStatus(importId, 2);

        } catch (Exception ex) {
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> exception in Impex.startReverseImport   "
                            + ex.toString());
        }

    }

    /**
     * Gets study sponsor information
     * 
     * @param expStudySponsor
     *            PK of er_rev_studysponsor
     * @return Returns a Hashtable of sponsor information data with following
     *         keys: sponsorStudy : Study Id at sponsor sponsorAccount: Sponsor
     *         account sponsorJMSFactoryPath : JMS Factory JNDI path at
     *         sponsor's application server sponsorJMSTopicPath : JMS Topic JNDI
     *         path at sponsor's application server siteCode : Site Code of the
     *         A2A Site
     */
    public static Hashtable getStudySponsorInformation(String expStudySponsor) {

        Hashtable info = new Hashtable();

        String spSQL = "select FK_SPONSOR_STUDY ,FK_SPONSOR_ACCOUNT , SPONSOR_JMSFACTORYPATH, SPONSOR_JMSTOPIC ,SITE_CODE from er_rev_studysponsor where PK_REV_STUDYSPONSOR = ? ";

        PreparedStatement pstmtSp = null;
        Connection conn = null;

        String sponsorStudy = null;
        String sponsorAccount = null;
        String sponsorJMSFactoryPath = null;
        String sponsorJMSTopicPath = null;
        String siteCode = null;

        try {
            // conn = com.velos.eres.service.util.EJBUtil.getConnection();

            CommonDAO cDao = new CommonDAO();
            conn = cDao.getConnection();

            pstmtSp = conn.prepareStatement(spSQL);
            pstmtSp.setString(1, expStudySponsor);
            ResultSet rs = pstmtSp.executeQuery();

            while (rs.next()) {

                sponsorStudy = rs.getString("FK_SPONSOR_STUDY");
                sponsorAccount = rs.getString("FK_SPONSOR_ACCOUNT");
                sponsorJMSFactoryPath = rs.getString("SPONSOR_JMSFACTORYPATH");
                sponsorJMSTopicPath = rs.getString("SPONSOR_JMSTOPIC");
                siteCode = rs.getString("SITE_CODE");

                info.put("sponsorStudy", sponsorStudy);
                info.put("sponsorAccount", sponsorAccount);
                info.put("sponsorJMSFactoryPath", sponsorJMSFactoryPath);
                info.put("sponsorJMSTopicPath", sponsorJMSTopicPath);
                info.put("siteCode", siteCode);

            }

            return info;

        } catch (Exception ex) {
            System.out.println("Impex.getStudySponsorInformation --> "
                    + ex.toString());
            return info;
        } finally {
            try {
                if (pstmtSp != null)
                    pstmtSp.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        // end of method
    }

    /**
     * Transfers the exported data for an exportId to ER_REV_DATASENT
     * 
     * @param expId
     *            the export Id
     * @param studySponsorPk
     *            PK of er_rev_studysponsor
     * @return Error String in case of any error
     */
    public static String backupRevExportedData(int expId, int studySponsorPk) {
        Connection conn = null;
        CallableStatement cstmt = null;
        String strOUT;

        try {
            CommonDAO cDao = new CommonDAO();
            conn = cDao.getConnection();

            cstmt = conn
                    .prepareCall("{call PKG_IMPEX_REVERSE.SP_BACKUP_EXPDATA( ?,?,? ) } ");

            cstmt.setInt(1, expId);
            cstmt.setInt(2, studySponsorPk);

            cstmt.registerOutParameter(3, java.sql.Types.VARCHAR);
            // execute the statement
            cstmt.execute();

            strOUT = cstmt.getString(3);// returns the err string if any

            return strOUT;
        } catch (Exception ex) {
            System.out.println("Impex.backupRevExportedData --> "
                    + ex.toString());
            return ex.toString();
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /**
     * Resets the object attributes to null so that they can be garbage
     * collected
     */
    public void resetMe() {
        if (impexRequest != null) {
            impexRequest.resetMe();
        }
        impexRequest = null;
        requestType = null;
        importPath = null;
        dropTableFlag = null;

        // clean hashtable
        if (importParam != null) {
            for (Enumeration e = importParam.elements(); e.hasMoreElements();) {
                importParam.remove(e.nextElement());
            }
        }
        importParam = null;
        if (exportData != null) {

            for (int i = 0; i < exportData.size(); i++) {
                exportData.remove(i);
            }
        }

    }


    /**
     * Transfers the imported data for an importID to ER_REV_RECVD
     * 
     * @param impId
     *            the Import Id
     * @return Error String in case of any error
     */
    public static String notifyUsersAfterImport(int impId) {
        Connection conn = null;
        CallableStatement cstmt = null;
        String strOUT;

        try {
            CommonDAO cDao = new CommonDAO();
            conn = cDao.getConnection();

            cstmt = conn
                    .prepareCall("{call PKG_IMPEX_REVERSE.sp_notifySponsor( ?,? ) } ");

            cstmt.setInt(1, impId);
            cstmt.registerOutParameter(2, java.sql.Types.VARCHAR);

            cstmt.execute();

            strOUT = cstmt.getString(2);// returns the err string if any

            return strOUT;
        } catch (Exception ex) {
            System.out.println("Impex.notifyUsersAfterImport --> "
                    + ex.toString());
            return ex.toString();
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }
    
    ///////////////////// function to get Reverse A2A status
    /**
     * Function to return Reverse A2A status, returns 0 if Reverse A2A is not running, returns 1 if Reverse A2A is running
     * 
     * @return returns 0 if Reverse A2A is not running, returns 1 if Reverse A2A is running
     * 
     */
    public static int f_get_ReverseA2AStatus() {

        String sql = "Select pkg_impex_reverse.f_get_ReverseA2AStatus() as stat  from dual ";

        
        PreparedStatement pstmtSQL= null;

        Connection conn = null;
        
        int retVal = -1;
        
        try {
            conn = CommonDAO.getConnection();
            pstmtSQL = conn.prepareStatement(sql);
            ResultSet rs = pstmtSQL.executeQuery();

            DBLogger.log2DB("debug", "MSG",  "IMPEX --> Impex.f_get_ReverseA2AStatus() ");
            try {

                while (rs.next()) {
                	retVal = rs.getInt("stat");
                }
                    
            } catch (SQLException sqlEx) {
                retVal = -1;
                DBLogger.log2DB("fatal", "Exception",
                        "SQLException in Impex.f_get_ReverseA2AStatus() --> "
                                + sqlEx.toString());
                
            }
            return retVal;

        } catch (Exception ex) {
            // Debug.println("EXCEPTION IN FETCHING FROM SITE TABLE " + ex);
            DBLogger.log2DB("fatal", "Exception", "IMPEX --> Impex.Impex.f_get_ReverseA2AStatus:  " + ex.toString());
            return -1;
        } finally {
            try {
                if (pstmtSQL != null)
                	pstmtSQL.close();
                 
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
                
            } catch (Exception e) {
            }
        }

        // end of method
    }

    
    
    /**
     * Sends A2A Status Email
     *      * 
     * @return Error String in case of any error
     */
    public static String sendA2AstatusEmail() {
        Connection conn = null;
        CallableStatement cstmt = null;
        String strOUT;

        try {
            
            conn = CommonDAO.getConnection();

            cstmt = conn.prepareCall("{call PKG_IMPEX_REVERSE.sp_notifyA2AStatus( ? ) } ");

            cstmt.registerOutParameter(1, java.sql.Types.VARCHAR);
            // execute the statement
            cstmt.execute();

            strOUT = cstmt.getString(1);// returns the err string if any

            return strOUT;
        } catch (Exception ex) {
            System.out.println("Impex.sendA2AstatusEmail --> "
                    + ex.toString());
            return ex.toString();
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    /** 09/03/08 Sonia Abrol
     * Sends A2A Connection Failure Email
     *      * 
     * @return Error String in case of any error
     */
    public static String sendA2AConnFailEmail(String connAdd) {
        Connection conn = null;
        CallableStatement cstmt = null;
        String strOUT;

        try {
            
            conn = CommonDAO.getConnection();

            cstmt = conn.prepareCall("{call PKG_IMPEX_REVERSE.sp_notifyA2AConnFail( ?,? ) } ");

            cstmt.setString(1,connAdd);
            
            cstmt.registerOutParameter(2, java.sql.Types.VARCHAR);
            // execute the statement
            cstmt.execute();

            strOUT = cstmt.getString(2);// returns the err string if any

            return strOUT;
        } catch (Exception ex) {
            System.out.println("Impex.sendA2AConnFailEmail --> "
                    + ex.toString());
            return ex.toString();
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    // /////////////////////////////////////////
    // end of class

    
}
