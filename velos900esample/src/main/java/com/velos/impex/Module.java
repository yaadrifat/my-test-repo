/**
 * __astcwz_cmpnt_det: Author: Sonia Sahni Date and Time Created: Thu Jan 29
 * 10:58:55 IST 2004 Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * STARTING WITH ///__astcwz_ and /**__astcwz_
 */
// /__astcwz_model_idt#(0)
// /__astcwz_maxim_idt#(2011)
// /__astcwz_packg_idt#(1046,1047,1048~,,)
package com.velos.impex;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.service.util.EJBUtil;

// /__astcwz_class_idt#(1018!n)
public class Module implements java.io.Serializable {
    // /__astcwz_attrb_idt#(1020)
    private String name;

    private String type;

    private String dropModuleTableFlag;

    // /__astcwz_attrb_idt#(1022!0..n)
    private ArrayList alModuleData;

    public Module() {
        alModuleData = new ArrayList();
    }

    // /__astcwz_opern_idt#(1025)
    public void readData(int expId) {

        try {

            // make this method synchronized for multi-threaded mode

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> Starting Export........", this.name);

            int procedureCount = 0;
            ArrayList arData = null;

            // read data for this module from the database

            ArrayList arModuleProcedures = new ArrayList();

            // get Procedures to be executed for the module.

            arModuleProcedures = ModuleProcedure.getModuleProcedures(this.name,
                    this.type);

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> Got Export Procedures for", this.name);

            ArrayList inParam = new ArrayList();
            inParam.add(String.valueOf(expId)); // add export id

            procedureCount = arModuleProcedures.size();

            DBLogger.log2DB("debug", "MSG", "IMPEX --> Procedure Count: "
                    + procedureCount, this.name);

            for (int i = 0; i < procedureCount; i++) {
                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> Executing procedure : " + i, this.name);

                arData = new ArrayList();
                ModuleProcedure procObj = (ModuleProcedure) arModuleProcedures
                        .get(i);
                arData = procObj.executeProcedure(inParam); // got moduleData

                // add it to Module's alModuleData

                append2AlModuleData(arData);

                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> Got Data Objects in Module : "
                                + alModuleData.size(), this.name);

            }

        } catch (Exception e) {
        }

    }

    // /__astcwz_opern_idt#(1026)
    public ArrayList getAlModuleData() {
        return alModuleData;

    }

    // /__astcwz_opern_idt#(1031|1032)
    public Module restoreModuleData(String name) {
        return null;
    }

    // /__astcwz_opern_idt#(1034)
    public static String persistModuleData(Module mdObj, String filePath) {
        // serialize the data class for testing
        int count = 0;
        int sType = 0;

        sType = EJBUtil.getSystemType();

        ArrayList alMod = new ArrayList();

        alMod = mdObj.getAlModuleData();

        if (alMod != null) {
            DBLogger
                    .log2DB(
                            "debug",
                            "MSG",
                            "IMPEX --> Module.persistModuleData Total Module Data Objects Persisted: Array is not null");
            count = alMod.size();
        }

        DBLogger.log2DB("debug", "MSG",
                "IMPEX --> Module.persistModuleData Total Module Data Objects Persisted: "
                        + count);

        // String filePath = "";
        // filePath = Configuration.ERES_HOME;

        filePath = Impex.getFilePath(filePath, mdObj.getName() + ".vel");

        DBLogger.log2DB("debug", "MSG",
                "IMPEX --> Module.persistModuleData filePath : " + filePath);

        FileOutputStream fos = null;
        ObjectOutputStream out1 = null;
        try {
            fos = new FileOutputStream(filePath);
            out1 = new ObjectOutputStream(fos);
            out1.writeObject(mdObj);
            out1.close();
        } catch (Exception ex) {
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> exception in Module.persistModuleData(): "
                            + ex.toString());
        }

        // end of serialize

        return "Persisted";
    }

    // /__astcwz_opern_idt#(1045)
    public void prepareToImportModuleData() {

    }

    // /__astcwz_opern_idt#(1104)
    public void createModuleData() {

    }

    // /__astcwz_opern_idt#(1106)
    /**
     * Imports module data to eResearch database
     * 
     * @param impId
     *            Import ID ti identify the import request
     * 
     */
    public void saveData(int impId) {

        /** Saves the data held in alModuleData to database */
        ArrayList arModuleProcedures = new ArrayList();
        ArrayList arData = null;
        int procOutput = 0;
        int procedureCount = 0;
        String dropFlag = "0";

        ArrayList arERCleanup = new ArrayList();
        ArrayList arSCHCleanup = new ArrayList();

        String tableLocation = "";

        Connection conn = null;
        CommonDAO cDao = new CommonDAO();
        int tableCount = 0;

        try {

            DBLogger.log2DB("debug", "MSG", "IMPEX --> Module.saveData () ",
                    this.name);

            int modCount = 0;

            modCount = alModuleData.size();

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> Module.saveData () count : " + modCount,
                    this.name);

            // save 2 temporary tables

            // for table name parameter

            for (int count = 0; count < modCount; count++) {
                ModuleData md = new ModuleData();

                String moduleDataTableName = "imp" + this.name + count;

                md = (ModuleData) alModuleData.get(count);

                md.importModuleDataIntoTable(moduleDataTableName);

                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> Module.saveData () count : " + modCount
                                + " Module Imported", this.name);

                // get the table location
                tableLocation = (md.getModuleDataDefinition())
                        .getTableLocation();
                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> Module.saveData () tableLocation : "
                                + tableLocation, this.name);

                if (tableLocation.equalsIgnoreCase("ER")) {
                    arERCleanup.add("DROP TABLE " + moduleDataTableName);

                } else {
                    arSCHCleanup.add("DROP TABLE " + moduleDataTableName);
                }

            }

            // execute import procedures

            // get Procedures to be executed for the module.

            arModuleProcedures = ModuleProcedure.getModuleProcedures(this.name,
                    this.type);

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> Got Import Procedures for", this.name);

            ArrayList inParam = new ArrayList();
            inParam.add(String.valueOf(impId)); // add export id

            procedureCount = arModuleProcedures.size();

            DBLogger.log2DB("debug", "MSG", "IMPEX --> Procedure Count: "
                    + procedureCount, this.name);

            for (int i = 0; i < procedureCount; i++) {
                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> Executing procedure : " + i, this.name);

                arData = new ArrayList();
                ModuleProcedure procObj = (ModuleProcedure) arModuleProcedures
                        .get(i);
                arData = procObj.executeProcedure(inParam); // executed
                // procedure

                if (arData != null && arData.size() > 0) {
                    procOutput = ((Integer) arData.get(0)).intValue();
                }

                if (procOutput == 0) {
                    DBLogger.log2DB("debug", "MSG",
                            "IMPEX --> Import Procedure Executed: "
                                    + procObj.getProcName(), this.name);
                } else {
                    DBLogger.log2DB("debug", "MSG",
                            "IMPEX --> Error in Executing Import Procedure: "
                                    + procObj.getProcName(), this.name);
                }

            }

            // check for dropModuleTableFlag, if flag = 1, drop temporary tables

            dropFlag = getDropModuleTableFlag();

            if (EJBUtil.isEmpty(dropFlag))
                dropFlag = "0";

            if (dropFlag.equals("1")) {
                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> About to drop temporary tables....... ");
                tableCount = arERCleanup.size();
                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> Number of ER temporary tables....... "
                                + tableCount);
                if (tableCount > 0) {

                    conn = cDao.getConnection();
                    DBUtil.executeBatch(conn, arERCleanup);
                    conn.close();
                }

                tableCount = arSCHCleanup.size();
                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> Number of SCH temporary tables....... "
                                + tableCount);
                if (tableCount > 0) {
                    conn = cDao.getSchConnection();
                    // execute cleanup
                    DBUtil.executeBatch(conn, arSCHCleanup);
                    conn.close();

                }

                // /////////
            }

            // ///////////
        } catch (Exception ex) {
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> exception in Module.saveData(): "
                            + ex.toString());
            DBLogger.log2DB("fatal", "Exception",
                    "IMPEX --> exception Import process, import stoppped "
                            );
            
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException ex) {
            }
        }

        // end of method

    }

    // /__astcwz_opern_idt#()
    public String getName() {
        return name;
    }

    // /__astcwz_opern_idt#()
    public void setName(String aname) {
        name = aname;
    }

    // /__astcwz_opern_idt#()
    public void setAlModuleData(ModuleData moduleData) {
        alModuleData.add(moduleData);
    }

    public void setAlModuleData(ArrayList aModuleData) {
        alModuleData = aModuleData;
    }

    public void append2AlModuleData(ArrayList aMData) {
        int modCount = 0;

        modCount = aMData.size();

        // DBLogger.log2DB ("debug", "MSG", "IMPEX --> append2AlModuleData count
        // : " + modCount, this.name);

        for (int count = 0; count < modCount; count++) {
            ModuleData md = new ModuleData();
            md = (ModuleData) aMData.get(count);

            // DBLogger.log2DB ("debug", "MSG", "IMPEX --> append2AlModuleData
            // md : " + md, this.name);

            alModuleData.add(md);
        }

        DBLogger.log2DB("debug", "MSG",
                "IMPEX --> append2AlModuleData alModuleData count : "
                        + alModuleData.size(), this.name);

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * Returns the value of dropModuleTableFlag.
     */
    public String getDropModuleTableFlag() {
        return dropModuleTableFlag;
    }

    /**
     * Sets the value of dropModuleTableFlag.
     * 
     * @param dropModuleTableFlag
     *            The value to assign dropModuleTableFlag.
     */
    public void setDropModuleTableFlag(String dropModuleTableFlag) {
        this.dropModuleTableFlag = dropModuleTableFlag;
    }

    public static Hashtable getModuleListing(String moduleCategory,
            String moduleType) {

        Hashtable htModuleListing = new Hashtable();

        ArrayList arModuleSubtype = new ArrayList();
        ArrayList arModuleName = new ArrayList();

        String moduleSQL = "Select module_sequence, module_subtype, module_name from er_modules where trim(module_type) = ? and module_category = ? and module_status = 1 order by module_sequence ";

        PreparedStatement pstmtModule = null;
        Connection conn = null;

        String moduleName;
        String moduleSubType;

        try {
            // conn = com.velos.eres.service.util.EJBUtil.getConnection();

            CommonDAO cDao = new CommonDAO();
            conn = cDao.getConnection();

            pstmtModule = conn.prepareStatement(moduleSQL);

            pstmtModule.setString(1, moduleType);
            pstmtModule.setString(2, moduleCategory);

            ResultSet rs = pstmtModule.executeQuery();

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> Module.getModuleListing for : moduleCategory "
                            + moduleCategory + " and moduleType " + moduleType);

            while (rs.next()) {

                moduleSubType = rs.getString("module_subtype");
                moduleName = rs.getString("module_name");

                arModuleSubtype.add(moduleSubType);
                arModuleName.add(moduleName);

                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> Module.getModuleListing moduleSubType "
                                + moduleSubType);
                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> Module.getModuleListing moduleName "
                                + moduleName);

            }

            htModuleListing.put("moduleSubtype", arModuleSubtype);
            htModuleListing.put("moduleName", arModuleName);

            return htModuleListing;

        } catch (Exception ex) {
            // Debug.println("EXCEPTION IN FETCHING FROM SITE TABLE " + ex);
            DBLogger.log2DB("fatal", "Exception",
                    "Module.getModuleListing --> " + ex.toString());
            return htModuleListing;
        } finally {
            try {
                if (pstmtModule != null)
                    pstmtModule.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        // end of method
    }

    /**
     * The method gets List of ModuleProcedure Objects for the given module.
     * Parameters - module : moduleSubType, a given code to the module
     * procedureType : Type of procedure(s) required. Possible values - I -
     * Import; E - Export Returns - ArrayList - The ModuleProcedure objects are
     * returned in an ArrayList
     */

    /*
     * public ArrayList getModuleProcedures(String module, String procedureType) {
     * DBLogger.log2DB ("debug", "MSG", "IMPEX --> Getting Procedure
     * Information", this.name);
     * 
     * PreparedStatement pstmtModuleProc = null; PreparedStatement
     * pstmtProcParam = null;
     * 
     * Connection conn = null; String procName; String procDBUser; int
     * procSequence = 0;
     * 
     * String procParamType ; String procParamDataType ; Integer
     * procParamSequence ; int inParamCount = 0; int outParamCount = 0;
     * 
     * int procId = 0;
     * 
     * ArrayList alModuleProcs = new ArrayList(); // for list of procedures
     * 
     * StringBuffer sbModuleProcSQL = new StringBuffer(); StringBuffer
     * sbProcParamSQL = new StringBuffer();
     * 
     * sbModuleProcSQL.append("Select pk_moduleproc,
     * proc_name,proc_user,proc_sequence from er_moduleproc, er_modules ");
     * sbModuleProcSQL.append(" where module_subtype = ? and fk_module =
     * pk_module and"); sbModuleProcSQL.append(" proc_type = ? order by
     * proc_sequence ");
     * 
     * 
     * //for list of procedure parameters
     * 
     * sbProcParamSQL.append(" Select procparam_datatype, procparam_type,
     * procparam_seq from er_procparam "); sbProcParamSQL.append(" Where
     * fk_moduleproc = ? order by procparam_seq " );
     * 
     * try { conn = EJBUtil.getConnection(); pstmtModuleProc =
     * conn.prepareStatement(sbModuleProcSQL.toString());
     * 
     * pstmtModuleProc.setString(1,module);
     * pstmtModuleProc.setString(2,procedureType);
     * 
     * ResultSet rsProc = pstmtModuleProc.executeQuery();
     * 
     * while (rsProc.next()) {
     * 
     * ModuleProcedure mProcedure = new ModuleProcedure();
     * 
     * procId = rsProc.getInt("pk_moduleproc"); procName =
     * rsProc.getString("proc_name"); procDBUser =
     * rsProc.getString("proc_user"); procSequence =
     * rsProc.getInt("proc_sequence");
     * 
     * DBLogger.log2DB ("debug", "MSG", "IMPEX --> Got Procedure Information: " +
     * procName, this.name);
     * 
     * mProcedure.setProcName(procName); mProcedure.setProcDbUser(procDBUser);
     * mProcedure.setProcSequence(procSequence);
     * mProcedure.setProcType(procedureType);
     * 
     * //get procedure's parameters
     * 
     * pstmtProcParam = conn.prepareStatement(sbProcParamSQL.toString());
     * pstmtProcParam.setInt(1,procId);
     * 
     * ResultSet rsProcParam = pstmtProcParam.executeQuery(); ProcedureParam
     * pParam = new ProcedureParam();
     * 
     * while (rsProcParam.next()) {
     * 
     * procParamDataType = rsProcParam.getString("procparam_datatype");
     * procParamType = rsProcParam.getString("procparam_type");
     * procParamSequence = new Integer(rsProcParam.getInt("procparam_seq"));
     * 
     * pParam.setParamDataType(procParamDataType);
     * pParam.setParamType(procParamType);
     * pParam.setParamSequence(procParamSequence);
     * 
     * if (procParamType.equalsIgnoreCase("IN")) { inParamCount++; } else {
     * outParamCount++; } }
     * 
     * pParam.setOutParamCount(outParamCount);
     * pParam.setInParamCount(inParamCount);
     * 
     * DBLogger.log2DB ("debug", "MSG", "IMPEX --> Got Procedure Information
     * for: " + procName + "IN Parameters:" + inParamCount + ", OUT Parameters:" +
     * outParamCount, this.name);
     * 
     * mProcedure.setProcedureParam(pParam); alModuleProcs.add(mProcedure); }
     * return alModuleProcs;
     * 
     * }catch (Exception ex) { //Debug.println("EXCEPTION IN FETCHING FROM SITE
     * TABLE " + ex); DBLogger.log2DB ("fatal", "Exception", "Exception in IMPEX
     * --> " + ex.toString(),this.name); return alModuleProcs; }finally{ try{ if
     * (pstmtModuleProc != null) pstmtModuleProc.close(); if (pstmtProcParam !=
     * null) pstmtProcParam.close();
     * 
     * }catch (Exception e) { } try { if (conn!=null) conn.close(); }catch
     * (Exception e) {} }
     * 
     *  // end of Method }
     */

}
