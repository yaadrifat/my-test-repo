/**
 * __astcwz_cmpnt_det: Author: Konesa CodeWizard Date and Time Created: Thu Jan
 * 29 10:41:50 IST 2004 Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * STARTING WITH ///__astcwz_ and /**__astcwz_
 */
// /__astcwz_model_idt#(0)
// /__astcwz_maxim_idt#(2011)
// /__astcwz_packg_idt#(1046,1047,1048~,,)
package com.velos.impex;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;

import com.velos.eres.business.common.CommonDAO;

// /__astcwz_class_idt#(1021!n)
public class ModuleData implements java.io.Serializable {

    // /__astcwz_attrb_idt#(1086)
    private ArrayList moduleDataValues;

    // /__astcwz_opern_idt#(1083)
    private DataDefinition moduleDataDefinition;

    public ModuleData() {
        moduleDataValues = new ArrayList();
    }

    public DataDefinition getModuleDataDefinition() {
        return moduleDataDefinition;
    }

    public void setModuleDataDefinition(DataDefinition dataDefinition) {
        moduleDataDefinition = dataDefinition;
    }

    // /__astcwz_opern_idt#(1084)
    public ArrayList getModuleDataValues() {
        return moduleDataValues;
    }

    // /__astcwz_opern_idt#(1087)
    public void setModuleDataValues(ArrayList mDataValues) {
        moduleDataValues = mDataValues;

    }

    /** Creates and Returns a ModuleData object */
    public static ModuleData getModuleDataObject(ResultSet rset,
            String modDbUser) {
        try {
            ModuleData moduleDataObj = new ModuleData();

            ArrayList resultArrayList = null;
            ArrayList dataTypeArrayList = null;
            DataDefinition dataDef = null;

            ArrayList rowSet = new ArrayList();
            int rows = 0;

            if (rset != null) {
                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> getting ModuleData....");

                ResultSetMetaData metadataLocal = null;
                int nRowCount = 0;

                metadataLocal = rset.getMetaData();
                if (metadataLocal != null) {
                    int nColCount = metadataLocal.getColumnCount();

                    if (nColCount > 0) {
                        resultArrayList = new ArrayList();

                        dataDef = DataDefinition
                                .getDataDefinition(metadataLocal);
                        dataDef.setTableLocation(modDbUser);

                        DBLogger.log2DB("debug", "MSG",
                                "IMPEX --> got ModuleData DataDefinition...."
                                        + dataDef.getColumnCount());

                        ArrayList rowArrayList = null;

                        /*
                         * rowArrayList = new ArrayList(); for(int j=0;j<nColCount;j++) {
                         * rowArrayList.add(metadataLocal.getColumnName(j+1));
                         * dataTypeArrayList.add(metadataLocal.getColumnTypeName(j+1)); }
                         * resultArrayList.add(rowArrayList); //1st Obj -
                         * ArrayList of column names
                         * resultArrayList.add(dataTypeArrayList); //2nd Obj -
                         * ArrayList of column data types
                         */

                        // resultArrayList.add(dataDef); //1st Obj, add
                        // DataDefinition Object for the resultset
                        moduleDataObj.setModuleDataDefinition(dataDef);

                        Object colValue = new Object();

                        while (rset.next()) // iterate through all records
                        {
                            rowArrayList = new ArrayList();
                            for (int j = 0; j < nColCount; j++) {
                                String dataType = metadataLocal
                                        .getColumnTypeName(j + 1);
                                if (dataType.equalsIgnoreCase("CLOB")) {
                                    Clob clob_Data = rset.getClob(j + 1);
                                    if (clob_Data == null)
                                        colValue = "";
                                    else
                                        colValue = DBUtil
                                                .convertClobToString(clob_Data);
                                } else if (dataType.equalsIgnoreCase("BLOB")) {
                                    oracle.sql.BLOB blob_Data = (oracle.sql.BLOB) rset
                                            .getBlob(j + 1);
                                    if (blob_Data != null) {
                                        byte[] blobbytes = DBUtil
                                                .convertBlobToByteArray(blob_Data);
                                        if (blobbytes != null)
                                            colValue = blobbytes;
                                    } else {
                                        colValue = null;
                                    }
                                } else {
                                    colValue = rset.getString(j + 1);
                                }
                                if (rset.wasNull()) {
                                    if (dataType == "NUMBER") {
                                        colValue = "NULL";
                                    } else {
                                        colValue = "";
                                    }
                                }

                                if (colValue == null)
                                    colValue = "";
                                rowArrayList.add(colValue);
                            } // end of for for one complete record

                            rows++;

                            // Add the record to ArrayList
                            rowSet.add(rowArrayList);
                        } // end if while for resultset

                        // add data hashmap to the resultArrayList

                        moduleDataObj.setModuleDataValues(rowSet);
                        DBLogger.log2DB("debug", "MSG",
                                "IMPEX --> got ModuleData Data....Total Rows:"
                                        + rows);

                        // resultArrayList.add(rowSet); //2nd Obj, add hashmap
                        // of data rows

                    }

                    return moduleDataObj;
                } else {
                    DBLogger.log2DB("debug", "MSG",
                            "IMPEX --> metadataLocal is null for ResultSet");
                    System.out.println("metadataLocal is null for ResultSet");
                    return null;
                }
            } else // for null resultset
            {
                DBLogger.log2DB("debug", "MSG",
                        "IMPEX --> ResultSet is null for ResultSet");
                System.out.println("ResultSet is null for ResultSet");
                return null;
            }

        }

        catch (Exception e) {
            DBLogger.log2DB("fatal", "MSG",
                    "IMPEX --> Exception in ModuleData.getModuleDataObject "
                            + e.toString());
            return null;
            // e.printStackTrace();
        } finally {
            try {
                if (rset != null)
                    rset.close();
            } catch (Exception e) {
            }

        }
        // end of method
    }

    public void importModuleDataIntoTable(String tableName) {
        String tableDDL;
        int ddlSuccess = -1;

        String insSQL = "";
        Connection conn = null;

        int rowsAffected = 0;
        ArrayList arPlaceHolderDatatypes = new ArrayList();

        try {

            conn = getConnectionForImport();

            // set the tablename on DataDefinition Object

            moduleDataDefinition.setTableName(tableName);

            // get DDL for ModuleData

            tableDDL = moduleDataDefinition.getCreateDDL();

            // execute the DDL and create table

            ddlSuccess = DBUtil.createDBObject(conn, "table", tableName,
                    tableDDL);

            // if the table is successfully created, insert data

            insSQL = moduleDataDefinition.getDynamicInsertSQL();
            arPlaceHolderDatatypes = moduleDataDefinition.getDataTypes();

            rowsAffected = DBUtil.executeDynamicSQL(conn, insSQL,
                    arPlaceHolderDatatypes, moduleDataValues);

            DBLogger.log2DB("debug", "MSG",
                    "IMPEX --> ModuleData.importModuleDataIntoTable() : Rows Affected: "
                            + rowsAffected);

        } catch (Exception e) {
            DBLogger.log2DB("fatal", "MSG",
                    "IMPEX --> Exception in ModuleData.importModuleDataIntoTable() "
                            + e.toString());
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }

    public Connection getConnectionForImport() {
        /* Return a connection that uses Oracle drivers */

        Connection conn = null;
        CommonDAO cDao = new CommonDAO();

        String dbUser = null;
        dbUser = moduleDataDefinition.getTableLocation();

        if (dbUser != null) {
            if (dbUser.equalsIgnoreCase("SCH")) {
                conn = cDao.getSchConnection();
            } else {
                conn = cDao.getConnection();
            }
        } else {
            conn = cDao.getConnection();

        }

        return conn;

    }

}
