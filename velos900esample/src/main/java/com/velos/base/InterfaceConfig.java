package com.velos.base;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.jconfig.Configuration;
import org.jconfig.ConfigurationManager;

import com.velos.eres.service.util.StringUtil;

/**
 * This class is container for all the configuration specified for the
 * interfaces. <code>InterfaceConfig</code> acts as wrapper around xml
 * mappings defined in the files This class layers on
 * <code>org.jconfig.Configuration</code>. Configurations are loaded with
 * 
 * @see LoadProperties()
 * @author Vishal Abrol
 * @version 1.0
 */
/**
 * @author vishal
 *
 */
public class InterfaceConfig {

    private static Logger logger = Logger.getLogger("FileObserver");

    boolean load;

    Configuration config;

    Configuration mapping;

    private Parser parser;

    private String header;

    private String encoding;

    private String enclosed;

    private String unprocessedPath;

    private String badPath;

    private String processedPath;

    private String primary;

    private String dlm;

    private String ignoreFlag;

    private String fileName;

    private String filePath;

    private String[] colList;

    // This Array will keep partial list of columns to pass to Appication
    // Interface to Process.
    // This would be useful for columns where [VELPK] is defined in case of
    // updates
    private ArrayList partColList;

    // This will capture if [VELPK]
    private boolean VelPkFlag;
    
    //PREValidation of files
    
    //prevalidation enable/disable flag
    private String preValidate=null;
    
    //Should the Messages be counted
    private String enablemsgcount=null;
    
    //Limit of records count after which system should stop counting.
    //e.g. if set tp 50, system will stop couting at 50 and show the total count as 50+ 
    private int maxCounter=-1;
    
    //count of messages that should be validated.If not found,this validation would be disabled.
    private int validateMsgCount=0;
    
    private int mappingCount=0;
    

    private String[] dbColList;

    private String table;

    private String where;

    private StringBuffer insertSql;

    private StringBuffer updateSql;

    private StringBuffer selectSql;

    
   
    // Constructor to initialize variables.
    public InterfaceConfig() {
        logger.info("Initializing Interface Configuration.. ");
        load = false;
        config = null;
        parser = null;
        header = "";
        encoding = "";
        enclosed = "";
        dlm = "";
        ignoreFlag = "";
        fileName = "";
        filePath = "";
        unprocessedPath = "";
        badPath = "";
        processedPath = "";
        primary = "";
        table = "";
        where = "";
        insertSql = new StringBuffer();
        updateSql = new StringBuffer();
        selectSql = new StringBuffer();
        partColList = new ArrayList();
        VelPkFlag = false;
        preValidate="N";
        enablemsgcount="N";
        maxCounter=0;
        validateMsgCount=0;
        mappingCount=0;
        
    }

    // Start setGetMethod

    /**
     * Returns the parser retrieved.
     * 
     * @return Parser
     */
    public Parser getParser() {

        return parser;
    }

    /**
     * Sets the value of parser.
     * 
     * @param parser
     *            The value to assign to parser.
     */
    public void setParser(Parser parser) {
        this.parser = parser;
    }

    /**
     * Returns the value of header.
     * 
     * @return Header if data has a header(Y/N)
     */
    public String getHeader() {

        return header;
    }

    /**
     * Sets the value of header.
     * 
     * @param header
     *            The value to assign header.
     */
    public void setHeader(String header) {
        this.header = header;
    }

    /**
     * Returns the value of encoding
     * 
     * @return encoding i.e. delimted,fixed,XML
     */
    public String getEncoding() {

        return encoding;
    }

    /**
     * Sets the value of encoding.
     * 
     * @param encoding
     *            encoding i.e. delimted,fixed,XML
     */
    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    /**
     * Returns the value of delimiter for the data.
     * 
     * @return delimiter for the delimited data.
     */
    public String getDlm() {

        return dlm;
    }

    /**
     * Sets the value of dlm.
     * 
     * @param dlm
     *            The value to assign dlm.
     */
    public void setDlm(String dlm) {
        this.dlm = dlm;
    }

    /**
     * Returns the value of fileName.
     * 
     * @return File
     */

    /*
     * public String getFileName() { return fileName; }
     */

    /**
     * Sets the value of fileName.
     * 
     * @param fileName
     *            The value to assign fileName.
     */

    /*
     * blic void setFileName(String fileName) { this.fileName = fileName; }
     */

    /**
     * Returns the value of filePath.
     */

    /*
     * public String getFilePath() { return filePath; }
     */

    /**
     * Sets the value of filePath.
     * 
     * @param filePath
     *            The value to assign filePath.
     */

    /*
     * public void setFilePath(String filePath) { this.filePath = filePath; }
     */

    /**
     * Returns the value of colList.
     * 
     * @return <code>Array</code> of data columns specified.
     */
    public String[] getColList() {

        return colList;
    }

    /**
     * Sets the value of colList.
     * 
     * @param colList
     *            The value to assign colList.
     */
    public void setColList(String[] colList) {
        this.colList = colList;
    }

    /**
     * Returns the value of partColList.
     * 
     * @return <code>Array</code> of data columns specified.
     */
    public String[] getPartColList() {

        Object[] objArray = partColList.toArray();
        String[] strArray = new String[objArray.length];
        System.arraycopy(objArray, 0, strArray, 0, strArray.length);
        return strArray;
    }

    /**
     * Returns the value of dbColList.
     * 
     * @return <code>Array</code> of DB column specified.
     */
    public String[] getDbColList() {

        return dbColList;
    }

    /**
     * Sets the value of dbColList.
     * 
     * @param dbColList
     *            The value to assign dbColList.
     */
    public void setDbColList(String[] dbColList) {
        this.dbColList = dbColList;
    }

    /**
     * Returns the value of table.
     * 
     * @return database table
     */
    public String getTable() {

        return table;
    }

    /**
     * Sets the value of table.
     * 
     * @param table
     *            The value to assign table.
     */
    public void setTable(String table) {
        this.table = table;
    }

    /**
     * Returns the value of where.
     * 
     * @return where clause specified for the event.
     */
    public String getWhere() {

        return where;
    }

    /**
     * Sets the value of where.
     * 
     * @param where
     *            The value to assign where.
     */
    public void setWhere(String where) {
        this.where = where;
    }

    /**
     * Returns the value of unprocessedPath.
     */
    public String getUnprocessedPath() {

        return unprocessedPath;
    }

    /**
     * Sets the value of unprocessedPath.
     * 
     * @param unprocessedPath
     *            The value to assign unprocessedPath.
     */
    public void setUnprocessedPath(String unprocessedPath) {
        this.unprocessedPath = unprocessedPath;
    }

    /**
     * Returns the value of badPath.
     */
    public String getBadPath() {

        return badPath;
    }

    /**
     * Sets the value of badPath.
     * 
     * @param badPath
     *            The value to assign badPath.
     */
    public void setBadPath(String badPath) {
        this.badPath = badPath;
    }

    /**
     * Returns the value of processPath.
     */
    public String getProcessedPath() {

        return processedPath;
    }

    /**
     * Sets the value of processPath.
     * 
     * @param processPath
     *            The value to assign processPath.
     */
    public void setProcessedPath(String processPath) {
        this.processedPath = processPath;
    }

    /**
     * Returns the value of insertSql.
     */
    public StringBuffer getInsertSql() {

        return insertSql;
    }

    /**
     * Sets the value of insertSql.
     * 
     * @param insertSql
     *            The value to assign insertSql.
     */
    public void setInsertSql(StringBuffer insertSql) {
        this.insertSql = insertSql;
    }

    /**
     * Returns the value of updateSql.
     */
    public StringBuffer getUpdateSql() {

        return updateSql;
    }

    /**
     * Sets the value of updateSql.
     * 
     * @param updateSql
     *            The value to assign updateSql.
     */
    public void setUpdateSql(StringBuffer updateSql) {
        this.updateSql = updateSql;
    }

    /**
     * Returns the value of selectSql.
     */
    public StringBuffer getSelectSql() {

        return selectSql;
    }

    /**
     * Sets the value of selectSql.
     * 
     * @param selectSql
     *            The value to assign selectSql.
     */
    public void setSelectSql(StringBuffer selectSql) {
        this.selectSql = selectSql;
    }

    /**
     * Returns the value of enclosed.
     */
    public String getEnclosed() {

        return enclosed;
    }

    /**
     * Sets the value of enclosed.
     * 
     * @param enclosed
     *            The value to assign enclosed.
     */
    public void setEnclosed(String enclosed) {
        this.enclosed = enclosed;
    }

    /**
     * Returns the value of primary.
     */
    public String getPrimary() {

        return primary;
    }

    /**
     * Sets the value of primary.
     * 
     * @param primary
     *            The value to assign primary.
     */
    public void setPrimary(String primary) {
        this.primary = primary;
    }

    /**
     * Returns the value of ignoreFlag.
     */
    public String getIgnoreFlag() {
        return ignoreFlag;
    }

    /**
     * Sets the value of ignoreFlag.
     * 
     * @param ignoreFlag
     *            The value to assign ignoreFlag.
     */
    public void setIgnoreFlag(String ignoreFlag) {
        this.ignoreFlag = ignoreFlag;
    }

    /**
     * @return <code>org.jconfig.Configuration</code> object containing
     *         mapping of data with events.
     * @see org.jconfig.Configuration
     */
    public Configuration getMappingConfig() {

        return this.mapping;
    }

    /**
     * @return <code>org.jconfig.Configuration</code> object containing
     *         System- wide Configuration
     * @see org.jconfig.Configuration
     */
    public Configuration getInterfaceConfig() {

        return this.config;
    }

    // end setGetMethods

    /**
     * @return <code>boolean</code> if the configuration is already loaded.
     */
    public boolean IsConfigLoaded() {

        return this.load;
    }
    
    public String getEnablemsgcount() {
        return enablemsgcount;
    }

    public void setEnablemsgcount(String enablemessagecount) {
        this.enablemsgcount = enablemessagecount;
    }

    public int getMaxCounter() {
        return maxCounter;
    }

    public void setMaxCounter(int maxCounter) {
        this.maxCounter = maxCounter;
    }

    public String getPreValidate() {
        return preValidate;
    }

    public void setPreValidate(String preValidate) {
        this.preValidate = preValidate;
    }

    public int getValidateMsgCount() {
        return validateMsgCount;
    }

    public void setValidateMsgCount(int validateMsgCount) {
        this.validateMsgCount = validateMsgCount;
    }
    
    public int getMappingCount() {
        return mappingCount;
    }

    public void setMappingCount(int mappingCount) {
        this.mappingCount = mappingCount;
    }


    /**
     * @param event
     *            Name - event name defined in the configuration.
     * @return object of Interface <code>Application</code>. Event name
     *         mapping with Application type object is defined in configuration
     *         file. if no mapping found,it return null.
     */
    public Application getExtension(String event) throws ApplicationException,
            ClassNotFoundException, InstantiationException,
            IllegalAccessException {

        String className = "";
        Application app = null;
        logger.info("Retreiving Extension for Event " + event);
        className = getProperty(event, "extend");

        if (className != null) {

            Class appClass = Class.forName(className); // may throw
            // ClassNotFoundException
            Object appObject = appClass.newInstance();

            try {
                app = (Application) appObject;
            } catch (ClassCastException cce) {
                throw new ApplicationException("The specified class, "
                        + appClass.getName()
                        + ", doesn't implement Application.");
            }

            logger.info("Found extension for " + event + "--Extension found"
                    + app);

            return app;
        } else {
            logger.info("There is no extension defined for event " + event);

            return null;
        }
    }

    /**
     * @param encoding -
     *            XML,delimited
     * @return true if encoding supported by loaded event,false if not.
     */
    public boolean IsMessageSupported(String encode) {

        boolean supports = false;

        if ((config.getProperty(encode, null, "parser")) != null)

            return supports;
        else

            return false;
    }

    /**
     * This class intializes the system wide mapping. Classpath is searched for
     * interface_config.xml and mapping_config.xml files.
     */
    public void LoadProperties() throws ApplicationException {
        logger.info("Loading Properties from Interface Configuration files ");
        config = ConfigurationManager.getConfiguration("interface");
        mapping = ConfigurationManager.getConfiguration("mapping");
        logger.info("Loaded Properties successfully");
        load = true;
        if (config.isNew()) {
            throw new ApplicationException(
                    "Error Loading the configuration file,a new file was created.");
        }

        if (mapping.isNew()) {
            throw new ApplicationException(
                    "Error Loading the mapping,a new file was created.");
        }
    }

    /**
     * @param encode -
     *            encoding is used to load parser for the data object.
     * @return Parser -- parser which impelements interface <code>Parser</code>.
     *         all the dataobject parsing is done by defined parser. Parser
     *         <->Event <-> encoding is defined in configuration file.
     */
    public Parser getParser(String encode) throws ApplicationException,
            ClassNotFoundException, InstantiationException,
            IllegalAccessException {

        Parser parser = null;
        String className = "";
        className = config.getProperty(encode, null, "parser");
        logger.info("Retrieving Parser  for encoding " + encode);

        Class appClass = Class.forName(className); // may throw
        // ClassNotFoundException
        Object appObject = appClass.newInstance();

        try {
            parser = (Parser) appObject;
        } catch (ClassCastException cce) {
            throw new ApplicationException("The specified class, "
                    + appClass.getName() + ", doesn't implement Application.");
        }

        return parser;
    }

    /**
     * @return property value as <code>String</code>
     * @param proprety
     *            name for which value is retrieved.
     */
    public String getProperty(String key) {

        return config.getProperty(key);
    }

    /**
     * @return property value as <code>String</code>
     * @param proprety
     *            name for which value is retrieved.
     * @param category
     *            if the property is listed under a category.
     */
    public String getProperty(String key, String category) {

        return config.getProperty(key, "", category);
    }

    /**
     * Retrieves the attributes value for the event
     * 
     * @param event -
     *            event name to be loaded
     */
    public void LoadEvent(String event) {
        flushEvent();
        logger.debug("Loading event--" + event);
        setHeader(getProperty("header", event));
        logger.debug("Header--" +this.getHeader());
        setEncoding(getProperty("encoding", event));
        logger.debug("Encoding--" +this.getEncoding());
        setEnclosed(getProperty("enclosed", event));
        logger.debug("Enclosed--" +this.getEnclosed());
         setDlm(getProperty("dlm", event));
         logger.debug("dlm--" +this.getDlm());
        setIgnoreFlag(getProperty("ignore", event));
        logger.debug("ignore--" +this.getIgnoreFlag());
        // setFileName(getProperty("filename",event));
        // setFilePath(getProperty("filepath",event));
        setColList(config.getArray("collist", new String[] {}, event));
        logger.debug("ColList--" +this.getColList().toString());
        setDbColList(config.getArray("dbcollist", new String[] {}, event));
        logger.debug("DBColList--" +this.getDbColList().toString());
        setTable(getProperty("table", event));
        logger.debug("table--" +this.getTable());
        setPrimary(getProperty("primarykey", event));
        logger.debug("primarykey--" +this.getPrimary());
        setWhere(getProperty("where", event));
        logger.debug("where--" +this.getWhere());
        setProcessedPath(getProperty("processedpath", event));
        logger.debug("processedpath--" +this.getProcessedPath());
        setBadPath(getProperty("badfilepath", event));
        logger.debug("badpath--" +this.getBadPath());
        setUnprocessedPath(getProperty("unprocessedpath", event));
        logger.debug("unprocessedpath--" +this.getUnprocessedPath());
        
        if (getProperty("prevalidate", event).length()>0)
        this.setPreValidate((getProperty("prevalidate", event)).toUpperCase());
        
        logger.debug("prevalidate--" +this.getPreValidate());
        
        if (getProperty("enablemsgcount", event).length()>0)
            this.setEnablemsgcount((getProperty("enablemsgcount", event)).toUpperCase());
        
        logger.debug("enablemsgcount--" +this.getEnablemsgcount());
        
        if (getProperty("maxcounter", event).length()>0)
            this.setMaxCounter(new Integer(getProperty("maxcounter", event)).intValue());
        
        logger.debug("maxcounter--" +this.getMaxCounter());
        
        if (getProperty("validatemsgcount", event).length()>0)
            this.setValidateMsgCount(new Integer(getProperty("validatemsgcount", event)).intValue());
        
        if (getProperty("mappingcount", event).length()>0)
            this.setMappingCount(new Integer(getProperty("mappingcount", event)).intValue());
        
        logger.debug("validatemsgcount--" +this.getValidateMsgCount());

        try {
            CreateSQL();
        } catch (Exception e) {
        }
    }
    public void flushEvent() {
         setHeader("");
         setEncoding("");
         setEnclosed("");
         setDlm("");
        setIgnoreFlag("");
        setColList(new String[1]);
         setDbColList(new String[1]);
           setTable("");
          setPrimary("");
            setWhere("");
          setProcessedPath("");
         setBadPath("");
           setUnprocessedPath("");
           
           this.setPreValidate("");
        
                
        
            this.setEnablemsgcount("");
        
               
        
            this.setMaxCounter(-1);
        
        
        
            this.setValidateMsgCount(0);
        
                  this.setMappingCount(0);
        




    }

    /**
     * Method to create DB sql for the db columns specified in configuration
     * file.
     * 
     */
    public void CreateSQL() throws ApplicationException {
        int mismatch = 0;
        partColList=new ArrayList();
        String[] colArray = getColList();
        String[] dbColArray = getDbColList();
        int colArrLen = colArray.length;
        int dbColLen = dbColArray.length;
        StringBuffer valueStr = new StringBuffer();
        insertSql.delete(0, insertSql.length());
        updateSql.delete(0, updateSql.length());
        selectSql.delete(0, selectSql.length());

        String table = getTable();
        String where = getWhere();
        String tempStr = "";
        insertSql.append("insert into " + table);
        valueStr.append("Values");
        updateSql.append("update " + table + " set ");

        // Following Commented by VA. This would fail if a String field is
        // specfied in primarykey XML property
        // e.g if person_code has 'VEL123',then getting it as count would creash
        // in DefaultApplication
        // selectSql.append("Select " + primary + " as count from " + table);

        selectSql.append("Select count(*) as count from " + table);

        if (colArrLen != dbColLen) {

            throw new ApplicationException(
                    "Exception thrown in ProcessFormData"
                            + "Mismatch between the File Columns and DB columns defined.Please make sure that "
                            + "One-to-One mapping is available");

        }

        try {
            for (int i = 0; i < dbColLen; i++) {
                tempStr = colArray[i];

                if (i == 0) {
                    insertSql.append("(" + dbColArray[i] + ",");

                    if ((tempStr.indexOf("[VELEXPR]")) >= 0) {
                        tempStr = StringUtil.replace(tempStr, "[VELEXPR]", "");
                        valueStr.append("(" + tempStr + ",");

                        if ((tempStr.indexOf("[VELPK]")) < 0) {
                            updateSql.append(dbColArray[i] + "=" + tempStr
                                    + ",");
                            partColList.add("[VELEXPR]"+tempStr);
                        }

                    } else {
                        valueStr.append("(?,");

                        if ((tempStr.indexOf("[VELPK]")) < 0) {
                            updateSql.append(dbColArray[i] + "=?,");
                            partColList.add(tempStr);
                        }
                    }
                } else if (i == (dbColLen - 1)) {
                    insertSql.append(dbColArray[i] + ")");

                    if ((tempStr.indexOf("[VELEXPR]")) >= 0) {
                        tempStr = StringUtil.replace(tempStr, "[VELEXPR]", "");
                        valueStr.append(tempStr + ")");

                        if ((tempStr.indexOf("[VELPK]")) < 0) {
                            updateSql.append(dbColArray[i] + "=" + tempStr);
                            partColList.add("[VELEXPR]"+tempStr);
                        }
                    } else {
                        valueStr.append("?)");

                        if ((tempStr.indexOf("[VELPK]")) < 0) {
                            updateSql.append(dbColArray[i] + "=?");
                            partColList.add(tempStr);
                        }
                    }
                } else {
                    insertSql.append(dbColArray[i] + ",");

                    if ((tempStr.indexOf("[VELEXPR]")) >= 0) {
                        tempStr = StringUtil.replace(tempStr, "[VELEXPR]", "");
                        valueStr.append(tempStr + ",");

                        if ((tempStr.indexOf("[VELPK]")) < 0) {
                            updateSql.append(dbColArray[i] + "=" + tempStr
                                    + ",");
                            partColList.add("[VELEXPR]"+tempStr);
                        }
                    } else {
                        valueStr.append("?,");

                        if ((tempStr.indexOf("[VELPK]")) < 0) {
                            updateSql.append(dbColArray[i] + "=?,");
                            partColList.add(tempStr);
                        }
                    }
                }
            }
            String valueString=valueStr.toString();
            if ((valueString.indexOf("[VELREQUIRED]")) >= 0) 
                            valueString = StringUtil.replace(valueString, "[VELREQUIRED]","");
            
            
            insertSql.append(" " + valueString);
            

            // Process update SQL and remove any trailing commans from the
            // statement
            if (updateSql.lastIndexOf(",")==(updateSql.length() -1) ) 
                updateSql=updateSql.delete(updateSql.length()-1,updateSql.length());

            if (((getWhere()).length()) > 0) {
                selectSql.append(" where " + getWhere());
            }
            
            
            if ((updateSql.indexOf("[VELREQUIRED]")) >= 0)
                updateSql=new StringBuffer(StringUtil.replace(updateSql.toString(), "[VELREQUIRED]",""));

            logger.debug("SQL created for bind and Velos variables processing"
                    + "\n" + " InsertSQL-- " + insertSql + "\n"
                    + " UpdateSQL--" + updateSql + "\n" + " selectSql--"
                    + selectSql);
        } catch (Exception e) {
            logger.fatal("Exception occured in CreateSQL" + e.getMessage());
        }

    } // end createSQL

    /**
     * This method removes the enclosing character from the input String
     * otherwise return the string as it is
     * 
     * @param inputStr -
     *            process String
     * @return String - Processed String
     */
    public String StripEnclosedChar(String inputStr) {

        int len = (getEnclosed()).length();
        inputStr = inputStr.trim();

        if ((len > 0)
                && ((inputStr.indexOf(getEnclosed()) == 0) && (inputStr
                        .lastIndexOf(getEnclosed()) == inputStr.length() - len))) {
            inputStr = inputStr.substring(len);
            inputStr = inputStr.substring(0, (inputStr.length() - len));
        }

        return inputStr;
    }
}
