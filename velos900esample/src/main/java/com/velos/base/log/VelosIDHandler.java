package com.velos.base.log;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.jdbcplus.JDBCIDHandler;
import org.apache.log4j.jdbcplus.JDBCDefaultConnectionHandler;

import com.velos.base.DataHolder;
import com.velos.base.dbutil.DBEngine;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * Implement a sample JDBCIDHandler
 * 
 * @author 
 * <a href="mailto:vabrol@velos.com">Vishal Abrol</a>
 * @since 1.0
 * @version 1.0
 */
public class VelosIDHandler implements JDBCIDHandler
{
    private static int id = 0;
    //Connection con = null;
    DataHolder dataHolder=DataHolder.getDataHolder();
    DBEngine dbEngine=DBEngine.getEngine();
    /*String url = "jdbc:mysql://localhost/veloslog?user=loguser&password=eresearch";
    String username = "loguser";
    String password = "eresearch";
    
    static {
      try {
          // load driver
            Driver dMySql = (Driver) (Class.forName("com.mysql.jdbc.Driver").newInstance());
        DriverManager.registerDriver(dMySql);
      } catch (Exception e) {
          System.err.println("Could not register driver.");
          e.printStackTrace();
      }
    }

    public Connection getConnection() {
      return getConnection(url, username, password);
    }

    public Connection getConnection(String _url, String _username, String _password) {
      try {
        if (con != null && !con.isClosed())
          con.close();
            con = DriverManager.getConnection(url);
            System.out.println("Got Connection");
        con.setAutoCommit(false);
      } catch (Exception e) {
        e.printStackTrace();
      }

      return con;
    }
*/
    public synchronized Object getID()
   {
        GenerateID();
        dataHolder.setCURRENTID(id);
        return new Long(id);
   }
   
    public void GenerateID()
   {
      //Connection conn = getConnection();
        Connection conn = dbEngine.getConnection("logger");
       CallableStatement cstmt = null;
       ResultSet rs = null;
      try {
           // conn = getConnection();
            cstmt = conn
                    .prepareCall("{call SP_GENERATE_ID(?,?,?)}");

            cstmt.setString(1, "velos");
            cstmt.setString(2, "logcat");
            cstmt.registerOutParameter(3, java.sql.Types.INTEGER);
            cstmt.execute();

            id= cstmt.getInt(3);
            
            
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    DBEngine.returnConnection(conn);
            } catch (Exception e) {
            }

        }

   }
}

