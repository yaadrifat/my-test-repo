package com.velos.base.extension;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.velos.base.Application;
import com.velos.base.InterfaceConfig;
import com.velos.base.dbutil.DBEngine;
import com.velos.eres.service.util.EJBUtil;

public class PatImport implements Application {
    private static Logger logger = Logger.getLogger(AdvEventImport.class
            .getName());

    public int processMessage(ArrayList in, InterfaceConfig cfg, Connection conn) {

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList out = new ArrayList(), perList = new ArrayList();
        String sql = "", personCode = "", inVal = "";
        int pk_person = -1, output = -1;
        try {
            conn.setAutoCommit(false);
            logger.info("Check if the record already exist for the Patient ");
            personCode = (cfg.StripEnclosedChar((String) in.get(1))).trim();
            /*
             * Add person code(once in insert mode) twice as it is used as first
             * 2 bind variables in populating er_per
             */
            perList.add(personCode);
            logger.info("Setting bind variable value: " + personCode);
            out.add(personCode);
            inVal = DBEngine
                    .getDBValue(
                            "select pk_person from epat.person where lower(person_code)=lower(?)",
                            out);
            out.clear();
            if (inVal.length() > 0)
                pk_person = EJBUtil.stringToNum(inVal);
            if (pk_person > 0) {
                logger.info("Record already exist for Person Code:"
                        + personCode + " Update the record with the data. ");

                sql = "update epat.Person set PERSON_CODE=?,PERSON_FNAME=?,"
                        + "PERSON_LNAME=?,PERSON_DOB=to_date(?,'mm/dd/yyyy'),FK_CODELST_GENDER=?,FK_CODELST_RACE=?,"
                        + "FK_CODELST_ETHNICITY=?,PERSON_ADDRESS1=?,PERSON_ADDRESS2=?,"
                        + "PERSON_CITY=?,PERSON_STATE=?,PERSON_ZIP=?,PERSON_COUNTRY=?,PERSON_EMAIL=?,"
                        + "FK_SITE=?,PERSON_HPHONE=?,PERSON_BPHONE=?,FK_CODELST_PSTAT=?,FK_ACCOUNT=?,CREATOR=?,FK_TIMEZONE=?"
                        + " where pk_person=?";
                pstmt = conn.prepareStatement(sql);
                pstmt.clearParameters();

                // set Person Code and return if it's not provided
                inVal = cfg.StripEnclosedChar((String) in.get(1));
                logger.info("Processing Value: " + inVal);
                if (inVal.length() == 0)
                    return -1;
                pstmt.setString(1, inVal);

                // Add again
                perList.add(personCode);

                // Set Person FirstName
                inVal = cfg.StripEnclosedChar((String) in.get(3));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(2, inVal);

                // set Person LastName
                inVal = cfg.StripEnclosedChar((String) in.get(4));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(3, inVal);

                // set Person Dob
                inVal = cfg.StripEnclosedChar((String) in.get(5));
                logger.info("Processing value: " + inVal);
                pstmt.setString(4, inVal);

                // set FK_CODELST_GENDER
                inVal = cfg.StripEnclosedChar((String) in.get(6));
                if (inVal.length() == 0)
                    inVal = "[VELDEFAULT]";
                logger.info("Processing value: " + inVal);
                out.add(inVal);
                inVal = DBEngine.getDBValue("select mapping_lmapfld"
                        + " from er_mapping where (?)= (mapping_rmapfld) "
                        + "and mapping_type='gender'", out);
                pstmt.setString(5, inVal);
                // clear the parameter arraylist for next run,if any
                out.clear();

                // set Race
                inVal = cfg.StripEnclosedChar((String) in.get(8));
                if (inVal.length() == 0)
                    inVal = "[VELDEFAULT]";
                logger.info("Processing value: " + inVal);
                out.add(inVal);
                inVal = DBEngine.getDBValue("select mapping_lmapfld"
                        + " from er_mapping where (?)= (mapping_rmapfld) "
                        + " and mapping_type='race' ", out);
                pstmt.setString(6, inVal);
                // clear the parameter arraylist for next run,if any
                out.clear();

                // ethnicity
                inVal = cfg.StripEnclosedChar((String) in.get(10));
                if (inVal.length() == 0)
                    inVal = "[VELDEFAULT]";
                logger.info("Processing value: " + inVal);
                out.add(inVal);
                inVal = DBEngine.getDBValue("select mapping_lmapfld"
                        + " from er_mapping where (?)= (mapping_rmapfld) "
                        + " and mapping_type='ethnicity' ", out);
                pstmt.setString(7, inVal);

                // clear the parameter arraylist for next run,if any
                out.clear();

                // Address1
                inVal = cfg.StripEnclosedChar((String) in.get(12));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(8, inVal);

                // Address2
                inVal = cfg.StripEnclosedChar((String) in.get(13));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(9, inVal);

                // city
                inVal = cfg.StripEnclosedChar((String) in.get(14));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(10, inVal);

                // state
                inVal = cfg.StripEnclosedChar((String) in.get(15));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(11, inVal);

                // zip
                inVal = cfg.StripEnclosedChar((String) in.get(16));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(12, inVal);

                // country
                inVal = cfg.StripEnclosedChar((String) in.get(17));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(13, inVal);

                // email
                inVal = cfg.StripEnclosedChar((String) in.get(18));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(14, inVal);

                // site
                inVal = cfg.StripEnclosedChar((String) in.get(19));
                if (inVal.length() == 0)
                    inVal = "[VELDEFAULT]";
                logger.info("Processing value: " + inVal);
                out.add(inVal);
                inVal = DBEngine.getDBValue("select mapping_lmapfld"
                        + " from er_mapping where (?)= (mapping_rmapfld) "
                        + " and mapping_type='site' ", out);
                pstmt.setString(15, inVal);
                // populate list for er_per table
                perList.add(inVal);
                // clear the parameter arraylist for next run,if any
                out.clear();

                // Home Phone
                inVal = cfg.StripEnclosedChar((String) in.get(20));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(16, inVal);

                // work Phone
                inVal = cfg.StripEnclosedChar((String) in.get(21));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(17, inVal);

                // Status
                inVal = cfg.StripEnclosedChar((String) in.get(22));
                if (inVal.length() == 0)
                    inVal = "[VELDEFAULT]";
                logger.info("Processing value: " + inVal);
                out.add(inVal);
                inVal = DBEngine.getDBValue("select mapping_lmapfld"
                        + " from er_mapping where (?)= (mapping_rmapfld) "
                        + " and mapping_type='patient_status' ", out);
                pstmt.setString(18, inVal);
                // clear the parameter arraylist for next run,if any
                out.clear();

                // FK Account,retrieve from mapping table
                inVal = "[VELDEFAULT]";
                logger.info("Processing value: " + inVal);
                out.add(inVal);
                inVal = DBEngine.getDBValue("select mapping_lmapfld"
                        + " from er_mapping where (?)= (mapping_rmapfld) "
                        + " and mapping_type='account' ", out);
                pstmt.setString(19, inVal);

                // populate list for er_per table
                perList.add(inVal);
                // clear out the parameter List
                out.clear();

                // Creator,from mapping table
                inVal = "[VELDEFAULT]";
                logger.info("Processing value: " + inVal);
                out.add(inVal);
                inVal = DBEngine.getDBValue("select mapping_lmapfld"
                        + " from er_mapping where (?)= (mapping_rmapfld) "
                        + " and mapping_type='iuser' ", out);
                pstmt.setString(20, inVal);

                // populate list for er_per table
                perList.add(inVal);

                // clear the parameter arraylist for next run,if any
                out.clear();

                // TimeZone,from mapping table
                inVal = "[VELDEFAULT]";
                logger.info("Processing value: " + inVal);
                out.add(inVal);
                inVal = DBEngine.getDBValue("select mapping_lmapfld"
                        + " from er_mapping where (?)= (mapping_rmapfld) "
                        + " and mapping_type='timezone' ", out);
                pstmt.setString(21, inVal);

                // populate list for er_per table
                perList.add(inVal);

                // clear the parameter arraylist for next run,if any
                out.clear();

                // Pk_Perosn in where clause
                pstmt.setInt(22, pk_person);
                // populate list for er_per table
                perList.add((new Integer(pk_person)).toString());

                logger.debug("Updating record...");
                output = pstmt.executeUpdate();
                logger
                        .info("Person:Record processed sucessfully with update status"
                                + output);
                if (output >= 0) {
                    conn.commit();
                } else {
                    logger.error("Rolling back all the changes");
                    conn.rollback();
                }
                out.add(personCode);
                if (output >= 0)
                    output = DBEngine
                            .setDBValue(
                                    "update er_per set pk_per="
                                            + " (select pk_person from epat.person where lower(person_code)=lower(?))"
                                            + ",per_code=?,fk_site=?,fk_account=?,creator=?,fk_timezone=?  "
                                            + " where pk_per=? ", perList);
                out.clear();
                logger
                        .info("er_per:Record processed sucessfully with update status"
                                + output);

            } // end for (pk_person>0)

            else {

                logger.info("Record does not exist for Person Code:"
                        + personCode + " Insert the record with the data. ");
                sql = "insert into epat.Person(PK_PERSON,PERSON_CODE,PERSON_FNAME,"
                        + "PERSON_LNAME,PERSON_DOB,FK_CODELST_GENDER,FK_CODELST_RACE,"
                        + "FK_CODELST_ETHNICITY,PERSON_ADDRESS1,PERSON_ADDRESS2,"
                        + "PERSON_CITY,PERSON_STATE,PERSON_ZIP,PERSON_COUNTRY,PERSON_EMAIL,"
                        + "FK_SITE,PERSON_HPHONE,PERSON_BPHONE,FK_CODELST_PSTAT,FK_ACCOUNT,CREATOR,FK_TIMEZONE) "
                        + "values (seq_er_per.nextval,?,?,?,to_date(?,'mm/dd/yyyy'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                pstmt = conn.prepareStatement(sql);
                pstmt.clearParameters();

                // set Person Code and return if it's not provided
                inVal = cfg.StripEnclosedChar((String) in.get(1));
                logger.info("Processing Value: " + inVal);
                if (inVal.length() == 0)
                    return -1;
                pstmt.setString(1, inVal);

                // Add again
                perList.add(personCode);

                // Set Person FirstName
                inVal = cfg.StripEnclosedChar((String) in.get(3));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(2, inVal);

                // set Person LastName
                inVal = cfg.StripEnclosedChar((String) in.get(4));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(3, inVal);

                // set Person Dob
                inVal = cfg.StripEnclosedChar((String) in.get(5));
                logger.info("Processing value: " + inVal);
                pstmt.setString(4, inVal);

                // set FK_CODELST_GENDER
                inVal = cfg.StripEnclosedChar((String) in.get(6));
                if (inVal.length() == 0)
                    inVal = "[VELDEFAULT]";
                logger.info("Processing value: " + inVal);
                out.add(inVal);
                inVal = DBEngine.getDBValue("select mapping_lmapfld"
                        + " from er_mapping where (?)= (mapping_rmapfld) "
                        + "and mapping_type='gender'", out);
                pstmt.setString(5, inVal);
                // clear the parameter arraylist for next run,if any
                out.clear();

                // set Race
                inVal = cfg.StripEnclosedChar((String) in.get(8));
                if (inVal.length() == 0)
                    inVal = "[VELDEFAULT]";
                logger.info("Processing value: " + inVal);
                out.add(inVal);
                inVal = DBEngine.getDBValue("select mapping_lmapfld"
                        + " from er_mapping where (?)= (mapping_rmapfld) "
                        + " and mapping_type='race' ", out);
                pstmt.setString(6, inVal);
                // clear the parameter arraylist for next run,if any
                out.clear();

                // ethnicity
                inVal = cfg.StripEnclosedChar((String) in.get(10));
                if (inVal.length() == 0)
                    inVal = "[VELDEFAULT]";
                logger.info("Processing value: " + inVal);
                out.add(inVal);
                inVal = DBEngine.getDBValue("select mapping_lmapfld"
                        + " from er_mapping where (?)= (mapping_rmapfld) "
                        + " and mapping_type='ethnicity' ", out);
                pstmt.setString(7, inVal);

                // clear the parameter arraylist for next run,if any
                out.clear();

                // Address1
                inVal = cfg.StripEnclosedChar((String) in.get(12));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(8, inVal);

                // Address2
                inVal = cfg.StripEnclosedChar((String) in.get(13));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(9, inVal);

                // city
                inVal = cfg.StripEnclosedChar((String) in.get(14));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(10, inVal);

                // state
                inVal = cfg.StripEnclosedChar((String) in.get(15));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(11, inVal);

                // zip
                inVal = cfg.StripEnclosedChar((String) in.get(16));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(12, inVal);

                // country
                inVal = cfg.StripEnclosedChar((String) in.get(17));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(13, inVal);

                // email
                inVal = cfg.StripEnclosedChar((String) in.get(18));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(14, inVal);

                // site
                inVal = cfg.StripEnclosedChar((String) in.get(19));
                if (inVal.length() == 0)
                    inVal = "[VELDEFAULT]";
                logger.info("Processing value: " + inVal);
                out.add(inVal);
                inVal = DBEngine.getDBValue("select mapping_lmapfld"
                        + " from er_mapping where (?)= (mapping_rmapfld) "
                        + " and mapping_type='site' ", out);
                pstmt.setString(15, inVal);
                // populate list for er_per table
                perList.add(inVal);
                // clear the parameter arraylist for next run,if any
                out.clear();

                // Home Phone
                inVal = cfg.StripEnclosedChar((String) in.get(20));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(16, inVal);

                // work Phone
                inVal = cfg.StripEnclosedChar((String) in.get(21));
                logger.info("Processing Value: " + inVal);
                pstmt.setString(17, inVal);

                // Status
                inVal = cfg.StripEnclosedChar((String) in.get(22));
                if (inVal.length() == 0)
                    inVal = "[VELDEFAULT]";
                logger.info("Processing value: " + inVal);
                out.add(inVal);
                inVal = DBEngine.getDBValue("select mapping_lmapfld"
                        + " from er_mapping where (?)= (mapping_rmapfld) "
                        + " and mapping_type='patient_status' ", out);
                pstmt.setString(18, inVal);
                // clear the parameter arraylist for next run,if any
                out.clear();

                // FK Account,retrieve from mapping table
                inVal = "[VELDEFAULT]";
                logger.info("Processing value: " + inVal);
                out.add(inVal);
                inVal = DBEngine.getDBValue("select mapping_lmapfld"
                        + " from er_mapping where (?)= (mapping_rmapfld) "
                        + " and mapping_type='account' ", out);
                pstmt.setString(19, inVal);

                // populate list for er_per table
                perList.add(inVal);
                // clear out the parameter List
                out.clear();

                // Creator,from mapping table
                inVal = "[VELDEFAULT]";
                logger.info("Processing value: " + inVal);
                out.add(inVal);
                inVal = DBEngine.getDBValue("select mapping_lmapfld"
                        + " from er_mapping where (?)= (mapping_rmapfld) "
                        + " and mapping_type='iuser' ", out);
                pstmt.setString(20, inVal);

                // populate list for er_per table
                perList.add(inVal);

                // clear the parameter arraylist for next run,if any
                out.clear();

                // TimeZone,from mapping table
                inVal = "[VELDEFAULT]";
                logger.info("Processing value: " + inVal);
                out.add(inVal);
                inVal = DBEngine.getDBValue("select mapping_lmapfld"
                        + " from er_mapping where (?)= (mapping_rmapfld) "
                        + " and mapping_type='timezone' ", out);
                pstmt.setString(21, inVal);

                // populate list for er_per table
                perList.add(inVal);

                // clear the parameter arraylist for next run,if any
                out.clear();

                logger.debug("Inserting record...");
                output = pstmt.executeUpdate();
                logger
                        .info("Person:Record processed sucessfully with update status"
                                + output);
                if (output >= 0) {
                    conn.commit();
                } else {
                    logger.error("Rolling back all the changes");
                    conn.rollback();
                }
                out.add(personCode);
                if (output >= 0)
                    output = DBEngine
                            .setDBValue(
                                    "insert into er_per(pk_per"
                                            + ",per_code,fk_site,fk_account,creator,fk_timezone) values "
                                            + " ((select pk_person from epat.person where lower(person_code)=lower(?))"
                                            + ",?,?,?,?,?)", perList);
                out.clear();
                logger
                        .info("er_per:Record processed sucessfully with update status"
                                + output);

            } // end for else pk_person>0
        } catch (SQLException e) {
            logger.fatal("Error Processing the record :" + e.getMessage());
            try {
                conn.rollback();
            } catch (SQLException se) {
            }
            return -1;
        } catch (Exception e) {
            logger.fatal("Exception Processing the record :" + e.getMessage());
            try {
                conn.rollback();
            } catch (SQLException se) {
            }
            return -1;
        }
        return output;
    }

}
