package com.velos.base;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.log4j.Logger;

import com.velos.eres.service.util.StringUtil;

/**
 * This class abstracts the basic Java File I/O classes to make File I/O a
 * little more straightforward.
 */

public class VelosIO implements Parser {
    private static Logger logger = Logger.getLogger(VelosIO.class.getName());

    /** The constant used to indicate that a file is for reading */
    public final static int FOR_READING = 1;

    /** The constant used to indicate that a file is for writing */
    public final static int FOR_WRITING = 2;

    /** Stores whether the file was intended for writing or for reading */
    private int setting;

    /** The abstracted BufferedReader if the file was for reading */
    private BufferedReader br;

    /** The abstracted PrintWriter if the file was for writing */
    private PrintWriter pw;

    /** Indicates whether the end of the file has been reached or not */
    private boolean EOF = false;

    /**
     * Indicates whether the user wanted to extract tokens from each line in the
     * file
     */
    protected boolean tokens;

    /** The delimiter to use if tokens are desired by the user */
    protected String delimiter;

    /** The stores the current row being processed */
    protected String currentBuffer;

    

    /**
     * Returns the value of currentBuffer.
     */
    public String getCurrentBuffer() {
        return currentBuffer;
    }

    /**
     * Sets the value of currentBuffer.
     * 
     * @param currentBuffer
     *            The value to assign currentBuffer.
     */
    public void setCurrentBuffer(String currentBuffer) {
        this.currentBuffer = currentBuffer;
    }

    /**
     * Sets the Properties for the VelosIO
     * 
     * @param filename
     *            Filename of process
     * @param operation-
     *            whether file is for reading(1) or writing(0)
     */
    public void setProperties(String fileName, int operation) {
        try {
            if (operation == FOR_READING || operation == FOR_WRITING) {
                setting = operation;
            } else {
                throw new RuntimeException("Must specify reading or writing.");
            }

            if (setting == FOR_READING) {
                FileReader fr = new FileReader(fileName);
                br = new BufferedReader(fr);
            } else if (setting == FOR_WRITING) {
                FileWriter fw = new FileWriter(fileName);
                BufferedWriter bw = new BufferedWriter(fw);
                pw = new PrintWriter(bw);
            }
        } catch (IOException ioe) {
            throw new RuntimeException("IO Error");
        }
    }

    public void setProperties(String fileName, String delimiter) {

        try {
            setting = FOR_READING;
            if (delimiter != null) {
                if (delimiter.length() > 0) {
                    tokens = true;
                    this.delimiter = delimiter;
                }
            }

            FileReader fr = new FileReader(fileName);
            br = new BufferedReader(fr);
        } catch (IOException ioe) {
            // logger.error(ioe +"Message received with Exception
            // is"+ioe.getMessage() );
            // Rlog.fatal("common","It seems like I can not read the file. Did
            // you check if the file is there?");
        }

    }

    public VelosIO(String fileName, int operation) {

        try {
            if (operation == FOR_READING || operation == FOR_WRITING) {
                setting = operation;
            } else {
                throw new RuntimeException("Must specify reading or writing.");
            }

            if (setting == FOR_READING) {
                FileReader fr = new FileReader(fileName);
                br = new BufferedReader(fr);
            } else if (setting == FOR_WRITING) {
                FileWriter fw = new FileWriter(fileName);
                BufferedWriter bw = new BufferedWriter(fw);
                pw = new PrintWriter(bw);
            }
        } catch (IOException ioe) {
            throw new RuntimeException("IO Error");
        }

    }

    public VelosIO() {
        setting = FOR_READING;
        tokens = true;
        this.delimiter = ",";

    }

    public VelosIO(String fileName, String delimiter) {
        try {
            setting = FOR_READING;
            tokens = true;
            this.delimiter = delimiter;
            // logger.debug("File is set for Reading" + "Delimiter received is:
            // "+this.delimiter);
            FileReader fr = new FileReader(fileName);
            br = new BufferedReader(fr);
        } catch (IOException ioe) {
            logger.fatal("Message received with Exception is"
                    + ioe.getMessage());

        }

    }

    /**
     * Parser the current data set element and return the attributes in an
     * arraylist.
     * 
     * @return ArrayList - list of all the attibutes in the current data set
     *         element
     */
    public ArrayList getTokens() {
        ArrayList list = new ArrayList();
        String[] listArr = null;
        if (tokens && !EOF) {
            String temp = readLine();
            temp=(temp==null)?"":temp.trim();
            // logger.debug("File Reader read the next line: " + temp);
            if (!EOF) {
                listArr = StringUtil.strSplit(temp, delimiter, false);
                list = new ArrayList(Arrays.asList(listArr));
            }
        } else {
            // logger.error("Exception,Attempt to read unavailable Line ");
            throw new RuntimeException("Tokens not available.");
        }

        // return list.iterator();
        return list;
    }

    /**
     * Constructor
     * 
     * @param String
     *            fileName - fileName(data set) to process.
     * @param String
     *            path - path where the file is stored.
     * @param int
     *            operation - 1=Reading , 2= Writing
     */
    protected VelosIO(String fileName, String path, int operation) {
        try {
            File file = new File(path, fileName);

            if (operation == FOR_READING || operation == FOR_WRITING) {
                setting = operation;
            } else {
                throw new IOException("Must specify reading or writing.");
            }

            if (setting == FOR_READING) {
                FileReader fr = new FileReader(file);
                br = new BufferedReader(fr);
            } else if (setting == FOR_WRITING) {
                FileWriter fw = new FileWriter(file);
                BufferedWriter bw = new BufferedWriter(fw);
                pw = new PrintWriter(bw);
            }
        } catch (IOException ioe) {
            throw new RuntimeException("IO Error");
        }

    }

    public boolean EOF() {
        return EOF;
    }

    /**
     * read the next dataset element .
     * 
     * @return String - dataset element read.
     */
    public String readLine() {

        String temp = null;
        try {
            if (setting == FOR_READING) {
                temp = br.readLine();
                if (temp == null) {
                    EOF = true;
                }
            } else {
                throw new RuntimeException("File is not open for reading.");
            }
        } catch (IOException ioe) {
            throw new RuntimeException("IO Error");
        }
        this.currentBuffer = temp;
        return temp;
    }

    /**
     * if the file is opened in Write mode, writeLine writes the line to a file.
     * 
     * @param String
     *            line - line to write to a file.
     */

    public void writeLine(String line) {

        if (setting == FOR_WRITING) {
            pw.println(line);
        } else {
            throw new RuntimeException("File is not open for writing.");
        }

    }

    /**
     * To close the open stream to file.
     */
    public void close() {
        try {
            if (setting == FOR_READING) {
                br.close();
                setting = -1;
                br = null;
            } else if (setting == FOR_WRITING) {
                pw.close();
                setting = -1;
                pw = null;
            }
        } catch (IOException ioe) {
            logger.fatal("Error closing the file,Exception thrown is: " + ioe
                    + ":Message:" + ioe.getMessage());
            ioe.printStackTrace();
            throw new RuntimeException("IO Error");
        }
    }

    /**
     * Method that layers on Java rename Method and provide some more options
     * 
     * @param String -
     *            source file that need to be moved
     * @param String
     *            destFile - destination file name
     * @param String
     *            Path - Path where the file should be stored.
     * @return boolean - true if file moved successfully otherwise false.
     */
    public boolean MoveFile(String srcFile, String destFile, String path) {

        File file = new File(srcFile);
        File destPath = new File(path);
        boolean success = file.renameTo(new File(destPath, destFile));
        return success;

    }

    /**
     * Method that layers on Java rename Method and provide some more options
     * File is stored with the same name to specified location.
     * 
     * @param String -
     *            source file path that need to be moved-absolue path
     * @param String
     *            Path - Path where the file should be stored.
     * @return boolean - true if file moved successfully otherwise false.
     */

    public boolean MoveFile(String srcFile, String path) {

        File file = new File(srcFile);
        File destPath = new File(path);
        boolean success = file.renameTo(new File(destPath, file.getName()));
        return success;
    }

    /**
     * Method that layers on Java rename Method and provide some more options
     * 
     * @param File -
     *            source file that need to be moved
     * @param String
     *            destFile - destination file name
     * @param String
     *            Path - Path where the file should be stored.
     * @return boolean - true if file moved successfully otherwise false.
     */
    public boolean MoveFile(File srcFile, String destFile, String path) {

        // Destination directory
        File destPath = new File(path);

        // Move file to new directory
        boolean success = srcFile
                .renameTo(new File(destPath, srcFile.getName()));
        return success;
    }

    /**
     * Method that layers on Java rename Method and provide some more options
     * 
     * @param File -
     *            source file that need to be moved
     * @param String
     *            Path - Path where the file should be stored.
     * @return boolean - true if file moved successfully otherwise false.
     */
    public boolean MoveFile(File srcFile, String path) {

        try {
            // Destination directory
            File destPath = new File(path);
            if (!(destPath.exists()))
                destPath.mkdir();
            boolean success = srcFile.renameTo(new File(destPath, srcFile
                    .getName()
                    + System.currentTimeMillis()));
            return success;
        } catch (Exception ioe) {
            logger.fatal("Error moving file,error is" + ioe + "ioe.getmessage"
                    + ioe.getMessage());
            ioe.printStackTrace();
            return false;
        }
    }

    /**
     * Method parses the given file Name and return the filename without the
     * extension. e.g. if filename passed is 'java.txt',method will return
     * 'java'
     * 
     * @param String
     *            fileName - filename to parse.
     * @return String - filename without the extension.
     */
    public static String getFileName(String fileName) {
        int ind = fileName.lastIndexOf(".");
        if (0 < ind && ind < (fileName.length() - 1)) {
            return fileName.substring(0, ind);
        }
        return (null);
    }

    /**
     * Method parses the given file Name and return the extension wihtout the
     * filename. e.g. if filename passed is 'java.txt',method will return 'txt'
     * 
     * @param String
     *            fileName - filename to parse.
     * @return String - extension without the fileName
     */
    public static String getFileExtension(String text) {
        int ind = text.lastIndexOf(".");
        if (0 < ind && ind < (text.length() - 1)) {
            return text.substring(ind + 1).toLowerCase();
        }
        return (null);
    }
    


    

}
