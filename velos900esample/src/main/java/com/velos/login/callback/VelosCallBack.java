package com.velos.login.callback;

import javax.security.auth.callback.Callback;

import com.velos.login.ConnectObj;

public class VelosCallBack implements Callback, java.io.Serializable {
	private ConnectObj connectObj;

	public VelosCallBack() {

	}

	public ConnectObj getConnectObj() {
		return connectObj;
	}

	public void setConnectObj(ConnectObj connObj) {
		this.connectObj = connObj;
	}

}
