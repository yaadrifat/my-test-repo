/**
 * PatientDemographicsLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.velos.DHTS;

public class PatientDemographicsLocator extends org.apache.axis.client.Service implements com.velos.DHTS.PatientDemographics {

    public PatientDemographicsLocator() {
    }


    public PatientDemographicsLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PatientDemographicsLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for PatientDemographicsSoap12
    private java.lang.String PatientDemographicsSoap12_address = "http://cdgtest.duhs.duke.edu:8082/PatientDemographics.asmx";

    public java.lang.String getPatientDemographicsSoap12Address() {
        return PatientDemographicsSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PatientDemographicsSoap12WSDDServiceName = "PatientDemographicsSoap12";

    public java.lang.String getPatientDemographicsSoap12WSDDServiceName() {
        return PatientDemographicsSoap12WSDDServiceName;
    }

    public void setPatientDemographicsSoap12WSDDServiceName(java.lang.String name) {
        PatientDemographicsSoap12WSDDServiceName = name;
    }

    public com.velos.DHTS.PatientDemographicsSoap getPatientDemographicsSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PatientDemographicsSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPatientDemographicsSoap12(endpoint);
    }

    public com.velos.DHTS.PatientDemographicsSoap getPatientDemographicsSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.velos.DHTS.PatientDemographicsSoap12Stub _stub = new com.velos.DHTS.PatientDemographicsSoap12Stub(portAddress, this);
            _stub.setPortName(getPatientDemographicsSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPatientDemographicsSoap12EndpointAddress(java.lang.String address) {
        PatientDemographicsSoap12_address = address;
    }


    // Use to get a proxy class for PatientDemographicsSoap
    private java.lang.String PatientDemographicsSoap_address = "http://cdgtest.duhs.duke.edu:8082/PatientDemographics.asmx";

    public java.lang.String getPatientDemographicsSoapAddress() {
        return PatientDemographicsSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PatientDemographicsSoapWSDDServiceName = "PatientDemographicsSoap";

    public java.lang.String getPatientDemographicsSoapWSDDServiceName() {
        return PatientDemographicsSoapWSDDServiceName;
    }

    public void setPatientDemographicsSoapWSDDServiceName(java.lang.String name) {
        PatientDemographicsSoapWSDDServiceName = name;
    }

    public com.velos.DHTS.PatientDemographicsSoap getPatientDemographicsSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PatientDemographicsSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPatientDemographicsSoap(endpoint);
    }

    public com.velos.DHTS.PatientDemographicsSoap getPatientDemographicsSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.velos.DHTS.PatientDemographicsSoapStub _stub = new com.velos.DHTS.PatientDemographicsSoapStub(portAddress, this);
            _stub.setPortName(getPatientDemographicsSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPatientDemographicsSoapEndpointAddress(java.lang.String address) {
        PatientDemographicsSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.velos.DHTS.PatientDemographicsSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.velos.DHTS.PatientDemographicsSoap12Stub _stub = new com.velos.DHTS.PatientDemographicsSoap12Stub(new java.net.URL(PatientDemographicsSoap12_address), this);
                _stub.setPortName(getPatientDemographicsSoap12WSDDServiceName());
                return _stub;
            }
            if (com.velos.DHTS.PatientDemographicsSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.velos.DHTS.PatientDemographicsSoapStub _stub = new com.velos.DHTS.PatientDemographicsSoapStub(new java.net.URL(PatientDemographicsSoap_address), this);
                _stub.setPortName(getPatientDemographicsSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("PatientDemographicsSoap12".equals(inputPortName)) {
            return getPatientDemographicsSoap12();
        }
        else if ("PatientDemographicsSoap".equals(inputPortName)) {
            return getPatientDemographicsSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "PatientDemographics");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "PatientDemographicsSoap12"));
            ports.add(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "PatientDemographicsSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("PatientDemographicsSoap12".equals(portName)) {
            setPatientDemographicsSoap12EndpointAddress(address);
        }
        else 
if ("PatientDemographicsSoap".equals(portName)) {
            setPatientDemographicsSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
