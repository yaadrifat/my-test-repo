/**
 * DemographicsRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.velos.DHTS;

public class DemographicsRequest  implements java.io.Serializable {
    private com.velos.DHTS.RequiredParms requiredInput;

    private com.velos.DHTS.DemographicsParms demographicsParms;

    public DemographicsRequest() {
    }

    public DemographicsRequest(
           com.velos.DHTS.RequiredParms requiredInput,
           com.velos.DHTS.DemographicsParms demographicsParms) {
           this.requiredInput = requiredInput;
           this.demographicsParms = demographicsParms;
    }


    /**
     * Gets the requiredInput value for this DemographicsRequest.
     * 
     * @return requiredInput
     */
    public com.velos.DHTS.RequiredParms getRequiredInput() {
        return requiredInput;
    }


    /**
     * Sets the requiredInput value for this DemographicsRequest.
     * 
     * @param requiredInput
     */
    public void setRequiredInput(com.velos.DHTS.RequiredParms requiredInput) {
        this.requiredInput = requiredInput;
    }


    /**
     * Gets the demographicsParms value for this DemographicsRequest.
     * 
     * @return demographicsParms
     */
    public com.velos.DHTS.DemographicsParms getDemographicsParms() {
        return demographicsParms;
    }


    /**
     * Sets the demographicsParms value for this DemographicsRequest.
     * 
     * @param demographicsParms
     */
    public void setDemographicsParms(com.velos.DHTS.DemographicsParms demographicsParms) {
        this.demographicsParms = demographicsParms;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DemographicsRequest)) return false;
        DemographicsRequest other = (DemographicsRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.requiredInput==null && other.getRequiredInput()==null) || 
             (this.requiredInput!=null &&
              this.requiredInput.equals(other.getRequiredInput()))) &&
            ((this.demographicsParms==null && other.getDemographicsParms()==null) || 
             (this.demographicsParms!=null &&
              this.demographicsParms.equals(other.getDemographicsParms())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRequiredInput() != null) {
            _hashCode += getRequiredInput().hashCode();
        }
        if (getDemographicsParms() != null) {
            _hashCode += getDemographicsParms().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DemographicsRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "DemographicsRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requiredInput");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "RequiredInput"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "RequiredParms"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("demographicsParms");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "DemographicsParms"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "DemographicsParms"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
