/**
 * StatusReturn.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.velos.DHTS;

public class StatusReturn  implements java.io.Serializable {
    private com.velos.DHTS.StatusEntry primaryStatus;

    private com.velos.DHTS.StatusEntry[] statusList;

    public StatusReturn() {
    }

    public StatusReturn(
           com.velos.DHTS.StatusEntry primaryStatus,
           com.velos.DHTS.StatusEntry[] statusList) {
           this.primaryStatus = primaryStatus;
           this.statusList = statusList;
    }


    /**
     * Gets the primaryStatus value for this StatusReturn.
     * 
     * @return primaryStatus
     */
    public com.velos.DHTS.StatusEntry getPrimaryStatus() {
        return primaryStatus;
    }


    /**
     * Sets the primaryStatus value for this StatusReturn.
     * 
     * @param primaryStatus
     */
    public void setPrimaryStatus(com.velos.DHTS.StatusEntry primaryStatus) {
        this.primaryStatus = primaryStatus;
    }


    /**
     * Gets the statusList value for this StatusReturn.
     * 
     * @return statusList
     */
    public com.velos.DHTS.StatusEntry[] getStatusList() {
        return statusList;
    }


    /**
     * Sets the statusList value for this StatusReturn.
     * 
     * @param statusList
     */
    public void setStatusList(com.velos.DHTS.StatusEntry[] statusList) {
        this.statusList = statusList;
    }

    public com.velos.DHTS.StatusEntry getStatusList(int i) {
        return this.statusList[i];
    }

    public void setStatusList(int i, com.velos.DHTS.StatusEntry _value) {
        this.statusList[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StatusReturn)) return false;
        StatusReturn other = (StatusReturn) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.primaryStatus==null && other.getPrimaryStatus()==null) || 
             (this.primaryStatus!=null &&
              this.primaryStatus.equals(other.getPrimaryStatus()))) &&
            ((this.statusList==null && other.getStatusList()==null) || 
             (this.statusList!=null &&
              java.util.Arrays.equals(this.statusList, other.getStatusList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPrimaryStatus() != null) {
            _hashCode += getPrimaryStatus().hashCode();
        }
        if (getStatusList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getStatusList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getStatusList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StatusReturn.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "StatusReturn"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("primaryStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "PrimaryStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "StatusEntry"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "StatusList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "StatusEntry"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
