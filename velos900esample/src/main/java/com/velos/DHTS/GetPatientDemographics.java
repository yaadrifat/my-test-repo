/**
 * GetPatientDemographics.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.velos.DHTS;

public class GetPatientDemographics  implements java.io.Serializable {
    private com.velos.DHTS.DemographicsRequest rInput;

    public GetPatientDemographics() {
    }

    public GetPatientDemographics(
           com.velos.DHTS.DemographicsRequest rInput) {
           this.rInput = rInput;
    }


    /**
     * Gets the rInput value for this GetPatientDemographics.
     * 
     * @return rInput
     */
    public com.velos.DHTS.DemographicsRequest getRInput() {
        return rInput;
    }


    /**
     * Sets the rInput value for this GetPatientDemographics.
     * 
     * @param rInput
     */
    public void setRInput(com.velos.DHTS.DemographicsRequest rInput) {
        this.rInput = rInput;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPatientDemographics)) return false;
        GetPatientDemographics other = (GetPatientDemographics) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.rInput==null && other.getRInput()==null) || 
             (this.rInput!=null &&
              this.rInput.equals(other.getRInput())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRInput() != null) {
            _hashCode += getRInput().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPatientDemographics.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", ">GetPatientDemographics"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RInput");
        elemField.setXmlName(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "rInput"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://DHTS.DukeHealth.org/", "DemographicsRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
