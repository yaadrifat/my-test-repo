/*
 * Classname			PortalAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					06/25/2003
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.portalAgent;

import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.common.PortalDao;
import com.velos.eres.business.portal.impl.PortalBean;


/**
 * Remote interface for PortalAgentBean session EJB
 * 
 * @author Manimaran
 * @version 1.0, 04/04/2007
 */
@Remote
public interface PortalAgentRObj {
    public PortalBean getPortalDetails(int portalId);

    public int setPortalDetails(PortalBean portal);
    
    public int updatePortal(PortalBean portal);
   
    public PortalDao getPortalValues(int accountId,int userId);
    
    public PortalDao getPortalValue(int portalId);
    
    public PortalDao insertPortalValues(int pid,String portalPatPop,String selIds,int usr,int user,String ipAdd);
    
    public PortalDao deletePortalPop(int portalId);
 // Overloaded for INF-18183 ::: Akshi
    public PortalDao deletePortalPop(int portalId,Hashtable<String, String> args);
    
 // Overloaded for INF-18183 ::: Akshi
    public void deletePortal(int portalId,Hashtable<String, String> args);

    public void deletePortal(int portalId);
   
    // Modified for PP-18306 AGodara
    public PortalDao getDesignPreview(int portalId,int patProtProtocolId);
    
    /** creates default logins for the portal according to patient population option*/
    public int createPatientLogins(int portalId, int creator, String ipAdd);
    
    /**validates portal bean
     * @return int 0 : validations passed
     * 			   -3 : Duplicate Portal Name
     * 			   -4 : Portal with 'Provide Create Patients Login button in 'Manage Logins'' already exists 		
     */
    public int validatePortal(PortalBean pb);
}
