/*
 * Classname			InvoiceAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					10/18/2005
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.invoiceAgent;

import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.eres.business.common.InvoiceDao;
import com.velos.eres.business.common.MileAchievedDao;
import com.velos.eres.business.invoice.InvoiceBean;



/**
 * Remote interface for PatFacilityAgentBean session EJB
 * 
 * @author Sonia Abrol
 * @version 1.0, 09/22/2005
 */
@Remote
public interface InvoiceAgentRObj {
    
	public InvoiceBean getInvoiceDetails(int pb);

    public int setInvoiceDetails(InvoiceBean pb);
    
  
    public int updateInvoice(InvoiceBean pb);

    public int  deleteInvoice(int Id);
    // Overloaded for INF-18183 ::: AGodara
    public int  deleteInvoice(int Id,Hashtable<String, String> auditInfo);
    
    public InvoiceDao getSavedInvoices(int studyId) ;
    
    public MileAchievedDao getAchievedMilestones(int studyId, String rangeFrom,String rangeTo, String milestoneReceivableStatus,String milestoneType, String site);
    
    public MileAchievedDao  getAchievedMilestonesForUser(int studyId, String rangeFrom,String rangeTo, String milestoneReceivableStatus,String milestoneType, String user);
    
    
    /**
     * Gets the total amount invoiced for an invoice
     * 
     * @param inv Invoice PK
     *            Description of the Parameter
     */
    public double getTotalAmountInvoiced(int inv); 
    
    public String getInvoiceNumber() ;//JM:
        
}
