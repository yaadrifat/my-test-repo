/**
 *@author Sonika Talwar 
 * date Aug 28, 2002
 *@version 1.0
 */

package com.velos.eres.service.specimenApndxAgent;

import java.util.Hashtable;

import javax.ejb.Remote;


import com.velos.eres.business.common.SpecimenApndxDao;
import com.velos.eres.business.specimenApndx.impl.SpecimenApndxBean;


@Remote
public interface SpecimenApndxAgentRObj {
    public int setSpecimenApndxDetails(SpecimenApndxBean pask);

    public SpecimenApndxBean getSpecimenApndxDetails(int specApndxId);
    
    public SpecimenApndxDao getSpecimenApndxUris(int pkSpecimen);

    public int updateSpecimenApndx(SpecimenApndxBean pask);
 
    public int removeSpecApndx(int specApndxId);
    
    // removeSpecApndx() Overloaded for INF-18183 ::: Raviesh
    public int removeSpecApndx(int specApndxId,Hashtable<String, String> args);
}