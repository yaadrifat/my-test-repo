/*
 * Classname : BrowserRows
 * 
 * Version information: 1.0
 *
 * Date 05/06/2002
 * 
 * Copyright notice: Velos Inc.
 */

package com.velos.eres.service.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;

import oracle.jdbc.driver.OracleTypes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.json.JSONWriter;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.FormResponseDao;
import com.velos.eres.business.common.SaveFormDao;
import com.velos.esch.business.common.CrfDao;


/**
 * BrowserRows class exposes methods to paginate result of SQLs and hold data
 * for required page.
 * 
 * @author Sonia Sahni
 * @verison 1.0 05/06/2002
 */
/*
 * Modified by Sonia, 10/26/04 getFormPageRows() to implement point n click
 * sorting
 */
public class BrowserRows extends CommonDAO implements java.io.Serializable {
    /**
     * the Columns
     */
    private ArrayList bColumns;
    
    private ArrayList bColTypes;
    
    private ArrayList resultSet;
    
    private String orderBy;
    
    private String orderType;
    

    /**
     * the Values
     */
    private Hashtable bValues;

    private boolean hasMore;

    private boolean hasPrevious;

    private long showPages;

    private long rowReturned;

    private long totRows;

    private long startPage;

    private long firstRec;

    private long lastRec;

    	// GETTER SETTER METHODS
    
    /**
	 * @return the orderBy
	 */
	public String getOrderBy() {
		return orderBy;
	}

	/**
	 * @param orderBy the orderBy to set
	 */
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	/**
	 * @return the orderType
	 */
	public String getOrderType() {
		return orderType;
	}

	/**
	 * @param orderType the orderType to set
	 */
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	/**
	 * @return the resultset
	 */
	public ArrayList getResultSet() {
		return resultSet;
	}

	/**
	 * @param resultset the resultset to set
	 */
	public void setResultSet(ArrayList result) {
		this.resultSet.add(result);
	}
	
    public ArrayList getBColumns() {
        return this.bColumns;
    }

    public void setBColumns(ArrayList bColumns) {
        this.bColumns = bColumns;
    }

    public void setBColumns(String bColumn) {
        this.bColumns.add(bColumn);
    }

    public Hashtable getBValues() {
        return this.bValues;
    }

    public void setBValues(Hashtable bValues) {
        this.bValues = bValues;
    }

    public boolean getHasMore() {
        return this.hasMore;
    }

    public void setHasMore(boolean hasMore) {
        this.hasMore = hasMore;
    }

    public boolean getHasPrevious() {
        return this.hasPrevious;
    }

    public void setHasPrevious(boolean hasPrevious) {
        this.hasPrevious = hasPrevious;
    }

    public long getShowPages() {
        return this.showPages;
    }

    public void setShowPages(long showPages) {
        this.showPages = showPages;
    }

    public long getRowReturned() {
        return this.rowReturned;
    }

    public void setTotalRows(long totRows) {
        this.totRows = totRows;
    }

    public long getFirstRec() {
        return this.firstRec;
    }

    public void setFirstRec(long firstRec) {
        this.firstRec = firstRec;
    }

    public long getLastRec() {
        return this.lastRec;
    }

    public void setLastRec(long lastRec) {
        this.lastRec = lastRec;
    }

    public long getTotalRows() {
        return this.totRows;
    }

    public void setRowReturned(long rowReturned) {
        this.rowReturned = rowReturned;
    }

    public long getStartPage() {
        return this.startPage;
    }

    public void setStartPage(long startPage) {
        this.startPage = startPage;
    }
    
    public ArrayList getBColTypes() {
		return bColTypes;
	}

	public void setBColTypes(ArrayList colTypes) {
		bColTypes = colTypes;
	}
	
	public void setBColTypes(String colType) {
		this.bColTypes.add(colType);
	}

    // END OF GETTER SETTER METHODS

    public BrowserRows() {
        bColumns = new ArrayList();
        bColTypes=new ArrayList();
        bValues = new Hashtable();
        resultSet= new ArrayList();
        orderBy="";
        orderType="";
    }

    // end of class

    
    public void getPageRows(long page, long rowsRequired, String bSql,
            long totalPages, String countSql, String orderBy, String order) {

        Rlog.debug("common", "get Page Rows");

        CallableStatement cstmt = null;

        ResultSet rs;
        ResultSetMetaData rm;
        Hashtable ht = new Hashtable();

        int rows = 0;
        int colCount = 0;
        String colName = "";
        int success = -1;
        long endRecord = 0;
        long totalRows = 0;
        long firstRecord = 0;
        long lastPageRecord = 0;
        long displayPages = 0;
        long calcStPg = 0;
        String browserSql = "";
        int orderPos = 0;

        Connection conn = null;
        try {
            browserSql = bSql;

            browserSql.toLowerCase();

            orderPos = (browserSql.toLowerCase()).lastIndexOf("order by");

            // for handling in fieldBrowserPg where the order is also made null
            if (order == null || (order.equals("null"))) {
                Rlog.debug("common", "INSDIE THE IF OF ORDSER --1");
                browserSql = browserSql;

            } else {

                if (orderPos == -1) {

                    Rlog.debug("common", "INSDIE THE IF OF ORDSER --2");
                    browserSql = browserSql + " order by " + orderBy + " "
                            + order;
                } else {
                    Rlog.debug("common", "INSDIE THE IF OF ORDSER --3");
                    
                    if (! StringUtil.isEmpty(orderBy))
                    {
                    	browserSql = browserSql + "," + orderBy + " " + order;
                    }	
                }

            }

            // change order by

            Rlog.debug("common", "SQL" + browserSql);
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_get_page_records(?,?,?,?,?,?)}");

            cstmt.setLong(1, page);
            cstmt.setLong(2, rowsRequired);
            cstmt.setString(3, browserSql);
            cstmt.setString(4, countSql);

            cstmt.registerOutParameter(5, OracleTypes.CURSOR);
            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);

            cstmt.execute();
            rs = (ResultSet) cstmt.getObject(5);
            totalRows = cstmt.getLong(6);
            setTotalRows(totalRows);

            if (rs != null) {
                rm = rs.getMetaData();
                colCount = rm.getColumnCount();

                if (colCount > 0) {
                    for (int i = 1; i <= colCount; i++) {
                        colName = rm.getColumnName(i);
                        setBColumns(StringUtil.stripScript(colName.toUpperCase()));
                    }
                }

                while (rs.next()) {
                    ArrayList ar = new ArrayList();
                    for (int i = 1; i <= colCount; i++) {
                        ar.add(StringUtil.stripScript(rs.getString(i)));
                    }
                    rows++;
                    ht.put(new Integer(rows), ar);
                }

                setBValues(ht);

                endRecord = (page * rowsRequired)
                        + ((totalPages - page) * rowsRequired);

                firstRecord = (page - 1) * rowsRequired + 1;

                if (firstRecord > (totalPages * rowsRequired)) {
                    setHasPrevious(true);
                } else {
                    setHasPrevious(false);
                }

                long temp = 0;
                if ((totalRows / rowsRequired) * rowsRequired == totalRows) {
                    temp = totalRows / rowsRequired;
                } else {
                    temp = totalRows / rowsRequired + 1;
                }

                if ((page / totalPages) * totalPages == page) {
                    if (((page / totalPages) * totalPages) <= temp) {
                        displayPages = totalPages;
                        if (((page / totalPages) * totalPages) == temp) {
                            setHasMore(false);
                        } else {
                            setHasMore(true);
                        }
                    } else {
                        displayPages = temp - (temp / totalPages) * totalPages;
                        setHasMore(false);
                    }
                } else {
                    if (((page / totalPages + 1) * totalPages) <= temp) {
                        displayPages = totalPages;
                        if (((page / totalPages + 1) * totalPages) == temp) {
                            setHasMore(false);
                        } else {
                            setHasMore(true);
                        }
                    } else {
                        displayPages = temp - (temp / totalPages) * totalPages;
                        setHasMore(false);
                    }
                }

                setShowPages(displayPages);
                setRowReturned(rows);

                // calculate the starting page no

                if (page % totalPages == 1) {
                    startPage = page;
                } else if (page % totalPages != 0) {
                    startPage = ((page / totalPages) * totalPages) + 1;
                } else {
                    startPage = ((page / totalPages - 1) * totalPages) + 1;
                }
                setStartPage(startPage);

            } else // resultset is null
            {
                setRowReturned(0);
                setShowPages(1);
                setHasPrevious(false);
                setHasMore(false);
                setStartPage(1);
            }

            // calculate first record # and last record # for dispalying on page

            if (page == 1) {
                setFirstRec(1);
            } else {
                setFirstRec(firstRecord);
            }

            lastPageRecord = page * rowsRequired;

            if (lastPageRecord > totalRows)
                lastPageRecord = totalRows;

            setLastRec(lastPageRecord);
          
        }// end of try

        catch (SQLException ex) {
            Rlog.fatal("common",
                    "BrowserRows:Exception in calling getPageRows : " + ex);
            setRowReturned(0);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        
        
    }
    public void getBrowserRows(long page, long rowsRequired, String bSql,
            long totalPages, String countSql, String orderBy, String order) 
    {
    	
    	getBrowserRows( page,  rowsRequired, bSql,
                 totalPages, countSql, orderBy,  order, true);
    	
    }
    public void getBrowserRows(long page, long rowsRequired, String bSql,
            long totalPages, String countSql, String orderBy, String order,boolean mask) {

        Rlog.debug("common", "get Page Rows");

        CallableStatement cstmt = null;

        ResultSet rs;
        ResultSetMetaData rm;
        Hashtable ht = new Hashtable();

        int rows = 0;
        int colCount = 0;
        String colName = "",colType="";
        int success = -1;
        long endRecord = 0;
        long totalRows = 0;
        long firstRecord = 0;
        long lastPageRecord = 0;
        long displayPages = 0;
        long calcStPg = 0;
        String browserSql = "";
        int orderPos = 0;
        boolean isMaskApplicable=false;

        Connection conn = null;
        try {
            browserSql = bSql;

            browserSql.toLowerCase();

            orderPos = (browserSql.toLowerCase()).lastIndexOf("order by");
            if (order.length()<=0) order="asc";
            if (orderBy.length()<=0) orderBy="1";
            // for handling in fieldBrowserPg where the order is also made null
            if (order == null || (order.equals("null"))) {
                Rlog.debug("common", "INSDIE THE IF OF ORDSER --1");
                browserSql = browserSql;

            } else {
            	
            	String orderByStr="";
				if ( (orderBy.toLowerCase().endsWith("_datesort")) || (orderBy.toLowerCase().endsWith("_numsort")) )
            	{
            		orderByStr=orderBy;
            	} else
            	{
            		orderByStr=" lower("+orderBy+") ";	
            	}
            		
            	
            	

                if (orderPos == -1) {

                    Rlog.debug("common", "INSDIE THE IF OF ORDSER --2");
                    browserSql = browserSql + " order by " + orderByStr + " "
                            + order + " ,rowcount " + order;
                } else {
                    Rlog.debug("common", "INSDIE THE IF OF ORDSER --3");
                    browserSql = browserSql + "," + orderByStr + " " + order + " ,rowcount " + order;
                }

            }
            	
            // set OrderBy
            this.setOrderBy(orderBy);
            this.setOrderType(order);
            
            Rlog.debug("common", "SQL" + browserSql);
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_get_next_records(?,?,?,?,?,?)}");

            cstmt.setLong(1, page);
            cstmt.setLong(2, rowsRequired);
            cstmt.setString(3, browserSql);
            cstmt.setString(4, countSql);

            cstmt.registerOutParameter(5, OracleTypes.CURSOR);
            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);

            cstmt.execute();
            rs = (ResultSet) cstmt.getObject(5);
            totalRows = cstmt.getLong(6);
            setTotalRows(totalRows);
            int maskRight=-1;
            ArrayList maskList=new ArrayList();
            if (rs != null) {
                rm = rs.getMetaData();
                colCount = rm.getColumnCount();
                
                if (colCount > 0) {
                    for (int i = 1; i <= colCount; i++) {
                        colName = rm.getColumnName(i);
                        colType=rm.getColumnTypeName(i);
                        setBColumns(colName.toUpperCase());
                        this.setBColTypes(colType.toUpperCase());
                        if  (colName.equals("RIGHT_MASK"))  isMaskApplicable=true;
                        if (colName.toUpperCase().startsWith("MASK"))
                        {
                        	maskList.add(i);
                        }
                        
                    }
                }
                
                
                while (rs.next()) {
                	if (mask)
                    {
                    	 
                    	String maskRightStr="0";
                    	if (isMaskApplicable)
                    		maskRightStr=StringUtil.trueValue(rs.getString("RIGHT_MASK"));
                    	if (maskRightStr.length()>0)
                    	{
                    		maskRight=EJBUtil.stringToNum(maskRightStr);
                         }
                    }
                	ArrayList ar = new ArrayList();
                      for (int i = 1; i <= colCount; i++) {
                    	  
                    	  colType=(String)this.bColTypes.get(i - 1);
                	     if ((maskRight<4) && (mask) && (maskList.indexOf(i)>=0)) {
                	     ar.add("****");
                	     }
                	     else{ 
                        ar.add(EJBUtil.typeCast(StringUtil.stripScript(rs.getString(i)),colType));
                	     }
                    }
                    rows++;
                    //ht.put(new Integer(rows), ar);
                    this.setResultSet(ar);
                    
                }

                //setBValues(ht);

                endRecord = (page * rowsRequired)
                        + ((totalPages - page) * rowsRequired);

                firstRecord = page;

                if (firstRecord > (totalPages * rowsRequired)) {
                    setHasPrevious(true);
                } else {
                    setHasPrevious(false);
                }

                long temp = 0;
                if ((totalRows / rowsRequired) * rowsRequired == totalRows) {
                    temp = totalRows / rowsRequired;
                } else {
                    temp = totalRows / rowsRequired + 1;
                }

                if ((page / totalPages) * totalPages == page) {
                    if (((page / totalPages) * totalPages) <= temp) {
                        displayPages = totalPages;
                        if (((page / totalPages) * totalPages) == temp) {
                            setHasMore(false);
                        } else {
                            setHasMore(true);
                        }
                    } else {
                        displayPages = temp - (temp / totalPages) * totalPages;
                        setHasMore(false);
                    }
                } else {
                    if (((page / totalPages + 1) * totalPages) <= temp) {
                        displayPages = totalPages;
                        if (((page / totalPages + 1) * totalPages) == temp) {
                            setHasMore(false);
                        } else {
                            setHasMore(true);
                        }
                    } else {
                        displayPages = temp - (temp / totalPages) * totalPages;
                        setHasMore(false);
                    }
                }

                setShowPages(displayPages);
                setRowReturned(rows);

                // calculate the starting page no

                if (page % totalPages == 1) {
                    startPage = page;
                } else if (page % totalPages != 0) {
                    startPage = ((page / totalPages) * totalPages) + 1;
                } else {
                    startPage = ((page / totalPages - 1) * totalPages) + 1;
                }
                setStartPage(startPage);

            } else // resultset is null
            {
                setRowReturned(0);
                setShowPages(1);
                setHasPrevious(false);
                setHasMore(false);
                setStartPage(1);
            }

            // calculate first record # and last record # for dispalying on page

            if (page == 1) {
                setFirstRec(1);
            } else {
                setFirstRec(firstRecord);
            }

            lastPageRecord = page * rowsRequired;

            if (lastPageRecord > totalRows)
                lastPageRecord = totalRows;

            setLastRec(lastPageRecord);
          
        }// end of try

        catch (SQLException ex) {
            Rlog.fatal("common",
                    "BrowserRows:Exception in calling getPageRows : " + ex);
            ex.printStackTrace();
            setRowReturned(0);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        
        
    }

    public String getBValues(int row, String colName) {
        int colSeq = 0;
        String colVal = "";
        ArrayList rowList = new ArrayList();

        if (bValues.isEmpty()) {
            return colVal;
        }

        // get the sequence of column name in col ArrayList
        colSeq = bColumns.indexOf(colName.toUpperCase());

        if (colSeq >= 0) {
            rowList = (ArrayList) bValues.get(new Integer(row));
            colVal = (String) rowList.get(colSeq);
        }

        return colVal;
    }
    
    public String getDataValue(int row, String colName) {
        int colSeq = 0;
        String colVal = "";
        ArrayList rowList = new ArrayList();

        if (this.getResultSet().isEmpty()) {
            return colVal;
        }

        // get the sequence of column name in col ArrayList
        colSeq = bColumns.indexOf(colName.toUpperCase());

        if (colSeq >= 0) {
            rowList = (ArrayList) this.getResultSet().get(new Integer(row));
            colVal = (String) rowList.get(colSeq);
        }

        return colVal;
    }
    
    
    
    /**
     * Method for Filled Form responses' pagination. Populates the BrowserRows
     * object with required set of rows.
     * 
     * @param studyPatAccId
     *            account id/ study id / patient depending on type of form
     * @param formId
     *            Form Id
     * @param userId
     *            Id of logged in user
     * @param linkFrom
     *            Flag to identify what the form is linked with. A-account,
     *            S-study, P-patient
     * @param formFillDt
     *            Date where clause
     * @param page
     *            The page number for which rows are required
     * @param rowsRequired
     *            Number of rows required per page
     * @param totalPages
     *            Total number of pages in which the entire data should be
     *            divided
     * @param orderByColName
     *            order by clause
     */
    public void getFormPageRows(int studyPatAccId, int formId, int userId,
            String linkFrom, String formFillDt,String responseId ,long page, long rowsRequired,
            long totalPages, String orderByColName, String orderByType) {

        Rlog.debug("common", "Inside getFormPageRows in BrowserRows");
        int rows = 0;
        int colCount = 0;
        String colName = "";
        int success = -1;
        long endRecord = 0;
        long totalRows = 0;
        long firstRecord = 0;
        long lastPageRecord = 0;
        long displayPages = 0;
        long calcStPg = 0;
        String browserSql = "";
        int orderPos = 0;
        
        ResultSet rs;
        ResultSetMetaData rm;
        CallableStatement cstmt = null;
        Hashtable ht = new Hashtable();
        SaveFormDao saveDao=new SaveFormDao();
        String formLinkCol="";
        String formLinkStr="";

       
        Connection conn = null;



        String dtWhereClause = "";
        String whereClause1 = "";
        Calendar formFillDtCal = Calendar.getInstance();
        String formPkStr = "";
        
        try {
            conn = getConnection();
            // VA: This should be called through Service Layer
            //But we can;t change too many files right now because
            //  this code (7.1.2.2) change need to be deployed to 7.0.1.7 code
            //Minimal files are are way to go but this need to be optimized
            formLinkCol=saveDao.getChildDetails(formId,linkFrom,EJBUtil.stringToNum(responseId));
            formLinkCol=(formLinkCol==null)?"":formLinkCol;

            if (linkFrom.equals("A")) {
                whereClause1 = " and acctforms_filldate ";
                formPkStr = "pk_acctforms = a.fk_object";
                if (formLinkCol.length()>0)
                formLinkStr=" AND EXISTS (SELECT * FROM ER_FORMSLINEAR WHERE fk_form="+ formId +"  AND fk_filledform=pk_acctforms AND " + formLinkCol.trim() +"="+responseId +") ";
            } else if ((linkFrom.equals("SA")) || (linkFrom.equals("S"))) {
                whereClause1 = " and studyforms_filldate ";
                formPkStr = "pk_studyforms = a.fk_object";
                if (formLinkCol.length()>0)
                    formLinkStr=" AND EXISTS (SELECT * FROM ER_FORMSLINEAR WHERE fk_form="+ formId +"  AND fk_filledform=pk_studyforms AND " + formLinkCol.trim() +"="+responseId +") ";
            } else if (linkFrom.equals("PA") || linkFrom.contains("SP")) {
                whereClause1 = " and patforms_filldate ";
                formPkStr = "pk_patforms = a.fk_object";
                if (formLinkCol.length()>0)
                    formLinkStr=" AND EXISTS (SELECT * FROM ER_FORMSLINEAR WHERE fk_form="+ formId +"  AND fk_filledform=pk_patforms AND " + formLinkCol.trim() +"="+responseId +") ";
            } else if (linkFrom.equals("C")) {
                whereClause1 = " and crfforms_filldate ";
                if (formLinkCol.length()>0)
                    formLinkStr=" AND EXISTS (SELECT * FROM ER_FORMSLINEAR WHERE fk_form="+ formId +"  AND fk_filledform=pk_crfforms AND " + formLinkCol.trim() +"="+responseId +") ";
            }
            

        if (!formFillDt.equals("ALL")) {
            dtWhereClause = whereClause1
                    + EJBUtil.getRelativeTime(formFillDtCal, formFillDt) + " ";
        }
        
        if (formLinkStr.length()>0)
        {
        	  dtWhereClause=dtWhereClause+ " " + formLinkStr;
        }

        /*
         * sending the seperator with dtwhere clause to seperate it in stored
         * procedure to use in union SQL after union SQL needs only the data
         * filter which is part before the seperator, while before union SQL
         * needs the whole part.
         */
        dtWhereClause = dtWhereClause
                + "[VELSEP]and (((pk_lf  IN  ( SELECT DISTINCT a.fk_object  FROM ER_OBJECTSHARE a WHERE"
                + " fk_objectshare_id="
                + userId
                + " AND ((a.object_number=4) OR (a.object_number=5) ) AND"
                + "((a.objectshare_type='U') OR (a.objectshare_type='G'))) OR e.creator="
                + userId + ")))";

        Rlog.debug("common",
                "Inside getFormPageRows in BrowserRows dtWhereClause "
                        + dtWhereClause);

        

            cstmt = conn
                    .prepareCall("{call PKG_LNKFORM.SP_GET_FORM_RECORDS(?,?,?,?,?,?,?,?,?,?)}");

            cstmt.setInt(1, studyPatAccId);
            cstmt.setInt(2, formId);
            cstmt.setString(3, linkFrom);
            cstmt.setString(4, dtWhereClause);
            cstmt.setLong(5, page);
            cstmt.setLong(6, rowsRequired);

            cstmt.setString(7, orderByColName);
            cstmt.setString(8, orderByType);

            cstmt.registerOutParameter(9, OracleTypes.CURSOR);
            cstmt.registerOutParameter(10, java.sql.Types.INTEGER);

            cstmt.execute();
            rs = (ResultSet) cstmt.getObject(9);
            totalRows = cstmt.getLong(10);
            setTotalRows(totalRows);

            if (rs != null) {
                rm = rs.getMetaData();
                colCount = rm.getColumnCount();

                if (colCount > 0) {
                    for (int i = 1; i <= colCount; i++) {
                        colName = rm.getColumnName(i);

                        // replace for &apos
                        colName = StringUtil.replace(colName, "&apos;", "'");
                        setBColumns(StringUtil.stripScript(colName.toUpperCase()));
                    }
                }

                while (rs.next()) {
                    ArrayList ar = new ArrayList();
                    for (int i = 1; i <= colCount; i++) {
                        ar.add( StringUtil.stripScript(StringUtil
                                .replace(rs.getString(i), "&apos", "'")) );
                    }
                    rows++;
                    ht.put(new Integer(rows), ar);
                }

                setBValues(ht);

                endRecord = (page * rowsRequired)
                        + ((totalPages - page) * rowsRequired);

                firstRecord = (page - 1) * rowsRequired + 1;

                if (firstRecord > (totalPages * rowsRequired)) {
                    setHasPrevious(true);
                } else {
                    setHasPrevious(false);
                }

                long temp = 0;
                if ((totalRows / rowsRequired) * rowsRequired == totalRows) {
                    temp = totalRows / rowsRequired;
                } else {
                    temp = totalRows / rowsRequired + 1;
                }

                if ((page / totalPages) * totalPages == page) {
                    if (((page / totalPages) * totalPages) <= temp) {
                        displayPages = totalPages;
                        if (((page / totalPages) * totalPages) == temp) {
                            setHasMore(false);
                        } else {
                            setHasMore(true);
                        }
                    } else {
                        displayPages = temp - (temp / totalPages) * totalPages;
                        setHasMore(false);
                    }
                } else {
                    if (((page / totalPages + 1) * totalPages) <= temp) {
                        displayPages = totalPages;
                        if (((page / totalPages + 1) * totalPages) == temp) {
                            setHasMore(false);
                        } else {
                            setHasMore(true);
                        }
                    } else {
                        displayPages = temp - (temp / totalPages) * totalPages;
                        setHasMore(false);
                    }
                }

                setShowPages(displayPages);
                setRowReturned(rows);

                // calculate the starting page no

                if (page % totalPages == 1) {
                    startPage = page;
                } else if (page % totalPages != 0) {
                    startPage = ((page / totalPages) * totalPages) + 1;
                } else {
                    startPage = ((page / totalPages - 1) * totalPages) + 1;
                }
                setStartPage(startPage);

            } else // resultset is null
            {
                setRowReturned(0);
                setShowPages(1);
                setHasPrevious(false);
                setHasMore(false);
                setStartPage(1);
            }

            // calculate first record # and last record # for dispalying on page

            if (page == 1) {
                setFirstRec(1);
            } else {
                setFirstRec(firstRecord);
            }

            lastPageRecord = page * rowsRequired;

            if (lastPageRecord > totalRows)
                lastPageRecord = totalRows;

            setLastRec(lastPageRecord);

        }// end of try

        catch (SQLException ex) {
            Rlog.fatal("common",
                    "BrowserRows:Exception in calling getFormPageRows : " + ex);
            setRowReturned(0);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Method to retrieve form specific data oonly using pagination. This method
     * serves data to all formdata browsers.
     * 
     * @param page
     *            page being retrieved
     * @param rowsRequired -
     *            Number of rows to retrieve for this request.
     * @param totalPages -
     *            Total number of pages.
     * @param orderBy -
     *            sorted by column
     * @param order -
     *            whether ASC or Desc
     * @param viewId -
     *            id for which data is retrieved.
     * @param search -
     */
    public void getPageRows(long page, long rowsRequired, long totalPages,
            String orderBy, String order, long viewId, String search) {

        Rlog.debug("common", "get Lookup Rows");

        CallableStatement cstmt = null;

        ResultSet rs;
        ResultSetMetaData rm;
        Hashtable ht = new Hashtable();

        int rows = 0;
        int colCount = 0;
        String colName = "";
        int colType = 0;
        int success = -1;
        long endRecord = 0;
        long totalRows = 0;
        long firstRecord = 0;
        long lastPageRecord = 0;
        long displayPages = 0;
        long calcStPg = 0;
        String browserSql = "";
        int orderPos = 0;

        Connection conn = null;
        try {
            /*
             * browserSql = bSql;
             * 
             * browserSql.toLowerCase();
             * 
             * orderPos = browserSql.lastIndexOf("order by");
             * 
             * if (orderPos == -1) { browserSql = browserSql + " order by " +
             * orderBy + " " + order; } else {
             * 
             * browserSql = browserSql +","+ orderBy + " " + order; }
             */

            // change order by
            Rlog.debug("common", "SQL" + browserSql);
            conn = getConnection();
            cstmt = conn.prepareCall("{call sp_get_lkp_records(?,?,?,?,?,?)}");

            cstmt.setLong(1, page);
            cstmt.setLong(2, rowsRequired);
            cstmt.setLong(3, viewId);
            cstmt.setString(4, search);
            cstmt.registerOutParameter(5, OracleTypes.CURSOR);
            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);

            cstmt.execute();
            rs = (ResultSet) cstmt.getObject(5);
            totalRows = cstmt.getLong(6);
            setTotalRows(totalRows);

            Rlog.debug("common", "got resultsin lookup search");

            if (rs != null) {
                rm = rs.getMetaData();
                colCount = rm.getColumnCount();
                Rlog
                        .debug("common", "cols in queryin kookup search"
                                + colCount);

                if (colCount > 0) {
                    for (int i = 1; i <= colCount; i++) {
                        colName = rm.getColumnName(i);
                        setBColumns(StringUtil.stripScript(colName.toUpperCase() ));

                    }
                }
                Date tempDate;
                String tempStr = "";
                while (rs.next()) {
                    ArrayList ar = new ArrayList();
                    for (int i = 1; i <= colCount; i++) {
                        colType = rm.getColumnType(i);

                        if (colType == 91) {
                            tempDate = (rs.getDate(i));
                            if (tempDate == null) {
                                tempStr = "";
                            } else {

                                tempStr = DateUtil.dateToString(tempDate);
                            }
                            ar.add(tempStr);
                        } else {

                            ar.add(StringUtil.stripScript(rs.getString(i)));
                        }
                    }
                    rows++;
                    ht.put(new Integer(rows), ar);
                }

                Rlog.debug("common", "rowsin query in lookup search" + rows);
                setBValues(ht);

                Rlog.debug("common", "HASHTABLE in lokup search"
                        + ht.toString());

                endRecord = (page * rowsRequired)
                        + ((totalPages - page) * rowsRequired);

                firstRecord = (page - 1) * rowsRequired + 1;

                Rlog.debug("common", "total rows in lookup search" + totalRows
                        + "end record" + endRecord);

                if (firstRecord > (totalPages * rowsRequired)) {
                    setHasPrevious(true);
                } else {
                    setHasPrevious(false);
                }

                long temp = 0;
                if ((totalRows / rowsRequired) * rowsRequired == totalRows) {
                    temp = totalRows / rowsRequired;
                } else {
                    temp = totalRows / rowsRequired + 1;
                }

                if ((page / totalPages) * totalPages == page) {
                    if (((page / totalPages) * totalPages) <= temp) {
                        displayPages = totalPages;
                        if (((page / totalPages) * totalPages) == temp) {
                            setHasMore(false);
                        } else {
                            setHasMore(true);
                        }
                    } else {
                        displayPages = temp - (temp / totalPages) * totalPages;
                        setHasMore(false);
                    }
                } else {
                    if (((page / totalPages + 1) * totalPages) <= temp) {
                        displayPages = totalPages;
                        if (((page / totalPages + 1) * totalPages) == temp) {
                            setHasMore(false);
                        } else {
                            setHasMore(true);
                        }
                    } else {
                        displayPages = temp - (temp / totalPages) * totalPages;
                        setHasMore(false);
                    }
                }

                setShowPages(displayPages);
                setRowReturned(rows);

                // calculate the starting page no

                if (page % totalPages == 1) {
                    startPage = page;
                } else if (page % totalPages != 0) {
                    startPage = ((page / totalPages) * totalPages) + 1;
                } else {
                    startPage = ((page / totalPages - 1) * totalPages) + 1;
                }
                setStartPage(startPage);

            } else // resultset is null
            {
                setRowReturned(0);
                setShowPages(1);
                setHasPrevious(false);
                setHasMore(false);
                setStartPage(1);
            }

            // calculate first record # and last record # for dispalying on page

            if (page == 1) {
                setFirstRec(1);
            } else {
                setFirstRec(firstRecord);
            }

            lastPageRecord = page * rowsRequired;

            if (lastPageRecord > totalRows)
                lastPageRecord = totalRows;

            setLastRec(lastPageRecord);

        }// end of try

        catch (SQLException ex) {
            Rlog.fatal("common",
                    "BrowserRows:Exception in calling getPageRows :forLookup Search "
                            + ex);
            setRowReturned(0);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // //////////////////

    public void getSchPageRows(long page, long rowsRequired, String bSql,
            long totalPages, String countSql, String orderBy, String order) {

        Rlog.debug("common", "get SCH Page Rows");

        CallableStatement cstmt = null;

        ResultSet rs;
        ResultSetMetaData rm;
        Hashtable ht = new Hashtable();

        int rows = 0;
        int colCount = 0;
        String colName = "";
        int success = -1;
        long endRecord = 0;
        long totalRows = 0;
        long firstRecord = 0;
        long lastPageRecord = 0;
        long displayPages = 0;
        long calcStPg = 0;
        String browserSql = "";
        int orderPos = 0;

        Connection conn = null;
        try {
            browserSql = bSql;

            browserSql.toLowerCase();

            orderPos = (browserSql.toLowerCase()).lastIndexOf("order by");

            // for handling in fieldBrowserPg where the order is also made null
            if (order == null || (order.equals("null"))) {
                Rlog.debug("common", "INSDIE THE IF OF ORDSER --1");
                browserSql = browserSql;

            } else {

                if (orderPos == -1) {

                    Rlog.debug("common", "INSDIE THE IF OF ORDSER --2");
                    browserSql = browserSql + " order by " + orderBy + " "
                            + order;
                } else {
                    Rlog.debug("common", "INSDIE THE IF OF ORDSER --3");
                    browserSql = browserSql + "," + orderBy + " " + order;
                }

            }

            // change order by

            Rlog.debug("common", "SQL" + browserSql);
            conn = getSchConnection();
            cstmt = conn
                    .prepareCall("{call sp_get_sch_page_records(?,?,?,?,?,?)}");

            cstmt.setLong(1, page);
            cstmt.setLong(2, rowsRequired);
            cstmt.setString(3, browserSql);
            cstmt.setString(4, countSql);

            cstmt.registerOutParameter(5, OracleTypes.CURSOR);
            cstmt.registerOutParameter(6, java.sql.Types.INTEGER);

            cstmt.execute();
            rs = (ResultSet) cstmt.getObject(5);
            totalRows = cstmt.getLong(6);
            setTotalRows(totalRows);

            if (rs != null) {
                rm = rs.getMetaData();
                colCount = rm.getColumnCount();

                if (colCount > 0) {
                    for (int i = 1; i <= colCount; i++) {
                        colName = rm.getColumnName(i);
                        setBColumns(StringUtil.stripScript(colName.toUpperCase()));
                    }
                }

                while (rs.next()) {
                    ArrayList ar = new ArrayList();
                    for (int i = 1; i <= colCount; i++) {
                        ar.add(StringUtil.stripScript(rs.getString(i)));
                    }
                    rows++;
                    ht.put(new Integer(rows), ar);
                }

                setBValues(ht);

                endRecord = (page * rowsRequired)
                        + ((totalPages - page) * rowsRequired);

                firstRecord = (page - 1) * rowsRequired + 1;

                if (firstRecord > (totalPages * rowsRequired)) {
                    setHasPrevious(true);
                } else {
                    setHasPrevious(false);
                }

                long temp = 0;
                if ((totalRows / rowsRequired) * rowsRequired == totalRows) {
                    temp = totalRows / rowsRequired;
                } else {
                    temp = totalRows / rowsRequired + 1;
                }

                if ((page / totalPages) * totalPages == page) {
                    if (((page / totalPages) * totalPages) <= temp) {
                        displayPages = totalPages;
                        if (((page / totalPages) * totalPages) == temp) {
                            setHasMore(false);
                        } else {
                            setHasMore(true);
                        }
                    } else {
                        displayPages = temp - (temp / totalPages) * totalPages;
                        setHasMore(false);
                    }
                } else {
                    if (((page / totalPages + 1) * totalPages) <= temp) {
                        displayPages = totalPages;
                        if (((page / totalPages + 1) * totalPages) == temp) {
                            setHasMore(false);
                        } else {
                            setHasMore(true);
                        }
                    } else {
                        displayPages = temp - (temp / totalPages) * totalPages;
                        setHasMore(false);
                    }
                }

                setShowPages(displayPages);
                setRowReturned(rows);

                // calculate the starting page no

                if (page % totalPages == 1) {
                    startPage = page;
                } else if (page % totalPages != 0) {
                    startPage = ((page / totalPages) * totalPages) + 1;
                } else {
                    startPage = ((page / totalPages - 1) * totalPages) + 1;
                }
                setStartPage(startPage);

            } else // resultset is null
            {
                setRowReturned(0);
                setShowPages(1);
                setHasPrevious(false);
                setHasMore(false);
                setStartPage(1);
            }

            // calculate first record # and last record # for dispalying on page

            if (page == 1) {
                setFirstRec(1);
            } else {
                setFirstRec(firstRecord);
            }

            lastPageRecord = page * rowsRequired;

            if (lastPageRecord > totalRows)
                lastPageRecord = totalRows;

            setLastRec(lastPageRecord);

        }// end of try

        catch (SQLException ex) {
            Rlog.fatal("common",
                    "BrowserRows:Exception in calling getPageRows : " + ex);
            setRowReturned(0);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // /////////////////////////////////////////////////

    // /////////////////SONIA SAHNI function used for patient schedule page
    // param schWhere - used for "filter criteria for crf data"
    // returns a hashtable of:
    // Hashtable OBJ of Event CRF Information
    // Hashtable OBJ of Visit CRF Information

    public Hashtable getSchedulePageRows(long page, long rowsRequired,
            String bSql, long totalPages, String countSql, String orderBy,
            String order, String schWhere) {

        CallableStatement cstmt = null;

        ResultSet rs;

        ResultSet rsCRF;

        ResultSetMetaData rm;
        Hashtable ht = new Hashtable();

        Hashtable htCRF = new Hashtable();
        Hashtable htVisitCRF = new Hashtable();

        Hashtable htReturn = new Hashtable();

        int rows = 0;
        int colCount = 0;
        String colName = "";
        int success = -1;
        long endRecord = 0;
        long totalRows = 0;
        long firstRecord = 0;
        long lastPageRecord = 0;
        long displayPages = 0;
        long calcStPg = 0;
        String browserSql = "";
        int orderPos = 0;

        int eventId = 0;
        int prevEventId = 0;
        int crfcount = 0;

        int prevVisit = 0;
        int visit = 0;
        String crfName = "";
        String crfType = "";

        Connection conn = null;
        try {
            browserSql = bSql;
            browserSql.toLowerCase();
            orderPos = (browserSql.toLowerCase()).lastIndexOf("order by");

            // for handling in fieldBrowserPg where the order is also made null
            if (order == null || (order.equals("null"))) {
                Rlog.debug("common", "INSDIE THE IF OF ORDSER --1");
                browserSql = browserSql;

            } else {

                if (orderPos == -1) {

                    Rlog.debug("common", "INSDIE THE IF OF ORDSER --2");
                    browserSql = browserSql + " order by " + orderBy + " "
                            + order;
                } else {
                    Rlog.debug("common", "INSDIE THE IF OF ORDSER --3");
                    browserSql = browserSql + "," + orderBy + " " + order;
                }

            }

            // change order by

            Rlog.debug("common", "SQL" + browserSql);
            conn = getSchConnection();
            cstmt = conn
                    .prepareCall("{call pkg_gensch.sp_get_schedule_page_records (?,?,?,?,?,?,?,?)}");

            cstmt.setLong(1, page);
            cstmt.setLong(2, rowsRequired);
            cstmt.setString(3, browserSql);
            cstmt.setString(4, countSql);

            cstmt.setString(5, schWhere);

            cstmt.registerOutParameter(6, OracleTypes.CURSOR);
            cstmt.registerOutParameter(7, java.sql.Types.INTEGER);
            cstmt.registerOutParameter(8, OracleTypes.CURSOR);

            cstmt.execute();
            rs = (ResultSet) cstmt.getObject(6);
            totalRows = cstmt.getLong(7);

            rsCRF = (ResultSet) cstmt.getObject(8);

            setTotalRows(totalRows);

            if (rs != null) {
                rm = rs.getMetaData();
                colCount = rm.getColumnCount();

                if (colCount > 0) {
                    for (int i = 1; i <= colCount; i++) {
                        colName = rm.getColumnName(i);
                        setBColumns(StringUtil.stripScript(colName.toUpperCase()));
                    }
                }

                while (rs.next()) {
                    ArrayList ar = new ArrayList();
                    for (int i = 1; i <= colCount; i++) {
                        ar.add(StringUtil.stripScript(rs.getString(i)));
                    }
                    rows++;
                    ht.put(new Integer(rows), ar);
                }

                setBValues(ht);

                endRecord = (page * rowsRequired)
                        + ((totalPages - page) * rowsRequired);

                firstRecord = (page - 1) * rowsRequired + 1;

                if (firstRecord > (totalPages * rowsRequired)) {
                    setHasPrevious(true);
                } else {
                    setHasPrevious(false);
                }

                long temp = 0;

                if ((totalRows / rowsRequired) * rowsRequired == totalRows) {
                    temp = totalRows / rowsRequired;
                } else {
                    temp = totalRows / rowsRequired + 1;
                }

                if ((page / totalPages) * totalPages == page) {
                    if (((page / totalPages) * totalPages) <= temp) {
                        displayPages = totalPages;
                        if (((page / totalPages) * totalPages) == temp) {
                            setHasMore(false);
                        } else {
                            setHasMore(true);
                        }
                    } else {
                        displayPages = temp - (temp / totalPages) * totalPages;
                        setHasMore(false);
                    }
                } else {
                    if (((page / totalPages + 1) * totalPages) <= temp) {
                        displayPages = totalPages;
                        if (((page / totalPages + 1) * totalPages) == temp) {
                            setHasMore(false);
                        } else {
                            setHasMore(true);
                        }
                    } else {
                        displayPages = temp - (temp / totalPages) * totalPages;
                        setHasMore(false);
                    }
                }

                setShowPages(displayPages);
                setRowReturned(rows);

                // calculate the starting page no

                if (page % totalPages == 1) {
                    startPage = page;
                } else if (page % totalPages != 0) {
                    startPage = ((page / totalPages) * totalPages) + 1;
                } else {
                    startPage = ((page / totalPages - 1) * totalPages) + 1;
                }
                setStartPage(startPage);

            } else // resultset is null
            {
                setRowReturned(0);
                setShowPages(1);
                setHasPrevious(false);
                setHasMore(false);
                setStartPage(1);
            }

            // calculate first record # and last record # for dispalying on page

            if (page == 1) {
                setFirstRec(1);
            } else {
                setFirstRec(firstRecord);
            }

            lastPageRecord = page * rowsRequired;

            if (lastPageRecord > totalRows)
                lastPageRecord = totalRows;

            setLastRec(lastPageRecord);

            // populate CRF DATA

            if (rsCRF != null) {
                CrfDao crfd = new CrfDao();
                ArrayList visitCRF = new ArrayList();
                ArrayList visitCRFTemp = new ArrayList();

                Rlog.debug("common", "Got CRF data");

                while (rsCRF.next()) {
                    eventId = rsCRF.getInt("event_id");

                    crfcount = crfcount + 1;

                    if (crfcount > 1 && prevEventId != eventId) {
                        htCRF.put(new Integer(prevEventId), crfd);
                        crfd = new CrfDao();

                    }

                   
                    crfd.setCrfIds(new Integer(0));

                    crfName = rsCRF.getString("crf_name");
                    crfType = rsCRF.getString("crftype");

                    crfd.setCrfNames(crfName);
                    crfd.setCrfLinkedFormIds(rsCRF.getString("fk_form"));
                    crfd.setCrfFormType(crfType);
                    
                    crfd.setCrfformEntryChars(rsCRF.getString("lf_entrychar"));
                    crfd.setCrfformSavedCounts(rsCRF.getString("response_count"));
                    crfd.setFormStatus(rsCRF.getString("form_stat"));
                    crfd.setLinkedFormRecordTypes(rsCRF.getString("form_record_type"));
                    crfd.setDisplayInSpecs(rsCRF.getString("disp_in_spec"));
                    
                    //modified by Sonia Abrol, use fk_visit instead of visit
                    
                    visit = rsCRF.getInt("fk_visit");

                    if (crfcount > 1 && prevVisit != visit) {

                        htVisitCRF.put(new Integer(prevVisit), visitCRF);
                        visitCRF = new ArrayList();

                    }

                    if (EJBUtil.isEmpty(crfName))
                        crfName = "";
                    visitCRF.add(crfName);
                    

                    prevEventId = eventId;
                    prevVisit = visit;
                }

                htCRF.put(new Integer(prevEventId), crfd);
                htVisitCRF.put(new Integer(prevVisit), visitCRF);

                Rlog.debug("common", "Got CRF data = last eventId"
                        + prevEventId);

            }

            // end of populate crf data

        }// end of try

        catch (Exception ex) {
            Rlog
                    .fatal(
                            "common",
                            "BrowserRows:Exception in calling getSchedulePageRows in BrowserRows in util : "
                                    + ex);
            setRowReturned(0);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

        htReturn.put("eventcrf", htCRF);
        htReturn.put("visitcrf", htVisitCRF);
        return htReturn;

    }
   
    // /////////end of class
    public String  toJSONString()
    {
    	String jsonStr="";
    	ArrayList columns=this.getBColumns();
    	ArrayList result= this.getResultSet();
    	ArrayList tempList;
    	 JSONStringer js = new JSONStringer();
    	 JSONWriter jsWriter = new JSONStringer();
    	 JSONObject jsonObj=new JSONObject();
    	 JSONObject jsonObjParent=new JSONObject();
    	 
    	 try {
    	 JSONArray jsonArray=new JSONArray();
    	  	
    	for (int i=0;i<result.size();i++)
    	{
    		tempList=(ArrayList)result.get(i);
    		jsonObj=new JSONObject();
    		for (int j=0;j<tempList.size();j++) 
    		{
    			jsonObj.put((String)columns.get(j),(String)tempList.get(j));
    		}
    		jsonArray.put(jsonObj);
    	}
    	
    	
    	//jsonObj=new JSONObject(getBrowserConfig());
    	jsonObjParent.put("resultSet", jsonArray);
    	jsonObjParent.put("rowsReturned",this.getRowReturned());
    	jsonObjParent.put("firstRecord",this.getFirstRec());
    	jsonObjParent.put("lastRecord",this.getLastRec());
    	jsonObjParent.put("totalRecords",this.getTotalRows());
    	jsonObjParent.put("orderBy",this.getOrderBy());
    	jsonObjParent.put("orderType",this.getOrderType());
    	
    	
    	
    	 	 } catch(JSONException e)
    	 {
    		 e.printStackTrace();
    	 }
    	 	 String tempStr="";
    	String returnStr=jsonObjParent.toString();
    	/*returnStr=returnStr.substring(0,returnStr.lastIndexOf("}")  ) + ","+getBrowserConfig()+"}";*/
    	
    	
    	return returnStr;
    	
    }

    public String  toExportString(String type,ArrayList expCols)
    {
    	String fileUrl="",label="",dataVal="";
    	int indx=-1;
    	ArrayList columns=this.getBColumns();
    	ArrayList result= this.getResultSet();
    	ArrayList tempList,colList=new ArrayList();
    	ArrayList colLabels=(ArrayList)expCols.get(0);
    	ArrayList colIds=(ArrayList) expCols.get(1);
    	
    	String rowData="",column="";
    	if (type==null) type="xls";
    	 
    	try {
    	ReportIO rio=new ReportIO();
    	rio.initializeWriter(type,"export_");
    	rio.write("<table width='100%' cellspacing='1' cellpadding='0' border='1'><tr>");
    	for (int i=0;i<columns.size();i++)
    	{
    	  	column=(String)columns.get(i);
    	  	indx=colIds.indexOf(column);
    	 if (indx>=0) {
    	  	label=(String)colLabels.get(indx);
    	    label=(label==null)?"":label;
    	}  else label="";
    	   if (label.length()==0) continue;
    	   colList.add(i);
    	 if (rowData.length()==0)  
    		 rowData="<th width='15%'>"+colLabels.get(indx)+"</th>";
    	 else
    		 rowData=rowData+"<th width='15%'>"+colLabels.get(indx)+"</th>";
    	 
    	}
    	rio.write(rowData+"</tr>");
    	
    	for (int i=0;i<result.size();i++)
    	{
    		rowData=""; 
    		dataVal="";
    		rio.write("<tr>");
    		tempList=(ArrayList)result.get(i);
    		for (int j=0;j<tempList.size();j++) 
    		{
    			if (colList.indexOf(j)<0) continue;
    			dataVal=(String)tempList.get(j);
    			dataVal=(dataVal==null)?"":dataVal;
    			if (dataVal.indexOf("[VELSEP]")>=0 )
    				dataVal=dataVal.replace("[VELSEP]", "");
    			if (rowData.length()>0) 
    				rowData=rowData+"<td>"+dataVal+"</td>";
    			else 
    				rowData="<td>"+dataVal+"</td>";
    		}
    		rio.write(rowData+"</tr>");
    	}
    	rio.write("</table>");
    	rio.deactivateWriter();
    	fileUrl=rio.getFileUrl();
    	} catch(Exception e)
    	{
    	}
    	return fileUrl;
    	
    	
    }

    ////
    
    /**
     * New Method for Filled Form responses' pagination. Populates the BrowserRows
     * object with required set of rows.
     * 
     * @param studyPatAccId
     *            account id/ study id / patient depending on type of form
     * @param formId
     *            Form Id
     * @param userId
     *            Id of logged in user
     * @param linkFrom
     *            Flag to identify what the form is linked with. A-account,
     *            S-study, P-patient
     * @param formFillDt
     *            Date where clause
     * @param page
     *            The page number for which rows are required
     * @param rowsRequired
     *            Number of rows required per page
     * @param totalPages
     *            Total number of pages in which the entire data should be
     *            divided
     * @param orderByColName
     *            order by clause
     */
    public void getFormPageRowsLinear(int studyPatAccId, int formId, int userId,
            String linkFrom, String formFillDt,String responseId ,long page, long rowsRequired,
            long totalPages, String orderByColName, String orderByType,String studyId,String specRecOnly,String patProtId,String formDispType) {

    			Hashtable htMoreParams = new Hashtable();
    			
    			String sqlString = "";
    			String countSql = "";
    			
    			htMoreParams.put("responseId",responseId)	;
    			htMoreParams.put("dateFilter",formFillDt)	;
    			//BK,FIXED BUG 7010,14/SEP/2011
    			if(!StringUtil.isEmpty(specRecOnly)){
    				htMoreParams.put("specRecOnly",specRecOnly)	;
    			} 
    			//Bug#8966 :Yogendra:18 April2012  
    			if(null!=patProtId){
    				htMoreParams.put("patProtId", patProtId);
    				htMoreParams.put("formDispType", formDispType);
    			}
        		Rlog.debug("common", "Inside getFormPageRows in BrowserRows");
        
        		FormResponseDao frDao = new FormResponseDao();
        		
        		sqlString = frDao.getFormResponsesSQL( studyPatAccId , formId , linkFrom, orderByColName,
        				orderByType, studyId, userId,  htMoreParams);

        		System.out.println("sqlString fo rgetFormPageRowsLinear" + sqlString);
        		
        		System.out.println("sqlString fo rgetFormPageRowsLinear" + orderByColName);
        		System.out.println("sqlString fo rgetFormPageRowsLinear" + orderByType);
        		
        		countSql = "select count(*) from (" +  sqlString +")";
        		
        		getPageRows( page,  rowsRequired, sqlString,
                        totalPages, countSql, "",  "");
           
        
    }

    //// end of class
}
