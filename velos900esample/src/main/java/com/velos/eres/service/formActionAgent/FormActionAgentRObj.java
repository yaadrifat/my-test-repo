

package com.velos.eres.service.formActionAgent;

import java.util.Hashtable;
import javax.ejb.Remote;
import com.velos.eres.business.formAction.impl.FormActionBean;


/**
 * Remote interface for AccountAgentBean session EJB
 * 
 * @author sajal
 * @version 1.0, 02/20/2001
 */
@Remote
public interface FormActionAgentRObj {
//    public FormActionBean getAccountDetails(Integer accId);
   
	public FormActionBean getFormActionDetails(Integer formActionId);

    public int setFormActionDetails(FormActionBean formAction);
    
    public int updateFormAction(FormActionBean fask);
//061008ML
    public int removeFormAction(int formActionId);
 
// end of   061008ML

    // Overloaded for INF-18183 ::: AGodara
    public int removeFormAction(int formActionId,Hashtable<String, String> auditInfo);
    
}
