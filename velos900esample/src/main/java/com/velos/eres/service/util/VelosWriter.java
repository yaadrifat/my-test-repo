package com.velos.eres.service.util;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

public class VelosWriter {
    private PrintWriter out;

    private FileWriter file;

    private String downloadStr;

    public VelosWriter() {
        downloadStr = "";
    }

    /**
     * Returns the value of downloadStr.
     */
    public String getDownloadStr() {
        return downloadStr;
    }

    /**
     * Sets the value of downloadStr.
     * 
     * @param downloadStr
     *            The value to assign downloadStr.
     */
    public void setDownloadStr(String downloadStr) {
        this.downloadStr = downloadStr;
    }

    public void GetFileWriter(String prefixName, String ext) {
        String completeFileName = "";
        String filePath = "";
        try {
        	//Added by IA 10.17.2006 Bug  2659
            com.aithent.file.uploadDownload.Configuration.readSettings("eres");

            com.aithent.file.uploadDownload.Configuration
                    .readUploadDownloadParam(
                            com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD
                                    + "fileUploadDownload.xml", null);

            filePath = com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER;

     	
    //        Configuration.readSettings();
    //        Configuration.readAppendixParam(Configuration.ERES_HOME
     //               + "eresearch.xml");
     //       filePath = Configuration.UPLOADFOLDER;
            completeFileName = NameFile(prefixName, ext);
            Rlog.debug("common",
                    "VelosWriter:getFileWriter:name of file created is"
                            + completeFileName);

            if (EJBUtil.getSystemType() == 0) {
                file = new FileWriter(filePath + "\\" + completeFileName, true);
            } else {
                file = new FileWriter(filePath + "//" + completeFileName, true);
            }
            out = new PrintWriter(file);
            
         //   downloadStr = Configuration.DOWNLOADSERVLET + "?file="
         //                + completeFileName;
            
            downloadStr = com.aithent.file.uploadDownload.Configuration.DOWNLOADSERVLET
            + "?file=" + completeFileName;
            
            setDownloadStr(downloadStr);

        } catch (IOException e)

        {

            Rlog.fatal("common", "Exception in Creating file " + e);

        }
    }

    private String NameFile(String prefixName, String ext) {
        
        return generateFileName(prefixName, ext,true);
    }

    /**
     * @withBracket to include square bracket in the name, default false
     * */
    public String generateFileName(String prefixName, String ext, boolean withBracket) {
        Calendar now = Calendar.getInstance();
        
        String extension = "";
        String startSquare="[";
        String endSquare="]";
        
        
        if (StringUtil.isEmpty(ext))
        {
        	
        	extension = "";
        }
        else
        {
        	extension=ext;
        }
        
        if (withBracket == false)
        {
        	startSquare="";
        	endSquare="";
        	
        }
        
        
        String fileName = prefixName + startSquare + now.get(now.DAY_OF_MONTH)
                + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)
                + now.get(now.HOUR_OF_DAY) + now.get(now.MINUTE)
                + now.get(now.SECOND) + now.get(now.MILLISECOND) + endSquare + extension;
        
        return fileName;
    }

    public void closeFile() {
        try {
            out.flush();
            out.close();
            file.flush();
            file.close();
        } catch (IOException e) {
            Rlog.fatal("coomon",
                    "VelosWriter Error,error in sutting the stream" + e);
        }

    }

    public void writeFile(String writeStr) {
        out.print(writeStr);
    }

}
