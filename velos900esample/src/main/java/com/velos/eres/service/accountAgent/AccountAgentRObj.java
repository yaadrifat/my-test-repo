/*
 * Classname			AccountAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					02/20/2001
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.accountAgent;

import javax.ejb.Remote;

import com.velos.eres.business.account.impl.AccountBean;
import com.velos.eres.business.common.AccountDao;

/**
 * Remote interface for AccountAgentBean session EJB
 * 
 * @author sajal
 * @version 1.0, 02/20/2001
 */
@Remote
public interface AccountAgentRObj {
    public AccountBean getAccountDetails(Integer accId);

    public int setAccountDetails(AccountBean account);

    public int updateAccount(AccountBean ask);

    public String getCreationDate(int accId);

    public int copyAccount(int accId);
    
    public int getPortalsCount(int accId);//JM:

    public AccountDao getAccounts();
    public AccountDao getLoginModuleDetails(int accId);
    public AccountDao getLoginModule(int loginModuleId);
}
