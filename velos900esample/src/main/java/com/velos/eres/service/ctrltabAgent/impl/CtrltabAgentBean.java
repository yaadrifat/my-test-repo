/*
 *  Classname			CtrltabserAgentBean.class
 *
 *  Version information
 *
 *  Date					05/18/2006
 *
 *  Copyright notice
 */

package com.velos.eres.service.ctrltabAgent.impl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.ctrltab.impl.CtrltabBean;
import com.velos.eres.service.ctrltabAgent.CtrltabAgentRObj;
import com.velos.eres.service.util.Rlog;

/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * 
 * @author Manimaran
 * @created May 18, 2006
 */

@Stateless
public class CtrltabAgentBean implements CtrltabAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * Looks up on the Ctrltab id parameter passed in and constructs and returns
     * a Ctrltab state holder. containing all the summary details of the u<br>
     * 
     * 
     * @param ctrltabId
     *            the Ctrltab id
     * @return The ctrltabDetails value
     */
    public CtrltabBean getCtrltabDetails(int ctrltabId) {

        CtrltabBean retrieved = null;

        try {

            retrieved = (CtrltabBean) em.find(CtrltabBean.class, new Integer(
                    ctrltabId));
        } catch (Exception e) {
            System.out.print("Error in getCtrltabDetails() in CtrltabAgentBean"
                    + e);
        }

        return retrieved;
    }

    /**
     * Sets a Ctrltab.
     * 
     * @param ctrltab
     *            The new ctrltabDetails value
     * @return void
     */

    public int setCtrltabDetails(CtrltabBean ctrltab) {
        try {
            CtrltabBean cd = new CtrltabBean();
            cd.updateCtrltab(ctrltab);
            em.persist(cd);
            return (cd.getCtabId());
        } catch (Exception e) {
            System.out
                    .print("Error in setCtrltabDetails() in CtrltabAgentBean "
                            + e);
        }
        return 0;
    }

    /**
     * Description of the Method
     * 
     * @param usk
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public int updateCtrltab(CtrltabBean usk) {

        CtrltabBean retrieved = null;

        int output;

        try {

            retrieved = (CtrltabBean) em.find(CtrltabBean.class, new Integer(
                    usk.getCtabId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateCtrltab(usk);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog.debug("ctrltab", "Error in updateCtrltab in CtrltabAgentBean"
                    + e);
            return -2;
        }
        return output;
    }


}
 
