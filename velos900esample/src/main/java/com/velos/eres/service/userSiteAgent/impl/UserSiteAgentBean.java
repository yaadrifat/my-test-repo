/*
 * Classname			UserSiteAgentBean
 * 
 * Version information 	1.0
 *
 * Date					04/23/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.service.userSiteAgent.impl;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.common.PatFacilityDao;
import com.velos.eres.business.common.UserSiteDao;
import com.velos.eres.business.userSite.impl.UserSiteBean;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * @author Sajal
 * @version 1.0 04/23/2003
 */

@Stateless
public class UserSiteAgentBean implements UserSiteAgentRObj {

    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * Calls getUserSiteDetails() on entity bean - UserSiteBean Looks up on the
     * User Site id parameter passed in and constructs and returns a User Site
     * state holder. containing all the summary details of the user site<br>
     * 
     * @param userSiteId
     *            the User Site id
     * @see UserSiteBean
     */
    public UserSiteBean getUserSiteDetails(int userSiteId) {
        UserSiteBean usb = null;

        try {
            usb = (UserSiteBean) em.find(UserSiteBean.class, new Integer(
                    userSiteId));

        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getUserSiteDetails() in UserSiteAgentBean" + e);
        }
        return usb;
    }

    /**
     * Creates a new a User Site. Calls setUserSiteDetails() on entity bean
     * UserSiteBean
     * 
     * @param a
     *            user site state holder containing user site attributes to be
     *            set.
     * 
     * @return void
     * @see UserSiteBean
     */

    public int setUserSiteDetails(UserSiteBean ussk) {
        try {
            em.merge(ussk);
            return (ussk.getUserSiteId());
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in setUserSiteDetails() in UserSiteAgentBean " + e);
        }
        return 0;
    }

    /**
     * Updates User Site Details. Calls updateUserSite() on entity bean
     * UserSiteBean
     * 
     * @param a
     *            user site state holder containing user site attributes to be
     *            set.
     * 
     * @return int 0 for successful ; -2 for exception
     * @see UserSiteBean
     */

    public int updateUserSite(UserSiteBean ussk) {

        UserSiteBean usb = null;
        int output;

        try {
            usb = (UserSiteBean) em.find(UserSiteBean.class, new Integer(ussk
                    .getUserSiteId()));

            if (usb == null) {
                return -2;
            }
            output = usb.updateUserSite(ussk);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in updateUserSite in UserSiteAgentBean" + e);
            return -2;
        }
        return output;
    }

    /**
     * Calls updateUserSite of UserSiteDao Overloaded method
     * 
     * @param Array
     *            of pkUserSites
     * @param Array
     *            of corresponding rights for each user site
     * @param userId
     * @param IP
     *            Address
     * @returns 0 if successful, -1 exception
     * @see UserSiteDao
     */

    public int updateUserSite(String[] pkUserSites, String[] rights, int usr,
            String ipAdd) {
        int ret = 0;
        UserSiteDao userSiteDao = new UserSiteDao();
        try {
            ret = userSiteDao.updateUserSite(pkUserSites, rights, usr, ipAdd);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in updateUserSite calling SP in UserSiteAgentBean"
                            + e);
            return -1;
        }
    }
    /**
     * Calls updateUserSite of UserSiteDao Overloaded method
     * 
     * @param Array
     *            of pkUserSites
     * @param Array
     *            of corresponding rights for each user site
     * @param userId
     * @param IP
     *            Address
     * @returns 0 if successful, -1 exception
     * @see UserSiteDao
     */

    public int updateUserSite(String[] pkUserSites, String[] rights, int usr, int userID,
            String ipAdd) {
        int ret = 0;
        UserSiteDao userSiteDao = new UserSiteDao();
        try {
            ret = userSiteDao.updateUserSite(pkUserSites, rights, usr,userID, ipAdd);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in updateUserSite calling SP in UserSiteAgentBean"
                            + e);
            return -1;
        }
    }

    /**
     * Calls getUserSiteTree of UserSiteDao
     * 
     * @param Account
     *            Id
     * @param userId
     * @returns UserSiteDao
     * @see UserSiteDao
     */

    public UserSiteDao getUserSiteTree(int accountId, int loginUser) {
        UserSiteDao userSiteDao = new UserSiteDao();
        try {
            userSiteDao.getUserSiteTree(accountId, loginUser);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getUserSiteTree in UserSiteAgentBean" + e);
        }
        return userSiteDao;
    }

    /**
     * Get organizations with view access for an account user, calls
     * getSitesWithViewRight of Session Bean
     * 
     * @param Account
     *            Id
     * @param userId
     * @returns UserSiteDao
     * @see UserSiteDao
     */

    public UserSiteDao getSitesWithViewRight(int accId, int userId) {
        UserSiteDao userSiteDao = new UserSiteDao();
        try {
            userSiteDao.getSitesWithViewRight(accId, userId);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getSitesWithViewRight in UserSiteAgentBean" + e);
        }
        return userSiteDao;
    }

    /**
     * Get organizations with new access for an account user, calls
     * getSitesWithNewRight of Session Bean
     * 
     * @param Account
     *            Id
     * @param userId
     * @returns UserSiteDao
     * @see UserSiteDao
     */

    public UserSiteDao getSitesWithNewRight(int accId, int userId) {
        UserSiteDao userSiteDao = new UserSiteDao();
        try {
            userSiteDao.getSitesWithNewRight(accId, userId);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getSitesWithNewRight in UserSiteAgentBean" + e);
        }
        return userSiteDao;
    }

    /**
     * Calls createUserSiteData() on UserSiteDao * to create er_usersite data
     * for all the organizations(sites) for user's account * The default site
     * gets rights same as user's default group's Manage Patients rights *
     * Author: Sonia Sahni 12th May 2003 *
     * 
     * @param user :
     *            Pk_user *
     * @param account :
     *            pk of user account *
     * @param def_group :
     *            pk of user default group *
     * @param old_site :
     *            pk of previous user site *
     * @param def_site :
     *            pk of user site *
     * @param creator :
     *            pk of creator (fk_user) *
     * @param ip :
     *            ip add of client machine *
     * @returns : 0 for successful update, -1 for error
     */

    public int createUserSiteData(int user, int account, int def_group,
            int old_site, int def_site, int creator, String ip, String mode) {

        UserSiteDao userSiteDao = new UserSiteDao();
        int ret = 0;

        try {
            ret = userSiteDao.createUserSiteData(user, account, def_group,
                    old_site, def_site, creator, ip, mode);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in createUserSiteData in UserSiteAgentBean" + e);
            return -1;
        }
        return ret;

    }

    /**
     * Get user site right
     * 
     * @param userId
     * @param siteId
     * @return userSite Right
     * @see UserSiteHome
     */

    public int getRightForUserSite(int userId, int siteId) {

        int output = 0;
        try {

            Query query = em.createNamedQuery("findUserSiteRight");
            query.setParameter("fkuser", userId);
            query.setParameter("fksite", siteId);

            ArrayList list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                UserSiteBean sBean = (UserSiteBean) list.get(0);
                output = StringUtil.stringToNum(sBean.getUserSiteRight());
                // SiteStateKeeper stkeep=sBean.getSiteStateKeeper();
                // Rlog.debug("site","Site"+stkeep);

            }
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getRightForUserSite in UserSiteAgentBean" + e);
            return output;
        }
        return output;
    }

    /**
     * Get all organizations with view access for an account user and rights
     * given in any study, calls getSitesWithViewRight of Session Bean
     * 
     * @param Account
     *            Id
     * @param userId
     * @returns UserSiteDao
     * @see UserSiteDao
     */

    public UserSiteDao getAllSitesWithViewRight(int accId, int userId) {
        UserSiteDao userSiteDao = new UserSiteDao();
        try {
            userSiteDao.getAllSitesWithViewRight(accId, userId);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getAllSitesWithViewRight in UserSiteAgentBean"
                            + e);
        }
        return userSiteDao;
    }

    public UserSiteDao getSitesWithRightForStudy(int studyId, int accId,
            int userId) {
        UserSiteDao userSiteDao = new UserSiteDao();
        try {
            userSiteDao.getSitesWithRightForStudy(studyId, accId, userId);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getSitesWithRightForStudy in UserSiteAgentBean"
                            + e);
        }
        return userSiteDao;
    }

    public UserSiteDao getSitesToEnrollPat(int studyId, int accId, int userId) {
        UserSiteDao userSiteDao = new UserSiteDao();
        try {
            userSiteDao.getSitesToEnrollPat(studyId, accId, userId);
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getSitesWithRightForStudy in UserSiteAgentBean"
                            + e);
        }
        return userSiteDao;
    }
   
    /** Returns the maximum right the user has on any of patient facilities. User's user site access rights are also
     * checked for all the patient facilities*/
    public int getUserPatientFacilityRight(int user , int perPk) {

        PatFacilityDao patfacDao = new PatFacilityDao();
        int ret = 0;

        try {
            ret = patfacDao.getUserPatientFacilityRight(user, perPk);
            
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Exception in getUserPatientFacilityRight in UserSiteAgentBean" + e);
            return -1;
        }
        return ret;

    }

    /**
     *  Function to get user's right in  patient's registering sites for a given study patient enrollments
     *  
     * @param int
     *            userId - User Id
     * @param int
     *            patprot -patprot id
     */
    
    public int getMaxRightForStudyPatient(int user, int pat, int study) 
    {
    	int right = 0;
    	UserSiteDao userSiteDao = new UserSiteDao();
        try {
        		right = userSiteDao.getMaxRightForStudyPatient(user, pat,study);
            
        } catch (Exception e) {
            Rlog.fatal("userSite",
                    "Error in getMaxRightForStudyPatientin UserSiteAgentBean"
                            + e);
        }
        return right;
    	
    }
    
    /**JM: 032406 function added 
     * Function to get site names in account
     * 
     * @param int
     * 			accid - Account Id 
     * 
     */
    
    public UserSiteDao getAllSitesInAccount(int accId){
    	
            UserSiteDao userSiteDao = new UserSiteDao();
            try {
                userSiteDao.getAllSitesInAccount(accId);
            } catch (Exception e) {
                Rlog.fatal("userSite",
                        "Error in getAllSitesInAccount in UserSiteAgentBean"
                                + e);
            }
            return userSiteDao;
        }

}// end of class
