/*
 *  Classname : CommonAgentBean
 *
 *  Version information: 1.0
 *
 *  Date: 05/04/2004
 *
 *  Copyright notice: Velos, Inc
 *
 *  Author: Vishal Abrol
 */
package com.velos.eres.service.commonAgent.impl;

/*
 * Import Statements
 */
import java.util.ArrayList;

import javax.ejb.Stateless;

import com.velos.eres.business.common.CommonDAO;
import com.velos.eres.business.common.AccountDao;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.SasExportDao;
import com.velos.eres.business.common.SettingsDao;
import com.velos.eres.business.common.SiteDao;
import com.velos.eres.business.common.StudyDao;
import com.velos.eres.business.common.UserDao;
import com.velos.eres.service.commonAgent.CommonAgentRObj;
import com.velos.eres.service.util.Rlog;

/*
 * End of Import Statements
 */
/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * commonAgentBean.<br>
 * <br>
 * 
 * 
 * @author Vishal Abrol
 * @created March 18, 2005
 * @see commonAgent
 * @version 1.0 05/04/2004
 * @ejbHome CommonAgentHome
 * @ejbRemote CommonAgentRObj
 */
@Stateless
public class CommonAgentBean implements CommonAgentRObj {
    /**
     * 
     */
    private static final long serialVersionUID = 5257064021684399088L;

    /**
     * Gets the sasHandle attribute of the CommonAgentBean object
     * 
     * @param type
     *            Description of the Parameter
     * @param id
     *            Description of the Parameter
     * @return The sasHandle value
     */
    public SasExportDao getSasHandle(String type, int id) {

        try {
            SasExportDao sasDao = new SasExportDao(type, id);
            Rlog.debug("common", "SasExportDao in commonAgentBean:getSasHandle"
                    + sasDao);
            return sasDao;
        } catch (Exception e) {
            Rlog.fatal("common",
                    "Exception In getSasHandle(String,int) in commonagentbean"
                            + e);
            return null;
        }

    }
    
    /**
     * Gets the CommonnDaon object
     * 
     * @param type
     *            Description of the Parameter
     * @param id
     *            Description of the Parameter
     * @return The sasHandle value
     */
    public CommonDAO getCommonHandle() {

        try {
            CommonDAO commonDao = new CommonDAO();
            return commonDao;
        } catch (Exception e) {
            Rlog.fatal("common",
                    "Exception In getCommonHandle() in commonagentbean"
                            + e);
            e.printStackTrace();
            return null;
        }

    }

    /**
     * Return an empty settingsDao Instance
     * 
     * @return SettingsDao Object
     */
    public SettingsDao getSettingsInstance() {
        return new SettingsDao();
    }
	//Added by Gopu for July-August Enhancement (#U2)
	/**
     * Return an empty settingsDao Instance
     * 
     * @return SettingsDao Object
     */
    public int updateSettingsWithoutPK(SettingsDao settingsDao) {
		return settingsDao.updateSettingsWithoutPK();
    }

    public int insertSettings(SettingsDao settingsDao) {
		return settingsDao.insertSettings();
    }





    /**
     * Populates and return the SettingDao object with requested data
     * 
     * @param modnum
     *            Module number
     * @param modname
     *            Module name
     * @param keyword
     *            Keyword to retrieve the value for
     * @return SettingDao Return populated DAO with requested data,null if error
     */
    public SettingsDao retrieveSettings(int modnum, int modname, String keyword) {

        try {
            SettingsDao settingDao = new SettingsDao();
            settingDao.retrieveSettings(modnum, modname, keyword);
            Rlog.debug("common",
                    "SettingsDao in commonAgentBean:reTrieveSettings"
                            + settingDao);
            return settingDao;
        } catch (Exception e) {
            Rlog.fatal("common",
                    "Exception In retrieveSettings(modnum,modname,keyword) in commonagentbean"
                            + e);
            return null;
        }

    }

    /**
     * Populates and return the SettingDao object with requested data
     * 
     * @param modnum
     *            Module number
     * @param modname
     *            Module name
     * @param keyword
     *            ArrayList of the keywords
     * @return SettingDao Return populated DAO with requested data,null if error
     */
    public SettingsDao retrieveSettings(int modnum, int modname,
            ArrayList keyword) {

        try {
            SettingsDao settingDao = new SettingsDao();
            settingDao.retrieveSettings(modnum, modname, keyword);
            Rlog.debug("common",
                    "SettingsDao in commonAgentBean:reTrieveSettings"
                            + settingDao);
            return settingDao;
        } catch (Exception e) {
            Rlog.fatal("common",
                    "Exception In retrieveSettings(modnum,modname,keyword) in commonagentbean"
                            + e);
            return null;
        }

    }

    /**
     * Populates and return the SettingDao object with requested data
     * 
     * @param modnum
     *            Module number
     * @param modname
     *            Module name
     * @return SettingDao Return populated DAO with requested data,null if error
     */
    public SettingsDao retrieveSettings(int modnum, int modname) {

        try {
            SettingsDao settingDao = new SettingsDao();
            settingDao.retrieveSettings(modnum, modname);
            Rlog.debug("common",
                    "SettingsDao in commonAgentBean:reTrieveSettings"
                            + settingDao);
            return settingDao;
        } catch (Exception e) {
            Rlog.fatal("common",
                    "Exception In retrieveSettings(modnum,modname,keyword) in commonagentbean"
                            + e);
            return null;
        }

    }

    /**
     * Purge the setting based on Velos Internal Id
     * 
     * @param purgeId
     *            Internal Id of the setting to delete
     * 
     */
    public int purgeSettings(int purgeId) {
        int ret = 0;
        try {
            SettingsDao settingDao = new SettingsDao();
            ret = settingDao.purgeSettings(purgeId);
            Rlog.debug("common", "SettingsDao in commonAgentBean:purgeSettings"
                    + settingDao);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("common",
                    "Exception In purgeSettings() in commonagentbean" + e);
            return -1;
        }

    }

    /**
     * Purge the setting based on Velos Internal Id's
     * 
     * @param purgeId
     *            ArrayList of Internal Id's of the setting to delete
     * 
     */
    public int purgeSettings(ArrayList purgeList) {
        int ret = 0;
        try {
            SettingsDao settingDao = new SettingsDao();
            ret = settingDao.purgeSettings(purgeList);
            Rlog.debug("common", "SettingsDao in commonAgentBean:purgeSettings"
                    + settingDao);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("common",
                    "Exception In purgeSettings() in commonagentbean" + e);
            return -1;
        }

    }

    /**
     * Remove the records from settings based on the combination of module
     * number,module name,keyword and value
     * 
     * @param modnum
     *            ID for that record in specified module
     * @param modname
     *            Module name for which rows are deleted
     * @param keyword
     *            Keyword
     * @param value
     *            Value of keyword
     * @return return the status, -1 if an error
     */
    public int purgeSettings(String modnum, String modname, String keyword,
            String value) {
        int ret = 0;
        try {
            SettingsDao settingDao = new SettingsDao();
            ret = settingDao.purgeSettings(modnum, modname, keyword, value);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("common",
                    "Exception In purgeSettings() in commonagentbean" + e);
            return -1;
        }

    }

    /**
     * Remove the records from settings based on the combination of module
     * number,module name and keyword
     * 
     * @param modnum
     *            ID for that record in specified module
     * @param modname
     *            Module name for which rows are deleted
     * @param keyword
     *            Keyword
     * @return return the status, -1 if an error
     */
    public int purgeSettings(String modnum, String modname, String keyword) {
        int ret = 0;
        try {
            SettingsDao settingDao = new SettingsDao();
            ret = settingDao.purgeSettings(modnum, modname, keyword);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("common",
                    "Exception In purgeSettings() in commonagentbean" + e);
            return -1;
        }

    }

    /**
     * Remove the records from settings based on the combination of module
     * number,module name and keyword
     * 
     * @param modnum
     *            ID for that record in specified module
     * @param modname
     *            Module name for which rows are deleted
     * @param keywordList
     *            ArrayList of keywords
     * @return return the status, -1 if an error
     */
    public int purgeSettings(String modnum, String modname,
            ArrayList keywordList) {
        int ret = 0;
        try {
            SettingsDao settingDao = new SettingsDao();
            ret = settingDao.purgeSettings(modnum, modname, keywordList);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("common",
                    "Exception In purgeSettings() in commonagentbean" + e);
            return -1;
        }

    }

    // ///////////////////////////////

    /**
     * Returns the Account Primary key
     * 
     * @param siteCode
     *            The unique site code for an account
     * @return the Account Primary key
     */

    public int getAccountPK(String siteCode) {
        AccountDao ad = new AccountDao();
        int id = 0;

        id = ad.getAccountPK(siteCode);

        return id;

    }

    /**
     * Gets the site PK. If you are on ASP type deployment, it is advisable to
     * use getSitePK(String siteId, int pkAccount )
     * 
     * @param siteId
     *            Unique Site Id
     * @return Site Primary Key
     */
    public int getSitePK(String siteId) {
        int id = 0;
        id = SiteDao.getSitePK(siteId);
        return id;

    }

    /**
     * Gets the site PK
     * 
     * @param siteId
     *            Unique Site Id
     * @param pkAccount
     *            Primary Key of site account
     * @return Site Primary Key
     */

    public int getSitePK(String siteId, int pkAccount) {
        int id = 0;
        id = SiteDao.getSitePK(siteId, pkAccount);
        return id;

    }

    /**
     * Gets the user PK. Please note: For Non System users, it is advisable to
     * store their email addresses in eResearch. User email is a good parameter
     * to find a user.
     * 
     * @param firstName
     *            User's first Name
     * @param firstName
     *            User's last Name
     * @param userEmail
     *            User's email address
     * @return user Primary Key
     */

    public int getUserPK(String firstName, String lastName, String userEmail) {
        int id = 0;
        id = UserDao.getUserPK(firstName, lastName, userEmail);
        return id;

    }

    /**
     * Gets the user PK on the basis of first name and last name. It is
     * advisable to use getUserPK(String firstName, String lastName, String
     * userEmail ) as there is a chance that system may have more than one user
     * with same first name and last name
     * 
     * @param firstName
     *            User's first Name
     * @param firstName
     *            User's last Name
     * @return user Primary Key
     */

    public int getUserPK(String firstName, String lastName) {
        int id = 0;
        id = UserDao.getUserPK(firstName, lastName);
        return id;

    }

    /**
     * Returns the internal Code List Primary key corresponding to specific code
     * type,code subtype,and code description combination
     * 
     * @param type
     *            maps to Code_type in er_codelst
     * @param subType
     *            maps to Code_subtyp in er_codelst
     * @param description
     *            maps to code_Desc in er_codelst
     * @return Primary Key of the code list value
     */
    public int getCodeListPK(String type, String subType, String description) {
        int id = 0;
        CodeDao cd = new CodeDao();
        id = cd.getCodeId(type, subType, description);

        return id;

    }

    /**
     * Returns the internal Code List Primary key corresponding to specific code
     * type and code subtype combination
     * 
     * @param type
     *            maps to Code_type in er_codelst
     * @param subType
     *            maps to Code_subtyp in er_codelst
     * @return Primary Key of the code list value
     */
    public int getCodeListPK(String type, String subType) {
        int id = 0;
        CodeDao cd = new CodeDao();
        id = cd.getCodeId(type, subType);

        return id;

    }

    /**
     * Returns the internal Code List Primary key corresponding to specific code
     * type and code descriptioncombination
     * 
     * @param type
     *            Code Type, maps to CodeLst_type in er_codelst
     * @param desc
     *            Code Description, maps to CodeLst_desc in er_codelst
     * @return Primary Key of the code list value
     */
    public int getCodeListPKFromDesc(String type, String desc) {
        int id = 0;
        CodeDao cd = new CodeDao();
        id = cd.getCodeIdFromDesc(type, desc);

        return id;

    }

    // ///////
    /**
     * Gets Study PK
     * 
     * @param study
     *            Number The Study Number
     * @param Primary
     *            Key of study account
     * @return Study Primary Key
     */

    public int getStudyPK(String studyNum, int pkAccount) {
        int id = 0;

        id = StudyDao.getStudyPK(studyNum, pkAccount);
        return id;

    }

    /**
     * Gets study Protocol PK
     * 
     * @param studyPK
     *            The Study Primary Key
     * @param CalendarName
     *            The Calendar Name
     * @return Protocol Primary Key
     */

    public int getProtocolPK(int studyPK, String CalendarName) {
        int id = 0;
        StudyDao sd = new StudyDao();

        id = sd.getProtocolPK(studyPK, CalendarName);

        return id;

    }

    // ///////end of class
}
