/*
 * Classname : StudySiteAgentBean
 *
 * Version information: 1.0
 *
 * Date: 10/27/2004
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.service.studySiteAgent.impl;

/* Import Statements */

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.common.StudySiteDao;
import com.velos.eres.business.studySite.impl.StudySiteBean;
import com.velos.eres.service.studySiteAgent.StudySiteAgentRObj;
import com.velos.eres.service.util.Rlog;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import java.util.Hashtable;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.business.audit.impl.AuditBean;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * StudySiteAgentBean.<br>
 * <br>
 *
 * @author Anu Khanna
 * @see studyAgentAgentBean
 * @ejbHome studySiteAgentHome
 * @ejbRemote studySiteAgentRObj
 */

@Stateless
public class StudySiteAgentBean implements StudySiteAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
	@Resource private SessionContext context;

    /**
     *
     * Looks up on the id parameter passed in and constructs and returns a
     * studySite state keeper. containing all the details of the studySite <br>
     *
     * @param study
     *            Site Id
     */

    public StudySiteBean getStudySiteDetails(int studySiteId) {
        StudySiteBean retrieved = null;

        try {
            retrieved = (StudySiteBean) em.find(StudySiteBean.class,
                    new Integer(studySiteId));

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Exception in StudySiteStateKeeper() in StudySiteAgentBean"
                            + e);
        }

        return retrieved;

    }

    /**
     * Creates studySIte record.
     *
     * @param a
     *            State Keeper containing the studySiteId attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */

    public int setStudySiteDetails(StudySiteBean ssk) {
        try {
            StudySiteBean stdSiteBean=new StudySiteBean();
            stdSiteBean.updateStudySite(ssk);
             em.persist(stdSiteBean);
             return stdSiteBean.getStudySiteId();
        } catch (Exception e) {
            Rlog
                    .fatal("studySite",
                            "Error in setStudySiteDetails() in StudySiteAgentBean "
                                    + e);
            return -2;
        }

    }

    /**
     * Updates a studySite record.
     *
     * @param a
     *            studySite state keeper containing studySite attributes to be
     *            set.
     * @return int for successful:0 ; e\Exception : -2
     */
    public int updateStudySite(StudySiteBean ssk) {

        StudySiteBean retrieved = null; // Entity Bean Remote Object
        int output;

        try {
            retrieved = (StudySiteBean) em.find(StudySiteBean.class,
                    new Integer(ssk.getStudySiteId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateStudySite(ssk);
            em.merge(retrieved);
        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Error in updateStudySite in StudySiteAgentBean" + e);
            return -2;
        }
        return output;
    }

    /**
     * Removes a studySite record.
     *
     * @param a
     *            StudySite Primary Key
     * @return int for successful:0; e\Exception : -2
     */

    public int removeStudySite(int studySiteId) {
        int output;
        StudySiteBean retrieved = null;
        try {

            retrieved = (StudySiteBean) em.find(StudySiteBean.class,
                    new Integer(studySiteId));
            em.remove(retrieved);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("studySite", "Exception in removeStudySite" + e);
            return -1;

        }

    }

    public StudySiteDao getCntForSiteInStudy(int studyId, int siteId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        try {

            studySiteDao.getCntForSiteInStudy(studyId, siteId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Exception In getCntForSiteInStudy in StudySiteAgentBean "
                            + e);

        }

        return studySiteDao;

    }

    public StudySiteDao getStudySiteTeamValues(int studyId, int orgId,
            int userId, int accId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        try {

            studySiteDao.getStudySiteTeamValues(studyId, orgId, userId, accId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Exception In getStudySiteTeamValues in StudySiteAgentBean "
                            + e);

        }

        return studySiteDao;

    }

    public StudySiteDao getAllStudyTeamSites(int studyId, int accId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        try {
            studySiteDao.getAllStudyTeamSites(studyId, accId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Exception In getAllStudyTeamSites in StudySiteAgentBean "
                            + e);

        }

        return studySiteDao;

    }

    public int getCountSitesForUserStudy(int studyId, int userId, int accountId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        int rows = 0;
        try {
            rows = studySiteDao.getCountSitesForUserStudy(studyId, userId,
                    accountId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Exception In getCountSitesForUserStudy in StudySiteAgentBean "
                            + e);

        }

        return rows;

    }

    public StudySiteDao getSitesForUserStudy(int studyId, int userId,
            int accountId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        try {
            studySiteDao.getSitesForUserStudy(studyId, userId, accountId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Exception In getSitesForUserStudy in StudySiteAgentBean "
                            + e);

        }

        return studySiteDao;

    }

    public StudySiteDao getStdSitesLSampleSize(int studyId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        try {
            studySiteDao.getStdSitesLSampleSize(studyId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Exception In getStdSitesLSampleSize in StudySiteAgentBean "
                            + e);

        }

        return studySiteDao;

    }

    public int updateLSampleSizes(String[] studySiteIds, String[] localSamples,
            int modifiedBy, String ipAdd) {
        int ret = 0;
        StudySiteDao studySiteDao = new StudySiteDao();
        try {
            ret = studySiteDao.updateLSampleSizes(studySiteIds, localSamples,
                    modifiedBy, ipAdd);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Exception In updateLSampleSizes in StudySiteAgentBean "
                            + e);

        }

        return ret;

    }

    public StudySiteDao getAddedAccessSitesForStudy(int studyId, int userId,
            int accountId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        try {
            studySiteDao
                    .getAddedAccessSitesForStudy(studyId, userId, accountId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Exception In getAddedAccessSitesForStudy in StudySiteAgentBean "
                            + e);

        }

        return studySiteDao;

    }

    public int deleteOrgFromStudyTeam(int studyId, int accId, int siteId, int userId) {
        int ret = 0;
        StudySiteDao studySiteDao = new StudySiteDao();
        try {
            ret = studySiteDao.deleteOrgFromStudyTeam(studyId, accId, siteId, userId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Exception In getAddedAccessSitesForStudy in StudySiteAgentBean "
                            + e);

        }

        return ret;

    }

    //Overloaded for INF-18183 ::: Ankit
    public int deleteOrgFromStudyTeam(int studyId, int accId, int siteId, int userId, Hashtable<String, String> auditInfo) {
        int ret = 0;
        StudySiteDao studySiteDao = new StudySiteDao();
        try {
      	
        	String condition =" fk_study ="+studyId+
			 " AND fk_site = "+siteId;
        	String pkVal = "";
        	String ridVal = "";
        	
        	AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String moduleName = (String)auditInfo.get(AuditUtils.APP_MODULE);
            Hashtable<String, ArrayList<String>> rowValues = AuditUtils.getRowValues("ER_STUDYSITES",condition,"eres");/*Fetches the RID/PK_VALUE*/
            ArrayList<String> rowPK  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_PK_KEY);
    		ArrayList<String> rowRID  = (ArrayList<String>)rowValues.get(AuditUtils.ROW_RID_KEY);
    		
    		for(int i=0; i<rowPK.size(); i++)
			{
    			pkVal = rowPK.get(i);
    			ridVal = rowRID.get(i);
    			audit = new AuditBean("ER_STUDYSITES",pkVal,ridVal,
            			userID,currdate,moduleName,ipAdd,reason);
	        	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
			}
        	
            ret = studySiteDao.deleteOrgFromStudyTeam(studyId, accId, siteId, userId);

        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("studySite",
                    "Exception In getAddedAccessSitesForStudy in StudySiteAgentBean "
                            + e);

        }

        return ret;

    }
    
    public int checkSuperUser(int studyId, int userId, int accountId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        int rows = 0;
        try {
            rows = studySiteDao.checkSuperUser(studyId, userId, accountId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Exception In checkSuperUser in StudySiteAgentBean " + e);

        }

        return rows;

    }

    public StudySiteDao getSitesForStatAndEnrolledPat(int studyId, int userId,
            int accId) {
        StudySiteDao studySiteDao = new StudySiteDao();
        try {
            studySiteDao.getSitesForStatAndEnrolledPat(studyId, userId, accId);

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Exception In getSitesForStatAndEnrolledPat in StudySiteAgentBean "
                            + e);

        }

        return studySiteDao;

    }

    /**
     *
     * Looks up the studysite bean for a study and site and returns a studySite
     * state keeper. containing all the details of the studySite <br>
     *
     * @param study
     *            Site Id
     */

    public StudySiteBean findBeanByStudySite(int study, int site) {

        StudySiteBean toreturn = null;

        try {
            Query query = em.createNamedQuery("findByStudySite");
            query.setParameter("fk_study", study);
            query.setParameter("fk_site", site);
            ArrayList list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                toreturn = (StudySiteBean) list.get(0);
            }
            return toreturn;

        } catch (Exception e) {
            Rlog.fatal("studySite",
                    "Exception in findBeanByStudySite() in StudySiteAgentBean"
                            + e);
        }

        return toreturn;

    }

    // end of class
}
