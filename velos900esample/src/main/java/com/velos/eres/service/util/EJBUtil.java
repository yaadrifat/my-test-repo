package com.velos.eres.service.util;

/* Import all the exceptions can be thrown */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringReader;
import java.io.Writer;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.json.JSONObject;

import oracle.sql.CLOB;

import com.velos.eres.service.accountAgent.AccountAgentRObj;
import com.velos.eres.service.accountWrapperAgent.AccountWrapperAgentRObj;
import com.velos.eres.service.addressAgent.AddressAgentRObj;
import com.velos.eres.service.appendixAgent.AppendixAgentRObj;
import com.velos.eres.service.browserRowsAgent.BrowserRowsAgentRObj;
import com.velos.eres.service.catLibAgent.CatLibAgentRObj;
import com.velos.eres.service.codelstAgent.CodelstAgentRObj;
import com.velos.eres.service.commonAgent.CommonAgentRObj;
import com.velos.eres.service.ctrltabAgent.CtrltabAgentRObj;
import com.velos.eres.service.dynrepAgent.DynRepAgentRObj;
import com.velos.eres.service.dynrepdtAgent.DynRepDtAgentRObj;
import com.velos.eres.service.dynrepfltrAgent.DynRepFltrAgentRObj;
import com.velos.eres.service.eschReportsAgent.EschReportsAgentRObj;
import com.velos.eres.service.extURightsAgent.ExtURightsAgentRObj;
import com.velos.eres.service.fieldLibAgent.FieldLibAgentRObj;
import com.velos.eres.service.fldRespAgent.FldRespAgentRObj;
import com.velos.eres.service.fldValidateAgent.FldValidateAgentRObj;
import com.velos.eres.service.formActionAgent.FormActionAgentRObj;
import com.velos.eres.service.formFieldAgent.FormFieldAgentRObj;
import com.velos.eres.service.formLibAgent.FormLibAgentRObj;
import com.velos.eres.service.formNotifyAgent.FormNotifyAgentRObj;
import com.velos.eres.service.formQueryAgent.FormQueryAgentRObj;
import com.velos.eres.service.formQueryStatusAgent.FormQueryStatusAgentRObj;
import com.velos.eres.service.formSecAgent.FormSecAgentRObj;
import com.velos.eres.service.formStatAgent.FormStatAgentRObj;
import com.velos.eres.service.groupAgent.GroupAgentRObj;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.invoiceAgent.InvoiceAgentRObj;
import com.velos.eres.service.invoiceAgent.InvoiceDetailAgentRObj;
import com.velos.eres.service.labAgent.LabAgentRObj;
import com.velos.eres.service.linkedFormsAgent.LinkedFormsAgentRObj;
import com.velos.eres.service.mileApndxAgent.MileApndxAgentRObj;
import com.velos.eres.service.milepaymentAgent.MilepaymentAgentRObj;
import com.velos.eres.service.milepaymentAgent.PaymentDetailAgentRObj;
import com.velos.eres.service.milestoneAgent.MilestoneAgentRObj;
import com.velos.eres.service.moreDetailsAgent.MoreDetailsAgentRObj;
import com.velos.eres.service.msgcntrAgent.MsgcntrAgentRObj;
import com.velos.eres.service.patFacilityAgent.PatFacilityAgentRObj;
import com.velos.eres.service.patLoginAgent.PatLoginAgentRObj;
import com.velos.eres.service.patProtAgent.PatProtAgentRObj;
import com.velos.eres.service.patStudyStatAgent.PatStudyStatAgentRObj;
import com.velos.eres.service.patTXArmAgent.PatTXArmAgentRObj;
import com.velos.eres.service.perApndxAgent.PerApndxAgentRObj;
import com.velos.eres.service.perIdAgent.PerIdAgentRObj;
import com.velos.eres.service.personAgent.PersonAgentRObj;
import com.velos.eres.service.portalAgent.PortalAgentRObj;
import com.velos.eres.service.portalDesignAgent.PortalDesignAgentRObj;
import com.velos.eres.service.prefAgent.PrefAgentRObj;
import com.velos.eres.service.reportAgent.ReportAgentRObj;
import com.velos.eres.service.reportsAgent.ReportsAgentRObj;
import com.velos.eres.service.savedRepAgent.SavedRepAgentRObj;
import com.velos.eres.service.sectionAgent.SectionAgent;
import com.velos.eres.service.siteAgent.SiteAgentRObj;
import com.velos.eres.service.specimenAgent.SpecimenAgentRObj;
import com.velos.eres.service.specimenApndxAgent.SpecimenApndxAgentRObj;
import com.velos.eres.service.specimenStatusAgent.SpecimenStatusAgentRObj;
import com.velos.eres.service.statusHistoryAgent.StatusHistoryAgentRObj;
import com.velos.eres.service.stdSiteAddInfoAgent.StdSiteAddInfoAgentRObj;
import com.velos.eres.service.stdSiteRightsAgent.StdSiteRightsAgentRObj;
import com.velos.eres.service.storageAgent.StorageAgentRObj;
import com.velos.eres.service.storageAllowedItemsAgent.StorageAllowedItemsAgentRObj;
import com.velos.eres.service.storageStatusAgent.StorageStatusAgentRObj;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.studyIdAgent.StudyIdAgentRObj;
import com.velos.eres.service.studyINDIDEAgent.StudyINDIDEAgentRObj;
import com.velos.eres.service.studyNIHGrantAgent.StudyNIHGrantAgentRObj;
import com.velos.eres.service.studyNotifyAgent.StudyNotifyAgentRObj;
import com.velos.eres.service.studyRightsAgent.StudyRightsAgentRObj;
import com.velos.eres.service.studySiteAgent.StudySiteAgentRObj;
import com.velos.eres.service.studySiteApndxAgent.StudySiteApndxAgentRObj;
import com.velos.eres.service.studyStatusAgent.StudyStatusAgentRObj;
import com.velos.eres.service.studyTXArmAgent.StudyTXArmAgentRObj;
import com.velos.eres.service.studyVerAgent.StudyVerAgentRObj;
import com.velos.eres.service.studyViewAgent.StudyViewAgentRObj;
import com.velos.eres.service.submissionAgent.ReviewMeetingTopicAgent;
import com.velos.eres.service.submissionAgent.SubmissionAgent;
import com.velos.eres.service.submissionAgent.SubmissionBoardAgent;
import com.velos.eres.service.submissionAgent.SubmissionProvisoAgent;
import com.velos.eres.service.submissionAgent.SubmissionStatusAgent;
import com.velos.eres.service.teamAgent.TeamAgentRObj;
import com.velos.eres.service.ulinkAgent.ULinkAgentRObj;
import com.velos.eres.service.updateSchedulesAgent.UpdateSchedulesAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.userSessionAgent.UserSessionAgentRObj;
import com.velos.eres.service.userSessionLogAgent.UserSessionLogAgentRObj;
import com.velos.eres.service.userSiteAgent.UserSiteAgentRObj;
import com.velos.eres.service.usrGrpAgent.UsrGrpAgentRObj;
import com.velos.eres.service.usrSiteGrpAgent.UsrSiteGrpAgentRObj;
import com.velos.eres.service.storageKitAgent.StorageKitAgentRObj;
import com.velos.eres.ctrp.service.CtrpDraftAgent;
import com.velos.eres.audit.service.AuditRowEresAgent;



/**
 * This is a utility class for obtaining EJB references. All modules Home
 * objects should be referred from this location.
 */

public final class EJBUtil {

    static int cReturn;

    static float cFloatReturn;

    /**
     * @deprecated Use the one in StringUtil.java, instead.
     */
    public static String integerToString(Integer i) {

        String returnString = null;
        try {
            if (i == null) {
                return returnString;
            }

            returnString = i.toString();
            return returnString;

        } catch (Exception e) {
            Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO String:}" + e);
            return returnString;
        }

    }

    /**
     * @deprecated Use the one in StringUtil.java, instead.
     */
    public static Integer stringToInteger(String str) {
        Integer returnInteger = null;

        // Rlog.debug("common","EJBUtil stringToInteger value of Str" + str);

        try {

            if (str != null) {
                // Rlog.debug("common","inside str!=null");
                str = str.trim();
                if (str.length() > 0) {
                    // Rlog.debug("common","inside str.length>0");
                    returnInteger = Integer.valueOf(str);
                }
            }
            return returnInteger;
        } catch (Exception e) {
            Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO Integer:}" + e);
            return returnInteger;
        }
    }

    /**
     * @deprecated Use the one in StringUtil.java, instead.
     */
    public static String floatToString(Float i) {
        String returnString = null;
        try {

            if (i == null) {

                return returnString;
            }
            returnString = i.toString();

            return returnString;
        } catch (Exception e) {
            Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO String:}" + e);
            return returnString;
        }
    }

    /**
     * @deprecated Use the one in StringUtil.java, instead.
     */
    public static int stringToNum(String str) {

        int iReturn = 0;
        // Rlog.debug("common","inside stringToNum"+str);

        try {

            if ((str == null) || str.equals("null")) {
                return cReturn;
            } else if (str.equals("")) {
                return cReturn;
            } else {
                iReturn = Integer.parseInt(str);
                return iReturn;
            }

        } catch (Exception e) {
            Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO NUMBER:}" + e);
            return iReturn;
        }

    }

    /**
     * @deprecated Use the one in StringUtil.java, instead.
     */
    public static float stringToFloat(String str) {
        float iReturn = 0;
        try {

            if (str == null) {
                return cFloatReturn;
            } else if (str == "") {
                return cFloatReturn;
            } else {
                iReturn = Float.parseFloat(str);
                return iReturn;
            }
        } catch (Exception e) {
            Rlog.fatal("common", "{EXCEPTION IN CONVERTING TO FLOAT:}" + e);
            return iReturn;
        }
    }

    public static String format(String str, int places, String toReplace) {

        String finalStrSecond = str;

        int strLen = str.length();
        // int pos = str.indexOf(".");
        int pos = str.indexOf(toReplace);
        String subStr = "";

        if (pos != -1) {
            subStr = str.substring(pos + 1);
            int subStrLen = subStr.length();
            subStrLen = subStrLen - 1;

            if (subStrLen >= places) {
                if (!(subStr.equals("0"))) {
                    String subStrFirst = subStr.substring(places - 1, places);
                    String subStrSecond = subStr.substring(places, places + 1);
                    int tempVarFirst = stringToNum(subStrFirst);
                    int tempVarSecond = stringToNum(subStrSecond);

                    if (tempVarSecond >= 5) {
                        tempVarFirst = tempVarFirst + 1;
                    }
                    String convStr = new Integer(tempVarFirst).toString();

                    String finalStrFirst = str.substring(0, pos + places);
                    finalStrSecond = finalStrFirst.concat(convStr);
                }
            } else {
                subStrLen = subStrLen + 1;
                places = places - subStrLen;
                subStr = "";
                for (int i = 0; i < places; i++) {
                    subStr = subStr + "0";
                }

                finalStrSecond = finalStrSecond + subStr;
            }
        } else {
            for (int i = 0; i < places; i++) {
                subStr = subStr + "0";
            }

            finalStrSecond = finalStrSecond + toReplace + subStr;
        }

        return finalStrSecond;

    }

    // ////////////////////////////////////////////////////////////////////////////////////

    /*
     *
     *
     * if the appServerParam are set in configuration class then return it,
     * otherwise read
     *
     *
     * it from xml file and store in appServerParam then return.
     *
     *
     */

    public static Hashtable getContextProp() {

        try {

            if (Configuration.getServerParam() == null)
                Configuration.readSettings();

        } catch (Exception e) {

            Rlog.fatal("common", "EJBUtil:getContextProp:general ex" + e);

        }

        return Configuration.getServerParam();

    }
    
    public static StudyAgent getStudyAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            // System.out.println((String)initial.lookup("SiteAgentBean"));
            return (StudyAgent) initial.lookup(JNDINames.StudyAgentHome);
        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }

    public static AccountAgentRObj getAccountAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (AccountAgentRObj) initial.lookup(JNDINames.AccountAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getAccountAgentHome() in EJBUtil "
                    + e);
            return null;
        }

    }

    public static FormActionAgentRObj getFormActionAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (FormActionAgentRObj) initial.lookup(JNDINames.FormActionAgentHome);

        } catch (Exception e) {
            Rlog.fatal("formAction", "Error in getFormActionAgentHome() in EJBUtil "
                    + e);
            return null;
        }

    }

    public static AppendixAgentRObj getAppendixAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (AppendixAgentRObj) initial.lookup(JNDINames.AppendixAgentHome);

        } catch (Exception e) {
            Rlog.fatal("appendix",
                    "Error in getAppendixAgentHome() in EJBUtil " + e);
            return null;
        }

    }

    public static GroupAgentRObj getGroupAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (GroupAgentRObj) initial.lookup(JNDINames.GroupAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }

    public static AddressAgentRObj getAddressAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (AddressAgentRObj) initial.lookup(JNDINames.AddressAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }

    public static MsgcntrAgentRObj getMsgcntrAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (MsgcntrAgentRObj) initial.lookup(JNDINames.MsgcntrAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "gen ex=" + e);

            return null;

        }

    }

    public static FormNotifyAgentRObj getFormNotifyAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (FormNotifyAgentRObj) initial.lookup(JNDINames.FormNotifyAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }

    public static CatLibAgentRObj getCatLibAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (CatLibAgentRObj) initial.lookup(JNDINames.CatLibAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }

    public static PatFacilityAgentRObj getPatFacilityAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (PatFacilityAgentRObj) initial.lookup(JNDINames.PatFacilityAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=PatFacility" + e);
            return null;
        }
    }

    public static FldRespAgentRObj getFldRespAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (FldRespAgentRObj) initial.lookup(JNDINames.FldRespAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }

    public static FormLibAgentRObj getFormLibAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (FormLibAgentRObj) initial.lookup(JNDINames.FormLibAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }

    public static FieldLibAgentRObj getFieldLibAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (FieldLibAgentRObj) initial.lookup(JNDINames.FieldLibAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }

    public static FormFieldAgentRObj getFormFieldAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (FormFieldAgentRObj) initial.lookup(JNDINames.FormFieldAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }

    public static LinkedFormsAgentRObj getLinkedFormAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (LinkedFormsAgentRObj) initial.lookup(JNDINames.LinkedFormsAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }

    public static FormSecAgentRObj getFormSecAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (FormSecAgentRObj) initial.lookup(JNDINames.FormSecAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }

    public static FormStatAgentRObj getFormStatAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (FormStatAgentRObj) initial.lookup(JNDINames.FormStatAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }

    public static SubmissionAgent getSubmissionAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (SubmissionAgent) initial.lookup(JNDINames.SubmissionAgentHome);
        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }

    public static SubmissionBoardAgent getSubmissionBoardAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (SubmissionBoardAgent) initial.lookup(JNDINames.SubmissionBoardAgentHome);
        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }

    public static SubmissionStatusAgent getSubmissionStatusAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (SubmissionStatusAgent) initial.lookup(JNDINames.SubmissionStatusAgentHome);
        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }

    public static ReviewMeetingTopicAgent getReviewMeetingTopicAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (ReviewMeetingTopicAgent) initial.lookup(JNDINames.ReviewMeetingTopicAgentHome);
        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }
    
    public static SectionAgent getSectionAgentHome() {

        try {

            InitialContext initial = new InitialContext();
            return (SectionAgent) initial.lookup(JNDINames.SectionAgentHome);

        } catch (Exception e) {

            Rlog
                    .fatal("common", "EXCEPTION IN GETSTUDYSECTION AGENT HOME:"
                            + e);

            return null;

        }

    }

    public static StudyStatusAgentRObj getStudyStatusAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (StudyStatusAgentRObj) initial.lookup(JNDINames.StudyStatusAgentHome);

        } catch (Exception e) {

            Rlog
                    .fatal("common", "EXCEPTION IN GETSTUDY STATUS AGENT HOME:"
                            + e);

            return null;

        }

    }

    public static TeamAgentRObj getTeamAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (TeamAgentRObj) initial.lookup(JNDINames.TeamAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "EXCEPTION IN GETSTUDY TEAM AGENT HOME:" + e);

            return null;

        }

    }

    // //dinesh 02/23/2001

    public static UserAgentRObj getUserAgentHome() {

        try {

            InitialContext initial = new InitialContext(getContextProp());

            return (UserAgentRObj) initial
                    .lookup(JNDINames.UserAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "Error in getUserAgentHome() in EJBUtil " + e);
            e.printStackTrace();
            return null;

        }

    }

    public static CodelstAgentRObj getCodelstAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (CodelstAgentRObj) initial.lookup(JNDINames.CodelstAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "Error in getCodelstAgentHome() in EJBUtil "
                    + e);

            return null;

        }

    }

    //KM:20,May2006.
    public static CtrltabAgentRObj getCtrltabAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (CtrltabAgentRObj) initial.lookup(JNDINames.CtrltabAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "Error in getCtrltabAgentHome() in EJBUtil "
                    + e);

            return null;

        }

    }




    public static SiteAgentRObj getSiteAgentHome() {

        try

        {

            // InitialContext initial = new InitialContext(getContextProp());
            InitialContext initial = new InitialContext();
            // System.out.println((String)initial.lookup(JNDINames."SiteAgentBean"));
            return (SiteAgentRObj) initial.lookup(JNDINames.SiteAgentHome);

            // return (SiteAgentHome)
            // PortableRemoteObject.narrow(objref,SiteAgentHome.class);
            // return (SiteAgentRObj)objref;

        } catch (Exception e) {

            Rlog.fatal("common", "Error in getSiteAgentHome() in EJBUtil " + e);

            return null;

        }

    }

    public static ULinkAgentRObj getULinkAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (ULinkAgentRObj) initial.lookup(JNDINames.ULinkAgentHome);

        } catch (Exception e) {

            Rlog
                    .fatal("common", "Error in getULinkAgentHome() in EJBUtil "
                            + e);

            return null;

        }

    }

    public static GrpRightsAgentRObj getGrpRightsAgentHome() {

        try {

            InitialContext initial = new InitialContext();
            return (GrpRightsAgentRObj) initial.lookup(JNDINames.GrpRightsAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "gen ex=" + e);

            return null;

        }

    }

    public static UsrGrpAgentRObj getUsrGrpAgentHome() {

        try {

            InitialContext initial = new InitialContext();
            return (UsrGrpAgentRObj) initial.lookup(JNDINames.UsrGrpAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "gen ex=" + e);

            return null;

        }

    }

    public static AccountWrapperAgentRObj getAccountWrapperAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (AccountWrapperAgentRObj) initial
                    .lookup(JNDINames.AccountWrapperAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common",
                    "Error in getAccountWrapperAgentHome() in EJBUtil " + e);

            return null;

        }

    }

    public static StudyRightsAgentRObj getStudyRightsAgentHome() {

        try {

            InitialContext initial = new InitialContext();
            return (StudyRightsAgentRObj) initial.lookup(JNDINames.StudyRightsAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "gen ex=" + e);

            return null;

        }

    }

    public static StudyNotifyAgentRObj getStudyNotifyAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (StudyNotifyAgentRObj) initial.lookup(JNDINames.StudyNotifyAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "gen ex=" + e);

            return null;

        }

    }
    //Modified for INF-18183 ::: Akshi
    public static StorageAllowedItemsAgentRObj getStorageAllowedItemValue() {

        try {

            InitialContext initial = new InitialContext();

            return (StorageAllowedItemsAgentRObj) initial.lookup(JNDINames.StorageAllowedItemsAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "gen ex=" + e);

            return null;

        }

    }

    public static ExtURightsAgentRObj getExtURightsAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (ExtURightsAgentRObj) initial
                    .lookup(JNDINames.ExtURightsAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "gen ex=" + e);

            return null;

        }

    }

    public static ReportsAgentRObj getReportsAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (ReportsAgentRObj) initial.lookup(JNDINames.ReportsAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "Error in getReportsAgentHome() in EJBUtil "
                    + e);

            return null;

        }
    }

    public static ReportAgentRObj getReportAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (ReportAgentRObj) initial.lookup(JNDINames.ReportAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "Error in getReportAgentHome() in EJBUtil "
                    + e);

            return null;

        }
    }

    public static DynRepAgentRObj getDynRepAgentHome() {

        try {

            InitialContext initial = new InitialContext();
            return (DynRepAgentRObj) initial.lookup(JNDINames.DynRepAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "Error in getDynRepAgentHome() in EJBUtil "
                    + e);

            return null;

        }
    }

    public static DynRepDtAgentRObj getDynRepDtAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (DynRepDtAgentRObj) initial.lookup(JNDINames.DynRepDtAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "Error in getDynRepDtAgentHome() in EJBUtil "
                    + e);

            return null;

        }
    }

    public static InvoiceAgentRObj getInvoiceAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (InvoiceAgentRObj) initial.lookup(JNDINames.InvoiceAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "Error in getInvoiceAgentHome() in EJBUtil "
                    + e);

            return null;

        }
    }

    public static PaymentDetailAgentRObj getPaymentDetailAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (PaymentDetailAgentRObj) initial.lookup(JNDINames.PaymentDetailAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "Error in getPaymentDetailAgentHome() in EJBUtil "
                    + e);

            return null;

        }
    }

    public static InvoiceDetailAgentRObj getInvoiceDetailAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (InvoiceDetailAgentRObj) initial.lookup(JNDINames.InvoiceDetailAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "Error in getInvoiceDetailAgentHome() in EJBUtil "
                    + e);

            return null;

        }
    }


    public static DynRepFltrAgentRObj getDynRepFltrAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (DynRepFltrAgentRObj) initial
                    .lookup(JNDINames.DynRepFltrAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common",
                    "Error in getDynRepFltrAgentHome() in EJBUtil " + e);

            return null;

        }
    }

    public static CommonAgentRObj getCommonAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (CommonAgentRObj) initial.lookup(JNDINames.CommonAgentHome);

        } catch (Exception e) {
            Rlog
                    .fatal("common", "getting CommonAgentHome in EJBUtil.java="
                            + e);
            return null;
        }
    }


    public static PrefAgentRObj getPrefAgentHome() {

        try {

            InitialContext initial = new InitialContext();
            return (PrefAgentRObj) initial.lookup(JNDINames.PrefAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "ex in PREF AGENT HOME" + e);

            return null;

        }

    }

    public static PersonAgentRObj getPersonAgentHome() {

        try {

            InitialContext initial = new InitialContext();

            return (PersonAgentRObj) initial.lookup(JNDINames.PersonAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "ex in PERSON AGENT HOME" + e);

            return null;

        }

    }

    public static PatProtAgentRObj getPatProtAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (PatProtAgentRObj) initial.lookup(JNDINames.PatProtAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getPatProtAgentHome() in EJBUtil "
                    + e);
            return null;
        }
    }

    public static PatStudyStatAgentRObj getPatStudyStatAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (PatStudyStatAgentRObj) initial.lookup(JNDINames.PatStudyStatAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common",
                    "Error in getPatStudyStatAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static StudyViewAgentRObj getStudyViewAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (StudyViewAgentRObj) initial.lookup(JNDINames.StudyViewAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getStudyViewAgentHome() in EJBUtil "
                    + e);
            return null;
        }
    }

    public static EschReportsAgentRObj getEschReportsAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (EschReportsAgentRObj) initial
                    .lookup(JNDINames.EschReportsAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getEschReportsAgentHome() in EJBUtil "
                    + e);
            return null;
        }
    }

    public static MilestoneAgentRObj getMilestoneAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (MilestoneAgentRObj) initial.lookup(JNDINames.MilestoneAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getMilestoneAgentHome() in EJBUtil "
                    + e);
            return null;
        }
    }

    public static MilepaymentAgentRObj getMilepaymentAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (MilepaymentAgentRObj) initial.lookup(JNDINames.MilepaymentAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common",
                    "Error in getMilepaymentAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static SavedRepAgentRObj getSavedRepAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (SavedRepAgentRObj) initial.lookup(JNDINames.SavedRepAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getSavedRepAgentHome() in EJBUtil "
                    + e);
            return null;
        }
    }

    public static StudyVerAgentRObj getStudyVerAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (StudyVerAgentRObj) initial.lookup(JNDINames.StudyVerAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getSTUDYvERAgentHome() in EJBUtil "
                    + e);
            return null;
        }
    }

    public static StudySiteApndxAgentRObj getStudySiteApndxAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (StudySiteApndxAgentRObj) initial.lookup(JNDINames.StudySiteApndxAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common",
                    "Error in getStudySiteApndxAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static StdSiteAddInfoAgentRObj getStdSiteAddInfoAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (StdSiteAddInfoAgentRObj) initial.lookup(JNDINames.StdSiteAddInfoAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common",
                    "Error in getStdSiteAddInfoAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static StdSiteRightsAgentRObj getStdSiteRightsAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (StdSiteRightsAgentRObj) initial.lookup(JNDINames.StdSiteRightsAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common",
                    "Error in getStdSiteRightsAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static MileApndxAgentRObj getMileApndxAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (MileApndxAgentRObj) initial.lookup(JNDINames.MileApndxAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getMileApndxHome() in EJBUtil" + e);
            return null;
        }
    }

    public static PerApndxAgentRObj getPerApndxAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (PerApndxAgentRObj) initial.lookup(JNDINames.PerApndxAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getPerApndxAgentHome() in EJBUtil"
                    + e);
            return null;
        }
    }

    public static UserSessionAgentRObj getUserSessionAgentHome() {

        try {
            InitialContext initial = new InitialContext();

            return (UserSessionAgentRObj) initial.lookup(JNDINames.UserSessionAgentHome);
        } catch (Exception e) {
            Rlog.fatal("common",
                    "Error in getUserSessionAgentHome() in EJBUtil" + e);
            return null;
        }
    }

    public static UserSessionLogAgentRObj getUserSessionLogAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (UserSessionLogAgentRObj) initial.lookup(JNDINames.UserSessionLogAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common",
                    "Error in getUserSessionLogAgentHome() in EJBUtil" + e);
            return null;
        }
    }

    public static UserSiteAgentRObj getUserSiteAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (UserSiteAgentRObj) initial.lookup(JNDINames.UserSiteAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getUserSiteAgentHome() in EJBUtil "
                    + e);
            return null;
        }
    }

    public static StudyIdAgentRObj getStudyIdAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (StudyIdAgentRObj) initial.lookup(JNDINames.StudyIdAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getStudyIdAgentHome() in EJBUtil "
                    + e);
            return null;
        }
    }
    public static MoreDetailsAgentRObj getMoreDetailsAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (MoreDetailsAgentRObj) initial.lookup(JNDINames.MoreDetailsAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getMoreDetailsAgentHome() in EJBUtil "
                    + e);
            return null;
        }
    }

    public static PerIdAgentRObj getPerIdAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (PerIdAgentRObj) initial.lookup(JNDINames.PerIdAgentHome);

        } catch (Exception e) {
            Rlog
                    .fatal("common", "Error in getPerIdAgentHome() in EJBUtil "
                            + e);
            return null;
        }
    }

    public static StudyRightsAgentRObj getGrpSupUserRigthtsAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (StudyRightsAgentRObj) initial.lookup(JNDINames.GrpSupUserRigthtsAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common",
                    "Error in getGrpSupUserRigthtsAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static BrowserRowsAgentRObj getBrowserRowsAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (BrowserRowsAgentRObj) initial
                    .lookup(JNDINames.BrowserRowsAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common",
                    "Error in getBrowserRowsAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static FldValidateAgentRObj getFldValidateAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (FldValidateAgentRObj) initial.lookup(JNDINames.FldValidateAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common",
                    "Error in getFldValidateAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static LabAgentRObj getLabAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (LabAgentRObj) initial.lookup(JNDINames.LabAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getLabAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static StudySiteAgentRObj getStudySiteAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (StudySiteAgentRObj) initial.lookup(JNDINames.StudySiteAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getStudySiteAgentHome() in EJBUtil "
                    + e);
            return null;
        }
    }

    public static FormQueryAgentRObj getFormQueryAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (FormQueryAgentRObj) initial.lookup(JNDINames.FormQueryAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getFormQueryAgentHome() in EJBUtil "
                    + e);
            return null;
        }
    }

    public static FormQueryStatusAgentRObj getFormQueryStatusAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (FormQueryStatusAgentRObj) initial.lookup(JNDINames.FormQueryStatusAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common",
                    "Error in getFormQueryStatusAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    /*
     * @return StatusHistoryAgentHome object
     */

    public static StatusHistoryAgentRObj getStatusHistoryAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (StatusHistoryAgentRObj) initial.lookup(JNDINames.StatusHistoryAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }

    public static StudyTXArmAgentRObj getStudyTXArmAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (StudyTXArmAgentRObj) initial.lookup(JNDINames.StudyTXArmAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common",
                    "Error in getStudyTXArmAgentHome() in EJBUtil " + e);
            return null;
        }
    }

    public static PatTXArmAgentRObj getPatTXArmAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (PatTXArmAgentRObj) initial.lookup(JNDINames.PatTXArmAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getPatTXArmAgentHome() in EJBUtil "
                    + e);
            return null;
        }
    }


    //Added by Manimaran for AdminPortal Enhancment.
    public static PortalAgentRObj getPortalAgentHome() {
    	 try {
             InitialContext initial = new InitialContext();
             return (PortalAgentRObj) initial.lookup(JNDINames.PortalAgentHome);

         } catch (Exception e) {
             Rlog.fatal("common", "Error in getPortalAgentHome() in EJBUtil "
                     + e);
             return null;
         }

    }
//  JM: 04/06/2007
    public static PatLoginAgentRObj getPatLoginHome() {
        try {
            InitialContext initial = new InitialContext();
            return (PatLoginAgentRObj) initial.lookup(JNDINames.PatLoginAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getPatLoginHome() in EJBUtil "
                    + e);
            return null;
        }
    }


     //JM:
    public static SpecimenAgentRObj getSpecimenAgentHome() {

        try {

            InitialContext initial = new InitialContext();
            return (SpecimenAgentRObj) initial.lookup(JNDINames.SpecimenAgentHome);

        } catch (Exception e) {

            Rlog.fatal("common", "Error in getSpecimenAgentHome() in EJBUtil " + e);

            return null;

        }

    }




    //Added by Manimaran for AdminPortal Enhancment.
    public static PortalDesignAgentRObj getPortalDesignAgentHome() {
    	 try {
             InitialContext initial = new InitialContext();
             return (PortalDesignAgentRObj) initial.lookup(JNDINames.PortalDesignAgentHome);

         } catch (Exception e) {
             Rlog.fatal("common", "Error in getPortalDesignAgentHome() in EJBUtil "
                     + e);
             return null;
         }

    }


    //  Added by Manimaran for Inventory Management Enhancment.
    public static StorageAgentRObj getStorageAgentHome() {
    	 try {
             InitialContext initial = new InitialContext();
             return (StorageAgentRObj) initial.lookup(JNDINames.StorageAgentHome);

         } catch (Exception e) {
             Rlog.fatal("common", "Error in getStorageAgentHome() in EJBUtil "
                     + e);
             return null;
         }
    }

//  Added by Manimaran for Inventory Management Enhancment.
    public static StorageStatusAgentRObj getStorageStatusAgentHome() {
   	 try {
            InitialContext initial = new InitialContext();
            return (StorageStatusAgentRObj) initial.lookup(JNDINames.StorageStatusAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getStorageStatusAgentHome() in EJBUtil "
                    + e);
            return null;
        }
   }

//JM: 16Oct2007: added
    public static SpecimenStatusAgentRObj getSpecimenStatusAgentHome() {
  	 try {
           InitialContext initial = new InitialContext();
           return (SpecimenStatusAgentRObj) initial.lookup(JNDINames.SpecimenStatusAgentHome);

       } catch (Exception e) {
           Rlog.fatal("common", "Error in getSpecimenStatusAgentHome() in EJBUtil " + e);
           return null;
       }
      }

    public static StorageAllowedItemsAgentRObj getStorageAllowedItemAgentHome() {
      	 try {
               InitialContext initial = new InitialContext();
               return (StorageAllowedItemsAgentRObj) initial.lookup(JNDINames.StorageAllowedItemsAgentHome);

           } catch (Exception e) {
               Rlog.fatal("common", "Error in getStorageAllowedItemAgentHome in EJBUtil "
                       + e);
               return null;
           }
      }



    public static SpecimenApndxAgentRObj getSpecimenApndxAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (SpecimenApndxAgentRObj) initial.lookup(JNDINames.SpecimenApndxAgentHome);

        } catch (Exception e) {
            Rlog.fatal("common", "Error in getSpecimenApndxAgentHome() in EJBUtil "
                    + e);
            return null;
        }
    }


    public static UpdateSchedulesAgentRObj getUpdateSchedulesAgentHome() {
      	 try {
               InitialContext initial = new InitialContext();
               return (UpdateSchedulesAgentRObj) initial.lookup(JNDINames.UpdateSchedulesAgentHome);

           } catch (Exception e) {
               Rlog.fatal("common", "Error in getUpdateSchedulesAgentHome() in EJBUtil@@ "
                       + e);
               return null;
           }
      }

    public static SubmissionProvisoAgent getSubmissionProvisoAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (SubmissionProvisoAgent) initial.lookup(JNDINames.SubmissionProvisoAgentHome);
        } catch (Exception e) {
            Rlog.fatal("common", "Exception in getSubmissionProvisoAgentHome" + e);
            return null;
        }
    }

//JM: 05Aug2009, #invp2.15(part2,3)
    public static StorageKitAgentRObj getStorageKitAgentHome() {

        try {

            InitialContext initial = new InitialContext();
            return (StorageKitAgentRObj) initial.lookup(JNDINames.StorageKitAgentHome);

        } catch (Exception e) {

            Rlog.fatal("util", "Exception in getStorageKitAgentHome" + e);

            return null;

        }

    }
    
    //Enhancement CTRP-20527 : AGodara
    public static StudyINDIDEAgentRObj getStudyINDIDEAgentHome() {

        try {
            InitialContext initial = new InitialContext();
            return (StudyINDIDEAgentRObj) initial.lookup(JNDINames.StudyINDIDEAgentHome);
        } catch (Exception e) {
            Rlog.fatal("EJBUtil", "Error in getStudyINDIDEAgentHome() in EJBUtil " + e);
            return null;
        }
    }
    
    //Ashu:11/22/1011:Added for enhancement CTRP-20527-1
    
    public static StudyNIHGrantAgentRObj getStudyNIHGrantAgentHome() {

        try {

            InitialContext initial = new InitialContext();
            return (StudyNIHGrantAgentRObj) initial.lookup(JNDINames.StudyNIHGrantAgentHome);

        } catch (Exception e) {

            Rlog.fatal("util", "Exception in getStudyNIHGrantAgentHome" + e);

            return null;

        }

    }


    public static String getActualPath(String dir, String fileName)

    {

        String path;

        dir = dir.trim();

        fileName = fileName.trim();

        File f = new File(dir, fileName);

        try {

            path = f.getAbsolutePath();

        }

        catch (Exception e)

        {

            path = null;

        }

        return path;

    }

    /** *********GET CONNECTION FROM RESOURCE********* */

    public static Connection getConnection() {

        InitialContext iCtx = null;

        Connection conn = null;

        Configuration conf = null;

        try {

            iCtx = new InitialContext(EJBUtil.getContextProp());

            // Rlog.debug("common","INITIAL CONTECXT:" + iCtx +"conf Path"
            // +JNDINames.ERESEARCH_CONF_PATH);

            if (Configuration.dsJndiName == null) {

                Configuration.readSettings();

            }

            Rlog
                    .debug("common", "JNDI NAME DS NAME"
                            + Configuration.dsJndiName);

            DataSource ds = (DataSource) iCtx.lookup(Configuration.dsJndiName);

            Rlog.debug("common", "DATASOURCE " + ds);
             return ds.getConnection();

        } catch (NamingException ne) {

            Rlog.fatal("common", ":getConnection():Naming exception:" + ne);

            return null;

        } catch (Exception se) {

            Rlog.fatal("common", ":getConnection():Sql exception:" + se);

            return null;

        } finally {

            try {

                if (iCtx != null)
                    iCtx.close();

            } catch (NamingException ne) {

                Rlog.fatal("common",
                        ":getConnection():context close exception:" + ne);

            }

        }

    }

    /** **********READ ENV VARIABLES************ */

    public static String getEnvVariable(String Name) throws Exception {

        String iline = "";

        String envValue = "";

        int i = 0;

        int sType = 0;

        Process theProcess = null;

        BufferedReader stdInput = null;

        try {

            // Figure out what kind of system we are running on

            // 0 = windows

            // 1 = unix

            sType = getSystemType();

            if (sType == 0)

            {

                String envPath = "cmd /C echo %" + Name + "%";



                theProcess = Runtime.getRuntime().exec(envPath);

            } else

            {

                theProcess = Runtime.getRuntime().exec("printenv " + Name);

            }

            stdInput = new BufferedReader(

            new InputStreamReader

            (theProcess.getInputStream()));

            envValue = stdInput.readLine();

            stdInput.close();

            theProcess.destroy();

            return envValue;

        } catch (IOException ioe) {

            throw new Exception(ioe.getMessage());

        }

    }

    public static int getSystemType() {

        Properties p = System.getProperties();

        String os = p.getProperty("os.name");

        int i = 0;

        int osType = 0;

        if ((i = os.indexOf("Win")) != -1) {

            osType = 0;

        }

        else {

            osType = 1;

        }

        return osType;

    }

    public static String getRelativeTimeDD(String ddName, String selected) {
        StringBuffer sb = new StringBuffer();

        String bNotSelected = "";
        String bSelected = "SELECTED";

        sb.append("<SELECT NAME='" + ddName + "'>");

        if (selected.equals("ALL"))
            sb.append("<OPTION value='ALL' " + bSelected + " >ALL</OPTION>");
        else
            sb.append("<OPTION value='ALL' " + bNotSelected + " >ALL</OPTION>");

        if (selected.equals("TM"))
            sb.append("<OPTION value='TM' " + bSelected
                    + " >This Month</OPTION>");
        else
            sb.append("<OPTION value='TM' " + bNotSelected
                    + " >This Month</OPTION>");

        if (selected.equals("TW"))
            sb.append("<OPTION value='TW' " + bSelected
                    + " >This Week</OPTION>");
        else
            sb.append("<OPTION value='TW' " + bNotSelected
                    + " >This Week</OPTION>");

        if (selected.equals("TY"))
            sb.append("<OPTION value='TY' " + bSelected
                    + " >This Year</OPTION>");
        else
            sb.append("<OPTION value='TY' " + bNotSelected
                    + " >This Year</OPTION>");

        if (selected.equals("T"))
            sb.append("<OPTION value='T' " + bSelected + " >Today</OPTION>");
        else
            sb.append("<OPTION value='T' " + bNotSelected + " >Today</OPTION>");

        if (selected.equals("LM"))
            sb.append("<OPTION value='LM' " + bSelected
                    + " >Last Month</OPTION>");
        else
            sb.append("<OPTION value='LM' " + bNotSelected
                    + " >Last Month</OPTION>");

        if (selected.equals("LW"))
            sb.append("<OPTION value='LW' " + bSelected
                    + " >Last Week</OPTION>");
        else
            sb.append("<OPTION value='LW' " + bNotSelected
                    + " >Last Week</OPTION>");

        if (selected.equals("LY"))
            sb
                    .append("<OPTION value='LY' " + bSelected
                            + ">Last Year</OPTION>");
        else
            sb.append("<OPTION value='LY' " + bNotSelected
                    + ">Last Year</OPTION>");

        if (selected.equals("Y"))
            sb.append("<OPTION value='Y' " + bSelected + ">Yesterday</OPTION>");
        else
            sb.append("<OPTION value='Y' " + bNotSelected
                    + ">Yesterday</OPTION>");

        sb.append("</SELECT>");

        return sb.toString();
    }
    
    public static String getRelativeTimeDDFull(String ddName, String selected) {
        return getRelativeTimeDDFull(ddName, selected, false);
    }

    public static String getRelativeTimeDDFull(String ddName, String selected, boolean mixedCaseAll) {
        StringBuffer sb = new StringBuffer();

        String bNotSelected = "";
        String bSelected = "SELECTED";
        String bAll = mixedCaseAll ? "ALL" : "All"; // Change the display text but not the internal value

        sb.append("<SELECT NAME='" + ddName + "'>");

        if (selected.equals("ALL"))
            sb.append("<OPTION value='ALL' " + bSelected + " >" + bAll + "</OPTION>");
        else
            sb.append("<OPTION value='ALL' " + bNotSelected + " >" + bAll + "</OPTION>");

        if (selected.equals("NM"))
            sb.append("<OPTION value='NM' " + bSelected
                    + " >Next Month</OPTION>");
        else
            sb.append("<OPTION value='NM' " + bNotSelected
                    + " >Next Month</OPTION>");

        if (selected.equals("NW"))
            sb.append("<OPTION value='NW' " + bSelected
                    + " >Next Week</OPTION>");
        else
            sb.append("<OPTION value='NW' " + bNotSelected
                    + " >Next Week</OPTION>");

        if (selected.equals("NY"))
            sb.append("<OPTION value='NY' " + bSelected
                    + " >Next Year</OPTION>");
        else
            sb.append("<OPTION value='NY' " + bNotSelected
                    + " >Next Year</OPTION>");

        if (selected.equals("TM"))
            sb.append("<OPTION value='TM' " + bSelected
                    + " >This Month</OPTION>");
        else
            sb.append("<OPTION value='TM' " + bNotSelected
                    + " >This Month</OPTION>");

        if (selected.equals("TW"))
            sb.append("<OPTION value='TW' " + bSelected
                    + " >This Week</OPTION>");
        else
            sb.append("<OPTION value='TW' " + bNotSelected
                    + " >This Week</OPTION>");

        if (selected.equals("TY"))
            sb.append("<OPTION value='TY' " + bSelected
                    + " >This Year</OPTION>");
        else
            sb.append("<OPTION value='TY' " + bNotSelected
                    + " >This Year</OPTION>");

        if (selected.equals("T"))
            sb.append("<OPTION value='T' " + bSelected + " >Today</OPTION>");
        else
            sb.append("<OPTION value='T' " + bNotSelected + " >Today</OPTION>");

        if (selected.equals("TO"))
            sb
                    .append("<OPTION value='TO' " + bSelected
                            + " >Tomorrow</OPTION>");
        else
            sb.append("<OPTION value='TO' " + bNotSelected
                    + " >Tomorrow</OPTION>");

        if (selected.equals("LM"))
            sb.append("<OPTION value='LM' " + bSelected
                    + " >Last Month</OPTION>");
        else
            sb.append("<OPTION value='LM' " + bNotSelected
                    + " >Last Month</OPTION>");

        if (selected.equals("LW"))
            sb.append("<OPTION value='LW' " + bSelected
                    + " >Last Week</OPTION>");
        else
            sb.append("<OPTION value='LW' " + bNotSelected
                    + " >Last Week</OPTION>");

        if (selected.equals("LY"))
            sb
                    .append("<OPTION value='LY' " + bSelected
                            + ">Last Year</OPTION>");
        else
            sb.append("<OPTION value='LY' " + bNotSelected
                    + ">Last Year</OPTION>");

        if (selected.equals("Y"))
            sb.append("<OPTION value='Y' " + bSelected + ">Yesterday</OPTION>");
        else
            sb.append("<OPTION value='Y' " + bNotSelected
                    + ">Yesterday</OPTION>");

        sb.append("</SELECT>");

        return sb.toString();
    }

    public static String getRelativeTimeDDSchedule(String ddName, String selected) {
        StringBuffer sb = new StringBuffer();

        String bNotSelected = "";
        String bSelected = "SELECTED";

        sb.append("<SELECT NAME='" + ddName + "'>");

        if (selected.equals("T"))
            sb.append("<OPTION value='T' " + bSelected + " >Today</OPTION>");
        else
            sb.append("<OPTION value='T' " + bNotSelected + " >Today</OPTION>");

        if (selected.equals("TO"))
            sb.append("<OPTION value='TO' " + bSelected + " >Tomorrow</OPTION>");
        else
            sb.append("<OPTION value='TO' " + bNotSelected + " >Tomorrow</OPTION>");

        if (selected.equals("N7"))
            sb.append("<OPTION value='N7' " + bSelected + " >Next 7 Days</OPTION>");
        else
            sb.append("<OPTION value='N7' " + bNotSelected + " >Next 7 Days</OPTION>");
        
        if (selected.equals("N14"))
            sb.append("<OPTION value='N14' " + bSelected + " >Next 14 Days</OPTION>");
        else
            sb.append("<OPTION value='N14' " + bNotSelected + " >Next 14 Days</OPTION>");

        if (selected.equals("NM"))
            sb.append("<OPTION value='NM' " + bSelected + " >Next Month</OPTION>");
        else
            sb.append("<OPTION value='NM' " + bNotSelected + " >Next Month</OPTION>");

        sb.append("</SELECT>");

        return sb.toString();
    }

    public static String getRelativeTime(Calendar cal, String timeCode) {
        //
        int month = 0;
        int yr = 0;
        int day = 0;
        int maxmonthday = 0;
        int day_of_week = 0;
        String returnStr = "";

        month = cal.get(Calendar.MONTH) + 1;
        yr = cal.get(Calendar.YEAR);
        day = cal.get(Calendar.DAY_OF_MONTH);

        // THIS MONTH

        if (timeCode.equals("TM")) {
            maxmonthday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            returnStr = " between  to_date('" + month + "/01/" + yr
                    + "','mm/dd/yyyy') and to_date('" + month + "/"
                    + maxmonthday + "/" + yr + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // THIS WEEK

        if (timeCode.equals("TW")) {
            day_of_week = cal.get(Calendar.DAY_OF_WEEK);
            cal.add(Calendar.DAY_OF_MONTH, -(day_of_week - 1));
            returnStr = " between to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            // for end date of the week
            cal.add(Calendar.DAY_OF_MONTH, 6);
            returnStr = returnStr + " and to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // THIS YEAR

        if (timeCode.equals("TY")) {
            returnStr = " between  to_date('01/01/" + yr
                    + "','mm/dd/yyyy') and to_date('12/31/" + yr
                    + "','mm/dd/yyyy')";
            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // TODAY

        if (timeCode.equals("T")) {
            returnStr = " = to_date('" + month + "/" + day + "/" + yr
                    + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // LAST WEEK

        if (timeCode.equals("LW")) {

            cal.add(Calendar.WEEK_OF_MONTH, -1);
            day_of_week = cal.get(Calendar.DAY_OF_WEEK);
            cal.add(Calendar.DAY_OF_MONTH, -(day_of_week - 1));
            returnStr = " between to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            cal.add(Calendar.DAY_OF_MONTH, 6);
            returnStr = returnStr + " and to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // LAST MONTH
        if (timeCode.equals("LM")) {
            cal.add(Calendar.MONTH, -1);
            maxmonthday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            returnStr = "  between  to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/01/" + cal.get(Calendar.YEAR)
                    + "','mm/dd/yyyy') and to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/" + maxmonthday + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // LAST YEAR
        if (timeCode.equals("LY")) {
            cal.add(Calendar.YEAR, -1);
            returnStr = " between  to_date('01/01/" + cal.get(Calendar.YEAR)
                    + "','mm/dd/yyyy') and to_date('12/31/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // YESTERDAY

        if (timeCode.equals("Y")) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
            returnStr = "  = to_date('" + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // TOMORROW

        if (timeCode.equals("TO")) {
            cal.add(Calendar.DAY_OF_MONTH, +1);
            returnStr = "  = to_date('" + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // NEXT WEEK

        if (timeCode.equals("NW")) {

            cal.add(Calendar.WEEK_OF_MONTH, +1);
            day_of_week = cal.get(Calendar.DAY_OF_WEEK);
            cal.add(Calendar.DAY_OF_MONTH, -(day_of_week - 1));
            returnStr = " between to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            cal.add(Calendar.DAY_OF_MONTH, 6);
            returnStr = returnStr + " and to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // NEXT MONTH
        if (timeCode.equals("NM")) {
            cal.add(Calendar.MONTH, +1);
            maxmonthday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            returnStr = " between  to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/01/" + cal.get(Calendar.YEAR)
                    + "','mm/dd/yyyy') and to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/" + maxmonthday + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // NEXT YEAR
        if (timeCode.equals("NY")) {
            cal.add(Calendar.YEAR, +1);
            returnStr = "  between  to_date('01/01/" + cal.get(Calendar.YEAR)
                    + "','mm/dd/yyyy') and to_date('12/31/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        return returnStr;

        // end of method

    }

    /**
     * More explicit version of getRelativeTime. It serves the same purpose as getRelativeTime
     * but this method is intended to get around some Oracle bugs by not using "between".
     *
     * @param lhs - "left-hand side" of an equation: like in lhs >= something
     * @param cal - Calendar with today's date for calculating the range
     * @param timeCode - keyword such as "TM" for This Month, "TW" for This Week, etc.
     * @return an expression that can be used in the where clause of SQL
     */
    public static String getRelativeTimeLong(String lhs, Calendar cal, String timeCode) {

        int month = 0;
        int yr = 0;
        int day = 0;
        int maxmonthday = 0;
        int day_of_week = 0;
        String returnStr = "";

        month = cal.get(Calendar.MONTH) + 1;
        yr = cal.get(Calendar.YEAR);
        day = cal.get(Calendar.DAY_OF_MONTH);

        // THIS MONTH
        if (timeCode.equals("TM")) {
            maxmonthday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            returnStr = " " + lhs + " >= to_date('" + month + "/01/" + yr + "','mm/dd/yyyy') "
                    + " and " + lhs + " <= to_date('" + month + "/"
                    + maxmonthday + "/" + yr + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;
        }

        // THIS WEEK
        if (timeCode.equals("TW")) {
            day_of_week = cal.get(Calendar.DAY_OF_WEEK);
            cal.add(Calendar.DAY_OF_MONTH, -(day_of_week - 1));
            returnStr = " " + lhs + " >= to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            // for end date of the week
            cal.add(Calendar.DAY_OF_MONTH, 6);
            returnStr += " and " + lhs + " <= to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // THIS YEAR
        if (timeCode.equals("TY")) {
            returnStr = " " + lhs + " >= to_date('01/01/" + yr + "','mm/dd/yyyy') "
                    +" and " + lhs +" <= to_date('12/31/" + yr + "','mm/dd/yyyy') ";
            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // TODAY
        if (timeCode.equals("T")) {
            returnStr = " " + lhs + " = to_date('" + month + "/" + day + "/" + yr
                    + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // LAST WEEK
        if (timeCode.equals("LW")) {

            cal.add(Calendar.WEEK_OF_MONTH, -1);
            day_of_week = cal.get(Calendar.DAY_OF_WEEK);
            cal.add(Calendar.DAY_OF_MONTH, -(day_of_week - 1));
            returnStr = " " + lhs + " >= to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            cal.add(Calendar.DAY_OF_MONTH, 6);
            returnStr += " and " + lhs + " <= to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // LAST MONTH
        if (timeCode.equals("LM")) {
            cal.add(Calendar.MONTH, -1);
            maxmonthday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            returnStr = " " + lhs + " >=  to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/01/" + cal.get(Calendar.YEAR)
                    + "','mm/dd/yyyy') and " + lhs + " <= to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/" + maxmonthday + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // LAST YEAR
        if (timeCode.equals("LY")) {
            cal.add(Calendar.YEAR, -1);
            returnStr = " " + lhs + " >= to_date('01/01/" + cal.get(Calendar.YEAR)
                    + "','mm/dd/yyyy') and " + lhs + " <= to_date('12/31/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // YESTERDAY
        if (timeCode.equals("Y")) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
            returnStr = " " + lhs + " = to_date('" + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // TOMORROW
        if (timeCode.equals("TO")) {
            cal.add(Calendar.DAY_OF_MONTH, +1);
            returnStr = " " + lhs + " = to_date('" + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // NEXT WEEK
        if (timeCode.equals("NW")) {

            cal.add(Calendar.WEEK_OF_MONTH, +1);
            day_of_week = cal.get(Calendar.DAY_OF_WEEK);
            cal.add(Calendar.DAY_OF_MONTH, -(day_of_week - 1));
            returnStr = " " + lhs +" >= to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            cal.add(Calendar.DAY_OF_MONTH, 6);
            returnStr += " and " + lhs + " <= to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // NEXT MONTH
        if (timeCode.equals("NM")) {
            cal.add(Calendar.MONTH, +1);
            maxmonthday = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            returnStr = " " + lhs + " >= to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/01/" + cal.get(Calendar.YEAR)
                    + "','mm/dd/yyyy') and " + lhs + " <= to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/" + maxmonthday + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }

        // NEXT YEAR
        if (timeCode.equals("NY")) {
            cal.add(Calendar.YEAR, +1);
            returnStr = " " + lhs + " >= to_date('01/01/" + cal.get(Calendar.YEAR)
                    + "','mm/dd/yyyy') and " + lhs + " <= to_date('12/31/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;

        }
        
        // NEXT 7 DAYS
        if (timeCode.equals("N7")) {
            cal.add(Calendar.DAY_OF_MONTH, 1);
            returnStr = " " + lhs +" >= to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            cal.add(Calendar.DAY_OF_MONTH, 6);
            returnStr += " and " + lhs + " <= to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;
        }
        
        // NEXT 14 DAYS
        if (timeCode.equals("N14")) {
            cal.add(Calendar.DAY_OF_MONTH, 1);
            returnStr = " " + lhs +" >= to_date('" + (cal.get(Calendar.MONTH) + 1)
                    + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";
            cal.add(Calendar.DAY_OF_MONTH, 13);
            returnStr += " and " + lhs + " <= to_date('"
                    + (cal.get(Calendar.MONTH) + 1) + "/"
                    + cal.get(Calendar.DAY_OF_MONTH) + "/"
                    + cal.get(Calendar.YEAR) + "','mm/dd/yyyy')";

            Rlog.debug("common", "rel date for code  " + timeCode + ": "
                    + returnStr);
            return returnStr;
        }

        return returnStr;
    }

    /*
     * Method to create a drop down @param name of the HTML element select
     * @param already selected value @param Ids of data value of dropdown @param
     * Desc - display value of dropdown
     */
//  Modified by Gopu to fix the bugzilla issue #2818 (Resize and make size static for the Study Number dropdown.)
    public static String createPullDown(String dName, int selValue,
            ArrayList id, ArrayList desc) {
        String strCodeId;
        Integer integerCodeId;

        StringBuffer mainStr = new StringBuffer();

        try {
            int counter = 0;
            String objType = "java.lang.Integer";
            boolean isString = false;
            int intcode = 0;

            // check whether ID is of type Integer or String
            if (id.size() > 0) {
                objType = (((Object) id.get(counter)).getClass()).getName();
            }

            if ((objType.toUpperCase()).equals("JAVA.LANG.STRING")) {
                isString = true;
            }

            mainStr.append("<SELECT NAME=" + dName + " STYLE='WIDTH:177px' >");

            Integer val = new java.lang.Integer(selValue);

            for (counter = 0; counter <= desc.size() - 1; counter++) {

                if (isString) {
                    strCodeId = (String) id.get(counter);
                    intcode = stringToNum(strCodeId);
                } else {
                    integerCodeId = (Integer) id.get(counter);
                    intcode = integerCodeId.intValue();

                }

                if (selValue == intcode) {
                    mainStr.append("<OPTION value = " + intcode + " SELECTED>"
                            + desc.get(counter) + "</OPTION>");
                } else {
                    mainStr.append("<OPTION value = " + intcode + ">"
                            + desc.get(counter) + "</OPTION>");
                }
            }
            if (val.toString().equals("") || val.toString().equals(null)
                    || val.toString().equals("0")) {
                mainStr
                		.append("<OPTION value='' SELECTED>").append(LC.L_Select_AnOption).append("</OPTION>");
            } else
                mainStr.append("<OPTION value='' >").append(LC.L_Select_AnOption).append("</OPTION>");

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in createPullDown()" + e;
        }
        return mainStr.toString();
    }

    public static String ArrayListToString(ArrayList srcList)
    {
    	return ArrayListToString(srcList,",");
    }

    public static String ArrayListToString(ArrayList srcList,String dlm)
    {
    	String objType="",idValue="",finalStr="";
    	if ((StringUtil.trueValue(dlm)).length()==0)
    		dlm=",";

    	if (srcList.size()>0)
            objType = (((Object) srcList.get(0)).getClass()).getName().toLowerCase();
        for (int i=0;i<srcList.size();i++)
        {
    	 if (objType.equals("java.lang.string"))
    		 idValue=(String)srcList.get(i);
    	else if (objType.equals("java.lang.integer"))
    			idValue=((Integer)srcList.get(i)).toString();

    	finalStr=(finalStr.length()==0)?idValue:finalStr+dlm+idValue;
    	 }


    return finalStr;
    }
    public static String createPullDownWAll(String dName, int selValue,
            ArrayList id, ArrayList desc) {
        String strCodeId;
        Integer integerCodeId;
        int selectFlag=0;

        StringBuffer mainStr = new StringBuffer();

        try {
            int counter = 0;
            String objType = "java.lang.Integer";
            boolean isString = false;
            int intcode = 0;

            String allValue=ArrayListToString(id,",");

            // check whether ID is of type Integer or String
            if (id.size() > 0) {
                objType = (((Object) id.get(counter)).getClass()).getName();
            }

            if ((objType.toUpperCase()).equals("JAVA.LANG.STRING")) {
                isString = true;
            }

            mainStr.append("<SELECT NAME=" + dName + " STYLE='WIDTH:177px' >");

            Integer val = new java.lang.Integer(selValue);
            mainStr.append("<OPTION value ='"+allValue +"' SELECTED>All</OPTION>");
            for (counter = 0; counter <= desc.size() - 1; counter++) {

                if (isString) {
                    strCodeId = (String) id.get(counter);
                    intcode = stringToNum(strCodeId);
                } else {
                    integerCodeId = (Integer) id.get(counter);
                    intcode = integerCodeId.intValue();

                }

                if (selValue == intcode) {
                    mainStr.append("<OPTION value = " + intcode + " SELECTED>"
                            + desc.get(counter) + "</OPTION>");
                    selectFlag=1;
                } else {
                    mainStr.append("<OPTION value = " + intcode + ">"
                            + desc.get(counter) + "</OPTION>");
                }
            }




            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in createPullDown()" + e;
        }
        return mainStr.toString();
    }


    // =========================================================================================================
    // JM : 04May05 for creating org drop down with additional item "All"
    /*
     * Method to create a drop down @param name of the HTML element select
     * @param already selected value @param Ids of data value of dropdown @param
     * Desc - display value of dropdown
     */
    public static String createPullDownOrg(String dName, int selValue,
            ArrayList id, ArrayList desc) {
        String strCodeId;
        Integer integerCodeId;

        StringBuffer mainStr = new StringBuffer();

        try {
            int counter = 0;
            String objType = "java.lang.Integer";
            boolean isString = false;
            int intcode = 0;

            // check whether ID is of type Integer or String
            if (id.size() > 0) {
                objType = (((Object) id.get(counter)).getClass()).getName();
            }

            if ((objType.toUpperCase()).equals("JAVA.LANG.STRING")) {
                isString = true;
            }

            mainStr.append("<SELECT NAME=" + dName + ">");

            Integer val = new java.lang.Integer(selValue);

            for (counter = 0; counter <= desc.size() - 1; counter++) {

                if (isString) {
                    strCodeId = (String) id.get(counter);
                    intcode = stringToNum(strCodeId);
                } else {
                    integerCodeId = (Integer) id.get(counter);
                    intcode = integerCodeId.intValue();

                }

                if (selValue == intcode) {
                    mainStr.append("<OPTION value = " + intcode + " SELECTED>"
                            + desc.get(counter) + "</OPTION>");
                } else {
                    mainStr.append("<OPTION value = " + intcode + ">"
                            + desc.get(counter) + "</OPTION>");
                }
            }
            // if (val.toString().equals("") || val.toString().equals(null) ||
            // val.toString().equals("0"))
            // JM : 04May05
            if (val.toString().equals("0")) {
                mainStr.append("<OPTION value ='0' SELECTED>All</OPTION>");
            } else
                mainStr.append("<OPTION value ='0' >All</OPTION>");

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in createPullDown()" + e;
        }
        return mainStr.toString();
    }

    // =========================================================================================================
    public static Vector ArrayListToVector(ArrayList al) {
        Vector vt = new Vector(50);
        vt.addAll(al);
        return vt;
    }

    public static boolean isEmpty(String param) {

        if ((!(param == null)) && (!(param.trim()).equals(""))
                && ((param.trim()).length() > 0))
            return false;
        else
            return true;

    }

    public static ArrayList strArrToArrayList(String[] param) {

        Arrays t = null;
        ArrayList toReturn = new ArrayList(t.asList(param));
        return toReturn;

    }

    public static ArrayList removeRange(ArrayList param, int startIndex,
            int lastIndex) {

        ArrayList toReturn = new ArrayList();
        int count = 0;
        for (count = 0; count < startIndex; count++) {
            toReturn.add(param.get(count));
        }

        for (int i = lastIndex; i < param.size(); i++) {
            toReturn.add(param.get(i));
        }
        return toReturn;

    }

    /*
     * Method to create a drop down with String as value @param name of the HTML
     * element select @param already selected value @param Ids of data value of
     * dropdown @param Desc - display value of dropdown
     */
    public static String createPullDownWithStr(String dName, String selValue,
            ArrayList id, ArrayList desc) {
        String idVal = "";

        StringBuffer mainStr = new StringBuffer();
        String objType = "java.lang.Integer";
        Integer integerCodeId;
        boolean isString = false;

        try {
            int counter = 0;
            // check whether ID is of type Integer or String

            mainStr.append("<SELECT NAME=" + dName + ">");

            for (counter = 0; counter <= desc.size() - 1; counter++) {
                objType = (((Object) id.get(counter)).getClass()).getName();
                if ((objType.toUpperCase()).equals("JAVA.LANG.STRING")) {
                    isString = true;
                } else {
                    isString = false;
                }

                if (isString) {
                    idVal = (String) id.get(counter);
                } else {
                    integerCodeId = (Integer) id.get(counter);
                    idVal = integerToString(integerCodeId);
                }

                if (selValue.equals(idVal)) {
                    mainStr.append("<OPTION value = '" + idVal + "' SELECTED>"
                            + desc.get(counter) + "</OPTION>");
                } else {
                    mainStr.append("<OPTION value = '" + idVal + "'>"
                            + desc.get(counter) + "</OPTION>");
                }
            }
            if (selValue == null || selValue.equals("")
                    || selValue.equals("null") || selValue.equals("0")) {
                mainStr
                        .append("<OPTION value='' SELECTED>").append(LC.L_Select_AnOption).append("</OPTION>");
            } else
                mainStr.append("<OPTION value='' >").append(LC.L_Select_AnOption).append("</OPTION>");

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            e.printStackTrace();
            return "Exception in createPullDown()" + e;

        }
        return mainStr.toString();
    }

    /*
     * Method to create a drop down without the 'Select an Option' in the drop
     * down @param name of the HTML element select @param already selected value
     * in String @param Ids of data value of dropdown @param Desc - display
     * value of dropdown
     */
    public static String createPullDownWithStrNoSelect(String dName,
            String selValue, ArrayList id, ArrayList desc) {
        String strCodeId;
        Integer integerCodeId;

        StringBuffer mainStr = new StringBuffer();

        try {
            int counter = 0;
            String objType = "java.lang.Integer";
            boolean isString = false;

            // check whether ID is of type Integer or String
            // comment here and check with each element. what if arraylist is
            // a mix of integer +String
            /*
             * if (id.size() > 0) { objType = (((Object)
             * id.get(counter)).getClass()).getName(); }
             *
             * if ((objType.toUpperCase()).equals("JAVA.LANG.STRING")) {
             * isString = true; }
             */

            mainStr.append("<SELECT id='" + dName + "' NAME='" + dName + "'>");

            for (counter = 0; counter <= desc.size() - 1; counter++) {
                objType = (((Object) id.get(counter)).getClass()).getName();
                if ((objType.toUpperCase()).equals("JAVA.LANG.STRING")) {
                    isString = true;
                } else {
                    isString = false;
                }

                if (isString) {
                    strCodeId = (String) id.get(counter);
                } else {
                    integerCodeId = (Integer) id.get(counter);
                    strCodeId = integerToString(integerCodeId);

                }

                if (selValue.equals(strCodeId)) {
                    mainStr.append("<OPTION value = " + strCodeId
                            + " SELECTED>" + desc.get(counter) + "</OPTION>");
                } else {
                    mainStr.append("<OPTION value = " + strCodeId + ">"
                            + desc.get(counter) + "</OPTION>");
                }
            }

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in createPullDownWithStrNoSelect()" + e;
        }
        return mainStr.toString();
    }

    /*
     * Method to create a drop down without the 'Select an Option' in the drop
     * down @param name of the HTML element select @param already selected value
     * @param Ids of data value of dropdown @param Desc - display value of
     * dropdown
     */
    public static String createPullDownNoSelect(String dName, int selValue,
            ArrayList id, ArrayList desc) {
        String strCodeId;
        Integer integerCodeId;

        StringBuffer mainStr = new StringBuffer();

        try {
            int counter = 0;
            String objType = "java.lang.Integer";
            boolean isString = false;
            int intcode = 0;

            // check whether ID is of type Integer or String
            if (id.size() > 0) {
                objType = (((Object) id.get(counter)).getClass()).getName();
            }

            if ((objType.toUpperCase()).equals("JAVA.LANG.STRING")) {
                isString = true;
            }

            mainStr.append("<SELECT NAME=" + dName + ">");

            Integer val = new java.lang.Integer(selValue);

            for (counter = 0; counter <= desc.size() - 1; counter++) {

                if (isString) {
                    strCodeId = (String) id.get(counter);
                    intcode = stringToNum(strCodeId);
                } else {
                    integerCodeId = (Integer) id.get(counter);
                    intcode = integerCodeId.intValue();

                }

                if (selValue == intcode) {
                    mainStr.append("<OPTION value = " + intcode + " SELECTED>"
                            + desc.get(counter) + "</OPTION>");
                } else {
                    mainStr.append("<OPTION value = " + intcode + ">"
                            + desc.get(counter) + "</OPTION>");
                }
            }

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in createPullDown()" + e;
        }
        return mainStr.toString();
    }



    public void populateCLOB( CLOB clob, String data ) throws IOException, SQLException {


        StringReader reader = new StringReader( data );

        Writer clobWriter = clob.getCharacterOutputStream( );

        // Get optimal buffer size to read/write data
        char[] buffer = new char[ clob.getBufferSize( ) ];
        int read = 0;
        int bufflen = buffer.length;

        // Read from String and write to CLOB
        while( (read = reader.read(buffer,0,bufflen)) > 0 ) {
          clobWriter.write( buffer, 0, read );
        }

        clobWriter.close( );
        reader.close( );

      }




    /** returns median value from an ArrayList of Integers. If an ArrayList of String type is passed, it will return 0*/
    public static float calculateMedian(ArrayList arNumbers)
    {
    	int count = 0;
    	float medianValue = 0;
    	int middlePos = 0;
    	float number1 = 0;
    	float number2 = 0;

    	String objType = "java.lang.Integer";
        boolean isString = false;

    	count = arNumbers.size();

    	if (count == 0)
    	{
    		return 0;
    	}
      //find the type of object in the arrayList

        objType = (((Object) arNumbers.get(0)).getClass()).getName();
    	if ((objType.toUpperCase()).equals("JAVA.LANG.STRING")) {
    	    isString = true;
    	} else {
    	    isString = false;
    	}

    	if (isString)
    	{
    		return 0;
    	}
    	//get object Array
    	Object[] obj = new Object[count];

    	obj = arNumbers.toArray();



    	//sort this list

    	if (obj != null)
    	{
	    		Arrays.sort(obj);



	    	//check if count is even or odd
	    	if (count % 2 == 0) //even number
	    	{
	    		middlePos = count/2;

	    		number1 = ((Float)obj[middlePos - 1 ]).floatValue();
	    		number2 =  ((Float)obj[middlePos ]).floatValue();

	    		medianValue = (number1 + number2)/2;
	    	}
	    	else //odd
	    	{
	    		middlePos = (count-1)/2 + 1;
	    		medianValue = ((Float)obj[middlePos - 1]).floatValue();
	    	}
    	}
    	return medianValue;
    }

    /**
     * by Sonia Abrol, 02/22/07
     * Method to serialize an object
     * */
    public static void serializeMe(Object obj, String filePath)
    {
    	 FileOutputStream fos = null;
         ObjectOutputStream out1 = null;
         try {
             fos = new FileOutputStream(filePath);
             out1 = new ObjectOutputStream(fos);
             out1.writeObject(obj);
             out1.close();
             // clean up
             fos = null;
             out1 = null;
         } catch (IOException ex) {
             System.out.println("exception in serializeMe"
                     + ex);
         }


    }

    /**
     * by Sonia Abrol, 02/22/07
     * Method to read a serialized object
     * */
    public static Object readSerializedObject(String filepath)
    {
    	 FileInputStream fis = null;
    	 ObjectInputStream in = null;
    	 Object obj = null;

    	  try
    	 {
    		  fis = new FileInputStream(filepath);
    	      in = new ObjectInputStream(fis);
    	      obj = in.readObject();
    	      in.close();
    	   }
    	 catch(IOException ex)
    	  {
    		 System.out.println("exception in readSerializedObject"
                     + ex);
    		 ex.printStackTrace();
    	   }
    	 catch(ClassNotFoundException ex)
    	 	{
    		 System.out.println("exception in readSerializedObject"
                     + ex);
    		 ex.printStackTrace();
    	    }
    	 return obj;

    }

    public static String getUniqueFileName(String prefixString,String ext)
    {
    	String filePath  = "";
    	String completeFileName  = "";
    	int sType = 0;
    	Calendar now = Calendar.getInstance();
    	if (StringUtil.isEmpty(prefixString))
    	{

    		prefixString = "adhoc";
    	}

    	 com.aithent.file.uploadDownload.Configuration.readSettings("eres");

         com.aithent.file.uploadDownload.Configuration
                 .readUploadDownloadParam(
                         com.aithent.file.uploadDownload.Configuration.FILE_UPLOAD_DOWNLOAD
                                 + "fileUploadDownload.xml", null);

         filePath = com.aithent.file.uploadDownload.Configuration.DOWNLOADFOLDER;

		  completeFileName = prefixString + "[" + now.get(now.DAY_OF_MONTH)
		         + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)
		         + now.get(now.HOUR_OF_DAY) + now.get(now.MINUTE)
		         + now.get(now.SECOND) + "]" + ext;

		 sType = EJBUtil.getSystemType();


		 if (sType == 0) {

			 completeFileName = filePath + "\\" + completeFileName;
		 } else {
			 completeFileName = filePath + "//" + completeFileName;
		 }
		 return completeFileName;

    }
    public static String typeCast(String data,String type){
    	String retData="";
    	if (type==null) retData=data;
    	if (type.equals("DATE"))
    	{
    		retData=DateUtil.format2DateFormat(data,"N");
    	} else
    	{
    	  retData=data;
    	}

    return	retData;
    }


    /* JM: 08Sep2010: #3315-part(14), sch_codelst Hide/unhide compliant:
     * Method to create a drop down with String as value, @param name of the HTML
     * element select, @param already selected value, @param Ids of data value of
     * dropdown, @param Desc - display value of dropdown, @param ifHide - codelst_hide value of the dd
     */
    public static String createPullDownWithStrIfHidden(String dName, String selValue,
            ArrayList id, ArrayList desc, ArrayList ifHide) {
        String idVal = "";

        StringBuffer mainStr = new StringBuffer();
        String objType = "java.lang.Integer";
        Integer integerCodeId;
        boolean isString = false;
        String hideStr = "";

        try {
            int counter = 0;
            // check whether ID is of type Integer or String

            mainStr.append("<SELECT NAME=" + dName + ">");

            for (counter = 0; counter <= desc.size() - 1; counter++) {
                objType = (((Object) id.get(counter)).getClass()).getName();
                if ((objType.toUpperCase()).equals("JAVA.LANG.STRING")) {
                    isString = true;
                } else {
                    isString = false;
                }

                hideStr = (String) ifHide.get(counter);

                if (isString) {
                    idVal = (String) id.get(counter);
                } else {
                    integerCodeId = (Integer) id.get(counter);
                    idVal = integerToString(integerCodeId);
                }

                if (selValue.equals(idVal)) {
                    mainStr.append("<OPTION value = '" + idVal + "' SELECTED>"
                            + desc.get(counter) + "</OPTION>");
                } else {

                	if (hideStr.equals("N")){

                		mainStr.append("<OPTION value = '" + idVal + "'>"
                				+ desc.get(counter) + "</OPTION>");
                	}
                }
            }
            if (selValue == null || selValue.equals("")
                    || selValue.equals("null") || selValue.equals("0")) {
                mainStr
                        .append("<OPTION value='' SELECTED>").append(LC.L_Select_AnOption).append("</OPTION>");
            } else
                mainStr.append("<OPTION value='' >").append(LC.L_Select_AnOption).append("</OPTION>");

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            e.printStackTrace();
            return "Exception in createPullDown()" + e;

        }
        return mainStr.toString();
    }


    /*
     * Method to create a drop down without the 'Select an Option' in the drop
     * down @param name of the HTML element select @param already selected value
     * in String @param Ids of data value of dropdown @param Desc - display
     * value of dropdown, @param Desc - display value of dropdown, @param ifHide - codelst_hide value of the dd
     */
    public static String createPullDownWithStrNoSelectIfHidden(String dName,
            String selValue, ArrayList id, ArrayList desc, ArrayList ifHide) {
        String strCodeId;
        Integer integerCodeId;
        String hideStr = "";


        StringBuffer mainStr = new StringBuffer();

        try {
            int counter = 0;
            String objType = "java.lang.Integer";
            boolean isString = false;

            hideStr = (String) ifHide.get(counter);

            mainStr.append("<SELECT id='" + dName + "' NAME='" + dName + "'>");

            for (counter = 0; counter <= desc.size() - 1; counter++) {
                objType = (((Object) id.get(counter)).getClass()).getName();
                if ((objType.toUpperCase()).equals("JAVA.LANG.STRING")) {
                    isString = true;
                } else {
                    isString = false;
                }

                if (isString) {
                    strCodeId = (String) id.get(counter);
                } else {
                    integerCodeId = (Integer) id.get(counter);
                    strCodeId = integerToString(integerCodeId);

                }

                if (selValue.equals(strCodeId)) {
                    mainStr.append("<OPTION value = " + strCodeId
                            + " SELECTED>" + desc.get(counter) + "</OPTION>");
                } else {

                	if (hideStr.equals("N")){
                		mainStr.append("<OPTION value = " + strCodeId + ">"+ desc.get(counter) + "</OPTION>");
                	}
                }
            }

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in createPullDownWithStrNoSelectIfHidden()" + e;
        }
        return mainStr.toString();
    }
	
    public static JSONObject hashMapToJSON(HashMap map) {
	    JSONObject json = new JSONObject();
	    for(Object key : map.keySet()) {
	        if (key == null || !(key instanceof String)) { continue; }
	        try {
                json.put((String)key, map.get(key));
            } catch (Exception e) {}
	    }
	    return json;
	}
    
    public static CtrpDraftAgent getCtrpDraftAgentHome() {
        try {
            InitialContext initial = new InitialContext();
            return (CtrpDraftAgent) initial.lookup(JNDINames.CtrpDraftAgentHome);
        } catch (Exception e) {
            Rlog.fatal("common", "gen ex=" + e);
            return null;
        }
    }
    public static UsrSiteGrpAgentRObj getUsrSiteGrpAgentBean() {
    	try {
    		InitialContext initial = new InitialContext();
    		return (UsrSiteGrpAgentRObj) initial.lookup(JNDINames.UsrSiteGrpAgentBean);
    	} catch (Exception e) {
    		Rlog.fatal("common", "gen ex=" + e);
    		return null;
    	}
    }
    public static AuditRowEresAgent getAuditRowEresAgentBean() {
    	try {
    		InitialContext initial = new InitialContext();
    		return (AuditRowEresAgent) initial.lookup(JNDINames.AuditRowEresAgentHome);
    	} catch (Exception e) {
    		Rlog.fatal("common", "gen ex=" + e);
    		return null;
    	}
    }
    // END OF CLASS

}