/*
 * Classname			StudyRightsRObj.class
 * 
 * Version information   
 *
 * Date					03/15/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.studyRightsAgent;

import javax.ejb.Remote;

import com.velos.eres.business.studyRights.impl.StudyRightsBean;

/**
 * Remote interface for studyRightsAgentRObj session EJB
 * 
 * @author Deepali
 */
@Remote
public interface StudyRightsAgentRObj {
    /**
     * gets the Rights details of a Study
     */
    public StudyRightsBean getStudyRightsDetails(int id);

    /**
     * sets the Rights detailsof a Study
     */
    /* public int setStudyRightsDetails(StudyRightsBean study); */

    /**
     * calls updateStudyRights on entity bean
     */

    public int updateStudyRights(StudyRightsBean ssk);

    public StudyRightsBean getGrpSupUserStudyRightsDetails(int id);

    /* public int setGrpSupUserStudyRightsDetails(StudyRightsBean ssk); */

    public int updateGrpSupUserStudyRights(StudyRightsBean ssk);

}
