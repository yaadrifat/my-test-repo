/*
 * Classname			StudyStatusAgentBean.class
 * 
 * Version information
 *
 * Date					02/26/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.studyStatusAgent.impl;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.StudyStatusDao;
import com.velos.eres.business.studyStatus.impl.StudyStatusBean;
import com.velos.eres.service.studyStatusAgent.StudyStatusAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.Configuration;
import com.velos.eres.service.util.StringUtil;

/**
 * The stateless session EJB StudyStatusAgentBean acting as a facade for the
 * StudyStatus entity CMP.
 * 
 * @author sonia sahni
 */

@Stateless
public class StudyStatusAgentBean implements StudyStatusAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;

    public StudyStatusBean getStudyStatusDetails(int id) {
        StudyStatusBean retrieved = null; // 

        try {
            Rlog.debug("studystatus", "IN TRY OF SESSION BEAN");

            retrieved = (StudyStatusBean) em.find(StudyStatusBean.class,
                    new Integer(id));

        } catch (Exception e) {
            // e.printStackTrace();
            Rlog.fatal("studystatus", "EXCEPTION IN GETTING STUDY STATUS" + e);
        }

        return retrieved;
    }

    /**
     * Calls setStudyStatusDetails() on StudyStatus Entity Bean. Creates a new
     * status with the values in the StateKeeper object.
     * 
     * @param ssk
     *            a StudyStatusStateKeeper object containing Study Status
     *            attributes.
     */
    /*
     * Modified by Sonia, 08/14/04, added logic to manage end dates of study
     * status records, added javadoc
     */
    /*
     * Modified by Sonia, 08/30/04 added logic to find the latest status, and
     * set its end date to null if its not null, this way we should have atleast
     * one cuurrent status
     */
    /*
     * Modified by Dylan, 12/14/10 added a new query for old study statuses that 
     * checks for a study status with current flag of one when the provided StudyStatusBean
     * has a current flag of one (since we should only have 1 current status.)
     * 
     * Also, changed signature to return primary key of new status. Returns -1 on failure.
     */
    public int setStudyStatusDetails(StudyStatusBean ssk) {
    	int newPk = 0;
    	String dateFormat = Configuration.getAppDateFormat();
        if (dateFormat.indexOf("MMM") != -1)
        	dateFormat = StringUtil.replace(dateFormat,"MMM","Mon");
    	try {

            //get the last isCurrent Status and update it to 0
            String newStatIsCurrentFlag = ssk.getCurrentStat();
            
            if (StringUtil.isEmpty(newStatIsCurrentFlag))
            {
            	newStatIsCurrentFlag="0";
            }

            if ( newStatIsCurrentFlag.equals("1") )
            {

            	Query query = em.createNamedQuery("findByIsCurrentFlag");

            	query.setParameter("FK_STUDY", ssk.getStatStudy());
            	query.setParameter("IS_CURRENT_FLAG", ssk.getCurrentStat());

            	ArrayList<StudyStatusBean> list = (ArrayList<StudyStatusBean>) query.getResultList();
            	if (list == null)
            		list = new ArrayList<StudyStatusBean>();

            	if (list.size() > 0)
            	{
            		//get the bean

            		StudyStatusBean oldIsCurrentFlagStat = (StudyStatusBean) list.get(0);

            		//set the flag to 0
            		oldIsCurrentFlagStat.setCurrentStat("0");
            		em.merge(oldIsCurrentFlagStat);
            	}

            } // end of if newStatIsCurrentFlag.equals("1")
    		
            StudyStatusBean ssbNew = new StudyStatusBean();

            ssbNew.updateStudyStatus(ssk);
            em.persist(ssbNew);

            
            
            int studyId = 0;

            StudyStatusBean prevCurStat = null;
            StudyStatusBean nextStat = null;
            String latestEndDate = "";
            StudyStatusBean latestStat = null;
            ArrayList list = new ArrayList();
            Query query = null;

            // find the last current status and update its end date
            Rlog.debug("studystatus", "before getid*****");

            newPk = ssbNew.getId();

            Rlog.debug("studystatus", "new pk******" + newPk);
            studyId = StringUtil.stringToNum(ssbNew.getStatStudy());

            Rlog.debug("studystatus", "studyId ******" + studyId);
            // study pk
            query = em.createNamedQuery("findByPreviousCurrentStatusProt");
            query.setParameter("fk_study", studyId);
            query.setParameter("begindate", ssbNew.returnStatStartDate());
            query.setParameter("pk_studystat", newPk);
            query.setParameter("date_format",dateFormat);
            
            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                prevCurStat = (StudyStatusBean) list.get(0);
                prevCurStat.updateStatEndDate(ssbNew.returnStatStartDate());
            } else // the start date of this status exists between 2 existing
            // status records or is the first record
            {

                // get the previous record to change its end date

                // find the a previous status from this new start date and
                // update the previous status's end date
                Rlog.debug("studystatus", "in else ******");

                query = em.createNamedQuery("findByPreviousStatusProt");
                query.setParameter("fk_study", studyId);
                query.setParameter("begindate", ssbNew.returnStatStartDate());
                query.setParameter("pk_studystat", newPk);
                query.setParameter("date_format",dateFormat);
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    prevCurStat = (StudyStatusBean) list.get(0);
                    prevCurStat.updateStatEndDate(ssbNew.returnStatStartDate());
                }
                // ////////

                query = em.createNamedQuery("findByNextStatusProt");
                query.setParameter("fk_study", studyId);
                query.setParameter("begindate", ssbNew.returnStatStartDate());
                query.setParameter("pk_studystat", newPk);
                query.setParameter("date_format",dateFormat);
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    nextStat = (StudyStatusBean) list.get(0);
                    // a next status found,
                    // update the new status's end date with the next status's
                    // start date
                    ssbNew.updateStatEndDate(nextStat.returnStatStartDate());
                }

                // ////////////
            }
            // find the latest status, and set its end date to null if its not
            // null

            try {
                query = em.createNamedQuery("findByLatestStatusProt");
                query.setParameter("fk_study", studyId);

                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    latestStat = (StudyStatusBean) list.get(0);

                    // a latest status found, get its end date
                    latestEndDate = latestStat.returnStatEndDate();

                    if (!StringUtil.isEmpty(latestEndDate)) {
                        Rlog.debug("studystatus", "in if latestEndDate");
                        latestStat.updateStatEndDate("");
                        Rlog.debug("studystatus", "in if after setStatEndDate");
                    }

                }

            } catch (Exception ex) {
                latestStat = null;
            }
            // end of try
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "EXCEPTION IN SETTING STUDY STATUS: setStudyStatusDetails"
                            + e);
            e.printStackTrace();
            newPk = -1;
        }

        return newPk;
    }

    /**
     * Calls updateStudyStatus() on StudyStatus Entity Bean. Updates the study
     * status with the values in the StudyStatusStateKeeper object.
     * 
     * @param ssk
     *            A StudyStatusStateKeeper object containing StudyStatus
     *            attributes
     * @return int - 0 if successful; -2 for Exception;
     */
    /*
     * Modified by Sonia, 08/14/04, added logic to manage end dates of study
     * status records, added javadoc
     */
    /*
     * Modified by Sonia, 08/30/04 added logic to find the latest status, and
     * set its end date to null if its not null, this way we should have atleast
     * one cuurrent status
     */
    /*
     * Modified by Sonia, 09/14/04 when finding the next status do not set its
     * stataus end date to null (if any status is found)
     */
    public int updateStudyStatus(StudyStatusBean ssk) {
        StudyStatusBean ssObj = null; // 
        // Object
        int ret = 0;
        int studystatPkId = 0;
        int studyId = 0;

        StudyStatusBean prevCurStat = null;
        StudyStatusBean nextStat = null;
        StudyStatusBean latestStat = null;
        String oldStartDate = "";
        String newStartDate = "";
        String oldEndDate = "";
        String latestEndDate = "";
        ArrayList list = new ArrayList();
        Query query = null;
        String dateFormat = Configuration.getAppDateFormat();
        if (dateFormat.indexOf("MMM") != -1)
        	dateFormat = StringUtil.replace(dateFormat,"MMM","Mon");

        try {

            ssObj = (StudyStatusBean) em.find(StudyStatusBean.class, ssk
                    .getId());

            // get old start date and end date
            oldStartDate = ssObj.returnStatStartDate();
            oldEndDate = ssObj.returnStatEndDate();
            // get new start date

            newStartDate = ssk.returnStatStartDate();

            ret = ssObj.updateStudyStatus(ssk);

            // if there is a change in the start date

            studystatPkId = ssk.getId();
            studyId = StringUtil.stringToNum(ssk.getStatStudy());

            if (!newStartDate.equals(oldStartDate)) {
                // find the a previous status from this new start date and
                // update the previous status's end date

                query = em.createNamedQuery("findByPreviousStatusProt");
                query.setParameter("fk_study", studyId);
                query.setParameter("begindate", newStartDate);
                query.setParameter("pk_studystat", studystatPkId);
                query.setParameter("date_format",dateFormat);
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    prevCurStat = (StudyStatusBean) list.get(0);
                    prevCurStat.updateStatEndDate(ssObj.returnStatStartDate());
                }

                // see if the the start date of this status exists between 2
                // existing status records

                query = em.createNamedQuery("findByNextStatusProt");
                query.setParameter("fk_study", studyId);
                query.setParameter("begindate", newStartDate);
                query.setParameter("pk_studystat", studystatPkId);
                query.setParameter("date_format",dateFormat);
                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    nextStat = (StudyStatusBean) list.get(0);
                    // a next status found,
                    // update the new status's end date with the next status's
                    // start date
                    ssObj.updateStatEndDate(nextStat.returnStatStartDate());
                } else // now this status has become the current status because
                // there is no next record
                {
                    ssObj.updateStatEndDate(null);
                    Rlog
                            .debug("studystatus",
                                    "***update study stat*** findByNextStatusProt status, did not get any next status ");
                }

                // find the latest status, and set its end date to null if its
                // not null

                try {
                    query = em.createNamedQuery("findByLatestStatusProt");
                    query.setParameter("fk_study", studyId);

                    list = (ArrayList) query.getResultList();
                    if (list == null)
                        list = new ArrayList();
                    if (list.size() > 0) {
                        latestStat = (StudyStatusBean) list.get(0);

                        // a latest status found, get its end date
                        latestEndDate = latestStat.returnStatEndDate();

                        if (!StringUtil.isEmpty(latestEndDate)) {
                            Rlog.debug("studystatus", "in if latestEndDate");
                            latestStat.updateStatEndDate("");
                            Rlog.debug("studystatus",
                                    "in if after setStatEndDate");
                        }

                    }

                } catch (Exception ex) {
                    latestStat = null;
                    Rlog
                            .debug(
                                    "studystatus",
                                    "***update study stat*** findByLatestStatusProt status, did not get any latest status");
                }

            } // end of if for checking if status is changed

            return ret;
        } catch (Exception e) {
            // e.printStackTrace();
            Rlog.fatal("studystatus", "EXCEPTION IN UPDATING STUDY STATUS" + e);
            return -2;
        }
    }

    // added on 04/10/01 to fetch the details of studystatus
    // with a perticular study id dinesh

    public StudyStatusDao getStudy(int study) {
        try {
            Rlog.debug("studystatus", "session bean: outside try block");

            StudyStatusDao studystatusDao = new StudyStatusDao();
            Rlog.debug("studystatus", "before calling the get study");
            studystatusDao.getStudy(study);
            Rlog.debug("studystatus", "before returning from the getStudy");

            return studystatusDao;
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In getStudy in StudyStatusAgentBean " + e);
        }
        return null;
    }

    public StudyStatusDao getStudyStatusDesc(int study, int siteId, int usr,
            int accId) {
        try {
            Rlog.debug("studystatus", "session bean: outside try block");

            StudyStatusDao studystatusDao = new StudyStatusDao();
            Rlog.debug("studystatus", "before calling the get study");
            studystatusDao.getStudyStatusDesc(study, siteId, usr, accId);
            Rlog.debug("studystatus", "before returning from the getStudy");
            return studystatusDao;
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In getStudy in StudyStatusAgentBean " + e);
        }
        return null;
    }

    public int checkStatus(String studyId) {
        try {
            Rlog.debug("studystatus",
                    "StudyStatusAgentBean.checkStatus : inside try block");
            StudyStatusDao studystatusDao = new StudyStatusDao();
            Rlog
                    .debug("studystatus",
                            "StudyStatusAgentBean.checkStatus before calling the get study");
            return studystatusDao.checkStatus(studyId);
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In StudyStatusAgentBean.checkStatus  " + e);
        }
        return 0;
    }

    /**
     * Removes Study Status *
     * 
     * @param studyStatusId
     *            Study Status Id to be removed
     * @return int :0 if successful else it returns -1
     */
    /*
     * Modified by Sonia, 08/14/04, added logic to manage end dates of study
     * status records, added javadoc
     */
    /*
     * Modified by Sonia, 08/30/04 added logic to find the latest status, and
     * set its end date to null if its not null, this way we should have atleast
     * one cuurrent status
     */ 
     
    public int studyStatusDelete(int studyStatusId) {
        StudyStatusBean ssObj = null; // Study Status Entity Bean Remote

        ArrayList list = new ArrayList();
        Query query = null;
        // Object

        // int maxId = 0;
        String statusType = "";
        int studyId = 0;
        String nextStartDate = "";
        Enumeration enum_velos;

        StudyStatusBean prevCurStat = null;
        StudyStatusBean nextStat = null;
        StudyStatusBean latestStat = null;
        String latestEndDate = "";
        String dateFormat = Configuration.getAppDateFormat();
        if (dateFormat.indexOf("MMM") != -1)
        	dateFormat = StringUtil.replace(dateFormat,"MMM","Mon");

        try {
            ssObj = (StudyStatusBean) em.find(StudyStatusBean.class,
                    studyStatusId);

            // update end dates
            studyId = StringUtil.stringToNum(ssObj.getStatStudy());

            // see if the the start date of the deleted status exists between 2
            // existing status records

            query = em.createNamedQuery("findByNextStatusProt");
            query.setParameter("fk_study", studyId);
            query.setParameter("begindate", ssObj.returnStatStartDate());
            query.setParameter("pk_studystat", studyStatusId);
            query.setParameter("date_format",dateFormat);
            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                nextStat = (StudyStatusBean) list.get(0);
                // a next status found,
                // get next start date
                nextStartDate = nextStat.returnStatStartDate();
            }

            // find the a previous status from the deleted start date and update
            // the previous status's end date

            query = em.createNamedQuery("findByPreviousStatusProt");
            query.setParameter("fk_study", studyId);
            query.setParameter("begindate", ssObj.returnStatStartDate());
            query.setParameter("pk_studystat", studyStatusId);
            query.setParameter("date_format",dateFormat);
            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                prevCurStat = (StudyStatusBean) list.get(0);
                prevCurStat.updateStatEndDate(nextStartDate);
            }

            // /////////

            StudyStatusDao studyStDao = new StudyStatusDao();
            /* method to get the codelst subtype of the status to be deleted */
            statusType = studyStDao.getCodelstStatusType(studyStatusId, StringUtil
                    .stringToNum(ssObj.getStatStudy()));
            em.remove(ssObj);

            // find the latest status, and set its end date to null if its not
            // null

            try {
                query = em.createNamedQuery("findByLatestStatusProt");
                query.setParameter("fk_study", studyId);

                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    latestStat = (StudyStatusBean) list.get(0);

                    // a latest status found, get its end date
                    latestEndDate = latestStat.returnStatEndDate();

                    if (!StringUtil.isEmpty(latestEndDate)) {
                        Rlog.debug("studystatus", "in if latestEndDate");
                        latestStat.updateStatEndDate("");
                        Rlog.debug("studystatus",
                                "in if after updateStatEndDate");
                    }

                }

            } catch (Exception ex) {
                latestStat = null;
            }
            // //////////////

            // if status is active then call method setActualDateToNull which
            // further calls SP
            if (statusType.equals("active")) {
                studyStDao.setActualDateToNull(studyId, statusType);
            }
            Rlog.debug("studystatus",
                    "StudyStatusAgentBean.studyStatusDelete line 5");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception in studyStatusDelete() in StudyStatusAgentBean"
                            + e);
            return -1;
        }

    }   
    
 // Overloaded for INF-18183 ::: Akshi
    public int studyStatusDelete(int studyStatusId,Hashtable<String, String> args) {
        StudyStatusBean ssObj = null; // Study Status Entity Bean Remote
        ArrayList list = new ArrayList();
        Query query = null;
        // Object
        // int maxId = 0;
        String statusType = "";
        int studyId = 0;
        String nextStartDate = "";
        Enumeration enum_velos;
        StudyStatusBean prevCurStat = null;
        StudyStatusBean nextStat = null;
        StudyStatusBean latestStat = null;
        String latestEndDate = "";
        String dateFormat = Configuration.getAppDateFormat();
        if (dateFormat.indexOf("MMM") != -1)
        	dateFormat = StringUtil.replace(dateFormat,"MMM","Mon");
        try {
        	AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
            String getRidValue= AuditUtils.getRidValue("ER_STUDYSTAT","eres","PK_STUDYSTAT="+studyStatusId);/*Fetches the RID/PK_VALUE*/
            audit = new AuditBean("ER_STUDYSTAT",String.valueOf(studyStatusId),getRidValue,
        			userID,currdate,appmodule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
            em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/           
            ssObj = (StudyStatusBean) em.find(StudyStatusBean.class,
                    studyStatusId);
            // update end dates
            studyId = StringUtil.stringToNum(ssObj.getStatStudy());
            // see if the the start date of the deleted status exists between 2
            // existing status records

            query = em.createNamedQuery("findByNextStatusProt");
            query.setParameter("fk_study", studyId);
            query.setParameter("begindate", ssObj.returnStatStartDate());
            query.setParameter("pk_studystat", studyStatusId);
            query.setParameter("date_format",dateFormat);
            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                nextStat = (StudyStatusBean) list.get(0);
                // a next status found,
                // get next start date
                nextStartDate = nextStat.returnStatStartDate();
            }
            // find the a previous status from the deleted start date and update
            // the previous status's end date

            query = em.createNamedQuery("findByPreviousStatusProt");
            query.setParameter("fk_study", studyId);
            query.setParameter("begindate", ssObj.returnStatStartDate());
            query.setParameter("pk_studystat", studyStatusId);
            query.setParameter("date_format",dateFormat);
            list = (ArrayList) query.getResultList();
            if (list == null)
                list = new ArrayList();
            if (list.size() > 0) {
                prevCurStat = (StudyStatusBean) list.get(0);
                prevCurStat.updateStatEndDate(nextStartDate);
            }
            // /////////
            StudyStatusDao studyStDao = new StudyStatusDao();
            /* method to get the codelst subtype of the status to be deleted */
            statusType = studyStDao.getCodelstStatusType(studyStatusId, StringUtil
                    .stringToNum(ssObj.getStatStudy()));
            em.remove(ssObj);

            // find the latest status, and set its end date to null if its not
            // null

            try {
                query = em.createNamedQuery("findByLatestStatusProt");
                query.setParameter("fk_study", studyId);

                list = (ArrayList) query.getResultList();
                if (list == null)
                    list = new ArrayList();
                if (list.size() > 0) {
                    latestStat = (StudyStatusBean) list.get(0);

                    // a latest status found, get its end date
                    latestEndDate = latestStat.returnStatEndDate();

                    if (!StringUtil.isEmpty(latestEndDate)) {
                        Rlog.debug("studystatus", "in if latestEndDate");
                        latestStat.updateStatEndDate("");
                        Rlog.debug("studystatus",
                                "in if after updateStatEndDate");
                    }
                }
            } catch (Exception ex) {
                latestStat = null;
            }
            // //////////////

            // if status is active then call method setActualDateToNull which
            // further calls SP
            if (statusType.equals("active")) {
                studyStDao.setActualDateToNull(studyId, statusType);
            }
            Rlog.debug("studystatus",
                    "StudyStatusAgentBean.studyStatusDelete line 5");
            return 0;
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("studystatus",
                    "Exception in studyStatusDelete() in StudyStatusAgentBean"
                            + e);
            return -1;
        }
    }
    
    public int getFirstActiveEnrollPK(int studyId) {
        int ret = 0;
        try {

            StudyStatusDao studystatusDao = new StudyStatusDao();

            ret = studystatusDao.getFirstActiveEnrollPK(studyId);

        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In getStudy in StudyStatusAgentBean " + e);
        }
        return ret;
    }

    public String getMinActiveDate(int studyId) {
        String ret = "";
        try {

            StudyStatusDao studystatusDao = new StudyStatusDao();

            ret = studystatusDao.getMinActiveDate(studyId);

        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In getMinActiveDate in StudyStatusAgentBean "
                            + e);
        }
        return ret;
    }

    public String getMinPermanentClosureDate(int studyId) {
        String ret = "";
        try {
            StudyStatusDao studystatusDao = new StudyStatusDao();

            ret = studystatusDao.getMinPermanentClosureDate(studyId);

        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In getMinPermanentClosureDate in StudyStatusAgentBean "
                            + e);
        }
        return ret;
    }

    public int getCountStudyStat(int studyId) {
        int ret = 0;
        try {
            StudyStatusDao studystatusDao = new StudyStatusDao();

            ret = studystatusDao.getCountStudyStat(studyId);

        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In getCountStudyStat in StudyStatusAgentBean "
                            + e);
        }
        return ret;
    }

    public StudyStatusBean findByAtLeastOneStatus(int studyId, String codelstId) {
        ArrayList list = null;
        Query query = null;
        CodeDao codeDao=new CodeDao();

        query = em.createNamedQuery("findByAtLeastOneStatusProt");
        query.setParameter("fk_study", new Integer(studyId));
        query.setParameter("pk_codelst", codeDao.getCodeId("studystat",codelstId));
        

        list = (ArrayList) query.getResultList();
        if (list == null)
            list = new ArrayList();
        if (list.size() > 0) {
            return (StudyStatusBean) list.get(0);
        } else
            return null;

    }
    
    public int getCountByOrgStudyStat(int studyId,int orgId,String studyTypeList,String flag) {
        int ret = 0;
        try {
            StudyStatusDao studystatusDao = new StudyStatusDao();

            ret = studystatusDao.getCountByOrgStudyStat(studyId,orgId,studyTypeList, flag);

        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In getCountByOrgStat in StudyStatusAgentBean "
                            + e);
        }
        return ret;
    }

    
    public StudyStatusDao getDAOByOrgStudyStat(int studyId,int orgId,String studyTypeList,String flag) {
        
        try {
            StudyStatusDao studystatusDao = new StudyStatusDao();

             studystatusDao.getCountByOrgStudyStat(studyId,orgId,studyTypeList, flag);
             
             return studystatusDao;

        } catch (Exception e) {
            Rlog.fatal("studystatus",
                    "Exception In getCountByOrgStat in StudyStatusAgentBean "
                            + e);
        }
        return null;
    }


}// end of class
