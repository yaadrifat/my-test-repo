/*
 * Classname			ReportAgent.class
 * 
 * Version information   
 *
 * Date					03/03/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.reportAgent;

import javax.ejb.Remote;

import com.velos.eres.business.common.ReportDaoNew;

/**
 * Remote interface for ReportAgent session EJB
 * 
 * @author Sunita Pahwa
 */

@Remote
public interface ReportAgentRObj {

    // get the XMLs for a report id
    public ReportDaoNew getRepXml(int repId, int accId, String args);

    // get the XSLs for a report id
    public ReportDaoNew getRepXsl(int xslId);

    // get the XSLs for a report id
    public ReportDaoNew getRepXslList(int repId);

    // get a list of all reports
    public ReportDaoNew getAllRep(int accId);

    // get header and footer
    public ReportDaoNew getRepHdFtr(int repId, int accId, int userId);

    // get XML for milestone reports
    public ReportDaoNew getRepMileXml(String reportType, int studyId,
            int patId, String startDate, String endDate, String intervalType,
            int orgId);
    public ReportDaoNew getFilterColumns(String repCat) ;

}