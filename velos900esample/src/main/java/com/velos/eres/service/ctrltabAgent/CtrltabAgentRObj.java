/*
 * Classname			CtrltabAgent.class
 * 
 * Version information   
 *
 * Date					05/18/2006
 * 
 * Copyright notice
 */

package com.velos.eres.service.ctrltabAgent;


import javax.ejb.Remote;

import com.velos.eres.business.ctrltab.impl.CtrltabBean;


/**
 * Remote interface for CtrltabAgent session EJB
 * 
 * @author KM
 */
@Remote
public interface CtrltabAgentRObj {
    /**
     * gets the ctrltab details
     */
    CtrltabBean getCtrltabDetails(int ctrltabId);

    /**
     * sets the ctrltab details
     */
    public int setCtrltabDetails(CtrltabBean ctrltab);

    /**
     * updates the ctrltab
     */

    public int updateCtrltab(CtrltabBean ctsk);

    
} 
