/*
 * Classname			StorageAllowedItemsAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					08/13/2007
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.storageAllowedItemsAgent;

import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.eres.business.common.StorageAllowedItemsDao;
import com.velos.eres.business.storageAllowedItems.impl.StorageAllowedItemsBean;

/**
 * Remote interface for StorageAllowedItemsAgentBean session EJB
 * 
 * @author Khader
 * @version 1.0, 08/13/2007
 */
@Remote
public interface StorageAllowedItemsAgentRObj {
	
    public int setStorageAllowedItemsDetails(StorageAllowedItemsBean storageAllowedItems);
    public int updateStorageAllowedItems(StorageAllowedItemsBean storageAllowedItems);
    public StorageAllowedItemsDao getStorageAllowedItemValue(int pkStorage);
    // Overloaded for INF-18183 ::: Akshi
    public void deleteStorageAllowedItems(int fkStorage,Hashtable<String, String> args);
    
}
