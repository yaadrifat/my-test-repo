/*
 * Classname			StudyNotifyAgentBean.class 
 * 
 * Version information
 *
 * Date					03/16/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.studyNotifyAgent.impl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.eres.business.common.StudyNotifyDao;
import com.velos.eres.business.studyNotify.impl.StudyNotifyBean;
import com.velos.eres.service.studyNotifyAgent.StudyNotifyAgentRObj;
import com.velos.eres.service.util.Rlog;

/**
 * The stateless session EJB acting as a facade for the entity BMP.<br>
 * <br>
 * 
 * @author sajal
 */
@Stateless
public class StudyNotifyAgentBean implements StudyNotifyAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    /**
     * Looks up on the StudyNotify id parameter passed in and constructs and
     * returns a StudyNotify state keeper. containing all the details of the
     * studyNotify<br>
     * 
     * @param studyNotifyId
     *            the StudyNotify id
     */
    public StudyNotifyBean getStudyNotifyDetails(int studyNotifyId) {

        try {
            return (StudyNotifyBean) em.find(StudyNotifyBean.class,
                    new Integer(studyNotifyId));
        } catch (Exception e) {
            Rlog.fatal("studyNotify",
                    "Error in getStudyNotifyDetails() in StudyNotifyAgentBean"
                            + e);
            return null;
        }

    }

    /**
     * Sets an StudyNotify.
     * 
     * @param a
     *            studyNotify state keeper containing studyNotify attributes to
     *            be set.
     * @return void
     */

    public int setStudyNotifyDetails(StudyNotifyBean snsk) {
        StudyNotifyBean studyNotify = new StudyNotifyBean();
        try {

            studyNotify.updateStudyNotify(snsk);
            em.persist(studyNotify);
            return studyNotify.getStudyNotifyId();
        } catch (Exception e) {
            Rlog.fatal("studyNotify",
                    "Error in setStudyNotifyDetails() in StudyNotifyAgentBean "
                            + e);
        }
        return 0;
    }

    public int updateStudyNotify(StudyNotifyBean snsk) {

        StudyNotifyBean studyNotify = new StudyNotifyBean();
        int output;

        try {
            studyNotify = (StudyNotifyBean) em.find(StudyNotifyBean.class,
                    new Integer(snsk.getStudyNotifyId()));

            if (studyNotify == null) {
                return -2;
            }
            output = studyNotify.updateStudyNotify(snsk);

        } catch (Exception e) {
            Rlog.debug("studyNotify",
                    "Error in updateStudyNotify in StudyNotifyAgentBean" + e);
            return -2;
        }
        return output;
    }

 // notify subscribers
    public int notifySubscribers(int studyPK){
    	
        StudyNotifyDao sdStudyNotify = new StudyNotifyDao();
        
        int output;

        try {
        	output = 	sdStudyNotify.notifySubscribers(studyPK);
            return output;
            

        } catch (Exception e) {
            Rlog.debug("studyNotify",
                    "Error in notifySubscribers in StudyNotifyAgentBean" + e);
            return -1;
        }
        
        
    }
    
}// end of class

