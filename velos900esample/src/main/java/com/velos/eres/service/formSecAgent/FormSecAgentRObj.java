/*
 * Classname : FormSecAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 17/07/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: SoniaKaura
 */

package com.velos.eres.service.formSecAgent;

/* Import Statements */

import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.common.FormSecDao;
import com.velos.eres.business.formSec.impl.FormSecBean;

/* End of Import Statements */

/**
 * Remote interface for FormSecAgent session EJB
 * 
 * @author SoniaKaura
 * @version : 1.0 17/07/2003
 */
@Remote
public interface FormSecAgentRObj {
    /**
     * gets the FormSec record
     */
    FormSecBean getFormSecDetails(int formSecId);

    /**
     * creates a new FormSec Record
     */
    public int setFormSecDetails(FormSecBean formFldsk);

    /**
     * updates the FormSec Record
     */
    public int updateFormSec(FormSecBean formFldsk);
    
    //Overloaded for INF-18183 ::: Ankit
    public int updateFormSec(FormSecBean formFldsk, Hashtable<String, String> auditInfo);

    /**
     * remove FormSec Record
     */
    public int removeFormSec(int formSecId);

    /**
     * send the DAO to the Entity Bean through this Session bean
     * 
     */

    public int insertNewSections(FormSecDao formSecDao);

    /**
     * send the DAO to the Entity Bean through this Session bean
     * 
     */

    public FormSecDao getAllSectionsOfAForm(int formLibId);

    /**
     * send the DAO to the Entity Bean through this Session bean
     * 
     */

    public int updateNewSections(FormSecDao formSecDao);

    public int updateDefaultFormSec(int pkFormSec, int pkForm, String user,
            String ipAdd);

    public int updateBrowserFlag(int pkFormSec);

    public FormSecBean findByFormIdAndSeq(int formLibId, int seq);
    
    /**
     * returns true if field as any inter field action associated with the field
     * 
     * @param sectionId
     * 
     *            
     */

    public boolean sectionHasInterFieldActions(String sectionId) ;

}
