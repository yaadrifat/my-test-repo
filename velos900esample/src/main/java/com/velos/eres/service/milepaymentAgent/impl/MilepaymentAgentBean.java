/*
 * Classname			MilepaymentAgentBean
 * 
 * Version information 	1.0
 *
 * Date					05/28/2002
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.service.milepaymentAgent.impl;

import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.MilepaymentDao;
import com.velos.eres.business.milepayment.impl.MilepaymentBean;
import com.velos.eres.service.milepaymentAgent.MilepaymentAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

/**
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * @author Sonika
 * @version 1.0 05/28/2002
 */
@Stateless
public class MilepaymentAgentBean implements MilepaymentAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    
    @Resource
    private SessionContext context;

    /*
     * getMilepaymentDetails Calls getMilepaymentDetails() on entity bean -
     * MilepaymentBean Looks up on the Milepayment id parameter passed in and
     * constructs and returns a Milepayment state holder.
     * 
     * @param milepaymentId the Milepayment id
     * 
     * @see MilepaymentBean
     */
    public MilepaymentBean getMilepaymentDetails(int milepaymentId) {

        try {
            return (MilepaymentBean) em.find(MilepaymentBean.class,
                    new Integer(milepaymentId));
        } catch (Exception e) {
            Rlog.debug("milepayment",
                    "Error in getMilepaymentDetails() in MilepaymentAgentBean"
                            + e);
            return null;
        }

    }

    /**
     * Creates a new a Milepayment. Calls setMilepaymentDetails() on entity bean
     * MilepaymentBean
     * 
     * @param an
     *            Milepayment state holder containing Milepayment attributes to
     *            be set.
     * @return generated milepayment id
     * @see MilepaymentBean
     */

    public int setMilepaymentDetails(MilepaymentBean msk) {
        MilepaymentBean mPaymentBean = new MilepaymentBean();

        try {
            mPaymentBean.updateMilepayment(msk);
            em.persist(mPaymentBean);
            return mPaymentBean.getId();
            // mskSaved = milepaymentRObj.getMilepaymentStateKeeper();
            // Rlog.debug("milepayment","MilepaymentAgentBean.setMilepaymentDetails
            // " + mskSaved);
            // return mskSaved;
        } catch (Exception e) {
            System.out
                    .print("Error in setMilepaymentDetails() in MilepaymentAgentBean "
                            + e);
        }
        return -1;
    }

    /**
     * Updates Milepayment Details. Calls updateMilepayment() on entity bean
     * MilepaymentBean
     * 
     * @param an
     *            Milepayment state holder containing Milepayment attributes to
     *            be set.
     * @return int 0 for successful ; -2 for exception
     * @see MilepaymentBean
     */

    public int updateMilepayment(MilepaymentBean msk) {

        MilepaymentBean milepaymentRObj = null; // Milepayment Entity Bean
        // Remote Object
        int output;
        
        String oldRecordType = "";
        String newRecordType = "";
        MilepaymentDao md = new MilepaymentDao();
        try {

            milepaymentRObj = (MilepaymentBean) em.find(MilepaymentBean.class,
                    new Integer(msk.getId()));
            if (milepaymentRObj == null) {
                return -2;
            }
            
            oldRecordType = milepaymentRObj.getMilepaymentDelFlag();
            newRecordType = msk.getMilepaymentDelFlag();
            
            if ((!oldRecordType.equals(newRecordType)) && newRecordType.equals("Y"))
            {
            	// the payment is deleted, delete the payment details
            	md.deletePaymentDetails(msk.getId());
            	
            }
            
            output = milepaymentRObj.updateMilepayment(msk);
        } catch (Exception e) {
            Rlog.fatal("milepayment",
                    "Error in updateMilepayment in MilepaymentAgentBean" + e);
            return -2;
        }
        return output;
    }
    
    
    // Overloaded for INF-18183 ::: AGodara
    public int updateMilepayment(MilepaymentBean msk,Hashtable<String, String> auditInfo) {

        MilepaymentBean milepaymentRObj = null; // Milepayment Entity Bean
        int output;
        
        String oldRecordType = "";
        String newRecordType = "";
        MilepaymentDao md = new MilepaymentDao();
        try {

            milepaymentRObj = (MilepaymentBean) em.find(MilepaymentBean.class,
                    new Integer(msk.getId()));
            if (milepaymentRObj == null) {
                return -2;
            }
            
            oldRecordType = milepaymentRObj.getMilepaymentDelFlag();
            newRecordType = msk.getMilepaymentDelFlag();
            
            if ((!oldRecordType.equals(newRecordType)) && newRecordType.equals("Y"))
            {
            	AuditBean audit=null;
                String currdate =DateUtil.getCurrentDate();
            	String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
                String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
                String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
                String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
                String rid= AuditUtils.getRidValue("ER_MILEPAYMENT","eres","PK_MILEPAYMENT="+msk.getId()); //Fetches the RID
                
            	//POPULATE THE AUDIT BEAN 
            	audit = new AuditBean("ER_MILEPAYMENT",String.valueOf(msk.getId()),rid,userID,currdate,app_module,ipAdd,reason);
            	em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE	
            	
            	// the payment is deleted, delete the payment details
            	md.deletePaymentDetails(msk.getId());
            }
            output = milepaymentRObj.updateMilepayment(msk);
            if(output!=0){
            	context.setRollbackOnly();
            }
           
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("milepayment",
                    "Error in updateMilepayment in MilepaymentAgentBean" + e);
            return -2;
        }
        return output;
    }
    public MilepaymentDao getMilepaymentReceived(int studyId) {
        try {
            Rlog.debug("milepayment",
                    "In getMilepaymentReceived in MilepaymentAgentBean - 0");
            MilepaymentDao milepaymentDao = new MilepaymentDao();
            Rlog.debug("milepayment",
                    "In getMilepaymentReceived in MilepaymentAgentBean - 1");
            milepaymentDao.getMilepaymentReceived(studyId);
            Rlog.debug("milepayment",
                    "In getMilepaymentReceived in MilepaymentAgentBean - 2");
            return milepaymentDao;
        } catch (Exception e) {
            Rlog.fatal("milepayment",
                    "Exception In getMilepaymentReceived in MilepaymentAgentBean "
                            + e);
        }
        return null;

    }

}// end of class
