/*
 * Classname			BrowserRowsAgentRObj
 * 
 * Version information 	1.0
 *
 * Date					12/23/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.service.browserRowsAgent;

import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.service.util.BrowserRows;

// import com.velos.eres.business.common.MilepaymentDao;

/**
 * Remote interface for BrowserRowsAgent session EJB
 * 
 * @author Sonia Sahni
 */
@Remote
public interface BrowserRowsAgentRObj {

    public BrowserRows getPageRows(long page, long rowsRequired, String bSql,
            long totalPages, String countSql, String orderBy, String order);

    public BrowserRows getSchPageRows(long page, long rowsRequired,
            String bSql, long totalPages, String countSql, String orderBy,
            String order);

    public Hashtable getSchedulePageRows(long page, long rowsRequired,
            String bSql, long totalPages, String countSql, String orderBy,
            String order, String schWhere);

}
