/*
 * Classname			UserSessionLogAgentRObj
 * 
 * Version information 	1.0
 *
 * Date					03/10/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.service.userSessionLogAgent;

import javax.ejb.Remote;

import com.velos.eres.business.userSessionLog.impl.UserSessionLogBean;

/**
 * Remote interface for UserSessionLogAgent session EJB
 * 
 * @author Sonia Sahni
 */
@Remote
public interface UserSessionLogAgentRObj {
    /**
     * gets the userSessionLog details
     */
    public UserSessionLogBean getUserSessionLogDetails(int userSessionLogId);

    /**
     * sets the UserSession details
     */
    public int setUserSessionLogDetails(UserSessionLogBean usk);

    public int updateUserSessionLog(UserSessionLogBean usk);

}
