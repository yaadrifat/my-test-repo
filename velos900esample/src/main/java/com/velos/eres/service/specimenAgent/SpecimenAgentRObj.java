/*
 * Classname			SpecimenAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					07/09/2007
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.eres.service.specimenAgent;

import java.util.Hashtable;
import javax.ejb.Remote;
import java.util.HashSet;

import com.velos.eres.business.specimen.impl.SpecimenBean;
import com.velos.eres.business.common.SpecimenDao;

/**
 * Remote interface for SpecimenAgentBean session EJB
 * 
 * @author Jnanamay Majumdar
 * @version 1.0, 07/09/2007
 */
@Remote
public interface SpecimenAgentRObj {
    public SpecimenBean getSpecimenDetails(int pkSpec);    
    
    public int setSpecimenDetails(SpecimenBean specimen);    
    
    public int updateSpecimen(SpecimenBean specimen);
    
    public int deleteSpecimens(String[] deleteIds);
    
    // Overloaded for INF-18183 ::: AGodara
    public int deleteSpecimens(String[] deleteIds,Hashtable<String, String> auditInfo);
    
    // Overloaded for INF 21676 ::: Panjvir
    public HashSet<String> getChildSpecimenId(int Parent_SpecId);
    
    public String getSpecimenIdAuto() ;
    
    public String getSpecimenIdAuto(String studyId, String patientId, int accId) ;
    
    public SpecimenDao getSpecimenChilds(int specId, int accId);   
    
    public String getParentSpecimenId(int specId) ;
    
}
