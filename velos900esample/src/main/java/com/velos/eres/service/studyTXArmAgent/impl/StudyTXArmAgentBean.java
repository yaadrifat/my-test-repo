/**
 * Classname : StudyTXArmAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 05/02/2005
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Anu Khanna
 */

package com.velos.eres.service.studyTXArmAgent.impl;

/* Import Statements */
import java.util.Hashtable;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ejb.SessionContext;
import javax.annotation.Resource;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.common.StudyTXArmDao;
import com.velos.eres.business.studyTXArm.impl.StudyTXArmBean;
import com.velos.eres.service.studyTXArmAgent.StudyTXArmAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.business.audit.impl.AuditBean;

/* End of Import Statements */

/**
 * The stateless session EJB acting as a facade for the entity CMP -
 * StudyTXArmAgentBean.<br>
 * <br>
 * 
 * @author Anu Khanna
 * @see StudyTXArmAgentBean
 * @ejbHome studyTXArmAgentHome
 * @ejbRemote studyTXArmAgentRObj
 */

@Stateless
public class StudyTXArmAgentBean implements StudyTXArmAgentRObj {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    @Resource private SessionContext context;

    /**
     * 
     * Looks up on the id parameter passed in and constructs and returns a
     * StudyTXArm state keeper. containing all the details of the StudyTXArm
     * <br>
     * 
     * @param StudyTXArm
     *            Id
     */

    public StudyTXArmBean getStudyTXArmDetails(int studyTXArmId) {
        StudyTXArmBean retrieved = null;

        try {
            retrieved = (StudyTXArmBean) em.find(StudyTXArmBean.class,
                    new Integer(studyTXArmId));

        } catch (Exception e) {
            Rlog.fatal("studyTXArm",
                    "Exception in StudyTXArmStateKeeper() in StudyTXArmAgentBean"
                            + e);
        }

        return retrieved;

    }

    /**
     * Creates StudyTXArm record.
     * 
     * @param a
     *            State Keeper containing the studyTXArmId attributes to be set.
     * @return int for successful:0; e\Exception : -2
     */

    public int setStudyTXArmDetails(StudyTXArmBean stk) {
        try {
            em.merge(stk);
            return stk.getStudyTXArmId();
        } catch (Exception e) {
            Rlog.fatal("studyTXArm",
                    "Error in setStudyTXArmDetails() in StudyTXArmAgentBean "
                            + e);
            return -2;
        }

    }

    /**
     * Updates a StudyTXArm record.
     * 
     * @param a
     *            StudyTXArm state keeper containing StudyTXArm attributes to be
     *            set.
     * @return int for successful:0 ; e\Exception : -2
     */

    public int updateStudyTXArm(StudyTXArmBean stk) {

        StudyTXArmBean retrieved = null; // Entity Bean Remote Object
        int output;

        try {
            retrieved = (StudyTXArmBean) em.find(StudyTXArmBean.class,
                    new Integer(stk.getStudyTXArmId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateStudyTXArm(stk);

        } catch (Exception e) {
            Rlog.fatal("StudyTXArm",
                    "Error in updateStudyTXArm in StudyTXArmAgentBean" + e);
            return -2;
        }
        return output;
    }

    /**
     * Removes a StudyTXArm record.
     * 
     * @param a
     *            StudyTXArm Primary Key
     * @return int for successful:0; e\Exception : -2
     */

    // Overloaded for INF-18183 ::: Akshi
    public int removeStudyTXArm(int studyTXArmId,Hashtable<String, String> args) {
        int output;
        StudyTXArmBean retrieved = null;

        try {
        	
        	AuditBean audit=null;
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appmodule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the application module name from the Hashtable*/
            String getRidValue= AuditUtils.getRidValue("ER_STUDYTXARM","eres","PK_STUDYTXARM="+studyTXArmId);/*Fetches the RID/PK_VALUE*/ 
            audit = new AuditBean("ER_STUDYTXARM",String.valueOf(studyTXArmId),getRidValue,
        			userID,currdate,appmodule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
        	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/

            retrieved = (StudyTXArmBean) em.find(StudyTXArmBean.class,
                    new Integer(studyTXArmId));

            em.remove(retrieved);
            return 0;
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.fatal("studyTXArm", "Exception in removeStudyTXArm" + e);
            return -1;

        }

    }
    public int removeStudyTXArm(int studyTXArmId) {
        int output;
        StudyTXArmBean retrieved = null;

        try {

            retrieved = (StudyTXArmBean) em.find(StudyTXArmBean.class,
                    new Integer(studyTXArmId));

            em.remove(retrieved);
            return 0;
        } catch (Exception e) {
            Rlog.fatal("studyTXArm", "Exception in removeStudyTXArm" + e);
            return -1;

        }

    }

    public StudyTXArmDao getStudyTrtmtArms(int studyId) {

        try {

            StudyTXArmDao studyTXArmDao = new StudyTXArmDao();
            studyTXArmDao.getStudyTrtmtArms(studyId);
            return studyTXArmDao;
        } catch (Exception e) {
            Rlog.fatal("studyTXArm",
                    "Exception In getStudyTrtmtArms in StudyTXArmAgentBean "
                            + e);
        }
        return null;
    }

    public int getCntTXForPat(int studyId, int studyTXArmId) {
        int ret = 0;
        try {

            StudyTXArmDao studyTXArmDao = new StudyTXArmDao();
            ret = studyTXArmDao.getCntTXForPat(studyId, studyTXArmId);

        } catch (Exception e) {
            Rlog.fatal("studyTXArm",
                    "Exception In getCntTXForPat in StudyTXArmAgentBean " + e);
        }
        return ret;
    }

}
