package com.velos.eres.service.submissionAgent;

import java.util.ArrayList;
import java.util.Calendar;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.EIRBDao;
import com.velos.eres.business.submission.impl.SubmissionBean;
import com.velos.eres.service.util.Rlog;

@Stateless
@Remote( { SubmissionAgent.class } )
public class SubmissionAgentBean implements SubmissionAgent {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

    public SubmissionBean getSubmissionDetails(int id) {
        if (id == 0) {
            return null;
        }
        SubmissionBean submissionBean = null;
        try {
            submissionBean = (SubmissionBean) em.find(SubmissionBean.class, new Integer(id));
        } catch (Exception e) {
            Rlog.fatal("submission", "Exception in SubmissionAgentBean.getSubmssionDetails "+e);
            return submissionBean;
        }

        return submissionBean;
    }

    public Integer updateSubmissionByFkStudy(SubmissionBean submissionBean) {
        Integer output = 0;
        Query query = em.createNamedQuery("findCurrentByFkStudy");
        query.setParameter("fkStudy", submissionBean.getFkStudy());
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return 0; }
        SubmissionBean bean = (SubmissionBean)list.get(0);
        try {
            if (submissionBean.getSubmissionFlag() != null) {
                bean.setSubmissionFlag(submissionBean.getSubmissionFlag());
            }
            if (submissionBean.getSubmissionStatus() != null &&
                    submissionBean.getSubmissionStatus() > 0) {
                bean.setSubmissionStatus(submissionBean.getSubmissionStatus());
            }
            if (submissionBean.getSubmissionType() != null &&
                    submissionBean.getSubmissionType() > 0) {
                bean.setSubmissionType(submissionBean.getSubmissionType());
            }
            bean.setLastModifiedBy(submissionBean.getLastModifiedBy());
            bean.setLastModifiedDate(Calendar.getInstance().getTime());
            em.merge(bean);
            output = bean.getId();
        } catch (Exception e) {
            Rlog.fatal("submission", "Exception in SubmissionAgentBean.updateSubmissionByFkStudy "+e);
            output = -1;
        }
        return output;
    }
    
    public Integer getExistingSubmissionByFkStudy(Integer fkStudy) {
        Integer output = 0;
        Query query = em.createNamedQuery("findCurrentByFkStudy");
        query.setParameter("fkStudy", fkStudy);
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return 0; }
        SubmissionBean bean = (SubmissionBean)list.get(0);
        output = bean.getId();
        return output;
    }
    
    public Integer getExistingSubmissionByFkStudyAndFlag(Integer fkStudy, Integer submissionFlag) {
        Integer output = 0;
        Query query = em.createNamedQuery("findCurrentByFkStudyAndFlag");
        query.setParameter("fkStudy", fkStudy);
        query.setParameter("submissionFlag", submissionFlag);
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return 0; }
        SubmissionBean bean = (SubmissionBean)list.get(0);
        output = bean.getId();
        return output;
    }
    
    public Integer createSubmission(SubmissionBean submissionBean) {
        Integer output = 0;
        try {
            SubmissionBean newBean = new SubmissionBean();
            newBean.setDetails(submissionBean);
            newBean.setSubmissionStatus(getSubmissionStatus("submitted"));
            em.persist(newBean);
            output = newBean.getId();
        } catch(Exception e) {
            Rlog.fatal("submission", "Exception in SubmissionAgentBean.createSubmission "+e);
            output = -1;
        }
        return output;
    }
    
    public boolean getSubmissionAccess(String usrId, String submissionPK) {
        EIRBDao eIrbDao = new EIRBDao();
        return eIrbDao.getSubmissionAccess(usrId, submissionPK);
    }

    private Integer getSubmissionStatus(String subtype) {
        if (subtype == null) { return new Integer(0); }
        CodeDao codeDao = new CodeDao();
        codeDao.getCodeValues("subm_status");
        ArrayList codeSubtypes = codeDao.getCSubType();
        ArrayList codePks = codeDao.getCId();
        for (int iX=0; iX<codeSubtypes.size(); iX++) {
            if (subtype.equals(codeSubtypes.get(iX))) {
                return (Integer)codePks.get(iX);
            }
        }
        return new Integer(0);
    }
    
}
