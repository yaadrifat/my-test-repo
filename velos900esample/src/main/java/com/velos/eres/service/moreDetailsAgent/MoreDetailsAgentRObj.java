package com.velos.eres.service.moreDetailsAgent;


/* Import Statements */

import javax.ejb.Remote;

import com.velos.eres.business.common.MoreDetailsDao;
import com.velos.eres.business.moreDetails.impl.*;

/* End of Import Statements */

/**
 * Remote interface for StudyIdAgent session EJB
 * 
 * @author Sonia Sahni
 * @version : 1.0 09/23/2003
 */
@Remote
public interface MoreDetailsAgentRObj {
    
    MoreDetailsBean getMoreDetailsBean(int id);

    
    public int setMoreDetails(MoreDetailsBean mdb);

    
    public int updateMoreDetails(MoreDetailsBean mdb);

    
    public int removeMoreDetails(int id);

    public int createMultipleMoreDetails(MoreDetailsBean mdb, String defUserGroup );


    public int updateMultipleMoreDetails(MoreDetailsBean mdb);

    public MoreDetailsDao getMoreDetails(int modId,String modName, String defUserGroup );

}
