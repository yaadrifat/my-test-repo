/*
 * Classname			ULinkAgentBean.class
 * 
 * Version information
 *
 * Date					03/15/2001
 * 
 * Copyright notice
 */
package com.velos.eres.service.ulinkAgent.impl;
import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.UsrLinkDao;
import com.velos.eres.business.ulink.impl.ULinkBean;
import com.velos.eres.service.ulinkAgent.ULinkAgentRObj;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
/**
 * 
 * 
 * The stateless session EJB acting as a facade for the entity CMP.<br>
 * <br>
 * 
 * 
 * @author dinesh
 * 
 * 
 */
@Stateless
public class ULinkAgentBean implements ULinkAgentRObj
{
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;
    
    @Resource
    private SessionContext context;
    /**
     * 
     * 
     * Looks up on the ULink id parameter passed in and constructs and returns a
     * User state keeper.
     * 
     * 
     * containing all the summary details of the u<br>
     * 
     * 
     * @param Id
     *            the UlinkId
     * 
     * 
     */
    public ULinkBean getULinkDetails(int lnkId) {
        ULinkBean ulB = null;
        try {
            ulB = (ULinkBean) em.find(ULinkBean.class, new Integer(lnkId));
        }
        catch (Exception e) {
            Rlog.debug("ulink", "Error in getULinkDetails() in ULinkAgentBean"
                    + e);
        }
        return ulB;
    }
    /**
     * 
     * 
     * Sets a User.
     * 
     * 
     * @param an
     *            user state holder containing account attributes to be set.
     * 
     * 
     * @return void
     * 
     * 
     */
    public int setULinkDetails(ULinkBean ulink)
    {
        try {
            Rlog.debug("ulink", ulink.getLnkAccId());
            Rlog.debug("ulink", ulink.getLnkUserId());
            em.merge(ulink);
            Rlog.debug("ulink",
                    "ULinkAgentBean.setULinkDetails ULinkStateKeeper ");
            return (ulink.getLnkId());
        }
        catch (Exception e) {
            Rlog.debug("ulink", "Error in setULinkDetails() in ULinkAgentBean "
                    + e);
        }
        return 0;
    }
    public int updateULink(ULinkBean ulink) {
        ULinkBean ulb = null;
        int output;
        try {
            ulb = (ULinkBean) em.find(ULinkBean.class, new Integer(ulink
                    .getLnkId()));
            if (ulb == null) {
                return -2;
            }
            output = ulb.updateULink(ulink);
        }
        catch (Exception e) {
            Rlog.debug("ulink", "Error in updateULink in ULinkAgentBean" + e);
            return -2;
        }
        return output;
    }
    public int deleteULink(ULinkBean ulink) {
        ULinkBean ulb = null; // User Entity Bean Remote Object
        int output;
        try {
            ulb = (ULinkBean) em.find(ULinkBean.class, new Integer(ulink
                    .getLnkId()));
            if (ulb == null) {
                return -2;
            }
            em.remove(ulb);
            return 0;
        }
        catch (Exception e) {
            Rlog.debug("ulink", "Error in delete ulinkin ULinkAgentBean" + e);
            return -2;
        }
    }
    // Overloaded for INF-18183 -- AGodara 
    public int deleteULink(ULinkBean ulink,Hashtable<String, String> auditInfo){
        
    	ULinkBean ulb = null; // User Entity Bean Remote Object
    	AuditBean audit=null;
    	try {
        	
    		String currdate =DateUtil.getCurrentDate(); //
            String userID=(String)auditInfo.get(AuditUtils.USER_ID_KEY); //Fetches the User ID from the Hashtable
            String ipAdd=(String)auditInfo.get(AuditUtils.IP_ADD_KEY); //Fetches the IP Address from the Hashtable
            String reason=(String)auditInfo.get(AuditUtils.REASON_FOR_DEL); //Fetches the reason for deletion from the Hashtable
            String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
            String rid= AuditUtils.getRidValue("ER_USR_LNKS","eres","PK_USR_LNKS="+ulink.getLnkId()); //Fetches the RID
                      
            //POPULATE THE AUDIT BEAN 
            audit = new AuditBean("ER_USR_LNKS",String.valueOf(ulink.getLnkId()),rid,userID,currdate,app_module,ipAdd,reason);
        	
            ulb = (ULinkBean) em.find(ULinkBean.class, new Integer(ulink
                    .getLnkId()));
            if (ulb == null) {
				return -2;
			} 
            
            em.persist(audit); //PERSIST THE DATA IN AUDIT TABLE
            em.remove(ulb);
            return 0;
        }catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.debug("ulink", "Error in delete ULinkAgentBean - deleteULink(ULinkBean ulink,Hashtable<String, String> auditInfo)" + e);
            return -2;
        }
    }
    
    
    public UsrLinkDao getULinkValuesByAccountId(int accountId, String accType) {
        try
        {
            Rlog
                    .debug("ulink",
                            "In getULinkValuesByAccountId in ULinkAgentBean line number 0");
            UsrLinkDao usrLinkDao = new UsrLinkDao();
            Rlog
                    .debug("ulink",
                            "In getULinkValuesByAccountId in ULinkAgentBean line number 1");
            usrLinkDao.getULinkValuesByAccountId(accountId, accType);
            Rlog
                    .debug("ulink",
                            "In getULinkValuesByAccountId in ULinkAgentBean line number 2");
            return usrLinkDao;
        } catch (Exception e)
        {
            Rlog.fatal("ulink",
                    "Exception In getULinkValuesByAccountId in ULinkAgentBean "
                            + e);
        }
        return null;
    }
    public UsrLinkDao getULinkValuesByUserId(int userId) {
        try
        {
            Rlog
                    .debug("ulink",
                            "In getULinkValuesByUserId in ULinkAgentBean line number 0");
            UsrLinkDao usrLinkDao = new UsrLinkDao();
            Rlog
                    .debug("ulink",
                            "In getULinkValuesByUserId in ULinkAgentBean line number 1");
            usrLinkDao.getULinkValuesByUserId(userId);
            Rlog
                    .debug("ulink",
                            "In getULinkValuesByUserId in ULinkAgentBean line number 2");
            return usrLinkDao;
        } catch (Exception e)
        {
            Rlog.fatal("ulink",
                    "Exception In getULinkValuesByUserId in ULinkAgentBean "
                            + e);
        }
        return null;
    }
}// end of class
