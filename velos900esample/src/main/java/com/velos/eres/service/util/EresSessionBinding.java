/*
 * Classname : eresSessionBinding
 *
 * Version information: 1.0
 *
 * Date 03/15/2003
 *
 */

package com.velos.eres.service.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import com.velos.eres.web.user.UserJB;
import com.velos.eres.web.userSession.UserSessionJB;
import com.velos.eres.service.util.DateUtil;

/**
 * EresSessionBinding class for session tracking
 *
 * @author Sonika Talwar
 * @verison 1.0 03/15/2003
 */
public class EresSessionBinding implements HttpSessionBindingListener {
    int currUserId = 0;

    int pkUserSession = 0;

    String usrLogName = "";

    public void valueBound(HttpSessionBindingEvent event) {
        try {
            HttpSession currSession = event.getSession();
            String sessUserId = (String) currSession.getAttribute("userId");
            String ipAdd = (String) currSession.getAttribute("ipAdd");

            Rlog.debug("user", "SESSION CREATED for user Id : " + sessUserId);

            int userId = EJBUtil.stringToNum(sessUserId);

            currUserId = userId;
            UserJB userB = (UserJB) currSession.getAttribute("currentUser");

            Date date = new Date(currSession.getCreationTime());

            String loginTime = DateUtil.dateToString(date, null);




            // set the loginflag in er_user table to 1 indicating user logged in
            userB.setUserId(userId);
            userB.getUserDetails();
            usrLogName = userB.getUserLoginName();
            Rlog.debug("user",
                    "Login name retrieved in eressessionbinding:valueBound "
                            + usrLogName);
            usrLogName = (usrLogName == null) ? "" : usrLogName;
            if (usrLogName.length() > 0) {
                userB.setUserLoginFlag("1");
                userB.updateUser();
            }

            // insert into er_usersession table
            UserSessionJB userSessionB = new UserSessionJB();
            userSessionB.setUserId(sessUserId);
            userSessionB.setLoginTime(loginTime);
            userSessionB.setIpAdd(ipAdd);
            userSessionB.setSuccessFlag("1");

            userSessionB.setUserSessionDetails();
            pkUserSession = userSessionB.getId();

            currSession.setAttribute("pkUserSession", String
                    .valueOf(pkUserSession));

        } catch (Exception e) {
            Rlog.fatal("user", "ERROR in valueBound of EresSessionBinding : "
                    + e);
        }
    }

    public void valueUnbound(HttpSessionBindingEvent event) {
        HttpSession currSession = event.getSession();
        Date date = null;
        // Bug#9649,8659 Date: 24-05-2012  Ankit
        try{date = new Date(currSession.getLastAccessedTime());}
        catch(Exception e){date = new Date();}
        try {
            Rlog.debug("user",
                    "SESSION DESTROYED user Id from instance variable: "
                            + currUserId);

            int userId = currUserId;
            UserJB userB = new UserJB();
            // set the loginflag in er_user table to 0 indicating user logged
            // out
            userB.setUserId(userId);
            userB.getUserDetails();
            usrLogName = userB.getUserLoginName();
            Rlog.debug("user",
                    "Login name retrieved in eressessionbinding:valueUnbound "
                            + usrLogName);
            usrLogName = (usrLogName == null) ? "" : usrLogName;
            if (usrLogName.length() > 0) {
                userB.setUserLoginFlag("0");
                userB.updateUser();
            }

            

 
            String logoutTime = DateUtil.dateToString(date, null);

            // update logout time in er_usersession table
            UserSessionJB userSessionB = new UserSessionJB();
            userSessionB.setId(pkUserSession);
            userSessionB.getUserSessionDetails();
            userSessionB.setLogoutTime(logoutTime);
            userSessionB.updateUserSession();
        } catch (Exception e) {
            Rlog.fatal("user", "ERROR in valueUnbound of EresSessionBinding"
                    + e);
        }

    }
}
