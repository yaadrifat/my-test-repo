/*
 * Classname			ExtURightsRObj.class
 * 
 * Version information   
 *
 * Date					03/16/2001
 * 
 * Copyright notice
 */

package com.velos.eres.service.extURightsAgent;

import javax.ejb.Remote;

import com.velos.eres.business.extURights.impl.ExtURightsBean;

/**
 * Remote interface for ExtURightsAgentRObj session EJB
 * 
 * @author Deepali
 */
@Remote
public interface ExtURightsAgentRObj {
    /**
     * gets the Rights details of External Users of a study
     */
    public ExtURightsBean getExtURightsDetails(int id);

    /**
     * sets the Rights details of External User of a Study
     */
    public int setExtURightsDetails(ExtURightsBean extu);

    /**
     * updates the Rights details of External User of a Study
     */
    public int updateExtURights(ExtURightsBean esk);
}
