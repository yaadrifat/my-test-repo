package com.velos.eres.ctrp.business;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ER_CTRP_DRAFT")
@NamedQueries( {
	@NamedQuery(name = "findCtrpDraftsByStudyId", 
			query = " SELECT OBJECT(draft) FROM CtrpDraftBean draft "+
			" where fk_account = :fk_account and fkStudy = :fkStudy "),
    @NamedQuery(name = "findCtrpDraftByDraftId",
            query = "SELECT OBJECT(draft) FROM CtrpDraftBean draft " +
            " where draft.id = :draftId "),
    @NamedQuery(name = "findCtrpDraftsByStudyIds", 
    		query = " SELECT OBJECT(draft) FROM CtrpDraftBean draft "+
    		" where fk_account = :fk_account and fkStudy in (:fkStudys) ")
})
public class CtrpDraftBean implements Serializable {
	
	private static final long serialVersionUID = -1068479576125516856L;
	private Integer id;
    private Integer fkStudy;
	private Integer ctrpDraftType;
    private Integer fkCodelstSubmissionType;
    private String amendNumber;
    private Date amendDate;
    private Integer draftXMLReqd;
    private Integer fkTrialOwner;
    private String nciStudyId;
    private String leadOrgStudyId;
    private String submitOrgStudyId;
    private String nctNumber;
    private String otherNumber;
    private Integer fkCodelstPhase;
    private Integer draftPilot;
    private Integer studyType;
    private Integer fkCodelstInterventionType;
    private String interventionName;
    private String fkCodelstDiseaseSite;
    private Integer studyPurpose;
    private String studyPurposeOther;
    private String leadOrgCtrpId;
    private Integer fkSiteLeadOrg;
    private Integer fkAddLeadOrg;
    private Integer leadOrgType;
    private String submitOrgCtrpId;
    private Integer fkSiteSubmitOrg;
    private Integer fkAddSubmitOrg;
    private Integer submitOrgType;
    private Integer submitNCIDesignated;
    private String piCtrpId;
    private Integer fkUserPi;
    private String piFirstName;
    private String piLastName;
    private Integer fkAddPi;
    private String sponsorCtrpId;
    private Integer fkSiteSponsor;
    private Integer fkAddSponsor;
    private Integer responsibleParty;
    private Integer rpSponsorContactType;
    private String rpSponsorContactId;
    private String sponsorPersonalFirstName;
    private Integer fkUserSponsorPersonal;
    private String sponsorPersonalLastName;
    private Integer fkAddSponsorPersonal;
    private String sponsorGenericTitle;
    private String rpEmail;
    private String rpPhone;
    private Integer rpExtn;
    private Integer summ4SponsorType;
    private String summ4SponsorId;
    private Integer fkSiteSumm4Sponsor;
    private Integer fkAddSumm4Sponsor;
    private String sponsorProgramCode;
    private Integer fkCodelstCtrpStudyStatus;
    private Date ctrpStudyStatusDate;
    private String ctrpStudyStopReason;
    private Date ctrpStudyStartDate;
    private Integer isStartActual;
    private Date ctrpStudyCompDate;
    private Integer isCompActual;
    private Date ctrpStudyAccOpenDate;
    private Date ctrpStudyAccClosedDate;
    private Integer siteTargetAccrual;
    private Integer oversightCountry;
    private Integer oversightOrganization;
    private Integer fdaIntvenIndicator;
    private Integer section801Indicator;
    private Integer delayPostIndicator;
    private Integer dmAppointIndicator;
    private Integer deletedFlag;
    private String ipAdd;
    private Integer creator;
    private Integer lastModifiedBy;


    public CtrpDraftBean(){}
    
    public void setId(Integer id) {
        this.id = id;
    }

	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_CTRP_DRAFT", allocationSize=1)
    @Column(name = "PK_CTRP_DRAFT")
    public Integer getId() {
        return id;
    }

	public void setFkStudy(Integer fkStudy) {
		this.fkStudy = fkStudy;
	}

	@Column(name = "FK_STUDY")
	public Integer getFkStudy() {
		return fkStudy;
	}

	public void setCtrpDraftType(Integer ctrpDraftType) {
		this.ctrpDraftType = ctrpDraftType;
	}

	@Column(name = "CTRP_DRAFT_TYPE")
	public Integer getCtrpDraftType() {
		return ctrpDraftType;
	}

	public void setFkCodelstSubmissionType(Integer fkCodelstSubmissionType) {
		this.fkCodelstSubmissionType = fkCodelstSubmissionType;
	}

	@Column(name = "FK_CODELST_SUBMISSION_TYPE")
	public Integer getFkCodelstSubmissionType() {
		return fkCodelstSubmissionType;
	}
    
	public void setAmendNumber(String amendNumber){
		this.amendNumber=amendNumber;
	}
	@Column(name = "AMEND_NUMBER")
	public String getAmendNumber(){
		return this.amendNumber;
	}
	public void setAmendDate(Date amendDate){
		this.amendDate=amendDate;
	}
	@Column(name = "AMEND_DATE")
	public Date getAmendDate(){
		return this.amendDate;
	}
	public void setDraftXMLReqd(Integer draftXMLReqd){
		this.draftXMLReqd=draftXMLReqd;
	}
	@Column(name = "DRAFT_XML_REQD")
	public Integer getDraftXMLReqd(){
		return this.draftXMLReqd;
	}
	public void setFkTrialOwner(Integer fkTrialOwner){
		this.fkTrialOwner=fkTrialOwner;
	}
	@Column(name = "FK_TRIAL_OWNER")
	public Integer getFkTrialOwner(){
		return this.fkTrialOwner;
	}
	public void setLeadOrgStudyId(String leadOrgStudyId){
		this.leadOrgStudyId=leadOrgStudyId;
	}
	@Column(name = "LEAD_ORG_STUDYID")
	public String getLeadOrgStudyId(){
		return this.leadOrgStudyId;
	}
	public void setNciStudyId(String nciStudyId){
		this.nciStudyId=nciStudyId;
	}	
	@Column(name = "NCI_STUDYID") 
	public String getNciStudyId() {
		return this.nciStudyId;
	}	
	public void setSubmitOrgStudyId(String submitOrgStudyId){
		this.submitOrgStudyId=submitOrgStudyId;
	}
	@Column(name = "SUBMIT_ORG_STUDYID")
	public String getSubmitOrgStudyId(){
		return this.submitOrgStudyId;
	}
	public void setNctNumber(String nctNumber){
		this.nctNumber=nctNumber;
	}
	@Column(name = "NCT_NUMBER")
	public String getNctNumber(){
		return this.nctNumber;
	}
	public void setOtherNumber(String otherNumber){
		this.otherNumber=otherNumber;
	}
	@Column(name = "OTHER_NUMBER")
	public String getOtherNumber(){
		return this.otherNumber;
	}
	public void setFkCodelstPhase(Integer fkCodelstPhase){
		this.fkCodelstPhase=fkCodelstPhase;
	}
	@Column(name = "FK_CODELST_PHASE")
	public Integer getFkCodelstPhase(){
		return this.fkCodelstPhase;
	}
	public void setDraftPilot(Integer draftPilot){
		this.draftPilot=draftPilot;
	}
	@Column(name = "DRAFT_PILOT")
	public Integer getDraftPilot(){
		return this.draftPilot;
	}
	public void setStudyType(Integer studyType){
		this.studyType=studyType;
	}
	@Column(name = "STUDY_TYPE")
	public Integer getStudyType(){
		return this.studyType;
	}
	public void setFkCodelstInterventionType(Integer fkCodelstInterventionType){
		this.fkCodelstInterventionType=fkCodelstInterventionType;
	}
	@Column(name = "FK_CODELST_INTERVENTION_TYPE")
	public Integer getFkCodelstInterventionType(){
		return this.fkCodelstInterventionType;
	}
	public void setInterventionName(String interventionName){
		this.interventionName=interventionName;
	}
	@Column(name = "INTERVENTION_NAME")
	public String getInterventionName(){
		return this.interventionName;
	}
	public void setFkCodelstDiseaseSite(String fkCodelstDiseaseSite){
		this.fkCodelstDiseaseSite=fkCodelstDiseaseSite;
	}
	@Column(name = "FK_CODELST_DISEASE_SITE")
	public String getFkCodelstDiseaseSite(){
		return this.fkCodelstDiseaseSite;
	}
	public void setStudyPurpose(Integer studyPurpose){
		this.studyPurpose=studyPurpose;
	}
	@Column(name = "STUDY_PURPOSE")
	public Integer getStudyPurpose(){
		return this.studyPurpose;
	}
	public void setStudyPurposeOther(String studyPurposeOther){
		this.studyPurposeOther=studyPurposeOther;
	}
	@Column(name = "STUDY_PURPOSE_OTHER")
	public String getStudyPurposeOther(){
		return this.studyPurposeOther;
	}
	public void setLeadOrgCtrpId(String leadOrgCtrpId){
		this.leadOrgCtrpId=leadOrgCtrpId;
	}
	@Column(name = "LEAD_ORG_CTRPID")
	public String getLeadOrgCtrpId(){
		return this.leadOrgCtrpId;
	}
	public void setFkSiteLeadOrg(Integer fkSiteLeadOrg){
		this.fkSiteLeadOrg=fkSiteLeadOrg;
	}
	@Column(name = "LEAD_ORG_FK_SITE")
	public Integer getFkSiteLeadOrg(){
		return this.fkSiteLeadOrg;
	}
	public void setFkAddLeadOrg(Integer fkAddLeadOrg){
		this.fkAddLeadOrg=fkAddLeadOrg;
	}
	@Column(name = "LEAD_ORG_FK_ADD")
	public Integer getFkAddLeadOrg(){
		return this.fkAddLeadOrg;
	}
	public void setLeadOrgType(Integer leadOrgType){
		this.leadOrgType=leadOrgType;
	}
	@Column(name = "LEAD_ORG_TYPE")
	public Integer getLeadOrgType(){
		return this.leadOrgType;
	}
	public void setSubmitOrgCtrpId(String submitOrgCtrpId){
		this.submitOrgCtrpId=submitOrgCtrpId;
	}
	@Column(name = "SUBMIT_ORG_CTRPID")
	public String getSubmitOrgCtrpId(){
		return this.submitOrgCtrpId;
	}
	public void setFkSiteSubmitOrg(Integer fkSiteSubmitOrg){
		this.fkSiteSubmitOrg=fkSiteSubmitOrg;
	}
	@Column(name = "SUBMIT_ORG_FK_SITE")
	public Integer getFkSiteSubmitOrg(){
		return this.fkSiteSubmitOrg;
	}
	public void setFkAddSubmitOrg(Integer fkAddSubmitOrg){
		this.fkAddSubmitOrg=fkAddSubmitOrg;
	}
	@Column(name = "SUBMIT_ORG_FK_ADD")
	public Integer getFkAddSubmitOrg(){
		return this.fkAddSubmitOrg;
	}
	public void setSubmitOrgType(Integer submitOrgType){
		this.submitOrgType=submitOrgType;
	}
	@Column(name = "SUBMIT_ORG_TYPE")
	public Integer getSubmitOrgType(){
		return this.submitOrgType;
	}
	public void setSubmitNCIDesignated(Integer submitNCIDesignated) {
		this.submitNCIDesignated=submitNCIDesignated;
	}
	@Column(name = "SUBMIT_NCI_DESIGNATED") 
	public Integer getSubmitNCIDesignated(){
		return this.submitNCIDesignated;
	}
	public void setPiCtrpId(String piCtrpId){
		this.piCtrpId=piCtrpId;
	}
	@Column(name = "PI_CTRPID")
	public String getPiCtrpId(){
		return this.piCtrpId;
	}
	public void setFkUserPI(Integer fkUserPi){
		this.fkUserPi=fkUserPi;
	}
	@Column(name = "PI_FK_USER")
	public Integer getFkUserPI(){
		return this.fkUserPi;
	}
	public void setPiFirstName(String piFirstName){
		this.piFirstName=piFirstName;
	}
	@Column(name = "PI_FIRST_NAME")
	public String getPiFirstName(){
		return this.piFirstName;
	}
	public void setPiLastName(String piLastName){
		this.piLastName=piLastName;
	}
	@Column(name = "PI_LAST_NAME")
	public String getPiLastName(){
		return this.piLastName;
	}
	public void setFkAddPi(Integer fkAddPi){
		this.fkAddPi=fkAddPi;
	}
	@Column(name = "PI_FK_ADD")
	public Integer getFkAddPi(){
		return this.fkAddPi;
	}
	public void setSponsorCtrpId(String sponsorCtrpId){
		this.sponsorCtrpId=sponsorCtrpId;
	}
	@Column(name = "SPONSOR_CTRPID")
	public String getSponsorCtrpId(){
		return this.sponsorCtrpId;
	}
	public void setFkSiteSponsor(Integer fkSiteSponsor){
		this.fkSiteSponsor=fkSiteSponsor;
	}
	@Column(name = "SPONSOR_FK_SITE")
	public Integer getFkSiteSponsor(){
		return this.fkSiteSponsor;
	}
	public void setFkAddSponsor(Integer fkAddSponsor){
		this.fkAddSponsor=fkAddSponsor;
	}
	@Column(name = "SPONSOR_FK_ADD")
	public Integer getFkAddSponsor(){
		return this.fkAddSponsor;
	}
	public void setResponsibleParty(Integer responsibleParty){
		this.responsibleParty=responsibleParty;
	}
	@Column(name = "RESPONSIBLE_PARTY")
	public Integer getResponsibleParty(){
		return this.responsibleParty;
	}
	public void setRpSponsorContactType(Integer rpSponsorContactType){
		this.rpSponsorContactType=rpSponsorContactType;
	}
	@Column(name = "RP_SPONSOR_CONTACT_TYPE")
	public Integer getRpSponsorContactType(){
		return this.rpSponsorContactType;
	}
	public void setRpSponsorContactId(String rpSponsorContactId){
		this.rpSponsorContactId=rpSponsorContactId;
	}
	@Column(name = "RP_SPONSOR_CONTACT_ID")
	public String getRpSponsorContactId(){
		return this.rpSponsorContactId;
	}
	public void setSponsorPersonalFirstName(String sponsorPersonalFirstName){
		this.sponsorPersonalFirstName=sponsorPersonalFirstName;
	}
	@Column(name = "SPONSOR_PERSONAL_FIRST_NAME")
	public String getSponsorPersonalFirstName(){
		return this.sponsorPersonalFirstName;
	}
	public void setFkUserSponsorPersonal(Integer fkUserSponsorPersonal){
		this.fkUserSponsorPersonal=fkUserSponsorPersonal;
	}
	@Column(name = "SPONSOR_PERSONAL_FK_USER")
	public Integer getFkUserSponsorPersonal(){
		return this.fkUserSponsorPersonal;
	}
	public void setSponsorPersonalLastName(String sponsorPersonalLastName){
		this.sponsorPersonalLastName=sponsorPersonalLastName;
	}
	@Column(name = "SPONSOR_PERSONAL_LAST_NAME")
	public String getSponsorPersonalLastName(){
		return this.sponsorPersonalLastName;
	}
	public void setFkAddSponsorPersonal(Integer fkAddSponsorPersonal){
		this.fkAddSponsorPersonal=fkAddSponsorPersonal;
	}
	@Column(name = "SPONSOR_PERSONAL_FK_ADD")
	public Integer getFkAddSponsorPersonal(){
		return this.fkAddSponsorPersonal;
	}
	public void setSponsorGenericTitle(String sponsorGenericTitle){
		this.sponsorGenericTitle=sponsorGenericTitle;
	}
	@Column(name = "SPONSOR_GENERIC_TITLE")
	public String getSponsorGenericTitle(){
		return this.sponsorGenericTitle;
	}
	public void setRpEmail(String rpEmail){
		this.rpEmail=rpEmail;
	}
	@Column(name = "RP_EMAIL")
	public String getRpEmail(){
		return this.rpEmail;
	}
	public void setRpPhone(String rpPhone){
		this.rpPhone=rpPhone;
	}
	@Column(name = "RP_PHONE")
	public String getRpPhone(){
		return this.rpPhone;
	}
	public void setRpExtn(Integer rpExtn){
		this.rpExtn=rpExtn;
	}
	@Column(name = "RP_EXTN")
	public Integer getRpExtn(){
		return this.rpExtn;
	}
	public void setSumm4SponsorType(Integer summ4SponsorType){
		this.summ4SponsorType=summ4SponsorType;
	}
	@Column(name = "SUMM4_SPONSOR_TYPE")
	public Integer getSumm4SponsorType(){
		return this.summ4SponsorType;
	}
	public void setSumm4SponsorId(String summ4SponsorId){
		this.summ4SponsorId=summ4SponsorId;
	}
	@Column(name = "SUMM4_SPONSOR_ID")
	public String getSumm4SponsorId(){
		return this.summ4SponsorId;
	}
	public void setFkSiteSumm4Sponsor(Integer fkSiteSumm4Sponsor){
		this.fkSiteSumm4Sponsor=fkSiteSumm4Sponsor;
	}
	@Column(name = "SUMM4_SPONSOR_FK_SITE")
	public Integer getFkSiteSumm4Sponsor(){
		return this.fkSiteSumm4Sponsor;
	}
	public void setFkAddSumm4Sponsor(Integer fkAddSumm4Sponsor){
		this.fkAddSumm4Sponsor=fkAddSumm4Sponsor;
	}
	@Column(name = "SUMM4_SPONSOR_FK_ADD")
	public Integer getfkAddSumm4Sponsor(){
		return this.fkAddSumm4Sponsor;
	}
	public void setSponsorProgramCode(String sponsorProgramCode){
		this.sponsorProgramCode=sponsorProgramCode;
	}
	@Column(name = "SPONSOR_PROGRAM_CODE")
	public String getSponsorProgramCode(){
		return this.sponsorProgramCode;
	}
	public void setFkCodelstCtrpStudyStatus(Integer fkCodelstCtrpStudyStatus){
		this.fkCodelstCtrpStudyStatus=fkCodelstCtrpStudyStatus;
	}
	@Column(name = "FK_CODELST_CTRP_STUDY_STATUS")
	public Integer getFkCodelstCtrpStudyStatus(){
		return this.fkCodelstCtrpStudyStatus;
	}
	public void setCtrpStudyStatusDate(Date ctrpStudyStatusDate){
		this.ctrpStudyStatusDate=ctrpStudyStatusDate;
	}
	@Column(name = "CTRP_STUDY_STATUS_DATE")
	public Date getCtrpStudyStatusDate(){
		return this.ctrpStudyStatusDate;
	}
	public void setCtrpStudyStopReason(String ctrpStudyStopReason){
		this.ctrpStudyStopReason=ctrpStudyStopReason;
	}
	@Column(name = "CTRP_STUDY_STOP_REASON")
	public String getCtrpStudyStopReason(){
		return this.ctrpStudyStopReason;
	}
	public void setCtrpStudyStartDate(Date ctrpStudyStartDate){
		this.ctrpStudyStartDate=ctrpStudyStartDate;
	}
	@Column(name = "CTRP_STUDY_START_DATE")
	public Date getCtrpStudyStartDate(){
		return this.ctrpStudyStartDate;
	}
	public void setIsStartActual(Integer isStartActual){
		this.isStartActual=isStartActual;
	}
	@Column(name = "IS_START_ACTUAL")
	public Integer getIsStartActual(){
		return this.isStartActual;
	}
	public void setCtrpStudyCompDate(Date ctrpStudyCompDate){
		this.ctrpStudyCompDate=ctrpStudyCompDate;
	}
	@Column(name = "CTRP_STUDY_COMP_DATE")
	public Date getCtrpStudyCompDate(){
		return this.ctrpStudyCompDate;
	}
	public void setIsCompActual(Integer isCompActual){
		this.isCompActual=isCompActual;
	}
	@Column(name = "IS_COMP_ACTUAL")
	public Integer getIsCompActual(){
		return this.isCompActual;
	}
	public void setCtrpStudyAccOpenDate(Date ctrpStudyAccOpenDate){
		this.ctrpStudyAccOpenDate=ctrpStudyAccOpenDate;
	}
	@Column(name = "CTRP_STUDYACC_OPEN_DATE")
	public Date getCtrpStudyAccOpenDate(){
		return this.ctrpStudyAccOpenDate;
	}
	public void setCtrpStudyAccClosedDate(Date ctrpStudyAccClosedDate){
		this.ctrpStudyAccClosedDate=ctrpStudyAccClosedDate;
	}
	@Column(name = "CTRP_STUDYACC_CLOSED_DATE")
	public Date getCtrpStudyAccClosedDate(){
		return this.ctrpStudyAccClosedDate;
	}
	public void setSiteTargetAccrual(Integer siteTargetAccrual){
		this.siteTargetAccrual=siteTargetAccrual;
	}
	@Column(name = "SITE_TARGET_ACCRUAL")
	public Integer getSiteTargetAccrual(){
		return this.siteTargetAccrual;
	}
	public void setOversightCountry(Integer oversightCountry){
		this.oversightCountry=oversightCountry;
	}
	@Column(name = "OVERSIGHT_COUNTRY")
	public Integer getOversightCountry(){
		return this.oversightCountry;
	}
	public void setOversightOrganization(Integer oversightOrganization){
		this.oversightOrganization=oversightOrganization;
	}
	@Column(name = "OVERSIGHT_ORGANIZATION")
	public Integer getOversightOrganization(){
		return this.oversightOrganization;
	}
	// corrected getters setters naming
	public void setFdaIntvenIndicator(Integer fdaIntvenIndicator){
		this.fdaIntvenIndicator=fdaIntvenIndicator;
	}
	@Column(name = "FDA_INTVEN_INDICATOR")
	public Integer getFdaIntvenIndicator(){
		return this.fdaIntvenIndicator;
	}
	public void setSection801Indicator(Integer section801Indicator){
		this.section801Indicator=section801Indicator;
	}
	@Column(name = "SECTION_801_INDICATOR")
	public Integer getSection801Indicator(){
		return this.section801Indicator;
	}
	public void setDelayPostIndicator(Integer delayPostIndicator){
		this.delayPostIndicator=delayPostIndicator;
	}
	@Column(name = "DELAY_POST_INDICATOR")
	public Integer getDelayPostIndicator(){
		return this.delayPostIndicator;
	}
	// corrected getters setters naming
	public void setDmAppointIndicator(Integer dmAppointIndicator){
		this.dmAppointIndicator=dmAppointIndicator;
	}
	@Column(name = "DM_APPOINT_INDICATOR")
	public Integer getDmAppointIndicator(){
		return this.dmAppointIndicator;
	}
	public void setDeletedFlag(Integer deletedFlag){
		this.deletedFlag=deletedFlag;
	}
	@Column(name = "DELETED_FLAG")
	public Integer getDeletedFlag(){
		return this.deletedFlag;
	}
	public void setIpAdd(String ipAdd){
		this.ipAdd=ipAdd;
	}
	@Column(name = "IP_ADD")
	public String getIpAdd(){
		return this.ipAdd;
	}
	public void setCreator(Integer creator){
		this.creator=creator;
	}
	@Column(name = "CREATOR")
	public Integer getCreator(){
		return this.creator;
	}
	public void setLastModifiedBy(Integer lastModifiedBy){
		this.lastModifiedBy=lastModifiedBy;
	}
	@Column(name = "LAST_MODIFIED_BY")
	public Integer getLastModifiedBy(){
		return this.lastModifiedBy;
	}
}
