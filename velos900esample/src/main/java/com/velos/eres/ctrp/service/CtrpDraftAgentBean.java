package com.velos.eres.ctrp.service;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.ArrayList;
import java.util.Hashtable;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.business.audit.impl.AuditBean;
import com.velos.eres.business.common.CtrpDraftDao;
import com.velos.eres.ctrp.business.CtrpDraftBean;
import com.velos.eres.ctrp.business.CtrpValidationDao;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

@Stateless
@Remote( { CtrpDraftAgent.class } )
public class CtrpDraftAgentBean implements CtrpDraftAgent {
    @PersistenceContext(unitName = "eres")
    protected EntityManager em;

	public CtrpDraftBean getCtrpDraftBean(Integer id) {
		if (id == null || id == 0) { return null; }
		CtrpDraftBean ctrpDraftBean = null;
		try {
			ctrpDraftBean = (CtrpDraftBean) em.find(CtrpDraftBean.class, new Integer(id));
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftAgentBean.getCtrpDraftBean: "+e);
		}
		return ctrpDraftBean;
	}

	public Integer createCtrpDraft(CtrpDraftBean ctrpDraftBean) {
		Integer output = 0;
		try {
			ctrpDraftBean.setDeletedFlag(0); // New draft cannot have deleted_flag = 1
			em.persist(ctrpDraftBean);
			output = ctrpDraftBean.getId();
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftAgentBean.createCtrpDraft: "+e);
			output = -1;
		}
		return output;
	}

	public Integer updateCtrpDraft(CtrpDraftBean ctrpDraftBean) {
		Integer output = 0;
		if (ctrpDraftBean.getId() < 1) { return output; }
		try {
			CtrpDraftBean originalBean = (CtrpDraftBean) em.find(CtrpDraftBean.class, ctrpDraftBean.getId());
			ctrpDraftBean.setCreator(originalBean.getCreator()); // Keep the original
			if (ctrpDraftBean.getIpAdd() == null) {
				ctrpDraftBean.setIpAdd(originalBean.getIpAdd()); // Keep the original if new is null
			}
			if (ctrpDraftBean.getDeletedFlag() == null) { ctrpDraftBean.setDeletedFlag(0); }
			em.merge(ctrpDraftBean);
			output = ctrpDraftBean.getId();
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftAgentBean.updateCtrpDraft: "+e);
			output = -1;
		}
		return output;
	}

	public CtrpValidationDao runCtrpDraftValidation(Integer ctrpDraftId) {
		Integer output = 0;
		CtrpValidationDao ctrpValidationDao = new CtrpValidationDao();
		
		//ctrpValidationDao.setDraftId(ctrpDraftId);
		try {
			ctrpValidationDao.setCtrpDraftId(ctrpDraftId);
			ctrpValidationDao.runCtrpDraftValidation();
			//output = ctrpDraftBean.getId();
		} catch (Exception e) {
			Rlog.fatal("ctrp", "Exception in CtrpDraftAgentBean.runCtrpDraftValidation: "+e);
			output = -1;
		}
		//return output;
		
		return ctrpValidationDao;
	}
	
	 public CtrpDraftBean findByDraftId(Integer ctrpDraftId) {
        Query query = em.createNamedQuery("findCtrpDraftByDraftId");
        query.setParameter("draftId", ctrpDraftId.intValue());
        ArrayList list = (ArrayList) query.getResultList();
        if (list == null || list.size() == 0) { return null; }
        return (CtrpDraftBean) list.get(0);
    }

	 public ArrayList findDraftsByStudyIds(Integer accId, String studyIds) {
		Query query = em.createNamedQuery("findCtrpDraftsByStudyIds");
		query.setParameter("fk_account", accId);
		query.setParameter("fkStudys", studyIds);
		ArrayList list = (ArrayList) query.getResultList();
		if (list == null || list.size() == 0) { return null; }
		return list;
	}
	 
	public int unmarkStudyCtrpReportable(String studyIds) {
		int ret=0;
		try {
	    
            Rlog.debug("studyDraft",
                    "In unmarkStudyCtrpReportable in CtrpDraftAgentBean line number 0");
            CtrpDraftDao ctrpDraftDao = new CtrpDraftDao();
            Rlog.debug("studyDraft",
                    "In unmarkStudyCtrpReportable in CtrpDraftAgentBean line number 1");
            ret= ctrpDraftDao.unmarkStudyCtrpReportable(studyIds);
            return ret;

        } catch (Exception e) {
            Rlog.fatal("studyDraft",
                    "Exception In unmarkStudyCtrpReportable in CtrpDraftAgentBean " + e);
            return ret;
        }
	}
	public int deleteStudyDrafts(String studyIds) {
		int ret=0;
		try {	
			
			Rlog.debug("studyDraft",
			"In deleteStudyDrafts in CtrpDraftAgentBean line number 0");
			CtrpDraftDao ctrpDraftDao = new CtrpDraftDao();
			Rlog.debug("studyDraft",
			"In deleteStudyDrafts in CtrpDraftAgentBean line number 1");
			ret= ctrpDraftDao.deleteStudyDrafts(studyIds);
			return ret;
			
		} catch (Exception e) {
			Rlog.fatal("studyDraft",
					"Exception In deleteStudyDrafts in CtrpDraftAgentBean " + e);
			return ret;
		}
	}
	public int deleteStudyDrafts(String studyIds,Hashtable<String, String> args) {  //Modified for Bug#8029:Akshi
		int ret=0;
		String condition="";
		try {
			AuditBean audit=null; //Audit Bean for AppDelete
			String pkVal = "";
			String ridVal = "";
            String currdate =DateUtil.getCurrentDate(); /*gets the current date from DateUtil*/
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
            
        		condition = condition + studyIds;  
        	
        	condition = "FK_STUDY IN ("+condition.substring(1);

        	Hashtable<String, ArrayList<String>> getRowValues = AuditUtils.getRowValues("ER_CTRP_DRAFT",condition,"eres");/*Fetches the RID/PK_VALUE*/
        	ArrayList<String> rowPK  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_PK_KEY);
    		ArrayList<String> rowRID  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_RID_KEY);
    		//String getRidValue= AuditUtils.getRidValues("ER_PATPROT","PK_PATPROT="+condition);/*Fetches the RID/PK_VALUE*/ 
            for(int i=0; i<rowPK.size(); i++)
			{
    			pkVal = rowPK.get(i);
    			ridVal = rowRID.get(i);
    			audit = new AuditBean("ER_CTRP_DRAFT",pkVal,ridVal,
        			userID,currdate,appModule,ipAdd,reason);
    			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
			}
			Rlog.debug("studyDraft",
			"In deleteStudyDrafts in CtrpDraftAgentBean line number 0");
			CtrpDraftDao ctrpDraftDao = new CtrpDraftDao();
			Rlog.debug("studyDraft",
			"In deleteStudyDrafts in CtrpDraftAgentBean line number 1");
			ret= ctrpDraftDao.deleteStudyDrafts(studyIds);
			return ret;
			
		} catch (Exception e) {
			Rlog.fatal("studyDraft",
					"Exception In deleteStudyDrafts in CtrpDraftAgentBean " + e);
			return ret;
		}
	}
}
