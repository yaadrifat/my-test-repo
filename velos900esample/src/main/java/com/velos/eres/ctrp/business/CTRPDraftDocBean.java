/*
 * Classname : CTRPDraftDocBean
 * 
 * Version information: 1.0
 *
 * Date: 12/20/2011
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Ashu Khatkar
 */

package com.velos.eres.ctrp.business;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;

@Entity
@Table(name="ER_CTRP_DOCUMENTS")
public class CTRPDraftDocBean implements Serializable
{
  public Integer pkCtrpDoc;
  public Integer fkCtrpDraft;
  public Integer fkStudyApndx;
  public Integer fkCtrpDocType;
  public Integer creator;
  public Integer lastModifiedBy;
  public String ipAdd;


  public CTRPDraftDocBean()
  {
  }

  public CTRPDraftDocBean(Integer pkCtrpDoc, Integer fkCtrpDraft, Integer fkStudyApndx, Integer fkCtrpDocType)
  {
    setPkCtrpDoc(pkCtrpDoc);
    setFkCtrpDraft(fkCtrpDraft);
    setFkStudyApndx(fkStudyApndx);
    setFkCtrpDocType(fkCtrpDocType);
  }

  @Id
  @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN")
  @SequenceGenerator(name="SEQ_GEN", sequenceName="SEQ_ER_CTRP_DOCUMENTS", allocationSize=1)
  @Column(name="PK_CTRP_DOC")
  
  public Integer getPkCtrpDoc()
  {
    return this.pkCtrpDoc;
  }

  public void setPkCtrpDoc(Integer pkCtrpDoc)
  {
    this.pkCtrpDoc = pkCtrpDoc;
  }

  @Column(name="FK_CTRP_DRAFT")
  public Integer getFkCtrpDraft()
  {
    return this.fkCtrpDraft;
  }

  public void setFkCtrpDraft(Integer fkCtrpDraft)
  {
    this.fkCtrpDraft = fkCtrpDraft;
  }

  @Column(name="FK_STUDYAPNDX")
  public Integer getFkStudyApndx()
  {
    return this.fkStudyApndx;
  }

  public void setFkStudyApndx(Integer fkStudyApndx)
  {
    this.fkStudyApndx = fkStudyApndx;
  }

  @Column(name="FK_CODELST_CTRP_DOCTYPE")
  public Integer getFkCtrpDocType()
  {
    return this.fkCtrpDocType;
  }

  public void setFkCtrpDocType(Integer fkCtrpDocType)
  {
    this.fkCtrpDocType = fkCtrpDocType;
  }

  @Column(name="IP_ADD")
  public String getIpAdd()
  {
    return this.ipAdd;
  }

  public void setIpAdd(String ipAdd)
  {
    this.ipAdd = ipAdd;
  }

  @Column(name="CREATOR")
 
  public Integer getCreator() {
	return creator;
 }

	public void setCreator(Integer creator) {
		this.creator = creator;
	}



  @Column(name="LAST_MODIFIED_BY")
  public Integer getLastModifiedBy() {
	return lastModifiedBy;
}

public void setLastModifiedBy(Integer lastModifiedBy) {
	this.lastModifiedBy = lastModifiedBy;
}

 
  /**
   * 
   * @param ssk
   * @return int
   */
  
  public int updateCtrpDraftDocInfo(CTRPDraftDocBean ssk) {
		try{			
			if(ssk.getPkCtrpDoc()>0){
			setPkCtrpDoc(ssk.getPkCtrpDoc());}
			
			setFkCtrpDocType(ssk.getFkCtrpDocType());
			setFkCtrpDraft(ssk.getFkCtrpDraft());
			setFkStudyApndx(ssk.getFkStudyApndx());
			setCreator(ssk.getCreator());
	        setLastModifiedBy(ssk.getLastModifiedBy());
	        setIpAdd(ssk.getIpAdd());
	        
			return 0;
		}catch(Exception e){
	           Rlog.fatal("CTRPDraftDoc", " error in CTRPDraftDocBean.updateCtrpDraftDocInfo");
	           return -2;				
		}
		
}


  
  
}