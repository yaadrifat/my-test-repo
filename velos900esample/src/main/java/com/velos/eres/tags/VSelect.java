/**
 * __astcwz_cmpnt_det: Author: Konesa CodeWizard Date and Time Created: Wed Jul
 * 09 14:45:29 IST 2003 Note: DO NOT DELETE OR MODIFY KONESA CODEWIZARD COMMENTS
 * STARTING WITH ///__astcwz_ and /**__astcwz_
 */
// /__astcwz_model_idt#(0)
// /__astcwz_maxim_idt#(1587)
// /__astcwz_packg_idt#(1580,1582,1583,1584~,,,)
package com.velos.eres.tags;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

// /__astcwz_class_idt#(1478!n)
public class VSelect extends VInputBase {
    // /__astcwz_attrb_idt#(1487)
    private boolean multiple;

    // /__astcwz_attrb_idt#(1489)
    private int size;

    private String mandatoryOverRide;

    // /__astcwz_default_constructor
    public VSelect() {
        this.mandatoryOverRide = "";
    }

    // /__astcwz_opern_idt#()
    public boolean getMultiple() {
        return multiple;
    }

    // /__astcwz_opern_idt#()
    public int getSize() {
        return size;
    }

    // /__astcwz_opern_idt#()
    public void setMultiple(boolean amultiple) {
        multiple = amultiple;
    }

    // /__astcwz_opern_idt#()
    public void setSize(int asize) {
        size = asize;
    }

    public String getSizeAttribute() {

        // get size
        if (this.size > 0) {
            return " size = \"" + this.size + "\"";
        } else {
            return "";

        }

    }

    public String getMultipleAttribute() {

        // get multiple
        if (this.multiple) {
            return " multiple = \"1\"";
        } else {
            return "";

        }

    }

    public String getMandatoryOverRide() {
        return this.mandatoryOverRide;
    }

    public void setMandatoryOverRide(String mandatoryOverRide) {
        this.mandatoryOverRide = mandatoryOverRide;
    }

    public String drawXSL() {

        StringBuffer xslStr = new StringBuffer();
        String str = "";
        // if field label is hidden, alignment=top is not applicable

        String hideLabelStr = "";
        hideLabelStr = getHideLabel();
        if (StringUtil.isEmpty(hideLabelStr)) {
            hideLabelStr = "0";
        }

        if (hideLabelStr.equals("1") && align.equals("top")) {
            align = "left";
        }

        // get input label XSL
        if (!align.equals("top")) {
            xslStr.append(getInputLabelXSL());
        }
        xslStr.append("<td>");

        // get input label XSL
        if (align.equals("top")) {
            xslStr.append(getInputLabelXSL());
        }

        xslStr.append("<span id=\""+this.name+"_span\"><select ");
        xslStr.append(getTagClass());
        xslStr.append(getStyle());
        // xslStr.append(getTitle());
        xslStr.append(getToolTip());
        xslStr.append(getId());
        xslStr.append(getDisabledAttribute());
        xslStr.append(getNameAttribute());
        xslStr.append(getReadOnlyAttribute());
        xslStr.append(getMultipleAttribute());
        xslStr.append(getSizeAttribute());
        // sonia 05/11/04 for field Actions
        xslStr.append(getFieldActionJavaScript());

        xslStr.append(" >");
        // add 'Select an Option' in the dropdown
        xslStr.append("<option>");
        xslStr.append("<xsl:attribute name=\"value\"></xsl:attribute>");
        xslStr.append("Select an Option");
        xslStr.append("</option>");

        xslStr.append("<xsl:for-each select = \"" + this.name + "/resp\">");
        xslStr.append("<option>");
        xslStr
                .append("<xsl:attribute name=\"value\"><xsl:value-of select=\"@dataval\"/></xsl:attribute>");
        xslStr.append("<xsl:if test=\"@selected='1'\">");
        xslStr.append("<xsl:attribute name=\"selected\"/>");
        xslStr.append("</xsl:if>");
        xslStr.append("  <xsl:value-of select=\"@dispval\"/>");
        xslStr.append("</option>");
        xslStr.append("</xsl:for-each>");
        xslStr.append("</select> </span>");//end span
        // xslStr.append(getHelpIcon());

        // madatory override check
        if (mandatoryOverRide == null)
            mandatoryOverRide = "0";
        if (mandatoryOverRide.equals("1")) {
            Rlog.debug("common", "inside mandatoryOverRide*****");
            str = "mnd";
            /*
             * strBuf = appendHiddenFields(xslStr, str); xslBuf = ""; xslBuf =
             * xslBuf.append(strBuf);
             */
            xslStr.append("<input ");
            Rlog.debug("common", "inside drawxsl 1 getHiddenId(str) vs::"
                    + getHiddenId(str));
            xslStr.append(getHiddenId(str));
            Rlog.debug("common",
                    "inside drawxsl 1 getHiddenNameAttribute(str) vs::"
                            + getHiddenNameAttribute(str));
            xslStr.append(getHiddenNameAttribute(str));
            Rlog.debug("common", "inside drawxsl 1 getHiddenType(str) vs::"
                    + getHiddenType());
            xslStr.append(getHiddenType());
            xslStr.append(" >");
            Rlog.debug("common",
                    "inside drawxsl 1 getHiddenValueAttribute(str) vs::"
                            + getHiddenValueAttribute(str));
            xslStr.append(getHiddenValueAttribute(str));
            xslStr.append("</input>");
            Rlog.debug("common", "inside drawxsl before to xsl vs");
            Rlog.debug("common", "inside drawxsl 1 xslStr() vs::"
                    + xslStr.toString());
            Rlog.debug("common", "inside drawxsl after to xsl vs");

            xslStr.append("<input ");
            xslStr.append(getHiddenReasonId(str));
            xslStr.append(getHiddenReasonNameAttr(str));
            xslStr.append(getHiddenType());
            xslStr.append(" >");
            xslStr.append("</input>");
            Rlog.debug("common", "inside drawxsl after adding reason"
                    + xslStr.toString());

        }
        xslStr.append("</td>");

        return xslStr.toString();

    }

}
