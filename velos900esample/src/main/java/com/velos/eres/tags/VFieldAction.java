package com.velos.eres.tags;

import java.io.StringReader;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import oracle.xml.parser.v2.DOMParser;
import oracle.xml.parser.v2.XMLDocument;
import oracle.xml.parser.v2.XMLElement;
import oracle.xml.parser.v2.XMLNode;

import org.w3c.dom.NodeList;

import com.velos.eres.service.util.EnvUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* **************************History*********************************
 Modified by :Sonia Sahni  Modified On:06/21/2004  
 Comment: 1. Added JavaDoc comments for some methods 
 2. Modified methods getFieldActions() and getFormFieldActionScripts. See method History  	  
 **************************END****************************************/

/**
 * VFieldAction encapsulates Field Action information and exposes methods to get
 * Field Actions , get Field Action Keywords, generate scripts, etc.
 * 
 * @author Sonia Sahni
 * @vesion %I%, %G%
 */
public class VFieldAction {
    private String actionType;

    private String actionTemplate;

    private String actionCondition;

    private String sourceField;

    private Hashtable htKeywordValues;

    private String fieldSystemId;

    /**
     * Flag to indicate if the field action should be executed when the form is
     * activated in modify mode
     */
    private boolean includeInFormActivation;

    /**
     * Gets value of includeInFormActivation flag.
     * 
     * @return value for includeInFormActivation flag. When set to true, the
     *         field action is also executed when the form is opened in Modify
     *         mode
     */
    public boolean getIncludeInFormActivation() {
        return includeInFormActivation;
    }

    /**
     * Sets the value of includeInFormActivation.
     * 
     * @param includeInFormActivation
     *            The value to assign includeInFormActivation. <BR>
     *            When set to true, the field action is also executed when the
     *            form is opened in Modify mode
     */
    public void setIncludeInFormActivation(boolean includeInFormActivation) {
        this.includeInFormActivation = includeInFormActivation;
    }

    /**
     * Returns the value of fieldSystemId.
     */
    public String getFieldSystemId() {
        return fieldSystemId;
    }

    /**
     * Sets the value of fieldSystemId.
     * 
     * @param fieldSystemId
     *            The value to assign fieldSystemId.
     */
    public void setFieldSystemId(String fieldSystemId) {
        this.fieldSystemId = fieldSystemId;
    }

    /**
     * Returns the value of actionType.
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * Sets the value of actionType.
     * 
     * @param actionType
     *            The value to assign actionType.
     */
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    /**
     * Returns the value of actionCondition.
     */
    public String getActionCondition() {
        return actionCondition;
    }

    /**
     * Sets the value of actionCondition.
     * 
     * @param actionCondition
     *            The value to assign actionCondition.
     */
    public void setActionCondition(String actionCondition) {
        this.actionCondition = actionCondition;
    }

    /**
     * Returns the value of sourceField.
     */
    public String getSourceField() {
        return sourceField;
    }

    /**
     * Sets the value of sourceField.
     * 
     * @param sourceField
     *            The value to assign sourceField.
     */
    public void setSourceField(String sourceField) {
        this.sourceField = sourceField;
    }

    /**
     * Returns the value of htKeywordValues.
     */
    public Hashtable getHtKeywordValues() {
        return htKeywordValues;
    }

    /**
     * Returns the value of a keyword from htKeywordValues (if the keyword
     * exists)
     */
    public ArrayList getHtKeywordValues(String keyWrd) {
        ArrayList arrValues = new ArrayList();

        if (htKeywordValues != null && htKeywordValues.size() > 0) {

            // check if the key word exists in the hashtable
            if (htKeywordValues.containsKey(keyWrd)) {
                arrValues = (ArrayList) htKeywordValues.get(keyWrd);
            }

        }
        return arrValues;
    }

    /**
     * Sets the value of htKeywordValues.
     * 
     * @param htKeywordValues
     *            The value to assign htKeywordValues.
     */
    public void setHtKeywordValues(Hashtable htKeywordValues) {
        this.htKeywordValues = htKeywordValues;
    }

    /**
     * Returns the value of actionTemplate.
     */
    public String getActionTemplate() {
        return actionTemplate;
    }

    /**
     * Sets the value of actionTemplate.
     * 
     * @param actionTemplate
     *            The value to assign actionTemplate.
     */
    public void setActionTemplate(String actionTemplate) {
        this.actionTemplate = actionTemplate;
    }

    /**
     * Basic constructor for VFieldAction
     */
    public VFieldAction() {
    }

    /**
     * Returns a Hastable of VFieldAction objects for every field in the form
     * for which any field action is set
     * 
     * @param formId
     *            form Id
     * @return Hashtable A Hastable of : Key: Field Id Value: ArrayList of
     *         VFieldAction objects for the field
     */

    /***************************************************************************
     * **************************History Modified by :Sonia Sahni Modified
     * On:06/21/2004 Comment: 1. Added JavaDoc comments 2. To read and set
     * template's activation_flag setting. END*************************
     **************************************************************************/

    public static Hashtable getFieldActions(String formId) {

        StringBuffer sbFfldSQL = new StringBuffer();

        // sbFfldSQL.append("Select FK_FIELD,PK_FLDACTION,FLDACTION_TYPE,
        // FLDACTION_CONDITION, e.FLDACTIONLIB_TEMPLATE.getClobVal()
        // FLDACTIONLIB_TEMPLATE,FLD_SYSTEMID ");
        // sbFfldSQL.append(" from er_fldaction, er_fldactionlib e, er_fldlib
        // Where FK_FORM = ? and FLDACTION_TYPE = FLDACTIONLIB_TYPE and PK_FIELD
        // = FK_FIELD Order by FK_FIELD ") ;

        sbFfldSQL
                .append("Select nvl(FK_FIELD,0)FK_FIELD ,PK_FLDACTION,FLDACTION_TYPE, FLDACTION_CONDITION, e.FLDACTIONLIB_TEMPLATE.getClobVal() FLDACTIONLIB_TEMPLATE, nvl(e.ACTIVATION_FLAG,0) ACTIVATION_FLAG ");
        sbFfldSQL
                .append(" from er_fldaction, er_fldactionlib e Where FK_FORM  = ? and FLDACTION_TYPE = FLDACTIONLIB_TYPE  Order by FK_FIELD ASC");

        PreparedStatement pstmtFld = null;
        Connection conn = null;

        int field = 0;
        int prevField = 0;
        int fldPkAction = 0;
        int totalActionRows = 0;
        String fldActionType;
        String fldActionCondition;
        String fldActionTemplate = "";
        String sourceFld = "";
        Clob templateCLob = null;
        int activationFlag = 0;

        Hashtable htFields = new Hashtable();

        ArrayList arVFieldActionObj = new ArrayList();
        String fldSysId;

        try {
            conn = EnvUtil.getConnection();

            pstmtFld = conn.prepareStatement(sbFfldSQL.toString());

            pstmtFld.setString(1, formId);
            ResultSet rs = pstmtFld.executeQuery();

            Rlog
                    .debug("formlib",
                            "VFieldAction.getFieldActions method started");

            while (rs.next()) {
                totalActionRows++;

                sourceFld = rs.getString("FK_FIELD");

                field = StringUtil.stringToNum(sourceFld);

                fldPkAction = rs.getInt("PK_FLDACTION");
                fldActionType = rs.getString("FLDACTION_TYPE");
                fldActionCondition = rs.getString("FLDACTION_CONDITION");
                activationFlag = rs.getInt("ACTIVATION_FLAG");

                templateCLob = rs.getClob("FLDACTIONLIB_TEMPLATE");

                if (!(templateCLob == null)) {
                    fldActionTemplate = templateCLob.getSubString(1,
                            (int) templateCLob.length());
                }

                // fldSysId = rs.getString("FLD_SYSTEMID");

                if (totalActionRows == 1) {
                    prevField = field;
                }

                if (prevField != field) {

                    if (prevField == 0) // for multiple source type actions
                    {
                        Rlog.debug("formlib", "sonia......prevField "
                                + prevField);

                        if (htFields.containsKey(String.valueOf(prevField))) {
                            ArrayList arPrev = new ArrayList();
                            arPrev = (ArrayList) htFields.get(String
                                    .valueOf(prevField));
                            if (arPrev != null && arPrev.size() > 0) {
                                arVFieldActionObj.addAll(arPrev);
                            }
                        }
                    }

                    htFields.put(String.valueOf(prevField), new ArrayList(
                            arVFieldActionObj));
                    arVFieldActionObj.clear();
                }

                // create a VFieldAction object
                VFieldAction fldActionObj = new VFieldAction();
                Hashtable htActionInfo = new Hashtable();

                // fldActionObj.setFieldSystemId(fldSysId);
                fldActionObj.setActionType(fldActionType);
                fldActionObj.setActionTemplate(fldActionTemplate);
                fldActionObj.setSourceField(sourceFld);
                fldActionObj.setActionCondition(fldActionCondition);

                htActionInfo = fldActionObj.getFieldActionKeywords(fldPkAction);

                // get keyword information for this field Action

                fldActionObj.setHtKeywordValues(htActionInfo);

                if (activationFlag == 1)
                    fldActionObj.setIncludeInFormActivation(true);
                else
                    fldActionObj.setIncludeInFormActivation(false);

                arVFieldActionObj.add(fldActionObj);

                prevField = field;
            }

            // for last field
            if (field == 0) // for multiple source type actions
            {
                if (htFields.containsKey(sourceFld)) {
                    ArrayList arPrev = new ArrayList();
                    arPrev = (ArrayList) htFields.get(sourceFld);
                    if (arPrev != null && arPrev.size() > 0) {
                        arVFieldActionObj.addAll(arPrev);
                    }
                }
            }
            htFields.put(sourceFld, new ArrayList(arVFieldActionObj));
            Rlog.debug("formlib", "sonia......sourceFld " + sourceFld);

            return htFields;

        } catch (Exception ex) {
            Rlog.fatal("formlib",
                    "VFieldAction.getFieldActions -- > Exception " + ex);
            return htFields;
        } finally {
            try {
                if (pstmtFld != null)
                    pstmtFld.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

        // end of method

    }

    /**
     * Returns a Hastable of Action - Keywords data for every field action
     * 
     * @param pkFieldAction
     *            Primary Key of the Field Action
     * @return Hastable of Action - Keywords data for every field action: Key:
     *         Action Keyword Name Value : ArrayList of keyword values
     */
    public Hashtable getFieldActionKeywords(int pkFieldAction) {

        StringBuffer sbFldActionInfoSQL = new StringBuffer();
        sbFldActionInfoSQL
                .append(" Select  INFO_TYPE , INFO_VALUE from er_fldactioninfo ");
        sbFldActionInfoSQL
                .append(" Where FK_FLDACTION  = ? Order by INFO_TYPE ");

        PreparedStatement pstmtInfo = null;
        Connection conn = null;

        int totalActionInfoRows = 0;
        String infoType = "";
        String prevInfoType = "";
        String infoValue = "";

        Hashtable htKeyInfo = new Hashtable();

        try {
            conn = EnvUtil.getConnection();

            pstmtInfo = conn.prepareStatement(sbFldActionInfoSQL.toString());

            pstmtInfo.setInt(1, pkFieldAction);
            ResultSet rs = pstmtInfo.executeQuery();

            Rlog.debug("formlib",
                    "VFieldAction.getFieldActionKeywords method started");

            ArrayList keyWordValues = new ArrayList();

            while (rs.next()) {
                totalActionInfoRows++;

                infoType = rs.getString("INFO_TYPE");
                infoValue = rs.getString("INFO_VALUE");

                if (totalActionInfoRows == 1) {
                    prevInfoType = infoType;
                }

                if (!infoType.equals(prevInfoType)) {
                    if (keyWordValues.size() > 0) // if for the keyword there
                                                    // are any values specified
                    {
                        htKeyInfo.put(prevInfoType,
                                new ArrayList(keyWordValues));
                        keyWordValues.clear();
                    }
                }

                keyWordValues.add(infoValue);
                prevInfoType = infoType;
            }

            // for the last keyword in the loop
            if (keyWordValues.size() > 0) {
                htKeyInfo.put(prevInfoType, keyWordValues);

            }

            return htKeyInfo;

        } catch (Exception ex) {
            Rlog.fatal("formlib",
                    "VFieldAction.getFieldActionKeywords -- > Exception " + ex);
            return htKeyInfo;
        } finally {
            try {
                if (pstmtInfo != null)
                    pstmtInfo.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        // end of method
    }

    /**
     * Generates and returns java script for field Action
     * 
     * @return String of java script for field Action
     */

    public String getActionScript() {

        StringBuffer sbActionScr = new StringBuffer();
        String templateXMLStr = "";
        String varStr = "";
        String strContent = "";
        String hasKeywords = "";
        String replaceKeyWord;
        String replaceType = "";
        int keyWordValuesSize = 0;
        String replaceValues = "";
        String actionScript = "";

        templateXMLStr = getActionTemplate();

        if (!StringUtil.isEmpty(templateXMLStr)) {

            // sbActionScr.append("if ( currentfld.name == '" +
            // getFieldSystemId() + "' ) { ");

            // read from template

            try {
                DOMParser parser = new DOMParser();
                parser.setPreserveWhitespace(true);
                parser.parse(new StringReader(templateXMLStr));
                XMLDocument doc = parser.getDocument();

                // get script variables
                NodeList nl = doc.getElementsByTagName("variable");

                for (int n = 0; n < nl.getLength(); n++) {
                    XMLElement elem = (XMLElement) nl.item(n);
                    XMLNode textNode = (XMLNode) elem.getFirstChild();
                    varStr = textNode.getNodeValue();

                    if (!StringUtil.isEmpty(varStr)) {
                        sbActionScr.append(varStr);

                    }
                } // end of for for variable nodes

                // get script contents

                NodeList nlContent = doc.getElementsByTagName("content");

                for (int n = 0; n < nlContent.getLength(); n++) {
                    XMLElement elem = (XMLElement) nlContent.item(n);
                    XMLNode textNode = (XMLNode) elem.getFirstChild();
                    strContent = textNode.getNodeValue();

                    // check if the textNode has any keywords to be replaced.
                    if (elem.hasAttribute("keywords")) {

                        // check the value of the attribute

                        hasKeywords = elem.getAttribute("keywords");

                        if (elem.hasAttribute("type")) {
                            replaceType = elem.getAttribute("type");
                        }

                        if (hasKeywords.equals("yes")) {
                            // get the keywords
                            NodeList nlKeyWord = elem
                                    .getElementsByTagName("keyword");

                            for (int p = 0; p < nlKeyWord.getLength(); p++) {
                                ArrayList arrValues = new ArrayList();
                                XMLElement elemKey = (XMLElement) nlKeyWord
                                        .item(p);
                                XMLNode textNodeKey = (XMLNode) elemKey
                                        .getFirstChild();

                                replaceKeyWord = textNodeKey.getNodeValue();

                                // find the replaceKeyWord in
                                // VFieldAction.htKeywordValues }

                                arrValues = getHtKeywordValues(replaceKeyWord);

                                keyWordValuesSize = arrValues.size();

                                if (replaceType.equals("loop")) {
                                    // replace the keyword in contents for all
                                    // the values in arrValues; new line for
                                    // each keyword value

                                    StringBuffer sbLoop = new StringBuffer();
                                    String newStrContent = "";

                                    for (int i = 0; i < keyWordValuesSize; i++) {
                                        replaceValues = (String) arrValues
                                                .get(i);
                                        if (replaceKeyWord
                                                .equals("[VELCONDITIONVALUE]"))
                                            replaceValues = StringUtil
                                                    .escapeSpecialCharJS(replaceValues);

                                        newStrContent = StringUtil.replace(
                                                strContent, replaceKeyWord,
                                                replaceValues);
                                        newStrContent = StringUtil.replace(
                                                newStrContent, "[VELCOUNTER]",
                                                String.valueOf(i));

                                        sbLoop.append(newStrContent);

                                    }

                                    strContent = sbLoop.toString();
                                } else // replaceType != loop
                                {

                                    if (keyWordValuesSize > 0) {
                                        replaceValues = (String) arrValues
                                                .get(0);
                                        if (replaceKeyWord
                                                .equals("[VELCONDITIONVALUE]"))
                                            replaceValues = StringUtil
                                                    .escapeSpecialCharJS(replaceValues);
                                        Rlog.debug("formlib",
                                                "VFieldAction.getActionScript() : not loop type:"
                                                        + replaceValues);
                                        strContent = StringUtil.replace(
                                                strContent, replaceKeyWord,
                                                replaceValues);

                                    }
                                }

                            }

                        }
                    } // end of if for keywords

                    if (!StringUtil.isEmpty(strContent)) {
                        sbActionScr.append(strContent);

                    }

                } // end of for for content nodes
                //	
            }// end of try
            catch (Exception e) {
                Rlog.fatal("formlib",
                        "Exception in VFieldAction.getActionScript() : " + e);
            }

            // sbActionScr.append(" } ");

            Rlog.debug("formlib", "VFieldAction.getActionScript() : "
                    + sbActionScr.toString());
        }// end of empty check for string
        actionScript = StringUtil.decodeString(sbActionScr.toString());
        return actionScript;
    }

    public static String getActionConditionOptions(String selValue) {
        StringBuffer sbCondition = new StringBuffer();

        String[] arrCondition = { "velEquals", "velNotEquals",
                "velGreaterThan", "velLessThan", "velLessThanEquals",
                "velGreaterThanEquals", "velIsNull","velIsNotNull","" };
        String[] arrConditionDisp = { "Equal To", "Not Equal To",
                "Greater Than", "Less Than", "Less Than Equal To",
                "Greater Than Equal To", "Is Null","Is Not Null","Select an Option" };

        String condition = "";
        String conditionDisp = "";

        for (int i = 0; i < arrCondition.length; i++) {
            condition = arrCondition[i];
            conditionDisp = arrConditionDisp[i];

            if (condition.equals(selValue)) {
                sbCondition.append("<OPTION value = \"" + condition
                        + "\" SELECTED>" + conditionDisp + "</OPTION>");
            } else {
                sbCondition.append("<OPTION value = \"" + condition + "\">"
                        + conditionDisp + "</OPTION>");
            }
        }

        return sbCondition.toString();

    }

    public Hashtable getFieldActionKeywordInfoForForm(int formId, String keyWord) {
        /* returns a Hastable of Action - Keywords data for every field action */

        StringBuffer sbFldActionInfoSQL = new StringBuffer();

        sbFldActionInfoSQL
                .append(" Select PK_FLDACTION, fld_name, fld_uniqueid from er_fldactioninfo,er_fldaction, er_fldlib ");
        sbFldActionInfoSQL
                .append(" Where er_fldaction.fk_form = ? and FK_FLDACTION  = PK_FLDACTION  and INFO_TYPE = ? and  ");
        sbFldActionInfoSQL
                .append(" pk_field = INFO_VALUE ORDER BY PK_FLDACTION ");

        PreparedStatement pstmtInfo = null;
        Connection conn = null;

        int totalActionInfoRows = 0;

        String fldAction = "";
        String prevAction = "";
        String fldName = "";
        String fldNameStr = "";
        String uniqueId = "";
        String uniqueIdStr = "";
        StringBuffer sbUid = new StringBuffer();

        Hashtable htKeyInfo = new Hashtable();

        try {
            conn = EnvUtil.getConnection();

            pstmtInfo = conn.prepareStatement(sbFldActionInfoSQL.toString());

            pstmtInfo.setInt(1, formId);
            pstmtInfo.setString(2, keyWord);

            ResultSet rs = pstmtInfo.executeQuery();

            Rlog
                    .debug("formlib",
                            "VFieldAction.getFieldActionKeywordTargetField method started");

            while (rs.next()) {
                totalActionInfoRows++;

                fldAction = rs.getString("PK_FLDACTION");
                fldName = rs.getString("fld_name");
                uniqueId = rs.getString("fld_uniqueid");

                if (totalActionInfoRows == 1) {
                    prevAction = fldAction;
                }

                if (!fldAction.equals(prevAction)) {
                    if (fldNameStr.length() > 1) {
                        fldNameStr = fldNameStr.substring(1, fldNameStr
                                .length());
                    }

                    uniqueIdStr = sbUid.toString();

                    if (uniqueIdStr.length() > 1) {
                        uniqueIdStr = uniqueIdStr.substring(1, uniqueIdStr
                                .length());
                    }

                    fldNameStr = fldNameStr
                            + "<BR><BR><B> Field Id(s):</B>&nbsp;"
                            + uniqueIdStr;
                    htKeyInfo.put(prevAction, fldNameStr);

                    fldNameStr = "";
                    sbUid = new StringBuffer();
                    uniqueIdStr = "";
                }

                fldNameStr = fldNameStr + ",[" + fldName + "]";
                sbUid.append(",[" + uniqueId + "]");
                prevAction = fldAction;
            }

            // for the last keyword in the loop
            if (fldNameStr.length() > 1) {
                fldNameStr = fldNameStr.substring(1, fldNameStr.length());

                uniqueIdStr = sbUid.toString();
                if (uniqueIdStr.length() > 1) {
                    uniqueIdStr = uniqueIdStr
                            .substring(1, uniqueIdStr.length());
                }

                fldNameStr = fldNameStr + "<BR><BR><B> Field Id(s):</B>&nbsp;"
                        + uniqueIdStr;
                htKeyInfo.put(prevAction, fldNameStr);
            }

            return htKeyInfo;

        } catch (Exception ex) {
            Rlog.fatal("formlib",
                    "VFieldAction.getFieldActionKeywordTargetField -- > Exception "
                            + ex);
            return htKeyInfo;
        } finally {
            try {
                if (pstmtInfo != null)
                    pstmtInfo.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        // end of method
    }

    public String getSingleFieldActionKeywordField(int fieldAction,
            String fldType) {
        /* returns Target field/Source Field Keyword data the field action */

        StringBuffer sbFldActionInfoSQL = new StringBuffer();

        sbFldActionInfoSQL
                .append(" Select fld_name from er_fldactioninfo, er_fldlib ");
        sbFldActionInfoSQL
                .append(" Where  FK_FLDACTION  = ?  and INFO_TYPE = ? and  ");
        sbFldActionInfoSQL.append(" pk_field = INFO_VALUE ");

        PreparedStatement pstmtInfo = null;
        Connection conn = null;

        String fldAction = "";
        String prevAction = "";
        String fldName = "";
        String fldNameStr = "";

        Hashtable htKeyInfo = new Hashtable();

        try {
            conn = EnvUtil.getConnection();

            pstmtInfo = conn.prepareStatement(sbFldActionInfoSQL.toString());

            pstmtInfo.setInt(1, fieldAction);
            pstmtInfo.setString(2, fldType);

            ResultSet rs = pstmtInfo.executeQuery();

            Rlog
                    .debug("formlib",
                            "VFieldAction.getSingleFieldActionKeywordTargetField method started");

            while (rs.next()) {
                fldName = rs.getString("fld_name");
                fldNameStr = fldNameStr + ",[" + fldName + "]";

            }

            // for the last keyword in the loop
            if (fldNameStr.length() > 1) {
                fldNameStr = fldNameStr.substring(1, fldNameStr.length());
            }

            return fldNameStr;

        } catch (Exception ex) {
            Rlog.fatal("formlib",
                    "VFieldAction.getSingleFieldActionKeywordField -- > Exception "
                            + ex);
            return "-";
        } finally {
            try {
                if (pstmtInfo != null)
                    pstmtInfo.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        // end of method
    }

    /**
     * returns a hashtable for field action scripts
     * 
     * @param formId
     *            form id
     * @return Hashtable with: key: customjs (its value will have form's custom
     *         js) key: activationjs (its value will have form's activation js)
     * 
     */
    /***************************************************************************
     * **************************History Modified by :Sonia Sahni Modified
     * On:06/21/2004 Comment: 1. Added JavaDoc comments 2. Added a check to
     * include a FieldAction script in activationjs depending on its template's
     * activation_flag setting. END*************************
     **************************************************************************/
    public static Hashtable getFormFieldActionScripts(String formId) {
        Hashtable htScripts = new Hashtable();
        Hashtable htData = new Hashtable();
        htData = VFieldAction.getFieldActions(formId);

        StringBuffer sbZeroScr = new StringBuffer();
        StringBuffer sbFieldScr = new StringBuffer();
        StringBuffer sbFormActivationScr = new StringBuffer();
        StringBuffer sbFieldTypeCheck = new StringBuffer();

        StringBuffer sbFinal = new StringBuffer();

        sbFinal
                .append(" var actualField ; if (currentfld.type == undefined) { actualField = currentfld[0]; } else { actualField = currentfld; } ");

         

        for (Enumeration e = htData.keys(); e.hasMoreElements();) {
            String htFieldkey = "";
            htFieldkey = (String) e.nextElement();
            ArrayList arObjs = new ArrayList();
            arObjs = (ArrayList) htData.get(htFieldkey);

             

            StringBuffer sbscr = new StringBuffer();

            for (int i = 0; i < arObjs.size(); i++) {

                VFieldAction vf = new VFieldAction();
                ArrayList arSource = new ArrayList();

                StringBuffer sbBeginBlock = new StringBuffer();
                String srcField = "";
                StringBuffer srcCheckbox = new StringBuffer();

                String scr = "";
                vf = (VFieldAction) arObjs.get(i);

                arSource = (ArrayList) vf.getHtKeywordValues("[VELSOURCE]");

                for (int j = 0; j < arSource.size(); j++) {
                    srcField = (String) arSource.get(j);

                    if (j == 0) {
                        sbBeginBlock.append("if (actualField.name == '"
                                + srcField + "'");
                    } else {
                        sbBeginBlock.append(" || actualField.name == '"
                                + srcField + "' ");

                    }

                }

                if (i == 0 && !(vf.getSourceField()).equals("0")) {
                    sbscr.append(sbBeginBlock.toString()); // for main if block
                    sbscr.append(srcCheckbox.toString()); // for main if block

                    sbscr.append(") { ");
                    sbscr.append("\n");
                }
                // after theloop of source field

                sbscr.append(sbBeginBlock.toString());// for individual field
                                                        // action of a field
                sbscr.append(srcCheckbox.toString()); // for individual field
                                                        // action of a field
                sbscr.append(") { ");
                sbscr.append("\n");

                scr = vf.getActionScript();
                sbscr.append(scr);
                sbscr.append("\n } \n");

                if (vf.getIncludeInFormActivation()) {
                    sbFormActivationScr.append("<script>" + scr + "</script>");
                }

                // if last field action append a return statement and a closing
                // '}' for the field
                if (i == arObjs.size() - 1) {

                    if (!(vf.getSourceField()).equals("0")) {
                        sbscr.append("return ; ");
                        sbscr.append("\n } \n"); // for main loop for field
                        sbFieldScr.append(sbscr.toString());
                    } else {
                        sbZeroScr.append(sbscr.toString());
                    }
                }

            }

        } // for iterating through the hashtable

        // update the form_custom_js

        sbFinal.append(sbZeroScr.toString());
        sbFinal.append(sbFieldScr.toString());

        htScripts.put("customjs", sbFinal.toString());
        htScripts.put("activationjs", sbFormActivationScr.toString());

        return htScripts;
        // end of method
    }

    // end of class

}
