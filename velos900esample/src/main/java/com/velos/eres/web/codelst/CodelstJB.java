/*

 * Classname : CodelstJB.java

 * 

 * Version information

 *

 * Date 03/19/2001

 * 

 * Copyright notice: Aithent

 */

package com.velos.eres.web.codelst;

import java.util.ArrayList;

import com.velos.eres.business.codelst.impl.CodelstBean;
import com.velos.eres.business.common.CodeDao;
import com.velos.eres.service.codelstAgent.CodelstAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * 
 * Client side bean for Codelst
 * 
 * @author Dinesh
 * 
 */

public class CodelstJB {

    /**
     * 
     * the codelst Id
     * 
     */

    private int clstId;

    /**
     * 
     * the codelst account ID
     * 
     */

    private String clstAccId;

    /**
     * 
     * the codelst account type
     * 
     */

    private String clstType;

    /**
     * 
     * the codelst Sub type
     * 
     */

    private String clstSubType;

    /**
     * 
     * the codelst description
     * 
     */

    private String clstDesc;

    /**
     * 
     * the codelst Hide
     * 
     */

    private String clstHide;

    /**
     * 
     * the codelst Seq
     * 
     */

    private String clstSeq;

    /**
     * 
     * the codelst Maintenance
     * 
     */

    private String clstMaint;

    /*
     * 
     * creator
     * 
     */

    private String creator;

    /*
     * 
     * last modified by
     * 
     */

    private String modifiedBy;

    /*
     * 
     * IP Address
     * 
     */

    private String ipAdd;

    // Start of getter setter methods

    public int getClstId() {

        return this.clstId;

    }

    public void setClstId(int clstId) {

        this.clstId = clstId;

    }

    public String getClstAccId() {

        return this.clstAccId;

    }

    public void setClstAccId(String clstAccId) {

        this.clstAccId = clstAccId;

    }

    public String getClstType() {

        return this.clstType;

    }

    public void setClstType(String clstType) {

        this.clstType = clstType;

    }

    public String getClstSubType() {

        return this.clstSubType;

    }

    public void setClstSubType(String clstSubType) {

        this.clstSubType = clstSubType;

    }

    public String getClstDesc() {

        return this.clstDesc;

    }

    public void setClstDesc(String clstDesc) {

        this.clstDesc = clstDesc;

    }

    public String getClstHide() {

        return this.clstHide;

    }

    public void setClstHide(String clstHide) {

        this.clstHide = clstHide;

    }

    public String getClstSeq() {

        return this.clstSeq;

    }

    public void setClstSeq(String clstSeq) {

        this.clstSeq = clstSeq;

    }

    public String getClstMaint() {

        return this.clstMaint;

    }

    public void setClstMaint(String clstMaint) {

        this.clstMaint = clstMaint;

    }

    /**
     * 
     * @return Creator
     * 
     */

    public String getCreator() {

        return this.creator;

    }

    /**
     * 
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     * 
     */

    public void setCreator(String creator) {

        this.creator = creator;

    }

    /**
     * 
     * @return Last Modified By
     * 
     */

    public String getModifiedBy() {

        return this.modifiedBy;

    }

    /**
     * 
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     * 
     */

    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = modifiedBy;

    }

    /**
     * 
     * @return IP Address
     * 
     */

    public String getIpAdd() {

        return this.ipAdd;

    }

    /**
     * 
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     * 
     */

    public void setIpAdd(String ipAdd) {

        this.ipAdd = ipAdd;

    }

    // End of setter getter methods here

    // TILL HERE

    /**
     * 
     * Full arguments constructor.
     * 
     */

    public CodelstJB(int clstId) {

        setClstId(clstId);

    }

    public CodelstJB() {

        Rlog.debug("codelst", "CodelstJB");

    }

    public CodelstJB(int clstId, String clstAccId, String clstType,
            String clstSubType,

            String clstDesc, String clstHide, String clstSeq, String clstMaint,
            String creator,

            String modifiedBy, String ipAdd) {

        setClstId(clstId);

        setClstAccId(clstAccId);

        setClstType(clstType);

        setClstSubType(clstSubType);

        setClstDesc(clstDesc);

        setClstHide(clstHide);

        setClstSeq(clstSeq);

        setClstMaint(clstMaint);

        setCreator(creator);

        setModifiedBy(modifiedBy);

        setIpAdd(ipAdd);

        Rlog.debug("codelst", "CodelstJB.CodelstJB(all parameters)");

    }

    public CodelstBean getCodelstDetails() {

        CodelstBean clsk = null;

        try {

            CodelstAgentRObj codelstAgent = EJBUtil.getCodelstAgentHome();
            clsk = codelstAgent.getCodelstDetails(this.clstId);

        } catch (Exception e) {

            Rlog.debug("codelst", "Error in getCodelstDetails() in CodelstJB "
                    + e);

        }

        if (clsk != null)

        {

            this.clstId = clsk.getClstId();

            this.clstAccId = clsk.getClstAccId();

            this.clstType = clsk.getClstType();

            this.clstSubType = clsk.getClstSubType();

            this.clstDesc = clsk.getClstDesc();

            this.clstHide = clsk.getClstHide();

            this.clstSeq = clsk.getClstSeq();

            this.clstMaint = clsk.getClstMaint();

            this.creator = clsk.getCreator();

            this.modifiedBy = clsk.getModifiedBy();

            this.ipAdd = clsk.getIpAdd();

        }

        return clsk;

    }

   
    
    //Modified by Manimaran on 20,May06 for Role based Study Rights.
    public int setCodelstDetails()

    {
    	int id=0;
        try {

            CodelstAgentRObj codelstAgent = EJBUtil.getCodelstAgentHome();

            id=codelstAgent.setCodelstDetails(this
                    .createCodelstStateKeeper());

        } catch (Exception e) {

            Rlog.fatal("codelst", "Error in setCodelstDetails() in CodelstJB "
                    + e);

        }
        
       
        if (id < 0) {
            return id;
        } else if (id > 0) {

            this.setClstId(id);
           
        }    
        return id;
    }


    public int updateCodelst()

    {

        int output;

        try {

            CodelstAgentRObj codelstAgentRObj = EJBUtil.getCodelstAgentHome();

            output = codelstAgentRObj.updateCodelst(this
                    .createCodelstStateKeeper());

        }

        catch (Exception e) {
            Rlog
                    .fatal("codelst",
                            "EXCEPTION IN SETTING Codelst DETAILS TO  SESSION BEAN"
                                    + e);
            return -2;

        }

        return output;

    }

    public CodelstBean createCodelstStateKeeper()

    {

        return new CodelstBean(clstId, clstAccId, clstType, clstSubType,
                clstDesc, clstHide, clstSeq, clstMaint, creator, modifiedBy,
                ipAdd);

    }

    /**
     * 
     * Calls getCodelstData(int , String ) of Codelst Session Bean:
     * CodelstAgentBean
     * 
     * 
     * 
     * @param accId
     * 
     * @param codelstType
     * 
     * @See CodeDao
     * 
     */

    public CodeDao getCodelstData(int accId, String codelstType) {

        CodeDao codeDao = new CodeDao();

        try {

            Rlog.debug("codelst", "CodelstJB.getCodelstData starting");

            CodelstAgentRObj codelstAgentRObj = EJBUtil.getCodelstAgentHome();

            Rlog.debug("codelst", "CodelstJB.getCodelstData after remote");

            codeDao = codelstAgentRObj.getCodelstData(accId, codelstType);

            if (codelstAgentRObj.getCodelstData(accId, codelstType) instanceof CodeDao)

            {

                Rlog.debug("codelst", "Type is CodeDao");

            } else

            {

                Rlog.debug("codelst", "Type is something else");

            }

            Rlog.debug("codelst", "CodelstJB.getCodelstData after Dao");

        } catch (Exception e) {

            Rlog.fatal("codelst", "Error in getCodelstData in CodelstJB " + e);

        }

        return codeDao;

    }

    /**
     * 
     * Calls getCodeDescription(int ) of Codelst Session Bean: CodelstAgentBean
     * 
     * 
     * 
     * @param codeId
     * 
     * @See CodeDao
     * 
     */

    public String getCodeCustomCol(int codeId) {

        Rlog.debug("codelst", "This is here");

        String codeCustomCol = "";

        try {

            Rlog.debug("codelst", "CodelstJB.getCodeCustomCol starting");

            CodelstAgentRObj codelstAgentRObj = EJBUtil.getCodelstAgentHome();

            codeCustomCol = codelstAgentRObj.getCodeCustomCol(codeId);

            Rlog.debug("codelst", "CodelstJB.getCodeCustomCol after Dao");

        } catch (Exception e) {

            Rlog.fatal("codelst", "Error in getCodeCustomCol in CodelstJB "
                    + e);

        }

        return codeCustomCol;

    }

    /**
     * 
     * Calls getCodeDescription(int ) of Codelst Session Bean: CodelstAgentBean
     * 
     * 
     * 
     * @param codeId
     * 
     * @See CodeDao
     * 
     */

    public String getCodeDescription(int codeId) {

        Rlog.debug("codelst", "This is here");

        String codeDesc = "";

        try {

            Rlog.debug("codelst", "CodelstJB.getCodeDescription starting");

            CodelstAgentRObj codelstAgentRObj = EJBUtil.getCodelstAgentHome();

            codeDesc = codelstAgentRObj.getCodeDescription(codeId);

            Rlog.debug("codelst", "CodelstJB.getCodeDescription after Dao");

        } catch (Exception e) {

            Rlog.fatal("codelst", "Error in getCodeDescription in CodelstJB "
                    + e);

        }

        return codeDesc;

    }

    
    
    
    
    
    
    
    
    
    
    
    /**
     * 
     * Calls getRolesForEvent(int ) of Codelst Session Bean: CodelstAgentBean
     * 
     * 
     * 
     * @param eventId
     * 
     * @See CodeDao
     * 
     */

    public CodeDao getRolesForEvent(int eventId) {

        Rlog.debug("codelst", "This is here");

        CodeDao codeDao = new CodeDao();

        try {

            Rlog.debug("codelst", "CodelstJB.getRolesForEvent starting");

            CodelstAgentRObj codelstAgentRObj = EJBUtil.getCodelstAgentHome();

            Rlog.debug("codelst", "CodelstJB.getRolesForEvent after remote");

            codeDao = codelstAgentRObj.getRolesForEvent(eventId);

            Rlog.debug("codelst", "CodelstJB.getRolesForEvent after Dao");

        } catch (Exception e) {

            Rlog
                    .fatal("codelst", "Error in getRolesForEvent in CodelstJB "
                            + e);

        }

        return codeDao;

    }

    /**
     * 
     * Calls getDescForEventUsers(ArrayList , ArrayList ) of Codelst Session
     * Bean: CodelstAgentBean
     * 
     * 
     * 
     * @param eventUserIds
     *            User Ids or codelst Ids
     * 
     * @param userTypes
     *            User Types U for User, R for role and S for mail users
     * 
     * @See CodeDao
     * 
     */

    public CodeDao getDescForEventUsers(ArrayList eventUserIds,
            ArrayList userTypes) {

        Rlog.debug("codelst", "This is here");

        CodeDao codeDao = new CodeDao();

        try {

            Rlog.debug("codelst", "CodelstJB.getDescForEventUsers starting");

            CodelstAgentRObj codelstAgentRObj = EJBUtil.getCodelstAgentHome();

            codeDao = codelstAgentRObj.getDescForEventUsers(eventUserIds,
                    userTypes);

            Rlog.debug("codelst", "CodelstJB.getDescForEventUsers after Dao");

        } catch (Exception e) {

            Rlog.fatal("codelst", "Error in getDescForEventUsers in CodelstJB "
                    + e);

        }

        return codeDao;

    }

    /**
     * Gets the CodeDao Object
     * 
     * @return empty CodeDao Object
     */
    public CodeDao getCodeDaoInstance() {
        try {
            Rlog.debug("codelst", "coodelstJB.getCodeDaoInstance() starting");
            CodelstAgentRObj codelstAgentRObj = EJBUtil.getCodelstAgentHome();
            return codelstAgentRObj.getCodeDaoInstance();
        } catch (Exception e) {
            Rlog.fatal("codelst", "Error in codelstJB:getCodeDAoInstance " + e);
            return null;
        }

    }

    /*
     * 
     * Sonia Sahni
     * 
     * Gets all studies for the codelst (the codelst belongs to the study team)
     * 
     * returns HTML for study titles
     * 
     * the argument spage takes the name of the jsp page that should be opened
     * when the codelst clicks on the title
     * 
     */

    /*
     * 
     * generates a String of XML for the codelst details entry form.<br><br>
     * 
     */

    public String toXML() {

        return new String(
                "<?xml version=\"1.0\"?>"
                        +

                        "<?cocoon-process type=\"xslt\"?>"
                        +

                        "<?xml-stylesheet href=\"/stylesheet/form-html.xsl\" type=\"text/xsl\"?>"
                        +

                        "<?xml-stylesheet href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>"
                        +

                        // to be done "<form action=\"codelstsummary.jsp\">" +

                        "<head>" +

                        // to be done "<title>Codelst Summary </title>" +

                        "</head>" +

                        "<input type=\"hidden\" name=\"clstId\" value=\""
                        + this.getClstId() + "\" size=\"10\"/>" +

                        "<input type=\"text\" name=\"clstAccId\" value=\""
                        + this.getClstAccId() + "\" size=\"10\"/>" +

                        "<input type=\"text\" name=\"clstType\" value=\""
                        + this.getClstType() + "\" size=\"15\"/>" +

                        "<input type=\"text\" name=\"clstSubType\" value=\""
                        + this.getClstSubType() + "\" size=\"15\"/>" +

                        "<input type=\"text\" name=\"clstDesc\" value=\""
                        + this.getClstDesc() + "\" size=\"200\"/>" +

                        "<input type=\"text\" name=\"ClstHide\" value=\""
                        + this.getClstHide() + "\" size=\"1\"/>" +

                        "<input type=\"text\" name=\"clstSeq\" value=\""
                        + this.getClstSeq() + "\" size=\"3\"/>" +

                        "<input type=\"text\" name=\"clstMaint\" value=\""
                        + this.getClstMaint() + "\" size=\"1\"/>" +

                        "</form>"

        );

    }// end of method

    /**
     * Gets the codeId attribute of the CodeDao object
     * 
     * @param type
     *            codelist type
     * @param subType
     *            codelst subtype
     * @return The codeId value
     */
    public int getCodeId(String type, String subType)
    {

        int codePK = 0;

        try {

            
            CodelstAgentRObj codelstAgentRObj = EJBUtil.getCodelstAgentHome();

            codePK = codelstAgentRObj.getCodeId(type,subType);
            

        } catch (Exception e) {

            Rlog.fatal("codelst", "Error in getCodeId in CodelstJB "
                    + e);

        }

        return codePK;

    }
}// end of class
