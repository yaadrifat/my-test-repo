/*
 * Classname : StudyRightsJB.java
 * 
 * Version information
 *
 * Date 03/15/2001
 * 
 * Copyright notice: Aithent Technologies 
 */

package com.velos.eres.web.studyRights;

import java.util.ArrayList;

import com.velos.eres.business.common.CtrlDao;
import com.velos.eres.business.studyRights.impl.StudyRightsBean;
import com.velos.eres.service.studyRightsAgent.StudyRightsAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * Client side bean for the Study Rights
 * 
 * @author Sonia Sahni
 */

public class StudyRightsJB {

    /**
     * the Study Rights id
     */
    private int id;

    /**
     * Array List of Feature Sequence
     */
    private ArrayList grSeq;

    /**
     * the Array List of Feature Rights
     */
    private ArrayList ftrRights;

    /**
     * the Array List containing reference name of the feature
     */
    private ArrayList grValue;

    /**
     * the Array List containing user friendly name of the feature
     */
    private ArrayList grDesc;

    private String studyTeamId;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;
    
    private String superRightsStringForStudy;

    // GETTER SETTER METHODS

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList getGrSeq() {
        return this.grSeq;
    }

    public void setGrSeq(String seq) {
        grSeq.add(seq);
    }

    public void setGrSeq(ArrayList seq) {
        this.grSeq = seq;
    }

    public ArrayList getFtrRights() {
        return this.ftrRights;
    }

    public String getFtrRightsBySeq(String seq) {
        // int count = Integer.parseInt(seq) - 1;
        
    	if (this.ftrRights != null && this.ftrRights.size() > 0)
    	{
    		return this.ftrRights.get(Integer.parseInt(seq) - 1).toString();
    	}
    	else
    	{
    		return "0";
    	}
    }

    public String getFtrRightsByValue(String val) {
    	if (grValue != null && grValue.size()> 0 )
    	{
    		int seq = grValue.indexOf(val);
    		return this.ftrRights.get(seq).toString();
    	}else
    	{
    		return "0";
    	}
    }

    public void setFtrRights(String rights) {
        ftrRights.add(rights);
    }

    public void setFtrRights(ArrayList rights) {
        this.ftrRights = rights;
    }

    public ArrayList getGrValue() {
        return this.grValue;
    }

    public String getGrValueBySeq(String seq) {
        int count = Integer.parseInt(seq) - 1;
        return this.grValue.get(count).toString();
    }

    public void setGrValue(String val) {
        grValue.add(val);
    }

    public void setGrValue(ArrayList val) {
        this.grValue = val;
    }

    public ArrayList getGrDesc() {
        return this.grDesc;
    }

    public String getGrDescBySeq(String seq) {
        int count = Integer.parseInt(seq) - 1;
        return this.grDesc.get(count).toString();
    }

    public String getGrDescByValue(String val) {
        int seq = grValue.indexOf(val);
        return this.grDesc.get(seq).toString();
    }

    public void setGrDesc(String desc) {
        grDesc.add(desc);
    }

    public void setGrDesc(ArrayList desc) {
        this.grDesc = desc;
    }

    public String getStudyTeamId() {
        return this.studyTeamId;
    }

    public void setStudyTeamId(String studyTeamId) {
        this.studyTeamId = studyTeamId;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS
    public StudyRightsJB(int id) {
        setId(id);
    }

    public StudyRightsJB() {
        grSeq = new ArrayList();
        ftrRights = new ArrayList();
        grValue = new ArrayList();
        grDesc = new ArrayList();
    }

    /**
     * Full arguments constructor.
     * 
     * @param
     */
    public void StudyRightsJB(int id, ArrayList grSeq, ArrayList ftrRights,
            ArrayList grValue, ArrayList grDesc, String studyTeamId,
            String creator, String modifiedBy, String ipAdd) {
        Rlog.debug("studyrights", "StudyRightsJB Start constructor");
        setId(id);
        setGrSeq(grSeq);
        setFtrRights(ftrRights);
        setGrValue(grValue);
        setGrDesc(grDesc);
        setStudyTeamId(studyTeamId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);

    }

    /**
     * calls getGrpRightsDetails() on the GrpRightsAgent facade and extracts
     * this state holder into the been attribute fields.
     */
    public StudyRightsBean getStudyRightsDetails() {
        StudyRightsBean srk = null;
        try {
            StudyRightsAgentRObj studyRightsAgentRObj = EJBUtil
                    .getStudyRightsAgentHome();
            srk = studyRightsAgentRObj.getStudyRightsDetails(this.id);
        } catch (Exception e) {
            Rlog.fatal("studyrights", "Exception in getGrpRightsDetails" + e);

        }
        if (srk != null) {

            this.id = srk.getId();
            this.grSeq = srk.getGrSeq();
            this.ftrRights = srk.getFtrRights();
            this.grValue = srk.getGrValue();
            this.grDesc = srk.getGrDesc();
            this.studyTeamId = srk.getStudyTeamId();
            this.creator = srk.getCreator();
            this.modifiedBy = srk.getModifiedBy();
            this.ipAdd = srk.getIpAdd();
        }
        return srk;
    }
    
    
    /** method created to populate super user study rights in the studyrightsJB. 
     * to preserve how study rights are maintained. this will also replace the use of getStudyRightsDetails()
     * since we already have rights when we get the study team pk, we dont have to load the studyrights bean again from the database
     * */
    
    public StudyRightsBean loadStudyRights()
    {
        StudyRightsBean srk = new StudyRightsBean();
        String studyRight = "";
        int sLength = 0, counter = 0;
        CtrlDao cntrl = new CtrlDao() ;
        ArrayList cVal, cDesc, cSeq;
        cVal = new ArrayList();
        cVal = new ArrayList();
        cSeq = new ArrayList();
        
        
        studyRight = getSuperRightsStringForStudy();
        
    	sLength = studyRight.length();
    	 
          
         for (counter = 0; counter <= (sLength - 1); counter++) {
        	 srk.setFtrRights(String.valueOf(studyRight.charAt(counter)));
        	 srk.setGrSeq(Integer.toString(counter));
         }

         cntrl.getControlValues("study_rights");
         cVal = cntrl.getCValue();
         cDesc = cntrl.getCDesc();
         srk.setGrValue(cVal);
         srk.setGrDesc(cDesc);
         
         this.grSeq = srk.getGrSeq();
         this.ftrRights = srk.getFtrRights();
         this.grValue = srk.getGrValue();
         this.grDesc = srk.getGrDesc();
         this.studyTeamId = "0" ;
          
    	return srk;
    }
    

    /**
     * calls setGrpRightsDetails() on the GrpRightsAgent facade.
     */
    /*
     * public int setStudyRightsDetails() { try { int result = 0;
     * 
     * StudyRightsAgentRObj studyRightsAgentRObj =
     * EJBUtil.getStudyRightsAgentHome(); result =
     * studyRightsAgentRObj.setStudyRightsDetails(this
     * .createStudyRightsStateKeeper()); return result; } catch (Exception e) {
     * Rlog.fatal("studyrights", "Exception in setStudyRightsDetails" + e);
     * return -2; // In case of Exception } }
     */

    /**
     * calls updateStudyRights() on the GrpRightsAgent facade.
     */
    public int updateStudyRights() {
        try {
            int result = 0;
            Rlog.debug("studyrights", "update study rights");
            StudyRightsAgentRObj studyRightsAgentRObj = EJBUtil
                    .getStudyRightsAgentHome();
            result = studyRightsAgentRObj.updateStudyRights(this
                    .createStudyRightsStateKeeper());
            return result;
        } catch (Exception e) {
            Rlog.fatal("studyrights", "EXCEPTION IN UPDATE STUDY RIGHTS");
            return -2; // In case of Exception
        }
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * calls getGrpSupUserRightsDetails() on the GrpRightsAgent facade and
     * extracts this state holder into the been attribute fields.
     */
    public StudyRightsBean getGrpSupUserStudyRightsDetails() {
        StudyRightsBean srk = null;
        try {
            StudyRightsAgentRObj studyRightsAgentRObj = EJBUtil
                    .getStudyRightsAgentHome();
            srk = studyRightsAgentRObj.getGrpSupUserStudyRightsDetails(this.id);
        } catch (Exception e) {
            Rlog.fatal("studyrights",
                    "EXCEPTION IN setGrpSupUserStudyRightsDetails STUDY RIGHTS "
                            + e);
        }
        if (srk != null) {
            this.id = srk.getId();
            this.grSeq = srk.getGrSeq();
            this.ftrRights = srk.getFtrRights();
            this.grValue = srk.getGrValue();
            this.grDesc = srk.getGrDesc();
            this.studyTeamId = srk.getStudyTeamId();
            this.creator = srk.getCreator();
            this.modifiedBy = srk.getModifiedBy();
            this.ipAdd = srk.getIpAdd();
        }
        return srk;
    }

    /**
     * calls setGrpRightsDetails() on the GrpRightsAgent facade.
     */
    /*
     * public int setGrpSupUserStudyRightsDetails() { try { int result = 0;
     * 
     * StudyRightsAgentRObj studyRightsAgentRObj = EJBUtil
     * .getStudyRightsAgentHome(); result =
     * studyRightsAgentRObj.setGrpSupUserStudyRightsDetails(this
     * .createStudyRightsStateKeeper()); return result; } catch (Exception e) {
     * Rlog.fatal("studyrights", "EXCEPTION IN setGrpSupUserStudyRightsDetails
     * STUDY RIGHTS " + e); return -2; // In case of Exception } }
     */

    /**
     * calls updateStudyRights() on the GrpRightsAgent facade.
     */
    public int updateGrpSupUserStudyRights() {
        try {
            int result = 0;
            Rlog.debug("studyrights", "update study rights");
            StudyRightsAgentRObj studyRightsAgentRObj = EJBUtil
                    .getStudyRightsAgentHome();
            result = studyRightsAgentRObj.updateGrpSupUserStudyRights(this
                    .createStudyRightsStateKeeper());
            return result;
        } catch (Exception e) {
            Rlog.fatal("studyrights", "EXCEPTION IN UPDATE STUDY RIGHTS " + e);
            return -2; // In case of Exception
        }
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
     * places bean attributes into a Group Rights StateHolder.
     */
    public StudyRightsBean createStudyRightsStateKeeper() {

        return new StudyRightsBean(id, (new Integer(id)).toString(), grSeq,
                ftrRights, grValue, grDesc, creator, modifiedBy, ipAdd);
    }

	public String getSuperRightsStringForStudy() {
		return superRightsStringForStudy;
	}

	public void setSuperRightsStringForStudy(String superRightsStringForStudy) {
		this.superRightsStringForStudy = superRightsStringForStudy;
	}

    /*
     * generates a String of XML for the Group Rights details entry form.<br><br>
     * Note that it is planned to encapsulate this detail in another object.
     */
    /*
     * public String toXML() {
     * 
     * return new String( "<?xml version=\"1.0\"?>" + "<?cocoon-process
     * type=\"xslt\"?>" + "<?xml-stylesheet href=\"/stylesheet/form-html.xsl\"
     * type=\"text/xsl\"?>" + "<?xml-stylesheet
     * href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>" + "<form
     * action=\"UpdateTeam.jsp\">" + "<head>" + "<title>Team Details </title>" + "</head>" + "<input
     * type=\"hidden\" name=\"id\" value=\"" + this.getId() + "\" size=\"10\"/>" + "<input
     * type=\"hidden \" name=\"Study\" value=\"" + this.getStudyId() + "\"
     * size=\"10\"/>" + "<input type=\"text\" name=\" User\" value=\"" +
     * this.getTeamUser() + "\" size=\"12\"/>" + "<input type=\"text\" name=\"
     * Role\" value=\"" + this.getTeamUserRole ()+ "\" size=\"30\"/>" + "</form>" );
     * }//end of method
     */

}// end of class

