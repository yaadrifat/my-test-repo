/*
 * Classname : StudyIdJB
 * 
 * Version information: 1.0
 *
 * Date: 09/23/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Sahni
 */

package com.velos.eres.web.studyId;

/* IMPORT STATEMENTS */

import com.velos.eres.business.common.StudyIdDao;
import com.velos.eres.business.studyId.impl.StudyIdBean;
import com.velos.eres.service.studyIdAgent.StudyIdAgentRObj;
import com.velos.eres.service.studyIdAgent.impl.StudyIdAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side bean for StudyId
 * 
 * @author Sonia Sahni
 * @version 1.0 09/23/2003
 */

public class StudyIdJB {

    /**
     * The primary key:pk_studyid
     */

    private int id;

    /**
     * The foreign key reference to er_study
     */
    private String studyId;

    /**
     * The Id Type
     */

    private String studyIdType;

    /**
     * The Alternate Study Id
     */
    private String alternateStudyId;

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    // /////////////////////////////////////////////////////////////////////////////////

    // GETTER AND SETTER METHODS

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setStudyId(String studyId) {
        this.studyId = studyId;
    }

    public String getStudyId() {
        return studyId;
    }

    public void setStudyIdType(String studyIdType) {
        this.studyIdType = studyIdType;
    }

    public String getStudyIdType() {
        return studyIdType;
    }

    public void setAlternateStudyId(String alternateStudyId) {
        this.alternateStudyId = alternateStudyId;
    }

    public String getAlternateStudyId() {
        return alternateStudyId;
    }

    /**
     * @return The Creator of the Form Section
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * 
     * @param to
     *            set the Creator of the Form Section
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the IP Address of the Form Section request
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Section request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // //////////////////////////////////////////////////////////////////

    // END OF THE GETTER AND SETTER METHODS

    /**
     * Constructor
     * 
     * @param studyIdId:
     *            Unique Id constructor
     * 
     */

    public StudyIdJB(int id) {
        setId(id);
    }

    /**
     * Default Constructor
     */

    public StudyIdJB() {
        Rlog.debug("studyId", "StudyIdJB.StudyIdJB() ");
    }

    public StudyIdJB(int id, String studyId, String studyIdType,
            String alternateStudyId, String creator, String modifiedBy,
            String ipAdd) {
        setId(id);
        setStudyId(studyId);
        setStudyIdType(studyIdType);
        setAlternateStudyId(alternateStudyId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

    /**
     * Calls getStudyIdDetails of StudyId Session Bean: StudyIdAgentBean
     * 
     * @return StudyIdStatKeeper
     * @see StudyIdAgentBean
     */

    public StudyIdBean getStudyIdDetails() {
        StudyIdBean studyIdsk = null;
        try {
            StudyIdAgentRObj studyIdAgentRObj = EJBUtil.getStudyIdAgentHome();
            studyIdsk = studyIdAgentRObj.getStudyIdDetails(this.id);
            Rlog.debug("studyId",
                    "StudyIdJB.getStudyIdDetails() StudyIdStateKeeper "
                            + studyIdsk);
        } catch (Exception e) {
            Rlog.fatal("studyId", "Error in StudyId getStudyIdDetails" + e);
        }

        if (studyIdsk != null) {
            setId(studyIdsk.getId());
            setStudyId(studyIdsk.getStudyId());
            setStudyIdType(studyIdsk.getStudyIdType());
            setAlternateStudyId(studyIdsk.getAlternateStudyId());
            setCreator(studyIdsk.getCreator());
            setModifiedBy(studyIdsk.getModifiedBy());
            setIpAdd(studyIdsk.getIpAdd());

        }

        return studyIdsk;

    }

    /**
     * Calls setStudyIdDetails() of Study Id Session Bean: StudyIdAgentBean
     * 
     */

    public int setStudyIdDetails() {
        int toReturn;
        try {

            StudyIdAgentRObj studyIdAgentRObj = EJBUtil.getStudyIdAgentHome();

            StudyIdBean tempStateKeeper = new StudyIdBean();

            tempStateKeeper = this.createStudyIdStateKeeper();

            toReturn = studyIdAgentRObj.setStudyIdDetails(tempStateKeeper);
            this.setId(toReturn);

            Rlog.debug("studyId", "StudyIdJB.setStudyIdDetails()");

            return toReturn;

        } catch (Exception e) {
            Rlog.fatal("studyId", "Error in setStudyIdDetails() in StudyIdJB "
                    + e);
            return -2;
        }
    }

    /**
     * 
     * @return a statekeeper object for the StudyId Record with the current
     *         values of the bean
     */
    public StudyIdBean createStudyIdStateKeeper() {

        return new StudyIdBean(id, studyId, studyIdType, alternateStudyId,
                creator, modifiedBy, ipAdd);

    }

    /**
     * Calls updateStudyId() of StudyId Session Bean: StudyIdAgentBean
     * 
     * @return The status as an integer
     */
    public int updateStudyId() {
        int output;
        try {
            StudyIdAgentRObj studyIdAgentRObj = EJBUtil.getStudyIdAgentHome();
            output = studyIdAgentRObj.updateStudyId(this
                    .createStudyIdStateKeeper());
        } catch (Exception e) {
            Rlog
                    .debug("studyId",
                            "EXCEPTION IN SETTING studyId DETAILS TO  SESSION BEAN"
                                    + e);
            return -2;
        }
        return output;
    }

    public int removeStudyId() {

        int output;

        try {

            StudyIdAgentRObj studyIdAgentRObj = EJBUtil.getStudyIdAgentHome();
            output = studyIdAgentRObj.removeStudyId(this.id);
            return output;

        } catch (Exception e) {
            Rlog.fatal("studyId", "in StudyIdJB -removeStudyId() method" + e);
            return -1;
        }

    }

    /**
     * Method to add the multiple study Ids
     * 
     * @param StudyIdStateKeeper
     * 
     */

    public int createMultipleStudyIds(StudyIdBean ssk ,String defuserGroup) {

        int ret = 0;

        try {
            Rlog.debug("studyId", "StudyIdJB.createMultipleStudyIds ");
            StudyIdAgentRObj studyIdAgentRObj = EJBUtil.getStudyIdAgentHome();
            ret = studyIdAgentRObj.createMultipleStudyIds(ssk,defuserGroup);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("studyId", "createMultipleStudyIds in StudyIdJB " + e);
            return -2;
        }

    }

    /**
     * Method to update multiple study Ids
     * 
     * @param StudyIdStateKeeper
     * 
     */

    public int updateMultipleStudyIds(StudyIdBean ssk) {

        int ret = 0;

        try {
            Rlog.debug("studyId", "StudyIdJB.updateMultipleStudyIds");
            StudyIdAgentRObj studyIdAgentRObj = EJBUtil.getStudyIdAgentHome();
            ret = studyIdAgentRObj.updateMultipleStudyIds(ssk);

            return ret;
        } catch (Exception e) {
            Rlog.fatal("studyId", "updateMultipleStudyIds() in StudyIdJB " + e);
            return -2;
        }

    }

    public StudyIdDao getStudyIds(int studyId,String defUserGroup) {

        StudyIdDao sd = new StudyIdDao();

        try {
            Rlog.debug("studyId", "StudyIdJB.updateMultipleStudyIds");
            StudyIdAgentRObj studyIdAgentRObj = EJBUtil.getStudyIdAgentHome();

            sd = studyIdAgentRObj.getStudyIds(studyId,defUserGroup);

            return sd;
        } catch (Exception e) {
            Rlog.fatal("studyId", "getStudyIds() in StudyIdJB " + e);
            return null;
        }

    }

}
