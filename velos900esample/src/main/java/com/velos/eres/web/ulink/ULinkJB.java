/*

 * Classname : ULinkJB.java

 * 

 * Version information

 *
 * Date 03/15/2001

 * 

 * Copyright notice: Aithent Technologies 
 */

package com.velos.eres.web.ulink;

import java.util.Hashtable;
import com.velos.eres.business.common.UsrLinkDao;
import com.velos.eres.business.ulink.impl.ULinkBean;
import com.velos.eres.service.ulinkAgent.ULinkAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/**
 * 
 * 
 * 
 * 
 * 
 * Client side bean for the User Links
 * 
 * 
 * 
 * 
 * 
 * @author Sonia Sahni
 * 
 * 
 * 
 * 
 * 
 */

public class ULinkJB {

    /***************************************************************************
     * * * * * the Link Id * * * *
     */

    private int lnkId;

    /***************************************************************************
     * * * * * the User id * * * *
     */

    private String lnkUserId;

    /***************************************************************************
     * * * * * the Account ID * * * *
     */

    private String lnkAccId;

    /***************************************************************************
     * * * * * the Link URI * * * *
     */

    private String lnkURI;

    /***************************************************************************
     * * * * * the Link Description * * * *
     */

    private String lnkDesc;

    /***************************************************************************
     * * * * * the Link Group Name, so that links can be put under user defined
     * headings * * * *
     */

    private String lnkGrpName;

    /* * * creator * */

    private String creator;

    /* * * last modified by * */

    private String modifiedBy;

    /* * * IP Address * */

    private String ipAdd;

    private String lnkType;

    // END OF MEMBER DECLARATION

    // GETTER SETTER METHODS

    public int getLnkId()

    {

        return this.lnkId;

    }

    public void setLnkId(int lnkId)

    {

        this.lnkId = lnkId;

    }

    public String getLnkAccId()

    {

        return this.lnkAccId;

    }

    public void setLnkAccId(String lnkAccId)

    {

        this.lnkAccId = lnkAccId;

    }

    public String getLnkUserId()

    {

        return this.lnkUserId;

    }

    public void setLnkUserId(String lnkUserId)

    {

        this.lnkUserId = lnkUserId;

    }

    public String getLnkDesc()

    {

        return this.lnkDesc;

    }

    public void setLnkDesc(String lnkDesc)

    {

        this.lnkDesc = lnkDesc;

    }

    public String getLnkGrpName()

    {

        return this.lnkGrpName;

    }

    public void setLnkGrpName(String lnkGrpName)

    {

        this.lnkGrpName = lnkGrpName;

    }

    public String getLnkURI()

    {

        return this.lnkURI;

    }

    public void setLnkURI(String lnkURI)

    {

        this.lnkURI = lnkURI;

    }

    /**
     * 
     * @return Creator
     * 
     */

    public String getCreator() {

        return this.creator;

    }

    /**
     * 
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     * 
     */

    public void setCreator(String creator) {

        this.creator = creator;

    }

    /**
     * 
     * @return Last Modified By
     * 
     */

    public String getModifiedBy() {

        return this.modifiedBy;

    }

    /**
     * 
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     * 
     */

    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = modifiedBy;

    }

    /**
     * 
     * @return IP Address
     * 
     */

    public String getIpAdd() {

        return this.ipAdd;

    }

    /**
     * 
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     * 
     */

    public void setIpAdd(String ipAdd) {

        this.ipAdd = ipAdd;

    }

    public String getLnkType() {
        return this.lnkType;
    }

    public void setLnkType(String lnkType) {
        this.lnkType = lnkType;
    }

    // END OF GETTER SETTER METHODS

    public ULinkJB(int lnkId) {

        setLnkId(lnkId);

    }

    // Default Contructor

    public ULinkJB() {
    }

    /**
     * 
     * 
     * 
     * 
     * 
     * Full arguments constructor.
     * 
     * 
     * 
     * 
     * 
     * @param
     * 
     * 
     * 
     * 
     * 
     */

    public ULinkJB(int lnkId, String lnkAccId, String lnkUserId, String lnkURI,
            String lnkDesc, String lnkGrpName, String creator,

            String modifiedBy, String ipAdd, String lnkType) {

        Rlog.debug("ulink", "Begin User links ClientJB Constructor " + lnkId);

        setLnkId(lnkId);

        setLnkAccId(lnkAccId);

        setLnkDesc(lnkDesc);

        setLnkGrpName(lnkGrpName);

        setLnkURI(lnkURI);

        setLnkUserId(lnkUserId);

        setCreator(creator);

        setModifiedBy(modifiedBy);

        setIpAdd(ipAdd);

        setLnkType(lnkType);

        Rlog.debug("ulink", "User links ClientJB Constructor ");

    }

    /**
     * 
     * 
     * 
     * 
     * 
     * calls getULinkDetails() on the UlinkAgent facade and extracts this state
     * holder into the been attribute fields.
     * 
     * 
     * 
     * 
     * 
     */

    public ULinkBean getULinkDetails() {

        ULinkBean lsk = null;

        try {

            ULinkAgentRObj ulinkAgentRObj = EJBUtil.getULinkAgentHome();
            lsk = ulinkAgentRObj.getULinkDetails(this.lnkId);

        }

        catch (Exception e)

        {

            Rlog.fatal("ulink",
                    "EXCEPTION IN GETTING USERLINK DETAILS FROM SESSION BEAN"
                            + e);

        }

        if (lsk != null) {

            setLnkId(lsk.getLnkId());
            setLnkAccId(lsk.getLnkAccId());

            setLnkUserId(lsk.getLnkUserId());
            setLnkDesc(lsk.getLnkDesc());

            setLnkGrpName(lsk.getLnkGrpName());

            setLnkURI(lsk.getLnkURI());

            setCreator(lsk.getCreator());

            setModifiedBy(lsk.getModifiedBy());

            setIpAdd(lsk.getIpAdd());

            setLnkType(lsk.getLnkType());

        }

        return lsk;

    }

    /***************************************************************************
     * * * * * calls setCustomer() on the ReservationAgent facade. * * * *
     */

    public void setULinkDetails() {
        try {
            ULinkAgentRObj ulinkAgentRObj = EJBUtil.getULinkAgentHome();
            ulinkAgentRObj.setULinkDetails(this.createULinkStateKeeper());

        } catch (Exception e) {

            Rlog.fatal("ulink",
                    "EXCEPTION IN SETTING USERLINK DETAILS TO  SESSION BEAN"
                            + e);

        }

    }

    public int updateUlink()

    {
        int output;
        try {

            ULinkAgentRObj ulinkAgentRObj = EJBUtil.getULinkAgentHome();
            output = ulinkAgentRObj.updateULink(this.createULinkStateKeeper());

        }

        catch (Exception e) {

            Rlog.fatal("ulink",
                    "EXCEPTION IN SETTING USERLINK DETAILS TO  SESSION BEAN"
                            + e);
            return -2;

        }

        return output;

    }

    /*
     * 
     * 
     * 
     * 
     * 
     * places bean attributes into a StudyStateHolder.
     * 
     * 
     * 
     * 
     * 
     */

    public ULinkBean createULinkStateKeeper()

    {

        return new ULinkBean(lnkId, lnkUserId, lnkAccId, lnkURI, lnkDesc,
                lnkGrpName, creator, modifiedBy, ipAdd, lnkType);

    }

    public UsrLinkDao getULinkValuesByUserId(int userId) {

        UsrLinkDao usrLinkDao = new UsrLinkDao();

        try {

            Rlog.debug("ulink", "ULinkJB.getULinkValuesByUserId starting");

            ULinkAgentRObj uLinkAgentRObj = EJBUtil.getULinkAgentHome();

            Rlog.debug("ulink", "ULinkJB.getULinkValuesByUserId home");

            usrLinkDao = uLinkAgentRObj.getULinkValuesByUserId(userId);

            Rlog.debug("ulink", "ULinkJB.getULinkValuesByUserId after Dao");

            return usrLinkDao;

        } catch (Exception e) {

            Rlog.fatal("ulink",
                    "Error in getULinkValuesByUserId(int userId) in ULinkJB "
                            + e);

        }

        return usrLinkDao;

    }

    public UsrLinkDao getULinkValuesByAccountId(int accountId, String accType) {

        UsrLinkDao usrLinkDao = new UsrLinkDao();

        try {

            Rlog.debug("ulink", "ULinkJB.getULinkValuesByAccountId starting");

            ULinkAgentRObj uLinkAgentRObj = EJBUtil.getULinkAgentHome();

            Rlog.debug("ulink", "ULinkJB.getULinkValuesByAccountId remote");

            usrLinkDao = uLinkAgentRObj.getULinkValuesByAccountId(accountId,
                    accType);

            Rlog.debug("ulink", "ULinkJB.getULinkValuesByAccountId after Dao");

            return usrLinkDao;

        } catch (Exception e) {

            Rlog.fatal("ulink",
                    "Error in getULinkValuesByAccountId(int accountId) in ULinkJB "
                            + e);

        }

        return usrLinkDao;

    }

    // to delete links

    public int deleteUlink()

    {

        int output;

        try {

            ULinkAgentRObj uLinkAgentRObj = EJBUtil.getULinkAgentHome();

            output = uLinkAgentRObj.deleteULink(this.createULinkStateKeeper());

        }

        catch (Exception e) {

            Rlog.fatal("ulink", "EXCEPTION IN deleting u link " + e);
            return -2;

        }

        return output;

    }
   
    // Overloaded for INF-18183 -- AGodara
    public int deleteUlink(Hashtable<String, String> userInfo){
    	
    	int output;
        try {
            ULinkAgentRObj uLinkAgentRObj = EJBUtil.getULinkAgentHome();
            output = uLinkAgentRObj.deleteULink(this.createULinkStateKeeper(),userInfo);
        }
        catch (Exception e) {
            Rlog.fatal("ulink", "In method - deleteUlink(Hashtable<String, String> userInfo) " + e);
            return -2;
        }
        return output;
    }

    /*
     * 
     * 
     * 
     * 
     * 
     * generates a String of XML for the link details entry form.<br><br>
     * 
     * 
     * 
     * 
     * 
     * Note that it is planned to encapsulate this detail in another object.
     * 
     * 
     * 
     * 
     * 
     */

    public String toXML()

    {

        return new String("<?xml version=\"1.0\"?>");

    }// end of method

}// end of class
