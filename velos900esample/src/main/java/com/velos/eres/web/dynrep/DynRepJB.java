/*
 *  Classname : DynRepJB
 *
 *  Version information: 1.0
 *
 *  Date: 09/28/2003
 *
 *  Copyright notice: Velos, Inc
 *
 *  Author: Vishal Abrol
 */
package com.velos.eres.web.dynrep;

/*
 * IMPORT STATEMENTS
 */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.eres.business.common.DynRepDao;
import com.velos.eres.business.dynrep.impl.DynRepBean;
import com.velos.eres.service.dynrepAgent.DynRepAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/*
 * END OF IMPORT STATEMENTS
 */

/**
 * Client side bean for dynrep
 * 
 * @author Vishal Abrol
 * @created October 25, 2004
 * @version 1.0 11/05/2003
 */

/*
 * Modified by Sonia Abrol, 04/14/05, added new attributes, repUseUniqueId,
 * repUseDataValue
 */

public class DynRepJB implements Serializable {
    /**
     * The primary key:pk_dynrep
     */

    private int id;

    /**
     * The foreign key reference to Form Lib table.
     */
    private String formId;

    /**
     * report name
     */
    private String repName;

    /**
     * report Description
     */
    private String repDesc;

    /**
     * report Filter
     */

    private String repFilter;

    /**
     * report orderby
     */
    private String repOrder;

    /**
     * Foreign key to table er_user
     */
    private String userId;

    /**
     * Foreign key to table er_account
     */
    private String accId;

    /**
     * report header name
     */
    private String repHdr;

    /**
     * report Footer name
     */
    private String repFtr;

    /**
     * The creator
     */

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    private String repType;

    private String studyId;

    private String perId;

    private String shareWith;

    /**
     * Stores whether Report is single form report pr multiform
     */
    public String reportCategory;

    /**
     * formId String in case of multiform report. Stores multiple formids
     * delimited by delimiter
     */
    public String formIdStr;

    /**
     * Flag for the report, to Use Unique Field ID as Display Name. However,
     * user will have an option to have a different setting for each field.
     * possible values: 0:true,1:false
     */
    private String repUseUniqueId;

    /**
     * Flag for the report, to Display Data value of multiple choice fields.
     * However, user will have an option to have a different setting for each
     * field. possible values: 0:true,1:false (default, display value will be
     * shown)
     */
    private String repUseDataValue;

    
    /**
     * Sets the id attribute of the DynRepJB object
     * 
     * @param id
     *            The new id value
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Sets the formId attribute of the DynRepJB object
     * 
     * @param formId
     *            The new formId value
     */
    public void setFormId(String formId) {
        this.formId = formId;
    }

    /**
     * Sets the repName attribute of the DynRepJB object
     * 
     * @param repName
     *            The new repName value
     */
    public void setRepName(String repName) {
        this.repName = repName;
    }

    /**
     * Sets the repDesc attribute of the DynRepJB object
     * 
     * @param repDesc
     *            The new repDesc value
     */
    public void setRepDesc(String repDesc) {
        this.repDesc = repDesc;
    }

    /**
     * Sets the repFilter attribute of the DynRepJB object
     * 
     * @param repFilter
     *            The new repFilter value
     */
    public void setRepFilter(String repFilter) {
        this.repFilter = repFilter;
    }

    /**
     * Sets the repOrder attribute of the DynRepJB object
     * 
     * @param repOrder
     *            The new repOrder value
     */
    public void setRepOrder(String repOrder) {
        this.repOrder = repOrder;
    }

    /**
     * Sets the repHdr attribute of the DynRepJB object
     * 
     * @param repHdr
     *            The new repHdr value
     */
    public void setRepHdr(String repHdr) {
        this.repHdr = repHdr;
    }

    /**
     * Sets the repFtr attribute of the DynRepJB object
     * 
     * @param repFtr
     *            The new repFtr value
     */
    public void setRepFtr(String repFtr) {
        this.repFtr = repFtr;
    }

    /**
     * Sets the userId attribute of the DynRepJB object
     * 
     * @param userId
     *            The new userId value
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * Sets the accId attribute of the DynRepJB object
     * 
     * @param accId
     *            The new accId value
     */
    public void setAccId(String accId) {
        this.accId = accId;
    }

    /**
     * Gets the id attribute of the DynRepJB object
     * 
     * @return The id value
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the formId attribute of the DynRepJB object
     * 
     * @return The formId value
     */
    public String getFormId() {
        return formId;
    }

    /**
     * Gets the repName attribute of the DynRepJB object
     * 
     * @return The repName value
     */
    public String getRepName() {
        return repName;
    }

    /**
     * Gets the repDesc attribute of the DynRepJB object
     * 
     * @return The repDesc value
     */
    public String getRepDesc() {
        return repDesc;
    }

    /**
     * Gets the repFilter attribute of the DynRepJB object
     * 
     * @return The repFilter value
     */
    public String getRepFilter() {
        return repFilter;
    }

    /**
     * Gets the repOrder attribute of the DynRepJB object
     * 
     * @return The repOrder value
     */
    public String getRepOrder() {
        return repOrder;
    }

    /**
     * Gets the repHdr attribute of the DynRepJB object
     * 
     * @return The repHdr value
     */
    public String getRepHdr() {
        return repHdr;
    }

    /**
     * Gets the repFtr attribute of the DynRepJB object
     * 
     * @return The repFtr value
     */
    public String getRepFtr() {
        return repFtr;
    }

    /**
     * Gets the userId attribute of the DynRepJB object
     * 
     * @return The userId value
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Gets the accId attribute of the DynRepJB object
     * 
     * @return The accId value
     */
    public String getAccId() {
        return accId;
    }

    /**
     * @return the Creator of the Form Field is returned
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The new creator value
     */

    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return modifiedBy
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The new modifiedBy value
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the IP Address of the Form Notification request
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The new ipAdd value
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * Gets the repType attribute of the DynRepJB object
     * 
     * @return The repType value
     */
    public String getRepType() {
        return this.repType;
    }

    /**
     * Sets the repType attribute of the DynRepJB object
     * 
     * @param repType
     *            The new repType value
     */
    public void setRepType(String repType) {
        this.repType = repType;
    }

    /**
     * Gets the studyId attribute of the DynRepJB object
     * 
     * @return The studyId value
     */
    public String getStudyId() {
        return this.studyId;
    }

    /**
     * Sets the studyId attribute of the DynRepJB object
     * 
     * @param studyId
     *            The new studyId value
     */
    public void setStudyId(String studyId) {
        this.studyId = studyId;
    }

    /**
     * Gets the perId attribute of the DynRepJB object
     * 
     * @return The perId value
     */
    public String getPerId() {
        return this.perId;
    }

    /**
     * Sets the perId attribute of the DynRepJB object
     * 
     * @param perId
     *            The new perId value
     */
    public void setPerId(String perId) {
        this.perId = perId;
    }

    /**
     * Returns the value of shareWith.
     * 
     * @return The shareWith value
     */
    public String getShareWith() {
        return shareWith;
    }

    /**
     * Sets the value of shareWith.
     * 
     * @param shareWith
     *            The value to assign shareWith.
     */
    public void setShareWith(String shareWith) {
        this.shareWith = shareWith;
    }

    /**
     * Returns the value of reportCategory.
     * 
     * @return The reportCategory value
     */
    public String getReportCategory() {
        return reportCategory;
    }

    /**
     * Sets the value of reportCategory.
     * 
     * @param reportCategory
     *            The value to assign reportCategory.
     */
    public void setReportCategory(String reportCategory) {
        this.reportCategory = reportCategory;
    }

    /**
     * Returns the value of formIdStr.
     * 
     * @return The formIdStr value
     */
    public String getFormIdStr() {
        return formIdStr;
    }

    /**
     * Sets the value of formIdStr.
     * 
     * @param formIdStr
     *            The value to assign formIdStr.
     */
    public void setFormIdStr(String formIdStr) {
        this.formIdStr = formIdStr;
    }

    /**
     * Returns the value of repUseUniqueId.
     */
    public String getRepUseUniqueId() {
        return repUseUniqueId;
    }

    /**
     * Sets the value of repUseUniqueId.
     * 
     * @param repUseUniqueId
     *            The value to assign repUseUniqueId.
     */
    public void setRepUseUniqueId(String repUseUniqueId) {
        this.repUseUniqueId = repUseUniqueId;
    }

    /**
     * Returns the value of repUseDataValue.
     */
    public String getRepUseDataValue() {
        return repUseDataValue;
    }

    /**
     * Sets the value of repUseDataValue.
     * 
     * @param repUseDataValue
     *            The value to assign repUseDataValue.
     */
    public void setRepUseDataValue(String repUseDataValue) {
        this.repUseDataValue = repUseDataValue;
    }

    // END OF THE GETTER AND SETTER METHODS

    /**
     * Constructor for the DynRepJB object
     */
    public DynRepJB() {
        Rlog.debug("dynrep", "DynRepJB.DynRepJB() ");
    }

    /**
     * Constructor for the DynRepJB object
     * 
     * @param id
     *            Description of the Parameter
     * @param formId
     *            Description of the Parameter
     * @param repName
     *            Description of the Parameter
     * @param repDesc
     *            Description of the Parameter
     * @param repFilter
     *            Description of the Parameter
     * @param repOrder
     *            Description of the Parameter
     * @param userId
     *            Description of the Parameter
     * @param accId
     *            Description of the Parameter
     * @param repHdr
     *            Description of the Parameter
     * @param repFtr
     *            Description of the Parameter
     * @param creator
     *            Description of the Parameter
     * @param modifiedBy
     *            Description of the Parameter
     * @param ipAdd
     *            Description of the Parameter
     * @param repType
     *            Description of the Parameter
     * @param studyId
     *            Description of the Parameter
     * @param perId
     *            Description of the Parameter
     * @param shareWith
     *            Description of the Parameter
     * @param reportCategory
     * @param formIdStr
     * @param repUseUniqueId
     * @param repUseDataValue
     */
    public DynRepJB(int id, String formId, String repName, String repDesc,
            String repFilter, String repOrder, String userId, String accId,
            String repHdr, String repFtr, String creator, String modifiedBy,
            String ipAdd, String repType, String studyId, String perId,
            String shareWith, String reportCategory, String formIdStr,
            String repUseUniqueId, String repUseDataValue) {
        setId(id);
        setFormId(formId);
        setRepName(repName);
        setRepDesc(repDesc);
        setRepFilter(repFilter);
        setRepOrder(repOrder);
        setRepHdr(repHdr);
        setRepFtr(repFtr);
        setUserId(userId);
        setAccId(accId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setRepType(repType);
        setStudyId(studyId);
        setPerId(perId);
        setShareWith(shareWith);
        setReportCategory(reportCategory);
        setFormIdStr(formIdStr);
        setRepUseUniqueId(repUseUniqueId);
        setRepUseDataValue(repUseDataValue);

    }

    /**
     * Gets the dynRepDetails attribute of the DynRepJB object
     * 
     * @return The dynRepDetails value
     */
    public DynRepBean getDynRepDetails() {
        DynRepBean dynrepsk = null;
        try {
            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();

            dynrepsk = dynrepAgentRObj.getDynRepDetails(this.id);
            Rlog
                    .debug("dynrep",
                            "DynRepJB.getDynRepDetails() DynRepStateKeeper "
                                    + dynrepsk);
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynrepJB getDynRepDetails" + e);
        }

        if (dynrepsk != null) {
            setId(dynrepsk.getId());
            setFormId(dynrepsk.getFormId());
            setRepName(dynrepsk.getRepName());
            setRepDesc(dynrepsk.getRepDesc());
            setRepFilter(dynrepsk.getRepFilter());
            setRepOrder(dynrepsk.getRepOrder());
            setRepHdr(dynrepsk.getRepHdr());
            setRepFtr(dynrepsk.getRepFtr());
            setUserId(dynrepsk.getUserId());
            setAccId(dynrepsk.getAccId());
            setCreator(dynrepsk.getCreator());
            setModifiedBy(dynrepsk.getModifiedBy());
            setIpAdd(dynrepsk.getIpAdd());
            Rlog.debug("dynrep", "repTtpe in DynRepJB:getDynRepDetails"
                    + dynrepsk.getRepType());
            setRepType(dynrepsk.getRepType());
            setStudyId(dynrepsk.getStudyId());
            setPerId(dynrepsk.getPerId());
            setShareWith(dynrepsk.getShareWith());
            setReportCategory(dynrepsk.getReportCategory());
            setFormIdStr(dynrepsk.getFormIdStr());
            setRepUseUniqueId(dynrepsk.getRepUseUniqueId());
            setRepUseDataValue(dynrepsk.getRepUseDataValue());

        }

        return dynrepsk;

    }

    /**
     * Sets the dynRepDetails attribute of the DynRepJB object
     * 
     * @return Description of the Return Value
     */
    public int setDynRepDetails() {
        int toReturn;
        try {

            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();

            DynRepBean tempStateKeeper = new DynRepBean();

            tempStateKeeper = this.createDynRepStateKeeper();

            toReturn = dynrepAgentRObj.setDynRepDetails(tempStateKeeper);
            Rlog.debug("dynrep", "DynRepJB.setDynRepDetails() Outta thewre2"
                    + dynrepAgentRObj + "statekeeper" + tempStateKeeper);
            this.setId(toReturn);
            Rlog.debug("dynrep", "DynRepJB.setDynRepDetails()");
            return toReturn;
        } catch (Exception e) {
            Rlog
                    .fatal("dynrep", "Error in setDynRepDetails() in dynrepJB "
                            + e);
            return -2;
        }
    }

    /**
     * @return a statekeeper object for the dynrepId Record with the current
     *         values of the bean
     */
    public DynRepBean createDynRepStateKeeper() {

        return new DynRepBean(id, formId, repName, repDesc, repFilter,
                repOrder, userId, accId, repHdr, repFtr, creator, modifiedBy,
                ipAdd, repType, studyId, perId, shareWith, reportCategory,
                formIdStr, repUseUniqueId, repUseDataValue);
    }

    /**
     * Calls updateDynRep() of Lab Session Bean: LabAgentBean
     * 
     * @return The status as an integer
     */
    public int updateDynRep() {
        int output;
        try {
            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();

            output = dynrepAgentRObj.updateDynRep(this
                    .createDynRepStateKeeper());
        } catch (Exception e) {
            Rlog.debug("dynrep",
                    "EXCEPTION IN SETTING dynrep DETAILS TO  SESSION BEAN" + e);
            return -2;
        }
        return output;
    }

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     */
    public int removeDynRep() {
        int output;
        try {

            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();

            output = dynrepAgentRObj.removeDynRep(this.id);
            return output;
        } catch (Exception e) {
            Rlog.fatal("dynrep", "in DynRepJB -removeDynRep() method" + e);
            return -1;
        }

    }
    // removeDynRep() Overloaded for INF-18183 ::: Raviesh
    public int removeDynRep(Hashtable<String, String> args) {
        int output;
        try {
            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();
            output = dynrepAgentRObj.removeDynRep(this.id,args);
            return output;
        } catch (Exception e) {
            Rlog.fatal("dynrep", "in DynRepJB -removeDynRep() method" + e);
            return -1;
        }
    }

    /**
     * Gets the mapData attribute of the DynRepJB object
     * 
     * @param formId
     *            Description of the Parameter
     * @return The mapData value
     */
    public DynRepDao getMapData(String formId) {
        DynRepDao dynDao = new DynRepDao();
        try {

            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();

            dynDao = dynrepAgentRObj.getMapData(formId);

            return dynDao;
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynRepJB:getMapData(formId) " + e);
            e.printStackTrace();
        }
        return dynDao;
    }

    /**
     * Gets the reportData attribute of the DynRepJB object
     * 
     * @param sql
     *            Description of the Parameter
     * @param fldNames
     *            Description of the Parameter
     * @param formType
     *            Description of the Parameter
     * @return The reportData value
     */
    public DynRepDao getReportData(String sql, String[] fldNames,
            String formType  , Hashtable moreParameters) {
        DynRepDao dynDao = new DynRepDao();
        try {

            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();

            dynDao = dynrepAgentRObj.getReportData(sql, fldNames, formType  ,  moreParameters);
            Rlog.debug("dynrep", "DynRepJB.getReportData() after Dao");
            return dynDao;
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynRepJB:getReportData() " + e);
        }
        return dynDao;
    }

    public DynRepDao getReportData(String sql, String[] fldNames,
            String formType, ArrayList fldTypes  , Hashtable htMore) {
        DynRepDao dynDao = new DynRepDao();
        try {
            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();

            dynDao = dynrepAgentRObj.getReportData(sql, fldNames, formType,
                    fldTypes  , htMore);
            Rlog.debug("dynrep", "DynRepJB.getReportData() after Dao");
            return dynDao;
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynRepJB:getReportData() " + e);
        }
        return dynDao;
    }

    /**
     * Gets the reportDetails attribute of the DynRepJB object
     * 
     * @param repIdStr
     *            Description of the Parameter
     * @return The reportDetails value
     */
    public DynRepDao getReportDetails(String repIdStr) {
        DynRepDao dynDao = new DynRepDao();
        try {

            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();
            dynDao = dynrepAgentRObj.getReportDetails(repIdStr);
            Rlog.debug("dynrep", "DynRepJB.getReportDetails() after Dao");
            return dynDao;
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynRepJB:getReportDetails() " + e);
        }
        return dynDao;

    }

    /**
     * Description of the Method
     * 
     * @param repIdStr
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public DynRepDao populateHash(String repIdStr) {
        DynRepDao dynDao = new DynRepDao();
        try {

            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();

            dynDao = dynrepAgentRObj.populateHash(repIdStr);
            Rlog.debug("dynrep", "DynRepJB.populateHash() after Dao");
            return dynDao;
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynRepJB:populateHash() " + e);
        }
        return dynDao;

    }

    /**
     * Sets the filterDetails attribute of the DynRepJB object
     * 
     * @param dynDao
     *            The new filterDetails value
     * @param mode
     *            The new filterDetails value
     * @return Description of the Return Value
     */
    public int setFilterDetails(DynRepDao dynDao, String mode) {
        // DynRepDao dynDao=new DynRepDao();
        int ret = 0;

        try {

            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();

            dynrepAgentRObj.setFilterDetails(dynDao, mode);
            Rlog.debug("dynrep", "DynRepJB.setFilterDetails() after Dao");

        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynRepJB:setFilterDetails() " + e);
        }
        return ret;

    }

    /**
     * Gets the filter attribute of the DynRepJB object
     * 
     * @param repId
     *            Description of the Parameter
     * @return The filter value
     */
    public DynRepDao getFilter(String repId) {
        DynRepDao dynDao = new DynRepDao();
        try {

            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();

            Rlog.debug("dynrep", "DynrepJB.getFilter() after remote");
            dynDao = dynrepAgentRObj.getFilter(repId);
            Rlog.debug("dynrep", "DynRepJB.getFilter()() after Dao");
            return dynDao;
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynRepJB:getFilter()() " + e);
        }
        return dynDao;

    }

    /**
     * Gets the filterDetails attribute of the DynRepJB object
     * 
     * @param repId
     *            Description of the Parameter
     * @return The filterDetails value
     */
    public DynRepDao getFilterDetails(String repId) {
        DynRepDao dynDao = new DynRepDao();
        try {

            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();

            dynDao = dynrepAgentRObj.getFilterDetails(repId);
            Rlog.debug("dynrep", "DynRepJB.getFilterDetails()() after Dao");
            return dynDao;
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynRepJB:getFilterDetails()() " + e);
        }
        return dynDao;

    }

    /**
     * Description of the Method
     * 
     * @param fltrIds
     *            Description of the Parameter
     * @return Description of the Return Value
     */
        public int removeFilters(ArrayList fltrIds) {
        int ret = -1;
        try {
            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();

            ret = dynrepAgentRObj.removeFilters(fltrIds);
            Rlog.debug("dynrep", "DynRepJB.removeFilters after Dao");

        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynRepJB:getFilterDetails()() " + e);
            return -2;
        }
        return ret;
    }
 // Overloaded for INF-18183 ::: Akshi
    public int removeFilters(ArrayList fltrIds,Hashtable<String, String> args) {
        int ret = -1;
        try {
            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();

            ret = dynrepAgentRObj.removeFilters(fltrIds,args);
            Rlog.debug("dynrep", "DynRepJB.removeFilters after Dao");

        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynRepJB:getFilterDetails()() " + e);
            return -2;
        }
        return ret;
    }
    
    /**
     * Gets the sectionDetails attribute of the DynRepJB object
     * 
     * @param paramCols
     *            Description of the Parameter
     * @param formId
     *            Description of the Parameter
     * @return The sectionDetails value
     */
    public DynRepDao getSectionDetails(ArrayList paramCols, String formId) {
        DynRepDao dynDao = new DynRepDao();
        try {

            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();

            Rlog.debug("dynrep", "DynrepJB.getSectionDetails() after remote");
            dynDao = dynrepAgentRObj.getSectionDetails(paramCols, formId);
            Rlog.debug("dynrep", "DynRepJB.getSectionDetails()() after Dao");
            return dynDao;
        } catch (Exception e) {
            Rlog
                    .fatal("dynrep", "Error in DynRepJB:getSectionDetails()() "
                            + e);
        }
        return dynDao;
    }

    /**
     * Description of the Method
     * 
     * @param repIdStr
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public DynRepDao fillReportContainer(String repIdStr) {
        DynRepDao dynDao = new DynRepDao();
        try {
            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();
            dynDao = dynrepAgentRObj.fillReportContainer(repIdStr);
            return dynDao;
        } catch (Exception e) {
            Rlog
                    .fatal("dynrep", "Error in DynRepJB:fillReportContainer() "
                            + e);
        }
        return dynDao;
    }

    /**
     * Gets the formNames attribute of the DynRepJB object
     * 
     * @param formIdStr
     *            Description of the Parameter
     * @param delimiter
     *            Description of the Parameter
     * @return The formNames value
     */
    public DynRepDao getFormNames(String formIdStr, String delimiter) {
        DynRepDao dynDao = new DynRepDao();
        try {
            DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();

            dynDao = dynrepAgentRObj.getFormNames(formIdStr, delimiter);
            return dynDao;
        } catch (Exception e) {
            Rlog
                    .fatal("dynrep", "Error in DynRepJB:fillReportContainer() "
                            + e);
        }
        return dynDao;
    }

/**
 * Description of the Method
 * 
 * @return Description of the Return Value
 */
public int copyDynRep(){
    int output;
    try {

        DynRepAgentRObj dynrepAgentRObj = EJBUtil.getDynRepAgentHome();

        output = dynrepAgentRObj.copyDynRep(this.repName, this.id, this.userId);        
        
        return output;
    } catch (Exception e) {
        Rlog.fatal("dynrep", "in DynRepJB -copyDynRep() method" + e);
        return -1;
    }

}
}
