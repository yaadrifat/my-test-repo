package com.velos.eres.web.dynrep.holder;

import java.io.Serializable;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * Contains the filter details ,managed by ReportHolder
 * 
 * @author vabrol
 * @created October 25, 2004
 */
public class FilterContainer implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3258130249966301236L;

    String filterId;

    String filterName;

    String filterStr;

    String formIdStr;

    String delimiter;

    ArrayList filterCols;

    ArrayList filterColNames;

    ArrayList filterQualifier;

    ArrayList filterData;

    ArrayList filterExclude;

    ArrayList filterBbrac;

    ArrayList filterEbrac;

    ArrayList filterOper;

    ArrayList filterSeq;

    ArrayList filterDbId;

    ArrayList formIds;

    ArrayList choppedFilters;

    ArrayList choppedFilterIds;

    /** Flag to identify if its a new filter or old */
    String filterMode;

    /**
     * the type of date range applied on the filter. Possible values: A:All,
     * M:Month,Y:Year,DR:given range, PS:patient study status
     */
    private String filterDateRangeType;

    /**
     * the 'from' date for the date range filter. In case of 'patient study
     * status', the attribute stores the code subtype of patient study status
     */
    private String filterDateRangeFrom;

    /**
     * the 'to' date for the date range filter. In case of 'patient study
     * status', the attribute stores the code subtype of patient study status
     */
    private String filterDateRangeTo;

    /**
     * Constructor for the FilterContainer object
     */
    public FilterContainer() {
        filterId = "";
        filterName = "";
        filterCols = new ArrayList();
        filterQualifier = new ArrayList();
        filterData = new ArrayList();
        filterExclude = new ArrayList();
        filterBbrac = new ArrayList();
        filterEbrac = new ArrayList();
        filterOper = new ArrayList();
        filterSeq = new ArrayList();
        filterExclude = new ArrayList();
        filterColNames = new ArrayList();
        filterDbId = new ArrayList();
        formIds = new ArrayList();
        formIdStr = "";
        delimiter = "";

        /* Stores the seperated filters and their Ids for each form */
        choppedFilters = new ArrayList();
        choppedFilterIds = new ArrayList();
        filterMode = "";
        filterDateRangeTo = "";
        filterDateRangeFrom = "";
        filterDateRangeType = "";

    }

    /**
     * Returns the value of filterId.
     * 
     * @return The filterId value
     */
    public String getFilterId() {
        return filterId;
    }

    /**
     * Sets the value of filterId.
     * 
     * @param filterId
     *            The value to assign filterId.
     */
    public void setFilterId(String filterId) {
        this.filterId = filterId;
    }

    /**
     * Returns the value of filterName.
     * 
     * @return The filterName value
     */
    public String getFilterName() {
        return filterName;
    }

    /**
     * Sets the value of filterName.
     * 
     * @param filterName
     *            The value to assign filterName.
     */
    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    /**
     * Returns the value of filterCols.
     * 
     * @return The filterCols value
     */
    public ArrayList getFilterCols() {
        return filterCols;
    }

    /**
     * Sets the value of filterCols.
     * 
     * @param filterCols
     *            The value to assign filterCols.
     */
    public void setFilterCols(ArrayList filterCols) {
        this.filterCols = filterCols;
    }

    /**
     * Sets the value of filterCols.
     * 
     * @param filterCol
     *            The new filterCols value
     */
    public void setFilterCols(String filterCol) {
        this.filterCols.add(filterCol);
    }

    /**
     * Returns the value of filterQualifier.
     * 
     * @return The filterQualifier value
     */
    public ArrayList getFilterQualifier() {
        return filterQualifier;
    }

    /**
     * Sets the value of filterQualifier.
     * 
     * @param filterQualifier
     *            The value to assign filterQualifier.
     */
    public void setFilterQualifier(ArrayList filterQualifier) {
        this.filterQualifier = filterQualifier;
    }

    /**
     * Sets the value of filterQualifier.
     * 
     * @param filterQualifier
     *            The value to add to filterQualifier.
     */
    public void setFilterQualifier(String filterQualifier) {
        this.filterQualifier.add(filterQualifier);
    }

    /**
     * Returns the value of filterData.
     * 
     * @return The filterData value
     */
    public ArrayList getFilterData() {
        return filterData;
    }

    /**
     * Sets the value of filterData.
     * 
     * @param filterData
     *            The value to assign filterData.
     */
    public void setFilterData(ArrayList filterData) {
        this.filterData = filterData;
    }

    /**
     * Sets the value of filterData.
     * 
     * @param filterData
     *            The value to assign filterData.
     */
    public void setFilterData(String filterData) {
        this.filterData.add(filterData);
    }

    /**
     * Returns the value of filterExclude.
     * 
     * @return The filterExclude value
     */
    public ArrayList getFilterExclude() {
        return filterExclude;
    }

    /**
     * Sets the value of filterExclude.
     * 
     * @param filterExclude
     *            The value to assign filterExclude.
     */
    public void setFilterExclude(ArrayList filterExclude) {
        this.filterExclude = filterExclude;
    }

    /**
     * Sets the value of filterExclude.
     * 
     * @param filterExclude
     *            The value to assign filterExclude.
     */
    public void setFilterExclude(String filterExclude) {
        this.filterExclude.add(filterExclude);
    }

    /**
     * Returns the value of filterBbrac.
     * 
     * @return The filterBbrac value
     */
    public ArrayList getFilterBbrac() {
        return filterBbrac;
    }

    /**
     * Sets the value of filterBbrac.
     * 
     * @param filterBbrac
     *            The value to assign filterBbrac.
     */
    public void setFilterBbrac(ArrayList filterBbrac) {
        this.filterBbrac = filterBbrac;
    }

    /**
     * Sets the value of filterBbrac.
     * 
     * @param filterBbrac
     *            The value to assign filterBbrac.
     */
    public void setFilterBbrac(String filterBbrac) {
        this.filterBbrac.add(filterBbrac);
    }

    /**
     * Returns the value of filterEbrac.
     * 
     * @return The filterEbrac value
     */
    public ArrayList getFilterEbrac() {
        return filterEbrac;
    }

    /**
     * Sets the value of filterEbrac.
     * 
     * @param filterEbrac
     *            The value to assign filterEbrac.
     */
    public void setFilterEbrac(ArrayList filterEbrac) {
        this.filterEbrac = filterEbrac;
    }

    /**
     * Sets the value of filterEbrac.
     * 
     * @param filterEbrac
     *            The value to assign filterEbrac.
     */
    public void setFilterEbrac(String filterEbrac) {
        this.filterEbrac.add(filterEbrac);
    }

    /**
     * Returns the value of filterOper.
     * 
     * @return The filterOper value
     */
    public ArrayList getFilterOper() {
        return filterOper;
    }

    /**
     * Sets the value of filterOper.
     * 
     * @param filterOper
     *            The value to assign filterOper.
     */
    public void setFilterOper(ArrayList filterOper) {
        this.filterOper = filterOper;
    }

    /**
     * Sets the value of filterOper.
     * 
     * @param filterOper
     *            The value to assign filterOper.
     */
    public void setFilterOper(String filterOper) {
        this.filterOper.add(filterOper);
    }

    /**
     * Returns the value of FilterSeq.
     * 
     * @return The filterSeq value
     */
    public ArrayList getFilterSeq() {
        return filterSeq;
    }

    /**
     * Sets the value of FilterSeq.
     * 
     * @param filterSeq
     *            The new filterSeq value
     */
    public void setFilterSeq(ArrayList filterSeq) {
        this.filterSeq = filterSeq;
    }

    /**
     * Sets the value of FilterSeq.
     * 
     * @param FilterSeq
     *            The value to assign FilterSeq.
     */
    public void setFilterSeq(String FilterSeq) {
        this.filterSeq.add(FilterSeq);
    }

    /**
     * Returns the value of filterStr.
     * 
     * @return The filterStr value
     */
    public String getFilterStr() {
        return filterStr;
    }

    /**
     * Sets the value of filterStr.
     * 
     * @param filterStr
     *            The value to assign filterStr.
     */
    public void setFilterStr(String filterStr) {
        this.filterStr = filterStr;
    }

    /**
     * Returns the value of filterColNames.
     * 
     * @return The filterColNames value
     */
    public ArrayList getFilterColNames() {
        return filterColNames;
    }

    /**
     * Sets the value of filterColNames.
     * 
     * @param filterColNames
     *            The value to assign filterColNames.
     */
    public void setFilterColNames(ArrayList filterColNames) {
        this.filterColNames = filterColNames;
    }

    /**
     * Sets the value of filterColNames.
     * 
     * @param filterColName
     *            The new filterColNames value
     */
    public void setFilterColNames(String filterColName) {
        this.filterColNames.add(filterColName);
    }

    /**
     * Returns the value of filterDbId.
     * 
     * @return The filterDbId value
     */
    public ArrayList getFilterDbId() {
        return filterDbId;
    }

    /**
     * Sets the value of filterDbId.
     * 
     * @param filterDbId
     *            The value to assign filterDbId.
     */
    public void setFilterDbId(ArrayList filterDbId) {
        this.filterDbId = filterDbId;
    }

    /**
     * Sets the value of filterDbId.
     * 
     * @param filterDbId
     *            The value to aadd to filterDbId.
     */
    public void setFilterDbId(String filterDbId) {
        this.filterDbId.add(filterDbId);
    }

    /**
     * Returns the value of formIds.
     * 
     * @return The formIds value
     */
    public ArrayList getFormIds() {
        return formIds;
    }

    /**
     * Sets the value of formIds.
     * 
     * @param formIds
     *            The value to assign formIds.
     */
    public void setFormIds(ArrayList formIds) {
        this.formIds = formIds;
    }

    /**
     * Sets the value of formIds.
     * 
     * @param formId
     *            The new formIds value
     */
    public void setFormIds(String formId) {
        this.formIds.add(formId);
    }

    /**
     * Returns the value of choppedFilters.
     * 
     * @return The choppedFilters value
     */
    public ArrayList getChoppedFilters() {
        return choppedFilters;
    }

    /**
     * Returns the value of choppedFilters.
     * 
     * @return The choppedFilters value
     */
    public String getChoppedFilter(String formId) {
        int index = -1;
        index = formIds.indexOf(formId);
        if (index >= 0)
            return (String) choppedFilters.get(index);
        else
            return "";

    }

    /**
     * Sets the value of choppedFilters.
     * 
     * @param choppedFilters
     *            The value to assign choppedFilters.
     */
    public void setChoppedFilters(ArrayList choppedFilters) {
        this.choppedFilters = choppedFilters;
    }

    /**
     * Sets the value of choppedFilters.
     * 
     * @param choppedFilter
     *            The new choppedFilters value
     */
    public void setChoppedFilters(String choppedFilter) {
        this.choppedFilters.add(choppedFilter);
    }

    /**
     * Returns the value of formIdStr.
     * 
     * @return The formIdStr value
     */
    public String getFormIdStr() {
        return formIdStr;
    }

    /**
     * Sets the value of formIdStr.
     * 
     * @param formIdStr
     *            The value to assign formIdStr.
     */
    public void setFormIdStr(String formIdStr) {
        this.formIdStr = formIdStr;
        formIds.clear();
        addFormIds();
    }

    /**
     * Returns the value of choppedFilterIds.
     */
    public ArrayList getChoppedFilterIds() {
        return choppedFilterIds;
    }

    /**
     * Sets the value of choppedFilterIds.
     * 
     * @param choppedFilterIds
     *            The value to assign choppedFilterIds.
     */
    public void setChoppedFilterIds(ArrayList choppedFilterIds) {
        this.choppedFilterIds = choppedFilterIds;
    }

    /**
     * Sets the value of choppedFilterIds.
     * 
     * @param choppedFilterIds
     *            The value to assign choppedFilterIds.
     */
    public void setChoppedFilterIds(String choppedFilterId) {
        this.choppedFilterIds.add(choppedFilterId);
    }

    /**
     * Returns the value of filterMode.
     */
    public String getFilterMode() {
        return filterMode;
    }

    /**
     * Sets the value of filterMode.
     * 
     * @param filterMode
     *            The value to assign filterMode.
     */
    public void setFilterMode(String filterMode) {
        this.filterMode = filterMode;
    }

    /**
     * Returns the value of filterDateRangeType.
     */
    public String getFilterDateRangeType() {
        return filterDateRangeType;
    }

    /**
     * Sets the value of filterDateRangeType.
     * 
     * @param filterDateRangeType
     *            The value to assign filterDateRangeType.
     */
    public void setFilterDateRangeType(String filterDateRangeType) {
        this.filterDateRangeType = filterDateRangeType;
    }

    /**
     * Returns the value of filterDateRangeFrom.
     */
    public String getFilterDateRangeFrom() {
        return filterDateRangeFrom;
    }

    /**
     * Sets the value of filterDateRangeFrom.
     * 
     * @param filterDateRangeFrom
     *            The value to assign filterDateRangeFrom.
     */
    public void setFilterDateRangeFrom(String filterDateRangeFrom) {
        this.filterDateRangeFrom = filterDateRangeFrom;
    }

    /**
     * Returns the value of filterDateRangeTo.
     */
    public String getFilterDateRangeTo() {
        return filterDateRangeTo;
    }

    /**
     * Sets the value of filterDateRangeTo.
     * 
     * @param filterDateRangeTo
     *            The value to assign filterDateRangeTo.
     */
    public void setFilterDateRangeTo(String filterDateRangeTo) {
        this.filterDateRangeTo = filterDateRangeTo;
    }

    /**
     * Parses the FormIds delimited list and stores in formIds List
     */
    public void addFormIds() {
        String[] formArray;
        String[] formNameArray;
        if (formIdStr == null) {
            Rlog.fatal("dynrep", "Cannot add null String to Form List");
        }
        if (formIdStr.indexOf(delimiter) >= 0) {
            formArray = StringUtil.strSplit(formIdStr, delimiter, false);
            for (int i = 0; i < formArray.length; i++) {
                if (formIds.indexOf(formArray[i]) < 0) {
                    setFormIds(formArray[i]);
                }
            }

        } else {
            setFormIds(formIdStr);

        }

    }

    /**
     * Description of the Method
     */
    public void reset() {
        filterCols.clear();
        filterQualifier.clear();
        filterData.clear();
        filterExclude.clear();
        filterBbrac.clear();
        filterEbrac.clear();
        filterOper.clear();
        filterSeq.clear();
        filterExclude.clear();
        formIds.clear();
        choppedFilters.clear();
        filterMode = "";
        filterDateRangeTo = "";
        filterDateRangeFrom = "";
        filterDateRangeType = "";

    }

    /**
     * Returns the filter string for default date range filter. Will be used, if
     * no chopped filters are defined
     * 
     * @return the filter string for default date range filter
     */
    public String getDateRangeFilterString() {
        String fltrStr = "";

        if (!StringUtil.isEmpty(filterDateRangeType)) {
            if ((!StringUtil.isEmpty(filterDateRangeFrom))
                    && (!StringUtil.isEmpty(filterDateRangeTo))) {
                // for date range, Year and Month filters
                if (filterDateRangeType.equals("DR")
                        || filterDateRangeType.equals("M")
                        || filterDateRangeType.equals("Y")) {

                    fltrStr = " ( trunc(created_on) >= to_date('"
                            + filterDateRangeFrom
                            + "',PKG_DATEUTIL.F_GET_DATEFORMAT) and trunc(created_on) <= to_date('"
                            + filterDateRangeTo + "',PKG_DATEUTIL.F_GET_DATEFORMAT) )";
                } else if (filterDateRangeType.equals("PS")) {
                    fltrStr = " ( trunc(created_on) >= NVL(Pkg_Util.f_get_min_patstudystatdate([VELSTUDY],[VELPAT],'"
                            + filterDateRangeFrom
                            + "'),TO_DATE(PKG_DATEUTIL.F_GET_FUTURE_NULL_DATE_STR,PKG_DATEUTIL.F_GET_DATEFORMAT)) ";
                    fltrStr = fltrStr
                            + "  and trunc(created_on) <= NVL(Pkg_Util.f_get_min_patstudystatdate([VELSTUDY],[VELPAT],'"
                            + filterDateRangeTo + "'),SYSDATE) )";
                }
            }
        }
        return fltrStr;

    }

    /**
     * Description of the Method
     */
    public void Display() {
        System.out.println("*************Filter Objects lists****************");
        System.out.println(filterCols + "\n" + filterQualifier + "\n"
                + filterData + "\n" + filterExclude + "\n" + filterBbrac + "\n"
                + filterEbrac + "\n" + filterOper + "\n" + filterSeq + "\n"
                + filterExclude + "\n" + formIds + "\n" + choppedFilters
                + "\n\n" + filterStr + "\n\nfilterMode:" + filterMode);
        System.out.println("\n filterDateRangeTo" + filterDateRangeTo
                + "\n filterDateRangeFrom" + filterDateRangeFrom
                + "\n filterDateRangeType" + filterDateRangeType);

        System.out.println("*************Filter Objects END****************");
    }

}
