/*
 * Classname:           GroupJB
 *
 * Version information: 1.0
 *
 * Date: 				02/25/2001
 *
 * Copyright notice: 	Velos inc.
 */

package com.velos.eres.web.group;

import com.velos.eres.business.common.GroupDao;
import com.velos.eres.business.group.impl.GroupBean;
import com.velos.eres.service.groupAgent.GroupAgentRObj;
import com.velos.eres.service.groupAgent.impl.GroupAgentBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * Client side bean for Group
 *
 * @author Sajal
 * @version 1.0 02/25/2001
 */

public class GroupJB {

    /**
     * Group Id
     */
    private int groupId;

    /**
     * Group Accound Id
     */
    private String groupAccId;

    /**
     * Group Name
     */
    private String groupName;

    /**
     * Group Description
     */
    private String groupDesc;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    /*
     * Super User Flag
     */
    private String groupSuperUserFlag;

    /*
     * Super User Flag
     */
    private String groupSuperUserRights;

    /*
     * Budget Super User Flag
     */
    private String groupSupBudFlag;

    /*
     * Budget Super User Rights
     */
    private String groupSupBudRights;

    /*
     * Group Hidden value
     */

    private String groupHidden; //KM

    /**
     * the codelst id for the specific role
     *
     */

    public String roleId;


    // GETTER SETTER METHODS

    /**
     *
     *
     * @return Id of the group
     */
    public int getGroupId() {
        return this.groupId;
    }

    /**
     *
     *
     * @param groupId
     *            Id of the group
     */
    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    /**
     *
     *
     * @return Account Id of the group
     */
    public String getGroupAccId() {
        return this.groupAccId;
    }

    /**
     *
     *
     * @param groupAccId
     *            Account Id of the group
     */
    public void setGroupAccId(String groupAccId) {
        this.groupAccId = groupAccId;
    }

    /**
     *
     *
     * @return name of the group
     */
    public String getGroupName() {
        return this.groupName;
    }

    /**
     *
     *
     * @param groupName
     *            name of the group
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     *
     *
     * @return Description of the group
     */
    public String getGroupDesc() {
        return this.groupDesc;
    }

    /**
     *
     *
     * @param groupDesc
     *            description of the group
     */
    public void setGroupDesc(String groupDesc) {
        this.groupDesc = groupDesc;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return Last Modified By
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The value that is required to be registered as record last
     *            modified by
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @param groupSuperUserFlag
     *            The groupSuperUserFlag to indicate if the group is a super
     *            user group
     */
    public void setGroupSuperUserFlag(String groupSuperUserFlag) {
        this.groupSuperUserFlag = groupSuperUserFlag;
    }

    /**
     * @param groupSuperUserRights
     *            The groupSuperUserRights - study rights for the super user
     *            group
     */
    public void setGroupSuperUserRights(String groupSuperUserRights) {
        this.groupSuperUserRights = groupSuperUserRights;
    }

    /**
     * @return groupSuperUserFlag
     */
    public String getGroupSuperUserFlag() {
        return this.groupSuperUserFlag;
    }

    /**
     * @return groupSuperUserRights
     */
    public String getGroupSuperUserRights() {
        return this.groupSuperUserRights;
    }

    /**
     * Returns the value of groupSupBudFlag.
     */
    public String getGroupSupBudFlag() {
    	groupSupBudFlag = (groupSupBudFlag == null)?"":groupSupBudFlag;
        return groupSupBudFlag;
    }

    /**
     * Sets the value of groupSupBudFlag.
     *
     * @param groupSupBudFlag
     *            The value to assign groupSupBudFlag.
     */
    public void setGroupSupBudFlag(String groupSupBudFlag) {
        this.groupSupBudFlag = groupSupBudFlag;
    }

    /**
     * Returns the value of groupSupBudRights.
     */
    public String getGroupSupBudRights() {
        return groupSupBudRights;
    }

    /**
     * Sets the value of groupSupBudRights.
     *
     * @param groupSupBudRights
     *            The value to assign groupSupBudRights.
     */
    public void setGroupSupBudRights(String groupSupBudRights) {
        this.groupSupBudRights = groupSupBudRights;
    }

    //Added by Manimaran for Enh.#U11
    /**
     *
     * @return value of group hidden
     */
    public String getGroupHidden() {
        return this.groupHidden;
    }

    /**
     * @param groupHidden
     *            name of the groupHidden
     */
    public void setGroupHidden(String groupHidden) {
    	String grpH = "";

    	if (StringUtil.isEmpty(groupHidden))
    	{
    		grpH = "0";
    	}
    	else
    	{

    		grpH = groupHidden;
    	}
        this.groupHidden = grpH;
    }


    /**
     * @return the codelst id for the specific role
     */

    public String getRoleId() {
        return this.roleId;
    }

    /**
     * @param roleID
     *            the codelst id for the specific role
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }


    // END OF GETTER SETTER METHODS

    /**
     * Sets the id of the group
     *
     * @param groupId
     */
    public GroupJB(int groupId) {
        setGroupId(groupId);
        Rlog.debug("group", "GroupJB constructor with groupId");
    }

    /**
     * Default Constructor
     *
     */
    public GroupJB() {
        Rlog.debug("group", "GroupJB blank constructor");
    };

    /**
     * Full arguments constructor.
     *
     * @param groupId
     * @param groupAccId
     * @param groupName
     * @param groupDesc
     */
    public GroupJB(int groupId, String groupAccId, String groupName,
            String groupDesc, String creator, String modifiedBy, String ipAdd,
            String groupSuperUserFlag, String groupSuperUserRights,
            String groupSupBudFlag, String groupSupBudRights, String groupHidden, String roleId) {
        setGroupId(groupId);
        setGroupAccId(groupAccId);
        setGroupName(groupName);
        setGroupDesc(groupDesc);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setGroupSuperUserFlag(groupSuperUserFlag);
        setGroupSuperUserRights(groupSuperUserRights);
        setGroupSupBudFlag(groupSupBudFlag);
        setGroupSupBudRights(groupSupBudRights);
        setGroupHidden(groupHidden);//KM
        setRoleId(roleId);


        Rlog.debug("group", "GroupJB full arguments constructor");
    }

    /**
     * Calls getGroupDetails() of Group Session Bean: GroupAgentBean
     *
     * @return State keeper which holds all the values of the group
     * @see GroupAgentBean
     */
    public GroupBean getGroupDetails() {
        GroupBean gsk = null;
        try {
            GroupAgentRObj groupAgentRObj = EJBUtil.getGroupAgentHome();
            gsk = groupAgentRObj.getGroupDetails(this.groupId);
            Rlog.debug("group", "GroupJB.getGroupDetails() GroupBean " + gsk);
        } catch (Exception e) {
            Rlog.fatal("group", "Error in getGroupDetails() in GroupJB " + e);
        }
        if (gsk != null) {
            this.groupId = gsk.getGroupId();
            this.groupAccId = gsk.getGroupAccId();
            this.groupName = gsk.getGroupName();
            this.groupDesc = gsk.getGroupDesc();
            this.creator = gsk.getCreator();
            this.modifiedBy = gsk.getModifiedBy();
            this.ipAdd = gsk.getIpAdd();
            setGroupSuperUserFlag(gsk.getGroupSuperUserFlag());
            setGroupSuperUserRights(gsk.getGroupSuperUserRights());
            setGroupSupBudFlag(gsk.getGroupSupBudFlag());
            setGroupSupBudRights(gsk.getGroupSupBudRights());
            this.groupHidden = gsk.getGroupHidden();
            this.roleId= gsk.getRoleId();

        }
        return gsk;
    }

    /**
     * Calls setGroupDetails() of Group Session Bean: GroupAgentBean
     *
     * @see GroupAgentBean
     */

    public void setGroupDetails() {
        try {
            Rlog.debug("group", "GroupJB.setGroupDetails() ipAdd= "
                    + getIpAdd());
            GroupAgentRObj groupAgentRObj = EJBUtil.getGroupAgentHome();
            this.setGroupId(groupAgentRObj.setGroupDetails(this
                    .createGroupStateKeeper()));
            Rlog.debug("group", "GroupJB.setGroupDetails()");
        } catch (Exception e) {
            Rlog.fatal("group", "Error in setGroupDetails() in GroupJB " + e);
        }
    }

    /**
     * Calls updateGroup() of Group Session Bean: GroupAgentBean
     *
     * @return 0 when updation is successful and -2 when exception occurs
     */

    public int updateGroup() {
        int output;
        try {
            GroupAgentRObj groupAgentRObj = EJBUtil.getGroupAgentHome();
            output = groupAgentRObj.updateGroup(this.createGroupStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("group",
                    "EXCEPTION IN SETTING GROUP DETAILS TO  SESSION BEAN " + e);
            return -2;
        }
        return output;
    }

    /**
     *
     *
     * @return the group StateKeeper Object with the current values
     */
    public GroupBean createGroupStateKeeper() {

        return new GroupBean(

        this.groupId, this.groupAccId, this.groupName, this.groupDesc,
                this.creator, this.modifiedBy, this.ipAdd,
                this.groupSuperUserFlag, this.groupSuperUserRights,
                this.groupSupBudFlag, this.groupSupBudRights, this.groupHidden, this.roleId);

    }

    /**
     * Returns the list of all the groups for an account
     *
     * @param accId
     *            Id of the account for which list of groups is required
     * @return GroupDao Object holding the list of all the groups
     * @see GroupDao
     */
    public GroupDao getByAccountId(int accId) {
        GroupDao groupDao = new GroupDao();
        try {
            Rlog.debug("group", "GroupJB.getByAccountId starting");

            GroupAgentRObj groupAgentRObj = EJBUtil.getGroupAgentHome();
            Rlog.debug("group", "GroupJB.getByAccountId after remote");
            groupDao = groupAgentRObj.getByAccountId(accId);
            Rlog.debug("group", "GroupJB.getByAccountId after Dao");
            return groupDao;
        } catch (Exception e) {
            Rlog.fatal("group",
                    "Error in getByAccountId(int accId) in GroupJB " + e);
        }
        return groupDao;
    }

    /**
     * Calls getCount() of Group Session Bean: GroupAgentBean
     *
     * @param groupName
     * @param accId
     * @return returns int
     * @See GroupDao
     */
    public int getCount(String groupName, String accId) {
        int count = 0;
        try {
            Rlog.debug("group", "GroupJB.getCount starting");
            GroupAgentRObj groupAgentRObj = EJBUtil.getGroupAgentHome();
            Rlog.debug("group", "GroupJB.getCount after remote");
            count = groupAgentRObj.getCount(groupName, accId);
            Rlog.debug("group", "GroupJB.getCount after Dao");
        } catch (Exception e) {
            Rlog.fatal("group", "Error in getCount(groupName) in GroupJB " + e);
        }
        return count;
    }

    /**
     * Calls getDefaultStudySuperUserRights(String p_user) of Group Session Bean: GroupAgentBean
     *
     * @param p_user
     *
     * @return returns String
     * @See GroupDao
     */
   public  String getDefaultStudySuperUserRights(String p_user) {
        String rights ;
        try {

            GroupAgentRObj groupAgentRObj = EJBUtil.getGroupAgentHome();

            rights  = groupAgentRObj.getDefaultStudySuperUserRights(p_user);

        } catch (Exception e) {
            Rlog.fatal("group", "Exception in getDefaultStudySuperUserRights(p_user) in GroupJB " + e);
            rights  = "";
        }
        return rights  ;
    }

    /**
     * Calls getGroupDaoInstance() on GroupDao; *
     *
     * @return GroupDao Object
     * @see GroupDao
     */
    public GroupDao getGroupDaoInstance() {
        GroupDao gDao = null;
        try {
            GroupAgentRObj groupAgentRObj = EJBUtil.getGroupAgentHome();
            gDao = groupAgentRObj.getGroupDaoInstance();
        } catch (Exception e) {
            Rlog.fatal("user",
                    "Error in getgroupAgentRObj.getSettingDao in GroupJB " + e);
        }
        return gDao;
    }

    /**
     * generates a String of XML for the group details entry form.<br>
     * <br>
     * Not yet in Use
     *
     * @return
     */
    public String toXML() {
        Rlog.debug("group", "GroupJB.toXML()");
        return new String(
                "<?xml version=\"1.0\"?>"
                        + "<?cocoon-process type=\"xslt\"?>"
                        + "<?xml-stylesheet href=\"/stylesheet/form-html.xsl\" type=\"text/xsl\"?>"
                        + "<?xml-stylesheet href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>"
                        +
                        // to be done "<form action=\"groupsummary.jsp\">" +
                        "<head>" +
                        // to be done "<title>Group Summary </title>" +
                        "</head>"
                        + "<input type=\"hidden\" name=\"groupId\" value=\""
                        + this.getGroupId() + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\"groupAccId\" value=\""
                        + this.getGroupAccId() + "\" size=\"10\"/>"
                        + "<input type=\"text\" name=\" groupName\" value=\""
                        + this.getGroupName() + "\" size=\"50\"/>"
                        + "<input type=\"text\" name=\" groupDesc\" value=\""
                        + this.getGroupDesc() + "\" size=\"200\"/>" + "</form>");

    }// end of method




  //JM: 15Apr2010: Enh #D-FIN2
   public int copyRoleDefRightsToGrpSupUserRights(int roleId, int grpId){

	   int success = 0;

	   try {
           GroupAgentRObj groupAgentRObj = EJBUtil.getGroupAgentHome();
           success = groupAgentRObj.copyRoleDefRightsToGrpSupUserRights(roleId, grpId);
       } catch (Exception e) {
           Rlog.fatal("group",
                   "Error in copyRoleDefRightsToGrpSupUserRights() in GroupJB " + e);
       }

       return success;


   }

}// end of class

