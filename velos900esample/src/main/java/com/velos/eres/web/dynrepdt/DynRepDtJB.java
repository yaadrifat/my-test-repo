/*
 *  Classname : DynRepJB
 *
 *  Version information: 1.0
 *
 *  Date: 09/28/2003
 *
 *  Copyright notice: Velos, Inc
 *
 *  Author: Vishal Abrol
 */
/*Modified by Sonia Abrol. 01/26/05, added a new attribute formType. 
 This attribute is added to distinguish between application forms and core table lookup forms*/
package com.velos.eres.web.dynrepdt;

/*
 * IMPORT STATEMENTS
 */
import java.util.ArrayList;

import javax.persistence.Column;

import com.velos.eres.business.dynrepdt.impl.DynRepDtBean;
import com.velos.eres.service.dynrepdtAgent.DynRepDtAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/*
 * END OF IMPORT STATEMENTS
 */

/**
 * Client side bean for dynrep
 * 
 * @author Vishal Abrol
 * @created October 25, 2004
 * @version 1.0 11/05/2003
 */
/*
 * Modified by Sonia Abrol, 04/14/05, added new attributes, useUniqueId,
 * useDataValue
 */

public class DynRepDtJB {
    /**
     * The primary key:pk_dynrepview
     */

    private int id;

    /**
     * The foreign key reference to er_dynrep table.
     */
    private String repId;

    /**
     * report name
     */
    private String repCol;

    /**
     * report column sequence
     */
    private String colSeq;

    /**
     * report col width
     */

    private String colWidth;

    /**
     * report Format
     */
    private String colFormat;

    /**
     * report dispname
     */
    private String colDisp;

    /**
     * The creator
     */

    private String creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    private String modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    private String ipAdd;

    private String colName;

    private String colType;

    private String formId;

    /**
     * Distinguishes between application forms and core table lookup forms <br>
     * Possible Values: <br>
     * 'F' - Application forms <br>
     * 'C' - COre table lookup forms
     */
    private String formType;

    /**
     * Flag to Use 'Unique Field ID' as Display Name. possible values:
     * 0:true,1:false
     */
    private String useUniqueId;

    /**
     * Flag to Display Data value of multiple choice fields. possible values:
     * 0:true,1:false (default, display value will be shown)
     */

    private String useDataValue;
    
    
    /**
     *the repeat number count for the selected field
     */
    private String repeatNumber;
    

    public String getRepeatNumber() {
		return repeatNumber;
	}

	public void setRepeatNumber(String repeatNumber) {
		this.repeatNumber = repeatNumber;
	}

    
    
    /**
     *Flag to calculate summary information for this field - min/max/median/avg. Possible values-0:do not calculate, 1:calculate
     */
    private String calcSum;


    /**
     *Flag to calculate count/percentage information. Possible values-0:do not calculate,1:calculate
     */
    private String calcPer;

    
    public String getCalcPer() {
		return calcPer;
	}

	public void setCalcPer(String calcPer) {
		this.calcPer = calcPer;
	}

	public String getCalcSum() {
		return calcSum;
	}

	public void setCalcSum(String calcSum) {
		this.calcSum = calcSum;
	}

	// /////////////////////////////////////////////////////////////////////////////////
    /**
     * Sets the id attribute of the DynRepDtJB object
     * 
     * @param id
     *            The new id value
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the id attribute of the DynRepDtJB object
     * 
     * @return The id value
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the repId attribute of the DynRepDtJB object
     * 
     * @param repId
     *            The new repId value
     */
    public void setRepId(String repId) {
        this.repId = repId;
    }

    /**
     * Sets the repCol attribute of the DynRepDtJB object
     * 
     * @param repCol
     *            The new repCol value
     */
    public void setRepCol(String repCol) {
        this.repCol = repCol;
    }

    /**
     * Sets the colSeq attribute of the DynRepDtJB object
     * 
     * @param colSeq
     *            The new colSeq value
     */
    public void setColSeq(String colSeq) {
        this.colSeq = colSeq;
    }

    /**
     * Sets the colWidth attribute of the DynRepDtJB object
     * 
     * @param colWidth
     *            The new colWidth value
     */
    public void setColWidth(String colWidth) {
        this.colWidth = colWidth;
    }

    /**
     * Sets the colFormat attribute of the DynRepDtJB object
     * 
     * @param colFormat
     *            The new colFormat value
     */
    public void setColFormat(String colFormat) {
        this.colFormat = colFormat;
    }

    /**
     * Sets the colDisp attribute of the DynRepDtJB object
     * 
     * @param colDisp
     *            The new colDisp value
     */
    public void setColDisp(String colDisp) {
        this.colDisp = colDisp;
    }

    /**
     * Sets the colName attribute of the DynRepDtJB object
     * 
     * @param colName
     *            The new colName value
     */
    public void setColName(String colName) {
        this.colName = colName;
    }

    /**
     * Sets the colType attribute of the DynRepDtJB object
     * 
     * @param colType
     *            The new colType value
     */
    public void setColType(String colType) {
        this.colType = colType;
    }

    /**
     * Gets the repId attribute of the DynRepDtJB object
     * 
     * @return The repId value
     */
    public String getRepId() {
        return repId;
    }

    /**
     * Gets the repCol attribute of the DynRepDtJB object
     * 
     * @return The repCol value
     */
    public String getRepCol() {
        return repCol;
    }

    /**
     * Gets the colSeq attribute of the DynRepDtJB object
     * 
     * @return The colSeq value
     */
    public String getColSeq() {
        return colSeq;
    }

    /**
     * Gets the colWidth attribute of the DynRepDtJB object
     * 
     * @return The colWidth value
     */
    public String getColWidth() {
        return colWidth;
    }

    /**
     * Gets the colFormat attribute of the DynRepDtJB object
     * 
     * @return The colFormat value
     */
    public String getColFormat() {
        return colFormat;
    }

    /**
     * Gets the colDisp attribute of the DynRepDtJB object
     * 
     * @return The colDisp value
     */
    public String getColDisp() {
        return colDisp;
    }

    /**
     * Gets the colName attribute of the DynRepDtJB object
     * 
     * @return The colName value
     */
    public String getColName() {
        return colName;
    }

    /**
     * Gets the colType attribute of the DynRepDtJB object
     * 
     * @return The colType value
     */
    public String getColType() {
        return colType;
    }

    /**
     * @return the Creator of the Form Field is returned
     */

    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The new creator value
     */

    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return modifiedBy
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     *            The new modifiedBy value
     */

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the IP Address of the Form Notification request
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The new ipAdd value
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * Returns the value of formId.
     */
    public String getFormId() {
        return formId;
    }

    /**
     * Sets the value of formId.
     * 
     * @param formId
     *            The value to assign formId.
     */
    public void setFormId(String formId) {
        this.formId = formId;
    }

    /**
     * Returns the value of formType.
     */
    public String getFormType() {
        return formType;
    }

    /**
     * Sets the value of formType.
     * 
     * @param formType
     *            The value to assign formType.
     */
    public void setFormType(String formType) {
        this.formType = formType;
    }

    /**
     * Returns the value of useUniqueId.
     */
    public String getUseUniqueId() {
        return useUniqueId;
    }

    /**
     * Sets the value of useUniqueId.
     * 
     * @param useUniqueId
     *            The value to assign useUniqueId.
     */
    public void setUseUniqueId(String useUniqueId) {
        this.useUniqueId = useUniqueId;
    }

    /**
     * Returns the value of useDataValue.
     */
    public String getUseDataValue() {
        return useDataValue;
    }

    /**
     * Sets the value of useDataValue.
     * 
     * @param useDataValue
     *            The value to assign useDataValue.
     */
    public void setUseDataValue(String useDataValue) {
        this.useDataValue = useDataValue;
    }

    // END OF THE GETTER AND SETTER METHODS

    /**
     * Constructor for the DynRepDtJB object
     */
    public DynRepDtJB() {
        Rlog.debug("dynrep", "DynRepDtJB.DynRepDtJB() ");
    }

    /**
     * Constructor for the DynRepDtJB object
     * 
     * @param id
     *            Description of the Parameter
     * @param repId
     *            Description of the Parameter
     * @param repCol
     *            Description of the Parameter
     * @param colSeq
     *            Description of the Parameter
     * @param colWidth
     *            Description of the Parameter
     * @param colFormat
     *            Description of the Parameter
     * @param colDisp
     *            Description of the Parameter
     * @param creator
     *            Description of the Parameter
     * @param modifiedBy
     *            Description of the Parameter
     * @param ipAdd
     *            Description of the Parameter
     * @param colName
     *            Description of the Parameter
     * @param colType
     *            Description of the Parameter
     * @param formId
     * @param formType
     * @param useUniqueId
     * @param useDataValue
     * 
     */
    public DynRepDtJB(int id, String repId, String repCol, String colSeq,
            String colWidth, String colFormat, String colDisp, String creator,
            String modifiedBy, String ipAdd, String colName, String colType,
            String formId, String formType, String useUniqueId,
            String useDataValue ,String calcSum, String calcPer) {
        setId(id);
        setRepId(repId);
        setRepCol(repCol);
        setColSeq(colSeq);
        setColWidth(colWidth);
        setColFormat(colFormat);
        setColDisp(colDisp);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setColName(colName);
        setColType(colType);
        setFormId(formId);
        setFormType(formType);
        setUseUniqueId(useUniqueId);
        setUseDataValue(useDataValue);
        setCalcSum(calcSum);
        setCalcPer(calcPer);
        setRepeatNumber(repeatNumber);
    }

    /**
     * Gets the dynRepDtDetails attribute of the DynRepDtJB object
     * 
     * @return The dynRepDtDetails value
     */
    public DynRepDtBean getDynRepDtDetails() {
        DynRepDtBean dynrepsk = null;
        try {
            DynRepDtAgentRObj dynrepAgentRObj = EJBUtil.getDynRepDtAgentHome();

            dynrepsk = dynrepAgentRObj.getDynRepDtDetails(this.id);
            Rlog.debug("dynrep",
                    "DynRepJB.getDynRepDtDetails() DynRepDtStateKeeper "
                            + dynrepsk);
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynrepDtJB getDynRepDtDetails" + e);
        }

        if (dynrepsk != null) {
            setId(dynrepsk.getId());
            setRepId(dynrepsk.getRepId());
            setRepCol(dynrepsk.getRepCol());
            setColSeq(dynrepsk.getColSeq());
            setColWidth(dynrepsk.getColWidth());
            setColFormat(dynrepsk.getColFormat());
            setColDisp(dynrepsk.getColDisp());
            setCreator(dynrepsk.getCreator());
            setModifiedBy(dynrepsk.getModifiedBy());
            setIpAdd(dynrepsk.getIpAdd());
            setColName(dynrepsk.getColName());
            setColType(dynrepsk.getColType());
            setFormId(dynrepsk.getFormId());
            setFormType(dynrepsk.getFormType());
            setUseUniqueId(dynrepsk.getUseUniqueId());
            setUseDataValue(dynrepsk.getUseDataValue());
            setCalcSum(dynrepsk.getCalcSum());
            setCalcPer(dynrepsk.getCalcPer());
            setRepeatNumber(dynrepsk.getRepeatNumber());

        }

        return dynrepsk;

    }

    /**
     * Sets the dynRepDtDetails attribute of the DynRepDtJB object
     * 
     * @return Description of the Return Value
     */
    public int setDynRepDtDetails() {
        int toReturn;
        try {

            DynRepDtAgentRObj dynrepAgentRObj = EJBUtil.getDynRepDtAgentHome();

            DynRepDtBean tempStateKeeper = new DynRepDtBean();

            tempStateKeeper = this.createDynRepDtStateKeeper();

            toReturn = dynrepAgentRObj.setDynRepDtDetails(tempStateKeeper);
            Rlog
                    .debug("dynrep",
                            "DynRepDtJB.setDynRepDtDetails() Outta thewre2"
                                    + dynrepAgentRObj + "statekeeper"
                                    + tempStateKeeper);
            this.setId(toReturn);
            Rlog.debug("dynrep", "DynRepDtJB.setDynRepDtDetails()");
            return toReturn;
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in setDynRepDtDetails() in dynrepJB "
                    + e);
            return -2;
        }
    }

    /**
     * @return a statekeeper object for the dynrepId Record with the current
     *         values of the bean
     */
    public DynRepDtBean createDynRepDtStateKeeper() {

        return new DynRepDtBean(id, repId, repCol, colSeq, colWidth, colFormat,
                colDisp, creator, modifiedBy, ipAdd, colName, colType, formId,
                formType, useUniqueId, useDataValue , calcSum,  calcPer,repeatNumber);
    }

    /**
     * Calls updateDynRep() of Lab Session Bean: LabAgentBean
     * 
     * @return The status as an integer
     */
    public int updateDynRepDt() {
        int output;
        try {
            DynRepDtAgentRObj dynrepAgentRObj = EJBUtil.getDynRepDtAgentHome();

            output = dynrepAgentRObj.updateDynRepDt(this
                    .createDynRepDtStateKeeper());
        } catch (Exception e) {
            Rlog.debug("dynrep",
                    "EXCEPTION IN SETTING dynrepDt DETAILS TO  SESSION BEAN"
                            + e);
            return -2;
        }
        return output;
    }

    /**
     * Description of the Method
     * 
     * @return Description of the Return Value
     */
    public int removeDynRepDt() {

        int output;

        try {

            DynRepDtAgentRObj dynrepAgentRObj = EJBUtil.getDynRepDtAgentHome();

            output = dynrepAgentRObj.removeDynRepDt(this.id);
            return output;
        } catch (Exception e) {
            Rlog.fatal("dynrep", "in DynRepDtJB -removeDynRepDt() method" + e);
            return -1;
        }

    }

    /**
     * Sets the allCols attribute of the DynRepDtJB object
     * 
     * @param repId
     *            The new allCols value
     * @param repCols
     *            The new allCols value
     * @param colSeqs
     *            The new allCols value
     * @param colWidths
     *            The new allCols value
     * @param colFormats
     *            The new allCols value
     * @param colDisps
     *            The new allCols value
     * @param usr
     *            The new allCols value
     * @param ipadd
     *            The new allCols value
     * @param colNames
     *            The new allCols value
     * @param colDataTypes
     *            The new allCols value
     * @param formId
     *            The new allCols value
     * @param formType
     *            The new allCols value
     * @param useUniqueId
     *            The new allCols value
     * @param useDataValues
     *            The new allCols value
     * 
     * @return Description of the Return Value
     */
    public int setAllCols(String repId, ArrayList repCols, ArrayList colSeqs,
            ArrayList colWidths, ArrayList colFormats, ArrayList colDisps,
            String usr, String ipadd, ArrayList colNames,
            ArrayList colDataTypes, String formId, String formType,
            ArrayList useUniqueIds, ArrayList useDataValues ,ArrayList calcSums, ArrayList calcPers ,ArrayList repeatNumbers) {
        int output = 0;
        try {

            DynRepDtAgentRObj dynrepdtAgentRObj = EJBUtil
                    .getDynRepDtAgentHome();

            // DynRepDtAgentRObj dynrepdtAgentRObj = dynrepdtAgentHome.create();

            output = dynrepdtAgentRObj
                    .setAllCols(repId, repCols, colSeqs, colWidths, colFormats,
                            colDisps, usr, ipadd, colNames, colDataTypes,
                            formId, formType, useUniqueIds, useDataValues , calcSums,  calcPers ,  repeatNumbers);
            Rlog.debug("dynrep", "DynRepDtJB.setAllCols after call");
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynRepDtJB.setAllCols " + e);
            return -2;
        }
        return output;
    }

    /**
     * Description of the Method
     * 
     * @param dyncolIds
     *            Description of the Parameter
     * @param repId
     *            Description of the Parameter
     * @param repCols
     *            Description of the Parameter
     * @param colSeqs
     *            Description of the Parameter
     * @param colWidths
     *            Description of the Parameter
     * @param colFormats
     *            Description of the Parameter
     * @param colDisps
     *            Description of the Parameter
     * @param usr
     *            Description of the Parameter
     * @param ipadd
     *            Description of the Parameter
     * @param colNames
     *            Description of the Parameter
     * @param colDataTypes
     *            Description of the Parameter
     * @param formId
     *            The new allCols value
     * @param formType
     *            The new allCols value
     * @param useUniqueId
     *            The new allCols value
     * @param useDataValues
     *            The new allCols value
     * 
     * @return Description of the Return Value
     */
    public int updateAllCols(ArrayList dyncolIds, String repId,
            ArrayList repCols, ArrayList colSeqs, ArrayList colWidths,
            ArrayList colFormats, ArrayList colDisps, String usr, String ipadd,
            ArrayList colNames, ArrayList colDataTypes, String formId,
            String formType, ArrayList useUniqueIds, ArrayList useDataValues ,ArrayList calcSums, ArrayList calcPers,ArrayList repeatNumbers) {
        int output = 0;
        try {

            DynRepDtAgentRObj dynrepdtAgentRObj = EJBUtil
                    .getDynRepDtAgentHome();

            output = dynrepdtAgentRObj.updateAllCols(dyncolIds, repId, repCols,
                    colSeqs, colWidths, colFormats, colDisps, usr, ipadd,
                    colNames, colDataTypes, formId, formType, useUniqueIds,
                    useDataValues , calcSums,  calcPers,  repeatNumbers);
            Rlog.debug("dynrep", "DynRepDtJB.updateAllCols after call");
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynRepDtJB.updateAllCols " + e);
            return -2;
        }
        return output;
    }

    /**
     * Description of the Method
     * 
     * @param repColIds
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public int removeAllCols(ArrayList repColIds) {
        int output = 0;
        try {
            Rlog.debug("dynrep", "DynRepDtJB.removeAllCol starting");
            DynRepDtAgentRObj dynrepdtAgentRObj = EJBUtil
                    .getDynRepDtAgentHome();

            output = dynrepdtAgentRObj.removeAllCols(repColIds);
            Rlog.debug("dynrep", "DynRepDtJB.removeAllCol after call");
        } catch (Exception e) {
            Rlog.fatal("dynrep", "Error in DynRepDtJB.removeAllCol " + e);
            return -2;
        }
        return output;
    }

}
