/**
 * 
 * Client side bean for Address
 * @author Ashwani Godara
 * 
 * @version 1.0 11/21/2011
 * 
 */

package com.velos.eres.web.studyIDEIND;

import java.util.Hashtable;

import com.velos.eres.business.common.StudyINDIDEDAO;
import com.velos.eres.business.studyINDIDE.impl.StudyINDIDEBean;
import com.velos.eres.service.studyINDIDEAgent.StudyINDIDEAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

public class StudyINDIDEJB {
	
	private int pkStudyINDIDE;
	
	private String fkStudy;
	
	private String typeIndIde;
	
	private String numberIndIde;
	
	private  String fkIndIdeGrantor;
	
	private  String fkIndIdeHolder;
	
	private  String fkProgramCode;
	
    private  String expandIndIdeAccess;
    
    private  String fkAccessType;
    
    private  String exemptINDIDE;
    
    private String creator;
    
    private String modifiedBy;
    
    private String ipAdd;
    
  //Added for [IND/IDE] NCI Division /Program for CSV Draft Export By:YOGENDRA PRATAP SINGH
    private String nciDevisionProgram;
	
    // Default no arguments constructor     
    public StudyINDIDEJB() {
        Rlog.debug("StudyINDIDEJB", "StudyINDIDEJB() ");
    }
    
    

	public int getPkStudyINDIDE() {
		return pkStudyINDIDE;
	}

	public void setPkStudyINDIDE(int pkStudyINDIDE) {
		this.pkStudyINDIDE = pkStudyINDIDE;
	}

	public String getFkStudy() {
		return fkStudy;
	}

	public void setFkStudy(String fkStudy) {
		this.fkStudy = fkStudy;
	}

	public String getTypeIndIde() {
		return typeIndIde;
	}

	public void setTypeIndIde(String typeIndIde) {
		this.typeIndIde = typeIndIde;
	}

	public String getNumberIndIde() {
		return numberIndIde;
	}

	public void setNumberIndIde(String numberIndIde) {
		this.numberIndIde = numberIndIde;
	}

	public String getFkIndIdeGrantor() {
		return fkIndIdeGrantor;
	}

	public void setFkIndIdeGrantor(String fkIndIdeGrantor) {
		this.fkIndIdeGrantor = fkIndIdeGrantor;
	}

	public String getFkIndIdeHolder() {
		return fkIndIdeHolder;
	}

	public void setFkIndIdeHolder(String fkIndIdeHolder) {
		this.fkIndIdeHolder = fkIndIdeHolder;
	}

	public String getFkProgramCode() {
		return fkProgramCode;
	}

	public void setFkProgramCode(String fkProgramCode) {
		this.fkProgramCode = fkProgramCode;
	}

	public String getExpandIndIdeAccess() {
		return expandIndIdeAccess;
	}

	public void setExpandIndIdeAccess(String expandINDIDEAccess) {
		this.expandIndIdeAccess = expandINDIDEAccess;
	}

	public String getFkAccessType() {
		return fkAccessType;
	}

	public void setFkAccessType(String fkAccessType) {
		this.fkAccessType = fkAccessType;
	}

	public String getExemptINDIDE() {
		return exemptINDIDE;
	}

	public void setExemptINDIDE(String exemptINDIDE) {
		this.exemptINDIDE = exemptINDIDE;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getIpAdd() {
		return ipAdd;
	}

	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}
    
	public StudyINDIDEBean createStudyIndIdeStateKeeper() {

    	Rlog.debug("StudyINDIDEJB", "createStudyINDIDEStateKeeper");

        return new StudyINDIDEBean(        		
        		
        		this.pkStudyINDIDE,
        		this.fkStudy,
        		this.typeIndIde,
        		this.numberIndIde,
        		this.fkIndIdeGrantor,
        		this.fkIndIdeHolder,
        		this.fkProgramCode,
        		this.expandIndIdeAccess,
        		this.fkAccessType,
        		this.exemptINDIDE,
        		this.creator,
        		this.modifiedBy,
        		this.ipAdd
        );


    }


	 public StudyINDIDEDAO getStudyINDIDEInfo(int studyId) {
	        try {
	        	StudyINDIDEAgentRObj studyINDIDEAgent = EJBUtil
	                    .getStudyINDIDEAgentHome();
	            return studyINDIDEAgent.getStudyINDIDEInfo(studyId);
	        } catch (Exception e) {
	            Rlog.fatal("studystatus",
	                    "Error in getStudyStatusDesc() in SectionStatusJB " + e);
	            return null;
	        }
	    }
	
	 // Deletes IND/IDE records 
	 public int deleteIndIdeInfo(int indIdeId,Hashtable<String, String> auditInfo) {
	        try {
	        	StudyINDIDEAgentRObj studyINDIDEAgent = EJBUtil
	                    .getStudyINDIDEAgentHome();
	            return studyINDIDEAgent.deleteIndIdeInfo(indIdeId,auditInfo);
	        } catch (Exception e) {
	            Rlog.fatal("StudyINDIDEJB",
	                    "Error in deleteIndIdeInfo() in StudyINDIDEJB " + e);
	            return 0;
	        }
	    }
	// Updates IND/IDE records
	 public int updateIndIdeInfo() {
	        try {
	        	StudyINDIDEAgentRObj studyINDIDEAgent = EJBUtil
	                    .getStudyINDIDEAgentHome();
	            return studyINDIDEAgent.updateIndIdeInfo(this.createStudyIndIdeStateKeeper());
	        } catch (Exception e) {
	            Rlog.fatal("StudyINDIDEJB",
	                    "Error in updateIndIdeInfo() in StudyINDIDEJB " + e);
	            return 0;
	        }
	    }
	 
	// Enter new IND/IDE record for a study
	 public int setIndIdeInfo() {
	        try {
	        	StudyINDIDEAgentRObj studyINDIDEAgent = EJBUtil
	                    .getStudyINDIDEAgentHome();
	            return studyINDIDEAgent.setIndIdeInfo(this.createStudyIndIdeStateKeeper());
	        } catch (Exception e) {
	            Rlog.fatal("StudyINDIDEJB",
	                    "Error in setIndIdeInfo() in StudyINDIDEJB " + e);
	            return 0;
	        }
	    }


	//Set [IND/IDE] NCI Division /Program for CSV Draft Export 
	public String getNciDevisionProgram() {
		return nciDevisionProgram;
	}
	
	public void setNciDevisionProgram(String nciDevisionProgram) {
		this.nciDevisionProgram = nciDevisionProgram;
	}
	 	 

	 

}
