/*

 * Classname : CtrltabJB.java

 * 

 * Version information

 *

 * Date 05/18/2006

 * 

 * Copyright notice: Aithent

 */

package com.velos.eres.web.ctrltab;

import com.velos.eres.business.ctrltab.impl.CtrltabBean;
import com.velos.eres.service.ctrltabAgent.CtrltabAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;


/**
 * 
 * Client side bean for Ctrltab
 * 
 * @author Manimaran
 * 
 */

public class CtrltabJB {
	
	 /**
     * 
     * the ctrltab Id
     * 
     */
	
	private int ctabId;
	
	 /**
     * 
     * the ctrltab Controlvalue
     * 
     */
		
	private String ctabCtrlValue;
	
	/**
     * 
     * the ctrltab ControlDesc
     * 
     */
		
	private String ctabCtrlDesc;
	
	/**
     * 
     * the ctrltab ControlKey
     * 
     */
	
	private String ctabCtrlKey;
	
	/**
     * 
     * the ctrltab ControlSeq
     * 
     */
	
	private String ctabCtrlSeq; 
	
	/*
     * 
     * creator
     * 
     */
	
	private String creator;
	
	 /*
     * 
     * last modified by
     * 
     */
	
	
	private String modifiedBy;
	
	/*
     * 
     * IP Address
     * 
     */
	
	private String ipAdd;
	

	public int getCtabId() {

        return this.ctabId;

    }

    public void setCtabId(int ctabId) {

        this.ctabId = ctabId;

    }
    
    public String getCtabCtrlValue() {
    	
    	return this.ctabCtrlValue;
    }
    
    public void setCtabCtrlValue(String ctabCtrlValue){
    	
    	this.ctabCtrlValue = ctabCtrlValue;
    }
    
    public String getCtabCtrlDesc() {
    	
    	return this.ctabCtrlDesc;
    	
    }
    
    public void setCtabCtrlDesc(String ctabCtrlDesc) {
    	
    	this.ctabCtrlDesc = ctabCtrlDesc;
    	
    }
    
    public String  getCtabCtrlKey() {
    	
    	return this.ctabCtrlKey;
    	
    }
    
    public void setCtabCtrlKey(String ctabCtrlKey) {
    	
    	this.ctabCtrlKey = ctabCtrlKey;
    	
    }
    
    public String getCtabCtrlSeq() {
    	
    	return this.ctabCtrlSeq;
    
    }
    
    public  void setCtabCtrlSeq(String ctabCtrlSeq) {
    	
    	this.ctabCtrlSeq = ctabCtrlSeq;
    
    }
    
    
    public String getCreator() {

        return this.creator;

    }

    public void setCreator(String creator) {

        this.creator = creator;

    }

    
    public String getModifiedBy() {

        return this.modifiedBy;

    }

    

    public void setModifiedBy(String modifiedBy) {

        this.modifiedBy = modifiedBy;

    }

   

    public String getIpAdd() {

        return this.ipAdd;

    }

  
    public void setIpAdd(String ipAdd) {

        this.ipAdd = ipAdd;

    }

    // End of setter getter methods here
 
    
    public CtrltabJB(int ctabId) {
    	
    	setCtabId(ctabId);
    }
   
    public CtrltabJB() {

        Rlog.debug("ctrltab", "CtrltabJB");

    }
    
    public CtrltabJB(int ctabId, String ctabCtrlValue, String ctabCtrlDesc, String ctabCtrlKey,
	
	String ctabCtrlSeq, String creator , String modifiedBy , String ipAdd) {

    	setCtabId(ctabId);

        setCtabCtrlValue(ctabCtrlValue);
  
        setCtabCtrlDesc(ctabCtrlDesc);
        
        setCtabCtrlKey(ctabCtrlKey);
        
        setCtabCtrlSeq(ctabCtrlSeq);
        
        setCreator(creator);
        
        setModifiedBy(modifiedBy);
        
        setIpAdd(ipAdd);
        
        Rlog.debug("ctrltab", "CtrltabJB.CtrltabJB(all parameters)");
    
    }
    
    
    public CtrltabBean getCtrltabDetails() {

        CtrltabBean ctsk = null;

        try {

            CtrltabAgentRObj ctrltabAgent = EJBUtil.getCtrltabAgentHome();
            ctsk = ctrltabAgent.getCtrltabDetails(this.ctabId);

        } catch (Exception e) {

            Rlog.debug("ctrltab", "Error in getCtrltabDetails() in CtrltabJB "
                    + e);

        }
            
        if (ctsk != null)

        {

            this.ctabId = ctsk.getCtabId();

            this.ctabCtrlValue = ctsk.getCtabCtrlValue();

            this.ctabCtrlDesc = ctsk.getCtabCtrlDesc();

            this.ctabCtrlKey = ctsk.getCtabCtrlKey();

            this.ctabCtrlSeq = ctsk.getCtabCtrlSeq();

            this.creator = ctsk.getCreator();

            this.modifiedBy = ctsk.getModifiedBy();

            this.ipAdd = ctsk.getIpAdd();

        }

        return ctsk;

    }
    
    
    public void setCtrltabDetails()

    {

        try {

            CtrltabAgentRObj ctrltabAgent = EJBUtil.getCtrltabAgentHome();

            this.setCtabId(ctrltabAgent.setCtrltabDetails(this
                    .createCtrltabStateKeeper()));

        } catch (Exception e) {

            Rlog.fatal("ctrltab", "Error in setCtrltabDetails() in CtrltabJB "
                    + e);

        }

    }
    
    
    public int updateCtrltab()

    {

        int output;

        try {

            CtrltabAgentRObj ctrltabAgentRObj = EJBUtil.getCtrltabAgentHome();

            output = ctrltabAgentRObj.updateCtrltab(this
                    .createCtrltabStateKeeper());

        }

        catch (Exception e) {
            Rlog
                    .fatal("ctrltab",
                            "EXCEPTION IN SETTING Ctrltab DETAILS TO  SESSION BEAN"
                                    + e);
            return -2;

        }

        return output;

    }

    
    public CtrltabBean createCtrltabStateKeeper()
    {

        return new CtrltabBean(ctabId, ctabCtrlValue, ctabCtrlDesc, ctabCtrlKey,
        		
        		 ctabCtrlSeq, creator ,  modifiedBy , ipAdd);

    }


} 
    
    
    
  
    