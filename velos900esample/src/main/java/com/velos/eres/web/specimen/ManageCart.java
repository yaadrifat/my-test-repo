package com.velos.eres.web.specimen;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.velos.eres.business.common.CodeDao;
import com.velos.eres.business.common.StorageDao;
import com.velos.eres.business.common.StorageKitDao;
import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.web.specimenStatus.SpecimenStatusJB;
import com.velos.eres.web.storage.StorageJB;
import com.velos.eres.web.storageKit.StorageKitJB;
import com.velos.eres.web.storageStatus.StorageStatusJB;
/**
 * This class manages the functionality starting from add to cart to prepare specimen.
 *  
 * @author Bikash
 *
 */
public class ManageCart {	
	public static HashMap paramMap;
	/**
	 * public constructor.
	 */
	public ManageCart(){
		paramMap=new HashMap();
	}
	/**
	 * Addition of items to cart.
	 * 
	 * @param aPreparationCartDto
	 * @param bPreparationCartDto
	 * @return
	 */
	public static PreparationCartDto addDtoToCart(PreparationCartDto aPreparationCartDto,
			PreparationCartDto bPreparationCartDto){
		aPreparationCartDto.setPersonPIdList(bPreparationCartDto.getPersonPIdList());
		aPreparationCartDto.setStudyNumberList(bPreparationCartDto.getStudyNumberList());
		aPreparationCartDto.setCalendarList(bPreparationCartDto.getCalendarList());
		aPreparationCartDto.setVisitList(bPreparationCartDto.getVisitList());
		aPreparationCartDto.setScheduledDateList(bPreparationCartDto.getScheduledDateList());
		aPreparationCartDto.setEventList(bPreparationCartDto.getEventList());
		aPreparationCartDto.setStorageKitList(bPreparationCartDto.getStorageKitList());
		aPreparationCartDto.setPkStorageList(bPreparationCartDto.getPkStorageList());
		aPreparationCartDto.setFkSchEventList(bPreparationCartDto.getFkSchEventList());
		aPreparationCartDto.setFkStudyList(bPreparationCartDto.getFkStudyList());
		aPreparationCartDto.setFkVisitList(bPreparationCartDto.getFkVisitList());
		aPreparationCartDto.setFkPerList(bPreparationCartDto.getFkPerList());
		aPreparationCartDto.setFkSiteList(bPreparationCartDto.getFkSiteList());
		return aPreparationCartDto;
	}
	/**
	 * Add data in form of string to cart.
	 * 
	 * @param aPreparationCartDto
	 * @param patientId
	 * @param studyNumber
	 * @param calendar
	 * @param visit Visit name.
	 * @param scheduledDate
	 * @param event
	 * @param storageKit
	 * @param pkStorage
	 * @param fkSchEvent
	 * @param fkVisit
	 * @param fkStudy
	 * @param fkPer
	 * @param fkSite
	 */
	public static void addStringToCart(PreparationCartDto aPreparationCartDto,
			String patientId,String studyNumber,String calendar,String visit
			,String scheduledDate,String event,String storageKit
			,String pkStorage,String fkSchEvent,String fkVisit
			,String fkStudy,String fkPer,String fkSite){
		aPreparationCartDto.getPersonPIdList().add(patientId);
		aPreparationCartDto.getStudyNumberList().add(studyNumber);
		aPreparationCartDto.getCalendarList().add(calendar);
		aPreparationCartDto.getVisitList().add(visit);
		aPreparationCartDto.getScheduledDateList().add(scheduledDate);
		aPreparationCartDto.getEventList().add(event);
		aPreparationCartDto.getStorageKitList().add(storageKit);
		aPreparationCartDto.getPkStorageList().add(pkStorage);
		aPreparationCartDto.getFkSchEventList().add(fkSchEvent);
		aPreparationCartDto.getFkVisitList().add(fkVisit);
		aPreparationCartDto.getFkStudyList().add(fkStudy);
		aPreparationCartDto.getFkPerList().add(fkPer);
		aPreparationCartDto.getFkSiteList().add(fkSite);

	}
	/**
	 * Checking for duplicate records in destination data transfer object.
	 * @param pPreparationCartSourceDto
	 * @param pPreparationCartDestDto
	 * @return PreparationCartDto
	 */
	public static PreparationCartDto checkDuplicate(PreparationCartDto pPreparationCartSourceDto,
			PreparationCartDto pPreparationCartDestDto){
		boolean isAdded = false;boolean isMatched = false;
		PreparationCartDto aTempDto=new PreparationCartDto();       
		for(int i=0;i<pPreparationCartSourceDto.getPersonPIdList().size();i++){
			isMatched = false;
			String compareStringFirst = pPreparationCartSourceDto.getPersonPIdList().get(i)
			+ pPreparationCartSourceDto.getCalendarList().get(i)
			+ pPreparationCartSourceDto.getVisitList().get(i)+pPreparationCartSourceDto.getEventList().get(i)
			+pPreparationCartSourceDto.getStorageKitList().get(i)
			+pPreparationCartSourceDto.getScheduledDateList().get(i);
			for(int j=0;j<pPreparationCartDestDto.getPersonPIdList().size();j++){	
				String compareStringSecond = pPreparationCartDestDto.getPersonPIdList().get(j)
				+ pPreparationCartDestDto.getCalendarList().get(j)
				+ pPreparationCartDestDto.getVisitList().get(j)+pPreparationCartDestDto.getEventList().get(j)
				+pPreparationCartDestDto.getStorageKitList().get(j)
				+pPreparationCartDestDto.getScheduledDateList().get(j);
				if(compareStringFirst.equals(compareStringSecond)){
					isMatched = true;
					break;
				}
			}
			if(!isMatched){
				addStringToCart(aTempDto,pPreparationCartSourceDto.getPersonPIdList()
						.get(i),pPreparationCartSourceDto.getStudyNumberList().get(i),
						pPreparationCartSourceDto.getCalendarList().get(i),pPreparationCartSourceDto.getVisitList().get(i)
						,pPreparationCartSourceDto.getScheduledDateList().get(i),pPreparationCartSourceDto
						.getEventList().get(i),pPreparationCartSourceDto.getStorageKitList().get(i)
						,pPreparationCartSourceDto.getPkStorageList().get(i)
						,pPreparationCartSourceDto.getFkSchEventList().get(i),pPreparationCartSourceDto.getFkVisitList().get(i),
						pPreparationCartSourceDto.getFkStudyList().get(i),pPreparationCartSourceDto.getFkPerList().get(i),
						pPreparationCartSourceDto.getFkSiteList().get(i));
				isAdded = true;
			}
		}
		if(!isAdded){
			aTempDto = null;
		}
		return aTempDto;
	}

	public static PreparationCartDto remove(PreparationCartDto pDto,int index){
		pDto.remove(index);
		return pDto;
	}

	/**
	 * Create Storages and Prepare Specimen.
	 * 
	 * @param preparationCartDto
	 */
	public static String prepareSpecimen(PreparationCartDto preparationCartDto,List<String> printIndices){		
		StorageJB  storageClient;		
		ArrayList<StorageJB> theStorageList = new ArrayList<StorageJB>();
		ArrayList<SpecimenJB> tempList;
		
		String selPks = "";
		for(int index=0;index<preparationCartDto.getPkStorageList().size();index++){
			storageClient= new StorageJB();
			storageClient.setPkStorage(Integer.parseInt(preparationCartDto.getPkStorageList().get(index)));
			storageClient.getStorageDetails();
			int pkTempStorage= storageClient.getPkStorage();
			theStorageList= formAndSaveStorages(storageClient);
			tempList = formAndSaveSpecimen(pkTempStorage, theStorageList,preparationCartDto,index);
			if(printIndices.contains(""+index)){			    
			      for(SpecimenJB tempSpecimen:tempList){
			    	  selPks = selPks + "," +tempSpecimen.getPkSpecimen();
			      }
			}
			        
		}
		selPks = selPks + ",";
		preparationCartDto.removeAllElements();
        return selPks;
	}

	
	/**
	 * Forms Specimen client bean.
	 * 
	 * @param storageKitInfo
	 * @param aChildStorageB
	 * @param isParentStorKit
	 * @param parentPkStorage
	 * @param fk_specimen
	 * @param preparationCartDto
	 * @param index
	 * @return
	 */
	private static SpecimenJB setSpecimenBean(StorageKitDao storageKitInfo, StorageJB aChildStorageB, boolean isParentStorKit,
			Integer parentPkStorage, String fk_specimen,PreparationCartDto preparationCartDto,int index) {
		SpecimenJB specJB = new SpecimenJB();
		specJB.setCreator((String)paramMap.get("userId"));
		specJB.setIpAdd((String)paramMap.get("ipAdd"));	
		specJB.setFkSpec(fk_specimen);
		specJB.setFkAccount(aChildStorageB.getAccountId());
		String specExpectedQty =((storageKitInfo.getDefSpecQuants().get(0)) == null)?"":(storageKitInfo.getDefSpecQuants().get(0)).toString();
		String specQuantUnits =((storageKitInfo.getDefSpecQuantUnits().get(0)) == null)?"":(storageKitInfo.getDefSpecQuantUnits().get(0)).toString();
		String specDisposition =((storageKitInfo.getKitSpecDispositions().get(0)) == null)?"":(storageKitInfo.getKitSpecDispositions().get(0)).toString();
		String specEnvtCons =((storageKitInfo.getKitSpecEnvtCons().get(0)) == null)?"":(storageKitInfo.getKitSpecEnvtCons().get(0)).toString();	
		String specType =((storageKitInfo.getDefSpecimenTypes().get(0)) == null)?"":(storageKitInfo.getDefSpecimenTypes().get(0)).toString();		
		specJB.setSpecExpectQuant(specExpectedQty);
		specJB.setSpecExpectedQUnits(specQuantUnits);
		specJB.setSpecDisposition(specDisposition);
		specJB.setSpecEnvtCons(specEnvtCons);
		specJB.setSpecType(specType);
		specJB.setFkStorage(aChildStorageB.getPkStorage()+""); 
		specJB.setFkStudy(preparationCartDto.getFkStudyList().get(index));
		specJB.setFkSchEvents(preparationCartDto.getFkSchEventList().get(index));
		specJB.setFkPer(preparationCartDto.getFkPerList().get(index));
		specJB.setFkSite(preparationCartDto.getFkSiteList().get(index));
		specJB.setFkVisit(preparationCartDto.getFkVisitList().get(index));
		String collDtVar;
		//collection date updated,BK,Apr1-2011
		if (! StringUtil.isEmpty(preparationCartDto.getScheduledDateList().get(index)))
	 	{
	 		collDtVar = preparationCartDto.getScheduledDateList().get(index) + " " + "00:00:00";;
	 	}else{
	 		collDtVar = "";
	 	}
		specJB.setSpecCollDate(collDtVar);
		return specJB;
	}


	/**
	 * Fetches Storage kit data.
	 * 
	 * @param childPkStorage
	 * @return
	 */
	private static StorageKitDao getStorageKitInfo(Integer childPkStorage) {
		StorageKitJB strKitBean = new StorageKitJB();
		StorageKitDao strKitDao= new StorageKitDao();
		strKitDao= strKitBean.getStorageKitAttributes(childPkStorage);
		return strKitDao;
	}


	/**
	 * Set the Storage status bean for creation of new status for storage.
	 * 
	 * @param storageClient
	 * @return StorageStatusJB
	 */
	private static StorageStatusJB setStorageStaus(StorageJB storageClient) {
		CodeDao cd1 = new CodeDao();
		int fkCodelstSpecimenStat = cd1.getCodeId("storage_stat", "Available");		 
		StorageStatusJB storageStatB = new StorageStatusJB();
		storageStatB.setFkStorage(storageClient.getPkStorage()+"");
		storageStatB.setSsStartDate(DateUtil.dateToString(new Date()));
		storageStatB.setFkCodelstStorageStat(fkCodelstSpecimenStat+"");
		storageStatB.setSsNotes("");
		storageStatB.setFkUser("");
		storageStatB.setFkStudy("");
		storageStatB.setIpAdd((String)paramMap.get("ipAdd"));	
		storageStatB.setCreator((String)paramMap.get("userId"));
		return storageStatB;

	}
	private static StorageStatusJB setStorageStausOcc(StorageJB storageClient) {
		CodeDao cd1 = new CodeDao();
		int fkCodelstSpecimenStat = cd1.getCodeId("storage_stat", "Occupied");		 
		StorageStatusJB storageStatB = new StorageStatusJB();
		storageStatB.setFkStorage(storageClient.getPkStorage()+"");
		storageStatB.setSsStartDate(DateUtil.dateToString(new Date()));
		storageStatB.setFkCodelstStorageStat(fkCodelstSpecimenStat+"");
		storageStatB.setSsNotes("");
		storageStatB.setFkUser("");
		storageStatB.setFkStudy("");
		storageStatB.setIpAdd((String)paramMap.get("ipAdd"));	
		storageStatB.setCreator((String)paramMap.get("userId"));
		return storageStatB;

	}
	/**
	 *Forms number of storages based on storage kit template and saves them.
	 * 
	 * @param pStorageClient instance of storageJB.
	 */
	private static ArrayList<StorageJB> formAndSaveStorages(StorageJB  pStorageClient){	
		CodeDao cd = new CodeDao();
		StorageDao storageDao = pStorageClient.getAllChildren(pStorageClient.getPkStorage());
		StorageStatusJB aStorageStatusClient = new StorageStatusJB();		
		ArrayList <Integer> pk_Storages = storageDao.getPkStorages();
		pStorageClient.setFkStorageKit(""+pStorageClient.getPkStorage());
		pStorageClient.setPkStorage(0);
		pStorageClient.setStorageId(pStorageClient.getStorageIdAuto());
		pStorageClient.setTemplate("0");
		//AK:Fixed BUg#5807 (16thMar2011)	
		pStorageClient.setStorageDim1CellNum(""+pk_Storages.size());
		pStorageClient.setStorageDim2CellNum("1");
		//AK:Fixed BUG#5807 and 5958(25th Mar2011)
		pStorageClient.setStorageDim1Naming("azz");
		pStorageClient.setStorageDim1Order("LR");
		pStorageClient.setStorageDim2Naming("1n");
		pStorageClient.setStorageDim2Order("BF");	
		//AK:Fixed BUG#6031(15thApr2011)
		String ItemCode = cd.getCodeId("cap_unit","Items")+"";
		pStorageClient.setFkCodelstCapUnits(ItemCode);
		pStorageClient.setFkCodelstStorageType(pStorageClient.getFkCodelstChildStype());
		pStorageClient.setFkCodelstChildStype(pStorageClient.getFkCodelstChildStype());//AK:Fixed BUG#6032 (19thApr11)
		pStorageClient.setCreator((String)paramMap.get("userId"));
		pStorageClient.setIpAdd((String)paramMap.get("ipAdd"));
		pStorageClient.setModifiedBy(null);	
		pStorageClient.setStorageCapNum(""+pk_Storages.size());		
		int test = pStorageClient.setStorageDetails();	
		System.out.println("New generated Parent storage Number "+test);
		aStorageStatusClient=setStorageStaus(pStorageClient);
		aStorageStatusClient.setStorageStatusDetails();
		ArrayList<StorageJB> storageClienList = new ArrayList();
		StorageJB tempStorageClient;
		StorageStatusJB tempStorageStatusClient;
		for(int i =0; i< pk_Storages.size(); i++){
			tempStorageClient = new StorageJB();
			tempStorageClient.setPkStorage(pk_Storages.get(i));
			tempStorageClient.getStorageDetails();
			tempStorageClient.setPkStorage(0);
			tempStorageClient.setFkStorage(""+pStorageClient.getPkStorage());
			tempStorageClient.setTemplate("0");
			tempStorageClient.setStorageId(tempStorageClient.getStorageIdAuto());
			tempStorageClient.setStorageCapNum("1");
			tempStorageClient.setModifiedBy(null);	
			tempStorageClient.setCreator((String)paramMap.get("userId"));
			tempStorageClient.setIpAdd((String)paramMap.get("ipAdd"));
			//AK:Fixed BUg#5807 (16thMar2011)	
			tempStorageClient.setStorageCoordinateX(""+(i+1));
			tempStorageClient.setStorageCoordinateY("1");
			//AK:Fixed BUG#5807 and 5958 (25th Mar2011)
			tempStorageClient.setStorageDim1Naming("azz");
			tempStorageClient.setStorageDim1Order("LR");
			tempStorageClient.setStorageDim2Naming("1n");
			tempStorageClient.setStorageDim2Order("BF");
			//AK:Fixed BUG#6031(15thApr2011)
			ItemCode = cd.getCodeId("cap_unit","Items")+"";
			tempStorageClient.setFkCodelstCapUnits(ItemCode);
			
			int newGeneratedNum= tempStorageClient.setStorageDetails();	
			System.out.println("New generated child storage Number "+newGeneratedNum);
			storageClienList.add(tempStorageClient);
			tempStorageStatusClient = new StorageStatusJB();
			tempStorageStatusClient=setStorageStaus(tempStorageClient);
			tempStorageStatusClient.setStorageStatusDetails();

		}
		return storageClienList;
	}
	/**
	 * Forms number of specimen based on storage kit template and saves them.
	 * 
	 * @param pStorageClient
	 * @param pStorageList
	 * @param preparationCartDto
	 * @param index
	 * @return ArrayList
	 */
	private static ArrayList<SpecimenJB> formAndSaveSpecimen(int  pkTempStoage,ArrayList<StorageJB>  pStorageList,
			PreparationCartDto preparationCartDto,int index){
		StorageStatusJB aStorageStatusClient=new StorageStatusJB();
		ArrayList<SpecimenJB> aSpecimenList = new ArrayList<SpecimenJB>();	
		StorageDao aStorageDao = new StorageJB().getImmediateChildren(pkTempStoage);
		ArrayList anImmediateChildStorageList = aStorageDao.getPkStorages();	
		SpecimenJB parentSpecJB;
		SpecimenJB childSpecJB;
		String specProcTypeList="";
		int j=0;
		for(int i =0; i< anImmediateChildStorageList.size(); i++){
			int firstLevelPkStorage =(Integer) anImmediateChildStorageList.get(i);		
			//Added method to fetch the Storage Kit Data
			StorageKitDao storageKitInfo=getStorageKitInfo(firstLevelPkStorage);
			//Set ER_Speciment for main Parent
			parentSpecJB=setSpecimenBean(storageKitInfo,pStorageList.get(j),true,firstLevelPkStorage,null,preparationCartDto,index);
			parentSpecJB.setSpecimenId(parentSpecJB.getSpecimenIdAuto());
			int genaratedPkSpecimen=parentSpecJB.setSpecimenDetails();
			aStorageStatusClient=setStorageStausOcc(pStorageList.get(j));
			aStorageStatusClient.setStorageStatusDetails();
			j++;
			aSpecimenList.add(parentSpecJB);
			createSpecimenStatus(parentSpecJB, preparationCartDto, index);
			//AK:enhancement #INVP2.8.1 (16thMar2011)	
			specProcTypeList=((storageKitInfo.getDefProcTypes().get(0)) == null)?"":(storageKitInfo.getDefProcTypes().get(0)).toString();
			createProcessingTypeStatus(parentSpecJB, preparationCartDto, index,specProcTypeList);
			//Get the Children of the main Storage		
			StorageDao strChild2Dao = new StorageJB().getAllChildren(firstLevelPkStorage);
			ArrayList secondLevelChildStorageList = strChild2Dao.getPkStorages();
			for(int k =0; k< secondLevelChildStorageList.size(); k++){
				String specimenId;
				if(k<10)
				{
					specimenId = parentSpecJB.getSpecimenId()+"_0"+(k+1);
				}
				else{
					specimenId = parentSpecJB.getSpecimenId()+"_"+(k+1);
				}
				int secondLevelPkStorage =(Integer) secondLevelChildStorageList.get(k);			
				StorageKitDao strChildrenKitInfo=getStorageKitInfo(secondLevelPkStorage);
				childSpecJB=setSpecimenBean(strChildrenKitInfo,pStorageList.get(j),false,firstLevelPkStorage,genaratedPkSpecimen+"",					
				preparationCartDto,index);
				aStorageStatusClient=setStorageStausOcc(pStorageList.get(j));
				aStorageStatusClient.setStorageStatusDetails();
				j++;
				childSpecJB.setSpecimenId(specimenId);
				int genaratedNumber=childSpecJB.setSpecimenDetails();
				aSpecimenList.add(childSpecJB);
				createSpecimenStatus(childSpecJB, preparationCartDto, index);
				//AK:enhancement #INVP2.8.1 (16thMar2011)
				specProcTypeList=((strChildrenKitInfo.getDefProcTypes().get(0)) == null)?"":(strChildrenKitInfo.getDefProcTypes().get(0)).toString();
				createProcessingTypeStatus(childSpecJB, preparationCartDto, index,specProcTypeList);
				System.out.println("New generated specimen Number"+genaratedNumber);

			}
		}
		return aSpecimenList;
	}
	/**
	 * Creates the specimen status.
	 * 
	 * @param pSpecimenB
	 * @param preparationCartDto
	 * @param index
	 * @return SpecimenStatusJB
	 */
	private static SpecimenStatusJB createSpecimenStatus(SpecimenJB pSpecimenB,PreparationCartDto preparationCartDto,int index){
		CodeDao cd = new CodeDao();
		int pending_Stat = cd.getCodeId("specimen_stat","Pending");
		//today
		Date date = new java.util.Date();
		String sysDate = DateUtil.dateToString(date);
		sysDate = sysDate + " " + "00:00:00";

		SpecimenStatusJB specChildStatB = new SpecimenStatusJB();
		specChildStatB.setFkSpecimen(""+pSpecimenB.getPkSpecimen());
		specChildStatB.setSsDate(sysDate);
		specChildStatB.setFkCodelstSpecimenStat(pending_Stat + "");
		specChildStatB.setSsQuantity("");
		specChildStatB.setSsQuantityUnits("");
		specChildStatB.setSsAction("");
		specChildStatB.setFkStudy(preparationCartDto.getFkStudyList().get(index));
		specChildStatB.setFkUserRecpt("");
		specChildStatB.setSsTrackingNum("");
		specChildStatB.setSsNote("");
		specChildStatB.setSsStatusBy((String)paramMap.get("userId"));
		specChildStatB.setCreator((String)paramMap.get("userId"));
		specChildStatB.setIpAdd((String)paramMap.get("ipAdd"));
		specChildStatB.setModifiedBy(null);		
		specChildStatB.setSpecimenStatusDetails();
		return specChildStatB;
	}
	//End here
	//AK:Added for enhancement #INVP2.8.1 (16thMar2011)
	private static void createProcessingTypeStatus(SpecimenJB pSpecimenB,PreparationCartDto preparationCartDto,int index, String specProcTypeList){
		CodeDao cd = new CodeDao();
		String [] procTypeCodelist= specProcTypeList.split(",");
		for(int i=0; i<procTypeCodelist.length; i++){
			if(!procTypeCodelist[i].equals("") && ! procTypeCodelist[i].equals("0") ){
				int pending_Stat = cd.getCodeId("specimen_stat","Processed"); //AK:Fixed BUG#6029 (19thApr11)
				Date date = new java.util.Date();
				String sysDate = DateUtil.dateToString(date);
				sysDate = sysDate + " " + "00:00:00";
				SpecimenStatusJB specChildStatB = new SpecimenStatusJB();
				specChildStatB.setFkSpecimen(""+pSpecimenB.getPkSpecimen());
				specChildStatB.setSsDate(sysDate);
				specChildStatB.setFkCodelstSpecimenStat(""+pending_Stat);
				specChildStatB.setSsProcType(procTypeCodelist[i]);
				specChildStatB.setSsProcDate(sysDate);
				specChildStatB.setSsQuantity("");
				specChildStatB.setSsQuantityUnits("");
				specChildStatB.setSsAction("");
				specChildStatB.setFkStudy(preparationCartDto.getFkStudyList().get(index));
				specChildStatB.setFkUserRecpt("");
				specChildStatB.setSsTrackingNum("");
				specChildStatB.setSsNote("");
				specChildStatB.setSsStatusBy((String)paramMap.get("userId"));
				specChildStatB.setCreator((String)paramMap.get("userId"));
				specChildStatB.setIpAdd((String)paramMap.get("ipAdd"));
				specChildStatB.setModifiedBy(null);		
			    int statuslist=specChildStatB.setSpecimenStatusDetails();
			   System.out.println("Status record generated for Processing type " +statuslist);
			}
		}
		
	}
}
