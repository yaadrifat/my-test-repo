/*

 * Classname : UserSiteJB
 * 
 * Version information: 1.1
 *
 * Date: 01/27/2012
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Yogesh
 */

package com.velos.eres.web.usrSiteGrp;

/* IMPORT STATEMENTS */
import com.velos.eres.business.common.UserSiteGroupDao;
import com.velos.eres.service.usrSiteGrpAgent.UsrSiteGrpAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */


public class UsrSiteGrpJB {

	 /**
     * the UserSiteGroup Id
     */
    public int userSiteGrp;
    
    /**
     * the fkuser Id
     */
    public String fkuserId;
    /**
     * the fkgroupIds Id
     */
    public String fkGrpIds;
    
    /**
     * the fkUserSite Id
     */
    public String fkUserSite;
    
    /**
     * the creater 
     */
    public String creator;
    
    /**
     * the lastmodifiedby Id
     */
    public String modifiedBy;
    
    /**
     * the ip address Id
     */
    public String ipAdd;

    // GETTER SETTER METHODS

    public int getUserSiteGrp() {
		return userSiteGrp;
	}

	public void setUserSiteGrp(int userSiteGrp) {
		this.userSiteGrp = userSiteGrp;
	}

	public String getFkuserId() {
		return fkuserId;
	}

	public void setFkuserId(String fkuserId) {
		this.fkuserId = fkuserId;
	}

	public String getFkGrpIds() {
		return fkGrpIds;
	}

	public void setFkGrpIds(String fkGrpIds) {
		this.fkGrpIds = fkGrpIds;
	}

	public String getFkUserSite() {
		return fkUserSite;
	}

	public void setFkUserSite(String fkUserSite) {
		this.fkUserSite = fkUserSite;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getIpAdd() {
		return ipAdd;
	}

	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}

    // end of getter setter methods here


	/**
     * 
     * Default Constructor
     * 
     * 
     * 
     */

    public UsrSiteGrpJB()

    {

        Rlog.debug("usrgrp", "UsrSiteGrpJB.UsrSiteGrpJB() ");

    };

   public UsrSiteGrpJB(int userSiteGrp, String fkuserId, String fkGrpIds,String fkUserSite,String creator,
            String modifiedBy, String ipAdd) {
			setUserSiteGrp(userSiteGrp);
			setFkuserId(fkuserId);
			setFkGrpIds(fkGrpIds);
			setFkUserSite(fkUserSite);
			setCreator(creator);
			setModifiedBy(modifiedBy);
			setIpAdd(ipAdd);
			Rlog.debug("usrSitegrp", "UsrSiteGrpJB.UsrSiteGrpJB(all parameters)");
    }

   public int updateUserSiteGroup(int userid, String[] userpkSite,String[] fkGroupIds,String[] pkUserSiteGrp,String ipAdd,String creator)
   {
	 int output=0;  
	   try{
		   UsrSiteGrpAgentRObj userSiteGroup = EJBUtil.getUsrSiteGrpAgentBean(); 
		   output = userSiteGroup.updateUsrSiteGrp(userid,userpkSite, fkGroupIds,pkUserSiteGrp,ipAdd,creator);
           return output;
	   }catch (Exception exp) {
		   Rlog.fatal("usrSitegrp", "UsrSiteGrpJB.updateUserSiteGroup ");
		   System.out.print("Exception found in UsrSiteGrpJB.updateUserSiteGroup "+exp);
		   exp.printStackTrace();
	}
	   
	   return output;
   }
   
   public UserSiteGroupDao getUsrSiteGrp(int userid)
   {
	   UserSiteGroupDao userSiteGroupDao = new UserSiteGroupDao();
	   try{
		   UsrSiteGrpAgentRObj userSiteGroup = EJBUtil.getUsrSiteGrpAgentBean(); 
		   userSiteGroupDao = userSiteGroup.getUsrSiteGrp(userid);
           return userSiteGroupDao;
	   }catch (Exception exp) {
		   Rlog.fatal("usrSitegrp", "UsrSiteGrpJB.getUsrSiteGrp ");
		   System.out.print("Exception found in UsrSiteGrpJB.getUsrSiteGrp "+exp);
		   exp.printStackTrace();
	}
	   
	   return userSiteGroupDao;
   }
  
}// end of class

