package com.velos.eres.business.moreDetails.impl;

/* Import Statements */
import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */
@Entity
@Table(name = "ER_MOREDETAILS")

public class MoreDetailsBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3906646393056409909L;

    /**
     * The primary key
     */

    private int id;

    /**
     * The Module pk
     */
    private Integer modId;
    
    /**
     * Module Name
     */
    private String modName;

    /**
     * The Module element Id
     */

    private Integer modElementId;

    /**
     * The Alternate Per Id
     */
    public String modElementData;

    /**
     * The creator
     */

    public Integer creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    public Integer modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    public String ipAdd;

    private ArrayList moreDetailBeans;

    private String recordType;

    // //////////////////////////////////////////////////////////////////////////////////////
   

	/**
     * 
     * 
     * @return formSecId
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_MOREDETAILS", allocationSize=1)
    @Column(name = "PK_MOREDETAILS")
    public int getId() {
        return id;
    }

    /**
     * 
     * 
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }
    @Column(name = "FK_MODPK")
	public String getModId() {
		return StringUtil.integerToString(modId);
	}

	public void setModId(String modId) {
		 if (!StringUtil.isEmpty(modId))
	            this.modId = Integer.valueOf(modId);
	}
	@Column(name = "MD_MODNAME")
	public String getModName() {
		return modName;
	}

	public void setModName(String modName) {
		this.modName = modName;
	}
	@Column(name = "MD_MODELEMENTPK")
	public String getModElementId() {
		return StringUtil.integerToString(modElementId);
	}

	public void setModElementId(String modElementId) {
		if (!StringUtil.isEmpty(modElementId))
            this.modElementId = Integer.valueOf(modElementId);
	}

    @Column(name = "MD_MODELEMENTDATA")
    public String getModElementData() {
		return modElementData;
	}

	public void setModElementData(String modElementData) {
		this.modElementData = modElementData;
	}
		
	@Transient
	public ArrayList getMoreDetailBeans() {
		return moreDetailBeans;
	}

	public void setMoreDetailBeans(ArrayList moreDetailBeans) {
		this.moreDetailBeans = moreDetailBeans;
	}
	public void setMoreDetailBeans(MoreDetailsBean moreDetailBean) {
		this.moreDetailBeans.add(moreDetailBean);
	}
	
	/**
     * @return the Creator
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);

    }

    /**
     * @param takes
     *            a string
     */

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }

    }

    /**
     * @return the unique ID of the User
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {

        return StringUtil.integerToString(this.modifiedBy);

    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {

        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }

    }

    /**
     * @return the IP Address of the Form Notification request
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Notification request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    
    @Transient
    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    // END OF THE GETTER AND SETTER METHODS


    /**
     * updates the PerId Record
     * 
     * @param PerIdStateKeeper
     *            psk
     * @return int 0: in case of successful updation; -2 in caseof exception
     */
    public int updateMoreDetails(MoreDetailsBean mdb) {
        try {
            this.setModId(mdb.getModId());
            this.setModName(mdb.getModName());
            this.setModElementId(mdb.getModElementId());
            this.setModElementData(mdb.getModElementData());
            this.setModifiedBy(mdb.getModifiedBy());
            this.setIpAdd(mdb.getIpAdd());
            this.setCreator(mdb.getCreator());

            return 0;
        } catch (Exception e) {
            Rlog.fatal("perId", " error in perIdBean.updatePerId");
            return -2;
        }
    }

    public MoreDetailsBean() {
    	moreDetailBeans = new ArrayList();
    }

    public MoreDetailsBean(int id, String modId, String modName,
            String modElementId, String modElementData,String creator, String modifiedBy,
            String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setId(id);
        setModId(modId);
        setModName(modName);
        setModElementId(modElementId);
        setModElementData(modElementData);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        moreDetailBeans = new ArrayList();
    }

}
