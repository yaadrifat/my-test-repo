//sajal dt 03/27/2001

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

public class UsrLinkDao extends CommonDAO implements java.io.Serializable {
    private ArrayList lnksIds;

    private ArrayList accIds;

    private ArrayList usrIds;

    private ArrayList lnksUris;

    private ArrayList lnksDescs;

    private ArrayList lnksGrpNames;

    private ArrayList lnksSubTypes;

    private int cRows;

    public UsrLinkDao() {
        lnksIds = new ArrayList();
        accIds = new ArrayList();
        usrIds = new ArrayList();
        lnksUris = new ArrayList();
        lnksDescs = new ArrayList();
        lnksGrpNames = new ArrayList();
        lnksSubTypes = new ArrayList();
    }

    // Getter and Setter methods

    public ArrayList getLnksIds() {
        return this.lnksIds;
    }

    public void setLnksIds(ArrayList lnksIds) {
        this.lnksIds = lnksIds;
    }

    public ArrayList getAccIds() {
        return this.accIds;
    }

    public void setAccIds(ArrayList accIds) {
        this.accIds = accIds;
    }

    public ArrayList getUsrIds() {
        return this.usrIds;
    }

    public void setUsrIds(ArrayList usrIds) {
        this.usrIds = usrIds;
    }

    public ArrayList getLnksUris() {
        return this.lnksUris;
    }

    public void setLnksUris(ArrayList lnksUris) {
        this.lnksUris = lnksUris;
    }

    public ArrayList getLnksDescs() {
        return this.lnksDescs;
    }

    public void setLnksDescs(ArrayList lnksDescs) {
        this.lnksDescs = lnksDescs;
    }

    public ArrayList getLnksGrpNames() {
        return this.lnksGrpNames;
    }

    public void setLnksGrpNames(ArrayList lnksGrpNames) {
        this.lnksGrpNames = lnksGrpNames;
    }

    public ArrayList getLnksSubTypes() {
        return this.lnksSubTypes;
    }

    public void setLnksSubTypes(ArrayList lnksSubTypes) {
        this.lnksSubTypes = lnksSubTypes;
    }

    // ////////////////////////////////

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setLnksIds(Integer lnksId) {
        lnksIds.add(lnksId);
    }

    public void setAccIds(Integer accId) {
        accIds.add(accId);
    }

    public void setUsrIds(Integer usrId) {
        usrIds.add(usrId);
    }

    public void setLnksUris(String lnksUri) {
        lnksUris.add(lnksUri);
    }

    public void setLnksDescs(String lnksDesc) {
        lnksDescs.add(lnksDesc);
    }

    public void setLnksGrpNames(String lnksGrpName) {
        lnksGrpNames.add(lnksGrpName);
    }

    public void setLnksSubTypes(String lnksSubType) {
        lnksSubTypes.add(lnksSubType);
    }

    // end of getter and setter methods

    public void getULinkValuesByAccountId(int accountId, String accType) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        ResultSet rs;
        try {
            conn = getConnection();

            if (accType.equalsIgnoreCase("all")) {

                // JM: 18/08/05
                /*
                 * String mystr="select a.PK_USR_LNKS as lnkId, " +
                 * "a.FK_ACCOUNT as accId, " + "b.CODELST_SUBTYP as codeSubtype, " +
                 * "a.FK_USER as userId, " + "a.LINK_URI as lnkUri, " +
                 * "a.LINK_DESC as lnkDesc, " + "a.LINK_GRPNAME as grpName " +
                 * "from ER_USR_LNKS a, " + "ER_CODELST b " + "where
                 * a.FK_ACCOUNT = ? " + "and fk_user is null and
                 * a.FK_CODELST_LNKTYPE=b.PK_CODELST " + "order by
                 * b.CODELST_SEQ";
                 */
                // JM: 13/09/05 modified
                String mystr = "select a.PK_USR_LNKS as lnkId, "
                        + " a.FK_ACCOUNT as accId, "
                        + " b.CODELST_SUBTYP as codeSubtype, "
                        + " a.FK_USER as userId, "
                        + " a.LINK_URI as lnkUri, "
                        + " a.LINK_DESC as lnkDesc, "
                        + " a.LINK_GRPNAME as grpName "
                        + " from ER_USR_LNKS a, "
                        + " ER_CODELST b "
                        + " where a.FK_ACCOUNT = ? "
                        + " and fk_user is null and a.FK_CODELST_LNKTYPE=b.PK_CODELST "
                        + " order by b.CODELST_SEQ, lower(a.LINK_GRPNAME), lower(a.LINK_DESC)";

                Rlog.debug("ulink", "Sql is " + mystr);
                pstmt = conn.prepareStatement(mystr);
                pstmt.setInt(1, accountId);
                rs = pstmt.executeQuery();

            } else {
                // JM: 18/08/05

                /*
                 * String mystr="select a.PK_USR_LNKS as lnkId, " +
                 * "a.FK_ACCOUNT as accId, " + "b.CODELST_SUBTYP as codeSubtype, " +
                 * "a.FK_USER as userId, " + "a.LINK_URI as lnkUri, " +
                 * "a.LINK_DESC as lnkDesc, " + "a.LINK_GRPNAME as grpName " +
                 * "from ER_USR_LNKS a, " + "ER_CODELST b " + "where
                 * a.FK_ACCOUNT = ? and trim(b.CODELST_SUBTYP)= ? " + "and
                 * fk_user is null and a.FK_CODELST_LNKTYPE=b.PK_CODELST " +
                 * "order by b.CODELST_SEQ";
                 */
                // JM: 13/09/05 modified
                String mystr = "select a.PK_USR_LNKS as lnkId, "
                        + " a.FK_ACCOUNT as accId, "
                        + " b.CODELST_SUBTYP as codeSubtype, "
                        + " a.FK_USER as userId, "
                        + " a.LINK_URI as lnkUri, "
                        + " a.LINK_DESC as lnkDesc, "
                        + " a.LINK_GRPNAME as grpName "
                        + " from ER_USR_LNKS a, "
                        + " ER_CODELST b "
                        + " where a.FK_ACCOUNT = ? and trim(b.CODELST_SUBTYP)= ? "
                        + " and fk_user is null and a.FK_CODELST_LNKTYPE=b.PK_CODELST "
                        + " order by b.CODELST_SEQ, lower(a.LINK_GRPNAME), lower(a.LINK_DESC)";

                Rlog.debug("ulink", "Sql is " + mystr);

                Rlog.debug("ulink", "account type is *" + accType + "*");
                pstmt = conn.prepareStatement(mystr);
                pstmt.setInt(1, accountId);
                pstmt.setString(2, accType);
                rs = pstmt.executeQuery();
            }

            while (rs.next()) {
                setLnksIds(new Integer(rs.getInt("lnkId")));
                setAccIds(new Integer(rs.getInt("accId")));
                setLnksSubTypes(rs.getString("codeSubtype"));
                setUsrIds(new Integer(rs.getInt("userId")));
                setLnksUris(rs.getString("lnkUri"));
                setLnksDescs(rs.getString("lnkDesc"));
                setLnksGrpNames(rs.getString("grpName"));

                rows++;
                Rlog.debug("ulink", "ULinkDao.getULinkValuesByAccountId rows "
                        + rows);
            }
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("ulink",
                    "ULinkDao.getULinkValuesByAccountId EXCEPTION IN FETCHING FROM Ulink table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    public void getULinkValuesByUserId(int userId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            // JM: 18/08/05

            /*
             * pstmt = conn.prepareStatement("select PK_USR_LNKS, " +
             * "FK_ACCOUNT, " + "FK_USER, " + "LINK_URI, " + "LINK_DESC, " +
             * "LINK_GRPNAME from ER_USR_LNKS where FK_USER = ? order by
             * LINK_GRPNAME");
             * 
             */

            // JM: 13/09/05 modified
            pstmt = conn
                    .prepareStatement("select PK_USR_LNKS, "
                            + "FK_ACCOUNT, "
                            + "FK_USER, "
                            + "LINK_URI, "
                            + "LINK_DESC, "
                            + "LINK_GRPNAME from ER_USR_LNKS where FK_USER = ? order by lower(LINK_GRPNAME),lower(LINK_DESC)");

            pstmt.setInt(1, userId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setLnksIds(new Integer(rs.getInt("PK_USR_LNKS")));
                setAccIds(new Integer(rs.getInt("FK_ACCOUNT")));
                setUsrIds(new Integer(rs.getInt("FK_USER")));
                setLnksUris(rs.getString("LINK_URI"));
                setLnksDescs(rs.getString("LINK_DESC"));
                setLnksGrpNames(rs.getString("LINK_GRPNAME"));
                rows++;
                Rlog.debug("ulink", "ULinkDao.getULinkValuesByUserId rows "
                        + rows);
            }
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("ulink",
                    "ULinkDao.getULinkValuesByUserId EXCEPTION IN FETCHING FROM Ulink table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

}
