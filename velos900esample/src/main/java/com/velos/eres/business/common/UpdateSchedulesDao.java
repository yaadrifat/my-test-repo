package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;

import oracle.jdbc.driver.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;

public class UpdateSchedulesDao extends CommonDAO implements java.io.Serializable {
	
	 public int discShedulesAllPatient(int discSchCal, String discDate, String discReason, int newSchCal, String newSchStartDate, int usr, String ipAdd) { 

	        int ret = 0;
		 	CallableStatement cstmt = null;

	        Rlog.debug("updateSchedules", "In discShedulesAllPatient in UpdateSchedulesDao");
	        Connection conn = null;  
	        try {
	            conn = getConnection();
	            cstmt = conn.prepareCall("{call PKG_RESCHEDULE.SP_DISC_ALLSCHS(?,?,?,?,?,?,?,?)}");

	            cstmt.setInt(1, discSchCal);
	            cstmt.setDate(2, DateUtil.dateToSqlDate((DateUtil.stringToDate(discDate,null))));
	            cstmt.setString(3, discReason);
	            cstmt.setInt(4, newSchCal);
	            cstmt.setString(5, newSchStartDate);
	            cstmt.setInt(6, usr);
	            cstmt.setString(7, ipAdd);
	            cstmt.registerOutParameter(8, java.sql.Types.INTEGER);

	            cstmt.execute();

	            Rlog.debug("updateschedules", " after execute of SP_DISC_ALLSCHS");
	            ret = cstmt.getInt(8);
	            Rlog.debug("updateschedules", " after execute of RETURN VALUE" + ret);
	            
	            
	            return ret;

	        } catch (SQLException ex) {
	            Rlog.fatal("updateSchedules",
	                    "In discShedulesAllPatient in UpdateSchedulesDao line EXCEPTION IN calling the procedure"
	                            + ex);
	            
	            return -1;

	        } finally {
	            try {
	                if (cstmt != null)
	                    cstmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }

	        }
	    }


	 
	 public int updateEventStatusCurrAndDisc(int eveStatDiscCal, String scheduleFlag, String dateOccurDisc, String allEveDate, int dstatusDisc, String allEveStatDate, int usr, String ipAdd ) { 

	        int ret = 0;
		 	CallableStatement cstmt = null;

	        Rlog.debug("updateSchedules", "In updateEventStatusCurrAndDisc in UpdateSchedulesDao");
	        Connection conn = null;
	        try {
	            conn = getConnection();
	            cstmt = conn.prepareCall("{call PKG_RESCHEDULE.SP_UPDATE_EVENTS(?,?,?,?,?,?,?,?,?)}");

	            cstmt.setInt(1, eveStatDiscCal);
	            cstmt.setString(2, scheduleFlag);
	            cstmt.setString(3, dateOccurDisc);
	            cstmt.setDate(4, DateUtil.dateToSqlDate((DateUtil.stringToDate(allEveDate,null))));
	            cstmt.setInt(5, dstatusDisc);
	            cstmt.setDate(6, DateUtil.dateToSqlDate((DateUtil.stringToDate(allEveStatDate,null))));
	            cstmt.setInt(7, usr);
	            cstmt.setString(8, ipAdd);
	            cstmt.registerOutParameter(9, java.sql.Types.INTEGER);

	            cstmt.execute();

	            Rlog.debug("updateschedules", " after execute of SP_UPDATE_EVENTS");
	            ret = cstmt.getInt(9);
	            Rlog.debug("updateschedules", " after execute of RETURN VALUE" + ret);
	            
	            return ret;

	        } catch (SQLException ex) {
	            Rlog.fatal("updateSchedules",
	                    "In updateEventStatusCurrAndDisc in UpdateSchedulesDao line EXCEPTION IN calling the procedure"
	                            + ex);
	            
	            return -1;

	        } finally {
	            try {
	                if (cstmt != null)
	                    cstmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }

	        }
	    }


	 
	 
	 public int generateNewSchedules(int newSchAllCal, String patSchStartDate, int patEnrollDtCheck, int usr, String ipAdd ) { 

	        int ret = 0;
		    CallableStatement cstmt = null;

	        Rlog.debug("updateSchedules", "In generateNewSchedules in UpdateSchedulesDao");
	        Connection conn = null;
	        try {
	            conn = getConnection();
	            cstmt = conn.prepareCall("{call PKG_RESCHEDULE.SP_GENERATE_SCHEDULES(?,?,?,?,?,?)}");

	            cstmt.setInt(1, newSchAllCal);
	            cstmt.setDate(2, DateUtil.dateToSqlDate((DateUtil.stringToDate(patSchStartDate,null))));
	            cstmt.setInt(3, patEnrollDtCheck);
	            cstmt.setInt(4, usr);
	            cstmt.setString(5, ipAdd);
	            cstmt.registerOutParameter(6, java.sql.Types.INTEGER); 

	            cstmt.execute();

	            Rlog.debug("updateschedules", " after execute of SP_GENERATE_SCHEDULES");
	            ret = cstmt.getInt(6);
	            Rlog.debug("updateschedules", " after execute of RETURN VALUE" + ret);
	            
	            return ret;

	        } catch (SQLException ex) {
	            Rlog.fatal("updateSchedules",
	                    "In generateNewSchedules in UpdateSchedulesDao line EXCEPTION IN calling the procedure"
	                            + ex);
	            
	            return -1;

	        } finally {
	            try {
	                if (cstmt != null)
	                    cstmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }

	        }
	    }

	 
	 
	 
	 
}