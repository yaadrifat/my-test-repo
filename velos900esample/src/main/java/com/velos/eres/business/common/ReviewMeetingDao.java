/*
 * Classname            ReviewMeetingDao.class
 * 
 * Version information  1.0
 *
 * Date                 06/01/2009
 * 
 * Copyright notice     Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;


public class ReviewMeetingDao extends CommonDAO implements java.io.Serializable {
    private ArrayList meetingPKs;
    private ArrayList meetingDates;
    private ArrayList boardIdList;
    private Hashtable meetingPKsHash;
    private Hashtable meetingDatesHash;
    
    public ReviewMeetingDao() {
        meetingPKs = new ArrayList();
        meetingDates = new ArrayList();
        boardIdList = new ArrayList();
        meetingPKsHash = new Hashtable();
        meetingDatesHash = new Hashtable();
    }
    
    public String getMeetingDropDown(int index, String boardId, String selMeetingId) {
        StringBuffer sb = new StringBuffer();
        sb.append("<select name='meetingDate").append(index).append("' disabled >");
        boolean selectedExists = false;
        ArrayList hashedMeetingPKs = (ArrayList)meetingPKsHash.get(boardId);
        ArrayList hashedMeetingDates = (ArrayList)meetingDatesHash.get(boardId);
        // Handle the no data situation
        if (hashedMeetingPKs == null) {
            sb.append("</select>");
            return sb.toString();
        }
        for (int iX=0; iX<hashedMeetingPKs.size(); iX++ ) {
            sb.append("<option value='").append(hashedMeetingPKs.get(iX)).append("'");
            if (StringUtil.stringToNum(selMeetingId) == Integer.valueOf((String)hashedMeetingPKs.get(iX))) {
                sb.append(" selected");
                selectedExists = true;
            }
            sb.append(">").append(hashedMeetingDates.get(iX)).append("</option>");
        }
        sb.append("<OPTION value =''");
        if (!selectedExists) {
            sb.append(" selected");
        }
        sb.append(">Select an Option</OPTION>");
        sb.append("</select>");
        return sb.toString();
    }
    
    public String getMeetingValueTextPairs(String boardId) {
        StringBuffer sb = new StringBuffer();
        ArrayList hashedMeetingPKs = (ArrayList)meetingPKsHash.get(boardId);
        ArrayList hashedMeetingDates = (ArrayList)meetingDatesHash.get(boardId);
        if (hashedMeetingPKs == null) { return ""; }
        for (int iX=0; iX<hashedMeetingPKs.size(); iX++ ) {
            sb.append("{value:\"").append(hashedMeetingPKs.get(iX)).append("\",text:\"");
            sb.append(hashedMeetingDates.get(iX)).append("\"},");
        }
        sb.setLength(sb.length()-1);
        return sb.toString();
    }
    
    private void clearMeetings() {
        meetingPKs.clear();
        meetingDates.clear();
        boardIdList.clear();
        meetingPKsHash.clear();
        meetingDatesHash.clear();
    }
    
    private void setMeetingPK(String pk) {
        meetingPKs.add(pk);
    }
    
    private void setMeetingDate(String date) {
        meetingDates.add(date);
    }
    
    private void setReviewBoard(String reviewBoardId) {
        boardIdList.add(reviewBoardId);
    }
    
    private void hashMeetings() {
        for (int iBoard=0; iBoard<boardIdList.size(); iBoard++) {
            ArrayList hashedMeetingPKs = (ArrayList)meetingPKsHash.get(boardIdList.get(iBoard));
            if (hashedMeetingPKs == null) {
                hashedMeetingPKs = new ArrayList();
            }
            hashedMeetingPKs.add(meetingPKs.get(iBoard));
            meetingPKsHash.put(boardIdList.get(iBoard), hashedMeetingPKs);

            ArrayList hashedMeetingDates = (ArrayList)meetingDatesHash.get(boardIdList.get(iBoard));
            if (hashedMeetingDates == null) {
                hashedMeetingDates = new ArrayList();
            }
            hashedMeetingDates.add(meetingDates.get(iBoard));
            meetingDatesHash.put(boardIdList.get(iBoard), hashedMeetingDates);
        }
    }
    
    public void getReviewMeetings(int accountId, int grpId, ArrayList boardIdList) {
        clearMeetings();
        
        StringBuffer csvList = new StringBuffer();
        for (int iX=0; iX<boardIdList.size(); iX++) {
            csvList.append(boardIdList.get(iX)).append(",");
        }
        if (csvList.length() > 0) {
            csvList.setLength(csvList.length()-1); // get rid of the last comma
        } else {
            csvList.append("0"); // avoid exception when boardIdList is empty
        }

        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            StringBuffer sql = new StringBuffer();
            sql.append("select m.pk_review_meeting, m.meeting_date, m.fk_review_board from er_review_meeting m ");
            sql.append(" inner join er_review_board b on m.fk_review_board = b.pk_review_board ");
            sql.append(" where m.fk_account = ? and m.fk_review_board in ( ");
            sql.append(csvList).append(" ) and ");
            sql.append(" ','|| b.board_group_access || ',' like '%,").append(grpId).append(",%'  ");
            sql.append(" order by m.fk_review_board, m.meeting_date ");
            
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, accountId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                setMeetingPK(rs.getString("pk_review_meeting"));
                setMeetingDate(DateUtil.dateToString(rs.getDate("meeting_date")));
                setReviewBoard(rs.getString("fk_review_board"));
            }
            hashMeetings();
        } catch(SQLException e) {
            Rlog.fatal("ReviewMeetingDao", " error in getReviewMeetings() "+e);
        } finally {
            try {
                if (pstmt != null) pstmt.close();
            } catch (Exception e) {}
            try {
                if (conn != null) conn.close();
            } catch (Exception e) {}
        }
    }
}
