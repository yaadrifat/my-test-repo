package com.velos.eres.business.submission.impl;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * @ejbHomeJNDIname ejb.SubmissionProvisoBean
 */

@Entity
@Table(name = "ER_SUBMISSION_PROVISO")

public class SubmissionProvisoBean implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Integer id;
    private Integer fkSubmission;
    private Integer fkSubmissionBoard;
    private Date    provisoDate;
    private Integer provisoEnteredBy;
    private String  submissionProviso;
    private Integer creator;
    private Integer lastModifiedBy;
    private Date    lastModifiedDate;
    private String  ipAdd;
    private String provisoEnteredByName;
    private String provisoType;
    
    
    public SubmissionProvisoBean() {}
    
    public SubmissionProvisoBean(Integer id, Integer fkSubmission, Integer fkSubmissionBoard,
            Date provisoDate, Integer provisoEnteredBy,
             String submissionProviso,
             Integer creator, Integer lastModifiedBy, String ipAdd,String provisoType) {
        setId(id);
        setFkSubmission(fkSubmission);
        setFkSubmissionBoard(fkSubmissionBoard);
        setProvisoDate(provisoDate);
        setProvisoEnteredBy(provisoEnteredBy);
        setSubmissionProviso(submissionProviso);
        setCreator(creator);
        setLastModifiedBy(lastModifiedBy); 
        setIpAdd(ipAdd);
        setProvisoType(provisoType);
    }
    
    public void setDetails(SubmissionProvisoBean anotherBean) {
        setFkSubmission(anotherBean.getFkSubmission());
        setFkSubmissionBoard(anotherBean.getFkSubmissionBoard());
        setProvisoDate(anotherBean.getProvisoDate());
        setProvisoEnteredBy(anotherBean.getProvisoEnteredBy());
        setSubmissionProviso(anotherBean.getSubmissionProviso());
        setCreator(anotherBean.getCreator());
        setLastModifiedBy(anotherBean.getLastModifiedBy());
        setIpAdd(anotherBean.getIpAdd());
        setProvisoType(anotherBean.getProvisoType());
    }

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_PROVISO", allocationSize=1)
    @Column(name = "PK_SUBMISSION_PROVISO")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "FK_SUBMISSION")
    public Integer getFkSubmission() {
        return fkSubmission;
    }

    public void setFkSubmission(Integer fkSubmission) {
        this.fkSubmission = fkSubmission;
    }

    @Column(name = "FK_SUBMISSION_BOARD")
    public Integer getFkSubmissionBoard() {
        return fkSubmissionBoard;
    }

    public void setFkSubmissionBoard(Integer fkSubmissionBoard) {
        this.fkSubmissionBoard = fkSubmissionBoard;
    }

   

    @Column(name = "proviso_date")
    public Date getProvisoDate() {
        return provisoDate;
    }

    public void setProvisoDate(Date provisoDate) {
        this.provisoDate = provisoDate;
    }

    @Column(name = "fk_user_enteredby")
    public Integer getProvisoEnteredBy() {
        return provisoEnteredBy;
    }

    public void setProvisoEnteredBy(Integer provisoEnteredBy) {
        this.provisoEnteredBy = provisoEnteredBy;
    }
 
    @Transient
    public String getProvisoEnteredByName() {
        return provisoEnteredByName;
    }

    public void setProvisoEnteredByName(String provisoEnteredByName) {
        this.provisoEnteredByName = provisoEnteredByName;
    }
    
    @Lob @Basic(fetch=FetchType.EAGER)	
    @Column(name = "proviso"  , length = 2147483647)
    public String getSubmissionProviso() {
        return submissionProviso;
    }

    public void setSubmissionProviso(String submissionProviso) {
        this.submissionProviso = submissionProviso;
    }

  
    @Column(name = "creator")
    public Integer getCreator() {
        return creator;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    @Column(name = "LAST_MODIFIED_BY")
    public Integer getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(Integer lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Column(name = "LAST_MODIFIED_DATE")
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Column(name = "FK_CODELST_PROVISOTYPE")
    public String getProvisoType() {
        return provisoType;
    }

    public void setProvisoType(String provisoType) {
        this.provisoType = provisoType;
    }

    
}
