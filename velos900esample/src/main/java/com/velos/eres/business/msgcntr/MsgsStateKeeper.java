/*
 * Classname : MsgsStateKeeper.java
 * 
 * Version information 1.0
 *
 * Date 04/30/2001
 * 
 * Copyright notice: Velos Inc.
 */

package com.velos.eres.business.msgcntr;

import java.util.*;
import com.velos.eres.service.util.*;

/**
 * MsgsStateKeeper class resulting from implementation of the StateKeeper
 * pattern
 * 
 * @author Dinesh @ version 1.0 04/30/2001
 */

public class MsgsStateKeeper implements java.io.Serializable {

    /**
     * the msgcntr author id
     */

    private String authorId;

    /**
     * the msgcntr multiple ids
     */
    private ArrayList msgIds;

    /**
     * the msgcntr multiple Study ids
     */
    private ArrayList msgStudyIds;

    /**
     * the msgcntr External user ids
     */
    private ArrayList msgUserXs;

    /**
     * the msgcntr multiple UserFrom ids
     */
    private ArrayList msgGrants;

    // GETTER SETTER METHODS

    public String getAuthorId() {
        return this.authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public ArrayList getMsgIds() {
        return this.msgIds;
    }

    public void setMsgIds(ArrayList msgIds) {
        this.msgIds = msgIds;
    }

    public ArrayList getMsgStudyIds() {
        return this.msgStudyIds;
    }

    public void setMsgStudyIds(ArrayList msgStudyIds) {
        this.msgStudyIds = msgStudyIds;
    }

    public ArrayList getMsgUserXs() {
        return this.msgUserXs;
    }

    public void setMsgUserXs(ArrayList msgUserXs) {
        this.msgUserXs = msgUserXs;
    }

    public ArrayList getMsgGrants() {
        return this.msgGrants;
    }

    public void setMsgGrants(ArrayList msgGrants) {
        this.msgGrants = msgGrants;
    }

    // END OF GETTER SETTER METHODS

    public MsgsStateKeeper() {
    }

    public MsgsStateKeeper(String authorId, ArrayList msgIds,
            ArrayList msgStudyIds, ArrayList msgUserXs, ArrayList msgGrants) {
        Rlog.debug("msgcntr", "MsgsStateKeeper constructor start");

        setAuthorId(authorId);
        setMsgIds(msgIds);
        setMsgStudyIds(msgStudyIds);
        setMsgUserXs(msgUserXs);
        setMsgGrants(msgGrants);
        Rlog.debug("msgcntr", "MsgsStateKeeper constructor end");
    }

    public MsgsStateKeeper getMsgsStateKeeper() {
        return new MsgsStateKeeper(authorId, msgIds, msgStudyIds, msgUserXs,
                msgGrants);
    }

    /**
     * Setter for all attributes.<br>
     * Receives a MsgcntrStateKeeper and assigns attributes to local variables
     * 
     * @param msgsk
     *            The MsgcntrStateKeeper to set to.
     */
    public void setMsgsStateKeeper(MsgsStateKeeper msgsk) {
        setAuthorId(msgsk.getAuthorId());
        setMsgIds(msgsk.getMsgIds());
        setMsgStudyIds(msgsk.getMsgStudyIds());
        setMsgUserXs(msgsk.getMsgUserXs());
        setMsgGrants(msgsk.getMsgGrants());
    }

    /**
     * Overriden for unit testing purposes
     * 
     * @param anObject
     *            Object on which to check equality.
     */
    public boolean equals(Object anObject) {
        if (!(anObject instanceof MsgsStateKeeper))
            return false;
        MsgsStateKeeper aBean = (MsgsStateKeeper) anObject;
        return aBean.getAuthorId() == getAuthorId() &&

        aBean.getMsgIds() == getMsgIds() &&

        aBean.getMsgStudyIds().equals(getMsgStudyIds()) &&

        aBean.getMsgUserXs().equals(getMsgUserXs()) &&

        aBean.getMsgGrants().equals(getMsgGrants());
    }

}