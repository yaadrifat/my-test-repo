/*
 * Classname			GroupBean
 *
 * Version information	1.0
 *
 * Date					02/25/2001
 *
 * Copyright notice		Velos Inc.
 */

package com.velos.eres.business.group.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * The Group CMP entity bean for Account Groups.<br>
 * <br>
 *
 * @author Sajal
 * @version 1.0 02/25/2001
 * @ejbHome GroupHome
 * @ejbRemote GroupRObj
 */
@Entity
@Table(name = "er_grps")
public class GroupBean implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 3258407348337914424L;

    /**
     * the Group Id
     */
    public int groupId;

    /**
     * Group Account Id
     */
    public Integer groupAccId;

    /**
     * Group Name
     */
    public String groupName;

    /**
     * Group Description
     */
    public String groupDesc;

    /**
     * the record creator
     */
    public Integer creator;

    /**
     * last modified by
     */
    public Integer modifiedBy;

    /**
     * the IP Address
     */
    public String ipAdd;

    /**
     * groupSuperUserFlag
     */
    public Integer groupSuperUserFlag;

    /**
     * groupSuperUserRights
     */

    public String groupSuperUserRights;

    /**
     * Budget Super User Flag
     */
    public String groupSupBudFlag;

    /**
     * Budget Super User Rights
     */
    public String groupSupBudRights;

    /**
     * Value of Group Hidden
     */
    public String groupHidden;//KM


    /**
     * the codelst id for the specific role
     *
     */

    public Integer roleId;


    // GETTER SETTER METHODS

    /**
     *
     * @return id of the group
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_GRPS", allocationSize=1)
    @Column(name = "PK_grp")
    public int getGroupId() {
        return this.groupId;
    }

    public void setGroupId(int grpId) {
        this.groupId = grpId;
    }

    /**
     *
     *
     * @return account Id of the group
     */
    @Column(name = "fk_account")
    public String getGroupAccId() {
        return StringUtil.integerToString(this.groupAccId);
    }

    /**
     *
     *
     * @param groupAccId
     *            Account id of the group
     */

    public void setGroupAccId(String groupAccId) {
        if (groupAccId != null) {
            this.groupAccId = Integer.valueOf(groupAccId);
        }
    }

    /**
     *
     *
     * @return group name of the group
     */
    @Column(name = "grp_name")
    public String getGroupName() {
        String groupName;
        groupName = this.groupName;
        return groupName;
    }

    /**
     *
     *
     * @param groupName
     *            Group name of the group
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     *
     *
     * @return group description of the group
     */
    @Column(name = "grp_desc")
    public String getGroupDesc() {
        String groupDesc;
        groupDesc = this.groupDesc;
        return groupDesc;
    }

    /**
     *
     *
     * @param groupDesc
     *            Description of the group to be set
     */
    public void setGroupDesc(String groupDesc) {
        this.groupDesc = groupDesc;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * @param groupSuperUserFlag
     *            The groupSuperUserFlag to indicate if the group is a super
     *            user group
     */

    /**
     * @return groupSuperUserFlag
     */
    @Column(name = "GRP_SUPUSR_FLAG")
    public String getGroupSuperUserFlag() {
        return StringUtil.integerToString(this.groupSuperUserFlag);
    }

    public void setGroupSuperUserFlag(String groupSuperUserFlag) {
        if (groupSuperUserFlag != null) {
            this.groupSuperUserFlag = Integer.valueOf(groupSuperUserFlag);
        }

    }

    /**
     * @return groupSuperUserRights
     */
    @Column(name = "GRP_SUPUSR_RIGHTS")
    public String getGroupSuperUserRights() {
        return this.groupSuperUserRights;
    }

    /**
     * @param groupSuperUserRights
     *            The groupSuperUserRights - study rights for the super user
     *            group
     */
    public void setGroupSuperUserRights(String groupSuperUserRights) {
        this.groupSuperUserRights = groupSuperUserRights;
    }

    /**
     * Returns the value of groupSupBudFlag.
     */
    @Column(name = "GRP_SUPBUD_FLAG")
    public String getGroupSupBudFlag() {
        return groupSupBudFlag;
    }

    /**
     * Sets the value of groupSupBudFlag.
     *
     * @param groupSupBudFlag
     *            The value to assign groupSupBudFlag.
     */
    public void setGroupSupBudFlag(String groupSupBudFlag) {
        this.groupSupBudFlag = groupSupBudFlag;
    }

    /**
     * Returns the value of groupSupBudRights.
     */
    @Column(name = "GRP_SUPBUD_RIGHTS")
    public String getGroupSupBudRights() {
        return groupSupBudRights;
    }

    /**
     * Sets the value of groupSupBudRights.
     *
     * @param groupSupBudRights
     *            The value to assign groupSupBudRights.
     */
    public void setGroupSupBudRights(String groupSupBudRights) {
        this.groupSupBudRights = groupSupBudRights;
    }

    //Added by Manimaran for Enh.#U11
    /**
     * Returns the value of groupHidden.
     */
    @Column(name = "GRP_HIDDEN")
    public String getGroupHidden() {
        return groupHidden;
    }

    /**
     * Sets the value of groupHidden.
     *
     * @param groupHidden
     *            The value to assign groupHidden.
     */
    public void setGroupHidden(String groupHidden) {

    	String grpH = "";

     	if (StringUtil.isEmpty(groupHidden))
    	{
    		grpH = "0";
    	}
    	else
    	{

    		grpH = groupHidden;
    	}

        this.groupHidden = grpH;
    }


    /**
     * @return the codelst id for the specific role
     */
    @Column(name = "FK_CODELST_ST_ROLE")
    public String getRoleId() {
        return StringUtil.integerToString(this.roleId);
    }

    /**
     * @param roleID
     *            the codelst id for the specific role
     */
    public void setRoleId(String roleId) {
        if (roleId != null) {
            this.roleId = Integer.valueOf(roleId);
        }
    }

    // END OF GETTER SETTER METHODS

    /**
     * Returns the state keeper object of the group which holds all the details
     * of the group.
     *
     * @return State keeper of the group
     */

    /**
     * Update the details of the group
     *
     * @param gsk
     *            Group State Keeper which holds values of the group
     * @return 0 when updation is successful and -2 when exception occurs
     */
    public int updateGroup(GroupBean gsk) {
        try {
            setGroupAccId(gsk.getGroupAccId());
            setGroupName(gsk.getGroupName());
            setGroupDesc(gsk.getGroupDesc());
            setCreator(gsk.getCreator());
            setModifiedBy(gsk.getModifiedBy());
            setIpAdd(gsk.getIpAdd());

            setGroupSuperUserFlag(gsk.getGroupSuperUserFlag());
            setGroupSuperUserRights(gsk.getGroupSuperUserRights());
            setGroupSupBudFlag(gsk.getGroupSupBudFlag());
            Rlog.debug("group", "GroupBean.updateGroup"
                    + gsk.getGroupSupBudRights());
            setGroupSupBudRights(gsk.getGroupSupBudRights());
            setGroupHidden(gsk.getGroupHidden());//KM

            setRoleId(gsk.getRoleId());

            Rlog.debug("group", "GroupBean.updateGroup");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("group", " error in GroupBean.updateGroup");
            return -2;
        }
    }

    public GroupBean() {

    }

    public GroupBean(int groupId, String groupAccId, String groupName,
            String groupDesc, String creator, String modifiedBy, String ipAdd,
            String groupSuperUserFlag, String groupSuperUserRights,
            String groupSupBudFlag, String groupSupBudRights, String groupHidden, String roleId) {
        super();
        // TODO Auto-generated constructor stub
        setGroupId(groupId);
        setGroupAccId(groupAccId);
        setGroupName(groupName);
        setGroupDesc(groupDesc);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setGroupSuperUserFlag(groupSuperUserFlag);
        setGroupSuperUserRights(groupSuperUserRights);
        setGroupSupBudFlag(groupSupBudFlag);
        setGroupSupBudRights(groupSupBudRights);
        setGroupHidden(groupHidden);
        setRoleId(roleId);
    }

}// end of class
