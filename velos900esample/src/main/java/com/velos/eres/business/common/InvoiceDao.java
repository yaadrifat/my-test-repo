

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

/**
 * InvoiceDao - data Access class for Invoice module
 *
 * @author Sonia Abrol
 * @created 11/04/2005
 */
public class InvoiceDao extends CommonDAO implements java.io.Serializable {

	 /**
	 *
	 */
	private static final long serialVersionUID = 7212503437124169558L;

	/**
	 * the Invoice Primary Key
	 */
	private ArrayList <String> id;

	/**
	 * the Invoice Number
	 */
	private ArrayList <String> invNumber;

	/**
	 * the Invoice addressed to
	 */
	private ArrayList <String> invAddressedto;

	/**
	 * the Invoice sent from
	 */
	private ArrayList <String> invSentFrom;

	/**
	 * the invoice header
	 */
	private ArrayList <String> invHeader;

	/**
	 * the invoice footer
	 */
	private ArrayList <String> invFooter;

	/**
	 * Milestone rule status type
	 */
	private ArrayList <String> invMileStatusType;

	/**
	 * the invoice interval type
	 */
	private ArrayList <String> invIntervalType;

	/**
	 * Invoice interval from
	 */
	private ArrayList <String> invIntervalFrom;

	/**
	 * Invoice interval To
	 */
	private ArrayList  <String> invIntervalTo;

	/**
	 * Invoice Date
	 */
	private ArrayList <String> invDate;

	/**
	 * the invoice Notes
	 */
	private ArrayList <String> invNotes ;

	/**
	 * the invoice Other user
	 */
	private ArrayList <String> invOtherUser;



	private ArrayList  <String> invPayDueBy;

	private ArrayList <String> invPayUnit;

	private ArrayList <String> creator;

	private ArrayList <String> invStats;

	private ArrayList <String> historyIds;

	private ArrayList <String> subTypes;





    public InvoiceDao() {
		super();
		// TODO Auto-generated constructor stub
		this.id = new ArrayList <String>();
		invAddressedto = new ArrayList<String>();
		invDate = new ArrayList <String>();
		invFooter = new ArrayList<String>();
		invHeader = new ArrayList <String>();
		invIntervalFrom = new ArrayList <String>();
		invIntervalTo = new ArrayList <String>();
		invIntervalType = new ArrayList <String>();
		invMileStatusType = new ArrayList <String>();
		invNotes = new ArrayList<String>();
		invNumber = new ArrayList <String>();
		invOtherUser = new ArrayList <String>();
		invPayDueBy = new ArrayList <String>();
		invPayUnit = new ArrayList<String>();
		invSentFrom = new ArrayList<String>();
		creator = new ArrayList<String>();
		invStats = new ArrayList<String>();
		historyIds = new ArrayList<String>();
		subTypes = new ArrayList<String>();

	}



    // end of getter and setter methods

    public ArrayList<String> getCreator() {
		return creator;
	}



	public void setCreator(ArrayList<String> creator) {
		this.creator = creator;
	}

	public void setCreator(String creator) {
		this.creator.add(creator);
	}


	public static long getSerialVersionUID() {
		return serialVersionUID;
	}



	public ArrayList<String> getId() {
		return id;
	}



	public void setId(ArrayList<String> id) {
		this.id = id;
	}

	public void setId(String id) {
		this.id.add(id);
	}

	public ArrayList<String> getInvAddressedto() {
		return invAddressedto;
	}



	public void setInvAddressedto(ArrayList<String> invAddressedto) {
		this.invAddressedto = invAddressedto;
	}

	public void setInvAddressedto(String invAddressedto) {
		this.invAddressedto.add(invAddressedto);
	}


	public ArrayList<String> getInvDate() {
		return invDate;
	}



	public void setInvDate(ArrayList<String> invDate) {
		this.invDate = invDate;
	}

	public void setInvDate(String invDate) {
		this.invDate.add(invDate);
	}


	public ArrayList<String> getInvFooter() {
		return invFooter;
	}



	public void setInvFooter(ArrayList<String> invFooter) {
		this.invFooter = invFooter;
	}

	public void setInvFooter(String invFooter) {
		this.invFooter.add(invFooter);
	}


	public ArrayList<String> getInvHeader() {
		return invHeader;
	}



	public void setInvHeader(ArrayList<String> invHeader) {
		this.invHeader = invHeader;
	}

	public void setInvHeader(String invHeader) {
		this.invHeader.add(invHeader);
	}


	public ArrayList<String> getInvIntervalFrom() {
		return invIntervalFrom;
	}



	public void setInvIntervalFrom(ArrayList<String> invIntervalFrom) {
		this.invIntervalFrom = invIntervalFrom;
	}



	public void setInvIntervalFrom(String invIntervalFrom) {
		this.invIntervalFrom.add(invIntervalFrom);
	}


	public ArrayList<String> getInvIntervalTo() {
		return invIntervalTo;
	}



	public void setInvIntervalTo(ArrayList<String> invIntervalTo) {
		this.invIntervalTo = invIntervalTo;
	}

	public void setInvIntervalTo(String invIntervalTo) {
		this.invIntervalTo.add(invIntervalTo);
	}

	public ArrayList<String> getInvIntervalType() {
		return invIntervalType;
	}



	public void setInvIntervalType(ArrayList<String> invIntervalType) {
		this.invIntervalType = invIntervalType;
	}


	public void setInvIntervalType(String invIntervalType) {
		this.invIntervalType.add(invIntervalType);
	}

	public ArrayList<String> getInvMileStatusType() {
		return invMileStatusType;
	}



	public void setInvMileStatusType(ArrayList<String> invMileStatusType) {
		this.invMileStatusType = invMileStatusType;
	}


	public void setInvMileStatusType(String invMileStatusType) {
		this.invMileStatusType.add(invMileStatusType);
	}


	public ArrayList<String> getInvNotes() {
		return invNotes;
	}



	public void setInvNotes(ArrayList<String> invNotes) {
		this.invNotes = invNotes;
	}


	public void setInvNotes(String invNotes) {
		this.invNotes.add(invNotes);
	}

	public ArrayList<String> getInvNumber() {
		return invNumber;
	}



	public void setInvNumber(ArrayList<String> invNumber) {
		this.invNumber = invNumber;
	}

	public void setInvNumber(String invNumber) {
		this.invNumber.add(invNumber);
	}


	public ArrayList<String> getInvOtherUser() {
		return invOtherUser;
	}



	public void setInvOtherUser(ArrayList<String> invOtherUser) {
		this.invOtherUser = invOtherUser;
	}

	public void setInvOtherUser(String invOtherUser) {
		this.invOtherUser.add(invOtherUser);
	}


	public ArrayList<String> getInvPayDueBy() {
		return invPayDueBy;
	}



	public void setInvPayDueBy(ArrayList<String> invPayDueBy) {
		this.invPayDueBy = invPayDueBy;
	}

	public void setInvPayDueBy(String invPayDueBy) {
		this.invPayDueBy.add(invPayDueBy);
	}

	public ArrayList<String> getInvPayUnit() {
		return invPayUnit;
	}



	public void setInvPayUnit(ArrayList<String> invPayUnit) {
		this.invPayUnit = invPayUnit;
	}

	public void setInvPayUnit(String invPayUnit) {
		this.invPayUnit.add(invPayUnit);
	}


	public ArrayList<String> getInvSentFrom() {
		return invSentFrom;
	}



	public void setInvSentFrom(ArrayList<String> invSentFrom) {
		this.invSentFrom = invSentFrom;
	}

	public void setInvSentFrom(String invSentFrom) {
		this.invSentFrom.add(invSentFrom);
	}

	//JM: invStats and historyIds
	public ArrayList<String> getInvStats() {
    	return this.invStats;
    }

    public void setInvStats(ArrayList<String> invStats) {
        this.invStats = invStats;
    }

    public void setInvStats(String invStat) {
        this.invStats.add(invStat);
    }


    public ArrayList<String> getHistoryIds() {
    	return this.historyIds;
    }

    public void setHistoryIds(ArrayList<String> historyIds) {
        this.historyIds = historyIds;
    }

    public void setHistoryIds(String historyId) {
        this.historyIds.add(historyId);
    }



    public ArrayList<String> getSubTypes() {
    	return this.subTypes;
    }

    public void setSubTypes(ArrayList<String> subTypes) {
        this.subTypes = subTypes;
    }

    public void setSubTypes(String subType) {
        this.subTypes.add(subType);
    }

	/**
     * Gets the saved invoices for a study
     *
     * @param accountId
     *            Description of the Parameter
     */
    public void getSavedInvoices(int studyId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            //JM: 21Feb2008: changed for #Fin11: Feb2008 Enhancements
            pstmt = conn.prepareStatement(" select pk_invoice, inv_number,to_char(inv_date,PKG_DATEUTIL.F_GET_DATEFORMAT) inv_date, "
            		+ " usr_lst(inv.creator) creator,inv_notes,to_char(inv_intervalfrom,PKG_DATEUTIL.F_GET_DATEFORMAT)inv_intervalfrom, "
            		+ " to_char(inv_intervalto,PKG_DATEUTIL.F_GET_DATEFORMAT) inv_intervalto, "
            		+ " (select codelst_desc from er_codelst where pk_codelst = FK_CODELST_STAT) as invoice_Status, "
            		+ " pk_status,  CODELST_SUBTYP from er_invoice inv, "
            		+ " (Select CODELST_DESC, CODELST_SUBTYP, PK_STATUS, STATUS_MODPK, FK_CODELST_STAT from er_invoice i, "
            		+ " ER_STATUS_HISTORY, er_codelst cd Where i.fk_study = ? and i.pk_invoice = STATUS_MODPK "
            		+ " and STATUS_MODTABLE = 'er_invoice' and STATUS_END_DATE is null and cd.pk_codelst = FK_CODELST_STAT ) stat "
            		+ " where fk_study = ? "
            		+ " and  status_modpk(+) = pk_invoice "
            		+ " order by to_date(inv_date,PKG_DATEUTIL.F_GET_DATEFORMAT) desc");

            pstmt.setInt(1, studyId);
            pstmt.setInt(2, studyId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setId( rs.getString("PK_INVOICE"));
                this.setInvNumber( rs.getString("inv_number"));
                this.setInvDate(rs.getString("inv_date"));
                this.setCreator( rs.getString("creator"));
                this.setInvIntervalFrom(rs.getString("inv_intervalfrom"));
                this.setInvIntervalTo(rs.getString("inv_intervalto"));
                this.setInvNotes(rs.getString("inv_notes"));
                //JM: 21Feb2008
                this.setInvStats(rs.getString("invoice_Status"));
                this.setHistoryIds(rs.getString("pk_status"));
                this.setSubTypes(rs.getString("CODELST_SUBTYP"));



                Rlog.debug("invoice", "InvoiceDao.getSavedInvoices Invoices count"
                        + id.size());

            }


        } catch (Exception ex) {
            Rlog.fatal("invoice",
                    "InvoiceDao.getSavedInvoices EXCEPTION IN FETCHING FROM er_invoice table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

    ///
    /**
     * Gets the total amount invoiced for an invoice
     *
     * @param inv Invoice PK
     *            Description of the Parameter
     */
    public double getTotalAmountInvoiced(int inv) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        double total = 0;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select sum(amount_invoiced) total from er_invoice_detail where fk_inv = ? and detail_type = 'H'");
            pstmt.setInt(1, inv);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            		total = Double.parseDouble((rs.getString("total")));

            }

            return total ;

        } catch (Exception ex) {
            Rlog.fatal("invoice",
                    "InvoiceDao.getTotalAmountInvoiced EXCEPTION IN FETCHING FROM er_invoice_detail table"
                            + ex);
            return total;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }


    /**
     * deletes the invoice details
     *
     * @param inv Invoice PK
     *            Description of the Parameter
     */
    public int deleteInvoiceDetails(int inv) {
        int ret= 0;

        PreparedStatement pstmt = null;
        Connection conn = null;


        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("Delete from er_invoice_detail where fk_inv = ? ");
            pstmt.setInt(1, inv);

           	if (pstmt.executeUpdate() >=0 )
           	{
           		ret = 0;
           	  Rlog.fatal("invoice",
                      "InvoiceDao.deleteInvoiceDetails data deleted, returning 0");
           	}
           	else
           	{
           		ret = -1;
           		Rlog.fatal("invoice",
                "InvoiceDao.deleteInvoiceDetails data not deleted -1");

           	}
           	conn.commit();

            return ret ;

        } catch (Exception ex) {
            Rlog.fatal("invoice",
                    "InvoiceDao.deleteInvoiceDetails EXCEPTION IN deleting FROM er_invoice_detail table"
                            + ex);
            return ret;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

    /**
     * checks if there any dependencies of the invoice
     *
     * @param inv Invoice PK
     *            Description of the Parameter
     */
    public boolean hasDependencies(int inv) {

        PreparedStatement pstmt = null;
        Connection conn = null;
        int total = 0;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select count(9) ct from er_milepayment_details where mp_linkto_type = 'I' and mp_linkto_id = ? ");
            pstmt.setInt(1, inv);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            		total = rs.getInt(1);

            }

            if (total > 0)
            {
            	return true;
            }
            else
            {
            	return false;

            }


        } catch (Exception ex) {
            Rlog.fatal("invoice",
                    "InvoiceDao.hasDependencies EXCEPTION IN FETCHING FROM er_invoice_detail table"
                            + ex);
            return true;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }


    /**
     * Gets the invoice number sequence from the er_invoice table
     *
     * @param studyId
     *            Study Id
     *            JM: 111706
     */
    public String getInvoiceNumber() {

        PreparedStatement pstmt = null;
        Connection conn = null;
        String invNumber=null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select SEQ_ER_INVOICE_NUM.nextval INV_NUMBER from dual");
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                invNumber =  rs.getString("INV_NUMBER");

            }
            return invNumber;

        } catch (Exception ex) {
            Rlog.fatal("invoice",
                    "InvoiceDao.getInvoiceNumber EXCEPTION IN FETCHING FROM SEQ_ER_INVOICE_NUM.nextval"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }
        return invNumber;

    }


    // end of class

}
