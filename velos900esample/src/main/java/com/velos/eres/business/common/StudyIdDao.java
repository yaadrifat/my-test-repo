/*
 * Classname			StudyIdDao.class
 * 
 * Version information 	1.0
 *
 * Date					09/24/2003
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * StudyIdDao for getting StudyId records
 * 
 * @author Sonia Sahni
 * @version : 1.0 09/24/2003
 */

public class StudyIdDao extends CommonDAO implements java.io.Serializable {
    private ArrayList id;

    private ArrayList studyIdType;

    private ArrayList studyIdTypesDesc;

    private ArrayList alternateId;

    private ArrayList study;

    private ArrayList recordType;

    private ArrayList dispType;

    private ArrayList dispData;

    private int cRows;

    public StudyIdDao() {
        id = new ArrayList();
        studyIdType = new ArrayList();
        studyIdTypesDesc = new ArrayList();
        alternateId = new ArrayList();
        study = new ArrayList();
        recordType = new ArrayList();
        dispType = new ArrayList();
        dispData = new ArrayList();

    }

    // Getter and Setter methods
    public void setId(ArrayList id) {
        this.id = id;
    }

    public void setStudyIdType(ArrayList studyIdType) {
        this.studyIdType = studyIdType;
    }

    public void setStudyIdTypesDesc(ArrayList studyIdTypesDesc) {
        this.studyIdTypesDesc = studyIdTypesDesc;
    }

    public void setAlternateId(ArrayList alternateId) {
        this.alternateId = alternateId;
    }

    public void setStudy(ArrayList study) {
        this.study = study;
    }

    public ArrayList getId() {
        return id;
    }

    public ArrayList getStudyIdType() {
        return studyIdType;
    }

    public ArrayList getStudyIdTypesDesc() {
        return studyIdTypesDesc;
    }

    public ArrayList getAlternateId() {
        return alternateId;
    }

    public ArrayList getStudy() {
        return study;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setId(Integer id) {
        this.id.add(id);
    }

    public void setStudyIdType(Integer studyIdType) {
        this.studyIdType.add(studyIdType);
    }

    public void setStudyIdTypesDesc(String studyIdTypesDesc) {
        this.studyIdTypesDesc.add(studyIdTypesDesc);
    }

    public void setAlternateId(String alternateId) {
        this.alternateId.add(alternateId);
    }

    public void setStudy(Integer study) {
        this.study.add(study);
    }

    public void setRecordType(ArrayList recordType) {
        this.recordType = recordType;
    }

    public ArrayList getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType.add(recordType);
    }

    public void setDispType(ArrayList dispType) {
        this.dispType = dispType;
    }

    public ArrayList getDispType() {
        return dispType;
    }

    public void setDispType(String disp) {
        this.dispType.add(disp);
    }

    /**
     * Returns the value of dispData.
     * 
     * @return an ArrayList of dispData (String objects)
     */
    public ArrayList getDispData() {
        return dispData;
    }

    /**
     * Sets the value of dispData.
     * 
     * @param dispData
     *            The value to assign dispData.
     */
    public void setDispData(ArrayList dispData) {
        this.dispData = dispData;
    }

    /**
     * Add a value of dispData.
     * 
     * @param dispData
     *            The value to add to dispData.
     */
    public void setDispData(String data) {
        this.dispData.add(data);
    }

    // end of getter and setter methods
    /*
     * Vishal Abrol on 07/20/2004 Added support to handle dropdown in other
     * studyids.
     */
    /**
     * Gets all Study Ids for a study. Sets the values in the class attributes
     * 
     * @param studyPK -
     *            study id for which values are retrieved.
     */

    public void getStudyIds(int studyPk,String defUserGroup) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sbSQL = new StringBuffer();
        
        int grpId = 0;
        
        grpId = StringUtil.stringToNum(defUserGroup);
        
        try {
            conn = getConnection();

            sbSQL
                    .append(" Select pk_studyid, pk_codelst, codelst_desc, STUDYID_ID,'M' record_type, codelst_seq,codelst_custom_col");
            sbSQL.append(",codelst_custom_col1 from  er_studyid, er_codelst ");
            sbSQL
                    .append(" where fk_study = ? and  pk_codelst = FK_CODELST_IDTYPE  and ");
            sbSQL.append(" codelst_type = 'studyidtype' ");
            sbSQL.append("  UNION ");
            sbSQL
                    .append("  Select 0 pk_studyid , pk_codelst, codelst_desc,' ' STUDYID_ID ,'N' record_type, codelst_seq,codelst_custom_col");
            sbSQL
                    .append(",codelst_custom_col1 from  er_codelst  where pk_codelst not in ");
            sbSQL
                    .append("(Select FK_CODELST_IDTYPE from er_studyid where   fk_study = ? ) and ");
            sbSQL.append(" codelst_type = 'studyidtype' ");
            
            
            if (grpId > 0) //append group check
            {
            	sbSQL.append(" and not exists (select * from er_codelst_hide h where h.codelst_type = 'studyidtype' and h.fk_grp = ? and h.fk_codelst = pk_codelst and h.CODELST_HIDE_TABLE = 1) ");
            	
            }
            
            
            sbSQL.append(" order by codelst_seq ");

            pstmt = conn.prepareStatement(sbSQL.toString());

            Rlog.debug("studyId", "SQL- " + sbSQL);
            pstmt.setInt(1, studyPk);
            pstmt.setInt(2, studyPk);
            
            if (grpId > 0) //append group check
            {
            	pstmt.setInt(3, grpId);
            }
            

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                setId(new Integer(rs.getInt("pk_studyid")));
                setStudyIdType(new Integer(rs.getInt("pk_codelst")));
                setStudyIdTypesDesc(rs.getString("codelst_desc"));
                setAlternateId(rs.getString("STUDYID_ID"));
                setRecordType(rs.getString("record_type"));
                setDispType(rs.getString("codelst_custom_col"));
                setDispData(rs.getString("codelst_custom_col1"));
                rows++;
                Rlog.debug("studyId", "studyIDDao.getStudyIds rows " + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("studyId",
                    "StudyIdDao.getStudyIds EXCEPTION IN FETCHING rows" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // end of class
}
