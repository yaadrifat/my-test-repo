/*
 * Classname			SiteDao.class
 * 
 * Version information 	1.0
 *
 * Date					03/26/2001
 * 
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.EnvUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * SiteDao for getting Site records
 * 
 * @author Sajal
 * @version : 1.0 03/26/2001
 */

/* Modified by Sonia Abrol, added an attribute siteIdentifier , 03/17/05 */
 
public class SiteDao extends CommonDAO implements java.io.Serializable {
    private ArrayList siteIds;

    private ArrayList siteTypes;

    private ArrayList siteAddIds;

    private ArrayList siteNames;

    private ArrayList siteInfos;

    private ArrayList siteParents;

    private ArrayList siteStats;

	private ArrayList siteNotes;  // Amarnadh

    private ArrayList siteIdentifiers;

    private ArrayList siteSequences;

    private int cRows;

    /* Added an attribute siteNotes for June Enhancement '07  #U7 */
    
    public SiteDao() {
        siteIds = new ArrayList();
        siteTypes = new ArrayList();
        siteAddIds = new ArrayList();
        siteNames = new ArrayList();
		siteNotes = new ArrayList();
        siteInfos = new ArrayList();
        siteParents = new ArrayList();
        siteStats = new ArrayList();
        siteIdentifiers = new ArrayList();
        siteSequences = new ArrayList();
    }

    // Getter and Setter methods

    public ArrayList getSiteIds() {
        return this.siteIds;
    }

    public void setSiteIds(ArrayList siteIds) {
        this.siteIds = siteIds;
    }

    public ArrayList getSiteTypes() {
        return this.siteTypes;
    }

    public void setSiteTypes(ArrayList siteTypes) {
        this.siteTypes = siteTypes;
    }

    public ArrayList getSiteAddIds() {
        return this.siteAddIds;
    }

    public void setSiteAddIds(ArrayList siteAddIds) {
        this.siteAddIds = siteAddIds;
    }

   /* Modified by Amarnadh ,Added an attribute siteNotes for June Enhancement '07  #U7 */
    
	public ArrayList getSiteNotes(){
		return this.siteNotes;
	}
    
	public void setSiteNotes(ArrayList siteNotes) {
		this.siteNotes = siteNotes;
    } 

	public ArrayList getSiteNames() {
        return this.siteNames;
    }


	public void setSiteNames(ArrayList siteNames) {
        this.siteNames = siteNames;
    }

    public ArrayList getSiteInfos() {
        return this.siteInfos;
    }

    public void setSiteInfos(ArrayList siteInfos) {
        this.siteInfos = siteInfos;
    }

    public ArrayList getSiteParents() {
        return this.siteParents;
    }

    public void setSiteParents(ArrayList siteParents) {
        this.siteParents = siteParents;
    }

    public ArrayList getSiteStats() {
        return this.siteStats;
    }

    public void setSiteStats(ArrayList siteStats) {
        this.siteStats = siteStats;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setSiteIds(Integer siteId) {
        siteIds.add(siteId);
    }

    public void setSiteTypes(String siteType) {
        siteTypes.add(siteType);
    }

    public void setSiteAddIds(Integer siteAddId) {
        siteAddIds.add(siteAddId);
    }

	public void setSiteNotes(String Notes) {
		siteNotes.add(siteNotes);
    }  

    public void setSiteNames(String siteName) {
        siteNames.add(siteName);
    }

    public void setSiteInfos(String siteInfo) {
        siteInfos.add(siteInfo);
    }

    public void setSiteParents(String siteParent) {
        siteParents.add(siteParent);
    }

    public void setSiteStats(String siteStat) {
        siteStats.add(siteStat);
    }

    /**
     * Returns the value of siteIdentifiers.
     */
    public ArrayList getSiteIdentifiers() {
        return siteIdentifiers;
    }

    /**
     * Sets the value of siteIdentifiers.
     * 
     * @param siteIdentifiers
     *            The value to assign siteIdentifiers.
     */
    public void setSiteIdentifiers(ArrayList siteIdentifiers) {
        this.siteIdentifiers = siteIdentifiers;
    }

    /**
     * Appends a siteIdentifier to siteIdentifiers.
     * 
     * @param siteIdentifier
     *            The value to assign siteIdentifier.
     */
    public void setSiteIdentifiers(String siteIdentifier) {
        this.siteIdentifiers.add(siteIdentifiers);
    }

    /**
     * Returns the value of siteSequences.
     */
    public ArrayList getSiteSequences() {
        return siteSequences;
    }

    /**
     * Sets the value of siteSequences.
     * 
     * @param siteSequences
     *            The value to assign siteSequences.
     */
    public void setSiteSequences(ArrayList siteSequences) {
        this.siteSequences = siteSequences;
    }

    /**
     * Sets the value of siteSequences.
     * 
     * @param siteSequences
     *            The value to assign siteSequences.
     */
    public void setSiteSequences(String siteSequence) {
        this.siteSequences.add(siteSequence);
    }

    // end of getter and setter methods

    /**
     * Gets all Sites for an AccountModel. Sets the values in the class attributes
     * 
     * @param groupId
     *            Id of the Group
     */

    //Query Modified by Manimaran for Enh#U11.
    
    public void getSiteValues(int accountId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            /* Added an attribute siteNotes for June Enhancement '07  #U7 */
            pstmt = conn.prepareStatement("select b.PK_SITE, "
                            + "(select CODELST_DESC from er_codelst where PK_CODELST = b.FK_CODELST_TYPE) as SITE_TYPE, "
                            + "b.FK_PERADD, "
							+ "b.SITE_NOTES,"
                            + "b.SITE_NAME, "
                            + "b.SITE_INFO, "
                            + "(select a.SITE_NAME from ER_SITE a where a.PK_SITE = b.SITE_PARENT) as PARENT_SITE,"
                            + "b.SITE_STAT, b.site_id,b.site_seq from ER_SITE b where b.FK_ACCOUNT = ? and b.site_hidden <> 1"
                            + " order by lower(SITE_NAME) ");

            pstmt.setInt(1, accountId);

            ResultSet rs = pstmt.executeQuery();
            
          /*  Added an attribute siteNotes for June Enhancement '07  #U7 */

            while (rs.next()) {
                setSiteIds(new Integer(rs.getInt("PK_SITE")));
                setSiteTypes(rs.getString("SITE_TYPE"));
                setSiteAddIds(new Integer(rs.getInt("FK_PERADD")));
				setSiteNotes(rs.getString("SITE_NOTES"));
                setSiteNames(rs.getString("SITE_NAME"));
                setSiteInfos(rs.getString("SITE_INFO"));
                setSiteParents(rs.getString("PARENT_SITE"));
                setSiteStats(rs.getString("SITE_STAT"));
                setSiteIdentifiers(rs.getString("SITE_ID"));
                setSiteSequences(rs.getString("SITE_SEQ"));
                rows++;
                Rlog.debug("site", "SiteDao.getSiteValues rows " + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("site","SiteDao.getSiteValues EXCEPTION IN FETCHING FROM Site table" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Gets all Sites for study team members. Sets the values in the class
     * attributes
     * 
     * @param studyId
     *            Id of the Group
     */

    public void getSiteValuesForStudy(int studyId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select distinct fk_siteid, site_name "
                            + " from  er_studyteam, er_user, er_site "
                            + " where er_studyteam.fk_user=er_user.pk_user "
                            + " and er_user.fk_siteid = er_site.pk_site "
                            + " and er_studyteam.fk_study = ? "
                            + " order by lower(site_name) ");
            pstmt.setInt(1, studyId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setSiteIds(new Integer(rs.getInt("fk_siteid")));
                setSiteNames(rs.getString("SITE_NAME"));
                rows++;
                Rlog .debug("site", "SiteDao.getSiteValuesForStudy rows " + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("site","SiteDao.getSiteValuesForStudy EXCEPTION IN FETCHING FROM Site table" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    /**
     * Description of the Method
     *
     * @param dName
     *            Description of the Parameter
     * @param selValue
     *            Description of the Parameter
     * @param prop
     *            Description of the Parameter
     * @return Description of the Return Value
     */
    public String toPullDown(String dName, int selValue, String prop) {
        Integer siteId = null;
        String hideStr = "";
        boolean selectedFlag = false;

        StringBuffer mainStr = new StringBuffer();

        try {

            int counter = 0;
            mainStr.append("<SELECT NAME=" + dName + " " + prop + ">");
            Integer val = new java.lang.Integer(selValue);

            for (counter = 0; counter <= siteIds.size() - 1; counter++) {
            	siteId = (Integer) siteIds.get(counter);

                if (selValue == siteId.intValue())
                {
                    mainStr.append("<OPTION value = " + siteId + " SELECTED>" + siteNames.get(counter) + "</OPTION>");
                    selectedFlag = true;
                }
                else
                {
              		mainStr.append("<OPTION value = " + siteId + ">"+ siteNames.get(counter) + "</OPTION>");
                }
            }
            if (val.toString().equals("") || val.toString().equals(null) || val.toString().equals("0") || selectedFlag == false ) {
                mainStr.append("<OPTION value ='' SELECTED>Select an Option</OPTION>");
            } else {
                mainStr.append("<OPTION value ='' >Select an Option</OPTION>");
            }

            mainStr.append("</SELECT>");
        } catch (Exception e) {
            return "Exception in toPullDown" + e;
        }
        return mainStr.toString();
    }
    

    /**
     * Gets all Sites for an AccountModel and site type. Sets the values in the class
     * attributes
     * 
     * @param accountId
     * @param siteType
     */

    public void getSiteValues(int accountId, String siteType) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            
            /* Added an attribute siteNotes for June Enhancement '07  #U7 */

            String sqlSite = "select b.PK_SITE,  c.CODELST_DESC as SITE_TYPE, b.FK_PERADD, b.SITE_NAME, b.SITE_NOTES,b.SITE_INFO, "
                    + "(select a.SITE_NAME from ER_SITE a where a.PK_SITE = b.SITE_PARENT) as PARENT_SITE, "
                    + "b.SITE_STAT from ER_SITE b , ER_CODELST c where b.FK_ACCOUNT = ? and c.pk_codelst = b.FK_CODELST_TYPE and CODELST_SUBTYP = '"
                    + siteType + "'";

            Rlog.debug("site", "sqlSite " + sqlSite);

            Rlog.debug("site", "accountid " + accountId);
            Rlog.debug("site", "siteType " + siteType);

            pstmt = conn.prepareStatement(sqlSite);

            pstmt.setInt(1, accountId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	
            	/* Added an attribute siteNotes for June Enhancement '07  #U7 */
            	
                setSiteIds(new Integer(rs.getInt("PK_SITE")));
                setSiteTypes(rs.getString("SITE_TYPE"));
                setSiteAddIds(new Integer(rs.getInt("FK_PERADD")));
				setSiteNotes(rs.getString("SITE_NOTES"));
                setSiteNames(rs.getString("SITE_NAME"));
                setSiteInfos(rs.getString("SITE_INFO"));
                setSiteParents(rs.getString("PARENT_SITE"));
                setSiteStats(rs.getString("SITE_STAT"));
                rows++;
                Rlog.debug("site", "SiteDao.getSiteValues rows " + rows);
            }

            setCRows(rows);
        } catch (Exception ex) {
            Rlog.fatal("site", "SiteDao.getSiteValues EXCEPTION IN FETCHING FROM Site table" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Gets all Sites for an AccountModel and site type code id in . 
     * Sets the values in the class
     * attributes
     * 
     * @param accountId
     * @param siteType
     */

    public void getBySiteTypeCodeId(int accountId, String siteType) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            
            /* Added an attribute siteNotes for June Enhancement '07  #U7 */

            String sqlSite = "select b.PK_SITE,  c.CODELST_DESC as SITE_TYPE, b.FK_PERADD, b.SITE_NAME, b.SITE_NOTES,b.SITE_INFO, "
                    + "(select a.SITE_NAME from ER_SITE a where a.PK_SITE = b.SITE_PARENT) as PARENT_SITE, "
                    + "b.SITE_STAT from ER_SITE b , ER_CODELST c where b.FK_ACCOUNT = ? and c.pk_codelst = b.FK_CODELST_TYPE "
                    + "and c.pk_codelst in (" + siteType + ")"
                    + " order by lower(b.SITE_NAME) ";

            Rlog.debug("site", "sqlSite " + sqlSite);
            Rlog.debug("site", "accountid " + accountId);
            Rlog.debug("site", "siteType " + siteType);

            pstmt = conn.prepareStatement(sqlSite);
            pstmt.setInt(1, accountId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	
            	/* Added an attribute siteNotes for June Enhancement '07  #U7 */
            	
                setSiteIds(new Integer(rs.getInt("PK_SITE")));
                setSiteTypes(rs.getString("SITE_TYPE"));
                setSiteAddIds(new Integer(rs.getInt("FK_PERADD")));
				setSiteNotes(rs.getString("SITE_NOTES"));
                setSiteNames(rs.getString("SITE_NAME"));
                setSiteInfos(rs.getString("SITE_INFO"));
                setSiteParents(rs.getString("PARENT_SITE"));
                setSiteStats(rs.getString("SITE_STAT"));
                rows++;
                Rlog.debug("site", "SiteDao.getSiteValues rows " + rows);
            }

            setCRows(rows);
        } catch (Exception ex) {
            Rlog.fatal("site", "SiteDao.getSiteValues EXCEPTION IN FETCHING FROM Site table" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // ///////////////////////////////////////

    /**
     * Gets the next site sequence for an AccountModel
     * 
     * @param accountId
     *            Id of the AccountModel
     */

    public static int getNextSiteSequence(int accountId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        int curSeq = 0;
        try {
            conn = EnvUtil.getConnection();

            pstmt = conn .prepareStatement("select nvl(max(site_seq),0) from er_site where fk_account = ?");
            pstmt.setInt(1, accountId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                curSeq = rs.getInt(1);
                Rlog.debug("site", "SiteDao.getNextSiteSequence  curSeq" + curSeq);
            }
            curSeq = curSeq + 1;
            return curSeq;
        } catch (SQLException ex) {
            Rlog.fatal("site", "SiteDao.getNextSiteSequence EXCEPTION " + ex);
            return curSeq;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // added new method for clickable sorting in Manage Accounts >>
    // Organizations on 29th March
    public void getSiteValues(int accountId, String orderBy, String orderType) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            
            /* Added an attribute siteNotes for June Enhancement '07  #U7 */
            
            pstmt = conn .prepareStatement("select b.PK_SITE, "
                            + "(select CODELST_DESC from er_codelst where PK_CODELST = b.FK_CODELST_TYPE) as SITE_TYPE, "
                            + "b.FK_PERADD, "
							+ "b.SITE_NOTES,"
                            + "b.SITE_NAME, "
                            + "b.SITE_INFO, "
                            + "(select a.SITE_NAME from ER_SITE a where a.PK_SITE = b.SITE_PARENT) as PARENT_SITE,"
                            + "b.SITE_STAT, b.site_id,b.site_seq ,b.site_notes from ER_SITE b where b.FK_ACCOUNT = ? order by lower("
                            + orderBy + ") " + orderType + " ");

            pstmt.setInt(1, accountId);

            ResultSet rs = pstmt.executeQuery();
            
           /* Added an attribute siteNotes for June Enhancement '07  #U7 */

            while (rs.next()) {
                setSiteIds(new Integer(rs.getInt("PK_SITE")));
                setSiteTypes(rs.getString("SITE_TYPE"));
                setSiteAddIds(new Integer(rs.getInt("FK_PERADD")));
				setSiteNotes(rs.getString("SITE_NOTES"));
                setSiteNames(rs.getString("SITE_NAME"));
                setSiteInfos(rs.getString("SITE_INFO"));
                setSiteParents(rs.getString("PARENT_SITE"));
                setSiteStats(rs.getString("SITE_STAT"));
                setSiteIdentifiers(rs.getString("SITE_ID"));
                setSiteSequences(rs.getString("SITE_SEQ"));
                rows++;
                Rlog.debug("site", "SiteDao.getSiteValues rows " + rows);
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("site", "SiteDao.getSiteValues EXCEPTION IN FETCHING FROM Site table" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    // //////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Gets the site PK
     * 
     * @param siteId
     *            Unique Site Id
     * @return Site Primary Key
     */

    public static int getSitePK(String siteId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int sitePk = 0;
        try {
            conn = EnvUtil.getConnection();

            pstmt = conn
                    .prepareStatement("select pk_site from er_site where lower(trim(SITE_ID)) = lower(trim(?))");
            pstmt.setString(1, siteId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                sitePk = rs.getInt(1);
                Rlog.debug("site", "SiteDao.getSitePK(String siteId) sitePk"
                        + sitePk);
            }
            return sitePk;
        } catch (SQLException ex) {
            Rlog.fatal("site", "SiteDao.getSitePK(String siteId) EXCEPTION "
                    + ex);
            return sitePk;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    /**
     * Gets the site PK from the site name (er_site.site_name)
     * 
     * @param siteId
     *            Unique Site Name
     * @param pkAccount
     *            Primary Key of site account
     * @return Site Primary Key
     */

    public static int getSitePKBySiteName(String siteName, int pkAccount) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int sitePk = 0;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select pk_site from er_site where lower(trim(SITE_NAME)) = lower(trim(?)) and fk_account = ? ");
            pstmt.setString(1, siteName);
            pstmt.setInt(2, pkAccount);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                sitePk = rs.getInt(1);
                Rlog.debug("site","SiteDao.getSitePKBySiteName(String siteId, int pkAccount ) sitePk" + sitePk);
            }
            return sitePk;
        } catch (SQLException ex) {
            Rlog.fatal("site","SiteDao.getSitePKBySiteName(String siteId, int pkAccount ) EXCEPTION " + ex);
            return sitePk;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    /**
     * Gets the site PK
     * 
     * @param siteId
     *            Unique Site Id
     * @param pkAccount
     *            Primary Key of site account
     * @return Site Primary Key
     */

    public static int getSitePK(String siteId, int pkAccount) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int sitePk = 0;
        try {
            conn = getConnection();

            pstmt = conn.prepareStatement("select pk_site from er_site where lower(trim(SITE_ID)) = lower(trim(?)) and fk_account = ? ");
            pstmt.setString(1, siteId);
            pstmt.setInt(2, pkAccount);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                sitePk = rs.getInt(1);
                Rlog.debug("site","SiteDao.getSitePK(String siteId, int pkAccount ) sitePk" + sitePk);
            }
            return sitePk;
        } catch (SQLException ex) {
            Rlog.fatal("site","SiteDao.getSitePK(String siteId, int pkAccount ) EXCEPTION " + ex);
            return sitePk;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

    /**
     * Fetches the site's PK based on the altId
     * @param siteId er_site.site_altid
     * @return pk of the site
     */
    public static int getSitePKFromAltId(String siteAltId,int pkAccount) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        int sitePk = 0;
        try {
            conn = EnvUtil.getConnection();

            pstmt = conn
                    .prepareStatement("select pk_site from er_site where lower(trim(site_altid)) = lower(trim(?)) and fk_account = ? ");
            pstmt.setString(1, siteAltId);
            pstmt.setInt(2, pkAccount);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                sitePk = rs.getInt(1);
                Rlog.debug("site", "SiteDao.getSitePKFromAltId(String siteId) sitePk"
                        + sitePk);
            }
            return sitePk;
        } catch (SQLException ex) {
            Rlog.fatal("site", "SiteDao.getSitePKFromAltId(String siteId) EXCEPTION "
                    + ex);
            return sitePk;
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
    /**
     * Get the list of sites that a user has access to.
     * 
     * @param userId
     */
    public void getSitesByUser(int userId) {
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = EnvUtil.getConnection();
            pstmt = conn.prepareStatement("select pk_site from ERV_ALLUSERSITES_DISTINCT where fk_user = ? and site_hidden <> 1 ");
            pstmt.setInt(1, userId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
            	siteIds.add(rs.getInt("pk_site"));
            }
        } catch (Exception e) {
            Rlog.fatal("site", "SiteDao.getSitesByUser EXCEPTION: "+e);
        } finally {
            try {
            	if (pstmt != null) { pstmt.close(); }
            } catch (Exception e) {}
            try {
                if (conn != null) { conn.close(); }
            } catch (Exception e) {}
        }
    }
    
    // end of class
}
