/*
 * Classname : FormSecBean
 * 
 * Version information: 1.0
 *
 * Date: 09/22/2003
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Sonia Sahni
 */

package com.velos.eres.business.studyId.impl;

/* Import Statements */

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* End of Import Statements */

/**
 * The StudyIdBean CMP entity bean.<br>
 * <br>
 * 
 * @author Sonia Sahni
 * @vesion 1.0 09/22/2003
 * @ejbHome StudyIdHome
 * @ejbRemote StudyIdRObj
 */

@Entity
@Table(name = "ER_STUDYID")
public class StudyIdBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3762818198939972662L;

    /**
     * The primary key
     */

    public int id;

    /**
     * The study pk
     */
    public Integer studyId;

    /**
     * The Id Type
     */

    public Integer studyIdType;

    /**
     * The Alternate Study Id
     */
    public String alternateStudyId;

    /**
     * The creator
     */

    public Integer creator;

    /**
     * The last modification by: LAST_MODIFICATION_BY
     */

    public Integer modifiedBy;

    /**
     * The IP Address : IP_ADD
     */

    public String ipAdd;

    private ArrayList studyIdBeans;

    private String recordType;

    // //////////////////////////////////////////////////////////////////////////////////////

    /**
     * 
     * 
     * @return formSecId
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STUDYID", allocationSize=1)
    @Column(name = "PK_STUDYID")
    public int getId() {
        return id;
    }

    /**
     * 
     * 
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * 
     * @return formLibId
     */
    @Column(name = "FK_STUDY")
    public String getStudyId() {
        return StringUtil.integerToString(studyId);

    }

    /**
     * 
     * 
     * @param studyId
     */
    public void setStudyId(String studyId) {
        if (!StringUtil.isEmpty(studyId))
            this.studyId = Integer.valueOf(studyId);
    }

    public void setStudyIdType(String studyIdType) {
        if (!StringUtil.isEmpty(studyIdType))
            this.studyIdType = Integer.valueOf(studyIdType);

    }

    @Column(name = "FK_CODELST_IDTYPE")
    public String getStudyIdType() {
        return StringUtil.integerToString(studyIdType);

    }

    public void setAlternateStudyId(String alternateStudyId) {
        this.alternateStudyId = alternateStudyId;
    }

    @Column(name = "STUDYID_ID")
    public String getAlternateStudyId() {
        return alternateStudyId;
    }

    /**
     * @return the Creator
     */
    @Column(name = "CREATOR")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);

    }

    /**
     * @param takes
     *            a string
     */

    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }

    }

    /**
     * @return the unique ID of the User
     */
    @Column(name = "LAST_MODIFIED_BY")
    public String getModifiedBy() {

        return StringUtil.integerToString(this.modifiedBy);

    }

    /**
     * @param the
     *            set unique ID of the User
     */

    public void setModifiedBy(String modifiedBy) {

        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }

    }

    /**
     * @return the IP Address of the Form Notification request
     */
    @Column(name = "IP_ADD")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param set
     *            the IP Address of the Form Notification request
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public void setStudyIdBeans(ArrayList StudyIdStateBeans) {
        this.studyIdBeans = StudyIdStateBeans;
    }

    @Transient
    public ArrayList getStudyIdBeans() {
        return studyIdBeans;
    }

    public void setStudyIdBeans(StudyIdBean StudyIdBean) {
        this.studyIdBeans.add(StudyIdBean);
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    @Transient
    public String getRecordType() {
        return recordType;
    }

    // END OF THE GETTER AND SETTER METHODS

    // ////////////////////////////////////////////////////////////////////

    /**
     * creates a study alternate id record.
     * 
     * @param ssk
     *            the StateKeeper containing details of the study alternate id
     */

    /*
     * public StudyIdStateKeeper getStudyIdStateKeeper() { Rlog.debug("studyId",
     * "StudyIdBean.getStudyIdStateKeeper"); return new
     * StudyIdStateKeeper(getId(), getStudyId(), getStudyIdType(),
     * getAlternateStudyId(), getCreator(), getModifiedBy(), getIpAdd()); }
     */

    /**
     * sets up a state keeper containing details of the study id
     */

    /*
     * public void setStudyIdStateKeeper(StudyIdStateKeeper ssk) { GenerateId
     * stId = null; try { Connection conn = null; conn = getConnection();
     * 
     * Rlog.debug("studyId", "StudyIdBean.setStudyIdStateKeeper() Connection :" +
     * conn);
     * 
     * this.id = stId.getId("SEQ_ER_STUDYID", conn);
     * 
     * Rlog.debug("studyId", "StudyIdBean.setStudyIdStateKeeper() studyId :" +
     * this.studyId); conn.close();
     * 
     * setStudyId(ssk.getStudyId()); setStudyIdType(ssk.getStudyIdType());
     * setAlternateStudyId(ssk.getAlternateStudyId());
     * setCreator(ssk.getCreator()); setIpAdd(ssk.getIpAdd()); } catch
     * (Exception e) { Rlog.fatal("studyId", "Error in setFormSecStateKeeper()
     * in FormSecBean " + e); } }
     */

    /**
     * updates the Form studyId Record
     * 
     * @param studyIdssk
     * @return int 0: in case of successful updation; -2 in caseof exception
     */
    public int updateStudyId(StudyIdBean ssk) {
        try {
            setStudyId(ssk.getStudyId());
            setStudyIdType(ssk.getStudyIdType());
            setAlternateStudyId(ssk.getAlternateStudyId());
            setModifiedBy(ssk.getModifiedBy());
            setIpAdd(ssk.getIpAdd());
            setCreator(ssk.getCreator());
            return 0;
        } catch (Exception e) {
            Rlog.fatal("studyId", " error in studyIdBean.updateStudyId");
            return -2;
        }
    }

    public StudyIdBean() {
        this.studyIdBeans = new ArrayList();
    }

    public StudyIdBean(int id, String studyId, String studyIdType,
            String alternateStudyId, String creator, String modifiedBy,
            String ipAdd) {
        super();
        this.studyIdBeans = new ArrayList();
        // TODO Auto-generated constructor stub
        setId(id);
        setStudyId(studyId);
        setStudyIdType(studyIdType);
        setAlternateStudyId(alternateStudyId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);

    }

}
