package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.CLOB;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.EnvUtil;

public class FormActionDao extends CommonDAO implements java.io.Serializable {

		private ArrayList sourceFormIds;

	    private ArrayList targetFormIds;

	    private ArrayList operators;

	    private ArrayList sourceFormFields;

	    private ArrayList targetFormFields;
	    
	    private ArrayList srcFldUIds;
	    
	    private ArrayList trgFldUIds;
	    //060508ML
	    private ArrayList formActionIds;
	    
	    
	    
	    private int rows;
	    
	    public int getRows() {
			return rows;
		}

		public void setRows(int rows) {
			this.rows = rows;
		}

		public ArrayList getOperators() {
			return operators;
		}

		public void setOperators(ArrayList operators) {
			this.operators = operators;
		}
		
		public void setOperators(String operator) {
		        this.operators.add(operator);
		    }
		
		public ArrayList getSourceFormFields() {
			return sourceFormFields;
		}
		

		public void setSourceFormFields(ArrayList sourceFormFields) {
			this.sourceFormFields = sourceFormFields;
		}
		public void setSourceFormFields(String sourceFormField) {
			this.sourceFormFields.add(sourceFormField);
		}


		public ArrayList getSourceFormIds() {
			return sourceFormIds;
		}

		public void setSourceFormIds(ArrayList sourceFormIds) {
			this.sourceFormIds = sourceFormIds;
		}
		public void setSourceFormIds(Integer sourceFormId) {
			this.sourceFormIds.add(sourceFormId);
		}
		public ArrayList getTargetFormFields() {
			return targetFormFields;
		}

		public void setTargetFormFields(ArrayList targetFormFields) {
			this.targetFormFields = targetFormFields;
		}
		public void setTargetFormFields(String targetFormField) {
			this.targetFormFields.add(targetFormField);
		}

		public ArrayList getTargetFormIds() {
			return targetFormIds;
		}
		
		public void setTargetFormIds(Integer targetFormId) {
	        this.targetFormIds.add(targetFormId);
	    }

		public void setTargetFormIds(ArrayList targetFormIds) {
			this.targetFormIds = targetFormIds;
		}
		//060508ML
		public ArrayList getFormActionIds() {
			return formActionIds;
		}
		
		public void setFormActionIds(Integer formActionId) {
	        this.formActionIds.add(formActionId);
	    }

		public void setFormActionIds(ArrayList formActionIds) {
			this.formActionIds = formActionIds;
		}
		
		//end of 060508ML
		
		public ArrayList getSrcFldUIds() {
			return srcFldUIds;
		}

		public void setSrcFldUIds(ArrayList srcFldUIds) {
			this.srcFldUIds = srcFldUIds;
		}

		public void setSrcFldUIds(String srcFldUId) {
			this.srcFldUIds.add(srcFldUId);
		}

		public ArrayList getTrgFldUIds() {
			return trgFldUIds;
		}

		public void setTrgFldUIds(ArrayList trgFldUIds) {
			this.trgFldUIds = trgFldUIds;
		}
		
		public void setTrgFldUIds(String trgFldUId) {
			this.trgFldUIds.add(trgFldUId);
		}

		public FormActionDao() {
	    	sourceFormIds = new ArrayList();
	    	targetFormIds = new ArrayList();
	    	operators = new ArrayList();
	    	sourceFormFields = new ArrayList();
	    	targetFormFields = new ArrayList();
	    	srcFldUIds=new ArrayList();
	    	trgFldUIds=new ArrayList();
	    	formActionIds=new ArrayList();
	    	
	    }
		
		public void getFieldActions(String formId) {
			StringBuffer sbFfldSQL = new StringBuffer();
			PreparedStatement pstmtFld = null;
		    Connection conn = null;		        
		    int rows = 0;
		    try{
	// 060508ML add PK_Formaction		
			 sbFfldSQL.append(" select PK_FORMACTION,FORMACTION_SOURCEFORM,FORMACTION_SOURCEFLD,FORMACTION_OPERATOR,FORMACTION_TARGETFORM,FORMACTION_TARGETFLD");
			 sbFfldSQL.append(", (select fld_uniqueid from erv_formflds where FORMACTION_SOURCEFORM=pk_formlib and FORMACTION_SOURCEFLD=fld_systemid) as srcfld_uniqueid ");
			 sbFfldSQL.append(", (select fld_uniqueid from erv_formflds where FORMACTION_TARGETFORM=pk_formlib and FORMACTION_TARGETFLD=fld_systemid) as trgfld_uniqueid ");
			 sbFfldSQL.append(" from  ER_FORMACTION where FORMACTION_SOURCEFORM=?");
			 
	         conn = EnvUtil.getConnection();
			 pstmtFld = conn.prepareStatement(sbFfldSQL.toString());

	            pstmtFld.setString(1, formId);
	            
	            ResultSet rs = pstmtFld.executeQuery();
	            while(rs.next()){
	            	rows++;	            	
	            	// 060508ML add setFormActionIds          	           		
	            		setSourceFormIds(Integer.parseInt(formId));
	            		setFormActionIds(rs.getInt("PK_FORMACTION"));
	            		setTargetFormIds(rs.getInt("FORMACTION_TARGETFORM"));
	            		setOperators(rs.getString("FORMACTION_OPERATOR"));
	            		setTargetFormFields(rs.getString("FORMACTION_TARGETFLD"));
	            		setSourceFormFields(rs.getString("FORMACTION_SOURCEFLD"));
	            		this.setSrcFldUIds((rs.getString("srcfld_uniqueid")));
	            		this.setTrgFldUIds((rs.getString("trgfld_uniqueid")));
	            		
	            
	            
	            }
			}catch (Exception ex) {
	            Rlog.fatal("formlib",
	                    "FORMACTIONDAO.getFieldActions -- > Exception " + ex);
	            ex.printStackTrace();
	            
	        } finally {
	            try {
	                if (pstmtFld != null)
	                    pstmtFld.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }
	        }
	}

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
