package com.velos.eres.business.mdb;


import java.util.Hashtable;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import com.velos.eres.business.common.ExpToFormDao;

/*
 Author : Sonia Abrol
 Date : 	 11/21/06
 */
@MessageDriven(activationConfig = {
       @ActivationConfigProperty(propertyName="destinationType",
         propertyValue="javax.jms.Topic"),
       @ActivationConfigProperty(propertyName="destination",
         propertyValue="topic/migrateFormVer")})
public class MigrateVersionMDB implements  MessageListener {
    private MessageDrivenContext ctx = null;

    public MigrateVersionMDB() {
    }


    // --- MessageListener
    public void onMessage(Message message) {
        try {
            String modifiedBy = new String();
            String formId= new String();
            String formLibVer = "";
            String formType = "";
            String ipAdd = "";
            
            Hashtable htData = new Hashtable();
            ObjectMessage objMessage = null;
            

            if (message instanceof ObjectMessage) {
                objMessage = (ObjectMessage) message;

                System.out.println("MigrateVersionMDB Bean got message. The information is:");

                modifiedBy = objMessage.getStringProperty("modifiedBy");
                formId = objMessage.getStringProperty("formId");
                formLibVer = objMessage.getStringProperty("formLibVer");
                formType = objMessage.getStringProperty("formType");
                ipAdd = objMessage.getStringProperty("ipAdd");

                System.out.println("\n formId : " + formId);
                System.out.println("\n modifiedBy : " + modifiedBy);
                System.out.println("\n formLibVer : " + formLibVer);
                System.out.println("\n formType : " + formType);
                System.out.println("\n ipAdd : " + ipAdd);

                ExpToFormDao exp = new ExpToFormDao();
                exp.migrateFormToLatestVersion(formId , formType, formLibVer, ipAdd, modifiedBy);
            } else {
                System.out
                        .println("MigrateVersionMDB Received a Message of wrong type: Expected ObjectMessage, Received "
                                + message.getClass().getName());
            }

        } catch (Exception e) { //catch (javax.jms.JMSException e) {
            System.out.println("Exception in MigrateVersionMDB Bean:  " + e);
            e.printStackTrace();
            
        }
    }

} // MigrateVersionMDB
