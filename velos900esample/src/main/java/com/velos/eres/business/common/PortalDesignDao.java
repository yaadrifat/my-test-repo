 
/*
 * Classname			PortalDesignDao.class
 * 
 * Version information 	1.0
 *
 * Date					04/23/2007
 * 
 * Copyright notice		Velos, Inc.
 * 
 * Author 				Manimaran
 */

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import com.velos.eres.service.util.Rlog;
import java .sql.Clob;




public class PortalDesignDao extends CommonDAO implements java.io.Serializable {
    
	private ArrayList portalModIds;

    private ArrayList fkIds;
    
    private ArrayList portalModTypes;

    private ArrayList portalModAligns;
    
    private ArrayList portalModFroms;

    private ArrayList portalModTos;
    
    private ArrayList portalModFromUnits;
    
    private ArrayList portalModToUnits;

    private ArrayList portalModFormResps;

    private ArrayList portalKeys;    //
    
    private ArrayList portalBgColors;    

    private ArrayList portalHeaders;

    private ArrayList portalFooters;
    
	public PortalDesignDao() {
		portalModIds = new ArrayList();
		fkIds = new ArrayList();
		portalModTypes = new ArrayList();
		portalModAligns = new ArrayList();
		portalModFroms = new ArrayList();
		portalModTos = new ArrayList();
		portalModFromUnits= new ArrayList();
		portalModToUnits = new ArrayList();
		portalModFormResps = new ArrayList();
		portalKeys = new ArrayList();//JM
    	portalBgColors = new ArrayList();    	
    	portalHeaders = new ArrayList();
    	portalFooters = new ArrayList();    	
    	
   } 
    
    
	  // Getter and Setter methods

    public ArrayList getPortalModIds() {
        return this.portalModIds;
    }

    public void setPortalModIds(ArrayList portalModIds) {
        this.portalModIds = portalModIds;
    }
    
    public void setPortalModIds(Integer portalModId) {
        this.portalModIds.add(portalModId);
    }
    
    public ArrayList getFkIds() {
        return this.fkIds;
    }

    public void setFkIds(ArrayList fkIds) {
        this.fkIds = fkIds;
    }
    
    public void setFkIds(String fkId) {
        this.fkIds.add(fkId);
    }
    
    public ArrayList getPortalModTypes() {
        return this.portalModTypes;
    }

    public void setPortalModTypes(ArrayList portalModTypes) {
        this.portalModTypes = portalModTypes;
    }
    
    public void setPortalModTypes(String portalModType) {
        this.portalModTypes.add(portalModType);
    }
    
    public ArrayList getPortalModAligns() {
        return this.portalModAligns;
    }

    public void setPortalModAligns(ArrayList portalModAligns) {
        this.portalModAligns = portalModAligns;
    }
    
    public void setPortalModAligns(String portalModAlign) {
        this.portalModAligns.add(portalModAlign);
    }
    
    public ArrayList getPortalModFroms() {
        return this.portalModFroms;
    }

    public void setPortalModFroms(ArrayList portalModFroms) {
        this.portalModFroms = portalModFroms;
    }
    
    public void setPortalModFroms(Integer portalModFrom) {
        this.portalModFroms.add(portalModFrom);
    }
    
    public ArrayList getPortalModTos() {
        return this.portalModTos;
    }

    public void setPortalModTos(ArrayList portalModTos) {
        this.portalModTos = portalModTos;
    }
    
    public void setPortalModTos(Integer portalModTo) {
        this.portalModTos.add(portalModTo);
    }
    
    
    public ArrayList getPortalModFromUnits() {
        return this.portalModFromUnits;
    }

    public void setPortalModFromUnits(ArrayList portalModFromUnits) {
        this.portalModFromUnits = portalModFromUnits;
    }
    
    public void setPortalModFromUnits(String portalModFromUnit) {
        this.portalModFromUnits.add(portalModFromUnit);
    }
    
    public ArrayList getPortalModToUnits() {
        return this.portalModToUnits;
    }

    public void setPortalModToUnits(ArrayList portalModToUnits) {
        this.portalModToUnits = portalModToUnits;
    }
    
    public void setPortalModToUnits(String portalModToUnit) {
        this.portalModToUnits.add(portalModToUnit);
    }
    
    public ArrayList getPortalModFormResps() {
        return this.portalModFormResps;
    }

    public void setPortalModFormResps(ArrayList portalModFormResps) {
        this.portalModFormResps = portalModFormResps;
    }
    
    public void setPortalModFormResps(String portalModFormResp) {
        this.portalModFormResps.add(portalModFormResp);
    }
    
    //JM: 14Aug2007
    public ArrayList getPortalKeys() {
        return this.portalKeys;
    }

    public void setPortalKeys(ArrayList portalKeys) {
        this.portalKeys = portalKeys;
    }
    
    public void setPortalKeys(String portalKey) {
        this.portalKeys.add(portalKey);
    }
    //JM:
    
    public ArrayList getPortalBgColors() {
        return this.portalBgColors;
    }

    public void setPortalBgColors(ArrayList portalBgColors) {
        this.portalBgColors = portalBgColors;
    }
    
    public void setPortalBgColors(String portalBgColor) {
        this.portalBgColors.add(portalBgColor);
    }
    
    public ArrayList getPortalHeaders() {
        return this.portalHeaders;
    }

    public void setPortalHeaders(ArrayList portalHeaders) {
        this.portalHeaders = portalHeaders;
    }
    
    public void setPortalHeaders(String portalHeader) {
        this.portalHeaders.add(portalHeader);
    }
    
    public ArrayList getPortalFooters() {
        return this.portalFooters;
    }

    public void setPortalFooters(ArrayList portalFooters) {
        this.portalFooters = portalFooters;
    }
    
    public void setPortalFooters(String portalFooter) {
        this.portalFooters.add(portalFooter);
    }
    
    public void getPortalModValues(int portalId) {    
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String headerStr ="";
        String footerStr ="";
        Clob headerClob = null;
        Clob footerClob =null;
        try {
            conn = getConnection();
            pstmt = conn
            .prepareStatement(" select pk_portal, pk_portal_modules,fk_id,pm_type,pm_align,pm_from,pm_to,pm_from_unit,pm_to_unit,pm_form_after_resp, "+
            		"portal_bgcolor,portal_header,portal_footer from er_portal b left outer join  er_portal_modules a  on  b.pk_portal=a.fk_portal where b.pk_portal = ?");
                                    
            pstmt.setInt(1, portalId);            
            ResultSet rs = pstmt.executeQuery();            	
            while (rs.next()) {       
            	setPortalKeys(rs.getString("pk_portal"));
            	setPortalModIds(new Integer(rs.getInt("pk_portal_modules")));            	
            	setFkIds(rs.getString("fk_id"));            	
            	setPortalModTypes(rs.getString("pm_type"));            	
            	setPortalModAligns(rs.getString("pm_align"));            	
            	setPortalModFroms(new Integer(rs.getInt("pm_from")));
            	setPortalModTos(new Integer(rs.getInt("pm_to")));
            	setPortalModFromUnits(rs.getString("pm_from_unit"));
            	setPortalModToUnits(rs.getString("pm_to_unit"));
            	setPortalModFormResps(rs.getString("pm_form_after_resp"));
            	setPortalBgColors(rs.getString("portal_bgcolor"));           	
            	headerClob =rs.getClob("portal_header");
            	if (headerClob!=null) {
            		headerStr = headerClob.getSubString(1,(int) headerClob.length());
            	}
            	setPortalHeaders(headerStr);
            	footerClob = rs.getClob("portal_footer");
            	if (footerClob!=null) {
            		footerStr  = footerClob.getSubString(1,(int) footerClob.length());
            	}
            	setPortalFooters(footerStr);            	
                rows++;
                Rlog.debug("portal", "PortalDesignDao.getPortalModValues rows " + rows);
            }

          
       } catch (SQLException ex) {
            Rlog.fatal("portalDesign",
                    "PortalDesignDao.getPortalModValues EXCEPTION IN FETCHING FROM portal Module table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
    
//  JM: 17Aug2007:  issue #3061
public int deletePortalModules(int modId){
	
	int success = 0;
	PreparedStatement pstmt = null;
	Connection conn = null;
	
	try{
		conn = getConnection();
		pstmt = conn.prepareCall(" delete from ER_PORTAL_MODULES where PK_PORTAL_MODULES = ?");
		pstmt.setInt(1, modId);	
		ResultSet rs = pstmt.executeQuery();
		
		rs.next();
		
		
	}catch (SQLException ex){
		 Rlog.fatal("portalDesign", "PortalDesignDao.deletePortalModules EXCEPTION IN FETCHING FROM portal Module table"
                         + ex);
		
		return -1;
		
	}
	finally{
		try {
            if (pstmt != null)
            	pstmt.close();
        } catch (Exception e) {
        }
        try {
            if (conn != null)
                conn.close();
        } catch (Exception e) {
        }
		
		
	}
		
	return success;
}

//JM: 20Aug2007: issue #3072
// When the study is changed/reset corresponding form/calendars should also get deleted from the ER_PORTAL_MODULES table 
public int deletePortalDesignData(int portalID){
	
	int success = 0;
	PreparedStatement pstmt = null;
	Connection conn = null;
	
	try{
		conn = getConnection();
		pstmt = conn.prepareCall(" delete from ER_PORTAL_MODULES where FK_PORTAL = ?");
		pstmt.setInt(1, portalID);	
		ResultSet rs = pstmt.executeQuery();
		
		rs.next();
		
		
	}catch (SQLException ex){
		 Rlog.fatal("portalDesign", "PortalDesignDao.deletePortalDesignData EXCEPTION IN FETCHING FROM portal Module table"
                         + ex);
		
		return -1;
		
	}
	finally{
		try {
            if (pstmt != null)
            	pstmt.close();
        } catch (Exception e) {
        }
        try {
            if (conn != null)
                conn.close();
        } catch (Exception e) {
        }
		
		
	}
		
	return success;
}

 /*Pass moduleTypeCode as "SCH" for schedule, "F" forms*/
public void getPortalModValues(int portalId,String moduleTypeCode) {    
    int rows = 0;
    PreparedStatement pstmt = null;
    Connection conn = null;
    String headerStr ="";
    String footerStr ="";
    Clob headerClob = null;
    Clob footerClob =null;
    
    String portalModuleType="";
    
    try {
    	
    	if (moduleTypeCode.equals("SCH"))
    	{
    		
    		portalModuleType = "'S','SF'"; 
    	}else //forms
    	{
    		portalModuleType = "'EF','LF'";
    	}
    	
    	
        conn = getConnection();
        pstmt = conn
        .prepareStatement(" select pk_portal, pk_portal_modules,fk_id,pm_type,pm_align,pm_from,pm_to,pm_from_unit,pm_to_unit,pm_form_after_resp, "+
        		"portal_bgcolor,portal_header,portal_footer from er_portal b left outer join  er_portal_modules a  on  b.pk_portal=a.fk_portal where b.pk_portal = ? and pm_type in (" +portalModuleType +")");
        
                                
        pstmt.setInt(1, portalId);            
        ResultSet rs = pstmt.executeQuery();            	
        while (rs.next()) {       
        	setPortalKeys(rs.getString("pk_portal"));
        	setPortalModIds(new Integer(rs.getInt("pk_portal_modules")));            	
        	setFkIds(rs.getString("fk_id"));            	
        	setPortalModTypes(rs.getString("pm_type"));            	
        	setPortalModAligns(rs.getString("pm_align"));            	
        	setPortalModFroms(new Integer(rs.getInt("pm_from")));
        	setPortalModTos(new Integer(rs.getInt("pm_to")));
        	setPortalModFromUnits(rs.getString("pm_from_unit"));
        	setPortalModToUnits(rs.getString("pm_to_unit"));
        	setPortalModFormResps(rs.getString("pm_form_after_resp"));
        	setPortalBgColors(rs.getString("portal_bgcolor"));           	
        	headerClob =rs.getClob("portal_header");
        	if (headerClob!=null) {
        		headerStr = headerClob.getSubString(1,(int) headerClob.length());
        	}
        	setPortalHeaders(headerStr);
        	footerClob = rs.getClob("portal_footer");
        	if (footerClob!=null) {
        		footerStr  = footerClob.getSubString(1,(int) footerClob.length());
        	}
        	setPortalFooters(footerStr);            	
            rows++;
             
        }

      
   } catch (SQLException ex) {
        Rlog.fatal("portalDesign",
                "PortalDesignDao.getPortalModValues(int portalId,String moduleTypeCode) EXCEPTION IN FETCHING FROM portal Module table"
                        + ex);
    } finally {
        try {
            if (pstmt != null)
                pstmt.close();
        } catch (Exception e) {
        }
        try {
            if (conn != null)
                conn.close();
        } catch (Exception e) {
        }

    }

}

	
}
