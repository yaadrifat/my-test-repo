package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

public class StudyINDIDEDAO extends CommonDAO implements java.io.Serializable{

	/**
	 * Generated  serialVersionUID
	 */
	private static final long serialVersionUID = 5393696226070471068L;
	
	
	private ArrayList<Integer> idIndIdeList;
    private ArrayList<Integer> typeIndIdeList;
    private ArrayList<String> numberIndIdeList;
    private ArrayList<Integer> grantorList;
    private ArrayList<Integer> holderList;
    private ArrayList<Integer> programCodeList;
    private ArrayList<Integer> expandedAccessList;
    private ArrayList<Integer> accessTypeList;
    private ArrayList<Integer> exemptIndIdeList;
    
   
    public StudyINDIDEDAO() {
    	
    	idIndIdeList = new ArrayList<Integer>();
        typeIndIdeList = new ArrayList<Integer>();
        numberIndIdeList = new ArrayList<String>();
        grantorList = new ArrayList<Integer>();
        holderList = new ArrayList<Integer>();
        programCodeList = new ArrayList<Integer>();
        expandedAccessList = new ArrayList<Integer>();
        accessTypeList = new ArrayList<Integer>();
        exemptIndIdeList = new ArrayList<Integer>();

   }
	
    public ArrayList<Integer> getIdIndIdeList() {
		return idIndIdeList;
	}

	public void setIdIndIdeList(ArrayList<Integer> idIndIdeList) {
		this.idIndIdeList = idIndIdeList;
	}

	public void setIdIndIdeList(Integer idIndIdeList) {
		this.idIndIdeList.add(idIndIdeList);
	}





	public ArrayList<Integer> getTypeIndIdeList() {
		return typeIndIdeList;
	}

	public void setTypeIndIdeList(ArrayList<Integer> typeIndIdeList) {
		this.typeIndIdeList = typeIndIdeList;
	}

	public void setTypeIndIdeList(Integer typeIndIdeList) {
		this.typeIndIdeList.add(typeIndIdeList);
	}





	public ArrayList<String> getNumberIndIdeList() {
		return numberIndIdeList;
	}

	public void setNumberIndIdeList(ArrayList<String> numberIndIdeList) {
		this.numberIndIdeList = numberIndIdeList;
	}

	public void setNumberIndIdeList(String numberIndIdeList) {
		this.numberIndIdeList.add(numberIndIdeList);
	}






	public ArrayList<Integer> getGrantorList() {
		return grantorList;
	}
	
	public void setGrantorList(ArrayList<Integer> grantorList) {
		this.grantorList = grantorList;
	}

	public void setGrantorList(Integer grantorList) {
		this.grantorList.add(grantorList);
	}





	public ArrayList<Integer> getHolderList() {
		return holderList;
	}

	public void setHolderList(ArrayList<Integer> holderList) {
		this.holderList = holderList;
	}
	
	public void setHolderList(Integer holderList) {
		this.holderList.add(holderList);
	}







	public ArrayList<Integer> getProgramCodeList() {
		return programCodeList;
	}
	public void setProgramCodeList(ArrayList<Integer> programCodeList) {
		this.programCodeList = programCodeList;
	}
	public void setProgramCodeList(Integer programCodeList) {
		this.programCodeList.add(programCodeList);
	}







	public ArrayList<Integer> getExpandedAccessList() {
		return expandedAccessList;
	}
	public void setExpandedAccessList(ArrayList<Integer> expandedAccessList) {
		this.expandedAccessList = expandedAccessList;
	}
	public void setExpandedAccessList(Integer expandedAccessList) {
		this.expandedAccessList.add(expandedAccessList);
	}






	public ArrayList<Integer> getAccessTypeList() {
		return accessTypeList;
	}
	public void setAccessTypeList(ArrayList<Integer> accessTypeList) {
		this.accessTypeList = accessTypeList;
	}
	public void setAccessTypeList(Integer accessTypeList) {
		this.accessTypeList.add(accessTypeList);
	}




	public ArrayList<Integer> getExemptIndIdeList() {
		return exemptIndIdeList;
	}
	public void setExemptIndIdeList(ArrayList<Integer> exemptIndIdeList) {
		this.exemptIndIdeList = exemptIndIdeList;
	}
	public void setExemptIndIdeList(Integer exemptIndIdeList) {
		this.exemptIndIdeList.add(exemptIndIdeList);
	}

	
	
	
	public int getStudyIndIdeInfo(int studyId) {
		
		int rows=0;
    	PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = getConnection();
            pstmt = conn.prepareStatement("SELECT PK_STUDY_INDIIDE, INDIDE_TYPE,INDIDE_NUMBER, FK_CODELST_INDIDE_GRANTOR"
            		+ ", FK_CODELST_INDIDE_HOLDER, FK_CODELST_PROGRAM_CODE, INDIDE_EXPAND_ACCESS, FK_CODELST_ACCESS_TYPE"
            		+ ", INDIDE_EXEMPT FROM ER_STUDY_INDIDE WHERE FK_STUDY = ?");
                      
            
            pstmt.setInt(1, studyId);
          
            ResultSet rs = pstmt.executeQuery();
            
            while (rs.next()) {
            	setIdIndIdeList(rs.getInt("PK_STUDY_INDIIDE"));
            	setTypeIndIdeList(rs.getInt("INDIDE_TYPE"));
            	setNumberIndIdeList(rs.getString("INDIDE_NUMBER"));
            	setGrantorList(rs.getInt("FK_CODELST_INDIDE_GRANTOR"));
            	setHolderList(rs.getInt("FK_CODELST_INDIDE_HOLDER"));
            	setProgramCodeList(rs.getInt("FK_CODELST_PROGRAM_CODE"));
            	setExpandedAccessList(rs.getInt("INDIDE_EXPAND_ACCESS"));
            	setAccessTypeList(rs.getInt("FK_CODELST_ACCESS_TYPE"));
            	setExemptIndIdeList(rs.getInt("INDIDE_EXEMPT"));
            	rows++;
            }
                        
       } catch (SQLException ex) {
            Rlog.fatal("Study",
                    "StudyINDIDEDAO.getStudyIndIdeInfo EXCEPTION IN FETCHING FROM ER_STUDY_INDIDE table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return rows;
    }

	
}
