package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import java.sql.CallableStatement;

import com.velos.eres.service.util.GenerateId;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;
import com.velos.eres.business.common.SettingsDao;

public class BrowserDao extends CommonDAO implements java.io.Serializable {
	
	private  ArrayList columnNames;
	private  ArrayList colSequences;
	private  ArrayList colSettings;
	private ArrayList  colExpLabels;
	private ArrayList searchNameList;
	private ArrayList searchCriteriaList;
	private ArrayList searchBelongsToList;
	private ArrayList searchDefaultList;
	
	

    
    
	
	public BrowserDao(){
		columnNames = new ArrayList();		
		colSequences = new ArrayList();		
		colSettings = new ArrayList();
		searchNameList=new ArrayList();
		searchCriteriaList=new ArrayList();
		searchDefaultList=new ArrayList();
		colExpLabels=new ArrayList();
	}

	public  ArrayList getColnames() {
		return columnNames;
	}

	public ArrayList getColSequences() {
		return colSequences;
	}

	public ArrayList getColSettings() {
		return colSettings;
	}

	public void setColSequences(Integer colSeq) {
        this.colSequences.add(colSeq);
    }

	 public void setColNames(String colName) {
	        this.columnNames.add(colName);
	    }
	
	 public void setColSettings(String colSet) {
	        this.colSettings.add(colSet);
	    }
	
	public ArrayList getSearchNameList() {
		return searchNameList;
	}

	public void setSearchNameList(ArrayList searchNameList) {
		this.searchNameList = searchNameList;
	}

	public void setSearchNameList(String searchName) {
		this.searchNameList.add(searchName);
	}

	public ArrayList getSearchCriteriaList() {
		return searchCriteriaList;
	}

	public void setSearchCriteriaList(ArrayList searchCriteriaList) {
		this.searchCriteriaList = searchCriteriaList;
	}
	
	public void setSearchCriteriaList(String searchCriteria) {
		this.searchCriteriaList.add(searchCriteria);
	}
	public ArrayList getSearchDefaultList() {
		return searchDefaultList;
	}

	public void setSearchDefaultList(ArrayList searchDefaultList) {
		this.searchDefaultList = searchDefaultList;
	}
	public void setSearchDefaultList(String searchDefault) {
		this.searchDefaultList.add(searchDefault);
	}
	 
	public ArrayList getColExpLabels() {
		return colExpLabels;
	}

	public void setColExpLabels(ArrayList colExpLabels) {
		this.colExpLabels = colExpLabels;
	}
	
	public void setColExpLabels(String colExpLabel) {
		this.colExpLabels.add(colExpLabel);
	}
/*
 * This method is used to get the column sequences, column names, column settings 
 * and set them into setColSequences(),setColNames(),setColSettings()
 */
	 
		 
	 
	public void getBrowserDetails(String bmodulename){
		
		 	int rows = 0;
	               
	        String l_colSetting="";
	        String l_colName="",l_label="";
	        
	        PreparedStatement pstmt = null;
	        Connection conn = null;
	        
	        try{
	        	conn = getConnection();
	        	pstmt = conn.prepareStatement("SELECT browserconf_colname, browserconf_seq, browserconf_settings,browserconf_explabel "
	                    + "FROM er_browserconf "
	                    + "WHERE fk_browser = (SELECT pk_browser "
	                    + "FROM er_browser "
	                    + "WHERE lower(browser_module) =?)" 
	                    + "order by browserconf_seq ");
	        	pstmt.setString(1, bmodulename.toLowerCase());
	        	
	            ResultSet rs = pstmt.executeQuery();
	            Rlog.debug("In BrowserDao.getBrowserDetails","result set is "+rs);
	            while(rs.next()){
	            	
	            	setColSequences(new Integer(rs.getInt("browserconf_seq")));
	            	l_colName=rs.getString("browserconf_colname");
	            	l_colName=(l_colName==null)?"":l_colName;
	            	setColNames(l_colName);
	            	l_colSetting=rs.getString("browserconf_settings");
	            	l_colSetting=(l_colSetting==null)?"":l_colSetting;
	            	setColSettings(l_colSetting);
	            	l_label=rs.getString("browserconf_explabel");
	            	l_label=(l_label==null)?"":l_label;
	            	setColExpLabels(l_label);
	                rows++;
	                
	            	Rlog.debug("BrowserDao.getBrowserDetails","rows = " + rows);
	            	
	            }
	         	           
	        } catch(Exception e){
	        	Rlog.fatal("In BrowserDao.getBrowserDetails","exception "+e);
	        	e.printStackTrace();
	        	
	        } finally {
	            try { if (pstmt != null) { pstmt.close(); } } catch (Exception e) {}
	            try { if (conn != null) { conn.close(); } } catch (Exception e) {}
	        }
	}
			
		public String getBrdetailString(String pModuleName,int accId,int grpId,int userId,
		        String[] skipColNames){
			String columnName="";
			String colSetting="";
			boolean hideflag=false;
			String settingsVal="";
			String sVal="";
			String accountColumnSettingString = "";
			ArrayList sValAl;
			ArrayList mNumAl;
			ArrayList mNameAl;	
			int indx=0;
			String colArrayString = "";
			String finalMetaString = "";
			String metaString = "";
			String finalBrowserString = "";
			
			try{
				// to get the comma seperated column names string if the columnNames ArrayList is empty
				if(columnNames== null || columnNames.size()==0){
					
					//get system level settings for a browser
					getBrowserDetails(pModuleName);
					
				}
				
				
				
				
				// to get the comma seperated column settings string on th baisis of account/group/user
				
						SettingsDao sdao = new SettingsDao();  
						
						sdao.getSettingsValue(pModuleName.toUpperCase()+"_META",accId,grpId,userId);
											
												
						 sValAl = new ArrayList();
						 mNumAl= new ArrayList();
						 mNameAl= new ArrayList();	
						 
						 sValAl = sdao.getSettingValue();
						 		//PERSON_CODE,PFNAME,PERSON_STATUS		 
						 mNumAl = sdao.getSettingsModName();
						 mNameAl = sdao.getSettingsModNum();
						 
						 //create the available column list of the basis of account settings, if present otherwise system settings we loaded
						 if(!sValAl.equals("") || sValAl!=null){
							 
							 
						 }
						 
						 
						indx = mNumAl.indexOf("1"); //aaccount level
							if(indx>-1)
							{
								accountColumnSettingString = (String) sValAl.get(indx);						
							}
							
							if (StringUtil.isEmpty(accountColumnSettingString))
							{
								accountColumnSettingString = "";
								
								
							}
							//append the String with the delimiter
							accountColumnSettingString = "," + accountColumnSettingString + ",";	
						 
						 
						 ///////
						 
						 
						 indx = mNumAl.indexOf("2"); //user level					
						if(indx>-1){
							sVal=(String) sValAl.get(indx);						
						}else{
							indx = mNumAl.indexOf("4"); //group level
							if(indx>-1){
								sVal=(String) sValAl.get(indx);						
							}else{
								indx = mNumAl.indexOf("1"); //aaccount level
								if(indx>-1){
									sVal=(String) sValAl.get(indx);						
								}
							}							
						}
						
						if (StringUtil.isEmpty(sVal))
						{
							sVal = "";
							
						}
						
						
						
						// forming an arraylist out of sVal
						String[] sValArray=new String[0];
						if (sVal.length()>0)
						 sValArray = sVal.split( "," );
						
						ArrayList sValList = StringUtil.strArrToArrayList(sValArray);
						String sValStr="";
						indx=-1;
						if (!(sValList.equals(columnNames))) {
						for(int i = 0; i < columnNames.size() ; i++)
						{
							//sValStr = (String) sValList.get(i);
							sValStr=(String)columnNames.get(i);
							indx=sValList.indexOf(sValStr);
							if (indx<0)
							{
								sValList.add(sValStr);
							}
						}	
						}
												
					if (sVal.length()>0) 	sVal = "," + sVal +",";	
                    //prepare the Meta data Strings
						
					for(int i = 0; i < sValList.size() ; i++)
					{
						 columnName=(String)sValList.get(i);
						 boolean skipThisCol = false;
						 if (skipColNames != null) {
						     for (int j = 0; j < skipColNames.length; j++) {
						         if(skipColNames[j] == null) { continue; }
						         if(skipColNames[j].equals(columnName)) {
						             skipThisCol = true;
						             break;
						         }
						     }
						 }
						 if (skipThisCol) { continue; }
						indx=columnNames.indexOf(columnName);
						 
						 // search for the column in the account string
						  if ( accountColumnSettingString.contains("," + columnName +",") || accountColumnSettingString.equals(",,")) 
						  {
													  
							  colArrayString = colArrayString + "," + "\"" + columnName + "\"";
							  
							  //get the meta data for this column
							  metaString = colSettings.get(indx).toString();
							  metaString=(metaString==null)?"":metaString;
							  
							  //check if there is any user/group/acc setting defined for this browser, if yes make the column visible/invisible
							  
							  if (! sVal.equals(",,")) //not an empty string
							  {
								  //check if teh column is defined for user settings
								  
								  if ( ! sVal.contains("," + columnName +",") && (sVal.length()>2) )
								  {
									  //process metastring to hide the column
									  metaString = metaString.replaceAll("}", " ,\"hidden\":true }");
									  
								  }
								  
							  }
							  if (metaString.trim().length()>0)
							  finalMetaString = finalMetaString + "," + metaString ;
							   
							  
							  
							  
						  } //end of if column exists in account settings
						  
					}           
					
					
					//remove the first "," from final Strings
					
					finalMetaString = finalMetaString.substring(1);
					colArrayString = colArrayString.substring(1);
				
					//finalBrowserString = "\"colArray\":[" + colArrayString + "],\"meta\":[" + finalMetaString + "],\"usrColArray\":["+sVal +"]"; 
					finalBrowserString = "\"colArray\":[" + colArrayString + "],\"meta\":[" + finalMetaString + "]";
					
				
				
				return finalBrowserString;
				
			}catch(Exception e){
				
				Rlog.fatal("In BrowserDao.getBrdetailString","exception "+e);
	        	e.printStackTrace();
	        	return finalBrowserString;
			}
		}
		
		private String sortMetaString(String keyword, String metaString) {
            if (metaString == null) { return null; }
            ArrayList<Integer> keywordSeq = null;
            ArrayList<String> keywordColnames = null;
            String moduleName = keyword.substring(0, keyword.indexOf("_"));
            this.getBrowserDetails(moduleName);
            keywordSeq = this.getColSequences();
            keywordColnames = this.getColnames();
            int maxSeq = 0;
            for (int iX=0; iX<keywordSeq.size(); iX++) {
                if (keywordSeq.get(iX) > maxSeq) {
                    maxSeq = keywordSeq.get(iX);
                }
            }
            ++maxSeq;
            HashMap<String,Integer> hash = new HashMap<String,Integer>();
            for (int iX=0; iX<keywordColnames.size(); iX++ ) {
                hash.put(keywordColnames.get(iX), keywordSeq.get(iX));
            }
            String[] splitStrings = metaString.split(",");
            ArrayList<String> sortedStrings = new ArrayList<String>(maxSeq);
            for (int iY=0; iY<maxSeq; iY++) {
                sortedStrings.add("");
            }
            for (int iY=0; iY<splitStrings.length; iY++) {
                Integer mOrder = hash.get(splitStrings[iY]);
                if (mOrder == null) { mOrder = Integer.valueOf(0); }
                sortedStrings.set(mOrder.intValue(), splitStrings[iY]);
            }
            StringBuffer sb = new StringBuffer();
            for (int iY=0; iY<maxSeq; iY++) {
                if (sortedStrings.get(iY) != null && sortedStrings.get(iY).length() > 0) {
                    sb.append(sortedStrings.get(iY)).append(",");
                }
            }
            sb.deleteCharAt(sb.length()-1);
		    return sb.toString();
		}

	    public void setBrowserSettings(int modNum, String modName, ArrayList settingsKeyword, ArrayList settingsValue){
	        
	        ArrayList sortedSettingsValue = null;
	        if (settingsKeyword != null && settingsKeyword.size() > 0) {
	            sortedSettingsValue = new ArrayList();
	            for (int iKey=0; iKey<settingsKeyword.size(); iKey++) {
	                String moduleName = (String)settingsKeyword.get(iKey);
	                if (moduleName.endsWith("_meta")) {
	                    sortedSettingsValue.add(sortMetaString(moduleName, (String)settingsValue.get(iKey)));
	                } else {
	                    sortedSettingsValue.add(settingsValue.get(iKey));
	                }
	            }
	        } else {
	            sortedSettingsValue = settingsValue;
	        }

	    	CallableStatement cstmt = null;
	       	Connection conn = null;
	        int settingsModName=-1;
	        Object[]  sKeywords=null;
	        Object[] sValues=null;
	      
	    	try{
	    		conn = getConnection();
	    		SettingsDao sdao = new SettingsDao();
	    		settingsModName = sdao.getSettingModNameCode(modName);
	    			    		
	    		if(settingsKeyword!=null){
		    		for(int count=0; settingsKeyword.size()>count;count++){
		    			String str=(String) settingsKeyword.get(count);
		    			str=str.toUpperCase();		    		
		    		}
	    		}
	    		sKeywords = settingsKeyword.toArray();	    		
	    		sValues = sortedSettingsValue != null ? 
	    		        sortedSettingsValue.toArray() : settingsValue.toArray();	    		
	    		
	    		ArrayDescriptor inSettingKeyords = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
	    		ARRAY settingsKeywordArr = new ARRAY(inSettingKeyords,conn,sKeywords);
	    		
	    		ArrayDescriptor inSettingValues = ArrayDescriptor.createDescriptor("ARRAY_STRING", conn);
	    		ARRAY settingsValueArr = new ARRAY(inSettingValues,conn,sValues);
	    		
	    		cstmt = conn.prepareCall("{call pkg_browser.SP_INSERT_BROWSERSETTINGS(?,?,?,?)}");
	    		
	    		cstmt.setInt(1,modNum);
	    		cstmt.setInt(2,settingsModName);	    		
	    		cstmt.setArray(3,settingsKeywordArr);
	    		cstmt.setArray(4,settingsValueArr);
	    		cstmt.execute();
	      		
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}finally {
	            try {
	                if (cstmt != null)
	                    cstmt.close();
	            } catch (Exception e) {
	            }
	            try {
	                if (conn != null)
	                    conn.close();
	            } catch (Exception e) {
	            }
	        }
	    }
	    
	    
	    public int saveSearch(String module,String name,String criteria,String isDefault,String userId,String oper,int pk_brsearch)
	    {
	    	String l_module=StringUtil.trueValue(module);
	    	String l_name=StringUtil.trueValue(name);
	    	String l_criteria=StringUtil.trueValue(criteria);
	    	String l_isDefault=StringUtil.trueValue(isDefault);
	    	int li_userId=StringUtil.stringToNum(StringUtil.trueValue(userId));
	    
	    	int id=0,ret=0;
	    	
	    	
	    	Connection connection = null;
	        PreparedStatement pstmt = null;
         	String sql = "";
         	         	
	        try {
	            // Disable auto-commit
	        	connection = getConnection();                
                
                
	        	if(oper.equals("I")){
	        		
	        			sql = "INSERT INTO er_browsersearch(pk_browsersearch,browsersearch_name, "
	                        + " browsersearch_criteria,browsersearch_isdefault,browsersearch_module,fk_user,creator) VALUES(?,?,?,?,?,?,?)";
	        			
	        			pstmt = connection.prepareStatement(sql);	
	                    id = GenerateId.getId("seq_er_browsersearch", connection);
	                    pstmt.setInt(1, id);
	                    pstmt.setString(2, l_name);
	                    pstmt.setString(3, l_criteria);
	                    pstmt.setString(4,l_isDefault );
	                    pstmt.setString(5, l_module);
	                    pstmt.setInt(6,li_userId);
	                    pstmt.setInt(7,li_userId);
	                    ret=pstmt.executeUpdate();
	                   
	        	}     
	        	//update when we do not have pk_browsersearch 
	             if(oper.equals("U") || pk_brsearch==0){
	               		
	              		sql = " update er_browsersearch " +
	              				" set browsersearch_criteria=?, browsersearch_isdefault=?  " +
	              				" where  browsersearch_name=?  and  browsersearch_module=?  and  fk_user=?  ";
	              		
	              		pstmt = connection.prepareStatement(sql);
	              		
	              		pstmt.setString(1, l_criteria);
	              		pstmt.setString(2, l_isDefault);
	              		pstmt.setString(3, l_name);
	              		pstmt.setString(4, l_module);
	              		pstmt.setInt(5, li_userId);
	              			        		
	              		ret=pstmt.executeUpdate();
	              		connection.commit();
	             }
	             // update when we have pk_browsersearch 
	             if(oper.equals("U") || pk_brsearch>0){
	               		
	              		sql = " update er_browsersearch " +
	              				" set browsersearch_criteria=?, browsersearch_isdefault=?  " +
	              				" where  pk_brsearch=?  ";
	              		
	              		pstmt = connection.prepareStatement(sql);
	              		
	              		pstmt.setString(1, l_criteria);
	              		pstmt.setString(2, l_isDefault);
	              		pstmt.setInt(3, pk_brsearch);
	              			        		
	              		ret=pstmt.executeUpdate();
	              		connection.commit();
	             }
	          
	             
	             return ret;
	        } catch (SQLException e) {
	            // Not all of the statements were successfully executed

	            e.printStackTrace();
	            return -1;
	        }finally{
	           
	        	try { 
	        	    if (pstmt != null) { pstmt.close(); }
	        	} catch(Exception e) {}
	        	try{
	                if (connection!=null) connection.close();
	            }catch(Exception e){
	                
	            }
	        }
	    }
	    
	    public int deleteSearch(String module,String name,String userId,int pk_brsearch)
	    {
	    	Connection connection = null;
	        PreparedStatement pstmt = null;
	    	String sql = "";
	    	int ret=0;
	    	
	    	String l_module=StringUtil.trueValue(module);
	    	String l_name=StringUtil.trueValue(name).trim();
	    	int li_userId=StringUtil.stringToNum(StringUtil.trueValue(userId));
	    	
	    	try{	 	
	    		connection = getConnection();
	    			// delete when we do not have the pk_browsersearch value
	    			if(pk_brsearch==0){
	              		sql = " DELETE FROM er_browsersearch " +
	              			  " WHERE  trim(browsersearch_name)=?  and  browsersearch_module=?  and  fk_user=? ";
	              		pstmt = connection.prepareStatement(sql);
	              		pstmt.setString(1, l_name);
	              		pstmt.setString(2, l_module);
	              		pstmt.setInt(3, li_userId); 
	              		
	    			}
	    			//delete when we have the pk_browsersearch value
	    			if(pk_brsearch>0){
	    				
	    				sql = " DELETE FROM er_browsersearch " +
            			  	  " WHERE  pk_brsearch=? ";
	            		pstmt = connection.prepareStatement(sql);
	            		pstmt.setInt(1, pk_brsearch);
	    			}
	              		ret=pstmt.executeUpdate();
	              		connection.commit();
	              		return ret;
	              		
	    	}catch (SQLException e) {
	                 // Not all of the statements were successfully executed
   	            e.printStackTrace();
   	            return -1;
   	        }finally{
   	        		try { 
   	        			if (pstmt != null) { pstmt.close(); }
   	        		} catch(Exception e) {}
   	        			
   	        		try{
        	            if (connection!=null) connection.close();
        	        }catch(Exception e){
        	                
        	            }
	        }
	    	

	    }
	    
	    public void getBrowserSavedSearch(String module,String userId)
	    {
	    	int rows = 0,li_userId=0;
	    	String isDefault="",l_module=module.toLowerCase();
	    	//String l_belongsToStr=StringUtil.trueValue(belongsToStr);
	    	//if (l_belongsToStr.length()==0) l_belongsToStr="U,G,A";
	    	
	    	String l_userId=StringUtil.trueValue(userId);
	    	li_userId=StringUtil.stringToNum(l_userId);
	    	
	    	
	    	PreparedStatement pstmt = null;
	        Connection conn = null;
	        String sql="select * from er_browsersearch where lower(browsersearch_module)=? and fk_user=?" +
	        		" order by browsersearch_name asc";
	        
	        try{
	        	conn = getConnection();
	        	pstmt = conn.prepareStatement(sql);
	        	pstmt.setString(1, l_module);
	        	pstmt.setInt(2, li_userId);
	        	
	            ResultSet rs = pstmt.executeQuery();
	            while(rs.next()){
	            	this.setSearchNameList(rs.getString("browsersearch_name"));
	            	this.setSearchCriteriaList(rs.getString("browsersearch_criteria"));
	            	isDefault=rs.getString("browsersearch_isdefault");
	            	isDefault=StringUtil.trueValue(isDefault);
	            	this.setSearchDefaultList(isDefault);
	            	rows++;
	                
	            }
	         	           
	        }catch(Exception e){
	        	Rlog.fatal("In BrowserDao.getBrowserDetails","exception "+e);
	        	e.printStackTrace();
	        	
	        }finally{
                try { 
                    if (pstmt != null) { pstmt.close(); }
                } catch(Exception e) {}
	            try{
	                if (conn != null) { conn.close(); }
	            } catch(Exception e) {}
	        }
	    }
	   
	    public String createBrowserSavedSearchDD(String name){
	    	
	    	String savedSearch="";
	    	//if (this.getSearchNameList().size()>0)
	    //	{
	    		savedSearch=StringUtil.createPullDownWithStr(name,"",this.getSearchCriteriaList(),this.getSearchNameList() );
	    	//}
	    	return savedSearch;
	    }

		
		
}

	
	
	
	
	
	
	
	
	
	
	
	
