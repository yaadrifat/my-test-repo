/*
 * Classname			SectionBean.class
 * 
 * Version information
 *
 * Date					02/22/2001
 * 
 * Copyright notice
 */

package com.velos.eres.business.section.impl;

/**
 * 
 */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

@Entity
@Table(name = "er_Studysec")
public class SectionBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8044895250233684656L;

    // PK
    public int id;

    // Study ID of the section
    public Integer secStudy;

    // section name
    public String secName;

    // section number
    public String secNum;

    // contents of the section
    // public String contents ;
    public String contents;

    // public flag for the section
    public String secPubFlag;

    // sequence of the secton in the study
    public Integer secSequence;

    // Study Version ID of the section
    public Integer secStudyVer;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    /** * For extra sections ** */

    /*public String contents2;

    public String contents3;

    public String contents4;

    public String contents5;*/

    public SectionBean() {

    }

    public SectionBean(int id, String secStudy, String secName, String secNum,
            String contents, String secPubFlag, String secSequence,
            String secStudyVer, String creator, String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setId(id);
        setSecStudy(secStudy);
        setSecName(secName);
        setSecNum(secNum);
        //setContents(contents);
        setSecPubFlag(secPubFlag);
        setSecSequence(secSequence);
        setSecStudyVer(secStudyVer);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        /*
         * setContents2(contents2); setContents3(contents3);
         * setContents4(contents4); setContents5(contents5);
         */
    }

    /** ******************** */

    // GETTER SETTER METHODS
    
    @Transient
    public String getContents() {
    	Rlog.debug("section", "contents1 length**");
        return this.contents;

    }
    
    public void setContents(String  con) 
    {
    	Rlog.debug("section", "contents1 length**");
        this.contents = con;

    }
    
    
    /*
    @Lob(type=LobType.CLOB,fetch=FetchType.EAGER )
    @Column(name = "STUDYSEC_TEXT")
     public String getContents() {
        Rlog.debug("section", "contents1 length**");

        //
        String s1 = "";
        String s2 = "";
        String s3 = "";
        String s4 = "";
        String s5 = "";
        String str = "";

        s1 = this.contents;
        s2 = getContents2();
        s3 = getContents3();
        s4 = getContents4();
        s5 = getContents5();

        if (s1 == null)
            s1 = "";
        if (s2 == null)
            s2 = "";
        if (s3 == null)
            s3 = "";
        if (s4 == null)
            s4 = "";
        if (s5 == null)
            s5 = "";

        str = s1 + s2 + s3 + s4 + s5;

        Rlog.debug("section", "get sec1*" + s1.length() + "*");
        Rlog.debug("section", "get sec2*" + s2.length() + "*");
        Rlog.debug("section", "get sec3*" + s3.length() + "*");
        Rlog.debug("section", "get sec4*" + s4.length() + "*");
        Rlog.debug("section", "get sec5*" + s5.length() + "*");
        Rlog.debug("section", "get sec5*" + str.length() + "*"); 

      //  Rlog.debug("section", "this.contents" + this.contents.length() + "*");
       // return this.contents;
    }*/

    /*
    public void setContents(String contents) {
        if (contents == null || contents.equals("")) {
            Rlog.debug("section",
                    "SectionBean.setContents Contents are null or empty");
            this.contents = " ";
        } else {
            long len = 0;

            String s1 = "";
            String s2 = "";
            String s3 = "";
            String s4 = "";
            String s5 = "";

            len = contents.length();

            Rlog.debug("section", "GOT SECTION CONTENTS *" + len + "*");

            if (len > 15200) {
                if (len <= 19000) {
                    s5 = contents.substring(15200, (int) len);
                } else {
                    s5 = contents.substring(15200, 19000);
                }

                s4 = contents.substring(11400, 15200);
                s3 = contents.substring(7600, 11400);
                s2 = contents.substring(3800, 7600);
                s1 = contents.substring(0, 3800);
            } else if (len > 11400 && len <= 15200) {
                s4 = contents.substring(11400);
                s3 = contents.substring(7600, 11400);
                s2 = contents.substring(3800, 7600);
                s1 = contents.substring(0, 3800);
            } else if (len > 7600 && len <= 11400) {
                s3 = contents.substring(7600);
                s2 = contents.substring(3800, 7600);
                s1 = contents.substring(0, 3800);
            } else if (len > 3800 && len <= 7600) {
                s2 = contents.substring(3800);
                s1 = contents.substring(0, 3800);
            } else {
                s1 = contents;
            }

            this.contents = s1;

            setContents2(s2);
            setContents3(s3);
            setContents4(s4);
            setContents5(s5);

            Rlog.debug("section", "sec1*" + s1.length() + "*");
            Rlog.debug("section", "sec2*" + s2.length() + "*");
            Rlog.debug("section", "sec3*" + s3.length() + "*");
            Rlog.debug("section", "sec4*" + s4.length() + "*");
            Rlog.debug("section", "sec5*" + s5.length() + "*"); 
        	
        	//this.contents = contents;
        	//Rlog.debug("section", "this.contents" + this.contents.length() + "*");
        }

    }*/

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_STUDYSEC", allocationSize=1)
    @Column(name = "PK_STUDYSEC")
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "STUDYSEC_NAME")
    public String getSecName() {
        Rlog.debug("section", "In getSecName of sectionBean");
        return this.secName;
    }

    public void setSecName(String secName) {
        this.secName = secName;
    }

    @Column(name = "STUDYSEC_NUM")
    public String getSecNum() {
        Rlog.debug("section", "In getSecNum of sectionBean");
        return this.secNum;
    }

    public void setSecNum(String secNum) {
        this.secNum = secNum;
    }

    @Column(name = "STUDYSEC_PUBFLAG")
    public String getSecPubFlag() {
        Rlog.debug("section", "In getSecPubFlag of sectionBean");
        return this.secPubFlag;
    }

    public void setSecPubFlag(String secPubFlag) {
        this.secPubFlag = secPubFlag;
    }

    @Column(name = "STUDYSEC_SEQ")
    public String getSecSequence() {
        Rlog.debug("section", "In getSecSequence of sectionBean");
        return StringUtil.integerToString(this.secSequence);
    }

    public void setSecSequence(String secSequence) {
        Rlog.debug("section", "In setSecSequence of sectionBean before if "
                + secSequence);
        if (secSequence != null) {
            Rlog.debug("section",
                    "In setSecSequence of sectionBean inside if before value");
            this.secSequence = Integer.valueOf(secSequence);
            Rlog.debug("section",
                    "In setSecSequence of sectionBeaninside if after value="
                            + this.secSequence);
        }
        Rlog.debug("section", "In setSecSequence of sectionBean after if");
    }

    @Column(name = "FK_STUDY")
    public String getSecStudy() {
        Rlog.debug("section", "In getSecStudy of sectionBean");
        return StringUtil.integerToString(this.secStudy);
    }

    public void setSecStudy(String secStudy) {

        if (secStudy != null) {
            this.secStudy = Integer.valueOf(secStudy);
        }

    }

    @Column(name = "FK_STUDYVER")
    public String getSecStudyVer() {
        Rlog.debug("section", "In getSecStudyVer of sectionBean");
        return StringUtil.integerToString(this.secStudyVer);
    }

    public void setSecStudyVer(String secStudyVer) {

        if (secStudyVer != null) {
            this.secStudyVer = Integer.valueOf(secStudyVer);
        }

    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    
    /**
     * update section data
     */

    public int updateSection(SectionBean ssk) {
        Rlog.debug("section", "IN ENTITY BEAN - SECTION  STATE HOLDER");

        try {
            Rlog.debug("section",
                    "In updateSection in SectionBean before setting study id="
                            + ssk.getSecStudy());
            setSecStudy(ssk.getSecStudy());
            Rlog.debug("section",
                    "In updateSection in SectionBean before setting name="
                            + ssk.getSecName());
            setSecName(ssk.getSecName());
            Rlog.debug("section",
                    "In updateSection in SectionBean before setting number="
                            + ssk.getSecNum());
            setSecNum(ssk.getSecNum());

            //Rlog.debug("section","In updateSection in SectionBean before setting contents");
            //setContents(ssk.getContents());
            
            Rlog.debug("section",
                    "In updateSection in SectionBean before setting public flag="
                            + ssk.getSecPubFlag());
            setSecPubFlag(ssk.getSecPubFlag());
            Rlog.debug("section",
                    "In updateSection in SectionBean before setting sequence="
                            + ssk.getSecSequence());
            setSecSequence(ssk.getSecSequence());
            setSecStudyVer(ssk.getSecStudyVer());
            setCreator(ssk.getCreator());
            setModifiedBy(ssk.getModifiedBy());
            setIpAdd(ssk.getIpAdd());
            Rlog.debug("section", "In updateSection in SectionBean ending");
            return 0;

        } catch (Exception e) {
            // System.out.println("EXCEPTION " + e);
            Rlog.fatal("section", "EXCEPTION IN CREATING NEW STUDY SECTION "
                    + e);
            return -2;
        }
    }

    // GET SET FOR EXTRA SECTION CONTENTS

    /* @Column(name = "STUDYSEC_CONTENTS2")
    public String getContents2() {
        if (this.contents2 == null)
            return "";

        return this.contents2;
    }

    public void setContents2(String contents2) {
        this.contents2 = contents2;
    }

    @Column(name = "STUDYSEC_CONTENTS3")
    public String getContents3() {
        if (this.contents3 == null)
            return "";
        return this.contents3;
    }

    public void setContents3(String contents3) {
        this.contents3 = contents3;
    }

    @Column(name = "STUDYSEC_CONTENTS4")
    public String getContents4() {
        if (this.contents4 == null)
            return "";
        return this.contents4;
    }

    public void setContents4(String contents4) {
        this.contents4 = contents4;
    }

    @Column(name = "STUDYSEC_CONTENTS5")
    public String getContents5() {
        if (this.contents5 == null)
            return "";
        return this.contents5;
    }

    public void setContents5(String contents5) {

        this.contents5 = contents5;
    } */

}// end of class

