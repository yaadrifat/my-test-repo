/*
 * Classname			EschReportsDao
 * 
 * Version information 	1.0
 *
 * Date					07/14/2001
 * 
 * Copyright notice		Velos, Inc.
 *
 * Author 				Vikas
 */

package com.velos.eres.business.common;

/* IMPORT STATEMENTS */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.eres.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * EschReportsDao for fetching EschReports data from the database for various
 * eschreports
 * 
 * @author Dinesh
 * @version : 1.0 05/12/2001
 */

public class EschCostReportDao extends CommonDAO implements
        java.io.Serializable {

    public ArrayList fk_assocs = new ArrayList();

    public ArrayList fk_events = new ArrayList();

    public ArrayList descriptions = new ArrayList();

    public ArrayList start_date_times = new ArrayList();

    public ArrayList months = new ArrayList();

    public ArrayList event_exeons = new ArrayList();

    public ArrayList eventcost_values = new ArrayList();

    public ArrayList eventcost_descs = new ArrayList();

    public ArrayList eventcost_values1 = new ArrayList();

    public ArrayList eventcost_descs1 = new ArrayList();

    public ArrayList patient_ids = new ArrayList();

    public ArrayList sub_totals = new ArrayList();

    public ArrayList per_codes = new ArrayList();

    public void getCostReport(int reportNumber, int protocol_id, int orderBy) {

        PreparedStatement pstmt = null;
        Connection conn = null;

        String costQuery = "select  FK_EVENT,EVENTCOST_VALUE,EVENTCOST_DESC from sch_eventcost   where FK_EVENT in (";
        String patientCodeQuery = "select per_code from er_per where pk_per in (";

        try {
            conn = getConnection();

            String mysql = "select FK_ASSOC,DESCRIPTION,PATIENT_ID, "
                    + "to_char(EVENT_EXEON,'MONTH') MONTH, "
                    + "START_DATE_TIME, "
                    + "EVENT_EXEON "
                    + ", (select per_code from er_per where pk_per = trim(patient_id))  PER_CODE  "
                    + "	from  sch_events1  where "
                    + " SESSION_ID= lpad(to_char(?),10,'0') "
                    + " and isconfirmed = 5 ";

            if (reportNumber != 18 && orderBy == 3) {
                mysql += " and patient_id =  lpad(to_char(";
                mysql += reportNumber;
                mysql += "),10,'0') ";
            }
            if (orderBy == 1) {
                mysql += " order by patient_id,FK_ASSOC";
            } else {
                mysql += " order by EVENT_EXEON,FK_ASSOC";

            }

            Rlog.debug("report", "THESQL:: " + mysql);

            pstmt = conn.prepareStatement(mysql);
            pstmt.setInt(1, protocol_id);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                // fk_assocs.add(rs.getString("FK_ASSOC"));
                Integer id = new Integer(rs.getString("FK_ASSOC"));
                costQuery += id;
                costQuery += ",";

                descriptions.add(rs.getString("DESCRIPTION"));
                months.add(rs.getString("MONTH"));
                patient_ids.add(rs.getString("PATIENT_ID"));
                per_codes.add(rs.getString("PER_CODE"));
                event_exeons.add(rs.getString("EVENT_EXEON"));
                start_date_times.add(rs.getString("START_DATE_TIME"));

            }
            costQuery = costQuery.substring(0, costQuery.length() - 1);// to
            // remove
            // the
            // suffix
            // ','
            costQuery += ") order by FK_EVENT";

        }

        catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getEventdefValues EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

        try {
            conn = getConnection();
            Rlog.debug("report", costQuery);
            pstmt = conn.prepareStatement(costQuery);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                fk_events.add(rs.getString("FK_EVENT"));
                eventcost_values.add(rs.getString("EVENTCOST_VALUE"));
                eventcost_descs.add(rs.getString("EVENTCOST_DESC"));

            }

            String s2 = "";
            String s3 = "";
            float subTotal = 0;
            for (int l = 0; l < fk_events.size() - 1; l++) {
                if (l == 0) {
                    s2 = (String) eventcost_values.get(l);
                    Float f1 = new Float(s2);
                    subTotal = f1.floatValue();
                    s2 = "$" + s2;
                    s3 = (String) eventcost_descs.get(l);
                }
                if (((String) fk_events.get(l)).equals((String) fk_events
                        .get(l + 1))) {
                    s2 = s2 + "," + "$" + (String) eventcost_values.get(l + 1);
                    s3 = s3 + "," + (String) eventcost_descs.get(l + 1);
                    Float f2 = new Float((String) eventcost_values.get(l + 1));
                    subTotal += f2.floatValue();
                    if (l + 1 == fk_events.size() - 1) {
                        eventcost_values1.add(s2);
                        eventcost_descs1.add(s3);
                        sub_totals.add(new Float(subTotal));
                    }
                } else {
                    eventcost_values1.add(s2);
                    eventcost_descs1.add(s3);
                    sub_totals.add(new Float(subTotal));

                    s2 = "$" + (String) eventcost_values.get(l + 1);
                    s3 = (String) eventcost_descs.get(l + 1);
                    Float f2 = new Float((String) eventcost_values.get(l + 1));
                    subTotal = f2.floatValue();
                    if (l + 1 == fk_events.size() - 1) {
                        eventcost_values1.add(s2);
                        eventcost_descs1.add(s3);
                        sub_totals.add(new Float(subTotal));

                    }
                }
            }

        } catch (SQLException ex) {
            Rlog.fatal("eventdef",
                    "EventdefDao.getEventdefValues EXCEPTION IN FETCHING FROM Eventdef table"
                            + ex);
        }

        finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }

}