

package com.velos.eres.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;

import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * MileAchievedDao - data Access class for Milestones Achieved module 
 * 
 * @author Sonia Abrol
 * @created 11/04/2005
 */
public class MileAchievedDao extends CommonDAO implements java.io.Serializable {

	 

	private ArrayList  id;
	
	private ArrayList  milestone;
	
	private ArrayList  milestoneDesc;
	
	private ArrayList  study;
	
	private ArrayList  patient;
	
	private ArrayList  patientCode;
	
	private ArrayList  patprot;
	
	private ArrayList  achievedOn;
	
	private ArrayList  totalAchieved;
	
	private ArrayList  amountDue;
	
	private ArrayList  amountInvoiced;
	
	private Hashtable htAchieved;
	
	private MilestoneDao milestoneDao;
	
	private ArrayList arInvCount;
	
	private ArrayList arPaymentCount;
	
	   
    
    public MileAchievedDao() {
		super();
		// TODO Auto-generated constructor stub
		intial();
		milestoneDao = new MilestoneDao();
			}       

    public MileAchievedDao(MileAchievedDao md) {
    		intial();
    		this.setAchievedOn(md.getAchievedOn());
    		this.setAmountDue(md.getAmountDue());
    		this.setAmountInvoiced(md.getAmountInvoiced());
    		this.setId(md.getId());
    		this.setMilestone(md.getMilestone());
    		this.setMilestoneDesc(md.getMilestoneDesc());
    		this.setPatient(md.getPatient());
    		this.setPatprot(md.getPatprot());
    		this.setStudy(md.getStudy());
    		this.setTotalAchieved(md.getTotalAchieved());
    		this.setPatientCode(md.getPatientCode());
    		this.setArPaymentCount(md.getArPaymentCount() );
    		this.setArInvCount(md.getArInvCount());
    		
    		Rlog.debug("milestone",
                    "MileAchievedDao.getAchievedMilestones got new obj" + (this.getId()).size());
    		 
		}

    	private void intial()
    	{
    		this.id = new ArrayList ();
    		milestone = new ArrayList ();
    		milestoneDesc = new ArrayList ();
    		study = new ArrayList ();
    		patient = new ArrayList ();
    		patprot = new ArrayList ();
    		achievedOn = new ArrayList ();
    		amountDue = new ArrayList ();
    		amountInvoiced = new ArrayList ();
    		htAchieved = new Hashtable();
    		patientCode = new ArrayList ();
    		arInvCount = new ArrayList();
    		arPaymentCount = new ArrayList();
    		
    	}
    	
    	/* does not reset htAchieved*/
       	private void reset()
    	{
    		this.id.clear();
    		milestone.clear();
    		milestoneDesc.clear();
    		study.clear();
    		patient.clear();
    		patprot.clear();
    		achievedOn.clear();
    		amountDue.clear();
    		amountInvoiced.clear();
    		arInvCount.clear();
    		arPaymentCount.clear();
    	}

    	/* removes after index*/
       	private void removeFrom(long index)
    	{
       		int idx = 0;
       		
       		idx = (int) index;
       		
       		Rlog.debug("milestone","index" + idx);
       		Rlog.debug("milestone","id.size()" + id.size());
       		
       		while (id.size() > idx)
       		{
       			
       			Rlog.debug("milestone","old size:" +  id.size());
       			id.remove(idx);
       			

       			if (milestone.size() > idx)
       			{
       				milestone.remove(idx);
       			}	
       			
        		//milestoneDesc.remove(idx);
        		
       			if (study.size() > idx)
       			{
       				study.remove(idx);
       			}
       			
       			if (patient.size() > idx)
       			{
       				patient.remove(idx);
       			}
       			
       			if (patprot.size() > idx)
       			{
       				patprot.remove(idx);
       			}
       			
       			if (achievedOn.size() > idx)
       			{
       				achievedOn.remove(idx);
       			}	
        		
        		if (amountDue.size() > idx)
       			{
       			 	amountDue.remove(idx);
       			}		
        		
        		if (amountInvoiced.size() > idx)
       			{
        			amountInvoiced.remove(idx);
       			}
        		
        		if (arInvCount.size() > idx)
       			{
        			arInvCount.remove(idx);
       			}
        		
        		if (arPaymentCount.size() > idx)
       			{
        			arPaymentCount.remove(idx);
       			}
        		
        		Rlog.debug("milestone","new size:" + id.size());
        		
       		}	
    		
    	}

       	

	public MilestoneDao getMilestoneDao() {
			return milestoneDao;
		}

		public void setMilestoneDao(MilestoneDao milestoneDao) {
			this.milestoneDao = milestoneDao;
		}

	public Hashtable getHtAchieved() {
		return htAchieved;
	}





	public void setHtAchieved(Hashtable htAchieved) {
		this.htAchieved = htAchieved;
	}



	public void setHtAchieved(String milestone, MileAchievedDao  mileAchieved) {
		this.htAchieved.put(milestone,mileAchieved);
	}


	public ArrayList getAchievedOn() {
		return achievedOn;
	}





	public void setAchievedOn(ArrayList achievedOn) {
		this.achievedOn = achievedOn;
	}


	public void setAchievedOn(String achievedOn) {
		this.achievedOn.add(achievedOn);
	}



	public ArrayList getAmountDue() {
		return amountDue;
	}





	public void setAmountDue(ArrayList amountDue) {
		this.amountDue = amountDue;
	}


	public void setAmountDue(String amountDue) {
		this.amountDue.add(amountDue);
	}



	public ArrayList getAmountInvoiced() {
		return amountInvoiced;
	}



	public void setAmountInvoiced(ArrayList amountInvoiced) {
		this.amountInvoiced = amountInvoiced;
	}


	public void setAmountInvoiced(String amountInvoiced) {
		this.amountInvoiced.add(amountInvoiced);
	}




	public ArrayList getId() {
		return id;
	}





	public void setId(ArrayList id) {
		this.id = id;
	}

	public void setId(String id) {
		this.id.add(id);
	}




	public ArrayList getMilestone() {
		return milestone;
	}





	public void setMilestone(ArrayList milestone) {
		this.milestone = milestone;
	}


	public void setMilestone(String milestone) {
		this.milestone.add(milestone);
	}


	public ArrayList getMilestoneDesc() {
		return milestoneDesc;
	}


	public void setMilestoneDesc(ArrayList milestoneDesc) {
		this.milestoneDesc = milestoneDesc;
	}

	public void setMilestoneDesc(String milestoneDesc) {
		this.milestoneDesc.add(milestoneDesc);
	}




	public ArrayList getPatient() {
		return patient;
	}





	public void setPatient(ArrayList patient) {
		this.patient = patient;
	}

	public void setPatient(String patient) {
		this.patient.add(patient);
	}




	public ArrayList getPatprot() {
		return patprot;
	}

	public void setPatprot(ArrayList patprot) {
		this.patprot = patprot;
	}

	public void setPatprot(String patprot) {
		this.patprot.add(patprot);
	}




	public ArrayList getStudy() {
		return study;
	}


	public void setStudy(ArrayList study) {
		this.study = study;
	}

	public void setStudy(String study) {
		this.study.add(study);
	}




	public ArrayList getTotalAchieved() {
		return totalAchieved;
	}





	public void setTotalAchieved(ArrayList totalAchieved) {
		this.totalAchieved = totalAchieved;
	}

	public void setTotalAchieved(String totalAchieved) {
		this.totalAchieved.add(totalAchieved);
	}


	/**
     * Gets Achieved milestones
     * 
     * @param studyId
     *            study id
     */
    public void getAchievedMilestones(int studyId, String rangeFrom,String rangeTo, String milestoneReceivableStatus,String milestoneType, String site) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        String milestone = "";
        String prevMilestone = "";
        MilestoneDao mileDao = new MilestoneDao(); 
         MileAchievedDao mdTemp = new MileAchievedDao();
         String payAbleStatus = "";
         int sitePk = 0;
         
        
        try {
        	
        	sitePk = StringUtil.stringToNum(site);
        	
        	if (StringUtil.isEmpty(milestoneReceivableStatus)) //from payment module, to get info for all types of milestones
        	{
        		
        		payAbleStatus = "";
        	}
        	else
        	{
        		payAbleStatus = "rec"; // for receivable milestones, for invoicing
        		
        	}
           	
            conn = getConnection();
            
            sqlDetail.append("select pk_mileachieved, milestone_count, fk_milestone, nvl(fk_per,0) fk_per , m.fk_study, fk_patprot, to_char(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ach_date, milestone_type,milestone_amount ");
            
            if (! StringUtil.isEmpty(payAbleStatus)) // otherwise we dont need calculated invoice amount
            {
            	sqlDetail.append(", nvl((select sum(amount_invoiced) from er_invoice_detail det where det.fk_milestone = m.fk_milestone  and det.fk_mileachieved =  m.pk_mileachieved),0) amount_invoiced ");
            }	
            else
            {
            	sqlDetail.append(", '' amount_invoiced " ); 
            }
            
            sqlDetail.append(" ,nvl(pkg_patient.f_get_patcodes(fk_per),' ') patfacilityIds");
            sqlDetail.append(" from er_milestone,er_mileachieved m " );
            
            if (payAbleStatus.equals("rec"))
            {
            	sqlDetail.append(", er_codelst p ");
            }	
            
            sqlDetail.append(" where m.fk_study = ? and pk_milestone = fk_milestone and milestone_achievedcount > 0 and m.is_complete  = 1 and milestone_Type = ?  ");
            
            if (! StringUtil.isEmpty(rangeFrom))
            {
            	sqlDetail.append(" AND TO_DATE(TO_CHAR(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) BETWEEN TO_DATE(?,PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(?,PKG_DATEUTIL.F_GET_DATEFORMAT) ");
            	
            }
            
            if (payAbleStatus.equals("rec"))
            {
            	sqlDetail.append(" and p.pk_codelst = milestone_paytype and trim(p.codelst_subtyp) = 'rec'  ");
            }	
            
            if (milestoneReceivableStatus.equals("UI"))
            {
            	
            	sqlDetail.append(" and not exists (select * from er_invoice_detail det where det.fk_milestone = pk_milestone and det.fk_mileachieved = pk_mileachieved)  ");
            	
            }
            
            if (sitePk > 0) //add filter for site
            {
            	
            	sqlDetail.append(" and ((fk_per is null) or ( 1 =  pkg_patient.f_is_enrollingsite(fk_per, m.fk_study, ?)  ) ) ");
            	
            }
            
            sqlDetail.append(" order by fk_milestone,pk_mileachieved ");
            
            Rlog.fatal("milestone", "MileAchievedDao.getAchievedMilestones SQL" + sqlDetail.toString());
            
            pstmt = conn.prepareStatement(sqlDetail.toString());
            pstmt.setInt(1, studyId);
            pstmt.setString(2, milestoneType);
            
            if (! StringUtil.isEmpty(rangeFrom))
            {
            	pstmt.setString(3,rangeFrom );
            	pstmt.setString(4,rangeTo );
            	
            	if (sitePk > 0) //add filter for site
                {
            		pstmt.setInt(5,sitePk);
                }	
            	
            }
            else
            {
            	if (sitePk > 0) //add filter for site
                {
            		pstmt.setInt(3,sitePk);
                }
            	
            }
            

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	
            	milestone = rs.getString("fk_milestone");
            	
            	 
            	
            	if (rows == 0)
            	{
            		prevMilestone = milestone;
            		
            	}
            	
            	if ( ! prevMilestone.equals(milestone) )
            	{
            		//set it to hashtable
            		 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones adding to hashtable" +prevMilestone);
            		 this.setHtAchieved(prevMilestone, new MileAchievedDao(mdTemp));
            		 mdTemp = new MileAchievedDao();
            		//this.reset(); //clear data arraylists
            		  
            	}
            	
            	 
            	 
            	
            	mdTemp.setId( rs.getString("pk_mileachieved"));
            	mdTemp.setMilestone(milestone);
                mdTemp.setStudy(rs.getString("fk_study"));
            	mdTemp.setPatient(rs.getString("fk_per"));
            	mdTemp.setPatprot(rs.getString("fk_patprot"));
            	mdTemp.setAchievedOn(rs.getString("ach_date"));
            	mdTemp.setAmountDue(rs.getString("milestone_amount"));
            	mdTemp.setAmountInvoiced(rs.getString("amount_invoiced"));
            	//System.out.println("amount-inv.************.."  );
            	//System.out.println("amount-inv" + rs.getString("amount_invoiced"));
            	//System.out.println("amount-inv.....***********........"  );
            	//Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones setAmountInvoiced "+ rs.getString("amount_invoiced") );
            	mdTemp.setPatientCode(rs.getString("patfacilityIds"));
               
                prevMilestone = milestone;
                
                Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones"
                        + mdTemp.id.size());
                
                rows++;
                
            }
            
            this.setHtAchieved(milestone, new MileAchievedDao(mdTemp));
            mdTemp = new MileAchievedDao();
            
            
            // get milestones for the milestone type and recievable status
            
            mileDao.getMilestoneRows(studyId, milestoneType, payAbleStatus, milestoneReceivableStatus);
            
            //iterate throughthe milestones, and calculate the number of milestones achieved for the given date range
            
            ArrayList mileStoneIds = new ArrayList();
            ArrayList milestoneCounts = new ArrayList();
            int mileCount = 0;
            String mileStoneId  = "";
            int countPerMileStone = 0;
            int achDetailCount = 0;
            long totalMilestonesAchieved = 0;
            long calculateAchievedRows = 0;
            
            
            mileStoneIds = mileDao.getMilestoneIds();
            
            mileCount = mileStoneIds.size();
            milestoneCounts = mileDao.getMilestoneCounts ();
            
            for (int i =0; i < mileCount ; i++)
     		{
     		
     			mileStoneId = String.valueOf(((Integer)mileStoneIds.get(i)).intValue());
     			
     			//count required to make one milestone
     			countPerMileStone = StringUtil.stringToNum((String) milestoneCounts.get(i));
     			
    			MileAchievedDao ach = new MileAchievedDao();
    			ArrayList ids = new ArrayList();

    			if (htAchieved.containsKey(mileStoneId))
    			{
    			
    				ach = (MileAchievedDao) htAchieved.get(mileStoneId);
    				ids = ach.getId();
    				achDetailCount = ids.size();
    				
    				if (countPerMileStone > 1 &&  achDetailCount > 0)
    				{
    					//calculate the milestones achieved
    					
    					totalMilestonesAchieved = Math.round(Math.floor(achDetailCount/countPerMileStone));
    					
    					calculateAchievedRows = totalMilestonesAchieved * countPerMileStone;
    					
    				}
    				else
    				{
    					totalMilestonesAchieved = achDetailCount;
    					calculateAchievedRows  = achDetailCount;
    					
    				}
    				
    				// remove extra achieved rows from MileAchieveddao if any
    				
    				if (calculateAchievedRows  < achDetailCount)
    				{
    					 
    					 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, have to delete extra rows for milestone: " +mileStoneId + " remove rows: " + (achDetailCount - calculateAchievedRows));
    					 //remove after index calculateAchievedRows
    					 
    					 ach.removeFrom(calculateAchievedRows);
    					 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, left: " + (ach.getId()).size());
    					    					  
    		            htAchieved.put(mileStoneId,ach);
    		            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, removed extra rows");
    		            	
    				}
    				
    			}
    			else
    			{
    				achDetailCount = 0;
    				totalMilestonesAchieved = 0;
    			}
    			
    			mileDao.setMilestoneAchievedCounts(String.valueOf(totalMilestonesAchieved));
    			
    			
    		    			
     		}
            
            this.setMilestoneDao(mileDao);

            
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "MileAchievedDao.getAchievedMilestones EXCEPTION IN FETCHING FROM er_invoice table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

	public ArrayList getPatientCode() {
		return patientCode;
	}

	public void setPatientCode(ArrayList patientCode) {
		this.patientCode = patientCode;
	}
	
	public void setPatientCode(String patCode) {
		this.patientCode.add(patCode);
	}
	
	
	///////////
	/**
     * Gets Achieved milestones
     * 
     * @param mileStoneId
     *            mileStoneId
     */
    public void getAchievedMilestones(int mileStoneId , String user) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        String milestone = "";
        
        MilestoneDao mileDao = new MilestoneDao(); 
         MileAchievedDao mdTemp = new MileAchievedDao();
         int userPk = 0;
         
         long calculateAchievedRows = 0;
         long countPerMileStone = 0;
        
         long totalMilestonesAchieved = 0;
         
        try {
        	
        	userPk = StringUtil.stringToNum(user) ;
        	
            conn = getConnection();
            
            sqlDetail.append("select pk_mileachieved,milestone_count,  fk_milestone, nvl(fk_per,0) fk_per , m.fk_study, fk_patprot, to_char(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ach_date, milestone_type,milestone_amount ");
            
            sqlDetail.append(" ,nvl(pkg_patient.f_get_patcodes(fk_per),' ') patfacilityIds");
            sqlDetail.append(" from er_milestone p,er_mileachieved m " );
            
            sqlDetail.append(" where pk_milestone = ? and pk_milestone = fk_milestone and milestone_achievedcount > 0 and m.is_complete = 1 ");
            
           /* if (userPk > 0) //add filter for site
            {
            	
            	sqlDetail.append(" and ((fk_per is null) or ( 0 <  pkg_user.f_chk_studyright_using_pat(fk_per, p.fk_study, ?)  ) ) ");
            	
            }*/
            
            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones SQL" + sqlDetail.toString());
            
            pstmt = conn.prepareStatement(sqlDetail.toString());
            pstmt.setInt(1, mileStoneId);
            
        	/*if (userPk > 0) //add filter for site
            {
        		pstmt.setInt(2,userPk);
            }*/	
            
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	
            	milestone = rs.getString("fk_milestone");
            	
            
            	mdTemp.setId( rs.getString("pk_mileachieved"));
            	mdTemp.setMilestone(milestone);
                
            	mdTemp.setStudy(rs.getString("fk_study"));
            	mdTemp.setPatient(rs.getString("fk_per"));
            	mdTemp.setPatprot(rs.getString("fk_patprot"));
            	mdTemp.setAchievedOn(rs.getString("ach_date"));
            	mdTemp.setAmountDue(rs.getString("milestone_amount"));
            	
            	mdTemp.setPatientCode(rs.getString("patfacilityIds"));
                
                Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones"
                        + mdTemp.id.size());
                
                rows++;
                
            }
            
            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones SQL rows" + rows);
            // get milestones for the milestone type and recievable status
            
            mileDao.getMilestoneDetails(mileStoneId);
            
            
            ArrayList achCounts = mileDao.getMilestoneAchievedCounts();
            ArrayList countPerMilestone = mileDao.getMilestoneCounts();
            
            if (achCounts != null && achCounts.size() > 0)
            {
            	totalMilestonesAchieved = StringUtil.stringToNum((String)(achCounts.get(0)));
            	countPerMileStone = StringUtil.stringToNum((String)(countPerMilestone.get(0)));
            }
            
            if (countPerMileStone > 1)
            {
            	calculateAchievedRows =  totalMilestonesAchieved * countPerMileStone;
            }
            else
            {
            	calculateAchievedRows =  totalMilestonesAchieved ;
            	
            }
            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones SQL calculateAchievedRows" + calculateAchievedRows + "rows" + rows);
           //          remove extra achieved rows from MileAchieveddao if any
			
			if (calculateAchievedRows  < rows)
			{
				 
				 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, have to delete extra rows for milestone: " +mileStoneId + " remove rows: " + (rows - calculateAchievedRows));
				 //remove after index calculateAchievedRows
				 
				 mdTemp.removeFrom(calculateAchievedRows);
				 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, left: " + (mdTemp.getId()).size());
				    					  
	            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, removed extra rows");
	            	
			}
            
		   this.setHtAchieved(milestone, new MileAchievedDao(mdTemp));
	        this.setMilestoneDao(mileDao);

            
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "MileAchievedDao.getAchievedMilestones(milestoneId) EXCEPTION :"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }
    
    
    /**
     * Gets Achieved milestones For user
     * 
     * @param studyId
     *            study id
     */
    public void getAchievedMilestonesForUser(int studyId, String rangeFrom,String rangeTo, String milestoneReceivableStatus,String milestoneType, String user) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        String milestone = "";
        String prevMilestone = "";
        MilestoneDao mileDao = new MilestoneDao(); 
         MileAchievedDao mdTemp = new MileAchievedDao();
         String payAbleStatus = "";
         int userPk = 0;
        
        try {
        	
        	userPk = StringUtil.stringToNum(user);
        	
        	if (StringUtil.isEmpty(milestoneReceivableStatus)) //from payment module, to get info for all types of milestones
        	{
        		
        		payAbleStatus = "";
        	}
        	else
        	{
        		payAbleStatus = "rec"; // for receivable milestones, for invoicing
        		
        	}
           	
            conn = getConnection();
            
            sqlDetail.append("select pk_mileachieved, fk_milestone, nvl(fk_per,0) fk_per , m.fk_study, fk_patprot, to_char(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ach_date, milestone_type,milestone_amount ");
            
            if (! StringUtil.isEmpty(payAbleStatus)) // otherwise we dont need calculated invoice amount
            {
            	sqlDetail.append(", nvl((select sum(amount_invoiced) from er_invoice_detail det where det.fk_milestone = m.fk_milestone and det.fk_per = m.fk_per ),0) amount_invoiced ");
            }	
            else
            {
            	sqlDetail.append(", '' amount_invoiced " ); 
            }
            
            sqlDetail.append(" ,nvl(pkg_patient.f_get_patcodes(fk_per),' ') patfacilityIds");
            sqlDetail.append(" from er_milestone,er_mileachieved m " );
            
            if (payAbleStatus.equals("rec"))
            {
            	sqlDetail.append(", er_codelst p ");
            }	
            
            sqlDetail.append(" where m.fk_study = ? and pk_milestone = fk_milestone and milestone_achievedcount > 0 and m.is_complete = 1 and  milestone_Type = ?  ");
            
            if (! StringUtil.isEmpty(rangeFrom))
            {
            	sqlDetail.append(" AND TO_DATE(TO_CHAR(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT),PKG_DATEUTIL.F_GET_DATEFORMAT) BETWEEN TO_DATE(?,PKG_DATEUTIL.F_GET_DATEFORMAT) AND TO_DATE(?,PKG_DATEUTIL.F_GET_DATEFORMAT) ");
            	
            }
            
            if (payAbleStatus.equals("rec"))
            {
            	sqlDetail.append(" and p.pk_codelst = milestone_paytype and trim(p.codelst_subtyp) = 'rec'  ");
            }	
            
            if (milestoneReceivableStatus.equals("UI"))
            {
            	
            	sqlDetail.append(" and not exists (select * from er_invoice_detail det where det.fk_milestone = pk_milestone)  ");
            	
            }
            
            /*if (userPk > 0) //add filter for site
            {
            	
            	sqlDetail.append(" and ((fk_per is null) or ( 0 <  pkg_user.f_chk_studyright_using_pat(fk_per, m.fk_study, ?)  ) ) ");
            	
            }*/
            
            sqlDetail.append(" order by fk_milestone ");
            
            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesForUser SQL" + sqlDetail.toString());
            
            pstmt = conn.prepareStatement(sqlDetail.toString());
            pstmt.setInt(1, studyId);
            pstmt.setString(2, milestoneType);
            
            if (! StringUtil.isEmpty(rangeFrom))
            {
            	pstmt.setString(3,rangeFrom );
            	pstmt.setString(4,rangeTo );
            	
            	/*if (userPk > 0) //add filter for site
                {
            		pstmt.setInt(5,userPk);
                }*/	
            	
            }
            else
            {
            	/*if (userPk > 0) //add filter for site
                {
            		pstmt.setInt(3,userPk);
                }*/
            	
            }
            

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	
            	milestone = rs.getString("fk_milestone");
            	
            	if (rows == 0)
            	{
            		prevMilestone = milestone;
            		
            	}
            	
            	if ( ! prevMilestone.equals(milestone) )
            	{
            		//set it to hashtable
            		 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesForUser adding to hashtable" +prevMilestone);
            		 this.setHtAchieved(prevMilestone, new MileAchievedDao(mdTemp));
            		 mdTemp = new MileAchievedDao();
            		//this.reset(); //clear data arraylists
            	}
            		
            	
            	mdTemp.setId( rs.getString("pk_mileachieved"));
            	mdTemp.setMilestone(milestone);
                
            	mdTemp.setStudy(rs.getString("fk_study"));
            	mdTemp.setPatient(rs.getString("fk_per"));
            	mdTemp.setPatprot(rs.getString("fk_patprot"));
            	mdTemp.setAchievedOn(rs.getString("ach_date"));
            	mdTemp.setAmountDue(rs.getString("milestone_amount"));
            	mdTemp.setAmountInvoiced(rs.getString("amount_invoiced"));
            	Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesForUser setAmountInvoiced "+ rs.getString("amount_invoiced") );
            	
            	mdTemp.setPatientCode(rs.getString("patfacilityIds"));
               
                prevMilestone = milestone;
                
                Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestonesForUser"
                        + mdTemp.id.size());
                
                rows++;
                
            }
            
            this.setHtAchieved(milestone, new MileAchievedDao(mdTemp));
            mdTemp = new MileAchievedDao();
            
            
            // get milestones for the milestone type and recievable status
            
            mileDao.getMilestoneRows(studyId, milestoneType, payAbleStatus, milestoneReceivableStatus);
            
            //iterate throughthe milestones, and calculate the number of milestones achieved for the given date range
            
            ArrayList mileStoneIds = new ArrayList();
            ArrayList milestoneCounts = new ArrayList();
            int mileCount = 0;
            String mileStoneId  = "";
            int countPerMileStone = 0;
            int achDetailCount = 0;
            long totalMilestonesAchieved = 0;
            long calculateAchievedRows = 0;
            
            mileStoneIds = mileDao.getMilestoneIds();
            
            mileCount = mileStoneIds.size();
            milestoneCounts = mileDao.getMilestoneCounts ();
            
            for (int i =0; i < mileCount ; i++)
     		{
     		
     			mileStoneId = String.valueOf(((Integer)mileStoneIds.get(i)).intValue());
     			
     			//count required to make one milestone
     			countPerMileStone = StringUtil.stringToNum((String) milestoneCounts.get(i));
     			
    			MileAchievedDao ach = new MileAchievedDao();
    			ArrayList ids = new ArrayList();

    			if (htAchieved.containsKey(mileStoneId))
    			{
    			
    				ach = (MileAchievedDao) htAchieved.get(mileStoneId);
    				ids = ach.getId();
    				achDetailCount = ids.size();
    				
    				if (countPerMileStone > 1 &&  achDetailCount > 0)
    				{
    					//calculate the milestones achieved
    					
    					totalMilestonesAchieved = Math.round(Math.floor(achDetailCount/countPerMileStone));
    					calculateAchievedRows = totalMilestonesAchieved * countPerMileStone;

    					
    				}
    				else
    				{
    					totalMilestonesAchieved = achDetailCount;
    					calculateAchievedRows  = achDetailCount;
    				}
    				
    			}
    			else
    			{
    				achDetailCount = 0;
    				totalMilestonesAchieved = 0;
    			}
    			
    			//    			 remove extra achieved rows from MileAchieveddao if any
				
				if (calculateAchievedRows  < achDetailCount)
				{
					 
					 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, have to delete extra rows for milestone: " +mileStoneId + " remove rows: " + (achDetailCount - calculateAchievedRows));
					 //remove after index calculateAchievedRows
					 
					 ach.removeFrom(calculateAchievedRows);
					 Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, left: " + (ach.getId()).size());
					    					  
		            htAchieved.put(mileStoneId,ach);
		            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones, removed extra rows");
		            	
				}
				
    			mileDao.setMilestoneAchievedCounts(String.valueOf(totalMilestonesAchieved));
    			
    			
    		    			
     		}
            
            this.setMilestoneDao(mileDao);

            
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "MileAchievedDao.getAchievedMilestonesForUser EXCEPTION IN FETCHING FROM er_invoice table"
                            + ex);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }
    
    /**
     * 
     * @author Sonia Abrol
     * date - 08/26/08
     *  
     * Gets milestone Achievemnt details
     * 
     * @param studyId
     *            study id
     * @param   milestoneType milestone type code
     * @param htParams ashtable of additional parameters (for later use)        
     */
    public void getALLAchievedMilestones(int studyId, String milestoneType , Hashtable htParams) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        String milestone = "";
        String prevMilestone = "";
        String mileDesc = "";
        String prevMileDesc = "";
        String prevAchCount = "";
        String achCount = "";
        String prevCount = "";
        String count = "";
        String mileAmount = "";
        String prevMileAmount = "";
        
        
          
         MileAchievedDao mdTemp = new MileAchievedDao();
          
        
        try {
        	
        	 
            conn = getConnection();
            
            sqlDetail.append("select pk_mileachieved, milestone_count, pk_milestone, nvl(fk_per,0) fk_per , m.fk_study, fk_patprot, to_char(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ach_date, milestone_type,milestone_amount ");
            
             
            sqlDetail.append(" ,nvl(pkg_patient.f_get_patcodes(fk_per),' ') patfacilityIds, (select count(*) from er_invoice_detail invd where invd.fk_milestone = pk_milestone) inv_count ,");
            
            sqlDetail.append("(select count(*) from er_milepayment_details where fk_mileachieved = pk_mileachieved and nvl(mp_amount,0) > 0 ) pay_count, Pkg_Milestone_New.f_getMilestoneDesc( PK_MILESTONE) mileDesc,milestone_achievedcount ");
            sqlDetail.append(", (decode(?,'SM',(select to_char(studystat_date,PKG_DATEUTIL.F_GET_DATEFORMAT) from er_studystat where pk_studystat=fk_per ),' ')) study_status_date ");
            
            sqlDetail.append(" from er_milestone,er_mileachieved m " );
            
            //KM-#D-FIN7
            sqlDetail.append(" where er_milestone.fk_study = ? and m.fk_milestone (+)= pk_milestone  and milestone_Type = ? and fk_codelst_milestone_stat = (select pk_codelst from er_codelst where codelst_type ='milestone_stat' and codelst_subtyp='A') ");
            
            sqlDetail.append(" and MILESTONE_DELFLAG !='Y' ");
            
            sqlDetail.append(" order by pk_milestone,ach_date,pk_mileachieved ");
            
            Rlog.debug("milestone", "MileAchievedDao.getAchievedMilestones SQL" + sqlDetail.toString());
            
            pstmt = conn.prepareStatement(sqlDetail.toString());
            
            pstmt.setString(1, milestoneType);
            pstmt.setInt(2, studyId);
            pstmt.setString(3, milestoneType);


            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
            	
            	milestone = rs.getString("pk_milestone");
            	mileDesc= rs.getString("mileDesc");
            	achCount =  rs.getString("milestone_achievedcount");
            	mileAmount =  rs.getString("milestone_amount");
            	count = rs.getString("milestone_count");
            	
            	if (rows == 0)
            	{
            		prevMilestone = milestone;
            		prevMileDesc = mileDesc;
            		prevAchCount = achCount;
            		prevMileAmount = mileAmount;
            		prevCount = count ;
            	}
            	
            	if ( ! prevMilestone.equals(milestone) )
            	{
            		//set it to hashtable
            		  
            		 this.setHtAchieved(prevMilestone, new MileAchievedDao(mdTemp));
            		 this.setMilestone(prevMilestone);
            		 this.setMilestoneDesc(prevMileDesc);
            		 milestoneDao.setMilestoneAchievedCounts(prevAchCount);
            		 milestoneDao.setMilestoneAmounts(prevMileAmount);
            		 milestoneDao.setMilestoneCounts(prevCount);
            		 
            		 mdTemp = new MileAchievedDao();
            		//this.reset(); //clear data arraylists
            		  
            	}
            	
            	 
            	 
            	
            	mdTemp.setId( rs.getString("pk_mileachieved"));
            	mdTemp.setMilestone(milestone);
                mdTemp.setStudy(rs.getString("fk_study"));
            	mdTemp.setPatient(rs.getString("fk_per"));
            	mdTemp.setPatprot(rs.getString("fk_patprot"));
            	mdTemp.setAchievedOn(rs.getString("ach_date"));
            	//mdTemp.setAmountDue(rs.getString("milestone_amount"));
             
            	if (milestoneType.equals("SM"))
            	{
            		mdTemp.setPatientCode(rs.getString("study_status_date"));
            	}
            	else
            	{
            		mdTemp.setPatientCode(rs.getString("patfacilityIds"));
               	}
            	
            	mdTemp.setArInvCount(new Integer(rs.getInt("inv_count")));
            	mdTemp.setArPaymentCount(new Integer(rs.getInt("pay_count")));
            	 
               
                prevMilestone = milestone;
                prevAchCount = achCount;
                prevMileDesc = mileDesc;
                prevMileAmount = mileAmount;
                prevCount = count; 
                
                Rlog.debug("milestone", "MileAchievedDao.getALLAchievedMilestones"
                        + mdTemp.id.size());
                
                rows++;
                
            }
            
            
            
            if (rows > 0)
            {
            	this.setHtAchieved(milestone, new MileAchievedDao(mdTemp));
                this.setMilestone(milestone);
                this.setMilestoneDesc(mileDesc);
                
            	milestoneDao.setMilestoneAchievedCounts(achCount);
            	milestoneDao.setMilestoneAmounts(mileAmount);
            	milestoneDao.setMilestoneCounts(count);
            }
            
            mdTemp = new MileAchievedDao();
              
            //System.out.println("end of method");
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "MileAchievedDao.getALLAchievedMilestones EXCEPTION IN FETCHING data"
                            + ex );
            ex.printStackTrace();
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }

        }

    }

	public ArrayList getArInvCount() {
		return arInvCount;
	}

	public void setArInvCount(ArrayList arInvCount) {
		this.arInvCount = arInvCount;
	}
	
	public void setArInvCount(Integer invCount) {
		this.arInvCount.add(invCount);
	}

	public ArrayList getArPaymentCount() {
		return arPaymentCount;
	}

	public void setArPaymentCount(ArrayList arPaymentCount) {
		this.arPaymentCount = arPaymentCount;
	}

	public void setArPaymentCount(Integer paymentCount) {
		this.arPaymentCount.add(paymentCount);
	}
	/**
     * Check Achieved milestones
     * @param mileStoneId, studyId
     * 16Mar2011 @Ankit #5924
     */
    public boolean checkAchievedMilestones(int mileStoneId, int studyId) {
        
    	PreparedStatement pstmt = null;
        Connection conn = null;
        StringBuffer sqlDetail = new StringBuffer();
        String milestone = "";
        boolean flage = false;
        try {
        	
        	conn = getConnection();
            
            sqlDetail.append("select pk_mileachieved,milestone_count,  fk_milestone, nvl(fk_per,0) fk_per , m.fk_study, fk_patprot, to_char(ach_date,PKG_DATEUTIL.F_GET_DATEFORMAT) ach_date, milestone_type,milestone_amount ");
            sqlDetail.append(" ,nvl(pkg_patient.f_get_patcodes(fk_per),' ') patfacilityIds");
            sqlDetail.append(" from er_milestone p,er_mileachieved m " );
            sqlDetail.append(" where pk_milestone = ? and pk_milestone = fk_milestone and m.fk_study = ? ");
            
            Rlog.debug("milestone", "MileAchievedDao.checkAchievedMilestones SQL" + sqlDetail.toString());
            
            pstmt = conn.prepareStatement(sqlDetail.toString());
            pstmt.setInt(1, mileStoneId);
            pstmt.setInt(2, studyId);
        	
            ResultSet rs = pstmt.executeQuery();

            if(rs.next()) {
            	flage = true;
            }
            
            Rlog.debug("milestone", "MileAchievedDao.checkAchievedMilestones SQL rows" + flage);
            
        } catch (Exception ex) {
            Rlog.fatal("milestone",
                    "MileAchievedDao.checkAchievedMilestones(milestoneId,StudyId) EXCEPTION :"
                            + ex);
           return flage;
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
        return flage;
    }
    // end of class

}
