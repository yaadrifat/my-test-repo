/*
 * Classname			PatientDao.class
 *
 * Version information 	1.0
 *
 * Date					06/08/2001
 *
 * Copyright notice		Velos, Inc.
 */

package com.velos.eres.business.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;


import com.velos.eres.business.person.impl.PersonBean;
import com.velos.eres.service.util.EnvUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/**
 * PatientDao for getting Patient records
 *
 * @author Sonia Sahni, Sajal
 * @version : 1.0 06/08/2001
 */

public class PatientDao extends CommonDAO implements java.io.Serializable {
    private ArrayList patient;
    
    //AK:Added for INVP.1
    private ArrayList patNames;
    private ArrayList patIds;

    private int cRows;

    public PatientDao() {
        patient = new ArrayList();
        patNames=new ArrayList();
        patIds=new ArrayList();
    }

    // Getter and Setter methods

    public ArrayList getPatient() {
        return this.patient;
    }

    public void setPatient(ArrayList patient) {
        this.patient = patient;
    }

    public void setPatient(PersonBean p) {
        patient.add(p);
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }
    
  //AK:Added for enh INVP9.1 autocomplete
    public ArrayList getPatNames() {
    	return this.patNames;
    }
    public void setPatName(String patName) {
     this.patNames.add(patName);
    }

    public ArrayList getPatIds() {
    	return this.patIds;
    }
    public void setPatId(String patId) {
            this.patIds.add(patId);
    }

    // end of getter and setter methods

    /**
     * Gets patients for an account and/or status . Sets the values in the class
     * attributes
     *
     * @param OrganizationId
     *            of the user
     * @param PatientID
     *            for patient
     * @param Status
     */

    public void getPatients(int usrOrganization, String patientID, int patStatus) {
        PersonBean psk = new PersonBean();
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String selectClause = null;
        String whereClause = null;

        String orderBy = null;
        try {
            conn = getPatConnection();

            selectClause = "Select PK_PERSON ,PERSON_CODE ,PERSON_ALTID ,PERSON_LNAME ,PERSON_FNAME ,PERSON_MNAME ,PERSON_SUFFIX ,PERSON_PREFIX ,PERSON_DEGREE ,PERSON_MOTHER_NAME ,PERSON_DOB ,FK_CODELST_GENDER  ,PERSON_AKA, "
                    + "FK_CODELST_RACE, PERSON_ADD_RACE, FK_CODELST_ETHNICITY, PERSON_ADD_ETHNICITY, FK_CODELST_PRILANG,FK_CODELST_MARITAL ,FK_CODELST_RELIGION ,FK_ACCOUNT ,PERSON_SSN ,PERSON_DRIV_LIC,FK_PERSON_MOTHER,PERSON_ETHGRP , PERSON_BIRTH_PLACE,PERSON_MULTI_BIRTH, "
                    + "FK_CODELST_CITIZEN ,PERSON_MILVET  , FK_CODELST_NATIONALITY ,PERSON_DEATHDT ,FK_SITE ,CREATOR ,CREATED_ON , FK_CODELST_EMPLOYMENT ,FK_CODELST_EDU ,PERSON_NOTES_CLOB , FK_CODELST_PSTAT ,FK_CODELST_BLOODGRP ,PERSON_ADDRESS1 ,PERSON_ADDRESS2 ,PERSON_CITY ,PERSON_STATE ,PERSON_ZIP ,PERSON_COUNTRY ,PERSON_HPHONE ,PERSON_BPHONE ,PERSON_EMAIL ,PERSON_COUNTY ,PERSON_REGDATE ,PERSON_REGBY,FK_CODLST_NTYPE from person  ";

            if (usrOrganization != 0) {
                whereClause = " where FK_SITE = " + usrOrganization;
            }

            if ((patientID != null) && (!(patientID.trim().equals("")))) {
                if (whereClause != null) {
                    whereClause = whereClause
                            + " and UPPER(PERSON_CODE) = UPPER('" + patientID
                            + "')";
                } else {
                    whereClause = " where UPPER(PERSON_CODE) = UPPER('"
                            + patientID + "')";
                }
            }

            if (patStatus != 0) {
                if (whereClause != null) {
                    whereClause = whereClause + " and FK_CODELST_PSTAT = "
                            + patStatus;
                } else {
                    whereClause = " where FK_CODELST_PSTAT = " + patStatus;
                }
            }

            if (whereClause != null) {
                selectClause = selectClause + whereClause;
            }

            orderBy = " Order By PERSON_REGDATE DESC ";
            selectClause = selectClause + orderBy;
            pstmt = conn.prepareStatement(selectClause);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                rows++;

                psk.setPersonPId(rs.getString("PERSON_CODE"));

                psk.setPersonAccount(Integer.toString(rs.getInt("FK_ACCOUNT")));

                psk.setPersonLocation(Integer.toString(rs.getInt("FK_SITE")));
                psk.setPersonPKId(rs.getInt("PK_PERSON"));
                psk.setPersonAlias(rs.getString("PERSON_AKA"));
                psk.setPersonAltId(rs.getString("PERSON_ALTID"));

                psk.setPersonBirthPlace(rs.getString("PERSON_BIRTH_PLACE"));

                psk.setPersonBloodGr(Integer.toString(rs
                        .getInt("FK_CODELST_BLOODGRP")));
                psk.setPersonCitizen(Integer.toString(rs
                        .getInt("FK_CODELST_CITIZEN")));
                psk.setPersonDeathDate(rs.getString("PERSON_DEATHDT"));
                //System.out.println("patsql row3");
                psk.setPersonDegree(Integer
                        .toString(rs.getInt("PERSON_DEGREE")));
                psk.setPersonDob(rs.getString("PERSON_DOB"));
                psk.setPersonDrivLic(rs.getString("PERSON_DRIV_LIC"));
                psk.setPersonEducation(Integer.toString(rs
                        .getInt("FK_CODELST_EDU")));

                psk.setPersonEmployment(Integer.toString(rs
                        .getInt("FK_CODELST_EMPLOYMENT")));
                psk.setPersonEthGroup(Integer.toString(rs
                        .getInt("PERSON_ETHGRP")));
                psk.setPersonFname(rs.getString("PERSON_FNAME"));
                psk.setPersonGender(Integer.toString(rs
                        .getInt("FK_CODELST_GENDER")));
                psk.setPersonLname(rs.getString("PERSON_LNAME"));
                psk.setPersonMarital(Integer.toString(rs
                        .getInt("FK_CODELST_MARITAL")));
                psk.setPersonMname(rs.getString("PERSON_MNAME"));
                psk.setPersonMotherId(Integer.toString(rs
                        .getInt("FK_PERSON_MOTHER")));
                psk.setPersonMotherName(rs.getString("PERSON_MOTHER_NAME"));

                //System.out.println("patsql row4");
                psk.setPersonMultiBirth(rs.getString("PERSON_MULTI_BIRTH"));
                psk.setPersonNationality(Integer.toString(rs
                        .getInt("FK_CODELST_NATIONALITY")));
                psk.setPersonNotes(rs.getString("PERSON_NOTES_CLOB"));

                psk.setPersonPrefix(rs.getString("PERSON_PREFIX"));
                psk.setPersonPrimaryLang(Integer.toString(rs
                        .getInt("FK_CODELST_PRILANG")));
                psk.setPersonRace(Integer
                        .toString(rs.getInt("FK_CODELST_RACE")));
                psk.setPersonAddRace(rs.getString("PERSON_ADD_RACE"));
                psk.setPersonEthnicity(Integer.toString(rs
                        .getInt("FK_CODELST_ETHNICITY")));
                psk.setPersonAddEthnicity(rs.getString("PERSON_ADD_ETHNICITY"));

                psk.setPersonRelegion(Integer.toString(rs
                        .getInt("FK_CODELST_RELIGION")));
                psk.setPersonSSN(rs.getString("PERSON_SSN"));
                psk.setPersonStatus(Integer.toString(rs
                        .getInt("FK_CODELST_PSTAT")));

                System.out.println("patsql row5");
                psk.setPersonSuffix(rs.getString("PERSON_SUFFIX"));
                psk.setPersonVetMilStatus(rs.getString("PERSON_MILVET"));
                psk.setPersonAddress1(rs.getString("PERSON_ADDRESS1"));
                psk.setPersonAddress2(rs.getString("PERSON_ADDRESS2"));
                psk.setPersonCity(rs.getString("PERSON_CITY"));
                psk.setPersonCountry(rs.getString("PERSON_COUNTRY"));
                psk.setPersonCounty(rs.getString("PERSON_COUNTY"));
                psk.setPersonEmail(rs.getString("PERSON_EMAIL"));
                psk.setPersonHphone(rs.getString("PERSON_HPHONE"));
                psk.setPersonBphone(rs.getString("PERSON_BPHONE"));

                System.out.println("patsql row6");

                psk.setPersonState(rs.getString("PERSON_STATE"));
                psk.setPersonZip(rs.getString("PERSON_ZIP"));
                psk.setPersonRegDate(rs.getString("PERSON_REGDATE"));
                psk.setPersonRegBy(Integer.toString(rs.getInt("PERSON_REGBY")));

                System.out.println("patsql row7");
                setPatient(psk);
                psk = new PersonBean();
            }
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("patient",
                    "PATIENT DAO.getPatients EXCEPTION IN FETCHING FROM PATIENT table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * Implements patient search Sets the values in the class attributes
     *
     * @param siteId
     *            of the user
     * @param studyID
     * @param patientCode
     * @param dob
     * @param lname
     * @param fname
     */

    public void searchPatients(String siteId, String studyId, String patientCode) {
        PersonBean psk = new PersonBean();
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String selectClause = null;
        String whereClause = null;
        String orderBy = null;
        try {
            conn = EnvUtil.getConnection();
            selectClause = "Select PK_PERSON, PERSON_CODE, PERSON_ALTID, PERSON_LNAME, PERSON_FNAME, "
                    + "PERSON_MNAME, PERSON_SUFFIX, PERSON_PREFIX, PERSON_DEGREE, PERSON_MOTHER_NAME, "
                    + "PERSON_DOB, FK_CODELST_GENDER , PERSON_AKA, "
                    + "FK_CODELST_RACE, PERSON_ADD_RACE, FK_CODELST_ETHNICITY, PERSON_ADD_ETHNICITY"
                    + "FK_CODELST_PRILANG, "
                    + "FK_CODELST_MARITAL, FK_CODELST_RELIGION, FK_ACCOUNT, PERSON_SSN, PERSON_DRIV_LIC, "
                    + "FK_PERSON_MOTHER, PERSON_ETHGRP, PERSON_BIRTH_PLACE, PERSON_MULTI_BIRTH, "
                    + "FK_CODELST_CITIZEN, PERSON_MILVET, FK_CODELST_NATIONALITY, PERSON_DEATHDT, FK_SITE, "
                    + "CREATOR, CREATED_ON,  FK_CODELST_EMPLOYMENT, FK_CODELST_EDU, PERSON_NOTES_CLOB,  "
                    + "FK_CODELST_PSTAT, FK_CODELST_BLOODGRP, PERSON_ADDRESS1, PERSON_ADDRESS2, PERSON_CITY, "
                    + "PERSON_STATE, PERSON_ZIP, PERSON_COUNTRY, PERSON_HPHONE, PERSON_BPHONE, PERSON_EMAIL, "
                    + "PERSON_COUNTY, PERSON_REGDATE, PERSON_REGBY, FK_CODLST_NTYPE "
                    + "from person "
                    + "where  FK_SITE = "
                    + siteId
                    + " and FK_CODELST_PSTAT = (select ctrl_value from er_ctrltab where ctrl_key = 'active' ) "
                    + "and PK_PERSON not in (select distinct fk_per from er_patprot where fk_study = "
                    + studyId + ") ";

            if ((patientCode != null) && (!(patientCode.trim().equals("")))) {

                whereClause = " and UPPER(PERSON_CODE) = UPPER('" + patientCode
                        + "')";
                selectClause = selectClause + whereClause;
            }

            orderBy = " Order By PERSON_LNAME, PERSON_FNAME ";
            selectClause = selectClause + orderBy;
            pstmt = conn.prepareStatement(selectClause);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                rows++;
                psk.setPersonPId(rs.getString("PERSON_CODE"));
                psk.setPersonAccount(Integer.toString(rs.getInt("FK_ACCOUNT")));
                psk.setPersonLocation(Integer.toString(rs.getInt("FK_SITE")));
                psk.setPersonPKId(rs.getInt("PK_PERSON"));
                psk.setPersonAlias(rs.getString("PERSON_AKA"));
                psk.setPersonAltId(rs.getString("PERSON_ALTID"));
                psk.setPersonBirthPlace(rs.getString("PERSON_BIRTH_PLACE"));
                psk.setPersonBloodGr(Integer.toString(rs
                        .getInt("FK_CODELST_BLOODGRP")));
                psk.setPersonCitizen(Integer.toString(rs
                        .getInt("FK_CODELST_CITIZEN")));
                psk.setPersonDeathDate(rs.getString("PERSON_DEATHDT"));
                psk.setPersonDegree(Integer
                        .toString(rs.getInt("PERSON_DEGREE")));
                psk.setPersonDob(rs.getString("PERSON_DOB"));
                psk.setPersonDrivLic(rs.getString("PERSON_DRIV_LIC"));
                psk.setPersonEducation(Integer.toString(rs
                        .getInt("FK_CODELST_EDU")));
                psk.setPersonEmployment(Integer.toString(rs
                        .getInt("FK_CODELST_EMPLOYMENT")));
                psk.setPersonEthGroup(Integer.toString(rs
                        .getInt("PERSON_ETHGRP")));
                psk.setPersonFname(rs.getString("PERSON_FNAME"));
                psk.setPersonGender(Integer.toString(rs
                        .getInt("FK_CODELST_GENDER")));
                psk.setPersonLname(rs.getString("PERSON_LNAME"));
                psk.setPersonMarital(Integer.toString(rs
                        .getInt("FK_CODELST_MARITAL")));
                psk.setPersonMname(rs.getString("PERSON_MNAME"));
                psk.setPersonMotherId(Integer.toString(rs
                        .getInt("FK_PERSON_MOTHER")));
                psk.setPersonMotherName(rs.getString("PERSON_MOTHER_NAME"));
                psk.setPersonMultiBirth(rs.getString("PERSON_MULTI_BIRTH"));
                psk.setPersonNationality(Integer.toString(rs
                        .getInt("FK_CODELST_NATIONALITY")));
                psk.setPersonNotes(rs.getString("PERSON_NOTES_CLOB"));
                psk.setPersonPrefix(rs.getString("PERSON_PREFIX"));
                psk.setPersonPrimaryLang(Integer.toString(rs
                        .getInt("FK_CODELST_PRILANG")));
                psk.setPersonRace(Integer
                        .toString(rs.getInt("FK_CODELST_RACE")));
                psk.setPersonAddRace(rs.getString("PERSON_ADD_RACE"));
                psk.setPersonEthnicity(Integer.toString(rs
                        .getInt("FK_CODELST_ETHNICITY")));
                psk.setPersonAddEthnicity(rs.getString("PERSON_ADD_ETHNICITY"));
                psk.setPersonRelegion(Integer.toString(rs
                        .getInt("FK_CODELST_RELIGION")));
                psk.setPersonSSN(rs.getString("PERSON_SSN"));
                psk.setPersonStatus(Integer.toString(rs
                        .getInt("FK_CODELST_PSTAT")));
                psk.setPersonSuffix(rs.getString("PERSON_SUFFIX"));
                psk.setPersonVetMilStatus(rs.getString("PERSON_MILVET"));
                psk.setPersonAddress1(rs.getString("PERSON_ADDRESS1"));
                psk.setPersonAddress2(rs.getString("PERSON_ADDRESS2"));
                psk.setPersonCity(rs.getString("PERSON_CITY"));
                psk.setPersonCountry(rs.getString("PERSON_COUNTRY"));
                psk.setPersonCounty(rs.getString("PERSON_COUNTY"));
                psk.setPersonEmail(rs.getString("PERSON_EMAIL"));
                psk.setPersonHphone(rs.getString("PERSON_HPHONE"));
                psk.setPersonBphone(rs.getString("PERSON_BPHONE"));
                psk.setPersonState(rs.getString("PERSON_STATE"));
                psk.setPersonZip(rs.getString("PERSON_ZIP"));
                psk.setPersonRegDate(rs.getString("PERSON_REGDATE"));
                psk.setPersonRegBy(Integer.toString(rs.getInt("PERSON_REGBY")));
                setPatient(psk);
                psk = new PersonBean();
            }
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("patient",
                    "PATIENT DAO.searchPatients EXCEPTION IN FETCHING FROM PATIENT table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * @param siteId
     *            entered by user
     * @param patientCode
     */

    public boolean patientCodeExists(String patientCode, String siteId) {
        boolean codeExists = false;
        PreparedStatement pstmt = null;
        PreparedStatement pstmtSite = null;
        Connection conn = null;
        int count=0;
        try {
           conn=EnvUtil.getConnection();
            String countSite = this.CreateOrgTree(siteId);


            try {
                if (pstmtSite != null)
                	pstmtSite.close();
            } catch (Exception e) {
            }
            String sql="select count(*) count "
                + "from er_per "
                + "where lower(trim(PER_CODE)) = lower(trim(?)) "
                + "and FK_SITE in (" + countSite + ")";

            pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, patientCode);


            ResultSet rs = pstmt.executeQuery();

            rs.next();
            count = rs.getInt("count");
            if (count > 0) {
                codeExists = true;
            }
        } catch (SQLException ex) {
        	ex.printStackTrace();
            Rlog.fatal("person", "PatientDao.patientCodeExists " + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return codeExists;

    }

    public boolean patientCodeExistsOneOrg(String patientCode, String siteId) {
        boolean codeExists = false;
        String test;
        PreparedStatement pstmt = null;
        PreparedStatement pstmtSite = null;
        Connection conn = null;
        int count=0;
        try {
           conn=EnvUtil.getConnection();
           String sql="select count(fk_site) count "
                + "from er_per "
                + "where lower(trim(PER_CODE)) = lower(trim(?)) "
                + "and FK_SITE = " + siteId  ;

            pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, patientCode);


            ResultSet rs = pstmt.executeQuery();

            // test = rs.getString("count");

            rs.next();

            count = rs.getInt("count");
            if (count > 0) {
                codeExists = true;
            }
        } catch (SQLException ex) {
        	ex.printStackTrace();
            Rlog.fatal("person", "PatientDao.patientCodeExistsOneOrg " + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return codeExists;

    }

    // to delete patient.
    // km
    public int deletePatient(int patientId) {   // Modified for INF-18183 ::: Akshi
    	int ret=0;
        CallableStatement cstmt = null;
        Rlog.debug("person", "In deletePatient in PatientDao line number 0");
        Connection conn = null;
        try {
            conn = EnvUtil.getConnection();
            cstmt = conn.prepareCall("{call delete_patient(?)}");

            cstmt.setInt(1, patientId);
            cstmt.execute();
        } catch (SQLException ex) {
        	ret = -1;
            Rlog.fatal("person",
                    "In deletePatient in PatientDao line EXCEPTION IN calling the procedure"
                            + ex);
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
        return ret;
    }

    // km

    // ////////

    public void searchPatientsForEnr(String siteId, String studyId,
            String patientCode, String age, String genderVal, String regBy,
            String pstat, String pName, String pStudyExists) {
        PersonBean psk = new PersonBean();
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String selectClause = null;
        String fromClause = null;
        String whereClause = null;
        String orderBy = null;
        StringBuffer whereCl = new StringBuffer();
        int lowLimit = 0, highLimit = 0;
        String ageFilter = "";
        Calendar cal = new GregorianCalendar();
        String sqlStr = null;

        try {
            conn = EnvUtil.getConnection();
            selectClause = " Select PK_PERSON, PERSON_CODE, PERSON_DOB, FK_CODELST_GENDER , person.FK_ACCOUNT, FK_SITE,FK_CODELST_PSTAT, PERSON_REGBY, PERSON_FNAME ,PERSON_LNAME, PERSON_MNAME   ";

            fromClause = " from person ";
            whereClause = " where FK_SITE = "
                    + siteId
                    + " and PK_PERSON not in (select distinct fk_per from er_patprot where fk_study = "
                    + studyId + ")  ";

            // + " and FK_CODELST_PSTAT = (select ctrl_value from er_ctrltab
            // where ctrl_key = 'active' ) "

            // patient code

            if ((patientCode != null) && (!(patientCode.trim().equals("")))) {
                whereCl.append(" and (UPPER(PERSON_CODE) LIKE UPPER('%"
                        + patientCode + "%'))");
            }

            // patient status

            if ((pstat != null) && (!(pstat.trim().equals("")))) {
                whereCl.append(" and (fk_codelst_pstat =  "
                        + StringUtil.stringToNum(pstat) + " )");
            }

            // patient gender

            if ((genderVal != null) && (!(genderVal.trim().equals("")))) {
                whereCl.append("  and ( fk_codelst_gender  = " + genderVal
                        + " ) ");
            }

            // patient age
            if ((age != null) && (!(age.trim().equals("")))) {
                if (age.equals("0,15")) {
                    lowLimit = cal.get(Calendar.YEAR)
                            - StringUtil.stringToNum("0");
                    highLimit = cal.get(Calendar.YEAR)
                            - StringUtil.stringToNum("15");
                    ageFilter = " and ((SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) >= '"
                            + highLimit
                            + "' ) and (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) <='"
                            + lowLimit + "'))";
                }// end for age.equals("1,15")
                if (age.equals("16,30")) {
                    lowLimit = cal.get(Calendar.YEAR)
                            - StringUtil.stringToNum("16");
                    highLimit = cal.get(Calendar.YEAR)
                            - StringUtil.stringToNum("30");
                    ageFilter = " and ((SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) >= '"
                            + highLimit
                            + "' ) and (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) <='"
                            + lowLimit + "'))";
                }// end for age.equals("16,30")
                if (age.equals("31,45")) {
                    lowLimit = cal.get(Calendar.YEAR)
                            - StringUtil.stringToNum("31");
                    highLimit = cal.get(Calendar.YEAR)
                            - StringUtil.stringToNum("45");
                    ageFilter = " and ((SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) >= '"
                            + highLimit
                            + "' ) and (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) <='"
                            + lowLimit + "'))";
                }// end for age.equals("31,45")
                if (age.equals("46,60")) {
                    lowLimit = cal.get(Calendar.YEAR)
                            - StringUtil.stringToNum("46");
                    highLimit = cal.get(Calendar.YEAR)
                            - StringUtil.stringToNum("60");
                    ageFilter = " and ((SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) >= '"
                            + highLimit
                            + "' ) and (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) <='"
                            + lowLimit + "'))";
                }// end for age.equals("46,60")
                if (age.equals("61,75")) {
                    lowLimit = cal.get(Calendar.YEAR)
                            - StringUtil.stringToNum("61");
                    highLimit = cal.get(Calendar.YEAR)
                            - StringUtil.stringToNum("75");
                    ageFilter = " and ((SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) >= '"
                            + highLimit
                            + "' ) and (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) <='"
                            + lowLimit + "'))";
                }// end for age.equals("61,75")
                if (age.equals("76,Over")) {
                    lowLimit = cal.get(Calendar.YEAR)
                            - StringUtil.stringToNum("76");
                    ageFilter = " and (SUBSTR(TO_CHAR(person_dob,'YYYYMMDD'),0,4) <= '"
                            + lowLimit + "' )";
                }// end for age.equals("76,Over")

                whereCl.append(ageFilter);
            }

            // patient registered by

            if ((regBy != null) && (!(regBy.trim().equals("")))) {
                fromClause = fromClause + ",er_user c ";
                whereCl
                        .append("and c.pk_user = person_regby  and ( ( lower(c.usr_lastname) like lower('%"
                                + regBy
                                + "%') ) or ( lower(c.usr_firstname) like lower('%"
                                + regBy
                                + "%') ) or ( lower(c.usr_firstname||' '||c.usr_lastname) like lower('%"
                                + regBy
                                + "%') ) or ( lower(c.usr_lastname||' '||c.usr_firstname) like lower('%"
                                + regBy + "%') ))");
            }

            // patient pStudyExists

            if ((pStudyExists != null) && (!pStudyExists.equals("0"))
                    && (!(pStudyExists.trim().equals("")))) {
                whereCl
                        .append(" and ( PK_PERSON  in (select distinct fk_per from er_patprot where fk_study = "
                                + pStudyExists + ") )  ");
            }

            if ((pName != null) && (!(pName.trim().equals("")))) {
                whereCl.append(" and  ( lower( person_fname) like lower('%"
                        + pName.trim()
                        + "%') or   lower(person_lname) like lower('%"
                        + pName.trim()
                        + "%')  or  lower (person_mname ) like lower('%"
                        + pName.trim() + "%') )");
            }

            orderBy = " Order By PERSON_CODE ";

            sqlStr = selectClause + fromClause + whereClause
                    + whereCl.toString() + orderBy;

            Rlog.debug("patient", "In searchPatientForEnr sql " + sqlStr);

            pstmt = conn.prepareStatement(sqlStr);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                rows++;
                psk.setPersonPId(rs.getString("PERSON_CODE"));
                psk.setPersonAccount(Integer.toString(rs.getInt("FK_ACCOUNT")));
                psk.setPersonLocation(Integer.toString(rs.getInt("FK_SITE")));
                psk.setPersonPKId(rs.getInt("PK_PERSON"));

                psk.setPersonDob(rs.getString("PERSON_DOB"));
                psk.setPersonGender(Integer.toString(rs
                        .getInt("FK_CODELST_GENDER")));
                psk.setPersonStatus(Integer.toString(rs
                        .getInt("FK_CODELST_PSTAT")));
                // psk.setPersonRegBy(rs.getString("Regby_Name"));
                psk.setPersonFname(rs.getString("PERSON_FNAME"));
                psk.setPersonLname(rs.getString("PERSON_LNAME"));
                psk.setPersonMname(rs.getString("PERSON_MNAME"));

                setPatient(psk);
                psk = new PersonBean();
            }
            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("patient",
                    "PATIENT DAO.searchPatients EXCEPTION IN FETCHING FROM PATIENT table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }

    // ///////////////////////////////////////////////////////////////////////

    /**
     * This method gets access right for complete patient data depending on
     * studies the patient is enrolled in and user's group <br>
     * parameter
     *
     * @param userId
     * @param groupId
     * @param patientId
     */
    public int getPatientCompleteDetailsAccessRight(int userId, int groupId,
            int patientId) {

        int retRight = 0;
        CallableStatement cstmt = null;
        Connection conn = null;

        try {
            conn = EnvUtil.getConnection();

            cstmt = conn
                    .prepareCall("{call PKG_STUDYSTAT.SP_GET_PATIENT_RIGHT(?,?,?,?)}");
            cstmt.setInt(1, userId);
            cstmt.setInt(2, groupId);
            cstmt.setInt(3, patientId);

            cstmt.registerOutParameter(4, java.sql.Types.INTEGER);

            cstmt.execute();
            retRight = cstmt.getInt(4);
            return retRight;

        } catch (Exception e) {
            Rlog
                    .fatal(
                            "patient",
                            "EXCEPTION in getPatientCompleteDetailsAccessRight, excecuting Stored Procedure PKG_STUDYSTAT.SP_GET_PATIENT_RIGHT"
                                    + e);
            return 0;
        } finally {
            try {
                if (cstmt != null)
                    cstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }
    }
    public boolean patientCheck(String fname,String lname,String date,String accountId,String patId) {
        boolean patExists = false;
       PreparedStatement pstmt = null;

        Connection conn = null;
        int count=0;
        patId=(patId==null)?"":patId;

        try {
        	conn=EnvUtil.getConnection();

            String sql="select count(*) count "
            + "from epat.person "
            + "where lower(trim(person_fname)) = lower(?) and lower(trim(person_lname)) = lower(?) "
            +" and person_dob=to_date (?, PKG_DATEUTIL.F_GET_DATEFORMAT) "
            + "and FK_account in (" + accountId + ")";
            if (patId.length()>0)
            	sql=sql+" and upper ( trim (person_code))<> upper( trim ('"+patId+"'))";

            pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, fname);
            pstmt.setString(2, lname);
            pstmt.setString(3, date);
 

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            count = rs.getInt("count");
            if (count > 0) {
                patExists = true;
            }
        } catch (SQLException ex) {
            Rlog.fatal("person", "PatientDao.patientCodeExists " + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return patExists;

    }
    public String CreateOrgTree(String siteId) {

        PreparedStatement pstmtSite = null;
        Connection conn = null;
        int count=0;
        try {
            conn = EnvUtil.getConnection();
            pstmtSite = conn.prepareStatement("Select  Pkg_Util. f_get_child_parent_org("+ siteId + ") as childs FROM  dual");
            ResultSet rsSite = pstmtSite.executeQuery();

            rsSite.next();
            String countSite = rsSite.getString ("childs");


            try {
                if (pstmtSite != null)
                	pstmtSite.close();
            } catch (Exception e) {
            }try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        return countSite;
    }catch(SQLException e){
    	e.printStackTrace();
    	return "";
    }
    }

    public int getViewPHIRight(int  user,int group) {

       PreparedStatement pstmt = null;
       int right = 0;
        Connection conn = null;



        try {
        	conn=EnvUtil.getConnection();

            String sql=" select pkg_studystat.f_get_viewPHI(?, ?) right from dual ";

              pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, user);
            pstmt.setInt(2, group);

            ResultSet rs = pstmt.executeQuery();

            rs.next();
            right = rs.getInt("right");

        } catch (SQLException ex) {
            Rlog.fatal("person", "PatientDao.getViewPHIRight" + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }
        return right;

    }
    
  //AK: Added method for Autocomplete filters
    public void getPatientsRows(String userID ,String actId) {
        PreparedStatement stmt = null;
        ResultSet rs;
        String sql="";
        Connection conn = null;
  	      try {
  	          conn = getConnection();
  	          sql="select pk_person, person_code from ERV_PERSON_COMPLETE where fk_account = ? and EXISTS"+
  	         "(select * from ER_USERSITE usr,ER_PATFACILITY fac where fk_user=? and usersite_right >=4"+
  	          "and usr.fk_site = fac.fk_site and fac.patfacility_accessright > 0  and fk_per=pk_person)";
  	          stmt = conn.prepareStatement(sql);
  	          stmt.setString(1, actId);
  	          stmt.setString(2, userID);         
  	          rs = stmt.executeQuery();    
  			  while(rs.next())
  			  {				  
  	            	setPatId(rs.getString(1));
  	            	setPatName(rs.getString(2));	 				  
  			  }
  	      }
        catch (SQLException ex) {
            Rlog.fatal("common",
                    "Patient:Exception in calling getPatientsRows :forAutoComplete Search "
                            + ex);
        } finally {
            try {
                if (stmt != null)
              	  stmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }

        }

    }
   


    // end of class
}
