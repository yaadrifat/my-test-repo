/*
 * Classname			AccountBean
 *
 * Version information : 1.0
 *
 * Date					02/20/2001
 *
 * Copyright notice : Velos Inc
 */

package com.velos.eres.business.account.impl;

/* IMPORT STATEMENTS */

import java.io.Serializable;
import java.util.Date;

import javax.ejb.EntityContext;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.DateUtil;
import com.velos.eres.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

/* END OF IMPORT STATEMENTS */

/**
 * The Account CMP entity bean.<br>
 * <br>
 *
 * @author Sajal
 *
 * @version 1.0, 02/20/2001
 *
 * @ejbHome AccountHome
 *
 * @ejbRemote AccountRObj
 */
@Entity
@Table(name = "er_account")
public class AccountBean implements Serializable {

    /*
     * CLASS METHODS
     *
     * public int getAccId() public String getAccType() public void
     * setAccType(String accountType) public String getAccRoleType() public void
     * setAccRoleType(String accountRoleType) public String getAccName() public
     * void setAccName(String accountName) public String getAccStartDate()
     * public void setAccStartDate(String accountStartDate) public String
     * getAccEndDate() public void setAccEndDate(String accountEndDate) public
     * String getAccStatus() public void setAccStatus(String accountStatus)
     * public String getAccMailFlag() public void setAccMailFlag(String
     * accountMailFlag) public String getAccPubFlag() public void
     * setAccPubFlag(String accountPubFlag) public String getAccNote() public
     * void setAccNote(String accountNote) public String getAccCreator() public
     * void setAccCreator(String accountCreator) public String getAccLogo()
     * public void setAccLogo(String accountLogo) public AccountPK
     * ejbCreate(AccountStateKeeper ask) public void
     * ejbPostCreate(AccountStateKeeper ask) public AccountStateKeeper
     * getAccountStateKeeper() public void
     * setAccountStateKeeper(AccountStateKeeper ask) public int
     * updateAccount(AccountStateKeeper ask) public void ejbActivate() public
     * void ejbLoad() public void ejbStore() public void ejbPassivate() public
     * void ejbRemove() public void setEntityContext(EntityContext ctx) public
     * void unsetEntityContext()
     *
     * END OF CLASS METHODS
     */

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /*
     * the account Id
     */
    public Integer accId;

    /*
     * the account type
     */
    public String accType;

    /*
     * the account role type
     */
    public Integer accRoleType;

    /*
     * the account name
     */
    public String accName;

    /*
     * the account start date
     */
    public Date accStartDate = null;

    /*
     * the account end date
     */
    public Date accEndDate = null;

    /*
     * the account status
     */
    public String accStatus;

    /*
     * the account mail flag
     */
    public String accMailFlag;

    /*
     * the account public flag
     */
    public String accPubFlag;

    /*
     * the account note
     */
    public String accNote;

    /*
     * the account creator
     */
    public Integer accCreator;

    /*
     * the account logo
     */
    public String accLogo;

    /*
     * the record creator
     */
    public Integer accMaxUsr;

    /*
     * the record creator
     */
    public Integer accMaxPortal;//JM:


    /*
     * the record creator
     */
    public Integer accMaxStorage;

    /*
     * the account module Right
     */
    public String accModRight;

    /*
     * the record creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * the IP Address
     */
    public String ipAdd;

    public String ldapEnabled;

    public String wsEnabled;

    public Integer accSkin;

    public EntityContext context;

    public Integer autoGenStudy;//KM

    public Integer autoGenPatient;

    // GETTER SETTER METHODS

    /**
     * @return Account Id
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_ER_ACCOUNT", allocationSize=1)
    @Column(name = "PK_Account")
    public Integer getAccId() {
        return this.accId;
    }

    public void setAccId(Integer accId) {
        this.accId = accId;
    }

    /**
     * @return Account Type
     */
    @Column(name = "ac_type")
    public String getAccType() {
        return this.accType;
    }

    /**
     * @param accType
     *            The value that is required to be registered as Account Type
     */
    public void setAccType(String accountType) {
        this.accType = accountType;
    }

    /**
     *
     *
     * @return Account Role Type
     */
    @Transient
    public String getAccRoleType() {
        return StringUtil.integerToString(this.accRoleType);
    }

    /**
     *
     *
     * @param accRoleType
     *            Account Role Type
     */
    public void setAccRoleType(String accountRoleType) {
        if (accountRoleType != null && accountRoleType.trim().length() > 0) {
            this.accRoleType = Integer.valueOf(accountRoleType);
        }
    }

    /**
     * @return Name of the Account
     */
    @Column(name = "ac_name")
    public String getAccName() {
        return this.accName;
    }

    /**
     * @param accName
     *            The value that is required to be registered as Account Name
     */
    public void setAccName(String accountName) {
        this.accName = accountName;
    }

    /**
     * @return Account Start Date
     */
    @Column(name = "ac_strtdt")
    public Date getAccStartDatePersistent() {

        return this.accStartDate;
    }

    @Transient
    public String getAccStartDate() {
        return DateUtil.dateToString(getAccStartDatePersistent());
    }

    /**
     * @param accStartDate
     *            The value that is required to be registered as Account Start
     *            Date
     */
    public void setAccStartDatePersistent(Date accountStartDate) {

        this.accStartDate = accountStartDate;

    }

    /**
     * @param accStartDate
     *            The value that is required to be registered as Account Start
     *            Date
     */
    public void setAccStartDate(String accountStartDate) {

        setAccStartDatePersistent(DateUtil.stringToDate(accountStartDate,null));

    }

    /**
	 * @return the ldapEnabled
	 */
    @Column(name = "ac_enableldap")
	public String getLdapEnabled() {
		return ldapEnabled;
	}

	/**
	 * @param ldapEnabled the ldapEnabled to set
	 */
	public void setLdapEnabled(String ldapEnabled) {
		this.ldapEnabled = ldapEnabled;
	}

    /**
	 * @return the wsEnabled
	 */
    @Column(name = "ac_enablews")
	public String getWSEnabled() {
		return wsEnabled;
	}

	/**
	 * @param wsEnabled the ldapEnabled to set
	 */
	public void setWSEnabled(String wsEnabled) {
		this.wsEnabled = wsEnabled;
	}

	   /**
	 * @return the accSkin
	 */
    @Column(name = "fk_codelst_skin")
	public String getAccSkin() {
		return StringUtil.integerToString(accSkin);
	}

	/**
	 * @param ldapEnabled the ldapEnabled to set
	 */
	public void setAccSkin(String accSkin) {
		if ( accSkin != null){
		this.accSkin =  StringUtil.stringToNum(accSkin);
		}
	}

	//Added by Manimaran for the Enh#GL7

	/**
	 * @return the autoGenStudy
	 */
    @Column(name = "ac_autogen_study")
	public String getAutoGenStudy() {
		return StringUtil.integerToString(autoGenStudy);
	}

	/**
	 * @param autoGenStudy the autoGenStudy to set
	 */
	public void setAutoGenStudy(String autoGenStudy) {
		if ( autoGenStudy != null){
		this.autoGenStudy =  StringUtil.stringToNum(autoGenStudy);
		}
	}


	/**
	 * @return the autoGenPatient
	 */
    @Column(name = "ac_autogen_patient")
	public String getAutoGenPatient() {
		return StringUtil.integerToString(autoGenPatient);
	}

	/**
	 * @param autoGenPatient the autoGenPatient to set
	 */
	public void setAutoGenPatient(String autoGenPatient) {
		if ( autoGenPatient != null){
		this.autoGenPatient =  StringUtil.stringToNum(autoGenPatient);
		}
	}



	public AccountBean(Integer accId, String accType, String accRoleType,
            String accName, String accStartDate, String accEndDate,
            String accStatus, String accMailFlag, String accPubFlag,
            String accNote, String accCreator, String accLogo,
            String accMaxUsr, String accMaxPortal, String accMaxStorage, String accModRight,
            String creator, String modifiedBy, String ipAdd,String ldapEnabled,String wsEnabled ,
            String accSkin, String autoGenStudy, String autoGenPatient ) { //KM

        setAccId(accId);
        setAccType(accType);
        setAccRoleType(accRoleType);
        setAccName(accName);
        setAccStartDate(accStartDate);
        setAccEndDate(accEndDate);
        setAccStatus(accStatus);
        setAccMailFlag(accMailFlag);
        setAccPubFlag(accPubFlag);
        setAccNote(accNote);
        setAccCreator(accCreator);
        setAccLogo(accLogo);
        setAccMaxUsr(accMaxUsr);
        setAccMaxPortal(accMaxPortal);
        setAccMaxStorage(accMaxStorage);
        setAccModRight(accModRight);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setLdapEnabled(ldapEnabled);
        setWSEnabled(wsEnabled);
        setAccSkin(accSkin);
        setAutoGenStudy(autoGenStudy);
        setAutoGenPatient(autoGenPatient);
    }

    public AccountBean() {
    }

    /**
     * @return Account End Date
     */
    @Column(name = "ac_endt")
    public Date getAccEndDatePersistent() {
        return this.accEndDate;
    }

    /**
     * @return Account End Date
     */

    public String getAccEndDate() {
        return DateUtil.dateToString(getAccEndDatePersistent());
    }

    /**
     * @param accEndDate
     *            The value that is required to be registered as Account End
     *            Date
     */
    public void setAccEndDatePersistent(Date accountEndDate) {
        this.accEndDate = accountEndDate;
    }

    /**
     * @param accEndDate
     *            The value that is required to be registered as Account End
     *            Date
     */
    public void setAccEndDate(String accountEndDate) {
        DateUtil sDate = null;
        setAccEndDatePersistent(DateUtil.stringToDate(accountEndDate,null));
    }

    /**
     * @return Account Status
     */
    @Column(name = "ac_stat")
    public String getAccStatus() {
        return this.accStatus;
    }

    /**
     * @param accStatus
     *            The value that is required to be registered as Account Status
     */
    public void setAccStatus(String accountStatus) {
        this.accStatus = accountStatus;
    }

    /**
     * @return Value of Account Mail Flag
     */
    @Column(name = "ac_mailflag")
    public String getAccMailFlag() {
        return this.accMailFlag;
    }

    /**
     * @param accMailFlag
     *            The value that is required to be registered as Account Mail
     *            Flag
     */
    public void setAccMailFlag(String accountMailFlag) {
        this.accMailFlag = accountMailFlag;
    }

    /**
     * @return Value of Account Public Flag
     */
    @Column(name = "ac_pubflag")
    public String getAccPubFlag() {
        return this.accPubFlag;
    }

    /**
     * @param accPubFlag
     *            The value that is required to be registered as Account Public
     *            Flag
     */
    public void setAccPubFlag(String accountPubFlag) {
        this.accPubFlag = accountPubFlag;
    }

    /**
     * @return Account Note
     */
    @Column(name = "ac_note")
    public String getAccNote() {
        return this.accNote;
    }

    /**
     * @param accNote
     *            The value that is required to be registered as Account Note
     */
    public void setAccNote(String accountNote) {
        this.accNote = accountNote;
    }

    /**
     * @return The Id of the Account Creator
     */
    @Column(name = "ac_usrcreator")
    public String getAccCreator() {
        return StringUtil.integerToString(this.accCreator);
    }

    /**
     * @param accCreator
     *            The User Id that is required to be registered as Account
     *            Creator
     */
    public void setAccCreator(String accountCreator) {
        if (accountCreator != null) {
            this.accCreator = Integer.valueOf(accountCreator);
        }
    }

    /**
     * @return Account Logo
     */
    @Column(name = "ac_logo")
    public String getAccLogo() {
        return this.accLogo;
    }

    /**
     * @param accLogo
     *            The value that is required to be registered as Account Logo
     */
    public void setAccLogo(String accountLogo) {
        this.accLogo = accountLogo;
    }

    /**
     * @return The Max number of permissible users in this account
     */
    @Column(name = "AC_MAXUSR")
    public String getAccMaxUsr() {
        return StringUtil.integerToString(this.accMaxUsr);
    }

    /**
     * @param accMaxUsr
     *            The Max number of permissible users in this account
     */
    public void setAccMaxUsr(String accMaxUsr) {
        if (accMaxUsr != null) {
            this.accMaxUsr = Integer.valueOf(accMaxUsr);
        }
    }


    //JM:

    /**
     * @return The Max number of permissible portals in this account
     */
    @Column(name = "MAX_PORTALS")
    public String getAccMaxPortal() {
        return StringUtil.integerToString(this.accMaxPortal);
    }

    /**
     * @param accMaxUsr
     *            The Max number of permissible portals in this account
     */
    public void setAccMaxPortal(String accMaxPortals) {
        if (accMaxPortals != null) {
            this.accMaxPortal = Integer.valueOf(accMaxPortals);
        }
    }



    /**
     * @return The Max space in MB permissible for this account for files
     *         uploading
     */
    @Column(name = "AC_MAXSTORAGE")
    public String getAccMaxStorage() {
        return StringUtil.integerToString(this.accMaxStorage);
    }

    /**
     * @param accMaxStorage
     *            The Max space in MB permissible for this account for files
     *            uploading
     */
    public void setAccMaxStorage(String accMaxStorage) {
        if (accMaxStorage != null) {
            this.accMaxStorage = Integer.valueOf(accMaxStorage);
        }
    }

    /**
     * @return Account Module Right
     */
    @Column(name = "AC_MODRIGHT")
    public String getAccModRight() {
        return this.accModRight;
    }

    /**
     * @param accModRight
     *            The value that is required to be registered as Account Module
     *            Right
     */
    public void setAccModRight(String accountModRight) {
        this.accModRight = accountModRight;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Retrieves the details of an account in AccountStateKeeper Object
     *
     * @return AccountStateKeeper consisting of the account details
     *
     * @see AccountStateKeeper
     */
    @Transient
    /*
     * public AccountStateKeeper getAccountStateKeeper() { Rlog.debug("account",
     * "AccountBean.getAccountStateKeeper getAccId() " + getAccId());
     * Rlog.debug("account", "AccountBean.getAccountStateKeeper getAccType() " +
     * getAccType()); Rlog.debug("account", "AccountBean.getAccountStateKeeper
     * getAccRoleType() " + getAccRoleType()); Rlog.debug("account",
     * "AccountBean.getAccountStateKeeper getAccName() " + getAccName());
     * Rlog.debug("account", "AccountBean.getAccountStateKeeper
     * getAccStartDate() " + getAccStartDate()); Rlog.debug("account",
     * "AccountBean.getAccountStateKeeper getAccStatus() " + getAccStatus());
     * Rlog.debug("account", "AccountBean.getAccountStateKeeper getAccStatus() " +
     * getAccStatus()); Rlog.debug("account", "AccountBean.getAccountStateKeeper
     * getAccMailFlag() " + getAccMailFlag()); Rlog.debug("account",
     * "AccountBean.getAccountStateKeeper getAccPubFlag() " + getAccPubFlag());
     * Rlog.debug("account", "AccountBean.getAccountStateKeeper getAccNote() " +
     * getAccNote()); Rlog.debug("account", "AccountBean.getAccountStateKeeper
     * getAccMaxUsr() " + getAccMaxUsr()); Rlog.debug("account",
     * "AccountBean.getAccountStateKeeper getAccMaxStorage() " +
     * getAccMaxStorage()); Rlog.debug("account",
     * "AccountBean.getAccountStateKeeper getAccCreator() " + getAccCreator());
     * Rlog.debug("account", "AccountBean.getAccountStateKeeper getAccLogo() " +
     * getAccLogo()); return (new AccountStateKeeper(getAccId(), getAccType(),
     * getAccRoleType(), getAccName(), getAccStartDate(), getAccEndDate(),
     * getAccStatus(), getAccMailFlag(), getAccPubFlag(), getAccNote(),
     * getAccCreator(), getAccLogo(), getAccMaxUsr(), getAccMaxStorage(),
     * getAccModRight(), getCreator(), getModifiedBy(), getIpAdd())); }
     */
    /**
     * Sets up a state keeper containing details of the account
     */
    /*
     * public void setAccountStateKeeper(AccountStateKeeper ask) { GenerateId
     * accountId = null; try { // id =
     * genId.getId("SEQ_ER_STUDY",getConnection()); Connection conn = null; //
     * Class.forName("oracle.jdbc.driver.OracleDriver"); // conn //
     * =DriverManager.getConnection("jdbc:oracle:thin:@192.8.8.44:1521:evelos","er","er");
     * setAccType(ask.getAccType()); Rlog.debug("account",
     * "AccountBean.setAccountStateKeeper() accType :" + accType);
     * setAccRoleType(ask.getAccRoleType()); Rlog.debug("account",
     * "AccountBean.setAccountStateKeeper() accRoleType :" + accRoleType);
     * setAccName(ask.getAccName()); Rlog.debug("account",
     * "AccountBean.setAccountStateKeeper() accName :" + accName);
     * setAccStartDate(ask.getAccStartDate()); Rlog.debug("account",
     * "AccountBean.setAccountStateKeeper() accStartDate :" + accStartDate);
     * setAccEndDate(ask.getAccEndDate()); Rlog.debug("account",
     * "AccountBean.setAccountStateKeeper() accEndDate :" + accEndDate);
     * setAccStatus(ask.getAccStatus()); Rlog.debug("account",
     * "AccountBean.setAccountStateKeeper() accStatus :" + accStatus);
     * setAccMailFlag(ask.getAccMailFlag()); Rlog.debug("account",
     * "AccountBean.setAccountStateKeeper() accMailFlag :" + accMailFlag);
     * setAccPubFlag(ask.getAccPubFlag()); Rlog.debug("account",
     * "AccountBean.setAccountStateKeeper() accPubFlag :" + accPubFlag);
     * setAccNote(ask.getAccNote()); Rlog.debug("account",
     * "AccountBean.setAccountStateKeeper() accNote :" + accNote);
     * setAccCreator(ask.getAccCreator()); Rlog.debug("account",
     * "AccountBean.setAccountStateKeeper() accCreator :" + accCreator);
     * setAccLogo(ask.getAccLogo()); Rlog.debug("account",
     * "AccountBean.setAccountStateKeeper() accLogo :" + accLogo);
     * setAccMaxUsr(ask.getAccMaxUsr());
     * setAccMaxStorage(ask.getAccMaxStorage());
     * setAccModRight(ask.getAccModRight()); setCreator(ask.getCreator());
     * setModifiedBy(ask.getModifiedBy()); setIpAdd(ask.getIpAdd()); } catch
     * (Exception e) { Rlog.fatal("account", "Error in setAccountStateKeeper()
     * in AccountBean " + e); } }
     */
    /**
     * Saves the account details in the AccountStateKeeper to the database and
     * returns 0 if the update is successful and -2 if it fails
     *
     * @param ask
     *            Account State Keeper with the updated Account Information
     *
     * @return 0 - if update is successful <br>
     *         -2 - if update fails
     *
     * @see AccountStateKeeper
     *
     */
    public int updateAccount(AccountBean ask) {
        try {
            setAccType(ask.getAccType());
            setAccRoleType(ask.getAccRoleType());
            setAccName(ask.getAccName());
            setAccStartDate(ask.getAccStartDate());
            setAccEndDate(ask.getAccEndDate());
            setAccStatus(ask.getAccStatus());
            setAccMailFlag(ask.getAccMailFlag());
            setAccPubFlag(ask.getAccPubFlag());
            setAccNote(ask.getAccNote());
            setAccCreator(ask.getAccCreator());
            setAccLogo(ask.getAccLogo());
            setAccMaxUsr(ask.getAccMaxUsr());
            setAccMaxPortal(ask.getAccMaxPortal());//JM:
            setAccMaxStorage(ask.getAccMaxStorage());
            setAccModRight(ask.getAccModRight());
            setCreator(ask.getCreator());
            setModifiedBy(ask.getModifiedBy());
            setIpAdd(ask.getIpAdd());
           this.setLdapEnabled(ask.getLdapEnabled());
           this.setWSEnabled(ask.getWSEnabled());
           setAutoGenStudy(ask.getAutoGenStudy());//KM
           setAutoGenPatient(ask.getAutoGenPatient());
            Rlog.debug("account", "AccountBean.updateAccount");
            return 0;
        } catch (Exception e) {
            Rlog.fatal("account", " error in AccountBean.updateAccount");
            return -2;
        }
    }

}// end of class
