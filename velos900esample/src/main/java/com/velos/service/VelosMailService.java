package com.velos.service; 
 

import com.velos.eresmailer.MailerTask;
import com.velos.eresmailer.eresDispatcher;
import org.jboss.system.ServiceMBeanSupport;

public class VelosMailService extends ServiceMBeanSupport implements VelosMailServiceMBean { 

 
    
  public void init() throws Exception { } 

  public void startService() throws Exception {
  //mailertask introduced,apr-08-2011
	  MailerTask.startMailer();
      eresDispatcher.startDispatcher();       
  } 
  public void stopService() throws Exception { 
      eresDispatcher.stopDispatcher();
      MailerTask.stopMailer();
 
  } 
   
  } 