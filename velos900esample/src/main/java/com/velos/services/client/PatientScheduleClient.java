package com.velos.services.client;

import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.velos.services.OperationException;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.Visit;
import com.velos.services.patientschedule.PatientScheduleService;
/***
 * Client Class/Remote object for Patient Schedule Services 
 * @author Virendra
 *
 */
public class PatientScheduleClient{
	/**
	 * Invokes the remote object.
	 * @return remote PtientDemo service
	 * @throws OperationException
	 */
	
	private static PatientScheduleService getPatScheduleRemote()
	throws OperationException{
		
	PatientScheduleService PatScheduleRemote = null;
	InitialContext ic;
	try{
		ic = new InitialContext();
		PatScheduleRemote =
			(PatientScheduleService) ic.lookup(PatientScheduleService.class.getName());
		}
		catch(NamingException e){
			throw new OperationException(e);
		}
		return PatScheduleRemote;
	}
	/**
	 * Calls getPatientSchedule on getPatientSchedule Remote object
	 * to return PatientSchedule 
	 * @param patientId
	 * @param studyIdentifier
	 * @return PatSchedule
	 * @throws OperationException
	 */
	public static ArrayList<PatientSchedule> getPatientSchedules(PatientIdentifier patientId, StudyIdentifier studyIdentifier) 
	throws OperationException{
		PatientScheduleService patScheduleService = getPatScheduleRemote();
		return patScheduleService.getPatientSchedules(patientId, studyIdentifier);
	}
}