
package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Data transfer object representing a study status.
 * @author dylan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyStatuses")
public class StudyStatuses
    extends ServiceObject
{

    /**
     * 
     */
    private static final long serialVersionUID = -8643891583943218535L;
    /**
     * Generated serialVersionUID
     */
 
    protected List<StudyStatus> studyStatus;
    
    @NotNull 
    @Size(min=1)
    @Valid
	public List<StudyStatus> getStudyStatus() {
        return studyStatus;
    }

    public void setStudyStatus(List<StudyStatus> studyStatus) {
        this.studyStatus = studyStatus;
    }
    
    public StudyIdentifier getParentIdentifier() {
        if (super.parentIdentifier == null || 
                super.parentIdentifier.getId().size() < 1) { return null; }
        for (SimpleIdentifier myId : super.parentIdentifier.getId()) {
            if (myId instanceof StudyIdentifier) {
                return (StudyIdentifier)myId;
            }
        }
        return null;
    }
    
    public void setParentIdentifier(StudyIdentifier studyIdentifer) {
        List<SimpleIdentifier> idList = new ArrayList<SimpleIdentifier>();
        idList.add(studyIdentifer);
        super.parentIdentifier = new ParentIdentifier();
		super.parentIdentifier.setId(idList);
	}
    
}
