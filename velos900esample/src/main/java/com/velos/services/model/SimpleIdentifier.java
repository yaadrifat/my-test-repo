/**
 * 
 */
package com.velos.services.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Marker interface for classes for all objects that provide unique identification for 
 * objects in the service framework.
 * @author dylan
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="SimpleIdentifier")
public class SimpleIdentifier implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2625074652777863144L;
	/**
	 * 
	 */
	
	protected String OID;
	
	public SimpleIdentifier(){
		
	}
	
	public SimpleIdentifier(String OID) {
		super();
		this.OID = OID;
	}
	
	public String getOID() {
		return OID;
	}

	public void setOID(String OID) {
		this.OID = OID;
	}

	
	
	
}
