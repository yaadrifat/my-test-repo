
package com.velos.services.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="StudyTeamMember")
public class StudyTeamMember
    extends ServiceObject
{ 
	
	/**
     * 
     */
    private static final long serialVersionUID = -7468328089497338166L;

    /**
	 * @deprecated Field should not be used.
	 * @author dylan
	 *
	 */
	public enum UserType{
		DEFAULT
	}
	
	/**
	 * @deprecated Field should not be used.
	 * @author dylan
	 *
	 */
	protected UserType userType;
	


	protected Code status; 
	
    protected UserIdentifier userId;


    protected Code teamRole;

    public StudyTeamMember(){
    	
    }
    
    @NotNull
    public UserIdentifier getUserId() {
		return userId;
	}

	public void setUserId(UserIdentifier userId) {
		this.userId = userId;
	}
	
	/**
	 * 
	 * @deprecated This field will be removed in future version. eResearch supports user type
	 * through group settings, not at the study team level. ONLY USE {@link UserType#DEFAULT}
	 * @return
	 */
	@NotNull
	public UserType getUserType() {
		return userType;
	}

	/**
	 * 
	 * @deprecated This field will be removed in future version. For now, ONLY USE {@link UserType#DEFAULT}
	 * @return
	 */
	public void setUserType(UserType userType) {
		this.userType = userType;
	}
  
	@NotNull
	@Valid
	public Code getStatus() {
		return status;
	}

	public void setStatus(Code status) {
		this.status = status;
	}

	@NotNull
	@Valid
	public Code getTeamRole() {
		return teamRole;
	}
	
	
	public void setTeamRole(Code teamRole) {
		this.teamRole = teamRole;
	}
	//virendra;Fixed#6121, overloading setParentId for studyIdentifier
    public StudyIdentifier getParentIdentifier() {
        if (super.parentIdentifier == null || 
                super.parentIdentifier.getId().size() < 1) { return null; }
        for (SimpleIdentifier myId : super.parentIdentifier.getId()) {
            if (myId instanceof StudyIdentifier) {
                return (StudyIdentifier)myId;
            }
        }
        return null;
    }
    
    public void setParentIdentifier(StudyIdentifier studyIdentifer) {
        List<SimpleIdentifier> idList = new ArrayList<SimpleIdentifier>();
        idList.add(studyIdentifer);
        super.parentIdentifier = new ParentIdentifier();
        super.parentIdentifier.setId(idList);
    }

}
