package com.velos.services.model;

import java.io.Serializable;
import java.util.Date;

public class EventStatus implements Serializable{

	/**
	 * Class for EventStatus which represents EventStatus 
	 * of an event in eResearch
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected String eventName;
	protected Code eventStatusCode;
	protected Date statusValidFrom;
	protected OrganizationIdentifier siteOfService;
	//Virendra: Fixed bug#6118
	protected Code coverageType;
	protected String reasonForChangeCoverType; 
	protected String notes;
	
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public Code getEventStatusCode() {
		return eventStatusCode;
	}
	public void setEventStatusCode(Code eventStatusCode) {
		this.eventStatusCode = eventStatusCode;
	}
	public Date getStatusValidFrom() {
		return statusValidFrom;
	}
	public void setStatusValidFrom(Date statusValidFrom) {
		this.statusValidFrom = statusValidFrom;
	}
	public OrganizationIdentifier getSiteOfService() {
		return siteOfService;
	}
	public void setSiteOfService(OrganizationIdentifier siteOfService) {
		this.siteOfService = siteOfService;
	}
	public Code getCoverageType() {
		return coverageType;
	}
	public void setCoverageType(Code coverageType) {
		this.coverageType = coverageType;
	}
	public String getReasonForChangeCoverType() {
		return reasonForChangeCoverType;
	}
	public void setReasonForChangeCoverType(String reasonForChangeCoverType) {
		this.reasonForChangeCoverType = reasonForChangeCoverType;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	
}