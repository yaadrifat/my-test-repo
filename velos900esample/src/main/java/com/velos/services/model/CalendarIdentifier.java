/**
 * 
 */
package com.velos.services.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class builds identifies a unique instance of a calendar.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="CalendarIdentifier")

public class CalendarIdentifier extends SimpleIdentifier {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7546528070255374263L;
	//Virendra
	public CalendarIdentifier() {
	}
	
	public CalendarIdentifier(String OID) {
		this.OID = OID;
	}
}
