package com.velos.services.patientdemographics;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.CommonDAO;
import com.velos.services.Issue;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;

public class PatientDemographicsDAO extends CommonDAO{
	
	/**
	 * DataAccess object for PatientDemographics Services 
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(PatientDemographicsDAO.class);
	//Virendra:#6020 added input param fk_account
	private static String patDemoGetPersonPKSql="Select pk_person from person where person_code= ? and fk_account= ?";
	protected static ResponseHolder response = new ResponseHolder();
	Issue issue = new Issue();
	
/**
 * public static method with params as PatientId and userAccountID returns
 * Primary key personPk of Person table.
 * @param patientId
 * @param userAccountId
 * @return personPK
 * @throws OperationException
 */
public static Integer getPersonPKFromPersonCode(String patientId, int userAccountId) throws OperationException {
		
		PreparedStatement pstmt = null;
		Connection conn = null;
		Integer personPK = 0;
		try{
			
			conn = getConnection();

			if (logger.isDebugEnabled()) logger.debug(" sql:" + patDemoGetPersonPKSql);
			
			pstmt = conn.prepareStatement(patDemoGetPersonPKSql);
			pstmt.setString(1, patientId);
			pstmt.setInt(2, userAccountId);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				personPK = rs.getInt("PK_PERSON");
			}
			
		}
		catch(Throwable t){
	
			t.printStackTrace();
			
			throw new OperationException();
			
		}
		finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}

		}
		return personPK;

	}
	
	
	
}