/**
 * 
 */
package com.velos.services;

/**
 * @author dylan
 *
 */
public enum IssueTypes { 
	  /**
	   * Multiple objects were found for a request that expected a single object.
	  */
	  MULTIPLE_OBJECTS_FOUND("MULTIPLE_OBJECTS_FOUND", "Multiple objects were found for a request that expected a single object."),
	  
	  /**
	   * A code was not found in the database
	   */
	  CODE_NOT_FOUND("CODE_NOT_FOUND", "A code was not found in the database"),
	  
	  /**
	   * Data validation problem.
	   */
	  DATA_VALIDATION("DATA_VALIDATION", "Data validation problem."),
	  
	  /**
	   * This user already exists.
	   */
	  DUPLICATE_USER("DUPLICATE_USER", "This user already exists."),
	  
	  /**
	   * Study author not found
	   */
	  STUDY_AUTHOR_NOT_FOUND("STUDY_AUTHOR_NOT_FOUND", "Study author not found"),
	  
	  /**
	   * Study Principal Investigator not found
	   */
	  STUDY_PRINCIPAL_INVESTIGATOR_NOT_FOUND("STUDY_PRINCIPAL_INVESTIGATOR_NOT_FOUND", "Study Principal Investigator not found"),
	  
	  /**
	   * Study Coordinator not found
	   */
	  STUDY_COORDINATOR_NOT_FOUND("STUDY_COORDINATOR_NOT_FOUND", "Study Coordinator not found"),
	  
	  /**
	   * The study number already exists
	   */
	  STUDY_NUMBER_EXISTS("STUDY_NUMBER_EXISTS", "The study number already exists"),
	  
	  /**
	   * The Study could not be found
	   */
	  STUDY_NOT_FOUND("STUDY_NOT_FOUND", "The Study could not be found"),
	  
	  /**
	   * The Study Organization could not be found
	   */
	  STUDY_ORG_NOT_FOUND("STUDY_ORG_NOT_FOUND", "The Study Organization could not be found"),
	  
	  /**
	   * Error occurred removing a study organization
	   */
	  STUDY_ERROR_REMOVE_ORGANIZATION("STUDY_ERROR_REMOVE_ORGANIZATION", "An error occured removing a study organization"),
	  
	  /**
	   * Error occurred removing a study organization
	   */
	  STUDY_ERROR_REMOVE_ORGANIZATION_CURRENT_STUDYSTATUS("STUDY_ERROR_REMOVE_ORGANIZATION_CURRENT_STUDYSTATUS","An error occured removing a study organization"),
	  
	  /**
	   * Error occurred removing a study organization
	   */
	  STUDY_ERROR_REMOVE_ORGANIZATION_ENROLLED_PATIENT("STUDY_ERROR_REMOVE_ORGANIZATION_ENROLLED_PATIENT","An error occured removing a study organization"),
	  
	  /**
	   * Error occurred creating a study organization
	   */
	  STUDY_ERROR_CREATE_ORGANIZATION("STUDY_ERROR_CREATE_ORGANIZATION", "An error occured creating a study organization"),
	  
	  /**
	   * Error occurred updating a study organization
	   */
	  STUDY_ERROR_UPDATE_ORGANIZATION("STUDY_ERROR_UPDATE_ORGANIZATION", "An  error occured updating a study organization"),
	  
	  /**
	   * Error occurred creating a study status.
	   */
	  STUDY_ERROR_CREATE_STATUS("STUDY_ERROR_CREATE_STATUS", "Error occured creating a study status."),
	  
	  /**
	   * Error occurred updating a study status.
	   */
	  STUDY_ERROR_UPDATE_STATUS("STUDY_ERROR_UPDATE_STATUS", "Error occured updating a study status."),
	  
	  /**
	   * Error occurred creating a study team member.
	   */
	  STUDY_ERROR_CREATE_TEAM_MEMBER("STUDY_ERROR_CREATE_TEAM_MEMBER", "Error occured creating a study team member."),
	  
	  /**
	   * Error occurred updating a study team member.
	   */
	  STUDY_ERROR_UPDATE_TEAM_MEMBER("STUDY_ERROR_UPDATE_TEAM_MEMBER", "Error occured updating a study team member."),
	  
	  /**
	   * Error occurred removing a study team member.
	   */
	  STUDY_ERROR_REMOVE_TEAM_MEMBER("STUDY_ERROR_REMOVE_TEAM_MEMBER", "Error occured removing a study team member."),
	  
	  /**
	   * Data Manager can not be removed from Study Team.
	   */
	  STUDY_REMOVE_DATA_MANAGER("STUDY_REMOVE_DATA_MANAGER", "Data Manager can not be removed from Study Team."),
	  
	  /**
	   * Error occurred creating a More Study Details value.
	   */
	  STUDY_ERROR_CREATE_MSD("STUDY_ERROR_CREATE_MSD", "Error occured creating a More Study Details value."),
	  
	  /**
	   * Error occurred updating a More Study Details value.
	   */
	  STUDY_ERROR_UPDATE_MSD("STUDY_ERROR_UPDATE_MSD", "Error occured updating a More Study Details value."),
	  
	  /**
	   * Request contained a method not implemented
	   */
	  NOT_IMPLEMENTED("NOT_IMPLEMENTED", "A request contained a method not implemented"),
	  
	  /**
	   * Unknown (throwable) error was found
	   */
	  UNKNOWN_THROWABLE("UNKNOWN_THROWABLE", "A throwable was found."),
	  
	  /**
	   * Error occurred creating a study team member. Already exists
	   */
	  STUDY_ERROR_TEAM_MEMBER_ALREADY_EXISTS("STUDY_ERROR_TEAM_MEMBER_ALREADY_EXISTS", "Error occurred creating a study team member. Already exists"),
	  
	  /**
	   * Enrollment Notification User not found
	   */
	  ENROLLMENT_NOTIFICATION_USER_NOT_FOUND("ENROLLMENT_NOTIFICATION_USER_NOT_FOUND", "Enrollment Notification User not found"),
	  
	  /**
	   * Approve Enrollment User not found
	   */
	  GET_APPROVE_ENROLLMENT_NOTIFICATION_USER_NOT_FOUND("GET_APPROVE_ENROLLMENT_NOTIFICATION_USER_NOT_FOUND", "Approve Enrollment User not found"),
	  
	  /**
	   * Get Assigned to User not found:
	   */
	  GET_ASSIGNED_TO_USERID_NOT_FOUND("GET_ASSIGNED_TO_USERID_NOT_FOUND", "Get Assigned to User not found:"),

	  /**
	   * Get Documented by User not found:
	   */
	  GET_DOCUMENTED_BY_USERID_NOT_FOUND("GET_DOCUMENTED_BY_USERID_NOT_FOUND" , "Get Documented by User not found:"),
	  
	  /**
	   * User is not authorized via team membership
	   */
	  STUDY_TEAM_AUTHORIZATION("STUDY_TEAM_AUTHORIZATION", "User is not authorized via team membership"), 
	  
	  /**
	   * User not authorized via group membership
	   */
	  GROUP_AUTHORIZATION("GROUP_AUTHORIZATION", "User not authorized via group membership"),
	  
	  /**
	   * Organization not found
	   */
	  ORGANIZATION_NOT_FOUND("ORGANIZATION_NOT_FOUND", "Organization not found"),
	  
	  /**
	   * Study Organization already exists in study
	   */
	  STUDY_ERROR_CREATE_DUPLICATE_ORGANIZATION("STUDY_ERROR_CREATE_DUPLICATE_ORGANIZATION","Study Organization already exists in study"),
	  
	  /**
	   * Study Status Organization was not found in the study
	   */
	  STUDY_STATUS_ORG_NOT_IN_STUDY("STUDY_STATUS_ORG_NOT_IN_STUDY", "The study statis organization was not found in the study"),
	  
	  /**
	   * User not found
	   */
	  USER_NOT_FOUND("USER_NOT_FOUND", "User not found"),
	  
	  /**
	   * @deprecated hidden users will be allowed to be specified in Velos eResearch
	   */
	  USER_HIDDEN("USER_HIDDEN","User is hidden"), 
	  
	  /**
	   * Heart Beat(runtime) not found
	   */
	  HEART_BEAT_NOT_FOUND("HEART_BEAT_NOT_FOUND" , "No Heart Beat"),
	 
	  /**
	   * Database connection not found
	   */
	  DATABASE_CONNECTION_NOT_FOUND("DATABASE_CONNECTION_NOT_FOUND","Database connection not found"),
	  
	  /**
	   * Exception while reading manifest file
	   */
	  EXECPTION_WHILE_READING_MANIFEST_FILE("EXECPTION_WHILE_READING_MANIFEST_FILE","Exception while reading manifest file:"),
	  
	  /**
	   * Exception while getting attributes from manifest file:
	   */
	  EXECPTION_WHILE_GETTING_ATTRIBUTES_FROM_MANIFEST_FILE("EXECPTION_WHILE_GETTING_ATTRIBUTES_FROM_MANIFEST_FILE","Exception while getting attributes from manifest file:"),
	  
	  /**
	   * Exception while getting remote Monitoring Services
	   */
	  EXECPTION_GETTING_REMOTE_MONITORING_SERVICES("EXECPTION_GETTING_REMOTE_MONITORING_SERVICES","exception while getting remote Monitoring Services"),
	  
	  /**
	   * Patient not found
	   */
	  PATIENT_NOT_FOUND("PATIENT_NOT_FOUND","Patient not found"),
	  
	  /**
	   * Exception while accessing patient data
	   */
	  PATIENT_DATA_AUTHORIZATION("PATIENT_DATA_AUTHORIZATION","User not authorized to access data of this patient"),
	  
	  /**
	   * Exception while getting studies for patient
	   */
	  STUDIES_FOR_PATIENT_NOT_FOUND("STUDIES_FOR_PATIENT_NOT_FOUND","Studies for Patient not found"),
	  
	  /**
	   * Exception while getting remote patient schedule Services
	   */
	  PATIENT_SCHEDULE_NOT_FOUND("PATIENT_SCHEDULE_NOT_FOUND","Patient schedule not found"),
	  
	  /**
	   * Exception while getting remote Monitoring Services
	   */
	  EXECPTION_GETTING_REMOTE_PATIENT_DEMOGRAPHICS_SERVICES("EXECPTION_GETTING_REMOTE_PATIENT_DEMOGRAPHICS_SERVICES","exception getting remote PatientDemographics Services"),
	  
	  /**
	   * Exception while getting Study Status
	   */
	  STUDY_STATUS_NOT_FOUND("STUDY_STATUS_NOT_FOUND","Study status not found"),
	  
	  /**
	   * Calendar not found
	   */
	  CALENDAR_NOT_FOUND("CALENDAR_NOT_FOUND", "Calendar could not be found"),
	  
	  /**
	   * Event not found
	   */
	  EVENT_NOT_FOUND("EVENT_NOT_FOUND", "Event could not be found"),
	  
	  /**
	   * Visit not found
	   */
	  VISIT_NOT_FOUND("VISIT_NOT_FOUND", "Visit could not be found"),
	  
	  /**
	   * Currency not found
	   */
	  CURRENCY_NOT_FOUND("CURRENCY_NOT_FOUND", "Currency could not be found"),
	  
	  /**
	   * Exception while creating study calendar
	   */
	  ERROR_CREATING_STUDY_CALENDAR("ERROR_CREATING_STUDY_CALENDAR","Error Creating Study Calendar"),
	  
	  /**
	   * Exception while creating event
	   */
	  ERROR_CREATING_STUDY_CALENDAR_EVENT("ERROR_CREATING_STUDY_CALENDAR_EVENT","Error creating study calendar event"),
	  
	  /**
	   * Exception while creating study calendar event cost
	   */
	  ERROR_CREATING_STUDY_CALENDAR_EVENT_COST("ERROR_CREATING_STUDY_CALENDAR_EVENT_COST","Error creating study calendar event cost"),
	  
	  /**
	   * Exception while creating study calendar event cost
	   */
	  STUDY_CALENDAR_ERROR_CREATE_EVENT("STUDY_CALENDAR_ERROR_CREATE_EVENT","Error creating study calendar event"),
	  
	  /**
	   *Exception while creating study calendar
	   */
	  CALENDAR_NAME_ALREADY_EXISTS("CALENDAR_NAME_ALREADY_EXISTS", "Calendar name already exists"),
	  
      /**
       * Visit name already exists
       */
	  VISIT_NAME_ALREADY_EXISTS("VISIT_NAME_ALREADY_EXISTS", "Visit name already exists"),
	  
      /**
       * Visit interval exceeds calendar duration
       */
	  VISIT_INTERVAL_EXCEEDS_CALENDAR_DURATION("VISIT_INTERVAL_EXCEEDS_CALENDAR_DURATION", "visit's interval is past Calendar duration"),
	  
	/**
	 * Duplicate visit name in request
	 */
	DUPLICATE_VISIT_NAME("DUPLICATE_VISIT_NAME", "Duplicate visit name in request"), 
	
	/**
	 * Duplicate event name in request
	 */
	DUPLICATE_EVENT_NAME("DUPLICATE_EVENT_NAME", "Duplicate event name in request")	; 
	  
	  private  String code;
	  private  String message;

	  IssueTypes(String code, String message) {
	     this.code = code;
	     this.message = message;
	  }

	  public String getCode() { return code; }
	  public String getMessage() { return message; }
	  @Override
	  public String toString() {
	    return code + ": " + message;
	  }
}
