/**
 * 
 */
package com.velos.services.monitoring;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author dylan
 *
 */
@Entity
@Table(name = "svc_session_log")
public class SessionLog implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -8305354064889060296L;

	/**
     * the UserSession Id
     */
    public Integer id;

    /**
     * the user id
     */
    public String userIdent;
    
    /**
     * the login date time of user session
     */
    public Date loginTime;

    /**
     * the logout date time of user session
     */
    public Date logoutTime;

    /*
     * Login sucess flag
     */
    public Boolean successFlag;

    /*
     * IP Address
     */
    public String remoteAddress;

    public SessionLog(){
		
	}


	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SVC_SESSION")
    @Column(name = "PK_SESSION_LOG")
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "USER_IDENT")
	public String getUserIdent() {
		return userIdent;
	}


	public void setUserIdent(String userIdent) {
		this.userIdent = userIdent;
	}

	@Column(name = "LOGIN_TIME")
	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Date getLogoutTime() {
		return logoutTime;
	}
	
	@Column(name = "LOGOUT_TIME")
	public void setLogoutTime(Date logoutTime) {
		this.logoutTime = logoutTime;
	}
	
	

	@Column(name = "REMOTE_ADDR")
	public String getRemoteAddress() {
		return remoteAddress;
	}

	public void setRemoteAddress(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}



	public Boolean getSuccessFlag() {
		return successFlag;
	}



	public void setSuccessFlag(Boolean successFlag) {
		this.successFlag = successFlag;
	}



	
}
