/**
 * 
 */
package com.velos.services.monitoring;
 

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;
import javax.xml.datatype.XMLGregorianCalendar;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.MessageTraffic;
import com.velos.services.model.MonitorVersion;


/**
 * A remote service supporting the monitoring of service messages.
 * 
 * @author dylan
 *
 */
@Remote
public interface MessageService  {
	public static final String MESSAGE_LOG_KEY = "com.velos.services.message";
	
	public MessageLog setMessageDetails(MessageLog message ) throws OperationException;
	public void updateMessage(MessageLog message) throws OperationException;
	public List<MessageLog> getMessages(Date fromDate, Date toDate, String module) throws OperationException;
	//virendra
	public ResponseHolder getHeartBeat() throws OperationException;
	public MessageTraffic getMessageTraffic(Date fromDate, Date toDate) throws OperationException;
	
}
