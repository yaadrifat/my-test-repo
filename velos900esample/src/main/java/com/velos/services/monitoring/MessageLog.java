/**
 * 
 */
package com.velos.services.monitoring;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 *Transfer object defining properties of a message either incoming to the service 
 *api or outgoing from. 
 * @author dylan
 *
 *
 * Three named queries on svc_message_log, findMessageCountAll for count of all messages irrespective of status,,
 *findMessageCountSuccess for succeded messages,SUCCESS_FLAG=1,
 *findMessageCountFailure for failed messages,SUCCESS_FLAG=0. 
 */

@Entity
@Table(name = "svc_message_log")

@NamedQueries({
@NamedQuery(name="findMessageCountAll",query="SELECT count(id) FROM MessageLog WHERE trim(INITIATION_TIME) BETWEEN :fromDate AND :toDate"),
@NamedQuery(name="findMessageCountSuccess", query="SELECT count(id) FROM MessageLog WHERE trim(INITIATION_TIME) BETWEEN :fromDate AND :toDate AND SUCCESS_FLAG = 1"),
@NamedQuery(name="findMessageCountFailure", query="SELECT count(id) FROM MessageLog WHERE trim(INITIATION_TIME) BETWEEN :fromDate AND :toDate AND SUCCESS_FLAG = 0")
})

public class MessageLog implements Serializable{
	public final static String FIELD_MODULE = MessageLog.class.getName() + ".module";
	public final static String FIELD_BODY = MessageLog.class.getName() + ".body";
	public final static String FIELD_ENDPOINT = MessageLog.class.getName() + ".endpoint";
	public final static String FIELD_IDENTITY = MessageLog.class.getName() + ".identity";
	public final static String FIELD_MESSAGE_ID = MessageLog.class.getName() + ".messageId";
	public final static String FIELD_OPERATION = MessageLog.class.getName() + ".operation";
	public final static String FIELD_ROUTE = MessageLog.class.getName() + ".route";
	public final static String FIELD_TIME = MessageLog.class.getName() + ".time";
	/**
	 * 
	 */
	private static final long serialVersionUID = -531160150663496301L;
	
	@Transient
	public static final String ROUTE_INCOMING = "INCOMING";
	@Transient
	public static final String ROUTE_OUTGOING = "OUTGOING";

	private Integer id;
	
	private String module;
	
	/** Defines the enpoint being communicated to. May store a WS-Addressing reply-to 
	 * url, or other type of identifier.
	 */
	private String endpoint;
	
	/**
	 * The time that the message was received or created for processing
	 */
	private Date time;
	
	/**
	 * The route of the message.
	 */
	private String route;
	
	/**
	 * The itentity of the user
	 */
	private Integer identity;
	
	/**
	 * Body of the message
	 */
	private String body;
	
	/**
	 * Id of the message
	 */
	private String messageId;
	
	/**
	 * Operation of the message
	 */
	private String operation;
	
	/**
	 * Length of time, in milliseconds, to process the messagey
	 * 
	 */
	private Long duration;
	
	/**
	 * Foreign key to a session
	 */
	private Integer sessionId;
	
	/**
	 * Indicates whether message was successfully-processed
	 */
	private Boolean successFlag;
	
	private String errorType;
	
	private String errorString;
	
	
	private static String[] itemNames = new String[]{
			FIELD_MODULE,
			FIELD_BODY,
			FIELD_ENDPOINT,
			FIELD_IDENTITY,
			FIELD_MESSAGE_ID,
			FIELD_OPERATION,
			FIELD_ROUTE,
			FIELD_TIME};
	private static String[] itemDescriptions = new String[]{
			"service module",
			"body of message",
			"external endoint",
			"user identity",
			"message id",
			"message operation",
			"message route",
			"message time"};
	
	private static OpenType[] types = new OpenType[]{
			SimpleType.STRING,
			SimpleType.STRING,
			SimpleType.STRING,
			SimpleType.STRING,
			SimpleType.STRING,
			SimpleType.STRING,
			SimpleType.STRING,
			SimpleType.DATE};
	
	private static CompositeType compositeType = null;

	public MessageLog(){
		
	}
	
	@Column(name = "MODULE")
	public String getModule(){
		return module;
	}
	
	@Column(name = "ENDPOINT")
	public String getEndpoint() {
		return endpoint;
	}
	
	@Column(name = "INITIATION_TIME")
	public Date getTime() {
		return time;
	}
	
	@Column(name = "ROUTE")
	public String getRoute() {
		return route;
	}
	
	@Column(name = "FK_USER")
	public Integer getIdentity() {
		return identity;
	}
	
	@Column(name = "BODY")
	public String getBody() {
		return body;
	}
	
	@Column(name= "MESSAGE_ID")
	public String getMessageId() {
		return messageId;
	}
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public void setRoute(String route) {
		this.route = route;
	}
	public void setIdentity(Integer identity) {
		this.identity = identity;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	} 
	
	public void setModule(String module){
		this.module = module;
	}
	
	public CompositeData toCompositeType() throws OpenDataException{

		Map itemMap = new HashMap();
		itemMap.put(FIELD_MODULE, module);
		itemMap.put(FIELD_BODY, body);
		itemMap.put(FIELD_ENDPOINT, endpoint);
		itemMap.put(FIELD_IDENTITY, identity);
		itemMap.put(FIELD_MESSAGE_ID, messageId);
		itemMap.put(FIELD_OPERATION, operation);
		itemMap.put(FIELD_ROUTE, route);
		itemMap.put(FIELD_TIME, new Date(time.getTime()));
		

		
		return new CompositeDataSupport(compositeType, itemMap);
	}
	
	public static CompositeType getCompositeType(){
		if (compositeType == null){
			try {
				compositeType = new CompositeType(
						"com.velos.management.message", 
						"Type describing a message",
						itemNames,
						itemDescriptions,
						types);
			} catch (OpenDataException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return compositeType;
	}
	
	@Override
	public String toString(){
		StringBuilder returnStr = new StringBuilder();
		returnStr.append("messageId: " + messageId);
		returnStr.append(" module: " + module);
		returnStr.append(" endpoint: " + endpoint);
		returnStr.append(" route: " + route);
		returnStr.append(" time: " + time.toString());
		returnStr.append(" identity: " + identity);
		returnStr.append(" body: " + body);
		
		return returnStr.toString();
	}
	
	@Column(name = "OPERATION")
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	@Column(name = "DURATION")
	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	@Column(name = "FK_SESSION_LOG")
	public Integer getSessionId() {
		return sessionId;
	}

	public void setSessionId(Integer sessionId) {
		this.sessionId = sessionId;
	}
	
	@Column(name = "SUCCESS_FLAG")
	public Boolean getSuccessFlag() {
		return successFlag;
	}

	public void setSuccessFlag(Boolean successFlag) {
		this.successFlag = successFlag;
	}
	
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SVC_MESSAGE")
    @Column(name = "PK_MESSAGE_LOG")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column (name="ERROR_STRING")
	public String getErrorString() {
		return errorString;
	}

	public void setErrorString(String errorString) {
		this.errorString = errorString;
	}

	@Column (name="ERROR_TYPE")
	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}
	

	
	
	
}
