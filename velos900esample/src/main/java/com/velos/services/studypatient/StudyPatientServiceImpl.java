package com.velos.services.studypatient;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.velos.eres.business.common.StudySiteDao;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.studySiteAgent.StudySiteAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatient;
import com.velos.services.util.ObjectLocator;
/**
 * Implementation for StuyPatient Service 
 * @author Virendra
 *
 */
@Stateless
@Remote(StudyPatientService.class)
public class StudyPatientServiceImpl 
extends AbstractService 
implements StudyPatientService{

@EJB
private ObjectMapService objectMapService;
@EJB
GrpRightsAgentRObj groupRightsAgent;
@EJB
private UserAgentRObj userAgent;
@Resource 
private SessionContext sessionContext;
@EJB
private StudySiteAgentRObj studySiteAgent;


private static Logger logger = Logger.getLogger(StudyPatientServiceImpl.class.getName());
	/**
	 * getStudyPatients with param StudyIdentifier
	 */
	public List<StudyPatient> getStudyPatients(StudyIdentifier studyIdentifier) throws OperationException {
		
		try{
			callingUser = 
				getLoggedInUser(
						sessionContext,
						userAgent);
			//call to ObjectLocator for StudyPk
			Integer studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService);
			objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, studyPK);
			
			if(studyPK ==0 || studyPK == null){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found"));
				throw new OperationException("Study not found");
				
			}
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();
			
			boolean hasViewManageProt = 
				GroupAuthModule.hasViewPermission(manageProtocolPriv);
			
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasViewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view study data"));
				throw new AuthorizationException("User Not Authorized to view study data");
			}
			
			// Handle access control in the same way as the where clause in Patient.getStudyPatientsSQL().
			// Get sitePK and pass it into StudyPatientDAO.
			StudySiteDao studySiteDao = studySiteAgent.getSitesForStatAndEnrolledPat(studyPK, 
			        callingUser.getUserId(), 0); // 0 = accountId, which is not used in the SQL
			ArrayList siteIds = studySiteDao.getSiteIds();
			Integer sitePK = 0;
			if (siteIds != null && siteIds.size() > 0) {
			    sitePK = (Integer)siteIds.get(0);
			}
			
			ArrayList<StudyPatient> listCompleteStudyPatient = 
				(ArrayList<StudyPatient>) StudyPatientDAO.
				getStudyPatientByStudyPK(studyPK, sitePK);
			
			if(listCompleteStudyPatient.isEmpty()){
				addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Patient not found"));
				throw new OperationException("Patient not found");
			}
			//Virendra:#6074
			TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
			int patientManagePrivileges = teamAuthModule.getPatientManagePrivileges();//StudyTeamPrivileges();
			
			if (!TeamAuthModule.hasViewPermission(patientManagePrivileges)){
				if (logger.isDebugEnabled()) logger.debug("user does not have permission to view study patient");
			
				addIssue(
						new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
								"User does not have permission to view study patient"));
				throw new AuthorizationException("User does not have permission to view study patient");
			}
			//Hide PHI information if user not having complete patient data view rights 
			Integer managePatientPrivComplete = 
				groupAuth.getAppViewCompletePatientDataPrivileges();
			//Virendra:#6044
			int viewPatientDetailsPrivileges = teamAuthModule.getPatientViewDataPrivileges();
			
			if(managePatientPrivComplete == 3 || 
					(!TeamAuthModule.hasViewPermission(viewPatientDetailsPrivileges)) ){
				return getLstPhiStudyPatient(listCompleteStudyPatient);
			}
			return listCompleteStudyPatient;
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}
	}
	
	
	private ArrayList<StudyPatient> getLstPhiStudyPatient(
			ArrayList<StudyPatient> lstStudyPat) throws OperationException{
		
		for(int i = 0; i < lstStudyPat.size(); i++ ){
			StudyPatient studyPat = lstStudyPat.get(i);
			studyPat.setStudyPatFirstName("*");
			studyPat.setStudyPatLastName("*");
		}
		return lstStudyPat;
	}
}