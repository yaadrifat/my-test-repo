/**
 * Created On Mar 16, 2011
 */
package com.velos.services.outbound;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAnyElement;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.sun.org.apache.xerces.internal.dom.DocumentImpl;
import com.velos.services.CRUDAction;
import com.velos.services.model.SimpleIdentifier;

/**
 * @author Kanwaldeep
 *
 */

public class Change implements Serializable{	

	public Change()
	{
		super(); 
	}

	protected CRUDAction action; 
	protected String module; 
	protected SimpleIdentifier identifier;
	protected String timeStamp;
	
	protected List<JAXBElement> additionalInformation; 
	
	/**
	 * @return {@link com.velos.services.CRUDAction} to represent Action for this change
	 */
	public CRUDAction getAction() {
		return action;
	}
	/**
	 * @param action - {@link com.velos.services.CRUDAction}  which represents Action for this change
	 */
	public void setAction(CRUDAction action) {
		this.action = action;
	}
	/**
	 * @return String module which represent the module for this change.
	 */
	public String getModule() {
		return module;
	}
	/**
	 * @param module(String) to represent the module for this change.
	 */
	public void setModule(String module) {
		this.module = module;
	}
	/**
	 * @return {@link com.velos.services.model.SimpleIdentifier} to represent the OID which has been changed
	 */
	public SimpleIdentifier getIdentifier() {
		return identifier;
	}
	/**
	 * @param identifier {@link com.velos.services.model.SimpleIdentifier} to represent the OID of object changed
	 */
	public void setIdentifier(SimpleIdentifier identifier) {
		this.identifier = identifier;
	}
	/**
	 * @param timeStamp - TimeStamp of Change
	 */
	public void setTimeStamp(String timeStamp)
	{
		this.timeStamp = timeStamp; 
	}
	/**
	 * @return TimeStamp of Change. If there are multiple updates on same object this will return the latest one.
	 */
	public String getTimeStamp()
	{
		return timeStamp; 
	}
	
	public int hashCode()
	{
		if(this.identifier != null) return identifier.hashCode() ;		
		return 0; 
	}
	

	public boolean equals(Object o)
	{
		if(this == o) return true; 
		if(!(o.getClass().equals(this.getClass()))) return false; 
		Change change1 = (Change) o; 
		if(!this.action.equals(change1.getAction())) return false; 
		if(!this.module.equalsIgnoreCase(change1.getModule())) return false; 
		if(!this.identifier.getOID().equalsIgnoreCase(change1.getIdentifier().getOID())) return false; 
		
		return true; 
	}

	public void setAdditionalInformation(List<JAXBElement> addinfo) {
		this.additionalInformation = addinfo; 		
	}
	@XmlAnyElement
	public List<JAXBElement> getAdditionalInformation()
	{
		return additionalInformation; 
	}
}
