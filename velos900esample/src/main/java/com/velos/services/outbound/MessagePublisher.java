/**
 * Created On Mar 16, 2011
 */
package com.velos.services.outbound;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;


/**This class is used to do all JMS related operations.
 * @author Kanwaldeep
 *
 */
public class MessagePublisher {
	
	private String connectionFactoryJNDI; 
	private String topicJNDI; 
	private Connection connection; 
	private Session session; 
	private boolean isTransacted = false; 
	private int ackMode = Session.CLIENT_ACKNOWLEDGE; 
	private Topic topic; 
	private static Logger logger = Logger.getLogger(MessagePublisher.class); 
	private int retryCounter = 0 ;
	private int maxRetry = 3; 
	/**
	 * @param connectionFactoryJNDI
	 * @param topicJNDI
	 */
	public MessagePublisher(String connectionFactoryJNDI, String topicJNDI)
	{
		this.connectionFactoryJNDI = connectionFactoryJNDI; 
		this.topicJNDI = topicJNDI; 
	}
	public void publishMessage(String message) throws OutBoundException
	{
		try{
			getSession(); 
			getTopic(); 
			TextMessage messageObj = session.createTextMessage(message); 
			MessageProducer producer = session.createProducer(topic); 
			producer.send(messageObj); 
		}catch(JMSException jmse)
		{
			logger.error("Unable to publish message to Topic ", jmse); 
			throw new OutBoundException(MessagingConstants.JMS_MESSAGE_PUBLISH_FAILED, message); 
		}
		
		
	}
	/**
	 * 
	 */
	private Topic getTopic() throws OutBoundException{
		try{
			if(topic == null)
			{
				InitialContext initialContext = new InitialContext(); 
				topic = (Topic) initialContext.lookup(topicJNDI);
				
			}
		}catch(NamingException ne)
		{
			logger.error("Unable to locate Topic with topic JNDI: "+ topicJNDI); 
			throw new OutBoundException(MessagingConstants.JMS_TOPIC_NOT_FOUND, "Unable to locate Topic with topic JNDI " + topicJNDI); 
		}catch(Exception e)
		{
			throw new OutBoundException(MessagingConstants.JMS_TOPIC_NOT_FOUND, "Unable to get Topic because of exception : " + e.getMessage()); 
		}
		
		return topic; 
	}
	/**
	 * @param connectionFactoryJNDI the connectionFactoryJNDI to set
	 */
	public void setConnectionFactoryJNDI(String connectionFactoryJNDI) {
		this.connectionFactoryJNDI = connectionFactoryJNDI;
	}
	/**
	 * @return the connectionFactoryJNDI
	 */
	public String getConnectionFactoryJNDI() {
		return connectionFactoryJNDI;
	}
	/**
	 * @param topicJNDI the topicJNDI to set
	 */
	public void setTopicJNDI(String topicJNDI) {
		this.topicJNDI = topicJNDI;
	}
	/**
	 * @return the topicJNDI
	 */
	public String getTopicJNDI() {
		return topicJNDI;
	}
	
	/**
	 * @return the {@link javax.jms.Connection}
	 */
	public Connection getConnection() throws OutBoundException{
		if(connection == null)
		{
			do{
			try{
				InitialContext initialContext= new InitialContext(); 
				ConnectionFactory factory = (ConnectionFactory) initialContext.lookup(connectionFactoryJNDI); 
				connection = factory.createConnection();
			}catch(NamingException ne)
			{
				logger.error("Unable to locate JNDI for Connection Factory : " + connectionFactoryJNDI);
			} catch (JMSException e) {
				
				logger.error("Unable to get JMS connection :: Retry Counter "+ retryCounter ); 
			}
		}while(connection == null && retryCounter++ < maxRetry );
		}
		
		if(connection == null) throw new OutBoundException(MessagingConstants.JMS_CONNECTION_FAILED, "Unable to get Connection"); 
		return connection;
	}
	
	/**
	 * @return the {@link javax.jms.Session}
	 */
	public Session getSession() throws OutBoundException{
		if(session == null)
		{
			try{
				if(connection == null ) connection = getConnection(); 
				session = connection.createSession(isTransacted, ackMode); 				
			}catch(JMSException jmse)
			{
				throw new OutBoundException(MessagingConstants.JMS_SESSION_FAILED, "Unable to get Session"); 
			}
		}
		return session;
	}

	
	/**
	 * Closes JMS Session
	 */
	public void closeSession()
	{
		try {
			if(session != null)
					
				session.close();
		
			} catch (JMSException e) {
				// TODO Auto-generated catch block	
				e.printStackTrace();
			} 
		
	}
	
	/**
	 * Closes JMS Connection
	 */
	public void closeConnection()
	{
		try{
			if(connection != null)
				connection.close();		
		}catch(JMSException jmse)
		{
			// TODO Auto-generated catch block
		}
	}
	/**
	 * @param maxRetry the maxRetry to set
	 */
	public void setMaxRetry(int maxRetry) {
		this.maxRetry = maxRetry;
	}
	/**
	 * @return the maxRetry
	 */
	public int getMaxRetry() {
		return maxRetry;
	}
}
