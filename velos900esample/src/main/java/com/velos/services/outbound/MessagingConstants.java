package com.velos.services.outbound;

/**
 * Constants needed for 
 * @author Kanwaldeep 
 */
public interface MessagingConstants {
	
	public static String OUTBOUND_MESSAGE_INTERVAL = "OUTBOUND_MESSAGE_FREQUENCY"; 
	
	
	// Message status 
	public static int MESSAGE_STATUS_NEW = 0; 
	public static int MESSAGE_STATUS_IN_PROCESS = 1; 
	public static int MESSAGE_STATUS_PROCESSED = 2; 
	public static int MESSAGE_STATUS_ERRORED_OUT = 4; 
	public static int MESSAGE_STATUS_RE_PROCESS = 5; 
	
	public static String MESSAGE_ACTION_INSERT = "I"; 
	public static String MESSAGE_ACTION_DELETE = "D";
	public static String MESSAGE_ACTION_UPDATE = "U"; 
	
	// Topic information 
	
	public static String TOPIC_CONNECTION_FACTORY_JNDI = "topicconnectionfactory"; 
	public static String TOPIC_JNDI="topicjndi"; 
	
	
	//Exception Constants 
	
	//Exception handling for different kind of errors is controlled by this limit
	//- less than this need to transfer to history table with error message
	// - greater than this need to set for re processing in next batch cycle
	public static int OUTBOUND_EXCEPTION_CONTROLLER_LIMIT = -200; 
	
	
	// List error which can cause by network failure - for these errors don't move messages out of queue Table
    
	
	public static int JMS_CONNECTION_FAILED = -101; 
	public static int JMS_SESSION_FAILED = -102; 
	public static int JMS_TOPIC_NOT_FOUND = -103; 
	public static int JMS_MESSAGE_PUBLISH_FAILED = -104; 
	
	//Exception constants for messages that are failed and can't be reprocessed
	
	public static int UNABLE_TO_MARSHALL_TO_XML = -201; 

}
