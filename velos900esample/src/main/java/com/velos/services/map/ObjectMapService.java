/**
 * 
 */
package com.velos.services.map;


import javax.ejb.Remote;

/**
 * @author dylan
 *
 */ 


@Remote
public interface ObjectMapService {

	public static final String PERSISTENCE_UNIT_STUDY = "er_study";
	public static final String PERSISTENCE_UNIT_USER = "er_user";
	public static final String PERSISTENCE_UNIT_ORGANIZATION ="er_site";
	public static final String PERSISTENCE_UNIT_STUDY_STATUS = "er_studystat";
	public static final String PERSISTENCE_UNIT_PERSON = "person"; 
	public static final String PERSISTENCE_UNIT_EVENT_STATUS = "sch_codelst";
	public static final String PERSISTENCE_UNIT_EVENT = "sch_event1";
	public static final String PERSISTENCE_UNIT_PATPROT = "er_patprot";
	public static final String PERSISTENCE_UNIT_VISIT= "sch_protocol_visit";
	public static final String PERSISTENCE_UNIT_STUDY_CALENDAR="event_assoc";
	public static final String PERSISTENCE_UNIT_EVENT_COST="sch_eventcost"; 
	
	public ObjectMap getObjectMapFromId(String id) 
		throws MultipleObjectsFoundException;
	
//	public ObjectMap createObjectMap(String tableName, Integer tablePK); 
	
	public ObjectMap getOrCreateObjectMapFromPK(String table_name, Integer tablePK);
	//virendra
	public Integer getObjectPkFromOID(String OID);
}
