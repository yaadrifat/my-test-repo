/**
 * 
 */
package com.velos.services.calendar;


import java.util.List;

import javax.ejb.Remote;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.CalendarEvent;
import com.velos.services.model.CalendarEvents;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.CalendarSummary;
import com.velos.services.model.CalendarVisit;
import com.velos.services.model.CalendarVisits;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.StudyCalendar;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VisitIdentifier;



/**
 * @author dylan
 *
 */


@Remote

public interface StudyCalendarService {
	
	
	
	//Calendar methods
	public ResponseHolder createStudyCalendar(StudyCalendar study) throws OperationException;
	
	public ResponseHolder updateStudyCalendarSummary(
			CalendarIdentifier calendarIdentifier, 
			StudyIdentifier studyIdentifier, 
			String calendarName,
			CalendarSummary calendarSummary) 
	throws OperationException;
	
	public StudyCalendar getStudyCalendar(CalendarIdentifier calendarIdentifier, StudyIdentifier studyIdentifier, String calendarName) 
	throws OperationException;
	
	//Visit Methods
	public ResponseHolder updateStudyCalendarVisit(			
			CalendarIdentifier studyCalendarIdentifier,
			String visitName,			
			CalendarVisit studyCalendarVisit) 
	throws OperationException;
	
	public CalendarVisit getStudyCalendarVisit(			
			CalendarIdentifier calendarIdentifier,			
			VisitIdentifier visitIdentifier,
			String visitName) 
	throws OperationException;
	
	
	public ResponseHolder addVisitsToStudyCalendar(			
			CalendarIdentifier calendarIdentifier,			
			CalendarVisits newVisit) 
	throws OperationException;
	

	public ResponseHolder removeVisitFromStudyCalendar(
			CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier,
			String visitName) 
	throws OperationException;
	
	
	//Event Methods
	
	public  ResponseHolder updateStudyCalendarEvent(
			CalendarIdentifier studyCalendarIdentifier,
			VisitIdentifier visitIdentifier, 
			String visitName,
			EventIdentifier eventIdentifier, 
			String eventName,
			CalendarEvent studyCalendarEvent) 
	throws OperationException;
	
	public  CalendarEvent getStudyCalendarEvent(
			CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier, 
			String visitName,
			EventIdentifier eventIdentifier, 
			String eventName) 
	throws OperationException;
	
	
	public ResponseHolder addEventsToStudyCalendarVisit(
			CalendarIdentifier calendarIdentifier,
			String visitName,
			VisitIdentifier visitIdentifier, 
			CalendarEvents studyCalendarEvent) 
	throws OperationException;
	
	
	public ResponseHolder removeEventFromStudyCalendar(
			CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier,
			String visitName,
			EventIdentifier eventIdentifier,
			String eventName) 
	throws OperationException;
	
}
