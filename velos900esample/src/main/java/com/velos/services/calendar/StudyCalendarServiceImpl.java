/**
 * 
 */
package com.velos.services.calendar;

import static javax.ejb.TransactionAttributeType.REQUIRED;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.log4j.Logger;

import com.velos.eres.business.catLib.impl.CatLibBean;
import com.velos.eres.business.study.impl.StudyBean;
import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.catLibAgent.CatLibAgentRObj;
import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.studyAgent.StudyAgent;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.esch.business.eventassoc.impl.EventAssocBean;
import com.velos.esch.business.protvisit.impl.ProtVisitBean;
import com.velos.esch.service.eventassocAgent.EventAssocAgentRObj;
import com.velos.esch.service.eventcostAgent.EventcostAgentRObj;
import com.velos.esch.service.eventdefAgent.EventdefAgentRObj;
import com.velos.esch.service.protvisitAgent.ProtVisitAgentRObj;
import com.velos.esch.service.util.DateUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthenticationException;
import com.velos.services.AuthorizationException;
import com.velos.services.CRUDAction;
import com.velos.services.CodeNotFoundException;
import com.velos.services.CompletedAction;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.MultipleObjectsFoundException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.CalendarEvent;
import com.velos.services.model.CalendarEvents;
import com.velos.services.model.CalendarIdentifier;
import com.velos.services.model.CalendarSummary;
import com.velos.services.model.CalendarVisit;
import com.velos.services.model.CalendarVisits;
import com.velos.services.model.Code;
import com.velos.services.model.Duration;
import com.velos.services.model.EventIdentifier;
import com.velos.services.model.StudyCalendar;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.model.Duration.TimeUnits;
import com.velos.services.util.CodeCache;
import com.velos.services.util.ObjectLocator;

/**
 * @author dylan
 *
 */
@Stateless
@Remote(StudyCalendarService.class)
public class StudyCalendarServiceImpl extends AbstractService implements StudyCalendarService {
	
    private static Logger logger = Logger.getLogger(StudyCalendarServiceImpl.class.getName()); 
	@EJB
	GrpRightsAgentRObj groupRightsAgent;
	
	@EJB
	private ObjectMapService objectMapService;
	
	@EJB
	private EventAssocAgentRObj eventAssocAgent; 
	
	@EJB
	private StudyAgent studyAgent;
	
	@EJB
	private UserAgentRObj userAgent;
	
	@EJB 
	private CatLibAgentRObj catlib;
	@EJB 
	private EventcostAgentRObj eventCostAgent;
	@EJB 
	private EventdefAgentRObj eventDefAgent;
	@EJB
	private ProtVisitAgentRObj protVisitAgent; 
	
	@Resource
	private SessionContext sessionContext; 
	/**
	 * 
	 */
	public ResponseHolder createStudyCalendar(StudyCalendar studyCalendar) throws OperationException {
		
		//Virendra
		try{
		
			try {
			callingUser = getLoggedInUser(sessionContext,userAgent);
		} catch (AuthenticationException e) {
			throw new OperationException();
		}
		validate(studyCalendar);
		checkGroupUpdatePermissions();
		Integer studyPK = 0;
		studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyCalendar.getStudyIdentifier(),objectMapService);
		if(studyPK == 0){
			Issue issue = new Issue(IssueTypes.STUDY_NOT_FOUND, "Study not found for Study Calendar ID : " + studyCalendar.getCalendarIdentifier()+": Study Identifier"+ studyCalendar.getStudyIdentifier());
			addIssue(issue);
			logger.error(issue);
			throw new OperationException(issue.toString());
			
		}
		checkStudyTeamCreatePermissions(studyPK);
		
		EventAssocBean eventAssocBean = new EventAssocBean();
		
		Integer studyCalendarPK = createStudyCalendar(studyCalendar, eventAssocBean);
				
		CalendarIdentifier calendarIdentifier = new CalendarIdentifier();
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY_CALENDAR, studyCalendarPK);
		calendarIdentifier.setOID(map.getOID()); 
		
		response.addObjectCreatedAction(calendarIdentifier);
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", e);
			throw new OperationRolledBackException(response.getIssues());

		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;
	}
	

	/* (non-Javadoc)
	 * @see com.velos.services.calendar.StudyCalendarService#updateStudyCalendar(com.velos.services.model.CalendarIdentifier, com.velos.services.model.StudyCalendar)
	 */
	public ResponseHolder updateStudyCalendarSummary(
			CalendarIdentifier calendarIdentifier,StudyIdentifier studyIdentifier, String calendarName, CalendarSummary calendarSummary)
			throws OperationException {
		try {
			callingUser = getLoggedInUser(sessionContext,userAgent);
		} catch (AuthenticationException e) {
			throw new OperationException();
		}
		validate(calendarSummary);
		checkGroupUpdatePermissions();
		Integer studyCalendarPK = updateStudyCalendarSummary(calendarIdentifier, calendarSummary, false);
		// TODO Auto-generated method stub
		return null;
	}

	private Integer updateStudyCalendarSummary(CalendarIdentifier calendarIdentifier,
			CalendarSummary calendarSummary, boolean isNew) throws OperationException {
		ObjectMap obj = null;
		try {
			obj = objectMapService.getObjectMapFromId(calendarIdentifier.getOID());
		} catch (MultipleObjectsFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//need to check if oid of calendar only
		Integer calendarPK = obj.getTablePK();
		if(!obj.getTableName().equalsIgnoreCase(ObjectMapService.PERSISTENCE_UNIT_EVENT)){
			
			addIssue(
					new Issue(
							IssueTypes.CALENDAR_NOT_FOUND, 
							"Study calendar not found"));
				throw new OperationException(); 

		}
		//commented virendra
		//StudyIdentifier studyIdentifier = new StudyIdentifier();
		
		EventAssocBean eventAssocBean = eventAssocAgent.getEventAssocDetails(calendarPK);
		//calendarSummaryIntoBean(calendarPK, studyIdentifier, calendarSummary, eventAssocBean, isNew);
		Integer eventAssocPK= eventAssocAgent.updateEventAssoc(eventAssocBean);
		return eventAssocPK;
	
	}


	/* (non-Javadoc)
	 * @see com.velos.services.calendar.StudyCalendarService#getStudyCalendar(com.velos.services.model.CalendarIdentifier)
	 */
	public StudyCalendar getStudyCalendar(CalendarIdentifier calendarIdentifier, StudyIdentifier studyIdentifier, String calendarName)
			throws OperationException {
		
		try{
			try {
				callingUser = getLoggedInUser(sessionContext,userAgent);
			} catch (AuthenticationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		checkGroupViewPermissions(); 	
		
		// getStudyId from calendarIdentifier 
		Integer studyPK = 0;
		Integer calendarID = 0;
		
		calendarID = objectMapService.getObjectPkFromOID(calendarIdentifier.getOID()); 
		
		
		if(calendarID == 0){
			
			Issue issue = new Issue(IssueTypes.CALENDAR_NOT_FOUND, "For Calendar ID : " + calendarIdentifier.getOID());
			addIssue(issue);
			
			studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier, objectMapService);
			if(studyPK == 0 ){
					addIssue(new Issue(
							IssueTypes.STUDY_NOT_FOUND, 
							"The Study could not be found for study number: "+studyIdentifier.getStudyNumber()));
					throw new OperationException(issue.toString());
			} 
			StudyCalendarDAO studyCalendarDAO = new StudyCalendarDAO();
			calendarID = studyCalendarDAO.locateCalendarPK(calendarName, studyPK);
			
			
			if(calendarID == 0)
			{
				issue = new Issue(IssueTypes.CALENDAR_NOT_FOUND, "For Calendar ID : " + calendarIdentifier);
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
		
		}
		
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_EVENT, calendarID);
		calendarIdentifier.setOID(map.getOID());
		//getStudyId from eventID 
		EventAssocBean calendarBean = eventAssocAgent.getEventAssocDetails(calendarID); 
		if(calendarBean == null)
		{
			Issue issue = new Issue(IssueTypes.CALENDAR_NOT_FOUND, "For Calendar ID : " + calendarIdentifier.getOID());
			addIssue(issue);
			logger.error(issue);
			throw new OperationException(issue.toString());
		}
		
		StudyCalendar calendar = getStudyCalendar(calendarBean); 
		calendar.setCalendarIdentifier(calendarIdentifier); 
		return calendar;
		
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", e);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", t);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		
		
	}
	/**
	 * 
	 * @param calendarBean
	 * @return
	 * @throws OperationException
	 */
	private StudyCalendar getStudyCalendar(EventAssocBean calendarBean) throws OperationException{
		
		 
		
		StudyCalendar calendar = new StudyCalendar(); 
		//StudyIdentifier
		StudyIdentifier studyIdentifier = new StudyIdentifier(); 
		ObjectMap map = objectMapService.getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_STUDY, EJBUtil.stringToInteger(calendarBean.getChain_id())); 
		studyIdentifier.setOID(map.getOID()); 
		StudyBean studyBean = studyAgent.getStudyDetails(EJBUtil.stringToInteger(calendarBean.getChain_id())); 
		studyIdentifier.setStudyNumber(studyBean.getStudyNumber()); 
		
		calendar.setStudyIdentifier(studyIdentifier);
		calendar.setCalendarSummary(marshallCalendarSummary(calendarBean));		
		calendar.setVisits(marshallVisits(calendarBean.getEvent_id())); 
		
		return calendar; 
	}
	/**
	 * 
	 * @param protocolID
	 * @return
	 * @throws OperationException
	 */
	private CalendarVisits marshallVisits(Integer protocolID) throws OperationException {
	
		StudyCalendarDAO dao = new StudyCalendarDAO(); 	
		CalendarVisits visits = null; 
		try{
			visits = dao.getStudyCalendarVisits(protocolID, EJBUtil.stringToNum(callingUser.getUserAccountId()), null, null); 	
		}catch(SQLException sqe)
		{
			logger.error(sqe); 
			throw new OperationException();
		}
		return visits; 
	}

	/**
	 * 
	 */
	public ResponseHolder updateStudyCalendarVisit(
			CalendarIdentifier studyCalendarIdentifier, String visitName,
			CalendarVisit studyCalendarVisit) throws OperationException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.velos.services.calendar.StudyCalendarService#getStudyCalendarVisit(com.velos.services.model.CalendarIdentifier, com.velos.services.model.VisitIdentifier, java.lang.String)
	 */
	public CalendarVisit getStudyCalendarVisit(CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier, String visitName)
			throws OperationException {
		
		checkGroupViewPermissions();
		
				
		
		Integer visitPK = locateVisit(calendarIdentifier, visitIdentifier, visitName) ; 
	
		StudyCalendarDAO dao = new StudyCalendarDAO(); 
		dao.getCalendarStudyPKForVisit(visitPK); 
		checkStudyTeamViewPermissions(dao.getStudyPK()); 
		
		CalendarVisit visit = null;
		try {
			visit = dao.getStudyCalendarVisit( EJBUtil.stringToInteger(callingUser.getUserAccountId()), visitPK, dao.getCalendarPK()); 
			//	visit = (CalendarVisit) dao.getStudyCalendarVisits(null, EJBUtil.stringToInteger(callingUser.getUserAccountId()), visitPK, null).getVisit().get(0);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return visit;
	}

	/* (non-Javadoc)
	 * @see com.velos.services.calendar.StudyCalendarService#addVisitsToStudyCalendar(com.velos.services.model.CalendarIdentifier, java.util.List)
	 */
	@TransactionAttribute(REQUIRED)
	public ResponseHolder addVisitsToStudyCalendar(
			CalendarIdentifier calendarIdentifier, CalendarVisits newVisits)
			throws OperationException {
		try
		{
			checkGroupUpdatePermissions(); 
			int calendarID = objectMapService.getObjectPkFromOID(calendarIdentifier.getOID()); 
			//getStudyId from eventID 
			EventAssocBean calendarBean = eventAssocAgent.getEventAssocDetails(calendarID); 
			if(calendarBean == null)
			{
				Issue issue = new Issue(IssueTypes.CALENDAR_NOT_FOUND, "For Calendar ID : " + calendarIdentifier);
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			
			validate(newVisits); 
			
			Integer studyPK = Integer.parseInt(calendarBean.getChain_id()); 
			checkStudyTeamUpdatePermissions(studyPK); 
			//pass Calendar Duration 
			int duration= EJBUtil.stringToInteger(calendarBean.getDuration()); 
			String durationUnit = calendarBean.getDurationUnit(); 
			if(durationUnit.equalsIgnoreCase("W"))
			{
				duration = duration * 7; 
			}else if (durationUnit.equalsIgnoreCase("M"))
			{
				duration = duration * 30; 
			}else if(durationUnit.equalsIgnoreCase("Y"))
			{
				duration = duration*365; 
			}
			Map<String, Object> parameters = new HashMap<String, Object>(); 
			parameters.put("ProtVisitAgentRObj", protVisitAgent); 
			parameters.put("ResponseHolder" , response); 
			parameters.put("StudyCalendarServiceImpl", this); 
			parameters.put("ObjectMapService", objectMapService); 
			//parameters for events
			parameters.put("EventdefAgentRObj", eventDefAgent); 
			parameters.put("EventAssocAgentRObj", eventAssocAgent); 
			parameters.put("EventcostAgentRObj", eventCostAgent); 
			parameters.put("callingUser", callingUser);
			parameters.put("sessionContext", sessionContext); 
		
			CalendarVisitHelper helper = new CalendarVisitHelper(); 
			helper.addVisitsToCalendar(calendarID, newVisits, duration, parameters); 
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", e);
			throw new OperationRolledBackException(response.getIssues());
	
		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;
	}

	
	

	/* (non-Javadoc)
	 * @see com.velos.services.calendar.StudyCalendarService#removeVisitFromStudyCalendar(com.velos.services.model.CalendarIdentifier, com.velos.services.model.VisitIdentifier, java.lang.String)
	 */
	public ResponseHolder removeVisitFromStudyCalendar(
			CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier, String visitName)
			throws OperationException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.velos.services.calendar.StudyCalendarService#updateStudyCalendarEvent(com.velos.services.model.CalendarIdentifier, com.velos.services.model.VisitIdentifier, java.lang.String, com.velos.services.model.EventIdentifier, java.lang.String, com.velos.services.model.Event)
	 */
	public ResponseHolder updateStudyCalendarEvent(
			CalendarIdentifier studyCalendarIdentifier,
			VisitIdentifier visitIdentifier, String visitName,
			EventIdentifier eventIdentifier, String eventName,
			CalendarEvent studyCalendarEvent) throws OperationException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.velos.services.calendar.StudyCalendarService#getStudyCalendarEvent(com.velos.services.model.CalendarIdentifier, com.velos.services.model.VisitIdentifier, java.lang.String, com.velos.services.model.EventIdentifier, java.lang.String)
	 */
	public CalendarEvent getStudyCalendarEvent(CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier, String visitName,
			EventIdentifier eventIdentifier, String eventName)
			throws OperationException {
		
		checkGroupViewPermissions(); 
		Integer eventPK = locateEvent(calendarIdentifier, visitName, visitIdentifier, eventName, eventIdentifier); 
		
				
		StudyCalendarDAO dao = new StudyCalendarDAO(); 
		dao.getCalendarStudyPKForEvent(eventPK); 		
		checkStudyTeamViewPermissions(dao.getStudyPK()); 
		
		CalendarEvent event = new CalendarEvent(); 
		try {
			
			event = dao.getStudyCalendarEvent(eventPK,  EJBUtil.stringToInteger(callingUser.getUserAccountId())); 
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	
		
		return event;
	}

	/* (non-Javadoc)
	 * @see com.velos.services.calendar.StudyCalendarService#addEventsToStudyCalendarVisit(com.velos.services.model.CalendarIdentifier, java.lang.String, com.velos.services.model.VisitIdentifier, java.util.List)
	 */
	public ResponseHolder addEventsToStudyCalendarVisit(
			CalendarIdentifier calendarIdentifier, String visitName,
			VisitIdentifier visitIdentifier,CalendarEvents studyCalendarEvents)
	throws OperationException {
		try{	
			checkGroupUpdatePermissions();
			validate(studyCalendarEvents);

			int visitPK = locateVisit(calendarIdentifier, visitIdentifier, visitName); 

			StudyCalendarDAO dao = new StudyCalendarDAO();
			dao.getCalendarStudyPKForVisit(visitPK);
			Integer studyPK = dao.getStudyPK();
			Integer calendarPK = dao.getCalendarPK(); 
			checkStudyTeamCreatePermissions(studyPK);

			ProtVisitBean visitBean = protVisitAgent.getProtVisitDetails(visitPK); 

			Map<String, Object> parameters = new HashMap<String, Object>(); 
			parameters.put("ObjectMapService", objectMapService); 
			parameters.put("EventdefAgentRObj", eventDefAgent); 
			parameters.put("EventAssocAgentRObj", eventAssocAgent); 
			parameters.put("EventcostAgentRObj", eventCostAgent); 
			parameters.put("ResponseHolder", response); 
			parameters.put("StudyCalendarServiceImpl", this); 
			parameters.put("callingUser", callingUser);
			parameters.put("sessionContext", sessionContext); 

			CalendarEventHelper helper = new CalendarEventHelper(); 
			helper.addEventsToVisit(studyCalendarEvents, calendarPK, visitPK, visitBean.getDisplacement(), parameters); 

			response.addAction(new CompletedAction("Event created and added to visit", CRUDAction.CREATE));
		}catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", e);
			throw new OperationRolledBackException(response.getIssues());

		}
		catch(Throwable t){
			sessionContext.setRollbackOnly();
			addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("StudyCalendarServiceImpl create", t);
			throw new OperationRolledBackException(response.getIssues());

		}
		return response;
	}
	

	/* (non-Javadoc)
	 * @see com.velos.services.calendar.StudyCalendarService#removeEventFromStudyCalendar(com.velos.services.model.CalendarIdentifier, com.velos.services.model.VisitIdentifier, java.lang.String, com.velos.services.model.EventIdentifier, java.lang.String)
	 */
	public ResponseHolder removeEventFromStudyCalendar(
			CalendarIdentifier calendarIdentifier,
			VisitIdentifier visitIdentifier, String visitName,
			EventIdentifier eventIdentifier, String eventName)
			throws OperationException {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * 
	 * @param calendarBean
	 * @return
	 */
	private CalendarSummary marshallCalendarSummary(EventAssocBean calendarBean) 
	{
	
		CalendarSummary summary = new CalendarSummary(); 
		StudyCalendarDAO dao = new StudyCalendarDAO(); 
		summary.setCalendarDescription(calendarBean.getDescription()); 
		summary.setCalendarDuration(new Duration(Integer.parseInt(calendarBean.getDuration()),calendarBean.getDurationUnit())); 
		summary.setCalendarName(calendarBean.getName()); 
		Integer callingUserAccountId = Integer.valueOf(callingUser.getUserAccountId());
		CodeCache codeCache = CodeCache.getInstance();
		Code status = codeCache.getSchCodeSubTypeByPK(CodeCache.CODE_TYPE_STUDY_CALENDAR_STATUS,
				Integer.parseInt(calendarBean.getStatCode()), 
				callingUserAccountId); 
		
		summary.setCalendarStatus(status);
		
		CatLibBean catlibbean = catlib.getCatLibDetails(calendarBean.getEventCalType()); 
		
		Code catLibCode = new Code(); 
		catLibCode.setType(catlibbean.getCatLibType()); 
		catLibCode.setCode(catlibbean.getCatLibName()); 
		catLibCode.setDescription(catlibbean.getCatLibDesc()); 
		summary.setCalendarType(catLibCode); 	
		
		
		return summary; 
	}
	
	/**
	 * 
	 * @throws OperationException
	 */
	private void checkGroupViewPermissions() throws OperationException
	{
		//Get the user's permission for viewing protocols
		GroupAuthModule groupAuth = 
			new GroupAuthModule(callingUser, groupRightsAgent);

		Integer manageProtocolPriv = 
			groupAuth.getAppManageProtocolsPrivileges();

		boolean hasViewManageProt = 
			GroupAuthModule.hasViewPermission(manageProtocolPriv);
		if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
		if (!hasViewManageProt){
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view studies and cant access Study Calendars"));
			throw new AuthorizationException("User Not Authorized to view Study Calendar");
		}
	}
	/**
	 * 
	 * @throws OperationException
	 */
	private void checkGroupUpdatePermissions() throws OperationException
	{
		//Get the user's permission for updating protocols
		GroupAuthModule groupAuth = 
			new GroupAuthModule(callingUser, groupRightsAgent);

		Integer manageProtocolPriv = 
			groupAuth.getAppManageProtocolsPrivileges();

		boolean hasUpdateManageProt = 
			GroupAuthModule.hasEditPermission(manageProtocolPriv);
		if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
		if (!hasUpdateManageProt){
			addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to update studies and cant update Study Calendars"));
			throw new AuthorizationException("User Not Authorized to view Study Calendar");
		}
	}
	/**
	 * 
	 * @param studyPK
	 * @throws OperationException
	 */
	private void checkStudyTeamViewPermissions(int studyPK) throws OperationException
	{
		TeamAuthModule teamAuth = 
			new TeamAuthModule(callingUser.getUserId(), studyPK );
		Integer manageStudySetup = teamAuth.getStudySetupPrivileges(); 
		
		boolean hasViewStudyCalPermissions = TeamAuthModule.hasViewPermission(manageStudySetup); 
		if(logger.isDebugEnabled()) logger.debug("user study team - STUDYCAL priv :" + manageStudySetup); 
		if(!hasViewStudyCalPermissions)
		{
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User Not Authorized to view Study Calendar"));
			throw new AuthorizationException("User Not Authorized to view Study Calendar");
		}
		
		
	}
	/**
	 * 
	 * @param studyPK
	 * @throws OperationException
	 */
	private void checkStudyTeamUpdatePermissions(int studyPK) throws OperationException
	{
		TeamAuthModule teamAuth = 
			new TeamAuthModule(callingUser.getUserId(), studyPK );
		Integer manageStudySetup = teamAuth.getStudySetupPrivileges(); 
		
		boolean hasUpdateStudyCalPermissions = TeamAuthModule.hasEditPermission(manageStudySetup); 
		if(logger.isDebugEnabled()) logger.debug("user study team - STUDYCAL priv :" + manageStudySetup); 
		if(!hasUpdateStudyCalPermissions)
		{
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User Not Authorized to Update Study Calendar"));
			throw new AuthorizationException("User Not Authorized to update Study Calendar");
		}
	}
	
	/**
	 * 
	 * @param studyPK
	 * @throws OperationException
	 */

	private void checkStudyTeamCreatePermissions(int studyPK) throws OperationException
	{
		TeamAuthModule teamAuth = 
			new TeamAuthModule(callingUser.getUserId(), studyPK );
		Integer manageStudySetup = teamAuth.getStudySetupPrivileges(); 
		
		boolean hasUpdateStudyCalPermissions = TeamAuthModule.hasEditPermission(manageStudySetup); 
		if(logger.isDebugEnabled()) logger.debug("user study team - STUDYCAL priv :" + manageStudySetup); 
		if(!hasUpdateStudyCalPermissions)
		{
			addIssue(new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, "User Not Authorized to Create Study Calendar"));
			throw new AuthorizationException("User Not Authorized to create Study Calendar");
		}
	}
	/**
	 * 
	 * @param ctx
	 * @return
	 * @throws Exception
	 */
	@AroundInvoke
	public Object myInterceptor(InvocationContext ctx) throws Exception {
		response = new ResponseHolder();
		callingUser = 
			getLoggedInUser(
					sessionContext,
					userAgent);
		return ctx.proceed();

	}
	
	/**
	 * 
	 * @param studyCalendar
	 * @param eventAssocBean
	 * @return
	 * @throws OperationException
	 */
	private Integer createStudyCalendar(
			StudyCalendar studyCalendar,
			EventAssocBean eventAssocBean) 
	throws OperationException{
		//Integer intCalendarCreated= 0;
		Integer studyCalendarPK = 0;
		StudyIdentifier studyId =studyCalendar.getStudyIdentifier();
		//studyCalendar.getCalendarSummary().getCalendarName();
		
		Integer studyPK= ObjectLocator.studyPKFromIdentifier(callingUser, studyId, objectMapService);
		StudyCalendarDAO dao = new StudyCalendarDAO();
		Integer countSimilarCalendars = dao.checkDuplicateCalendarsInStudy(studyCalendar.getCalendarSummary().getCalendarName(), studyPK);
		if(countSimilarCalendars > 0){
				Issue issue = new Issue(IssueTypes.CALENDAR_NAME_ALREADY_EXISTS, "For Calendar ID : " + studyCalendar.getCalendarSummary().getCalendarName());
				addIssue(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
		}
		
		
		calendarSummaryIntoBean(0, studyCalendar.getStudyIdentifier(), studyCalendar.getCalendarSummary(),eventAssocBean, true );
		//setEventAssocDetails
		
		//Integer intDuplicateStudyCalendar= eventAssocAgent.copyStudyProtocol(protocolId, protocolName, userId, ipAdd, accId);
		studyCalendarPK = eventAssocAgent.setEventAssocCalendarDetails(eventAssocBean);
		//eventAssocBean.setEvent_id(studyCalendarPK);
		
		if (studyCalendarPK == -2 ){
			sessionContext.setRollbackOnly();
			addIssue(new Issue(IssueTypes.ERROR_CREATING_STUDY_CALENDAR));
			throw new OperationException("Error Creating Study Calendar, Study Calendar already exists");

		}
		//studyCalendarPK = eventAssocBean.getEvent_id();
		return studyCalendarPK;
	}
	/**
	 * 
	 * @param calendarPK
	 * @param calendarStudy
	 * @param calendarSummary
	 * @param persistentEventAssocBean
	 * @param isNew
	 * @throws OperationException
	 */
	private void calendarSummaryIntoBean(
			Integer calendarPK, StudyIdentifier calendarStudy,
			CalendarSummary calendarSummary, 
			EventAssocBean persistentEventAssocBean, boolean isNew)
	throws 
	OperationException{
		
		try {
			callingUser = 
				getLoggedInUser(
						sessionContext,
						userAgent);
		} catch (AuthenticationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
		persistentEventAssocBean.setIpAdd(IP_ADDRESS_FIELD_VALUE);
		if (isNew){ 
			persistentEventAssocBean.setCreator(EJBUtil.integerToString(callingUser.getUserId()));
		}
		persistentEventAssocBean.setModifiedBy(EJBUtil.integerToString(callingUser.getUserId()));
		
		persistentEventAssocBean.setDescription(calendarSummary.getCalendarDescription());
		//virendra
		persistentEventAssocBean.setStatusUser(EJBUtil.integerToString(callingUser.getUserId()));
		persistentEventAssocBean.setStatusDate(DateUtil.dateToString(new Date()));
		persistentEventAssocBean.setUser_id(callingUser.getUserAccountId());
		
		Duration calendarDuration = calendarSummary.getCalendarDuration();
		Integer duration = calendarDuration.getValue();
		TimeUnits durationUnit = calendarDuration.getUnit();
		
		if(duration != null && durationUnit != null){
			String strDurationUnit = Duration.encodeUnit(durationUnit);
			persistentEventAssocBean.setDuration(EJBUtil.integerToString(duration));
			persistentEventAssocBean.setDurationUnit(strDurationUnit);
			
		}
		
		//String strDurationUnit = durationUnit.equals(other)
		
		
				
		persistentEventAssocBean.setName(calendarSummary.getCalendarName());
		
		Integer stdCalendarStatusCodePk=0;
		Integer calTypeCodePk=0;
		
		try{
			stdCalendarStatusCodePk = dereferenceSchCode(calendarSummary.getCalendarStatus(), CodeCache.CODE_TYPE_STUDY_CALENDAR_STATUS, callingUser);
			persistentEventAssocBean.setStatCode(EJBUtil.integerToString(stdCalendarStatusCodePk));
		}catch(CodeNotFoundException e){
		addIssue(
				new Issue(
						IssueTypes.CODE_NOT_FOUND, 
						"Study Calendar Status Code not found"));
			throw new OperationException(); 
		
		}
		if(stdCalendarStatusCodePk == 0 || stdCalendarStatusCodePk == null){
			addIssue(
					new Issue(
							IssueTypes.CODE_NOT_FOUND, 
							"Study Calendar Status Code not found"));
				throw new OperationException(); 
			
		}
		
		
		StudyCalendarDAO studyCalendarDAO = new StudyCalendarDAO();
		calTypeCodePk = studyCalendarDAO.locateCatLibPK(calendarSummary.getCalendarType(), CodeCache.CODE_TYPE_STUDY_CALENDAR_STATUS_LIB, callingUser.getUserId());
		if(calTypeCodePk == 0){
			addIssue(
					new Issue(
							IssueTypes.CODE_NOT_FOUND, 
							"Study Calendar Status Type Code not found"));
				throw new OperationException(); 
			
		}
		persistentEventAssocBean.setEventCalType(calTypeCodePk);
		Integer studyPK = ObjectLocator.studyPKFromIdentifier(callingUser, calendarStudy, objectMapService);
		if (studyPK == 0 || studyPK == null){
			Issue issue = new Issue(
						IssueTypes.STUDY_NOT_FOUND, 
						"The Study could not be found");
			addIssue(issue);
			throw new OperationException();
		} 
		persistentEventAssocBean.setChain_id(EJBUtil.integerToString(studyPK));
		persistentEventAssocBean.setEvent_type(CodeCache.CODE_STUDY_CALENDAR_TYPE);
		persistentEventAssocBean.setCalAssocTo(CodeCache.CODE_STUDY_PATIENT_CALENDAR_TYPE);
	}
	/**
	 * 
	 * @param calendarIdentifier
	 * @param visitName
	 * @param visitIdentifier
	 * @param eventName
	 * @param eventIdentifier
	 * @return
	 * @throws OperationException
	 */
	private Integer locateEvent(CalendarIdentifier calendarIdentifier, 
			String visitName,
			VisitIdentifier visitIdentifier,
			String eventName, 
			EventIdentifier eventIdentifier) throws OperationException{
		
		Integer eventPK = null; 
		if(eventIdentifier != null && eventIdentifier.getOID() != null)
		{
			eventPK = objectMapService.getObjectPkFromOID(eventIdentifier.getOID()); 
			if(eventPK == null)
			{ 
				addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND,				
					"Event not found for Event Identifier : OID " + eventIdentifier.getOID())); 
				throw new OperationException(); 
			}
			return eventPK; 			
		}
		
		if(eventName != null && eventName.length() > 0)
		{
			Integer visitPK = locateVisit(calendarIdentifier, visitIdentifier, visitName); 
		
			StudyCalendarDAO dao = new StudyCalendarDAO(); 
			eventPK = dao.getEventPKByEventName(eventName, visitPK); 
			if(eventPK == null)
			{
				StringBuffer errorMessage = new StringBuffer("Event not found with Event Name "+eventName +"for Visit "); 
				if(visitIdentifier != null && visitIdentifier.getOID() != null)
				{
					errorMessage.append(" OID" + visitIdentifier); 
				}
				else
				{
					errorMessage.append(" Name " + visitName + " CalendarIdentifier OID : " + calendarIdentifier.getOID()); 
				}
				addIssue(new Issue(IssueTypes.EVENT_NOT_FOUND, errorMessage.toString())); 
				throw new OperationException(); 
			}
				
			return eventPK; 
		}
			
		
		
		
		addIssue(new Issue(IssueTypes.DATA_VALIDATION, "EventIdentifer OR EventName and VisitIdentifer" +
				" OR EventName and VisitName and CalendarIdentifier is required to find event")); 
		throw new OperationException(); 
		
	}
	/**
	 * 
	 * @param calendarIdentifier
	 * @param visitIdentifier
	 * @param visitName
	 * @return
	 * @throws OperationException
	 */
	private Integer locateVisit(CalendarIdentifier calendarIdentifier, VisitIdentifier visitIdentifier, String visitName) throws OperationException
	{
		
			Integer visitPK = null; 
			if(visitIdentifier != null && visitIdentifier.getOID() != null && visitIdentifier.getOID().length() > 0)
			{
				visitPK = objectMapService.getObjectPkFromOID(visitIdentifier.getOID()); 
				if(visitPK == null)
				{
					addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND, "Visit not found for Visit Identifier - OID " + visitIdentifier.getOID())); 	
					throw new OperationException(); 
				}
				return visitPK; 
			}
			
			if(visitName != null && visitName.length() > 0 && calendarIdentifier != null 
					&& calendarIdentifier.getOID() != null && calendarIdentifier.getOID().length() > 0 )
			{
				Integer calendarPK = objectMapService.getObjectPkFromOID(calendarIdentifier.getOID()); 
				StudyCalendarDAO dao = new StudyCalendarDAO(); 
				visitPK = dao.getVisitPKByName(visitName, calendarPK); 
				if(visitPK == null)
				{
					addIssue(new Issue(IssueTypes.VISIT_NOT_FOUND, "Visit not found for VisitName " + visitName + 
						" and CalendarIdentifier OID " + calendarIdentifier.getOID() )); 
					throw new OperationException(); 
				}
				
				return visitPK; 
			}
			
			addIssue(new Issue(IssueTypes.DATA_VALIDATION, " VisitIdenfier OR CalendarIdentifier and VisitName are required to recognize the Visit")); 
			throw new OperationException(); 
	}
	
	

}
