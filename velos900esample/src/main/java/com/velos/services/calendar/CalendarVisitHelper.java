/**
 * Created On Jun 8, 2011
 */
package com.velos.services.calendar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.velos.eres.business.user.impl.UserBean;
import com.velos.eres.service.util.EJBUtil;
import com.velos.esch.business.common.ProtVisitDao;
import com.velos.esch.business.protvisit.impl.ProtVisitBean;
import com.velos.esch.service.protvisitAgent.ProtVisitAgentRObj;
import com.velos.services.AbstractService;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ValidationException;
import com.velos.services.map.ObjectMap;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.CalendarVisit;
import com.velos.services.model.CalendarVisits;
import com.velos.services.model.Duration;
import com.velos.services.model.VisitIdentifier;
import com.velos.services.ResponseHolder;
import com.velos.services.model.Duration.TimeUnits;

/**
 * @author Kanwaldeep
 *
 */
public class CalendarVisitHelper{
	
	private static Logger logger = Logger.getLogger(CalendarVisitHelper.class); 
	
	public void addVisitsToCalendar(Integer calendarID, CalendarVisits newVisits, int calendarDuration, Map<String, Object> parameters) throws OperationException
	{
		//Get list of saved Visits for this Calendar - used to validate Names and get Relative Visit PK 
		ProtVisitDao dao = ((ProtVisitAgentRObj)parameters.get("ProtVisitAgentRObj")).getProtocolVisits(calendarID); 
		List visitIDs = dao.getVisit_ids(); 
		List visitNames = dao.getNames();
		List<String> visitNamesUpperCase = new ArrayList<String>(); 
		List visitDisplacements = dao.getDisplacements(); 
		for(int j = 0; j < visitNames.size(); j++)
		{
			visitNamesUpperCase.add(visitNames.get(j).toString().toUpperCase()); 
		}
		
	
		Map<String, CalendarVisit> visitNotSaved = new HashMap<String, CalendarVisit>(); 
		for(CalendarVisit visit: newVisits.getVisit())
		{
			if(visitNotSaved.containsKey(visit.getVisitName().toUpperCase()))
			{
				Issue issue = new Issue(IssueTypes.DUPLICATE_VISIT_NAME, "For Visit Name : " + visit.getVisitName());
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
				
			}
			visitNotSaved.put(visit.getVisitName().toUpperCase(), visit); 
		}
		
		
		while(!visitNotSaved.isEmpty())
		{
			visitNotSaved = addVisitsToCalendar(calendarID, visitNotSaved,  visitIDs, visitNamesUpperCase, visitDisplacements, calendarDuration, parameters); 
		}
		
	}
	/**
	 * @param calendarID
	 * @param visitNotSaved
	 * @param savedVisits
	 */
	private Map<String, CalendarVisit> addVisitsToCalendar(int calendarID,
			Map<String,CalendarVisit> visitList, List visitIDs, List<String> visitNames, List visitDisplacements, int calendarDuration, Map<String, Object> parameters) throws OperationException{
		Map<String, CalendarVisit> visitNotSaved = new HashMap<String, CalendarVisit>(); 

		for(CalendarVisit visit: visitList.values())
		{
			 if(visit.getRelativeVisitName() == null || visitNames.contains(visit.getRelativeVisitName().toUpperCase()))
			 {
				 int visitPK = addVisitToStudyCalendar(visit, calendarID, visitIDs, visitNames, visitDisplacements, calendarDuration, parameters);
				 visitIDs.add(visitPK);
				 visitNames.add(visit.getVisitName().toUpperCase()); 
				
			
			 }else if(visitList.containsKey(visit.getRelativeVisitName().toUpperCase()))
			 {
				 //check if relative visit is there in still to add List 				 
				 visitNotSaved.put(visit.getVisitName() , visit); 
			 }
			 else
			 {
				 Issue issue = new Issue(IssueTypes.VISIT_NOT_FOUND, "Relative visit not found :Relative Visit Name : " + visit.getRelativeVisitName());
				 ((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				 logger.error(issue);
				 throw new OperationException(issue.toString());
			 }
				 
		}
		
		return visitNotSaved; 
		
	}

	/**
	 * @param visit
	 * @param calendarID
	 */
	@SuppressWarnings("unchecked")
	private Integer addVisitToStudyCalendar(CalendarVisit visit, int calendarID, List visitIDs, List<String> visitNames, List visitDisplacements, int calendarDuration, Map<String, Object> parameters) throws OperationException{
		
		ProtVisitAgentRObj protVisitAgent = (ProtVisitAgentRObj)parameters.get("ProtVisitAgentRObj"); 
		UserBean callingUser = (UserBean) parameters.get("callingUser"); 
		// check name if it already exists 
		if(visitNames.contains(visit.getVisitName().toUpperCase()))
		{
			Issue issue = new Issue(IssueTypes.VISIT_NAME_ALREADY_EXISTS, "For Visit Name : " + visit.getVisitName());
			((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
			logger.error(issue);
			throw new OperationException(issue.toString());
		}
		//
		ProtVisitBean visitBean = new ProtVisitBean(); 
		visitBean.setName(visit.getVisitName()); 
		int visitNum = 0; 
		try
		{
			visitNum = protVisitAgent.getMaxVisitNo(calendarID);	
		}catch(NullPointerException npe)
		{
			// do nothing - Calendar is not created yet
		}
		// visit_no is not sequence of visit it appears on screen it is order in which visits are entered. 
		//TODO need to check with sonia - are we not going to store PPM sent seq number or eMaven should store it.
		visitBean.setVisit_no(visitNum+1); 
		visitBean.setDescription(visit.getVisitDescription()); 
		int displacement = 0; 
		if(visit.isNoIntervalDefined())
		{
			visitBean.setIntervalFlag("1"); 
		}
		else if(visit.getRelativeVisitName() != null && visit.getRelativeVisitName().length() > 0)
		{
			
			// get index for relative visit
			int i = visitNames.indexOf(visit.getRelativeVisitName().toUpperCase()); 
			
			visitBean.setInsertAfter((Integer) visitIDs.get(i));
			visitBean.setInsertAfterInterval(visit.getRelativeVisitInterval().getValue());
			visitBean.setInsertAfterIntervalUnit(Duration.encodeUnit(visit.getRelativeVisitInterval().getUnit())); 
			
			displacement =(Integer) visitDisplacements.get(i) + visit.getRelativeVisitInterval().getValue(); 
			
			if(displacement > calendarDuration)
			{

				Issue issue = new Issue(IssueTypes.VISIT_INTERVAL_EXCEEDS_CALENDAR_DURATION, "For Visit Name : " + visit.getVisitName());
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
			visitDisplacements.add(displacement); 
			visitBean.setDisplacement(Integer.toString(displacement)); 
			visitBean.setDays(displacement); 
			visitBean.setIntervalFlag("0"); 
			
		}else if(visit.getAbsoluteInterval() != null)
		{	
			displacement = visit.getAbsoluteInterval().getValue();
			visitBean.setDays(displacement); 
			visitBean.setDisplacement(Integer.toString(displacement));
			visitDisplacements.add(displacement);
			visitBean.setIntervalFlag("0"); 
			
		}else
		{
			((ResponseHolder) parameters.get("ResponseHolder"))
			.getIssues().add(new Issue(IssueTypes.DATA_VALIDATION, "Either NoIntervalDefined should be true or RelativeVisit and IntervalAfter or AbsoluteInterval is required"));
			throw new ValidationException();
		}
		
		int visitAfterValue = 0; 
		TimeUnits visitAfterUnit = TimeUnits.DAY; 
		if(visit.getVisitWindowAfter() != null)
		{
			visitAfterValue = visit.getVisitWindowAfter().getValue(); 
			visitAfterUnit = visit.getVisitWindowAfter().getUnit(); 	
			
			if(!visitAfterUnit.equals(TimeUnits.DAY)){

				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Please enter visitWindowAfter unit as 'DAY'"));
				throw new OperationException();

			}
		}
		
		visitBean.setDurationAfter(Integer.toString(visitAfterValue)); 
		visitBean.setDurationUnitAfter(Duration.encodeUnit(visitAfterUnit));
		int visitBeforeValue = 0; 
		TimeUnits visitBeforeUnit = TimeUnits.DAY; 
		if(visit.getVisitWindowBefore() != null)
		{
			visitBeforeValue = visit.getVisitWindowBefore().getValue(); 
			visitBeforeUnit = visit.getVisitWindowBefore().getUnit(); 
			if(!visitBeforeUnit.equals(TimeUnits.DAY)){

				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(
						new Issue(
								IssueTypes.DATA_VALIDATION, 
								"Please enter visitWindowBefore unit as 'DAY'"));
				throw new OperationException();

			}
			 
		}
		
		visitBean.setDurationBefore(Integer.toString(visitBeforeValue));
		visitBean.setDurationUnitBefore(Duration.encodeUnit(visitBeforeUnit));
		visitBean.setProtocol_id(calendarID); 
		visitBean.setCreator(EJBUtil.integerToString(callingUser.getUserId()))	; 
		visitBean.setip_add(AbstractService.IP_ADDRESS_FIELD_VALUE); 
	
		int responseSave = protVisitAgent.setProtVisitDetails(visitBean); 
		
	
		if(responseSave < 0)
		{
			if(responseSave == -1)
			{
				Issue issue = new Issue(IssueTypes.VISIT_NAME_ALREADY_EXISTS, "For Visit Name : " + visit.getVisitName());
				((ResponseHolder) parameters.get("ResponseHolder")).getIssues().add(issue);
				logger.error(issue);
				throw new OperationException(issue.toString());
			}
		}
		visitBean.setVisit_id(responseSave); 
		int visitPK = visitBean.getVisit_id(); 
		if(!(visit.getEvents() == null || visit.getEvents().getEvent().isEmpty()))
		{
			CalendarEventHelper helper = new CalendarEventHelper(); 
			helper.addEventsToVisit(visit.getEvents(),calendarID, visitPK, Integer.toString(displacement), parameters); 
		}
		VisitIdentifier visitIdentifier = new VisitIdentifier(); 
		ObjectMap map = ((ObjectMapService) parameters.get("ObjectMapService")).getOrCreateObjectMapFromPK(ObjectMapService.PERSISTENCE_UNIT_VISIT, visitPK); 
		visitIdentifier.setOID(map.getOID()); 
		((ResponseHolder) parameters.get("ResponseHolder")).addObjectCreatedAction(visitIdentifier); 
		return visitPK ; 
	}

}
