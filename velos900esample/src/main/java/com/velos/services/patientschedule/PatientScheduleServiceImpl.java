package com.velos.services.patientschedule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import com.velos.eres.service.grpRightsAgent.GrpRightsAgentRObj;
import com.velos.eres.service.userAgent.UserAgentRObj;
import com.velos.eres.service.util.EJBUtil;
import com.velos.services.AbstractService;
import com.velos.services.AuthorizationException;
import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.authorization.GroupAuthModule;
import com.velos.services.authorization.TeamAuthModule;
import com.velos.services.map.ObjectMapService;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.PatientSchedule;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.Visit;
import com.velos.services.util.ObjectLocator;
/**
 * Session bean dealing with Patient Schedule object 
 * @author Virendra
 *
 */
@Stateless
@Remote(PatientScheduleService.class)
public class PatientScheduleServiceImpl 
extends AbstractService 
implements PatientScheduleService{

@EJB
private ObjectMapService objectMapService;
@EJB
GrpRightsAgentRObj groupRightsAgent;
@EJB
private UserAgentRObj userAgent;
@Resource 
private SessionContext sessionContext;
	
	private static Logger logger = Logger.getLogger(PatientScheduleServiceImpl.class.getName());
	/**
	 * 
	 */
			
	public ArrayList<PatientSchedule> getPatientSchedules(PatientIdentifier patientId, StudyIdentifier studyIdentifier)
			throws OperationException {
		
		try{
			callingUser = 
				getLoggedInUser(
						sessionContext,
						userAgent);
			Integer studyPK =
				ObjectLocator.studyPKFromIdentifier(callingUser, studyIdentifier,objectMapService);
				
			if (studyPK == null || studyPK==0){
				addIssue(new Issue(IssueTypes.STUDY_NOT_FOUND, "Patient study not found"));
				throw new OperationException("Patient study not found");
			}
			Integer personPK = ObjectLocator.personPKFromPatientIdentifier(
					callingUser, 
					patientId, 
					objectMapService);
			
			if (personPK == null || personPK == 0){
				addIssue(new Issue(IssueTypes.PATIENT_NOT_FOUND, "Study Patient not found"));
				throw new OperationException("Patient not found");
			}
			
			GroupAuthModule groupAuth = 
				new GroupAuthModule(callingUser, groupRightsAgent);
			Integer manageProtocolPriv = 
				groupAuth.getAppManageProtocolsPrivileges();
			
			boolean hasViewManageProt = 
				GroupAuthModule.hasViewPermission(manageProtocolPriv);
			
			if (logger.isDebugEnabled()) logger.debug("user manage protocol priv: " + manageProtocolPriv);
			if (!hasViewManageProt){
				addIssue(new Issue(IssueTypes.GROUP_AUTHORIZATION, "User Not Authorized to view Patient data"));
				throw new AuthorizationException("User Not Authorized to view Patient data");
			}
			
			TeamAuthModule teamAuthModule = new TeamAuthModule(callingUser.getUserId(), studyPK);
			int studyStatusPrivileges = teamAuthModule.getStudyStatusPrivileges();
			
			if (!TeamAuthModule.hasViewPermission(studyStatusPrivileges)){
				if (logger.isDebugEnabled()) logger.debug("user does not have view permission to view study Status");
			
				addIssue(
						new Issue(IssueTypes.STUDY_TEAM_AUTHORIZATION, 
								"User does not have view permission to view Study Status"));
				throw new AuthorizationException("User does not have view permission to view Study Status");
			}			
			//PatientScheduleDAO call for PatientSchedule with primary info
			Map patScheduleMap = PatientScheduleDAO.getPatientSchedule(personPK, studyPK);
			ArrayList<PatientSchedule> lstPatSchedule= (ArrayList<PatientSchedule>) patScheduleMap.get("lstPatientSchedule");
			ArrayList<Integer> lstPatProtPK = (ArrayList<Integer>) patScheduleMap.get("lstPatProtPK");
			//Virendra: Fixed#6059, throw exception for null schedule
			if (lstPatSchedule == null || lstPatSchedule.isEmpty() ){
				addIssue(new Issue(IssueTypes.PATIENT_SCHEDULE_NOT_FOUND, "Patient schedule not found"));
				throw new OperationException("Patient schedule not found");
			}
			
			
			PatientScheduleDAO patScheduleDAO = new PatientScheduleDAO();
			//Calling PatientScheduleDAO for Visits and Events
			if(lstPatProtPK != null){
			for(int i= 0;i< lstPatProtPK.size(); i++){
				PatientSchedule patSchedule = lstPatSchedule.get(i);
				
				ArrayList<Integer> lstVisitPK = (ArrayList) PatientScheduleDAO.getVisitPkPatProtPk(personPK, studyPK, lstPatProtPK.get(i));
				ArrayList<Visit> lstVisit = new ArrayList<Visit>();
				for(Integer visitPK:lstVisitPK ){
					Visit patVisit = patScheduleDAO.getVisitsEvents(visitPK, 
							lstPatProtPK.get(i),
							EJBUtil.stringToInteger(callingUser.getUserAccountId()) );
					lstVisit.add(patVisit);
				}
				patSchedule.setVisits(lstVisit);
				//lstPatSchedule.add(patSchedule);
				}
			}
			
			//HashMap visitEventMap = PatientScheduleDAO.getVisitPkPatProtPk(personPK, studyPK, patProtPK);
			
			//ArrayList<Integer> lstVisitPK = (ArrayList) visitEventMap.get("VisitPk");
			
			
			//ArrayList<Integer> lstPatProtPK1 = (ArrayList) visitEventMap.get("PatProtPk");
			
			
			//Getting Visits on list of VisitPk
			
			return lstPatSchedule;
			
		}
		catch(OperationException e){
			sessionContext.setRollbackOnly();
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", e);
			e.setIssues(response.getIssues());
			throw e;
		}
		catch(Throwable t){
			this.addUnknownThrowableIssue(t);
			if (logger.isDebugEnabled()) logger.debug("PatientDemoServiceImpl create", t);
			throw new OperationException(t);
		}

	}
		
}