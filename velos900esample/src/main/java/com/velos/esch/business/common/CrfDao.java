/*
 Classname	CrfDao.class
 
 Version information 	1.0
 
 Date	02/21/2001
 
 Copyright notice		Velos, Inc.
 
 Author 		Sajal
 */

package com.velos.esch.business.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.velos.esch.service.util.Rlog;
import com.velos.eres.service.util.StringUtil;

public class CrfDao extends CommonDAO implements java.io.Serializable {
    private ArrayList crfIds;

    private ArrayList crfEvents1Ids;

    private ArrayList crfNumbers;

    private ArrayList crfNames;

    private ArrayList crfPatProtIds;

    private ArrayList crfFlags;

    private ArrayList crfDelFlags;

    private ArrayList crfformEntryChars;

    private ArrayList crfLinkedFormIds;

    private ArrayList crfformSavedCounts;
    
    private ArrayList formStatus;
    
    private ArrayList linkedFormRecordTypes;

    private int cRows;
    
    /*
     * Array list for form name
     */

    private ArrayList formName;

    /*
     * Array list for Linked Form ID (primary key)
     */

    private ArrayList formId;

    /*
    * Array list for otherLinks
    */
   
    private ArrayList otherLinks;
   
   /*
    * Array list for crfFormType
    */
   
   private ArrayList crfFormType;
  
   private String[] saFormNames;
   
   private String[] saOtherLinks;
   
   private String[] saCrfFormType;
   
   /*
    * Array list for displayinspec
    */
   
    private ArrayList displayInSpecs;
   
   
   
   public CrfDao() {
        crfIds = new ArrayList();
        crfEvents1Ids = new ArrayList();
        crfNumbers = new ArrayList();
        crfNames = new ArrayList();
        crfPatProtIds = new ArrayList();
        crfFlags = new ArrayList();
        crfDelFlags = new ArrayList();
        crfformEntryChars = new ArrayList();
        crfLinkedFormIds = new ArrayList();
        crfformSavedCounts = new ArrayList();
        formId = new ArrayList();
        formName = new ArrayList();   
        otherLinks=new ArrayList();
        crfFormType=new ArrayList();
        formStatus = new ArrayList();
        linkedFormRecordTypes = new ArrayList();
        displayInSpecs = new ArrayList();
        
    }

    public void resetDao() {
        crfIds.clear();
        crfEvents1Ids.clear();
        crfNumbers.clear();
        crfNames.clear();
        crfPatProtIds.clear();
        crfFlags.clear();
        crfDelFlags.clear();
        crfformEntryChars.clear();
        crfLinkedFormIds.clear();
        crfformSavedCounts.clear();
        formStatus.clear();
        linkedFormRecordTypes.clear();
        displayInSpecs.clear();
        
    }

    // Getter and Setter methods

    public ArrayList getFormStatus() {
		return formStatus;
	}

	public void setFormStatus(ArrayList formStatus) {
		this.formStatus = formStatus;
	}
	
	public void setFormStatus(String formStatus) {
		this.formStatus.add(formStatus);
	}
	
	

	public ArrayList getCrfIds() {
        return this.crfIds;
    }

    public void setCrfIds(ArrayList crfIds) {
        this.crfIds = crfIds;
    }

    public ArrayList getCrfEvents1Ids() {
        return this.crfEvents1Ids;
    }

    public void setCrfEvents1Ids(ArrayList crfEvents1Ids) {
        this.crfEvents1Ids = crfEvents1Ids;
    }

    public ArrayList getCrfNumbers() {
        return this.crfNumbers;
    }

    public void setCrfNumbers(ArrayList crfNumbers) {
        this.crfNumbers = crfNumbers;
    }

    public ArrayList getCrfNames() {
        return this.crfNames;
    }

    public void setCrfNames(ArrayList crfNames) {
        this.crfNames = crfNames;
    }

    public ArrayList getCrfPatProtIds() {
        return this.crfPatProtIds;
    }

    public void setCrfPatProtIds(ArrayList crfPatProtIds) {
        this.crfPatProtIds = crfPatProtIds;
    }

    public ArrayList getCrfFlags() {
        return this.crfFlags;
    }

    public void setCrfFlags(ArrayList crfFlags) {
        this.crfFlags = crfFlags;
    }

    public ArrayList getCrfDelFlags() {
        return this.crfDelFlags;
    }

    public void setCrfDelFlags(ArrayList crfDelFlags) {
        this.crfDelFlags = crfDelFlags;
    }

    public int getCRows() {
        return this.cRows;
    }

    public void setCRows(int cRows) {
        this.cRows = cRows;
    }

    public void setCrfIds(Integer crfId) {
        crfIds.add(crfId);
    }

    public void setCrfEvents1Ids(String crfEvents1Id) {
        crfEvents1Ids.add(crfEvents1Id);
    }

    public void setCrfNumbers(String crfNumber) {
        crfNumbers.add(crfNumber);
    }

    public void setCrfNames(String crfName) {
        crfNames.add(crfName);
    }

    public void setCrfPatProtIds(String crfPatProtId) {
        crfPatProtIds.add(crfPatProtId);
    }

    public void setCrfFlags(String crfFlag) {
        crfFlags.add(crfFlag);
    }

    public void setCrfDelFlags(String crfDelFlag) {
        crfDelFlags.add(crfDelFlag);
    }

    public ArrayList getCrfLinkedFormIds() {
        return this.crfLinkedFormIds;
    }

    public void setCrfLinkedFormIds(ArrayList crfLinkedFormIds) {
        this.crfLinkedFormIds = crfLinkedFormIds;
    }

    public void setCrfLinkedFormIds(String crfLinkedFormId) {
        this.crfLinkedFormIds.add(crfLinkedFormId);
    }

    public ArrayList getCrfformEntryChars() {
        return this.crfformEntryChars;
    }

    public void setCrfformEntryChars(ArrayList crfformEntryChars) {
        this.crfformEntryChars = crfformEntryChars;
    }

    public void setCrfformEntryChars(String crfformEntryChar) {
        this.crfformEntryChars.add(crfformEntryChar);
    }

    public ArrayList getCrfformSavedCounts() {
        return this.crfformSavedCounts;
    }

    public void setCrfformSavedCounts(ArrayList crfformSavedCounts) {
        this.crfformSavedCounts = crfformSavedCounts;
    }

    public void setCrfformSavedCounts(String crfformSavedCount) {
        this.crfformSavedCounts.add(crfformSavedCount);
    }
    
    public ArrayList getFormId() {
        return this.formId;
    }
   
    public void setFormId(Integer formId) {
        this.formId.add(formId);
    }
    
    public void setFormName(String[] saFormNames) {
        this.saFormNames = saFormNames;
    }

    public ArrayList getFormName() {
        return this.formName;
    }

    public void setFormName(ArrayList formName) {
        this.formName = formName;
    }
    
    
    public void setFormName(String formName) {
        this.formName.add(StringUtil.escapeSpecialCharHTML(formName));
    }
    
    public void setOtherLinks(String[] saOtherLinks){
    	this.saOtherLinks = saOtherLinks;
    }
    
    public void setCrfFormType(String[] saCrfFormType){
    	this.saCrfFormType = saCrfFormType;
    }
    
    public ArrayList getOtherLinks(){
         return this.otherLinks=otherLinks;	
    }
    
    public void setOtherLinks(ArrayList otherLinks){
           this.otherLinks = otherLinks;	
    }
    
    
    public ArrayList getCrfFormType(){
        return this.crfFormType=crfFormType;	
   }
   
   public void setCrfFormType(ArrayList crfFormType){
          this.crfFormType = crfFormType;	
   }
   
   public void setOtherLinks(String otherLinks) {
       this.otherLinks.add(StringUtil.escapeSpecialCharHTML(otherLinks));
   }
   
   public void setCrfFormType(String crfFormType) {
       this.crfFormType.add(StringUtil.escapeSpecialCharHTML(crfFormType));
   }
    
   

    // end of getter and setter methods

    /**
     * Gets all CRFs associated to all the events of a PtProtId
     * 
     * @param patProtId
     */

    public void getCrfValues(int patProtId) {
        int rows = 0;
        PreparedStatement pstmt = null;
        Connection conn = null;
        String sqlStr = "";
        try {
            conn = getConnection();
            sqlStr = "select b.pk_crf, " + "b.crf_number " + "from sch_crf b "
                    + "where b.fk_patprot = ? " + "and b.crf_flag = 'E' "
                    + "and b.crf_delflag <> 'Y' "
                    + "and pk_crf = (select min(pk_crf) " + "from sch_crf a "
                    + "where a.fk_patprot = ? "
                    + "and a.crf_number = b.crf_number "
                    + "and a.crf_flag = 'E' " + "and a.crf_delflag <> 'Y') "
                    + "order by b.crf_number ";
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, patProtId);
            pstmt.setInt(2, patProtId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                setCrfIds(new Integer(rs.getInt("pk_crf")));
                setCrfNumbers(rs.getString("crf_number"));
                rows++;
            }

            setCRows(rows);
        } catch (SQLException ex) {
            Rlog.fatal("crf",
                    "CrfStatDao.getCrfStatValues EXCEPTION IN FETCHING FROM CrfStat table"
                            + ex);
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (Exception e) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (Exception e) {
            }
        }

    }
  
   //Added by Gopu for May-June Enhancement(#F1)  
    /**
     * Delete Event CRF 
     * 
     * @param eventId
     */
    
  
  public int deleteEventCrf(int eventId){ //Modified for INF-18183.
	int ret=0;
  	PreparedStatement pstmt = null;
    Connection conn = null;
    String sql = "";

      try {
          conn = getConnection();

          sql = "delete from esch.sch_event_crf where fk_event=?"; 

          pstmt = conn.prepareStatement(sql);
          pstmt.setInt(1, eventId);
          pstmt.executeQuery();
      } catch (SQLException ex) {
    	  ret = -1;
          Rlog.fatal("crfDao",
                  "deleteEventCrf.deleteEventCrf EXCEPTION"
                          + ex);
      } finally {
          try {
              if (pstmt != null)
            	  pstmt.close();
          } catch (Exception e) {
          }
          try {
              if (conn != null)
                  conn.close();
          } catch (Exception e) {
          }

      }
      return ret;
 	
  }
  //Added by Gopu for May-June Enhancement(#F1)  
  /**
   * Get all Event CRF Forms 
   * 
   * @param eventId
   * 
   * @param dispType
   * 
   */
  
  
  public void getEventCrfForms(int eventId,String dispType){
	  PreparedStatement pstmt = null;
      Connection conn = null;
      String sql = "";
      try {
          conn = getConnection();

          if (!(dispType.equals("link"))) {          
	          sql = "select a.form_name,a.pk_formlib, c.form_type ,   lf_display_inspec as lf_display_inspec,lf_entrychar from eres.er_formlib a, esch.sch_event_crf  c,er_linkedforms ff where a.pk_formlib= c.fk_form and c.form_type in ('PS', 'SP','PR') and c.fk_event=? and  ff.fk_formlib = C.fk_form order by c.form_type desc, lower(a.form_name)"; 	  
	          pstmt = conn.prepareStatement(sql);
	          pstmt.setInt(1, eventId);
          } else {
	          sql="select other_links,form_type from esch.sch_event_crf where fk_event=? and form_type not in('SP','PS','PR')";	
	          pstmt = conn.prepareStatement(sql);
	          pstmt.setInt(1, eventId);                      
          }                      
          ResultSet rs = pstmt.executeQuery();
          if (!(dispType.equals("link")) ){
            while (rs.next()) {
               setFormName(rs.getString("FORM_NAME"));
               setFormId(new Integer(rs.getInt("pk_formlib")));
               setCrfFormType(rs.getString("FORM_TYPE"));
               setDisplayInSpecs(rs.getString("lf_display_inspec"));
               setCrfformEntryChars(rs.getString("lf_entrychar"));
            }
          }else{            
          while (rs.next()) {
        	  setOtherLinks(rs.getString("OTHER_LINKS"));
        	  setCrfFormType(rs.getString("FORM_TYPE"));
           }
         } 
      } catch (SQLException ex) {
          Rlog.fatal("crfDao","CrfDao EXCEPTION"+ ex);
      } finally {
          try {
              if (pstmt != null)
                  pstmt.close();
          } catch (Exception e) {
          }
          try {
              if (conn != null)
                  conn.close();
          } catch (Exception e) {
          }

      }
   }
  
  /** Returns 'Visit Mark Done' prompt Flag = 1 if all the CRfs are filled for the scheduled event record 
   *  and if the event has no marked
   * status. It will return 0 if the event already has a status. 
   * @param schEventId PK to identify scheduled event (sch_events1 pk)
   * @return 1 if application should prompt for marking 'events done', 0 - for no prompts
   */
  public int getEventMarkDoneFlagWhenAllCRFsFilled(int schEventId){
	  PreparedStatement pstmt = null;
      Connection conn = null;
      String sql = "";
      int flag = 0;
      try {
          conn = getConnection();

           
	          sql="select pkg_gensch.f_getCRFEventMarkDoneFlag(?) as cflag from dual";	
	          pstmt = conn.prepareStatement(sql);
	          pstmt.setInt(1, schEventId);                      
                                
          ResultSet rs = pstmt.executeQuery();
          
            while (rs.next()) {
            	flag =   rs.getInt("cflag");
            }
            
      	  if (pstmt != null)
              pstmt.close();
    
      	  if (conn != null)
            conn.close();
    
      	  
           return flag;
           
      } catch (SQLException ex) {
          Rlog.fatal("crfDao","CrfDao EXCEPTION in getEventMarkDoneFlagWhenAllCRFsFilled()"+ ex);
          
          return 0;
          
      } finally{
    	  try {
     	  if (pstmt != null)
              pstmt.close();
     
    	  }
    	  catch (SQLException ex) {
              Rlog.fatal("crfDao","CrfDao EXCEPTION in getEventMarkDoneFlagWhenAllCRFsFilled()"+ ex);
              
              return 0;
              
          } 
    	  
    	  try {
          	  if (conn != null)
                conn.close();
        	  }
        	  catch (SQLException ex) {
                  Rlog.fatal("crfDao","CrfDao EXCEPTION in getEventMarkDoneFlagWhenAllCRFsFilled()"+ ex);
                  return 0;
              } 
      } 
      
   }

		public ArrayList getLinkedFormRecordTypes() {
			return linkedFormRecordTypes;
		}
		
		public void setLinkedFormRecordTypes(ArrayList linkedFormRecordTypes) {
			this.linkedFormRecordTypes = linkedFormRecordTypes;
		}
		
		public void setLinkedFormRecordTypes(String linkedFormRecordType) {
			this.linkedFormRecordTypes.add(linkedFormRecordType);
		}

		public ArrayList getDisplayInSpecs() {
			return displayInSpecs;
		}

		public void setDisplayInSpecs(ArrayList displayInSpecs) {
			this.displayInSpecs = displayInSpecs;
		}
		
		public void setDisplayInSpecs(String displayInSpec) {
			this.displayInSpecs.add(displayInSpec);
		}


}
