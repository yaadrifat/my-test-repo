/* 
 * Classname			BgtApndxBean.class
 * 
 * Version information
 *
 * Date					03/26/2002
 *
 * Author 				Sajal
 * Copyright notice
 */

package com.velos.esch.business.bgtApndx.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/**
 * The Budget Appendix CMP entity bean.<br>
 * <br>
 * 
 * @author Sajal
 */
@Entity
@Table(name = "SCH_BGTAPNDX")
public class BgtApndxBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3546923597613511223L;

    /**
     * the budget appendix Id
     */
    public int bgtApndxId;

    /**
     * the Budget
     */
    public Integer bgtApndxBudget;

    /**
     * the budget appendix description
     */
    public String bgtApndxDesc;

    /**
     * the budget appendix URI
     */
    public String bgtApndxUri;

    /**
     * the budget appendix type
     */

    public String bgtApndxType;

    /*
     * creator
     */
    public Integer creator;

    /*
     * last modified by
     */
    public Integer modifiedBy;

    /*
     * IP Address
     */
    public String ipAdd;

    // GETTER SETTER METHODS
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_BGTAPNDX", allocationSize=1)
    @Column(name = "PK_BGTAPNDX")
    public int getBgtApndxId() {
        return this.bgtApndxId;
    }

    public void setBgtApndxId(int bgtApndxId) {
        this.bgtApndxId = bgtApndxId;
    }

    @Column(name = "FK_BUDGET")
    public String getBgtApndxBudget() {
        return StringUtil.integerToString(this.bgtApndxBudget);
    }

    public void setBgtApndxBudget(String bgtApndxBudget) {
        if (bgtApndxBudget != null) {
            this.bgtApndxBudget = Integer.valueOf(bgtApndxBudget);
        }
    }

    @Column(name = "BGTAPNDX_DESC")
    public String getBgtApndxDesc() {
        return this.bgtApndxDesc;
    }

    public void setBgtApndxDesc(String bgtApndxDesc) {
        this.bgtApndxDesc = bgtApndxDesc;
    }

    @Column(name = "BGTAPNDX_URI")
    public String getBgtApndxUri() {
        return this.bgtApndxUri;
    }

    public void setBgtApndxUri(String bgtApndxUri) {
        this.bgtApndxUri = bgtApndxUri;
    }

    @Column(name = "BGTAPNDX_TYPE")
    public String getBgtApndxType() {
        return this.bgtApndxType;
    }

    public void setBgtApndxType(String bgtApndxType) {
        this.bgtApndxType = bgtApndxType;
    }

    /**
     * @return The Id of the creator of the record
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * @param creator
     *            The Id of the user who is inserting this record
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * @return The Id of the user modifying this record
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * @param modifiedBy
     *            The Id of the user modifying this record
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * @return IP Address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The IP Address of the client machine
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /*
     * * returns the state keeper object associated with this budget appendix
     */
    /*
     * public BgtApndxStateKeeper getBgtApndxStateKeeper() {
     * Rlog.debug("BgtApndx", "GET BgtApndx KEEPER"); return new
     * BgtApndxStateKeeper(getBgtApndxId(), getBgtApndxBudget(),
     * getBgtApndxDesc(), getBgtApndxUri(), getBgtApndxType(), getCreator(),
     * getModifiedBy(), getIpAdd()); }
     */

    /**
     * sets up a state keeper containing details of the budget appendix
     */
    /*
     * public void setBgtApndxStateKeeper(BgtApndxStateKeeper bask) {
     * Rlog.debug("BgtApndx", "IN BgtApndx STATE HOLDER"); GenerateId genId =
     * null; try { Connection conn = null; conn = getConnection();
     * 
     * Rlog.debug("BgtApndx", "Connection :" + conn); bgtApndxId =
     * genId.getId("SEQ_SCH_BGTAPNDX", conn); conn.close();
     * 
     * setBgtApndxBudget(bask.getBgtApndxBudget()); Rlog.debug("BgtApndx",
     * "bgtApndxBudget '" + bask.getBgtApndxBudget() + "'");
     * setBgtApndxDesc(bask.getBgtApndxDesc()); Rlog.debug("BgtApndx",
     * "bgtApndxDesc '" + bask.getBgtApndxDesc() + "'");
     * setBgtApndxUri(bask.getBgtApndxUri()); Rlog.debug("BgtApndx",
     * "bgtApndxUri '" + bask.getBgtApndxUri() + "'");
     * setBgtApndxType(bask.getBgtApndxType()); Rlog.debug("BgtApndx",
     * "bgtApndxType'" + bask.getBgtApndxType() + "'");
     * setCreator(bask.getCreator()); Rlog.debug("BgtApndx", "etCreator '" +
     * bask.getCreator() + "'"); setModifiedBy(bask.getModifiedBy());
     * Rlog.debug("BgtApndx", "etModifiedBy '" + bask.getModifiedBy() + "'");
     * setIpAdd(bask.getIpAdd()); Rlog.debug("BgtApndx", "etIpAdd '" +
     * bask.getIpAdd() + "'"); } catch (Exception e) { Rlog.fatal("BgtApndx",
     * "EXCEPTION IN CREATING NEW BUDGET APPENDIX" + e); } }
     */

    public int updateBgtApndx(BgtApndxBean bask) {

        try {
            setBgtApndxBudget(bask.getBgtApndxBudget());
            setBgtApndxDesc(bask.getBgtApndxDesc());
            setBgtApndxUri(bask.getBgtApndxUri());
            setBgtApndxType(bask.getBgtApndxType());
            setCreator(bask.getCreator());
            setModifiedBy(bask.getModifiedBy());
            setIpAdd(bask.getIpAdd());
            return 0;
        } catch (Exception e) {
            Rlog.fatal("BgtApndx", " error BgtApndxBean.update");
            return -2;
        }
    }

    public BgtApndxBean() {

    }

    public BgtApndxBean(int bgtApndxId, String bgtApndxBudget,
            String bgtApndxDesc, String bgtApndxUri, String bgtApndxType,
            String creator, String modifiedBy, String ipAdd) {
        super();
        // TODO Auto-generated constructor stub
        setBgtApndxId(bgtApndxId);
        setBgtApndxBudget(bgtApndxBudget);
        setBgtApndxDesc(bgtApndxDesc);
        setBgtApndxUri(bgtApndxUri);
        setBgtApndxType(bgtApndxType);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
    }

}// end of class

