/*
 * Classname : AdvEveBean
 *
 * Version information: 1.0
 *
 * Date: 11/28/2001
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.business.advEve.impl;

/* Import Statements */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.velos.eres.service.util.StringUtil;
//import com.velos.esch.service.util.DateUtil;-KM
import com.velos.eres.service.util.DateUtil;

import com.velos.esch.service.util.Rlog;

// import java.sql.*;

/* End of Import Statements */

/**
 * CMP entity bean for Adverse Event
 *
 * <p>
 * In eResearch Architecture, the class resides at the Business layer.
 * </p>
 *
 * @author Arvind Kumar
 * @version %I%, %G%
 */
/*
 * *************************History********************************* Modified by
 * :Sonia Modified On:06/11/2004 Comment: 1. Added new attribute formStatus and
 * respective getter/setters 2. Modified methods for the new attribute 3.
 * Modified JavaDoc comments
 * *************************END****************************************
 */
@Entity
@Table(name = "SCH_ADVERSEVE")
public class AdvEveBean implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 3258413941079029558L;

    /**
     * adverse event id
     */
    public int advEveId;

    /**
     * patient event id
     */
    public String advEveEvents1Id;

    /**
     * type
     */
    public Integer advEveCodelstAeTypeId;


    /**
     * MedDRA code
     */

    public String advEveMedDRA;

    /**
     * Adverse Event Dictionary
     */

    public String advEveDictionary;

    /**
     * description
     */
    public String advEveDesc;

    /**
     * start date
     */
    public Date advEveStDate;

    /**
     * end date
     */
    public Date advEveEndDate;

    /**
     * entered by
     */
    public Integer advEveEnterBy;


    /**
     * reported by
     */
    public Integer advEveReportedBy;

    /**
     * linked To
     */
    public Integer advEveLinkedTo;



    /**
     * outcome type
     */
    public String advEveOutType;

    /**
     * outcome date
     */
    public Date advEveOutDate;


    /**
     * discovery  date
     */
    public Date advEveDiscoveryDate;


    /**
     * logged  date
     */
    public Date advEveLoggedDate;


    /**
     * event outcome notes
     */
    public String advEveOutNotes;

    /**
     * additional information
     */
    public String advEveAddInfo;

    /**
     * notes
     */
    public String advEveNotes;

    /**
     * study
     */
    public Integer fkStudy;

    /**
     * patient
     */
    public Integer fkPer;

    /**
     * severity
     */
    public Integer advEveSeverity;

    /**
     * body system affected
     */
    public Integer advEveBdsystemAff;

    /**
     * relationship
     */
    public Integer advEveRelationship;

    /**
     * recovery description
     */
    public Integer advEveRecoveryDesc;

    /**
     * creator
     */
    public Integer creator;

    /**
     * modified By
     */
    public Integer modifiedBy;

    /**
     * IP Address
     */
    public String ipAdd;

    /**
     * Toxicity Grade
     */
    public Integer advEveGrade;

    /**
     * Adverse Event Name
     */
    public String advEveName;

    /**
     * associated lookup (CDE)
     */
    public Integer advEveLKPId;

    /**
     * adverse event treatment course
     */
    public String advEveTreatment;

    /** adverse event form status */
    public Integer formStatus;

    /** adverse event added by Gopu dated on 01/20/05 */
    public Integer fkOutcomeAction;

    // GETTER SETTER METHODS

    /**
     * Returns an id for the adverse event
     *
     * @return the adverse event id
     */
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_GEN") 
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "SEQ_SCH_ADVERSEVE", allocationSize=1)
    @Column(name = "PK_ADVEVE")
    public int getAdvEveId() {
        return this.advEveId;
    }

    /**
     * Sets the id for the adverse event
     *
     * @param advEveId
     *            the adverse event id
     */

    public void setAdvEveId(int advEveId) {
        this.advEveId = advEveId;
    }

    /**
     * Gets the id for the associated patient event
     *
     * @return event id
     * @deprecated patient events are no longer associated with an adverse event
     */
    @Column(name = "FK_EVENTS1")
    public String getAdvEveEvents1Id() {
        return this.advEveEvents1Id;
    }

    /**
     * Sets the id for the associated patient event
     *
     * @param advEveEvents1Id
     *            id for the associated patient event
     * @deprecated patient events are no longer associated with an adverse event
     */
    public void setAdvEveEvents1Id(String advEveEvents1Id) {
        this.advEveEvents1Id = advEveEvents1Id;
    }

    /**
     * Gets the id for the adverse event's type
     *
     * @return adverse event type id
     */
    @Column(name = "FK_CODLST_AETYPE")
    public String getAdvEveCodelstAeTypeId() {
        return StringUtil.integerToString(this.advEveCodelstAeTypeId);
    }

    /**
     * Sets the id for the adverse event's type
     *
     * @param advEveCodelstAeTypeId
     *            adverse event type id
     */
    public void setAdvEveCodelstAeTypeId(String advEveCodelstAeTypeId) {
        if (advEveCodelstAeTypeId != null) {
            this.advEveCodelstAeTypeId = Integer.valueOf(advEveCodelstAeTypeId);
        }
    }

    //KM: added 05/10/06 May - June requirement PS1
    /**
     * Gets the MedDRA code
     *
     * @return MedDra code
     */

    @Column(name="MedDRA")
    public String getAdvEveMedDRA(){
    	return this.advEveMedDRA;
    }


    /**
     * Sets the MedDra code
     *
     * @param advEveMedDRA
     *           MedDRA code
     */

    public void setAdvEveMedDRA(String advEveMedDRA){
    	this.advEveMedDRA=advEveMedDRA;
    }

    //KM: added 05/10/06 May - June requirement PS1

    /** Gets the adverse event's dictionary
     *
     * @return adverse event's dictionary
     */

    @Column(name="DICTIONARY")
    public String getAdvEveDictionary(){
    	return this.advEveDictionary;
    }

    /**
     * Sets the adverse event's dictionary
     * @param advEveDictionary
     * 			adverse event's dictionary
     */

    public void setAdvEveDictionary(String advEveDictionary) {
    	this.advEveDictionary = advEveDictionary;
    }

    /**
     * Gets the adverse event's description text
     *
     * @return adverse event's description text
     */
    @Column(name = "AE_DESC")
    public String getAdvEveDesc() {
        return this.advEveDesc;
    }

    /**
     * Sets the adverse event's description text
     *
     * @param advEveDesc
     *            adverse event's description text
     */

    public void setAdvEveDesc(String advEveDesc) {
        this.advEveDesc = advEveDesc;
    }

    /**
     * Gets the adverse event's start date
     *
     * @return adverse event's start date
     */

    @Column(name = "AE_STDATE")
    public Date getAdvEveStDt() {
        return this.advEveStDate;
    }

    /**
     * Sets the adverse event's start date
     *
     * @param advEveStDate
     *            adverse event's start date
     */

    public void setAdvEveStDt(Date advEveStDate) {
        this.advEveStDate = advEveStDate;

    }




    @Transient
    public String getAdvEveStDate() {
        return DateUtil.dateToString(getAdvEveStDt());
    }

    /**
     * Sets the adverse event's start date
     *
     * @param advEveStDate
     *            adverse event's start date
     */

    public void setAdvEveStDate(String advEveStDate) {
        setAdvEveStDt(DateUtil.stringToDate(advEveStDate, null));

    }



    @Column(name = "AE_DISCVRYDATE")
    public Date getAdvEveDiscoveryDt() {
        return this.advEveDiscoveryDate;
    }

    /**
     * Sets the adverse event's start date
     *
     * @param advEveStDate
     *            adverse event's start date
     */

    public void setAdvEveDiscoveryDt(Date advEveDiscoveryDate) {
        this.advEveDiscoveryDate = advEveDiscoveryDate;

    }


    @Transient
    public String getAdvEveDiscoveryDate() {
        return DateUtil.dateToString(getAdvEveDiscoveryDt());
    }

    /**
     * Sets the adverse event's start date
     *
     * @param advEveStDate
     *            adverse event's start date
     */

    public void setAdvEveDiscoveryDate(String advEveDiscoveryDate) {

    	setAdvEveDiscoveryDt(DateUtil.stringToDate(advEveDiscoveryDate, null));

    }
    @Column(name = "AE_LOGGEDDATE")
    public Date getAdvEveLoggedDt() {
        return this.advEveLoggedDate;
    }

    /**
     * Sets the adverse event's start date
     *
     * @param advEveStDate
     *            adverse event's start date
     */

    public void setAdvEveLoggedDt(Date advEveLoggedDate) {
    	this.advEveLoggedDate = advEveLoggedDate;
    }

    @Transient
    public String getAdvEveLoggedDate() {
        return DateUtil.dateToString(getAdvEveLoggedDt());
    }

    /**
     * Sets the adverse event's start date
     *
     * @param advEveStDate
     *            adverse event's start date
     */

    public void setAdvEveLoggedDate(String advEveLoggedDate) {
       setAdvEveLoggedDt(DateUtil.stringToDate(advEveLoggedDate, null));

    }
    /**
     * Gets the adverse event's end date
     *
     * @return adverse event's end date
     */
    @Column(name = "AE_ENDDATE")
    public Date getAdvEveEndDt() {
        return this.advEveEndDate;
    }

    /**
     * Sets the adverse event's end date
     *
     * @param advEveEndDate
     *            adverse event's end date
     */

    public void setAdvEveEndDt(Date advEveEndDate) {
        this.advEveEndDate = advEveEndDate;

    }

    @Transient
    public String getAdvEveEndDate() {
        return DateUtil.dateToString(getAdvEveEndDt());
    }

    /**
     * Sets the adverse event's end date
     *
     * @param advEveEndDate
     *            adverse event's end date
     */

    public void setAdvEveEndDate(String advEveEndDate) {
        setAdvEveEndDt(DateUtil.stringToDate(advEveEndDate, null));

    }

    /**
     * Gets the user who entered the adverse event
     *
     * @return user who entered the adverse event
     */
    @Column(name = "AE_ENTERBY")
    public String getAdvEveEnterBy() {
        return StringUtil.integerToString(this.advEveEnterBy);
        // return this.advEveEnterBy;
    }

    /**
     * Sets the user who entered the adverse event
     *
     * @param advEveEnterBy
     *            user who entered the adverse event
     */

    public void setAdvEveEnterBy(String advEveEnterBy) {

        if (advEveEnterBy != null) {
            this.advEveEnterBy = Integer.valueOf(advEveEnterBy);
        }

        // this.advEveEnterBy = advEveEnterBy;
    }
    /**
     * Gets the user who entered the adverse event
     *
     * @return user who entered the adverse event
     */
    @Column(name = "AE_REPORTEDBY")
    public String getAdvEveReportedBy() {

        return StringUtil.integerToString(this.advEveReportedBy);

        //
    }

    /**
     * Sets the user who entered the adverse event
     *
     * @param advEveEnterBy
     *            user who entered the adverse event
     */

    public void setAdvEveReportedBy(String advEveReportedBy) {

    	if(advEveReportedBy != null){

             this.advEveReportedBy = StringUtil.stringToInteger(advEveReportedBy);
    	}
    	else {
    		 this.advEveReportedBy = null;
    	}



    }
    /**
     * Gets the user who entered the adverse event
     *
     * @return user who entered the adverse event
     */
    @Column(name = "FK_LINK_ADVERSEVE")
    public String getAdvEveLinkedTo() {

        return StringUtil.integerToString(this.advEveLinkedTo);

        //
    }

    /**
     * Sets the user who entered the adverse event
     *
     * @param advEveEnterBy
     *            user who entered the adverse event
     */

    public void setAdvEveLinkedTo(String advEveLinkedTo) {

    	if(advEveLinkedTo != null){

             this.advEveLinkedTo = StringUtil.stringToInteger(advEveLinkedTo);
    	}
    	else {
    		 this.advEveLinkedTo = null;
    	}



    }
    /**
     * Gets the adverse event outcome type
     *
     * @return outcome type
     */
    @Column(name = "AE_OUTTYPE")
    public String getAdvEveOutType() {
        return this.advEveOutType;
    }

    /**
     * Sets the adverse event outcome type
     *
     * @param advEveOutType
     *            outcome type
     */
    public void setAdvEveOutType(String advEveOutType) {
        this.advEveOutType = advEveOutType;

    }

    /**
     * Gets the adverse event outcome date
     *
     * @return outcome date
     */
    @Column(name = "AE_OUTDATE")
    public Date getAdvEveOutDt() {
        return this.advEveOutDate;
    }

    /**
     * Sets the adverse event outcome date
     *
     * @param advEveOutDate
     *            outcome date
     */
    public void setAdvEveOutDt(Date advEveOutDate) {
        this.advEveOutDate = advEveOutDate;

    }

    @Transient
    public String getAdvEveOutDate() {
        return DateUtil.dateToString(getAdvEveOutDt());
    }

    /**
     * Sets the adverse event outcome date
     *
     * @param advEveOutDate
     *            outcome date
     */
    public void setAdvEveOutDate(String advEveOutDate) {
        setAdvEveOutDt(DateUtil.stringToDate(advEveOutDate, null));

    }

    /**
     * Gets the adverse event outcome notes
     *
     * @return outcome notes
     */
    @Column(name = "AE_OUTNOTES")
    public String getAdvEveOutNotes() {
        return this.advEveOutNotes;
    }

    /**
     * Sets the adverse event outcome notes
     *
     * @param advEveOutNotes
     *            outcome notes
     */

    public void setAdvEveOutNotes(String advEveOutNotes) {
        this.advEveOutNotes = advEveOutNotes;
    }

    /**
     * Gets the adverse event additional information code
     *
     * @return additional information code
     */
    @Column(name = "AE_ADDINFO")
    public String getAdvEveAddInfo() {
        return this.advEveAddInfo;
    }

    /**
     * Sets the adverse event additional information code
     *
     * @param advEveAddInfo
     *            additional information code
     */

    public void setAdvEveAddInfo(String advEveAddInfo) {
        this.advEveAddInfo = advEveAddInfo;
    }

    /**
     * Gets the adverse event notes
     *
     * @return notes
     */
    @Column(name = "AE_NOTES")
    public String getAdvEveNotes() {
        return this.advEveNotes;
    }

    /**
     * Sets the adverse event notes
     *
     * @param advEveNotes
     *            notes
     */

    public void setAdvEveNotes(String advEveNotes) {
        this.advEveNotes = advEveNotes;
    }

    /**
     * Gets patient's study on which the adverse event occurred
     *
     * @return study
     */
    @Column(name = "FK_STUDY")
    public String getFkStudy() {
        return StringUtil.integerToString(this.fkStudy);
    }

    /**
     * Sets patient's study on which the adverse event occurred
     *
     * @param fkStudy
     *            study
     */

    public void setFkStudy(String fkStudy) {
        if (fkStudy != null) {
            this.fkStudy = Integer.valueOf(fkStudy);
        }
    }

    /**
     * Gets patient
     *
     * @return patient
     */
    @Column(name = "FK_PER")
    public String getFkPer() {
        return StringUtil.integerToString(this.fkPer);
    }

    /**
     * Sets patient
     *
     * @param fkPer
     *            patient
     */

    public void setFkPer(String fkPer) {
        if (fkPer != null) {
            this.fkPer = Integer.valueOf(fkPer);
        }
    }

    /**
     * Gets severity
     *
     * @return severity
     */
    @Column(name = "AE_SEVERITY")
    public String getAdvEveSeverity() {
        return StringUtil.integerToString(this.advEveSeverity);
    }

    /**
     * Sets severity
     *
     * @param advEveSeverity
     *            severity
     */

    public void setAdvEveSeverity(String advEveSeverity) {
        Rlog.debug("advEve", "AdvEveBean.setAdvEveSeverity line 1");
        if (advEveSeverity != null && !advEveSeverity.equals("")) {
            Rlog.debug("advEve", "AdvEveBean.setAdvEveSeverity line 2a");
            this.advEveSeverity = Integer.valueOf(advEveSeverity);
        } else {
            Rlog.debug("advEve", "AdvEveBean.setAdvEveSeverity line 2b");
            this.advEveSeverity = null;
        }
    }

    /**
     * Gets body system affected
     *
     * @return body system affected
     * @deprecated body system affected is no longer associated with an adverse
     *             event
     */
    @Column(name = "AE_BDSYSTEM_AFF")
    public String getAdvEveBdsystemAff() {
        return StringUtil.integerToString(this.advEveBdsystemAff);
    }

    /**
     * Sets body system affected
     *
     * @param advEveBdsystemAff
     *            body system affected
     * @deprecated body system affected is no longer associated with an adverse
     *             event
     */

    public void setAdvEveBdsystemAff(String advEveBdsystemAff) {
        if (advEveBdsystemAff != null && !advEveBdsystemAff.equals("")) {
            this.advEveBdsystemAff = Integer.valueOf(advEveBdsystemAff);
        } else {
            this.advEveBdsystemAff = null;
        }

    }

    /**
     * Gets relationship
     *
     * @return relationship
     */
    @Column(name = "AE_RELATIONSHIP")
    public String getAdvEveRelationship() {
        return StringUtil.integerToString(this.advEveRelationship);
    }

    /**
     * Sets relationship
     *
     * @param advEveRelationship
     *            relationship
     */

    public void setAdvEveRelationship(String advEveRelationship) {
        if (!StringUtil.isEmpty(advEveRelationship)) {
            this.advEveRelationship = Integer.valueOf(advEveRelationship);
        } else {
            this.advEveRelationship = null;
        }
    }

    /**
     * Gets recovery description
     *
     * @return recovery description
     */
    @Column(name = "AE_RECOVERY_DESC")
    public String getAdvEveRecoveryDesc() {
        return StringUtil.integerToString(this.advEveRecoveryDesc);
    }

    /**
     * Sets recovery description
     *
     * @param advEveRecoveryDesc
     *            recovery description
     */

    public void setAdvEveRecoveryDesc(String advEveRecoveryDesc) {
        Rlog.debug("advEve", "AdvEveBean.setAdvEveRecoveryDesc line 1");
        if (!StringUtil.isEmpty(advEveRecoveryDesc)) {
            Rlog.debug("advEve", "AdvEveBean.setAdvEveRecoveryDesc line 2");
            this.advEveRecoveryDesc = Integer.valueOf(advEveRecoveryDesc);
        } else {
            this.advEveRecoveryDesc = null;
        }
    }

    /**
     * Gets the user who created the adverse event
     *
     * @return the user who created the adverse event
     */
    @Column(name = "creator")
    public String getCreator() {
        return StringUtil.integerToString(this.creator);
    }

    /**
     * Sets the user who created the adverse event
     *
     * @param creator
     *            the user who created the adverse event
     */
    public void setCreator(String creator) {
        if (creator != null) {
            this.creator = Integer.valueOf(creator);
        }
    }

    /**
     * Gets the user who modified the adverse event
     *
     * @return the user who modified the adverse event
     */
    @Column(name = "last_modified_by")
    public String getModifiedBy() {
        return StringUtil.integerToString(this.modifiedBy);
    }

    /**
     * Sets the user who modified the adverse event
     *
     * @param modifiedBy
     *            the user who modified the adverse event
     */
    public void setModifiedBy(String modifiedBy) {
        if (modifiedBy != null) {
            this.modifiedBy = Integer.valueOf(modifiedBy);
        }
    }

    /**
     * Gets the IP address of the user who created/modified the adverse event
     *
     * @return IP address
     */
    @Column(name = "ip_add")
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * Sets the IP address of the user who created/modified the adverse event
     *
     * @param ipAdd
     *            IP address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * Sets the grade
     *
     * @param advEveGrade
     *            adverse event grade
     */

    public void setAdvEveGrade(String advEveGrade) {
        if (advEveGrade != null) {
            this.advEveGrade = Integer.valueOf(advEveGrade);
        } else {
            this.advEveGrade = null;
        }

    }

    /**
     * Gets the grade
     *
     * @return adverse event grade
     */
    @Column(name = "AE_GRADE")
    public String getAdvEveGrade() {
        return StringUtil.integerToString(this.advEveGrade);
    }

    /**
     * Sets name for the adverse event
     *
     * @param advEveName
     *            adverse event Name
     */

    public void setAdvEveName (String advEveName) {
        this.advEveName = advEveName;
    }

    /**
     * Gets name for the adverse event
     *
     * @return returns the adverse event Name
     */
    @Column(name = "AE_NAME")
    public String getAdvEveName() {
        return this.advEveName;
    }

    /**
     * Associates a CDE Lookup Id with the adverse event
     *
     * @param advEveLKPId
     *            CDE Lookup Id
     */

    public void setAdvEveLKPId(String advEveLKPId) {
        if (advEveLKPId != null) {
            this.advEveLKPId = Integer.valueOf(advEveLKPId);
        } else {
            this.advEveLKPId = new Integer(0);
        }

    }

    /**
     * gets CDE Lookup Id
     *
     * @return CDE Lookup Id
     */
    @Column(name = "AE_LKP_ADVID")
    public String getAdvEveLKPId() {
        return StringUtil.integerToString(this.advEveLKPId);
    }

    /**
     * Gets adverse event form status
     *
     * @return adverse event form status
     */
    @Column(name = "FORM_STATUS")
    public String getFormStatus() {
        return StringUtil.integerToString(this.formStatus);

    }

    /**
     * Sets adverse event form status
     *
     * @param formStatus
     *            adverse event form status
     */
    public void setFormStatus(String formStatus) {
        if (!StringUtil.isEmpty(formStatus)) {
            this.formStatus = Integer.valueOf(formStatus);
        } else {
            this.formStatus = null;
        }

    }

    // Method added by Gopu dated on 01/20/05
    @Column(name = "FK_CODELST_OUTACTION")
    public String getFkOutcomeAction() {

        return StringUtil.integerToString(this.fkOutcomeAction);

    }

    public void setFkOutcomeAction(String fkOutcomeAction) {
        if (!StringUtil.isEmpty(fkOutcomeAction)) {
            this.fkOutcomeAction = Integer.valueOf(fkOutcomeAction);
        } else {
            this.fkOutcomeAction = null;
        }

    }

    /**
     * Gets the adverse event's treatment course
     *
     * @return adverse event's treatment course
     */
    @Column(name = "AE_TREATMENT_COURSE")
    public String getAdvEveTreatment() {
        return this.advEveTreatment;
    }

    /**
     * Sets the adverse event's treatment course
     *
     * @param advEveDesc
     *            adverse event's tretamnet course
     */

    public void setAdvEveTreatment(String advEveTreatment) {
        this.advEveTreatment = advEveTreatment;
    }

    // Method added by Gopu dated on 01/20/05

    // / END OF GETTER SETTER METHODS

    /**
     * creates an AdvEveStateKeeper object using the bean attributes
     *
     * @return AdvEveStateKeeper AdvEveStateKeeper object
     * @see AdvEveStateKeeper
     */
    /*
     * *************************History*********************************
     * Modified by :Sonia Modified On:06/11/2004 Comment: to set new attribute
     * formStatus
     * *************************END****************************************
     */

    /*
     * public AdvEveStateKeeper getAdvEveStateKeeper() { Rlog.debug("advEve",
     * "AdvEveBean.getAdvEveStateKeeper"); return new
     * AdvEveStateKeeper(getAdvEveId(), getAdvEveEvents1Id(),
     * getAdvEveCodelstAeTypeId(), getAdvEveDesc(), getAdvEveStDate(),
     * getAdvEveEndDate(), getAdvEveEnterBy(), getAdvEveOutType(),
     * getAdvEveOutDate(), getAdvEveOutNotes(), getAdvEveAddInfo(),
     * getAdvEveNotes(), getFkStudy(), getFkPer(), getAdvEveSeverity(),
     * getAdvEveBdsystemAff(), getAdvEveRelationship(), getAdvEveRecoveryDesc(),
     * getCreator(), getModifiedBy(), getIpAdd(), getAdvEveGrade(),
     * getAdvEveName(), getAdvEveLKPId(), getAdvEveTreatment(), getFormStatus(),
     * getFkOutcomeAction()); }
     */

    /**
     * sets bean's attributes
     *
     * @return edsk an AdvEveStateKeeper object containing adverse event details
     */
    /*
     * *************************History*********************************
     * Modified by :Sonia Modified On:06/11/2004 Comment: to set new attribute
     * formStatus
     * *************************END****************************************
     */
    /*
     * public void setAdvEveStateKeeper(AdvEveStateKeeper usk) { GenerateId
     * advId = null;
     *
     * try { Connection conn = null; conn = getConnection();
     *
     * advEveId = advId.getId("SEQ_SCH_ADVERSEVE", conn); conn.close(); //
     * setAdvEveId(usk.getAdvEveId());
     *
     * setAdvEveEvents1Id(usk.getAdvEveEvents1Id());
     * setAdvEveCodelstAeTypeId(usk.getAdvEveCodelstAeTypeId());
     * setAdvEveDesc(usk.getAdvEveDesc());
     * setAdvEveStDate(usk.getAdvEveStDate());
     * setAdvEveEndDate(usk.getAdvEveEndDate());
     * setAdvEveEnterBy(usk.getAdvEveEnterBy());
     * setAdvEveOutType(usk.getAdvEveOutType());
     * setAdvEveOutDate(usk.getAdvEveOutDate());
     * setAdvEveOutNotes(usk.getAdvEveOutNotes());
     * setAdvEveAddInfo(usk.getAdvEveAddInfo());
     * setAdvEveNotes(usk.getAdvEveNotes()); setCreator(usk.getCreator());
     * setModifiedBy(usk.getModifiedBy()); setIpAdd(usk.getIpAdd());
     * setFkStudy(usk.getFkStudy()); setFkPer(usk.getFkPer());
     * setAdvEveSeverity(usk.getAdvEveSeverity());
     * setAdvEveBdsystemAff(usk.getAdvEveBdsystemAff());
     * setAdvEveRelationship(usk.getAdvEveRelationship());
     * setAdvEveRecoveryDesc(usk.getAdvEveRecoveryDesc());
     * setAdvEveGrade(usk.getAdvEveGrade()); setAdvEveName(usk.getAdvEveName());
     * setAdvEveLKPId(usk.getAdvEveLKPId());
     * setAdvEveTreatment(usk.getAdvEveTreatment());
     * setFormStatus(usk.getFormStatus()); // Added by Gopu dated on 01/20/05
     * setFkOutcomeAction(usk.getFkOutcomeAction());
     *
     * Rlog.debug("advEve", "AdvEveBean.setAdvEveStateKeeper() advEveId :" +
     * advEveId); } catch (Exception e) { Rlog.fatal("advEve", "Error in
     * setAdvEveStateKeeper() in AdvEveBean " + e); } }
     */

    /**
     * updates adverse event
     *
     * @param edsk
     *            an AdvEveStateKeeper object containing adverse event details
     * @return success flag
     *         <ul>
     *         <li> 0 if successful</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     */
    /*
     * *************************History*********************************
     * Modified by :Sonia Modified On:06/11/2004 Comment: to set new attribute
     * formStatus
     * *************************END****************************************
     */
    public int updateAdvEve(AdvEveBean edsk) {
        try {

            setAdvEveEvents1Id(edsk.getAdvEveEvents1Id());
            setAdvEveCodelstAeTypeId(edsk.getAdvEveCodelstAeTypeId());
            setAdvEveMedDRA(edsk.getAdvEveMedDRA());//KM
            setAdvEveDictionary(edsk.getAdvEveDictionary());//KM
            setAdvEveDesc(edsk.getAdvEveDesc());
            setAdvEveStDate(edsk.getAdvEveStDate());
            setAdvEveEndDate(edsk.getAdvEveEndDate());
            setAdvEveEnterBy(edsk.getAdvEveEnterBy());
            setAdvEveReportedBy(edsk.getAdvEveReportedBy());
            setAdvEveLinkedTo(edsk.getAdvEveLinkedTo());
            setAdvEveDiscoveryDate(edsk.getAdvEveDiscoveryDate());
            setAdvEveLoggedDate(edsk.getAdvEveLoggedDate());
            setAdvEveOutType(edsk.getAdvEveOutType());
            setAdvEveOutDate(edsk.getAdvEveOutDate());
            setAdvEveOutNotes(edsk.getAdvEveOutNotes());
            setAdvEveAddInfo(edsk.getAdvEveAddInfo());
            setAdvEveNotes(edsk.getAdvEveNotes());
            setFkStudy(edsk.getFkStudy());
            setFkPer(edsk.getFkPer());
            setAdvEveSeverity(edsk.getAdvEveSeverity());
            setAdvEveBdsystemAff(edsk.getAdvEveBdsystemAff());
            setAdvEveRelationship(edsk.getAdvEveRelationship());
            setAdvEveRecoveryDesc(edsk.getAdvEveRecoveryDesc());
            setCreator(edsk.getCreator());
            setModifiedBy(edsk.getModifiedBy());
            setIpAdd(edsk.getIpAdd());
            setAdvEveGrade(edsk.getAdvEveGrade());
            setAdvEveName(edsk.getAdvEveName());
            setAdvEveLKPId(edsk.getAdvEveLKPId());
            setAdvEveTreatment(edsk.getAdvEveTreatment());
            setFormStatus(edsk.getFormStatus());
            // Added by Gopu dated on 01/20/05
            setFkOutcomeAction(edsk.getFkOutcomeAction());
            return 0;
        } catch (Exception e) {
            Rlog.fatal("advEve", " error in AdvEveBean.updateAdvEve : " + e);
            return -2;
        }
    }

    public AdvEveBean() {
    }

public AdvEveBean(int advEveId, String advEveEvents1Id,String advEveCodelstAeTypeId,String advEveMedDRA,String advEveDictionary, String advEveDesc,
            String advEveStDate, String advEveEndDate, String advEveEnterBy,String advEveReportedBy,
            String advEveLinkedTo,
            String advEveDiscoveryDate,String advEveLoggedDate,
            String advEveOutType, String advEveOutDate, String advEveOutNotes,
            String advEveAddInfo, String advEveNotes, String fkStudy,
            String fkPer, String advEveSeverity, String advEveBdsystemAff,
            String advEveRelationship, String advEveRecoveryDesc,
            String creator, String modifiedBy, String ipAdd,
            String advEveGrade, String advEveName, String advEveLKPId,
            String advEveTreatment, String formStatus, String fkOutcomeAction) {
        super();
        // TODO Auto-generated constructor stub
        setAdvEveId(advEveId);
        setAdvEveEvents1Id(advEveEvents1Id);
        setAdvEveCodelstAeTypeId(advEveCodelstAeTypeId);

        setAdvEveMedDRA(advEveMedDRA);//KM
        setAdvEveDictionary(advEveDictionary);//KM
        setAdvEveDesc(advEveDesc);
        setAdvEveStDate(advEveStDate);
        setAdvEveEndDate(advEveEndDate);
        setAdvEveEnterBy(advEveEnterBy);
        setAdvEveReportedBy(advEveReportedBy); //KN
        setAdvEveLinkedTo(advEveLinkedTo); //KN
        setAdvEveDiscoveryDate(advEveDiscoveryDate) ; //KN
        setAdvEveLoggedDate(advEveLoggedDate); //KN
        setAdvEveOutType(advEveOutType);
        setAdvEveOutDate(advEveOutDate);
        setAdvEveOutNotes(advEveOutNotes);
        setAdvEveAddInfo(advEveAddInfo);
        setAdvEveNotes(advEveNotes);
        setFkStudy(fkStudy);
        setFkPer(fkPer);
        setAdvEveSeverity(advEveSeverity);
        setAdvEveBdsystemAff(advEveBdsystemAff);
        setAdvEveRelationship(advEveRelationship);
        setAdvEveRecoveryDesc(advEveRecoveryDesc);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setAdvEveGrade(advEveGrade);
        setAdvEveName(advEveName);
        setAdvEveLKPId(advEveLKPId);
        setAdvEveTreatment(advEveTreatment);
        setFormStatus(formStatus);
        setFkOutcomeAction(fkOutcomeAction);
    }

}// end of class
