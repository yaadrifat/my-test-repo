/*
 * Classname			EventkitAgentRObj
 * 
 * Version information : 1.0  
 *
 * Date					05/09/2008
 * 
 * Copyright notice : Velos Inc
 */

package com.velos.esch.service.eventKitAgent;

import java.util.Hashtable;
import javax.ejb.Remote;

import com.velos.esch.business.common.EventKitDao;
import com.velos.esch.business.eventKit.impl.EventKitBean;

/**
 * Remote interface for EventKitAgentBean session EJB
 * 
 * @author Sonia Abrol
 * @version 1.0, 05/09/2008
 */
@Remote
public interface EventKitAgentRObj {

    public EventKitBean getEventKitDetails(int eventKitId);
    
    public int setEventKitDetails(EventKitBean eventkit) ;

    public int updateEventKit(EventKitBean eventcrf);
    
    public EventKitDao getEventStorageKits(int eventId);
    
    public void deleteEventKit(int eventId);
    
    //for multiple inserts
    public int setEventKitDetails(EventKitBean eventkit,String[] storagePks) ;
 
    public int deleteEventKitRecord(int eventKitId) ;
    
    public int deleteEventKitRecord(int eventKitId, Hashtable<String, String> argsvalues) ;//Overloaded for INF-18183 ::: Ankit
}
