/*
 * Classname : PortalFormsAgentBean
 *
 * Version information: 1.0
 *
 * Date: 03DEC2010
 *
 * Copyright notice: Velos, Inc
 *
 * Author: Jnanamay Majuamdar
 */
package com.velos.esch.service.portalFormsAgent.impl;

/* IMPORT STATEMENTS */


import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import com.aithent.audittrail.reports.AuditUtils;
import com.velos.eres.service.util.DateUtil;
import com.velos.esch.business.audit.impl.AuditBean;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.esch.business.common.PortalFormsDao;
import com.velos.esch.business.eventassoc.impl.EventAssocBean;
import com.velos.esch.business.portalForms.impl.PortalFormsBean;
import  com.velos.esch.service.portalFormsAgent.PortalFormsAgentRObj;
import com.velos.esch.service.util.Rlog;
import com.velos.esch.service.util.StringUtil;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP CrflibBean.<br>
 * <br>
 *
 */
@Stateless
public class PortalFormsAgentBean implements PortalFormsAgentRObj {
    @PersistenceContext(unitName = "esch")
    protected EntityManager em;

    @Resource private SessionContext context;



    
    
    
    /**
     * Sets the Portal Forms details
     * 
     * @param 
     *            PortalFormsStateKeeper, which holds the Portal Forms attributes to be
     *            set
     * @return the generated Portal Form id, in case the Portal Form attributes are set
     *         successfully, 0 - if an error occurs
     */
    // PP-18306 AGodara 
    public int setPortalForms(PortalFormsBean pfsk) {
        try {        	
        	EventAssocBean event = em.find(EventAssocBean.class, StringUtil.stringToInteger(pfsk.getEventId()));
        	pfsk.setCaledarId(event.getChain_id());
        	PortalFormsBean pf = new PortalFormsBean();
        	pf.updatePortalForms(pfsk);
            em.persist(pf);
            return pf.getPortalFormId();            
        } catch (Exception e) {
            Rlog.fatal("portalForms",
                    "Error in setPortalForms() in PortalFormsAgentBean " + e);
        }
        return 0;
    }
    
    
    
    public PortalFormsBean getPortalFormDetails(int pfId) {
    	PortalFormsBean pf = null;

        try {
        	pf = (PortalFormsBean) em.find(PortalFormsBean.class, new Integer(pfId));
        } catch (Exception e) {
            Rlog.fatal("portalForms","Error in getPortalFormDetails() in PortalFormsAgentBean" + e);
        }
        return pf;
    }
    
    
    
    
    
    
    public int updatePortalForms(PortalFormsBean pfsk) {

        PortalFormsBean retrieved = null;
        int output;

        try {

            retrieved = (PortalFormsBean) em.find(PortalFormsBean.class, pfsk
                    .getPortalFormId());

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updatePortalForms(pfsk);

        } catch (Exception e) {
            Rlog
                    .debug("portalForms", "Error in updatePortalForms in PortalFormsAgentBean"
                            + e);
            return -2;
        }
        return output;
    }


    public PortalFormsDao getCheckedFormsForPortalCalendar(int calId, int portalId) {
    	
    	try {
            Rlog.debug("portalForms",
                    "PortalFormsAgentBean.getCheckedFormsForPortalCalendar starting");
            PortalFormsDao pfDao = new PortalFormsDao();
            pfDao.getCheckedFormsForPortalCalendar(calId, portalId);
            return pfDao;
        } catch (Exception e) {
            Rlog.fatal("portalForms",
                    "Exception In getCheckedFormsForPortalCalendar in PortalFormsAgentBean " + e);
        }
        return null;
    	
    }
    public int removePortalFormId(int pfId){
    	
    	PortalFormsDao pfDao = new PortalFormsDao();
            int ret = 0;
            try {
                Rlog.debug("portalForms", "PortalFormsAgentBean:removePortalFormId- 0");
                ret = pfDao.removePortalFormId(pfId);

            } catch (Exception e) {
                Rlog.fatal("portalForms", "PortalFormsAgentBean:removePortalFormId-Exception In removePortalFormId "
                        + e);
                return -1;
            }

            return ret;

    }

	//removePortalFormId() Overloaded for INF-18183 ::: Raviesh
	 public int removePortalFormId(int pfId,Hashtable<String, String> args){
	 	
	 	PortalFormsDao pfDao = new PortalFormsDao();
	         int ret = 0;
	         try {	         	
	         	 AuditBean audit=null; //Audit Bean for AppDelete
	              String currdate =DateUtil.getCurrentDate(); /*gets the current date from DateUtil*/
	              String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
	              String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
	              String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
	              String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
	              String getRidValue= AuditUtils.getRidValue("SCH_PORTAL_FORMS","esch","PK_PF="+pfId);/*Fetches the RID/PK_VALUE*/ 
	          	audit = new AuditBean("SCH_PORTAL_FORMS",String.valueOf(pfId),getRidValue,
	          			userID,currdate,appModule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
	          	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
	         	
	             Rlog.debug("portalForms", "PortalFormsAgentBean:removePortalFormId- 0");
	             ret = pfDao.removePortalFormId(pfId);
	             
	             if(ret!=1){context.setRollbackOnly();}; /*Checks whether the actual delete has occurred with return 1 else, rollback */
	
	         } catch (Exception e) {
	         	context.setRollbackOnly();
	             Rlog.fatal("portalForms", "PortalFormsAgentBean:removePortalFormId-Exception In removePortalFormId "
	                     + e);
	             return -1;
	         }
	
	         return ret;
	
	 } 
 
	 public int removeCalFromPortal(int potalId, int calId){
    	PortalFormsDao pfDao = new PortalFormsDao();
        int ret = 0;
        try {
            Rlog.debug("portalForms", "PortalFormsAgentBean:removeCalFromPortal - 0");
            ret = pfDao.removeCalFromPortal(potalId, calId);

        } catch (Exception e) {
            Rlog.fatal("portalForms", "PortalFormsAgentBean:removeCalFromPortal-Exception In removePortalFormId "
                    + e);
            return -1;
        }

        return ret;

    }
    
    // Overloaded for INF-18183 ::: AGodara
    public int removeCalFromPortal(int potalId, int calId,Hashtable<String, String> auditInfo){
    	PortalFormsDao pfDao = new PortalFormsDao();
        int ret = 0;
        Connection conn = null;
        try {
        	String condition ="FK_PORTAL ="+ potalId+" and FK_CALENDAR<>"+calId;
        	Hashtable<String, ArrayList<String>> rowValues = AuditUtils.getRowValues("SCH_PORTAL_FORMS", condition, "esch");
        	String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
        	conn = AuditUtils.insertAuditRow("SCH_PORTAL_FORMS", rowValues, app_module, auditInfo, "esch");
		
            Rlog.info("portalForms", "PortalFormsAgentBean:removeCalFromPortal - 0");
            ret = pfDao.removeCalFromPortal(potalId, calId);
            if(ret==1){
            	conn.commit();
            }
        } catch (Exception e) {
        	try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
            Rlog.fatal("portalForms", "PortalFormsAgentBean:removeCalFromPortal-Exception In removePortalFormId "
                    + e);
            return -1;
        }finally{
        	try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
        return ret;
    }
    
    // PP-18306 ::: AGodara
    public int removeMulCalForms(int potalId, String calIds,Hashtable<String, String> auditInfo){
    	PortalFormsDao pfDao = new PortalFormsDao();
        int ret = 0;
        Connection conn = null;
        try {
        	String condition ="FK_PORTAL ="+ potalId+" and FK_CALENDAR NOT IN ("+calIds+")";
     		Hashtable<String, ArrayList<String>> rowValues = AuditUtils.getRowValues("SCH_PORTAL_FORMS", condition, "esch");
        	String app_module=(String)auditInfo.get(AuditUtils.APP_MODULE);// Fetches the APP_Module from the Hashtable
        	conn = AuditUtils.insertAuditRow("SCH_PORTAL_FORMS", rowValues, app_module, auditInfo, "esch");
		
            Rlog.info("portalForms", "PortalFormsAgentBean:removeCalFromPortal - 0");
            ret = pfDao.removeMulCalForms(potalId, calIds);
            if(ret==1){
            	conn.commit();
            }
        } catch (Exception e) {
        	try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
            Rlog.fatal("portalForms", "PortalFormsAgentBean:removeCalFromPortal-Exception In removePortalFormId "
                    + e);
            return -1;
        }finally{
        	try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
        return ret;
    }
    
  
	public PortalFormsDao getPortalFormsDAO(int eventId1,ArrayList<String> selId){
		
		PortalFormsDao pfDao = new PortalFormsDao();
		try{
			pfDao.getPortalFormsDAO(eventId1, selId);
		}catch(Exception e){
			Rlog.fatal("PortalFormsAgentBean", "getPortalFormsDAO-Exception In removePortalFormId "
                    + e);
			return null;
		}
		return pfDao;
	}

	public int setPortalFormsDAO(PortalFormsDao pfDao) {

		int output =0;
		try{
			output = pfDao.setPortalFormsDAO(pfDao);
		}catch(Exception e){
			Rlog.fatal("PortalFormsAgentBean", "setPortalFormsDAO-Exception In removePortalFormId "
                    + e);
			return -2;
		}
		return output;
	}

}// end of class

