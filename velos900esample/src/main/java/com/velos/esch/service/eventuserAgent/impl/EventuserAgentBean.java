/*
 * Classname : EventuserAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 06/21/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana 
 */

package com.velos.esch.service.eventuserAgent.impl;

/* IMPORT STATEMENTS */

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.aithent.audittrail.reports.AuditUtils;
import com.velos.esch.business.audit.impl.AuditBean;
import com.velos.eres.service.util.DateUtil;
import com.velos.esch.business.common.EventuserDao;
import com.velos.esch.business.eventuser.impl.EventuserBean;
import com.velos.esch.service.eventuserAgent.EventuserAgentRObj;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP
 * EventuserBean.<br>
 * <br>
 * 
 */
@Stateless
public class EventuserAgentBean implements EventuserAgentRObj {
    @PersistenceContext(unitName = "esch")
    protected EntityManager em;
    @Resource private SessionContext context;

    /**
     * 
     */
    private static final long serialVersionUID = 3257565122447683633L;

    /*
     * * Calls getEventuserDetails() on Eventuser Entity Bean EventuserBean.
     * Looks up on the Eventuser id parameter passed in and constructs and
     * returns a Eventuser state holder. containing all the summary details of
     * the Eventuser<br>
     * 
     * @param eventuserId the Eventuser id @ee EventuserBean
     */
    public EventuserBean getEventuserDetails(int eventuserId) {

        try {

            return (EventuserBean) em.find(EventuserBean.class, new Integer(
                    eventuserId));

        } catch (Exception e) {
            System.out
                    .print("Error in getEventuserDetails() in EventuserAgentBean"
                            + e);
            return null;
        }

    }

    /**
     * Calls setEventuserDetails() on Eventuser Entity Bean
     * EventuserBean.Creates a new Eventuser with the values in the StateKeeper
     * object.
     * 
     * @param eventuser
     *            a eventuser state keeper containing eventuser attributes for
     *            the new Eventuser.
     * @return int - 0 if successful; -2 for Exception;
     */

    public int setEventuserDetails(EventuserBean ecsk) {
        try {
            ArrayList userIds = ecsk.getUserIds();
            ArrayList evDuration = ecsk.getEvDuration();
            ArrayList evNotes = ecsk.getEvNotes();

            for (int i = 0; i < userIds.size(); i++) {
                String userId = (String) userIds.get(i);
                ecsk.setUserId(userId);

                if ((ecsk.getUserType()).equals("R")) // only for resources
                {
                    String duration = (String) evDuration.get(i);
                    String notes = (String) evNotes.get(i);
                    ecsk.setDuration(duration);
                    ecsk.setNotes(notes);
                }

                em.merge(ecsk);
            }

            return (ecsk.getEventuserId());
        } catch (Exception e) {
            System.out
                    .print("Error in setEventuserDetails() in EventuserAgentBean "
                            + e);
        }
        return 0;
    }

    public int updateEventuser(EventuserBean ecsk) {

        EventuserBean retrieved = null; // Eventuser Entity Bean Remote Object
        int output;

        try {

            retrieved = em.find(EventuserBean.class, ecsk.getEventuserId());

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateEventuser(ecsk);

        } catch (Exception e) {
            Rlog.debug("eventuser",
                    "Error in updateEventuser in EventuserAgentBean" + e);
            return -2;
        }
        return output;
    }

    
    public int removeEventusers(int eventId, String eventType) {

        int output = 0;
        Enumeration eventuserId;

        try {
     	
        	
            String Id = Integer.toString(eventId);
            Id = Id.trim();
            eventType = eventType.trim();

            Query querySiteIdentifier = em.createNamedQuery("findEventuserIds");
            querySiteIdentifier.setParameter("eventId", Id);
            querySiteIdentifier.setParameter("eventusr_type", eventType);
            ArrayList list = (ArrayList) querySiteIdentifier.getResultList();
            if (list == null)
                list = new ArrayList();
            System.out.println("SiteAgentBean.setSiteDetails SiteStateKeeper:enum_velos "+ list);

            for (int i = 0; i < list.size(); i++) {
                em.remove((EventuserBean) list.get(i));
            }

        } catch (Exception e) {
        	
            Rlog.debug("eventuser",
                    "Error in removeEventuserChild  in EventuserAgentBean" + e);
            return -2;
        }
        return output;
    }
    

    // removeEventusers() Overloaded for INF-18183 ::: Raviesh
    public int removeEventusers(int eventId, String eventType,Hashtable<String, String> args) {

        int output = 0;
        Enumeration eventuserId;
        String condition="";
        try {

        	AuditBean audit=null;
        	String pkVal = "";
        	String ridVal = "";
           
            String currdate =DateUtil.getCurrentDate();
            String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
            String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
            String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
            String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
                   	
        	condition = "FK_EVENT="+eventId ;

        	Hashtable<String, ArrayList<String>> getRowValues = AuditUtils.getRowValues("SCH_EVENTUSR",condition,"esch");/*Fetches the RID/PK_VALUE*/
        	ArrayList<String> rowPK  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_PK_KEY);
    		ArrayList<String> rowRID  = (ArrayList<String>)getRowValues.get(AuditUtils.ROW_RID_KEY);
    		
            //String getRidValue= AuditUtils.getRidValues("ER_PATPROT","PK_PATPROT="+condition);/*Fetches the RID/PK_VALUE*/ 
            
    		for(int i=0; i<rowPK.size(); i++)
			{
    			pkVal = rowPK.get(i);
    			ridVal = rowRID.get(i);
    			audit = new AuditBean("SCH_EVENTUSR",pkVal,ridVal,
        			userID,currdate,appModule,ipAdd,reason);
    			em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
			}
    		        	
            String Id = Integer.toString(eventId);
            Id = Id.trim();
            eventType = eventType.trim();

            Query querySiteIdentifier = em.createNamedQuery("findEventuserIds");
            querySiteIdentifier.setParameter("eventId", Id);
            querySiteIdentifier.setParameter("eventusr_type", eventType);
            ArrayList list = (ArrayList) querySiteIdentifier.getResultList();
            if (list == null)
                list = new ArrayList();
            System.out.println("SiteAgentBean.setSiteDetails SiteStateKeeper:enum_velos "+ list);

            for (int i = 0; i < list.size(); i++) {
                em.remove((EventuserBean) list.get(i));
            }

        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.debug("eventuser",
                    "Error in removeEventuserChild  in EventuserAgentBean" + e);
            return -2;
        }
        return output;
    }

    
    public int removeEventuser(int eventuserId) {

        EventuserBean retrieved = null; // Eventdef Entity Bean Remote Object
        int output = 0;

        try {

            retrieved = (EventuserBean) em.find(EventuserBean.class,
                    new Integer(eventuserId));
            em.remove(retrieved);
        } catch (Exception e) {
        	
            Rlog.debug("eventuser",
                    "Error in removeEventuser  in EventuserAgentBean" + e);
            return -2;
        }
        return output;
    }

    
    // removeEventuser() Overloaded for INF-18183 ::: Raviesh
    public int removeEventuser(int eventuserId,Hashtable<String, String> args) {

        EventuserBean retrieved = null; // Eventdef Entity Bean Remote Object
        int output = 0;

        try {

        	  AuditBean audit=null; //Audit Bean for AppDelete
              String currdate =DateUtil.getCurrentDate(); /*gets the current date from DateUtil*/
              String userID=(String)args.get(AuditUtils.USER_ID_KEY); /*Fetches the User ID from the Hashtable*/
              String ipAdd=(String)args.get(AuditUtils.IP_ADD_KEY); /*Fetches the IP Address from the Hashtable*/
              String reason=(String)args.get(AuditUtils.REASON_FOR_DEL); /*Fetches the reason from the Hashtable*/
              String appModule=(String)args.get(AuditUtils.APP_MODULE); /*Fetches the App Module Name from the Hashtable*/
              
              String getRidValue= AuditUtils.getRidValue("SCH_EVENTUSR","esch","PK_EVENTUSR="+eventuserId);/*Fetches the RID/PK_VALUE*/ 
          	audit = new AuditBean("SCH_EVENTUSR",String.valueOf(eventuserId),getRidValue,
          			userID,currdate,appModule,ipAdd,reason);/*POPULATE THE AUDIT BEAN*/ 
          	em.persist(audit);/*PERSIST THE DATA IN AUDIT TABLE*/
        	
            retrieved = (EventuserBean) em.find(EventuserBean.class,
                    new Integer(eventuserId));
            em.remove(retrieved);
        } catch (Exception e) {
        	context.setRollbackOnly();
            Rlog.debug("eventuser",
                    "Error in removeEventuser  in EventuserAgentBean" + e);
            return -2;
        }
        return output;
    }
    
    
    // gets roles for an event

    public EventuserDao getEventRoles(int eventId) {
        EventuserDao evd = new EventuserDao();
        try {
            evd.getEventRoles(eventId);
            return evd;
        } catch (Exception e) {
            Rlog.fatal("eventuser",
                    "Error in getEventRoles in EventuserAgentBean" + e);
            return null;
        }

    }

    // gets roles for an event

    public EventuserDao getEventUsers(int eventId) {
        EventuserDao evd = new EventuserDao();
        try {
            evd.getEventUsers(eventId);
            return evd;
        } catch (Exception e) {
            Rlog.fatal("eventuser",
                    "Error in getEventRoles in EventuserAgentBean" + e);
            return null;
        }

    }

    // get selected as well as available roles for the user

    public EventuserDao getAllRolesForEvent(int eventId) {
        EventuserDao evd = new EventuserDao();
        try {
            evd.getAllRolesForEvent(eventId);
            return evd;
        } catch (Exception e) {
            Rlog.fatal("eventuser",
                    "Error in getEventRoles in EventuserAgentBean" + e);
            return null;
        }

    }
    
    
    /**
     * Calls setEventuserDetails() on Eventuser Entity Bean
     * EventuserBean.Creates a new Eventuser with the values in the StateKeeper
     * object.
     * 
     * @param eventuser
     *            a eventuser state keeper containing eventuser attributes for
     *            the new Eventuser.
     * @return int - 0 if successful; -2 for Exception;
     */

    public int setEventuserDetailsSingle(EventuserBean ecsk) {
        try {
             
        	EventuserBean ub = new EventuserBean();
        	ub.updateEventuser(ecsk);
        	em.persist(ub);
        	
             
            return (ub.getEventuserId());
        } catch (Exception e) {
            System.out.print("Error in setEventuserDetailsSingle() in EventuserAgentBean "
                            + e);
        }
        return 0;
    }
    

}// end of class
