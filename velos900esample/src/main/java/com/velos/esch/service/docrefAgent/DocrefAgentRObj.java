package com.velos.esch.service.docrefAgent;

import javax.ejb.Remote;

import com.velos.esch.business.docref.impl.DocrefBean;

@Remote
public interface DocrefAgentRObj {

    public int setDocRefDetails(DocrefBean docrefBean);

    public int remove(DocrefBean docrefBean);

    public int remove(int docrefId);
}
