/*
 * Classname : AdvEveAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 11/27/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.service.advEveAgent;

/* Import Statements */
import java.rmi.RemoteException;
import java.util.Hashtable;

import javax.ejb.Remote;

import com.velos.eres.business.common.NCILibDao;
import com.velos.esch.business.advEve.impl.AdvEveBean;
import com.velos.esch.business.common.EventInfoDao;

/**
 * 
 * Remote interface for AdvEveAgent Session Bean
 * 
 * @author Arvind Kumar
 * @version %I%, %G%
 */
/*
 * *************************History********************************* Modified by
 * :Sonia Modified On:06/11/2004 Comment: 1. Modified JavaDoc comments
 * *************************END****************************************
 */
@Remote
public interface AdvEveAgentRObj {

    /**
     * returns an AdvEveStateKeeper object with Adverse Event details.
     * 
     * @param advEveId
     *            the adverse event id
     * @throws RemoteException
     *             if there is any communication-related exception that may
     *             occur during the execution of the remote method call
     * @see com.velos.esch.business.advEve.AdvEveStateKeeper
     */

    // AdvEveStateKeeper getAdvEveDetails(int advEveId) throws RemoteException;
    // commented by JMajumdar dt. 20Jan05
    public AdvEveBean getAdvEveDetails(int advEveId);

    /**
     * Creates a new Adverse Event.
     * 
     * @param edsk
     *            AdvEveStateKeeper object with Adverse Event details
     * @return success flag
     *         <ul>
     *         <li> id of new adverse event</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     * @throws RemoteException
     *             if there is any communication-related exception that may
     *             occur during the execution of the remote method call
     * @see com.velos.esch.business.advEve.AdvEveStateKeeper
     */
    public int setAdvEveDetails(AdvEveBean edsk);

    /**
     * Updates an Adverse Event.
     * 
     * @param edsk
     *            AdvEveStateKeeper object with Adverse Event details
     * @return success flag
     *         <ul>
     *         <li> 0 if successful</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     * @throws RemoteException
     *             if there is any communication-related exception that may
     *             occur during the execution of the remote method call
     * @see com.velos.esch.business.advEve.AdvEveStateKeeper
     */

    public int updateAdvEve(AdvEveBean edsk);

    /**
     * Returns an EventInfoDao object with adverse events.
     * 
     * @param patProtID
     *            patient enrollment id
     * @return returns EventInfoDao with adverse event details
     * @throws RemoteException
     *             if there is any communication-related exception that may
     *             occur during the execution of the remote method call
     * @see com.velos.esch.business.common.EventInfoDao
     */

    public EventInfoDao getAdverseEventNames(int patProtID);

    /**
     * Returns an NCILibDao object with patient adverse events toxicity
     * information.
     * 
     * @param toxicityGroup
     *            toxicity group
     * @param labDate
     *            lab test date
     * @param labUnit
     *            lab unit
     * @param labSite
     *            lab site
     * @param labResult
     *            lab result
     * @param labLLN
     *            lab LLN
     * @param labULN
     *            lab ULN
     * @param userRangeFlag
     *            flag whether to user user specified range or lab facility's
     *            range
     * @param nciVersion
     *            NCI CDE version
     * @param patient
     *            patient
     * @return returns NCILibDao
     * @see com.velos.eres.business.common.NCILibDao
     */
    public NCILibDao getToxicityGrade(int toxicityGroup, String labDate,
            String labUnit, int labSite, String labResult, int labLLN,
            int labULN, int userRangeFlag, String nciVersion, int patient);

    /**
     * Returns an NCILibDao object with adverse events toxicity names. Calls
     * getNCILib() on NCILibDao object.
     * 
     * @param nciVersion
     * @return returns NCILibDao
     * @see com.velos.eres.business.common.NCILibDao
     */

    public NCILibDao getNCILib(String nciVersion);

    // added by J.Majumdar dt. 19Jan05
    public int reomoveAdvEve(int advId);
    // Overloaded for INF-18183 ::: AGodara
    public int reomoveAdvEve(int advId,Hashtable<String, String> auditInfo);
}