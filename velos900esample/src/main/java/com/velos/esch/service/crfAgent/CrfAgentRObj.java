/*
 * Classname : CrfAgentRObj
 * 
 * Version information: 1.0
 *
 * Date: 11/23/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.service.crfAgent;

/* Import Statements */
import javax.ejb.Remote;

import com.velos.esch.business.common.CrfDao;
import com.velos.esch.business.crf.impl.CrfBean;

@Remote
public interface CrfAgentRObj {

    CrfBean getCrfDetails(int crfId);

    public int setCrfDetails(CrfBean account);

    public int updateCrf(CrfBean usk);

    public CrfDao getCrfValues(int patProtId);

}