/*
 * Classname			GenerateId
 * 
 * Version information  1.0
 *
 * Date					07/03/2000
 * 
 * Copyright notice		Velos Inc.
 * 
 * Author 				From eClinical
 */

/** * $Id: GenerateId.java,v 1.2 2005/09/19 17:15:10 vabrol Exp $
 * Copyright 2000 Velos, Inc. All rights reserved.
 */

package com.velos.esch.service.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Generates the id from database sequence
 * 
 * @author Dharani Babu
 * @version 1.0, 07/03/2000
 */
public class GenerateId {
    public static int getId(String seqName, Connection conn)
            throws SQLException {
        Statement stmt = conn.createStatement();
        Rlog.debug("common", "select " + seqName + ".nextval from dual");
        ResultSet rs = stmt.executeQuery("select " + seqName
                + ".nextval from dual");
        rs.next();
        return rs.getInt(1);
    }
}
