/*
 * Classname : CrfStatAgentBean
 * 
 * Version information: 1.0
 *
 * Date: 11/26/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.service.crfStatAgent.impl;

/* IMPORT STATEMENTS */

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.velos.esch.business.common.CrfStatDao;
import com.velos.esch.business.crfStat.impl.CrfStatBean;
import com.velos.esch.service.crfStatAgent.CrfStatAgentRObj;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * The stateless session EJB acting as a facade for the entity CMP CrfStatBean.<br>
 * <br>
 * 
 */
@Stateless
public class CrfStatAgentBean implements CrfStatAgentRObj {

    @PersistenceContext(unitName = "esch")
    protected EntityManager em;

    /**
     * Calls getCrfStatDetails() on CrfStat Entity Bean CrfBean. Looks up on the
     * CrfStat id parameter passed in and constructs and returns a Crf state
     * holder. containing all the summary details of the Crf<br>
     * 
     * @param crfStatId
     *            the CrfStat id
     * @ee CrfStatBean
     */
    public CrfStatBean getCrfStatDetails(int crfStatId) {

        try {
            return (CrfStatBean) em.find(CrfStatBean.class, new Integer(
                    crfStatId));
        } catch (Exception e) {
            System.out.print("Error in getCrfStatDetails() in CrfStatAgentBean"
                    + e);
            return null;
        }

    }

    /**
     * Calls setCrfStatDetails() on CrfStat Entity Bean crfStatBean.Creates a
     * new CrfStat with the values in the StateKeeper object.
     * 
     * @param crfStat
     *            a crfStat state keeper containing crfStat attributes for the
     *            new CrfStat.
     * @return int - 0 if successful; -2 for Exception;
     */

    public int setCrfStatDetails(CrfStatBean edsk) {
        try {
            em.merge(edsk);
            return (edsk.getCrfStatId());
        } catch (Exception e) {
            System.out
                    .print("Error in setCrfStatDetails() in CrfStatAgentBean "
                            + e);
        }
        return 0;
    }

    public int updateCrfStat(CrfStatBean edsk) {

        CrfStatBean retrieved = null; // Crf Entity Bean Remote Object
        int output;

        try {

            retrieved = (CrfStatBean) em.find(CrfStatBean.class, new Integer(
                    edsk.getCrfStatId()));

            if (retrieved == null) {
                return -2;
            }
            output = retrieved.updateCrfStat(edsk);

        } catch (Exception e) {
            Rlog.debug("crfStat", "Error in updateCrfStat in CrfStatAgentBean"
                    + e);
            return -2;
        }
        return output;
    }

    public CrfStatDao getCrfValues(int patProtId, int visit) {
        try {
            Rlog.debug("person",
                    "In getPatients in PersonAgentBean line number 0");
            CrfStatDao crfStatDao = new CrfStatDao();
            Rlog.debug("person",
                    "In getPatients in PersonAgentBean line number 1");
            crfStatDao.getCrfValues(patProtId, visit);
            Rlog.debug("person",
                    "In getPatients in PersonAgentBean line number 2");
            return crfStatDao;
        } catch (Exception e) {
            Rlog.fatal("person", "Exception In getPatients in PersonAgentBean "
                    + e);
        }
        return null;

    }

}// end of class
