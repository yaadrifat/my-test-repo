/*
 * Classname : CrfStatJB
 * 
 * Version information: 1.0 
 *
 * Date: 11/26/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.web.crfStat;

/* IMPORT STATEMENTS */

import com.velos.esch.business.common.CrfStatDao;
import com.velos.esch.business.crfStat.impl.CrfStatBean;
import com.velos.esch.service.crfStatAgent.CrfStatAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side Java Bean for CRFSTAT
 * 
 * @author Arvind
 * @version 1.0 11/26/2001
 * 
 */

public class CrfStatJB {

    /**
     * crfStat id
     */
    private int crfStatId;

    /**
     * crf id
     */
    private String crfStatCrfId;

    /**
     * codelist Crf stat
     */
    private String crfStatCodelstCrfStatId;

    /**
     * Enter By
     */
    private String crfStatEnterBy;

    /**
     * Review By
     */
    private String crfStatReviewBy;

    /**
     * Review On
     */
    private String crfStatReviewOn;

    /**
     * Sent To
     */
    private String crfStatSentTo;

    /**
     * Sent Flag
     */
    private String crfStatSentFlag;

    /**
     * Sent By
     */
    private String crfStatSentBy;

    /**
     * Sent On
     */
    private String crfStatSentOn;

    /*
     * creator
     */
    private String creator;

    /*
     * Modified By
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    // GETTER SETTER METHODS

    public int getCrfStatId() {
        return this.crfStatId;
    }

    public void setCrfStatId(int crfStatId) {
        this.crfStatId = crfStatId;
    }

    public String getCrfStatCrfId() {
        return this.crfStatCrfId;
    }

    public void setCrfStatCrfId(String crfStatCrfId) {
        this.crfStatCrfId = crfStatCrfId;
    }

    public String getCrfStatCodelstCrfStatId() {
        return this.crfStatCodelstCrfStatId;
    }

    public void setCrfStatCodelstCrfStatId(String crfStatCodelstCrfStatId) {
        this.crfStatCodelstCrfStatId = crfStatCodelstCrfStatId;
    }

    public String getCrfStatEnterBy() {
        return this.crfStatEnterBy;
    }

    public void setCrfStatEnterBy(String crfStatEnterBy) {
        this.crfStatEnterBy = crfStatEnterBy;
    }

    public String getCrfStatReviewBy() {
        return this.crfStatReviewBy;
    }

    public void setCrfStatReviewBy(String crfStatReviewBy) {
        this.crfStatReviewBy = crfStatReviewBy;
    }

    public String getCrfStatReviewOn() {
        return this.crfStatReviewOn;
    }

    public void setCrfStatReviewOn(String crfStatReviewOn) {
        this.crfStatReviewOn = crfStatReviewOn;
    }

    public String getCrfStatSentTo() {
        return this.crfStatSentTo;
    }

    public void setCrfStatSentTo(String crfStatSentTo) {
        this.crfStatSentTo = crfStatSentTo;
    }

    public String getCrfStatSentFlag() {
        return this.crfStatSentFlag;
    }

    public void setCrfStatSentFlag(String crfStatSentFlag) {
        this.crfStatSentFlag = crfStatSentFlag;
    }

    public String getCrfStatSentBy() {
        return this.crfStatSentBy;
    }

    public void setCrfStatSentBy(String crfStatSentBy) {
        this.crfStatSentBy = crfStatSentBy;
    }

    public String getCrfStatSentOn() {
        return this.crfStatSentOn;
    }

    public void setCrfStatSentOn(String crfStatSentOn) {
        this.crfStatSentOn = crfStatSentOn;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return IP Address
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param modifiedBy
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts CrfStat Id
     * 
     * @param crfStatId
     *            Id of the CrfStat
     */
    public CrfStatJB(int crfStatId) {
        setCrfStatId(crfStatId);
    }

    /**
     * Default Constructor
     * 
     */
    public CrfStatJB() {
        Rlog.debug("crfStat", "CrfStatJB.CrfStatJB() ");
    }

    public CrfStatJB(int crfStatId, String crfStatCrfId,
            String crfStatCodelstCrfStatId, String crfStatEnterBy,
            String crfStatReviewBy, String crfStatReviewOn,
            String crfStatSentTo, String crfStatSentFlag, String crfStatSentBy,
            String crfStatSentOn, String creator, String modifiedBy,
            String ipAdd) {

        setCrfStatId(crfStatId);
        setCrfStatCrfId(crfStatCrfId);
        setCrfStatCodelstCrfStatId(crfStatCodelstCrfStatId);
        setCrfStatEnterBy(crfStatEnterBy);
        setCrfStatReviewBy(crfStatReviewBy);
        setCrfStatReviewOn(crfStatReviewOn);
        setCrfStatSentTo(crfStatSentTo);
        setCrfStatSentFlag(crfStatSentFlag);
        setCrfStatSentBy(crfStatSentBy);
        setCrfStatSentOn(crfStatSentOn);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        Rlog.debug("crfStat", "CrfStatJB.CrfStatJB(all parameters)");
    }

    /**
     * Calls getCrfStatDetails() of CrfStat Session Bean: CrfStatAgentBean
     * 
     * 
     * @return The Details of the crfStat in the CrfStat StateKeeper Object
     * @See CrfStatAgentBean
     */

    public CrfStatBean getCrfStatDetails() {
        CrfStatBean edsk = null;

        try {

            CrfStatAgentRObj crfStatAgent = EJBUtil.getCrfStatAgentHome();
            edsk = crfStatAgent.getCrfStatDetails(this.crfStatId);
            Rlog.debug("crfStat",
                    "CrfStatJB.getCrfStatDetails() CrfStatStateKeeper " + edsk);
        } catch (Exception e) {
            Rlog.debug("crf", "Error in getCrfStatDetails() in CrfStatJB " + e);
        }
        if (edsk != null) {

            this.crfStatId = edsk.getCrfStatId();
            this.crfStatCrfId = edsk.getCrfStatCrfId();
            this.crfStatCodelstCrfStatId = edsk.getCrfStatCodelstCrfStatId();
            this.crfStatEnterBy = edsk.getCrfStatEnterBy();
            this.crfStatReviewBy = edsk.getCrfStatReviewBy();
            this.crfStatReviewOn = edsk.getCrfStatReviewOn();
            this.crfStatSentTo = edsk.getCrfStatSentTo();
            this.crfStatSentFlag = edsk.getCrfStatSentFlag();
            this.crfStatSentBy = edsk.getCrfStatSentBy();
            this.crfStatSentOn = edsk.getCrfStatSentOn();
            this.creator = edsk.getCreator();
            this.modifiedBy = edsk.getModifiedBy();
            this.ipAdd = edsk.getIpAdd();

        }
        return edsk;
    }

    /**
     * Calls setcrfStatDetails() of crfStat Session Bean: crfStatAgentBean
     * 
     */

    public void setCrfStatDetails() {

        try {

            CrfStatAgentRObj crfStatAgent = EJBUtil.getCrfStatAgentHome();
            this.setCrfStatId(crfStatAgent.setCrfStatDetails(this
                    .createCrfStatStateKeeper()));
            Rlog.debug("crfStat", "CrfStatJB.setCrfStatDetails()");
        } catch (Exception e) {
            Rlog.fatal("crfStat", "Error in setCrfStatDetails() in CrfStatJB "
                    + e);
        }
    }

    /**
     * Calls updateCrfStat() of CrfStat Session Bean: CrfStatAgentBean
     * 
     * @return
     */
    public int updateCrfStat() {
        int output;
        try {

            CrfStatAgentRObj crfStatAgentRObj = EJBUtil.getCrfStatAgentHome();
            output = crfStatAgentRObj.updateCrfStat(this
                    .createCrfStatStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("crfStat",
                    "EXCEPTION IN SETTING CRFSTAT DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * 
     * 
     * @return the CrfStat StateKeeper Object with the current values of the
     *         Bean
     */

    public CrfStatBean createCrfStatStateKeeper() {
        Rlog.debug("crfStat", "CrfStatJB.createCrfStatStateKeeper ");

        return new CrfStatBean(crfStatId, crfStatCrfId,
                crfStatCodelstCrfStatId, crfStatEnterBy, crfStatReviewBy,
                crfStatReviewOn, crfStatSentTo, crfStatSentFlag, crfStatSentBy,
                crfStatSentOn, creator, modifiedBy, ipAdd);
    }

    public CrfStatDao getCrfValues(int patProtId, int visit) {
        CrfStatDao crfStatDao = new CrfStatDao();
        try {

            CrfStatAgentRObj crfStatAgent = EJBUtil.getCrfStatAgentHome();
            Rlog.debug("person", "PersonJB.getPatients after remote");
            crfStatDao = crfStatAgent.getCrfValues(patProtId, visit);
            Rlog.debug("person", "PersonJB.getPatients after Dao");
        } catch (Exception e) {
            Rlog.fatal("person", "Error in getPatients in PersonJB " + e);
        }
        return crfStatDao;
    }

}// end of class
