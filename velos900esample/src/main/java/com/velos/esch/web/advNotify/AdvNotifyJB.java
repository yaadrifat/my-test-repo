/*
 * Classname : AdvNotifyJB
 * 
 * Version information: 1.0 
 *
 * Date: 12/11/2002
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.web.advNotify;

/* IMPORT STATEMENTS */

import com.velos.esch.business.advNotify.impl.AdvNotifyBean;
import com.velos.esch.service.advNotifyAgent.AdvNotifyAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * AdvNotifyJB is a client side Java Bean for Adverse Event Notification
 * business components. The class implements the Business Delegate pattern to
 * reduce coupling between presentation-tier clients and business services.
 * 
 * <p>
 * In eResearch Architecture, the class resides at the Application layer.
 * </p>
 * 
 * @author Arvind Kumar
 * @version %I%, %G%
 */
public class AdvNotifyJB {

    /**
     * adverse event notification id
     */
    private int advNotifyId;

    /**
     * adverse event id
     */
    private String advNotifyAdverseId;

    /**
     * notification type
     */
    private String advNotifyCodelstNotTypeId;

    /**
     * notification date
     */
    private String advNotifyDate;

    /**
     * notes
     */
    private String advNotifyNotes;

    /**
     * adverse event notification flag. The flag determines if the notification
     * is sent.
     */
    private String advNotifyValue;

    /** creator */
    private String creator;

    /**
     * modified By
     */
    private String modifiedBy;

    /**
     * IP Address
     */
    private String ipAdd;

    // GETTER SETTER METHODS

    /**
     * Returns an id for the adverse event notification
     * 
     * @return the adverse event info id
     */
    public int getAdvNotifyId() {
        return this.advNotifyId;
    }

    /**
     * Sets the id for the adverse event notification
     * 
     * @param advNotifyId
     *            the adverse event notification id
     */
    public void setAdvNotifyId(int advNotifyId) {
        this.advNotifyId = advNotifyId;
    }

    /**
     * Gets id of the parent adverse event
     * 
     * @return id of the parent adverse event
     */
    public String getAdvNotifyAdverseId() {
        return this.advNotifyAdverseId;
    }

    /**
     * Sets the id for the parent adverse event
     * 
     * @param advInfoAdverseId
     *            the parent adverse event
     */
    public void setAdvNotifyAdverseId(String advNotifyAdverseId) {
        this.advNotifyAdverseId = advNotifyAdverseId;
    }

    /**
     * Gets notification type
     * 
     * @return notification type
     */

    public String getAdvNotifyCodelstNotTypeId() {
        return this.advNotifyCodelstNotTypeId;
    }

    /**
     * Sets notification type
     * 
     * @param advNotifyCodelstNotTypeId
     *            notification type
     */
    public void setAdvNotifyCodelstNotTypeId(String advNotifyCodelstNotTypeId) {
        this.advNotifyCodelstNotTypeId = advNotifyCodelstNotTypeId;
    }

    /**
     * Gets notification date
     * 
     * @return notification date
     */

    public String getAdvNotifyDate() {
        return this.advNotifyDate;
    }

    /**
     * Sets notification date
     * 
     * @param advNotifyDate
     *            notification date
     */
    public void setAdvNotifyDate(String advNotifyDate) {
        this.advNotifyDate = advNotifyDate;
    }

    /**
     * Gets notification notes
     * 
     * @return notification notes
     */
    public String getAdvNotifyNotes() {
        return this.advNotifyNotes;
    }

    /**
     * Sets notification notes
     * 
     * @param advNotifyNotes
     *            notification notes
     */
    public void setAdvNotifyNotes(String advNotifyNotes) {
        this.advNotifyNotes = advNotifyNotes;
    }

    /**
     * Gets notification flag. The flag determines if the notification is sent.
     * 
     * @return notification flag
     */
    public String getAdvNotifyValue() {
        return this.advNotifyValue;
    }

    /**
     * Sets notification flag. The flag determines if the notification is sent.
     * 
     * @param advNotifyValue
     *            notification flag
     */
    public void setAdvNotifyValue(String advNotifyValue) {
        this.advNotifyValue = advNotifyValue;
    }

    /**
     * Gets the user who created the adverse event notification
     * 
     * @return the user who created the adverse event notification
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * Sets the user who created the adverse event notification
     * 
     * @param creator
     *            the user who created the adverse event notification
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * Gets the user who modified the adverse event notification
     * 
     * @return the user who modified the adverse event notification
     */

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * Sets the user who modified the adverse event notification
     * 
     * @param modifiedBy
     *            the user who modified the adverse event notification
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * Gets the IP address of the user who created/modified the adverse event
     * info
     * 
     * @return IP address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * Sets the IP address of the user who created/modified the adverse event
     * info
     * 
     * @param ipAdd
     *            IP address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Class constructor: creates an adverse event notification object for an
     * adverse event notification id
     * 
     * @param advNotifyId
     *            adverse event notification id
     */
    public AdvNotifyJB(int advNotifyId) {
        setAdvNotifyId(advNotifyId);
    }

    /**
     * Default Constructor
     */
    public AdvNotifyJB() {
        Rlog.debug("advNotify", "AdvNotifyJB.AdvNotifyJB() ");
    }

    /**
     * Class constructor: full argument constructor
     * 
     * @param advNotifyId
     *            adverse event notification id
     * @param advNotifyAdverseId
     *            parent adverse event id
     * @param advNotifyCodelstNotTypeId
     *            notification code
     * @param advNotifyDate
     *            notification date
     * @param advNotifyNotes
     *            notification notes
     * @param advNotifyValue
     *            notification flag
     * @param creator
     *            creator
     * @param modifiedBy
     *            modified By
     * @param ipAdd
     *            IP Address
     */
    public AdvNotifyJB(int advNotifyId, String advNotifyAdverseId,
            String advNotifyCodelstNotTypeId, String advNotifyDate,
            String advNotifyNotes, String advNotifyValue, String creator,
            String modifiedBy, String ipAdd) {

        setAdvNotifyId(advNotifyId);
        setAdvNotifyAdverseId(advNotifyAdverseId);
        setAdvNotifyCodelstNotTypeId(advNotifyCodelstNotTypeId);
        setAdvNotifyDate(advNotifyDate);
        setAdvNotifyNotes(advNotifyNotes);
        setAdvNotifyNotes(advNotifyValue);

        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        Rlog.debug("advNotify", "AdvNotifyJB.AdvNotifyJB(all parameters)");
    }

    /**
     * Populates itself with the adverse event notification details using its
     * attribute : advNotifyId Calls getAdvNotifyDetails() on the Adverse Event
     * Notification Session Bean: AdvNotifyAgentBean. The session bean method
     * returns an AdvNotifyStateKeeper object that is used by this method to
     * populate the object's attributes.
     * 
     * @see AdvNotifyStateKeeper
     */
    public AdvNotifyBean getAdvNotifyDetails() {
        AdvNotifyBean edsk = null;

        try {
            AdvNotifyAgentRObj advNotifyAgent = EJBUtil.getAdvNotifyAgentHome();
            edsk = advNotifyAgent.getAdvNotifyDetails(this.advNotifyId);
            Rlog.debug("advNotify",
                    "AdvNotifyJB.getAdvNotifyDetails() AdvNotifyStateKeeper "
                            + edsk);
        } catch (Exception e) {
            Rlog.debug("advNotify",
                    "Error in getAdvNotifyDetails() in AdvNotifyJB " + e);
        }
        if (edsk != null) {
            this.advNotifyId = edsk.getAdvNotifyId();
            this.advNotifyAdverseId = edsk.getAdvNotifyAdverseId();
            this.advNotifyCodelstNotTypeId = edsk
                    .getAdvNotifyCodelstNotTypeId();
            this.advNotifyDate = edsk.getAdvNotifyDate();
            this.advNotifyNotes = edsk.getAdvNotifyNotes();
            this.advNotifyValue = edsk.getAdvNotifyValue();
            this.creator = edsk.getCreator();
            this.modifiedBy = edsk.getModifiedBy();
            this.ipAdd = edsk.getIpAdd();
        }
        return edsk;
    }

    /**
     * Creates new adverse event notification using the object's attributes.
     * Calls setAdvNotifyDetails() on the Adverse Event Notification Session
     * Bean: AdvNotifyAgentBean.
     */
    public void setAdvNotifyDetails() {

        try {

            AdvNotifyAgentRObj advNotifyAgent = EJBUtil.getAdvNotifyAgentHome();
            Rlog.debug("advNotify",
                    "AdvNotifyJB.advNotifyAgentHome.create() END");
            this.setAdvNotifyId(advNotifyAgent.setAdvNotifyDetails(this
                    .createAdvNotifyStateKeeper()));
            Rlog.debug("advNotify", "AdvNotifyJB.setAdvNotifyDetails()");
        } catch (Exception e) {
            Rlog.fatal("advNotify",
                    "Error in setAdvNotifyDetails() in AdvNotifyJB " + e);
        }
    }

    /**
     * Updates adverse event notification using the object's attributes. Calls
     * updateAdvNotify() on the Adverse Event Notification Session Bean:
     * AdvEveNotifyBean.
     * 
     * @return success flag
     *         <ul>
     *         <li> 0 if successful</li>
     *         <li> -2 in case of a failure</li>
     *         </ul>
     */
    public int updateAdvNotify() {
        int output;
        try {
            AdvNotifyAgentRObj advNotifyAgentRObj = EJBUtil
                    .getAdvNotifyAgentHome();
            output = advNotifyAgentRObj.updateAdvNotify(this
                    .createAdvNotifyStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("advNotify",
                    "EXCEPTION IN SETTING AdvNotify DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Returns an AdvNotifyStateKeeper object with the current values of the
     * Bean
     * 
     * @return AdvNotifyStateKeeper
     * @see AdvNotifyStateKeeper
     */

    public AdvNotifyBean createAdvNotifyStateKeeper() {

        return new AdvNotifyBean(advNotifyId, advNotifyAdverseId,
                advNotifyCodelstNotTypeId, advNotifyDate, advNotifyNotes,
                advNotifyValue, creator, modifiedBy, ipAdd);
    }

}// end of class
