package com.velos.esch.web.bgtSection;

/* IMPORT STATEMENTS*/

import java.util.Hashtable;

import com.velos.esch.business.bgtSection.impl.BgtSectionBean;
import com.velos.esch.business.common.BgtSectionDao;
import com.velos.esch.service.bgtSectionAgent.BgtSectionAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/*
 * * BgtSectionJB is a client side Java Bean for Budget Sections' business
 * components. The class implements the Business Delegate pattern to reduce
 * coupling between presentation-tier clients and business services.
 * 
 * <p> In eResearch Architecture, the class resides at the Application layer.
 * </p>
 * 
 * @author Sonia Sahni
 * 
 * @version %I%, %G%
 */

/*
 * *************************History********************************* Modified by
 * :Sonika Modified On:06/21/2004 Comment: 1. Added new attributes
 * bgtSectionPatNo, bgtSectionPersonlFlag, bgtSectionType, bgtSectionNotes and
 * respective getter/setters
 * *************************END****************************************
 */

public class BgtSectionJB {

    /**
     * bgtSection id
     */
    private int bgtSectionId;

    /**
     * bgtCal
     */
    private String bgtCal;

    /**
     * bgtSection bgtSectionName
     */
    private String bgtSectionName;

    /**
     * bgtSection bgtSectionVisit
     */

    private String bgtSectionVisit;

    /**
     * bgtSection Delete Flag (Whether the record has been deleted logically or
     * not)
     */
    private String bgtSectionDelFlag;

    /**
     * bgtSection Sequence
     */
    private String bgtSectionSequence;

    /*
     * creator
     */
    private String creator;

    /*
     * Modified By
     */

    private String modifiedBy;

    /*
     * IP Address
     */

    private String ipAdd;

    /*
     * budget section patient number
     */

    private String bgtSectionPatNo;

    /*
     * budget personnel section flag
     */

    private String bgtSectionPersonlFlag;

    /*
     * budget section type - One time fees, per patient fees
     */

    private String bgtSectionType;

    /*
     * budget section notes
     */

    private String bgtSectionNotes;

    //Added by IA 9/18/2006
    
    /*  Section Resarch Total ( cost/patient)
	
    */
    private String srtCostTotal;          


    /* Section Research Total ( Total Cost)
    	
    */
    private String srtCostGrandTotal;     


    /* Section SOC Total ( cost/patient)
    	
    */
    private String socCostTotal;          


    /* Section SCO Total ( Total Cost)
    	
    */
    private String socCostGrandTotal;  
    
    
    //End Added By IA 9/18/2006
    
    
    //Added by IA 11.03.2006
    private String socCostSponsorAmount;          


    /* Section SCO Total ( Total Cost)
    	
    */

    private String socCostVariance;          


    /* Section SCO Total ( Total Cost)
    	
    */

    private String srtCostSponsorAmount;          


    /* Section SCO Total ( Total Cost)
    	
    */

    private String srtCostVariance;          


    /* Section SCO Total ( Total Cost)
    	
    */

    
    // Start of Getter setter methods

    /**
     * Gets the budget section calendar
     * 
     * @return section calendar
     */

    public String getBgtCal() {
        return this.bgtCal;
    }

    /**
     * Sets the budget section calendar
     * 
     * @param budget
     *            section calendar
     */
    public void setBgtCal(String bgtCal) {
        this.bgtCal = bgtCal;
    }

    /**
     * Gets the budget section Id
     * 
     * @return section Id
     */
    public int getBgtSectionId() {
        return this.bgtSectionId;
    }

    /**
     * Sets the budget section Id
     * 
     * @param budget
     *            section Id
     */

    public void setBgtSectionId(int bgtSectionId) {
        this.bgtSectionId = bgtSectionId;
    }

    /**
     * Gets the budget section name
     * 
     * @return section name
     */

    public String getBgtSectionName() {
        return this.bgtSectionName;
    }

    /**
     * Sets the budget section name
     * 
     * @param budget
     *            section name
     */
    public void setBgtSectionName(String bgtSectionName) {
        this.bgtSectionName = bgtSectionName;
    }

    /**
     * Gets the budget section visit
     * 
     * @return section sequence
     */
    public String getBgtSectionVisit() {
        return this.bgtSectionVisit;
    }

    /**
     * Sets the budget section visit
     * 
     * @param budget
     *            section visit
     */

    public void setBgtSectionVisit(String bgtSectionVisit) {
        this.bgtSectionVisit = bgtSectionVisit;
    }

    /**
     * Gets the user who created the budget section
     * 
     * @return the user who created the budget section
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * Sets the user who created the budget section
     * 
     * @param creator
     *            the user who created the budget section
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * Gets the user who modified the budget section
     * 
     * @return the user who modified the budget section
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * Sets the user who modified the budget section
     * 
     * @param modifiedBy
     *            the user who modified the budget section
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * Gets the IP address of the user who created/modified the budget section
     * 
     * @return IP address
     */

    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * Sets the IP address of the user who created/modified the budget section
     * 
     * @param ipAdd
     *            IP address
     */

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    /**
     * Gets the budget section delete flag
     * 
     * @return delete flag
     */
    public String getBgtSectionDelFlag() {
        return this.bgtSectionDelFlag;
    }

    /**
     * Sets the budget section delete flag
     * 
     * @param budget
     *            section delete flag
     */
    public void setBgtSectionDelFlag(String bgtSectionDelFlag) {
        this.bgtSectionDelFlag = bgtSectionDelFlag;
    }

    /**
     * Gets the budget section sequence
     * 
     * @return section sequence
     */
    public String getBgtSectionSequence() {
        return this.bgtSectionSequence;
    }

    /**
     * Sets the budget section sequence
     * 
     * @param budget
     *            section sequence
     */
    public void setBgtSectionSequence(String bgtSectionSequence) {
        this.bgtSectionSequence = bgtSectionSequence;
    }

    /**
     * Gets the budget section patient number
     * 
     * @return section patient number
     */
    public String getBgtSectionPatNo() {
        return this.bgtSectionPatNo;
    }

    /**
     * Sets the budget section patient number
     * 
     * @param budget
     *            section patient number
     */
    public void setBgtSectionPatNo(String bgtSectionPatNo) {
        this.bgtSectionPatNo = bgtSectionPatNo;
    }

    /**
     * Gets the budget section personnel flag
     * 
     * @return section personnel flag
     */
    public String getBgtSectionPersonlFlag() {
        return this.bgtSectionPersonlFlag;
    }

    /**
     * Sets the budget section personnel flag
     * 
     * @param budget
     *            section personnel flag
     */
    public void setBgtSectionPersonlFlag(String bgtSectionPersonlFlag) {
        this.bgtSectionPersonlFlag = bgtSectionPersonlFlag;
    }

    /**
     * Gets the budget section type
     * 
     * @return section type
     */
    public String getBgtSectionType() {
        return this.bgtSectionType;
    }

    /**
     * Sets the budget section type
     * 
     * @param budget
     *            section type
     */
    public void setBgtSectionType(String bgtSectionType) {
        this.bgtSectionType = bgtSectionType;
    }

    /**
     * Gets the budget section notes
     * 
     * @return section notes
     */
    public String getBgtSectionNotes() {
        return this.bgtSectionNotes;
    }

    /**
     * Sets the budget section notes
     * 
     * @param budget
     *            section notes
     */
    public void setBgtSectionNotes(String bgtSectionNotes) {
        this.bgtSectionNotes = bgtSectionNotes;
    }
    
    // Added by IA 9/18/2006
    
    public String getSrtCostTotal() {
        return this.bgtSectionNotes;
    }

    /**
     * Sets the Section Research Total ( cost/patient )
     * 
     * @param budget
     *            section cost
     */
    public void setSrtCostTotal(String srtCostTotal) {
        this.srtCostTotal = srtCostTotal;
    }
    
    public String getSrtCostGrandTotal() {
        return this.srtCostGrandTotal;
    }

    /**
     * Sets the Section Research Total ( Total cost )
     *  
     * @param budget
     *            section cost
     */
    public void setSrtCostGrandTotal(String srtCostGrandTotal) {
        this.srtCostGrandTotal = srtCostGrandTotal;
    }
    
    
    public String getSocCostTotal() {
        return this.socCostTotal;
    }

    /**
     * Sets the Section Research Total ( Total cost )
     *  
     * @param budget
     *            section cost
     */
    public void setSocCostTotal(String socCostTotal) {
        this.socCostTotal = socCostTotal;
    }
    
    
    public String getSocCostGrandTotal() {
        return this.socCostGrandTotal;
    }

    /**
     * Sets the Section Research Total ( Total cost )
     *  
     * @param budget
     *            section cost
     */
    public void setSocCostGrandTotal(String socCostGrandTotal) {
        this.socCostGrandTotal = socCostGrandTotal;
    }
    
   // End added by IA 9/18/2006
    
    
    //Added by IA 11.03.2006
    
    public String getSocCostSponsorAmount() {
        return this.socCostSponsorAmount;
    }

    /**
     * Sets the Section Research Total ( Total cost )
     *  
     * @param budget
     *            section cost
     */
    public void setSocCostSponsorAmount(String socCostSponsorAmount) {
        this.socCostSponsorAmount= socCostSponsorAmount;
    }
    
    
    public String getSocCostVariance() {
        return this.socCostVariance;
    }

    /**
     * Sets the Section Research Total ( Total cost )
     *  
     * @param budget
     *            section cost
     */
    public void setSocCostVariance(String socCostVariance) {
        this.socCostVariance= socCostVariance;
    }
    
    
    public String getSrtCostSponsorAmount() {
        return this.srtCostSponsorAmount;
    }

    /**
     * Sets the Section Research Total ( Total cost )
     *  
     * @param budget
     *            section cost
     */
    public void setSrtCostSponsorAmount(String srtCostSponsorAmount) {
        this.srtCostSponsorAmount = srtCostSponsorAmount;
    }
    
    
    public String getSrtCostVariance() {
        return this.srtCostVariance;
    }

    /**
     * Sets the Section Research Total ( Total cost )
     *  
     * @param budget
     *            section cost
     */
    public void setSrtCostVariance(String srtCostVariance) {
        this.srtCostVariance = srtCostVariance;
    }
    
    
    
    
    //End added

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts BgtSection Id
     * 
     * @param bgtSectionId
     *            Id of the BgtSection
     */
    public BgtSectionJB(int bgtSectionId) {
        setBgtSectionId(bgtSectionId);
    }

    /**
     * Default Constructor
     * 
     */
    public BgtSectionJB() {
        Rlog.debug("bgtSection", "BgtSectionJB.BgtSectionJB() ");
    }

    public BgtSectionJB(int bgtSectionId, String bgtCal, String bgtSectionName,
            String bgtSectionVisit, String bgtSectionDelFlag,
            String bgtSectionSequence, String creator, String modifiedBy,
            String ipAdd, String bgtSectionPatNo, String bgtSectionPersonlFlag,
            String bgtSectionType, String bgtSectionNotes, String srtCostTotal, 
            String srtCostGrandTotal, String socCostTotal, String socCostGrandTotal,
            String srtCostSponsorAmount, String srtCostVariance, String socCostSponsorAmount,
            String socCostVariance
) {
        setBgtSectionId(bgtSectionId);
        setBgtCal(bgtCal);
        setBgtSectionName(bgtSectionName);
        setBgtSectionVisit(bgtSectionVisit);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setBgtSectionDelFlag(bgtSectionDelFlag);
        setBgtSectionSequence(bgtSectionSequence);
        setBgtSectionPatNo(bgtSectionPatNo);
        setBgtSectionPersonlFlag(bgtSectionPersonlFlag);
        setBgtSectionType(bgtSectionType);
        setBgtSectionNotes(bgtSectionNotes);
        setSrtCostTotal(srtCostTotal);
        setSrtCostGrandTotal(srtCostGrandTotal);
        setSocCostTotal(socCostTotal);
   
        //Added by IA 11.03.2006
        setSocCostSponsorAmount(socCostSponsorAmount);
        setSocCostVariance(socCostVariance);
        setSrtCostSponsorAmount(socCostSponsorAmount);
        setSrtCostVariance(socCostVariance);
        //End added

        Rlog.debug("bgtSection", "BgtSectionJB.BgtSectionJB(all parameters)");
    }

    /**
     * Calls getBgtSectionDetails() of BgtSection Session Bean:
     * BgtSectionAgentBean
     * 
     * 
     * @return The Details of the bgtSection in the BgtSection StateKeeper
     *         Object
     * @See BgtSectionAgentBean
     */

    public BgtSectionBean getBgtSectionDetails() {
        BgtSectionBean edsk = null;

        try {
            BgtSectionAgentRObj bgtSectionAgent = EJBUtil
                    .getBgtSectionAgentHome();
            edsk = bgtSectionAgent.getBgtSectionDetails(this.bgtSectionId);
            Rlog.debug("bgtSection",
                    "BgtSectionJB.getBgtSectionDetails() BgtSectionStateKeeper "
                            + edsk);
        } catch (Exception e) {
            Rlog.debug("bgtSection",
                    "Error in getBgtSectionDetails() in BgtSectionJB " + e);
        }
        if (edsk != null) {
            setBgtSectionId(edsk.getBgtSectionId());
            setBgtCal(edsk.getBgtCal());
            setBgtSectionName(edsk.getBgtSectionName());
            setBgtSectionVisit(edsk.getBgtSectionVisit());
            setBgtSectionDelFlag(edsk.getBgtSectionDelFlag());
            setBgtSectionSequence(edsk.getBgtSectionSequence());
            setCreator(edsk.getCreator());
            setModifiedBy(edsk.getModifiedBy());
            setIpAdd(edsk.getIpAdd());
            setBgtSectionPatNo(edsk.getBgtSectionPatNo());
            setBgtSectionPersonlFlag(edsk.getBgtSectionPersonlFlag());
            setBgtSectionType(edsk.getBgtSectionType());
            setBgtSectionNotes(edsk.getBgtSectionNotes());
            setSrtCostTotal(edsk.getSrtCostTotal());
            setSrtCostGrandTotal(edsk.getSrtCostGrandTotal());
            setSocCostTotal(edsk.getSocCostTotal());
            setSocCostGrandTotal(edsk.getSocCostGrandTotal());
            
            //Added by IA 11.03.2006
            
            setSocCostSponsorAmount(edsk.getSocCostSponsorAmount());
            setSocCostVariance(edsk.getSocCostVariance());
            setSrtCostSponsorAmount(edsk.getSrtCostSponsorAmount());
            setSrtCostVariance(edsk.getSrtCostVariance());
            //end added

            

        }
        return edsk;
    }

    /**
     * 
     */

    public void setBgtSectionDetails() {

        int bgtSecId = 0;
        int bgtCalId = 0;
        int creator = 0;
        String ipAdd = "";
        int ret = 0;
        try {

            BgtSectionAgentRObj bgtSectionAgent = EJBUtil
                    .getBgtSectionAgentHome();
            bgtSecId = bgtSectionAgent.setBgtSectionDetails(this
                    .createBgtSectionStateKeeper());
            Rlog.debug("bgtSection",
                    "BgtSectionJB.setBgtSectionDetails() bgtSecId" + bgtSecId);
            this.setBgtSectionId(bgtSecId);

            if (bgtSecId > 0) {
                bgtCalId = EJBUtil.stringToNum(this.getBgtCal());

                creator = EJBUtil.stringToNum(this.getCreator());

                ipAdd = this.getIpAdd();

                ret = insertDefaultSecLineitems(bgtSecId, bgtCalId, creator,
                        ipAdd);

            }

        } catch (Exception e) {
            Rlog.fatal("bgtSection",
                    "Error in setBgtSectionDetails() in BgtSectionJB " + e);
        }
    }

    /**
     * Calls updateBgtSection() of BgtSection Session Bean: BgtSectionAgentBean
     * 
     * @return
     */
    public int updateBgtSection() {
        int output;
        try {

            BgtSectionAgentRObj bgtSectionAgentRObj = EJBUtil
                    .getBgtSectionAgentHome();
            output = bgtSectionAgentRObj.updateBgtSection(this
                    .createBgtSectionStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("bgtSection",
                    "EXCEPTION IN SETTING BgtSection DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    
    //Overloaded for INF-18183 ::: Ankit
    public int updateBgtSection(Hashtable<String, String> auditInfo) {
        int output;
        try {

            BgtSectionAgentRObj bgtSectionAgentRObj = EJBUtil
                    .getBgtSectionAgentHome();
            output = bgtSectionAgentRObj.updateBgtSection(this
                    .createBgtSectionStateKeeper(), auditInfo);
        } catch (Exception e) {
            Rlog.fatal("bgtSection",
                    "EXCEPTION IN SETTING BgtSection DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    /**
     * 
     * 
     * @return the BgtSection StateKeeper Object with the current values of the
     *         Bean
     */

    public BgtSectionBean createBgtSectionStateKeeper() {

        return new BgtSectionBean(bgtSectionId, bgtCal, bgtSectionName,
                bgtSectionVisit, bgtSectionSequence, bgtSectionDelFlag,
                creator, modifiedBy, ipAdd, bgtSectionPatNo,
                bgtSectionPersonlFlag, bgtSectionType, bgtSectionNotes, 
                srtCostTotal, srtCostGrandTotal, socCostTotal, socCostGrandTotal, 
                srtCostSponsorAmount, srtCostVariance, socCostSponsorAmount, socCostVariance);
    }

    /**
     * Calls getBudgetSections() of BgtSection Session Bean: BgtSectionAgentBean
     * 
     * @param calId
     *            calendar Id
     * @retun BgtSectionDao
     */
    public BgtSectionDao getBgtSections(int calId) {
        Rlog.debug("bgtSection", "BgtSectionJB.getBudgetSections");
        BgtSectionDao bgtSectionDao = new BgtSectionDao();
        try {
            BgtSectionAgentRObj bgtSectionAgentRObj = EJBUtil
                    .getBgtSectionAgentHome();
            bgtSectionDao = bgtSectionAgentRObj.getBgtSections(calId);
        } catch (Exception e) {
            Rlog.fatal("bgtSection", "BgtSectionJB.getBudgetSections " + e);
            e.printStackTrace();
            return bgtSectionDao;
        }
        return bgtSectionDao;
    }

    /**
     * Delete Section from budget
     * 
     * @return 0 for successful delete, -2 in case of exception
     */
    public int bgtSectionDelete(int bgtSectionId) {
        try {
            int ret = 0;
            Rlog.debug("bgtSection", "BgtSectionJB BgtSectionDelete");
            BgtSectionAgentRObj bgtSectionAgentRObj = EJBUtil
                    .getBgtSectionAgentHome();
            ret = bgtSectionAgentRObj.bgtSectionDelete(bgtSectionId);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("bgtSection", "Error in BgtSectionJB BgtSectionDelete "
                    + e);
            return -2;
        }

    }

    public int insertDefaultSecLineitems(int bgtSecId, int bgtCalId,
            int creator, String ipAdd)

    {
        try {
            int ret = 0;
            Rlog.debug("bgtSection", "BgtSectionJB BgtSectionDelete");
            BgtSectionAgentRObj bgtSectionAgentRObj = EJBUtil
                    .getBgtSectionAgentHome();
            ret = bgtSectionAgentRObj.insertDefaultSecLineitems(bgtSecId,
                    bgtCalId, creator, ipAdd);
            return ret;
        } catch (Exception e) {
            Rlog.fatal("bgtSection",
                    "Error in BgtSectionJB insertDefaultSecLineitems " + e);
            return -2;
        }

    }
    
    /**
     * Get all the sections (with calendar names) for a budget
     * 
     * @param budgetId
     */

    public BgtSectionDao  getAllBgtSections(int budgetId)
    {
    	 Rlog.debug("bgtSection", "BgtSectionJB.getAllBgtSections");
         BgtSectionDao bgtSectionDao = new BgtSectionDao();
         try {
             BgtSectionAgentRObj bgtSectionAgentRObj = EJBUtil
                     .getBgtSectionAgentHome();
             bgtSectionDao = bgtSectionAgentRObj.getAllBgtSections(budgetId);
         } catch (Exception e) {
             Rlog.fatal("bgtSection", "BgtSectionJB.getAllBgtSections " + e);
             e.printStackTrace();
             return bgtSectionDao;
         }
         return bgtSectionDao;
    }

    /** 
     * Update patient number for all per patient sections in a budget
     *  */
    public int applyNumberOfPatientsToAllSections(String sourceSectionPK,String bgtCalPK,String patNo,String last_modified_by,String ipAdd)
    {
   	  int ret = -1;
   	  
     try {
         BgtSectionAgentRObj bgtSectionAgentRObj = EJBUtil
                 .getBgtSectionAgentHome();
         ret= bgtSectionAgentRObj.applyNumberOfPatientsToAllSections(sourceSectionPK,bgtCalPK,patNo,last_modified_by,ipAdd);
         
         return ret;
     } catch (Exception e) {
         Rlog.fatal("bgtSection", "Exception in BgtSectionJB.applyNumberOfPatientsToAllSections" + e);
         e.printStackTrace();
         return ret;
     }

    }
    
}// end of class
