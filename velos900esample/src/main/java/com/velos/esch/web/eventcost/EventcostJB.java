/*
 * Classname : EventcostJB
 * 
 * Version information: 1.0 
 *
 * Date: 06/21/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Dinesh Khurana
 */

package com.velos.esch.web.eventcost;

/* IMPORT STATEMENTS */

import java.util.Hashtable;
import com.velos.esch.business.common.EventdefDao;
import com.velos.esch.business.eventcost.impl.EventcostBean;
import com.velos.esch.service.eventcostAgent.EventcostAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

public class EventcostJB {

    /**
     * eventcostId
     */
    private int eventcostId;

    /**
     * eventcostValue
     */
    private String eventcostValue;

    /**
     * eventId
     */
    private String eventId;

    /**
     * eventcostDescId
     */
    private String eventcostDescId;

    /**
     * currencyId
     */
    private String currencyId;

    /*
     * creator
     */
    private String creator;

    /*
     * last modified by
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    // GETTER SETTER METHODS

    // Start of Getter setter methods

    public int getEventcostId() {
        return this.eventcostId;
    }

    public void setEventcostId(int eventcostId) {
        this.eventcostId = eventcostId;
    }

    public String getEventcostValue() {
        return this.eventcostValue;
    }

    public void setEventcostValue(String eventcostValue) {
        this.eventcostValue = eventcostValue;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventcostDescId() {
        return this.eventcostDescId;
    }

    public void setEventcostDescId(String eventcostDescId) {
        this.eventcostDescId = eventcostDescId;
    }

    public String getCurrencyId() {
        return this.currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getIpAdd() {
        return this.ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts Eventcost Id
     * 
     * @param eventcostId
     *            Id of the Eventcost
     */

    public EventcostJB(int eventcostId) {
        setEventcostId(eventcostId);
    }

    /**
     * Default Constructor
     * 
     */
    public EventcostJB() {
        Rlog.debug("eventcost", "EventcostJB.EventcostJB() ");
    }

    public EventcostJB(int eventcostId, String eventcostDesc,
            String eventcostValue, String eventId, String eventcostDescId,
            String currencyId, String creator, String modifiedBy, String ipAdd) {

        setEventcostId(eventcostId);
        setEventcostValue(eventcostValue);
        setEventId(eventId);
        setEventcostDescId(eventcostDescId);
        setCurrencyId(currencyId);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);

        Rlog.debug("eventcost", "EventcostJB.EventcostJB(all parameters)");
    }

    /**
     * Calls getEventcostDetails() of Eventcost Session Bean: EventcostAgentBean
     * 
     * 
     * @return The Details of the eventcost in the Eventcost StateKeeper Object
     * @See EventcostAgentBean
     */

    public EventcostBean getEventcostDetails() {
        EventcostBean ecsk = null;

        try {

            EventcostAgentRObj eventcostAgent = EJBUtil.getEventcostAgentHome();
            ecsk = eventcostAgent.getEventcostDetails(this.eventcostId);
            Rlog.debug("eventcost",
                    "EventcostJB.getEventcostDetails() EventcostStateKeeper "
                            + ecsk);
        } catch (Exception e) {
            Rlog.debug("eventcost",
                    "Error in getEventcostDetails() in EventcostJB " + e);
        }
        if (ecsk != null) {

            this.eventcostId = ecsk.getEventcostId();
            this.eventcostValue = ecsk.getEventcostValue();
            this.eventId = ecsk.getEventId();
            this.eventcostDescId = ecsk.getEventcostDescId();
            this.currencyId = ecsk.getCurrencyId();
            this.creator = ecsk.getCreator();
            this.modifiedBy = ecsk.getModifiedBy();
            this.ipAdd = ecsk.getIpAdd();
        }
        return ecsk;
    }

    /**
     * Calls setEventcostDetails() of Eventcost Session Bean: EventcostAgentBean
     * 
     */

    public void setEventcostDetails() {

        try {

            EventcostAgentRObj eventcostAgent = EJBUtil.getEventcostAgentHome();
            this.eventcostId = eventcostAgent.setEventcostDetails(this
                    .createEventcostStateKeeper());
            Rlog.debug("eventcost", "EventcostJB.setEventcostDetails()");
        } catch (Exception e) {
            Rlog.debug("eventcost",
                    "Error in setEventcostDetails() in EventcostJB " + e);
        }
    }

    /**
     * Calls updateEventcost() of Eventcost Session Bean: EventcostAgentBean
     * 
     * @return
     */
    public int updateEventcost() {
        int output;
        try {

            EventcostAgentRObj eventcostAgentRObj = EJBUtil
                    .getEventcostAgentHome();
            output = eventcostAgentRObj.updateEventcost(this
                    .createEventcostStateKeeper());
        } catch (Exception e) {
            Rlog.debug("eventcost",
                    "EXCEPTION IN SETTING USER DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * Calls propagateEventUpdates() of eventdefDao.
     * 
     * @return
     */
    public int propagateEventcost(int protocol_id, String propagateInVisitFlag,
            String propagateInEventFlag, String mode, String calType) {
        int output = 0;
        EventcostBean ecsk;
        EventdefDao eventdefdao = new EventdefDao();
        try {

            EventcostAgentRObj eventcostAgentRObj = EJBUtil
                    .getEventcostAgentHome();
            ecsk = this.createEventcostStateKeeper();
            
            Rlog.debug("eventcost", "ecsk.getEventId()" + ecsk.getEventId());
            Rlog.debug("eventcost", "ecsk.getEventcostId()" + ecsk.getEventcostId());
            
            System.out.println("ecsk.getEventId()" + ecsk.getEventId());
			System.out.println("ecsk.getEventcostId()" + ecsk.getEventcostId());
            
            output = eventdefdao.propagateEventUpdates(protocol_id, EJBUtil
                    .stringToNum(ecsk.getEventId()), "EVENT_COST", ecsk
                    .getEventcostId(), propagateInVisitFlag,
                    propagateInEventFlag, mode, calType);
        }

        catch (Exception e) {
            Rlog.debug("eventcost",
                    "EXCEPTION IN PROPAGATING EVENT COST DETAILS");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    public int removeEventcost(int eventId, int eventcostId, String mode) {
        int output = 0;
        EventdefDao eventdefdao = new EventdefDao();
        try {            
            System.out.println("EventId" + eventId);
			System.out.println("EventcostId" + eventcostId);
            
            output = eventdefdao.removeEventCost(eventId, eventcostId, mode);
        }

        catch (Exception e) {
            Rlog.debug("eventcost",
                    "EXCEPTION IN REMOVING EVENT COST DETAILS");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    // removeEventcost() Overloaded for INF-18183 ::: Raviesh
    public int removeEventcost(int eventId, int eventcostId, String mode,Hashtable<String, String> args) {
        int output = 0;
        EventdefDao eventdefdao = new EventdefDao();
        try {            
            System.out.println("EventId" + eventId);
			System.out.println("EventcostId" + eventcostId);
            
            output = eventdefdao.removeEventCost(eventId, eventcostId, mode,args);
        }

        catch (Exception e) {
            Rlog.debug("eventcost",
                    "EXCEPTION IN REMOVING EVENT COST DETAILS");
            e.printStackTrace();
            return -2;
        }
        return output;
    }
    
    /**
     * 
     * 
     * @return the Eventcost StateKeeper Object with the current values of the
     *         Bean
     */

    public EventcostBean createEventcostStateKeeper() {

        return new EventcostBean(eventcostId, eventcostValue, eventId,
                eventcostDescId, currencyId, creator, modifiedBy, ipAdd);
    }

    /*
     * generates a String of XML for the eventcost details entry form.<br><br>
     * NOT IN USE
     */

    public String toXML() {
        Rlog.debug("eventcost", "EventcostJB.toXML()");
        return "test";
        // to be implemented later
        /*
         * return new String("<?xml version=\"1.0\"?>" + "<?cocoon-process
         * type=\"xslt\"?>" + "<?xml-stylesheet
         * href=\"/stylesheet/form-html.xsl\" type=\"text/xsl\"?>" + "<?xml-stylesheet
         * href=\"/stylesheet/form-wml.xsl\" type=\"text/xsl\" media=\"wap\"?>" + "<head>" + "</head>" + "<input
         * type=\"hidden\" name=\"eventcostId\" value=\"" +
         * this.getEventcostId()+ "\" size=\"10\"/>" + "<input type=\"text\"
         * name=\"eventcostCodelstJobtype\" value=\"" +
         * this.getEventcostCodelstJobtype() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventcostAccountId\" value=\"" +
         * this.getEventcostAccountId() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventcostSiteId\" value=\"" +
         * this.getEventcostSiteId() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventcostPerAddressId\" value=\"" +
         * this.getEventcostPerAddressId() + "\" size=\"38\"/>" + "<input
         * type=\"text\" name=\"eventcostLastName\" value=\"" +
         * this.getEventcostLastName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"eventcostFirstName\" value=\"" +
         * this.getEventcostFirstName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"eventcostMidName \" value=\"" +
         * this.getEventcostMidName() + "\" size=\"30\"/>" + "<input
         * type=\"text\" name=\"eventcostWrkExp\" value=\"" +
         * this.getEventcostWrkExp() + "\" size=\"100\"/>" + "<input
         * type=\"text\" name=\"eventcostPhaseInv\" value=\"" +
         * this.getEventcostPhaseInv() + "\" size=\"100\"/>" + "<input
         * type=\"text\" name=\"eventcostSessionTime\" value=\"" +
         * this.getEventcostSessionTime() + "\" size=\"3\"/>" + "<input
         * type=\"text\" name=\"eventcostLoginName\" value=\"" +
         * this.getEventcostLoginName() + "\" size=\"20\"/>" + "<input
         * type=\"text\" name=\"eventcostPwd\" value=\"" +
         * this.getEventcostPwd() + "\" size=\"20\"/>" + "<input type=\"text\"
         * name=\"eventcostSecQues\" value=\"" + this.getEventcostSecQues() +
         * "\" size=\"150\"/>" + "<input type=\"text\" name=\"eventcostAnswer\"
         * value=\"" + this.getEventcostAnswer() + "\" size=\"150\"/>" + "<input
         * type=\"text\" name=\"eventcostStatus\" value=\"" +
         * this.getEventcostStatus() + "\" size=\"1\"/>" + "<input
         * type=\"text\" name=\"eventcostCodelstSpl\" value=\"" +
         * this.getEventcostCodelstSpl() + "\" size=\"10\"/>" + "<input
         * type=\"text\" name=\"eventcostGrpDefault\" value=\"" +
         * this.getEventcostGrpDefault() + "\" size=\"10\"/>" + "</form>" );
         */

    }// end of method

}// end of class
