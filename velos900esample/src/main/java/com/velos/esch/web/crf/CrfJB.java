/*
 * Classname : CrfJB
 * 
 * Version information: 1.0 
 *
 * Date: 11/23/2001
 * 
 * Copyright notice: Velos, Inc
 *
 * Author: Arvind
 */

package com.velos.esch.web.crf;

/* IMPORT STATEMENTS */

import com.velos.esch.business.common.CrfDao;
import com.velos.esch.business.crf.impl.CrfBean;
import com.velos.esch.service.crfAgent.CrfAgentRObj;
import com.velos.esch.service.util.EJBUtil;
import com.velos.esch.service.util.Rlog;

/* END OF IMPORT STATEMENTS */

/**
 * Client side Java Bean for CRF
 * 
 * @author Arvind
 * @version 1.0 11/23/2001
 * 
 */

public class CrfJB {

    /**
     * crf id
     */
    private int crfId;

    /**
     * event1 id
     */
    private String events1Id;

    /**
     * crf number
     */
    private String crfNumber;

    /**
     * crf name
     */

    private String crfName;

    /*
     * creator
     */
    private String creator;

    /*
     * creator
     */
    private String modifiedBy;

    /*
     * IP Address
     */
    private String ipAdd;

    /**
     * PatProt id
     */
    private String patProtId;

    /**
     * CRF Flag (whether CRF or Other form)
     */
    private String crfFlag;

    /**
     * CRF Delete Flag (Whether the record has been deleted logically or not)
     */
    private String crfDelFlag;

    // GETTER SETTER METHODS

    public int getCrfId() {
        return this.crfId;
    }

    public void setCrfId(int crfId) {
        this.crfId = crfId;
    }

    public String getEvents1Id() {
        return this.events1Id;
    }

    public void setEvents1Id(String events1Id) {
        this.events1Id = events1Id;
    }

    public String getCrfNumber() {
        return this.crfNumber;
    }

    public void setCrfNumber(String crfNumber) {
        this.crfNumber = crfNumber;
    }

    public String getCrfName() {
        return this.crfName;
    }

    public void setCrfName(String crfName) {
        this.crfName = crfName;
    }

    /**
     * @return Creator
     */
    public String getCreator() {
        return this.creator;
    }

    /**
     * @param creator
     *            The value that is required to be registered as Creator of the
     *            record
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return IP Address
     */
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return IP Address
     */
    public String getIpAdd() {
        return this.ipAdd;
    }

    /**
     * @param ipAdd
     *            The value that is required to be registered as the IP Address
     */
    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    public String getPatProtId() {
        return this.patProtId;
    }

    public void setPatProtId(String patProtId) {
        this.patProtId = patProtId;
    }

    public String getCrfFlag() {
        return this.crfFlag;
    }

    public void setCrfFlag(String crfFlag) {
        this.crfFlag = crfFlag;
    }

    public String getCrfDelFlag() {
        return this.crfDelFlag;
    }

    public void setCrfDelFlag(String crfDelFlag) {
        this.crfDelFlag = crfDelFlag;
    }

    // END OF GETTER SETTER METHODS

    /**
     * Constructor; Accepts Crf Id
     * 
     * @param crfId
     *            Id of the Crf
     */
    public CrfJB(int crfId) {
        setCrfId(crfId);
    }

    /**
     * Default Constructor
     * 
     */
    public CrfJB() {
        Rlog.debug("crf", "CrfJB.CrfJB() ");
    }

    public CrfJB(int crfId, String events1Id, String crfNumber, String crfName,
            String creator, String modifiedBy, String ipAdd, String patProtId,
            String crfFlag, String crfDelFlag) {

        setCrfId(crfId);
        setEvents1Id(events1Id);
        setCrfNumber(crfNumber);
        setCrfName(crfName);
        setCreator(creator);
        setModifiedBy(modifiedBy);
        setIpAdd(ipAdd);
        setPatProtId(patProtId);
        setCrfFlag(crfFlag);
        setCrfDelFlag(crfDelFlag);
        Rlog.debug("crf", "CrfJB.CrfJB(all parameters)");
    }

    /**
     * Calls getCrfDetails() of Crf Session Bean: CrfAgentBean
     * 
     * 
     * @return The Details of the crf in the Crf StateKeeper Object
     * @See CrfAgentBean
     */

    public CrfBean getCrfDetails() {
        CrfBean edsk = null;

        try {
            CrfAgentRObj crfAgent = EJBUtil.getCrfAgentHome();
            edsk = crfAgent.getCrfDetails(this.crfId);
            Rlog.debug("crf", "CrfJB.getCrfDetails() CrfStateKeeper " + edsk);
        } catch (Exception e) {
            Rlog.debug("crf", "Error in getCrfDetails() in CrfJB " + e);
        }
        if (edsk != null) {
            this.crfId = edsk.getCrfId();
            this.events1Id = edsk.getEvents1Id();
            this.crfNumber = edsk.getCrfNumber();
            this.crfName = edsk.getCrfName();
            this.creator = edsk.getCreator();
            this.modifiedBy = edsk.getModifiedBy();
            this.ipAdd = edsk.getIpAdd();
            this.patProtId = edsk.getPatProtId();
            this.crfFlag = edsk.getCrfFlag();
            this.crfDelFlag = edsk.getCrfDelFlag();
        }
        return edsk;
    }

    /**
     * Calls setCrfDetails() of Crf Session Bean: CrfAgentBean
     * 
     */

    public void setCrfDetails() {

        try {
            CrfAgentRObj crfAgent = EJBUtil.getCrfAgentHome();
            this.setCrfId(crfAgent.setCrfDetails(this.createCrfStateKeeper()));
            Rlog.debug("crf", "CrfJB.setCrfDetails()");
        } catch (Exception e) {
            Rlog.fatal("crf", "Error in setCrfDetails() in CrfJB " + e);
        }
    }

    /**
     * Calls updateCrf() of Crf Session Bean: CrfAgentBean
     * 
     * @return
     */
    public int updateCrf() {
        int output;
        try {

            CrfAgentRObj crfAgentRObj = EJBUtil.getCrfAgentHome();
            output = crfAgentRObj.updateCrf(this.createCrfStateKeeper());
        } catch (Exception e) {
            Rlog.fatal("crf",
                    "EXCEPTION IN SETTING CRF DETAILS TO  SESSION BEAN");
            e.printStackTrace();
            return -2;
        }
        return output;
    }

    /**
     * 
     * 
     * @return the Crf StateKeeper Object with the current values of the Bean
     */

    public CrfBean createCrfStateKeeper() {

        return new CrfBean(crfId, events1Id, crfNumber, crfName, creator,
                modifiedBy, ipAdd, patProtId, crfFlag, crfDelFlag);
    }

    public CrfDao getCrfValues(int patProtId) {
        CrfDao crfDao = new CrfDao();
        try {
            CrfAgentRObj crfAgent = EJBUtil.getCrfAgentHome();
            crfDao = crfAgent.getCrfValues(patProtId);
        } catch (Exception e) {
            Rlog.fatal("crf", "Error in getCrfValues in CrfJB " + e);
        }
        return crfDao;
    }

}// end of class
