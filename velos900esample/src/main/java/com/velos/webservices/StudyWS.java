/**
 *
 */
package com.velos.webservices;
   
import java.lang.management.ManagementFactory;
import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.xml.ws.WebServiceContext;

import org.apache.log4j.Logger;

import com.velos.services.Issue;
import com.velos.services.IssueTypes;
import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.StudyClient;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.Study;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyOrganization;
import com.velos.services.model.StudyStatus;
import com.velos.services.model.StudyStatusIdentifier;
import com.velos.services.model.StudyStatuses;
import com.velos.services.model.StudySummary;
import com.velos.services.model.StudyTeamMember;
import com.velos.services.model.UserIdentifier;

/**
 * @author dylan
 * 
 */
@WebService(
		serviceName = "StudyService", 
		endpointInterface = "com.velos.webservices.StudySEI", 
		targetNamespace = "http://velos.com/services/")	  
public class StudyWS implements StudySEI{
	
	private static Logger logger = Logger.getLogger(StudyWS.class.getName());
	
	@Resource
	private WebServiceContext context;
 
 
	public StudyWS() { 
 
		  MBeanServer mbs = ManagementFactory.getPlatformMBeanServer(); 
	      ObjectName name;
		try {
			name = new ObjectName("com.velos:type=ServiceMonitor,name=Study");
			ServiceMonitor mbean = new ServiceMonitor(); 
			if (mbs.isRegistered(name)) { mbs.unregisterMBean(name); }
			mbs.registerMBean(mbean, name); 
		} catch (Throwable t) {
			logger.error("Error creating MBean for ServiceMonitor", t);
		}
	}
         
	public ResponseHolder createStudy(Study study, boolean createNonSystemUsers)
			throws OperationException {

		ResponseHolder response = new ResponseHolder();
		try {
			response = StudyClient.create(study, createNonSystemUsers);
			
		} catch (OperationException e) {
			logger.error("create study", e);
			throw e;
		} catch (Throwable t) {
			logger.error("create study", t);
			response.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return response;
	}

	public ResponseHolder updateStudySummary(StudyIdentifier studyId,
			StudySummary summary, boolean createNonSystemUsers)

	throws OperationException {

		ResponseHolder response = new ResponseHolder();
		try {
			response = StudyClient.updateStudySummary(studyId, summary,
					createNonSystemUsers);

		} catch (OperationException e) {
			logger.error("create study", e);
			throw e;
		} catch (Throwable t) {
			logger.error("create study", t);
			response.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return response;
	}
 
	public Study getStudy(StudyIdentifier studyId) throws OperationException {

		try {

			Study fetchedStudy = StudyClient.getStudy(studyId);
			context.getMessageContext().put("userN", context.getUserPrincipal().getName());
			return fetchedStudy;
		} catch (OperationException e) {
			logger.error("getStudy", e);
			context.getMessageContext().put(Constants.ERROR_STRING_KEY, e.getMessage());
			throw e;
		} catch (Throwable t) {
			logger.error("getStudy", t); 
			throw new OperationException(t);
		}
	 		
	}
	  
	public StudySummary getStudySummary(StudyIdentifier studyId) throws OperationException {

		try {
			StudySummary fetchedStudySummary = StudyClient.getStudySummary(studyId);
			return fetchedStudySummary;
		} catch (OperationException e) {
			logger.error("getStudy", e);
			throw e;
		} catch (Throwable t) {
			logger.error("getStudy", t);
			throw new OperationException(t);
		}
 
	}


	
	

	public ResponseHolder addStudyOrganization(
			StudyIdentifier studyIdentifier,
			StudyOrganization studyOrganization) throws OperationException {
		ResponseHolder response = new ResponseHolder();
		try {
			response = StudyClient.addStudyOrganization(studyIdentifier, studyOrganization);  

		} catch (OperationException e) {
			logger.error("addStudyOrganization", e);
			throw e;
		} catch (Throwable t) {
			logger.error("addStudyOrganization", t);
			response.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return response;
	}  

	public ResponseHolder addStudyStatus(
			StudyIdentifier studyIdentifier,
			StudyStatus studyStatus) throws OperationException {
		ResponseHolder response = new ResponseHolder();
		try {
			response = StudyClient.addStudyStatus(studyIdentifier, studyStatus);  

		} catch (OperationException e) {
			logger.error("addStudyStatus", e);
			throw e;
		} catch (Throwable t) {
			logger.error("addStudyStatus", t);
			response.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return response;
	}

	public ResponseHolder addStudyTeamMember(
			StudyIdentifier studyIdentifier,
			StudyTeamMember studyTeamMember,
			boolean createNonSystemUsers)
	throws OperationException {
		ResponseHolder response = new ResponseHolder();
		try {
			response = 
				StudyClient.addStudyTeamMember(
						studyIdentifier, 
						studyTeamMember, 
						createNonSystemUsers);  
		} catch (OperationException e) {
			logger.error("addStudyTeamMember", e);
			throw e;
		} catch (Throwable t) {
			logger.error("addStudyTeamMember", t);
			response.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return response;
	}

	public ResponseHolder removeStudyOrganization(
			StudyIdentifier studyIdentifier,
			OrganizationIdentifier organizationIdentifier)
	throws OperationException {
		ResponseHolder response = new ResponseHolder();
		try {
			response = 
				StudyClient.removeStudyOrganization(
						studyIdentifier, 
						organizationIdentifier);  

		} catch (OperationException e) {
			logger.error("removeStudyOrganization", e);
			throw e;
		} catch (Throwable t) {
			logger.error("removeStudyOrganization", t);
			response.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return response;
	}

	public ResponseHolder removeStudyTeamMember(
			StudyIdentifier studyIdentifier,
			UserIdentifier userIdentifier) 
	throws OperationException {
		ResponseHolder response = new ResponseHolder();
		try {
			response = 
				StudyClient.removeStudyTeamMember(
						studyIdentifier, 
						userIdentifier);  

		} catch (OperationException e) {
			logger.error("removeStudyTeamMember", e);
			throw e;
		} catch (Throwable t) {
			logger.error("removeStudyTeamMember", t);
			response.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return response;
	}




	public ResponseHolder updateStudyTeamMember(
			StudyIdentifier studyIdentifier,
			StudyTeamMember studyTeamMember) 
	throws OperationException {
		ResponseHolder response = new ResponseHolder();
		try {
			response = 
				StudyClient.updateStudyTeamMember(
						studyIdentifier, 
						studyTeamMember);  

		} catch (OperationException e) {
			logger.error("updateStudyTeamMember", e);
			throw e;
		} catch (Throwable t) {
			logger.error("updateStudyTeamMember", t);
			response.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return response;
	}
 
	/* (non-Javadoc)
	 * @see com.velos.services.study.StudyService#updateStudyOrganization(com.velos.services.model.StudyIdentifier, com.velos.services.model.StudyOrganization)
	 */
	public ResponseHolder updateStudyOrganization(
			StudyIdentifier studyIdentifier,
			StudyOrganization studyOrganization)
	throws OperationException {
		ResponseHolder response = new ResponseHolder();
		try {
			response = 
				StudyClient.updateStudyOrganization(
						studyIdentifier, 
						studyOrganization);  

		} catch (OperationException e) {
			logger.error("updateStudyOrganization", e);
			throw e;
		} catch (Throwable t) {
			logger.error("updateStudyOrganization", t);
			response.addIssue(new Issue(IssueTypes.UNKNOWN_THROWABLE));
		}
		return response;
	}


	public StudyStatuses getStudyStatuses(StudyIdentifier studyIdentifier)
			throws OperationException {
	
		try {
			return StudyClient.getStudyStatuses(studyIdentifier);
		}
		catch (OperationException e) {
			logger.error("getStudyStatuses", e);
			throw e;
		} catch (Throwable t) {
			logger.error("getStudyStatuses", t);

		}
		return null; 
	}
	/**
	 * 
	 */
	public StudyStatus getStudyStatus(StudyStatusIdentifier studyStatusIdentifier)
	throws OperationException {
		StudyStatus studyStatus = StudyClient.getStudyStatus(studyStatusIdentifier);
		return studyStatus;
}

}
