package com.velos.webservices;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import org.apache.log4j.Logger;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.PatientDemographicsClient;
import com.velos.services.model.PatientDemographics;
import com.velos.services.model.PatientIdentifier;

/**
 * WebServices class dealing with all PatientDemographics Services operations.
 * @author virendra
 *
 */
@WebService(
		serviceName="PatientDemographicsService",
		endpointInterface="com.velos.webservices.PatientDemographicsSEI",
		targetNamespace="http://velos.com/services/"
		)
public class PatientDemographicsWS implements PatientDemographicsSEI{
	
	private static Logger logger = Logger.getLogger(PatientDemographicsWS.class.getName());
	ResponseHolder response = new ResponseHolder();
	
	@Resource
	private WebServiceContext context;  
	
	public PatientDemographicsWS(){
		
	}
	/***getPatientDemographics method in WebServicesClass
	 * calls Client class with PatientIdentifier object and
	 * returns PatientDemographics object
	 * @return
	 */
	public PatientDemographics getPatientDemographics(PatientIdentifier patientId)
			throws OperationException {
		PatientDemographics patientDemographics = PatientDemographicsClient.getPatientDemographics(patientId);
		return patientDemographics;
	}
	}