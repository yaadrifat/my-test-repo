/**
 * 
 */
package com.velos.webservices;

import java.util.List;

import javax.jws.WebService;
import javax.jws.WebResult;
import javax.jws.WebParam;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;

import com.velos.services.ResponseHolder;
import com.velos.services.model.OrganizationIdentifier;
import com.velos.services.model.Study;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyOrganization;
import com.velos.services.model.StudyStatus;
import com.velos.services.model.StudyStatusIdentifier;
import com.velos.services.model.StudyStatuses;
import com.velos.services.model.StudySummary;
import com.velos.services.model.StudyTeamMember;
import com.velos.services.model.UserIdentifier;

/**
 * @author dylan
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")
public interface StudySEI {

	@WebResult(name = "Response")
	public abstract ResponseHolder createStudy(
			@WebParam(name = "Study") 
			Study study, 
			
			@WebParam(name = "createNonSystemUsers")
			boolean createNonSystemUsers)
			throws OperationException, OperationRolledBackException;

	
	@WebResult(name = "Study")
	public abstract Study getStudy(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id) 
		throws OperationException;
	
	//DRM - 5431 - fixed typo in studysummary result name
	@WebResult(name = "StudySummary")
	public abstract StudySummary getStudySummary(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id) 
		throws OperationException;
	
	@WebResult(name = "Response")
	public abstract ResponseHolder updateStudySummary(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id,
			
			@WebParam(name = "StudySummary")
			StudySummary studySummary,

			@WebParam(name = "createNonSystemUsers")
			boolean createNonSystemUsers) 
		throws OperationException, OperationRolledBackException;
	
	
	@WebResult(name = "Response")
	public abstract ResponseHolder addStudyTeamMember(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id,
			
			@WebParam(name = "StudyTeamMember")
			StudyTeamMember studyTeamMember,

			@WebParam(name = "createNonSystemUsers")
			boolean createNonSystemUsers) 
		throws OperationException, OperationRolledBackException; 
	
	@WebResult(name = "Response")
	public abstract ResponseHolder updateStudyTeamMember(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id,
			
			@WebParam(name = "StudyTeamMember")
			StudyTeamMember studyTeamMember) 
		throws OperationException, OperationRolledBackException;
	
	@WebResult(name = "Response")
	public abstract ResponseHolder removeStudyTeamMember(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id,
	 
			@WebParam(name = "StudyTeamUserIdentifier")
			UserIdentifier userIdentifier)

		throws OperationException, OperationRolledBackException;
	
	@WebResult(name = "Response")
	public abstract ResponseHolder addStudyOrganization(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id,
			
			@WebParam(name = "StudyOrganization")
			StudyOrganization studyOrganization) 
		throws OperationException, OperationRolledBackException;
	   
	@WebResult(name = "Response") 
	public abstract ResponseHolder updateStudyOrganization(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id,
			
			@WebParam(name = "StudyOrganization")
			StudyOrganization studyOrganization) 
		throws OperationException, OperationRolledBackException;
	
	@WebResult(name = "Response")
	public abstract ResponseHolder removeStudyOrganization(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id,
			
			@WebParam(name = "OrganizationIdentifier")
			OrganizationIdentifier organizationIdentifier) 
		throws OperationException, OperationRolledBackException;
	
	
	@WebResult(name = "Response")
	public abstract ResponseHolder addStudyStatus(
			@WebParam(name = "StudyIdentifier")
			StudyIdentifier id,
			
			@WebParam(name = "StudyStatus")
			StudyStatus studyStatus)
		throws OperationException, OperationRolledBackException;
//	
//	@WebResult(name = "Response")
//	public abstract ResponseHolder updateStatus( 
//			@WebParam(name = "StudyIdentifier")
//			StudyIdentifier id,
//			
//			@WebParam(name = "StudyStatus")
//			StudyStatus studyStatus) 
//		throws OperationException, OperationRolledBackException;
	
	@WebResult(name = "StudyStatuses")
	public StudyStatuses getStudyStatuses(
			@WebParam(name = "StudyIdentifier")  
			StudyIdentifier studyIdentifier) 
	
	throws OperationException;
	
//	
//	@WebResult(name = "Response")
//	public abstract ResponseHolder updateStudyOrganizations(
//			@WebParam(name = "StudyIdentifier")
//			StudyIdentifier id,
//			
//			@WebParam(name = "StudyOrganizations")
//			List<StudyOrganization> studyOrganizations) 
//		throws OperationException;
//	
//	@WebResult(name = "Response")
//	public abstract ResponseHolder updateStudyTeamMembers(
//			@WebParam(name = "StudyIdentifier")
//			StudyIdentifier id,
//			
//			@WebParam(name = "StudyTeamMembers")
//			List<StudyTeamMember> studyTeamMembers,
//
//			@WebParam(name = "createNonSystemUsers")
//			boolean createNonSystemUsers)  
//		throws OperationException;
	
	/**
	 * 
	 * @param studyStatusIdentifier
	 * @return StudyStatus
	 * @throws OperationException
	 */
	@WebResult(name = "StudyStatus" )
	public StudyStatus getStudyStatus(
	@WebParam(name = "StudyStatusIdentifier")		
	StudyStatusIdentifier studyStatusIdentifier)
			throws OperationException;

}