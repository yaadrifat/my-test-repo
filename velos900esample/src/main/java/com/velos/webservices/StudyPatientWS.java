package com.velos.webservices;

import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import org.apache.log4j.Logger;

import com.velos.services.OperationException;
import com.velos.services.ResponseHolder;
import com.velos.services.client.StudyPatientClient;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudyPatient;
/**
 * Webservice class for StudyPatient.
 * @author Virendra
 *
 */
@WebService(
		serviceName="StudyPatientService",
		endpointInterface="com.velos.webservices.StudyPatientSEI",
		targetNamespace="http://velos.com/services/")
		
public class StudyPatientWS implements StudyPatientSEI{
	
	private static Logger logger = Logger.getLogger(StudyPatientWS.class.getName());
	ResponseHolder response = new ResponseHolder();
	
	@Resource
	private WebServiceContext context;  
	
	public StudyPatientWS(){
		
	}
	/***
	 * Calls getStudyPatient methiod of StudyPatient client with StudyIdentifier
	 * and returns List of SudyPatient object.
	 */
	public List<StudyPatient> getStudyPatients(StudyIdentifier StudyId)
			throws OperationException {
		List<StudyPatient> studyPatients = StudyPatientClient.getStudyPatients(StudyId);
		return studyPatients;
	}

}