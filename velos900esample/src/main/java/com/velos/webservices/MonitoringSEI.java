package com.velos.webservices;

import java.io.IOException;
import java.util.Date;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.MessageTraffic;
import com.velos.services.model.MonitorVersion;
/**
 * Service Endpoint Interface dealing with generation of Operations
 *  of monitoring services
 * @author Virendra
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")

	public interface MonitoringSEI {
		/**
		 * method declaration for runtime status of eSP
		 * @return ResponseHolder Object with response if Database is Up and Running
		 * and eSP Runtime is up and running and report issues if any problem.  
		 * @throws OperationException
		 * @throws OperationRolledBackException
		 */
		@WebResult(name = "Response" )
		public abstract ResponseHolder getHeartBeat()
				throws OperationException, OperationRolledBackException;    
		/**
		 * method declaration for version monitoring of eSP 
		 * @return MonitorVersion Object with eResearchCompatibilityVersionNumber,
		 * eSPBuildDate, eSPBuildNumber, eSPVersionNumber.
		 * @throws OperationException
		 * @throws OperationRolledBackException
		 * @throws IOException
		 */
		@WebResult(name = "MonitorVersion")  
		public abstract MonitorVersion getMonitorVersion()
			throws OperationException, OperationRolledBackException, IOException;
		/**
		 * method declaration for Message Traffic dealt by eSP
		 * @param fromDate
		 * @param toDate
		 * @return MessageTraffic object with count of all messages, count of success messages,
		 *  and count of failed messages in a date range.
		 * @throws OperationException
		 * @throws OperationRolledBackException
		 */
		@WebResult(name = "MessageTraffic")
		public abstract MessageTraffic getMessageTraffic(
			@WebParam(name = "fromDate") 
			Date fromDate,
			@WebParam(name = "toDate" ) 
			Date toDate
			)
			throws OperationException, OperationRolledBackException;
	
		
	
}