/**
 * 
 */
package com.velos.webservices;

/**
 * @author dylan
 *
 */
public class Constants {
	public static final String SERVICES_NAMESPACE = "http://velos.com/services";
	public static final String MODEL_NAMESPACE = "http://velos.com/services/model";
	public static final String ERROR_STRING_KEY = "com.velos.webservice.errString";
	public static final String USER_BEAN_KEY = "com.velos.services.userbeankey";
}
