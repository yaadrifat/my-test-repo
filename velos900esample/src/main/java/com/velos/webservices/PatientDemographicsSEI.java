package com.velos.webservices;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.velos.services.OperationException;
import com.velos.services.OperationRolledBackException;
import com.velos.services.ResponseHolder;
import com.velos.services.model.PatientDemographics;
import com.velos.services.model.PatientIdentifier;
import com.velos.services.model.StudyIdentifier;
import com.velos.services.model.StudySummary;
/**
 * Service Endpoint Interface dealing with generation of Operations
 *  of Patient demographics services
 * @author Virendra
 *
 */
@WebService(
		 targetNamespace="http://velos.com/services/")
	public interface PatientDemographicsSEI {
		/***
		 * public method takes PatientIdentifier as input
		 * and returns PatientDemographics object for the patient 
		 * @param patientId
		 * @return
		 * @throws OperationException
		 */
		@WebResult(name = "PatientDemographics" )
		public abstract PatientDemographics getPatientDemographics(
		@WebParam(name = "PatientIdentifier")		
		PatientIdentifier patientId)
				throws OperationException; 
		
}