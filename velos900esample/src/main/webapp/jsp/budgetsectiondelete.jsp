<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Bgt_SecDel%><%--Budget Section Delete*****--%></title>

<!-- YK 05Aug11: Removed for Bug #6728 already using Whichcss_skin.js in panel.jsp -->
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.aithent.audittrail.reports.AuditUtils"%>
<%@ page import = "com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<jsp:useBean id="bgtSectionB" scope="request" class="com.velos.esch.web.bgtSection.BgtSectionJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<% String src;
	src= request.getParameter("srcmenu");
	
	String budgetTemplate = request.getParameter("budgetTemplate");			

	String includedIn = request.getParameter("includedIn");
	
	if(StringUtil.isEmpty(includedIn))
	{
	 includedIn = "";
	}

	String fromRt = request.getParameter("fromRt");
	if (fromRt==null || fromRt.equals("null")) fromRt="";
%>

<% if ("S".equals(budgetTemplate)) { %>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<% }  else { %>

	<jsp:include page="include.jsp" flush="true"/> 

<% } %>	
	
<BODY> 
<br>

<DIV class="popDefault" id="div1">
<% 
String budgetId= "";
String bgtSectionId= "";
String calId = "";
String selectedTab = request.getParameter("selectedTab");
String mode = request.getParameter("mode");
		
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{ 	
		bgtSectionId= request.getParameter("bgtSectionId");
		budgetId= request.getParameter("budgetId");
		calId= request.getParameter("calId");
		int ret=0;
		
		String delMode=request.getParameter("delMode");
	
		if (delMode.equals("null")) {
			delMode="final";
%>
	<FORM name="budgetSectionDelete" id="bdgtsecdel" method="post" action="budgetsectiondelete.jsp" onSubmit="if (validate(document.budgetSectionDelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>

<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
		
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="bdgtsecdel"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>


 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="bgtSectionId" value="<%=bgtSectionId%>">
  	 <input type="hidden" name="budgetId" value="<%=budgetId%>">
  	 <input type="hidden" name="calId" value="<%=calId%>">
  	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
	 <input type="hidden" name="budgetTemplate" value="<%=budgetTemplate%>">
	 <input type="hidden" name="mode" value="<%=mode%>">	 	 	 	 

     <input type="hidden" name="includedIn" value="<%=includedIn%>">	 	 	 	 
	 <input type="hidden" name="fromRt" value="<%=fromRt%>">
	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {

			//delete the section logically
			bgtSectionB.setBgtSectionId(EJBUtil.stringToNum(bgtSectionId));
			bgtSectionB.getBgtSectionDetails();
			bgtSectionB.setBgtSectionDelFlag("Y");
			//Modified for INF-18183 ::: Ankit	
			ret = bgtSectionB.updateBgtSection(AuditUtils.createArgs(session,"",LC.L_Budget));
  
			if (ret==-2) {%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_SecNotRem_FromBgt%><%--Section not removed from Budget.*****--%> </p>			
			<%}else {
				 			tSession.setAttribute("lineItemDeleted", "Y");  
				%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_SecRem_FromBgt%><%--Section removed from Budget.*****--%> </p>
                <% if ("S".equals(budgetTemplate)) {  %>
                  <META HTTP-EQUIV=Refresh CONTENT="1; URL=budgetsections.jsp?includedIn=<%=includedIn%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&budgetId=<%=budgetId%>&calId=<%=calId%>&budgetTemplate=<%=budgetTemplate%>&mode=<%=mode%>&fromRt=<%=fromRt%>">
                <% } else { %>
                  <script>
                  	window.opener.location.reload();
                 	window.location.href = "budgetsections.jsp?includedIn=<%=includedIn%>&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&budgetId=<%=budgetId%>&calId=<%=calId%>&budgetTemplate=<%=budgetTemplate%>&mode=<%=mode%>&fromRt=<%=fromRt%>";
                  </script>
                <% }  %>
			<%}
			} //end esign
	} //end of delMode	
  }//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<% if (! (includedIn.equals("P") || 
        (includedIn.equals("") && !"S".equals(budgetTemplate)))) { %>

	<div class ="mainMenu" id = "emenu">
	<jsp:include page="getmenu.jsp" flush="true"/> 
	</div>
<% } %>

</body>
</HTML>


