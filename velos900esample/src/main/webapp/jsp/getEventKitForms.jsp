<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="storageB" scope="request" class="com.velos.eres.web.storage.StorageJB"/>
<jsp:useBean id="eventkitB" scope="request" class="com.velos.esch.web.eventKit.EventKitJB"/>

<jsp:useBean id="CRFB" scope="request" class="com.velos.esch.web.eventCrf.EventCrfJB"/>

<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.business.common.*"%>
<head>
</head>


<%
 HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession)) {
	
    String name = "";
    String id = "";
    Integer childStoragePk = null;
    int eventId = 0;
    ArrayList kitFkStorages = new ArrayList();
    
    ArrayList arFormIds = new ArrayList();
    ArrayList arFormNames = new ArrayList();
    ArrayList arFormDispTypes = new ArrayList();
    ArrayList arFormDispInSpecs = new ArrayList();
    ArrayList arFormEntryChars  = new ArrayList();
    
    String fk_sch_events1 = ""; 
	
	eventId = EJBUtil.stringToNum(request.getParameter("eventId"));
	
	fk_sch_events1 = request.getParameter("fk_sch_events1");
	
	
	int accId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));
	
	
	String dispType = "all";
	int kitCount = 0;
	int storagePk = 0;
	int storageCount = 0;
	
	String formName = "";
	Integer formPK = null;
	String formType = "";
	String formDispInSpec = "";
	String crfHrf = "";
	String entryChar = "";
	
	//get CRFS for the event
	CrfDao  crfdao = new CrfDao();
	
	crfdao = CRFB.getEventCrfForms(eventId,dispType);
	
	arFormNames = crfdao.getFormName();
    arFormIds = crfdao.getFormId();
    arFormDispTypes = crfdao.getCrfFormType();
    arFormDispInSpecs = crfdao.getDisplayInSpecs();
    arFormEntryChars = crfdao.getCrfformEntryChars();
	
	//get Storage KIts for this event
	EventKitDao evk = new EventKitDao();
	evk = eventkitB.getEventStorageKits(eventId);
	kitFkStorages = evk.getStorageKits();
	
	kitCount = kitFkStorages.size();
	
	%>
		<table>
		
	<%
	if ( kitCount > 0 )
	{
		//iterate through kits and display forms for each kit component
		for (int k = 0; k<kitCount; k++ )
		{
			storagePk = EJBUtil.stringToNum( (String) kitFkStorages.get(k));
			
			StorageDao sdao = new StorageDao();
			sdao = storageB.getImmediateChildren(storagePk);
			
			ArrayList pkStorages = new ArrayList();
		    ArrayList storageIds = new ArrayList();
		    ArrayList storageNames = new ArrayList();
		    
		     
		    
		    pkStorages = sdao.getPkStorages();
		    storageIds =  sdao.getStorageIds();
		    storageNames = sdao.getStorageNames();
		    
		    //iterate through all kit components
		    storageCount = pkStorages.size();
		    
		     if (  storageCount > 0 )
			{	
				for (int j = 0; j<storageCount; j++)
				{
					childStoragePk = (Integer) pkStorages.get(j); 
					name = (String) storageNames.get(j);
					id = (String) storageIds.get(j);
					%>
						<tr><td><%=name%></td><td>
					<%
						//show form for each storage component
						 for ( int f=0;f<arFormIds.size();f++)
						 {
							formPK = (Integer) 	arFormIds.get(f); 
						 	formName = (String) arFormNames.get(f);
						 	formType = (String) arFormDispTypes.get(f);
						 	formDispInSpec = (String) arFormDispInSpecs.get(f);
						 	entryChar = (String) arFormEntryChars.get(f);
						 	
						 	if (StringUtil.isEmpty(formDispInSpec))
						 	{
						 		formDispInSpec = "0";
						 	}
						 	if (formDispInSpec.equals("1"))
						 	{
						 		//BK,FIX#5981-MAR-28-2010
						 	
						 			//crfHrf = "<A onClick=\" fkStorageForSpecimen ="+childStoragePk+"; calledFromSchedule = true; fk_sch_events1 = " +fk_sch_events1 + "; return openlinkedForm("+formPK.intValue()+",'"+entryChar+"','"+formType+"',document.sch)\" href=\"Javascript:void(0)\">"+formName+"</A> ";
						 		%>
						 			<BR><%=crfHrf %> 
						 		<%
						 	}
    					 }
					%>
						</td>
						</tr> <!-- for storage name row -->
					<%	
						
				} //storagecount
				
					
			} //for storage counts
		    
		    
		 }  //for number of storages
	} 
	//there are storage Kits linked with the event   
	    %>
</table>
	    <%
	 
}//session
%>