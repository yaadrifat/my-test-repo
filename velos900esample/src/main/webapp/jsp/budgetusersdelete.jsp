<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_BgtUse_Del %><%-- Budget Users >> Delete*****--%></title>

<!-- YK 05Aug11: Removed for Bug #6728 already using Whichcss_skin.js in panel.jsp -->
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.aithent.audittrail.reports.AuditUtils"%>
<%@page import="com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*"%>
<jsp:useBean id ="budgetUsersB" scope="request" class="com.velos.esch.web.budgetUsers.BudgetUsersJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<%  String src;
    src= request.getParameter("srcmenu");
    String budgetTemplate=StringUtil.htmlEncodeXss(request.getParameter("budgetTemplate"));
    String bottomdivClass="tabDefBotN";
    if ("S".equals(request.getParameter("budgetTemplate"))) { %>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
    <% } else { 
        bottomdivClass="popDefault";
    %>
<jsp:include page="include.jsp" flush="true"/> 
 <% }  %>

<BODY> 
<br>

<DIV class="<%=bottomdivClass %>" id="div1">
<% 
String budgetId= "";
String bgtUserId= "";
String selectedTab="";
String budgetType="";
String mode="";
	
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{ 	
		budgetId= request.getParameter("budgetId");
		bgtUserId= request.getParameter("bgtUsrId");
		selectedTab = request.getParameter("selectedTab");
		budgetType = request.getParameter("budgetType");
		mode = request.getParameter("mode");
		int ret=0;
		
		String delMode=request.getParameter("delMode");
		if (delMode==null) {
			delMode="final";
%>
	
	<FORM name="budgetUsersDelete" id="bdgtusersdelfrm" method="post" action="budgetusersdelete.jsp" onSubmit="if (validate(document.budgetUsersDelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>

	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%-- Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
		
	<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="bdgtusersdelfrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="budgetId" value="<%=budgetId%>">
  	 <input type="hidden" name="bgtUsrId" value="<%=bgtUserId%>">
  	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
   	 <input type="hidden" name="budgetType" value="<%=budgetType%>">
   	 <input type="hidden" name="mode" value="<%=mode%>">	 	 	 
   	 <input type="hidden" name="budgetTemplate" value="<%=budgetTemplate%>">	 	 	 
		 
	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
			// Modified for INF-18183 ::: AGodara 
			ret = budgetUsersB.removeBudgetUser(StringUtil.stringToNum(bgtUserId),AuditUtils.createArgs(tSession,"",LC.L_Budget));  
			if (ret==-2) {%>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_UsrNotRem_FromBgt%><%-- User not removed from Budget.*****--%> </p>			
			<%}else { %>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC. M_User_RemFromBgt%><%-- User removed from Budget.*****--%> </p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=budgetrights.jsp?srcmenu=<%=src%>&budgetId=<%=budgetId%>&selectedTab=<%=selectedTab%>&budgetType=<%=budgetType%>&mode=<%=mode%>&budgetTemplate=<%=budgetTemplate%>">				
			<%}
			} //end esign
	} //end of delMode	
  }//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/> 
</div>

</body>
</HTML>


