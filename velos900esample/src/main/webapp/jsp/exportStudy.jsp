<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title>Export</title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

</head>

<SCRIPT Language="javascript">
 function  validate(formobj){
      if (!(validate_col('Esign',formobj.eSign))) return false

	 if(isNaN(formobj.eSign.value) == true) {
		alert("Incorrect e-Signature. Please enter again");
		formobj.eSign.focus();
		return false;

	}
   }


</SCRIPT>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="studyVerB" scope="request" class="com.velos.eres.web.studyVer.StudyVerJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="evassocDao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.impex.*"%>





<% String src;
	src= request.getParameter("srcmenu");
	src = "";
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<body>

<br>

<DIV class="formDefault" id="div1">

  <%

	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))

	{

	    String usr = null;
		usr = (String) tSession.getValue("userId");
		String uName = (String) tSession.getValue("userName");
		String accId = (String) tSession.getValue("accountId");	

	  	String studyId = request.getParameter("studyId");
	  
	    ArrayList arModuleSubtype = new ArrayList();
	    ArrayList arModuleName = new ArrayList();
		
		if (EJBUtil.isEmpty(studyId))
		{
			 studyId = "0"; 
		 }
			 

	%>
	 <P class = "userName"><%= uName %></p>
	 
	 <form name="exportdata" method="POST" action="startExport.jsp" onsubmit="return validate(document.exportdata);">  

	 <input type="hidden" name = "accountId"  value = <%=accId%>>
 	 <input type="hidden" name = "studyId"  value = <%=studyId%>>
  	 <input type="hidden" name = "userId"  value = <%=usr%>>
	 
	<P class="sectionHeadings"> Manage Protocols >> Export </P>	 
		
 	<p class="defComments"> Select Modules: </p>
	<table border = 0>
	<%	
		
		Hashtable htModules = Module.getModuleListing("study","E"); //get 'Export' modules for 'Study'
		
		//get participating sites
		
		ArrayList arSites = new ArrayList ();
		 
		arSites = A2ASite.getA2ASites();
 
		
		int count = 0;
		int modCount = 0;
		String moduleSubtype = "";
		String moduleName  = "";
		
		
		count = htModules.size();
				
		if (count == 2 )
		{
		
			arModuleSubtype = (ArrayList) htModules.get("moduleSubtype");
			arModuleName = (ArrayList) htModules.get("moduleName");
			
			modCount = arModuleSubtype.size();
			
			System.out.println("modCount" + modCount);
			
			for (int i=0; i<modCount; i++)
			{
				moduleSubtype = (String) arModuleSubtype.get(i);
				moduleName = (String) arModuleName.get(i); 
				%>
				<tr>
					<td>
						<input type="checkbox" name="module" value = <%= moduleSubtype %> CHECKED><%= moduleName%>
					</td>
					
					<%
					
						if (moduleSubtype.equalsIgnoreCase("study_ver"))
						{
    						//get study versions
    						StudyVerDao studyVerDao = studyVerB.getAllVers(EJBUtil.stringToNum(studyId),"F");
    						ArrayList studyVerIds = studyVerDao.getStudyVerIds();
    				   		ArrayList studyVerNumbers = studyVerDao.getStudyVerNumbers();
    						int len = studyVerIds.size();
    
    						String verNumber ;
    						String studyVerId;
							
							for(int ctr = 0;ctr<len;ctr++)
							{
								verNumber=((studyVerNumbers.get(ctr))==null)?"-":(studyVerNumbers.get(ctr)).toString();
								studyVerId = ((Integer) studyVerIds.get(ctr)).toString();
						%>
								 <tr><td>
								 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name = <%=moduleSubtype + "_pk"%>  value = <%=studyVerId%> CHECKED><%=verNumber%> <br>
								 </td>
								 </tr>
						<%	
							}
						%>
							<input type="hidden" name = <%=moduleSubtype + "_totalcount"%>  value = <%=len%>>
												
						<%		 
							
						}
						else if (moduleSubtype.equalsIgnoreCase("study_cal"))
						{
            				//get study protocols
            				
            				evassocDao = eventassocB.getStudyProts(EJBUtil.stringToNum(studyId),"A");
            	
            				ArrayList eventIds=evassocDao.getEvent_ids() ; 
            				ArrayList names= evassocDao.getNames(); 
            		   	  	int calLen= eventIds.size();
            				Integer calid;
	           				String calname="";
						
						    for(int calctr = 0;calctr<calLen;calctr++)
							{	
								calid = (Integer)eventIds.get(calctr);
								calname=((names.get(calctr)) == null)?"-":(names.get(calctr)).toString();
			
							%>
						 <tr><td>
							 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name = <%=moduleSubtype + "_pk"%>   value = <%=calid%> CHECKED><%=calname%> 
						 </td>
						 </tr>
								
					 	<%	
						}
						%>
						<input type="hidden" name = <%=moduleSubtype + "_totalcount"%>  value = <%=calLen%>>
					<%	 
					}
					
					%>
					
					
					
				</tr>
				<%
				
			}
		
		
		} 
		
			
	
	%>

    </table>
	<HR/>
	<p class="defComments"> Select Participating Sites: </p>
	
	<table border = 0>
	
	<%
		int siteCount = 0;
		
		siteCount = arSites.size();
		String siteName;
		int siteId = 0;
		
		for (int stCounter = 0; stCounter < siteCount ; stCounter++)
		{
			A2ASite site = new A2ASite();
			site = 	(A2ASite) arSites.get(stCounter);
			
			siteId = site.getId();
			siteName = site.getName();
		%>
			<tr>
				<td>
					<input type="checkbox" name="partSite" value = <%= siteId %> CHECKED><%= siteName%>
				</td>
			</tr>	
					
		<%	
			   	 
		
		 }   
	
	%>
	</table>
	
	<table width = 70% border = 0><tr><td align = "right">
      <tr>
	  <td><br>
   	        e-Signature <FONT class="Mandatory">* </FONT>
	</td>
	<td><br>
       	<input type="password" name="eSign" maxlength="8">
	</td>
	<td><br>
		<button type="submit"><%=LC.L_Submit%></button>
	</td></tr>
	</table>
	
	</form>
   
	<%
		//get export module information


} else {  //else of if body for session



%>

  <jsp:include page="timeout.html" flush="true"/>

  <%

}


%>


  <div> 

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>



</div>

<div class ="mainMenu" id = "emenu">
	<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>





