<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML> 

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>

<script>

function openFormTypeWin(pageRight) 
{
		if (f_check_perm(pageRight,'N') == true) { 
		param1="fieldCategory.jsp?type=T&mode=N";
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=700,height=350,top=250 ,left=200");
		windowName.focus();
		}
}

function openEditFormTypeWin(formobj,catLibId,pageRight)
{ 
        param1="fieldCategory.jsp?catLibId="+catLibId+"&mode=M&type=T";
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=300");
		windowName.focus();
}

////////////// FOR PAGINATION
function setOrder(formObj,orderBy,pgRight) //orderBy column number
{
	var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";		
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";	
	}

	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	setSearchFilters(document.formlib);
	formObj.action="formLibraryBrowser.jsp?mode=initial&srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType;
	formObj.submit(); 

	}

	//for setting search
	//Added by Ganapathy on 11th April 2005
	 function setSearchFilters(form)
	 {
  		  form.nameSearch.value=form.txtName.value;
	 	  form.typeSearch.value=form.ByFormType.value;
	 }

function confirmBox(formName,pageRight) 
{
	if (f_check_perm(pageRight,'E') == true) 
		{
			var paramArray = [formName];
			msg=getLocalizedMessageString("L_Del_FrmLib",paramArray);/*msg="Delete " + formName + " from Library?";*****/
 	
			if (confirm(msg)) 
			{
    			return true;
			}
			else
			{
				return false;
			}
		} 
		else 
		{
			return false;
		}
}

function openFormPreview(formId)
{
	windowName=window.open("formpreview.jsp?formId="+formId,"Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=825,height=600,top=90,left=150");
	windowName.focus();
}

</script>

<body>

<title><%=LC.L_Form_Lib%><%--Form Library*****--%></title>





<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<% String src;

src= request.getParameter("srcmenu");

%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<!-- km -->
<jsp:useBean id ="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="userSiteB" scope="request" class="com.velos.eres.web.userSite.UserSiteJB"/>



<onload="setfocus()">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>



<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>



<%
 HttpSession tSession = request.getSession(true); 
 
  if (sessionmaint.isValidSession(tSession))
	{
%>
<DIV class="tabDefTopN" id="div1">  
<jsp:include page="librarytabs.jsp" flush="true"></jsp:include>
</DIV>
	<DIV class="tabDefBotN" id="div3"> 

<%
		 String selectedTab = request.getParameter("selectedTab");
		 String modRight = (String) tSession.getValue("modRight");
		 int pageRight = 0;
		 GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
		 pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
		 String formName = request.getParameter("txtName");
		 if (formName == null ) 
		{
			formName="";
		}
		
		 int patProfileSeq = 0, formLibSeq = 0;

		 // To check for the account level rights
		 acmod.getControlValues("module");
		 ArrayList acmodfeature =  acmod.getCValue();
		 ArrayList acmodftrSeq = acmod.getCSeq();
		 char formLibAppRight = '0';
		 char patProfileAppRight = '0';
		 patProfileSeq = acmodfeature.indexOf("MODPATPROF");
		 formLibSeq = acmodfeature.indexOf("MODFORMS");
		 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
		 patProfileSeq = ((Integer) acmodftrSeq.get(patProfileSeq)).intValue();
		 formLibAppRight = modRight.charAt(formLibSeq - 1);
		 patProfileAppRight = modRight.charAt(patProfileSeq - 1);
	if (((String.valueOf(formLibAppRight).compareTo("1") == 0) || (String.valueOf(patProfileAppRight).compareTo("1") == 0)) &&(pageRight >=4))
		{
		 
		 
		 String pullDown;
		 String formType="";
		 String name="";
		 String desc="";
		 String status="";
		 String sharedWith="";
		 String formId="";
		 String catLibId="";
		 String txtName=""  ;    //request.getParameter("txtName");
		 String fName = request.getParameter("txtName");
		 int fkCati = 0 ;
		 int forLastAll = 0 ;
		 
		 if ( fName != null )
		 txtName = fName ;
		 
		 String txtFormType=""  ; //request.getParameter("ByFormType");
		 String fkCat = request.getParameter("ByFormType");
		
		  if ( fkCat != null )
		 {  
		 	 txtFormType = fkCat ;
			 fkCati = EJBUtil.stringToNum(txtFormType);
		}
		 
		 int iFormType= EJBUtil.stringToNum(txtFormType);
		 String accId=(String)tSession.getAttribute("accountId");
		 String usr = (String) tSession.getValue("userId");
		 
		 int iusr= EJBUtil.stringToNum(usr);
		 int iaccId=EJBUtil.stringToNum(accId);
		
		 String catLibType="T" ;
		 ArrayList ids= new ArrayList();
		 ArrayList names= new ArrayList();
		 CatLibDao catLibDao=new CatLibDao();

		 /*int pageRight = 0;
		 GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
		 pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));*/
		
		String mode=request.getParameter("mode");

				
		///FOR HANDLING PAGINATION
		
		String pagenum = "";
		int curPage = 0;
		long startPage = 1;
		String stPage;
		long cntr = 0;
		
		
		

		pagenum = request.getParameter("page");

		if (pagenum == null)
		{
			pagenum = "1";
		}
		curPage = EJBUtil.stringToNum(pagenum);
		String sortOrderType="";
		String orderBy = "";
		orderBy = request.getParameter("orderBy");		
		String orderType = "";
		orderType = request.getParameter("orderType");
		String str2 ="";
		String str3 ="";
		String str4 ="";
		String str5 ="";
		String str6 ="";
		String str7 ="";
		String str1 ="";
		String count1="";
		String count2="";
		String countSql = "";
		String formSql = "";
	
		if (orderType == null)
		{
			//For sorting 
			orderType = "asc";
		}
		//Added by Ganapathy on 11th April 2005  Form Library Sorting

		if (EJBUtil.isEmpty(orderBy) || (orderBy==null)){
		orderBy="catlib_name";
		
				sortOrderType="lower(catlib_name) asc , lower(form_name) asc " ;
		} else {
	 			if (orderBy.equals("catlib_name")){
					sortOrderType = "lower(catlib_name)"+orderType+", lower(form_name) asc " ;
				}
			else {
					sortOrderType = "lower(catlib_name) asc , lower("+orderBy+") "+orderType ;
				}
		}
		

		//km-to access the forms other than the primary organization
					  
		userB.setUserId(EJBUtil.stringToNum(usr));
		userB.getUserDetails();
		int userPrimOrg = EJBUtil.stringToNum(userB.getUserSiteId());
     				
		ArrayList userSiteIds = new ArrayList();
		ArrayList userSiteSiteIds = new ArrayList();	
     		ArrayList userSiteRights = new ArrayList();
		
		UserSiteDao userSiteDao = userSiteB.getUserSiteTree(EJBUtil.stringToNum(accId),EJBUtil.stringToNum(usr));
		userSiteIds = userSiteDao.getUserSiteIds();
		userSiteSiteIds = userSiteDao.getUserSiteSiteIds();
		userSiteRights = userSiteDao.getUserSiteRights();
		int len1 = userSiteIds.size();
			
			
			String siteIdAcc="";
			int accrights=0;
			int userSiteSiteId=0;		
			int userSiteRight=0;
			for (int counter=0;counter<len1;counter++) {
	 		userSiteSiteId = EJBUtil.stringToNum(((userSiteSiteIds.get(counter)) == null)?"-":(userSiteSiteIds.get(counter)).toString());
     			userSiteRight = EJBUtil.stringToNum(((userSiteRights.get(counter)) == null)?"-":(userSiteRights.get(counter)).toString());
			
			if (userPrimOrg!=userSiteSiteId)
			   accrights=userSiteRight;
			if(accrights!=0)
			siteIdAcc=siteIdAcc+(userSiteSiteId+",");
					
			}
		        siteIdAcc=siteIdAcc+userPrimOrg; 
			
			 // Query modified by Gopu for Nov.2005 Enhancement #4
			 //Query Modified by Manimaran to get forms which have access other than Primaray organization.
								
			str1 =" select distinct pk_formlib , catlib_name,form_name,(select usr_firstname || ' ' || usr_lastname from er_user where pk_user=fl.creator) as creator,to_char(fl.created_on) as created_on , "
					   + " (select usr_firstname || ' ' || usr_lastname from er_user where pk_user=fl.last_modified_by) as last_modified_by,to_char(fl.last_modified_date) as last_modified_date,"
			           + " fk_catlib, case when (nvl(length(form_desc),0)<100) then NVL(form_desc,'-')"
					   + " when (length(form_desc)>=100) then substr(form_desc,1,100)||'...' end	 as  form_desc,form_status,cd.codelst_desc form_statusdesc,fl.form_sharedwith form_sharedwith "
					   + " from  er_formlib fl, er_catlib cl, er_objectshare osh, er_codelst cd "
			           + " where cd.pk_codelst=fl.form_status "
					   + " and fl.fk_account =  " + accId 
			   		   + " and  fl.form_linkto = 'L' "
					   + " and  fl.record_type <> 'D' "
					   + " and fl.pk_formlib = osh.fk_object  "
					   + " and fl.fk_account=cl.fk_account "
					   + " and fl.fk_catlib = cl.pk_catlib  "
					   + " and cl.catlib_type='T' "
				     + " and osh.objectshare_type in('U','O')"	  
				   + " and osh.object_number = 1 "
		   +" and (osh.fk_objectshare_id = " +usr+" or osh.fk_objectshare_id in ("+siteIdAcc+"))";
					 
		
			if (formName.equals(""))
			{
				str2 =" ";
			}
			else 
			{
				 str2=" and lower(fl.form_name) like lower('%"+formName.trim()+"%') ";
			}
			
			
			if(fkCati == 0 || fkCati == -1 )
			{
				str3=" ";
			}
			
			else															   
			{
				str3=" and fl.fk_catlib=" +fkCati ;
			}
	
		//Query modified by Ganapathy on 11th April 2005   Form Library Sorting

/*			if ( fkCati == 0 || fkCati == -1 )
			{
			  	

				str4= "  UNION  (   SELECT  NULL, CATLIB_NAME  ,NULL, "
					+ "  PK_CATLIB 	, NULL , NULL, NULL, NULL  "
					+ "  FROM ER_CATLIB "
					+ "  WHERE FK_ACCOUNT =  "+accId+" "
					+ "  AND CATLIB_TYPE='T' "
					+ "  AND RECORD_TYPE <> 'D' "
					+ "  AND ER_CATLIB.PK_CATLIB NOT IN (SELECT  ER_FORMLIB.FK_CATLIB "
 					+ "  FROM ER_FORMLIB WHERE FK_ACCOUNT = "+accId+" )) "   ;
			
			}
			else
			{
				str4=" ";
			} */
 
// 			str5=" ORDER BY (fk_catlib)";	
		
//			formSql = str1 + str2 + str3 + str4 + str5 ;
					
			formSql = str1 + str2 + str3 + str4 ;
		
			count1 = "select count(*) from  ( " ;
			count2 = ")"  ;
		
			



		//Query modified by Ganapathy on 11th April 2005   Form Library Sorting

//		countSql = count1 + str1 + str2 + str3 + str4 + str5 + count2 ; 
				countSql = count1 + formSql + count2 ; 
		long rowsPerPage=0;
		long totalPages=0;	
		long rowsReturned = 0;
		long showPages = 0;
			
	   boolean hasMore = false;
	   boolean hasPrevious = false;
	   long firstRec = 0;
	   long lastRec = 0;	   
	   long totalRows = 0;	   	   
	   
		//majumdar changed the following line to test for pagination
		rowsPerPage =  Configuration.MOREBROWSERROWS ;
		totalPages =Configuration.PAGEPERBROWSER ; 
  


       BrowserRows br = new BrowserRows();
       br.getPageRows(curPage,rowsPerPage,formSql ,totalPages,countSql,sortOrderType,"");
   	   rowsReturned = br.getRowReturned();
	   showPages = br.getShowPages();
	   startPage = br.getStartPage();
	   hasMore = br.getHasMore();
	   hasPrevious = br.getHasPrevious();	 
	   totalRows = br.getTotalRows();	   
	   firstRec = br.getFirstRec();
	   lastRec = br.getLastRec();
		
		
		
		
		
		
		
		
		
		//////////FOR PAGINATION END

		if(mode==null)
		{
		
		mode="visited";
		
		
		catLibDao= catLibJB.getCategoriesWithAllOption(iaccId,catLibType,forLastAll);
		ids = catLibDao.getCatLibIds();
		names= catLibDao.getCatLibNames();
		pullDown=EJBUtil.createPullDown("ByFormType", -1, ids, names);

				
		}

		else
		{
		 
		 catLibDao= catLibJB.getCategoriesWithAllOption(iaccId,catLibType,forLastAll);
		 ids = catLibDao.getCatLibIds();
		 names= catLibDao.getCatLibNames();
		 pullDown=EJBUtil.createPullDown("ByFormType",fkCati, ids, names);
		
		 
		 }
		 
		
		//}
	//if (((String.valueOf(formLibAppRight).compareTo("1") == 0) || //(String.valueOf(patProfileAppRight).compareTo("1") == 0)) && (pageRight >=4))
//if (pageRight >=4) 
	//{
		
 %>
				<!--Added by Ganapathy on 11th April 2005   Form Library Sorting-->
<Form name="formlib" action="formLibraryBrowser.jsp?srcmenu=<%=src%>&page=1&mode=final"  method="post" onSubmit="setSearchFilters(document.formlib)"> 
	<Input type="hidden" name="mode" value=<%=mode%> >
	<Input type="hidden" name="formId" value=<%=formId%> >
	<input type="hidden" name="selectedTab" value=<%=selectedTab%>>
	<input type="hidden" name="srcmenu" value=<%=src%>>
	<Input type="hidden" name="page" value="<%=curPage%>">
	<Input type="hidden" name="orderType" value="<%=orderType%>">
	<Input type="hidden" name="orderBy" value="<%=orderBy%>">
	<Input type="hidden" name="nameSearch" >
	<Input type="hidden" name="typeSearch" value="<%=fkCat%>">


<div class="tmpHeight"></div>
	<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign" >	
      <tr height="20">
		<td colspan="5"><b><%=LC.L_Search_By%><%--Search By*****--%></b></td>
	</tr>
	<tr>	
 	 <td width="10%"> <%=LC.L_Frm_Name%><%--Form Name*****--%>:  </td>
	 <td width="20%"><input type="text" name="txtName" size = 30 MAXLENGTH = 50 value="<%=formName%>">	   </td>
	 <td width="15%" align="right"><%=LC.L_Form_Type%><%--Form Type****--%>: &nbsp;&nbsp;</td>
	<td width="20%"><%=pullDown%> </td>
	<td width ="35%" align="center"><button type="submit"><%=LC.L_Search%></button></td>
	</tr>				
</table>
<div class="tmpHeight"></div>
	<table width="98%">
	<tr>
	<td width="46%"><P class="defComments"><%=LC.L_Lib_Forms%><%-- Library Forms*****--%></P></td>
	<td width="20%" align="right">
	<A href="#" onClick="openFormTypeWin(<%=pageRight%>)">
		<img src="../images/jpg/AddNewCategory.gif" border="0" title="<%=LC.L_NewFrmCat%>">
	</A>
	</td>

	<td width="18%" align="right"><A href="copyFormFromLib.jsp?srcmenu=tdmenubaritem4&selectedTab=3&calledFrom=L" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_Copy_ExistingFrm_Upper%><%--COPY AN EXISTING FORM*****--%></A></td>


	<td width="16%" align="right"> <A href="formcreation.jsp?srcmenu=<%=src%>&calledFrom=L&mode=N&selectedTab=1&formLibId=<%=formId%>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_CreateNew_Frm_Upper%><%--CREATE A NEW FORM*****--%></A></td>
	</tr>	
	</table>
 
	<table class="outline midAlign" width="99%" cellpadding="0" cellspacing="0"  >
					<tr> 
									<!--Added by Ganapathy on 11th April 2005   Form Library Sorting-->
				        <th width="13%" onclick="setOrder(document.formlib,'catlib_name')"> <%=LC.L_Form_Type%><%--Form Type*****--%> &loz;</th>
				        <th width="25%" onclick="setOrder(document.formlib,'form_name')"> <%=LC.L_Name%><%--Name*****--%> &loz; </th>
				        <th width="22%" onclick="setOrder(document.formlib,'form_desc')"> <%=LC.L_Description%><%--Description*****--%> &loz;</th>
						<th width="10%" onclick="setOrder(document.formlib,'form_statusdesc')"><%=LC.L_Status%><%--Status*****--%> &loz;</th>
						<th width="13%" onclick="setOrder(document.formlib,'form_sharedwith')"><%=LC.L_Shared_with%><%--Shared with--%> &loz;</th>
						<th width="8%"><%=LC.L_Preview%><%--Preview*****--%></th>
<th width="5%"><%=LC.L_Delete%><%--Delete*****--%></th>						
						<th width="5%"><%=LC.L_Info%><%--Info*****--%></th>						
					</tr>
				   <%int count=0;
		for(count=1;count<=rowsReturned;count++)
		{
				
		
						
						
						String tempFormType =  br.getBValues(count,"FK_CATLIB");	
								
												
						if ( count !=1 )
						{ 
						  	//formType=tempFormType;
							 formType  = br.getBValues(count,"CATLIB_NAME");	
						  
						}
						if (   (  count == 1 ) ||    (     ! tempFormType.equals(   br.getBValues(count-1,"FK_CATLIB")    )       ))
						{

						   // formType=tempFormType;
						    formType = br.getBValues(count,"CATLIB_NAME");	
						   
						}
						else
						{
							formType="";	
						}

			

			
			name=  br.getBValues(count,"FORM_NAME");
			name = (  name ==null )?"-":( name) ;	
			
			// Added by Gopu for Nov.2005 Enhancement #4
			String creator="";
			String createdon="";
			String modifiedby="";
			String modifiedon="";
			creator=  br.getBValues(count,"creator");			
			creator = (  creator ==null )?"-":( creator) ;				
			
			
			createdon=  br.getBValues(count,"created_on");
			createdon = (  createdon ==null )?"-":( createdon) ;	
			
			modifiedby=  br.getBValues(count,"last_modified_by");
			modifiedby = (  modifiedby ==null )?"-":( modifiedby) ;	
			
			modifiedon=  br.getBValues(count,"last_modified_date");
			modifiedon = (  modifiedon ==null )?"-":( modifiedon) ;	
			
			/////
			
			
			name=StringUtil.escapeSpecialCharHTML(name);
			
			desc= br.getBValues(count,"FORM_DESC");	
			desc = (  desc==null )?"-":(  desc) ;	

			status= br.getBValues(count,"form_statusdesc");	
			status = (  status==null )?"":(  status) ;	
			sharedWith= br.getBValues(count,"FORM_SHAREDWITH");	
			
			sharedWith = (  sharedWith==null )?"-":(  sharedWith) ;	
			
			formId= br.getBValues(count,"PK_FORMLIB");	
			formId = (  formId==null )?"-":(  formId) ;	
			
			catLibId= br.getBValues(count,"FK_CATLIB");	
			catLibId = (  catLibId==null )?"-":(  catLibId) ;	
			
			
			
			String nameHref=null ;
			if ( name.equals("-"))
			{ 
				nameHref="-";
			}
			else 
			{
				nameHref= " <A href= 'formcreation.jsp?formLibId="+formId+"&mode=M&calledFrom=L&selectedTab=1&srcmenu="+src+"'		 onClick=\"\">"+name+"</A>"	;
			
			}

			if ((count%2)!=0) 
				{%>
			 <tr class="browserEvenRow"> 
				<%}else
				 { %>
				 <tr class="browserOddRow"> 
				<%}%>	
	
				<td>&nbsp;<A href="#" onClick="openEditFormTypeWin(document.formlib,<%=catLibId%>,<%=pageRight%>)"><%=formType%></A>
				</td>
				<%
					%>
				<td><%=nameHref%></td>
				<td><%=desc%></td>	
				<td><%=status%></td>
				  <%if(sharedWith.equals("P"))
					{
					  sharedWith=LC.L_Private;/*sharedWith="Private";*****/
					}
				  else if(sharedWith.equals("A"))
					{
					  sharedWith=LC.L_All_AccUsers;/*sharedWith="All Account Users";*****/
					}
				  else if(sharedWith.equals("G"))
					{
					  sharedWith=MC.M_AllUsr_InGrp;/*sharedWith="All Users in a Group";*****/
					}
				  else if(sharedWith.equals("S"))
					{
					  sharedWith=MC.M_UsrsStd_Team;/*sharedWith="All Users in a "+LC.Std_Study+" Team";*****/
					}
				  else if(sharedWith.equals("O"))
					{
					  sharedWith=MC.M_AllUsr_InOrg;/*sharedWith="All Users in an Organization";*****/
					}%>
				<td><%=sharedWith%></td>

				<td  align="center"><A href="#" onClick="openFormPreview(<%=formId%>)"><img src="../images/jpg/preview.gif" title="<%=LC.L_Preview%>" border="0"></A></td>				
				<td  align="center"><A href="formLibDelete.jsp?selectedTab=<%=selectedTab%>&formId=<%=formId%>&srcmenu=<%=src%>" onclick="return confirmBox('<%=StringUtil.escapeSpecialCharJS(name)%>',<%=pageRight%>)"><img src="../images/delete.png" title="<%=LC.L_Delete%>" border="0"/></A></td>
				
				<td  align="center">
				<A><img src="../images/jpg/help.jpg" border="0" onmouseover="return overlib('<%="<tr><td><font size=2><b>"+LC.L_Creator/*Creator*****/+" : </b></font><font size=1>"+StringUtil.escapeSpecialCharJS(creator)+"</font></td></tr><tr><td><font size=2><b>"+LC.L_Created_On/*Created On*****/+": </b></font><font size=1>"+createdon+"</font></td></tr><tr><td><font size=2><b>"+LC.L_Last_ModifiedBy/*Last Modified By*****/+": </b></font><font size=1>"+ StringUtil.escapeSpecialCharJS(modifiedby)+"</font></td></tr><tr><td><font size=2><b>"+LC.L_Last_ModifiedOn/*Last Modified On*****/+" : </b></font><font size=1>"+modifiedon+"</font></td></tr>"%>',CAPTION,'<%=LC.L_Form_Upper %><%-- FORM*****--%>',RIGHT,ABOVE)"; onmouseout="return nd();"></A>
				</td>
				
								
							
				</tr>
		<%}%>

					
	</table>
	<!-- Bug#9751 16-May-2012 Ankit -->
	<table width="98%" cellspacing="0" cellpadding="0" class="midalign">
		<tr valign="top"><td>
		<% if (totalRows > 0) 
		{ Object[] arguments = {firstRec,lastRec,totalRows};		
		%>
					<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<%} else {%>
			<font class="recNumber"><BR><b><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></b></font>	
		<%}%>	
	   </td></tr>
	</table>
			
	<div align="center" class="midalign">		
	<%
		if (curPage==1) startPage=1;
	    for ( count = 1; count <= showPages;count++)
		{
		
		 
   			cntr = (startPage - 1) + count;
	 		if ((count == 1) && (hasPrevious))
			{  
			  %>
				
			  	<A href="formLibraryBrowser.jsp?selectedTab=<%=selectedTab%>&mode=final&txtName=<%=txtName%>&ByFormType=<%=txtFormType%>&srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> > </A>
				&nbsp;&nbsp;&nbsp;&nbsp;
					
				<%
  			}	%>
		
		<%
 		 if (curPage  == cntr)
		 {	 
	    	 %>	   
				<FONT class = "pageNumber"><%= cntr %></Font>
       		<%
       	}else
        {	%>		
		<A href="formLibraryBrowser.jsp?selectedTab=<%=selectedTab%>&mode=final&txtName=<%=txtName%>&ByFormType=<%=txtFormType%>&srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>"><%= cntr%></A>
       <%}%>
	
		<%	}
		if (hasMore)
		{   %>
	   &nbsp;&nbsp;&nbsp;&nbsp;
	   <A href="formLibraryBrowser.jsp?selectedTab=<%=selectedTab%>&mode=final&txtName=<%=txtName%>&ByFormType=<%=txtFormType%>&srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
		<%	}	%>
	</div>
	
	</FORM>  
	  
	 <%
	 
	 
		
	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right
	}
else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div><div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/></div>

</body>

</html>

			
