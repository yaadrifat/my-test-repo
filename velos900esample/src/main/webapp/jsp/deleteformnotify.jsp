<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<head>
	<title><%=LC.L_Del_FormNotification%><%--Delete Form Notification*****--%></title>
</head>

<jsp:include page="popupJS.js" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY> 
<br>

<jsp:useBean id="formNotifyJB" scope="request"  class="com.velos.eres.web.formNotify.FormNotifyJB"/>

<DIV class="popDefault" id="div1">
<% 

HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession))	{
  
	String formNotifyId = request.getParameter("notifyid");
	    formNotifyId=(formNotifyId==null)?"":formNotifyId;
	String delMode=request.getParameter("delMode");
	if (delMode==null) {
		delMode="final";
%>
	<FORM name="deleteformnotify" id="delformnotifyfrm" method="post" action="deleteformnotify.jsp" onSubmit="if (validate(document.deleteformnotify)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br>
	<P class="successfulmsg"><%=LC.L_Del_FormNotification%><%--Delete Form Notification*****--%></P>
	<P class="defComments"><%=MC.M_Etr_Esign_ToProc%><%--Please enter e-Signature to proceed*****--%></P>	
	<table width="60%" cellpadding=0 cellspacing=0>
		<tr valign=baseline bgcolor="#cccccc">
			<td>
				<jsp:include page="submitBar.jsp" flush="true"> 
					<jsp:param name="displayESign" value="Y"/>
					<jsp:param name="formID" value="delformnotifyfrm"/>
					<jsp:param name="showDiscard" value="N"/>
				</jsp:include>
			</td>
		</tr>
	</table>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	
 	 <input type="hidden" name="notifyid" value="<%=formNotifyId%>">
	  </FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {

					int ret=0;
					formNotifyJB.setFormNotifyId(EJBUtil.stringToNum(formNotifyId));
					// Modified for INF-18183 ::: AGodara 
					ret = formNotifyJB.removeFormNotify(AuditUtils.createArgs(tSession,"",""));
					
%><br><BR><BR><BR><BR><%
	if(ret == 0) {
%>
		<p class = "successfulmsg" align = center> <%=MC.M_FrmNotific_DelSucc%><%--Form Notification was deleted successfully.*****--%></p>
<%
		} 
		 else {
%>
		<p class = "successfulmsg" align = center> <%=MC.M_FrmNotific_CntBeDel%><%--Form Notification could not be deleted .*****--%> </p>
<%
		}
	 %>
	<% if (ret >= 0)
		{%>
		  <script>
				window.opener.location.reload();
				setTimeout("self.close()",1000);
		  </script>	  				
		<%
		} // end of if status got deleted 
		  else
			 {
		 	%>
			 	<table width="550" border = "0">
		 			<tr>
						<td align="center">
							<button onClick="window.self.close();"><%=LC.L_Close%></button>	 
						</td>
		   			</tr>
	 			</table>	
			<%
		  }  
		
	} //end esign
 } //end of delMode
}//end of if body for session
else {
%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/> 
<%
}
%>
   
</DIV>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</BODY>
</HTML>
