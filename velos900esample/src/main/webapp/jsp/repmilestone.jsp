<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Mstone_Rpts%><%--Milestone Reports*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT language="Javascript">


function fOpenWindow(type,section,frmobj){
	width=300;
	height= 300;
	if (type == "Y"){
		width=200;
		height=70;
	}
	if (type == "D"){
		width=500;
		height=120;
	}
	if (type == "M"){
		width=270;
		height=70;
	}
	frmobj.year.value ="";
	frmobj.month.value ="";
	frmobj.dateFrom.value ="";
	frmobj.dateTo.value ="";

	frmobj.range.value = "";
	if (type == 'A'){
		frmobj.range.value = "All"
	}
	 if (type != 'A'){
		windowName=window.open("selectDateRange.jsp?parm="+type +"&section=" +section,"Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,dependant=true,width=" +width +",height="+height)
		windowName.focus();
	}
}


function fSetId(ddtype,frmobj){
	frmobj.orgId.value = "";
	frmobj.selOrg.value = "None";
	frmobj.val.value = "";
	studyPk =frmobj.studyPk.value;
    windowName=window.open("sitepopup.jsp?studyPk="+studyPk,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=300,height=300");
	windowName.focus();
}

function fValidate(frmobj){

	reportChecked=false;
	for(i=0;i<frmobj.reportName.length;i++){
		sel = frmobj.reportName[i].checked;
		if (frmobj.reportName[i].checked){
	   		reportChecked=true;
		}
	}
	if (reportChecked==false) {
		alert("<%=MC.M_Selc_Rpt%>");/*alert("Please select a Report");*****/
		return false;
	}

	frmobj.action = "repmilegen.jsp";

	reportNo = frmobj.repId.value;

	switch (reportNo) {

		case "67": //Milestones Summary (Study)
		case "68": //Milestones Summary (All Patients)
		case "70": //Milestones by Month
		case "71": //Milestones by Quarter
		case "72": //Milestones by Year
		case "73": //Payments Received by Month
		case "74": //Payments Received by Quarter
		case "75": //Payments Received by Year
		case "76": //Milestones/Payment Discrepancy by Month
		case "77": //Milestones/Payment Discrepancy by Quarter
		case "78": //Milestones/Payment Discrepancy by Year
		case "48": //Milestone Forecast by Month
		case "49": //Milestone Forecast by Quarter
		case "50": //Milestone Forecast by Year

				dateChecked=false;
				if (frmobj.range.value != ""){
	   				dateChecked=true;
				}
				if (dateChecked==false) {
					alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
					return false;
				}
	   			break;


		case "69": //Milestones Summary (Patient)
				dateChecked=false;
	   			if (fnTrimSpaces(frmobj.id.value) == "") {
					alert("<%=MC.M_Selc_Pat%>");/*alert("Please select a <%=LC.Pat_Patient%>");*****/
	   				return false;
   				}
				if (frmobj.range.value != ""){
	   				dateChecked=true;
				}
				if (dateChecked==false) {
					alert("<%=MC.M_Selc_DtRange%>");/*alert("Please select a Date Range");*****/
					return false;
				}
	   			break;
	}

    if (!(validate_col('Generated Report Name',frmobj.genRepName))) return false

}

function fSetIdNew(ddtype,frmobj){
	if (ddtype == "patient") {
		frmobj.id.value = "";
		frmobj.selPatient.value = "None";
		frmobj.val.value = "";
		openwindow();
	}

	if (ddtype == "report") {//report Id and name are concatenated by %.Need to separate them
		for(i=0;i<frmobj.reportName.length;i++)	{
			if (frmobj.reportName[i].checked){
				lsReport = frmobj.reportName[i].value;
				ind = lsReport.indexOf("%");
				frmobj.repId.value = lsReport.substring(0,ind);
				frmobj.repName.value = lsReport.substring(ind+1,lsReport.length);
				break;
			}
		}
	}
}

</SCRIPT>

<SCRIPT language="JavaScript1.1">
function openwindow() {
    windowName=window.open("patientsearch.jsp?openMode=P&srcmenu=&patid=&patstatus=","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=400,top=200,left=200");
	windowName.focus();
}

</SCRIPT>

</head>

<% String src="";
src= request.getParameter("srcmenu");
String studyId = request.getParameter("studyId");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="repdao" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao1" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="repdao2" scope="request" class="com.velos.eres.business.common.ReportDaoNew"/>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="ctrlD" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page import="com.velos.eres.service.util.LC"%><%@ page import="com.velos.eres.service.util.MC"%>

<body>

<DIV class="tabDefTop" id="div1">
<table><tr><td>
<P class="sectionHeadings"><%=LC.L_Milestones_Reports%><%--Milestones >> Reports*****--%> </P>
</td></tr></table>
<jsp:include page="milestonetabs.jsp" flush="true">
<jsp:param name="studyId" value="<%=studyId%>"/>
</jsp:include>
</div>
<DIV class="tabDefBot" id="div2">

<%
HttpSession tSession = request.getSession(true);

   if (sessionmaint.isValidSession(tSession))
   {
     String pageRight= "";
  int pgRight = 0;


	pageRight = (String) tSession.getValue("mileRight");
  pgRight = EJBUtil.stringToNum(pageRight);

  if (pgRight > 0 ){

	String uName =(String) tSession.getValue("userName");
	String userId = (String) tSession.getValue("userId");

	int acc = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));
	String tab = request.getParameter("selectedTab");

	String modRight = (String) tSession.getValue("modRight");

	int modlen = modRight.length();

	ctrlD.getControlValues("module");
	int ctrlrows = ctrlD.getCRows();

	ArrayList feature =  ctrlD.getCValue();
	ArrayList ftrDesc = ctrlD.getCDesc();
	ArrayList ftrSeq = ctrlD.getCSeq();
	ArrayList ftrRight = new ArrayList();
	String strR;

	ArrayList ctrlValue = new ArrayList();

	for (int counter = 0; counter <= (modlen - 1);counter ++)
	{
		strR = String.valueOf(modRight.charAt(counter));
		ftrRight.add(strR);
	}

	grpRights.setGrSeq(ftrSeq);
    grpRights.setFtrRights(ftrRight);
	grpRights.setGrValue(feature);
	grpRights.setGrDesc(ftrDesc);


	int eptRight = 0;
	eptRight  = Integer.parseInt(grpRights.getFtrRightsByValue("MODEPT"));

	Calendar cal = Calendar.getInstance();
	int currYear = cal.get(cal.YEAR);

	String filterText="";

	int counter=0;
	String ver = "";

	CtrlDao ctrl = new CtrlDao();
	ctrl.getControlValues("mile_status_rep");
	String catId = (String) ctrl.getCValue().get(0);

	repdao.getAllRep(EJBUtil.stringToNum(catId),acc); //Milestone Status Reports
	ArrayList repIdsAllStatuses= repdao.getPkReport();
	ArrayList namesAllStatuses= repdao.getRepName();
   	ArrayList descAllStatuses= repdao.getRepDesc();
	ArrayList repFilterAllStatuses = repdao.getRepFilters();

	CtrlDao ctrl1 = new CtrlDao();
	ctrl1.getControlValues("mile_pay_rep");
	catId = (String) ctrl1.getCValue().get(0);
	repdao1.getAllRep(EJBUtil.stringToNum(catId),acc); //Milestone/Payment Received Reports
	ArrayList repIdsOnePayments= repdao1.getPkReport();
	ArrayList namesOnePayments= repdao1.getRepName();
   	ArrayList descOnePayments= repdao1.getRepDesc();
	ArrayList repFilterOnePayments = repdao1.getRepFilters();


	CtrlDao ctrl2 = new CtrlDao();
	ctrl2.getControlValues("mile_project_rep");
	catId = (String) ctrl2.getCValue().get(0);
	repdao2.getAllRep(EJBUtil.stringToNum(catId),acc); //Milestone Projection Reports
	ArrayList repIdsProjections= repdao2.getPkReport();
	ArrayList namesProjections= repdao2.getRepName();
   	ArrayList descProjections= repdao2.getRepDesc();
	ArrayList repFilterProjections = repdao2.getRepFilters();


	int repIdAllStatus=0;
	String nameAllStatus="";
	int repIdOnePayment=0;
	String nameOnePayment="";
	int repIdProjection = 0;
	String nameProjection = "";

	int lenAllStatus = repIdsAllStatuses.size() ;
	int lenOnePayment = repIdsOnePayments.size() ;
	int lenProjection = repIdsProjections.size() ;
%>

<Form Name="reports" method="post" action=""  onSubmit="return fValidate(document.reports)">
<input type="hidden" name="srcmenu" value='<%=src%>'>
<input type="hidden" name="selectedTab" value='<%=tab%>'>
<input type=hidden name=year>
<input type=hidden name=month>
<input type=hidden name=year1>
<input type=hidden name=dateFrom>
<input type=hidden name=dateTo>

 <table width="100%">
<tr>
	<th width="35%"><Font class="reportText"><%=LC.L_Report%><%--Report*****--%></font></th>
	<th width="25%"><Font class="reportText"><%=LC.L_Required_Filters%><%--Required Filters*****--%></font></th>
	<th width="40%"><Font class="reportText"><%=LC.L_Available_Filters%><%--Available Filters*****--%></font></th>
 </tr>
</table>
<table width="100%">
	<tr>
	<td width="60%">

		<table>

         <tr>
         	 <td colspan=3>
         	<p class = "reportHeadings"><BR><%=LC.L_Mstone_StatusRpts%><%--Milestone Status Reports*****--%></p>
         	</td>
         	</tr>
<%
 		for(counter = 0;counter<lenAllStatus;counter++)
		{
		    repIdAllStatus = ((Integer)repIdsAllStatuses.get(counter)).intValue();
			nameAllStatus=((namesAllStatuses.get(counter)) == null)?"-":(namesAllStatuses.get(counter)).toString();

			if ((counter%2)==0) {
		  	%>
      			<tr class="browserEvenRow">
        	<%
			}else{ %>
      			<tr class="browserOddRow">
        	<%}%>
         	<td width="35%">
			<Input Type="radio" name="reportName" value="<%=repIdAllStatus%>%<%=nameAllStatus%>" onClick=fSetIdNew('report',document.reports)> <%=nameAllStatus%>
			</td>
			<td width="25%"><%=repFilterAllStatuses.get(counter)%></td>
		</tr>
        <%
          }%>

         <tr>
         	 <td colspan=3>
         	<p class = "reportHeadings"><BR><%=MC.M_MstoneOrPment_Rpt%><%--Milestones/Payments Received Reports*****--%></p>
         	</td>
         	</tr>

         	<% for(counter = 0;counter<lenOnePayment;counter++)
         	{
			repIdOnePayment = ((Integer)repIdsOnePayments.get(counter)).intValue();
			nameOnePayment=((namesOnePayments.get(counter)) == null)?"-":(namesOnePayments.get(counter)).toString();
			if ((counter%2)==0) {
		  	%>
      			<tr class="browserEvenRow">
        	<%
			}else{ %>
      			<tr class="browserOddRow">
        	<%
		}
         	%><td width="35%">
		<%
		if(eptRight==0){
			if ((repIdOnePayment!=65) && (repIdOnePayment!=66)) {
		%>
			<Input Type="radio" name="reportName"  value="<%=repIdOnePayment%>%<%=nameOnePayment%>" onClick=fSetIdNew('report',document.reports)> <%=nameOnePayment%>
			<%}%>
		<%} else {%>
				<Input Type="radio" name="reportName"  value="<%=repIdOnePayment%>%<%=nameOnePayment%>" onClick=fSetIdNew('report',document.reports)> <%=nameOnePayment%>
		<%}%>
		</td>
		<td width="25%"><%=repFilterOnePayments.get(counter)%></td>
		</tr>
         	<%}%>

         <tr>
         	 <td colspan=3>
         	<p class = "reportHeadings"><BR><%=LC.L_Mstone_ProjectionRpts%><%--Milestone Projection Reports*****--%></p>
         	</td>
         	</tr>

<%
 		for(counter = 0;counter<lenProjection;counter++)
		{
		    repIdProjection = ((Integer)repIdsProjections.get(counter)).intValue();
			nameProjection=((namesProjections.get(counter)) == null)?"-":(namesProjections.get(counter)).toString();

			if ((counter%2)==0) {
		  	%>
      			<tr class="browserEvenRow">
        	<%
			}else{ %>
      			<tr class="browserOddRow">
        	<%}%>
         	<td width="35%">
			<Input Type="radio" name="reportName" value="<%=repIdProjection%>%<%=nameProjection%>" onClick=fSetIdNew('report',document.reports)> <%=nameProjection%>
			</td>
			<td width="25%"><%=repFilterProjections.get(counter)%></td>
		</tr>
        <%
          }%>


         </table>
</td>

 <td width="40%" valign="top">
	<table><tr>
         	<td><br><br><Font class=reportText><%=LC.L_Date_Filter%><%--*****--%>:</Font></td></tr>
			<tr>
			<td colspan="2">
         	<Input type="radio" name="filterType" checked value="1" onClick="fOpenWindow('A','1', document.reports)"> <%=LC.L_All%><%--All*****--%>
         	<Input type="radio" name="filterType" value="2" onClick="fOpenWindow('Y','1', document.reports)"> <%=LC.L_Year%><%--Year*****--%>
         	<Input type="radio" name="filterType" value="3" onClick="fOpenWindow('M','1', document.reports)"> <%=LC.L_Month%><%--Month*****--%>
         	<Input type="radio" name="filterType" value="4" onClick="fOpenWindow('D','1', document.reports)"> <%=LC.L_Date_Range%><%--Date Range*****--%> <br>
         	<input type=text name=range size=30 READONLY value="All">
         	</td>
			</tr>
			 <tr>
	<td colspan="2"><br><br><Font class=reportText><%=LC.L_Addl_Filters%><%--Additional Filters*****--%>:</Font><BR></td>
</tr>

	<tr>
       	<td><FONT class = "comments"><A href="#" onclick="fSetId('org',document.reports)"><%=LC.L_Select_Org%><%--Select Organization*****--%></A> </FONT> </td>
       	<td><Input type="text" name="selOrg" size="20" READONLY value="None"></td>
	</tr>

<tr>
		<td><FONT class = "comments"><A href="#" onclick="fSetIdNew('patient',document.reports)"><%=LC.L_Select_APat%><%--Select a <%=LC.Pat_Patient%>*****--%></A></FONT></td>
		<td><Input type=text name=selPatient size=20 READONLY value=None></td>
</tr>
		<tr>
			<td colspan="2" align="center">
			<br><br>
	<!--		<A onClick = "return fValidate(document.reports)" href="#"><input type="image" src="../images/jpg/displayreport.jpg" align="absmiddle" border=0></A>-->
	<input type="image" src="../images/jpg/displayreport.jpg" align="absmiddle" border=0>
			</td>
		</tr>

         </table>
</td></table>

<Input type = hidden  name=id value="">
<Input type = hidden  name="val" >
<Input type=hidden name="studyPk" value=<%=studyId%>>
<input type="hidden" name="orgId" value=>
<Input type=hidden name="repId">
<Input type=hidden name="repName">
<Input type=hidden name="rangeType">
<Input Type=hidden name="reportNumber">

<Input type=hidden name="filterText" value = "<%=filterText%>">

<table width="75%" cellspacing="0" cellpadding="0" border=0 >
<tr>
	<td>
		<FONT class="comments"><%=MC.M_GiveName_ForGenrRpt%><%--Give a name for the Generated Report*****--%> </Font><FONT class="Mandatory"> * </FONT>
	</td>
	<td>
		<Input name="genRepName" type=text size=30 maxlength=100>
	</td>
</tr>
</table>

</form>


  <%


	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right




} //end of if session times out
else{
%>
	<jsp:include page="timeout.html" flush="true"/>

<%
}
%>

<div>
<jsp:include page="bottompanel.jsp" flush="true"/>

</div>

</div>

<DIV class="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
</body>
</html>


