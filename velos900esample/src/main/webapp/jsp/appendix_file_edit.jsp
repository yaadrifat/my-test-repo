<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<%
boolean isIrb = "irb_upload_tab".equals(request.getParameter("selectedTab")) ? true : false; 
String includeTabsJsp = isIrb ? "irbnewtabs.jsp" : "studytabs.jsp";
if (isIrb) { 
%>
<title><%=MC.M_ResComp_NewUpldDocu%><%--Research Compliance >> New Application >> Upload Documents*****--%></title>
<% } else { %>
<title><%=MC.M_StdVer_FileDets%><%--<%=LC.Std_Study%> >> Version >> File Details--%></title>
<% } %>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



</HEAD>



<% String src;
String from = "appendixver";
src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true">

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   



<SCRIPT Language="javascript">

 function  validate(formobj){

//     formobj=document.update

     if (!(validate_col('Description',formobj.desc))) return false
      if(formobj.desc.value.length>500){  
	     alert(" <%=MC.M_SrtDesc_MaxChar%>");/*alert(" 'Short Description' exceeded maximum number of characters allowed.");*****/
	     formobj.desc.focus();
	     return false;
     } 
	 
	 if (!(validate_col('Esign',formobj.eSign))) return false
	 
 	 if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
   }	 

 }


</SCRIPT>

<BODY>
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="appendixB" scope="request" class="com.velos.eres.web.appendix.AppendixJB"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<DIV class="BrowserTopn" id="div1"> 
  <jsp:include page="<%=includeTabsJsp%>" flush="true">
  <jsp:param name="from" value="<%=from%>"/>  
  </jsp:include>
  </DIV>
<DIV class="BrowserBotN BrowserBotN_S_3" id = "div1">
  <%

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))
{
	String uName = (String) tSession.getValue("userName");

	String sessStudyId = (String) tSession.getValue("studyId");
	String studyVerId = request.getParameter("studyVerId");

	String studyNo = (String) tSession.getValue("studyNo");

	String tab = request.getParameter("selectedTab");
	
	String appndxId = request.getParameter("appndxId");
	String shortDesc="";
	
	int stId=EJBUtil.stringToNum(sessStudyId);
	
	appendixB.setId(EJBUtil.stringToNum(appndxId));
	appendixB.getAppendixDetails();
	shortDesc=appendixB.getAppendixDescription();
	
%>

<form name="update" id="appFileEditId" METHOD=POST action="appendix_file_update.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>&appndxId=<%=appndxId%>" onsubmit = "if (validate(document.update)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<input type="hidden" name="studyVerId" value=<%=studyVerId%>>
    <table width="100%" >
      <tr> 
        <td colspan="2" align="center"> 
          <P class = "defComments"> <b><%=LC.L_Edit_Dets%><%--Edit details*****--%> </b></P>
        </td>
      </tr>
    <tr><td>&nbsp;</td></tr>
      <tr> 
        <td width="20%" align="right"> <%=LC.L_Short_Desc%><%--Short Description*****--%> <FONT class="Mandatory" >* </FONT> 
        </td>
        <td width="65%"> 
          <TextArea type=text name=desc row=3 cols=50 value="<%=shortDesc%>"><%=shortDesc%> </TextArea>
        </td>
      </tr>
      <tr> 
        <td width="35%"> </td>
        <td width="65%"> 
          <P class="defComments"> <%=MC.M_ShortDescFile_500CharMax%><%--Give a short description of your file (500 char 
            max.)*****--%> </P>
        </td>
      </tr>
    </table>
    <table width="100%" >
      
      <tr> 
        <td  colspan=2> <%=MC.M_DoYouWant_InfoToPublic%><%--Do you want Information in this section to be available 
          to the public?*****--%> </td>
      </tr>
      <tr> 
        <td colspan=2> 
		<%
			if(appendixB.getAppendixPubFlag().equals("Y")){
		%>
          <input type="Radio" name="pubflag" value=Y checked>
          <%=LC.L_Yes%><%--Yes*****--%> 
          <input type="Radio" name="pubflag" value=N >
          <%=LC.L_No%><%--No*****--%> 
		<%
			} else {
		%>
		  <input type="Radio" name="pubflag" value=Y >
          <%=LC.L_Yes%><%--Yes*****--%> 
          <input type="Radio" name="pubflag" value=N checked>
          <%=LC.L_No%><%--No*****--%> 
		<%
			} 
		%>
		  
		  &nbsp; <A href="pubvsnonpub.htm" target="Information" onClick="openwin()"><%=MC.M_PublicVsNotPublic_Info%><%--What 
          is Public vs Not Public Information?*****--%></A> </td>
      </tr>
    </table>

	 <BR>


	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="appFileEditId"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>


  </form>
	
    <BR>
    <table width="100%" cellspacing="0" cellpadding="0">
	  <tr>
      <td align=right> 
 <!--       <input type="Submit" name="submit" value="Submit"> -->
<!-- <input type="image" src="../images/jpg/Submit.gif" onClick = "return validate()" align="absmiddle">	-->
       </td>
      </tr>
    </table>
  </form>
  <%

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
</body>

</html>
