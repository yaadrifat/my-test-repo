<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=MC.M_View_UserInEres%><%--View User in eResearch*****--%></title>
<%
	response.setHeader("Cache-Control", "max-age=1, must-revalidate");
%>
<%@ page
	import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB"%><%@page
	import="com.velos.eres.service.util.MC"%><%@page
	import="com.velos.eres.service.util.LC"%>
<jsp:useBean id="userB" scope="page"
	class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="addressUserB" scope="page"
	class="com.velos.eres.web.address.AddressJB" />
<jsp:useBean id="siteB" scope="page"
	class="com.velos.eres.web.site.SiteJB" />
<jsp:useBean id="addressSiteB" scope="page"
	class="com.velos.eres.web.address.AddressJB" />
<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />

<%@ page language="java"
	import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil"%>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%
	String src;
	String pname;
	HttpSession tSession = request.getSession(true);
	int userId = EJBUtil.stringToNum(request.getParameter("userId"));
	//pname=request.getParameter("pname");
	src = request.getParameter("srcmenu");
	String uName = (String) tSession.getValue("userName");
	src = null;
%>

<%--

<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>  --%>


<%
	int ienet = 2;

	String agent1 = request.getHeader("USER-AGENT");
	if (agent1 != null && agent1.indexOf("MSIE") != -1)
		ienet = 0; //IE
	else
		ienet = 1;
	if (ienet == 0) {
%>
<body style="overflow: scroll;">
<%
	} else {
%>
<body>
<%
	}
%>

<!--<body style="overflow:scroll;">-->

<%--

<DIV class="formDefault" id = "div1">

--%>

<%
	// String uName = (String) tSession.getValue("userName");

	String acc = (String) tSession.getValue("accountId");

	if (sessionmaint.isValidSession(tSession))

	{
%>
<jsp:include page="sessionlogging.jsp" flush="true" />
<%
	int accId = EJBUtil.stringToNum(acc);
		int pageRight = 0;
		GrpRightsJB grpRights = (GrpRightsJB) tSession
				.getValue("GRights");
		pageRight = Integer.parseInt(grpRights
				.getFtrRightsByValue("MUSERS"));

		//String mode = request.getParameter("mode");
		//(mode.equals("M") 
		if ((pageRight >= 6) && (pageRight == 5 || pageRight == 7)) {
%>
<%
	String dJobType = "";
%>
<%
	String userAddresId = "";
			CodeDao cdJtype = new CodeDao();
			CodeDao cdPri = new CodeDao();
			//	CodeDao cd2 = new CodeDao();
			//	CodeDao cd = new CodeDao();
			//	CodeDao cd3 = new CodeDao();
			String userLastName = "";
			String userFirstName = "";
			String userMidName = "";
			String userAddPri = "";
			String userAddCity = "";
			String userAddState = "";
			String userAddZip = "";
			String userAddCountry = "";
			String userAddPhone = "";
			String userEmail = "";

			String userGroup = "";

			String userLogin = "";

			String userOldPwd = "";

			String userSession = "";

			String userSiteId = "";

			String userJobType = "";

			String userSpl = "";

			String userWrkExp = "";

			String userPhaseInv = "";

			String siteName = "";

			String siteAddPri = "";

			String userSecretQues = "";

			String userSecretAns = "";

			String dUsrGrps = "";

			String dAccSites = "";

			String dPrimSpl = "";

			//cd2.getAccountSites(accId); 

			//cd.getCodeValues("job_type"); 

			//cd3.getCodeValues("prim_sp");

			//userId =685;//= EJBUtil.stringToNum(request.getParameter("userId"));

			userB.setUserId(userId);

			userB.getUserDetails();

			if (userB.getUserPerAddressId() != null) {

				addressUserB.setAddId(EJBUtil.stringToNum(userB
						.getUserPerAddressId()));

				addressUserB.getAddressDetails();

			}

			if (userB.getUserSiteId() != null) {

				siteB.setSiteId(EJBUtil.stringToNum(userB
						.getUserSiteId()));

				siteB.getSiteDetails();

			}

			userLastName = userB.getUserLastName();

			userFirstName = userB.getUserFirstName();

			userMidName = userB.getUserMidName();

			userGroup = userB.getUserGrpDefault();

			userLogin = userB.getUserLoginName();

			userSecretAns = userB.getUserAnswer();

			userSecretQues = userB.getUserSecQues();

			userAddPri = addressUserB.getAddPri();

			userAddCity = addressUserB.getAddCity();

			userAddState = addressUserB.getAddState();

			userAddCountry = addressUserB.getAddCountry();

			userAddZip = addressUserB.getAddZip();

			userAddPhone = addressUserB.getAddPhone();

			userEmail = addressUserB.getAddEmail();

			userJobType = userB.getUserCodelstJobtype();

			userSpl = userB.getUserCodelstSpl();

			userWrkExp = userB.getUserWrkExp();

			userPhaseInv = userB.getUserPhaseInv();

			userOldPwd = userB.getUserPwd();

			userSiteId = userB.getUserSiteId();

			siteName = siteB.getSiteName();

			siteAddPri = addressSiteB.getAddPri();

			userAddresId = userB.getUserPerAddressId();

			cdJtype.getCodeValuesById(EJBUtil.stringToNum(userJobType));

			if (cdJtype.getCDesc().size() > 0)

				userJobType = (String) cdJtype.getCDesc().get(0);

			cdPri.getCodeValuesById(EJBUtil.stringToNum(userSpl));

			if (cdPri.getCDesc().size() > 0)

				userSpl = (String) cdPri.getCDesc().get(0);
%>







<br>







<Form name="userdetails" method="post" action="" onSubmit=""><input
	type="hidden" name="userOldPwd" size=20 value=<%=userOldPwd%>>



<input type="hidden" name="userId" size=20 value=<%=userId%>> <input
	type="hidden" name="userAddId" size=20 value=<%=userAddresId%>>



<input type="hidden" name="userSession" size=20 value=<%=userSession%>>

<input type="hidden" name="srcmenu" size=20 value=<%=src%>>

<table width="100%" cellspacing="0" cellpadding="0" border="0"
	class="basetbl outline">

	<tr id="headSection">

		<th><%=LC.L_Member_Dets%><%--Member Details*****--%></th>

	</tr>

</table>

<table width="100%" cellspacing="0" cellpadding="0" border="0"
	class="basetbl outline">

	<tr>

		<td width="300"><%=LC.L_First_Name%><%--First Name*****--%></td>

		<td width="300"><%=userFirstName%></td>



	</tr>



	<tr>



		<td width="300"><%=LC.L_Last_Name%><%--Last Name*****--%></td>



		<td width="300"><%=userLastName%></td>



	</tr>



	<tr>



		<td width="300"><%=LC.L_Address%><%--Address*****--%></td>



		<td width="300"><%=userAddPri%></td>



	</tr>



	<tr>



		<td width="300"><%=LC.L_City%><%--City*****--%></td>



		<td width="300"><%=userAddCity%></td>



	</tr>



	<tr>



		<td width="300"><%=LC.L_State%><%--State*****--%></td>



		<td width="300"><%=userAddState%></td>



	</tr>



	<tr>



		<td width="300"><%=LC.L_ZipOrPostal_Code%><%--Zip/Postal Code*****--%>



		</td>



		<td width="300"><%=userAddZip%></td>



	</tr>



	<tr>



		<td width="300"><%=LC.L_Country%><%--Country*****--%></td>



		<td width="300"><%=userAddCountry%></td>



	</tr>



	<tr>



		<td width="300"><%=LC.L_Phone%><%--Phone*****--%></td>



		<td width="300"><%=userAddPhone%></td>



	</tr>



	<tr>



		<td width="300"><%=LC.L_EhypMail%><%--E-Mail*****--%></td>



		<td width="300"><%=userEmail%></td>



	</tr>



	<tr>



		<td width="300"><%=LC.L_Job_Type%><%--Job Type*****--%></td>



		<td width="300"><%=userJobType%></td>



	</tr>



</table>



<br>















<table width="100%" width="100%" cellspacing="0" cellpadding="0"
	border="0" class="basetbl outline">



	<tr id="headSection">



		<th><%=LC.L_Investigator_Dets%><%--Investigator Details*****--%>



		</th>



	</tr>



</table>















<table width="100%" cellspacing="0" cellpadding="0" border="0"
	class="basetbl outline">







	<tr>



		<td width="300"><%=LC.L_Primary_Speciality%><%--Primary Speciality*****--%>



		</td>



		<td width="300"><%=userSpl%></td>



	</tr>



	<tr>



		<td width="300"><%=MC.M_YearsInvl_ClinicRes%><%--How many years have been involved in clinical research?*****--%>



		</td>



		<td width="300"><%=userWrkExp%></td>



	</tr>



	<tr>



		<td width="300"><%=MC.M_WhichPhase_TrialInvl%><%--Which phases of trials have been involved with?*****--%>



		</td>



		<td width="300"><%=userPhaseInv%></td>



	</tr>







</table>







</Form>







<%
	} //end of if body for page right

		else

		{
%>







<jsp:include page="accessdenied.jsp" flush="true" />















<%
	} //end of else body for page right

	}//end of if body for session

	else

	{
%>







<jsp:include page="timeout.html" flush="true" />



<%
	}
%>



<div><jsp:include page="bottompanel.jsp" flush="true" /></div>



<%--



</div> --%>







</body>
</html>

