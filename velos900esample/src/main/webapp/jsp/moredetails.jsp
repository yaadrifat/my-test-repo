<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- saved from url=(0022)http://internet.e-mail -->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB" %>
<%@page import="com.velos.eres.web.study.StudyJB" %>
<%@page import="com.velos.eres.web.user.UserJB,com.velos.eres.web.studyRights.StudyRightsJB" %>
<%@page import="com.velos.esch.web.eventassoc.EventAssocJB" %>
<%@page import="com.velos.esch.web.eventdef.EventdefJB" %>
<%@page import="com.velos.esch.web.advEve.AdvEveJB" %>

<jsp:include page="localization.jsp" flush="true"/>
<HTML><HEAD><TITLE><%=LC.L_More_Dets%><%--More Details*****--%></TITLE>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="include.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</HEAD>

<script>


//KM-Modified for 3917
function setValue(formobj,count,cbcount){

	totalcbcount=formobj.cbcount.value;
	if(formobj.alternateId.value == undefined)
	     value=formobj.alternateId[count].value;
	else 
	    value=formobj.alternateId.value; 
	
	if (value=="Y") {
		if(formobj.alternateId.value == undefined)
			formobj.alternateId[count].value="N";
		else 
			formobj.alternateId.value="N";

		
		if (totalcbcount==1) 
			formobj.alternate.checked=false;
		else 
			formobj.alternate[cbcount-1].checked=false;
	}

	if ((value.length==0) || (value=="N"))  {
		
		if (formobj.alternateId.value == undefined)
			formobj.alternateId[count].value="Y";
		else 
			formobj.alternateId.value="Y";
		
		if (totalcbcount==1) 
			formobj.alternate.checked=true;
		else 
			formobj.alternate[cbcount-1].checked=true;
	}
}




function setDD(formobj)
{
	
	optvalue=formobj.ddlist.value;
	if (optvalue.length>0)
	{
	arrayofStrings=optvalue.split("||");
	if (arrayofStrings.length>1) {
	for (var j=0;j<arrayofStrings.length;j++)
	{
	var ddStr=arrayofStrings[j];
	arrayofDD=ddStr.split(":");
	var ddcount=arrayofDD[0];
	var selvalue=arrayofDD[1];
	var opt = formobj.alternateId[ddcount].options;
	for (var i=0;i<opt.length;i++){
	if (opt[i].value==selvalue){
	formobj.alternateId[ddcount].selectedIndex=i ;
	}
	}
	}
	} else 
	{
	var ddStr=arrayofStrings[0];
	arrayofDD=ddStr.split(":");
	var ddcount=arrayofDD[0];
	var selvalue=arrayofDD[1];
	
	//KM-issue 3917
 	var opt = formobj.alternateId.options; 
	if (opt == undefined)
    	opt = formobj.alternateId[ddcount].options; 
	
	for (var i=0;i<opt.length;i++){
	if (opt[i].value==selvalue){
		
		if (formobj.alternateId.options != undefined)
			formobj.alternateId.selectedIndex=i ; 
		else 
			formobj.alternateId[ddcount].selectedIndex=i ; 
	}
	}
	}// end else
	}//optvalue.length>0	
}



function validate(formobj) {

 	 if (!(validate_col('e-Signature',formobj.eSign))) return false
	
     if(isNaN(formobj.eSign.value) == true) {
    	 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	   }

	return true;
}
 
 
</script>


<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {	
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%><%@ page import="com.velos.eres.service.util.*"%>

<jsp:useBean id="mdJB" scope="request" class="com.velos.eres.web.moreDetails.MoreDetailsJB"/>
<DIV class="popDefault" id="div1">

<%
	HttpSession tSession = request.getSession(true); 
	 if (sessionmaint.isValidSession(tSession))
	{
	 %>
	<jsp:include page="sessionlogging.jsp" flush="true"/>

<% 	
	 String accountId = (String) tSession.getValue("accountId");
	 String sessUserId = (String) tSession.getValue("userId");
	 String modId = request.getParameter("modId");
	 String modName=request.getParameter("modName");
	 String pageRightStr=request.getParameter("pageRight");
	 pageRightStr=(pageRightStr==null)?"":pageRightStr;
	 //Modified By Yogendra: Bug# 7985
	 String accessFromPage = request.getParameter("accessFromPage");
	 accessFromPage=(accessFromPage==null)?"":accessFromPage;
	 String calledFrom = StringUtil.trueValue(request.getParameter("calledFrom"));
	  
	  String defUserGroup = (String) tSession.getAttribute("defUserGroup");
	  	
	  	
	 int pageRight=EJBUtil.stringToNum(pageRightStr);
	 
	 if ("user".equals(request.getParameter("modName"))) {
		 GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		 pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));
		 if (StringUtil.stringToNum(sessUserId) == StringUtil.stringToNum(modId)) {
			 pageRight = 7;
		 }
		 UserJB userB = new UserJB();
		 userB.setUserId(StringUtil.stringToNum(modId));
		 userB.getUserDetails();
		 if (pageRight < 4 ||
				 StringUtil.stringToNum(accountId) != StringUtil.stringToNum(userB.getUserAccountId())) {
%>
<jsp:include page="accessdenied.jsp" flush="true"/>
</div>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
<%			 
			return;
		 }
	 } else if ("evtaddlcode".equals(request.getParameter("modName"))) {
		 if (StringUtil.isEmpty(calledFrom) && accessFromPage != null) {
			 if (accessFromPage.equalsIgnoreCase("eventlibrary")) {
				 calledFrom = "L";
			 }
		 }
		 if ("S".equals(calledFrom)) {
			 StudyRightsJB stdRights =(StudyRightsJB) tSession.getAttribute("studyRights");
			 if (stdRights == null || stdRights.getFtrRights() == null || stdRights.getFtrRights().size() == 0) {
				 pageRight = 0;
			 } else {
				 pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
			 }
		 } else if ("P".equals(calledFrom)) {
			 GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
			 pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
		 } else if ("L".equals(calledFrom)) {
			 GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
			 pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("EVLIB"));
		 } else {
			 pageRight = 0;
		 }
		 int moreAcctId = -1;
		 if("S".equals(calledFrom)) {
			 EventAssocJB eventAssocJB = new EventAssocJB();
			 eventAssocJB.setEvent_id(StringUtil.stringToNum(modId));
			 eventAssocJB.getEventAssocDetails();
			 moreAcctId = StringUtil.stringToNum(eventAssocJB.getUser_id());
			 if (moreAcctId < 1) { 
				 // If event_assoc is null; it could be a new event just created in event_def => check
				 EventdefJB eventdefJB = new EventdefJB();
				 eventdefJB.setEvent_id(StringUtil.stringToNum(modId));
				 eventdefJB.getEventdefDetails();
				 moreAcctId = StringUtil.stringToNum(eventdefJB.getUser_id());
			 }
		 } else {
			 EventdefJB eventdefJB = new EventdefJB();
			 eventdefJB.setEvent_id(StringUtil.stringToNum(modId));
			 eventdefJB.getEventdefDetails();
			 moreAcctId = StringUtil.stringToNum(eventdefJB.getUser_id());
		 }
		 if (pageRight < 4 ||
				 StringUtil.stringToNum(accountId) != moreAcctId) {
%>
<jsp:include page="accessdenied.jsp" flush="true"/>
</div>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
<%			 
			 return;
		 }
	 } else if ("advtype".equals(request.getParameter("modName"))) {
		 StudyRightsJB stdRights =(StudyRightsJB) tSession.getAttribute("studyRights");
		 String studyId = (String) tSession.getAttribute("studyId");
		 TeamDao teamDao = new TeamDao();
		 teamDao.getTeamRights(StringUtil.stringToNum(studyId),StringUtil.stringToNum(sessUserId));
		 ArrayList tId = teamDao.getTeamIds();
		 if (tId.size() == 0) {
			 pageRight = 0;
		 } else {
			 stdRights.setId(StringUtil.stringToNum(tId.get(0).toString()));
			 ArrayList teamRights;
			 teamRights  = new ArrayList();
			 teamRights = teamDao.getTeamRights();
			 stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
			 stdRights.loadStudyRights();
			 if ((stdRights.getFtrRights().size()) == 0){
				 pageRight= 0;
			 } else {
				 pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYMPAT"));
			 }
		 }
		 AdvEveJB advEveJB = new AdvEveJB();
		 advEveJB.setAdvEveId(StringUtil.stringToNum(modId));
		 advEveJB.getAdvEveDetails();
		 StudyJB studyJB = new StudyJB();
		 studyJB.setId(StringUtil.stringToNum(advEveJB.getFkStudy()));
		 studyJB.getStudyDetails();
		 if (pageRight < 4 || 
				 StringUtil.stringToNum(accountId) !=  StringUtil.stringToNum(studyJB.getAccount())) {
%>
<jsp:include page="accessdenied.jsp" flush="true"/>
</div>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
<%			 
			 return;
		 }
	 }
	 
	 //int studyRights = EJBUtil.stringToNum(request.getParameter("studyRights"));

         
	 //get study ids
	 MoreDetailsDao mdDao = new MoreDetailsDao();
	 mdDao = mdJB.getMoreDetails(EJBUtil.stringToNum(modId),modName,defUserGroup);
	 
 	 ArrayList modElementIdList  = new ArrayList();
	 ArrayList modElementDescList = new ArrayList();
	 ArrayList idList = new ArrayList();
	 ArrayList modElementDataList = new ArrayList();
 	 ArrayList recordTypeList = new ArrayList();
	 ArrayList dispTypeList=new ArrayList();
	 ArrayList dispDataList=new ArrayList();
	 
	 idList = mdDao.getId();
	 modElementIdList =  mdDao.getMdElementIds();
	 modElementDescList = mdDao.getMdElementDescs();
	 modElementDataList = mdDao.getMdElementValues();
	 recordTypeList = mdDao.getRecordTypes ();
	 dispTypeList=mdDao.getDispTypes();
	 dispDataList=mdDao.getDispDatas();
	 
	 
	

	 String strElementDesc ; 
 	 String strElementValue ; 
  	 String strRecordType ; 
  	 Integer iElementId; 
   	 Integer iId;
	 String disptype="";
	 String dispdata="";
	 String ddStr="";
  	 
   	
%>

<Form  name="moredetails" id="moredet" action="updatemoredetails.jsp" method="post" onSubmit="if (validate(document.moredetails)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}" >
<P class="sectionHeadingsFrm">		
<%=LC.L_More_Dets%><%--More Details*****--%>
</P>	
<table width="98%" cellspacing="2" cellpadding="2" border="0" class="basetbl">
<!--<tr>
   <th width="45%" >Other Study ID Type</th>
   <th width="55%" >Other Study ID</th>				
</tr> -->
<% 
	int counter = 0,cbcount=0;
	for (counter = 0; counter <= modElementIdList.size() -1 ; counter++)
		{
				strElementDesc = (String) modElementDescList.get(counter); 
 	 			strElementValue = (String) modElementDataList.get(counter); 

 	 			disptype=(String) dispTypeList.get(counter);
				dispdata=(String) dispDataList.get(counter);
				if (disptype==null) disptype="";
				if (dispdata==null) dispdata="";
				
 	 			if (strElementValue == null)
 	 				strElementValue = "";
 	 				
  	 			strRecordType = (String) recordTypeList.get(counter); 
				iElementId = (Integer) modElementIdList.get(counter) ; 
   	 			iId = (Integer) idList.get(counter) ;  
			
	%>
		
	<tr>
		     <td width="45%" >
				<%= strElementDesc %>
			 </td>	
		     <td width="55%" >
				<% if ((disptype.toLowerCase()).equals("chkbox")){
				cbcount=cbcount+1;
				%>
				<input type = "hidden" name = "alternateId" value = "<%=strElementValue.trim()%>" size = "25" maxlength = "100" >
						<%	 
					
				if ((strElementValue.trim()).equals("Y")){%>				
				 <input type="checkbox" name="alternate" value="<%=strElementValue.trim()%>" onClick="setValue(document.moredetails,<%=counter%>,<%=cbcount%>)" checked>
				  <% }else{%>
				  <input type="checkbox"  name="alternate" value="<%=strElementValue.trim()%>" onClick="setValue(document.moredetails,<%=counter%>,<%=cbcount%>)">
				  
				<%}} else if ((disptype.toLowerCase()).equals("dropdown")) {
				if (ddStr.length()==0) ddStr=(counter)+":"+strElementValue;
					else ddStr=ddStr+"||"+(counter)+":"+strElementValue;
				%>
					 
				  <%=dispdata%>
				 <%}  else if ((disptype.toLowerCase()).equals("splfld")) {%> 
				  <%=dispdata%>
				 <%}else {%>
				<input type = "text" name = "alternateId" value = "<%=strElementValue.trim()%>" size = "25" maxlength = "100" >
				<%}%>
				<input type = "hidden" name = "recordType" value = "<%=strRecordType %>" >
				<input type = "hidden" name = "id" value = "<%=iId%>" >
				<input type = "hidden" name = "elementId" value = "<%=iElementId%>" >
				<input type = "hidden" name = "modId" value = "<%=modId%>" >
				<input type = "hidden" name = "modName" value = "<%=modName%>" >
			 </td>	
	</tr>	
	
	<%
		}	
	%>
	<% if (modElementIdList.size() == 0) { %>
	<tr><td><P align="center"><%=MC.M_NoData_Found %></P></td></tr>
	<% }  %>
	<input type="hidden" name="cbcount" value=<%=cbcount%>>
	<input type="hidden" name="ddlist" value="<%=ddStr%>">
	
	<% 
	 String showSubmit = "";
	 if (pageRight>5) {
		showSubmit = "Y";
	} else {showSubmit ="N";}
	%>
	</table>
	<table width="98%">
	<tr><td style="width: 60%">
	<% if (modElementIdList.size() > 0) { %>
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="moredet"/>
			<jsp:param name="showDiscard" value="N"/>
			<jsp:param name="showSubmit" value="<%=showSubmit%>"/>
	</jsp:include>
	<% }  %>
	</td>
	<%--Modified By Yogendra: Bug# 7985 --%>
	<%if(accessFromPage.equalsIgnoreCase("fetchProt")){ %>
		<td style="width: 30%"><input class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-button-text" type="button" onclick="window.history.back();" value="<%=LC.L_Back%>" name="<%=LC.L_Back%>"></input></td>
	<%}%>
	</tr>
	</table>
	</form>
	<script>
	setDD(document.moredetails);
	</script>
<%}else{%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/> 
<%}%>
</div>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

