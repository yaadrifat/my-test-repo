<!--  Modified by gopu to fix the Nov.2005 Enhancement #5-->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.eres.service.util.EJBUtil" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<script>

function fopen(link,pgRight) {

// commented by gopu to fix bugzilla issues # 2439
		//windowName = window.open(link,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=400,height=280')
		//windowName.focus();
// Added by gopu to fix bugzilla issues # 2439

/*		document.getElementById("loggedonerror").action=link;
		document.getElementById("loggedonerror").submit();
*/

//JM: 031506: #2534
		document.loggedonerror.action = link;
		document.loggedonerror.submit();


}
</script>
<body>

<%

HttpSession tSession = request.getSession(true);


	int pageRight=0;

	//userB.validateUser(request.getParameter("userId"),request.getParameter("password"));
	//String logUsr=""+userB.getUserId();

	if (sessionmaint.isValidSession(tSession))

	{

	String uName = (String) tSession.getValue("userName");

	String accId = (String) tSession.getValue("accountId");

	//get the default group of the login user. If the default group is Admin then show reset password and reset user session for users

	//logUsr = ""+userB.getUserId();
 	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
    pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));
}






%>

<!-- Added by gopu to fix bugzilla issues # 2439	-->

<% if (sessionmaint.isValidSession(tSession)){ %>
<form name="loggedonerror" method="post" onsubmit="return fopen()">
<%}else{%>
<form  name="loggedonerror" method="post" action="login.jsp" >
<%}%>
<input type="hidden" name="fromPage" value = "reset" />
<input type="hidden" value="" name="password" id = "password" />
<input type="hidden" value="" name="logid" id = "logid" />
<input type="hidden" value="" name="username" id = "username" />

<input type="hidden" value="" name="userId" id = "userId" />

<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/>

<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil"%>


<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<br>
<table width = "100%" border = 0>
	<tr><td>
			<p class = "sectionHeadings">
				<%=MC.M_SorryNotLogged_SomeReason%><%--We are sorry; you cannot be logged on due to the following reason:*****--%>
			</p>
		</td>
	</tr>
	<tr><!-- Added by gopu to fix Nov.2005 Enhancement #5 -->
		<td>
		    <!-- Rohit Bug N0: 4675 -->
			<p class = "defComments"><br><br><%=MC.M_SessLoggedIn%><%--You have logged in at a different location; please save data and close previous session first!*****--%></p>
		</td>
	</tr>
	<tr>
	<td align="center"><br>
		<button onClick = "window.self.close()"><%=LC.L_Close%></button>
	</td>
</tr>
</table>

<!-- Added by gopu to fix bugzilla issues # 2439 -->
<SCRIPT>
	   document.getElementById("logid").value=window.opener.document.getElementById("loguser").value;
document.getElementById("username").value=window.opener.document.getElementById("loguser").value;
	   document.getElementById("password").value=window.opener.document.getElementById("password").value;
	   document.getElementById("userId").value= window.opener.document.getElementById("userId").value;
	   window.opener.window.history.back()
</SCRIPT>
</form>
</body>
