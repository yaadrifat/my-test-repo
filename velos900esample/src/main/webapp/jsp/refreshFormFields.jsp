<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<body>
<title><%=LC.L_Refresh_FormXsls%><%--Refresh Form XSLs*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<% 
int formId;
formId= EJBUtil.stringToNum(request.getParameter("formId"));

%>

<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="formFieldJB" scope="request"  class="com.velos.eres.web.formField.FormFieldJB"/>


<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.business.fieldLib.impl.FieldLibBean"

%>


<%
 HttpSession tSession = request.getSession(true); 
 
  if (sessionmaint.isValidSession(tSession))
	{
	
%>
<DIV >

<%	
		 int pageRight = 7;	
		 

		 if(pageRight >= 4)
		{
 %>
		
<Form name="formlib" action="refreshFormFields.jsp"  method="post" > 
	
	<%=MC.M_EtrIds_Comma432%><%--Enter form Ids separated by a comma. Example 223,123,432*****--%>
<textarea name=formIdList cols=25 rows=7> </textarea>
<Input name="Submit" type="Submit"/>
		<BR><BR>
	
	  <%
	  	  String idList = "";
	  
	  	idList = request.getParameter("formIdList");
	  
	  	if (! StringUtil.isEmpty(idList ))
	  	{
	  		idList = idList.trim();
	  		
	  		String[] formArray =  StringUtil.chopChop(idList, ',') ;
	  		
	  		if (formArray.length > 0  )
	  		{
	  			for (int k = 0; k < formArray.length; k++)
	  				{	
		  	 			ArrayList fieldNames = null;
			 			ArrayList fieldIds = null;
						ArrayList formFldIds = null;
						ArrayList formFldSeqs = null;
						
						 String fieldName = "";
						 Integer fieldId = null;
						 Integer formFieldId = null;
						 int len= 0;
						 Integer formFldSeq = null;
				 
						 FormLibDao formLibDao = new FormLibDao();
						 formLibDao = formlibB.getSectionFieldInfo(Integer.parseInt(formArray[k]));	 	
						 
						 
						 fieldNames = formLibDao.getFieldNames();
						 fieldIds =	formLibDao.getFieldIds();	
						 formFldIds = formLibDao.getFormFldIds();
						 formFldSeqs = formLibDao.getFormFldSeqs();	
						
						 len = fieldIds.size(); 
					  
					    int count=0;
						int fmtcount=1;
						int repcount=1;
					
						int id = 0;
						//int tcount=0;
						out.println("<BR>"+LC.L_For_Form_Upper/*FOR FORM*****/+":" + formArray[k]);
						System.out.println("<BR>FOR FORM:" + formArray[k]);
						for(count=0;count<len;count++)
						{					
						
							FieldLibBean fieldLsk  = new FieldLibBean();
								
							fieldName = (String) fieldNames.get(count);
							fieldId  = (Integer) fieldIds.get(count);
							id = fieldId.intValue();
							formFieldId = (Integer) formFldIds.get(count);
							formFldSeq =  (Integer) formFldSeqs.get(count);
							
							if (id > 0)
							{
								if (StringUtil.isEmpty(fieldName ))
									fieldName = "";
								
								  if (fieldName.length()  >= 26)
									{
									fieldName=fieldName.substring(0,25) ;
									fieldName=fieldName.concat(" ...");
									}
								
								
								
								fieldLibJB.setFieldLibId(id);
								fieldLibJB.getFieldLibDetails();
								
								formFieldJB.setFormFieldId(formFieldId.intValue() );
								formFieldJB.getFormFieldDetails();
								
								fieldLsk = fieldLibJB.createFieldLibStateKeeper();
								fieldLsk.setFormFldId(formFieldId.toString() );
								fieldLsk.setFormFldSeq(formFldSeq.toString());
								fieldLsk.setFormFldMandatory(formFieldJB.getFormFldMandatory());
								fieldLsk.setFormFldBrowserFlg(formFieldJB.getFormFldBrowserFlg());
								
								fieldLibJB.updateToFormField(fieldLsk);
								
								out.println("<BR>"+LC.L_Updated_Fieldname/*Updated fieldName:*****/+" " + fieldName);
								System.out.println("<BR>Updated fieldName: " + fieldName);
								
							}							
							
					} //count ==len 
		
				}		// end of for loop for forms
			//form array > 0		
			}	
		}		
					
	%>
		</form>
		<%
 } //end of rights 
else
{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right
}
else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
</body>
</html>


			
