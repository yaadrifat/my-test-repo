<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page import="com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>
<html>
<head>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%!
String buildBarChart(int index, ArrayList countList, int total) {
    if (countList.size() < 1) { return "&nbsp;"; }
    if (total < 1) { return "&nbsp;"; }
    double percent = Double.valueOf(((Integer)countList.get(index)).intValue()).doubleValue() * 100d;
    percent /= Double.valueOf(total).doubleValue();
    StringBuffer sb = new StringBuffer();
    sb.append("<table height=\"100%\" width=\"").append(Math.round(percent))
    .append("%\"><tr><td width=\"100%\" style=\"background-color:#880000\">&nbsp;</td></tr></table>");
    return sb.toString();
}
String buildSummaryTable(ArrayList tabList, String accountId, String grpId, String userId) {
    EIRBDao eIrbDao = new EIRBDao();
    eIrbDao.getSummarySubmissionTypes(EJBUtil.stringToNum(accountId), EJBUtil.stringToNum(grpId),
            EJBUtil.stringToNum(userId));
    ArrayList summarySubmissionTypes = eIrbDao.getSummarySubmissionTypes();
    ArrayList summarySubmissionTypesCounts = eIrbDao.getSummarySubmissionTypesCounts();
    eIrbDao.getSummarySubmissionStatuses(EJBUtil.stringToNum(accountId), EJBUtil.stringToNum(grpId),
            EJBUtil.stringToNum(userId));
    eIrbDao.getSummaryPIs(EJBUtil.stringToNum(accountId), EJBUtil.stringToNum(grpId),
            EJBUtil.stringToNum(userId));
    ArrayList summaryPIs = eIrbDao.getSummaryPIs();
    ArrayList summaryPICounts = eIrbDao.getSummaryPICounts();
    
    
    StringBuffer sb = new StringBuffer();
    for (int iX=0; iX<tabList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tabList.get(iX);
        String dispTxtLink = "<a href=\"irbsubmissions.jsp?selectedTab="+settings.getObjSubType()+
        "&mode=N\">" + settings.getObjDispTxt() + "</a>";
        if ("irb_summary_tab".equals(settings.getObjSubType())) { continue; }
        if ("irb_new_tab".equals(settings.getObjSubType())) {
            if (summarySubmissionTypesCounts.size() < 1) {
                sb.append("<tr class=\"browserEvenRow\" align=\"left\"><td width=\"30%\"><b>")
                .append(dispTxtLink).append("</b></td><td align=\"center\" width=\"20%\">")
                .append("0</td><td width=\"50%\"></td></tr>");
                continue;
            }
            sb.append("<tr class=\"browserEvenRow\" align=\"left\"><td width=\"30%\"><b>")
            .append(dispTxtLink).append("</b></td><td align=\"center\" width=\"20%\">")
            .append(summarySubmissionTypesCounts.get(summarySubmissionTypesCounts.size()-1))
            .append("</td><td width=\"50%\"></td></tr>");
            if (summarySubmissionTypesCounts.size() < 2) { continue; }
            for (int iY=0; iY<summarySubmissionTypes.size()-1; iY++) {
                sb.append("<tr class=\"browserOddRow\" ><td align=\"right\" width=\"30%\"><i>")
                .append(summarySubmissionTypes.get(iY))
                .append("</i></td><td align=\"center\" width=\"20%\">")
                .append(summarySubmissionTypesCounts.get(iY))
                .append("</td><td>").append(buildBarChart(iY, summarySubmissionTypesCounts,
                        ((Integer)summarySubmissionTypesCounts.get(summarySubmissionTypesCounts.size()-1)).intValue()))
                .append("</td></tr>");
            }
        } else if ("irb_assigned_tab".equals(settings.getObjSubType())) {
            sb.append( buildSubmissionStatusTable("admin_review", dispTxtLink, eIrbDao) );
        } else if ("irb_compl_tab".equals(settings.getObjSubType())) {
            sb.append( buildSubmissionStatusTable("admin_rev_comp", dispTxtLink, eIrbDao) );
        } else if ("irb_pend_rev".equals(settings.getObjSubType())) {
            sb.append( buildSubmissionStatusTable("reviewer", dispTxtLink, eIrbDao) );
        } else if ("irb_post_tab".equals(settings.getObjSubType())) {
            sb.append( buildSubmissionStatusTable("asgn_pr", dispTxtLink, eIrbDao) );
        } else if ("irb_appr_tab".equals(settings.getObjSubType()))
        {
        	continue;
        }
        else if ("irb_pend_tab".equals(settings.getObjSubType())) {
            if (summaryPICounts.size() < 1) {
                sb.append("<tr class=\"browserEvenRow\" align=\"left\"><td width=\"30%\"><b>")
                .append(dispTxtLink).append("</b></td><td align=\"center\" width=\"20%\">")
                .append("0</td><td width=\"50%\"></td></tr>");
                continue;
            }
            sb.append("<tr class=\"browserEvenRow\" align=\"left\"><td width=\"30%\"><b>")
            .append(dispTxtLink).append("</b></td><td align=\"center\" width=\"20%\">")
            .append(summaryPICounts.get(0))
            .append("</td><td width=\"50%\"></td></tr>");
            if (summaryPICounts.size() < 2) { continue; }
            for (int iY=1; iY<summaryPICounts.size(); iY++) {
                sb.append("<tr class=\"browserOddRow\" ><td align=\"right\" width=\"30%\"><i>")
                .append(summaryPIs.get(iY))
                .append("</i></td><td align=\"center\" width=\"20%\">")
                .append(summaryPICounts.get(iY))
                .append("</td><td>").append(buildBarChart(iY, summaryPICounts,
                        ((Integer)summaryPICounts.get(0)).intValue()))
                .append("</td></tr>");
            }
        } else {
            sb.append("<tr class=\"browserEvenRow\" align=\"left\"><td width=\"30%\"><b>")
            .append(settings.getObjDispTxt()).append("</b></td><td align=\"center\" width=\"20%\"></td>")
            .append("<td width=\"50%\"></td></tr>");
        }
    }
    return sb.toString();
}
String buildSubmissionStatusTable(String codelstSubtype, String dispText, EIRBDao eIrbDao) {
    ArrayList subtotalSummarySubmissionStatusSubtypes = eIrbDao.getSubtotalSummarySubmissionStatusSubtypes();
    ArrayList subtotalSummarySubmissionStatusCounts = eIrbDao.getSubtotalSummarySubmissionStatusCounts();
    int arrayIndex = -1;
    StringBuffer sb = new StringBuffer();
    if (subtotalSummarySubmissionStatusSubtypes != null) {
        for (int iY=0; iY<subtotalSummarySubmissionStatusSubtypes.size(); iY++) {
            if (codelstSubtype.equals(subtotalSummarySubmissionStatusSubtypes.get(iY))) {
                arrayIndex = iY;
                break;
            }
        }
    }
    if (arrayIndex < 0) {
        sb.append("<tr class=\"browserEvenRow\" align=\"left\"><td width=\"30%\"><b>")
        .append(dispText).append("</b></td><td align=\"center\" width=\"20%\">")
        .append("0</td><td width=\"50%\"></td></tr>");
        return sb.toString();
    }
    sb.append("<tr class=\"browserEvenRow\" align=\"left\"><td width=\"30%\"><b>")
    .append(dispText).append("</b></td><td align=\"center\" width=\"20%\">")
    .append(subtotalSummarySubmissionStatusCounts.get(arrayIndex))
    .append("</td><td width=\"50%\"></td></tr>");
    ArrayList[] ssCounts = eIrbDao.getSummarySubmissionStatusCounts();
    ArrayList[] ssNames  = eIrbDao.getSummarySubmissionStatusAssignees();
    for (int iY=0; iY<ssCounts[arrayIndex].size(); iY++) {
        sb.append("<tr class=\"browserOddRow\" ><td align=\"right\" width=\"30%\"><i>")
        .append(ssNames[arrayIndex].get(iY) == null ? LC.L_Not_Assigned/*"Not Assigned"*****/ : ssNames[arrayIndex].get(iY))
        .append("</i></td><td align=\"center\" width=\"20%\">")
        .append(ssCounts[arrayIndex].get(iY))
        .append("</td><td>").append(buildBarChart(iY, ssCounts[arrayIndex], 
                ((Integer)subtotalSummarySubmissionStatusCounts.get(arrayIndex)).intValue()))
        .append("</td></tr>");
    }
    return sb.toString();
}
%>
<%
ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = null;
String titleStr = "";
if (sessionmaint.isValidSession(request.getSession(true)))
{
    String accId = (String) request.getSession(true).getValue("accountId");
    
    ArrayList tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "top_menu");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("irb_menu".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
                titleStr += settings.getObjDispTxt();
            }
            break;
        } 
    }
    tempList = null;

    tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "irb_menu");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("manage_menu".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
                titleStr += " >> " + settings.getObjDispTxt() +
                ("Manage Submission".equals(settings.getObjDispTxt()) ? "s" : "");
            }
            break;
        } 
    }
    tempList = null;

    tabList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "irb_subm_tab");
    for (int iX=0; iX<tabList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tabList.get(iX);
        if (settings.getObjSubType().equals(request.getParameter("selectedTab"))) {
        	Object[] arguments = {titleStr,settings.getObjDispTxt()};
            titleStr = VelosResourceBundle.getLabelString("L_Gtgt",arguments);
            /*titleStr += " >> "+ settings.getObjDispTxt();*****/
            break;
        }
    }
}
%>
<title><%=titleStr %></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<% 
String src= request.getParameter("srcmenu");
HttpSession tSession = request.getSession(true); 
String accountId = "";
String studyId = "";
if (sessionmaint.isValidSession(tSession))
{
    accountId = (String) tSession.getValue("accountId");
    studyId = (String) tSession.getValue("studyId");
    if(accountId == null || accountId == "") {
    %>
		<jsp:include page="timeout.html" flush="true"/>
	<%
        return;
    }
} else {
    %>
		<jsp:include page="timeout.html" flush="true"/>
    <%
    return;
}
%>

<jsp:include page="include.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<body>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<%
	String from = "version";
	String tab = request.getParameter("selectedTab");

	  boolean showThisTab = false;
  int submissionBrowserRight = 0;

	int subSumRight= 0;
	int subNewGroupRight=  0;
	int subComplGroupRight=  0;
	int subAssignedGroupRight  = 0;
	int subPRGroupRight  = 0;
	int subPIGroupRight  = 0;
	
	int subPendingReviewsRight = 0;
    int subFinalOutcomeRight = 0;	     	   	   	
    
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	subSumRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SSUM"));
	subNewGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SNS"));
	subComplGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SCS"));
    subAssignedGroupRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SAS"));
    subPRGroupRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SPRS"));
    subPIGroupRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SPI"));  

	subPendingReviewsRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_PREV"));  
   subFinalOutcomeRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_OUT"));  
	
  if ("irb_summary_tab".equals(tab) )
		{
			if (subSumRight >=6 || subSumRight == 4)
			{
				showThisTab = true;
				submissionBrowserRight = subSumRight;
			}
			else 	{
				tab="irb_new_tab";
			}	
		}
		
	if ("irb_new_tab".equals(tab))
		{
			if (subNewGroupRight == 4 || subNewGroupRight >= 6)
			{
				showThisTab = true;
				submissionBrowserRight = subNewGroupRight;
			}
			else
			{
				tab="irb_assigned_tab";
			}
		}
		
  	if ("irb_assigned_tab".equals(tab) )
		{
			if (subAssignedGroupRight >=6 || subAssignedGroupRight == 4)
			{
				showThisTab = true;
				submissionBrowserRight = subAssignedGroupRight;
			}else
			{
				tab="irb_compl_tab";
			}
		 	
		}
	
  	if ("irb_compl_tab".equals(tab) )
		{
			if (subComplGroupRight >=6 || subComplGroupRight == 4)
			{
				showThisTab = true;
				submissionBrowserRight = subComplGroupRight;
			}else
			{
				tab="irb_pend_rev";
			}
		 	
		}
	
		if ("irb_pend_rev".equals(tab) )
		{
			if (subPendingReviewsRight >=6 || subPendingReviewsRight == 4) //MIGHT NEED TO CHANGE IT
			{
				showThisTab = true;
				submissionBrowserRight = subPendingReviewsRight;
			}else
			{
				tab="irb_post_tab";
			} 
		 	
		}
	
	
	if ("irb_post_tab".equals(tab) )
		{
			if (subPRGroupRight >=6 || subPRGroupRight == 4)
			{
				showThisTab = true;
				submissionBrowserRight = subPRGroupRight;
			} 
			else
			{
				tab = "irb_pend_tab";
			}
		 	
		}		
	
  	if ("irb_pend_tab".equals(tab) )
		{
			if (subPIGroupRight >=6 || subPIGroupRight == 4)
			{
				showThisTab = true;
				submissionBrowserRight = subPIGroupRight;
			} 
		 	
		}		

	if ("irb_appr_tab".equals(tab) )
		{
			if (subFinalOutcomeRight >=6 || subFinalOutcomeRight == 4)
			{
				showThisTab = true;
				submissionBrowserRight = subFinalOutcomeRight;
			} 
		 	
		}		
			
	if (!showThisTab )
	{
		tab="";
	}
	
String studyIdForTabs = ""; 
studyIdForTabs = request.getParameter("studyId");

String includeTabsJsp = "irbsubmtabs.jsp";
%>
<DIV class="BrowserTopn" id = "divtab">
<jsp:include page="<%=includeTabsJsp%>" flush="true">
				<jsp:param name="from" value="<%=from%>"/> 
				<jsp:param name="selectedTab" value="<%=tab%>"/>
				<jsp:param name="studyId" value="<%=studyIdForTabs%>"/>
</jsp:include>
</DIV>

<%

%>

<DIV class="BrowserBotN BrowserBotN_RC_1" id = "div1">
  <%@ page language = "java" import = "com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
  <%
  if (sessionmaint.isValidSession(tSession))
  {
    String userId = (String) tSession.getValue("userId");
    String uName = (String) tSession.getValue("userName");
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");
    String tableHeader = null;
    String tableContent = null;
    if ("irb_summary_tab".equals(tab)) {
        tableHeader = "<td align=\"center\"><P class=\"defComments\"><b>"+LC.L_Activity_Summary/*Activity Summary*****/+"</b></P></td>";
        tableContent = buildSummaryTable(tabList, accountId, defUserGroup, userId);
    } else {
        tableHeader = "";
        tableContent = "";
    }
  %>
  
    
    <%
    	
    if (showThisTab )
    {
    	if ("irb_summary_tab".equals(tab)) {
    
    	%>
    	<table width="100%" cellspacing="0" cellpadding="0" border="0" class="midAlign">
    	<tr height="5"><td></td></tr>
      <tr><%=tableHeader%></tr>
      <tr ><td align="center" width="99%" ><br /></td><td></td></tr>
    </table>
      
      
    <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midalign">
      <%=tableContent%>
    </table>
    	<% } else {%>
			<jsp:include page="submissionBrowser.jsp" flush="true">
						<jsp:param name="tabsubtype" value="<%=tab%>"/>
						<jsp:param name="submissionBrowserRight" value="<%=submissionBrowserRight%>"/> 
			</jsp:include>
    	
    	
    	<% } 
    		
    } //show this tab
    	else
    	{
    	%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%
  
    	}		%>
    		
    		
    <br />

  <%
  }//end of if body for session
  else
  {
  %>
    <jsp:include page="timeout.html" flush="true"/>
  <%
  }
  %>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
</body>

</html>

