<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>

<title><%=LC.L_Crf_NotificationDets%><%--CRF Notification Details*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.esch.business.common.CrfDao" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


</head>
	
<SCRIPT>
  function setParam(str,formobj){
	        
			formobj.subVal.value=str;
		if (!(validate(formobj))) {
			 return false  }
//		 setVals(formobj.subVal.value,formobj);	
		formobj.submit();
	}
	
	function setVals(str,formobj){

		formobj.action = formobj.action + "?addFlag=" + str; 

		return true;	
	}

function openwin1(formobj) {
	  var names = formobj.crfNotifyToName.value;
	  var ids = formobj.crfNotifyToId.value;
	  var windowToOpen = "multipleusersearchdetails.jsp?fname=&lname=&from=crfnotify&mode=initial&ids=" + ids + "&names=" + names ;
      window.open(windowToOpen,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200");
	  }
	
function  validate(formobj){
 	if (!(validate_col('Notify To',formobj.crfNotifyToName))){ 
 	   formobj.subVal.value="Submit";     	  
 	  return false }
	if (!(validate_col('CRF Status',formobj.crfStat))){
		 formobj.subVal.value="Submit";     	  
		 return false
		 }
 	if (!(validate_col('e-Signature',formobj.eSign))){ 
 		formobj.subVal.value="Submit";     	  
 		return false 
 	}

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		formobj.subVal.value="Submit";     	  
		return false;
   }
    return setVals(formobj.subVal.value,formobj);	
	return true
}


</SCRIPT>
	

<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.SchCodeDao,com.velos.eres.business.common.UserDao,com.velos.eres.web.studyRights.StudyRightsJB"%>


<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="crfNotifyB" scope="request" class="com.velos.esch.web.crfNotify.CrfNotifyJB"/>
<jsp:useBean id="crfB" scope="request" class="com.velos.esch.web.crf.CrfJB"/>
<jsp:useBean id="eventAssocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
		
<% 
	String src;
	src= request.getParameter("srcmenu");
	String selectedTab = request.getParameter("selectedTab");
	String from =  request.getParameter("from");
	String globalFlag = request.getParameter("globalFlag");
	int pageRight = 0;
	

%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<body>
<br>
<DIV class="browserDefault"  id="div1">

<%
   HttpSession tSession = request.getSession(true); 
   if (sessionmaint.isValidSession(tSession))
	{
	String studyId = request.getParameter("studyId");	
	String protocolId = request.getParameter("protocolId");	


	SchCodeDao cd1 = new SchCodeDao();
	cd1.getCodeValues("crfstatus");	
	Calendar cal1 = new GregorianCalendar();
	String mode = request.getParameter("mode");
	
	String uName = (String) tSession.getValue("userName");	
		
	String crfNotifyId = "";

/*	String tempCrfId ="";	
	String dCrfNotifyStat = ""; */
	
	String crfNotifyToId="";
	String crfNotifyCrfId ="";
	String crfNotifyStatus="";
	String crfNotifyToName ="";
	String dCrfNotifyStat="";



	
	

	String crfDD = "";
 
	
	String protName = "";

	if(protocolId != null) {
		eventAssocB.setEvent_id(EJBUtil.stringToNum(protocolId));
		eventAssocB.getEventAssocDetails();
		protName = 	eventAssocB.getName();
	} else {
		protocolId = "";
	}
	
	
%>
	<jsp:include page="studytabs.jsp" flush="true"> 
		<jsp:param name="from" value="<%=from%>"/> 
		<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
		<jsp:param name="studyId" value="<%=studyId%>"/>
	</jsp:include>
	<%
	stdRights =(StudyRightsJB) tSession.getValue("studyRights");	 

	if ((stdRights.getFtrRights().size()) == 0){
	 	pageRight= 0;
		}else{
	pageRight = 	Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
	}
	if (pageRight >= 4 )
	  {
	 %> 	  

<table width=100%>
 <tr>
 	<td  width="20%" class=tdDefault><B><%=LC.L_Protocol_Calendar%><%--Protocol Calendar*****--%>:</B></td><td width="80%"  class=tdDefault><B><%=protName%></B></td>
 </tr>
</table>



<%
	if(mode.equals("M")){
		crfNotifyId = request.getParameter("crfNotifyId");
		crfNotifyB.setCrfNotifyId(EJBUtil.stringToNum(crfNotifyId));
		crfNotifyB.getCrfNotifyDetails();		
		crfNotifyToId = crfNotifyB.getCrfNotifyUsersTo();
		crfNotifyStatus = crfNotifyB.getCrfNotifyCodelstNotStatId();
		crfNotifyCrfId = crfNotifyB.getCrfNotifyCrfId();
		
		UserDao userDao = userB.getUsersDetails(crfNotifyToId);	
		ArrayList fname = userDao.getUsrFirstNames();
		ArrayList lname = userDao.getUsrLastNames();
		for(int i=0;i<fname.size();i++){

			crfNotifyToName = crfNotifyToName + ((String)lname.get(i)).trim() + "," + ((String)fname.get(i)).trim() + ";";
		}
		crfNotifyToName = crfNotifyToName.substring(0,crfNotifyToName.length()-1);
		
		dCrfNotifyStat = cd1.toPullDown("crfStat",EJBUtil.stringToNum(crfNotifyStatus));
		
		crfDD = "<select name='crfId'> <option value=0 >"+LC.L_All/*All*****/+"</option>";		
		crfDD = crfDD + "</select>"; 
	
	} else {
		 dCrfNotifyStat = cd1.toPullDown("crfStat"); 
		crfDD = "<select name='crfId'> <option value=0 selected>"+LC.L_All/*All*****/+"</option>";		
		crfDD = crfDD + "</select>";
	}
%>

<Form name="crfNotify" method=post action="updatstudycrfnot.jsp" onsubmit="return validate(document.crfNotify)">
	<p class = "sectionHeadings" ><%=LC.L_Crf_Notification%><%--CRF Notification*****--%></p>
			<table><tr>
		<td class=tdDefault><B><%=LC.L_For_AllCrfs%><%--For All Crfs*****--%></B></td>
		</tr>		</table>



	<table width=100%>
		<tr width=100%>
			<td width=35%>
				<%=LC.L_Send_MsgTo%><%--Send Message To*****--%> <FONT class="Mandatory">* </FONT>
			</td>
			<td width=65%>
				<input type=text name=crfNotifyToName maxlength=100 size=20 readonly value = '<%=crfNotifyToName%>' > 
				<A HREF=# onClick='openwin1(document.crfNotify)' ><%=LC.L_SelOrDeSel_Usrs%><%--Select/Deselect User(s)*****--%></A>
			</td> 
		</tr>
		<input type=hidden name=crfNotifyToId	value='<%=crfNotifyToId%>' >
		
		<input type=hidden name=globalFlag value=<%=globalFlag%>>

	


		<tr>
			<td>
				<%=MC.M_WhenCRFStatus_ChgTo%><%--When CRF Status Changed To*****--%><FONT class="Mandatory">* </FONT>
			</td>
			<td>
				<%=dCrfNotifyStat%>
			</td> 
		</tr>


</table>		

<%	   
	if ((mode.equals("M") && pageRight >=6) || (mode.equals("N") && (pageRight == 5 || pageRight == 7 ))) 
		{%>

<table width=100% >
	<tr>


	<td class=tdDefault>
	<%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT>
	</td >
	<td class=tdDefault>
		<input type="password" name="eSign" maxlength="8">	
		<input type="image" src="../images/jpg/Submit.gif" align="absmiddle"  border="0" onClick="return setVals('Submit',document.crfNotify);">	
        <button onClick="return setParam('Add',document.crfNotify);"><%=LC.L_Submit_AddAnother%></button>			
			
	</td>		
	</tr>
</table>
<%
}
%>		
		
<input type=hidden name=crfNotifyId value=<%=crfNotifyId%>>
<input type=hidden name=mode value=<%=mode%>>
<input type=hidden name=studyId value='<%=studyId%>'>
<input type=hidden name=protocolId value='<%=protocolId%>'>
<input type="hidden" name="selectedTab" value="<%=selectedTab%>">
<input type="hidden" name="from" value="<%=from%>">
<input type="hidden" name="srcmenu" value="<%=src%>">
		
<% //column added for netscape 4.79 support%>
    <input type="hidden" name="subVal" value="Sumbit">
</Form>	
<%
	}//end of if page right
}//end of if body for session
else
{
%>
<jsp:include page="timeout.html" flush="true"/> 
<%
}
%>
<div>
<jsp:include page="bottompanel.jsp" flush="true"/> 

</div>
</div>
<DIV class="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>   
</DIV>
</body>
</html>

