<!--

	Project Name:	Velos eResearch

	Author:	Jnanamay Majumdar

	Created on Date:	05Sept2007

	Purpose:	Update page for the Storage Status, GUI

	File Name:	updatestoragestatus.jsp

-->
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>

  <jsp:useBean id="storageB" scope="page" class="com.velos.eres.web.storage.StorageJB"/> 
  <jsp:useBean id="codeListJB" scope="page" class="com.velos.eres.web.codelst.CodelstJB"/>
  <jsp:useBean id="StorageStatJB" scope="request" class="com.velos.eres.web.storageStatus.StorageStatusJB"/>
   <jsp:useBean id="StorageStatusJB" scope="request" class="com.velos.eres.web.storageStatus.StorageStatusJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.*,com.velos.eres.service.util.*, java.text.SimpleDateFormat, com.velos.eres.web.storageStatus.StorageStatusJB"%>
  
  <%
    
	String src = null;
	src = request.getParameter("srcmenu");
	String eSign = request.getParameter("eSign");	
	String mode = request.getParameter("mode");

	
	HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))

   {
     %>
     <jsp:include page="sessionlogging.jsp" flush="true"/> 

     <%   
   
   	String oldESign = (String) tSession.getValue("eSign");
	
	if(!oldESign.equals(eSign)) 
	{
%>
  	   <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} 
	else 
  	{
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");
		
		

		int ret = -1;		
		int sret = -1;


		String fkStorage = request.getParameter("pkstorage");
		fkStorage = (fkStorage==null)?"":fkStorage;

		String pkStorStat = request.getParameter("pkStorageStat");
		pkStorStat = (pkStorStat==null)?"":pkStorStat;		
			
		//Storage status..when it is disabled
		String storStatDis = request.getParameter("storStatDisabled");
		storStatDis = (storStatDis==null)?"":storStatDis;			

		String storStat = request.getParameter("storageStatus"); 
		storStat = (storStat==null)?"":storStat;		

		String storDate = request.getParameter("statDate");
		storDate = (storDate==null)?"":storDate;		
		
		String storStudy = request.getParameter("selStudyIds");
		storStudy = (storStudy==null)?"":storStudy;

		String storUser = request.getParameter("creatorId");
		storUser = (storUser==null)?"":storUser;

		String storNotes = request.getParameter("statusNotes");
		storNotes = (storNotes==null)?"":storNotes;
		
		//JM: 28Dec2007----------------------
		//custom column for new mode alone...
		
		String customCol = "";
				
		if(mode.equals("N")) {
		customCol = codeListJB.getCodeCustomCol(EJBUtil.stringToNum(storStat));
		customCol = (customCol==null)?"":customCol;		
		}

		
		
		if (customCol.equals("C")){				
		
		String numStorageChilds = "";
		numStorageChilds = storageB.findAllChildStorageUnitIds(fkStorage);				
		String childs[] = numStorageChilds.split(",");	
		
		
		for (int k=0; k<childs.length; k++){	
			
			StorageStatusJB strStatB = new StorageStatusJB();
			strStatB.setFkStorage(childs[k]);		
					
			strStatB.setSsStartDate(storDate);
			strStatB.setFkCodelstStorageStat(storStat);
			strStatB.setSsNotes(storNotes);
			strStatB.setFkUser(storUser);			
			strStatB.setFkStudy(storStudy);
			strStatB.setIpAdd(ipAdd);
			strStatB.setCreator(usr);
			sret=strStatB.setStorageStatusDetails();		
		}	
		
		}//end of if, customCol
		
		//JM: 28Dec2007----------------------
		
		
		
		
		

	
		if (mode.equals("M")){
			
 			StorageStatJB.setPkStorageStat(EJBUtil.stringToNum(pkStorStat));
 			StorageStatJB.getStorageStatusDetails();
 
     	}
			StorageStatJB.setFkStorage(fkStorage);
			StorageStatJB.setSsStartDate(storDate);		
					
			//JM: 18Sep2007: added to block the storage status in modify mode, #3157
			if (mode.equals("N")){
			StorageStatJB.setFkCodelstStorageStat(storStat);
			}
			
						
			
			//add status notes
			StorageStatJB.setSsNotes(storNotes);

			StorageStatJB.setFkUser(storUser);			
			StorageStatJB.setFkStudy(storStudy); 
			StorageStatJB.setIpAdd(ipAdd); 
			
		if (mode.equals("M")){		
 			StorageStatJB.setModifiedBy(usr); 			           
			ret=StorageStatJB.updateStorageStatus();
		}	
		else{
			StorageStatJB.setCreator(usr);		
			ret=StorageStatJB.setStorageStatusDetails();				
		}		
		
		
		
		
	if ( ret >=0){
	%>
	  <br>
      <br>
      <br>
      <br>
      <br>
     <p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
	  <script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
	  </script>	  

<% }

}//end of if for eSign check

}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





