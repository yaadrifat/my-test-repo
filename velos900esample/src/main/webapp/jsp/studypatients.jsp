<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
 <html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<title><%=MC.M_MngPat_StdPat%><%--Manage <%=LC.Pat_Patients%> >> <%=LC.Std_Study%> <%=LC.Pat_Patients%>*****--%></title>
<jsp:include page="panel.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"/>
<SCRIPT Language="javascript">
window.name="studypatients";
var orgObject;

enrPat=function(formobj)
{
 studyVal=$('dStudy').value;
 //Modified For Bug# 6809 :Yogendra
 if(studyVal.length==0){
	 alert("<%=MC.M_OptNotAval_WhenStdNotAvailable%>");
	 return false;
 }
 if (studyVal.indexOf(",")>=0)
 {
 alert("<%=MC.M_OptNotAval_WhenAllStdSel%>");/*alert("This option is not available when 'ALL' <%=LC.Std_Studies%> is selected");******/
 return false;
 }
var link="enrollpatientsearch.jsp?from=studypatients&selectedTab="+$('selectedTab').value+"&searchFrom=initial&srcmenu="+$('srcmenu').value+"&studyId="+$('dStudy').value+"&dPatSite="+$('dPatSite').value;
reDirectUrl =link;
setTimeout( "window.location.href = reDirectUrl", 0 );


}

function changeStudy(formobj){



	if ((formobj.dPatSite.value == "All" || formobj.dPatSite.value == "" || formobj.dPatSite.value == "0") && (formobj.dStudy.value == "" || formobj.dStudy.value == "0"))
	{
		alert("<%=MC.M_CntSelOrg_WhileStd%>");/*alert("You cannot select 'All' Organizations while selecting 'All' <%=LC.Std_Studies%>");*****/
		window.location.reload(false);



	}
	else
	{
	formobj.action="studypatients.jsp?srcmenu="+formobj.srcmenu.value+"&selectedTab="+formobj.selectedTab.value+"&openMode="+formobj.openMode.value;

	formobj.submitMode.value="S";//search
	formobj.currentpage.value= 1;
	formobj.orderByColumnCode.value = "";
	formobj.orderType.value = "";

	formobj.submit();
	}

}
var stdId="";
function checkForAll()
{
	var patSite=$('dPatSite'),dStudy=$('dStudy') ;
	stdId = dStudy.value;//YPS:6743:14 March 2012
	var ip=patSite.selectedIndex,is=dStudy.selectedIndex;
	//FIX #6045
	$('dStudyPermCloseDate').value = dStudy.value;
	$('dStudyPermCloseDate').selectedIndex = is;
	var idate=$('dStudyPermCloseDate').selectedIndex;
	//alert($('dStudyPermCloseDate').options[idate].text + '*'+dStudy.value);
	
	var e = document.getElementById("enroll_links");
	if (e){
		if ($('dStudyPermCloseDate').options[idate].text != dStudy.value){
			//alert('Study Closed- Hide links');
			e.style.display = 'none';
		} else	{
			if (dStudy.options[is].text=='All')
			{
				 e.style.display = 'none';
			}
			else
			{
				e.style.display = 'block';
			}
		}
	}
	
	if (is<0) return false;

	if ((patSite.options[ip].text=='All') && (dStudy.options[is].text=='All'))
	{
		alert("<%=MC.M_CntSelOrg_WhileStd%>");/*alert("You cannot select 'All' Organizations while selecting 'All' <%=LC.Std_Studies%>");*****/
		return false;
        //window.location.reload(false);
	}

	return true;
}


//km
function confirmBox(pid,study){
	var paramArray = [pid,study];
    if (confirm(getLocalizedMessageString("M_WantDel_PatStd",paramArray)))/*if (confirm("Do you want to delete <%=LC.Pat_Patient%> "+pid+" from <%=LC.Std_Study%> "+ study+"?" ))*****/
   	return true;
    else
	return false;

}


 function back(pk,patId) {

	window.opener.document.reports.id.value = pk;
	window.opener.document.reports.selPatient.value = patId;
	window.opener.document.reports.val.value = patId;
	self.close();

}


	function  openReport(formobj,act,reportId,reportName,repPatient,repStudy,repFilter,repPatProt){

		formobj.repId.value = reportId;
		formobj.repName.value = reportName;
		formobj.studyPk.value = repStudy;
		formobj.id.value = repPatient;
		formobj.filterType.value = repFilter;
		formobj.protId.value = repPatProt;

		formobj.action="repRetrieve.jsp";
		formobj.target="_new";
		formobj.submit();
		}


	function  validate(formobj){

	 /*if (!(isInteger(formobj.filterLastVisit.value))){
	 		alert("Please enter a valid number");
			formobj.filterLastVisit.focus();
            return false;
		 }*/
		 if (formobj.cbxExcludeNotEnrolled.checked)
		 {
		 	formobj.excludeNotEnrolled.value = "1";
		 }
		 else
		 {
		 	formobj.excludeNotEnrolled.value = "0";
		 }
		}
function openEditDetails(patientProtId,patientId,studId){

	windowName=window.open("editMulEventDetails.jsp?patProtId="+patientProtId+"&pkey="+patientId+"&studyId="+studId+"&calledFrom=E","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=900,height=500,left=150");
	windowName.focus();
}


function openPatientWorkflow(){
	studyVal=$('dStudy').value;
	var studyVal;
	if (studyVal){
		studyName = encodeURIComponent($('dStudy').options[$('dStudy').selectedIndex].text);
	}
	windowName=window.open("patientSearchWorkflow.jsp?studyId="+$('dStudy').value + "&studyName=" + studyName,"PatientWorkflow","toolbar=no,scrollbars=no,resizable=yes,menubar=no,status=yes,width=620,height=660,left=150");
	windowName.focus();
}

//JM: 032106: Modified JS function signature: #2401
//function openStudyCentric(studId){
function openStudyCentric(){
	studyVal=$('dStudy').value;
	 if (studyVal.indexOf(",")>=0)
	 {
	 alert("<%=MC.M_OptNotAval_WhenAllStdSel%>");/*alert("This option is not available when 'ALL' <%=LC.Std_Studies%> is selected");*****/
	 return false;
	 }
	windowName=window.open("studycentricpatenroll.jsp?studyId="+$('dStudy').value,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=700,height=400,left=150");
	windowName.focus();
	}

function openExcelWin(formobj)
{
	param1=formobj.excelLink.value ;
    windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=yes,status=yes,width=675,height=600,top=60,left=200");
	windowName.focus();

}
function openWinStatus(patId,patStatPk,changeStatusMode)
{
	windowName= window.open("patstudystatus.jsp?changeStatusMode="+changeStatusMode+"&statid=" + patStatPk + "&pkey=" + patId,"patstatusdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=500");
	windowName.focus();

}

function setOrder(formObj,orderBy) //orderBy column name code
{
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
	}
	formObj.orderByColumnCode.value= orderBy;

	formObj.submitMode.value="P";//pagination
	formObj.submit();
}

function jumpToPage(formObj,pagenum)
{

	formObj.currentpage.value= pagenum;
	formObj.submitMode.value="P";//pagination

	formObj.submit();
}



function setfocus(){
   //km-to fix the Bug 2321
   if(document.forms[0].flag.value==1)
      document.forms[0].patientid.focus();
}

//Added by Gopu for May-June 2006 Enhancement (#P1)

function openOrgpopupView(patId,viewCompRight) {

	var viewCRight ;
	var rtStr;

	viewCRight = parseInt(viewCompRight);

	if (viewCRight <=0)
	{
		rtStr = "nx";
	}
	else
	{

		rtStr = "dx";
	}

    windowName = window.open("patOrgview.jsp?patId="+patId+"&c="+rtStr,"patient","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=300,height=150,left=400,top=400");
	windowName.focus();
}
function orgRefresh(filter)
{
	//Modified For Bug# 6743 | Date:28 Feb 2012 | By:Yogendra Pratap Singh
	studyVal=$('dStudy').value;
	if(studyVal.length==0){
		studyVal=stdId;
		var patOnStd=document.getElementById('dStudy');
		for(var i=0; i<patOnStd.options.length; i++){
			if(patOnStd.options[i].value==studyVal){
				patOnStd.options[i].selected=true;
		  		break;
		 	}
		}
	}
  l_fltr=filter||'';
  orgObject=new VELOS.ajaxObject("getStudyData.jsp",{
     urlData:"keyword=patenrorg&studyId="+studyVal+"&userId="+$('userId').value+"&accountId="+$('accountId').value,
     success:function(o){
     $('orgDD').innerHTML=o.responseText;
      paginate_study.rowsPerPage=0;
      if (checkForAll())
        paginate_study.runFilter(l_fltr);
     }
   });
 orgObject.startRequest();
}
pStdLink=function(elCell, oRecord, oColumn, oData){
var htmlStr="";
//var pcode=htmlEncode(oRecord.getData("PER_CODE"));
var pcode=escape(encodeString(oRecord.getData("PER_CODE")));
htmlStr="<A href =\"patientschedule.jsp?srcmenu="+$('srcmenu').value+"&selectedTab=3&mode=M&pkey="+oRecord.getData("FK_PER")+"&patProtId="+oRecord.getData("PK_PATPROT")+"&patientCode="+pcode+"&page=patientEnroll&studyId="+oRecord.getData("FK_STUDY")
          +"&generate=N&statid="+oRecord.getData("PK_PATSTUDYSTAT")+"\">" + oData +"</A>";
elCell.innerHTML=htmlStr;
}
orgColumn=function(elCell, oRecord, oColumn, oData){
 if (!(oData)) {
  elCell.innerHTML="(<%=LC.L_Pats_NotEnrl%><%--<%=LC.Pat_Patient_Lower%>(s) not enrolled*****--%>)";
  }
  else{
  elCell.innerHTML=oData;
  }
}
otherLink=function(elCell, oRecord, oColumn, oData)
{
 var htmlStr="<div align=\"center\" ><A HREF=\"javascript:void(0);\" onClick=\"openOrgpopupView('"+oData+"',"+oRecord.getData("RIGHT_MASK")+");\"><img title=\"<%=LC.L_View%>\" src=\"./images/View.gif\" border =\"0\"></A></div>";
 elCell.innerHTML=htmlStr;
}
statusLink=function(elCell, oRecord, oColumn, oData)
{
var reason=oRecord.getData("PATSTUDYSTAT_REASON");
if (!(reason)) reason="";
var note=oRecord.getData("PATSTUDYSTAT_NOTE");
if (!(note)) note="";
note=htmlEncode(note);
var per=oRecord.getData("FK_PER");
if (!per) per="";
var htmlStr='<table style="border-style:none" border="0"><tr><td style="border-style:none">';

if (per.length>0) {
var pcode=escape(encodeString(oRecord.getData("PER_CODE")));
htmlStr=htmlStr+"  <A Title=\"<%=LC.L_Change_Status%><%--Change Status*****--%>\" href =\"enrollpatient.jsp?srcmenu="+$('srcmenu').value+"&selectedTab=2&pkey="+oRecord.getData("FK_PER")+"&patProtId="+oRecord.getData("PK_PATPROT")+"&patientCode="+pcode+"&page=patientEnroll&studyId="+oRecord.getData("FK_STUDY")+"\"";
htmlStr=htmlStr+" onmouseover=\"return overlib('<tr><td><font size=2><b><%=LC.L_Reason%><%--Reason*****--%>: </b></font><font size=1>"+reason+"</font></td></tr><tr><td><font size=2><b><%=LC.L_Status_Date%><%--Status Date*****--%>: </b></font><font size=1>"+oRecord.getData("PATSTUDYSTAT_DATE_DATESORT")+
	"</font></td></tr><tr><td><font size=2><b><%=LC.L_Notes%><%--Notes*****--%>: </b></font><font size=1>"+note+"</font></td></tr>',CAPTION,'<%=LC.L_MostRct_Stat%><%--Most Recent Status*****--%>',RIGHT,ABOVE);\"  onmouseout=\"return nd();\"><img title=\"<%=LC.L_Edit%>\" src=\"./images/edit.gif\" border =\"0\"><%//=LC.L_C%><%--C*****--%></A>"
	}

htmlStr+='</td><td style="border-style:none">&nbsp;</td><td style="border-style:none">'+oData+'</td></tr></table>';

elCell.innerHTML=htmlStr;
}
//Ashu Added for 'Requiremnt_Spec_COMP_REQ_1' requirement 2feb11.
currStatusLink=function(elCell, oRecord, oColumn, oData)
{
var currStat=oRecord.getData("PATSTUDYCUR_STAT");
if (!(currStat)) currStat="";
var reason=oRecord.getData("PATSTUDYSTAT_REASON");
if (!(reason)) reason="";
var note=oRecord.getData("PATSTUDYSTAT_NOTE");
if (!(note)) note="";
note=htmlEncode(note);
var per=oRecord.getData("FK_PER");
if (!per) per="";
var htmlStr='<table style="border-style:none" border="0"><tr><td style="border-style:none">';

if (per.length>0 && currStat.length>0) {
var pcode=escape(encodeString(oRecord.getData("PER_CODE")));
htmlStr=htmlStr+"  <A Title=\"<%=LC.L_Change_Status%><%--Change Status*****--%>\" href =\"enrollpatient.jsp?srcmenu="+$('srcmenu').value+"&selectedTab=2&pkey="+oRecord.getData("FK_PER")+"&patProtId="+oRecord.getData("PK_PATPROT")+"&patientCode="+pcode+"&page=patientEnroll&studyId="+oRecord.getData("FK_STUDY")+"\"><img title=\"<%=LC.L_Edit%>\" src=\"./images/edit.gif\" border =\"0\"><%//=LC.L_C%><%--C*****--%></A>"
	}

htmlStr+='</td><td style="border-style:none">&nbsp;</td><td style="border-style:none">'+oData+'</td></tr></table>';

elCell.innerHTML=htmlStr||"";
}

editEvent=function(elCell, oRecord, oColumn, oData)
{
 var subType=oRecord.getData("PATSTUDYSTAT_SUBTYPE");
 var htmlStr="";
 if (subType)
 {
 if ((subType.toLowerCase())!="lockdown")
 {
 var htmlStr="<div align=\"center\" ><A href=\"javascript:void(0)\" onClick=\"openEditDetails('" +oData+"','"+oRecord.getData("FK_PER")+"','"+ oRecord.getData("FK_STUDY")+"')\"><img title=\"<%=LC.L_Edit%>\" src=\"./images/edit.gif\" border =\"0\"><%//=LC.L_Edit%><%--Edit*****--%></A></div>"
 }
 }
 elCell.innerHTML=htmlStr;
}

reportLink=function(elCell, oRecord, oColumn, oData)
{
var htmlStr="";
var per=oRecord.getData("FK_PER");
if (!per) per="";
if (per.length>0)
{
htmlStr="<A href=\"javascript:void(0);\" onClick =\"openReport(document.search,'repRetrieve.jsp','95','<%=LC.L_Pat_VisitCal%><%--<%=LC.Pat_Patient%> Visit Calendar*****--%>','"+ per+"','"+oRecord.getData("FK_STUDY")+"','4',"+oRecord.getData("PK_PATPROT")+")\" title=\"<%=MC.M_PatVisit_CalRpt%><%--<%=LC.Pat_Patient%> Visit Calendar Report*****--%>\"><img src=\"../images/jpg/Calendar.gif\" title=\"<%=LC.L_Calendars%>\"/></A>";
htmlStr=htmlStr+"  <A href=\"javascript:void(0);\" onClick =\"openReport(document.search,'repRetrieve.jsp','88','<%=MC.M_AdvEvt_Pat%><%--Adverse Events by <%=LC.Pat_Patient%>*****--%>','"+ per+"','"+oRecord.getData("FK_STUDY")+"','1',"+oRecord.getData("PK_PATPROT")+")\" title=\"<%=LC.L_AdvEvt_Rpt%><%--Adverse Events Report*****--%>\"><img src=\"./images/UnexpectedEvent.gif\" title=\"<%=LC.L_Adverse_Event%>\"/></A>";
htmlStr=htmlStr+"  <A href=\"javascript:void(0);\" onClick =\"openReport(document.search,'repRetrieve.jsp','66','<%=LC.L_PatEvt_CrfStat%><%--<%=LC.Pat_Patient%> Event/CRF Status*****--%>','"+ per+"','"+oRecord.getData("FK_STUDY")+"','1',"+oRecord.getData("PK_PATPROT")+")\" title=\"<%=LC.L_PatEvt_CrfStatRpt%><%--<%=LC.Pat_Patient%> Event/CRF Status Report*****--%>\"><%=LC.L_Crf%><%--CRF*****--%></A>";
htmlStr=htmlStr+"  <A href=\"javascript:void(0);\" onClick =\"openReport(document.search,'repRetrieve.jsp','93','<%=LC.L_Pat_Timeline%><%--<%=LC.Pat_Patient%> Timeline*****--%>','"+ per+"','"+oRecord.getData("FK_STUDY")+"','1',"+oRecord.getData("PK_PATPROT")+")\" title=\"<%=LC.L_PatTimeline_Rpt%><%--<%=LC.Pat_Patient%> Timeline Report*****--%>\"><%=LC.L_Tl%><%--TL*****--%></A>";
}
elCell.innerHTML=htmlStr;
}

delLink=function(elCell, oRecord, oColumn, oData)
{
var htmlStr="";
var per=oRecord.getData("FK_PER");
if (!per) per="";
var gn=$('groupName').value;
if ((per.length>0) && (gn.toLowerCase()=='admin'))
{
var pcode=escape(encodeString(oRecord.getData("PER_CODE")));
htmlStr="<div align=\"center\" ><A title=\"<%=LC.L_Delete%><%--Delete*****--%>\" href=\"deletepatstudy.jsp?patProtId="+oRecord.getData("PK_PATPROT")+"&srcmenu="+$('srcmenu').value+"&patientId="+per+"&patientCode="+pcode+"&studyNo="+escape(oRecord.getData("STUDY_NUMBER"))+
"&studyId="+oRecord.getData("FK_STUDY")+"&delId=studypat\" onclick=\"return confirmBox('"+oRecord.getData("PER_CODE")+"','"+oRecord.getData("STUDY_NUMBER")+"');\">"+
"<img src=\"./images/delete.gif\" border=\"0\" /></A></div>";
//htmlStr="<A title=\"Delete\" href=\"deletepatstudy.jsp?patProtId="+oRecord.getData("PK_PATPROT")+"&srcmenu="+$('srcMenu').value+"&patientId="+per+"&patientCode="+oRecord.getData("PER_CODE")+"&studyNo="+oRecord.getData("STUDY_NUMBER")+"&studyId="+oRecord.getData("FK_STUDY")+"&delId=studypat\"" onClick=\"return confirmBox('"+oRecord.getData("PER_CODE")+"','"+oRecord.getData("STUDY_NUMBER")+"')\"><img src=\"./images/delete.gif\" border=\"0\" align=\"left\"/></A>";

}
elCell.innerHTML=htmlStr;
}

perIdLink=function(elCell, oRecord, oColumn, oData)
{
 var htmlStr="";

 var right =parseInt(oRecord.getData("RIGHT_MASK"));
 if (right>=4)
 {
  var patHPhone="",patBPhone="";
  var pcode=escape(encodeString(oData));
  var url="patientdetails.jsp?srcmenu="+$('srcmenu').value+"&selectedTab=1&mode=M&pkey="+oRecord.getData("FK_PER")+"&patProtId="+oRecord.getData("PK_PATPROT")+"&patientCode="+pcode+"&page=patientEnroll&studyId="+oRecord.getData("FK_STUDY")+"&generate=N&statid="+oRecord.getData("PK_PATSTUDYSTAT")+"&studyVer=null";
  //Parse the phone numbers
  var phoneStr=oRecord.getData("MASK_PERSON_PHONE");
  var phoneArray=phoneStr.split("[VELSEP]");
  if (phoneArray)
   {
	  if (phoneArray[1]) patHPhone=phoneArray[1];
	  if (phoneArray[2]) patBPhone=phoneArray[2];
 }
 //Parse the Address
 var patAddress1="",patAddress2="",patCity="",patState="",patZip="",patCountry="";
 var personNotes=oRecord.getData("PERSONNOTES");
  if (!personNotes) personNotes="";

  personNotes=htmlEncode(personNotes);

 var addStr=oRecord.getData("MASK_PATADDRESS");
 var addArray=addStr.split("[VELSEP]");
  if (addArray)
  {
   if (addArray[1]) patAddress1=addArray[1]||"";
   if (addArray[2]) patAddress2=addArray[2]||"";
   if (addArray[3]) patCity=addArray[3]||"";
   if (addArray[4]) patState=addArray[4]||"";
   if (addArray[5]) patZip=addArray[5]||"";
   if (addArray[6]) patCountry=addArray[6]||"";

  }
    var plname=oRecord.getData("MASK_PERSON_LNAME");
    if (!plname) plname="";

    var pfname=oRecord.getData("MASK_PERSON_FNAME");
    if (!pfname) pfname="";


	var patientName=plname+","+pfname;
	var paramArray = [patientName];
  htmlStr="<table style=\"border-style:none\"><tr style=\"border-style:none\"><td style=\"border-style:none\"><A href="+url+" onmouseover=\"return overlib('<tr><td><font size=2><b><%=LC.L_Home_Phone%><%--Home Phone*****--%> : </b></font><font size=1>"+patHPhone+"</font></td></tr><tr><td><font size=2><b><%=LC.L_Work_Phone%><%--Work Phone*****--%>: </b></font><font size=1>"+patBPhone+
	"</font></td></tr><tr><td><font size=2><b><%=LC.L_Address%><%--Address*****--%>: </b></font></td></tr><tr><td><font size=1>"+patAddress1+"</font></td></tr><tr><td><font size=1>"+patAddress2+"</font></td></tr><tr><td><font size=1>"+patCity+"&nbsp;<font size=1>"+patState+
	"</font>&nbsp;<font size=1>"+patZip+"</font></td></tr><tr><td><font size=1>"+patCountry+
	"</font></td></tr><tr><td><font size=2><b><%=LC.L_Notes%><%--Notes*****--%>: </b></font><font size=1>"+personNotes+"</font></td></tr>',CAPTION,'"+getLocalizedMessageString('L_Pat_Hyp',paramArray)<%--<%=LC.Pat_Patient%> - "+patientName+"*****--%>+"',RIGHT,ABOVE);\" onmouseout=\"return nd();\" ><img src=\"./images/View.gif\" border=\"0\" align=\"left\"/></A>";
 htmlStr=htmlStr+ "</td> <td style=\"border-style:none\">"+ oData + "</td></tr></table>";
 } else
 {
  htmlStr=oData;
 }
 elCell.innerHTML=htmlStr;
}

var paginate_study;

<% if (sessionmaint.isValidSession(request.getSession(false))) { %>
$E.addListener(window, "load", function() {
  orgRefresh();
  paginate_study=new VELOS.Paginator('studyPatient',{
 				sortDir:"asc",
				sortKey:"SITE_NAME",
				defaultParam:"userId,accountId,grpId",
				filterParam:"dStudy,dPatSite,cbxExcludeNotEnrolled,pstudyid,filterLastVisit,patientid,filterNextVisit,filterEnrDate,patientstatus",
				dataTable:'serverpagination',
				refreshMethod:'orgRefresh()',
				rowSelection:[5,10,25,50]/*,
				rowsPerPage:5,
				navigation: false,
				searchEnable:false*/

				});
				//paginate_study.render();

	/*var paginate_pat=new VELOS.Paginator('allPatient',{
 				sortDir:"asc",
				sortKey:"PERSON_CODE",
				defaultParam:"userId,accountId,grpId",
				filterParam:"patCode,patage,siteId,patName,gender,regby,pstat,studyId,speciality",
				dataTable:'serverpagination_pat'
				});
				paginate_pat.render();*/

				/*paginate_study.runFilter=function (filter)
 				{
 					alert("local");
					if (checkForAll()) {orgRefresh()};
							var l_filter;
					 		if (filter && filter!='reset')
					 	{l_filter=filter }
					  else {
					  l_filter=this.buildCriteria();
					}

					this.setExtParam(l_filter);
					this.startIndex=0;
					this.render();
					return false;

					} */



				 }

 ) ;
<% } %>

</SCRIPT>




<jsp:useBean id ="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km-->
<jsp:useBean id="studyStatB" scope="page" class="com.velos.eres.web.studyStatus.StudyStatusJB" />
<jsp:useBean id="CommonB" scope="page" class="com.velos.eres.web.common.CommonJB" />


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.person.*,com.velos.eres.service.util.*,com.velos.eres.web.user.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<jsp:useBean id="person" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id ="reportIO" scope="session" class="com.velos.eres.service.util.ReportIO"/>


<% String src;



src= request.getParameter("srcmenu");

String openMode=request.getParameter("openMode"); //open this window as popup or full screen

%>






<body onload="setfocus()" class="yui-skin-sam">
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>



<%

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

{

  String defUserGroup = (String) tSession.getAttribute("defUserGroup");

	int pageRight = 0;
	String superUserRights = "";

	//Added by Manimaran to give access right for default admin group to the delete link
	String userId = (String) tSession.getValue("userId");
    int usrId = EJBUtil.stringToNum(userId);

    userB = (UserJB) tSession.getValue("currentUser");
	String accId = (String) tSession.getValue("accountId");


	String defGroup = userB.getUserGrpDefault();

	groupB.setGroupId(EJBUtil.stringToNum(defGroup));
	groupB.getGroupDetails();
	String groupName = groupB.getGroupName();

	superUserRights = groupB.getDefaultStudySuperUserRights(userId);

	if (StringUtil.isEmpty(superUserRights))
	{
		superUserRights = "";
	}



	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");

    pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));

	if ( pageRight >= 4)

	{

%>
<br>

<%
	CodeDao cdDisplay = new CodeDao();
 	String filDoneOn = "";
	String dStatus = "";
	StringBuffer sb = new StringBuffer();

	// selFirst="";
	String pid = "";
	String pstat =  "";
	String ddStudy = "";
	//String pstudy = "";
	String filEnrDt = "";
	String filVisit = "";
	String filNext = "";
	int inSite = 0;

	String excludeNotEnrolled = "";
	String excludeChecked="";

	//Added by Manimaran on Apr21,2005 for Enrolled PatientStudyId search
	String patStudyId = "";
	  Calendar calEnrolDt = Calendar.getInstance();
	  Calendar calNextVisitDate = Calendar.getInstance();

	int p;

	Integer iStat;
	String anySiteSelected = "0";

	String uOrganization = "";
    String selectedTab="" ;

    selectedTab=request.getParameter("selectedTab") ;
 	pid = request.getParameter("patientid") ;
	if(pid == null)
    	pid = "";
	pstat = request.getParameter("patientstatus") ;
	if(pstat == null)
	   pstat = "";
	String pstudy = request.getParameter("dStudy") ;
	//Added by Manimaran on Apr21,2005 for Enrolled PatientStudyId search
	patStudyId=request.getParameter("pstudyid") ;

	patStudyId=(patStudyId==null)?"":patStudyId;
	ArrayList arrStudySites = new ArrayList();
	ArrayList arrSiteNames =  new ArrayList();

	boolean showStudyCentric = false;
	SettingsDao settingsdao = new SettingsDao();
	ArrayList arSettingsValue = new ArrayList();
	String studyCentricStr = "";

 	//This check has been added to send studyid from enrolledtab screen
	if ((pstudy=="")||(pstudy==null))	pstudy=request.getParameter("studyId");

	pstudy=(pstudy==null)?"":pstudy;

	int pStudyIntValue = 0;



	ArrayList stdIds = new ArrayList();
	ArrayList stdNums = new ArrayList();
	ArrayList stdDates = new ArrayList();

	//JM: blocked
	StudyDao studyDao = studyB.getUserStudies(userId,"dStudy",EJBUtil.stringToNum(pstudy),"activeForEnrl");
	stdIds = studyDao.getStudyIds();
	stdNums = studyDao.getStudyNumbers();
	stdDates = studyDao.getStudyStatDates();

	//following code is to select the first study from drop down when the page opened because enroll patient browser is going to be removed.
	String selFirst;
	if (pstudy.length()==0){

		if ((stdIds!=null) && (stdIds.size()>0))
			pstudy=EJBUtil.integerToString((Integer)stdIds.get(0));
			selFirst="Y";

	}
	if (pstudy.equals("0"))
	{
		selFirst = "Y";
	}


	filEnrDt = request.getParameter("filterEnrDate") ;
	filVisit = request.getParameter("filterLastVisit") ;
	filNext = request.getParameter("filterNextVisit") ;

	excludeNotEnrolled = request.getParameter("cbxExcludeNotEnrolled") ;

	String orderType = "";
	String orderByColumnCode = "";
	orderType = request.getParameter("orderType");


	orderByColumnCode = request.getParameter("orderByColumnCode");


	String pagenum = "";
		int curPage = 0;
		long startPage = 1;
		String stPage;
		long cntr = 0;
		pagenum = request.getParameter("currentpage");

		String submitMode = request.getParameter("submitMode");





		if (StringUtil.isEmpty(submitMode))
		{
			submitMode = "S"; //not submitted via pagination
		}

		if (submitMode.equals("S"))
		{
				pagenum = "1";
		}

		if (pagenum == null)
		{
			pagenum = "1";
		}


		curPage = EJBUtil.stringToNum(pagenum);

	if (excludeNotEnrolled == null)
	{
		excludeNotEnrolled="0";
	}

	if (excludeNotEnrolled.equals("1"))
	{
		excludeChecked = "CHECKED";
	}

	String selSite = null;
	selSite =  request.getParameter("dPatSite");

	if(selSite ==null)
	selSite="All";


	uOrganization = userB.getUserSiteId();


	uOrganization = selSite ;

	pStudyIntValue = EJBUtil.stringToNum(pstudy);

	if (StringUtil.isEmpty(orderType) )
	{
		orderType = "asc";
	}
	if (StringUtil.isEmpty(orderByColumnCode))
	{
	  System.out.println("pStudyIntValue " + pStudyIntValue );

		 if (pStudyIntValue > 0)
		 {
			orderByColumnCode = " lower(site_name),lowerpatstdid"; //patient last name
		}
		else //All stuudies
		{
			orderByColumnCode = " lower(study_number),lower(site_name),lowerpatstdid";
		}
	}

	int tableSize = 0;
	int colWidth = 90;

	//here 12/11 is the number of columns in the browser
	if (groupName.equalsIgnoreCase("admin"))
	{
		tableSize = 13 * colWidth;
	}
	else
	{
		tableSize = 12 * colWidth;
	}

	 if (pStudyIntValue > 0)
		 {
		 		tableSize = tableSize + colWidth; //additional column for study number when study is not selected
		 }


	if (filEnrDt == null) filEnrDt = "ALL";
	if (filNext == null) filNext = "ALL";
	if (StringUtil.isEmpty(filVisit))
	{
	 filVisit = "";
	 }

	String dEnrFilter = EJBUtil.getRelativeTimeDD("filterEnrDate\' id=\'filterEnrDate",filEnrDt);
	String dNextVisit = EJBUtil.getRelativeTimeDDFull("filterNextVisit\' id=\'filterNextVisit",filNext);

	String studyNumDisplay = "";

	if (pid == null) pid = null;

	if (pstat == null) pstat = null;



	if (uOrganization  == null) uOrganization  = "";
	if (uOrganization.equals("All")){
	 uOrganization  = "0";}

	int iPstat = 0;

	 CodeDao cd1 = new CodeDao();

   	cd1.getCodeValues("patStatus",EJBUtil.stringToNum(accId));
   	cd1.setCType("patStatus");
	cd1.setForGroup(defUserGroup);
	 cd1.getHiddenCodelstDataForUser();

	 String hideStr = "";

	iPstat = EJBUtil.stringToNum(pstat);
	sb.append("<SELECT NAME='patientstatus' id='patientstatus'>") ;
	sb.append("<OPTION value=''>"+LC.L_All/*All*****/+"</OPTION>");

 		if (cd1.getCDesc().size() > 0)

		{

			for (int counter = 0; counter <= cd1.getCDesc().size() -1 ; counter++)

			{
			    	hideStr = cd1.getCodeHide(counter) ;

						if (StringUtil.isEmpty(hideStr))
						{
							hideStr = "N";
						}

				iStat = (Integer) cd1.getCId().get(counter);

				if(	iStat.intValue() == iPstat)

				{

				sb.append("<OPTION value = "+ iStat+" SELECTED>" + cd1.getCDesc().get(counter)+ "</OPTION>");

				}

				else

				{
					if (hideStr.equals("N"))
                	{
						sb.append("<OPTION value = "+ iStat+">" + cd1.getCDesc().get(counter)+ "</OPTION>");
					}

				}

			}

		}
 		sb.append("</SELECT>");
 	dStatus = sb.toString();




	String uName = (String) tSession.getValue("userName");
 	UserSiteDao usd = new UserSiteDao ();
	String ddSite = "";

	  //Now sites will appear for a study depending on the rights

	StringBuffer sbSite = new StringBuffer();

	Integer iSite ;
	int defCnt = 0;


	int patDataDetail = 0;
	int personPK = 0;


	personPK = EJBUtil.stringToNum(request.getParameter("pkey"));



		StringBuffer strb = new StringBuffer();
		strb.append("<table width='100%' cellspacing='1' cellpadding='0'>");
		strb.append("<tr><th width='15%'>"+LC.L_Study_Number/*Study Number*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Patient_Id/*Patient ID*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Patient_StudyId/*Patient Study ID*****/+"</th>");
		strb.append("<th width='23%'>"+LC.L_Pat_Name/*Patient Name*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Race/*Race*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Ethnicity/*Ethnicity*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Home_Phone/*Home Phone*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Work_Phone/*Work Phone*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Address_1/*Address 1*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Address_2/*Address 2*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_City/*City*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_State/*State*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Zip_Code/*Zip Code*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Country/*Country*****/+"</th>");
		strb.append("<th width='15%'>"+LC.L_Notes/*Notes*****/+"</th>");
		strb.append("<th width='8%'>"+LC.L_Enrolled_On/*Enrolled On*****/+"</th>");
		strb.append("<th width='8%'>"+LC.L_Last_Visit/*Last Visit*****/+"</th>");
		strb.append("<th width='8%'>"+LC.L_Next_Visit/*Next Visit*****/+"</th>");
	//JM: 040306
	//strb.append("<th width='15%'>"+LC.Pat_Patient+" Status</th></tr>");
		strb.append("<th width='15%'>"+LC.L_Patient_Status/*Patient Status*****/+"</th>");
		strb.append("<th width='8%'>"+LC.L_Organization/*Organization*****/+"</th></tr>");

	%>

<Form  name="search" method="POST" onsubmit="return false;">
 <input type=hidden name="orderType" value="<%=orderType%>">
 <input type=hidden name="orderByColumnCode" value="<%=orderByColumnCode%>">
 <input type=hidden name="openMode" value="<%=openMode%>">
<input type=hidden name="submitMode" value="">

 <input type=hidden name="patStudyId" value="<%=patStudyId%>">
 <input type=hidden name="pstudy" value="<%=pstudy%>">
 <input type=hidden name="flag" value="1">



<%
	StringBuffer sbStd = new StringBuffer();
	//FIX #6045
	StringBuffer sbStdPermCloseDate = new StringBuffer();



	Integer iStd ;
	String allValue=EJBUtil.ArrayListToString(stdIds);
	
	allValue=StringUtil.trueValue(allValue);
	sbStd.append("<SELECT NAME='dStudy' id='dStudy' onchange='if (checkForAll()) {orgRefresh();}' STYLE='WIDTH:177px' >") ;
	//sbStd.append("<OPTION value='"+allValue+"'>All</OPTION>") ;

	//FIX #6045 new dd which contains closure dates, no closure study will have a study id
	String studyPermClosureDate = "";
	sbStdPermCloseDate.append("<SELECT NAME='dStudyPermCloseDate' id='dStudyPermCloseDate' STYLE='visibility:hidden' >") ;
	//sbStdPermCloseDate.append("<OPTION value='"+allValue+"'>All</OPTION>") ;
	
	if (stdIds.size() > 0)
		{
			   for (int counter = 0; counter < stdIds.size() ; counter++)
				{

			    iStd = (Integer) stdIds.get(counter);
			    studyPermClosureDate = (String)stdDates.get(counter);
				studyPermClosureDate = (StringUtil.isEmpty(studyPermClosureDate))? ""+iStd:studyPermClosureDate;
				
				if(	iStd.intValue() == pStudyIntValue)
				{

				sbStd.append("<OPTION value = "+ iStd+" SELECTED>" + stdNums.get(counter)+ "</OPTION>");
				sbStdPermCloseDate.append("<OPTION value = "+ iStd +" SELECTED>" + studyPermClosureDate + "</OPTION>");
				
				}
				else
				{

				sbStd.append("<OPTION value = "+ iStd+">" + stdNums.get(counter)+ "</OPTION>");
				sbStdPermCloseDate.append("<OPTION value = "+ iStd +">" + studyPermClosureDate + "</OPTION>");
				
				}
				}
		}

	    sbStd.append("</SELECT>");
	    sbStdPermCloseDate.append("</SELECT>");
	    ddStudy  = sbStd.toString();
	    String ddStudyCloseDates = sbStdPermCloseDate.toString();;
%>

<DIV class="tabDefTopN" id="div1">
  <jsp:include page="mgpatienttabs.jsp" flush="true"/>
</div>

<DIV class="tabFormTopN tabFormTopN_PS" id="div2">


 <table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl outline midAlign">
    <tr height="18" >
		<td colspan="8"><b><%=LC.L_Search_By%><%--Search By*****--%></b></td>
	</tr>
	<tr >
	 <td width="12%">	<%=MC.M_PatOn_Study%><%--<%=LC.Pat_Patients%> on <%=LC.Std_Study%>*****--%>: </td><td width="10%">	<%=ddStudy%></td>
	 <td width="10%" align="right"> <%=LC.L_Organization%><%--Organization*****--%>:&nbsp; </td> <td >	<span id="orgDD"> </span>	</td>
    <td align="right" width="10%"> <%=LC.L_Last_Visit%><%--Last Visit*****--%>:&nbsp;  </td><td width="10%">     <Input type=text name="filterLastVisit" id="filterLastVisit" value = "<%=filVisit%>" size = 15 MAXLENGTH = 10>    </td>
     <td></td>
      <td width="10%"></td>
    </tr>


<input type="hidden" id="srcmenu" Value="<%=src%>">
<input type="hidden" id="selectedTab"  value="<%=selectedTab%>">

     <tr>

      <td > <%=LC.L_Patient_Id%><%--<%=LC.Pat_Patient%> ID*****--%>: </td><td>  <Input type=text name="patientid" id="patientid" value = "<%=pid%>" size = 15>     </td>
	  <!--Added by Manimaran on Apr21,2005 for Enrolled PatientStudyId search-->
	  <td align="right"> <%=LC.L_Enrolled_On%><%--Enrolled On*****--%>:&nbsp; </td><td width="10%">  <%= dEnrFilter%> </td>
	  <td align="right"> <%=LC.L_Next_Visit%><%--Next Visit*****--%>:&nbsp; </td><td>   <%= dNextVisit%>	  </td>
  	  <td width="10%"></td>
  	  <td width="10%"></td>
  	</tr>
  	<tr >
      <td > <%=LC.L_Patient_StudyId%><%--<%=LC.Pat_Patient%> <%=LC.Std_Study%> ID*****--%>:</td><td> <Input type=text name="pstudyid" id="pstudyid" value = "<%=patStudyId%>" size = 15>  </td>
      <td align="right"><%=LC.L_Patient_Status%><%--<%=LC.Pat_Patient%> Status*****--%>:&nbsp; </td><td> <%=dStatus%></td>
	 <td colspan="3" align="center">
    		<Input type="hidden"  name="excludeNotEnrolled" value=<%=excludeNotEnrolled%>>
    		<Input name="cbxExcludeNotEnrolled" id="cbxExcludeNotEnrolled" type="checkbox" value="1" <%=excludeChecked%>/><%=MC.M_ExcludePat_CurNotEnrl%><%--Exclude <%=LC.Pat_Patients_Lower%> not currently Enrolled*****--%> &nbsp;
      </td>
      	  	<td width="10%">	<!--input type="image" src="../images/jpg/go_white.gif" onClick="checkForAll(document.search)" align="bottom" border=0-->
      	  	<button style="margin:-0.5em 1em 0em 1em" type="submit" onclick="if (checkForAll()) { paginate_study.rowsPerPage=0;  (paginate_study.runFilter())};"><%=LC.L_Search%></button>
	</td>
    </tr>
  </table>
  <!--FIX #6045-->
  <%=ddStudyCloseDates%>
</div>


<DIV class="tabFormBotN tabFormBotN_P_2" id="div2">


<DIV>

  <%


	if ((pid == null) && (pstat == null) && (pstudy == null)){%>

  <P class="defComments"> <%=MC.M_PlsSrchCrit_ViewThePatList%><%--Please specify the Search criteria and then click on 'Search' to view the <%=LC.Pat_Patient%> List*****--%></P>

  <%}else{
  	    String siteForPatFacility = "";
  	    String studyNumber = "";

         defCnt = 0;



		String orgStrr= "";

		if (arrStudySites.contains(uOrganization))
		{
			  defCnt ++;
		}
		if(defCnt>0 )
		{
		  selSite = uOrganization;
		}
		else if(selSite.equals("All"))// || selSite.equals("") || selSite.equals("0"))
		{
		selSite = "0";
		}
		else{

		if(arrStudySites.size() > 0)
		selSite = arrStudySites.get(0).toString();
		else
		selSite = "0";

		}

		if(selSite.equals("0")){
			anySiteSelected="0";
				}
		else
		{
			anySiteSelected="1";
		}


		if(selSite.equals("0")){

  		 for(int arr = 0;arr<arrStudySites.size(); arr++)
  		 {
  		  if(arr == 0)
  		      orgStrr = arrStudySites.get(arr).toString();
		  else{
  		      orgStrr = orgStrr+","+arrStudySites.get(arr).toString();
		   }
		}
		}
		else
		 {
			orgStrr = selSite;
		}

		if (anySiteSelected.equals("0"))
		{
			siteForPatFacility = "0";
		}
		else
		{
			siteForPatFacility = orgStrr;
		}





	  	studyB.setId(pStudyIntValue);
	  	studyB.getStudyDetails();

	  	studyNumber = studyB.getStudyNumber();
	  	studyPermClosureDate = studyStatB.getMinPermanentClosureDate(pStudyIntValue);
	  	
	  	String linkStyle="";
		//If the study is marked for permanent closure, the three top links would dispappear
		if (StringUtil.isEmpty(studyPermClosureDate)){
			linkStyle="display:block;";
		} else {
			linkStyle="display:none;";
		}
	%>
  		<table id="enroll_links" style="<%=linkStyle%>">

  		<tr>
  		 <td colspan = "7"><B><%=MC.M_EtrScrnOrEnrl_Dets%><%--Enter Screening/Enrollment details*****--%> </B>&nbsp;</td>
		<td> &nbsp;&nbsp;<A href = "javascript:void(0);" onClick="enrPat(document.search)"><%=MC.M_Selc_ExistingPat_Upper%><%--SELECT AN EXISTING <%=LC.Pat_Patient_Upper%>*****--%></A> </td>

		<td>&nbsp;&nbsp;&nbsp;&nbsp;<A onClick="openStudyCentric()" href = "#"><%=MC.M_AddNew_Pat_Upper%><%--ADD A NEW <%=LC.Pat_Patient_Upper%>*****--%></A></td>

		<td>&nbsp;&nbsp;&nbsp;&nbsp;<%
		int accIdNum = EJBUtil.stringToNum(accId);
		SettingsDao settingsDao =
			CommonB.retrieveSettings(accIdNum, 1);
		final String ACCELARATED_ENROLLMENT_CONFIG = "ACCELARATED_ENROLLMENT_CONFIG";
		boolean isAccelaratedEnrollOn = false;
		for (int x = 0; x<settingsDao.getSettingKeyword().size(); x++){
			String settingKey = (String)settingsDao.getSettingKeyword().get(x);
			if(ACCELARATED_ENROLLMENT_CONFIG.equalsIgnoreCase(settingKey)){
				isAccelaratedEnrollOn = true;
				break;
			}
		}
		if (isAccelaratedEnrollOn){
			%><a onClick="openPatientWorkflow()" href = "#"><%=LC.L_Accel_Enrol_Upper%><%--ACCELERATED ENROLLMENT*****--%></a><%
		}
		%></td>



		</tr>
		<tr height="10"><td></td></tr>
		</table>
		<%

        Calendar now = Calendar.getInstance();
		String startDt = "";
		String endDt =  "";

		//startDt =  ""+ (now.get(now.MONTH) + 1) + "/" + "01" + "/" + now.get(now.YEAR) ;

		startDt = DateUtil.getFormattedDateString(String.valueOf(now.get(now.YEAR)),"01",String.valueOf( now.get(now.MONTH) + 1) ) ;

	    now.add(now.MONTH,1);

	    //endDt = "" + (now.get(now.MONTH) + 1) + "/" + now.getActualMaximum(now.DAY_OF_MONTH) + "/" + now.get(now.YEAR)  ;

	    endDt  = DateUtil.getFormattedDateString(String.valueOf(now.get(now.YEAR)),String.valueOf(now.getActualMaximum(now.DAY_OF_MONTH)),String.valueOf( now.get(now.MONTH) + 1) ) ;
%>


			<Input type=hidden name="repName" value="">
			<Input type=hidden name="repId" value="">
			<Input type=hidden name="studyPk" value="">
			<Input type=hidden name="filterType" value = "">
			<input type=hidden name="id" value="">
			<input type=hidden name="protId" value="">
			<input type=hidden name="dateFrom" value="<%=startDt%>">
			<input type=hidden  name="dateTo" value="<%=endDt%>">
			<Input type="hidden" name="currentpage" value="<%=curPage%>">
			<input type="hidden" id="grpId" value="<%=defGroup%>">
			<input type="hidden" id="userId" value="<%=usrId%>">
			<input type="hidden" id="accountId" value="<%=accId%>">
			<input type="hidden" id="siteForPatFacility" value="<%=siteForPatFacility%>">
			<input type="hidden" id="orgStrr" value="<%=orgStrr%>">
			<input type="hidden" id="groupName" value="<%=groupName%>">








<div id="serverpagination"></div>




	  </Form>


  <%}//end of Parameter check

  /////////////////

} //end of if body for page right

else



{



%>



<jsp:include page="accessdenied.jsp" flush="true"/>

<%



} //end of else body for page right





  ///////////////

}//end of if body for session

else{

%>



  <jsp:include page="timeout.html" flush="true"/>

  <%

}



%>

  <div>

    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>

</div>

</div>


<div class ="mainMenu" id = "emenu">

 <jsp:include page="getmenu.jsp" flush="true"/>

</div>
</body>

</html>
