<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title><%=MC.M_Std_TeamRoleBrowser%><%--<%=LC.Std_Study%> Team Role Browser*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT Language="javascript">

function openRoleWin() {

	param1="rolerights.jsp?mode=N";
	windowName = window.open(param1,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=450,height=575')
	windowName.focus();
}

function openRoleWin1(role,rolesubtype,cid) {

	param1="rolerights.jsp?mode=M"+"&role="+role+"&rolesubtype="+rolesubtype+"&cid="+cid;
	windowName = window.open(param1,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=450,height=575')
	windowName.focus();
}
</script>
</head>
<Link Rel=STYLESHEET HREF="styles/velos_style.css" type=text/css>
<body>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page import="com.velos.eres.service.util.LC"%><%@page import="com.velos.eres.service.util.MC"%>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%

 HttpSession tSession = request.getSession(true);
 String acc = (String) tSession.getValue("accountId");
 int accountId = EJBUtil.stringToNum(acc);

CodeDao codedao = new CodeDao();
codedao.getCodeValues("role");
ArrayList cDesc = codedao.getCDesc();
ArrayList cSubType = codedao.getCSubType();
ArrayList cId = codedao.getCId();

String cdesc=null;
String csubtype=null;
String cid=null;

int len = cId.size();
int counter = 0;
%>
<DIV class="browserDefault_veloshome" id="div1">

&nbsp;&nbsp;<B><%=MC.M_Std_TeamRoleBrowser%><%--<%=LC.Std_Study%> Team Role Browser*****--%></B>
<br>
<table width="100%" cellspacing="0" cellpadding="0" border="0" >
	<tr>
		<td width="25%">&nbsp;</td>
		<td width="25%">&nbsp;</td>
		<td width="25%"> <A href=# onClick="openRoleWin()"><%=LC.L_Add_NewRole%><%--Add New Role*****--%></A></td>
	</tr>
	<tr>&nbsp;</tr>
</table>
<table width="100%" cellspacing="0" cellpadding="1" border=1 >
	<tr>
		<td width="25%">&nbsp;<B><%=LC.L_Roles%><%--Roles*****--%></B> </td>
		<td width="25%"><B> <%=LC.L_Role_SubType%><%--Role Sub Type*****--%></B></td>
	</tr>
	<tr>&nbsp;</tr>
<%

 for(counter = 0;counter<len;counter++)
 {

  	cid= (cId.get(counter)).toString();
	cdesc=((cDesc.get(counter)) == null)?"-":(cDesc.get(counter)).toString();
 	csubtype=((cSubType.get(counter)) == null)?"-":(cSubType.get(counter)).toString();
%>
<tr>
<td>
<A href=# onClick="openRoleWin1('<%=cdesc%>','<%=csubtype%>','<%=cid%>')"> &nbsp;<%=cdesc%></A> </td>
<td><%=csubtype%></td>
</tr>
<%
 }
%>
</table>
</div>

<jsp:include page="velosmenus.htm" flush="true"/>
</body>
</html>
