<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<body>
  
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <jsp:useBean id="strB" scope="request" class="com.velos.eres.web.storage.StorageJB"/>
  <%@page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*"%>
 
  <%

	String eSign = request.getParameter("eSign");	
	HttpSession tSession = request.getSession(true); 

	if (sessionmaint.isValidSession(tSession))

   { 
     %>
     <jsp:include page="sessionlogging.jsp" flush="true"/> 

     <%   
   
   	String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign)) 
	{
%>
  	   <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} 
	else 
  	{
		String ipAdd = (String) tSession.getValue("ipAdd");
    	String usr = null;
	    usr = (String) tSession.getValue("userId");
		
		String copyCounts[] = null;
		String strgIds[] =null;
		String strgNames[] =null;
		String templates[] =null;
		String strgPks[] = null;

		String template =null;

		copyCounts = request.getParameterValues("copyCount");
		strgIds = request.getParameterValues("strgId");
		strgNames = request.getParameterValues("strgName");
		strgPks = request.getParameterValues("strgPk");

		String counts[] =null;
		String totRows = "1";
		
		totRows = (new Integer(copyCounts.length)).toString();
		
		String templateValues[]=new String[EJBUtil.stringToNum(totRows)];
		for(int i=0; i<EJBUtil.stringToNum(totRows);i++)
		{
		    template =  request.getParameter("template"+i);
			if(template != null)
				templateValues[i] = "1";
			else
				templateValues[i]="0";
			if (copyCounts[i] == "")
				copyCounts[i]="0";

		}
	
		int ret =strB.copyStorageUnitsAndTemplates(strgPks,copyCounts,strgIds,strgNames,templateValues,usr,ipAdd);

		if(ret == 0) {
      %>
      <br><br>
	  <p class ="successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>
	   <SCRIPT>
			window.opener.location.reload();
           	setTimeout("self.close()",1000);
      </SCRIPT>
	  
      <% } else {%>
	  <br><br>
	  <p class = "successfulmsg" align = center> <%=MC.M_DataNotSvd_Succ %><%-- Data was not saved successfully*****--%> </p>	
	   <SCRIPT>
			window.opener.location.reload();
           	setTimeout("self.close()",1000);
      </SCRIPT>
	  <% } 


}//end of eSign check
}//end session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
	<div class = "myHomebottomPanel"> 
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>

</BODY>
</HTML>
