<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title>Start Export</title>
  
<%@ page import="java.sql.*,com.velos.eres.service.util.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.impex.*,java.text.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

</head>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "java.util.*,com.velos.eres.business.common.*"%>

<body>
<br>
<DIV id="div1"> 

<%
	HttpSession tSession = request.getSession(true); 
	if (sessionmaint.isValidSession(tSession))
	{

%>	
		<jsp:include page="sessionlogging.jsp" flush="true"/>
 <%   

				
		String param = "";
		String paramVal = "";
		String paramName = "";
		String originatingSiteCode  = "";
		
		String accId ;
		
		int  selectedModulePkParam ;
		String  totalModulePkParam ;
		
		int selectedModulePk = 0;
		int totalModulePk = 0;

		String[] partSites = null;
		int siteCount = 0;
			String[] arrModulePK = null;
					StringBuffer sbModulePK = new StringBuffer();
		
		StringBuffer sbExportXML = new StringBuffer();
		
		sbExportXML.append("<exp>");

		accId = request.getParameter("accId");
		String userId = (String) tSession.getValue("userId");
		
		String linkToFlag = request.getParameter("linkToFlag");
		
		if (StringUtil.isEmpty(linkToFlag))
		{
			linkToFlag = "";
		}
		
							
		sbExportXML.append("<account>" + accId + "</account>");
		
		
		//get SITE_CODE OF THE ACCOUNT FROM WHERE DATA IS EXPORTED
		
		if (! StringUtil.isEmpty(accId))
		{
			PreparedStatement pstmt = null;
		    String sqlAcc = ""; 
		  	
		  	Connection conn = null;
		  	
		  	sqlAcc = " Select site_code from er_account where pk_account = ?";
		  	
		  	CommonDAO cd = new CommonDAO();
		  	
		  	
		  	try{
				conn = cd.getConnection();
					 
				pstmt = conn.prepareStatement(sqlAcc);
				pstmt.setInt(1,EJBUtil.stringToNum(accId));			
				 
				ResultSet rs = pstmt.executeQuery();
					
				while (rs.next())
				{	
					originatingSiteCode = rs.getString("site_code");
				}
			 }
			  catch (Exception ex)
			  {
			  	out.println("Exception in getting the account site code: " + ex );
			   }
			  finally
				{
					try
					{
						if (pstmt != null ) pstmt.close();
					}
					catch (Exception e) 
					{
					}
					try 
					{
						if (conn!=null) conn.close();
					}
					catch (Exception e) {}
					
				}	
			
		}
		
		
		
		sbExportXML.append("<originatingSiteCode>"+ originatingSiteCode + "</originatingSiteCode>");
		
							
		int reqId = 0;

	   	sbExportXML.append("<modules>");
	
		totalModulePkParam = request.getParameter("patform_totalcount");
					
		totalModulePk = EJBUtil.stringToNum(totalModulePkParam);
					
		if (totalModulePk > 0) //get the selected module Primary keys  
				{
						arrModulePK = request.getParameterValues("patform");
						
						if (arrModulePK == null)
						{
							selectedModulePkParam = 0; 
						}else //the module has some primary keys selected
						{
							selectedModulePkParam = arrModulePK.length;
							
							for (int m = 0; m < selectedModulePkParam; m++)
							{
								sbModulePK.append("<modulePK>" + arrModulePK[m] +"</modulePK>");
							}
														
						}
						
					}
					else
					{
						selectedModulePkParam = 0;
					}
					
				sbExportXML.append("<module name = \"sp_patform\"");
					
					if ((selectedModulePkParam == totalModulePk || totalModulePk == 0) && (!linkToFlag.equals("L")) ) 
					{
						sbExportXML.append(" expAll=\"Y\" > ");
					}
					else if (selectedModulePkParam < totalModulePk || linkToFlag.equals("L") )
					{
						sbExportXML.append(" expAll=\"N\" >");
						sbExportXML.append(sbModulePK.toString());
					}
	
						sbExportXML.append("</module>");
	
			 	 sbExportXML.append("</modules>");
			 sbExportXML.append("</exp>");
		 System.out.println(sbExportXML.toString());
		%>
		<%
		
		// get participating sites
		
		partSites = request.getParameterValues("partSite");
		siteCount = partSites.length;
	
		//Prepare export Request

		ImpexRequest expReq = new ImpexRequest();
		
		Impex impexObj = new Impex();
		
   		for (int k=0; k < siteCount ; k++)
   		{
   			expReq.setExpSites(partSites[k]);
   		}
		
		 expReq.setReqDetails(sbExportXML.toString());
		 expReq.setReqStatus("0"); //export not started

		 expReq.setReqBy(userId);

		 //set datetime
		 
		Calendar now = Calendar.getInstance();
		String dt = "";
		dt = "" + now.get(now.DAY_OF_MONTH) + now.get(now.MONTH) + (now.get(now.YEAR) - 1900) ;
		Calendar calToday=  Calendar.getInstance();
		String format="yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat todayFormat=new SimpleDateFormat(format);
		dt = todayFormat.format(calToday.getTime());
		
		 expReq.setReqDateTime(dt);
	 
		// Log this export request

		impexObj.setImpexRequest(expReq);
		reqId = impexObj.recordExportRequest();
		impexObj.setRequestType("E");		
		impexObj.start();
		
		%>
			<BR><BR><BR><BR>
			<P align = "center" class="defComments">
			
			We have recorded your request. 
			<BR> Your request Id is : <font color="red"><b><%=reqId%></b></font> . Please use this Id to track your export request.
			<BR> Please wait while we take you to the 'Export Logs' page.
		
			</P>
		  <META HTTP-EQUIV=Refresh CONTENT="2; URL=viewexpstatus.jsp">
		<%
    
     }//end of if body for session 	
	
	else
	{
%>
	  <jsp:include page="timeout.html" flush="true"/>
<%
	
} //end of else of the Session 
 

%>
</div>
</body>

</html>

