<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>

<script>
</script>


<body>

<title><%=LC.L_Std_Forms%><%--<%=LC.Std_Study%> Forms*****--%></title>
<%@ page import="com.velos.eres.service.util.LC"%>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<% String src;

src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String studyId = null;
String from="form";

%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>


<DIV  class="browserDefault" id="div1">

<jsp:include page="studytabs.jsp" flush="true">
<jsp:param name="from" value="<%=from%>"/>
<jsp:param name="selectedTab" value="<%=selectedTab%>"/>
</jsp:include>


<br>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.user.UserJB"
%>


<%
 HttpSession tSession = request.getSession(true);


  if (sessionmaint.isValidSession(tSession))
 {
	 // by salil
	 studyId = (String) tSession.getValue("studyId");
	 if(studyId == "" || studyId == null) {
		%>
			<jsp:include page="studyDoesNotExist.jsp" flush="true"/>
		<%
		}
	 else{
	 //check for page right
	  int istudyId = EJBUtil.stringToNum(studyId);
	  GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	  StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
	  int study_acc_form_right = Integer.parseInt(grpRights.getFtrRightsByValue("STUDYFRMSACC"));
      int study_team_form_access_right=Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));

	 if (study_acc_form_right>=4 && study_team_form_access_right>=4)
	 {
		 ArrayList arrFrmIds = null;
		 ArrayList arrFrmNames = null;
		 ArrayList arrDesc = null;
		 ArrayList arrLastEntryDate = null;
		 ArrayList arrNumEntries = null;
		  //by salil
		 ArrayList arrEntryChar=null;
		 int frmId = 0;
		  String entryChar="";

		 String accId=(String)tSession.getAttribute("accountId");
		 int iaccId=EJBUtil.stringToNum(accId);
 	     String userId = (String) tSession.getValue("userId");
	     UserJB userB = (UserJB) tSession.getValue("currentUser");
   	     String siteId = userB.getUserSiteId();

		 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
		 lnkFrmDao = lnkformB.getStudyForms(iaccId,istudyId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId));
		 arrFrmIds = lnkFrmDao.getFormId();
		 arrFrmNames = lnkFrmDao.getFormName();
		 arrDesc = lnkFrmDao.getFormDescription();
		 arrEntryChar = lnkFrmDao.getFormEntryChar();
		 arrLastEntryDate = lnkFrmDao.getFormLastEntryDate();
		 arrNumEntries = lnkFrmDao.getFormSavedEntriesNum();
		 int len = arrFrmNames.size();
 %>
 <Form name="formlib" action="formLibraryBrowser.jsp"  method="post">

 <table width="100%" cellspacing="1" cellpadding="0" >
					<tr>
				        <th width="20%"><%=LC.L_Frm_Name%><%--Form Name*****--%></th>
				        <th width="25%"><%=LC.L_Description%><%--Description*****--%> </th>
						<th width="15%"><%=LC.L_Last_EntryOn%><%--Last Entry On*****--%> </th>
						<th width="20%"><%=LC.L_Number_OfEntries%><%--Number of Entries*****--%></th>
					</tr>
	 <%
	 int counter=0;
	 int i=0;
	for(counter=0;counter<len;counter++)
		{
		frmId = ((Integer)arrFrmIds.get(counter)).intValue();
		entryChar=(String)arrEntryChar.get(counter);
		String frmName=((arrFrmNames.get(counter)) == null)?"-":(arrFrmNames.get(counter)).toString();
		String desc=((arrDesc.get(counter)) == null)?"-":(arrDesc.get(counter)).toString();
		String lastEntryDate=((arrLastEntryDate.get(counter))== null)?"-":(arrLastEntryDate.get(counter)).toString();
		String numEntries=((arrNumEntries.get(counter))== null)?"-":(arrNumEntries.get(counter)).toString();
		int lk = Integer.parseInt(numEntries);
			if ((i%2)==0)
				{i++;%>
				<tr class="browserEvenRow">
				<%}else
				 {i++;%>
				 <tr class="browserOddRow">
					<%}
				if( lk==0){%>
					<td width="10%"><A href="studyformdetails.jsp?srcmenu=tdmenubaritem3&selectedTab=8&formId=<%=frmId%>&mode=N&studyId=<%=studyId%>&formDispLocation=S&entryChar=<%=entryChar%>"><%=frmName%></A></td>
				<%}
				 else
				 {%>
					<td width="10%"><A href="formfilledstudybrowser.jsp?formId=<%=frmId%>&srcmenu=<%=src%>&studyId=<%=studyId%>&selectedTab=<%=selectedTab%>&entryChar=<%=entryChar%>"><%=frmName%></A></td>
				<%}%>


				<!--<td width="10%"><A href="formfilledstudybrowser.jsp?formId=<%=frmId%>&srcmenu=<%=src%>&studyId=<%=studyId%>&selectedTab=<%=selectedTab%>&entryChar=<%=entryChar%>"><%=frmName%></A></td>-->
				<td width="8%"><%=desc%></td>
				<%if(lastEntryDate!="-"){%>
				<td width="13%"><%=lastEntryDate%></td>
					<%}else{%>
				<td width="13%"><%=lastEntryDate%></td>
					<%}%>
				<td width="13%"><%=numEntries%></td>

		</tr>

<%		} //for loop%>


	</table>
	 <%
	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right
} //study id not null
} //session
else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div><div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/></div>
</div>
</body>

</html>



















