<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>
<jsp:useBean 
    id="homepage" 
	class="com.velos.eres.service.util.SearchHomepageXml"
	scope="session" 
/>

<%-- <jsp:setProperty 
	name="homepage" 
	property="root" 
	value = Configuration.ERES_HOME.HomePage.xml" 
/>--%>

<% homepage.setRoot(Configuration.ERES_HOME+"HomePage.xml"); %>
<html>
<head>
	<title><%=LC.L_Eres_Home_Page%><%--eResearch Home Page*****--%></title>
</head>

<body>
	<table border="0">
		<tr>
		<td width="400">
		<% {
		    String sectionsList[] = homepage.listOfSections();
		   	int noOfSections = sectionsList.length;
		   	for(int j =0;j<noOfSections;j++)
		   	{
		   	 if (sectionsList[j] == null)
			 {
			  break;
			 }
		%>
		  <h3>
		  	  <%= sectionsList[j] %>
		  </h3>
		<% Vector linksList = homepage.listOfLinks(sectionsList[j]);
		   int size = linksList.size();
		   for(int i =0;i<size;i++)
		   {
		    String type = (String)((Hashtable)linksList.elementAt(i)).get("Type");
			if(type.equals("email"))
			{
		%>
			<br>
			<A HREF="mailto:<%=((Hashtable)linksList.elementAt(i)).get("Link") %>">
			   <%=((Hashtable)linksList.elementAt(i)).get("Name")%>
			</A>
		<%
		  	}
			if (type.equals("Link"))
			{
		%>
		  	<br> 
			<A HREF="http://<%=((Hashtable)linksList.elementAt(i)).get("Link") %>"> 
				<%=((Hashtable)linksList.elementAt(i)).get("Name")%> 
			</A>							
		<%
		  	}
			}
			}
			}
		%>
		</td>
	</tr>  
</table>

</body>
</html>
