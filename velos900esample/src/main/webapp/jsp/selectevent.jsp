<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Select_Evts%><%--Select Events*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">
	
	function validate(formobj) {
		if (!(validate_col('Esign',formobj.eSign))) return false

 		 if(isNaN(formobj.eSign.value) == true) {
			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
			formobj.eSign.focus();
			return false;
		}
	
		count = formobj.noOfCheckboxes.value;
		for (i=0;i<count;i++) {
			if (count > 1) {
				if (formobj.eventcheckbox[i].checked) {
					formobj.submit();
					return false;
				}
			} else {
				if (formobj.eventcheckbox.checked) {
					formobj.submit();
					return false;
				}			
			}
		}
		alert("<%=MC.M_Selc_AnEvt%>");/*lert("Please select an Event.");*****/
		return false;
	}
	

	 function fclose() {
		self.close();
	}  
	
		
</SCRIPT>

</head>

<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>

<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1) 
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {	
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>
<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"%>

<% int pageRight = 0;
   HttpSession tSession = request.getSession(true); 
   
   if (sessionmaint.isValidSession(tSession))

 {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
   int accId = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));
   GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
   pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CGRP"));   

   String description="";
   String protocolId="";
   String categoryId="";
   String eventId="";
   String eventType="";	 
   String eventName="";
   String catName="";
  
   String src= request.getParameter("srcmenu");  
   String duration= request.getParameter("duration");
   protocolId= request.getParameter("protocolId");
   String calledFrom= request.getParameter("calledFrom");
   String mode= request.getParameter("mode");
   String calStatus= request.getParameter("calStatus");
   String cost= request.getParameter("cost"); 

   int counter = 0;
   Integer id;
   String uName = (String) tSession.getValue("userName");

   categoryId= request.getParameter("catId");
   catName= request.getParameter("catName");
	  			
   ArrayList eventIds= null; 
   ArrayList names= null;
   ArrayList descriptions= null;
   ArrayList event_types= null;
   
 	ctrldao= eventdefB.getAllProtAndEvents(EJBUtil.stringToNum(categoryId)); 
	eventIds=ctrldao.getEvent_ids(); 
	names= ctrldao.getNames();
	descriptions= ctrldao.getDescriptions();
	event_types= ctrldao.getEvent_types();

   int len = eventIds.size() ;   

   pageRight = 7;
 if (pageRight > 0 )
	{
 %>

<body style="overflow:scroll;"> 
<P class = "userNameNew"><%= uName %></p>
<P class="sectionHeadings"> <%=LC.L_Select_Evts%><%--Select Events*****--%> </P>

<% if (len>1) { %>
  <table width="100%" cellspacing="0" cellpadding="0" border=0 >
    <tr > 
      <td width = "70%"> 
        <P class = "defComments"><%Object[] arguments = {catName};%><%=VelosResourceBundle.getMessageString("M_FlwgEvt_PcolCal",arguments)%> <%--The following are the Events currently listed in Category "<%=catName%>". Select the Events that you wish to use in your protocol calendar***** --%></P>
      </td>
    </tr>
  </table>
  
  <Form name="eventlibrary" method="post" action="protocoleventsave.jsp?mode=<%=mode%>&srcmenu=<%=src%>&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&cost=<%=cost%>" onsubmit="return validate(document.eventlibrary)">

  	<table width="60%" cellspacing="0" cellpadding="0">
	<tr>
	   <td width="30%"> 
		<P class="defComments"><%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT></P>
	   </td>
	   <td>
		<input type="password" name="eSign" maxlength="8">
	   </td>
	</tr>
	</table>

   	<br>
  
	 <table width="100%">
      <tr class = "popupHeader"> 
 	    <th width="10%"><%=LC.L_Select%><%--Select*****--%></th>
        <th width="40%"> <%=LC.L_Event%><%--Event*****--%> </th>
        <th width="50%"> <%=LC.L_Description%><%--Description*****--%> </th>
      </tr>
 
	
 <%
 	int i=1;
	int noOfCheckboxes=0;
    for(counter = 0;counter<len;counter++)
	{

	id = (Integer)eventIds.get(counter);
	eventId = id.toString();

eventType=((event_types.get(counter)) == null)?"-":(event_types.get(counter)).toString();

eventName=((names.get(counter)) == null)?"-":(names.get(counter)).toString();

description=((descriptions.get(counter)) == null)?"-":(descriptions.get(counter)).toString();

  if (eventType.equals("E")) {
  	i++;
	if ((i%2)==0) {
	%>
      <tr class="browserEvenRow"> 
  <%
		}
	else {
  %>
      <tr class="browserOddRow"> 
   <%
		}
	%>
	<td align=center><INPUT name=eventcheckbox type=checkbox value=<%=id%>></td>
	<td><A href="viewevent.jsp?eventId=<%=id%>&calledFrom=P"> <%=eventName%></A></td>																																					 
	<td><%=description%></td>
	
    </tr>
 <%
 	noOfCheckboxes++;
		}
	}		
%>
    </table>
	
	<input type=hidden name=noOfCheckboxes value=<%=noOfCheckboxes%>>
	
  <br>
  <table width="100%" cellspacing="0" cellpadding="0">
  	<tr>
      <td align=center> 
      	<button type="submit" onclick="return validate(document.eventlibrary)"><%=LC.L_Search%></button>
      </td>
      </tr>
  </table>
  </Form>

 <%
 	} else { //end of len 
 %>
  <table width="100%" cellspacing="0" cellpadding="0" border=0 >
    <tr> 
      <td width = "70%"> 
      <%Object[] arguments1 = {catName}; %>
        <P class = "defComments"><%=VelosResourceBundle.getMessageString("M_ThereNoEvtCat_PlsSel",arguments1)%><%--There are no Events in the Category "<%=catName%>". Please select a different Category.*****--%></P>
      </td>
   	 <tr>
      <td align=center> 
		<button onClick="fclose()"><%=LC.L_Back%></button>
      </td>
      </tr>
  </table>

 <%	}
	
	} //end of if body for page right
else
{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
 <%
 } //end of else body for page right
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%>
 
</div>

</body>


</html>
