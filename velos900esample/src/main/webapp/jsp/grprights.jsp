<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD></HEAD>
<BODY>
<%=LC.L_Save_Sec%> <%=LC.L_2%><%-- SAVE SECTION 2*****--%>

<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>

<%=MC.M_GetUsr_FromSess_Upper%><%--GET USER FROM SESSION*****--%>

<%HttpSession tSession = request.getSession(true); 
  out.print(tSession);
 UserJB user = (UserJB) tSession.getValue("currentUser");%>

<%=MC.M_GetGrp_RightsInfo_Upper%><%--GET GROUP RIGHTS INFORMATION*****--%>

<%grpRights.setId(Integer.parseInt(user.getUserGrpDefault()));%>
<%grpRights.getGrpRightsDetails();%>

<%=LC.L_My_Acc_Upper%><%--MY ACCOUNT*****--%> <BR>
<TABLE border = 1 width = 200>
<TR>
<TD>
<%
if ( Integer.parseInt(grpRights.getFtrRightsByValue("EPROFILE")) > 0)
{
	out.print("<A HREF = 'editProfile.jsp'> "+LC.L_Edit_Profile/*Edit Profile*****/+" </A>");
}else
{
	//out.print("I have do not have right to edit my profile");
}
%> 
</TD>
</TR>
<TR>
<TD>
<%
if ( Integer.parseInt(grpRights.getFtrRightsByValue("MACCOUNT")) > 0)
{
	out.print("<A HREF = 'manageAccount.jsp'> "+LC.L_Manage_Acc/*Manage Account*****/+"</A>");
}else
{
	//out.print("I have do not have right to edit my profile");
	
}
%>

</TD>
</TR>
</TABLE>

<br>
<div STYLE = "margin-left:350px;position:absolute;border-style = solid">
<TABLE border = 0 width = 250>
<TR>
<TD>
<%
if ( Integer.parseInt(grpRights.getFtrRightsByValue("HNOTIFICATION")) > 0)
{
	out.print("<A HREF = 'notification.jsp'> "+MC.M_SignUp_NotificSrv/*Sign Up for the Notification Service*****/+"</A>");
}else
{
	//out.print("I have do not have right to edit my profile");
	
}

%>
</TD>
</TR>
<TR>
<TD>
<%
if ( Integer.parseInt(grpRights.getFtrRightsByValue("HPERSHOME")) > 0)
{
	out.print("<A HREF = 'personalize.jsp'> "+MC.M_Personalize_MyHomepage/*Personalize My HomePage*****/+"</A>");
}else
{
	//out.print("I have do not have right to edit my profile");
	
}

%>
</TD>
</TR>
<TR>
<TD>
<%
if ( Integer.parseInt(grpRights.getFtrRightsByValue("HLIBRARY")) > 0)
{
	out.print("<A HREF = library.jsp'> "+LC.L_My_Lib/*My Library*****/+"</A>");
}else
{
	//out.print("I have do not have right to edit my profile");
	
}

%>
</TD>
</TR>
</TABLE>
</div>

<br>
<TABLE border = 1 width = 200>
<TR>
<TD>
<%
if ( Integer.parseInt(grpRights.getFtrRightsByValue("HSTUDYSUM")) > 0)
{
	out.print("<A HREF = 'searchSummary.jsp'> "+LC.L_Search_StdSummary/*Search Study Summary*****/+"</A>");
}else
{
	//out.print("I have do not have right to edit my profile");
	
}

%>
</TD>
</TR>
</TABLE>

<br>
<i><%=MC.M_AcesTo_FlwStdPcol%><%--You have access to folowing Study Protocols*****--%>:</i>

<%! UserStudiesStateKeeper usk = null;
	ArrayList title;ArrayList num;ArrayList study; StringBuffer studyList;
	int counter = 0; int len = 0;String pagename = "studyMain.jsp";
%>

<%
	title = new ArrayList();
	study = new ArrayList();
	num = new ArrayList();
	studyList = new StringBuffer();

	user.setUserId(10);
	usk = user.getUserStudies();
	
	title = usk.getUTitles();
	study = usk.getUStudies();
	num = usk.getUStudyNumbers();
	len = study.size();
	

	studyList.append("<Table border = 1>");

	for(counter = 0;counter<len;counter++)
	{
		studyList.append("<TR><TD>");	 
		studyList.append("<A HREF = "+ pagename + "?id=" + study.get(counter) +"> [" + num.get(counter) + "] "+title.get(counter) +"</A>");	 
		studyList.append("</TR></TD>");	 
	}			
	studyList.append("</Table>");	
	out.print(studyList.toString());	

%>


<br>
<TABLE border = 1 width = 400>
<TR>
<TD>
<%
if ( Integer.parseInt(grpRights.getFtrRightsByValue("HCSTUDY")) > 0)
{
	out.print("<A HREF = 'addStudy.jsp'> "+MC.M_Docu_NewStdPcol/*Document a New Study Protocol*****/+"</A>");
}else
{
	//out.print("I have do not have right to edit my profile");
	
}

%>
</TD>
</TR>

<TR>
<TD>
<%
if ( Integer.parseInt(grpRights.getFtrRightsByValue("HDPROTOCOL")) > 0)
{
	out.print("<A HREF = 'addStudy.jsp'> "+LC.L_Deactivated_Pcol/*Deactivated Protocol*****/+"</A>");
}else
{
	//out.print("I have do not have right to edit my profile");
	
}

%>
</TD>
</TR>

</TABLE>

<%=LC.L_Got_Rights%><%--GOT Rights*****--%>



<%=LC.L_Update_GrpRights_Upper%><%--UPDATE GROUP RIGHTS*****--%>

<%grpRights.setId(2);%>
<%grpRights.setFtrRights("5");%>
<%grpRights.setFtrRights("6");%>
<%grpRights.setFtrRights("7");%>
<%grpRights.updateGrpRights();%>


<%=LC.L_Updation_Complete%><%--UPDATION COMPLETE*****--%>


<%HttpSession thisSession = request.getSession(true); %>
<%out.print(thisSession);%>
<%thisSession.putValue("GRights",grpRights);%>
<%=LC.L_Session_IsSet%><%--Session is set*****--%>

</BODY>
</HTML>


