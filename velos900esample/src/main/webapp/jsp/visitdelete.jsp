<%@page import="com.aithent.audittrail.reports.AuditUtils"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Visit_List%><%--Visit List*****--%></title>

<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>

<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import = "com.velos.eres.service.util.*"%>
<%@ page language = "java" import="com.velos.eres.business.common.*,com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<% String src;
	src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<BODY> 
<br>

<DIV class="formDefault" id="div1">
<% 
	String duration= "";
	String protocolId= "";
   	String calledFrom= "";
	String mode= "";
	String calStatus= "";
	String visitId="";	
	String calAssoc=request.getParameter("calassoc");
        calAssoc=(calAssoc==null)?"":calAssoc;

	
HttpSession tSession = request.getSession(true); 
 if (sessionmaint.isValidSession(tSession))	{ 	
		duration= request.getParameter("duration");
		protocolId= request.getParameter("protocolId");
   		calledFrom= request.getParameter("calledFrom");

		
		mode= request.getParameter("mode");
		calStatus= request.getParameter("calStatus");

		visitId=request.getParameter("visitId"); 	
		int visId=EJBUtil.stringToNum(visitId); 	

		int ret=0;
		
		String delMode=request.getParameter("delMode");
		if (delMode==null) {
			delMode="final";
			
%>
	<FORM name="deleteVisit" method="post" action="visitdelete.jsp" onSubmit="return validate(document.deleteVisit)">
	<br><br>
	
	<P class="defComments"><%=MC.M_EsignToProc_WithVisitDel%><%--Please enter e-Signature to proceed with Visit Delete.*****--%></P>
	<TABLE width="100%" cellspacing="0" cellpadding="0" >
	<tr>
	   <td width="40%">
		<%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT> 
	   </td>
	   <td width="60%">
		<input type="password" name="eSign" maxlength="8" SIZE=15 >
	   </td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
		 <button type="submit"><%=LC.L_Submit%></button>
		</td>
	</tr>
	</table>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="visitId" value="<%=visitId%>">
  	 <input type="hidden" name="duration" value="<%=duration%>">
  	 <input type="hidden" name="protocolId" value="<%=protocolId%>">
  	 <input type="hidden" name="calledFrom" value="<%=calledFrom%>">
  	 <input type="hidden" name="mode" value="<%=mode%>">
  	 <input type="hidden" name="calStatus" value="<%=calStatus%>">
  	 <input type="hidden" name="calassoc" value="<%=calAssoc%>">
	 
	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {
			
			
			ProtVisitDao protVisitDao = new ProtVisitDao();
			int childVisitCount = protVisitDao.getProtocolVisitChildCount(visId);
								
			if(childVisitCount > 0) {
			%>
			<br><br><br><br><br><br><br>
			<table width=100%>
				<tr>
				<td align=center>

				<p class = "successfulmsg">
				 <%=MC.M_VisitCntDel_ReferedMore%><%--Visit Cannot be deleted. It is referred by one or more child visits*****--%></p>
				</td>
				</tr>
                                 <tr>
				<td align=center>
					<A href="visitlist.jsp?srcmenu=<%=src%>&selectedTab=
					3&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>" type="submit"><%=LC.L_Back%></A>
				</td>		
				</tr>	 
				
			</table>			
			
			<%	
			
			}
			
			else {
			
			// Modified for INF-18183 ::: Raviesh
			ret = protVisitB.removeProtVisit(EJBUtil.stringToNum(protocolId), visId, calledFrom,AuditUtils.createArgs(session,"",LC.L_Cal_Lib));  

		    if (ret==-1) {%>

			<br><br><br><br><br><br>

			<table width=100%>
				<tr>
				<td align=center>

				<p class = "successfulmsg">
				 <%=MC.M_VisitAssocCal_CntDel%><%--The Visit is associated with a Calendar, cannot be deleted.*****--%>
				</p>
				</td>
				</tr>

				<tr height=20></tr>
				<tr>
				<td align=center>
					<A href="visitlist.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>" type="submit"><%=LC.L_Back%></A>
				</td>		
				</tr>		
			</table>				
		<%}else { %>
			<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%> </p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=visitlist.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>">
			<%}
		 }	
		} //end esign
	   } //end of delMode	
	 }//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
 <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/> 
</div>

</body>
</HTML>


