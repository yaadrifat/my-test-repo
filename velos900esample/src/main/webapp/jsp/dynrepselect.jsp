<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Add_FldToRpt%><%--Add Fields To Report*****--%> </title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<script>
function selectLab(formobj){
		num = formobj.numSel.value ;
		
		
	for (i=0;i<num;i++) 
		{
			if (num > 1)
			{
				if (formobj.selected[i].checked)
				{	formobj.action="dynrepselect.jsp";
					formobj.target="";
					formobj.submit();
					return true;
				}
			} 
			else 
			{
				if (formobj.selected.checked) 
				{	formobj.action="dynrepselect.jsp";
					formobj.target="";
					formobj.submit();
					return true;
				}	 
			}
	 }
	alert("<%=MC.M_Selc_Fld%>");/*alert("Please Select a Field.");*****/
	return false;
}
function deSelectLab(formobj){
 num = formobj.numDsel.value ;
 
		for (i=0;i<num;i++) 
		{
			if (num > 1) 
			{
				if (formobj.deselected[i].checked) 
				{
					formobj.action="dynrepselect.jsp";
					formobj.target="";
					formobj.submit();
					return true;
					
				}
			} 
			else 
			{
				if (formobj.deselected.checked) 
				{
					formobj.action="dynrepselect.jsp";
					formobj.target="";
					formobj.submit();
					return true;
				}	 
			}
	}
	alert("<%=MC.M_Deselect_Fld%>");/*alert("Please Deselect a Field.");*****/
	return false;

}
 function checkAll(formobj){
 	act="check";
 	totcount=formobj.totcount.value;
  if (formobj.All.value=="checked") act="uncheck" ;
   if (totcount==1){
   if (act=="uncheck") formobj.selected.checked =false ;
   else formobj.selected.checked =true ;}
   else {
     for (i=0;i<totcount;i++){
     if (act=="uncheck") formobj.selected[i].checked=false;
     else formobj.selected[i].checked=true;
     }
    }
    
    
    if (act=="uncheck") formobj.All.value="unchecked"; 
    else formobj.All.value="checked"; 
    
    
 }
  function uncheckAll(formobj){
 	act="check";
 	totcount=formobj.numDsel.value;
  if (formobj.UAll.value=="checked") act="uncheck" ;
   if (totcount==1){
   if (act=="uncheck") formobj.deselected.checked =false ;
   else formobj.deselected.checked =true ;}
   else {
     for (i=0;i<totcount;i++){
     if (act=="uncheck") formobj.deselected[i].checked=false;
     else formobj.deselected[i].checked=true;
     }
    }
    
    
    if (act=="uncheck") formobj.UAll.value="unchecked"; 
    else formobj.UAll.value="checked"; 
    
    
 }

function setMode(formobject, flag)
{
	if(flag=="R")
	{
		formobject.refreshFlag.value="captureIdsSel" ;
		
		num = formobject.numSel.value ; 
		for (i=0;i<num;i++) 
		{
			if (num > 1)
			{
				if (formobject.selected[i].checked)
				{
					return true;
				}
			} 
			else 
			{
				if (formobject.selected.checked) 
				{
					return true;
				}	 
			}
	 }
	alert("<%=MC.M_Selc_Fld%>");/*alert("Please Select a Field.");*****/
	return false;
		
		
		
	}
	if(flag=="L")
	{
		
		formobject.refreshFlag.value="captureIdsDSel" ;
		 num = formobject.numDsel.value ;
		for (i=0;i<num;i++) 
		{
			if (num > 1) 
			{
				if (formobject.deselected[i].checked) 
				{
					return true;
				}
			} 
			else 
			{
				if (formobject.deselected.checked) 
				{
					return true;
				}	 
			}
	}
	alert("<%=MC.M_Deselect_Fld%>");/*alert("Please Deselect a Field.");*****/
	return false;
		
		
		
		
	}
	if(flag=="S")
	{
		formobject.refreshFlag.value="submit" ;
	}
	if(flag=="SE")
	{
		formobject.mode.value="final" ;
	}function setMode(formobject, flag)
{
	if(flag=="R")
	{
		formobject.refreshFlag.value="captureIdsSel" ;
		
		num = formobject.numSel.value ; 
		for (i=0;i<num;i++) 
		{
			if (num > 1)
			{
				if (formobject.selected[i].checked)
				{
					return true;
				}
			} 
			else 
			{
				if (formobject.selected.checked) 
				{
					return true;
				}	 
			}
	 }
	alert("<%=MC.M_Selc_Fld%>");/*alert("Please Select a Field.");*****/
	return false;
		
		
		
	}
	if(flag=="L")
	{
		
		formobject.refreshFlag.value="captureIdsDSel" ;
		 num = formobject.numDsel.value ;
		for (i=0;i<num;i++) 
		{
			if (num > 1) 
			{
				if (formobject.deselected[i].checked) 
				{
					return true;
				}
			} 
			else 
			{
				if (formobject.deselected.checked) 
				{
					return true;
				}	 
			}
	}
	alert("<%=MC.M_Deselect_Fld%>");/*alert("Please Deselect a Field.");*****/
	return false;
		
		
		
		
	}
	if(flag=="S")
	{
		formobject.refreshFlag.value="submit" ;
	}
	if(flag=="SE")
	{
		formobject.mode.value="final" ;
	}
		
}
		
}


function  validate(formobj)
 {

         num = formobj.numDsel.value ;
	
	if (num==0){
	alert("<%=MC.M_Selc_FldBeforeSbmt%>");/*alert("Please Select Fields before Submitting");*****/
	return false;
	}
 	window.close();
}
  
</script>
<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="dynrepB" scope="page" class="com.velos.eres.web.dynrep.DynRepJB" />
<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%>

<%
	//String selected[]=request.getParameterValues("selected");
	
%>


<% 

   HttpSession tSession = request.getSession(true);
   int pageRight = 0;	
   GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
   //pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));

%>

<DIV class="popDefault" id="div1"> 

<%
	if (sessionmaint.isValidSession(tSession))
	{
		
		String src="";
		String fldName="",fldCol="",formType="",fldType="";
		String formId="",formName="";
		String groupNamePullDown="",refreshFlag="";
		ArrayList mapCols= null; 
		ArrayList mapOrigIds= null; 
		ArrayList mapDispNames= null;
		ArrayList mapFormTypes=null;
		ArrayList mapColTypes=null;
		ArrayList selectList=null;
		ArrayList selectTemp=null;
		ArrayList deselectList=null;
		int counter = 0;
		int len=0,index=-1;
		String from=request.getParameter("from");
		if (from==null) from="";
		String selected[] = request.getParameterValues("selected");
		String deselected[]=request.getParameterValues("deselected");
		//get previously selected labs list
		String selectAll[]= request.getParameterValues("select");
		if (selectAll!=null) selectList=EJBUtil.strArrToArrayList(selectAll);
		if (!(selected==null))	selectTemp=EJBUtil.strArrToArrayList(selected);
		if (!(selectTemp==null)){
		if (!(selectList==null)){
			for(int i=0;i<selectTemp.size();i++){
				selectList.add(selectTemp.get(i));
			}
			
		}else {
			selectList=selectTemp;
		}
		}
		if (!(deselected==null)) deselectList=EJBUtil.strArrToArrayList(deselected);
		//remove the deselected values from selected list
		if (!(selectList==null)) 
				{
				for(int i=0;i<selectList.size();i++)
				{	
					System.out.println("select"+selectList.get(i));
					
										
				}	
					    
					}		
			
		
			if (!(deselectList==null)) 
				{
				for(int i=0;i<deselectList.size();i++)
				{	
					System.out.println("deselect"+deselectList.get(i));
					index=selectList.indexOf(deselectList.get(i));
					System.out.println(index);
					if (index>-1)		selectList.remove(index);
										
				}	
					    
					}
				
		//String deselectedNames[] = request.getParameterValues("deselectedName");
		request.setAttribute("selectList",selectList);
		String uName = (String) tSession.getValue("userName");
		formId=request.getParameter("formId");
		formName=request.getParameter("formName");
		String accountId=(String)tSession.getAttribute("accountId");
		String mode= request.getParameter("mode");
		src= request.getParameter("srcmenu");
		System.out.println("src" + src);
		String selectedTab = request.getParameter("selectedTab");
		String study1=request.getParameter("study1");
		study1=(study1==null)?"":study1;
		if (formId == null ) 
		{
			formId="" ;
		}
		
		if (formName == null ) 
		{
			formName="" ;
		}
		
		//here we will get all the categories
		//DynRepDao dyndao=new DynRepDao();
		DynRepDao dyndao=dynrepB.getMapData(formId);
		//dyndao.getMapData(formId);
		dyndao.preProcessMap();
		mapCols= dyndao.getMapCols();
		mapOrigIds= dyndao.getMapOrigIds();
		mapDispNames= dyndao.getMapDispNames();
		mapFormTypes=dyndao.getMapFormTypes();
		mapColTypes=dyndao.getMapColTypes();
		System.out.println("length od mapformtypes in repslect"+mapFormTypes.size());
		if (mapFormTypes.size()>0)
		formType= (String)mapFormTypes.get(0);
		len=mapOrigIds.size();
		System.out.println("length of arraylist"+len);
		
%> 
    
			
			<table width="100%" border="0">	
				<tr>
				
				<td  width="50%" >
			  
				<!-- THE COLUMN FOR THE BROWSER -->		
			<!--<Form name="labselect" action="labdata.jsp?page=Demographics"  method="post" onsubmit="return validate(document.labselect)"  target="_opener">-->
			<%if (from.equals("pat")){%>
			<Form name="fldselect" action="dynreportpat.jsp"  method="post" onsubmit="return validate(document.fldselect);this.close();void(0);"  target="dynrep">
			<%} else{ %>
			<Form name="fldselect" action="dynreportstudy.jsp"  method="post" onsubmit="return validate(document.fldselect);this.close();void(0);"  target="dynrep">
			<%}%>
				<input type="hidden" name="refreshFlag" value=<%=refreshFlag%>>
				<input type="hidden" name="srcmenu" value=<%=src%>>
				<input type="hidden" name="mode" value="fldselect">
				<input type="hidden" name="formId" value="<%=formId%>">
				<input type="hidden" name="formName" value="<%=formName%>">
				<input type="hidden" name="formType" value="<%=formType%>">
				<input type="hidden" name="study1" value="<%=study1%>">
				<input type="hidden" name="selectedTab" value="<%=selectedTab%>">
				<input type="hidden" name="totcount" value="<%=len%>">
		
		
				<%if (selectList!=null){
		for(int i=0;i<selectList.size();i++){%>
			<Input type="hidden" name="select" value="<%=(String)selectList.get(i)%>">
		<%}}%>
		  	<br>
			 <br>
			 <br>
			 <br>			 			
			  <div style="overflow:auto;overflow-x:hidden; height:400; border:groove;">

				<table width="100%" cellspacing="1" cellpadding="0" border="0" align="">
				    <tr> 
				    
						<th width="5%"><p align="left"><input type="checkbox" name="All" value="" onClick="checkAll(document.fldselect)"></p></th>
						<th width="33%"><%=LC.L_Fld_Name%><%--Field Name*****--%> </th>
						
						
					</tr>

		<%
										
					for(counter = 0;counter<len;counter++)
					{
						
						
						mode="captureIds" ;
												
						fldName = ((mapDispNames.get(counter)) == null)?"-":(mapDispNames.get(counter)).toString();
						fldCol =  ((mapCols.get(counter)) == null)?"-":(mapCols.get(counter)).toString();
						fldType=((mapColTypes.get(counter)) == null)?"":(mapColTypes.get(counter)).toString();	
						String selFields = StringUtil.encodeString(fldName+"[VEL]"+fldCol+"[VEL]"+fldType);
						
						if ((counter%2)==0) 
						{

						%>

					      	<tr class="browserEvenRow"> 

						<%}else
						 { %>

				      		<tr class="browserOddRow"> 

						<%}  %>	
					
						
						
						<td width="5%"><input type="checkbox" name="selected" value="<%=selFields%>" > </td>
						<td><%=fldName%></td>
							
			    	 	</tr>
						
					<%
			}//for loop 		
		
					
					%>
					
				
				</table>
				
				<input type="hidden" name="numSel" value="<%=len%>" >
			  </div>
				
			</td>	<!-- END OF THE TD OF THE FIELD BROWSER -->
		  
			
			<td width="5%"> <!-- TD FOR THE ARROWS BMP -->
			
			<A href="#" onClick="selectLab(document.fldselect)"><img src="../images/jpg/formright.gif"  align="absmiddle"   border="0"></img></A>
				<!--<input  type="image" src="../images/jpg/formright.gif" onClick="" align="absmiddle"   border="0">-->
				<br>
				<br>
				<A href="#" onClick="deSelectLab(document.fldselect)"><img src="../images/jpg/formleft.gif"  align="absmiddle"   border="0"></img></A>
				<!--<input  type="image" src="../images/jpg/formleft.gif" onClick="" align="absmiddle"   border="0">-->
			</td><!-- TD FOR THE ARROWS BMP-->
			
			<td width="50%">
			<br><br><br><br>

			<div style="overflow:auto;overflow-x:hidden; height:400;border:groove;" >			 
			<table width="100%" cellspacing="1" cellpadding="0" border="0" align="left"> 
			    <tr> 
						<th width="5%"><p align="left"><input type="checkbox" name="UAll" value="" onClick="uncheckAll(document.fldselect)"></p></th>
					        <th width="33%"> <%=LC.L_Fld_Name%><%--Field Name*****--%></th>
					</tr>
					<%
					int cnt=0;
					String deselectedName = "";
					String tmp = "";
					if (!(selectList==null)) 
					{
					   //cnt =deselected.length;
					   cnt=selectList.size();
					}
					String deselId = "";
					String deselName = "",deselType="";
					String deselUniqId = "",deselCatId="";
					String deselNciId="",deselGradeCalc="";
					int strlen  =0 ;
					int secondpos  = 0 ;
					int firstpos  = 0 ,thirdpos=0;
					String temp="";
					
					for(counter = 0;counter<cnt;counter++)
					{
						temp=StringUtil.decodeString(((String)selectList.get(counter)));
						
						StringTokenizer st=new StringTokenizer(temp,"[VEL]");
						 strlen = (temp).length();		
						 firstpos = (temp).indexOf("[VEL]",1);	
						 deselName = (temp).substring(0,firstpos);	
						 //deselId = (temp).substring(firstpos+5,strlen);
						 secondpos=(temp).indexOf("[VEL]",firstpos+5);
						 deselId = (temp).substring(firstpos+5,secondpos);
						 deselType = (temp).substring(secondpos+5,strlen);
						 /*thirdpos=(temp).indexOf(";",secondpos+1);
						 deselUniqId=(temp).substring(secondpos+1,thirdpos);
						 deselCatId= (temp).substring(thirdpos+1,strlen);*/
						// deselName=st.nextToken();
						 //deselId=st.nextToken();
						 
						 						 
						String selFields =StringUtil.encodeString(deselName.trim()+"[VEL]"+deselId.trim()+"[VEL]"+deselType.trim());  
						%>
						<input type="hidden" name="deselectedAll" value="<%=selFields%>" >
						<%
						if ((counter%2)==0) 
						{
								
						%>

					      	<tr class="browserEvenRow"> 

						<%} else
						 { %>

				      		<tr class="browserOddRow"> 

						<%}  %>	
						<td width="5%"><input type="checkbox" name="deselected" value="<%=selFields%>"> </td>
						<td> <%=deselName%> </A></td>
						
					<%}%>	
					
			</table>		
			
			</div>
			<input type="hidden" name="numDsel" value="<%=cnt%>" >
			</td>	<!-- END OF THE TD FOR THE ADDED FEILDS -->			
		</tr>
		</table>	
		
				
				<table width="50%" border="0" align="right">
				<tr>
					<TD>		    
					<td width="10%"><button onClick="window.close()"><%=LC.L_Close%></button></td>
					<td width="10%"><input type="image" src="../images/jpg/Submit.gif" align="absmiddle" border="0" onClick="setMode(document.fldselect,'S')" ></td>				</tr>	
				</table>	
			</Form>

	

		<%



		 

	}//end of if body for session

	else

	{

	%>

		<jsp:include page="timeout.html" flush="true"/>

	<%

	}

	%>

	

</div>

 

 


</body>

</html>


