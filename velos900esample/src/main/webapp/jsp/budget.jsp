<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Budget_Open%><%--Budget >> Open*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />

<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB"/>


<%@ page import="com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.user.UserJB,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.business.common.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
   	<jsp:include page="include.jsp" flush="true"/>
   	<jsp:include page="ui-include.jsp" flush="true"></jsp:include>


<SCRIPT Language="javascript">

function orgnTest(formobj,var1) {
if (document.all) {
	if(var1==1)
	{
	formobj.orgId.style.visibility="hidden";
	document.all["SO"].style.visibility="hidden";
	} else
	{
	formobj.orgId.style.visibility="visible";
	document.all["SO"].style.visibility="visible";
	}
}
}

function setflag(formobj,var1)
 {
 formobj.hidOrgFlag.value = var1;
 if (var1 == "0") {
 	if (document.getElementById) {
 	  formobj.orgId.value = ""; }
 	else {
 		formobj.orgId.selectedIndex = 0 ;

 		 }


 }
}

function openwin1() {
      window.open("usersearchdetails.jsp?fname=&lname=&from=budget","usersearchdetails","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=800,height=450,left=100,top=200")
;}



 function  validate(formobj){
   var budScopeVal ;

	if (formobj.mode.value == "M")
	{
    		budScopeVal = formobj.budScope.value;

	}

    if (!(validate_col('Budget Name',formobj.budgetName))) return false



    if ( formobj.budgetDesc.value != null && formobj.budgetDesc.value.length > 4000)
    {
    	alert("<%=MC.M_CommentsExceed_MaxLimit%>");/*alert("Comments exceed the maximum limit of 4000 characters");*****/
    	formobj.budgetDesc.focus();
    	return false;
    }

    if (!(validate_col('Budget Created By',formobj.budgetCreatedByName))) return false
	if (!(validate_col('Currency',formobj.currencyId))) return false

	if(formobj.mode.value != "M")
		if (!(validate_col('Budget Template',formobj.budgetTemplate))) return false

	if(formobj.mode.value == "N") {
		for(var iX=0; iX<formobj.orgFlag.length; iX++) {
			if (formobj.orgFlag[iX].checked) {
				if (formobj.orgFlag[iX].value == "1") {
			    	 if (!(validate_col('orgId',formobj.orgId))) return false
				}
			}
		}
	}

	if (formobj.hidOrgFlag.value == "1")
	{
    	if (formobj.includedIn.value!="P"){
    	 if (!(validate_col('orgId',formobj.orgId))) return false
		}

    if (formobj.mode.value == "M")
	  {
		oldorg = formobj.oldsite.value;

		if (document.getElementById) {
 	         	neworg = formobj.orgId.value;
 	   } else {
 	      neworg=formobj.orgId[formobj.orgId.selectedIndex].value;

 	   }


		if (budScopeVal == "O")
		{
		       if (!(oldorg==neworg)) {
           		  if(!(confirm("<%=MC.M_ChgBgtOrg_Cont%> ")))/*if(!(confirm("You are changing this budget's Organization. If you continue, access rights will apply to all users within the new organization selected. The users of previous organization will no longer have access to this budget.\n\nAre you sure you would like to proceed? ")))*****/
     				{
                		return false;
             		}
			    }
		}
		}

	}

	if (formobj.hidOrgFlag.value == "0")
	{

		 i = formobj.orgId.options.selectedIndex;

		 value =formobj.orgId.options[i].value;

    	if (value == '' || value==null)
		{
		}
		else
		{
    		alert("<%=MC.M_BgtIsOrgNonspec_CntSelOrg%>");/*alert("Budget is Organization non-specific. You can not select an Organization");*****/
			return false;
		}

	}

    /////
    if (formobj.mode.value == "M")
	{
    	if (budScopeVal == "S")
		{
  		   oldstudy = formobj.oldstudy.value;
     	   i = formobj.budgetStudyId.options.selectedIndex;
		   newstudy = formobj.budgetStudyId.options[i].value;

	       if (!(oldstudy==newstudy)) {
           		  if(!(confirm("<%=MC.M_StdBgtChg_SureProc%> ")))/*if(!(confirm("You are changing this budget's <%=LC.Std_Study%>. If you continue, access rights will apply to all users in the Team of the new <%=LC.Std_Study%> selected. The users of previous <%=LC.Std_Study%> will no longer have access to this budget.\n\nAre you sure you would like to proceed? ")))*****/
     				{
                		return false;
             		}
			    }
		}
    }

	//////

	if (!(validate_col('e-Signature',formobj.eSign))) return false

	/*if(formobj.budgetStatus.value=='F'){

		if(!(confirm("Selecting the Freeze option will not allow further changes to this budget. Are you sure you would like to proceed?"))){
		return false;
		}
	}

	if(formobj.budgetStatus.value=='T'){

		if(!(confirm("Selecting the Template option will not allow further changes to this budget. Are you sure you would like to proceed?"))){
		return false;
		}
	}*/

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }

   }

</SCRIPT>



<jsp:useBean id="budgetB" scope="page" class="com.velos.esch.web.budget.BudgetJB"/>
<jsp:useBean id="budget" scope="page" class="com.velos.esch.web.budget.BudgetJB"/>
<jsp:useBean id="bgtcalB" scope="page" class="com.velos.esch.web.budgetcal.BudgetcalJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="bgtcaldao" scope="request" class="com.velos.esch.business.common.BudgetcalDao"/>
<jsp:useBean id="schcodedao" scope="request" class="com.velos.esch.business.common.SchCodeDao"/>

<% String src = request.getParameter("srcmenu");
	String mode =   request.getParameter("mode");

// includedIn will be passed as P when this page is included in Protocol Calendar tabs to show the default budget

String includedIn = request.getParameter("includedIn");
if(StringUtil.isEmpty(includedIn))
{
 includedIn = "";
}

//*** Added specifically for combined budget
String from = request.getParameter("from");
if(StringUtil.isEmpty(from))
{
	from = "";
}
//***

	  int ienet = 2;
  int proceed = 1;
  String agent1 ;
  agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
%>

<% if ( ! includedIn.equals("P")) { %>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<%}%>

  <%

	HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))
		{

	String defUserGroup = (String) tSession.getAttribute("defUserGroup");

		String uName = (String) tSession.getValue("userName");
		String budgetId= request.getParameter("budgetId");
		int bgtId=EJBUtil.stringToNum(budgetId);
		String fromPage = request.getParameter("fromPage");
		String userIdFromSession = (String) tSession.getValue("userId");

		///////////////////by salil
		String modRight = (String) tSession.getValue("modRight");
		int modlen = modRight.length();
		CtrlDao acmod = new CtrlDao();
		acmod.getControlValues("module");
		ArrayList acmodfeature =  acmod.getCValue();
		ArrayList acmodftrSeq = acmod.getCSeq();
		int budSeq = acmodfeature.indexOf("MODBUD");
		budSeq = ((Integer) acmodftrSeq.get(budSeq)).intValue();
		char budAppRight = modRight.charAt(budSeq - 1);
	String budgetStudyId="";

		int pageRight = 0;
		int saveAstemplateRight = 0;
//SV, 8/26/04, part of the fix for budget rights, bug #1679. When in edit mode, look at the rights
// pushed by the budget tab (patientbudget.jsp, studybudget.jsp). Otherwise, look at group rights.

		String fromRt = request.getParameter("fromRt");
		if (fromRt==null || fromRt.equals("null")) fromRt="";
		%>
		<Input type="hidden" name="fromRt" value=<%=fromRt%> />
		<%	
		if (fromRt.equals("")){
			//SM:Bug#5269, #5278
			if(mode.equals("N")){
				//In new mode check group rights
				GrpRightsJB bRights = (GrpRightsJB) tSession.getAttribute("GRights");		
				pageRight = Integer.parseInt(bRights.getFtrRightsByValue("BUDGT"));
			}else{
				//In modify mode check budget rights
				GrpRightsJB bRights = (GrpRightsJB) tSession.getAttribute("BRights");		
				pageRight = Integer.parseInt(bRights.getFtrRightsByValue("BGTDET"));
			}
		} else{
			pageRight = EJBUtil.stringToNum(fromRt);
		}	
	  		
 if(mode.equals("M")){
	//get group right for 'save as template'
	GrpRightsJB grpRightsForTemp = (GrpRightsJB) tSession.getAttribute("GRights");
	saveAstemplateRight = Integer.parseInt(grpRightsForTemp.getFtrRightsByValue("BUDTEMPL"));

 }else{
	//for old data, we may get a '6' where budget 'edit' was allocated before,
	if	 (pageRight == 6)
	{
		pageRight = 4; //make it only view right	as there is no edit right now
	}
	if (pageRight == 5)
	{
		pageRight = 7;
	}

	saveAstemplateRight = 0; //in new mode no right for template

	//end of change for old data
}

if(pageRight > 0){%>
<body onload="javascript:if (typeof(document.budget.budgetName)!='undefined') document.budget.budgetName.focus();">
<%}else{%>
<body>
<%}
//SV, 8/26/04, part of fix for bug #1679, split up "if", so we could the tabs even when we have only "view" rights.
//    	if ( (String.valueOf(budAppRight).compareTo("1") == 0) && (pageRight >= 4 ))	{
    	if (String.valueOf(budAppRight).compareTo("1") == 0) {
		//////////////////end by salil
		//if(mode.equals("M"))
		//{
		%>

		<%
		//}
		String budgetname="";
		String budgetver="";
		String budgetdesc="";
		String budgetCreatedByName="";


		String budgetCreatedById="";
		String sAcc= (String) tSession.getValue("accountId");
		String cd1="";
		int counter,siteId=0;
		String currencyId="";
		String budgetTemplate="";
		String calStatus="";
		String orgFlag="";
		String orgId="";

		String selected="";
		StringBuffer mainStr = new StringBuffer();
		String bgtCalFlag="";
		String budgetStatus="";
		String budScope = "";
		String desc = "";
		String templateType = "";
		String bgtStatDesc ="";
		String codeSubType="";
		String budgetCodelistStatus = "";

		SchCodeDao schCode;
		ArrayList codeSubTypes;

		int defaultStatus = 0;

		int currId = 0;
//SV,8/26/04, get the template type ahead, so we could include budgettabs.jsp here.
			budgetB.setBudgetId(bgtId);
			budgetB.getBudgetDetails();
			budgetStudyId=budgetB.getBudgetStudyId();

			budgetTemplate=budgetB.getBudgetTemplate();

			BudgetDao bgtDao = new BudgetDao();
			bgtDao = budget.getBgtTemplateSubType(EJBUtil.stringToNum(budgetTemplate),bgtId);
			codeSubTypes = bgtDao.getBgtTemplates();
			ArrayList descs = bgtDao.getBgtDescs();
			ArrayList templateTypes = bgtDao.getBgtTypes();

			if(codeSubTypes.size()>0)
			{
				codeSubType = codeSubTypes.get(0).toString().trim();
				desc = descs.get(0).toString();
				templateType= templateTypes.get(0).toString();

			}






%>


<%
	String divClass= "";

 if ( ! includedIn.equals("P")) { %>

	<div class="tabDefTopN" id="div1">
	 			<jsp:include page="budgettabs.jsp" flush="true">
				<jsp:param name="budgetTemplate" value="<%=templateType%>"/>
				<jsp:param name="mode" value="<%=mode%>"/>
				<jsp:param name="studyId" value="<%=budgetStudyId%>"/>

				<jsp:param name = "selectedTab" value="1"/>
				</jsp:include>

	</div>

<%
	divClass = "tabDefBotN";
} else
{
	divClass="popDefault";
}

%>


<div class="<%=divClass%>" id="div2">

<%

// SV, 8/26/04, here is the if split up from above.
	if ( pageRight >= 4 )	{

		BudgetDao bgtdao = new BudgetDao();
		boolean val = bgtdao.getCodeValAndBgtTemplateIds(EJBUtil.stringToNum(userIdFromSession),"budget_template");

		ArrayList arrFlgSeparatedIds = bgtdao.getBgtIdWithFlgs();

		ArrayList arrTemplateDesc = bgtdao.getBgtDescs();

		ArrayList arrHideUnhides = bgtdao.getCodelstHideUnhide(); //JM: 08Sep2010, #3356-part(14)

		System.out.println("arrFlgSeparatedIds" + arrFlgSeparatedIds + " arrTemplateDesc " +arrTemplateDesc );


		String templatePullDown =  "";
		StringBuffer templateDD=new StringBuffer();

     	SiteDao siteDao = new SiteDao();
     	siteDao.getSiteValues(EJBUtil.stringToNum(sAcc));


		SchCodeDao cd = new SchCodeDao();
	    cd.getCodeValues("currency", EJBUtil.stringToNum(sAcc));


		String acc = (String) tSession.getValue("accountId");
		StringBuffer orgn = new StringBuffer();
		String userSiteId = "";
		int primSiteId = 0;
		int userId = 0;
		String siteName = "";

		SchCodeDao schCodeSt = new SchCodeDao();
		//schCodeSt.getCodeValues("budget_stat" );
		//schCodeSt.setCType("budget_stat");
	    

		if (mode.equals("N"))
		{
			defaultStatus = schCodeSt.getCodeId("budget_stat", "W");
			budgetCodelistStatus = String.valueOf(defaultStatus);
		}

		if(mode.equals("N")){
		userId = EJBUtil.stringToNum(userIdFromSession);
			userB.setUserId(userId);
			userB.getUserDetails();

			userSiteId = userB.getUserSiteId();
		    userSiteId=(userSiteId==null)?"":userSiteId;
			primSiteId= EJBUtil.stringToNum(userSiteId);

			siteB.setSiteId(primSiteId);
			siteB.getSiteDetails();

		siteName = siteB.getSiteName();

		int cnt =0 ;
		orgn.append("<SELECT  Size='1' NAME=orgId >") ;
		if(!userSiteId.equals("")){
			orgn.append("<OPTION value="+primSiteId+" selected>"+siteName +"</OPTION>");

		}
		else{
			orgn.append("<OPTION value=''  selected >"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
			cnt ++;
		}

		for (counter = 0; counter <= (siteDao.getSiteIds()).size() -1 ; counter++){
     		siteId = ((Integer)((siteDao.getSiteIds()).get(counter))).intValue();
			if(siteId!=primSiteId)
     		orgn.append("<OPTION value = "+ siteId +">" + (siteDao.getSiteNames()).get(counter) + "</OPTION>");
     	}
		if(cnt==0){
		 orgn.append("<OPTION value='' >"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
		}
		 orgn.append("</SELECT>");
		}

		///////////////////study



		int studyId=0;
		StringBuffer study=new StringBuffer();

		StudyDao studyDao = new StudyDao();


		studyDao.getBudgetStudyValuesForAccount(sAcc);
		if(mode.equals("N")){

			study.append("<SELECT NAME=budgetStudyId STYLE='WIDTH:177px'>") ;
			study.append("<OPTION value=''  selected>"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");

			for (counter = 0; counter <= (studyDao.getStudyNumbers()).size() -1 ; counter++){
			studyId = ((Integer)((studyDao.getStudyIds()).get(counter))).intValue();


			study.append("<OPTION value = "+ studyId +"  >" + (studyDao.getStudyNumbers()).get(counter)+ "</OPTION>");
		}
		study.append("</SELECT>");

		//JM: 08Sep2010, #3356-part(14)
		//templatePullDown =  EJBUtil.createPullDownWithStr("budgetTemplate","", arrFlgSeparatedIds,arrTemplateDesc);

		templatePullDown =  EJBUtil.createPullDownWithStrIfHidden("budgetTemplate","", arrFlgSeparatedIds,arrTemplateDesc, arrHideUnhides);
		}


//	out.println(" budgetTemplate="+ templateType);



		if (mode.equals("M")) {
//done above			budgetB.setBudgetId(bgtId);
//done above.			budgetB.getBudgetDetails();
			budgetname=budgetB.getBudgetName();

			budgetver=budgetB.getBudgetVersion();
			budgetver = (   budgetver  == null      )?"":(  budgetver ) ;

			budgetdesc=budgetB.getBudgetdesc();
			budgetdesc = (   budgetdesc  == null      )?"":(  budgetdesc ) ;


			budgetCreatedById=budgetB.getBudgetCreator();
    		budScope =  budgetB.getBudgetRScope();

			userB.setUserId(EJBUtil.stringToNum(budgetCreatedById));
	   		userB.getUserDetails();
		    budgetCreatedByName = userB.getUserFirstName() + " " + userB.getUserLastName();


			currencyId=budgetB.getBudgetCurrency();
			cd1 = cd.toPullDown("currencyId",EJBUtil.stringToNum(currencyId));


			budgetStatus=budgetB.getBudgetStatus();

			budgetCodelistStatus=budgetB.getBudgetCodeListStatus();
			bgtStatDesc = schcodedao.getCodeDescription(EJBUtil.stringToNum(budgetCodelistStatus));

			//commented by sonia, to implement codelist dropdown for status
    		/* if(budgetStatus.equals("F")) {
    		bgtStatDesc = "Freeze";
    		} else if(budgetStatus.equals("W")) {
    		bgtStatDesc = "Work in Progress";
    		}else if(budgetStatus.equals("T")) {
    		bgtStatDesc = "Template";
    		}*/



			calStatus=budgetB.getBudgetCFlag();
			orgFlag=budgetB.getBudgetSFlag();
			orgId=budgetB.getBudgetSiteId();

			bgtCalFlag=budgetB.getBudgetCFlag();




			/////////////////////

			orgn.append("<SELECT NAME=orgId >") ;
			orgn.append("<OPTION value='' >"+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");
			for (counter = 0; counter <= (siteDao.getSiteIds()).size() -1 ; counter++){

			siteId = ((Integer)((siteDao.getSiteIds()).get(counter))).intValue();

				if(siteId==(EJBUtil.stringToNum(orgId)))
				{
				  selected= LC.L_Selected_Lower;/*selected="selected";*****/
				}
				else{
				selected="";
				}


     		orgn.append("<OPTION value = "+ siteId + " " + selected +">" + (siteDao.getSiteNames()).get(counter) + "</OPTION>");
     		}
	     	orgn.append("</SELECT>");



			study.append("<SELECT NAME=budgetStudyId  STYLE='WIDTH:177px' >") ;
			study.append("<OPTION value='' > "+LC.L_Select_AnOption/*Select an Option*****/+"</OPTION>");

			for (counter = 0; counter <= (studyDao.getStudyNumbers()).size() -1 ; counter++){
			studyId = ((Integer)((studyDao.getStudyIds()).get(counter))).intValue();

			if(studyId==EJBUtil.stringToNum(budgetStudyId))
			{
			selected= LC.L_Selected_Lower;/*selected="selected";*****/
			} else
			selected="";


			study.append("<OPTION value = "+ studyId + " " + selected+ ">" + (studyDao.getStudyNumbers()).get(counter) + "</OPTION>");
			}
			study.append("</SELECT>");


		/////////////////////////////////////////////////////

		     //templatePullDown =  EJBUtil.createPullDownWithStr("budgetTemplate",templateDesc, arrFlgSeparatedIds,arrTemplateDesc);
		       /*   String templateId="";

		        templateDD.append("<SELECT NAME=budgetTemplate>") ;
			templateDD.append("<OPTION value='' > Select an Option</OPTION>");
			for (counter = 0; counter <= arrFlgSeparatedIds.size() -1 ; counter++){
			templateId=(String)arrFlgSeparatedIds.get(counter);
			if(templateId.equals(budgetTemplate+":"+templateType))
			{
			selected="selected";
			} else
			selected="";

			templateDD.append("<OPTION value = "+ templateId + " " + selected+ ">" + (String)(arrTemplateDesc).get(counter) + "</OPTION>");
			}

			templateDD.append("</SELECT>");*/

		////////////////////////////////////////////////
			}else{


			userB.setUserId(EJBUtil.stringToNum(userIdFromSession));
		    userB.getUserDetails();
			budgetCreatedByName = userB.getUserFirstName() + " " + userB.getUserLastName();
			budgetCreatedById=userIdFromSession;
			SchCodeDao schD = new SchCodeDao();
			currId = schD.getCodePKByDesc("currency","US Dollars","$" );
			cd1 = cd.toPullDown("currencyId",currId);
			///////////////
%>

<%
				//////////////
			}

		String schCodeStDD="";
		
		//KM-D-FIN8
		String roleCodePk="";
		
	    if (! StringUtil.isEmpty(budgetStudyId) && EJBUtil.stringToNum(budgetStudyId) > 0) 
		{
			ArrayList tId = new ArrayList();
			
			TeamDao teamDao = new TeamDao();
			teamDao.getTeamRights(EJBUtil.stringToNum(budgetStudyId),EJBUtil.stringToNum(userIdFromSession));
			tId = teamDao.getTeamIds();
							
			if (tId != null && tId.size() > 0)
			{
				ArrayList arRoles = new ArrayList();
				arRoles = teamDao.getTeamRoleIds();		
				
				if (arRoles != null && arRoles.size() >0 )	
				{
					roleCodePk = (String) arRoles.get(0);
					
					if (StringUtil.isEmpty(roleCodePk))
					{
						roleCodePk="";
					}
				}	
				else
				{
					roleCodePk ="";
				}
				
			}	
			else
			{
				roleCodePk ="";
			}
		} else
			{			
			  roleCodePk ="";
			 
			}  
		
		

		schCodeSt.setForGroup(defUserGroup);
		schCodeSt.getCodeValuesForStudyRole("budget_stat",roleCodePk);
		
		if (saveAstemplateRight < 6)
	    {
	    	//remove template entry from codelist arraylists for status

	    	schCodeSt.removeForSubType("T");

	    }

		
		schCodeStDD = schCodeSt.toPullDown("dBudgetCodeStatus",EJBUtil.stringToNum(budgetCodelistStatus));
%>


		<Form name="budget" id="budgetForm" method="post" action="updatebudget.jsp"  onSubmit="if (validate(document.budget)==false) {setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">
<%
if (pageRight == 4) {
%>
   <P class = "defComments"><FONT class="Mandatory"><%=MC.M_YouOnly_ViewPerm%><%--You have only View permission*****--%></Font></P>
<%}%>

		 <table width="100%" cellspacing="2" cellpadding="2" border="0">
			<%if ((!(budgetStatus ==null)) && (budgetStatus.equals("F") || budgetStatus.equals("T"))&&(mode.equals("M"))) { Object[] arguments = {bgtStatDesc};%>
          	<P class = "defComments"><FONT class="Mandatory"><%=VelosResourceBundle.getMessageString("M_BgtStat_CntChgBgt",arguments)%><%--Budget Status is '<%=bgtStatDesc%>'. You cannot make any changes to the budget.*****--%></Font></P>
			<%}%>			
			</td></tr>
		</table>

		 <table width="100%" cellspacing="2" cellpadding="2">
	     	<tr height="5"><td colspan="6">&nbsp;</td></tr>
	     	<tr >
      	 		<td width="16%"> <%=LC.L_Budget_Name%><%--Budget Name*****--%> <FONT class="Mandatory">* </FONT> </td>
        		<td width="30%"> <input type="text" name="budgetName" size = 40   MAXLENGTH = 100 	value="<%=budgetname%>"> </td>
        		<td colspan="3"></td>
		    </tr>
			<tr>
			     <td > &nbsp; &nbsp; &nbsp; <%=LC.L_Version_Number%><%--Version Number*****--%> </td>
	    	    <td>
	        	 <input type="text" name="budgetVer" size = 40 MAXLENGTH = 200 	value="<%=budgetver%>">
		        </td>
		        <td colspan="3">&nbsp;</td>
    		</tr>
		  	<tr>
		      	<td >  &nbsp; &nbsp; &nbsp; <%=MC.M_Comments_Limit4000Chars%><%--Comments <FONT class="Mandatory"><br>(Limit 4000 characters)</FONT>*****--%> </td>
	        	<td colspan="2">
				<TEXTAREA  name="budgetDesc" rows=3 cols=50><%=budgetdesc%></TEXTAREA>
				</td>
		        <td colspan="3">&nbsp;</td>
	    	 </tr>

	     <tr>
		      	 <td> <%=LC.L_Created_By%><%--Created By*****--%> <FONT class="Mandatory">*</FONT></td>
        		<td>
	          	<input type="text" name="budgetCreatedByName" size = 20  MAXLENGTH = 50 	value="<%=budgetCreatedByName%>" readonly >
			  	<input type="hidden" name="budgetCreatedById" value="<%=budgetCreatedById%>">
               	<A HREF=# onClick=openwin1() ><%=LC.L_Select_User%><%--Select User*****--%></A>
			     </td>
		        <td colspan="3">&nbsp;</td>
		 </tr>
		 <tr>
	    	    <td><%=LC.L_Bgt_Curr%><%--Budget Currency*****--%> <FONT class="Mandatory">* </FONT></td>

<%--				<td><%if(mode.equals("M")){ out.println(cd1);} else{out.println(mainStr);}%></td>
--%>
				<td><%=cd1%></td>
		        <td colspan="2">&nbsp;</td>
		 </tr>

		 <tr>
      	 <td ><%=LC.L_Bgt_Template%><%--Budget Template*****--%> <FONT class="Mandatory">* </FONT> </td>


		  <% if (mode.equals("N")) {%>
			<td>
			  <%=templatePullDown%>

			</td>
			
			<%	   } else { //mode check %>

				<td><%=desc%><input type="hidden" name="budgetTemplate" value="<%=budgetTemplate+":"+templateType%>"/></td>
		        <td colspan="3">&nbsp;</td>
			<% }%>
		  </select>
		  </tr>
		 <tr>
	        <td> <%=LC.L_Bgt_Status%><%--Budget Status*****--%> </td>

			<td>
		 		<%= schCodeStDD %>
	 	 </td>
		        <td colspan="3">&nbsp;</td>
		  </tr>

		  <tr><td colspan="5">&nbsp;</td></tr>
		<tr>
			<td>&nbsp;</td>
	    <td >
		<%
		if (! includedIn.equals("P"))
			{
				if(mode.equals("N")) {

					  %>
			    <Input type="radio" name="orgFlag" value="0"  onClick="setflag(document.budget,0)"> <%=MC.M_Bgt_OrgNonspecific%><%--Budget is Organization non-specific*****--%>
				<input type="hidden" name="hidOrgFlag" value=0>
			    </td>
			    <tr>
			    	<td>&nbsp;</td>
			    <td>
			    <Input type="radio" name="orgFlag" value="1" checked  onClick="setflag(document.budget,1)" > <%=MC.M_BgtSpecific_ToOrg%><%--Budget is specific to Organization*****--%>:
				<input type="hidden" name="hidOrgFlag" value=1>
			    </td>
				<%}else{ %>
				<input type="hidden" name="hidOrgFlag" value=<%=orgFlag%>>
				<Input type="radio" name="orgFlag" value="0" onClick="setflag(document.budget,0)" <% if( orgFlag.equals("0")){out.println("checked=true");} %> > <%=MC.M_Bgt_OrgNonspecific%><%--Budget is Organization non-specific*****--%>
			    </td>
			    <tr>
			    	<td>&nbsp;</td>
			    <td>
			    <Input type="radio" name="orgFlag" value="1"  onClick="setflag(document.budget,1)" <% if( orgFlag.equals("1")){out.println("checked=true"); }%>> <%=MC.M_BgtSpecific_ToOrg%><%--Budget is specific to Organization*****--%>:
			    </td>

		<%  }
		} else
		{ %>
		 	<%=MC.M_BgtSpecific_ToOrg%><%--Budget is specific to Organization*****--%>:
		 		<input type="hidden" name="hidOrgFlag" value=<%=orgFlag%>>
		 		<input type="hidden" name="orgFlag" value=1>
		 		<div style="visibility:hidden;"><%=study%></div>


		<%
		}%>

		<td>
<!-- commented by Vishal because of problem in netscape 6.2-->
	    <!--		<Font class=comments>-->
	      	<Font class=comments>
	      	 <%=orgn%></Font>
	      	<!--</Font>-->



			 </td>

	    </tr>
		<tr><td>&nbsp;</td></tr>
		<% if (! includedIn.equals("P"))
			{
		%>
				<tr>
					<td>&nbsp;</td>
			    <td >
			     <%=MC.M_AssocBgt_ToStd%><%--Associate Budget to the following <%=LC.Std_Study%>*****--%>:
			    </td>

				<td ><Font class=comments>
			      	<%=study%></Font>
		      	</td>
			    </tr>
			<tr><td>&nbsp;</td></tr>
		<% } %>
		</table>


		<%-- if(mode.equals("N")) {%>
		 <table width="600" cellspacing="2" cellpadding="2">
		<tr>
			<td>
			<P class="defComments">If Budget Template is <%=LC.Pat_Patient%> Budget</P>
		    </td>
			<td> &nbsp </td>
		</tr>

		<tr>
	    <td>

	    <Input type="radio" name="bgtCalFlag" value="0" <% out.println("checked=true"); %> > Create a budget without using any existing protocol calendar template
	    </td>
		</tr>
	    <tr>
	    <td>
	    <Input type="radio" name="bgtCalFlag" value="1" > Create a budget from an existing protocol calendar
	    </td>
		</tr>
		<%}else{ %>

		<tr>
		<td>
		<Input type="radio" name="bgtCalFlag" value="0" <% if( bgtCalFlag.equals("0")){out.println("checked=true");} %> > Create a budget without using any existing protocol calendar template
	    </td>
		</tr>

	    <tr>
	    <td>
	    <Input type="radio" name="bgtCalFlag" value="1" <% if( bgtCalFlag.equals("1")){out.println("checked=true"); }%>> Create a budget from an existing protocol calendar
	    </td>
		</tr>
		</table>


		<%}--%>


		<%if(!((!(budgetStatus ==null)) && (budgetStatus.equals("F") || budgetStatus.equals("T") )&&(mode.equals("M")))) {%>


		<table width="600" cellspacing="2" cellpadding="2">
		<tr>
</table>

<%/*SV, 8/26/04, show only if we are creating/modifying a budget with a valid status.*/%>

<%if ((!((!(budgetStatus ==null)) && (budgetStatus.equals("F") || budgetStatus.equals("T"))&&(mode.equals("M")))) && (pageRight>=6)) {%>





<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="budgetForm"/>
		<jsp:param name="showDiscard" value="N"/>
		<jsp:param name="noBR" value="Y"/>
</jsp:include>
<%}%>




		<%}%>

    <input type="hidden" name="mode" value="<%=mode %>">
	<input type="hidden" name="src"  value="<%=src%>">
	<input type="hidden" name="budgetId" value="<%=budgetId%>">
    <input type="hidden" name="fromPage" value="<%=fromPage%>">
    <input type="hidden" name="includedIn" value="<%=includedIn%>">

 <input type="hidden" name="budScope" value="<%=budScope%>">

	<%
	if(mode.equals("M"))
	 {%>
	     <input type="hidden" name="oldsite" value="<%=orgId%>">
		 <input type="hidden" name="oldstudy" value="<%=budgetStudyId%>">

	<%}%>



 </Form>

	<%

	 if ( ! includedIn.equals("P")) { %>

	   <button tabindex = 2 onClick="history.go(-1);"><%=LC.L_Back%></button>
	<% } %>

	<% if(orgFlag.equals("0")) {%>

		<SCRIPT Language="javascript">
		<%

if (ienet == 0)
{
		%>

		//window.document.budget.orgId.style.visibility="hidden";
		//document.all["SO"].style.visibility="hidden";
		<%
		}else{
		%>
//		window.document.div1.document.budget.orgId.visibility="hidden";
//		document.layers["SO"].visibility="hidden";
//		alert(document.SO.visibility);
//			submenuItem1.style.visibility ="hidden";
//		window.document.div1.document.budget.SO.visibility="hidden";
//		document.all["SO"].style.visibility="hidden";
//		window.document.div1.document.budget.SO.visibility="hidden";
//alert(window.document.layers.SO.display);
//alert(window.document.div1.document.layers[SO].visibility);
//		window.document.layers.SO.visibility="hidden";
<%}%>



		</SCRIPT>

	<% } %>

  <%}  //end of if body for rights
	else{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%
	}// end of else for rights
} // end if modRight
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}

%>
  <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</DIV>

<% if ( ! includedIn.equals("P")) { %>

	<div class ="mainMenu" id = "emenu" id = "emenu">
	<jsp:include page="getmenu.jsp" flush="true"/>
	</div>
<% }%>

</body>
</html>
