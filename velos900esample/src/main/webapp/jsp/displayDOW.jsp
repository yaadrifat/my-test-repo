<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Sch_Evts%><%--Schedule Events*****--%></title>

</HEAD>

	<% String src;

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   


<SCRIPT>

 function popupView(formobj)
  { 

   if (formobj.displayType.options[formobj.displayType.selectedIndex].value!="V")
   {
    alert("<%=MC.M_VisitNegIntrvl_SwitchView%>");/*alert("'Visits with negative intervals' or 'Visits with same day intervals' are defined for this Calender.\n You can only switch to 'Defined Visit' view");*****/
    formobj.displayType.value="V";
    return false;
   }
   
  }
  function checkChange(formobj)
  {
   if ((formobj.calStatus.value!='A') || (formobj.calStatus.value!='F'))
   if (formobj.changeFlag.value=="Y") {
   if (confirm("<%=MC.M_SureContWoutSvg_SbmtBtn%>"))/*if (confirm("Are you sure you want to continue without saving changes?\n       (To save your changes, Please use 'submit' button )"))*****/
   {
    return true;
   }
   else 
   {
   return false;
   }
  }
  }

	function uncheck(row,pos, formobj, displacement, num_columns) {	

//		displacement = eval("formobj.checkbox"+row+"["+(pos-1)+"].value");
					var ctrVal = "";
					var value="";
					var valueCheck="";
					var unCheckStr="";
					var delimitedVal="N";
			if (num_columns > 1) {
				if (!(eval("formobj.checkbox"+row+"["+(pos-1)+"].checked"))) {
					eval("value=formobj.checkbox"+row+"["+(pos-1)+"].value");
					if (value.indexOf(";")>=0)
					{
					   arrayOfStrings=value.split(";");
					   eval("formobj.checkbox"+row+"["+(pos-1)+"].value="+arrayOfStrings[0]);
					   delimitedVal="Y";
								   
					}
				if (delimitedVal=="Y")	unCheckStr=displacement+";"+arrayOfStrings[1];
				else unCheckStr=displacement;
					eval("formobj.uncheckedDisp"+row+"["+(pos-1)+"].value = '"+unCheckStr+"'" );
				} else if (eval("formobj.checkbox"+row+"["+(pos-1)+"].checked"))
				{   	

						if (eval("formobj.displayType.value=='V'"))
				     		eval("value=formobj.visitid"+row+"["+(pos-1)+"].value");
				     		else
				     		value="";
					eval("valueCheck=formobj.checkbox"+row+"["+(pos-1)+"].value");
					if (valueCheck.indexOf(";")>=0)
					 	ctrVal =  valueCheck ;
					 	else 
					 	 {
					 	 if (value.length>0)
						 ctrVal =  valueCheck + ";"+value;
						 else
						 ctrVal =  valueCheck ;
						}
										
					eval("formobj.checkbox"+row+"["+(pos-1)+"].value='"+ctrVal+"'");

				}
			}
			else {
				if (!(eval("formobj.checkbox"+row+ ".checked"))) {
					eval("value=formobj.checkbox"+row+".value");
	
					if (value.indexOf(";")>=0)
					{
					   arrayOfStrings=value.split(";");
					   eval("formobj.checkbox"+row+".value="+arrayOfStrings[0]);

					   
					}
					unCheckStr=displacement+";"+arrayOfStrings[1];
					eval("formobj.uncheckedDisp"+row+ ".value = '" +unCheckStr+"'");
				
				} else if (eval("formobj.checkbox"+row+".checked"))
				{
					if (eval("formobj.displayType.value=='V'"))
					eval("value=formobj.visitid"+row+".value");
					else
					value="";
					
					eval("valueCheck=formobj.checkbox"+row+".value");
					if (valueCheck.indexOf(";")>=0)
					ctrVal =  valueCheck ;
					else {
						 if (value.length>0)
						 ctrVal =  valueCheck + ";"+value;
						 else
						 ctrVal =  valueCheck ;
						}	
					eval("formobj.checkbox"+row+".value='"+ctrVal+"'");
					//alert("formobj.checkbox"+row+".value");


				}


			}
	formobj.changeFlag.value="Y";
	}

	
	function validate(formobj) {

		if (formobj == document.display){

			if ((formobj.displayDur.value=='0') || (formobj.displayDur.value=="")) {

				alert("<%=MC.M_ValidValue_ForDisp%>");/*alert("Please specify a valid value for displaying the page.");*****/

				return false;

			}

		}

	 
		
		if (formobj == document.displayDOW) {
			if (formobj.pageRight.value > 4) {

			    if (!(validate_col('Esign',formobj.eSign))) return false



 			 	if(isNaN(formobj.eSign.value) == true) {

 			 		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

					formobj.eSign.focus();

					return false;

			    }

			}

		}

		return true; 	

	}

</SCRIPT>


<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>


<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%>


<jsp:useBean id="ctrldao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>

<jsp:useBean id="eventlistdao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>

<jsp:useBean id="assocdao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>

<jsp:useBean id="assoclistdao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>
<jsp:useBean id="visitdao" scope="request" class="com.velos.esch.business.common.ProtVisitDao"/>



<BODY >



<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1000;"></div>

<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>



<%



	int pageRight=0;

	int rowNo = 0;

	String eventId="";

	String protocolId=request.getParameter("protocolId");


	HttpSession tSession = request.getSession(true); 



    if (sessionmaint.isValidSession(tSession)) {

		 if(protocolId == "" || protocolId == null || protocolId.equals("null") || protocolId.equals("")) {
		 %>
	
		  <jsp:include page="calDoesNotExist.jsp" flush="true"/>
	
		  <%
	
		}else {

		
    	String mode = request.getParameter("mode");

	String calledFrom = request.getParameter("calledFrom");

	String calStatus = request.getParameter("calStatus");	
	
	

	String calAssoc=request.getParameter("calassoc");
    calAssoc=(calAssoc==null)?"":calAssoc;
	
	String eventType="";	 

	String name="";

	String eventName="";

	String duration= "";
	
	String newDuration = "";
	
	newDuration = (String) tSession.getAttribute("newduration");
	
	if (StringUtil.isEmpty(newDuration))
	{
		duration =  request.getParameter("duration"); 
	}
	else
	{
		duration = newDuration ; 
	}
	

	String eventDur = "";

	String description="";

	String displacement="";

	String orgId="";

	Integer Id=new Integer(protocolId);

	int num_intervals = 0;

	Integer dur= new Integer("2");

	ArrayList disps = new ArrayList();

	int displayDur = 0;

	int inputDur = 0;

	int totalProtocolVisits = 0;
	String refresh_flag=request.getParameter("refresh_flag"); //first time the screen is loaded, this will be effective, after that it's changed to "Y"
	//displayType - D- Days, W- Weeks, M - Months
	if (refresh_flag == null) refresh_flag = "";
	String displayType=request.getParameter("displayType");
	int pageNo=EJBUtil.stringToNum(request.getParameter("pageNo"));
		
			String uName = (String) tSession.getValue("userName");
	
			String userId = (String) (tSession.getValue("userId"));
	
			String accId = (String) (tSession.getValue("accountId"));
	
			String timeOut = (String) (tSession.getAttribute("userSession"));
			//SV, 10/26/04, With the calendar enhancement, this screen is one of the entry points in calendar details. If we are here 
			// This approach works because, only the first, while coming from protocollist.jsp, is the protocolName request parameter set.
			// From here on http session variable is used to display calendar name.				
			
			//JM:
			//String protocolName = request.getParameter("protocolName");
			String protocolName = StringUtil.decodeString(request.getParameter("protocolName"));
			
			if (protocolName != null)
					tSession.putValue("protocolname", protocolName);
			if (calledFrom.equals("S")) {
	
			   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	 
	
			   if ((stdRights.getFtrRights().size()) == 0){
	
					pageRight= 0;
	
			   }else{
	
					pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
	
			   }	
	
			} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
	
				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	
				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
	
			}
	
	 
	
			if (pageRight > 0 )
	
			{
	
				eventType = "P";
	
				ArrayList eventNameIds= null; 
	
				ArrayList eventNames= null; 
	
				ArrayList eventIds=null; 
	
				ArrayList protocolIds=null; 
	
				ArrayList names=null ; 
	
				ArrayList descriptions= null; 
	
				ArrayList eventTypes=null; 
	
				ArrayList eventDurs= null; 
	
				ArrayList displacements= null; 
	
				ArrayList orgIds= null; 
	
				ArrayList eventOrgIds = null;
	
				ArrayList costs= null;
	
				ArrayList parentCosts= null; 
	
				ArrayList visit_names = null; //SV, 9/27/04,
				ArrayList visit_ids = null; //SV, 9/27/04,
				ArrayList visit_displacements = null; //SV, 9/27/04,
				ArrayList event_visits = null;
				
				ArrayList defDisplacements=new ArrayList();
				String negativeVisit="N";
				%>
	
	
<DIV class="tabDefTop" id="div1"> 
	
				
	
				<%
	
				if(mode.equals("N")) {
	
				%>
	
					<P class="sectionHeadings"> <%=LC.L_PcolCal_New%><%--Protocol Calendar >> New*****--%> </P>
	
					<%
	
				} else if(mode.equals("M")) {
	
				%>
	
					<P class="sectionHeadings"> <%=LC.L_PcolCal_Modify%><%--Protocol Calendar >> Modify*****--%> </P>
	
				<%
	
				}
	
				%>
	
				<jsp:include page="protocoltabs.jsp" flush="true"/> 
	

</div>
<DIV class="tabDefBot" id="div2"> 
	
				<% if (pageRight == 4) {%>
	
					<P class = "defComments"><FONT class="Mandatory"><%=MC.M_YouViewRgt_ChgSvd%><%--You have only View rights. Any changes made will not be saved.*****--%></Font></P>
	
				<%}%>
				<%
				visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId));
				totalProtocolVisits = visitdao.getTotalVisits(EJBUtil.stringToNum(protocolId));
				defDisplacements=visitdao.getDisplacements();
				//DEBUG System.out.println("displayDOW: totalProtocolVisits="+new Integer(totalProtocolVisits).toString());			
				int pages=0;
	
				if ((totalProtocolVisits > 0) && !refresh_flag.equals("Y")) { 
					displayType = "V"; //SV, 10/01, item 5 on requirements doc - if there are defined visits, default to that view.
				}
				refresh_flag = "Y";
	
				//calculate the number of pages
				if (!(displayType.equals("O"))) {	
				    displayDur=EJBUtil.stringToNum(request.getParameter("displayDur"));
				    
				    if (displayDur == 0)
					displayDur = 3; //default is this.
	
					
					if ((displayType.equals("D")) || (displayType.equals("W"))) {		
			
					    inputDur = displayDur * 7;
			
					} else if (displayType.equals("M")) {
			
					    inputDur = displayDur * 30;
			
					} else if (displayType.equals("Y")) {
			
					    inputDur = displayDur * 365;
			
					} 
			
					if (displayType.equals("V"))
					{
	//					inputDur = displayDur;
						inputDur = EJBUtil.stringToNum(duration);
	//					displayDur = totalProtocolVisits;
					}
			
				}
			
	
				if (displayType.equals("V")) {
						pages = 1;
						pageNo = 1; //SV, 10/26/04, fix for bug #1834
				}
				else {
					if (inputDur > 0) {
	
						pages=(EJBUtil.stringToNum(duration)/inputDur);
					
						if ((EJBUtil.stringToNum(duration)%inputDur)!=0) {
						
							pages++;
						}
					}
				}
	
				int fromDisp= 0 ;
	
				int toDisp = 0;
	
				if (pageNo==1) {
	
					fromDisp= pageNo;
	
					toDisp= inputDur;
	
				}else {
	
					fromDisp= ((pageNo-1)*inputDur)+1;
	
					toDisp= pageNo*inputDur;
	
				}
	
				//	out.println("from Disp " + fromDisp + " -  To Disp " + toDisp);
				if (displayType.equals("V"))
				{
	
					num_intervals = totalProtocolVisits;
				}
				else {
					num_intervals = displayDur;
				}
				%>
				
<%				if (calledFrom.equals("P")  || calledFrom.equals("L")) { 
	
					//this is to get the event names with 0 displacement
	
					eventlistdao= eventdefB.getProtSelectedEvents(EJBUtil.stringToNum(protocolId));
	
					eventNameIds=eventlistdao.getEvent_ids(); 
	
					eventNames= eventlistdao.getNames();
	
					eventOrgIds= eventlistdao.getOrg_ids();	   
	
					parentCosts= eventlistdao.getCosts();	   
	
					//this is to get all the events within a displacement range
					System.out.println("fromDisp,toDisp"+fromDisp+"*"+toDisp);
					ctrldao= eventdefB.getFilteredRangeEvents(Id.intValue(),fromDisp,toDisp); //Id-protocol Id  
	
					eventIds=ctrldao.getEvent_ids() ;
	
					protocolIds=ctrldao.getChain_ids() ; 
	
					names= ctrldao.getNames(); 
	
					descriptions= ctrldao.getDescriptions(); 
	
					eventTypes= ctrldao.getEvent_types(); 
	
					eventDurs= ctrldao.getDurations(); 
	
					displacements= ctrldao.getDisplacements(); 
	
					orgIds= ctrldao.getOrg_ids(); 
	
					costs = ctrldao.getCosts();
					event_visits = ctrldao.getFk_visit(); //SV, 9/27/04
	
				}
	
				else if (calledFrom.equals("S")) { //called From Study
	
					//this is to get the event names with 0 displacement
	
					assoclistdao= eventassocB.getProtSelectedEvents(EJBUtil.stringToNum(protocolId));
	
					eventNameIds=assoclistdao.getEvent_ids(); 
	
					eventNames= assoclistdao.getNames();
	
					parentCosts= assoclistdao.getCosts();		   
	
				eventOrgIds= assoclistdao.getOrg_ids(); 
	
					//this is to get all the events within a displacement range
			System.out.println("fromDisp,toDisp"+fromDisp+"*"+toDisp);  
					assocdao= eventassocB.getFilteredRangeEvents(Id.intValue(),fromDisp,toDisp);  
	
					eventIds=assocdao.getEvent_ids() ;
	
					protocolIds=assocdao.getChain_ids() ; 
	
					names= assocdao.getNames(); 
	
					descriptions= assocdao.getDescriptions(); 
	
					eventTypes= assocdao.getEvent_types(); 
	
					eventDurs= assocdao.getDurations(); 
	
					displacements= assocdao.getDisplacements(); 
	
					orgIds= assocdao.getOrg_ids(); 
	
					costs = assocdao.getCosts();		
					event_visits = assocdao.getEventVisits(); //SV, 9/27/04
				}
				System.out.println("Displacements"+displacements);
				
				//Loop through displacement to see if multiple visit defined with same intervals
				Integer tempDisp=null;
				String mulVisitSameDay="N";
				int pos=-1;
				for (int count=0;count<defDisplacements.size();count++)
				{	 pos=-1;	
				     tempDisp=(Integer)defDisplacements.get(count);
				     pos=defDisplacements.lastIndexOf(tempDisp);
					if (count!=pos) {
					    mulVisitSameDay="Y";				    
					 break;   
					}
					}
				//End loop
				
				//check if any negative displacement visit is define. If yes, disable the
				//week,month and days view
				for (int i=0;i<displacements.size();i++)
				{
				  if  (EJBUtil.stringToNum((String) displacements.get(i)) <0)
				  {
				    negativeVisit="Y";
				   break;
				  }
				}
				
				%>
				
				
	  
	
				<Form name="display"  method="post" action="displayDOW.jsp?selectedTab=4&mode=<%=mode%>&duration=<%=duration%>&calledFrom=<%=calledFrom%>&protocolId=<%=protocolId%>&srcmenu=<%=src%>&calStatus=<%=calStatus%>&pageNo=<%=pageNo%>&refresh_flag=<%=refresh_flag%>&calassoc=<%=calAssoc%>">
	
					<table border="0">
	
					<tr>
	
					<td> <P class="defComments"><%=MC.M_DispCurrlyEvts_Interval%><%--Display interval of currently associated Scheduled Events*****--%></P>
	
					</td>
	
					<td> 
	                   <%	if ( (negativeVisit.equals("Y")) || (mulVisitSameDay.equals("Y")) ) { %>
	                   <select name="displayType" onChange="return popupView(document.display)">
	                   <option value="D"><%=LC.L_Days_7%><%--Days * (7)*****--%></option>
	
								<option value="W"><%=LC.L_Weeks%><%--Weeks*****--%></option>
	
								<option value="M"><%=LC.L_Months%><%--Months*****--%></option>
	
								<option value="V" Selected ><%=LC.L_Defined_Visits%><%--Defined Visits*****--%></option>
								</select>
	                   
	                   <%}else{%>
						<select name="displayType">
	
						
							
							  <%if ((displayType.equals("D")) || (displayType.equals("O"))) { %>
	
								<option value="D" Selected ><%=LC.L_Days_7%><%--Days * (7)*****--%></option>
	
								<option value="W"><%=LC.L_Weeks%><%--Weeks*****--%></option>
	
								<option value="M"><%=LC.L_Months%><%--Months*****--%></option>
	
								<option value="V"><%=LC.L_Defined_Visits%><%--Defined Visits*****--%></option>
							<%} else if (displayType.equals("W")){ %>
	
								<option value="D" ><%=LC.L_Days_7%><%--Days * (7)*****--%></option>
	
								<option value="W" Selected><%=LC.L_Weeks%><%--Weeks*****--%></option>
	
								<option value="M"><%=LC.L_Months%><%--Months*****--%></option>
								<option value="V"><%=LC.L_Defined_Visits%><%--Defined Visits*****--%></option>
	
							<%} else if (displayType.equals("M")){ %>
	
								<option value="D" ><%=LC.L_Days_7%><%--Days * (7)*****--%></option>
	
								<option value="W"><%=LC.L_Weeks%><%--Weeks*****--%></option>
	
								<option value="M" Selected><%=LC.L_Months%><%--Months*****--%></option>
								<option value="V"><%=LC.L_Defined_Visits%><%--Defined Visits*****--%></option>
							<%} else if (displayType.equals("V")){ %>
	
								<option value="D" ><%=LC.L_Days_7%><%--Days * (7)*****--%></option>
	
								<option value="W"><%=LC.L_Weeks%><%--Weeks*****--%></option>
	
								<option value="M"><%=LC.L_Months%><%--Months*****--%></option>
								<option value="V" Selected><%=LC.L_Defined_Visits%><%--Defined Visits*****--%></option>
	
							<%}%>
	
						</select>
					<%}//end else for negativeVisit
					%>
						</td>
	
						<td width=50>&nbsp;</td>
	
	
						<%if (displayType.equals("V")) {%>
							
						<td><P class="defComments"><%=MC.M_VisitsNum_InThisCal%><%--Number of visits in this calendar*****--%></P></td>
						<td><Input type="text" name="displayVisits" size=2 value=<%=totalProtocolVisits%> readonly=true></td>
						<td><Input type="hidden" name="displayDur" size=2 value=<%=displayDur%>></td>
	
						<% } else {%>
							<td><P class="defComments"><%=MC.M_IntervalsNum_OnPage%><%--Number of intervals on a page*****--%></P></td>
							<td><Input type="text" name="displayDur" size=2 value=<%=displayDur%>></td>
							<td> <P class="defComments"><%=LC.L_OnOne_Page%><%--on one page.*****--%></P></td>
							
						<%}%>
						<Input type="hidden" name="refresh_flag" value="<%=refresh_flag%>">
	
						<td>
	
						<button type="submit" onclick="return validate(document.display)"><%=LC.L_Search%><%--<%=LC.L_Search%>*****--%></button> 
	
						</td>
						</tr>
	
					 </table>				
	
				</Form>
	
				
	
				
	
				<%			if ((calStatus.equals("A")) || (calStatus.equals("F"))){%>
	
				<P class = "defComments"><FONT class="Mandatory"><%=MC.M_ChgNotSvd_ForActvCal%><%--Changes made will not be saved for an Active/Frozen Calendar*****--%></Font></P>
	
				<%}%>
	
	
	
				<%
	
				if ((displayType!= null) && displayType.equals("O")) {
	
					//		out.println("null");
	
				}
	
				else {
				    if (displayType.equals("V")) {	
				 %>  
					
					<Form name="displayDOW" method="post" action="saveEventToProto.jsp" id="displayForm">
					<%} else{ %>
					<Form name="displayDOW" method="post" action="saveEventToProtocol.jsp" id="displayForm" >
					<%} %>
	
						<INPUT NAME="mode" TYPE=hidden  value=<%=mode%>>
	
						<INPUT NAME="srcmenu" TYPE=hidden value=<%=src%>>
	
						<INPUT NAME="calledFrom" TYPE=hidden  value=<%=calledFrom%>>
	
						<INPUT NAME="duration" TYPE=hidden  value=<%=duration%>>
	
						<INPUT NAME="protocolId" TYPE=hidden  value=<%=protocolId%>>
	
						<INPUT NAME="calStatus" TYPE=hidden  value=<%=calStatus%>>
	
						<INPUT NAME="displayType" TYPE=hidden  value=<%=displayType%>>
	
						<INPUT NAME="pageNo" TYPE=hidden  value=<%=pageNo%>>
	
						<INPUT NAME="fromDisp" TYPE=hidden  value=<%=fromDisp%>>
	
						<INPUT NAME="toDisp" TYPE=hidden  value=<%=toDisp%>>	
	
						<INPUT NAME="displayDur" TYPE=hidden  value=<%=displayDur%>>
						
						<INPUT NAME="calassoc" TYPE=hidden  value=<%=calAssoc%>>
	
					       <input type="hidden" name="pageRight" value=<%=pageRight%>> 
	
					      <input type="hidden" name="changeFlag" value=""> 
	
					    <%if (! displayType.equals("V")) {%>
						<P class="defComments"><%=LC.L_Page%><%--Page*****--%>: 	
	
							<% for (int i=1;i<=pages;i++) {
	
								if (i==pageNo) {
	
								%>
	
									<Font class="mandatory" style="font-size:16;"><B><%=i%></B></Font>
	
								<%
	
								}
	
								else {
	
								%>
	
									<A style="font-size:14;" href="displayDOW.jsp?selectedTab=4&mode=<%=mode%>&duration=<%=duration%>&calledFrom=<%=calledFrom%>&protocolId=<%=protocolId%>&srcmenu=<%=src%>&calStatus=<%=calStatus%>&pageNo=<%=i%>&displayType=<%=displayType%>&displayDur=<%=displayDur%>&refresh_flag=<%=refresh_flag%>"  onClick="return checkChange(document.displayDOW)"> <%=i%></A>
	
								<%
	
								}
	
							} //pages loop
						} //if not "V"
							%>
	
							<%
	
							if ((!(calStatus.trim().equals("A")) && !(calStatus.trim().equals("F")) && pageRight >=6 && mode.equals("M")) ||(!(calStatus.trim().equals("A")) && !(calStatus.trim().equals("F")) && pageRight >=5 && mode.equals("N"))) {
	
							%>
							         
	                                                        <% 
							   if(!calStatus.equals("D")){
								%>
							 
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Esignature%><%--e-Signature*****--%> <FONT class="Mandatory">* </FONT>
	
								<input type="password" name="eSign" id="eSign" maxlength="8" onkeyup="ajaxvalidate('misc:'+this.id,4,'eSignMessage','<%=LC.L_Valid_Esign%><%--Valid e-Sign*****--%>','<%=LC.L_Invalid_Esign%><%--Invalid e-Sign*****--%>','sessUserId')"><span id="eSignMessage"></span>
	
								&nbsp;&nbsp;<input type="image" src="../images/jpg/Submit.gif" align="absmiddle" onclick="if (validate(document.displayDOW)==false) {setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}" border="0" id="submit_btn"> 
							<%	
							}
							%>	
	
<%--	SV, 10/28/04, per Rehan we don't need to show this message any more
							<FONT class = "Mandatory">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data will be lost if not saved within  <%=timeOut%> minutes from </FONT><input type=text name="currentTime" size=5 Readonly> <FONT class = "Mandatory">Hrs.</FONT> 
							<SCRIPT>
								todayDate = new Date();
								min = (todayDate.getMinutes()).toString();
									if (min.length == 1) { 
										min = "0" + min;
									} 
								currentTime = todayDate.getHours()+":"+min;
								document.displayDOW.currentTime.value=currentTime;
							</SCRIPT>
--%>								
							<%}%>
						<% 
	
							if ((calStatus.equals("A")) || (calStatus.equals("F")) || (pageRight==4)) {
	
							} else {
							  String m;
							  if (mode.equals("N")){m="N";}else{m="E";}
	
							%>
	
							&nbsp;&nbsp;&nbsp;&nbsp;<Font class="defComments"> <A href="eventbrowser.jsp?srcmenu=<%=src%>&selectedTab=2&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>"  onclick ="return f_check_perm(<%=pageRight%>,'<%=m%>')" ><%=LC.L_AddOrEdt_Evt%><%--Add/Edit Events*****--%></A> </Font>
							&nbsp;&nbsp;&nbsp;&nbsp;<Font class="defComments"> <A href="visitlist.jsp?srcmenu=<%=src%>&selectedTab=3&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&displayType=D&displayDur=<%=displayDur%>&pageNo=1"  onclick ="return f_check_perm(<%=pageRight%>,'<%=m%>')" ><%=LC.L_AddOrEdt_Visits%><%--Add/Edit Visits*****--%></A> </Font>
	
							<%
	
							}
	
						%>
	
						</P>
	
						<%
	
						//calculate the table width
	
						String table_width="";
	
						if (displayType.equals("D")) {
	
							table_width = num_intervals*45+"%";
	
						} else if (displayType.equals("V")) {
							table_width = totalProtocolVisits*5+"%";
						} else {
							table_width = num_intervals*10+"%";
						}	
	
						%>
	
						<TABLE border=1 cellPadding=1 cellSpacing=1 width=<%=table_width%>>
	
							<TR>
	
								<TH><%=LC.L_Event%><%--Event*****--%></TH>
								<%
								//SV, 9/27/04, predefined visit headers
								if (displayType.equals("V")) {
								
									visit_ids = visitdao.getVisit_ids();
									visit_names = visitdao.getNames();
									visit_displacements = visitdao.getDisplacements();								
	//								for (int j=0; j < visit_names.size(); j++) 
									for(int j=(((pageNo*num_intervals)-num_intervals)+1);j<=(pageNo*num_intervals);j++) 
									{
										if (j > visit_ids.size()) break;
									
									%>
										
									
								<TH> <%=visit_names.get(j-1)%></TH>
									<%}
								}
								
								else {
									//make the header row which shows weeks/months
		
									for(int j=(((pageNo*num_intervals)-num_intervals)+1);j<=(pageNo*num_intervals);j++)
		
									{%>	 
		
										<TH>
		
										<% if (displayType.equals("M")) {%>
		
											<%=LC.L_Month%><%--Month*****--%><%=j%>
		
										<% } else {%>
		
											<%=LC.L_Week%><%--Week*****--%><%=j%>
		
										<%} %> 
		
										</TH>
		
										<%
		
									} //loop for making headings 
								}%>
		
							</TR>
	
							<%
	
							rowNo = 0;
	
							int noOfCheckboxes = 0;
	
							// detail section
							//loop of all the events with 0 displacement filling in the details (checkboxes)
	
							for (int i=0;i<eventNameIds.size();i++) {
								noOfCheckboxes = 0;
	
								ArrayList rowDisp = new ArrayList();	
								ArrayList eventVisitRefresh = new ArrayList();	
	
								//store the cost of each 0 displacement event.
	
								//The cost is the row number in the table. 
	
								Integer parentCost = new Integer((String)parentCosts.get(i));
	
								int intervalcount =1 ;
	
								//check whether the row(cost) is same, store displacements for the row
	
								for(int k=0;k<costs.size();k++)
	
								{
									Integer cost = new Integer((String)costs.get(k));	
									if (parentCost.intValue()==cost.intValue())
	
									{
	
										rowDisp.add((String)displacements.get(k));
										eventVisitRefresh.add(event_visits.get(k));
	
									} 
									
	
								}
	
								%>
	
								<%
	
								if (i%2 == 0) 
	
								{ 
	
								%>
	
									<tr class="browserEvenRow"> 
	
								<%
	
								}
	
								else {
	
								%>
	
									<tr class="browserOddRow"> 
	
								<%
	
								}
	
								%> 
	
								<td width=5%>
	
									<%=eventNames.get(i)%>
	
									<% if (calledFrom.equals("P")  || calledFrom.equals("L")) {%> 
	

<%--Reverted back 10/31, as it started to fall apart with this approach, first deletes failed to apply to schedule events.
	SV, 10/29, bug 1827, started as a propagation bug, but it seems like a design issue. We don't know if it was meant that we use the
event id's (org_ids in event list). changing it to use eventnameIds instead. This will be the event id of the event record with event type = 'A' and displacement = 0
												<input type=hidden name="eventIds" value="<%=eventOrgIds.get(i)%>">
 SV, 11/1, now again, turn fix for 1827	back on. This will open a pandora box, we will face it!!!?--%>	
												<input type=hidden name="eventIds" value="<%=eventNameIds.get(i)%>">
										
										<input type=hidden name="parentCosts" value="<%=parentCosts.get(i)%>">
	
									<%} else { %>
	
<%--backand forth on this: now we want 1827 fixed										<input type=hidden name="eventIds" value="<%=eventOrgIds.get(i)%>">  --%>
<%-- first fix for 1827, is to use the event id for the event name(event record with type  = 'A' and disp = 0) --%>
										<input type=hidden name="eventIds" value="<%=eventNameIds.get(i)%>"> 
										<input type=hidden name="parentCosts" value="<%=parentCosts.get(i)%>">					
	
									<%}%>			
	
								</td>
	
								<%if(displayType.equals("D")) {%>
	
									<td>
	
								<% }
	
								int endloop = 7;
	
								int incfactor=1;
	
								if (displayType.equals("W")) {
	
									incfactor = 7;
	
								} else if (displayType.equals("M")) { 
	
									incfactor = 30;
	
									endloop = 30;
	
								}
								else if (displayType.equals("V")) {
									incfactor = 1;
									endloop = 7;
								}
								String interval= "";
	
								if (displayType.equals("V")) {
										String dispdur ="" ;
										if (displayType.equals("M")) {
	
										interval=LC.L_Month/*Month*****/+ intervalcount;
										dispdur =LC.L_Month/*Month*****/ +" "+ (((pageNo*num_intervals)-num_intervals)+intervalcount) ;
	
									 } else {
	
										interval=LC.L_Week/*Week*****/+ intervalcount;
										dispdur =LC.L_Week/*Week*****/ +" "+ (((pageNo*num_intervals)-num_intervals)+intervalcount) ;
	
										}	
										
	
	// The loop here picks up the list of visits in event.
								for(int v=(((pageNo*num_intervals)-num_intervals)+1);v<=(pageNo*num_intervals);v++)
								{
									if (v > visit_ids.size()) break;
									
									noOfCheckboxes++;
								%>
									<%--the hidden uncheckedDisp is used in saveEventToProto.jsp.Thisstores the displacements unchecked by the user--%>
									<td>
	
									<INPUT type="hidden" name=uncheckedDisp<%=(i+1)%> value="" size=1 >
	
									<input type=hidden value=<%=v%> size=1>
									
									<%if (visit_ids!=null) { %>
										<input type=hidden name="visitid<%=i+1%>" value=<%=visit_ids.get(v-1)%> size=1>
										<%}%>

									 
									<%String tempValue=visit_displacements.get(v-1)+";"+visit_ids.get(v-1);%>
									<INPUT id=checkbox<%=(i+1)%> onmouseOver="return overlib('<%=LC.L_Interval%><%--Interval*****--%>: <%=visit_names.get(v-1)%> <BR><%=LC.L_Event%><%--Event*****--%>: <%=eventNames.get(i)%>');"  onMouseOut="return nd()" name=checkbox<%=(i+1)%> type=checkbox  value="<%=tempValue%>" onclick="uncheck(<%=(i+1)%>,<%=noOfCheckboxes%>, document.displayDOW, <%=visit_displacements.get(v-1)%>, <%=num_intervals%>)"	
									<% //if the # of checkbox is greater than duration then disable the checkbox
									if((v+fromDisp-1)>EJBUtil.stringToNum(duration)) 
									{%>
										disabled = "true"
									<%}
									for(int p=0;p<rowDisp.size();p++)
	
									{
										Integer disp = new Integer((String)rowDisp.get(p));
	
										int calcDisp = 0;
	
										if(pageNo!=1) //reduce the displacement so as to start acc to page #
	
										{						
	
											calcDisp = (disp.intValue() - fromDisp)+1;
	
										}	
	
										else {
	
											calcDisp = disp.intValue();
	
										}				 
	//if( visit_displacements.get(v-1).equals(new Integer(calcDisp)) ) {
										
										
										if( visit_displacements.get(v-1).equals(disp) && visit_ids.get(v-1).equals(eventVisitRefresh.get(p)) ) {%>  checked="true" <%}
									}%>
									</td>
	
									<%} //for loop%>
								<input type="hidden" name="totalProtocolVisits" value="totalProtocolVisits=<%=totalProtocolVisits%>"> 
									
								<%} // if display_type = "V" statement
								else {
								for(int m=1 ;m<=(num_intervals*endloop); m+=incfactor){
	
									noOfCheckboxes++;
	
									%>	
	
									<%	if(!displayType.equals("D")) {%>
	
										<td>
	
									<% }%>		
	
									<%--//the hidden uncheckedDisp is used in saveEventToProto.jsp.Thisstores the displacements unchecked by the user--%>
	
									<INPUT type="hidden" name=uncheckedDisp<%=(i+1)%> value="" size=1 >
	
									<input type=hidden value=<%=m%> size=1>  
	
									<%
					String dispdur ="" ;
									if (displayType.equals("M")) {
	
										interval=LC.L_Month/*Month*****/ + intervalcount;
										dispdur =LC.L_Month/*Month*****/+" " + (((pageNo*num_intervals)-num_intervals)+intervalcount) ;
	
									 } else {
	
										interval=LC.L_Week/*Week*****/ + intervalcount;
										dispdur =LC.L_Week/*Week*****/+" " + (((pageNo*num_intervals)-num_intervals)+intervalcount) ;
	
										}	
										
											%>
	
										
	
	<!--							<INPUT id=checkbox1 onmouseOver="return overlib('Interval:<%=interval%> for Event Name:<%=eventNames.get(i)%>');"  onMouseOut="return nd()" name=checkbox<%=(i+1)%> type=checkbox  value=<%=m%> onclick="uncheck(<%=(i+1)%>,<%=noOfCheckboxes%>, document.displayDOW)" -->
	<!--SV, REDTAG we can show the visit's interval here? -->	
							<INPUT id=checkbox<%=(i+1)%> onmouseOver="return overlib('<%=LC.L_Interval%><%--Interval*****--%>:<%=dispdur%> <%=LC.L_ForEvt_Name%><%--for Event Name*****--%>:<%=eventNames.get(i)%>');"  onMouseOut="return nd()" name=checkbox<%=(i+1)%> type=checkbox  value=<%=m%> onclick="uncheck(<%=(i+1)%>,<%=noOfCheckboxes%>, document.displayDOW, <%=m%>, <%=num_intervals%>)"
									<%
	
										
	
									//if the # of checkbox is > than duration then disable the checkbox
	
									if((m+fromDisp-1)>EJBUtil.stringToNum(duration)) {%> disabled = "true"<%} 
	
									//check the checkbox for the same displacement
	
									for(int p=0;p<rowDisp.size();p++)
	
									{
	
										Integer disp = new Integer((String)rowDisp.get(p));
	
										int calcDisp = 0;
	
										if(pageNo!=1) //reduce the displacement so as to start acc to page #
	
										{						
	
											calcDisp = (disp.intValue() - fromDisp)+1;
	
										}	
	
										else {
	
											calcDisp = disp.intValue();
	
										}				 
	
										if(m == calcDisp ) {%>  checked="true" <%}
	
									}%> onhelp="">
	
									<%
	
									if (displayType.equals("D")) { 
	
										if((m%7 == 0) && (m < (num_intervals*7))) 
	
										{
	
										  intervalcount++;	
	
											%>
	
										</td>
	
										<td>
	
										<%}
	
									}else {
	
										 intervalcount++;	
	
										%>
	
										
	
										</td>
	
									<%}
	
									
	
								} //loop of weeks 
							} //else %>
								<%
	
								if (displayType.equals("D")) {
	
								%> 
	
									</td>
	
								<%}%>
	
								</tr>
	
							<%
	
								
	
							}// loop of eventNamesIds
	
							%>
	
						</TABLE>
	
					<%
	
					} //end if display type
	
					%>
	
					<BR>
	
					<table width="100%" cellspacing="0" cellpadding="0">
					<%
	
				if ((!(calStatus.trim().equals("A")) && !(calStatus.trim().equals("F")) && pageRight >=6 && mode.equals("M")) ||(!(calStatus.trim().equals("A")) && !(calStatus.trim().equals("F")) && pageRight >=5 && mode.equals("N"))) {
	
							%>
	
	
					<tr><td colspan=2 align=center><FONT class="Mandatory" size=2> <%=MC.M_RembClickSub_ToSave%><%--Please remember to click on Submit above to save data*****--%></Font></td></tr>
					<%}%>
	
						<tr>
	
							<td align=center> 
	
<%-- SV, 10/28, always take back to visitlist								<%if (mode.equals("M")) {%>
	
<!--SV, 10/25/04, cal-enh-??									<A href = "protocolcalendar.jsp?protocolId=<%=protocolId%>&srcmenu=<%=src%>&selectedTab=1&mode=<%=mode%>&calledFrom=<%=calledFrom%> " type="submit"><%=LC.L_Back%></A> -->
									<A href = "visitlist.jsp?srcmenu=<%=src%>&selectedTab=3&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&displayType=D&displayDur=<%=displayDur%>&pageNo=1" type="submit"><%=LC.L_Back%></A>
	
								<%}%>
								else { %>
	
									<A href = "eventbrowser.jsp?protocolId=<%=protocolId%>&srcmenu=<%=src%>&selectedTab=2&mode=<%=mode%>&calledFrom=<%=calledFrom%>&calStatus=<%=calStatus%>&duration=<%=duration%>" type="submit"><%=LC.L_Back%></A>
	
								<%}%>
--%>	
										<A type="submit" href = "visitlist.jsp?srcmenu=<%=src%>&selectedTab=3&duration=<%=duration%>&protocolId=<%=protocolId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&displayType=D&displayDur=<%=displayDur%>&pageNo=1&calassoc=<%=calAssoc%>" onClick="return checkChange(document.displayDOW)"><%=LC.L_Back%></A>

							</td>
	
						<td> 
	
								<A type="submit" href="fetchProt.jsp?protocolId=<%=protocolId%>&srcmenu=<%=src%>&selectedTab=5&mode=<%=mode%>&calledFrom=<%=calledFrom%>&calStatus=<%=calStatus%>&displayType=<%=displayType%>&pageNo=1&duration=<%=duration%>&displayDur=<%=displayDur%>&calassoc=<%=calAssoc%>"  onClick="return checkChange(document.displayDOW)"><%=LC.L_Next%></A>
	
							</td> 
	
						</tr>
	
					</table>
	
				</Form>

			<%
				
			} //end of if body for page right
	
			else
	
			{
	
			%>
	
				<jsp:include page="accessdenied.jsp" flush="true"/>
	
			<%
	
			} //end of else body for page right

		} //end of calnotfound

		
	}//end of if body for session

	else

	{

	%>

		<jsp:include page="timeout.html" flush="true"/>

	<%

	}

	%>

	<div> 

	    <jsp:include page="bottompanel.jsp" flush="true"/>

	</div>

</div>

<div class ="mainMenu" id = "emenu"> 

  <jsp:include page="getmenu.jsp" flush="true"/>

</div>

<div id="tipDiv" style="position:absolute; visibility:visible;  z-index:100"></div>
<script>linkFormSubmit('displayForm');</script>
</BODY>

<% if (sessionmaint.isValidSession(tSession)) //set the current time

{

%>


<%

}

%>

</HTML>








