<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<title><%=LC.L_View_Notes%><%--View Notes*****--%></title>
</head>
<body>
<DIV style="font-family:Verdana,Arial,Helvetica,sans-serif">

<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="submB" scope="session" class="com.velos.eres.web.submission.SubmissionJB"/>
<jsp:useBean id="submStatusB" scope="request" class="com.velos.eres.web.submission.SubmissionStatusJB"/>
<%@ page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*" %>
<%@ page import="com.velos.eres.web.grpRights.GrpRightsJB" %>
<%@ page import="java.util.ArrayList,java.util.Calendar,java.util.Hashtable" %>
<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
    boolean isMSIE = false;
    if (request.getHeader("USER-AGENT").toLowerCase().indexOf("msie") != -1) { isMSIE = true; }

    String accountId = (String)tSession.getAttribute("accountId");
    String studyId = request.getParameter("studyId");
	tSession.setAttribute("studyId", StringUtil.htmlEncodeXss(studyId));
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");
    String usrId = (String) tSession.getValue("userId");
    String usrFullName = UserDao.getUserFullName(EJBUtil.stringToNum(usrId));
    String submissionPK = StringUtil.htmlEncodeXss(request.getParameter("submissionPK"));
    String submissionStatusPK = StringUtil.htmlEncodeXss(request.getParameter("submissionStatusPK"));
    String selectedTab = StringUtil.htmlEncodeXss(request.getParameter("selectedTab"));

    GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	int subSumRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SSUM"));
	int subNewGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SNS"));
	int subComplGroupRight =  Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SCS"));
	int subAssignedGroupRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SAS"));
	int subPRGroupRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SPRS"));
	int subPIGroupRight = Integer.parseInt(grpRights.getFtrRightsByValue("MIRB_SPI"));

	boolean showNotes = false;
	if (subSumRight == 4 || subSumRight >=6) { showNotes = true; } 
	if (subNewGroupRight == 4 || subNewGroupRight >=6) { showNotes = true; } 
	if (subComplGroupRight == 4 || subComplGroupRight >=6) { showNotes = true; } 
	if (subAssignedGroupRight == 4 || subAssignedGroupRight >=6) { showNotes = true; } 
	if (subPRGroupRight == 4 || subPRGroupRight >=6) { showNotes = true; } 
	if (subPIGroupRight == 4 || subPIGroupRight >=6) { showNotes = true; }
    
    if (showNotes) {
        showNotes = submB.getSubmissionAccess(usrId, submissionPK);
    }
    
    if (submissionPK == null || submissionPK.length() == 0 
            || submissionStatusPK == null || submissionStatusPK.length() == 0
            || !showNotes) { 
%>
<script>
  self.close();
</script></form></body></html>
<%      return;
    }
    String notes = null;
    if (submStatusB.getSubmissionStatusDetails(EJBUtil.stringToNum(submissionStatusPK)) > 0) {
        notes = submStatusB.getSubmissionNotes();
    }
    if (notes == null) { notes = ""; }
%>
<table width="100%" cellspacing="0" cellpadding="0" Border="0">
<tr><td><br /></td></tr>
</table>
<table width="90%" cellspacing="0" cellpadding="0" Border="0" style="font-size:8pt" align="center">
<tr><td><br /></td></tr>
<tr align="center">
  <td><%=LC.L_Notes%><%--Notes*****--%><br/><TextArea READONLY id="notes" name="notes" rows="15" cols="50"><%=notes%></TextArea>
</tr>
<tr><td><br /></td></tr>
<tr><td align=center> 
<button onClick = "self.close()" ><%=LC.L_Close%></button> 
</td></tr>
</table>

<%    
} 
else { // session invalid
%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%
}
%>
  <div class = "myHomebottomPanel">
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
</form>
</body>
</html>
