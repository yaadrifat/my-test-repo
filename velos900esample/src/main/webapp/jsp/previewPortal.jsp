<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Preview%><%-- Preview*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>
	<%-- Nicholas : Start --%>
	<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
	<%-- Nicholas : End --%>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>


<SCRIPT>

 function  validate(formobj){ 	
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {

	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

	formobj.eSign.focus();

	return false;

   }
 }
 
 
  
function openFormPreview(formId,mode)
{
	var targetVar;
	
	if (mode == 'EF')
	{
		targetVar = "_self";
	}
	else
	{
		targetVar = "PreviewForm";
	}
	
	windowName=window.open("formpreview.jsp?formId="+formId,targetVar,"toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=825,height=600,top=90,left=150");
	windowName.focus();
}
	
</SCRIPT>

<jsp:useBean id="portalJB" scope="request" class="com.velos.eres.web.portal.PortalJB"/>
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="formB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="codeB" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page import="com.velos.eres.web.linkedForms.LinkedFormsJB,com.velos.esch.web.eventassoc.EventAssocJB,com.velos.esch.business.common.EventdefDao,com.velos.esch.business.common.CrfDao,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.text.*,com.velos.eres.business.person.impl.PersonBean,com.velos.eres.web.patProt.*,com.velos.eres.business.patProt.impl.*"%>
	<%-- Nicholas : Start --%>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<%-- Nicholas : End --%>
<style type="text/css"> 
#left { 
 display:inline;
 position: relative; overflow:auto; height:500px; }

#right { 
 display:inline;
 position: relative; overflow:auto;height:500px; 
 
}

#center { 
display:inline;
position: relative; 
left: 30%;   overflow:auto;height:500px;
    
}


</style>

<jsp:include page="include.jsp" flush="true"/> 
<div> 


    <jsp:include page="bottompanel.jsp" flush="true"/>

  </div>


<%
int ienet = 2;
boolean isEF = false;

	int perId = 0;
	perId = EJBUtil.stringToNum(request.getParameter("patientId"));
	
	String studyPk = "";
	
	int portalId = EJBUtil.stringToNum(request.getParameter("portalId")) ;//er_portal_logins.fk_portal OR er_portal.pk_portal
	HttpSession tSession = request.getSession(true);	
	//String ipAdd = (String) tSession.getValue("ipAdd");
	
	String headerStr = "";
	String footerStr = "";
	String bgColor = "";
	String textColor = "";
	String styleString = "";
	int patProtId = 0; 
	String selfLogout = "";
	String portalConsentForm = "";
	int filledformPKConsent = 0;
	String portalConsentingFormName ="";
	String userId="";	
	userId = (String) tSession.getValue("userId");
	
	PortalDao pdao = new PortalDao();
	
	portalJB.setPortalId(portalId);
	portalJB.getPortalDetails();

	studyPk = portalJB.getPortalStudy();
	
	
	int patProtProtocolId=0;
	if ((!StringUtil.isEmpty(studyPk)) && (perId > 0))
	 {
		PatProtJB pjb = new PatProtJB();
		PatProtBean pb = new PatProtBean();
		pb = pjb.findCurrentPatProtDetails(StringUtil.stringToNum(studyPk), perId);

		patProtId = pb.getPatProtId();
		// Bug #7654 AGodara
		if(pb.getPatProtProtocolId()!=null){
			patProtProtocolId = Integer.parseInt(pb.getPatProtProtocolId());
		}
		
	}
	
	// patProtProtocolId/calendarId = 0 would be passed if patient is not on any calendar/schedule
	pdao = portalJB.getDesignPreview(portalId, patProtProtocolId);

	
	headerStr = portalJB.getPortalHeader();
	footerStr = portalJB.getPortalFooter();
	bgColor = portalJB.getPortalBgColor();
	textColor = portalJB.getPortalTxtColor();
	selfLogout =  portalJB.getPortalSelfLogout();
	portalConsentForm = portalJB.getPortalConsentingForm();
	
	if (!StringUtil.isEmpty(portalConsentForm) && perId > 0)
	{
		
		
	   	int consentingCode = 0;
	   	 
        consentingCode  = codeB.getCodeId("fillformstat", "consent");
        
        Hashtable htParam = new Hashtable();
         
        htParam.put("status",String.valueOf(consentingCode) );
        
        filledformPKConsent = formB.getPatFilledFormPKForCreatorForParam(EJBUtil.stringToNum(portalConsentForm),perId,EJBUtil.stringToNum(userId), htParam);
	
		formB.setFormLibId(EJBUtil.stringToNum(portalConsentForm));
		formB.getFormLibDetails();
		portalConsentingFormName = formB.getFormLibName();
	}	
	
	if (StringUtil.isEmpty(bgColor))
	{
		bgColor = "transparent";
	}
	else
	{
		bgColor = "#"+bgColor ;
	}
	
	if (StringUtil.isEmpty(textColor))
	{
		textColor = "black";
	}
	else
	{
		textColor = "#" + textColor ;
	}
	
	
	styleString = "color:" +  textColor + ";background-color:" + bgColor + ";";
	
	
	if(StringUtil.isEmpty(selfLogout))
	{
		selfLogout = "1";
	}
	
	if (perId > 0)
	{
		tSession.setAttribute("pp_selfLogout", selfLogout);
	}

String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
   	else
	ienet = 1;

if (ienet == 0) {
styleString = styleString + "overflow:scroll";
} 
%>
<body style="<%=styleString%>">
 
<%
String tab= request.getParameter("selectedTab");
 
  
	%> 
	
	
	
	<%
	

	
	String acc="";	
	//acc = (String) tSession.getValue("accId");
	
	
	String formsAlignment = "";
	String scheduleAlignment = "";
	String alignVal = "";
	String moduleType = "";
	String moduleName = "";
	String moduleId = "";
	LinkedFormsJB lfJB = new LinkedFormsJB();
	String dispType = "";
	String entryChar = "";
	String recordType = "";
	
	ArrayList linkedModuleTypes = new ArrayList();
	ArrayList linkedModuleAlignment = new ArrayList();
	ArrayList linkedModuleNames = new ArrayList();
	ArrayList linkedModuleIds = new ArrayList();
	
	ArrayList arScheduleFrom = new ArrayList();
	ArrayList arScheduleFromUnit = new ArrayList();
	ArrayList arScheduleTo = new ArrayList();
	ArrayList arScheduleToUnit = new ArrayList();
	ArrayList arlinkedModuleFormAfter = new ArrayList();
	
	
	if (!StringUtil.isEmpty(portalConsentForm) && perId > 0 && filledformPKConsent <= 0)
	{
		linkedModuleAlignment.add("C");		
		linkedModuleTypes.add("EF");
		linkedModuleNames.add(portalConsentingFormName);
		linkedModuleIds.add(portalConsentForm);
		
		arScheduleFrom.add("");
		arScheduleFromUnit.add("");
		arScheduleTo.add("");
		arScheduleToUnit.add("");
		
		arlinkedModuleFormAfter.add("P");
		
		tSession.setAttribute("pp_consenting", portalConsentForm);
	}
	else
	{	
		linkedModuleAlignment = pdao.getLinkedModuleAlignment();
		linkedModuleTypes = pdao.getLinkedModuleTypes();
		linkedModuleNames = pdao.getLinkedModuleNames();
		linkedModuleIds= pdao.getLinkedModuleIds();
		
		arScheduleFrom = pdao.getLinkedModuleRangeFrom();
		arScheduleFromUnit = pdao.getLinkedModuleRangeFromUnit();
		arScheduleTo = pdao.getLinkedModuleRangeTo();
		arScheduleToUnit = pdao.getLinkedModuleRangeToUnit();
		arlinkedModuleFormAfter = pdao.getLinkedModuleFormAfter();
		
		tSession.removeAttribute("pp_consenting");
	}
	
	
	String scheduleFrom = "";
	String scheduleFromUnit = "";
	String scheduleTo = "";
	String scheduleToUnit = "";
	
	String linkedModuleFormAfter = "";
	
	
	StringBuffer rightDIV = new StringBuffer();
	StringBuffer centerDIV = new StringBuffer();
	StringBuffer leftDIV = new StringBuffer();
	StringBuffer sbSchedule = new StringBuffer();
	
	String htmlOut = "";
	int formCount = 0;
	String formName = "";
	int filledformPK = 0;
	
	int formStatusActiveCode = 0;
	int formStatusLockdownCode = 0;
	int currentFormStatus = 0;
	boolean allowEntry = true;
	String schedulePatProt = "";
	String isScheduleWithCRF = "false";
	
	if (perId > 0	)
	{
		formStatusActiveCode = codeB.getCodeId("frmstat", "A");
		//formStatusLockdownCode = codeB.getCodeId("frmstat", "L");
	}
		
	if (linkedModuleTypes != null)
	{
		for (int k=0;k<linkedModuleTypes.size();k++) //iterate through the data access object
		{
			
			alignVal = (String) linkedModuleAlignment.get(k);
			moduleType = (String) linkedModuleTypes.get(k);
			moduleName	= ((String) linkedModuleNames.get(k) ==null)?"":(String) linkedModuleNames.get(k);
			System.out.println("::::::::;"+moduleName);
			moduleId = (String) linkedModuleIds.get(k);
			linkedModuleFormAfter = (String) arlinkedModuleFormAfter.get(k); 
			
			scheduleFrom = (String) arScheduleFrom.get(k);
			
			if (StringUtil.isEmpty(scheduleFrom))
			{
				scheduleFrom = "";
			}
			
			
			scheduleFromUnit = (String) arScheduleFromUnit.get(k);
			
			if (StringUtil.isEmpty(scheduleFromUnit))
			{
				scheduleFromUnit = "";
			}
			
			
			scheduleTo = (String) arScheduleTo.get(k);
			
			if (StringUtil.isEmpty(scheduleTo))
			{
				scheduleTo = "";
			}
			
			
			scheduleToUnit = (String) arScheduleToUnit.get(k);
			
			if (StringUtil.isEmpty(scheduleToUnit))
			{
				scheduleToUnit = "";
			}
			
			if (StringUtil.isEmpty(alignVal))
			{
				alignVal = "L";
			}
			
			if ( moduleType.equals("EF") || moduleType.equals("LF") )  
			{
				 if ( perId>0 )
					{
						tSession.setAttribute("pp_linkedModuleFormAfter", linkedModuleFormAfter);
					}
						
				String[] arForm = null;
				
				arForm = StringUtil.chopChop(moduleId,',');
				
				if (arForm!= null && arForm.length > 0 )
				{
				
				 if (moduleType.equals("EF") )
				  {
				  	//get linked forms details
						isEF = true;			
						
						if ( perId>0 )
						{
							tSession.setAttribute("pp_isEditForm", "true");
						//check if the form response option is printer friendly format or not available, then get the resp count
							
							if (arForm[0].equals(portalConsentForm) && filledformPKConsent <= 0)
							{
								filledformPK = 0;
							}
							else
							{
								filledformPK = formB.getPatFilledFormPKForCreator(EJBUtil.stringToNum(arForm[0]),perId,EJBUtil.stringToNum(userId));
							}
						
							
							//System.out.println("filledformpk" + filledformPK)	;
							
							//get form details
							formB.setFormLibId(EJBUtil.stringToNum(arForm[0]));
							formB.getFormLibDetails();	
							
							currentFormStatus = EJBUtil.stringToNum(formB.getFormLibStatus());
											
						
						}	
							
						
						lfJB.findByFormId(EJBUtil.stringToNum(arForm[0]));
						
						dispType = lfJB.getLFDisplayType();
						entryChar = lfJB.getLFEntry();
						recordType = lfJB.getRecordType();
						
						if (StringUtil.isEmpty(studyPk) && (dispType.equals("PS") || dispType.equals("PR")) )
						{
							allowEntry = false;
						}
						else
						{
							allowEntry = true;
						}
						
						//System.out.println("recordType" + recordType);
						if ((! StringUtil.isEmpty(recordType)) &&  recordType.equals("D"))
						{
							allowEntry = false;
						}
						
				  	%>
				  		<div>
						<table width="100%"><tr><th id='TH_SCH'>
						<%=headerStr %> </th></tr></table>
						
				  	
				  	<%
				  
				  //System.out.println("perId" + perId + "linkedModuleFormAfter" + linkedModuleFormAfter + "filledformPK" + filledformPK + "entryChar" + entryChar + "lfJB.getLfDataCnt()" + lfJB.getLfDataCnt() );
				  
				    if ( perId<=0 )
					{
					%>
											
						<jsp:include page="formpreview.jsp" flush="true"> 
							<jsp:param name="formId" value="<%=arForm[0]%>"/>
						</jsp:include>
							
					<%	
						
						//htmlOut =  " <script> openFormPreview('"+arForm[0]+"','EF'); </script>";
					}
					else if (perId > 0 && (( ( linkedModuleFormAfter.equals("BL") || linkedModuleFormAfter.equals("P") ) &&  filledformPK == 0 ) || ( linkedModuleFormAfter.equals("N")  && entryChar.equals("M")) || ( (linkedModuleFormAfter.equals("N") || linkedModuleFormAfter.equals("BL") ) && entryChar.equals("E") &&  filledformPK == 0)) )//for patient data entry 
					{
						String formstr	= "";
						
						formstr = arForm[0] + "*" + entryChar + "*" + lfJB.getLfDataCnt();			

					  if (currentFormStatus == formStatusActiveCode && (allowEntry == true))
						{
						
						if (dispType.equals("PA") )
						{
						%>
							<jsp:include page="patformdetails.jsp" flush="true"> 
								<jsp:param name="formId" value="<%=arForm[0]%>"/>
								<jsp:param name="formFillDt" value="ALL"/>
								<jsp:param name="entryChar" value="<%=entryChar%>"/>
								<jsp:param name="mode" value="N"/>
								<jsp:param name="formDispLocation" value="PA"/>
								<jsp:param name="calledFromForm" value="formpreview"/>
								<jsp:param name="formPullDown" value="<%=formstr%>"/>
								<jsp:param name="pkey" value="<%=perId%>"/>
							</jsp:include>
						 
						<%
						} else if (dispType.equals("PS") || dispType.equals("SP") || dispType.equals("PR"))
						{
						%>
							<jsp:include page="patstudyformdetails.jsp" flush="true"> 
								<jsp:param name="formId" value="<%=arForm[0]%>"/>
								<jsp:param name="formFillDt" value="ALL"/>
								<jsp:param name="entryChar" value="<%=entryChar%>"/>
								<jsp:param name="mode" value="N"/>
								<jsp:param name="formDispLocation" value="SP"/>
								<jsp:param name="calledFromForm" value="formpreview"/>
								<jsp:param name="formPullDown" value="<%=formstr%>"/>
								<jsp:param name="pkey" value="<%=perId%>"/>
								<jsp:param name="calledFrom" value="S"/>
								<jsp:param name="patProtId" value="<%=patProtId%>"/>
								<jsp:param name="studyId" value="<%=studyPk%>"/>
							</jsp:include>
						
						<%
						}
						
					  } //form status check
					  else
							{
								%>
									<jsp:include page="formaccesserror.jsp" flush="true"></jsp:include>
								<%
							}	
						
 						
					} else if (perId > 0 && ( ( linkedModuleFormAfter.equals("N") && entryChar.equals("E") ) || linkedModuleFormAfter.equals("P") || linkedModuleFormAfter.equals("BL") ) && filledformPK > 0 )//for patient data entry
					{
						
						//include printer friendly format
						%>
						<p class="sectionHeadings"><%=MC.M_FrmAldySbmt_ThankYou%><%-- You have already submitted this form. Thank you!*****--%></p>
						<%
						
						if (linkedModuleFormAfter.equals("P"))
						{
							if (currentFormStatus == formStatusActiveCode && (allowEntry == true)  )
							{
								%>
									<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <A href="#" onClick="openPrintWinCommon('<%=arForm[0]%>','<%=filledformPK%>','<%=dispType%>','<%=studyPk%>','<%=perId%>','<%=patProtId%>','','','')" ><%=LC.L_Print%><%-- Print*****--%></A> <%=LC.L_Your_Resp%><%-- your response*****--%>
									 <BR>
								
								<%
							}
							else
							{
								%>
									<jsp:include page="formaccesserror.jsp" flush="true"></jsp:include>
								<%
							}
						}
						
					}
					
				%>
					
						<BR><%=footerStr %>
						</div>
				
				<%
				} //for EF
				else
				{
				
					if (perId > 0)
					{
						tSession.setAttribute("pp_isEditForm", "false");
					}
					if (arForm.length  > 0)
						{
						 	/*htmlOut =  "<table border = 0 ><tr><td><font style=\"font-family: Arial, Helvetica, sans-serif; font-size: 10pt; font-style: normal; line-height: normal; font-weight: normal; font-variant: normal; text-transform: none;\">Your Forms: </font>";*****/
						 	htmlOut =  "<table border = 0 ><tr><td><font style=\"font-family: Arial, Helvetica, sans-serif; font-size: 10pt; font-style: normal; line-height: normal; font-weight: normal; font-variant: normal; text-transform: none;\">"+LC.L_Your_Forms+": </font>";
							
						}
						else
						{
							htmlOut =  "";
							
						}
					for ( int z = 0; z<arForm.length; z++)
					{ 
						
						
						//get linked forms details
						
						
						lfJB.findByFormId(EJBUtil.stringToNum(arForm[z]));
						
						dispType = lfJB.getLFDisplayType();
						entryChar = lfJB.getLFEntry();
						recordType = lfJB.getRecordType();
						
						formB.setFormLibId(EJBUtil.stringToNum(arForm[z]));
						formB.getFormLibDetails();	
						currentFormStatus = EJBUtil.stringToNum(formB.getFormLibStatus());
						
						if (StringUtil.isEmpty(formsAlignment) )
						{
						 formsAlignment = alignVal;
						} 
						
						if (StringUtil.isEmpty(studyPk) && (dispType.equals("PS") || dispType.equals("PR")) )
						{
							allowEntry = false;
						}
						else
						{
							allowEntry = true;
						}
						//System.out.println("recordType2" + recordType);
						
						if ((! StringUtil.isEmpty(recordType)) &&  recordType.equals("D"))
						{
							allowEntry = false;
						}
						
						if (perId<=0)
						{
						 	htmlOut = htmlOut + "<BR><A href=\"#\" onClick=\"openFormPreview('"+arForm[z]+"','LF')\" >"+ formB.getFormLibName() +"</A>";
						}
						else //the form can be answered
						{
							filledformPK = formB.getPatFilledFormPKForCreator(EJBUtil.stringToNum(arForm[z]),perId,EJBUtil.stringToNum(userId));
							
							if (entryChar.equals("E") && filledformPK > 0)
							{
								
								
								//System.out.println("filledformPK" + filledformPK);
								
									 
								htmlOut = htmlOut + "<BR><BR><b>" + formB.getFormLibName() + "</b> : "+MC.M_FrmAldySbmt_ThankYou;/*htmlOut = htmlOut + "<BR><BR><b>" + formB.getFormLibName() + "</b> : You have already submitted this form. Thank you!";*****/
								
								if (linkedModuleFormAfter.equals("P") )
								{
									if (currentFormStatus == formStatusActiveCode && (allowEntry == true))
									{
						
										/*htmlOut = htmlOut +
										" <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href=\"#\" onClick=\"openPrintWinCommon('" + arForm[z] +"','" + filledformPK +"','"+ dispType + "','" +
										 studyPk +"','" + perId +"','" +  patProtId +"','','','')\" >Print</A> your response<BR>";*****/
										htmlOut = htmlOut +
										" <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href=\"#\" onClick=\"openPrintWinCommon('" + arForm[z] +"','" + filledformPK +"','"+ dispType + "','" +
										 studyPk +"','" + perId +"','" +  patProtId +"','','','')\" >"+LC.L_Print+"</A> "+LC.L_Your_Resp+"<BR>";
									} 
									else
									{
										htmlOut = htmlOut + MC.M_FrmInActv_NoAcesRgt;/*htmlOut = htmlOut + "<BR>This form is currently not 'Active' or you do not have access rights to view the form. Printer Friendly Format not available";*****/
									}
							  
								}
							
								
							}
							else //multiple entry
							{		
								if (currentFormStatus == formStatusActiveCode && (allowEntry == true))
									{
										htmlOut = htmlOut + "<BR><A href=\"#\" onClick=\"fk_sch_events1=''; return openlinkedForm('"+arForm[z]+"','"+entryChar+"','"+dispType+"',document.preview)\" >"+ formB.getFormLibName() +"</A>";
									}
									else
									{
										htmlOut = htmlOut + "<BR><BR><b>" + formB.getFormLibName() + "</b>:  "+MC.M_FrmNotAval_ForEntry;/*htmlOut = htmlOut + "<BR><BR><b>" + formB.getFormLibName() + "</b>:  This form is currently not available for data entry";*****/ 
									}	
							}	
							
						}
					} //for loop
					if (arForm.length  > 0)
					{
						htmlOut = htmlOut+"</td></tr></table>";
					}
					
				  } //check for EF	
				} //null check
				
				
				  if (alignVal.equals("L"))
				  {
				  	leftDIV.append(htmlOut);
				  }else if (alignVal.equals("C"))
				  {
				  	centerDIV.append(htmlOut);
				  } 
				  else //right
				  {
				  	rightDIV.append(htmlOut);
				  }
				
		 } // module type
			
			if ( moduleType.equals("S") || moduleType.equals("SF") )
			{
				htmlOut = "";
				
				
				 if (StringUtil.isEmpty(scheduleAlignment))
				 { 
				 	scheduleAlignment = alignVal;
				 }
				 
				 htmlOut="<font style=\"font-family: Arial, Helvetica, sans-serif; font-size: 10pt; font-style: normal; line-height: normal; font-weight: bold; font-variant: normal; text-transform: none;\">";
				 if (perId <= 0)
				 {
					 htmlOut = htmlOut + LC.L_Protocol_Calendar+": "+ moduleName ;/*htmlOut = htmlOut + "Protocol Calendar: "+ moduleName ;*****/
				 }
				 else
				 {
					 htmlOut = htmlOut + MC.M_Apmt_OnCal+" '"+moduleName+"' :";/*htmlOut = htmlOut + "Your appointments:";*****/
				 }
				 
				 htmlOut= htmlOut+"</font>";
				 //get study linked with the calendar
				 
				 /*EventAssocJB ev = new EventAssocJB();
				 ev.setEvent_id(EJBUtil.stringToNum(moduleId));
				 ev.getEventAssocDetails();
				 studyPk = ev.getChain_id();*/ 
				
				EventdefDao evd = new  EventdefDao();
				Hashtable htParam = new Hashtable();
				 
				htParam.put("scheduleFrom" ,scheduleFrom);
				htParam.put("scheduleFromUnit" ,scheduleFromUnit);
				htParam.put("scheduleTo" ,scheduleTo);
				htParam.put("scheduleToUnit" ,scheduleToUnit);
				
				
				//get schedule 
				evd.FetchPatientSchedule(EJBUtil.stringToNum(moduleId),perId,htParam);
				
				ArrayList arEventNames = new ArrayList();
				ArrayList arVisitNames = new ArrayList();
				ArrayList<String> arSchDates = new ArrayList<String>();       // Modifiedy by Akshi for Bug 7679
				ArrayList arStatuses = new ArrayList();
				ArrayList arNotes = new ArrayList();
				ArrayList arEventIds = new ArrayList();
				Hashtable htCRF = new Hashtable();
				ArrayList arFkVisits = new ArrayList();
				ArrayList arAssocIds = new ArrayList();
				
				
				ArrayList arSchedulePatProtIds = new ArrayList();
							
				arEventNames = evd.getNames();
				arVisitNames = evd.getVisitName();
				arSchDates = evd.getActualDateString();
				// Modifiedy by Akshi for Bug 7679				
				for(int i=0;i<arSchDates.size();i++)
				{
					String dateVar=(String)arSchDates.get(i);
					if(dateVar==null)
					{
						arSchDates.remove(i);
						arSchDates.add(i,LC.L_Not_Defined);						
					}
				}				
				evd.setActualDateString(arSchDates);
				arSchDates = evd.getActualDateString();				
				// -- End of Bug 7679
				arStatuses = evd.getStatus();
				arNotes = evd.getNotes();
				arEventIds = evd.getEvent_ids();
				arSchedulePatProtIds = evd.getPatprotId();
				arFkVisits = evd.getFkVisit();
				arAssocIds = evd.getOrg_ids();//JM: 08DEC2010, Enh PP-1
				
				EventAssocJB evJB = new EventAssocJB();
				
				int evCount = 0;
				String oldVisit = "";
				String newVisit = "";
				String displayVisit = "";
				String visitName = "";
				String notes="";
				Integer ev_id = null;
				int evCRFCount = 0;
				Integer assocId = null;
				
				
				ArrayList crfIds = new ArrayList();
				ArrayList crfNames = new ArrayList();
				ArrayList crfformEntryChars = new ArrayList();
				ArrayList crfLinkedFormIds = new ArrayList();
				ArrayList crfformSavedCounts = new ArrayList(); 
				ArrayList arCRFFormStatuses =  new ArrayList();
				ArrayList crfFormTypes = new ArrayList();
				
				ArrayList crfLinkedFormRecordTypes = new ArrayList();
				
				Integer crfId = null;
				String crfName = "";
				String crfFormEntryChar = "";
				String crfformSavedCount = "";
				String crfLinkedForm = "";
				String crfString = "";
				String crfHrf = "";
				String crfType = "";
				String event_id = "";
				boolean showNewFormLink = false;
				String formRecordType = "";
				
				evCount = arEventNames.size();
				
				htCRF = evd.getHtScheduleCRF();
				
				
				sbSchedule.append("<table width=\"90%\"   border = 0>");
				sbSchedule.append("<tr id='TH_SCH'><th id='TH_SCH'>"+LC.L_Visit+"</th><th id='TH_SCH'>"+LC.L_Scheduled_Date+"</th><th id='TH_SCH'>"+LC.L_Event_Name+"</th><th id='TH_SCH'>"+LC.L_Status+"</th><th id='TH_SCH'>"+LC.L_Notes+"</th>");
				/*sbSchedule.append("<tr id='TH_SCH'><th id='TH_SCH'>Visit</th><th id='TH_SCH'>Scheduled Date</th><th id='TH_SCH'>Event Name</th><th id='TH_SCH'>Status</th><th id='TH_SCH'>Notes</th>");*****/
				
				if (moduleType.equals("SF")) //show CRF only for Schedule with CRF option
					{
					sbSchedule.append("<th id='TH_SCH'>"+LC.L_Forms+"</th>");/*sbSchedule.append("<th id='TH_SCH'>Forms</th>");*****/
					}	
					
				sbSchedule.append("</tr>");	
				
				
				for (int i=0;i<evCount;i++)
				{
				
					newVisit = (String) arFkVisits.get(i);
					visitName = (String) arVisitNames.get(i);
					
					
					notes =  (String) arNotes.get(i);
					ev_id = (Integer)arEventIds.get(i);
					event_id = ev_id.toString();
					
					assocId = (Integer)arAssocIds.get(i);//JM: 08DEC2010, Enh PP-1
					
					if (i==0)
					{
						schedulePatProt = (String)arSchedulePatProtIds.get(i);
						
						
					}	
					
					if (StringUtil.isEmpty(notes))
					{
						notes = "&nbsp;";
					}
										
					if (!oldVisit.equals(newVisit))
					{
						displayVisit = visitName;
					}
					else
					{
						displayVisit = "&nbsp;";
					}
					
					if (moduleType.equals("SF")) //show CRF only for Schedule with CRF option
					{
					
					 isScheduleWithCRF = "true"; //flag that a schedule with CRF is included
					 
					 tSession.setAttribute("pp_scheduleCRF", "true");
					 
					//see if there is any CRF info
						if (htCRF.containsKey(ev_id))
						{
								CrfDao evCrf = new  CrfDao();
					      		evCrf = (CrfDao) htCRF.get(ev_id);
					      		
					      			      		
					      		
					      		//Prepare CRF STRING
					      		
					      		crfIds = evCrf.getCrfIds();		
					
					      		crfNames = evCrf.getCrfNames();		
					      		crfformEntryChars = evCrf.getCrfformEntryChars();
					      		crfLinkedFormIds = evCrf.getCrfLinkedFormIds();
					      		crfformSavedCounts = evCrf.getCrfformSavedCounts();
					      		arCRFFormStatuses = evCrf.getFormStatus();	
					      				
					      		evCRFCount = crfIds.size();
					      		crfFormTypes = evCrf.getCrfFormType();
					      		
					      		crfLinkedFormRecordTypes = evCrf.getLinkedFormRecordTypes(); 
								
								//System.out.println("evCRFCount " + evCRFCount );
									
					      		crfString = "";
					      		crfHrf = "";
							for (int p = 0; p < evCRFCount; p++)
					      		{
					      			crfName = "";
					      			
					      			crfFormEntryChar = "";
					      			crfLinkedForm = "";
									crfformSavedCount = "";
					
					      		    crfId = (Integer) crfIds.get(p);			      		    
					      			crfName = (String) crfNames.get(p);				
										
					      			crfLinkedForm = (String) crfLinkedFormIds.get(p);	
					      			
					      			
					
							if (!crfLinkedForm.equals("0"))
							 {
					      			crfFormEntryChar = (String)	crfformEntryChars.get(p);
					      			crfformSavedCount = (String)crfformSavedCounts.get(p);
					      			
					      			currentFormStatus = EJBUtil.stringToNum((String) arCRFFormStatuses.get(p));
					      			formRecordType = (String) crfLinkedFormRecordTypes.get(p);
					      			
					      			if (StringUtil.isEmpty(formRecordType))
					      			{
					      				formRecordType = "N";
					      			}
					      			
					      			if (formRecordType.equals("D"))
					      			{
					      			  currentFormStatus = 0; //so that the form cannot be answered
					      			}
					      			
					      			//System.out.println("crfformSavedCount" + crfformSavedCount);								
					      			
					      			if (currentFormStatus == formStatusActiveCode)
					      			{
						      			if (StringUtil.isEmpty(crfformSavedCount))
						      			{
						      				crfformSavedCount = "<i>("+MC.M_NoResponseEntered+")</i>";/*crfformSavedCount = "<i>(No Response Entered)</i>";*****/
						      				showNewFormLink = true;
						      			}
						      			else
						      			{
						      				if (crfFormEntryChar.equals("M"))
						      					showNewFormLink = true;
						      				else
						      					showNewFormLink = false;
						      			}
						      		}
						      		else
						      		{
						      			crfformSavedCount = "<i>"+MC.M_FrmNotAval_ForEntry+"</i>";/*crfformSavedCount = "<i>This form is currently not available for data entry</i>";*****/
						      				showNewFormLink = false;
						      		
						      		}	
													
					      			crfLinkedForm = EJBUtil.isEmpty(crfLinkedForm) ? "0" : crfLinkedForm;
					      			crfFormEntryChar = EJBUtil.isEmpty(crfFormEntryChar) ? "0" : crfFormEntryChar;
					
									crfType = (String) crfFormTypes.get(p);
									
								 
										
											  if (showNewFormLink)
											  {							
									 			crfHrf = "<A onClick=\" calledFromSchedule = true; fk_sch_events1 = " +event_id + "; return openlinkedForm("+crfLinkedForm+",'"+crfFormEntryChar+"','"+crfType+"',document.preview)\" href=\"Javascript:void(0)\">"+crfName +"</A> ";
									 		  }
									 		  else
									 		  {
									 		  	crfHrf = "<b>" + crfName + "</b>";
									 		  }	
											
											  int showForm = evJB.getEventFormAccees(EJBUtil.stringToNum(crfLinkedForm), assocId.intValue(), portalId );
											  
											 
											  if (showForm == 1){
											  		crfString= crfString + "<BR>" + crfHrf + "<BR>&nbsp;&nbsp;&nbsp;" + crfformSavedCount  ;
											  }
											
											
										
									}			    
									
					      		}	// end of for loop
									
							}
							else
							{
								crfString = "&nbsp; "; 
							}      		
										
					}
					else
					{
						isScheduleWithCRF = "false";
					}
					
					// end if CRF info
					
				if(i%2==0){ 	
					sbSchedule.append("<TR class=\"browserEvenRow\">");	
				}else{
					sbSchedule.append("<TR class=\"browserOddRow\">");
				}
					
					sbSchedule.append(" <td>"+ displayVisit +"</td><td>"+ arSchDates.get(i) +"</td><td>"+ arEventNames.get(i)  +"</td><td>"+ arStatuses.get(i) +"</td><td>"+ notes +"</td>");
					
					if (moduleType.equals("SF")) //show CRF only for Schedule with CRF option
					{
						sbSchedule.append("<td>" + crfString+ "</td>");
					}	
					
					sbSchedule.append("</tr> ");
					
					oldVisit =newVisit ;
					 
				}	
				%>
				
				
				
				
				<%
				
				if (evCount ==0)
				{
					sbSchedule.append("<TR class=\"browserOddRow\" ><td colspan=5>"+MC.M_NoSch_Avail+"</td></TR>");/*sbSchedule.append("<TR class=\"browserOddRow\" ><td colspan=5>No Schedule Available</td></TR>");*****/
				}		
				sbSchedule.append("</table>");
				
				htmlOut = htmlOut + sbSchedule.toString();
				
						
				  if (alignVal.equals("L"))
				  {
				  	leftDIV.append(htmlOut);
				  }else if (alignVal.equals("C"))
				  {
				  	centerDIV.append(htmlOut);
				  } 
				  else //right
				  {
				  	rightDIV.append(htmlOut);
				  } 	
			}
			
			
		  
		
		}
		
	}	
	
	
	%>
	
<BR>

<form name="preview" METHOD="POST">
	
	<input type="hidden" name="formStudy" value="<%=studyPk%>" />
	<input type="hidden" name="patStudyId" value="<%=studyPk%>" />
	<input type="hidden" name="formPatient" value="<%=perId%>" />
	<input type="hidden" name="formPatprot" value="<%=patProtId%>" />
	<input type="hidden" name="schedulePatprot" value="<%=schedulePatProt%>" />
	
</form>


 <div>

<% if (!isEF) { %> 
 <table width="100%"><tr><th id='TH_SCH'>
		<%=headerStr %> </th></tr></table>
		 
 <table width="100%" border =0>
 <tr>
	<%
		if (! StringUtil.isEmpty(leftDIV.toString()))
		{
			%>
				<td   >
					<%= leftDIV.toString() %>
				</td>
			
			
			<%
		
		
		}
	
	%>
	  
				
	<%
		if (! StringUtil.isEmpty(rightDIV.toString()))
		{
			%>
				<td   >
					<%= rightDIV.toString() %>
				</td>
			
			
			<%
		
		
		}
	
	%>		
		 
	 
	<%
		if (! StringUtil.isEmpty(centerDIV.toString()))
		{
			%>
				<td align="center"  >
					<%= centerDIV.toString() %> 
				</td>
			
			
			<%
		
		
		}
	
	%>
	</tr>
	
</table>
<BR><BR>
<table width="100%"><tr><th id='TH_SCH'>
						<%=footerStr%></th></tr></table>
						

<% } //for isEF
%>


<BR>
<table width="100%">
<tr><td align="center"><b><%=LC.L_Powered_By%><%-- Powered By*****--%></b><BR><img src="./images/velos.jpg" alt="<%-- Velos eResearch*****--%><%=LC.L_Velos_Eres%>" border="0"/> 
	 
	</td></tr>
</table>


<% if (isScheduleWithCRF.equals("true") && (! StringUtil.isEmpty(schedulePatProt)) ) 
	{	//if CRF module is included and there is a patprot related with schedule, then set the hidden field with schedule's patprot id
		
	
		%>
			<script>
				document.preview.formPatprot.value = document.preview.schedulePatprot.value; 						
			</script>
		
		<%
		
	}%>

<BR>

  

 </div>
  

</body>
</html>
