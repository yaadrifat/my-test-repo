<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Del_Bgt%><%--Delete Budget*****--%></title>

<!-- YK 05Aug11: Removed for Bug #6728 already using Whichcss_skin.js in panel.jsp -->
<SCRIPT>
function  validate(formobj){
if (!(validate_col('e-Signature',formobj.eSign))) return false;

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
   return true;
  
}


</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<jsp:useBean id="budgetB" scope="page" class="com.velos.esch.web.budget.BudgetJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 

<% String src;
	src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  


<BODY> 
<br>

<DIV class="formDefault" id="div1">
<% 
	
	String budgetId="";	
	
	HttpSession tSession = request.getSession(true); 

	 if (sessionmaint.isValidSession(tSession))	{
  	 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 	
		budgetId=request.getParameter("budgetId");
		
		int ret=0;
		
		String delMode=request.getParameter("delMode");
		if (delMode==null) {
			delMode="final";
%>

	<FORM name="bgtdelete" id="bdgdelfrmid" method="post" action="budgetdelete.jsp" onSubmit="if (validate(document.bgtdelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="budgetId" value="<%=budgetId%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">	 
	<br><br>

		<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
		
<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="bdgdelfrmid"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {	

		budgetB.setBudgetId(EJBUtil.stringToNum(budgetId));
		budgetB.getBudgetDetails();
		budgetB.setBudgetDelFlag("Y");
		//Modified for INF-18183 ::: Akshi
		ret=budgetB.updateBudget(AuditUtils.createArgs(session,"",LC.L_Budget));		
		budgetB.setBudgetId(EJBUtil.stringToNum(budgetId));
		budgetB.getBudgetDetails();

		if (ret >= 0) {
		%>
		<br><br><br><br><br><br>
		
		<p class = "successfulmsg" align = center> <%=MC.M_Data_DelSucc%><%--Data deleted successfully.*****--%> </p>
		<META HTTP-EQUIV=Refresh CONTENT="1; URL=budgetbrowserpg.jsp?srcmenu=<%=src%>">
		<%}%>
			
<%	
		} //end esign
	} //end of delMode	
 }//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
<div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>
<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/> 
</div>

</body>
</HTML>
