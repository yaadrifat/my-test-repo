<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>
<%@page import="com.velos.eres.business.common.CodeDao"%>
<%@page import="com.velos.eres.service.util.VelosResourceBundle"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>
<%
int ctrpRights=0;
try {
	GrpRightsJB grpRights = (GrpRightsJB) request.getSession(false).getAttribute("GRights");
	if (grpRights == null) { return; }
	ctrpRights=Integer.parseInt(grpRights.getFtrRightsByValue("REGREPORT_CTRP"));
	if (!isAccessibleFor(ctrpRights, 'V')){ 
		out.println("<div><p>&nbsp;</p><p class='sectionHeadings' align='center'>"+LC.L_Access_Forbidden+"</p></div>");
		return;
	}
} catch(Exception e) { return; } 
%>
<%
CodeDao cdDraftStatus = new CodeDao();
String readyStat = cdDraftStatus.getCodeDescription(cdDraftStatus.getCodeId("ctrpDraftStatus", "readySubmission"));
Object[] params = { readyStat };				
%>
<jsp:include page="ctrpDraftIndustrialInc.jsp" flush="false"></jsp:include>
<jsp:include page="ui-include.jsp" flush="false"></jsp:include>
<jsp:useBean id="ctrpDraftJB"  scope="request" class="com.velos.eres.ctrp.web.CtrpDraftJB"/>

<s:form id="ctrpDraftIndustrialForm" name="ctrpDraftIndustrialForm" action="#" onsubmit="return validateForm();" method="post"> 
	<%
	String draftId = request.getParameter("draftId");
	if (draftId == null) { draftId = request.getParameter("ctrpDraftJB.id"); }
	Integer flag = ctrpDraftJB.getHideeSign();
	String saved = null;
	saved = request.getParameter("saved");
	
	%>
	<div id="ctrp_draftValidation" style="padding:2px; width:99%">
		<table width="100%">
			<tr>
				<td width="70%">&nbsp;</td>
				<%if (draftId != null){ %>
				<td>
					<a href="javascript:void(0);" onClick="return runCtrpValidationReport('<%=draftId%>');" style="valign:right"><img border="0" title="<%=LC.L_Printer_Friendly%>" alt="<%=LC.L_Printer_Friendly%>" src="./images/printer.gif"/></a>
				</td>
				<%} %>
				<%if (flag == null || flag == 0){ %>
					<%if (((draftId == null || draftId == "0") && isAccessibleFor(ctrpRights, 'N'))
	   					|| (draftId != null && isAccessibleFor(ctrpRights, 'E'))){%>
				<td>
				&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					<a href="javascript:void(0);" onClick="validateThisDraft();" style="valign:right"><%=LC.CTRP_DraftValidateDraft%></a>
				</td>
					<%} %>
				<%} %>
			</tr>
		</table>
	</div>
	<div class="col_100 maincontainer column">
		<%-- Section: Trial Submission Type --%>
		<div id="ctrp_draft1_section" class="portlet portletstatus ui-widget ui-widget-border
				ui-helper-clearfix ui-corner-all" >
			<div id="ctrp_draft1content" onclick="toggleDiv('ctrp_draft1')" class="portlet-header portletstatus 
					ui-widget ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all">
				<span class="ui-icon ui-icon-triangle-1-s"></span>
				<%=LC.CTRP_DraftTrialSubmSec %>
			</div>
			<div id="ctrp_draft1" style="padding:2px; width:99%">
				<jsp:include page="ctrpDraftIndusTrialSubmission.jsp" />
			</div>
		</div>

		<%-- Section: Trial Identifiers --%>
		<div id="ctrp_draft2_section" class="portlet portletstatus ui-widget ui-widget-border
				ui-helper-clearfix ui-corner-all" >
			<div id="ctrp_draft2content" onclick="toggleDiv('ctrp_draft2')" class="portlet-header portletstatus 
					ui-widget ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all">
				<span class="ui-icon ui-icon-triangle-1-s"></span>
				<%=LC.CTRP_DraftTrialIdenti %>
			</div>
			<div id="ctrp_draft2" style="padding:2px; width:99%">
				<jsp:include page="ctrpDraftIndusTrialIdentify.jsp" />
			</div>
		</div>

		<%-- Section: Trial Details --%>
		<div id="ctrp_draft3_section" class="portlet portletstatus ui-widget ui-widget-border
				ui-helper-clearfix ui-corner-all" >
			<div id="ctrp_draft3content" onclick="toggleDiv('ctrp_draft3')" class="portlet-header portletstatus 
					ui-widget ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all">
				<span class="ui-icon ui-icon-triangle-1-s"></span>
				<%=LC.CTRP_DraftSectTrialDetails %>
			</div>
			<div id="ctrp_draft3" style="padding:2px; width:99%">
				<jsp:include page="ctrpDraftIndusTrialDetails.jsp" />
			</div>
		</div>
		<%-- Section: Lead or Submitting Organizations / Principal Investigator --%>
		<div id="ctrp_draft4_section" class="portlet portletstatus ui-widget ui-widget-border
				ui-helper-clearfix ui-corner-all" >
			<div id="ctrp_draft4content" onclick="toggleDiv('ctrp_draft4')" class="portlet-header portletstatus 
					ui-widget ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all">
				<span class="ui-icon ui-icon-triangle-1-s"></span>
				<%=MC.CTRP_DraftLeadSubOrgorPiInvst %> 
			</div>
			<div id="ctrp_draft4" style="padding:2px; width:99%">
				<jsp:include page="ctrpDraftIndusTrialOrgPrincInvs.jsp" />
			</div>
		</div>
		<%-- Section: Summary 4 Information (for trials as NCI designated centers) --%>
		<div id="ctrp_draft5_section" class="portlet portletstatus ui-widget ui-widget-border
				ui-helper-clearfix ui-corner-all" >
			<div id="ctrp_draft5content" onclick="toggleDiv('ctrp_draft5')" class="portlet-header portletstatus 
					ui-widget ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all">
				<span class="ui-icon ui-icon-triangle-1-s"></span>
				<%=LC.CTRP_DraftSectSumm4Info %>
			</div>
			<div id="ctrp_draft5" style="padding:2px; width:99%">
				<jsp:include page="ctrpDraftIndusTrialSumm4.jsp" />
			</div>
		</div>
		<%-- Section: Status/Dates (site specific) --%>
		<div id="ctrp_draft6_section" class="portlet portletstatus ui-widget ui-widget-border
				ui-helper-clearfix ui-corner-all" >
			<div id="ctrp_draft6content" onclick="toggleDiv('ctrp_draft6')" class="portlet-header portletstatus 
					ui-widget ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all">
				<span class="ui-icon ui-icon-triangle-1-s"></span>
				<%=LC.CTRP_DraftSectStatusDates %>
			</div>
			<div id="ctrp_draft6" style="padding:2px; width:99%">
				<jsp:include page="ctrpDraftIndusTrialStatusDate.jsp" />
			</div>
		</div>
		<%-- Section: Trial Related Documents --%>
		<div id="ctrp_draft7_section" class="portlet portletstatus ui-widget ui-widget-border
				ui-helper-clearfix ui-corner-all" >
			<div id="ctrp_draft7content" onclick="toggleDiv('ctrp_draft7')" class="portlet-header portletstatus 
					ui-widget ui-widget-header ui-widget-content ui-helper-clearfix ui-corner-all">
				<span class="ui-icon ui-icon-triangle-1-s"></span>
				<%=LC.CTRP_DraftSectTrialDocs %>
			</div>
			<div id="ctrp_draft7" style="padding:2px; width:99%">
				<jsp:include page="ctrpDraftIndusTrialDocs.jsp" />
			</div>
		</div>

	</div> <%-- End of col_100 maincontain div --%>
	<br/>
	<%if (flag == null || flag == 0) { %>
	<%if (((draftId == null || draftId == "0") && isAccessibleFor(ctrpRights, 'N'))
	   || (draftId != null && isAccessibleFor(ctrpRights, 'E'))){%>
	<%-- Draft Status and e-Sign & submit bar --%>
	<table width="90%" cellpadding="2px">
		<tr><td colspan="5">&nbsp;</td></tr>
		<tr>
			<td align="right" width="44%"><%=LC.CTRP_DraftStatus %>&nbsp;&nbsp;&nbsp;</td>
			<td>&nbsp;</td>
			<td>
				<s:property escape="false" value="ctrpDraftJB.draftStatusMenu" />
			</td>
			<td>
				<button type="button" id="validateDraft" name="validateDraft" 
					onClick="validateThisDraft();"><%=LC.CTRP_DraftValidateDraft%></button>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td width="45%" align="right">
				<span id="eSignMessage"></span>&nbsp;
				<%=LC.L_Esignature %><FONT class="Mandatory">* </FONT>&nbsp;
			</td>
			<td>
			</td>
			<td width="220em" align="left">
				<span id="ctrpDraftJB.eSign_error" class="errorMessage" style="display:none; text-align:left"></span>
				<input type="password" name="eSign" id="eSign" maxlength="8"
					onkeyup="ajaxvalidate('misc:'+this.id,4,'eSignMessage','<%=LC.L_Valid_Esign %>','<%=LC.L_Invalid_Esign %>','sessUserId')"
					onkeypress="return fnOnceEnterKeyPress(event)" />
	   		</td>
			<td>
				<button type="submit" id="submit_btn" onclick="return fnClickOnce(event)" ondblclick="return false"><%=LC.L_Save%></button>
			</td>
			<td width="22%">&nbsp;</td>
		</tr>
	</table>
	<%} %>
	<%} else { %>
		<table width="100%">
			<tr>
			<td align="left">
				<p class = "sectionHeadings">
					<%=VelosResourceBundle.getMessageString("CTRP_DraftReadyNoChange",readyStat)%>
				</p>
			</td>
			</tr>
		</table>
	<%} %>
	<br/>

	<%-- Hidden fields --%>
	<s:hidden name="ctrpDraftJB.id" id="ctrpDraftJB.id" />
	<s:hidden name="ctrpDraftJB.fkStudy" id="ctrpDraftJB.fkStudy" />
	<s:if test="hasActionErrors()">
		<script> 
			$j(document).ready(function(){
				showUpdateDraftDialog(false);
			});
		</script>
	</s:if>
	<s:if test="!hasActionErrors()">		
		<script>
			$j(document).ready(function(){
				<%if (draftId != null && saved != null){%>
					showUpdateDraftDialog(true);
				<%}%>
			});
		</script>
	</s:if> 
</s:form>