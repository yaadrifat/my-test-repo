<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Select_StartDay%><%--Select Start Day*****--%></title>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></script>

<SCRIPT LANGUAGE="JavaScript">


	function fselect(day,weekNum)

	{

		if (document.layers) 

		{

		window.opener.document.div1.document.enroll.selDay.value = day;

		window.opener.document.div1.document.enroll.dispSelDay.value = weekNum;

		}

		else

		{

		window.opener.document.enroll.selDay.value = day;

		window.opener.document.enroll.dispSelDay.value = weekNum;

		}

		self.close();

	} 

	

</SCRIPT>





</head>

<%

int ienet = 2;



String agent1 = request.getHeader("USER-AGENT");

   if (agent1 != null && agent1.indexOf("MSIE") != -1) 

     ienet = 0; //IE

    else

	ienet = 1;

	if(ienet == 0) {	

%>

<body style="overflow:scroll;">

<%

	} else {

%>

<body>

<%

	}

%>





<jsp:useBean id="EvJB" scope="session" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>



<%@ page language = "java" import="com.velos.eres.web.reports.ReportsDao,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.esch.business.common.*"%>

<%

HttpSession tSession = request.getSession(true);

StringBuffer output = new StringBuffer();



boolean noDataFlag = false;

if (sessionmaint.isValidSession(tSession))

{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String protId=request.getParameter("protId");

	

	EventAssocDao evDao = new EventAssocDao(); 



	evDao=EvJB.fetchCalTemplate(protId);



	

	output.append("<BR>");



////scheduling reports



ArrayList eventIds;

ArrayList eventNames;

ArrayList eventDurations;

ArrayList fuzzyPeriods;

ArrayList eventDisplacements;



String eventId;

String eventName;

String eventDuration;

String eventDisplacement;

String prevEventDisplacement = "";



String protName;

String protDuration;



int rows = 0;

int i = 0;

int actDay = 0;

int weekNum = 0;

String dispStr = "";

int insideNewRowTest = 0;



				eventIds = evDao.getEvent_ids();

				eventNames = evDao.getNames();

				eventDisplacements = evDao.getDisplacements();

	

				protName=evDao.getProtName();

				protDuration=evDao.getProtDuration();

				

				int rowCount=1;

				Integer duration=new Integer(protDuration);

				if(eventIds.size()==0){

					rows=0;}

					else{

					rows=eventIds.size();

					}

	//Take out the negative displacements from the list
	
	/*ArrayList eventDispNeg=new ArrayList();
	ArrayList eventNamesNeg=new ArrayList();
	ArrayList eventDispPos=new ArrayList();
	ArrayList eventNamesPos=new ArrayList();
	ArrayList eventIdsPos=new ArrayList();
	
	for (int j=0;j<eventDisplacements.size();j++)
	{
	    if (EJBUtil.stringToNum((String)eventDisplacements.get(j))<0)
	    {
	        eventDispNeg.add(eventDisplacements.get(j));
	        eventNamesNeg.add(eventNames.get(j));
	    }
	    else 
	    {
	        eventDispPos.add(eventDisplacements.get(j));
	        eventNamesPos.add(eventNames.get(j));
	        eventIdsPos.add(eventIds.get(j));
	    }
	}*/
	//end

	if(eventIds.size()==0){

		rows=0;}

		else{

		rows=eventIds.size();

		}
		

             output.append("<table class = 'browserDefault' width='100%' cellspacing='0' cellpadding='0' border=0 >");

             output.append("    <tr > ");

             output.append("      <td width = '50%'> ");

             output.append("        <P class = 'sectionHeadings' align='left'> "+LC.L_Pcol_CalTemplate/*Protocol Calendar Template*****/+" </P>");

             output.append("      </td>");

              output.append("<td><P class = 'sectionHeadings' align='left'> "+LC.L_Protocol_Calendar/*Protocol Calendar*****/+": " + protName);

		 output.append("    </P></td></tr>");

             output.append("  </table>");





             output.append("<Form name='report' method=\"POST\">");

             output.append("  <table class = 'browserDefault' width='100%' cellspacing='0' cellpadding='0' border=0 >");

             output.append("    <tr > ");


             output.append("    <td class='welcome' align='right'>");


             output.append("    </td>");

             output.append("    </tr>");

             output.append("  </table>");

			//output.append("</br>");



				if(rows > 0) {



  weekNum = 0;

output.append("<table  width='100%' cellspacing='0' cellpadding='3' border=0>");

output.append("  <tr> ");

output.append("    <th class = 'reportHeadings' width='05%' > "+LC.L_Week/*Week*****/+" </th>");

output.append("    <th class = 'reportHeadings'  width='05%' > "+LC.L_Day/*Day*****/+" </th>");

output.append("    <th class = 'reportHeadings'  width='20%' > "+LC.L_Event/*Event*****/+"</th>");

output.append("    <th class = 'reportHeadings'  width='20%' ></th>");

output.append("  </tr>");



//output.append("<tr bgColor=#fafad2 class = 'reportGreyRow'> <td > week1 </td><td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td>  </tr> ");	





for(i = 0 ; i < rows ; i++)

  {	

	eventId= ((eventIds.get(i)) == null)?"-":eventIds.get(i).toString();

	eventName = ((eventNames.get(i)) == null)?"-":eventNames.get(i).toString();

	eventDisplacement = ((eventDisplacements.get(i)) == null)?"-":eventDisplacements.get(i).toString();



	if (i > 0)

	{

		prevEventDisplacement  = ((eventDisplacements.get(i - 1)) == null)?"-":eventDisplacements.get(i - 1).toString();

	} 

	

	Integer temp=new Integer(eventDisplacement);

	int printval=temp.intValue();

	actDay = temp.intValue() ;

	int newrowTest=0;	

	

	if(printval%7==1)

	{

	newrowTest=1;

	//printval=7;

	}

	else

	{

	//printval=printval%7;

	newrowTest=0;

	}



	if(printval%7==0)

	{

		printval=7;

	}

	else

	{

		printval=printval%7;

	}

	

//	(EJBUtil.stringToNum(eventDisplacement)%7)





	/*if((newrowTest==1) && (!prevEventDisplacement.equals(eventDisplacement)) )

				{

			//output.append("<tr class = 'reportGreyRow' ><td> week " +  (rowCount/7+1) + " </td><td></td><td></td><td></td></tr>");

			output.append("<tr bgcolor = 'red'><td> week " +  (rowCount/7+1) + " </td><td></td><td></td><td></td></tr>");

			weekNum ++;

		}*/

		

	if(newrowTest==1)

	{

		if (!prevEventDisplacement.equals(eventDisplacement)) 

				{

			 output.append("<tr class = 'reportGreyRow' ><td> "+LC.L_Week/*week*****/+" " +  (rowCount/7+1) + " </td><td></td><td></td><td></td></tr>");

  			  weekNum ++;

 			//output.append("<tr bgcolor = 'red'><td> week " +  (rowCount/7+1) + " </td><td></td><td></td><td></td><td></td><td></td><td></td></tr>");	

		}	

   }

   

//	if((printval%7==1) && (rows!=duration.intValue()) && (!prevEventDisplacement.equals(eventDisplacement)) )

	

	Integer displacement=new Integer(eventDisplacement);



	rowCount=displacement.intValue();

	

	//weekNum = rowCount/7+1;

	dispStr = LC.L_Week/*"Week "*****/ + weekNum + ", "+LC.L_Day/*"Day "*****/ +  printval;

	

      if((i%2) == 0)

      {

      output.append("<tr class='browserEvenRow'>");

      }

      else

      {

      output.append("<tr  class='browserOddRow'>");

      }



	        

  	

      	output.append(" <td width='15%' align='center'> " + "" + " </td>");



		output.append(" <td width='15%' align='center' valign=top>" +printval+ "</td>");



		output.append(" <td width='50%' align='center' valign=top> " + eventName + " </td>");

		output.append(" <td width='20%' align='center' valign=top><A href='#' onClick=\"fselect(" +actDay + ",\'"+ dispStr +"\')\">"+LC.L_Select/*Select*****/+"</A></td>");



		output.append("</tr>");



		



		if(i<(rows-1))

		{

			Integer first=new Integer(eventDisplacement);

			Integer second=new Integer(eventDisplacements.get(i+1).toString());

			int diff=second.intValue()-first.intValue();	

			 if(diff>1)

			  {

				

				for(int k=1;k<diff;k++)

				{

				

   				if(rowCount%7==0)

			   	{

			   	insideNewRowTest=1;

				}

		   		else

			   	{

			   	insideNewRowTest=0;

				}

				

					if(insideNewRowTest==1)

					{

  					  weekNum ++;

 			//output.append("<tr bgcolor = 'red'><td> week " + rowCount + "*" + (rowCount/7+1) + " </td><td></td><td></td><td></td><td></td><td></td><td></td></tr>");

					output.append("<tr class = 'reportGreyRow' ><td> "+LC.L_Week_Lower/*week*****/+" " +  (rowCount/7+1) + " </td><td></td><td></td><td></td></tr>");	

	

			   }



   

   				 		

					if((k%2) == 0)

	                {

    	            output.append("<tr class='browserEvenRow'>");

        	        }

            	    else

                	{

	                output.append("<tr  class='browserOddRow'>");

    	            }

					output.append(" <td width='15%' align='center'> " + "" + " </td>");

					output.append(" <td width='05%' align='center'> " + (rowCount%7 + 1) +" 			</td>");			

					output.append(" <td width='50%' align='center'> " + "-" + " </td>");

					output.append(" <td width='05%' align='center'> " + "-"+ " 			</td>");

					output.append("</tr>");

					rowCount++;

		

			

					

					/*if((rowCount%7)==0)

					{

					output.append("<tr class = 'reportGreyRow'><td > week "+ (rowCount/7+1) + "</td><td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> </tr> ");	

					}

					*/

							

				}//end of for

			  } //end of if 

	} //end of if 

 



} //end of for loop



/////////////////add rest of rows.....

if(rowCount%7==0)

			{

output.append("<tr class = 'reportGreyRow'><td> "+LC.L_Week_Lower/*week*****/+" " + (rowCount/7+1) + " </td><td></td><td></td><td></td></tr>");

			}









int test=duration.intValue()-rowCount;

int xCount = 0;

int restPrintVal=0;

  rowCount = rowCount + 1; 

  

	for(int p=1;p<=test  ;p++)

			{

 			//xCount =  rowCount%7 ;

			

			if(rowCount%7==0)

			{

		restPrintVal=7;			

			xCount=1;

			}

			else	

			{

			restPrintVal=rowCount%7;

//			xCount=rowCount%7;



			xCount=0;

			}

		

		

			

				if((p%2) == 0)

                {

                output.append("<tr class='browserEvenRow'>");

                }

                else

                {

                output.append("<tr  class='browserOddRow'>");

                }

	

	//		output.append("<tr>");

			output.append(" <td width='05%' align='center'> " + "" + " </td>");

			//output.append(" <td width='05%' align='center'> " +rowCount +  (rowCount%7 + 1) + " 			</td>");

			output.append(" <td width='05%' align='center'> " +  restPrintVal+"</td>");			

			output.append(" <td width='20%' align='center'> " + "-" + " </td>");

			output.append(" <td width='05%' align='center'> " + "-"+ " 			</td>");

			output.append("</tr>");

			rowCount++;

			

			

//		if((xCount==1) && (p!=duration.intValue()))

		if(xCount==1)

		{

			output.append("<tr class = 'reportGreyRow' ><td> "+LC.L_Week_Lower/*week*****/+" " + (rowCount/7+1) + " </td><td></td><td></td><td></td></tr>");

		//	output.append("<tr bgcolor = 'blue' ><td> week " + (rowCount/7+1) + " </td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>");	

		}

			/*

			if(((rowCount%7)==0)&&(duration.intValue()!=rowCount))

			{

			output.append("<tr class = 'reportGreyRow' ><td > week "+  ((rowCount/7)+1) + "</td><td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> <td> </td> </tr> ");	

			}*/

			

		



			}//end of for

 

 

/////////////////////////////end of insertions.



output.append("</table>");

//output.append("</div>");

			}

			}

%>

 

<%=output.toString()%> 

</body>



</html>

