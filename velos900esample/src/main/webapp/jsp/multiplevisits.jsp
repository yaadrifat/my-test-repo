<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<%
String calStatus = request.getParameter("calStatus");
SchCodeDao cd = new SchCodeDao();
String calStatusDesc = cd.getCodeDescription(cd.getCodeId("calStatStd","O"));
Object[] arguments = {calStatusDesc};
String jsMessage = VelosResourceBundle.getMessageString("M_OfflineVisitDayZero",arguments);
%>

<head> 

<title><%=LC.L_Add_MultiVisits%><%--Add Multiple Visits*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.StringUtil" %>
<%@ page import = "com.velos.eres.service.util.MC,com.velos.eres.service.util.LC"%>
<%@ page import = "com.velos.eres.service.util.VelosResourceBundle"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
 
<script>
if (document.getElementById('submit_btn')) { document.getElementById('submit_btn').disabled=false; }
if (document.getElementById('add_another')) { document.getElementById('add_another').disabled=false; }

function  blockSubmit() {
	setTimeout('document.getElementById(\'submit_btn\').disabled=true', 10);
	setTimeout('document.getElementById(\'add_another\').disabled=true', 10);
}

function addMultipleVisit(formobj)
{
   formobj.addflag.value = "Add";
   formobj.action = "multiplevisitssave.jsp?from=submitadd";
   if( validate (formobj) == false) {
		formobj.addflag.value = "";
		return false;
   }
}


function setVals(formobj){
	if(formobj.addflag.value != 'Add') {
	formobj.action = "multiplevisitssave.jsp?from=submit";
	return validate(formobj);
	}	
}

function  validate(formobj){

 	var ret = 0;

	if(allfldquotecheck(formobj)==false)
		  return false;

	if (!(validate_col('Visit Name',formobj.visitName))) {
		return false;
	}

	//KM-#4476
	insertAfterVal = formobj.insertAfter.options[formobj.insertAfter.selectedIndex].value ;
	if(insertAfterVal != "") {
		optVal = insertAfterVal.substring(insertAfterVal.indexOf("/")+1,insertAfterVal.length);
		if (optVal == '') {
			alert("<%=MC.M_IntervalDefn_NotApplicab%>");/*alert("This option of Interval definition is not applicable when the Parent Visit has 'No Interval Defined'");*****/
			return false;
		}
	}
	
	if (formobj.noInterval.checked == false)
		noInterval = false;
	else 
		noInterval = true;

	if ( isWhitespace(formobj.months.value))
		m = false;
	else
		m = true;

	if (isWhitespace(formobj.weeks.value))
		w = false;
	else
		w=true;

	if (isWhitespace(formobj.days.value))
		d = false;
	else
		d = true;

	if (isWhitespace(formobj.insertAfterInterval.value))
		i = false;
	else
		i = true;

	if ( !m && !w && !d && !i && !noInterval) {
		alert("<%=MC.M_EtrInterval_MthWeeksDays%>");/*alert("Enter interval in months/weeks/days or Insert After format or Select No Interval option");*****/
		formobj.months.focus();
		return false;
	}

	//KM -- Validation for No interval visit
	else if ((m || w || d) && i && noInterval){
		alert("<%=MC.M_EtrInterval_MthWeeksDays%>");/*alert("Enter interval in months/weeks/days or Insert After format or Select No Interval option");*****/
		return false;
	}


	else if ( (m || w || d) && i) {
		alert("<%=MC.M_Interval_InMthWksDay%>");/*alert("Either Enter interval in months/weeks/days or Insert After format");*****/
		formobj.months.focus();
		return false;
	}

	//KM -- Validation for No interval visit
	else if ( (m || w || d) && noInterval) {
		alert("<%=MC.M_EtrInterval_OrSelNoOpt%>");/*alert("Either Enter interval in months/weeks/days or Select No Interval option");*****/
		formobj.months.focus();
		return false;
	}
	else if (i && noInterval) {
		alert("<%=MC.M_EtrInterval_OrSelNone%>");/*alert("Either Enter interval in Insert After format or Select No Interval option");*****/
		formobj.insertAfterInterval.focus();
		return false;
	}

	else if ( m || w || d) {
	     ret = validate_interval(formobj,formobj.months.value, formobj.weeks.value, formobj.days.value);
	     if (ret < 0)
	     	return false;
	}
	//KV: Fixed Bug No. 4860 & 4862 .Checked the integer Validations 
	else if (noInterval) {
			if (!(isInteger(formobj.beforenum.value))){
				alert("<%=LC.L_Invalid_VisitWindow%>");/*alert("Invalid Visit Window");*****/
		 		formobj.beforenum.focus();
		 	 	return false;
		 	}
			if (!(isInteger(formobj.afternum.value))){
				alert("<%=LC.L_Invalid_VisitWindow%>");/*alert("Invalid Visit Window");*****/
		 		formobj.afternum.focus();
		 	 	return false;
		 	}
		return true;
	}
	else { // insert after is entered:
		if (!isInteger(formobj.insertAfterInterval.value)) {
			alert("<%=MC.M_EtrInter_NotValid%>");/*alert("Interval entered for Insert After is not valid");*****/
			formobj.insertAfterInterval.focus();
			return false;
		}
		else {
			p = parseInt(formobj.insertAfterInterval.value);
			if (isNaN(p) || (p == 0) ) {
				alert("<%=MC.M_EtrInter_NotValid%>");/*alert("Interval entered for Insert After is not valid");*****/
				formobj.insertAfterInterval.focus();
				return false;
			}

			if (isWhitespace(formobj.intervalUnit.value)) {
				formobj.insertAfterInterval.focus();
				alert("<%=MC.M_SelDayWeek_AftIntr%>");/*alert("Select Days/Weeks/Months for insert After interval");*****/
				return false;
			}
			else if (isWhitespace(formobj.insertAfter.value)) {
				alert("<%=MC.M_SelVisit_AftIntr%>");/*alert("Select a Visit Name for insert After interval");*****/
				formobj.insertAfterInterval.focus();
				return false;

			}
			//KV: Fixed Bug No. 4860 & 4862 .Checked the integer Validations
			if (!(isInteger(formobj.beforenum.value))){
				alert("<%=LC.L_Invalid_VisitWindow%>");/*alert("Invalid Visit Window");*****/
			 	formobj.beforenum.focus();
			 	 return false;
			 }
			
			 if (!(isInteger(formobj.afternum.value))){
				 alert("<%=LC.L_Invalid_VisitWindow%>");/*alert("Invalid Visit Window");*****/
			 	formobj.afternum.focus();
			 	 return false;
			 }
			else return true;
		}
	} //else
	
	//KM-D-FIN4
	if (!(isInteger(formobj.beforenum.value))){
		alert("<%=LC.L_Invalid_VisitWindow%>");/*alert("Invalid Visit Window");*****/
	 	formobj.beforenum.focus();
	 	 return false;
	 }
	
	 if (!(isInteger(formobj.afternum.value))){
		 alert("<%=LC.L_Invalid_VisitWindow%>");/*alert("Invalid Visit Window");*****/
	 	formobj.afternum.focus();
	 	 return false;
	 }

	return true ;
   }

function validate_interval(formobj,month, week, day) //KM-3407
{
	var monthvalid = true;
	var weekvalid = true;
	var dayvalid = true;
	var ret = 0;
	var m = 0;
	var w = 0;
	var d = 0;

	if (isInteger(month))
		m = parseInt(month);
	else
		monthvalid = false;


	if (isInteger(week))
		w = parseInt(week);
	else
		weekvalid = false;

	if (isInteger(day))
		d = parseInt(day);
  	else if (isNegNum(day)) {
		d = parseInt(day.substring(1,day.length));
	}
	else
		dayvalid = false;

	//Modified by Manimaran to fix the Bug2354

	if  (m <= 0) monthvalid = false;
	if  (w <= 0) weekvalid = false;
	//if  (d <= 0) dayvalid = false; 
//DFIN25 DAY0 BK
	if ( (m == 0) && (w == 0) && (d == 0)) {
		alert("<%=MC.M_MonthDay_NotValid%>");/*alert("Month/Day/week entered is not valid");*****/
		formobj.days.focus();
		return -1;
	}
//MAR-29-11,BK ,FIXED #5901	
	if( d==0 && (w > 1 || m >1)){
	    alert ("<%=MC.M_DayValid_FirstWeek%>");/*alert ("Day 0 interval is only valid for first week of first month");*****/
	    formobj.days.focus();
			return -1;
	    }
	if( d==0 && formobj.calStatus.value == 'O' ){
		alert("<%=jsMessage%>");
		 formobj.days.focus();
			return -1;
	}
	if (isNaN(m) == true) m = 0;
	if (isNaN(w) == true) w = 0;
	if (isNaN(day) == true) {
	 if (!(isNegNum(day) ==true)) d=0;
	}

	if (!monthvalid) {
		alert("<%=MC.M_MonthEtr_NotValid%>");/*alert("Month entered is not valid");*****/
		formobj.months.focus();
		return -1;
	}


	if (!weekvalid) {
		alert("<%=MC.M_WeekEtr_NotValid%>");/*alert("Week entered is not valid");*****/
		formobj.weeks.focus();
		return -1;
	}

	if (!dayvalid) {
		alert("<%=MC.M_DayEtr_IsInvalid%>");/*alert("Day entered is not valid");*****///KM
		formobj.days.focus();
		return -1;
	}



	if ( (m > 0) && (w == 0) && (d > 30)) {
		alert("<%=MC.M_DaysCntMore30_WhenMth%>");/*alert("Days cannot be more than 30 when month is entered");*****/
		formobj.days.focus();
		return -6;
	}

	if (m > 0) {
		if (w > 4) {
			alert("<%=MC.M_WeeksCntMoreThan4_WhenMth%>");/*alert("Weeks cannot be more than 4 when month is entered");*****/
			formobj.weeks.focus();
			return -7;
		}

	}
	if (w > 0) 	{
		if ( d > 7) {
			alert("<%=MC.M_DaysCntMore7_WhenWeek%>");/*alert("Days cannot be more than 7 when week is entered");*****/
			formobj.days.focus();
			return -8;
		}
	}

	return 0;


}



</script>



<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%>
<%@page import="com.velos.esch.business.common.SchCodeDao"%>
<jsp:useBean id="protVisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>

<jsp:useBean id="visitdao" scope="request" class="com.velos.esch.business.common.ProtVisitDao"/>


<jsp:useBean id="eventlistdao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:include page="include.jsp" flush="true"/>

<body id="forms">

<DIV class="popDefault" id="div1">

<%
    String duration ="";
	duration=request.getParameter("duration");
	String mode =request.getParameter("mode");
	//JM: 18Apr2008: modified
	//String src = request.getParameter("src");
	String src = request.getParameter("srcmenu");

	String protocolId = request.getParameter("protocolId");

	String fromPage = request.getParameter("fromPage");
	String calledFrom = request.getParameter("calledFrom");
	String calAssoc = request.getParameter("calassoc");



	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

	{

		String uName = (String) tSession.getValue("userName");

		String intervalUnit = "";
		int insertAfter = 0;
		ArrayList visitNames= new ArrayList();
		ArrayList visitIds= new ArrayList();
		ArrayList displacements = new ArrayList();
		ArrayList numOfDaysList = new ArrayList();

%>
   <P class="sectionHeadings"> <%=MC.M_Cal_AddMultiVisit%><%--Calendar >> Add Multiple Visits*****--%> </P>
   <form name="multiplevisits" id="multVisitFrm" METHOD=POST action=multiplevisitssave.jsp onsubmit="if (setVals(document.multiplevisits)== false) { setValidateFlag('false'); return false; } else { if(document.multiplevisits.addflag.value=='Add') setValidateFlag('false'); else setValidateFlag('true'); blockSubmit(); return true;}">

	<input type="hidden" name = "addflag" value ="">
	<input type=hidden name=duration value=<%=duration%>>
    <input type=hidden name=mode value=<%=mode%>>
	<input type=hidden name=protocolId value=<%=protocolId%>>
	<input type=hidden name=calledFrom value=<%=calledFrom%>>
	<input type=hidden name=calStatus value=<%=calStatus%>>
	<input type=hidden name=fromPage value=<%=fromPage%>>

	<input type=hidden name=calassoc value=<%=calAssoc%>><!--JM: 18APR2008-->
	<input type=hidden name=srcmenu value=<%=src%>><!--JM: 18APR2008-->
	<input type="hidden" id="calStatusDesc" name="calStatusDesc" value="<%=calStatusDesc%>"/>


	<TABLE width="98%" cellspacing="0" cellpadding="0" border="0">

    <tr>
	<td width="12%" align="center"> <%=LC.L_Visit_Name%><%--Visit Name*****--%> <FONT class="Mandatory">* </FONT> <BR><BR>
		<INPUT NAME="visitName" value="" TYPE=TEXT SIZE=15 MAXLENGTH=50> </td>
	<td width="12%" align="center"> <%=LC.L_Description%><%--Description*****--%> <BR><BR>
		<INPUT NAME="description" value="" TYPE=TEXT SIZE=15 MAXLENGTH=200> </td>
    <td width="20%" align="center"> <%=LC.L_Interval%><%--Interval*****--%><FONT class="Mandatory">* </FONT><BR>
    	<%=LC.L_Month%><%--Month*****--%>&nbsp;&nbsp;&nbsp;<%=LC.L_Week%><%--Week*****--%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Day%><%--Day*****--%>&nbsp;&nbsp;&nbsp;&nbsp;<BR>
    	<INPUT NAME="months" value="" TYPE=TEXT SIZE=3 MAXLENGTH=5>
    	<INPUT NAME="weeks" value="" TYPE=TEXT SIZE=3 MAXLENGTH=5>
    	<INPUT NAME="days" value="" TYPE=TEXT SIZE=3 MAXLENGTH=5>  </td>
    <td width="10%" align="center" nowrap><BR><BR>
    	&nbsp;&nbsp;<%=LC.L_Or%><%--Or*****--%>&nbsp; <INPUT NAME="insertAfterInterval" value="" TYPE=TEXT SIZE=2 MAXLENGTH=5>
    	</td>
	<td width="18%"><BR><BR>
	<select name="intervalUnit">
			<% if (intervalUnit.equals("D")) { %>
				<option value="D" Selected ><%=LC.L_Days%><%--Days*****--%></option>
				<option value="W"><%=LC.L_Weeks%><%--Weeks*****--%></option>
				<option value="M"><%=LC.L_Months%><%--Months*****--%></option>
			<%} else if  (intervalUnit.equals("W")) { %>
				<option value="D" ><%=LC.L_Days%><%--Days*****--%></option>
				<option value="W" Selected><%=LC.L_Weeks%><%--Weeks*****--%></option>
				<option value="M"><%=LC.L_Months%><%--Months*****--%></option>
			<%} else if  (intervalUnit.equals("M")) { %>
				<option value="D"><%=LC.L_Days%><%--Days*****--%></option>
				<option value="W"><%=LC.L_Weeks%><%--Weeks*****--%></option>
				<option value="M" Selected><%=LC.L_Months%><%--Months*****--%></option>
			<%} else { %>
				<OPTION value='' SELECTED> <%=LC.L_Select_AnOption%><%--Select an Option*****--%> </OPTION>
				<option value="D"><%=LC.L_Days%><%--Days*****--%></option>
				<option value="W"><%=LC.L_Weeks%><%--Weeks*****--%></option>
				<option value="M" ><%=LC.L_Months%><%--Months*****--%></option>
			<%}%>

		</select></td>
		<%
			String durUnitA="";
			String durUnitB="";			
			int visitid = 0;
			String visitId ="";
			visitId = request.getParameter("visitId");
			int currVisit = EJBUtil.stringToNum(visitId);

			visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId));
		    visitIds = visitdao.getVisit_ids();
			displacements = visitdao.getDisplacements();
			numOfDaysList = visitdao.getDays();


			visitNames = visitdao.getNames();


			String insertAfterPullDown = new String("<Select name='insertAfter'> ") ;
			if ( (currVisit == 0) || (insertAfter == 0) )
				insertAfterPullDown += "<OPTION value='' SELECTED> "+LC.L_Select_AnOption/*Select an Option*****/+" </OPTION>";

			for (int counter = 0; counter < visitIds.size() ; counter++){
				visitid = EJBUtil.stringToNum(visitIds.get(counter).toString());
				//if (visitid == currVisit)
					//continue;
				//DFIN-25-DAY0				
				String tempDisp;
                if("0".equals(""+numOfDaysList.get(counter))){
                		tempDisp = "0";
                	}
                else if("0".equals(""+displacements.get(counter))){
            		tempDisp = "";
            	}
                else{
                	tempDisp = ""+displacements.get(counter);
                }

				//insertAfterPullDown += "<OPTION value = "+ visitIds.get(counter) + "/" + displacements.get(counter);
				insertAfterPullDown += "<OPTION value = "+ visitIds.get(counter) + "/" + tempDisp;
				if (insertAfter == visitid)
					insertAfterPullDown += " Selected ";
				insertAfterPullDown += " >" + visitNames.get(counter)+ "</OPTION>";
			}
			insertAfterPullDown += ("</SELECT>");
		%>
		<td width="40%"> <BR><BR> <%=LC.L_After%><%--After*****--%>&nbsp<%=insertAfterPullDown%> </td>
		<!-- Rohit Fixed Bug No. 4870 -->
		<!--KV: Fixed Bug No.4863 Restricted user entry to three digit integer-->
		<td class=tdDefault align="center" nowrap> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Visit_Window%><%--Visit Window*****--%> <BR><BR>&nbsp&nbsp<input type="text" name="beforenum" value="" size="5" maxlength="3"> 
		<Select Name="durUnitA">
		<Option value="D"><%=LC.L_Days%><%--Days*****--%></Option>
		<Option value="W"><%=LC.L_Weeks%><%--Weeks*****--%></Option>
		<Option value="M"><%=LC.L_Months%><%--Months*****--%></Option>
		<Option value="Y"><%=LC.L_Years%><%--Years*****--%></Option>
	    </Select>
		<%=LC.L_Before%><%--Before*****--%> </td>
		<td>&nbsp;</td> <td nowrap><BR><BR>
		<!--KV: Fixed Bug No.4863 Restricted user entry to three digit integer-->
		<input type="text" name="afternum" value="" size="5" maxlength="3">  <Select Name="durUnitB">
		<Option value="D"><%=LC.L_Days%><%--Days*****--%></Option>
		<Option value="W"><%=LC.L_Weeks%><%--Weeks*****--%></Option>
		<Option value="M"><%=LC.L_Months%><%--Months*****--%></Option>
		<Option value="Y"><%=LC.L_Years%><%--Years*****--%></Option>
	    </Select> <%=LC.L_After%><%--After*****--%> </td>  
		</tr>
		<tr height="30"> <td>&nbsp;</td> <td>&nbsp;</td><td>&nbsp;</td> <td colspan="2"> &nbsp; <%=LC.L_Or%><%--Or*****--%>  <input type="checkbox" name="noInterval" > <%=MC.M_NoIntervalDefined%><%--No Interval Defined*****--%></td>  <!--KM-D-FIN4-->
		
		</tr>
	</table>

<TABLE width="100%" cellspacing="0" cellpadding="0" >
<tr bgcolor="<%=StringUtil.eSignBgcolor%>" align="center">
<td align="center">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="<%=StringUtil.eSignBgcolor%>" >
	<tr bgcolor="<%=StringUtil.eSignBgcolor%>"><td align="center">
	<button onClick="return addMultipleVisit(document.multiplevisits);"><%=LC.L_Submit_AddAnother%></button>
    </td></tr>
</table>
</td>
<td bgcolor="<%=StringUtil.eSignBgcolor%>" align="center">
<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="N"/>
			<jsp:param name="formID" value="multVisitFrm"/>
			<jsp:param name="showDiscard" value="N"/>
			<jsp:param name="noBR" value="Y"/>
	</jsp:include>	
</td>
</tr>
</table>



<TABLE width="98%" cellspacing="0" cellpadding="0" >

<tr><td><b><%=LC.L_PrevAdd_Vst%><%--Previously Added Visits*****--%>:</b></td></tr>
	<hr>

 
    <!--tr>
	<td width="20%" align="center"> Visit Name <FONT class="Mandatory">* </FONT> </td>
	<td width="20%" align="left"> Description </td>
    <td width="20%" align="left"> Interval&nbsp<FONT class="Mandatory">* </FONT> </td>
	</tr-->
	 </TABLE>
  
   <table width="98%" cellspacing="0" cellpadding="0" >

<tr height="35">
	<td width="12%" align="center"> <%=LC.L_Visit_Name%><%--Visit Name*****--%>  <BR><BR> </td>
	<td width="12%" align="center"> <%=LC.L_Description%><%--Description*****--%> <BR><BR></td>
    <td width="20%" align="center"> <%=LC.L_Interval%><%--Interval*****--%><BR>
    <%=LC.L_Month%><%--Month*****--%>&nbsp;&nbsp;&nbsp;<%=LC.L_Week%><%--Week*****--%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Day%><%--Day*****--%>&nbsp;&nbsp;&nbsp;&nbsp;<BR>
    </td>
    <td width="10%">&nbsp;</td>
	<td width="18%">&nbsp;</td>
	<td width="26%">&nbsp;</td>
	<td nowrap align="center"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Visit_Window%><%--Visit Window*****--%> </td>
</tr>

	<%
		   ArrayList eventVisitIds =null;
		   ArrayList flagsStrings =null;
		   eventlistdao= eventdefB.getAllProtSelectedEvents(EJBUtil.stringToNum(protocolId),"", "");
		   eventVisitIds = eventlistdao.getFk_visit();
		   flagsStrings = eventlistdao.getFlags();
		   String flagStr ="";



		    String monthStr = "";
			String weekStr = "";
			String dayStr = "";
			String intervalStr = "";
			int interval = 0;
			intervalUnit = "";
			String visit_name = "";

			String description = "";
			String displacement = "";
			int months =0, weeks=0;
			Integer days=null;
			int disp=0;
			insertAfter = 0;
			duration="";
			int pageRight=0;
			String visitNo = "";
			String durationBefore ="";
			String durationUnitBefore = "";
			String durationAfter ="";
			String durationUnitAfter ="";
			int noIntervalFlag = 0;




		   for(int i=0; i<eventVisitIds.size();i++) {
			   flagStr = flagsStrings.get(i).toString();
			   if(flagStr.equals("V")) {
					
					monthStr="";//KM
					weekStr="";
					dayStr="";
					visitId = eventVisitIds.get(i).toString();
					protVisitB.setVisit_id(EJBUtil.stringToNum(visitId));
					protVisitB.getProtVisitDetails();
					visit_name = protVisitB.getName();
					visitNo= new Integer(protVisitB.getVisit_no()).toString();
					description = protVisitB.getDescription();
					description = (   description  == null)?"":(  description ) ;
					//disp = protVisitB.getDisplacement();
					months = protVisitB.getMonths();
					weeks = protVisitB.getWeeks();
					days = protVisitB.getDays();
					insertAfter = (protVisitB.getInsertAfter()==null)?0:(protVisitB.getInsertAfter()).intValue();
					noIntervalFlag = Integer.parseInt(protVisitB.getIntervalFlag());
					

					 if (insertAfter > 0) {
						monthStr = "";
						weekStr = "";
						dayStr = "";
						intervalStr = (protVisitB.getInsertAfterInterval()).toString();
						intervalUnit = protVisitB.getInsertAfterIntervalUnit();

					   }
					   else {

					   	if (months > 0)
							monthStr = new Integer(months).toString();

					   	if (weeks > 0)
							weekStr = new Integer(weeks).toString();

					   	if (days != null)
							dayStr = days.toString();

						intervalStr = "";
						intervalUnit = "";

					   }
						
					durationBefore = protVisitB.getDurationBefore();
					durationBefore  = (   durationBefore  == null)?"0":(  durationBefore ) ;
					durationUnitBefore = protVisitB.getDurationUnitBefore();
					durationUnitBefore  = (   durationUnitBefore  == null)?"":(  durationUnitBefore ) ;
					durationAfter = protVisitB.getDurationAfter();
					durationAfter  = (   durationAfter  == null)?"0":(  durationAfter ) ;
					durationUnitAfter = protVisitB.getDurationUnitAfter();
					durationUnitAfter  = (   durationUnitAfter  == null)?"":(  durationUnitAfter ) ;



%>


   <tr width="100%">
   <td width="12%"> <INPUT NAME="visitName1"  TYPE=TEXT SIZE=15 MAXLENGTH=50  value="<%=visit_name%>" readonly>  </td>

   <td width="12%"> <INPUT NAME="description1"  TYPE=TEXT SIZE=15 MAXLENGTH=200 value="<%=description%>" readonly > </td>
   <%
      if(noIntervalFlag == 0)
       {
      %>
	    <td width="20%"><INPUT NAME="months1"  TYPE=TEXT SIZE=3 MAXLENGTH=5  value="<%=monthStr%>" readonly >	
	    <INPUT NAME="weeks1"  TYPE=TEXT SIZE=3 MAXLENGTH=5 value="<%=weekStr%>" readonly > 
	    <INPUT NAME="days1"  TYPE=TEXT SIZE=3 MAXLENGTH=5 value="<%=dayStr%>" readonly > </td>
	<td width="10%" nowrap> &nbsp;&nbsp;<%=LC.L_Or%><%--Or*****--%>&nbsp;<INPUT NAME="insertAfterInterval1" value="<%=intervalStr%>" TYPE=TEXT SIZE=2 MAXLENGTH=5 readonly> </td>
	<td width="18%">
	<select name="intervalUnit1" disabled>
			<% if (intervalUnit.equals("D")) { %>
				<option value="D" Selected ><%=LC.L_Days%><%--Days*****--%></option>
				<option value="W"><%=LC.L_Weeks%><%--Weeks*****--%></option>
				<option value="M"><%=LC.L_Months%><%--Months*****--%></option>
			<%} else if  (intervalUnit.equals("W")) { %>
				<option value="D" ><%=LC.L_Days%><%--Days*****--%></option>
				<option value="W" Selected><%=LC.L_Weeks%><%--Weeks*****--%></option>
				<option value="M"><%=LC.L_Months%><%--Months*****--%></option>
			<%} else if  (intervalUnit.equals("M")) { %>
				<option value="D"><%=LC.L_Days%><%--Days*****--%></option>
				<option value="W"><%=LC.L_Weeks%><%--Weeks*****--%></option>
				<option value="M" Selected><%=LC.L_Months%><%--Months*****--%></option>
			<%} else { %>
				<OPTION value='' SELECTED> <%=LC.L_Select_AnOption%><%--Select an Option*****--%> </OPTION>
				<option value="D"><%=LC.L_Days%><%--Days*****--%></option>
				<option value="W"><%=LC.L_Weeks%><%--Weeks*****--%></option>
				<option value="M" ><%=LC.L_Months%><%--Months*****--%></option>
			<%}%>

		</select></td>
		<%
			visitid = 0;
			visitId ="";
			visitId = request.getParameter("visitId");
			currVisit = EJBUtil.stringToNum(visitId);

			visitdao = protVisitB.getProtocolVisits(EJBUtil.stringToNum(protocolId));
		    visitIds = visitdao.getVisit_ids();
			displacements = visitdao.getDisplacements();
			visitNames = visitdao.getNames();

			insertAfterPullDown = new String("<Select name='insertAfter1' disabled> ") ;
			if ( (currVisit == 0) || (insertAfter == 0) )
				insertAfterPullDown += "<OPTION value='' SELECTED > "+LC.L_Select_AnOption/*Select an Option*****/+" </OPTION>";

			for (int counter = 0; counter < visitIds.size() ; counter++){
				visitid = EJBUtil.stringToNum(visitIds.get(counter).toString());
				if (visitid == currVisit)
					continue;

				insertAfterPullDown += "<OPTION value = "+ visitIds.get(counter) + "/" + displacements.get(counter);
				if (insertAfter == visitid)
					insertAfterPullDown += " Selected ";
				insertAfterPullDown += " >" + visitNames.get(counter)+ "</OPTION>";
			}
			insertAfterPullDown += ("</SELECT>");
		%>
		<td width="26%" nowrap> <%=LC.L_After%><%--After*****--%>&nbsp<%=insertAfterPullDown%> </td>
		<% }
		else{
			%>
			<td colspan = "4" align = "center"><%=MC.M_NoIntervalDefined%><%--No Interval Defined*****--%></td><%
		}%>   
		<td nowrap> &nbsp;&nbsp; <input type="text" value="<%=durationBefore%>" size="5" readonly> 
		<Select disabled>
		<Option value="D" <%if (durationUnitBefore.equals("D")){%> Selected <%}%>><%=LC.L_Days%><%--Days*****--%></Option>
		<Option value="W" <%if (durationUnitBefore.equals("W")){%> Selected <%}%>><%=LC.L_Weeks%><%--Weeks*****--%></Option>
		<Option value="M" <%if (durationUnitBefore.equals("M")){%> Selected <%}%>><%=LC.L_Months%><%--Months*****--%></Option>
		<Option value="Y" <%if (durationUnitBefore.equals("Y")){%> Selected <%}%>><%=LC.L_Years%><%--Years*****--%></Option>
	    </Select> <%=LC.L_Before%><%--Before*****--%>
		</td>
		<!-- Rohit Fixed Bug No. 4870 -->
		<td nowrap> &nbsp;&nbsp; <input type="text" value="<%=durationAfter%>" size="5" readonly> 
		<Select disabled>
		<Option value="D" <%if (durationUnitAfter.equals("D")){%> Selected <%}%>><%=LC.L_Days%><%--Days*****--%></Option>
		<Option value="W" <%if (durationUnitAfter.equals("W")){%> Selected <%}%>><%=LC.L_Weeks%><%--Weeks*****--%></Option>
		<Option value="M" <%if (durationUnitAfter.equals("M")){%> Selected <%}%>><%=LC.L_Months%><%--Months*****--%></Option>
		<Option value="Y" <%if (durationUnitAfter.equals("Y")){%> Selected <%}%>><%=LC.L_Years%><%--Years*****--%></Option>
	    </Select> <%=LC.L_After%><%--After*****--%>
		</td>
<%
 }
}

%>

</table>
</div>
</form>


<%

} else {  //else of if body for session
%>
  <jsp:include page="timeout.html" flush="true"/>
<%}%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</body>
</html>
