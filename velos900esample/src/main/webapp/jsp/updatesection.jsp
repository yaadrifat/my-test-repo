<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.MC" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
<jsp:useBean id="sectionB" scope="session" class="com.velos.eres.web.section.SectionJB"/>
<jsp:useBean id="codelstB" scope="session" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>
<%

   int ret=2;
   String msg=null;
   String mode=null;
   String sectionId=null;
   String sectionName = null;
   String sectionNumber = null;
   String sectionContent = null;
   String sectionPubFlag = null;
   String studyId=null;
   int id;

   String tab = request.getParameter("selectedTab");	
   String src = request.getParameter("srcmenu");
   String studyVerId = request.getParameter("studyVerId");  
   mode = request.getParameter("mode");
   String eSign = request.getParameter("eSign");

   
   if(mode.equals("N") && request.getParameter("dd_text").equals("dd")) {
	sectionName = request.getParameter("sectionNameDD");
   } else {
       sectionName = request.getParameter("sectionName");
   }
  	
   sectionNumber = request.getParameter("sectionNumber");
   String template = request.getParameter("template");
   sectionId = request.getParameter("sectionId");

   //Bug #6576 AGodara
   String userAgent = request.getHeader("User-Agent");
   if( userAgent != null && userAgent.contains("MSIE")){
	   sectionContent = request.getParameter("sectionContents");
   }else
	   sectionContent = request.getParameter("sectionContentsta");

   sectionPubFlag = request.getParameter("sectionPubFlag");
   String sectionSequence= request.getParameter("sectionSequence");
   if(sectionSequence.equals("")){ sectionSequence = "0";}

	HttpSession tSession = request.getSession(true);

  if (sessionmaint.isValidSession(tSession))
   {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = null;
	usr = (String) tSession.getValue("userId");
    studyId = (String) tSession.getValue("studyId");
    String accId = (String) tSession.getValue("accountId");



 if (mode.equals("M"))

 {

   id=EJBUtil.stringToNum(sectionId);
   sectionB.setId(id);
   sectionB.getSectionDetails();
 }

  sectionB.setSecStudy(studyId);
  sectionB.setSecStudyVer(studyVerId); 
  sectionB.setContents(sectionContent);
  sectionB.setSecName(sectionName);
  sectionB.setSecNum(sectionNumber);
  sectionB.setSecPubFlag(sectionPubFlag);
  //sectionB.setSecSequence("0");   
  sectionB.setSecSequence(sectionSequence);

   if (mode.equals("M")) {
	   sectionB.setModifiedBy(usr);
       sectionB.setIpAdd(ipAdd);		
	   sectionB.updateSection();
   }

   else if (mode.equals("N"))  {     
		sectionB.setCreator(usr);
		sectionB.setIpAdd(ipAdd);	
		if(template != null)
		{
			codelstB.setClstAccId(accId);
			codelstB.setClstType("section");
			codelstB.setClstDesc(sectionName);
			codelstB.setCreator(usr);
			codelstB.setIpAdd(ipAdd);
			codelstB.setCodelstDetails();
			ret=codelstB.getClstId();
			if(ret!=-1){
			sectionB.setSectionDetails();
			}
		}else{
		sectionB.setSectionDetails();
		}
   }

   if (ret == 0) {
	   msg = MC.M_SecDet_SvdSucc;/*msg = "section  details saved successfully";*****/
   }
   else {
	   msg = MC.M_SecDets_NotSvd;/*msg = "section details not saved";*****/
   }  

%>
<br>
<br>
<br>
<br>
<br>
<% if (ret!=-1) {%>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>

<META HTTP-EQUIV=Refresh CONTENT="1; URL=sectionBrowserNew.jsp?srcmenu=<%=src%>&mode=M&selectedTab=<%=tab%>&studyId=<%= studyId%>&studyVerId=<%=studyVerId%>">

<% }else { %>

<p class = "successfulmsg" align = center> <%=MC.M_SecAldyExst_EntDiffName %><%-- This Section already exists in the template list.
Please Enter a different Name.*****--%>
<table>
<tr>
<td align=center>

		<button onclick="window.history.back();"><%=LC.L_Back%></button>
</td>		
</tr>		
</table>		



 </p>
<%}%>

<%


} //end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</BODY>
</HTML>