<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Select_Pcol%><%--Select Protocol*****--%></title>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<SCRIPT Language="javascript">

 function back(rpType,formobj) {
	i=formobj.protocolId.options.selectedIndex;
	if (i>-1) {	
	  if (document.all) {
		if (rpType=='all') {
			window.opener.document.reports.protId.value = formobj.protocolId.options[i].value;	
			window.opener.document.reports.selProtocol.value = formobj.protocolId.options[i].text;
		} else if (rpType=='single') {
			window.opener.document.reports.protId2.value = formobj.protocolId.options[i].value;	
			window.opener.document.reports.selProtocol2.value = formobj.protocolId.options[i].text;		
		}	
	 } else {
		if (rpType=='all') {
			if (((navigator.appVersion).substring(0,1)) == '5') { 
				window.opener.document.reports.protId.value = formobj.protocolId.options[i].value;	
				window.opener.document.reports.selProtocol.value = formobj.protocolId.options[i].text;
			} else {
				window.opener.document.div1.document.reports.protId.value = formobj.protocolId.options[i].value;	
				window.opener.document.div1.document.reports.selProtocol.value = formobj.protocolId.options[i].text;
			}			
		} else if (rpType=='single') {
			if (((navigator.appVersion).substring(0,1)) == '5') { 
				window.opener.document.reports.protId2.value = formobj.protocolId.options[i].value;	
				window.opener.document.reports.selProtocol2.value = formobj.protocolId.options[i].text;		
			} else {
				window.opener.document.div1.document.reports.protId2.value = formobj.protocolId.options[i].value;	
				window.opener.document.div1.document.reports.selProtocol2.value = formobj.protocolId.options[i].text;
			}		
		}		 
	 } 
	self.close();	 
  }
}  
</SCRIPT>

</head>

<% response.setHeader("Cache-Control", "max-age=1, must-revalidate");%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<%@ page language = "java" import="com.velos.eres.service.util.*,com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.esch.business.common.EventAssocDao"%>

<jsp:useBean id="studyProt" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<body>

<Form  name="protocol" method="post" action="" >

<%

 String studyPk = request.getParameter("studyPk");
 String reportType = request.getParameter("reportType");

 HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession))
{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	EventAssocDao eventAssoc = studyProt.getStudyProts(EJBUtil.stringToNum(studyPk));
	ArrayList protocolIds = new ArrayList();
	protocolIds = eventAssoc.getEvent_ids();
	ArrayList names = eventAssoc.getNames();
	
	int len = protocolIds.size();

	StringBuffer protList = new StringBuffer();
	
	
	protList.append("<SELECT name=protocolId>");
	String protId="";

	for(int counter=0;counter<len;counter++){
		protId = (protocolIds.get(counter)).toString();
		protList.append("<option value="+(protocolIds.get(counter)).toString() +" 			 >" +(names.get(counter)).toString() +"</option>");
	}

	protList.append("</select>");
%>

 <table width="100%" cellspacing="0" cellpadding="0" border="0" >
    <tr> 
	<td><P class = "defComments">&nbsp;&nbsp;<%=MC.M_SelPcol_AndSel%><%--Select Protocol and click on Select*****--%><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=protList%> </P></td>
	<td align=center>
		<br><br>
	 <A onclick="back('<%=reportType%>',document.protocol)" href="#"> <img src="./images/select.gif" align="bottom" border="0" > </A>
      </td>
    </tr>
  </table>
</Form>

<%
}//end of if body for session
else{
%>
  <jsp:include page="timeout.html" flush="true"/>
<%}%>



</body>
</html>
