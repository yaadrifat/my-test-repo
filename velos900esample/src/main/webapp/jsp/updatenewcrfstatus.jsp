<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>

<jsp:include page="skinChoser.jsp" flush="true"/>



<BODY>
  <jsp:useBean id="crfStatB" scope="request" class="com.velos.esch.web.crfStat.CrfStatJB"/>
  <jsp:useBean id="crfB" scope="request" class="com.velos.esch.web.crf.CrfJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.MC"%>
  <%

	String src = null;
	src = request.getParameter("srcmenu");
	String eSign = request.getParameter("eSign");

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
   	String oldESign = (String) tSession.getValue("eSign");


/*	out.println(patProtId);
	out.println(studyId);	
	out.println(statDesc);
	out.println(statid);	
*/


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");
	String eventId =""; 
	
	
	String mode="";
	
	String crfNumber ="";
	String crfName ="";
	String crfStat ="";
	String crfEnterById ="";
	String crfReviewById ="";
	String crfReviewOn ="";
	String crfSentToId ="";
	String crfSentById ="";
	String crfSentOn ="";
	String crfStatSentFlag = "0";
	
	String crfId = "";
	String crfStatId = "";

	String pkey = request.getParameter("pkey");
	String visit = request.getParameter("visit");
	String visitName=request.getParameter("visitName");
	visitName=(visitName==null)?"":visitName;
	String eventName = request.getParameter("eventName");
	String formType = request.getParameter("formType");
		
	int temp = 0;

	mode = request.getParameter("mode");
	crfNumber = request.getParameter("crfNumber");
	crfName = request.getParameter("crfName");
	crfStat = request.getParameter("crfStat");
	crfEnterById = request.getParameter("crfEnterById");
	crfReviewById = request.getParameter("crfReviewById");
	crfReviewOn = request.getParameter("crfReviewOn");
	crfSentToId = request.getParameter("crfSentToId");
	crfSentById = request.getParameter("crfSentById");
	crfSentOn = request.getParameter("crfSentOn");
	crfStatSentFlag = request.getParameter("crfStatSentFlag");
	eventId = request.getParameter("eventId");	


	String patProtId= request.getParameter("patProtId");
	String studyId = (String) tSession.getValue("studyId");	
	String statDesc=request.getParameter("statDesc");
	String statid=request.getParameter("statid");	
	String studyVer=request.getParameter("studyVer");	
	String patientCode =request.getParameter("patientCode");	




	
	crfEnterById = (crfEnterById == null)?"":crfEnterById;
	crfSentToId = (crfSentToId == null)?"":crfSentToId;
	crfSentById = (crfSentById == null)?"":crfSentById;
	crfSentOn = (crfSentOn == null)?"":crfSentOn;

	if(crfStatSentFlag == null) {
		crfStatSentFlag ="0";
	} else if(crfStatSentFlag.equals("on")){
		crfStatSentFlag = "1";
	}

	if(mode.equals("N")) {
		crfB.setEvents1Id(eventId);
		crfB.setCrfNumber(crfNumber);
		crfB.setCrfName(crfName);
		crfB.setCreator(usr);
		crfB.setIpAdd(ipAdd);
		crfB.setPatProtId(patProtId);
		if(formType.equals("crf")){
			crfB.setCrfFlag("E");
		} else {
			crfB.setCrfFlag("O");
		}
		crfB.setCrfDelFlag("N");
		crfB.setCrfDetails();
		crfId = (new Integer(crfB.getCrfId())).toString();
		
		crfStatB.setCrfStatCrfId(crfId);
		crfStatB.setCrfStatCodelstCrfStatId(crfStat);
		crfStatB.setCrfStatEnterBy(crfEnterById);
		crfStatB.setCrfStatReviewBy(crfReviewById);		
		crfStatB.setCrfStatReviewOn(crfReviewOn);
		crfStatB.setCrfStatSentTo(crfSentToId);
		crfStatB.setCrfStatSentFlag(crfStatSentFlag);
		crfStatB.setCrfStatSentBy(crfSentById);
		crfStatB.setCrfStatSentOn(crfSentOn);
		crfStatB.setCreator(usr);
		crfStatB.setIpAdd(ipAdd);
		
		crfStatB.setCrfStatDetails();
		
		crfStatId = (new Integer(crfStatB.getCrfStatId())).toString();
				
	}
	
	if(mode.equals("NS")) {
		crfId = request.getParameter("crfId");
		crfStatB.setCrfStatCrfId(crfId);
		crfStatB.setCrfStatCodelstCrfStatId(crfStat);
		crfStatB.setCrfStatEnterBy(crfEnterById);
		crfStatB.setCrfStatReviewBy(crfReviewById);		
		crfStatB.setCrfStatReviewOn(crfReviewOn);
		crfStatB.setCrfStatSentTo(crfSentToId);
		crfStatB.setCrfStatSentFlag(crfStatSentFlag);
		crfStatB.setCrfStatSentBy(crfSentById);
		crfStatB.setCrfStatSentOn(crfSentOn);
		crfStatB.setCreator(usr);
		crfStatB.setIpAdd(ipAdd);
		crfStatB.setCrfStatDetails();
		
		crfStatId = (new Integer(crfStatB.getCrfStatId())).toString();
				
	}	
	
	if(mode.equals("M")) {
		crfId = request.getParameter("crfId");
		crfStatId = request.getParameter("crfStatId");
		
		crfB.setCrfId(EJBUtil.stringToNum(crfId));
		crfB.getCrfDetails();
		crfB.setEvents1Id(eventId);
		crfB.setCrfNumber(crfNumber);
		crfB.setCrfName(crfName);
		crfB.setModifiedBy(usr);
		crfB.setIpAdd(ipAdd);
		crfB.updateCrf();
		
		crfStatB.setCrfStatId(EJBUtil.stringToNum(crfStatId));
		crfStatB.getCrfStatDetails();
		crfStatB.setCrfStatCodelstCrfStatId(crfStat);
		crfStatB.setCrfStatEnterBy(crfEnterById);
		crfStatB.setCrfStatReviewBy(crfReviewById);		
		crfStatB.setCrfStatReviewOn(crfReviewOn);
		crfStatB.setCrfStatSentTo(crfSentToId);
		crfStatB.setCrfStatSentFlag(crfStatSentFlag);
		crfStatB.setCrfStatSentBy(crfSentById);
		crfStatB.setCrfStatSentOn(crfSentOn);
		crfStatB.setModifiedBy(usr);
		crfStatB.setIpAdd(ipAdd);
		
		crfStatB.updateCrfStat();

	}	
%>
<%--
src*<%=src%>
crfId*<%=crfId%><br>
crfStatId*<%=crfStatId%><br>
crfNumber*<%=crfNumber%><br>
crfName*<%=crfName%><br>
crfStat*<%=crfStat%><br>
crfEnterById*<%=crfEnterById%><br>
crfReviewById*<%=crfReviewById%><br>
crfReviewOn*<%=crfReviewOn%><br>
crfSentToId*<%=crfSentToId%><br>
crfSentById*<%=crfSentById%><br>
crfSentOn*<%=crfSentOn%><br>
crfStatSentFlag*<%=crfStatSentFlag%><BR>
eventId*<%=eventId%>
--%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>



	<META HTTP-EQUIV=Refresh CONTENT="3; URL=crfstatusbrowser.jsp?srcmenu=<%=src%>&page=patientEnroll&selectedTab=3&eventId=<%=eventId%>&pkey=<%=pkey%>&visit=<%=visit%>&visitName=<%=visitName%>&eventname=<%=eventName%>&studyId=<%=studyId%>&statDesc=<%=statDesc%>&statid=<%=statid%>&patProtId=<%=patProtId%>&studyVer=<%=studyVer%>&patientCode=<%=patientCode%>&formType=<%=formType%>">

<%
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





