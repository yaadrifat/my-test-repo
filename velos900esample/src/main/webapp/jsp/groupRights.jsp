<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%!
int disableNew(int right) {
    switch(right) {
    case 7: return 6;
    case 5: return 4;
    case 3: return 2;
    case 1: return 0;
    }
    return right;
}

int disableEdit(int right) {
    switch(right) {
    case 7: return 5;
    case 6: return 4;
    case 3: return 1;
    case 2: return 0;
    }
    return right;
}

%>
<HTML>  

<HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>

<TITLE><%=MC.M_MngAcc_GrpRgt%><%-- Manage Account >> Group Rights*****--%></TITLE>

<SCRIPT Language="javascript">
	
	//Added by Manimaran for June Enhancement #GL2
	var arr = new Array();
	function checkAll(formobj){
        totcount=formobj.totalrows.value;
		var applicableRightsStr;
		
		if(formobj.All.checked) {
            for (i=0;i<totcount;i++) {

				if(arr[i].substring(0,2) == "H_"){
				   formobj.newr[i].checked = false;
				   formobj.edit[i].checked = false;
				   formobj.view[i].checked = false;
				   formobj.rights[i].value =0;	
				}
				 
				else
			   {
					applicableRightsStr = formobj.applicableRightsString[i].value;
					
					if (isEmpty(applicableRightsStr)|| applicableRightsStr.indexOf('N') > -1)
					{
						formobj.newr[i].checked = true;
					}	
					if (isEmpty(applicableRightsStr) || applicableRightsStr.indexOf('E') > -1)
					{
					
						formobj.edit[i].checked = true;
					}	
					
					if (isEmpty(applicableRightsStr) || applicableRightsStr.indexOf('V') > -1)
					{
						formobj.view[i].checked = true;
					}
					showRight = formobj.showRight[i].value ;
					
					if (showRight=='T')
					{
						formobj.rights[i].value = formobj.applicableRightsInt[i].value;
					}
					else //hidden rights
					{
						formobj.rights[i].value = 0;
					}	
			   }
            }
        }
        else {  
  		   for (i=0;i<totcount;i++) {
			formobj.newr[i].checked = false;
			formobj.edit[i].checked = false;
			formobj.view[i].checked = false;
			formobj.rights[i].value=0;
         }
       }
   }	
	
	
	function openFilter(grpId,type)
	{
	windowName=window.open("suprightfilter.jsp?groupid="+grpId+"&type="+type,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=550,height=400,left=150");//AK:Fixed BUg#5777 15Mar,11
	windowName.focus();
	}

	function changeRights(obj,row, frmname)

	{
       selrow = row ;
	   totrows = frmname.totalrows.value;
   

	  if (totrows > 1)

		rights =frmname.rights[selrow].value;

	  else

		rights =frmname.rights.value;

	  objName = obj.name;

	  //alert(selrow +"Count" +totrows +"Rights" +rights) ;

	  if (obj.checked)

   	  {
       	if (objName == "newr")

	   {

		rights = parseInt(rights) + 1;

		if (!frmname.view[selrow].checked)

		{ 

			frmname.view[selrow].checked = true;

			rights = parseInt(rights) + 4;

		}

		

	}



       	if (objName == "edit") 

	{

		rights = parseInt(rights) + 2;

		if (!frmname.view[selrow].checked)

		{ 

			frmname.view[selrow].checked = true;

			rights = parseInt(rights) + 4;

		}

		

	}



	

       	if (objName == "view") rights = parseInt(rights) + 4;

		if (totrows > 1 )

			frmname.rights[selrow].value = rights;

		else

			frmname.rights.value = rights;

	  }else

	{

       	if (objName == "newr") rights = parseInt(rights) - 1;

       	if (objName == "edit") rights = parseInt(rights) - 2;

       	if (objName == "view") 

	{	

		if (frmname.newr[selrow].checked) 

		{

			alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/

			frmname.view[selrow].checked = true;		

			

		}

	  else if (frmname.edit[selrow].checked) 

		{

		  alert("<%=MC.M_UsrRgt_CntRevoke%>");/*alert("The user has right to New or Edit. You can not revoke right to View");*****/

			frmname.view[selrow].checked = true;		

		}

	  else

		{		
          
		  rights = parseInt(rights) - 4;

		}

	}

		

	



		if (totrows > 1 )

			frmname.rights[selrow].value = rights;

		else

			frmname.rights.value = rights;

    	 }

		//alert("Final Rights::" +rights);

	}
	
function validate(formobj){
	 if (!(validate_col('e-Signature',formobj.eSign))) return false

    if(isNaN(formobj.eSign.value) == true) {
    	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
    	formobj.eSign.focus();
    	return false;
    }
}	

</SCRIPT>



<% String src;
src= request.getParameter("srcmenu");
%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   



<BODY>


<jsp:useBean id="groupB" scope="session" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="modGrpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>

<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="xtra" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<DIV class="tabDefTopN" id="divTab"> 
	<jsp:include page="accounttabs.jsp" flush="true"> 
	<jsp:param name="selectedTab" value="2"/>
	</jsp:include>
</DIV>			
<DIV class="tabDefBotN" id="div1">  
  <%

	HttpSession tSession = request.getSession(true); 

	int pageRight = 0;

       

	if (sessionmaint.isValidSession(tSession))

	{
	String grpName = null;
	String uName = (String) tSession.getValue("userName");

  	 GrpRightsJB gRights = (GrpRightsJB) tSession.getValue("GRights");		

       	 pageRight = Integer.parseInt(gRights.getFtrRightsByValue("MGRPRIGHTS"));

	if (pageRight > 0)

	{



%>
  <P class = "userName"> <%= uName %> </P>
 <%
	    String grpSuperUserFlag = "";
	    String grpSupBudFlag="";
		String grpChecked = "";
		String grpBudChecked="";
		
		int grpId = Integer.parseInt(request.getParameter("groupId"));
		groupB.setGroupId(grpId);
  	    groupB.getGroupDetails();
        grpName  = groupB.getGroupName();
        grpSuperUserFlag = groupB.getGroupSuperUserFlag(); //get supre user flag
	
	if (grpSuperUserFlag == null) grpSuperUserFlag = "0";	
        if (grpSuperUserFlag.equals("1"))grpChecked = "CHECKED";
        
	grpSupBudFlag=groupB.getGroupSupBudFlag();
	
	if (grpSupBudFlag == null) grpSupBudFlag = "0";	
        if (grpSupBudFlag.equals("1"))grpBudChecked = "CHECKED";
        

	
	
        	
%>
  <P class="comments"> <b><%=MC.M_Assign_RightsToGrp%><%-- Assign Rights to Group*****--%> : <%=grpName%></b></P>
  <Form Name="grpRights" id="grpRightsFrm" method="post" action="updategrprights.jsp" onSubmit="if (validate(document.grpRights)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<%
	grpRights.setId(grpId);

 	out.print("<Input type=hidden name=\"fk_grp\" value="+grpId +">");  

	grpRights.getGrpRightsDetails();

	ctrl.getControlValues("app_rights");

	int rows = ctrl.getCRows();

	ArrayList feature =  ctrl.getCValue();
	ArrayList ftrDesc = ctrl.getCDesc();
	ArrayList ftrSeq = ctrl.getCSeq();
	ArrayList ftrCustomcol1 = ctrl.getCCustom1();
	
//out.print(ftrSeq.size());
	int prevSeq =0;
	String str="";
	
	String applicableRightsString = "";
	int applicableRightsInt = 0;
	//get those rights which will be hidden according to the accounts module
	xtra.getControlValues("hid_rights");

	int xrows = xtra.getCRows();

	ArrayList xfeature =  xtra.getCValue();
	ArrayList xftrDesc = xtra.getCDesc();
	ArrayList xftrSeq = xtra.getCSeq();
	
	Vector vtXFeature = new Vector(50);
	vtXFeature.addAll(xfeature);
	
	//get account module


	String modRight = (String) tSession.getValue("modRight");
	int modlen = modRight.length();
	
	acmod.getControlValues("module");
	int acmodrows = acmod.getCRows();

	ArrayList acmodfeature =  acmod.getCValue();
	ArrayList acmodftrDesc = acmod.getCDesc();
	ArrayList acmodftrSeq = acmod.getCSeq();
	ArrayList acmodftrRight = new ArrayList();
	String strR;
	
	ArrayList acmodValue = new ArrayList();
	for (int counter = 0; counter <= (modlen - 1);counter ++)
	{
		strR = String.valueOf(modRight.charAt(counter));
		acmodftrRight.add(strR);
	}
	
	modGrpRights.setGrSeq(acmodftrSeq);
    modGrpRights.setFtrRights(acmodftrRight);		
	modGrpRights.setGrValue(acmodfeature);
	modGrpRights.setGrDesc(acmodftrDesc);

	//int hidIndex = -1;
    
   // end of account module
   //Added by Manimaran for June Enhancement #GL2. 
   for(int i = 0; i<feature.size();i++){
 %>
   <SCRIPT Language="javascript">
      arr[<%=i%>]='<%=feature.get(i).toString()%>'
   </SCRIPT>
  <%
  }
  %>
    <Input type="hidden" name="totalrows" value= <%=rows%> >
    <Input type="hidden" name="src" value= <%=src%> >
    
    <table cellspacing = "5" >
    	<tr>
		<td>
		   	<Input type="checkbox" name="supuser" value = "1" <%=grpChecked%>> <%=MC.M_SuperUsr_RightsAll%><%-- Super User Rights For All Studies*****--%> 
    	</td>
    	<%
	         if (grpSuperUserFlag.equals("1"))
				{
    	%>
    		<td>
		    	<A href="supuserstudyrights.jsp?mode=M&groupId=<%=grpId%>&srcmenu=<%=src%>&fromPage=groupbrowser"> <%=LC.L_Assign_Rights%><%-- Assign Rights*****--%> </A>
			&nbsp;<A href="javascript:void(0);" onClick="openFilter(<%=grpId%>,'STUDY')"> <%=LC.L_Filters%><%-- Filters*****--%></A>
	    	</td>
        	<%
				}
    	%>		
    		
    	</tr>
	<tr>
		<td>
		   	<Input type="checkbox" name="supbud" value = "1" <%=grpBudChecked%>> <%=MC.M_AllBgts_SURgts%><%-- Super User Rights For All Budgets*****--%>
    	</td>
    	<%
	         if (grpSupBudFlag.equals("1"))
				{
    	%>
    		<td>
		    	<A href="supbudgetrights.jsp?mode=M&groupId=<%=grpId%>&srcmenu=<%=src%>&fromPage=groupbrowser"> <%=LC.L_Assign_Rights%><%-- Assign Rights*****--%> </A>
			&nbsp;<A href="javascript:void(0);" onClick="openFilter(<%=grpId%>,'BUD')"> <%=LC.L_Filter%><%-- Filters*****--%></A>
	    	</td>
        	<%
				}
    	%>		
    		
    	</tr>
    	
  	</table>
<div class="tmpHeight"></div>
    <TABLE width="650" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign">
     
	  <TH width="350"></TH>
      <TH width="100"><%=LC.L_New%><%-- New*****--%></TH>
      <TH width="100"><%=LC.L_Edit%><%-- Edit*****--%></TH>
      <TH width="100"><%=LC.L_View%><%-- View*****--%></TH>
	  <!-- Modified by Amar for Bugzilla issue #3023 -->
	  <tr><td align="left"><input type="checkbox" name="All"  onClick="checkAll(document.grpRights)">  <%=LC.L_SelOrDeSel_All%><%-- Select / Deselect All*****--%></td> </tr>
      <% 
	 boolean showRight = false;

	 boolean showNew = true;
	 boolean showEdit = true;
	 boolean showView = true;
	 
     int hidIndex = -1;
	 int clubrights = 0;
	 int idxCount = 0;
	 
	 String xSeq = "-1";
	 String XRight = "-1";
	 
	 for(int count=0;count<rows;count++){ 
	
    	idxCount = -1;
    	clubrights = 0;
    	hidIndex = -1;
    	
    	
    	String ftr = (String) feature.get(count);
    	String desc = (String) ftrDesc.get(count);
    	Integer seq = (Integer) ftrSeq.get(count);
    	String rights = grpRights.getFtrRightsByValue(ftr);
    	
    	applicableRightsString = (String)ftrCustomcol1.get(count);
    		
    	showNew = true;
    	showEdit = true;
    	showView = true;
    	applicableRightsInt = 7;

    	if (!StringUtil.isEmpty(applicableRightsString))
    	{
    		if (applicableRightsString.equalsIgnoreCase("null"))
    		{
    			applicableRightsString="";
    		}
    		if (! applicableRightsString.contains("N"))
    		{
    				applicableRightsInt  = applicableRightsInt -1;
    				showNew = false;
    		}
    		
    		if (! applicableRightsString.contains("E"))
    		{
    				applicableRightsInt  = applicableRightsInt -2;
    				showEdit = false;
    		} 
    		
    	}
    	else
		{
			applicableRightsString="";
	
    	}
    	
    	if ( (ftr.substring(0,2)).equals("H_") )
    	{
    		desc = "<br><b>" + desc + "</b>";
    		showNew = false;
    		showEdit = false;
    		showView = false;
    		rights = "0";
    		applicableRightsInt = 0;
    		
    	} else if ( (ftr.compareTo("ASSIGNUSERS") == 0) || (ftr.compareTo("MGRPRIGHTS") == 0) 
    		|| (ftr.compareTo("BUDTEMPL") == 0))
    	{
    		desc = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + desc ;
    	} else if ( ftr.compareTo("MPATIENTS") == 0 )
    	{
    		desc = desc + "<i> ("+MC.M_Users_PrimaryOrg+") </i>";/*desc = desc + "<i> (user's primary organization) </i>";*****/
    	}
    	else if ( ftr.compareTo("A2A") == 0 )
    	{
    		desc = "<br><b>"+ desc + "</b>";
    		showNew = false;
    		showEdit = false;
    		showView = true;
    		
    		applicableRightsInt = 4;
    		
    	}
    	
	%>
	
	<%
	
    	hidIndex = xfeature.indexOf(ftr); //
    	
    	//System.out.println(	"hidIndex, ftr" + hidIndex + "*" + ftr);
				
	if (hidIndex >= 0)
	{
		
		do
		{
			
			idxCount = vtXFeature.indexOf(ftr, idxCount + 1);
					
			if (idxCount >= 0)
			{
				xSeq = ((Integer) xftrSeq.get(idxCount)).toString(); //the sequence of the xtra module
					

  				XRight =  (String) modGrpRights.getFtrRightsBySeq(xSeq);
  				

				clubrights = clubrights + EJBUtil.stringToNum(XRight) ;
				
				
			}
	
			
		
		}  while (idxCount != -1);

		
		
			
	
		//look for this right in acMod String
		if (clubrights > 0)
		{
			showRight = true;
		}
		else
		{
			showRight = false;
		}
	}
	else
	{
		showRight = true;
	}
	%>
          <Input type="hidden" name=rights value= <%=rights%> >
		
    <%
		if (!showRight)
		{
		%>
		<tr style="display: none;"> <%-- YK:Fixed Bug#8558 --%>
		<td>
		<Input type="hidden" name="newr">
		<Input type="hidden" name="edit">
		<Input type="hidden" name="view">
		<Input type="hidden" name="showRight" value="F">
		
		 <input type="hidden" name="applicableRightsInt" value="<%=applicableRightsInt%>" />
		 <input type="hidden" name="applicableRightsString" value="<%=applicableRightsString%>" />	
		 	 
		</td>
		</tr>
		<%
		}
		else
		{
		  		
		%>
		<TR>
		<TD>
		<%= desc%>
		</TD>
		<%
    		//check for special rights

	       	if ((ftr.compareTo("MGRPRIGHTS") == 0) || (ftr.compareTo("REPORTS") == 0) || (ftr.compareTo("DSM") == 0) || (ftr.compareTo("MILEST") == 0) || (ftr.compareTo("VIEWPATCOM") == 0)
	       		|| (ftr.compareTo("BUDTEMPL") == 0))
			{
			  showNew = false;
			  applicableRightsInt = disableNew(applicableRightsInt);
			}
			
			if ((ftr.compareTo("DSM") == 0) || (ftr.compareTo("MILEST") == 0) || (ftr.compareTo("VIEWPATCOM") == 0)
				|| (ftr.compareTo("BUDGT") == 0))
			{
			  showEdit = false;
			  applicableRightsInt = disableEdit(applicableRightsInt);
			}
			
			/*if (ftr.compareTo("BUDTEMPL") == 0)
			{
				showView=false;
			}*/ //commented for now as it was giving a bug
			
			
			//end of check		
		  %> <input type="hidden" name="applicableRightsInt" value="<%=applicableRightsInt%>" /> 
		  	 <input type="hidden" name="applicableRightsString" value="<%=applicableRightsString%>" />
		  	 <Input type="hidden" name="showRight" value="T">
			 
		  <%
		  if (rights.compareTo("7") == 0){
    		  %>
		  		<TD align="center">
				<%
				if (showNew)
				{

		  		%>
               
       	        <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.grpRights)" CHECKED>
				<%
				}
				else
				{
				%>
           		<Input type="hidden" name="newr" value="0">
				<%
				}
				%>
                </TD>
	            <TD align="center">
				<%
				if (showEdit)
				{
		  		%>
				
    	           <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.grpRights)" CHECKED>
				<%
				}
				else
				{
				%>
           		<Input type="hidden" name="edit" value="0">
				<%
				} 
				%>
     	       </TD>							
		        <TD align="center"> 
		         <%
			  	if (showView)
			   	{
		  	  	%>
				
        		  <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.grpRights)" CHECKED>
        		<%
				}
				else
				{
				%>
	                <Input type="hidden" name="view" value="0"> 	 
				<%
				} 
				%>    
        		    
		        </TD>
        <%}else if (rights.compareTo("1") == 0){
		   %>
		      <TD align="center">
		   <% 
			if (showNew)
				{
		    	%>
				<Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.grpRights)" CHECKED>
		     	<%
				}
				else
				{ %>
                 <Input type="hidden" name="newr" value="0">	 
				<%
				 }
				%>
                </TD>
	            <TD align="center">
				<%
				if (showEdit)
				{
		  		%>
		         <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.grpRights)" >
		        <%
				}
				else
				{
				%>
                <Input type="hidden" name="edit" value="0"> 	 
				<%
				} 
				%>
				</TD>
	        <TD align="center"> 
	        <%
			  if (showView)
			   {
		  	  %>
                 <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.grpRights)">
            	<%
				}
				else
				{
				%>
	                <Input type="hidden" name="view" value="0"> 	 
				<%
				} 
				%>
	        </TD>
        <%}else if(rights.compareTo("3") == 0){
        		%>
		        <TD align="center">
		      <%
    		if (showNew)
				{
		    	%>
    		   <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.grpRights)" CHECKED>
	         	<%
				}
				else
				{ %>
                 <Input type="hidden" name="newr" value="0">	 
				<% }
				%>
                </TD>
	            <TD align="center">
				<%
				if (showEdit)
				{
		  		%>
		       <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.grpRights)" CHECKED>
				<%
				}
				else
				{
				%>
                 <Input type="hidden" name="edit" value="0"> 	 
				<%
				} 
				%>
				</TD>	
        <TD align="center"> 
        <%
		  if (showView)
		   {
		  %>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.grpRights)">
          <%
			}
			else
			{
			%>
	           <Input type="hidden" name="view" value="0"> 	 
			<%
			} 
			%>
        </TD>
        <%}else if (rights.compareTo("2") == 0)
		{
		%>
		   <TD align="center">
		      <%
    		if (showNew)
				{
		    	%>
               <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.grpRights)">
			   <%
				}
				else
				{ %>
                 <Input type="hidden" name="newr" value="0">	 
				<% }
				%>
                </TD>
	            <TD align="center">
				<%
				if (showEdit)
				{
		  		%>
			     <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.grpRights)" CHECKED>
				<%
				}
				else
				{
				%>
                 <Input type="hidden" name="edit" value="0"> 	 
				<%
				} 
				%>
				</TD> 
	        <TD align="center"> 
	        	<%
		  if (showView)
		   {
		  %>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.grpRights)">
          		<%
				}
				else
				{
				%>
	                <Input type="hidden" name="view" value="0"> 	 
				<%
				} 
				%>	
          			
        </TD>
        <%}else if (rights.compareTo("4") == 0){%>
		   <TD align="center">
		      <%
    		if (showNew)
				{
		    	%>
                 <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.grpRights)">
				 <%
				}
				else
				{ %>
                 <Input type="hidden" name="newr" value="0">	 
				<% }
				%>
                </TD>
	            <TD align="center">
				<%
				if (showEdit)
				{
		  		%>
		      <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.grpRights)">
			   <%
				}
				else
				{
				%>
                 <Input type="hidden" name="edit" value="0"> 	 
				<%
				} 
				%>
				</TD>
	         <TD align="center"> 
	          <%
			    if (showView)
			    {
		  	   %>
          		<Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.grpRights)" CHECKED>
	          	<%
				}
				else
				{
				%>
	                <Input type="hidden" name="view" value="0"> 	 
				<%
				} 
				%>
	          		   
        </TD>
        <%}else if (rights.compareTo("5") == 0){
		%>
		   <TD align="center">
		      <%
    		if (showNew)
				{
		    	%>
	           <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.grpRights)" CHECKED>
		   	 <%
				}
				else
				{ %>
                 <Input type="hidden" name="newr" value = "0">	 
				<% }
				%>
                </TD>
	            <TD align="center">
				<%
				if (showEdit)
				{
		  		%>
			 <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.grpRights)">
			    <%
				}
				else
				{
				%>
                 <Input type="hidden" name="edit" value="0"> 	 
				<%
				} 
				%>
				</TD>
        	<TD align="center"> 
        	  <%
			  	if (showView)
			   	{
		  	  	%>
          <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.grpRights)" CHECKED>
          	<%
			}
			else
			{
			%>
                <Input type="hidden" name="view" value="0"> 	 
			<%
			} 
			%>
          	
          			
        </TD>
        <%}else if (rights.compareTo("6") == 0){
		%>
		   <TD align="center">
		      <%
    		if (showNew)
				{
		    	%>
	          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.grpRights)" >
			  	   	 <%
				}
				else
				{ %>
                 <Input type="hidden" name="newr" value="0">	 
				<% }
				%>
                </TD>
	            <TD align="center">
				<%
				if (showEdit)
				{
		  		%>
	           <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.grpRights)" CHECKED>
   			    <%
				}
				else
				{
				%>
                 <Input type="hidden" name="edit" value="0"> 	 
				<%
				} 
				%>
				</TD>
        	<TD align="center"> 
        	<%
			if (showView)
			{
		  	%>
          	<Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.grpRights)" CHECKED>
          	<%
			}
			else
			{
			%>
                <Input type="hidden" name="view" value="0"> 	 
			<%
			} 
			%>
          		
        </TD>
        <%}else if (rights.compareTo("1") == 0){ 	%>
		   <TD align="center">
		      <%
    		if (showNew)
				{
		    	%>
	          <Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.grpRights)" CHECKED>
			   <%
				}
				else
				{ %>
                 <Input type="hidden" name="newr" value="0">	 
				<% }
				%>
                </TD>
	            <TD align="center">
				<%
				if (showEdit)
				{
		  		%>
			    <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.grpRights)">
               <%
				}
				else
				{
				%>
                 <Input type="hidden" name="edit" value="0"> 	 
				<%
				} 
				%>
			</TD>
        	<TD align="center"> 
        	<%
			  if (showView)
			   {
		  	   %>
          	    <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.grpRights)" CHECKED>
          	  <%
			   }
			  else
				{
				%>
                 <Input type="hidden" name="view" value="0"> 	 
				<%
				} 
				%>
	       	</TD>
        <%}else{ 
			%>
		   <TD align="center">
		      <%
    		if (showNew)
				{
		   	%>
		   		<Input type="checkbox" name="newr" onclick="changeRights(this,<%=count%>, document.grpRights)">
		   <%
				}
				else
				{ %>
                 <Input type="hidden" name="newr" value="0">	 
				<% }
				%>
                </TD>
	            <TD align="center">
				<%
				if (showEdit)
				{
		  		%> 
               <Input type="checkbox" name="edit" onclick="changeRights(this,<%=count%>, document.grpRights)">
			   <%
				}
				else
				{
				%>
                 <Input type="hidden" name="edit" value="0"> 	 
				<%
				} 
				%>
		</TD>
        <TD align="center">
				<%
				if (showView)
				{
		  		%> 
     	      <Input type="checkbox" name="view" onclick="changeRights(this,<%=count%>, document.grpRights)">
  			   <%
				}
				else
				{
				%>
                 <Input type="hidden" name="view" value="0"> 	 
				<%
				} 
				%>
			  
	        </TD>
        <%}%>
      </TR>
      <%}
	 }//end of show
	  %>
      <% if (pageRight > 4)

		  { %>

	  <TR>
	   <td colspan="4">
	    
	  
	  <jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="grpRightsFrm"/>
			<jsp:param name="showDiscard" value="N"/>
	  </jsp:include>
	  
		
		 </td>
	  </tr>
      <TR></tr>
      <TR>
     <% } 
	 %>
 
        <TD COLSPAN =4 align="right"> 
          <% if (pageRight > 4)

		  { %>
          <!--<Input Type=Submit Value=submit name=Submit>-->
	 <!-- <br><input type="image" src="../images/jpg/Submit.gif" onClick = "return validate()" align="absmiddle" border="0">	-->
          <% } 

		else { %>
          <!--<Input Type=Submit Value=submit name=Submit DISABLED>-->
	<!--<input type="image" src="../images/jpg/Submit.gif" onClick = "" align="absmiddle" DISABLED>	-->

          <% } %>
        </TD>
      </TR>
    </TABLE>
  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
</div><div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</BODY>

</HTML>







