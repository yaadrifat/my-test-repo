<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title>Register with eResearch</title>

<%@ page import="com.velos.eres.service.util.LC"%>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*"%>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>



<jsp:include page="panel.jsp" flush="true"/>



<SCRIPT Language="javascript">

 function  validate(){

     formobj=document.signup

     if (!(validate_col('User F Name',formobj.userFirstName))) return false

     if (!(validate_col('User LastName',formobj.userLastName))) return false

     if (!(validate_col('User Address',formobj.addPriUser))) return false

     if (!(validate_col('User City',formobj.addCityUser))) return false

     if (!(validate_col('User State',formobj.addStateUser))) return false

     if (!(validate_col('User Country',formobj.addCountryUser))) return false

     if (!(validate_col('User Email',formobj.addPhoneUser))) return false

     if (!(validate_col('User Email',formobj.addEmailUser))) return false

     if (!(validate_col('SiteName',formobj.siteName))) return false

     if (!(validate_col('Site Address',formobj.sitePerAdd))) return false

     if (!(validate_col('Site City',formobj.addCitySite))) return false

     if (!(validate_col('Site State',formobj.addStateSite))) return false

     if (!(validate_col('Site Country',formobj.addCountrySite))) return false

     if (!(validate_col('User Question',formobj.userSecQues))) return false

     if (!(validate_col('User Answer',formobj.userAnswer))) return false

 }



function openwin() {

      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")

;}



</SCRIPT>



<body>

<DIV class="formDefault" id="div1" >

<%	int userId = 0;

	String userAddresId = "";

	String dJobType = "" ;

	String dAccType= "" ;

	String dPrimSpl= "" ;

	CodeDao cd = new CodeDao();

	CodeDao cd2 = new CodeDao();

	CodeDao cd3 = new CodeDao();

	cd2.getCodeValues("acc_type");

	cd.getCodeValues("job_type");

	cd3.getCodeValues("prim_sp");

      dAccType = cd2.toPullDown("accType");

      dJobType = cd.toPullDown("userCodelstJobtype");

      dPrimSpl = cd3.toPullDown("primarySpeciality");

%>



<Form name="signup" method="post" action="accountsave.jsp" onSubmit="return validate();">

<table width="400" cellspacing="0" cellpadding="0">

<tr><td>

<P class = "defComments">

<br>

Please complete and submit the following registration form. Once you are approved, your account will be created and you will receive an email confirming your assigned login and password.

</P></td></tr>

</table>



<P class = "sectionHeadings">

Member Information

</P>



<table width="500" cellspacing="0" cellpadding="0">

  <tr>

    <td width="200">

       First Name <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<input type="text" name="userFirstName" size = 35 MAXLENGTH = 30>

    </td>

  </tr>

  <tr>

    <td width="200">

       Last Name <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<input type="text" name="userLastName" size = 35 MAXLENGTH = 30>

    </td>

  </tr>

  <tr>

    <td width="200">

       Address <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<input type="text" name="addPriUser" size = 35 MAXLENGTH = 50>

    </td>

  </tr>

   <tr>

    <td width="200">

       City <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<input type="text" name="addCityUser" size = 35  MAXLENGTH = 30>

    </td>

  </tr>

  <tr>

    <td width="200">

       State <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<input type="text" name="addStateUser" size = 35 MAXLENGTH = 30>

    </td>

  </tr>

  <tr>

    <td width="200">

       Country <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<input type="text" name="addCountryUser" size = 35 MAXLENGTH = 30>

    </td>

  </tr>

  <tr>

    <td width="200">

       Zip/Postal Code </FONT>

    </td>

    <td>

	<input type="text" name="addZipUser" size = 35 MAXLENGTH = 15>

    </td>

  </tr>

  <tr>

    <td width="200">

       Phone <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<input type="text" name="addPhoneUser" size = 35 MAXLENGTH = 100>

    </td>

  </tr>

  <tr>

    <td width="200">

       E-Mail <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<input type="text" name="addEmailUser" size = 35 MAXLENGTH = 100>

    </td>

  </tr>

  <tr>

    <td width="200">

       Account Type

    </td>

    <td>

	<%=dAccType%>

    </td>

  </tr>

  <tr>

    <td width="200">

       Job Type

    </td>

    <td>

	<%=dJobType%>

    </td>

  </tr>

</table>

<br>



<table  width="550" cellspacing="0" cellpadding="0" >

  <tr>

    <td width="500" colspan = "3" id="tdComments">

	If you are an investigator, please complete the following:

    </td>

  </tr>

	<tr height=10>	</tr>

  <tr>

    <td width="225">

       Primary Speciality

    </td>

    <td width="225">

	<%=dPrimSpl%>

  </td>

  </tr>

  <tr>

     <td  width="300">

       How many years have you been involved in clinical research?

    </td>

    <td >

	<input type="text" name="userWrkExp" size = 20 MAXLENGTH = 100>

    </td>

  </tr>

  <tr>

    <td width="300">

	Which phases of trials have you been involved with?

    </td>

    <td >

	<input type="text" name="userPhaseInv" size = 20 MAXLENGTH = 100>

    </td>

  </tr>

</table>



<P class = "sectionHeadings">

Organization Details

</P>



<table width="500" cellspacing="0" cellpadding="0" >

  <tr>

    <td width="200">

       Organization Name <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<input type="text" name="siteName" size = 35 MAXLENGTH = 50>

    </td>

  </tr>



  <tr>

    <td width="200">

       Address <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<input type="text" name="sitePerAdd" size = 35 MAXLENGTH = 50>

    </td>

  </tr>

   <tr>

    <td width="200">

       City <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<input type="text" name="addCitySite" size = 35  MAXLENGTH = 30>

    </td>

  </tr>

  <tr>

    <td width="200">

       State <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<input type="text" name="addStateSite" size = 35 MAXLENGTH = 30>

    </td>

  </tr>

  <tr>

    <td width="200">

       Country <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<input type="text" name="addCountrySite" size = 35 MAXLENGTH = 30>

    </td>

  </tr>

  <tr>

    <td width="200">

       Zip/Postal Code </FONT>

    </td>

    <td>

	<input type="text" name="addZipSite" size = 35 MAXLENGTH = 15>

    </td>

  </tr>

  <tr>

    <td width="200">

       Phone

    </td>

    <td>

	<input type="text" name="addPhoneSite" size = 35 MAXLENGTH = 100>

    </td>

  </tr>

  <tr>

    <td width="200">

       Secret Question <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<input type="text" name="userSecQues" size = 35 MAXLENGTH = 150>

    </td>

  </tr>

  <tr>

    <td width="200">

       Your Answer  <FONT class="Mandatory">* </FONT>

    </td>

    <td>

	<input type="text" name="userAnswer" size = 35 MAXLENGTH = 150>

    </td>

  </tr>

</table>



<table width="500" cellspacing="0" cellpadding="0" >



  <tr></tr>

  <tr>

    <td colspan="2">

	<input type="checkbox" name="accMailFlag" value = "Y" CHECKED>

	&nbsp;

       Check if you want Velos to contact you regarding useful information

    </td>

  </tr>

  <tr>

    <td colspan = "2">

	<input type="checkbox" name="accPubFlag" value = "Y" CHECKED>

	&nbsp;

	Check if you agree to Velos giving your name to other companies that might have useful information for you

    </td>

  </tr>

</table>



<P class = "defComments">

Registration Agreement and <A href="privacypolicy.htm" target="Information" onClick="openwin()">Privacy Policy</A>

</P>



<table width="100%" cellspacing="0" cellpadding="0">

<tr><td>

	<TextArea rows=8 cols=70% readonly class = "defComments">TERMS OF SERVICE

www.veloseresearch.com and www.velos.com (the "Velos Web Sites") are owned and operated by Velos Inc. ("Velos").  This document sets forth the Terms of Service upon which you and/or any organization(s) (collectively, "User") on whose behalf you use the Velos Web Sites may access the products, services, information, and other materials available through the Velos Web Sites.

Before accessing the Velos Web Sites, please read the following terms and conditions carefully, as they govern the use of the Velos Web Sites and all products, services, information and other materials available through them.

By accessing and using the Velos Web Sites, you agree to abide by the Terms of Service below.  We have created these rules and guidelines to protect you and our other users.  If you have questions about the Terms of Service, or you encounter another user who is not abiding by these Terms of Service, please contact us.

1. Acceptance of Terms of Service

By accessing, viewing, or using the products, services, information or other materials available on or through the Velos Web Sites, you thereby indicate that you understand these Terms of Service and that you accept and agree to comply with them.  Velos reserves the right to change these Terms of Service from time to time.  Velos will post the most recent date of review on this page.  Your continued use of the Velos Web Sites following any such change(s) to these Terms of Service will be conclusively deemed acceptance of such change(s).

2. Privacy Policy

The use, storage, and disclosure of information collected on or contained in the Velos Web Sites may be subject to various federal and state laws, including, but not limited to, laws addressing privacy rights and confidentiality of medical records.  Velos will follow all applicable federal and state laws, rules, and regulations.  For more specific information on our Privacy Policy, click here.

3. Termination

Velos reserves the right, in its sole discretion, to immediately suspend or terminate these Terms of Service and/or your ability to use the Velos Web Sites and/or any other service provided to you by Velos, without cause and without notice to you.

In the event that Velos determines, in its sole discretion, that you have breached any portion of these Terms of Service, or have otherwise demonstrated conduct inappropriate for the Velos Web Sites, Velos reserves the right to (a) warn you via email that you have violated these Terms of Service; (b) delete any content provided to Velos; (c) discontinue your ability to use the Velos Web Sites and/or any other service provided to you by Velos; (d) notify, send information or content to, and/or fully cooperate with the proper law enforcement authorities for further action; and/or (e) any other action which Velos deems to be appropriate.

4. Modifications to the Velos Web Sites

Velos may modify or discontinue either of or both of the Velos Web Sites without notice or liability to you or any user.

5. Content of Messages

You understand and agree that you are responsible for the content of any messages posted or sent by you or any user of your account (whether authorized or unauthorized), and the consequences of any such messages.  You certify that:

·	any information you provide to us is true and correct to the best of your knowledge;

·	you have the full authority to provide any information provided, whether that information pertains to you or to any other person; and

·	you permit Velos to use the information provided within the limitations of Velos Privacy Policy and applicable law.

Velos reserves the right to immediately terminate your registration without notice if it determines, in its sole discretion, that you are violating any of the terms contained in this Terms of Service.



Velos reserves the right to delete any material from the Velos Web Sites without notice to you or any other user of the Velos Web Sites.



You understand and acknowledge: (a) Velos permits you to access content that is protected by copyrights, trademarks and/or other intellectual property and proprietary rights ("Rights"); (b) the Rights are valid and protected in all media and technologies existing now or later developed; and (c) except as otherwise set forth, these Terms of Service and the applicable copyright, trademark and other laws govern your use of such content.  You may not copy, reproduce, retransmit, distribute, publish, commercially exploit or otherwise transfer any material on the Velos Web Sites subject to any Rights. You shall have the burden of proving that any information, software, image or any other content on the Velos Web Sites is not protected by Rights.  You acknowledge that Velos has acquired Rights to use any materials posted to the Velos Web Sites (as described below), and that you will not acquire any Rights by downloading such materials.



You grant Velos and its affiliates a royalty-free, perpetual, worldwide, irrevocable, non-exclusive right (including any moral rights) and license to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute, communicate to the public, perform and display content submitted to any "public area" of the Velos Web Sites (excluding other private user-only areas), in whole or in part, and/or permit Velos to incorporate it in other works in any form, media or technology now known or later developed, for the full term of any Rights that may exist in such content.  By submitting content to Velos, you represent and warrant that the holder of any Rights, including, but not limited to, moral rights in such content, has completely and effectively waived all such rights and validly and irrevocably granted to you the right to grant the license stated above.  You understand and agree that Users may access, display, view, store and reproduce such content for personal use.



6. Velos Intellectual Property Rights

Except as expressly provided herein, Velos does not grant you any express or implied right under any patent, copyright, trademark, or trade secret information.  Unauthorized use of any material contained on the Velos Web Sites may violate patent laws, copyright laws, trademark laws, trade secret laws, laws pertaining to privacy and publicity rights or other regulations or statutes.  Please be aware that Velos actively and aggressively enforces its intellectual property rights to the fullest extent of the law.



The contents of the Velos Web Sites are Copyright © Velos, Inc. All Rights Reserved. Velos also owns a copyright in the Velos Web Sites as a collective work and/or compilation, and in the selection, coordination, arrangement, and enhancement of the content of the Velos Web Sites.



Velos(TM) and Velos ®, and any and all other names, logos, and icons identifying Velos products, services, information and other materials are proprietary marks of Velos, and any use of such marks without the express written permission of Velos is strictly prohibited.  Other product and company names mentioned herein might be the trademarks of their respective owners and as such, those third parties also may have the right to aggressively enforce their intellectual property rights to the fullest extent of the law.



7. Third Party Content and Products

Some or all of the information and other materials or products available on the Velos Web Sites may have been prepared or may be offered by third parties not affiliated with Velos. Velos makes no warranties with regard to such information or other materials or products, nor does Velos assume any responsibility or liability for any decisions based upon such information or other materials or for any use of such products.  Content provided by Velos should not be a substitute for discussions with and/or evaluations from qualified Health Care professionals. User's correspondence or any other dealings with such third parties are solely between User and such third parties and may be subject to any terms and conditions as such third parties may prescribe.  User agrees that Velos shall not be responsible for any loss or damage of any sort incurred as a result of any such dealings or as the result of the presence of such third parties on the Velos Web Sites.



8. Links to Other Sites

Velos may provide links, in its sole discretion, to other sites on the World Wide Web for the convenience of its users.  While we make every effort to ensure a high level of quality in our content, these sites are maintained by third parties over which Velos exercises no control.  Accordingly, Velos expressly disclaims any responsibility for the content, the accuracy of the information or other materials, and/or quality of products or services provided by or advertised on these third-party web sites.   Moreover, these links do not imply an endorsement by Velos with respect to any third-party, any web site, or any products or services provided by any third-party, through its web site or otherwise.



9. Your Password

Use of certain portions of the Velos Web Sites requires a password.  You will be assigned a unique login user name ("User Name") and a password ("Password").  Each User Name and corresponding Password can only be used by one User.  Anyone with knowledge of both a User Name and Password can gain access to restricted portions of the Velos Web Sites and the information available to each User.  Accordingly, all Passwords must be kept secret.  By agreeing to the Terms of Service, you agree to be solely responsible for the confidentiality and use of your respective Password(s), as well as for any communications entered through the Velos Web Sites using such Passwords.  If you become aware of any loss, theft, or unauthorized use of your Password, you should immediately notify Velos of such loss, theft, or unauthorized use.  Velos reserves the right to delete or change a Password or to withdraw user privileges or deny services at any time and for any reason.  If you forget or lose your password, please contact us and we will assist you in recovering it.



10. Confidentiality on the Internet

While Velos uses reasonable efforts to create safe, secure, and reliable Web Sites, please be advised that the use of other related services over the Internet cannot be guaranteed.  Use of the Internet, including, without limitation, the Velos Web Sites, is solely at your own risk and is subject to all applicable local, state, national and international laws and regulations.  Accordingly, Velos and its affiliates are not responsible for the security of any information transmitted via the Internet, the accuracy of the information contained on the Velos Web Sites, or for the consequences of any reliance on such information.  You assume all risks involved in using the Internet, including, without limitation, the Velos Web Sites and must make your own determination as to these matters.



11. Security of the Velos Web Sites

Any actual or attempted unauthorized use of the Velos Web Sites including, without limitation, "hacking," may result in criminal and/or civil prosecution.  For your protection, Velos reserves the right to view, monitor, and record activity on the Velos Web Sites without notice or your permission.  Any information obtained by monitoring, reviewing or recording activity on the Velos Web Sites is subject to review by law enforcement organizations in connection with investigation or prosecution of possible criminal activity on the Velos Web Sites.  Velos will also comply with all court orders involving requests for such information.



12. Events Beyond Velos Control

You expressly absolve and release Velos from any claim of harm resulting from a cause beyond Velos control, including, but not limited to, failure of electronic or mechanical equipment or communication lines, telephone or other interconnect problems, computer viruses, unauthorized access, theft, operator errors, severe weather, earthquakes, or natural disasters, strikes or other labor problems, wars or governmental restrictions.



13. Disclaimer of Warranties; Indemnification of Velos

YOU UNDERSTAND AND EXPRESSLY AGREE THAT:

·	YOUR USE OF THE VELOS WEB SITES IS AT YOUR OWN RISK. THE VELOS WEB SITES ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS.

·	VELOS EXPRESSLY DISCLAIMS ALL WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT.

·	VELOS MAKES NO WARRANTY THAT VELOS.COM WILL MEET YOUR REQUIREMENTS, OR THAT THE VELOS WEB SITES WILL OPERATE UNINTERRUPTED OR IN A TIMELY, SECURE OR ERROR-FREE MANNER.

·	YOU MAY DOWNLOAD OR OTHERWISE OBTAIN MATERIAL AND/OR DATA THROUGH THE USE OF THE VELOS WEB SITES AT YOUR OWN DISCRETION AND RISK AND THAT YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR LOSS OF DATA RESULTING FROM SUCH MATERIAL AND/OR DATA.

·	VELOS DOES NOT GUARANTEE THE ACCURACY OR COMPLETENESS OF ANY INFORMATION ACCESSIBLE FROM OR PROVIDED IN CONNECTION WITH THE VELOS WEB SITES.

·	VELOS IS NOT RESPONSIBLE FOR ANY ERRORS OR OMISSIONS IN OR FOR THE RESULTS OBTAINED FROM THE USE OF SUCH INFORMATION.

·	VELOS IS NOT ENGAGED IN RENDERING LEGAL, MEDICAL, COUNSELING OR OTHER PROFESSIONAL SERVICES OR ADVICE. VELOS ENCOURAGES YOU TO SEEK APPROPRIATE PROFESSIONAL ADVICE OR CARE FOR ANY SITUATION OR PROBLEM THAT YOU MAY HAVE.

NO OTHER WARRANTIES SHALL EXIST THAT ARE NOT EXPRESSLY MADE HEREIN.



14. Limitation of Liability

VELOS SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE OR CONSEQUENTIAL DAMAGES, WHETHER FORESEEABLE OR NOT, RESULTING FROM (A) THE USE OR THE INABILITY TO USE THE VELOS WEB SITES; (B) UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR TRANSMISSIONS OR DATA; OR (C) ANY BREACH OF THESE TERMS OF SERVICE, INCLUDING, BUT NOT LIMITED TO, THE COST OF PROCUREMENT OF SUBSTITUTE GOODS AND SERVICES, DAMAGES FOR LOSS OF PROFITS, USE, DATA OR OTHER INTANGIBLES, EVEN IF VELOS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.



VELOS CANNOT CONTROL ANY PARTICULAR RESULTS FROM ANY THERAPEUTIC COURSE OF ACTION. VELOS DISCLAIMS ANY LIABILITY FOR, AND YOU EXPRESSLY WAIVE ANY COURSE OF ACTION AGAINST VELOS, INC. BASED ON OR RELATED TO, PARTICIPATION IN ANY CLINICAL <%=LC.Std_Study_Upper%> DESCRIBED ON THE VELOS WEB SITES.



15. Indemnification

You agree to indemnify and hold each of Velos, subsidiaries, affiliates, officers, directors and employees, harmless from any claim or demand (including, but not limited to, reasonable attorney fees) made by any third party due to or arising out of your use of the Velos Web Sites.



16. Assignment

Velos may assign its rights and obligations under these Terms of Service at any time without notice to you.



17. Applicable Law

These Terms of Service shall be governed by and construed in accordance with the laws of the State of California excluding its conflict of laws provisions.  Any action to enforce these Terms of Service will be brought in the federal or state courts in and for the State of California, and all parties to these Terms of Service expressly agree to be subject to the jurisdiction and venue of such courts.



18. International Applicability

Velos is controlled and operated by Velos from its offices within the United States.  Velos makes no representation that the information and other materials on the Velos Web Sites are appropriate or available for use in other locations, and access to them from any territory where any of the contents of the Velos Web Sites are illegal is prohibited.  Those who choose to access the Velos Web Sites from other locations do so at their own risk and are responsible for compliance with applicable local laws.



19. Contact Information

Any questions regarding the Velos Web Sites and any requests for additional information concerning Velos and its products and services may be directed to Velos at information@Velos.com.



Use of the Velos Web Site indicates your acceptance of the above Terms of Service.



Revised March 28, 2001

</TextArea>

</td></tr>

</table>

<br>

<table width="430" cellspacing="0" cellpadding="0">

<tr><td>

	<P class = "italicsComments">By clicking on the I Accept button, you agree to the terms and conditions in the Agreement and your registration will be submitted for account creation. 	</P>

</td>

<td>

<input type="Submit" name="submit" value="I Accept">

</td>

</tr>

</table>

</Form>

<div>

<jsp:include page="bottompanel.jsp" flush="true">

</jsp:include>

</div>



</div>

<div id="emenu">

</div>



</body>

</html>

