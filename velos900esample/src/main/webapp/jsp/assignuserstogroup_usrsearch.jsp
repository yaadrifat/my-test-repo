<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<title><%=MC.M_Assign_UsersToGrp%><%--Assign Users to Group*****--%></title>



<SCRIPT Language="javascript">
function checkUser()
{
formobj=document.users
 if (!(validate_col('E-Signature',formobj.eSign))) return false
     if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect E-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }	

totrows = document.users.totalrows.value;
	if (totrows < 1)
	{
		alert("<%=MC.M_SelAtLeast_OneUsr%>");/*alert("Please select at least one user");*****/
		return false;
	}
	return true;
}
function changeCount(row)

	{

	  //alert("Check");	

	  selrow = row ;

	  //alert(selrow +obj.name);



	  totrows = document.users.totalrows.value;

	  totusers = document.users.lusers.value;

	  rows = parseInt(totrows);	

	  usernum = parseInt(totusers);	

	

	if (usernum > 1)

	{

	  if (document.users.assign[selrow].checked)

		{ 

			

			rows = rows + 1;

		}

	  else

		{

			rows = rows - 1;

		}

	

	}else{

	  if (document.users.assign.checked)

		{ 

			

			rows = rows + 1;

		}

	  else

		{

			rows = rows - 1;

		}



	}	

	document.users.totalrows.value = rows;

	}

	



</SCRIPT>



<% String src;

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*" %>



<%--Link Rel=STYLESHEET HREF="common.css" type=text/css--%>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>





<body>
<br>
<jsp:useBean id="groupB" scope="session" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id="userB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil"%>
<DIV class="browserDefault" id = "div1"> 
  <%

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

{

	String uName = (String) tSession.getValue("userName");



	int pageRight = 0;

	String accId = (String) tSession.getValue("accountId");

	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("ASSIGNUSERS"));	

	

   if (pageRight > 0 )

	 {

       UserDao userDao=new UserDao(); 

	 int grpId = EJBUtil.stringToNum(request.getParameter("grpId"));	

	 String grpName = request.getParameter("grpName");

	 userDao.getAvailableAccountUsers(EJBUtil.stringToNum(accId),grpId); 	



       ArrayList usrLastNames;

       ArrayList usrFirstNames;

       ArrayList usrMidNames;

       ArrayList usrIds;



	 String usrLastName = null;

	 String usrFirstName = null;

	 String usrMidName = null;

	String usrId=null;	

	 int counter = 0;

%>

  <P class = "userName"> <%= uName %> </P>
  <P class="sectionHeadings"> <%=MC.M_MngAccGrp_AssnUsrGrp%><%--Manage Account >> Groups >> Assign Users to Group*****--%>: <%=grpName %></P>

  <Form name="users" method="post" action="updategrpusers.jsp" >
    <Input type="hidden" name= "gId" value= <%=grpId%> >
    <Input type="hidden" name="totalrows" value=0  >
    <Input type="hidden" name= "gName" value= <%=grpName%> >
    <Input type="hidden" name= "src" value= <%=src%> >
    <table width="550" cellspacing="0" cellpadding="0" border=0>
      <tr > 
        <td width = "350"> 
        </td>
        <td width="200"> 
          <p align = right> <A href="userdetails.jsp?mode=N&srcmenu=<%=src%>"><%=LC.L_Add_NewUser%><%--Add 
            New User*****--%></A> </p>
        </td>
      </tr>
    </table>
    <table width="450" border=0>
      <tr> </tr>
      <%

		usrLastNames = userDao.getUsrLastNames();

		usrFirstNames = userDao.getUsrFirstNames();

		usrMidNames = userDao.getUsrMidNames();

            usrIds = userDao.getUsrIds();

		int i;

		int lenUsers = usrLastNames.size();

	%>
      <tr id ="browserBoldRow"><%Object[] arguments1 = {lenUsers}; %> 
        <td width = 400> <%=MC.M_Tot_NumOfUsers%><%--Total Number of Users*****--%> :<%=VelosResourceBundle.getLabelString("L_Usr_S",arguments1)%> <%-- <%= lenUsers%> User(s)*****--%> </td>
      </tr>
      <tr> 
        <th> <%=LC.L_Available_Users%><%--Available Users*****--%> </th>
        <th> <%=LC.L_Select%><%--Select--%> </th>
      </tr>
      <Input type="hidden" name = "lusers" value= <%=lenUsers%> >
      <%

		for(i = 0 ; i < lenUsers ; i++)

		  {



		usrLastName=((usrLastNames.get(i)) == null)?"-":(usrLastNames.get(i)).toString();

		usrFirstName=((usrFirstNames.get(i))==null)?"-":(usrFirstNames.get(i)).toString();

		usrMidName=((usrMidNames.get(i))==null)?"-":(usrMidNames.get(i)).toString();

		usrId = ((Integer)usrIds.get(i)).toString();



		if ((i%2)==0) {

  %>
      <tr class="browserEvenRow"> 
        <%

		}

		else{

  %>
      <tr class="browserOddRow"> 
        <%

		}

  %>
        <td width =450>  <%=usrFirstName%>&nbsp;<%=usrLastName%></A> </td>
        <td width =50> 
          <Input type="checkbox" name="assign" value = "<%=usrId %>" onclick="changeCount(<%=i%>)"  >
        </td>
      </tr></tr>
      <%

		}

%>
</table>
<% if (pageRight > 4) { %>
<table>
<tr>
	   <td >
		<%=LC.L_EHypenSign%><%--E-Signature*****--%> <FONT class="Mandatory">* </FONT>
	   </td>
	   <td>
		<input type="password" name="eSign" maxlength="8">
	   </td>
</tr>
</table>
<%}%>



<table  width="450" border=0>
      <TR> 
        <TD COLSPAN =4 align="right"> &nbsp;
          <% if (pageRight > 4)

		  { %>
	<br><input type="image" src="../images/jpg/Submit.gif" onClick = "return checkUser()" align="absmiddle" border="0">	

          <% } 

		else { %>
          <!--<Input Type=Submit Value=submit name=Submit DISABLED>-->
          <% } %>
        </TD>
      </TR>
    </table>
	
	

  </Form>

  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>
