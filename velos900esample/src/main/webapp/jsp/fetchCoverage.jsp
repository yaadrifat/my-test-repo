<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html>
<head>
<style type="text/css">
.yui-dt-liner { white-space:normal; } 
.yui-skin-sam .yui-dt td.up { background-color: #efe; } 
.yui-skin-sam .yui-dt td.down { background-color: #fee; }
.table-basic { cellspacing:0px;border-spacing:0px;border-style:groove;border-bottom-width:thin;border-top-width:thin;border-left-width:thin;border-right-width:thin; }
.table-basic th { cellspacing:0px;border-spacing:0px;border-style:groove;border-bottom-width:thin;border-top-width:thin;border-left-width:thin;border-right-width:thin; }
.table-basic td { cellspacing:0px;border-spacing:0px;border-style:groove;border-bottom-width:thin;border-top-width:thin;border-left-width:thin;border-right-width:thin; }
</style>
<%@page import="com.velos.eres.web.objectSettings.ObjectSettingsCache,com.velos.eres.web.objectSettings.ObjectSettings"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@page import="com.velos.esch.business.common.SchCodeDao"%>
<%@page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>
<%@page import="java.text.DecimalFormat" %>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="commonB" scope="request" class="com.velos.eres.web.common.CommonJB"/>
<%
ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = null;
String titleStr = LC.L_Calendar;/*String titleStr = "Calendar";*****/
if (sessionmaint.isValidSession(request.getSession(true)))
{
    String accId = (String) request.getSession(true).getValue("accountId");
    
    ArrayList tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "protocol_tab");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("7".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
            	Object[] arguments = {titleStr,settings.getObjDispTxt()};
            	titleStr = VelosResourceBundle.getLabelString("L_Gtgt",arguments);
            	/*titleStr +=  " >> " + settings.getObjDispTxt();*****/
            }
            break;
        } 
    }
    tempList = null;
}
String src= StringUtil.htmlEncodeXss(request.getParameter("srcmenu"));
%>
<title><%=titleStr %></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<jsp:include page="ui-include.jsp" flush="true"/>
</head>

<% 
HttpSession tSession = request.getSession(true); 
String accountId = "";
String studyId = "";
String grpId = "";
String usrId = "";
if (sessionmaint.isValidSession(tSession)) {
	accountId = (String) tSession.getValue("accountId");
	studyId = (String) tSession.getValue("studyId");
	grpId = (String) tSession.getValue("defUserGroup");
	usrId = (String) tSession.getValue("userId");
	String sessId = tSession.getId();
	if (sessId.length()>8) { sessId = sessId.substring(0,8); }
	sessId = Security.encrypt(sessId);
	char[] chs = sessId.toCharArray();
	StringBuffer sb = new StringBuffer();
	DecimalFormat df = new DecimalFormat("000");
	for (int iX=0; iX<chs.length; iX++) {
	    sb.append(df.format((int)chs[iX]));
	}
	String keySessId = sb.toString();

	if ("N".equals(request.getParameter("mode"))) {
		studyId = "";
		tSession.removeAttribute("studyId");
	}
%>
<body class="yui-skin-sam yui-dt yui-dt-liner">
<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<%
	String from = "version";
	String tab = request.getParameter("selectedTab");
    String calassoc = StringUtil.trueValue(request.getParameter("calassoc"));
	String studyIdForTabs = request.getParameter("studyId");
    String includeTabsJsp = "irbnewtabs.jsp";
    int pageRight = 0;
	String calledFrom = request.getParameter("calledFrom");
	if (calledFrom.equals("S")) {
		StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
		if ((stdRights.getFtrRights().size()) == 0){
			pageRight= 0;
		} else {
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
		}
	} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
	}
	%>
	<DIV class="BrowserTopn" id = "div1">
		<jsp:include page="protocoltabs.jsp" flush="true">
		<jsp:param name="calassoc" value="<%=StringUtil.htmlEncodeXss(calassoc)%>" />
		<jsp:param name="selectedTab" value="<%=StringUtil.htmlEncodeXss(tab)%>" />
		</jsp:include>
	</DIV>
	<DIV class="BrowserBotN BrowserBotN_CL_2" id = "div2">
	<%
	if (pageRight > 0) {

	} // end of pageRight > 0
	String protocolId = request.getParameter("protocolId");
	if(protocolId == null || protocolId == "" || protocolId.equals("null") || protocolId.equals("")) {
%>
	<jsp:include page="calDoesNotExist.jsp" flush="true"/>
<%
	} else {
	String calStatus = null;
	SchCodeDao scho = new SchCodeDao();
	String calStatusPk = "";
	String calStatDesc = "";
	if (pageRight > 0) {
	    String tableName = null;
 		if("L".equals(calledFrom) || "P".equals(calledFrom)){
	   		tableName ="event_def";
			eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventdefB.getEventdefDetails();
			//KM-#DFin9
			calStatusPk = eventdefB.getStatCode();
			calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
			calStatDesc = scho.getCodeDescription(EJBUtil.stringToNum(calStatusPk));

	   	} else {
	   		tableName ="event_assoc";
			eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventassocB.getEventAssocDetails();
			//KM-#DFin9
			calStatusPk = eventassocB.getStatCode();
			calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
			calStatDesc = scho.getCodeDescription(EJBUtil.stringToNum(calStatusPk));

	   	}
		String duration = StringUtil.trueValue(request.getParameter("duration"));
		StringBuffer urlParamSB = new StringBuffer();
		urlParamSB.append("protocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&calledFrom=").append(StringUtil.htmlEncodeXss(calledFrom))
		.append("&calledfrom=").append(StringUtil.htmlEncodeXss(calledFrom))
		.append("&duration=").append(StringUtil.htmlEncodeXss(duration))
		.append("&calassoc=").append(StringUtil.htmlEncodeXss(calassoc))
		.append("&calstatus=").append(StringUtil.htmlEncodeXss(request.getParameter("calStatus")))
		.append("&mode=").append(StringUtil.htmlEncodeXss(request.getParameter("mode")))
		.append("&calProtocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&tableName=").append(tableName)
		.append("&pr=").append(pageRight);
		;
		SchCodeDao schDao = new SchCodeDao();
		schDao.getCodeValues("coverage_type");
		ArrayList covSubTypeList = schDao.getCSubType();
		ArrayList covDescList = schDao.getCDesc();
		StringBuffer sb1 = new StringBuffer();
		for (int iX=0; iX<covSubTypeList.size(); iX++) {
		    String covSub1 = ((String)covSubTypeList.get(iX)).replaceAll("'", "\\\\'");
		    String covDesc1 = ((String)covDescList.get(iX)).replaceAll("'", "\\\\'");
			if (covSub1 == null || covDesc1 == null) { continue; }
			sb1.append("<b>").append(covSub1).append("</b>").append("=").append(covDesc1).append("<br/>");
		}
		String covTypeLegend = sb1.toString();
%>
<script>
function reloadCoverageGrid() {
	YAHOO.example.CoverageGrid = function() {
		var args = {
			urlParams: "<%=urlParamSB.toString()%>",
			dataTable: "coveragegrid"
		};
		
		myCoverageGrid = new VELOS.coverageGrid('fetchCoverageJSON.jsp', args);
		myCoverageGrid.startRequest();
    }();
}
YAHOO.util.Event.addListener(window, "load", function() {
	reloadCoverageGrid();
});
function openExport(format) {
	var newWin = window.open("donotdelete.html","exportWin","toolbar=no,scrollbars=yes,resizable=yes,menubar=yes,status=no,left=20,top=20");
	newWin.document.write('<html>');
	newWin.document.write('<head><title><%=LC.L_Coverage_AnalysisExport%></title></head><body>');/*newWin.document.write('<head><title>Coverage Analysis Export</title></head><body>');*****/
	newWin.document.write('<form name="dummy" target="_self" action="fetchCoverageExport.jsp?');
	newWin.document.write('format='+format+'&');
	newWin.document.write('<%=urlParamSB.toString()%>');
	newWin.document.write('" method="POST">');
	newWin.document.write('<input type="hidden" id="key" name="key" ');
	newWin.document.write(' value="<%=keySessId%>"/>');
	newWin.document.write('</form>');
	newWin.document.write('</body></html>');
	newWin.document.close();
	newWin.document.dummy.submit();
}
</script>

<%
	int accId = EJBUtil.stringToNum((String) tSession.getAttribute("accountId"));
	SettingsDao setDao = commonB.getSettingsInstance();
	setDao.getSettingsValue("COVERAGE_ANALYSIS_MODE", accId, 0, 0);
	ArrayList setValArray = setDao.getSettingValue();
	String covMode = null;
	if (setValArray != null && setValArray.size() > 0) {
		covMode = (String)setValArray.get(0);
	}
	if (covMode == null || covMode.length() == 0) { covMode = "V"; } // Make view-first the default mode
	StringBuffer secondElement = new StringBuffer();
	if (covMode.equals("V")) {
	    secondElement.append("<input type='hidden' id='save_changes' name='save_changes'/><FONT class='Mandatory'>");
	    if ((calStatus.equals("A")) || (calStatus.equals("F")) || !isAccessibleFor(pageRight, 'E')) {
	    	secondElement.append("("+MC.M_ClkView_ChgSvd+")");/*secondElement.append("(Click row to view; changes cannot be saved)");*****/
	    } else {
	    	secondElement.append("("+MC.M_ClkRow_ToEdt+")");/*secondElement.append("(Click row to edit)");*****/
	    }
	    secondElement.append("</FONT>");
	} else if (covMode.equals("E")) {
		secondElement.append("<button type='submit' id='save_changes' name='save_changes'")
			.append(" onclick=\"if (f_check_perm("+pageRight+",'E')) { void(0); } \">"+LC.L_Preview_AndSave+"</button>");
	}
%>
  <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign"  >
  <tr height="7"></tr>
  <tr>
  <!--KM-#DFin9-->
  <td><%=LC.L_Cal_Status%><%-- Calendar Status*****--%>: <%=calStatDesc%>&nbsp;&nbsp;&nbsp;&nbsp;<%=secondElement.toString()%>
    &nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onmouseover="return overlib('<%=covTypeLegend%>',CAPTION,'<%=LC.L_Coverage_TypeLegend%><%-- Coverage Type Legend*****--%>');" onmouseout="return nd();"><%=LC.L_Coverage_TypeLegend%><%-- Coverage Type Legend*****--%></a>
    &nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Export_To%><%-- Export to*****--%>:&nbsp;&nbsp;<a href="javascript:openExport('html');"><img title="<%=LC.L_Printer_Friendly %><%-- Printer Friendly*****--%>" alt="<%=LC.L_Printer_Friendly %><%-- Printer Friendly*****--%>" align="bottom" border="0" src="../images/jpg/preview.gif" width="21" height="20"/></a>
    &nbsp;&nbsp;<a href="javascript:openExport('excel');"><img title="<%=LC.L_Export_ToExcel%><%-- Export to Excel*****--%>" alt="<%=LC.L_Export_ToExcel%><%-- Export to Excel*****--%>" align="bottom" border="0" src="../images/jpg/excel.GIF"/></a>
    &nbsp;&nbsp;<a href="javascript:openExport('word');"><img title="<%=LC.L_ExportTo_Word%><%-- Export to Word*****--%>" alt="<%=LC.L_ExportTo_Word%><%-- Export to Word*****--%>" align="bottom" border="0" src="../images/jpg/word.GIF"/></a>
  </td>
  </tr>
    <tr height="7"></tr>
  </table>
  <div id="coveragegrid"></div>
  <div id="coveragenotes"></div>
<%
  } // end of pageRight
  else {
%>
<!--12-04-2011 #6013 @Ankit  -->
	<jsp:include page="accessdenied.jsp" flush="true"/>
<%
  } // end of else of pageRight
} // end of else of invalid protocolId
} else { // else of valid session
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
} // end of else of valid session
%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu" > 
  <jsp:include page="getmenu.jsp" flush="true"/> 
</div>
</body>
</html>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>