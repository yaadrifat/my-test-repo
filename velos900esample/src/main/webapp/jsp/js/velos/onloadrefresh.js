var BrwsNt_SuppAjax=M_BrwsNt_SuppAjax;
var Nt_Fnd404=M_404Nt_Fnd;
function refresh(cjsondata) {
var tElements = cjsondata.fields.field.length;
var ename;
var nelement;
for (var i=0;i<tElements;i++) {
	ename = document.getElementsByName(cjsondata.fields.field[i].name);
	for (var j=0;j<ename.length;j++) {
		ename[j].setAttribute("readOnly","readOnly");
		ename[j].style.backgroundColor = "#ccccff";
		if (ename[j].type == 'select-one') {
			var k;
			for(k=ename[j].options.length-1;k>=0;k--)
			{
				if (!ename[j].options[k].selected) ename[j].remove(k);
			}
		}
	}
}


var tElements = cjsondata.fields.links.length;
var aElements=document.getElementsByTagName("a");
for (var i=0;i<tElements;i++) {
	for (var j=0;j<aElements.length;j++) {
		if (cjsondata.fields.links[i].name == aElements[j].innerHTML) {
			aElements[j].innerHTML="";
			aElements[j].disabled=true;
		}
	}
}

var tElements = cjsondata.fields.images.length;
var aElements=document.getElementsByTagName("img");
for (var i=0;i<tElements;i++) {
	for (var j=0;j<aElements.length;j++) {
		if (cjsondata.fields.images[i].name == aElements[j].id) {
			aElements[j].style.display="none";
		}
	}
}

}


function getHTTPObject(){
if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
else if (window.XMLHttpRequest) return new XMLHttpRequest();
else {
alert(BrwsNt_SuppAjax);/*alert("Your browser does not support AJAX.");*****/
return null;
}
}

function getJsonData(url){
httpObject = getHTTPObject();
if (httpObject != null) {
httpObject.open("GET", url, false);
  httpObject.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
  httpObject.setRequestHeader("Cache-Control", "no-cache");
httpObject.send(null);
  if(httpObject.status != 200){document.write(Nt_Fnd404/*"404 not found"*****/);}
	var finalString = httpObject.responseText;
	if (finalString != "") {
		var jsondata = finalString;
		return jsondata;
		}
	}
}


function refreshStatus(rule) {
	var tElements = rule.statuses.length;
	var ename;
	var found = false;
	var selected = "";
	ename = document.getElementsByName('patstatus');
	for (var j=0;j<ename.length;j++) {
		var k;
		for(k=ename[j].options.length-1;k>=0;k--)
		{
			found = false;
			selected = "";
			for (var i=0;i<tElements;i++) {
				if (ename[j].options[k].value == rule.statuses[i].status) {
					found = true;
					selected = rule.statuses[i].defaultStatus;
				}
			}
			if (found) {
				if (selected == "Y") ename[j].options[k].selected = true;
			} else {
				ename[j].remove(k);
			}
		}
	}
}