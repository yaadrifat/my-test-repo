/**
 * AK:Added file for enhancement PCAL-20071
 */
 function includeJS(file)
{

  var script  = document.createElement('script');
  script.src  = file;
  script.type = 'text/javascript';
  script.defer = true;

  document.getElementsByTagName('head').item(0).appendChild(script);

}

/* include any js files here */

// Introduced for localization
includeJS('js/velos/velosUtil.js');
includeJS('js/yui/build/event-simulate/event-simulate.js');
var CommErr_CnctVelos= M_CommErr_CnctVelos; /*Storing Value from MC.jsp to local var*/
// Define manageVisitsGrid object. "VELOS" is a namespace so that the object can be called globally
var Delete_Row=L_Delete_Row;
var ValEtrFlw_DaysIntValid=M_ValEtrFlw_DaysIntValid;
var ValEtrFlwBef_WinLessValid=M_ValEtrFlwBef_WinLessValid;
var ValEtrFlwAft_WinLessValid=M_ValEtrFlwAft_WinLessValid;
var ValEtrFwg_IntrDgtPos=M_ValEtrFwg_IntrDgtPos;
var ValEtrFwg_VstSglVal=M_ValEtrFwg_VstSglVal;
var ValEtrFwg_VstGt2Val=M_ValEtrFwg_VstGt2Val;
var PlsSvSpec_DepdIntr=M_PlsSvSpec_DepdIntr;
var PlsSv_VstDepend=M_PlsSv_VstDepend;
var CldNtCont_InvldIndx=M_CldNtCont_InvldIndx;
var Vsts_Added=L_Vsts_Added;
var Vst_Updt=L_Vst_Updt;
var Visits_Deleted=L_Visits_Deleted;
var ThereNoChg_Save=M_ThereNoChg_Save;
var Esignature=L_Esignature;
var Valid_Esign=L_Valid_Esign;
var Invalid_Esign=L_Invalid_Esign;
var PlsEnterEsign=M_PlsEnterEsign;
var DayValid_FirstWeek=M_DayValid_FirstWeek;
var Unhide_Visit=L_Unhide_Visit;
var Hide_Visit=L_Hide_Visit;
var CfmVst_Chg=M_CfmVst_Chg;
var PleaseWait_Dots=L_PleaseWait_Dots;
var MngVsts=L_MngVsts;
var LoadingPlsWait=L_LoadingPlsWait;
var SvChldVst_BfrPrtVst=M_SvChldVst_BfrPrtVst;
var AddSetAft_OfflineZeroVst=M_AddSetAft_OfflineZeroVst;
//Ak:Added localization support for PCAL-20801
var CpyngVstAlso_EvtChgPrmt=M_CpyngVstAlso_EvtChgPrmt;
var WantCpyVst_EvtChgPrmt=M_WantCpyVst_EvtChgPrmt;
var CopyVisit=L_CopyVisit;
var Row=L_Row;
var ParentChild_OfflineZeroVst=M_ParentChild_OfflineZeroVst;
var lastTimeSubmitted = 0;
var myDataTable;
var myDialog = null;
var visitSeq=0;
var calStatus=null;
var calStatDesc=null;
var manageVisitGridChangeList = [];
var manageVisitGridListRemainder = [];
var updateRowsRemainder = [];

var pgRight=0;
var duration=0;
var protocolId=0;
var calenderId=0;
var insertAftervisitIds = []; 
var insertAftervisitNames =[];
var updateRows = [];
var addedVisits = [];
var updatedVisits = [];
var deletedVisits = [];
var updateVisitIds = [];
var childVisitIds = [];
var changedChildVisitIds = [];
var parentVisitIds = [];
var childVisitInsertAfter = [];
var fakeVisits = null;
var fakeVisitsNames = null;
var noTimePointdefined =null;
var fixedPointdefined = null;
var depnPointdefined = null;

var intervalTypeIds = []; 
var intervalTypeNames =[];
var offlineStatusDesc = "";
var classAssoc=null;
var calledFrom=null;
var delCount = 0; //PCAL20353 AND PCAL 20354
var fakeCount =0;
var visitNotListed = L_VisitNot_Listed;
var formatterFail = false;
var formatterFailTwo = false;
var formatterFailThree = false;
var selectedCell = null;//Bug#10298 : To store the cell.
var respJ = null; // response in JSON format
var jsonDataToReset = null // Json data to store value
VELOS.manageVisitsGrid = function (url, args) {
	pgRight = parseInt(YAHOO.util.Dom.get("pageRight").value);
	this.url = url;  // save it in this.url; later it will be sent in startRequest()
	incomingUrlParams = args.urlParams; 
	if (incomingUrlParams) {
		incomingUrlParams = incomingUrlParams.replace("'", "\"");
	}
	showPanel(); // Show the wait panel; this will be hidden later

	// Define handleSuccess() of VELOS.manageVisitsGrid object. This will be added to the callback object to be
	// used when the call is processed successfully.
	this.handleSuccess = function(o) {
		  this.dataTable = args.dataTable ? args.dataTable : 'manageVisitsGrid'; // name of the div to hold manageVisitsGrid
		  try {
				respJ = $J.parse(o.responseText);
			} catch(e) {
				alert(CommErr_CnctVelos);/*alert(svrCommErr);*****/
				return;
			}
			if (respJ.error < 0) {
				var paramArray = [respJ.errorMsg];
				alert(getLocalizedMessageString("L_Error_Msg",paramArray));/*alert('Error Msg: '+respJ.errorMsg);*****/
				return;
			}
			//Getting all common JSOn data for all rows
			visitSeq=respJ.visitSeq;
			calStatus=respJ.calStatus;
			duration=respJ.duration;
			protocolId=respJ.protocolId;
			calenderId=respJ.wrongVisitCalender;
			calStatDesc=respJ.calStatDesc;
			insertAftervisitIds=respJ.insertAftVisitIds;
			insertAftervisitNames=respJ.insertAftVisitNames;
			
			intervalTypeIds = respJ.timePointListPk;
			intervalTypeNames = respJ.timePointListDesc;
			offlineStatusDesc = respJ.offlineStatusDesc;
			fakeCount=0;
			var myColumnDefs = [];
			updateRows =[];	
			
			//Flush out all
			manageVisitGridChangeList = [];
			updateRows =[];		
			updatedVisits = [];
			addedVisits = [];
			updateVisitIds = [];
			//PCAL20353 AND PCAL 20354
			deletedVisits = [];
			deleteRows = [];
			childVisitIds = [];						
			changedChildVisitIds = [];						
			parentVisitIds = [];						
			childVisitInsertAfter = [];						
			myColumnDefs = VELOS.manageVisitsGrid.processColumns(respJ,respJ.colArray);
			fakeVisits = [];
			fakeVisits = respJ.wrongVisitArray;
			
			fakeVisitsNames = [];
			fakeVisitsNames = respJ.wrongVisitNameArray;
			
			noTimePointdefined = respJ.noTimePointdefined;
			fixedPointdefined = respJ.fixedPointdefined;
			depnPointdefined = respJ.depnPointdefined;
			classAssoc=$j("#calassoc").val();
			calledFrom=$j("#calledFrom").val();
			var myDataSource = new YAHOO.util.DataSource(respJ.dataArray);		
			myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
			myDataSource.responseSchema = {
				 fields:respJ.fieldsArray
			};
			var maxWidth = 800;
			var maxHeight = 800;

			if (screen.availWidth >= 1000 && screen.availWidth < 1100) { maxWidth = 800; }
			else if (screen.availWidth >= 1100 && screen.availWidth < 1290) { maxWidth = 1000; }

			if (screen.availHeight >= 700 && screen.availHeight < 900) { maxHeight = 320; }
			else if (screen.availHeight >= 900 && screen.availHeight < 1000) { maxHeight = 480; }

			var calcHeight = respJ.dataArray.length*40 + 40;
			if (calcHeight > maxHeight) { calcHeight = maxHeight; }

			calcHeight = 320;
			myDataTable = new YAHOO.widget.DataTable(
					this.dataTable,
				myColumnDefs, myDataSource,
				{
					width:"98%",
					height:calcHeight+"px",
					scrollable:false
				}
			);
			
			//AK:Added for enhancement PCAL-20353 and PCAL-20354
			var tabLength= myDataTable.getRecordSet().getLength();
			var visitCells= $D.getElementsByClassName('yui-dt-col-visitName', 'td');
			for (var iX=0; iX<tabLength; iX++) {
				var el = new YAHOO.util.Element(visitCells[iX]);
				var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
				var parentId = parent.get('id');
				
				var noIntervalIdTd = parent.getElementsByClassName('yui-dt-col-noIntervalId', 'td')[0];
				var noIntervalIdEl = new YAHOO.util.Element(noIntervalIdTd);
				var noIntervalId = noIntervalIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
				VELOS.manageVisitsGrid.wipeNonChildColumns(myDataTable, el, noIntervalId);			

				var visitSeqTd = parent.getElementsByClassName('yui-dt-col-visitSeq', 'td')[0];
				var visitSeqEl = new YAHOO.util.Element(visitSeqTd);
				var tempVisitSeq = visitSeqEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;//Ak:Fixed bug#7169
				
				//Truncate visit name if length is more than 29 characters //Ak:Fixed BUG#7541
				var visitNameTd = parent.getElementsByClassName('yui-dt-col-visitName', 'td')[0];
				var visitNameEl = new YAHOO.util.Element(visitNameTd);
				var tempVisitName = visitNameEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
				var vName = (tempVisitName.length>8)? tempVisitName.substring(0,8)+"...":tempVisitName;
				visitNameEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML=vName;
				
				var insertAfterIdTd = parent.getElementsByClassName('yui-dt-col-insertAfterId', 'td')[0];
				var insertAfterIdEl = new YAHOO.util.Element(insertAfterIdTd);
				var insertAfterId = insertAfterIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
				
				//Truncate visit name if length is more than 29 characters //Ak:Fixed BUG#7541
				var insertAfterTd = parent.getElementsByClassName('yui-dt-col-insertAfter', 'td')[0];
				var insertAfterEl = new YAHOO.util.Element(insertAfterTd);
				var insertAfterName = insertAfterEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
				var vAfterName=insertAfterName;
				if(insertAfterId!="-1/-1"){
					vAfterName = (insertAfterName.length>8)? insertAfterName.substring(0,8)+"...":insertAfterName;
				}
				insertAfterEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML=vAfterName;
				
				
				var descriptionTd = parent.getElementsByClassName('yui-dt-col-description', 'td')[0];
				var descriptionEl = new YAHOO.util.Element(descriptionTd);
				var tempDescription = descriptionEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
				var vDescription = (tempDescription.length>8)? tempDescription.substring(0,8)+"...":tempDescription;
				descriptionEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML=vDescription;
				
				var visitIdTd = parent.getElementsByClassName('yui-dt-col-visitId', 'td')[0];
				var visitIdEl = new YAHOO.util.Element(visitIdTd);
				var tempVisitId = visitIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
				for(var fK=0;fK<fakeVisitsNames.length;fK++)
				{
				  if(fakeVisits[fK]==tempVisitId && calenderId==protocolId)
					{
						var insertAfterTd = parent.getElementsByClassName('yui-dt-col-insertAfter', 'td')[0];
						var insertAfterEl = new YAHOO.util.Element(insertAfterTd);
						var tempInsertAfter = insertAfterEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
						if(visitNotListed!=tempInsertAfter){
						tempInsertAfter = (tempInsertAfter.length>8)? tempInsertAfter.substring(0,8)+"...":tempInsertAfter;}
						tempInsertAfter="<FONT class='mandatory'>"+tempInsertAfter+"</FONT>";
						insertAfterEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML=tempInsertAfter;
						fakeCount++;						
					
					}else if(fakeVisitsNames[fK]==tempVisitName && calenderId==protocolId){//YK:Fixed Bug#10534//Added for Bug#9089:By Yogendra Pratap Singh
						var insertAfterTd = parent.getElementsByClassName('yui-dt-col-insertAfter', 'td')[0];
						var insertAfterEl = new YAHOO.util.Element(insertAfterTd);
						var tempInsertAfter = insertAfterEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
						if(visitNotListed!=tempInsertAfter){
						tempInsertAfter = (tempInsertAfter.length>8)? tempInsertAfter.substring(0,8)+"...":tempInsertAfter;}
						tempInsertAfter="<FONT class='mandatory'>"+tempInsertAfter+"</FONT>";
						insertAfterEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML=tempInsertAfter;
						fakeCount++;
					}
				}
				var intervalTd = parent.getElementsByClassName('yui-dt-col-interval', 'td')[0];
				jQuery(intervalTd).addClass("yui-dt-highlighted");
				
				var delButtonTd = parent.getElementsByClassName('yui-dt-col-delete', 'td')[0];
				var delButtonEl = new YAHOO.util.Element(delButtonTd);
				var delButton = delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
				var displayStyle ="";
				if (!f_check_perm_noAlert(pgRight,'E')){
					displayStyle='style="display:none" ';
				}
				delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML
				= '<div align="center"><img src="../images/delete.png" title="'+Delete_Row/*Delete Row*****/+'" border="0"'
				+ ' name="delete_e'+tempVisitSeq+'" id="delete_e'+tempVisitSeq+'" '+displayStyle+'/></div> ';

				var delButtonTd = parent.getElementsByClassName('yui-dt-col-copyVisit', 'td')[0]; //AK:Added for enhancement PCAL-20801
				var delButtonEl = new YAHOO.util.Element(delButtonTd);
				var delButton = delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
				delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML
				= '<div align="center"><img src="../images/copy.png" title="'+CopyVisit+'" border="0"'
				+ ' name="copy_e'+tempVisitSeq+'" id="copy_e'+tempVisitSeq+'"/> </div>';
			}
			if(fakeCount>0){
				if('S'==calledFrom){
					
					if('P'==classAssoc){
					$j("#fakeVisitsStudy").show();	
					}
					if('S'==classAssoc){
						$j("#fakeVisitsAdmin").show();	
					}
					
					
				}else if('P'==calledFrom || 'L'==calledFrom){
					$j("#fakeVisitsLib").show();	
				}
				}else{
					if('S'==calledFrom){
						
						if('P'==classAssoc){
						$j("#fakeVisitsStudy").hide();	
						}
						if('S'==classAssoc){
							$j("#fakeVisitsAdmin").hide();	
						}
						
						
					}else if('P'==calledFrom || 'L'==calledFrom){
						$j("#fakeVisitsLib").hide();	
					}
				}
		//To make hide/unhide column hide and unhide
		  if(calStatus=='O'){
			   var tabLength= myDataTable.getRecordSet().getLength();
				for (var iX=0; iX<tabLength; iX++) {
					var visitCells= $D.getElementsByClassName('yui-dt-col-visitName', 'td');
					var el = new YAHOO.util.Element(visitCells[iX]);
					var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
					var parentId = parent.get('id');
					
					var visitSeqTd = parent.getElementsByClassName('yui-dt-col-visitSeq', 'td')[0];
					var visitSeqEl = new YAHOO.util.Element(visitSeqTd);
					var tempVisitSeq = visitSeqEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML; //Ak:Fixed bug#7169
					
					var hideFlagTd = parent.getElementsByClassName('yui-dt-col-hideFlag', 'td')[0];
					var hideFlagEl = new YAHOO.util.Element(hideFlagTd);
					var hideFlag = hideFlagEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
					//PCAL20353 AND PCAL 20354
					var displayStyle ="";
					if (!f_check_perm_noAlert(pgRight,'E')){
						displayStyle='style="display:none" ';
					}
					if(hideFlag!=0 && hideFlag==1){
					hideFlagEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML
					= '<div align="center"><img src="../images/unhide.png" title="'+Unhide_Visit/*Unhide Visit*****/+'" border="0"'         //Akshi:Fixed bug#7710
					+ ' name="hideFlag_e'+tempVisitSeq+'" id="hideFlag_e'+tempVisitSeq+'" '+displayStyle+'/> </div>';
					}else{
					hideFlagEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML
						= '<div align="center"><img src="../images/hide.png" title="'+Hide_Visit/*Hide Visit*****/+'" border="0"'           //Akshi:Fixed bug#7710
						+ ' name="hideFlag_e'+tempVisitSeq+'" id="hideFlag_e'+tempVisitSeq+'" '+displayStyle+'/> </div>';
						
					}
				}
				
			}
			   myDataTable.subscribe("editorBlurEvent",function(oArgs) { 
				   myDataTable.onEventSaveCellEditor;
					var ev = oArgs.event;
				   if(formatterFail){//Bug#10298 : To focus the cell on clicking other cell
					   formatterFailTwo=false;
						formatterFailThree=false;
						myDataTable.onEventShowCellEditor({target:selectedCell}); 
					}   
			   }); 
				// I subscribe to the cellClickEvent to respond to the action icons and, if none, to pop up the cell editor
			   myDataTable.subscribe('cellClickEvent',function(oArgs) {
						var target = oArgs.target;
						var column = this.getColumn(target);
						var oRecord = this.getRecord(target);
						var tempvisitSeq =oRecord.getData('visitSeq');
						var offlineFlag =oRecord.getData('offlineFlag');
						var visitNameSel =oRecord.getData('visitName');
						var visitId = parseInt(oRecord.getData('visitId'));
						if (column.key=="afterText") return false;
						
						var ev = oArgs.event;
						   if(formatterFail){//Bug#10298 : To focus the cell on clicking other cell
							   formatterFailTwo=false;
								formatterFailThree=false;
								myDataTable.onEventShowCellEditor({target:selectedCell}); 
								return false;
							} 
						   
						//AK:Added for enhancement PCAL-20353 and PCAL-20354
						if(calStatus=='O'){
								if(offlineFlag!=null && offlineFlag>0){
									if(column.key=='visitName'|| column.key=='months'||column.key=='weeks'||column.key=='days'||column.key=='intervalUnit'||column.key=='insertAfter'||column.key=='insertAfterInterval'
										|| column.key=='noInterval'//!important
									){
											f_check_perm(pgRight,'E');
											return false;
									}
							    }
					    }

						//Enabling/disabling time-point child columns
						var noIntervalId = oRecord.getData('noIntervalId');
						var returnVal = VELOS.manageVisitsGrid.enableDisableChildColumns(column, noIntervalId);
						if (returnVal < 1){f_check_perm(pgRight,'E'); return false;}

						switch(column.formatter){
						//Fixed bug 6958.
						case 'customInteger':
							column.formatter = function(el, oRecord, oColumn, oData) {
								formatterFail = false;
								if (oData != '0'){
									if (oData != '' || oData != null){
										if (/^[0]+/g.test(oData)){
											oData = oData.replace(/^[0]+/g,""); //remove leading zeros
										}
									}
								}if (oData == '0'){							
									el.innerHTML = oData;
									oRecord.setData.oColumn= el.innerHTML;
									if (myDataTable && myDataTable._oCellEditor){
										myDataTable._oCellEditor.value = oData;
									}
								}
								else{
									switch(oColumn.key){
									case 'days':
										if (/^-{0,5}\d{0,5}$/.test(oData)){
											formatterFailThree=false;
											el.innerHTML = oData;
											oRecord.setData(oColumn.key, oData);
											if (myDataTable && myDataTable._oCellEditor){
												myDataTable._oCellEditor.value = oData;
											}
										}else{
											formatterFail = true;
											oRecord.setData(oColumn.key, '');
											if (myDataTable && myDataTable._oCellEditor){
												myDataTable._oCellEditor.value = null;
											}
											alert(ValEtrFlw_DaysIntValid);/*alert("The value entered does not pass the following criteria:\n \n -'Days' should be an integer.\n -'Days' should be less than 6 digits.\n \nPlease enter a valid value.");*****/
										}
										break;
										// Fixed #6958,BK,AUG-31-2011
									case 'beforenum':
										if (/^\d{0,3}$/.test(oData)){
											formatterFailThree=false;
											el.innerHTML = oData;
											oRecord.setData(oColumn.key, oData);
											if (myDataTable && myDataTable._oCellEditor){
												myDataTable._oCellEditor.value = oData;
											}
										}else{
											formatterFail = true;
											oRecord.setData(oColumn.key, '');
											if (myDataTable && myDataTable._oCellEditor){
												myDataTable._oCellEditor.value = null;
											}
											alert(ValEtrFlwBef_WinLessValid);/*alert("The value entered does not pass the following criteria:\n \n -'Visit Window before interval' should be an integer.\n -'Visit Window before interval' should be less than 4 digits.\n -'Visit Window before interval' should be a positive number.\n \nPlease enter a valid value.");*****/
										}
										break;
										
									case 'afternum':
										if (/^\d{0,3}$/.test(oData)){
											formatterFailThree=false;
											el.innerHTML = oData;
											oRecord.setData(oColumn.key, oData);
											if (myDataTable && myDataTable._oCellEditor){
												myDataTable._oCellEditor.value = oData;
											}
										}else{
											formatterFail = true;
											oRecord.setData(oColumn.key, '');
											if (myDataTable && myDataTable._oCellEditor){
												myDataTable._oCellEditor.value = null;
											}
											alert(ValEtrFlwAft_WinLessValid);/*alert("The value entered does not pass the following criteria:\n \n -'Visit Window After interval' should be an integer.\n -'Visit Window After interval' should be less than 4 digits.\n -'Visit Window After interval' should be a positive number.\n \nPlease enter a valid value.");*****/
										}
										break;
									case 'insertAfterInterval':
										if (/^\d{0,5}$/.test(oData)){
											formatterFailThree=false;
											el.innerHTML = oData;
											oRecord.setData(oColumn.key, oData);
											if (myDataTable && myDataTable._oCellEditor){
												myDataTable._oCellEditor.value = oData;
											}
										}else{
											formatterFail = true;
											oRecord.setData(oColumn.key, '');
											if (myDataTable && myDataTable._oCellEditor){
												myDataTable._oCellEditor.value = null;
											}
											alert(ValEtrFwg_IntrDgtPos);/*alert("The value entered does not pass the following criteria:\n \n -'Insert after interval' should be an integer.\n -'Insert after interval' should be less than 6 digits.\n -'Insert after interval' should be a positive number.\n \nPlease enter a valid value.");*****/
										}
										break;
									default:
										if (/^\d{0,5}$/.test(oData)){
											formatterFailThree=false;
											el.innerHTML = oData;
											oRecord.setData(oColumn.key, oData);
											if (myDataTable && myDataTable._oCellEditor){
												myDataTable._oCellEditor.value = oData;
											}
										}else{
											formatterFail = true;
											oRecord.setData(oColumn.key, '');
											if (myDataTable && myDataTable._oCellEditor){
												myDataTable._oCellEditor.value = null;
											}
											var paramArray = [oColumn.label,oColumn.label,oColumn.label];
											alert(getLocalizedMessageString("M_ValEtrFwg_IntPosVal",paramArray));/*alert("The value entered does not pass the following criteria:\n \n -'"+oColumn.label+"' should be an integer.\n -'"+oColumn.label+"' should be less than 6 digits.\n -'"+oColumn.label+"' should be positive number.\n \nPlease enter a valid value.");*****/
										}
										break;
									}
										
							    }
							
							};
							break;
						case 'customVarchar':
							column.formatter= function(el, oRecord, oColumn, oData) {//Ak:Fixed bug#6999
								formatterFail = false;
								
								if (!oData)
									oData = '';

								if (/['\\"|]+/g.test(oData)){ //AK:Fixed Bug#6999
									formatterFail = true;
									if (myDialog)
										myDialog.hide();
									oRecord.setData(oColumn.key, '');
									if (myDataTable && myDataTable._oCellEditor){
										myDataTable._oCellEditor.value = null;
									}
									if(column.key=='visitName')
									    alert(ValEtrFwg_VstSglVal);/*alert("The value entered does not pass the following criterion:\n \n - 'Visit Name' should not contain following special characters: pipe(|), single quote('),backslash(\\), double quote(\").\n \nPlease enter a valid value.");*****/
									else{
										var paramArray = [oColumn.label];
										alert(getLocalizedMessageString("M_ValEtrFwg_SplCharVal",paramArray));/*alert("The value entered does not pass the following criterion:\n \n - '"+oColumn.label+"' should not contain following special characters: pipe(|), single quote('),backslash(\\), double quote(\").\n \nPlease enter a valid value.");*****/
									}
									return;
								}
								
								if (oData.length < 51 && column.key=='visitName' ){ //AK:Fixed BUG#7530
									formatterFailThree=false;
									el.innerHTML = oData;
									oRecord.setData(oColumn.key, VELOS.manageVisitsGrid.decodeData(oData));
								}else if (oData.length < 201 && column.key!='visitName' ){  //AK:Fixed BUG#7586
									formatterFailThree=false;
									el.innerHTML = oData;
									oRecord.setData(oColumn.key, VELOS.manageVisitsGrid.decodeData(oData));								
								}else{
									formatterFail = true;
									if (myDialog)
										myDialog.hide();
									oRecord.setData(oColumn.key, '');
									if (myDataTable && myDataTable._oCellEditor){
											myDataTable._oCellEditor.value = null;
									}
									if(column.key=='visitName')
									    alert(ValEtrFwg_VstGt2Val);/*alert("The value entered does not pass the following criterion:\n \n -'Visit Name' length should not be greater than 250 characters.\n \nPlease enter a valid value.");*****/
									else{
										var paramArray = [oColumn.label];
										alert(getLocalizedMessageString("M_ValEtrFwg_LenGtVal",paramArray));/*alert("The value entered does not pass the following criterion:\n \n -'"+oColumn.label+"'length should not be greater than 200 characters.\n \nPlease enter a valid value.");*****/
									}
								}
							}; 
							break;
						default:
							//SM: bug #9329-Removed the duplicate code from here.
							//Control will flow to next switch and use the default action of next switch if necessary.
							break;
						//End of Switch
						}
						switch(column.action){
							case 'delete':
								if ((visitId > 0 && false == f_check_perm_noAlert(pgRight,'E')) || (visitId == 0 && false == f_check_perm_noAlert(pgRight,'N'))){//BUG#7543
									f_check_perm(pgRight,'E');
									return false;
								} else{
									//Can not allow to delete visit with the calender status of freeze,Active,Deactivated
									if ( (calStatus == 'F') || (calStatus == 'A')|| (calStatus == 'D')) {
										var paramArray = [calStatDesc];
										alert(getLocalizedMessageString("M_CntDel_CalStat",paramArray));/*alert("Cannot Delete Visit in calendar with status "+calStatDesc);*****/
										return false;
									}
									//Do not allow to delete the visit which are added before the 'Offline status'of calendar
									var offlineFlag=oRecord.getData('offlineFlag');
									
									if(offlineFlag!=null && offlineFlag!='' && offlineFlag>0){           //Akshi:Fixed bug#7843  
										var paramArray = [calStatDesc];
										alert(getLocalizedMessageString("M_CntDel_CalStat",paramArray));/*alert("Cannot Delete Visit in calendar with status "+calStatDesc);*****/
										return false;
									}
									if(VELOS.manageVisitsGrid.validateVisitForDeletion(myDataTable,visitId,visitNameSel)){
										//Fixed #7323,BK,Oct/17/2011
										var confirmMessage;
										if(visitId != 0){
										var paramArray = [oRecord.getData('visitName')];
											confirmMessage = getLocalizedMessageString("M_YouWtDel_PermSv",paramArray);/*"Are you sure you want to delete Visit '"+oRecord.getData('visitName')+"'? \n \nTo delete this Item permanently, please click on 'Preview and Save'.";*****/
										}
										else{
										var paramArray = [oRecord.getData('visitName')];										
										confirmMessage = getLocalizedMessageString("M_SureWant_DelVst",paramArray)/*"Are you sure you want to delete Visit '"+oRecord.getData('visitName')+"'"*****/;
										}
										if (confirm(confirmMessage)) {
											 var change=manageVisitGridChangeList['visitSeq'+tempvisitSeq]; //Ak:Fixed BUG#7591 
											 if(change==undefined){//AK:Fixed Bug#7195
												 manageVisitGridChangeList['visitSeq'+tempvisitSeq] = tempvisitSeq;
												 if (deleteRows == null) deleteRows = [];
												 deleteRows['visitSeq'+tempvisitSeq] = visitId;
												 deletedVisits['visitSeq'+tempvisitSeq] = oRecord.getData('visitName');
											 }else{ //AK:Fixed Bug#7195
												 if(visitId != 0){
													 //alert("enter into the if else"+visitId);
													 updateRows['visitSeq'+tempvisitSeq] = undefined; 
													 updatedVisits['visitSeq'+tempvisitSeq] = undefined;
													 updateVisitIds['visitSeq'+tempvisitSeq] = undefined;
													 if (deleteRows == null) deleteRows = [];
													 deleteRows['visitSeq'+tempvisitSeq] = visitId;
													 deletedVisits['visitSeq'+tempvisitSeq] = oRecord.getData('visitName');
												 }else{
													 addedVisits['visitSeq'+tempvisitSeq]=undefined;
													 manageVisitGridChangeList['visitSeq'+tempvisitSeq] = undefined;
												 } 
												 
											 }
											myDataTable.deleteRow(target);
											oRecord.setData(column.key, true);
											delCount ++;
											
									    }
									}
									
								}
								return false;
								break;
							case 'hide':
								if ((visitId > 0 && false == f_check_perm_noAlert(pgRight,'E')) || (visitId == 0 && false == f_check_perm_noAlert(pgRight,'N'))){//BUG#7543
									f_check_perm(pgRight,'E');
									return false;
								} else{
									 var hideFlagId="hideFlag_e"+tempvisitSeq;
									 var src=$(hideFlagId).src;
									 
									 if((src.search(/unhide.png/))>-1){                 //Akshi:Fixed bug#7710
										 $(hideFlagId).src="../images/hide.png";        //Akshi:Fixed bug#7710
										 $(hideFlagId).title=L_Hide_Visit;//Ak:Fixed Bug#7196
										 oRecord.setData(column.key, 0);
										 
									 }else if((src.search(/hide.png/))>-1){             //Akshi:Fixed bug#7710
										 $(hideFlagId).src="../images/unhide.png";      //Akshi:Fixed bug#7710
										 $(hideFlagId).title=L_Unhide_Visit; //Ak:Fixed Bug#7196
										 oRecord.setData(column.key, 1);
									 }
									 manageVisitGridChangeList['visitSeq'+tempvisitSeq] = tempvisitSeq;
									 updateRows['visitSeq'+tempvisitSeq] = tempvisitSeq;
								}
								return false;
								break;
							case 'copy':
								//for 'copy' functionality will check only new access right
								//Ak:Added for Enhancement PCAL-20081		
								if (f_check_perm(pgRight,'N')){									
		                         	var visitName=oRecord.getData('visitName');		
									var confirmCopyMessage;
									if(visitName!=null && visitName!=''){
										var paramArray = [visitName,visitName]; //Ak:Added localization support for PCAL-20801
										confirmCopyMessage = getLocalizedMessageString("CpyngVstAlso_EvtChgPrmt",paramArray);
									}
									else{
										confirmCopyMessage = getLocalizedMessageString("WantCpyVst_EvtChgPrmt");
									}
									if (confirm(confirmCopyMessage)) {													
										VELOS.manageVisitsGrid.copyVisit(myDataTable,oRecord);
									 }									
								}
								return false;
								break;			
							default:
								if (visitId == 0){ //New Record
									if (f_check_perm(pgRight,'N')){
										if (column.key == 'insertAfter'){
											VELOS.manageVisitsGrid.updateDependent(column,oRecord,'visitId','v',respJ);
										}
										if (column.key =='intervalUnit'){
											column.editor.dropdownOptions=respJ.intervalUnitList;
											column.editor.render();
										}
										// If no action is given, I try to edit it
										this.onEventShowCellEditor(oArgs);//Ak:Fixed Bug#6905
									}
								}else{//Existing Record 
									if (f_check_perm(pgRight,'E')){
										if (column.key == 'insertAfter'){
											VELOS.manageVisitsGrid.updateDependent(column,oRecord,'visitId','v',respJ);
										}
										if (column.key =='intervalUnit'){
											VELOS.manageVisitsGrid.updateDropDown(column,oRecord,'visitId','i',respJ);
										}
										// If no action is given, I try to edit it
										this.onEventShowCellEditor(oArgs);
									}
								}
								return true;
								break;
						}
					});
			   
			   myDataTable.subscribe("editorKeydownEvent",function(oArgs) {
					var ed = this._oCellEditor;  // Should be: oArgs.editor, see: http://yuilibrary.com/projects/yui2/ticket/2513909
					var ev = oArgs.event;
					var KEY = YAHOO.util.KeyListener.KEY;
					var Textbox = YAHOO.widget.TextboxCellEditor;
					var Textarea = YAHOO.widget.TextareaCellEditor;
					var cell = ed.getTdEl();
					var oColumn = ed.getColumn();
					var row,rec;
					
					var editNext = function(cell) {
						cell = myDataTable.getNextTdEl(cell);
						while (cell && !myDataTable.getColumn(cell).editor) {
							cell = myDataTable.getNextTdEl(cell);
						}
						if (cell) {
							//Enabling/disabling time-point child columns
							var oRecord = myDataTable.getRecord(cell);
							var noIntervalId = oRecord.getData('noIntervalId');
							var returnVal = VELOS.manageVisitsGrid.enableDisableChildColumns(myDataTable.getColumn(cell), noIntervalId);
							if (returnVal < 1) {
								editNext(cell);
							}else{
								if(formatterFail) return;//SM:similar fix #5835
								YAHOO.util.UserAction.click(cell);
								//this.showCellEditor(cell);
							}
						}
					};
					var editPrevious = function(cell) {
						cell = myDataTable.getPreviousTdEl(cell);
						while (cell && !myDataTable.getColumn(cell).editor) {
							cell = myDataTable.getPreviousTdEl(cell);
						}
						if (cell) {
							//Enabling/disabling time-point child columns
							var oRecord = myDataTable.getRecord(cell);
							var noIntervalId = oRecord.getData('noIntervalId');
							var returnVal = VELOS.manageVisitsGrid.enableDisableChildColumns(myDataTable.getColumn(cell), noIntervalId);
							if (returnVal < 1) {
								editPrevious(cell);
							}else{
								if(formatterFail) return;//SM:similar fix #5835
								YAHOO.util.UserAction.click(cell);
								//this.showCellEditor(cell);
							}
						}
					};
						
					switch (ev.keyCode) {
						case KEY.TAB:
							YAHOO.util.Event.stopEvent(ev);
							ed.save();
							if (ev.shiftKey) {
								editPrevious(cell);
							} else {
								editNext(cell);
							}
							break;
					}
			   });

			   myDataTable.subscribe('editorSaveEvent',function(oArgs) {			
					var elCell = oArgs.editor.getTdEl();
					var oOldData = oArgs.oldData;
					var oNewData = oArgs.newData;			
					var ev = oArgs.event;
					var oRecord = this.getRecord(elCell);			
					var column =  this.getColumn(elCell);
					var visitSeq = oRecord.getData('visitSeq');
					var visitIds = oRecord.getData('visitId');
					var regularExp =  new RegExp('visitSeq[0-9]+$');
					var insertAftervisitId;
					var len;
					var noIntervalId;
					if(formatterFail){
						oRecord.setData(column.key, oArgs.oldData);
						this.onEventShowCellEditor({target:elCell}); 
						selectedCell=elCell;
						if(ev.keyCode!=KEY.ENTER) //Bug#10298 : To stop double enter to save values
						{formatterFailTwo=true;}
						formatterFailThree=false;
						return false;
					}
					if(formatterFailTwo){
						oRecord.setData(column.key, oArgs.oldData);
						this.onEventShowCellEditor({target:elCell}); 
						selectedCell=elCell;
						formatterFailTwo=false;
						formatterFailThree=true;
						return false;
					}
					if(formatterFailThree){
						oRecord.setData(column.key, oArgs.oldData);
						this.onEventShowCellEditor({target:elCell}); 
						selectedCell=elCell;
						formatterFailThree=false;
						return false;
					}
					//#6994	Oct/17/2011,BK
					if(column.key == 'months' || column.key =='weeks' || column.key == 'days' ||
							column.key == 'intervalUnit' || column.key =='insertAfterInterval' || column.key == 'insertAfter'|| column.key == 'noInterval'){
						if (column.key == 'noInterval'){
							VELOS.manageVisitsGrid.setDropDownId(this,elCell,intervalTypeIds,intervalTypeNames);
							
							noIntervalId = oRecord.getData('noIntervalId');
							//Wipe data from time-point child columns for which data is no more valid.
							VELOS.manageVisitsGrid.wipeNonChildColumns(this, elCell, noIntervalId);
						}
						noIntervalId = oRecord.getData('noIntervalId');
						if (oOldData != oNewData){
							if (noIntervalId == fixedPointdefined){
								if(column.key == 'months' || column.key =='weeks' || column.key == 'days'){
									VELOS.manageVisitsGrid.changeDependentOfParent(this, oRecord);
								}
							}
							if (noIntervalId == depnPointdefined){
								if(column.key == 'intervalUnit' || column.key =='insertAfterInterval'){
									VELOS.manageVisitsGrid.changeDependentOfParent(this, oRecord);
								}
							}
						}

						VELOS.manageVisitsGrid.getDependentOnMe(this,elCell);
						var insertAfter = oRecord.getData("insertAfter");
						var insertAfterIds = oRecord.getData("insertAfterId");
						insertAfterIds=insertAfterIds==undefined?'':insertAfterIds;
						insertAftervisitId = insertAfterIds.substring(0,insertAfterIds.indexOf("/"));
						if(column.key == 'intervalUnit' || column.key =='insertAfterInterval' || column.key == 'insertAfter'){
							if (column.key == 'insertAfter' && oOldData != oNewData){
								VELOS.manageVisitsGrid.setDropDownId(this,elCell,insertAftervisitIds,insertAftervisitNames);
							}
							if (column.key == 'insertAfter' && oOldData == oNewData){
								var realFakeVisit = oRecord.getData("isRealFakeVisit");
								if(realFakeVisit=="true"){
									var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
									var insertAfterTd = parent.getElementsByClassName('yui-dt-col-insertAfter', 'td')[0];
									var insertAfterEl = new YAHOO.util.Element(insertAfterTd);
									var varOldData="<FONT class='mandatory'>"+oOldData+"</FONT>";
									insertAfterEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = varOldData;
									return false;
								}
							}
							for (oKey in updateVisitIds ) {	
								if(oKey.match(regularExp) && updateVisitIds[oKey] != undefined){
									if ((updateVisitIds[oKey] == insertAftervisitId && (oOldData != oNewData)) || (updatedVisits[oKey] == insertAfter && (oOldData != oNewData))){
										var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
										var Td = parent.getElementsByClassName('yui-dt-col-'+column.key, 'td')[0];
										var El = new YAHOO.util.Element(Td);
										alert(PlsSvSpec_DepdIntr);//alert("Please save all visits before specifying depending intervals.");*****
										El.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = oArgs.oldData;
										this.updateCell(oRecord, column.key, oArgs.oldData, false);
										oRecord.setData(column.key, oArgs.oldData);	
										this.updateCell(oRecord, 'insertAfterId',childVisitInsertAfter[visitIds], false);
										oRecord.setData('insertAfterId',childVisitInsertAfter[visitIds]);
										if (column.key == 'insertAfter'){
											VELOS.manageVisitsGrid.setDropDownId(this,elCell,insertAftervisitIds,insertAftervisitNames);
										}
										var isFake = oRecord.getData("isRealFakeVisit");
										    if(isFake=="true"){
										    	if(column.key=="insertAfter"){
										    		var oldssDatas= (oArgs.oldData.length>8)? oArgs.oldData.substring(0,8)+"...":oArgs.oldData;
										    		oldssDatas="<FONT class=\"Mandatory\">"+oldssDatas+"</FONT>";
										    		if(oArgs.oldData==visitNotListed){oldssDatas="<FONT class=\"Mandatory\">"+oArgs.oldData+"</FONT>";}
													this.updateCell(oRecord, column.key, oldssDatas, false);
													oRecord.setData(column.key, oArgs.oldData);
													this.updateCell(oRecord, 'insertAfterId', "-1/-1", false);
													oRecord.setData("insertAfterId","-1/-1");
										    	}
										    }
										    VELOS.manageVisitsGrid.changeDependentOfParent(this,oRecord);
										return false;
									}
								}
							}
							len = childVisitIds.length;
							if(childVisitIds.indexOf(oRecord.getData('visitId'))< 0  && oArgs.oldData != oArgs.newData){
							childVisitIds[len] = insertAftervisitId;
							}
						}	
						//#6994	Oct/20/7056,BK
							if (visitIds != "0" && noIntervalId!=noTimePointdefined){
								VELOS.manageVisitsGrid.checkChildChanged(this, oRecord,oRecord.getData('visitId'),oRecord.getData('insertAfterId'));
							}
							for (oKey in updateVisitIds ) {
								if(oKey.match(regularExp) && updateVisitIds[oKey] != undefined){
									var childVisit = updateVisitIds[oKey];
									//YK:Fixed Bug#10689
									if(parentVisitIds.indexOf(childVisit)>=0 && (changedChildVisitIds.indexOf(childVisit)>=0))
									{
										continue;
									}
									if ((parentVisitIds.indexOf(oRecord.getData('visitId'))>=0) && ((changedChildVisitIds.indexOf(childVisit)>=0))){		
										alert(SvChldVst_BfrPrtVst);//alert("Please save all child visits before specifying intervals for parent visit.");****
										this.updateCell(oRecord, column.key, oArgs.oldData, false);
										oRecord.setData(column.key, oArgs.oldData);
										var isFake = oRecord.getData("isRealFakeVisit");
										if(column.key == 'noInterval'){
											VELOS.manageVisitsGrid.wipeNonChildColumns(this, elCell, jsonDataToReset.noInterValId);
											
											if(jsonDataToReset.noInterValId==depnPointdefined){
											//noIntervalId
											this.updateCell(oRecord, 'noIntervalId', jsonDataToReset.noInterValId, false);
											oRecord.setData('noIntervalId', jsonDataToReset.noInterValId);
											//insertAfterInterval
											this.updateCell(oRecord, 'insertAfterInterval', jsonDataToReset.insertAfterInterval, false);
											oRecord.setData('insertAfterInterval', jsonDataToReset.insertAfterInterval);
											//intervalUnit
											this.updateCell(oRecord, 'intervalUnit', jsonDataToReset.intervalUnit, false);
											oRecord.setData('intervalUnit', jsonDataToReset.intervalUnit);
											//insertAfter
											var insertAfterValues= jsonDataToReset.insertAfter;
											if(insertAfterValues!=visitNotListed){
												insertAfterValues= (insertAfterValues.length>8)? insertAfterValues.substring(0,8)+"...":insertAfterValues;
											}
											if(jsonDataToReset.insertAfterId=="-1/-1"){
												insertAfterValues="<FONT class=\"Mandatory\">"+insertAfterValues+"</FONT>";
											}
											this.updateCell(oRecord, 'insertAfter',insertAfterValues, false);
											oRecord.setData('insertAfter', jsonDataToReset.insertAfter);
											//insertAfterId
											this.updateCell(oRecord,'insertAfterId', jsonDataToReset.insertAfterId, false);
											oRecord.setData('insertAfterId', jsonDataToReset.insertAfterId);
											}
											if(jsonDataToReset.noInterValId==fixedPointdefined){
												//noIntervalId
												this.updateCell(oRecord, 'noIntervalId', jsonDataToReset.noInterValId, false);
												oRecord.setData('noIntervalId', jsonDataToReset.noInterValId);
												//insertAfterInterval
												this.updateCell(oRecord, 'months', jsonDataToReset.months, false);
												oRecord.setData('months', jsonDataToReset.months);
												//intervalUnit
												this.updateCell(oRecord, 'weeks', jsonDataToReset.weeks, false);
												oRecord.setData('weeks', jsonDataToReset.weeks);
												//insertAfter
												this.updateCell(oRecord, 'days', jsonDataToReset.days, false);
												oRecord.setData('days', jsonDataToReset.days);
											}
											
										}
										if(column.key=="insertAfter"){
									    	this.updateCell(oRecord,'insertAfterId', jsonDataToReset.insertAfterId, false);
											oRecord.setData('insertAfterId', jsonDataToReset.insertAfterId);
									    }
									    if(isFake=="true"){
									    	if(column.key=="insertAfter"){
									    		var oldssDatas= (oArgs.oldData.length>8)? oArgs.oldData.substring(0,8)+"...":oArgs.oldData;
									    		oldssDatas="<FONT class=\"Mandatory\">"+oldssDatas+"</FONT>";
									    		if(oArgs.oldData==visitNotListed){oldssDatas="<FONT class=\"Mandatory\">"+oArgs.oldData+"</FONT>";}
												this.updateCell(oRecord, column.key, oldssDatas, false);
												oRecord.setData(column.key, oArgs.oldData);
												this.updateCell(oRecord, 'insertAfterId', "-1/-1", false);
												oRecord.setData("insertAfterId","-1/-1");
									    	}
									    }
										VELOS.manageVisitsGrid.changeDependentOfParent(this,oRecord);
										if(column.key=="noInterval" ||column.key=="insertAfter" || column.key=="months" || column.key=="weeks" || column.key=="days" || column.key=="insertAfterInterval" || column.key=="intervalUnit" ){
											if(jsonDataToReset!=null && jsonDataToReset.noInterValId!=noTimePointdefined || noIntervalId!=noTimePointdefined){
												updateRows['visitSeq'+visitSeq] = undefined; 
												updatedVisits['visitSeq'+visitSeq] = undefined;
												updateVisitIds['visitSeq'+visitSeq] = undefined;
											}
										}
										return false; 
									}
								}
							}
					}
					else{
						var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
						var Td = parent.getElementsByClassName('yui-dt-col-'+column.key, 'td')[0];
						var El = new YAHOO.util.Element(Td);
						var oData = oNewData;
						//oData is null if formatter encounters error
						if (oData != null){
							//BK,02/09/2011,Bug 6959
							if(column.key == 'description' || column.key == 'visitName'){
								oNewData = oData.replace(/^\s+|\s+$/g, '') ;
							}
							oData = VELOS.manageVisitsGrid.encodeData(oData);
							if(column.key == 'visitName' || column.key == 'description'){	//Ak:Fixed BUG#7541
							  oData = (oData.length>8)? oData.substring(0,8)+"...":oData;
							}
							El.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = oData;
							oRecord.setData(column.key, oNewData);
							if(column.key == 'visitName'){
								VELOS.manageVisitsGrid.changeDependentNameOfParent(this, oRecord,oOldData,oNewData);
							}
							if(column.key == 'visitName'){
								var visitNames = oRecord.getData('visitName');
								VELOS.manageVisitsGrid.getDependentOnMe(this,elCell);
								noIntervalId = oRecord.getData('noIntervalId');
								if (visitIds != "0" && noIntervalId!=noTimePointdefined){
									VELOS.manageVisitsGrid.checkChildChanged(this, oRecord,oRecord.getData('visitId'),oRecord.getData('insertAfterId'));
								}
								var insertAfter = oRecord.getData("insertAfter");
								if(insertAfter.indexOf("</font>")>0){
									insertAfter=insertAfter.substring((insertAfter.indexOf(">")+1),insertAfter.indexOf("</font>"));
								}
								var insertAfterIds = oRecord.getData("insertAfterId");
								insertAfterIds=insertAfterIds==undefined?'':insertAfterIds;
								insertAftervisitId = insertAfterIds.substring(0,insertAfterIds.indexOf("/"));
								for (oKey in updateVisitIds ) {
									if(oKey.match(regularExp) && updateVisitIds[oKey] != undefined){
										if ((updateVisitIds[oKey] == insertAftervisitId && (oOldData != oNewData)) || (updatedVisits[oKey] == insertAfter && (oOldData != oNewData))){
											var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
											var Td = parent.getElementsByClassName('yui-dt-col-'+column.key, 'td')[0];
											var El = new YAHOO.util.Element(Td);
											alert(PlsSvSpec_DepdIntr);//alert("Please save all visits before specifying depending intervals.");*****
											El.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = oArgs.oldData;
											var oldsData= (oArgs.oldData.length>8)? oArgs.oldData.substring(0,8)+"...":oArgs.oldData;
											this.updateCell(oRecord, column.key, oldsData, false);
											oRecord.setData(column.key, oArgs.oldData);	
											if (column.key == 'insertAfter'){
												VELOS.manageVisitsGrid.setDropDownId(this,elCell,insertAftervisitIds,insertAftervisitNames);
											}
											return false;
										}
									}
								}
								
								for (oKey in updateVisitIds ) {
									if(oKey.match(regularExp) && updateVisitIds[oKey] != undefined){
										var childVisit = updateVisitIds[oKey];
										if ((parentVisitIds.indexOf(oRecord.getData('visitId'))>=0) && (changedChildVisitIds.indexOf(childVisit)>=0)){		
											alert(SvChldVst_BfrPrtVst);//alert("Please save all child visits before specifying intervals for parent visit.");****
											var oldsData= (oArgs.oldData.length>8)? oArgs.oldData.substring(0,8)+"...":oArgs.oldData;
											this.updateCell(oRecord, column.key, oldsData, false);
											oRecord.setData(column.key, oArgs.oldData);
											VELOS.manageVisitsGrid.changeDependentNameOfParent(this, oRecord,oNewData,oOldData);
											return false; 
										}
									}
								}
							}
							
						}else{
							oData = oOldData;
							oRecord.setData(column.key, oData);
							this.onEventShowCellEditor({target:elCell});
						}
					}
					if(oNewData == L_Select_AnOption){				
						VELOS.manageVisitsGrid.eraseDependent(this,elCell,column.key,'');
					}
					if(column.key == 'visitName'){ //Ak:added to display correct visit name in 'Preview and Save' dialog.
						var seqExist=manageVisitGridChangeList['visitSeq'+visitSeq];
						if(seqExist!= undefined){
							var addedVisit=addedVisits['visitSeq'+visitSeq];
							if(addedVisit!=undefined)
								addedVisits['visitSeq'+visitSeq]=oRecord.getData('visitName');
							var updatedVisit=updatedVisits['visitSeq'+visitSeq];
							if(updatedVisit!=undefined)
								updatedVisits['visitSeq'+visitSeq]=oRecord.getData('visitName');
						}
						
					}
					if (oOldData != oNewData){
						if(!((oOldData == null || oOldData == '') && (oNewData == L_Select_AnOption 
								|| oNewData == '' || oNewData==null ))){
							VELOS.manageVisitsGrid.auditAction(visitSeq, 'Updated',oRecord.getData('visitName'),oRecord.getData('visitId'));
						}
					}
					
				});	
				
			   myDataTable.subscribe("cellMouseoverEvent", function(oArgs) {//AK:Added for enhancement PCAL-20801
				    var target = oArgs.target;
					var column = this.getColumn(target);
					var oRecord = this.getRecord(target);
					var visitName =oRecord.getData('visitName');
					myDataTable.onEventHighlightRow(oArgs);
					
					if(column.key=='visitSeq' ){
						/* Bug#9791 16-May-2012 -Sudhir*/
						return overlib(L_Serial+"&nbsp;#",CAPTION,L_ColName);				
					}
					
					//Added for Bug#9300 : Raviesh
					if(column.key=='visitName'){
						if(visitName!=null && visitName!=''){
						return overlib(oRecord.getData('visitName'),CAPTION,L_Visit_Name);}
						else{
							return overlib(L_Visit_Name,CAPTION,L_ColName);
						}
					}					
					
					if(column.key=='description'){
						var descricption=oRecord.getData('description')
						if(descricption!=null && descricption!=''){
						  return overlib(descricption,CAPTION,L_Visit_Desc);}
						else{
							return overlib(L_Description,CAPTION,L_ColName);
						}
					}

					if(column.key=='beforenum' || column.key=='durUnitA'){
						return overlib(L_Visit_Window+" - "+L_Before,CAPTION,L_ColName);				
					}
					
					if(column.key=='afternum' || column.key=='durUnitB'){
						return overlib(L_Visit_Window+" - "+L_After,CAPTION,L_ColName);
					}
					
					if(column.key=='noInterval'){
						var descricption=oRecord.getData('noInterval')
						if(descricption!=null && descricption!=''){
							return overlib(L_Time_Point_Type,CAPTION,L_ColName);
						}
					}
					
					if(column.key=='interval'){
							return overlib(L_CalculatedTime_Point,CAPTION,L_ColName);	
					}					
					
					if(column.key=='months'){
						return overlib(L_FixedTime_Point+" - "+L_Month,CAPTION,L_ColName);}
					else if(column.key=='weeks'){
						return overlib(L_FixedTime_Point+" - "+L_Week,CAPTION,L_ColName);}
					else if(column.key=='days'){
						return overlib(L_FixedTime_Point+" - "+L_Day,CAPTION,L_ColName);}
					
					if(column.key=='insertAfterInterval'){
						return overlib(L_DepndTime_Point+" - "+L_Interval,CAPTION,L_ColName);}
					else if(column.key=='intervalUnit'){
						return overlib(L_DepndTime_Point+" - "+L_Mwd,CAPTION,L_ColName);}
					else if(column.key=='afterText'){
						return overlib(L_DepndTime_Point+" - "+L_After,CAPTION,L_ColName);}
					else if(column.key=='insertAfter'){
						var insertAfter =oRecord.getData('insertAfter');
						if(insertAfter!=null && insertAfter!=''){
							return overlib(oRecord.getData('insertAfter'),CAPTION,L_Visit_Name);}
							else{
								return overlib(L_DepndTime_Point+" - "+L_Visit_Name,CAPTION,L_ColName);
							}
						}
					//End for Bug#9300
					
		    	});
			   
				myDataTable.subscribe("cellMouseoutEvent", function(oArgs) {
					myDataTable.onEventUnhighlightRow(oArgs);
					return nd();
				});
			   myDataTable.focus();
		
			   if (YAHOO.util.Dom.get("rowCount")){
					YAHOO.util.Dom.get("rowCount").value="0";
					 
				}	
			   
			   if (calStatus == 'A'|| calStatus == 'D' || calStatus == 'F') {
					if ($('save_changes')) { $('save_changes').disabled = true;  $('save_changes').visibility = "hidden";}
				}
				
			   
		hidePanel();
	};
	this.handleFailure = function(o) {
		var paramArray = [o.responseText];
        alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp",paramArray));/*alert(svrCommErr+":"+o.responseText);*****/
		hidePanel();
	};
	this.cArgs = [];
	this.callback = {
		success:this.handleSuccess,
		failure:this.handleFailure,
		argument:this.cArgs
	};
	this.initConfigs(args);
}
/** 
 * Checks for updated child visits.
 * 
 */			   
VELOS.manageVisitsGrid.checkChildChanged = function (myDataTable, oRecord,selVisitId,insertAfter){
	var arrRecords = myDataTable.getRecordSet();
	var insertAfterIds;
	var insertAfterId;
	var tempRecord;
	var insertAftervisitId;
	var aftervisitId;
	var insertAfterName;
	var timePointType;
	var nointervalTypeId=oRecord.getData("noIntervalId");
	var visitSel=oRecord.getData("visitName");
	var visitIdSel=oRecord.getData("visitId");
	
	if(visitIdSel != "0" && nointervalTypeId!=noTimePointdefined){
		var regularExp =  new RegExp('visitSeq[0-9]+$');
		for(rec=0; rec < arrRecords.getLength(); rec++){
			tempRecord = arrRecords.getRecord(rec);
			insertAfterIds = tempRecord.getData("insertAfterId");
			aftervisitId = tempRecord.getData("visitId");
			insertAfterName=tempRecord.getData("insertAfter");
			timePointType = tempRecord.getData("noIntervalId");
			insertAftervisitId = insertAfterIds==undefined ?'':insertAfterIds.substring(0,insertAfterIds.indexOf("/"));
			insertAfterId = insertAfter.substring(0,insertAfter.indexOf("/"));
			if(nointervalTypeId==fixedPointdefined && (visitSel!=insertAfterName || visitIdSel!=insertAfterId )){
				continue;
			}
			if (changedChildVisitIds.indexOf(aftervisitId)<0 && parentVisitIds.indexOf(aftervisitId)<0 && timePointType==depnPointdefined){
				if(parentVisitIds.indexOf(selVisitId)<0){
			        var len= changedChildVisitIds.length;
					changedChildVisitIds[len]=aftervisitId;
				}else{ //YK:Fixed Bug#10689
					var len= changedChildVisitIds.length;
					changedChildVisitIds[len]=selVisitId;
				}
			}
		}
	}
}
/**
 * Added method to set the DropDown fields values based on selection criteria.
 */

VELOS.manageVisitsGrid.updateDependent = function (columnObject,recordObject,dependent,depValue,respJ){
	var dependentId = recordObject.getData(dependent);
	if(dependentId!=undefined && dependentId>0 ){
		var depArray = respJ[depValue+dependentId];
		insertAftervisitIds = [];
		insertAftervisitNames = [];	
		
		for (var iL=0; iL<depArray.length; iL++){
			var depJSON = depArray[iL];
			for(key in depJSON){
				insertAftervisitIds[iL] = key;
				insertAftervisitNames[iL] =depJSON[key];
			}
		}
		if(columnObject.key == 'insertAfter'){
			insertAftervisitNames = (insertAftervisitNames.length > 0)? insertAftervisitNames:null;
			columnObject.editor.dropdownOptions = insertAftervisitNames;
			columnObject.editor.render();
		}
	} else if(respJ.insertAftVisitNames.length>1) {
		insertAftervisitIds=respJ.insertAftVisitIds;
		insertAftervisitNames=respJ.insertAftVisitNames;
		columnObject.editor.dropdownOptions = insertAftervisitNames;
		columnObject.editor.render();
		
	} else{
		alert(PlsSv_VstDepend);/*alert("Please save all visits before specify depending intervals.");*****/
	}

}

/**
 * Update the dropdown values which is not having  Id field
 * 
 */

VELOS.manageVisitsGrid.updateDropDown = function (columnObject,recordObject,dependent,depValue,respJ){
	var visitId = recordObject.getData(dependent);
	if(visitId!=undefined ){
		var insertUnitValues=[];
		insertUnitValues= respJ[depValue+visitId];
		if(insertUnitValues!='' || insertUnitValues!= undefined){
			if(columnObject.key == 'intervalUnit'){
				columnObject.editor.dropdownOptions = insertUnitValues;
				columnObject.editor.render();	
			}
		}
	
	} 
}


/**
 * Method to erase the value when user selects 'Select an Option' in dropdown 
 */

VELOS.manageVisitsGrid.eraseDependent = function (myGrid,elCell,dependent,depValue,showDisabled){
	var oRecord = myGrid.getRecord(elCell);
	var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
	var depTd = parent.getElementsByClassName('yui-dt-col-'+dependent, 'td')[0];	
	var depEl = new YAHOO.util.Element(depTd);	
	var dep = depEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;	
	if (oRecord)
		oRecord.setData(dependent, depValue);
	depEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = depValue;
	
	if (showDisabled){
		depTd.bgColor = '#E6E4E4';
	}
}

VELOS.manageVisitsGrid.resetCellColor = function (myGrid,elCell,dependent){
	var oRecord = myGrid.getRecord(elCell);
	var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
	var depTd = parent.getElementsByClassName('yui-dt-col-'+dependent, 'td')[0];	
	var visitSeqTd = parent.getElementsByClassName('yui-dt-col-visitSeq', 'td')[0];	
	depTd.bgColor = visitSeqTd.bgColor;
}

VELOS.manageVisitsGrid.changeDependentOfParent =  function (myGrid,oRecordSel){
	var tabLength= myGrid.getRecordSet().getLength();
	var recordSet = myGrid.getRecordSet();
	var visitSeqSel = oRecordSel.getData('visitSeq');
	var visitIdSel = oRecordSel.getData('visitId');
	var visitNameSel = oRecordSel.getData('visitName');
	var noIntervalSel = oRecordSel.getData('noInterval');
	var insertAfterNameSel = oRecordSel.getData('insertAfter');
	var noIntervalId = oRecordSel.getData('noIntervalId');
	var insertAfterIdSel;
	var m, w, d, displacementSel;
	
	if (noIntervalId == noTimePointdefined){
		if (column.key == 'noInterval'){
			//Do nothing
			return;
		}
	}
	if (noIntervalId == fixedPointdefined){
		var monthsSel = oRecordSel.getData('months');
		var weeksSel = oRecordSel.getData('weeks');
		var daysSel = oRecordSel.getData('days')
		if (monthsSel || weeksSel || daysSel){
			m = parseInt(oRecordSel.getData('months'));
			w = parseInt(oRecordSel.getData('weeks'));
			d = parseInt(oRecordSel.getData('days'));
			displacementSel = VELOS.manageVisitsGrid.calculateDisp(w,m,d);
			if (d==0 && displacementSel == 1)
				displacementSel--;
		} else return;
	}
	if (noIntervalId == depnPointdefined){
		var insertAfterVisitName = oRecordSel.getData("insertAfter");
		if(insertAfterVisitName==L_Select_AnOption)
		{
			return false;
		}
		var incompleteInfo = false;
		if (!insertAfterVisitName || insertAfterVisitName == L_Select_AnOption) incompleteInfo = true;
		insertAfterIdSel = oRecordSel.getData("insertAfterId");
		if (!insertAfterIdSel) incompleteInfo = true;
		insertAfterIdSel=insertAfterIdSel==undefined?'':insertAfterIdSel;
		var arrayInsertAfterId=insertAfterIdSel.split("/");
		insertAfterIdSel = arrayInsertAfterId[0];
		if (!arrayInsertAfterId[0])incompleteInfo = true;
		if (!arrayInsertAfterId[1])incompleteInfo = true;
		displacementSel =arrayInsertAfterId[1]==undefined?'': parseInt(arrayInsertAfterId[1]);
		
		var insertAfterInterval = oRecordSel.getData("insertAfterInterval");
		if (!insertAfterInterval)incompleteInfo = true;
		var intervalUnit = oRecordSel.getData("intervalUnit");
		if (!intervalUnit)incompleteInfo = true;

		if (!incompleteInfo){
			var months=0;
			var weeks=0;
			var days=0;
			if (intervalUnit==L_Months){
				months = parseInt(insertAfterInterval);
				displacementSel+= months*30;
			} else if (intervalUnit==L_Weeks){
				weeks = parseInt(insertAfterInterval);
				displacementSel+= weeks*7;
			} else { //days
				days = parseInt(insertAfterInterval);
				displacementSel+= days;
			}
		} else return;
	}
	VELOS.manageVisitsGrid.auditAction(visitSeqSel, 'Updated',visitNameSel,visitIdSel);
	insertAfterIdSel = oRecordSel.getData("insertAfterId");
	var isRealFakeVisitSel = oRecordSel.getData('isRealFakeVisit');//YK:Fixed Bug#10555
	for (var iX=0; iX<tabLength; iX++) {
		var oRecord = recordSet.getRecord(iX);
		var visitSeq = oRecord.getData('visitSeq');
		if (visitSeqSel != visitSeq){
			var visitId = oRecord.getData('visitId');
			var visitName = oRecord.getData('visitName');
			var insertAfterName = oRecord.getData('insertAfter');
			var insertAfterId = oRecord.getData('insertAfterId');
			var arrayInsertAfterId=insertAfterId.split("/");
			insertAfterId = arrayInsertAfterId[0];
			var isFakeVisit = oRecord.getData('isFakeVisit');
			var isRealFakeVisit = oRecord.getData('isRealFakeVisit');//YK:Fixed Bug#10555
			var arrayIsFakeVisit=isFakeVisit.split("/");
			isFakeVisit = (arrayIsFakeVisit[2]==undefined)?'':arrayIsFakeVisit[2];
			if(insertAfterId == "-1" && insertAfterName != visitNotListed && visitIdSel == isFakeVisit){
				insertAfterName = arrayIsFakeVisit[1];
				insertAfterId = isFakeVisit;
			}

			if(insertAfterNameSel != visitName){//YK:Fixed Bug#10738
				if (visitIdSel == insertAfterId || (insertAfterName != visitNotListed && visitIdSel == isFakeVisit)&& (insertAfterIdSel.indexOf("-1")==-1)&& visitIdSel!=0){
					var oData = visitIdSel+"/"+displacementSel;
					myGrid.updateCell(oRecord, "insertAfterId", oData , false);
				    oRecord.setData("insertAfterId",oData);
					VELOS.manageVisitsGrid.auditAction(visitSeq, 'Updated',visitName,visitId);
				    VELOS.manageVisitsGrid.changeDependentOfParent(myGrid,oRecord);
				}else if (visitIdSel == insertAfterId || (insertAfterName != visitNotListed && (visitNameSel == insertAfterName))&& visitIdSel!=0){
					var oData = visitIdSel+"/"+displacementSel;
					myGrid.updateCell(oRecord, "insertAfterId", oData , false);
				    oRecord.setData("insertAfterId",oData);
					VELOS.manageVisitsGrid.auditAction(visitSeq, 'Updated',visitName,visitId);
				    VELOS.manageVisitsGrid.changeDependentOfParent(myGrid,oRecord);
				}
				if (insertAfterIdSel=="-1/-1" && (visitIdSel == insertAfterId || visitNameSel == insertAfterName)&& visitIdSel!=0){
					var oData = "-1/-1";
					myGrid.updateCell(oRecord, "insertAfterId", oData , false);
				    oRecord.setData("insertAfterId",oData);
				    var ddVisitIds = respJ['insertAftVisitPks'];
					var ddVisitNames = respJ['insertAftVisitNames'];
				    var parentId = "";

					for (var iL=0; iL<ddVisitIds.length; iL++){
						if (ddVisitNames[iL] == insertAfterName){
							parentId =ddVisitIds[iL]; 
							break;
						}
					}
					isFakeVisit="true/"+insertAfterName+"/"+parentId;
					myGrid.updateCell(oRecord, 'isFakeVisit', isFakeVisit, false);
					oRecord.setData('isFakeVisit',isFakeVisit);    
				    VELOS.manageVisitsGrid.changeDependentOfParent(myGrid,oRecord);
				}
			}
		}
	}
}

VELOS.manageVisitsGrid.changeDependentNameOfParent =  function (myGrid,oRecordSel,oldName,newName){
	var tabLength= myGrid.getRecordSet().getLength();
	var recordSet = myGrid.getRecordSet();
	var noInterval =oRecordSel.getData('noIntervalId');
	var visitName =oRecordSel.getData('visitName');
	var visitIdSel = oRecordSel.getData('visitId');
	var tempVisitName=oldName;
	for (var iX=0; iX<tabLength; iX++) {
		var oRecord = recordSet.getRecord(iX);
		var visitSeq = oRecord.getData('visitSeq');
		if (noInterval == depnPointdefined){
			var insertAfterName = oRecord.getData('insertAfter');
			
			if(insertAfterName.indexOf("</font>")>0){
				insertAfterName=insertAfterName.substring((insertAfterName.indexOf(">")+1),insertAfterName.indexOf("</font>"));
			}
			
			var visitCells= $D.getElementsByClassName('yui-dt-col-visitName', 'td');
			var el = new YAHOO.util.Element(visitCells[iX]);
			var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));

			var parentId = parent.get('id');
			var insertAfterTd = parent.getElementsByClassName('yui-dt-col-insertAfter', 'td')[0];
			var insertAfterEl = new YAHOO.util.Element(insertAfterTd);
			var insertAfter = insertAfterEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;

			var insertAfterId = oRecord.getData('insertAfterId');
			var arrayInsertAfterId=insertAfterId.split("/");
			insertAfterId = arrayInsertAfterId[0];
			if (visitIdSel == insertAfterId || (insertAfterName != visitNotListed && insertAfterName == tempVisitName ) && visitIdSel!=0){
				
				var tempNewName= (newName.length>8)? newName.substring(0,8)+"...":newName;
			    //myGrid.updateCell(oRecord, "insertAfter", tempNewName , false);
			    oRecord.setData("insertAfter",newName);
			}
		}
	}
}
VELOS.manageVisitsGrid.changeDependentOfFake =  function (myGrid,oRecordSel,visitIdSel,visitNameSel,insertAfter,insertAfterIdSel,interval){
	var tabLength= myGrid.getRecordSet().getLength();
	var recordSet = myGrid.getRecordSet();
	//var oColumnSel = myGrid.getColumn(elCell);
	//var oRecordSel = myGrid.getRecord(elCell);
	var visitSeqSel = oRecordSel.getData('visitSeq');
	var visitIdSel = oRecordSel.getData('visitId');
	var visitNameSel = oRecordSel.getData('visitName');
	var insertAfterVisitName = oRecordSel.getData("insertAfter");
	if(insertAfterVisitName==L_Select_AnOption)
	{
		return false;
	}
	for (var iX=0; iX<tabLength; iX++) {
		var oRecord = recordSet.getRecord(iX);
		var visitSeq = oRecord.getData('visitSeq');
		var visitId = oRecord.getData('visitId');
		var visitName = oRecord.getData('visitName');
		var insertAfterName = oRecord.getData('insertAfter');
		var insertAfterId = oRecord.getData('insertAfterId');

	if(visitNameSel.length>0){
		if(insertAfterIdSel!="-1/-1"){
			if(visitNameSel==insertAfterName && visitNameSel!=insertAfter){
				var disp="";
				disp =visitIdSel+"/"+interval;
			    myGrid.updateCell(oRecord, "insertAfterId", disp , false);
			    oRecord.setData("insertAfterId",disp);
			    VELOS.manageVisitsGrid.auditAction(visitSeq, 'Updated',visitName,visitId);
				VELOS.manageVisitsGrid.auditAction(visitSeqSel, 'Updated',visitNameSel,visitIdSel);
				var insertAfterInterval = oRecord.getData("insertAfterInterval");
				var insertAfterId = oRecord.getData('insertAfterId');
				var duration = oRecord.getData("intervalUnit");
				var months=0;
				var weeks=0;
				var days=0;
				if (duration==L_Months)
					months = insertAfterInterval;
				else if (duration==L_Weeks)
					weeks = insertAfterInterval;
				else //days
					days = insertAfterInterval;
				var m = parseInt(months);
				var w = parseInt(weeks);
				var d = parseInt(days);
				var displacement= VELOS.manageVisitsGrid.calculateDisp(w,m,d);
				var dispSelected = disp;
				dispSelected = dispSelected.substring((dispSelected.indexOf("/")+1));
				displacement =parseInt(dispSelected)+parseInt(displacement);
				VELOS.manageVisitsGrid.changeDependentOfFake(myGrid,oRecord,visitId,visitName,insertAfter,insertAfterId,displacement);
			    
			
			}
	  	}else if (insertAfterIdSel=="-1/-1"){
			if(visitName==insertAfter && visitNameSel!=insertAfter){
			  	var disp="";
			  	var insertAfterInterval = oRecord.getData("insertAfterInterval");
				var insertAfterId = oRecord.getData('insertAfterId');
				insertAfterId=insertAfterId==undefined ?'':insertAfterId;
				var duration = oRecord.getData("intervalUnit");
			    var months=0;
				var weeks=0;
				var days=0;
				if (duration==L_Months)
					months = insertAfterInterval;
				else if (duration==L_Weeks)
					weeks = insertAfterInterval;
				else //days
					days = insertAfterInterval;
				var m = parseInt(months);
				var w = parseInt(weeks);
				var d = parseInt(days);
				var displacement= VELOS.manageVisitsGrid.calculateDisp(w,m,d);
				if(insertAfterId=="-1/-1"){
					disp="-1/-1";
					myGrid.updateCell(oRecord, "insertAfterId", disp, false);
					oRecord.setData("insertAfterId",disp);
					var insertAfterIdSel = oRecord.getData("insertAfterId");
					myGrid.updateCell(oRecordSel, "insertAfterId", disp , false);
					oRecordSel.setData('insertAfterId',disp);
					//insertAfterElSel.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML = disp;
					VELOS.manageVisitsGrid.auditAction(visitSeqSel, 'Updated',visitNameSel,visitIdSel);
				}else{
					disp=insertAfterId;
					var dispSelected = disp;
					dispSelected = dispSelected.substring((dispSelected.indexOf("/")+1));
					displacement =parseInt(dispSelected)+parseInt(displacement);
					disp =insertAfterId;
					myGrid.updateCell(oRecord, "insertAfterId", disp , false);
					oRecord.setData("insertAfterId",disp);
					var insertAfterIdSel = oRecord.getData("insertAfterId");
					myGrid.updateCell(oRecordSel, "insertAfterId", (visitId+"/"+displacement) , false);
					oRecordSel.setData('insertAfterId',(visitId+"/"+displacement));
					VELOS.manageVisitsGrid.auditAction(visitSeqSel, 'Updated',visitNameSel,visitIdSel);
				}

				VELOS.manageVisitsGrid.changeDependentOfFake(myGrid,oRecord,visitId,visitName,insertAfter,insertAfterIdSel,displacement);
		  }else if(visitNameSel==insertAfterName && visitNameSel!=insertAfter){
			    disp ="-1/-1";
				myGrid.updateCell(oRecord, "insertAfterId" , disp , false);
				oRecord.setData("insertAfterId",disp);
				VELOS.manageVisitsGrid.auditAction(visitSeq, 'Updated',visitName,visitId);
				VELOS.manageVisitsGrid.auditAction(visitSeqSel, 'Updated',visitNameSel,visitIdSel);
				VELOS.manageVisitsGrid.changeDependentOfFake(myGrid,oRecord,visitId,visitName,insertAfter,insertAfterIdSel,'');
		  }
	  }
	}
  }
}	

VELOS.manageVisitsGrid.findRealDisplacement = function (myGrid, parentPk){
	var tabLength= myGrid.getRecordSet().getLength();
	var recordSet = myGrid.getRecordSet();
	for (var iX=0; iX<tabLength; iX++) {
		var oRecord = recordSet.getRecord(iX);
		var visitSeq = oRecord.getData('visitSeq');
		var visitId = oRecord.getData('visitId');
		if (parentPk == visitId){
			var noIntervalId = oRecord.getData('noIntervalId');
			var insertAfterIdSel;
			var m, w, d, displacementSel;
	
			if (noIntervalId == noTimePointdefined){
				if (column.key == 'noInterval'){
					//Do nothing
					return;
				}
			}
			if (noIntervalId == fixedPointdefined){
				var monthsSel = oRecord.getData('months');
				var weeksSel = oRecord.getData('weeks');
				var daysSel = oRecord.getData('days')
				if (monthsSel || weeksSel || daysSel){
					m = parseInt(oRecord.getData('months'));
					w = parseInt(oRecord.getData('weeks'));
					d = parseInt(oRecord.getData('days'));
					displacementSel = VELOS.manageVisitsGrid.calculateDisp(w,m,d);
					if (d==0 && displacementSel == 1)
						displacementSel--;
				} else return null;
			}
			if (noIntervalId == depnPointdefined){
				var incompleteInfo = false;
				insertAfterIdSel = oRecord.getData("insertAfterId");
				if (!insertAfterInterval) incompleteInfo = true;
				insertAfterIdSel=insertAfterIdSel==undefined?'':insertAfterIdSel;
				var arrayInsertAfterId=insertAfterIdSel.split("/");
				insertAfterIdSel = arrayInsertAfterId[0];
				if (!insertAfterIdSel) incompleteInfo = true;
				displacementSel = parseInt(arrayInsertAfterId[1]);
				if (!displacementSel) incompleteInfo = true;
				
				var insertAfterInterval = oRecord.getData("insertAfterInterval");
				if (!insertAfterInterval) incompleteInfo = true;
				var intervalUnit = oRecord.getData("intervalUnit");
				if (!intervalUnit) incompleteInfo = true;
				if (!incompleteInfo){
					var months=0;
					var weeks=0;
					var days=0;
					if (intervalUnit==L_Months){
						months = parseInt(insertAfterInterval);
						displacementSel+= months*30;
					} else if (intervalUnit==L_Weeks){
						weeks = parseInt(insertAfterInterval);
						displacementSel+= weeks*7;
					} else {//days
						days = parseInt(insertAfterInterval);
						displacementSel+= days;
					}
				} else return null;
			}
			return displacementSel;
		}
	}
	return;
}

/**
 *  Function to set Codelist value in corresponding hidden columns.
*/
VELOS.manageVisitsGrid.setDropDownId = function (myGrid,elCell,myIds,myTypes){		
	var oRecord = myGrid.getRecord(elCell);			
	var oColumn =  myGrid.getColumn(elCell);	
	jsonDataToReset=null;
	for (var j=0; j<myIds.length; j++){
		if (myTypes[j] == oRecord.getData(oColumn.key)) break;
	}
	var noInterId = oRecord.getData("noIntervalId");
	var months = oRecord.getData("months");
	var weeks = oRecord.getData("weeks");
	var days = oRecord.getData("days");
	var instAftrInrval = oRecord.getData("insertAfterInterval");
	var aftrInrvalUnt = oRecord.getData("intervalUnit");
	var insrtAftr = oRecord.getData("insertAfter");
	var insrtAftrId = oRecord.getData("insertAfterId");
	oRecord.setData(oColumn.key +'Id', myIds[j]);
	var parent = new YAHOO.util.Element($D.getAncestorByTagName(elCell, 'tr'));
	var insertAfterTd = parent.getElementsByClassName('yui-dt-col-'+oColumn.key, 'td')[0];
	var insertAfter = oRecord.getData(oColumn.key);
	
	var visitId = oRecord.getData("visitId");
	var visitName = oRecord.getData("visitName");
	var fakeVisit = oRecord.getData("isFakeVisit");
	
	var arrayFakeVisit = fakeVisit.split('/');
	var isFakeVisit = arrayFakeVisit[0];
	var isFakeVisitname = arrayFakeVisit[1];
	
	var Id = oRecord.getData(oColumn.key+'Id');
	myGrid.updateCell(oRecord, oColumn.key+'Id', myIds[j], false);
	oRecord.setData(oColumn.key+'Id', myIds[j]);
	Id = oRecord.getData(oColumn.key+'Id');
	var tempInsertAfter=insertAfter;
	if(noInterId==fixedPointdefined ){
		jsonDataToReset = {"noInterValId":noInterId,"months":months,"weeks":weeks,"days":days};
	}
	if(noInterId==depnPointdefined ){
		jsonDataToReset = {"noInterValId":noInterId,"insertAfterInterval":instAftrInrval,"intervalUnit":aftrInrvalUnt,"insertAfter":insrtAftr,"insertAfterId":insrtAftrId};
	}
	if(oColumn.key=="insertAfter"){
		// To check deleted visit is not selected as Parent.
		var insertAfterIdAftr=oRecord.getData("insertAfterId");
		insertAfterIdAftr=insertAfterIdAftr==undefined?'':insertAfterIdAftr;
		var visitIdAftr=insertAfterIdAftr.split("/");
		var insrtAftrAftr = oRecord.getData("insertAfter");
		var regularExp =  new RegExp('visitSeq[0-9]+$');
		for (oKey in deleteRows) {
			if (oKey.match(regularExp) && deleteRows[oKey] != undefined) {
				if(visitIdAftr[0]==deleteRows[oKey] || insrtAftrAftr==deletedVisits[oKey] )
				{
					return false;
				}
			}
		}
	
		if(isFakeVisit=="true" && myIds[j]=="-1/-1" && isFakeVisitname==insertAfter){
			if(visitNotListed==insertAfter){
				tempInsertAfter ="<FONT class='mandatory'>"+insertAfter+"</FONT>";
			}else{
				tempInsertAfter ="<FONT class='mandatory'>"+((insertAfter.length>8)? insertAfter.substring(0,8)+"...":insertAfter)+"</FONT>";
			}
		}else if(myIds[j]=="-1/-1"){
			var ddVisitIds = respJ['insertAftVisitPks'];
			var ddVisitNames = respJ['insertAftVisitNames'];	
			var parentId = "";

			for (var iL=0; iL<ddVisitIds.length; iL++){
				if (ddVisitNames[iL] == insertAfter){
					parentId =ddVisitIds[iL]; 
					break;
				}
			}
			isFakeVisit="true/"+insertAfter+"/"+parentId;
			myGrid.updateCell(oRecord, 'isFakeVisit', isFakeVisit, false);
			oRecord.setData('isFakeVisit',isFakeVisit);
			var circularRelation = VELOS.manageVisitsGrid.findCircularRelations(myGrid, oRecord);
			if (circularRelation){
				myGrid.updateCell(oRecord, 'insertAfter', '', false);
				oRecord.setData('insertAfter', '');
				myGrid.updateCell(oRecord, 'insertAfterId', '', false);
				oRecord.setData('insertAfterId', '');
				myGrid.updateCell(oRecord, 'isFakeVisit', '', false);
				oRecord.setData('isFakeVisit','');
				return;
			}
			VELOS.manageVisitsGrid.changeDependentOfFake(myGrid,oRecord,visitId,visitName,insertAfter,Id,'');
			if(visitNotListed==insertAfter){
				tempInsertAfter =insertAfter;
			}else{
				tempInsertAfter = (insertAfter.length>8)? insertAfter.substring(0,8)+"...":insertAfter;	
			}
		}else{
			var incompleteInfo = false;
			if (!insrtAftr || insrtAftr == L_Select_AnOption) incompleteInfo = true;
			var insertAfterInterval = oRecord.getData('insertAfterInterval');
			if (!insertAfterInterval) incompleteInfo = true;
			var duration = oRecord.getData('intervalUnit');
			if (!duration) incompleteInfo = true;
			var months=0;
			var weeks=0;
			var days=0;
			var circularRelation = VELOS.manageVisitsGrid.findCircularRelations(myGrid, oRecord);
			if (circularRelation){
				myGrid.updateCell(oRecord, 'insertAfter', '', false);
				oRecord.setData('insertAfter', '');
				myGrid.updateCell(oRecord, 'insertAfterId', '', false);
				oRecord.setData('insertAfterId', '');
				myGrid.updateCell(oRecord, 'isFakeVisit', '', false);
				oRecord.setData('isFakeVisit','');
				return;
			}
			if (!incompleteInfo){
				if (duration==L_Months)
					months = insertAfterInterval;
				else if (duration==L_Weeks)
					weeks = insertAfterInterval;
				else //days
					days = insertAfterInterval;
			
				var m = parseInt(months);
				var w = parseInt(weeks);
				var d = parseInt(days);
				var displacement= VELOS.manageVisitsGrid.calculateDisp(w,m,d);
				var dispSelected = myIds[j];
				var arrayParent = dispSelected.split('/');
				var dispParent = VELOS.manageVisitsGrid.findRealDisplacement(myGrid, arrayParent[0]);		
	
				if (dispParent != null){
					var insertAfterId = arrayParent[0] +'/'+dispParent;
					myGrid.updateCell(oRecord, 'insertAfterId', insertAfterId, false);
					oRecord.setData('insertAfterId', insertAfterId);
					myGrid.updateCell(oRecord, 'isFakeVisit', '', false);
					oRecord.setData('isFakeVisit','');
					VELOS.manageVisitsGrid.changeDependentOfParent(myGrid,oRecord);
				}
			}
			tempInsertAfter = (insertAfter.length>8)? insertAfter.substring(0,8)+"...":insertAfter;	
		}
		myGrid.updateCell(oRecord, oColumn.key, tempInsertAfter , false);
		oRecord.setData(oColumn.key,insertAfter);
	}
};

/**
 * Method to check parent Visit is not changed to No Time Point
 */

VELOS.manageVisitsGrid.getDependentOnMe = function (myGrid,elCell){
	var oRecordSel = myGrid.getRecord(elCell);			
	var oColumnSel =  myGrid.getColumn(elCell);	

	var visitName = oRecordSel.getData("visitName");
	var visitId = oRecordSel.getData("visitId");
	var insertAfterNameSel = oRecordSel.getData("insertAfter");
	var insertAfterIdSel = oRecordSel.getData("insertAfterId");
	var insertIDDSel=insertAfterIdSel==undefined ?'':insertAfterIdSel.substring(0,insertAfterIdSel.indexOf("/"));
	parentVisitIds = [];
	childVisitInsertAfter = [];
	var tabLength= myGrid.getRecordSet().getLength();
	var recordSet = myGrid.getRecordSet();
	var length;
	for (var iX=0; iX<tabLength; iX++) {
		var oRecord = recordSet.getRecord(iX);
		var insertAfterName = oRecord.getData("insertAfter");
		var noIntervalId =  oRecord.getData("noIntervalId");
		var visitIdIt =  oRecord.getData("visitId");
		var visitNameIt =  oRecord.getData("visitName");
		var insertAfterId =  oRecord.getData("insertAfterId");
		var insertIDD;
		insertIDD= insertAfterId==undefined ?'':insertAfterId.substring(0,insertAfterId.indexOf("/"));

		if(insertIDD=="-1" && insertAfterName==visitName){
			insertIDD=visitId;
		}
		if(insertIDDSel=="-1" && visitNameIt==insertAfterNameSel && noIntervalId==noTimePointdefined){
			insertIDD=visitIdIt;
		}
		if(insertIDD.length>0 && insertIDD!="-1" && parentVisitIds.indexOf(insertIDD)<0 && parentVisitIds.indexOf(visitId)<0){
			length=parentVisitIds.length;
			parentVisitIds[length]=insertIDD;
		}
		childVisitInsertAfter[visitIdIt]=insertAfterId;//YK:Fixed Bug#10555
	 }
}

/**
 * Checks for child visits.
 * Fixed 7392
 */			   
VELOS.manageVisitsGrid.checkChild = function (visitId,visitName){
	var arrRecords = myDataTable.getRecordSet();
	var insertAfterIds;
	var insertAfter;
	var tempRecord;
	var insertAftervisitId;
	for(records=0; records < arrRecords.getLength(); records++){
		tempRecord = arrRecords.getRecord(records);
		insertAfter = tempRecord.getData("insertAfter");
		insertAfterIds = tempRecord.getData("insertAfterId");
		insertAftervisitId = insertAfterIds==undefined ?'':insertAfterIds.substring(0,insertAfterIds.indexOf("/"));
		if(insertAftervisitId == visitId || visitName==insertAfter){
		       return true;
		}		
	}
	return false;
	
   }
/**
 * Method to customize the Grid
 */ 
/*Function to enable/disable editors for time-point child columns */
VELOS.manageVisitsGrid.enableDisableChildColumns = function (oColumn, noIntervalId){
	var key = oColumn.key;
	if (noIntervalId == noTimePointdefined){
		if ((key == 'months') ||
			(key == 'weeks') ||
			(key == 'days') ||
			(key == 'insertAfterInterval') ||
			(key == 'intervalUnit') ||
			(key == 'insertAfter'))
			return 0;
	}
	if (noIntervalId == fixedPointdefined){
		if ((key == 'insertAfterInterval') ||
			(key == 'intervalUnit') ||
			(key == 'insertAfter'))
			return 0;
	}
	if (noIntervalId == depnPointdefined){
		if ((key == 'months') ||
			(key == 'weeks') ||
			(key == 'days'))
			return 0;
	}
	return 1;
};

/*Function to erase data from for time-point child columns*/
VELOS.manageVisitsGrid.wipeNonChildColumns = function (myGrid, elCell, noIntervalId){
	if (noIntervalId == noTimePointdefined){
		VELOS.manageVisitsGrid.eraseDependent(myGrid,elCell,'months','',true);
		VELOS.manageVisitsGrid.eraseDependent(myGrid,elCell,'weeks','',true);
		VELOS.manageVisitsGrid.eraseDependent(myGrid,elCell,'days','',true);
		VELOS.manageVisitsGrid.eraseDependent(myGrid,elCell,'insertAfterInterval','',true);
		VELOS.manageVisitsGrid.eraseDependent(myGrid,elCell,'intervalUnit','',true);
		VELOS.manageVisitsGrid.eraseDependent(myGrid,elCell,'insertAfter','',true);
		VELOS.manageVisitsGrid.eraseDependent(myGrid,elCell,'insertAfterId','',true);
	}
	if (noIntervalId == fixedPointdefined){
		VELOS.manageVisitsGrid.eraseDependent(myGrid,elCell,'insertAfterInterval','',true);
		VELOS.manageVisitsGrid.eraseDependent(myGrid,elCell,'intervalUnit','',true);
		VELOS.manageVisitsGrid.eraseDependent(myGrid,elCell,'insertAfter','',true);
		VELOS.manageVisitsGrid.eraseDependent(myGrid,elCell,'insertAfterId','',true);

		VELOS.manageVisitsGrid.resetCellColor(myGrid,elCell,'months');
		VELOS.manageVisitsGrid.resetCellColor(myGrid,elCell,'weeks');
		VELOS.manageVisitsGrid.resetCellColor(myGrid,elCell,'days');
	}
	if (noIntervalId == depnPointdefined){
		VELOS.manageVisitsGrid.eraseDependent(myGrid,elCell,'months','',true);
		VELOS.manageVisitsGrid.eraseDependent(myGrid,elCell,'weeks','',true);
		VELOS.manageVisitsGrid.eraseDependent(myGrid,elCell,'days','',true);

		VELOS.manageVisitsGrid.resetCellColor(myGrid,elCell,'insertAfterInterval');
		VELOS.manageVisitsGrid.resetCellColor(myGrid,elCell,'intervalUnit');
		VELOS.manageVisitsGrid.resetCellColor(myGrid,elCell,'insertAfter');
		VELOS.manageVisitsGrid.resetCellColor(myGrid,elCell,'insertAfterId');
	}
};

VELOS.manageVisitsGrid.processColumns = function (respJ,colArray){
	myColumnDefs = [];
	var isIe = jQuery.browser.msie;	
	if (colArray) {
		var tempColumnDefs =[];
		var f;
		myColumnDefs = colArray;
		tempColumnDefs = colArray;
		for (var iX=0; iX<myColumnDefs.length; iX++) {
			
			
			myColumnDefs[iX].width = myColumnDefs[iX].key=='interval' ? (isIe==true?80:50):(isIe==true?80:50);
			VELOS.manageVisitsGrid.cellEditors(myColumnDefs[iX]);
			
			
			//Set the width of the Interval B children's columns
			if(myColumnDefs[iX].key=='intervalB'){
				
				var childrenDefs=myColumnDefs[iX].children;
				myColumnDefs[iX].halign="center";
				for (var i=0; i<childrenDefs.length; i++) {
					childrenDefs[i].width = 
					(childrenDefs[i].key=='insertAfterInterval')?(isIe==true?50:40):(childrenDefs[i].key=='intervalUnit')?(isIe==true?50:50):
					(childrenDefs[i].key=='afterText')? (isIe==true?35:30):(childrenDefs[i].key == 'insertAfter')  ?(isIe==true?80:70):(isIe==true?80:70);
					myColumnDefs[iX].width+=childrenDefs[i].width;
					VELOS.manageVisitsGrid.cellEditors(childrenDefs[i]);
					if (childrenDefs[i].key == 'intervalUnit') {
						childrenDefs[i].editor = new YAHOO.widget.DropdownCellEditor({dropdownOptions:respJ.intervalUnitList,disableBtns:true});
					}
					if (childrenDefs[i].key == 'insertAfter') {
						childrenDefs[i].editor = new YAHOO.widget.DropdownCellEditor({dropdownOptions:respJ.insertAftVisitNames,disableBtns:true});
					}
				}
			}
			
			//Set the width of the 'Visit Window' children's columns
			if(myColumnDefs[iX].key=='visitWindow'){
				myColumnDefs[iX].halign="center";
				myColumnDefs[iX].width=(isIe==true?90:0);
				var childrenDefs=myColumnDefs[iX].children;
				for (var i=0; i<childrenDefs.length; i++) {
					childrenDefs[i].width =(isIe==true?80:100);
					if(childrenDefs[i].key=='afterLabel'){
						var afterChildrenDefs=childrenDefs[i].children;
						for (var j=0; j<afterChildrenDefs.length; j++) {
							afterChildrenDefs[j].width = afterChildrenDefs[j].key=='afternum' ? (isIe==true?25:5):(isIe==true?25:5);
							//myColumnDefs[iX].width+=afterChildrenDefs[j].width;
							VELOS.manageVisitsGrid.cellEditors(afterChildrenDefs[j]);
							if (afterChildrenDefs[j].key == 'durUnitB') {
								afterChildrenDefs[j].width = (isIe==true?55:30);
								afterChildrenDefs[j].editor = new YAHOO.widget.DropdownCellEditor({dropdownOptions:respJ.durUnitAList,disableBtns:true});
							}
						}
						
					}
                    if(childrenDefs[i].key=='beforeLabel'){
                    	var beforeChildrenDefs=childrenDefs[i].children;
						for (var j=0; j<beforeChildrenDefs.length; j++) {
							beforeChildrenDefs[j].width = beforeChildrenDefs[j].key=='beforenum' ?(isIe==true?25:5):(isIe==true?25:5);
							//myColumnDefs[iX].width+=beforeChildrenDefs[j].width;
							VELOS.manageVisitsGrid.cellEditors(beforeChildrenDefs[j]);
							if (beforeChildrenDefs[j].key == 'durUnitA') {
								beforeChildrenDefs[j].width = (isIe==true?55:30);
								beforeChildrenDefs[j].editor = new YAHOO.widget.DropdownCellEditor({dropdownOptions:respJ.durUnitBList,disableBtns:true});
							}
						}
						
					}
					
				}
			}			
			
			//Set the width of the 'Visit Window' children's columns
			if(myColumnDefs[iX].key=='intervalA'){
				myColumnDefs[iX].halign="center";
				var childrenDefs=myColumnDefs[iX].children;
				for (var i=0; i<childrenDefs.length; i++) {
					childrenDefs[i].width= 
					(childrenDefs[i].key=="months" )?(isIe==true?50:30):( childrenDefs[i].key=="weeks")?(isIe==true?50:30):(isIe==true?50:30);	
					//Ak:Fixed BUG#7135
					myColumnDefs[iX].width+=childrenDefs[i].width;
					VELOS.manageVisitsGrid.cellEditors(childrenDefs[i]);
					
				}
			}
			if (myColumnDefs[iX].key == 'visitSeq') { // SM:Bug#9308
				myColumnDefs[iX].madeUp = true;
				myColumnDefs[iX].halign="middle";
				myColumnDefs[iX].width = (isIe==true?50:30);
			}
			if (myColumnDefs[iX].key == 'copyVisit') { //Ak:Added for enhancement PCAL-20801
				myColumnDefs[iX].madeUp = true;
				myColumnDefs[iX].halign="left";
				myColumnDefs[iX].width = (isIe==true?40:20);
			}
			if (myColumnDefs[iX].key == 'delete') { //Ak:Added for enhancement PCAL-20353
				myColumnDefs[iX].madeUp = true;
				myColumnDefs[iX].halign="left";
				myColumnDefs[iX].width = (isIe==true?45:25);
			}
			if(calStatus=='O'){  //Ak:Added for enhancement PCAL-20354
				if (myColumnDefs[iX].key == 'hideFlag') {
					myColumnDefs[iX].madeUp = true;
					myColumnDefs[iX].halign="left";
					myColumnDefs[iX].width = (isIe==true?70:50);
				}
				if (myColumnDefs[iX].key == 'noInterval') {
					myColumnDefs[iX].editor = new YAHOO.widget.DropdownCellEditor({dropdownOptions:respJ.timePointListDesc,disableBtns:true});
					//myColumnDefs[iX].formatter = 'checkbox';
					myColumnDefs[iX].width = (isIe==true?90:80);
				} 
			
				if (myColumnDefs[iX].key == 'visitName') {
					myColumnDefs[iX].halign="left";
					myColumnDefs[iX].width = (isIe==true?80:65);
				}
				if (myColumnDefs[iX].key == 'description') {
					myColumnDefs[iX].halign="left";
					myColumnDefs[iX].width = (isIe==true?80:65);
				}
				
				
			}else if(calStatus=='A' || calStatus=='D'){
				if (myColumnDefs[iX].key == 'noInterval') {
					myColumnDefs[iX].editor = new YAHOO.widget.DropdownCellEditor({dropdownOptions:respJ.timePointListDesc,disableBtns:true});
					//myColumnDefs[iX].formatter = 'checkbox';
					myColumnDefs[iX].width = (isIe==true?150:130);
				} 
			
				if (myColumnDefs[iX].key == 'visitName') {
					myColumnDefs[iX].halign="left";
					myColumnDefs[iX].width = (isIe==true?125:115);
				}
				if (myColumnDefs[iX].key == 'description') {
					myColumnDefs[iX].halign="left";
					myColumnDefs[iX].width = (isIe==true?130:115);
				}
			}else{
				if (myColumnDefs[iX].key == 'noInterval') {
					myColumnDefs[iX].editor = new YAHOO.widget.DropdownCellEditor({dropdownOptions:respJ.timePointListDesc,disableBtns:true});
					//myColumnDefs[iX].formatter = 'checkbox';
					myColumnDefs[iX].width = (isIe==true?120:120);
				} 
			
				if (myColumnDefs[iX].key == 'visitName') {
					myColumnDefs[iX].halign="left";
					myColumnDefs[iX].width = (isIe==true?100:80);
				}
				if (myColumnDefs[iX].key == 'description') {
					myColumnDefs[iX].halign="left";
					myColumnDefs[iX].width = (isIe==true?100:80);
				}
			}
			
			myColumnDefs[iX].resizeable = true;
			myColumnDefs[iX].sortable = false;	
		}
	}
	return myColumnDefs;
	
}

/**
 * Method added to customize the fields.
 */

VELOS.manageVisitsGrid.types = {
		'varchar':{
			editor:'textbox',
			editorOptions:{disableBtns:true},
			formatter:'varchar'
		},
		'date': {
			parser:'date',
			formatter:'date',
			editor:'date',
			// I don't want the calendar to have the Ok-Cancel buttons below
			editorOptions:{
				disableBtns:false,
				validator: YAHOO.widget.DataTable.validateDate
			},
			// I want to send the dates to my server in the same format that it sends it to me
			// so I use this stringer to convert data on the way from the client to the server
			stringer: function (date) {
				return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() +
					' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
			}
		},
		integer: {
			formatter:'integer',
			parser:'number',
			// I am using my own RegularExpressionCellEditor, defined below
			editor:'regexp',
			// I'm only accepting digits (probably this data type should be called integer)
			editorOptions: {
				disableBtns:true,
				regExp:'^\\d{0,11}$'
				//validator: YAHOO.widget.DataTable.validateNumber
			}
		},
		number: {
			formatter:'number',
			parser:'number',
			// I am using my own RegularExpressionCellEditor, defined below
			editor:'regexp',
			// I'm only accepting digits (probably this data type should be called integer)
			editorOptions: {
				disableBtns:true,
				regExp:'^\\d{0,11}([.,]\\d{0,2})?$'
				//regExp:'^\\d{11}$',
				//validator: YAHOO.widget.DataTable.validateNumber
			}
		},
		currency: {
			parser:'number',
			formatter:'currency',
			editor:'regexp',
			// for currency I accept numbers with up to two decimals, with dot or comma as a separator
			// It won't accept currency signs and thousands separators really messes things up
			editorOptions: {
				disableBtns:true,
				regExp:'^\\d*([.,]\\d{0,2})?$',
				validator: function (value) {
					return parseFloat(value.replace(',','.'));
				}
			},
			// When I pop up the cell editor, if I don't change the dots into commas, the whole thing will look inconsistent.
			cellEditorFormatter: function(value) {
				return ('' + value).replace('.',',');
			},
			disableBtns:true
		},
		// This is the default, do-nothing data type.
		// There is no need to parse, format or stringify anything and if an editor is needed, the textbox editor will have to do.
		string: {
			editor:'textbox'
		}
	};

/**
 * Add Rows function
 */
VELOS.manageVisitsGrid.addRows = function(intervalval,intervalId) {
	var count = eval(YAHOO.util.Dom.get("rowCount").value);
	
	if(YAHOO.lang.isNumber(count)) {
		var myArray = [];
		for(var i=0;i<count;i++) {
			visitSeq++;
			var dataJSON = {"visitSeq":visitSeq,"visitId":'0',"visitName":"","delete":"false","description":'',"durUnitA":'Days',"durUnitB":'Days',
					"noInterval":intervalval,"noIntervalId":intervalId,"noTimePointId":intervalId,"months":'',"weeks":'',"days":'',"insertAfterInterval":'',"afterText":L_After,
					"insertAfter":'',"beforenum":'0',"afternum":'0',"intervalUnit":'',"insertAfterId":'',"isFakeVisit":'',"isRealFakeVisit":'',"fromCopyVisitId":'0',"hideFlag":'0',"offlineFlag":'0'};
			var record = YAHOO.widget.DataTable._cloneObject(dataJSON);
			record.row = i;
			myArray.push(record);
			manageVisitGridChangeList['visitSeq'+visitSeq] = 'Added';    
		}
		myDataTable.addRows(myArray);
		if (count>0) {//AK:Added for enhancement PCAL-20353 and PCAL-20354
			VELOS.manageVisitsGrid.applyRowAttribs(myDataTable,count);
		}
		
		YAHOO.util.Dom.get("rowCount").value = "0";
		return;
	}
	YAHOO.log(CldNtCont_InvldIndx);/*YAHOO.log("Could not continue due to invalid index.");*****/
	YAHOO.util.Dom.get("rowCount").value = "0";
	}
/**
 * Apply attribute when calendar status is offline
 */
VELOS.manageVisitsGrid.applyRowAttribs = function(myGrid, rCount) {  //AK:Added for enhancement PCAL-20353 and PCAL-20354
	var tabLength= myGrid.getRecordSet().getLength();

	for (var iX=(tabLength-rCount); iX<tabLength; iX++) {
		var visitCells= $D.getElementsByClassName('yui-dt-col-visitName', 'td');
		var el = new YAHOO.util.Element(visitCells[iX]);
		var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
		var parentId = parent.get('id');
		
		var intervalTd = parent.getElementsByClassName('yui-dt-col-interval', 'td')[0];
		jQuery(intervalTd).removeClass("yui-dt-asc");
		jQuery(intervalTd).removeClass("yui-dt-desc");
		jQuery(intervalTd).addClass("yui-dt-highlighted");
		
		var visitSeqTd = parent.getElementsByClassName('yui-dt-col-visitSeq', 'td')[0];
		var visitSeqEl = new YAHOO.util.Element(visitSeqTd);
		var tempvisitSeq = visitSeqEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;

		var delButtonTd = parent.getElementsByClassName('yui-dt-col-delete', 'td')[0];
		var delButtonEl = new YAHOO.util.Element(delButtonTd);
		var delButton = delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		delButtonEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML
		= '<div align="center"><img src="../images/delete.png" title="'+Delete_Row/*Delete Row*****/+'" border="0"'
		+ ' name="delete_e'+tempvisitSeq+'" id="delete_e'+tempvisitSeq+'" /></div> ';	
		var copyVisitTd = parent.getElementsByClassName('yui-dt-col-copyVisit', 'td')[0];
		var copyVisitEl = new YAHOO.util.Element(copyVisitTd);
		copyVisitEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML
		= '<div align="center"><img src="../images/copy.png" title="'+CopyVisit+'" border="0"'
		+ ' name="copy_e'+tempvisitSeq+'" id="copy_e'+tempvisitSeq+'"/> </div>';

		if(calStatus=='O'){
			var hideFlagTd = parent.getElementsByClassName('yui-dt-col-hideFlag', 'td')[0];
			var hideFlagEl = new YAHOO.util.Element(hideFlagTd);
			var hideFlag = hideFlagEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
	
			hideFlagEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML
			= '<div align="center"><img src="../images/hide.png" title="'+Hide_Visit/*Hide Visit*****/+'" border="0"'      //Akshi:Fixed bug#7710
			+ ' name="hideFlag_e'+tempvisitSeq+'" id="hideFlag_e'+tempvisitSeq+'"/></div> ';
		}
		var noIntervalIdTd = parent.getElementsByClassName('yui-dt-col-noIntervalId', 'td')[0];
		var noIntervalIdEl = new YAHOO.util.Element(noIntervalIdTd);
		var noIntervalId = noIntervalIdEl.getElementsByClassName('yui-dt-liner', 'div')[0].innerHTML;
		VELOS.manageVisitsGrid.wipeNonChildColumns(myGrid, el, noIntervalId);
	}
}

/**
 * Added method to set the editor for all columns
 */

VELOS.manageVisitsGrid.cellEditors = function(tempColumnsDef) {	
	if (tempColumnsDef.type!=undefined){
		var type= tempColumnsDef.type;
	 	var typeInfo =VELOS.multiSpecimensGrid.types[type];
	 	if (typeInfo.parser!=undefined){
	 		tempColumnsDef.parser=typeInfo.parser;
	 	}
	 	if(tempColumnsDef.formatter==undefined){
	 	  if (typeInfo.formatter!=undefined)
	 		 tempColumnsDef.formatter=typeInfo.formatter;
	 	}
	 	if (typeInfo.stringer!=undefined)
	 		tempColumnsDef.stringer=typeInfo.stringer;
		if (typeInfo.editorOptions!=undefined){
			tempColumnsDef.editor=new YAHOO.widget.TextboxCellEditor(typeInfo.editorOptions);
		}
	
	}
}
/*
 * Method to maintain the list Added/updated/Deleted visits records.
 */

VELOS.manageVisitsGrid.auditAction = function(visitSeq, action,visitName,visitId){
	var change=manageVisitGridChangeList['visitSeq'+visitSeq];
	var prevAct ='';
	if(change == undefined){
		if(action=='Updated'){
			manageVisitGridChangeList['visitSeq'+visitSeq] = visitSeq;
			if (updateRows == null) updateRows = [];
			updateRows['visitSeq'+visitSeq] = visitSeq;
			updatedVisits['visitSeq'+visitSeq] = visitName;	
			 updateVisitIds['visitSeq'+visitSeq] = visitId;
		}else{
			manageVisitGridChangeList['visitSeq'+visitSeq] = visitSeq;
		}
	}else{
	//error rectification
		if(isNaN(change)){
			var pos =change.indexOf(' ');
			if (pos>0){
				prevAct = change.slice(0,pos);
			}else{
				prevAct = change;
			}
		}
		if(prevAct=='Added'){//AK:Fixed Bug#7169
			addedVisits['visitSeq'+visitSeq] = visitName ;
			manageVisitGridChangeList['visitSeq'+visitSeq] = visitSeq;
		}
	}
}


/**
 * Preview and Save button functionality
 */
VELOS.manageVisitsGrid.saveDialog = function(e){
	myDataTable.saveCellEditor();//Ak:Fixed Bug#6910
	if(formatterFail){//Bug#10298 : To focus to the cell on clicking preview and save.
		myDataTable.onEventShowCellEditor({target:selectedCell}); 
		if(e) YAHOO.util.Event.stopEvent(e);
		formatterFailTwo=false;
		formatterFailThree=false;
		return false;
	}
	
	var myGrid = myDataTable;		
	if(!VELOS.manageVisitsGrid.validate(myGrid)){
		if(e) YAHOO.util.Event.stopEvent(e);
		return false;
	}
	var regularExp =  new RegExp('visitSeq[0-9]+$');

	var saveDialog = document.createElement('div');	
	if (!$('saveDialog')) {
		saveDialog.setAttribute('id', 'saveDialog');
		saveDialog.innerHTML  = '<div class="hd" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; width=\"23em\"">'+CfmVst_Chg/*Confirm Visit Changes*****/+'</div>';
		saveDialog.innerHTML += '<div class="bd" id="insertForm" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt; overflow: visible;"></div>';
		$D.insertAfter(saveDialog, $('save_changes'));
	}

	var noChange = true;
	var numChanges = 0;
	myGridName = MngVsts/*'Manage Visits'*****/;
	var rows = 0;
		var updateJSON = "[" ; 
		var specVal ="";
			
		for (oKey in manageVisitGridChangeList) {
		if (oKey.match(regularExp) && manageVisitGridChangeList[oKey] != undefined) {	
		
		noChange = false;
			
			numChanges++;
			if(updateRows[oKey] != undefined){
				if(rows != 0){
					updateJSON += ",";
				}						
				updateJSON += "{\"i\":\""+updateRows[oKey]+"\"}";
				rows ++;
	
		}
	
		}
	}
		updateJSON += "]";
		//alert("The json for update:"   +updateJSON);		
		
		rows = 0;
		var deleteJSON = "[" ; 
		var specVal ="";
			
		for (oKey in manageVisitGridChangeList) {
		if (oKey.match(regularExp) && manageVisitGridChangeList[oKey] != undefined) {	
		
		noChange = false;		
		
			if(deleteRows[oKey] != undefined){
				if(rows != 0){
					deleteJSON += ",";
				}						
				deleteJSON += "{\"i\":\""+deleteRows[oKey]+"\"}";
				rows ++;
	
		}
	
		}
	}
		deleteJSON += "]";
		//alert("The json for delete:"   +deleteJSON);		
	
		
		
	var maxChangesDisplayedBeforeScroll = 25;
		$('insertForm').innerHTML = '<form id="dialogForm" method="POST" action="updateManageVisits.jsp?'+incomingUrlParams+'"><div id="changeRowsContent"></div><br/><div id="insertFormContent"></div></form>';
	
		
		var countAddedUpdated = 0;
			for (oKey in addedVisits) {
				if (oKey.match(regularExp) && addedVisits[oKey] != undefined) {
					countAddedUpdated++;
					 if(countAddedUpdated ==1){
						 $('changeRowsContent').innerHTML +='<b> '+Vsts_Added/*Visits Added*****/+' </b><br/>'; 
					 }
					noChange = false;
					$('changeRowsContent').innerHTML += addedVisits[oKey]+'<br/>';					
				}
			}
			countAddedUpdated = 0;			
		
			for (oKey in updatedVisits) {				
				if (oKey.match(regularExp) && updatedVisits[oKey] != undefined) {
					countAddedUpdated++;
					 if(countAddedUpdated ==1){
						 $('changeRowsContent').innerHTML += '<b> '+Vst_Updt/*Visits Updated*****/+' </b><br/>'; 
					 }
					noChange = false;
					$('changeRowsContent').innerHTML += updatedVisits[oKey]+'<br/>';					
				}
			}
			//Ak:Fixed BUG#7195
			countAddedUpdated = 0;	
			
			for (oKey in deletedVisits) {
				if (oKey.match(regularExp) && deletedVisits[oKey] != undefined) {
					countAddedUpdated++;
					 if(countAddedUpdated ==1){
						 $('changeRowsContent').innerHTML += '<b> '+Visits_Deleted/*Visits Deleted*****/+' </b> <br/>'; 
					 }
					noChange = false;
					$('changeRowsContent').innerHTML += deletedVisits[oKey]+'<br/>';					
				}
			}
			countAddedUpdated = 0;	
	
		if (numChanges > 0){
			var paramArray = [myGridName];
			$('changeRowsContent').innerHTML = '<b>'+getLocalizedMessageString("L_For_Cl",paramArray)/*For '+ myGridName + '*****/+':</b><br/>' + $('changeRowsContent').innerHTML;			
			specVal = yuiDataTabletoJSON(myGrid);			
			specVal = htmlEncode(specVal);
			updateJSON = htmlEncode(updateJSON);	
			deleteJSON = htmlEncode(deleteJSON);	
			$('insertFormContent').innerHTML += '<input type="hidden" name="myVisitGrid" value="'+specVal+'" />';
			$('insertFormContent').innerHTML += '<input type="hidden" name="updateInfo" value="'+updateJSON+'" />';		
			$('insertFormContent').innerHTML += '<input type="hidden" name="deleteInfo" value="'+deleteJSON+'" />';
			
		}
		
		

	if (numChanges < maxChangesDisplayedBeforeScroll) {
		$('insertForm').style.width = null;
		$('insertForm').style.height = null;
		$('insertForm').style.overflow = 'visible';
	} else {
		$('insertForm').style.width = '51em';
		$('insertForm').style.height = '35em';
		$('insertForm').style.overflow = 'scroll';
	}
	if (noChange) {
		$('insertFormContent').innerHTML += '<table width="220px"><tr><td style="font-size:8pt;">&nbsp;&nbsp;'+ThereNoChg_Save/*There are no changes to save.*****/+'&nbsp;&nbsp;</td></tr></table>';
		noChange = true;
	} else {
		var paramArray = [numChanges];
		$('insertFormContent').innerHTML += getLocalizedMessageString("M_TotAddOrDel_Rows",paramArray)/*'Total added/updated/deleted row(s): '+numChanges*****/+'<br/><table><tr><td width="150px"><span id="eSignMessage" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;"></span></td>'+
			'<td style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:8pt;">'+Esignature/*e-Signature*****/+' <FONT class="Mandatory">*</FONT>&nbsp</td>'+
			'<td><input type="password" name="eSign" id="eSign" maxlength="8" '+
			' onkeyup="ajaxvalidate(\'misc:\'+this.id,4,\'eSignMessage\',\''+Valid_Esign/*Valid e-Sign*****/+'\',\''+Invalid_Esign/*Invalid e-Sign*****/+'\',\'sessUserId\')" '+
				' onkeypress="return VELOS.multiSpecimensGrid.fnOnceEnterKeyPress(event)" '+
			' />&nbsp;</td></tr></table>';		
	}
		if($('insertFormContentOne')){
			$('insertFormContentOne').innerHTML =""; 
		}
		
		var myDialog = new YAHOO.widget.Dialog('saveDialog',
			{
				visible:false, fixedcenter:true, modal:true, resizeable:true,
				draggable:"true", autofillheight:"body", constraintoviewport:false
			});
	var handleCancel = function(e) {
		myDialog.cancel();
	};
	var handleSubmit = function(e) {
		try {
			if ('' == document.getElementById('eSignMessage').innerHTML) {
				alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
        		return false;
			}
			//Changes By Sudhir On 3/30/2012 for Bug # 5796
			if (Valid_Esign != document.getElementById('eSignMessage').innerHTML) {/*if ('Valid e-Sign' != document.getElementById('eSignMessage').innerHTML) {*****/
        		alert(document.getElementById('eSignMessage').innerHTML);
        		return false;
        	}
			//Changes By Sudhir On 3/30/2012 for Bug # 5796
	        var thisTimeSubmitted = new Date();
	        if (lastTimeSubmitted && thisTimeSubmitted) {
	        	if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) { return false; }
		        lastTimeSubmitted = thisTimeSubmitted;
	        }
		} catch(e) {}
		myDialog.submit();
	};
	var onButtonsReady = function() {
	    if (!$E.getListeners($('dialog_submit'))) { $E.addListener($('dialog_submit'), 'click', handleSubmit, false, true); }
	    if (!$E.getListeners($('dialog_cancel'))) { $E.addListener($('dialog_cancel'), 'click', handleCancel); }
	}
	YAHOO.util.Event.onContentReady("saveDialog", onButtonsReady);
	var myButtons = noChange ?
		[   { text: getLocalizedMessageString("L_Close"), handler: handleCancel } ] :
		[
			{ text: getLocalizedMessageString('L_Save'),   handler: handleSubmit },
			{ text: getLocalizedMessageString('L_Cancel'), handler: handleCancel }
		];
		
	var onSuccess = function(o) {
		hideTransitPanel();
		var respJ = null; // response in JSON format
		try {
			respJ = $J.parse(o.responseText);
			
		} catch(e) {
			// Log error here
			return;
		}
			 //alert("Your data was successfully submitted. The response was: " + respJ.resultMsg);	
			showFlashPanel(respJ.resultMsg);			
			if (respJ.result == 0) {	
				if (window.parent.reloadManageVisitSGrid()) { setTimeout('window.parent.reloadManageVisitSGrid()', 1500); }
				
			}		
	};
	var onFailure = function(o) {
		hideTransitPanel();
		var paramArray = [o.status];
		alert(getLocalizedMessageString("M_CommErr_CnctVelosSupp",paramArray));/*alert("Communication Error. Please contact Velos Support: " + o.status);*****/
	};
	var onStart = function(o) {
		showTransitPanel(PleaseWait_Dots);/*showTransitPanel('Please wait...');*****/
	}
	myDialog.callback.customevents = { onStart:onStart };
	myDialog.callback.success = onSuccess;
	myDialog.callback.failure = onFailure;
	myDialog.cfg.queueProperty("buttons", myButtons);
	myDialog.render(document.body);
	if (!formatterFail){
		myDialog.show();
		if (document.getElementById('eSign')) { document.getElementById('eSign').focus(); }
	}

	var showFlashPanel = function(msg) {
		// Initialize the temporary Panel to display while waiting for external content to load
		if (!(VELOS.flashPanel)) {
			VELOS.flashPanel =
				new YAHOO.widget.Panel("flashPanel",
					{ width:"240px",
					  fixedcenter:true,
					  close:false,
					  draggable:false,
					  zindex:4,
					  modal:true,
					  visible:false
					}
				);
		}
		VELOS.flashPanel.setHeader("");
		VELOS.flashPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
		VELOS.flashPanel.render(document.body);
		VELOS.flashPanel.show();
		setTimeout('VELOS.flashPanel.hide();', 6000);
	}

	var showTransitPanel = function(msg) {
		if (!VELOS.transitPanel) {
			VELOS.transitPanel =
				new YAHOO.widget.Panel("transitPanel",
					{ width:"240px",
					  fixedcenter:true,
					  close:false,
					  draggable:false,
					  zindex:4,
					  modal:true,
					  visible:false
					}
				);
		}
		VELOS.transitPanel.setHeader("");
		VELOS.transitPanel.setBody('<table align="center"><tr><td style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size:8pt;"><b>'+msg+'</b></td></tr><table>');
		VELOS.transitPanel.render(document.body);
		VELOS.transitPanel.show();
	}

	var hideTransitPanel = function() {
		if (VELOS.transitPanel) { VELOS.transitPanel.hide(); }
	}
	
	$j(function() {
		if ($j("input:submit")){
			$j( "input:submit").button();
		}
		if ($j("a:submit")){
			$j( "a:submit").button();
		}
		if ($j("button")){
			$j("button").button();
		}
	});	
	//}
}
//End of saveDialog method
/**
 * Added to validate offline calendar day zero visit case
 * Fixed Bug 7226,BK,OCT/18/2011
 */
VELOS.manageVisitsGrid.validateDisp= function(oRecord,OfflineFlag){
	var insertAfterInterval = parseInt(oRecord.getData("insertAfterInterval"));
	if (isNaN(insertAfterInterval) == true) insertAfterInterval = 0;
	var seqNo=oRecord.getData("visitSeq");
	var unit = oRecord.getData("intervalUnit");
	var insertAfterIds = oRecord.getData("insertAfterId");	
	insertAfterIds=insertAfterIds==undefined ?'':insertAfterIds;
	var pos = insertAfterIds.indexOf('/');
	var prevDisp = 0,interval = 0;
	var depVisit,depInterval,displacement;	
	if(pos>0){
	 depVisit = insertAfterIds.substring(0,insertAfterIds.indexOf("/"));
     depInterval =  insertAfterIds.substring(insertAfterIds.indexOf("/")+1,insertAfterIds.length);
	prevDisp = parseInt(depInterval);	
	}
	 //FIX#7010		 
	if (unit==L_Months)
		interval = insertAfterInterval *30;
	else if (unit==L_Weeks)
		interval = insertAfterInterval *7;
	else //days
		interval = insertAfterInterval;
	displacement = prevDisp + interval;
	   if(displacement == 0 && OfflineFlag ==0){    //AK:Fixed Bug#7633(4thJan2012) //AK:Fixed bug#7468
	   alert(Row+" #"+seqNo+":"+AddSetAft_OfflineZeroVst);
         /*alert("Adding a visit using \'Set After'' option leads to a \'Day Zero(0)\' visit. " +
	   		"\'Day Zero(0)\' visit can not be added for \'Offline for editing\' calendar.\nPlease specify some other " +
	   		"visit interval to avoid \'Day Zero(0)\' visit.");*****/
	   return false;
 }
	   return true;
	
}
//AK:Added for BUG#7635
VELOS.manageVisitsGrid.validateChildZeroDays= function(oRecord,visitId,calDisplacement,OfflineFlag,arrRecords){
	var seqNo=oRecord.getData("visitSeq");
	for(rec=0; rec < arrRecords.getLength(); rec++){
		var prevDisp = 0,interval = 0;
		oRecordTemp = arrRecords.getRecord(rec);
		var insertAfterInterval = parseInt(oRecordTemp.getData("insertAfterInterval"));
		if (isNaN(insertAfterInterval) == true) insertAfterInterval = 0;
		insertAfterVal = oRecordTemp.getData("insertAfter");
			if(insertAfterVal!=null && insertAfterVal!=""){
				var insertAfterInterval =oRecordTemp.getData("insertAfterInterval");
				var insertAfterIds = oRecordTemp.getData("insertAfterId");
				var unit = oRecordTemp.getData("intervalUnit");
				var pos = insertAfterIds.indexOf('/');
				tempinsertAfterIds =  insertAfterIds.substring(0,insertAfterIds.indexOf("/"));
					if(visitId==tempinsertAfterIds){
						var calInterval=parseInt(calDisplacement)+parseInt(insertAfterInterval);
						if(calInterval==0 && unit==L_Days ){
							 alert(Row+" #"+seqNo+":"+ParentChild_OfflineZeroVst);
							return false;
						
						}
					}
			}
		
	}
	   return true;	
}
/**
 * Added to validate offline calendar day zero visit case
 * Fixed Bug 7226,7228,BK,OCT/18/2011
 */
VELOS.manageVisitsGrid.checkUpdatedVisit= function(visitId){
	var regularExp =  new RegExp('visitSeq[0-9]+$');
	for (oKey in updateVisitIds ) {	
		if(oKey.match(regularExp) && updateVisitIds[oKey] != undefined){
			if (updateVisitIds[oKey] == visitId){
				return true;
			}
		}	
	}
		return false;
}
/**
 * Added to calculateDisplacement for interval 1
 * Fixed Bug 7227,7228,BK,OCT/18/2011
 */
VELOS.manageVisitsGrid.calculateDisp= function(weeks,months,days){	
	if (isNaN(months) == true) months = 0;
	if (isNaN(weeks) == true) weeks = 0;
	if (isNaN(days) == true) days = 0;
	if (months == 0) months++;
	if (weeks == 0) weeks++;
	if (days == 0) days++;	
	var displacement = 0;
	if (months > 0) displacement+= (months -1)*30;
	if (weeks > 0) displacement+= (weeks -1)*7;
	displacement += days; 
	return displacement;
}
/**
 * Added method to validate Interval
 */
VELOS.manageVisitsGrid.validateInterval= function(myGrid,month, week, day,visitSeq,visitId,oRecord,offlineFlg){	
	var monthvalid = true;
	var weekvalid = true;
	var dayvalid = true;
	var ret = 0;
	var m = parseInt(month);
	var w = parseInt(week);
	var d = parseInt(day);

	if  (m <= 0) monthvalid = false;
	if  (w <= 0) weekvalid = false;
	
	if ( (m == 0) && (w == 0) && (d == 0)) {
		var paramArray = [visitSeq];
		alert(getLocalizedMessageString("M_MthOrWkEtr_RowVld",paramArray));/*alert("Month/Day/week entered for row #"+visitSeq+" is not valid");*****/
		VELOS.manageVisitsGrid.fnShowCellEditor(rec,"months");
        return false;
	}

	if( d==0 && (w > 1 || m >1)){
	   alert (DayValid_FirstWeek);/*alert ("Day 0 interval is only valid for first week of first month");*****/
	   VELOS.manageVisitsGrid.fnShowCellEditor(rec,"days");
	   return false;
    }	
	if(( d==0 || (VELOS.manageVisitsGrid.calculateDisp(w,m,d) == 0))&& calStatus == 'O' && (visitId =='0' 
		|| VELOS.manageVisitsGrid.checkUpdatedVisit(visitId)) && offlineFlg=='0' ){
		var paramArray = [visitSeq, offlineStatusDesc]; //AK:Fixed BUG#7639 and BUG#7634
		alert(getLocalizedMessageString("M_DayIntr_CntOfflineCal",paramArray));
		VELOS.manageVisitsGrid.fnShowCellEditor(rec,"days");
		return false;
	}
	if (!monthvalid) {
		var paramArray = [visitSeq];
		alert(getLocalizedMessageString("M_MthEtr_RowNtVld",paramArray));/*alert("Month entered for row #"+visitSeq+" is not valid");*****/
		VELOS.manageVisitsGrid.fnShowCellEditor(rec,"months");
		return false;
	}
	//FIX #6938 6936 BK AUG-29-2011
	if (isNaN(m) == true) m = 0;
	if (isNaN(w) == true) w = 0;
	if (isNaN(day) == true) {
	 if (!(isNegNum(day) ==true)) d=0;
	}
	if (!weekvalid) {
		var paramArray = [visitSeq];
		alert(getLocalizedMessageString("M_WkEtr_RowNtVld",paramArray));/*alert("Week entered for row #"+visitSeq+" is not valid");*****/
		VELOS.manageVisitsGrid.fnShowCellEditor(rec,"weeks");
		return false;
	}

	if (!dayvalid) {
		var paramArray = [visitSeq];
		alert(getLocalizedMessageString("M_DayEtr_RowNtVld",paramArray));/*alert("Day entered for row #"+visitSeq+" is not valid");*****/
		VELOS.manageVisitsGrid.fnShowCellEditor(rec,"days");
		return false;
	}

	if ( (m > 0) && (w == 0) && (d > 30)) {
		var paramArray = [visitSeq];
		alert(getLocalizedMessageString("M_DayEtrRow_CntMonEtr",paramArray));/*alert("Days entered for row #"+visitSeq+" cannot be more than 30 when month is entered");*****/		
		VELOS.manageVisitsGrid.fnShowCellEditor(rec,"days");
		return false;
	}

	if (m > 0) {
		if (w > 4) {
			var paramArray = [visitSeq];
			alert(getLocalizedMessageString("M_WkEtrRow_CntMonEtr",paramArray));/*alert("Weeks entered for row #"+visitSeq+" cannot be more than 4 when month is entered");*****/			
			VELOS.manageVisitsGrid.fnShowCellEditor(rec,"weeks");
			return false;
		}

	}
	if (w > 0) 	{
		if ( d > 7) {
			var paramArray = [visitSeq];
			alert(getLocalizedMessageString("M_DayEtrRow_CntWkEtr",paramArray));/*alert("Days entered for row #"+visitSeq+" cannot be more than 7 when week is entered");*****/			
			VELOS.manageVisitsGrid.fnShowCellEditor(rec,"days");
			return false;
		}
	}

	return true;
	
}
/**
 * Added method to validate circular relations in dependent visits
 */
VELOS.manageVisitsGrid.findCircularRelations = function(myGrid, oRecordChild){
	var arrRecords = myGrid.getRecordSet();
	var whileParent = true;
	var raiseFlag = false;
	var myParentArray = [];
	var visitSeqProcess = oRecordChild.getData("visitSeq");
	var insertAfterVisitName = oRecordChild.getData("insertAfter");
	if(insertAfterVisitName==L_Select_AnOption)
	{
		return false;
	}
	while(whileParent){
		var visitSeqChild = oRecordChild.getData("visitSeq");
		var visitIdChild = oRecordChild.getData("visitId");
		var incompleteInfo = false;
		var insertAfterChild = oRecordChild.getData("insertAfter");
		if (!insertAfterChild || insertAfterChild == L_Select_AnOption) incompleteInfo = true;
		var insertAfterIdChild = oRecordChild.getData("insertAfterId");
		if (!insertAfterIdChild) incompleteInfo = true;
		insertAfterIdChild=insertAfterIdChild==undefined?'':insertAfterIdChild;
		var arrayInsertAfterId=insertAfterIdChild.split("/");
		insertAfterIdChild = arrayInsertAfterId[0];
		if (!insertAfterIdChild) incompleteInfo = true;
		if(incompleteInfo){
			whileParent = false;
			break;
		}
		if (whileParent){
			if (insertAfterIdChild == "-1"){
				//Fake Visit
				var fakeVisitChild = oRecordChild.getData("isFakeVisit");
				if (fakeVisitChild && fakeVisitChild != ""){
					var arrayFakeVisit=fakeVisitChild.split("/");
					fakeVisitChild = arrayFakeVisit[0];
					insertAfterIdChild  = arrayFakeVisit[2];
					if (insertAfterIdChild == "-1")
						whileParent = false;
				}else{
					whileParent = false;
				}
			}else{
				var displacementChild = parseInt(arrayInsertAfterId[1]);
				var insertAfterIntervalChild = oRecordChild.getData("insertAfterInterval");
			}
		}
		//whileParent = VELOS.manageVisitsGrid.getParentOfMe(myGrid, oRecordChild);
		if (whileParent){
			for(iPV=0; iPV < arrRecords.getLength(); iPV++){
				var oRecordParent = arrRecords.getRecord(iPV);
				var visitIdParent = oRecordParent.getData("visitId");
				var visitSeqParent = oRecordParent.getData("visitSeq");
				
				if (visitSeqChild != visitSeqParent){
					if (insertAfterIdChild == visitIdParent){
						//Match found
						var visitNameParent = oRecordParent.getData("visitName");
						if (myParentArray.indexOf(visitSeqParent) >= 0){
							raiseFlag = true;
							whileParent = false;
							var paramArray = [visitSeqChild, visitSeqParent];
							alert(getLocalizedMessageString("M_CircularRelationFound",paramArray));
						} else {
							var len = (myParentArray)? myParentArray.length:0;
							myParentArray[len] = visitSeqParent;
						}
						
						if (!raiseFlag){
							var noIntervalIdParent=oRecordParent.getData("noIntervalId");
							if (noIntervalIdParent == noTimePointdefined){
								//Last parent found
								iPV = arrRecords.getLength();
								whileParent = false;
							}else if (noIntervalIdParent == fixedPointdefined){
								//Last parent found
								iPV = arrRecords.getLength();
								whileParent = false;
							}else if (noIntervalIdParent == depnPointdefined){
								//There is parent left
								oRecordChild = oRecordParent;
								iPV = arrRecords.getLength();
								break;
							}
						}
					}						
				}
				if (raiseFlag){
					//Circular relation found
					iPV = arrRecords.getLength();			
					whileParent = false;
					break;
				}
			}
		}
	}
	
	if (raiseFlag){
		//Circular relation found
		return true;
	}
	return false;
}

/**
 * Added method to validate all manadatory fields
 */
VELOS.manageVisitsGrid.validate= function(myGrid){
	var arrRecords = myGrid.getRecordSet();
	for(rec=0; rec < arrRecords.getLength(); rec++){
		oRecord = arrRecords.getRecord(rec);	
		if (!VELOS.manageVisitsGrid.validateColumnContent(rec, oRecord, 'visitName', L_Visit_Name))
				return false;
		//Bk:Fixed Bug#6961,7170
		if(!VELOS.manageVisitsGrid.validateDuplicateName(myGrid,rec))  return false;
		oRecord = arrRecords.getRecord(rec);
		var noIntervalId=oRecord.getData("noIntervalId");
		var insertAfter=oRecord.getData("insertAfter");
		var insertAfterId=oRecord.getData("insertAfterId");
		insertAfterId=insertAfterId==undefined?'':insertAfterId;
		var visitId=insertAfterId=insertAfterId.split("/");
		var regularExp =  new RegExp('visitSeq[0-9]+$');
		for (oKey in deleteRows) {
			if (oKey.match(regularExp) && deleteRows[oKey] != undefined) {
				if(visitId[0]==deleteRows[oKey] || insertAfter==deletedVisits[oKey] )
				{
					var paramArray = [(oRecord.getData("visitSeq"))];
					alert(getLocalizedMessageString("M_EthrVNSelOrCorrect",paramArray));
					VELOS.manageVisitsGrid.fnShowCellEditor(rec,"insertAfter");
					return false;
				}
			}
		}
		
		
		
		
		
			
		var months=oRecord.getData("months");
		var weeks=oRecord.getData("weeks");
		var days=oRecord.getData("days");
		var insertAfterInterval=oRecord.getData("insertAfterInterval");
		var visitId = oRecord.getData("visitId");
		var visitName = oRecord.getData("visitName");
		var offlineFlg=oRecord.getData("offlineFlag"); //AK:Fixed Bug#7633
    	if (isWhitespace(months))
    		m = false;
		else
			m = true;
    	

		if (isWhitespace(weeks))
			w = false;
		else
			w=true; 
		
		
		if (isWhitespace(days))
			d = false;
		else
			d = true;

		if (isWhitespace(insertAfterInterval))
			i = false;
		else
			i = true;

		if (noIntervalId == noTimePointdefined){
			if(VELOS.manageVisitsGrid.checkChild(visitId,visitName)){
				var paramArray = [(oRecord.getData("visitSeq"))];
				alert(getLocalizedMessageString("M_VstNoIntvlDefine",paramArray));/*alert("For Row #{0}:Visit Interval ''No Time Point Defined'' can not be set for parent visit.");*****/
				VELOS.manageVisitsGrid.fnShowCellEditor(rec,"noInterval");
				return false; 
			}
		
		}else if (noIntervalId == fixedPointdefined){
			if ( !m && !w && !d) {
				var paramArray = [(oRecord.getData("visitSeq"))];
				alert(getLocalizedMessageString("M_FixedTimePoint_Columns",paramArray));/*alert("For row #"+ (rec+1)+", you have selected Time Point Type as 'Fixed Time Point'. Please complete all the 'Fixed Time Point' columns.");*****/
				//alert(getLocalizedMessageString("M_EtrMonWk_FmtOptRow",paramArray));/*alert("Enter interval in months/weeks/days or Insert After format or Select No Interval option for row #" + (rec+1));*****/
				var colName = (!m)? 'months' : (!w)? 'weeks' : 'days';
				VELOS.manageVisitsGrid.fnShowCellEditor(rec,colName);
				return false;
			}
			if ( m || w || d) {
				if(!VELOS.manageVisitsGrid.validateInterval(myGrid,months,weeks,days,oRecord.getData("visitSeq"),visitId,oRecord,offlineFlg)) //AK:Fixed BNug#7633
					return false;
			}
		}else if (noIntervalId == depnPointdefined){
		
			var p =oRecord.getData("insertAfterInterval"); 
			var intUnit =oRecord.getData("intervalUnit"); 
			var insAfter =oRecord.getData("insertAfter"); 
			if (isWhitespace(insertAfterInterval) || (p == '0') || isWhitespace(intUnit) || isWhitespace(insAfter)){
				var paramArray = [(oRecord.getData("visitSeq"))];
				alert(getLocalizedMessageString("M_DepTimePoint_Columns",paramArray));/*alert("For row #"+ (rec+1)+", you have selected Time Point Type as 'Dependent Time Point'. Please complete all the 'Dependent Time Point' columns.");*****/
				//alert(getLocalizedMessageString("M_EtrMonWk_FmtOptRow",paramArray));/*alert("Enter interval in months/weeks/days or Insert After format or Select No Interval option for row #" + (rec+1));*****/
				var colName = (isWhitespace(insertAfterInterval) || (p == '0'))? 'insertAfterInterval'
						: isWhitespace(intUnit) ? "intervalUnit" : "insertAfter";
				VELOS.manageVisitsGrid.fnShowCellEditor(rec,colName);
				return false;
			}
			var circularRelation = VELOS.manageVisitsGrid.findCircularRelations(myGrid, oRecord);
			if (circularRelation){
				return false;
			}

		}
		if ((visitId > 0 && (VELOS.manageVisitsGrid.checkUpdatedVisit(visitId)) && calStatus == 'O')){
			var calCulatedDisp=VELOS.manageVisitsGrid.calculateDisp(Number(weeks),Number(months),Number(days));
			 if(!VELOS.manageVisitsGrid.validateChildZeroDays(oRecord,visitId,calCulatedDisp,offlineFlg,arrRecords))  {
			 	return false;
			 }
		}
		//Ak:Fixed BUG#6950(5th Sept,2011)
		insertAfterVal = oRecord.getData("insertAfter");
		if(insertAfterVal != "" && i) { //Ak:Fixed Bug#6950
			var insertAfterIds = oRecord.getData("insertAfterId");
			insertAfterIds=insertAfterIds==undefined ?'':insertAfterIds;
			optVal = insertAfterIds.substring(insertAfterIds.indexOf("/")+1,insertAfterIds.length);
			if (optVal == '') {
				var paramArray = [(oRecord.getData("visitSeq"))];
				alert(getLocalizedMessageString("M_RowOptDefn_NoItrvlDef",paramArray));/*alert("For row #"+ (rec+1)+" option of Interval definition is not applicable when the Parent Visit has 'No Interval Defined'");*****/
				return false;
			}
		}
	}
	return true;
	
}

VELOS.manageVisitsGrid.fnShowCellEditor= function(rec,colName){
	var columnCells = $D.getElementsByClassName('yui-dt-col-'+colName, 'td', myDataTable.getTableEl());
	for (var iX=0; iX<columnCells.length; iX++) {
		if(rec == iX){
			var el = new YAHOO.util.Element(columnCells[iX]);
			var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
			var elCell = parent.getElementsByClassName('yui-dt-col-'+colName, 'td')[0];
			myDataTable.showCellEditor(elCell);
			return false;
		}
	}
	return false;
}
/**
 *  FIX BK #6961
 *  validating duplicate name.
 */
VELOS.manageVisitsGrid.validateDuplicateName= function(myGrid,rec){
	var regularExp =  new RegExp('visitSeq[0-9]+$');	
	var visitSeq;
	var rowNum;
	var myRecord;
	var oRecord;
	var depRec;
	var arrRecords = myGrid.getRecordSet();	
	for (oKey in manageVisitGridChangeList) {	
		//alert(deleteRows[key])
		if (oKey.match(regularExp) && manageVisitGridChangeList[oKey] != undefined ) {	
						myRecord = arrRecords.getRecord(rec);
		//Fixed:7020,BK,SEP/12/2011.		
						for(depRec = rec+1; depRec < arrRecords.getLength(); depRec++){
						 oRecord = arrRecords.getRecord(depRec);
						if (!VELOS.manageVisitsGrid.validateColumnContent(depRec, oRecord, 'visitName', L_Visit_Name))
								return false;
						if(oRecord.getData("visitName").toUpperCase() == myRecord.getData("visitName").toUpperCase()){
							var isModified=false;
							for (oKeyTemp in manageVisitGridChangeList) { //Ak:Added for BUG#7538
								if(oRecord.getData("visitSeq")== manageVisitGridChangeList[oKeyTemp]){
									isModified=true;
								    rowNum = oRecord.getData("visitSeq");
								    break;
								    }
							}
						
							if(!isModified){
								rowNum = rec+1;
							}

							var paramArray = [rowNum];
							alert(getLocalizedMessageString("M_RowVstDupl_ChgName",paramArray));/*alert("For Row #" +rowNum +":Visit Name is duplicate,Change the Name.");*****/
							VELOS.manageVisitsGrid.fnShowCellEditor(depRec,"visitName");
							return false;
						}	
					}
		}
	}	
	return true;
}
/**
 * Added method to validate all manadatory fields.
 */

VELOS.manageVisitsGrid.validateColumnContent = function(recCount, oRecord, columnKey, columnLabel) {
	var visitSequence = oRecord.getData('visitSeq');
	var columnContent = oRecord.getData(columnKey);	
	if(columnContent == undefined || columnContent.length == 0){
		var paramArray = [columnLabel,visitSequence];
		alert(getLocalizedMessageString("M_EtrFor_Row",paramArray));/*alert("Enter "+columnLabel+" for row #" + (recCount+1));*****/
		
		var columnCells = $D.getElementsByClassName('yui-dt-col-'+columnKey, 'td', myDataTable.getTableEl());
		for (var iX=0; iX<columnCells.length; iX++) {
			if(recCount == iX){
				var el = new YAHOO.util.Element(columnCells[iX]);
				var parent = new YAHOO.util.Element($D.getAncestorByTagName(el, 'tr'));
				var elCell = parent.getElementsByClassName('yui-dt-col-'+columnKey, 'td')[0];
				myDataTable.showCellEditor(elCell);
				return false;
			}
		}
		return false;
 	}
	return true;
}


VELOS.manageVisitsGrid.fnOnceEnterKeyPress = function(e) {
	try {
        if (e.keyCode == 13 || e.keyCode == 10) {
			if ('' == document.getElementById('eSignMessage').innerHTML) {
				alert(PlsEnterEsign);/*alert('Please enter e-Signature.');*****/
        		return false;
			}
        	if (L_Valid_Esign != document.getElementById('eSignMessage').innerHTML) {
        		alert(document.getElementById('eSignMessage').innerHTML);
        		return false;
        	}
            var thisTimeSubmitted = new Date();
            if (!lastTimeSubmitted) { return true; }
            if (!thisTimeSubmitted) { return true; }
            if ( thisTimeSubmitted - lastTimeSubmitted <= 1000 ) {
                return false;
            }
            lastTimeSubmitted = thisTimeSubmitted;
        }
	} catch(e) {}
	return true;
}

VELOS.manageVisitsGrid.decodeData = function(oData) {
	oData = oData.replace(/[&]lt;/g,"<"); //replace less than
	oData = oData.replace(/[&]gt;/g,">"); //replace greater than
	oData = oData.replace(/[&]amp;/g,"&"); //replace &
	return oData;
}

VELOS.manageVisitsGrid.encodeData = function(oData) {
	oData = oData.replace(/[&]/g,"&amp;"); //replace &
	oData = oData.replace(/[<]/g,"&lt;"); //replace less than
	oData = oData.replace(/[>]/g,"&gt;"); //replace greater than
	return oData;
}

VELOS.manageVisitsGrid.prototype.initConfigs = function(args) {
	if (!args) { return false; }
	if (args.constructor != Object) { return false; }
	if (args.urlParams) { this.urlParams = args.urlParams; }
	else { this.dataTable = 'manageVisitsGrid'; }
	if ((args.success) && (typeof args.success == 'function')) {
		this.handleSuccess = args.success;
	}
	if ((args.failure) && (typeof args.failure == 'function')) {
		this.handleSuccess=args.success;
	}
	return true;
}

VELOS.manageVisitsGrid.prototype.startRequest = function() {
	$C.asyncRequest(
		'POST',
		this.url,
		this.callback,
		this.urlParams
	);
}

VELOS.manageVisitsGrid.prototype.render = function() {
	this.startRequest();
}

VELOS.manageVisitsGrid.prototype.loadingPanel = function() {
	// Initialize the temporary Panel to display while waiting for external content to load
	if (!(VELOS.wait)) {
		VELOS.wait =
			new YAHOO.widget.Panel("wait",
				{ width:"240px",
				  fixedcenter:true,
				  close:false,
				  draggable:false,
				  zindex:4,
				  modal:true,
				  visible:false
				}
			);

		VELOS.wait.setHeader(LoadingPlsWait);/*VELOS.wait.setHeader("Loading, please wait...");*****/
		VELOS.wait.setBody('<img class="asIsImage" src="../images/jpg/loading_pg.gif" />');
		VELOS.wait.render(document.body);
	}
}

VELOS.manageVisitsGrid.prototype.showPanel = function () {
	if (!(VELOS.wait)) { loadingPanel(); }
	VELOS.wait.show();
}

VELOS.manageVisitsGrid.prototype.hidePanel = function() {
	if (VELOS.wait) { VELOS.wait.hide(); }
}

/**
 * Ak:Added method to validate the visit before deletion
 */
VELOS.manageVisitsGrid.validateVisitForDeletion=function(myGrid,visitId,visitNameSel){ //AK:Added for enhancement PCAL-20353
	var arrRecords = myGrid.getRecordSet();
	var count=0;
	var array = new Array();
	for(rec=0; rec < arrRecords.getLength(); rec++){
		oRecord = arrRecords.getRecord(rec);
		insertAfterVal = oRecord.getData("insertAfter");
		if(insertAfterVal != "") {
			var insertAfterIds = oRecord.getData("insertAfterId");
			insertAfterIds=insertAfterIds==undefined ?'':insertAfterIds;
			insertAftervisitId = insertAfterIds.substring(0,insertAfterIds.indexOf("/"));
			if(insertAftervisitId==visitId || visitNameSel==insertAfterVal){
				var visitName=oRecord.getData("visitName");
				array.push(visitName);
				count++;
			}
		}	
	}
	if(count>0){
		array=array.join(",");
		var paramArray = [array];
		alert(getLocalizedMessageString("M_ToDelVst_FstChgRef",paramArray));/*alert("To delete this visit,you have to delete "+array+" first or change the parent visit reference please");*****/
		return false;
	}
	return true;
	
}
/**
 * Ak: Added for the enhancement PCAL-20801 for copying a visit
 */
VELOS.manageVisitsGrid.copyVisit= function(myDataTable,fromCopyVisitRecord){
	
	var myArray = [];
	visitSeq++;
	var fromCopyVisitId=0;
	var visitId=fromCopyVisitRecord.getData("visitId")
	if(visitId>0)
		fromCopyVisitId=visitId;
	else
		fromCopyVisitId=fromCopyVisitRecord.getData("fromCopyVisitId");
		
	var copyDataJSON = {"visitSeq":visitSeq,"visitId":'0',"visitName":"","delete":"false","description":fromCopyVisitRecord.getData("description"),"durUnitA":fromCopyVisitRecord.getData("durUnitA"),"durUnitB":fromCopyVisitRecord.getData("durUnitB"),
			"noInterval":fromCopyVisitRecord.getData("noInterval"),"noIntervalId":fromCopyVisitRecord.getData("noIntervalId"),"noTimePointId":fromCopyVisitRecord.getData("noTimePointId"),"months":fromCopyVisitRecord.getData("months"),"weeks":fromCopyVisitRecord.getData("weeks"),"days":fromCopyVisitRecord.getData("days"),"insertAfterInterval":fromCopyVisitRecord.getData("insertAfterInterval"),"afterText":L_After,
			"insertAfter":fromCopyVisitRecord.getData("insertAfter"),"beforenum":fromCopyVisitRecord.getData("beforenum"),"afternum":fromCopyVisitRecord.getData("afternum"),"intervalUnit":fromCopyVisitRecord.getData("intervalUnit"),"insertAfterId":fromCopyVisitRecord.getData("insertAfterId"),"isFakeVisit":'',"isRealFakeVisit":'',"fromCopyVisitId":fromCopyVisitId,"hideFlag":'0',"offlineFlag":'0'};

	var record = YAHOO.widget.DataTable._cloneObject(copyDataJSON);		
	var tabLength= myDataTable.getRecordSet().getLength();
	record.row = tabLength+1;
	myArray.push(record);										
	myDataTable.addRows(myArray);
	VELOS.manageVisitsGrid.applyRowAttribs(myDataTable,1);
	manageVisitGridChangeList['visitSeq'+visitSeq] = 'Added'; 
	
}
//End here



