function updateSourceField(fieldObj, value){
	if (!fieldObj){
		return;
	}
	if (value && value != null && value != "null"){
		//for safety in IE, check that value is in
		//dropdown before setting it if this is an options field
		var output = fieldObj.options;
		
		if (output){
			var isValuePresent = false;
			var isSelectOptionPresent = false;
			for(var i=0;i<output.length;i++) {
			    if(output[i].value == value){
			    	isValuePresent=true;
			    	break;
			    }
			    if(output[i].value == ""){
			    	isSelectOptionPresent = true;
			    }
			    	
			}
			if (isValuePresent){
				fieldObj.value = value;
			}
			else if (isSelectOptionPresent){
				fieldObj.value = "";
			}
		}
		else{ //not a select field, safe to set value
			fieldObj.value = value;
		}
	}
	
}

function dereferenceCode(remoteValue, codeType){
	var codeKeyField = document.getElementById(codeType + "_" + remoteValue);
	if (codeKeyField != null){
		return codeKeyField.value;
	}
	return null;
}