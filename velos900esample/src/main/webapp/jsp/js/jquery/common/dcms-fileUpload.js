// This map holds status of whether all browse fileds has valid file selection.
var filesStatusMap = new Object();
filesStatusMap['firstFile'] = 0;
var uploadFlag=false;
var insertUrl=null;
var userName=null;
var pass=null;

// This method id called when user selects a file through browse button on html
// and clicks OK
function onFileSelect(elementId, formId, docFieldId, fileId) {
	
    var ajaxC = uploadClosure(fileId, elementId);
    ajaxC.startRequest(formId,docFieldId);
}

// This is a closure to send file upload POST request.
function uploadClosure(fileID, statusKey){
    var uploadHandler = function(response) {
        filesStatusMap[statusKey] = 1;
        uploadFlag=true;
    }
    
    var callback = {
        upload:uploadHandler
    };

    return {
        startRequest:function(formName,docField) {
            var docId = document.getElementById(docField).value;
            filesStatusMap[statusKey] = 0;
            YAHOO.util.Connect.setForm(formName,true);
            var dcmsXml = readXml1("../../xml/dcmsAttachment.xml");
            $j(dcmsXml).find("dcms").each(function() {
            	insertUrl=$j(this).find("dcmsIpAddress").text();
				userName=$j(this).find("userName").text();
				pass=$j(this).find("password").text();
				
			});
            
            
            // Start the transaction.
            YAHOO.util.Connect.asyncRequest('POST', 'http://'+insertUrl+'/FileData-war/DcmsFileServlet?userName='+userName+'&password='+pass+'&docId='+docId,
                    callback);
        }
    };
}

// This method is called as a json response from server side as a result of GET request
// when we click submit button. If response from server is OK for all the files
// then we submit main application form
function fileStatus(response) {
    var responseObj =  eval(response);

    if(responseObj[0].status_code == 0){
        filesStatusMap[statusKey] = 1;
        
        document.ApplicationForm.FileId1.value = responseObj[0].fileId;

        // submit main application form
        document.ApplicationForm.submit();
    }
}
function onSubmit(elementId,docFieldId){
    //Submit only if all files submitted successfully
    if(filesStatusMap[elementId] == 1) {
        getFileStatus(docFieldId);
    } else {
        alert('Cannot submit! File failed while uploading or  it\'s in progress, please try again');
    }
}

// This method sends a GET request to dcms server for verifying all document Ids status.
function getFileStatus(docFieldId) {
    var docIds = document.getElementById(docFieldId).value;
    YAHOO.util.Get.script('http://'+insertUrl+'/FileData-war/DcmsFileStatusServlet?param=testRequest&docIds='+ docIds+'&time='+ new Date(), {
            onSuccess: gSuccess,
            scope    : this
        });
}

function gSuccess(o){
    o.purge();  // immediately removes script node after executing it
}
