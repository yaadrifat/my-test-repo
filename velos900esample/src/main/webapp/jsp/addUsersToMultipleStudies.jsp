<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>
<%-- Add Users to Multiple <%=LC.Std_Studies%>*****--%>
<%=MC.M_AddUsers_MultiStd%>
</title>

<%@ page import ="java.util.*, java.io.*, org.w3c.dom.*,com.velos.eres.business.common.*, com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*"  %>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>

<script Language="javascript"> 

function validateme(formObj){

	//JM: 120706
 	counter = 0; 
 	totRows = formObj.numrows.value; 
 	
 for(cnt = 0; cnt<totRows; cnt++){

	userId = "formObj.userId" + cnt;
	userIds = eval(userId).value ;
		
	roleId = "formObj.role" + cnt;
	roleIds = eval(roleId).value;
		
	studyId = "formObj.studyId" + cnt;
	studyIds = eval(studyId).value;	
	
    if ((userIds=='' && roleIds == '' && studyIds == '')){
        counter++;
            
	    if (counter == totRows){
	        /*alert("Please enter atleast one Response");*****/
			  alert("<%=MC.M_EtrAtleast_OneResp%>");
	    	return false;
	    }
	}


	if ((userIds !='') && (roleIds == '') && (studyIds == '')){
	/*alert("Please enter data in all mandatory fields")*****/
	  alert("<%=MC.M_Etr_MandantoryFlds%>")
	return false;			
 	}

	if ((userIds=='') && (roleIds == '') && (studyIds !='')){
	/*alert("Please enter data in all mandatory fields")*****/
	alert("<%=MC.M_Etr_MandantoryFlds%>")
	return false;		
 	}
 	
 	if ((userIds=='') && (roleIds != '') && (studyIds == '')){
	/*alert("Please enter data in all mandatory fields")*****/
	 alert("<%=MC.M_Etr_MandantoryFlds%>")
	return false;		
 	}
	
	if ((userIds !='') && (roleIds == '') && (studyIds != '')){
	/*alert("Please enter the role name")*****/
	alert("<%=MC.M_Etr_RoleName%>")
	return false;		
 	}

	if ((userIds !='') && (roleIds != '') && (studyIds ==='')){
	/*alert("Please enter the <%=LC.Std_Study_Lower%> name (s) ")*****/
	  alert("<%=MC.M_PlsEtr_StdName_S%>")
	return false;		
 	}
 	
 	if ((userIds=='') && (roleIds != '') && (studyIds != '')){
	/*alert("Please enter user name (s) ")*****/
	  alert("<%=MC.M_PlsEtr_UserName_S%> ")
	return false;		
 	}
	 
 }
	
	////////////////////
	
	
	if(!(validate_col('e-Signature',formObj.eSign))) return false
	if (isNaN(formObj.eSign.value) == true){
		/*alert("Incorrect e-Signature. Please enter again");*****/
		  alert("<%=MC.M_IncorrEsign_EtrAgain%>");
		formObj.eSign.focus();
		return false;		
	}

}

//openLookup() not used here, kept for future use
function openLookup(formobj, name, cntr) {

				
	formobj.target="Lookup";
	formobj.method="post";	
	if(name=='user'){


	formobj.action="multilookup.jsp?viewId=6000&form=addUsersToMultipleStudies&seperator=,"+
		"&keyword=userFName"+cntr+"|USRFNAME|[VELHIDE]~userLName"+cntr+"|USRLNAME|[VELHIDE]~userId"+cntr+"|USRPK|[VELHIDE]~userName"+cntr+"|[VELEXPR]*[VELKEYWORD=USRFNAME]*[VELSTR=]*[VELSPACE]*[VELKEYWORD=USRLNAME]";

	}

/////////////////////////
 
////////////////////////

if (name=='study'){

		//var selstudyId = formobj.studyIdNew;
		//alert(selstudyId);
	
		 
		
	formobj.action="multilookup.jsp?viewId=6013&form=addUsersToMultipleStudies&seperator=,"+
           "&keyword=studyName"+cntr+"|STUDY_NUMBER~studyId"+cntr + "|LKP_PK|[VELHIDE]";
}

formWin =open('donotdelete.html','Lookup','resizable=1,status=0, width=850,height=550 top=100,left=100,menubar=no,scrollbars=1');

	if (formWin && !formWin.closed) formWin.focus();	
	formobj.submit();
	formobj.target="_self";
	formobj.action="updateAddUsersToMultipleStudies.jsp";
	void(0);
}


</script>



<jsp:useBean id="TeamB" scope="request" class="com.velos.eres.web.team.TeamJB"/>
<jsp:useBean id="sess" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="include.jsp" flush="true"/>
<BODY>
<%
HttpSession tSession = request.getSession(true);
if (sess.isValidSession(tSession)){
	//Added by Manimaran to fix the Bug 2737
	int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MUSERS"));	
        if (pageRight >= 6)
	{
%>

<p class = "sectionHeadings" ><%-- Add Users to Multiple <%=LC.Std_Studies%>*****--%><%=MC.M_AddUsers_MultiStd%></P>
<br>


<P class='DefComments'>
	<font size="2">
	<%--    You can assign multiple users to multiple <%=LC.Std_Studies_Lower%> using the following grid. If you choose a user that is already on any of the selected <%=LC.Std_Studies_Lower%>,
	 the user will not get added again. The best way to use this grid is to identify users and <%=LC.Std_Studies_Lower%> for a 'Role'.*****--%>  <%=MC.M_AssignUsrTo_MultiStd%> 
	</font>
</p>



<DIV class="popDefault" id="div1">
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl midalign" >
	<tr>
	<th align="center" colspan="2"> <%--Select Users *****--%>  <%=LC.L_Select_Users%>   <FONT class="Mandatory">* </FONT></th>
	<th align="center" colspan="2"><%--Select <%=LC.Std_Study%>  *****--%><%=LC.L_Select_Study%> <FONT class="Mandatory">* </FONT></th>	
	<th align= center><%-- Role*****--%><%=LC.L_Role%> <FONT class="Mandatory">* </FONT></th>
	</tr>
<%
          CodeDao cdRole = new CodeDao();
   	   	  cdRole.getCodeValues("role");
         // String dRole = cdRole.toPullDown("role");
         String role = ""; 

%>


	<form name=addUsersToMultipleStudies id="usrtomultstudies" method = post action = "updateAddUsersToMultipleStudies.jsp" onsubmit="if (validateme(document.addUsersToMultipleStudies)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
		  
	<input type="hidden" name="pageMode" value="final">
<br>
<%
	int numrows = 5;
	
	for (int i=0; i <  numrows; i++){ %>

	<tr width = "100%">
	
	<td width="25%">
	<input type="hidden" name="userId<%=i%>" size = 25 >
	<input type="text" name="userName<%=i%>" size = 25 readonly>
	<input type="hidden" name="userLName<%=i%>" size = 25 >
	<input type="hidden" name="userFName<%=i%>" size = 25 >
	</td>
	
	<td width="15%">
	<A href=# onClick="return openLookup(document.addUsersToMultipleStudies,'user',<%=i%>);"><%--Select Users *****--%>  <%=LC.L_Select_Users%>   </A>
		 
	</td>	
		
	
	<td width="25%"><input type="text" name="studyName<%=i%>" size = 35 readonly> <input type="hidden" name="studyId<%=i%>" size = 25 > 
	</td>
	<td width="15%">
	<A href=# onClick="return openLookup(document.addUsersToMultipleStudies,'study',<%=i%>);" ><%-- Select <%=LC.Std_Study%>*****--%><%=LC.L_Select_Study%></A>
	  
	</td>
	
	     <%
	     //role = "role[" + i + "]";	     
	     role = "role" + i ;	     
	     String dRole = cdRole.toPullDown(""+role);
	     
	     %>     
	<td width="20%">&nbsp;
		<%=dRole%> 
	</td>
	          
	</tr>

	<%
	}
	
	%>
		<input type="hidden" name="numrows" value="<%=numrows%>">
</table>

<br>

<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="usrtomultstudies"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

</div>

</form>

<%
//Added by Manimaran to fix the Bug 2737
}
else
{
%>
<jsp:include page="accessdenied.jsp" flush="true"/>
<%
}

}
//end of session
else{
%>

<jsp:include page="timeout.html" flush='true'></jsp:include>
<%}%>

<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</BODY>

</html> 