<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page import="com.velos.eres.service.util.*,com.velos.eres.barcode.*,com.velos.eres.business.common.*"%>
<%@ page import="java.util.ArrayList,java.text.DecimalFormat"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<title><%=LC.L_Generate_BarCode%><%--Generate Bar Code*****--%></title>
<script>
var popupWindow;
function popupKeywords(type) {
	var keywordFile = "keySpecimen.html";
	if (type == 'storage') {
		keywordFile = "keyStorage.html";
	}
	popupWindow = window.open(keywordFile, "PopupInfo", 
		"toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=450, top=110, left=80");
	popupWindow.focus();
}
</script>
<jsp:include page="include.jsp" flush="true"/>
</head>
<body>

<%
  HttpSession tSession = request.getSession(true);
  if (sessionmaint.isValidSession(tSession)) {
      String sessId = tSession.getId();
      if (sessId.length()>8) { sessId = sessId.substring(0,8); }
      sessId = Security.encrypt(sessId);
      char[] chs = sessId.toCharArray();
      StringBuffer sb = new StringBuffer();
      DecimalFormat df = new DecimalFormat("000");
      for (int iX=0; iX<chs.length; iX++) {
          sb.append(df.format((int)chs[iX]));
      }
      String keySessId = sb.toString();
      String labelType = StringUtil.htmlEncodeXss(
              StringUtil.trueValue(request.getParameter("labelType")));
      if (labelType == null || labelType.length() == 0) { labelType = "specimen"; }
      LabelDao ldao = new LabelDao();
      ldao.getTemplateList((String)tSession.getAttribute("accountId"), labelType);
      ArrayList templatePks = ldao.getTemplatePks();
      ArrayList templateNames = ldao.getTemplateNames();
%>
  <form name="gen" style="margin:2px;font-family:Arial,Helvetica,sans-serif;" action="velos-doc.pdf" method="post" target="preview">
    <%=LC.L_Choose_Atemplate%><%--Choose a template*****--%>
    <%=EJBUtil.createPullDownNoSelect("templatePk", 0, templatePks, templateNames)%>
    <input type="submit" name="submit" value="<%=LC.L_Go%><%--Go*****--%>">
    <input type="hidden" id="selPks" name="selPks" value="<%=StringUtil.htmlEncodeXss(request.getParameter("selPks"))%>"/>
    <input type="hidden" id="labelType" name="labelType" value="<%=labelType%>"/>
    <input type="hidden" id="accountId" name="accountId" value="<%=StringUtil.htmlEncodeXss((String)tSession.getAttribute("accountId"))%>"/>
    <input type="hidden" id="key" name="key" value="<%=keySessId%>" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="#" onClick="popupKeywords('<%=labelType%>');"><%=LC.L_View_AvailableKeywords%><%--View Available Keywords******--%></a>
  </form>
  <iframe name="preview" src="" 
						width="100%" height="90%" frameborder="1" scrolling="yes" allowautotransparency=true>
  </iframe>
<%
  } else { // session invalid
%>
<%=MC.M_Your_SessTimedOut%><%--Your session timed out.*****--%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%} %>
<jsp:include page="bottompanel.jsp" flush="true"/>
</body>
</html>
