<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="patFacility" scope="request" class="com.velos.eres.web.patFacility.PatFacilityJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.business.pref.impl.*,com.velos.eres.business.person.impl.*"%>


<%

String eSign = request.getParameter("eSign");

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

{
%>

<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String oldESign = (String) tSession.getValue("eSign");





	if(!oldESign.equals(eSign)) {

%>


	<jsp:include page="incorrectesign.jsp" flush="true"/> 
<%

	} else {

 	

	String ipAdd = (String) tSession.getValue("ipAdd");

	String usr = null;

	usr = (String) tSession.getValue("userId");

	


  int personPK  = 0;	

  String mode = null;

  String organization = "";

  String regBy = "";

  String account = "";

  String phyOther="";

  String specialityIds = "";
  String patientFacilityId = "";
  int patFacilityPK = 0;
  String regdate = "";
  String accessFlag = "";
  int savedKey = 0;
  String isDefault = "";

	
	organization = request.getParameter("patorganization");
	organization=(organization==null)?"":organization;

   
 
 
      mode = request.getParameter("mode");
	   
	 String patientId = request.getParameter("patientId");
  	patientFacilityId = request.getParameter("patFacilityID");
	regBy = request.getParameter("patregby");
	phyOther=request.getParameter("phyOther");
   	regdate = request.getParameter("regdate");
	accessFlag = request.getParameter("accessFlag");
	isDefault =  request.getParameter("isDefault");
   
   specialityIds = request.getParameter("selSpecialityIds");
   
    if(StringUtil.isEmpty(regBy ))
    {
    	regBy = "";
    }
      
	if(StringUtil.isEmpty(regdate))
    {
    	regdate = "";
    }
	   
   if(StringUtil.isEmpty(specialityIds))
   {
       specialityIds="";
   }
   
   if (organization.equals("")) organization = null; 

      int saved = 0;
	if (mode.equals("M"))
	{

		patFacilityPK  = EJBUtil.stringToNum(request.getParameter("patFacilityPK"));

		patFacility.setId(patFacilityPK );
	
        patFacility.getPatFacilityDetails();

	}

	    patFacility.setPatientFacilityId(patientFacilityId); 
		patFacility.setPatientPK(patientId);
		patFacility.setPatientSite(organization);
		patFacility.setRegDate(regdate);
		patFacility.setRegisteredBy(regBy);
		patFacility.setRegisteredByOther(phyOther);
		patFacility.setPatSpecialtylAccess(specialityIds);
		patFacility.setPatAccessFlag(accessFlag);
		//out.println("accessFlag" + accessFlag);
	    patFacility.setIsDefault(isDefault);
		patFacility.setIPAdd(ipAdd);		 
	
		if (mode.equals("M"))

		{

		patFacility.setLastModifiedBy(usr);

		saved = patFacility.updatePatFacility();

		  

	}else

	{


		patFacility.setCreator(usr);

		patFacility.setPatFacilityDetails();
		savedKey = patFacility.getId(); 
		
	if (savedKey > 0)

		  {
			saved = 0;
		 }

		else

		{
			saved = -1;
		}
	}



	  if (saved == 0)


	  {
%>


<br>

<br> 
 
<br>

<br>

<br>

<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>

<%

}

else

{

%>

<br>

<br>

<br>

<br>

<br>

<p class = "successfulmsg" align = center> <%=MC.M_DataCnt_Svd %><%-- Data could not be saved.*****--%></p>



<%

}
%>
<script>
	window.opener.document.patient.action="patientdetails.jsp" ;
	window.opener.document.patient.submit();
	setTimeout("self.close()",1000); 
</script>
<%

}//end of if for eSign check

}//end of if body for session



else



{



%>



  <jsp:include page="timeout.html" flush="true"/>
  <%



}



%>





</BODY>

</HTML>












