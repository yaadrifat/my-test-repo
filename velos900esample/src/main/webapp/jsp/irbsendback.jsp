<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<script>
function validate(formobj) {
	
	if (!(validate_col('e-Signature',formobj.eSign))) return false;
		
	if (formobj.notes != null 
			&& formobj.notes.value.length > 4000 ) {
		var paramArray = [formobj.notes.value.length];
		alert(getLocalizedMessageString("M_NotesExcd_4kCur",paramArray));/*alert('Notes may not exceed 4000 characters. Currently:'+formobj.notes.value.length);*****/
		return false;
	}
	return true;
}
</script>
<jsp:include page="include.jsp" flush="true"/>
<title><%=MC.M_SendBackTo_RevBoard%><%--Send Back to Review Board*****--%></title>
</head>
<body>
<DIV style="font-family:Verdana,Arial,Helvetica,sans-serif">
<form name="mainForm" id="mainForm" action="updateSubmissionStatus.jsp" 
      onSubmit="if(validate(this)==false){setValidateFlag('false');return false;}else{return true;}"
      method="post">
<jsp:useBean id ="sessionmaint" scope="request" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="submBoardB" scope="request" class="com.velos.eres.web.submission.SubmissionBoardJB"/>
<%@ page import="com.velos.eres.business.common.*,com.velos.eres.service.util.*" %>
<%@ page import="java.util.ArrayList,java.util.Calendar,java.util.Hashtable" %>
<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
    CodeDao codeDao = new CodeDao();
    codeDao.getCodeValues("subm_status");
    ArrayList codeSubtypes1 = codeDao.getCSubType();
    ArrayList cIds1 = codeDao.getCId();
    String resubmittedStatus = "";
    for(int iX=0; iX<codeSubtypes1.size(); iX++) {
        if ("resubmitted".equals((String)codeSubtypes1.get(iX))) {
            resubmittedStatus = ""+cIds1.get(iX);
        }
    }
    boolean isMSIE = false;
    if (request.getHeader("USER-AGENT").toLowerCase().indexOf("msie") != -1) { isMSIE = true; }
    CodeDao cdSubmStatus = new CodeDao();
    cdSubmStatus.getCodeValues("subm_status");
    cdSubmStatus.setCType("subm_status");
    String cdStatusPowerBarMenu = cdSubmStatus.toPullDown("submissionStatus");

    String accountId = (String)tSession.getAttribute("accountId");
    String studyId = request.getParameter("studyId");
	tSession.setAttribute("studyId", StringUtil.htmlEncodeXss(studyId));
    String defUserGroup = (String) tSession.getAttribute("defUserGroup");
    String usrId = (String) tSession.getValue("userId");
    String usrFullName = UserDao.getUserFullName(EJBUtil.stringToNum(usrId));
    String submissionPK = StringUtil.htmlEncodeXss(request.getParameter("submissionPK"));
    String submissionBoardPK = StringUtil.htmlEncodeXss(request.getParameter("submissionBoardPK"));
    String selectedTab = StringUtil.htmlEncodeXss(request.getParameter("selectedTab"));
    
    EIRBDao eIrbDao = new EIRBDao();
    eIrbDao.getReviewBoards(EJBUtil.stringToNum(accountId), EJBUtil.stringToNum(defUserGroup));
    ArrayList boardNameList = eIrbDao.getBoardNameList();
    ArrayList boardIdList = eIrbDao.getBoardIdList();
    
    if (submissionPK == null || submissionPK.length() == 0 
            || submissionBoardPK == null || submissionBoardPK.length() == 0) { 
%>
<script>
  self.close();
</script></form></body></html>
<%      return;
    }  %>
<%
    String reviewBoardName = null;
    for(int iBoard=0; iBoard<boardNameList.size(); iBoard++) {
        submBoardB.findByPkSubmissionBoard(EJBUtil.stringToNum(submissionBoardPK));
        String fkReviewBoard = submBoardB.getFkReviewBoard();
        if (fkReviewBoard.equals(boardIdList.get(iBoard))) {
            reviewBoardName = (String) boardNameList.get(iBoard);
            break;
        }
    }
    if (reviewBoardName == null) { reviewBoardName = "Review Board"; }
%>
<table width="100%" cellspacing="0" cellpadding="0" Border="0">
<tr><td><br /></td></tr>
<tr><td><P class="sectionHeadings"><%Object[] arguments = {reviewBoardName}; %>
	    <%=VelosResourceBundle.getMessageString("M_SendBack_To",arguments)%><%--Send Back to <%=reviewBoardName%>*****--%></P></td></tr>
</table>
<table width="90%" cellspacing="0" cellpadding="0" Border="0" style="font-size:8pt" align="center">
<tr><td><br /></td></tr>
<tr align="center">
  <input type="hidden" id="submissionPK" name="submissionPK" value="<%=EJBUtil.stringToNum(submissionPK)%>" />
  <input type="hidden" id="submissionBoardPK" name="submissionBoardPK" value="<%=EJBUtil.stringToNum(submissionBoardPK)%>" />
  <input type="hidden" id="submissionStatus" name="submissionStatus" value="<%=EJBUtil.stringToNum(resubmittedStatus)%>" />
  <input type="hidden" id="selectedTab" name="selectedTab" value="<%=selectedTab%>" />
  <td><%=LC.L_Notes%><%--Notes*****--%><br/><TextArea id="notes" name="notes" rows="15" cols="50"></TextArea>
</tr>
</table>

<div class="popDefault" style="width:100%">
<table width="100%"><tr><td></td><td>
<jsp:include page="submitBar.jsp" flush="true"> 
  <jsp:param name="displayESign" value="Y"/>
  <jsp:param name="formID" value="mainForm"/>
  <jsp:param name="showDiscard" value="N"/>
</jsp:include>
</td></tr></table>
</div>
<%    
} 
else { // session invalid
%>
<jsp:include page="timeout_childwindow.jsp" flush="true"/>
<%
}
%>
  <div class = "myHomebottomPanel">
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
</form>
</body>
</html>

