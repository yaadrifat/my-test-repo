<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=MC.M_Acc_FrmRespBrow%><%--Account >> Form Response Browser*****--%></title>

<script>

//function openWin(formId,studyId) 
//{  
//	windowName= window.open("studyformdetails.jsp?formId="+formId+"&mode=N&studyId="+studyId+"&formDispLocation=S","information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=500");
	//windowName.focus();
//}
	
function checkPerm(pageRight) 
{
	
	if (f_check_perm(pageRight,'N') == true) 
	{
		return true ;	
 	
	} 
	else 
	{
		return false;
	}
}	
	

	
</script>

<%
String src= request.getParameter("srcmenu");
String calledFromForm = "";
       calledFromForm = request.getParameter("calledFromForm");
       
	String outputTarget = ""; //if a target name is passed, open the new and edit page in this target
	 outputTarget = request.getParameter("outputTarget");
	   
	 if (StringUtil.isEmpty(outputTarget))
	 {
	 	outputTarget = "";
	 }  
		       	 
     
      if (StringUtil.isEmpty(calledFromForm))
      {
     	calledFromForm = "";
      }
      
   
    String specimenPk = "";
    specimenPk = request.getParameter("specimenPk");
    
     if (StringUtil.isEmpty(specimenPk))
	 {
	 	specimenPk = "";
	 }  
	
      
   
   if (calledFromForm.equals(""))   
   {
%>
	<jsp:include page="panel.jsp" flush="true"> 
	<jsp:param name="src" value="<%=src%>"/>
	</jsp:include>   
	
 <% }
 else
 { %>
 	<jsp:include page="popupJS.js" flush="true"/>
 		<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
	<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>	
 	<%	
 }
 %> 

<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>   
<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.eres.business.common.CtrlDao,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.web.user.UserJB,com.velos.eres.business.common.*"%>

<br>
<%
if (calledFromForm.equals(""))   
   {
%>
<div class="browserDefault" id="div1">
<%
}	
%>	
	
<%
HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession)) {

		int pageRight = 0;	
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("ACTFRMSACC"));

		String modRight = (String) tSession.getValue("modRight");
		int formLibSeq = 0;
		
	 	// To check for the account level rights
		 acmod.getControlValues("module");
		 ArrayList acmodfeature =  acmod.getCValue();
		 ArrayList acmodftrSeq = acmod.getCSeq();
		 char formLibAppRight = '0';
		 formLibSeq = acmodfeature.indexOf("MODFORMS");
		 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
		 formLibAppRight = modRight.charAt(formLibSeq - 1); 
		 
		 
	     ArrayList arrFrmNames = null;
		 ArrayList arrNumEntries = null;
 		 ArrayList arrFrmIds = null;
		 ArrayList arrEntryChar = null;
		 ArrayList arrFrmInfo = new ArrayList();
		 String entryChar="";
		 String numEntries = "";
		 String frmInfo= "";
		 int formId = 0;
		 String firstFormInfo = "";
  	     String strFormId = request.getParameter("formPullDown");	 
	 	 String userId = (String) tSession.getValue("userId");
	     UserJB userB = (UserJB) tSession.getValue("currentUser");
   	     String siteId = userB.getUserSiteId();
		 String accId=(String)tSession.getAttribute("accountId");
		 int iaccId=EJBUtil.stringToNum(accId);
		 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
		 lnkFrmDao = lnkformB.getAccountForms(iaccId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId));
		 
		 arrFrmIds = lnkFrmDao.getFormId();		 
		 arrFrmNames = lnkFrmDao.getFormName();
		 arrEntryChar = lnkFrmDao.getFormEntryChar();
		 arrNumEntries = lnkFrmDao.getFormSavedEntriesNum();
	
		 if (arrFrmIds.size() > 0) { 	
    		 if (strFormId==null) {
    		 	formId = EJBUtil.stringToNum(arrFrmIds.get(0).toString());
    			entryChar = arrEntryChar.get(0).toString();
    			numEntries = arrNumEntries.get(0).toString();		   
    			firstFormInfo = arrFrmIds.get(0).toString() + "*"+ entryChar + "*" + arrNumEntries.get(0).toString();
    		 }
    		 else {
    	 		 StringTokenizer strTokenizer = new StringTokenizer(strFormId,"*");
    	 		 
    	 		 if (calledFromForm.equals(""))
    	 		 {
    	 	     	formId = EJBUtil.stringToNum(strTokenizer.nextToken());
    			 	entryChar = strTokenizer.nextToken();
    		     	numEntries = strTokenizer.nextToken();
    			 	firstFormInfo = strFormId;	   
    			 }	
    			 else
    			 {
    			 	formId = EJBUtil.stringToNum(strTokenizer.nextToken());
				String accFormId = request.getParameter("formId");
    			 	entryChar = strTokenizer.nextToken();  			 	
    			 	lnkformB.findByFormId(formId );
		   		 	numEntries = lnkformB.getLfDataCnt();
    			 	//get the number of times the form was answered
    			 	//prepare strFormId again
    			 	
    			 	strFormId = formId + "*" + entryChar + "*" + numEntries;
    			 	firstFormInfo = strFormId;	   
    			 
    			 }
    		 }
	   
	   //check for access rights if form is called from a 'link form' link	
       if (!calledFromForm.equals(""))
    	{
    		
    		if (!arrFrmIds.contains(new Integer(formId)))
    		{
    			// check if the form is hidden and user has access to it
				if (! lnkformB.hasFormAccess(formId, EJBUtil.stringToNum(userId)))    			
				{
	    			//user does not have access rights to this form
    				pageRight  = 0;
    			}	
    		}
    		
    	}	

		//JM:
//	if ((String.valueOf(formLibAppRight).compareTo("1") == 0) &&(pageRight >=4))
	if  (pageRight >=4){
	if ((String.valueOf(formLibAppRight).compareTo("1") == 0))
	 {     	
    		 for (int i=0;i<arrFrmIds.size();i++)
    		 {  //store the formId, entryChar and num Entries separated with a *
    		  	 frmInfo = arrFrmIds.get(i).toString() + "*"+ arrEntryChar.get(i).toString() + "*" + arrNumEntries.get(i).toString();			 
    		 	 arrFrmInfo.add(frmInfo);		 
    		 }
    		 		
    				 
    	 	 String dformPullDown = EJBUtil.createPullDownWithStrNoSelect("formPullDown",firstFormInfo, arrFrmInfo , arrFrmNames);
    		 
			 StringBuffer formBuffer = new StringBuffer(dformPullDown);

			 //submit the form when the user selects the form name
			 if (calledFromForm.equals(""))	 
			 {
			 	formBuffer.replace(0,7,"<SELECT onChange=\"document.accform.submit();\"");
			 }
			 //else
			 //{
			 	//formBuffer.replace(0,7,"<SELECT DISABLED onChange=\"document.accform.submit();\"");
			 //}	
			 
			 dformPullDown = formBuffer.toString();
						 
    		 //date filter
    	  	 String formFillDt = "";
    	 	 formFillDt = request.getParameter("formFillDt") ;
     	 	 if (formFillDt == null) formFillDt = "ALL";	 	 
    		 String dDateFilter = EJBUtil.getRelativeTimeDD("formFillDt",formFillDt);
             String formName= "";
		     String formStatus = "";
	         String statSubType = "";
		   
    	     if (formId >0) {
	     	   formLibB.setFormLibId(formId);
   	   		   formLibB.getFormLibDetails();
			   formName=formLibB.getFormLibName();
		   
		   	   formStatus = formLibB.getFormLibStatus();  
			   CodeDao cdao = new CodeDao();
			   cdao.getCodeValuesById(EJBUtil.stringToNum(formStatus));
			   ArrayList arrSybType = cdao.getCSubType();
			   statSubType = arrSybType.get(0).toString();
		    }		   
     	
    	   String linkFrom = "A";
    	   String hrefName = "formfilledaccountbrowser.jsp?srcmenu="+src;
    	   String modifyPageName = "acctformdetails.jsp?srcmenu="+src;
	// Added by Gopu to fix the bugzilla issue #2808
		formLibB.setFormLibId(formId);
		String formLibVer = formLibB.getFormLibVersionNumber(formId);
    	%>
    		<BR>
			
    		<Form name="accform" method="post" action="formfilledaccountbrowser.jsp" onsubmit="">
    		<input type="hidden" name="srcmenu" value=<%=src%>>
    		<input type="hidden" name="calledFromForm" value=<%=calledFromForm%>>
    		
    		<input type="hidden" name="previousPage" value="account">
    		
			<table width="100%" align="center">
  			<%
  			 if (calledFromForm.equals(""))	 
			 {%>    	
			 <tr class="searchBg" height="38" >
    			<td width="10%" align="right" >	
    			<%=LC.L_Frm_Name%><%--Form Name*****--%>:
    			</td>
    			<td width="90%" >  <%=dformPullDown%>
			 <%
			 	}else{
			 %>    				
    		<tr height="38" id="formfilledaccont" ><td width="10%" align="left" >
  			<%}
			//Only Active forms can be answered 
			 if (statSubType.equals("A") && (entryChar.equals("M")) || ((entryChar.equals("E")) && (numEntries.equals("0")))   ) 
			  {
			  	
			  	String jsCheck="";
			  	String outputTargetParam = "";
			  	String hrefurl = "";
			  	
			  	hrefurl= "acctformdetails.jsp?srcmenu="+src+"&formId="+formId+"&mode=N&formDispLocation=A&entryChar="+entryChar+"&formFillDt="+formFillDt+"&formPullDown="+firstFormInfo+"&calledFromForm="+calledFromForm + "&specimenPk=" + specimenPk ;
			  	
			  	if (!StringUtil.isEmpty(outputTarget))
			  	{
			  	
			  		outputTargetParam = "&outputTarget=" + outputTarget;
			  		hrefurl= hrefurl +  outputTargetParam;
			  		
			  		jsCheck = " onClick='openWindowCommon(\""+hrefurl+"\",\""+ outputTarget +"\");' ";
			  		hrefurl = "#";
			  	}
			  	else
			  	{
			  		jsCheck = " onClick =\" return checkPerm(" +pageRight+ " )\"";
			  	}
			  	
			    %>
			    
			     
			    
    			<A type="submit" <%=jsCheck%> HREF="<%=hrefurl%>" ><%=LC.L_New%></A>	
    		<%
    		  }
    		
    		%> 
    		</td>
    		</tr>    		
    		</table>
    		<br>			
    		<table align="center" width="100%">
    		<tr >
    		<td width="50%">
    		<P class="blackComments"><%=MC.M_Prev_EntriesForFrm%><%--Previous entries for Form*****--%>: <B>"<%=formName%>"</B></P>
    		</td>
    		<% if (calledFromForm.equals(""))	 
			 { %>
				<td width="15%"><%=LC.L_Filter_ByDate%><%--Filter By Date*****--%> &nbsp;  <%=dDateFilter%> </td>
    		<td width="35%" align="left"><button type="submit"><%=LC.L_Search%></button></td>		
    		<% } %>	
    		</tr>
    		</table>
    			 
    	    </Form>
    	
    	<!--include the jsp for showing the records-->
    	 <jsp:include page="formdatarecords.jsp" flush="true">
     	 <jsp:param name="formId" value="<%=formId%>"/>
    	 <jsp:param name="linkFrom" value="<%=linkFrom%>"/>
    	 <jsp:param name="hrefName" value="<%=hrefName%>"/>
    	 <jsp:param name="entryChar" value="<%=entryChar%>"/>	 
    	 <jsp:param name="modifyPageName" value="<%=modifyPageName%>"/>
    	 <jsp:param name="formFillDt" value="<%=formFillDt%>"/>
    	 <jsp:param name="formPullDown" value="<%=firstFormInfo%>"/>
    	 <jsp:param name="srcmenu" value="<%=src%>"/>
    	 <jsp:param name="pageRight" value="<%=pageRight%>"/>
    	 <jsp:param name="statSubType" value="<%=statSubType%>"/>		 	 	<jsp:param name="formLibVer" value="<%=formLibVer%>"/>	 		 	 				 		 	 		 	 
    	 <jsp:param name="calledFromForm" value="<%=calledFromForm%>"/>		 	 		 
    	 <jsp:param name="outputTarget" value="<%=outputTarget%>"/>
    	 <jsp:param name="specimenPk" value="<%=specimenPk%>"/>
    	 <jsp:param name="previousPage" value="account"/>
    	 		 	 				 		 	 		 	 
    	 </jsp:include>
    	    
    	<% if (calledFromForm.equals(""))
    		 {
		%>
    	<A href="myHome.jsp?srcmenu=<%=src%>" ><%=MC.M_Back_ToMyHomepage%><%--Back to My Homepage*****--%></A>	 
    	<%
    	}
    		%>	
    		
  <%
	   } //ids.size
	  //end of if body for page right
		else{
		%>
  			<jsp:include page="accessdenied.jsp" flush="true"/>
		<%
		}
		
		//JM: 
		 }else {%>

<br><br><br><P class="defComments"> <%=MC.M_ThisFrm_Del%><%--This Form has been deleted*****--%> </p>

		 <%}//JM:
	} 
	 else 
	   {
	%>
	  <P class="defComments"><%=MC.M_NoAssocForms%><%--No associated Forms*****--%></P>	   
    <%  }	
}//end of if body for session
else {
%>
	<jsp:include page="timeout.html" flush="true"/>
<%}
%>
<br>

<%if (calledFromForm.equals(""))   
   {
%>	
<div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<DIV class="mainMenu" id = "emenu"> 
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>
	<%
}
	%>

</body>
</html>

