<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<%@ page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<title><%=LC.L_Study_AdminSch%><%--<%=LC.Std_Study%> >> Admin Schedule*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<script type="text/javascript">

window.name="studyadmin";
function saveLib(url, calendarRight)
{
	if (f_check_perm(calendarRight,'N')) {
	    url=url+"&mode=initial";
		windowName=window.open(url,"SaveToLibrary","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,top=150, left=200,width=750,height=400");
		windowName.focus();
	return true;
	} else {
	  return false;
	}
}


function selectCalndar(formobj,id)
{
  studyId=
 formobj.action="studyadmincal.jsp?srcmenu=tdmenubaritem3&selectedTab=10&studyId=&calId="+id
}


//KM:The width is changed to fix Bug#2629 on 07July06.
function openwindowcalendar(pageRight,src)
{
    if (f_check_perm(pageRight,'N')) {
		windowname=window.open("protocollist.jsp?mode=N&srcmenu="+src+"&selectedTab=7&calledFrom=S&calassoc=S" ,"protocollist","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=820,height=350 top=120,left=150, ");
		windowname.focus();
		return true;
	} else {
	  return false;
	}
}


function openwindowcopy(pageRight,src,tab)
{
    if (f_check_perm(pageRight,'N')) {
		windowname=window.open("copystudyprotocol.jsp?srcmenu="+src+"&selectedTab="+tab+"&from=initial&calassoc=S" ,"copystudyprotocol","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=350 top=120,left=150, ");
		windowname.focus();
		return true;
	} else {
	  return false;
	}
}


function confirmBox(cal,pgRight,status,statDesc) {
	if (f_check_perm(pgRight,'E') == true) {
		if (status=="A")
		{
		//JM: 22Feb2011: #5865
		//alert("Active Calendars cannot be deleted");
		 var paramArray = [statDesc];
		 alert(getLocalizedMessageString("M_CldrCnt_Del",paramArray));/*alert(statDesc+" Calendars cannot be deleted");*****/
		 return false;
		}
		var paramArray = [cal];
		msg=getLocalizedMessageString("M_DelPcol_Cal",paramArray);/*msg="Delete Protocol Calendar '" + cal+ "'?";*****/
		if (confirm(msg)) {
    		return true;
		}
		else{
			return false;
		}
	} else {
		return false;
	}
}


</script>

<%String src = "";
            HttpSession tSession = request.getSession(true);
            src = request.getParameter("srcmenu");

            String from = "admin";

            String tab = request.getParameter("selectedTab");


            String studyIdForTabs = "";
 			studyIdForTabs = request.getParameter("studyId");



%>


<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>" />
</jsp:include>

<body>
<!-- Prabhat: Bug#4277 Fixed -->
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<jsp:useBean id="sessionmaint" scope="session"	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="eventassocB" scope="request"	class="com.velos.esch.web.eventassoc.EventAssocJB" />
<jsp:useBean id="eventAssocDao" scope="request"	class="com.velos.esch.business.common.EventAssocDao" />
<jsp:useBean id="studyB" scope="request"	class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="commonB" scope="request"	class="com.velos.eres.web.common.CommonJB" />
<jsp:useBean id="acmod" scope="request"	class="com.velos.eres.business.common.CtrlDao" />

<%@ page language="java"	import="com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>

<DIV class="BrowserTopn" id="divTab"><jsp:include page="studytabs.jsp"	flush="true">
	<jsp:param name="from" value="<%=from%>" />
	<jsp:param name="selectedTab" value="<%=tab%>" />
	<jsp:param name="studyId" value="<%=studyIdForTabs%>" />
</jsp:include></DIV>


<%
String stid= (String)tSession.getAttribute("studyId");
if(stid==null || stid==""){%>
   <DIV class="BrowserBotN BrowserBotN_S_1" id="div1">
	<%} else {%>
<DIV class="BrowserBotN BrowserBotN_S_3" id="div1"><%}
	if (sessionmaint.isValidSession(tSession))

            {

             String study = (String) tSession.getValue("studyId");
            	int studyId = EJBUtil.stringToNum(study);

			    //reset session attribute
				tSession.setAttribute("newduration", "");
				tSession.setAttribute("protocolname", "");


                String accountId = (String) tSession.getValue("accountId");
                //Get the parameters to pass to patientscedeule page


                String page1 = request.getParameter("page1");

                String selectedTab =  request.getParameter("selectedTab");


                String generate = request.getParameter("generate");
                generate=(generate==null)?"":generate;
                String pagenum = request.getParameter("nextpage");
                pagenum=(pagenum==null)?"":pagenum;

                String orderBy = request.getParameter("orderBy");
                orderBy=(orderBy==null)?"":orderBy;

                String orderType = "";
                orderType = request.getParameter("orderType");
                orderType=(orderType==null)?"":orderType;

                String visit = request.getParameter("visit");
                visit=(visit==null)?"":visit;
                String  month = request.getParameter("month");
                month=(month==null)?"0":month;
                 String year = request.getParameter("year");
                 year=(year==null)?"0":year;

                String event = request.getParameter("event");
                event=(event==null)?"":event;
                String evtStatus = request.getParameter("dstatus");
                evtStatus=(evtStatus==null)?"":evtStatus;

                String visitxt = request.getParameter("visitext");
                visitxt=(visitxt==null)?"":visitxt;
                String evttxt = request.getParameter("eventext");
                evttxt=(evttxt==null)?"":evttxt;
                String ststxt ="All";
                String durtxt = request.getParameter("monthyrtext");
                durtxt=(durtxt==null)?"":durtxt;
                ststxt = request.getParameter("statustext");
                ststxt=(ststxt==null)?"":ststxt;

               String calSchDate=request.getParameter("schdate");
                calSchDate=(calSchDate==null)?"":calSchDate;

                String protocolId=request.getParameter("calId");
                protocolId=(protocolId==null)?"":protocolId;
                //End getting params for patientscedule

                int counter = 0;
                int len = 0;
                Integer id, formId, formLibId;
                int pageRight = 0;
                int calendarRight = 0;
                int formRights = 0; // for the associated form browser
                int activeId = 0;
                CodeDao cd = new CodeDao();
                activeId = cd.getCodeId("frmstat", "A");%> <%if (study == "" || study == null) {

                %> <jsp:include page="studyDoesNotExist.jsp" flush="true" /> <%} else {

                    studyB.setId(studyId);
                    studyB.getStudyDetails();
                    // int studyId = EJBUtil.stringToNum(study);

                    // String tab = request.getParameter("selectedTab");

                    StudyRightsJB stdRights = (StudyRightsJB) tSession.getValue("studyRights");
                    //Added by IA 12.06.2006  Bug 2747
    				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
    				calendarRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
                    //End Added

                    if ((stdRights.getFtrRights().size()) == 0) {

                        pageRight = 0;

                    } else {

                        pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));

                    }


                   //System.out.println("pageRight.dssss" + pageRight + "stdRights.getFtrRights().size()" +stdRights.getFtrRights().size());






                    /// entered for having rights for study linked forms

                    StudyRightsJB studyFormRights = (StudyRightsJB) tSession
                            .getValue("studyRights");

                    if ((studyFormRights.getFtrRights().size()) == 0) {
                        formRights = 0;
                    } else {
                        formRights = Integer.parseInt(studyFormRights
                                .getFtrRightsByValue("STUDYFRMMANAG"));
                    }

                    if ((pageRight > 0)) {

                        if (pageRight >= 4) {
                            eventAssocDao = eventassocB.getStudyAdminProts(studyId,"");

                            ArrayList eventIds = eventAssocDao.getEvent_ids();
                            ArrayList chainIds = eventAssocDao.getChain_ids();
                            ArrayList names = eventAssocDao.getNames();
                            ArrayList descriptions = eventAssocDao
                                    .getDescriptions();
                            ArrayList eventTypes = eventAssocDao
                                    .getEvent_types();
                            ArrayList durations = eventAssocDao.getDurations();
							
							//KM-#D-Fin9
							ArrayList stats = eventAssocDao.getCodeSubTypes();
							ArrayList calStatDescs = eventAssocDao.getCodeDescriptions();

                            len = eventIds.size();

                            //len = 1;

                            String eventType = "";
                            String name = "";
                            String description = "";
                            String chainId = "";
                            String duration = "";
                            String status = "";
                            String statusText = "";

%>

<table width="99%" cellspacing="0" cellpadding="0" border="0" class="midAlign">
	<tr height="5"><td></td></tr>
	<tr>

<%
//Commented by Gopu to fix the bugzilla Issue #2359
//	grpRights = (GrpRightsJB) tSession.getValue("GRights");
//	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
%>
		<td width="40%">
		<P class="sectionheadings"><%=MC.M_Lnk_AdminCals%><%--Linked Administrative Calendars are*****--%>:
		</P>
		</td>
<td width="30%" align="right">
		<p><A href="#"
	onClick="openwindowcopy(<%=pageRight%>,'<%=src%>',<%=tab%>)"><%=MC.M_Copy_ExistingCal_Upper%><%--COPY AN EXISTING CALENDAR*****--%></A></p>
		</td>
<%
//Added by Gopu to fix the bugzilla Issue #2359
		stdRights =(StudyRightsJB)tSession.getValue("studyRights");
	  		 if ((stdRights.getFtrRights().size()) == 0){
		 		pageRight= 0;
		// 	 }else{
		//		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSEC"));
			}
%>

		<td width="30%" align="right">
		<p><A href="#"
			onClick="return openwindowcalendar(<%=pageRight%>,'<%=src%>')"><%=MC.M_SelCal_FromLib_Upper%><%--SELECT A CALENDAR FROM YOUR LIBRARY*****--%></A></p>
		</td>
	</tr>
</table>
<div class="tmpHeight"></div>
<table width="99%" cellspacing="0" cellpadding="0" border="0" class="midAlign outline">
	<tr>
		<th width="20%"><%=LC.L_Name%><%--Name*****--%></th>
		<th width="10%"><%=LC.L_Refresh_Notifications%></th>
		<th width="25%"><%=LC.L_Description%><%--Description*****--%></th>
		<th width="15%"><%=LC.L_Cal_Status%><%--Calendar Status*****--%></th>
		<th width="15%"><%=LC.L_Status_Dets%><%--Status Details*****--%></th>
		<th width="15%"><%=LC.L_Reports%><%--Reports*****--%></th>
		<!-- commented by gopu to fix #2373 -->
		<!-- th width="10%"></th-->
                <th width="10%"><%=LC.L_Delete%><%--Delete*****--%></th>
	</tr>
	<%if (len<=0){ %>
	<tr><td colspan=2><font class="recNumber"><%=MC.M_NoCalsFound%><%--No Calendars Found*****--%></font></td></tr>

	<%} %>
	<%for (counter = 0; counter < len; counter++) {

                                id = (Integer) eventIds.get(counter);
                                eventType = ((eventTypes.get(counter)) == null) ? "-"
                                        : (eventTypes.get(counter)).toString();

                                name = ((names.get(counter)) == null) ? "-"
                                        : (names.get(counter)).toString();
                                /* SV, 8/25, added check for description = "null" */
                                description = ((descriptions.get(counter) == null) || descriptions
                                        .get(counter).equals("null")) ? ""
                                        : (descriptions.get(counter))
                                                .toString();
                                chainId = ((chainIds.get(counter)) == null) ? "-"
                                        : (chainIds.get(counter)).toString();

                                duration = ((durations.get(counter)) == null) ? "-"
                                        : (durations.get(counter)).toString();

                                status = ((stats.get(counter)) == null) ? "-"
                                        : (stats.get(counter)).toString();

								//KM-#DFin9
								statusText = ((calStatDescs.get(counter)) == null) ? "-"
                                        : (calStatDescs.get(counter)).toString();
								

								if ((counter % 2) == 0) {

                                %>
	<tr class="browserEvenRow">
		<%}

                                else {

                                %>
	</tr>
	<tr class="browserOddRow">
		<%}

                                %>

		<td> <table width="100%" border="0"><tr><td width="15%" style="border: 0px;"> <A href="javascript:void(0);" title="<%=LC.L_Save_ToLib%><%--Save To Library*****--%>"
			onClick="return saveLib('caltolibrary.jsp?eventid=<%=id%>&amp;name=<%=StringUtil.encodeString(name)%>&amp;desc=<%=StringUtil.encodeString(description)%>',<%=calendarRight%> );"><img
			src="./images/save.gif" alt="<%=LC.L_Save_ToLib%><%--Save To Library*****--%>" border="0"
			></A> <%--SV, 10/27/04, enter scheduled events page, while editing: <A href="protocolcalendar.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&protocolId=<%=id%>&calledFrom=S"> <%=name%> </A></td> --%>


		<!--JM: 14Apr08: modified
		<A
			href="displayDOW.jsp?selectedTab=4&amp;mode=M&amp;duration=<%=duration%>&amp;calledFrom=S&amp;protocolId=<%=id%>&amp;srcmenu=<%=src%>&amp;calStatus=<%=status%>&amp;pageNo=1&amp;displayType=V&amp;headingNo=1&amp;displayDur=3&amp;pageRight=<%=pageRight%>&amp;protocolName=<%=StringUtil.encodeString(name)%>&amp;calassoc=S">
		<%=name%> </A>-->
		</td><td style="border: 0px;">
		<A href="fetchProt2.jsp?protocolId=<%=id%>&amp;srcmenu=<%=src%>&amp;selectedTab=6&amp;mode=M&amp;calledFrom=S&amp;calStatus=<%=status%>&amp;displayType=V&amp;pageNo=1&amp;duration=<%=duration%>&amp;headingNo=1&amp;displayDur=3&amp;pageRight=<%=pageRight%>&amp;protocolName=<%=StringUtil.encodeString(name)%>&amp;calassoc=S"><%=name%> </A>
		</td>
		</tr>
		</table>
		</td>
		<td  align="center">
		<% if (status.equals("A")) {
		%>
			<A	href="refreshProtocolMessages.jsp?srcmenu=<%=src%>&amp;selectedTab=<%=selectedTab%>&amp;calendarId=<%=id%>&calledFrom=S&amp;mode="
			 border="0" />
			 <img src="../images/jpg/refreshicon.jpg" title="<%=LC.L_Refresh_Notifications%>" alt="<%=LC.L_Refresh_Notifications%><%--Refresh Notifications*****--%>"  border="0"	>
			 </A>
		<% } %>

		</td>

		<td><%=(description.length() == 0) ? "-": description%></td>
		<td>
		<!--KM-#5882 -->
		<A
			href="calendarstatus.jsp?mode=M&amp;selectedTab=10&amp;protocolId=<%=id%>&amp;srcmenu=<%=src%>&amp;calassoc=S&amp;studyId=<%=studyId%>"
			onClick="return f_check_perm(<%=pageRight%>,'V')"><%=statusText%></A>
		</td>
		<td><A
			href="protocolhistory.jsp?mode=M&amp;srcmenu=tdmenubaritem3&amp;selectedTab=10&amp;protocolId=<%=id%>&amp;calledFrom=S&amp;calassoc=S"
			onClick="return f_check_perm(<%=pageRight%>,'V')"> <%=LC.L_Status_Dets%><%--Status Details*****--%></A></td>
		<td><a href="#"
			onclick="window.open('repRetrieve.jsp?protId=<%=id%>&amp;repId=106&amp;repName=<%=LC.L_Pcol_CalPreview%><%--Protocol Calendar Preview*****--%>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><img border="0" title="<%=LC.L_Preview%>" alt="<%=LC.L_Preview%>" src="./images/Preview.gif" ><%//=LC.L_Preview%><%--Preview*****--%></a>&nbsp;&nbsp;&nbsp;
		<a href="#"
			onclick="window.open('repRetrieve.jsp?protId=<%=id%>&amp;repId=107&amp;repName=<%=LC.L_Mock_Schedule%><%--Mock Schedule*****--%>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><%=LC.L_Schedule%><%--Schedule*****--%></a>
		</td>
		<td  align="center"><!--<A
			href="studycalendardelete.jsp?srcmenu=<%=src%>&amp;selectedTab=<%=tab%>&amp;calendarId=<%=id%>&amp;delMode=null&calassoc=S"
			onClick="return confirmBox('<%--=name--%>',<%--=pageRight--%>,'<%--=status--%>')"><img src="./images/delete.gif" border="0" align="left"/></A>-->
			<A
			href="studycalendardelete.jsp?srcmenu=<%=src%>&amp;selectedTab=<%=tab%>&amp;calendarId=<%=id%>&amp;delMode=null&calassoc=S&amp;from=admin"
			onClick="return confirmBox('<%=name%>',<%=pageRight%>,'<%=status%>', '<%=statusText%>')"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/></A></td>

	</tr>
	<%}

                        %>
</table>

</tr>
<tr> <!-- TR for main table  -->
<jsp:include page="studyschedule.jsp" flush="true">
<jsp:param name="page1" value="<%=page1%>"/>
<jsp:param name="studyId" value="<%=studyId%>"/>
<jsp:param name="selectedTab" value="10"/>
<jsp:param name="generate" value="<%=generate%>"/>
<jsp:param name="nextpage" value="<%=pagenum%>"/>
<jsp:param name="orderBy" value="<%=orderBy%>"/>
<jsp:param name="orderType" value="<%=orderType%>"/>
<jsp:param name="visit" value="<%=visit%>"/>
<jsp:param name="month" value="<%=month%>"/>
<jsp:param name="year" value="<%=year%>"/>
<jsp:param name="event" value="<%=event%>"/>
<jsp:param name="dstatus" value="<%=evtStatus%>"/>
<jsp:param name="visitext" value="<%=visitxt%>"/>
<jsp:param name="eventext" value="<%=evttxt%>"/>
<jsp:param name="monthyrtext" value="<%=durtxt%>"/>
<jsp:param name="statustext" value="<%=ststxt%>"/>
<jsp:param name="calassoc" value="S"/>
<jsp:param name="schdate" value="<%=calSchDate%>"/>
<jsp:param name="calId" value="<%=protocolId%>"/>

</jsp:include>
</tr>
</table>
<%} // end of if page right
                        else {

                        %> <b>
<p class="defcomments"><%=MC.M_NoRgt_ToViewAssocCal%><%--You do not have rights to view Associated Calendars*****--%></p>
</b> <%}%>
<%} else { //page right and formright  %> <jsp:include
	page="accessdenied.jsp" flush="true" /> <%}
                } //end of if body for check on studyId

            } //end of if session times out

            else

            {

            %> <jsp:include page="timeout.html" flush="true" /> <%} //end of else body for page right

        %>
<div><jsp:include page="bottompanel.jsp" flush="true" /></div>
</DIV>
<div class="mainMenu" id="emenu"><jsp:include page="getmenu.jsp"
	flush="true" /></div>
</body>

</html>
