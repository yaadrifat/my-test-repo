<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=MC.M_Pat_FrmRespBrow%><%--<%=LC.Pat_Patient%> >> >> Form Response Browser*****--%></title>

<script>

//function openWin(formId,studyId)
//{
//	windowName= window.open("studyformdetails.jsp?formId="+formId+"&mode=N&studyId="+studyId+"&formDispLocation=S","information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=500");
	//windowName.focus();
//}


function checkPerm(pageRight,orgRight)
{
	//Modified by Manimaran to fix the Bug2387.
	if (f_check_perm_org(pageRight,orgRight,'N') == true)
	{
		return true ;
 	}
	else
	{
		return false;
	}
}

</script>

<%
String src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
int patId=EJBUtil.stringToNum(request.getParameter("pkey"));

String calledFromForm = "";
       calledFromForm = request.getParameter("calledFromForm");

      if (StringUtil.isEmpty(calledFromForm))
      {
     	calledFromForm = "";
      }

   String outputTarget = ""; //if a target name is passed, open the new and edit page in this target
	   outputTarget = request.getParameter("outputTarget");

	 if (StringUtil.isEmpty(outputTarget))
	 {
	 	outputTarget = "";
	 }

	 String specimenPk = "";
    specimenPk = request.getParameter("specimenPk");

     if (StringUtil.isEmpty(specimenPk))
	 {
	 	specimenPk = "";
	 }


   if (calledFromForm.equals(""))
   {
%>
	<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>"/>
	</jsp:include>

 <% }
 else
 { %>
 	<jsp:include page="popupJS.js" flush="true"/>
	<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
	<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>
 	<%
 }
 %>

<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="lnkformB" scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="personB" scope="request" class="com.velos.eres.web.person.PersonJB"/>
<jsp:useBean id="userSiteB" scope="page" class="com.velos.eres.web.userSite.UserSiteJB" />

<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.eres.business.common.CtrlDao,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.business.common.*,com.velos.eres.web.user.UserJB"%>


<%
  personB.setPersonPKId(patId);
  personB.getPersonDetails();
  String patientCode = personB.getPersonPId();
  patientCode = (patientCode==null)?"":patientCode;

if (calledFromForm.equals(""))
   {
%>
	<div class="BrowserTopn" id="div1">

	<jsp:include page="patienttabs.jsp" flush="true">
	<jsp:param name="pkey" value="<%=patId%>"/>
	<jsp:param name="patientCode" value="<%=StringUtil.encodeString(patientCode)%>"/>
	<jsp:param name="page" value="patient"/>
	</jsp:include>
</div>
<div class="BrowserBotN BrowserBotN_top1" id="div2">


<%
	} //end of if calledFromForm
HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)) {

	int pageRight = 0;
	int orgRight = 0;
	String mode=request.getParameter("mode");
	String patRightString = null;

	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

	if (grpRights!= null)
	{
		patRightString = grpRights.getFtrRightsByValue("PATFRMSACC");
	}
	if (patRightString != null)
	{
		pageRight = EJBUtil.stringToNum((patRightString));
	}

	String modRight = (String) tSession.getValue("modRight");
	int patProfileSeq = 0, formLibSeq = 0;

        String selectedStudy=request.getParameter("selectedStudy");
        selectedStudy=(selectedStudy==null)?"":selectedStudy;
	 // To check for the account level rights
	 acmod.getControlValues("module");
	 ArrayList acmodfeature =  acmod.getCValue();
	 ArrayList acmodftrSeq = acmod.getCSeq();
	 char formLibAppRight = '0';
	 char patProfileAppRight = '0';
	 patProfileSeq = acmodfeature.indexOf("MODPATPROF");
	 formLibSeq = acmodfeature.indexOf("MODFORMS");
	 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
	 patProfileSeq = ((Integer) acmodftrSeq.get(patProfileSeq)).intValue();
	 formLibAppRight = modRight.charAt(formLibSeq - 1);
	 patProfileAppRight = modRight.charAt(patProfileSeq - 1);

	  ArrayList arrFrmNames = null;
		 ArrayList arrNumEntries = null;
 		 ArrayList arrFrmIds = null;
		 ArrayList arrEntryChar = null;
		 ArrayList arrFrmInfo = new ArrayList();
		 String frmName = "";
		 String entryChar="";
		 String numEntries = "";
		 String frmInfo= "";
		 int formId = 0;
		 String firstFormInfo = "";
		 String lfDispInPat = "";

  	     String strFormId = request.getParameter("formPullDown");

	 	 String accId=(String)tSession.getAttribute("accountId");
		 int iaccId=EJBUtil.stringToNum(accId);
 	     String userId = (String) tSession.getValue("userId");
	     UserJB userB = (UserJB) tSession.getValue("currentUser");
   	     String siteId = userB.getUserSiteId();

		 LinkedFormsDao lnkFrmDao = new LinkedFormsDao();
		 lnkFrmDao = lnkformB.getPatientForms(iaccId,patId, EJBUtil.stringToNum(userId), EJBUtil.stringToNum(siteId));

		 arrFrmIds = lnkFrmDao.getFormId();
		 arrFrmNames = lnkFrmDao.getFormName();
		 arrEntryChar = lnkFrmDao.getFormEntryChar();
		 arrNumEntries = lnkFrmDao.getFormSavedEntriesNum();

		 if (strFormId==null) {
		    if (arrFrmIds.size() > 0) {
       		 	formId = EJBUtil.stringToNum(arrFrmIds.get(0).toString());
       			entryChar = arrEntryChar.get(0).toString();
       			numEntries = arrNumEntries.get(0).toString();
       			firstFormInfo = arrFrmIds.get(0).toString() + "*"+ entryChar + "*" + arrNumEntries.get(0).toString();
			} else {
				  firstFormInfo="lab";
			}
		 }
		 else if (strFormId.equals("lab")) {
		 	 firstFormInfo="lab";
	     }
		 else
		 {
		 	 StringTokenizer strTokenizer = new StringTokenizer(strFormId,"*");
    		  if (calledFromForm.equals(""))
    	 		 {
		 	     formId = EJBUtil.stringToNum(strTokenizer.nextToken());
				 entryChar = strTokenizer.nextToken();
			     numEntries = strTokenizer.nextToken();
				 firstFormInfo = strFormId;
				}
				else
    			 {
    			 	formId = EJBUtil.stringToNum(strTokenizer.nextToken());
    			 	entryChar = strTokenizer.nextToken();



    			 	//to get numentries, use the data already retrieved

    			 	int idxForm = 0;

    			 	if (arrFrmIds != null)
    			 	{
    			 		idxForm = arrFrmIds.indexOf(new Integer(formId));

    			 		 if (idxForm >=0)
    			 		{
    			 			if (arrNumEntries != null && arrNumEntries.size() >idxForm )
    			 			{
    			 					numEntries = ((Integer) arrNumEntries.get(idxForm)).toString();
    			 					if (StringUtil.isEmpty(numEntries))
    			 					{
    			 						numEntries = "0";
    			 					}
    			 			}
    			 		}
    			 		else
    			 		{
    			 						numEntries = "0";
    			 		}


    			 	}
    			 	else
    			 	{
    			 						numEntries = "0";
    			 	}

    			 	//lnkformB.findByFormId(formId );
		   		 	//numEntries = lnkformB.getLfDataCnt();


    			 	//prepare strFormId again
    			 	strFormId = formId + "*" + entryChar + "*" + numEntries;
    			 	firstFormInfo = strFormId;
    			 	//lfType = lnkformB.getLFDisplayType();

    			 }
		 }

		//check for access rights if form is called from a 'link form' link
      if (!calledFromForm.equals(""))
    	{

    		if (!arrFrmIds.contains(new Integer(formId)))
    		{
	    		// check if the form is hidden and user has access to it
				if (! lnkformB.hasFormAccess(formId, EJBUtil.stringToNum(userId)))
				{
    			//user does not have access rights to this form
    			pageRight  = 0;
    			}
    		}

    	}

    orgRight = userSiteB.getUserPatientFacilityRight(EJBUtil.stringToNum(userId), patId );

    //if the form link is called from patient portal, grant all rights
		 String ignoreRights = "";
		 ignoreRights = (String) tSession.getValue("pp_ignoreAllRights") ;

		 String pp_linkedModuleFormAfter = "";



		if (StringUtil.isEmpty(ignoreRights))
		{
			ignoreRights = "false";
		}else
		{

			pp_linkedModuleFormAfter = (String) tSession.getAttribute("pp_linkedModuleFormAfter");
			pp_linkedModuleFormAfter=(pp_linkedModuleFormAfter==null)?"":pp_linkedModuleFormAfter;
			if (pp_linkedModuleFormAfter.length()==0) pp_linkedModuleFormAfter="N";
		}

		if (ignoreRights.equals("true") || (! StringUtil.isEmpty(outputTarget) ))
		{
			pageRight = 7;

			formLibAppRight = '1';
			patProfileAppRight = '1';
			orgRight = 7;
		}




//JM: 04-13-2006:
//	if (((String.valueOf(formLibAppRight).compareTo("1") == 0) || (String.valueOf(patProfileAppRight).compareTo("1") == 0)) &&(pageRight >=4 && orgRight >= 4))
	if (((String.valueOf(formLibAppRight).compareTo("1") == 0) || (String.valueOf(patProfileAppRight).compareTo("1") == 0)) &&(pageRight >=4))
	{
		 for (int i=0;i<arrFrmIds.size();i++)
		 {  //store the formId, entryChar and num Entries separated with a *
		  	 frmInfo = arrFrmIds.get(i).toString() + "*"+ arrEntryChar.get(i).toString() + "*" + arrNumEntries.get(i).toString();
		 	 arrFrmInfo.add(frmInfo);
		 }

		 if (formId > 0) {
		 	lnkformB.findByFormId(formId);
		 	lfDispInPat = lnkformB.getLfDispInPat();
		 }

	 	 //to add lab entry in the DD
	 	 arrFrmInfo.add("lab");
	 	arrFrmNames.add(LC.L_Lab); /*arrFrmNames.add("Lab"); *****/

	 	 String dformPullDown = EJBUtil.createPullDownWithStrNoSelect("formPullDown",firstFormInfo, arrFrmInfo , arrFrmNames);
		 StringBuffer formBuffer = new StringBuffer(dformPullDown);

		 //submit the form when the user selects the form name
		 if (calledFromForm.equals(""))
		 {
			 formBuffer.replace(0,7,"<SELECT onChange=\"document.patform.submit();\"");
		 } /*else
		 	 {
		 	 formBuffer.replace(0,7,"<SELECT DISABLED onChange=\"document.patform.submit();\"");
		 	 } */

		 dformPullDown = formBuffer.toString();


		 //date filter
	  	 String formFillDt = "";
	 	 formFillDt = request.getParameter("formFillDt") ;
 	 	 if (formFillDt == null) formFillDt = "ALL";
		 String dDateFilter = EJBUtil.getRelativeTimeDD("formFillDt",formFillDt);


	   String linkFrom = "PA";
	   String hrefName = "formfilledpatbrowser.jsp?srcmenu="+src;
	   String modifyPageName = "patformdetails.jsp?srcmenu="+src;
	   String formName= "";
	   String formStatus = "";
	   String statSubType = "";

         if (formId >0) {
	     	   formLibB.setFormLibId(formId);
   	   		   formLibB.getFormLibDetails();
			   formName=formLibB.getFormLibName();

		   	   formStatus = formLibB.getFormLibStatus();
			   CodeDao cdao = new CodeDao();
			   cdao.getCodeValuesById(EJBUtil.stringToNum(formStatus));
			   ArrayList arrSybType = cdao.getCSubType();
			   statSubType = arrSybType.get(0).toString();
		}
	   %>

		<Form method="post" name="patform" action="formfilledpatbrowser.jsp" onsubmit="">
		<input type="hidden" name="srcmenu" value=<%=src%>>
		<input type="hidden" name="selectedTab" value=<%=selectedTab%>>
		<input type="hidden" name="pkey" value=<%=patId%>>
		<input type="hidden" name="calledFromForm" value=<%=calledFromForm%>>
		
		<input type="hidden" name="previousPage" value="patient"/>
		
		<div class="tmpHeight"></div>
			<table width="99%" border="0" cellspacing="0" cellpadding="0" class=" midAlign">
<tr><td>&nbsp;</td></tr>
    		<tr >
    		<td width="30%" align="right">
    			<%
		 if (calledFromForm.equals(""))
		  {
		%>
			<%=LC.L_Frm_Name%><%-- Form Name*****--%>:
		<%
		}

		%>

		</td>
		<%
		 if (calledFromForm.equals(""))
		  {
		%>

		<td width="50%" >  <%=dformPullDown%>

		<%
		}
		%>


		<%
 	  	String jsCheck="";

		//if (lfDispInPat.equals("0")) { 	//if Form Display in Patient Profile is 1 then the user cannot add new data to the form

			//Only Active forms can be answered
			 if (statSubType.equals("A") &&(entryChar.equals("M")) || ((entryChar.equals("E")) && (numEntries.equals("0"))))
			 {

			  	String hrefurl = "";

			  	hrefurl= "patformdetails.jsp?srcmenu="+src+"&selectedTab="+selectedTab+"&formId="+formId+"&mode=N&formDispLocation=PA&entryChar="+entryChar+"&pkey="
			  	+	patId+"&formFillDt="+formFillDt+"&formPullDown="+firstFormInfo+"&calledFromForm="+calledFromForm
			  	+ "&specimenPk=" + specimenPk  ;

			  	if (!StringUtil.isEmpty(outputTarget))
			  	{


			  		hrefurl= hrefurl +  "&outputTarget=" + outputTarget;

			  		jsCheck = " onClick='openWindowCommon(\""+hrefurl+"\",\""+ outputTarget +"\");' ";
			  		hrefurl = "#";
			  	}
			  	else
			  	{
			  		jsCheck = " onClick ='javascript:if(checkPerm(\"" +pageRight+ "\",\""+ orgRight +"\" )){window.location=\""+hrefurl+"\"} return false;'";           //Modified for fixing Bug#9090 : Raviesh
			  	}
			  	
			  	if (!"formpreview".equals(calledFromForm)) { %>
			  <button id="newlink" type="button" <%=jsCheck%>><%=LC.L_New%></button>			 <!-- Modified for fixing Bug#9090 : Raviesh -->
		     <%
			     }

		   }
		// } else if (lfDispInPat.equals("1")){

			 if ((entryChar.equals("E")) && (numEntries.equals("0"))){
			 %>



			<script>
			            //Changed the mode=N again. Rajeev made the mode=M for some fix. Since he is not sure about the change
			            //I reset the mode back to N to fix a reopened bug 2127
						window.location.href="patformdetails.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&formId=<%=formId%>&mode=N&formDispLocation=PA&pkey=<%=patId%>&entryChar=<%=entryChar%>&formFillDt=<%=formFillDt%>&formPullDown=<%=firstFormInfo%>&calledFromForm=<%=calledFromForm%>&page=patient&specimenPk=<%=specimenPk%>" ;

			</script>
			</div>

		 <%}
	//}%>
		</tr>
    		<tr><td>&nbsp;</td></tr>
    		</table>
    	<%  if ("formpreview".equals(calledFromForm) && !StringUtil.isEmpty(jsCheck)) { %>
    	<table>
    		<tr>
    		<td width="50%" >
			  <A id="newlink" style="text-decoration:underline; cursor:hand" type="submit" <%=jsCheck%> ><%=LC.L_New%></A>
    		</td>
    		</tr>
    	</table>
    	<%  }  %>

	<%if (firstFormInfo.equals("lab")){}
	else
	{
	if (ignoreRights.equals("false")) {  %>
				<table align="center">
    		<tr>
    		<td>
    		<P class="blackComments"><%=MC.M_Prev_EntriesForFrm%><%-- Previous entries for Form*****--%>: <B>"<%=formName%>"</B></P>
		</td>
		<td colspan="2">
		</td>
		<% if (calledFromForm.equals(""))
		{ %>

			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Filter_ByDate%><%-- Filter By Date*****--%> &nbsp;  <%=dDateFilter%> </td>
			<td><button type="submit"><%=LC.L_Search%></button></td>
			<%
    		 }
   			%>
		</tr>
		</table>
	<% }
	}%>
	 </Form>


	<!--include the jsp for showing the records-->

	<%
	//Modified by Manimaran to fix the Bug2387


	if (firstFormInfo.equals("lab")){%>
	<jsp:include page="labbrowser.jsp" flush="true">
   	 <jsp:param name="selectedTab" value="<%=selectedTab%>"/>
	 <jsp:param name="mode" value="<%=mode%>"/>
	 <jsp:param name="page" value="patient"/>
	 <jsp:param name="pkey" value="<%=patId%>"/>
	 <jsp:param name="patientCode" value="<%=StringUtil.encodeString(patientCode)%>"/>
	 <jsp:param name="fromlab" value="pat"/>
	 <jsp:param name="pageRightOrg" value="<%=pageRight%>"/>
	 <jsp:param name="orgRightPat" value="<%=orgRight%>"/>
	 <jsp:param name="selectedStudy" value="<%=selectedStudy%>"/>
    	 </jsp:include>

	<%}else{

	 if (arrFrmIds.size() > 0  || ( ignoreRights.equals("true") && ( entryChar.equals("E") || pp_linkedModuleFormAfter.equals("P") ) ) ) {
	%>
    	 <jsp:include page="formdatarecords.jsp" flush="true">
    	 <jsp:param name="formId" value="<%=formId%>"/>
    	 <jsp:param name="linkFrom" value="<%=linkFrom%>"/>
    	 <jsp:param name="hrefName" value="<%=hrefName%>"/>
    	 <jsp:param name="modifyPageName" value="<%=modifyPageName%>"/>
    	 <jsp:param name="selectedTab" value="<%=selectedTab%>"/>
    	 <jsp:param name="entryChar" value="<%=entryChar%>"/>
    	 <jsp:param name="formFillDt" value="<%=formFillDt%>"/>
    	 <jsp:param name="formPullDown" value="<%=firstFormInfo%>"/>
    	 <jsp:param name="srcmenu" value="<%=src%>"/>
    	 <jsp:param name="pageRight" value="<%=pageRight%>"/>
     	 <jsp:param name="orgRight" value="<%=orgRight%>"/>

    	 <jsp:param name="statSubType" value="<%=statSubType%>"/>
		 <jsp:param name="patientCode" value="<%=StringUtil.encodeString(patientCode)%>"/>
		 <jsp:param name="calledFromForm" value="<%=calledFromForm%>"/>
		 <jsp:param name="outputTarget" value="<%=outputTarget%>"/>
		 <jsp:param name="specimenPk" value="<%=specimenPk%>"/>
		 <jsp:param name="previousPage" value="patient"/>

    	 </jsp:include>
		 	 <%}
	   else if( ignoreRights.equals("false"))
	   {
	%>
	  <P class="defComments"><%=MC.M_NoAssocForms%><%-- No associated Forms.*****--%></P>
    <%  }

    if ( ignoreRights.equals("true") && pp_linkedModuleFormAfter.equals("N") )
    {
	    %>
	    	<script>
				var newl = document.getElementById("newlink");
				//alert("newl " + newl );
				window.location =newl ;
			//	newl.click();
	    	</script>
	    <%
     }

   }

	} //end of if body for page right
	else{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%
	} //end of else body for page right
}//end of if body for session
else {
%>
	<jsp:include page="timeout.html" flush="true"/>
<%}
%>
<br>
<%if (calledFromForm.equals(""))
  {
%>

<div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

	<DIV class="mainMenu" id = "emenu">
	  <jsp:include page="getmenu.jsp" flush="true"/>
	</DIV>
<%
}
		%>
</body>
</html>

