<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%--<title>Create Account in eResearch</title>*****--%>
<title><%=MC.M_Create_EresAcc%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB" />
<jsp:useBean id="accountB" scope="page" class="com.velos.eres.web.account.AccountJB" />
<jsp:useBean id="addressUserB" scope="page" class="com.velos.eres.web.address.AddressJB" />
<jsp:useBean id="siteB" scope="page" class="com.velos.eres.web.site.SiteJB" />
<jsp:useBean id="addressSiteB" scope="page" class="com.velos.eres.web.address.AddressJB" />
<jsp:useBean id="ctrl" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>


<%@ page language = "java" import="java.text.*,com.velos.eres.business.group.*,com.velos.eres.business.common.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="formjs.js"></SCRIPT>
<SCRIPT Language="javascript">


 function  validate(formobj){
 //    formobj=document.accountCreation;

/*	flag = 0
	for (count=0;count < accountType.length;count++ )
     		if (!(validate_col('User Account',formobj.accountType))){
	 alert("account");
	 return false;
	}*/

     if (!(validate_col('Activation/Deactivation/Reactivation Date',formobj.accStartDt))) return false;

	 if (!(validate_col('Storage Space',formobj.storage))) return false;

 	 if (!(validate_col('Number of Users',formobj.totalusers))) return false;








	 // if (formobj.userStatus.value==''||formobj.userStatus.value==null)
	//   alert(formobj.userStatus.value);
	// alert("Please enter data in all mandatory fields");
	//  return false;










	 if(formobj.storage.value < -1) {
	 	/*alert("Please enter a valid storage space");*****/
		alert("<%=MC.M_PlsEtr_StrgSpc%>");
		formobj.storage.focus();
		return false;
	 }

	 if(formobj.totalusers.value < -1) {
	 	/*alert("Please enter a valid number of users");*****/
		alert("<%=MC.M_PlsEtr_NumUsr%>");
		formobj.totalusers.focus();
		return false;
	 }

}

function toggleValue(formobj)
{
 if (formobj.ldapenabled.checked){
     formobj.ldapenabled.value="Y";
     }
    else{
      formobj.ldapenabled.value="";
      }
}

function toggleValueWS(formobj)
{
 if (formobj.wsenabled.checked){
     formobj.wsenabled.value="Y";
     }
    else{
      formobj.wsenabled.value="";
      }
}

function changeRights(obj,row, frmname)

	{

	  selrow = row ;

	  totrows = frmname.totalrows.value;
		//alert(totrows);
	  if (totrows > 1)

		rights =frmname.modrights[selrow].value;

	  else

		rights =frmname.modrights.value;

	  objName = obj.name;

	  //alert(selrow +"Count" +totrows +"Rights" +rights) ;

	  if (obj.checked)

	  	  {
			if (totrows > 1 )
			{
			frmname.modules[selrow].checked = true;
			}
			else
			{
			frmname.modules.checked = true;
			}

			rights = 1;
		}
		else
		{
			if (totrows > 1 )
			{
			frmname.modules[selrow].checked = false;
			}
			else
			{
			frmname.modules.checked = false;
			}

			rights = 0;

		}

		if (totrows > 1 )

			frmname.modrights[selrow].value = rights;

		else

			frmname.modrights.value = rights;



		//alert("Rights" +rights);

	}
/////////////////////

</SCRIPT>

<%




	ctrl.getControlValues("module");
	int ctrlrows = ctrl.getCRows();

	ArrayList feature =  ctrl.getCValue();
	ArrayList ftrDesc = ctrl.getCDesc();
	ArrayList ftrSeq = ctrl.getCSeq();
	ArrayList ftrRight = new ArrayList();

	String modRight = "";
	int rLength = 0;


	CodeDao cd = new CodeDao();
	CodeDao cd1 = new CodeDao();
	CodeDao cd2 = new CodeDao();
	CodeDao cd3 = new CodeDao();

	String from = request.getParameter("from");
	if(from == null) from = "";

	String mode = request.getParameter("mode");
	String msgId = request.getParameter("msgId");
    	int userId = EJBUtil.stringToNum(request.getParameter("userId"));
	int accountId = 0;
	String dPrimSpl = "";
      String dJobType = "" ;
	String dAccRoleType ="";
	String dTimeZone="";
	String accCreationDate = "";
	userB.setUserId(userId);
	userB.getUserDetails();
	if (userB.getUserAccountId() != null) {
		accountId = EJBUtil.stringToNum(userB.getUserAccountId());
		accCreationDate = accountB.getCreationDate(accountId);
		accCreationDate = DateUtil.dateToString(java.sql.Date.valueOf(accCreationDate.substring(0,10)));


		accountB.setAccId(accountId);
		accountB.getAccountDetails();

		modRight= accountB.getAccModRight();
		rLength = modRight.length();

		for (int counter = 0; counter <= (rLength - 1);counter ++)
		{
			ftrRight.add(String.valueOf(modRight.charAt(counter)));
		}

	}

	if (userB.getUserPerAddressId() != null)
	{
		addressUserB.setAddId(EJBUtil.stringToNum(userB.getUserPerAddressId()));
		addressUserB.getAddressDetails();
	}
	if (userB.getUserSiteId() != null) {
		siteB.setSiteId(EJBUtil.stringToNum(userB.getUserSiteId()));
		siteB.getSiteDetails();
	}
	if (siteB.getSitePerAdd() != null) {
		addressSiteB.setAddId(EJBUtil.stringToNum(siteB.getSitePerAdd()));
		addressSiteB.getAddressDetails();
	}

	cd.getCodeValues("job_type");
	cd1.getCodeValues("prim_sp");
	cd2.getCodeValues("role_type");
	cd3.getTimeZones();

	String accMaxUsers = "";
	String accMaxPortals = ""; //JM:
	String accMaxStorage = "";
	String  userLastName="";
	String  userFirstName="";
	String  userMidName="";
	String  userAddPri="";
	String  userAddCity="";
	String  userAddState="";
	String  userAddZip="";
	String  userAddCountry="";
	String  userAddPhone="";
	String  userAccMailFlag="";
	String  accPubFlag="";
	String  userAddEmail="";
	String 	userAccType = "";
	String  userAccRoleType="";
	String  userJobType="";
	String  userSpl="";
	String  userWrkExp="";
	String  userPhaseInv="";
	String userCode = "";
	String  siteName="";
	String  accStartDt="";
	String  siteAddPri="";
	String  siteAddCity="";
	String  siteAddState="";
	String  siteAddZip="";
	String  siteAddPhone="";
	String  siteAddCountry = "";
	String  login="";
	String  pwd="";
	String status="";
	String accNote="";
	String timeZone="",ldapEnabled="",wsEnabled="";
	String autoGenStudy ="";//KM
	String autoGenPatient ="";

	if (mode.equals("M")) {
		userLastName =userB.getUserLastName();
		userLastName = (   userLastName  == null      )?"":(  userLastName ) ;

		userFirstName= userB.getUserFirstName();
		userFirstName = (   userFirstName  == null      )?"":(  userFirstName ) ;

		userMidName= userB.getUserMidName();
		userMidName = (   userMidName  == null      )?"":(  userMidName ) ;

		userAddPri= addressUserB.getAddPri();
		userAddPri = (   userAddPri  == null      )?"":(  userAddPri ) ;

		userAddCity =addressUserB.getAddCity();
		userAddCity = (   userAddCity  == null      )?"":(  userAddCity ) ;


		userAddState =addressUserB.getAddState();
		userAddState = (   userAddState  == null      )?"":(  userAddState ) ;

		userAddCountry= addressUserB.getAddCountry();
		userAddCountry = (   userAddCountry  == null      )?"":(  userAddCountry ) ;

		userAddZip= addressUserB.getAddZip();
		userAddZip = (   userAddZip  == null      )?"":(  userAddZip ) ;


		userAddPhone= addressUserB.getAddPhone();
		userAddPhone = (   userAddPhone  == null      )?"":(  userAddPhone ) ;

		userAccMailFlag = accountB.getAccMailFlag();
		userAccMailFlag = (   userAccMailFlag  == null      )?"":(  userAccMailFlag ) ;

		userCode = userB.getUserCode((new Integer(userId)).toString());
		accMaxUsers = accountB.getAccMaxUsr();

		//JM:
		accMaxPortals = accountB.getAccMaxPortal();
		accMaxPortals = (accMaxPortals==null)?"":accMaxPortals;

		accMaxStorage = accountB.getAccMaxStorage();

		accNote = accountB.getAccNote();
		accNote = (   accNote  == null      )?"":(  accNote ) ;

		if (userAccMailFlag == null){
			 userAccMailFlag="N";
		}
		accPubFlag = accountB.getAccPubFlag();
		if (accPubFlag == null) {
			accPubFlag="N";
		}

		//KM
		autoGenStudy = accountB.getAutoGenStudy();
		autoGenStudy = (autoGenStudy == null) ? "" : (autoGenStudy);
		autoGenPatient = accountB.getAutoGenPatient();
		autoGenPatient = (autoGenPatient == null) ? "" : (autoGenPatient);

		userAddEmail =addressUserB.getAddEmail();
		userAccType =accountB.getAccType();
		userAccRoleType =accountB.getAccRoleType();
		userJobType =userB.getUserCodelstJobtype();
		timeZone = userB.getTimeZoneId();
		userSpl= userB.getUserCodelstSpl();

		userWrkExp =userB.getUserWrkExp();
		userWrkExp = (   userWrkExp  == null      )?"":(  userWrkExp ) ;

		userPhaseInv= userB.getUserPhaseInv();
		userPhaseInv = (   userPhaseInv  == null      )?"":(  userPhaseInv ) ;

		siteName =siteB.getSiteName();
		accStartDt =accountB.getAccStartDate();


		if(accStartDt == "null" || accStartDt.equals("")) {
			
			accStartDt = DateUtil.dateToString(new Date());
		} else {
			accStartDt =accountB.getAccStartDate();
		}

		siteAddPri =addressSiteB.getAddPri();
		siteAddPri = (   siteAddPri  == null      )?"":(  siteAddPri ) ;

		siteAddCity =addressSiteB.getAddCity();
		siteAddCity = (   siteAddCity  == null      )?"":(  siteAddCity ) ;

		siteAddState =addressSiteB.getAddState();
		siteAddState = (   siteAddState  == null      )?"":(  siteAddState ) ;

		siteAddCountry =addressSiteB.getAddCountry();
		siteAddCountry = (   siteAddCountry  == null      )?"":(  siteAddCountry ) ;

		siteAddZip= addressSiteB.getAddZip();
		siteAddZip = (   siteAddZip  == null      )?"":(  siteAddZip ) ;

		siteAddPhone= addressSiteB.getAddPhone();
		siteAddPhone = (   siteAddPhone  == null      )?"":(  siteAddPhone ) ;

		login = userB.getUserLoginName();
		pwd = userB.getUserPwd();
//		status = accountB.getAccStatus();
		status = userB.getUserStatus();

		if (accMaxUsers == null) {
			if(userAccType.equals("G")){
				accMaxUsers = "5";
			} else {
				accMaxUsers = "1";
			}
		}


		if (accMaxStorage == null) {
			if(userAccType.equals("G")){
				accMaxStorage = "25";
			} else {
				accMaxStorage = "15";
			}
		}


		if (status == null){
			status="";
		}
	      dJobType = cd.toPullDown("jobType", EJBUtil.stringToNum(userJobType));
	      dPrimSpl = cd1.toPullDown("primarySpeciality", EJBUtil.stringToNum(userSpl));
	      dAccRoleType = cd2.toPullDown("accRoletype", EJBUtil.stringToNum(userAccRoleType));
		  dTimeZone = cd3.toPullDown("timeZone", EJBUtil.stringToNum(timeZone));

		 ldapEnabled=accountB.getLdapEnabled();
		 ldapEnabled=(ldapEnabled==null)?"N":ldapEnabled;

		 wsEnabled=accountB.getWSEnabled();
		 wsEnabled=(wsEnabled==null)?"N":wsEnabled;

	}else{
		
		accStartDt = DateUtil.dateToString(new Date());
	   dJobType = cd.toPullDown("jobType");
	   dPrimSpl = cd1.toPullDown("primarySpeciality");
	  dAccRoleType = cd2.toPullDown("accRoletype");
	  dTimeZone = cd3.toPullDown("timeZone");
	}


%>


<body>

<br>

<DIV class="browserDefault_veloshome" id="div1">
<%
HttpSession tSession = request.getSession(true);
	if (sessionmaint.isValidSession(tSession))

		{
%>

<Form name="accountCreation" method="post" action="updateaccountuser.jsp"   onSubmit = "return validate(document.accountCreation)"  >

<P class = "sectionHeadings">
<%--Personal Details*****--%><%=LC.L_Personal_Dets%>
</P>

<input type="hidden" name="userId" Value="<%=userId%>">
<input type="hidden" name="accountId" Value="<%=accountId%>">
<input type="hidden" name="msgId" Value="<%=msgId%>">


<table width="100%" cellspacing="0" cellpadding="0">
	<tr>
    <td width="25%">
       <%--User ID*****--%><%=LC.L_User_Id%>
    </td>
    <td>
	<input type="text" name="userCode" size = 35 MAXLENGTH = 30 Value="<%=userCode%>" READONLY>
    </td>
  </tr>
  <tr>
    <td width="25%">
       <%--First Name*****--%><%=LC.L_First_Name%>
    </td>
    <td>
	<input type="text" name="userFirstName" size = 35 MAXLENGTH = 30 Value="<%=userFirstName%>" READONLY>
    </td>
  </tr>
  <tr>
    <td width="25%">
       <%--Last Name*****--%><%=LC.L_Last_Name%>
    </td>
    <td>
	<input type="text" name="userLastName" size = 35 MAXLENGTH = 30 Value="<%=userLastName%>" READONLY>
    </td>
  </tr>
  <tr>
    <td width="25%">
       <%--Address*****--%><%=LC.L_Address%>
    </td>
    <td>
	<input type="text" name="userAddress" size = 35 MAXLENGTH = 50 Value="<%=userAddPri%>" READONLY>
    </td>
  </tr>
   <tr>
    <td width="25%">
       <%--City*****--%><%=LC.L_City%>
    </td>
    <td>
	<input type="text" name="userCity" size = 35  MAXLENGTH = 30 Value="<%=userAddCity%>" READONLY>
    </td>
  </tr>
  <tr>
    <td width="25%">
       <%--State*****--%><%=LC.L_State%>
    </td>
    <td>
	<input type="text" name="userState" size = 35 MAXLENGTH = 30 Value="<%=userAddState%>" READONLY>
    </td>
  </tr>
  <tr>
    <td width="25%">
       <%--Country*****--%><%=LC.L_Country%>
    </td>
    <td>
	<input type="text" name="userCountry" size = 35 MAXLENGTH = 30 Value="<%=userAddCountry%>" READONLY>
    </td>
  </tr>
  <tr>
    <td width="25%">
       <%--Zip/Postal Code*****--%><%=LC.L_ZipOrPostal_Code%>
    </td>
    <td>
	<input type="text" name="userZip" size = 15 MAXLENGTH = 15 Value="<%=userAddZip%>" READONLY>
    </td>
  </tr>
  <tr>
    <td width="25%">
       <%--Phone*****--%><%=LC.L_Phone%>
    </td>
    <td>
	<input type="text" name="userPhone" size = 35 MAXLENGTH = 100 Value="<%=userAddPhone%>" READONLY>
    </td>
  </tr>
  <tr>
    <td width="25%">
       <%--E-Mail*****--%><%=LC.L_EhypMail%>
    </td>
    <td>
	<input type="text" name="userEmail" size = 35 MAXLENGTH = 100 Value="<%=userAddEmail%>" READONLY>
    </td>
  </tr>

  	<tr>
	<td width="25%"> <%--Time Zone*****--%><%=LC.L_Time_Zone%>
 <FONT class="Mandatory">* </FONT> </td>
	<td > <%=dTimeZone%> </td>
	</tr>




      <tr>
    <td width="25%">
       <%--Role Type*****--%><%=LC.L_Role_Type%>
    </td>
    <td>
	<%=dAccRoleType%>
    </td>
  </tr>
  <tr>
    <td width="25%">
       <%--Job Type*****--%><%=LC.L_Job_Type%>
    </td>
    <td>
	<%=dJobType%>
    </td>
  </tr>

</table>
<br>

<table width="100%" cellspacing="0" cellpadding="0" >
  <tr>
    <td colspan = "3" id="tdComments">
	<%--If you are an investigator, please complete the following:*****--%><%=MC.M_IfInvestigator_Complete%>:
    </td>
  </tr>
	<tr height=10>	</tr>
  <tr>
    <td width="25%">
       <%--Primary Speciality*****--%><%=LC.L_Primary_Speciality%>
    </td>
    <td width="75%">
	<%=dPrimSpl%>
   </td>
  </tr>
  <tr>
     <td  width="25%">
       <%--How many years have you been involved in clinical research?*****--%>
	  	   <%=MC.M_Years_YouInClnclRes%>
    </td>
    <td >
	<input type="text" name="userWrkExp" size = 20 MAXLENGTH = 100 Value="<%=userWrkExp%>" READONLY>
    </td>
  </tr>
  <tr>
    <td width="25%">
	<%--Which phases of trials have you been involved with?*****--%><%=MC.M_PhaseTrials_YouInvolved%>
    </td>
    <td >
	<input type="text" name="userPhaseInv" size = 35 MAXLENGTH = 100 Value="<%=userPhaseInv%>" READONLY>
    </td>
  </tr>
</table>

<P class = "sectionHeadings">
<%--Organization Details*****--%><%=LC.L_Org_Dets%>
</P>

<table width="100%" cellspacing="0" cellpadding="0" >
  <tr>
    <td width="25%">
       <%--Organization Name*****--%><%=LC.L_Organization_Name%>
    </td>
    <td>
	<input type="text" name="siteName" size = 35 MAXLENGTH = 50 Value="<%=siteName%>" READONLY>
    </td>
  </tr>

  <tr>
    <td >
       <%--Address*****--%><%=LC.L_Address%>
    </td>
    <td>
	<input type="text" name="siteAddress" size = 35 MAXLENGTH = 50 Value="<%=siteAddPri%>" READONLY>
    </td>
  </tr>
   <tr>
    <td >
       <%--City*****--%><%=LC.L_City%>
    </td>
    <td>
	<input type="text" name="siteCity" size = 35  MAXLENGTH = 30 Value="<%=siteAddCity%>" READONLY>
    </td>
  </tr>
  <tr>
    <td>
       <%--State*****--%><%=LC.L_State%>
    </td>
    <td>
	<input type="text" name="siteState" size = 35 MAXLENGTH = 30 Value="<%=siteAddState%>" READONLY>
    </td>
  </tr>

  <tr>
    <td>
       <%--Country*****--%><%=LC.L_Country%>
    </td>
    <td>
	<input type="text" name="siteCountry" size = 35 MAXLENGTH = 30 Value="<%=siteAddCountry%>" READONLY>
    </td>
  </tr>
  <tr>
    <td >
       <%--Zip/Postal Code*****--%><%=LC.L_ZipOrPostal_Code%>
    </td>
    <td>
	<input type="text" name="siteZip" size = 15 MAXLENGTH = 15 Value="<%=siteAddZip%>" READONLY>
    </td>
  </tr>
  <tr>
    <td >
       <%--Phone*****--%><%=LC.L_Phone%>
    </td>

 <td>
	<input type="text" name="sitePhone" size = 20 MAXLENGTH = 100 Value="<%=siteAddPhone%>" READONLY>
    </td>
  </tr>
<% if(from.equals("accountUsers")) { %>
</table>
<br>
 <table width="100%">


<tr>
<td align=center>
		<button onclick="window.history.back();"><%=LC.L_Back%></button>
</td>
</tr>
</table>


<%
	} else { // else for if to check from
%>
  <tr height="10"></tr>
  <tr>
    <td colspan="2">
	<%
		if(userAccMailFlag.equals("Y")){
	%>
	<input type="checkbox" name="accMailFlag" Value="<%=userAccMailFlag%>" CHECKED READONLY>
	<%
		} else {
	%>
	<input type="checkbox" name="accMailFlag" Value="<%=userAccMailFlag%>" READONLY>
	<%
		}
	%>
	&nbsp;
       <%--Check if you want Velos to contact you regarding useful information*****--%><%=MC.M_ChkVelosCont_UsefulInfo%>
    </td>
  </tr>

  <tr>
    <td colspan = "2">
	<%
		if(accPubFlag.equals("Y")){
	%>
	<input type="checkbox" name="accPubFlag" Value="<%=accPubFlag%>" CHECKED READONLY>
	<%
		} else {
	%>
	<input type="checkbox" name="accPubFlag" Value="<%=accPubFlag%>" READONLY>
	<%
		}
	%>
	&nbsp;
	<%--Check if you agree to Velos giving your name to other companies that might have useful information for you*****--%><%=MC.M_ChkAgreeVelos_CompInfo%>
    </td>
  </tr>
</table>

<br>
<table width="100%" cellspacing="0" cellpadding="0">
	<tr>
	<td width="25%"><%--Registration Agreement Accepted On*****--%><%=MC.M_Reg_AgmntAccepted%>
</td>
    	<td>
	<td>
	<input type="text" name="acceptedOn" size = 20 MAXLENGTH = 100 Value="<%=accCreationDate%>" READONLY>
	</td>
</table>
<br>

<P class = "sectionHeadings">
<%--For Velos Use*****--%><%=MC.M_For_VelosUse%>
</P>

<table width="100%" cellspacing="0" cellpadding="0" >
  <tr >
    <td width="25%" id="tdComments">
	<%--User Account Type*****--%><%=LC.L_User_AccType%>
 <FONT class="Mandatory">* </FONT>
    </td>
    <td >
	<select name=userAccType>
<%
		if(userAccType.equals("I")) {
%>
		<option value=I SELECTED><%--Individual*****--%><%=LC.L_Individual%></option>
<%		} else {
%>
		<option value=I><%--Individual*****--%><%=LC.L_Individual%></option>
<%		}
		if(userAccType.equals("G")) {
%>
		<option value=G SELECTED><%--Group*****--%><%=LC.L_Group%></option>
<%		} else {
%>
		<option value=G><%--Group*****--%><%=LC.L_Group%></option>
<%		}
%>

<%
		if(userAccType.equals("E")) {
%>
		<option value=E SELECTED><%--Evaluation*****--%><%=LC.L_Evaluation%></option>
<%		} else {
%>
		<option value=E><%--Evaluation*****--%><%=LC.L_Evaluation%></option>
<%		}
%>
	</select>
	</td>
	</tr>
<tr><td><BR></td> </tr>
  <tr>
    <td width="25%">
       <%--User Login ID*****--%><%=LC.L_User_LoginId%>
    </td>
    <td>
	<input type="text" name="userLogin" size = 35 MAXLENGTH = 20 Value="<%=login%>" READONLY>
    </td>
  </tr>
  <tr>
    <td width="25%">
       <%--LDAP Enabled*****--%><%=LC.L_Ldap_Enabled%>
    </td>
    <td>
    <%if (ldapEnabled.equals("Y")){ %>
	<input type="checkbox" name="ldapenabled" Value="<%=ldapEnabled%>" onclick="toggleValue(document.accountCreation)" checked>
	<%}else{ %>
	<input type="checkbox" name="ldapenabled" Value="<%=ldapEnabled%>" onclick="toggleValue(document.accountCreation)">
	<%} %>

    </td>
  </tr>
  <tr>
    <td width="25%">
       <%--Web Services Enabled*****--%><%=LC.L_Web_ServicesEnabled%>
    </td>
    <td>
    <%if (wsEnabled.equals("Y")){ %>
	<input type="checkbox" name="wsenabled" Value="<%=wsEnabled%>" onclick="toggleValueWS(document.accountCreation)" checked>
	<%}else{ %>
	<input type="checkbox" name="wsenabled" Value="<%=wsEnabled%>" onclick="toggleValueWS(document.accountCreation)">
	<%} %>

    </td>
  </tr>
  <tr>
    <td width="25%">
       <%--Space allotted for uploading files (enter "-1" for allotting unlimited space)*****--%>
		<%=MC.M_UpldFiles_UnlimitedSpace%>

	   <FONT class="Mandatory">* </FONT>
    </td>
    <td>
	<input type="text" name="storage" size = 5 MAXLENGTH = 4 Value=<%=accMaxStorage%> > <%--MB*****--%><%=LC.L_Mb%>
    </td>
  </tr>

  <tr>
    <td width="25%">
       <%--Number of Users allowed (enter "-1" for allowing unlimited users)*****--%><%=MC.M_EtrMns1ForUnltd_Usr%>
<FONT class="Mandatory">* </FONT>
    </td>
    <td>
	<input type="text" name="totalusers" size = 10 MAXLENGTH = 10 Value=<%=accMaxUsers%>>
    </td>
  </tr>

<!-- JM:07May2007 -->
<tr>
    <td width="25%">
       <%--Number of Portals allowed (enter "-1" for allowing unlimited Portals)*****--%><%=MC.M_EtrMns1ForUnltd_Portals%>
    </td>
    <td>
	<input type="text" name="totalportals" size = 10 MAXLENGTH = 10 Value=<%=accMaxPortals%>>
    </td>
  </tr>

<!-- // -->
  <tr>
    <td width="25%">
       <%--Notes*****--%><%=LC.L_Notes%>
    </td>
	<td>
       <TextArea name="accNote" rows=3 cols = 30  MAXLENGTH = 500><%=accNote%></TextArea>
    </td>
  </tr>

</table>
<br>

<Input type="hidden" name="totalrows" value= <%=ctrlrows%> >

<table width="430" cellspacing="0" cellpadding="0">
<tr><td colspan = 2><%--Check the Enhanced Modules granted:*****--%><%=MC.M_CheckEnhanced_ModGrant%>
 </td> </tr>
 <%
 String ftr = "";
 String desc= "";
 Integer seq ;
 String rightChar = "";
 String isChecked = "";
 //ctrlrows = 13;

 for(int count=0;count<ctrlrows;count++)
 {
	 isChecked = "";
 	 ftr = (String) feature.get(count);
 	 desc = (String) ftrDesc.get(count);
	 seq = (Integer) ftrSeq.get(count);
	 rightChar = (String) ftrRight.get(count);
	 if (rightChar.equals("1"))
	 {
	 	isChecked = "CHECKED";
	 }
 %>
 	<tr>

	<TD align="center">
	<Input type="hidden" name="modrights" value= <%=rightChar%> >
	<Input type="checkbox" name="modules" onclick="changeRights(this,<%=count%>, document.accountCreation)" <%=isChecked%>>
    </TD>

	<td>
	<%=desc%>
	</td>
	</tr>

 <%
  }
 //Added by Manimaran for the Enh.#GL7
 String checkStudy ="";
 String checkPatient ="";
 if (autoGenStudy.equals("1")) {
	 checkStudy ="checked";
 }
 if(autoGenPatient.equals("1")) {
	 checkPatient = "checked";
 }


 %>
 <tr>
	<td align="center">
	<Input type="checkbox" name="autoGenPatient" onclick="" <%=checkPatient%>>
	</td>
	<td>
	<%--Enable System-generated <%=LC.Pat_Patient%> ID*****--%>
	<%=MC.M_EnableSys_PatID%>
	</td>
 </tr>

 <tr>
	<td align="center">
	<Input type="checkbox" name="autoGenStudy" onclick="" <%=checkStudy%>>
	</td>
	<td>
	<%=MC.M_EnableSys_StdNum%><%-- Enable System-generated <%=LC.Std_Study%> Number--%>
	</td>
 </tr>


 </table>
<br>

<table width="430" cellspacing="0" cellpadding="0">
  <tr>
    <td>
<%if (status.equals("A")){%>
	<input type="Radio" name="userStatus" value="A" CHECKED><%--Activate*****--%><%=LC.L_Activate%>

	<input type="Radio" name="userStatus" value="D"><%--Deactivate*****--%><%=LC.L_Deactivate%>

	<input type="Radio" name="userStatus" value="A"><%--Reactivate*****--%><%=LC.L_Reactivate%>

	<input type="Radio" name="userStatus" value="J"><%--Rejected*****--%><%=LC.L_Rejected%>
<%}else if (status.equals("D")){%>
	<input type="Radio" name="userStatus" value="A"><%--Activate*****--%><%=LC.L_Activate%>
	<input type="Radio" name="userStatus" value="D" CHECKED><%--Deactivate*****--%><%=LC.L_Deactivate%>
	<input type="Radio" name="userStatus" value="A"><%--Reactivate*****--%><%=LC.L_Reactivate%>
	<input type="Radio" name="userStatus" value="J"><%--Rejected*****--%><%=LC.L_Rejected%>
<%}else if (status.equals("R")){%>
	<input type="Radio" name="userStatus" value="A"><%--Activate*****--%><%=LC.L_Activate%>
	<input type="Radio" name="userStatus" value="D" ><%--Deactivate*****--%><%=LC.L_Deactivate%>
	<input type="Radio" name="userStatus" value="A" CHECKED><%--Reactivate*****--%><%=LC.L_Reactivate%>
	<input type="Radio" name="userStatus" value="J"><%--Rejected*****--%><%=LC.L_Rejected%>
<%}else if (status.equals("J")){%>
	<input type="Radio" name="userStatus" value="A"><%--Activate*****--%><%=LC.L_Activate%>
	<input type="Radio" name="userStatus" value="D"><%--Deactivate*****--%><%=LC.L_Deactivate%>
	<input type="Radio" name="userStatus" value="A"><%--Reactivate*****--%><%=LC.L_Reactivate%>
	<input type="Radio" name="userStatus" value="J" CHECKED><%--Rejected*****--%><%=LC.L_Rejected%>
<%}else {%>
	<input type="Radio" name="userStatus" value="A" CHECKED><%--Activate*****--%><%=LC.L_Activate%>
	<input type="Radio" name="userStatus" value="D" ><%--Deactivate*****--%><%=LC.L_Deactivate%>
	<input type="Radio" name="userStatus" value="A" ><%--Reactivate*****--%><%=LC.L_Reactivate%>
	<input type="Radio" name="userStatus" value="J"><%--Rejected*****--%><%=LC.L_Rejected%>
<%}%>
    </td>
  </tr>

</table>

<table>
   <tr>
    <td width="300">
       <%=MC.M_ActDeactReact_Dt%><%--Activation/Deactivation/Reactivation Date --%> <FONT class="Mandatory">* </FONT>
    </td>
     <td>
	<input type="text" name="accStartDt" value="<%=accStartDt%>" size = 15 >
    </td>
  </tr>
</table>




<table width="150" >
<tr>
<td align="right">
<!--<input type="Submit" name="submit" value="Submit">-->
	  <button type="submit"><%=LC.L_Submit%></button>
</td>
</tr>
</table>
<% } // end of if for from check

}//end of if body for session

else

{
%>
  <jsp:include page="timeout_admin.html" flush="true"/>
  <%
}

%>
</Form>
<div>
<jsp:include page="bottompanel.jsp" flush="true"/>

</div>


</div>

	<jsp:include page="velosmenus.htm" flush="true"/>


</body>
</html>
