<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<style type="text/css">
.yui-dt-liner { white-space:normal; } 
.yui-skin-sam .yui-dt td.up { background-color: #efe; } 
.yui-skin-sam .yui-dt td.down { background-color: #fee; }
</style>
<%@page import="com.velos.eres.web.objectSettings.ObjectSettingsCache,com.velos.eres.web.objectSettings.ObjectSettings"%>
<%@page import="com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.esch.business.common.SchCodeDao"%>
<%@page import="com.velos.eres.business.ulink.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*" %>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<%
ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
ArrayList tabList = null;
String titleStr = LC.L_Calendar;/*String titleStr = "Calendar";*****/
if (sessionmaint.isValidSession(request.getSession(true)))
{
    String accId = (String) request.getSession(true).getValue("accountId");
    
    ArrayList tempList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "protocol_tab");
    for (int iX=0; iX<tempList.size(); iX++) {
        ObjectSettings settings = (ObjectSettings) tempList.get(iX);
        if ("6".equals(settings.getObjSubType())) {
            if (settings.getObjDispTxt() != null) {
            	Object[] arguments = {titleStr,settings.getObjDispTxt()};
            	titleStr = VelosResourceBundle.getLabelString("L_Gtgt",arguments);
                /*titleStr +=  " >> " + settings.getObjDispTxt();*****/
            }
            break;
        } 
    }
    tempList = null;
}
String src= StringUtil.htmlEncodeXss(request.getParameter("srcmenu"));
String mode= StringUtil.htmlEncodeXss(request.getParameter("mode")); //Yk: Added for Enhancement :PCAL-19743
%>
<title><%=titleStr %></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<jsp:include page="ui-include.jsp" flush="true"/>
</head>
<script type="text/javascript">
//YK: Added for PCAL-20461
var defaultHeight = navigator.userAgent.indexOf("MSIE") != -1 ? 650 : 500;
jQuery.noConflict();
function openSequenceDialog(calStatus) //YK: Added for PCAL-20461 - Opens the Dialog Window
{
	if(calStatus!='A' && calStatus!='F' && calStatus!='D') {
		jQuery("#editVisitDiv").dialog({ 
		height: defaultHeight,width: 700,position: 'center' ,modal: true,
		closeText:'',
		buttons: [
		          {
		              text: L_Close,
		              click: function() { 
		        	  jQuery(this).dialog("close");
		              }
		          }
		      ],
		close: function() { $j("#editVisitDiv").dialog("destroy"); },
		beforeClose: function(event, ui) { if(!checkBeforeClose()){ return false;} }
        });
	}else {
	alert(M_SrySeqDisp_ModActDact);
		}
  
}
//YK: Added for PCAL-20461 - Loads the Visit of the Calender
var loadModifySequenceDialog = function(calStatus, pgRight,calledFrom, protocolId,calassoc,visit) {

	var needToCall = false;
	if(VELOS.dataGrid.checkDataChange())
	{
		alert(M_PlsPrewSave_MoveFwd);
		return false;
	}
	if (f_check_perm(pgRight,'E')){
		if(calStatus!='A' && calStatus!='F' && calStatus!='D') {
			needToCall=true;
		}else{
			needToCall=false;
			alert(M_SrySeqDisp_ModActDact);
		}
	
		var myObj = jQuery('#embedDataHere');
		jQuery('#embedDataHere').html("<p><%=LC.L_Loading%>...<img class=\"asIsImage\" src=\"../images/jpg/loading_pg.gif\" /></p>"); // Wipe out last data for performance
			
		if (navigator.userAgent.indexOf("MSIE") != -1) {
			// For IE it's faster to open dialog and then load
			openSequenceDialog(calStatus); 
		    jQuery('#embedDataHere').load("manageEventSequence.jsp?calledFrom="+calledFrom+"&protocolId="+protocolId+"&calStatus="+calStatus+"&calassoc="+calassoc+"&visitSel="+visit+"" );
		} else {
			// For FF, it's faster to load and then open
			jQuery('#embedDataHere').load("manageEventSequence.jsp?calledFrom="+calledFrom+"&protocolId="+protocolId+"&calStatus="+calStatus+"&calassoc="+calassoc+"&visitSel="+visit+""  );
			openSequenceDialog(calStatus);
		}
		    
	}
}
//Yk: Added for Enhancement :PCAL-19743
function openEvtDetailsPopUp(link) {
	windowName=window.open(link,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=800,height=610,top=70 ,left=150");
    windowName.focus();
}

//PCAL-22322	AGODARA
function addEventsToVisitsWindow(link,pageRight,calStatDesc) {

	var calStatus = document.getElementById('calStatus').value;
	
	if (calStatus == 'W' || calStatus == 'O') {
		if (f_check_perm(pageRight,'E') == false) {
			 return false;
		}
	}
	if ((calStatus == 'F') || (calStatus == 'A')|| (calStatus == 'D')) {
		alert(M_CntEvt_VisitCal + " " + calStatDesc);
		return false;
	}
	windowName=window.open(link,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=1000,height=575 top=300,left=250");
	windowName.focus();
}
</script>
<% 
HttpSession tSession = request.getSession(true); 
String accountId = "";
String studyId = "";
String grpId = "";
String usrId = "";
if (sessionmaint.isValidSession(tSession)) {
	accountId = (String) tSession.getValue("accountId");
	studyId = (String) tSession.getValue("studyId");
	grpId = (String) tSession.getValue("defUserGroup");
	usrId = (String) tSession.getValue("userId");
	
	if ("N".equals(request.getParameter("mode"))) {
		studyId = "";
		tSession.removeAttribute("studyId");
	}
%>
<body class="yui-skin-sam yui-dt yui-dt-liner">
<div id="overDiv" style="position:absolute;visibility:hidden; z-index:1100;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
<%
	String from = "version";
	String tab = request.getParameter("selectedTab");
    String calassoc = StringUtil.trueValue(request.getParameter("calassoc"));
	String studyIdForTabs = request.getParameter("studyId");
    String includeTabsJsp = "irbnewtabs.jsp";
    int pageRight = 0;
	String calledFrom = request.getParameter("calledFrom");
	if (calledFrom.equals("S")) {
		StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
		if ((stdRights.getFtrRights().size()) == 0){
			pageRight= 0;
		} else {
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
		}
	} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
	}
	%>
	<DIV class="BrowserTopn" id = "div1">
		<jsp:include page="protocoltabs.jsp" flush="true">
		<jsp:param name="calassoc" value="<%=StringUtil.htmlEncodeXss(calassoc)%>" />
		<jsp:param name="selectedTab" value="<%=StringUtil.htmlEncodeXss(tab)%>" />
		</jsp:include>
	</DIV>
	<DIV class="BrowserBotN BrowserBotN_CL_1" id = "div2" style="overflow: auto;">
	<%
	if (pageRight > 0) {

	} // end of pageRight > 0
	String protocolId = request.getParameter("protocolId");
	if(protocolId == null || protocolId == "" || protocolId.equals("null") || protocolId.equals("")) {
%>
	<jsp:include page="calDoesNotExist.jsp" flush="true"/>
<%
	} else {
	String calStatus = null;
	String calStatusPk = "";
	String calStatDesc = "";
	String protocolName = ""; // YK : Added for Bug#7528
	SchCodeDao scho = new SchCodeDao();
	if (pageRight > 0) {
	    String tableName = null;
 		if("L".equals(calledFrom) || "P".equals(calledFrom)){
	   		tableName ="event_def";
			eventdefB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventdefB.getEventdefDetails();
			//KM-#DFin9
			// YK : Added for Bug#7528
			protocolName = eventdefB.getName();
			calStatusPk = eventdefB.getStatCode();
			calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
			calStatDesc = scho.getCodeDescription(EJBUtil.stringToNum(calStatusPk));

	   	} else {
	   		tableName ="event_assoc";
			eventassocB.setEvent_id(EJBUtil.stringToNum(protocolId));
			eventassocB.getEventAssocDetails();
			//KM-#DFin9
			// YK : Added for Bug#7528
			protocolName = eventassocB.getName();
			calStatusPk = eventassocB.getStatCode();
			calStatus = scho.getCodeSubtype(EJBUtil.stringToNum(calStatusPk)).trim();
			calStatDesc = scho.getCodeDescription(EJBUtil.stringToNum(calStatusPk));

	   	}
 		// YK : Added for Bug#7528
 		tSession.setAttribute("protocolname",protocolName);
 		
		String duration = StringUtil.trueValue(request.getParameter("duration"));
		StringBuffer urlParamSB = new StringBuffer();
		urlParamSB.append("protocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&calledFrom=").append(StringUtil.htmlEncodeXss(calledFrom))
		.append("&calledfrom=").append(StringUtil.htmlEncodeXss(calledFrom))
		.append("&duration=").append(StringUtil.htmlEncodeXss(duration))
		.append("&calassoc=").append(StringUtil.htmlEncodeXss(calassoc))
		.append("&calstatus=").append(StringUtil.htmlEncodeXss(request.getParameter("calStatus")))
		.append("&mode=").append(StringUtil.htmlEncodeXss(request.getParameter("mode")))
		.append("&calProtocolId=").append(StringUtil.htmlEncodeXss(protocolId))
		.append("&tableName=").append(tableName)
		.append("&doGet=").append(pageRight);
		;
%>
<script>
//Yk: Modified for Enhancement :PCAL-19743
function reloadDataGrid(sortOrder) {
	YAHOO.example.DataGrid = function() {
		var args = {
			urlParams: "<%=urlParamSB.toString()%>&sortOrder="+sortOrder,
			dataTable: "datagrid"
		};
		
		myDataGrid = new VELOS.dataGrid('fetchProtJSON.jsp', args);
		myDataGrid.startRequest();
    }();
}
YAHOO.util.Event.addListener(window, "load", function() {
	reloadDataGrid('asc');       //Akshi: Added for Bug#7646
});

// YK : Added for Bug#7470 	
function resetSort()
{
	document.getElementById('sortOrder').value="";
	reloadDataGrid();
}

</script>

<%
%>
<div class="tmpHeight"></div>
  <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign">
  <tr height="20" >
  	<td colspan="2">
  		<%=LC.L_Cal_Status%><%--Calendar Status*****--%>: <%=calStatDesc%>&nbsp;&nbsp;<BR/>
  	</td>
  	<td colspan="2">
		<%if ((!protocolId.equals("null"))) {%>
			<%=LC.L_Preview_Calendar%><%--<%=LC.L_Preview%> <%=LC.L_Calendar%> Preview*****--%>: 
		<%}%>	  	
	</td>
  </tr>
  <tr align="left">
  <!--KM-#DFin9-->
  <td>
    <%if ((calStatus.equals("A")) || (calStatus.equals("F")) || (calStatus.equals("D"))){%>
	       <FONT class="Mandatory"><%=MC.M_Changes_CntSvd%><%--Changes cannot be saved*****--%></Font>&nbsp;&nbsp;&nbsp;&nbsp;
			<%}%>
	<%if(calStatus.equals("F") || (calStatus.equals("A"))|| (calStatus.equals("D"))){%>
	<button id="save_changes" name="save_changes" disabled style="visibility:hidden"><%=LC.L_Preview_AndSave%><%--Preview and Save*****--%></button>
	&nbsp;<button id="add_eve_tovisits" name="add_eve_tovisits" disabled style="visibility:hidden"><%=MC.M_AddMul_EvtToVisits%></button>
	<%}else{%>
	<button id="save_changes" name="save_changes" onclick="if (f_check_perm(<%=pageRight%>,'E')) { VELOS.dataGrid.saveDialog(); }"><%=LC.L_Preview_AndSave%><%--Preview and Save*****--%></button>		
	<% String veLink="addevtvisits.jsp?mode="+mode+"&srcmenu="+src+"&duration="+duration+"&protocolid="+protocolId+"&calledfrom="+calledFrom+"&calstatus="+calStatus+"&calltime=new"+"&calassoc="+calassoc;	%>
	&nbsp;<button id="add_eve_tovisits" name="add_eve_tovisits" onclick="addEventsToVisitsWindow('<%=veLink%>',<%=pageRight%>,'<%=calStatDesc%>')"><%=MC.M_AddMul_EvtToVisits%></button>
	<%}%>
	
	<%--//YK: Added for PCAL-20461 - Button to Access the Modify Sequence/Display window --%>
	<%if (!calStatus.equals("A") && !calStatus.equals("F") && !calStatus.equals("D"))
		{%>
    &nbsp;<button id="openNewWindow" name="openNewWindow" onclick="loadModifySequenceDialog('<%=calStatus%>','<%=pageRight %>','<%=calledFrom%>','<%=protocolId%>','<%=calassoc%>','ALL')"><%=LC.L_ModSeq_Disp%></button>
    <%}else{
    	%>
    &nbsp;<button id="openNewWindow" name="openNewWindow" disabled  style="visibility:hidden"><%=LC.L_ModSeq_Disp%></button>
        <%
    }%>
    </td>
    <td><a href="javascript:void(0);" onclick="resetSort();"><%=LC.L_Reset_Sort%></a></td> <%-- YK : Modified for Bug#7470 --%>
    <%if ((!protocolId.equals("null"))) {%>
			<td><!-- Ak:Added for enhancement PCAL-20071 -->
			<%
			String acId=(String)tSession.getValue("accountId");
			
			ReportDaoNew repDao = new ReportDaoNew();
			repDao.getReports("rep_calendar",EJBUtil.stringToNum(acId));
			
			StringBuffer sb = new StringBuffer();
	
		 	sb.append("<SELECT id='calReps' NAME='calReps'>") ;
			for (int cnt = 0; cnt <= repDao.getPkReport().size() -1 ; cnt++)
			{
				sb.append("<OPTION value = '"+ repDao.getPkReport().get(cnt)+"' >" + repDao.getRepName().get(cnt)+ "</OPTION>");
	
			}
			sb.append("</SELECT>");
			String repdD = sb.toString();
			%>
			<%=repdD%></td>
			<td align="left">
				<button onClick="return validateReport(document.protocoltab);"><%=LC.L_Display%></button>
			</td>
	<%}%>
  </tr>
  <tr>
  	<td colspan="6">
  		<div id="datagrid"></div>
  	</td>
  </tr>
  </table>
  
  <%-- YK: Added for PCAL-20461 Start of Edit Visit modal dialog --%>
<div id="editVisitDiv" title="<%=LC.L_ModSeq_Disp%>" style="display:none">
<div id='embedDataHere'>
</div>
</div>
<%-- Yk: Modified for Enhancement :PCAL-19743 --%>
<input type="hidden" name="sortOrder" id="sortOrder" value="asc">     <%-- Akshi: Added for Bug#7646 --%>
<input type="hidden" name="srcmenu" id="srcmenu" value="<%=src%>">
<input type="hidden" name="duration" id="duration" value="<%=duration%>">
<input type="hidden" name="protocolId" id="protocolId" value="<%=protocolId%>">
<input type="hidden" name="calledFrom" id="calledFrom" value="<%=calledFrom%>">
<input type="hidden" name="studyId" id="studyId" value="<%=studyId%>"/>
<input type="hidden" name="mode" id="mode" value="<%=mode%>">
<input type="hidden" name="calStatus" id="calStatus" value="<%=calStatus%>">
<input type="hidden" name="calassoc" id="calassoc" value="<%=calassoc%>">
<%-- End of Edit Visit modal dialog --%>
  
  
  
<%
  } // end of pageRight
  else {
%>
<!--12-04-2011 #6013 @Ankit  -->
	<jsp:include page="accessdenied.jsp" flush="true"/>
<%
  } // end of else of pageRight
} // end of else of invalid protocolId
} else { // else of valid session
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
} // end of else of valid session
%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu" > 
  <jsp:include page="getmenu.jsp" flush="true"/> 
</div>
</body>
</html>
<%!
private boolean isAccessibleFor(int pageRight, char type) {
    switch(type) {
    case 'V': return ((pageRight & 0x0004) >> 2) == 1;
    case 'E': return ((pageRight & 0x0002) >> 1) == 1;
    case 'N': return  (pageRight & 0x0001) == 1;
    }
    return false;
}
%>