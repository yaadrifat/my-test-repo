<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<TITLE> <%=LC.L_Validate_User%><%--Validate User*****--%> </TITLE>
<%!
String htmlEncode(String input) {
    if (input == null) { return ""; }
    return StringUtil.htmlEncodeXss(input);
}
%>



<HEAD>
<%
   //load the setting here again . If the user does not come from ereslogin.jsp but instead through a post from another url. we need to be able
   //to load these settings.
	Configuration.readSettings();

%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>
<script>
var formWin = null;

function openHome(form,act){
var w = screen.availWidth-10;
var h = screen.availHeight-120;
document.login.target="formWin";
document.login.action=act;
//myHome = open('donotdelete.html','formWin','resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=' + h + ',width=' + w)

myHome = open(act,'formWin','resizable=yes,menubar=yes,toolbar=yes,location=yes,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=' + h + ',width=' + w)
if (myHome && !myHome.closed)
{
//document.login.submit();
window.history.back();
 myHome.focus();
}
else {
	popupBlock();
}
/*if (myHome && !myHome.closed) myHome.focus();
else {
	alert("popup blocker,please check" + document.login);
	document.login.target="";
	document.login.action="sendtospace.jsp";
	alert(document.login.action);
	document.login.submit();
}*/

//form.submit();
void(0);
}

function popupBlock()
{
	document.login.target="";
	logname=document.login.loguser.value;
	logurl=document.login.logurl.value;
	document.login.action="ereslogin.jsp?mode=popup&logname="+logname+"&logurl="+logurl;
	document.login.submit();
}
	</script>
<body>
<form name="login"  METHOD="POST">


<jsp:useBean id="userLB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id="userSessionB" scope="request" class="com.velos.eres.web.userSession.UserSessionJB"/>
<jsp:useBean id="accountB" scope="request" class="com.velos.eres.web.account.AccountJB"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="alnot" scope="request" class="com.velos.esch.web.alertNotify.AlertNotifyJB"/>
<jsp:useBean id="codeLst" scope="request" class="com.velos.eres.web.codelst.CodelstJB"/>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>


<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB" />

<!-- Gopu : import the objects for customized filed -->
<%@ page import="com.velos.esch.business.common.SchCodeDao,java.util.*,java.text.*,com.velos.eres.service.util.*,java.util.*,java.text.SimpleDateFormat,com.velos.eres.web.user.ConfigFacade, com.velos.eres.business.common.*"%>

<% String login;

//modifed by Gopu for MAHI Enahncement on 8th Mar 2005
	ConfigFacade.getConfigFacade();

%>

<% login= request.getParameter("username");

//JM: 24Nov2009: #4085
String fromPg = request.getParameter("fromPage");
fromPg = (fromPg==null)?"":fromPg;

int usrId = EJBUtil.stringToNum(request.getParameter("userId"));


	if (fromPg.equals("reset")){
		userB.setUserId(usrId);
		userB.getUserDetails();
		userB.setUserLoginFlag("0");
		userB.updateUser();

%>
	<script>
	window.self.close();
	</script>

<%
	}



HttpSession tSession = request.getSession(true);

HttpSession tempSession = request.getSession(false);
if(!(tempSession == null)){
	tempSession.invalidate();
}
%>

<%! String password; %>

<% password =request.getParameter("password");
	String scWidth = "";

	String scHeight = "";
	int permittedFailedAccessCount = 0;



	scHeight= request.getParameter("scHeight");

	scWidth = request.getParameter("scWidth");



%>
<!-- commented by gopu to fix bugzilla issues # 2439 -->

<input type="hidden" name="logurl" value=<%=request.getServerName()%>>


<input type="hidden" name="loguser" id="loguser" value="<%=htmlEncode(login)%>">
<input type="hidden" name="password" id="password" value="<%=htmlEncode(password)%>">
<input type="hidden" name="userId" id="userId" value="0">

<%

 userLB.validateUser(login,password);

 %>

<%! int output; %>

<%
int velosid = 0;
permittedFailedAccessCount = Configuration.FAILEDACCESSCOUNT;
Configuration conf = new Configuration();
String validLoginId = conf.getVelosAdminId(conf.ERES_HOME+"eresearch.xml");
velosid = EJBUtil.stringToNum(validLoginId);

output = 0;
int failCount = 0; int userFound = 0; int origfailCount = 0;
String loginFlagStr = "";
int loginFlag = 0;
String failedUserStat = "";


output=userLB.getUserId();
loginFlagStr = userLB.getUserLoginFlag();

if ( (!(loginFlagStr == null )) && (!(loginFlagStr.equals(""))))

 loginFlag = EJBUtil.stringToNum(loginFlagStr);
 else
 loginFlag = 0;

 %>

<% if (output==0) {

	userFound = userLB.findUser(login);

	if (userFound == 0)
	{
		failCount = EJBUtil.stringToNum(userLB.getUserAttemptCount());
		origfailCount = EJBUtil.stringToNum(userLB.getUserAttemptCount());
		failedUserStat = userLB.getUserStatus();


		//user with pending accout status (not yet activated by velos admin)
		if (failedUserStat.equals("P") || failedUserStat.equals("D")) {
		%>
    	<SCRIPT>
    	var w = screen.availWidth-10;
	    var h = screen.availHeight-120;
        signError = window.open("signerror.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
        window.history.back();
        if (signError && !signError.closed) signError.focus();
        else
        	popupBlock();
        </SCRIPT>
		<%
		}
	}
	else if (userFound == -1)
	{
	%>
    	<SCRIPT>
    	var w = screen.availWidth-10;
	    var h = screen.availHeight-120;
        signError = window.open("signerror.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
        window.history.back();
		if (signError && !signError.closed)  signError.focus();
		else
			popupBlock();
        </SCRIPT>
    <%
	}

	failCount++;

	if ( (failCount > permittedFailedAccessCount) && (failedUserStat.equals("A")))
	{


		String faildatetime = DateUtil.getCurrentDateTime();


		String faildate =  faildatetime.substring(0,10);
		String failtime =  faildatetime.substring(11);

		userLB.setUserAttemptCount(String.valueOf(failCount));
		//set the user status to blocked
		userLB.setUserStatus("B");
		// Added By Amarnadh for Bugzilla issue #3197
		Integer uId = new Integer(userLB.getUserId());
		userLB.setModifiedBy(uId.toString());

		userLB.updateUser();

		alnot.notifyAdmin(userLB.getUserId(),request.getRemoteAddr(),velosid,faildate,failtime );
		%>

		<SCRIPT>

		var w = screen.availWidth-400;
		var h = screen.availHeight-420;
		signerror = window.open("accountfreeze.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
		window.history.back();
		if (signerror && !signerror.closed) signerror.focus();
		else
			popupBlock();
		</SCRIPT>
	<% return ;
	}
	else
	{
		if(failedUserStat.equals("A"))
		{
			userLB.setUserAttemptCount(String.valueOf(failCount));
			userLB.updateUser();

			//log in er_usersession table
 			String loginTime = DateUtil.getCurrentDateTime();


			String failedUser = String.valueOf(userLB.getUserId());

			userSessionB.setUserId(failedUser);
			userSessionB.setLoginTime(loginTime);
			userSessionB.setIpAdd(request.getRemoteAddr());
			userSessionB.setSuccessFlag("0");
			userSessionB.setUserSessionDetails();



		}
	}

if(failedUserStat.equals("A") || (failedUserStat.equals("B") && origfailCount <= permittedFailedAccessCount))
{
%>

<SCRIPT>

var w = screen.availWidth-10;
var h = screen.availHeight-120;
signerror = window.open("signerror.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
window.history.back();
if (signerror && !signerror.closed) signerror.focus();
else
	popupBlock();
</SCRIPT>

<%
} //end of status = A
else if (failedUserStat.equals("B") && failCount > permittedFailedAccessCount)
{
%>
<SCRIPT>
var w = screen.availWidth-400;
var h = screen.availHeight-420;

signerror = window.open("accountfreeze.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
window.history.back();
if (signerror && !signerror.closed) signerror.focus();
else
	popupBlock();
</SCRIPT>

<%
}

 } else if (loginFlag==1) {

 %>

<SCRIPT>
var w = screen.availWidth-400;
var h = screen.availHeight-400;

// commented by gopu to fix bugzilla issues # 2439

//signerror = window.open("loggedon_err.jsp?userId=<%=login%>"+"&password=<%=password%>","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")


signerror = window.open("loggedon_err.jsp?","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")

//window.history.back();
if (signerror && !signerror.closed) signerror.focus();
else
	popupBlock();


</SCRIPT>


<%} else {
String stat = null;
stat = userLB.getUserStatus();
failCount = EJBUtil.stringToNum(userLB.getUserAttemptCount());


%>
<%


if (stat.equals("A"))

{
%>
<%
	HttpSession thisSession = request.getSession(true);

	thisSession.setAttribute("currentUser",userLB);
  	thisSession.setAttribute("loginname",  userLB.getUserLoginName());

	String uName;
	String sessionTime = null;
	String accSkin = null;
	int userSession = 0;

	accountB.setAccId(EJBUtil.stringToNum(userLB.getUserAccountId()));

	accountB.getAccountDetails();

	thisSession.setAttribute("totalUsersAllowed", accountB.getAccMaxUsr() );
	thisSession.setAttribute("totalPortalsAllowed", accountB.getAccMaxPortal() );//JM:
	thisSession.setAttribute("accMaxStorage", accountB.getAccMaxStorage() );

	String accName = accountB.getAccName();

	accSkin  = accountB.getAccSkin();
	//if (accSkin == null) accSkin  ="default";

	if (accName == null) accName="default";

	thisSession.setAttribute("accName", accName );

	uName = userLB.getUserFirstName() + " " + userLB.getUserLastName();

	String modRight = "";

	modRight = accountB.getAccModRight();
	thisSession.setAttribute("modRight", modRight );


	//set user's protocol management module access in session

	char protocolManagementRight = '0';
	int protocolMgmtSeq = 0,protocolMgmtModSeq = 0;

	modCtlDao.getControlValues("module"); //get extra modules

	ArrayList modCtlDaofeature =  modCtlDao.getCValue();
	ArrayList modCtlDaoftrSeq = modCtlDao.getCSeq();

	protocolMgmtModSeq = modCtlDaofeature.indexOf("MODPROTOCOL");
	protocolMgmtSeq = ((Integer) modCtlDaoftrSeq.get(protocolMgmtModSeq)).intValue();
	protocolManagementRight = modRight.charAt(protocolMgmtSeq - 1);

	thisSession.setAttribute("protocolManagementRight", String.valueOf(protocolManagementRight) );


	String eSign = userLB.getUserESign();

	sessionTime = userLB.getUserSessionTime() ;

	thisSession.setAttribute("userName", uName );
	thisSession.setAttribute("eSign", eSign );
	thisSession.setAttribute("ipAdd",request.getRemoteAddr());
	thisSession.setAttribute("userId", String.valueOf(userLB.getUserId()));
	thisSession.setAttribute("accountId", userLB.getUserAccountId());
	thisSession.setAttribute("userSession", sessionTime);
	thisSession.setAttribute("studyId","");
   	thisSession.setAttribute("studyNo","");
	thisSession.setAttribute("userAddressId",userLB.getUserPerAddressId());
	//set screen width n height in session
	thisSession.setAttribute("appScHeight",scHeight);
   	thisSession.setAttribute("appScWidth",scWidth);
	thisSession.setAttribute("sessionName","er");
	thisSession.setAttribute("accSkin",accSkin);

	//user's default group
	thisSession.setAttribute("defUserGroup",userLB.getUserGrpDefault());



	//Sonia ABrol, 10/08/07 get the timezone desc from database
	//accessing the DAO directly because this patch is needed for Korea selectively and I want to change minimum java files

	SchCodeDao schCode = new SchCodeDao();
	String tzDesc = "";
	tzDesc = schCode.getDefaultTimeZone();

	if (StringUtil.isEmpty(tzDesc))
	{
		tzDesc = "";
	}
	thisSession.setAttribute("defTZDesc",tzDesc);
	// end of TZ change



	//bind the session for tracking after setting all the attributes
	EresSessionBinding eresSession = new EresSessionBinding();
	thisSession.setAttribute("eresSessionBinder",eresSession);

	userSession = Integer.parseInt(sessionTime) * 60;
	thisSession.setMaxInactiveInterval(userSession);



	//by salil
	grpRights.setId(Integer.parseInt(userLB.getUserGrpDefault()));
	grpRights.getGrpRightsDetails();
	thisSession.setAttribute("GRights",grpRights);
	thisSession.setAttribute("defGroupPK",userLB.getUserGrpDefault());

	//end change by salil
	String eSignExp=userLB.getUserESignExpiryDate();
	if (StringUtil.isEmpty(eSignExp)) eSignExp= DateUtil.getFormattedDateString("9999","01","01");
	eSignExp= DateUtil.dateToString(DateUtil.stringToDate(eSignExp, null));

	String pwdExp=userLB.getUserPwdExpiryDate();
	if (StringUtil.isEmpty(pwdExp)) pwdExp=DateUtil.getFormattedDateString("9999","01","01");
	pwdExp= DateUtil.dateToString(DateUtil.stringToDate(pwdExp, null));



	Date eSignExpDate = DateUtil.stringToDate(eSignExp,null);
	Date pwdExpDate = DateUtil.stringToDate(pwdExp,null);
	Date d = new Date();


if(d.after(eSignExpDate) || d.after(pwdExpDate)) {

%>

<SCRIPT>

var w = screen.availWidth-10;
var h = screen.availHeight-120;
pwdexpired = window.open("pwdexpired.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w)
window.history.back();
if (pwdexpired && !pwdexpired.closed) pwdexpired.focus();
else
	popupBlock();
</SCRIPT>

<%

}

else

{



	grpRights.setId(Integer.parseInt(userLB.getUserGrpDefault()));
	grpRights.getGrpRightsDetails();
	thisSession.setAttribute("GRights",grpRights);

	userLB.setUserAttemptCount("0");
	userLB.updateUser();



%>

<!--Session is set<%=userSession%>-->


<%
//Added by IA 12.4.2006
String theme = "";
String homepage = "";
//String description = "";
//CodeDao cd = new CodeDao();

theme = userLB.getUserTheme();
homepage = codeLst.getCodeCustomCol(EJBUtil.stringToNum(theme) );

//description = codeLst.getCodeDescription(EJBUtil.stringToNum(theme) );


if (homepage != null ) {

}else{
	homepage  = "myHome.jsp?srcmenu=tdMenuBarItem1";
}


//<jsp:forward page = "myHome.jsp?srcmenu=tdMenuBarItem1"> </jsp:forward>%>



<SCRIPT>
	openHome('document.login',"<%=homepage%>");

</SCRIPT>


<%

}

}



else
{
  if (stat.equals("B") && failCount <= permittedFailedAccessCount)
	{
   %>
    <SCRIPT>
    //alert("failCount <= permittedFailedAccessCount");
    var w = screen.availWidth-10;
    var h = screen.availHeight-120;
    signError = window.open("signerror.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
    window.history.back();
    if (signError && !signError.closed)    signError.focus();
    else
    	popupBlock();
   </SCRIPT>

    <%
	}
	else if (stat.equals("B") && failCount > permittedFailedAccessCount )
	{
   %>
    <SCRIPT>
	 //alert("failCount > permittedFailedAccessCount");
     var w = screen.availWidth-400;
     var h = screen.availHeight-420;

     signerror = window.open("accountfreeze.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
     window.history.back();
     if (signerror && !signerror.closed)  signerror.focus();
     else
     	popupBlock();
   </SCRIPT>

    <%
	}
	else

	{
	%>
    <SCRIPT>
    //alert("equals(A)");
    var w = screen.availWidth-10;
    var h = screen.availHeight-120;
    signError = window.open("signerror.jsp","","resizable=yes,menubar=yes,toolbar=yes,location=no,status=yes,maximize=yes,left=0,top=0,screenX=0,screenY=0,height=" + h + ",width=" + w,"new")
    window.history.back();
    if (signError && !signError.closed)    signError.focus();
    else
    	popupBlock();
    </SCRIPT>
    <%
	}
}



}



%>
<!-- Added by gopu to fix bugzilla issues # 2439 -->
<script>
	document.getElementById("userId").value = "<%=output%>";
</script>

</form>

</body>

</HTML>

















