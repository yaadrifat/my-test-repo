<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=MC.M_Pers_HPageInEres%><%--Personalize Homepage in eResearch*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<body >

<DIV class="formDefault" id="div1">

<Form name="personalize" method="post" action="updategroup.jsp">

<P class = "sectionHeadings">
	<%=LC.L_User_Name%><%--User Name*****--%>
</P>

<table width="600" cellspacing="0" cellpadding="0">
  <tr> 
    <td>
	<A href=""><%=LC.L_Edit_Profile%><%--Edit Profile*****--%></A>
    </td>
  </tr>
 <tr height="10"> 
 </tr>
  <tr> 
    <td>
	<%=MC.M_LnkBeShown_OnHPage%><%--Links to be shown on your homepage*****--%>
    </td>
    <td >
	<A href=""><%=LC.L_Add_NewLink%><%--Add New Link*****--%></A>
   </td>
  </tr>
</table>
<br>
</div>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>


</Form>
</body>
</html>
