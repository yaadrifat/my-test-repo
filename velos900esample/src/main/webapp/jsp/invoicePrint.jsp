<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Invoice_Print %><%-- Invoice >> Print*****--%></title>

	<%@ page import="java.util.*,java.io.*" %>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
	
	<%@ page language = "java" import="com.velos.eres.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.DOMUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.VelosResourceBundle,com.velos.eres.service.util.*"%>
</head>
		
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="whichcss.js"></SCRIPT>
			
<body>
 <DIV class="popDefault" id="div1">
<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{
%>
	<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
	<%
	String accId=(String)tSession.getValue("accountId");
	String pageRight = request.getParameter("pageRight");
	String invPk = request.getParameter("invPk");
	
	ReportDaoNew rdao = new ReportDaoNew();
	rdao.getReports("rep_invoice",EJBUtil.stringToNum(accId));
	ArrayList pkReps = rdao.getPkReport();
    ArrayList repNames = rdao.getRepName();
    
    
    StringBuffer mainStr = new StringBuffer();
	String repList ="";
	int startIndx =0;
	int stopIndx =0;
	String labelKeyword ="";
	String labelString ="";
	String messageKeyword ="";
	String messageString ="";
	String messageKey ="";
	String tobeReplaced ="";
	String messageParaKeyword []=null;
	int counter = 0;
 	mainStr.append("<SELECT NAME='repList'>") ;
	for (counter = 0; counter <= rdao.getPkReport().size() -1 ; counter++)
	{
		mainStr.append("<OPTION value = "+ rdao.getPkReport().get(counter)+">" + rdao.getRepName().get(counter)+ "</OPTION>");
	}
	mainStr.append("</SELECT>");
	repList = mainStr.toString();

	//KM- Patient Label change for Invoice print dropdown.
	while(repList.contains("VELLABEL[")){
			startIndx = repList.indexOf("VELLABEL[") + "VELLABEL[".length();
			stopIndx = repList.indexOf("]",startIndx);
			labelKeyword = repList.substring(startIndx, stopIndx);
			labelString = LC.getLabelByKey(labelKeyword);
			repList=StringUtil.replaceAll(repList, "VELLABEL["+labelKeyword+"]", labelString);
	}
		
	while(repList.contains("VELMESSGE[")){
		startIndx = repList.indexOf("VELMESSGE[") + "VELMESSGE[".length();
		stopIndx = repList.indexOf("]",startIndx);
		messageKeyword = repList.substring(startIndx, stopIndx);
		messageString = MC.getMessageByKey(messageKeyword);
		repList=StringUtil.replaceAll(repList, "VELMESSGE["+messageKeyword+"]", messageString);
	}
	while(repList.contains("VELPARAMESSGE[")){
		startIndx = repList.indexOf("VELPARAMESSGE[") + "VELPARAMESSGE[".length();
		stopIndx = repList.indexOf("]",startIndx);
		messageKeyword = repList.substring(startIndx, stopIndx);
		messageKey=messageKeyword.substring(0, messageKeyword.indexOf("||"));
		messageKey=messageKey.trim();
		messageParaKeyword=messageKeyword.substring(messageKeyword.indexOf("||")+2).split(java.util.regex.Pattern.quote("||"));
		messageString = VelosResourceBundle.getMessageString(messageKey,messageParaKeyword);
		repList=StringUtil.replaceAll(repList, "VELPARAMESSGE["+messageKeyword+"]", messageString);
	}
	if(!repList.contains("word.GIF")){
		if(repList.contains(LC.L_Word_Format)){
			tobeReplaced = "<img border=\"0\" title=\""+LC.L_Word_Format+"\" alt=\""+LC.L_Word_Format+"\" src=\"./images/word.GIF\"></img>";
			repList=StringUtil.replace(repList, LC.L_Word_Format, tobeReplaced);
		}
		if(repList.contains(LC.L_Excel_Format)){
			tobeReplaced = "<img border=\"0\" title=\""+LC.L_Excel_Format+"\" alt=\""+LC.L_Excel_Format+"\" src=\"./images/excel.GIF\"></img>";
			repList=StringUtil.replace(repList,LC.L_Excel_Format, tobeReplaced);
		}
		if(repList.contains(LC.L_Printer_FriendlyFormat)){
			tobeReplaced = "<img border=\"0\" src=\"./images/printer.gif\" title=\""+LC.L_Printer_FriendlyFormat+"\" alt=\""+LC.L_Printer_FriendlyFormat+"\"></img>";
			repList=StringUtil.replace(repList, LC.L_Printer_FriendlyFormat, tobeReplaced);
		}
	}
	%>

		
	<Form name = "invoicePrint" method="post" id="invprintfrm" action="repRetrieveInvoice.jsp" onsubmit = "">
		<input type="hidden" name ="invPk" value ="<%=invPk %>" >	

				<table width="98%" cellspacing="0" cellpadding="0" border=0 >
			<tr height="85"> 
			<td width="50%" align="right"> <font="arial"><b><%=MC.M_Selc_InvPrintFormat%><%--Select Invoice Print Format*****--%>:</b></font>&nbsp;&nbsp;  </td>
			<td width="50%"> <%=repList%> </td>
			</tr>
			
		</table>
		
		<jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="N"/>
				<jsp:param name="formID" value="invprintfrm"/>
				<jsp:param name="showDiscard" value="N"/>
		</jsp:include>
		
	
	</form>
<%	
}//end of if body for session
else{
%>
  <jsp:include page="timeout.html" flush="true"/>
<%
}
%>

</DIV>	
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</html>
