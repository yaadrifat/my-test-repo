<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>

<BODY>
<%--
<DIV class="browserDefault" id = "div1"> 
--%>

<jsp:useBean id ="bgtapndx" scope="request" class="com.velos.esch.web.bgtApndx.BgtApndxJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java"  import="com.velos.esch.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
  <%

String	src = request.getParameter("srcmenu");
String eSign = request.getParameter("eSign");
String budgetId = request.getParameter("bgtId");
String budgetType = request.getParameter("budgetType");
String selectedTab = request.getParameter("selectedTab");
String budgetTemplate = request.getParameter("budgetTemplate");
String budgetMode = request.getParameter("budgetMode");
String fromRt = request.getParameter("fromRt");
if (fromRt==null || fromRt.equals("null")) fromRt="";

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

   { %>
<Input type="hidden" name="fromRt" value=<%=fromRt%> />
<jsp:include page="sessionlogging.jsp" flush="true"/>
<% 	
   	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
   	  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {
	//String tab = request.getParameter("selectedTab");
	String appndxId = request.getParameter("apndxId");
	String desc = request.getParameter("desc");
	// Added By Amarnadh for issue #3011
	String usr = null;
	usr = (String) tSession.getValue("userId");

	bgtapndx.setBgtApndxId(EJBUtil.stringToNum(appndxId));  
	bgtapndx.getBgtApndxDetails();
	bgtapndx.setBgtApndxDesc(desc); 
	bgtapndx.setModifiedBy(usr); // Amarnadh
	bgtapndx.updateBgtApndx();
	

%>
<br>
<br>
<br>
<br>
<br>
<p class = "sectionHeadings" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully.*****--%> </p>

<META HTTP-EQUIV=Refresh CONTENT="1; URL=budgetapndx.jsp?budgetTemplate=<%=budgetTemplate%>&mode=<%=budgetMode%>&srcmenu=<%=src%>&budgetId=<%=budgetId%>&selectedTab=<%=selectedTab%>&budgetType=<%=budgetType%>&fromRt=<%=fromRt%>">

<%
	}//end of if for eSign check		
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





