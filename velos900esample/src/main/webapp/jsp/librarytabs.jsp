<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.CtrlDao,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration"%>
<%@ page import="com.velos.eres.web.objectSettings.ObjectSettingsCache, com.velos.eres.web.objectSettings.ObjectSettings"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>

<%!
  private String getHrefBySubtype(String subtype) {
      if (subtype == null) { return "#"; }
      if ("1".equals(subtype)) {
          return "protocollist.jsp?mode=N&srcmenu=tdmenubaritem4&calledFrom=P&selectedTab=1";
      }
      if ("2".equals(subtype)) {
          return "eventlibrary.jsp?srcmenu=tdmenubaritem4&calledFrom=L&duration=&protocolId=&mode=M&calStatus=W&selectedTab=2";
      }
      if ("3".equals(subtype)) {
          return "formLibraryBrowser.jsp?srcmenu=tdmenubaritem4&selectedTab=3";
      }
      if ("4".equals(subtype)) {
          return "fieldLibraryBrowser.jsp?srcmenu=tdmenubaritem4&mode=initial&selectedTab=4";
      }
      return "#";
  }
%>

<%
  HttpSession tSession = request.getSession(true);
  if (!sessionmaint.isValidSession(tSession))
  {
      %>
      <jsp:include page="timeout.html" flush="true"/>
      <%
      return;
  }
  String mode="N";
  String selclass="";
  String tab = request.getParameter("selectedTab");
  
  String uName = (String) tSession.getValue("userName");
  String accId = (String) tSession.getValue("accountId");

  

  ObjectSettingsCache objCache = ObjectSettingsCache.getObjectSettingsCache();
  ArrayList tabList = objCache.getAccountObjects(EJBUtil.stringToNum(accId), "lib_tab");
  %>
<DIV>
	<!-- <table  class= "selectedTab" cellspacing="0" cellpadding="0" border="0">  -->
	<table cellspacing="0" cellpadding="0" border="0">
		<tr>
        <%
        // Figure out the access rights 
        String modRight = (String) tSession.getValue("modRight");
        int formLibSeq = 0 , patProfileSeq=0;
        acmod.getControlValues("module");
        ArrayList acmodfeature =  acmod.getCValue();
        ArrayList acmodftrSeq = acmod.getCSeq();
        char formLibAppRight = '0';
        char patProfileAppRight = '0';
        formLibSeq = acmodfeature.indexOf("MODFORMS");
		patProfileSeq = acmodfeature.indexOf("MODPATPROF");
        formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();
        patProfileSeq = ((Integer) acmodftrSeq.get(patProfileSeq)).intValue();
        formLibAppRight = modRight.charAt(formLibSeq - 1);
        patProfileAppRight = modRight.charAt(patProfileSeq - 1);


		int pageRight= 0;
		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

        for (int iX=0; iX<tabList.size(); iX++) {
            ObjectSettings settings = (ObjectSettings)tabList.get(iX);
    		// If this tab is configured as invisible in DB, skip it
    		if ("0".equals(settings.getObjVisible())) {
    		    continue;
    		}
    		// Figure out the access rights
            boolean showThisTab = false;
    		
			if ("1".equals(settings.getObjSubType())) {
				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
				if (pageRight > 0) {showThisTab = true; }
    		}
			else if ("2".equals(settings.getObjSubType())) {
				//KM-#4099	
				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("EVLIB"));
					if (pageRight > 0) {showThisTab = true; }
	   		}
			else if ("3".equals(settings.getObjSubType())) {
    		    pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
				if (((String.valueOf(formLibAppRight).compareTo("1") == 0) 
    		            || (String.valueOf(patProfileAppRight).compareTo("1") == 0)) &&(pageRight >=4))
    		    {
    		        showThisTab = true; 
    		    }
    		} else if ("4".equals(settings.getObjSubType())) {
    			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFLDLIB"));
				if (((String.valueOf(formLibAppRight).compareTo("1") == 0) 
    			        ||(String.valueOf(patProfileAppRight).compareTo("1") == 0)) &&(pageRight >=4))
    			{
    			    showThisTab = true;
    			}
    		} else {
    		    showThisTab = true;
    		}

            if (!showThisTab) { continue; } // no access right; skip this tab

    		// Figure out the selected tab position
    		if (tab == null) { 
                selclass = "unselectedTab";
    		} else if (tab.equals(settings.getObjSubType())) {
                selclass = "selectedTab";
            } else {
                selclass = "unselectedTab";
            }
            %> 
			<td  valign="TOP">
				<table class="<%=selclass%>"  cellspacing="0" cellpadding="0"  border="0">
					<tr>
					 <!-- td class="<%=selclass%>" rowspan="3" valign="top" >
							<img src="../images/leftgreytab.gif" height=20 border=0 alt="">
						</td>  --> 
						<td class="<%=selclass%>">
							<a href="<%=getHrefBySubtype(settings.getObjSubType())%>"><%=settings.getObjDispTxt()%></a>
						</td>
						<!--  <td class="<%=selclass%>" rowspan="3" valign="top" >
							<img src="../images/rightgreytab.gif"  height=20 border=0 alt="">
						</td>  -->
					</tr>
				</table>
			</td>
            <%
        } // End of tabList loop
        %>
		</tr>
   <!--  	<tr>
		     <td colspan=5 height=10></td>
	    </tr>  -->
	</table>
<table class="tabBorder" width="99%" height="5"><tr><td></td></tr></table>	
</DIV>

