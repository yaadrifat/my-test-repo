<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html> 
<head> 
<title><%=LC.L_Query_AndForm%><%--Query and Form*****--%></title> 
<%@ page language = "java" import="com.velos.eres.service.util.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil" %>
<%@page language = "java" import="com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.business.common.SaveFormDao,com.velos.eres.business.common.TeamDao,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB,java.util.*,com.velos.eres.service.util.*,java.util.*,com.velos.eres.business.common.CodeDao, com.velos.eres.business.common.FormLibDao" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="formLibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id="lnkformsB"scope="request" class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="stdRights" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
</head>

<jsp:include page="include.jsp" flush="true"/> 
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<script type="text/javascript">
function refreshQuery(formobj){
		formobj.submit();
}
</script>
<body style="overflow:visible"> 
<%
	String ignoreRights = "";
	String pp_consenting = "";//portal

	HttpSession tSession = request.getSession(true);
	
	if (sessionmaint.isValidSession(tSession)){

		String defUserGroup = (String) tSession.getAttribute("defUserGroup");
		String qryId = request.getParameter("qryId");
		String qryFld = StringUtil.encodeString(request.getParameter("fldName"));
		String perId =request.getParameter("pkey");
		String formId =request.getParameter("formId");
		String fillFormId = request.getParameter("fillFormId");
		String studyId = request.getParameter("studyId");
		String formverId =request.getParameter("formverId");
		String patprotId =request.getParameter("patprotId");
		String eventId =request.getParameter("eventId");
		String formCount =request.getParameter("formCount");
		

		ignoreRights = (String) tSession.getValue("pp_ignoreAllRights") ;
		pp_consenting = (String) tSession.getValue("pp_consenting") ;
 
	  	int pageRight=0,formRights=0;   
	  	SaveFormDao saveDao=new SaveFormDao();

	  	//GET STUDY TEAM RIGHTS 
	  	String userId = (String) tSession.getValue("userId");
	  	
	    TeamDao teamDao = new TeamDao();
	    teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userId));
	    ArrayList tId = teamDao.getTeamIds();
	    Hashtable htMoreParams = new Hashtable();
    
	    String roleCodePk = "";
	    
	    if (tId.size() == 0) 
		{ 
	    	pageRight=0 ;
	    }
		else 
		{
			ArrayList arRoles = new ArrayList();
			arRoles = teamDao.getTeamRoleIds();		
			
			if (arRoles != null && arRoles.size() >0 )	
			{
				roleCodePk = (String) arRoles.get(0);
				
				if (StringUtil.isEmpty(roleCodePk))
				{
					roleCodePk="";
				}
			}	
			else
			{
				roleCodePk ="";
			}
			
			htMoreParams.put("teamRolePK",roleCodePk);
			
    		stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
    		ArrayList teamRights ;
			teamRights  = new ArrayList();
			teamRights = teamDao.getTeamRights();
				 
			stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
			stdRights.loadStudyRights();
		 
    	 
	    	if ((stdRights.getFtrRights().size()) == 0)
			{
	    	 	pageRight= 0;
	    	 	formRights=0;
	    	}
			else
			{
	    		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMACC"));
	    		formRights = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
	    	}
	    }
	    /// entered for having rights for study linked forms 
		
	    if (StringUtil.isEmpty(ignoreRights))
		{
			ignoreRights = "false";
		}
		
		if (ignoreRights.equals("true"))
		{
			pageRight=7;
			formRights=7;   
		}
		String accId=(String)tSession.getAttribute("accountId");
	  	  	
		session.setAttribute("formQueryRight",""+pageRight);
   		formLibB.setFormLibId(EJBUtil.stringToNum(formId));
	  	formLibB.getFormLibDetails();
	  	String formStatus = formLibB.getFormLibStatus();
	  	
		lnkformsB.findByFormId(EJBUtil.stringToNum(formId));
	  	
		String entryChar = lnkformsB.getLFEntry();
		entryChar = (entryChar == null)?"":entryChar;
		
		String mode = "M";
		
		CodeDao cdao = new CodeDao();
	    cdao.getCodeValuesById(EJBUtil.stringToNum(formStatus));
	    ArrayList arrSybType = cdao.getCSubType();
	    String statSubType = arrSybType.get(0).toString();
		
		if(pageRight>= 4){			
			%>
			<table width="100%" height="100%" cellpadding="3px" cellspacing="3px">
	           	<div id="ddOpt" style="font-family:Arial,Helvetica,sans-serif;font-size:12">
	            <tr height="5%">
					<td>
						<form name="rev" style="margin:0px" action="addeditquery.jsp?studyId=<%=studyId%>&formId=<%=formId%>&filledFormId=<%=fillFormId%>&from=4" method="post" target="preview">
							<b> <%=LC.L_Query_History%><%--Query History*****--%> </b>&nbsp;&nbsp;<button name="goButton" onClick="return refreshQuery(document.rev);"><%=LC.L_Refresh%></button>
							&nbsp;&nbsp;<label style="color:red">&nbsp;<%=MC.M_ToSeeDboard_RefreshRes%><%--To see your changes in dashboard, please click the 'Display' button to refresh dashboard results*****--%> </label> 
						</form>
					</td>
				</tr>
	           	</div> 
				<tr height="40%"> 
					<td id="previewTD" valign="top" width="40%">				
						<iframe name="preview" src="formQueryHistory.jsp?studyId=<%=studyId%>&formQueryId=<%=qryId%>&fieldName=<%=qryFld%>&filledFormId=<%=fillFormId%>&from=4&formId=<%=formId%>" width="100%" height="100%" frameborder="1" scrolling="yes" allowautotransparency=true></iframe>
			 		</td> 				
				</tr>
				<tr height="5%">
					<td><b> <%=LC.L_Form%><%--Form*****--%> </b></td>
				</tr>
				<tr height="95%">
					<td id="reviewTD" valign="top" width="60%"> 
						<iframe name="review" src="patstudyformdetails.jsp?srcmenu=null&calledFromForm=dashboard&formPullDown=<%=formId%>*M*<%=formCount%>&selectedTab=null&formId=<%=formId%>&formLibVer=<%=formverId%>&mode=M&studyId=<%=studyId%>&pkey=<%=perId%>&formDispLocation=SP&filledFormId=<%=fillFormId%>&patProtId=<%=patprotId%>&calledFrom=S&statDesc=null&statid=null&entryChar=<%=entryChar%>&schevent=<%=eventId%>&fk_sch_events1=<%=eventId%>&fkcrf=null&formFillDt=ALL&responseId=&parentId=&fkStorageForSpecimen=&showPanel=true&submissionType=&formCategory=" width="100%" height="100%" frameborder="1" scrolling="yes" allowautotransparency="true"></iframe>			
			 		</td> 
				</tr>
			</table>
		<%} 
		else{
			%>
			  <jsp:include page="accessdenied.jsp" flush="true"/>
			<%
		} //end of else body for page right	
	}else{
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%}	%>
		
	
	  <div class = "myHomebottomPanel"> 
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</body> 
</html>