<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page import="com.velos.eres.service.util.LC"%>
<title><%=MC.M_Std_SecInEres%><%--<%=LC.Std_Study%> Section in eResearch*****--%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB" %><%@page import="com.velos.eres.service.util.MC"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="sectionB" scope="page" class="com.velos.eres.web.section.SectionJB" />
<jsp:useBean id="codelstB" scope="page" class="com.velos.eres.web.codelst.CodelstJB" />


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<% String src="";

src= request.getParameter("srcmenu");

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>
<SCRIPT Language="javascript">
 function  validate(mode){
   if((mode == 'N') && document.section.sectionNameDD.disabled == false) {
   } else {
   	formobj=document.section
        if (!(validate_col('Section Name',formobj.sectionName))) return false
   }
  }


 function openwin() {

      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=600,height=450")

;}

function fMakeTextDisabled(){
	   document.section.template.disabled = false;
	   document.section.sectionName.disabled = false;
	   document.section.sectionNameDD.disabled = true;

}

function fMakeDDDisabled(){
	   document.section.template.disabled = true;
	   document.section.sectionName.disabled = true;
	   document.section.sectionNameDD.disabled = false;

}

</SCRIPT>

<body>
<DIV class="formDefault" id="div1">
  <jsp:include page="studytabs.jsp" flush="true"/>
  <%

 HttpSession tSession = request.getSession(true);

 String mode="";

String tab="";

 int sectionId= 0;

 String acc = (String) tSession.getValue("accountId");

if(sessionmaint.isValidSession(tSession))
   {
	int accId = EJBUtil.stringToNum(acc);
	int pageRight = 0;
//	mode = request.getParameter("mode");
	mode = "M";

	tab = request.getParameter("selectedTab");
	sectionId = EJBUtil.stringToNum(request.getParameter("sectionId"));

	String study = (String) tSession.getValue("studyId");
      if(study == "" || study == null) {
%>
  <jsp:include page="studyDoesNotExist.jsp" flush="true"/>
  <%
	   } else {

	    	StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
		if ((stdRights.getFtrRights().size()) == 0){
		 	pageRight= 0;
		   }else{
			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSEC"));
	   	}
	 }

	if (pageRight > 0)
	   {
		String sectionName = "" ;
		String sectionContents = "" ;
		String sectionPublicFlag = "" ;
		if (mode.equals("M"))
		   {
			sectionB.setId(sectionId);
			sectionB.getSectionDetails();
			sectionName = sectionB.getSecName();
			sectionContents = sectionB.getContents();
			sectionPublicFlag = sectionB.getSecPubFlag();
		   }
%>
  <Form name="section" method="post" action="updatesection.jsp" >
    <input type="hidden" name="srcmenu" value="<%=src%>">
    <input type="hidden" name="selectedTab" value="<%=tab%>">
    <%

		if (mode.equals("M"))
		   {
%>
    <input type="hidden" name="sectionId" size = 20  value = '<%=sectionId%>' >
    <input type="hidden" name="mode" size = 20  value = '<%=mode%>' >
    <P class = "sectionHeadings"> <%=LC.L_Std_Sec%><%--<%=LC.Std_Study%> Section*****--%> </P>

    <table width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20%"> <P class = "defComments"> <%=LC.L_Sec_Name%><%--Section Name*****--%> </P> </td>
        <td width="80%">
          <%=sectionName%>
        </td>
      </tr>
	  <tr>
	  	<td colspan=2 height=10>
		</td>
	  </tr>
	  <tr>
        <td width="20%" valign=top> <P class = "defComments"> <%=LC.L_Sec_Contents%><%--Section Contents*****--%> </P> </td>
        <td width="80%">
          <%=sectionContents%>
        </td>
      </tr>
      <tr>
        <td width="500" colspan=2> <br><br><%=MC.M_DoYouWant_InfoToPublic%><%--Do you want Information in this section to
          be available to the public?*****--%> </td>
      <tr>
        <td colspan=2>
          <input type="Radio" name="sectionPubFlag" value='Y'

<%
		if((sectionPublicFlag!=null)&&(sectionPublicFlag.equals("Y")))
		   {
%>
checked
<%
		   }
%>
>
          <%=LC.L_Yes%><%--Yes*****--%>
          <input type="Radio" name="sectionPubFlag" value='N'

<%
		if((sectionPublicFlag!=null)&&(sectionPublicFlag.equals("N")))
		   {
%>
checked
<%
		   }
%>
>
          <%=LC.L_No%><%--No*****--%> &nbsp; <A href="pubvsnonpub.htm" target="Information" onClick="openwin()"><%=MC.M_WhatPub_NonPubInfo%><%--What
          is Public vs Non Public Information?*****--%></A> </td>
      </tr></tr>
    </table>
    <%
		} else {
		   CodeDao codeDao = codelstB.getCodelstData(accId, "section");
		   ArrayList codelstDescs = codeDao.getCDesc();

		   String codelstDesc = "";

		   int rows = codeDao.getCRows();
%>
    <input type="hidden" name="mode" size = 20  value = '<%=mode%>' >
    <P class = "sectionHeadings"> <%=LC.L_Std_Sec%><%--<%=LC.Std_Study%> Section*****--%> </P>
    <table width="500" cellspacing="0" cellpadding="0">
      <tr>
        <td width="300"> <%=LC.L_Sec_Name%><%--Section Name*****--%> <FONT class="Mandatory">* </FONT> </td>
      </tr>
    </table>
    <table width="500" cellspacing="0" cellpadding="0">
      <tr>
        <td width="300" align=center>
          <input type=radio name=dd_text value='dd' checked onClick=fMakeDDDisabled()>
        </td>
        <td>
          <select name='sectionNameDD' >
            <%
		for(int count=0; count<rows ; count++) {
		   codelstDesc = (String) codelstDescs.get(count);
%>
            <option value='<%=codelstDesc%>'><%=codelstDesc%></option>
            <%
		}
%>
          </select>
        </td>
      </tr>
      <tr>
        <td width="300" align=center>
          <input type=radio name=dd_text value='text'  onClick=fMakeTextDisabled()>
        </td>
        <td>
          <input type="text" name="sectionName" size=30 MAXLENGTH = 100 value="<%=sectionName%>" disabled>
          <input type=checkbox name=template disabled>
          <%=MC.M_Add_ToTemplateList%><%--Add to Template List*****--%> </td>
      </tr>
      <tr>
        <td width="300"> <%=LC.L_Sec_Contents%><%--Section Contents*****--%> </td>
        <td>
          <TextArea name="sectionContents" rows=10 cols=60 ><%=sectionContents%></TextArea>
        </td>
      </tr>
	  <tr>
        <td width="500" colspan=2>
          <p class = "defComments"><%=MC.M_SecLimit_20000Char%><%--Each section has a maximum limit of 20,000
            characters*****--%></P>
        </td>
      </tr>
      <tr>
        <td width="500" colspan=2> <%=MC.M_DoYouWant_InfoToPublic%><%--Do you want Information in this section to
          be available to the public?*****--%> </td>
      <tr>
        <td colspan=2>
          <input type="Radio" name="sectionPubFlag" value='Y'

<%
		if((sectionPublicFlag!=null)&&(sectionPublicFlag.equals("Y")))
		   {
%>
checked
<%
		   }
%>
>
          <%=LC.L_Yes%><%--Yes*****--%>
          <input type="Radio" name="sectionPubFlag" value='N' checked >
          <%=LC.L_No%><%--No*****--%> &nbsp; <A href="pubvsnonpub.htm" target="Information" onClick="openwin()"><%=MC.M_WhatPub_NonPubInfo%><%--What
          is Public vs Non Public Information?*****--%></A> </td>
      </tr></tr>
    </table>
    <%
		}
%>
    <br>
  </Form>
  <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<%
	} else {      //end of else body for page right
%>
<%
	}
} else {            //end of if body for session

%>
<jsp:include page="timeout.html" flush="true"/>
<%

}

%>
<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>

