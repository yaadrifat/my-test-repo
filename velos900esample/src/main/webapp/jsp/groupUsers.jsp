<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=MC.M_MngAcc_GrpUsr%><%-- Manage Account >> Group Users*****--%></title>



<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB" %>




<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>



<% String src;

src= request.getParameter("srcmenu");

%>



<jsp:include page="panel.jsp" flush="true"> 

<jsp:param name="src" value="<%=src%>"/>

</jsp:include>   



<SCRIPT>

function openwin() {

      window.open("","users","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400");}





</SCRIPT>  



<body>
<br>
<jsp:useBean id="groupB" scope="session" class="com.velos.eres.web.group.GroupJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import="com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.EJBUtil"%>
<DIV class="browserDefault" id = "div1"> 
  <%

HttpSession tSession = request.getSession(true);

String grpName = null;

String grpId = null;

if (sessionmaint.isValidSession(tSession))

{

	String uName = (String) tSession.getValue("userName");



	int pageRight = 0;



	String accId = (String) tSession.getValue("accountId");



	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		

	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("ASSIGNUSERS"));	


	grpId = request.getParameter("groupId");

	groupB.setGroupId(EJBUtil.stringToNum(grpId));

	groupB.getGroupDetails();

	grpName  = groupB.getGroupName();

   if (pageRight > 0 )

	 {



 %>

  <P class = "userName"> <%= uName %> </P>
  
  <Form name="groupUserBrowser" method="post" action="" onsubmit="">
    <table width="600" cellspacing="0" cellpadding="0" border=0 >
      <tr > 
	     <td width = "150"> 
          <P class = "sectionHeadings"> </P>
        </td>
        <td width = "450" align=right> 

		  <p> <A href="assignuserstogroupusrsearch.jsp?mode=N&grpId=<%=grpId%>&srcmenu=<%=src%>"><%=MC.M_AssignUsr_ThisGrp_Upper%><%-- ASSIGN USERS TO THIS GROUP*****--%></A> </p>
        </td>

      </tr>
    </table>
    <table width="600" border=0>
      <tr> 
        <th> <%=MC.M_Users_AssignedToGrp%><%-- Users assigned to group*****--%>: '<%=grpName%>' </th>
      </tr>
      <%





      UserDao userDao =  new UserDao(); 



      ArrayList usrLastNames;

      ArrayList usrFirstNames;

      ArrayList usrMidNames;



      ArrayList usrIds;	

	

      String usrLastName = null;



      String usrFirstName = null;

      String usrMidName = null;

      String usrId = null;	





      int counter = 0;

	

      userDao.getGroupUsers(Integer.parseInt(grpId));

		usrIds = userDao.getUsrIds();

		usrLastNames = userDao.getUsrLastNames();

		usrFirstNames = userDao.getUsrFirstNames();

		usrMidNames = userDao.getUsrMidNames();

	int i;

	int lenUsers = usrLastNames.size();	

	Object[] arguments1 = {lenUsers};

	%>
      <tr id ="browserBoldRow"> 
        <td width = 600><%=MC.M_Tot_NumOfUsers%><%-- Total Number of Users*****--%> : <%=VelosResourceBundle.getLabelString("L_Usr_S",arguments1)%><%--<%= lenUsers%> User(s)*****--%> </td>
      </tr>
      <%

		for(i = 0 ; i < lenUsers ; i++)

		  {



		usrLastName=((usrLastNames.get(i)) == null)?"-":(usrLastNames.get(i)).toString();

		usrFirstName=((usrFirstNames.get(i))==null)?"-":(usrFirstNames.get(i)).toString();

		usrMidName=((usrMidNames.get(i))==null)?"-":(usrMidNames.get(i)).toString();

	

		usrId = ((Integer)usrIds.get(i)).toString();



		if ((i%2)==0) {

  %>
      <tr class="browserEvenRow"> 
        <%

		}

		else{

  %>
      <tr class="browserOddRow"> 
        <%

		}

  %>
        <td width =600> <A href = "userdetails.jsp?mode=M&srcmenu=<%=src%>&userId=<%=usrId%>"> 
          <%= usrFirstName%>&nbsp;<%= usrLastName%></A> </td>
      </tr>
      <%

		}

%>
    </table>
  </Form>
  <%

	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div> 
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</div>
<div class ="mainMenu" id = "emenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>

</html>
