<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>

<BODY>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>


 <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
  <%

	HttpSession tSession = request.getSession(true);

    if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>

<%
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");
	String src = request.getParameter("srcmenu");

	String visitId = request.getParameter("eventVisitId");
	String selectedTab = request.getParameter("selectedTab");
	String mode = request.getParameter("mode");

	String eventId= request.getParameter("eventId");
	String shufflngEventId= request.getParameter("shufflngEventId");
	if (shufflngEventId==null) shufflngEventId="0";
	int shuffEventId= EJBUtil.stringToNum(shufflngEventId);


	String sequence = request.getParameter("sequence");
	String sufflngSeq = request.getParameter("sufflngSeq");
	String calledFrom = request.getParameter("calledFrom");
	String calStatus = request.getParameter("calStatus");
	String protId = request.getParameter("protId");
	String calAssoc = request.getParameter("calAssoc");


	calledFrom = calledFrom.trim();
	int setId = 0;

//System.out.println("#####################"+ protId + " " + eventId + " " +shufflngEventId+ " "+ sequence+ " " + sufflngSeq + " " +calledFrom+" "+ " " + calAssoc + " " +selectedTab);


	//shuffle the sequences
	if (shuffEventId > 0) {

    	if (calledFrom.equals("P")  || calledFrom.equals("L")){

			eventdefB.setEvent_id(Integer.parseInt(eventId));
			eventdefB.getEventdefDetails();
			eventdefB.setEventSequence(sufflngSeq);
			eventdefB.setModifiedBy(usr);
			eventdefB.setIpAdd(ipAdd);
			setId=eventdefB.updateEventdef();

			eventdefB.setEvent_id(Integer.parseInt(shufflngEventId));
			eventdefB.getEventdefDetails();
			eventdefB.setEventSequence(sequence);
			eventdefB.setModifiedBy(usr);
			eventdefB.setIpAdd(ipAdd);
			setId=eventdefB.updateEventdef();

    	}else if (calledFrom.equals("S")){

			eventassocB.setEvent_id(Integer.parseInt(eventId));
			eventassocB.getEventAssocDetails();
			eventassocB.setEventSequence(sufflngSeq);
			eventassocB.setModifiedBy(usr);
			eventassocB.setIpAdd(ipAdd);
			setId=eventassocB.updateEventAssoc();

			eventassocB.setEvent_id(Integer.parseInt(shufflngEventId));
			eventassocB.getEventAssocDetails();
			eventassocB.setEventSequence(sequence);
			eventassocB.setModifiedBy(usr);
			eventassocB.setIpAdd(ipAdd);
			setId=eventassocB.updateEventAssoc();

		}

	}
%>

<META HTTP-EQUIV=Refresh CONTENT="0; URL=fetchProt.jsp?srcmenu=<%=src%>&selectedTab=3&protocolId=<%=protId%>&calledFrom=<%=calledFrom%>&mode=<%=mode%>&calStatus=<%=calStatus%>&calassoc=<%=calAssoc%>&pageNo=1">
<%
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





