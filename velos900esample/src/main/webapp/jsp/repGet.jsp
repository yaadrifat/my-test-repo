<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>

<TITLE><%=LC.L_All_AccUsers%><%--All Account Users*****--%></TITLE>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>

<body>
<jsp:useBean id="repB" scope="request" class="com.velos.eres.web.report.ReportJBNew"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,javax.xml.transform.TransformerFactory,javax.xml.transform.Transformer,javax.xml.transform.Source,javax.xml.transform.stream.StreamSource,javax.xml.transform.stream.StreamResult,javax.xml.transform.TransformerException,javax.xml.transform.TransformerConfigurationException,java.io.*,java.net.URL"%>
<%
	String repBy="SPahwa";
	String repDate="";
	String repArgs="None";
	String repTitle="All Account Users";
	String repName="All Account Users";
	String params="";
	
	Calendar now = Calendar.getInstance();
	repDate=""+now.get(now.DAY_OF_MONTH) + now.get(now.MONTH) + (now.get(now.YEAR) - 1900);

	ReportDaoNew rD =new ReportDaoNew();
	rD=repB.getRepXml(4,66, "66" );
	ArrayList repXml = rD.getRepXml(); 
		
	rD=repB.getRepXsl(3);
	ArrayList repXsl = rD.getXsls();
		
	Object temp;
	String xml=null;
	String xsl=null;
		
	temp=repXml.get(0);			

	if (!(temp == null)) 
	{
		xml=temp.toString();
	}
	else
	{

		out.println(MC.M_Err_GettingXml/*"error in getting xml"*****/);
	}
		

	temp=repXsl.get(0);			
	if (!(temp == null))
	{
		xsl=temp.toString();
		}
	else
	{
		out.println(MC.M_Err_GettingXsl/*"error in getting xsl"****/);
	}		
	//out.println(xml);
	//get hdr and ftr
	int i = rD.getRepHdFtr(4,66,4);
	//out.println("i="+i);
	if (i > 0)
	{
		String hdrFilePath=rD.getHdrFilePath();
		String ftrFilePath=rD.getFtrFilePath();
		//get the folder name
		System.out.println(hdrFilePath);
		Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");
		String filePath = Configuration.UPLOADFOLDER;
		String fileDnPath = Configuration.DOWNLOADSERVLET;
		
		//make the file name
		String fileName="reporthtml"+"["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].html" ;
		//make the complete file name
		String htmlFile = filePath + "/"+fileName;
		response.setContentType("text/html");    
	
		 try
	    {	
		
			//first save the output in html file
			TransformerFactory tFactory1 = TransformerFactory.newInstance();
			Reader mR1=new StringReader(xml); 
			Reader sR1=new StringReader(xsl); 
			Source xmlSource1=new StreamSource(mR1);
			Source xslSource1 = new StreamSource(sR1);

	 		Transformer transformer1 = tFactory1.newTransformer(xslSource1);
			//Set the params
			transformer1.setParameter("hdrFileName", hdrFilePath);
			transformer1.setParameter("ftrFileName", ftrFilePath);
			transformer1.setParameter("repTitle",repTitle);
			transformer1.setParameter("repName",repName);
			transformer1.setParameter("repBy",repBy);
			transformer1.setParameter("repDate",repDate);
			transformer1.setParameter("argsStr",repArgs);
			transformer1.setParameter("cond","F");
			transformer1.setParameter("wd","");
			transformer1.setParameter("xd","");
			transformer1.setParameter("hd", "");
			

			// Perform the transformation, sending the output to html file
    	  	transformer1.transform(xmlSource1, new StreamResult(htmlFile));
	
			//now send it to console			
    	  	TransformerFactory tFactory = TransformerFactory.newInstance();
			Reader mR=new StringReader(xml); 
			Reader sR=new StringReader(xsl); 
			Source xmlSource=new StreamSource(mR);
			Source xslSource = new StreamSource(sR);
			// Generate the transformer.
   			Transformer transformer = tFactory.newTransformer(xslSource);

			//Set the param for header and footer
			transformer.setParameter("hdrFileName", hdrFilePath);
			transformer.setParameter("ftrFileName", ftrFilePath);
			transformer.setParameter("repTitle",repTitle);
			transformer.setParameter("repName",repName);
			transformer.setParameter("repBy",repBy);
			transformer.setParameter("repDate",repDate);
			transformer.setParameter("argsStr",repArgs);
			transformer.setParameter("cond","T");
			transformer.setParameter("wd","repGetWord.jsp?htmlFile=" + htmlFile +"&fileDnPath="+fileDnPath+"&filePath="+filePath);
			transformer.setParameter("xd","repGetExcel.jsp?htmlFile=" + htmlFile +"&fileDnPath="+fileDnPath+"&filePath="+filePath);
			transformer.setParameter("hd","repGetHtml.jsp?htmlFileName=" + fileName +"&fileDnPath="+fileDnPath+"&filePath="+filePath);
												  	  
			// Perform the transformation, sending the output to the response.
	      	transformer.transform(xmlSource, new StreamResult(out));
		
	    }
    	catch (Exception e)
    	{
      		out.write(e.getMessage());
     	}
	 	 
	 }
	else
	{
		out.println(MC.M_ErrGet_HdrFtr/*"error in getting header/footer"*****/);
	}

  %>
</body>
</html>