<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<HTML>
<HEAD>
<title><%=LC.L_Pcol_Cal%><%--Protocol calendars*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<% String src;

src= request.getParameter("srcmenu");

%>


<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page import="com.velos.eres.service.util.*"%>

<DIV  id="div1">

<%
String eventId="";
String msg="";
String userId="";

HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
 {
 %>
  <jsp:include page="sessionlogging.jsp" flush="true"/>

  <%
	String studyId = (String) tSession.getValue("studyId");
	userId = (String) tSession.getValue("userId");

	String ipAdd = (String) tSession.getValue("ipAdd");

	int ret = 0;

	String calAssoc = request.getParameter("calassoc");
	//JM: 13Dec2006
		//calAssoc=(calAssoc==null)?"P":calAssoc;


	eventId=request.getParameter("eventId");
%>

<%
	ret=eventassocB.AddProtToStudy(eventId,studyId,userId,ipAdd,calAssoc);
%>
<BR>
<BR>


<%
	if (ret== -1 ) {
		String name = request.getParameter("name");
		name = name.replace("[VELSP]", " "); Object[] arguments1 = {name};
		msg = VelosResourceBundle.getMessageString("M_PcolCalStd_CaseCopyNew",arguments1);/*" The Protocol Calendar  '" + name + "' that you have selected has already been associated with the current study. Please go back to the Library and select another calendar for association. In case you want to associate the same calendar again, create a Copy of the Existing Calendar under a new name."*****/
%>
  <p class = "blackComments" > <%=msg%> </p>
  <table width="100%" cellspacing="0" cellpadding="0">
	<tr>
      <td align=center>
		<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
      </td>
      </tr>
  </table>
<%
   }
	else {

		msg = MC.M_Data_SavedSucc/*"Data saved successfully"*****/;
%>
  	 <BR>
  	 <BR>
     <BR>
  	 <BR>
  	  <p class = "successfulmsg" align = center > <%=msg%> </p>
	  <script>
			window.opener.location.reload();
			setTimeout("self.close()",1000);
	  </script>


<%
	 }
}//end of if body for session

else

{
%>
  <jsp:include page="timeout.html" flush="true"/>

<%

}

%>

</div>


</BODY>

</HTML>
