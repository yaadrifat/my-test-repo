<%@taglib prefix="s" uri="/struts-tags" %>

<table>
<tr>
<td>
<table width="50%">
<tr>
<td align="right">
Label 1:
</td>
<td align="left">
<s:fielderror id="ctrpDraftJB.field1" >
<s:param>ctrpDraftJB.field1</s:param>
</s:fielderror>
<s:textfield name="ctrpDraftJB.field1" style="height:2em; width:20em" size="30" />
</td>
</tr>
<tr>
<td align="right">
Another Label - Label 2:
</td>
<td align="left">
<input style="height:2em; width:20em" type="text" id="freetext2" name="freetext" size="30"/>
</td>
</tr>
<tr>
<td align="right">
Label 3:
</td>
<td align="left">
<input style="height:2em; width:20em" type="text" id="freetext3" name="freetext" size="30"/>
</td>
</tr>
<tr>
<td align="right">
Another Label - Label 4:
</td>
<td align="left">
<input style="height:2em; width:20em" type="text" id="freetext4" name="freetext" size="30"/>
</td>
</tr>
</table>
</td>
<td>
<table width="50%">
<tr>
<td>
<table width="50%">
<tr>
<td align="right">
Label 5:
</td>
<td align="left">
<input style="height:2em; width:20em" type="text" id="freetext5" name="freetext" size="30"/>
</td>
</tr>
<tr>
<td align="right">
Another Label - Label 6:
</td>
<td align="left">
<input style="height:2em; width:20em" type="text" id="freetext6" name="freetext" size="30"/>
</td>
</tr>
<tr>
<td align="right">
Label 7:
</td>
<td align="left">
<input style="height:2em; width:20em" type="text" id="freetext7" name="freetext" size="30"/>
</td>
</tr>
<tr>
<td align="right">
Another Label - Label Eight:
</td>
<td align="left">
<input style="height:2em; width:20em" type="text" id="freetext8" name="freetext" size="30"/>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>