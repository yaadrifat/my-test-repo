<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<%-- Nicholas : Start --%>
	<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
	<%-- Nicholas : End --%>
<SCRIPT LANGUAGE="JavaScript" src="calendar.js"></SCRIPT>
 
<SCRIPT  Language="JavaScript1.2">
function fclose_to_role()
{
	window.opener.location.reload();
	//05Apr2011 @Ankit #4814
	//self.close();
}

function toggleSelect(formobj,counter,mileStoneId,detCount,delAllCount)
{
	 
 
  var chkVal;
  var newVal;
  var c;
  
	//FIN-20609 Ankit Kumar
  	if(document.getElementById('mileDetailCheck'+delAllCount).checked)
	{
 		document.getElementById('mileDetailAmountInvoiced'+delAllCount).readOnly = false;     //22Nov2011 Akshi: Fixed for Bug #7798
	}
	else
	{
		document.getElementById('mileDetailAmountInvoiced'+delAllCount).readOnly = true;     //22Nov2011 Akshi: Fixed for Bug #7798
	}

  
  c = parseInt(counter);
  
    
  if (parseInt(detCount) == 1)
  {
  	chkval = eval("formobj.mileDetailCheckHidden" + mileStoneId + ".value"  );
  }
  else
  {
  	chkval = parseInt(eval("formobj.mileDetailCheckHidden" + mileStoneId  + "[" + c + "].value"  ));
  }  

  
  
  if (chkval == 1)
  {
  	newVal= 0;
  }
  else
  {
  	newVal= 1;
  } 
  
  if (parseInt(detCount) == 1)
  {
  	eval("formobj.mileDetailCheckHidden" + mileStoneId + ".value = " + newVal  );
  	
  }
  else
  {
  	eval("formobj.mileDetailCheckHidden" + mileStoneId  + "[" + c + "].value = " + newVal  );
  }
  

}

function f_editboxes(formobj,counter,mileStoneId,countPerMilestone)
{
	//enable/disable editboxes only if its an 'every patient' milestone

	var dispDetail;
	var c;
	var detailCount;
	var totalmilecount;
	var isSelected;
	var mileAmountForGroupMilestones ;

	totalmilecount = parseInt(formobj.totalmilecount.value);


	//if (parseInt(countPerMilestone) <= 1)	{


	if (totalmilecount == 1)
		{
			dispDetail = formobj.displayDetails.value;
			detailCount = formobj.mileDetailCount.value;
			isSelected = formobj.selectMilestone.checked;
			origmileAmount = formobj.origmileAmount.value;
			mileAmountForGroupMilestones  = formobj.mileAmountForGroupMilestones.value;

		}
		else
		{
			dispDetail = formobj.displayDetails[counter].value;
			detailCount = formobj.mileDetailCount[counter].value;
			isSelected = formobj.selectMilestone[counter].checked;

			 origmileAmount = formobj.origmileAmount[counter].value;
			 mileAmountForGroupMilestones  = formobj.mileAmountForGroupMilestones[counter].value;
		}

		 if (parseInt(countPerMilestone) > 1)
		 {
				 origmileAmount = mileAmountForGroupMilestones;
		 }


		if (! isSelected)
		{
			alert("<%=MC.M_Selc_MstoneFirst%>");/*alert("Please select the Milestone first");*****/
			
          if(dispDetail == 1){
             
				if (totalmilecount == 1)
				{
					formobj.displayDetails.value = "0";
				}
				else
				{
					formobj.displayDetails[counter].value = "0";
				}
			
         }else {    //Ashu fixed BUG#5754 2Feb,2011
	        	 if (totalmilecount == 1)
	 			{
	 				formobj.displayDetails.value = "1";
	 			}
	 			else
	 			{
	 				formobj.displayDetails[counter].value = "1";
	 			}	             
             
        }


			return false;

		}

		if (totalmilecount == 1)
		{
			formobj.mileAmountInvoiced.value = formobj.origmileAmountInvoiced.value
			formobj.mileAmountInvoiced.style.color='black';           //Added to fix Bug#7603 : Raviesh
		}
		else
		{
			formobj.mileAmountInvoiced[counter].value = formobj.origmileAmountInvoiced[counter].value
			formobj.mileAmountInvoiced[counter].style.color='black';    //Added to fix Bug#7603 : Raviesh
		}




		if (dispDetail == 1) // show detail, make the detail amount editable
		{

			if (totalmilecount == 1)
			{
				formobj.mileAmountInvoiced.readOnly = true;
				formobj.mileAmountInvoiced.style.color='black';         //Added to fix Bug#7603 : Raviesh
			}
			else if (totalmilecount > 1)
			{
				  formobj.mileAmountInvoiced[counter].readOnly = true;
				  formobj.mileAmountInvoiced[counter].style.color='black';          //Added to fix Bug#7603 : Raviesh
			}

			if (detailCount == 1)
			{
				eval("formobj.mileDetailAmountInvoiced" + mileStoneId + ".readOnly = false");
				eval("formobj.mileDetailAmountInvoiced" + mileStoneId + ".value = formobj.mileDetailAmountInvoicedPrev" + mileStoneId + ".value" );				
				eval("formobj.mileDetailAmountInvoiced" + mileStoneId +".style.color = 'black' ");             //Added to fix Bug#7603 : Raviesh
				eval("formobj.mileDetailCheck" + mileStoneId + ".disabled = false"  );
				eval("formobj.mileDetailCheck" + mileStoneId + ".checked = true"  );
				
				eval("formobj.mileDetailCheckHidden" + mileStoneId + ".value = 1"  );
				
				 
			}
			else
			{

				 for (c=0;c<detailCount;c++)
				 {

					eval("formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].readOnly = false");
					
					eval("formobj.mileDetailCheck" + mileStoneId + "[" + c + "].disabled = false"  );
					eval("formobj.mileDetailCheck" + mileStoneId + "[" + c + "].checked = true"  );
					eval("formobj.mileDetailCheckHidden" + mileStoneId  + "[" + c + "].value = 1"  );
					
					eval("formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].value = formobj.mileDetailAmountInvoicedPrev" + mileStoneId + "[" + c + "].value" );
					eval("formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].style.color = 'black' ");             //Added to fix Bug#7603 : Raviesh
				}

			}
		}
		else // if displaydetail = 0
		{



		  if (totalmilecount == 1)
			{
				formobj.mileAmountInvoiced.readOnly = false;
				formobj.mileAmountInvoiced.style.color='black';              //Added to fix Bug#7603 : Raviesh
				origmileAmount = formobj.origmileAmount.value;
				mileAmountForGroupMilestones  = formobj.mileAmountForGroupMilestones.value;

			}
			else if (totalmilecount > 1)
			{
				  formobj.mileAmountInvoiced[counter].readOnly = false;
				  formobj.mileAmountInvoiced[counter].style.color='black';             //Added to fix Bug#7603 : Raviesh
				  origmileAmount = formobj.origmileAmount[counter].value;
				  mileAmountForGroupMilestones  = formobj.mileAmountForGroupMilestones[counter].value;

			}


			 if (parseInt(countPerMilestone) > 1)
			 {
				 origmileAmount = mileAmountForGroupMilestones;
			 }


			if (detailCount == 1)
			{
				eval("formobj.mileDetailAmountInvoiced" + mileStoneId + ".readOnly = true");

                	eval("formobj.mileDetailCheck" + mileStoneId + ".disabled = true"  );
                	eval("formobj.mileDetailCheck" + mileStoneId + ".checked = false"  );
                	eval("formobj.mileDetailCheckHidden" + mileStoneId  + ".value = 0"  );
                	
				eval("formobj.mileDetailAmountInvoiced" + mileStoneId + ".value = formobj.mileDetailAmountInvoicedPrev" + mileStoneId + ".value" );
				eval("formobj.mileDetailAmountInvoiced" + mileStoneId +".style.color = 'black' ");                   //Added to fix Bug#7603 : Raviesh
			}
			else
			{

				 for (c=0;c<detailCount;c++)
				 {
                    eval("formobj.mileDetailCheck" + mileStoneId + "[" + c + "].disabled = true"  );
                    eval("formobj.mileDetailCheck" + mileStoneId + "[" + c + "].checked = false"  );
                    eval("formobj.mileDetailCheckHidden" + mileStoneId  + "[" + c + "].value = 0"  );
					eval("formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].value = formobj.mileDetailAmountInvoicedPrev" + mileStoneId + "[" + c + "].value" );
					eval("formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].readOnly = true");
					eval("formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].style.color = 'black' ");               //Added to fix Bug#7603 : Raviesh
				}

			}

		}

	/*}
	else
	{


	}*/

}

function reclaculate(formobj)
{
	//reclaculate the final amount for all the milestone

	var milestones;
	var i;
	var  isSelected;
	var dispDetail ;
	var countPerMilestone ;
	var detailCount;
	var mileStoneId ;
	var amountTotal;
	var detailRecSel;

		if (formobj.totalmilecount.value == 0)
  		{
  			return false;
  		}

	if (!validateNumbers(formobj))
	{
		return false;
	}



	milestones = parseInt(formobj.totalmilecount.value);



	//iterate through each milestone

	if (milestones > 1)
	{
			for (i=0;i<milestones;i++)
			{
					isSelected = formobj.selectMilestone[i].checked;
					if (isSelected ) //milestone is selected
					{
						// check if detail view is selected



						// you can change the detailed amount only if countPerMilestone <= 1

						//if (countPerMilestone <= 1)	{
							dispDetail = formobj.displayDetails[i].value;

							detailCount = formobj.mileDetailCount[i].value;
							mileStoneId = formobj.mileStoneId[i].value;
							countPerMilestone = parseInt(formobj.countPerMilestone[i].value);

							if (dispDetail == 1) // the details can be changed so recalculate
							{
								////////////////
							 if (detailCount == 1)
								{
									eval("formobj.mileAmountInvoiced["+i+"].value = formobj.mileDetailAmountInvoiced" + mileStoneId + ".value");
									eval("formobj.mileAmountInvoiced["+i+"].style.color='black' ");             //Added to fixr Bug#7603 : Raviesh
								}
								else if (detailCount > 1)
								{
									 amountTotal = 0;

									 for (c=0;c<detailCount;c++)
									 {
									     detailRecSel = eval("formobj.mileDetailCheckHidden" + mileStoneId + "[" + c + "].value");
									     
									     if (parseInt(detailRecSel) == 1)
									     {
										  eval("amountTotal = amountTotal + parseFloat(formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].value)");
										 } 

									}
									amountTotal = amountTotal.toFixed(2);
									eval("formobj.mileAmountInvoiced["+i+"].value =" + amountTotal );
									eval("formobj.mileAmountInvoiced["+i+"].style.color='black' ");              //Added to fix Bug#7603 : Raviesh
								}

								////////////////
							} // end of if detail==1
							else //reclaculate the detail amount when high level is changed
							{
								amountTotal = 0;

								amountTotal = formobj.mileAmountInvoiced[i].value;

								if (detailCount  > 0)
								{
									amountTotal = amountTotal /detailCount ;
								}
								amountTotal = parseFloat(amountTotal );
								amountTotal = amountTotal.toFixed(2);

								///////////

								 if (detailCount == 1)
								{
									eval("formobj.mileDetailAmountInvoiced" + mileStoneId + ".value = " + amountTotal);
									eval("formobj.mileAmountInvoiced" + mileStoneId +".style.color='black' ");             //Added to fix Bug#7603 : Raviesh
								}
								else if (detailCount > 1)
								{
									 for (c=0;c<detailCount;c++)
									 {
										eval("formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].value = " + amountTotal);
										eval("formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].style.color='black' ");            //Added to fix Bug#7603 : Raviesh
									}

								}


							////////////
						} // end of elsefor dispDetail==1




					} // end of isselected

			} // end of for loop

	} // endof if for milestone count
	else //one milestone
	{
			isSelected = formobj.selectMilestone.checked;

			if (isSelected ) //milestone is selected
			{


				// you can change the detailed amount only if countPerMilestone <= 1

				//if (countPerMilestone <= 1)	{

					dispDetail = formobj.displayDetails.value;
					detailCount = formobj.mileDetailCount.value;
					mileStoneId = formobj.mileStoneId.value;
					countPerMilestone = parseInt(formobj.countPerMilestone.value);

					if (dispDetail == 1) // the details can be changed so recalculate
						{

								////////////////
							 if (detailCount == 1)
								{
									eval("formobj.mileAmountInvoiced.value = formobj.mileDetailAmountInvoiced" + mileStoneId + ".value");
									eval("formobj.mileAmountInvoiced.style.color='black' "); // Dec-07-2011 Bug#7902 Ankit
								}
								else if (detailCount >  1)
								{
									 amountTotal = 0;

									 for (c=0;c<detailCount;c++)
									 {
									    detailRecSel = eval("formobj.mileDetailCheckHidden" + mileStoneId + "[" + c + "].value");
									     
									     if (parseInt(detailRecSel) == 1)
									     {
										   eval("amountTotal = amountTotal + parseFloat(formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].value)");
										  } 

									}

									amountTotal = amountTotal.toFixed(2);
									eval("formobj.mileAmountInvoiced.value =" + amountTotal );
									eval("formobj.mileAmountInvoiced.style.color='black' "); 

								}

								////////////////
					} // end of if detail==1
					else{ // relaculate the detail rows
								amountTotal = 0;
								amountTotal = formobj.mileAmountInvoiced.value;

								if (detailCount > 0)
								{
									amountTotal = amountTotal /detailCount ;
								}

								amountTotal = parseFloat(amountTotal );
								amountTotal = amountTotal.toFixed(2);

								if (detailCount == 1)
								{
									eval("formobj.mileDetailAmountInvoiced" + mileStoneId + ".value = " + amountTotal );
									eval("formobj.mileDetailAmountInvoiced" + mileStoneId + ".style.color='black' ");             //Added to fix Bug#7603 : Raviesh
								}
								else if (detailCount >  1)
								{

									 for (c=0;c<detailCount;c++)
									 {
										eval("formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].value = " + amountTotal );
										eval("formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].style.color='black' ");           //Added to fix Bug#7603 : Raviesh

									}
								}


					}



				//} // end of countPerMilestone

			}	// end of isselected

	}	// end of else for one milestone

}

	function setSelected(formobj,counter)
	{

		var milestones;
		var selcount ;

		selcount = parseInt(formobj.selectedCount.value);

		milestones = parseInt(formobj.totalmilecount.value);

		if (milestones == 1)
		{
			if (formobj.selectMilestone.checked)
			{
				formobj.isChecked.value="1";
				selcount = selcount + 1;
			}
			else
			{
				formobj.isChecked.value="0";
				selcount = selcount -1;
			}
		}
		else
		{
			if (formobj.selectMilestone[counter].checked)
			{
				formobj.isChecked[counter].value="1";
				selcount = selcount + 1;
			}
			else
			{
				formobj.isChecked[counter].value="0";
				selcount = selcount - 1;
			}

		}


		formobj.selectedCount.value = selcount;

	}


  function validate(formobj)
  {
  		if (formobj.totalmilecount.value == 0)
  		{
  			alert("<%=MC.M_NoMstone_InvCntCmpt%>");/*alert("There are no Milestones available, the invoice cannot be completed.");*****/
  			return false;
  		}

	    if (! validateNumbers(formobj))
	    {
	    	return false;
	    }

  	  	

		//FIN-20609 Ankit Kumar
		
		var detailCount = parseInt(formobj.detailCount.value);
		if (detailCount == 1)
		{
			var detailAmountInv = parseFloat(document.getElementById('mileDetailAmountInvoiced0').value);
			var detailAmountInvPrev = parseFloat(document.getElementById('mileDetailAmountInvoicedPrev0').value);
			if(detailAmountInv < 0)
			{
				alert("<%=MC.M_InvAmtNt_NegVal%>");
				document.getElementById('mileDetailAmountInvoiced0').focus();
				return false;
			}
			if (detailAmountInvPrev < detailAmountInv){
				alert("<%=MC.M_InvAmtGt_MstoneAmt%>");
				document.getElementById('mileDetailAmountInvoiced0').focus();
				return false;
			}
		}
		else
		{
			for (i=0;i<detailCount;i++)
			{
				var detailAmountInv = parseFloat(document.getElementById('mileDetailAmountInvoiced'+i).value);
				var detailAmountInvPrev = parseFloat(document.getElementById('mileDetailAmountInvoicedPrev'+i).value);
				var mileDetailCheck = document.getElementById('mileDetailCheck'+i).checked;
				if(mileDetailCheck && detailAmountInv < 0)
				{
					alert("<%=MC.M_InvAmtNt_NegVal%>");
					document.getElementById('mileDetailAmountInvoiced'+i).focus();
					return false;
				}
				if (mileDetailCheck && detailAmountInvPrev < detailAmountInv){
					alert("<%=MC.M_InvAmtGt_MstoneAmt%>");
					document.getElementById('mileDetailAmountInvoiced'+i).focus();
					return false;
				}
			}
		}
		
		var milestones = parseInt(formobj.totalmilecount.value);
		if (milestones == 1)
		{
			var origmileAmount = parseFloat(formobj.origmileAmount.value);
			var mileAchCount = parseFloat(formobj.mileAchCount.value);
			var milePrevInvAmount = parseFloat(formobj.milePrevInvoicedAmount.value);
			var mileAmountInv = parseFloat(formobj.mileAmountInvoiced.value);
			var milestoneType = formobj.detailMilestoneType.value;
			if(mileAmountInv < 0)
			{
				alert("<%=MC.M_InvAmtNt_NegVal%>");
				formobj.mileAmountInvoiced.focus();
				return false;
			}
			if ((milestoneType != 'AM') && ((origmileAmount * mileAchCount) < (milePrevInvAmount+mileAmountInv))){
				alert("<%=MC.M_InvAmtGt_MstoneAmt%>");
				formobj.mileAmountInvoiced.focus();
				return false;
			}
			else if ((milestoneType == 'AM') && ((origmileAmount) < (milePrevInvAmount + mileAmountInv))){
				alert("<%=MC.M_InvAmtGt_MstoneAmt%>");
				formobj.mileAmountInvoiced.focus();
				return false;
			}
		}
		else
		{
			for (i=0;i<milestones;i++)
			{
				var origmileAmount = parseFloat(formobj.origmileAmount[i].value);
				var mileAchCount = parseFloat(formobj.mileAchCount[i].value);
				var milePrevInvAmount = parseFloat(formobj.milePrevInvoicedAmount[i].value);
				var mileAmountInv = parseFloat(formobj.mileAmountInvoiced[i].value);
				var milestoneType = formobj.detailMilestoneType[i].value;
				if(mileAmountInv < 0)
				{
					alert("<%=MC.M_InvAmtNt_NegVal%>");
					formobj.mileAmountInvoiced[i].focus();
					return false;
				}
				if ((milestoneType != 'AM') && ((origmileAmount * mileAchCount) < (milePrevInvAmount+mileAmountInv))){
					alert("<%=MC.M_InvAmtGt_MstoneAmt%>");
					formobj.mileAmountInvoiced[i].focus();
					return false;
				}
				if ((milestoneType == 'AM') && ((origmileAmount) < (milePrevInvAmount + mileAmountInv))){
					alert("<%=MC.M_InvAmtGt_MstoneAmt%>");
					formobj.mileAmountInvoiced[i].focus();
					return false;
				}
			}
		}
		reclaculate(formobj);              //22Nov2011 Akshi: Fixed for Bug #7798
		
		//End of FIN-20609

  	  	var selcount;

  	  	selcount = parseInt(formobj.selectedCount.value );

  	  	if (selcount <= 0)
  	  	{
  	  		alert("<%=MC.M_SelcAtleast_OneMstone%>");/*alert("Please select atleast one Milestone");*****/
  	  		return false;
  	  	}

	  	return true;
  }

	//FIN-20609 Ankit Kumar Desc:-Validation on onblur of Invoice amount.
	function checkAmt(origmileAmount, mileAchCount, milePrevInvAmount, mileAmountInvObj, milestoneType) {
		var mileAmountInv = parseFloat(mileAmountInvObj.value);
		if(mileAmountInv < 0)
		{
			mileAmountInvObj.style.color = 'red';
			 return;
		}
		if ((milestoneType != 'AM') && ((origmileAmount * mileAchCount) < (milePrevInvAmount + mileAmountInv))){
				mileAmountInvObj.style.color = 'red';
		}
		else if ((milestoneType == 'AM') && ((origmileAmount) < (milePrevInvAmount + mileAmountInv))){
			mileAmountInvObj.style.color = 'red';
		}
		else{
			mileAmountInvObj.style.color = 'black';
		}
	}

	//Desc:-Validation on onblur of Invoice Detail Amount.
	function checkDetailAmount(origmileDetailAmount, mileDetailAmtObj) {
		var mileDetailAmt = parseFloat(mileDetailAmtObj.value);
		if(mileDetailAmt < 0)
		{
			mileDetailAmtObj.style.color = 'red';
			 return;
		}
		if (origmileDetailAmount < mileDetailAmt){
			mileDetailAmtObj.style.color = 'red';
		}
		else{
			mileDetailAmtObj.style.color = 'black';
		}
	}
	//End of FIN-20609
	
  function validateNumbers(formobj)
{
	//reclaculate the final amount for all the milestone

	var milestones;
	var i;
	var  isSelected;
	var dispDetail ;
	var countPerMilestone ;
	var detailCount;
	var mileStoneId ;
	var isNumber ;
	var isDec ;


	milestones = parseInt(formobj.totalmilecount.value);

	//iterate through each milestone

	if (milestones > 1)
	{
			for (i=0;i<milestones;i++)
			{
					isSelected = formobj.selectMilestone[i].checked;
					if (isSelected ) //milestone is selected
					{
						// check if detail view is selected

						//countPerMilestone = parseInt(formobj.countPerMilestone[i].value);

						// you can change the detailed amount only if countPerMilestone <= 1

						//if (countPerMilestone <= 1)	{
							dispDetail = formobj.displayDetails[i].value;


							if (dispDetail == 1) // the details can be changed so recalculate
							{
								detailCount = formobj.mileDetailCount[i].value;
								mileStoneId = formobj.mileStoneId[i].value;
								////////////////
							 if (detailCount == 1)
								{
									eval("isDec = isDecimalAndNotBlank(formobj.mileDetailAmountInvoiced" + mileStoneId + ".value)");

									if (!isDec)
									{
										//check if number
										eval("isNumber = isIntegerAndNotBlank(formobj.mileDetailAmountInvoiced" + mileStoneId + ".value)");
										eval("isNeg = isNegNum(formobj.mileDetailAmountInvoiced" + mileStoneId + ".value)");

										if (!isNumber && !isNeg)
										{
											alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
										eval("formobj.mileDetailAmountInvoiced" + mileStoneId + ".focus()");
										return false;
										}
									}
								}
								else
								{
									 amountTotal = 0;

									 for (c=0;c<detailCount;c++)
									 {

									    eval("isDec = isDecimalAndNotBlank(formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].value )");
										if (!isDec)
										{
										  eval("isNumber = isIntegerAndNotBlank(formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].value )");
										  eval("isNeg = isNegNum(formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].value )");
										  
										  if (!isNumber && !isNeg)
											{
											  	alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
												eval("formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].focus()");
												return false;
											}
										}
									}

								}

								////////////////
							} // end of if detail==1
							else // validate header
							{

									isDec = isDecimalAndNotBlank(formobj.mileAmountInvoiced[i].value);
										
									if (!isDec)
									{
										isNumber = isIntegerAndNotBlank(formobj.mileAmountInvoiced[i].value);
										isNeg= isNegNum(formobj.mileAmountInvoiced[i].value);

										if (!isNumber)
										{
											alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
											formobj.mileAmountInvoiced[i].focus();
											return false;
										}
								   }

							}

						//} // end of countPerMilestone



					} // end of isselected

			} // end of for loop

	} // endof if for milestone count
	else //one milestone
	{
			isSelected = formobj.selectMilestone.checked;

			if (isSelected ) //milestone is selected
			{
				//countPerMilestone = parseInt(formobj.countPerMilestone.value);

				// you can change the detailed amount only if countPerMilestone <= 1


					dispDetail = formobj.displayDetails.value;


					if (dispDetail == 1) // the details can be changed so recalculate
						{
								detailCount = formobj.mileDetailCount.value;
								mileStoneId = formobj.mileStoneId.value;
								////////////////
							 if (detailCount == 1)
								{

								   eval("isDec = isDecimalAndNotBlank(formobj.mileDetailAmountInvoiced" + mileStoneId + ".value)");

								   if (!isDec)
									{
										eval("isNumber = isIntegerAndNotBlank(formobj.mileDetailAmountInvoiced" + mileStoneId + ".value)");
										eval("isNeg = isNegNum(formobj.mileDetailAmountInvoiced" + mileStoneId + ".value)");

										if (!isNumber && !isNeg)
										{
											alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
											eval("formobj.mileDetailAmountInvoiced" + mileStoneId + ".focus()");
											return false;
										}
									}
								}
								else if (detailCount > 1)
								{

								 for (c=0;c<detailCount;c++)
									{
										eval("isDec = isDecimalAndNotBlank(formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].value )");

										if (!isDec)
										{
										    eval("isNumber = isIntegerAndNotBlank(formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].value )");
										    eval("isNeg = isNegNum(formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].value )");

										    if (!isNumber && !isNeg)
											{
										    		alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
													eval("formobj.mileDetailAmountInvoiced" + mileStoneId + "[" + c + "].focus()");
													return false;
											}
										}
								  }
								}

								////////////////
					} // end of if detail==1
					else // validate header
							{

								isDec= isDecimalAndNotBlank(formobj.mileAmountInvoiced.value);

								if (!isDec)
								{
								isNumber = isIntegerAndNotBlank(formobj.mileAmountInvoiced.value);
								isNeg = isNegNum(formobj.mileAmountInvoiced.value);

									if (!isNumber && !isNeg)
									{
										alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
										formobj.mileAmountInvoiced.focus();
										return false;
									}

								}
							}

			}	// end of isselected

	}	// end of else for one milestone
	return true;
}
 //Added Ashu for enhancement:Req_spec_INV1_REQ 
 
 //Added this method to support 'Check All' functionality
 
 function checkAll(formobj,counter,totalmilesPerSection,isSectionLevel,checkAllCount){
	  var loopCount=0;
	  var totCount=0;
	  var selcount=0;
	  var chkAllLen=0;
	  var milestones = parseInt(formobj.totalmilecount.value);
	  
	  if(isSectionLevel){
		  loopCount=parseInt(counter);
		  totCount=parseInt(totalmilesPerSection)+parseInt(counter);
	  }
	  else{
		  totCount = parseInt(formobj.totalmilecount.value);
	  }
	  chkAllLen= parseInt(formobj.chkall.length);	  
	  selcount = parseInt(formobj.selectedCount.value);
	  
	 //checked all corresponding checkboxes of section or page when 'Check All' checkbox is selected. 	  
	 if (formobj.chkall[checkAllCount]!=null && formobj.chkall[checkAllCount].checked){
		 	
    	 if(milestones==1){
        		if(!formobj.selectMilestone.checked){
					formobj.selectMilestone.checked=true;
					formobj.isChecked.value="1";
					selcount=selcount+1;
	        	}
        }else {
	         for (i=loopCount;i<totCount;i++){
			     
			        if(!formobj.selectMilestone[i].checked){
						formobj.selectMilestone[i].checked=true;
						formobj.isChecked[i].value="1";
						selcount=selcount+1;
		        	}
		        	
		     }
		         //Added code to set the Check All checkbox value true when page level 'check All' is selected.
		         if(!isSectionLevel){
		        	 for (k=1;k<chkAllLen;k++){
		        		 formobj.chkall[k].checked=true;
		 	        }
	
		   } 
        }	   
     }else{    //When mark 'Check all' box unchecked set all corresponding check boxed unchecked.
    	 if(milestones==1){
    		 if(formobj.selectMilestone.checked){
					formobj.selectMilestone.checked=false;
					formobj.isChecked.value="0";
					selcount=selcount-1;
         	}

    	 }else{
    	    for (j=loopCount;j<totCount;j++){
		        	if(formobj.selectMilestone[j].checked){
						formobj.selectMilestone[j].checked=false;
						formobj.isChecked[j].value="0";
						selcount=selcount-1;
	            	}
            }
    	 }
    	 
         //Added code to set the Check All checkbox value false when page level 'check All' is unselected.
	         if(!isSectionLevel){
	        	 for (k=1;k<chkAllLen;k++){
	        		 formobj.chkall[k].checked=false;
	        	 }
	         }    
     }

	 formobj.selectedCount.value = selcount;     
 }

 //Added Ashu for enhancement:Req_spec_INV1_REQ 
 
 //Added this method to support 'Set All To' functionality for page level
 
  function setAll(formobj,counter,totalmile,setAllCount,mileTypeHdr){
	 
	 var isSelectedMileStone=false;

	 var loopCount=parseInt(counter);
	 var totCount=parseInt(totalmile)+parseInt(counter);
	
	 var defaultDisplayDetValue=formobj.defaultDisplayDetails[setAllCount].value;
	 var milestones = parseInt(formobj.totalmilecount.value);
	 var selecCount= parseInt(formobj.selectedCount.value);
		  
     if(milestones==1){
    	 if(formobj.selectMilestone.checked){
			 var counteperMilestone=parseInt(formobj.countPerMilestone.value);
			 var mileStoneId=parseInt(formobj.mileStoneId.value);
			 formobj.displayDetails.value=defaultDisplayDetValue;
			 f_editboxes(formobj,0,mileStoneId,counteperMilestone);
			 isSelectedMileStone=true;
	    }
                
     }
     else {
		 for (i=loopCount;i<totCount;i++){
			 	 if(formobj.selectMilestone[i].checked){
							 var counteperMilestone=parseInt(formobj.countPerMilestone[i].value);
							 var mileStoneId=parseInt(formobj.mileStoneId[i].value);
							 formobj.displayDetails[i].value=defaultDisplayDetValue;
							 f_editboxes(formobj,i,mileStoneId,counteperMilestone);
							 isSelectedMileStone=true;
			 	 }
	     }
    }
     if(!isSelectedMileStone){
    	 var paramArray = [mileTypeHdr];
    	 alert(getLocalizedMessageString("M_NoMstoneSec_SelMstone",paramArray));/*alert("No Milestone is selected for '"+ mileTypeHdr+"' section. Please select the Milestone first.");*****/
    	 return false;
     }
 }
	//Ashu Added for enhancement-Req_spec_INV1_REQ
	 
	//Added this method to support 'Set All To' functionality
  
  function setAllPageLevel(formobj){
	  var loopCount=0;
	  
	  
	  var chkAllLen =formobj.chkall.length;	  
	  var totCount = parseInt(formobj.totalmilecount.value);
	  
	  var selecCount= parseInt(formobj.selectedCount.value);
	  var defaultDisplayDetValue=formobj.defaultDisplayDetails[0].value;
	  
	  for (j=0;j<chkAllLen-1;j++){   //Iterate throug each 'Check all' list milestones and check whether header is not'AM'  
		  var mileStoneType=formobj.milestoneType[j].value;
		  var totCount=parseInt(formobj.mileCount[j].value);
		  totCount=loopCount+totCount;
		  if(mileStoneType!="AM"){
	          //Added the code to loop trhough each milestone except "AM" to set detail values.
			   for (i=loopCount;i<totCount;i++){
					 	 if(formobj.selectMilestone[i].checked){
									 var counteperMilestone=parseInt(formobj.countPerMilestone[i].value);
									 var mileStoneId=parseInt(formobj.mileStoneId[i].value);
									 formobj.displayDetails[i].value=defaultDisplayDetValue;
									 //Call ' f_editboxes'method when set the detailDisplayed value(Aleady implemented functionality).
									 f_editboxes(formobj,i,mileStoneId,counteperMilestone);
									
					 	 }
			   }
				loopCount=totCount;		  
		 }
	  }
     //If no mile stone selected then publish this message.
     if(selecCount==0){
    	 alert("<%=MC.M_NoMstoneSel_SelMstone%>");/*alert("No Milestone is selected. Please select the Milestone first.");*****/
    	 
    	 return false;
     }
 }
 </SCRIPT>

</HEAD>
	<%-- Nicholas : Start --%>
	<jsp:include page="skinChoser.jsp" flush="true"/>
	<%-- Nicholas : End --%>

<BODY>
  <jsp:useBean id="statusB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>
  <jsp:useBean id="InvB" scope="request" class="com.velos.eres.web.invoice.InvoiceJB"/>
  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,java.text.*"%>
  <%

	String src = null;

	String eSign = request.getParameter("eSign");
	String mode = request.getParameter("mode");

	String studyId = request.getParameter("studyId");


HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
//   	String oldESign = (String) tSession.getValue("eSign");
	String eventId = (String) tSession.getValue("eventId");

//	if(!oldESign.equals(eSign))
//	{
%>
  
<%
//	}
//	else   	{
	
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");

	String invNumber = request.getParameter("invNumber");
	String paymentDueIn = request.getParameter("paymentDueIn");
	String paymentDueUnit = request.getParameter("paymentDueUnit");
	String addressedTo = request.getParameter("addressedTo");
	String sentFrom = request.getParameter("sentFrom");
	String invHeader = request.getParameter("invHeader");
	String invFooter = request.getParameter("invFooter");
	String invDate = request.getParameter("invDate");
	String milestoneReceivableStatus = request.getParameter("milestoneReceivableStatus");
	String dateRangeType = request.getParameter("dateRangeType");
	String invNotes = request.getParameter("invNotes");
	String dtFilterDateFrom = request.getParameter("dtFilterDateFrom");
	String dtFilterDateTo = request.getParameter("dtFilterDateTo");
	String dSites = request.getParameter("dSites");
	String intAccNum = request.getParameter("intAccNum");//JM:
	
	//Commented by Manimaran for Requirement Changes for FIN12
	//String includeHeader = request.getParameter("includeHeader");//KM
	//String includeFooter = request.getParameter("includeFooter");

	int invPk = 0;
	

%>

<%
//04Apr2011 @Ankit #5780
String flag = (String)session.getAttribute("flag");
if(flag!=null)
{
	session.removeAttribute("flag");
}
else
{
	invPk = 1;
}
if (StringUtil.isEmpty(mode) && flag!=null)
{
	mode = "N";
}

	if(mode.equals("N"))
	{
			//04Apr2011 @Ankit #5771
			if(invNumber==null || invNumber.equals("")){
				invNumber = InvB.getInvoiceNumber();
			}
			InvB.setInvAddressedto(addressedTo);
			InvB.setInvDate(invDate);
			InvB.setInvFooter(invFooter) ;
			InvB.setInvHeader(invHeader) ;
			InvB.setInvIntervalFrom(dtFilterDateFrom) ;
			InvB.setInvIntervalTo(dtFilterDateTo);
			InvB.setInvIntervalType(dateRangeType) ;
			InvB.setInvMileStatusType(milestoneReceivableStatus) ;
			InvB.setInvNotes(invNotes);
			InvB.setInvNumber(invNumber) ;
			//setInvOtherUser(iBean.getInvOtherUser()) ;
			InvB.setInvSentFrom(sentFrom) ;
			InvB.setInvPayDueBy(paymentDueIn);
			InvB.setInvPayUnit(paymentDueUnit);
			InvB.setCreator(usr);
			InvB.setIPAdd(ipAdd);
			InvB.setStudy(studyId );
			InvB.setSite(dSites);
			InvB.setIntnlAccNum(intAccNum);//JM
			InvB.setInvStat("W");
			InvB.setInvoiceDetails();
			invPk = InvB.getId();



	//}

	//---------------JM: 21Feb2008 added for #FIN11: Feb2008 enhancements-----------
		String moduleId="";
        CodeDao codedao = new CodeDao();
	    int codeLstId = 0;
	    String codeLstStatId="";

	    Date dt = new java.util.Date();
		String stDate = DateUtil.dateToString(dt);
	

		codeLstId = codedao.getCodeId("invStatus","W");
		codeLstStatId= ""+ codeLstId;
		if(mode.equals("N")) {
			   moduleId=invPk+"";
		}


			statusB.setStatusModuleId(moduleId);
			statusB.setStatusModuleTable("er_invoice");
			statusB.setStatusCodelstId(codeLstStatId);
			statusB.setStatusStartDate(stDate);
			statusB.setIpAdd(ipAdd);

			if(mode.equals("N")) {
			     statusB.setCreator(usr);
			     statusB.setRecordType("N");
			     statusB.setStatusHistoryDetailsWithEndDate();
            }

	//---------------JM: 21Feb2008-----------
}//blocked up above...



%>

<SCRIPT>
	//05Apr2011 @Ankit #4814
	fclose_to_role();
</SCRIPT>
 <!-- Get the achieved milestones for invoice -->
 <%
 	int totalmilecount = 0;
 	int totalCounter = 0;
 	//Ashu Added for enhancement-Req_spec_INV1_REQ
 	int mileCount = 0;
 	int milestoneId=0;
 	int totalCheckAllCount = 0;
 	int detailCount = 0;

 if (invPk > 0)
 {

 	%>

 	<DIV class="popDefault"  >
	 	<form name="invdet" action="geninvoice.jsp" method="post" onSubmit="return validate(document.invdet)" >
		<input type="hidden" name="studyId" value="<%=studyId%>"/>
		<input type="hidden" name="invPk" value="<%=invPk%>"/>

	 		<table width="100%">
			<tr>
	        <th><%=LC.L_Select %><%-- Select*****--%></th>
	        <th><%=LC.L_Milestone %><%-- Milestone*****--%></th>
	        <th><%=LC.L_Count %><%-- Count*****--%> </th>
	        <th><%=LC.L_Mstone_Amt %><%-- Milestone Amount*****--%></th>
	        <th><%=LC.L_Previously_InvAmt %><%-- Previously Invoiced Amount*****--%></th>
	        <th><%=LC.L_Inv_Amt %><%-- Invoice Amount*****--%></th>
			 </tr>
			
			<tr id="checkAll" name="checkAll" style="visibility:hidden;">
			
				<td><input type="checkbox" name="chkall"
					onClick="checkAll(document.invdet,0,0,false,0)"> <b><%=LC.L_Check_All%><%--Check All*****--%></b></td>
				<td colspan=4>
				<a href=# onClick="return setAllPageLevel(document.invdet);"><b><%=LC.L_Set_AllTo %><%--Set All To *****--%></b> </a>
					<Select name="defaultDisplayDetails" onChange="">
					<option value="0" SELECTED><%=MC.M_High_LevelView %><%--High Level View*****--%></option>
					<option value="1"><%=LC.L_Detail_View %><%--Detail View*****--%></option>
				</Select>
				</td>	
						
			</tr>

 	<%
 		// iterate through 5 milestone types

 		String[] arMilestoneType =  new String[5];
 		arMilestoneType[0] = "PM";
 		arMilestoneType[1] = "VM";
 		arMilestoneType[2] = "EM";
 		arMilestoneType[3] = "SM";
		arMilestoneType[4] = "AM";//KM


 		String[] arMilestoneTypeHeader =  new String[5];
 		arMilestoneTypeHeader[0] = LC.L_Pat_StatusMstones;/*arMilestoneTypeHeader[0] = "Patient Status Milestones";*****/
 		arMilestoneTypeHeader[1] = LC.L_Visit_Mstones;/*arMilestoneTypeHeader[1] = "Visit Milestones";*****/
 		arMilestoneTypeHeader[2] = LC.L_Evt_Mstones;/*arMilestoneTypeHeader[2] = "Event Milestones";*****/
 		arMilestoneTypeHeader[3] = LC.L_Std_Mstones;/*arMilestoneTypeHeader[3] = "Study Milestones";*****/
 		arMilestoneTypeHeader[4] = LC.L_Addl_Mstones;/*arMilestoneTypeHeader[4] = "Additional Milestones";*****/


 		String displayMilestoneHeader ="";


 		for (int t = 0; t <arMilestoneType.length;t++)
 		{


 			%>

 			<tr colspan = 6>
		        <td><p class="successfulmsg"><%=arMilestoneTypeHeader[t]%></p></td>
		     </tr>

		 	<%
	 		MileAchievedDao mile = new MileAchievedDao();

	 		MilestoneDao milestoneDao = new MilestoneDao();
	 		ArrayList arMilestoneIds = new ArrayList();
	 		ArrayList arMilestoneRuleDescs = new ArrayList();

	 		ArrayList arMilestoneAchievedCounts = new ArrayList();
	 		ArrayList arCountPerMilestone = new ArrayList();
	 		ArrayList arMileAmounts = new ArrayList();
	 		ArrayList arMileTotalInvoicedAmounts = new ArrayList();



	 		String mileStoneId = "";
	 		Hashtable htAchieved = new Hashtable();

	 		int milecount = 0;
	 		int mileDetailCount = 0;
	 		String mileAchCount = "0";
	 		String mileDesc = "";
		  int countPerMilestone  = 0;

		   double calcAmount = 0.00;
		   double mileAmount = 0.00;
		   double mileAmountForGroupMilestones = 0.00;
		   double mileDetailAmountsPrevInvoiced = 0.00;
  		   double totalMilePrevInvoicedAmount = 0.00;


	 		mile = InvB.getAchievedMilestones(studyId, dtFilterDateFrom,dtFilterDateTo, milestoneReceivableStatus,arMilestoneType[t],dSites);

	 		milestoneDao = mile.getMilestoneDao();
	 		htAchieved = mile.getHtAchieved();

	 		arMilestoneIds = milestoneDao.getMilestoneIds();
			//KM
			if(!arMilestoneType[t].equals("AM"))
	 		    arMilestoneRuleDescs = milestoneDao.getMilestoneRuleDescs();
			else
				arMilestoneRuleDescs = milestoneDao.getMilestoneDescriptions();

	 		arMilestoneAchievedCounts = milestoneDao.getMilestoneAchievedCounts();
	 		arCountPerMilestone  = milestoneDao.getMilestoneCounts();
	 		arMileAmounts =  milestoneDao.getMilestoneAmounts();
			arMileTotalInvoicedAmounts  =  milestoneDao.getTotalInvoicedAmounts();


	 		if (arMilestoneIds != null)
	 		{
	 			milecount = arMilestoneIds.size();

	 		}
	 		else
	 		{
		 		milecount  = 0;
	 		}
	 		
	 		totalmilecount = totalmilecount + milecount;
	 		mileCount = milecount;
			//Ashu Added for enhancement-Req_spec_INV1_REQ
			if(milecount>0) { 
				totalCheckAllCount=totalCheckAllCount+1;
		 	%>
						<tr>
									<td><input type="checkbox" name="chkall" onClick="checkAll(document.invdet,'<%=totalCounter%>','<%=mileCount%>',true,'<%=totalCheckAllCount%>')">
									 <b><%=LC.L_Check_All %><%--Check All*****--%></b>
									 <input name="milestoneType" type="hidden" value="<%=arMilestoneType[t]%>"></input>
									 <input type= "hidden"  name="mileCount"  value="<%=milecount%>" />
									 </td>
									<td colspan=4> 
									<%
		 			        	      if (!arMilestoneType[t].equals("AM")) {
		 			                %>	
									<a href="JavaScript:void(0);" onClick="return setAll(document.invdet,'<%=totalCounter%>','<%=mileCount%>','<%=totalCheckAllCount%>','<%=arMilestoneTypeHeader[t]%>');"><b><%=LC.L_Set_AllTo %><%-- Set All To*****--%></b> </a>
										<Select name="defaultDisplayDetails" onChange="">
										<option value="0" SELECTED><%=MC.M_High_LevelView %><%--High Level View*****--%></option>
										<option value="1"><%=LC.L_Detail_View %><%--Detail View*****--%></option>
									</Select>
									</td>
                    	</tr>
	
		 <%  } }
	 		

	 		// for each milestone, get the milestone achieved details

	 		for (int i =0; i < milecount ; i++)
	 		{


	 			mileDesc = (String ) arMilestoneRuleDescs.get(i);
	 			mileStoneId = String.valueOf(((Integer)arMilestoneIds.get(i)).intValue());

				MileAchievedDao ach = new MileAchievedDao();
				ArrayList patients = new ArrayList();
				ArrayList ids = new ArrayList();
				ArrayList patientCodes = new ArrayList();
				ArrayList achievedOn = new ArrayList();
				ArrayList arMileDetailAmountsPrevInvoiced = new ArrayList();

				countPerMilestone =  EJBUtil.stringToNum((String) arCountPerMilestone.get(i));
				mileAchCount = (String) arMilestoneAchievedCounts.get(i);
				mileAmount = Double.parseDouble((String) arMileAmounts.get(i));

				totalMilePrevInvoicedAmount = 0;


				if (countPerMilestone > 1)
				{
			 		mileAmountForGroupMilestones = mileAmount/countPerMilestone;
			 	}

				if (htAchieved.containsKey(mileStoneId))
				{


					ach = (MileAchievedDao) htAchieved.get(mileStoneId);
					ids = ach.getId();
					patients = ach.getPatient();
					mileDetailCount = ids.size();
					patientCodes = ach.getPatientCode();
					calcAmount = EJBUtil.stringToNum(mileAchCount) * mileAmount;
					achievedOn = ach.getAchievedOn();
					arMileDetailAmountsPrevInvoiced = ach.getAmountInvoiced();
					
					//for (int ct = 0; ct < arMileDetailAmountsPrevInvoiced.size(); ct++)
					//{
						//totalMilePrevInvoicedAmount = totalMilePrevInvoicedAmount + Double.parseDouble((String) arMileDetailAmountsPrevInvoiced.get(ct));
					//}
	                
					
					

				}
				else
				{
					mileAchCount = "0";
					mileDetailCount = 0;
					
					if(!arMilestoneType[t].equals("AM"))
					{
						calcAmount = 0.00;
					}
					else
					{
						calcAmount = mileAmount;
					}	
					
					
				}
				
				
				String strTotalPrevInvAmount = "";
				strTotalPrevInvAmount = (String)arMileTotalInvoicedAmounts.get(i);
				
				if (! StringUtil.isEmpty(strTotalPrevInvAmount))
				{
					 totalMilePrevInvoicedAmount = Double.parseDouble(strTotalPrevInvAmount);
				}
				else
				{
					totalMilePrevInvoicedAmount = 0;
				}
				
				
				totalMilePrevInvoicedAmount = Math.rint(totalMilePrevInvoicedAmount * 100)/100;



	 			%>
	 			<tr>
		 			<td> <input type="checkbox" onClick= "setSelected(document.invdet,'<%=totalCounter%>')" name="selectMilestone" value="<%=mileStoneId%>"/>
		 			<input type="hidden" name="isChecked" value="0"/>
		 			<%if(!arMilestoneType[t].equals("AM")) {%>

					<Select name="displayDetails" onChange="f_editboxes(document.invdet,'<%=totalCounter%>','<%=mileStoneId%>','<%=countPerMilestone%>')"><option value="0" SELECTED><%=MC.M_High_LevelView %><%-- High Level View*****--%></option>
		 			<option value="1" ><%=LC.L_Detail_View %><%-- Detail View*****--%></option></Select>
					<%}
					else {%>
					<input name="displayDetails" type="hidden" value="0">
					<%}%>
		 			</td>
		 			<td><%=mileDesc %></td>
		 			<td>
		 			 <% if(!arMilestoneType[t].equals("AM")) { %>
		 				<%=mileAchCount%>
		 			<% } %>
		 			</td>
		 			<td><%=NumberUtil.roundOffNo(calcAmount)%></td>
		 			<td><%=NumberUtil.roundOffNo(totalMilePrevInvoicedAmount)%></td>

				 	<td><input type="text" name="mileAmountInvoiced"  value="<%=NumberUtil.roundOffNo(Math.rint((calcAmount-totalMilePrevInvoicedAmount)*100)/100) %>" onblur="checkAmt(<%=mileAmount%>,<%=mileAchCount%>,<%=NumberUtil.roundOffNo(totalMilePrevInvoicedAmount)%>,this,'<%=arMilestoneType[t]%>');" />
				 		<input type= "hidden"  name="mileStoneId"  value="<%=mileStoneId%>" />
				 		<input type= "hidden"  name="countPerMilestone"  value="<%=countPerMilestone%>" />
				 		<input type= "hidden"  name="mileAmountDue"  value="<%=calcAmount%>" />
				 		<input type= "hidden"  name="mileAchCount"  value="<%=mileAchCount%>" />

				 		<input type= "hidden"  name="origmileAmount"  value="<%=mileAmount%>" />
				 		<input type= "hidden"  name="origmileAmountInvoiced"  value="<%=Math.rint((calcAmount-totalMilePrevInvoicedAmount)*100)/100%>" />
			 			<input type= "hidden"  name="mileDetailCount"  value="<%=mileDetailCount%>" />
			 			<input type= "hidden"  name="mileAmountForGroupMilestones"  value="<%=mileAmountForGroupMilestones%>" />
			 			<input type= "hidden"  name="milePrevInvoicedAmount"  value="<%=NumberUtil.roundOffNo(totalMilePrevInvoicedAmount)%>" />
			 			<input type= "hidden"  name="detailMilestoneType"  value="<%=arMilestoneType[t]%>" />


				 	</td>

	 			</tr>


	 			<%
	 				for (int k = 0; k < mileDetailCount; k++)
	 				{

	 				mileDetailAmountsPrevInvoiced = Double.parseDouble((String) arMileDetailAmountsPrevInvoiced.get(k));
	 				%>
	 				
	 				
	 				<input type="hidden" name="mileDetailPat<%=mileStoneId%>"  value="<%= (String)patients.get(k)%>" />
	 				<input type="hidden" name="mileDetAchievedOn<%=mileStoneId%>"  value="<%=(String)achievedOn.get(k)%>" />
	 				<input type="hidden" name="mileDetFKAchieved<%=mileStoneId%>"  value="<%=(String)ids.get(k)%>" />
	 				


		 				<%
						if(k%2==0){
					%>
						<TR class="browserEvenRow">
					<%
					}else{
					%>
						<TR class="browserOddRow">
					<%
					}
					%>
					 <td><input type="checkbox" name="mileDetailCheck<%=mileStoneId%>"  id="mileDetailCheck<%=detailCount%>" onClick="toggleSelect(document.invdet,'<%=k%>',<%=mileStoneId%>,<%=mileDetailCount%>,<%=detailCount%>)" value="0" disabled/>
					     <input type="hidden" name="mileDetailCheckHidden<%=mileStoneId%>"  value="0" />
					 </td>
		 			<td>&nbsp;</td>
		 			<td>&nbsp;&nbsp;&nbsp;<%= (String) patientCodes.get(k)%></td>
		 			<td>
		 			<%
				 		if (countPerMilestone <= 1)
				 		{
				 		%>
				 			<%=NumberUtil.roundOffNo(mileAmount)%>
				 			<input type="hidden" name="mileDetailAmount<%=mileStoneId%>"  value="<%=NumberUtil.roundOffNo(mileAmount)%>" />
				 		<%
				 		}
				 		else { %>

					 		<%=NumberUtil.roundOffNo(mileAmountForGroupMilestones)%>
					 		<input type="hidden" name="mileDetailAmount<%=mileStoneId%>"  value="<%=mileAmountForGroupMilestones%>" />
				 		<%}

				 	%>
				 		</td>
		 			<td><%=NumberUtil.roundOffNo(mileDetailAmountsPrevInvoiced)%></td>
				 	<td><%
				 		if (countPerMilestone <= 1)
				 		{
				 			%>

				 			<input type="text" READONLY name="mileDetailAmountInvoiced<%=mileStoneId%>" id="mileDetailAmountInvoiced<%=detailCount%>" value="<%=NumberUtil.roundOffNo(Math.rint((mileAmount - mileDetailAmountsPrevInvoiced)*100)/100)%>"  onblur="checkDetailAmount(<%=NumberUtil.roundOffNo(Math.rint((mileAmount - mileDetailAmountsPrevInvoiced)*100)/100)%>,this);" />
				 			<input type="hidden" name="mileDetailAmountInvoicedPrev<%=mileStoneId%>" id="mileDetailAmountInvoicedPrev<%=detailCount%>" value="<%=NumberUtil.roundOffNo(Math.rint((mileAmount - mileDetailAmountsPrevInvoiced)*100)/100)%>" />

				 			<%
				 		}
				 		else { %>
				 			<input type="text" READONLY name="mileDetailAmountInvoiced<%=mileStoneId%>" id="mileDetailAmountInvoiced<%=detailCount%>"  value="<%=NumberUtil.roundOffNo(Math.rint((mileAmountForGroupMilestones - mileDetailAmountsPrevInvoiced)*100)/100)%>" onblur="checkDetailAmount(<%=NumberUtil.roundOffNo(Math.rint((mileAmountForGroupMilestones - mileDetailAmountsPrevInvoiced)*100)/100)%>,this);" />
				 			<input type="hidden" name="mileDetailAmountInvoicedPrev<%=mileStoneId%>" id="mileDetailAmountInvoicedPrev<%=detailCount%>"  value="<%=NumberUtil.roundOffNo(Math.rint((mileAmountForGroupMilestones - mileDetailAmountsPrevInvoiced)*100)/100)%>" />

				 		<%}

				 	%></td>
		 			</tr>


	 				<%
	 					detailCount++;
	 				}

	 			totalCounter = totalCounter + 1;
	 			// end of for
	 		}
	 	}	// for milestone types


 %>
 </table>

<P>
<br>
<input type= "hidden"  name="totalmilecount"  value="<%=totalmilecount%>" />
<input type= "hidden"  name="detailCount"  value="<%=detailCount%>" />
<input type= "hidden"  name="selectedCount"  value="0" />
<button onClick="reclaculate(document.invdet)"><%=LC.L_ReCalculate%></button>
<button type="submit"><%=LC.L_Generate_Invoice%></button>
</P>
</form>
</div>
<%

	}
	else
	{
		%>
			<%=MC.M_InvCnt_Svd%><%-- Invoice could not be saved*****--%>

		<%
	}

//}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

<script>
//Ashu Added for enhancement-Req_spec_INV1_REQ
var len =document.invdet.chkall.length;

if(len>=3)
	{
		document.all.checkAll.style.visibility = "visible"; // to make page level 'Check All' and 'Set All To' functionality visible.
	}
	
</script>

</HTML>





