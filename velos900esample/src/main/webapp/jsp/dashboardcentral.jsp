<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="acmod" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/><!--km-->
<jsp:useBean id="groupB" scope="page" class="com.velos.eres.web.group.GroupJB"/><!--km-->
<jsp:useBean id="repdao" scope="request" class="com.velos.eres.business.common.ReportDaoNew" />
<jsp:useBean id="repdao1" scope="request" class="com.velos.eres.business.common.ReportDaoNew" />
<jsp:useBean id="grpRights" scope="request" class="com.velos.eres.web.grpRights.GrpRightsJB" />
<jsp:useBean id="ctrlD" scope="request" class="com.velos.eres.business.common.CtrlDao" />
<jsp:useBean id="siteB" scope="request" class="com.velos.eres.web.site.SiteJB" />
<jsp:useBean id="codeB" scope="request" class="com.velos.eres.web.codelst.CodelstJB" />
<jsp:useBean id="repB" scope="request" class="com.velos.eres.web.report.ReportJBNew" />
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao" />
<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />

<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.EJBUtil,java.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,java.sql.*,com.velos.eres.business.common.*,com.velos.eres.service.util.StringUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.*"%>
</head>

<title><%=LC.L_Dboard_Central%><%--Dashboard Central*****--%></title>
<!-- <link href="./js/tree/xmlTree.css" type="text/css" rel="stylesheet"> -->
<script type="text/javascript" src="./js/chart/EJSChart.js"> </script>
<script type="text/javascript" src="./js/tree/xmlTree.js"></script>
<script type="text/javascript">//km
var chart ='';

function openNewWin(url){
	var w = screen.availWidth-100;
	windowName = window.open(url,"Win","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width="+w+",height=800,left=10,top=100");
	windowName.focus();
}

function changeDBType(formobj){ 
 	formobj.action="dashboardcentral.jsp?";
 	formobj.submit();
}
function getElementsById(sId){
	var outArray = new Array();	
	if(typeof(sId)!='string' || !sId)
	{
		return outArray;
	};
	
	if(document.evaluate)
	{
		var xpathString = "//*[@id='" + sId.toString() + "']"
		var xpathResult = document.evaluate(xpathString, document, null, 0, null);
		while ((outArray[outArray.length] = xpathResult.iterateNext())) { }
		outArray.pop();
	}
	else if(document.all)
	{
		
		for(var i=0,j=document.all[sId].length;i<j;i++){
			outArray[i] =  document.all[sId][i];
		}
		
	}else if(document.getElementsByTagName)
	{
	
		var aEl = document.getElementsByTagName( '*' );	
		for(var i=0,j=aEl.length;i<j;i+=1){
		
			if(aEl[i].id == sId )
			{
				outArray.push(aEl[i]);
			};
		};	
		
	};
	
	return outArray;
 }

function removeChart(chart){
	if (chart.length != 0){
		chart.remove();
	}
}

function callAjaxGetFinancialData(nodeNo, snodeNo, linkVal){
	//if (parseInt(linkVal) == 0) return;
	var ao=new VELOS.ajaxObject("ajaxFinancialDataFetch.jsp", {
	urlData:"nodeNo="+nodeNo + "&snodeNo="+snodeNo  ,
	reqType:"POST",
	outputElement: "Browser" } 
   );
   ao.startRequest();
}

function confirmBox(name){
	var paramArray = [name];
    if (confirm(getLocalizedMessageString("M_Want_DelStd",paramArray))) {/* if (confirm("Do you want to delete <%=LC.Std_Study%> "+ name+"?" )) {*****/
		return true;
	}  else {
		return false;
	}
}//km

function opensvwin(id) {
     windowName = window.open("studyview.jsp?studyVPk="+ id,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
	 windowName.focus();
;}

</script>
<%
	//Declarations
	String src="tdMenuBarItem3";
	String studySql;
	String countSql;
	String calledfrom = request.getParameter("calledfrom");

	long rowsPerPage=0;
	long totalPages=0;	
	long rowsReturned = 0;
	long showPages = 0;
	long totalRows = 0;	   
	long firstRec = 0;
	long lastRec = 0;	   
	int openCount = 0;
	int resolvedCount = 0;
	int reopenCount = 0;
	int closedCount = 0;
	int enrolled = 0;

	String studyNumber = null;
	String studyTitle = null;
	String studyStatus = null;
	String studyActualDt = null;
	String studyIRB = null;
	String studyIRB2 = null;
	String studySite = null;
	String studyPI = null;

	int studyId=0;
	boolean hasMore = false;
	boolean hasPrevious = false;
	CodeDao  cd = new CodeDao();
	String codeLst= cd.getValForSpecificSubType();
	HttpSession tSession = request.getSession(true); 
	String userId= (String) tSession.getValue("userId");
	String searchCriteria= request.getParameter("searchCriteria");
	String protocolStatus= request.getParameter("protocolStatus");
	
	String pagenum = "";
	int curPage = 0;
	String orderBy = "";
	orderBy = request.getParameter("orderBy");

	String orderType = "";
	orderType = request.getParameter("orderType");

	BrowserRows pr;
	BrowserRows br;
	
	long startPage = 1;
	long cntr = 0;
	String pagenumView = request.getParameter("pageView");
	if (pagenumView == null) {pagenumView = "1";}
	int curPageView = EJBUtil.stringToNum(pagenumView);
	
	long startPageView = 1;
	long cntrView = 0;
	String defGroup;
	String groupName;
	String rightSeq;
	
	String orderByView = "";
	orderByView = request.getParameter("orderByView");

	String orderTypeView = "";
	orderTypeView = request.getParameter("orderTypeView");
	
	int usrId;
	String dProtocolStatus = "" ;
	CodeDao cd1 = new CodeDao();
	int pageRight;
	//GrpRightsJB grpRights;
	ArrayList aRightSeq ;
	int iRightSeq ;
	String accountId= (String) tSession.getValue("accountId");
%>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<body onload="onLoad();">
<!-- div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div-->
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>
	<%
	String report_id = "", report_name = "", report_type = "", repid_name = "";
	StringBuffer rep_first = new StringBuffer();
	StringBuffer dashboardCatDD = new StringBuffer();
	String dashboardCat = request.getParameter("dbcat");
	
	if (sessionmaint.isValidSession(tSession)){
		String acc = (String) tSession.getValue("accountId");
		int accId = EJBUtil.stringToNum(acc);
		
		String defUserGroup = (String) tSession.getAttribute("defUserGroup");
		

		String protocolManagementRight = (String) tSession.getValue("protocolManagementRight");

		int protocolManagementRightInt = 0;
		protocolManagementRightInt = EJBUtil.stringToNum(protocolManagementRight);
		dashboardCat = (dashboardCat == null) ? "" : dashboardCat;
		
		CodeDao codeDao = codeB.getCodeDaoInstance();
		codeDao.getCodeValues("dashboard");
		codeDao.setCType("dashboard");
	    codeDao.setForGroup(defUserGroup);
	    codeDao.getHiddenCodelstDataForUser();
		
		ArrayList dashboardCatList = codeDao.getCDesc();
		//Yogendra : Bug#9224 (Hide the Dashboard types: Data Manager and Financial Manager dashboards ):11 April 2012 
		ArrayList<String> removeDashboardList = new ArrayList<String>();
		//Query Management Dashboard,IRB Dashboard,Financial Dashboard,Data Manager Dashboard
		removeDashboardList.add(LC.L_Fin_Dboard); /* removeDashboardList.add("Financial Dashboard");*****/
		removeDashboardList.add(LC.L_DataMgr_Dboard); /*removeDashboardList.add("Data Manager Dashboard");*****/
		removeDashboardList.add(LC.L_Irb_Dboard); /*removeDashboardList.add("IRB Dashboard");*****/
		dashboardCatList.removeAll(removeDashboardList);
		//Changes End For Bug#9224
		//Retrieve all the filter columns available for this category
		ArrayList repFilterColumns = new ArrayList();
		ArrayList repMapFilterColumns = new ArrayList();
		ArrayList repFilterDispNames = new ArrayList();
		ArrayList repFilterMandatoryList = new ArrayList();
		ArrayList repFilterDependOnList = new ArrayList();
		ArrayList repFilterKeywords = new ArrayList();
		ArrayList repFilterDispDIV = new ArrayList();

		//Check rights
		grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
		String modRight = (String) tSession.getValue("modRight");

		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("DASH"));

	
		int modlen = modRight.length();

		modCtlDao.getControlValues("module"); //get extra modules

		ArrayList modCtlDaofeature = modCtlDao.getCValue();
		ArrayList modCtlDaoftrSeq = modCtlDao.getCSeq();

		int modseq = 0, budseq = 0, mileSeq = 0, milestoneSeq = 0, dsmSeq = 0;
		int patProfileSeq = 0, formLibSeq = 0, dsmModSeq = 0;
		int adhocqSeq = 0;
		int adhocqModSeq = 0;

		char budRight = '0';
		char mileRight = '0';
		char dataSafMonAppRight = '0';
		char patProfileAppRight = '0';
		char formLibAppRight = '0';
		char adhocqAppRight = '0';

		modseq = modCtlDaofeature.indexOf("MODBUD");
		mileSeq = modCtlDaofeature.indexOf("MODMIL");
		dsmModSeq = modCtlDaofeature.indexOf("MODDSM");
		patProfileSeq = modCtlDaofeature.indexOf("MODPATPROF");
		formLibSeq = modCtlDaofeature.indexOf("MODFORMS");
		adhocqModSeq = modCtlDaofeature.indexOf("ADHOCQ");

		budseq = ((Integer) modCtlDaoftrSeq.get(modseq)).intValue();
		milestoneSeq = ((Integer) modCtlDaoftrSeq.get(mileSeq)).intValue();
		patProfileSeq = ((Integer) modCtlDaoftrSeq.get(patProfileSeq)).intValue();
		formLibSeq = ((Integer) modCtlDaoftrSeq.get(formLibSeq)).intValue();
		dsmSeq = ((Integer) modCtlDaoftrSeq.get(dsmModSeq)).intValue();
		adhocqSeq = ((Integer) modCtlDaoftrSeq.get(adhocqModSeq)).intValue();

		budRight = modRight.charAt(budseq - 1);
		mileRight = modRight.charAt(milestoneSeq - 1);
		dataSafMonAppRight = modRight.charAt(dsmSeq - 1);
		patProfileAppRight = modRight.charAt(patProfileSeq - 1);
		formLibAppRight = modRight.charAt(formLibSeq - 1);
		adhocqAppRight = modRight.charAt(adhocqSeq - 1);


		dashboardCatDD.append("<SELECT NAME='dbcat' id='dbcat' onchange='javascript:changeDBType(document.dashboardpg)'>");
	 
		String tempReport = "";
		String hideStr = "";
		
		for (int counter = 0; counter < dashboardCatList.size(); counter++) {
			tempReport = (String) dashboardCatList.get(counter);
			hideStr = codeDao.getCodeHide(counter) ;
			if (StringUtil.isEmpty(hideStr))
			{
				hideStr = "N";
			}
			
			//Assumption is that dashboardCatList will never be null
			if (dashboardCat.length() == 0 && hideStr.equals("N"))
				dashboardCat = ((String) dashboardCatList.get(counter)).toLowerCase();
			
			if (dashboardCat.equals(tempReport.toLowerCase()))
			{
				if (hideStr.equals("N"))
                	{
						dashboardCatDD.append("<OPTION value = '" 	+ tempReport.toLowerCase() + "' selected>" 	+ tempReport + "</OPTION>");
					}	
			}else{
					 
				if (hideStr.equals("N"))
                	{
						dashboardCatDD.append("<OPTION value = '" 	+ tempReport.toLowerCase() + "'>" + tempReport + "</OPTION>");
					}	
			}	
		}

		dashboardCatDD.append("</SELECT>"); 
	 		
		//end processing
		
		/*codeDao.resetDao();
		codeDao.getCodeValues("dashboard", dashboardCat, "codelst_seq");
		ArrayList codeSubTypeList = codeDao.getCSubType();
		ArrayList codeDescList = codeDao.getCDesc();
		ArrayList reportFilterKeywords = new ArrayList();
		ArrayList reportFilterApplicable = new ArrayList();*/
	}
%>

<Form name="dashboardpg" method=post onsubmit="" action="">
<div class="BrowserTopn">
<!-- New Dashlet BEGIN-->
	<Table width="100%" cellpadding="0" cellspacing="0" border="0">
		<%if (StringUtil.isEmpty(calledfrom) )
			{ %>
		<tr height="45" class="searchBg">
		<td width="1%"></td>
				<td  width="99%"><b><%=LC.L_Select_DboardType%><%--Select Dashboard Type*****--%></b> &nbsp;&nbsp;<%=dashboardCatDD.toString()%>
			</td>
		</tr>
	</Table>
</div>
<DIV class="BrowserBotN BrowserBotN_D_1" id="div1">
	<DIV id="Dashboard"><%
	if (dashboardCat.equals(LC.L_Fin_Dboard_Lower)){ /*if (dashboardCat.equals("financial dashboard")){*****/
	%>
		<jsp:include page="db_financial.jsp" flush="true"/>
	<%}
	if (dashboardCat.equals(LC.L_DataMgr_Dboard_Lower)){ /*if (dashboardCat.equals("data manager dashboard")){*****/
		%>
		<jsp:include page="db_datamanager.jsp" flush="true"/>
	<%}	 
	if (dashboardCat.equals(LC.L_QryMgmt_Dboard_Lower)){ /*if (dashboardCat.equals("query management dashboard")){*****/
		%>
		<jsp:include page="db_queryMgmt.jsp" flush="true"/>
	<%}	 %>

	</DIV>
</DIV>
<!-- New Dashlet END -->
<% } //end of if
	else
{%>
	<jsp:include page="timeout.html" flush="true"/>
<%
}
%>
</Form>
<div><jsp:include page="bottompanel.jsp" flush="true" /></div>
</body>
</html>
