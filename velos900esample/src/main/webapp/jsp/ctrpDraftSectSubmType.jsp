<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>

<script>
$j(document).ready(function(){
	submTypeAction();
});
var submTypeA = <s:property escape="false" value="ctrpDraftJB.trialSubmTypeA" />
var submTypeAorU = <s:property escape="false" value="ctrpDraftJB.trialSubmTypeAorU" />
var submTypeAorO = <s:property escape="false" value="ctrpDraftJB.trialSubmTypeAorO" />
var submTypeU = <s:property escape="false" value="ctrpDraftJB.trialSubmTypeU" />
var statusTypeWithdrawn = <s:property escape="false" value="ctrpDraftJB.statusTypeWithdrawn" />

function submTypeAction() {
	populateTrialStatus();
	setAsteriskOnXMLReqd();
	setAmendmentExtra();
	setAsteriskOnNCIId();
	setAsteriskOnLeadOrg();
	setAsteriskOnProtocolIRBDoc();
}

//populateTrialStatus() added to fix Bug#8215 : Raviesh
var withdrawnOption;
function populateTrialStatus(){
	var trialStatus= "#ctrpDraftJB\\.trialStatus";
	var trialStatusOptions=$j(trialStatus);
	var selectedTrialStatus="";
	var isPresent="";
	if(trialStatusOptions.find('option:[value='+statusTypeWithdrawn+']').text().length>0){
		withdrawnOption= trialStatusOptions.find('option:[value='+statusTypeWithdrawn+']').text();
	}
	if($('ctrpDraftJB.ctrpSubmType').options[$('ctrpDraftJB.ctrpSubmType').selectedIndex].value != submTypeU){
		var selIndxVal = $('ctrpDraftJB.trialStatus').options[$('ctrpDraftJB.trialStatus').selectedIndex].value;
		trialStatusOptions.find('option:[value='+statusTypeWithdrawn+']').remove();
		if(selIndxVal == statusTypeWithdrawn){
			$j("select#ctrpDraftJB\\.trialStatus option[value='']").attr("selected", "selected");// Bug#9011 27-Mar-2012 Ankit
		}
   	}else{
   		selectedTrialStatus =$('ctrpDraftJB.trialStatus').options[$('ctrpDraftJB.trialStatus').selectedIndex].value; //YK :Fixed Bug #8714 
   		isPresent = trialStatusOptions.find('option:[value="'+statusTypeWithdrawn+'"]').text(); //YK :Fixed Bug #8711 
   		if(isPresent.length<=0)
	 	{ 
   		    trialStatusOptions.find('option:[value=""]').remove();
		    trialStatusOptions.append('<option value="'+statusTypeWithdrawn+'">'+withdrawnOption+'</option>');
		    if(selectedTrialStatus==""){
		    trialStatusOptions.append('<option value="" selected ><%=LC.L_Select_AnOption%></option>');
		    }else{
		    trialStatusOptions.append('<option value="" ><%=LC.L_Select_AnOption%></option>');
		    }
	 	}
   }
}

function setAsteriskOnXMLReqd() {
	if (!$('XMLReqd_asterisk')) { return; }
	if (!$('ctrpDraftJB.ctrpSubmType')) { return; }
	$('XMLReqd_asterisk').style.visibility = 'hidden';
	for (var iX=0; iX<submTypeAorO.length; iX++) {
		if ($('ctrpDraftJB.ctrpSubmType').options[$('ctrpDraftJB.ctrpSubmType').selectedIndex].value
			== submTypeAorO[iX]) {
			$('XMLReqd_asterisk').style.visibility = 'visible';
		}
	}
}
function setAmendmentExtra() {
	if ($('ctrpDraftJB.ctrpSubmType').options[$('ctrpDraftJB.ctrpSubmType').selectedIndex].value
			== submTypeA) {
		$('DraftAmendmentExtra').style.visibility = 'visible';
		$('change_memo_Doc_asterisk').style.visibility = 'visible';
	} else {
		$('DraftAmendmentExtra').style.visibility = 'hidden';
		$('change_memo_Doc_asterisk').style.visibility = 'hidden';
	}
}
function setAsteriskOnNCIId() {
	if (!$('nci_id_asterisk')) { return; }
	if (!$('ctrpDraftJB.ctrpSubmType')) { return; }
	$('nci_id_asterisk').style.visibility = 'hidden';
	for (var iX=0; iX<submTypeAorU.length; iX++) {
		if ($('ctrpDraftJB.ctrpSubmType').options[$('ctrpDraftJB.ctrpSubmType').selectedIndex].value
			== submTypeAorU[iX]) {
			$('nci_id_asterisk').style.visibility = 'visible';
		}
	}
}
function setAsteriskOnLeadOrg() {
	if (!$('lead_org_id_asterisk')) { return; }
	if (!$('ctrpDraftJB.ctrpSubmType')) { return; }
	$('lead_org_id_asterisk').style.visibility = 'hidden';
	for (var iX=0; iX<submTypeAorO.length; iX++) {
		if ($('ctrpDraftJB.ctrpSubmType').options[$('ctrpDraftJB.ctrpSubmType').selectedIndex].value
			== submTypeAorO[iX]) {
			$('lead_org_id_asterisk').style.visibility = 'visible';
		}
	}
}

function setAsteriskOnProtocolIRBDoc() {
	if (!$('protocol_doc_asterisk')) { return; }
	if (!$('ctrpDraftJB.ctrpSubmType')) { return; }
	$('protocol_doc_asterisk').style.visibility = 'hidden';
	$('irb_App_Doc_asterisk').style.visibility = 'hidden';
	for (var iX=0; iX<submTypeAorO.length; iX++) {
		if ($('ctrpDraftJB.ctrpSubmType').options[$('ctrpDraftJB.ctrpSubmType').selectedIndex].value
			== submTypeAorO[iX]) {
			$('protocol_doc_asterisk').style.visibility = 'visible';
			$('irb_App_Doc_asterisk').style.visibility = 'visible';
		}
	}
}

</script>

<table>
	<tr>
		<td width="50%">
			<div>
			<table>
				<tr>
					<td align="right" width="30%">
						<%=LC.CTRP_DraftSectSubmType %>
						<A href="#" align="left" alt="" onmouseout="return nd();" 
						onmouseover="return overlib('<%=MC.CTRP_DraftSubmTypeHelpNonIndus%>',CAPTION,'<%=LC.CTRP_DraftSectSubmType%>',RIGHT , BELOW);">
						<img src="../images/jpg/help.jpg" border="0"></A>
						<span id="ctrpDraftJB.ctrpSubmType_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td valign="top" width="1%">
						<FONT id="draft_subm_type_asterisk" style="visibility:visible" class="Mandatory">*</FONT>
					</td>
					<td align="left" valign="top">
						<s:property escape="false" value="ctrpDraftJB.trialSubmTypeMenu" />
					</td>
				</tr>
				<tr>
					<td align="right">
						<%=LC.CTRP_DraftXMLRequired %>
						<span id="ctrpDraftJB.draftXmlRequired_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td valign="top" width="1%">
						<FONT id="XMLReqd_asterisk" style="visibility:hidden" class="Mandatory">*</FONT>
					</td>
					<td align="left">
						<s:radio name="ctrpDraftJB.draftXmlRequired" list="ctrpDraftJB.draftXmlReqList" 
							    listKey="radioCode" listValue="radioDisplay" value="ctrpDraftJB.draftXmlRequired" />
					</td>
				</tr>
				<tr><td colspan="3">&nbsp;</td></tr>
			</table>
			</div>
		</td>
		<td width="50%" align="left">
			<div id="DraftAmendmentExtra" style="visibility:hidden">
				<table>
					<tr>
						<td align="right" width="30%">
							<%=LC.CTRP_DraftAmendmentNumber %>
							<span id="ctrpDraftJB.amendNumber_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td valign="top" width="1%">
							<FONT class="Mandatory">* </FONT>
						</td>
						<td align="left" valign="top">
							<s:textfield name="ctrpDraftJB.amendNumber" id="ctrpDraftJB.amendNumber" style="width:25em" />
						</td>
					</tr>
					<tr>
						<td align="right"><%=LC.CTRP_DraftAmendmentDate %>
							<span id="ctrpDraftJB.amendDate_error" class="errorMessage" style="display:none;"></span>
						</td>
						<td valign="top">	
							<FONT class="Mandatory">* </FONT>
						</td>
						<td align="left" valign="top">
							<s:textfield name="ctrpDraftJB.amendDate" id="ctrpDraftJB.amendDate" 
									cssClass="datefield" />
						</td>
					</tr>
					<tr><td colspan="3">&nbsp;</td></tr>
				</table>
			</div>
		</td>
	</tr>
</table>

