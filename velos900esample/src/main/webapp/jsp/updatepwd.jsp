<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<Link Rel=STYLESHEET HREF="common.css" type=text/css>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"></jsp:include>
<BODY>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.Security,java.text.*"%>
<%

	int ret =-1;

	String userPass = "";
	String userESign = "";
	String confPass = "";
	String confESign = "";
	int daysExpPass = 0;
	int daysExpESign = 0;
	Date d = new Date();
	 
	String msg = "";

	String src = request.getParameter("src");
	String tab = request.getParameter("selectedTab");
	String eSign = request.getParameter("eSign");
	String from = request.getParameter("from");

	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String oldESign = (String) tSession.getValue("eSign");
	if((!oldESign.equals(eSign))&&(! (from.equals("pwdexpired")))) {
	%>
	<jsp:include page="incorrectesign.jsp" flush="true"/>
	<%
	} else {

	 int userId = EJBUtil.stringToNum((String) tSession.getValue("userId"));
		int pwdFlag=0;
		int eSignFlag=0;
		userB.setUserId(userId);
		userB.getUserDetails();

		String Password=userB.getUserPwd();
		String Esign=userB.getUserESign();

		// modified by gopu to fix the issues #1917 for trim leading and trailing spaces
		//userPass = request.getParameter("newPass").trim();
		//userESign = request.getParameter("newESign").trim();

		//Modified by Manimaran to fix the Bug #2532 to avoid the page gets struck.
		userPass = request.getParameter("newPass");
		userPass=(userPass==null)?"":userPass;
		/*Added For Bug#10355 Date:3 July 2012 By : Yogendra Pratap*/
		confPass = request.getParameter("confPass");
		confPass=(confPass==null)?"":confPass;
 		userESign = request.getParameter("newESign");
 		confESign = request.getParameter("confESign");

		// modified by gopu to fix the issues #1917 for trim leading and trailing spaces
		//String userPassword=request.getParameter("oldPass").trim();
		//String userEsign=request.getParameter("oldESign").trim();

		//Modified by Manimaran to fix the Bug #2532 to avoid the page gets struck.
		String userPassword=request.getParameter("oldPass");

		userPassword=(userPassword==null)?"":userPassword;

		String userEsign=request.getParameter("oldESign");


		//Encrypt Passwords
		userPass=Security.encryptSHA(userPass);
		userPassword=Security.encryptSHA(userPassword);
		/*Added For Bug#10355 Date:3 July 2012 By : Yogendra Pratap*/
		confPass=Security.encryptSHA(confPass);



		String checkPass = request.getParameter("checkPass");
		String checkESign = request.getParameter("checkESign");

		if(checkPass == null) checkPass = "off";
		if(checkESign == null) checkESign = "off";

		daysExpPass = EJBUtil.stringToNum(request.getParameter("daysExp"));
		daysExpESign = EJBUtil.stringToNum(request.getParameter("daysExpESign"));

		if( from.equals("changepwd") || (checkPass.equals("on") && from.equals("pwdexpired"))) {
	    	d = new Date();
			if(daysExpPass == 0) {
				//Added For Bug#10355 Date:3 July 2012 By : Yogendra Pratap
				pwdFlag=1;
				//userB.setUserPwdExpiryDate(DateUtil.getFormattedDateString("9999","01","01"));

				//userB.setUserPwdExpiryDays("0");

		   		 
		   		//userB.setUserPwdReminderDate(DateUtil.getFormattedDateString("9999","01","01"));
			} else {
				d.setDate(d.getDate() + daysExpPass);
				//userB.setUserPwdExpiryDate(sdf.format(d));
				userB.setUserPwdExpiryDate(DateUtil.dateToString(d));
				userB.setUserPwdExpiryDays((new Integer(daysExpPass)).toString());

	 			 
	 			userB.setUserPwdReminderDate(DateUtil.getFormattedDateString("9999","01","01"));

			}
		}

		if( from.equals("changepwd") || (checkESign.equals("on") && from.equals("pwdexpired"))) {
			d = new Date();
    		if(daysExpESign == 0) {
    			//Added For Bug#10355 Date:3 July 2012 By : Yogendra Pratap
				eSignFlag=1;
    			//userB.setUserESignExpiryDate(DateUtil.getFormattedDateString("9999","01","01"));

    			//userB.setUserESignExpiryDays("0");

    		   	 
    		   	//userB.setUserESignReminderDate(DateUtil.getFormattedDateString("9999","01","01"));
    		} else {
    			d.setDate(d.getDate() + daysExpESign);
    		 	//userB.setUserESignExpiryDate(sdf.format(d));
    		 	userB.setUserESignExpiryDate(DateUtil.dateToString(d));
    		 	userB.setUserESignExpiryDays((new Integer(daysExpESign)).toString());

    		 	 
    		 	userB.setUserESignReminderDate(DateUtil.getFormattedDateString("9999","01","01"));
    		}
		}

		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = null;
		usr = (String) tSession.getValue("userId");

		userB.setModifiedBy(usr);
		userB.setIpAdd(ipAdd);


		if( ((checkPass.equals("on"))) && !(Password.equals((userPassword))) )
		{
		%>
		<br><br><br><br><br><br>
           <p class = "successfulmsg" align = center>  <%=MC.M_Etr_CorrOldPwd %><%-- Please enter correct old password.*****--%>
		</p>

		<table width="100%">

		<tr>
		   <td align=center>
				<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
			</td>
		</tr>
		</table>
	<%
	} else if( ((checkPass.equals("on"))) && (Password.equals(userPass)) )
		{
		%>
		<br><br><br><br><br><br>
           <p class = "successfulmsg" align = center>  <%=MC.M_NewPwdCnt_SameOldPwd %><%-- New password can not be same as old password. Please correct to continue.*****--%>
		</p>

		<table width="100%">

		<tr>
		   <td align=center>

		 <%if (from.equals("changepwd")){%>
		<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
		<%} else {%>
		<A type="submit" href="./pwdexpired.jsp"><%=LC.L_Back%></A>
		<%}%>

		</td>
		</tr>
		</table>

		<%
		} else if ( ((checkPass.equals("on"))) && (userPass.equals(Esign)) )
		{ %>
		<br><br><br><br><br><br>
           <p class = "successfulmsg" align = center> <%=MC.M_NewPwdSame_EsignPlsCrt %><%-- New Password cannot be same as e-signature. Please correct to continue.*****--%>		</p>

		<table width="100%">

		<tr>
		   <td align=center>
		 <%if (from.equals("changepwd")){%>
		<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
		<%} else {%>
		<A type="submit" href="./pwdexpired.jsp"><%=LC.L_Back%></A>
		<%}%>

		</tr>
		</table>


		<%}
		else if ( ((checkESign.equals("on"))) && !(userEsign.equals(Esign)) )
		{%>
		<BR><BR><BR><BR><BR>
	      <p class = "successfulmsg" align = center>  <%=MC.M_Etr_CorrOldEsign %><%-- Please enter correct old esign.*****--%>	     </p>

		<table width="100%">

		<tr>
		   <td align=center>
		<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>

		</td>
		</tr>
		</table>

		<%}
		else if ( ((checkESign.equals("on"))) && (userESign.equals(Password)) )
		{%>
		<BR><BR><BR><BR><BR>
	      <p class = "successfulmsg" align = center> <%=MC.M_NewEsignCnt_SamePwd %><%-- New e-signature cannot be same as Password. Please correct to continue.*****--%>	     </p>

		<table width="100%">

		<tr>
		   <td align=center>
		<%if (from.equals("changepwd")){%>
		<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
		<%} else {%>
		<A type="submit" href="./pwdexpired.jsp"><%=LC.L_Back%></A>
		<%}%>

		</td>
		</tr>
		</table>
		<%--Added For Bug#10355 Date:3 July 2012 By : Yogendra Pratap--%>
		<%}else if( (checkPass.equals("on")) && !confPass.equalsIgnoreCase(userPass)){%>
			<br><br><br><br><br><br>
	        <p class = "successfulmsg" align = center>  <%=MC.M_NewConfirm_PwdSame %><%-- Values in 'New Password' and 'Confirm Password' are not same. Please enter again.*****--%>
			</p>
	
			<table width="100%">
	
			<tr>
			   <td align=center>
					<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
				</td>
			</tr>
			</table>
		<%}else if( (checkESign.equals("on")) &&  !confESign.equalsIgnoreCase(userESign)){%>
		<br><br><br><br><br><br>
			        <p class = "successfulmsg" align = center>  <%=MC.M_NewConfirm_EsignSame %><%-- Values in 'New e-Signature' and 'Confirm e-Signature' are not same. Please enter again.*****--%>
			</p>
			
			<table width="100%">
			
			<tr>
			   <td align=center>
					<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
				</td>
			</tr>
			</table>
			
			
		<%}else if(eSignFlag==1 || pwdFlag==1){%>
			
			<br><br><br><br><br><br>
			        <p class = "successfulmsg" align = center>  <%=MC.M_InvalidExp_PlsEtrAgn %><%-- Invalid Expiration days.Please enter again.*****--%>
			</p>
			
			<table width="100%">
			
			<tr>
			   <td align=center>
					<button onclick="window.history.back(); return false;"><%=LC.L_Back%></button>
				</td>
			</tr>
			</table>
		<%}
		else {



		if(checkPass.equals("on")) {

   		   userB.setUserPwd(userPass);
		}
		if(checkESign.equals("on")) {

   		   userB.setUserESign(userESign);
		}

		ret = userB.updateUser();
		userB.setUserId(userId);

		userB.getUserDetails();
		tSession.putValue("eSign", userB.getUserESign() );

		if (ret==0) {

			msg=MC.M_Data_SavedSucc;/* msg="Data Saved Successfully";*****/

			tSession.putValue("currentUser", userB);

		}

	else {

		msg=MC.M_Data_NotSaved;/* msg="Data Not Saved";*****/

	}

%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%= msg%></p>
<% if((checkPass.equals("on")) && (checkESign.equals("on")) ){ %>
<p class = "successfulmsg" align = center>  <%=MC.M_NewPwd_ApplicabOnRelogin %><%-- Your new Password will be applicable on Relogin. Your new e-Signature will be applicable immediately.*****--%>  </p>
<%} else if(checkPass.equals("on")) {%>
<p class = "successfulmsg" align = center>  <%=MC.M_NewPwdApplicab_OnRelogin %><%-- Your new Password will be applicable on Relogin.*****--%> </p>
<%} else if(checkESign.equals("on")) {%>
<p class = "successfulmsg" align = center>  <%=MC.M_NewEsignApplicab_Imdt %><%-- Your new e-Signature will be applicable immediately.*****--%>  </p>
<%}%>


<%
   if(from.equals("pwdexpired")) {
%>

<META HTTP-EQUIV=Refresh CONTENT="3; URL=myHome.jsp?srcmenu=tdMenuBarItem1">

<%
   } else {
%>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=changepwd.jsp?srcmenu=<%=src%>&selectedTab=<%=tab%>">
<%
}
}// end of if for password
}//end of if for esign

}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</BODY>
</HTML>
