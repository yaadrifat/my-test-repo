<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="page" class="com.velos.eres.web.user.UserJB"/><!--km-->

<jsp:useBean id="submissionProvisoB" scope="request" class="com.velos.eres.web.submission.SubmissionProvisoJB"/>

<jsp:include page="include.jsp" flush="true"/> 


<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,java.util.*,com.velos.eres.service.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,com.velos.eres.service.util.StringUtil,java.sql.*,com.velos.eres.business.common.*"%>  

<%

HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession)) {

	String defUserGroup = (String) tSession.getAttribute("defUserGroup");
	String submissionPK = request.getParameter("submissionPK");
	String submissionBoardPK = request.getParameter("submissionBoardPK");

	String tabsubtype = request.getParameter("tabsubtype");
	
	String userId ="";
	String mode = request.getParameter("mode");
    String provisoDate = "";
    CodeDao cdProvisoType = new CodeDao();   
    String provisoType = "";
    
    cdProvisoType.getCodeValues("prov_type");
	cdProvisoType.setCType("prov_type");
    cdProvisoType.setForGroup(defUserGroup); 
	
						
    if (StringUtil.isEmpty(mode))
    {
    	mode = "M";
    }
    
	String provisoId = "";
	String enteredBy = "";
    String enteredByName = "";
    String proviso = "";
    String ddProvisoType="";
    
	if (mode.equals("M"))
	{
			provisoId = request.getParameter("provisoId");
		
			submissionProvisoB.setId(EJBUtil.stringToNum(provisoId));
			submissionProvisoB.getSubmissionProvisoDetails();
	
	

			submissionPK = submissionProvisoB.getFkSubmission();
			submissionBoardPK = submissionProvisoB.getFkSubmissionBoard();
            
            provisoDate = submissionProvisoB.getSubmissionProvisoDate();
            
            enteredBy = submissionProvisoB.getProvisoEnteredBy();
            
            userB.setUserId(EJBUtil.stringToNum(enteredBy));
			userB.getUserDetails();
	
			enteredByName = userB.getUserFirstName() + " " +userB.getUserLastName();

             
			
			proviso = submissionProvisoB.getSubmissionProviso();
			provisoType = submissionProvisoB.getProvisoType();
	}	else
		{
			provisoDate = DateUtil.getCurrentDate();
		}
    
    userId = (String) tSession.getValue("userId");
    
    
    
	ddProvisoType = cdProvisoType.toPullDown("ddProvisoType",EJBUtil.stringToNum(provisoType));


	//Added by Manimaran to give access right for default admin group to the delete link 
	int usrId = EJBUtil.stringToNum(userId);
	
	if (StringUtil.isEmpty(enteredBy))
	{
		enteredBy = userId;
		userB.setUserId(usrId);
		userB.getUserDetails();
	
		enteredByName = userB.getUserFirstName() + " " +userB.getUserLastName();

	}
	
	 
	
	%>
		
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT>
if (document.getElementById('eSign')) { document.getElementById('eSign').disabled=false; } 
if (document.getElementById('submit_btn')) { document.getElementById('submit_btn').disabled=false; }

function  blockSubmit() {
	 setTimeout('document.getElementById(\'eSign\').disabled=true', 10);
	 setTimeout('document.getElementById(\'submit_btn\').disabled=true', 10);
}
function  validate(formobj){
	
	 if (!(validate_col('Proviso Date',formobj.provDate))) return false	;
	 if (!(validate_col('Proviso Type',formobj.ddProvisoType))) return false	;
	 if (!(validate_col('Proviso',formobj.proviso))) return false	;	 
  	if (!(validate_col('e-Signature',formobj.eSign))) return false
  	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
  		return false;
   	}
}
</SCRIPT>
    		<div class="tableDefault">
    			<p class="sectionHeadings"><%=LC.L_Proviso_Details%><%--Proviso Details*****--%></p>
    				<form id = "addprovForm" name="addprov" action="updateProviso.jsp" METHOD="POST"
                    onsubmit="if (validate(document.addprovForm)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); blockSubmit(); return true; }">
	    			<table width="100%">
	    			<tr>
					
						<td>
							<%=LC.L_Date_Entered%><%--Date Entered *****--%><FONT class="Mandatory">* </FONT>
							</td>
<%-- INF-20084 Datepicker-- AGodara --%>			
		    			<td><input type="text" id="provDate" name="provDate" class="datefield" size = 20 MAXLENGTH = 20 value="<%=provisoDate%>" READONLY></td>
						<td>
							<%=LC.L_Proviso_Type%><%--Proviso Type*****--%><FONT class="Mandatory">* </FONT>
							</td>    			
		    			<td > 
			
							<%=ddProvisoType %>
						</td>		
	    				
						<td>
	    					<%=LC.L_Entered_By%><%--Entered By*****--%>
	    				</td>
	    				<td>
	    					 <input type=hidden name="enteredBy" value='<%=enteredBy%>'> 
							 <input type=text name="enteredByName" value="<%=enteredByName%>" readonly > 

			  				  <A HREF=# onClick='openCommonUserSearchWindow("addprov","enteredBy","enteredByName")' ><%=LC.L_Select_User%><%--SELECT USER*****--%></A>
			   
			
	    				</td>
	    				</tr>
	    				
	    			<tr>
	    					<td>
	    					<%=LC.L_Proviso%><%--Proviso*****--%> <FONT class="Mandatory" >* </FONT>
	    				</td>
	    				<td colspan=5>
	    					<textarea cols=100 rows = 2 name="proviso"><%=proviso%></textarea>
	    				</td>
	    				
	    				</tr>
	    				</table>
	    			
					<jsp:include page="submitBar.jsp" flush="true"> 
							<jsp:param name="displayESign" value="Y"/>
							<jsp:param name="formID" value="addprovisos"/>
							<jsp:param name="showDiscard" value="N"/>
							<jsp:param name="showSubmit" value="Y"/>
					</jsp:include>

	
					<input type="hidden" name="submissionPK" id="submissionPK" value="<%=submissionPK%>">
				    <input type="hidden" name="submissionBoardPK" id="submissionBoardPK" value="<%=submissionBoardPK%>">
				 	<input type="hidden" name="provisoId" id="provisoId" value="<%=provisoId%>">
				    <input type="hidden" name="mode" id="mode" value="<%=mode%>">
				    
    	    			</form>
    	    	</div>
    
    						
    						<% } %>
    							
    			<div> 
    
    <jsp:include page="bottompanel.jsp" flush="true"/> 
</div>				