<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Link_Form%><%--Link Form*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>



<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


<SCRIPT Language="javascript">

 function  validate(formobj)
 {
 	checkQuote="N";
 	if (   !(validate_col ('  Section', formobj.section)  )  )  return false
	if (!(validate_col('Sequence',formobj.sequence))) return false		
 	if (   !(validate_col ('Field Name', formobj.fldname)  )  )  return false
 	if (   !(validate_col ('Select Form Name', formobj.linkFormId)  )  )  return false
 	if (! (checkquote(formobj.instructions.value)))
	{
		  formobj.instructions.focus();
		  return false;
	}
	
	 if(!( validateDataSize(formobj.instructions,1000,'Field Help'))) return false
	
     if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false
	 
     if (! (splcharcheck(formobj.instructions.value)))
	{
		  formobj.instructions.focus();
		  return false;
	}
     if (! (splcharcheckForXSL(formobj.instructions.value)))
	{
		  formobj.instructions.focus();
		  return false;
	}
	

	if(isNaN(formobj.eSign.value) == true) 
	{
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		 return false;
   	}
	
	if(isNaN(formobj.sequence.value) == true) 
	{
		alert("<%=MC.M_SecSeqNum_EtrAgn%>");/*alert("Section Sequence has to be a number. Please enter again");*****/
		formobj.sequence.focus();
		 return false;
   	}
	
	
  }
  
////////////////////////////////////////////////////
function openLookup(formobj){
		var accId=formobj.accId.value;
		var userId=formobj.userId.value;
		var type=formobj.formDisplayType.value;
		var linkedStudy = formobj.linkedStudy.value;
			
		var filter="";
		var typeFilter = "";
		var studyFilter = "";
		
		//studyflg=formobj.studyselect[0].checked;
		if (type=="SA")
		{
			typeFilter = "'SA'";
		} else	if (type=="S")
		{
			typeFilter = "'S','SA'";
			studyFilter = "  and (b.fk_study is null or b.fk_study = "+ linkedStudy +")  ";
		} else if (type=="A")
		{
			typeFilter = "'A'"
		} else if (type=="PA")
		{
			typeFilter = "'PA'"
		}else if (type=="SP")
		{
			typeFilter = "'SP','PS','PR','PA'"
			studyFilter = "  and (b.fk_study is null or b.fk_study = "+ linkedStudy +")  ";
		}
		else if (type=="PS")
		{
			typeFilter = "'PA','PS','PR'"
		}
		else if (type=="PR")
		{
			typeFilter = "'PA','PS','PR'"
		}
		
		filter = "linkFormType|"+type+"|"+(linkedStudy==null?"":linkedStudy);
		 
		//******************************endhere**************************************************//
			 
		formobj.dfilter.value=filter;
		formobj.target='formLkp';
		formobj.action="multilookup.jsp?viewId=&viewName=Form Browser&maxselect=1&form=linkFormType&seperator=;&frameMode=TB&keyword=linkFormName|VELFORMNAME~linkFormId|VELFORMPK|[VELHIDE]&filter=[VELGET:dfilter]";
		
		windowname=window.open('donotdelete.html' ,'formLkp',"toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=800,height=600 top=100,left=100 0, ");
		formobj.submit();
		formobj.action="linkFormTypeSubmit.jsp";
		formobj.target="";
		windowname.focus();	


}
	
	
/////////////////////////////////////////////////// 
function openPreviewWin(formobj)
{
	var formId='';
	formId = formobj.linkFormId.value;
	
	if (formId.length <= 0 || formId == '')
	{
		alert("<%=MC.M_Selc_Frm%>");/*alert("Please select a Form");*****/
		return;
	}
	param1="formpreview.jsp?formId="+formId;
	
	windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=825,height=600 ,top=90 ,left=150");
	windowName.focus();
}
 


</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="formSecJB" scope="request"  class="com.velos.eres.web.formSec.FormSecJB"/>
<jsp:useBean id="formFieldJB" scope="request"  class="com.velos.eres.web.formField.FormFieldJB"/>
<jsp:useBean id="LinkedFormsJB" scope="request"  class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="FrmLibJB" scope="request"  class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:include page="include.jsp" flush="true"/>
	
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%>

<body>
<DIV class="popDefault" id="div1"> 
 <%

	HttpSession tSession = request.getSession(true);
	int pageRight = 0;	

   if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	 GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
	 String calledFrom=request.getParameter("calledFrom");
	 String lnkFrom=request.getParameter("lnkFrom");
	 if(lnkFrom==null) 
		{lnkFrom="-";}
	 
	 String stdId=request.getParameter("studyId");
	 int studyId= EJBUtil.stringToNum(stdId);			

		String userIdFromSession = (String) tSession.getValue("userId");
		String accId = (String) tSession.getValue("accountId");
	
		
		if( calledFrom.equals("A")){ //from account form management
	 	    if (lnkFrom.equals("S")) 
	 	      {   
		 		 ArrayList tId ;
				 tId = new ArrayList();
				 if(studyId >0)
				 {
				 	TeamDao teamDao = new TeamDao();
					teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
					tId = teamDao.getTeamIds();
					if (tId.size() <=0)
					 {
						pageRight  = 0;
					  } else 
					  {
					  	StudyRightsJB stdRights = new StudyRightsJB();
						stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
						 
						ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();
							 
						stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRights.loadStudyRights();
					 		
						if ((stdRights.getFtrRights().size()) == 0)
						{					
						  pageRight= 0;		
						} else 
						{								
							pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
						}
					  }
			    }
			 }	
    	 	else
    	    {		 
    	  		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
    		 }
	  	} 
		
	 	if( calledFrom.equals("L")) //form library
	 	{
			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
	 	}
		
		if( calledFrom.equals("St")) //study
	 	{
			StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	
	  		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
		}
		 

	if (pageRight >= 4)
		
	{
	
		String mode=request.getParameter("mode");
		
		String fieldLibId="";
		
		String fldInstructions="";
		
		// THE STYLE OBJECT WHICH TAKES VALUES OF ATTRIBUTES OF THE FIELD  
		// LIKE BOLD, SAMELINE, ALIGN , ITALICS
		Style aStyle= new Style();
		String formId="";
		String formSecId="";
		String fldSeq="";
		String fldAlign="";
		String samLinePrevFld="";
		String fldBold="";
		String fldItalics="";
		String formFldId="";
		String fldType="";
		String codeStatus="";
		String fldIsVisible="";
		String offlineFlag = "0";
		formId=request.getParameter("formId");		
		String fldName = "";
		String accountId=(String)tSession.getAttribute("accountId");
		String linkedFormIdStr = "";
		String linkedFormName = "";
		
		int tempAccountId=EJBUtil.stringToNum(accountId);
		

		
		ArrayList idSec= new ArrayList();
		ArrayList descSec= new ArrayList();
		
		String formDisplayType  = "";
		String linkedStudy = "";
				
		String pullDownSection;
		FormSecDao formSecDao= new FormSecDao();
		formSecDao = formSecJB.getAllSectionsOfAForm( EJBUtil.stringToNum(formId));
		idSec= formSecDao.getFormSecId();
		descSec=formSecDao.getFormSecName();
		
		pullDownSection=EJBUtil.createPullDown("section", 0, idSec, descSec);
		
		
		//get linked form information
			
		 LinkedFormsJB.findByFormId(EJBUtil.stringToNum(formId));
		
		 formDisplayType = LinkedFormsJB.getLFDisplayType();
		 linkedStudy = LinkedFormsJB.getStudyId();
						
		if (mode.equals("M"))
		{
			
				formId=request.getParameter("formId");		
				formFldId=request.getParameter("formFldId");
				codeStatus = request.getParameter("codeStatus");
				
				
				formFieldJB.setFormFieldId(Integer.parseInt(formFldId) );
				formFieldJB.getFormFieldDetails();
				fieldLibId=formFieldJB.getFieldId();
				offlineFlag = formFieldJB.getOfflineFlag();
				
				
				fieldLibJB.setFieldLibId(Integer.parseInt(fieldLibId));
				fieldLibJB.getFieldLibDetails();
			
				formSecId=formFieldJB.getFormSecId();  
				fldIsVisible = fieldLibJB.getFldIsVisible();
				
				
				int secId =EJBUtil.stringToNum(formSecId) ;
				
				formSecJB.setFormSecId(secId);
				formSecJB.getFormSecDetails();	
				
				String sectionName = "";
				sectionName = formSecJB.getFormSecName();
							
				
				// TO MAKE A READONLY BOX FOR NOT LETTING THE EDITING OF THE SECTION FOR FIELD IN EDIT MODE 
				
				pullDownSection = "<label>"+sectionName +"</label>" 
				 							+ "<Input type=\"hidden\" name=\"section\" value="+ formSecId +" >  " ;
				
				fldSeq=((formFieldJB.getFormFldSeq()) == null)?"":(formFieldJB.getFormFldSeq()) ;
				
						
				//  WHILE GETTING THE STYLE WE CHECK ITS VALUES TO DISPLAY APPROPRIATELY
				aStyle=fieldLibJB.getAStyle( ); 
				if ( aStyle != null )  
				{
						samLinePrevFld = (  (   aStyle.getSameLine()   ) == null)?"-1":(   aStyle.getSameLine()   ) ;	
					}
			
				
			
				
				fldType=fieldLibJB.getFldType();
				
				fldInstructions = ((fieldLibJB.getFldInstructions()) == null)?"":(fieldLibJB.getFldInstructions()) ;		
				
				fldName = fieldLibJB.getFldName();
				
				linkedFormIdStr = fieldLibJB.getFldLinkedForm();
				
				if (! StringUtil.isEmpty(linkedFormIdStr))	
				{
					FrmLibJB.setFormLibId(EJBUtil.stringToNum(linkedFormIdStr));
					FrmLibJB.getFormLibDetails();
					linkedFormName = FrmLibJB.getFormLibName();
				}		
				
							
				
				
		}
		

		if(((pageRight == 6 && mode.equals("M") ) || (pageRight == 5 && mode.equals("N")) || pageRight == 7) && (mode.equals("M") && (!codeStatus.equals("Lockdown") && !codeStatus.equals("Deactivated")) )|| mode.equals("N")){%>
		<Form name="linkFormType" method="post" id="linkFrm" action="linkFormTypeSubmit.jsp" onsubmit="if (validate(document.linkFormType)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
			<%}else{%>
		<Form name="linkFormType" method="post" onsubmit="return false">
			<%}%>

	<table width="60%" cellspacing="0" cellpadding="0" border="0">
  	<tr>
		<td ><P class="sectionHeadings"> <%=MC.M_FldTyp_LnkFrm_Upper%><%--FIELD TYPE:  LINK FORM*****--%></P> </td>
		
	 </tr>   
	 </table>
	 <br>
 
	
	<Input type="hidden" name="mode" value=<%=mode%> >
	<Input type="hidden" name="formId" value=<%=formId%> >  
	<Input type="hidden" name="formFldId" value=<%=formFldId%> >  
	<Input type="hidden" name="formDisplayType" value=<%=formDisplayType%>>  
	<Input type="hidden" name="linkedStudy" value=<%=linkedStudy%>>  
	<input type=hidden name="accId" value="<%=accId%>">
	<input type=hidden name="userId" value="<%=userIdFromSession%>">
	<input type="hidden" name="dfilter" readonly value="">
	<input type="hidden" name="linkFormId" readonly value="<%=linkedFormIdStr%>">

		 
	<table width="80%" cellspacing="0" cellpadding="0" border="0"  >
		<tr> 
      <td width="20%"><%=LC.L_Section%><%--Section*****--%><FONT class="Mandatory">* </FONT></td>
        <td width="60%"><%=pullDownSection%> </td>
	  </tr>
	<tr> 
      <td width="20%"><%=LC.L_Sequence%><%--Sequence*****--%><FONT class="Mandatory">* </FONT></td>
        <td width="60%"><input type="text" name="sequence" size = 10 MAXLENGTH = 10 value="<%=fldSeq%>"> </td>
		</tr>
		</table>
			
	<table width="80%" cellspacing="0" cellpadding="0" border="0"  align="" >
		<tr>
			<td width="20%">
			<%=LC.L_Fld_Name%><%--Field Name*****--%> <FONT class="Mandatory">* </FONT>
			</td>
			<td width="60%">
			<input type="text" name="fldname" size = 40 MAXLENGTH = 50 value="<%=fldName%>"> 
			</td>
			</tr>
		<tr><td ><label><%=MC.M_FldHelp_MouseOver%><%--Field Help <I> (Displayed on Mouse Over)</I>*****--%></label></td>
		  	<td width="60%">
			<textarea name="instructions" cols="40" rows="3"  MAXLENGTH=500 ><%=fldInstructions%></textarea>
			</td>
		</tr>
		</table>
		<BR>
	<table  width="48%" cellspacing="0" cellpadding="0" border="0" >
		<%//Commented as per UCSF requirements*/%>
	<%//Now uncommented as per 19th April enhancements of %>
		<%if (  ( samLinePrevFld.equals("-1")  ) || ( samLinePrevFld.equals("0" ) ) ||  (samLinePrevFld.equals("") ) )
		   {%>
			<tr><td width="25%">	<input type="checkbox" name="sameLine" value="1" ><%=MC.M_SameLine_AsPrevFld%><%--Same line as previous field*****--%><br></td></tr>
		 <%}%>
		 <% if (   samLinePrevFld.equals("1")  ) 
		   {%>
			<tr><td width="25%">	<input type="checkbox" name="sameLine" value="1" CHECKED><%=MC.M_SameLine_AsPrevFld%><%--Same line as previous field*****--%><br></td></tr>
		 <%}%>
	</table>
	<table width="100%">
    <tr><td width="15%"><%=LC.L_Select_FormName%><%--Select Form Name*****--%><FONT class="Mandatory">*</FONT></td><td width="50%"><input type="text" name="linkFormName" readonly value="<%=linkedFormName%>" size="35" >
        <A href="#" onClick="openLookup(document.linkFormType)"><%=LC.L_Select%><%--Select*****--%></A>
     </td><td><A href="#" onClick="openPreviewWin(document.linkFormType)"><%=LC.L_Preview%><%--Preview*****--%></A></td></tr>
    </table>	
    <table width="70%">
    <tr>
    <td><p class="redMessage"><%=MC.M_FrmEmbedded_PrevChild%><%--If the form being linked has embedded linked forms within it, those links will not be active at the point of data entry.<br>Only one level of parent-child form linking is allowed.<br> You can click on the ''Preview''link to check if the selected form has any child form links*****--%></p>
		</td></tr></table>
		
	<br> <br>
	<%//out.println("codeStatus"+codeStatus);%>
	<br>
	<%
		if(((pageRight == 6 && mode.equals("M") ) || (pageRight == 5 && mode.equals("N")) || pageRight == 7) && (mode.equals("M") && (!codeStatus.equals("Lockdown") && !codeStatus.equals("Deactivated")) )|| mode.equals("N")){
		
		if( (calledFrom.equals("St") || calledFrom.equals("A")) && codeStatus.equals("Active")) {
			;
		}else{%>
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="linkFrm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
		<%}
	}%>
     <br>

 	 <table width="100%" cellspacing="0" cellpadding="0">
      <td align=right> 

      </td> 
      </tr>
  </table>
  </Form>
  
<%

	} //end of if body for page right
	else
	{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%

	} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>
 <div class = "myHomebottomPanel"> 
 <jsp:include page="bottompanel.jsp" flush="true"/>
 </div>
</body>

</html>
