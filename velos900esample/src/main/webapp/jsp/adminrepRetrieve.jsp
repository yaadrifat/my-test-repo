<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<TITLE><%=LC.L_Rpt_Output%><%--Report Output*****--%></TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</HEAD>

<body>
<jsp:useBean id="repB" scope="request" class="com.velos.eres.web.report.ReportJBNew"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,javax.xml.transform.TransformerFactory,javax.xml.transform.Transformer,javax.xml.transform.Source,javax.xml.transform.stream.StreamSource,javax.xml.transform.stream.StreamResult,javax.xml.transform.TransformerException,javax.xml.transform.TransformerConfigurationException,java.io.*,java.text.*,java.net.URL,com.velos.eres.web.user.UserJB"%>


<%
	
	String repDate="";
	String repTitle="";

	String repName=request.getParameter("repName");
	String format = "";
	String repArgsDisplay = "";
%>
<%--	
	String dtFrom=request.getParameter("dateFrom");
	String dtTo=request.getParameter("dateTo");
	

	
	String repArgs = "";

	String monthString = null;
	String filterType = request.getParameter("filterType");
	
	String month = request.getParameter("month");
	String year = null;

	
	if (filterType == null) {
		filterType = request.getParameter("filterType2"); ///for account reports 
	}
	
	if(filterType == null) filterType = "";	
	if(filterType.equals("2")) { //Year
		year = request.getParameter("year");
		dtFrom = "01/01/"+year;
		dtTo = "12/31/"+year;
	}

	if(filterType.equals("3")) { //Month
		year = request.getParameter("year1");
		dtFrom = month+"/01/"+year;
		String lastDate="";
		switch(EJBUtil.stringToNum(month)) {
			case(1):
				monthString = "January";
				lastDate="31";
				break;
			case(2):
				monthString = "February";
				int yrNum=0;
				yrNum = EJBUtil.stringToNum(year);
				if (((yrNum) % (400)) == 0) {
					lastDate="29";
				}else {
					lastDate="28";
				}
				break;
			case(3):
				monthString = "March";
				lastDate="31";
				break;
			case(4):
				monthString = "April";
				lastDate="30";
				break;
			case(5):
				monthString = "May";
				lastDate="31";
				break;
			case(6):
				monthString = "June";
				lastDate="30";
				break;
			case(7):
				monthString = "July";
				lastDate="31";
				break;
			case(8):
				monthString = "August";
				lastDate="31";
				break;
			case(9):
				monthString = "September";
				lastDate="30";
				break;
			case(10):
				monthString = "October";
				lastDate="31";
				break;	
			case(11):
				monthString = "November";
				lastDate="31";
				break;
			case(12):
				monthString = "December";
				lastDate="31";
				break;
		}
		dtTo = month+"/"+lastDate+"/"+year;
	}

	if(filterType.equals("1")) { //All Dates	
		dtFrom = "01/01/1900";
		dtTo = "01/01/3000";
	}
	
	if(dtFrom == null) dtFrom = "";
	dtFrom = dtFrom.trim();			
	if (!(dtFrom.equals(""))) {
		//converting dates to the required format

		format="MM/dd/yyyy";
		SimpleDateFormat dformat=new SimpleDateFormat(format);

		Calendar cal=  Calendar.getInstance();

		
		cal.setTime(dformat.parse(dtFrom));
		
		format="dd-MMM-yyyy";
		SimpleDateFormat FromFormat=new SimpleDateFormat(format);
		String dateFrom = FromFormat.format(cal.getTime());	


		String dateTo="";

		cal.setTime(dformat.parse(dtTo));
		format="dd-MMM-yyyy";
		SimpleDateFormat ToFormat=new SimpleDateFormat(format);
		dateTo = ToFormat.format(cal.getTime());
	
		if(filterType.equals("1")) { //All Dates	
			repArgsDisplay = "";	
		}
		else {
			repArgsDisplay = dtFrom + " To " + dtTo ;			
		}

		repArgs= dateFrom + "," + dateTo;
	
	}
--%>
<%
	int repId = EJBUtil.stringToNum(request.getParameter("repId"));
	

//out.println(repId);
	int repXslId = 0;

//	String dispStyle=request.getParameter("dispStyle");	

	String params="";	

	String hdr_file_name="";
	String ftr_file_name="";
	String filePath="";
	String hdrfile="";
	String ftrfile="";	
	String hdrFilePath="";
	String ftrFilePath="";

	switch (repId) {
	
		case 63: //Account Report (Detailed) 
			params = "" ;
			repXslId = repId;
			break;

		case 64: //Account Report (Summary)
			params = "" ;
			repXslId = repId;
			break;			
			
		default:out.println("default");	
	}
	
	Calendar now = Calendar.getInstance();
	repDate=""+now.get(now.DAY_OF_MONTH) + now.get(now.MONTH) + (now.get(now.YEAR) - 1900);

	Calendar calToday=  Calendar.getInstance();
	format="MM/dd/yyyy";

	SimpleDateFormat todayFormat=new SimpleDateFormat(format);
	repDate = todayFormat.format(calToday.getTime());

	ReportDaoNew rD =new ReportDaoNew();
	
	rD=repB.getRepXml(repId,0,params);
	
	ArrayList repXml = rD.getRepXml();
	
//out.println(repXslId);	 
		
	rD=repB.getRepXsl(repXslId);
	ArrayList repXsl = rD.getXsls();
		
	Object temp;
	String xml=null;
	String xsl=null;
	

	temp=repXml.get(0);				
	
	if (!(temp == null)) 
	{
		xml=temp.toString();
	}
	else
	{

		out.println(MC.M_Err_GettingXml/*"error in getting xml"*****/);
		return;
	}

	temp=repXsl.get(0);
				
	if (!(temp == null))
	{
		xsl=temp.toString();
		}
	else
	{
		out.println(MC.M_Err_GettingXsl/*"error in getting xsl"*****/);
		return;
	}
	
//	out.println(xml.length());
	if ((xml.length())==34) { //no data found
%>
		<br><br><br><br><br><br><br><br><br>
		<P class="sectionHeadings">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%Object[] arguments = {repName}; %> <%=VelosResourceBundle.getMessageString("M_NoDataFnd_Rpt",arguments)%><%--No data found for the Report '<%=repName%>'.*****--%> </P>
<%		return;
	}

	
	//get hdr and ftr
	//Changed By Deepali
	rD=repB.getRepHdFtr(repId,0,0);
	
	byte[] hdrByteArray=rD.getHdrFile();
	byte[] ftrByteArray=rD.getFtrFile();
	String hdrflag, ftrflag;	
	hdr_file_name="temph["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].jpg";
	Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
	filePath = Configuration.REPTEMPFILEPATH;
	
	hdrfile=filePath+ "/" + hdr_file_name;
	ftr_file_name="tempf["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].jpg";
	ftrfile=filePath+ "/" + ftr_file_name;

	hdrFilePath="../temp/"+hdr_file_name;
	ftrFilePath="../temp/"+ftr_file_name;
		
	
	//check for byte array
	if (!(hdrByteArray ==null)) 
	{
		hdrflag="1";
		ByteArrayInputStream fin=new ByteArrayInputStream(hdrByteArray);
					
		BufferedInputStream fbin=new BufferedInputStream(fin);
		
		File fo=new File(hdrfile);

		FileOutputStream fout = new FileOutputStream(fo);
		Rlog.debug("3","after output stream");		
		int c ;
		while ((c = fbin.read()) != -1){
				fout.write(c);
			}
		fbin.close();
		fout.close();
		}	
	else
	{
		hdrflag="0";	
	}
		
		
		//check for length of byte array
	if (!(ftrByteArray ==null))
		{	
		ftrflag="1";
		ByteArrayInputStream fin1=new ByteArrayInputStream(ftrByteArray);
		Rlog.debug("1","1");
		BufferedInputStream fbin1=new BufferedInputStream(fin1);
		File fo1=new File(ftrfile);
		Rlog.debug("2","2");		
		FileOutputStream fout1 = new FileOutputStream(fo1);
		Rlog.debug("3","3");		
		int c1 ;
		while ((c1 = fbin1.read()) != -1){

			fout1.write(c1);
		}
		
		fbin1.close();
		fout1.close();
	}
	else
	{
		ftrflag="0";	
	}

		
	//get the folder name
	Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");
	String fileDnPath = Configuration.DOWNLOADSERVLET;
	
	Configuration.readReportParam(Configuration.ERES_HOME + "eresearch.xml");
	filePath = Configuration.REPDWNLDPATH;
	Rlog.debug("report", filePath);
	//make the file name
	String fileName="reporthtml"+"["+ now.get(now.DAY_OF_MONTH) + now.get(Calendar.MONTH) + (now.get(now.YEAR) - 1900)+ now.get(now.HOUR_OF_DAY)+ now.get(now.MINUTE)+ now.get(now.SECOND) + "].html" ;
	//make the complete file name
	String htmlFile = filePath + "/"+fileName;
	response.setContentType("text/html");    

	 try
    {	
	
		//first save the output in html file
		TransformerFactory tFactory1 = TransformerFactory.newInstance();
		Reader mR1=new StringReader(xml); 
		Reader sR1=new StringReader(xsl); 
		Source xmlSource1=new StreamSource(mR1);
		Source xslSource1 = new StreamSource(sR1);

 		Transformer transformer1 = tFactory1.newTransformer(xslSource1);
		//Set the params
		transformer1.setParameter("hdrFileName", hdrFilePath);
		transformer1.setParameter("ftrFileName", ftrFilePath);
		transformer1.setParameter("repTitle",repTitle);
		transformer1.setParameter("repName",repName);
		transformer1.setParameter("repBy","Velos Administrator");
		transformer1.setParameter("repDate",repDate);
		transformer1.setParameter("argsStr",repArgsDisplay);
		transformer1.setParameter("cond","F");
		transformer1.setParameter("wd","");
		transformer1.setParameter("xd","");
		transformer1.setParameter("hd", "");
		transformer1.setParameter("hdrflag", hdrflag);
		transformer1.setParameter("ftrflag", ftrflag);
		transformer1.setParameter("dl", "");		
	
		// Perform the transformation, sending the output to html file
	  	transformer1.transform(xmlSource1, new StreamResult(htmlFile));

		//now send it to console			
	  	TransformerFactory tFactory = TransformerFactory.newInstance();
		Reader mR=new StringReader(xml); 
		Reader sR=new StringReader(xsl); 
		Source xmlSource=new StreamSource(mR);
		Source xslSource = new StreamSource(sR);
		// Generate the transformer.
			Transformer transformer = tFactory.newTransformer(xslSource);

		//Set the param for header and footer
		transformer.setParameter("hdrFileName", hdrFilePath);
		transformer.setParameter("ftrFileName", ftrFilePath);
		transformer.setParameter("repTitle",repTitle);
		transformer.setParameter("repName",repName);
		transformer.setParameter("repBy","Velos Administrator");
		transformer.setParameter("repDate",repDate);
		transformer.setParameter("argsStr",repArgsDisplay);
		transformer.setParameter("cond","T");
		transformer.setParameter("wd","repGetWord.jsp?htmlFile=" + htmlFile +"&fileDnPath="+fileDnPath+"&filePath="+filePath);
		transformer.setParameter("xd","repGetExcel.jsp?htmlFile=" + htmlFile +"&fileDnPath="+fileDnPath+"&filePath="+filePath);
		transformer.setParameter("hd","repGetHtml.jsp?htmlFileName=" + fileName +"&fileDnPath="+fileDnPath+"&filePath="+filePath);
		transformer.setParameter("hdrflag", hdrflag);
		transformer.setParameter("ftrflag", ftrflag);
		transformer.setParameter("dl", fileDnPath);
											  	  
		// Perform the transformation, sending the output to the response.
      	transformer.transform(xmlSource, new StreamResult(out));
			
    }
	catch (Exception e)
	{
  		out.write(e.getMessage());
 	}
	
%>


</body>
</html>
