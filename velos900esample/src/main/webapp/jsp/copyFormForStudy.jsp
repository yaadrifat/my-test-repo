<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</HEAD>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<script>

function  validate(formobj, numRows)
 {
 	
	
	flag = false ;
	if(numRows > 1)
	{
		for ( count = 0 ; count < numRows ; count ++ )
		{
			
			if (  formobj.selectedForms[count].checked )
			{
				flag = true ;
				break ;
			}
		} 
	}
	
	else if (numRows==1)
	{
		if (  formobj.selectedForms.checked )
				{
					flag = true ;
					
				}
	}
 
		
 
    if ( flag == false ) 
	{
		alert("<%=MC.M_SelFrm_ToBeCopied%> ");/*alert("Please select a form to be copied ");*****/
		return false ;
	}
	
	//if (   !(validate_col (' Category', formobj.newFormType)  )  )  return false;
		
 	if (   !(validate_col (' Form Name', formobj.formName)  )  )  return false;
	
		
     if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false;
	 
	

  }
  

	checkQuote="N";

</script>


<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1) 
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>

<title><%=LC.L_StdForm_Copy%><%--<%=LC.Std_Study%> Forms >> Copy*****--%> </title>


<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*" %>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



<br>
<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="linkedFormsJB" scope="request"  class="com.velos.eres.web.linkedForms.LinkedFormsJB"/>
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>

<%@ page language = "java" import = "com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil"
%>
<jsp:include page="include.jsp" flush="true"/>
<DIV  class="popDefault" id="div1">  

<%
 HttpSession tSession = request.getSession(true); 
 
 
  if (sessionmaint.isValidSession(tSession))
	{
		 String selectedTab = request.getParameter("selectedTab");
		 String accId=(String)tSession.getAttribute("accountId");
		 String usr = (String) tSession.getValue("userId");
		 String studyId = request.getParameter("studyId") ; 
		 int iusr= EJBUtil.stringToNum(usr);
		 int iaccId=EJBUtil.stringToNum(accId);
 		 int istudy=EJBUtil.stringToNum(studyId);
		
		 
		 String catLibType="T" ;
		
		 ArrayList ids= new ArrayList();
		 ArrayList names= new ArrayList();
		 CatLibDao catLibDao=new CatLibDao();

		
		 String pullDown= "";
		 String formDisplayType="";
		 String name="";
		 String desc="";
		 String formId="";
		 String linkedFormId="";

		 ArrayList arrFormLibIds = null;
		 ArrayList arrName=null;
		 ArrayList arrDesc=null;
		 ArrayList arrFormDisplayType=null;
		 ArrayList arrLinkedFormId = null ;
		 
		 LinkedFormsDao linkedFormsDao = new LinkedFormsDao();
		 int pageRight = 7;
		 String mode=request.getParameter("mode");

				
			catLibDao= catLibJB.getAllCategories(iaccId,catLibType);
			ids = catLibDao.getCatLibIds();
			names= catLibDao.getCatLibNames();
		//	pullDown=EJBUtil.createPullDown("newFormType", 0, ids, names);

			
			linkedFormsDao=linkedFormsJB.getStudyLinkedForms(istudy);
			
			arrName=linkedFormsDao.getFormName();
			arrDesc=linkedFormsDao.getFormDescription();
			arrFormDisplayType =linkedFormsDao.getFormDisplayType();
			arrFormLibIds = linkedFormsDao.getFormId();
			arrLinkedFormId = linkedFormsDao.getLFId();
				
		 	int len=arrName.size();
if (pageRight >=6) 
	{
		
 %>
<Form name="copyStudyForms" method="POST" id="cpyStdFrm" action="copyFormForStudySubmit.jsp"  onSubmit="if (validate(document.copyStudyForms, '<%=len%>')== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<Input type="hidden" name="mode" value=<%=mode%> >
<input type="hidden" name="studyId" value=<%=studyId%>>

	<table width="50%" border="0">
	<tr>
    <P class="defComments"><%=MC.M_CopyFrm_EtrDetsForNewFrm%><%--Enter the following details for your new form*****--%></P>
	</tr>
	<tr>
	<td width="25%" align="right"><FONT class="Mandatory">* </FONT> <%=LC.L_Frm_Name%><%--Form Name*****--%> </td>
	 <td width="25%"><input type="text" name="formName" size = 30 MAXLENGTH = 50 > </td>
	 </tr>
	<!--  <tr>
	 <td width="25%" align="right"><FONT class="Mandatory">* </FONT> Form Type</td>
	<td ><%=pullDown%> </td>
	</tr>	 -->
	</table>
	<br>
	<br>
	<table width="80%" cellspacing="1" cellpadding="0" >
					<tr> 
				        <th width="28%"> <%=LC.L_Name%><%--Name*****--%> </th>
				        <th width="30%"> <%=LC.L_Description%><%--Description*****--%> </th>
				        <th width="22%"> <%=LC.L_Selected%><%--Selected*****--%> </th>
					</tr>
				   <%int count=0;
		for(count=0;count<len;count++)
		{
				

			name= ((arrName.get(count)) == null)?"-":(arrName.get(count)).toString();
			desc= ((arrDesc.get(count)) == null)?"-":(arrDesc.get(count)).toString();
			formId= ((arrFormLibIds.get(count)) == null)?"-":(arrFormLibIds.get(count)).toString();
			linkedFormId=arrLinkedFormId.get(count).toString();
			String nameHref=null ;
			if ( name.equals("-"))
			{ 
				nameHref="-";
			}
			else 
			{
				//nameHref="<A href='formcreation.jsp?formLibId="+formId+"&mode=M&srcmenu="+src+"&selectedTab=1'>"+name+"</A>"	;
				//nameHref="<A href='formcreation.jsp?formLibId="+formId+"&mode=M>"+name+"</A>"	;
			}

		

			if ((count%2)==0) 
				{%>
			 <tr class="browserEvenRow"> 
				<%}else
				 { %>
				 <tr class="browserOddRow"> 
				<%}%>	
					
				<td width="28%"><%=name%></td>
				<td width="30%"><%=desc%></td>	
				<td width="22%"><input type="checkbox" name="selectedForms" value="<%=formId%>" onClick=""></td>	
				 </tr>
		<%}%>
					
		</table>
		</table>	
			


<jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="cpyStdFrm"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>



	</FORM>
	 <%
		
	} //end of if body for page right

else

{

%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%

} //end of else body for page right
	}
else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>
  <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
</body>

</html>


			
