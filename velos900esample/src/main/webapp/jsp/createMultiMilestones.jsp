<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Create_Mstones%><%--Create Milestones*****--%></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="include.jsp" flush="true"/>
<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">

//KM-#4518
function asyncValidate(formobj)
{
	selval = formobj.bgtcalId.value;
	studyId = formobj.studyId.value;

	if (formobj.bgtcalFlag){
		formobj.bgtcalFlag.value = formobj.bgtcalId.value;
		if (formobj.bgtcalId.value!=''){
			var bgtcalFlag = formobj.bgtcalFlag.options[formobj.bgtcalFlag.selectedIndex].text;
		
			var flag = 'none';
			if (bgtcalFlag=='S'){
				flag = 'none';
			}else{
				if (document.getElementById('agent').value == '0'){
					 flag = 'block';
				}else{
					flag = '';
				}
			}
			
			var countArray = document.getElementsByName("count");
			var statusArray = document.getElementsByName("dStatus");
			
			for (var i=0; i<countArray.length; i++){
				if (bgtcalFlag=='S') countArray[i].value ='';
				countArray[i].style.display=flag;
				if (bgtcalFlag=='S') statusArray[i].value='';
				statusArray[i].style.display=flag;
			}
		
			var countTDArray = document.getElementsByName("countTD");
			var statusTDArray = document.getElementsByName("statusTD");
			
			for (var i=0; i<countTDArray.length; i++){
				countTDArray[i].style.display=flag;
				statusTDArray[i].style.display=flag;
			}
		} 
	}
	if (selval == '')
		selval =0;
	new VELOS.ajaxObject("ajaxValidation.jsp", {
   		urlData:"bgtcalId="+selval+"&studyId="+studyId,
		   reqType:"POST",
		   outputElement: "calStatMessage" }

   ).startRequest();

}
//KM
window.onload = function() {
	asyncValidate(document.milerule);
}


function validate(formobj) {


mileStoneWIPStat=formobj.isWIPStat.value


if (mileStoneWIPStat ==0){

	alert("<%=MC.M_NoRoleRgt_ToCreateMstone%>");/*alert("You do not have role rights to create Milestone(s)");*****/
	return false;

}




     var selectedRule;
	 var ruleSubType;
	 var typecount = 0;

	 var calendarct ;

     calendarct = formobj.calcount.value;

     formobj.milestoneTypeSelected[0].value = "0";
     formobj.milestoneTypeSelected[1].value = "0";
     formobj.milestoneTypeSelected[2].value = "0";

         for (counter = 0 ; counter < 2 ; counter ++)
         {
			if (formobj.milestoneType[counter].checked)
			{
				if (calendarct == 0)
				{
						alert("<%=MC.M_NoCalSel_ToBgt%>");/*alert("There is no Calendar selected for this Budget. Visit and Event Milestones can be created for a Calendar only.");*****/
					   	return false;
				}
				formobj.milestoneTypeSelected[counter].value = "1";



				if (! validate_col('Calendar',formobj.bgtcalId)  )
			        {

						formobj.bgtcalId.focus();
			      	   	return false;
			         }

				typecount = typecount + 1;
				if (counter == 0)
				{

				    if (! validate_col('Visit Milestone Rule',formobj.rule)  )
			        {

						formobj.rule.focus();
			      	   	return false;
			         }

			        selectedRule  =  formobj.rule.selectedIndex;
		        	ruleSubType = formobj.ruleSubType[selectedRule].value;


			         if ( ( ! validate_col_no_alert('Event Status',formobj.dEventStatus[counter] ))  )
	        			{

				    		if (ruleSubType != 'vm_4')
				    		{
								alert("<%=MC.M_Selc_EvtStatus%>");/*alert("Please select Event Status");*****/
								formobj.dEventStatus[counter].focus();
				    	  	   return false;
				    	  	 }
	         			}


	         		if ( ( validate_col_no_alert('Event Status',formobj.dEventStatus[counter] ))  )
	        			{

				    		if (ruleSubType == 'vm_4')
				    		{
								alert("<%=MC.M_CntSelEvt_MstoneRule%>");/*alert("You cannot select an Event Status with the selected Milestone Rule");*****/
								formobj.dEventStatus[counter].focus();
				    	  	    return false;
				    	  	}
	         			}


		          }
			      else //counter != 0
		          {
		            if (! validate_col('Event Milestone Rule',formobj.EMrule)  )
			         {

						formobj.EMrule.focus();
			      	   	return false;
			         }

			        selectedRule  =  formobj.EMrule.selectedIndex;
			        ruleSubType = formobj.EVRuleSubType[selectedRule].value;


			        if ( !validate_col_no_alert('Event Status',formobj.dEventStatus[counter] )  )
	        		{

		    			if (ruleSubType != 'em_4')
		    			{
		    				alert("<%=MC.M_Selc_EvtStatus%>");/*alert("Please select Event Status");*****/
							formobj.dEventStatus[counter].focus();
		    	  	   		return false;
		    	  	 	}
 			   		 }

 			   		if ((validate_col_no_alert('Event Status',formobj.dEventStatus[counter])) )
			        {

			    		if (ruleSubType == 'em_4')
			    		{
			    			alert("<%=MC.M_CntSelEvt_MstoneRule%>");/*alert("You cannot select an Event Status with the selected Milestone Rule");*****/
							formobj.dEventStatus[counter].focus();
			    	  	   return false;
			    	  	 }

			         }

	          	}


	       	if (! isInteger(formobj.mLimit[counter].value))
			{
					formobj.mLimit[counter].focus();
					alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
				 	return false;
			}

			if ( !isInteger(formobj.count[counter].value) )
			{
				if (!isNegNum(formobj.count[counter].value))
				{
					formobj.count[counter].focus();
					alert("<%=MC.M_Etr_ValidNum%>");/*alert("Please enter a valid Number");*****/
				 	return false;
				}
				else
				{
					if (formobj.count[counter].value == '-0')
					{
						formobj.count[counter].focus();
						alert("<%=MC.M_InvalidNegVal_EtrVldCnt%>");/*alert("Invalid negative value. Please enter a valid 'Count'");*****/
					 	return false;
					}

					if (parseInt(formobj.count[counter].value) < -1)
					{
						formobj.count[counter].focus();
						alert("<%=MC.M_CntLess_EtrValidCount%>");/*alert("The count cannot be less than -1. Please enter a valid 'Count'");*****/
					 	return false;
					}
				}
			}

       } //for checkbox check

     }

      //for additional milestones
     if (formobj.milestoneType[2].checked)
     {
     	typecount = typecount + 1;



     	if (parseInt(calendarct) > 0)
     	{
     		if (! validate_col('Calendar',formobj.bgtcalId)  )
			        {

						formobj.bgtcalId.focus();
			      	   	return false;
			      }

     	}
     	formobj.milestoneTypeSelected[2].value = "1";
     }

     if (typecount == 0)
     {
     	alert("<%=MC.M_SelAtleast_OneMstoneTyp%>");/*alert("Please select atleast one Milestone Type");*****/
     	return false;

     }
	if (!(validate_col('e-Signature',formobj.eSign))) return false
	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
	}
}


function fclose() {
	self.close();
}


</SCRIPT>

<%
int ienet = 2;

String agent1 = request.getHeader("USER-AGENT");
   if (agent1 != null && agent1.indexOf("MSIE") != -1)
     ienet = 0; //IE
    else
	ienet = 1;
	if(ienet == 0) {
%>
<body style="overflow:scroll;">
<%
	} else {
%>
<body>
<%
	}
%>
<P class="sectionHeadings"><%=LC.L_Create_Mstones%><%--Create Milestones*****--%> </P>

<input type="hidden" id="agent" name="agent" value="<%=ienet%>"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="budgetcalB" scope="request"	class="com.velos.esch.web.budgetcal.BudgetcalJB" />
<jsp:useBean id="protvisitB" scope="request" class="com.velos.esch.web.protvisit.ProtVisitJB"/>

<jsp:useBean id="stdRightsB" scope="page" class="com.velos.eres.web.studyRights.StudyRightsJB" />
<jsp:useBean id="milestoneB" scope="request" class="com.velos.eres.web.milestone.MilestoneJB"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />
<jsp:include page="include.jsp" flush="true"/>

<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB, com.velos.esch.business.common.EventAssocDao,com.velos.esch.business.common.*,com.velos.eres.service.util.*"%>
<%
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession)) {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%

		int accId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));
		String uName = (String) tSession.getValue("userName");
		String userIdFromSession = (String) tSession.getValue("userId");

		String studyId = request.getParameter("studyId");
		String bgtcalId ;
		String budgetId = request.getParameter("budgetId");
		String bgtcalIdStr = "";
		String bgtcalFlagStr = "";
		int baselineIndex = -1;
		int calcount = 0;
		int mileRight = 0;
		String textDisp="";

		//KM-08Jun10-#4966
		CodeDao cdStat = new CodeDao();
		int wipPk = cdStat.getCodeId("milestone_stat","WIP");
		String wipDesc = cdStat.getCodeDescription(wipPk);

		ArrayList teamId ;
		teamId = new ArrayList();
		TeamDao teamDao = new TeamDao();
		teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
		teamId = teamDao.getTeamIds();

		if (teamId.size() <=0)
		{
			mileRight  = 0;
			StudyRightsJB stdRightstemp = new StudyRightsJB();
		}
		if (teamId.size() == 0) {
			mileRight=0 ;
		}else {
			stdRightsB.setId(EJBUtil.stringToNum(teamId.get(0).toString()));
		 	ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();

						stdRightsB.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRightsB.loadStudyRights();


			if ((stdRightsB.getFtrRights().size()) == 0){
				mileRight = 0;
			}else{
				mileRight = Integer.parseInt(stdRightsB.getFtrRightsByValue("MILESTONES"));
			}
		}

		GrpRightsJB grprightsJB = (GrpRightsJB) tSession.getValue("GRights");
		int mileGrpRight = Integer.parseInt(grprightsJB.getFtrRightsByValue("MILEST"));



		if ((mileRight == 5 || mileRight  ==7) && (mileGrpRight >= 4))//JM: 06Jun2010: #4973
		{
			BudgetcalDao budgetcalDao = budgetcalB.getAllBgtCals(EJBUtil.stringToNum(budgetId));

			ArrayList bgtcalIds = budgetcalDao.getBgtcalIds();
			ArrayList bgtcalProtNames = budgetcalDao.getBgtcalProtNames();
			ArrayList bgtcalProtIds = budgetcalDao.getBgtcalProtIds();
			ArrayList bgtcalFlags = budgetcalDao.getBgtcalClinicFlags(); //Retrieving Admin/studysetup flags 

			while(bgtcalIds != null && (bgtcalProtIds.indexOf("0")>=0))
			{
				//remove the default baseline record
				baselineIndex = bgtcalProtIds.indexOf("0");
				if (baselineIndex > -1)
				{
					bgtcalIds.remove(baselineIndex);
					bgtcalProtNames.remove(baselineIndex);
					bgtcalProtIds.remove(baselineIndex);
					bgtcalFlags.remove(baselineIndex);
				}
			}

			calcount = bgtcalIds.size();

			if(bgtcalIds != null && calcount > 0) {

				String ddName = "bgtcalId onchange ='asyncValidate(document.milerule)'";
				if (ienet != 0)
					ddName += " onkeydown='asyncValidate(document.milerule)'";

				bgtcalIdStr = "<b>"+LC.L_Select_Cal/*Select Calendar*****/+": </b> &nbsp;  " +  EJBUtil.createPullDown(ddName,0,bgtcalIds,bgtcalProtNames) ;
				bgtcalFlagStr = " " +  EJBUtil.createPullDown("bgtcalFlag style='display:none'",0,bgtcalIds,bgtcalFlags) ;

			}
			else
			{
				bgtcalIdStr = MC.M_NoCalLnkBgt_AddlMstone/*There are no Calendars linked with this Budget. You can only create 'Additional Milestones'*****/+" "+"<input type=hidden name=\"bgtcalId\" value=0>";
			}



			ArrayList curr = new ArrayList();


			CodeDao codeLst = new CodeDao();
			String 	dRule = "";

			CodeDao codeLstEventRule = new CodeDao();
			String dEMRule = "";



		//JM: 16Aug2010: #4971

		String roleCodePk="";

			ArrayList tId = new ArrayList();

			TeamDao teamRoleDao = new TeamDao();
			teamRoleDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
			tId = teamRoleDao.getTeamIds();

			if (tId != null && tId.size() > 0)
			{
				ArrayList arRoles = new ArrayList();
				arRoles = teamRoleDao.getTeamRoleIds();

				if (arRoles != null && arRoles.size() >0 )
				{
					roleCodePk = (String) arRoles.get(0);

					if (StringUtil.isEmpty(roleCodePk))
					{
						roleCodePk="";
					}
				}
				else
				{
					roleCodePk ="";
				}

			}
			else
			{
				roleCodePk ="";
			}

		CodeDao cdMs = new CodeDao();

		cdMs.getCodeValuesForStudyRole("milestone_stat",roleCodePk);

		ArrayList statSubTypes = cdMs.getCSubType();


	%>

	<DIV class="popDefault" id="div1">


	   <form name="milerule" method="post" id="milerulefrm" action="saveMultiMilestones.jsp" onsubmit="if (validate(document.milerule)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">


	   <input type="hidden" name="studyId" value="<%=studyId%>">
	   <input type="hidden" name="budgetId" value="<%=budgetId%>">
	   <input type="hidden" name="calcount" value="<%=calcount%>">


	   <% if (!statSubTypes.contains("WIP")){%>

       	<input type="hidden" name="isWIPStat" value="0">

       <%}else{%>
       <input type="hidden" name="isWIPStat" value="WIP">

       <%}%>





	<table width="100%"> <tr> <td width="38%"> <%= bgtcalIdStr + bgtcalFlagStr%> </td> <td width="62%"> <span id="calStatMessage" > <%=textDisp%> </span></td> </tr></table><BR>

		<table>

		<%
		int i = 0;


			ArrayList fk_visits= new ArrayList();




			String dStatus = "";
			String dpayType = "";
			String dpayFor = "";
			CodeDao codeLstPayType = new CodeDao();
			CodeDao codeLstPayFor = new CodeDao();

			ArrayList ruleSubType = new ArrayList();
			ArrayList EMRuleSubType = new ArrayList();

			CodeDao codeLstStatus = new CodeDao();
			SchCodeDao cdEvent = new SchCodeDao();

			String dEventStatus = "";

			codeLstStatus.getCodeValues("patStatus",accId);
			dStatus = codeLstStatus.toPullDown("dStatus");

	 		cdEvent.getCodeValues("eventstatus",0);
	 		dEventStatus =   cdEvent.toPullDown("dEventStatus");


			//JM: 30Dec2009: #3357
			codeLstPayType.getCodeValues("milepaytype");

	        //retrieve 'Receivable' by default
	        int default_sel = codeLstPayType.getCodeId("milepaytype", "rec");

			dpayType = EJBUtil.createPullDownWithStrNoSelectIfHidden("dpayType",""+default_sel,codeLstPayType.getCId(),codeLstPayType.getCDesc(),codeLstPayType.getCodeHide());




			codeLstPayFor.getCodeValues("milePayfor");
			 dpayFor = codeLstPayFor.toPullDown("dpayFor");



		  %>

		  <%

			codeLst.getCodeValues("VM");
			dRule = codeLst.toPullDown("rule");

			codeLstEventRule.getCodeValues("EM");
			dEMRule = codeLstEventRule.toPullDown("EMrule");


			ruleSubType = codeLst.getCSubType();

			if (ruleSubType != null )
			{
				for ( int k=0; k < ruleSubType.size(); k++)
				{
				 	%>
			 			<input type=hidden name=ruleSubType value='<%=ruleSubType.get(k) %>'>
				 	<%

				}
			}

			EMRuleSubType = codeLstEventRule.getCSubType();

			if (EMRuleSubType != null )
			{
				for ( int k=0; k < EMRuleSubType.size(); k++)
				{
				 	%>
			 			<input type=hidden name=EVRuleSubType value='<%=EMRuleSubType.get(k) %>'>
				 	<%

				}
			}

			%>
			<table width="90%">
			<tr>
			<!--<th class="headernoColor" width = '35%'>Milestone Type<FONT class="Mandatory">* </FONT> </th> -->
			<th class="headernoColor" width = '20%'><%=LC.L_Milestone_Rule%><%--Milestone Rule*****--%><FONT class="Mandatory">* </FONT></th>
			<th class="headernoColor" width = '30%'><%=LC.L_Event_Status%><%--Event Status*****--%><FONT class="Mandatory">* </FONT></th>			
			<th id="countTD" name="countTD" class="headernoColor"  width=10% ><%=LC.L_Pat_Count%><%--Patient Count*****--%></th>
			<th id="statusTD" name="statusTD" class="headernoColor"  width=15% ><%=LC.L_Patient_Status%><%--Patient Status*****--%></th>
			<th class="headernoColor" width='5%' ><%=LC.L_Limit%><%--Limit*****--%></th>
			<th class="headernoColor" width='5%' ><%=LC.L_Payment_Type%><%--Payment Type*****--%></th>
			<th class="headernoColor" width='15%' ><%=LC.L_Payment_For%><%--Payment For*****--%></th>

			</tr>

			<tr>
			<td><hr class="thinLine"></td>
			<td><hr class="thinLine"></td>
			<td id="countTD" name="countTD"><hr class="thinLine"></td>
			<td id="statusTD" name="statusTD" ><hr class="thinLine"></td>
			<td><hr class="thinLine"></td>
			<td><hr class="thinLine"></td>
			<td><hr class="thinLine"></td>
			 </tr>
			<% for ( int kk = 0; kk < 2; kk++) { %>
		 		<tr>
					<td align=left  class="tdDefault">
						<% if ( kk == 0 ) { %>
							<input type=checkbox name="milestoneType" value="VM"> <b><%=LC.L_Visit_Mstones%><%--Visit Milestones*****--%> </b>
							<input type=hidden name="milestoneTypeSelected" value="0">
							<input type=hidden name="milestoneTypeCode" value="VM">


						<% } else { %>
							<input type=checkbox name="milestoneType" value="EM"> <b><%=LC.L_Evt_Mstones%><%--Event Milestones*****--%> </b>
							<input type=hidden name="milestoneTypeSelected" value="0">
							<input type=hidden name="milestoneTypeCode" value="EM">
						<% } %>
				</td>
				</tr>
				<tr>	<td align=left>
						<% if ( kk == 0 ) { %>
							<%=dRule%>
						<% } else { %>
							<%=dEMRule%>
						<% } %>

					</td>

					<td align=left><%=dEventStatus%></td>
					<td align="center" id="countTD" name="countTD"><input type=text name=count size=5 maxlength=10 class="leftAlign" /></td>
					<td id="statusTD" name="statusTD"><%=dStatus%></td>

				<td><input type=text name="mLimit" size=3 maxlength=10  class="leftAlign" ></td>
			<td >
		<%=dpayType%>
		</td>
		<td >
		<%=dpayFor%>
		</td>
		 	</tr>

		 <% } //end of for %>
		 <!-- for additional milestones-->

		  	<tr>
				<td align=left  class="tdDefault">
			 		<br><br>
							<input type=checkbox name="milestoneType" value="AM"> <b><%=LC.L_Addl_Mstones%><%--Additional Milestones*****--%> </b>
							<input type=hidden name="milestoneTypeSelected" value="0">
							<input type=hidden name="milestoneTypeCode" value="AM">

							<input type=hidden name="count" size=5 maxlength=10 class="leftAlign" >
							<input type=hidden name="dEventStatus" value="0">
							<input type=hidden name="dStatus" value="0">


			 	</td>
				</tr>
				<tr>
					<td align=left>-</td>
					<td id="countTD" name="countTD" align=left>-</td>
					<td id="statusTD" name="statusTD" align=left>-</td>
					<td >-</td>

				<td>-<input type=hidden name="mLimit" size=3 maxlength=10  class="leftAlign" ></td>
			<td >
		<%=dpayType%>
		</td>
		<td >
		<%=dpayFor%>
		</td>
		</tr>
		<tr><td colspan=7><p class="defComments">
			<%Object[] arguments = {wipDesc}; %>
	    <%=VelosResourceBundle.getMessageString("M_NoteMstoneAmt_OptNonCal",arguments)%><%--<b> Please Note: </b><BR>- Milestone Payment Amount will default to the respective line item's Cost/Patient amount (with all calculations applied e.g. indirects, discounts etc. <BR>- All Milestones created will default to '<%=wipDesc%>' Status<br>- If 'Additional Milestones' option is selected, all non-Calendar related line items will be published as 'Additional Milestones'*****--%> 

			</td> </p>
		</tr>
		<tr>
			<td colspan=7>
				<jsp:include page="submitBar.jsp" flush="true">
					<jsp:param name="displayESign" value="Y"/>
					<jsp:param name="formID" value="milerulefrm"/>
					<jsp:param name="showDiscard" value="Y"/>
					<jsp:param name="noBR" value="Y"/>
				</jsp:include>
			</td>
		</tr>
	</table>

	  </Form>
	  <%

		}
		else
		{
			%>

		  <jsp:include page="accessdenied.jsp" flush="true"/>

		  <%

		}
	} //end of if session times out

else

{

%>
  <jsp:include page="timeout_childwindow.jsp" flush="true"/>
  <%

} //end of else body for page right

%>
</div>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>


</body>

</html>












