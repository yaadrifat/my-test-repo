<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.HashMap,org.json.*,com.velos.eres.service.util.LC,com.velos.eres.service.util.MC"%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%
    request.setCharacterEncoding("UTF-8");
    response.setContentType("application/json");

	HttpSession tSession = request.getSession(true);
    JSONObject jsObj = new JSONObject();
    if (!sessionmaint.isValidSession(tSession)) {
        // Session is invalid; print an error and exit
        response.sendError(HttpServletResponse.SC_FORBIDDEN, "SESSION_TIME_OUT");
        jsObj.put("error", new Integer(-1));
        jsObj.put("errorMsg", MC.M_UsrNot_LoggedIn/*"User is logged in."*****/);
	    out.println(jsObj.toString());
        return;
    }
    
    jsObj.put("error", new Integer(0));
    jsObj.put("errorMsg", MC.M_UsrNot_LoggedIn/*"User is logged in."*****/);
    out.println(jsObj.toString());
    // <html><head></head><body></body></html>
%>
