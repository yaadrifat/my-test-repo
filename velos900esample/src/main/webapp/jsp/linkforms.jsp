<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Link_Forms%><%--Link Forms*****--%></title>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT>

 function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false;
	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   	}
 }
 function addform(formobj,num)
 {
		for (i=0;i<num;i++) {
			if (num > 1) {
				if (formobj.selForms[i].checked) {
					formobj.submit();
					return true;
				}
			} else {
				if (formobj.selForms.checked) {
					formobj.submit();
					return true;
				}
			}
		}

 }
//Modified for INF-18183::Akshi
var checkBoxBefore= [];
var checkBoxAfter= [];
var checkBoxCount =0;

/*This is a common function used for retrieving the checkbox values before and after*/
function getCheckBoxesvalues(){
	var checkBoxes = document.getElementsByTagName("input");
	var checkBoxArray =[];
	var checkCount =0;
	
	for (i=0;i<checkBoxes.length;i++) {
		if(checkBoxes[i].type=="checkbox"){
			checkBoxArray[checkCount] = checkBoxes[i].checked;
			checkCount++;
		}
	}
	return checkBoxArray;
}
	
function checkChanges()
{
	var result=false;
	checkBoxAfter = getCheckBoxesvalues();
	for (x=0;x<checkBoxBefore.length;x++) {
		if(checkBoxBefore[x] != checkBoxAfter[x])
		{
			result= true;
			break;
		}
	}
	return result;
}
	
//window.setTimeout('checkBoxBefore = checkBoxesvalues()', 1000);

 
</SCRIPT>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,com.velos.eres.business.common.LinkedFormsDao,com.velos.eres.web.linkedForms.LinkedFormsJB,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="include.jsp" flush="true"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

</head>
<body>
<%
	//KM-4267
	String eventName = "";

	String eventId=request.getParameter("eventId");
	String calName = request.getParameter("calName");
	calName = StringUtil.decodeString(calName);
	String calledFrom = request.getParameter("calledFrom");
	String protocolId = request.getParameter("protocolId");
	String studyId = request.getParameter("studyId");
	String formSP=request.getParameter("formSP");
	String formPS=request.getParameter("formPS");
	String patLinks=request.getParameter("patLinks");
	String fromPage = request.getParameter("fromPage");

	StudyDao studydao=new StudyDao();
	String study="";
	if(studyId!=null && !studyId.equals("null") && !studyId.equals("")){
		study=studydao.getStudy(EJBUtil.stringToNum(studyId));
	}

	if (StringUtil.isEmpty(calledFrom))
	{
		calledFrom = "";
	}
	if (StringUtil.isEmpty(patLinks))
	{
		patLinks = "";
	}


	if (calledFrom.equals("P")||calledFrom.equals("L") ||(calledFrom.equals("S") && fromPage.equals("selectEvent"))){
       	  eventdefB.setEvent_id(EJBUtil.stringToNum(eventId));
       	  eventdefB.getEventdefDetails();
          eventName = eventdefB.getName();
//JM: 12Nov2009: #4399
	  	  eventName = (eventName==null)?"":eventName;
	  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
	  		eventName=eventName.substring(0,1000);
	  	  }

    }else{
       	  eventassocB.setEvent_id(EJBUtil.stringToNum(eventId));
          eventassocB.getEventAssocDetails();
       	  eventName = eventassocB.getName();
//JM: 12Nov2009: #4399
	  	  eventName = (eventName==null)?"":eventName;
	  	  if (eventName.length()>1000){//for the old existing event names > 1000 or larger
	  		eventName=eventName.substring(0,1000);
	  	  }

    }






%>
<Form name="selectForms" id="selLinkFormsId" method="post" action="addcrflinkforms.jsp" onsubmit="if(!checkChanges()){alert('No changes to save'); self.close(); return false;}; if (validate(document.selectForms)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
<DIV>

<P class="sectionHeadings"><%=LC.L_Link_Forms%><%--Link Forms*****--%></P>

<%  if(calledFrom.equals("S")) { %>
<P class="defComments"><%=LC.L_Study_Number%><%--<%=LC.Std_Study%> Number*****--%>:   <%=study%>
</P>
<% } %>

<%  if(! calledFrom.equals("L"))  { %>
<P class="defComments"><%=LC.L_Pcol_Cal%><%--Protocol Calendar*****--%>:   <%=calName%></P>
<% } %>
<P class="defComments"><%=LC.L_Event%><%--Event*****--%>:   <%=eventName%> </P>
<input type="hidden" name=eventId value="<%=eventId%>">

<table class = tableDefault width="100%" cellspacing="0" cellpadding="0" border=0 >
	<tr>
		<th width="80%"><P class="sectionHeadings"> <%=LC.L_ActiveOrLkdownFrm%><%--Active / Lockdown Forms*****--%> </P></th>
		<th width="20%"><P class="sectionHeadings"> <%=LC.L_Select%><%--Select*****--%> </P></th>
	</tr>
</table>
<%
	HttpSession tSession = request.getSession(true);
	String accId=(String)tSession.getValue("accountId");
	String dispType="SP_PS";
	String prevDispType="";
	String selForms="";
	ArrayList formNames = null;
	ArrayList formIds=null;
	ArrayList formTypes=null;
	LinkedFormsJB linkedFormsJB=new LinkedFormsJB();
	LinkedFormsDao linkedFormsDao=(LinkedFormsDao)linkedFormsJB.getStudyActiveLockdownForms(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(accId));
	formNames = linkedFormsDao.getFormName();
	formIds=linkedFormsDao.getFormId();
	formTypes=linkedFormsDao.getFormLFDisplayType();
	int formcount = formNames.size();
	String formName = "";
	String formId="";
	String formType="";

	int cnt=formIds.size()+4;
	boolean flag=false;
	String[] st = null;

	if (StringUtil.isEmpty(formSP))
	{
		formSP = "";
	}

	if (StringUtil.isEmpty(formPS))
	{
		formPS = "";
	}

	String[] sp = formSP.split(",");
	String[] ps = formPS.split(",");
	int restrictedFormCount = 0;
	int allStudyFormCount = 0;

	if(formcount==0){
%>
		<table class = tableDefault width="100%" cellspacing="0" cellpadding="0" border=0 >
			<tr>
				<td class=tdDefault><b><%=MC.M_FrmLnkPats_ToStd%><%--Forms Linked to <%=LC.Pat_Patients%> of this <%=LC.Std_Study%>*****--%></b>

				<%  if(calledFrom.equals("L") || calledFrom.equals("P")){ %>
						<BR> &nbsp;&nbsp;&nbsp; <font class="mandatory">***** <%=LC.L_Not_Applicab%><%--Not Applicable*****--%> *****</font>
					<% } %>

				</td>
			</tr>
		</table>
		<br>
		<table class = tableDefault width="100%" cellspacing="0" cellpadding="0" border=0>
			<tr><td class=tdDefault><b> <%=MC.M_FrmLkdPat_Std%><%--Forms Linked to <%=LC.Pat_Patient%>(All <%=LC.Std_Studies%>)*****--%></b></td></tr>
		</table>
		<br>
		<table class = tableDefault width="100%" cellspacing="0" cellpadding="0" border=0>
			<tr><td class=tdDefault><b><%=MC.M_FrmLkdPat_StdRest%><%--Forms Linked to <%=LC.Pat_Patient%>(All <%=LC.Std_Studies%> - Restricted)*****--%> </b></td></tr>
		</table>



	<%}
	%>
		<table class = tableDefault width="100%" cellspacing="0" cellpadding="0" border=0 >
	<%



	for (int j=0 ; j<formcount; j++){
		 formName =((formNames.get(j))==null)?"":formNames.get(j).toString();
		 formId=((formIds.get(j)) == null)?"":formIds.get(j).toString();
		 formType=((formTypes.get(j)) == null)?"":formTypes.get(j).toString();

		   if(formType.equals("PR") )
		 {
			restrictedFormCount = restrictedFormCount + 1;
		 }

		 if(formType.equals("PS") )
		 {
			allStudyFormCount  = allStudyFormCount  + 1;
		 }



			if (!formType.equals(prevDispType)){
				prevDispType=formType;
				if (formType.equals("SP") || j ==0){
				//st=sp;
				%>

				<tr>
					<td class=tdDefault><b><%=MC.M_FrmLnkPats_ToStd%><%--Forms Linked to <%=LC.Pat_Patients%> of this <%=LC.Std_Study%>*****--%></b><BR><BR>

					<%  if(calledFrom.equals("L") || calledFrom.equals("P")){ %>
						<BR> &nbsp;&nbsp;&nbsp; <font class="mandatory">***** <%=LC.L_Not_Applicab%><%--Not Applicable*****--%> *****</font>
					<% } %>

					</td>

				</tr>

				<%
				}  if(formType.equals("PS")  ){
				%>


				<tr><td class=tdDefault><BR><BR><b><%=MC.M_FrmLkdPat_Std%><%--Forms Linked to <%=LC.Pat_Patient%>(All <%=LC.Std_Studies%>)*****--%> </b> <BR><BR></td> </tr>


			<%	}  if(formType.equals("PR")  ){
					if (allStudyFormCount ==0)
					{

					%>

						<tr>
							<td class=tdDefault  align="left">
							<BR><BR><b><%=MC.M_FrmLkdPat_Std%><%--Forms Linked to <%=LC.Pat_Patient%>(All <%=LC.Std_Studies%>)*****--%></b><br><br>
							</td>
						</tr>


					<%

					}
					%>


				<tr ><td class=tdDefault><BR><BR><b><%=MC.M_FrmLkdPat_StdRest%><%--Forms Linked to <%=LC.Pat_Patient%>(All <%=LC.Std_Studies%> - Restricted)*****--%> </b> <br><br></td></tr>


			<%	}
		    	}

		if (formType.equals("SP") )
			st=sp;
		else if (formType.equals("PS") || formType.equals("PR"))
			st=ps;
		for (int k=0; k<st.length;k++){
			flag=false;
         		if(formId.equals(st[k])){
		      		flag=true;
		      		selForms = formId+"[VELSEP]"+formName+"[VELSEP]"+formType;
				break;
			}else{
				flag=false;
			}
		}
	      %>

	      <%
		if ((j%2)==0) {
	 	%>
			<tr class="browserEvenRow">
		<%
	     	}else{ %>
	  		<tr class="browserOddRow">
		<%
		}
		if (flag==true){
		%>
			<td class=tdDefault width="80%" align="left">&nbsp;&nbsp;<%=formName%>  </td>
			<td align="center" width="20%"><input type="checkbox" checked name="selForms" value="<%=selForms%>"></td>
		<%
		}
		else{
			selForms = formId+"[VELSEP]"+formName+"[VELSEP]"+formType;
		%>
			<td class=tdDefault width="80%" align="left">&nbsp;&nbsp;<%=formName%> </td>
			<td align="center" width="20%"><input type="checkbox" name="selForms" value="<%=selForms%>"></td>
		<%}%>
			<script>
				//Constructing the checkbox array at the run time
				checkBoxBefore[checkBoxCount] = <%=flag%>;
				checkBoxCount++;
			</script>
			</tr>

      	<%
      	}

	if (restrictedFormCount == 0 && formcount > 0)
	{
	if ( allStudyFormCount == 0)
	{
			%>


					<tr>
						<td class=tdDefault align="left">
						<BR><BR><b><%=MC.M_FrmLkdPat_Std%><%--Forms Linked to <%=LC.Pat_Patient%>(All <%=LC.Std_Studies%>)*****--%></b><BR><BR>
						</td>
					</tr>



			<%

	}
	%>
	<br>
					<tr>
						<td class=tdDefault align="left">
						<BR><BR><b><%=MC.M_FrmLkdPat_StdRest%><%--Forms Linked to <%=LC.Pat_Patient%>(All <%=LC.Std_Studies%> - Restricted)*****--%></b><BR><BR>
						</td>
					</tr>

	<%

	}



	String labs="~[VELSEP]Labs[VELSEP]labs";
	String demo="~[VELSEP]Demographics[VELSEP]demo";
	String adv="~[VELSEP]Adverse Events[VELSEP]adv";
	String Patstat="~[VELSEP]Patient Status[VELSEP]Patstat";

%>


 	<tr><td class=tdDefault><BR><BR><b><%=LC.L_Patient_Links%><%--<%=LC.Pat_Patient%> Links*****--%></b></td></tr>


<%
	String strCheck="";
	if(patLinks.indexOf("labs") !=-1 ){
	    strCheck = "checked";
	}
%>
	<tr class="browserEvenRow" width="80%" align="left"><td> <%=LC.L_Labs%><%--Labs*****--%></td><td align="center" width="20%" border=1><input type="checkbox" <%= strCheck%> name="selForms" value="<%=labs%>"></td>
	<script>
		//Constructing the checkbox array at the run time
		checkBoxBefore[checkBoxCount] = <%=("checked".equals(strCheck))?true:false%>;
		checkBoxCount++;
	</script>
<%
	   strCheck = "";
	if(patLinks.indexOf("demo") !=-1 ) {
		    strCheck = "checked";
	}
%>
	<tr class="browserOddRow" width="80%" align="left"><td><%=LC.L_Demographics%><%--Demographics*****--%></td><td align="center" width="20%" border=1><input type="checkbox" <%= strCheck%> name="selForms" value="<%=demo%>"></td>
	<script>
		//Constructing the checkbox array at the run time
		checkBoxBefore[checkBoxCount] = <%=("checked".equals(strCheck))?true:false%>;
		checkBoxCount++;
	</script>
<%
	strCheck = "";
	if(patLinks.indexOf("adv") !=-1) {
	     strCheck = "checked";
	}
%>
	<tr class="browserEvenRow" width="80%" align="left"><td> <%=LC.L_Adverse_Events%><%--Adverse Events*****--%></td><td align="center" width="20%" border=1><input type="checkbox"  <%= strCheck%> name="selForms" value="<%=adv%>"></td>
	<script>
		//Constructing the checkbox array at the run time
		checkBoxBefore[checkBoxCount] = <%=("checked".equals(strCheck))?true:false%>;
		checkBoxCount++;
	</script>
<%
	strCheck = "";
	if(patLinks.indexOf("Patstat") !=-1){
		strCheck = "checked";
	}
%>
	<tr class="browserOddRow" width="80%" align="left"><td><%=LC.L_Patient_Status%><%--<%=LC.Pat_Patient%> Status--%></td><td align="center" width="20%" border=1><input type="checkbox" <%= strCheck%> name="selForms" value="<%=Patstat%>"></td>
	<script>
		//Constructing the checkbox array at the run time
		checkBoxBefore[checkBoxCount] = <%=("checked".equals(strCheck))?true:false%>;
		checkBoxCount++;
	</script>


</table>

<jsp:include page="propagateEventUpdate.jsp" flush="true">
<jsp:param name="fromPage" value="<%=fromPage%>"/>
<jsp:param name="formName" value="selectForms"/>
<jsp:param name="eventName" value="<%=eventName%>"/>
</jsp:include>

<input type="hidden" name="protocolId" value=<%=protocolId %>>
<input type="hidden" name="calledFrom" value=<%=calledFrom%>>

<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="selLinkFormsId"/>
		<jsp:param name="showDiscard" value="Y"/>
</jsp:include>

</div>
</form>
<div class ="mainMenu" id = "emenu"> </div>
<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</html>
