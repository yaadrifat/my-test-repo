<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
	<title><%=LC.L_VelosEres_Register%><%--Velos eResearch : Register*****--%></title>

	<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,com.velos.eres.service.util.*"%>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<link rel="stylesheet" href="styles/velos_style.css" type="text/css">
<body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<table border="0" cellspacing="0" cellpadding="0" width="770">

<!--  LIBRARY JAVASCRIPT START -->
<script language="JavaScript">
<!--
function fwLoadMenus() {
	var At_AGlance=M_At_AGlance;
	var varPartners=L_Partners_Lower;
	var varFaqs=L_Faqs;
	var varProducts=L_Products_Lower;
	var varServices=L_Services_Lower;
	var varAccounts=L_Accounts_Lower;
	var varPricing=L_Pricing_Lower;
	var varSecurity=L_Security_Lower;
	var varHipaa=L_Hipaa;
	var varDemo=L_Demo_Lower;
	var varContact=L_Contact_Lower;
	var varRegister=L_Register_Lower;
	var InThe_News=L_InThe_News;
	var varhome=L_Home;
  if (window.fw_menu_0) return;
  window.fw_menu_0 = new Menu("root",92,19,"Arial, Helvetica, sans-serif",12,"#9933ff","#ffffff","#ffffff","#000084");
  fw_menu_0.addMenuItem(At_AGlance,"location='ataglance.html'");/*fw_menu_0.addMenuItem("at a glance","location='ataglance.html'");*****/
  fw_menu_0.addMenuItem(InThe_News,"location='news.html'");/*fw_menu_0.addMenuItem("in the news","location='news.html'");*****/
  fw_menu_0.addMenuItem(varPartners,"location='partners.html'");/*fw_menu_0.addMenuItem("partners","location='partners.html'");*****/
  fw_menu_0.addMenuItem(varFaqs,"location='faqs.html'");/*fw_menu_0.addMenuItem("faqs","location='faqs.html'");*****/
   fw_menu_0.hideOnMouseOut=true;
  window.fw_menu_1 = new Menu("root",137,19,"Arial, Helvetica, sans-serif",12,"#9933ff","#ffffff","#ffffff","#000084");
  fw_menu_1.addMenuItem(varProducts+" &amp; "+varServices,"location='products.html'");/*fw_menu_1.addMenuItem("products &amp; services","location='products.html'");*****/
  fw_menu_1.addMenuItem(varAccounts+" &amp; "+varPricing,"location='pricing.html'");/*fw_menu_1.addMenuItem("accounts &amp; pricing","location='pricing.html'");*****/
  fw_menu_1.addMenuItem(varSecurity+" &amp; "+varHipaa,"location='security.html'");/*fw_menu_1.addMenuItem("security &amp; HIPAA","location='security.html'");*****/
   fw_menu_1.hideOnMouseOut=true;
  window.fw_menu_2 = new Menu("root",82,19,"Arial, Helvetica, sans-serif",12,"#9933ff","#ffffff","#ffffff","#000084");
  fw_menu_2.addMenuItem(varDemo,"location='demo.html'");/*fw_menu_2.addMenuItem("demo","location='demo.html'");*****/
   fw_menu_2.hideOnMouseOut=true;
  window.fw_menu_3 = new Menu("root",82,19,"Arial, Helvetica, sans-serif",12,"#9933ff","#ffffff","#ffffff","#000084");
  fw_menu_3.addMenuItem(varContact,"location='contact.html'");/*fw_menu_3.addMenuItem("contact","location='contact.html'");*****/
   fw_menu_3.hideOnMouseOut=true;
  window.fw_menu_4 = new Menu("root",82,19,"Arial, Helvetica, sans-serif",12,"#9933ff","#ffffff","#ffffff","#000084");
  fw_menu_4.addMenuItem(varRegister,"location='register.jsp'");/*fw_menu_4.addMenuItem("register","location='register.jsp'");*****/
   fw_menu_4.hideOnMouseOut=true;
  window.fw_menu_5 = new Menu("root",82,19,"Arial, Helvetica, sans-serif",12,"#9933ff","#ffffff","#ffffff","#000084");
  fw_menu_5.addMenuItem(varhome,"location='index.html'");/*fw_menu_5.addMenuItem("home","location='index.html'");*****/
   fw_menu_5.hideOnMouseOut=true;

  fw_menu_3.writeMenus();
} // fwLoadMenus()


function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


//-->
</script>
<SCRIPT LANGUAGE="JavaScript">
function Openflashtour()
	{
	thewindow = window.open('http://www.veloseresearch.com/eres/jsp/producttour.html', 'anew', config='height=430,width=570,toolbar=no,menubar=no,scrollbars=no,resizable=no,location=no,directories=no,status=no');
	}
</SCRIPT>


<script language="JavaScript1.2" src="fw_menu.js"></script>

<script language="JavaScript1.2">fwLoadMenus();</script>

<!--  LIBRARY JAVASCRIPT END -->
<SCRIPT Language="javascript">


 function  validate(){
     formobj=document.signup

     if (!(validate_col('User F Name',formobj.userFirstName))) return false
     if (!(validate_col('User LastName',formobj.userLastName))) return false
     if (!(validate_col('User Address',formobj.addPriUser))) return false
     if (!(validate_col('User City',formobj.addCityUser))) return false
     if (!(validate_col('User State',formobj.addStateUser))) return false
     if (!(validate_col('User Country',formobj.addCountryUser))) return false
     if (!(validate_col('User Phone',formobj.addPhoneUser))) return false
     if (!(validate_col('User Email',formobj.addEmailUser))) return false
     if (!(validate_col('SiteName',formobj.siteName))) return false
     if (!(validate_col('Site Address',formobj.sitePerAdd))) return false
     if (!(validate_col('Site City',formobj.addCitySite))) return false
     if (!(validate_col('Site State',formobj.addStateSite))) return false
     if (!(validate_col('Site Country',formobj.addCountrySite))) return false     
     if (!(validate_col('User Question',formobj.userSecQues))) return false
     if (!(validate_col('User Answer',formobj.userAnswer))) return false
     if (!(validate_col('Login ID',formobj.userLogin))) return false     
     if (!(validate_col('User Password',formobj.userPwd))) return false
     if (!(validate_col('User Confirm Password',formobj.userConfirmPwd))) return false	
     if (!(validate_col('E-Signature',formobj.userESign))) return false
     if (!(validate_col('Confirm E-Signature',formobj.userConfirmESign))) return false

     if(formobj.addEmailUser.value.search("@") == -1) {
	 alert("<%=MC.M_EtrValid_EmailAddr%>");/*alert("Please enter a valid email address.");*****/
	 formobj.addEmailUser.focus();
	 return false;
     } else {
	var temp = new String(formobj.addEmailUser.value.substr(formobj.addEmailUser.value.search("@") + 1));
	 if (temp.indexOf(".") == -1) {
		 alert("<%=MC.M_EtrValid_EmailAddr%>");/*alert("Please enter a valid email address.");*****/
	    formobj.addEmailUser.focus();
	    return false;
	 }

     }

    if( formobj.userPwd.value.length < 8)
	{
	 alert("<%=MC.M_PwdMust8CharLong_ReEtr%>");/*alert("Password must be atleast 8 characters long. Please enter again.");*****/
	 formobj.userPwd.focus()
	 return false;
	} 

    if( formobj.userPwd.value != formobj.userConfirmPwd.value)
	{
	 alert("<%=MC.M_PwdConfirmPwd_NotSame%>");/*alert("Values in 'Password' and 'Confirm Password' are not same. Please enter again.");*****/
	 formobj.userPwd.focus()
	 return false;
	} 

    if( isNaN(formobj.userESign.value) == true)
	{
	 alert("<%=MC.M_Vldt_EsignForNume%>");/*alert("E-Signature must be numeric. Please enter again.");*****/
	 formobj.userESign.focus()
	 return false;
	}     

    if( formobj.userESign.value.length < 4)
	{
	 alert("<%=MC.M_EsignCharMust4_EtrAgain%>");/*alert("E-Signature must be atleast 4 characters long. Please enter again.");*****/
	 formobj.userESign.focus()
	 return false;
	} 

    if( formobj.userESign.value != formobj.userConfirmESign.value)
	{
	 alert("<%=MC.M_ValEsign_CnfrmEsign_Same%>");/*alert("Values in 'E-Signature' and 'Confirm E-Signature' are not same. Please enter again.");*****/
	 formobj.userESign.focus()
	 return false;
	} 

 }

function openwin() {
      window.open("","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,width=500,height=400")
;}

</SCRIPT>

<%	int userId = 0;
	String userAddresId = "";
	String dJobType = "" ;
	String dRoleType= "" ;
	String dPrimSpl= "" ;
	String dTimeZone="";
	CodeDao cd = new CodeDao();
	CodeDao cd2 = new CodeDao();
	CodeDao cd3 = new CodeDao();
	
	CodeDao cd4 = new CodeDao();
	
	cd2.getCodeValues("role_type"); 	
	cd.getCodeValues("job_type"); 
	cd3.getCodeValues("prim_sp");
	cd4.getTimeZones();
      dRoleType = cd2.toPullDown("roleType");	
      dJobType = cd.toPullDown("userCodelstJobtype");	
      dPrimSpl = cd3.toPullDown("primarySpeciality");		
	  dTimeZone=cd4.toPullDown("timeZone");	
%>


<table border="0" cellspacing="0" cellpadding="0" width="770">
<Form name="signup" method="post" action="accountsave.jsp" >

	 <!-- HEADER OBJECT START-->
	<tablecellspacing="0" cellpadding="0">
	<tr>
	  <td colspan="14"><img src="./images/toplogo.jpg" width="770" height="60" alt="<%=LC.L_Velos_Eres%><%--Velos eResearch*****--%>"></td>
	</tr>
	<tr>
	  	  <td align="right"><img src="./images/toplefcurve.gif" width="18" height="16"></td> 
          <td><img src="./images/vspacer.gif" width="201" height="16" border="0"></td> 
          <td><img src="./images/topmidcurve.gif" width="30" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="index.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_5,250,77);" ><img name="Home" src="./images/home.gif" height="16" border="0" alt="<%=LC.L_Home%><%--Home*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="ataglance.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_0,335,77);" ><img name="About" src="./images/about.gif" height="16" border="0" alt="<%=LC.L_About%><%--About*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="products.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_1,420,77);" ><img name="vpopmenu_r2_c3" src="./images/solutions.gif" height="16" border="0" alt="<%=LC.L_Solutions%><%--Solutions*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="demo.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_2,515,77);" ><img name="vpopmenu_r2_c4" src="./images/demo.gif" height="16" border="0" alt="<%=LC.L_Demo%><%--Demo*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="contact.html" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_3,597,77);" ><img name="vpopmenu_r2_c6" src="./images/contact.gif" height="16" border="0" alt="<%=LC.L_Contact%><%--Contact*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td width="85" align="center" bgcolor="9999FF"><a href="register.jsp" onMouseOut="FW_startTimeout();"  onMouseOver="window.FW_showMenu(window.fw_menu_4,683,77);" ><img name="vpopmenu_r2_c7" src="./images/register.gif" height="16" border="0" alt="<%=LC.L_Register%><%--Register*****--%>"></td>
     </tr>
	 </table>
	 <!-- HEADER OBJECT END -->


	 
	   <table width="770" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">


	   		  <tr>
		   	   <!-- LIBRARY OBJECT START-->
	  	      <td width="2" bgcolor="9999ff"><img src="./images/sspacer.gif">				  
		      </td>
		   	   <!-- LIBRARY OBJECT END-->
			  	  <td width="125" valign="top" background="./images/leftsidebg.gif">
				  <BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>
				  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="./images/registersm.gif" width="68" height="45" border="0" alt="<%=LC.L_Register%><%--Register*****--%>">
				  </td>
				  <td>

<table width="650" cellspacing="10" cellpadding="0" border="0">
  <tr>
	  <td>
	  <p>
	  <img src="./images/titleregister.gif" width="77" height="21" border="0" alt="<%=LC.L_Register%><%--Register*****--%>">
	  <BR><BR><%=MC.M_PlsCompAppr_AssignDets%><%--Please complete and submit the following registration form. Once it is approved, your account will be created and you will receive an email confirming your assigned login and password.<BR><BR> <b>Personal Details</b>*****--%>
	  <img src="./images/sspacer.gif" width="650" height="2">
	  </p>
	  </td>
  </tr>
</table>				  
<table width="650" cellspacing="0" cellpadding="0" border="0">
  <tr>
  <td width="10" rowspan="14">
  </td>
  <td>
  	  <tr> 
      	   <td>
       	   <%=LC.L_First_Name%><%--First Name*****--%> <FONT class="Mandatory">* </FONT>
    	   </td>
    	   <td>
		   <input type="text" name="userFirstName" size = 25 MAXLENGTH = 35>
   		   </td>
      	   <td>
      	   <%=LC.L_Last_Name%><%--Last Name*****--%> <FONT class="Mandatory">* </FONT>
      	   </td>
      	   <td>
	  	   <input type="text" name="userLastName" size = 25 MAXLENGTH = 35>
      	   </td>
  	  </tr>
  <tr> 
    <td>
       <%=LC.L_Phone%><%--Phone*****--%> <FONT class="Mandatory">* </FONT>
    </td>
    <td>
	<input type="text" name="addPhoneUser" size = 25 MAXLENGTH = 100>
    </td>
    <td>
       <%=LC.L_E_Mail%><%--E_Mail*****--%> <FONT class="Mandatory">* </FONT>
    </td>
    <td>
	<input type="text" name="addEmailUser" size = 25 MAXLENGTH = 100>
    </td>
  </tr>
  <tr> 
    <td>
       <A href="pricing.html" ><%=LC.L_Account_Type%><%--Account Type*****--%></a>
    </td>
    <td>
	<select name=accType>
		<option value=I><%=LC.L_Individual%><%--Individual*****--%></option>
		<option value=G><%=LC.L_Group%><%--Group*****--%></option>
		<option value=E><%=LC.L_Evaluation%><%--Evaluation*****--%></option>
	</select>
    </td>
    <td>
       <%=LC.L_Job_Type%><%--Job Type*****--%> 
    </td>
    <td>
	<%=dJobType%>  
    </td>
  </tr>		
  <tr>		
	<td>
	<%=LC.L_Time_Zone%><%--Time Zone*****--%> <FONT class="Mandatory">* </FONT> 
	</td>
	<td colspan="2"> <%=dTimeZone%> </td>
  </tr>

  </td>
  </tr>	
 </table>


<table width="650" cellspacing="0" cellpadding="0" border="0">
  <tr>
  <td width="10" rowspan="14">
  </td>
  <td>
  <tr> 
    <td colspan = "3">
	<BR><i><%=MC.M_IfInvestigator_Complete%><%--If you are an investigator, please complete the following*****--%>:</i>
    </td>
  </tr>	
  <tr> 
    <td colspan="3">
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=LC.L_Primary_Speciality%><%--Primary Speciality*****--%>
    </td>
    <td>
	<%=dPrimSpl%>  
  </td>
  </tr>	
  <tr> 
     <td colspan="3">
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=MC.M_Years_YouInClnclRes%><%--How many years have you been involved in clinical research?*****--%>
    </td>
    <td colspan="2">
	<input type="text" name="userWrkExp" size = 20 MAXLENGTH = 100>
    </td>
  </tr>	
  <tr> 
    <td colspan="3">
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=MC.M_PhaseTrials_YouInvolved%><%--Which phases of trials have you been involved with?*****--%>
    </td>
    <td colspan="2">
	<input type="text" name="userPhaseInv" size = 20 MAXLENGTH = 100>
    </td>
  </tr>
  </td>
  </tr>	
 </table>

<table width="650" cellspacing="0" cellpadding="0" border="0">
  <tr>
    <td colspan="3">
  	  <b><BR>&nbsp;&nbsp;<%=LC.L_Org_Dets%><%--Organization Details*****--%></b><BR>
	  &nbsp;&nbsp;<img src="./images/sspacer.gif" width="650" height="2">
    </td>
  </tr>
  <tr>
  <td width="40" rowspan="14">
  </td>
  <td>
  <tr> 
    <td>
       <%=LC.L_Organization_Name%><%--Organization Name*****--%> <FONT class="Mandatory">* </FONT>
    </td>
    <td>
	<input type="text" name="siteName" size = 50 MAXLENGTH = 50>
    </td>
  </tr>

  <tr> 
    <td colspan="2">
    <table cellspacing="0" cellpadding="0" border="0">
  <tr> 
    <td width="138">
       <%=LC.L_Address%><%--Address*****--%> <FONT class="Mandatory">* </FONT>
    </td>
    <td colspan="4">
	<input type="text" name="sitePerAdd" size = 50 MAXLENGTH = 50>
    </td>
  </tr>	
	<tr>
	<td>	
       <%=LC.L_City%><%--City*****--%> <FONT class="Mandatory">* </FONT>
    </td>
    <td>
	<input type="text" name="addCitySite" size = 15  MAXLENGTH = 30>
    </td>
    <td>
       &nbsp;&nbsp;&nbsp;<%=LC.L_State%><%--State*****--%> <FONT class="Mandatory">* </FONT>
    </td>
    <td>
	<input type="text" name="addStateSite" size = 10 MAXLENGTH = 30>
    </td>
    <td>
       <%=LC.L_ZipOrPostal_Code%><%--Zip/Postal Code*****--%> </FONT>
    </td>
    <td>
	<input type="text" name="addZipSite" size = 10 MAXLENGTH = 15>
    </td>
    </tr>
	    <tr>
          <td>
            <%=LC.L_Country%><%--Country*****--%> <FONT class="Mandatory">* </FONT>
          </td>
          <td>
	        <input type="text" name="addCountrySite" size = 15 MAXLENGTH = 30>
          </td>
          <td>
            &nbsp;&nbsp;&nbsp;<%=LC.L_Phone%><%--Phone*****--%>
          </td>
          <td>
	        <input type="text" name="addPhoneSite" size = 15 MAXLENGTH = 100>
          </td>
        </tr>

   </table>
  </td>
  </tr>

<table width="650" cellspacing="0" cellpadding="0" border="0">
  <tr>
    <td colspan="3">
  	  <b><BR>&nbsp;&nbsp;<%=LC.L_Login_Dets%><%--Login Details*****--%></b>
	  	  &nbsp;&nbsp;<img src="./images/sspacer.gif" width="650" height="2">
    </td>
  </tr>
  <tr>
  <td width="40" rowspan="14">
  </td>
  <td>
  <tr> 
  <tr> 
    <td>
       <%=LC.L_User_Name%><%--User Name*****--%> <FONT class="Mandatory">* </FONT>
    </td>
  </tr>
  <tr> 
    <td>
	<input type="text" name="userLogin" size = 35 MAXLENGTH = 20>
    </td>
  </tr>
  <tr> 
    <td>
       <%=LC.L_Password%><%--Password*****--%> <FONT class="Mandatory">* </FONT>
    </td>
    <td>
       <%=LC.L_Confirm_Pwd%><%--Confirm Password*****--%> <FONT class="Mandatory">* </FONT>
    </td>
   </tr>
   <tr>
    <td>
	<input type="password" name="userPwd" size = 35 MAXLENGTH = 20>
    </td>
    <td>
	<input type="password" name="userConfirmPwd" size = 35 MAXLENGTH = 20>
    </td>
  </tr>
  <tr> 
    <td>
       <%=LC.L_Esign_Numeric%><%--e-Signature (numeric)*****--%><FONT class="Mandatory">* </FONT>
    </td>
    <td>
       <%=LC.L_Confirm_Esign%><%--Confirm e-Signature*****--%> <FONT class="Mandatory">* </FONT>
    </td>
  </tr>
  <tr> 
    <td>
	<input type="password" name="userESign" size = 35 MAXLENGTH = 8>
    </td>
    <td>
	<input type="password" name="userConfirmESign" size = 35 MAXLENGTH = 8>
    </td>
  </tr>
  <tr> 
    <td>
       <%=LC.L_Secret_Que%><%--Secret Question*****--%> <FONT class="Mandatory">* </FONT>
    </td>
    <td>
       <%=LC.L_Your_Answer%><%--Your Answer*****--%>  <FONT class="Mandatory">* </FONT>
    </td>
  </tr>
  <tr> 
    <td>
	<input type="text" name="userSecQues" size = 35 MAXLENGTH = 150>
    </td>
    <td>
	<input type="text" name="userAnswer" size = 35 MAXLENGTH = 150>
    </td>
  </tr>
  <tr> 
    <td colspan="2">
	<input type="checkbox" name="accMailFlag" value = "Y" CHECKED>
	&nbsp;
       <%=MC.M_ChkVelosCont_UsefulInfo%><%--Check if you want Velos to contact you regarding useful information*****--%>
    </td>
  </tr>
  <tr> 
    <td colspan = "2">
	<input type="checkbox" name="accPubFlag" value = "Y" CHECKED>
	&nbsp;
	<%=MC.M_ChkAgreeVelos_CompInfo%><%--Check if you agree to Velos giving your name to other companies that might have useful information for you*****--%>
  </td>
  </tr>	
 </table>
 <table>
    <BR><BR><b>&nbsp;&nbsp;&nbsp;<%=LC.L_Reg_AgreementAnd%><%--Registration Agreement and*****--%> <A href="privacypolicy.html" target="Information" onClick="openwin()"><%=LC.L_Privacy_Policy%><%--Privacy Policy*****--%></A>
    <BR><BR>
    <A href="termsofservice.html" target="Information" onClick="openwin()">&nbsp;&nbsp;&nbsp;<%=LC.L_Terms_OfService%><%--Terms of Service*****--%></a></b>
    </td>
  </tr>
  <tr><td colspan="2">
	<P class = "italicsComments">&nbsp;&nbsp;&nbsp;<%=MC.M_ClkAcptBtn_Agree%><%--By clicking on the I Accept button, you agree to the*****--%> <A href="termsofservice.html" target="Information" onClick="openwin()"><%=MC.M_TermsCndnAgment_Acc%><%--terms and conditions in the Agreement</a> and your registration will<BR> &nbsp;&nbsp;&nbsp;be submitted for account creation.*****--%> 	</P>
	</td>
  </tr>
  <tr>
	<td>
	&nbsp;&nbsp;<input type="image" src="./images/iaccept.gif" border="0" onClick = "return validate()" align="absmiddle" border="0">	
	</td>
	<td>
	&nbsp;&nbsp;<a href="index.html"><img src="./images/idecline.gif" width="81" height="25" border="0" alt="<%=LC.L_I_Decline%><%--I decline*****--%>"></a>	
	</td>
	</tr>
 </table>
</form>
				  </td>
		   	   <!-- LIBRARY OBJECT START-->
	  	      <td width="2" bgcolor="9999ff"><img src="./images/sspacer.gif">				  
		      </td>
		   	   <!-- LIBRARY OBJECT END-->
			  </tr>

      </table>
	  <!-- FOOTER OBJECT START-->
	   <table width="770" cellspacing="0" cellpadding="0" border="0">
      <tr>
  		  <td align="center"><img src="./images/veloscopyright.gif" width="272" height="16"></td>
          <td><img src="./images/botmidcurve.gif" width="30" height="16" border="0"></td>
          <td bgcolor="#FFFFFF"><img src="./images/spacer.gif" width="450" height="16" border="0"></td>
	  	  <td align="right"><img src="./images/botrigcurve.gif" width="18" height="16"></td> 
      </tr>
	  </table>
	   <table width="770" cellspacing="0" cellpadding="0" border="0">
		<tr>
          <td bgcolor="9999ff"><img src="./images/sspacer.gif" width="436" height="16" border="0"></td>
		  <td align="center"><a href="termsofservice.html"><img name="Terms of service" src="./images/termsofservice.gif" width="110" height="16" border="0" alt="<%=LC.L_Terms_OfService%><%--Terms of Service*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td align="center"><a href="privacypolicy.html"><img name="Privacy policy" src="./images/privacypolicy.gif" width="110" height="16" border="0" alt="<%=LC.L_Privacy_Policy%><%--Privacy Policy*****--%>"></a></td>
          <td><img src="./images/mspacer.gif" width="2" height="16" border="0"></td>
          <td align="center"><a href="sitemap.html"><img name="Site map" src="./images/sitemap.gif" width="110" height="16" border="0" alt="<%=LC.L_Sitemap%><%--Sitemap*****--%>"></a></td>
		</tr>
        <tr>
          <td colspan="6"><img src="./images/botbar.gif" width="770" height="8" border="0"></td>
        </tr>
		</table>
		</tr>


     </table>
	 <!-- FOOTER OBJECT END-->

</table>
</body>
</html>
