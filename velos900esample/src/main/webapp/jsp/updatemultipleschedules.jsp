<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
 

</HEAD> 
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>

<jsp:useBean id="updateSchJB" scope="request"	class="com.velos.eres.web.updateSchedules.UpdateSchedulesJB" />
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.MC"%>
	
	
<%
	HttpSession tSession = request.getSession(true);
	int ret =0;
	 
	
	int patEnrollDtCheckVal =0;

	String calId1 = request.getParameter("paramprotCalId1");
	String calId2 = request.getParameter("paramprotCalId2");
	String calId3 = request.getParameter("paramprotCalId3");
	String calId4 = request.getParameter("paramprotCalId4");
	String calId5 = request.getParameter("paramprotCalId5");
	
	
	int protCalId1 = EJBUtil.stringToNum(calId1);
	int protCalId2 = EJBUtil.stringToNum(calId2);
	int protCalId3 = EJBUtil.stringToNum(calId3);
	int protCalId4 = EJBUtil.stringToNum(calId4);
	int protCalId5 = EJBUtil.stringToNum(calId5);
	
	
	String scheduleFlag ="";
	
	String optVal = request.getParameter("optVal");
	
	
	String discDate = request.getParameter("discDate");
	String discReason = request.getParameter("discReason");
	String newSchStartDate = request.getParameter("newSchStartDate");
	
	String dstatusDiscon = request.getParameter("dstatusDiscon");
	int dstatusDisconVal = EJBUtil.stringToNum(dstatusDiscon);
	String dstatusGen = request.getParameter("dstatusGen");
	int dstatusGenVal = EJBUtil.stringToNum(dstatusGen);
	
	
	String dateOccurDisc = request.getParameter("dateOccur"); 
	String allEveDate = request.getParameter("allevedate");
	String dstatusDisc = request.getParameter("dstatusDisc"); 
	int dstatusDiscVal = EJBUtil.stringToNum(dstatusDisc);
	String allEveStatDate = request.getParameter("allevestatdate"); 
	
	

	String dateOccurCurr = request.getParameter("dateOccur1");
	String alleventdate = request.getParameter("alleventdate");
	String dstatusCurr = request.getParameter("dstatusCurr");
	int dstatusCurrVal = EJBUtil.stringToNum(dstatusCurr);
	String allEventStatDate = request.getParameter("alleventstatdate");
	
	

	String patSchStartDate = request.getParameter("patschstdate");
	String patEnrollDtCheck = request.getParameter("patenrolldtcheck");
	
	if (patEnrollDtCheck != null)
		patEnrollDtCheckVal = 1;
	else
		patEnrollDtCheckVal = 0;
		

String ipAdd = (String) tSession.getValue("ipAdd");
String usr = null;
usr = (String) tSession.getValue("userId");
int iusr = EJBUtil.stringToNum(usr);
String eSign = request.getParameter("eSign");
if (sessionmaint.isValidSession(tSession))

{
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {

		
		if (optVal.equals("1")) {
			ret = updateSchJB.discShedulesAllPatient(protCalId1, discDate, discReason, protCalId2, newSchStartDate, iusr, ipAdd);
			if(!dstatusDiscon.equals(""))
			{
				scheduleFlag = "D";
				ret = updateSchJB.updateEventStatusCurrAndDisc(protCalId1, scheduleFlag, "OA", discDate, dstatusDisconVal, discDate, iusr, ipAdd );
			}
			 
			
			
			if(!dstatusGen.equals("") )
			{
				scheduleFlag = "C";
				ret = updateSchJB.updateEventStatusCurrAndDisc(protCalId2, scheduleFlag, "B", discDate, dstatusGenVal, discDate, iusr, ipAdd );
			}
			
		}
		
		
		if (optVal.equals("2")) {
			scheduleFlag = "D";
			ret = updateSchJB.updateEventStatusCurrAndDisc(protCalId3, scheduleFlag, dateOccurDisc, allEveDate, dstatusDiscVal, allEveStatDate, iusr, ipAdd );
		}
		

		if (optVal.equals("3")) {
			scheduleFlag = "C";
			ret = updateSchJB.updateEventStatusCurrAndDisc(protCalId4, scheduleFlag, dateOccurCurr, alleventdate, dstatusCurrVal, allEventStatDate, iusr, ipAdd );
		}
		
		if (optVal.equals("4")) {
			ret = updateSchJB.generateNewSchedules(protCalId5, patSchStartDate, patEnrollDtCheckVal, iusr, ipAdd);
		}
	
		if(ret==0) {
%>		
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc %><%-- Data was saved successfully*****--%> </p>
<SCRIPT>
           	setTimeout("self.close()",1000);
</SCRIPT>
<%
} else {
%>	
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_DataNotSvd_Succ %><%-- Data was not saved successfully*****--%> </p>
<SCRIPT>
           	setTimeout("self.close()",1000);
</SCRIPT>

<%
}
}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</BODY>

</HTML>





