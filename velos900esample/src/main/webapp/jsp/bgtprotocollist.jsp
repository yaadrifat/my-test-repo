<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Pcol_Cal%><%--Protocol calendars*****--%></title>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<META HTTP-EQUIV="Pragma" CONTENT="no-cache">

<META HTTP-EQUIV="Expires" CONTENT="-1">



</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<!-- <SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT> -->


<SCRIPT LANGUAGE="JavaScript">



	function validate(formobj) {



if(formobj.length.value==0)

{

return false;

}

	if (!(validate_col('e-Signature',formobj.eSign))) return false;



 		 if(isNaN(formobj.eSign.value) == true) {

			alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

			formobj.eSign.focus();

			return false;

		}


		count = formobj.noOfCheckboxes.value;

		for (i=0;i<count;i++) {

			if (count > 1) {

				if (formobj.bgtcalcheckbox[i].checked) {

					formobj.submit();

					return false;

				}

			} else {

				if (formobj.bgtcalcheckbox.checked) {

					formobj.submit();

					return false;

				}

			}

		}

		alert("<%=MC.M_Selc_Cal%>");/*alert("Please select a Calendar.");*****/

		return false;

	}



	 function fclose() {

		self.close();

	}





</SCRIPT>















<% String src="";



src= request.getParameter("srcmenu");

String selectedTab = request.getParameter("selectedTab");

%>





<%

int ienet = 2;



String agent1 = request.getHeader("USER-AGENT");

   if (agent1 != null && agent1.indexOf("MSIE") != -1)

     ienet = 0; //IE

    else

	ienet = 1;

	if(ienet == 0) {

%>

<body style="overflow:scroll;">

<%

	} else {

%>

<body>

<%

	}

%>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>

<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>

<jsp:useBean id="eventdefdao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>

<jsp:useBean id="eventassocdao" scope="request" class="com.velos.esch.business.common.EventAssocDao"/>





<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.esch.business.common.*"%>








<% String calledFrom = "";

	calledFrom = ((request.getParameter("calledFrom")) == null)?"P":request.getParameter("calledFrom");

%>

  <%



   HttpSession tSession = request.getSession(true);



   if (sessionmaint.isValidSession(tSession))
	{
 %>
<jsp:include page="sessionlogging.jsp" flush="true"/>
	 <jsp:include page="include.jsp" flush="true"/>
<%
	int accId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));

	String uName = (String) tSession.getValue("userName");

	String userIdFromSession = (String) tSession.getValue("userId");



	String studyId = request.getParameter("studyId");

	String bgtId=request.getParameter("budgetId");

	String protType=request.getParameter("protType");


	String fringeBenefit = request.getParameter("fringeBenefit");
	String fringeBenefitApply = request.getParameter("fringeBenefitApply");
	String costDiscount = request.getParameter("costDiscount");
	String costDiscountApply = request.getParameter("costDiscountApply");
	String sponsorOHead = request.getParameter("sponsorOHead");
	String sponsorOHeadApply = request.getParameter("sponsorOHeadApply");


	int len= 0;

	ArrayList eventIds=new ArrayList() ;

	ArrayList chainIds=new ArrayList() ;

	ArrayList names= new ArrayList();

	ArrayList descriptions= new ArrayList();

	ArrayList stats= new ArrayList();
	ArrayList calTypes =  new ArrayList();



	String name="";

	String description="";

	String chainId="";

	String duration="";

	String status="";

	String statusText = "";
	String calType = "";

	String calTypeDesc = "";



	int noOfCheckboxes=0;

	int pageRight= 1;

	int study = 0;



 		if (protType.equals("S")){



			ArrayList tId = new ArrayList();

			TeamDao teamDao = new TeamDao();

			teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));

			tId = teamDao.getTeamIds();

				if (tId.size() <=0)

				{

				pageRight  = 0;

				}



			}



			else



				{

				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");

				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));

				}



		String tab = request.getParameter("selectedTab");

	   	if (pageRight > 0) {



			String eventType="P";


				
				if (protType.equals("L"))

				{


//SV, 10/27/04, getHeaderlist fetches a filtered list of based on userid, cal-enh
				eventdefdao= eventdefB.getHeaderList(accId,EJBUtil.stringToNum(userIdFromSession), eventType,EJBUtil.stringToNum(bgtId));

				
				eventIds=eventdefdao.getEvent_ids() ;
				chainIds=eventdefdao.getChain_ids() ;

				names= eventdefdao.getNames();

				descriptions= eventdefdao.getDescriptions();

				//KM-DFin9
				stats = eventdefdao.getCodeDescriptions();

				len= eventIds.size();
		   	  	calTypeDesc = LC.L_Library_Calendar;/*calTypeDesc = "Library Calendar";*****/

				}else

				{

				eventassocdao= eventassocB.getStudyProts(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(bgtId));

				eventIds=eventassocdao.getEvent_ids() ;

				chainIds=eventassocdao.getChain_ids() ;

				names= eventassocdao.getNames();

				descriptions= eventassocdao.getDescriptions();

				//KM-DFin9
				stats = eventassocdao.getCodeDescriptions();

				calTypes = eventassocdao.getProtocolAssocTo();

		   	  	len= eventIds.size();

				}



  	         	int counter = 0;

				Integer id;





%>

<DIV  id="div1">


  <Form name="bgtprotocol" id="bgtprotlist" method="post" action="savebgtcal.jsp" onsubmit="if (validate(document.bgtprotocol)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">



    <input type="hidden" name="srcmenu" value='<%=src%>'>

    <input type="hidden" name="selectedTab" value='<%=tab%>'>

    <input type="hidden" name="bgtId" value='<%=bgtId%>'>

	<input type="hidden" name="protType" value='<%=protType%>'>

	<input type="hidden" name="length" value='<%=len%>'>



	<input type="hidden" name="fringeBenefit" value='<%=fringeBenefit%>'>
	<input type="hidden" name="fringeBenefitApply" value='<%=fringeBenefitApply%>'>
	<input type="hidden" name="costDiscount" value='<%=costDiscount%>'>
	<input type="hidden" name="costDiscountApply" value='<%=costDiscountApply%>'>
	<input type="hidden" name="sponsorOHead" value='<%=sponsorOHead%>'>
	<input type="hidden" name="sponsorOHeadApply" value='<%=sponsorOHeadApply%>'>






<%   if(len==0) { %>

		 	<% if(protType.equals("L")) {%>

	        <P class = "defComments"><%=MC.M_NoCalInLib_AvalCal%><%--There is no calendar in the library or all the available calendars have already been selected.*****--%></P>

			<% } else{ %>

        	<P class = "defComments"><%=MC.M_NoCalInSel_StdCalAldy%><%--There is no calendar in the  selected <%=LC.Std_Study_Lower%> or all the available calendars have already been selected.*****--%></P>

			<%}%>

			  <table width="100%" cellspacing="0" cellpadding="0" border="0">

				<tr>

		      <td align=center>

			  <button onClick = "self.close()"><%=LC.L_Close%></button>

		      </td>

		      </tr>

			  </table>

	 <% } else {%>


   <table width="100%" cellspacing="0" cellpadding="0" border="0" >

    <tr >

      <td width = "99%">

		<% if(protType.equals("L")) {%>

        <P class = "defComments"><%=MC.M_PcolCalCurrListed_AddBgt%><%--The following are the Protocol Calendars currently listed in your Library. Select the Calendar that you wish to add to your budget.*****--%></P>

		<% } else{ %>

        <P class = "defComments"><%=MC.M_CalAssocStd_SelCalAddBgt%><%--The following are the Protocol Calendars Associated to the <%=LC.Std_Study%>. Select the Calendar that you wish to add to your budget.*****--%></P>

		<%}%>



      </td>

    </tr>

   </table>


  <br><br>

    <jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="bgtprotlist"/>
		<jsp:param name="showDiscard" value="Y"/>
  </jsp:include>









    <table  width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl" >

       <tr>

        	<th width="10%"> <%=LC.L_Select%><%--Select*****--%> </th>

	        <th width="40%"> <%=LC.L_Name%><%--Name*****--%> </th>

	        <th width="20%"> <%=LC.L_Description%><%--Description*****--%> </th>

	        <th width="10%"><%=LC.L_Status%><%--Status*****--%> </th>
	        <th width="20%"><%=LC.L_Cal_Type%><%--Calendar Type*****--%> </th>

      </tr>

       <%



    for(counter = 0;counter<len;counter++)

	{



		id = (Integer)eventIds.get(counter);

		name=((names.get(counter)) == null)?"-":(names.get(counter)).toString();

		//description=((descriptions.get(counter)) == null)?"-":(descriptions.get(counter)).toString();

		//KM-#4516
		if (descriptions.get(counter) == null)
			description = "-";
		else if(descriptions.get(counter).toString().equals("null"))
			description = "-";
		else
			description = descriptions.get(counter).toString();

		chainId=((chainIds.get(counter)) == null)?"-":(chainIds.get(counter)).toString();

		statusText = ((stats.get(counter)) == null)?"-":(stats.get(counter)).toString();

		if(! protType.equals("L"))
		{
		  	calType = (String) calTypes.get(counter);

		  	if(! StringUtil.isEmpty(calType))
		  	{
		  		if (calType.equals("S"))
		  		{
		  			calTypeDesc = LC.L_Admin_Calendar;/*calTypeDesc = "Admin Calendar";*****/
		  		}
		  		else
		  		{
		  			calTypeDesc = LC.L_Study_Calendar+" ";/*calTypeDesc = LC.Std_Study+" Calendar ";*****/
		  		}
		  	}else
		  	{
		  			calTypeDesc = LC.L_Study_Calendar+" ";/*calTypeDesc = LC.Std_Study + " Calendar ";*****/
		  	}

		}


		//KM-DFin9
		if (statusText.equals("-"))
			statusText = LC.L_None;/*statusText = "None";*****/


		if ((counter%2)==0) {



  %>

      <tr class="browserEvenRow">

        <%



		}



		else{



  %>

      <tr class="browserOddRow">

        <%



		}



  %>

	     <td align=center><INPUT name=bgtcalcheckbox type=checkbox value=<%=id%> ></td>



	    <td> <%=name%> </td>

        <td> <%=description%> </td>

        <td> <%=statusText%> </td>
        <td><%=calTypeDesc%></td>

	    </tr>

      <%

	noOfCheckboxes++;

		}



%>

    </table>







  	<input type=hidden name=noOfCheckboxes value=<%=noOfCheckboxes%>>


  </Form>


  	 <table width="100%">

<tr>

<td align=center width="50%">

<!--<A onClick = "self.close()" href="#"><input type="image" src="../images/jpg/Close.gif" border="0" align="absmiddle"></A> -->

</td>

</tr>

</table>


  <%

  } // end of length check

  }else{%>

  <jsp:include page="accessdenied.jsp" flush="true"/>

  <%}





	} //end of if session times out



else



{



%>

  <jsp:include page="timeout_childwindow.jsp" flush="true"/>

  <%



} //end of else body for page right



%>

</div>
	<div class ="mainMenu" id = "emenu"> </div>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
	</body>





</html>

























