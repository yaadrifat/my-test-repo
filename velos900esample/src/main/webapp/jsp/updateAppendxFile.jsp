<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>


</HEAD>

<BODY>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
<%
  String upld;

	Configuration.readAppendixParam(Configuration.ERES_HOME + "eresearch.xml");

upld=Configuration.UPLOADSERVLET;


	String src = null;
	src=request.getParameter("src");
	String tab = request.getParameter("selectedTab");
	String eSign = request.getParameter("eSign");
	//String fileName = " "; 
	String fileDesc = request.getParameter("desc");
	String pubFlag = request.getParameter("pubflag");
	HttpSession tSession = request.getSession(true);
%>
 <form name=upload METHOD=POST ENCTYPE=multipart/form-data>
<%if (sessionmaint.isValidSession(tSession))
{
	String ipAdd = (String) tSession.getValue("ipAdd");
	String oldESign = (String) tSession.getValue("eSign");
	String accId = (String) tSession.getValue("accountId"); 
	String sessStudyId = (String) tSession.getValue("studyId");
	String studyNo = (String) tSession.getValue("studyNo");
	int stId=EJBUtil.stringToNum(sessStudyId);
	
	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
	} else {%>
	<input type="hidden" name="uploadsrv" value="<%=upld%>">
	<input type="hidden" name="srcmenu" value="<%=src%>">
	<input type="hidden" name="selectedtab" value="<%=tab%>">
  	<input type="hidden" name="accId" value=<%=accId%>>
	<input type=hidden name=desc value="<%=fileDesc%>">
	<input type="hidden" name="pubflag" value="<%=pubFlag%>">
    <input type="hidden" name="type" value='file'>
    <input type="hidden" name="study" value=<%=stId%>>
    <input type="hidden" name="studyId" value=<%=stId%>>
	<SCRIPT language="Javascript1.2">
		upload.action = document.upload.uploadsrv.value;
	
		//upload.submit(); 
	</SCRIPT>
<%	
}//end of if for eSign check
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>
</form>
</BODY>

</HTML>





