<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--JM: 12Dec2006-->
<!--<title>Section Delete</title>-->
<title><%=LC.L_Cal_Del%><%--Calendar Delete*****--%></title>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.EJBUtil"%>
<%@ page import="com.velos.eres.service.util.LC,com.aithent.audittrail.reports.AuditUtils"%><%@page import="com.velos.eres.service.util.MC"%>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<BODY>
<br>
<%
//JM: 12Dec2006
//String from = "calendar";


String from = request.getParameter("from");
%>

<DIV class="BrowserTopn" id="div1">
  <jsp:include page="studytabs.jsp" flush="true">
  <jsp:param name="from" value="<%=from%>"/>
  </jsp:include>
</DIV>
<DIV class="BrowserBotN BrowserBotN_S_1" id="div1">
<%
	String calendarId= "";
	String selectedTab="";
	String calAssoc= request.getParameter("calassoc");
	calAssoc=(calAssoc==null)?"":calAssoc;
HttpSession tSession = request.getSession(true);
 if (sessionmaint.isValidSession(tSession))	{
		calendarId= request.getParameter("calendarId");
		selectedTab=request.getParameter("selectedTab");
		int studyId = EJBUtil.stringToNum((String) tSession.getValue("studyId"));
		int ret=0;

		String delMode=request.getParameter("delMode");

		if (delMode.equals("null")) {
			delMode="final";


%>

	<FORM name="studycalendardelete" id="stdcaldelfrm" method="post" action="studycalendardelete.jsp" onSubmit="return validate(document.studycalendardelete)">

	<br><br>





	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>

		</table>

	<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="stdcaldelfrm"/>
		<jsp:param name="showDiscard" value="N"/>
	</jsp:include>


 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="calendarId" value="<%=calendarId%>">
   	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
   	 <input type="hidden" name="calassoc" value="<%=calAssoc%>">


	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
 		  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			} else {
				//Modified for INF-18183 ::: Ankit
				ret = eventassocB.studyCalendarDelete(studyId,EJBUtil.stringToNum(calendarId),AuditUtils.createArgs(session,"",LC.L_Study));/*ret = eventassocB.studyCalendarDelete(studyId,EJBUtil.stringToNum(calendarId),AuditUtils.createArgs(session,"",LC.Std_Study));*****/
			if (ret==-2) {%>
				<br><br> <p class = "successfulmsg" align = center> <%=MC.M_SelCalCntDel_PatEnrl%><%--Selected Calendar cannot be deleted. <%=LC.Pat_Patients%> are enrolled in this calendar.*****--%></p>
				<P align="center"><button onclick="window.history.go(-2);"><%=LC.L_Back%></button></P>
			<%}else if (ret==-3) { %>
				<br><br><br><br> <p class = "successfulmsg" align = center><%=MC.M_SelCalCntDel_BgtLnk%><%--Selected Calendar cannot be deleted. Budget is linked with the calendar.*****--%></p>
				<P align="center"><button onclick="window.history.go(-2);"><%=LC.L_Back%></button></P>
				<%}  else if (ret==-4) { %>
				<br><br><br><br> <p class = "successfulmsg" align = center><%=MC.M_SelCalCntDel_MstoneLnk%><%--Selected Calendar cannot be deleted. Milestone is linked with the calendar.*****--%></p>
				<P align="center"><button onclick="window.history.go(-2);"><%=LC.L_Back%></button></P>
			 <%}else { %>
				<br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_DelSucc%><%--Data Deleted successfully*****--%> </p>
				<% if (calAssoc.equals("S")){ %>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=studyadmincal.jsp?mode=M&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>">
				<%} else{ %>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=studyprotocols.jsp?mode=M&srcmenu=<%=src%>&selectedTab=<%=selectedTab%>">

			<%}}

			} //end esign
	} //end of delMode
  }//end of if body for session
else { %>
 <jsp:include page="timeout.html" flush="true"/>
 <% } %>


<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>


</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</body>
</HTML>


