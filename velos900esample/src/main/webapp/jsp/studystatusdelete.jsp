<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@page import="com.aithent.audittrail.reports.AuditUtils,com.velos.eres.service.util.LC"%>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><%=LC.L_Std_StatusDel%><%--Study Status Delete*****--%></title>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
<SCRIPT>
function  validate(formobj){
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<%@ page language = "java" import="java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.business.common.StudyStatusDao,com.velos.eres.service.util.*"%>
<jsp:useBean id="studyStatB" scope="request" class="com.velos.eres.web.studyStatus.StudyStatusJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/> 
<jsp:useBean id="studyB" scope="request" class="com.velos.eres.web.study.StudyJB"/>  <!--KM-->


<% String src;
src= request.getParameter("srcmenu");

//JM:
String from = request.getParameter("from");
%>

<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>  

<BODY> 
<br>

<DIV class="BrowserTopn" id="div1">
  <jsp:include page="studytabs.jsp" flush="true">
   <jsp:param name="from" value="<%=from%>"/> 
  </jsp:include>
</div>
<DIV class="BrowserBotN BrowserBotN_S_1" id="div2">
<% 
	String studyStatusId= "";
	String selectedTab="";
	int userId = 0;


HttpSession tSession = request.getSession(true); 

 if (sessionmaint.isValidSession(tSession))	{ 	
		
		studyStatusId= request.getParameter("studyStatusId");
		selectedTab=request.getParameter("selectedTab");
		userId = EJBUtil.stringToNum((String)tSession.getValue("userId"));

		String selOrgId = request.getParameter("selOrgId");
		
		int studyId = EJBUtil.stringToNum(request.getParameter("studyId"));	

		//KM-07Aug08
		String  subType = request.getParameter("subType");
			
		int ret=0;
		String delMode=request.getParameter("delMode");
	
		if (delMode.equals("null")) {
			delMode="final";
%>
		
	<FORM name="studystatusdelete" id="stdstatdel" method="post" action="studystatusdelete.jsp" onSubmit="if (validate(document.studystatusdelete)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

	<br><br>
	
	<TABLE width="98%" cellspacing="0" cellpadding="0" >
		<tr><td align="center">
		<b><%=MC.M_PlsEtrEsign_Del%><%--Please enter e-Signature to proceed with deletion*****--%> </b>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	
		</table>
			

	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="stdstatdel"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>			

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
  	 <input type="hidden" name="srcmenu" value="<%=src%>">
  	 <input type="hidden" name="studyStatusId" value="<%=studyStatusId%>">
   	 <input type="hidden" name="selectedTab" value="<%=selectedTab%>">
   	 <input type="hidden" name="studyId" value="<%=studyId%>">
   	 <input type="hidden" name="selOrgId" value="<%=selOrgId%>">
     <input type="hidden" name="subType" value="<%=subType%>">

   	 	 
   	 	 

	</FORM>
<%
	} else {
			String eSign = request.getParameter("eSign");	
			String oldESign = (String) tSession.getValue("eSign");
		
			if(!oldESign.equals(eSign)) {
%>
 		  <jsp:include page="incorrectesign.jsp" flush="true"/>	
<%
			} else {

			 
			
			studyStatB.setId(EJBUtil.stringToNum(studyStatusId));
			studyStatB.getStudyStatusDetails();
			studyStatB.setModifiedBy(String.valueOf(userId));
			studyStatB.updateStudyStatus();
			//Modified for INF-18183 ::: Akshi
			ret = studyStatB.studyStatusDelete(EJBUtil.stringToNum(studyStatusId),AuditUtils.createArgs(session,"",LC.L_Study));     //Akshi: Fixed for Bug #7604
			StudyStatusDao ssd=  new StudyStatusDao();
			
			ssd.updateStartEndDate(studyId,userId); 
			 
			if (ret==-2) {%>
				<br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_NotRemSucc%><%--Data not removed successfully*****--%> </p>			
			<%}else { %>
				<br><br><br><br><br> <p class = "successfulmsg" align = center> <%=MC.M_Data_RemSucc%><%--Data removed successfully*****--%> </p>
				<META HTTP-EQUIV=Refresh CONTENT="1; URL=studystatusbrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&selsite=<%=selOrgId%>">				
			<%}

			//KM-Added on 07Aug08
			if(subType.equals("prmnt_cls")) {
				
				studyB.setId(studyId);
				studyB.getStudyDetails();
				studyB.setStudyEndDate("");
				studyB.updateStudy();	
			}

			} //end esign
	} //end of delMode	
  }//end of if body for session 
else { %>
 <jsp:include page="timeout.html" flush="true"/> 
 <% } %>
 
 <div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
  
</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/> 
</div>

</body>
</HTML>


