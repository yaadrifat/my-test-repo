<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.service.util.LC,com.velos.eres.service.util.MC" %>
<table>
	<tr>
		<td width="50%" valign="top">
			<table>
				<tr>
					<td align="right" width="30%"><%=LC.CTRP_DraftLeadOrgId%></td>
					<td width="1%">&nbsp;&nbsp;</td>
					<td align="left">
						<s:textfield name="ctrpDraftJB.leadOrgCtrpId" id="ctrpDraftJB.leadOrgCtrpId" size="50" maxsize="50" />
					</td>
				</tr>
				<tr>
					<td>
						<span id="ctrpDraftJB.leadOrgCtrpId_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td valign="top">
						<FONT id="lead_org_ctrp_id_asterisk" class="Mandatory">* </FONT>
					</td>
					<td align="left">
						<FONT id="lead_org_ctrp_id_mandatory" class="Black">
						<%=MC.CTRP_LeadOrgEnterOrSelect%>
						</FONT>
					</td>
				</tr>
				<tr>
					<td align="right">
						<a href="javascript:openSelectOrg(constleadOrg);" ><%=LC.L_Select %></a>
						<s:hidden name="ctrpDraftJB.leadOrgFkSite" id="ctrpDraftJB.leadOrgFkSite" />
						<s:hidden name="ctrpDraftJB.leadOrgFkAdd" id="ctrpDraftJB.leadOrgFkAdd" />
					</td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Name%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.leadOrgName' id='ctrpDraftJB.leadOrgName' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Street_Address%>
					<span id="ctrpDraftJB.leadOrgStreet_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.leadOrgStreet' id='ctrpDraftJB.leadOrgStreet' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_City%>
					<span id="ctrpDraftJB.leadOrgCity_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.leadOrgCity' id='ctrpDraftJB.leadOrgCity' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.CTRP_DraftStateProvince%>
					<span id="ctrpDraftJB.leadOrgState_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.leadOrgState' id='ctrpDraftJB.leadOrgState' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Zip_Code%>
					<span id="ctrpDraftJB.leadOrgZip_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.leadOrgZip' id='ctrpDraftJB.leadOrgZip' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Country%>
					<span id="ctrpDraftJB.leadOrgCountry_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.leadOrgCountry' id='ctrpDraftJB.leadOrgCountry' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Email_Addr%><%--Modified for Bug 8360 :Yogendra Pratap Singh --%>
					<span id="ctrpDraftJB.leadOrgEmail_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.leadOrgEmail' id='ctrpDraftJB.leadOrgEmail' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Phone%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.leadOrgPhone' id='ctrpDraftJB.leadOrgPhone' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_TTY%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.leadOrgTty' id='ctrpDraftJB.leadOrgTty' type="text" size="50" maxsize="100" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Fax%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.leadOrgFax' id='ctrpDraftJB.leadOrgFax' type="text" size="50" maxsize="100" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Url_Upper%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.leadOrgUrl' id='ctrpDraftJB.leadOrgUrl' type="text" size="50" maxsize="100" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Type%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left">
						<s:property escape="false" value="ctrpDraftJB.leadOrgTypeMenu" />
					</td>
				</tr>
			</table>
		</td>
		<td width="50%" valign="top">
			<table>
				<tr>
					<td align="right" width="30%"><%=LC.CTRP_PrinInvID%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.piId' id='ctrpDraftJB.piId' type="text" size="50" maxsize="50" /></td>
				</tr>
				<tr>
					<td>
						<span id="ctrpDraftJB.piId_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td valign="top">
					<FONT id="pi_id_asterisk" class="Mandatory">* </FONT>
					</td>
					<td>
						<FONT id="pi_id_mandatory" class="Black">
						<%=MC.CTRP_PiEnterOrSelect%>
						</FONT>
					</td>
				</tr>
				<tr>
					<td align="right">
						<a href="javascript:openSelectUser(constPrinv);" ><%=LC.L_Select %></a>
						<s:hidden name="ctrpDraftJB.piFkUser" id="ctrpDraftJB.piFkUser" />
						<s:hidden name="ctrpDraftJB.piFkAdd" id="ctrpDraftJB.piFkAdd" />
					</td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_First_Name%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.piFirstName' id='ctrpDraftJB.piFirstName' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Middle_Name%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.piMiddleName' id='ctrpDraftJB.piMiddleName' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Last_Name%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.piLastName' id='ctrpDraftJB.piLastName' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Street_Address%>
					<span id="ctrpDraftJB.piStreetAddress_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.piStreetAddress' id='ctrpDraftJB.piStreetAddress' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_City%>
					<span id="ctrpDraftJB.piCity_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.piCity' id='ctrpDraftJB.piCity' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.CTRP_DraftStateProvince%>
					<span id="ctrpDraftJB.piState_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.piState' id='ctrpDraftJB.piState' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Zip_Code%>
					<span id="ctrpDraftJB.piZip_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.piZip' id='ctrpDraftJB.piZip' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Country%>
					<span id="ctrpDraftJB.piCountry_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.piCountry' id='ctrpDraftJB.piCountry' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Email_Addr%><%--Modified for Bug 8360 :Yogendra Pratap Singh --%>
					<span id="ctrpDraftJB.piEmail_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.piEmail' id='ctrpDraftJB.piEmail' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Phone%>
					<span id="ctrpDraftJB.piPhone_error" class="errorMessage" style="display:none;"></span>
					</td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.piPhone' id='ctrpDraftJB.piPhone' type="text" size="50" cssClass="readonly-input" readonly="true" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_TTY%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.piTty' id='ctrpDraftJB.piTty' type="text" size="50" maxsize="100" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Fax%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.piFax' id='ctrpDraftJB.piFax' type="text" size="50" maxsize="100" /></td>
				</tr>
				<tr>
					<td align="right"><%=LC.L_Url_Upper%></td>
					<td>&nbsp;&nbsp;</td>
					<td align="left"><s:textfield name='ctrpDraftJB.piUrl' id='ctrpDraftJB.piUrl' type="text" size="50" maxsize="100" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
