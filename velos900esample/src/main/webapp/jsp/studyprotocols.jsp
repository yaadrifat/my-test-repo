<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Study_Setup%><%--Study >> Setup*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page import="com.velos.eres.service.util.*"%>

</head>

<script type="text/javascript">
window.name="studysetup";
function f_delete(name, count, studytxarmid,pageRight)
{

 if (f_check_perm(pageRight,'E')) {
  if(count > 0){
  var paramArray = [name];
  alert(getLocalizedMessageString("M_TmentArmAldy_PatCntDel",paramArray));/*alert("Treatment Arm '" +name+ "' is already used for a <%=LC.Pat_Patient_Lower%>. You cannot delete it.");*****/
  return false;
  }
  else if (confirm("<%=MC.M_DoYouWant_DelStdTreatArm%>"))/*if (confirm("Do you want to delete this treatment arm from the <%=LC.Std_Study_Lower%>?"))*****/
	   {
     windowName= window.open("studytxarmdelete.jsp?studytxArmId="+studytxarmid,'studytxtdelete',"toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,top=150, left=200,width=650,height=350");
	 windowName.focus();
  }
  }else
  {
  return false;
  }
}


function openFormPreview(formId)
{
	windowName=window.open("formpreview.jsp?formId="+formId,"Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=825,height=600,top=90,left=150");
	windowName.focus();
}


function saveLib(url, calendarRight)
{
	if (f_check_perm(calendarRight,'N')) {
	    url=url+"&mode=initial" ;
		windowName=window.open(url,"SaveToLibrary","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,top=150, left=200,width=750,height=400");
		windowName.focus();
	return true;
	} else {
	  return false;
	}

}

//Added by Manimaran for Enhancement F4.
function saveFormLib(url, formlibRight,migrateStatPK, statCodeId )
{

	//KM-#2888.
	if(isNotMigrateStatus(migrateStatPK, statCodeId) == false)
		return false;

	if (f_check_perm(formlibRight,'N')) {
		windowName=window.open(url,"SaveToLibrary","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,top=150, left=200,width=650,height=330");
		windowName.focus();
	return true;
	} else {
	  return false;
	}

}



function opentrtarm(studyId,txId,mode,txSize,pageRight)
{
	if (mode=="N") {
	if (f_check_perm(pageRight,'N')) {
	windowname=window.open("studytrtarm.jsp?studyId=" + studyId+ "&stdTXArmId=" + txId+ "&mode="+mode+"&pageRight="+pageRight ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=650,height=500 top=120,left=200, ");
	windowname.focus();
	}else {
	return false;
	}
	}else {

	windowname=window.open("studytrtarm.jsp?studyId=" + studyId+ "&stdTXArmId=" + txId+ "&mode="+mode+"&pageRight="+pageRight ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=650,height=500 top=120,left=200, ");
	windowname.focus();

}

}

function openwindowsettings(studyid, pageRight)
{

	//JM: 16Sep2010, #5290
	//if (f_check_perm(pageRight,'E') == false) {
			 //return false;
	//}

	windowname=window.open("studyDictSettings.jsp?studyid=" + studyid+ "&pagemode=initial" ,"Information","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=650,height=600 top=120,left=200, ");
	windowname.focus();

}

//KM:The width is changed for enhancement #c1 and Bug#2629 on 07July06.
function openwindowcalendar(pageRight,src)
{
    if (f_check_perm(pageRight,'N')) {
//windowname=window.open("protocollist.jsp?mode=N&srcmenu="+src+"&selectedTab=7&calledFrom=S" ,"protocollist","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=820,height=350 top=120,left=150, ");
		windowname=window.open("protocollist.jsp?mode=N&srcmenu="+src+"&selectedTab=7&calledFrom=S&calassoc=P" ,"protocollist","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=820,height=350 top=120,left=150, ");
		windowname.focus();
		return true;
	} else {
	  return false;
	}
}

function openwindowcopy(pageRight,src,tab)
{
    if (f_check_perm(pageRight,'N')) {
		windowname=window.open("copystudyprotocol.jsp?srcmenu="+src+"&selectedTab="+tab+"&from=initial&calassoc=P" ,"copystudyprotocol","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=700,height=350 top=120,left=150, ");
		windowname.focus();
		return true;
	} else {
	  return false;
	}
}

//KM-03/04/2008(Dartmouth)
function updateMultSchedules(pageRight,src)
{
    if (f_check_perm(pageRight,'N')) {
		windowname=window.open("updatemultschedules.jsp?srcmenu="+src+"&from=initial" ,"updatemultschedules","toolbar=no,resizable=yes,scrollbars=yes,menubar=no,status=yes,width=900,height=650 top=120,left=150, ");
		windowname.focus();
		return true;
	} else {
	  return false;
	}
}

//(Dartmouth)




function confirmBox(cal,pgRight) {
	if (f_check_perm(pgRight,'E') == true) {
		var paramArray = [cal];
		msg=getLocalizedMessageString("M_DelPcol_Cal",paramArray);/*msg="Delete Protocol Calendar '" + cal+ "'?";*****/
		if (confirm(msg)) {
    		return true;
		}
		else{
			return false;
		}
	} else {
		return false;
	}
}
function check(cal,pgRight,lfDataCnt) {
	if (f_check_perm(pgRight,'E') == true) {
	  if(lfDataCnt > 0)
		{
			alert("<%=MC.M_FrmAns_CntRem%>");/*alert("Form has been answered and cannot be removed.");*****/
			return false;
		}
		var paramArray = [cal];
		msg=getLocalizedMessageString("M_DelFrm_Std",paramArray);/*msg="Delete Form  '" + cal+ "'  from <%=LC.L_Study%>?";*****/
		if (confirm(msg)) {
    		return true;
		}
		else{
			return false;
		}
	} else {
		return false;
	}
}

function openwin1(fid,pkLf,pageRight) {
	if(pageRight >= 4)
	{
		windowName=window.open("formstatushistory.jsp?linkFrom=S&formId="+fid+"&linkedFormId="+pkLf,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=300,top=200 , left=200")
	    windowName.focus();
	}


}
function openwin2(study,fright) {

	if(f_check_perm(fright,'N')== true){
    windowName=window.open("linkformstostudyacc.jsp?lnkStudyId="+study+"&linkFrom=S","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=830,height=500, top =100 , left = 100")
	windowName.focus();}
	else
	{ return false;}
}

function openCopyWin(study,fright) {

	if(f_check_perm(fright,'N')== true){
    windowName=window.open("copyFormForStudy.jsp?studyId="+study,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=680,height=500, top = 100 , left = 100")
	windowName.focus();}
	else
	{return false;}
}

function openwin3(pkLf,statCodeId,activeId)
{
	param1="attributesForStudyForms.jsp?linkFrom=S&linkedFormId="+pkLf+"&statCodeId="+statCodeId+"&activeId="+activeId ;
    windowName=window.open(param1,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=650,height=500, top=200 , left=200")
	windowName.focus();
}


function openhideseq(study,fright) {

	if(f_check_perm(fright,'E')== true){
	  windowName=window.open("formhideseq.jsp?studyId="+study+"&calledFrom=S","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=680,height=500, top = 100 , left = 100");
	  windowName.focus();}
	else
	{
	return false;
	}
}



function isNotMigrateStatus(migrateStatPK, statCodeId)
{

	if (migrateStatPK == statCodeId )
	{
		alert("<%=MC.M_AppMigrating_FrmResps%>");/*alert("The application is migrating the form's responses to the latest Version. The form cannot be edited at this point.");*****/
		return false;
	}
	else
	{
		return true;
	}
}

</script>

<% String src="";
 HttpSession tSession = request.getSession(true);
src= request.getParameter("srcmenu");
String from = "calendar";
String tab = request.getParameter("selectedTab");

	String studyIdForTabs = "";
 	studyIdForTabs = request.getParameter("studyId");
 	String calFocus=(String)request.getParameter("calFocus");


%>


<jsp:include page="panel.jsp" flush="true">
	<jsp:param name="src" value="<%=src%>" />
</jsp:include>
<script>
function getfocus(calFocus) //Ak:Fixed BUG#6922
{
	if(calFocus!=null && calFocus=='true'){
		 var element = document.getElementById("calDisplay");
		 element.scrollIntoView(true);
	}
}
window.onload =function(){ //Ak:Fixed BUG#6922
		 getfocus('<%=calFocus%>');
};

</script>
<body >
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_shadow.js"><!-- overLIB (c) Erik Bosrup --></script>
<script language="JavaScript" src="overlib_hideform.js"><!-- overLIB (c) Erik Bosrup --></script>

<jsp:useBean id="sessionmaint" scope="session"	class="com.velos.eres.service.util.SessionMaint" />
<jsp:useBean id="eventassocB" scope="request"	class="com.velos.esch.web.eventassoc.EventAssocJB" />
<jsp:useBean id="ctrldao" scope="request"	class="com.velos.esch.business.common.EventAssocDao" />
<jsp:useBean id="linkedformsdao" scope="request"	class="com.velos.eres.business.common.LinkedFormsDao" />
<jsp:useBean id="linkedformsB" scope="request"	class="com.velos.eres.web.linkedForms.LinkedFormsJB" />
<jsp:useBean id="studyB" scope="request"	class="com.velos.eres.web.study.StudyJB" />
<jsp:useBean id="commonB" scope="request"	class="com.velos.eres.web.common.CommonJB" />
<jsp:useBean id="acmod" scope="request"	class="com.velos.eres.business.common.CtrlDao" />
<jsp:useBean id="stdTXArmJB" scope="request"	class="com.velos.eres.web.studyTXArm.StudyTXArmJB" />
<%@ page language="java"	import="com.velos.esch.business.common.SchCodeDao,com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.StringUtil,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.web.grpRights.GrpRightsJB"%>

<DIV class="BrowserTopn" id="divTab">
	<jsp:include page="studytabs.jsp"	flush="true">
	<jsp:param name="from" value="<%=from%>" />
	<jsp:param name="selectedTab" value="<%=tab%>" />
	<jsp:param name="studyId" value="<%=studyIdForTabs%>" />
</jsp:include></DIV>
<%
String stid= (String)tSession.getAttribute("studyId");
if(stid==null || stid==""){%>
   <DIV class="BrowserBotN BrowserBotN_S_1" id="div1">
<%} else {%>
<DIV class="BrowserBotN BrowserBotN_S_3" id="div1"><%
}%>
<%
   if (sessionmaint.isValidSession(tSession))

	{

	String study = (String) tSession.getValue("studyId");
    int studyId = EJBUtil.stringToNum(study);

	String accountId = (String) tSession.getValue("accountId");

	//reset session attribute
	tSession.setAttribute("newduration", "");
	tSession.setAttribute("protocolname", "");


%> <%

  	   	int counter = 0;
		int len=0;
    	Integer id,formId,formLibId;
	    int pageRight= 0;
	    int formRights = 0; // for the associated form browser
	    int calendarRight = 0;
	    int formRightLibrary = 0;
		int activeId = 0;
		CodeDao cd = new CodeDao();
		activeId=cd.getCodeId("frmstat","A");

		int migrateStatPK = 0;
		 migrateStatPK  = cd.getCodeId("frmstat","M");
		 int formStatusInt = 0;

		//Added by Gopu to fix the Bugzilla Issue #2359
		String calledFrom = "";
		calledFrom = ((request.getParameter("calledFrom")) == null)?"P":request.getParameter("calledFrom");
		//
		%>

		<%
	   if(study == "" || study == null) {
	%> <jsp:include page="studyDoesNotExist.jsp" flush="true" /> <%

	   } else {

			studyB.setId(studyId);
			studyB.getStudyDetails();
		  // int studyId = EJBUtil.stringToNum(study);

		  // String tab = request.getParameter("selectedTab");

   	   	   StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");

	  	   if ((stdRights.getFtrRights().size()) == 0){

		 	pageRight= 0;

		   }else{

			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYCAL"));
	   	   }

		   /// entered for having rights for study linked forms

		   StudyRightsJB studyFormRights =(StudyRightsJB) tSession.getValue("studyRights");

	  	   if ((studyFormRights.getFtrRights().size()) == 0){
		 	formRights= 0;
		   }else{
           		formRights = Integer.parseInt(studyFormRights.getFtrRightsByValue("STUDYFRMMANAG"));
	   	   }

	  	   //IA 11.06.2006 Bug 2747

			GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
    		calendarRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
    		formRightLibrary = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
    	//end added


	   	if ((pageRight > 0 ) || (formRights >0))  {


			if( pageRight >= 4){
			ctrldao= eventassocB.getStudyProts(studyId);

			ArrayList eventIds=ctrldao.getEvent_ids() ;
			ArrayList chainIds=ctrldao.getChain_ids() ;
			ArrayList names= ctrldao.getNames();
			ArrayList descriptions= ctrldao.getDescriptions();
			ArrayList eventTypes= ctrldao.getEvent_types();
			ArrayList durations= ctrldao.getDurations();
			
			//JM: 04Feb2011, #D-FIN9
			//ArrayList stats= ctrldao.getStatus();
			
			ArrayList codeSubTypes= ctrldao.getCodeSubTypes();			
			ArrayList codeDescs= ctrldao.getCodeDescriptions();

	   	  	len= eventIds.size();


			 //len = 1;

			String eventType = "";
			String name="";
			String description="";
			String chainId="";
			String duration="";
			String status="";
			String statusText="";
%> 
<p class="sectionheadings lhsFont"><%=LC.L_StdDictOrSet%><%--<%=LC.L_Study%> Dictionaries/Settings*****--%></p>

<table width="99%" cellspacing="0" cellpadding="0" border="0" >
	<tr>
		<td width="70%" class="defComments lhsFont">
		<%=MC.M_IfNoMdf_DefSettApplied%><%--If no modifications have been specified, the default settings will be applied.*****--%>
		</td>
		<td width="30%"><A href="#"
			onClick="openwindowsettings(<%=studyId%>, <%=pageRight%>);"><%=MC.M_ViewOrEditDict_Set_Upper%><%--VIEW/EDIT DICTIONARIES AND SETTINGS*****--%></A></td>
		<td></td>
	</tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0"  class="outline midAlign">
	<tr>
		<th width="50%"><%=LC.L_Type%><%--Type*****--%></th>
		<th width="50%"><%=LC.L_Use%><%--Use*****--%></th>
	</tr>

	<%//display dictionaries setting browser
    int index=-1;
   String studyLkpDict = studyB.getStudyAdvlkpVer();
   if (EJBUtil.isEmpty(studyLkpDict))
			studyLkpDict = "0";
   int studyLkpDictId=EJBUtil.stringToNum(studyLkpDict);
		LookupDao lookupDao = new LookupDao();
		lookupDao = studyB.getAllViewsForLkpType(EJBUtil.stringToNum(accountId) ,"NCI");
		ArrayList viewIds = lookupDao.getLViewIds();
		ArrayList viewNames = lookupDao.getLViewNames();
		System.out.println("viewids"+viewIds+"viewNames"+viewNames+"studyLkpdict"+studyLkpDict);
		String settingValue="";
	 if (studyLkpDictId==0)
	 {
		settingValue=LC.L_Free_TextEntry;/*settingValue="Free Text Entry";*****/
	 }
	 else
	 {
	   index=viewIds.indexOf(new Integer(studyLkpDict));
	   settingValue=(String)viewNames.get(index);
	  }%>
	<tr class="browserEvenRow">
		<td width="50%"><%=LC.L_Adv_EvtDict%><%--Adverse Event Dictionary*****--%></td>
		<td width="50%"><%=settingValue%></td>
	</tr>
	<%

	SettingsDao settingsDao=commonB.retrieveSettings(studyId,3,"PATSTUDY_ID_GEN");
	ArrayList settingsValue=settingsDao.getSettingValue();
	ArrayList settingsKeyword=settingsDao.getSettingKeyword();
	if (settingsValue.size()>0)
	  settingValue=(String)settingsValue.get(0);
	  else
	settingValue=LC.L_Manual/*"manual"*****/;

//end settings browser display


%>

	<tr class="browserOddRow">
		<td width="50%"><%=MC.M_PatStdId_Genr%><%--<%=LC.Pat_Patient%> <%=LC.L_Study%> ID Generation*****--%></td>
		<td width="50%"><%=settingValue%></td>
	</tr>
</table>
</br>
<%// May enhn
 StudyTXArmDao tDao = new StudyTXArmDao();
	  Integer txId;
	  String txName = "";
	  String txDesc = "";
	  tDao = stdTXArmJB.getStudyTrtmtArms(studyId);
	  ArrayList txIds = tDao.getStudyTXArmIds();
	  ArrayList txNames = tDao.getTXNames();
	  ArrayList txDescs = tDao.getTXDescs();
	  int txSize = txIds.size();

	  %>
<p class="sectionheadings lhsFont"><%=LC.L_Std_TreatmentArm%><%--Study Treatment Arm*****--%></p>


<table width="98%" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td width="70%" class="defComments lhsFont">
		<%=MC.M_TreatArm_AssocStd%><%--Treatment Arms currently associated with this <%=LC.L_Study_Lower%> are*****--%>:
		</td>
		<td></td>
		<td width="30%">
		<p><A href="#" onClick="opentrtarm(<%=studyId%>,0,'N',<%=txSize%>,<%=pageRight%>)"><%=LC.L_ADDNEW%><%--ADD NEW*****--%></A></p>
		</td>
	</tr>
</table>

<table width="99%" border="0" cellspacing="0" cellpadding="0"  class="outline midAlign">
	<tr>
		<th width="40%"><%=LC.L_Name%><%--Name*****--%></th>
		<th width="50%"><%=LC.L_Description%><%--Description*****--%></th>
		<th width="10%"><%=LC.L_Delete%><%--Delete*****--%></th>
	</tr>

	<%int count = 0;

	  for(counter = 0;counter<txSize;counter++)
	{

		txId = (Integer)txIds.get(counter);
		txName=((txNames.get(counter)) == null)?"-":(txNames.get(counter)).toString();

		txDesc=((txDescs.get(counter)) == null)?"-":(txDescs.get(counter)).toString();

		count = stdTXArmJB.getCntTXForPat(studyId, txId.intValue());

		if ((counter%2)==0) {

  %>
	<tr class="browserEvenRow">
		<%

		}

		else{

  %>
	</tr>
	<tr class="browserOddRow">
		<%

		}

  %>
		<td><A href="#" onClick="opentrtarm(<%=studyId%>,<%=txId%>,'M',0,<%=pageRight%>)"><%=txName%></A></td>
		<td><%=txDesc%></td>
		<td align="center"><A href="#"
			onClick="return f_delete('<%=txName%>','<%=count%>','<%=txId%>',<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" /></A></td>

	</tr>

	<%}%>
</table>
</br>
<%
	  // --May enhn%>
<p class="sectionheadings lhsFont"><%=LC.L_Assoc_Cal%><%--Associated Calendars*****--%></p>

<table width="98%" cellspacing="0" cellpadding="0" border="0">
	<tr>

<%
//Commented by Gopu to fix the bugzilla Issue #2359
//	grpRights = (GrpRightsJB) tSession.getValue("GRights");
//	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
%>
		<td width="30%" class="defComments lhsFont">
		<%=MC.M_CalsWith_StdAre%><%--Calendars currently associated with this <%=LC.L_Study_Lower%> are*****--%>:
		</td>

		<!-- Dartmouth -->
		<td width="20%">
		<A href="#" onClick="updateMultSchedules(<%=pageRight%>,'<%=src%>')"> <%=LC.L_Update_MultiSch_Upper%><%--UPDATE MULTIPLE SCHEDULES*****--%> </A>
		</td>

		<!-- Dartmouth -->

		<td width="15%" align="left">
		<A href="#"
	onClick="openwindowcopy(<%=pageRight%>,'<%=src%>',<%=tab%>)"><%=MC.M_Copy_ExistingCal_Upper%><%--COPY AN EXISTING CALENDAR*****--%></A>
<%
//Commented by Gopu to fix the bugzilla Issue #2359
		stdRights =(StudyRightsJB)tSession.getValue("studyRights");
	  		 if ((stdRights.getFtrRights().size()) == 0){
		 		pageRight= 0;
	//		 }else{
	//			pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSEC"));
			}
%>

		<td width="20%" align="right">
		<p><A href="#"
			onClick="return openwindowcalendar(<%=pageRight%>,'<%=src%>')"><%=MC.M_SelCal_FromLib_Upper%><%--SELECT A CALENDAR FROM YOUR LIBRARY*****--%></A></p>
		</td>
	</tr>
</table>
 
<table width="99%" border="0" cellspacing="0" cellpadding="0"  class="outline midAlign" id="calDisplay"> 
	<tr>

		<th width="25%"><%=LC.L_Name%><%--Name*****--%></th>
		<th width="10%"><%=LC.L_Refresh_Notifications%><%--Refresh Notifications*****--%></th>
		<th width="20%"><%=LC.L_Description%><%--Description*****--%></th>
		<th width="10%"><%=LC.L_Status%><%--Status*****--%></th>
		<th width="10%"><%=LC.L_Status_Dets%><%--Status Details*****--%></th>
		<th width="10%"><%=LC.L_Reports%><%--Reports*****--%></th>
		<th width="5%"><%=LC.L_Delete%><%--Delete*****--%></th>
		<th width="5%"><%=LC.L_Save_ToLib%><%--Save to Library*****--%> </th><!--KM-->

	</tr>
	<%

    for(counter = 0;counter<len;counter++)
	{

		id = (Integer)eventIds.get(counter);
		eventType=((eventTypes.get(counter)) == null)?"-":(eventTypes.get(counter)).toString();

		name=((names.get(counter)) == null)?"-":(names.get(counter)).toString();
		/* SV, 8/25, added check for description = "null" */
		description=((descriptions.get(counter) == null) || descriptions.get(counter).equals("null"))?"":(descriptions.get(counter)).toString();
		chainId=((chainIds.get(counter)) == null)?"-":(chainIds.get(counter)).toString();

		duration=((durations.get(counter)) == null)?"-":(durations.get(counter)).toString();

		
		//JM: 07Feb2011, #D-FIN9
		//status=((stats.get(counter)) == null)?"-":(stats.get(counter)).toString();				
		status=((codeSubTypes.get(counter)) == null)?"-":(codeSubTypes.get(counter)).toString();//this is code sub type
			
		statusText=((codeDescs.get(counter)) == null)?"-":(codeDescs.get(counter)).toString();//this is code description
		
				
			
		
		 
		

		if ((counter%2)==0) {

  %>
	<tr class="browserEvenRow">
		<%

		}

		else{

  %>
	</tr>
	<tr class="browserOddRow">
		<%

		}

  %>

<!--
		<td><A href="javascript:void(0);"  onClick="return saveLib('caltolibrary.jsp?eventid=<%=id%>&amp;name=<%=StringUtil.encodeString(name)%>&amp;desc=<%=StringUtil.encodeString(description)%>', <%=calendarRight%>);">
		<img src="./images/save.gif" alt="Save To Library" border="0"
			height="15px" width="15px"></A> <%--SV, 10/27/04, enter scheduled events page, while editing: <A href="protocolcalendar.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&protocolId=<%=id%>&calledFrom=S"> <%=name%> </A></td> --%>
-->

		<td>

		<A href="fetchProt2.jsp?protocolId=<%=id%>&amp;srcmenu=<%=src%>&amp;selectedTab=6&amp;mode=M&amp;calledFrom=S&amp;calStatus=<%=status%>&amp;displayType=V&amp;pageNo=1&amp;duration=<%=duration%>&amp;displayDur=3&amp;pageRight=<%=pageRight%>&amp;protocolName=<%=StringUtil.encodeString(name)%>&amp;calassoc=P"><%=name%> </A>

		<!--
		<A
		href="displayDOW.jsp?selectedTab=4&amp;mode=M&amp;duration=<%=duration%>&amp;calledFrom=S&amp;protocolId=<%=id%>&amp;srcmenu=<%=src%>&amp;calStatus=<%=status%>&amp;pageNo=1&amp;displayType=V&amp;headingNo=1&amp;displayDur=3&amp;pageRight=<%=pageRight%>&amp;protocolName=<%=StringUtil.encodeString(name)%>">
		<%=name%> </A>
		-->

		</td>
		<td align="center">
		<% if (!status.equals("null") && status.equals("A")) {
		%>
			<A	href="refreshProtocolMessages.jsp?srcmenu=<%=src%>&amp;selectedTab=<%=tab%>&amp;calendarId=<%=id%>&amp;mode="
			 border="0" />
			 <img src="../images/jpg/refreshicon.jpg" alt="<%=LC.L_Refresh_Notifications%><%--Refresh Notifications*****--%>" title="<%=LC.L_Refresh_Notifications%><%--Refresh Notifications*****--%>" border="0"	>
			 </A>
		<% } %>

		</td>

		<td><%=(description.length()==0)?"-":description%></td>
		<td>
		<!--KM-5882 -->
		<A
			href="calendarstatus.jsp?mode=M&amp;selectedTab=7&amp;protocolId=<%=id%>&amp;srcmenu=<%=src%>&amp;studyId=<%=studyId%>"
			onClick="return f_check_perm(<%=pageRight%>,'V')"><%=statusText%></A>
		</td>
		<td><A
			href="protocolhistory.jsp?mode=M&amp;srcmenu=tdmenubaritem3&amp;selectedTab=7&amp;protocolId=<%=id%>&amp;calledFrom=S"
			onClick="return f_check_perm(<%=pageRight%>,'V')"> <%=LC.L_Status_Dets%><%--Status Details*****--%></A></td>
		<td><a href="#"
			onclick="window.open('repRetrieve.jsp?protId=<%=id%>&amp;repId=106&amp;repName=<%=LC.L_Pcol_CalPreview%><%--Protocol Calendar Preview*****--%>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><img border="0" title="<%=LC.L_Preview%><%--Preview*****--%>" alt="<%=LC.L_Preview%><%--Preview*****--%>" src="../images/jpg/preview.gif"></a>&nbsp;&nbsp;&nbsp;
		<a href="#"
			onclick="window.open('repRetrieve.jsp?protId=<%=id%>&amp;repId=107&amp;repName=<%=LC.L_Mock_Schedule%><%--Mock Schedule*****--%>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><%=LC.L_Schedule%><%--Schedule*****--%></a>
		</td>
		<td align="center"><!--<A
			href="studycalendardelete.jsp?srcmenu=<%=src%>&amp;selectedTab=<%=tab%>&amp;calendarId=<%=id%>&amp;delMode=null"
			onClick="return confirmBox('<%=name%>',<%=pageRight%>)"><img src="./images/delete.gif" border="0" /></A>-->
			<A
			href="studycalendardelete.jsp?srcmenu=<%=src%>&amp;selectedTab=<%=tab%>&amp;calendarId=<%=id%>&amp;delMode=null&amp;from=calendar"
			onClick="return confirmBox('<%=name%>',<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" /></A></td>

			<td align="center"><A href="javascript:void(0);"  onClick="return saveLib('caltolibrary.jsp?eventid=<%=id%>&amp;name=<%=StringUtil.encodeString(name)%>&amp;desc=<%=StringUtil.encodeString(description)%>', <%=calendarRight%>);">
		<img src="./images/save.gif" alt="<%=LC.L_Save_ToLib%><%--Save To Library*****--%>" title="<%=LC.L_Save_ToLib%><%--Save To Library*****--%>" border="0"/> </A> <%--SV, 10/27/04, enter scheduled events page, while editing: <A href="protocolcalendar.jsp?mode=M&srcmenu=tdmenubaritem3&selectedTab=1&protocolId=<%=id%>&calledFrom=S"> <%=name%> </A></td> --%>

	</tr>
	<%

		}

%>
</table>
<% } // end of if page right
		else
		{ %> 
<p class="defcomments lhsFont"><%=MC.M_NoRgt_ToViewAssocCal %><%-- You do not have rights to view Associated
Calendars*****--%></p>
 <%}%> <%
	// To check for the account level rights
		String modRight = (String) tSession.getValue("modRight");
		 int patProfileSeq = 0, formLibSeq = 0;
		 acmod.getControlValues("module");
		 ArrayList acmodfeature =  acmod.getCValue();
		 ArrayList acmodftrSeq = acmod.getCSeq();
		 char formLibAppRight = '0';


		 formLibSeq = acmodfeature.indexOf("MODFORMS");
		 formLibSeq = ((Integer) acmodftrSeq.get(formLibSeq)).intValue();

		 formLibAppRight = modRight.charAt(formLibSeq - 1);
	 	if ((String.valueOf(formLibAppRight).compareTo("1") == 0) &&   (formRights >= 4)   )
		{

		%> <br>
<!---changed /entered by salil---> <%
			 int lfPk=0 ;
			linkedformsdao=linkedformsB.getStudyLinkedForms(studyId);
			ArrayList formIds=linkedformsdao.getLFId();
			ArrayList formStatIds=linkedformsdao.getFormStatId();
			ArrayList formLibIds=linkedformsdao.getFormId();
			ArrayList formnames= linkedformsdao.getFormName();
			ArrayList formdescriptions= linkedformsdao.getFormDescription();
			ArrayList formstatuss= linkedformsdao.getFormStatus();
			ArrayList formlinkedtos=linkedformsdao.getFormDisplayType();
			ArrayList lfPks=linkedformsdao.getLFId() ;
			ArrayList statCodeIds = linkedformsdao.getStatCodeId();
			ArrayList lfDataCnts = linkedformsdao.getLfDataCnt();
			ArrayList frmlkdfrom= linkedformsdao.getFormLinkedFrom();
	        // Added by gopu for Nov.2005 Enhancement #6
			ArrayList creators=linkedformsdao.getCreator();
			ArrayList createdons=linkedformsdao.getCreatedon();
			ArrayList modifiedbys=linkedformsdao.getModifiedby();
			ArrayList modifiedons=linkedformsdao.getModifiedon();

   	  		len=formnames.size();
			Integer formStatId,statCodeId;
			Integer lfDataCnt = null;
	%> <BR>

<p class="sectionheadings lhsFont"><%=LC.L_Associated_Forms%><%--Associated Forms*****--%></p>

<table width="98%" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td width="30%" class="defComments lhsFont">
		<%=MC.M_FrmAssocWith_Std%><%--Forms currently associated with this <%=LC.L_Study_Lower%> are*****--%>:
		</td>
		<td width="20%">
<%if (len > 0) {%> <A href="#"
	onClick="openCopyWin('<%=study%>',<%=formRights%>)"><%=MC.M_Copy_ExistingFrm_Upper%><%--COPY AN EXISTING FORM*****--%></A> <%}%></td>

		<td width="20%">

		<p><A href="#" onClick="openhideseq('<%=study%>',<%=formRights%>)"><%=MC.M_Disp_SeqOpts_Upper%><%--DISPLAY AND SEQUENCING OPTIONS*****--%></A></p>

		</td>
		<td width="25%" align="right">
		<p><A href="#" onClick="openwin2('<%=study%>',<%=formRights%>)"><%=MC.M_Selc_FrmYourLib_Upper%><%--SELECT A FORM FROM YOUR LIBRARY*****--%></A></p>
		</td>
	</tr>
</table>
<table width="99%" border="0" cellspacing="0" cellpadding="0"  class="outline midAlign">
	<tr>
		<th width="24%"><%=LC.L_Name%><%--Name*****--%></th>
		<th width="23%"><%=LC.L_Description%><%--Description*****--%></th>
		<th width="8%"><%=LC.L_Linked_To%><%--Linked To*****--%></th>
		<th width="10%"><%=LC.L_Status%><%--Status*****--%></th>
		<th width="10%"><%=LC.L_Preview%><%--Preview*****--%></th>
		<th width="8%"><%=LC.L_Delete%><%--Delete*****--%> </th>
		<th width="5%"><%=LC.L_Info%><%--Info*****--%> </th>
		<th width="10%"><%=LC.L_Save_ToLib%><%--Save to Library*****--%> </th><!--KM-->
	</tr>
	<%
			String formname="";
			String formdescription="";
			String formstatus="";
			String formlinkedto="";
			String 	formlkdfrom = "";
        // Added by gopu for Nov.2005 Enhancement #6
			String creator="";
			String createdon="";
			String modifiedby="";
			String modifiedon="";


    for(counter = 0;counter<len;counter++)
	{
		// salil
		formId = (Integer)formIds.get(counter);
		formLibId = (Integer)formLibIds.get(counter);
		formStatId = (Integer)formStatIds.get(counter);
		statCodeId = (Integer)statCodeIds.get(counter);

		formStatusInt = statCodeId.intValue();

		lfDataCnt = (Integer)lfDataCnts.get(counter);
		formlkdfrom = (frmlkdfrom.get(counter)).toString();

		formname=((formnames.get(counter)) == null)?"-":(formnames.get(counter)).toString();
		formdescription=((formdescriptions.get(counter)) == null)?"-":(formdescriptions.get(counter)).toString();
		formstatus=((formstatuss.get(counter)) == null)?"-":(formstatuss.get(counter)).toString();
		lfPk = (   (Integer )lfPks.get(counter)  ).intValue();
		String linkedToHref = "";
		formlinkedto=((formlinkedtos.get(counter))== null)?"-":(formlinkedtos.get(counter)).toString();

		// Added by Gopu for Nov.2005 Enhancement #4
		creator=((creators.get(counter)) == null)?"-":(creators.get(counter)).toString();

		createdon=((createdons.get(counter)) == null)?"-":(createdons.get(counter)).toString();

		modifiedby=((modifiedbys.get(counter)) == null)?"-":(modifiedbys.get(counter)).toString();

		modifiedon=((modifiedons.get(counter)) == null)?"-":(modifiedons.get(counter)).toString();

		if ( !  formlinkedto.equals("-") )
		{
			linkedToHref="<A href=# onClick=\"openwin3("+lfPk+","+statCodeId+","+activeId+")\" >" + formlinkedto+"</A>" ;

		}
		else
		{
			linkedToHref = formlinkedto ;
		}

		if ((counter%2)==0) {
  %>
	<tr class="browserEvenRow">
		<%

		}

		else{

  %>
	</tr>
	<tr class="browserOddRow">
		<%

		}

  %>
		<td>
		<!--KM:Apr17,2006-changed from lnkfrom to lnkFrom -->
		<A onClick = "return isNotMigrateStatus(<%=migrateStatPK%>,<%=formStatusInt%>)"
		href="formcreation.jsp?mode=M&amp;srcmenu=tdmenubaritem3&amp;selectedTab=1&amp;calledFrom=St&amp;formLibId=<%=formLibId%>&amp;lnkFrom=<%=formlkdfrom%>
		&amp;studyId=<%=studyId%>&amp;lfPk=<%=lfPk%>">
		<%=formname%></A></td>
		<td><%=formdescription%></td>
		<td><A href=""><%=linkedToHref%></A></td>
		<td align="center">
		<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
		<tr><td style="border: 0px"><A onClick = "return isNotMigrateStatus(<%=migrateStatPK%>, <%=formStatusInt%>)"
		href="formStatus.jsp?srcmenu=tdMenuBarItem1&amp;selectedTab=<%=tab%>&amp;formStatId=<%=formStatId%>&amp;lnkfrom=<%=formlkdfrom%>&amp;formId=<%=formLibId%>
		&amp;studyId=<%=studyId%>&amp;from=S"><%=formstatus%></A></td>
		<td width="25%" style="border: 0px"><A href="#" onClick="openwin1(<%=formLibId%>,<%=lfPk%>,<%=pageRight%>)"><img border="0" title="<%=LC.L_History%>" alt="<%=LC.L_History%>" src="./images/History.gif"/></A></td>
		</tr></table></td>

		<td align="center"><A href="#" onClick="openFormPreview(<%=formLibId%>)"><img src="../images/jpg/preview.gif" title="<%=LC.L_Preview%>" border="0" /></A></td>


		<td align="center">
		<% if (formStatusInt != migrateStatPK) { %>
		<A
			href="formdelete.jsp?srcmenu=<%=src%>&amp;selectedTab=<%=tab%>&amp;formId=<%=formId%>&amp;delMode=null&amp;from=S"
			onClick="return check('<%=StringUtil.escapeSpecialCharJS(formname)%>',<%=formRights%>,'<%=lfDataCnt%>')"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" /> </A>
		 <% } %>
		<td align="center">
		<A><img src="../images/jpg/help.jpg" border="0"  onmouseover="return overlib('<%="<tr><td><font size=2><b>"+LC.L_Creator/*Creator*****/+" : </b></font><font size=1>"+StringUtil.escapeSpecialCharJS(creator)+"</font></td></tr><tr><td><font size=2><b>"+LC.L_Created_On/*Created On*****/+": </b></font><font size=1>"+createdon+"</font></td></tr><tr><td><font size=2><b>"+LC.L_Last_ModifiedBy/*Last Modified By*****/+": </b></font><font size=1>"+StringUtil.escapeSpecialCharJS(modifiedby)+"</font></td></tr><tr><td><font size=2><b>"+LC.L_Last_ModifiedOn/*Last Modified On*****/+" : </b></font><font size=1>"+modifiedon+"</font></td></tr>"%>',CAPTION,'<%=LC.L_Form_Upper%><%--FORM*****--%>',RIGHT,ABOVE)"; onmouseout="return nd();"></A>
		</td>
		</td>
		<!--Modified By Amarnadh for issue #3219-->
		<!-- Added by Manimaran for F4 Enhancement -->
		<td align="center">
		<A href="javascript:void(0);" onClick="return saveFormLib('formtolibrary.jsp?formLibId=<%=formLibId%>&amp;name=<%=StringUtil.encodeString(formname)%>',		<%=formRightLibrary%>,<%=migrateStatPK%>,<%=formStatusInt%>); "> <img src="./images/save.gif" alt="<%=LC.L_Save_ToLib%><%--Save To Library*****--%>" title="<%=LC.L_Save_ToLib%><%--Save To Library*****--%>" border="0"  ></A>
		</td> <!--KM- Modified -->

	</tr>
	<%

		}

%>
</table>

<% }//end of formright check
		else
		{ %> <!-- <b><p class="defcomments">You do not have rights to view Associated Forms</p></b> -->
<%}%> <!--- end of change by salil--> <%}else{  //page right and formright  %>
<jsp:include page="accessdenied.jsp" flush="true" /> <%}
	} //end of if body for check on studyId

	} //end of if session times out

else

{

%> <jsp:include page="timeout.html" flush="true" /> <%

} //end of else body for page right

%>
<div><jsp:include page="bottompanel.jsp" flush="true" /></div>
</DIV>
<div class="mainMenu" id="emenu"><jsp:include page="getmenu.jsp"
	flush="true" /></div>
</body>

</html>
