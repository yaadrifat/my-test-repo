	var varTo=L_To_Lower;
	var varOf=L_Of_Lower;
	var varRecord_S=L_Record_S;
	var xmldoc = new ActiveXObject("Microsoft.XMLDOM");
	var xsldoc = new ActiveXObject("Microsoft.XMLDOM");
	var xmlCon = new ActiveXObject("Microsoft.XMLDOM");

	xmldoc.async = false;
	i=xmldoc.loadXML(W1.xml);
	if(i == true){ 
		xsldoc.async = false;
		i= xsldoc.loadXML(W2.xml);
		if (i == false){
		location.reload();
		}else{
			xmlCon.async = false;
			j = xmlCon.loadXML(W3.xml);
			if (j == false){
				location.reload();
			}else{
				var sTyp = xmlCon.selectSingleNode("properties/property[@name='sortType']").getAttribute("value");
				var rNode = xmldoc.selectSingleNode("Result");
				iTotal = parseInt(rNode.getAttribute("total"));

function Properties(nam, typ, ord)
{
	this.name=nam;
	this.order=(ord==null? '1': ord);
	this.stype=typ;
}

var propArray = new Array();
var cNodes = rNode.selectNodes("Header/field");

for(i=0; i<cNodes.length; i++)
{
	temp = cNodes.item(i);
	propArray[i] = new Properties(temp.getAttribute("name"), temp.getAttribute("sorttype"), temp.getAttribute("order"));
}

var sIndex;
var fRow = rNode.selectSingleNode("row");
if (fRow==null)
{
	trCol = xsldoc.createElement("td");
	trCol.setAttribute("colspan", "" + cNodes.length)
	trCol.text = xmlCon.selectSingleNode("properties/property[@name='noMatch']").getAttribute("value");
	trNode = xsldoc.createElement("tr").appendChild(trCol);

 	var tNode = xsldoc.selectSingleNode("/xsl:stylesheet/xsl:template[@match='Result']/table");
	var bNode = tNode.selectSingleNode("tbody");
	bNode.replaceChild(trNode, bNode.selectSingleNode("xsl:for-each"));
	SResult.innerHTML = xmldoc.transformNode(xsldoc);
}
else
{
	sIndex = parseInt(fRow.getAttribute("id"));

	openPage(1);
}

function openPage(pNum)
{
	var pLength = parseInt(xmlCon.selectSingleNode("properties/property[@name='clientsize']").getAttribute("value"));
	var iNum = sIndex + (pNum-1)*pLength;
	var iEnd = iNum + pLength - 1
	var rLength = parseInt(rNode.lastChild.getAttribute("id"));
	var tNode = xsldoc.selectSingleNode("/xsl:stylesheet/xsl:template[@match='Result']/table/tbody/xsl:for-each");
	tNode.setAttribute("select", "row[@id $ge$ " + iNum + " $and$ @id $le$ " + iEnd + "]");
	/*var sMore = "<font face='Arial, Helvetica, sans-serif' color='#cc6633' size='2'>" + iNum + " to " + (rLength<iEnd? rLength: iEnd)+ " of " + iTotal +" Record(s)<br/></font><center>";*****/
	var sMore = "<font face='Arial, Helvetica, sans-serif' color='#cc6633' size='2'>" + iNum + " "+varTo+" " + (rLength<iEnd? rLength: iEnd)+ " "+varOf +" "+ iTotal +varRecord_S+" "+"<br/></font><center>";
	ilastRecord = parseInt(rNode.selectNodes("row").length)
	var pCount = Math.ceil(ilastRecord/pLength);
	if(pCount>1)
		for(i=0;i<pCount;i++)
			sMore += "<a href='javascript:openPage(" + (i + 1) + ")'>" + (i + 1) + "</a> | ";
	sMore += "<br>";
	if(sIndex!=1)
		sMore += "<input type='image' src='../images/jpg/Back.gif' onClick='scroll(-1)' alt='Back'/>";

	if(sIndex + ilastRecord !=iTotal + 1)
		sMore += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='image' src='../images/jpg/Next.gif' onClick='scroll(1)' alt='More'/>";
	sMore += "</center>";
		
	SResult.innerHTML = xmldoc.transformNode(xsldoc) + sMore;

}

function getFldIndex(val)
{
	for (i=0; i<cNodes.length; i++)
		if (trim(val) == trim(cNodes.item(i).getAttribute("name"))) return i;
	return 0;
}

var ind, fAsc;
function sortField(nam)
{
	ind = getFldIndex(nam);
	if (sTyp=='S')
	{
		document.forms[2].sortType.value = propArray[ind].stype
		document.forms[2].sortOrder.value = propArray[ind].order
		document.forms[2].sortCol.value = ind;
		document.forms[2].action = opnrhref;
		document.forms[2].submit();
	}
	else
	{
		var tArray = new Array();
		chld = rNode.childNodes;
		len = chld.length - 1;
		for(i=0;i<len; i++)
		{
			tArray[i] = chld.item(1);
			rNode.removeChild(chld.item(1));
		}

		var tNew;
		fAsc = propArray[ind].order;
		switch(propArray[ind].stype)
		{
			case 'N':
				tNew = tArray.sort(sortNumbers);
				break;
			case 'D':
				tNew = tArray.sort(sortDates);
				break;
			default :
				tNew = tArray.sort(sortStrings);
				break;
		}

		for(i=0;i<tArray.length;i++)
		{
			tArray[i].setAttribute("id", (sIndex + i))
			rNode.appendChild(tArray[i]);
		}

		for(i=0;i<cNodes.length;i++)
		{
			cNodes.item(i).removeAttribute("order");
			propArray[i].order = 1;
		}
	
		propArray[ind].order = -1*fAsc;
		cNodes.item(ind).setAttribute("order", "" + -1*fAsc);
		alert(cNodes.item(ind).getAttribute("order"))	
		rNode.selectSingleNode("Header/field[@name = '"+trim(nam)+"']").setAttribute("order", (propArray[ind].order));
		openPage(1);
	}		
}

function sortNumbers(a, b)
{
	aNode = a.selectSingleNode("field[" + ind + "]").text;
	bNode = b.selectSingleNode("field[" + ind + "]").text;
	return fAsc*(parseInt(aNode) - parseInt(bNode));
}

function sortDates(a, b)
{
	aNode = a.selectSingleNode("field[" + ind + "]").text;
	bNode = b.selectSingleNode("field[" + ind + "]").text;
	return fAsc*((new Date(a) < new Date(b))? -1: 1);
}

function sortStrings(a, b)
{
	aNode = a.selectSingleNode("field[" + ind + "]").text;
	bNode = b.selectSingleNode("field[" + ind + "]").text;
	return fAsc*(aNode < bNode? -1: 1);
}

function scroll(val)
{
	val += parseInt(rNode.getAttribute("pagecount"));
	var frms = document.forms
	para = "";
	if (frms.length>0)
	{
		for (i=0; i<frms.length; i++)
		{
			elmts = frms[i].elements
			if (elmts.length>0)
			{
				for(j=0; j<elmts.length; j++)
				{
					if ((elmts[j].type=="hidden")||(elmts[j].type=="text"))
						para += "&" + elmts[j].name + "=" + elmts[j].value;
					else if (elmts[j].type=="select-multiple")
					{
						arr = elmts[j].options
						for(k=0; k<arr.length; k++)
							if(arr[k].selected)
								para += "&" + elmts[j].name + "=" + arr[k].value;
					}
					else if (elmts[j].type=="select-one")
						para += "&" + elmts[j].name + "=" + elmts[j].options[elmts[j].selectedIndex].value;
					else if ((elmts[j].type=="checkbox")||(elmts[j].type=="radio"))
						if (elmts[j].checked)
							para += "&" + elmts[j].name + "=" + elmts[j].value;
				}
			}
		}
	}

	document.forms[2].action = opnrhref + "?scroll=" + val + para;
	document.forms[2].submit()
}


function conDisp()
{
	window.open("Configure.html", "Configure");
}

var reg = new RegExp('^[\\n,\\r,\\t, ]$');
function trim(sVal)
{
	return ltrim(rtrim(sVal));
}
 
function rtrim(sVal)
{
	var len = sVal.length;
	return (reg.test(sVal.substring(len - 1))? rtrim(sVal.substring(0, len - 1)): sVal);
}
 
function ltrim(sVal)
{
	var len = sVal.length;
	return (reg.test(sVal.substring(0, 1))? ltrim(sVal.substring(1)): sVal);
}

//function remainder(val)
//{
//	return (val%2)
//}

function filterResult(val)
{
	document.forms[2].filter.value = val;

	document.forms[2].submit();
}

		}
	}

}