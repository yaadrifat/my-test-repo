<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Frm_FormSet%><%-- Form >> Form Settings*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.StringUtil,com.velos.eres.service.util.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>

<script>

function  validate(formobj){
  //for sharewith

		 if((formobj.rbSharedWith[2].checked == true) && (formobj.selGrpNames.value==""))
		 {
		 alert("<%=MC.M_PlsSelectGrp%>");/*alert("Please Select Group");*****/
		 return false;
		 }

		 if((formobj.rbSharedWith[3].checked == true) && (formobj.selStudy.value==""))
		 {
		 alert("<%=MC.M_PlsSelectStd%>");/*alert("Please Select <%=LC.Std_Study%>");*****/
		 return false;
		 }

		 if((formobj.rbSharedWith[4].checked == true) && (formobj.selOrg.value==""))
		 {
		 alert("<%=MC.M_PlsSelectOrg%>");/*alert("Please Select Organization");*****/
		 return false;
		 }
  //end sharewith
	 if (!(validate_col('e-Signature',formobj.eSign))) return false
	 if(isNaN(formobj.eSign.value) == true) {
		 alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
		formobj.eSign.focus();
		return false;
    	}   
}
 
function openWin(formId,formNotify,mode,pageRight,codeStatus,formMode)
{ 		 
	if((formMode=="M") && (f_check_perm(pageRight,'E') == true) )
	{	//KM-19Nov	
		str="formNotify.jsp?formNotifyId="+formNotify+"&formLibId="+formId+"&mode="+mode+"&codeStatus="+codeStatus;
		windowName= window.open(str,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=700,height=350,top=150,left=150");
		windowName.focus();
		return;		
	}
	else if  ((formMode == "N") && ((f_check_perm(pageRight,'N') == true) || pageRight == 7))
	{
		str = "formNotify.jsp?formLibId="+formId+"&mode="+mode+"&codeStatus="+codeStatus;
		windowName= window.open(str,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=700,height=350,top=150,left=150");
		windowName.focus();
		return;
	}

}

function openAddEditWin(formId,formobj,mode) {
	
    calledFrom = formobj.calledFrom.value;
	lnkFrom = formobj.lnkFrom.value;
	studyId= formobj.studyId.value;	
	codeStatus = formobj.codeStatus.value;
    str = "fieldBrowser.jsp?formId="+ formId+"&mode="+mode+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom+"&studyId="+studyId+"&codeStatus="+codeStatus;
	var e=window.open(str ,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=825,height=500,top=90,left=90");
	e.focus();
	
}
/***************** Sonia - for field Actions*********************/
function openWinFldAction(formId,formobj,mode,fldActionId,type,right) {
    calledFrom = formobj.calledFrom.value;
	lnkFrom = formobj.lnkFrom.value;
	studyId= formobj.studyId.value;	

	if (type == "tempGrey")
	{	
	    str = "defineFieldAction.jsp?fldActionId="+ fldActionId +"&formId="+ formId+"&mode="+mode+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom+"&studyId="+studyId+"&pgrt="+right;
	}
	else
	{
	  str = "defineCalcFieldAction.jsp?fldActionId="+ fldActionId +"&formId="+ formId+"&mode="+mode+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom+"&studyId="+studyId+"&pgrt="+right;	
	}	    
	var e=window.open(str ,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=500,height=250,top=90,left=90");
	e.focus();
	
}

//added 05/05
function openWinFormAction(formId,formobj,mode,formActionId,type,right) {
    calledFrom = formobj.calledFrom.value;
	lnkFrom = formobj.lnkFrom.value;
	studyId= formobj.studyId.value;	
    str = "defineFormAction.jsp?formActionId="+ formActionId +"&formId="+ formId+"&mode="+mode+"&calledFrom="+calledFrom+"&lnkFrom="+lnkFrom+"&studyId="+studyId+"&pgrt="+right;
	var e=window.open(str ,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=600,height=350,top=90,left=90");
	e.focus();
	
}

 function f_delete_formNotify(notifyId,pageRight)
 {
 if (f_check_perm(pageRight,'E')) 
	{
		/*if (confirm("Do you want to delete this Form Notification?"))*****/ 
		if (confirm("<%=MC.M_WantToDel_FrmNotfic%>"))
		{
			
		        windowName= window.open("deleteformnotify.jsp?notifyid="+notifyId ,"formnotifydel","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=250");
			windowName.focus();
		} else 
		{
			return false
		}
	} else
	{
		return false;
	}//end of edit right
  
 }
 
 function f_delete_fldAction(fieldActionId,formId,pageRight) {
	if (f_check_perm(pageRight,'E')) 
	{
		calledFrom = document.frmsetting.calledFrom.value;
		/*if (confirm("Do you want to delete this Field Action?"))*****/ 
		if (confirm("<%=MC.M_WantToDel_FldAct%>"))
		{
			windowName= window.open("deleteFieldAction.jsp?formId="+formId + "&fieldAction=" + fieldActionId+"&calledFrom="+calledFrom,"fldactdel","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=500,height=250");
			windowName.focus();
		} else 
		{
			return false
		}
	} else
	{
		return false;
	}//end of edit right
} //end of function

/* ---------060408ML
 deleteformaction.jsp needs to be built, so far just a mock using deleteformnotify.jsp  */
function f_delete_formAction(formActionId,pageRight) {
	if (f_check_perm(pageRight,'E')) 
	{
	
		/*if (confirm("Do you want to delete this Form Action?"))*****/ 
		if (confirm("<%=MC.M_WantToDel_FrmAct%>")) 
		{
			windowName= window.open("deleteformaction.jsp?formActionId="+formActionId, "formactiondel", "toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=350");
			windowName.focus();
		} else 
		{
			return false
		}
	} else
	{
		return false;
	}//end of edit right
} //end of function
// --------------------------------end of 060408ML

/***************** end of change; Sonia - for field Actions*********************/

function openFormPreview(formId)
{
	windowName=window.open("formpreview.jsp?formId="+formId,"Information","toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=825,height=600,top=90,left=150");
	windowName.focus();
}

</script>

<% String src;
src= request.getParameter("srcmenu");

String formId = request.getParameter("formId");

if (StringUtil.isEmpty(formId))
{
	formId = "0";
}
String mode=request.getParameter("mode");
String codeStatus =request.getParameter("codeStatus");
String calledFrom=request.getParameter("calledFrom");
String selectedTab = request.getParameter("selectedTab");
%>

<body>
<jsp:include page="panel.jsp" flush="true"> 
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>   

<br>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="formNotifyJB" scope="request"  class="com.velos.eres.web.formNotify.FormNotifyJB"/>
<jsp:useBean id="fldlibB" scope="request" class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="formlibB" scope="request" class="com.velos.eres.web.formLib.FormLibJB"/>
<%@ page language = "java" import = "com.velos.eres.tags.*,com.velos.eres.business.site.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.studyRights.StudyRightsJB"%>

<DIV class="BrowserTopn" id="div1">
<%

   HttpSession tSession = request.getSession(true); 
   
   if (sessionmaint.isValidSession(tSession))
   {

		 int pageRight = 0;	
		 GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");		
		 String lnkFrom=request.getParameter("lnkFrom");
 		 if(lnkFrom==null) 
		 {lnkFrom="-";}

		 String stdId=request.getParameter("studyId");
		 int studyId= EJBUtil.stringToNum(stdId);			

		String userIdFromSession = (String) tSession.getValue("userId");
		
		if( calledFrom.equals("A")){ //from account form management
	 	    if (lnkFrom.equals("S")) 
	 	      {   
		 		 ArrayList tId ;
				 tId = new ArrayList();
				 if(studyId >0)
				 {
				 	TeamDao teamDao = new TeamDao();
					teamDao.getTeamRights(studyId,EJBUtil.stringToNum(userIdFromSession));
					tId = teamDao.getTeamIds();
					if (tId.size() <=0)
					 {
						pageRight  = 0;
					  } else 
					  {
					  	StudyRightsJB stdRights = new StudyRightsJB();
						stdRights.setId(EJBUtil.stringToNum(tId.get(0).toString()));
						 
						ArrayList teamRights ;
						teamRights  = new ArrayList();
						teamRights = teamDao.getTeamRights();
							 
						stdRights.setSuperRightsStringForStudy((String)teamRights.get(0));
						stdRights.loadStudyRights();
						 		
						if ((stdRights.getFtrRights().size()) == 0)
						{					
						  pageRight= 0;		
						} else 
						{								
							pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
						}
					  }
			    }
			 }	
    	 	else
    	    {	
    	    	
    	  		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MACCFRMS"));
    		 }
	  	} 
		
	 	if( calledFrom.equals("L")) //form library
	 	{
			pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("MFRMLIB"));
	 	}
		
		if( calledFrom.equals("St")) //study
	 	{
			StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");	
	  		pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYFRMMANAG"));
		}
		 

		if (pageRight >=4)
		{
			String formLibIdTmp=request.getParameter("formId");
			
			int rows=0;;
			int count=0;
			int formLibId;
			String formNotifyId=null;		
			String msgType=null;
			String msgSendType=null;
			String msgText=null;
			String msgTypeVal = null;

			String selFieldNames="";
			ArrayList fldNames=null;
			ArrayList fldUniIds=null;
			ArrayList fldBrowserFlags=null;
			String sharedWith="";			
			 
			formLibId = EJBUtil.stringToNum(formLibIdTmp);
			

     	 	if( calledFrom.equals("L")) //form library
     	 	{
     			formlibB.setFormLibId(formLibId);		
      		    formlibB.getFormLibDetails();
     			sharedWith=formlibB.getFormLibSharedWith();			
     	 	}
					
			
			FormNotifyDao formNotifyDao = formNotifyJB.getAllFormNotifications(formLibId);
				
			ArrayList formNotifyIds = formNotifyDao.getFormNotifyId(); 
			ArrayList msgTypes = formNotifyDao.getMsgType();
			ArrayList msgTexts = formNotifyDao.getMsgText();
			ArrayList msgSendTypes = formNotifyDao.getMsgSendType();
			ArrayList userLists = formNotifyDao.getUserList();
			rows=formNotifyDao.getRows();	

			FieldLibDao fldLibDao = new FieldLibDao();
			fldLibDao = fldlibB.getFieldsInformation(formLibId);	
			fldNames = fldLibDao.getFldName();
			fldUniIds = fldLibDao.getFldUniqId();
			fldBrowserFlags = fldLibDao.getFldBrowserFlag();
			int totRecords=fldUniIds.size();
			int flagnum=0;
			int j=0;

			for(int i=0;i<totRecords;i++)
			{
				if(fldBrowserFlags.get(i).equals("1"))
				{
				 flagnum++;
				}
			
			}
			if(flagnum==1)
			{
				for(int i=0;i<totRecords;i++)
				{
					if(fldBrowserFlags.get(i).equals("1"))
					{
						
						selFieldNames=fldNames.get(i).toString();
					}
				}
			}
			else
			{
				for(int i=0;i<totRecords;i++)
				{
					if(fldBrowserFlags.get(i).equals("1"))
					{
						if(flagnum>1)
						{
						selFieldNames=selFieldNames+fldNames.get(i).toString();
						selFieldNames=selFieldNames+",";
						flagnum--;
						}
						else
						{
						selFieldNames=selFieldNames+fldNames.get(i).toString();
						}

					}
				
				}
			}
			
	selFieldNames=selFieldNames.trim();
%>
<jsp:include page="formtabs.jsp" flush="true">
<jsp:param name="formId" value="<%=formId%>"/>
<jsp:param name="mode" value="<%=mode%>"/>
<jsp:param name="codeStatus" value="<%=codeStatus%>"/>
<jsp:param name="calledFrom" value="<%=calledFrom%>"/>
<jsp:param name="lnkFrom" value="<%=lnkFrom%>"/>
<jsp:param name="studyId" value="<%=studyId%>"/>
</jsp:include>

</div>
<DIV class="BrowserBotN BrowserBotN_FLsetting_1" id="div2">		
<%
 if (formId.equals("0")) {		
%>
   <P class = "defComments"><FONT class="defComments"><%=MC.M_SaveFrm_BeforeDets%><%-- Please save the Form first before entering details here.*****--%></FONT></P>  
<%
} else 

{%>		

		
	<Form name="frmsetting" id="frmsettingid" method="post" action="updateformsharewith.jsp" onsubmit="if (validate(document.frmsetting)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}"> 	
		
		<input type="hidden" name="formId" value=<%=formId%>>
		<input type="hidden" name="calledFrom" value=<%=calledFrom%>>
		<input type="hidden" name="lnkFrom" value=<%=lnkFrom%>>
		<input type="hidden" name="studyId" value=<%=studyId%>>
		<input type="hidden" name="srcmenu" value=<%=src%>>
		<input type="hidden" name="selectedTab" value=<%=selectedTab%>>
		
  	<%if( calledFrom.equals("L")) //form library
     {%>
								
  	<!--sharewith-->
   	 <jsp:include page="objectsharewith.jsp" flush="true">
   	 <jsp:param name="objNumber" value="1"/>
   	 <jsp:param name="mode" value="M"/>
   	 <jsp:param name="formobject" value="document.frmsetting"/>
   	 <jsp:param name="formnamevalue" value="frmsetting"/>
   	 <jsp:param name="fkObj" value="<%=formId%>"/>	 		  	
   	 <jsp:param name="sharedWith" value="<%=sharedWith%>"/>	 		  	 		 	 	 	 
	 </jsp:include>
	<!--end sharewith-->
	<br>    
   
   <jsp:include page="submitBar.jsp" flush="true"> 
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="frmsettingid"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>
<br>			
		<%} //calledFrom L%>
						
			<table width="99%" cellspacing="0" cellpadding="0" border="0" class="midAlign">
			<tr>
			<td width="50%"><b><%=MC.M_FldsToDisped_Browser%><%-- Fields to be Displayed in Browser*****--%> </b>&nbsp;&nbsp;&nbsp;<A href="#" onClick="openAddEditWin(<%=formLibId%>,document.frmsetting,'<%=mode%>')"><%//=LC.L_AddOrEdit%><%-- Add/Edit*****--%><img title="<%=LC.L_Edit%>" src="./images/edit.gif" border ="0"></A></td>
				</tr>
			<tr><td colspan="2"><textarea  name="fldsInBrowser"  rows="2" cols="80" readOnly=true><%=selFieldNames%></textarea></td>
			</tr>	
			</table>
		<!--******************* Sonia ***************  -->
			<%
				//get form field Actions Browser Data
			
				FieldActionDao fd = new FieldActionDao();
				VFieldAction vfAction = new VFieldAction();
				Hashtable htTarget = new Hashtable();
				Hashtable htSource = new Hashtable();
				
				String srcFldName = "";
				String actionName= "";
				String targetFields = "";

				Integer fldActionId ;
				String fldActionType = "";
				int actionCount = 0;
					
				
				fd.getFieldActionsBrowserInfo(formId );

				htTarget = vfAction.getFieldActionKeywordInfoForForm(formLibId ,"[VELTARGETID]" );
				htSource = vfAction.getFieldActionKeywordInfoForForm(formLibId , "[VELSOURCEFLDID]");
				
				System.out.println("htSource .size()" + htSource .size());
				//get target fields for the field Actions form
				
				ArrayList arFldActionIds = new ArrayList();
				ArrayList arFldActionNames = new ArrayList();
				ArrayList arFldNames = new ArrayList();	
				ArrayList arFldActionTypes = new ArrayList();
				
				arFldActionIds = fd.getFldActionIds();
				arFldActionNames = fd.getFldActionNames();
				arFldNames  = fd.getFldNames();
				arFldActionTypes  = fd.getFldActionTypes();
				
				actionCount = arFldActionIds.size();
				
			%>
			 <br>
	
	<!-- hiding the feature		    
			<table width="98%">
			<tr>
			<td width="40%" > <b>Inter Form Actions </b> </td>
				<% if((codeStatus.equals("WIP") || codeStatus.equals("Offline")) && (pageRight >= 6 ) ){%>
				<td width="40%" align ="right" colspan=4>  <A href ="#" onclick="openWinFormAction(<%=formLibId%>,document.frmsetting,'N','','tempGrey','<%=pageRight%>')">Create an Inter-Form Validation</A>
				</td>
							
				<%}%>
			</tr>
			<tr> 
	        <th width="15%"> Source Form.Field </th>
    	    <th width="30%"> Operation </th>
    	    <th width="25%"> Target Form.Field </th>
       	    <th width="15%"> </th>
       	     
   	    	 
      		</tr>
      		
      		
      		<%
      		FormActionDao faDao = new FormActionDao();
      		ArrayList targetFormIds = new ArrayList();
      		ArrayList operators = new ArrayList();
      		ArrayList sourceFlds = new ArrayList();
      		ArrayList targetFlds = new ArrayList();
      		ArrayList targetForms = new ArrayList();
      		ArrayList sourceForms = new ArrayList();
      		ArrayList srcFldUIds = new ArrayList();
      		ArrayList trgFldUIds = new ArrayList();
      		 //ML060508
      		ArrayList formActionIds = new ArrayList();
      		int formActionCount = 0;
      		String operator="";
      		String sourceFld="";
      		String targetFld="";
      		String targetForm="";
      		String sourceForm="",srcFldUId="",trgFldUId="";
     
    		 //ML0605088
     		String formActionId = "";
    		 // endof ML060508 
      		int targetFormId=0;
      		int sourceFormId=0;
      		String targetFormName="";
      		String sourceFormName="";
      		faDao.getFieldActions(formId);
      	//060508ML
			formActionIds=faDao.getFormActionIds();
      		formActionCount = formActionIds.size();
    		//
      		targetFormIds = faDao.getTargetFormIds();
      		operators = faDao.getOperators();
      		sourceFlds = faDao.getSourceFormFields(); 
      		targetFlds = faDao.getTargetFormFields();
      		targetForms = faDao.getTargetFormIds();
      		sourceForms = faDao.getSourceFormIds();
      		srcFldUIds=faDao.getSrcFldUIds();
      		trgFldUIds=faDao.getTrgFldUIds();
  
      		for (int m = 0; m < formActionCount; m++){
      			
      			operator = (String) operators.get(m);
      			
      			if(operator.equals(">")){operator= LC.L_Greater_Than;}/*if(operator.equals(">")){operator="Greater Than";}*****/
      			if(operator.equals("<")){operator=LC.L_Less_Than;}/*if(operator.equals("<")){operator="Less Than";}*****/
      			if(operator.equals("=")){operator=LC.L_Equal_To;}/*if(operator.equals("=")){operator="Equal To";}*****/
      			if(operator.equals("!=")){operator= LC.L_NotEqual_To;}/*if(operator.equals("!=")){operator="Not Equal To";}*****/
      			if(operator.equals(">=")){operator=MC.M_Greater_ThanEqualTo;}/*if(operator.equals(">=")){operator="Greater Than Equal To";}*****/
      			if (operator.equals("<=")){operator=MC.M_LessThan_EqualTo;}/*if (operator.equals("<=")){operator="Less Than Equal To";}*****/
      			
      			
      			
      			sourceFld = (String) sourceFlds.get(m);
      			targetFld = (String) targetFlds.get(m);
      			targetForm =  targetForms.get(m).toString();
      			sourceForm =  sourceForms.get(m).toString();
      			srcFldUId=srcFldUIds.get(m).toString();
      			trgFldUId=trgFldUIds.get(m).toString();
      			formActionId = formActionIds.get(m).toString();

      			if(!(targetForm.equals(""))){
      				targetFormId = EJBUtil.stringToNum(targetForm);
      				formlibB.setFormLibId(targetFormId);
      				 formlibB.getFormLibDetails();
      				targetFormName = formlibB.getFormLibName();
      			}
      			if(!(sourceForm.equals(""))){
      				sourceFormId = EJBUtil.stringToNum(sourceForm);
      				formlibB.setFormLibId(sourceFormId);
      				 formlibB.getFormLibDetails();
      				sourceFormName = formlibB.getFormLibName();
      			}
      			
      			if ((m%2)==0) 
				{
					%>
  					<tr class="browserEvenRow"> 
					<%

				}

				else
				{
				%>
			      <tr class="browserOddRow"> 
		<%
				}      			
		%>			
					<td><%=sourceFormName%>.<%=srcFldUId%></td>
					<td><%=operator%></td>
					<td><%=targetFormName%>.<%=trgFldUId%></td>
					<td><A href ="#" onclick="openWinFormAction(<%=formLibId%>,document.frmsetting,'M',<%=formActionId%>,'tempGrey','<%=pageRight%>')"><img border="0" title="<%=LC.L_Edit%>" alt="<%=LC.L_Edit%>" src="./images/edit.gif"/></td>
 					<td><A href="#" onClick="return f_delete_formAction('<%=formActionId%>','<%=pageRight%>')"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A></td>
				</tr>
      	<%
      		}
      	%>	
      		</table>
    				  		 
      		<br/> -->
    				
      				
      					<table width="99%" cellspacing="0" cellpadding="0" border="0" class="midAlign">
      		
      		
			<tr>
			<td width="40%" > <b><%=LC.L_Inter_FldActions%><%-- Inter Field Actions*****--%> </b> </td>
				<% 
			if((codeStatus.equals("WIP") || codeStatus.equals("Offline")) && (pageRight >= 6 ) ){%>
				<td width="40%" align ="right" colspan=4>  <%=MC.M_AddAct_ForTyp%><%-- Add a New Action for type*****--%>: <A href ="#" onclick="openWinFldAction(<%=formLibId%>,document.frmsetting,'N','','tempGrey','<%=pageRight%>')"><%=LC.L_Change_State_Upper %><%-- CHANGE STATE*****--%></A>
					&nbsp;&nbsp;<A href ="#" onclick="openWinFldAction(<%=formLibId%>,document.frmsetting,'N','','tempAdd','<%=pageRight%>')"><%=LC.L_Calculation_Upper%><%-- CALCULATION*****--%></A>
					</td>
				<%}%>
			</tr>	
				 
      		<tr class="outline"> 
      		<table width="99%" cellspacing="0" cellpadding="0" border="0" class="midAlign outline">
      		<tr> 
	        <th width="15%"> <%=LC.L_Source_Fld_S%><%-- Source Field(s)*****--%> </th>
    	    <th width="30%"> <%=LC.L_Target_Field_S%><%-- Target Field(s)*****--%> </th>
    	    <th width="25%"> <%=LC.L_Action_Type%><%-- Action Type*****--%> </th>
       	    <th width="15%"><%=LC.L_Edit%><%-- Edit*****--%></th> 	
   	    	<th width="15%"><%=LC.L_Delete%><%-- Delete*****--%></th> 	
   	    	</tr>
			
			<%
				for (int k = 0; k < actionCount; k++)
				{	
					//srcFldName = (String) arFldNames.get(k);
					actionName = (String) arFldActionNames.get(k);
					fldActionId = (Integer)arFldActionIds.get(k);
					fldActionType = (String) arFldActionTypes.get(k);
					
						if ( htTarget.containsKey(fldActionId.toString()))
						{
							targetFields = (String) htTarget.get(fldActionId.toString());
						}
						else
						{
							targetFields = "-";
						}	
						
						
						if ( htSource.containsKey(fldActionId.toString()))
						{
							srcFldName = (String) htSource.get(fldActionId.toString());
						}
						else
						{
							srcFldName = "-";
						}	
						
						
								
					if ((k%2)==0) 
					{
						%>
      					<tr class="browserEvenRow"> 
						<%

					}

					else
					{
					%>
				      <tr class="browserOddRow"> 
			<%
					}
			%>
						<td><%= srcFldName%></td>
						<td><%= targetFields%></td>
						<td><%= actionName%></td>
						 <td>
				
			<%	
			//KM-#3967 -- Inter Field Action editing/modification should not be allowed	
			if ((codeStatus.equals("WIP") || codeStatus.equals("Offline")) && (pageRight >= 6 ) ){%>
			<A href ="#" onclick="openWinFldAction(<%=formLibId%>,document.frmsetting,'M',<%=fldActionId%>,'<%=fldActionType%>','<%=pageRight%>')"><img border="0" title="<%=LC.L_Edit%>" alt="<%=LC.L_Edit%>" src="./images/edit.gif"/>
			<%}%>	
			
			</td>
			<td>
			<% if ((codeStatus.equals("WIP") || codeStatus.equals("Offline")) && (pageRight >= 6 ) ) { %>	
			
			<A href="#" onClick="return f_delete_fldAction(<%=fldActionId%>,<%=formLibId%>,'<%=pageRight%>')"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>
				
			<%}%>
			</td>
			
			</tr>
					
				<%	
						
						
				
				} // end of for for Field Actions	
			%>
			</table>
		</tr>
	</table>
			<br>				
		<!--****************** Sonia ****************  -->	
		<table width="99%" cellspacing="0" cellpadding="0" border="0" class="midAlign">
			<tr>
			<td width="40%" > <b><%=LC.L_Associated_Messages%><%-- Associated Messages*****--%> </b> </td>
				<% if((!(codeStatus.equals("Deactivated"))) && pageRight > 4){%>
				 <td colspan="2" align ="right"> <A href ="#" onclick="openWin(<%=formLibId%>,0, 'N',<%=pageRight%>,'<%=codeStatus%>','<%=mode%>')"><%//=LC.L_Add_New%><%-- ADD NEW*****--%><img title="<%=LC.L_NewMessage%>" src="./images/Create.gif" border ="0">  </td>
				<%}%>
			</tr>	
		</table>		 
      	<table width="99%" cellspacing="0" cellpadding="0" border="0" class="midAlign outline"> 
		    <tr>
		        <th width="45%"> <%=LC.L_Msg_Type%><%-- Message Type*****--%> </th>
	    	    <th width="55%"> <%=LC.L_Notify%><%-- Notify*****--%> </th>
		    	<th width="10%"><%=LC.L_Delete%><%-- Delete*****--%>  </th>
	      	</tr>
<%			
			for(count = 0;count<rows;count++)
			{	
				
				formNotifyId=(formNotifyIds.get(count)).toString() ;
				msgType=((msgTypes.get(count)) == null)?"-":(msgTypes.get(count)).toString();
				msgTypeVal = msgType;
				if (msgType.equals("P"))
				{
					msgType= LC.L_Popup_MsgType;/*msgType="Pop-up Message Type";*****/
				}
				else
				{
					if (msgType.equals("N"))
						msgType=MC.M_Notfic_SentByEmail;/*msgType="Notification sent by email";*****/
						
				}
				msgSendType=((msgSendTypes.get(count)) == null)?"-":(msgSendTypes.get(count)).toString();
				if (msgSendType.equals("F"))
				{
					msgSendType=MC.M_UserSubmit_FirstTime;/*msgSendType="User Submitting First Time";*****/
				}
				else 
				{
					if (msgSendType.equals("E"))
						msgSendType= MC.M_UseSbmt_Time;/*msgSendType="User Submitting Every Time";*****/
				}
				
				//msgText=((msgTexts.get(count)) ==null)?"-":(msgTexts.get(count)).toString();

				
				if ((count%2)==0) 
				{
%>
      				<tr class="browserEvenRow"> 
<%

				}

				else
				{
%>
			      <tr class="browserOddRow"> 
<%
				}
%>				
				<td> <A href="#" onclick="openWin(<%=formLibId%>,<%=formNotifyId%>,'M',<%=pageRight%>,'<%=codeStatus%>','<%=mode%>')"> <%= msgType%> </A> </td>
				<td> <%= msgSendType%> </td>
				<td>
				<% if (msgTypeVal.equals("P") && (codeStatus.equals("Freeze") || codeStatus.equals("Active") )) {} else {%>
					<A href="javascript:void(0)" onClick="f_delete_formNotify('<%=formNotifyId%>','<%=pageRight%>')"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0" align="left"/></A>
				<%}%>
				</td>
				
			
				</tr>
								
				
				
<%
			}  
%>


</table>
<br>
		<%if(calledFrom.equals("St"))
			{%>
		<A href="studyprotocols.jsp?srcmenu=tdmenubaritem3&selectedTab=7&mode=<%=mode%>&studyId=<%=studyId%>&calledFrom=<%=calledFrom%>"><%=MC.M_BackTo_StdSetupPage%><%-- Back to the <%=LC.Std_Study%> Setup page*****--%></A>
		<%} 
		if(calledFrom.equals("L"))
		{%>
		<A href ="formLibraryBrowser.jsp?selectedTab=3&srcmenu=<%=src%>"><%=MC.M_BackTo_FrmLib%><%-- Back to the Form Library*****--%> </A>
		<%}
		 if(calledFrom.equals("A"))
		{%>
		<A href ="accountforms.jsp?selectedTab=&srcmenu=tdMenuBarItem2"><%=MC.M_BackTo_MgmtBrowser%><%-- Back to the Form Management browser*****--%></A>
		<%}%>
	<input type="hidden" name="codeStatus" value=<%=codeStatus%>>
</Form>
<%	
		
		} //end of formId=0
				
		} //end of if of page rights
		
		else
		{
%>
			<jsp:include page="accessdenied.jsp" flush="true"/>
<%
		}
		
%>
	
<%
	} // end of if of session time out 
	else
	{
%>
	<jsp:include page="timeout.html" flush="true"/>	
<%
	}
%>
	<div class = "myHomebottomPanel"> 
	<jsp:include page="bottompanel.jsp" flush="true"/>
	</div>
</div>
	<div class ="mainMenu" id="emenu"> <jsp:include page="getmenu.jsp" flush="true"/></div>
</body>
</html>


