<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "java.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,java.sql.*,com.velos.eres.business.common.*,com.velos.eres.web.user.UserJB"%>
<%!
private String htmlEncode(String str) {
    return StringUtil.htmlEncodeXss(StringUtil.trueValue(str))
    .replaceAll("\\(","&#40;").replaceAll("\\)","&#41;");
}
private String htmlDecode(String str) {
    return StringUtil.htmlDecode(StringUtil.trueValue(str))
    .replaceAll("&#40;","\\(").replaceAll("&#41;","\\)").replaceAll("&quot;","\"");
}
private static final String LAST_CRITERIA = "LAST_CRITERIA";
private static final String LAST_SEARCH_STR = "LAST_SEARCH_STR";
private static final String CURROUT = "currout";
private static final String EMPTY_STRING = "";
%>
<%
try {
	if (!sessionmaint.isValidSession(request.getSession(false))) {
		out.println(EMPTY_STRING);
		return;
	}
} catch(Exception e) {}
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String src="tdMenuBarItem3";
	int ienet = 2;
	String agent1 = request.getHeader("USER-AGENT");
   	if (agent1 != null && agent1.indexOf("MSIE") != -1) {
     	ienet = 0; //IE
    } else {
		ienet = 1;
	}
	String pagenum = "";
	String ignoreRights = "";
	int curPage = 0;
	long startPage = 1;
	long cntr = 0;
	String pagenumView = "";
	int curPageView = 0;
	long startPageView = 1;
	long cntrView = 0;
	String tempSearch="";
	LookupDao lookupDao = new LookupDao();
	String ddlookup ="";
	pagenum = request.getParameter("page");
	if (pagenum == null){
		pagenum = "1";
	}
	curPage = EJBUtil.stringToNum(pagenum);
	String orderBy = "";
	orderBy = request.getParameter("orderBy");
	String orderType = "";
	orderType = request.getParameter("orderType");
	if (orderType == null){
		orderType = "asc"; /*YK 03JAN -  BUG # 5710*/
	}
	if (orderBy==null){
		orderBy="1";
	}
	pagenumView = request.getParameter("pageView");
	if (pagenumView == null){
		pagenumView = "1";
	}
	curPageView = EJBUtil.stringToNum(pagenumView);
	String orderByView = "";
	orderByView = request.getParameter("orderByView");
	String orderTypeView = "";
	orderTypeView = request.getParameter("orderTypeView");
	if (orderTypeView == null){
		orderTypeView = "asc"; /*YK 03JAN -  BUG # 5710*/
	}
	String orderBy_str="";
	if (!orderBy.equals("") ){
		orderBy_str = " order by " + orderBy+" " +orderType; 
	}else {
		orderBy_str =  "";
	}
	System.out.println("orderBy_str="+orderBy_str);
	String search=request.getParameter("search");
	if ("[VEL_NAV]".equals(search)) {
		search = StringUtil.htmlDecode(request.getParameter("search_for_nav"));
	}
	if (search==null) search=""; else search=search.replace('~','%');
	String formname=request.getParameter("form");
	String fDatacolumn=request.getParameter("fdatacolumn");
	String fDispcolumn=request.getParameter("fdispcolumn");
	String fCalculate = request.getParameter("fcalculate");
	String fCalcexpr = request.getParameter("fcalcexpr");
	String fkeyword_str=request.getParameter("keyword");
	String dfilter=FilterUtil.validateDFilter(request);
	String dfilterReq=request.getParameter("dfilter");
	if (dfilter!=null){
		if (dfilter.length()>0){
			dfilter=dfilter.replace('~','%');
		}
	} //to be check and remove unwanted bracket
	String search_data=request.getParameter("search_data");
	String search_data_req=request.getParameter("search_data");
	String search_data_val=request.getParameter("search_data_val");
	if ((search_data_req == null || search_data_req.length() == 0)
	        && search_data_val != null && search_data_val.length() > 0) {
	    search_data_req = htmlDecode(search_data_val);
	    search_data = htmlDecode(search_data_val);
	}
	if (search_data==null) search_data="";
	search_data=search_data.toLowerCase();
	search_data=FilterUtil.sanitizeText(search_data);
	String scriteria=request.getParameter("scriteria");
	if (scriteria==null) scriteria="";
	String lookup_column="",dfilter_search="",search_str="",criteria_str="";	
	String lookup_column_val=request.getParameter("lookup_column");  
	criteria_str=request.getParameter("criteria_str");
	if (criteria_str==null) criteria_str="";
	if (CURROUT.equals(request.getParameter("lookup_filter")) && criteria_str.length() == 0) {
	    search_data_req = search_data = "";
	}
	HttpSession tSession = request.getSession(true); 
	int vlookup=0;
	String userId = (String) tSession.getValue("userId");
	String accountId=(String) tSession.getValue("accountId");
	UserJB user = (UserJB) tSession.getValue("currentUser");	
	String orgId=user.getUserSiteId();;
	orgId=(orgId==null)?"":orgId;
	if (lookup_column_val!=null){
		if ((lookup_column_val.equals("0")) || (lookup_column_val.length()==0)){
			vlookup = 0;
		} else {				
			vlookup = EJBUtil.stringToNum((request.getParameter("lookup_column"))
					.substring(0,((request.getParameter("lookup_column")).indexOf("~"))));
		}
	}
	int pageRight = 0,index=-1;
	String outputStr="", tempOutputStr="" ,viewName="";
	ignoreRights = (String) tSession.getValue("pp_ignoreAllRights") ;
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
	if (grpRights!=null) {	
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
	}
	
	//for patient portal
	if (StringUtil.isEmpty(ignoreRights)) {
		ignoreRights = "false";
	}
	if (ignoreRights.equals("true")) {
		pageRight = 7;
		accountId=(String) tSession.getValue("pp_accountId");
	}
		
	String viewId=(request.getParameter("viewId"));
	viewId=(viewId==null)?"":(viewId);
	if (viewId.length()==0)	{
		viewName=request.getParameter("viewName");
		System.out.println("viewName"+viewName);		
		viewId=lookupDao.getViewId(viewName);
	}
	lookupDao.getViewDetail(EJBUtil.stringToNum(viewId));	
	String viewFilter=lookupDao.getViewFilter();
	viewFilter=(viewFilter==null)?"":viewFilter;
	if (viewFilter.indexOf("[:ACCID]")>=0) 
		viewFilter=StringUtil.replace(viewFilter,"[:ACCID]",accountId);
	if (viewFilter.indexOf("[:USERID]")>=0) 
		viewFilter=StringUtil.replace(viewFilter,"[:USERID]",userId);
	if (viewFilter.indexOf("[:ORGID]")>=0) 
		viewFilter=StringUtil.replace(viewFilter,"[:ORGID]",orgId);	
	//System.out.println("viewid in getlookup.jsp"+viewId);
	lookupDao.getLookupView(viewId,"lookup_column",vlookup,"");
	lookupDao.getReturnValues(viewId);
	ddlookup = lookupDao.getLookupDropDown();
	String retDisp=lookupDao.getRetDispValue();
	String retData=lookupDao.getRetDataValue();
	ArrayList lKeyword=lookupDao.getLViewRetKeywords();
	ArrayList lColumns=lookupDao.getLViewRetColumns();
	ArrayList lkpCollen=lookupDao.getLViewColLen();
	ArrayList lkpIsDisp=lookupDao.getLViewIsDisplay();
	ArrayList retKeyword=new ArrayList();
	ArrayList retFields=new ArrayList();
	ArrayList urlKeywords=new ArrayList();
	ArrayList urlFields=new ArrayList();
	ArrayList dbKeywords=lookupDao.getLViewRetKeywords();
	ArrayList dbColumns=lookupDao.getLViewRetColumns();
	ArrayList genOutput=new ArrayList();
	ArrayList tempCol=new ArrayList();
	//populate return keywords arraylist
	StringTokenizer st_sep=null;
	StringTokenizer st = new StringTokenizer(fkeyword_str,"~");
	while (st.hasMoreTokens()) {
		st_sep= new StringTokenizer(st.nextToken(),"|");
		urlFields.add(st_sep.nextToken());
		urlKeywords.add((String)st_sep.nextToken());		 
	}
	
	for (int i=0;i<urlKeywords.size();i++)	{
		//System.out.println(i + (String)urlKeywords.get(i));	
		if ((dbKeywords.contains(urlKeywords.get(i))) || (((String)urlKeywords.get(i))).indexOf("[VELEXPR]") >= 0) {
			index=dbKeywords.indexOf(urlKeywords.get(i));
			//System.out.println(index);
			if ( index>=0 ) {
				outputStr="[field]"+urlFields.get(i)+"[keyword]"+urlKeywords.get(i)+"[dbcol]"+dbColumns.get(index);
				//System.out.println("adding value"+dbColumns.get(index));
				tempCol.add(((String)dbColumns.get(index)).toLowerCase());
			} else {
				outputStr="[field]"+urlFields.get(i)+"[keyword]"+urlKeywords.get(i)+"[dbcol]"+"[VELEXPR]";
				tempCol.add("[VELEXPR]");
			}  
			genOutput.add(outputStr);
		}  
	 }
	 //create seach string
	if (lookup_column_val!=null) {
		if ((lookup_column_val.equals("0")) || (lookup_column_val=="")) {}
		else{
			lookup_column=lookup_column_val.substring(((request.getParameter("lookup_column")).indexOf("~"))+1);
		}
	}
	if ((lookup_column_val!=null)&& (search_data.length()>0)){			
		if ((lookup_column_val.equals("0")) || (lookup_column_val=="")) {
		} else if (lookup_column.equals("[VELALL]")) {
			String tempStr="";
			ArrayList tempList=lookupDao.getLViewColumns();
			if (search.length()>0) search="("+search+")";
			tempSearch="";
			for (int i=0;i<tempList.size();i++){ 
				tempStr=(String)tempList.get(i);
				if (scriteria.equals("contains")) {	
				if (tempSearch.length()>0) {
					tempSearch= tempSearch + "  or (lower("+tempStr+") like  lower('%"+search_data.trim() +"%'))";
				} else {
					tempSearch=" (lower("+tempStr+") like  lower('%"+search_data.trim() +"%'))";}
				}
				if (scriteria.equals("isequalto")) {
					if(tempSearch.length()>0) {
						tempSearch= tempSearch + " or (lower(" + tempStr+") =  lower('"+search_data.trim() +"'))";
					} else {
						tempSearch = " (lower(" + tempStr+") =  lower('"+search_data.trim() +"'))";
					}
				}
				if (scriteria.equals("start"))	{ 
					if(tempSearch.length()>0){
						tempSearch=tempSearch + " or (lower(" + tempStr+") like  lower('"+search_data.trim() +"%'))" ; 
					} else {
						tempSearch= "  (lower(" + tempStr+") like  lower('"+search_data.trim() +"%'))" ; 
					}
				}
				if (scriteria.equals("ends")){	
					if(tempSearch.length()>0) {
						tempSearch= tempSearch + " or (lower(" + tempStr+") like lower('%"+search_data.trim() +"'))" ;
					} else {
						tempSearch = " (lower(" + tempStr+") like lower('%"+search_data.trim() +"'))" ;
					}
				}
			} //end for loop of templist	
			if (tempSearch.length()>0) tempSearch="("+tempSearch+")";
		} //end check for (lookup_column.equals("[VELALL]")
		else {
			if (scriteria.equals("contains")){	
				if (search.length()>0) {
					search= search + "  and lower("+lookup_column+") like  lower('%"+search_data.trim() +"%')";
				}else{
					search=" lower("+lookup_column+") like  lower('%"+search_data.trim() +"%')";
				}
			}
			if (scriteria.equals("isequalto")){	
				if(search.length()>0) {
					search= search + " and lower(" + lookup_column+") =  lower('"+search_data.trim() +"')"; 
				}else{
					search = " lower(" + lookup_column+") =  lower('"+search_data.trim() +"')";}
			}
			if (scriteria.equals("start"))	{ 
				if(search.length()>0){
					search=search + " and lower(" + lookup_column+") like  lower('"+search_data.trim() +"%')" ; 
				} else {
					search= "  lower(" + lookup_column+") like  lower('"+search_data.trim() +"%')" ; 
				}
			}
			if (scriteria.equals("ends"))	{	
				if(search.length()>0) {
					search= search + " and lower(" + lookup_column+") like lower('%"+search_data.trim() +"')" ;
				} else {
					search = " lower(" + lookup_column+") like lower('%"+search_data.trim() +"')" ;
				}
			}
		} // end of else clause
	} // end of existence check
		
 	search=(search==null)?"":search;	   
  	if (search.length()>0){
		if (tempSearch.length()>0) search=search+" and "+tempSearch;
	} else {
		if (tempSearch.length()>0) search=tempSearch;      
	}
	if (search.length()>0) search_str=" and " + search;
	if (viewFilter.length()>0) search_str=search_str+ " and " + viewFilter;
	if (CURROUT.equals(request.getParameter("lookup_filter")) ||
	        criteria_str.indexOf("] AND [") > -1) {
  	    String lastCriteria = StringUtil.trueValue((String)tSession.getAttribute(LAST_CRITERIA));
  	    if (criteria_str.indexOf(lastCriteria) > -1) {
  	        if (criteria_str.equals(lastCriteria)) {
  	        	search_str = StringUtil.trueValue((String)tSession.getAttribute(LAST_SEARCH_STR));
  	        } else {
  	        	search_str += StringUtil.trueValue((String)tSession.getAttribute(LAST_SEARCH_STR));
  	        }
  	    }
  	}
  	if (criteria_str.length() > 0) {
		tSession.setAttribute(LAST_CRITERIA, criteria_str);
		tSession.setAttribute(LAST_SEARCH_STR, search_str);
  	}
  	if (dfilter!=null){
  		if (dfilter.length()>0){
  	  		dfilter_search=" and "+dfilter+search_str+orderBy_str; 	  
  	  	}   else {
  	  		dfilter_search=search_str+orderBy_str;
  	  	}	  	  
  	}
  	System.out.println("order_by="+orderBy+" order_type="+orderType+"; after complete:"+dfilter_search);  
	//end creating
	long rowsPerPage=0;
   	long totalPages=0;	
	long rowsReturned = 0;
	long showPages = 0;
	long totalRows = 0;	   
	long firstRec = 0;
	long lastRec = 0;	   
	ArrayList bColumns;
	String studyNumber = null;
	String studyTitle = null;
	String studyPhase = null;
	String studyStatus = null;
	String statusSubType = null;
	String studyStatusNote = null;
	String studyStatusDate = null;
	int studyId=0,dynsize=0;
	boolean hasMore = false;
	boolean hasPrevious = false;	
	String  study_verparent = "",retDispVal="",retDataVal="";
	rowsPerPage =  Configuration.ROWSPERBROWSERPAGE ;
	totalPages =Configuration.PAGEPERBROWSER ; 
	BrowserRows br = new BrowserRows();	     
	rowsPerPage=100;	br.getPageRows(curPage,rowsPerPage,totalPages,orderBy,orderType,EJBUtil.stringToNum(viewId),dfilter_search);
	bColumns=br.getBColumns();	
	dynsize=(100/(bColumns.size()-1));
	rowsReturned = br.getRowReturned();
	showPages = br.getShowPages();
	startPage = br.getStartPage();
	hasMore = br.getHasMore();
	hasPrevious = br.getHasPrevious();
	totalRows = br.getTotalRows();	    
	firstRec = br.getFirstRec();
	lastRec = br.getLastRec();	  	  
%>
<Form name="lookuppgResults" id="lookuppgResults" method="post" onsubmit="javascript:void(0);" >
	<input name="viewname" type="hidden" value="<%=lookupDao.getViewName()%>"/>
	<table width="90%" cellspacing="2" cellpadding="2" border="0" >
		<tr><td colspan="7">&nbsp;</td></tr>
		<tr>
			<td colspan="1">
			<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
				<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
			<% } else {  %>
				<font class="recNumber"><%=MC.M_NoRecordsFound%><%-- No Records Found*****--%></font>	
			<% }  %>
			</td>
			<td  colspan="5">
			<!--Added by Gopu to fix the bugzilla Issue #2640 -->
			<A href="#" onClick="setValue(document.lookuppgResults,'','remove')"><%=LC.L_Rem_SelectedRecord%><%-- Remove Selected Record*****--%></A>
			</td>
		</tr>
	</table> 
	<Input type="hidden" name="targetId" id="targetId" value="<%=StringUtil.htmlEncodeXss(request.getParameter("targetId"))%>">
	<Input type="hidden" name="srcmenu" value="<%=src%>">
	<Input type="hidden" name="page" value="<%=curPage%>">
 	<%-- YK 27Dec - UI-1 - Requirement--%>		
	<%
	orderType=orderType.equals("")?"asc":orderType;
	%>
	<Input type="hidden" name="orderBy" value="<%=orderBy%>">
	<Input type="hidden" name="orderType" value="<%=orderType%>">
	<Input type="hidden" name="totalrows" value="<%=totalRows%>">	
    <input name="viewId" type="hidden" value="<%=viewId%>">
    <input name="keyword" type="hidden" value="<%=fkeyword_str%>">
    <% if (lookup_column_val==null) lookup_column_val="";%>
    <input name="lookup_column_val" type="hidden" value="<%=lookup_column_val%>">	
    <input name="search_data_val" type="hidden" value="<%=htmlEncode(search_data_req)%>">		
    <input name="scriteria_val" type="hidden" value="<%=scriteria%>">		
    <input name="form" type="hidden" value="<%=formname%>">			
    <input name="fdatacolumn" type="hidden" value="<%=fDatacolumn%>">		
    <input name="fdispcolumn" type="hidden" value="<%=fDispcolumn%>">		
   	<input name="dfilter" type="hidden" value="<%=StringUtil.htmlEncodeXss(dfilterReq).replace('%','~').replaceAll("\\(","&#40;").replaceAll("\\)","&#41;")%>">			
	<input name="search" type="hidden" value="">
    <input name="fcalculate" type="hidden" value="<%=fCalculate%>">			
    <input name="fcalcexpr" type="hidden" value="<%=fCalcexpr%>">
    <input name="search_for_nav" type="hidden" value="<%=StringUtil.trueValue(StringUtil.htmlEncodeXss(search)).replace('%','~')%>">
    	
    <%-- YK 27Dec - UI-1 - Requirement Starts --%>			
    <table width="100%" cellspacing="0" cellpadding="0" style="border:1px solid #9D9D9D;" class="midalign">
    	<tr> 
        	<th width="5%" class="normalcolNo" height="20">&nbsp;</th>
<%	
	String arrowImg="";
	String title="";
	for (int count_column=0;count_column<(bColumns.size()-1);count_column++){
		if (lkpIsDisp.get(count_column).equals("Y")){
       	  		/*YK 03JAN -  BUG # 5710*/
       		if(orderBy.equals("")) {
				orderBy=String.valueOf(count_column+1);
			}
			if(orderBy.equals(String.valueOf(count_column+1))){
				arrowImg=(orderType.equals("desc"))?"./images/yuilookandfeel/sortdescending.png":"./images/yuilookandfeel/sortascending.png";
				title=(orderType.equals("desc"))?MC.M_Click_SortAscend/*"Click to sort ascending"*****/:MC.M_Click_SortDescend/*"Click to sort descending"*****/;
            %>
        		<th class="selectedcol"  title="<%=title %>" width="<%=lkpCollen.get(count_column)%>" onClick="setOrder(document.lookuppg,'<%=count_column+1%>' )"><%=(String)bColumns.get(count_column)%> <img class="headerImage" src="<%=arrowImg%>" alt="<%=LC.L_Arrow_Lower%><%--arrow--%>"/></th>
  			<% } else { %>
  				<th class="normalcol"   width="<%=lkpCollen.get(count_column)%>" onClick="setOrder(document.lookuppg,'<%=count_column+1%>' )"><%=(String)bColumns.get(count_column)%> </th>
			<%
  			}
		}
	}	
%>     
		</tr>	  
<%
	String normalrowCss="";
	String selectedColCss="";
	for(int counter = 1;counter<=rowsReturned;counter++) {
		outputStr="";	
		index=-1;
		if ((counter%2)==0) {
			normalrowCss="normalinfoeven";
			selectedColCss="selectedinfoeven";
		} else {
			normalrowCss="normalinfoodd";
			selectedColCss="selectedinfoodd";
		}
%>
			<tr class="normalinfo" height="25">
				<td  class="<%= normalrowCss%>"  >
				<!--Added by Gopu to fix the bugzilla Issue #2640 -->
				<a href="#" onClick="setValue(document.lookuppgResults,'<%=counter%>','');$j('#selectLookupDialog' ).dialog('destroy');"><%=LC.L_Select%></a> <%-- YK 03JAN -  BUG # 5709 --%>
				<%-- YK 27Dec - UI-1 - Requirement Ends --%>
      			</td>
<%
		for (int i=0;i<(tempCol.size());i++){
  			index=(bColumns).indexOf(((String)tempCol.get(i)).toUpperCase());	
			if (index>=0) {
				if (outputStr!=null){
					if (outputStr.length()>0) {
						outputStr=outputStr+"[end]"+genOutput.get(i) + "[value]"+(String)br.getBValues(counter,(String)bColumns.get(index));
					} else {
						outputStr=genOutput.get(i) + "[value]"+(String)br.getBValues(counter,(String)bColumns.get(index));
					}
				}
			}
		}
		for (int i=0;i<(bColumns.size()-1);i++){
			String tempStr=(String)br.getBValues(counter,(String)bColumns.get(i));
			if (tempStr!=null && !tempStr.equals("null")){ //KM-03/10/2008
				tempStr=(String)br.getBValues(counter,(String)bColumns.get(i));
			} else {	
				tempStr="-";
			}  
			//check if the colums is defined hidden,hide it if it is
			if (lkpIsDisp.get(i).equals("Y")){ 
				/*YK 27Dec - UI-1 - Requirement Starts*/
				if(orderBy.equals(String.valueOf(i+1))){
				%>	 
				<td class="<%= selectedColCss%>" width="<%=lkpCollen.get(i)%>"> <%=tempStr%> </td>
				<%
				}else{
				%>	 
				<td class="<%= normalrowCss%>"  width="<%=lkpCollen.get(i)%>"> <%=tempStr%> </td>
				<%
				}
  				/* YK 27Dec - UI-1 - Requirement Ends */
   	  		} else {
     			%>
     			<Input name="<%=(String)bColumns.get(i)%>" type="hidden" value="<%=tempStr%>">
     			<%
			}
		} //end for (int i=0;i<(bColumns.size()-1);i++)
		tempOutputStr="";
		for (int z=0;z<tempCol.size();z++){
			if (tempCol.get(z).equals("[VELEXPR]")) {	 
				if (tempOutputStr!=null){
					if (tempOutputStr.length()>0) { 
						tempOutputStr=tempOutputStr+"[end]"+genOutput.get(z)+"[value]"+"";
					} else { 
						tempOutputStr="[end]"+genOutput.get(z)+"[value]"+"";
					}
				}
			}
		}
		retDataVal=StringUtil.encodeString(retDataVal);  
		retDispVal=StringUtil.encodeString(retDispVal);
		outputStr=outputStr + tempOutputStr;
		outputStr=StringUtil.encodeString(outputStr);
%>
      <input type="hidden" value="<%=outputStr%>" name="totStr">
      </tr>
<%
	}
%>
    </table>
	<input type="hidden" name="outputString" value='<%=outputStr%>'> 	
	<table>
		<tr>
			<td>
<% 
	if (totalRows > 0) {
		Object[] arguments = {firstRec,lastRec,totalRows};
%>
			<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
<%  } else { %>
			<font class="recNumber"><%=MC.M_NoRecordsFound%><%-- No Records Found*****--%></font>	
<%  }  %>	
			</td>
		</tr>
	</table>	
	<table align=center>
		<tr>
<%
	if (curPage==1) startPage=1;
	for (int count = 1; count <= showPages;count++)	{
		cntr = (startPage - 1) + count;
		if ((count == 1) && (hasPrevious)) {
%>
			<td colspan = 2>
				<A style="font-size:10pt;font-weight:bold;" href="javascript:void(0);" onclick="navigateLookupResults('getlookupmodalresults.jsp?criteria_str=<%=criteria_str%>&scriteria=<%=scriteria%>&lookup_column=<%=lookup_column_val%>&srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&viewId=<%=viewId%>&dfilter=<%=StringUtil.htmlEncodeXss(dfilterReq).replace('%','~').replaceAll("\\(","&#40;").replaceAll("\\)","&#41;")%>&form=<%=formname%>&fdatacolumn=<%=fDatacolumn%>&fdispcolumn=<%=fDispcolumn%>&keyword=<%=fkeyword_str%>');">< <%=LC.L_Previous%><%-- Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
			</td>	
<%		}   %>
			<td>
<%
		if (curPage  == cntr) {
%>
			<FONT class="pageNumber"><%=cntr%></Font>
<%      } else {    %>		
			<A style="font-size:10pt;font-weight:bold;" href="javascript:void(0);" onclick="navigateLookupResults('getlookupmodalresults.jsp?criteria_str=<%=criteria_str%>&scriteria=<%=scriteria%>&lookup_column=<%=lookup_column_val%>&srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&viewId=<%=viewId%>&dfilter=<%=StringUtil.htmlEncodeXss(dfilterReq).replace('%','~').replaceAll("\\(","&#40;").replaceAll("\\)","&#41;")%>&form=<%=formname%>&fdatacolumn=<%=fDatacolumn%>&fdispcolumn=<%=fDispcolumn%>&keyword=<%=fkeyword_str%>');"><%=cntr%></A>
<%		}  %>
			</td>
<%
	}
	if (hasMore) {   
%>
			<td colspan = 3 align = center>&nbsp;&nbsp;&nbsp;&nbsp;
			<A style="font-size:10pt;font-weight:bold;" href="javascript:void(0);" onclick="navigateLookupResults('getlookupmodalresults.jsp?criteria_str=<%=criteria_str%>&scriteria=<%=scriteria%>&lookup_column=<%=lookup_column_val%>&srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&viewId=<%=viewId%>&dfilter=<%=StringUtil.htmlEncodeXss(dfilterReq).replace('%','~').replaceAll("\\(","&#40;").replaceAll("\\)","&#41;")%>&form=<%=formname%>&fdatacolumn=<%=fDatacolumn%>&fdispcolumn=<%=fDispcolumn%>&keyword=<%=fkeyword_str%>');">< <%=LC.L_Next%><%-- Next*****--%> <%=totalPages%>></A>
			</td>
<%  }  %>
		</tr>
	</table>
</Form>
<br>
