<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html>

<head>

<title><%=MC.M_Copy_FromFldLib%><%--Copy From Field Library*****--%> </title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<%-- Nicholas : Start --%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<%--<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>--%>
<%-- Nicholas : End --%>
<script>
function openWin(formobject,fldLibId,tmpFldDataType)
{

	formobject.fieldLibId.value = fldLibId ;
	formobject.fldDataType.value =tmpFldDataType ;

	param1="copyField.jsp?&mode=M" ;
	windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=600");
	windowName.focus();




}



 function setSearchFilters(form)
 {


 	  form.nameSearch.value=form.name.value;
 	  form.keywordSearch.value=form.keyword.value;
	  form.categorySearch.value=form.category.value;



 }
</script>

<script type="text/javascript">

//JM: 08Dec2009: #4272

var v=0, w=0;

function fnEnterKeyPressOnce(e){


var keyId = e.keyCode;

   switch(keyId){

      case 13:

		  v=v+1;
		  if (v>1) return false;


      break;


   }

}

function fnHrefClickOnce(e){


 w=w+1;

 if (w>1){
	return false;
 }

}

</script>


<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="fieldLibJB" scope="request"  class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id="catLibJB" scope="request" class="com.velos.eres.web.catLib.CatLibJB"/>

<%@ page language = "java" import = "com.velos.eres.business.group.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.business.user.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB"%>

<% int pageRight = 7;

   HttpSession tSession = request.getSession(true);

%>

<BR>
<%
int ienet = 2;
String agent1 = request.getHeader("USER-AGENT");
if (agent1 != null && agent1.indexOf("MSIE") != -1)
   	ienet = 0; //IE
else
	ienet = 1;

if (ienet == 0) {
%>
<body style="overflow:scroll">
<%} else {%>
<body>
<%}%>





<DIV class="popDefault" id="div1">

<%
	if (sessionmaint.isValidSession(tSession))

	{

		int accId = EJBUtil.stringToNum((String) (tSession.getValue("accountId")));
		String fldLibId="";
		String fkCategory="";
		String fldName="";
		String fkFormId="";
		String fldUniqueId="";
		String fldDescription="";
		String fldType="";
		String fldKeyword="";
		String catLibName="";
		String fldDataType="";
		String tmpFldDataType= "";
		int counter = 0;
		int len=0;
		int fkCatlib = 0 ;

		String uName = (String) tSession.getValue("userName");

		String accountId=(String)tSession.getAttribute("accountId");



		////////SEARCH FILTERS

	  	String  nameSearch = "";
	 	String  keywordSearch = "";
	  	String  categorySearch = "";

		if (( request.getParameter("nameSearch"))!= null &&  (request.getParameter("nameSearch").length() > 0)  )		    nameSearch= request.getParameter("nameSearch");
   		if (( request.getParameter("keywordSearch"))!= null &&  (request.getParameter("keywordSearch").length() > 0)  )		    keywordSearch= request.getParameter("keywordSearch");
   		if (( request.getParameter("categorySearch"))!= null &&  (request.getParameter("categorySearch").length() > 0)  )		    categorySearch= request.getParameter("categorySearch");


		fldName= request.getParameter("name");
		if (  nameSearch != null || (!nameSearch.equals("null")  )   ) fldName = nameSearch ;

		String mode= request.getParameter("mode");
		fkCategory= request.getParameter("category");
		if ( ( categorySearch != null )   || (   !categorySearch.equals("null")  )  ) fkCategory = categorySearch ;
		fkFormId= request.getParameter("fkFormId");





		fldKeyword= request.getParameter("keyword");
		if ( keywordSearch != null || (!keywordSearch.equals("null") )   )   fldKeyword = keywordSearch ;

		int tempAccountId=EJBUtil.stringToNum(accountId);
		tempAccountId=EJBUtil.stringToNum(accountId);
		fkCatlib = EJBUtil.stringToNum(fkCategory);

			if (fldName == null )
		{
			fldName="";
		}

		if (fldKeyword == null )
		{
			fldKeyword="" ;
		}
		///FOR HANDLING PAGINATION
	String pagenum = "";
	int curPage = 0;
	long startPage = 1;
	String stPage;
	long cntr = 0;


	pagenum = request.getParameter("page");

	if (pagenum == null)
	{
		pagenum = "1";
	}
	curPage = EJBUtil.stringToNum(pagenum);

	String orderBy = "";
	orderBy = request.getParameter("orderBy");


	String orderType = "";
	orderType = request.getParameter("orderType");
	String str2 ="";
	String str3 ="";
	String str4 ="";
	String str5 ="";
	String str6 ="";
	String str7 ="";
	String str1 ="";
	String count1="";
	String count2="";

	if (orderType == null)
	{
		orderType = "";
	}

			 str1="SELECT NVL(FL.PK_FIELD, -999)   PK_FIELD,  FL.FK_CATLIB FK_CATLIB , NVL(FL.FLD_LIBFLAG,'-') FLD_LIBFLAG ,"
						+"  NVL(FL.FLD_NAME,'-' ) FLD_NAME , case when (nvl(length(FLD_DESC),0)<100) then NVL(FLD_DESC,'-')"
						+"  when (length(FLD_DESC)>=100) then substr(FLD_DESC,1,100)||'...' end	 as  FLD_DESC, NVL(FL.FLD_UNIQUEID,'-') FLD_UNIQUEID , "
						+"  NVL(FL.FLD_SYSTEMID,'-') FLD_SYSTEMID, NVL(FL.FLD_KEYWORD, '-') FLD_KEYWORD,NVL(FL.FLD_TYPE, '-') FLD_TYPE,"
						+"  NVL(FL.FLD_DATATYPE,'-' ) FLD_DATATYPE,NVL(FL.FLD_INSTRUCTIONS,'-') FLD_INSTRUCTIONS,NVL(FL.FLD_FORMAT,'-') FLD_FORMAT ,"
						+"  NVL(FL.FLD_REPEATFLAG,0)FLD_REPEATFLAG , CAT.CATLIB_NAME CATLIB_NAME" ;

			 str2="  FROM ER_FLDLIB FL ,  ER_CATLIB CAT  WHERE FL.FK_ACCOUNT=  "+tempAccountId
						+" AND  FL.RECORD_TYPE <> 'D'   AND CAT.RECORD_TYPE <> 'D'  "
						+" AND   FL.FK_CATLIB = CAT.PK_CATLIB  "
						+" AND FL.FLD_LIBFLAG = 'L' ";


			if ( !fldName.equals(""))
			{
				 str3=" AND lower(FLD_NAME) LIKE lower('%"+fldName.trim()+"%')";
			}
			else
			{
				 str3= " ";
			}

			if ( !fldKeyword.equals(""))
			{
			   str4=" AND lower(FLD_KEYWORD)  LIKE lower('%"+fldKeyword.trim()+"%') " ;
			}
			else
			{
				str4=" ";
			}

			if ( fkCatlib == 0 || fkCatlib == -1 )
			{
			  	str5= "  " ;

			}
			else
			{
				str5 = " AND FK_CATLIB = "+fkCatlib;
			}


			if ( fkCatlib == 0 || fkCatlib == -1 )
			{
			  	str6= "  UNION  (   SELECT  NULL   , PK_CATLIB  ,NULL, "
					+ "  NULL 	, NULL , NULL ,"
					+ "  NULL , NULL , NULL , "
					+ "  NULL , NULL , NULL , "
					+ "  NULL , CATLIB_NAME  "
					+ "  FROM ER_CATLIB  WHERE FK_ACCOUNT =  " +tempAccountId
					+ "  AND CATLIB_TYPE='C' AND RECORD_TYPE <> 'D'  "
					+ "  AND PK_CATLIB  NOT IN ( SELECT FK_CATLIB "
					+ "	 FROM ER_FLDLIB F , ER_CATLIB C    "
					+ "  WHERE   F.FK_ACCOUNT  = "+tempAccountId +"   AND "
					+ "  F.RECORD_TYPE <> 'D' AND FK_CATLIB = PK_CATLIB  )  ) " ;

			}
			else
			{
				str6 = "  ";
			}



			str7="order by  ( FK_CATLIB )    " ;


			String finalLibSql = str1+str2+str3+str4+str5+str6+str7 ;


			String countSql = "";

			if ( fkCatlib == 0 || fkCatlib == -1 )
			{
					if (     ! fldName.equals("")  )
					{
						count1=" AND lower(FLD_NAME) LIKE lower('%"+fldName.trim()+"%')";
					}
					else
					{
					 count1= " ";
					}
					if ( !fldKeyword.equals(""))
					{
				 	  count2=" AND lower(FLD_KEYWORD)  LIKE lower('%"+fldKeyword.trim()+"%') " ;
					}
					else count2="";

					countSql = "select count(*) from  (   "
								+"  SELECT		 NVL(FL.PK_FIELD, -999)   PK_FIELD,  FL.FK_CATLIB   "
								+"	FROM ER_FLDLIB  FL ,  ER_CATLIB CAT  WHERE FL.FK_ACCOUNT=  " +tempAccountId
								+"	AND  FL.RECORD_TYPE <> 'D'     AND CAT.RECORD_TYPE <> 'D'    "
								+ " AND   FL.FK_CATLIB = CAT.PK_CATLIB  AND FL.FLD_LIBFLAG = 'L'   "   + count1 + count2
								+"  UNION  (   SELECT  NULL   , PK_CATLIB   FROM ER_CATLIB  WHERE FK_ACCOUNT = "+ tempAccountId
								+"  AND CATLIB_TYPE='C' AND RECORD_TYPE <> 'D'   "
								+"  AND PK_CATLIB  NOT IN  "
								+" ( SELECT FK_CATLIB     FROM ER_FLDLIB F , ER_CATLIB C    WHERE   F.FK_ACCOUNT  = " + tempAccountId
								+" AND   F.RECORD_TYPE <> 'D'   "
								+ "AND FK_CATLIB = PK_CATLIB  )   )    )  "   ;
			}
			else
			{
					if (     ! fldName.equals("")  )
					{
						count1=" AND lower(FLD_NAME) LIKE lower('%"+fldName.trim()+"%')";
					}
					else
					{
					 count1= " ";
					}
					if ( !fldKeyword.equals(""))
					{
				 	  count2=" AND lower(FLD_KEYWORD)  LIKE lower('%"+fldKeyword.trim()+"%') " ;
					}
					else
					{ count2= ""; }


				countSql = "SELECT  count( PK_FIELD)  "
								+"	FROM ER_FLDLIB   FL ,  ER_CATLIB CAT  WHERE FL.FK_ACCOUNT = " +   tempAccountId
								+" AND  FL.RECORD_TYPE <> 'D'   "
								+" AND CAT.RECORD_TYPE <> 'D'  "
								+" AND   FL.FK_CATLIB = CAT.PK_CATLIB  "
								+" AND FL.FLD_LIBFLAG = 'L'     AND FL.FK_CATLIB = " + fkCatlib+count1+count2 ;
			}



	   long rowsPerPage=0;
   	   long totalPages=0;
	   long rowsReturned = 0;
	   long showPages = 0;

	   boolean hasMore = false;
	   boolean hasPrevious = false;
	   long firstRec = 0;
	   long lastRec = 0;
	   long totalRows = 0;

		rowsPerPage =  Configuration.MOREBROWSERROWS ;
		totalPages =Configuration.PAGEPERBROWSER ;



       BrowserRows br = new BrowserRows();
       br.getPageRows(curPage,rowsPerPage,finalLibSql,totalPages,countSql,orderBy,"null");
   	   rowsReturned = br.getRowReturned();
	   showPages = br.getShowPages();
	   startPage = br.getStartPage();
	   hasMore = br.getHasMore();
	   hasPrevious = br.getHasPrevious();
	   totalRows = br.getTotalRows();
	   firstRec = br.getFirstRec();
	   lastRec = br.getLastRec();


//////////////////////////////////////////////////////////////// END PAGINATION



		//here we will get all the categories

		String catLibType="C";
		CatLibDao catLibDao= new CatLibDao();
		ArrayList categoryIds= new ArrayList();
		ArrayList categoryDesc= new ArrayList();
		String categoryPullDown;
		int forLastAll = 0 ;
		FieldLibDao fieldLibDao = new FieldLibDao();

		if ( mode.equals("initial"))
		{
			mode= "final";



			catLibDao= catLibJB.getCategoriesWithAllOption(tempAccountId,catLibType, forLastAll);

			categoryIds = catLibDao.getCatLibIds();
			categoryDesc= catLibDao.getCatLibNames();
			categoryPullDown=EJBUtil.createPullDown("category", -1, categoryIds, categoryDesc);




		}
		else
		{
			int tempfkCategory= 0;
			int tempfkFormId=0;

			if (fkCategory != null)
			tempfkCategory=EJBUtil.stringToNum(fkCategory);

			if (fkFormId !=null)
			 tempfkFormId= EJBUtil.stringToNum(fkFormId);



			//to create the Category Pull Down
			catLibDao.getCategoriesWithAllOption(tempAccountId,catLibType,forLastAll);
			categoryIds = catLibDao.getCatLibIds();
			categoryDesc= catLibDao.getCatLibNames();
			categoryPullDown=EJBUtil.createPullDown("category", tempfkCategory, categoryIds, categoryDesc);



		}



		if (pageRight > 0 )

   		{

%>


			<br>
		    <Form name="fieldLibrary" action="copyFromFieldLibrary.jsp?page=1&mode=final"  method="post" onSubmit= "setSearchFilters(document.fieldLibrary)">

			<Input type="hidden" name="mode" value=<%=mode%> >
			<Input type="hidden" name="page" value="<%=curPage%>">
			<Input type="hidden" name="orderBy" value="<%=orderBy%>">
			<Input type="hidden" name="orderType" value="<%=orderType%>">
			<Input type="hidden" name="nameSearch" >
			<Input type="hidden" name="keywordSearch">
			<Input type="hidden" name="categorySearch">


			<table width="100%" cellspacing="0" cellpadding="0" border="0" class="basetbl">
   				 <tr>
				      <td width="10%"><%=LC.L_Category%><%--Category*****--%></td>
			          <td width="20%"><%=categoryPullDown%> </td>
					   <td width="10%"><%=LC.L_Fld_Name%><%--Field Name*****--%></td>
			          <td width="19%"><input type="text" name="name" size = 20 MAXLENGTH = 50 value="<%=fldName%>"> </td>
					  <td width="7%"><%=LC.L_Keyword%><%--Keyword*****--%> </td>
			          <td width="20%"><input type="text" name="keyword" size = 25 MAXLENGTH = 50 value="<%=fldKeyword%>"> </td>
					  <td width ="20%"><button type="submit"><%=LC.L_Search%></button></td>
	  			</tr>
				</table>

				<br>
				<br>
<%-- Nicholas : Start --%>
				<table width="100%" cellspacing="1" cellpadding="0" border="0" class="basetbl outline midalign">
<%-- Nicholas : End --%>
				    <tr>
				        <th width="10%"> <%=LC.L_Category%><%--Category*****--%> </th>
				        <th width="17%"> <%=LC.L_Fld_Name%><%--Field Name*****--%> </th>
				        <th width="10%"> <%=MC.M_FldID_InTheLib%><%--Field ID (in the Library)*****--%> </th>
						<th width="18%"><%=LC.L_Description%><%--Description*****--%> </th>
						<th width="13%"><%=LC.L_Fld_Type%><%--Field Type*****--%> </th>
						<th width="8%"> <%=LC.L_Copy_Upper%><%--COPY*****--%>  </th>
					</tr>


		<%
					String tempStr2 = "";
					String catLibNameHref="";
					String tempStr1 = "";
					String deleteHref = "";
					String fldNameHref="";
					String copyHref="";
					for(counter = 1;counter<=rowsReturned;counter++)
					{
						mode = "M";



						tempStr1= br.getBValues(counter,"FK_CATLIB");

						fldLibId= br.getBValues(counter,"PK_FIELD");
						fldLibId = (  fldLibId==null )?"0":(  fldLibId) ;

						if ( counter !=1 )
						{
						  tempStr2 = br.getBValues(counter-1,"FK_CATLIB");
						 }


						if (   ( counter == 1 ) || ( ! tempStr1.equals(tempStr2 ) )    )
						{

							catLibName=  br.getBValues(counter,"CATLIB_NAME");

						}
						else
						{
							catLibName="&nbsp;&nbsp";
						}

						fldName = (     ( br.getBValues(counter,"FLD_NAME")  ) == null)?"-":(  br.getBValues(counter,"FLD_NAME")   );

						fldUniqueId = (  (br.getBValues(counter,"FLD_UNIQUEID")   ) == null)?"-":br.getBValues(counter,"FLD_UNIQUEID") ;

						fldDescription = (  (br.getBValues(counter,"FLD_DESC")      ) == null)?"-":br.getBValues(counter,"FLD_DESC") ;

						tmpFldDataType=(   ( br.getBValues(counter,"FLD_DATATYPE")    ) == null)?"-":br.getBValues(counter,"FLD_DATATYPE") ;

						if (tmpFldDataType.equals("ET") )
						{ fldDataType=LC.L_Text;/*fldDataType="Text";*****/ }
						if (tmpFldDataType.equals("EN") )
						{ fldDataType=LC.L_Number;/*fldDataType="Number";*****/ }
						if (tmpFldDataType.equals("ED") )
						{ fldDataType=LC.L_Date;/*fldDataType="Date";*****/ }
						if (tmpFldDataType.equals("EI") )
						{ fldDataType=LC.L_Time;/*fldDataType="Time";*****/ }
						if (tmpFldDataType.equals("ML") )
						{ fldDataType=LC.L_Lookup;/*fldDataType="Lookup";*****/ }
						if (tmpFldDataType.equals("MD") )
						{ fldDataType=LC.L_Dropdown;/*fldDataType="Dropdown";*****/ }
						if (tmpFldDataType.equals("MC") )
						{ fldDataType=LC.L_Checkbox;/*fldDataType="Checkbox";*****/ }
						if (tmpFldDataType.equals("MR") )
						{ fldDataType=LC.L_Radio_Button;/*fldDataType="Radio Button";*****/ }




						 //copyHref ="<A href= 'copyField.jsp?mode="+mode+"&fieldLibId="+fldLibId+"&fldDataType="+tmpFldDataType+"'> Copy </A>"  ;

						if ((counter%2)==0)
						{

						%>

					      	<tr class="browserEvenRow">

						<%}else
						 { %>

				      		<tr class="browserOddRow">

						<%} %>


						<td><%=catLibName%></td>
						<td><%=fldName%></td>
						<td><%=fldUniqueId%></td>
						<td><%=fldDescription%></td>
						<td><%=fldDataType%></td>
						<!--<td><%=copyHref%></td>-->
						<!--Fixed Bug No: 4592 By Rohit -->
						<td align =center><A href= 'copyField.jsp?mode=<%=mode%>&fieldLibId=<%=fldLibId%>&fldDataType=<%=tmpFldDataType%>' onkeypress="return fnEnterKeyPressOnce(event)" onClick="return fnHrefClickOnce(event)" ><img border="0" align="absbotton" title="<%=LC.L_Copy%><%--Copy*****--%>" alt="Copy Budget" src="./images/copy.gif"></A>
						</td>
			    	 	</tr>

					<%
			}//for loop

					%>


				</table>
	<table>
		<tr><td>
		<% if (totalRows > 0)
		{ Object[] arguments = {firstRec,lastRec,totalRows};
		%>
					<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%-- <font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<%} else {%>
			<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>
		<%}%>
		</td></tr>
		</table>
		<table align=center>
	<tr>
	<%
		if (curPage==1) startPage=1;
	    for (int count = 1; count <= showPages;count++)
		{


   			cntr = (startPage - 1) + count;
	 		if ((count == 1) && (hasPrevious))
			{
			  %>
				<td colspan = 2>
			  	<A href="copyFromFieldLibrary.jsp?mode=final&categorySearch=<%=categorySearch%>&nameSearch=<%=nameSearch%>&keywordSearch<%=keywordSearch%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> </A>
				&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<%
  			}	%>
		<td>
		<%
 		 if (curPage  == cntr)
		 {
	    	 %>
				<FONT class = "pageNumber"><%= cntr %></Font>
       		<%
       	}else
        {	%>
		<A href="copyFromFieldLibrary.jsp?mode=final&categorySearch=<%=categorySearch%>&nameSearch=<%=nameSearch%>&keywordSearch<%=keywordSearch%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>"><%= cntr%></A>
       <%}%>
		</td>
		<%	}
		if (hasMore)
		{   %>
	   <td colspan = 3 align = center>&nbsp;&nbsp;&nbsp;&nbsp;
	   <A href="copyFromFieldLibrary.jsp?mode=final&categorySearch=<%=categorySearch%>&nameSearch=<%=nameSearch%>&keywordSearch<%=keywordSearch%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
		</td>
		<%	}	%>
	   </tr>
	  </table>


			</Form>









	<jsp:include page="paramHide.jsp" flush="true"/>

		<%



		} //end of if body for page right

		else

		{

		%>

			<jsp:include page="accessdenied.jsp" flush="true"/>

		<%

		} //end of else body for page right

	}//end of if body for session

	else

	{

	%>

		<jsp:include page="timeout.html" flush="true"/>

	<%

	}

	%>


</body>

</html>

