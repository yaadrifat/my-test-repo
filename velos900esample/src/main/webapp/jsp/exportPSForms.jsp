<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>

<script type="text/javascript">
	function checkUncheckAll(formobj){
		var checks = document.getElementsByName('patform');
		var boolCheck = checks[0].checked;

		if (boolCheck == true){
			formobj.btnChkUnchk.value ='Check All';
			for(var i=0; i< checks.length; i++){
				checks[i].checked = false;
			}
		}else{
			formobj.btnChkUnchk.value ='Uncheck All';
			for(var i=0; i< checks.length; i++){
				checks[i].checked = true;
			}
		}
	}
</script>
</HEAD>

<BODY>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.sql.*,java.util.*,com.velos.eres.service.util.*,com.velos.impex.*"%>
  <%

	HttpSession tSession = request.getSession(true);
	
	String linkToFlag = request.getParameter("linkToFlag");
		
		if (StringUtil.isEmpty(linkToFlag))
		{
			linkToFlag = "";
		}
		 
	 if (sessionmaint.isValidSession(tSession)) {
%>
	<jsp:include page="sessionlogging.jsp" flush="true"/> 

	<% if (linkToFlag.equals("L")) { %> 
		<p class="defComments"> Select forms from Form Library: </p>
	<%
	}
	else
	{ %>
		<p class="defComments"> Select <%=LC.Pat_Patient%> (All Studies Forms): </p>
	<% } %>
	
		
		<form name="expForm" action="startFormExport.jsp" method="POST">

		<input name="btnChkUnchk" type="button" value="<%=LC.L_Uncheck_All%><%--Uncheck All--%>" onClick="return checkUncheckAll(document.expForm);"/><br/>
		<table>
		
<%	
	int ret = 0;

	    String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");
		

		String accId = (String) tSession.getValue("accountId");
	  	PreparedStatement pstmt = null;
	    StringBuffer sqlBuf = new StringBuffer(); 
	  	int rows = 0;
	  	Connection conn = null;

	  	String formName = null;
	  	int formId = 0;

		
		
		// get the all patient - all study forms 
	try{		
	    	if (linkToFlag.equals("L"))
	    	{
	    	sqlBuf.append("select PK_FORMLIB , FORM_NAME, FORM_DESC, lower(FORM_NAME) frmname, LF_DISPLAYTYPE");
			 sqlBuf.append(" FROM 	ER_FORMLIB  b "); 
			 sqlBuf.append(" WHERE b.FK_ACCOUNT = ? and form_linkto = 'L' and nvl(b.record_type,'N') <> 'D' ");
			 sqlBuf.append(" order by FRMNAME asc ");
	    	}
	    	else
	    	{
	    	 sqlBuf.append("select a.PK_LF as PK_LF,PK_FORMLIB , FORM_NAME, FORM_DESC, lower(FORM_NAME) frmname");
			 sqlBuf.append(" FROM ER_LINKEDFORMS  a  ,	ER_FORMLIB  b, er_codelst c"); 
			 sqlBuf.append(" WHERE b.FK_ACCOUNT = ?  and c.PK_CODELST = FORM_STATUS and trim(codelst_type) = 'frmstat' and trim(codelst_subtyp) = 'A' AND a.FK_FORMLIB = PK_FORMLIB    AND ");
 			 sqlBuf.append(" ((LF_DISPLAYTYPE = 'PS' OR LF_DISPLAYTYPE = 'PR') OR (LF_DISPLAYTYPE = 'PA' OR LF_DISPLAYTYPE = 'A'))   and  a.record_type <> 'D' ");
			 sqlBuf.append(" order by FRMNAME asc ");
			 }
			 
			 		 
			 CommonDAO cd = new CommonDAO();
			 conn = cd.getConnection();
			 
			 pstmt = conn.prepareStatement(sqlBuf.toString());
	  		 pstmt.setInt(1,EJBUtil.stringToNum(accId));			
	  		 
	  		 ResultSet rs = pstmt.executeQuery();
			
	  		 int cnt =0;
			while (rs.next())
			{	
				cnt++;
				formId = rs.getInt("PK_FORMLIB");
				formName = rs.getString("FORM_NAME");
			 %>
					<tr>
					
					<td>
						<label><%=cnt%>]</label>	<input type="checkbox" id="patform" name="patform" value = <%= formId %> CHECKED><%= formName%> &nbsp;[<%= formId%>]
					</td>
					</tr>
			<%	
				 rows ++;
			}
			%> 
			<input type="hidden" name = "patform_totalcount"  value = <%=rows%>>
			<input type="hidden" name = "linkToFlag"  value = <%=linkToFlag%>>
			 
			<%	
			 
	  }
	  catch (Exception ex)
	  {
	  	out.println("Exception in getting the forms: " + ex );
	  	out.println("Exception in getting the forms: " + sqlBuf.toString());
	  }
	  finally
		{
			try
			{
				if (pstmt != null ) pstmt.close();
			}
			catch (Exception e) 
			{
			}
			try 
			{
				if (conn!=null) conn.close();
			}
			catch (Exception e) {}
			
		}
		
	  %>
	  </table>
	  	  
	  	<HR/>
	<p class="defComments"> Select Participating Sites: </p>
	
	<table border = 0>
	
	<%
		int siteCount = 0;
		ArrayList arSites = new ArrayList ();
			 
		arSites = A2ASite.getA2ASites();
				
		siteCount = arSites.size();
		String siteName;
		int siteId = 0;
		
		for (int stCounter = 0; stCounter < siteCount ; stCounter++)
		{
			A2ASite site = new A2ASite();
			site = 	(A2ASite) arSites.get(stCounter);
			
			siteId = site.getId();
			siteName = site.getName();
		%>
			<tr>
				<td>
					<input type="checkbox" name="partSite" value = <%= siteId %> CHECKED><%= siteName%>
				</td>
			</tr>	
					
		<%	
			   	 
		
		 }   
	
	%>
	</table>
	
	<table width = 70% border = 0><tr><td align = "right">
      <tr>
		<td><br>
		
		<Input type="hidden" name="accId" value=<%=accId%>>
		<button type="submit"><%=LC.L_Submit%></button>
	</td></tr>
	</table>
	  
	 </form>  
	  	  	  
  <%	

	  
}//end of if body for session
else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





