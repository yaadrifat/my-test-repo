<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Specimen_UrlPage%><%--Specimen Url Page*****--%></title>

<%@ page import="java.util.*,com.velos.esch.service.util.EJBUtil" %><%@page import="com.velos.eres.service.util.LC"%><%@page import="com.velos.eres.service.util.MC"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<!-- YK 05Aug11: Removed for Bug #6728 already using Whichcss_skin.js in panel.jsp -->
</head>

<jsp:useBean id ="sessionmaint" scope="page" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="specimenApndxB" scope="page" class="com.velos.eres.web.specimenApndx.SpecimenApndxJB"/>

<SCRIPT Language="javascript">
 function  validate(formobj){

	  if (!(validate_col('URL',formobj.specimenApndxUri))) return false
     if (!(validate_col('Description',formobj.specimenApndxDesc))) return false
     if (!(validate_col('eSign',formobj.eSign))) return false


	if(isNaN(formobj.eSign.value) == true) {
	alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect E-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>


<% String src;
src= request.getParameter("srcmenu");
%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>


<body>
<br>

<DIV class="formDefault" id="div1">
<%
	String mode = request.getParameter("mode");
	System.out.println("mode.."+mode);
	String specimenId = request.getParameter("pkId");

	String selectedTab = request.getParameter("selectedTab");
	System.out.println("spec id is .."+specimenId);
	String specApndxDesc = "";
	String specApndxUri = "";
	String specApndxId = "";
	String specId = request.getParameter("specimenId");

	//JM: 21Jul2009: issue no-4174
		String perId = "", stdId = "";
		perId = request.getParameter("pkey");
	 	perId = (perId==null)?"":perId;

		stdId = request.getParameter("studyId");
		stdId = (stdId==null)?"":stdId;





	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))
	{
%>
<P class="sectionHeadings"> <%=LC.L_Specimen_Url%><%--Specimen >> Url*****--%> </P>

<%
	if (mode.equals("M")) {
	        specimenApndxB.setSpecimenApndxId(EJBUtil.stringToNum(specId));
		specimenApndxB.getSpecimenApndxDetails();
		specApndxDesc = specimenApndxB.getSpecApndxDesc();
		specApndxUri  = specimenApndxB.getSpecApndxUri();
		specApndxId = Integer.toString(specimenApndxB.getSpecimenApndxId());

	}
%>
<form name="addurl" id="specaddurl" METHOD=POST action="specimenurlsave.jsp" onsubmit="if (validate(document.addurl)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<input type=hidden name="mode" value=<%=mode%>>
<input type=hidden name="specId" value=<%=specApndxId%>>
<input type=hidden name="fkSpecimenId" value=<%=specimenId%>>

<input type=hidden name="srcmenu" value=<%=src%>>
<input type=hidden name="selectedTab" value=<%=selectedTab%>>

<input type="hidden" name="pkey" value="<%=perId%>">
<input type="hidden" name="studyId" value="<%=stdId%>">

<TABLE width="90%">
      <tr>
        <td>
          <P class = "defComments"> <%=MC.M_AddLnk_ToSpmenAppx%><%--Add Links to your Specimen Appendix*****--%></P>
        </td>
      </tr>
    </table>
    <table width="100%" >

      <tr>
        <td width="35%"> <%=LC.L_Url_Upper%><%--URL*****--%> <FONT class="Mandatory">* </FONT> </td>
        <td width="65%">
          <input type=text name="specimenApndxUri" value='<%=specApndxUri%>' MAXLENGTH=255 size=50>
        </td>
      </tr>
      <tr>
        <td width="35%"> </td>
        <td width="65%">
          <P class="defComments_txt"> <%=MC.M_EtrCpltUrl_255Max%><%--Enter complete url eg. 'http://www.centerwatch.com' or 'ftp://ftp.centerwatch.com' (255 char max.)*****--%> </P>
        </td>
      </tr>
      <tr>
        <td width="35%"> <%=LC.L_Short_Desc%><%--Short Description*****--%> <FONT class="Mandatory" >* </FONT>
        </td>
        <td width="65%">
          <input type=text name="specimenApndxDesc"  value='<%=specApndxDesc%>' MAXLENGTH=100  size=40>
        </td>
      </tr>
      <tr>
        <td width="35%"> </td>
        <td width="65%">
          <P class="defComments_txt"> <%=MC.M_NameToLnk_100CharMax%><%--Give a friendly name to your link. (100 char max.)*****--%> </P>
        </td>
      </tr>

	</table>

	  </BR>
	<jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="specaddurl"/>
		<jsp:param name="showDiscard" value="N"/>
</jsp:include>

</form>

<%

} else {  //else of if body for session

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

}

%>

 <div class = "myHomebottomPanel">
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</div>
<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>
</body>

</html>



