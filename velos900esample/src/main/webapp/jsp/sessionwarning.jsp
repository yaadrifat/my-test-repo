<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<script type="text/javascript" src="js/yui/build/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="js/yui/build/event/event-min.js"></script>
<script type="text/javascript" src="js/yui/build/connection/connection-min.js"></script>
<script type="text/javascript" src="js/yui/build/json/json-min.js"></script>

<jsp:useBean id ="sessionmaint2" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language="java" import="com.velos.eres.web.user.UserJB,com.velos.eres.service.util.*"%>

<%
int userSessionTime = 0; // In min
if (sessionmaint2.isValidSession(request.getSession(false))) {
    UserJB userB2 = (UserJB) request.getSession(false).getValue("currentUser");
    if (userB2 != null) {
        String sessTimeStr = StringUtil.trueValue(userB2.getUserSessionTime());
        try {
            userSessionTime = Integer.parseInt(sessTimeStr);
        } catch(Exception e) {}
    }
%>
<SCRIPT>
// Display session timeout warning
var userTimeoutInMSec = <%=userSessionTime%> * 60 * 1000;
var warningPeriodInMSec = 60 * 1000;
var sessTimeoutWarning = '<%=MC.M_Usr_SessTimeout%>';
if (userTimeoutInMSec - warningPeriodInMSec < 10000) {
	warningPeriodInMSec = 30000;
	sessTimeoutWarning = '<%=MC.M_Usr_SessTimeoutShort%>';
}
// sessTimedOut verbiage taken from timeout.html
var sessTimedOut = "<%=MC.M_SessTimedOut_LoginAgain%>"/*var sessTimedOut = "Your session has timed out. This window will thus be closed. Please login again to continue."*****/
	
// Start the timer
setTimeout("show_session_warning()", userTimeoutInMSec-warningPeriodInMSec);
       
// Run this function when the timeout is near
function show_session_warning() {
	var isExpired = false;
    alert(sessTimeoutWarning);
    // Send a signal to the server to keep the session alive
    YAHOO.util.Connect.asyncRequest('POST', 'sessioncheck.jsp', 
    {
        success: function(o) {
    	    var resp = YAHOO.lang.JSON.parse(o.responseText);
    	    try { if (resp.error < 0) {isExpired = true;} } catch(e) {isExpired = true;}
    	    if (isExpired) {
    	    	alert(sessTimedOut);
                try { window.self.close(); } catch(e) {}
        	    return;
        	}
    	    // If a popup was open, brint it to front
    	    try { windowName.focus(); } catch(e) {}
    	    try { windowname.focus(); } catch(e) {}
    	    // Start the timer again
    	    setTimeout("show_session_warning()", userTimeoutInMSec-warningPeriodInMSec);
        },
        failure: function(o) {
            alert(sessTimedOut);
            try { window.self.close(); } catch(e) {}
            return;
        }
    }, null);
}
</SCRIPT>
<%
} // End of valid session clause
%>
