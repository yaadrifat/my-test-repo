<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<HTML>

<HEAD>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>

<BODY>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="eventassocB" scope="request" class="com.velos.esch.web.eventassoc.EventAssocJB"/>
<jsp:useBean id="eventDao" scope="request" class="com.velos.esch.business.common.EventdefDao"/>

<%@ page language="java" import="java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.esch.web.eventdef.EventdefJB,com.velos.esch.web.eventassoc.EventAssocJB"%>

<%

	HttpSession tSession = request.getSession(true);
	String ipAdd = (String) tSession.getValue("ipAdd");

    String usr = null;
	usr = (String) tSession.getValue("userId");
	String acc = (String) tSession.getValue("accountId");
 


	if (sessionmaint.isValidSession(tSession)) {
		%>
		<jsp:include page="sessionlogging.jsp" flush="true"/>
		<%
		String oldESign = (String) tSession.getValue("eSign");

		String eSign = request.getParameter("eSign");



		String mode=request.getParameter("mode");
		String duration=request.getParameter("duration");
		String calstatus=request.getParameter("calstatus");
		String calassoc=request.getParameter("calassoc");


		String[] visitIds=new String[2000];
		String[] eventIds=new String[2000];



		visitIds= request.getParameterValues("visIds");
	 	eventIds= request.getParameterValues("evIds");




	 	String calledFrm = request.getParameter("calledfrom");

	 	String evtName = request.getParameter("eventName");

		int setId=0;


		String protId = request.getParameter("calProtocolId");
		if (protId==null) protId = "";


		String displacement = "";

		int visitLen=visitIds.length;
		String visIdTemp="";
		String visId="";


		int evtLen=eventIds.length;
		String evtIdTemp="";
		String evtId="";


		String evtsName="";



		int max_Seq_val = 0;
		String seq="";
		String tableName = request.getParameter("tableName");

		int index = 0;


					for(int cnt=0;cnt<visitLen;cnt++) {
					    visIdTemp=visitIds[cnt];


						index = visIdTemp.indexOf(",");
					    visId = visIdTemp.substring(0,index);
					    displacement = visIdTemp.substring(index+1);



						for(int cntInner=0;cntInner<evtLen;cntInner++) {
					    	evtIdTemp=eventIds[cntInner];
					    	index = evtIdTemp.indexOf(",");
					    	evtId = evtIdTemp.substring(0,index);

					    	evtsName = evtIdTemp.substring(index+1);




							max_Seq_val = eventdefB.getMaxSeqForVisitEvents(Integer.parseInt(visId), evtsName, tableName);///for seq checking...


							if (calledFrm.equals("P")  || calledFrm.equals("L")){


								if (max_Seq_val>=0){


									seq = ""+ (max_Seq_val + 1);
									EventdefJB evtdefB = new EventdefJB();
									evtdefB.setEvent_id(Integer.parseInt(evtId));
									evtdefB.getEventdefDetails();

									//KM-No interval defined visits
									if(!displacement.equals("0"))
										evtdefB.setDisplacement(displacement);
									else
										evtdefB.setDisplacement(null);

							        evtdefB.setEventVisit(visId);
							        evtdefB.setEventSequence(seq);

							        evtdefB.setCreator(usr);
							        evtdefB.setIpAdd(ipAdd);
							        setId=evtdefB.setEventdefDetails();

							        evtdefB.CopyEventDetails(evtId, ""+setId, "C", usr, ipAdd);


								}

							}else if (calledFrm.equals("S")){

								if (max_Seq_val>=0){

									seq = ""+ (max_Seq_val + 1);
									EventAssocJB evtassB = new EventAssocJB();
									evtassB.setEvent_id(Integer.parseInt(evtId));
									evtassB.getEventAssocDetails();

									//KM-No interval defined visits
									if(!displacement.equals("0"))
										evtassB.setDisplacement(displacement);
									else
										evtassB.setDisplacement(null);

							        evtassB.setEventVisit(visId);
							        evtassB.setEventSequence(seq);
							        evtassB.setCreator(usr);
							        evtassB.setIpAdd(ipAdd);
							        evtassB.setCalAssocTo(calassoc);//25-Mar-2011 @Ankit #5966
							        setId=evtassB.setEventAssocDetails();


							        evtassB.CopyEventDetails(evtId, ""+setId, "S", usr, ipAdd);


						        }

							}



						}





					}








if(setId > 0)
  {
%>
  <br>
  <Br>
  <br>
  <Br>
  <p class = "sectionHeadings" align = center> <%=MC.M_Data_SavedSucc%><%--Data saved successfully*****--%> </p>
  <script>
  window.opener.location.reload();
  setTimeout("self.close()",1000);
  </script>
<%
}

else

{

%>

<br>

<br>

<br>

<br>

<br>

<p class = "sectionHeadings" align = center> <%=MC.M_DataNotSvd_EvtsAldyExst%><%--Data could not be saved as Event(s) already exists for Visit(s)*****--%></p>



<table width=50%>

  <tr>

  <td align="right">

<button onclick="history.go(-1);return false;"><%=LC.L_Back%></button>
<!--<A type="submit" href="addevtvisits.jsp?mode=<%=mode%>&srcmenu=tdMenuBarItem4&duration=<%=duration%>&protocolid=<%=protId%>&calledfrom=<%=calledFrm%>&calstatus=<%=calstatus%>&calltime=new&calassoc=<%=calassoc%>"><%=LC.L_Back%></A>-->

</td>

</tr>

</table>
<%

}

		//}// end of if body for e-sign
	}//end of if body for session
	else {
	%>
		<jsp:include page="timeout.html" flush="true"/>
	<%
	}
	%>
<jsp:include page="bottompanel.jsp" flush="true"/>
</BODY>
</HTML>
