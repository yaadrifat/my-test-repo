<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>  
<head>
<title><%--Edit Portal Details*****--%><%=LC.L_Edit_PortalDets%></title>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>

<jsp:include page="popupJS.js" flush="true"/>

<SCRIPT Language="javascript">

function  validate(formobj) {    
	
	
	preStatus = formobj.preStatus.value;
	activeId = formobj.activeId.value;
	deactivateId = formobj.deactivateId.value;
	wipId = formobj.wipId.value;
	 
	if (formobj.status){
		if (!(validate_col('Status',formobj.status))) return false
	}
	
	if (!(validate_col('Status Date',formobj.statusDate))) return false
	
 
	
	var status =formobj.status.options[formobj.status.selectedIndex].value;	
	
	if(status != preStatus)
	{
			formobj.mode.value ='N';
			formobj.EndStatusDate.value = formobj.statusDate.value;
			formobj.isCurrent.value = 1;
		}
	else
	{
		formobj.mode.value='M';
	}	


	if(preStatus == activeId &&  status == wipId)
	{
		/*alert("You cannot change status from 'Active' to 'Work in progress'");*****/
		alert("<%=MC.M_CntChg_ActvToWorkInPgress%>");
		formobj.status.value = activeId;		
		return false;
	}
	
	
	if(preStatus == deactivateId && status == wipId)
	{
		/*alert("You cannot change status from 'Deactivate' to 'Work in Progress'");*****/
		alert("<%=MC.M_CntChg_DeacToWorkInPgress%>");
		formobj.status.value = deactivateId;	
		return false;
	}

	

	     
     	
    	if (  !(validate_col('e-Signature',formobj.eSign)     )   ) return false
	
	
	/*if(formobj.EndStatusDate)
	{
	    if (formobj.EndStatusDate.value =="") {
     		alert("Please select Previous Status End Date" );
		formobj.EndStatusDate.focus();
		return false;
            }	
	} */  
	  
	if (isNaN(formobj.eSign.value) == true) {
		/* alert("Incorrect e-Signature. Please enter again"); *****/
		alert("<%=MC.M_IncorrEsign_EtrAgain%>"); 
		formobj.eSign.focus();
		return false;
	}
	  
 }
 
  	

</SCRIPT>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="statusB" scope="request" class="com.velos.eres.web.statusHistory.StatusHistoryJB"/>

<%@ page language = "java" import = "com.velos.eres.business.common.StatusHistoryDao, com.velos.eres.business.common.CodeDao,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<body>
<DIV class="popDefault" id="div1"> 
<%

HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
%>
	<jsp:include page="sessionlogging.jsp" flush="true"/>
	<jsp:include page="include.jsp" flush="true"/>
<%
	
	String mode=request.getParameter("mode");

	if(mode == null)
		mode = "";	
	String moduleTable = request.getParameter("moduleTable");
	String modulePk = request.getParameter("portalId");
	String statusCode = request.getParameter("statusCode");
	
	String accountId=(String)tSession.getAttribute("accountId");
        String portalStat = request.getParameter("portalStat");
        String portalName = request.getParameter("portalName");
        portalName = StringUtil.decodeString(portalName);//KM
	String isCurrentStat = request.getParameter("isCurrentStat");
	String statusId = request.getParameter("statusId");

	
	int tempAccountId=EJBUtil.stringToNum(accountId);
	//String pageRight="";
	//String currentStatId="";
	
	String notes = "";
    
//	JM: 14Jun2007
	int pageRight = 0;
	GrpRightsJB grpRights = (GrpRightsJB) 	tSession.getValue("GRights");		
	pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("PORTALAD"));
	

        

	//if (EJBUtil.isEmpty(pageRight))
		//pageRight = "7";
	//if (EJBUtil.stringToNum(pageRight) >= 7) { 
		if (pageRight >=4){
		
		
		
		/*StatusHistoryDao statusHistDao = new StatusHistoryDao();	
		ArrayList statusIds = new ArrayList();	
		
		ArrayList isCurrentStatArr= new ArrayList();
		statusHistDao = statusB.getStatusHistoryInfo(EJBUtil.stringToNum(modulePk), moduleTable);
		statusIds = statusHistDao.getStatusIds();
		isCurrentStatArr = statusHistDao.getIsCurrentStats();
   	  	int len= statusHistDao.getCRows();
		String currentVal="";
		for (int i=0;i<len;i++) {
			currentVal = (isCurrentStatArr.get(i) == null)?"":(isCurrentStatArr.get(i)).toString();
			if ( currentVal.equals("1")) {
			     currentStatId = (String)statusIds.get(i).toString();
		        }
    		}*/

		
		String statusDate="";
		
		//if (isCurrentStat==null)
		//    isCurrentStat="1";
						
		if (mode.equals("M"))
		{ 
			statusB.setStatusId(EJBUtil.stringToNum(statusId));
			statusB.getStatusHistoryDetails();
			statusCode = statusB.getStatusCodelstId();
			notes = statusB.getStatusNotes();
			if (EJBUtil.isEmpty(notes))
			notes = "";
			statusDate = statusB.getStatusStartDate();
			if (statusDate==null)
				statusDate="";
					
		}
		
		
		
%>

<Form id="status_form" name="status" method="post" action="updatePortalStatus.jsp" onsubmit="if (validate(document.status)==false) {setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">

	<table width="99%" cellspacing="0" cellpadding="0" border="0">
		<tr>
		<td ><P class="sectionHeadings"> <%-- Edit Portal Status ***** --%><%=LC.L_Edit_PortalStatus%> </P> </td>
		</tr>   
	 </table>
	
<input name="moduleTable" type=hidden value=<%=moduleTable%>>
<input name="modulePk" type=hidden value=<%=modulePk%>>
<!--input name="mode" type=hidden value=<%=mode%>-->
<input name="statusId" type=hidden value=<%=statusId%>>
<input name="statCode" type=hidden value=<%=statusCode%>>
<br>
<table width="99%" cellspacing="2" cellpadding="2" border="0" class="basetbl" >
	<tr> 
      	<td width="20%"><%-- Portal Name***** --%><%=LC.L_Portal_Name%></td>
        <td width="60%"><%=portalName%></td>
	</tr>
	<!--tr> 
        <td width="20%">Current Status</td>
        <td width="60%"><%=portalStat%></td>		
	</tr-->
	  
<% 
//if(mode.equals("N")) { 
	CodeDao codeStatus = new CodeDao();
	codeStatus.getCodeValues("portalstatus");	
	
	String statusDD="";
	statusDD = codeStatus.toPullDown("status",EJBUtil.stringToNum(statusCode));

	CodeDao cd = new CodeDao();
	int wipId = cd.getCodeId("portalstatus","W");
	int activeId = cd.getCodeId("portalstatus","A");
	int deactivateId = cd.getCodeId("portalstatus","D");

       %>
       <tr>
       <td width="20%"><%-- Portal Status***** --%><%=LC.L_Portal_Status%> <FONT class="Mandatory">* </FONT></td>
       <td width="60%">
		<%=statusDD%></td>
       </tr>

      
       
       <!--tr>
	<%
	if(mode.equals("N")) {
	%>
		
		<td colspan=2 width="20%"> <input type="checkbox" name="currentstat" checked> This is Portal's current status</td>

	 <% 
         } else if (EJBUtil.stringToNum(isCurrentStat)==0) {
	 %>
		 
		 <td colspan=2 width="20%"> <input type="checkbox" name="currentstat"> This is Portals's current status</td>

		 <% } else if (EJBUtil.stringToNum(isCurrentStat)==1)  { %>
		 
		 <td colspan=2 width="20%"> <input type="checkbox" name="currentstat" checked disabled> This is Portals's current status</td>

		 <%}%>
	       </tr-->
       
      <tr> 
	  
		<td width="200"> <%-- Status Date***** --%><%=LC.L_Status_Date%> <FONT class="Mandatory">* </FONT></td>
<%-- INF-20084 Datepicker-- AGodara --%>
		<td width=300><input type="text" name="statusDate" class="datefield" size = 15 MAXLENGTH = 50	value ="<%=statusDate%>" readonly></td>

	</tr>
	
	<% if(mode.equals("N")) { %>
	
	<!--tr> 
        <td width="200"> Previous Status Ends on <FONT class="Mandatory">* </FONT></td>

        <td width=300> 
	
          <input type="text" name="EndStatusDate" size = 15 MAXLENGTH = 50 readonly value ="" >
        		 &nbsp&nbsp&nbsp<A href=# onClick="return fnShowCalendar(document.status.EndStatusDate)"> Select Date</A>

	</td>
	</tr-->
	
	<%} %>
	
	<tr> 

        <td width="200"> <%-- Notes***** --%><%=LC.L_Notes%></td>

        <td width=300> 

	<TEXTAREA name="notes"  rows=3 cols=30><%=notes%></TEXTAREA></td>

	</tr>

</table>
	<br>
	 <% if (pageRight>=6) {%>
	
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="status_form"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>
	
		
	<%} %>
     <br>

 	 <table width="100%" cellspacing="0" cellpadding="0">
      <td align=right> 

      </td> 
      </tr>
  </table>

  
  <input name="isCurrent" type="hidden"  value="1"> 
  <!-- we dont need to edit current status for portal status, and old records are not edited. so new record is always current-->
  <input name="portalStat" type="hidden" value="<%=portalStat%>">
  
  <input name="preStatus" type="hidden" value=<%=EJBUtil.stringToNum(statusCode)%>>
  <input type="hidden" name="activeId" value=<%=activeId%>>
  <input type="hidden" name="deactivateId" value=<%=deactivateId%>>
  <input type="hidden" name="wipId" value=<%=wipId%>>
  <input type="hidden" name="EndStatusDate"  >
   
  <input type="hidden" name="mode" value="">

  </Form>
  
<%

	} //end of if body for page right
	else
	{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%

	} //end of else body for page right

}//end of if body for session

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
<%

}

%>
 <div class = "myHomebottomPanel"> 
 <jsp:include page="bottompanel.jsp" flush="true"/>
 </div>
</body>

</html>

