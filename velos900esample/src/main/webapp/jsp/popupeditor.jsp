<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Popup_Editor%><%-- PopUp Editor*****--%> </title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>


	 <%
	 	  String formatTextAreaWidth = request.getParameter("formatTextAreaWidth");
		 formatTextAreaWidth=(formatTextAreaWidth==null)?"500":formatTextAreaWidth;

		 String formatTextAreaHeight = request.getParameter("formatTextAreaHeight");
		 formatTextAreaHeight=(formatTextAreaHeight==null)?"200":formatTextAreaHeight;

	 	 %>	       
	 	 	 
	 	 	 
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<script type="text/javascript" src="./FCKeditor/fckeditor.js"></script>
	<script type="text/javascript">
	var fck;
	var oFCKeditor;
   function init()
      {
	
      oFCKeditor = new FCKeditor("editor") ;
      oFCKeditor.BasePath = './FCKeditor/';
      oFCKeditor.Width = <%=formatTextAreaWidth%> ;
      oFCKeditor.Height = <%=formatTextAreaHeight%>; // 400 pixels
      //oFCKeditor.ToolbarSet="Velos";
      oFCKeditor.ToolbarSet=document.editorForm.toolbar.value;
      //oFCKeditor.Config['SkinPath'] = '/FCKeditor/editor/skins/office2003/' ;
      oFCKeditor.ReplaceTextarea() ;
	  		
      
      }
    </script>
<script type="text/javascript">
     overRideChar("#");
   overRideChar("&");
   overRideFld("name");
   var editor;
  function FCKeditor_OnComplete(oFCKeditor)
{
	 fck= FCKeditorAPI.GetInstance('editor');
	
	 setValue();
	 
}
function GetText()
{
	// This functions shows that you can interact directly with the editor area
	// DOM. In this way you have the freedom to do anything you want with it.

	// Get the editor instance that we want to interact with.
	//var oEditor = FCKeditorAPI.GetInstance('FCKeditor1') ;

	// Get the Editor Area DOM (Document object).
	var oDOM = fck.EditorDocument ;

	var iLength ;
	var htmlLess;
	// The are two diffent ways to get the text (without HTML markups).
	// It is browser specific.

	if ( document.all )		// If Internet Explorer.
	{
		//iLength = oDOM.body.innerText.length ;
		htmlLess=oDOM.body.innerText;
	
	}
	else					// If Gecko.
	{
		var r = oDOM.createRange() ;
		r.selectNodeContents( oDOM.body ) ;
		htmlLess=r.toString();
	
		//iLength = r.toString().length ;
	}
	return htmlLess;
}

  </script>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="include.jsp" flush="true"/>
<body>
<DIV class="popDefault" id="div1"> 
<%

  HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
  String prefix="";
  
 String toolbar=request.getParameter("toolbar");
 toolbar=toolbar==null?"Velos":toolbar;
 
 String dataForm=request.getParameter("formname");
 dataForm=(dataForm==null)?"":dataForm;
 
 String dataField=request.getParameter("fieldname");
 dataField=(dataField==null)?"":dataField;
 
 String formatField=request.getParameter("formatfieldname");
 formatField=(formatField==null)?"":formatField;
 
 String length=request.getParameter("length");
 length=(length==null)?"":length;
 
 String displayDIV=request.getParameter("displayDIV");
 displayDIV=(displayDIV==null)?"":displayDIV;
 
 String formatTextAreaStyle="";
 
 formatTextAreaStyle=request.getParameter("formatTextAreaStyle");
 formatTextAreaStyle=(formatTextAreaStyle==null)?"width:400;height:200;":formatTextAreaStyle;
  


%>

<form name="editorForm" id="popupfrm" METHOD="POST" onSubmit="if (setContents()== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">

<input type="hidden" name="toolbar" value='<%=toolbar%>'>
<input type="hidden" name="formname" value='<%=dataForm%>'>
<input type="hidden" name="fieldname" value='<%=dataField%>'>
<input type="hidden" name="formatfieldname" value='<%=formatField%>'>
<input type="hidden" name="prefix" value='<%=prefix%>'>
<input type="hidden" name="length" value='<%=length%>'>
<input type="hidden" name="displayDIV" value='<%=displayDIV%>'>


<table width="70%">
	<tr>
		<td colspan="2">
			<P class="sectionHeadings"> <%=LC.L_Formatted_DispText%><%-- Formatted Display Text*****--%>: </P>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea id="editor" name="editor"  rows="" cols="100" style="<%=formatTextAreaStyle%>"></textarea>
		</td>
	</tr>
<% 
	
if ("displayMom".equals(displayDIV)) { %>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>	
			<input type="button" name="setContent" onClick="setContents()" value="<%=LC.L_Set_Content%><%--Set Content--%>"></input>
		</td>
	</tr>
<% } else { %>
	<tr>
		<td align=center colspan=2>
			<jsp:include page="submitBar.jsp" flush="true"> 
				<jsp:param name="displayESign" value="N"/>
				<jsp:param name="formID" value="popupfrm"/>
				<jsp:param name="showDiscard" value="Y"/>
			</jsp:include>
		</td>
	</tr>

<% } %>
</table>
</form>
<script>
 init();
</script>
<script>
function setContents()
{
 var text;
if (typeof(window.opener.document.forms[document.editorForm.formname.value].elements[document.editorForm.formatfieldname.value].value)!="undefined")
{
	
	var dispDiv ;
		
	dispDiv = document.editorForm.displayDIV.value;

	
					
 text=fck.GetXHTML( true );

 //text=htmlEncode(text);
 if (text.indexOf("\r\n")>=0) text=replaceSubstring(text,"\r\n","<br/>");
 if (text.length>parseInt(document.editorForm.length.value))
 {
	    var paramArray = [document.editorForm.length.value];
		alert(getLocalizedMessageString("M_FmtDispAled_PlsCont",paramArray));/*alert("Formatted display text exceeds the allowed length of "+ document.editorForm.length.value+".Please correct to continue.");*****/
  return false;
 }
 if (document.editorForm.prefix.value=="Y") text="[VELDISABLE]"+text;
 window.opener.document.forms[document.editorForm.formname.value].elements[document.editorForm.formatfieldname.value].value=text;

 if (dispDiv!="")
	{
		displayDIVElem = window.opener.document.getElementById(dispDiv);
		displayDIVElem.innerHTML = text;
	}
 
 }
 
 window.self.close();
 return false;
 
}

function setValue()
{


var data;
var prefix="";
if (typeof(window.opener.document.forms[document.editorForm.formname.value].elements[document.editorForm.formatfieldname.value].value)!="undefined")
{
data=window.opener.document.forms[document.editorForm.formname.value].elements[document.editorForm.formatfieldname.value].value;
//check if the field is disabled and remove [VELDISABLE]. 
 if (data.length>0) 
 {
  if (data.indexOf("[VELDISABLE]")>=0)
  {
   data=replaceSubstring(data,"[VELDISABLE]","");
   prefix="Y";
  }
 }
//data=htmlDecode(data);
fck.SetHTML(data);
document.editorForm.prefix.value=prefix;
}
}
</script>
<%
}//end sessionmaint
%>
</DIV>
<div class = "myHomebottomPanel"> 
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</body>
</html>



