<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=LC.L_Grid_Monitor%><%--Grid Monitor*****--%></title>

<script>

function setOrder(formObj,orderBy,pgRight) //orderBy column number
{
	var lorderType;
	 if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";
	}

if (orderBy==2) orderBy="lower(study_number)";
	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	searchCriteria=formObj.searchCriteria1.value;
	formObj.action="revSetup.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType+"&searchCriteria="+searchCriteria;
	formObj.submit();
}

function openWin()
{


	windowName=window.open("testJMSConnection.jsp","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=600,height=500");
    windowName.focus();



}

</script>

<%
String src;
src= request.getParameter("srcmenu");
%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.web.grpRights.GrpRightsJB,com.velos.eres.service.util.*,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,java.sql.*,com.velos.eres.business.common.CtrlDao,java.util.* "%>

<br>

<div class="browserDefault" id="div1">
<P class="sectionHeadings"> <%=LC.L_Grid_Monitor%><%--Grid Monitor*****--%></P>
<%
String pagenum = "";
int curPage = 0;
long startPage = 1;
String stPage;
long cntr = 0;

pagenum = request.getParameter("page");

if (pagenum == null)
{
	pagenum = "1";
}
curPage = EJBUtil.stringToNum(pagenum);

String orderBy = "";
orderBy = request.getParameter("orderBy");


String orderType = "";
orderType = request.getParameter("orderType");


if (orderType == null)
{
	orderType = "asc";
}

String searchCriteria = request.getParameter("searchCriteria");
if (searchCriteria==null) {searchCriteria="";}

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)) {
	//get milestone module account right
	String modRight = (String) tSession.getValue("modRight");
	String accountId = (String) tSession.getValue("accountId");

	int modlen = modRight.length();
	CtrlDao acmod = new CtrlDao();
	acmod.getControlValues("module");
	ArrayList acmodfeature =  acmod.getCValue();
	ArrayList acmodftrSeq = acmod.getCSeq();
	//int a2aSeq = acmodfeature.indexOf("A2A");
	//a2aSeq	= ((Integer) acmodftrSeq.get(a2aSeq)).intValue();
	//char A2Aright = modRight.charAt(a2aSeq - 1);

	String userId = (String) tSession.getValue("userId");
	int pageRight = 7;
 char A2Aright = '1';

    	if (String.valueOf(A2Aright).compareTo("1") == 0 )	{

		//////////////////end by salil



	   StringBuffer sql= new StringBuffer();

	    sql.append("SELECT   (select study_number from er_study where pk_study = fk_study) study_number,A.FK_STUDY FK_STUDY, (SELECT COUNT(*) FROM ER_REV_PENDINGDATA ");
		sql.append( "WHERE fk_study = A.fk_study AND RP_MODULE = B.MODULE_SUBTYPE) DATA_COUNT , ");
		sql.append(" revSponsor.SITE_CODE SITE_CODE,revSponsor.backup_msgfolder backup_msgfolder,module_name, ");
		sql.append(" to_char(A.rs_last_exported_on,pkg_dateutil.f_get_datetimeformat) rs_last_exported_on ,		to_char(A.rs_next_export_due,pkg_dateutil.f_get_datetimeformat) rs_next_export_due,fk_sponsor_account,pk_rs ");
		sql.append(" FROM ER_REVMODEXP_SETUP A, ER_MODULES B , ER_REV_STUDYSPONSOR revSponsor  WHERE 	 PK_MODULE = FK_MODULE AND ");
		 sql.append(" revSponsor.FK_SITESTUDY =  FK_STUDY AND revSponsor.pk_rev_studysponsor = fk_rev_studysponsor and site_code = (select site_code from er_account where pk_account = "+accountId+") ");
		 sql.append(" ORDER BY pk_rev_studysponsor, FK_STUDY, MODULE_SEQUENCE") ;


	   String countSql = "select count(pk_rs) from (" + sql.toString() +" ) " ;


	   long rowsPerPage=0;
   	   long totalPages=0;
	   long rowsReturned = 0;
	   long showPages = 0;

	   String studyNumber = null;
	   int studyId=0;
	   int oldStudyId = 0;
	   String module = "";
	   String lastExportedOn;
	   String nextDue = "";
	   String siteCode ="";

	   boolean hasMore = false;
	   boolean hasPrevious = false;
	   long firstRec = 0;
	   long lastRec = 0;
	   long totalRows = 0;
	   String dataCount = "";

	   rowsPerPage =  50;
	   totalPages =Configuration.PAGEPERBROWSER ;

       BrowserRows br = new BrowserRows();
	   if ((orderBy==null ) ||orderBy.equals("null") ) orderBy = "lower(study_number)";

	   br.getPageRows(curPage,rowsPerPage,sql.toString(),totalPages,countSql,orderBy,orderType);
   	   rowsReturned = br.getRowReturned();
	   showPages = br.getShowPages();
	   startPage = br.getStartPage();
	   hasMore = br.getHasMore();
	   hasPrevious = br.getHasPrevious();
	   totalRows = br.getTotalRows();
	   firstRec = br.getFirstRec();
	   lastRec = br.getLastRec();

%>

<Form name="revSetup" method=post onsubmit="" action="revSetup.jsp?srcmenu=<%=src%>&page=1">


	<Input type="hidden" name="srcmenu" value="<%=src%>">
	<Input type="hidden" name="page" value="<%=curPage%>">
	<Input type="hidden" name="orderBy" value="<%=orderBy%>">
	<Input type="hidden" name="orderType" value="<%=orderType%>">
	<Input type="hidden" name="searchCriteria1" value="<%=searchCriteria%>">

	<P class="sectionHeadings"> <A href="dataRecvd.jsp"><%=MC.M_ClkHere_DataInfoSite%><%--Click Here</A> to View Data Aggregation Information at this Site*****--%></P>
	<P class="sectionHeadings"> <A href="#" onClick="return openWin();"><%=MC.M_ClkHere_JmsConEres%><%--Click Here</A> to test Java Messaging Service (JMS) Connection with a Remote eResearch Site*****--%></P>



    <P class="sectionHeadings"><%=MC.M_ProcSch_ForSite%><%--Aggregation Process Schedule for this Site*****--%></P>

    <table width="100%" >
      <tr>
        <th width="20%" onClick="setOrder(document.revSetup,1)"><%=LC.L_Study%><%--<%=LC.Std_Study%>*****--%></th>
        <th width="20%"> <%=LC.L_Module%><%--Module*****--%> </th>
        <th width="15%" > <%=LC.L_Last_ExportedOn%><%--Last Exported On*****--%></th>
        <th width="15%" > <%=MC.M_Next_ExportDue%><%--Next Export Due*****--%></th>
         <th width="15%" > <%=MC.M_Data_ReadyForExp%><%--Data ready for Export*****--%></th>
         <th width="15%" > <%=LC.L_Site_Code%><%--Site Code*****--%> </th>
      </tr>

      <%
    for(int counter = 1;counter<=rowsReturned;counter++)
	{
		oldStudyId = studyId;

		studyId=EJBUtil.stringToNum(br.getBValues(counter,"fk_study")) ;
		studyNumber=br.getBValues(counter,"study_number");
		module=br.getBValues(counter,"module_name");
		lastExportedOn =br.getBValues(counter,"rs_last_exported_on");
		nextDue =br.getBValues(counter,"rs_next_export_due");
		dataCount =br.getBValues(counter,"data_count");
		siteCode =br.getBValues(counter,"site_code");

		if (StringUtil.isEmpty(studyNumber))
		{
			studyNumber = LC.L_No_Study/*"No "+ LC.Std_Study*****/;
		}


		if (oldStudyId != studyId )
		{
			%>
			    <tr><td> <b><%=studyNumber%></b>  </td></tr>
			<%
		}


		if ((counter%2)==0) {
  %>
      <tr class="browserEvenRow">
        <%
		}
		else{
  %>
      <tr class="browserOddRow">
        <%
		}

  %>
        <td> &nbsp;</td>
        <td> <%=module%> </td>
        <td> <%=lastExportedOn%> </td>
         <td> <%=nextDue%> </td>
         <td> <%=dataCount%> </td>
         <td> <%=siteCode%> </td>

      </tr>
      <%

	}
%>
    </table>

	<table>
	<tr>
	<td>
	<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%} else {%>
		<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>
	<%}%>
	</td>
	</tr>
	</table>


	<table align=center>
	<tr>
<%
	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
   cntr = (startPage - 1) + count;

	if ((count == 1) && (hasPrevious))
	{
    %>
	<td colspan = 2>
  	<A href="revSetup.jsp?srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	</td>
	<%
  	}
	%>
	<td>
	<%

	 if (curPage  == cntr)
	 {
     %>
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
       %>

	   <A href="revSetup.jsp?srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>"><%= cntr%></A>
       <%
    	}
	 %>
	</td>
	<%
	  }

	if (hasMore)
	{
   %>
   <td colspan = 3 align = center>
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="revSetup.jsp?<%=searchCriteria%>&srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
	</td>
	<%
  	}
	%>
   </tr>
  </table>

  </Form>

	<%
	} //end of if body for page right
	else{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%
	} //end of else body for page right
}//end of if body for session
else {
%>
	<jsp:include page="timeout.html" flush="true"/>
<%}
%>
<br>
<div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<DIV class="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>

</body>
</html>

