<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<%@ page language = "java" import="com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.aithent.audittrail.reports.AuditUtils"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<jsp:useBean id="labJB" scope="request" class="com.velos.eres.web.lab.LabJB"/>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="studyB" scope="page" class="com.velos.eres.web.study.StudyJB" />


<SCRIPT>
function  validate(formobj){
	var fdaRegulated = document.getElementById("FDARegulated").value;
	var reasonDel = document.getElementById("reason_del").value;
	reasonDel=reasonDel.replace(/^\s+|\s+$/g,"");
	if(fdaRegulated=="1" && reasonDel.length<=0)
	{
		alert("<%=MC.M_Etr_MandantoryFlds%>");
		document.getElementById("reason_del").focus();
		return false;
			
	}
	if (!(validate_col('e-Signature',formobj.eSign))) return false

	if(isNaN(formobj.eSign.value) == true) {
		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/
	formobj.eSign.focus();
	return false;
   }
}
</SCRIPT>

<% String src;
	src= request.getParameter("srcmenu");
%>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<BODY>
<br>

<DIV class="formDefault" id="div1">
<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))	{
	//String userId =request.getParameter("userId");
	//int userPK =EJBUtil.stringToNum(userId);

	String delMode=request.getParameter("delMode");

	String pkey=request.getParameter("pkey");

	String patProtId=request.getParameter("patProtId");

	String totcount = request.getParameter("rowsReturned");

	String page1 = request.getParameter("page");
	String orderBy = request.getParameter("orderBy");
	String orderType = request.getParameter("orderType");
	String selLabs = request.getParameter("selLabs");

	//JM: 12Aug2008: #3297
	String studyId = request.getParameter("studyId"); //KM-Modified

	//KM-29Aug08 #3297
	String fromLab = request.getParameter("fromLab");


//JM: 07Jul2009: #INVP2.11
	String specimenPk=request.getParameter("specimenPk");
		specimenPk=(specimenPk==null)?"":specimenPk;

		String calldFrom=request.getParameter("calledFromPg");
		calldFrom=(calldFrom==null)?"":calldFrom;
		studyId = studyId==null?"0":studyId;
		String fdaRegulated="";
		studyB.setId(EJBUtil.stringToNum(studyId));
	    	studyB.getStudyDetails();
	    	fdaRegulated=studyB.getFdaRegulatedStudy();
		fdaRegulated = fdaRegulated==null?"0":fdaRegulated;

	if (delMode==null) {
		delMode="final";



%>
	<FORM name="deleteLabs" id="dellabsfrm" method="post" action="deleteLabs.jsp?srcmenu=<%=src%>" onSubmit="if (validate(document.deleteLabs)== false) { setValidateFlag('false'); return false; } else { setValidateFlag('true'); return true;}">
	<br><br>
	<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr>
	<td align="right" valign="top" width="50%"><%=LC.L_Reason_ForDelete %> 
	<%if(fdaRegulated.equals("1")) {%>
	<font class="Mandatory">*</font>
	<%}%>&nbsp;
	</td>
	<Input type="hidden" name="FDARegulated" id="FDARegulated" value="<%=fdaRegulated %>">
	<td><textarea maxlength="4000" name="reason_del" id="reason_del"></textarea>
	<br><font class="Mandatory"><%=MC.M_Limit4000_Char %></font>
	</td>
	</tr>
	</table>
	<P class="defComments"><%=MC.M_EsignToProc_WithDelLabs%><%--Please enter e-Signature to proceed with deletion of Labs .*****--%></P>
	<jsp:include page="submitBar.jsp" flush="true"> 
			<jsp:param name="displayESign" value="Y"/>
			<jsp:param name="formID" value="dellabsfrm"/>
			<jsp:param name="showDiscard" value="N"/>
	</jsp:include>

 	 <input type="hidden" name="delMode" value="<%=delMode%>">
	 <input type="hidden" name="selLabs" value="<%=selLabs%>">
     <input type="hidden" name="srcmenu" value="<%=src%>">
	 <input type="hidden" name="pkey" value="<%=pkey%>">
	 <input type="hidden" name="patProtId" value="<%=patProtId%>">
     <input type="hidden" name="rowsReturned" value="<%=totcount%>">
	<input type="hidden"  name="page" value="<%=page1%>">
	 <input type="hidden" name="orderBy" value="<%=orderBy%>">
     <input type="hidden" name="orderType" value="<%=orderType%>">
     <input type="hidden" name="studyId" value="<%=studyId%>">  <!--KM-->
	 <input type="hidden" name="fromLab" value ="<%=fromLab%>">


	<input type="hidden" name="specimenPk" value="<%=specimenPk%>">
	<input type="hidden" name="calledFromPg" value="<%=calldFrom%>">



</FORM>


<%
	} else {
			String reason_del = request.getParameter("reason_del");
			reason_del = reason_del.trim();
			reason_del = reason_del==null?"":reason_del;
			String eSign = request.getParameter("eSign");
			String oldESign = (String) tSession.getValue("eSign");
			if(!oldESign.equals(eSign)) {
%>
	 		  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
			} else {

	StringTokenizer idLabs=new StringTokenizer(selLabs,",");
	int numIds=idLabs.countTokens();
	String[] strArrLabs = new String[numIds] ;


	for(int cnt=0;cnt<numIds;cnt++)
			{
					strArrLabs[cnt] = idLabs.nextToken();

			}
	// Modified for INF-18183 ::: AGodara 
	int i = labJB.deleteLab(strArrLabs,"ER_PATLABS","pk_patlabs","number",AuditUtils.createArgs(tSession,reason_del));
	%>
		<br><br><br><br>
		<%
if(i == 0) {
%>
<p class = "successfulmsg" align = center> <%=MC.M_Del_Succ%><%--Deleted successfully.*****--%></p>
<%
		} else {
%>
<p class = "successfulmsg" align = center> <%=MC.M_DataCnt_BeDel%><%--Data could not be deleted .*****--%> </p>
<%
		}



	//Modified by Manimaran for the issue 3297
	if (fromLab.equals("pat")) {
%>
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=formfilledpatbrowser.jsp?searchFrom=search&selectedTab=11&pagenum=1&formPullDown=lab&mode=initial&srcmenu=<%=src%>&page=<%=page1%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>" >

<%} else if (fromLab.equals("patstd")){%>

	<META HTTP-EQUIV=Refresh CONTENT="1; URL=formfilledstdpatbrowser.jsp?searchFrom=search&selectedTab=8&pagenum=1&formPullDown=lab&mode=initial&srcmenu=<%=src%>&page=<%=page1%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&studyId=<%=studyId%>" >

<%}else if (calldFrom.equals("specimen")){%>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=specimenlabbrowser.jsp?searchFrom=search&specimenPk=<%=specimenPk%>&calledFromPg=<%=calldFrom%>&selectedTab=1&pagenum=1&formPullDown=specimen&mode=initial&srcmenu=<%=src%>&page=<%=page1%>&pkey=<%=pkey%>&patProtId=<%=patProtId%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&studyId=<%=studyId%>" >
	<%


   }
	} //end esign
	} //end of delMode
}//end of if body for session
else{
%>
<jsp:include page="timeout.html" flush="true"/>
<%
}
%>

<div class = "myHomebottomPanel"> 
  <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>

</DIV>

<div class ="mainMenu" id = "emenu">
<jsp:include page="getmenu.jsp" flush="true"/>
</div>

</BODY>
</HTML>

