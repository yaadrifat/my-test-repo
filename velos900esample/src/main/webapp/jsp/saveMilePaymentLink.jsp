<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="calendar.js"></SCRIPT>

<SCRIPT  Language="JavaScript1.2">
</SCRIPT>

</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>

<BODY>
  <jsp:useBean id="PayB" scope="request" class="com.velos.eres.web.milepayment.PaymentDetailJB"/>

  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*"%>
  <%

	String src = null;

	
	String paymentPk = request.getParameter("paymentPk");
	String pageRight = request.getParameter("pR");
	String studyId = request.getParameter("studyId");
	
HttpSession tSession = request.getSession(true); 

if (sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
   	String oldESign = (String) tSession.getValue("eSign");
  String eSign = request.getParameter("eSign");
    
  if(!(oldESign.equals(eSign))) {

	%>
	   <jsp:include page="incorrectesign.jsp" flush="true"/>	
	<%
} else 
 {

	
		String ipAdd = (String) tSession.getValue("ipAdd");
		String usr = (String) tSession.getValue("userId");
		
		String arApplyAmount[] = null;
		String arLevel1Id[] = null;
		String arMileAchvd[] = null;
		String arLevel2Id[] = null;
		String arLinkType[] = null;
		String arLinktoId[] = null;
		String arPkIds[] = null;
		String arIsSelected [] = null;
		String arMileDetailCount[] = null;
		 
		
		int count = 0;
		
		String applyAmount = "";
		String level1Id = "";
		String level2Id = "";
		String linkType = "";
		String linktoId = "";
		String pkId = "";
		String isSelected = "";
		int intMileDetailCount = 0;
		String mileDetailCount = "";
		InvoiceDetailDao inv = new InvoiceDetailDao();
		
		count = EJBUtil.stringToNum(request.getParameter("totalmilecount"));
		
		
		
		if (count > 1)
		{
			
			arApplyAmount = request.getParameterValues("applyAmount");
		//	arLevel1Id = request.getParameterValues("level1Id")	;
			arLinkType = request.getParameterValues("linkType")	;
			arLinktoId = request.getParameterValues("linktoId")	;
			arPkIds = request.getParameterValues("oldPk")	;
			arIsSelected = request.getParameterValues("isChecked");
			arMileDetailCount = request.getParameterValues("mileDetailCount")	;
	
		}
		else
		{
		
			isSelected = request.getParameter("isChecked");
			arIsSelected = new String[1];
			arIsSelected[0] = isSelected;
			
			
			applyAmount = request.getParameter("applyAmount");
			
			arApplyAmount = new String[1];
			arApplyAmount[0] = applyAmount;
			
			//level1Id = request.getParameter("level1Id")	;
			
			//arLevel1Id = new String[1];
			//arLevel1Id[0] = level1Id ;
			
				    
			linkType = request.getParameter("linkType")	;
	   		
			arLinkType = new String[1];
			arLinkType[0] = linkType;
	   		
			linktoId = request.getParameter("linktoId")	;
	
			arLinktoId  = new String[1];
			arLinktoId[0] = linktoId ;
			
			pkId = request.getParameter("oldPk")	;
	
			arPkIds = new String[1];
			arPkIds[0] = pkId;
			
			mileDetailCount = request.getParameter("mileDetailCount")	;
			arMileDetailCount = new String[1];
			arMileDetailCount [0] = mileDetailCount;
			
			
		}
		
		PaymentDetailsDao pd  = new PaymentDetailsDao();
		
			for (int i = 0; i< count; i++)
			{
		
				isSelected = arIsSelected[i];
				
				if (isSelected.equals("1")) //milestone was selected for the invoice
				{
				//set the record for the milestone
					 pd.setAmount(arApplyAmount[i]); 
					 pd.setFkPayment(paymentPk) ;
					 pd.setLinkedTo(arLinkType[i] ) ;
					 pd.setLinkToId(arLinktoId[i]) ;
					 pd.setIPAdd(ipAdd) ;
					 pd.setCreator(usr);
					 pd.setLastModifiedBy(usr);
					 pd.setLinkToLevel1("0");
				 	 pd.setLinkToLevel2("0");
					 pd.setId(arPkIds[i]);
					 pd.setLinkMileAchieved("0");
				
				
					// get the detailed records
					
					String arMileDetailApplyAmount[] = null;
					String arMileDetailLevel1[] = null;
					String arMileDetailType[] = null;
					String arMileDetailOldPk[] = null;
				
					intMileDetailCount	 = EJBUtil.stringToNum(arMileDetailCount[i]);
					System.out.println("intMileDetailCount" + intMileDetailCount  );
					
					 if (intMileDetailCount > 1)
					 {
					 	arMileDetailApplyAmount =  request.getParameterValues("applyAmount"+arLinktoId[i])	;
					 	
					 	System.out.println("arMileDetailApplyAmount" + arMileDetailApplyAmount + "*" + applyAmount+arLinktoId[i] + "*");
					 	arMileDetailLevel1 =  request.getParameterValues("level1Id"+arLinktoId[i])	;
					 	arMileDetailType =  request.getParameterValues("linkType"+arLinktoId[i])	;
					 	arMileDetailOldPk  =  request.getParameterValues("oldPk"+arLinktoId[i])	;
					 	arMileAchvd =  request.getParameterValues("level1ACHId"+arLinktoId[i])	;
	
					 	
					 }
					 else
					 {
					 	arMileDetailApplyAmount = new String[1];
					 	arMileDetailLevel1 = new String[1];
					 	arMileDetailType = new String[1];
					 	arMileDetailOldPk = new String[1];
					 	arMileAchvd =  new String[1];
					 	 
					 	arMileDetailApplyAmount[0] =  request.getParameter("applyAmount"+arLinktoId[i])	;
					 	arMileDetailLevel1[0] =  request.getParameter("level1Id"+arLinktoId[i])	;
					 	arMileDetailType[0] =  request.getParameter("linkType"+arLinktoId[i])	;
					 	arMileDetailOldPk[0] =  request.getParameter("oldPk"+arLinktoId[i])	;
					 	arMileAchvd[0] =  request.getParameter("level1ACHId"+arLinktoId[i])	;
					 }
					 
						 
					 
					 // if mileDetailCount > 0, add invoice detail records
					 
					 if (intMileDetailCount > 0)
					 {
					 	for (int det=0; det < intMileDetailCount ; det++)
					 	{
					 		
							 pd.setAmount(arMileDetailApplyAmount[det]); 
							 pd.setFkPayment(paymentPk) ;
							 pd.setLinkedTo(arMileDetailType[det] ) ;
							 pd.setLinkToId(arLinktoId[i]) ;
							 pd.setIPAdd(ipAdd) ;
							 pd.setCreator(usr);
							 pd.setLastModifiedBy(usr);
							 pd.setLinkToLevel1(arMileDetailLevel1[det]);
						 	 pd.setLinkToLevel2("0");
						     pd.setId(arMileDetailOldPk[det]);
						     pd.setLinkMileAchieved(arMileAchvd[det]);
							
									 
					 	}
					 
					 }
					 
				}		// is selected 
				  
			}
		
		
		
		int ret = 0;
		ret = PayB.setPaymentDetail(pd);
		 
		if (ret > 0)
		{
		
		
	%>
	<BR><BR><BR><BR><BR>
	<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully.*****--%> </p>
		
		<META HTTP-EQUIV=Refresh CONTENT="1; URL=linkMilestonePayBrowser.jsp?paymentPk=<%=paymentPk%>&studyId=<%=studyId%>&pR=<%=pageRight%>">	
	
		<%
		} %>
	
	<%

 } // end of esign
}//end of if body for session

else
{
%>
 <jsp:include page="timeout_childwindow.jsp" flush="true"/> 
  <%
}
%>

</BODY>

<script>

</script>

</HTML>





