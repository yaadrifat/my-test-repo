<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Fld_Copy%><%--Field Copy*****--%> </title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.service.util.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<jsp:include page="include.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
<script>
function openWin(fldDataType,fieldLibId,mode)
{  
	
	alert(fieldLibId);
	alert(mode);

	if (fldDataType.substr(0,1) == "E" )
	{		
		param1="editBox.jsp?fieldLibId="+fieldLibId+"&mode="+mode+"toRefresh=yes" ;
		alert (param1);
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=600");
		windowName.focus();
	}
	else if (fldDataType.substr(0,1) == "M" )
	{
		var paramArray = [fldDataType];    
		alert(getLocalizedMessageString("L_InsideM",paramArray));/*alert("inside M"+fldDataType);*****/
		 param2 = "multipleChoiceBox.jsp?fieldLibId="+fieldLibId+"&mode="+mode+"toRefresh=yes"  ;
		windowName= window.open(param2,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=600");
		windowName.focus();
	}   
 	
}

</script>





<jsp:useBean id="fieldlibB" scope="request" class="com.velos.eres.web.fieldLib.FieldLibJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>



<body>
<DIV class="popDefault" id="div1"> 

 <%@ page language = "java" import ="com.velos.eres.business.common.*,java.util.*, com.velos.eres.service.util.EJBUtil" %>
  
  <%

 HttpSession tSession = request.getSession(true); 
if (sessionmaint.isValidSession(tSession)) 
{

	String mode =request.getParameter("mode");
	String fieldLibId =request.getParameter("fieldLibId");
	String fldDataType =request.getParameter("fldDataType");

	//out.println("MODE IS "+ mode);
	//out.println("fieldLibId IS "+ fieldLibId);
	//out.println("fldDataType IS "+ fldDataType);

    String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");
	//String strid= request.getParameter("fieldId");
	
	int id=Integer.parseInt(fieldLibId);
	int newFldId;
	String flag="L";
	newFldId=fieldlibB.updateFieldLibRecord(id,flag,usr,ipAdd);
	//out.println("return value: "+newFldId);
	
	
	if ( newFldId > 0) 
   { %> 
   			<br>
			<br>
			<br>
              <P align="center" class="successfulmsg"><%=MC.M_Fld_CopiedSucc%><%--Field Copied Successfully*****--%> </P>
			  	<script>
					window.opener.location.reload();
				</script>	
	   <%     
	  	  if (fldDataType.substring(0,1).equals("E" ) )  
         { %>  
		 
  			<META HTTP-EQUIV=Refresh CONTENT="1; URL=editBox.jsp?fieldLibId=<%=newFldId%>&mode=<%=mode%>&toRefresh=yes">
			
       <%}
	   else if (fldDataType.substring(0,1).equals("M" )) 
         { %>  
		 
  			<META HTTP-EQUIV=Refresh CONTENT="1; URL=multipleChoiceBox.jsp?fieldLibId=<%=newFldId%>&mode=<%=mode%>&toRefresh=yes">
	   <%}%>
  
   <%} else
    { %>
	<P class="defComments"><%=LC.L_Fld_NotCopied%><%--Field Not Copied*****--%>  </P> 
   <button type="submit" onclick="window.history.back();"><%=LC.L_Back%></button></p>
   <%}%>      
  	
  
<% 

}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>






