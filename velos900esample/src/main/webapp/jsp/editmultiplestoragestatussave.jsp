<%--

	Project Name:		Velos eResearch
	Author:				Jnanamay Majumdar
	Created on Date:	15Nov2007
	Purpose:			Update page fop Storage Update Status page
	File Name:			editmultiplestoragestatussave.jsp
--%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</HEAD>

<body>


  <jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <jsp:useBean id="storageB" scope="page" class="com.velos.eres.web.storage.StorageJB"/>
  <jsp:useBean id="codeListJB" scope="page" class="com.velos.eres.web.codelst.CodelstJB"/>
  <%@ page language = "java" import = "com.velos.eres.web.specimen.SpecimenJB,com.velos.eres.business.common.*,java.util.*,java.text.*,com.velos.eres.service.util.*,com.velos.eres.web.storageStatus.StorageStatusJB"%>


  <%

	String eSign = request.getParameter("eSign");

	HttpSession tSession = request.getSession(true);

	if (sessionmaint.isValidSession(tSession))

   {

    String user = null;
	user = (String) tSession.getValue("userId");

	String accId = (String) tSession.getValue("accountId");

	String ipAdd = null;
	ipAdd = (String) tSession.getValue("ipAdd");


     %>
     <jsp:include page="sessionlogging.jsp" flush="true"/>

     <%

   	String oldESign = (String) tSession.getValue("eSign");

	if(!oldESign.equals(eSign))
	{
%>
  	   <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	}
	else
  	{


  	boolean bb = false;
  	CodeDao cdSpecStat = new CodeDao();
  	ArrayList subTypeStatuses = new ArrayList();
  	String subTypeStatus = "";
  	String specIdSeq = "";

  	int printme = -1;
  	int countRows = 0;
  	countRows = EJBUtil.stringToNum(request.getParameter("cntNumrows"));


			String[] fkStorage = new String[countRows];
			String[] storageStatus = new String[countRows];
			String[] stDate = new String[countRows];
			String[] storgeStudy = new String[countRows];
			String[] storgeUser = new String[countRows];
			String[] notes = new String[countRows];



 		for (int g=0 ; g<countRows ; g++) {


 	  		fkStorage[g] = request.getParameter("fkStorag"+(g+1));
 	  		storageStatus[g] = request.getParameter("storStat"+(g+1));
 	  		stDate[g] = request.getParameter("statDt"+(g+1));
 	  		storgeStudy[g] = request.getParameter("selStudyIds"+(g+1));
 	  		storgeUser[g] = request.getParameter("creatorId"+(g+1));
 	  		notes[g] = request.getParameter("statusNotes"+(g+1));


 		}


	 for (int f=0 ; f<countRows ; f++) {


  		StorageStatusJB stStatJB = new StorageStatusJB();

			if (!storageStatus[f].equals("") && !stDate[f].equals("")){
				stStatJB.setFkStorage(fkStorage[f]);
				stStatJB.setFkCodelstStorageStat(storageStatus[f]);


					//JM: 09Jan2008----------------------
					// Adding a new Status for a Storage should also add a similar status for all Child storages, but should be customizable for each storage status codelist option

					String customCol = "";
					int sret = -1;


					customCol = codeListJB.getCodeCustomCol(EJBUtil.stringToNum(storageStatus[f]));
					customCol = (customCol==null)?"":customCol;


					if (customCol.equals("C")){

					String numStorageChilds = "";
					numStorageChilds = storageB.findAllChildStorageUnitIds(fkStorage[f]);
					numStorageChilds = (numStorageChilds==null)?"":numStorageChilds;

					String childs[] = numStorageChilds.split(",");


						for (int k=0; k<childs.length; k++){

							StorageStatusJB strStatB = new StorageStatusJB();

							strStatB.setFkStorage(childs[k]);
							strStatB.setSsStartDate(stDate[f]);
							strStatB.setFkCodelstStorageStat(storageStatus[f]);
							strStatB.setSsNotes(notes[f]);
							strStatB.setFkUser(storgeUser[f]);
							strStatB.setFkStudy(storgeStudy[f]);
							strStatB.setIpAdd(ipAdd);
							strStatB.setCreator(user);
							sret=strStatB.setStorageStatusDetails();
						}

					}//end of if, customCol

					//JM: 09Jan2008----------------------

				stStatJB.setSsStartDate(stDate[f]);
				stStatJB.setFkStudy(storgeStudy[f]);
				stStatJB.setFkUser(storgeUser[f]);
				stStatJB.setSsNotes(notes[f]);
				stStatJB.setCreator(user);
				stStatJB.setIpAdd(ipAdd);
				printme=stStatJB.setStorageStatusDetails();
			}


	}


	if(printme>=0){
%>
	  <br>
      <br>
      <br>
      <br>
      <br>
      <p class = "sectionHeadings" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>


     <script>
			window.opener.location.reload();
			setTimeout("self.close()",2000);
	  </script>


<%}else{%>

      <br>
      <br>
      <br>
      <br>
      <br>
      <p class = "sectionHeadings" align = "center" > <%=MC.M_DataCnt_Svd%><%--Data could not be saved*****--%></p>
      <table width="70%" >
      <tr>
      <td width="10%"></td>
      <td width="60%" align="center">&nbsp;&nbsp;
      <button onclick="history.go(-1);"><%=LC.L_Back%></button>
</td></tr>	</table>

<%}


}//end of eSign check

}//end session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

	<div class = "myHomebottomPanel">
	    <jsp:include page="bottompanel.jsp" flush="true"/>
	</div>



</BODY>

</HTML>
