<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<jsp:include page="jqueryUtils.jsp" flush="true"/>
<html>
<head>
<title><%=LC.L_Cal_Lib%><%-- Calendar Library*****--%></title>
<meta http-equiv="Content-Type" content="text/html; charset=">
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<script>


function newCalendar(formobj,url){

window.opener.location=url;
//window.opener.location.reload();
calassoc=formobj.calassoc.value;

//formobj.action=url;
//if (calassoc=="S") formobj.target="studyadmin";
//else formobj.target="studysetup";
//formobj.submit();
this.window.close();
}

function openCalType(pageRight)
{
		if (f_check_perm(pageRight,'N') == true) {
		param1="fieldCategory.jsp?type=L&mode=N";
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=300,top=250 ,left=200");
		windowName.focus();
		}
}

function openEditCalTypeWin(formobj,catLibId,pageRight)
{
        param1="fieldCategory.jsp?catLibId="+catLibId+"&mode=M&type=L";
		windowName= window.open(param1,"information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=650,height=300");
		windowName.focus();
}

function confirmBox(calName,pageRight)
{
	if (f_check_perm(pageRight,'E') == true)
		{
		var paramArray = [calName];
			msg=getLocalizedMessageString("L_Del_FrmLib",paramArray);/*msg="Delete " + calName + " from Library?";*****/

			if (confirm(msg))
			{
    			return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
}

// commented for sorting on 04-07-2005
//function setOrder(formObj,orderBy,pgRight) //orderBy column number
function setOrder(formObj,orderBy)
{
	var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";
	}

	lsrc = formObj.srcmenu.value;
	lcurPage = formObj.page.value;
	lselectedtab= formObj.selectedTab.value;

	// commented for sorting on 04-07-2005					//formObj.action="allPatientNew.jsp?srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType+"&selectedTab="+lselectedtab;
	formObj.action="protocollist.jsp?mode=final&srcmenu="+lsrc+"&page="+lcurPage+"&orderBy="+ orderBy +"&orderType="+lorderType;
	formObj.submit();
}

//Bug #8549
function openSelectProtocolWin(url,spanId){
	document.getElementById(spanId).style.display = "none";
	window.location.href = url;
}

</script>

<% String src="";
String from = "calendar";


//This parameter captures if this screen was called from study admin or study setup
String calAssoc = request.getParameter("calassoc");
if (calAssoc==null) calAssoc="P";


src= request.getParameter("srcmenu");
String selectedTab = request.getParameter("selectedTab");
String calledFrom = "";
calledFrom = ((request.getParameter("calledFrom")) == null)?"P":request.getParameter("calledFrom");

if (calledFrom.equals("L") || calledFrom.equals("P")) {%>
   <jsp:include page="panel.jsp" flush="true">
   <jsp:param name="src" value="<%=src%>"/>
   </jsp:include>

<%}else if (calledFrom.equals("S")){ //called from study setup%>
	<SCRIPT LANGUAGE="JavaScript" SRC="validations.js"></SCRIPT>
	<SCRIPT LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<%-- agodara 08Aug11: Removed for Bug #6728, already using Whichcss_skin.js in panel.jsp --%>
<%}%>


<body>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="eventdefB" scope="request" class="com.velos.esch.web.eventdef.EventdefJB"/>
<jsp:useBean id="catLibJB" scope="request"  class="com.velos.eres.web.catLib.CatLibJB"/>
<jsp:useBean id ="userB" scope="page" class="com.velos.eres.web.user.UserJB"/> <!--km-->
<jsp:useBean id ="userSiteB" scope="request" class="com.velos.eres.web.userSite.UserSiteJB"/>

<%@ page language = "java" import = "com.velos.eres.business.section.*,java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.*,com.velos.eres.web.studyRights.StudyRightsJB,com.velos.eres.service.util.EJBUtil,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.esch.business.common.SchCodeDao"%>

<%if (calledFrom.equals("L") || calledFrom.equals("P")) {%>
<DIV class="tabDefTopN" id="div1">
<%}else if (calledFrom.equals("S")){ %>
<DIV class="popDefault" id="div1">
<%}%>


  <%

   HttpSession tSession = request.getSession(true);

   if (sessionmaint.isValidSession(tSession))
	{

	if (calledFrom.equals("S")) {
	%>
	<jsp:include page="sessionlogging.jsp" flush="true"/>

	<%
	}

	int accId = EJBUtil.stringToNum((String) tSession.getValue("accountId"));
//SV, commented 10/28/04, cal-enh
	String usr = (String) tSession.getValue("userId");

	String study = (String) tSession.getValue("studyId");

	//reset session attribute
	tSession.setAttribute("newduration", "");
	tSession.setAttribute("protocolname", "");


	String searchName = request.getParameter("searchName");
	if (searchName==null) searchName="";

		int fkCati = 0 ;
	     int forLastAll = 0 ;
	      String fkCat = request.getParameter("ByCalType");

		  if ( fkCat != null )
		 {
			 fkCati = EJBUtil.stringToNum(fkCat);
		}
%>

<%
	if (calledFrom.equals("L") || calledFrom.equals("P")) {%>
  		<jsp:include page="librarytabs.jsp" flush="true"/>
	<%}%>

  <%

   int pageRight= 0;

  if((study == "" || study == null) && (calledFrom.equals("S"))) {

%>
	  <jsp:include page="studyDoesNotExist.jsp" flush="true"/>
  <%
  } else {
		int studyId = 0;
		if (calledFrom.equals("S") ){
			 studyId = EJBUtil.stringToNum(study);
			 StudyRightsJB stdRights =(StudyRightsJB) tSession.getValue("studyRights");
	  		 if ((stdRights.getFtrRights().size()) == 0){
		 		pageRight= 0;
			 }else{
				pageRight = Integer.parseInt(stdRights.getFtrRightsByValue("STUDYSEC"));
	   		 }
//Added by Gopu to fix the bugzilla Issue #2359

		GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
		pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));

		} else if ((calledFrom.equals("P")) || (calledFrom.equals("L"))) {
				GrpRightsJB grpRights = (GrpRightsJB) tSession.getValue("GRights");
				pageRight = Integer.parseInt(grpRights.getFtrRightsByValue("CALLIB"));
		}

		String tab = request.getParameter("selectedTab");

	   	if (pageRight > 0) {

			String eventType="P";

  	        int counter = 0;
			Integer id;

			String name="";
			String description="";
			String chainId="";
			String duration="";
			String status="";
			String statusText = "";
			String sharedWith="";
			String sharedWithText="";
			String calTypeFilter="";


			String whereClause="";
			if (!(searchName.equals(""))) {
			   whereClause = " AND ( lower(DESCRIPTION) like lower(trim('%" + searchName.trim() + "%')) or lower(NAME) like lower(trim('%" + searchName.trim() + "%')) )";
			}

			//////////FOR PAGINATION
			String calSql = "";
			String calSql1 = "";
			String calSql2 = "";
			String calSql3 = "";
			String countSql = "";
			String countSql1 = "";
			String countSql2 = "";
			String countSql3 = "";

			// userid being used for account information may be misleading.

		//km-to get the forms which have access rights other than the primary organization

		userB.setUserId(EJBUtil.stringToNum(usr));
		userB.getUserDetails();
		int userPrimOrg = EJBUtil.stringToNum(userB.getUserSiteId());

		ArrayList userSiteIds = new ArrayList();
		ArrayList userSiteSiteIds = new ArrayList();
     		ArrayList userSiteRights = new ArrayList();

		UserSiteDao userSiteDao = userSiteB.getUserSiteTree(accId,EJBUtil.stringToNum(usr));
		userSiteIds = userSiteDao.getUserSiteIds();
		userSiteSiteIds = userSiteDao.getUserSiteSiteIds();
		userSiteRights = userSiteDao.getUserSiteRights();
		int len1 = userSiteIds.size();


			String siteIdAcc="";
			int accrights=0;
			int userSiteSiteId=0;
			int userSiteRight=0;
			for (int cnt=0;cnt<len1;cnt++) {
	 		userSiteSiteId = EJBUtil.stringToNum(((userSiteSiteIds.get(cnt)) == null)?"-":(userSiteSiteIds.get(cnt)).toString());
     			userSiteRight = EJBUtil.stringToNum(((userSiteRights.get(cnt)) == null)?"-":(userSiteRights.get(cnt)).toString());

			if (userPrimOrg!=userSiteSiteId)
			   accrights=userSiteRight;
			if(accrights!=0)
			siteIdAcc=siteIdAcc+(userSiteSiteId+",");

			}
		        siteIdAcc=siteIdAcc+userPrimOrg;



			calSql1 = " select  EVENT_ID,catlib_name,fk_catlib, "
							+" CHAIN_ID,  "
							+"EVENT_TYPE,"
							+"NAME, "
							+"NOTES, "
							+"COST, "
							+"COST_DESCRIPTION,"
							+"DURATION,"
							+"USER_ID, "
							+"LINKED_URI,"
							+"FUZZY_PERIOD, "
							+"MSG_TO, "
							+" FK_CODELST_CALSTAT, " //KM-DFin9
							+ "(select trim(codelst_subtyp) from sch_codelst where pk_codelst = fk_codelst_calstat) codelst_subtyp, "
							+ "(select codelst_desc from sch_codelst where pk_codelst = fk_codelst_calstat)  codelst_desc, "
                    		+" case when (nvl(length(DESCRIPTION),0)<100) then NVL(DESCRIPTION,'-')"
							+" when (length(DESCRIPTION)>=100) then substr(DESCRIPTION,1,100)||'...' end	 as  DESCRIPTION, "
							+ "DISPLACEMENT, "
							+ "CALENDAR_SHAREDWITH "
							+" FROM EVENT_DEF evt,er_catlib lib"
							+" WHERE  evt.user_id =  "+ accId
							+" and lib.pk_catlib=evt.fk_catlib "
							+ " and (evt.calendar_sharedwith = 'A' OR "
							+ " evt.event_id in "
							+ " (select osh.fk_object  from er_objectshare osh "

							+ " where osh.objectshare_type in('U','O') "
							+ " and osh.object_number = 3"
		+ " and (osh.fk_objectshare_id = " +usr +" or osh.fk_objectshare_id in("+siteIdAcc+ "))))";


		if (calledFrom.equals("S"))
		{
		//Commented code to show all the calnder irrespective of the status

		//	calSql2 = "AND STATUS = 'F'";

		}

		if(fkCati == 0 || fkCati == -1 )
			{
				calTypeFilter=" ";
			}

			else
			{
				calTypeFilter=" and fk_catlib=" +fkCati ;
			}

			calSql3  =	" AND  TRIM(UPPER(EVENT_TYPE))= '"+eventType + "'"
								+ whereClause ;
			// commented for sorting on 04-07-2005
							//	+" ORDER BY NAME " ;
			calSql = calSql1 +calSql2 + calTypeFilter +calSql3 ;


//			countSql1 = " select count(*) "
//									+"FROM ESCH.EVENT_DEF  "
//									+"WHERE  USER_ID = "+accId ;
			countSql1 = "select count(*) from  ( " ;
			countSql2 = ")"  ;

//				if (calledFrom.equals("S"))
									{
//				countSql2 = "AND STATUS = 'F'";
									}
//				countSql3 = "AND TRIM(UPPER(EVENT_TYPE))= '"+eventType + "'"
//									+ whereClause
//									+" ORDER BY NAME ";
//			countSql =countSql1+countSql2+countSql3;
			countSql = countSql1 + calSql + countSql2;


			String sortOrderType="";
			String pagenum = "";
			int curPage = 0;
			long startPage = 1;
			long cntr = 0;
			String pagenumView = "";
			int curPageView = 0;
			long startPageView = 1;
			long cntrView = 0;
			String pullDown="";
			ArrayList ids= new ArrayList();
		 ArrayList names= new ArrayList();
		 CatLibDao catLibDao=new CatLibDao();

			pagenum = request.getParameter("page");
			if (pagenum == null)
			{
			pagenum = "1";
			}
			curPage = EJBUtil.stringToNum(pagenum);

			String orderBy = "";
			orderBy = request.getParameter("orderBy");

			String orderType = "";
			orderType = request.getParameter("orderType");

			if (orderType == null)
			{
			orderType = "asc";
			}

//Added by Ganapathy on 04-07-2005 for sorting
if (EJBUtil.isEmpty(orderBy) || (orderBy==null)){
				orderBy="catlib_name";
				sortOrderType="lower(catlib_name) asc ,lower(name) asc " ;
	} else {
	 		if (orderBy.equals("NAME")){
			sortOrderType = "lower(catlib_name) asc, lower(name) "+   orderType;
		} else if (orderBy.equals("catlib_name")){
		sortOrderType = "lower(catlib_name) "+ orderType + ", lower(name) asc"   ;
		}
		else {

			sortOrderType = "lower("+orderBy+")"+orderType;
		}
	}



			pagenumView = request.getParameter("pageView");
			if (pagenumView == null)
			{
			pagenumView = "1";
			}
			curPageView = EJBUtil.stringToNum(pagenumView);

			String orderByView = "";
			orderByView = request.getParameter("orderByView");

			String orderTypeView = "";
			orderTypeView = request.getParameter("orderTypeView");

			if (orderTypeView == null)
			{
			orderTypeView = "asc";
			}

			long rowsPerPage=0;
			long totalPages=0;
			long rowsReturned = 0;
			long showPages = 0;
			long totalRows = 0;
			long firstRec = 0;
			long lastRec = 0;
			boolean hasMore = false;
			boolean hasPrevious = false;




			rowsPerPage =  Configuration.MOREBROWSERROWS ;
 			totalPages =Configuration.PAGEPERBROWSER ;


			BrowserRows br = new BrowserRows();

			  br.getPageRows(curPage,rowsPerPage,calSql,totalPages,countSql,sortOrderType,"");
			rowsReturned = br.getRowReturned();
			showPages = br.getShowPages();

		     startPage = br.getStartPage();

			hasMore = br.getHasMore();

			hasPrevious = br.getHasPrevious();


			totalRows = br.getTotalRows();


			firstRec = br.getFirstRec();

			lastRec = br.getLastRec();


			///////////FOR PAGINATION END

 		catLibDao= catLibJB.getCategoriesWithAllOption(accId,"L",forLastAll);
		 ids = catLibDao.getCatLibIds();
		 names= catLibDao.getCatLibNames();
		 pullDown=EJBUtil.createPullDown("ByCalType",fkCati, ids, names);


%>

<%if (calledFrom.equals("L") || calledFrom.equals("P")) {%>
</div>
<DIV class="tabDefBotN" id="div1">
<%}%>
<!--Modified by Manimaran to fix the Bug2404-->
  <Form name="protocolbrowser" method="post" action="protocollist.jsp?page=1" onsubmit="">

    <input type="hidden" name="srcmenu" value='<%=src%>'>
    <input type="hidden" name="selectedTab" value='<%=tab%>'>
    <input type="hidden" name="calledFrom" value='<%=calledFrom%>'>
	<Input type="hidden" name="orderBy" value="<%=orderBy%>">
	<Input type="hidden" name="page" value="<%=curPage%>">
	<Input type="hidden" name="calassoc" value="<%=calAssoc%>">
	<Input type="hidden" name="orderType" value="<%=orderType%>">

<div class="tmpHeight"></div>
	<table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign">
      <tr height="20" >
		<td colspan="5"><b><%=LC.L_Search_By%><%-- Search By*****--%></b></td>
	</tr>
	<tr>

 	 <td width="15%"><%=LC.L_Cal_Name%><%-- Calendar Name*****--%>:</td>
	   <td width="25%"><input type="text" name="searchName" size = 30 MAXLENGTH = 50 value="<%=searchName%>">	   </td>
	   <td width="10%" align="right"><%=LC.L_Category%><%-- Type*****--%>:&nbsp;</td>
	   <td width="25%"><%=pullDown%> </td>
		<td width ="23%" ><button type="submit"><%=LC.L_Search%></button> </td>
	</tr>
	</table>
	<br>

   <table width="99%" cellspacing="0" cellpadding="0" border="0" class="basetbl midAlign">
    <tr >
      <td width = "25%">
        <P class = "defComments">
		<%if (searchName.equals("")) { %><%=LC.L_Lib_Calendars%><%-- Library Calendars*****--%>:<%}
		else { %>
		<%=MC.M_CalsMatch_SrchCriteria%><%-- The following Calendars match your search criteria*****--%>: "<%=searchName%>".
 		<%}%>
		<%if (calledFrom.equals("S")) { %><%=MC.M_SelCal_UsedInPcol%><%-- Select the Calendar that you wish to use in your Protocol.*****--%><%}%> </P>
      </td>
	  <%if (calledFrom.equals("P") || calledFrom.equals("L")){%>
	  <td width="24%">&nbsp;&nbsp;<A href="javascript:openCalType(<%=pageRight%>)"><img src="../images/jpg/AddNewCategory.gif" title="<%=MC.M_New_CalCategory%><%-- New Calendar Category*****--%>" border="0"/></A>
	  </td>
      <td width = "48%" align="right">
      	<p>  <A href="copyprotocol.jsp?srcmenu=<%=src%>&selectedTab=1&from=initial&calledFrom=<%=calledFrom%>" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_Copy_ExistingCal_Upper%><%-- COPY AN EXISTING CALENDAR*****--%></A>
       &nbsp;&nbsp;&nbsp;&nbsp;<A href="protocolcalendar.jsp?srcmenu=<%=src%>&mode=N&selectedTab=1&calledFrom=P" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_Create_ANewCal_Upper%><%-- CREATE A NEW CALENDAR*****--%></A> </p>
      </td>
	  <%}else if (calledFrom.equals("S")){%>
      <td width = "48%" align="right">
      	<p> &nbsp;&nbsp;<A href="javascript:newCalendar(document.protocolbrowser,'protocolcalendar.jsp?srcmenu=<%=src%>&mode=N&selectedTab=1&calledFrom=P')" onClick="return f_check_perm(<%=pageRight%>,'N')"><%=MC.M_Create_ANewCal_Upper%><%-- CREATE A NEW CALENDAR*****--%></A> </p>
      </td>
	  <%}%>
    </tr>
   </table>


    <table class="outline midAlign" width="99%" cellpadding="0" cellspacing="0"  >
       <tr> <!-- Modified by Ganapathy for Column sorting 04-07-2005 -->
       		 <th width="15%" onclick="setOrder(document.protocolbrowser,'catlib_name')"> <%=LC.L_Cal_Type%><%-- Calendar Type*****--%> &loz;</th>
        	<th width="15%" onclick="setOrder(document.protocolbrowser,'NAME')"> <%=LC.L_Name%><%-- Name*****--%> &loz;</th>
	        <th width="25%" onclick="setOrder(document.protocolbrowser,'DESCRIPTION')"> <%=LC.L_Description%><%-- Description*****--%> &loz;</th>
	        <th width="15%" onclick="setOrder(document.protocolbrowser,'codelst_subtyp')"> <%=LC.L_Status%><%-- Status*****--%> &loz;</th>
		<th width="13%" onclick="setOrder(document.protocolbrowser,'CALENDAR_SHAREDWITH')"><%=LC.L_Shared_with%><%-- Shared with*****--%> &loz;</th>
	        <th width="15%"><%=LC.L_Reports%><%-- Reports*****--%></th>
	        <th width="5%">
        	<!-- Bug#9818 16-May-2012 -Sudhir-->
	        <%if (!(status.equals("O")||status.equals("A")||status.equals("D"))  && (calledFrom.equals("S"))) {%>
        	<%=LC.L_Select%><%--Select*****--%>
        	<%}else if (calledFrom.equals("P") || calledFrom.equals("L")){%>
	        <%=LC.L_Delete%><%--Delete*****--%>
	        <%} %>
	        </th>
      </tr>
       <%

	int i ;
	String  idStr = "" ;
	String catLibName="";
    for(i = 1;i<=rowsReturned;i++)
	{

		idStr = br.getBValues(i,"EVENT_ID")  ;
		eventType =  br.getBValues(i,"EVENT_TYPE")  ;
		name =  br.getBValues(i,"NAME")  ;
		//catLibName=br.getBValues(i,"CATLIB_NAME");
		String tempCatName =  br.getBValues(i,"FK_CATLIB");


						if ( i !=1 )
						{
						  	//formType=tempFormType;
							 catLibName  = br.getBValues(i,"CATLIB_NAME");

						}
						if (   (  i == 1 ) ||    (     ! tempCatName.equals(   br.getBValues(i-1,"FK_CATLIB")    )       ))
						{

						   // formType=tempFormType;
						    catLibName = br.getBValues(i,"CATLIB_NAME");

						}
						else
						{
							catLibName="";
						}

		description = (String) br.getBValues(i,"DESCRIPTION")  ;
		if (description.equals("null")) description = "-";
    	chainId = br.getBValues(i,"CHAIN_ID")  ;
		duration =  br.getBValues(i,"DURATION")  ;

		//KM- Enh-DFIN#9
		status = br.getBValues(i,"codelst_subtyp");
		statusText = br.getBValues(i,"codelst_desc");

		sharedWith = br.getBValues(i,"CALENDAR_SHAREDWITH")  ;


		String catLibId= br.getBValues(i,"FK_CATLIB");
		catLibId = (catLibId==null )?"-":( catLibId) ;

		sharedWith = (sharedWith == null)?new String("A"):sharedWith; //SV, 10/14/04, while opening old calendars.

		if(sharedWith.equals("P"))
			{
			sharedWithText=LC.L_Private;/*sharedWithText="Private";*****/
			}
		  else if(sharedWith.equals("A"))
			{
			  sharedWithText=LC.L_All_AccUsers;/*sharedWithText="All Account Users";*****/
			}
		  else if(sharedWith.equals("G"))
			{
			  sharedWithText=MC.M_AllUsr_InGrp;/*sharedWithText="All Users in a Group";*****/
			}
		  else if(sharedWith.equals("S"))
			{
			  sharedWithText=MC.M_UsrsStd_Team;/*sharedWithText="All Users in a "+LC.Std_Study+" Team";*****/
			}
		  else if(sharedWith.equals("O"))
			{
			  sharedWithText=MC.M_AllUsr_InOrg;/*sharedWithText="All Users in an Organization";*****/
			}

		if ((i%2)==0) {

  %>
      <tr class="browserEvenRow">
        <%

		}

		else{

  %>
      <tr class="browserOddRow">
        <%

		}

  %>
  		<td>&nbsp;<A href="#" onClick="openEditCalTypeWin(document.protocolbrowser,<%=catLibId%>,<%=pageRight%>)"><%=catLibName%></A>
				</td>
        <td>
		<%if (calledFrom.equals("P") || calledFrom.equals("L")){%>
<!--			<A href="protocolcalendar.jsp?protocolId=<%=idStr%>&srcmenu=<%=src%>&mode=M&selectedTab=1&calledFrom=P&duration=<%=duration%>&pageNo=1&calStatus=<%=status%>"> <%=name%> </A>-->
			<!--JM: 14Apr2008 modified
			<A href="displayDOW.jsp?selectedTab=4&mode=M&duration=<%--=duration--%>&calledFrom=<%--=calledFrom--%>&protocolId=<%--=idStr--%>&srcmenu=<%--=src--%>&calStatus=<%--=status--%>&pageNo=1&displayType=V&headingNo=1&displayDur=3&pageRight=<%--=pageRight--%>&protocolName=<%--=StringUtil.encodeString(name)--%>" > <%--=name--%></A>
			-->
			<!--AK Commented for Enhancement PCAL_20801:
			<A href="manageVisits.jsp?protocolId=<%--=idStr--%>&srcmenu=<%--=src--%>&selectedTab=3&mode=M&calledFrom=<%--=calledFrom--%>&calStatus=<%--=status--%>&displayType=V&headingNo=1&pageNo=1&duration=<%--=duration--%>&displayDur=3&pageRight=<%--=pageRight--%>&calassoc=<%--=calAssoc--%>"><%--=name--%></A>
			-->
			<A href="fetchProt2.jsp?selectedTab=6&amp;mode=M&amp;duration=<%=duration%>&amp;calledFrom=<%=calledFrom%>&amp;protocolId=<%=idStr%>&amp;srcmenu==<%=src%>&amp;calStatus=<%=status%>&amp;displayType=V&amp;pageNo=1&amp;displayDur=3&amp;calassoc=<%=calAssoc%>&amp;calName=<%=StringUtil.encodeString(name)%>"><%=name%> </A>
           
		<%} else {%>
			<%=name%>
		<%}%>
		</td>
		<td> <%=description%> </td>
        <td> <%=statusText%></td>
		<td> <%=sharedWithText%> </td>

<td>
	<a href=#  onclick="window.open('repRetrieve.jsp?protId=<%=idStr%>&repId=106&repName=<%=LC.L_Pcol_CalPreview%><%--Protocol Calendar Preview*****--%>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><img border="0" title="<%=LC.L_Preview%>" alt="<%=LC.L_Preview%>" src="./images/Preview.gif" ><%//=LC.L_Preview %><%-- Preview*****--%><%//=LC.L_Preview%><%-- Preview*****--%></a>&nbsp;&nbsp;&nbsp;&nbsp;
	<a href=#  onclick="window.open('repRetrieve.jsp?protId=<%=idStr%>&repId=107&repName=<%=LC.L_Mock_Schedule%><%--Mock Schedule*****--%>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><%=LC.L_Schedule%><%-- Schedule*****--%></a>
</td>

	 <% 
	//@Ankit 14Mar11 #5921
	 if (!(status.equals("O")||status.equals("A")||status.equals("D"))  && (calledFrom.equals("S"))) {
	  //User can select only if the status is Freezed
	  //User is allowed to select any status calender- requirement October
	  %>
	        <td> <span id="<%=chainId%>"><A onclick='openSelectProtocolWin("selectprotocol.jsp?eventId=<%=chainId%>&srcmenu=<%=src%>&from=<%=from%>&calassoc=<%=calAssoc%>&name=<%=StringUtil.encodeString(name)%>",<%=chainId%>);' href="javascript:void(0)"><%=LC.L_Select%><%-- Select*****--%></A></span></td>
	<%}else if (calledFrom.equals("P") || calledFrom.equals("L")){%>
		<td  align="center"><A href="calLibraryDelete.jsp?protocolId=<%=idStr%>&srcmenu=<%=src%>&selectedTab=<%=tab%>" onclick="return confirmBox('<%=name%>',<%=pageRight%>)"><img src="./images/delete.gif" title="<%=LC.L_Delete%>" border="0"/></A></td>
	<%}else { %>
	    <td></td>
	<%}%>
      </tr>
      <%

	}//for

%>
<!-- Bug#9751 16-May-2012 Ankit -->
</table>
<table width="98%" cellspacing="0" cellpadding="0" class="midalign">
		<tr><td>
		<% if (totalRows > 0)
		{ Object[] arguments = {firstRec,lastRec,totalRows}; 
		%>
					<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
		<%} else {%>
			<font class="recNumber"><%=MC.M_NoRecordsFound%><%-- No Records Found*****--%></font>
		<%}%>
		</td></tr>
</table>

<div align="center" class="midalign">
<%

	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
   cntr = (startPage - 1) + count;

	if ((count == 1) && (hasPrevious))
	{
    %>
	<!--Modified by Manimaran to fix the Bug2404-->
	<% if (calledFrom.equals("L")){%>
  	<A href="protocollist.jsp?selectedTab=<%=tab%>&calledFrom=<%=calledFrom%>&srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&searchName=<%=searchName%>&ByCalType=<%=fkCati%>">< <%=LC.L_Previous%><%-- Previous*****--%> <%=totalPages%> > </A>&nbsp;&nbsp;&nbsp;&nbsp;
	<%}else{%>
	<A href="protocollist.jsp?calassoc=<%=calAssoc%>&selectedTab=<%=tab%>&calledFrom=<%=calledFrom%>&srcmenu=<%=src%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&searchName=<%=searchName%>&ByCalType=<%=fkCati%>">< <%=LC.L_Previous%><%-- Previous*****--%> <%=totalPages%> > </A>&nbsp;&nbsp;&nbsp;&nbsp;
	<%}
  	}
	 if (curPage  == cntr)
	 {
     %>
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
       %>
		<% if (calledFrom.equals("L")){%>
		<!--Modified by Gopu to fix the bugzilla Issue #2588 Calendar added into Admin Schedule gets added to Study Setup instead. -->
	<!--Modified by Manimaran to fix the Bug 2404 -->
	<A href="protocollist.jsp?calassoc=<%=calAssoc%>&selectedTab=<%=tab%>&calledFrom=<%=calledFrom%>&srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&searchName=<%=searchName%>&ByCalType=<%=fkCati%>"><%= cntr%></A>
	   <%}else{%>
	    <A href="protocollist.jsp?calassoc=<%=calAssoc%>&selectedTab=<%=tab%>&calledFrom=<%=calledFrom%>&srcmenu=<%=src%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&searchName=<%=searchName%>&ByCalType=<%=fkCati%>"><%= cntr%></A>
		<%}%>
       <%
    	}
	  }

	if (hasMore)
	{
   %>
	<!-- Modified by Manimaran to fix the Bug 2404 -->
	<!-- Modified by Gopu to fix the bugzilla Issue #2588 -->
 	<% if (calledFrom.equals("L")){%>
    &nbsp;&nbsp;&nbsp;&nbsp;<A href="protocollist.jsp?selectedTab=<%=tab%>&calledFrom=<%=calledFrom%>&srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&searchName=<%=searchName%>&ByCalType=<%=fkCati%>">< <%=LC.L_Next%><%-- Next*****--%> <%=totalPages%> ></A>
	<%}else{%>
	&nbsp;&nbsp;&nbsp;&nbsp;<A href="protocollist.jsp?calassoc=<%=calAssoc%>&selectedTab=<%=tab%>&calledFrom=<%=calledFrom%>&srcmenu=<%=src%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>&searchName=<%=searchName%>&ByCalType=<%=fkCati%>">< <%=LC.L_Next%><%-- Next*****--%> <%=totalPages%> ></A>
	<%}
  	}
	%>
  </div>
  
  </Form>

  <%}else{%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
  <%}
	} //end of if body for check on studyId

	} //end of if session times out

else

{

%>
  <jsp:include page="timeout.html" flush="true"/>
  <%

} //end of else body for page right

%>

<%if (calledFrom.equals("L") || calledFrom.equals("P")) {%>
 <div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
  </div>
<%}%>

</div>

<%if (calledFrom.equals("L") || calledFrom.equals("P")) {%>
<%}%>

</body>

</html>












