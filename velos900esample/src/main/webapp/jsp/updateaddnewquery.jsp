<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>

<head>

<title><%=LC.L_Form_Query%><%--Form Query*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>

<BODY>
 <jsp:useBean id="formQueryB" scope="request" class="com.velos.eres.web.formQuery.FormQueryJB"/>
  <jsp:useBean id="formQueryStatusB" scope="request" class="com.velos.eres.web.formQueryStatus.FormQueryStatusJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>

<%@ page language = "java" import ="com.velos.eres.business.common.*,java.util.*, com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*" %>

<%
 int iret = -1;
 int iupret = 0;
 String type="";
 String recordType = "";
 String mode =request.getParameter("mode");
 if(mode == null)
  mode = "";

 String eSign = request.getParameter("eSign");
 int pageRight =0 ;
 pageRight = EJBUtil.stringToNum((String)session.getAttribute("formQueryRight"));

HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession))
{

%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%
	String oldESign = (String) tSession.getValue("eSign");


	if(!oldESign.equals(eSign)) {
%>
  <jsp:include page="incorrectesign.jsp" flush="true"/>
<%
	}
else
	{

		String studyId ="";
		
    	studyId = request.getParameter("studyId");
	 	
	 	if (StringUtil.isEmpty(studyId))
	 	{
	 		studyId="";
	 	}
     
     
	String ipAdd = (String) tSession.getValue("ipAdd");
	String usr = (String) tSession.getValue("userId");
	String formQueryId = "";
	String fieldName = request.getParameter("fieldName");
	fieldName = StringUtil.decodeString(fieldName);

	int filledFormId = EJBUtil.stringToNum(request.getParameter("filledFormId"));
	int formId = EJBUtil.stringToNum(request.getParameter("formId"));

	int from =  EJBUtil.stringToNum(request.getParameter("from"));
	String thread = request.getParameter("thread");
	int fieldId = 0;
	String queryType = "";
	String queryStatusId = "";
	type = request.getParameter("type");
	String strqueryRespId = request.getParameter("cmbQueryResp");
	int queryRespId = EJBUtil.stringToNum(strqueryRespId);

	String strstatusId = request.getParameter("cmbStatus");
	int statusId = EJBUtil.stringToNum(strstatusId);

	String strenteredBy = request.getParameter("enteredById");

	int enteredBy = EJBUtil.stringToNum(strenteredBy);
	String enteredOn =  DateUtil.getCurrentDateTime();
	String instructions = request.getParameter("instructions");

   	if(thread.equals("new"))
    	{
    	fieldId = EJBUtil.stringToNum(request.getParameter("field"));

		int ret = formQueryB.insertQueryResponse(filledFormId , from , fieldId,
		  									   2 , queryRespId ,
		  									   statusId , instructions,
											   enteredOn ,enteredBy,
											    EJBUtil.stringToNum(usr) , ipAdd );



    	} else
    	{


			formQueryId = request.getParameter("formQueryId");

         	queryType = request.getParameter("queryType");
			if(mode.equals("M"))
			{
			queryStatusId = request.getParameter("queryStatusId");
			formQueryStatusB.setFormQueryStatusId(EJBUtil.stringToNum(queryStatusId));
			formQueryStatusB.getFormQueryStatusDetails();
         	formQueryStatusB.setFormQueryType(queryType);
         	formQueryStatusB.setQueryTypeId(strqueryRespId);
         	formQueryStatusB.setQueryNotes(instructions);
         	formQueryStatusB.setEnteredBy(strenteredBy);
         	formQueryStatusB.setEnteredOn(enteredOn);
			formQueryStatusB.setQueryStatusId(strstatusId);
         	formQueryStatusB.setModifiedBy(usr);
			formQueryStatusB.setIpAdd(ipAdd);
         	iupret = formQueryStatusB.updateFormQueryStatus();
			}
			else{

         	formQueryStatusB.setFormQueryId(formQueryId);
         	formQueryStatusB.setFormQueryType(queryType);
         	formQueryStatusB.setQueryTypeId(strqueryRespId);
         	formQueryStatusB.setQueryNotes(instructions);
         	formQueryStatusB.setEnteredBy(strenteredBy);
         	formQueryStatusB.setEnteredOn(enteredOn);
			formQueryStatusB.setQueryStatusId(strstatusId);
         	formQueryStatusB.setCreator(usr);
			formQueryStatusB.setIpAdd(ipAdd);

         	formQueryStatusB.setFormQueryStatusDetails();
         	iret = formQueryStatusB.getFormQueryStatusId();
			}
    	}
%>

<%  	if ((iupret == -2) || (iret==0)) {%>
			<br><br><br><br><br><p class = "successfulmsg" align = center>
			<%=MC.M_Data_NotSvdSucc%><%--Data not saved successfully*****--%></p> <Br><Br>
			<button name="back" onclick="window.history.back();"><%=LC.L_Back%></button></p>
		<% return;
		} else {
	%>
			<br><br><br><br><br><p class = "successfulmsg" align = center>
			<%=MC.M_Data_SavedSucc%><%--Data saved successfully*****--%></p>
			<%if(!thread.equals("new")){%>
<META HTTP-EQUIV=Refresh CONTENT="1; URL=formQueryHistory.jsp?studyId=<%=studyId%>&formQueryId=<%=formQueryId%>&fieldName=<%=StringUtil.encodeString(fieldName)%>&formId=<%=formId%>&from=<%=from%>&filledFormId=<%=filledFormId%>">
	<%}else{%>
	<META HTTP-EQUIV=Refresh CONTENT="1; URL=addeditquery.jsp?studyId=<%=studyId%>&formId=<%=formId%>&from=<%=from%>&filledFormId=<%=filledFormId%>">

				<%}
		}

}//esign

}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>

</HTML>





