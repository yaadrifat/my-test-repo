<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><%=MC.M_Mstones_SavedRpts%><%--Milestones Saved Reports*****--%></title>

<script>

function confirmBox(repName,pgRight) {
	if (f_check_perm(pgRight,'E') == true) {
	var paramArray = [repName];
	msg=getLocalizedMessageString("M_Del_FrmSvdRpt",paramArray);/*msg="Delete " + repName + " from Saved Reports?";*****/

	if (confirm(msg))
	{
    	return true;
	}
	else
	{
		return false;
	}
	} else {
		return false;
	}
}

function setOrder(formObj,orderBy,pgRight) //orderBy column number
{
	var lorderType;
	if (formObj.orderType.value=="asc") {
		formObj.orderType.value= "desc";
		lorderType="desc";
	} else 	if (formObj.orderType.value=="desc") {
		formObj.orderType.value= "asc";
		lorderType="asc";
	}

	lsrc = formObj.srcmenu.value;
	lselectedTab = formObj.selectedTab.value;
	lstudy = formObj.studyId.value;
	lcurPage = formObj.page.value;
	formObj.action="savedrepbrowser.jsp?srcmenu="+lsrc+"&selectedTab="+lstudy+"&page="+lstudy+"&page="+lcurPage+"&orderBy="+orderBy+"&orderType="+lorderType;
	formObj.submit();
}

function openWin(savedRepId) {
	windowName = window.open("repmileretrieve.jsp?savedRepId="+savedRepId,"Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,dependant=true,width=600,height=500,top=100,left=200");
	windowName.focus();
}

</script>

<%
String src;
src= request.getParameter("srcmenu");

String selectedTab = request.getParameter("selectedTab");
String study = request.getParameter("studyId");
int studyId = EJBUtil.stringToNum(study);

%>
<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>

<body>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ page language = "java" import = "com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.BrowserRows,com.velos.eres.service.util.Configuration,java.sql.*,java.util.*"%>
<%@ page import="com.velos.eres.service.util.*"%>



<DIV class="tabDefTop" id="div1">
<table><tr><td>
<P class="sectionHeadings"><%=LC.L_Mstone_SvdRpt%><%--Milestones >> Saved Reports*****--%> </P>
</td></tr></table>
  <jsp:include page="milestonetabs.jsp" flush="true">
  	<jsp:param name="studyId" value="<%=study%>"/>
  </jsp:include>
</div>
<DIV class="tabDefBot" id="div2">

<%
String pagenum = "";
int curPage = 0;
long startPage = 1;
String stPage;
long cntr = 0;

pagenum = request.getParameter("page");
if (pagenum == null)
{
	pagenum = "1";
}
curPage = EJBUtil.stringToNum(pagenum);

String orderBy = "";
orderBy = request.getParameter("orderBy");


String orderType = "";
orderType = request.getParameter("orderType");

if (orderType == null)
{
	orderType = "asc";
}

//String searchCriteria = request.getParameter("searchCriteria");
//if (searchCriteria==null) {searchCriteria="";}

HttpSession tSession = request.getSession(true);

if (sessionmaint.isValidSession(tSession)) {
	int pageRight = 0;
    pageRight = EJBUtil.stringToNum((String) tSession.getAttribute("mileRight"));

	if (pageRight > 0 )	{

	   String savedRepSql = "SELECT PK_SAVEDREP,  FK_REPORT, SAVEDREP_NAME," +
		 "to_char(SAVEDREP_ASOFDATE,PKG_DATEUTIL.F_GET_DATEFORMAT || 'hh:mi') SAVEDREP_ASOFDATE, FK_USER, b.usr_firstname || ' ' || b.usr_lastname   as user_name, " +
		 "FK_PAT, c.per_code, a.SAVEDREP_FILTER " +
		 " FROM ER_SAVEDREP a, ER_USER b, er_per c " +
		 " WHERE FK_STUDY = " + studyId + "  AND b.PK_USER = a.FK_USER and c.pk_per (+)= a.fk_pat order by PK_SAVEDREP DESC";

	   String countSql = "select count(*) " +
		 " FROM ER_SAVEDREP a, ER_USER b, er_per c " +
		 " WHERE FK_STUDY = " + studyId + "  AND b.PK_USER = a.FK_USER and c.pk_per (+)= a.fk_pat";


	   long rowsPerPage=0;
   	   long totalPages=0;

		rowsPerPage =  Configuration.ROWSPERBROWSERPAGE ;
		totalPages =Configuration.PAGEPERBROWSER ;

	   long rowsReturned = 0;
	   long showPages = 0;
	   long totalRows = 0;
	   long firstRec = 0;
	   long lastRec = 0;

	   int savedRepId=0;
	   int  savedRepReportId = 0;
	   String savedRepRepName= null;
	   String savedRepAsOfDate = null;
	   String savedRepUserId= null;
       String savedRepPatId= null;
	   String savedRepUserName = null;
	   String  savedRepPerCode= null;
       String  savedRepFilter= null;

	   boolean hasMore = false;
	   boolean hasPrevious = false;

       BrowserRows br = new BrowserRows();

	   br.getPageRows(curPage,rowsPerPage,savedRepSql,totalPages,countSql,orderBy,orderType);
   	   rowsReturned = br.getRowReturned();
	   showPages = br.getShowPages();
	   startPage = br.getStartPage();
	   hasMore = br.getHasMore();
	   hasPrevious = br.getHasPrevious();
	   totalRows = br.getTotalRows();
	   firstRec = br.getFirstRec();
	   lastRec = br.getLastRec();

%>

<Form name="savedRepBrowser" method=post onsubmit="" action="savedrepbrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>&page=1">

	<table width=100%>
	<tr>
		<td width=50%>
          <P class = "defComments"><%=MC.M_ClickRptName_ToViewRpt%><%--Click on the report name to view the saved report*****--%>:</P>
		</td>
	</tr>
	</table>

	<Input type="hidden" name="srcmenu" value="<%=src%>">
	<Input type="hidden" name="selectedTab" value="<%=selectedTab%>">
	<Input type="hidden" name="studyId" value="<%=studyId%>">
	<Input type="hidden" name="page" value="<%=curPage%>">
	<Input type="hidden" name="orderBy" value="<%=orderBy%>">
	<Input type="hidden" name="orderType" value="<%=orderType%>">

    <table width="100%" >
      <tr>
  		<th width="30%" > <%=LC.L_Rpt_Name%><%--Report Name*****--%> </th>
        <th width="20%"> <%=LC.L_Rpt_RunBy%><%--Report Run By*****--%> </th>
        <th width="10%"> <%=LC.L_Rpt_RunOn%><%--Report Run On*****--%> </th>
        <th width="10%"> <%=LC.L_Patient%><%--<%=LC.Pat_Patient%>*****--%></th>
        <th width="20%"> <%=LC.L_Filter_Criteria%><%--Filter Criteria*****--%> </th>
        <th width="10%">&nbsp;</th>
      </tr>

      <%
    for(int counter = 1;counter<=rowsReturned;counter++)
	{
	    savedRepId=EJBUtil.stringToNum(br.getBValues(counter,"PK_SAVEDREP"));
		savedRepReportId = EJBUtil.stringToNum(br.getBValues(counter,"FK_REPORT"));
		savedRepRepName=((br.getBValues(counter,"SAVEDREP_NAME")==null)?"-":br.getBValues(counter,"SAVEDREP_NAME"));
		savedRepAsOfDate=((br.getBValues(counter,"SAVEDREP_ASOFDATE")==null)?"-":br.getBValues(counter,"SAVEDREP_ASOFDATE"));
		savedRepUserName=((br.getBValues(counter,"user_name")==null)?"-":br.getBValues(counter,"user_name"));
 		savedRepPerCode=((br.getBValues(counter,"per_code")==null)?"-":br.getBValues(counter,"per_code"));
 		savedRepFilter=((br.getBValues(counter,"SAVEDREP_FILTER")==null)?"-":br.getBValues(counter,"SAVEDREP_FILTER"));

		if ((counter%2)==0) {
  %>
      <tr class="browserEvenRow">
        <%
		}
		else{
  %>
      <tr class="browserOddRow">
        <%
		}
  %>
        <td> <A href="#" onclick="openWin(<%=savedRepId%>)"> <%=savedRepRepName%> </A> </td>
        <td> <%=savedRepUserName%> </td>
        <td> <%=savedRepAsOfDate%> </td>
        <td> <%=savedRepPerCode%> </td>
        <td> <%=savedRepFilter%> </td>
        <td> <A href="savedrepdelete.jsp?studyId=<%=studyId%>&selectedTab=<%=selectedTab%>&savedRepId=<%=savedRepId%>&srcmenu=<%=src%>" onClick="return confirmBox('<%=savedRepRepName%>',<%=pageRight%>);"><img src="./images/delete.gif" border="0" align="left"/></A></td>
      </tr>
      <%

		}
%>
    </table>

	<table>
	<tr>
	<td>
	<% if (totalRows > 0) { Object[] arguments = {firstRec,lastRec,totalRows}; %>
		<font class="recNumber"><%=VelosResourceBundle.getMessageString("M_ToOfRecords",arguments)%></font><%--<font class="recNumber"><%=firstRec%> to <%=lastRec%> of <%=totalRows%> Record(s)</font>*****--%>
	<%} else {%>
		<font class="recNumber"><%=MC.M_NoRecordsFound%><%--No Records Found*****--%></font>
	<%}%>
	</td>
	</tr>
	</table>

	<table align=center>
	<tr>
<%
	if (curPage==1) startPage=1;

    for (int count = 1; count <= showPages;count++)
	{
   cntr = (startPage - 1) + count;

	if ((count == 1) && (hasPrevious))
	{
    %>
	<td colspan = 2>
  	<A href="savedrepbrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>&page=<%=cntr-1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Previous%><%--Previous*****--%> <%=totalPages%> ></A>&nbsp;&nbsp;&nbsp;&nbsp;
	</td>
	<%
  	}
	%>
	<td>
	<%

	 if (curPage  == cntr)
	 {
     %>
		<FONT class = "pageNumber"><%= cntr %></Font>
       <%
       }
      else
        {
       %>

	   <A href="savedrepbrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>&page=<%=cntr%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>"><%= cntr%></A>
       <%
    	}
	 %>
	</td>
	<%
	  }
	if (hasMore)
	{
   %>
   <td colspan = 3 align = center>
  	&nbsp;&nbsp;&nbsp;&nbsp;<A href="savedrepbrowser.jsp?srcmenu=<%=src%>&selectedTab=<%=selectedTab%>&studyId=<%=studyId%>&page=<%=cntr+1%>&orderBy=<%=orderBy%>&orderType=<%=orderType%>">< <%=LC.L_Next%><%--Next*****--%> <%=totalPages%>></A>
	</td>
	<%
  	}
	%>
   </tr>
  </table>

  </Form>

	<%
	} //end of if body for page right
	else{
%>
  <jsp:include page="accessdenied.jsp" flush="true"/>
<%
	} //end of else body for page right
}//end of if body for session
else {
%>
	<jsp:include page="timeout.html" flush="true"/>
<%}
%>
<br>
<div>
    <jsp:include page="bottompanel.jsp" flush="true"/>
</div>
</div>

<DIV class="mainMenu" id = "emenu">
  <jsp:include page="getmenu.jsp" flush="true"/>
</DIV>

</body>
</html>
