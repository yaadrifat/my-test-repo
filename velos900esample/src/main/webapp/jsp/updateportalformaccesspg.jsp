<%@page import="com.aithent.audittrail.reports.AuditUtils"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></HEAD>
<SCRIPT LANGUAGE="JavaScript" src="whichcss.js"></SCRIPT>
</head>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>

<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="PFormsB"  scope="request" class="com.velos.esch.web.portalForms.PortalFormsJB"/>

<%@ page language = "java" import = "com.velos.eres.business.study.*,com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.esch.web.portalForms.PortalFormsJB"%>
<%
   

   
   String eSign = request.getParameter("eSign");
HttpSession tSession = request.getSession(true);  
if (sessionmaint.isValidSession(tSession))
   {	
%>
<jsp:include page="sessionlogging.jsp" flush="true"/> 

<%   
   	String oldESign = (String) tSession.getValue("eSign");



   String ipAdd = (String) tSession.getValue("ipAdd");
   String usr = (String) tSession.getValue("userId");
   
   String calendarId=request.getParameter("calId");
   String patPortalId=request.getParameter("portalId");
   
   String[] portalformKeys =request.getParameterValues("portalformKey");//pks
   
   String[] portalEvtIds=request.getParameterValues("hiddenEvtId");
   String[] portalFormIds=request.getParameterValues("hiddenFormId");
   
   String[] checkVals=request.getParameterValues("inclChks");
   
   String totalCount=request.getParameter("counter");
   



String checkVal = "";
String portalformKey = "";
String portalEvtId = "";
String portalFormId = "";

int deletedId = 0;


for (int j=0; j<EJBUtil.stringToNum(totalCount); j++){
	
	portalformKey=portalformKeys[j];
	portalEvtId=portalEvtIds[j];
	portalFormId=portalFormIds[j];
	checkVal=checkVals[j];
	
	
	
	if (portalformKey.equals("") && "Y".equals(checkVal)){
		PortalFormsJB pfJB = new PortalFormsJB();
		
		pfJB.setPortalId(patPortalId);
		pfJB.setCaledarId(calendarId);
		pfJB.setEventId(portalEvtId);
		pfJB.setFormId(portalFormId);
		pfJB.setCreator(usr);
		pfJB.setIpAdd(ipAdd);
		pfJB.setPortalForms();
		
	}else if (!portalformKey.equals("")){
		PortalFormsJB portFrmJB = new PortalFormsJB();
		
		portFrmJB.setPortalFormId(EJBUtil.stringToNum(portalformKey));
		portFrmJB.getPortalFormDetails();
		if ("Y".equals(checkVal)){
			portFrmJB.setFormId(portalFormId);
		}else{	
			portFrmJB.setFormId("0");//blank form id saved			
			// Modified for INF-18183 ::: Raviesh
			deletedId = portFrmJB.removePortalFormId(EJBUtil.stringToNum(portalformKey),AuditUtils.createArgs(session,"",LC.L_Portal_Admin));
		}
		portFrmJB.setModifiedBy(usr);
		portFrmJB.setIpAdd(ipAdd);
		portFrmJB.updatePortalForms();
		
	}
	
	
}

  
%>
<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=MC.M_Data_SvdSucc%><%--Data was saved successfully*****--%> </p>
<script>
	//window.opener.location.reload();
	setTimeout("self.close()",1000);
</script>	
<%
//}//end of if for eSign check
}//end of if body for session

else
{
%>
  <jsp:include page="timeout.html" flush="true"/>
  <%
}
%>

</BODY>
</HTML>
