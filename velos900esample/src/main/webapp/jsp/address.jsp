<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD></HEAD>
<BODY>
<%=LC.L_Save_Section2_Upper%><%--SAVE SECTION 2*****--%>

<jsp:useBean id="addressB" scope="request" class="com.velos.eres.web.address.AddressJB"/>

<%@ page language = "java" import = "java.util.*, java.lang.*, java.text.*,com.velos.eres.service.util.*"%>

<%  addressB.setAddType(LC.L_12345/*12345*****/); %>
 <% addressB.setAddPri(LC.L_Primary_Address/*Primary Address*****/); %>
 <% addressB.setAddCity(LC.L_Addr_City/*Address City*****/); %>
 <% addressB.setAddState(LC.L_Addr_State/*Address State*****/); %>
 <% addressB.setAddZip(LC.L_1234567890/*1234567890*****/); %>
 <% addressB.setAddCountry(LC.L_Addr_Country/*Address Country*****/); %>
 <% addressB.setAddCounty(LC.L_Address_County/*Address county*****/); %>
 <% addressB.setAddPhone(LC.L_Addr_Phone/*Address phone*****/); %>
 <% addressB. setAddEmail(LC.L_Addr_Email/*Address email*****/); %>
 <% addressB.setAddPager(LC.L_Addr_Pager/*Address pager*****/); %>
 <% addressB.setAddMobile(LC.L_Addr_Mobile/*Address mobile*****/); %>
 <% addressB.setAddFax(LC.L_Addr_Fax/*Address Fax*****/); %>
 
<%  
   addressB.setAddEffDate(DateUtil.dateToString(new Date()));

%>

<% addressB.setAddressDetails(); %>

<%=LC.L_Get_Section1_Upper%><%--GET SECTION 1*****--%>
<%=LC.L_Addr_Id%><%--address Id*****--%> :
<%out.print(addressB.getAddId());%>
<BR>
<%=LC.L_Addr_Type%><%--address Type*****--%> :
<%out.print(addressB.getAddType());%>
<BR>
<%=LC.L_Addr_Prim%><%--address Primary*****--%> :
<%out.print(addressB.getAddPri());%>
<BR>
<%=LC.L_Addr_City%><%--address City*****--%> :
<%out.print(addressB.getAddCity());%>
<BR>
<%=LC.L_Addr_State%><%--address State*****--%> :
<%out.print(addressB.getAddState());%>
<BR>
<%=LC.L_Addr_ZipPostal_Code%><%--address Zip/Postal code*****--%> :
<%out.print(addressB.getAddZip());%>
<BR>
<%=LC.L_Addr_Country%><%--address Country*****--%> :
<%out.print(addressB.getAddCountry());%>
<BR>
<%=LC.L_Addr_Country%><%--address Country*****--%> :
<%out.print(addressB.getAddCounty());%>
<BR>
<%=LC.L_Addr_EffectiveDate%><%--address Effective date*****--%>:
<%out.print(addressB.getAddEffDate());%>
<BR>
<%=LC.L_Addr_Phone%><%--address Phone*****--%> :
<%out.print(addressB.getAddPhone());%>
<BR>
<%=LC.L_Addr_Email%><%--address Email*****--%>:
<%out.print(addressB.getAddEmail());%>
<BR>
<%=LC.L_Addr_Pager%><%--address Pager*****--%>:
<%out.print(addressB.getAddPager());%>
<BR>
<%=LC.L_Addr_Mobile%><%--address Mobile*****--%>:
<%out.print(addressB.getAddMobile());%>
<BR>
<%=LC.L_Addr_Fax%><%--address Fax*****--%>:
<%out.print(addressB.getAddFax());%>
<BR>
<%=LC.L_Get_Section1_Upper%><%--GOT SECTION 1*****--%>

</BODY>
</HTML>
