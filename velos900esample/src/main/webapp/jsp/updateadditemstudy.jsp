<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel=STYLESHEET HREF="common.css" type=text/css>
</HEAD>
<jsp:include page="skinChoser.jsp" flush="true"/>
<BODY>
	<jsp:useBean id="lineitemB" scope="session" class="com.velos.esch.web.lineitem.LineitemJB"/>
	<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
  <%@ page language = "java" import = "com.velos.eres.business.common.*,java.util.*,com.velos.eres.service.util.EJBUtil,com.velos.eres.service.util.*"%>
  <%
  
String src = null;
src = request.getParameter("src");	
String eSign = request.getParameter("eSign");
String mode = request.getParameter("mode");
String lineitemMode = request.getParameter("lineitemMode");		

HttpSession tSession = request.getSession(true); 

	if(sessionmaint.isValidSession(tSession))

   {
%>
<jsp:include page="sessionlogging.jsp" flush="true"/>
<%   
  	 	String oldESign = (String) tSession.getValue("eSign");
		  if(!oldESign.equals(eSign)) {
    		%>
       	 	 <jsp:include page="incorrectesign.jsp" flush="true"/>  	
       		<%
       		} else {
			String ipAdd = (String) tSession.getValue("ipAdd");
			String usr = (String) tSession.getValue("userId");  
			String selectedTab = request.getParameter("selectedTab");
			String bgtcalId = request.getParameter("bgtcalId");	
			bgtcalId=(bgtcalId==null)?"":bgtcalId;
			String budgetName=request.getParameter("budgetName"); 
			String bgtSectionId = request.getParameter("bgtSectionId"); 
			String budgetId = request.getParameter("budgetId"); 
			String type[] = request.getParameterValues("type");
			String description[] = request.getParameterValues("description");
			String category[] = request.getParameterValues("cmbCtgry");
			//Rohit CCF-FIN21
			String tmid[] = null;
			String cdm[] = null;

			ArrayList lineitemDescArrayList = new ArrayList();
			ArrayList lineitemNameArrayList = new ArrayList();
			ArrayList lineitemCPTArrayList = new ArrayList();
			ArrayList categories = new ArrayList();
			ArrayList tmids = new ArrayList();
			ArrayList cdms = new ArrayList();
			String creator="";	
			String msg="";
			int i=0;
			int count = 4;
			int output=0;
			String lineitemId = "";
			int total=5;
		String[] strArrlineitemDesc =  new String[total];
  		String[] strArrlineitemName = new String[total];
		String[] strArrlineitemCpt = new String[total];
  		String[] strArrlineitemRepeat = new String[total];
			
		if(lineitemMode.equals("M")) {
			lineitemId = request.getParameter("lineitemId");
			count = 0;

			}
	
			for(i=0;i<=count;i++) {
		   		if ((type[i] != null && !(type[i].equals("")))||(description[i] != null && !(description[i].equals(""))))
		  		{
				if(lineitemMode.equals("M")){
				String itemType = type[i];
				String itemDescription = description[i];
				String ctgry = category[i];
				//Rohit CCF-FIN21
				String itemtmid = null;
				String itemcdm = null;
				lineitemB.setLineitemId(EJBUtil.stringToNum(lineitemId));
				lineitemB.getLineitemDetails();
				lineitemB.setLineitemName(itemType);
				lineitemB.setLineitemDesc(itemDescription);
				lineitemB.setLineitemCategory(ctgry);
				lineitemB.setLineitemTMID(itemtmid);
				lineitemB.setLineitemCDM(itemcdm);
				
							
				}else{
			  	 lineitemNameArrayList.add(type[i]);
			  	 lineitemDescArrayList.add(description[i]);
				 lineitemCPTArrayList.add("");
				 categories.add(category[i]) ;
				//Rohit CCF-FIN21
				 tmids.add(null) ;
				 cdms.add(null) ;

				 }
		  		}
	
			}
			
			if(lineitemMode.equals("M")){
			lineitemB.setModifiedBy(usr); //Amarnadh #3010
			output = lineitemB.updateLineitem();
			}else{
			//public int setAllLineitems(String[] lineitemNames, String[] lineitemDescs,String[] lineitemCpts, String[] lineitemRepeatOpts,
							//String bgtSectionId,String budgetCalId,String ipAdd, String usr) {
		  	//output = lineitemB.setAllLineitems(lineitemNameArrayList,lineitemDescArrayList, bgtSectionId, ipAdd, usr);
			//output = lineitemB.setRepeatLineitems(( String [] ) lineitemNameArrayList.toArray ( new String [ lineitemNameArrayList.size() ] )
			 //   ,( String [] ) lineitemDescArrayList.toArray ( new String [ lineitemDescArrayList.size() ] ),strArrlineitemCpt,strArrlineitemRepeat,bgtSectionId,bgtcalId,ipAdd, usr);
			output= lineitemB.insertAllLineItems(lineitemNameArrayList, lineitemDescArrayList,lineitemCPTArrayList,categories,tmids,cdms,bgtSectionId,ipAdd,usr,null);
			}
	
			if (output == 0) {
				msg = MC.M_Data_SavedSucc;/*msg = "Data saved successfully";*****/
				//tSession.setAttribute("lineItemDeleted", "Y");  
			}else {
				msg = MC.M_Data_DetsNotSvd;/*msg = "Data details not saved";*****/
			}
	%>

<META HTTP-EQUIV=Refresh CONTENT="1; URL=studybudget.jsp?srcmenu=<%=src%>&budgetId=<%=budgetId%>&bgtcalId=<%=bgtcalId%>&pageMode=final&mode=<%=mode%>&selectedTab=<%=selectedTab%>">

<br>
<br>
<br>
<br>
<br>
<p class = "successfulmsg" align = center> <%=msg%> </p>
<%		} // end of esign
	}//end of if body for session
else
{
%>
<jsp:include page="timeout.html" flush="true"/>
<%
}	
%>
</BODY>
</HTML>
