<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="localization.jsp" flush="true"/>
<html>
<head>


<title><%=LC.L_Std_StatDets%><%--<%=LC.Std_Study%> >> Status Details*****--%></title>

<%@ page import="java.util.*,java.io.*,org.w3c.dom.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<%@ page import="com.velos.eres.service.util.*,com.velos.eres.web.user.ConfigFacade,com.velos.eres.web.user.ConfigObject, com.velos.eres.web.user.ConfigDetailsObject,com.velos.eres.business.common.CommonDAO"%>
</head>

<SCRIPT language="JavaScript1.1">

function openwin1() {
     windowName=window.open("usersearchdetails.jsp?fname=&lname=&from=studyStatus","Information","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200")
	windowName.focus();
;}

function openUser() {
     windowName=window.open("usersearchdetails.jsp?fname=&lname=&genOpenerFormName=studyStatus&genOpenerUserIdFld=assignedToId&genOpenerUserNameFld=assignedTo","User","toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=800,height=400,left=100,top=200") ;
	 windowName.focus();
;}

</SCRIPT>

<%@ page language = "java" import = "com.velos.eres.service.util.*,com.velos.eres.business.common.*,com.velos.eres.web.user.*,com.velos.esch.business.common.EventAssocDao"%>


<jsp:useBean id="studyStatB" scope="request" class="com.velos.eres.web.studyStatus.StudyStatusJB"/>
<jsp:useBean id="studySiteB" scope="request" class="com.velos.eres.web.studySite.StudySiteJB"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="userB" scope="request" class="com.velos.eres.web.user.UserJB"/>


<% String src;
String from = "status";
src= request.getParameter("srcmenu");

String selectedTab;
selectedTab= request.getParameter("selectedTab");
CodeDao activeCloseCd = new CodeDao();
String activeCodeId = ""+activeCloseCd.getCodeId("studystat","active");
String prmntClsId = ""+activeCloseCd.getCodeId("studystat","prmnt_cls");

%>

<jsp:include page="panel.jsp" flush="true">
<jsp:param name="src" value="<%=src%>"/>
</jsp:include>


<jsp:include page="ui-include.jsp" flush="true"></jsp:include>
<%--Changes uncommented for Bug#: 9814 By:Yogendra Pratap --%>
<!-- Changes By Sudhir on 05-Apr-2012 for Bug #9181-->
<jsp:include page="include.jsp" flush="true"/>
<!-- Changes By Sudhir on 05-Apr-2012 for Bug #9181-->
<SCRIPT Language="javascript">


 function  validate(formobj) {


// if(formobj.appRenStatus.value == "") formobj.appRenStatus.value = null
// if(formobj.appRenNum.value == "") formobj.appRenNum.value = null



	if (document.getElementById('pgcustomorg')) {
	     if (!(validate_col('Organization',formobj.accsites))) return false
	}

	if (document.getElementById('mandstattype')) {
		if (!(validate_col('Status Type',formobj.studyStatusType))) return false
	}

    if (document.getElementById('mandstdstat')) {
		if (!(validate_col('Protocol Status',formobj.protocolStatus))) return false
	}

    if (document.getElementById('mandstatfrom')) {
	      if (!(validate_col('Status Valid From',formobj.startDate))) return false
	}

    if (document.getElementById('manddocby')) {
		if (!(validate_col('Documented By',formobj.documentedBy))) return false
	}



	// if (!(validate_col('Status Type',formobj.studyStatusType))) return false
    if (!(validateDataSize ( formobj.protocolNotes,4000,'Notes'))) return false


	 ////KM

	if (document.getElementById('pgcustomorg')) {
	   if (!(validate_col('Organization',formobj.accsites))) return false
	}

	if (document.getElementById('pgcustomstatustype')) {
	   if (!(validate_col('Status Type',formobj.studyStatusType))) return false
	}

	if (document.getElementById('pgcustomstdstatus')) {
	   if (!(validate_col('Protocol Status',formobj.protocolStatus))) return false
	}

	if (document.getElementById('pgcustomdocby')) {
	   if (!(validate_col('Documented By',formobj.documentedBy))) return false
	}

	if (document.getElementById('pgcustomassignedto')) {
	   if (!(validate_col('Assigned To',formobj.assignedTo))) return false
	}
	if (document.getElementById('pgcustomstatfrom')) {
	   if (!(validate_col('Status Valid From',formobj.startDate))) return false
	}

	if (document.getElementById('pgcustomstatuntil')) {
	   if (!(validate_col('Status Valid Until',formobj.validDate))) return false
	}

	if (document.getElementById('pgcustommeetdt')) {
	   if (!(validate_col('Meeting Date',formobj.meetingDate))) return false
	}
	if (document.getElementById('pgcustomreview')) {
	   if (!(validate_col('Review Board',formobj.revBoard))) return false
	}
	if (document.getElementById('pgcustomoutcome')) {
	   if (!(validate_col('Outcome',formobj.studyStatusOutcome))) return false
	}
	if (document.getElementById('pgcustomnotes')) {
	   if (!(validate_col('Notes',formobj.protocolNotes))) return false
	}


	 ////


	 //added for date field validation

      if (!(validate_date(formobj.startDate))) return false
      if (!(validate_date(formobj.validDate))) return false
      if (!(validate_date(formobj.meetingDate))) return false


      if (!(validate_col('e-Signature',formobj.eSign))) return false


      if(isNaN(formobj.eSign.value) == true) {

		alert("<%=MC.M_IncorrEsign_EtrAgain%>");/*alert("Incorrect e-Signature. Please enter again");*****/

		formobj.eSign.focus();

		return false;

     }

      //Added by Manimaran for September Enhancement S8.

      if (formobj.currentStat.checked) {
            formobj.currStat.value=1;
      } else {
	    formobj.currStat.value=0;
      }


	 //condition for date field validation by JM on 07Apr05

     if ((formobj.startDate.value != null && formobj.startDate.value != '' )&&(formobj.validDate.value != null && formobj.validDate.value !=''))
	{

		if (CompareDates(formobj.startDate.value,formobj.validDate.value, '>'))
		{
		  	alert ("<%=MC.M_StatValid_FromDt%>");/*alert ("Status Valid From Date can not be greater than Status Valid Until Date");*****/

			formobj.startDate.focus();
			return false;
		}

	}

// Added by Ganapathy on 05/11/2005


	var statusId=formobj.protocolStatus.options[formobj.protocolStatus.selectedIndex].value;

	// Added by Gopu on 05th May 2005 for fixing the bug # 2132
   if (formobj.startDate.value != null && formobj.startDate.value != '' ) {//valid from date

	   if (statusId==<%=prmntClsId%>){// IH: Bug 3812

	   if(formobj.studyStartDt.value != null && formobj.studyStartDt.value !='')
		{

			if (CompareDates(formobj.studyStartDt.value,formobj.startDate.value, '>'))
			{

				//alert ("Status 'Valid From Date' should be greater than 'Study Start Date'.");

				if (confirm("<%=MC.M_EtrStdEndDt_LessStdStrtDt%>"))/*if (confirm("You are entering a '<%=LC.Std_Study%> End Date' that is less than the '<%=LC.Std_Study%> Start Date', would you like to proceed?"))*****/
				{}
				else
				{
					formobj.startDate.focus();
					return false;
				}

				//formobj.startDate.focus();
				//return false;
			}

		}
}

	if (statusId==<%=activeCodeId%>){// IH: Bug 3812
		if (formobj.studyEndDt.value != null && formobj.studyEndDt.value !='')
		{
			if (CompareDates(formobj.startDate.value,formobj.studyEndDt.value, '>'))
			{

				//alert ("Status 'Valid From Date' should be less than 'Study End Date'.");
				if (confirm("<%=MC.M_EtrStdStrtDt_GrtStdEndDt%>"))/*if (confirm("You are entering a '<%=LC.Std_Study%> Start Date' that is greater than the '<%=LC.Std_Study%> End Date', would you like to proceed?"))*****/
				{}
				else
				{
					formobj.startDate.focus();
					return false;
				}
				//formobj.startDate.focus();
				//return false;
			}
		}
	}
  }//valid from date end




			i= formobj.protocolStatus.options.selectedIndex;
			st = formobj.protocolStatus.options[i].text;



//		 aProtNum = formobj.aProtocolCount.value;
//		 if (aProtNum < 1 && st == 'Active')
//		 {
//		 	if (! confirm("You have no Active Calendars associated with this Study. If you change the Protocol Status to Active, you will not be able to make any changes in 			Protocol Calendars. Do you want to continue?"))
//			{
//				return false;
	//		}
//			}else
			if (st == 'Active'){
		 	if (! confirm("<%=MC.M_NoMdf_AfterActv%>"))/*if (! confirm("You will not be able to make further modifications to this study protocol after activation. Are you sure you want to activate?"))*****/
			{
				return false;
			}
		}



  } //end of validate

	function callAjaxGetStatusDD(formobj){

	   selval = formobj.studyStatusType.value;
	   usrRole = formobj.role.value;
	   //alert(usrRole)
 	   new VELOS.ajaxObject("getCodeDropDown.jsp", {
   		urlData:"selectedVal="+selval + "&ddName=protocolStatus&codeType=studystat&ddType=child&role="+usrRole ,
		   reqType:"POST",
		   outputElement: "span_studystatus" }

   ).startRequest();

}

</SCRIPT>


<body >
<br>
<DIV class="BrowserTopn"  id="div1">
<jsp:include page="studytabs.jsp" flush="true">
<jsp:param name="from" value="<%=from%>"/>
</jsp:include>
</div>
<DIV class="BrowserBotN BrowserBotN_S_3" id="div2">


<%
   boolean hasAccess = false;
   HttpSession tSession = request.getSession(true);
   if (sessionmaint.isValidSession(tSession))
	{

	String studyId = (String) tSession.getValue("studyId");
	String userIdFromSession = (String) tSession.getValue("userId");
	UserJB user = (UserJB) tSession.getValue("currentUser");
	String userId = (String) tSession.getValue("userId");

	String defUserGroup = (String) tSession.getAttribute("defUserGroup");

%>

<%-- commented for study versioning enhancements
	//to check Active Calendars
	int len = 0;
	int aCount = 0;
	EventAssocDao eventAssoc = studyProt.getStudyProts(EJBUtil.stringToNum(studyId));
	ArrayList protocolIds = new ArrayList();
	ArrayList protStats = new ArrayList();
	ArrayList names = new ArrayList();
	protStats = eventAssoc.getStatus();
	len = protStats.size();

	for(int counter=0;counter<len;counter++)
	{
	   if(protStats.get(counter).toString().equals("A"))
	   {
		aCount++;
		}
	}
--%>

<%
	String uName = (String) tSession.getValue("userName");
	String studyNo = (String) tSession.getValue("studyNo");
	String acc = (String) tSession.getValue("accountId");

// Added by Gopu on 05th May 2005 for fixing the bug # 2132
	String studyStartDt=request.getParameter("studyStartDt");
	String studyEndDt=request.getParameter("studyEndDt");
	String currentStatId=request.getParameter("currentStatId");//KM
	int pageRight = 7;

	String disableStr ="";
	String readOnlyStr ="";



	ConfigFacade cFacade=ConfigFacade.getConfigFacade();
	HashMap hashPgCustFld = new HashMap();

	ConfigDetailsObject cdoPgField = cFacade.populateObject(Integer.parseInt(acc), "studystatus");

	if (cdoPgField.getPcfField()!= null && cdoPgField.getPcfField().size() > 0) {
		for (int i=0;i<cdoPgField.getPcfField().size();i++){
			hashPgCustFld.put((String)cdoPgField.getPcfField().get(i), String.valueOf(i));
	    }
	}





	if (pageRight > 0 )
	{
 		hasAccess = true;
  int studyStatId = 0;
  String mode = null;
  String startDate =  "";
  String validDate = "";
  //Added By Amarnadh
  String meetingDate = "";
  String mStudyStatusOutcome = "";
  String mStudyStatusType = "";
  //End added
  String appRenNum = "";
  String appRenStatus = "";
  String protocolStatus ="";
  String hspnNum = "";
  String documentedById = "";
  String documentedBy = "-";
  String protocolNotes = "";
  String dAppRenStatus = "";
  String dProtocolStatus = "" ;
  String studyStatusOutcome = "";
  String studyStatusType = "";
  String dAppRenNum = "" ;

  //KM
  String currentStat="";
  String siteId = user.getUserSiteId();
  StringBuffer documentedByBuf= new StringBuffer();
  mode = request.getParameter("mode");
  CodeDao cd = new CodeDao();
  cd.setCType("studystat");
  cd.setForGroup(defUserGroup);



  CodeDao cd1 = new CodeDao();
  CodeDao cd2 = new CodeDao();
  //added by Amarnadh
  CodeDao studyStatusOutcomeDao = new CodeDao();
  CodeDao studyStatusTypeDao = new CodeDao();
  CodeDao cdReviewBoard = new CodeDao();


  //JM: 16Apr2010, #SW-FIN4

		String roleCodePk="";

		if (! StringUtil.isEmpty(studyId) && EJBUtil.stringToNum(studyId) > 0)
		{
			ArrayList tId = new ArrayList();

			TeamDao teamDao = new TeamDao();
			teamDao.getTeamRights(EJBUtil.stringToNum(studyId),EJBUtil.stringToNum(userIdFromSession));
			tId = teamDao.getTeamIds();

			if (tId != null && tId.size() > 0)
			{
				ArrayList arRoles = new ArrayList();
				arRoles = teamDao.getTeamRoleIds();

				if (arRoles != null && arRoles.size() >0 )
				{
					roleCodePk = (String) arRoles.get(0);

					if (StringUtil.isEmpty(roleCodePk))
					{
						roleCodePk="";
					}
				}
				else
				{
					roleCodePk ="";
				}

			}
			else
			{
				roleCodePk ="";
			}
		} else
			{
			  roleCodePk ="";
			}


  //JM: 16Apr2010, #SW-FIN4
  //studyStatusTypeDao.getCodeValues("studystat_type");
  studyStatusTypeDao.getCodeValuesForStudyRole("studystat_type",roleCodePk);


  studyStatusTypeDao.setCType("studystat_type");
  studyStatusTypeDao.setForGroup(defUserGroup);


  cd1.getCodeValues("studyaprstat");
  cd2.getCodeValues("studyaprno");

  studyStatusOutcomeDao.getCodeValues("studystat_out");
  studyStatusOutcomeDao.setCType("studystat_out");
  studyStatusOutcomeDao.setForGroup(defUserGroup);



  cdReviewBoard.getCodeValues("rev_board");

   cdReviewBoard.setCType("rev_board");
  cdReviewBoard.setForGroup(defUserGroup);

  String  lastStatId = null;

	StringBuffer mainStr = new StringBuffer();
	ArrayList  cId = new ArrayList();
	ArrayList cDesc = new ArrayList();
	StringBuffer sb = new StringBuffer();
	String statDesc = "";
	String dAccSites = "";
	String assignedToId= "";
	String assignedTo = "";
	String revBoard = "";
	String ddRevBoard = "";

	Integer orgn;
	//String selOrg = request.getParameter("accsites") ;
	String selOrg = request.getParameter("selOrgId");

	int accId = EJBUtil.stringToNum(acc);

	if(selOrg == null || selOrg.equals(""))
		selOrg = "0";

	int selOrgId = EJBUtil.stringToNum(selOrg);


	String orgAtt = "";
	String statTypeAtt ="";
	String stdStatAtt ="";
	String reviewAtt = "";
    String outcomeAtt = "";



      if (hashPgCustFld.containsKey("organization")) {
			int fldNumOrg = Integer.parseInt((String)hashPgCustFld.get("organization"));
			orgAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOrg));
			if(orgAtt == null) orgAtt ="";
      }

	  if (hashPgCustFld.containsKey("statustype")) {
			int fldNumStat = Integer.parseInt((String)hashPgCustFld.get("statustype"));
			statTypeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStat));
			if(statTypeAtt == null) statTypeAtt ="";
      }

	   if (hashPgCustFld.containsKey("studystatus")) {
			int fldNumStdStat = Integer.parseInt((String)hashPgCustFld.get("studystatus"));
			stdStatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStdStat));
			if(stdStatAtt == null) stdStatAtt ="";
      }

	  if (hashPgCustFld.containsKey("reviewboard")) {
			int fldNumReview = Integer.parseInt((String)hashPgCustFld.get("reviewboard"));
			reviewAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumReview));
			if(reviewAtt == null) reviewAtt ="";
      }

	   if (hashPgCustFld.containsKey("outcome")) {
			int fldNumOutcome = Integer.parseInt((String)hashPgCustFld.get("outcome"));
			outcomeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOutcome));
			if(outcomeAtt == null) outcomeAtt ="";
      }


	 StudySiteDao ssDao = new StudySiteDao();

     //  ssDao	= studySiteB.getAddedAccessSitesForStudy(EJBUtil.stringToNum(studyId), EJBUtil.stringToNum(userIdFromSession),accId);
     // changed req

      /* Changed by Sonia Abrol, 06/28/05, Now we need same organizations as we get on the study team page
	 	 ssDao	= studySiteB.getSitesForStatAndEnrolledPat(EJBUtil.stringToNum(studyId), EJBUtil.stringToNum(userIdFromSession),accId);
	 */
	 ssDao	= studySiteB.getAllStudyTeamSites(EJBUtil.stringToNum(studyId), accId);

	 ArrayList arrSiteIds = ssDao.getSiteIds();

	 ArrayList arrSiteNames = ssDao.getSiteNames();

     sb.append("<SELECT NAME='accsites'") ;

	 if(mode.equals("M") || orgAtt.equals("1")  ){
	 sb.append(LC.L_Disabled_Lower);/*sb.append("disabled");*****/
	 }
	 sb.append(">");

		if (arrSiteIds.size() > 0)
		{
			for (int counter = 0; counter <= arrSiteNames.size() -1 ; counter++)
			{
				orgn = (Integer) arrSiteIds.get(counter);

				if(	orgn.intValue() == selOrgId)	{
				sb.append("<OPTION value = "+ orgn+" SELECTED>" + arrSiteNames.get(counter)+ "</OPTION>");
				}
				else
				{
				sb.append("<OPTION value = "+ orgn+">" + arrSiteNames.get(counter)+ "</OPTION>");
				}
			}
		}
		sb.append("</SELECT>");
		dAccSites = sb.toString();

  if (mode.equals("M")) {
  		studyStatId = EJBUtil.stringToNum(request.getParameter("studyStatusId"));
		studyStatB.setId(studyStatId);
		studyStatB.getStudyStatusDetails();
		startDate = studyStatB.getStatStartDate();
		validDate = studyStatB.getStatValidDate();
		//Added By Amarnadh
		meetingDate = studyStatB.getStatMeetingDate();
		mStudyStatusOutcome = studyStatB.getStudyStatusOutcome();
		mStudyStatusType = studyStatB.getStudyStatusType();
		//End added
		siteId = studyStatB.getSiteId();
		currentStat =studyStatB.getCurrentStat();
		appRenNum = studyStatB.getStatAppRenNo();
		appRenStatus = studyStatB.getStatApproval();
		protocolStatus = studyStatB.getStudyStatus();
		hspnNum = studyStatB.getStatHSPN();
		documentedById = studyStatB.getStatDocUser();
		userB.setUserId(EJBUtil.stringToNum(documentedById));
		userB.getUserDetails();
		//documentedBy =userB.getUserFirstName()+ " " + userB.getUserLastName();

		documentedBy = ((userB.getUserFirstName() == null)?"":userB.getUserFirstName()) + " " + ((userB.getUserLastName() == null) ? "" : userB.getUserLastName());


		protocolNotes = studyStatB. getStatNotes();
		if(protocolNotes == null)  protocolNotes = "";
		if(protocolNotes.length() ==0)  protocolNotes = "";

		//in new mode get the first value of studystat_type and retrieve its children "studystat", in modify mode, use the saved status type

  		cd.getCodeValuesForCustom1("studystat",mStudyStatusType);

   	    dAppRenStatus = cd1.toPullDown("appRenStatus", EJBUtil.stringToNum(appRenStatus));
 //     dProtocolStatus = cd.toPullDown("protocolStatus", EJBUtil.stringToNum(protocolStatus));
        dProtocolStatus = cd.toPullDown("protocolStatus", EJBUtil.stringToNum(protocolStatus),"disabled");
		//Added by Amarnadh

		//studyStatusOutcome = studyStatusOutcomeDao.toPullDown("studyStatusOutcome",EJBUtil.stringToNum(mStudyStatusOutcome));

		if (outcomeAtt.equals("1"))
			studyStatusOutcome = studyStatusOutcomeDao.toPullDown("studyStatusOutcome",EJBUtil.stringToNum(mStudyStatusOutcome),"disabled");
		else
   		    studyStatusOutcome = studyStatusOutcomeDao.toPullDown("studyStatusOutcome",EJBUtil.stringToNum(mStudyStatusOutcome));

		//Modify mode it should show status types...
		studyStatusTypeDao.getCodeValues("studystat_type");

		studyStatusType = studyStatusTypeDao.toPullDown("studyStatusType",EJBUtil.stringToNum(mStudyStatusType) ,"disabled");

       	dAppRenNum = cd2.toPullDown("appRenNum", EJBUtil.stringToNum(appRenNum));

       	assignedToId = 	studyStatB.getStudyStatusAssignedTo();
       	if (StringUtil.isEmpty(assignedToId))
       	{
       		assignedToId = "";
       	}
       	if (! StringUtil.isEmpty(assignedToId))
       	{
       		userB.setUserId(EJBUtil.stringToNum(assignedToId));
			userB.getUserDetails();
			assignedTo =userB.getUserFirstName()+ " " + userB.getUserLastName();
		}

		revBoard = studyStatB.getStudyStatusReviewBoard();
		if (revBoard == null ) revBoard = ""; //KM-3695
		//ddRevBoard = cdReviewBoard.toPullDown("revBoard", EJBUtil.stringToNum(revBoard));

		if (reviewAtt.equals("1"))
			ddRevBoard = cdReviewBoard.toPullDown("revBoard", EJBUtil.stringToNum(revBoard),"disabled");
		else
		    ddRevBoard = cdReviewBoard.toPullDown("revBoard", EJBUtil.stringToNum(revBoard));
   }
	else
	{
	  	lastStatId = request.getParameter("lastStatId");

		dAppRenStatus = cd1.toPullDown("appRenStatus");


		//in new mode get the first value of studystat_type and retrieve its children "studystat"

		ArrayList arStudyStatTypeTemp = new ArrayList();

		arStudyStatTypeTemp =  studyStatusTypeDao.getCId();

		if (arStudyStatTypeTemp!= null && arStudyStatTypeTemp.size() > 0)
		{
			mStudyStatusType = String.valueOf( (Integer)arStudyStatTypeTemp.get(0));

			//JM: 04May2010, #SW-FIN4
			cd.getCodeValuesForStudyRoleAndCustom1("studystat",mStudyStatusType, roleCodePk);
		}
		else
		{
			cd.getCodeValues("studystat");
		}


		//to add a new status if study is already active, show only deactivate as option

		//dProtocolStatus = cd.toPullDown("protocolStatus");
		if (stdStatAtt.equals("1"))
				dProtocolStatus = cd.toPullDown("protocolStatus","disabled");
		else
			    dProtocolStatus = cd.toPullDown("protocolStatus");


		//Added By Amarnadh
		//studyStatusOutcome = studyStatusOutcomeDao.toPullDown("studyStatusOutcome");

		if (outcomeAtt.equals("1"))
			studyStatusOutcome = studyStatusOutcomeDao.toPullDown("studyStatusOutcome","disabled");
		else
			studyStatusOutcome = studyStatusOutcomeDao.toPullDown("studyStatusOutcome");

		//studyStatusType = studyStatusTypeDao.toPullDown("studyStatusType",EJBUtil.stringToNum(mStudyStatusType)," onChange=callAjaxGetStatusDD(document.studyStatus);");

		if (statTypeAtt.equals("1"))
  		     {System.out.println("First");
			studyStatusType = studyStatusTypeDao.toPullDown("studyStatusType",EJBUtil.stringToNum(mStudyStatusType)," onChange=callAjaxGetStatusDD(document.studyStatus); disabled");
  		     }
		else
			{
			System.out.println("Second");
			studyStatusType = studyStatusTypeDao.toPullDown("studyStatusType",EJBUtil.stringToNum(mStudyStatusType)," onChange=callAjaxGetStatusDD(document.studyStatus);");
			}



		dAppRenNum = cd2.toPullDown("appRenNum");
		documentedById = (String) tSession.getValue("userId");
		documentedBy = 	(String) tSession.getValue("userName");


		//Commented by Manimaran to  fix the issue  3304
		//assignedToId = (String) tSession.getValue("userId");
		//assignedTo = 	(String) tSession.getValue("userName");

		//ddRevBoard = cdReviewBoard.toPullDown("revBoard");
		if (reviewAtt.equals("1"))
			ddRevBoard = cdReviewBoard.toPullDown("revBoard","disabled");
		else
			ddRevBoard = cdReviewBoard.toPullDown("revBoard");

	}

%>
<!-- Added by Gopu on 05th May 2005 for fixing the bug # 2132  -->
	<tr><P>
        <td><%=LC.L_Std_StartDate%><%--<%=LC.Std_Study%> Start Date*****--%> :<%=studyStartDt%></td>		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<td> <%=LC.L_Std_EndDate%><%--<%=LC.Std_Study%> End Date*****--%> : <%=studyEndDt%></td>
		</P>
    </tr>

<Form name="studyStatus" id="studystatForm" method="post" action="updatestudystatus.jsp" onSubmit ="if (validate(document.studyStatus)==false) {setValidateFlag('false');return false;} else {setValidateFlag('true'); return true;}">
<P class = "defComments">
<%=MC.M_Etr_StatusDets%><%--Please enter status details*****--%>:
</P>
<input type="hidden" name="studyStatId" Value="<%=studyStatId%>">
<input type="hidden" name="srcmenu" Value="<%=src%>">
<Input type="hidden" name="selectedTab" value="<%=selectedTab%>">
<Input type="hidden" name="mode" value=<%=mode%>>
<Input type="hidden" name="hdnProtocolStatus" value=<%=protocolStatus%>>
<Input type="hidden" name="hdnAccsitess" value=<%=selOrgId%>>
<Input type="hidden" name="selOrgId" value=<%=selOrgId%>>
<!-- Added by Gopu on 05th May 2005 for fixing the bug # 2132  -->
<Input type="hidden" name="studyStartDt" value=<%=studyStartDt%>>
<input type="hidden" name="studyEndDt" value=<%=studyEndDt%>>
<!-- Added by Manimaran for September Enhancement(S8) -->
<input type="hidden" name="currStat" value="">
<input type="hidden" name="currentStatId" value=<%=currentStatId%>>
<input type="hidden" name="role" value=<%=roleCodePk%>>

<%
 if (mode.equals("N")) {

 %>
	 <Input type="hidden" name="lastStatId" value=<%=lastStatId%>>
<%
	}
%>
<table width="98%">


 <tr height = 20>
	 <%if (hashPgCustFld.containsKey("organization")) {
			int fldNumOrg= Integer.parseInt((String)hashPgCustFld.get("organization"));
			String orgMand = ((String)cdoPgField.getPcfMandatory().get(fldNumOrg));
			String orgLable = ((String)cdoPgField.getPcfLabel().get(fldNumOrg));
			orgAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOrg));


			if(orgAtt == null) orgAtt ="";
			if(orgMand == null) orgMand ="";


			if(!orgAtt.equals("0")) {
			if(orgLable !=null){
			%> <td width="20%">
			<%=orgLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Organization%><%--Organization*****--%>
			<%}

			if (orgMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomorg">* </FONT>
			<% } %>

    </td>
    <td>

	<%=dAccSites%>   <%if(orgAtt.equals("1")) {%><input type="hidden" name="accsites" value=""> <%}%>
    </td>
	<%} else if(orgAtt.equals("0")) {%>

	<input type="hidden" name="accsites" value="">
	<%}} else {%>

	 <td width="20%"><%=LC.L_Organization%><%--Organization*****--%> <FONT class="Mandatory" id="mandorg">* </FONT></td>
        <td><%=dAccSites%></td>

    <%}%>

</tr>

	  <tr> <td height=1></td></tr>


	 <tr>
	 <%if (hashPgCustFld.containsKey("statustype")) {
			int fldNumType = Integer.parseInt((String)hashPgCustFld.get("statustype"));
			String statTypeMand = ((String)cdoPgField.getPcfMandatory().get(fldNumType));
			String statTypeLable = ((String)cdoPgField.getPcfLabel().get(fldNumType));
			statTypeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumType));

			if(statTypeAtt == null) statTypeAtt ="";
			if(statTypeMand == null) statTypeMand ="";


			if(!statTypeAtt.equals("0")) {
			if(statTypeLable !=null){
			%> <td width="20%">
			<%=statTypeLable%>
			<%} else {%> <td width="20%">
			<%=LC.L_Status_Type%><%--Status Type*****--%> 
			<%}

			if (statTypeMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstatustype">* </FONT>
			<% } %>

    </td>
    <td>

	 <%=studyStatusType %>  <%if(statTypeAtt.equals("1")) {%><input type="hidden" name="studyStatusType" value=""> <%}%>
    </td>
	<%} else if(statTypeAtt.equals("0")) {%>

	<input type="hidden" name="studyStatusType" value="">
	<%}} else {%>

	 <td> <%=LC.L_Status_Type%><%--Status Type*****--%> <FONT class="Mandatory" id="mandstattype">* </FONT></td>
	 <td> <%=studyStatusType %>	</td>
    <%}%>
</tr>

	<tr><td height=1></td></tr>


	 <tr>
	 <%if (hashPgCustFld.containsKey("studystatus")) {
			int fldNumStdStat = Integer.parseInt((String)hashPgCustFld.get("studystatus"));
			String stdStatMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStdStat));
			String stdStatLable = ((String)cdoPgField.getPcfLabel().get(fldNumStdStat));
			stdStatAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStdStat));

			if(stdStatAtt == null) stdStatAtt ="";
			if(stdStatMand == null) stdStatMand ="";

			if(!stdStatAtt.equals("0")) {
			if(stdStatLable !=null){
			%> <td>
			<%=stdStatLable%>
			<%} else {%> <td>
			 <%=LC.L_Study_Status%><%--<%=LC.Std_Study%> Status*****--%> 
			<%}

			if (stdStatMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstdstatus">* </FONT>
			<% } %>

    </td>
    <td>  <span id="span_studystatus">  <%=dProtocolStatus%>

	<%//if(stdStatAtt.equals("1")) {%><!--input type="hidden" name="protocolStatus" value=""--> <%//}%>
    </span> </td>
	<%} else if(stdStatAtt.equals("0")) {%>


	<div id ="dd" Style = "visibility:hidden" > <%=dProtocolStatus%> </div>



	<%}} else {%>

	 <td > <%=LC.L_Study_Status%><%--<%=LC.Std_Study%> Status*****--%> <FONT class="Mandatory" id="mandstdstat">* </FONT>    </td>
    <td>
    <span id="span_studystatus">
	<%=dProtocolStatus%>
	</span>
    </td>
    <%}%>
   </tr>

 <tr><td height=1></td></tr>

			<tr>
				<%

				if (hashPgCustFld.containsKey("documentedby")) {

				int fldNumDocBy = Integer.parseInt((String)hashPgCustFld.get("documentedby"));
				String docByMand = ((String)cdoPgField.getPcfMandatory().get(fldNumDocBy));
				String docByLable = ((String)cdoPgField.getPcfLabel().get(fldNumDocBy));
				String docByAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumDocBy));

				disableStr ="";
				readOnlyStr ="";

				if(docByAtt == null) docByAtt ="";
  				if(docByMand == null) docByMand ="";

				if(!docByAtt.equals("0")) {

				if(docByLable !=null){
				%>
				<td>
				 <%=docByLable%>
				<%} else {%> <td>
				 <%=LC.L_Documented_By%><%--Documented By*****--%>
				<% }


			   if (docByMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomdocby">* </FONT>
		 	   <% }
			   %>

			   <%if(docByAtt.equals("1")) {
			       disableStr = "disabled"; }
  		       %>
 			  </td>
			  <td>

			  <% if(!docByAtt.equals("1")) { %>
			  <input type=hidden name='documentedById' value='<%=documentedById%>'>
			  <%}%>

			  <input type=text name='documentedBy' value="<%=documentedBy%>" size='30' readonly <%=disableStr%>>

			  <%if(!docByAtt.equals("1") && !docByAtt.equals("2")) { %>
			 <A HREF=# onClick=openwin1() ><%=LC.L_Select_User%><%--Select User*****--%></A>
			  <%}%>

			  </td>

				<%} else { %>

			  <input type=hidden name='documentedById' value='<%=documentedById%>'>
			  <input type=hidden name='documentedBy' value="<%=documentedBy%>" size='30' readonly <%=disableStr%>>

				<%}}  else {%>

				    <td >
					   <%=LC.L_Documented_By%><%--Documented By*****--%> <FONT class="Mandatory" id="manddocby">* </FONT>
					</td>
					<td>
					<input type=hidden name='documentedById' value='<%=documentedById%>'>
					<input type=text name='documentedBy' value="<%=documentedBy%>" size='30' readonly>
					<A HREF=# onClick=openwin1() ><%=LC.L_Select_User%><%--Select User*****--%></A>
					</td>

			 <%}%>
			</tr>



  <tr><td height=1></td></tr>



		<tr>
		<%

				if (hashPgCustFld.containsKey("assignedto")) {

				int fldNumAssgnTo = Integer.parseInt((String)hashPgCustFld.get("assignedto"));
				String assgnToMand = ((String)cdoPgField.getPcfMandatory().get(fldNumAssgnTo));
				String assgnToLable = ((String)cdoPgField.getPcfLabel().get(fldNumAssgnTo));
				String assgnToAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumAssgnTo));

				disableStr ="";
				readOnlyStr ="";

				if(assgnToAtt == null) assgnToAtt ="";
  				if(assgnToMand == null) assgnToMand ="";

				if(!assgnToAtt.equals("0")) {

				if(assgnToLable !=null){
				%>
				<td>
				 <%=assgnToLable%>
				<%} else {%> <td>
				 <%=LC.L_Assigned_To%><%--Assigned To*****--%>
				<% }


			   if (assgnToMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomassignedto">* </FONT>
		 	   <% }
			   %>

			   <%if(assgnToAtt.equals("1")) {
			       disableStr = "disabled"; }
  		       %>
 			   </td>
			   <td>
			   <% if(!assgnToAtt.equals("1")) { %>
			   <input type=hidden name='assignedToId' value='<%=assignedToId%>'>
			   <%}%>
			   <input type=text name='assignedTo' value="<%=assignedTo%>" size='30' readonly <%=disableStr%> >

			  <%if(!assgnToAtt.equals("1") && !assgnToAtt.equals("2")) { %>
			   <A HREF=# onClick=openUser() ><%=LC.L_Select_User%><%--Select User*****--%></A>
			  <%}%>

			  </td>

				<%} else { %>

			   <input type=hidden name='assignedToId' value='<%=assignedToId%>'>
			   <input type=hidden name='assignedTo' value="<%=assignedTo%>" size='30' readonly <%=disableStr%> >

				<% }}  else {%>

				<td >
				   <%=LC.L_Assigned_To%><%--Assigned To*****--%>
				</td>
				<td>
				<input type=hidden name='assignedToId' value='<%=assignedToId%>'>
				<input type=text name='assignedTo' value="<%=assignedTo%>" size='30' readonly>
				<A HREF=# onClick=openUser() ><%=LC.L_Select_User%><%--Select User*****--%></A>
				</td>

			 <%}%>
			</tr>



		<tr><td height=1></td></tr>




		<tr>
		<%

				if (hashPgCustFld.containsKey("statvalidfrom")) {

				int fldNumStatFrom = Integer.parseInt((String)hashPgCustFld.get("statvalidfrom"));
				String statFromMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStatFrom));
				String statFromLable = ((String)cdoPgField.getPcfLabel().get(fldNumStatFrom));
				String statFromAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStatFrom));

				disableStr ="";
				readOnlyStr ="";

				if(statFromAtt == null) statFromAtt ="";
  				if(statFromMand == null) statFromMand ="";

				if(!statFromAtt.equals("0")) {

				if(statFromLable !=null){
				%>
				<td>
				 <%=statFromLable%>
				<%} else {%> <td>
				 <%=LC.L_Status_ValidFrom%><%--Status Valid From*****--%>
				<% }


			   if (statFromMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstatfrom">* </FONT>
		 	   <% }
			   %>

			   <%if(statFromAtt.equals("1")) {
			       disableStr = "disabled"; }
				 else if (statFromAtt.equals("2") ) {
				   readOnlyStr = "readonly"; }
  		       %>
 			   </td>
			   <td>
<%-- INF-20084 Datepicker-- AGodara --%>
			  <%if(!statFromAtt.equals("1")) { %>
			   		<input type="text" name="startDate" class="datefield" size ="15" MAXLENGTH ="20"  <%=readOnlyStr%> <%=disableStr%> value='<%=startDate%>' >
			  <%}%>

			  </td>
			 <%}else{%>
			 		<input type="hidden" name="startDate" size ="15" MAXLENGTH ="20" value='<%=startDate%>' <%=readOnlyStr%> <%=disableStr%> >
			<%}}else {%>
					<td> <%=LC.L_Status_ValidFrom%><%--Status Valid From*****--%> <FONT class="Mandatory" id="mandstatfrom">* </FONT>  </td>
					<td><input type="text" name="startDate" class="datefield" size ="15" MAXLENGTH ="20" value='<%=startDate%>' ></td>
		    </tr>

			<%}%>
			</tr>


<tr><td height=1></td></tr>

<tr>
		<%

				if (hashPgCustFld.containsKey("statvaliduntil")) {

				int fldNumStatUntil = Integer.parseInt((String)hashPgCustFld.get("statvaliduntil"));
				String statUntilMand = ((String)cdoPgField.getPcfMandatory().get(fldNumStatUntil));
				String statUntilLable = ((String)cdoPgField.getPcfLabel().get(fldNumStatUntil));
				String statUntilAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumStatUntil));

				disableStr ="";
				readOnlyStr ="";

				if(statUntilAtt == null) statUntilAtt ="";
  				if(statUntilMand == null) statUntilMand ="";

				if(!statUntilAtt.equals("0")) {

				if(statUntilLable !=null){
				%>
				<td>
				 <%=statUntilLable%>
				<%} else {%> <td>
				  <%=LC.L_Status_ValidUntil%><%--Status Valid Until*****--%>
				<% }


			   if (statUntilMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomstatuntil">* </FONT>
		 	   <% }
			   %>

			   <%if(statUntilAtt.equals("1")) {
			       disableStr = "disabled"; }
				 else if (statUntilAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>


 			   </td>
			   <td>
<%-- INF-20084 Datepicker-- AGodara --%>
		<%if(!statUntilAtt.equals("1")) { %>
			<input type="text" name="validDate" class="datefield" readonly size="15" MAXLENGTH="20" value='<%=validDate%>'  <%=readOnlyStr%> <%=disableStr%> >
	  	<%}%>
			</td>
		<%}else{%>
			<input type="hidden" name="validDate" size="15" MAXLENGTH="20" value='<%=validDate%>'  <%=readOnlyStr%> <%=disableStr%> >
		<%}}else{%>
			<td>
			    <%=LC.L_Status_ValidUntil%><%--Status Valid Until*****--%>
			</td>
			<td><input type="text" name="validDate" class="datefield"  size="15" MAXLENGTH="20" value='<%=validDate%>'></td>
		<%}%>
		</tr>
		<tr><td height=1></td></tr>



	<tr>
		<%

				if (hashPgCustFld.containsKey("meetingdate")) {

				int fldNumMeetDt = Integer.parseInt((String)hashPgCustFld.get("meetingdate"));
				String meetingDtMand = ((String)cdoPgField.getPcfMandatory().get(fldNumMeetDt));
				String meetingDtLable = ((String)cdoPgField.getPcfLabel().get(fldNumMeetDt));
				String meetingDtAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumMeetDt));

				disableStr ="";
				readOnlyStr ="";

				if(meetingDtAtt == null) meetingDtAtt ="";
  				if(meetingDtMand == null) meetingDtMand ="";

				if(!meetingDtAtt.equals("0")) {

				if(meetingDtLable !=null){
				%>
				<td>
				 <%=meetingDtLable%>
				<%} else {%> <td>
				  <%=LC.L_Meeting_Date%><%--Meeting Date*****--%>
				<% }


			   if (meetingDtMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustommeetdt">* </FONT>
		 	   <% }
			   %>

			   <%if(meetingDtAtt.equals("1")) {
			       disableStr = "disabled"; }
				 else if (meetingDtAtt.equals("2") ) {
					 readOnlyStr = "readonly"; }

  		       %>


 			   </td>
			   <td>
<%-- INF-20084 Datepicker-- AGodara --%>
			   <%if(!meetingDtAtt.equals("1")) { %>
			 		<input type="text" name="meetingDate" class="datefield" size="15" MAXLENGTH="20" value='<%=meetingDate%>' <%=readOnlyStr%> <%=disableStr%>>
			  <%}else{%>
			  		<input type="text" name="meetingDate" size="15" MAXLENGTH="20" value='<%=meetingDate%>' <%=disableStr%>>
			  <%}%>
					</td>
			<%}else{%>
					<input type="hidden" name="meetingDate" size="15" MAXLENGTH="20" value='<%=meetingDate%>' <%=readOnlyStr%> <%=disableStr%>>
			<%}}else{%>
					<td><%=LC.L_Meeting_Date%><%--Meeting Date*****--%></td>
					<td>
						<input type="text" name="meetingDate" class="datefield" size="15" MAXLENGTH="20" value='<%=meetingDate%>'>
					</td>
			<%}%>
				</tr>


    <tr><td height=1></td></tr>


	 <tr>
	 <%if (hashPgCustFld.containsKey("reviewboard")) {
			int fldNumReview = Integer.parseInt((String)hashPgCustFld.get("reviewboard"));
			String reviewMand = ((String)cdoPgField.getPcfMandatory().get(fldNumReview));
			String reviewLable = ((String)cdoPgField.getPcfLabel().get(fldNumReview));
			reviewAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumReview));

			if(reviewAtt == null) reviewAtt ="";
			if(reviewMand == null) reviewMand ="";


			if(!reviewAtt.equals("0")) {
			if(reviewLable !=null){
			%> <td>
			<%=reviewLable%>
			<%} else {%> <td>
			 <%=LC.L_Review_Board%><%--Review Board*****--%>
			<%}

			if (reviewMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomreview">* </FONT>
			<% } %>

    </td>
    <td>

	 <%=  ddRevBoard%>  <%if(reviewAtt.equals("2")) {%><input type="hidden" name="revBoard" value="<%=revBoard%>"> <%}%>
    </td>
	<%} else if(reviewAtt.equals("0")) {%>
	<input type="hidden" name="revBoard" value="<%=revBoard%>">
	<%}} else {%>

	<td> <%=LC.L_Review_Board%><%--Review Board*****--%> </td>
	<td> <%= ddRevBoard%>	</td>
    <%}%>
  </tr>


 <tr><td height=1></td></tr>

 <tr>
	 <%if (hashPgCustFld.containsKey("outcome")) {
			int fldNumOutcome = Integer.parseInt((String)hashPgCustFld.get("outcome"));
			String outcomeMand = ((String)cdoPgField.getPcfMandatory().get(fldNumOutcome));
			String outcomeLable = ((String)cdoPgField.getPcfLabel().get(fldNumOutcome));
			outcomeAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumOutcome));

			if(outcomeAtt == null) outcomeAtt ="";
			if(outcomeMand == null) outcomeMand ="";


			if(!outcomeAtt.equals("0")) {
			if(outcomeLable !=null){
			%> <td>
			<%=outcomeLable%>
			<%} else {%> <td>
			 <%=LC.L_Outcome%><%--Outcome*****--%>
			<%}

			if (outcomeMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomoutcome">* </FONT>
			<% } %>

    </td>
    <td>

	 <%=studyStatusOutcome %>  <%if(outcomeAtt.equals("2")) {%><input type="hidden" name="studyStatusOutcome" value="<%=mStudyStatusOutcome%>"> <%}%>
    </td>
	<%} else if(outcomeAtt.equals("0")) {%>

	<input type="hidden" name="studyStatusOutcome" value ="<%=mStudyStatusOutcome%>" >
	<%}} else {%>
    <td> <%=LC.L_Outcome%><%--Outcome*****--%> </td>
	<td> <%=studyStatusOutcome %>	</td>
    <%}%>
  </tr>


  <!--End added -->
<%--
 <tr>
    <td width="300">
       App/Ren #
    </td>
    <td>
	<%=dAppRenNum%>
    </td>
  </tr>
   <tr>
    <td width="300">
       App/Ren Status
    </td>
    <td>
	<%=dAppRenStatus%>
    </td>
  </tr>

 <tr>
    <td width="300">
       HSPN #
    </td>
    <td>
	<input type="text" name="hspnNum" size = 15 MAXLENGTH = 15 value='<%=hspnNum%>'>
    </td>
  </tr>
     z
--%>


<tr>
        <%if (hashPgCustFld.containsKey("notes")) {
			int fldNumNotes = Integer.parseInt((String)hashPgCustFld.get("notes"));
			String notesMand = ((String)cdoPgField.getPcfMandatory().get(fldNumNotes));
			String notesLable = ((String)cdoPgField.getPcfLabel().get(fldNumNotes));
			String notesAtt = ((String)cdoPgField.getPcfAttribute().get(fldNumNotes));

			disableStr ="";
			readOnlyStr ="";
			if(notesAtt == null) notesAtt ="";
			if(notesMand == null) notesMand ="";


			if(!notesAtt.equals("0")) {
			if(notesLable !=null){
			%><td>
			<%=notesLable%>
			<%} else {%> <td>
			 <%=LC.L_Notes%><%--Notes*****--%>
			<%}

			if (notesMand.equals("1")) {
				%>
			   <FONT class="Mandatory" id="pgcustomnotes">* </FONT>
			<% }
		 %>

		  <%if(notesAtt.equals("1")) {
			 disableStr = "disabled"; }
		     else if (notesAtt.equals("2") ) {
			 readOnlyStr = "readonly"; }
		 %>



    </td>
    <td>
		<TextArea name="protocolNotes" rows=3 cols=60 MAXLENGTH =2000  <%=disableStr%>  <%=readOnlyStr%> > <%=protocolNotes%> </TextArea>
	</td>
    	<% } else { %>

	 <TextArea name="protocolNotes"  Style = "visibility:hidden"  rows=3 cols=60 MAXLENGTH =2000  <%=disableStr%>  <%=readOnlyStr%> > <%=protocolNotes%> </TextArea>

     <% }} else {
		 %>
  	 <!--Added by Manimaran for the September Enhancement (S8)-->
	 <td >
       <%=LC.L_Notes%><%--Notes*****--%>
    </td>
    <td>
	<TextArea name="protocolNotes" rows=3 cols=60 MAXLENGTH =2000><%=protocolNotes%></TextArea>
    </td>

  <%}%>

</tr>

  <tr>
 <td colspan="2">
 <% if (mode.equals("N")) { %>
       <!--KM-Modified based on code review -->
  <input type="checkbox" name="currentStat" checked> <%=MC.M_ThisStdsCur_Stat%><%--This is <%=LC.Std_Study%>'s Current Status*****--%>
 <%} else if(EJBUtil.stringToNum(currentStat)==0) {%>
 <input type="checkbox" name="currentStat"> <%=MC.M_ThisStdsCur_Stat%><%--This is <%=LC.Std_Study%>'s Current Status*****--%>
 <%} else if(EJBUtil.stringToNum(currentStat)==1) { %>
  <input type="checkbox" name="currentStat" checked disabled> <%=MC.M_ThisStdsCur_Stat%><%--This is <%=LC.Std_Study%>'s Current Status*****--%>
 <%}%>
 </td>
 </tr>

	   </table>
<br>

 <jsp:include page="submitBar.jsp" flush="true">
		<jsp:param name="displayESign" value="Y"/>
		<jsp:param name="formID" value="studystatForm"/>
		<jsp:param name="showDiscard" value="N"/>
 </jsp:include>

</Form>

<%

	} //end of if body for page right

else

{

%>

<jsp:include page="accessdenied.jsp" flush="true"/>

<%

} //end of else body for page right

}//end of if body for session

else

{

%>

<jsp:include page="timeout.html" flush="true"/>

<%

}

%>

<div class = "myHomebottomPanel">
<jsp:include page="bottompanel.jsp" flush="true"/>
</div>

</div>

<DIV class="mainMenu" id = "emenu">

  <jsp:include page="getmenu.jsp" flush="true"/>

</DIV>


</body>

</html>



