<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> View Migration Log</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Migrate Data - Forms - View Migration Log</div>
<BR>
<table>
<?php

$pk_vlnk_adapmod = $_GET["pk_vlnk_adapmod"];
$v_linked_to = $_GET["linkedto"];
$v_form = $_GET["fk_form"];
$v_tablename = ($v_linked_to=='A'?"ER_IMPACCTFORM":($v_linked_to=='S'?"ER_IMPSTUDYFORM":($v_linked_to=='P'?"ER_IMPPATFORM":"")));

$query_sql = "SELECT COUNT(*) as count FROM $v_tablename where fk_form = $v_form and custom_col = ".$pk_vlnk_adapmod;
$results = executeOCIQuery($query_sql,$ds_conn);
echo "<tr><td>Total rows in staging area: </td><td>";
echo $results["COUNT"][0];
if ($results["COUNT"][0] > 0) {	
	echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href=mig_form_view.php?pk_vlnk_adapmod=$pk_vlnk_adapmod&form_linkto=$v_linked_to&fk_form=$v_form>View</a>";
}
echo "</td></tr>";

$query_sql = "SELECT COUNT(*) as count FROM $v_tablename where fk_form = $v_form and export_flag = 9 and custom_col = ".$pk_vlnk_adapmod;
$results = executeOCIQuery($query_sql,$ds_conn);
echo "<tr><td>Total rows pending migration: </td><td>";
echo $results["COUNT"][0];
if ($results["COUNT"][0] > 0) {	
	echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href=mig_form_view.php?pk_vlnk_adapmod=$pk_vlnk_adapmod&form_linkto=$v_linked_to&flag=9&fk_form=$v_form>View</a>";
}
echo "</td></tr>";

$query_sql = "SELECT COUNT(*) as count FROM $v_tablename where fk_form = $v_form and export_flag = 0 and custom_col = ".$pk_vlnk_adapmod;
$results = executeOCIQuery($query_sql,$ds_conn);
echo "<tr><td>Total rows migrated: </td><td>";
echo $results["COUNT"][0];
if ($results["COUNT"][0] > 0) {	
	echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href=mig_form_view.php?pk_vlnk_adapmod=$pk_vlnk_adapmod&form_linkto=$v_linked_to&flag=0&fk_form=$v_form>View</a>";
}
echo "</td></tr>";

$query_sql = "SELECT COUNT(*) as count FROM $v_tablename where fk_form = $v_form and export_flag = -1 and custom_col = ".$pk_vlnk_adapmod;
$results = executeOCIQuery($query_sql,$ds_conn);
echo "<tr><td>Total rows with errors: </td><td>";
echo $results["COUNT"][0];
if ($results["COUNT"][0] > 0) {	
	echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href=mig_form_view.php?pk_vlnk_adapmod=$pk_vlnk_adapmod&form_linkto=$v_linked_to&flag=-1&fk_form=$v_form>View</a>";
}
echo "</td></tr>";




?>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
