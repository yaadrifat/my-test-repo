<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velos eTools -> caDSR Integration</title>
<script>
function getCDEData() {
 window.open ("https://cdebrowser.nci.nih.gov/CDEBrowser/","mywindow","status=1,toolbar=1");
}

function formPreview(formPk){
	var win = "form_preview.php?formPk="+formPk;
	window.open(win,'mywin',"toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=825,height=600,top=90,left=150");
}
function validate(form){


	if (form.uploadedfile.value == ""){
		alert("Choose a file to upload.");
		form.uploadedfile.focus();
		return false;
	}

	
	if (!form.expForm.checked && !form.expField.checked){
		alert("Select at least one import option.");
		return false;
	}
	
	if (form.expForm.checked && form.formname.value == ""){
		alert("Form name cannot be blank.");
		return false;
	}

	return true;
}

</script>

</head>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");
include_once("fieldXSL.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body>
<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	
<?php

if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 

	$v_sql = "select pk_catlib || '|' || fk_account as pk_catlib,catlib_name || decode(ac_name,null,'',' [' || ac_name || ']') as catlib_name from er_catlib,er_account WHERE pk_account = fk_account and catlib_type = 'T' and record_type <> 'D' order by ac_name || ' - ' || lower(catlib_name)";
	$results = executeOCIQuery($v_sql,$ds_conn);
	$cat_dropdown = '';
	for ($rec = 0; $rec < $results_nrows; $rec++){
		$cat_dropdown .= "<option value=".$results["PK_CATLIB"][$rec].">".$results["CATLIB_NAME"][$rec]."</option>";
	}

?>

<div class="navigate">caDSR Integration:</div>
 

<form name="formimport" enctype="multipart/form-data" action="nci_cadsr_import_xml.php" method="POST" onSubmit="if (validate(document.formimport) == false) return false;">
<table>
<tr><td>STEP 1:</td><td><a href="#" onClick="getCDEData();">Get data from CDE Browser</a><BR><i>Download Protocol Forms in XML format by click on link <b>[Download Data Elements as XML]</b></i></td></tr>
<tr><td> <br></td></tr>
<tr><td>STEP 2:</td><td>Upload File:</td></tr>
<tr><td> </td><td><input class="required" type="file" name="uploadedfile" size="75"/></td></tr>
<tr><td> <br></td></tr>
<tr><td>STEP 3:</td><td>Import</td></tr>
<TR><TD>&nbsp;
</TD><TD>Form Category: <select class="required" name="category"><?PHP echo $cat_dropdown; ?></select></TD></TR>
<tr><td> </td><td>Import to Form Library:<INPUT checked TYPE="CHECKBOX" name="expForm"/> Specify Form Name: <input class="required" type="text" name="formname" size="50"/></td></tr>
<tr><td> </td><td>Import to Field Library:<INPUT checked TYPE="CHECKBOX" name="expField"/></td></tr>

</table>
	<BR><input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" /> 

</form>
<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Note">
<tr>
<th align="left">Note</th>
</tr>
<tr><td align="left" valign="top">
<p>STEP 1: Download data elements in XML format by clicking on the link "Get data from CDE Browser" and save it to your local folder.</p>
<p>STEP 2: Select the downloaded XML file.</p>
<p>STEP 3: Import to Form and/or Field Library by selecting the appropriate name. Remember to specify a Form Name if importing into Form Library.</p>
</td></tr>
</table></div>

<?php } else {

	
  $curDir = getcwd();
  $target_path = $curDir."/upload/";

  $target_path = $target_path.basename( $_FILES['uploadedfile']['name']);

	$v_expForm = 0;
	$v_expField = 0;
  if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {

	if (isset($_POST["expForm"])) $v_expForm = 1;
	if (isset($_POST["expField"])) $v_expField = 1;

	list($v_category,$v_account) = explode("|",$_POST["category"]);\
	$v_formname = $_POST["formname"];

	$se_sql = "select form_name from er_formlib where fk_catlib = '".$v_category."' and fk_account = '".$v_account."' and form_name = '".$v_formname."'";
	$results = executeOCIQuery($se_sql,$ds_conn);
	$v_name_exists = $results["FORM_NAME"][0];
	
	if($v_name_exists != '') {
		echo "Form <b>$v_formname</b> already exists.<BR><BR>";
		echo '<meta http-equiv="refresh" content="2; url=./nci_cadsr_import_xml.php">';
	}
	
	$v_pk_formsec = 0;
	if ($v_expForm == 1) {
		$v_formname = $_POST["formname"];
		$v_formname = substr($v_formname,0,49);
		$results = executeOCIQuery("SELECT pk_codelst from er_codelst where codelst_type='frmlibstat' and codelst_subtyp='W'",$ds_conn);
		$v_form_status = $results["PK_CODELST"][0];
		$results = executeOCIQuery("SELECT seq_er_formlib.NEXTVAL as pk_formlib FROM dual",$ds_conn);
		$v_pk_formlib = $results["PK_FORMLIB"][0];
		$v_sql = "insert into er_formlib (pk_formlib,fk_catlib,fk_account,form_name,form_desc,form_sharedwith,form_status,form_linkto,form_xslrefresh,record_type) values ($v_pk_formlib,$v_category,$v_account,'$v_formname','$v_formname','A',$v_form_status,'L',1,'N')";
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);

		// Insert records in er_objectshare
		$results = executeOCIQuery("SELECT seq_er_objectshare.NEXTVAL as pk_objectshare FROM dual",$ds_conn);
		$v_pk_objectshare = $results["PK_OBJECTSHARE"][0];
		$v_sql = "insert into er_objectshare (pk_objectshare,object_number,fk_object,fk_objectshare_id,objectshare_type,record_type) values ($v_pk_objectshare,1,$v_pk_formlib,$v_account,'A','N')";
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);

		$v_sql = "insert into er_objectshare (pk_objectshare,object_number,fk_object,fk_objectshare_id,objectshare_type,record_type,fk_objectshare) select seq_er_objectshare.NEXTVAL,1,$v_pk_formlib,pk_user,'U','N',$v_pk_objectshare from er_user where fk_account = $v_account and usr_type <> 'X'";
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);

		// Form section
		$results = executeOCIQuery("SELECT seq_er_formsec.NEXTVAL as pk_formsec FROM dual",$ds_conn);
		$v_pk_formsec = $results["PK_FORMSEC"][0];
		$v_sql = "insert into er_formsec (pk_formsec,fk_formlib,formsec_name,formsec_seq,formsec_fmt,formsec_repno,record_type) values ($v_pk_formsec,$v_pk_formlib,' ','1','N',0,'N')";
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);

		// Insert record in ER_FLDLIB (data entry date)
		$results = executeOCIQuery("SELECT seq_er_fldlib.NEXTVAL as pk_field FROM dual",$ds_conn);
		$v_pk_field = $results["PK_FIELD"][0];
		$v_sql = "insert into er_fldlib (pk_field,fk_account,fld_libflag,fld_name,fld_desc,fld_uniqueid,fld_systemid,fld_type,fld_datatype,record_type,fld_align,fld_display_width,fld_bold) values ($v_pk_field,$v_account,'F','Data Entry Date','','er_def_date_01','er_def_date_01','E','ED','N','left',35,1)";
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);

		$fieldXSL = new fieldXSL();
		$fieldXSL->fieldName = 'Data Entry Date';
		$fieldXSL->fieldId = "er_def_date_01";
		$fieldXSL->fieldType = "E";
		$fieldXSL->fieldDataType = "ED";
		$fieldXSL->fieldMandatory = "1";
		$fieldXSL->getFieldXSL();


		$v_sql = "insert into er_formfld (pk_formfld,fk_formsec,fk_field,formfld_seq,formfld_mandatory,formfld_browserflg,record_type,formfld_xsl,formfld_javascr) values (seq_er_formfld.nextval,$v_pk_formsec,$v_pk_field,0,1,1,'N','".str_replace("'","''",$fieldXSL->xsl)."','".str_replace("'","''",$fieldXSL->js)."')";
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);
	}


	$xml = new XMLReader();
	$xml->open($target_path);


$v_sql = "";
$v_fieldKeyword = "";
while($xml->read()){

			switch ($xml->name) {
			case "PUBLICID":
				if (strlen($v_fieldKeyword) > 0) {
					createField($v_fieldKeyword,$v_fieldId,$v_fieldName,$v_fieldDesc,$v_fieldCategory,$v_valueDomainType,$v_datatype,$v_fieldLength,$v_fldminval,$v_fldmaxval,$v_fieldVersion,$v_dispval,$v_dataval,$v_expForm,$v_expField,$ds_conn,$v_pk_formsec,$v_account);
				}
				$v_dispval = array();
				$v_dataval = array();
				$xml->read();
				$v_fieldKeyword = str_replace("'","''",$xml->value);
				$xml->read();
				break;
			case "PREFERREDNAME":
				$xml->read();
				$v_fieldId =str_replace("'","''",$xml->value);
				$xml->read();
				break;
			case "LONGNAME":
				$xml->read();
				$v_fieldName = substr(str_replace("'","''",$xml->value),0,2000);
				$xml->read();
				break;
			case "PREFERREDDEFINITION":
				$xml->read();
				$v_fieldDesc = str_replace("'","''",$xml->value);
				$xml->read();
				break;
			case "CONTEXTNAME":
				$xml->read();
				$v_fieldCategory = str_replace("'","''",$xml->value);
				$xml->read();
				break;
			case "ValueDomainType":
				$xml->read();
				$v_valueDomainType = str_replace("'","''",$xml->value);
				$xml->read();
				break;
			case "Datatype":
				$xml->read();
				$v_datatype = str_replace("'","''",$xml->value);
				$xml->read();
				break;
			case "MaximumLength":
				$xml->read();
				$v_fieldLength = str_replace("'","''",$xml->value);
				if (strlen(trim($v_fieldLength)) == 0) $v_fieldLength = 'null';
				$xml->read();
				break;
			case "MinimumLength":
				$xml->read();
				$v_fldminval = str_replace("'","''",$xml->value);
				$xml->read();
				break;
			case "MaximumValue":
				$xml->read();
				$v_fldmaxval = str_replace("'","''",$xml->value);
				$xml->read();
				break;
			case "VERSION":
				$xml->read();
				$v_fieldVersion = str_replace("'","''",$xml->value);
				$xml->read();
				break;
			case "VALUEMEANING":
				$xml->read();
				$v_dispval[] = str_replace("'","''",$xml->value);
				$xml->read();
				break;
			case "VALIDVALUE":
				$xml->read();
				$v_dataval[] = str_replace("'","''",$xml->value);
				$xml->read();
				break;
			}
}

if (strlen($v_fieldKeyword) > 0) {
	createField($v_fieldKeyword,$v_fieldId,$v_fieldName,$v_fieldDesc,$v_fieldCategory,$v_valueDomainType,$v_datatype,$v_fieldLength,$v_fldminval,$v_fldmaxval,$v_fieldVersion,$v_dispval,$v_dataval,$v_expForm,$v_expField,$ds_conn,$v_pk_formsec,$v_account);
}


$xml->close();


if 	($v_expField == 1 && $v_name_exists == '') echo "CDE's imported.<BR><BR>";

//echo "Data Imported to table: ".$v_tablename;
	unlink("./upload/".$_FILES['uploadedfile']['name']);
	if ($v_expForm == 1 && $v_name_exists == '') {
		$v_html = "";
		$sql = "begin PKG_FORM.SP_CLUBFORMXSL(:pk_formlib,:html); end;";
		$stmt = oci_parse($ds_conn,$sql);
	
		//  Bind the input parameter
		oci_bind_by_name($stmt,':pk_formlib',$v_pk_formlib);
	
		// Bind the output parameter
		oci_bind_by_name($stmt,':html',$v_html);
	
		oci_execute($stmt);
	
		$sql = "begin PKG_FILLEDFORM.SP_GENERATE_PRINTXSL(".$v_pk_formlib."); end;";
		$results = executeOCIUpdateQuery($sql,$ds_conn);
	
		OCICommit($ds_conn);
		OCILogoff($ds_conn);
	
		echo "Form <b>$v_formname</b> created.<BR><BR>";
		echo '<a href=# onclick="formPreview('.$v_pk_formlib.')">Preview</a>';
	}
} else{
     echo "There was an error uploading the file, please try again!"."<BR>";
}


}

?>

	
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");


function createField($v_fieldKeyword,$v_fieldId,$v_fieldName,$v_fieldDesc,$v_fieldCategory,$v_valueDomainType,$v_datatype,$v_fieldLength,$v_fldminval,$v_fldmaxval,$v_fieldVersion,$v_dispval,$v_dataval,$v_expForm,$v_expField,$ds_conn,$v_pk_formsec,$v_account) {
	$v_fldseq = 0;
	$v_fieldDesc = substr($v_fieldDesc,0,950)." (Version:".$v_fieldVersion.")";

	if (strlen($v_fieldName) > 0 ) {
		if (strlen($v_fieldLength) == 0) $v_fieldLength = 'NULL';
		$v_respSeq = 1;
		$v_respSeq_fldlib = 1;
	
		$v_fldType = "";
		$v_fldDatatype = "";
		$v_fldcolcount = "null";
		if ($v_valueDomainType == "NonEnumerated"){
			switch ($v_datatype){
			case "CHARACTER":
				$v_fldType = "E";
				$v_fldDatatype = 'ET';
				break;
			case "DATE":
				$v_fldType = "E";
				$v_fldDatatype = 'ED';
				break;
			case "NUMBER":
				$v_fldType = "E";
				$v_fldDatatype = 'EN';
				break;
			case "COMMENT":
				$v_fldType = "C";
				$v_fldDatatype = '';
				$v_fldinstructions = stripslashes(str_replace("'","''",$v_fieldName[$i]));
				break;
			default:
				$v_fldType = "E";
				$v_fldDatatype = 'ET';
				break;
			}
		} else {
			$v_fldType = "M";
			$v_fldDatatype = 'MD';
			$v_fldcolcount = "1";
		}

		if 	($v_expForm == 1) {
			$results = executeOCIQuery("SELECT seq_er_fldlib.NEXTVAL as pk_field FROM dual",$ds_conn);
			$v_pk_field = $results["PK_FIELD"][0];
			$v_sql = "insert into er_fldlib (pk_field,fk_account,fld_libflag,fld_name,fld_desc,fld_uniqueid,fld_type,fld_datatype,record_type,fld_align,fld_keyword,fld_charsno,fld_display_width,fld_bold,fld_colcount) values 
			($v_pk_field,$v_account,'F','$v_fieldName','$v_fieldDesc','$v_fieldId','$v_fldType','$v_fldDatatype','N','left','$v_fieldKeyword',$v_fieldLength,35,1,$v_fldcolcount)";
			$results = executeOCIUpdateQuery($v_sql,$ds_conn);
			$v_sql = "insert into er_fldvalidate (pk_fldvalidate,fk_fldlib,record_type) values (seq_er_fldvalidate.nextval,$v_pk_field,'N')";
			$results = executeOCIUpdateQuery($v_sql,$ds_conn);
			$v_fldseq = $v_fldseq + 1;

			$results = executeOCIQuery("SELECT fld_systemid from er_fldlib where pk_field = ".$v_pk_field,$ds_conn);
			$v_fldSysId = $results["FLD_SYSTEMID"][0];
			
			$fieldXSL = new fieldXSL();
			$fieldXSL->fieldName = $v_fieldName;
			$fieldXSL->fieldId = $v_fldSysId;
			$fieldXSL->fieldType = $v_fldType;
			$fieldXSL->fieldDataType = $v_fldDatatype;
			$fieldXSL->fieldMandatory = 0;
			$fieldXSL->getFieldXSL();

			$v_sql = "insert into er_formfld (pk_formfld,fk_formsec,fk_field,formfld_seq,formfld_browserflg,record_type,formfld_mandatory,formfld_xsl,formfld_javascr) values 
			(seq_er_formfld.nextval,$v_pk_formsec,$v_pk_field,$v_fldseq,0,'N',0,'".str_replace("'","''",$fieldXSL->xsl)."','".str_replace("'","''",$fieldXSL->js)."')";
			$results = executeOCIUpdateQuery($v_sql,$ds_conn);
		}

		if 	($v_expField == 1) {
			$results = executeOCIQuery("SELECT PK_CATLIB FROM er_catlib where fk_account = $v_account and catlib_type = 'C' and record_type <> 'D' and upper(catlib_name) = '".strtoupper($v_fieldCategory)."'",$ds_conn);
			if (isset($results["PK_CATLIB"][0])){
				$v_pk_fieldcat = $results["PK_CATLIB"][0];
			} else {
				$results = executeOCIQuery("SELECT seq_er_catlib.NEXTVAL as pk_fieldcat FROM dual",$ds_conn);
				$v_pk_fieldcat = $results["PK_FIELDCAT"][0];
				$v_sql = "insert into er_catlib (pk_catlib,fk_account,catlib_type,record_type,catlib_name) values 
				($v_pk_fieldcat,$v_account,'C','N','$v_fieldCategory')";
				$results = executeOCIUpdateQuery($v_sql,$ds_conn);
				
			}
			
			$results = executeOCIQuery("SELECT PK_field FROM er_fldlib where fk_account = $v_account and fld_libflag = 'L' and record_type <> 'D' and upper(fld_name) = '".strtoupper($v_fieldName)."'",$ds_conn);
			if (isset($results["PK_FIELD"][0])){
				$v_pk_fieldlib = $results["PK_FIELD"][0];
				$v_newfield = 0;
			} else {
				$v_newfield = 1;
				$results = executeOCIQuery("SELECT seq_er_fldlib.NEXTVAL as pk_field FROM dual",$ds_conn);
				$v_pk_fieldlib = $results["PK_FIELD"][0];
				$v_sql = "insert into er_fldlib (pk_field,fk_account,fld_libflag,fld_name,fld_desc,fld_uniqueid,fld_type,fld_datatype,record_type,fld_align,fld_keyword,fld_charsno,fld_display_width,fld_bold,fk_catlib,fld_colcount) values 
				($v_pk_fieldlib,$v_account,'L','$v_fieldName','$v_fieldDesc','$v_fieldId','$v_fldType','$v_fldDatatype','N','left','$v_fieldKeyword',$v_fieldLength,35,1,$v_pk_fieldcat,$v_fldcolcount)";
				$results = executeOCIUpdateQuery($v_sql,$ds_conn);
				$v_sql = "insert into er_fldvalidate (pk_fldvalidate,fk_fldlib,record_type) values (seq_er_fldvalidate.nextval,$v_pk_fieldlib,'N')";
				$results = executeOCIUpdateQuery($v_sql,$ds_conn);
			}
		}

		}

	$count = count($v_dispval);
	for ($i=0;$i<$count;$i++) {
		$v_mvalues_disp = $v_dispval[$i];
		$v_mvalues_data = $v_dataval[$i];
// using display value as the data value because valuemeaning tag is descriptive to be used as data value		
		if 	($v_expForm == 1) {
			$v_sql = "insert into er_fldresp (pk_fldresp,fk_field,fldresp_seq,fldresp_dispval,fldresp_dataval,fldresp_score,record_type) values 
			(seq_er_fldresp.nextval,$v_pk_field,$v_respSeq,'$v_mvalues_disp','$v_mvalues_data',0,'N')";
			$results = executeOCIUpdateQuery($v_sql,$ds_conn);
			$v_respSeq = $v_respSeq + 1;
		}
		if 	($v_expField == 1 && $v_newfield == 1) {
			$v_sql = "insert into er_fldresp (pk_fldresp,fk_field,fldresp_seq,fldresp_dispval,fldresp_dataval,fldresp_score,record_type) values 
			(seq_er_fldresp.nextval,$v_pk_fieldlib,$v_respSeq_fldlib,'$v_mvalues_disp','$v_mvalues_data',0,'N')";
			$results = executeOCIUpdateQuery($v_sql,$ds_conn);
			$v_respSeq_fldlib = $v_respSeq_fldlib + 1;
		}
	}
	
}
?>
