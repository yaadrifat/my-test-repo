<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Select Datasource</title>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");
?>
</head>
<body>
<div id="fedora-content">
<div class="navigate">Select Datasource</div>
<br>	
<?PHP
$db = new Database("etools");
$result = $db->executeQuery("SELECT DS_RIGHTS from ET_GROUPS where PK_GROUPS=".$_SESSION['FK_GROUPS']);
//$result = $db->executeQuery($ds_1.$_SESSION['FK_GROUPS']);
$dropdown = '';
$flag = 'false';
if ($result->getRowCount() > 0) {
while ($result->next()) {
	$data = $result->getCurrentValuesAsHash();
	$v_ds_rights = explode("|",$data['DS_RIGHTS']);
	foreach ($v_ds_rights as $v_ds){
		list($pk_ds,$ds_access) = explode(":",$v_ds);
		if ($ds_access == 1) {
		$flag = 'true';
			$rs = $db->executeQuery("SELECT PK_DS,DS_NAME from ET_DS where PK_DS=".$pk_ds);
//			$rs = $db->executeQuery($ds_2.$pk_ds);
			while ($rs->next()) {
				$ds_data = $rs->getCurrentValuesAsHash();
  		        $dropdown .= "<option value=".$ds_data["PK_DS"].">".$ds_data["DS_NAME"]."</option>";
			}
		}	
	}
} 
} //else {echo "NO DATA".$_SESSION['FK_GROUPS'];}


?>
<form name="datasource" method=post action="loggedinusers.php">
<?php 
	if($_SESSION['FK_GROUPS'] == "1"){	
	$db = new Database("etools");
	$result = $db->executeQuery("SELECT * from ET_DS");
	if ($result->getRowCount() == 0) { 
		?>
		<table width="415">
		<tr>
		<td width="5%">&nbsp;</td>
		<td width="56%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="datasource_edit.php?mode=n">Create Datasource</a></td>
		</tr>
		</table>
	<?php 
		}elseif($flag!='false'){
	?>
		<table width="415">
		<tr>
		<td width="5%">&nbsp;</td>
		<td width="13%">Datasource:&nbsp;&nbsp;</td>
		<td width="11%"><select name="DS"><?PHP echo $dropdown; ?></select></td>
		<td width="15%"><img src="./img/submit.png" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" onClick="document.datasource.submit()"/></td>
		</tr>
		</table>
		<?php
			}else{
		?>
		<table width="415">
		<tr>
		<td width="5%">&nbsp;</td>
		<td width="56%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="datasource_dash.php">Enable Datasource</a></td>
		</tr>
		</table>
		<?php } ?>
<?php 
// this is for non admin users
	}else{
?>
<table>
<tr>
<td width="5%">&nbsp;</td>
<td>Datasource:&nbsp;&nbsp;</td>
<td><select name="DS"><?PHP echo $dropdown; ?></select></td>
<td>&nbsp;&nbsp;<img src="./img/submit.png" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" onClick="document.datasource.submit()"/></td>
</tr>
</table>
<?php
	}
?>
</form>

<?php
	$db = new Database("etools");
	$result = $db->executeQuery("SELECT * from ET_DS");
	if ($result->getRowCount() == 0) { ?>
		<div class="warning" style="margin-left: 0.5in; margin-right: 0.5in;">
		<table border="0" >
		<tr>
		<th align="left">Note</th>
		</tr>
		<tr><td align="left" valign="top">
			<p>Currently you dont have any datasource. To create a new datasource click the link "Create Datasource"</p></td>
		</tr>
		</table>
		</div>
	<?php
	}elseif($flag=="true"){?>
		<div class="warning" style="margin-left: 0.5in; margin-right: 0.5in;">
		<table border="0" >
		<tr>
		<th align="left">Note</th>
		</tr>
		<tr><td align="left" valign="top">
			<p>Please select a datasource before clicking on any menu item.</p></td>
		</tr>
		</table>
		</div>	
	<?php
	}else{
?>
		<div class="warning" style="margin-left: 0.5in; margin-right: 0.5in;">
		<table border="0" >
		<tr>
		<th align="left">Note</th>
		</tr>
		<tr><td align="left" valign="top">
			<p>All your datasources are disabled. Click the link "Enable Datasource" to enable the datasources</p></td>
		</tr>
		</table>
		</div>
<?php } ?>
      </div>


</body>
</html>
<?php
}
else header("location: index.php?fail=1");
?>
