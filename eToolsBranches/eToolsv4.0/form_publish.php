<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Form Transfer</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>

<script>
function formPreview(formPk){
	var win = "form_preview.php?formPk="+formPk;
	window.open(win,'mywin',"toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=825,height=600,top=90,left=150");
}


</script>

</head>


<body>

<div id="fedora-content">	
<div class="navigate">Form Transfer</div>
	
<?PHP

//$v_account = 1170;
//$v_account = 52;
//$v_account = 78;

if (isset($_POST["formname"])){
	$searchvalue = $_POST["formname"];
} else {
	$searchvalue = "";
}
?>
<form name="formtransfer" method="post" action="form_publish.php">
<table border = "1">
<tr>
	<td>Search Form Name: </td><td><input name="formname" type="text" size="50" maxlength="100" value="<?PHP echo $searchvalue; ?>"/></td>
	<td><input type=submit value=Submit></input></td>
</tr>

</table>
</form>

<?PHP
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
//$v_account = $_POST["fk_account"];
?>

<Table border="1"><TR>
<TH width="10%">FORM NAME</TH>
<TH width="35%">FORM DESCRIPTION</TH>
<TH width="25%">LINKED TO</TH>
<TH width="20%">STUDY NUMBER</TH>
<TH width="10%">&nbsp;</TH>
<TH width="10%">&nbsp;</TH>
</TR>
<?php

$query_sql = "SELECT pk_formlib,form_name, form_desc,decode(lf_displaytype,'S','Study','SP','Patient (Specific Study)','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','SA','All Studies','PA','All Patients') as lf_displaytype,DECODE(fk_study,NULL,'',(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study)) AS study_number FROM ER_FORMLIB a, ER_LINKEDFORMS b WHERE pk_formlib = fk_formlib AND  b.record_type <> 'D' AND form_status = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'frmstat' AND codelst_subtyp = 'A') AND LOWER(trim(form_name)) LIKE lower('%".$searchvalue."%')";
$results = executeOCIQuery($query_sql,$ds_conn);

for ($rec = 0; $rec < $results_nrows; $rec++){
?>
<TR>
	<TD><?php echo $results["FORM_NAME"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["FORM_DESC"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["LF_DISPLAYTYPE"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["STUDY_NUMBER"][$rec] . "&nbsp;"; ?></TD>
<?php
	echo "<td><a href=form_publish_save.php?pk_formlib=".$results["PK_FORMLIB"][$rec]."&formname=".urlencode($results["FORM_NAME"][$rec]).">Select</a></td>";
	echo '<td><a href=# onclick="formPreview('.$results["PK_FORMLIB"][$rec].')">Preview</a></td>';
	echo "</TR>";
}
?>
</TABLE>
<?php } ?>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
