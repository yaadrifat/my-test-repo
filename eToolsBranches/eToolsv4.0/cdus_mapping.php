<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> CDUS Submission</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Migrate Data - Standard - Edit Mapping</div>
<form action="cdus_mapping.php" method=post>
<?php
if (isset($_POST['col_name']) ) {
	$v_col_name =  $_POST['col_name'];
} else {
	$v_col_name =  "";
}
	
	
	if (isset($_GET['fk_exp_definition'])) {
		$v_exp_definition = $_GET['fk_exp_definition'];
		$v_exp_datamain = $_GET['pk_exp_datamain'];
	} else {
		$v_exp_definition = $_POST['fk_exp_definition'];
		$v_exp_datamain = $_POST['pk_exp_datamain'];
	}

?>
<!-- <form action="cdus_mapping.php" method=post> -->
<?php

	echo "<INPUT type=\"hidden\" name=\"fk_exp_definition\" value=\"".$v_exp_definition."\"></input>"; 
	echo "<INPUT type=\"hidden\" name=\"pk_exp_datamain\" value=\"".$v_exp_datamain."\"></input>"; 
	$query_sql = "SELECT distinct col_name FROM EXP_MAPPING, EXP_SECTION_COLS,EXP_SECTIONS WHERE  pk_exp_sections = fk_exp_sections AND pk_exp_section_cols = fk_exp_section_cols and fk_exp_definition = ".$v_exp_definition;
	$results = executeOCIQuery($query_sql,$ds_conn);
	$dropdown="";
	if ($v_col_name == "") {
		$dropdown .= "<option value='' selected>Select an option</option>";
	} else {
		$dropdown .= "<option value=''>Select an option</option>";
	}

	for ($rec = 0; $rec < $results_nrows; $rec++){
		if ($v_col_name == $results["COL_NAME"][$rec]) {
	      $dropdown .= "<option value=".$results["COL_NAME"][$rec]." SELECTED>".$results["COL_NAME"][$rec]."</option>";
	    } else {
	      $dropdown .= "<option value=".$results["COL_NAME"][$rec].">".$results["COL_NAME"][$rec]."</option>";
	    }
	}
?>
	Column Name: <select name="col_name"><?PHP echo $dropdown; ?></select>
	<input type="submit" name="go" value="Go"> 
<!-- </form> -->

<?php
if (!isset($_POST['submit'])){ 

	if (!isset($_POST['col_name'])) {	
		$query_sql = "SELECT pk_exp_mapping,fk_exp_datamain,col_name,source_value,target_value,sec_name FROM EXP_MAPPING, EXP_SECTION_COLS,EXP_SECTIONS  WHERE  pk_exp_sections = fk_exp_sections AND pk_exp_section_cols = fk_exp_section_cols AND target_value IS NULL AND fk_exp_definition = ".$v_exp_definition." order by fk_exp_datamain desc,sec_sequence ASC";
	} else {
		$query_sql = "SELECT pk_exp_mapping,fk_exp_datamain,col_name,source_value,target_value,sec_name FROM EXP_MAPPING, EXP_SECTION_COLS,EXP_SECTIONS  WHERE  pk_exp_sections = fk_exp_sections AND pk_exp_section_cols = fk_exp_section_cols AND fk_exp_definition = ".$v_exp_definition." and col_name = '".$v_col_name."' order by fk_exp_datamain desc,sec_sequence ASC";
	}
	
	$results = executeOCIQuery($query_sql,$ds_conn);
	$tot_rows = $results_nrows;
	echo "Total Rows: ".$tot_rows;
	echo "<INPUT type=\"hidden\" name=\"fk_exp_definition\" value=\"".$v_exp_definition."\"></input>"; 
?>
<Table border="1"><TR>
<TH>Export #</TH>
<TH>Section Name</TH>
<TH>Column Name</TH>
<TH>Source Value</TH>
<TH>Target Value</TH>
</TR>
<?php

for ($rec = 0; $rec < $tot_rows; $rec++){
	if ($v_exp_datamain == $results["FK_EXP_DATAMAIN"][$rec]) {
		echo "<TR>";
	} else {
		echo '<TR bgcolor="pink">';
	}

	echo "<TD>".$results["FK_EXP_DATAMAIN"][$rec]."</TD>"; 
	echo "<TD>".$results["SEC_NAME"][$rec]."</TD>"; 
	echo "<TD>".$results["COL_NAME"][$rec]."</TD>"; 
	echo "<TD>".$results["SOURCE_VALUE"][$rec]."</TD>"; 
	echo "<TD><INPUT size=\"50\" type=\"text\" name=\"target_value[$rec]\" value=\"".$results["TARGET_VALUE"][$rec]."\"></input></TD>";
	echo "<INPUT type=\"hidden\" name=\"pk_exp_mapping[$rec]\" value=\"".$results["PK_EXP_MAPPING"][$rec]."\"></input>" ;
	echo "</TR>";
}
?>
</TABLE>
<input type="submit" name="submit" value="Submit">
</form>
<?php } else {

$v_exp_definition = $_POST["fk_exp_definition"];
$pk_exp_mapping = $_POST["pk_exp_mapping"];
$target_value = $_POST["target_value"];

for($i=0;$i<count($pk_exp_mapping);$i++){
	if ($target_value[$i] != "") {
		$v_sql = "update exp_mapping set target_value = '".$target_value[$i]."' where pk_exp_mapping = ".$pk_exp_mapping[$i];
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);
	}
}
echo "Mapping Saved...<BR>";

OCICommit($ds_conn);
OCILogoff($ds_conn);

$url = "./cdus.php?fk_exp_definition=".$v_exp_definition;
echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";

}
?>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
