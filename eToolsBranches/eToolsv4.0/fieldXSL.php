<?PHP
class fieldXSL {
	public $fieldType;
	public $fieldDataType;
	public $fieldName;
	public $fieldMandatory;
	public $fieldId;
	public $xsl;
	public $js;
	
/*	
	function __constructor() {
		$this->xsl = "";
		$this->js = "";
	}
*/	
	public function getFieldXSL() {
		$this->xsl = "";
		$this->js = "";
		$fontClass = "";
		
		if ($this->fieldMandatory == 1) {
			$fontClass = '<font class = "mandatory">*</font>';
			if ($this->fieldDataType == "MR" || $this->fieldDataType == "MC" ) {
				$this->js = ' if (!(validate_chk_radio("'.$this->fieldName.'",formobj.'.$this->fieldId.'))) return false ; ';  
			} else {
				$this->js = ' if (!(validate_col("'.$this->fieldName.'",formobj.'.$this->fieldId.'))) return false ; ';
			}
		}
		if ($this->fieldType == "C") {
			$this->xsl .= '<td width="15%" align = "left" ><label   id = "'.$this->fieldId.'_id" >'.$this->fieldName.'</label>  &#xa0;&#xa0;&#xa0;</td>';
			$this->js = '';
		} else {
			switch ($this->fieldDataType) {
				case "ET":
					$this->xsl .= '<td width="35%" align = "left" >';
					$this->xsl .= '<label   id = "'.$this->fieldId.'_id" for = "'.$this->fieldId.'" ><b>'.$this->fieldName;
					$this->xsl .= $fontClass;
					$this->xsl .= '</b></label>  &#xa0;&#xa0;&#xa0;</td><td>';
					$this->xsl .= '<span id="'.$this->fieldId.'_span">';
					$this->xsl .= '<input  id = "'.$this->fieldId.'" name = "'.$this->fieldId.'" flddatatype = "T" type = "text" maxlength = "100" size = "20"';
					$this->xsl .= ' onChange="performAction(document.er_fillform1,document.er_fillform1.'.$this->fieldId.')"  >';
					$this->xsl .= '<xsl:attribute name="value"><xsl:value-of select="'.$this->fieldId.'"/></xsl:attribute>';
					$this->xsl .= '</input>    </span></td>';
					break;
				case "EN":
					$this->xsl .= '<td width="35%" align = "left" >';
					$this->xsl .= '<label   id = "'.$this->fieldId.'_id" for = "'.$this->fieldId.'" ><b>'.$this->fieldName;
					$this->xsl .= $fontClass;
					$this->xsl .= '</b></label>  &#xa0;&#xa0;&#xa0;</td><td>';
					$this->xsl .= '<span id="'.$this->fieldId.'_span">';
					$this->xsl .= '<input  id = "'.$this->fieldId.'" name = "'.$this->fieldId.'" flddatatype = "N" type = "text" maxlength = "100" size = "20"';
					$this->xsl .= ' onChange="performAction(document.er_fillform1,document.er_fillform1.'.$this->fieldId.')"  >';
					$this->xsl .= '<xsl:attribute name="value"><xsl:value-of select="'.$this->fieldId.'"/></xsl:attribute>';
					$this->xsl .= '</input>    </span></td>';
					$this->js .= ' if (! formobj.'.$this->fieldId.'.disabled) { if(isDecimal(formobj.'.$this->fieldId.'.value) == false) {  alert("Please enter a valid Number");  formobj.'.$this->fieldId.'.focus();  return false; } }';
					break;
				case "ED":
					$this->xsl .= '<td width="35%" align = "left" >';
					$this->xsl .= '<label   id = "'.$this->fieldId.'_id" for = "'.$this->fieldId.'" ><b>'.$this->fieldName;
					$this->xsl .= $fontClass;
					$this->xsl .= '</b></label>  &#xa0;&#xa0;&#xa0;</td><td>';
					$this->xsl .= '<span id="'.$this->fieldId.'_span">';
					$this->xsl .= '<input  id = "'.$this->fieldId.'" name = "'.$this->fieldId.'" flddatatype = "D" type = "text" size = "10"';
					$this->xsl .= ' onChange="performAction(document.er_fillform1,document.er_fillform1.'.$this->fieldId.')"  ';
					$this->xsl .= ' onBlur="performAction(document.er_fillform1,document.er_fillform1.'.$this->fieldId.')"  > ';
					$this->xsl .= '<xsl:attribute name="value"><xsl:value-of select="'.$this->fieldId.'"/></xsl:attribute>';
					$this->xsl .= '</input> ';
					$this->xsl .= '<A onClick="javascript:return fnShowCalendar(document.er_fillform1.'.$this->fieldId.')"><img src="../jsp/images/calendar.jpg" border="0" onMouseover="this.style.cursor=\'hand\'" onMouseOut="this.style.cursor=\'arrow\'"></img></A> ';
					$this->xsl .= '</span></td>';
					$this->js .= '  if (! formobj.'.$this->fieldId.'.disabled) { if (!(validate_date(formobj.'.$this->fieldId.'))) return false ; }';
					break;
				case "MD":
					$this->xsl .= '<td width="35%" align = "left" >';
					$this->xsl .= '<label   id = "'.$this->fieldId.'_id" ><b>'.$this->fieldName;
					$this->xsl .= $fontClass;
					$this->xsl .= '</b></label>  &#xa0;&#xa0;&#xa0;</td><td><span id="'.$this->fieldId.'_span">';
					$this->xsl .= '<select  id = "'.$this->fieldId.'" name = "'.$this->fieldId.'"';
					$this->xsl .= ' onChange="performAction(document.er_fillform1,document.er_fillform1.'.$this->fieldId.')"  ><option>';
					$this->xsl .= '<xsl:attribute name="value"></xsl:attribute>Select an Option</option>';
					$this->xsl .= '<xsl:for-each select = "'.$this->fieldId.'/resp"><option><xsl:attribute name="value"><xsl:value-of select="@dataval"/></xsl:attribute>';
					$this->xsl .= '<xsl:if test="@selected=\'1\'"><xsl:attribute name="selected"/></xsl:if> ';
					$this->xsl .= '<xsl:value-of select="@dispval"/></option></xsl:for-each>';
					$this->xsl .= '</select> </span></td>';
					break;
				case "MR":
					$this->xsl .= '<xsl:variable name = "colcount" select = "'.$this->fieldId.'/@colcount" />';
					$this->xsl .= '<td width="35%" align = "left" >';
					$this->xsl .= '<label   id = "'.$this->fieldId.'_id" ><b>'.$this->fieldName;
					$this->xsl .= $fontClass;
					$this->xsl .= '</b></label>  &#xa0;&#xa0;&#xa0;</td><td><table width="100%" border = "0"><tr><td>';
					$this->xsl .= '<span id="'.$this->fieldId.'_span">';
					$this->xsl .= '<table width="100%">';
					$this->xsl .= '<xsl:for-each select = "'.$this->fieldId.'/resp"><td width="100%">';
					$this->xsl .= '<input  name = "'.$this->fieldId.'" type = "radio" ';
					$this->xsl .= ' ondblclick= "this.checked = false; performAction(document.er_fillform1,document.er_fillform1.'.$this->fieldId.'); "';
					$this->xsl .= ' onClick="performAction(document.er_fillform1,document.er_fillform1.'.$this->fieldId.')"  >';
					$this->xsl .= '<xsl:attribute name="value"><xsl:value-of select="@dataval"/></xsl:attribute>';
					$this->xsl .= '<xsl:attribute name="id"><xsl:value-of select="@dispval"/></xsl:attribute>';
					$this->xsl .= '<xsl:if test="@checked=\'1\'"><xsl:attribute name="checked"/></xsl:if>';
					$this->xsl .= '<xsl:value-of select="@dispval"/></input></td>';
					$this->xsl .= '<xsl:if test = "number(position() mod $colcount)=0"><tr></tr></xsl:if>';
					$this->xsl .= '</xsl:for-each></table></span></td></tr></table></td>';
					break;
				case "MC":
					$this->xsl .= '<xsl:variable name = "colcount" select = "'.$this->fieldId.'/@colcount" />';
					$this->xsl .= '<td width="35%" align = "left" >';
					$this->xsl .= '<label   id = "'.$this->fieldId.'_id" ><b>'.$this->fieldName;
					$this->xsl .= $fontClass;
					$this->xsl .= '</b></label>  &#xa0;&#xa0;&#xa0;</td><td><table width="100%" border = "0"><tr><td>';
					$this->xsl .= '<span id="'.$this->fieldId.'_span">';
					$this->xsl .= '<table width="100%">';
					$this->xsl .= '<xsl:for-each select = "'.$this->fieldId.'/resp"><td width="100%">';
					$this->xsl .= '<input  name = "'.$this->fieldId.'" type = "checkbox" ';
					$this->xsl .= ' onClick="performAction(document.er_fillform1,document.er_fillform1.'.$this->fieldId.')"  >';
					$this->xsl .= '<xsl:attribute name="value"><xsl:value-of select="@dataval"/></xsl:attribute>';
					$this->xsl .= '<xsl:attribute name="id"><xsl:value-of select="@dispval"/></xsl:attribute>';
					$this->xsl .= '<xsl:if test="@checked=\'1\'"><xsl:attribute name="checked"/></xsl:if>';
					$this->xsl .= '<xsl:value-of select="@dispval"/></input></td><xsl:if test = "number(position() mod $colcount)=0"><tr></tr></xsl:if>';
					$this->xsl .= '</xsl:for-each></table></span></td></tr></table></td>';
					break;
			}
		}


	}


}

/*
Mandatory Field Javascript:
 if (!(validate_col("Sample text field",formobj.fld24541994_72714_115110))) return false ;


Data entry date:
<td width='15%' align = "left" ><label   id = "er_def_date_01_id" for = "er_def_date_01" >Data Entry Date<font class = "mandatory">*</font></label>  &#xa0;&#xa0;&#xa0;</td><td><span id="er_def_date_01_span"><input  id = "er_def_date_01" name = "er_def_date_01" flddatatype = "D" type = "text" size = "10" onChange="performAction(document.er_fillform1,document.er_fillform1.er_def_date_01)"  onBlur="performAction(document.er_fillform1,document.er_fillform1.er_def_date_01)"  ><xsl:attribute name="value"><xsl:value-of select="er_def_date_01"/></xsl:attribute></input><A onClick="javascript:return fnShowCalendar(document.er_fillform1.er_def_date_01)"><img src="../jsp/images/calendar.jpg" border="0" onMouseover="this.style.cursor='hand'" onMouseOut="this.style.cursor='arrow'"></img></A>    </span></td>
 if (!(validate_col("Data Entry Date",formobj.er_def_date_01))) return false ; if (! formobj.er_def_date_01.disabled) { if (!(validate_date(formobj.er_def_date_01))) return false ; }
 
Text field:
<td width='15%' align = "left" ><label   id = "fld24541994_72714_115110_id" for = "fld24541994_72714_115110" >Sample text field<font class = "mandatory">*</font></label>  &#xa0;&#xa0;&#xa0;</td><td><span id="fld24541994_72714_115110_span"><input  id = "fld24541994_72714_115110" name = "fld24541994_72714_115110" flddatatype = "T" type = "text" maxlength = "100" size = "20" onChange="performAction(document.er_fillform1,document.er_fillform1.fld24541994_72714_115110)"  ><xsl:attribute name="value"><xsl:value-of select="fld24541994_72714_115110"/></xsl:attribute></input>    </span></td>

 
Number field:
 <td width='15%' align = "left" ><label   id = "fld24541994_72715_115111_id" for = "fld24541994_72715_115111" >Sample number field</label>  &#xa0;&#xa0;&#xa0;</td><td><span id="fld24541994_72715_115111_span"><input  id = "fld24541994_72715_115111" name = "fld24541994_72715_115111" flddatatype = "N" type = "text" maxlength = "100" size = "20" onChange="performAction(document.er_fillform1,document.er_fillform1.fld24541994_72715_115111)"  ><xsl:attribute name="value"><xsl:value-of select="fld24541994_72715_115111"/></xsl:attribute></input>    </span></td>
 if (! formobj.fld24541994_72715_115111.disabled) { if(isDecimal(formobj.fld24541994_72715_115111.value) == false) {  alert("Please enter a valid Number");  formobj.fld24541994_72715_115111.focus();  return false; } }


Date field:
<td width='15%' align = "left" ><label   id = "fld24541994_72716_115112_id" for = "fld24541994_72716_115112" >Sample date field</label>  &#xa0;&#xa0;&#xa0;</td><td><span id="fld24541994_72716_115112_span"><input  id = "fld24541994_72716_115112" name = "fld24541994_72716_115112" flddatatype = "D" type = "text" size = "10" onChange="performAction(document.er_fillform1,document.er_fillform1.fld24541994_72716_115112)"  onBlur="performAction(document.er_fillform1,document.er_fillform1.fld24541994_72716_115112)"  ><xsl:attribute name="value"><xsl:value-of select="fld24541994_72716_115112"/></xsl:attribute></input><A onClick="javascript:return fnShowCalendar(document.er_fillform1.fld24541994_72716_115112)"><img src="../jsp/images/calendar.jpg" border="0" onMouseover="this.style.cursor='hand'" onMouseOut="this.style.cursor='arrow'"></img></A>    </span></td>
 if (! formobj.fld24541994_72716_115112.disabled) { if (!(validate_date(formobj.fld24541994_72716_115112))) return false ; }

Comment field:
<td width='15%' align = "left" ><label   id = "fld24541994_72717_115113_id" >Sample comment field</label>  &#xa0;&#xa0;&#xa0;</td>


Dropdown field:
<td width='15%' align = "left" ><label   id = "fld24541994_72718_115114_id" >Sample drop down field</label>  &#xa0;&#xa0;&#xa0;</td><td><span id="fld24541994_72718_115114_span"><select  id = "fld24541994_72718_115114" name = "fld24541994_72718_115114" onChange="performAction(document.er_fillform1,document.er_fillform1.fld24541994_72718_115114)"  ><option><xsl:attribute name="value"></xsl:attribute>Select an Option</option><xsl:for-each select = "fld24541994_72718_115114/resp"><option><xsl:attribute name="value"><xsl:value-of select="@dataval"/></xsl:attribute><xsl:if test="@selected='1'"><xsl:attribute name="selected"/></xsl:if>  <xsl:value-of select="@dispval"/></option></xsl:for-each></select> </span></td>


Radio button field:
<xsl:variable name = "colcount" select = "fld24541994_72719_115115/@colcount" /><td width='15%' align = "left" ><label   id = "fld24541994_72719_115115_id" >Sample radio button field</label>  &#xa0;&#xa0;&#xa0;</td><td><table width="100%" border = "0"><tr><td><span id="fld24541994_72719_115115_span"><table width="100%"><xsl:for-each select = "fld24541994_72719_115115/resp"><td width="100%"><input  name = "fld24541994_72719_115115" type = "radio" ondblclick= "this.checked = false; performAction(document.er_fillform1,document.er_fillform1.fld24541994_72719_115115); " onClick="performAction(document.er_fillform1,document.er_fillform1.fld24541994_72719_115115)"  ><xsl:attribute name="value"><xsl:value-of select="@dataval"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="@dispval"/></xsl:attribute><xsl:if test="@checked='1'"><xsl:attribute name="checked"/></xsl:if>  <xsl:value-of select="@dispval"/></input></td><xsl:if test = "number(position() mod $colcount)=0"><tr></tr></xsl:if></xsl:for-each></table></span></td></tr></table></td>

Checkbox field:
<xsl:variable name = "colcount" select = "fld24541994_72720_115116/@colcount" /><td width='15%' align = "left" ><label   id = "fld24541994_72720_115116_id" >Sample checkbox field</label>  &#xa0;&#xa0;&#xa0;</td><td><table width="100%" border = "0"><tr><td><span id="fld24541994_72720_115116_span"><table width="100%"><xsl:for-each select = "fld24541994_72720_115116/resp"><td width="100%"><input  name = "fld24541994_72720_115116" type = "checkbox" onClick="performAction(document.er_fillform1,document.er_fillform1.fld24541994_72720_115116)"  ><xsl:attribute name="value"><xsl:value-of select="@dataval"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="@dispval"/></xsl:attribute><xsl:if test="@checked='1'"><xsl:attribute name="checked"/></xsl:if>  <xsl:value-of select="@dispval"/></input></td><xsl:if test = "number(position() mod $colcount)=0"><tr></tr></xsl:if></xsl:for-each></table></span></td></tr></table></td>
 */
 
 ?>