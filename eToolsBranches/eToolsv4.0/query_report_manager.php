<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Query Manager</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 



?>
<script>
function reportPreview(reportPk){
	var win = "report_preview.php?reportPk="+reportPk;
	window.open(win,'mywin',"toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=825,height=600,top=90,left=150");
}


</script>
</head>


<body>

<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	

<div class="navigate">Published Reports</div>

<?php
if (isset($_GET["type"])) {
	echo '<a href="query_report_manager.php">Display Reports</a>';
} else { 
	echo '<a href="query_report_manager.php?type=hidden">Display Hidden Reports</a>'; 
}

	//echo '<a href="copy_standard_velos_report.php" style="padding-left:10px;">Copy a Standard Velos Report</a>';

?>

<Table border="1" width="100%"><TR>
<TH width="15%">REPORT TYPE</TH>
<TH width="25%">REPORT NAME</TH>
<TH width="40%">DESCRIPTION</TH>
<TH width="5%">&nbsp;</TH>
<TH width="5%">&nbsp;</TH>
<TH width="5%">&nbsp;</TH>
</TR>
<?php

$v_repHide = isset($_GET["type"])?"Y":"N";

$query_sql = "select (select codelst_desc from er_codelst where codelst_type = 'report' and codelst_subtyp = 'rep_type') as rep_type, rep_name, pk_report,rep_desc,rep_hide from er_report where (rep_type like 'rep_cstm%' or pk_report > 99999)  and rep_hide = '$v_repHide' order by 1,2" ;
$results = executeOCIQuery($query_sql,$ds_conn);

for ($rec = 0; $rec < $results_nrows; $rec++){
	$v_hide = ($results["REP_HIDE"][$rec] == 'Y'? '<font color=gray >': '');
?>
	<TR onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
	<TD><?php echo $v_hide.$results["REP_TYPE"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $v_hide.$results["REP_NAME"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["REP_DESC"][$rec] . "&nbsp;"; ?></TD>
<?php
	echo "<td align=center><a href=query_report_edit.php?mode=sql&pk_report=".$results["PK_REPORT"][$rec].">Edit SQL</a></td>";
	echo "<td align=center><a href=query_report_edit.php?mode=xsl&pk_report=".$results["PK_REPORT"][$rec].">Edit XSL</a></td>";
	echo '<td><a href=# onclick="reportPreview('.$results["PK_REPORT"][$rec].')">Preview</a></td>';
	echo "</TR>";
}
?>
</TABLE>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
