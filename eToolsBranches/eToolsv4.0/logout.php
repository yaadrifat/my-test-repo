<?php
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Clear session variable and redirect
if(@$_SESSION["user"]){
	$_SESSION["user"] = false;
	session_destroy();
}
header("location: index.php?logout=1");
?>