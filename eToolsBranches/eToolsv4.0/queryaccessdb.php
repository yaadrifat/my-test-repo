<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Query Database</title>
</head>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
$_SESSION['query'] = "";

if (isset($_GET["pk_vlnk_odbc"])) $v_pk_vlnk_odbc = $_GET["pk_vlnk_odbc"];
if (isset($_POST["pk_vlnk_odbc"])) $v_pk_vlnk_odbc = $_POST["pk_vlnk_odbc"];

//$v_pk_vlnk_odbc = $_GET["pk_vlnk_odbc"];
$query = "select odbc_name,odbc_dsn,odbc_user,odbc_pass from velink.vlnk_odbc where pk_vlnk_odbc = ".$v_pk_vlnk_odbc;
$results = executeOCIQuery($query,$ds_conn);

?>
<body>
<div id="fedora-content">	
<div class="navigate">Import Access DB</div>

<FORM action="queryaccessdb.php" method=post>
<INPUT type="hidden" name="pk_vlnk_odbc" value="<?PHP echo $v_pk_vlnk_odbc; ?>"/>

<?php
$connaccess=odbc_connect($results["ODBC_DSN"][0],$results["ODBC_USER"][0],$results["ODBC_PASS"][0]);

if ($connaccess) {
	$result = odbc_tables($connaccess);

		echo '<table border="1">';
		echo "<tr><th>Table Name</th><th>TYPE</th><th>&nbsp;</th><th>&nbsp;</th></tr>";
	   while (odbc_fetch_row($result)){
	     if(odbc_result($result,"TABLE_TYPE")=="TABLE")
			echo '<tr>';
	       echo"<td>".odbc_result($result,"TABLE_NAME")."</td>";
	       echo"<td>".odbc_result($result,"TABLE_TYPE")."</td>";
	       echo"<td><a href='queryaccesstable.php?table_name=".odbc_result($result,"TABLE_NAME")."&pk_vlnk_odbc=".$v_pk_vlnk_odbc."'>View</a></td>";
	       echo"<td><a href='accesstable_export.php?table_name=".odbc_result($result,"TABLE_NAME")."&pk_vlnk_odbc=".$v_pk_vlnk_odbc."'>Export</a></td>";
			echo '</tr>';
	   }
		echo '</table';


	if (isset($_POST['query']) ) {
	$v_query =  $_POST['query'];
	} else {
	if (isset($_SESSION['query']) ) {
	$v_query =  $_SESSION['query'];
	} else {
	$v_query =  "";
	}
	}
	?>
	<table width="100%" border="1">
	       <tr>

	<td>Query: </td>
	</tr>
	<tr>
	<td colspan="4"><Textarea name="query" type="text" cols=90 maxlength=10000 rows=10><?PHP echo $v_query; ?></textarea>
	</td>
	</tr><tr>
	<td><input type=submit name=submit></input></td>
	</tr>
	</table>


	</form>

	<?php
	if (isset($_POST['submit'])) {	

	?>

	<?php

	$query=$_POST['query'];

	$rs=odbc_exec($connaccess,$query);


	echo '<table border="1">';
	echo "<tr>";
		for ($i=1;$i<=odbc_num_fields($rs);$i++){
	  		echo "<th>".odbc_field_name($rs,$i)."[".odbc_field_type($rs,$i)." (".odbc_field_precision($rs,$i).")]"."</th>";
	      }
	echo "</tr>";

	$rows = 0;
	while (odbc_fetch_row($rs))
	{
	echo "<tr>";
		for ($i=1;$i<=odbc_num_fields($rs);$i++){
	  		echo "<td>".odbc_result($rs,$i)."</td>";
	      }
	echo "</tr>";
	$rows = $rows + 1;
	if ($rows > 100){exit;}

	}
	odbc_close($connaccess);

	?>
	</TABLE>
</div>


</body>
</html>
<?php
	}
OCICommit($ds_conn);
OCILogoff($ds_conn);
} else {echo odbc_errormsg();}
}
else header("location: index.php?fail=1");
?>
