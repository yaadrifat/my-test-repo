<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
<link type="text/css" rel="stylesheet" href="./styles/common.css">
    <title>Velos eTools -> Modify Form XSL</title>

<?php
include("./includes/header.php");
include_once "./adodb/adodb.inc.php";


$db = NewADOConnection("oci8");
$db->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);

/*
$v_output = "";
$v_retval = "";
exec("c:\temp\tidy.exe -xml -i -u -q -w 200 c:\temp\test.xsl",$v_output,$v_retval);
echo count($v_output);
foreach($v_output as $outputline){
	echo $outputline;
}
*/

?>
<style>
textarea
{
  font-size: 12px;
  color: #0000ff;
  font-family: courier
}



</style>
</head>


<body>

<div id="fedora-content">	

<?PHP


if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
$v_pk_formlib = $_GET["formPk"];
$v_type  = $_GET["type"];
switch ($v_type) {
case "xsl":
	echo '<div class="navigate">Modify Form XSL</div>';
	$v_sql = "SELECT form_xsl from er_formlib where pk_formlib = ".$v_pk_formlib;
	break;
case "viewxsl":
	echo '<div class="navigate">Modify Print Format XSL</div>';
	$v_sql = "SELECT form_viewxsl from er_formlib where pk_formlib = ".$v_pk_formlib;
	break;
case "xml":
	echo '<div class="navigate">View Form XML</div>';
	$v_sql = "SELECT a.form_xml.getclobval() from er_formlib a where pk_formlib = ".$v_pk_formlib;
	break;
}

$recordSet = $db->Execute($v_sql);
$v_xsl =  $recordSet->fields[0];
$doc = new domDocument;
$doc->preserveWhiteSpace = false;
$doc->encoding = 'utf-8';
$doc->loadXML($v_xsl);		
$doc->formatOutput = true;
$v_xsl = $doc->saveXML();
?>
<Form name="modifyxsl" method="post" action="form_modifyxsl.php">

<input name="pk_formlib" type="hidden" value="<?PHP echo $v_pk_formlib ?>"/>
<?PHP
echo "Form Name: ".$_GET["formname"];

?>
<table>
<tr>
<td><textarea name="xsl" rows=30 cols=100><?PHP echo htmlentities($v_xsl); ?></textarea></td>
</tr>
</table>

<input type="hidden" name="formname" value="<?PHP echo $_GET["formname"];?>"/>
<input type="hidden" name="frmstat" value="<?PHP echo $_GET["formstat"];?>"/>
<input type="hidden" name="linkedto" value="<?PHP echo $_GET["linkedto"];?>"/>
<input type="hidden" name="type" value="<?PHP echo $v_type;?>"/>

<?PHP
if ($v_type != "xml") {
?>
<!-- <BR	><b>Make a copy of XSL <input type="checkbox" name="savecopy" /></b> 
<BR> -->
<BR><input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" /> 
<?PHP
}
?>

</Form>
<?PHP
} else {
$v_xsl = html_entity_decode(htmlspecialchars($_POST["xsl"]));
$v_pk_formlib = $_POST["pk_formlib"];
$v_formstat = $_POST["frmstat"];
$v_linkto = $_POST["linkedto"];
$v_type = $_POST["type"];
if ($v_type == 'xsl') {
	$v_libcolname = "form_xsl";
	$v_libvercolname = "formlibver_xsl";
} else if ($v_type == 'viewxsl') {
	$v_libcolname = "form_viewxsl";
	$v_libvercolname = "formlibver_viewxsl";
}

$v_sql = "select pk_formcustomization from velink.vlnk_formcustomization where fk_formlib = $v_pk_formlib";
$recordSet = $db->execute($v_sql);
$v_pk_formcustomization = 0;
while (!$recordSet->EOF) {
	$v_pk_formcustomization = $recordSet->fields[0];
	break;
}
if ($v_pk_formcustomization == 0) {
	$v_sql = "select velink.seq_vlnk_formcustomization.nextval from dual";
	$recordSet = $db->Execute($v_sql);
	$v_pk_formcustomization = $recordSet->fields[0];
	$db->Execute("insert into velink.vlnk_formcustomization (pk_formcustomization,fk_formlib,orig_xsl,orig_viewxsl) values ($v_pk_formcustomization,$v_pk_formlib,(select form_xsl from er_formlib where pk_formlib = $v_pk_formlib),(select form_viewxsl from er_formlib where pk_formlib = $v_pk_formlib))");
}

/*$v_sql = "SELECT max(pk_formlibver) as pk_formlibver from er_formlibver where fk_formlib = ".$v_pk_formlib;
$recordSet = $db->Execute($v_sql);
$v_pk_formlibver =  $recordSet->fields[0];
$v_where = "pk_formlib = ".$v_pk_formlib;
$v_status = $db->UpdateClob('er_formlib',$v_libcolname,$v_xsl,$v_where);
if (!$v_status) print $db->ErrorMsg();
$v_where = "pk_formlibver = ".$v_pk_formlibver;*/

$v_where = "pk_formlib = ".$v_pk_formlib;

$v_status = $db->UpdateClob('er_formlib',$v_libcolname,$v_xsl,$v_where);
if (!$v_status) print $db->ErrorMsg();

$v_sql = "SELECT max(pk_formlibver) as pk_formlibver from er_formlibver where fk_formlib = ".$v_pk_formlib;
$recordSet = $db->Execute($v_sql);
$v_pk_formlibver =  $recordSet->fields[0];
//$v_where = "pk_formlibver = ".$v_pk_formlibver;
$v_where = "PK_FORMLIBVER = ".$v_pk_formlibver;

if (!empty($v_pk_formlibver)){
	$v_status = $db->UpdateClob('er_formlibver',$v_libvercolname,$v_xsl,$v_where);
	if (!$v_status) print $db->ErrorMsg();
}			
$db->Close();
echo "Form customization saved.";
$url = "./form_customization.php?formname=".$_POST["formname"]."&frmstat=".$v_formstat."&linkedto=".$v_linkto;
echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";
}
?>
</div>
</body>
</html>
<?php

$db->Close();
}
else header("location: ./index.php?fail=1");
?>
