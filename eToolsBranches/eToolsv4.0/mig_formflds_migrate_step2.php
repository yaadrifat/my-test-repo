<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

?>
<html>
<head>
<title>Velos eTools -> Migrate Form Data - Migrate</title>
</head>
<body>
<?PHP
// Check that we are logged in and an admin
if(@$_SESSION["user"]){
include("./includes/oci_functions.php");
include("./includes/header.php");
?>
<div id="fedora-content">	
	
<div class="navigate">Migrate Data - Forms - Migrate</div>

<?PHP
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

if (isset($_POST["pk_vlnk_adaptor"])) $pk_vlnk_adaptor = $_POST["pk_vlnk_adaptor"];
if (isset($_POST["pk_vlnk_adapmod"])) $pk_vlnk_adapmod = $_POST["pk_vlnk_adapmod"];
	$query_sql = "select nvl(record_type,'1') as record_type,fk_form,table_name from velink.vlnk_adapmod where pk_vlnk_adapmod =  ".$pk_vlnk_adapmod;
	$results = executeOCIQuery($query_sql,$ds_conn);
	
	
	if ($results["RECORD_TYPE"][0] == 1 ) {
		$v_tablename = $results["TABLE_NAME"][0];
		$select_sql = "select fk_field,table_colname,pk_imp_formfld 
			from er_mapform, velink.vlnk_imp_formfld
			where fk_form = ".$results["FK_FORM"][0]." and 
			mp_pkfld = fk_field and 
			mp_flddatatype = 'MC' and
			table_colname is not null and 
			fk_vlnk_adapmod = ".$_POST["pk_vlnk_adapmod"];
			$results = executeOCIQuery($select_sql,$ds_conn);
			$v_trows = $results_nrows;
		for ($rec = 0; $rec < $v_trows; $rec++){
		$v_sql = "delete from velink.vlnk_imp_formresp where fk_imp_formfld = ".$results["PK_IMP_FORMFLD"][$rec];
		$results_1 = executeOCIUpdateQuery($v_sql,$ds_conn);
		$v_sql = "select distinct ".$results["TABLE_COLNAME"][$rec]." as cbdata from ".$v_tablename." where ".$results["TABLE_COLNAME"][$rec]." is not null";
		$results_1 = executeOCIQuery($v_sql,$ds_conn);
		$v_trows_1 = $results_nrows;
			for ($rec_1 = 0; $rec_1 < $v_trows_1; $rec_1++){
				$v_cbdata = $results_1["CBDATA"][$rec_1];
				$v_cbdata = str_replace("'","''",$v_cbdata);
				$v_cbdata = str_replace("[","'",$v_cbdata);
			
				$v_cbdata = trim(strtolower(str_replace("]","'",$v_cbdata)));
				if (substr($v_cbdata,0,1) != "'") $v_cbdata = "'".$v_cbdata;
				if (substr($v_cbdata,strlen($v_cbdata)-1,1) != "'") $v_cbdata = $v_cbdata."'";
				
				$v_sql = "select fldresp_dispval,fldresp_dataval from er_fldresp where fk_field = ".$results["FK_FIELD"][$rec]." and lower(fldresp_dispval) in (".$v_cbdata.")";
				$results_2 = executeOCIQuery($v_sql,$ds_conn);
				$v_trows_2 = $results_nrows;
				$v_dispval = "";
				$v_dataval = "";
				for ($rec_2 = 0; $rec_2 < $v_trows_2; $rec_2++){
					if ($rec_2 == 0) {
						$v_dispval .= '['.$results_2["FLDRESP_DISPVAL"][$rec_2].']';
						$v_dataval .= '['.$results_2["FLDRESP_DATAVAL"][$rec_2].']';
					} else {
						$v_dispval .= ',['.$results_2["FLDRESP_DISPVAL"][$rec_2].']';
						$v_dataval .= '[VELCOMMA]['.$results_2["FLDRESP_DATAVAL"][$rec_2].']';
					}
				}

				if (!empty($v_dispval) && !empty($v_dataval)) {
					$v_sql = "insert into velink.vlnk_imp_formresp values (velink.seq_vlnk_imp_formresp.nextval,".$results["PK_IMP_FORMFLD"][$rec].",'".str_replace("'","''",$results_1["CBDATA"][$rec_1])."','".str_replace("'","''",$results_1["CBDATA"][$rec_1])."','".str_replace("'","''",$v_dispval."[VELSEP1]".$v_dataval)."')";
					$results_3 = executeOCIUpdateQuery($v_sql,$ds_conn);
				}
			}
		}
	
		$query_sql = "begin velink.pkg_velink_import.sp_imp_formdata(".$pk_vlnk_adapmod."); end;";
		$results = executeOCIUpdateQuery($query_sql,$ds_conn);
		echo "Form data moved to staging area...";
	}
		$query_sql = "update velink.vlnk_adapmod set record_type = '0' where pk_vlnk_adapmod = $pk_vlnk_adapmod";
		$results = executeOCIUpdateQuery($query_sql,$ds_conn);
		$url = "mig_forms.php?pk_vlnk_adaptor=".$pk_vlnk_adaptor."&refresh=0";
		echo "<meta http-equiv=\"refresh\" content=\"1; url=./".$url."\">";
} else header("location: ./index.php?fail=1");
?>
</div>
</body>
</html>