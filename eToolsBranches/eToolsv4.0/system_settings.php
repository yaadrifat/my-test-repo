<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
<script language="javascript">
	function showPatstd(str)
	{
	if (str=="")
	  {
	  document.getElementById("patstd").innerHTML="";
	  return;
	  }
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
		document.getElementById("patstd").innerHTML=xmlhttp.responseText;
		}
	  }
	xmlhttp.open("GET","system_settings_ajax.php?q="+str,true);
	xmlhttp.send();
	} 
</script>
    <title>Velos eTools -> System Settings</title>

<?php
include("./includes/oci_functions.php");	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body onLoad="showPatstd(document.pats.users.value);">

<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	


<?php

$query_sql = "SELECT to_char(MAX(fired_on),'dd-Mon-yyyy') as last_updated FROM TRACK_PATCHES";
$results = executeOCIQuery($query_sql,$ds_conn);
$last_updated = $results["LAST_UPDATED"][0];

$query_sql = "SELECT pk_ctrltab,ctrl_key,
DECODE(ctrl_key,'app_version','Application Version',
									 'site_url','Site URL',
									 'mailsettings','SMTP Server',
									 'vmailuser','SMTP User',
									 'vmailpass','SMTP Password',
									 'eresuser','Administrator email',
									 'irb_app_stat','IRB Status',
									 'icd','ICD9 Lookup') AS ctrl_key_desc,
DECODE(ctrl_key,'app_version','01',
									 'site_url','02',
									 'mailsettings','03',
									 'vmailuser','04',
									 'vmailpass','05',
									 'eresuser','06',
									 'irb_app_stat','07',
									 'icd','08') AS ctrl_key_seq,
ctrl_desc,ctrl_value
FROM ER_CTRLTAB
WHERE ctrl_key IN ('app_version','eresuser','ICD','irb_app_stat','mailsettings','site_url','vmailpass','vmailuser')
UNION
SELECT pk_codelst,trim(codelst_type),'Database timezone' AS ctrl_key_desc,'09',trim(codelst_subtyp),(select replace(tz_name,'  ','') from esch.sch_timezones where pk_tz = codelst_subtyp and rownum = 1) as codelst_desc FROM sch_codelst WHERE codelst_type = 'DBTimezone'
ORDER BY ctrl_key_seq";

$results = executeOCIQuery($query_sql,$ds_conn);
echo '<div class="navigate">System Settings</div>';
echo '<table  width="100%" border="1">';
echo '<TR><TH width="25%">Name</TH>';
//echo '<TH width="20%">Description</TH>';
echo '<TH width="20%">Value</TH>';
echo '<TH width="10%">&nbsp;</TH></TR>';
for ($rec = 0; $rec < $results_nrows; $rec++){
	echo '<TR onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';">';
	echo '<TD>'.$results["CTRL_KEY_DESC"][$rec].'</TD>';
//	echo '<TD>'.$results["CTRL_DESC"][$rec].'</TD>';
	if ($results["CTRL_KEY_SEQ"][$rec] == '01') {
		echo '<TD align="center"><font color=red size="5"><B>'.$results["CTRL_VALUE"][$rec].'</B></font></TD>';
		echo '<TD align="center"><B>'.$last_updated.'</B></TD>';
	} else {
		echo '<TD>'.$results["CTRL_VALUE"][$rec].'</TD>';
		echo '<TD>'.'<a href=system_settings_edit.php?pk='.$results["PK_CTRLTAB"][$rec].'&key='.$results["CTRL_KEY"][$rec].'&key_desc='.urlencode($results["CTRL_KEY_DESC"][$rec]).'&ctrl_desc='.urlencode($results["CTRL_DESC"][$rec]).'&ctrl_value='.urlencode($results["CTRL_VALUE"][$rec]).'>Edit</a>'.'</TD>';
	}

	echo '</TR>';
}
$usr = "SELECT pk_user,usr_firstname || ' ' || usr_lastname AS username FROM ER_USER WHERE USR_STAT = 'A' ORDER BY lower(usr_firstname || ' ' || usr_lastname)";
$results = executeOCIQuery($usr,$ds_conn);
$users = "";

$selusr = $_GET['u'];
for ($rec = 0; $rec < $results_nrows; $rec++){
	$results["USERNAME"][$rec] = str_replace(array("<script>","</script>"),array("",""),$results["USERNAME"][$rec]);
	if($selusr==$results["PK_USER"][$rec]){
		$users .= "<option value=".$results["PK_USER"][$rec]." selected >".$results["USERNAME"][$rec]."</option>";	
	}else{
		$users .= "<option value=".$results["PK_USER"][$rec].">".$results["USERNAME"][$rec]."</option>";	
	}
}

$ctr = "SELECT CTRL_VALUE from ER_CTRLTAB where CTRL_KEY='use_disp'";
$ctrresults = executeOCIQuery($ctr,$ds_conn);
if($ctrresults["CTRL_VALUE"][0]=="1"){
	$ctrval = "Display Value";
}else{
	$ctrval = "Data Value";
}

$ape = "SELECT * FROM ER_SETTINGS WHERE SETTINGS_KEYWORD='ACCELARATED_ENROLLMENT_CONFIG'";
$aperesults = executeOCIQuery($ape,$ds_conn);
$rowcount = $results_nrows;
if($rowcount!=0){
	if($aperesults["SETTINGS_VALUE"][0]=='1'){
		$apeval = "On";
	}else{
		$apeval = "Off";
	}
}else{
	$apeval = "Off";
}
echo '<tr onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';"><td width="25%">Interfield Action </td><td width="20%">'.$ctrval.'</td><td width="10%"><a href="system_settings_edit.php?re=ia&ct='.$ctrresults["CTRL_VALUE"][0].'">Edit</a></td></tr>';
echo '<FORM name="pats" method=post >';
echo '<TR bgColor=\'#d9d9d9\';><TD colspan=3><b>Select Users</b> - <SELECT name="users" onChange="showPatstd(document.pats.users.value)">'.$users.'</SELECT></TD></TR>';
echo '</form>';
echo "</table>";
echo '<div id="patstd"></div>';
?>

</div>
</body>
</html>
<?php
OCILogoff($ds_conn);
}
else header("location: ./index.php?fail=1");
?>



