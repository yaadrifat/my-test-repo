<?php
	
	
session_start();	// Maintain session state
//require_once "./includes/session.php";
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<style type="text/css">
	.popUp { position: absolute; top: 1000px; left: 200px; text-align: center; padding: 5px; border: 1px solid black; background: white; }
</style>
<head>
<title>Velos eTools -> Copy Standard Velos Report</title>
<script language="javascript" type="text/javascript" src="./js/popup.js"></script>
<script language="javascript" type="text/javascript">
	function getHTTPObject(){
		if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
			else if (window.XMLHttpRequest) return new XMLHttpRequest();
		else {
		  alert("Your browser does not support AJAX.");
			return null;
		}
	}
	
	function display_search_report_name(codelst_custom_col){
		if(codelst_custom_col != 'Select Report') {
			document.getElementById('search_report_name').style.display="block";
			document.getElementById('show_note').style.display="block";
			document.getElementById('view_report_name').style.display="none";
		} else {
			document.getElementById('search_report_name').style.display="none";
			document.getElementById('show_note').style.display="none";
			document.getElementById('view_report_name').style.display="none";
		}
	}
	
	function call_report_name(codelst_custom_col, report_name) {
		httpObject = getHTTPObject();
		if (httpObject != null) {
			httpObject.open("GET", "copy_standard_velos_report_ajax.php?codelst_custom_col="+codelst_custom_col+"&report_name="+report_name, false);	 
			httpObject.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
			httpObject.setRequestHeader("Cache-Control", "no-cache");
			httpObject.send(null);
			if(httpObject.status != 200){document.write("404 not found");}
			var finalString = httpObject.responseText;
			if (finalString != "") {
				document.getElementById('view_report_name').style.display="block";			
				document.getElementById('view_report_name').innerHTML = finalString;
				return false;
			}
		}
	}

	var httpObject = null;
	
	var pk_report = '';
	function select_report_name(report_name_id) {
	//alert(report_name_id);
		document.getElementById('new_report_name').value = '';
		document.getElementById('error_new_report_name').style.display = "none";
		document.getElementById('copy_new_report_name').style.display = "none";
		pk_report = report_name_id;
	}
	
	function copy_new_report_name(new_report_name) {
		if(document.getElementById('new_report_name').value == '') {
			document.getElementById('error_new_report_name').style.display = "block";
			document.getElementById('copy_new_report_name').style.display = "none";
			document.getElementById('error_new_report_name').innerHTML = "Please enter report name";
			document.getElementById('new_report_name').focus();
			return false;
		}
		httpObject = getHTTPObject();
		if (httpObject != null) {
			httpObject.open("GET", "copy_standard_velos_report_ajax.php?new_report_name="+new_report_name+"&pk_report="+pk_report, false);	 
			httpObject.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
			httpObject.setRequestHeader("Cache-Control", "no-cache");
			httpObject.send(null);
			if(httpObject.status != 200){document.write("404 not found");}
			var finalString = httpObject.responseText;
			if (finalString != "") {
				document.getElementById('error_new_report_name').style.display = "none";
				document.getElementById('copy_new_report_name').style.display = "block";			
				document.getElementById('copy_new_report_name').innerHTML = finalString;
				return false;
			}
		}
	}
	
</script>
</head>
<?php
	include("./includes/header.php");
	include("./includes/oci_functions.php");
	include ("./txt-db-api.php");
	$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]);
?>
<body> 
<div id="fedora-content">	
<?php
	//$v_sql = "SELECT CODELST_CUSTOM_COL FROM ER_CODELST WHERE codelst_type = 'report' AND CODELST_SUBTYP != 'rep_cstm' AND CODELST_SUBTYP != 'rep_adhoc' AND CODELST_SUBTYP != 'rep_qmgmt' AND FK_ACCOUNT IS NULL GROUP BY CODELST_CUSTOM_COL ORDER BY CODELST_CUSTOM_COL ASC";
	$v_sql = "SELECT CODELST_CUSTOM_COL FROM ER_CODELST WHERE codelst_type = 'report' AND CODELST_SUBTYP != 'rep_cstm' AND FK_ACCOUNT IS NULL AND CODELST_SEQ IS NOT NULL AND CODELST_HIDE = 'N' GROUP BY CODELST_CUSTOM_COL ORDER BY CODELST_CUSTOM_COL ASC";
	$results = executeOCIQuery($v_sql,$ds_conn);
	$standard_report = "";
	$standard_report.= "<option value = 'Select Report'>Select Report</option>";
	for ($rec = 0; $rec < $results_nrows; $rec++){
		if(trim($results["CODELST_CUSTOM_COL"][$rec]) != '') {
			if($standard_report == $results["CODELST_CUSTOM_COL"][$rec]){
				$standard_report.= "<option value='".$results["CODELST_CUSTOM_COL"][$rec]."' SELECTED>".ucfirst($results["CODELST_CUSTOM_COL"][$rec])."</option>";
			}else{
				$standard_report.= "<option value='".$results["CODELST_CUSTOM_COL"][$rec]."'>".ucfirst($results["CODELST_CUSTOM_COL"][$rec])."</option>";
			}
		}
	}
?>
<div class="navigate">Copy Standard Velos Report</div>
<!--<FORM name="copy_standard_velos_report" action="copy_standard_velos_report.php" method="post" onSubmit="call_report_name(document.getElementById('standard_report').value, document.getElementById('report_name').value)">-->
<div style="width:880px; margin-bottom:10px;">
	<table border="0" style="margin-right:30px; margin-top:15px; width:600px;">
		<tr>
			<td style="width:140px;"><b>Select Standard Report:</b></td>
			<td style="width:220px;">
				<select name="standard_report" id="standard_report" style="width:200px;" onChange="display_search_report_name(document.getElementById('standard_report').value)"><?PHP echo $standard_report; ?></select>
			</td>
			<td style="width:180px;"><a href="query_report_manager.php">Back to Published Reports</a></td>
		</tr>
	</table>
	
	<table border="0" id="search_report_name" style="margin-right:30px; display:none; margin-top:15px; width:457px;">
		<tr>
			<td style="width:138px;"><b>Report Name:</b></td>
			<td style="width:200px;">
				<input name='report_name' id='report_name' type='text' style="width:200px;" onKeydown="Javascript:if(event.keyCode==13) call_report_name(document.getElementById('standard_report').value, document.getElementById('report_name').value);">
			</td>
			<td style="width:50px;">
			 <img src="./img/search.png" onClick="call_report_name(document.getElementById('standard_report').value, document.getElementById('report_name').value);" align='absmiddle' border='0'>
		  </td>
		</tr>
	</table>	
</div>
<div id="view_report_name" style="display:none;"></div>

<div class="important" id="show_note" style="margin-top:20px; display:none;">
	<table border="0">
		<tr>
			<th align="left">Note</th>
		</tr>
		<tr>
			<td align="left" valign="top">
				<p>Please click search button or hit the enter key to display the available Standard Reports</p>
			</td>
		</tr>
	</table>
</div>

<div id="showpopup" style="border:2px solid black; background-color:#FFFFFF; width:450px; padding:10px; height:130px; font-size:150%; text-align:center; display:none;">
	<div style="float:right;"><img src="./img/close_button.gif" align='absmiddle' border='0' onClick="Popup.hide('showpopup')"></div>
	<table style="margin-top:55px; width:450px;">
		<tr>
			<td style="width:100px;"><b>Report Name:</b></td>
			<td style="width:270px;">
				<input name='new_report_name' id='new_report_name' type='text' style="width:265px;">
			</td>
			<td style="width:80px;">
				<img src="./img/submit.png" align='absmiddle' border='0' onClick="copy_new_report_name(document.getElementById('new_report_name').value);">
			</td>
		</tr>
	</table>
	<div id="error_new_report_name" style="float:left; margin-top:10px; font-size:13px; color:#FF0000; margin-left:100px;"></div>
	<div id="copy_new_report_name" style="float:left; margin-top:10px; font-size:13px; margin-left:100px;"></div>
</div>
</div>
<!--</FORM>-->
</body>
</html>
<?php
OCICommit($ds_conn);
OCILogoff($ds_conn);
}
else header("location: index.php?fail=1");
?>