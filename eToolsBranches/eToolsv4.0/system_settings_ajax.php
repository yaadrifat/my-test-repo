<?php
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){

include("./includes/oci_functions.php");	
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

$usr = $_GET['q'];
//$ids = "select PK_ACCOUNT, AC_AUTOGEN_PATIENT, AC_AUTOGEN_STUDY from er_account where pk_account='".$usr."'";
$ids = "select PK_ACCOUNT, AC_AUTOGEN_PATIENT, AC_AUTOGEN_STUDY from er_account where pk_account=(select FK_ACCOUNT from er_user where pk_user='".$usr."')";
$rts = executeOCIQuery($ids,$ds_conn);

if($rts["AC_AUTOGEN_PATIENT"][0]==0){
	$pattog = 'Off';
}else{
	$pattog = 'On';
}
if($rts["AC_AUTOGEN_STUDY"][0]==0){
	$stdtog = 'Off';
}else{
	$stdtog = 'On';
} 

$apeids = "select * from er_settings where SETTINGS_KEYWORD='ACCELARATED_ENROLLMENT_CONFIG' and SETTINGS_MODNUM=(select FK_ACCOUNT from er_user where pk_user=".$usr.")";
$ape_rts = executeOCIQuery($apeids,$ds_conn);
$tRows = $results_nrows;

if($ape_rts["SETTINGS_VALUE"][0]==1){
	$apeid = "On";
}else{
	$apeid = "Off";
}


$disrows = "";
$disrows.='<Table width="100%" border="1" id="patstd"  style="border-top:none;">';
$disrows.='<TR onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';">';
$disrows.="<TD width='25%'>Autogeneration Patient ID</TD>";
$disrows.='<TD width="20%">'.$pattog.'</TD>';
$disrows.='<TD width="10%"><a href="system_settings_edit.php?re=apid&pkac='.$rts["PK_ACCOUNT"][0].'&m=p&u='.$usr.'&id='.$rts["AC_AUTOGEN_PATIENT"][0].'">Edit</a></TD>';
$disrows.='</TR>';
$disrows.='<TR>';
$disrows.="<TD>Autogeneration Study ID</TD>";
$disrows.='<TD>'.$stdtog.'</TD>';
$disrows.='<TD><a href="system_settings_edit.php?re=apid&pkac='.$rts["PK_ACCOUNT"][0].'&m=s&u='.$usr.'&id='.$rts["AC_AUTOGEN_STUDY"][0].'">Edit</a></TD>';
$disrows.='</TR>';
$disrows.='<TR>';
// I have hide this feature based on the discussion with sonia, jeyakumar and nicholas on 9/1/2010
//------------------------------------------------------------------------------------------------
/*$disrows.="<TD>Accelarated Patient Enrollment</TD>";
$disrows.='<TD>'.$apeid.'</TD>';
if($apeid=="On"){
	$disrows.='<TD><a href="system_settings_edit.php?re=ape&sv=1&u='.$usr.'&pkac='.$rts["PK_ACCOUNT"][0].'">Edit</a></TD>';	
}else{
	$disrows.='<TD><a href="system_settings_edit.php?re=ape&sv=0&u='.$usr.'&pkac='.$rts["PK_ACCOUNT"][0].'">Edit</a></TD>';
} */

$disrows.='</TR>';
$disrows.='</Table>';


OCICommit($ds_conn);
OCILogoff($ds_conn);
echo $disrows;
}
else header("location: index.php?fail=1");
?>