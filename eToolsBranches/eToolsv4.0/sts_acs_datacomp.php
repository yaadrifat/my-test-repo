<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velink -> Change Data Completeness Criteria</title>
</head>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body>
<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	
<p fontsize="12"><b><u>STS Adult Cardiac Surgery Registry - Change Data Completeness Criteria</u></b></p>
<?PHP
if (!isset($_POST["keywordval"])){
	$query = "SELECT seqno,mp_dispname, keyword , harvest, core  FROM CARDIO_HARVEST , ER_MAPFORM WHERE product_id = 'ACS' AND fk_form = 537 AND keyword = mp_uid AND harvest IN ('Y' , 'O') AND keyword NOT IN ('DataVrsn','RecordID','HospName','ParticID','SurgDt') ORDER BY  mp_sequence";
	$results = executeOCIQuery($query,$ds_conn);
	echo '<form name="datacomp" action="sts_acs_datacomp.php" method="post">';
	echo '<table width="100%">';
	for($row=0;$row<$results_nrows;$row++){
		if ($results["CORE"][$row] == 'Y'){
			echo '<tr><td><input checked type="checkbox" name="keyword['.$row.']">'.$results["MP_DISPNAME"][$row].' ['.$results["KEYWORD"][$row].'] - '.$results["SEQNO"][$row];
		} else {
			echo '<tr><td><input type="checkbox" name="keyword['.$row.']">'.$results["MP_DISPNAME"][$row].' ['.$results["KEYWORD"][$row].'] - '.$results["SEQNO"][$row];
		}
		echo '<input type=hidden name="keywordval['.$row.']" value="'.$results["KEYWORD"][$row].'">';
		if ($results["HARVEST"][$row] == 'O') echo " <font color=red>Optional</font>"; 
		echo '</input></td></tr>';
	}
	echo '</table>';
	echo "<BR><input type=submit name=submit value=Submit>";
	echo '</form>';
} else {
	$v_keyword = $_POST["keyword"];
	$v_keywordval = $_POST["keywordval"];
	for($i=0;$i<count($v_keywordval);$i++){
		if (isset($v_keyword[$i])){
			$v_core = "Y";
		} else {
			$v_core = "N";
		}
		$query = "update CARDIO_HARVEST set core = '$v_core' where product_id = 'ACS' and keyword = '".$v_keywordval[$i]."'";
		executeOCIUpdateQuery($query, $ds_conn);
	}
echo "Data Saved Successfully";
}		
OCICommit($ds_conn);
OCILogoff($ds_conn);
?>

	
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
