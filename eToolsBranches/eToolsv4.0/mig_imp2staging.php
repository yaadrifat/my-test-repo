<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
<title>Velos eTools -> Migrate Data</title>

<?php
include("./includes/oci_functions.php");
include("./includes/header.php");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
</head>
<body>
<div id="fedora-content">	
<div class="navigate">Migrate Data - Standard - Import</div>
<?php

$v_mode = (isset($_GET["mode"])?$_GET["mode"]:$_POST["mode"]);

if (isset($_GET["pk_vlnk_adaptor"])) $pk_vlnk_adaptor = $_GET["pk_vlnk_adaptor"];
if (isset($_POST["pk_vlnk_adaptor"])) $pk_vlnk_adaptor = $_POST["pk_vlnk_adaptor"];

if (isset($_GET["pk_vlnk_adapmod"])) $pk_vlnk_adapmod = $_GET["pk_vlnk_adapmod"];
if (isset($_POST["pk_vlnk_adapmod"])) $pk_vlnk_adapmod = $_POST["pk_vlnk_adapmod"];
if (isset($_GET["rows"])) {
	$v_rows = $_GET["rows"];
} else {
	$v_rows = -1;
}

if ($v_mode == "empty") {
	if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
	?>
		<form name="import" method="post" action="mig_imp2staging.php">
			<input type="hidden" name="pk_vlnk_adapmod" value="<?PHP echo $pk_vlnk_adapmod;?>" />
			<input type="hidden" name="pk_vlnk_adaptor" value="<?PHP echo $pk_vlnk_adaptor;?>" />
			<input type="hidden" name="mode" value="empty" />
			<br>
			<table width="100%">
			<tr><td><input type="checkbox" name="delete_tempdata" id="delete_tempdata" /> <label for="delete_tempdata"> Delete data from intermediary table.</label></td>
			<tr><td><BR><input type="checkbox" name="delete_respdata" id="delete_respdata" /> <label for="delete_respdata"> Delete response mapping.</label></td>
			</table>
			<BR><input type="image" name="submit" value="SUBMIT" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />
		</form>
	<?PHP
	} else {
		if (isset($_POST["delete_tempdata"])){
			$query_sql = "delete from velink.vlnk_log where fk_vlnk_pipe in (select pk_vlnk_pipe from velink.vlnk_pipe where fk_vlnk_adapmod = ".$pk_vlnk_adapmod.")" ;
			$results = executeOCIUpdateQuery($query_sql,$ds_conn);
	
			$query_sql = "delete from velink.vlnk_pipe where fk_vlnk_adapmod = ".$pk_vlnk_adapmod ;
			$results = executeOCIUpdateQuery($query_sql,$ds_conn);
		}
	
		if (isset($_POST["delete_respdata"])){
			$query_sql = "delete from velink.vlnk_pipemap where fk_vlnk_adapmod = ".$pk_vlnk_adapmod ;
			$results = executeOCIUpdateQuery($query_sql,$ds_conn);
		}
		
		echo "<BR>Data emptied from Staging Area...";
		echo '<meta http-equiv="refresh" content="1; url=./mig_modules.php?pk_vlnk_adaptor='.$pk_vlnk_adaptor.'"> ';
	
	}
}

if ($v_mode == "import") {
	$query_sql = "SELECT table_name FROM velink.VLNK_ADAPMOD WHERE pk_vlnk_adapmod = ".$pk_vlnk_adapmod ;
	$results = executeOCIQuery($query_sql,$ds_conn);
	$tablename = $results["TABLE_NAME"][0];


	$query_sql = "SELECT map_col_name, pipe_colname,col_type FROM velink.VLNK_ADAPMODCOL,velink.VLNK_MODCOL WHERE pk_vlnk_modcol = fk_vlnk_modcol AND  map_col_name IS NOT NULL AND fk_vlnk_adapmod = ".$pk_vlnk_adapmod ;
	$results = executeOCIQuery($query_sql,$ds_conn);
	$totrows = $results_nrows;

	$v_col_sql = "";
	$v_data_sql = "";

	for ($rec = 0; $rec < $totrows; $rec++){
		$v_col_sql = $v_col_sql.$results["PIPE_COLNAME"][$rec].", ";

		$v_sql = "SELECT data_type FROM DBA_TAB_COLS WHERE owner = 'ERES' AND table_name = '".strtoupper($tablename)."' AND column_name = '".strtoupper($results["MAP_COL_NAME"][$rec])."'";
		$v_results = executeOCIQuery($v_sql,$ds_conn);

		if ($v_results["DATA_TYPE"][0] == "DATE") {
			$v_data_sql = $v_data_sql."to_char(".$results["MAP_COL_NAME"][$rec].",'mm/dd/yyyy'), ";
		} else {
			$v_data_sql = $v_data_sql.$results["MAP_COL_NAME"][$rec].", ";
		}

	}

	$v_ins_sql = "insert into velink.vlnk_pipe (pk_vlnk_pipe,fk_vlnk_adapmod,".substr($v_col_sql,0,strlen($v_col_sql)-2).") select velink.seq_vlnk_pipe.nextval,".$pk_vlnk_adapmod.",".substr($v_data_sql,0,strlen($v_data_sql)-2)." from ".$tablename;
	$results = executeOCIUpdateQuery($v_ins_sql,$ds_conn);
	echo "<BR>Data moved to Staging Area...";
	echo '<meta http-equiv="refresh" content="1; url=./mig_modules.php?pk_vlnk_adaptor='.$pk_vlnk_adaptor.'"> ';
}

?>
</div>
</body>
</html>
<?php
OCICommit($ds_conn);
OCILogoff($ds_conn);
}
else header("location: ./index.php?fail=1");
?>
