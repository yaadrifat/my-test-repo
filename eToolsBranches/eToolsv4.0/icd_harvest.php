<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velink -> ICD Harvest</title>
</head>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body>

<script>
function f_validate(){
	var v_check = 0;
	if (icd.partid.value ==""){
			alert("Please select Participant ID");
			icd.partid.focus();
			return false;
	}
	if (icd.year.value ==""){
			alert("Please select Year");
			icd.year.focus();
			return false
	}
	if (icd.Q1.checked== true){
		v_check = v_check +1;
		if (icd.Q2.checked== true){
			v_check = v_check +1; 
			if (icd.Q3.checked== true) {
				v_check = v_check +1;
				if (icd.Q4.checked== true){
					v_check = v_check +1;
				}
			}
		}
	}
	if (v_check == 0){
		alert("Please select Quarter");
		icd.Q1.focus();
		return false;
	}
	if ((icd.Q1.checked)== true && (icd.patpop1.value =="")){
			alert("Please select Population");
			icd.patpop1.focus();
			return false
	}
	if ((icd.Q2.checked)== true && (icd.patpop2.value =="")){
			alert("Please select Population");
			icd.patpop2.focus();
			return false
	}
	if ((icd.Q3.checked)== true && (icd.patpop3.value =="")){
			alert("Please select Population");
			icd.patpop3.focus();
			return false
	}
	if ((icd.Q4.checked)== true && (icd.patpop4.value =="")){
			alert("Please select Population");
			icd.patpop4.focus();
			return false
	}
}
</script>

<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	
<?PHP
if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
$dd_year = '<select name="year"><option value="">Select an option</option>';
for ($i=2000; $i<2100; $i++){
	$dd_year .= '<option value="'.$i.'">'.$i.'</option>';
}
$dd_year .= '</select>';


$v_query = "select col8 from er_formslinear where fk_form = 486";
$results = executeOCIQuery($v_query,$ds_conn);

$dd_partid = '<select name="partid"><option value="">Select an option</option>';
for ($rec = 0; $rec < $results_nrows; $rec++){
	$dd_partid .= '<option value="'.$results["COL8"][$rec].'">'.$results["COL8"][$rec].'</option>';
}
$dd_partid .= '</select>';

$dd_patpop="";
//$dd_patpop = '<select name="patpop"><option value="">Select an option</option>';
$dd_patpop .= '<option value="1">All ICD Patients</option>';
$dd_patpop .= '<option value="2">Only CMS primary prevention ICD patients</option>';
//$dd_patpop .= '</select>';


?>
<form name="icd" action="icd_harvest.php" method="POST" onSubmit="return f_validate();">
<p fontsize="12"><b><u>ACC-NCDR ICD Registry Harvest</u></b></p>
<table>
<tr><td>Participant ID:</td><td> <?php echo $dd_partid ?> </td></tr>
<tr><td>Year:</td><td> <?php echo $dd_year ?> </td></tr>
<tr><td>Quarter</td>
	<td><input type="checkbox" name="Q1" >Quarter 1(Jan 1 to Mar 31)</input></td>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;Patient Population:</td>
	<td><select name="patpop1"><option value="">Select an option</option><?php echo $dd_patpop ?></select></td>
</tr>
<tr><td>&nbsp;</td><td><input type="checkbox" name="Q2" >Quarter 2(Apr 1 to Jun 30)</input></td>
	<td>&nbsp;</td>
	<td><select name="patpop2"><option value="">Select an option</option><?php echo $dd_patpop ?></select></td>
</tr>
<tr><td>&nbsp;</td><td><input type="checkbox" name="Q3" >Quarter 3(Jul 1 to Sep 30)</input></td>
	<td>&nbsp;</td>
	<td><select name="patpop3"><option value="">Select an option</option><?php echo $dd_patpop ?></select></td>
</tr>
<tr><td>&nbsp;</td><td><input type="checkbox" name="Q4" >Quarter 4(Oct 1 to Dec 31)</input></td>
	<td>&nbsp;</td>
	<td><select name="patpop4"><option value="">Select an option</option><?php echo $dd_patpop ?></select></td>
</tr>

<!-- <tr><td>Patient Population:</td><td> <?php echo $dd_patpop ?> </td></tr> -->

<tr><td><input type="submit" name="submit" value="Submit"/></td></tr>
</table>
</form>

<?PHP } else { 

$v_partid = $_POST["partid"];
$v_year = $_POST["year"];
$v_patpop1 = $_POST["patpop1"];
$v_patpop2 = $_POST["patpop2"];
$v_patpop3 = $_POST["patpop3"];
$v_patpop4 = $_POST["patpop4"];

$v_query = "select zip_pwd,transmission_num from cardio_ctrltab where fk_account = 52";
$results = executeOCIQuery($v_query,$ds_conn);
$v_pwd = $results['ZIP_PWD'][0];
$v_tran = $results['TRANSMISSION_NUM'][0];
$v_tran = $v_tran + 1;
$v_query = "update cardio_ctrltab set transmission_num = ".$v_tran." where fk_account = 52";
$results = executeOCIUpdateQuery($v_query,$ds_conn);

$v_cwd = getcwd();

/*
	sleep(2);
	$v_command = 'zip.exe test.zip *.log';
	echo $v_command;
	$output = "";
	$return_var = "";
	exec($v_command,$output,$return_var);
	echo "Start...";
	echo "<BR>";
	foreach($output as $temp_output){
		echo $temp_output;
		echo "<BR>";
	}
	echo $return_var;
	echo "End...";
*/
if (isset($_POST["Q1"])){
//	echo "<BR>";
//	echo "Query Start...";
//	echo "<BR>";
//	flush();
	$v_query = "select pkg_accicd.f_acc_icd_harvest(489,$v_partid,'".$v_year."Q1',$v_patpop1,$v_tran) as hardata from dual";
	$results = executeOCIQuery($v_query,$ds_conn);
//	echo "<BR>";
//	echo "Query End...";
//	echo "<BR>";
//	flush();
	If (substr($results["HARDATA"][0],0,5) != 'Error'){
//		echo "<BR>";
//		echo "Creating files...start";
//		echo "<BR>";
//		flush();

		$filename = 'I'.$v_partid.'-'.$v_year.'Q1.xml';
		$fp = fopen( './icd/'.$filename,'w');
		$write = fputs($fp,$results["HARDATA"][0]);
		fclose($fp);
		echo "Quarter 1 data exported...";
		echo "<BR>";

//		echo "<BR>";
//		echo "Creating files...end";
//		echo "<BR>";
//		flush();

//		echo "<BR>";
//		echo "Zipping files...start";
//		echo "<BR>";
		flush();
		$v_command = '7za.exe a -tzip -p'.$v_pwd.' "d:\\program files\\easyphp1-8\\www\\support\\icd\\'.'I'.$v_partid.'-'.$v_year.'Q1.zip" "d:\\program files\\easyphp1-8\\www\\support\\icd\\'.$filename.'"';
		$output = "";
		$return_var = "";
		exec($v_command,$output,$return_var);
//		echo "<BR>";
//		echo "Files zipped...";
//		echo "<BR>";
//		flush();
		exec('del "d:\\program files\\easyphp1-8\\www\\support\\icd\\'.$filename.'"',$output,$return_var);
//		echo "<BR>";
//		echo "Files deleted...";
//		echo "<BR>";
//		flush();
	} else {
		echo "Quater 1:";
		echo "<BR>";
		echo $results["HARDATA"][0];
		echo "<BR>";
	}
/*
	echo "Start...";
	echo "<BR>";
	foreach($output as $temp_output){
		echo $temp_output;
		echo "<BR>";
	}
	echo $return_var;
	echo "End...";
	echo '<textarea name="test" rows="50" cols="100">'.$results["HARDATA"][0].'</textarea>';
*/
}

if (isset($_POST["Q2"])){
	$v_query = "select pkg_accicd.f_acc_icd_harvest(489,$v_partid,'".$v_year."Q2',$v_patpop2,$v_tran) as hardata from dual";
	$results = executeOCIQuery($v_query,$ds_conn);
	If (substr($results["HARDATA"][0],0,5) != 'Error'){
		$filename = 'I'.$v_partid.'-'.$v_year.'Q2.xml';
		$fp = fopen( './icd/'.$filename,'w');
		$write = fputs($fp,$results["HARDATA"][0]);
		fclose($fp);
		echo "Quarter 2 data exported...";
		echo "<BR>";
		$v_command = '7za.exe a -tzip -p'.$v_pwd.' "d:\\program files\\easyphp1-8\\www\\support\\icd\\'.'I'.$v_partid.'-'.$v_year.'Q2.zip" "d:\\program files\\easyphp1-8\\www\\support\\icd\\'.$filename.'"';
		$output = "";
		$return_var = "";
		exec($v_command,$output,$return_var);
		exec('del "d:\\program files\\easyphp1-8\\www\\support\\icd\\'.$filename.'"',$output,$return_var);
	} else {
		echo "Quater 2:";
		echo "<BR>";
		echo $results["HARDATA"][0];
		echo "<BR>";
	}
}

if (isset($_POST["Q3"])){
	$v_query = "select pkg_accicd.f_acc_icd_harvest(489,$v_partid,'".$v_year."Q3',$v_patpop3,$v_tran) as hardata from dual";
	$results = executeOCIQuery($v_query,$ds_conn);
	If (substr($results["HARDATA"][0],0,5) != 'Error'){
		$filename = 'I'.$v_partid.'-'.$v_year.'Q3.xml';
		$fp = fopen( './icd/'.$filename,'w');
		$write = fputs($fp,$results["HARDATA"][0]);
		fclose($fp);
		echo "Quarter 3 data exported...";
		echo "<BR>";
		$v_command = '7za.exe a -tzip -p'.$v_pwd.' "d:\\program files\\easyphp1-8\\www\\support\\icd\\'.'I'.$v_partid.'-'.$v_year.'Q3.zip" "d:\\program files\\easyphp1-8\\www\\support\\icd\\'.$filename.'"';
		$output = "";
		$return_var = "";
		exec($v_command,$output,$return_var);
		exec('del "d:\\program files\\easyphp1-8\\www\\support\\icd\\'.$filename.'"',$output,$return_var);
	} else {
		echo "Quater 3:";
		echo "<BR>";
		echo $results["HARDATA"][0];
		echo "<BR>";
	}
}

if (isset($_POST["Q4"])){
	$v_query = "select pkg_accicd.f_acc_icd_harvest(489,$v_partid,'".$v_year."Q4',$v_patpop4,$v_tran) as hardata from dual";
	$results = executeOCIQuery($v_query,$ds_conn);
	If (substr($results["HARDATA"][0],0,5) != 'Error'){
		$filename = 'I'.$v_partid.'-'.$v_year.'Q4.xml';
		$fp = fopen( './icd/'.$filename,'w');
		$write = fputs($fp,$results["HARDATA"][0]);
		fclose($fp);
		echo "Quarter 4 data exported...";
		echo "<BR>";
		$v_command = '7za.exe a -tzip -p'.$v_pwd.' "d:\\program files\\easyphp1-8\\www\\support\\icd\\'.'I'.$v_partid.'-'.$v_year.'Q4.zip" "d:\\program files\\easyphp1-8\\www\\support\\icd\\'.$filename.'"';
		$output = "";
		$return_var = "";
		exec($v_command,$output,$return_var);
		exec('del "d:\\program files\\easyphp1-8\\www\\support\\icd\\'.$filename.'"',$output,$return_var);
	} else {
		echo "Quater 4:";
		echo "<BR>";
		echo $results["HARDATA"][0];
		echo "<BR>";
	}
}


//$url = "./icd_harvest_view.php";
//echo "<meta http-equiv=\"refresh\" content=\"3; url=./".$url."\">";

} 
	
OCICommit($ds_conn);
OCILogoff($ds_conn);
?>

	
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
