<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Data Migration</title>

<?php
include("./includes/oci_functions.php");
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Migrate Data</div>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="./migration_newadap.php">New Migration Category</a>
<Table border="1" width="100%"><TR>
<TH width="25%">NAME</TH>
<TH width="25%">DESCRIPTION</TH>
<TH width="15%">AUDIT USER</TH>
<TH width="15%">CREATED ON</TH>
<TH width="10%">&nbsp;</TH>
<TH width="10%">&nbsp;</TH>
</TR>
<?php

$query_sql = "SELECT pk_vlnk_adaptor, adaptor_name, adaptor_desc, (select usr_firstname || ' ' || usr_lastname from er_user where pk_user = a.creator) as username,to_char(created_on,'mm/dd/yyyy hh24:mi:ss') as created_on from velink.vlnk_adaptor a where record_type <> 'D' order by pk_vlnk_adaptor desc";

$results = executeOCIQuery($query_sql,$ds_conn);

for ($rec = 0; $rec < $results_nrows; $rec++){
?>
	<TR onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
	<TD><?php echo $results["ADAPTOR_NAME"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["ADAPTOR_DESC"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php $results["USERNAME"][$rec] = str_replace(array("<script>","</script>"),array("",""),$results["USERNAME"][$rec]); echo $results["USERNAME"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["CREATED_ON"][$rec] . "&nbsp;"; ?></TD>
<?php
	echo "<td><a href=mig_modules.php?pk_vlnk_adaptor=".$results["PK_VLNK_ADAPTOR"][$rec]."&name=".urlencode($results["ADAPTOR_NAME"][$rec])."&desc=".urlencode($results["ADAPTOR_DESC"][$rec])."&uname=".urlencode($results["USERNAME"][$rec]).">Standard</a></td>";
	echo "<td><a href=mig_forms.php?pk_vlnk_adaptor=".$results["PK_VLNK_ADAPTOR"][$rec].">Forms</a></td>";
	echo "</TR>";
}
?>
</TABLE>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
