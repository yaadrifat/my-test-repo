<?PHP
function uploader($num_of_uploads=1, $file_types_array=array("xls"), $max_file_size=50048576, $upload_dir=""){
  if(!is_numeric($max_file_size)){
   $max_file_size = 50048576;
  }

  if(!isset($_POST["submitted"])){

   $form = "<form action='".$_SERVER['PHP_SELF']."' method='post' enctype='multipart/form-data'><table><tr><td>Select Excel File:</td><td><input type='hidden' name='submitted' value='TRUE' id='".time()."'><input type='hidden' name='MAX_FILE_SIZE' value='".$max_file_size."'>";
   for($x=0;$x<$num_of_uploads;$x++){
     $form .= "<input type='file' name='file[]'><font color='red'>*</font><br />";
   }
   $form .= "</td></tr><tr><td>Table Name:</td><td><input type='text' name='tabname'><font color='red'>*</font></td></tr>";
   $form .= "<tr><td><input type='submit' value='Upload'></td></tr></table>";

	$form .= '<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Note"><tr>
	<th align="left">Note</th></tr><tr><td align="left" valign="top">';
	$form .= '<p>While creating tables for importing files, please use a naming convention specific to your institution. We highly encourage Administrators to NOT use Velos table naming conventions so that there is an easy distinction in case issues arise.</p>';
	$form .= '<p>Maximum file name length (minus extension) is 15 characters. Anything beyond that will be truncated to only 15 characters.</p>';
	$form .= '<p>We have observed unexpected errors in case of date fields (less than 1/1/1970) in Excel files. In case your file data has such date fields, we would recommend converting the file to CSV and then using the function below to import it.</p>';
	$form .= "<p>First row of the file should contain Column Name, no special characters other than '_' (underscore) and should be one continuous string of characters (no space)</p>";
	$form .= '</td></tr></table></div>';




   $form .= "</form>";
   echo($form);

  }else{
  	$v_tabname = $_POST["tabname"];
   foreach($_FILES["file"]["error"] as $key => $value){
     if($_FILES["file"]["name"][$key]!=""){
       if($value==UPLOAD_ERR_OK){
         $origfilename = $_FILES["file"]["name"][$key];
         $filename = explode(".", $_FILES["file"]["name"][$key]);
         $filenameext = $filename[count($filename)-1];
         unset($filename[count($filename)-1]);
         $filename = implode(".", $filename);
         $filename = substr($filename, 0, 15).".".$filenameext;
         $file_ext_allow = FALSE;
         for($x=0;$x<count($file_types_array);$x++){
           if($filenameext==$file_types_array[$x]){
             $file_ext_allow = TRUE;
           }
         }
         if($file_ext_allow){
           if($_FILES["file"]["size"][$key]<$max_file_size){
             if(move_uploaded_file($_FILES["file"]["tmp_name"][$key], $upload_dir.$filename)){
               echo("File uploaded successfully. - <a href='".$upload_dir.$filename."' target='_blank'>".$filename."</a><br />");
               echo "Importing data into table...";

				$v_url = "./excelread/import.php?filename=".$filename."&tabname=".$v_tabname;
				echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$v_url."\">";

             }else{
               echo($origfilename." was not successfully uploaded<br />");
             }
           }else{
             echo($origfilename." was too big, not uploaded<br />");
           }
         }else{
           echo($origfilename." had an invalid file extension, not uploaded<br />");
         }
       }else{
         echo($origfilename." was not successfully uploaded<br />");
       }
     }
   }
  }
}
?>