<?php

if(!session_is_registered("session_count")) {
      $session_count = 0;
      $session_start = time();
      $_SESSION['session_count']=$session_count;
      $_SESSION['session_start']=$session_start;
} else {
      $session_count++;
}

$session_timeout = 15; 

$session_duration = time() - $session_start;
if ($session_duration > $session_timeout) {
    header("Location: logout.php");  
}
$session_start = time();

?>
