<?PHP

function getDisplacement($visit){

	$visit = strtoupper($visit).'|';
	$visit = str_replace('M','|M',$visit);
	$visit = str_replace('W','|W',$visit);
	$visit = str_replace('D','|D',$visit);

	$v_visitSplit = explode('|',$visit);
	$num_months = 0;
	$num_weeks = 0;
	$num_days = 0;
	$displacement = 0;

	for ($i=0;$i<count($v_visitSplit);$i++) {
		$v_test = substr($v_visitSplit[$i],0,1);
		switch ($v_test) {
		case "M":
			$num_months = substr($v_visitSplit[$i],1);
			$num_months = ($num_months > 0?($num_months ):0);
			break;
		case "W":
			$num_weeks = substr($v_visitSplit[$i],1);
			$num_weeks = ($num_weeks > 0?($num_weeks ):0);
			break;
		case "D":
			$num_days = substr($v_visitSplit[$i],1);
			break;
		}
	}

	//implemented this for the bug 6640
	if ($num_days==0){
		$pos = strpos($visit,"D");
		if($pos===false){
			$num_days=1;
		}
	}

	$displacement = ($num_months > 0?(($num_months - 1) * 30):0) + ($num_weeks > 0?(($num_weeks - 1)* 7):0) + ($num_days == 0?1:$num_days);
	
/*
	if ($week > 0) {
		$num_months  = (($month > 0 ) ? substr($visit,$month+1,$week-$month-1) : 0);
		$displacement = $displacement + (($month > 0 ) ? (substr($visit,$month+1,$week-$month-1) - 1)*30 : 0);
	} else {
		$num_months  = (($month > 0 ) ? substr($visit,$month+1) : 0);
		$displacement = $displacement + (($month > 0 ) ? (substr($visit,$month+1) - 1)*30 : 0);
	}
	
	if ($day > 0) {
		$num_weeks = (($week > 0) ? substr($visit,$week+1,$day-$week-1) : 0);
		$displacement = $displacement  + (($week > 0) ? (substr($visit,$week+1,$day-$week-1)-1)*7 : 0);
	} else {
		$num_weeks = (($week > 0) ? substr($visit,$week+1) : 0);
		$displacement = $displacement  + (($week > 0) ? (substr($visit,$week+1)-1)*7 : 0);
	}
	
	$num_days = (($day > 0) ? substr($visit,$day+1) : 0);
	$displacement = $displacement  + (($day > 0) ? (substr($visit,$day+1)) : 1);
	
*/
	
//echo $visit.'|Months:'.$num_months.'|Weeks:'.$num_weeks.'|Days:'.$num_days.'|Displacement:'.$displacement."<BR>";
	return $num_months.'|'.$num_weeks.'|'.$num_days.'|'.$displacement;

}

function getDisplacementValue($visit){
/*
	list($v_visit,$v_visit_name) = explode('|',$visit);
	$visit='XYZ'.strtoupper($v_visit);
	$month = strpos($visit,'M');
	$week = strpos($visit,'W');
	$day = strpos($visit,'D');
	$displacement = 0;
	
	if ($week > 0) {
		$num_months  = (($month > 0 ) ? substr($visit,$month+1,$week-$month-1) : 0);
		$displacement = $displacement + (($month > 0 ) ? (substr($visit,$month+1,$week-$month-1) - 1)*30 : 0);
	} else {
		$num_months  = (($month > 0 ) ? substr($visit,$month+1) : 0);
		$displacement = $displacement + (($month > 0 ) ? (substr($visit,$month+1) - 1)*30 : 0);
	}
	
	if ($day > 0) {
		$num_weeks = (($week > 0) ? substr($visit,$week+1,$day-$week-1) : 0);
		$displacement = $displacement  + (($week > 0) ? (substr($visit,$week+1,$day-$week-1)-1)*7 : 0);
	} else {
		$num_weeks = (($week > 0) ? substr($visit,$week+1) : 0);
		$displacement = $displacement  + (($week > 0) ? (substr($visit,$week+1)-1)*7 : 0);
	}
	
	$num_days = (($day > 0) ? substr($visit,$day+1) : 0);
	$displacement = $displacement  + (($day > 0) ? (substr($visit,$day+1)) : 1);

	if (($month == 0) && ($week == 0) && ($day == 0)) {return 0;} else {return $displacement;}
*/

	$visit = strtoupper($visit).'|';
	$visit = str_replace('M','|M',$visit);
	$visit = str_replace('W','|W',$visit);
	$visit = str_replace('D','|D',$visit);

	$v_visitSplit = explode('|',$visit);
	$num_months = 0;
	$num_weeks = 0;
	$num_days = 0;
	$displacement = 0;

	for ($i=0;$i<count($v_visitSplit);$i++) {
		$v_test = substr($v_visitSplit[$i],0,1);
		switch ($v_test) {
		case "M":
			$num_months = substr($v_visitSplit[$i],1);
			$num_months = ($num_months > 0?($num_months ):0);
			break;
		case "W":
			$num_weeks = substr($v_visitSplit[$i],1);
			$num_weeks = ($num_weeks > 0?($num_weeks ):0);
			break;
		case "D":
			$num_days = substr($v_visitSplit[$i],1);
			break;
		}
	}

	$displacement = ($num_months > 0?(($num_months - 1) * 30):0) + ($num_weeks > 0?(($num_weeks - 1)* 7):0) + ($num_days == 0?1:$num_days);
	return $displacement;
}

function displacementValidate($visit){
/*

	list($v_visit,$v_visit_name) = explode('|',$visit);
	$visit='XYZ'.strtoupper($v_visit);
	$month = strpos($visit,'M');
	$week = strpos($visit,'W');
	$day = strpos($visit,'D');

	if ($week > 0) {
		$num_months  = (($month > 0 ) ? substr($visit,$month+1,$week-$month-1) : 0);
	} else {
		$num_months  = (($month > 0 ) ? substr($visit,$month+1) : 0);
	}
	
	if ($day > 0) {
		$num_weeks = (($week > 0) ? substr($visit,$week+1,$day-$week-1) : 0);
	} else {
		$num_weeks = (($week > 0) ? substr($visit,$week+1) : 0);
	}
	
	$num_days = (($day > 0) ? substr($visit,$day+1) : 0);

*/
	$visit = strtoupper($visit).'|';
	$visit = str_replace('M','|M',$visit);
	$visit = str_replace('W','|W',$visit);
	$visit = str_replace('D','|D',$visit);

	$v_visitSplit = explode('|',$visit);
	$num_months = 0;
	$num_weeks = 0;
	$num_days = 0;
	$displacement = 0;

	for ($i=0;$i<count($v_visitSplit);$i++) {
		$v_test = substr($v_visitSplit[$i],0,1);
		switch ($v_test) {
		case "M":
			$num_months = substr($v_visitSplit[$i],1);
			$num_months = ($num_months > 0?$num_months:0);
			break;
		case "W":
			$num_weeks = substr($v_visitSplit[$i],1);
			$num_weeks = ($num_weeks > 0?$num_weeks:0);
			break;
		case "D":
			$num_days = substr($v_visitSplit[$i],1);
			break;
		}
	}

	$v_retval = 0;
	if (($num_months > 0) && ($num_weeks > 4)) $v_retval = 1;
	if ($num_weeks > 0 && $num_days > 7) $v_retval = 1;
	return $v_retval;
}

?>