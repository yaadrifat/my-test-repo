<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
<title>Velos eTools -&gt; Data Source</title>

<?php
include("./includes/oci_functions.php");	
include("./includes/header.php");
include ("./txt-db-api.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>
<body>

<div id="fedora-content">	
<div class="navigate">Data Sources</div>
<?PHP

$db = new Database("etools");
$rs = $db->executeQuery("SELECT PK_DS,DS_NAME,DS_HOST,DS_PORT,DS_SID FROM ET_DS  order by PK_DS");

	if($_SESSION["user"]=="admin"){
		
		echo "</br>";
		echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		echo "<a href=datasource_edit.php?mode=n>Add Data Source</a>";
		echo "</br>";
		echo "&nbsp;"; ?>
		<Table border="1" width="100%"><TR>
		<TH width="24%">DS Name </TH>
		<TH width="12%">DS Host </TH>
		<TH width="8%">DS Port </TH>
		<TH width="15%">DS SID </TH>
		<TH width="10%">DS Status </TH>		
		<TH width="6%">&nbsp;</TH>
		<TH width="10%">&nbsp;</TH>		
		<TH width="6%">&nbsp;</TH>
		</TR>		
	<?php }else{ ?>
		<Table border="1" width="100%"><TR>
		<TH width="24%">DS Name </TH>
		<TH width="21%">DS Host </TH>
		<TH width="11%">DS Port </TH>
		<TH width="12%">DS SID </TH>
		<TH width="15%">DS Status </TH>		
		</TR>	
	<?php
	}
?>
<?php

$dbs = new Database("etools");
$records = $dbs->executeQuery("SELECT DS_RIGHTS FROM ET_GROUPS where PK_GROUPS=1");
		if($records->getRowCount() >0){
		$tempV=0;
			while($records->next()){
				$data = $records->getCurrentValuesAsHash();				
				$dsRights = explode("|",$data['DS_RIGHTS']);								
				$tempV++;
			}
		}			
$v_counter = 0;
while ($rs->next()) {
$data = $rs->getCurrentValuesAsHash();
list($pk_ds, $ds)=explode(":",$dsRights[$v_counter]);
	if($_SESSION["user"]=="admin"){		
		if($ds=="1"){ ?>
			<TR bgcolor="#FFFFFF" onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
			<TD><?php echo $data["DS_NAME"] ?></TD>
			<TD><?php echo $data["DS_HOST"]?></TD>
			<TD><?php echo $data["DS_PORT"] ?></TD>
			<TD><?php echo $data["DS_SID"] ?></TD>
			<TD><?php if($ds=="1"){ echo "Enabled"; }else{ echo "Disabled"; } ?></TD>			
			<TD><?PHP echo '<a href=datasource_edit.php?mode=m&pk_ds='.$data["PK_DS"].'>Edit</a>'; ?></TD>
			<TD><?PHP echo '<a href=datasource_edit.php?mode=c&pk_ds='.$data["PK_DS"].'>Change Password</a>'; ?></TD>
			<TD><?PHP echo '<a href=datasource_dash.php?mode=d&pk_ds='.$data["PK_DS"].'>Delete</a>'; ?></TD>			
			<input type='hidden' name='dsst' value=<?PHP echo $ds; ?>>			
			</TR>					
		<?php }else{ ?>
			<TR bgcolor="#d4d4d3" onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#d4d4d3';">
			<TD><?php echo $data["DS_NAME"] ?></TD>
			<TD><?php echo $data["DS_HOST"]?></TD>
			<TD><?php echo $data["DS_PORT"] ?></TD>
			<TD><?php echo $data["DS_SID"] ?></TD>
			<TD><?php if($ds=="1"){ echo "Enabled"; }else{ echo "Disabled"; } ?></TD>			
			<TD><?PHP echo '<a href=datasource_edit.php?mode=m&pk_ds='.$data["PK_DS"].'>Edit</a>'; ?></TD>
			<TD><?PHP echo '<a href=datasource_edit.php?mode=c&pk_ds='.$data["PK_DS"].'>Change Password</a>'; ?></TD>			
			<TD><?PHP echo '<a href=datasource_dash.php?mode=d&pk_ds='.$data["PK_DS"].'>Delete</a>'; ?></TD>
			<input type='hidden' name='dsst' value=<?PHP echo $ds; ?>>			
			</TR>					
		<?php }	?>
	<?php	
	}else{ 
		if($ds=="1"){ ?>
			<TR bgcolor="#FFFFFF" onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
			<TD><?php echo $data["DS_NAME"] ?></TD>
			<TD><?php echo $data["DS_HOST"]?></TD>
			<TD><?php echo $data["DS_PORT"] ?></TD>
			<TD><?php echo $data["DS_SID"] ?></TD>
			<TD><?php if($ds=="1"){ echo "Enabled"; }else{ echo "Disabled"; } ?></TD>								
		<?php }else{ ?>
			<TR bgcolor="#d4d4d3" onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#d4d4d3';">
			<TD><?php echo $data["DS_NAME"] ?></TD>
			<TD><?php echo $data["DS_HOST"]?></TD>
			<TD><?php echo $data["DS_PORT"] ?></TD>
			<TD><?php echo $data["DS_SID"] ?></TD>
			<TD><?php if($ds=="1"){ echo "Enabled"; }else{ echo "Disabled"; } ?></TD>										
		<?php }	?>
<?php		
	}
	$v_counter++;
}
?>
<?php
	$v_mode = $_GET["mode"];
	$pk_num = $_GET["pk_ds"];	
	if($v_mode=="d"){
	 	$db = new Database("etools");		
		$noDs = $db->executeQuery("SELECT PK_GROUPS from ET_GROUPS");

		if($noDs->getRowCount() >0){
			while($noDs->next()){
				$noData = $noDs->getCurrentValuesAsHash();
				
				$rightsResult = $db->executeQuery("SELECT DS_RIGHTS from ET_GROUPS where PK_GROUPS=".$noData["PK_GROUPS"]);
				if($rightsResult->getRowCount() >0){
					while($rightsResult->next()){
						$data = $rightsResult->getCurrentValuesAsHash();
						$ds_rights = explode("|",$data['DS_RIGHTS']);
					}
				}
				
				for($i=0; $i<count($ds_rights)+1;$i++){
					$tmpStr = explode(":",$ds_rights[$i]);
					if($tmpStr[0]==$pk_num){
						unset($ds_rights[$i]);
					}
				}		
				$rightsStr = "";		
				for($i=0; $i<count($ds_rights)+1;$i++){	
					if($i==count($ds_rights)){
						$rightsStr.= $ds_rights[$i];
					}else{
						$rightsStr.= $ds_rights[$i]."|";									
					}
				}
		
				$tmpStr = substr($rightsStr,0,1);
				if ($tmpStr=="|"){
					$rightsStr = substr($rightsStr,1,strlen($rightsStr)-1);			
				}
				
				$lastStr = substr($rightsStr,strlen($rightsStr)-1,strlen($rightsStr));
				if ($lastStr=="|"){
					$rightsStr = substr($rightsStr,0,strlen($rightsStr)-1);			
				}
		
				$rightsStr = str_replace("||","|",$rightsStr);				
				$updateRs = $db->executeQuery("UPDATE ET_GROUPS set DS_RIGHTS='".$rightsStr."' where PK_GROUPS=".$noData["PK_GROUPS"]);
			} // while end
		}
		$deleteRs = $db->executeQuery("DELETE from ET_DS where PK_DS=".$pk_num);
		echo '<meta http-equiv="refresh" content="0; url=./datasource_dash.php">';
	}
?>
</TABLE>
</div>
</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
