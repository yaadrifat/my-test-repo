<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Migrate Data</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Migrate Data - Standard - Step 1</div>

<?PHP
if (isset($_GET["pk_vlnk_adapmod"])) {
	$pk_vlnk_module = $_GET["pk_vlnk_adapmod"];
} else {
	$pk_vlnk_module = 0;
}

$query_sql = "select count(*) as count from velink.vlnk_adapmodcol where fk_vlnk_adapmod = ".$pk_vlnk_module;
$results = executeOCIQuery($query_sql,$ds_conn);
if ($results["COUNT"][0] == 0) {
	if ($_SERVER['REQUEST_METHOD'] != 'POST'){
	$pk_vlnk_module = $_GET["pk_vlnk_module"];
	$pk_vlnk_adapmod = $_GET["pk_vlnk_adapmod"];
	$table_name = $_GET["tablename"];

	?>
	<form name="modcol" method="post" action="mig_mod_columns.php">
	<?PHP echo "<INPUT type=\"hidden\" name=\"pk_vlnk_adapmod\" value=".$pk_vlnk_adapmod."></input>" ?>

	<Table border="1" ><TR>
	<TH>COLUMN NAME</TH>
	<TH>MAP TO</TH>
<!--	<TH>MAP TYPE</TH>
	<TH>SUB TYPE</TH> -->
	</TR>
	<?php


	$query_sql = "select column_name from dba_tab_columns where table_name = '".strtoupper($table_name)."' order by column_name";
	$results = executeOCIQuery($query_sql,$ds_conn);

	$dd_columns = '<option value=""></option>';
	for ($rec = 0; $rec < $results_nrows; $rec++){
	           $dd_columns .= "<option value=".$results["COLUMN_NAME"][$rec].">".$results["COLUMN_NAME"][$rec]."</option>";
	}

	$query_sql = "SELECT pk_vlnk_modcol,column_name,map_type,replace(map_subtyp,'''','') as map_subtyp,is_required,is_unique,modcol_notes FROM velink.VLNK_MODCOL WHERE fk_vlnk_module = ".$pk_vlnk_module." order by is_required,modcol_seq,column_name";

	$results = executeOCIQuery($query_sql,$ds_conn);

	for ($rec = 0; $rec < $results_nrows; $rec++){
	echo '<TR onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';">';
/*
	if ($results["IS_REQUIRED"][$rec] == 1){
		echo "<TR bgcolor=orange>";
	} else {
		if ($rec % 2) {
			echo '<TR class="even">';
		} else {
			echo '<TR>';
		}
	}
*/
	?>
			<TD><?php echo $results["COLUMN_NAME"][$rec] . "&nbsp;"; 
				if ($results["IS_REQUIRED"][$rec] == 1) echo "<font color='red'><b><font size='3'>*</font></b></font>";
				if ($results["MODCOL_NOTES"][$rec] != "") {
			?>
	<a href="#" class="tip"><img src="./img/info_1.png">
	<span>
	<table class="ttip" width="100%"><tr><th align=left><?PHP echo $results["COLUMN_NAME"][$rec];?></th></tr><tr><td><?PHP echo $results["MODCOL_NOTES"][$rec];?></td></tr></table>
	</span></a> 
	
	<?php 
	} else {echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";}
	?>
	</TD>
		<td><?PHP echo "<select ".(($results["IS_REQUIRED"][$rec] == 1)?" class='required' ":"")."name=\"column_name[$rec]\">".$dd_columns."</select>"; ?></td>
		<?PHP echo "<INPUT type=\"hidden\" name=\"pk_vlnk_modcol[$rec]\" value=".$results["PK_VLNK_MODCOL"][$rec]."></input>" ?>

<!--	<TD><?php echo $results["MAP_TYPE"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["MAP_SUBTYP"][$rec] . "&nbsp;"; ?></TD> -->

	<?php
	//	echo "<td><a href=mig_mod_columns.php?pk_vlnk_module=".$results["PK_VLNK_MODULE"][$rec]."&pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec].">Select</a></td>";
		echo "</TR>";
	}
	?>
	</TABLE>
	<input type="image" name="submit" value="SUBMIT" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />

	</form>
	<?php
	} else {
	$v_columns = $_POST["column_name"];
	$v_pk_vlnk_modcol = $_POST["pk_vlnk_modcol"];
	$v_sql = "delete from velink.vlnk_adapmodcol where fk_vlnk_adapmod = ".$_POST["pk_vlnk_adapmod"];
	$results = executeOCIUpdateQuery($v_sql,$ds_conn);

	for ($i = 0;$i<count($v_columns);$i++){
		$v_sql = "insert into velink.vlnk_adapmodcol (pk_vlnk_adapmodcol,fk_vlnk_modcol,map_col_name,fk_vlnk_adapmod) values (velink.seq_vlnk_adapmodcol.nextval,".$v_pk_vlnk_modcol[$i].",'".$v_columns[$i]."',".$_POST["pk_vlnk_adapmod"].")";
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);
	}
		echo "Mapping Created...";
		$query_sql = "select fk_vlnk_adaptor from velink.vlnk_adapmod where pk_vlnk_adapmod = ".$_POST["pk_vlnk_adapmod"];
		$results = executeOCIQuery($query_sql,$ds_conn);
		echo '<meta http-equiv="refresh" content="0; url=./mig_modules.php?pk_vlnk_adaptor='.$results["FK_VLNK_ADAPTOR"][0].'"> ';

	}
	?>
	      </div>


	</body>
	</html>
	<?php
	OCICommit($ds_conn);
	OCILogoff($ds_conn);
} else {

	if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
	?>
		<form name="mod_mapping" action="mig_viewmapping.php" method="post">
		<Table border="1"><TR>
		<TH>Module column name</TH>
<!--		<TH>Type</TH>
		<TH>Subtype</TH> -->
		<TH>Table Column Name</TH>
		</TR>
		<?php

		$pk_vlnk_adapmod = $_GET["pk_vlnk_adapmod"];

		echo '<INPUT type="hidden" name="pk_vlnk_adapmod" value="'.$pk_vlnk_adapmod.'"/>';

		$query_sql = "select table_name from velink.vlnk_adapmod where pk_vlnk_adapmod = ".$pk_vlnk_adapmod;
		$results = executeOCIQuery($query_sql,$ds_conn);

		$query_sql = "select column_name from dba_tab_columns where table_name = '".strtoupper($results["TABLE_NAME"][0])."' order by column_name";
		$results_cols = executeOCIQuery($query_sql,$ds_conn);
		$col_nrows = $results_nrows;


		$query_sql = "SELECT pk_vlnk_adapmodcol,column_name,map_col_name,map_type,map_subtyp,is_required,modcol_notes FROM velink.VLNK_ADAPMODCOL,velink.VLNK_MODCOL WHERE pk_vlnk_modcol = fk_vlnk_modcol AND fk_vlnk_adapmod = ".$pk_vlnk_adapmod." order by is_required,modcol_seq,column_name";

		$results = executeOCIQuery($query_sql,$ds_conn);

		for ($rec = 0; $rec < $results_nrows; $rec++){
	echo '<TR onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';">';
/*
			if ($results["IS_REQUIRED"][$rec] == 1){
				echo "<TR bgcolor=orange>";
			} else {
				if ($rec % 2) {
					echo '<TR class="even">';
				} else {
					echo '<TR>';
				}
			}
*/
	?>
			<TD><?php echo $results["COLUMN_NAME"][$rec] . "&nbsp;"; 
				if ($results["IS_REQUIRED"][$rec] == 1) echo "<font color='red'><b><font size='3'>*</font></b></font>";
				if ($results["MODCOL_NOTES"][$rec] != "") {
			?>
	<a href="#" class="tip"><img src="./img/info_1.png">
	<span>
	<table class="ttip" width="100%"><tr><th align=left><?PHP echo $results["COLUMN_NAME"][$rec];?></th></tr><tr><td><?PHP echo $results["MODCOL_NOTES"][$rec];?></td></tr></table>
	</span></a> 
	
	<?php 
	} else {echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";}
	?>
	</TD>
<!--			<TD><?php echo $results["MAP_TYPE"][$rec] . "&nbsp;"; ?></TD>
			<TD><?php echo str_replace("'","",$results["MAP_SUBTYP"][$rec]) . "&nbsp;"; ?></TD> -->
		<?PHP
			$dd_columns = '<option value=""></option>';
			for ($rec_col = 0; $rec_col < $col_nrows; $rec_col++){
				if ($results["MAP_COL_NAME"][$rec] == $results_cols["COLUMN_NAME"][$rec_col]){
			    	$dd_columns .= "<option selected value=".$results_cols["COLUMN_NAME"][$rec_col].">".$results_cols["COLUMN_NAME"][$rec_col]."</option>";
			    } else {
			    	$dd_columns .= "<option value=".$results_cols["COLUMN_NAME"][$rec_col].">".$results_cols["COLUMN_NAME"][$rec_col]."</option>";
			    }
			}
		?>
			<TD><?PHP echo "<select ".(($results["IS_REQUIRED"][$rec] == 1)?" class='required' ":"")."name=\"column_name[$rec]\">".$dd_columns."</select>"; ?></TD>
			<INPUT type="hidden" name="pk_vlnk_adapmodcol[<?PHP echo $rec; ?>]" value='<?PHP echo $results["PK_VLNK_ADAPMODCOL"][$rec] ?>'/>
		<?php
			echo "</TR>";
		}
		?>
		</TABLE>
	<input type="image" name="submit" value="SUBMIT" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />
		</form>
	<?PHP
	} else {
		$v_pk_vlnk_adapmodcol = $_POST["pk_vlnk_adapmodcol"];
		$v_column_name = $_POST["column_name"];
		$v_pk_vlnk_adapmod = $_POST["pk_vlnk_adapmod"];
		for ($i=0;$i<count($v_column_name);$i++) {
			$query = "update velink.vlnk_adapmodcol set map_col_name = '".$v_column_name[$i]."' where pk_vlnk_adapmodcol = ".$v_pk_vlnk_adapmodcol[$i];
			executeOCIUpdateQuery($query,$ds_conn);
		}
		echo "Mapping Saved.";

		$query_sql = "select fk_vlnk_adaptor from velink.vlnk_adapmod where pk_vlnk_adapmod = ".$v_pk_vlnk_adapmod;
		$results = executeOCIQuery($query_sql,$ds_conn);
		echo '<meta http-equiv="refresh" content="0; url=./mig_modules.php?pk_vlnk_adaptor='.$results["FK_VLNK_ADAPTOR"][0].'"> ';

	}
}
}
else header("location: ./index.php?fail=1");
?>
