<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Query Database</title>
</head>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

if (isset($_GET["pk_vlnk_odbc"])) {
	$v_pk_vlnk_odbc = $_GET["pk_vlnk_odbc"];
} else {
	$v_pk_vlnk_odbc = $_POST["pk_vlnk_odbc"];
}

$query = "select odbc_name,odbc_dsn,odbc_user,odbc_pass from velink.vlnk_odbc where pk_vlnk_odbc = ".$v_pk_vlnk_odbc;
$results = executeOCIQuery($query,$ds_conn);
$connaccess=odbc_connect($results["ODBC_DSN"][0],$results["ODBC_USER"][0],$results["ODBC_PASS"][0]);

?>
<body>
<div id="fedora-content">	

<div class="navigate">Import Access DB - Export</div>
<BR>


<FORM action="accesstable_export.php" method=post>

<?php


if (isset($_POST['query']) ) {
$v_query =  $_POST['query'];
} else {
if (!empty($_SESSION['query']) ) {
$v_query =  $_SESSION['query'];
} else {
if (isset($_GET["table_name"])){
$v_query = 'select * from "'.$_GET["table_name"].'"';
} else {
if (isset($_GET["distinctquery"])){
$v_query = $_GET["distinctquery"];
} else {
$v_query = "";
}
}
}
}

if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
?>
<input type="hidden" name="pk_vlnk_odbc" value="<?PHP echo $v_pk_vlnk_odbc; ?>"/>
<input type="hidden" name="query" value='<?PHP echo $v_query; ?>'/>
<table width="100%" border="0">
<tr>
<td width="15%">Export Table Name:</td>
<td><INPUT name="tablename" type="text" size="50" value="temp_<?PHP echo $_GET['table_name']; ?>"></td>
</tr>
<tr>
<td><input type=submit name=submit value="Submit"></input></td>
</tr>
</table>


</form>

<?php
} else {

	if (isset($_POST['query'])) {
		$query=stripslashes($_POST['query']);
	} else {
		$query=$v_query;
	}


	$sql="SELECT count(*) as cnt from (".$query.")";
	$rs_cnt=odbc_exec($connaccess,$sql);

	$rs=odbc_exec($connaccess,$query);

	$create_sql = "";
	
		for ($i=1;$i<=odbc_num_fields($rs);$i++){
			
			if (odbc_field_type($rs,$i) == 'VARCHAR'){
				$create_sql .= str_replace(",","",str_replace("#","",str_replace("*","",str_replace("SESSION","SESSION1",str_replace("DATE","DATE1",str_replace("SYSDATE","SYSDATE1",str_replace("GRANT","GRANT1",str_replace(" ","",strtoupper("COL_".odbc_field_name($rs,$i))))))))))." VARCHAR2(4000),";
			} 
			if (odbc_field_type($rs,$i) == 'INTEGER' || odbc_field_type($rs,$i) == 'BIT' || odbc_field_type($rs,$i) == 'COUNTER' || odbc_field_type($rs,$i) == 'REAL' || odbc_field_type($rs,$i) == 'BYTE' || odbc_field_type($rs,$i) == 'DOUBLE' || odbc_field_type($rs,$i) == 'SMALLINT'){
				$create_sql .= str_replace(",","",str_replace("#","",str_replace("*","",str_replace("SESSION","SESSION1",str_replace("DATE","DATE1",str_replace("SYSDATE","SYSDATE1",str_replace("GRANT","GRANT1",str_replace(" ","",strtoupper("COL_".odbc_field_name($rs,$i))))))))))." NUMBER,";
			} 
			if (odbc_field_type($rs,$i) == 'DATETIME'){
				$create_sql .= str_replace(",","",str_replace("#","",str_replace("*","",str_replace("SESSION","SESSION1",str_replace("DATE","DATE1",str_replace("SYSDATE","SYSDATE1",str_replace("GRANT","GRANT1",str_replace(" ","",strtoupper("COL_".odbc_field_name($rs,$i))))))))))." DATE,";
			} 
			if (odbc_field_type($rs,$i) == 'LONGCHAR'){
				$create_sql .= str_replace("COMMENT","comment1",str_replace(",","",str_replace("#","",str_replace("*","",str_replace("SESSION","SESSION1",str_replace("DATE","DATE1",str_replace("SYSDATE","SYSDATE1",str_replace("GRANT","GRANT1",str_replace(" ","",strtoupper("COL_".odbc_field_name($rs,$i)))))))))))." CLOB,";
			} 
	      }

	$tabQuery = "SELECT * FROM all_tables WHERE table_name='".strtoupper($_POST["tablename"])."'";
	$results = executeOCIQuery($tabQuery,$ds_conn);

	if($results_nrows==0){
		$create_sql = "create table eres.".$_POST["tablename"]." (".substr($create_sql,0,strlen($create_sql)-1).")";
		$create_sql = str_replace("-","_",str_replace("'","",$create_sql));
		$results = executeOCIUpdateQuery($create_sql,$ds_conn);

	echo "<BR>";
	echo "Table created: <font size=2 color=green>".$_POST["tablename"]."</font>";
	echo "<br><BR>";

	$insert_sql = "";
	$rows = 0;
	while (odbc_fetch_row($rs))
	{
		for ($i=1;$i<=odbc_num_fields($rs);$i++){
			if (odbc_field_type($rs,$i) == 'VARCHAR' || odbc_field_type($rs,$i) == 'LONGCHAR'){
		  		$insert_sql .= "'".substr(str_replace("'","''",odbc_result($rs,$i)),0,3999)."',";
			} 
			if (odbc_field_type($rs,$i) == 'INTEGER' || odbc_field_type($rs,$i) == 'BIT' || odbc_field_type($rs,$i) == 'COUNTER' || odbc_field_type($rs,$i) == 'REAL' || odbc_field_type($rs,$i) == 'BYTE' || odbc_field_type($rs,$i) == 'DOUBLE' || odbc_field_type($rs,$i) == 'SMALLINT'){
		  		$insert_sql .= (odbc_result($rs,$i) == "" ? "NULL" : odbc_result($rs,$i)).",";
			} 
			if (odbc_field_type($rs,$i) == 'DATETIME'){
		  		$insert_sql .= "to_date('".substr(odbc_result($rs,$i),0,10)."','yyyy-mm-dd'),";
			} 
	      }
	$rows = $rows + 1;
	//if ($rows == 10){exit;}
	$insert_sql = "insert into ".$_POST["tablename"]." values (".substr($insert_sql,0,strlen($insert_sql)-1).")";
	//echo $insert_sql;
	//echo "<BR>";
	$results = executeOCIUpdateQuery($insert_sql,$ds_conn);
	//echo odbc_result($rs,1);
	$insert_sql = "";
	}
	echo "Total Rows Exported: <font size=2 color=green>".$rows."</font>";
	echo "<BR><BR>";
	echo "<font size=2 color=green> Export Successful</font>";
} else {
	echo "<font size=2 color=red> Data not Exported</font>";
	echo "<BR><BR>";
	echo "<font size=2 color=red> '".$_POST["tablename"]."' Table already exists</font>";
}
	
	odbc_close($connaccess);
}
?>
</div>


</body>
</html>
<?php
OCICommit($ds_conn);
OCILogoff($ds_conn);
//}
}
else header("location: index.php?fail=1");
?>
