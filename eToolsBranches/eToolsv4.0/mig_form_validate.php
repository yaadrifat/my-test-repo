<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Migrate Data -> Forms -> Validate</title>

<?php
include("./includes/oci_functions.php");	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Migrate Data - Forms - Validate</div>

<?php
// Field validation
$pk_vlnk_adapmod = $_GET["pk_vlnk_adapmod"];
$v_linked_to = $_GET["form_linkto"];
$v_stablename = $_GET["tablename"];
$v_tablename = ($v_linked_to=='A'?"ER_IMPACCTFORM":($v_linked_to=='S'?"ER_IMPSTUDYFORM":($v_linked_to=='P'?"ER_IMPPATFORM":"")));
$v_flag = false;
echo '<table width="100%"><tr><td align="right"><a href="./mig_forms.php?pk_vlnk_adaptor='.$_REQUEST["pk_vlnk_adaptor"].'">Back to Migration Page</a></td></tr></table>';
$query_sql = "select fld_name || lookup as field_name 
from velink.vlnk_imp_formfld,er_fldlib 
where fk_vlnk_adapmod = $pk_vlnk_adapmod and 
fld_mandatory = 1 and
pk_field(+) = fk_field
and table_colname is null";
$results = executeOCIQuery($query_sql,$ds_conn);
if ($results_nrows > 0) {
	echo "<BR>Following mandatory column(s) not mapped:<BR>";
	for ($rec = 0; $rec < $results_nrows; $rec++){
		echo " - ".$results["FIELD_NAME"][$rec]."<BR>";
	}
}else{
	echo "<BR>Mandatory column mapping: <font color=blue>PASS</font><BR>";
}

// Response validation
$query_sql = "select fld_name,map_to 
from velink.vlnk_imp_formresp, velink.vlnk_imp_formfld , er_fldlib
where pk_imp_formfld = fk_imp_formfld and 
fk_vlnk_adapmod = $pk_vlnk_adapmod and 
pk_field = fk_field and
fldresp is null";
$results = executeOCIQuery($query_sql,$ds_conn);
if ($results_nrows > 0) {
	$v_flag = true;
	echo "<BR>Following responses not mapped:<BR>";
	echo "<table border=1 width=100%><tr><th>Field name</th><th>Response</th></tr>";
	for ($rec = 0; $rec < $results_nrows; $rec++){
		echo "<tr><td>".$results["FLD_NAME"][$rec]."</td><td>".$results["MAP_TO"][$rec]."</td></tr>";
	}
	echo "</table>";
}else{
	echo "<BR>Responses validation:<font color=blue>PASS</font><BR>";
}


// Date validation
/*$query_sql = "select table_colname
from velink.vlnk_imp_formfld a
where fk_vlnk_adapmod = $pk_vlnk_adapmod and 
a.fld_type = 'Date' and table_colname <> 'trunc(SYSDATE)' and table_colname is not null and FLD_MANDATORY =1";*/
$query_sql = "select table_colname
from velink.vlnk_imp_formfld a
where fk_vlnk_adapmod = $pk_vlnk_adapmod and 
a.fld_type = 'Date' and table_colname <> 'trunc(SYSDATE)' and table_colname is not null and FLD_MANDATORY =1";
$results = executeOCIQuery($query_sql,$ds_conn);
$trows = $results_nrows;
echo "<BR>Date Validation:<i> (valid date format - MM/DD/YYYY)</i>";
echo "<table border=1 width=100%>";
echo "<tr><th>Error Type</th><th>Column Name</th><th>Value</th><th>&nbsp;</th></tr>";
if ($trows > 0) $v_flag = true;
for ($rec = 0; $rec < $trows; $rec++){
	$query_sql = "select distinct ".$results["TABLE_COLNAME"][$rec]." as colname from $v_stablename";
	$results1 = executeOCIQuery($query_sql,$ds_conn);
	$v_dates = "";
	for ($rec1 = 0; $rec1 < $results_nrows; $rec1++){
		$date = $results1["COLNAME"][$rec1];
		$dateParse = explode("/",$date);
		$validdate = true;
		if (count($dateParse) == 3) {
			list($month, $day, $year) = split('/', $date);
			if (!checkdate($month,$day,$year) || strlen($year) < 4) {
				$validdate = false;
			}
		} else {
				$validdate = false;
		}
		if (!$validdate) {
			$v_dates .= $date.", ";
		}
	}
	if (strlen($v_dates) > 0) {
		echo "<tr><td>Invalid date format<b></td><td>".$results["TABLE_COLNAME"][$rec]."</td></b><td>".substr($v_dates,0,strlen($v_dates)-2).'</td></tr>';
/*echo "<tr><td>Invalid date format<b></td><td>".$results["TABLE_COLNAME"][$rec]."</td></b><td>".substr($v_dates,0,strlen($v_dates)-2).'</td><td><A HREF="mig_validate_edit.php?pk_vlnk_adapmod='.$pk_vlnk_adapmod.'">Edit</td></tr>';*/
		//echo "<tr><td><TEXTAREA ROWS=5 COLS=80>".substr($v_dates,0,strlen($v_dates)-2)."</TEXTAREA></tr></td></table>";
	}


/*	if (strlen($v_dates) > 0) {
		echo "<BR><table><tr><td>Invalid data format (mm/dd/yyyy) in field: <b>".$results["TABLE_COLNAME"][$rec]."</b></td></tr><BR>";
		echo "<tr><td><TEXTAREA ROWS=5 COLS=80>".substr($v_dates,0,strlen($v_dates)-2)."</TEXTAREA></tr></td></table>";
	}*/
}
echo "</table>";

// Number validation
$query_sql = "select table_colname
from velink.vlnk_imp_formfld a
where fk_vlnk_adapmod = $pk_vlnk_adapmod and 
a.fld_type = 'Number' and table_colname is not null";
$results = executeOCIQuery($query_sql,$ds_conn);
$trows = $results_nrows;
echo "<BR>Number Validation:<BR>";
echo "<table border=1 width=100%>";
echo "<tr><th>Error Type</th><th>Column Name</th><th>Value</th><th>&nbsp;</th></tr>";
if ($trows > 0) $v_flag = true;
for ($rec = 0; $rec < $trows; $rec++){
	$query_sql = "select distinct trim(".$results["TABLE_COLNAME"][$rec].") as colname from $v_stablename";
	$results1 = executeOCIQuery($query_sql,$ds_conn);
	$v_num = "";
	for ($rec1 = 0; $rec1 < $results_nrows; $rec1++){
		if (!is_numeric($results1["COLNAME"][$rec1])) {
			$v_num .= $results1["COLNAME"][$rec1].', ';
		}
	}
	if (strlen($v_num) > 0) {
		echo "<TR>";
		echo "<TD>Invalid number</td><td>".$results["TABLE_COLNAME"][$rec]."</td><td>".substr($v_num,0,strlen($v_num)-2).'</td>';
/*		echo "<TD>Invalid number</td><td>".$results["TABLE_COLNAME"][$rec]."</td><td>".substr($v_num,0,strlen($v_num)-2).'</td><td><A HREF="mig_validate_edit.php?pk_vlnk_adaptor='.$v_pk_vlnk_adaptor.'&pk_vlnk_pipe='.$results["PK_VLNK_PIPE"][$rec].'&colname='.$v_pipe_colname[$i].'&pk_vlnk_adapmod='.$pk_vlnk_adapmod.'">Edit</td>';*/

//		echo '<td><A HREF="mig_validate_edit.php?pk_vlnk_adaptor='.$v_pk_vlnk_adaptor.'&pk_vlnk_pipe='.$results1["PK_VLNK_PIPE"][$rec1].'&colname='.$colname.'&pk_vlnk_adapmod='.$pk_vlnk_adapmod.'">Edit</A></td>' ; 
		echo "</TR>";

//		echo "<table><tr><td>Invalid number format in field: <b>".$results["TABLE_COLNAME"][$rec]."</b></td></tr>";
//		echo "<tr><td><TEXTAREA ROWS=5 COLS=80>".substr($v_num,0,strlen($v_num)-2)."</TEXTAREA></tr></td></table>";
	}
}
echo "</table>";

// Patient validation
$query_sql = "select fk_account from velink.vlnk_adapmod,velink.vlnk_adaptor where pk_vlnk_adaptor = fk_vlnk_adaptor and pk_vlnk_adapmod = $pk_vlnk_adapmod";
$results = executeOCIQuery($query_sql,$ds_conn);
$v_account = $results["FK_ACCOUNT"][0];
$query_sql = "select table_colname
from velink.vlnk_imp_formfld a
where fk_vlnk_adapmod = $pk_vlnk_adapmod and 
table_colname is not null and lookup='PATIENT'";
$results = executeOCIQuery($query_sql,$ds_conn);
$trows = $results_nrows;
for ($rec = 0; $rec < $trows; $rec++){
	$query_sql = "select distinct ".$results["TABLE_COLNAME"][$rec]." as colname from $v_stablename where not exists (select 9 from er_per where fk_account = $v_account and per_code = ".$results["TABLE_COLNAME"][$rec].")";
	$results1 = executeOCIQuery($query_sql,$ds_conn);
	$v_pat = "";
	if ($results_nrows > 0) $v_flag = true;
	for ($rec1 = 0; $rec1 < $results_nrows; $rec1++){
		$v_pat .= $results1["COLNAME"][$rec1].', ';
	}
	if (strlen($v_pat) > 0) {
		echo "<BR>Missing patients:<BR><table>";
		echo "<tr><td><TEXTAREA ROWS=5 COLS=80>".substr($v_pat,0,strlen($v_pat)-2)."</TEXTAREA></tr></td></table>";
	}
}


// Study validation
$query_sql = "select table_colname
from velink.vlnk_imp_formfld a
where fk_vlnk_adapmod = $pk_vlnk_adapmod and 
table_colname is not null and lookup='STUDY'";
$results = executeOCIQuery($query_sql,$ds_conn);
$trows = $results_nrows;
for ($rec = 0; $rec < $trows; $rec++){
	$query_sql = "select distinct ".$results["TABLE_COLNAME"][$rec]." as colname from $v_stablename where not exists (select 9 from er_study where fk_account = $v_account and study_number = ".$results["TABLE_COLNAME"][$rec].")";
	$results1 = executeOCIQuery($query_sql,$ds_conn);
	$v_study = "";
	if ($results_nrows > 0) $v_flag = true;
	for ($rec1 = 0; $rec1 < $results_nrows; $rec1++){
		$v_study .= $results1["COLNAME"][$rec1].', ';
	}
	if (strlen($v_study) > 0) {
		echo "<BR>Missing studies:<BR><table>";
		echo "<tr><td><TEXTAREA ROWS=5 COLS=80>".substr($v_study,0,strlen($v_study)-2)."</TEXTAREA></tr></td></table>";
	}
}

/*if (!$v_flag) {
	echo "No validation errors found.";
}*/

?>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
