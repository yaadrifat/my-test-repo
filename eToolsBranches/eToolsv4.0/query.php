<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Execute Query</title>
<style>
textarea
{
  font-size: 10px;
  color: #0000ff;
  font-family: courier
}
</style>	
</head>
<?php

include("./includes/header.php");

include_once "./adodb/adodb.inc.php";
include_once('./adodb/adodb-pager.inc.php'); 

$db = NewADOConnection("oci8");
$db->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);


if (!$db) die("Connection failed");
	
?>


<body onLoad="document.equery.query.focus();">
<div id="fedora-content">	

<div class="navigate">Execute Query</div>
<FORM name="equery" action="query.php" method=post>

<?php


if (isset($_GET['pk_queryman']) ) {
	$v_pk =  $_GET['pk_queryman'];
	$recordSet = $db->Execute("select query from velink.vlnk_queryman where pk_queryman = ".$v_pk);
	$v_query =  $recordSet->fields[0];
	$_SESSION['query'] = $recordSet->fields[0];
} else {
	if (isset($_POST['query']) ) {
		$v_query =  stripslashes($_POST['query']);
		$_SESSION['query'] = stripslashes($_POST['query']);

	} else {
		if (isset($_SESSION['query']) ) {
			$v_query =  stripslashes($_SESSION['query']);
		} else {
			$v_query =  "";
			$_SESSION['query'] = "";
		}
	}
}
?>

<table width="100%" border="0">
<tr>
<td colspan="2"><Textarea name="query" type="text" cols=110 maxlength=10000 rows=10><?PHP echo $v_query; ?></textarea>
</td>
</tr>
</table>

<input type="image" name="submit" value="Submit" src="./img/equery.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/equery_m.png';" onMouseOut="this.src='./img/equery.png';" />

</form>
<style>
table {border-collapse:separate;}
</style>

<style>DIV.tablerows {
height:380px;
width:900px;
overflow: auto;
align:left;
}</style>

<?php
if (isset($_POST['submit']) || strlen($_SESSION['query']) > 0	) {	

$query=$_SESSION['query'];

if (strtoupper(substr(trim($query),0,6)) == 'SELECT') {
	$rs = $db->Execute($query);
	if (!$rs) {
		echo "SQL ERROR: ";
	    print $db->ErrorMsg();
	} else {
	$pager = new ADODB_Pager($db,$query);
	$pager->htmlSpecialChars = false;
//	$pager->Render($rows_per_page=10); 
	echo "<table width='100%'><tr><td>";
	echo "<div class='tablerows'>";
	$pager->Render($rows_per_page=100); 
	echo "</div></td></tr></table>";
	}
} else {
	echo "Only SELECT statement allowed.";
}
?>


</TABLE>
<?php
} ?>
</div>


</body>
</html>
<?php 
$db->Close();

}
else header("location: index.php?fail=1");
?>
