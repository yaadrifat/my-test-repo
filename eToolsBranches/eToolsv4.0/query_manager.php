<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Query Manager</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 



?>

</head>
<script src="js/warning/jquery.min.js" type="text/javascript"></script>
<script src="js/warning/jquery-ui.min.js"></script>
<script src="js/warning/jquery.easy-confirm-dialog.js"></script>
<link rel="stylesheet" href="css/jquery-ui.css" type="text/css" />
<script type="text/javascript">
	$(function() {
		$(".confirm").easyconfirm();
		$("#alert").click(function() {
			alert("You approved the action");
		});
		
		
		var QueryTable = document.getElementById("QueryTable").rows.length;
		QueryTableLen = QueryTable-1;
		for(i=0;i<QueryTableLen;i++){
			$("#Publish_"+i).easyconfirm1({locale: { title: 'Warning Message', button: ['No','Yes']}});
			$("#Publish_"+i).click(function() {
				var pk_queryman = $(this).attr('pk_queryman');
				window.open("query_adodb_xsl.php?pk_queryman="+pk_queryman,'_self')
			});
			
			$("#CSV_"+i).easyconfirm({locale: { title: 'Warning Message', button: ['No','Yes']}});
			$("#CSV_"+i).click(function() {
				var pk_queryman = $(this).attr('pk_queryman');
				var xlsname = $(this).attr('xlsname');
				window.open("query_adodb_xls.php?pk_queryman="+pk_queryman+'&name='+xlsname,'_self')
			});
		
		}
	});
	
	</script>		
<body>

<div id="fedora-content">	
<div class="navigate">Query Manager</div>
<?php
echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<a href=query_adodb.php?mode=n>New Query</a>";

//echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=query_report_manager.php>Published Reports</a>";
//echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=query_formview.php>Create View from Form</a>";


?>
<Table border="1" width="100%" id="QueryTable"><TR>
<TH width="20%">QUERY NAME</TH>
<TH width="50%">DESCRIPTION</TH>
<TH width="10%">&nbsp;</TH>
<TH width="10%">&nbsp;</TH>
<TH width="10%">Export</TH>

</TR>
<?php

$query_sql = "SELECT pk_queryman,query_name,query_desc from velink.vlnk_queryman where nvl(record_type,'Y') <> 'D' order by lower(query_name)" ;

$results = executeOCIQuery($query_sql,$ds_conn);

for ($rec = 0; $rec < $results_nrows; $rec++){
?>
	<TR onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
	<TD><?php echo $results["QUERY_NAME"][$rec]; ?></TD>
	<TD><?php echo $results["QUERY_DESC"][$rec] . "&nbsp;"; ?></TD>
<?php
	echo "<td align=center><a href=query_adodb.php?pk_queryman=".$results["PK_QUERYMAN"][$rec].">Edit</a></td>";
	echo "<td align=center><a href=# id=Publish_".$rec." pk_queryman=".$results["PK_QUERYMAN"][$rec].">Publish</a></td>";
	//echo "<td align=center><a href=query_adodb_xsl.php?pk_queryman=".$results["PK_QUERYMAN"][$rec].">Publish</a></td>";
	//echo "<td align=center><a href=query_adodb_xls.php?pk_queryman=".$results["PK_QUERYMAN"][$rec]."&name=".urlencode($results["QUERY_NAME"][$rec]).">CSV</a></td>";
	echo "<td align=center><a href=# id=CSV_".$rec." pk_queryman=".$results["PK_QUERYMAN"][$rec]." xlsname=".urlencode($results["QUERY_NAME"][$rec]).">CSV</a></td>";
	echo "</TR>";
}
?>
</TABLE>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
