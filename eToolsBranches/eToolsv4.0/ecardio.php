<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> eCardio</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">eCardio</div>
<br>
<b>ACC-NCDR ICD Registry</b>

<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="icd_update_lookup.php">Update Lookup (Device/Med/AE)</a>
<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="icd_chpwd.php">Change Data Submission Password</a>
<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="icd_harvest.php">Harvest</a>
<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="icd_harvest_view.php">View Harvest Files</a>

<br><br><br>
<b>STS Adult Cardiac Surgery Registry</b>

<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="sts_acs_update_lookup.php">Upload Files (Valve Devices/VAD Devices/Risk Model Coefficients)</a>
<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="sts_acs_harvest.php">Harvest</a>
<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="sts_acs_harvest_view.php">View Harvest Files</a>
<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="sts_acs_datacomp.php">Change Data Completeness Criteria</a>



</div>

</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
