<?php
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){

include_once "./adodb/adodb.inc.php";
include_once('./adodb/adodb-pager.inc.php'); 
include_once('./adodb/toexport.inc.php'); 

$db = NewADOConnection("oci8");
$db->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);

if (!$db) die("Connection failed");

$v_sql = urldecode($_GET["data"]);
$rs = $db->Execute($v_sql);
if (!$rs) {
	echo "SQL ERROR: ";
	print $db->ErrorMsg();
} else {
	header("Content-type: application/x-msexcel");
	header("Content-Disposition: attachment; filename=data.csv");
	header("Pragma: public");
	header("Expires: 0");
	print rs2csv($rs); 
}

$db->Close();
}
else header("location: index.php?fail=1");
?>
