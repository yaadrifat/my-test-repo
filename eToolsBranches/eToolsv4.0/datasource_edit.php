<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> New Datasource</title>
	<link href="css/enhancement.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript">
	
		function VerfiyValidation() {
			if(document.getElementById("dsname").value == ''){
				alert("DS Name should not be empty");
				document.getElementById("dsname").focus();
				return false;
			}
			if(document.getElementById("dshost").value == ''){
				alert("DS Host should not be empty");
				document.getElementById("dshost").focus();
				return false;
			} else {
				errorString = "";
				theName = "IPaddress";
				
				var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
				var ipArray = document.getElementById("dshost").value.match(ipPattern);
				
				if (document.getElementById("dshost").value == "0.0.0.0")
				errorString = errorString + theName + ': '+document.getElementById("dshost").value+' is a special IP address and cannot be used here.';
				else if (document.getElementById("dshost").value == "255.255.255.255")
				errorString = errorString + theName + ': '+document.getElementById("dshost").value+' is a special IP address and cannot be used here.';
				if (ipArray == null)
				errorString = errorString + theName + ': '+document.getElementById("dshost").value+' is not a valid IP address.';
				else {
				for (i = 0; i < 4; i++) {
				thisSegment = ipArray[i];
				if (thisSegment > 255) {
				errorString = errorString + theName + ': '+document.getElementById("dshost").value+' is not a valid IP address.';
				i = 4;
				}
				if ((i == 0) && (thisSegment > 255)) {
				errorString = errorString + theName + ': '+document.getElementById("dshost").value+' is a special IP address and cannot be used here.';
				i = 4;
				}
				}
				}
				extensionLength = 3;
				if (errorString != ""){
					alert (errorString);
					document.getElementById("dshost").focus();
					return false;
				}
			}
			if(document.getElementById("dsport").value == ''){
				alert("DS Port should not be empty");
				document.getElementById("dsport").focus();
				return false;
			} else {
				var numericExpression = /^[0-9]+$/;
				if(document.getElementById("dsport").value.match(numericExpression)){
					document.getElementById("dssid").focus();
				} else {
				alert("DS Port should be numeric");
					document.getElementById("dsport").focus();
					return false;
				}
			}
			if(document.getElementById("dssid").value == ''){
				alert("DS SID should not be empty");
				document.getElementById("dssid").focus();
				return false;
			}
			if(document.getElementById("pass1").value == ''){
				alert("DS Password should not be empty");
				document.getElementById("pass1").focus();
				return false;
			}
			if(document.getElementById("pass2").value == ''){
				alert("DS RePassword should not be empty");
				document.getElementById("pass2").focus();
				return false;
			}
			if(document.getElementById("pass1").value != document.getElementById("pass2").value){
				alert("DS Password and DS RePassword should be same");
				document.getElementById("pass1").focus();
				return false;
			}
		}
	</script>
<?php
include("./includes/oci_functions.php");
include("./includes/header.php");
include ("./txt-db-api.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
</head>
<body>

<div id="fedora-content">	

<?PHP
if (isset($_GET["mode"])) $v_mode = $_GET["mode"];
if (isset($_POST["mode"])) $v_mode = $_POST["mode"];

if (!($_SERVER['REQUEST_METHOD'] == 'POST')) {
	if ($v_mode == 'n'){
		$v_dsname = "";
		$v_dshost = "";
		$v_dsport = "";
		$v_dssid = "";
	} elseif($v_mode == 'm') {
		$refNum = $_GET["pk_ds"];
		$db = new Database("etools");
		$rs = $db->executeQuery("SELECT PK_DS,DS_NAME,DS_HOST,DS_PORT,DS_SID,DS_PASS FROM ET_DS where PK_DS=".$refNum);
		$rs->next();
		$data = $rs->getCurrentValuesAsHash();
		$v_pkds = $data["PK_DS"];
		$v_dsname = $data["DS_NAME"];
		$v_dshost = $data["DS_HOST"];
		$v_dsport = $data["DS_PORT"];
		$v_dssid = $data["DS_SID"];	
		$v_pass = $data["DS_PASS"];
		?>
		<script type="text/javascript">
		function VerfiyValidation() {
		if(document.getElementById("dsname").value == ''){
				alert("DS Name should not be empty");
				document.getElementById("dsname").focus();
				return false;
			}
			if(document.getElementById("dshost").value == ''){
				alert("DS Host should not be empty");
				document.getElementById("dshost").focus();
				return false;
			} else {
				errorString = "";
				theName = "IPaddress";
				
				var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
				var ipArray = document.getElementById("dshost").value.match(ipPattern);
				
				if (document.getElementById("dshost").value == "0.0.0.0")
				errorString = errorString + theName + ': '+document.getElementById("dshost").value+' is a special IP address and cannot be used here.';
				else if (document.getElementById("dshost").value == "255.255.255.255")
				errorString = errorString + theName + ': '+document.getElementById("dshost").value+' is a special IP address and cannot be used here.';
				if (ipArray == null)
				errorString = errorString + theName + ': '+document.getElementById("dshost").value+' is not a valid IP address.';
				else {
				for (i = 0; i < 4; i++) {
				thisSegment = ipArray[i];
				if (thisSegment > 255) {
				errorString = errorString + theName + ': '+document.getElementById("dshost").value+' is not a valid IP address.';
				i = 4;
				}
				if ((i == 0) && (thisSegment > 255)) {
				errorString = errorString + theName + ': '+document.getElementById("dshost").value+' is a special IP address and cannot be used here.';
				i = 4;
				}
				}
				}
				extensionLength = 3;
				if (errorString != ""){
					alert (errorString);
					document.getElementById("dshost").focus();
					return false;
				}
			}
			if(document.getElementById("dsport").value == ''){
				alert("DS Port should not be empty");
				document.getElementById("dsport").focus();
				return false;
			} else {
				var numericExpression = /^[0-9]+$/;
				if(document.getElementById("dsport").value.match(numericExpression)){
					document.getElementById("dssid").focus();
				} else {
				alert("DS Port should be numeric");
					document.getElementById("dsport").focus();
					return false;
				}
			}
			if(document.getElementById("dssid").value == ''){
				alert("DS SID should not be empty");
				document.getElementById("dssid").focus();
				return false;
			}
			if(document.getElementById("pass1").value == ''){
				alert("DS Password should not be empty");
				document.getElementById("pass1").focus();
				return false;
			}
			if(document.getElementById("pass2").value == ''){
				alert("DS RePassword should not be empty");
				document.getElementById("pass2").focus();
				return false;
			}
			if(document.getElementById("pass1").value != document.getElementById("pass2").value){
				alert("DS Password and DS RePassword should be same");
				document.getElementById("pass1").focus();
				return false;
			}
			if(document.getElementById("pass1").value != document.getElementById("v_pass").value){
				alert("Password is wrong, Please enter a valid DB password");
				document.getElementById("pass1").focus();
				return false;
			}
		}
		</script>
		<?php		
	}else{ //this is for change password key is 'c'
		$refNum = $_GET["pk_ds"];
		$db = new Database("etools");
		$rs = $db->executeQuery("SELECT PK_DS,DS_NAME,DS_HOST,DS_PORT,DS_SID,DS_PASS FROM ET_DS where PK_DS=".$refNum);
		$rs->next();
		$data = $rs->getCurrentValuesAsHash();
		$v_pkds = $data["PK_DS"];
		$v_dsname = $data["DS_NAME"];
		$v_dshost = $data["DS_HOST"];
		$v_dsport = $data["DS_PORT"];
		$v_dssid = $data["DS_SID"];	
		$v_pass = $data["DS_PASS"];
	}
	
	$dbs = new Database("etools");
	$records = $dbs->executeQuery("SELECT DS_RIGHTS FROM ET_GROUPS where PK_GROUPS=1");
	if($records->getRowCount() >0){
		$tempVal = 0;
			while($records->next()){
				$data = $records->getCurrentValuesAsHash();				
				$dsRights = explode("|",$data['DS_RIGHTS']);	
				$tempVal++;
			}
		}
//	list($pk_ds, $ds)=explode(":",$dsRights[$v_pkds]);
	
	for($di=0; $di<count($dsRights); $di++){
		list($pk_ds, $tmp)=explode(":",$dsRights[$di]);
		if($pk_ds==$v_pkds){
			$ds=$tmp;
		}
	}
	
	if ($v_mode == 'n'){
		echo '<div class="navigate">Datasource - New</div>';
	} elseif($v_mode == 'm') {
		echo '<div class="navigate">Datasource - Edit</div>';
	}else{
		echo '<div class="navigate">Datasource - Change Password</div>';	
	}
	
	// this is for change passwor
	if($v_mode=='c'){ ?>
		<script type="text/javascript">
		function VerfiyValidation() {
			if(document.getElementById("chp_1").value != document.getElementById("v_pass").value){
				alert("Password is wrong, Please enter a valid DB password");
				document.getElementById("chp_1").focus()
				return false;
			}
			if(document.getElementById("chp_2").value == ''){
				alert("DS Password should not be empty");
				document.getElementById("chp_2").focus();
				return false;
			}
			if(document.getElementById("chp_3").value == ''){
				alert("DS RePassword should not be empty");
				document.getElementById("chp_3").focus();
				return false;
			}
			if(document.getElementById("chp_2").value != document.getElementById("chp_3").value){
				alert("DS Password and DS RePassword should be same");
				document.getElementById("chp_2").focus();
				return false;
			}
		}
		</script>
		<form id="changepass" name="edituser" action="datasource_edit.php" method="POST" onSubmit="return VerfiyValidation();">
			<input type='hidden' name='mode' value=<?PHP echo $v_mode; ?>>
			<input type='hidden' name='pk_ds' value=<?PHP echo $refNum; ?>>
			<input type='hidden' name='v_pass' value=<?PHP echo $v_pass; ?>>
			<BR><table width="700">
			<tr>
			  <td width="122">DS Name: </td>
			  <td width="555" colspan="2"><input class="textdisblae" id="dsname" type = "text" size="40" maxlength="25" name="dsname" value="<?PHP echo trim($v_dsname); ?>" disabled="disabled"></td></tr>
			<tr>
			  <td>DS Host : </td>
			  <td colspan="2"><input class="textdisblae" type = "text" size="40" maxlength="15" name="dshost" value="<?PHP echo trim($v_dshost); ?>"  disabled="disabled"></td></tr>
			<tr>
			  <td>DS Port : </td>
			  <td colspan="2"><input class="textdisblae" type = "text" size="40" name="dsport" value="<?PHP echo trim($v_dsport); ?>"  disabled="disabled"></td></tr>
			<tr>
			  <td>DS SID : </td>
			  <td colspan="2"><input class="textdisblae" type = "text" size="40" name="dssid" value="<?PHP echo trim($v_dssid); ?>"  disabled="disabled"></td></tr>
			
			<tr><?php 
				if($ds=="0"){?>
					  <td>DS Status : </td>
					  <td colspan="2">
					  <input class="textdisblae" name="dsstat" type="text" value="Disabled" disabled="disabled">
					  </td>					
				<?php }else{ ?>
					  <td>DS Status : </td>
					  <td colspan="2">
					  <input class="textdisblae" name="dsstat" type="text" value="Enabled" disabled="disabled"></td>										
				<?php }
			?>
			  </tr>
			<tr>
			  <td>DS Existing Password : </td>
			  <td colspan="2"><input name="chp_1" type="password" size="30" <?php echo (($v_mode == 'c') ? " class = 'required'" : "")?>type="password" class="required"/></td>
			</tr>

			<tr>
			  <td>DS New Password: </td>
			  <td colspan="2"><input name="chp_2" type="password" size="30" <?php echo (($v_mode == 'c') ? " class = 'required'" : "")?>type="password" class="required"/></td>
			</tr>

			<tr>
			  <td>DS Reenter Password: </td>
			  <td colspan="2"><input name="chp_3" type="password" size="30" <?php echo (($v_mode == 'c') ? " class = 'required'" : "")?>type="password" class="required"/></td>
			</tr>			
			<tr>
			  <td>&nbsp;</td>
			  <td colspan="2"><?php if(@$_GET["blank"]) echo "<font size=2 color=red>Fields cannot be empty</font>"; ?><?php if(@$_GET["pass"]) echo "<font size=2 color=red>Password mismatch</font>"; ?><?php if(@$_GET["mm"]) echo "<font size=2 color=red>Given Password is not matching with existing password</font>"; ?></td>
			  </tr>
			</table>
				<input type="image" src="./img/submit.png" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';"></form>
<?php	}else{
	?>
		<form id="edituser" name="edituser" action="datasource_edit.php" method="POST" onSubmit="return VerfiyValidation();">
			<input type='hidden' name='mode' value=<?PHP echo $v_mode; ?>>
			<input type='hidden' name='pk_ds' value=<?PHP echo $refNum; ?>>
			<input type='hidden' name='v_pass' value=<?PHP echo $v_pass; ?>>
			<BR><table width="700">

			<tr>
			  <td width="122">DS Name: </td>
			  <td width="555" colspan="2"><input name="dsname" type = "text" id="dsname" value="<?PHP echo $v_dsname; ?>" size="25" maxlength="25">
			  <span style="font-size:11px; font-style:italic; color:#999999;"> (Maximum Length: 25 Characters)</span></td></tr>
			<tr>
			  <td>DS Host : </td>
			  <td colspan="2"><input type = "text" size="40" maxlength="15" name="dshost" id="dshost" value="<?PHP echo $v_dshost; ?>"></td></tr>
			<tr>
			  <td>DS Port : </td>
			  <td colspan="2"><input type = "text" size="40" name="dsport" id="dsport" value="<?PHP echo $v_dsport; ?>"></td></tr>
			<tr>
			  <td>DS SID : </td>
			  <td colspan="2"><input type = "text" size="40" name="dssid" id="dssid" value="<?PHP echo $v_dssid; ?>"></td></tr>
			
			<tr><?php 
				if($ds=="0"){?>
					  <td>DS Status : </td>
					  <td colspan="2">
						<input  name="dsstat" type="radio" value="1">
					  Enable 
					  <input name="dsstat" type="radio" value="0" checked="checked">
					  Disable</td>					
				<?php }else{ ?>
					  <td>DS Status : </td>
					  <td colspan="2">
						<input name="dsstat" type="radio" value="1" checked="checked">
					  Enable 
					  <input name="dsstat" type="radio" value="0">
					  Disable</td>					
					
				<?php }
			?>
			  </tr>
			<tr>
			  <td>DS Password : </td>
			  <td colspan="2"><input name="pass1" type="password" id="pass1" size="20" <?php echo (($v_mode == 'n') ? " class = 'required'" : "")?>type="password" class="required"/></td>
			</tr>

			<tr>
			  <td>DS Reenter Password: </td>
			  <td colspan="2"><input name="pass2" type="password" id="pass2" size="20" <?php echo (($v_mode == 'n') ? " class = 'required'" : "")?>type="password" class="required"/></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td colspan="2"><?php if(@$_GET["blank"]) echo "<font size=2 color=red>Fields cannot be empty</font>"; ?><?php if(@$_GET["pass"]) echo "<font size=2 color=red>Password mismatch</font>"; ?><?php if(@$_GET["mm"]) echo "<font size=2 color=red>Given Password is not matching with existing password</font>"; ?><?php if(@$_GET["pempty"]) echo "<font size=2 color=red> Password fields cannot be left blank</font>"; ?></td>
			  </tr>
			</table>
				<input type="image" src="./img/submit.png" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';"></form>
		
	<?PHP
	}
} else {
	$v_pass = $_POST["pass1"];
	$v_pass2 = $_POST["pass2"];
	$v_mode = $_POST["mode"];

	$v_ch_pe = $_POST['chp_1'];
	$v_ch_pn = $_POST['chp_2'];
	$v_ch_pr = $_POST['chp_3'];	
		
	$v_pkds = $_POST["pk_ds"];
	$v_dsname = $_POST["dsname"];
	$v_dshost = $_POST["dshost"];
	$v_dsport = $_POST["dsport"];
	$v_dssid = $_POST["dssid"];
	$v_dssta = $_POST["dsstat"];
	
	switch($v_mode){
		case "n":
			if($v_dsname=="" || $v_dshost=="" || $v_dsport=="" || $v_dssid=="" || $v_pass=="" || $v_pass2==""){
				header("location:datasource_edit.php?mode=n&blank=1");	
			}else{
				if($v_pass == $v_pass2){
					$db = new Database("etools");			
					$rightsResult = $db->executeQuery("SELECT DS_RIGHTS from ET_GROUPS where PK_GROUPS=1");
					$rightsResult->next();
					$data = $rightsResult->getCurrentValuesAsHash();	
								
					$maxNum = $db->executeQuery("SELECT MAX(PK_DS) as pk_ds from ET_DS");
					$maxNum->next();
					$maxNumarray = $maxNum->getCurrentValuesAsHash();
								
					if($maxNumarray==0){
						$maxNum = $maxNumarray["pk_ds"]+1;
						$dsright = $maxNum.":".$v_dssta;								
						$result = $db->executeQuery("SELECT DS_NAME from ET_DS where DS_NAME='$v_dsname'");
						if ($result->getRowCount() > 0) {
							echo "Datasource is already exists";
							echo '<meta http-equiv="refresh" content="3; url=./datasource_dash.php">';
						} else {
							$rs = $db->executeQuery("INSERT INTO ET_DS (DS_NAME,DS_HOST,DS_PORT,DS_SID,DS_PASS) VALUES ('".$v_dsname."','".$v_dshost."','".$v_dsport."','".$v_dssid."','".$v_pass."')");			
							$dsrights = $db->executeQuery("UPDATE ET_GROUPS set DS_RIGHTS ='".$dsright."' where PK_GROUPS =1");
							echo "Data Saved.";
							echo '<meta http-equiv="refresh" content="0; url=./datasource_dash.php">';
						}								
					}else{
						$maxNum = $maxNumarray["pk_ds"]+1;
						$dsright = $data["DS_RIGHTS"]."|".$maxNum.":".$v_dssta;						
						$result = $db->executeQuery("SELECT DS_NAME from ET_DS where DS_NAME='$v_dsname'");				
						if ($result->getRowCount() > 0) {
							echo "Datasource is already exists";
							echo '<meta http-equiv="refresh" content="3; url=./datasource_dash.php">';
						} else {
							$rs = $db->executeQuery("INSERT INTO ET_DS (DS_NAME,DS_HOST,DS_PORT,DS_SID,DS_PASS) VALUES ('".$v_dsname."','".$v_dshost."','".$v_dsport."','".$v_dssid."','".$v_pass."')");			
							$dsrights = $db->executeQuery("UPDATE ET_GROUPS set DS_RIGHTS ='".$dsright."' where PK_GROUPS =1");
							echo "Data Saved.";
							echo '<meta http-equiv="refresh" content="0; url=./datasource_dash.php">';
						}	
					}				
				}else{
					header("location:datasource_edit.php?mode=n&pass=1&pk_ds=".$v_pkds);
				}		
			}										
		break;
		
		case "m":
			if($v_dsname=="" || $v_dshost=="" || $v_dsport=="" || $v_dssid=="" || $v_pass=="" || $v_pass2==""){
				header("location:datasource_edit.php?mode=m&blank=1&pk_ds=".$v_pkds);				
			}else{
//				if (!empty($v_pass) && !empty($v_pass2)){
				if ($v_pass == $v_pass2){
					$rs = $db->executeQuery("SELECT DS_PASS from ET_DS where PK_DS='$v_pkds'");
					$rs->next();
					$dspass = $rs->getCurrentValuesAsHash();
					if($dspass["DS_PASS"] == $v_pass){
						$rs = $db->executeQuery("UPDATE ET_DS set DS_PASS = '".$v_pass."' where PK_DS = ".$v_pkds);	
						$rs = $db->executeQuery("UPDATE ET_DS set DS_NAME = '".$v_dsname."',DS_HOST = '".$v_dshost."',DS_PORT = '".$v_dsport."',DS_SID = '".$v_dssid."' where PK_DS = ".$v_pkds);		
						
						$noDs = $db->executeQuery("SELECT PK_GROUPS from ET_GROUPS");		
						if($noDs->getRowCount() >0){
							while($noDs->next()){
								$noData = $noDs->getCurrentValuesAsHash();
								$dsrs = $db->executeQuery("SELECT DS_RIGHTS FROM ET_GROUPS where PK_GROUPS=".$noData["PK_GROUPS"]);
								if($dsrs->getRowCount()>0){
									while($dsrs->next()){
										$data = $dsrs->getCurrentValuesAsHash();
										$dsRi = explode("|",$data['DS_RIGHTS']);
									}
								}					
								$finalDs = "";				
								for($i=0; $i<count($dsRi)+1; $i++){
									$tmpStr = explode(":",$dsRi[$i]);				
									if($v_pkds==$tmpStr[0]){
										$finalDs.=$v_pkds.":".$v_dssta."|";
									}else if($tmpStr[0]>0){
										$finalDs.=$dsRi[$i]."|";
									}
								}			
								
								if (substr($finalDs,0,1)=="|"){
									$finalDs = substr($finalDs,1,(strlen($finalDs)-1));			
								}
								if (substr($finalDs,strlen($finalDs)-1,strlen($finalDs))=="|"){
									$finalDs = substr($finalDs,0,strlen($finalDs)-1);			
								} 
								$finalDs = str_replace("||","|",$finalDs);
//								echo "UPDATE ET_GROUPS set DS_RIGHTS ='".$finalDs."'where PK_GROUPS=".$noData["PK_GROUPS"]."<br/>";
								$rs = $db->executeQuery("UPDATE ET_GROUPS set DS_RIGHTS ='".$finalDs."' where PK_GROUPS=".$noData["PK_GROUPS"]);							
							}
						}	
						echo "Data Saved.";
						echo '<meta http-equiv="refresh" content="0; url=./datasource_dash.php">';
					}else{
						echo "<meta http-equiv='refresh' content='0; url=./datasource_edit.php?mode=m&pk_ds=".$v_pkds."'>";
						//header("location:datasource_edit.php?mode=m&mm=1&pk_ds=".$v_pkds);
					}
				}else{
					header("location:datasource_edit.php?mode=m&pass=1&pk_ds=".$v_pkds);
				}
			}
		break;
		
		case "c":
			if ($v_ch_pe=="" || $v_ch_pn=="" || $v_ch_pr==""){
				header("location:datasource_edit.php?mode=c&blank=1&pk_ds=".$v_pkds);				
			}else{
				if($v_ch_pn!=$v_ch_pr){
					header("location:datasource_edit.php?mode=c&pass=1&pk_ds=".$v_pkds);
				}else{
					$passQ = $db->executeQuery("SELECT DS_PASS from ET_DS where PK_DS='$v_pkds'");
					$passQ->next();
					$passrs = $passQ->getCurrentValuesAsHash();				
					if($passrs["DS_PASS"]!=$v_ch_pe){
						header("location:datasource_edit.php?mode=c&mm=1&pk_ds=".$v_pkds);
					}else{
						$newpass = $db->executeQuery("UPDATE ET_DS set DS_PASS ='".$v_ch_pn."' where PK_DS=".$v_pkds);
						echo "Data Saved.";
						echo '<meta http-equiv="refresh" content="0; url=./datasource_dash.php">';					
					}
				}
			}	
		break;
	}	
}
?>
</div>

</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
