<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Import ODBC DB</title>

<?php
include("./includes/oci_functions.php");
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Import ODBC DB</div>
<br>
	
<?PHP 
if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
?>
<form name="newodbc" action="query_odbc_new.php" method="post">
	<table>
	<tr><td>Name</td><td><INPUT name="oname" type="text" size="20"/></td></tr>
	<tr><td>ODBC DSN</td><td><INPUT name="odsn" type="text" size="20"/></td></tr>
	<tr><td>User</td><td><INPUT name="ouser" type="text" size="20"/></td></tr>
	<tr><td>Password</td><td><INPUT name="opass" type="text" size="20"/></td></tr>
	<tr><td><INPUT name="submit" type="submit" value="Submit"/></d></tr>
	</table>
</form>
<?PHP
} else {
	$v_name = $_POST["oname"];
	$v_dsn = $_POST["odsn"];
	$v_user = $_POST["ouser"];
	$v_pass = $_POST["opass"];

	$query = "insert into velink.vlnk_odbc (pk_vlnk_odbc,odbc_name,odbc_dsn,odbc_user,odbc_pass) values (velink.seq_vlnk_odbc.nextval,'$v_name','$v_dsn','$v_user','$v_pass')";
	$results = executeOCIUpdateQuery($query,$ds_conn);

	OCICommit($ds_conn);
	OCILogoff($ds_conn);

	echo "Data Saved Successfully !!!";
	echo '<meta http-equiv="refresh" content="0; url=./query_odbc.php"> ';
	
}
?>
      </div>


</body>
</html>

<?php
}
else header("location: ./index.php?fail=1");
?>
