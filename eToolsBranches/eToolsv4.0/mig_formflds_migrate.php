<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Migrate Form Data - Migrate</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 



?>
</head>


<script>
function startImport(adaptor,adapmod) {
	document.import_2.pk_vlnk_adaptor.value = adaptor;
	document.import_2.pk_vlnk_adapmod.value = adapmod;
//	document.import_2.target="_new"
	document.import_2.submit();
}
</script>


<body>



<div id="fedora-content">	
	
<div class="navigate">Migrate Data - Forms - Migrate</div>
	
<?php
//print "<pre>"; print_r($_REQUEST); print "</pre>"; 
if (isset($_GET["pk_vlnk_adaptor"])) $pk_vlnk_adaptor = $_GET["pk_vlnk_adaptor"];
if (isset($_POST["pk_vlnk_adaptor"])) $pk_vlnk_adaptor = $_POST["pk_vlnk_adaptor"];

if (isset($_GET["pk_vlnk_adapmod"])) $pk_vlnk_adapmod = $_GET["pk_vlnk_adapmod"];
if (isset($_POST["pk_vlnk_adapmod"])) $pk_vlnk_adapmod = $_POST["pk_vlnk_adapmod"];

if (isset($_GET["form_linkto"])) $v_form_linkto = $_GET["form_linkto"];
if (isset($_POST["form_linkto"])) $v_form_linkto = $_POST["form_linkto"];

if (isset($_GET["rows"])) {
	$v_rows = $_GET["rows"];
} else {
	$v_rows = -1;
}
$v_mode = (isset($_GET["mode"])?$_GET["mode"]:$_POST["mode"]);

if ($v_mode == "import") {
	$query_sql = "update velink.vlnk_adapmod set record_type = '1' where pk_vlnk_adapmod = $pk_vlnk_adapmod";
	$results = executeOCIUpdateQuery($query_sql,$ds_conn);
?>
<form name="import_2" action="mig_formflds_migrate_step2.php" method="post">
<input type="hidden" name="pk_vlnk_adaptor" />
<input type="hidden" name="pk_vlnk_adapmod" />
</form>
	<script>
		startImport(<?PHP echo $pk_vlnk_adaptor; ?>,<?PHP echo $pk_vlnk_adapmod; ?>);
	</script>

<?PHP	
		$url = "mig_forms.php?pk_vlnk_adaptor=".$pk_vlnk_adaptor."&refresh=0";
		echo "<meta http-equiv=\"refresh\" content=\"1; url=./".$url."\">";}


if ($v_mode == "empty"){

//if ($v_rows == 0 || isset($_POST["pk_vlnk_adapmod"])) {
	if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
?>
	<form name="import" method="post" action="mig_formflds_migrate.php">
		<input type="hidden" name="pk_vlnk_adapmod" value="<?PHP echo $pk_vlnk_adapmod;?>" />
		<input type="hidden" name="pk_vlnk_adaptor" value="<?PHP echo $pk_vlnk_adaptor;?>" />
		<input type="hidden" name="form_linkto" value="<?PHP echo $v_form_linkto;?>" />
		<input type="hidden" name="mode" value="<?PHP echo $v_mode;?>" />
		<br>
		<table width="100%">
		<tr><td><input type="checkbox" name="delete_tempdata" id="delete_tempdata" /> <label for="delete_tempdata"> Delete data from intermediary table.</label></td>
		<tr><td><BR><input type="checkbox" name="delete_respdata" id="delete_respdata" /> <label for="delete_respdata"> Delete response mapping.</label></td> 
		</table>
		<BR><input type="image" name="submit" value="SUBMIT" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />
	</form>
<?PHP
} else {
	if (isset($_POST["delete_tempdata"])){
		switch ($v_form_linkto) {
		case "A":
			$v_table = "ER_IMPACCTFORM";
			break;
		case "P":
			$v_table = "ER_IMPPATFORM";
			break;
		case "S":
			$v_table = "ER_IMPSTUDYFORM";
			break;
		}
		$query_sql = "delete from " .$v_table. " where custom_col = '".$pk_vlnk_adapmod."'" ;
		$results = executeOCIUpdateQuery($query_sql,$ds_conn);
		
		$query_sql = "delete from velink.vlnk_log where fk_vlnk_adapmod = $pk_vlnk_adapmod" ;
		$results = executeOCIUpdateQuery($query_sql,$ds_conn);

	}
	if (isset($_POST["delete_respdata"])){
		$query_sql = "delete from velink.vlnk_imp_formresp where exists (select * from velink.vlnk_imp_formfld where pk_imp_formfld = fk_imp_formfld and  fk_vlnk_adapmod = ".$pk_vlnk_adapmod.")" ;
		$results = executeOCIUpdateQuery($query_sql,$ds_conn);
		
		$updateQuery = "Update velink.vlnk_imp_formfld set FK_MAP_RESP=0";
		$results = executeOCIUpdateQuery($updateQuery,$ds_conn);
	}
	echo "Form data deleted from intermediary table...";
	$url = "mig_forms.php?pk_vlnk_adaptor=".$pk_vlnk_adaptor."&refresh=0";
	echo "<meta http-equiv=\"refresh\" content=\"1; url=./".$url."\">";
}
}
?>

      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
