<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Validate Data</title>

<?php
include("./includes/oci_functions.php");	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Migrate Data - Standard - Validate</div>
<?php
$pk_vlnk_adapmod = $_GET["pk_vlnk_adapmod"];
$v_pk_vlnk_adaptor = $_GET["pk_vlnk_adaptor"];
//$v_pk_vlnk_adaptor = $_POST["pk_vlnk_adaptor"];

echo '<table width="100%"><tr><td align="right"><a href="./mig_modules.php?pk_vlnk_adaptor='.$v_pk_vlnk_adaptor.'">Back to Migration Page</a></td></tr></table>';

// Mandatory field validation
$query_sql = "select pipe_colname,  column_name from velink.vlnk_adapmodcol,velink.vlnk_modcol where pk_vlnk_modcol = fk_vlnk_modcol and is_required = 1 and map_col_name is null and fk_vlnk_adapmod = ".$pk_vlnk_adapmod;
$results = executeOCIQuery($query_sql,$ds_conn);
$htmlstr = "";
for ($rec = 0; $rec < $results_nrows; $rec++){
	$htmlstr.=$results["COLUMN_NAME"][$rec].", ";
}
if (strlen($htmlstr) > 0) {
	echo "Mapping missing for mandatory column(s): <font color=red><b>".substr($htmlstr,0,strlen($htmlstr)-2)."</b></font><BR>";
} else {
	echo "Mandatory field mapping: <font color=blue>PASS</font><BR>";
}

// Mandatory field null check
$query_sql = "select pipe_colname,  column_name from velink.vlnk_adapmodcol,velink.vlnk_modcol where pk_vlnk_modcol = fk_vlnk_modcol and is_required = 1 and map_col_name is not null and fk_vlnk_adapmod = ".$pk_vlnk_adapmod;
$results = executeOCIQuery($query_sql,$ds_conn);
$i = 0;
$v_pipe_colstr = '';
$v_pipe_werstr = '';
for ($rec = 0; $rec < $results_nrows; $rec++){
	$v_pipe_colname[$i] = $results["PIPE_COLNAME"][$rec];
	$v_column_name[$i]  = $results["COLUMN_NAME"][$rec];
	$v_pipe_colstr .= $results["PIPE_COLNAME"][$rec].",";
	$v_pipe_werstr .= $results["PIPE_COLNAME"][$rec].' is null or ';
	$i++;
}
$htmlstr="";
if (strlen($v_pipe_colstr) > 0) {
	$v_pipe_colstr = substr($v_pipe_colstr,0,strlen($v_pipe_colstr)-1);
	$v_pipe_werstr = substr($v_pipe_werstr,0,strlen($v_pipe_werstr)-4);
	$query_sql = "select pk_vlnk_pipe,".$v_pipe_colstr." from velink.vlnk_pipe where fk_vlnk_adapmod = ".$pk_vlnk_adapmod." and (".$v_pipe_werstr.")";
	$results = executeOCIQuery($query_sql,$ds_conn);
	for ($rec = 0; $rec < $results_nrows; $rec++){
		for($i=0;$i<count($v_pipe_colname);$i++){
			if (strlen($results[$v_pipe_colname[$i]][$rec])==0){
				$htmlstr .= '<A HREF="mig_validate_edit.php?pk_vlnk_adaptor='.$v_pk_vlnk_adaptor.'&pk_vlnk_pipe='.$results["PK_VLNK_PIPE"][$rec].'&colname='.$v_pipe_colname[$i].'&pk_vlnk_adapmod='.$pk_vlnk_adapmod.'">Edit</A> ';
				$htmlstr .= $v_column_name[$i]." is blank<BR>";
			}
		}
	}
}

if (strlen($htmlstr) > 0) {
	echo "<BR>Following mandatory fields are blank:<BR>".$htmlstr;
} else {
	echo "<BR>Mandatory fields cannot be blank: <font color=blue>PASS</font><BR>";
}

// Response mapping validation
$query_sql = "select column_name,source_value from velink.vlnk_pipemap,velink.vlnk_modcol where pk_vlnk_modcol = fk_vlnk_modcol and target_value is null and fk_vlnk_adapmod = ".$pk_vlnk_adapmod;
$results = executeOCIQuery($query_sql,$ds_conn);
$htmlstr = "";
for ($rec = 0; $rec < $results_nrows; $rec++){
	$htmlstr.="<tr><td>".$results["COLUMN_NAME"][$rec]."</td> <td><font color=red>".$results["SOURCE_VALUE"][$rec]."</font></td></tr>";
}

if (strlen($htmlstr) > 0 ) {
	echo "<BR>Reponse mapping missing for:<a href='mig_datamapping.php?pk_vlnk_adapmod=".$pk_vlnk_adapmod."&refresh=0'> Edit Response Mapping</a><table>".$htmlstr."</table>";
//} elseif ($results_nrows == 0) {
//	echo "<BR><font color='red'>Responses not mapped.</font><BR>";
} else {	
	echo "<BR>Response mapping validation: <font color=blue>PASS</font><BR>";
}

// Date validation
$query_sql = "SELECT pipe_colname,map_col_name,column_name FROM velink.vlnk_adapmodcol, velink.vlnk_modcol WHERE pk_vlnk_modcol = fk_vlnk_modcol AND map_col_name IS NOT NULL AND col_type = 'DATE' and fk_vlnk_adapmod = ".$pk_vlnk_adapmod;
$results = executeOCIQuery($query_sql,$ds_conn);
$trows = $results_nrows;
echo "<BR>Date Validation:<i> (valid date format - MM/DD/YYYY)</i><BR>";
echo "<table border=1 width=100%>";
echo "<tr><th>Error Type</th><th>Column Name</th><th>Value</th><th>&nbsp;</th></tr>";
for ($rec = 0; $rec < $trows; $rec++){
	$colname = $results["PIPE_COLNAME"][$rec];
	$query_sql = "SELECT pk_vlnk_pipe,".$colname." as colname FROM velink.vlnk_pipe WHERE ".$colname." IS NOT NULL and fk_vlnk_adapmod = ".$pk_vlnk_adapmod;
	$results1 = executeOCIQuery($query_sql,$ds_conn);
	for ($rec1 = 0; $rec1 < $results_nrows; $rec1++){

		$date = $results1["COLNAME"][$rec1];
		$dateParse = explode("/",$date);
		if (count($dateParse) == 3) {
			list($month, $day, $year) = split('/', $date);
			if (!checkdate($month,$day,$year) || strlen($year) < 4) {
				echo "<TR>";
				echo "<TD>Invalid date</td><td>".$results["COLUMN_NAME"][$rec]."</td><td>".$date."</td>";
				echo '<td><A HREF="mig_validate_edit.php?pk_vlnk_adaptor='.$v_pk_vlnk_adaptor.'&pk_vlnk_pipe='.$results1["PK_VLNK_PIPE"][$rec1].'&colname='.$colname.'&pk_vlnk_adapmod='.$pk_vlnk_adapmod.'&pk_vlnk_adaptor='.$v_pk_vlnk_adaptor.'">Edit</A></td>' ; 
				echo "</TR>";
			}
		} else {
				echo "<TR>";
				echo "<TD>Invalid date</td><td>".$results["COLUMN_NAME"][$rec]."</td><td>".$date."</td>";
				echo '<td><A HREF="mig_validate_edit.php?pk_vlnk_adaptor='.$v_pk_vlnk_adaptor.'&pk_vlnk_pipe='.$results1["PK_VLNK_PIPE"][$rec1].'&colname='.$colname.'&pk_vlnk_adapmod='.$pk_vlnk_adapmod.'&pk_vlnk_adaptor='.$v_pk_vlnk_adaptor.'">Edit</A></td>' ; 
				echo "</TR>";
		}
	}
}
echo "</table>";

//Number validation
$query_sql = "SELECT pipe_colname,map_col_name,column_name FROM velink.vlnk_adapmodcol, velink.vlnk_modcol WHERE pk_vlnk_modcol = fk_vlnk_modcol AND map_col_name IS NOT NULL AND col_type = 'NUMBER' and map_type is null and fk_vlnk_adapmod = ".$pk_vlnk_adapmod;
$results = executeOCIQuery($query_sql,$ds_conn);
$trows = $results_nrows;
echo "<BR>Number Validation:<BR>";
echo "<table border=1 width=100%>";
echo "<tr><th>Error Type</th><th>Column Name</th><th>Value</th><th>&nbsp;</th></tr>";
for ($rec = 0; $rec < $trows; $rec++){
	$colname = $results["PIPE_COLNAME"][$rec];
	$query_sql = "SELECT pk_vlnk_pipe,".$colname." as colname FROM velink.vlnk_pipe WHERE ".$colname." IS NOT NULL and fk_vlnk_adapmod = ".$pk_vlnk_adapmod;
	$results1 = executeOCIQuery($query_sql,$ds_conn);
	for ($rec1 = 0; $rec1 < $results_nrows; $rec1++){

		$v_num = $results1["COLNAME"][$rec1];
		if (!is_numeric($v_num)) {
			echo "<TR>";
			echo "<TD>Invalid number</td><td>".$results["COLUMN_NAME"][$rec]."</td><td>".$v_num."</td>";
			echo '<td><A HREF="mig_validate_edit.php?pk_vlnk_adaptor='.$v_pk_vlnk_adaptor.'&pk_vlnk_pipe='.$results1["PK_VLNK_PIPE"][$rec1].'&colname='.$colname.'&pk_vlnk_adapmod='.$pk_vlnk_adapmod.'">Edit</A></td>' ; 
			echo "</TR>";
		}
	}
}
echo "</table>";


// Lookup validateion
$query_sql = "select fk_account from velink.vlnk_adapmod,velink.vlnk_adaptor where pk_vlnk_adaptor = fk_vlnk_adaptor and pk_vlnk_adapmod = $pk_vlnk_adapmod";
$results = executeOCIQuery($query_sql,$ds_conn);
$v_account = $results["FK_ACCOUNT"][0];

$query_sql = "SELECT pipe_colname,map_col_name,column_name,map_subtyp FROM velink.vlnk_adapmodcol, velink.vlnk_modcol WHERE pk_vlnk_modcol = fk_vlnk_modcol AND map_col_name IS NOT NULL AND map_type = 'LOOKUP' and fk_vlnk_adapmod = ".$pk_vlnk_adapmod;
$results = executeOCIQuery($query_sql,$ds_conn);
$trows = $results_nrows;
for ($rec = 0; $rec < $trows; $rec++){
	$v_missing = "";
	if ($results["MAP_SUBTYP"][$rec] == "STUDY"){
		$query_sql = "select distinct ".$results["PIPE_COLNAME"][$rec]." as snum from velink.vlnk_pipe where fk_vlnk_adapmod = $pk_vlnk_adapmod and not exists (select * from eres.er_study where upper(study_number) = upper(".$results["PIPE_COLNAME"][$rec].") and fk_account = $v_account)";
		$results1 = executeOCIQuery($query_sql,$ds_conn);
		for ($rec1 = 0; $rec1 < $results_nrows; $rec1++){
			$v_missing .= '"'.$results1["SNUM"][$rec1].'", ';
		}
		if ($results_nrows > 0) {
			echo "<BR>Missing Studies:<BR><table>";
			echo "<tr><td><TEXTAREA ROWS=5 COLS=80>".substr($v_missing,0,strlen($v_missing)-2)."</TEXTAREA></tr></td></table>";
		}
	}
	$v_missing = "";
	if ($results["MAP_SUBTYP"][$rec] == "PATIENT"){
		$query_sql = "select distinct ".$results["PIPE_COLNAME"][$rec]." as pid from velink.vlnk_pipe where fk_vlnk_adapmod = $pk_vlnk_adapmod and not exists (select * from eres.er_per where upper(per_code) = upper(".$results["PIPE_COLNAME"][$rec].") and fk_account = $v_account)";
		$results1 = executeOCIQuery($query_sql,$ds_conn);
		for ($rec1 = 0; $rec1 < $results_nrows; $rec1++){
			$v_missing .= '"'.$results1["PID"][$rec1].'", ';
		}
		if ($results_nrows > 0) {
			echo "<BR>Missing Patients:<BR><table>";
			echo "<tr><td><TEXTAREA ROWS=5 COLS=80>".substr($v_missing,0,strlen($v_missing)-2)."</TEXTAREA></tr></td></table>";
		}
	}

}




?>

</div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
