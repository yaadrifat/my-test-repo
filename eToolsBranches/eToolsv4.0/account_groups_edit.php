<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Groups</title>

	
<script>
function validate(form){
	if (form.gname.value == ""){
		alert("Group name cannot be blank.");
		return false;
	}
}
</script>
<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");
include ("./txt-db-api.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 



?>

</head>


<body>

<div id="fedora-content">	

<?PHP

$db = new Database("etools");



if (!($_SERVER['REQUEST_METHOD'] == 'POST')) {
$v_mode = $_GET["mode"];
$v_pk_groups = $_GET["pk_groups"];
if ($_GET["mode"] == 'n'){
	echo '<div class="navigate">Manage Account - Groups - New</div>';
	$v_mode = 'N';
	$v_appRights = "";
	$v_dsRights = "";
	$v_gname = "";
	$v_gdesc = "";
} else {
	echo '<div class="navigate">Manage Account - Groups - Edit</div>';
	$v_mode = 'M';
	$rs = $db->executeQuery("select GROUP_NAME,GROUP_DESC,APP_RIGHTS,DS_RIGHTS from ET_GROUPS where PK_GROUPS = ".$v_pk_groups);
	$rs->next();
	$data = $rs->getCurrentValuesAsHash();
	$v_appRights =  $data["APP_RIGHTS"];
	$v_dsRights =  $data["DS_RIGHTS"];
	$v_gname =  $data["GROUP_NAME"];
	$v_gdesc =  $data["GROUP_DESC"];
}
?>
	<form name="editgroup" action="account_groups_edit.php" method="POST" onSubmit="if (validate(document.editgroup) == false) return false;">
		<INPUT TYPE="hidden" NAME="pk_groups" value="<?PHP echo $v_pk_groups; ?>"/>
		<INPUT TYPE="hidden" NAME="mode" value="<?PHP echo $v_mode; ?>"/>
		<table border="1">
		<tr><td>Name: </td><td><input class="required" type="text" name="gname" size="30" value="<?PHP echo $v_gname; ?>"/></td></tr>
		<tr><td>Description: </td><td><input type="text" name="gdesc" size="50" value="<?PHP echo $v_gdesc; ?>"/></td></tr>
		<tr><td>Modules: </td><td>
<?PHP
$rs = $db->executeQuery("select pk_menu,menu_name,level,default_access,module from ET_MENUS order by disp_seq");
$v_counter = 0;
while ($rs->next()) {
	$data = $rs->getCurrentValuesAsHash();
	if ($data["default_access"] == 1) {
		echo '<INPUT TYPE="hidden" name="modules['.$v_counter.']" value="'.$data["pk_menu"].'"/>';
	} else {
		if ($data["level"] == 1) {
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

			$v_pos = strpos("PAD".$v_appRights,"|".$data["pk_menu"].":1");
			if (empty($v_pos)) $v_pos = -1;
			if ($v_pos >= 0){
				echo '<INPUT TYPE="CHECKBOX" checked name="modules['.$v_counter.']" value="'.$data["pk_menu"].'">'.$data["menu_name"];
			} else {
				echo '<INPUT TYPE="CHECKBOX" name="modules['.$v_counter.']" value="'.$data["pk_menu"].'">'.$data["menu_name"];
			}

		} else {
			echo "<B>".$data["menu_name"]."</B>";
			echo '<INPUT type="hidden" name="modules['.$v_counter.']" value="'.$data["pk_menu"].'LEVEL">';
		}
		echo "<BR>";
	}
	echo '<INPUT type="hidden" name="counter['.$v_counter.']" value="'.$data["pk_menu"].'">';
	$v_counter ++;
}
?>
</td></tr>
		<tr><td>Datasources: </td><td>
<?PHP
$rs = $db->executeQuery("select PK_DS,DS_NAME from ET_DS order by DS_NAME");
$v_counter = 0;
while ($rs->next()) {
	$data = $rs->getCurrentValuesAsHash();

	$v_pos = strpos("PAD".$v_dsRights,"|".$data["PK_DS"].":1");
	if (empty($v_pos)) $v_pos = strpos("PAD".$v_dsRights,$data["PK_DS"].":1");
	if (empty($v_pos)) $v_pos = -1;
	if ($v_pos >= 0){
		echo '<INPUT TYPE="CHECKBOX" checked name="ds['.$v_counter.']" value="'.$data["PK_DS"].'">'.$data["DS_NAME"];
	} else {
		echo '<INPUT TYPE="CHECKBOX" name="ds['.$v_counter.']" value="'.$data["PK_DS"].'">'.$data["DS_NAME"];
	}
	echo '<INPUT type="hidden" name="ds_counter['.$v_counter.']" value="'.$data["PK_DS"].'">';
	echo "<BR>";
	$v_counter ++;
}
?>
</td></tr>
		</table>
			<BR>
		<input type="image" src="./img/submit.png" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />

		</form>
	
<?PHP
} else {
$v_pk_groups = $_POST["pk_groups"];
$v_mode = $_POST["mode"];
$v_gname = $_POST["gname"];
$v_gdesc = $_POST["gdesc"];
$v_counter = $_POST["counter"];
$v_modules = $_POST["modules"];
(isset($_POST["ds"])) ? $v_ds = $_POST["ds"] : $v_ds = "";
$v_ds_counter = $_POST["ds_counter"];
$v_gstr = "";
$v_dstr = "";


for ($i=0; $i < count($v_counter); $i++) {
	$v_gstr .= '|';
	if (isset($v_modules[$i])){
		$v_gstr .= $v_counter[$i].':1';
	} else {
		$v_gstr .= $v_counter[$i].':0';
	}
}


for ($i=0; $i < count($v_ds_counter); $i++) {
	if ($i > 0) {
		$v_dstr .= '|';
	}
	if (isset($v_ds[$i])){
		$v_dstr .= $v_ds_counter[$i].':1';
	} else {
		$v_dstr .= $v_ds_counter[$i].':0';
	}
}

	$db = new Database("etools");
	if ($v_mode == 'N') {
		$result = $db->executeQuery("SELECT GROUP_NAME from ET_GROUPS where GROUP_NAME='$v_gname'");
		if ($result->getRowCount() > 0) {
			echo "Group already exists";
			echo '<meta http-equiv="refresh" content="3; url=./account_groups.php">';
		} else {
			$rs = $db->executeQuery("INSERT INTO ET_GROUPS (GROUP_NAME,GROUP_DESC,APP_RIGHTS,DS_RIGHTS) VALUES ('".$v_gname."','".$v_gdesc."','".$v_gstr."','".$v_dstr."')");
			echo "Data Saved.";
			echo '<meta http-equiv="refresh" content="0; url=./account_groups.php">';
		}
	} else {
		$rs = $db->executeQuery("UPDATE ET_GROUPS set GROUP_NAME = '$v_gname', GROUP_DESC = '$v_gdesc', APP_RIGHTS = '$v_gstr', DS_RIGHTS = '$v_dstr' where PK_GROUPS = ".$v_pk_groups);
		echo "Data Saved.";
		echo '<meta http-equiv="refresh" content="0; url=./account_groups.php">';
	}

}


?>
</div>

</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
