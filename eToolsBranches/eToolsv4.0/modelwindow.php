<?php	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Edit Code List</title>
<script type="text/javascript" src="js/jquery-latest.pack.js"></script>
<script>
$(document).ready(function() {	
	//select all the a tag with name equal to modal
	$('a[name=modal]').click(function(e) {
		//Cancel the link behavior
		e.preventDefault();
		
		//Get the A tag
		var id = $(this).attr('href');
	
		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		$('#mask').fadeIn(1000);	
		$('#mask').fadeTo("slow",0.8);	
	
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
              
		//Set the popup window to center
		$(id).css('top',  winH/2-$(id).height()/2);
		$(id).css('left', winW/2-$(id).width()/2);
	
		//transition effect
		$(id).fadeIn(2000); 
	
	});
	
	//if close button is clicked
	$('.window .close').click(function (e) {
		//Cancel the link behavior
		e.preventDefault();
		
		$('#mask').hide();
		$('.window').hide();
	});		
	
	//if mask is clicked
	$('#mask').click(function () {
		$(this).hide();
		$('.window').hide();
	});			
	
});
</script>
<style>
a {color:#333; text-decoration:none}
a:hover {color:#ccc; text-decoration:none}

#mask {
  position:absolute;
  left:0;
  top:0;
  z-index:9000;
  background-color:#000;
  display:none;
}
  
#boxes .window {
  position:absolute;
  left:0;
  top:0;
  width:440px;
  height:200px;
  display:none;
  z-index:9999;
  padding:20px;
}

#boxes #dialog {
  width:700px; 
  height:300px;
  padding:10px;
  background-color:#ffffff;
}

</style>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="js/validate.js"></SCRIPT>

<SCRIPT Language="javascript">
function validate(form){
	if (form.codelst_subtyp.value == "" || form.codelst_desc.value == "" || form.codelst_seq.value == "" || form.codelst_name.value == "") {
		alert("Sub Type / Description / Sequence cannot be blank.");
		return false;
	}
	if (!isInteger(form.codelst_seq.value)){
		alert("Sequence allows only numbers");
		return false;
	}
}

function setRights(formobj){
if (formobj.codelst_custom_col) {
	for (var i=0; i < formobj.codelst_custom_col.length; i++)
	   {
	   if (formobj.codelst_custom_col[i].checked && (formobj.codelst_custom_col[i].value == "dropdown" || formobj.codelst_custom_col[i].value == "splfld") && formobj.codelst_custom_col1.value == "")
		  {
		  alert("'HTML for Dropdown/Special Field' field can not be blank.");
		  formobj.codelst_custom_col1.focus();
		  return false;
		  }
	   }
}


	totrows = formobj.totalrows.value;
	var resultStr = "";
	for (i=0;i<totrows;i++){
		resultStr += formobj.rights[i].value;
	}
	formobj.rightstr.value = resultStr;	
}

	
	function changeRights(obj,row,formobj)

	{
	  selrow = row ;
	  totrows = formobj.totalrows.value;
	  if (totrows > 1) 		
	  	rights =formobj.rights[selrow].value;
	  else 		
	  	rights =formobj.rights.value;

	
	  objName = obj.name;

	  if (obj.checked)
   	  {
         
       	if (objName == "newr") 

	{
		rights = parseInt(rights) + 1;

		if (!formobj.view[selrow].checked)

		{ 
			formobj.view[selrow].checked = true;

			rights = parseInt(rights) + 4;
		}

	}


     	if (objName == "edit") 
	{

		rights = parseInt(rights) + 2;
		if (!formobj.view[selrow].checked)

		{ 
			formobj.view[selrow].checked = true;

			rights = parseInt(rights) + 4;

		}

	}



     	if (objName == "view") rights = parseInt(rights) + 4;

	if (totrows > 1 )

		formobj.rights[selrow].value = rights;

	else

		formobj.rights.value = rights;

  }else

	{

       	if (objName == "newr") rights = parseInt(rights) - 1;

       	if (objName == "edit") rights = parseInt(rights) - 2;

       	if (objName == "view") 
		{	
			if (formobj.newr[selrow].checked) 
			{
				alert("The user has right to New or Edit. You can not revoke right to View");
				formobj.view[selrow].checked = true;		
			}
	
		    else if (formobj.edit[selrow].checked) 
			{
				alert("The user has right to New or Edit. You can not revoke right to View");
				formobj.view[selrow].checked = true;		
			}
			else
			{		
			  rights = parseInt(rights) - 4;
			}
		}
		

	if (totrows > 1 ) {

		formobj.rights[selrow].value = rights;
		
    } 
	else {

		formobj.rights.value = rights; 
		

  	 }
	
  }
  
}

function fnEmpty(formobj){
	
		if (isEmpty(formobj.role.value))
		{	alert("Please enter mandatory fields");	
			formobj.role.focus();
			return false;	 
		} 
		if (isEmpty(formobj.rolesubtype.value)) {	 
			alert("Please enter mandatory fields");	
			formobj.rolesubtype.focus();
			return false;	 
		} 
		return true;
	} 


function setReadOnly(obj)
{
	if(obj.value == "chkbox" || obj.value == "")
	{
		document.forms[0].codelst_custom_col1.disabled = true;
		document.forms[0].codelst_custom_col1.value = "";
	} else {
		document.forms[0].codelst_custom_col1.disabled = false;
	}
} 
	
</SCRIPT>


</head>
<body >
<!-- <div class="browserDefault" id="browserDefault"> -->
	<div id="boxes">
		<div id="dialog" class="window">
		<table width="1000" border="0">
					<tr>
						<td width="127">Type</td>
						<TD width="863"><input disabled type = "text" name="codelst_type" value="<?php echo $results["CODELST_T"][$rec]; ?>"></TD>
						</tr><tr>
						<td>Sub Type</td><TD style="font-size:11px; font-style:italic; color:#999999;"><input name="codelst_subtyp" type = "text" class="required" value="<?php echo $results["CODELST_SUBTYP"][$rec]; ?>" maxlength="15" <?PHP echo (($v_exists == 0)? "":"readonly"); ?>> (Maximum Length: 15 Characters)</TD>
						</tr>
						<tr><td>Name</td><td><input name="codelst_name" type = "text" class="required" value="<?php echo $results["NAME"][$rec]; ?>" maxlength="200">
							  <span style="font-size:11px; font-style:italic; color:#999999;"> (Maximum Length: 200 Characters)</span></td>
						</tr>
						<tr>
							<td >Description</td><TD><input name="codelst_desc" type = "text" class="required" value="<?php echo htmlspecialchars($results["DESCRIPTION"][$rec]); ?>" size="100" maxlength="200">
							  <span style="font-size:11px; font-style:italic; color:#999999;"> (Maximum Length: 200 Characters)</span></TD>	
						</tr>			
		</table>
		</div>
		<a href="#"class="close"/>Close it</a> 
		<div id="mask"></div>
	</div>	

</body>
</html>


<?php
}
else header("location: index.php?fail=1");
?>
		

