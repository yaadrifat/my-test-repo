<?php
    
    //--------Author: Nicholas Louis ------------//

    session_start();
    header("Cache-control: private");
    if(@$_SESSION["user"]){
?>
<html>
<head>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
    <title>Velos eTools -> Query Scheduler</title>
<script>
    
</script>
</head>
 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script src="js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <script src="js/jquery-ui-sliderAccess.js" type="text/javascript"></script>
<!--    <script src="js/fastsearch.js" type="text/javascript"></script>-->

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link rel="stylesheet" href="css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="js/jquery-ui-timepicker-addon.css" type="text/css" />
  <style>
  .ui-autocomplete-loading {
    background: white url("images/ui-anim_basic_16x16.gif") right center no-repeat;
  }
  </style>    
<script>
    $( function() {
        function split( val ) {
          return val.split( /,\s*/ );
        }
        function extractLast( term ) {            
          return split( term ).pop();
        }
        $('.selrechide').val('');
        $( "#selr input" )
          .on( "keydown", function( event ) {
            selid = $(this).attr('id');
            if(selid != 'selreccol'){
                if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).autocomplete( "instance" ).menu.active ) {
                  event.preventDefault();
                }
            }else{
                $('.recoption').val('selreccol');
            }
          })
          .autocomplete({
            source: function( request, response ) {
              $.getJSON( "scheduler_ajax.php", {
                term: extractLast( request.term ),
                termkey: selid
              }, response );
            },
            search: function() {              
              var term = extractLast( this.value );              
              if ( term.length < 2 ) {
                return false;
              }
            },
            focus: function() {              
              return false;
            },
            select: function( event, ui ) {
              var terms = split( this.value );
              var ter = ui.item.value.split('-');
              terms.pop();
              terms.push(ter[0]);
              
              if(selid == 'selrecstudy'){
                var hidfldstpk = $('.selrechidestudy').val();                
                if(hidfldstpk.substring(0,1)==','){
                    hidfldstpk = hidfldstpk.substring(1,hidfldstpk.length);
                }
                if(hidfldstpk == ''){
                    hidfldstpk = ui.item.pkusr;
                }else{
                    hidfldstpk = hidfldstpk+','+ui.item.pkusr;
                }
                
              }else if(selid == 'selrecrole'){
                var hidfldrolpk = $('.selrechiderole').val();
                if(hidfldrolpk.substring(0,1)==','){
                    hidfldrolpk = hidfldrolpk.substring(1,hidfldrolpk.length);
                }
                if(hidfldrolpk == ''){
                    hidfldrolpk = ui.item.pkusr;
                }else{
                    hidfldrolpk = hidfldrolpk+','+ui.item.pkusr;
                }
              }else{
                var hidfldpk = $('.selrechide').val();
                if(hidfldpk.substring(0,1)==','){
                    hidfldpk = hidfldpk.substring(1,hidfldpk.length);
                }
                if(hidfldpk == ''){
                    hidfldpk = ui.item.pkusr;
                }else{
                    hidfldpk = hidfldpk+','+ui.item.pkusr;
                }
                

              }
                terms.push( "" );
                this.value = terms.join( ", " );              
              if(selid == 'selrecstudy'){                  
                  $('.selrechidestudy').val(hidfldstpk);
                  $('.recoption').val(selid);
              }else if(selid == 'selrecrole'){
                  $('.selrechiderole').val(hidfldrolpk);
                  $('.recoption').val(selid);                  
              }else{
                    $('.selrechide').val(hidfldpk);
                    $('.recoption').val(selid);
              }
              return false;
            }
          });
      } );  
</script>
  
<script type="text/javascript">    
    $(document).ready(function(){
        //$(".stdate").datepicker({ minDate: '0', changeMonth: true, changeYear: true, dateFormat: 'dd-mm-yy'});
        $(".stdate").datepicker({ minDate: '0', changeMonth: true, changeYear: true, dateFormat: 'mm/dd/yy'});
        
        var dinfo = '<p>Scheduler name should be a single word. </p><p>Select the relevant eMail template for the selected query.</p>';
        var winfo = '<p>Scheduler name should be a single word. </p><p>Days are option. All the days will be selected if the selection is none.</p><p>Select the relevant eMail template for the selected query.</p>';
        var minfo = '<p>Scheduler name should be a single word. </p><p>Months are option. All the months will be selected if the selection is none.</p><p>All day of the month will be calculated if day of the month is not selected atleast one.</p><p>Select the relevant eMail template for the selected query.</p>';
        var yinfo = 'yearly';
        var hinfo = '<p>Scheduler name should be a single word. </p><p>Every: is to be entered between 1 to 23</p><p>Select the relevant eMail template for the selected query.</p>';
        
        $('.stdtime').timepicker();
        $('#freqcomp1').hide();
        $('.note').show();
        $('.notediv').html(dinfo);
        $('#freqcomp2').hide();
        $('#freqcomp3').hide();
        $('#freqcomp4').hide();
        $('#freqcomp5').hide();
        $('#frmtdiv1').hide(); //CSV div
        $('#frmtdiv2').show(); //custom template div
        
        
        
        $('#selrecoptdiv1').hide(); //by column div
        $('#selrecoptdiv2').hide(); //by group div
        $('#selrecoptdiv3').hide(); //by study&role div
        $('#selrecoptdiv4').hide(); //by user div
        $('#selrecdiv').show();
        $('.selformat input:radio[name=msgtm]').filter('[value=csv]').prop('checked', true);
        $('.selformat').click(function(){
            switch($(this).val()){
                case "csv":
                    $('#frmtdiv1').show();
                    $('#frmtdiv2').hide();                    
                    $('.selrecoption input:radio').attr('disabled',true);
                    $('#selrecoptdiv1').hide(); //by column div
                    $('#selrecoptdiv4').hide(); //by user div
                break;
                    
                case "template":
                    $('#frmtdiv2').show();
                    $('#frmtdiv1').hide();
                    $('#csvname').val('');
                    $('.selrecoption input:radio').attr('disabled',false);                    
                break;
            }
        });
        $('.selrecopt input:radio[name=sch_recipient]').filter('[value=bygroup]').prop('checked', true);
        $('.selrecopt').click(function(){
            switch($(this).val()){
                case "bycolumn":
                    $('#selrecoptdiv1').show();
                    $('#selrecoptdiv2').hide();                    
                    $('#selrecoptdiv3').hide();
                    $('#selrecoptdiv4').hide();
                    $('#selrecstudy').val('');
                    $('#selrecrole').val('');
                    $('#selrec').val('');
                    $('#selrechide').val('');                    
                break;
                case "bygroup":
                    $('#selrecoptdiv1').hide();
                    $('#selrecoptdiv2').show();                    
                    $('#selrecoptdiv3').hide();
                    $('#selrecoptdiv4').hide();
                    $('#selrecstudy').val('');
                    $('#selrecrole').val('');
                    $('#selrec').val('');
                    $('#selrechide').val('');
                break;                    
                case "bystudyrole":                    
                    $('#selrecoptdiv1').hide();
                    $('#selrecoptdiv2').hide();
                    $('#selrecoptdiv3').show();
                    $('#selrecoptdiv4').hide();
                    $('#selrecgrp').val('');
                    $('#selrec').val('');
                    $('#selrechide').val('');
                break;
                case "byuser":                    
                    $('#selrecoptdiv1').hide();
                    $('#selrecoptdiv2').hide();
                    $('#selrecoptdiv3').hide();
                    $('#selrecoptdiv4').show();
                    $('#selrecgrp').val('');
                    $('#selrecstudy').val('');
                    $('#selrecrole').val('');
                    $('#selrechide').val('');
                break;
                
            }
        });


        $('.freqlist input:radio[name=freq]').filter('[value=daily]').prop('checked', true);
        $('.freqlist').click(function(){            
            switch($(this).val()){
                case "daily":
                    $('#freqcomp1').show();
                    $('#freqcomp2').hide();
                    $('#freqcomp3').hide();
                    $('#freqcomp4').hide();
                    $('#freqcomp5').hide();
                    $('.note').show();
                    $('.notediv').html(dinfo);
                    break;
                case "weekly":                    
                    $('#freqcomp1').hide();
                    $('#freqcomp2').show();
                    $('#freqcomp3').hide();
                    $('#freqcomp4').hide();
                    $('#freqcomp5').hide();
                    $('.note').show();
                    $('.notediv').html(winfo);
                    break;
                case "monthly":
                    $('#freqcomp1').hide();
                    $('#freqcomp2').hide();
                    $('#freqcomp3').show();
                    $('#freqcomp4').hide();                    
                    $('#freqcomp5').hide();
                    $('.note').show();
                    $('.notediv').html(minfo);                    
                    break;
                case "yearly":
                    $('#freqcomp1').hide();
                    $('#freqcomp2').hide();
                    $('#freqcomp3').hide();
                    $('#freqcomp4').show();
                    $('#freqcomp5').hide();
                    $('.note').show();
                    $('.notediv').html(yinfo);
                    break;
                case "hourly":
                    $('#freqcomp1').hide();
                    $('#freqcomp2').hide();
                    $('#freqcomp3').hide();
                    $('#freqcomp4').hide();
                    $('#freqcomp5').show();
                    $('.note').show();
                    $('.notediv').html(hinfo);
                    break;                    
            default:
                    $('#freqcomp1').hide();
                    $('#freqcomp2').hide();
                    $('#freqcomp3').hide();
                    $('#freqcomp4').hide();
                    $('#freqcomp5').hide();
                break;
            }
        });

  function ajname(){   
       var ajflag = 0;
        $.ajax({
            url:  "schedulername_aj.php",
            type: "get",
            data: {'schn':'t','gn':$('.schname').val()},                
            success: function(htm) {
                if(htm != 0){
                    alert('Scheduler name already exist.');
                    ajflag = 1;
                }
            }                
        }); 
          return ajflag;
   }
    $('.schfrm').submit(function(){
        var ajval;
        if($('.schname').val() == ''){
            alert('Scheduler name cannot be blank.');
            return false;
        }        
        
        if($('.schname').val() != ''){
            if($('.schname').val().indexOf(' ') > -1){
                alert('Scheduler name should be a single word.');
                return false;
            }else{                
                //var ajval = ajname();
            }
        } 
        
        if(ajval == 1){
            return false;
        }else if(ajval != 1){
        fre = $(".schfrm input[type='radio']:checked").val();
        switch(fre){
             case "daily":
                 if($('#stdate1').val()==""){
                     alert("Daily start date cannot be blank.");
                     return false;                         
                 }                          
                 if($('#stdtime1').val()==""){
                     alert("Daily start time cannot be blank..");
                     return false;                        
                 }                    
                 break;
             case "weekly":                    
                 if($('#stdate2').val()==""){                         
                         alert("Weekly start date cannot be blank.");
                         return false;
                 }
                 if($('#stdtime2').val()==""){
                     alert("Weekly start time cannot be blank..");
                     return false;                         
                 }
                 break;
             case "monthly":
                     if($('#stdate3').val()==""){                             
                             alert("Monthly start date cannot be blank.");
                             return false;
                     }
                     if($('#stdtime3').val()==""){
                         alert("Monthly start time cannot be blank..");
                         return false;
                     }
                    if($('#attime3').val()==""){
                         alert("At time cannot be blank..");
                         return false;
                     }
                 break;

             case "hourly":
                     if($('#stdate5').val()==""){                             
                             alert("Hourly start date cannot be blank.");
                             return false;
                     }
                     if($('#stdtime5').val()==""){
                         alert("Hourly start time cannot be blank..");
                        return false;                            
                     }
                     if($('#stdtime7').val()==""){
                         alert("Hourly 'Every' time interval cannot be blank..");
                        return false;                            
                     }
                     
                 break;
        }		
    }
        if($('.selsubjectline').val() == ''){
            alert('Subject line for email cannot be blank.');
            return false;                         
        }
        
        selre = $(".selrecoption input[type='radio']:checked").val();
        if($(".selformatdiv input[type='radio']:checked").val() == 'template'){        
            switch(selre){            
                case "bycolumn":
                        if($('.selreccol').val() == ''){
                            alert('Column name cannot be emtpy.');
                            return false;             
                        }
                    break;

                case "bygroup":
                        if($('.selrecgrp').val() == ''){
                            alert('Select atleast one group name.');
                            return false;             
                        }                
                    break;

                case "bystudyrole":
                        if($('.selrecstudy ').val() == ''){
                            alert('Select atleast one study.');
                            return false;             
                        }else if($('.selrecrole ').val() == ''){
                            alert('Select atleast one role.');
                            return false;
                        }                    
                    break;

                case "byuser":
                        if($('.selrec').val() == ''){
                            alert('Select atleast one recipient.');
                            return false;             
                        }
                    break;            
                }
        }else{
            if($('#csvname').val() == ''){
                alert('CSV name cannot be empty.');
                return false;             
            }
        }

    });    

});




</script>
<?php
    include("./includes/oci_functions.php");
    include("db_config.php");
    include("./includes/header.php");
    
    include_once "./adodb/adodb.inc.php";
    include_once('./adodb/adodb-pager.inc.php'); 
    include_once('./adodb/pivottable.inc.php'); 
    require_once('audit_queries.php');    
    
    
    $db = NewADOConnection("oci8");
    $db->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);
    $ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
    //$crestr = 'eres/'.$_SESSION["DS_PASS"].'@'.$_SESSION["DS_HOST"].'/'.$_SESSION["DS_SID"];
    $crestr = 'eres/'.$_SESSION["DS_PASS"].'@'.$_SESSION["DS_HOST"].':'.$_SESSION["DS_PORT"].'/'.$_SESSION["DS_SID"];    
    if (!$db) die("Connection failed");
    
?>
<body>
<?php
    if ($_SERVER['REQUEST_METHOD'] != 'POST'){         
    $pk_queryman = $_GET['pk_queryman'];
    $profiledata = "select query_name, query_desc from velink.vlnk_queryman where pk_queryman = ".$pk_queryman;
    $profiledatars = executeOCIQuery($profiledata,$ds_conn);
    
    $msgtemplateqry = "select * from esch.SCH_MSGTXT where MSGTXT_TYPE = 'et_sch'";
    $msgtemplaters = executeOCIQuery($msgtemplateqry,$ds_conn);
    
    $msgtemdropdown = '';
    for($tm =0; $tm<$results_nrows; $tm++){
        $msgtemdropdown .= "<option value=".$msgtemplaters["PK_MSGTXT"][$tm].">  ".$msgtemplaters["MSGTXT_SUBJECT"][$tm]."  </option>";
    }

//    $deltaflag1 = 0;
//    $qryexplod1 = explode(" ", $profiledatars['QUERY'][0]);
//    $qryexplodrs1 = [];
//    for($qe1=0; $qe1<sizeof($qryexplod1); $qe1++){
//        if(substr($qryexplod1[$qe1],0,1) == ':'){
//            $qryexplodrs1[$qe1] = $qryexplod1[$qe1];
//        }
//    }
//    $qryexplodrs1 = array_values($qryexplodrs1);
?>    
    <div id="fedora-content">
    <div class="navigate">Query Scheduler</div>
        <div id="schdialog">
            <form class="schfrm" name="schfrm" action="query_scheduler.php" method="post">
            <table width="100%">
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3">Query Name: <B style="color:green;"><?php echo $profiledatars['QUERY_NAME'][0] ?></style></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Query Description : <B  style="color:green;"><?php echo $profiledatars['QUERY_DESC'][0] ?></b></td></tr>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td>Scheduler Name: </td><td ><input id="schname" name="schname" class="schname required"/></td></tr>
                <tr colspan="2"><td>Run report: </td><td>
                            <input type="radio" name="freq" class="freqlist" value="daily" >Daily&nbsp;&nbsp;<input type="radio" name="freq" class="freqlist required" value="weekly">Weekly&nbsp;&nbsp;<input type="radio" name="freq" class="freqlist required" value="monthly">Monthly&nbsp;&nbsp;<input type="radio" name="freq" class="freqlist" value="hourly">Hourly                        
                    </td></tr>
                <tr><td>&nbsp; </td>
                    
                    <td>
                        <div id="freqcomp1" class="freqcomp">                    
                            <div class="parDiv"><div>Start Date: </div><div><input id="stdate1" class="stdate required" name="stdate1" type="text"/></div><div>At Time</div><div><input id="stdtime1" name="sttime1" class="stdtime required" type="text"/></div></div>
                           
                        </div>
                        <div id="freqcomp2" class="freqcomp">
                            <div class="parDiv"><div>Start Date: </div><div><input id="stdate2" class="stdate required" name="stdate2" type="text"/></div><div>Start time</div><div><input id="stdtime2" name="sttime2" class="stdtime required" /></div> </div>
                            <div class="parDiv"><p>Days: <input type="checkbox" name="fw_weekdays[]" value="SUN">Sunday&nbsp;&nbsp;<input type="checkbox" name="fw_weekdays[]" value="MON">Monday&nbsp;&nbsp;<input type="checkbox" name="fw_weekdays[]" value="TUE">Tuesday&nbsp;&nbsp;<input type="checkbox" name="fw_weekdays[]" value="WED">Wednesday&nbsp;&nbsp;<input type="checkbox" name="fw_weekdays[]" value="THU">Thursday&nbsp;&nbsp;<input type="checkbox" name="fw_weekdays[]" value="FRI">Friday&nbsp;&nbsp;<input type="checkbox" name="fw_weekdays[]" value="SAT">Saturday</p></div>
                        </div>
                        <div id="freqcomp3" class="freqcomp">
                            <div class="parDiv"><div>Start Date: </div><div><input id="stdate3" class="stdate required" type="text"  name="stdate3" /></div><div>Start time</div><div><input id="stdtime3" name="sttime3" class="stdtime required" /></div></div> 
                            <div class="parDiv">
                                <p>Months: <input type="checkbox" name="fm_months[]" value="JAN">January&nbsp;&nbsp;<input type="checkbox" name="fm_months[]" value="FEB">February&nbsp;&nbsp;<input type="checkbox" name="fm_months[]" value="MAR">March&nbsp;&nbsp;<input type="checkbox" name="fm_months[]" value="APR">April&nbsp;&nbsp;<input type="checkbox" name="fm_months[]" value="MAY">May&nbsp;&nbsp;<input type="checkbox" name="fm_months[]" value="JUN">June&nbsp;&nbsp;<input type="checkbox" name="fm_months[]" value="JUL">July&nbsp;&nbsp;<input type="checkbox" name="fm_months[]" value="AUG">August&nbsp;&nbsp;<input type="checkbox" name="fm_months[]" value="SEP">September&nbsp;&nbsp;<input type="checkbox" name="fm_months[]" value="OCT">October&nbsp;&nbsp;<input type="checkbox" name="fm_months[]" value="NOV">November&nbsp;&nbsp;<input type="checkbox" name="fm_months[]" value="DEC">December&nbsp;&nbsp;
                                </p>
                            </div>
                            <div class="parDiv">
                            <div class="parDiv"><div>Day of the month: </div><div>
                                    <select id='schdayofmonth' name="fmday_months" class="required">
                                        <option selected value=''>--Day--</option>
                                        <option value='1'>1</option>
                                        <option value='2'>2</option>
                                        <option value='3'>3</option>
                                        <option value='4'>4</option>
                                        <option value='5'>5</option>
                                        <option value='6'>6</option>
                                        <option value='7'>7</option>
                                        <option value='8'>8</option>
                                        <option value='9'>9</option>
                                        <option value='10'>10</option>
                                        <option value='11'>11</option>
                                        <option value='12'>12</option>
                                        <option value='13'>13</option>
                                        <option value='14'>14</option>
                                        <option value='15'>15</option>
                                        <option value='16'>16</option>
                                        <option value='17'>17</option>
                                        <option value='18'>18</option>
                                        <option value='19'>19</option>
                                        <option value='20'>20</option>
                                        <option value='21'>21</option>
                                        <option value='22'>22</option>
                                        <option value='23'>23</option>
                                        <option value='24'>24</option>
                                        <option value='25'>25</option>
                                        <option value='26'>26</option>
                                        <option value='27'>27</option>
                                        <option value='28'>28</option>
                                        <option value='29'>29</option>
                                        <option value='30'>30</option>
                                        <option value='31'>31</option>
                                    </select>
                                </div>
                                <div>At time</div><div><input id="attime3" name="attime3" class="stdtime required" /></div></div></div>
                        </div>
                        <div id="freqcomp4" class="freqcomp">
                            <div class="parDiv"><div>Start Date: </div><div><input id="stdate4" name="stdate4" class="stdate required" type="text" /></div></div> 
                            <div class="parDiv"><div>Date: </div><div><input id="stdate6" class="stdate required" name="stdate6" type="text"/></div><div>At time</div><div><input id="stdtime6" name="stdtime6" class="stdtime required" type="text"/></div></div>                        </div>
                        <div id="freqcomp5" class="freqcomp">
                            <div class="parDiv"><div>Start Date: </div><div><input id="stdate5" name="stdate5" class="stdate required" type="text"/></div><div>Start Time</div><div><input id="stdtime5" name="sttime5" class="stdtime required" /></div><div>Every:</div><div><input size="3" id="stdtime7" name="sttime7" class="stdtime1 required" /> { 1 - 23 }</div></div>                            
                    </td>
                </tr>

               <tr><td>Select Format: </td><td class="selformatdiv"> <input type="radio" name="msgtm" class="selformat" value="csv" >CSV&nbsp;&nbsp;&nbsp;<input type="radio" name="msgtm" class="selformat" value="template" >Custom template&nbsp;&nbsp;&nbsp;</td></tr>
                <tr><td>&nbsp;</td><td>
                    <div id="frmtdiv1">Name your CSV file: <input id="csvname" class="required" name="csvname" type="text"/> There wont be any email notification instead, you will get the delta query results in CSV file in < web server >/Scheduler/ folder.</div>
                    <div id="frmtdiv2">Select message template: <select name="msgtm" class="required"><?PHP echo $msgtemdropdown; ?></select></div>
                </td></tr>                
                <tr><td>Select recipients: </td><td class="selrecoption"><input id="selrecoption" type="radio" name="sch_recipient" class="selrecopt" value="bycolumn">By Column&nbsp;&nbsp;&nbsp;<!--<input id="selrecoption" type="radio" name="sch_recipient" class="selrecopt" value="bygroup">By Group&nbsp;&nbsp;&nbsp;<input id="selrecoption" type="radio" name="sch_recipient" class="selrecopt" value="bystudyrole">By Study & Role&nbsp;&nbsp;&nbsp;--><input id="selrecoption" type="radio" name="sch_recipient" class="selrecopt" value="byuser">By User</td><input class="recoption" type="hidden" name="recoption" value=""></tr>
                <tr id="selr"><td>&nbsp;</td><td>
                    <div id="selrecoptdiv1">Recipient Column Name: <input id="selreccol" name="sch_recipientcol" class="selreccol required"/><input type="hidden" id="selrechide" name="sch_recipienthide" class="selrechide" value=""/><input type="hidden" name="pkqry" value="<?php echo $pk_queryman ?>">&nbsp;&nbsp;&nbsp;(Column name which carry recipient pk in custom query)</div>
                    <div id="selrecoptdiv2">Select group: <input id="selrecgrp" name="sch_recipient" class="selrecgrp required"/><input type="hidden" id="selrechide" name="sch_recipienthide" class="selrechide" value=""/><input type="hidden" name="pkqry" value="<?php echo $pk_queryman ?>"></div>
                    <div id="selrecoptdiv3">Select study: <input id="selrecstudy" name="sch_recipient" class="selrecstudy required"/><input type="hidden" id="selrechide" name="sch_recipienthidestudy" class="selrechidestudy" value=""/><input type="hidden" name="pkqry" value="<?php echo $pk_queryman ?>">&nbsp;&nbsp;&nbsp;Select role: <input id="selrecrole" name="sch_recipient" class="selrecrole required"/><input type="hidden" id="selrechide" name="sch_recipienthiderole" class="selrechiderole" value=""/><input type="hidden" name="pkqry" value="<?php echo $pk_queryman ?>"></div>
                    <div id="selrecoptdiv4">Select user: <input id="selrec" name="sch_recipient" class="selrec required"/><input type="hidden" id="selrechide" name="sch_recipienthide" class="selrechide" value=""/><input type="hidden" name="pkqry" value="<?php echo $pk_queryman ?>"></div>
                    </td></tr>
                <tr><td colspan="2"><input class="schbtn" type="image" name="submit" value="Submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" /></td></tr>
                <tr><td>&nbsp;</td><td colspan="4">
                    <div class="note" ><table border="0" summary="Note: Note">
                        <tr><th align="left">Note</th></tr>
                        <tr><td align="left" valign="top" class="notediv"></td></tr>
                    </table></div>
                    </td></tr>
                
                
                
            </table>
            </form>
            <?php 
                if(substr(PHP_OS,0,3) != 'WIN'){
            ?>
                    <div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Note">
                    <tr><th align="left">Note</th></tr>
                    <tr><td align="left" valign="top">                            
                            <p>&nbsp;&nbsp;&nbsp;Instruction for LINUX</p>
                            <p>
                            <ul>
                                <li>After the submit, 3 Files will be created under etools > scheduler. f.e: If your scheduler name is 'etqry' then the files name will be 'etqryt.sh, 'etqrys.sql and 'etqryo.sh'</li>                                
                                <li>Manually run the 'etqryo.sh' file.</li>
                            </p>
                    </td></tr>
                    </table></div>  
                <?php }?>
        </div>        
    </div>
<?php
    }else{
	error_reporting(E_ALL ^ E_NOTICE);		
        $os_info =  PHP_OS;
        $schname = $_POST['schname'];
        $freqency = $_POST['freq'];
        $w_weekdays = $_POST['fw_weekdays'];
        $month = $_POST['fm_months'];
        $m_weekdays = $_POST['fm_weekdays'];    
        $yearlydatetime = $_POST['fy_exdate'];
        $rec_option = $_POST['recoption'];        
        $recipientstudy = $_POST['sch_recipienthidestudy'];
        $recipientrole = $_POST['sch_recipienthiderole'];
        $recipientcol = $_POST['sch_recipientcol']; //for the recipient from the column
        $pkquery = $_POST['pkqry'];
        $recipientcol = $_POST['sch_recipientcol'];
        $recipientcoldate = $_POST['sch_recipientcoldate'];
        $csvname = $_POST['csvname'];
        
        $pk_etsch = "select VELINK.SEQ_ET_SCHEDULER.NEXTVAL as PK_ETSCH from dual";
        $pk_etschrs = executeOCIQuery($pk_etsch,$ds_conn);
        
        
        switch ($rec_option){
            case "selreccol":
                $colcus_sql = "select query from velink.vlnk_queryman where PK_QUERYMAN = ".$pkquery;
                $colcus_rs = executeOCIQuery($colcus_sql,$ds_conn);                
                $colcusqry = $colcus_rs['QUERY'][0];
                
                
                
//                $colcusqry_rs = executeOCIQuery($colcusqry,$ds_conn);
//                $colcus = '';
//                for($col=0; $col<$results_nrows; $col++){
//                    $colcus_colname = strtoupper($recipientcol);
//                    if(strpos($colcusqry_rs[$colcus_colname][$col],',')==false){
//                        $colcus .= $colcusqry_rs[$colcus_colname][$col].',';
//                    }else{
//                        $colcus_ar = explode(',',$colcusqry_rs[$colcus_colname][$col]);                        
//                        for($colj=0; $colj<count($colcus_ar); $colj++){
//                            $colcus .= $colcus_ar[$colj].',';
//                        }
//                    }
//                }
//                if(substr($colcus, strlen($colcus)-1,  strlen($colcus)) == ','){
//                    $recipient = substr($colcus, 0,  strlen($colcus)-1);
//                }
                break;
            
            case "selrecgrp":                
                $grppk_sql = 'select * from er_user where fk_account in ('.$recipient.')';
                $grppk_rs = executeOCIQuery($grppk_sql,$ds_conn);
                $grppk = '';
                for($grp=0; $grp<$results_nrows; $grp++){
                    $grppk .= $grppk_rs['PK_USER'][$grp].',';
                }
                if(substr($grppk, strlen($grppk)-1,  strlen($grppk)) == ','){
                    $recipient = substr($grppk, 0,  strlen($grppk)-1);
                }
                break;
            
            case "selrecrole":
                $stdrole_sql = 'select * from er_studyteam where FK_CODELST_TMROLE in ('.$recipientrole.') and fk_study in ('.$recipientstudy.')';
                
                echo '$stdrole_sql: '.$stdrole_sql.'<br>';
                
                $stdrole_rs = executeOCIQuery($stdrole_sql,$ds_conn);
                $stdrole = '';
                for($std=0; $std<$results_nrows; $std++){
                    $stdrole .= $stdrole_rs['FK_USER'][$std].',';
                }
                if(substr($stdrole, strlen($stdrole)-1,  strlen($stdrole)) == ','){
                    $recipient = substr($stdrole, 0,  strlen($stdrole)-1);
                }
                break;

            
            case "selrec":
                $recipient = $_POST['sch_recipienthide'];
                break;
        }

        
        $recipient = str_replace(',',',',$recipient);   
        
        $dayofmonth = $_POST['fmday_months'];
        $we_weekdays = '';
        //$pkquery = $_POST['pkqry'];
        $msgtemplatepk = $_POST['msgtm'];
        $_SESSION['pkqry'] = $pkquery;
        
        $_SESSION['pkqry'] = '';
        
        if(empty($w_weekdays)){
            $w_weekdays = '*';
        }else{
            for($wd=0; $wd<count($w_weekdays);$wd++){
                $we_weekdays = $we_weekdays.','.$w_weekdays[$wd];
            }
            if(substr($we_weekdays,strlen($we_weekdays)-1,strlen($we_weekdays))==','){
                $we_weekdays = substr($we_weekdays,0,  strlen($we_weekdays)-1);
            }
            if(substr($we_weekdays,0,1)==','){
                $we_weekdays = substr($we_weekdays,1,  strlen($we_weekdays));
            }
        }
        
        
        $monthcol = '';
        
        if(empty($month)){
            $monthcol = '*';
        }else{    
            for($wd=0; $wd<count($month);$wd++){
                $monthcol = $monthcol.','.$month[$wd];
            }
            if(substr($monthcol,strlen($monthcol)-1,strlen($monthcol))==','){
                $monthcol = substr($monthcol,0,  strlen($monthcol)-1);
            }
            if(substr($monthcol,0,1)==','){
                $monthcol = substr($monthcol,1,  strlen($monthcol));
            }
        }    
        
        
        if(empty($dayofmonth)){
            $dayofmonth = '*';
        }
        
        if(substr($recipient,0,1)== ','){
            $recipient = substr($recipient,1,  strlen($recipient));
        }
        if(substr($recipient,strlen($recipient)-1,strlen($recipient))== ','){
            $recipient = substr($recipient,0,strlen($recipient)-1);
        }
        
             
        
        $pk_queryman = $_POST['pkqry'];
        
        $created_by = $_SESSION['user'];        

        
        if($schname == ''){
            $schname = 'pkqr_'.$pk_queryman;
        }        
        if(strlen($schname)>15){
            $schname = substr($schname,0,15);
        }
        
        if(substr($os_info,0,3)=="WIN"){            
            $extention = '.bat';            
        }else{
            $extention = '.sh';            
        } 
        
        $bat1filename = $schname.'o'.$extention;
        $bat2filename = $schname.'t'.$extention;
        $sqlfilename = $schname.'s.sql';
        
        if(file_exists("scheduler/".$bat1filename)){
            $bat1filename = 'et_'.$bat1filename;
        }
        if(file_exists("scheduler/".$bat2filename)){
            $bat2filename = 'et_'.$bat2filename;
        }
        if(file_exists("scheduler/".$sqlfilename)){
            $sqlfilename = 'et_'.$sqlfilename;
        }
        
        $montharray = ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'];
        $fredate_n = substr($_POST['stdate6'],0,2);
        if(substr($fredate_n,0,1)=='0'){
            $fredate_n = substr($fredate_n,1,2);
        }else{
            $fredate_n = substr($fredate_n,0,2);
        }
        $fredate_n = $montharray[$fredate_n];

        if ($we_weekdays == ''){
            $we_weekdays = '*';
        }
        if ($monthcol == ''){
            $monthcol = '*';
        }

        //refer this for linux command
        //http://www.computerhope.com/unix/ucrontab.htm  - linux
        //https://msdn.microsoft.com/en-us/library/windows/desktop/bb736357(v=vs.85).aspx  - windows
        
        switch($freqency){
            case "daily":
                    $startdate = $_POST['stdate1'];
                    $starttime = $_POST['sttime1'];                    
                    $freqdata = $startdate.' '.$starttime;
                    $starttime_lnx_time = explode(':',$starttime);
                    $starttime_lnx_time_m = $starttime_lnx_time[1];
                    $starttime_lnx_time_h = $starttime_lnx_time[0];
                    
                    $batchfileOneDataWin = "";
                    $batchfileOneDataLin = "";                    
                    $batchfileOneDataWin = 'schtasks /create /tn "'.$schname.'" /tr '.getcwd().'\Scheduler\\'.$bat2filename.' /sc '.$freqency.' /sd '.$startdate.' /st '.$starttime." /F /RU System";
                    //$batchfileOneDataWin = 'schtasks /create /tn "'.$schname.'" /tr '.getcwd().'\Scheduler\\'.$bat2filename.' /sc '.$freqency.' /sd '.$startdate.' /st '.$starttime." /F";
                    $batchfileOneDataLin = $starttime_lnx_m.' '.$starttime_lnx_time_h.' * * * '.getcwd().'/Scheduler/'.$bat2filename;
                    if(substr($batchfileOneDataLin,0,1)==' '){
                        $batchfileOneDataLin = substr($batchfileOneDataLin,1,strlen($batchfileOneDataLin));
                    }                    
                    
                break;
            
            case "weekly":
                    $startdate = $_POST['stdate2'];
                    $starttime = $_POST['sttime2'];

                    $starttime_lnx_time = explode(':',$starttime);
                    $starttime_lnx_time_m = $starttime_lnx_time[1];
                    $starttime_lnx_time_h = $starttime_lnx_time[0];
                    
                    $freqdata = $startdate.' '.$starttime.' '.$we_weekdays;

                    $batchfileOneDataWin = "";
                    $batchfileOneDataLin = "";
                    $batchfileOneDataWin = 'schtasks /create /tn "'.$schname.'" /tr '.getcwd().'\Scheduler\\'.$bat2filename.' /sc '.$freqency.' /d '.$we_weekdays.' /sd '.$startdate.' /st '.$starttime." /F /RU System";                    
                    //$batchfileOneDataWin = 'schtasks /create /tn "'.$schname.'" /tr '.getcwd().'\Scheduler\\'.$bat2filename.' /sc '.$freqency.' /d '.$we_weekdays.' /sd '.$startdate.' /st '.$starttime." /F";
                    $batchfileOneDataLin = $starttime_lnx_m.' '.$starttime_lnx_time_h.' * * '.$we_weekdays.' '.getcwd().'/Scheduler/'.$bat2filename;                    
                    if(substr($batchfileOneDataLin,0,1)==' '){
                        $batchfileOneDataLin = substr($batchfileOneDataLin,1,strlen($batchfileOneDataLin));
                    }                                        
                break;
            
            case "monthly":
                    $startdate = $_POST['stdate3'];
                    $starttime = $_POST['sttime3'];

                    $starttime_lnx_time = explode(':',$starttime);
                    $starttime_lnx_time_m = $starttime_lnx_time[1];
                    $starttime_lnx_time_h = $starttime_lnx_time[0];
                    
                    $freqdata = $startdate.' '.$starttime.' '.$month.' '.$w_weekdays;
                    $batchfileOneDataWin = '';                    
                    $batchfileOneDataWin = 'schtasks /create /tn "'.$schname.'" /tr '.getcwd().'\Scheduler\\'.$bat2filename.' /sc '.$freqency.' /sd '.$startdate.' /m '.$monthcol.' /d '.$dayofmonth.'  /st '.$starttime." /F /RU System";                    
                    //$batchfileOneDataWin = 'schtasks /create /tn "'.$schname.'" /tr '.getcwd().'\Scheduler\\'.$bat2filename.' /sc '.$freqency.' /sd '.$startdate.' /m '.$monthcol.' /d '.$dayofmonth.'  /st '.$starttime." /F";
                    $batchfileOneDataLin = $starttime_lnx_m.' '.$starttime_lnx_time_h.' '.$startdate.' '.$monthcol.' * '.getcwd().'/Scheduler/'.$bat2filename;
                    if(substr($batchfileOneDataLin,0,1)==' '){
                        $batchfileOneDataLin = substr($batchfileOneDataLin,1,strlen($batchfileOneDataLin));
                    }
                break;
            
            case "yearly":
                    $startdate = $_POST['stdate4'];
                    $starttime = $_POST['stdtime6'];
                    //$fredate = $_POST['stdate6'];
                    $fretime = $_POST['stdtime6'];
                    $freqency = 'monthly';
                    $freqdata = $startdate.' '.$starttime.' '.$fredate.' '.$fretime;
                    $batchfileOneDataWin = '';
                    $batchfileOneDataWin = 'schtasks /create /tn "'.$schname.'" /tr '.getcwd().'\Scheduler\\'.$bat2filename.' /sc '.$freqency.' /m '.$fredate_n.'  /sd '.$startdate.' /st '.$starttime." /F /RU System";
                    //$batchfileOneDataWin = 'schtasks /create /tn "'.$schname.'" /tr '.getcwd().'\Scheduler\\'.$bat2filename.' /sc '.$freqency.' /m '.$fredate_n.'  /sd '.$startdate.' /st '.$starttime." /F";
                    $batchfileOneDataLin = '';
                    if(substr($batchfileOneDataLin,0,1)==' '){
                        $batchfileOneDataLin = substr($batchfileOneDataLin,1,strlen($batchfileOneDataLin));
                    }                    
                break;
            
            case "hourly":
                    $startdate = $_POST['stdate5'];
                    $starttime = $_POST['sttime5']; 
                    $freqtime = $_POST['sttime7'];
                    $starttime_lnx_time_m = 0;
                    $starttime_lnx_time_h = $freqtime;
                    $freqdata = $startdate.' '.$starttime.' '.$freqtime;
                    $batchfileOneDataWin = '';
                    $batchfileOneDataWin = 'schtasks /create /tn "'.$schname.'" /tr '.getcwd().'\Scheduler\\'.$bat2filename.' /sc '.$freqency.' /sd '.$startdate.' /st '.$starttime.' /mo '.$freqtime."  /F /RU System";
                    //$batchfileOneDataWin = 'schtasks /create /tn "'.$schname.'" /tr '.getcwd().'\Scheduler\\'.$bat2filename.' /sc '.$freqency.' /sd '.$startdate.' /st '.$starttime.' /mo '.$freqtime."  /F";
                    $batchfileOneDataLin = $starttime_lnx_time_m.' '.$starttime_lnx_time_h.' * * * '.getcwd().'/Scheduler/'.$bat2filename;
                    if(substr($batchfileOneDataLin,0,1)==' '){
                        $batchfileOneDataLin = substr($batchfileOneDataLin,1,strlen($batchfileOneDataLin));
                    }                    
                break;              
        }
        
        
        if(substr($os_info,0,3)=="WIN"){
            $batone = $batchfileOneDataWin;
        }else{            
            $batone = $batchfileOneDataLin;
        }        
        
        $cusquery = "select query from velink.vlnk_queryman where PK_QUERYMAN = ".$pkquery;
        $cusqueryrs = executeOCIQuery($cusquery,$ds_conn);

        $msglongqry = "select MSGTXT_LONG from SCH_MSGTXT where PK_MSGTXT = ".$msgtemplatepk." and MSGTXT_TYPE='et_sch'";
        $msglongrs = executeOCIQuery($msglongqry,$ds_conn);
        
 	if(substr($os_info,0,3)=="WIN"){
            $batchfileTwodata = 'sqlplus -s '.$crestr.' @'.getcwd().'\Scheduler\\'.$sqlfilename.' > '.getcwd().'\Scheduler\\'.$schname.'.log';
        }else{
            $batchfileTwodata = 'export LD_LIBRARY_PATH=/usr/lib/oracle/11.2/client64/lib:$LD_LIBRARY_PATH'."\n".'export PATH=/usr/lib/oracle/11.2/client64/bin:$PATH'."\n".'sqlplus '.$crestr.' @'.getcwd().'/Scheduler/'.$sqlfilename.' > '.getcwd().'/Scheduler/'.$schname.'.log';
        }         

//        $pk_etsch = "select VELINK.SEQ_ET_SCHEDULER.NEXTVAL as PK_ETSCH from dual";
//        $pk_etschrs = executeOCIQuery($pk_etsch,$ds_conn);
        

        
        $weekd = array(1 => 'SUN', 2 => 'MON', 3 => 'TUE', 4 => 'WED', 5 => 'THU', 6 => 'FRI', 7 => 'SAT');        
        $weekdarr = explode(',',$we_weekdays);
        $weekdstr = '';
        for($wd=0; $wd<count($weekdarr); $wd++){
            $weekdkey = array_search($weekdarr[$wd], $weekd);
            $weekdstr .= $weekdkey.',';
        }
        if(substr($weekdstr,  strlen($weekdstr)-1,  strlen($weekdstr)) == ','){
            $weekdstr = substr($weekdstr,0,strlen($weekdstr)-1);
        }
        
        //$marr = array(01 =>'JAN',02 =>'FEB',03 =>'MAR',04 =>'APR',05 =>'MAY',06 =>'JUN',07 =>'JUL',08 =>'AUG',09 =>'SEP',10 =>'OCT',11 =>'NOV',12 =>'DEC');
		$marr = array(1 =>'JAN',2 =>'FEB',3 =>'MAR',4 =>'APR',5 =>'MAY',6 =>'JUN',7 =>'JUL',8 =>'AUG',9 =>'SEP',10 =>'OCT',11 =>'NOV',12 =>'DEC');
		
        $monarr = explode(',',$monthcol);
        $monstr = '';
        for($m=0; $m<count($monarr); $m++){
            $mkey = array_search($monarr[$m], $marr);
/*          if($mkey != 10 || $mkey != 11 || $mkey != 12){
                $mkey = '0'.$mkey;
            }*/			
			if(strlen($mkey) == 1){
				$mkey = '0'.$mkey;
			}			
            $monstr .= $mkey.',';
        }		
		
        if(substr($monstr,  strlen($monstr)-1,  strlen($monstr)) == ','){
            $monstr = substr($monstr,0,strlen($monstr)-1);
        }
        
        $monstr = $monstr.',e';

        $datefilter = '(select coalesce(LAST_EXECUTED, CREATED_ON) as datefilter from velink.etool_scheduler where PK_ETSCHEDULER ='.$pk_etschrs['PK_ETSCH'][0].")";
        
        $deltaflag = 0;
        $qryexplod = explode(" ", $cusqueryrs['QUERY'][0]);
        $qryexplodrs = [];
        for($qe=0; $qe<sizeof($qryexplod); $qe++){
            if(substr($qryexplod[$qe],0,1) == ':'){
                $qryexplodrs[$qe] = $qryexplod[$qe];
            }
        }
        $qryexplodrs = array_values($qryexplodrs);
        

        if(sizeof($qryexplodrs)>0){
            $deltaflag = 1;
            $trimmedqry = $cusqueryrs['QUERY'][0];
            for($qel=0; $qel<sizeof($qryexplodrs); $qel++){
                $trimmedqry = str_replace($qryexplodrs[$qel], $datefilter, $trimmedqry);
            }            
            $customquery = $trimmedqry;        
        }else{
            $customquery = $cusqueryrs['QUERY'][0];
        }
        
        if($rec_option == "selreccol"){
            $recipient = "select ','|| rowtocol ('select ".$recipientcol." from (".str_replace("'","''",$customquery).")')||',' as pk_user from dual";            
        }

        $subjectlineqry = "select MSGTXT_SUBJECT from sch_msgtxt where PK_MSGTXT =".$msgtemplatepk;
        $subjectliners = executeOCIQuery($subjectlineqry,$ds_conn);

        
        if($subjectliners['MSGTXT_SUBJECT'][0] != ''){      
            $subjectline = $subjectliners['MSGTXT_SUBJECT'][0];
        }else{
            $subjectline = 'Notification from Query scheduler.';
        }

        
        
        
        //FOR j IN ('.$customquery." and ','||".$recipientcol."||',' like '%,'||i.pk_user||',%'".') -- for correction 
        //FOR j IN ('.$customquery." and ',' like '%,'||i.pk_user||',%'".')
        
        $clientipadd = $_SERVER['SERVER_ADDR'];   
        $schname = strtoupper($schname);
        
        
        /*
        
        $exdir = "select COUNT(*) AS CNTD from ALL_directories where upper(directory_name) = 'ETSCHDIR'";
        $exdirrs = executeOCIQuery($exdir, $ds_conn);
        if($exdirrs['CNTD'][0]==0){
            $directorypathstr = "CREATE DIRECTORY ETSCHDIR AS '".$directorypath."'";
            $results = executeOCIQuery($directorypathstr, $ds_conn);            
        }*/
        $directorypath = getcwd().'\Scheduler';
        $csvname = strtolower($csvname);
        
        if($csvname != ''){
            if($rec_option == 'selreccol'){
                //and '',''||pk_user||'','' like ''%,'||i.PK_USER||',%'''
               $sqlfiledata = '                   
            set serveroutput on;
            SET FEEDBACK OFF;
            SET LINESIZE 32767;
            column fname new_value fname noprint
            select \''.$directorypath.'\\'.$csvname.'\'||to_char(sysdate,\'_yyyyMondd_HHMISS\')||\'.csv\' fname from dual;
            spool &&fname
             BEGIN
                Declare
                    ndate varchar2(100);
                    parsestr varchar2(2000);
                    tcount NUMBER;
                    l_count number;
                    l_value   varchar2(2000);
                    r_txt varchar2(32000);
                    i_value varchar2(500);

                    v_CursorID  NUMBER;
                    v_SelectRecords  VARCHAR2(500);
                    v_NUMRows  INTEGER;
                    CNO NUMBER;
                    CVAL varchar2(32000);

                    F UTL_FILE.FILE_TYPE;
                    CURSOR C1 IS '.$customquery.';
                    C1_R C1%ROWTYPE;
                    v_string varchar2(32000);
                    fdate varchar2(4000);
                    csvname varchar2(4000);

                begin

                  ndate := VELINK.F_NEXTEXECUTIONDATE(\''.$pkquery.'\',\''.$freqency.'\',\''.$pk_etschrs['PK_ETSCH'][0].'\',\''.$startdate.'\',\''.$weekdstr.'\',\''.$monstr.'\',\''.$freqtime.'\');

                    select count(*) into tcount from DBA_tables where table_name = \'ET'.$schname.'TBL\' and owner=\'VELINK\';
                    if tcount > 0 then
                      execute immediate \'DROP TABLE VELINK.ET'.$schname.'TBL\';                      
                    end if;

                    begin        
                        execute immediate \'create table velink.ET'.$schname.'TBL as '.str_replace("'","''",$customquery).'\';
                    commit;
                    end;


                    CNO := 1;
                    v_CursorID := DBMS_SQL.OPEN_CURSOR;
                    v_SelectRecords := \'SELECT * from VELINK.ET'.$schname.'TBL\';
                    DBMS_SQL.PARSE(v_CursorID,v_SelectRecords,DBMS_SQL.V7);

                    FOR I IN (SELECT COLUMN_NAME FROM ALL_TAB_COLS WHERE TABLE_NAME = \'ET'.$schname.'TBL\' AND OWNER = \'VELINK\')
                    LOOP      
                            DBMS_SQL.DEFINE_COLUMN(v_CursorID,CNO,I.COLUMN_NAME,100);
                            v_string := v_string||\',\'||I.COLUMN_NAME;
                            CNO := CNO+1;
                    END LOOP;
                    v_string := substr(v_string,2,length(v_string));                    
                    v_NumRows := DBMS_SQL.EXECUTE(v_CursorID);        
                    select to_char(sysdate,\'_yyyyMondd_HH-MI-SS\') into fdate from dual;                    
                    dbms_output.put_line(v_string);

                    LOOP
                            IF DBMS_SQL.FETCH_ROWS(v_CursorID) = 0 THEN      
                            EXIT;
                            END IF;      
                            CNO := 1;

                            FOR I IN (SELECT COLUMN_NAME FROM ALL_TAB_COLS WHERE TABLE_NAME = \'ET'.$schname.'TBL\' AND OWNER = \'VELINK\')
                            LOOP
                                    DBMS_SQL.COLUMN_VALUE(v_CursorId,CNO,CVAL);                                    
                                    dbms_output.put(CVAL||\',\');
                                    CNO := CNO+1;
                            END LOOP;      
                            dbms_output.put_line(\'\');
                    END LOOP;                    
                    execute immediate \'DROP TABLE VELINK.ET'.$schname.'TBL\';
                    EXCEPTION
                            WHEN OTHERS THEN
                            BEGIN
                            RAISE;
                            DBMS_SQL.CLOSE_CURSOR(v_CursorID);
                            END;

              end;
            END;
            /
            SPOOL OFF;
            exit;';
            }else{
               $sqlfiledata = '
            set serveroutput on;
            SET FEEDBACK OFF;
            SET LINESIZE 32767;
            column fname new_value fname noprint
            select \''.$directorypath.'\\'.$csvname.'\'||to_char(sysdate,\'_yyyyMondd_HHMISS\')||\'.csv\' fname from dual;
            spool &&fname                   
            BEGIN
                Declare
                    r_txt varchar2(32000);
                    ndate varchar2(100);
                    tcount NUMBER;
                    v_CursorID  NUMBER;
                    v_SelectRecords  VARCHAR2(500);
                    v_NUMRows  INTEGER;
                    CNO NUMBER;
                    CVAL varchar2(32000);

                    F UTL_FILE.FILE_TYPE;
                    CURSOR C1 IS '.$customquery.';
                    C1_R C1%ROWTYPE;
                    v_string varchar2(32000);
                    fdate varchar2(4000);
                    csvname varchar2(4000);

                begin
                  ndate := VELINK.F_NEXTEXECUTIONDATE(\''.$pkquery.'\',\''.$freqency.'\',\''.$pk_etschrs['PK_ETSCH'][0].'\',\''.$startdate.'\',\''.$weekdstr.'\',\''.$monstr.'\',\''.$freqtime.'\');
                      
                    select count(*) into tcount from DBA_tables where table_name = \'ET'.$schname.'TBL\' and owner=\'VELINK\';
                    if tcount > 0 then
                      execute immediate \'DROP TABLE VELINK.ET'.$schname.'TBL\';                      
                    end if;

                    begin        
                        execute immediate \'create table velink.ET'.$schname.'TBL as '.str_replace("'","''",$customquery).'\';
                    commit;
                    end;

                    CNO := 1;
                    v_CursorID := DBMS_SQL.OPEN_CURSOR;
                    v_SelectRecords := \'SELECT * from VELINK.ET'.$schname.'TBL\';
                    DBMS_SQL.PARSE(v_CursorID,v_SelectRecords,DBMS_SQL.V7);

                    FOR I IN (SELECT COLUMN_NAME FROM ALL_TAB_COLS WHERE TABLE_NAME = \'ET'.$schname.'TBL\' AND OWNER = \'VELINK\')
                    LOOP      
                            DBMS_SQL.DEFINE_COLUMN(v_CursorID,CNO,I.COLUMN_NAME,100);
                            v_string := v_string||\',\'||I.COLUMN_NAME;
                            CNO := CNO+1;
                    END LOOP;
                    v_string := substr(v_string,2,length(v_string));

                    v_NumRows := DBMS_SQL.EXECUTE(v_CursorID);   
                    dbms_output.put_line(v_string);

                    LOOP
                            IF DBMS_SQL.FETCH_ROWS(v_CursorID) = 0 THEN      
                            EXIT;
                            END IF;      
                            CNO := 1;

                            FOR I IN (SELECT COLUMN_NAME FROM ALL_TAB_COLS WHERE TABLE_NAME = \'ET'.$schname.'TBL\' AND OWNER = \'VELINK\')
                            LOOP
                                    DBMS_SQL.COLUMN_VALUE(v_CursorId,CNO,CVAL);                                    
                                    dbms_output.put(CVAL||\',\');
                                    CNO := CNO+1;
                            END LOOP;      
                            dbms_output.put_line(\'\');
                    END LOOP;                    
                    execute immediate \'DROP TABLE VELINK.ET'.$schname.'TBL\';
                    EXCEPTION
                            WHEN OTHERS THEN
                            BEGIN
                            RAISE;
                            DBMS_SQL.CLOSE_CURSOR(v_CursorID);
                            END;
              end;
            END;
            /
            SPOOL OFF;
            exit;';
            }            
        }else{
            if($rec_option == 'selreccol'){
                //and '',''||pk_user||'','' like ''%,'||i.PK_USER||',%'''
               $sqlfiledata = 'BEGIN
                Declare
                  ndate varchar2(100);
                  parsestr varchar2(2000);
                  l_count number;
                  l_value   varchar2(2000);
                  r_txt varchar2(32000);
                  i_value varchar2(500);
                begin
                  FOR i IN (SELECT PK_USER FROM ERES.ER_USER WHERE ('.$recipient.") like '%,'|| PK_USER || ',%'".')
                  LOOP      
                  i_value := i.pk_user;
                        FOR j IN ('.$customquery." and ','||".$recipientcol."||',' like '%,'||i.pk_user||',%'".')
                        LOOP
                           if INSTR(j.'.$recipientcol.', \',\') = 0 then
                           r_txt := \''.$msglongrs['MSGTXT_LONG'][0].'\';
                           INSERT INTO ESCH.SCH_DISPATCHMSG (PK_MSG, MSG_SENDON,MSG_STATUS,MSG_TYPE,MSG_TEXT,CREATED_ON,IP_ADD,FK_PAT,MSG_SUBJECT) values (ESCH.SCH_DISPATCHMSG_SEQ1.NEXTVAL,SYSDATE,0,\'UR\',r_txt,SYSDATE,\''.$clientipadd.'\',i_value,\''.$subjectline.'\');
                           else                        
                            parsestr := j.'.$recipientcol.';
                            parsestr := parsestr||\',\';
                            l_count := length(parsestr) - length(replace(parsestr,\',\',\'\'));
                            for i in 1 .. l_count loop                          
                              select regexp_substr(parsestr,\'[^,]+\',1,i) into l_value from dual;              
                              if l_value = i_value then
                               r_txt := \''.$msglongrs['MSGTXT_LONG'][0].'\';
                               INSERT INTO ESCH.SCH_DISPATCHMSG (PK_MSG, MSG_SENDON,MSG_STATUS,MSG_TYPE,MSG_TEXT,CREATED_ON,IP_ADD,FK_PAT,MSG_SUBJECT) values (ESCH.SCH_DISPATCHMSG_SEQ1.NEXTVAL,SYSDATE,0,\'UR\',r_txt,SYSDATE,\''.$clientipadd.'\',i_value,\''.$subjectline.'\');
                              end if;
                            end loop;
                           end if;
                        END LOOP;                    
                  END LOOP;              
                  ndate := VELINK.F_NEXTEXECUTIONDATE(\''.$pkquery.'\',\''.$freqency.'\',\''.$pk_etschrs['PK_ETSCH'][0].'\',\''.$startdate.'\',\''.$weekdstr.'\',\''.$monstr.'\',\''.$freqtime.'\');
              end;
            END;
            /
            commit;
            exit;';
            }else{
               $sqlfiledata = 'BEGIN
                Declare
                  r_txt varchar2(32000);
                  ndate varchar2(100);
                begin
                  FOR i IN (SELECT PK_USER FROM ERES.ER_USER WHERE PK_USER IN ('.$recipient.'))
                  LOOP        
                        FOR j IN ('.$customquery.')
                        LOOP
                            r_txt := \''.$msglongrs['MSGTXT_LONG'][0].'\';                        
                           INSERT INTO ESCH.SCH_DISPATCHMSG (PK_MSG, MSG_SENDON,MSG_STATUS,MSG_TYPE,MSG_TEXT,CREATED_ON,IP_ADD,FK_PAT,MSG_SUBJECT) values (ESCH.SCH_DISPATCHMSG_SEQ1.NEXTVAL,SYSDATE,0,\'UR\',r_txt,SYSDATE,\''.$clientipadd.'\',i.PK_USER,\''.$subjectline.'\');
                        END LOOP;
                  END LOOP;              
                  ndate := VELINK.F_NEXTEXECUTIONDATE(\''.$pkquery.'\',\''.$freqency.'\',\''.$pk_etschrs['PK_ETSCH'][0].'\',\''.$startdate.'\',\''.$weekdstr.'\',\''.$monstr.'\',\''.$freqtime.'\');
              end;
            END;
            /
            commit;
            exit;';
            }            
        }
        

	$lincont = '';
	if(substr($os_info,0,3)!="WIN"){		
            $lincont .= '#!/bin/bash '."\n".'(crontab -l ; echo "'.$batchfileOneDataLin.'") 2>&1 | grep -v "no crontab" | sort | uniq | crontab -';
            $batone = $lincont;
	}
        
        if(substr($os_info,0,3)=="WIN"){
            //$execoutput = exec("cmd.exe /c".$batone);
	    //$execoutput = pclose(popen("start /B ".$batone, "r"));
            $output = pclose(popen("start /B ".$batone, "r"));
            if($output == -1){
                echo "<script>alert('WARNING: Problem in creating schedule, check your filled in data.');</script>";  
            }else{
                $filenametobeadded = $bat1filename.','.$bat2filename.','.$sqlfilename;                        
                $batchfileOne = fopen("Scheduler/".$bat1filename, "w") or die("Unable to open ".$bat1filename." file!");     
                fwrite($batchfileOne, $batone);
                fclose($batchfileOne);
                $batchfileTwo = fopen("Scheduler/".$bat2filename, "w") or die("Unable to open ".$bat2filename."  file!");
                fwrite($batchfileTwo, $batchfileTwodata);
                fclose($batchfileTwo);
                $sqlfile = fopen("Scheduler/".$sqlfilename, "w") or die("Unable to open ".$sqlfilename."  file!");
                fwrite($sqlfile, $sqlfiledata);
                fclose($sqlfile);
                //$et_schQry = "Insert into velink.etool_scheduler (PK_ETSCHEDULER,SCH_NAME, FK_QUERY,FK_USER,ETSCH_TYPE,ETSCH_TYPEDATA,LAST_EXECUTED,NEXT_EXECUTION,CREATED_ON,CREATED_BY,STARTDATE,STARTTIME,FILENAMES) values (".$pk_etschrs['PK_ETSCH'][0].",'".$schname."',".$pk_queryman.",'".$recipient."','".$freqency."','".$batone."',null,null,SYSDATE,'".$created_by."','".$startdate."','".$starttime."','".$filenametobeadded."')";                
                if($rec_option != "selreccol"){
                    $et_schQry = "Insert into velink.etool_scheduler (PK_ETSCHEDULER,SCH_NAME, FK_QUERY,FK_USER,ETSCH_TYPE,ETSCH_TYPEDATA,LAST_EXECUTED,NEXT_EXECUTION,CREATED_ON,CREATED_BY,STARTDATE,STARTTIME,FILENAMES) values (".$pk_etschrs['PK_ETSCH'][0].",'".$schname."',".$pk_queryman.",'".$recipient."','".$freqency."','".$batone."',null,null,SYSDATE,'".$created_by."','".$startdate."','".$starttime."','".$filenametobeadded."')";                
                }else{                    
                    //$et_schQry = "Insert into velink.etool_scheduler (PK_ETSCHEDULER,SCH_NAME, FK_QUERY,FK_USER,ETSCH_TYPE,ETSCH_TYPEDATA,LAST_EXECUTED,NEXT_EXECUTION,CREATED_ON,CREATED_BY,STARTDATE,STARTTIME,FILENAMES) values (".$pk_etschrs['PK_ETSCH'][0].",'".$schname."',".$pk_queryman.",'".str_replace("'","''",$recipient)."','".$freqency."','".$batone."',null,null,SYSDATE,'".$created_by."','".$startdate."','".$starttime."','".$filenametobeadded."')";                
                    $et_schQry = "Insert into velink.etool_scheduler (PK_ETSCHEDULER,SCH_NAME, FK_QUERY,FK_USER,ETSCH_TYPE,ETSCH_TYPEDATA,LAST_EXECUTED,NEXT_EXECUTION,CREATED_ON,CREATED_BY,STARTDATE,STARTTIME,FILENAMES) values (".$pk_etschrs['PK_ETSCH'][0].",'".$schname."',".$pk_queryman.",'','".$freqency."','".$batone."',null,null,SYSDATE,'".$created_by."','".$startdate."','".$starttime."','".$filenametobeadded."')";                
                }
                $results = executeOCIUpdateQuery($et_schQry, $ds_conn);
                echo "<script>alert('SUCCESS: your query is scheduled.');</script>";  
            } 
        }else{
            chdir(getcwd().'/Scheduler/');
            $batchfileOne = fopen(getcwd().'/Scheduler/'.$bat1filename, 'w') or die('Cannot open file: '.$bat1filename);
            fwrite($batchfileOne, $batone);
            fclose($batchfileOne);
            $batchfileTwo = fopen(getcwd().'/Scheduler/'.$bat2filename, 'w') or die('Cannot open file: '.$bat2filename);
            fwrite($batchfileTwo, $batchfileTwodata);
            fclose($batchfileTwo);
            $sqlfile = fopen(getcwd().'/Scheduler/'.$sqlfilename, 'w') or die('Cannot open file: '.$sqlfilename);
            fwrite($sqlfile, $sqlfiledata);
            fclose($sqlfile);
            shell_exec("chmod -R 777 ".getcwd().'/Scheduler/'.$bat1filename);
            shell_exec("chmod +x ".getcwd().'/Scheduler/'.$bat1filename);
            shell_exec("chmod -R 777 ".getcwd().'/Scheduler/'.$bat2filename);
            shell_exec("chmod +x ".getcwd().'/Scheduler/'.$bat2filename);
            shell_exec("chmod -R 777 ".getcwd().'/Scheduler/'.$sqlfilename);
            shell_exec("chmod +x ".getcwd().'/Scheduler/'.$sqlfilename);     
            //$et_schQry = "Insert into velink.etool_scheduler (PK_ETSCHEDULER,SCH_NAME, FK_QUERY,FK_USER,ETSCH_TYPE,ETSCH_TYPEDATA,LAST_EXECUTED,NEXT_EXECUTION,CREATED_ON,CREATED_BY,STARTDATE,STARTTIME,FILENAMES) values (".$pk_etschrs['PK_ETSCH'][0].",'".$schname."',".$pk_queryman.",'".$recipient."','".$freqency."','".$batone."',null,null,SYSDATE,'".$created_by."','".$startdate."','".$starttime."','".$filenametobeadded."')";                
            if($rec_option != "selreccol"){
                $et_schQry = "Insert into velink.etool_scheduler (PK_ETSCHEDULER,SCH_NAME, FK_QUERY,FK_USER,ETSCH_TYPE,ETSCH_TYPEDATA,LAST_EXECUTED,NEXT_EXECUTION,CREATED_ON,CREATED_BY,STARTDATE,STARTTIME,FILENAMES) values (".$pk_etschrs['PK_ETSCH'][0].",'".$schname."',".$pk_queryman.",'".str_replace("'","''",$recipient)."','".$freqency."','".$batone."',null,null,SYSDATE,'".$created_by."','".$startdate."','".$starttime."','".$filenametobeadded."')";                
            }else{
                //$et_schQry = "Insert into velink.etool_scheduler (PK_ETSCHEDULER,SCH_NAME, FK_QUERY,FK_USER,ETSCH_TYPE,ETSCH_TYPEDATA,LAST_EXECUTED,NEXT_EXECUTION,CREATED_ON,CREATED_BY,STARTDATE,STARTTIME,FILENAMES) values (".$pk_etschrs['PK_ETSCH'][0].",'".$schname."',".$pk_queryman.",'".$recipient."','".$freqency."','".$batone."',null,null,SYSDATE,'".$created_by."','".$startdate."','".$starttime."','".$filenametobeadded."')";                
                $et_schQry = "Insert into velink.etool_scheduler (PK_ETSCHEDULER,SCH_NAME, FK_QUERY,FK_USER,ETSCH_TYPE,ETSCH_TYPEDATA,LAST_EXECUTED,NEXT_EXECUTION,CREATED_ON,CREATED_BY,STARTDATE,STARTTIME,FILENAMES) values (".$pk_etschrs['PK_ETSCH'][0].",'".$schname."',".$pk_queryman.",'','".$freqency."','".$batone."',null,null,SYSDATE,'".$created_by."','".$startdate."','".$starttime."','".$filenametobeadded."')";                
            }
            $results = executeOCIUpdateQuery($et_schQry, $ds_conn);
            echo "<script>alert( 'SUCCESS: Run/Execute this file ".getcwd().'/Scheduler/'.$bat2filename." to create the task ');</script>";
        }        
        echo '<meta http-equiv="refresh" content="0; url=./query_manager.php">';
    }
?>
</body>
</html>
<?php 
    $db->Close();
    }
    else header("location: index.php?fail=1");
?>
