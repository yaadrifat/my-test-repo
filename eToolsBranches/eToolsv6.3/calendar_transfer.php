<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Calendar Transfer</title>

<?php
include("db_config.php");
include("./includes/oci_functions.php");
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>

<script>
	function calPreview(calPk){
		var win = "calendar_preview.php?calpk="+calPk;		
		window.open(win,'mywin',"toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=825,height=600,top=90,left=150");
	}
</script>

</head>


<body>

<div id="fedora-content">	
<div class="navigate">Calendar Transfer</div>
	
<?PHP


if (isset($_POST["calname"])){
	$searchvalue = $_POST["calname"];
} else {
	$searchvalue = "";
}
?>
<form name="caltransfer" method="post" action="calendar_transfer.php">
<table border = "1">
<tr>
	<td>Search Calendar: </td><td><input name="calname" type="text" size="50" maxlength="200" value="<?PHP echo $searchvalue; ?>"/></td><td><img src="./img/search.png" onMouseOver="this.src='./img/search_m.png';" onMouseOut="this.src='./img/search.png';" onClick="document.caltransfer.submit();"/></td>
</tr>

</table>
	
</form>

<?PHP
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
//$v_account = $_POST["fk_account"];
?>
<Table border="1"><TR>
<TH width="10%">CALENDAR NAME</TH>
<TH width="35%">DESCRIPTION</TH>
<TH width="25%">STATUS</TH>
<TH width="20%">SHARED WITH</TH>
<TH width="10%">&nbsp;</TH>
<TH width="10%">&nbsp;</TH>
</TR>
<?php
$appVq = "select ctrl_value from er_ctrltab where ctrl_key='app_version'";
$appVqrs = executeOCIQuery($appVq,$ds_conn);
$appV = $appVqrs['CTRL_VALUE'][0];


$query_sql = "select event_id,name as calname,description as caldesc,(select CODELST_DESC from sch_codelst where pk_codelst=FK_CODELST_CALSTAT) as calstatus,
decode(calendar_sharedwith,
'A','All Account Users',
'G','All Users in a Group',
'O','All Users in an Organization',
'P','Private',
'S','All Users in Study Team') as calshare
 from event_def where event_type = 'P' AND 
 ((LOWER(trim(name)) LIKE lower('%".str_replace("'","''",trim($searchvalue,' '))."%')) or (LOWER(trim(description)) LIKE lower('%".str_replace("'","''",trim($searchvalue,' '))."%')))
 order by name";
$results = executeOCIQuery($query_sql,$ds_conn);



for ($rec = 0; $rec < $results_nrows; $rec++){
?>
	<TR onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
	<TD><?php echo $results["CALNAME"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["CALDESC"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["CALSTATUS"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["CALSHARE"][$rec] . "&nbsp;"; ?></TD>
<?php
	echo "<td  align=center'><a href=calendar_transfer_step2.php?calid=".$results["EVENT_ID"][$rec]."&calname=".urlencode($results["CALNAME"][$rec])."&av=".$appV.">Select</a></td>";
	echo '<td  align="center"><a href=# onclick="calPreview('.$results["EVENT_ID"][$rec].')">Preview</a></td>';
	echo "</TR>";
}
?>
</TABLE>

<?php } ?>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
