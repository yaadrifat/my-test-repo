<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> View Migration Log</title>

<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Migrate Data - Standard - View Migration Log</div>
<BR>
<table>
<?php

$pk_vlnk_adapmod = $_GET["pk_vlnk_adapmod"];

$query_sql = "SELECT COUNT(*) as count FROM velink.VLNK_PIPE WHERE fk_vlnk_adapmod = ".$pk_vlnk_adapmod;
$results = executeOCIQuery($query_sql,$ds_conn);
echo "<tr><td>Total rows in staging area: </td><td>";
echo $results["COUNT"][0];
echo "</td></tr>";

$query_sql = "SELECT COUNT(*) as count FROM velink.VLNK_PIPE WHERE process_flag = 1 AND fk_vlnk_adapmod = ".$pk_vlnk_adapmod;
$results = executeOCIQuery($query_sql,$ds_conn);
echo "<tr><td>Total rows migrated: </td><td>";
echo $results["COUNT"][0];
if ($results["COUNT"][0] > 0) {	
	echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href='mig_table_view.php?table=VLNK_PIPE&pk_vlnk_adapmod=$pk_vlnk_adapmod&flag=1'>View</a>";
}
echo "</td></tr>";

$query_sql = "SELECT COUNT(*) as count FROM velink.VLNK_PIPE WHERE (process_flag = 0 OR process_flag IS NULL) AND fk_vlnk_adapmod = ".$pk_vlnk_adapmod;
$results = executeOCIQuery($query_sql,$ds_conn);
echo "<tr><td>Total rows pending migration: </td><td>";
echo $results["COUNT"][0];
if ($results["COUNT"][0] > 0) {
	echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href='mig_table_view.php?table=VLNK_PIPE&pk_vlnk_adapmod=$pk_vlnk_adapmod&flag=0'>View</a>";
}
echo "</td></tr>";

$query_sql = "SELECT COUNT(*) as count FROM velink.VLNK_PIPE WHERE process_flag = -1 AND fk_vlnk_adapmod = ".$pk_vlnk_adapmod;
$results = executeOCIQuery($query_sql,$ds_conn);
echo "<tr><td>Total rows with errors: </td><td>";
echo $results["COUNT"][0];
if ($results["COUNT"][0] > 0) {
	echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href='mig_table_view.php?table=VLNK_PIPE&pk_vlnk_adapmod=$pk_vlnk_adapmod&flag=-1'>View</a>";
}
echo "</td></tr>";

$query_sql = "SELECT COUNT(*) as count FROM velink.VLNK_PIPE WHERE process_flag = 2 AND fk_vlnk_adapmod = ".$pk_vlnk_adapmod;
$results = executeOCIQuery($query_sql,$ds_conn);
echo "<tr><td>Total duplicate rows: </td><td>";
echo $results["COUNT"][0];
if ($results["COUNT"][0] > 0) {
	echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href='mig_table_view.php?table=VLNK_PIPE&pk_vlnk_adapmod=$pk_vlnk_adapmod&flag=2'>View</a>";
}
echo "</td></tr>";

$query_sql = "SELECT pk_vlnk_pipe,to_char(process_on,'mm/dd/yyyy HH:MI AM') as process_on,col1,col2,col3,col4,(SELECT message from velink.vlnk_log where fk_vlnk_pipe = pk_vlnk_pipe and pk_vlnk_log = (select max(pk_vlnk_log) from velink.vlnk_log where fk_vlnk_pipe = pk_vlnk_pipe group by fk_vlnk_pipe)) as message FROM velink.VLNK_PIPE WHERE process_flag = -1 AND fk_vlnk_adapmod = ".$pk_vlnk_adapmod;
$results = executeOCIQuery($query_sql,$ds_conn);
	
?>
</table>
</div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
