<?php
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velos eTools -> Quick Calendar</title>
</head>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");
include("./includes/calendar.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body>
<div id="fedora-content">	
<?PHP

$v_category = $_POST["category"]; //-- Calendar type (selected in the csv upload form)
$v_eventtype = $_POST["eventtype"]; //-- Event type
$v_account = $_POST["account"]; //-- user account no
$v_calname = $_POST["calname"];//-- Calendar name
$v_caldur = $_POST["calduration"]; //-- calendar duration
$v_durunit = $_POST["durunit"]; //-- calendar duration unit
$v_column = array_values($_POST["column"]); //-- CSV row can be written
$v_categoryName = $_POST["column1"]; //-- The eliminated rows from CSV

$catDUP = $_SESSION["mapedarr"];  //-- Duplicated category list (Before mapping)
$catLIST = $_SESSION["mapedarray"]; //-- Final Category list (after mapping) for the entire row in CSV
$catSRC = $_SESSION['srarray']; //-- Source Category from CSV with unique value
$eveList = $_SESSION['evename']; //-- Events name list from CSV
$tobewritten = $_SESSION['tobewritten'];
$splvisitArr = $_SESSION['splvisitArr']; //-- visit interval (whatever X in the csv


//-- ordering the column array and category list array
$newcol = array();
$caL = array();
for($col=0; $col < count($v_column); $col++){    
    $tmpcol = $v_column[$col];
    $caL[$col] = $tmpcol[1];
    unset($tmpcol[1]);    
    $newcol[$col] = array_values($tmpcol);
    unset($caL[0]);    
}
$v_column = $newcol;
array_values($caL);




for($ci=1; $ci<=count($v_column); $ci++){
    $v_categoryNa[$ci-1] = $v_column[$ci][1];
}

$v_migrate = (isset($_POST["migrate"])?"YES":"");

$results = executeOCIQuery("SELECT event_definition_seq.NEXTVAL as pk_protocol FROM dual",$ds_conn);
$v_pk_protocol = $results["PK_PROTOCOL"][0];

$pk_calStat_q = executeOCIQuery("select pk_codelst from sch_codelst where codelst_type = 'calStatLib'  and codelst_subtyp = 'W'",$ds_conn);
$pk_calStat = $pk_calStat_q["PK_CODELST"][0];

// Writing a calendar name under event_type as 'P'
$v_sql = "insert into event_def (EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,COST,DURATION,USER_ID,FUZZY_PERIOD,FK_CODELST_CALSTAT,
			DESCRIPTION,DISPLACEMENT,ORG_ID,EVENT_FLAG,CALENDAR_SHAREDWITH,DURATION_UNIT,FK_VISIT,EVENT_FUZZYAFTER,
			EVENT_DURATIONAFTER,EVENT_DURATIONBEFORE,FK_CATLIB,EVENT_LIBRARY_TYPE) values ($v_pk_protocol, $v_pk_protocol, 'P','$v_calname',0,$v_caldur,
			$v_account,null,$pk_calStat,'$v_calname',0,0,0,'A','$v_durunit',null,null,'D','D',$v_category,$v_eventtype)";
executeOCIUpdateQuery($v_sql,$ds_conn);


$v_counter = 0;
$k=0;
$j=0;

$vname_interval = [];

//-- writing visits
foreach($v_column as $rows=>$columns){
	$i=0;
	$v_cols = 0;
        // Fetching each column
	foreach($columns as $column){
		if ($v_counter == 0){
                     if ($v_cols > 0){

                            switch(substr($column,0,1))
                            {
                                case 'F':
                                    $curcolumn = substr($column,2,  strlen($column));
                                    //list($v_visit_name,$v_visit) = explode('|',$curcolumn);
                                    $curcolumnList = explode('|',$column);                                    
                                    $v_visit = $curcolumnList[2];
                                    $v_visit_name = $curcolumnList[1];
                                    
                                    if (strlen($v_visit_name) == 0) $v_visit = $v_visit_name;
                                    $no_interval_flag = 0;
                                    $insert_after = 0;
                                    break;
                                
                                case 'D':
                                    $curcolumn = substr($column,2,strlen($column));
                                    
                                    $curcolumnList = explode('|',$curcolumn);
                                    $v_visit = $curcolumnList[0];
                                    $v_visit_name = $curcolumnList[1];
                                    $dependvisitname = $curcolumnList[2];
                                    
                                    $no_interval_flag = 0;
                                    $pkprotocolqry = "select * from SCH_PROTOCOL_VISIT where FK_PROTOCOL = ".$v_pk_protocol." and VISIT_NAME='".$dependvisitname."'";                                    
                                    $pkprotocolqryrs = executeOCIQuery($pkprotocolqry,$ds_conn);
                                    
                                    if($pkprotocolqryrs["PK_PROTOCOL_VISIT"][0] != ''){
                                        $insert_after = $pkprotocolqryrs["PK_PROTOCOL_VISIT"][0];
                                    }else{
                                        $insert_after = 0;
                                    }
                                    break;
                                
                                case 'N':
                                    //list($v_visit,$v_visit_name) = explode('|',$column);
                                    
                                    $curcolumnList = explode('|',$column);
                                    $v_visit = $curcolumnList[2];
                                    $v_visit_name = $curcolumnList[1];
                                    
                                    if (strlen($v_visit_name) == 0) $v_visit_name = $v_visit;
                                    $no_interval_flag = 1;
                                    $insert_after = 0;
                                    break;
                            }
                            
                            list($v_months,$v_weeks,$v_days,$v_disp) = explode('|',getDisplacement_monweeday($v_visit));
                            //$v_disp = getDisplacement($v_visit);

                            //implemented for the bug 6460 and 6459
                            if($v_days==0 && $v_months==0 && $v_weeks==0){
                                    $v_days=0;
                            }
                            //implemented for the bug 25802
                            if(substr($column,0,1)=='D'){
                                $tv = $v_visit;
                                $v_visit = $v_visit_name;
                                $v_visit_name = $tv;
                                
                                $INSERT_AFTER_INTERVAL_UNIT = strtoupper(substr($v_visit,0,1));
                                $INSERT_AFTER_INTERVAL = substr($v_visit,1,strlen($v_visit));

                                $disCalqry = "select DISPLACEMENT from sch_protocol_visit where visit_name = '".$dependvisitname."' and fk_protocol = ".$v_pk_protocol;
                                $disCalqryrs = executeOCIQuery($disCalqry,$ds_conn);
                                switch($INSERT_AFTER_INTERVAL_UNIT){
                                    case 'W':
                                        $di = $INSERT_AFTER_INTERVAL * 7;
                                        $v_disp = $di+$disCalqryrs['DISPLACEMENT'][0];
                                        $v_days = $v_disp;
                                        $v_weeks = 0;
                                        break;
                                    
                                    case 'M':
                                        $di = $INSERT_AFTER_INTERVAL * 30;
                                        $v_disp = $di+$disCalqryrs['DISPLACEMENT'][0];
                                        $v_days = $v_disp;
                                        $v_months = 0;
                                        break;
                                    
                                    case 'D':
                                        $v_disp = $INSERT_AFTER_INTERVAL+$disCalqryrs['DISPLACEMENT'][0];
                                        break;
                                }
                            }else{
                                $INSERT_AFTER_INTERVAL_UNIT = '';
                                $INSERT_AFTER_INTERVAL = 0;
                            }
                            if(substr($column,0,1)=='N'){
                                $v_days = 'null';
                            }
                            
                            //$no_interval_flag = 0;
                            $results = executeOCIQuery("SELECT sch_prot_visit_seq.nextval as pk_visit FROM dual",$ds_conn);
                            $v_pk_visit = $results["PK_VISIT"][0];
                            
                            /*$v_sql = "insert into sch_protocol_visit (PK_PROTOCOL_VISIT,FK_PROTOCOL,VISIT_NO,VISIT_NAME,DISPLACEMENT,NUM_MONTHS,
                            NUM_WEEKS,NUM_DAYS,INSERT_AFTER,INSERT_AFTER_INTERVAL,creator,last_modified_by,NO_INTERVAL_FLAG) values ($v_pk_visit, $v_pk_protocol,$v_cols,'$v_visit_name',$v_disp,
                            $v_months,$v_weeks,$v_days,$insert_after,0,0,0,$no_interval_flag)";*/
                            //implemented for the bug 25802
                            $v_sql = "insert into sch_protocol_visit (PK_PROTOCOL_VISIT,FK_PROTOCOL,VISIT_NO,VISIT_NAME,DISPLACEMENT,NUM_MONTHS,
                            NUM_WEEKS,NUM_DAYS,INSERT_AFTER,INSERT_AFTER_INTERVAL,creator,last_modified_by,NO_INTERVAL_FLAG,INSERT_AFTER_INTERVAL_UNIT) values ($v_pk_visit, $v_pk_protocol,$v_cols,'$v_visit_name',$v_disp,
                            $v_months,$v_weeks,$v_days,$insert_after,$INSERT_AFTER_INTERVAL,0,0,$no_interval_flag,'$INSERT_AFTER_INTERVAL_UNIT')";
                            
                            
                            executeOCIUpdateQuery($v_sql,$ds_conn);
                            $v_visitno[$i] = $v_cols;
                            $v_visitdisp[$i] = $v_disp;                            
                            $v_visitpk[$i] = $v_pk_visit;
                            $i++;                         
                            $no_interval_flag = 0;
                     }
                    $v_cols++;
		}else{

                        if ($i == 0) {
                            $v_event[$k] = $column; //-- writes event name
                            $v_eventname = $column; //-- ?
                            $v_event_seq[$k] = $v_counter;                                
                            $k++;
                            $i++;                        
                        }else{
                            if (strlen($column) > 0) {
                                //echo 'c, ';
                                //echo $v_eventname;
                                $v_visiteventname[$j] = $v_eventname;
                                $v_visiteventno[$j] = $v_counter;                            
                                for($x=0; $x<count($v_visitno); $x++) {
                                    //echo $v_visitdisp[$x]."|".$v_visitno[$x]."|".$v_cols."+++++++++++++++<BR>";                                
                                    if ($v_visitno[$x] == $v_cols) {
                                        $v_visiteventdisp[$j] = $v_visitdisp[$x];
                                        $v_visiteventpk[$j] = $v_visitpk[$x];
                                        //echo "**************************";
                                    }
                                }
                                $j++;
                            }                            
                        }
		$v_cols++;                
		}
	}
	$v_counter++;
}

//
//echo '<pre>';
//print_r($v_visiteventname);
//echo '</pre><br><br>';
//
//
//echo '<pre>';
//print_r($v_event);
//echo '</pre><br><br>';

$tmpeventid = array();
$tmpeventcat = array();
$tmpeventname = array();
for($i=0; $i<count($v_event); $i++) {
	$results = executeOCIQuery("SELECT event_definition_seq.NEXTVAL as pk_event FROM dual",$ds_conn);
	$v_pk_event = $results["PK_EVENT"][0];
	$tmpeventid[$i] = $v_pk_event;
        
	/* ---------- Calendar from file description character restriction -------- */	
	$event_des = substr($v_event[$i],0,200);
        $cii = $i+1;
        $tmpeventcat[$i] = $caL[$cii];
        $tmpeventname[$i] = $v_event[$i];
	/*$v_sql = "insert into event_def (EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,COST,DURATION,USER_ID,FUZZY_PERIOD,STATUS,
				DESCRIPTION,DISPLACEMENT,ORG_ID,EVENT_FLAG,CALENDAR_SHAREDWITH,DURATION_UNIT,FK_VISIT,EVENT_FUZZYAFTER,
				EVENT_DURATIONAFTER,EVENT_DURATIONBEFORE,FK_CATLIB,EVENT_LIBRARY_TYPE) values ($v_pk_event, $v_pk_protocol, 'A','$v_event[$i]',$v_event_seq[$i],0,
				$v_account,0,null,'$event_des',0,0,0,'A','$v_durunit',null,0,'D','D',$v_category,$v_eventtype)";*/
        // fix for the bug 24849
        $v_sql = "insert into event_def (EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,COST,DURATION,USER_ID,FUZZY_PERIOD,STATUS,
				DESCRIPTION,DISPLACEMENT,ORG_ID,EVENT_FLAG,CALENDAR_SHAREDWITH,DURATION_UNIT,FK_VISIT,EVENT_FUZZYAFTER,
				EVENT_DURATIONAFTER,EVENT_DURATIONBEFORE,FK_CATLIB,EVENT_LIBRARY_TYPE,EVENT_CATEGORY) values ($v_pk_event, $v_pk_protocol, 'A','$v_event[$i]',$v_event_seq[$i],0,
				$v_account,0,null,'$event_des',0,0,0,'A','$v_durunit',null,0,'D','D',$v_category,$v_eventtype,'$caL[$cii]')";

        executeOCIUpdateQuery($v_sql,$ds_conn);
}

//echo '<pre>';
//print_r($tmpeventid);
//echo '</pre><br>';
//
//echo '<pre>';
//print_r($tmpeventcat);
//echo '</pre><br>';
//
//echo '<pre>';
//print_r($tmpeventname);
//echo '</pre>';


$v_categoryName = array_values($v_categoryName);
$v_cat = array();
for($y=0; $y<count($v_categoryName); $y++){
    $v_cat[$y] = $v_categoryName[$y][1];
}


$fkcc = '';
for($i=0; $i<count($v_visiteventname); $i++) {

    
        if($v_visiteventdisp[$i] == ''){
            $v_visiteventdisp_val = 0; 
        }else{
            $v_visiteventdisp_val = $v_visiteventdisp[$i]; 
        }
        if($v_visiteventpk[$i] == ''){
            $v_visiteventpk_val = 'null'; 
        }else{
            $v_visiteventpk_val = $v_visiteventpk[$i];
        }    
    
	$results = executeOCIQuery("SELECT event_definition_seq.NEXTVAL as pk_event FROM dual",$ds_conn);
	$v_pk_event = $results["PK_EVENT"][0];	
	
        $tevent[$i] = $v_pk_event;

        //filling the coverage analysis
        
        if(strlen($splvisitArr[$i+1]) > 1){
            $splchar = strtoupper(substr($splvisitArr[$i+1],2,strlen($splvisitArr[$i+1])));
            $splQry = "select pk_codelst from sch_codelst where codelst_type = 'coverage_type' and CODELST_SUBTYP = '".$splchar."'";
            $splQryrs = executeOCIQuery($splQry,$ds_conn);
            $fkcc = $splQryrs['PK_CODELST'][0];
            
        }
        if($fkcc == ''){
            $fkcc = 'null';
        }
	/* ---------- Calendar from file description character restriction -------- */

	$v_sql = "insert into event_def (EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,COST,DURATION,USER_ID,FUZZY_PERIOD,STATUS,
				DISPLACEMENT,ORG_ID,EVENT_FLAG,CALENDAR_SHAREDWITH,DURATION_UNIT,FK_VISIT,EVENT_FUZZYAFTER,
				EVENT_DURATIONAFTER,EVENT_DURATIONBEFORE,FK_CATLIB,EVENT_LIBRARY_TYPE,FK_CODELST_COVERTYPE) values ($v_pk_event, $v_pk_protocol, 'A','$v_visiteventname[$i]',$v_visiteventno[$i],0,
				$v_account,0,null,$v_visiteventdisp_val,0,0,'A','$v_durunit',$v_visiteventpk_val,0,'D','D',$v_category,$v_eventtype,$fkcc)";        
	executeOCIUpdateQuery($v_sql,$ds_conn);
	$fkcc = '';

        
	if ($v_migrate == "YES"){
            
            // for the bug #25808
            for($yi=0; $yi<count($v_cat); $yi++){                
                //for the bug #25822
                //$v_queryCAT = "select name from event_def where event_id = (SELECT chain_id FROM event_def WHERE EVENT_TYPE = 'E' AND user_id = ".$v_account." AND trim(upper(name)) = trim(upper('$v_visiteventname[$i]')))";
                //$v_queryCAT = "select name from event_def where event_id = (SELECT event_id FROM event_def WHERE EVENT_TYPE = 'E' AND user_id = ".$v_account." AND trim(upper(name)) = trim(upper('$v_visiteventname[$i]')) and chain_id = (SELECT event_id FROM event_def WHERE event_type = 'L' and user_id = ".$v_account." AND trim(upper(name)) = trim(upper('".$v_categoryName[$y][$i+1]."')) ))";
                $v_queryCAT = "select name from event_def where event_id = (SELECT event_id FROM event_def WHERE EVENT_TYPE = 'E' AND user_id = ".$v_account." AND trim(upper(name)) = trim(upper('$v_visiteventname[$i]')) and chain_id = (SELECT event_id FROM event_def WHERE event_type = 'L' and user_id = ".$v_account." AND trim(upper(name)) = trim(upper('".$v_cat[$yi]."')) ))";                            
                $resultsCAT = executeOCIQuery($v_queryCAT,$ds_conn);
                $ecat = $resultsCAT["NAME"][0];                
                
                //Checking the calendar existence 
                // for the bug #25808
                if($ecat != ''){
                
                //for the bug #25822
                //$v_query = "SELECT EVENT_ID,PAT_DAYSBEFORE, PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE, PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER,description, fuzzy_period, event_fuzzyafter, event_durationafter, event_cptcode, event_durationbefore,notes,duration,duration_unit FROM event_def WHERE EVENT_TYPE = 'E' and user_id = ".$v_account." and trim(upper(name)) = trim(upper('$v_visiteventname[$i]')) and chain_id = (SELECT chain_id FROM event_def WHERE EVENT_TYPE = 'E' AND user_id = ".$v_account." AND trim(upper(name)) = trim(upper('$v_visiteventname[$i]')))";
                $v_query = "SELECT EVENT_ID,PAT_DAYSBEFORE, PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE, PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER,description, fuzzy_period, event_fuzzyafter, event_durationafter, event_cptcode, event_durationbefore,notes,duration,duration_unit FROM event_def WHERE EVENT_TYPE = 'E' and user_id = ".$v_account." and trim(upper(name)) = trim(upper('$v_visiteventname[$i]')) and chain_id = (SELECT event_id FROM event_def WHERE event_type = 'L' and user_id = ".$v_account." AND trim(upper(name)) = trim(upper('".$v_cat[$yi]."')))";
                //echo '<br>'.$v_query.'<br>';                
		$results = executeOCIQuery($v_query,$ds_conn);
                
                //echo $v_pk_event.'<br>';
                
		if ($results_nrows > 0) {                        
			$v_libpk = $results["EVENT_ID"][0];
			$v_sql = "update event_def set PAT_DAYSBEFORE = ".(strlen($results["PAT_DAYSBEFORE"][0]) ==0?"null":$results["PAT_DAYSBEFORE"][0]).",
			PAT_DAYSAFTER = ".(strlen($results["PAT_DAYSAFTER"][0]) ==0?"null":$results["PAT_DAYSAFTER"][0]).",
			USR_DAYSBEFORE = ".(strlen($results["USR_DAYSBEFORE"][0]) ==0?"null":$results["USR_DAYSBEFORE"][0]).", 
			USR_DAYSAFTER = ".(strlen($results["USR_DAYSAFTER"][0]) ==0?"null":$results["USR_DAYSAFTER"][0]).", 
			PAT_MSGBEFORE = '".$results["PAT_MSGBEFORE"][0]."', 
			PAT_MSGAFTER = '".$results["PAT_MSGAFTER"][0]."', 
			USR_MSGBEFORE = '".$results["USR_MSGBEFORE"][0]."', 
			USR_MSGAFTER = '".$results["USR_MSGAFTER"][0]."',
			DESCRIPTION = '".$results["DESCRIPTION"][0]."', 
			FUZZY_PERIOD = '".$results["FUZZY_PERIOD"][0]."', 
			EVENT_FUZZYAFTER = '".$results["EVENT_FUZZYAFTER"][0]."', 
			EVENT_DURATIONAFTER = '".$results["EVENT_DURATIONAFTER"][0]."', 
			EVENT_CPTCODE = '".$results["EVENT_CPTCODE"][0]."', 
			EVENT_DURATIONBEFORE = '".$results["EVENT_DURATIONBEFORE"][0]."', 
			DURATION = '".$results["DURATION"][0]."', 
			DURATION_UNIT = '".$results["DURATION_UNIT"][0]."', 
			NOTES = '".$results["NOTES"][0]."'
			where event_id = $v_pk_event";
			executeOCIUpdateQuery($v_sql,$ds_conn);

			$v_sql = "insert into sch_eventcost (PK_EVENTCOST, EVENTCOST_VALUE, FK_EVENT, FK_COST_DESC,FK_CURRENCY)  
			select esch.sch_eventcost_seq.nextval,EVENTCOST_VALUE, $v_pk_event, FK_COST_DESC,FK_CURRENCY from sch_eventcost where FK_EVENT = $v_libpk";
			executeOCIUpdateQuery($v_sql,$ds_conn);

			$v_sql = "insert into sch_eventusr (PK_EVENTUSR, EVENTUSR, EVENTUSR_TYPE, FK_EVENT, EVENTUSR_DURATION, EVENTUSR_NOTES)  
			select esch.sch_eventusr_seq.nextval,EVENTUSR, EVENTUSR_TYPE, $v_pk_event, EVENTUSR_DURATION, EVENTUSR_NOTES from sch_eventusr where FK_EVENT = $v_libpk";
                        executeOCIUpdateQuery($v_sql,$ds_conn);

			$v_query = "SELECT PK_EVENTDOC, PK_DOCS, FK_EVENT from sch_eventdoc where fk_event = $v_libpk";
			$results = executeOCIQuery($v_query,$ds_conn);
			$rows = $results_nrows;
			for ($rec = 0; $rec < $rows; $rec++){
				$v_query = "SELECT esch.sch_docs_seq.nextval as pk from dual";
				$results1 = executeOCIQuery($v_query,$ds_conn);
				$v_pk_docs = $results1["PK"][0];
				$v_sql = "insert into sch_docs (PK_DOCS,DOC_NAME,DOC_DESC,DOC_TYPE,DOC,DOC_SIZE)  
				select $v_pk_docs,DOC_NAME,DOC_DESC,DOC_TYPE,DOC,DOC_SIZE from sch_docs where pk_docs = ".$results["PK_DOCS"][$rec];
                                executeOCIUpdateQuery($v_sql,$ds_conn);
				$v_sql = "insert into sch_eventdoc (PK_EVENTDOC, PK_DOCS, FK_EVENT)  
				values (esch.sch_eventdoc_seq.nextval,$v_pk_docs,$v_pk_event)";
                                executeOCIUpdateQuery($v_sql,$ds_conn);
			}

		}
                
                }    
            }  
	}
}

//source record updates
//$tmpeventid
//$tmpeventcat
//$tmpeventname

//for the bug #27372
if ($v_migrate == "YES"){
    for($i=0; $i<count($tmpeventid); $i++) {
        $v_queryCAT = "select name from event_def where event_id = (SELECT event_id FROM event_def WHERE EVENT_TYPE = 'E' AND user_id = ".$v_account." AND trim(upper(name)) = trim(upper('$tmpeventname[$i]')) and chain_id = (SELECT event_id FROM event_def WHERE event_type = 'L' and user_id = ".$v_account." AND trim(upper(name)) = trim(upper('".$tmpeventcat[$yi]."')) ))";                            
        $resultsCAT = executeOCIQuery($v_queryCAT,$ds_conn);
        $ecat = $resultsCAT["NAME"][0];

        //Checking the calendar existence 
        // for the bug #25808
        if($ecat != ''){

        //for the bug #25822
        //$v_query = "SELECT EVENT_ID,PAT_DAYSBEFORE, PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE, PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER,description, fuzzy_period, event_fuzzyafter, event_durationafter, event_cptcode, event_durationbefore,notes,duration,duration_unit FROM event_def WHERE EVENT_TYPE = 'E' and user_id = ".$v_account." and trim(upper(name)) = trim(upper('$v_visiteventname[$i]')) and chain_id = (SELECT chain_id FROM event_def WHERE EVENT_TYPE = 'E' AND user_id = ".$v_account." AND trim(upper(name)) = trim(upper('$v_visiteventname[$i]')))";
        $v_query = "SELECT EVENT_ID,PAT_DAYSBEFORE, PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE, PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER,description, fuzzy_period, event_fuzzyafter, event_durationafter, event_cptcode, event_durationbefore,notes,duration,duration_unit FROM event_def WHERE EVENT_TYPE = 'E' and user_id = ".$v_account." and trim(upper(name)) = trim(upper('$tmpeventname[$i]')) and chain_id = (SELECT event_id FROM event_def WHERE event_type = 'L' and user_id = ".$v_account." AND trim(upper(name)) = trim(upper('".$tmpeventcat[$yi]."')))";
        //echo '<br>'.$v_query.'<br>';                
        $results = executeOCIQuery($v_query,$ds_conn);

        //echo $v_pk_event.'<br>';

        if ($results_nrows > 0) {                        
                $v_libpk = $results["EVENT_ID"][0];
                $v_sql = "update event_def set PAT_DAYSBEFORE = ".(strlen($results["PAT_DAYSBEFORE"][0]) ==0?"null":$results["PAT_DAYSBEFORE"][0]).",
                PAT_DAYSAFTER = ".(strlen($results["PAT_DAYSAFTER"][0]) ==0?"null":$results["PAT_DAYSAFTER"][0]).",
                USR_DAYSBEFORE = ".(strlen($results["USR_DAYSBEFORE"][0]) ==0?"null":$results["USR_DAYSBEFORE"][0]).", 
                USR_DAYSAFTER = ".(strlen($results["USR_DAYSAFTER"][0]) ==0?"null":$results["USR_DAYSAFTER"][0]).", 
                PAT_MSGBEFORE = '".$results["PAT_MSGBEFORE"][0]."', 
                PAT_MSGAFTER = '".$results["PAT_MSGAFTER"][0]."', 
                USR_MSGBEFORE = '".$results["USR_MSGBEFORE"][0]."', 
                USR_MSGAFTER = '".$results["USR_MSGAFTER"][0]."',
                DESCRIPTION = '".$results["DESCRIPTION"][0]."', 
                FUZZY_PERIOD = '".$results["FUZZY_PERIOD"][0]."', 
                EVENT_FUZZYAFTER = '".$results["EVENT_FUZZYAFTER"][0]."', 
                EVENT_DURATIONAFTER = '".$results["EVENT_DURATIONAFTER"][0]."', 
                EVENT_CPTCODE = '".$results["EVENT_CPTCODE"][0]."', 
                EVENT_DURATIONBEFORE = '".$results["EVENT_DURATIONBEFORE"][0]."', 
                DURATION = '".$results["DURATION"][0]."', 
                DURATION_UNIT = '".$results["DURATION_UNIT"][0]."', 
                NOTES = '".$results["NOTES"][0]."'
                where event_id = $tmpeventid[$i]";
                executeOCIUpdateQuery($v_sql,$ds_conn);

                $v_sql = "insert into sch_eventcost (PK_EVENTCOST, EVENTCOST_VALUE, FK_EVENT, FK_COST_DESC,FK_CURRENCY)  
                select esch.sch_eventcost_seq.nextval,EVENTCOST_VALUE, $tmpeventid[$i], FK_COST_DESC,FK_CURRENCY from sch_eventcost where FK_EVENT = $v_libpk";
                executeOCIUpdateQuery($v_sql,$ds_conn);

                $v_sql = "insert into sch_eventusr (PK_EVENTUSR, EVENTUSR, EVENTUSR_TYPE, FK_EVENT, EVENTUSR_DURATION, EVENTUSR_NOTES)  
                select esch.sch_eventusr_seq.nextval,EVENTUSR, EVENTUSR_TYPE, $tmpeventid[$i], EVENTUSR_DURATION, EVENTUSR_NOTES from sch_eventusr where FK_EVENT = $v_libpk";
                executeOCIUpdateQuery($v_sql,$ds_conn);

                $v_query = "SELECT PK_EVENTDOC, PK_DOCS, FK_EVENT from sch_eventdoc where fk_event = $v_libpk";
                $results = executeOCIQuery($v_query,$ds_conn);
                $rows = $results_nrows;
                for ($rec = 0; $rec < $rows; $rec++){
                        $v_query = "SELECT esch.sch_docs_seq.nextval as pk from dual";
                        $results1 = executeOCIQuery($v_query,$ds_conn);
                        $v_pk_docs = $results1["PK"][0];
                        $v_sql = "insert into sch_docs (PK_DOCS,DOC_NAME,DOC_DESC,DOC_TYPE,DOC,DOC_SIZE)  
                        select $v_pk_docs,DOC_NAME,DOC_DESC,DOC_TYPE,DOC,DOC_SIZE from sch_docs where pk_docs = ".$results["PK_DOCS"][$rec];
                        executeOCIUpdateQuery($v_sql,$ds_conn);
                        $v_sql = "insert into sch_eventdoc (PK_EVENTDOC, PK_DOCS, FK_EVENT)  
                        values (esch.sch_eventdoc_seq.nextval,$v_pk_docs,$tmpeventid[$i])";
                        executeOCIUpdateQuery($v_sql,$ds_conn);
                }

        }

        }        
    }
}    





$v_query = "SELECT EVENT_ID,FK_VISIT, COST  from event_def where event_type = 'A' and chain_id =".$v_pk_protocol." and displacement <> 0 order by displacement,name";
$results = executeOCIQuery($v_query,$ds_conn);
$v_seq = 1;
$v_visit = "";


for ($rec = 0; $rec < $results_nrows; $rec++){        
    
    $v_query = "update event_def set event_sequence =".$results["COST"][$rec]." where event_id = ".$results["EVENT_ID"][$rec];
    executeOCIUpdateQuery($v_query,$ds_conn);
        
}
echo "Calendar <font color=green><b>$v_calname</b></font> created.";


$reC = executeOCIQuery("SELECT * from EVENT_DEF where NAME='".$v_calname."' and EVENT_TYPE='P' and USER_ID='".$v_account."'",$ds_conn);
$v_pk_calid = $reC["EVENT_ID"][0];

$results1 = executeOCIQuery("SELECT seq_er_objectshare.NEXTVAL as pk_objectshare FROM dual",$ds_conn);
$v_pk_objectshare = $results1["PK_OBJECTSHARE"][0];
$v_sql = "insert into er_objectshare (pk_objectshare,object_number,fk_object,fk_objectshare_id,objectshare_type,record_type) values ($v_pk_objectshare,3,$v_pk_calid,$v_account,'A','N')";
$results = executeOCIUpdateQuery($v_sql, $ds_conn);

// each calendar name entry in EVENT_DEF should write relevant records in ER_OBJECTSHARE table with no of users belongs to the account which users select while import calendar in etools

$query = "SELECT * from ER_USER where FK_ACCOUNT=$v_account";
$rs = executeOCIQuery($query, $ds_conn);
$totalrow = $results_nrows;

for($rr=0; $rr < $totalrow; $rr++){	
	$objshare = $rs["PK_USER"][$rr];
	$results = executeOCIQuery("SELECT seq_er_objectshare.NEXTVAL as pk_objshr FROM dual", $ds_conn);
	$v_pk_objshr = $results["PK_OBJSHR"][0];
	$v_sql = "insert into er_objectshare (pk_objectshare,object_number,fk_object,fk_objectshare_id,objectshare_type,record_type,fk_objectshare) values ($v_pk_objshr,3,$v_pk_calid,$objshare,'U','N',(select PK_OBJECTSHARE from ER_OBJECTSHARE where FK_OBJECT=$v_pk_calid and OBJECTSHARE_TYPE='A'))";	
	$results = executeOCIUpdateQuery($v_sql,$ds_conn);	
}

?>	
</div>
</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
