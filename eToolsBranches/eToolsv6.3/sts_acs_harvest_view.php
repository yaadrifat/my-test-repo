<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velink -> STS Harvest View</title>
</head>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body>

<div id="fedora-content">	
<p fontsize="12"><b><u>STS Adult Cardiac Surgery Registry Harvest Files</u></b></p>
<?PHP

$v_command = 'dir /b .\\sts_harvest\\*.*';
$output = "";
$return_var = "";
exec($v_command,$output,$return_var);

foreach($output as $temp_output){
	echo '<a href="./sts_harvest/'.$temp_output.'">'.$temp_output.'</a>';
	echo "<BR>";
}

	
OCICommit($ds_conn);
OCILogoff($ds_conn);
?>

	
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
