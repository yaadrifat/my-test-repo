<?php
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.
if(@$_SESSION["user"]){
?>
<html>
<head>
    <title>Velos eTools -> Users</title>
<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");
require_once('audit_queries.php');
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
<script>
function validate(form,mode){
	if (form.pass1.value != form.pass2.value){
		alert("Password does not match, please re-enter password.");
		return false;
	}
	if (form.loguser.value == ""){
		alert("Login name cannot be blank.");
		return false;
	}
	if (form.group.value == ""){
		alert("Group cannot be blank.");
		return false;
	}
	if (mode == 'n') {
		if (form.pass1.value == "" || form.pass2.value == ""){
			alert("Password cannot be blank.");
			return false;
		}
	}
}
</script>
</head>
<body >
<div id="fedora-content">	
<?PHP
if (isset($_GET["mode"])) $v_mode = $_GET["mode"];
if (isset($_POST["mode"])) $v_mode = $_POST["mode"];
if (!($_SERVER['REQUEST_METHOD'] == 'POST')) {
	if ($v_mode == 'n'){
		$v_grpName = "";
		$v_pk_users = "";
		$v_fname = "";
		$v_lname = "";
		$v_email = "";
		$v_phone = "";
		$v_loguser = "";
	} else {
		$v_pk_users = $_GET["pk_users"];
                //$rs = mysql_query("SELECT pk_users,first_name, last_name, group_name, phone, email,user_name FROM et_users, et_groups WHERE et_groups.pk_groups = et_users.fk_groups and pk_users =".$v_pk_users);                
                $rs = "SELECT pk_users,first_name, last_name, group_name, phone, email,user_name FROM et_users, et_groups WHERE et_groups.pk_groups = et_users.fk_groups and pk_users =".$v_pk_users;
                $rs = $db->query($rs);
                //$row = mysql_fetch_array($rs);
                $row = $rs->fetch_assoc();
		$v_grpName = $row["group_name"];
		$v_fname = $row["first_name"];
		$v_lname = $row["last_name"];
		$v_email = $row["email"];
		$v_phone = $row["phone"];
		$v_loguser = $row["user_name"];
	}

        $dd_grps = '';
        if($_SESSION["FK_GROUPS"] == 1){
            //$rsgrp = mysql_query("select pk_groups, group_name from et_groups");
            $rsgrp = "select pk_groups, group_name from et_groups";
            $rsgrp = $db->query($rsgrp);
        }else{            
            //$rsgrp = mysql_query("(select pk_groups, group_name from et_groups where pk_groups =".$_SESSION["FK_GROUPS"].") UNION  (select pk_groups, group_name from et_groups where fk_parentgroup=".$_SESSION["FK_GROUPS"].")");
            $rsgrp = "(select pk_groups, group_name from et_groups where pk_groups =".$_SESSION["FK_GROUPS"].") UNION  (select pk_groups, group_name from et_groups where fk_parentgroup=".$_SESSION["FK_GROUPS"].")";
            $rsgrp = $db->query($rsgrp);
        }
        
        //if(mysql_num_rows($rsgrp)== 0){
        if($rsgrp->num_rows== 0){
            //$rsgrp = mysql_query("select pk_groups, group_name from et_groups where pk_groups=".$_SESSION["FK_GROUPS"]);
            $rsgrp = "select pk_groups, group_name from et_groups where pk_groups=".$_SESSION["FK_GROUPS"];
            $rsgrp = $db->query($rsgrp);
        }
	//while ($rsgrp_row = mysql_fetch_array($rsgrp)) {
        while ($rsgrp_row = $rsgrp->fetch_assoc()) {
            if ($rsgrp_row["group_name"] == $v_grpName) {
                $dd_grps .= "<option selected value=".$rsgrp_row["pk_groups"].">".$rsgrp_row["group_name"]."</option>";
            } else {
                $dd_grps .= "<option value=".$rsgrp_row["pk_groups"].">".$rsgrp_row["group_name"]."</option>";
            }
	}

	if ($v_mode == 'n'){
		echo '<div class="navigate">Manage Account - Users - New User</div>';
	} else {
		echo '<div class="navigate">Manage Account - Users - Edit</div>';
	}
	?>
		<form name="edituser" action="account_users_edit.php" method="POST" onSubmit="if (validate(document.edituser,'<?PHP echo $v_mode;?>') == false) return false;">
			<input type='hidden' name='mode' value=<?PHP echo $v_mode; ?>>
			<input type='hidden' name='pk_users' value=<?PHP echo $v_pk_users; ?>>
			<BR><table>

			<tr><td>First Name: </td><td><input type = "text" size="40" name="fname" value="<?PHP echo $v_fname; ?>"></td></tr>
			<tr><td>Last Name: </td><td><input type = "text" size="40" name="lname" value="<?PHP echo $v_lname; ?>"></td></tr>
			<tr><td>Email Address: </td><td><input type = "text" size="40" name="email" value="<?PHP echo $v_email; ?>"></td></tr>
			<tr><td>Phone: </td><td><input type = "text" size="40" name="phone" value="<?PHP echo $v_phone; ?>"></td></tr>
			<tr><td>Login Name: </td><td><input  <?PHP  if ($v_mode == 'm') echo "disabled"; else echo 'class="required"';?> type = "text" size="40" name="loguser" value="<?PHP echo $v_loguser; ?>"></td></tr>
			<tr><td>Select Group: </td><td><select class="required" name="group"><?PHP echo $dd_grps; ?></select></td></tr>
			<tr><td>Password: </td><td><input <?PHP echo (($v_mode == 'n') ? " class = 'required'" : "")?>type="password" name="pass1" size="20"/></td></tr>
			<tr><td>Re-enter Password: </td><td><input <?PHP echo (($v_mode == 'n') ? " class = 'required'" : "")?>type="password" name="pass2" size="20"/></td></tr>
			</table>
				<BR>
			<input type="image" src="./img/submit.png" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';">

			</form>
		
	<?PHP
} else {
	$v_pk_users = $_POST["pk_users"];
	$v_group = $_POST["group"];
	$v_pass = $_POST["pass1"];
	$v_mode = $_POST["mode"];
	
	$v_fname = $_POST["fname"];
	$v_lname = $_POST["lname"];
	$v_email = $_POST["email"];
	$v_phone = $_POST["phone"];
	if (isset($_POST["loguser"])) $v_loguser = $_POST["loguser"];

	
	if ($v_mode == 'n') {
                //$result = mysql_query("SELECT user_name from et_users where user_name ='$v_loguser'");
                $result = "SELECT user_name from et_users where user_name ='$v_loguser'";
                $result = $db->query($result);
		//if (mysql_num_rows($result) > 0) {
                if ($result->num_rows > 0) {
			echo "User already exists";
			echo '<meta http-equiv="refresh" content="3; url=./account_users.php">';
		} else {
                        //$rs_ins = mysql_query("INSERT INTO et_users (last_name,first_name,email,phone,user_name,password,fk_groups) VALUES ('".$_POST["lname"]."','".$_POST["fname"]."','".$_POST["email"]."','".$_POST["phone"]."','".$_POST["loguser"]."','".md5($v_pass)."',".$v_group.")");
                        $rs_ins = "INSERT INTO et_users (last_name,first_name,email,phone,user_name,password,fk_groups) VALUES ('".$_POST["lname"]."','".$_POST["fname"]."','".$_POST["email"]."','".$_POST["phone"]."','".$_POST["loguser"]."','".md5($v_pass)."',".$v_group.")";
			$rs_ins = $db->query($rs_ins);
                        /*--- AUDIT ---*/
			$rowInfo = array(1,$_SESSION["user"],$grpNameRs['group_name'] ,"SYSDATE", "SYSDATE", "I");
			auditQuery(1,$rowInfo);

			$colArray = array('LAST_NAME','FIRST_NAME','EMAIL','PHONE','USER_NAME','PASSWORD','FK_GROUPS');
			$colvalarray = array($_POST["lname"],$_POST["fname"],$_POST["email"],$_POST["phone"],$_POST["loguser"],md5($v_pass),$v_group);
			$tblname = 'et_users';
			colQueries($colArray, $colvalarray, $tblname, "","");
			/*--- END ---*/

			echo "Data Saved.";
			echo '<meta http-equiv="refresh" content="0; url=./account_users.php">';
		}
	} else {
                //$rs_ins = mysql_query ("UPDATE et_users set password = '".md5($v_pass)."' where pk_users = ".$v_pk_users);
		if (!empty($v_pass)){
                    $rs_ins = "UPDATE et_users set password = '".md5($v_pass)."' where pk_users = ".$v_pk_users;
                    $rs_ins = $db->query($rs_ins);
                }

		/*--- AUDIT ---*/
                //$oldValue = mysql_query("SELECT first_name, last_name, email, phone, fk_groups  FROM et_users WHERE pk_users=".$v_pk_users);
                //$oldValueRs = mysql_fetch_array($oldValue);
                $oldValue = "SELECT first_name, last_name, email, phone, fk_groups  FROM et_users WHERE pk_users=".$v_pk_users;
                $oldValue = $db->query($oldValue);
                $oldValueRs = $oldValue->fetch_assoc();
		/*--- END ---*/

                //$grpName = mysql_query("SELECT group_name FROM et_groups WHERE pk_groups=".$v_group);
		//$grpNameRs = mysql_fetch_array($grpName);
                $grpName = "SELECT group_name FROM et_groups WHERE pk_groups=".$v_group;
                $grpName = $db->query($grpName);
                $grpNameRs = $grpName->fetch_assoc();
                
                $namArr = array();
                $valArr = array();
                $oldValueRsArr = array();
                $cnt = 0;
                
                if($oldValueRs['first_name'] != $v_fname){
                    $namArr[$cnt] = 'first_name';
                    $valArr[$cnt] = $v_fname;
                    $oldValueRsArr[$cnt] = $oldValueRs['first_name'];
                    $cnt++;
                }
                if($oldValueRs['last_name'] != $v_lname){
                    $namArr[$cnt] = 'last_name';
                    $valArr[$cnt] = $v_lname;
                    $oldValueRsArr[$cnt] = $oldValueRs['last_name'];
                    $cnt++;
                }                
                if($oldValueRs['email'] != $v_email){
                    $namArr[$cnt] = 'email';
                    $valArr[$cnt] = $v_email;
                    $oldValueRsArr[$cnt] = $oldValueRs['email'];
                    $cnt++;
                }
                if($oldValueRs['phone'] != $v_phone){
                    $namArr[$cnt] = 'phone';
                    $valArr[$cnt] = $v_phone;
                    $oldValueRsArr[$cnt] = $oldValueRs['phone'];
                    $cnt++;
                }
                if($oldValueRs['fk_groups'] != $v_group){
                    $namArr[$cnt] = 'fk_groups';
                    $valArr[$cnt] = $v_group;
                    $oldValueRsArr[$cnt] = $oldValueRs['fk_groups'];
                    $cnt++;
                }
                
                //$rs = mysql_query("UPDATE et_users set first_name = '".$v_fname."',last_name = '".$v_lname."',email= '".$v_email."',phone = '".$v_phone."', fk_groups = ".$v_group." where pk_users = ".$v_pk_users);                
                $rs = "UPDATE et_users set first_name = '".$v_fname."',last_name = '".$v_lname."',email= '".$v_email."',phone = '".$v_phone."', fk_groups = ".$v_group." where pk_users = ".$v_pk_users;
                $rs = $db->query($rs);


		/*--- AUDIT ---*/
		$rowInfo = array(1,$_SESSION["user"], $grpNameRs['group_name'], "SYSDATE", "SYSDATE", "U");
		auditQuery(1,$rowInfo);

		$colArray = $namArr;
		$colvalarray = $valArr;                
		$tblname = 'et_users';
                colQueries($colArray, $colvalarray, $tblname, $oldValueRsArr,'noclob');
		/*--- END ---*/

		echo "Data Saved.";
		echo '<meta http-equiv="refresh" content="0; url=./account_users.php">';
	}

}


?>
</div>

</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
