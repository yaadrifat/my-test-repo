<?PHP

function getDisplacement($visit){
    //echo '<br>$visit: '.$visit.'<br>';
	$visit = strtoupper($visit).'|';
	$visit = str_replace('M','|M',$visit);
	$visit = str_replace('W','|W',$visit);
	$visit = str_replace('D','|D',$visit);
  
	$v_visitSplit = explode('|',$visit);
	$num_months = 0;
	$num_weeks = 0;
	$num_days = 0;
	$displacement = 0;

        
	for ($i=0;$i<count($v_visitSplit);$i++) {
		$v_test = substr($v_visitSplit[$i],0,1);
		switch ($v_test) {
		case "M":
			$num_months = substr($v_visitSplit[$i],1);
			$num_months = ($num_months > 0?($num_months ):0);
			break;
		case "W":
			$num_weeks = substr($v_visitSplit[$i],1);
			$num_weeks = ($num_weeks > 0?($num_weeks ):0);
			break;
		case "D":
			$num_days = substr($v_visitSplit[$i],1);
			break;
		}
	}
        
	//implemented this for the bug 6640
	if ($num_days==0){
		$pos = strpos($visit,"D");
		if($pos===false){
			$num_days=1;
		}
	}

	$displacement = ($num_months > 0?(($num_months - 1) * 30):0) + ($num_weeks > 0?(($num_weeks - 1)* 7):0) + ($num_days == 0?1:$num_days);
	
/*
	if ($week > 0) {
		$num_months  = (($month > 0 ) ? substr($visit,$month+1,$week-$month-1) : 0);
		$displacement = $displacement + (($month > 0 ) ? (substr($visit,$month+1,$week-$month-1) - 1)*30 : 0);
	} else {
		$num_months  = (($month > 0 ) ? substr($visit,$month+1) : 0);
		$displacement = $displacement + (($month > 0 ) ? (substr($visit,$month+1) - 1)*30 : 0);
	}
	
	if ($day > 0) {
		$num_weeks = (($week > 0) ? substr($visit,$week+1,$day-$week-1) : 0);
		$displacement = $displacement  + (($week > 0) ? (substr($visit,$week+1,$day-$week-1)-1)*7 : 0);
	} else {
		$num_weeks = (($week > 0) ? substr($visit,$week+1) : 0);
		$displacement = $displacement  + (($week > 0) ? (substr($visit,$week+1)-1)*7 : 0);
	}
	
	$num_days = (($day > 0) ? substr($visit,$day+1) : 0);
	$displacement = $displacement  + (($day > 0) ? (substr($visit,$day+1)) : 1);
	
*/
	
//echo $visit.'|Months:'.$num_months.'|Weeks:'.$num_weeks.'|Days:'.$num_days.'|Displacement:'.$displacement."<BR>";
	//return $num_months.'|'.$num_weeks.'|'.$num_days.'|'.$displacement;
        return $displacement;

}

//--------- fix for the impact issue of 27372
function getDisplacement_monweeday($visit){
    //echo '<br>$visit: '.$visit.'<br>';
	$visit = strtoupper($visit).'|';
	$visit = str_replace('M','|M',$visit);
	$visit = str_replace('W','|W',$visit);
	$visit = str_replace('D','|D',$visit);
  
	$v_visitSplit = explode('|',$visit);
	$num_months = 0;
	$num_weeks = 0;
	$num_days = 0;
	$displacement = 0;

        
	for ($i=0;$i<count($v_visitSplit);$i++) {
		$v_test = substr($v_visitSplit[$i],0,1);
		switch ($v_test) {
		case "M":
			$num_months = substr($v_visitSplit[$i],1);
			$num_months = ($num_months > 0?($num_months ):0);
			break;
		case "W":
			$num_weeks = substr($v_visitSplit[$i],1);
			$num_weeks = ($num_weeks > 0?($num_weeks ):0);
			break;
		case "D":
			$num_days = substr($v_visitSplit[$i],1);
			break;
		}
	}
	if ($num_days==0){
		$pos = strpos($visit,"D");
		if($pos===false){
			$num_days=1;
		}
	}

	$displacement = ($num_months > 0?(($num_months - 1) * 30):0) + ($num_weeks > 0?(($num_weeks - 1)* 7):0) + ($num_days == 0?1:$num_days);
//echo $visit.'|Months:'.$num_months.'|Weeks:'.$num_weeks.'|Days:'.$num_days.'|Displacement:'.$displacement."<BR>";
	return $num_months.'|'.$num_weeks.'|'.$num_days.'|'.$displacement;
        //return $displacement;

}


function getDisplacementValue($visit){
    //echo 'getDisplacementValue: '.$visit.'<br>';    
    $modflag = '';
        switch(substr($visit,0,1))
        {
            case 'F':
                if(substr_count($visit,'|')==1){
                    $visitList = explode('|',$visit);
                    $visit = $visitList[1];                    
                }else{
                    $visitList = explode('|',$visit);
                    $visit = $visitList[2];
                }
                $modflag = 'F';                
                break;
            
            case 'D':
                $visitList = explode('|',$visit);
                //$visit = $visitList[1].$visitList[2];
                $visit = $visitList[2].$visitList[1];                
                //$visit = $visitList[2];
                //echo '$visit: '.$visit.'<br>';
                $modflag = 'D';                
                break;
            
            case 'N':
                $visitList = explode('|',$visit);
                $visit = $visitList[1];
                $modflag = 'N';                
                break;
        }        
        //echo '$visit: '.$visit.'<br>';    
	$visit = strtoupper($visit).'|';
	$visit = str_replace('M','|M',$visit);
	$visit = str_replace('W','|W',$visit);
	$visit = str_replace('D','|D',$visit);
	$v_visitSplit = explode('|',$visit);
        $v_visitSplit = array_filter($v_visitSplit);
        $v_visitSplit = array_values($v_visitSplit);
        $num_months = 0;
	$num_weeks = 0;
	$num_days = 0;
	$displacement = 0;        
        if($modflag!='D'){            
            for ($i=0;$i<count($v_visitSplit);$i++) {
                    $v_test = substr($v_visitSplit[$i],0,1);
                    switch ($v_test) {
                        case "M":
                            $num_months = substr($v_visitSplit[$i],1);
                            $num_months = ($num_months > 0?($num_months):0);
                            break;
                        case "W":
                            $num_weeks = substr($v_visitSplit[$i],1);
                            $num_weeks = ($num_weeks > 0?($num_weeks):0);
                            break;
                        case "D":
                            $num_days = substr($v_visitSplit[$i],1);
//                            if($v_visitSplit[$i] == 'D100'){
//                                echo '$num_days****: '.$num_days.'<br>';
//                            }                            
                            break;
                    }
            }
        }else{
            $num_months = 0;
            $num_weeks = 0;
            $num_days = 0;            
            $num_months_d = 0;
            $num_weeks_d = 0;
            $num_days_d = 0;

            $v_visitSplit = array_reverse($v_visitSplit);
            for ($i=0;$i<count($v_visitSplit);$i++) {
                //echo '$i: '.$i.'<br>';
                $v_test = substr($v_visitSplit[$i],0,1);                
                //echo '$v_test: '.$v_test.'<br>';
                
                if($i==0){
                    if($v_test == 'M'){
                        $num_months_d = substr($v_visitSplit[$i],1);                        
                    }else if($v_test == 'W'){                        
                        $num_weeks_d = substr($v_visitSplit[$i],1);                        
                    }else if($v_test == 'D'){
                        $num_days_d = substr($v_visitSplit[$i],1);
                    }
                }else if($i==2 &&  $v_test == 'D'){
                    $num_days = substr($v_visitSplit[$i],1);                    
                }else if($i > 2){
                    if($i-1 > 1){    
                        if($v_test == 'M'){
                            //--------for the fix 26310
                            //$num_months = substr($v_visitSplit[$i],1)-1;
                            $num_months = substr($v_visitSplit[$i],1)-2;
                        }else if($v_test == 'W'){                       
                            //echo substr($v_visitSplit[$i],1);
                            //$num_weeks = substr($v_visitSplit[$i],1)-1;  
                            $num_weeks = substr($v_visitSplit[$i],1)-2;                            
                        }                        
                    }
                }
                
                switch($v_test){
                    case 'M':
                        $num_months = $num_months_d+$num_months;
                        break;
                    
                    case 'W':
                        $num_weeks = $num_weeks_d+$num_weeks;
                        break;
                    
                    case 'D':
                        $num_days = $num_days_d+$num_days;
                        break;
                }
            }            
        }
        
        if($modflag == 'D'){            
            if($num_months>0){
                $num_months = $num_months * 30;
            }
            if($num_weeks>0){
                $num_weeks = $num_weeks * 7;
            }
            if($num_days == 0){
                $num_days = 0;
                //echo '$num_days ==0: '.$num_days.'<br>';
            }else{
                $num_days = $num_days;
                //echo '$num_days !=0: '.$num_days.'<br>';
            }
            $displacement = $num_months + $num_weeks + $num_days;            
            //$displacement = ($num_months > 0?(($num_months) * 30):0) + ($num_weeks > 0?(($num_weeks)* 7):0) + ($num_days == 0?1:$num_days);
        }else{

            $displacement = ($num_months > 0?(($num_months - 1) * 30):0) + ($num_weeks > 0?(($num_weeks - 1)* 7):0) + ($num_days == 0?1:$num_days);
        }
        return $displacement;
        
}

function displacementValidate($visit){
/*

	list($v_visit,$v_visit_name) = explode('|',$visit);
	$visit='XYZ'.strtoupper($v_visit);
	$month = strpos($visit,'M');
	$week = strpos($visit,'W');
	$day = strpos($visit,'D');

	if ($week > 0) {
		$num_months  = (($month > 0 ) ? substr($visit,$month+1,$week-$month-1) : 0);
	} else {
		$num_months  = (($month > 0 ) ? substr($visit,$month+1) : 0);
	}
	
	if ($day > 0) {
		$num_weeks = (($week > 0) ? substr($visit,$week+1,$day-$week-1) : 0);
	} else {
		$num_weeks = (($week > 0) ? substr($visit,$week+1) : 0);
	}
	
	$num_days = (($day > 0) ? substr($visit,$day+1) : 0);

*/
	$visit = strtoupper($visit).'|';
	$visit = str_replace('M','|M',$visit);
	$visit = str_replace('W','|W',$visit);
	$visit = str_replace('D','|D',$visit);

	$v_visitSplit = explode('|',$visit);
	$num_months = 0;
	$num_weeks = 0;
	$num_days = 0;
	$displacement = 0;

	for ($i=0;$i<count($v_visitSplit);$i++) {
		$v_test = substr($v_visitSplit[$i],0,1);
		switch ($v_test) {
		case "M":
			$num_months = substr($v_visitSplit[$i],1);
			$num_months = ($num_months > 0?$num_months:0);
			break;
		case "W":
			$num_weeks = substr($v_visitSplit[$i],1);
			$num_weeks = ($num_weeks > 0?$num_weeks:0);
			break;
		case "D":
			$num_days = substr($v_visitSplit[$i],1);
			break;
		}
	}

	$v_retval = 0;
	if (($num_months > 0) && ($num_weeks > 4)) $v_retval = 1;
	if ($num_weeks > 0 && $num_days > 7) $v_retval = 1;
	return $v_retval;
}

?>
