<?php
	/*------ Author: Nicholas L -----------*/	
session_start();
header("Cache-control: private");

if(@$_SESSION["user"]){
?>

<html>
<head>
<title>Velos eTools -&gt; Audit Trail</title>

<?php
include("db_config.php");
include("./includes/header.php");
?>
<script src="./js/jquery-1.10.2.js"></script>
<script src="./js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">	
<script>
  $(document).ready(function(){
     
        $("#datepicker").datepicker({dateFormat: 'mm/dd/yy'});
	$("#datepicker1").datepicker({dateFormat: 'mm/dd/yy'});
	$('.opt').hide();	
        $('.downLink').hide();

        $(".sbtbutton").click(function(e){        
            var formData = {
             'auditModule' : $('select[name=auditModule] option:selected').val(),
                'optModule'   : $('select[name=optModule] option:selected').val(),
                'datefrom'    : $('input[name=datefrom]').val(),
                'dateto'      : $('input[name=dateto]').val()
              };	
             $.ajax({ url: "audit_function.php",
                type : 'post',
                data : formData,
                success:function(result){
                    if(result == 1){
                        $("#resultCont").html('<b class="norecordmsg">No Records Found !</b>');                        
                        $('.downLink').fadeOut( "fast");
                    }else if(result == 2){
                        $("#resultCont").html('<b class="alertmsg">Please select search criteria !</b>');                        
                        $('.downLink').fadeOut( "fast");
                    }else{
                        if($('select[name=auditModule] option:selected').val() == 5){
                            $("#resultCont").css("overflow-x","scroll");
                            $("#resultCont").css("height","500px"); 
                        }else{
                            $("#resultCont").css("overflow-x","hidden");
                            $("#resultCont").css("height","auto"); 
                        }
                        $("#resultCont").html(result);
                        $('.downLink').fadeIn( "fast");
                    }
             }});
             e.preventDefault();
    });
	
	
    $('.selModule').on('change', function (e) {		
        if($('.selModule').val() == 3){
                $('.opt').show();
        }else{
                $('.opt').hide();
        }
    });
	
    $('#csvexport').click(function(){
        $cval = $('.cval').val();
        $mod = $('.selModule').val();
        $df = $('#datepicker').val();
        $dt = $('#datepicker1').val();
        $opt = $('.optval').val();
        if($cval == 'clob'){
            window.open("audit_csvexport.php?mod="+$mod+'&df='+$df+'&dt='+$dt+'&opt='+$opt+' &cval='+$cval,'_self');
        }else{            
            window.open("audit_csvexport.php?mod="+$mod+'&df='+$df+'&dt='+$dt+'&opt='+$opt,'_self');
        }        
    });
  }); 
</script>
</head>
<body>
<div id="fedora-content">
<?php 		
	//$m_sql = mysql_query("SELECT PK_AUDITMODULE, MODULE FROM et_audit_module");
        $m_sql = "SELECT PK_AUDITMODULE, MODULE FROM et_audit_module";
        $m_sql = $db->query($m_sql);
	$amodule = "";
	$amodule.= "<option value = '0'>Select Module</option>";
        //while($results = mysql_fetch_array($m_sql)){
        while($results = $m_sql->fetch_assoc()){
            $amodule .= "<option value=".$results["PK_AUDITMODULE"].">".$results["MODULE"]."</option>";
        }
	$content = "";
?>
	<div class="navigate">Audit Report</div>	
	<br>
	<form name="auditfrm" id="auditfrm" method="post">	
            <table border = "0">
            <tr>
                <td>Select module & Date range: </td><td><select name="auditModule" class="selModule"><?PHP echo $amodule;?></select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td class="opt">Select the option:<select name="optModule" class="optval">					
                        <option value="optLo">Login Details</option>
                        <option value="optDa">Datasource Details</option>
                        <option value="optPa">Page accessed Details</option>
                        <!--<option value="optAl">All</option>-->
                    </select></td>
                <td>Date from: </td><td><input type="text" name="datefrom" id="datepicker" size="10">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>Date to: </td><td><input type="text" name="dateto" id="datepicker1" size="10">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>			
                <td><input  type="image" class="sbtbutton" name="submit" value="Submit" src="./img/search.png"  align="absmiddle" border="0" onmouseover="this.src='./img/search_m.png';" onmouseout="this.src='./img/search.png';" /></input></td>
            </tr>		
            </table>		
	</form>
        <table width="100%" border="0" class="downLink"><tr><td style="text-align:right;"><a href="#" id="csvexport">CSV download</a></td></tr><tr><td>&nbsp;</td></tr></table>
	<div id="resultCont"></div>
</div>
</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
