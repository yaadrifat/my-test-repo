<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Query Database</title>
</head>
<?php
include("db_config.php");
include("./includes/header.php");

include_once "./adodb/adodb.inc.php";
include_once('./adodb/adodb-pager.inc.php'); 
include_once('./adodb/pivottable.inc.php'); 

$db = NewADOConnection("oci8");
$db->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);


if (!$db) die("Connection failed");

	
?>
<body>

<div id="fedora-content">	
<div class="navigate">Create Query from View - Select Columns</div>
<br>
<?php

if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
	
	$v_table = $_GET["table"];
	echo "<b>Selected View Name: ".$v_table."</b>";
	echo "<BR>";
	$v_columns = $db->MetaColumnNames($v_table);
	echo "<BR>";
	echo "<b><i>Select Columns:</i></b>";
	echo "<BR>";
	echo "<FORM action=query_adodb_viewcols.php method=post>";
	echo "<INPUT type=hidden name=tablename value=".$v_table."></INPUT>";
	echo "<TABLE width=100%>";
	$counter = 0;
	foreach($v_columns as $key => $value){
		if ($counter==0) echo "<TR>";
		echo "<TD width=25%>";
		echo '<INPUT TYPE="CHECKBOX" name="columns[]" value="'.$value.'">'.$value;
		echo "</TD>";
		$counter++;
		if ($counter==4) $counter = 0;
		if ($counter==0) echo "</TR>";
	}
	echo '<tr><td>&nbsp;</td></tr><tr><td><input type="submit" name="submit" value="Submit"></input></td></tr>';
	echo "</TABLE>";
	echo "</FORM>";

} else {
	echo "Query created...";
	$v_columns = $_POST["columns"];
	$v_table = $_POST["tablename"];
	$v_sql = "";
	foreach($v_columns as $col){
		$v_sql .= $col.",".chr(13);
	}
	$v_sql = "SELECT ".substr($v_sql,0,strlen($v_sql)-2).chr(13)."FROM ".$v_table;
	$_SESSION["query"] = $v_sql;

	$url = "./query_adodb.php";
	echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";
}
?>


</div>


</body>
</html>
<?php 
$db->Close();

}
else header("location: index.php?fail=1");
?>
