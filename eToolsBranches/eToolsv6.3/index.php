<?php
session_start();
header("Cache-control: private");
if(@$_SESSION['user']) header("location: login.php");
else{
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html"; charset="utf-8">
    <meta name="MSSmartTagsPreventParsing" content="TRUE">
    <link rel="stylesheet" type="text/css" media="print" href="css/print.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/layout.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/content.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/docbook.css">
<?PHP
    $file = file("register.php") or die("Authorization required!");
    $totalLines = sizeof($file);
    $line = 0;
    $match = 0;
    do{
        if("//" != substr($file[$line], 0, 2)){
            @list($ip_address, $user_name) = explode("|", $file[$line]);
            if($ip_address == $_SERVER["REMOTE_ADDR"]) $match = 1;
            else $match = 0;
        }
        if($match) break;
        $line++;
    } while($line < $totalLines);
?>
<title>Velos eTools</title>
<script src="js/jquery-1.10.2.js"></script>
<script>    
    $(function() {        
        $('.midBar .notificationBar').html('');        
        $('.loginErrmsg').html('');
        //---------------------Notification--------------------
        // First Character should be 'E' - Enable, 'D'-disable        
        var notmsg = "D eTools is not available from 8/11 9p.m to 8/15 9a.m due to annual system maintenance. For inquiry, please contact your account administrator or call the Velos customer service hotline.";                
        //-----------------------------------------------------
        if(notmsg.substring(0,1) == 'E'){        
            $('.midBar .notificationBar').html(notmsg.substring(1,notmsg.length));
        }else{            
            $('.midBar .notificationBar').html('');
        }
        
        var path = window.location.href;
        
        if(path.substring(path.length-6,path.length) == 'fail=1'){
           $('.loginErrmsg').html('You entered an invalid username or password. Please try again or contact your Account Administrator.');           
        }else{
            $('.loginErrmsg').html('');
        }
        
        //alert('width: '+screen.width+'  '+'height: '+screen.height);

            
       
        //for the bug fix #27520
        //if(screen.width >= 1280 && screen.height >= 800){            
        if(screen.width == 1280 && screen.height == 1024){            
            $('.parentDiv .midBar').css('min-height','716px');
            $('.parentDiv .midBox3').css({'min-height':'0px','padding-top':'198px'});
            $('.parentDiv .midBox1').css({'min-height':'0px','padding-top':'120px'});
            $('.parentDiv .midBox1 img').css('padding-bottom','35px');
        }
        //if(screen.width >= 1360 && screen.height >= 768){            
        if(screen.width == 1360 && screen.height == 768){            
            $('.parentDiv .midBar').css('min-height','536px');
            $('.parentDiv .midBox3').css({'min-height':'0px','padding-top':'72px'});
            $('.parentDiv .midBox1').css({'min-height':'0px','padding-top':'30px'});
            $('.parentDiv .midBox1 img').css('padding-bottom','25px');            
        }
        if(screen.width > 1361 && screen.height > 769){            
            $('.parentDiv .midBar').css('min-height','652px');
            $('.parentDiv .midBox3').css({'min-height':'0px','padding-top':'72px'});
            $('.parentDiv .midBox1').css({'min-height':'0px','padding-top':'30px'});
            $('.parentDiv .midBox1 img').css('padding-bottom','25px');            
        }        
/*        if(screen.width >= 1600 && screen.height >= 900){
            $('.parentDiv .midBar').css('min-height','651px');
            $('.parentDiv .midBox3').css({'min-height':'0px','padding-top':'142px'});
            $('.parentDiv .midBox1').css({'min-height':'0px','padding-top':'72px'});
            $('.parentDiv .midBox1 img').css('padding-bottom','35px');
        }*/
    });
</script>
</head>

<body onload = "document.getElementById('user').focus();" style="background-color: white;">
<?php 
include("db_config.php");


///************** Input eTools version number, Build number & eRes version here to validate correct eTools version **************///

            $cur_eT_version_no = '6.3';
            $cur_eT_build_no = '173';
            $cur_eRes_version_no = 'eResearch 10';            
            
///******************************************************************************************************************************///
            $key = 'bRuD5WYw5wd0rdHR9yLlM6wt2vteuiniQBqE70nAuhU=';
            $_SESSION['fnkey'] = $key;
include("gen_functions.php");            
$ver_result = "SELECT * from et_version";
$ver_result = $db->query($ver_result);

while($row = $ver_result->fetch_assoc()){
    $version_num = $row["et_version_num"];
    $build_num = $row["et_build_num"];
    $eres_ver = $row["eres_ver"];    
    $pk_et = $row["pk_et"];
}

    //$pk_et_pr = $pk_et-1;
    //$auto_result = mysqli_query("SELECT * from et_version where et_build_num = 'dsenc170' and et_version_num = '6.1'");
    //  $auto_result = mysqli_fetch_array($auto_result);

    $auto_result = "SELECT * from et_version where et_build_num = 'dsenc170' and et_version_num = '6.1'";
    $auto_result = $db->query($auto_result);
    $auto_result = $auto_result->fetch_assoc();
    

            
?>   
    <div class="parentDiv">
        <div class="topBar"></div>
        <div class="midBar">            
            <div class="notificationBar" ></div>
            <div>
                <div class="midBox1">
                    <img src="img/velos_logo.jpg" width="220" height="70" /><br>
                    <img src="img/client_logo01.jpg" width="220" height="70" /><br>
                    <img src="img/client_logo02.jpg" width="220" height="70" /><br>
                    <img src="img/client_logo03.jpg" width="220" height="70" /><br>
                </div>
                <div class="midBox2"><img src="img/login_divder.jpg" width="1" height="450" /></div>
                <?php
                    if($cur_eT_version_no == $version_num && $cur_eT_build_no == $build_num && $cur_eRes_version_no == $eres_ver){
                        if($auto_result['et_build_num'] == 'dsenc170'){                        
                            //$olddataqry = mysqli_query("select * from et_ds");
                            $olddataqry = "select * from et_ds";
                            $olddataqry = $db->query($olddataqry);
                            //$olddatacnt = mysqli_num_rows($olddataqry);
                            //$olddatacnt = $olddataqry->num_rows;
							$olddatacnt = $olddataqry->fetch_assoc();	
                            
                            $olddataqry = "select * from et_ds";
                            $olddataqry = $db->query($olddataqry);
							$olddatacnt = $olddataqry->fetch_assoc();	
                            //$olddatacnt = $olddataqry->num_rows;
                                   
                            $newdatacnt = 0;
                            while($olddata = $olddataqry->fetch_assoc()){
                            //while($olddata = mysqli_fetch_array($olddataqry)) {
                                $fnolddata = trim($olddata['ds_pass']);
                                //$fnolddata = trim($olddata[6]);
                                if(strlen($fnolddata)<25){
                                    //$key = 'bRuD5WYw5wd0rdHR9yLlM6wt2vteuiniQBqE70nAuhU=';
                                    $password_encrypted = my_encrypt($fnolddata, $_SESSION['fnkey']);
                                    //$password_dencrypted = my_decrypt($password_encrypted, $key);
                                    
                                    //echo '$password_encrypted: '.$password_encrypted.' - '.'$password_dencrypted: '.$password_dencrypted.'<br>';
                                    //$newdataQry = mysqli_query("Update et_ds set ds_pass='".$password_encrypted."' where pk_ds=".$olddata['pk_ds']);
                                    $newdataQry = "Update et_ds set ds_pass='".$password_encrypted."' where pk_ds=".$olddata['pk_ds'];
                                    $newdataQry = $db->query($newdataQry);
                                }
                                $newdatacnt++;
                            }
                            //exit;
                            //mysqli_query("Update et_version set et_build_num='dsenc170-e' where et_build_num='dsenc170'");
                            $updateqry = "Update et_version set et_build_num='dsenc170-e' where et_build_num='dsenc170'";
                            $updateqry = $db->query($updateqry);
                        }                        
                        if($newdatacnt != $olddatacnt){
                            echo 'There is a problem in your password encryption on et_ds. Contact your IT team.';
                        }
                ?>
                <form method = "post" action = "login.php">
                    <div class="midBox3">
                        <div class="elementbottommargin"><img src="img/etools_logo.jpg" width="120" height="40" /></div>
                        <div class="elementbottommargininput">
                            <input type = "text" id = "user" name = "user" class = "text" tabindex = "1" width="260" placeholder="User name">
                            <img src="img/loginusricon.jpg" id="input_img1">
                            <br>                           
                            <input type = "password" name = "pass" class = "text" tabindex = "2"  width="260" placeholder="Password">
                            <img src="img/loginpassicon.jpg" id="input_img2">
                        </div>
                        <div class="loginErrmsg"></div>
                        <div class="inputbtn"><input type = "image" src = "./img/loginbutton.jpg" width  = "260" height = "20" name = "submit" alt = "login" tabindex = "3"></div>
                        <div><a href="mailto:customersupport@veloseresearch.com">Forgot your login?</a></div>
                    </div>
                </form>
                <?php
                    }else{
                        echo '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';
                        echo '<table border = "1" cellspacing = "0" cellpadding = "0" width = "500" height="50" align="center" valign = "top" style="color:red;"><tr><td align="center" style="color:red;">eTools  or eResearch versions are mismatching..!</td></tr></table>';            
                    }
                ?>
            </div>
        </div>
        <div class="botBar">
            <div class="botBar1"><?php echo 'V'.$version_num.'  #'.$build_num.'  - for '.$eres_ver ?>   | Copyright 2016 Velos Inc. All Rights Reserved.</div>
            <div class="botBar2">Browser: Internet Explorer 11 and Firefox 39 and above | Screen Resolutions: 1280 X 1024 and 1360 X 768</div>
        </div>
    </div>
</body>
</html>
<?php
}
?>