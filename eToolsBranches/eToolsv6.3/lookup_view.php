<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Query Database</title>
</head>
<?php
include("db_config.php");
include("./includes/header.php");
include_once "./adodb/adodb.inc.php";

$db = NewADOConnection("oci8");
//$connect_string = '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST='.$_SESSION["DS_HOST"].')(PORT='.$_SESSION["DS_PORT"].'))(CONNECT_DATA=(SID='.$_SESSION["DS_SID"].')))';
$connect_string = '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST='.$_SESSION["DS_HOST"].')(PORT='.$_SESSION["DS_PORT"].'))(CONNECT_DATA=(SERVICE_NAME='.$_SESSION["DS_SID"].')))';
$db->Connect($connect_string, "eres", $_SESSION["DS_PASS"]);


if (!$db) die("Connection failed");

	
?>
<body>
<div id="fedora-content">	
<style>
	.gra{ color:#CCC;}
	.col{ color:#090; font-weight:bold;}
</style>
<?PHP 
$v_records_per_page = 50;
$v_pk_lkplib = $_GET["pk_lkplib"];
$v_table = $_GET["table"];

if (isset($_GET["page"])) {
	$v_page = $_GET["page"];
} else {
	$v_page = 1;
}

if ($v_table == 'ER_LKPDATA') {
	$v_sql = "select count(*) as count from er_lkpdata where fk_lkplib = ".$v_pk_lkplib;
} else {
	$v_sql = "select count(*) as count from ".$v_table;
}


$rs = $db->Execute($v_sql);
echo "Total Rows: ".$rs->fields["COUNT"]."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
$totalRc = $rs->fields["COUNT"];

$v_query = "select lkpcol_name,lkpcol_dispval from er_lkpcol where fk_lkplib = ".$v_pk_lkplib;
$rs = $db->Execute($v_query);
$v_sql = "";
$v_cols = "";

$rcCount = $totalRc;
$rcPerpg = $v_records_per_page;
$noPage = round($rcCount/$rcPerpg);

if($noPage==0) echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="gra"><<</span> &nbsp;&nbsp;  <span class="gra"><</span> &nbsp;&nbsp;  <span class="gra">></span> &nbsp;&nbsp;  <span class="gra">>></span>';

if($noPage>1 && $v_page == 1) echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="gra"><<</span> &nbsp;&nbsp; <span class="gra"><</span> &nbsp;&nbsp; <a class="col" href="lookup_view.php?pk_lkplib='.$v_pk_lkplib.'&page='.($v_page+1).'&table='.$v_table.'">></a> &nbsp;&nbsp; <a class="col" href="lookup_view.php?pk_lkplib='.$v_pk_lkplib.'&page='.($noPage).'&table='.$v_table.'">>></a>';
if($noPage>1 && $v_page > 1 &&  $v_page != $noPage) echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a class="col" href="lookup_view.php?pk_lkplib='.$v_pk_lkplib.'&page=1&table='.$v_table.'"><<</a> &nbsp;&nbsp; <a class="col" href="lookup_view.php?pk_lkplib='.$v_pk_lkplib.'&page='.($v_page-1).'&table='.$v_table.'"><</a> &nbsp;&nbsp; <a class="col" href="lookup_view.php?pk_lkplib='.$v_pk_lkplib.'&page='.($v_page+1).'&table='.$v_table.'">></a> &nbsp;&nbsp; <a class="col" href="lookup_view.php?pk_lkplib='.$v_pk_lkplib.'&page='.($noPage).'&table='.$v_table.'">>></a>';

if($noPage>1 && $v_page == $noPage) echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a class="col" href="lookup_view.php?pk_lkplib='.$v_pk_lkplib.'&page=1&table='.$v_table.'"><<</a> &nbsp;&nbsp; <a class="col" href="lookup_view.php?pk_lkplib='.$v_pk_lkplib.'&page='.($v_page-1).'&table='.$v_table.'"><</a> &nbsp;&nbsp; <span class="gra">></span> &nbsp;&nbsp; <span class="gra">>></span>';


if($noPage==0){
	$noPage = 1;
}
echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#666666">Pages displayed</font> :&nbsp;&nbsp; '.$v_page.'/'.$noPage;


echo '<TABLE width="100%" border="1">';
echo '<TR>';
while (!$rs->EOF) {
	$v_sql .= $rs->fields["LKPCOL_NAME"].",";
	echo "<TH>".$rs->fields["LKPCOL_DISPVAL"]."</TH>";
	$rs->MoveNext();
}
echo '</TR>';
$row_start = 0;
$v_page=$v_page-1;
if ($v_page > 0) {
	$row_start = $v_page * $v_records_per_page;
}

if ($v_table == 'ER_LKPDATA') {
	$v_sql = "select ".$v_sql."rownum as rown from er_lkpdata where fk_lkplib = ".$v_pk_lkplib;
} else {
	$v_sql = "select ".$v_sql."rownum as rown from $v_table";
}

$v_sql = "select * from (".$v_sql.") where rown > ".$row_start." and rown <= ".($row_start+$v_records_per_page);

$rs = $db->Execute($v_sql);
if (!$rs) 
	print $db->ErrorMsg();
else {
	$field_count = $rs->FieldCount()-1;
	while (!$rs->EOF) {
		echo '<TR onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';">';
		for ($i=0;$i<$field_count;$i++){
			echo "<TD>".$rs->fields[$i]."</TD>";
		}
		echo "</TR>";
		$rs->MoveNext();
	}
}
echo '</TABLE>';
?>


</TABLE>
</div>


</body>
</html>
<?php 
$db->Close();
}
else header("location: index.php?fail=1");
?>
