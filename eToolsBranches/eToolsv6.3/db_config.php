<?php
    $servername = "";
    $username = "";
    $password = "";
    $et_database = "";

    $db = new mysqli($servername, $username, $password, $et_database);    

    if($db->connect_errno > 0){
        die('Unable to connect to database [' . $db->connect_error . ']');
    }
    $_SESSION['et_db'] = $et_database;
    $selected = mysqli_select_db($db,$et_database);
?>