<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
<link type="text/css" rel="stylesheet" href="./styles/common.css">
    <title>Velos eTools -> Report Preview</title>

</head>


<body>

<?php
include_once "./adodb/adodb.inc.php";
$db = NewADOConnection("oci8");
$db->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);



if (!($_SERVER['REQUEST_METHOD'] == 'POST')) {
	$v_pk_report = $_GET["reportPk"];
?>
<div id="fedora-content">	
<div class="navigate">Report Preview</div>
<BR>
<form name="rpreview" method="post" action="report_preview.php">
Available filters for this report:<BR>
<table width="100%">

<tr><td>From Date</td><td><input type="text" name="filter[0]" size="10"><i> (mm/dd/yyyy)</i></td></tr>
<tr><td>To Date</td><td><input type="text" name="filter[1]" size="10"><i> (mm/dd/yyyy)</i></td></tr>
<input type="hidden" name="filtername[0]" value="fromDate">
<input type="hidden" name="filtername[1]" value="toDate">
<?PHP
	$v_query = "select rep_filterapplicable from er_report where pk_report = ".$v_pk_report;
	$rs = $db->Execute($v_query);
	$v_filter = $rs->fields["REP_FILTERAPPLICABLE"];
	$v_filter = str_replace(':',"','",$v_filter);
	
	$v_query = "select repfilter_coldispname, repfilter_keyword from er_repfilter where repfilter_keyword in ('$v_filter')";
	$rs = $db->Execute($v_query);
	$v_sql = "";
	$i = 2;
	while (!$rs->EOF) {
		echo '<tr><td>'.$rs->fields["REPFILTER_COLDISPNAME"].'</td><td><input type="text" name="filter['.$i.']" size="50"><i> (primary key)</i></td></tr>';
		echo '<input type="hidden" name="filtername['.$i.']" value="'.$rs->fields["REPFILTER_KEYWORD"].'">';
		$i++;
		$rs->MoveNext();
	}

?>
</table>
	<input type="hidden" name="reportPk" value="<?PHP echo $v_pk_report ?>">
<BR><input type="image" src="./img/submit.png"  align="absmiddle" border="0" onmouseover="this.src='./img/submit_m.png';" onmouseout="this.src='./img/submit.png';"> 
</form>


</div>
<?PHP
} else {
$v_pk_report = $_POST["reportPk"];

$v_filter = $_POST["filter"];
$v_filtername = $_POST["filtername"];
$v_sql = "rep_sql_clob";
for ($i=0;$i<count($v_filter);$i++) {
	if (strlen($v_filter[$i]) > 0) {
		$v_sql = "replace($v_sql,':".$v_filtername[$i]."','".$v_filter[$i]."')";
	}
}
//$v_sql = "SELECT Xmlgen.GETXML($v_sql) AS xml, rep_name FROM ER_REPORT where pk_report = ".$v_pk_report;
$v_sql = "SELECT DBMS_Xmlgen.GETXML($v_sql) AS xml, rep_name FROM ER_REPORT where pk_report = ".$v_pk_report;
$recordSet = $db->Execute($v_sql);

$v_xml =  $recordSet->fields[0];
$v_repname = $recordSet->fields[1];
if (strpos($v_xml,'oracle.jdbc.driver.OracleSQLException: ORA') > 0) {
	echo "Report SQL execution failed.<BR><BR>";
	echo substr($v_xml,strpos($v_xml,'<ERROR>'));        
}

$v_sql = "SELECT repxsl_xsl as xsl FROM ER_REPXSL WHERE fk_report = ".$v_pk_report;
$recordSet = $db->Execute($v_sql);
$v_xsl =  utf8_encode($recordSet->fields[0]);

$xsl = new DomDocument();
$inputdom = new DomDocument();

    if (!$inputdom->loadXML($v_xml)) {
        echo "Unable to load XML.";
    } else {
        if (!$xsl->loadXML($v_xsl)) {
            echo "Unable to load XSL.";
        } else {
            $proc = new XsltProcessor();
            $xsl = $proc->importStylesheet($xsl);
            $proc->setParameter(null, "titles", "Titles");
            $proc->setParameter(null, "repName", $v_repname);

            $newdom = $proc->transformToDoc($inputdom);
            print $newdom->saveXML();
        }
    }
}
?>


</body>
</html>
<?php

//$db->Close();

}
else header("location: ./index.php?fail=1");
?>
