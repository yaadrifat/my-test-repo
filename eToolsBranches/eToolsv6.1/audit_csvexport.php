<?php

//----- Author: Nicholas Louis ---------//

session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

include("db_config.php");
include("./includes/oci_functions.php");
include_once "./adodb/adodb.inc.php";
include_once('./adodb/adodb-pager.inc.php'); 
include_once('./adodb/toexport.inc.php');

if(@$_SESSION["user"]){
    $mod = $_GET['mod'];
    $df = $_GET['df'];
    $dt = $_GET['dt'];
    $opt = $_GET['opt'];
    $cval = $_GET['cval'];
    
    $ol_df = $df;
    $ol_dt = $dt;
    
    
    if($cval == 'clob'){
        $mod = 6;
    }
    $usrNameQry = mysql_query('SELECT first_name FROM et_users WHERE pk_users='.$_SESSION["PK_USERS"]);
    $usrName_row = mysql_fetch_array($usrNameQry);
    $usrName = $usrName_row['first_name'];

    $grpNameQry = mysql_query('SELECT group_name FROM et_groups WHERE pk_groups='.$_SESSION["FK_GROUPS"]);    
    $grpName_row = mysql_fetch_array($grpNameQry);
    $grpName = $grpName_row['group_name'];


    $colNameAr = array( //1: Manage Account, 2: Data source, 3: Session track, 4: execute query, 5: report designer
        1=>array('USER_NAME', 'TABLE_NAME', 'COLUMN_NAME', 'MODIFIED_ON', 'NEW_VALUE', 'OLD_VALUE','ACTION'),
        2=>array('USER_NAME', 'GROUP_NAME', 'COLUMN_NAME', 'MODIFIED_ON', 'NEW_VALUE', 'OLD_VALUE','ACTION'),
        3=>array('USER_NAME', 'GROUP_NAME', 'LOGIN_TIME', 'LOGGEDOUT_TIME',  'LOGIN_COUNT', 'LOGIN_DURATION','LOGIN_AVERAGE'),
        4=>array('USER_NAME', 'GROUP_NAME', 'QUERY_EXECUTED', 'QUERY_EXECUTED_ON', 'ACTION'),
        5=>array('USER_NAME', 'GROUP_NAME', 'COLUMN_NAME', 'MODIFIED_ON', 'NEW_VALUE', 'OLD_VALUE', 'OLD_VALUE_CLOB', 'NEW_VALUE_CLOB', 'ACTION', 'REMARK'),
        6=>array('USER_NAME', 'GROUP_NAME', 'COLUMN_NAME', 'MODIFIED_ON', 'NEW_VALUE', 'OLD_VALUE', 'OLD_VALUE_CLOB', 'NEW_VALUE_CLOB', 'ACTION', 'REMARK'));    
    $exeQuery = "";
    $selctedValue = $colNameAr[$mod];

    $csvName = '';
    if($mod==1){
        $csvName = 'manage_account_'.date('d-m-Y');
        $reportTitle = 'Manage Account';
    }elseif($mod==2){
        $csvName = 'data_source_'.date('d-m-Y');
        $reportTitle = 'Data Source';
    }elseif($mod==3){
        if($opt == 'optLo'){
            $csvName = 'session_login_activity_'.date('d-m-Y');
            $reportTitle = 'Login Activity';
        }elseif($opt == 'optDa'){
            $csvName = 'session_datasource_activity_'.date('d-m-Y');
            $reportTitle = 'Datasource Activity';
        }elseif($opt == 'optPa'){
            $csvName = 'session_pages_activity_'.date('d-m-Y');
            $reportTitle = 'Accessed Pages Activity';
        }
    }elseif($mod==4){
        $csvName = 'execute_query_'.date('d-m-Y');
        $reportTitle = 'Executed Query';
    }else{
        $csvName = 'report_designer_'.date('d-m-Y');
        $reportTitle = 'Report Designer';
    }
    
    
    if($mod == 3){
        $colNameAr = array(
        1=>array('USER_NAME', 'GROUP_NAME', 'LOGIN_TIME', 'LOGGEDOUT_TIME',  'LOGIN_COUNT', 'LOGIN_DURATION','LOGIN_AVERAGE'),
        2=>array('USER_NAME', 'GROUP_NAME','DS_NAME','DS_HOST','DS_VERSION','MODIFIED_ON'),
        3=>array('USER_NAME', 'GROUP_NAME','ACCESSED_PAGE','MODIFIED_ON'),
        4=>array('USER_NAME', 'GROUP_NAME', 'LOGIN_TIME', 'LOGGEDOUT_TIME',  'LOGIN_COUNT', 'LOGIN_DURATION','LOGIN_AVERAGE','DS_NAME','DS_HOST','DS_VERSION','DS_ACCESSED_ON','ACCESSED_PAGE','ACCESSED_ON'),
        );
        $csvExport = array();
        $months = array('Jan'=>01,'Feb'=>02,'Mar'=>03,'Apr'=>04,'May'=>05,'Jun'=>06,'Jul'=>07,'Aug'=>08,'Sep'=>09,'Oct'=>10,'Nov'=>11,'Dec'=>12);
        $datefromM = substr($df, 3, 3);
        if(strlen($months[$datefromM]) ==1){
                $datefromM = "0".$months[$datefromM];
        }
        $datefrom = substr($df,0,3).$datefromM.substr($df,6,strlen($df));
        $datetoM = substr($dt, 3, 3);
        if(strlen($months[$datetoM]) ==1){
                $datetoM = "0".$months[$datetoM];
        }
        $dateto = substr($dt,0,3).$datetoM.substr($dt,6,strlen($dt));
        
        $datefrom = date("Y-m-d", strtotime($datefrom));
        $dateto = date("Y-m-d", strtotime($dateto));
        
        $similardate = "find_in_set('".$datefrom."',date(MODIFIED_ON))";
        
        $currentDate = date("Y-m-d");    
        if($currentDate == $dateto){
            $dateto = date('Y-m-d', strtotime($currentDate.' +1 day'));
        }     

        
        switch($opt){
            case "optLo": //session tracking -> login details
                //if($datefrom == $dateto){
                if($ol_df == $ol_dt){
                    $usrList = mysql_query("select DISTINCT USER_NAME from et_audit_row where ".$similardate." and MOD_OPT ='".$opt."'");
                }else{
                    $usrList = mysql_query("select DISTINCT USER_NAME from et_audit_row where MODIFIED_ON BETWEEN '".$datefrom."' AND '".$dateto."' and MOD_OPT ='".$opt."'");
                }
                $loOutArray = array();
                $counter = 0;
                
                while($usrList_row = mysql_fetch_array($usrList)){
                    $usrDataListuni = mysql_query("select PK_AUDIT, MODULE, USER_NAME, GROUP_NAME, MODIFIED_ON, CREATED_ON, ACTION, REMARK, QUERY_EXECUTED, QUERY_EXECUTED_ON, MOD_OPT, LOGGEDOUT_TIME, LOGIN_DURATION, LOGIN_TIME, ACCESSED_PAGE, DS_NAME, DS_HOST, DS_VERSION from et_audit_row where MODIFIED_ON between '".$datefrom."' AND '".$dateto."' and MOD_OPT ='".$opt."' and USER_NAME ='".$usrList_row['USER_NAME']."' and LOGIN_DURATION <> '' order by MODIFIED_ON DESC");                    
                    $finalArray = array();
                    $duD = 0;
                    $duH = 0;
                    $duM = 0;
                    $duS = 0;
                    $loginCount = mysql_num_rows($usrDataListuni);
                    while($usrDataListuni_row = mysql_fetch_array($usrDataListuni)){
                        $finalArray = $usrDataListuni_row;
                        $duTmp = explode(":",$usrDataListuni_row['LOGIN_DURATION']);                                    
                        $duD = $duD+(int)$duTmp[1];
                        $duH = $duH+(int)$duTmp[2];
                        $duM = $duM+(int)$duTmp[3];
                        $duS = $duS+(int)$duTmp[4];

                        if($duH > 24){
                            $duD = $duD + 1;
                            $duH = $duH - 24;
                        }

                        if($duM > 60){
                            $duH = $duH + 1;
                            $duM = $duM - 60;
                        }

                        if($duS > 60){
                            $duM = $duM + 1;
                            $duS = $duS - 60;
                        }                        
                        
                        //$finDu = $duD." : ".$duH." : ".$duM." : ".$duS;
                        //$averageLogin = (int)($duD/$loginCount)." : ".(int)($duH/$loginCount)." : ".(int)($duM/$loginCount)." : ".(int)($duS/$loginCount);                                    
                        
                        $finDu = ($duD*24*60)+($duH*60)+$duM;
                        $averageLogin = round($finDu/$loginCount);                        
                        
                        $finalArray['USER_NAME'] = $usrDataListuni_row['USER_NAME'];
                        $finalArray['GROUP_NAME'] = $usrDataListuni_row['GROUP_NAME'];
                        $finalArray['LOGIN_TIME'] = $_SESSION['logintime'];
                        $finalArray['LOGIN_DURATION'] = $finDu;
                        $finalArray['LOGIN_COUNT'] = $loginCount;
                        $finalArray['AVERAGE_LOGIN'] = $averageLogin;
                        $finalArray['LOGGEDOUT_TIME'] = $finalArray['LOGGEDOUT_TIME'];
                    }
                    $loOutArray[$counter] = $finalArray;
                    $counter++;
                }            
            $totRows = sizeof($loOutArray);

            
            
            $loColname = array("User Name", "Group Name", "Login Time", "Logged out time", "Login count", "Total login duration (Minutes)", "Average of login duration (Minutes)");            
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename="'.$csvName.'.csv"');
            $output = fopen('php://output', 'w');
            echo "Audit Report : ".$reportTitle."\n\r\n\r";            
            fputcsv($output, $loColname);
            $loColname = array("User Name", "Group Name", "Login Time", "Logged out time", "Login count", "Login duration", "Average login");
            $dataAr = array();
            for($k=0; $k<$totRows; $k++){
                $grpNamQry = mysql_query("select group_name from et_groups where group_name='".$loOutArray[$k]["GROUP_NAME"]."'");                
                $grpNam_row = mysql_fetch_array($grpNamQry);
                $usrNamQry = mysql_query("select last_name, first_name from et_users where user_name='".$loOutArray[$k]["USER_NAME"]."'");                
                $usrNam_row = mysql_fetch_array($usrNamQry);
                $usrNam = $usrNam_row['first_name']." ".$usrNam_row['last_name'];
                $dataAr[$k][$loColname[0]] = $usrNam;
                $dataAr[$k][$loColname[1]] = $grpNam_row['group_name'];
                $dataAr[$k][$loColname[2]] = date("m-d-Y h:i",strtotime($loOutArray[$k]['LOGIN_TIME']));
                $dataAr[$k][$loColname[3]] = date("m-d-Y h:i",strtotime($loOutArray[$k]['LOGGEDOUT_TIME']));
                $dataAr[$k][$loColname[4]] = $loOutArray[$k]['LOGIN_COUNT'];
                $dataAr[$k][$loColname[5]] = $loOutArray[$k]['LOGIN_DURATION'];
                $dataAr[$k][$loColname[6]] = $loOutArray[$k]['AVERAGE_LOGIN'];
                //$dataAr[$k][$loColname[7]] = date("m-d-Y h:i",strtotime($loOutArray[$k]['LOGGEDOUT_TIME']));                
                fputcsv($output, $dataAr[$k]);
            }
            fclose($output);
            break;

            case "optDa": //session tracking -> data source
                $exeQuerylo .= "SELECT ";
                for($i=0; $i<sizeof($colNameAr[2]); $i++){
                        $exeQuerylo .= $colNameAr[2][$i].",";
                }
                $exeQuerylo = substr($exeQuerylo, 0,strlen($exeQuerylo)-1);

                $exeQuerylo = str_replace("MODIFIED_ON",  "DATE_FORMAT(MODIFIED_ON,'%m-%d-%Y %h:%i')", $exeQuerylo);
                
                //if($datefrom == $dateto){
                if($ol_df == $ol_dt){
                    $exeQuerylo .= " FROM et_audit_row WHERE MODULE = ".$mod." and MOD_OPT = 'optDa' and ".$similardate." order by MODIFIED_ON DESC";		                
                }else{
                    $exeQuerylo .= " FROM et_audit_row WHERE MODULE = ".$mod." and MOD_OPT = 'optDa' and MODIFIED_ON between '".$datefrom."'and '".$dateto."'  order by MODIFIED_ON DESC";		                
                }
                
                $logRs = mysql_query($exeQuerylo);
                header('Content-Type: text/csv; charset=utf-8');
                header('Content-Disposition: attachment; filename="'.$csvName.'.csv"');
                $output = fopen('php://output', 'w');
                echo "Audit Report : ".$reportTitle."\n\r\n\r"; 
                $loColname = ['USER_NAME','GROUP_NAME', 'DATA_SOURCE_NAME','DATA_SOURCE_HOST','DATA_SOURCE_VERSION','DATA_SOURCE_ACCESSED_ON'];
                fputcsv($output, $loColname);
                while($data = mysql_fetch_assoc($logRs)){
                    fputcsv($output, $data);
                }
                fclose($output);
            break;

            case "optPa": //session tracking -> page access
                $exeQuerylo .= "SELECT ";
                for($i=0; $i<sizeof($colNameAr[3]); $i++){
                    $exeQuerylo .= $colNameAr[3][$i].",";
                }
                $exeQuerylo = substr($exeQuerylo, 0,strlen($exeQuerylo)-1);
                
                $exeQuerylo = str_replace("MODIFIED_ON",  "DATE_FORMAT(MODIFIED_ON,'%m-%d-%Y %h:%i')", $exeQuerylo);
                
                //if($datefrom == $dateto){
                if($ol_df == $ol_dt){
                    $exeQuerylo .= " FROM et_audit_row WHERE MODULE = ".$mod." and MOD_OPT = 'optPa' and ".$similardate." order by MODIFIED_ON DESC";
                }else{
                    $exeQuerylo .= " FROM et_audit_row WHERE MODULE = ".$mod." and MOD_OPT = 'optPa' and MODIFIED_ON between '".$datefrom."'and '".$dateto."'  order by MODIFIED_ON DESC";
                }              
                
                $logRspa = mysql_query($exeQuerylo);
                header('Content-Type: text/csv; charset=utf-8');
                header('Content-Disposition: attachment; filename="'.$csvName.'.csv"');
                $output = fopen('php://output', 'w');
                echo "Audit Report : ".$reportTitle."\n\r\n\r"; 
                $loColname = ['USER_NAME','GROUP_NAME', 'LAST_ACCESSED','LAST_ACCESSED_ON'];
                fputcsv($output, $loColname);
                while($logRspars = mysql_fetch_assoc($logRspa)){
                    fputcsv($output, $logRspars);

                }
                fclose($output);
            break;
        }
    }else{
        $exeQuery .= "SELECT ";
        for($i=0; $i<sizeof($selctedValue); $i++){
            if($selctedValue[$i] != 'MODULE'){
                $exeQuery .= $selctedValue[$i].",";
            }
        }
        $exeQuery = substr($exeQuery, 0,strlen($exeQuery)-1);

        //echo $exeQuery.'<br>';
        
        if($mod == 4){            
            $exeQuery = str_replace("QUERY_EXECUTED_ON",  "DATE_FORMAT(QUERY_EXECUTED_ON,'%m-%d-%Y %h:%i') as QUERY_EXECUTED_ON", $exeQuery);
        }else{
            $exeQuery = str_replace("MODIFIED_ON",  "DATE_FORMAT(MODIFIED_ON,'%m-%d-%Y %h:%i') as MODIFIED_ON", $exeQuery);
        }
        $df = date("Y-m-d", strtotime($df));
        $dt = date("Y-m-d", strtotime($dt));
        
        $old_df = $df;
        $old_dt = $dt;        
        
        $similardate = "find_in_set('".$df."',date(MODIFIED_ON))";        
        
        $currentDate = date("Y-m-d");    
        if($currentDate == $dt){
            $dt = date('Y-m-d', strtotime($currentDate.' +1 day'));
        }     
        
        $df = date("Y-m-d", strtotime($df));
        $dt = date("Y-m-d", strtotime($dt));
        

        if($mod == 4){ //executed query
            //if($df == $dt){
            if($old_df == $old_dt){                
                $exeQuery .= " FROM et_audit_row WHERE MODULE = ".$mod." and ".$similardate."  order by QUERY_EXECUTED_ON DESC";                
            }else{            
                $exeQuery .= " FROM et_audit_row WHERE MODULE = ".$mod." and MODIFIED_ON between '".$df."'and '".$dt."'  order by MODIFIED_ON DESC";
            }            
        }elseif($mod == 5){//report designer

            //if($df == $dt){
            if($old_df == $old_dt){                 
                /*$exeQuery .= " FROM et_audit_row a, et_audit_column b WHERE a.PK_AUDIT = b.FK_AUDITROW and a.USER_NAME ='".$_SESSION["user"]."'
AND a.MODULE in (5,6) AND MODIFIED_ON between '".$df."' AND ".$similardate;*/
    
                $exeQuery .= " FROM et_audit_row a  LEFT JOIN et_audit_column b ON a.PK_AUDIT = b.FK_AUDITROW where a.USER_NAME ='".$_SESSION["user"]."'
AND a.MODULE in (5,6) AND ".$similardate;
                
            }else{            
                /*$exeQuery .= " FROM et_audit_row a, et_audit_column b WHERE a.PK_AUDIT = b.FK_AUDITROW and a.USER_NAME ='".$_SESSION["user"]."'
AND a.MODULE in (5,6) AND MODIFIED_ON between '".$df."' and '".$dt."'  order by MODIFIED_ON DESC";                */
            $exeQuery .= " FROM et_audit_row a LEFT JOIN et_audit_column b ON a.PK_AUDIT = b.FK_AUDITROW where a.USER_NAME ='".$_SESSION["user"]."'
AND a.MODULE in (5,6) AND a.MODIFIED_ON between '".$df."' and '".$dt."'  order by a.MODIFIED_ON DESC";                
            }
        }else{ //Manage account & Datasource
            //if($df == $dt){
            if($old_df == $old_dt){
                //$exeQuery .= " FROM et_audit_row a, et_audit_column b WHERE a.PK_AUDIT = b.FK_AUDITROW and a.MODULE =".$mod." and ".$similardate." order by MODIFIED_ON DESC";                            
                //$exeQuery .= " FROM et_audit_row a  LEFT JOIN et_audit_column b ON a.PK_AUDIT = b.FK_AUDITROW where a.USER_NAME ='".$_SESSION["user"]."'AND a.MODULE in (5,6) AND  ".$similardate;
                $exeQuery .= " FROM et_audit_row a  LEFT JOIN et_audit_column b ON a.PK_AUDIT = b.FK_AUDITROW where a.USER_NAME ='".$_SESSION["user"]."'AND a.MODULE = ".$mod." AND  ".$similardate;
            }else{            
                //$exeQuery .= " FROM et_audit_row a, et_audit_column b WHERE a.PK_AUDIT = b.FK_AUDITROW and a.MODULE =".$mod." and MODIFIED_ON between '".$df."'and '".$dt."' order by MODIFIED_ON DESC";                            
                //$exeQuery .= " FROM et_audit_row a LEFT JOIN et_audit_column b ON a.PK_AUDIT = b.FK_AUDITROW where a.USER_NAME ='".$_SESSION["user"]."' AND a.MODULE in (5,6) AND a.MODIFIED_ON between '".$df."' and '".$dt."'  order by a.MODIFIED_ON DESC";                
                $exeQuery .= " FROM et_audit_row a LEFT JOIN et_audit_column b ON a.PK_AUDIT = b.FK_AUDITROW where a.USER_NAME ='".$_SESSION["user"]."' AND a.MODULE =".$mod." AND a.MODIFIED_ON between '".$df."' and '".$dt."'  order by a.MODIFIED_ON DESC";                
            }
        }
//        
//        echo $exeQuery;
//        exit;
        
        $recordSet = mysql_query($exeQuery);
        
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="'.$csvName.'.csv"');
        $output = fopen('php://output', 'w');
        echo "Audit Report : ".$reportTitle."\n\r\n\r";
        fputcsv($output, $colNameAr[$mod]);
        
        while($rs = mysql_fetch_assoc($recordSet)){
            fputcsv($output, $rs);
        }
        fclose($output);       
    }
}
else header("location: index.php?fail=1");
?>