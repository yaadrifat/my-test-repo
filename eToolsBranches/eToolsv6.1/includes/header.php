<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"><html>
<head><title>Velos eTools</title>

<!--    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">		-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


    <link rel="stylesheet" type="text/css" media="print" href="css/print.css">
    
    <style type="text/css" media="screen">            
            @import url("css/layout.css");
            @import url("css/content.css");
            @import url("css/docbook.css");		
    </style>
    <meta name="MSSmartTagsPreventParsing" content="TRUE">
<?php
        //---------------- for enabling UTF-8 (This was done to enable korean character) for the ticket 38267--------------//
            mb_internal_encoding('UTF-8');
            mb_http_output('UTF-8');
            mb_http_input('UTF-8');
            mb_language('uni');
            mb_regex_encoding('UTF-8');
            ob_start('mb_output_handler');

            PutEnv("NLS_LANG=AMERICAN_AMERICA.AL32UTF8");
        //---------------- for enabling Korean character --------------//
?>
</head>
   <body>
    <!-- header BEGIN -->
    <div id="fedora-header">
     <table width="100%" border="0" height="72">
        <tr align="left"><td class="headertext" style="padding-left:450px; padding-top:10px;"><BR><BR>You are connected to:
	<font size=4 color=green>                        
         <?PHP		 
	  if (isset($_SESSION["DATASOURCE"])) {
			echo $_SESSION["DATASOURCE"]." Database";			
		} else {
			echo "&nbsp;";
		}
		?>
        </font> <font size=4 color=red><?PHP if (isset($_SESSION["VERSION"])) echo '{'.$_SESSION["VERSION"].'}'; ?>
         </font> 
		 </td>
		 </tr>
		 </table>
<?PHP
//include ("./txt-db-api.php");
    include("../db_config.php");
    if (isset($_SESSION["MODULE"])) {
        $module = $_SESSION["MODULE"];
    } else {
        $module = "";
    }

//$db = new Database("etools");    
//$result = $db->executeQuery("SELECT * from ET_VERSION");
$ver_result = mysql_query("SELECT * from et_version where pk_et = (select MAX(pk_et) from et_version)");

while($row = mysql_fetch_array($ver_result)) {
    $version_num = $row["et_version_num"];
    $build_num = $row["et_build_num"];
    $eres_ver = $row["eres_ver"];       
}

    //$loggedusr = $db->executeQuery("SELECT LAST_NAME, FIRST_NAME from ET_USERS where USER_NAME = '".$_SESSION["user"]."'");
    $loggedusr = mysql_query("SELECT last_name, first_name from et_users where user_name = '".$_SESSION["user"]."'");
    while ($log_row = mysql_fetch_array($loggedusr)) {
        $fName = $log_row["first_name"];
        $lName = $log_row["last_name"];
    }
?>	
    <div id="fedora-header-logo">
        <a id="link-internal"><img src="img/velos_etools_v3.png" alt="Velos eTools"></a><br>
        <span>Version <b style="color:red;"><?php echo $version_num ?></b> Build#<b style="color:red;"><?php echo $build_num ?></b> <i style="font-size:11px; color:gray;"> for <B><?php echo $eres_ver ?></b></i></span><br>
        <!-- <span  style="font-size:11px; color:gray; line-height:18px;">Compatible with : <b></b></span> -->
    </div>			
    <div id="fedora-header-items">
        <span class="userName"><p>Logged in as: &nbsp;</p>&nbsp;&nbsp;&nbsp;<?php echo "<p>".$fName."&nbsp;&nbsp;".$lName."</p>" ?></span>
        <span class="fedora-header-icon">
                <a href="./docs/Velos eTools Technical User Manual.pdf" id="link-internal" target="_blank" alt="eTools help manual" ><img src="img/help.png" alt="eTools help manual">Help</a>
        </span>
    </div>

    <div id="fedora-nav"></div>
    <!-- header END -->

    <!-- leftside BEGIN -->
    <div id="fedora-side-left">
    <div id="fedora-side-nav-label">Site Navigation:</div><ul id="fedora-side-nav">
<?PHP
    //$db = new Database("etools");
    //$result = $db->executeQuery("SELECT APP_RIGHTS from ET_GROUPS where PK_GROUPS=".$_SESSION['FK_GROUPS']);

    $result = mysql_query("SELECT app_rights from et_groups where pk_groups=".$_SESSION['FK_GROUPS']);    
    while($ar_row = mysql_fetch_array($result)){        
        $v_app_rights = $ar_row["app_rights"];        
    }    
    //$result = $db->executeQuery("SELECT pk_menu,menu_name,level,disp_seq,visible,page,module,default_access from ET_MENUS order by disp_seq");
    $m_result = mysql_query("SELECT pk_menu,menu_name,level,disp_seq,visible,page,module,default_access from et_menus order by disp_seq");
    if (mysql_num_rows($m_result) > 0){
        $v_level1 = false;
        $v_level2 = false;
        $v_menu = "";
        $v_has_child = false;
        while($m_row = mysql_fetch_array($m_result)){
            if($m_row["level"] == 0){
                $pos = strpos("PAD".$v_app_rights,"|".$m_row["pk_menu"].":1");                
                if (empty($pos)) $pos = -1;
                if (($m_row["visible"] == 1) && ($pos >= 0)){
                    if($v_level1) {
                            $v_menu .= "</li>";
                    }
                    if($v_level2) {
                            $v_menu .= "</ul>";
                            $v_level2 = false;
                    }
                    $v_level1 = true;
                    if ($v_has_child ){
                            echo $v_menu;
                            $v_has_child = false;
                    }
                    $v_menu = "";
                    if ($row["default_access"] == 1) $v_has_child = true;
                    $v_menu .= '<li><a href="menus.php?page='.$m_row["page"].'&module='.$m_row["module"].'" id="link-internal">'.$m_row["menu_name"].'</a>';                                            
                }                    
            }else{
                $pos = strpos($v_app_rights,'|'.$m_row["pk_menu"].":1");
                if (empty($pos)) $pos = -1;
                if (($m_row["visible"] == 1) && ($pos >= 0)){
                    if ($m_row["module"] == $module){ 
                        if($_SESSION["PK_USERS"] == 1){ //---- implemented for user rights enhancement - admin level ----//
                            $v_level2 = true;
                            if ($v_level1) {
                                    $v_menu .= '<ul>';
                            }
                            $v_level1 = false;
                            $v_menu .= '<li><strong><a href="menus.php?page='.$m_row["page"].'&module='.$m_row["module"].'" id="link-internal">'.$m_row["menu_name"].'</a></strong></li>';
                        //}else if($data["menu_name"] != 'Groups'){ //---- implemented for user rights enhancement - user level ----//
                        }else{ //--- fixing bug 19613 ---//
                            $v_level2 = true;
                            if ($v_level1) {
                                $v_menu .= '<ul>';
                            }
                            $v_level1 = false;
                            $v_menu .= '<li><strong><a href="menus.php?page='.$m_row["page"].'&module='.$m_row["module"].'" id="link-internal">'.$m_row["menu_name"].'</a></strong></li>';
                        }
                    }
                    $v_has_child = true;
                }
                //fix for the bug 23902
                //if($m_row["menu_name"] == 'View Datasource'){
                if($m_row["pk_menu"] == 32){
                    $v_has_child = true;   
                }                
            }
        }
    }
echo $v_menu;
?>

    </div>
    <!-- leftside END -->

    <!-- content BEGIN -->
    <div id="fedora-middle-two">
    <!-- footer END -->
</body>
</html>
