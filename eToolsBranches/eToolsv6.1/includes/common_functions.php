<?PHP
function daysSinceEpoch($date)
{
     $date = explode("-", $date);
     $years = $date[2] - 1970;

     $tot_days = $years * 365;
     $tot_days += floor(($years + 1) / 4); // Add
     $tot_days += date("z", mktime(1,0,0,round($date[1]),round($date[0]),$date[2]));
     return $tot_days;
}
?>