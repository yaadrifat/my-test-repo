<?php
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Groups</title>
    <script src="js/jquery-1.10.2.js"></script>
<script>
function validate(form){
    if (form.gname.value == ""){
        alert("Group name cannot be blank.");
        return false;
    }
}
</script>
<?php
    include("./includes/oci_functions.php");
    include("db_config.php");
    include("./includes/header.php");
    require_once('audit_queries.php');
    $ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>

</head>
<body>
<div id="fedora-content">	
<?PHP
    if (!($_SERVER['REQUEST_METHOD'] == 'POST')) {
    $v_mode = $_GET["mode"];
    $v_pk_groups = $_GET["pk_groups"];

    $rs = mysql_query("select group_name,group_desc,app_rights,ds_rights from et_groups where pk_groups = ".$v_pk_groups);
    $rs_row = mysql_fetch_array($rs);
    if ($_GET["mode"] == 'n'){
        echo '<div class="navigate">Manage Account - Groups - New</div>';
        $v_mode = 'N';
        $v_appRights = "";
        $v_gname = "";        
        $v_dsRights =  $rs_row["ds_rights"];        
        $v_gdesc = "";
    } else {
        echo '<div class="navigate">Manage Account - Groups - Edit</div>';
        $v_mode = 'M';	
        $v_appRights =  $rs_row["app_rights"];
        $v_dsRights =  $rs_row["ds_rights"];
        $v_gname =  $rs_row["group_name"];
        $v_gdesc =  $rs_row["group_desc"];
    }
?>
    <form name="editgroup" action="account_groups_edit.php" method="POST" onSubmit="if (validate(document.editgroup) == false) return false;">
        <input type="hidden" value="0" name="dns" class="dnsclass"/>
        <INPUT TYPE="hidden" NAME="pk_groups" value="<?PHP echo $v_pk_groups; ?>"/>
        <INPUT TYPE="hidden" NAME="mode" value="<?PHP echo $v_mode; ?>"/>
        <table border="1">
        <tr><td>Name: </td><td><input class="required" type="text" name="gname" size="30" value="<?PHP echo $v_gname; ?>"/></td></tr>
        <tr><td>Description: </td><td><input type="text" name="gdesc" size="50" value="<?PHP echo $v_gdesc; ?>"/></td></tr>
        <tr><td>Modules: </td><td>
        <?PHP
        // app rights display

        if(substr($v_appRights,0,1) == "|"){                        
            $v_appRights = substr($v_appRights,1,strlen($v_appRights));
        }                        
        $v_appRights_arr = explode("|",$v_appRights);       

        
        $rs = mysql_query("select pk_menu,menu_name,level,default_access,module from et_menus order by disp_seq");
        $v_counter = 0; 
        $str = '';
        while($rs_row = mysql_fetch_array($rs)){
            if ($rs_row["default_access"] == 1) {
                echo '<INPUT TYPE="hidden" name="modules['.$v_counter.']" value="'.$rs_row["pk_menu"].'"/>';
            }else{
                if($rs_row["level"] == 1) {                    
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";                    
                    $pkm_t = $rs_row["pk_menu"].":"."1";
                    $pkm_f = $rs_row["pk_menu"].":"."0";                    
                    if(in_array($pkm_t,$v_appRights_arr)){                        
                        echo '<INPUT TYPE="CHECKBOX" checked name="modules['.$v_counter.']" value="'.$rs_row["pk_menu"].'">'.$rs_row["menu_name"];
                    }else{                        
                        echo '<INPUT TYPE="CHECKBOX" name="modules['.$v_counter.']" value="'.$rs_row["pk_menu"].'">'.$rs_row["menu_name"];
                    }
                } else {
                    echo "<B>".$rs_row["menu_name"]."</B>";
                    echo '<INPUT type="hidden" name="modules['.$v_counter.']" value="'.$rs_row["pk_menu"].'LEVEL">';
                }
                echo "<BR>";
            }
            echo '<INPUT type="hidden" name="counter['.$v_counter.']" value="'.$rs_row["pk_menu"].'">';
            $v_counter ++;
        }
        ?>
        </td></tr>
        <tr><td>Datasources: </td><td class="dsChkbox">        
        <?PHP
        
        //-- display the datasource 
        $rs_pk = mysql_query("select pk_ds,ds_name,fk_user from et_ds order by ds_name");
        $v_counter = 0;
        
        while($rs_r = mysql_fetch_array($rs_pk)) {
            $v_pos = strpos("PAD".$v_dsRights,"|".$rs_r["pk_ds"].":1");
            if (empty($v_pos)) $v_pos = strpos("PAD".$v_dsRights,"|".$rs_r["pk_ds"].":1");
            if (empty($v_pos)) $v_pos = -1;
            
            if($_SESSION['FK_GROUPS'] == 1){
                if ($v_mode != 'N'){
                    $v_pos = strpos("PAD".$v_dsRights,"|".$rs_r["pk_ds"].":1");
                    if (empty($v_pos)) $v_pos = strpos("PAD".$v_dsRights,"|".$rs_r["pk_ds"].":1");
                    if (empty($v_pos)) $v_pos = -1;
                    
                    if($v_pos != -1){
                        echo '<INPUT TYPE="CHECKBOX" checked name="ds['.$v_counter.']" value="'.$rs_r["pk_ds"].'">'.$rs_r["ds_name"];   
                    }else{
                        echo '<INPUT TYPE="CHECKBOX" name="ds['.$v_counter.']" value="'.$rs_r["pk_ds"].'">'.$rs_r["ds_name"];
                    }
                }else {
                    echo '<INPUT TYPE="CHECKBOX" name="ds['.$v_counter.']" value="'.$rs_r["pk_ds"].'">'.$rs_r["ds_name"];
                }
                echo "<br>";
            }else{
                if($_GET["pk_groups"] == ''){
                    $grpval = $_POST["pk_groups"];
                }else{
                    $grpval = $_GET["pk_groups"];
                }
                $recordsot = mysql_query("SELECT ds_rights FROM et_groups where pk_groups = ".$grpval);
                if(mysql_num_rows($recordsot) > 0){
                    while($rowot = mysql_fetch_array($recordsot)){
                        if(substr($rowot['ds_rights'],0,1) == "|"){                        
                            $dsString = substr($rowot['ds_rights'],1,strlen($rowot['ds_rights']));
                        }                        
                        $dsRights = explode("|",$dsString);
                    }                    
                }                

                
                for($i=0; $i<sizeof($dsRights); $i++){
                    list($pk_ds, $ds)= explode(":",$dsRights[$i]);
                    
                    if($ds==1){
                        $dsQry = mysql_query("SELECT * from et_ds where pk_ds = ".$pk_ds);
                        //$dsQry = mysql_query("SELECT * from et_ds where pk_ds = ".$pk_ds." and fk_user = ".$_SESSION["PK_USERS"]);
                        $dsQry_rs = mysql_fetch_array($dsQry);                        
                        $ds_aviPK[$i] = $dsQry_rs["pk_ds"];
                        $ds_aviDS[$i] = $dsQry_rs["ds_name"];
                    }else{
                        $dsQry = mysql_query("SELECT * from et_ds where pk_ds = ".$pk_ds." and fk_user =".$_SESSION["PK_USERS"]);
                        $dsQry_rs = mysql_fetch_array($dsQry);
                        $ds_aviPK[$i] = $dsQry_rs["pk_ds"];
                        $ds_aviDS[$i] = $dsQry_rs["ds_name"];
                        $ds_aviPK_a[$i] = $dsQry_rs["pk_ds"];
                    }
                    
                }                
                $dis = '';
                if(in_array($rs_r["pk_ds"],$ds_aviPK)){
                    if($rs_r["fk_user"] == 1){
                        $dis = 'disabled';
                    }else{
                        $dis = '';
                    }
                    if ($v_mode!='N'){
                        if(in_array($rs_r["pk_ds"],$ds_aviPK_a)){
                            echo '<INPUT TYPE="CHECKBOX" name="ds['.$v_counter.']" value="'.$rs_r["pk_ds"].'">'.$rs_r["ds_name"];
                        }else{
                            echo '<INPUT '.$dis.' checked TYPE="CHECKBOX" name="ds['.$v_counter.']" value="'.$rs_r["pk_ds"].'">'.$rs_r["ds_name"];                        
                            if($dis != ''){
                                echo '<INPUT TYPE="hidden" name="ds['.$v_counter.']" value="'.$rs_r["pk_ds"].'">';                        
                            }                            
                        }
                    }else{
                        echo '<INPUT TYPE="CHECKBOX" name="ds['.$v_counter.']" value="'.$rs_r["pk_ds"].'">'.$rs_r["ds_name"];
                    }
                    echo '<br>';
                }                
            }
            echo '<INPUT type="hidden" name="ds_counter['.$v_counter.']" value="'.$rs_r["pk_ds"].'">';            
            $v_counter ++;             
        }        
        ?>
        </td></tr>
        </table>
        <BR>
        <input type="image" src="./img/submit.png" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />
    </form>	
<?PHP
}else{
        $pkgroup = $_POST["pk_groups"];
	$v_pk_groups = $_POST["pk_groups"];
	$v_mode = $_POST["mode"];
	$v_gname = $_POST["gname"];
	$v_gdesc = $_POST["gdesc"];
	$v_counter = $_POST["counter"];
	$v_modules = $_POST["modules"];
        
        if(isset($_POST["ds"])){
            $v_ds = $_POST["ds"];
        }else{
            $v_ds = 'NULL';
        }

	$v_ds_counter = $_POST["ds_counter"];
	$v_gstr = "";
	$v_dstr = "";
        $parent_group_pk = $_SESSION["FK_GROUPS"];        

        
        if($_SESSION["FK_GROUPS"] == 1){
            //$ex_ds = mysql_query("select ds_rights, ds_status from et_groups where pk_groups=1");
            $ex_ds = mysql_query("select ds_rights from et_groups where pk_groups=1");
        }else{
            //$ex_ds = mysql_query("select ds_rights, ds_status from et_groups where pk_groups=".$pkgroup);
            $ex_ds = mysql_query("select ds_rights from et_groups where pk_groups=".$pkgroup);
        }
        
        $ex_ds_rs = mysql_fetch_array($ex_ds);
        //$ex_ds_status = explode(",", $ex_ds_rs["ds_status"]);      
        if(substr($ex_ds_rs["ds_rights"],0,1) == "|"){
            $ex_ds_rs = substr($ex_ds_rs["ds_rights"],1,strlen($ex_ds_rs["ds_rights"]));
        }

        $ex_ds_pipe = explode("|", $ex_ds_rs);        
        if($v_mode == 'N'){            
            for($dsc=0; $dsc < count($v_ds_counter); $dsc++){
                if(isset($v_ds[$dsc])){
                    $v_dstr .= $v_ds[$dsc].":1|";
                }            
            }  
        }else{
            $tarr[]='';
            for($colD =0; $colD<count($ex_ds_pipe); $colD++){
                list($ckey,$cval) = explode(":",$ex_ds_pipe[$colD]);                
                $tarr[$colD] = $ckey;
            }
            
            for($colD1 =0; $colD1<count($tarr); $colD1++){
                if(in_array($tarr[$colD1],$v_ds)==true){
                    $v_dstr = $v_dstr.$tarr[$colD1].":1|";
                }else{
                    $v_dstr = $v_dstr.$tarr[$colD1].":0|";
                }
            }
            
            $vv_ds = array_values($v_ds);
            
            for($colD2 =0; $colD2<count($vv_ds); $colD2++){
                if(!in_array($v_ds[$colD2],$ex_ds_status)){                    
                    array_push($ex_ds_status, $vv_ds[$colD2]);
                }
            }
            $dsstat_str = '';
            for($dsst =0; $dsst<count($ex_ds_status); $dsst++){
                if($dsstat_str == ''){
                    $dsstat_str = $ex_ds_status[$dsst];
                }else{
                    $dsstat_str = $dsstat_str.','.$ex_ds_status[$dsst];   
                }                
            }

        }

        
            
        if(substr($v_dstr,strlen($v_dstr)-1,strlen($v_dstr))=="|"){
            $v_dstr = substr($v_dstr,0,strlen($v_dstr)-1);
        }
        
        $modarr[] = '';
        $mfmarr[] = '';
        $modulefrmetmenu = mysql_query("select pk_menu from et_menus where level = 0");
        while($mfm = mysql_fetch_array($modulefrmetmenu)){
            $mfmarr[] = $mfm["pk_menu"];
        }
        for($ch=0; $ch < count($v_counter); $ch++) {
            if(in_array($v_counter[$ch],$v_modules)){
                $v_gstr = $v_gstr.$v_counter[$ch].':1|';
            }else{
                if(in_array($v_counter[$ch],$mfmarr)){
                    $v_gstr = $v_gstr.$v_counter[$ch].':1|';
                }else{
                    $v_gstr = $v_gstr.$v_counter[$ch].':0|';
                }                
            }
        }
        
        if(substr($v_gstr,0,1) != '|'){
            $v_gstr = '|'.$v_gstr;
        }                
        if(substr($v_gstr,strlen($v_gstr)-1,strlen($v_gstr)) == '|'){
            $v_gstr = substr($v_gstr,0,strlen($v_gstr)-1);
        }         

        
        if($v_ds != 'NULL'){
            if ($v_mode == 'N') {
                    $result = mysql_query("SELECT group_name from et_groups where group_name='".$v_gname."'");
                    if (mysql_num_rows($result) > 0) {
                        echo "Group already exists";
                        echo '<meta http-equiv="refresh" content="3; url=./account_groups.php">';
                    }else{
                        $v_dstr = "|".$v_dstr;                
                        //$rs = mysql_query("INSERT INTO et_groups (fk_parentgroup,group_name,group_desc,app_rights,ds_rights,ds_status) VALUES ('".$parent_group_pk."','".$v_gname."','".$v_gdesc."','".$v_gstr."','".$v_dstr."','".$dsstat_str."')");
                        $rs = mysql_query("INSERT INTO et_groups (fk_parentgroup,group_name,group_desc,app_rights,ds_rights) VALUES ('".$parent_group_pk."','".$v_gname."','".$v_gdesc."','".$v_gstr."','".$v_dstr."')");
                        /*--- AUDIT ---*/
                        $rowInfo = array(1,$_SESSION["user"]," ", "SYSDATE", "SYSDATE", "I");
                        auditQuery(1,$rowInfo);

                        $colArray = array('GROUP_NAME','GROUP_DESC','APP_RIGHTS','DS_RIGHTS');
                        $colvalarray = array($v_gname,$v_gdesc,$v_gstr,$v_dstr);
                        $tblname = 'et_groups';
                        colQueries($colArray, $colvalarray, $tblname);
                        /*--- END ---*/

                        echo "Data Saved.";
                        echo '<meta http-equiv="refresh" content="0; url=./account_groups.php">';
                    }
            }else{
                    /*--- AUDIT ---*/
                    $oldValue = mysql_query("SELECT group_name, group_desc, app_rights, ds_rights  FROM et_groups WHERE pk_groups=".$v_pk_groups);            
                    $oldValueRs = mysql_fetch_array($oldValue);
                    /*--- END ---*/
                    $namArr = array();
                    $valArr = array();
                    $oldValueRsArr = array();
                    $cnt = 0;

                    if($oldValueRs['group_name'] != $v_gname){
                        $namArr[$cnt] = 'group_name';
                        $valArr[$cnt] = $v_gname;
                        $oldValueRsArr[$cnt] = $oldValueRs['group_name'];
                        $cnt++;
                    }
                    if($oldValueRs['group_desc'] != $v_gdesc){
                        $namArr[$cnt] = 'group_desc';
                        $valArr[$cnt] = $v_gdesc;
                        $oldValueRsArr[$cnt] = $oldValueRs['group_desc'];
                        $cnt++;
                    }                
                    if($oldValueRs['app_rights'] != $v_gstr){
                        $namArr[$cnt] = 'app_rights';
                        $valArr[$cnt] = $v_gstr;
                        $oldValueRsArr[$cnt] = $oldValueRs['app_rights'];
                        $cnt++;
                    }
                    if($oldValueRs['ds_rights'] != $v_dstr){
                        $namArr[$cnt] = 'ds_rights';
                        $valArr[$cnt] = $v_dstr;
                        $oldValueRsArr[$cnt] = $oldValueRs['ds_rights'];
                        $cnt++;
                    }
                    if(substr($v_dstr,0,1) != '|'){
                        $v_dstr = '|'.$v_dstr;
                    }                
                    if(substr($v_dstr,strlen($v_dstr)-1,strlen($v_dstr)) == '|'){
                        $v_dstr = substr($v_dstr,0,strlen($v_dstr)-1);
                    }

                    
                    //$rs = mysql_query("UPDATE et_groups set group_name = '".$v_gname."', group_desc = '".$v_gdesc."', app_rights = '".$v_gstr."', ds_rights = '".$v_dstr."', ds_status = '".$dsstat_str."' where pk_groups = ".$v_pk_groups);
                    $rs = mysql_query("UPDATE et_groups set group_name = '".$v_gname."', group_desc = '".$v_gdesc."', app_rights = '".$v_gstr."', ds_rights = '".$v_dstr."' where pk_groups = ".$v_pk_groups);
                    /*--- AUDIT ---*/
                    $rowInfo = array(1,$_SESSION["user"],"", "SYSDATE", "SYSDATE", "U");
                    auditQuery(1,$rowInfo);

                    $colArray = $namArr;
                    $colvalarray = $valArr;
                    $tblname = 'et_groups';                

                    colQueries($colArray, $colvalarray, $tblname, $oldValueRsArr);
                    /*--- END ---*/

                    echo "Data Saved.";
                    echo '<input type="hidden" value=0 name="dns"  class="dnsclass"/>';
                    echo '<meta http-equiv="refresh" content="0; url=./account_groups.php">';
            }            
        }else{
            echo "Data Not Saved.";
            echo '<input type="hidden" value=1 name="dns" class="dnsclass"/>';
            //echo "<script>alert('Atleast one datasource should be selected..!');</script>";
            if($v_mode == 'N'){
                if($v_pk_groups == 1){
                    echo "<script>alert('Atleast one datasource should be selected..!'); window.location.href='./account_groups_edit.php';</script>";                                    
                }else{
                    echo "<script>alert('Atleast one datasource should be selected..!'); window.location.href='./account_groups_edit.php?mode=n&pk_groups=".$v_pk_groups."';</script>";                                    
                }                
            }else{
                echo "<script>alert('Atleast one datasource should be selected..!'); window.location.href='./account_groups_edit.php?mode=m&pk_groups=".$v_pk_groups."';</script>";                
            }            
            
        }

}
?>
</div>

</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
