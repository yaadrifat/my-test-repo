<?php
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
include("db_config.php");
include("./includes/oci_functions.php");
include_once "./adodb/adodb.inc.php";
include_once('./adodb/adodb-pager.inc.php'); 
include_once('./adodb/toexport.inc.php');
require_once('audit_queries.php');


$db = NewADOConnection("oci8");
$db->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);

if (!$db) die("Connection failed");


if (isset($_GET['pk_queryman'])){
	//-- Audit
	$grpName = mysql_query("SELECT group_name from et_groups WHERE pk_groups =".$_SESSION["FK_GROUPS"]);	
	$grpNameVal = mysql_fetch_array($grpName);
    
	
	$rowInfo = array(5,$_SESSION["user"],$grpNameVal['group_name'], "SYSDATE", "", "", "E","CSV download");
	auditQuery(5,$rowInfo);	
	
	$v_pk_queryman = $_GET['pk_queryman'];
	$recordSet = $db->Execute("select query from velink.vlnk_queryman where pk_queryman = ".$v_pk_queryman);
	$v_query =  $recordSet->fields[0];
	$v_html = '<TABLE WIDTH="100%" BORDER="1">';
	$v_name = urldecode($_GET["name"]);
	$rs = $db->Execute($v_query);
	header("Content-type: application/x-msexcel");
	header('Content-Disposition: attachment; filename="'.$v_name.'.csv"');
	header("Pragma: public");
	header("Expires: 0");
	print rs2csv($rs);
}
$db->Close();

}
else header("location: index.php?fail=1");
?>
