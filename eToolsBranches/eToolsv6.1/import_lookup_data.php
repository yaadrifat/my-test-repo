<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velos eTools -> Upload</title>
</head>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 


?>
<body>

<div id="fedora-content">	
<?php
if (isset($_POST["tablename"])) {
	$tablename = $_POST["tablename"];
} else {
	$delimiter = $_POST["delimiter"];
	$fileDir = $_POST["fileDir"];
}

$lookupName = str_replace("'","''",$_POST["lookupName"]);
$lookupVersion =  str_replace("'","''",$_POST["lookupVersion"]);
$lookupDesc =  str_replace("'","''",$_POST["lookupDesc"]);
$lookupType =  str_replace("'","''",$_POST["lookupType"]);

$colName = $_POST["colName"];
$dispName = $_POST["dispName"];
$keyword = $_POST["keyword"];
$colSeq = $_POST["colSeq"];
$colShow = $_POST["colShow"];
$colSearch = $_POST["colSearch"];
$colWidth = $_POST["colWidth"];
$colType = $_POST["colType"];


$results = executeOCIQuery("SELECT MAX(pk_lkplib)+1 as pk_lkplib FROM ER_LKPLIB",$ds_conn);
$pk_lkplib = $results["PK_LKPLIB"][0];
if ($pk_lkplib < 100000) {$pk_lkplib = 100000;}

$results = executeOCIQuery("SELECT MAX(pk_lkpview)+1 as pk_lkpview FROM ER_LKPVIEW",$ds_conn);
$pk_lkpview = $results["PK_LKPVIEW"][0];
if ($pk_lkpview < 100000) {$pk_lkpview = 100000;}

$results = executeOCIQuery("SELECT MAX(pk_lkpcol)+1 as pk_lkpcol FROM ER_LKPCOL",$ds_conn);
$pk_lkpcol = $results["PK_LKPCOL"][0];
if ($pk_lkpcol < 100000) {$pk_lkpcol = 100000;}

$results = executeOCIQuery("SELECT MAX(pk_lkpviewcol)+1 as pk_lkpviewcol FROM ER_LKPVIEWCOL",$ds_conn);
$pk_lkpviewcol = $results["PK_LKPVIEWCOL"][0];
if ($pk_lkpviewcol < 100000) {$pk_lkpviewcol = 100000;}

$sql = "insert into er_lkplib (pk_lkplib,lkptype_name,lkptype_version,lkptype_desc,lkptype_type) values
 ($pk_lkplib,'$lookupName','$lookupVersion','$lookupDesc','$lookupType')";

$results = executeOCIUpdateQuery($sql,$ds_conn);

$sql = "insert into er_lkpview (pk_lkpview,lkpview_name,lkpview_desc,fk_lkplib) values
 ($pk_lkpview,'$lookupName','$lookupDesc',$pk_lkplib)";

$results = executeOCIUpdateQuery($sql,$ds_conn);

$results = executeOCIQuery("SELECT MAX(pk_lkpcol)+1 as pk_lkpcol FROM ER_LKPCOL",$ds_conn);
$pk_lkpcol = $results["PK_LKPCOL"][0];
if ($pk_lkpcol < 100000) {$pk_lkpcol = 100000;}

$results = executeOCIQuery("SELECT MAX(pk_lkpviewcol)+1 as pk_lkpviewcol FROM ER_LKPVIEWCOL",$ds_conn);
$pk_lkpviewcol = $results["PK_LKPVIEWCOL"][0];
if ($pk_lkpviewcol < 100000) {$pk_lkpviewcol = 100000;}


for ($i = 0; $i < count($colName); $i++){

      $tabColName = 'custom'.str_pad(($i + 1),3,'0',STR_PAD_LEFT);
      
      if (isset($_POST["tablename"])) {
		  $sql = "insert into er_lkpcol (pk_lkpcol,fk_lkplib,lkpcol_name,lkpcol_dispval,lkpcol_datatype,lkpcol_len,lkpcol_table,lkpcol_keyword) values
	      ($pk_lkpcol,$pk_lkplib,'$colName[$i]','$dispName[$i]','$colType[$i]',null,'$tablename','$keyword[$i]')";
	  } else {
		  $sql = "insert into er_lkpcol (pk_lkpcol,fk_lkplib,lkpcol_name,lkpcol_dispval,lkpcol_datatype,lkpcol_len,lkpcol_table,lkpcol_keyword) values
	      ($pk_lkpcol,$pk_lkplib,'$tabColName','$dispName[$i]','$colType[$i]',null,'ER_LKPDATA','$keyword[$i]')";
	  }
      $results = executeOCIUpdateQuery($sql,$ds_conn);

      $sql = "insert into er_lkpviewcol (pk_lkpviewcol, fk_lkpcol, lkpview_is_search, lkpview_seq, fk_lkpview, lkpview_displen, lkpview_is_display) values
      ($pk_lkpviewcol, $pk_lkpcol, '$colSearch[$i]',$colSeq[$i],$pk_lkpview,'$colWidth[$i]%','$colShow[$i]') ";

      $results = executeOCIUpdateQuery($sql,$ds_conn);

      $pk_lkpcol++;
      $pk_lkpviewcol++;
}


if (!isset($_POST["tablename"])) {

	$handle = fopen($fileDir, "r");
	$row = 0;
	while (($data = fgetcsv($handle, 10000, $delimiter)) !== FALSE) {
	      $num = count($data);
	      $row++;

	      if ($row == 1) {
	         $colsql = "insert into er_lkpdata (pk_lkpdata, fk_lkplib,";
	         for ($c=0; $c < $num; $c++) {
	             $colsql .= 'custom'.str_pad(($c + 1),3,'0',STR_PAD_LEFT).",";
	         }
	         $colsql = substr($colsql,0,strlen($colsql)-1).") values (";
	       } else {
	         $valsql = "seq_er_lkpdata.nextval,$pk_lkplib,";
	         for ($c=0; $c < $num; $c++) {
	             $dataval = str_replace("'","''",$data[$c]);
	             $valsql .= "'$dataval',";
	         }
	         $valsql = substr($valsql,0,strlen($valsql)-1).")";
	         $sql = $colsql.$valsql;

	         $results = executeOCIUpdateQuery($sql,$ds_conn);

	       }

	}
	echo "Total rows imports: ".$row;
} else {
	echo "Lookup imported";
}
ocilogoff($ds_conn);
?>
<meta http-equiv="refresh" content="0; url=./lookup.php">
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
