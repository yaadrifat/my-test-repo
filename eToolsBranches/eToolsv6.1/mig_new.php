<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Migrate Data</title>

<script> 
function validate() {
	if (document.migration.module.value == ""){
		alert("Please select a module for migration.");
		document.migration.module.focus();
		return false;
	}
	if (document.migration.TABLE.value == ""){
		alert("Please select a table for migration.");
		document.migration.TABLE.focus();
		return false;
	}
}
</script>	
	
<script> 
var tables = new Array();
</script>	
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
$query="select table_name as tables from dba_tables where owner in ('ERES','ESCH','EPAT') 
order by 1
";
$results = executeOCIQuery($query,$ds_conn);
for ($rec = 0; $rec < $results_nrows; $rec++){
?>
		<script>tables[<?PHP echo $rec; ?>] = <?PHP echo '"'.$results["TABLES"][$rec].'"';?>;</script>
<?PHP
	}
?>

<script language="javascript" type="text/javascript">
	function refreshlist(table){
	
		table=table.toUpperCase()
		var length=table.length;
		count = tables.length;
		var tlist = "";
		for (var i=0;i<count;i++){
			if (table == tables[i].substring(0,length)){
				tlist = tlist + '<tr onMouseOver="bgColor=\'#a4bef1\'" onMouseOut="bgColor=\'#FFFFFF\'">'+"<td ondblclick='document.migration.TABLE.value = this.innerHTML'>" + tables[i] + "</td></tr>";
			}
		}
		
		
		 
		tlist = (tlist.length == 0) ? tlist="No tables found":"<table border='1'>"+tlist+"</table>";

		
		document.getElementById('tables').innerHTML = tlist;
	}
      
</script>

</head>
<body>
<div id="fedora-content">	
<div class="navigate">Migrate Data - Standard - New Migration</div>
<?php


if ($_SERVER['REQUEST_METHOD'] != 'POST') {
$v_adaptor = $_GET["pk_vlnk_adaptor"];

$query_sql = "select pk_vlnk_module,module_name from velink.vlnk_module where module_type = 'I' order by module_name";
$results = executeOCIQuery($query_sql,$ds_conn);

$dd_modules = '<option value="">Select an option</option>';
for ($rec = 0; $rec < $results_nrows; $rec++){
           $dd_modules .= "<option value=".$results["PK_VLNK_MODULE"][$rec].">".$results["MODULE_NAME"][$rec]."</option>";
}


?>

<FORM name="migration" action="mig_new.php" method="post" onSubmit="if (validate() == false) return false;">
<table border="0">
<tr><td valign="top">
<input type="hidden" name="pk_adaptor" value="<?php echo $v_adaptor; ?>"></input>
<br>
<TABLE width="100%" border="0">
<TR><TD>Migration Name</TD><TD><INPUT TYPE="TEXT" NAME="mname" id="mname" size="50" ></INPUT></TD></TR>
<TR><TD width="25%">Module Name</TD><TD><?PHP echo "<select class=required name=\"module\">".$dd_modules."</select>"; ?></TD></TR>
<TR><TD>Table Name</TD><TD><INPUT TYPE="TEXT" class="required" NAME="TABLE" size=50 onKeyUp="refreshlist(document.migration.TABLE.value);"></INPUT></TD></TR>
</TABLE>
<BR><input type="image" name="submit" value="SUBMIT" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />

</td><td height="200px">

<style>DIV.tables {
height:100%;
overflow: auto;
align:left;
}</style>

<div class="tables" id="tables"></div>
</td></tr>
</table>
</form>

<script type="text/javascript">
	document.getElementById("mname").focus();
</script>

<?php 
} else {


$query = "insert into velink.vlnk_adapmod (pk_vlnk_adapmod,fk_vlnk_adaptor,fk_vlnk_module,table_name,adapmod_name) values (velink.seq_vlnk_adapmod.nextval,".$_POST["pk_adaptor"].",".$_POST["module"].",trim('".$_POST['TABLE']."'),trim('".$_POST['mname']."'))";
$results = executeOCIUpdateQuery($query,$ds_conn);
OCICommit($ds_conn);
OCILogoff($ds_conn);

echo "Data Saved Successfully !!!";

$url = "./mig_modules.php?pk_vlnk_adaptor=".$_POST['pk_adaptor'];
echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";
?>
<?PHP
}

?>
</div>

</body>
</html>


<?php
}
else header("location: index.php?fail=1");
?>
		
