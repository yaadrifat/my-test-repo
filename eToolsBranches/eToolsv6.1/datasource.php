<?php
session_start();
header("Cache-control: private");
if(@$_SESSION["user"]){
?>
<html>
<head>
    <title>Velos eTools -> Select Datasource</title>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");
?>
</head>
<body>
<div id="fedora-content">
<div class="navigate">Select Datasource</div>
<br>	
<?PHP
$grp_result = mysql_query("SELECT ds_rights from et_groups where pk_groups=".$_SESSION['FK_GROUPS']);
$dropdown = '';
$flag = 'false';
if(mysql_num_rows($grp_result) > 0) {
    while($row = mysql_fetch_array($grp_result)){
        $v_ds_rights = explode("|",$row['ds_rights']);
        foreach ($v_ds_rights as $v_ds){            
            list($pk_ds,$ds_access) = explode(":",$v_ds);            
            if($ds_access == 1){
            $flag = 'true';
            $rs = mysql_query("SELECT pk_ds, ds_name from et_ds where pk_ds=".$pk_ds);
                while($rsrow = mysql_fetch_array($rs)) {                     
                    $dropdown .= "<option value=".$rsrow["pk_ds"].">".$rsrow["ds_name"]."</option>";
                }
            }            
        }        
    } 
}

?>
<form name="datasource" method=post action="loggedinusers.php">
<?php 
    if($_SESSION['FK_GROUPS'] == "1"){	
    $ds_result = mysql_query("SELECT * from et_ds");
    if (mysql_num_rows($ds_result)== 0) { 
        ?>
        <table width="415">
        <tr>
        <td width="5%">&nbsp;</td>
        <td width="56%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="datasource_edit.php?mode=n">Create Datasource</a></td>
        </tr>
        </table>
<?php 
    }elseif($flag != 'false'){
?>
        <table width="415">
        <tr>
        <td width="5%">&nbsp;</td>
        <td width="13%">Datasource:&nbsp;&nbsp;</td>
        <td width="11%"><select name="DS"><?PHP echo $dropdown; ?></select></td>
        <td width="15%"><img src="./img/submit.png" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" onClick="document.datasource.submit()"/></td>
        </tr>
        </table>
        <?php
    }else{
        ?>
        <table width="415">
        <tr>
        <td width="5%">&nbsp;</td>
        <td width="56%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="datasource_dash.php">Enable Datasource</a></td>
        </tr>
        </table>
    <?php } ?>
<?php 
// this is for non admin users
	}else{
            if($flag != 'false'){
?>
                <table>
                    <tr>
                        <td width="5%">&nbsp;</td>
                        <td>Datasource:&nbsp;&nbsp;</td>
                        <td><select name="DS"><?PHP echo $dropdown; ?></select></td>
                        <td>&nbsp;&nbsp;<img src="./img/submit.png" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" onClick="document.datasource.submit()"/></td>
                    </tr>
                </table>
<?php
	}else{
?>
                <table width="415">
                    <tr>
                        <td width="5%">&nbsp;</td>
                        <td width="56%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="datasource_edit.php?mode=n">Create Datasource</a></td>
                    </tr>
                </table>            
<?php        }
        }
?>
</form>

<?php

        $dss_result = mysql_query("SELECT * from et_ds order by pk_ds");	

	if (mysql_num_rows($dss_result) == 0) { ?>
		<div class="warning" style="margin-left: 0.5in; margin-right: 0.5in;">
		<table border="0" >
		<tr>
		<th align="left">Note</th>
		</tr>
		<tr><td align="left" valign="top">
                    <p>Currently you dont have any datasource. To create a new datasource click the link "Create Datasource"</p></td>
		</tr>
		</table>
		</div>
	<?php
	}elseif($flag=="true"){?>
		<div class="warning" style="margin-left: 0.5in; margin-right: 0.5in;">
		<table border="0" >
		<tr>
		<th align="left">Note</th>
		</tr>
		<tr><td align="left" valign="top">
                    <p>Please select a datasource before clicking on any menu item.</p></td>
		</tr>
		</table>
		</div>	
	<?php
	}else{
?>
		<div class="warning" style="margin-left: 0.5in; margin-right: 0.5in;">
		<table border="0" >
		<tr>
		<th align="left">Note</th>
		</tr>
		<tr><td align="left" valign="top">
			<p>All your datasources are disabled. Click the link "Enable Datasource" to enable the datasources</p></td>
		</tr>
		</table>
		</div>
<?php } ?>
      </div>
</body>
</html>
<?php
}
else header("location: index.php?fail=1");
?>
