<?php
session_start();
header("Cache-control: private");

//if(isset($_GET['action'])){
    if ($_GET['action'] == 'CLIPBOARD' && $_GET['table'] != 'undefined') {
        include("./includes/oci_functions.php");    
        $ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]);    
        require_once('audit_queries.php');    
        include("db_config.php");
        //$db = new Database("etools");

        $act = $_GET['action'];
        $tbl = $_GET['table'];        
        //$v_tablestr = strtoupper($_GET['table']);
        $v_tablestr = strtoupper($tbl);  
        
        list($v_owner,$v_table) = explode(".",$v_tablestr);
        $query="select column_name,data_type,data_length from dba_tab_cols where owner = '$v_owner' and table_name = '$v_table' order by column_id";
        $results = executeOCIQuery($query,$ds_conn);
        $v_html = "";
        for ($rec = 0; $rec < $results_nrows; $rec++){
                $v_html .= $results["COLUMN_NAME"][$rec].", ";
        }
        $v_html = (strlen($v_html) > 0 ? substr($v_html,0,strlen($v_html)-2)  : "");
        print trim($v_html);

        /*--- AUDIT ---*/
        $colArray = array('CLIPBOARD');
        $colvalarray = array($v_html);
        $tblname = '';
        colQueries($colArray, $colvalarray, $tblname);
        /*--- END ---*/	
    }    
//}

if (isset($_GET['table']) and $_GET['action'] != 'CLIPBOARD'){
    include("./includes/oci_functions.php");    
    $ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]);    
    $v_tablestr = strtoupper($_GET['table']);
    list($v_owner,$v_table) = explode(".",$v_tablestr);
    $query="select column_name,data_type,data_length from dba_tab_cols where owner = '$v_owner' and table_name = '$v_table' order by column_id";
    $results = executeOCIQuery($query,$ds_conn);
//
    $v_html = "";
    for ($rec = 0; $rec < $results_nrows; $rec++){
            $v_html .= '<table border="1" width="100%"><tr onMouseOver="bgColor=\'#a4bef1\'" onMouseOut="bgColor=\'#FFFFFF\'">'."<td onclick='document.getElementById(\"clipboard\").value = document.getElementById(\"clipboard\").value + this.innerHTML + \", \"'  width='50%'>".$results["COLUMN_NAME"][$rec]."</td><td  width='30%'>".$results["DATA_TYPE"][$rec]."</td><td  width='15%'>".$results["DATA_LENGTH"][$rec]."</td></tr></table>";
    }
    if ($results_nrows == 0) $v_html .= "No tables found";
    $v_html .= "</table>";
    print $v_html;
}
?>