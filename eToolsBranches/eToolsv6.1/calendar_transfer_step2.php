<?php
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Calendar Transfer</title>

<?php
include("db_config.php");
include("./includes/header.php");
include_once "./adodb/adodb.inc.php";
include('gen_functions.php');
$source_db = NewADOConnection("oci8");
$source_db->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);

?>

 <SCRIPT language="JavaScript">
	function submitcal()
		{
			document.caltransfer.submit();
		}

	function validate(){
		if (document.getElementById("calname").value == "") {
			alert("Calendar Name cannot be blank");
			return false;
		}
	}
</SCRIPT>
      <script language="javascript" type="text/javascript">
      function getHTTPObject(){
      if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
      else if (window.XMLHttpRequest) return new XMLHttpRequest();
      else {
      alert("Your browser does not support AJAX.");
      return null;
      }
      }

      function checkExists(datasource){
            httpObject = getHTTPObject();
            if (httpObject != null){
                //alert(document.getElementById('category').value);
                httpObject.open("GET", "calendar_val.php?key="+datasource+"&calname="+document.getElementById('calname').value+"&category="+document.getElementById('category').value, false);
                httpObject.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
                httpObject.setRequestHeader("Cache-Control", "no-cache");
                httpObject.send(null);
                if(httpObject.status != 200){
                    document.write("404 not found");
                }
                var finalString = httpObject.responseText;
                finalString = finalString.replace(/^\s*/, '').replace(/\s*$/, '');
                console.log("*** "+finalString);
                
                if(finalString == "-1"){
                    document.getElementById('check').innerHTML = "<font color=red>Calendar name already exists.</font>";
                    return false;                   
                }else{
                    return true;
                }
            }
      }
      
      function refreshCategory(){
            document.getElementById("categoryspan").innerHTML='<select name="category" id="category"><option selected value=0>Processing...</option></select>';
            var datasource = document.caltransfer.DS.value;
            httpObject = getHTTPObject();
            if (httpObject != null) {
                httpObject.open("GET", "calendar_val.php?key="+datasource+"&refresh=category", false);
                httpObject.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
                httpObject.setRequestHeader("Cache-Control", "no-cache");
                httpObject.send(null);
                if(httpObject.status != 200){
                    document.write("404 not found");
                }
                var finalString = httpObject.responseText;
                if (finalString != "") {
                    document.getElementById("categoryspan").innerHTML='<select class="required" name="category" id="category">'+finalString+'</select>';
                }
            }
     }      
      var httpObject = null;
      </script>
</head>
<body onload="refreshCategory();">
<div id="fedora-content">	
<div class="navigate">Calendar Transfer</div>
<?php


$cat_dropdown = '';

$v_calname = (isset($_GET["calname"])) ?  $_GET["calname"] : $_POST["calname"];
$v_calid = (isset($_GET["calid"])) ?  $_GET["calid"] : $_POST["calid"];
$s_av = (isset($_GET["av"])) ?  $_GET["av"] : $_POST["av"];

if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
$result1 = mysql_query("SELECT ds_rights from et_groups where pk_groups=".$_SESSION['FK_GROUPS']);
$dropdown = '';
$counter=0;
if (mysql_num_rows($result1) > 0) {
     while ($data = mysql_fetch_array($result1)) {
	$v_ds_rights = explode("|",$data['ds_rights']);
	foreach ($v_ds_rights as $v_ds){
		list($pk_ds,$ds_access) = explode(":",$v_ds);
		if ($ds_access == 1) {
			if ($counter == 0){
				if (isset($_POST["DS"])){
					$ds = $_POST['DS'];
				} else {
					$ds = $pk_ds;
				}
				$counter++;
		 	}
                        $rs2 = mysql_query("SELECT pk_ds, ds_name from et_ds where pk_ds=".$pk_ds);
                         while ($ds_data = mysql_fetch_array($rs2)) {
				if ($ds == $ds_data["pk_ds"]){
			        $dropdown .= "<option selected value=".$ds_data["pk_ds"].">".$ds_data["ds_name"]."</option>";
		        } else {
			        $dropdown .= "<option value=".$ds_data["pk_ds"].">".$ds_data["ds_name"]."</option>";
		    	}
			}
		}
	}
} 
} 

        $rs3 = mysql_query("SELECT  pk_ds, ds_name, ds_host, ds_port, ds_sid, ds_pass from et_ds where pk_ds =".$ds);
        while ($ds_datars = mysql_fetch_array($rs3)) {
		//$connect_string = '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST='.$ds_data["ds_host"].')(PORT='.$ds_data["ds_port"].'))(CONNECT_DATA=(SID='.$ds_data["ds_sid"].')))';
                $connect_string = '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST='.$ds_data["ds_host"].')(PORT='.$ds_data["ds_port"].'))(CONNECT_DATA=(SERVICE_NAME='.$ds_data["ds_sid"].')))';
                $cdspass = my_decrypt(trim($ds_data["ds_pass"]), $_SESSION['fnkey']);

                //$cdspass = $ds_data["ds_pass"];
                //$cdspass = $ds_data["ds_pass"];
	}
	$target_db = NewADOConnection("oci8");
	$target_db->Connect($connect_string, "eres", $cdspass);
	$v_sql = "select pk_catlib || '|' || fk_account as pk_catlib,catlib_name || decode(ac_name,null,'',' [' || ac_name || ']') as catlib_name from er_catlib,er_account WHERE fk_account <> 46 and pk_account = fk_account and catlib_type = 'L' order by ac_name || ' - ' || catlib_name";
	$rs4 = $target_db->Execute($v_sql);
	if ($rs4) {
		$cat_dropdown = '';
		while (!$rs4->EOF) {
			$cat_dropdown .= "<option value=".$rs4->fields["PK_CATLIB"].">".$rs4->fields["CATLIB_NAME"]."</option>";
			$rs4->MoveNext();
		}
        }

?>
<FORM name="caltransfer" action="calendar_transfer_step2.php" method="post" onSubmit="if(checkExists(document.caltransfer.DS.value)==false) return false; if(validate()==false) return false;">
<input type="hidden" name="calid" value="<?php echo $v_calid; ?>"></input>
<input type="hidden" name="account" value="<?php echo $v_account; ?>"></input>
<input type="hidden" name="av" value="<?php echo $s_av; ?>"></input>


<TABLE width="100%" border="1">
    <TR><TD width="20%">Calendar Name</TD><TD><input class="required" size="50" type=text name="calname" id="calname" value="<?PHP echo $v_calname ?>"><div id="check"><font color="red"></font></div></TD></TR>
    <TR><TD>Transfer Calendar to </TD><TD><select class="required" name="DS" onChange="refreshCategory();"><?PHP echo $dropdown; ?></select></TD></TR>
    <TR><TD>Category</TD><TD><span id="categoryspan"><select class="required" name="category" id="category"><?PHP echo $cat_dropdown; ?></select></span></TD></TR>
</TABLE>
	<BR><input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />
	
	<div class="note" style="width:600px;">
		<table summary="Note: Note">
		<tr>
		<th align="left">Note</th>
		</tr>
		<tr><td align="left" valign="top">
		<p>Event's appendix and resource attributes will not be transferred to the Datasource</p>
		</td></tr>
		</table></div> 
<?PHP

?>
</FORM>
<?php 
}else{
$v_calid = $_POST["calid"];
$v_calname = $_POST["calname"];
$s_av = $_POST["av"];

list($v_catlib,$v_account) = explode("|",$_POST["category"]);

$ds = $_POST['DS'];
$rs5 = mysql_query("SELECT pk_ds, ds_name, ds_host, ds_port, ds_sid, ds_pass from et_ds where pk_ds=".$ds);
while ($ds_data = mysql_fetch_array($rs5)) {
    
	//$connect_string = '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST='.$ds_data["ds_host"].')(PORT='.$ds_data["ds_port"].'))(CONNECT_DATA=(SID='.$ds_data["ds_sid"].')))';
        $connect_string = '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST='.$ds_data["ds_host"].')(PORT='.$ds_data["ds_port"].'))(CONNECT_DATA=(SERVICE_NAME='.$ds_data["ds_sid"].')))';
        $dpppas = my_decrypt(trim($ds_data["ds_pass"]), $_SESSION['fnkey']);        
        //$dpppas = $ds_data["ds_pass"];
}


$target_db = NewADOConnection("oci8");
$target_db->Connect($connect_string, "eres", $dpppas );

$rSet = $target_db->Execute("select ctrl_value from er_ctrltab where ctrl_key='app_version'");
$d_av =  $rSet->fields[0];


if($s_av == substr($d_av,0,strlen($s_av))){

    $v_sql = "select EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,NOTES,COST,COST_DESCRIPTION,DURATION,
    USER_ID,FK_CATLIB,FUZZY_PERIOD,FK_CODELST_CALSTAT,DESCRIPTION,DISPLACEMENT,ORG_ID,
    nvl(EVENT_FLAG,0) as event_flag,PAT_DAYSBEFORE,PAT_DAYSAFTER,USR_DAYSBEFORE,USR_DAYSAFTER,
    PAT_MSGBEFORE,PAT_MSGAFTER,USR_MSGBEFORE,USR_MSGAFTER,CALENDAR_SHAREDWITH, 
    DURATION_UNIT,EVENT_FUZZYAFTER,EVENT_DURATIONAFTER,EVENT_CPTCODE,EVENT_DURATIONBEFORE,
    EVENT_CATEGORY,EVENT_SEQUENCE,FK_VISIT, EVENT_LIBRARY_TYPE, EVENT_LINE_CATEGORY, SERVICE_SITE_ID, FACILITY_ID, FK_CODELST_COVERTYPE, REASON_FOR_COVERAGECHANGE, COVERAGE_NOTES
    from event_def
    where event_id = $v_calid
    ";

    $rs6 = $source_db->Execute($v_sql);

    $target_db->BeginTrans();
    $v_error = "";
    $v_status = true;
    if (!$rs6){ 
            print $source_db->ErrorMsg();
    }else {
            // Insert the protocol row in event_def
            $recordSet = $target_db->Execute("select event_definition_seq.nextval from dual");
            $v_protocolid =  $recordSet->fields[0];

            $pk_calStat_q = $target_db->Execute("select pk_codelst from sch_codelst where codelst_type = 'calStatLib'  and codelst_subtyp = 'W'",$ds_conn);
            $pk_calStat =  $pk_calStat_q->fields[0];

            $v_sql = "insert into event_def (EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,NOTES,COST,COST_DESCRIPTION,DURATION,
            USER_ID,FK_CATLIB,FUZZY_PERIOD,FK_CODELST_CALSTAT,DESCRIPTION,DISPLACEMENT,ORG_ID,
            EVENT_FLAG,PAT_DAYSBEFORE,PAT_DAYSAFTER,USR_DAYSBEFORE,USR_DAYSAFTER,
            PAT_MSGBEFORE,PAT_MSGAFTER,USR_MSGBEFORE,USR_MSGAFTER,CALENDAR_SHAREDWITH, 
            DURATION_UNIT,EVENT_FUZZYAFTER,EVENT_DURATIONAFTER,EVENT_CPTCODE,EVENT_DURATIONBEFORE,
            EVENT_CATEGORY,EVENT_SEQUENCE,FK_VISIT, EVENT_LIBRARY_TYPE, EVENT_LINE_CATEGORY, SERVICE_SITE_ID, FACILITY_ID, FK_CODELST_COVERTYPE, REASON_FOR_COVERAGECHANGE, COVERAGE_NOTES) values (
            $v_protocolid,$v_protocolid,'P','".$v_calname."','".$rs6->fields["NOTES"]."',".$rs6->fields["COST"].",'".$rs6->fields["COST_DESCRIPTION"]."',".$rs6->fields["DURATION"].",
            $v_account,$v_catlib,'".$rs6->fields["FUZZY_PERIOD"]."',$pk_calStat,'".$rs6->fields["DESCRIPTION"]."','".$rs6->fields["DISPLACEMENT"]."',0,".
            $rs6->fields["EVENT_FLAG"].",'".$rs6->fields["PAT_DAYSBEFORE"]."','".$rs6->fields["PAT_DAYSAFTER"]."','".$rs6->fields["USR_DAYSBEFORE"]."','".$rs6->fields["USR_DAYSAFTER"]."','".
            $rs6->fields["PAT_MSGBEFORE"]."','".$rs6->fields["PAT_MSGAFTER"]."','".$rs6->fields["USR_MSGBEFORE"]."','".$rs6->fields["USR_MSGAFTER"]."','A','".
            $rs6->fields["DURATION_UNIT"]."','".$rs6->fields["EVENT_FUZZYAFTER"]."','".$rs6->fields["EVENT_DURATIONAFTER"]."','".$rs6->fields["EVENT_CPTCODE"]."','".$rs6->fields["EVENT_DURATIONBEFORE"]."','".
            $rs6->fields["EVENT_CATEGORY"]."',".((strlen($rs6->fields["EVENT_SEQUENCE"]) == 0) ? '0':$rs6->fields["EVENT_SEQUENCE"]).",null,'".$rs6->fields["EVENT_LIBRARY_TYPE"]."', '".$rs6->fields["EVENT_LINE_CATEGORY"]."', '', '', '".$rs6->fields["FK_CODELST_COVERTYPE"]."', '".$rs6->fields["REASON_FOR_COVERAGECHANGE"]."', '".$rs6->fields["COVERAGE_NOTES"]."')";
            ;

            $recordSet = $target_db->Execute("select event_definition_seq.currval from dual");
            $v_currentCalID =  $recordSet->fields[0];

            if ($v_status) {
                    $v_status = $target_db->Execute($v_sql);
                    $v_error = (!$v_status) ? $target_db->ErrorMsg()." EVENT_DEF (Protocol record)" : ""; 
            }

            $v_sql = "select PK_PROTOCOL_VISIT,FK_PROTOCOL,VISIT_NO,VISIT_NAME,
                            DESCRIPTION,DISPLACEMENT,NUM_MONTHS,NUM_WEEKS,
                            NUM_DAYS,INSERT_AFTER,INSERT_AFTER_INTERVAL,INSERT_AFTER_INTERVAL_UNIT, 
                            PROTOCOL_TYPE,VISIT_TYPE,OFFLINE_FLAG,HIDE_FLAG, NO_INTERVAL_FLAG, WIN_BEFORE_NUMBER, WIN_BEFORE_UNIT, WIN_AFTER_NUMBER, WIN_AFTER_UNIT
                            from sch_protocol_visit
                            where fk_protocol = $v_calid order by displacement
                            ";
            $rs7 = $source_db->Execute($v_sql);

            $v_counter = 0;
            if (!$rs7) 
                    print $source_db->ErrorMsg();
            else {
                    while (!$rs7->EOF) {

                            $recordSet = $target_db->Execute("select sch_prot_visit_seq.nextval from dual");
                            $v_visitid =  $recordSet->fields[0];
                            $v_visitid_src[$v_counter] = $rs7->fields["PK_PROTOCOL_VISIT"];
                            $v_visitid_tar[$v_counter] = $v_visitid;
                            if ($rs7->fields["INSERT_AFTER"] == "0") {
                                    $v_visitpk = '0';
                            } else {
                                    $v_visitcount = count($v_visitid_src);
                                    for ($i=0;$i<$v_visitcount;$i++){
                                            if ($rs7->fields["INSERT_AFTER"] == $v_visitid_src[$i]) {
                                                    $v_visitpk = $v_visitid_tar[$i];
                                                    break;
                                            }
                                    }
                            }
                            //fix for the bug 6527
                            if ($rs7->fields["NO_INTERVAL_FLAG"] == '') {
                                    $rs7->fields["NO_INTERVAL_FLAG"] = '0';
                            }

                            if ($rs7->fields["WIN_BEFORE_NUMBER"] == '') {
                                    $rs7->fields["WIN_BEFORE_NUMBER"] = '0';
                            }

                            if ($rs7->fields["WIN_BEFORE_UNIT"] == '') {
                                    $rs7->fields["WIN_BEFORE_UNIT"] = 'D';
                            }

                            if ($rs7->fields["WIN_AFTER_NUMBER"] == '') {
                                    $rs7->fields["WIN_AFTER_NUMBER"] = '0';
                            }

                            if ($rs7->fields["WIN_AFTER_UNIT"] == '') {
                                    $rs7->fields["WIN_AFTER_UNIT"] = 'D';
                            }			

                            $v_sql = "insert into sch_protocol_visit (PK_PROTOCOL_VISIT,FK_PROTOCOL,VISIT_NO,VISIT_NAME,
                                    DESCRIPTION,DISPLACEMENT,NUM_MONTHS,NUM_WEEKS,NUM_DAYS,INSERT_AFTER,INSERT_AFTER_INTERVAL,INSERT_AFTER_INTERVAL_UNIT, 
                                    PROTOCOL_TYPE,VISIT_TYPE,OFFLINE_FLAG,HIDE_FLAG, NO_INTERVAL_FLAG, WIN_BEFORE_NUMBER, WIN_BEFORE_UNIT, WIN_AFTER_NUMBER, WIN_AFTER_UNIT) values (
                            $v_visitid,$v_protocolid,".$rs7->fields["VISIT_NO"].",'".$rs7->fields["VISIT_NAME"]."','".
                            $rs7->fields["DESCRIPTION"]."','".$rs7->fields["DISPLACEMENT"]."',".$rs7->fields["NUM_MONTHS"].",".$rs7->fields["NUM_WEEKS"].",'".
                            $rs7->fields["NUM_DAYS"]."','".$v_visitpk."','".$rs7->fields["INSERT_AFTER_INTERVAL"]."','".$rs7->fields["INSERT_AFTER_INTERVAL_UNIT"]."','".
                            $rs7->fields["PROTOCOL_TYPE"]."','".$rs7->fields["VISIT_TYPE"]."',null,null,'".$rs7->fields["NO_INTERVAL_FLAG"]."', '".$rs7->fields["WIN_BEFORE_NUMBER"]."', '".$rs7->fields["WIN_BEFORE_UNIT"]."', '".$rs7->fields["WIN_AFTER_NUMBER"]."', '".$rs7->fields["WIN_AFTER_UNIT"]."')
                            ";

                            if ($v_status) {
                                    $v_status = $target_db->Execute($v_sql);

                                    $v_error = (!$v_status) ? $target_db->ErrorMsg()." SCH_PROTOCOL_VISIT " : ""; 
                            }

                            $v_counter = $v_counter + 1;
                            $rs7->MoveNext();
                    }
            }



            $v_sql = "select EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,NOTES,COST,COST_DESCRIPTION,DURATION,USER_ID,FK_CATLIB,FUZZY_PERIOD,STATUS,DESCRIPTION,DISPLACEMENT,ORG_ID,nvl(EVENT_FLAG,0) as event_flag,PAT_DAYSBEFORE,PAT_DAYSAFTER,USR_DAYSBEFORE,USR_DAYSAFTER,PAT_MSGBEFORE,PAT_MSGAFTER,USR_MSGBEFORE,USR_MSGAFTER,CALENDAR_SHAREDWITH,DURATION_UNIT,EVENT_FUZZYAFTER,EVENT_DURATIONAFTER,EVENT_CPTCODE,EVENT_DURATIONBEFORE,		EVENT_CATEGORY,EVENT_SEQUENCE,FK_VISIT, EVENT_LIBRARY_TYPE, EVENT_LINE_CATEGORY, SERVICE_SITE_ID, FACILITY_ID, FK_CODELST_COVERTYPE, REASON_FOR_COVERAGECHANGE, COVERAGE_NOTES from event_def where chain_id = $v_calid and event_type = 'A'";

            $rs8 = $source_db->Execute($v_sql);
            $v_counter = 0;

            if (!$rs8) 
                    print $source_db->ErrorMsg();
            else {
                    while (!$rs8->EOF) {

                    if (strlen($rs8->fields["FK_VISIT"]) == 0) {
                            $v_visitpk = 'null';
                    } else {
                            $v_visitcount = count($v_visitid_src);
                            for ($i=0;$i<$v_visitcount;$i++){
                                    if ($rs8->fields["FK_VISIT"] == $v_visitid_src[$i]) {
                                            $v_visitpk = $v_visitid_tar[$i];
                                            break;
                                    }
                            }
                    }

                    $recordSet = $target_db->Execute("select event_definition_seq.nextval from dual");
                    $v_eventid =  $recordSet->fields[0];
                    $v_source_eventid = $rs8->fields["EVENT_ID"];
                    // insert into event_def 
                    $v_sql = "insert into event_def (EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,NOTES,COST,COST_DESCRIPTION,DURATION,
                    USER_ID,FK_CATLIB,FUZZY_PERIOD,STATUS,DESCRIPTION,DISPLACEMENT,ORG_ID,
                    EVENT_FLAG,PAT_DAYSBEFORE,PAT_DAYSAFTER,USR_DAYSBEFORE,USR_DAYSAFTER,
                    PAT_MSGBEFORE,PAT_MSGAFTER,USR_MSGBEFORE,USR_MSGAFTER,CALENDAR_SHAREDWITH, 
                    DURATION_UNIT,EVENT_FUZZYAFTER,EVENT_DURATIONAFTER,EVENT_CPTCODE,EVENT_DURATIONBEFORE,
                    EVENT_CATEGORY,EVENT_SEQUENCE,FK_VISIT, EVENT_LIBRARY_TYPE, EVENT_LINE_CATEGORY, SERVICE_SITE_ID, FACILITY_ID, FK_CODELST_COVERTYPE, REASON_FOR_COVERAGECHANGE, COVERAGE_NOTES) values (
                    $v_eventid,$v_protocolid,'A','".$rs8->fields["NAME"]."','".$rs8->fields["NOTES"]."',".$rs8->fields["COST"].",'".$rs8->fields["COST_DESCRIPTION"]."',".$rs8->fields["DURATION"].",
                    $v_account,$v_catlib,'".$rs8->fields["FUZZY_PERIOD"]."','".$rs8->fields["STATUS"]."','".$rs8->fields["DESCRIPTION"]."','".$rs8->fields["DISPLACEMENT"]."',0,".
                    $rs8->fields["EVENT_FLAG"].",'".$rs8->fields["PAT_DAYSBEFORE"]."','".$rs8->fields["PAT_DAYSAFTER"]."','".$rs8->fields["USR_DAYSBEFORE"]."','".$rs8->fields["USR_DAYSAFTER"]."','".
                    $rs8->fields["PAT_MSGBEFORE"]."','".$rs8->fields["PAT_MSGAFTER"]."','".$rs8->fields["USR_MSGBEFORE"]."','".$rs8->fields["USR_MSGAFTER"]."','A','".
                    $rs8->fields["DURATION_UNIT"]."','".$rs8->fields["EVENT_FUZZYAFTER"]."','".$rs8->fields["EVENT_DURATIONAFTER"]."','".$rs8->fields["EVENT_CPTCODE"]."','".$rs8->fields["EVENT_DURATIONBEFORE"]."','".
                    $rs8->fields["EVENT_CATEGORY"]."',".((strlen($rs8->fields["EVENT_SEQUENCE"]) == 0) ? '0':$rs8->fields["EVENT_SEQUENCE"]).",$v_visitpk, '".$rs8->fields["EVENT_LIBRARY_TYPE"]."', '".$rs8->fields["EVENT_LINE_CATEGORY"]."', '', '', '".$rs8->fields["FK_CODELST_COVERTYPE"]."', '".$rs8->fields["REASON_FOR_COVERAGECHANGE"]."', '".$rs8->fields["COVERAGE_NOTES"]."')";
                    ;
                    if ($v_status) {
                            $v_status = $target_db->Execute($v_sql);
                            $v_error = (!$v_status) ? $target_db->ErrorMsg()." EVENT_DEF (Event record: displacement=0)" : ""; 
                    }
                    // Event cost
                    $v_sql = "select EVENTCOST_VALUE,FK_EVENT,(select codelst_desc from sch_codelst where pk_codelst = FK_COST_DESC) as cost_desc,
                            (select codelst_desc from sch_codelst where pk_codelst = FK_CURRENCY) as currency from sch_eventcost where fk_event = ".$v_source_eventid;

                    $rs9 = $source_db->Execute($v_sql);
                    if (!$rs9) 
                            print $source_db->ErrorMsg();
                    else {
                            while (!$rs9->EOF) {
                                    $recordSet = $target_db->Execute("select pk_codelst from sch_codelst where lower(trim(codelst_type)) = 'cost_desc' and lower(trim(codelst_desc)) = lower(trim('".$rs9->fields["COST_DESC"]."'))");
                                    (!isset($recordSet->fields[0])) ? $v_costid = "": $v_costid = $recordSet->fields[0];
                                    $recordSet = $target_db->Execute("select pk_codelst from sch_codelst where lower(trim(codelst_type)) = 'currency' and lower(trim(codelst_desc)) = lower(trim('".$rs9->fields["CURRENCY"]."'))");
                                    (!isset($recordSet->fields[0])) ? $v_currencyid = "": $v_currencyid = $recordSet->fields[0];
                                    $recordSet = $target_db->Execute("select sch_eventcost_seq.nextval from dual");
                                    $v_eventcostid =  $recordSet->fields[0];
                                    $v_sql = "insert into sch_eventcost (PK_EVENTCOST,EVENTCOST_VALUE,FK_EVENT,FK_COST_DESC,FK_CURRENCY,PROPAGATE_FROM) values (
                                    $v_eventcostid,".$rs9->fields["EVENTCOST_VALUE"].",$v_eventid,$v_costid,$v_currencyid,$v_eventcostid)";

                                    if ($v_status && strlen($v_costid) > 0 && strlen($v_currencyid) > 0) {
                                            $v_status = $target_db->Execute($v_sql);
                                            $v_error = (!$v_status) ? $target_db->ErrorMsg()." SCH_EVENTCOST (Event record: displacement=1)" : ""; 
                                    }
                                    $rs9->MoveNext();
                            }
                    }
                    // Event crf	
                    $v_sql = "select CRFLIB_NUMBER,CRFLIB_NAME,CRFLIB_FLAG,CRFLIB_FORMFLAG from sch_crflib where fk_events = ".$v_source_eventid;

                    $rs10 = $source_db->Execute($v_sql);
                    if (!$rs10) 
                            print $source_db->ErrorMsg();
                    else {
                            while (!$rs10->EOF) {
                                    $recordSet = $target_db->Execute("select seq_sch_crflib.nextval from dual");
                                    $v_eventcrf =  $recordSet->fields[0];
                                    $v_sql = "insert into sch_crflib (PK_CRFLIB,FK_EVENTS,CRFLIB_NUMBER,CRFLIB_NAME,CRFLIB_FLAG,CRFLIB_FORMFLAG,PROPAGATE_FROM) values (
                                    $v_eventcrf,$v_eventid,'".$rs10->fields["CRFLIB_NUMBER"]."','".$rs10->fields["CRFLIB_NAME"]."','".$rs10->fields["CRFLIB_FLAG"]."','".$rs10->fields["CRFLIB_FORMFLAG"]."',$v_eventcrf)";

                                    if ($v_status) {
                                            $v_status = $target_db->Execute($v_sql);
                                            $v_error = (!$v_status) ? $target_db->ErrorMsg()." SCH_CRFLIB (Event record: displacement=2)" : ""; 
                                    }
                                    $rs10->MoveNext();
                            }
                    }			
                    $v_counter++;
                    $rs8->MoveNext();
                    }
            }

            // ER_OBJECTSHARE
            $recordSet = $target_db->Execute("SELECT seq_er_objectshare.NEXTVAL as pk_objectshare FROM dual");
            $v_pk_objectshare = $recordSet->fields[0];

            if ($v_status) {
                    $v_status = $target_db->Execute("insert into er_objectshare (pk_objectshare,object_number,fk_object,fk_objectshare_id,objectshare_type,record_type) values ($v_pk_objectshare,3,$v_protocolid,$v_account,'A','N')");
                    $v_error = (!$v_status) ? $target_db->ErrorMsg()." er_objectshare" : ""; 
            }

            if ($v_status) {
                    $v_status = $target_db->Execute("insert into er_objectshare (pk_objectshare,object_number,fk_object,fk_objectshare_id,objectshare_type,record_type,fk_objectshare) select seq_er_objectshare.NEXTVAL,3,$v_protocolid,pk_user,'U','N',$v_pk_objectshare from er_user where fk_account = $v_account and usr_type <> 'X'");
                    $v_error = (!$v_status) ? $target_db->ErrorMsg()." er_objectshare" : ""; 
            }

            }
            //implemented for the enhancmet eT3.3-8
            // subcost item	
            $subCitem = $source_db->Execute("select * from sch_subcost_item where FK_CALENDAR=".$v_calid);
            $sci_count = $source_db->Execute("select count(*) from sch_subcost_item where FK_CALENDAR=".$v_calid);
            $esciCount = $sci_count->fields[0];	
            $src_db_pk_sci = array();
            $i=0;	
            if($esciCount>0){		
                    while(!$subCitem->EOF){
                            $sql = "insert into sch_subcost_item (PK_SUBCOST_ITEM, FK_CALENDAR, SUBCOST_ITEM_NAME, SUBCOST_ITEM_COST, SUBCOST_ITEM_UNIT, FK_CODELST_CATEGORY, FK_CODELST_COST_TYPE) values (esch.SEQ_SCH_SUBCOST_ITEM.nextval,".$v_currentCalID.",'".$subCitem->fields["SUBCOST_ITEM_NAME"]."',".$subCitem->fields["SUBCOST_ITEM_COST"].",".$subCitem->fields["SUBCOST_ITEM_UNIT"].",".$subCitem->fields["FK_CODELST_CATEGORY"].",".$subCitem->fields["FK_CODELST_COST_TYPE"].")";			
                            $v_status = $target_db->Execute($sql);
                            $v_error = (!$v_status) ? $target_db->ErrorMsg()." sch_subcost_item" : "";
                            $src_db_pk_sci[$i] = $subCitem->fields["PK_SUBCOST_ITEM"];
                            $i++;
                            $subCitem->MoveNext();
                    }
            }			

            //subcost item visit
            for($i=0; $i<count($src_db_pk_sci); $i++){	
                    $subcostitemVisit = $source_db->Execute("select FK_PROTOCOL_VISIT from sch_subcost_item_visit where fk_subcost_item=".$src_db_pk_sci[$i]." order by PK_SUBCOST_ITEM_VISIT");
                    $subcostitemVisit_c = $source_db->Execute("select count(*) from sch_subcost_item_visit where fk_subcost_item=".$src_db_pk_sci[$i]);
                    $subcostitemVisitCount = $subcostitemVisit_c->fields[0];

                    $subcostitem = 	$target_db->Execute("select PK_SUBCOST_ITEM from sch_subcost_item where FK_CALENDAR=".$v_currentCalID." order by PK_SUBCOST_ITEM");
                    if($subcostitemVisitCount>0){		
                            for($j=0; $j<$subcostitemVisitCount; $j++){
                                    $protocolVisitNoQ = $source_db->Execute("select VISIT_NO from sch_protocol_visit where PK_PROTOCOL_VISIT=".$subcostitemVisit->_array[$j]["FK_PROTOCOL_VISIT"]);
                                    $protocolVisitNo = $protocolVisitNoQ->fields[0];
                                    $esql = "insert into sch_subcost_item_visit (PK_SUBCOST_ITEM_VISIT, FK_SUBCOST_ITEM, FK_PROTOCOL_VISIT) values (ESCH.SEQ_SCH_SUBCOST_ITEM_VISIT.nextval,".$subcostitem->_array[$i]["PK_SUBCOST_ITEM"].",(select PK_PROTOCOL_VISIT from sch_protocol_visit where FK_PROTOCOL=".$v_currentCalID." and VISIT_NO=".$protocolVisitNo."))";
                                    //echo $esql."<br>";

                                    $v_status = $target_db->Execute($esql);
                                    $v_error = (!$v_status) ? $target_db->ErrorMsg()." sch_subcost_item_visit" : "";
                            }
                    }	
            }





    if ($v_status) {
            $target_db->CommitTrans();
            echo "<BR>Calendar transferred!";
    } else {
            $target_db->RollbackTrans();
            echo "<BR><Font color=red><b>Calendar transfer not successful: $v_error</b></font>";
    }

}else{
	echo "<BR><Font color=red><b>The Source and Destination versions are not compatible</b></font>";
}

}

?>
</div>


</body>
</html>


<?php
$target_db->Close();
$source_db->Close();
}
else header("location: index.php?fail=1");
?>
		
