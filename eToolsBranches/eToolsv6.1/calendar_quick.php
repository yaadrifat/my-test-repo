<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>
	<html>
	<head>    <title>Velos eTools -> Calendar from File</title>
<script>
function validate(formObj) {
	if (formObj.calname.value == ""){
		alert("Calendar name cannot be blank");
		formObj.calname.focus();
		return false;	
	}
	if (formObj.calduration.value == ""){
		alert("Calendar duration cannot be blank");
		formObj.calduration.focus();
		return false;	
	}
	if (formObj.delimiter.value == ""){
		alert("Delimiter cannot be blank");
		formObj.delimiter.focus();
		return false;	
	}
	if (formObj.uploadedfile.value == ""){
		alert("File name cannot be blank");
		formObj.uploadedfile.focus();
		return false;	
	}
} 
</script>
	</head>
	<?php
        include("db_config.php");
	include("./includes/header.php");
	include("./includes/oci_functions.php");
	include("./includes/calendar.php");
        require_once('customfunctions.php');

	$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
	<body>
	<div id="fedora-content">	
<?PHP
	if ($_SERVER['REQUEST_METHOD'] != 'POST'){                 
		$v_query = "SELECT pk_catlib,catlib_name || decode(ac_name,null,'',' [' || ac_name || ']') as catlib_name FROM ER_CATLIB,er_account WHERE fk_account <> 46 and pk_account = fk_account and catlib_type = 'L' order by ac_name || ' - ' || lower(catlib_name)";
		$results = executeOCIQuery($v_query,$ds_conn);
		$dd_lookup = '<select class="required" name="category">';
		for ($rec = 0; $rec < $results_nrows; $rec++){
			$dd_lookup .= '<option value="'.$results["PK_CATLIB"][$rec].'">'.$results["CATLIB_NAME"][$rec].'</option>';
		}
		$dd_lookup .= '</select>';
		
		$et_query = "SELECT * from  SCH_CODELST WHERE CODELST_TYPE='lib_type'";
		$rs = executeOCIQuery($et_query,$ds_conn);
		$event_type = '<select class="required" name="eventtype">';
		for ($rec = 0; $rec < $results_nrows; $rec++){
			$event_type .= '<option value="'.$rs["PK_CODELST"][$rec].'">'.$rs["CODELST_DESC"][$rec].'</option>';
		}
		$event_type .= '</select>';
		
?>
		<div class="navigate">Calendar from File:</div>
		<form name="calendar" enctype="multipart/form-data" action="calendar_quick.php" method="POST" onSubmit="if (validate(document.calendar) == false) return false;">
		<table>
		<tr><td>Calendar Name:</td><td><input type="text" name="calname" size="50" class="required" /></td></tr>
		<tr><td>Calendar Type:</td><td><?PHP echo $dd_lookup; ?></td></tr>
		<tr><td>Calendar Duration:</td><td><input class="required" type="text" name="calduration" size="10"/>
		<select class="required" name="durunit">
			<option value="D">Day(s)</option>
			<option value="W">Week(s)</option>
			<option value="M">Month(s)</option>
			<option value="Y">Year(s)</option>
		</select>
		</td></tr>
		<tr>
		  <td>Event Type: </td><td><?php echo $event_type; ?></td></tr>		
		<tr><td>Delimited File name:</td><td><input class="required" type="file" name="uploadedfile" size="75"/></td></tr>
		<tr><td>Delimiter:</td><td><input class="required" type="text" name="delimiter" size="2" value="," /></td></tr>
		<tr><td><input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" /></td><td align="right">Sample template: <a href="./template/calendar_template.csv">calendar_template.csv</a></td></tr>
		</table>
		</form>
		<script>
			document.calendar.calname.focus();
		</script>

		<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;">
		<table summary="Note: Note">
		<tr>
		<th align="left">Note</th>
		</tr>
		<tr><td align="left" valign="top">
		<p>Supported File Types: Any delimited file (delimiters such as comma, semicolon etc. are commonly used)</p>                
                <p>Visit should be defined as - section1|section2|section3|section4</p>                
                <p>section1 : Type of visit, F – Fixed Time Point, D – Dependent Time Point, N – No Time Point Defined.</p>
                <p>section2 : Visit Name, Any alpha numeric or numeric with or without space</p>
                <p>section3 : Visit Interval If Type of Visit as 'F', Interval should be any combination of Month, Week and Day. Ex. M2W3D4 or M1W2 or W1D3 or M1D3 or M1 or W2 or D10. 
 If Type of Visit as 'D', Interval should be either Month or Week or Day. Ex. M2 or W3 or D2</p>
                <p>section4 : Visit dependency, It is applicable only to 'Dependent Time Point' visit Dependent Visit should be available in the template and it should be either 'Fixed Time Point' or 'Dependent Time Point'.           Dependent Visit should be defined as a visit name in the prior
columns.</p>
                <p>Example Visits : 
                    For Fixed Time Point - F|V1|M1W2D2, F|Visit ABC|M2D4, F|Visit_One|D10 
                    For Dependent Time Point - D|V2|M1|V1, D|Visit CDE|D2|Visit ABC, D|Visit_Two|W3|Visit_One. 
                    For No Time Point Defined - N|V3, N|Visit FGH, N|Visit_Three</p>
                <br>
                <p>eTools Limitation</p>
                <p>First visit should always be a ‘fixed time point’.</p>
                <p>‘Dependent visits’ can only be dependent on ‘Fixed Time Point’ or other dependent visits, never dependent on a ‘No Time Point Defined’ visit.</p>
		</td></tr>
		</table></div>

	<?php }else{            
                     $v_delimiter = $_POST["delimiter"]; //-- delimiter from form
                     $v_category = $_POST["category"]; //-- caldendar type
                     $v_eventtype = $_POST["eventtype"]; //-- event type
                     $v_calname = $_POST["calname"]; //-- calendar name
                     $v_calduration = $_POST["calduration"]; //-- duration
                     $v_durunit = $_POST["durunit"]; //-- duration unit

                        if($delimiter == ''){
                            $delimiter = ',';
                        }                     
                     
                     // calendar exist check
                     $v_query = "SELECT fk_account FROM ER_CATLIB WHERE pk_catlib = ".$v_category;   
                     $results = executeOCIQuery($v_query,$ds_conn);
                     $v_account= $results["FK_ACCOUNT"][0];
                     $v_query = "SELECT count(*) as count FROM event_def WHERE event_type = 'P' and user_id =".$v_account." and lower(name) = lower('".trim($v_calname,' ')."')";
                     $results = executeOCIQuery($v_query,$ds_conn);
                     $v_calexists= $results["COUNT"][0];
                     
                     
                     if (($v_calexists > 0) || (intval($v_calduration) == 0)) {
                         // if the calendar is exist
                             $v_query = "SELECT pk_catlib,catlib_name as catlib_name FROM ER_CATLIB,er_account WHERE pk_account = fk_account and catlib_type = 'L'";
                             $results = executeOCIQuery($v_query,$ds_conn);			
                             $dd_lookup = '<select  class="required" name="category">';
                             for ($rec = 0; $rec < $results_nrows; $rec++){
                                     if ($v_category == $results["PK_CATLIB"][$rec]){
                                             $dd_lookup .= '<option selected value="'.$results["PK_CATLIB"][$rec].'">'.$results["CATLIB_NAME"][$rec].'</option>';
                                     } else {
                                             $dd_lookup .= '<option value="'.$results["PK_CATLIB"][$rec].'">'.$results["CATLIB_NAME"][$rec].'</option>';
                                     }
                             }
                             $dd_lookup .= '</select>';

                             $et_query = "SELECT * from  SCH_CODELST WHERE CODELST_TYPE='lib_type'";
                             $rs = executeOCIQuery($et_query,$ds_conn);
                             $event_type = '<select  class="required" name="eventtype">';
                             for ($rec = 0; $rec < $results_nrows; $rec++){
                                 if ($v_eventtype == $rs["PK_CODELST"][$rec]){
                                     $event_type .= '<option selected value="'.$rs["PK_CODELST"][$rec].'">'.$rs["CODELST_DESC"][$rec].'</option>';
                                 }else{
                                     $event_type .= '<option value="'.$rs["PK_CODELST"][$rec].'">'.$rs["CODELST_DESC"][$rec].'</option>';
                                 }                                
                             }
                             $event_type .= '</select>';                        
             ?>
                             <div class="navigate">Quick Calendar:</div>
                             <form enctype="multipart/form-data" action="calendar_quick.php" method="POST">
                             <table>
                             <tr><td>Calendar Name:</td><td><input class="required" type="text" name="calname" size="50" value="<?PHP echo $v_calname; ?>"/>
                             <?PHP if ($v_calexists > 0) echo '<font color="red"> <b>Calendar name already exists.</b></font>'; ?>
                             </td></tr>
                             <tr><td>Calendar Type :</td><td><?PHP echo $dd_lookup; ?></td></tr>
                             <tr><td>Calendar Duration :</td><td><input  class="required" type="text" name="calduration" size="10" value="<?PHP echo $v_calduration; ?>"/>
                             <select  class="required" name="durunit">
                     <?PHP
                             if ($v_durunit == 'D') { echo '<option selected value="D">Day(s)</option>'; } else { echo '<option value="D">Day(s)</option>';}
                             if ($v_durunit == 'W') { echo '<option selected value="W">Week(s)</option>'; } else { echo '<option value="W">Week(s)</option>';}
                             if ($v_durunit == 'M') { echo '<option selected value="M">Month(s)</option>';} else {echo '<option value="M">Month(s)</option>';}
                             if ($v_durunit == 'Y') { echo '<option selected value="Y">Year(s)</option>';} else {echo '<option value="Y">Year(s)</option>';}
                             echo "</select>";
                             if (intval($v_calduration) == 0) echo '<font color="red"><b> Invalid calendar duration.</b></font>';
                     ?>
                             </td></tr>
                             <tr><td>Event Type:</td><td><?PHP echo $event_type; ?></td></tr>
                             <tr><td>Delimited File name:</td><td><input  class="required" type="file" name="uploadedfile" size="100"/></td></tr>
                             <tr><td>Delimiter:</td><td><input  class="required" type="text" name="delimiter" size="2" value="<?PHP echo $v_delimiter; ?>"/></td></tr>
                             <tr><td><input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onmouseover="this.src='./img/submit_m.png';" onmouseout="this.src='./img/submit.png';" /></td></tr>
                             </table>
                             </form>
                             <div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Note">
                             <tr>
                             <!--<td rowspan="2" align="center" valign="top" width="25"><img alt="[Note]" src="./stylesheet-images/note.png"></td>-->
                             <th align="left">Note</th>
                             </tr>
                             <tr><td align="left" valign="top">
                                <p>Supported File Types: Any delimited file (delimiters such as comma, semicolon etc. are commonly used)</p>                
                                <p>Visit should be defined as - section1|section2|section3|section4</p>                
                                <p>section1 : Type of visit, F – Fixed Time Point, D – Dependent Time Point, N – No Time Point Defined.</p>
                                <p>section2 : Visit Name, Any alphanumberic or numberic with or withour space</p>
                                <p>section3 : Visit interval, Visit Interval If Type of Visit as 'F', Interval should be any combination of Month, Week and Day. Ex. M2W3D4 or M1W2 or W1D3 or M1D3 or M1 or W2 or D10
                 If Type of Visit as 'D', Interval should be either Month or Week or Day. Ex. M2 or W3 or D2</p>
                                <p>section4 : Visit dependency, It is applicable only to 'Dependent Time Point' visit Dependent Visit should be available in the template and it should be either 'Fixed Time Point' or 'Depenedent Time Point'           Dependentt Visit should be defined as a visit name in the prior
                columns.</p>
                                <p>Example Visits : 
                                    For Fixed Time Point - F|V1|M1W2D2, F|Visit ABC|M2D4, F|Visit_One|D10 
                                    For Dependent Time Point - D|V2|M1|V1, D|Visit CDE|D2|Visit ABC, D|Visit_Two|W3|Visit_One
                                    For No Time Point Defined - N|V3, N|Visit FGH, N|Visit_Three</p>
                                <br>
                                <p>eTools Limitation</p>
                                <p>First visit should always be a ‘fixed time point’.</p>
                                <p>‘Dependent visits’ can only be dependent on ‘Fixed Time Point’ or other dependent visits, never dependent on a ‘No Time Point Defined’ visit.</p>
                             </td></tr>
                             </table></div>
                     <?PHP	
                     } else {
                         // if the calendar is not exist
			$curDir = getcwd();
			$target_path = $curDir."/upload/";
			$target_path = $target_path.basename( $_FILES['uploadedfile']['name']);
			if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
                            
                            //mapping
                            $catSRC = array(); // Source Category
                            $eventSRC = array(); // Source Event
                            $catSRC_org = array(); // Source Category original (CSV)
                            $maphandle = fopen($target_path, "r");
                            // fix for the bug 26145
                            //while (($mapdata = fgetcsv($maphandle, 10000, $v_delimiter)) !== FALSE) {
                            while (($mapdata = fgetcsv($maphandle,0, $v_delimiter)) !== FALSE) {
                                $mnum = count($mapdata);
                                for ($mc=0; $mc < $mnum; $mc++) {
                                    if($mc==1){
                                        $catSRC[] = $mapdata[1];
                                        $eventSRC[] = $mapdata[0];
                                    }                                    
                                }
                            }
                            array_splice($catSRC,0,1);
                            array_splice($eventSRC,0,1);
                            // Fetch the available category for the account and library type
                            $mapdesqry = "select NAME from EVENT_DEF where EVENT_LIBRARY_TYPE = ".$v_eventtype." and EVENT_TYPE = 'L' and USER_ID = ".$v_account;
                            $mapdesrs = executeOCIQuery($mapdesqry,$ds_conn);

                            $catDES = array();  // Destination Category
                            $catDUP = array(); // Duplicate Category
                            
                            for($dc=0; $dc < sizeof($mapdesrs['NAME']); $dc++){
                                $catDES[] = $mapdesrs['NAME'][$dc];
                            }
                            
                            // when the given category is not available it will store 'n' in catDUP
                            if(sizeof($catSRC) > 0){
                                $mapcountT = 0;
                                $mapcountF = 0;
                                for($ac=0; $ac < sizeof($catSRC); $ac++){
                                    if(in_array(trim($catSRC[$ac]),$catDES)){
                                        $mapcountT = $mapcountT+1;
                                        $catDUP[$ac] = $ac;
                                    }else{
                                        $catDUP[$ac] = 'n';
                                        $mapcountF = $mapcountF+1;
                                    }
                                }
                            }
                            $catSRC_org = $catSRC;
                            $catSRC = array_unique($catSRC);                            
                            $catSRC = array_values($catSRC);
                            //if($mapcountF > 0){  -- fix for the bug 25822
                            if($mapcountF > 0 || $mapcountT > 0){ 
                                $event_cat = '<select name="ecategory[]" class="required">';
                                $event_cat .= '<option value=""> Select category </option>';
                                for($mac=0; $mac < sizeof($catDES); $mac++){                                    
                                    $event_cat .= '<option value="'.$catDES[$mac].'">'.$catDES[$mac].'</option>';
                                }
                                $event_cat .= '</select>';
                                
                                // Event Category mapping form
                                echo '<form action="calendar_quickmap.php" method="POST">';
                                echo '<input type="hidden" name="delimiter" value="'.$v_delimiter.'"/>';
                                echo '<input type="hidden" name="category" value="'.$v_category.'"/>';
                                echo '<input type="hidden" name="eventtype" value="'.$v_eventtype.'"/>';
                                echo '<input type="hidden" name="account" value="'.$v_account.'"/>';
                                echo '<input type="hidden" name="fileDir" value="'.$target_path.'"/>';
                                
                                echo '<input type="hidden" name="calname" value="'.$v_calname.'"/>';
                                echo '<input type="hidden" name="calduration" value="'.$v_calduration.'"/>';                                
                                echo '<input type="hidden" name="durunit" value="'.$v_durunit.'"/>';    
                             
                                
//                                echo '$catDES<br>';
//                                echo '<pre>';
//                                print_r($catDES);
//                                echo '<pre><br><br>';
//                                
//                                
//                                echo '$catDUP<br>';
//                                echo '<pre>';
//                                print_r($catDUP);
//                                echo '<pre><br><br>';
//                                echo '$catSRC<br>';
//                                echo '<pre>';
//                                print_r($catSRC);
//                                echo '<pre><br><br>';
//                                echo '$eventSRC<br>';
//                                echo '<pre>';
//                                print_r($eventSRC);
//                                echo '<pre><br><br>';
//                                echo '$catSRC_org<br>';
//                                echo '<pre>';
//                                print_r($catSRC_org);
//                                echo '<pre><br><br>';
//                                exit;
                                
                                
                                $_SESSION['maparr'] = $catDUP;
                                $_SESSION['srcarr'] = $catSRC;
                                $_SESSION['srevent'] = $eventSRC;
                                $_SESSION['srcorg'] = $catSRC_org;
                                echo '<div style="margin-left: 0.5in; margin-right: 0.5in;">';
                                echo '<table border="1"><tr><th width="200">Source Event Category</th><th width="200">Destination Event Category</th></tr>';
                                for($ec=0; $ec<sizeof($catSRC);$ec++){
                                    echo '<tr>';
                                    if(!in_array($catSRC[$ec],$catDES)){
                                        echo '<td>'.$catSRC[$ec].'</td>';
                                        echo '<td>'.$event_cat.'</td>';
                                    }else{
                                        echo '<td>'.$catSRC[$ec].'</td>';
                                        echo '<td><input name="ecategory[]" type="text" readonly value="'.$catSRC[$ec].'" /></td>';
                                    }
                                    echo '</tr>';
                                }
                                echo '<tr><td>&nbsp;</td><td><input type="image" name="mapsubmit" value="submit" src="./img/submit.png"  align="absright" border="0" onmouseover="this.src=\'./img/submit_m.png\';" onmouseout="this.src=\'./img/submit.png\';" /></td></tr>';
                                echo '</table></div></form>';
                                
                                echo '<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;">';
                                echo '<table summary="Note: Note">';
                                echo '<tr><th align="left">Note</th></tr>';
                                echo '<tr><td align="left" valign="top">';
                                echo '<p>Dropdown shows only to the Event Category which is not available in the connected datasource. </p>';
                                echo '<p>You cannot continue without mapping Event Category </p>';
                                echo '</td></tr></table></div>';
                                
                            }
                            
                        } else{
				echo "There was an error uploading the file, please try again!"."<BR>";
			}
                     }
	}
       ?>

</div>
</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
