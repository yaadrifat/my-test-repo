<?PHP
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>
	<html>
	<head>    <title>Velos eTools -> Calendar from File</title>

	</head>
	<?php
        include("db_config.php");
	include("./includes/header.php");
	include("./includes/oci_functions.php");
	include("./includes/calendar.php");
	$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
	<body>
	<div id="fedora-content">	
<?PHP
    
    $catMAPPED = $_POST["ecategory"]; //-- EC mapped array (only mapped ec will be listed)
    $catDUP = $_SESSION["maparr"]; //-- EC duplication between src to des
    $catSRC = $_SESSION['srcarr']; //-- EC source from mapping screen
    $eventSRC = $_SESSION['srevent']; //-- Events source from CSV
    
    $v_category = $_POST["category"]; //-- Calendar type
    $v_eventtype = $_POST["eventtype"]; //-- Event type
    $v_account = $_POST["account"]; //-- user account no
    $v_delimiter = $_POST["delimiter"]; //-- delimiter
    $target_path = $_POST["fileDir"];  //-- file stored directory
    $catSRC_org = $_SESSION['srcorg']; //-- EC source from CSV
    
    $v_calname = $_POST["calname"]; //-- calendar name
    $v_calduration = $_POST["calduration"]; //-- duration
    $v_durunit = $_POST["durunit"]; //-- duration unit
    
     //  Category listing after mapping
    $catLIST = array();
    for($mar=0; $mar<count($catDUP); $mar++){
        $catLIST[$mar] = $catSRC[$catDUP[$mar]];
    }
    
    $catLIST_cnt = count($catLIST);
    $tcnt = 0;    
    for($mar=0; $mar<count($catLIST); $mar++){
        $catLIST[$mar] = $catMAPPED[array_search($catSRC_org[$mar],$catSRC)];
    }

//    echo '$catLIST<br>';
//    echo '<pre>';
//    print_r($catLIST);
//    echo '<pre><br><br>';    
//    exit;
    
     // Calendar tabular with visit 
    echo '<div class="navigate">Quick Calendar:</div>';
    echo '<form action="calendar_quicksave.php" method="POST">';

    echo '<input type="hidden" name="delimiter" value="'.$v_delimiter.'"/>';
    echo '<input type="hidden" name="category" value="'.$v_category.'"/>';
    echo '<input type="hidden" name="eventtype" value="'.$v_eventtype.'"/>';
    echo '<input type="hidden" name="account" value="'.$v_account.'"/>';
    echo '<input type="hidden" name="fileDir" value="'.$target_path.'"/>';
    
    $_SESSION['mapedarr'] = $catDUP;
    $_SESSION['mapedarray'] = $catLIST;
    $_SESSION['srarray'] = $catSRC;
    echo 'Calendar Name: <b>'.$v_calname.'</b><input readonly size=30 type="hidden" name="calname" value="'.$v_calname.'"/>';
    echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Calendar Duration: <b>'.$v_calduration.'<input  readonly size=5 type="hidden" name="calduration1" value="'.$v_calduration.'"/>';
    switch ($v_durunit) {
    case 'D':
        echo ' Day(s)';
        $v_caldur = $v_calduration;
        break;
    case 'W':
        echo " Week(s)";
        $v_caldur = $v_calduration * 7;
        break;
    case 'M':
        $v_caldur = $v_calduration * 30;
        echo " Month(s)";
        break;
    case 'Y':
        $v_caldur = $v_calduration * 365;
        echo " Year(s)";
        break;
    }
    echo '</b><input  type="hidden" size=8 name="calduration" value="'.$v_caldur.'"/>';
    echo '<input  type="hidden" size=8 name="durunit" value="'.$v_durunit.'"/>';
    echo '<table width="100%" border="1" class="calfrmFile_tbl">';
    
    
    $handle = fopen($target_path, "r");
    $row = 0;
    $v_fatal = "";
    $v_fatal1 = "";
    $v_fatal2 = "";
    $v_fatal3 = "";
    $thlen = 0;
    $catlen = 0;
    $evelen = 0;
    $discal = array();
    
    while (($data = fgetcsv($handle, 10000, $v_delimiter)) !== FALSE) {
     $num = count($data);
     if($row == 0){
             echo '<TR>';
             // writing headers for the grid
             for ($c=0; $c < $num; $c++) {
                    if ($c == 0){
                            // event name column header (th) only
                            echo '<td bgcolor="#5C7099"><input style="background-color:#5C7099;border:none;color:white" readonly size="40"  type="text" name="column['.$row.']['.$c.']" value="'.$data[$c].'"/></td>';
                    }else{
                            // Check if the visit is duplicate
                            // $v_visit (M2/W3/D1)  and $v_visit_name (V1/V3)
                            $column = $data[$c];
                            switch(substr($column,0,1))
                            {
                                case 'F':                                    
                                    $curcolumn = substr($column,2,  strlen($column));
                                    list($v_visit_name,$v_visit) = explode('|',$curcolumn);
                                    $discal[$c] = $curcolumn;
                                    if (strlen($v_visit_name) == 0) $v_visit = $v_visit_name;                                    
                                    $discal = array_values($discal);
                                    break;
                                
                                case 'D':                                    
                                    $curcolumn = substr($column,2,strlen($column));
                                    $curcolumnList = explode('|',$curcolumn);
                                    $v_visit = $curcolumnList[1];                                    
                                    $v_visit_name = $curcolumnList[0];
                                    $dependvisitname = $curcolumnList[2];
                                    for($dc=0; $dc<sizeof($discal); $dc++){
                                        if(strpos($discal[$dc],$dependvisitname)!==false){
                                            $depvname = $discal[$dc];
                                        }
                                    }
                                    $depvnamearr = explode('|',$depvname);                                    
                                    $depmodname = 'D|'.$v_visit.'|'.$depvnamearr[1];
                                    break;
                                
                                case 'N':
                                    $curcolumn = substr($column,2,  strlen($column));
                                    //list($v_visit,$v_visit_name) = explode('|',$column);
                                    $v_visit_name = $curcolumn;
                                    if (strlen($v_visit_name) == 0) $v_visit_name = $v_visit;
                                    $discal[$c] = $v_visit_name;
                                    break;
                            }
                            
                             
                            //list($v_visit,$v_visit_name) = explode('|',$data[$c]);
                            //if (strlen($v_visit_name) == 0) $v_visit_name = $v_visit; //if there is no visitname then visit itself will come as visitname
                            $v_visits[$c-1] = $v_visit_name; // stores the visit names
                            
                            $vv_vis[$c-1] = $v_visit;
                            
                            $v_visitcount = 0;
                            $vv = 0;
                            if(strtolower(substr($v_visits[0],0,7)) == 'event_c'){
                                array_splice($v_visits,0);
                            }
                            array_values($v_visits);
                            
                            for($v=1; $v<count($v_visits); $v++){
                                if ($v_visit_name == $v_visits[$v]) $v_visitcount++;
                            }
                            
                            if ($v_visitcount > 1) $v_fatal3 = "X";
                            
                            if(substr($column,0,1) == 'D'){
                                $v_disp = getDisplacementValue($depmodname);
                                $v_dispvalidate = displacementValidate($depmodname);
                            }else{
                                //$v_disp = getDisplacementValue('F|W1D1');
                                $v_disp = getDisplacementValue($data[$c]);
                                $v_dispvalidate = displacementValidate($data[$c]);
                            }
                            
                            $thlen_tmp = strlen($data[$c]);                            
                            if($thlen_tmp > $thlen ){
                                $thlen = $thlen_tmp+2;
                            } 
                            
                            // Shows if the visit interval exceeds the duration - this will highlight the exceeding visit columns
                            
                            //if ($v_disp > $v_caldur && substr($data[$c],0,1) =='E') $v_fatal1 = "Visit interval exceeds calendar duration.";
                            //if ($v_caldur > $v_disp && substr($data[$c],0,1) != 'E') {
                            if ($v_caldur < $v_disp && substr($data[$c],0,1) != 'E') {
                                $v_fatal1 = "Visit interval exceeds calendar duration.";
                            }

                            
                            if ($v_disp == 0 || $v_dispvalidate > 0 ){
                                    echo '<td bgcolor="orange"><input style="background-color:red;border:none;color:orange" readonly size="'.$thlen.'" type="text" name="column['.$row.']['.$c.']" value="'.$data[$c].'"/>';
                                    if ($v_dispvalidate > 0) {
                                            echo " * ";
                                            $v_fatal2 = "X";
                                    }
                                    echo '</td>';
                                    if ($v_disp == 0) $v_fatal .= $data[$c].", ";

                            //}else if ($v_disp > $v_caldur){
                            //}else if ($v_caldur > $v_disp && substr($data[$c],0,1) != 'E'){
                                    
                                    
                            }else if ($v_caldur < $v_disp && substr($data[$c],0,1) != 'E'){
                                if($c==1){
                                    echo '<td bgcolor="orange"><input style="background-color:orange;border:none;color:white" readonly size="'.$thlen.'"  type="text" name="column['.$row.']['.$c.']" value="'.$data[$c].'"/></td>';                                    
                                }else{
                                    //exceeding visit columns
                                    echo '<td bgcolor="orange"><input style="background-color:orange;border:none;color:white" readonly size="'.$thlen.'"  type="text" name="column['.$row.']['.$c.']" value="'.$data[$c].'"/></td>';                                    
                                }                                    
                            }else{
                                if($c==1){
                                    $catlen_tmp = strlen($data[$c]);
                                    if($catlen_tmp > $catlen){
                                        $catlen = $catlen_tmp+1;
                                    }
                                    // exceeding event category header - Common Event Category Header
                                    echo '<td bgcolor="#5C7099"><input style="background-color:#5C7099;border:none;color:white" readonly  size="'.$catlen.'"  type="text" name="column['.$row.']['.$c.']" value="'.$data[$c].'"/></td>';                                    
                                }else{
                                    $thlen_tmp = strlen($data[$c]);                            
                                    if($thlen_tmp > $thlen ){
                                        $thlen = $thlen_tmp+1;
                                    }                                    
                                    // visit header which doesnt exceed - Regular visit
                                    echo '<td bgcolor="#5C7099"><input style="background-color:#5C7099;border:none;color:white" readonly size="'.$thlen.'"  type="text" name="column['.$row.']['.$c.']" value="'.$data[$c].'"/></td>';                                    
                                }
                            }
                    }
             }
             echo "</TR>";
     }else{
            // for the data rows
            $dup = array();
            $v_count = array();
//            for($du=0; $du<count($eventSRC); $du++){
//                $dup[$du] = $eventSRC[$du].':'.$catSRC_org[$du];
//            }
//            echo '<br>$eventSRC';
//            echo '<pre>';
//            print_r($eventSRC);
//            echo '</pre><br><br>';
//            
//            echo '<br>$catSRC_org';
//            echo '<pre>';
//            print_r($catSRC_org);
//            echo '</pre><br><br>';            
            
            //fix for the bug 25806
            for($du=0; $du<count($eventSRC); $du++){
                if(in_array($catSRC_org[$du], $catSRC)){
                    $dd = $catMAPPED[array_search($catSRC_org[$du], $catSRC)];
                    $dup[$du] = $eventSRC[$du].':'.$dd;
                }                
            }
//            echo '<br>$dup';
//            echo '<pre>';
//            print_r($dup);
//            echo '</pre><br><br>';
            
            $withoutduplicate = array_unique($dup);
            $withduplicate = array_diff_assoc($dup,$withoutduplicate);
            $dupkey = array_keys($withduplicate);
            
//            echo '<br>$withoutduplicate';
//            echo '<pre>';
//            print_r($withoutduplicate);
//            echo '</pre><br><br>';
            


//            echo '<br>$dupkey';
//            echo '<pre>';
//            print_r($dupkey);
//            echo '</pre><br><br>';
//            
//            echo '<br>$catSRC_org';
//            echo '<pre>';
//            print_r($catSRC_org);
//            echo '</pre><br><br>';            
            
            
            $tobewritten = array();
            for($fl=0; $fl<count($catSRC_org); $fl++){
                if(in_array($fl, $dupkey)){
                    $tobewritten[$fl] = 'y';        
                }else{
                    $tobewritten[$fl] = 'n';        
                }                
            }
//            
            // fix for the bug 25808
            for($fl1=0; $fl1<count($catSRC_org); $fl1++){
                
                $dubval = explode(':',$dup[$fl1]);
                $v_query = "SELECT count(*) as count FROM event_def WHERE EVENT_TYPE = 'E'  and chain_id = (select event_id from event_def where name = '".$dubval[1]."' and event_type = 'L' and user_id = ".$v_account.")   and user_id = ".$v_account." and trim(upper(name)) = trim(upper('$dubval[0]'))";
                //echo '<br>$v_query: '.$v_query.'<br>';
                $results = executeOCIQuery($v_query,$ds_conn);
                $v_count[$fl1] = $results["COUNT"][0];                
            }
            
            //exit;
//            echo '<br>$v_count';
//            echo '<pre>';
//            print_r($v_count);
//            echo '</pre><br><br>';
//            exit;
            //echo $v_count[$row];
//            echo '<br>$catMAPPED';
//            echo '<pre>';
//            print_r($catMAPPED);
//            echo '</pre><br><br>';
//            
//            echo '<br>$catSRC_org';
//            echo '<pre>';
//            print_r($catSRC_org);
//            echo '</pre><br><br>';            
//
//            echo '<br>$catSRC';
//            echo '<pre>';
//            print_r($catSRC);
//            echo '</pre><br><br>'; 
//
//            echo '<br>$eventSRC';
//            echo '<pre>';
//            print_r($eventSRC);
//            echo '</pre><br><br>';            
            
//
//            echo '$catLIST';
//            echo '<pre>';
//            print_r($catLIST);
//            echo '</pre><br><br>';  
//            
//            echo '$withoutduplicate';
//            echo '<pre>';
//            print_r($withoutduplicate);
//            echo '</pre><br><br>';              
//            
//            echo '$withduplicate';
//            echo '<pre>';
//            print_r($withduplicate);
//            echo '</pre><br><br>';  
//            
//            echo '$dupkey';
//            echo '<pre>';
//            print_r($dupkey);
//            echo '</pre><br><br>';  
//            
//            echo '$tobewritten';
//            echo '<pre>';
//            print_r($tobewritten);
//            echo '</pre><br><br>';     
//            exit;
//            
//            echo '--------------------------------';
            
            $_SESSION['tobewritten'] = $tobewritten;            
                $splvisit = '';
                $splvisitArr = array();
             for ($c=0; $c < $num; $c++) {                  
                 //24848
                 if($row!=0){
                     $rowM = $row-1;
                 }else{
                     $rowM = $row;
                 }

                 
                 //$rowM = $row;
                     if ($c == 0){
                            $evelen_tmp = strlen($data[$c]);
                            if($evelen_tmp > $evelen){
                                $evelen = $evelen_tmp + 2;
                            }
                         
                            $v_eventname = $data[$c];
                            $v_evename[] = $data[$c];
                            $catItem = $data[$c+1];
                            
                            // data columns under visit
                            if ($v_count[$rowM] == 0) {echo "<tr bgcolor=#5C7099>";} else {echo "<tr bgcolor=#e7f0fe>";}                            
                            //HEADER
                            // event category & visit header
                            if($tobewritten[$rowM] == 'n'){ 
                                if ($v_count[$rowM] == 0) {
                                    echo '<td bgcolor="#5C7099" style="width:'.$evelen.'px;"><input style="background-color:#5C7099;border:none;color:white" size="'.$evelen.'" readonly type="text" name="column['.$row.']['.$c.']" value="'.$data[$c].'"/></td>';                                        
                                }else{
                                    echo '<td bgcolor="#5C7099" style="width:'.$evelen.'px;"><input style="background-color:#5C7099;border:none;color:white"  size="'.$evelen.'"  readonly  type="text" name="column['.$row.']['.$c.']" value="'.$data[$c].'"/></td>';
                                    echo '<input type="hidden" name="column1['.$row.']['.$c.']" value="'.$data[$c].'"/>';                                    
                                }
                                // event name td 
                                //echo '<td bgcolor="#5C7099" style="width:'.$evelen.'px;"><input style="background-color:#5C7099;border:none;color:white" size="'.$evelen.'" readonly type="text" name="column['.$row.']['.$c.']" value="'.$data[$c].'"/></td>';    
                            }else{                                
                                // highlighted event name td
                                echo '<td bgcolor="#5C7099" style="width:'.$evelen.'px;"><input style="background-color:#5C7099;border:none;color:white"  size="'.$evelen.'"  readonly  type="text" name="column1['.$row.']['.$c.']" value="'.$data[$c].'"/></td>';
                                echo '<input type="hidden" name="column1['.$row.']['.$c.']" value="'.$data[$c].'"/>';
                            }                            
                     }elseif($c == 1){

                         // event category column                         
                         $catlen_tmp = strlen($catLIST[$rowM]);
                         if($catlen_tmp > $catlen){
                             $catlen = $catlen_tmp;
                         }
                         $existavail = 0;
                         if($tobewritten[$rowM] == 'n'){
                             if ($v_count[$rowM] == 0) {
                                 //event category td
                                echo '<td  bgcolor="white"  width="5px"><input style="border:none;color:black" readonly type="text" size="'.$catlen.'" for="'.$catlen.'" name="column['.$row.']['.$c.']" value="'.$catLIST[$rowM].'"/></td>';                            
                             }else{
                                 // highlighted event category td
                                 //shows the existing
                                echo '<td bgcolor="#fdf8df" width="5px"><input style="background-color:#fdf8df;border:none;color:black" readonly for="'.$catlen.'" size="'.$catlen.'"  type="text" name="column['.$row.']['.$c.']" value="'.$catLIST[$rowM].'"/></td>';                                                          
                                echo '<input type="hidden" name="column1['.$row.']['.$c.']" value="'.$data[$c].'"/>';
                                $existavail = 1;
                             }
                         }else{
                             // duplicate category td
                             // CSV level duplicate
                             echo '<td class="hightlightduplicate"  width="5px"><input style="border:none;color:black; background-color:#fcced1;" readonly for="'.$catlen.'" size="'.$catlen.'"  type="text" name="column1['.$row.']['.$c.']" value="'.$catLIST[$rowM].'"/></td>';                         
                         }
                     }else{
                        $thlen_tmp = strlen($data[$c]);                            
                        if($thlen_tmp > $thlen ){
                            $thlen = $thlen_tmp+2;
                        }
                         // visits columns
                                  
                         if($tobewritten[$rowM] == 'n'){                             
                             if ($v_count[$rowM] == 0) {
                                 // ordinary visit data td
                                echo '<td bgcolor="white"  style="width:10px;"><input style="border:none;text-align: center;" readonly size="'.$thlen.'" type="text" name="column['.$row.']['.$c.']" value="'.(empty($data[$c])?"":$data[$c]).'"/></td>';                                 
                             }else{                                 
                                 // allowed visit data td
                                 echo '<td style="width:10px;" bgcolor="#fdf8df"><input style="background-color:#fdf8df;border:none;text-align: center;" readonly size="'.$thlen.'"  type="text" name="column['.$row.']['.$c.']" value="'.(empty($data[$c])?"":$data[$c]).'"/></td>';
                                 echo '<input type="hidden" name="column1['.$row.']['.$c.']" value="'.$data[$c].'"/>';
                             }
                            if(!empty($data[$c])){
                                $splvisit = $splvisit.','.$data[$c];
                            }                             
                         }else{
                            // duplicated visit data td 
                            echo '<td  style="width:10px;" class="hightlightduplicate"><input style="background-color:#fcced1;border:none;text-align: center;" readonly  size="'.$thlen.'" type="text" name="column1['.$row.']['.$c.']" value="'.(empty($data[$c])?"":$data[$c]).'"/></td>';                             
                         }                         

                     }
                     
             }
             
             $splvisit1 = $splvisit1.$splvisit;
         echo '</tr>';         
         
    }        
    
     $row++;
    }
      $splvisitArr = explode(',',$splvisit1);
      unset($splvisitArr[0]);
      $_SESSION['splvisitArr'] = $splvisitArr;
    echo "</table>";
    echo "<BR>";
    
    
    if ((strlen($v_fatal) == 0) && (strlen($v_fatal1) == 0) && (strlen($v_fatal2) == 0) && (strlen($v_fatal3) == 0)){
            echo 'Highlighted events exists in the event library. Do you want to transfer event attributes (cost/messages/resources/documents) from library? <input align=right type=checkbox name=migrate checked><br><br>';
            echo '<input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onmouseover="this.src=\'./img/submit_m.png\';" onmouseout="this.src=\'./img/submit.png\';" />';
    } else {
            if (strlen($v_fatal) > 0) {
                    $v_fatal = substr($v_fatal,0,strlen($v_fatal)-2);
                    echo "<font color=red><b>Invalid visit interval definition(s): $v_fatal <BR>Valid format is MnWnDn, were n is the number of month(s)/week(s)/day(s).<br></b></font>";
            }
            if (strlen($v_fatal1) > 0) {
                    echo "<font color=#5C7099><b><BR>$v_fatal1</b></font>";
            }
            if (strlen($v_fatal2) > 0) echo "<font color=#5C7099><b><BR>* - Weeks cannot be more than 4 when month is entered / Days cannot be more than 7 when week is entered.<br></b></font>";
            if (strlen($v_fatal3) > 0) echo "<font color=magenta><b><BR>Visit name cannot be duplicate.<br></b></font>";
    }
    $_SESSION['evename'] = $v_evename;
    echo "</form>";
    fclose($handle);  
?>
</div>
</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
