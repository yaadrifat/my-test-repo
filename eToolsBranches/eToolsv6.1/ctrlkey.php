<?php
	
	
session_start();
header("Cache-control: private");	// Fixes IE6's back button problem.

if(@$_SESSION["user"]){
?>

<html>
<head>
<title>Velos eTools -> Code List</title>
<script language="javascript" type="text/javascript">
function getHTTPObject(){
	if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
		else if (window.XMLHttpRequest) return new XMLHttpRequest();
	else {
	  alert("Your browser does not support AJAX.");
		return null;
	}
}
	
function refreshPage(selected){
	if(document.getElementById('users').value == 'Select User') {
		document.getElementById('selectmodule').style.display="none";
		document.getElementById('selecttab').style.display="none";
		document.getElementById('moduledata').style.display="none";
		document.getElementById('subtype').value = 'Select Module';
		document.getElementById('objectname').value = 'Select Tab';
		return false;
	}
	if(selected != ""){			
		document.getElementById('selectmodule').style.display="block";
		document.getElementById('selecttab').style.display="block";
		refresh(selected, document.controlkey.subtype.value, document.controlkey.objectname.value);
	} else {
		document.getElementById('selectmodule').style.display="none";
		document.getElementById('selecttab').style.display="none";
		document.getElementById('moduledata').style.display="none";
	}
	document.getElementById('selectmodule').style.display="block";
	document.getElementById('selecttab').style.display="block";
}
	
function refresh(selected, subtype, objectname){
httpObject = getHTTPObject();
	if (httpObject != null) {
		httpObject.open("GET", "ctrlkey_ajax.php?pk_user="+selected+"&subtype="+subtype+"&objectname="+objectname, false);	 
		httpObject.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
		httpObject.setRequestHeader("Cache-Control", "no-cache");
		httpObject.send(null);
		if(httpObject.status != 200){document.write("404 not found");}
		var finalString = httpObject.responseText;
		if(document.getElementById('subtype').value == 'Select Module' && document.getElementById('objectname').value == 'Select Tab') {
			document.getElementById('moduledata').style.display = 'none';
		} else {
			document.getElementById('moduledata').style.display = 'block';
		}
		if (finalString != "") {
			//document.getElementById('moduledata').style.display="block";			
			document.getElementById('moduledata').innerHTML = finalString;
		}
	}
}
var httpObject = null;
</script>
</head>
<?php
        include("db_config.php");
	include("./includes/header.php");
	include("./includes/oci_functions.php");
	$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
<body onLoad="refresh('<?php echo $_SESSION["pk_user"] ?>', '<?php echo $_SESSION["subtype"] ?>', '<?php echo $_SESSION["objectname"] ?>');"> 
<div id="fedora-content" style="min-height:70px;">	
<?php
//print"<pre>"; print_r($_SESSION); print"</pre>";
if ($_SERVER['REQUEST_METHOD'] != 'POST') {		
	$v_sql = "SELECT pk_user, fk_account, usr_firstname || ' ' || usr_lastname AS username FROM ER_USER WHERE USR_STAT = 'A' ORDER BY lower(usr_firstname || ' ' || usr_lastname)";
	$results = executeOCIQuery($v_sql,$ds_conn);
	$users = "";
	$users.= "<option value = 'Select User'>Select User</option>";
	for ($rec = 0; $rec < $results_nrows; $rec++){
		if(trim($results["USERNAME"][$rec]) != '') {
			$results["USERNAME"][$rec] = str_replace(array("<script>","</script>"),array("",""),$results["USERNAME"][$rec]);
			if($results["PK_USER"][$rec] == $_SESSION["pk_user"]){
				$users.= "<option value=".$results["PK_USER"][$rec]." SELECTED>".$results["USERNAME"][$rec]."</option>";
			}else{
				$users.= "<option value=".$results["PK_USER"][$rec].">".$results["USERNAME"][$rec]."</option>";
			}
		}
	}
	$display_module_sql = "SELECT OBJECT_SUBTYPE, OBJECT_DISPLAYTEXT FROM ER_OBJECT_SETTINGS WHERE OBJECT_TYPE = 'TM' AND OBJECT_VISIBLE = '1' AND FK_ACCOUNT = '0' ORDER BY OBJECT_SEQUENCE ASC";
	$display_module_results = executeOCIQuery($display_module_sql,$ds_conn);
	$v_modules = "";
	$v_modules.= "<option value = 'Select Module'>Select Module</option>";
	for ($rec = 0; $rec < $results_nrows; $rec++){
			if($display_module_results["OBJECT_SUBTYPE"][$rec] != 'logout_menu' && $display_module_results["OBJECT_SUBTYPE"][$rec] != 'help_menu'){ //-- for the bug 16880
				if($display_module_results["OBJECT_SUBTYPE"][$rec] == $_SESSION["subtype"]){
					$v_modules.= "<option value=".$display_module_results["OBJECT_SUBTYPE"][$rec]." SELECTED>".$display_module_results["OBJECT_DISPLAYTEXT"][$rec]."</option>";
				}else{
					$v_modules.= "<option value=".$display_module_results["OBJECT_SUBTYPE"][$rec].">".$display_module_results["OBJECT_DISPLAYTEXT"][$rec]."</option>";
				}			
			}
	}
	
	$display_tab_sql = "SELECT OBJECT_NAME FROM ER_OBJECT_SETTINGS WHERE OBJECT_TYPE = 'T' AND OBJECT_VISIBLE = '1' AND FK_ACCOUNT = '0' GROUP BY OBJECT_NAME ORDER BY OBJECT_NAME ASC";
	$display_tab_results = executeOCIQuery($display_tab_sql,$ds_conn);
	$v_tabs = "";
	$v_tabs.= "<option value = 'Select Tab'>Select Tab</option>";
	for ($rec = 0; $rec < $results_nrows; $rec++){

		if($display_tab_results["OBJECT_NAME"][$rec] == 'acct_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'Account';
		}
		if($display_tab_results["OBJECT_NAME"][$rec] == 'budget_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'Budget';
		}
		if($display_tab_results["OBJECT_NAME"][$rec] == 'ctrp_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'CTRP';
		}
		if($display_tab_results["OBJECT_NAME"][$rec] == 'event_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'Event';
		}
		if($display_tab_results["OBJECT_NAME"][$rec] == 'form_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'Form';
		}
		if($display_tab_results["OBJECT_NAME"][$rec] == 'inv_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'Inventory';
		}
		if($display_tab_results["OBJECT_NAME"][$rec] == 'irb_new_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'IRB New Application';
		}
		if($display_tab_results["OBJECT_NAME"][$rec] == 'irb_pend_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'IRB PIs Area';
		}
		if($display_tab_results["OBJECT_NAME"][$rec] == 'irb_rev_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'IRB Reviewers Area';
		}
		if($display_tab_results["OBJECT_NAME"][$rec] == 'irb_subm_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'IRB Manage Submission';
		}
		if($display_tab_results["OBJECT_NAME"][$rec] == 'lib_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'Library';
		}
		if($display_tab_results["OBJECT_NAME"][$rec] == 'mgpat_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'Search Patient';
		}
		if($display_tab_results["OBJECT_NAME"][$rec] == 'milestone_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'Milestone';
		}
		if($display_tab_results["OBJECT_NAME"][$rec] == 'pat_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'Patient';
		}
		if($display_tab_results["OBJECT_NAME"][$rec] == 'payment_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'Payment';
		}
		if($display_tab_results["OBJECT_NAME"][$rec] == 'persacc_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'Personal Account';
		}
		if($display_tab_results["OBJECT_NAME"][$rec] == 'protocol_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'Protocol Calendar';
		}
		if($display_tab_results["OBJECT_NAME"][$rec] == 'study_tab') {
			$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec] = 'Study';
		}
		
		if($display_tab_results["OBJECT_NAME"][$rec] == $_SESSION["objectname"]){
			$v_tabs.= "<option value=".$display_tab_results["OBJECT_NAME"][$rec]." SELECTED>".$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec]."</option>";
		}else{
			$v_tabs.= "<option value=".$display_tab_results["OBJECT_NAME"][$rec].">".$display_tab_results["OBJECT_NAME_DISPLAYTEXT"][$rec]."</option>";
		}
		
	}
?>
<div class="navigate">Control Key </div>
<FORM name="controlkey" action="ctrlkey.php" method=post >
<div style="float:left; width:880px; margin-bottom:10px; margin-top:10px;">
	<table style="float:left; margin-right:30px;">
		<tr>
			<td style="width:100px;">Select User :</td>
			<td>
				<select name="users" id="users" onChange="refreshPage(document.controlkey.users.value)"><?PHP echo $users; ?></select>
			</td>
		</tr>
	</table>
</div>

<div style="float:left; width:880px; margin-bottom:10px;">	
	<table id="selectmodule" style="float:left; margin-right:30px;">
		<tr>
		<td style="width:100px;">Select Module :</td>
		<td>
			<select name="subtype" id="subtype" onChange="refreshPage(document.controlkey.users.value);"><?PHP echo $v_modules; ?></select>
		</td>
		</tr>			
	</table>
	
	<table id="selecttab" style="float:left;">
		<tr>
		<td style="width:80px;">Select Tabs :</td>
		<td>
			<select name="objectname" id="objectname" onChange="refreshPage(document.controlkey.users.value);"><?PHP echo $v_tabs; ?></select>
		</td>
		</tr>
	</table>
</div>
</FORM>
<div id="moduledata" style="display:none;"></div> 
</div>
</body>
</html>
<?php
}
OCICommit($ds_conn);
OCILogoff($ds_conn);
}
else header("location: index.php?fail=1");
?>