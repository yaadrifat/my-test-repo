<?php
	/*------ Author: Nicholas L -----------*/	
session_start();
header("Cache-control: private");

if(@$_SESSION["user"]){
?>

<html>
<head>
<title>Velos eTools -&gt; Track Patch</title>

<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
<script src="./js/jquery-1.10.2.js"></script>
<script src="./js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
  <script>
  $(function() {
    $( "#tabs" ).tabs();
    //for track patch header class change
    $('#tabs ul').switchClass("ui-widget-header","ui-widget-header-track",1,"easeInOutQuad");
  });
  </script>
</head>
<body>
    <?php
        $oraQry = 'select * from VELINK.ETOOL_TRACK_PATCH order by PK_PATCH desc ';
        $oraresult = executeOCIQuery($oraQry, $ds_conn);
        $mysqlQry = 'select * from ETOOL_TRACK_PATCH order by PK_PATCH desc';
        $mysqlresult = mysql_query($mysqlQry);
        $mysqlcount = mysql_num_rows($mysqlresult);
    ?>
<div id="fedora-content">
	<div class="navigate">Track Patch</div>	
	<br>
    
    <div id="tabs">
        <ul>
        <li><a href="#tabs-1">eTools - Oracle</a></li>
        <li><a href="#tabs-2">MySQL</a></li>
      </ul>
      <div id="tabs-1">
          <p>
            <table width="100%" border="1">
            <tr height="25">
                    <th width="8%">eTool Version</th>                                
                    <th width="8%">Build No</th>
                    <th width="8%">eResearch version</th>
                    <th width="17%">Patch name</th>
                    <th width="10%">Fired on</th>
                    <th width="35%">Query</th>
            </tr>
            <?php
            if($results_nrows >= 1 ){
                for($i=0; $i<$results_nrows; $i++){
                    echo '<tr>';
                        echo '<td>'.$oraresult['ETOOL_VERSION'][$i].'</td>';
                        echo '<td>'.$oraresult['ETOOL_BUILD'][$i].'</td>';
                        echo '<td>'.$oraresult['ERES_VERSION'][$i].'</td>';
                        echo '<td>'.$oraresult['DB_PATCH_NAME'][$i].'</td>';
                        echo '<td>'.$oraresult['FIRED_ON'][$i].'</td>';
                        echo '<td>'.$oraresult['FIRED_QUERY'][$i].'</td>';
                    echo '</tr>';
                }
            }else{
                echo '<tr><td colspan="7">No db patches applied</td></tr>';
            }
            ?>            
            </table>
          </p>
      </div>
      <div id="tabs-2">
          <p>
            <table width="100%" border="1">
            <tr height="25">
                    <th width="8%">eTool Version</th>                                
                    <th width="8%">Build No</th>
                    <th width="8%">eResearch version</th>
                    <th width="17%">Patch name</th>
                    <th width="10%">Fired on</th>
                    <th width="35%">Query</th>
            </tr>
            <?php
            if($mysqlcount >= 1 ){
                while($mysqldata = mysql_fetch_array($mysqlresult)){
                    echo '<tr>';
                        echo '<td>'.$mysqldata['ETOOL_VERSION'].'</td>';
                        echo '<td>'.$mysqldata['ETOOL_BUILD'].'</td>';
                        echo '<td>'.$mysqldata['ERES_VERSION'].'</td>';
                        echo '<td>'.$mysqldata['DB_PATCH_NAME'].'</td>';
                        echo '<td>'.$mysqldata['FIRED_ON'].'</td>';
                        echo '<td>'.$mysqldata['FIRED_QUERY'].'</td>';
                    echo '</tr>';
                }
            }else{
                echo '<tr><td colspan="7">No db patches applied</td></tr>';
            }
            ?>
            </table>
          </p>
      </div>
    </div>    
</div>
</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
