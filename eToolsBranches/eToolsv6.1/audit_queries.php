<?php
    /*------ Author: Nicholas L -----------*/	
    function auditQuery($moduleNumber,$valTofun){
        include("db_config.php");
        $insertQuery ="";
        $moduleElementsArr = array(
            1=>array('MODULE', 'USER_NAME', 'GROUP_NAME', 'MODIFIED_ON', 'CREATED_ON', 'ACTION'),
            2=>array('MODULE', 'USER_NAME', 'GROUP_NAME', 'MODIFIED_ON', 'CREATED_ON',  'ACTION'),
            3=>array('MODULE', 'USER_NAME', 'GROUP_NAME', 'MODIFIED_ON', 'CREATED_ON',  'ACTION', 'LOGIN_TIME'),
            4=>array('MODULE', 'USER_NAME', 'GROUP_NAME', 'MODIFIED_ON', 'QUERY_EXECUTED', 'QUERY_EXECUTED_ON', 'ACTION'),
            5=>array('MODULE', 'USER_NAME', 'GROUP_NAME', 'MODIFIED_ON', 'QUERY_EXECUTED', 'QUERY_EXECUTED_ON', 'ACTION','REMARK'),
            6=>array('MODULE', 'USER_NAME', 'GROUP_NAME', 'MODIFIED_ON', 'CREATED_ON',  'ACTION')
        );
        $moduleElementSize = sizeof($moduleElementsArr[$moduleNumber]);		
        $insertQuery = "INSERT INTO et_audit_row (";
        for($i=0; $i<sizeof($moduleElementsArr[$moduleNumber]); $i++){
            $insertQuery .= $moduleElementsArr[$moduleNumber][$i].',';
        }
        $insertQuery = substr($insertQuery, 0, strlen($insertQuery)-1);
        $insertQuery .= ") VALUES (";

        for($j=0; $j<$moduleElementSize; $j++){
            if($j==0){
                $insertQuery .= $valTofun[$j].',';
            }else{
                if($valTofun[$j] == 'SYSDATE'){
                    $insertQuery .= $valTofun[$j].' ,';
                }else{
                    $insertQuery .= "'".$valTofun[$j]."',";
                }
            }
        }	
        $insertQuery = substr($insertQuery, 0, strlen($insertQuery)-1);
        $insertQuery .= ")";
        $insertQuery = str_replace("SYSDATE","SYSDATE()",$insertQuery);
//        
//        echo $insertQuery;
//        exit;
//        
        $rowResults = mysql_query($insertQuery);
    }

    function colQueries($colA, $colv, $tbln, $oldv, $qman){
        include("db_config.php");
        if($qman != "clob"){
            if($oldv == ""){
                for($ca=0; $ca<sizeof($colA); $ca++){
                    $colSQL = mysql_query("INSERT INTO et_audit_column (FK_AUDITROW, OLD_VALUE, NEW_VALUE, COLUMN_NAME, TABLE_NAME) VALUES ((select MAX(PK_AUDIT) from et_audit_row),'','".$colv[$ca]."','".$colA[$ca]."','".$tbln."')");                    
                    $colSQL = "";
                }
            }else{
                if($qman == "queryman"){
                    for($ca=0; $ca<sizeof($colA); $ca++){
                        $colSQL = mysql_query("INSERT INTO et_audit_column (FK_AUDITROW, OLD_VALUE, NEW_VALUE, COLUMN_NAME, TABLE_NAME) VALUES ((select MAX(PK_AUDIT) from et_audit_row),'".$oldv[$ca]."','".$colv[$ca]."','".$colA[$ca]."','".$tbln."')");
                        $colSQL = "";
                    }
                }else{
                    for($ca=0; $ca<sizeof($colA); $ca++){
                        $colSQL = mysql_query("INSERT INTO et_audit_column (FK_AUDITROW, OLD_VALUE, NEW_VALUE, COLUMN_NAME, TABLE_NAME) VALUES ((select MAX(PK_AUDIT) from et_audit_row),'".$oldv[$ca]."','".$colv[$ca]."','".$colA[$ca]."','".$tbln."')");
                    }
                }
            }
        }else{
            $colSQL = mysql_query("INSERT INTO et_audit_column (FK_AUDITROW, OLD_VALUE_CLOB, NEW_VALUE_CLOB, COLUMN_NAME, TABLE_NAME, REMARKS) VALUES ((select MAX(PK_AUDIT) from et_audit_row),'','','','".$tbln."','clob')");
            $colSQL = mysql_query("UPDATE et_audit_column SET OLD_VALUE_CLOB = '".$colv[0]."', NEW_VALUE_CLOB = '".$colA[0]."', REMARKS ='clob' WHERE FK_AUDITROW=(select MAX(PK_AUDIT) from et_audit_row)");
        }		
    }

    function executeQuery($module, $queryStr, $sflg, $pkquery){
        include("db_config.php");
        $grpName = mysql_query('SELECT group_name from et_groups WHERE pk_groups ='.$_SESSION['FK_GROUPS']);
        $grpNameVal = mysql_fetch_array($grpName);
        $queryStr = str_replace("'",'"', $queryStr);
        if($sflg == 'yes'){
            if($pkquery == ""){
                $rowInfo = array($module,$_SESSION["user"], $grpNameVal['group_name'], "SYSDATE", $queryStr , "SYSDATE", "I");
            }else{
                $rowInfo = array($module,$_SESSION["user"], $grpNameVal['group_name'], "SYSDATE", $queryStr , "SYSDATE", "U");
            }
        }else{
            if($sflg == 'wq'){
                $rowInfo = array($module,$_SESSION["user"], $grpNameVal['group_name'], "SYSDATE", $queryStr , "SYSDATE", "E");
            }else{
                $rowInfo = array($module,$_SESSION["user"], $grpNameVal['group_name'], "SYSDATE", $queryStr , "SYSDATE", "E", "From Query Manager");
            }
        }
//        
//        echo '<pre>';
//        print_r($rowInfo);
//        echo '</pre>';
//        exit;
        auditQuery($module,$rowInfo);
    }
    
    function sessionTracking($param, $pagelink){
        include("db_config.php");
        $usernameQ = mysql_query("select user_name from et_users where pk_users=".$_SESSION['PK_USERS']);
        $usernameQrs = mysql_fetch_array($usernameQ);
        $groupnameQ = mysql_query("select group_name from et_groups where pk_groups=".$_SESSION['FK_GROUPS']);
        $groupnameQrs = mysql_fetch_array($groupnameQ);
        $auditModule = 3;
        switch($param){
            case "loggin":
                $rs = mysql_query("INSERT INTO et_audit_row (USER_NAME, GROUP_NAME, LOGIN_TIME,MODIFIED_ON, MOD_OPT) VALUES ('".$usernameQrs['user_name']."', '".$groupnameQrs['group_name']."',SYSDATE(),SYSDATE(),'optLo')");                                
                $loggedinTime = mysql_query("select LOGIN_TIME from et_audit_row where pk_audit = (SELECT MAX(pk_audit) FROM et_audit_row) and mod_opt = 'optLo'");
                $loggedinTime = mysql_fetch_array($loggedinTime);
                $loggedinTime = $loggedinTime['LOGIN_TIME'];
                $_SESSION['logintime'] = $loggedinTime;
            break;

            case "loggedout":
                $login = $_SESSION['logintime'];                
                $rs = mysql_query("UPDATE et_audit_row SET LOGGEDOUT_TIME =SYSDATE() WHERE MOD_OPT = 'optLo' and LOGIN_TIME = '".$login."' AND USER_NAME ='".$usernameQrs['user_name']."'");
                $loggedoutTimeD = mysql_query("select LOGGEDOUT_TIME from et_audit_row WHERE MOD_OPT = 'optLo' and LOGIN_TIME = '".$login."' AND USER_NAME ='".$usernameQrs['user_name']."'");
                $loggedoutTimeD = mysql_fetch_array($loggedoutTimeD);
                $loggedoutTimeD = $loggedoutTimeD['LOGGEDOUT_TIME'];

                $loggedinTime = new DateTime($login);
                $loggedoutTime = new DateTime($loggedoutTimeD);
                
                $difference = date_diff($loggedinTime, $loggedoutTime);
                
                $days = $difference->d;
                $hours = $difference->h;
                $minutes = $difference->i;
                $seconds = $difference->s;
                $loginDuration = $days.":".$hours.":".$minutes.":".$seconds;
                
                $rs = mysql_query("UPDATE et_audit_row SET USER_NAME ='".$usernameQrs['user_name']."', GROUP_NAME = '".$groupnameQrs['group_name']."', LOGGEDOUT_TIME ='".$loggedoutTimeD."', LOGIN_DURATION = '".$loginDuration."',MOD_OPT = 'optLo' WHERE LOGIN_TIME = '".$login."' AND USER_NAME ='".$usernameQrs['user_name']."'");				                
            break;

            case "pageaccessed":
                $rs = mysql_query("INSERT INTO et_audit_row (MODULE, USER_NAME, GROUP_NAME, ACCESSED_PAGE, MODIFIED_ON,MOD_OPT) VALUES ('".$auditModule."','".$usernameQrs['user_name']."','".$groupnameQrs['group_name']."','".$pagelink."', SYSDATE(),'optPa')");
            break;

            case "datasource":
                $dsname = $_SESSION["DATASOURCE"];
                $dsversion = $_SESSION["VERSION"];				
                $rs = mysql_query("INSERT INTO et_audit_row (MODULE, USER_NAME, GROUP_NAME, DS_NAME, DS_HOST, DS_VERSION, MODIFIED_ON, MOD_OPT) VALUES ('".$auditModule."','".$usernameQrs['user_name']."','".$groupnameQrs['group_name']."','".$dsname."','".$_SESSION["DS_HOST"]."', '".$dsversion."',SYSDATE(),'optDa')");                
            break;
        }		
    }    
?>