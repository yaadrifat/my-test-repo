<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Create Query from Views</title>
</head>
<?php
include("db_config.php");
include("./includes/header.php");

include_once "./adodb/adodb.inc.php";
include_once('./adodb/adodb-pager.inc.php'); 
include_once('./adodb/pivottable.inc.php'); 

$db = NewADOConnection("oci8");
$db->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);


if (!$db) die("Connection failed");

	
?>
<body>
<div id="fedora-content">	

<div class="navigate">Create Query from Views - Select View</div>
<?php

$v_tables = $db->MetaTables('VIEWS');
echo "<BR>";
echo '<TABLE width="100%">';
$counter = 0;
for ($i=0;$i<count($v_tables);$i++){
	if ($counter==0) echo "<TR>";
	echo '<td onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';"><a href="query_adodb_viewcols.php?table='.$v_tables[$i].'">'.$v_tables[$i].'</a></td>';
	$counter++;
	if ($counter==3) $counter = 0;
	if ($counter==0) echo "</TR>";
}
	
echo "</TABLE>";
?>


</div>


</body>
</html>
<?php 
$db->Close();

}
else header("location: index.php?fail=1");
?>
