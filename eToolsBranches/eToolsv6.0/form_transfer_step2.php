<?php
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Form Transfer</title>

<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");
include_once("./adodb/adodb.inc.php");
//$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]);
$source_db = NewADOConnection("oci8");
$source_db->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);

?>

 <SCRIPT language="JavaScript">
function submitform(flag)
{
	if (flag == "refresh") {
		formtransfer.flag.value = "refresh"
		document.formtransfer.submit();
	} else {
		formtransfer.flag.value = "submit"
		document.formtransfer.submit();
	}
}

function VerfiyValidation() {
	if(document.getElementById("formname").value == ''){
		alert("Form Name should not be empty");
		document.getElementById("formname").focus();
		return false;
	}
}

</SCRIPT>

</head>
<body>
<div id="fedora-content">	
<div class="navigate">Form Transfer</div>
<?php
$cat_dropdown = '';

$v_formname = (isset($_GET["formname"])) ?  trim($_GET["formname"]) : trim($_POST["formname"]);
$v_pk_formlib = (isset($_GET["pk_formlib"])) ?  $_GET["pk_formlib"] : $_POST["pk_formlib"];
$v_flag = (isset($_POST['flag'])?$_POST['flag']:"refresh");

// prepares datasource dropdown
if ($v_flag == "refresh") {	
//$db = new Database("etools");
//$result = $db->executeQuery("SELECT DS_RIGHTS from ET_GROUPS where PK_GROUPS=".$_SESSION['FK_GROUPS']);
$result = mysql_query("SELECT ds_rights from et_groups where pk_groups=".$_SESSION['FK_GROUPS']);
$dropdown = '';
$counter=0;
//if ($result->getRowCount() > 0) {
if (mysql_num_rows($result) > 0) {
    while ($data = mysql_fetch_array($result)) {
            //$data = $result->getCurrentValuesAsHash();
            $v_ds_rights = explode("|",$data['ds_rights']);
            foreach ($v_ds_rights as $v_ds){
                    list($pk_ds,$ds_access) = explode(":",$v_ds);
                    if ($ds_access == 1) {
                            if ($counter == 0){
                                    if (isset($_POST["DS"])){
                                            $ds = $_POST['DS'];
                                    } else {
                                            $ds = $pk_ds;
                                    }
                                    $counter++;
                            }
                            //$rs = $db->executeQuery("SELECT PK_DS,DS_NAME from ET_DS where PK_DS=".$pk_ds);
                            $rs = mysql_query("SELECT pk_ds, ds_name from et_ds where pk_ds=".$pk_ds);
                            while ($ds_data = mysql_fetch_array($rs)) {
                                    //$ds_data = $rs->getCurrentValuesAsHash();
                                    if ($ds == $ds_data["pk_ds"]){
                                        $dropdown .= "<option selected value=".$ds_data["pk_ds"].">".$ds_data["ds_name"]."</option>";
                                    } else {
                                        $dropdown .= "<option value=".$ds_data["pk_ds"].">".$ds_data["ds_name"]."</option>";
                                    }
                            }
                    }
            }
    } 
} 

//selects the target db from user
        $rs = mysql_query("SELECT pk_ds, ds_name, ds_host, ds_port, ds_sid, ds_pass from et_ds where pk_ds =".$ds);
	while ($ds_datars = mysql_fetch_array($rs)) {
		//$ds_data = $rs->getCurrentValuesAsHash();
		//$connect_string = '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST='.$ds_data["DS_HOST"].')(PORT='.$ds_data["DS_PORT"].'))(CONNECT_DATA=(SID='.$ds_data["DS_SID"].')))';		
		//$connect_string = "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=".$ds_data["DS_HOST"].")(PORT=".$ds_data["DS_PORT"]."))(CONNECT_DATA=(SERVICE_NAME=".$ds_data["DS_SID"].")))";		
		$connect_string = "(DESCRIPTION=(ADDRESS_LIST = (ADDRESS = (PROTOCOL=TCP)(HOST=".$ds_datars["ds_host"].")(PORT=".$ds_datars["ds_port"].")))(CONNECT_DATA=(SERVICE_NAME=".$ds_datars["ds_sid"].")))";                
                $ds_passrs = $ds_datars["ds_pass"];
	}
	$target_db = NewADOConnection("oci8");
	$target_db->Connect($connect_string, "eres", $ds_passrs);
    //selects form category
	$v_sql = "select pk_catlib || '|' || fk_account as pk_catlib,catlib_name || decode(ac_name,null,'',' [' || ac_name || ']') as catlib_name from er_catlib,er_account WHERE pk_account = fk_account and catlib_type = 'T' and record_type <> 'D' order by ac_name || ' - ' || lower(catlib_name)";    
	$catrs = $target_db->Execute($v_sql);
        //$catrs = executeOCIQuery($v_sql,$ds_connt);	
        
        //--- paranthis is missing {}---- 22-06-2015 for the vtiger issue//
	if($catrs){
            $cat_dropdown = '';
            while (!$catrs->EOF) {
                $cat_dropdown .= "<option value=".$catrs->fields["PK_CATLIB"].">".$catrs->fields["CATLIB_NAME"]."</option>";
                $catrs->MoveNext();
            }
       }
         
         

?>
<FORM name="formtransfer" action="form_transfer_step2.php" method="post" onSubmit="return VerfiyValidation();">
<input type="hidden" name="pk_formlib" value="<?php echo $v_pk_formlib; ?>"></input>

<TABLE width="100%" border="1">
<TR><TD width="20%">Form Name</TD><TD width="27%"><input class="required" style="border:none;" size="50" type=text name="formname" id="formname" value="<?PHP if(!$v_formname){
	echo $_GET['frmname'];
	}else{
		echo $v_formname;
	}
 ?>"></TD>
  <TD width="53%" id="form_name_exists"></TD>
</TR>
<TR><TD>Transfer Form to</TD><TD colspan="2"><select class="required" name="DS" onChange="submitform('refresh')"><?PHP echo $dropdown; ?></select></TD></TR>
<TR><TD>Category</TD><TD colspan="2"><select class="required" name="category"><?PHP echo $cat_dropdown; ?></select></TD></TR>
<TR>
  <TD><input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" /> </TD>
  <TD colspan="2">&nbsp;</TD>
</TR>

<tr><td>&nbsp;</td><td colspan="3"><?php if(@$_GET["frmempty"]) echo "<font size=2 color=red><b>Selected form is empty</b></font>"; ?></td></tr>
</TABLE>
<input type="hidden" name="frmname" value="<?PHP echo $v_formname; ?>" />
<input type="hidden" name="flag" value="submit" />
</FORM>
<?php 
}else{
$v_pk_formlib = $_POST["pk_formlib"];
list($v_catlib,$v_account) = explode("|",$_POST["category"]);

$ds = $_POST['DS'];

//$rs = $db->executeQuery("SELECT PK_DS,DS_NAME,DS_HOST,DS_PORT,DS_SID,DS_PASS from ET_DS where PK_DS=".$ds);
$rs_1 = mysql_query("SELECT pk_ds, ds_name, ds_host, ds_port, ds_sid, ds_pass from et_ds where pk_ds=".$ds);
while ($ds_data1 = mysql_fetch_array($rs_1)) {
    //$ds_data = $rs->getCurrentValuesAsHash();
    $connect_string = '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST='.$ds_data1["ds_host"].')(PORT='.$ds_data1["ds_port"].'))(CONNECT_DATA=(SID='.$ds_data1["ds_sid"].')))';
    $ds_pass = $ds_data1["ds_pass"];
}

$target_db = NewADOConnection("oci8");
$target_db->Connect($connect_string, "eres", $ds_pass);


// ER_FORMLIB
//echo "ER_FORMLIB......................<BR>";
//$v_sql = "select pk_formlib,form_name from er_formlib where form_name = '".$v_formname."'"; // bug 15218
// earlier it was coded in a wrong way - $source_db was used instead of $target_db

$v_sql = "select pk_formlib,form_name from er_formlib where lower(form_name) = lower('".$v_formname."') and fk_account = '".$v_account."' and FORM_LINKTO = 'L' and (RECORD_TYPE = 'N' or RECORD_TYPE='M')";
$rs_2 = $target_db->Execute($v_sql);

if($rs_2->fields["FORM_NAME"] == '') {
    $v_sql = "select pk_formlib,form_name,form_desc,form_xsl,a.form_xml.getclobval() as form_xml,form_viewxsl from er_formlib a where pk_formlib = ".$v_pk_formlib;
    $rs_3 = $source_db->Execute($v_sql);
    $target_db->BeginTrans();
    $v_error = "";
    $v_status = true;

if (!$rs_3) 
	print $source_db->ErrorMsg();
else {	
    while (!$rs_3->EOF){
        $recordSet = $target_db->Execute("SELECT pk_codelst from er_codelst where codelst_type='frmlibstat' and codelst_subtyp='W'");
        $v_form_status =  $recordSet->fields[0];
        $recordSet1 = $target_db->Execute("select seq_er_formlib.nextval from dual");
        $v_pk_formlib_tar =  $recordSet1->fields[0];

        if ($v_status) {                    
                $v_status = $target_db->Execute("insert into er_formlib (pk_formlib,fk_account,fk_catlib,form_name,form_desc,record_type,form_sharedwith,form_status,form_linkto,FORM_XSLREFRESH,form_saveformat) values ($v_pk_formlib_tar,$v_account,$v_catlib,'".str_replace("'","''",$_REQUEST["formname"])."','".str_replace("'","''",$rs_3->fields["FORM_DESC"])."','N','A',$v_form_status,'L',1,0)");
                $v_error = (!$v_status) ? $target_db->ErrorMsg()." ER_FORMLIB " : "";
        }

        if ($v_status) {
            if($rs_3->fields["FORM_XSL"]!=""){
//                echo "form xsl"."<br>";
//                echo $rs_3->fields["FORM_XSL"];
//                exit;                
                $v_status = $target_db->UpdateClob('er_formlib','form_xsl',$rs_3->fields["FORM_XSL"],'pk_formlib='.$v_pk_formlib_tar);
                $v_error = (!$v_status) ? $target_db->ErrorMsg()." ER_FORMLIB(form_xsl)" : ""; 
            }
        }

        if ($v_status) {
            if($rs_3->fields["FORM_XML"]!=""){
//                echo "form xml"."<br>";
//                var_dump($rs_3->fields["FORM_XML"]);
//                exit;                
                $v_status = $target_db->UpdateClob('er_formlib','form_viewxsl',$rs_3->fields["FORM_XML"],'pk_formlib='.$v_pk_formlib_tar); 
                $v_error = (!$v_status) ? $target_db->ErrorMsg()." ER_FORMLIB(form_viewxsl)" : ""; 
            }
        }

        if ($v_status) {
            if($rs_3->fields["FORM_VIEWXSL"]!=""){
//                echo "form viewxsl"."<br>";
//                var_dump($rs_3->fields["FORM_VIEWXSL"]);
//                exit;                
//                
                $v_status = $target_db->Execute("update er_formlib set form_xml = sys.xmltype.createxml(form_viewxsl) where pk_formlib=$v_pk_formlib_tar");
                $v_error = (!$v_status) ? $target_db->ErrorMsg()." ER_FORMLIB(form_xml)" : ""; 			
            }
        }

        if ($v_status) {
            if($rs_3->fields["FORM_VIEWXSL"]!=""){
//                echo "form viewxsl 2"."<br>";
//                var_dump($rs_3->fields["FORM_VIEWXSL"]);
//                exit;                 
                $v_status = $target_db->UpdateClob('er_formlib','form_viewxsl',$rs_3->fields["FORM_VIEWXSL"],'pk_formlib='.$v_pk_formlib_tar); 
                $v_error = (!$v_status) ? $target_db->ErrorMsg()." ER_FORMLIB(form_viewxsl)" : ""; 
            }
        }

        $rs_3->MoveNext();
    }
}


// ER_OBJECTSHARE
//echo "ER_OBJECTSHARE......................<BR>";
$recordSet = $target_db->Execute("SELECT seq_er_objectshare.NEXTVAL as pk_objectshare FROM dual");
$v_pk_objectshare = $recordSet->fields[0];

if ($v_status) {
	$v_status = $target_db->Execute("insert into er_objectshare (pk_objectshare,object_number,fk_object,fk_objectshare_id,objectshare_type,record_type) values ($v_pk_objectshare,1,$v_pk_formlib_tar,$v_account,'A','N')");
	$v_error = (!$v_status) ? $target_db->ErrorMsg()." er_objectshare" : ""; 
}

if ($v_status) {
	$v_status = $target_db->Execute("insert into er_objectshare (pk_objectshare,object_number,fk_object,fk_objectshare_id,objectshare_type,record_type,fk_objectshare) select seq_er_objectshare.NEXTVAL,1,$v_pk_formlib_tar,pk_user,'U','N',$v_pk_objectshare from er_user where fk_account = $v_account and usr_type <> 'X'");
	$v_error = (!$v_status) ? $target_db->ErrorMsg()." er_objectshare" : ""; 
}

// ER_FORMSEC
//echo "ER_FORMSEC......................<BR>";
$v_sql = "select pk_formsec,formsec_name,formsec_seq,formsec_fmt,formsec_repno from er_formsec a where record_type <> 'D' and fk_formlib = ".$v_pk_formlib;
$rs = $source_db->Execute($v_sql);
$v_counter = 0;
if (!$rs) 
	print $source_db->ErrorMsg();
else {
	while (!$rs->EOF) {
	$recordSet = $target_db->Execute("select seq_er_formsec.nextval from dual");
	$v_pk_formsec =  $recordSet->fields[0];
	$v_src_formsec[$v_counter] = $rs->fields["PK_FORMSEC"];
	$v_tar_formsec[$v_counter] = $v_pk_formsec;

	if ($v_status) {
		$v_status = $target_db->Execute("insert into er_formsec (pk_formsec,fk_formlib,formsec_name,formsec_seq,formsec_fmt,formsec_repno,record_type) values ($v_pk_formsec,$v_pk_formlib_tar,'".str_replace("'","''",$rs->fields["FORMSEC_NAME"])."',".$rs->fields["FORMSEC_SEQ"].",'".$rs->fields["FORMSEC_FMT"]."',".$rs->fields["FORMSEC_REPNO"].",'N')");
		$v_error = (!$v_status) ? $target_db->ErrorMsg()." er_formsec" : ""; 
	}

	$v_counter++;
	$rs->MoveNext();
	}
}

// ER_FLDLIB
//echo "ER_FLDLIB......................<BR>";
$v_sql = "select PK_FIELD,FLD_LIBFLAG,replace(FLD_NAME,'''','''''') as fld_name,FLD_DESC,FLD_UNIQUEID,FLD_SYSTEMID,FLD_KEYWORD,FLD_TYPE,FLD_DATATYPE,FLD_INSTRUCTIONS,FLD_LENGTH,FLD_DECIMAL,FLD_LINESNO,FLD_CHARSNO,FLD_DEFRESP,FK_LOOKUP,FLD_ISUNIQUE,FLD_ISREADONLY,FLD_ISVISIBLE,FLD_COLCOUNT,FLD_FORMAT,FLD_REPEATFLAG,FLD_BOLD,FLD_ITALICS,FLD_SAMELINE,FLD_ALIGN,FLD_UNDERLINE,FLD_COLOR,FLD_FONT,a.FLD_FONTSIZE,FLD_LKPDATAVAL,FLD_LKPDISPVAL,FLD_TODAYCHECK,FLD_OVERRIDE_MANDATORY,FLD_LKPTYPE,FLD_EXPLABEL,FLD_OVERRIDE_FORMAT,FLD_OVERRIDE_RANGE,FLD_OVERRIDE_DATE,FLD_HIDELABEL,FLD_HIDERESPLABEL,FLD_DISPLAY_WIDTH,FLD_LINKEDFORM,FLD_RESPALIGN,FLD_NAME_FORMATTED,FLD_SORTORDER from ER_FLDLIB a,ER_FORMFLD,ER_FORMSEC,ER_FORMLIB WHERE a.record_type <> 'D' and pk_field = fk_field AND pk_formsec = fk_formsec AND pk_formlib = fk_formlib AND pk_formlib = ".$v_pk_formlib;
$rs = $source_db->Execute($v_sql);
$v_counter = 0;
$v_counter_resp = 0;
if (!$rs) 
	print $source_db->ErrorMsg();
else {
	while (!$rs->EOF) {
		$recordSet = $target_db->Execute("select seq_er_fldlib.nextval from dual");
		$v_pk_fldlib =  $recordSet->fields[0];
		$v_src_fldlib[$v_counter] = $rs->fields["PK_FIELD"];
		$v_tar_fldlib[$v_counter] = $v_pk_fldlib;
//echo "------------------------------------------------------------------------------<BR>";

		if ($v_status) {
			$v_status = $target_db->Execute("insert into er_fldlib (PK_FIELD,FK_ACCOUNT,FLD_LIBFLAG,FLD_NAME,FLD_DESC,FLD_UNIQUEID,
			FLD_SYSTEMID,FLD_KEYWORD,FLD_TYPE,FLD_DATATYPE,FLD_INSTRUCTIONS,FLD_LENGTH,FLD_DECIMAL,FLD_LINESNO,FLD_CHARSNO,FLD_DEFRESP,
			FK_LOOKUP,FLD_ISUNIQUE,FLD_ISREADONLY,FLD_ISVISIBLE,FLD_COLCOUNT,FLD_FORMAT,FLD_REPEATFLAG,FLD_BOLD,FLD_ITALICS,FLD_SAMELINE,
			FLD_ALIGN,FLD_UNDERLINE,FLD_COLOR,FLD_FONT,RECORD_TYPE,FLD_FONTSIZE,FLD_LKPDATAVAL,FLD_LKPDISPVAL,FLD_TODAYCHECK,
			FLD_OVERRIDE_MANDATORY,FLD_LKPTYPE,FLD_EXPLABEL,FLD_OVERRIDE_FORMAT,FLD_OVERRIDE_RANGE,FLD_OVERRIDE_DATE,FLD_HIDELABEL,
			FLD_HIDERESPLABEL,FLD_DISPLAY_WIDTH,FLD_LINKEDFORM,FLD_RESPALIGN,FLD_NAME_FORMATTED,FLD_SORTORDER) values 
			($v_pk_fldlib,$v_account,'F','".str_replace("'","''",$rs->fields["FLD_NAME"])."','".str_replace("'","''",$rs->fields["FLD_DESC"]).
			"','".str_replace("'","''",$rs->fields["FLD_UNIQUEID"])."','".$rs->fields["FLD_SYSTEMID"]."','".$rs->fields["FLD_KEYWORD"]."','".$rs->fields["FLD_TYPE"].
			"','".$rs->fields["FLD_DATATYPE"]."','".str_replace("'","''",$rs->fields["FLD_INSTRUCTIONS"])."',".
			((strlen($rs->fields["FLD_LENGTH"]) == 0)?"NULL":$rs->fields["FLD_LENGTH"]).",".
			((strlen($rs->fields["FLD_DECIMAL"]) == 0)?"NULL":$rs->fields["FLD_DECIMAL"]).",".
			((strlen($rs->fields["FLD_LINESNO"]) == 0)?"NULL":$rs->fields["FLD_LINESNO"]).",".
			((strlen($rs->fields["FLD_CHARSNO"]) == 0)?"NULL":$rs->fields["FLD_CHARSNO"]).",'".$rs->fields["FLD_DEFRESP"]."',".
			((strlen($rs->fields["FK_LOOKUP"]) == 0)?"NULL":$rs->fields["FK_LOOKUP"]).",".
			((strlen($rs->fields["FLD_ISUNIQUE"]) == 0)?"NULL":$rs->fields["FLD_ISUNIQUE"]).",".
			((strlen($rs->fields["FLD_ISREADONLY"]) == 0)?"NULL":$rs->fields["FLD_ISREADONLY"]).",".
			((strlen($rs->fields["FLD_ISVISIBLE"]) == 0)?"NULL":$rs->fields["FLD_ISVISIBLE"]).",".
			(strlen($rs->fields["FLD_COLCOUNT"] == 0)?"NULL":$rs->fields["FLD_COLCOUNT"]).",'".$rs->fields["FLD_FORMAT"]."',".
			((strlen($rs->fields["FLD_REPEATFLAG"]) ==0 )?"NULL":$rs->fields["FLD_REPEATFLAG"]).",".
			((strlen($rs->fields["FLD_BOLD"]) == 0)?"NULL":$rs->fields["FLD_BOLD"]).",".
			((strlen($rs->fields["FLD_ITALICS"]) == 0)?"NULL":$rs->fields["FLD_ITALICS"]).",".
			((strlen($rs->fields["FLD_SAMELINE"]) == 0)?"NULL":$rs->fields["FLD_SAMELINE"]).",'".$rs->fields["FLD_ALIGN"]."',".
			((strlen($rs->fields["FLD_UNDERLINE"]) == 0)?"NULL":$rs->fields["FLD_UNDERLINE"]).",'".$rs->fields["FLD_COLOR"]."','".$rs->fields["FLD_FONT"]."','N',".
			((strlen($rs->fields["FLD_FONTSIZE"]) == 0)?"NULL":$rs->fields["FLD_FONTSIZE"]).",'".$rs->fields["FLD_LKPDATAVAL"]."','".$rs->fields["FLD_LKPDISPVAL"]."',".
			((strlen($rs->fields["FLD_TODAYCHECK"]) == 0)?"NULL":$rs->fields["FLD_TODAYCHECK"]).",".
			((strlen($rs->fields["FLD_OVERRIDE_MANDATORY"]) == 0)?"NULL":$rs->fields["FLD_OVERRIDE_MANDATORY"]).",'".$rs->fields["FLD_LKPTYPE"]."',".
			((strlen($rs->fields["FLD_EXPLABEL"]) == 0)?"NULL":$rs->fields["FLD_EXPLABEL"]).",".
			((strlen($rs->fields["FLD_OVERRIDE_FORMAT"]) == 0)?"NULL":$rs->fields["FLD_OVERRIDE_FORMAT"]).",".
			((strlen($rs->fields["FLD_OVERRIDE_RANGE"]) == 0)?"NULL":$rs->fields["FLD_OVERRIDE_RANGE"]).",".
			((strlen($rs->fields["FLD_OVERRIDE_DATE"]) == 0)?"NULL":$rs->fields["FLD_OVERRIDE_DATE"]).",".
			((strlen($rs->fields["FLD_HIDELABEL"]) == 0)?"NULL":$rs->fields["FLD_HIDELABEL"]).",".
			((strlen($rs->fields["FLD_HIDERESPLABEL"]) == 0)?"NULL":$rs->fields["FLD_HIDERESPLABEL"]).",".
			((strlen($rs->fields["FLD_DISPLAY_WIDTH"]) == 0)?"NULL":$rs->fields["FLD_DISPLAY_WIDTH"]).",'".$rs->fields["FLD_LINKEDFORM"]."','".$rs->fields["FLD_RESPALIGN"]."','".
			str_replace("'","''",$rs->fields["FLD_NAME_FORMATTED"])."','".$rs->fields["FLD_SORTORDER"]."')");
			$v_error = (!$v_status) ? $target_db->ErrorMsg()." er_fldlib" : ""; 
			if (!$v_status) echo "insert into er_fldlib (PK_FIELD,FK_ACCOUNT,FLD_LIBFLAG,FLD_NAME,FLD_DESC,FLD_UNIQUEID,FLD_SYSTEMID,FLD_KEYWORD,FLD_TYPE,FLD_DATATYPE,FLD_INSTRUCTIONS,FLD_LENGTH,FLD_DECIMAL,FLD_LINESNO,FLD_CHARSNO,FLD_DEFRESP,FK_LOOKUP,FLD_ISUNIQUE,FLD_ISREADONLY,FLD_ISVISIBLE,FLD_COLCOUNT,FLD_FORMAT,FLD_REPEATFLAG,FLD_BOLD,FLD_ITALICS,FLD_SAMELINE,FLD_ALIGN,FLD_UNDERLINE,FLD_COLOR,FLD_FONT,RECORD_TYPE,FLD_FONTSIZE,FLD_LKPDATAVAL,FLD_LKPDISPVAL,FLD_TODAYCHECK,FLD_OVERRIDE_MANDATORY,FLD_LKPTYPE,FLD_EXPLABEL,FLD_OVERRIDE_FORMAT,FLD_OVERRIDE_RANGE,FLD_OVERRIDE_DATE,FLD_HIDELABEL,FLD_HIDERESPLABEL,FLD_DISPLAY_WIDTH,FLD_LINKEDFORM,FLD_RESPALIGN,FLD_NAME_FORMATTED,FLD_SORTORDER) values ($v_pk_fldlib,$v_account,'F','".str_replace("'","''",$rs->fields["FLD_NAME"])."','".str_replace("'","''",$rs->fields["FLD_DESC"])."','".$rs->fields["FLD_UNIQUEID"]."','".$rs->fields["FLD_SYSTEMID"]."','".$rs->fields["FLD_KEYWORD"]."','".$rs->fields["FLD_TYPE"]."','".$rs->fields["FLD_DATATYPE"]."','".str_replace("'","''",$rs->fields["FLD_INSTRUCTIONS"])."',".(empty($rs->fields["FLD_LENGTH"])?"NULL":$rs->fields["FLD_LENGTH"]).",".(empty($rs->fields["FLD_DECIMAL"])?"NULL":$rs->fields["FLD_DECIMAL"]).",".(empty($rs->fields["FLD_LINESNO"])?"NULL":$rs->fields["FLD_LINESNO"]).",".(empty($rs->fields["FLD_CHARSNO"])?"NULL":$rs->fields["FLD_CHARSNO"]).",'".$rs->fields["FLD_DEFRESP"]."',".(empty($rs->fields["FK_LOOKUP"])?"NULL":$rs->fields["FK_LOOKUP"]).",".(empty($rs->fields["FLD_ISUNIQUE"])?"NULL":$rs->fields["FLD_ISUNIQUE"]).",".(empty($rs->fields["FLD_ISREADONLY"])?"NULL":$rs->fields["FLD_ISREADONLY"]).",".(empty($rs->fields["FLD_ISVISIBLE"])?"NULL":$rs->fields["FLD_ISVISIBLE"]).",".(empty($rs->fields["FLD_COLCOUNT"])?"NULL":$rs->fields["FLD_COLCOUNT"]).",'".$rs->fields["FLD_FORMAT"]."',".(empty($rs->fields["FLD_REPEATFLAG"])?"NULL":$rs->fields["FLD_REPEATFLAG"]).",".(empty($rs->fields["FLD_BOLD"])?"NULL":$rs->fields["FLD_BOLD"]).",".(empty($rs->fields["FLD_ITALICS"])?"NULL":$rs->fields["FLD_ITALICS"]).",".(empty($rs->fields["FLD_SAMELINE"])?"NULL":$rs->fields["FLD_SAMELINE"]).",'".$rs->fields["FLD_ALIGN"]."',".(empty($rs->fields["FLD_UNDERLINE"])?"NULL":$rs->fields["FLD_UNDERLINE"]).",'".$rs->fields["FLD_COLOR"]."','".$rs->fields["FLD_FONT"]."','N',".(empty($rs->fields["FLD_FONTSIZE"])?"NULL":$rs->fields["FLD_FONTSIZE"]).",'".$rs->fields["FLD_LKPDATAVAL"]."','".$rs->fields["FLD_LKPDISPVAL"]."',".(empty($rs->fields["FLD_TODAYCHECK"])?"NULL":$rs->fields["FLD_TODAYCHECK"]).",".(empty($rs->fields["FLD_OVERRIDE_MANDATORY"])?"NULL":$rs->fields["FLD_OVERRIDE_MANDATORY"]).",'".$rs->fields["FLD_LKPTYPE"]."',".(empty($rs->fields["FLD_EXPLABEL"])?"NULL":$rs->fields["FLD_EXPLABEL"]).",".(empty($rs->fields["FLD_OVERRIDE_FORMAT"])?"NULL":$rs->fields["FLD_OVERRIDE_FORMAT"]).",".(empty($rs->fields["FLD_OVERRIDE_RANGE"])?"NULL":$rs->fields["FLD_OVERRIDE_RANGE"]).",".(empty($rs->fields["FLD_OVERRIDE_DATE"])?"NULL":$rs->fields["FLD_OVERRIDE_DATE"]).",".(empty($rs->fields["FLD_HIDELABEL"])?"NULL":$rs->fields["FLD_HIDELABEL"]).",".(empty($rs->fields["FLD_HIDERESPLABEL"])?"NULL":$rs->fields["FLD_HIDERESPLABEL"]).",".(empty($rs->fields["FLD_DISPLAY_WIDTH"])?"NULL":$rs->fields["FLD_DISPLAY_WIDTH"]).",'".$rs->fields["FLD_LINKEDFORM"]."','".$rs->fields["FLD_RESPALIGN"]."','".$rs->fields["FLD_NAME_FORMATTED"]."','".$rs->fields["FLD_SORTORDER"]."')";
		}
		
		// ER_FLDRESP
//		echo "ER_FLDRESP......................<BR>";
		$v_sql = "select PK_FLDRESP,FK_FIELD,FLDRESP_SEQ,FLDRESP_DISPVAL,FLDRESP_DATAVAL,FLDRESP_SCORE,FLDRESP_ISDEFAULT,RECORD_TYPE from ER_fldresp WHERE record_type <> 'D' and FK_FIELD = ".$rs->fields["PK_FIELD"];
		$rs1 = $source_db->Execute($v_sql);
		if (!$rs1) print $source_db->ErrorMsg();
		while (!$rs1->EOF) {

//echo "insert into er_fldresp (PK_FLDRESP,FK_FIELD,FLDRESP_SEQ,FLDRESP_DISPVAL,FLDRESP_DATAVAL,FLDRESP_SCORE,FLDRESP_ISDEFAULT,RECORD_TYPE) values (seq_er_fldresp.nextval,$v_pk_fldlib,".$rs1->fields["FLDRESP_SEQ"].",'".$rs1->fields["FLDRESP_DISPVAL"]."','".$rs1->fields["FLDRESP_DATAVAL"]."',".(empty($rs1->fields["FLDRESP_SCORE"])?"NULL":$rs1->fields["FLDRESP_SCORE"]).",".$rs1->fields["FLDRESP_ISDEFAULT"].",'N')";
			if ($v_status) {
				$rs2 = $target_db->Execute("select seq_er_fldresp.nextval from dual");
				$v_pk_fldresp =  $rs2->fields[0];
				$v_src_fldresp[$v_counter_resp] = $rs1->fields["PK_FLDRESP"];
				$v_tar_fldresp[$v_counter_resp] = $v_pk_fldresp;
				$v_status = $target_db->Execute("insert into er_fldresp (PK_FLDRESP,FK_FIELD,FLDRESP_SEQ,FLDRESP_DISPVAL,FLDRESP_DATAVAL,
				FLDRESP_SCORE,FLDRESP_ISDEFAULT,RECORD_TYPE) values ($v_pk_fldresp,$v_pk_fldlib,".$rs1->fields["FLDRESP_SEQ"].
				",'".str_replace("'","''",$rs1->fields["FLDRESP_DISPVAL"])."','".
				str_replace("'","''",$rs1->fields["FLDRESP_DATAVAL"])."',".
				((strlen($rs1->fields["FLDRESP_SCORE"]) == 0)?"NULL":$rs1->fields["FLDRESP_SCORE"]).",".
				((strlen($rs1->fields["FLDRESP_ISDEFAULT"]) == 0)?"NULL":$rs1->fields["FLDRESP_ISDEFAULT"]).",'".$rs1->fields["RECORD_TYPE"]."')");
				$v_error = (!$v_status) ? $target_db->ErrorMsg()." er_fldresp" : ""; 
				if (!$v_status) echo "insert into er_fldresp (PK_FLDRESP,FK_FIELD,FLDRESP_SEQ,FLDRESP_DISPVAL,FLDRESP_DATAVAL,FLDRESP_SCORE,FLDRESP_ISDEFAULT,RECORD_TYPE) values (seq_er_fldresp.nextval,$v_pk_fldlib,".$rs1->fields["FLDRESP_SEQ"].",'".$rs1->fields["FLDRESP_DISPVAL"]."','".$rs1->fields["FLDRESP_DATAVAL"]."',".(empty($rs1->fields["FLDRESP_SCORE"])?"NULL":$rs1->fields["FLDRESP_SCORE"]).",".(empty($rs1->fields["FLDRESP_ISDEFAULT"])?"NULL":$rs1->fields["FLDRESP_ISDEFAULT"]).",'".$rs1->fields["RECORD_TYPE"]."')";
			}
			
			$v_counter_resp++;
			$rs1->MoveNext();
		}
//echo "------------------------------------------------------------------------------<BR>";

		// ER_FLDVALIDATE
//		echo "ER_FLDVALIDATE......................<BR>";

		$v_sql = "select PK_FLDVALIDATE,FK_FLDLIB,FLDVALIDATE_OP1,FLDVALIDATE_VAL1,FLDVALIDATE_LOGOP1,FLDVALIDATE_OP2,FLDVALIDATE_VAL2,FLDVALIDATE_LOGOP2,FLDVALIDATE_JAVASCR,RECORD_TYPE from ER_fldvalidate WHERE record_type <> 'D' and FK_FLDLIB = ".$rs->fields["PK_FIELD"];
		$rs1 = $source_db->Execute($v_sql);
                
                //fix for the bug 20829
                    $fldjava = str_replace("'","''",$rs8->fields["FLDVALIDATE_JAVASCR"]);
                //echo $rs1->fields["FLDVALIDATE_JAVASCR"];
                //echo $v_sql
                //exit;
                
                
		if (!$rs1) print $source_db->ErrorMsg();
		if (isset($rs1->fields["FLDVALIDATE_OP1"])){
			if ($v_status) {
				$v_status = $target_db->Execute("insert into er_fldvalidate (PK_FLDVALIDATE,FK_FLDLIB,FLDVALIDATE_OP1,FLDVALIDATE_VAL1,FLDVALIDATE_LOGOP1,FLDVALIDATE_OP2,FLDVALIDATE_VAL2,FLDVALIDATE_LOGOP2,FLDVALIDATE_JAVASCR,RECORD_TYPE) values (seq_er_fldvalidate.nextval,$v_pk_fldlib,'".$rs1->fields["FLDVALIDATE_OP1"]."','".$rs1->fields["FLDVALIDATE_VAL1"]."','".$rs1->fields["FLDVALIDATE_LOGOP1"]."','".$rs1->fields["FLDVALIDATE_OP2"]."','".$rs1->fields["FLDVALIDATE_VAL2"]."','".$rs1->fields["FLDVALIDATE_LOGOP2"]."','".$rs1->fields["FLDVALIDATE_JAVASCR"]."','N')");
				$v_error = (!$v_status) ? $target_db->ErrorMsg()." er_fldvalidate"  : ""; 
			}
		} else {
			if ($v_status) {
				$v_status = $target_db->Execute("insert into er_fldvalidate (PK_FLDVALIDATE,FK_FLDLIB,RECORD_TYPE) values (seq_er_fldvalidate.nextval,$v_pk_fldlib,'N')");
				$v_error = (!$v_status) ? $target_db->ErrorMsg()." er_fldvalidate" : ""; 
			}
		}

		// ER_FORMFLD
//		echo "ER_FORMFLD......................<BR>";
		$v_sql = "select PK_FORMFLD,FK_FORMSEC,FK_FIELD,FORMFLD_SEQ,FORMFLD_MANDATORY,FORMFLD_BROWSERFLG,FORMFLD_XSL,FORMFLD_JAVASCR from ER_formfld WHERE record_type <> 'D' and FK_field = ".$rs->fields["PK_FIELD"];
		$rs1 = $source_db->Execute($v_sql);
		if (!$rs1) print $source_db->ErrorMsg();
		if (!$v_status) print $source_db->ErrorMsg();

		for ($i=0;$i<count($v_src_formsec);$i++){
//echo $v_sql;
//echo $rs1->fields["FK_FORMSEC"]."+++";
//echo $v_src_formsec[$i];
//echo "<BR>";
			if ($rs1->fields["FK_FORMSEC"] == $v_src_formsec[$i]){
				$v_pk_formsec = $v_tar_formsec[$i];
				break;
			}
		}
			
//echo $rs1->fields["FORMFLD_SEQ"]."|||";
//echo empty($rs1->fields["FORMFLD_SEQ"])?"NULL":$rs1->fields["FORMFLD_SEQ"];
//echo "insert into er_formfld (PK_FORMFLD,FK_FORMSEC,FK_FIELD,FORMFLD_SEQ,FORMFLD_MANDATORY,FORMFLD_BROWSERFLG,FORMFLD_XSL,FORMFLD_JAVASCR,RECORD_TYPE) values (seq_er_formfld.nextval,$v_pk_formsec,$v_pk_fldlib,".(empty($rs1->fields["FORMFLD_SEQ"])?"NULL":$rs1->fields["FORMFLD_SEQ"]).",".(empty($rs1->fields["FORMFLD_MANDATORY"])?"NULL":$rs1->fields["FORMFLD_MANDATORY"]).",".(empty($rs1->fields["FORMFLD_BROWSERFLG"])?"NULL":$rs1->fields["FORMFLD_BROWSERFLG"]).",'".$rs1->fields["FORMFLD_XSL"]."','".$rs1->fields["FORMFLD_JAVASCR"]."','N')";	



		if ($v_status) {
			$v_status = $target_db->Execute("insert into er_formfld (PK_FORMFLD,FK_FORMSEC,FK_FIELD,FORMFLD_SEQ,FORMFLD_MANDATORY,
			FORMFLD_BROWSERFLG,FORMFLD_XSL,FORMFLD_JAVASCR,RECORD_TYPE) values (seq_er_formfld.nextval,$v_pk_formsec,$v_pk_fldlib,".
			((strlen($rs1->fields["FORMFLD_SEQ"]) == 0)?"NULL":$rs1->fields["FORMFLD_SEQ"]).",".
			((strlen($rs1->fields["FORMFLD_MANDATORY"]) == 0)?"NULL":$rs1->fields["FORMFLD_MANDATORY"]).",".
			((strlen($rs1->fields["FORMFLD_BROWSERFLG"]) == 0)?"NULL":$rs1->fields["FORMFLD_BROWSERFLG"]).",'".
			str_replace("'","''",$rs1->fields["FORMFLD_XSL"])."','".str_replace("'","''",$rs1->fields["FORMFLD_JAVASCR"])."','N')");
			$v_error = (!$v_status) ? $target_db->ErrorMsg() : ""; 
		}


			
		$v_counter++;
		$rs->MoveNext();
	}
}
// ER_FLDACTION
//echo "ER_FLDACTION......................<BR>";
$v_sql = "select pk_fldaction,fldaction_type,fk_field,fldaction_condition from ER_fldaction WHERE FK_FORM = ".$v_pk_formlib;
$rs = $source_db->Execute($v_sql);
if (!$rs) print $source_db->ErrorMsg();
while (!$rs->EOF) {
	$v_pk_field = "";
	for ($i=0;$i<count($v_src_fldlib);$i++){
		if ($rs->fields["FK_FIELD"] == $v_src_fldlib[$i]){
			$v_pk_field = $v_tar_fldlib[$i];
			break;
		}
	}
	if (strlen($v_pk_field) == 0) $v_pk_field = 'null';
	
	$rs2 = $target_db->Execute("select seq_er_fldaction.nextval from dual");
	$v_pk_fldaction =  $rs2->fields[0];
	if ($v_status) {
		$v_status = $target_db->Execute("insert into er_fldaction (PK_FLDACTION,FK_FORM,FLDACTION_TYPE,FK_FIELD,FLDACTION_CONDITION) values ($v_pk_fldaction,$v_pk_formlib_tar,'".$rs->fields["FLDACTION_TYPE"]."',".$v_pk_field.",'".$rs->fields["FLDACTION_CONDITION"]."')");
		$v_error = (!$v_status) ? $target_db->ErrorMsg()." er_fldaction" : ""; 
	}
	
	// ER_FLDACTIONINFO
//	echo "ER_FLDACTIONINFO......................<BR>";
	$v_sql = "select PK_FLDACTIONINFO,FK_FLDACTION,INFO_TYPE,INFO_VALUE from ER_fldactioninfo WHERE FK_FLDACTION = ".$rs->fields["PK_FLDACTION"];
	$rs1 = $source_db->Execute($v_sql);
	if (!$rs1) print $source_db->ErrorMsg();
	while (!$rs1->EOF) {
		if (($rs1->fields["INFO_TYPE"] == "[VELTARGETID]") || ($rs1->fields["INFO_TYPE"] == "[VELSOURCEFLDID]")) {
			for ($i=0;$i<count($v_src_fldlib);$i++){
				if ($rs1->fields["INFO_VALUE"] == $v_src_fldlib[$i]){
					$v_pk_field = $v_tar_fldlib[$i];
					break;
				}
			}
//echo "1--------------------------------------------------------------------<BR>";
//echo "insert into er_fldactioninfo (PK_FLDACTIONINFO,FK_FLDACTION,INFO_TYPE,INFO_VALUE) values (seq_er_fldactioninfo.nextval,$v_pk_fldaction,'".$rs1->fields["INFO_TYPE"]."','".$v_pk_field."')";
			if ($v_status) {
				$v_status = $target_db->Execute("insert into er_fldactioninfo (PK_FLDACTIONINFO,FK_FLDACTION,INFO_TYPE,INFO_VALUE) values (seq_er_fldactioninfo.nextval,$v_pk_fldaction,'".$rs1->fields["INFO_TYPE"]."','".$v_pk_field."')");
				$v_error = (!$v_status) ? $target_db->ErrorMsg()." er_fldactioninfo" : ""; 
			}
			
//echo "--------------------------------------------------------------------<BR>";

		} elseif ($rs1->fields["INFO_TYPE"] == "[VELCONDITIONID]") {
			for ($i=0;$i<count($v_src_fldresp);$i++){
				if ($rs1->fields["INFO_VALUE"] == $v_src_fldresp[$i]){
					$v_pk_fldresp = $v_tar_fldresp[$i];
					break;
				}
			}
//echo "2--------------------------------------------------------------------<BR>";
//echo "insert into er_fldactioninfo (PK_FLDACTIONINFO,FK_FLDACTION,INFO_TYPE,INFO_VALUE) values (seq_er_fldactioninfo.nextval,$v_pk_fldaction,'".$rs1->fields["INFO_TYPE"]."','".$v_pk_fldresp."')";
			if ($v_status) {
				$v_status = $target_db->Execute("insert into er_fldactioninfo (PK_FLDACTIONINFO,FK_FLDACTION,INFO_TYPE,INFO_VALUE) values (seq_er_fldactioninfo.nextval,$v_pk_fldaction,'".$rs1->fields["INFO_TYPE"]."','".$v_pk_fldresp."')");
				$v_error = (!$v_status) ? $target_db->ErrorMsg()." er_fldactioninfo" : ""; 
			}
//echo "--------------------------------------------------------------------<BR>";
		} else {
//echo "3--------------------------------------------------------------------<BR>";
//echo "insert into er_fldactioninfo (PK_FLDACTIONINFO,FK_FLDACTION,INFO_TYPE,INFO_VALUE) values (seq_er_fldactioninfo.nextval,$v_pk_fldaction,'".$rs1->fields["INFO_TYPE"]."','".$rs1->fields["INFO_VALUE"]."')";
			if ($v_status) {
				$v_status = $target_db->Execute("insert into er_fldactioninfo (PK_FLDACTIONINFO,FK_FLDACTION,INFO_TYPE,INFO_VALUE) values (seq_er_fldactioninfo.nextval,$v_pk_fldaction,'".$rs1->fields["INFO_TYPE"]."','".str_replace("'","''",$rs1->fields["INFO_VALUE"])."')");
				$v_error = (!$v_status) ? $target_db->ErrorMsg()." er_fldactioninfo" : ""; 
			}
			
//echo "--------------------------------------------------------------------<BR>";
		}
		$rs1->MoveNext();
	}
		$rs->MoveNext();
	
	
}
// this value was set to 0 earlier, now it set to 1 to get the preview in eResearch - 19/08/2010
    if($v_status){
    //	$v_status = $target_db->Execute("update er_formlib set form_xslrefresh = 0 where pk_formlib = $v_pk_formlib_tar");
            $v_status = $target_db->Execute("update er_formlib set form_xslrefresh = 1 where pk_formlib = $v_pk_formlib_tar");
            $v_error = (!$v_status) ? $target_db->ErrorMsg()." er_formlib(form_xslrefresh)" : ""; 
    }
    
    
    if($v_status){
            $target_db->CommitTrans();
            echo "<BR><Font color=green><b>Form transferred successfully!</b></font>";
            echo '<meta http-equiv="refresh" content="3; url=./form_transfer.php">';
    }else{
            $target_db->RollbackTrans();
            echo "<BR><Font color=red><b>Form transfer not successful: $v_error</b></font>";
    }

}else{
	echo "<BR><Font color=red><b>Form name already exists</b></font> <a href='javascript:history.go(-1)' style='margin-left:60px; outline:none;'>Back to Form Transfer</a>";
}

//$url = "./mig_forms.php?pk_vlnk_adaptor=".$_POST['pk_adaptor'];
//echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";
?>
<?PHP
}

?>
</div>


</body>
</html>


<?php

$source_db->Close();
$target_db->Close();
}
else header("location: index.php?fail=1");
?>
		
