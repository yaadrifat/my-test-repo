<?php
session_start();
header("Cache-control: private");
require_once("audit_queries.php");
if(@$_SESSION["user"]){
    $moduleArray = array("ds"=>"datasource", "dbm"=>"dbmaintenance", "rd"=>"reportdesigner", "mt"=>"datamigration", "account"=>"manageaccount", "logout"=>"logout", "form"=>"manageform", "calendar"=>"managecalendar", "budget"=>"managebudget","audit"=>"auditreport");
    $_SESSION["MODULE"] = $_GET["module"];
    $_SESSION["PAGE"] = $_GET["page"];
    if ((isset($_SESSION["DATASOURCE"])|| ($_SESSION["MODULE"] == 'logout'))){
        if (!empty($_SESSION["PAGE"])) {
            $curdir = basename(__DIR__);			
            $pageurl = $_SERVER['REMOTE_ADDR']."/".$curdir."/".$_SESSION["PAGE"];
            sessionTracking("pageaccessed",$pageurl);
            header("location: ".$_SESSION["PAGE"]);
        } else {
            $module = $moduleArray[$_SESSION["MODULE"]];
            $curdir = basename(__DIR__);			
            //$pageurl = $_SERVER['HTTP_HOST']."/".$curdir."/".$module;
            $pageurl = $_SERVER['REMOTE_ADDR']."/".$curdir."/".$module;
            sessionTracking("pageaccessed", $pageurl);
            header("location: etools.php?module=".$_SESSION["MODULE"]);
        }
    } else {
        header("location: datasource.php");
        $_SESSION["MODULE"] = "";
    }
}
else header("location: index.php?fail=1");
?>
