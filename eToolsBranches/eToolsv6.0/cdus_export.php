<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> CDUS Validate</title>

<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">CDUS Submission - Final Export</div>

<?php

$fk_exp_definition = $_GET["fk_exp_definition"];
$pk_datamain = $_GET["pk_datamain"];

$query_sql = "begin pkg_cdus.sp_export_data(".$fk_exp_definition.",".$pk_datamain."); end;";

$results = executeOCIUpdateQuery($query_sql,$ds_conn);

echo "Exported...";
$url = "./cdus.php?fk_exp_definition=".$fk_exp_definition;
echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";

?>
      </div>


</body>
</html>
<?php
OCICommit($ds_conn);
OCILogoff($ds_conn);
}
else header("location: ./index.php?fail=1");
?>
