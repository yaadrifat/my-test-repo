<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velos eTools -> Update Lookup</title>

<SCRIPT language="Javascript1.2">
function validate(form){


	if (form.uploadedfile.value == ""){
		alert("Choose a file to upload.");
		form.uploadedfile.focus();
		return false;
	}

	if (form.delimiter.value == ""){
		alert("Delimiter cannot be blank.");
		form.delimiter.focus();
		return false;
	}
	
	return true;
}
</SCRIPT>

</head>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body onLoad="document.lookup.uploadedfile.focus();">
<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	
<?php
if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 


$v_pk_lkplib = $_GET["pk_lkplib"];

$v_query = "SELECT lkpview_name,pk_lkpview FROM er_lkpview where fk_lkplib = ".$v_pk_lkplib." ORDER BY 1";
$results = executeOCIQuery($v_query,$ds_conn);

$dd_lookup = '<select name="lookup">';
for ($rec = 0; $rec < $results_nrows; $rec++){
	$dd_lookup .= '<option value="'.$results["PK_LKPVIEW"][$rec].'">'.$results["LKPVIEW_NAME"][$rec].'</option>';
}
$dd_lookup .= '</select>';




?>

<div class="navigate">Update Lookup data:</div>
<form name="lookup" enctype="multipart/form-data" action="update_lookup.php" method="POST" onSubmit="if (validate(document.lookup) == false) return false;">
<BR>
<table>
<tr><td>Lookup Name:</td><td><?PHP echo $dd_lookup; ?></td></tr>
<tr><td>Delimited File name:</td><td><input class="required" type="file" name="uploadedfile" size="100"/></td></tr>
<tr><td>Delimiter:</td><td><input type="text" class="required" name="delimiter" size="2"/></td></tr>



</table>
<BR><input type="image" name="submit" value="Submit" src="./img/submit.png"  align="absmiddle" border="0" onmouseover="this.src='./img/submit_m.png';" onmouseout="this.src='./img/submit.png';" />

</form>
<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Note">
<tr>
<th align="left">Note</th>
</tr>
<tr><td align="left" valign="top">
<p>Supported File Types: Any delimited file (delimiters such as comma, semicolon, pipe etc. are commonly used)</p>
<p>First row of the file should contain Column Name, no special characters other than '_' (underscore), should be one continuous string of characters (no space) and delimited with same delimiter as the actual data rows.</p>
</td></tr>
</table></div>

<?php } else {

$delimiter = $_POST["delimiter"];
$lookup = $_POST["lookup"];

$v_query = "select fk_lkplib  from er_lkpview where pk_lkpview = ".$lookup;
$results = executeOCIQuery($v_query,$ds_conn);
$v_lkplib = $results["FK_LKPLIB"][0];

$v_query = "SELECT pk_lkpcol,lkpcol_name,lkpcol_dispval FROM er_lkpcol WHERE fk_lkplib = ".$v_lkplib." order by lkpcol_name";
$results = executeOCIQuery($v_query,$ds_conn);


  $curDir = getcwd();
  $target_path = $curDir."/upload/";

  $target_path = $target_path.basename( $_FILES['uploadedfile']['name']);

  if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
//      echo "The file ".  basename( $_FILES['uploadedfile']['name']).
//      " has been uploaded"."<BR>";

?>
        <div class="navigate">Update Lookup data:</div>
        <form action="update_lookup_data.php" method="POST">
<?php
          echo '<input type="hidden" name="delimiter" value="'.$delimiter.'"/>';
          echo '<input type="hidden" name="lookup" value="'.$v_lkplib.'"/>';
          echo '<input type="hidden" name="fileDir" value="'.$target_path.'"/>';
?>
          <table width="50%" border="1">
          <tr>
<!--
          <th width="20%">pk lkpcol</th>
          <th width="20%">Column Name</th>
-->
          <th width="50%">Display Name</th>
          <th width="50%">File Column</th>
          </tr>
          <?php
          $handle = fopen($target_path, "r");
          $v_sql="";
          $dd_yn = '<option value="Y" selected>Yes</option><option value="N">No</option>';
		  $dd_filecols = '<option value="" selected>Select an option</option>';
          while (($data = fgetcsv($handle, 10000, $delimiter)) !== FALSE) {
             $num = count($data);
             $width = round(100 / $num);
             for ($c=0; $c < $num; $c++) {

				  $dd_filecols .= '<option value="'.$data[$c].'">'.$data[$c].'</option>';

             }
             break;
          }

//$v_query = "SELECT pk_lkpcol,lkpcol_name,lkpcol_dispval FROM er_lkpcol WHERE fk_lkplib = ".$v_lkplib;

		for ($rec = 0; $rec < $results_nrows; $rec++){
	        echo "<tr>";
//			echo '<td><input readonly size="30" type="text" name="pk_lkpcol['.$rec.']" value="'.$results["PK_LKPCOL"][$rec].'"/></td>';
//			echo '<td><input readonly size="30" type="text" name="lkpcol_name['.$rec.']" value="'.$results["LKPCOL_NAME"][$rec].'"/></td>';
			echo '<td><input readonly size="30" type="text" name="lkpcol_dispval['.$rec.']" value="'.$results["LKPCOL_DISPVAL"][$rec].'"/></td>';
			echo '<td><select name="filecol['.$rec.']"'.$dd_filecols.'</select></td>';
	        echo '</tr>';
		}


/*
  			echo '<td><input readonly size="30" type="text" name="colName['.$c.']" value="'.$data[$c].'"/></td>';
  			echo '<td><input size="30" type="text" name="dispName['.$c.']" value="'.$data[$c].'"/></td>';
  			echo '<td><input size="30" type="text" name="keyword['.$c.']" value="'.$data[$c].'"/></td>';
  			echo '<td align="center"><input size="5" type="text" name="colSeq['.$c.']" value="'.($c + 1).'"/></td>';
  			echo '<td align="center"><select name="colShow['.$c.']">'.$dd_yn.'</select></td>';
  			echo '<td align="center"><select name="colSearch['.$c.']">'.$dd_yn.'</select></td>';
  			echo '<td><input size="5" type="text" name="colWidth['.$c.']" value="'.$width.'"/></td>';
 */

  


          echo "</table>";
          echo "<BR>";
          //echo '<input type="submit" name="submit" value="Submit"/>';
		  ?>
		  <input type="image" name="submit" value="Submit" src="./img/submit.png"  align="absmiddle" border="0" onmouseover="this.src='./img/submit_m.png';" onmouseout="this.src='./img/submit.png';" />
<?PHP

          echo "</form>";

        /*
        while (($data = fgetcsv($handle, 10000, $delimiter)) !== FALSE) {
           $num = count($data);
           $row++;
        //   echo $row;
           for ($c=0; $c < $num; $c++) {
        		if ($row == 1) {
        			$v_sql = $v_sql.$data[$c]." varchar2(4000),";
        		} else {
        			$colval = "'".str_replace("'","''",$data[$c])."'";
        			$v_sql = $v_sql.$colval.",";
                }
           }
        	if ($row == 1) {
        		$v_sql = "create table ".$tablename." (".substr($v_sql,0,strlen($v_sql)-1).")";

        echo $v_sql;

        //		$results = executeOCIUpdateQuery($v_sql,$ds_conn);
        		$v_sql = "";
        	} else {
        		$v_sql = "insert into ".$tablename." values (".substr($v_sql,0,strlen($v_sql)-1).")";
        //		$results = executeOCIUpdateQuery($v_sql,$ds_conn);
        		$v_sql = "";
            }

        }
        */

        fclose($handle);
        //echo "Data Imported to table: ".$tablename."<BR>";
        //echo "Total rows imported: ".$row - 1;



  } else{
      echo "There was an error uploading the file, please try again!"."<BR>";
  }





}

?>

	
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
