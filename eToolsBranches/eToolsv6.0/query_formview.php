<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Create Form View</title>

<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>

<script>
function formPreview(formPk){
	var win = "form_preview.php?formPk="+formPk;
	window.open(win,'mywin',"toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=825,height=600,top=90,left=150");
}
</script>
</head>


<body onLoad="document.formimport.formname.focus();">
<div id="fedora-content">
<div class="navigate">Create Form View - Select Form</div>
<br>	
<?PHP

if (isset($_POST["formname"])){
	$searchvalue = $_POST["formname"];
	$v_formstat = $_POST["frmstat"];
	$v_linkto = $_POST["linkedto"];
	$query_sql = "select pk_codelst, codelst_desc  from er_codelst where codelst_type = 'frmstat' and codelst_subtyp <> 'W'";
	$results = executeOCIQuery($query_sql,$ds_conn);
	$v_all = "";
	$v_frmstat = "";
	for ($rec = 0; $rec < $results_nrows; $rec++){
		$v_all .= (($rec==0) ? "" : ",").$results["PK_CODELST"][$rec];
		$v_frmstat .= '<option '.($results["PK_CODELST"][$rec] == $v_formstat ? " selected ": "").'value="'.$results["PK_CODELST"][$rec].'">'.$results["CODELST_DESC"][$rec]."</option>";
	}
	$v_frmstat = '<option value="'.$v_all.'">All</option>'.$v_frmstat;

	$v_linkedto = '<option '.($v_linkto == "ALL"? "selected":"").' value="ALL">All</option>
	<option '.($v_linkto == "A"? "selected":"").' value="A">Account</option>
	<option '.($v_linkto == "PA"? "selected":"").' value="PA">All Patients</option>
	<option '.($v_linkto == "SA"? "selected":"").' value="SA">All Studies</option>
	<option '.($v_linkto == "C"? "selected":"").' value="C">CRF</option>
	<option '.($v_linkto == "PS"? "selected":"").' value="PS">Patient (All Studies)</option>
	<option '.($v_linkto == "PR"? "selected":"").' value="PR">Patient (All Studies - Restricted)</option>
	<option '.($v_linkto == "SP"? "selected":"").' value="SP">Patient (Specific Study)</option>
	<option '.($v_linkto == "S"? "selected":"").' value="S">Study</option>
	';
	
} else {
	$searchvalue = "";
	$query_sql = "select pk_codelst,codelst_desc from er_codelst where codelst_type = 'frmstat' and codelst_subtyp <> 'W'";
	$results = executeOCIQuery($query_sql,$ds_conn);
	$v_all = "";
	$v_frmstat = "";
	for ($rec = 0; $rec < $results_nrows; $rec++){
		$v_all .= (($rec==0) ? "" : ",").$results["PK_CODELST"][$rec];
		$v_frmstat .= '<option  value="'.$results["PK_CODELST"][$rec].'">'.$results["CODELST_DESC"][$rec]."</option>";
	}
	$v_frmstat = '<option value="'.$v_all.'">All</option>'.$v_frmstat;
	
	$v_linkedto = '<option selected value="ALL">All</option>
	<option value="A">Account</option>
	<option value="PA">All Patients</option>
	<option value="SA">All Studies</option>
	<option value="C">CRF</option>
	<option value="PS">Patient (All Studies)</option>
	<option value="PR">Patient (All Studies - Restricted)</option>
	<option value="SP">Patient (Specific Study)</option>
	<option value="S">Study</option>
	';
}
?>
<form name="formimport" method="post" action="query_formview.php">
<!-- <INPUT type=hidden name="fk_account" value=<? //echo $v_account; ?>></input> -->
<table border = "0">
<tr>
	<td>Search Form Name: </td><td><input name="formname" type="text" size="30" maxlength="100" value="<?PHP echo $searchvalue; ?>"/></td>
	<td>Status: </td><td><select name="frmstat"><?PHP echo $v_frmstat;?></select></td>
	<td>Linked to: </td><td><select name="linkedto" sort><?PHP echo $v_linkedto;?></select></td>
	<td><input type="image" name="submit" value="Submit" src="./img/search.png"  align="absmiddle" border="0" onmouseover="this.src='./img/search_m.png';" onmouseout="this.src='./img/search.png';" /></input></td>
</tr>

</table>
</form>

<?PHP
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
//$v_account = $_POST["fk_account"];
?>
<div class="formDis">
<Table border="1" width="100%"><TR>
<TH width="20%">FORM NAME</TH>
<TH width="30%">FORM DESCRIPTION</TH>
<TH width="5%">STATUS</TH>
<TH width="10%">LINKED TO</TH>
<TH width="20%">STUDY NUMBER</TH>
<TH width="5%">&nbsp;</TH>
<TH width="5%">&nbsp;</TH>
</TR>
<?php

$v_query_sql = "SELECT pk_formlib,form_name, form_desc,decode(trim(lf_displaytype),'C','CRF','S','Study','SP','Patient (Specific Study)','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','SA','All Studies','PA','All Patients') as lf_displaytype,DECODE(fk_study,NULL,'',(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study)) AS study_number,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = form_status) as status 
FROM ER_FORMLIB a, ER_LINKEDFORMS b 
WHERE pk_formlib = fk_formlib AND  b.record_type <> 'D' AND 
LOWER(trim(form_name)) LIKE lower('%".$searchvalue."%') and 
form_status in ($v_formstat)";

$v_query_sql .= ($v_linkto == 'ALL'? "":" and lf_displaytype = '$v_linkto'");

$results = executeOCIQuery($v_query_sql,$ds_conn);

for ($rec = 0; $rec < $results_nrows; $rec++){
?>
	<TR onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
	<TD><?php echo $results["FORM_NAME"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["FORM_DESC"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["STATUS"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["LF_DISPLAYTYPE"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["STUDY_NUMBER"][$rec] . "&nbsp;"; ?></TD>
<?php
	echo "<td  align='center'><a href=query_formview_exe.php?pk_formlib=".$results["PK_FORMLIB"][$rec]."&formname=".urlencode($results["FORM_NAME"][$rec]).">Select</a></td>";
	echo '<td  align="center"><a href=# onclick="formPreview('.$results["PK_FORMLIB"][$rec].')">Preview</a></td>';
	echo "</TR>";
}
?>
</TABLE>
</div>
<?php } ?>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
