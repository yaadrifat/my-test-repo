<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velos eTools -> Form from File</title>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="js/validate.js"></SCRIPT>
<script type="text/javascript">
function validate(formObj) {
	if (formObj.formname.value == "") {
		alert("Form name cannot be blank.");
		formObj.formname.focus();
		return false;
	}
	if (formObj.uploadedfile.value == "") {
		alert("File name cannot be blank.");
		formObj.uploadedfile.focus();
		return false;
	}
	if (formObj.delimiter.value == "") {
		alert("Delimiter cannot be blank.");
		formObj.delimiter.focus();
		return false;
	}
}

function idVal(){
	var refTab=document.getElementById("subFrm");	
	for ( var i = 1; i<refTab.rows.length; i++ ) { 
		var row = refTab.rows.item(i); 
		var idValue = row.cells[6].firstChild.value.toLowerCase();		
		if(idValue.trim() == "er_def_date_01"){
			alert("'"+row.cells[2].firstChild.value+"' field id is eResearch reserved word");
			row.cells[6].firstChild.value = row.cells[6].firstChild.value.trim();
			return false;
		}else if(row.cells[4].firstChild.value == "DATE" && idValue.trim() == "er_def_date_01"){
			alert("'"+row.cells[2].firstChild.value+"' field id is eResearch reserved word");
			row.cells[6].firstChild.value = row.cells[6].firstChild.value.trim();
			return false;
		}
		
		if(isNaN(row.cells[1].firstChild.value)==true){
			alert('please input only numbers on sequence!');
			return false;
		}
	}	
}
// to support trim function on IE8 
if (!String.prototype.trim) {
 String.prototype.trim = function() {
  return this.replace(/^\s+|\s+$/g,'');
 }
}
</script>

</head>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body>
<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	
<?php
if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 

//$v_account = 53;
//$v_account = 1170;

$v_query = "SELECT pk_catlib,catlib_name FROM ER_CATLIB WHERE catlib_type = 'T' and record_type <> 'D' order by lower(catlib_name)";
$results = executeOCIQuery($v_query,$ds_conn);

$dd_lookup = '<select name="category">';
for ($rec = 0; $rec < $results_nrows; $rec++){
	$dd_lookup .= '<option value="'.$results["PK_CATLIB"][$rec].'">'.$results["CATLIB_NAME"][$rec].'</option>';
}
$dd_lookup .= '</select>';


?>

<div class="navigate">Form from File:</div>
<form name="qform" enctype="multipart/form-data" action="form_quick.php" method="POST" onSubmit="if (validate(document.qform) == false) return false;">
<table>
<tr><td>Form Type:</td><td><?PHP echo $dd_lookup; ?></td></tr>
<tr><td>Form Name:</td><td><input class="required" type="text" name="formname" size="50" /></td></tr>
<tr><td>Delimited File name:</td><td><input class="required" type="file" name="uploadedfile" size="75"/></td></tr>
<tr><td>Delimiter:</td><td><input type="text" class="required" name="delimiter" size="2" value=","/></td></tr>



<tr><td>&nbsp;</td><td align="right">Sample template: <a href="./template/form_template.csv">form_template.csv</a></td></tr></table>
<input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onmouseover="this.src='./img/submit_m.png';" onmouseout="this.src='./img/submit.png';" /> 
</form>
<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Note">
<tr>
<th align="left">Note</th>
</tr>
<tr><td align="left" valign="top">
<p>Supported File Types: Any delimited file (delimiters such as comma, semicolon, pipe etc. are commonly used)</p>
<p>First row of the file should contain Column Name, no special characters other than '_' (underscore), should be one continuous string of characters (no space) and delimited with same delimiter as the actual data rows.</p>
<p>Please do not add the reserved "er_def_date_01" field in the Form template. This will be automatically added during the Form import</p>
</td></tr>
</table></div>

<?php } else {

$v_delimiter = $_POST["delimiter"];
$v_category = $_POST["category"];
$v_formname = $_POST["formname"];

$v_query = "SELECT fk_account FROM ER_CATLIB WHERE pk_catlib = ".$v_category;
$results = executeOCIQuery($v_query,$ds_conn);
$v_account= $results["FK_ACCOUNT"][0];

  $curDir = getcwd();
  $target_path = $curDir."/upload/";

  $target_path = $target_path.basename( $_FILES['uploadedfile']['name']);

  if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {

?>
        <div class="navigate">Quick Form:</div>
        <form name="frmVal"  method="POST" onsubmit="return idVal();" action="form_quick_save.php">
<?php
          echo '<input type="hidden" name="delimiter" value="'.$v_delimiter.'"/>';
          echo '<input type="hidden" name="category" value="'.$v_category.'"/>';
          echo '<input type="hidden" name="account" value="'.$v_account.'"/>';
          echo '<input type="hidden" name="fileDir" value="'.$target_path.'"/>';
          echo 'Form Name: <b>'.$v_formname.'</b><input readonly type="hidden" name="formname" value="'.$v_formname.'"/>';
?>

          <table width="100%" border="1" id="subFrm">
          <tr>
          <th width="10%">Section Name</th>
          <th width="5%">Section Seq.</th>
          <th width="10%">Field Name</th>
          <th width="20%">Description</th>
          <th width="5%">Type</th>
          <th width="5%">Multiple Choice Type</th>
          <th width="10%">Field ID</th>
          <th width="5%">Seq.</th>
          <th width="5%">Mandatory</th>
          <th width="10%">Multiple Choice Values</th>
          </tr>
          <?php
          $handle = fopen($target_path, "r");
          $row = -1;
          while (($data = fgetcsv($handle, 10000, $v_delimiter)) !== FALSE) {
             $num = count($data);
             $width = round(100 / $num);
			 if ($row >= 0) {
	             echo "<tr>";
	          	 echo '<td><input size="10" type="text" name="secName['.$row.']" value="'.$data[0].'"/></td>';
	          	 echo '<td><input size="2" type="text" name="secSeq['.$row.']" value="'.$data[1].'"/></td>';
	          	 echo '<td><input size="20" type="text" name="fieldName['.$row.']" value="'.$data[2].'"/></td>';
	          	 echo '<td><input size="20" type="text" name="fieldDesc['.$row.']" value="'.$data[3].'"/></td>';
	          	 echo '<td><input size="5" type="text" name="fieldType['.$row.']" value="'.$data[4].'"/></td>';
	          	 echo '<td><input size="5" type="text" name="multiChoice['.$row.']" value="'.$data[5].'"/></td>';
	          	 echo '<td><input size="10" type="text" name="fieldId['.$row.']" value="'.$data[6].'"/></td>';
	          	 echo '<td><input size="2" type="text" name="fieldSeq['.$row.']" value="'.$data[7].'"/></td>';
	          	 echo '<td><input size="2" type="text" name="mandatory['.$row.']" value="'.(isset($data[8])?$data[8]:"").'"/></td>';
	          	 echo '<td><input size="25" type="text" name="multiValues['.$row.']" value="'.(isset($data[9])?$data[9]:"").'"/></td>';
	             echo '</tr>';
			}
		     $row++;
          }

          echo "</table>";
          echo "<BR>";
//          echo '<input type="submit" name="submit" value="Submit"/>';
?>
<input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onmouseover="this.src='./img/submit_m.png';" onmouseout="this.src='./img/submit.png';" /> 
<?PHP
          echo "</form>";
        fclose($handle);
  } else{
      echo "There was an error uploading the file, please try again!"."<BR>";
  }
}
?>
</div>
</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
