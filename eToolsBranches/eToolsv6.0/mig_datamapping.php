<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Migrate Data</title>

<?php
    include("./includes/oci_functions.php");
    include("db_config.php");
    include("./includes/header.php");
    $ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]);
?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Migrate Data - Standard - Edit Mapping</div>

<form name="datamap" method="post" action="mig_datamap_save.php">
<?php


$pk_vlnk_adapmod = $_GET["pk_vlnk_adapmod"];

if ($_GET["refresh"] == 1) {
	$query_sql = "begin velink.pkg_velink.sp_generate_map(".$pk_vlnk_adapmod."); end;";
	$results = executeOCIUpdateQuery($query_sql,$ds_conn);
}

if (isset($_GET["mode"])) {
	$v_mode = $_GET["mode"];
} else {
	$v_mode = "NM";
}

echo "<INPUT type=\"hidden\" name=\"pk_vlnk_adapmod\" value=\"".$pk_vlnk_adapmod."\"></input>"; 

$query_sql = "SELECT fk_account,pk_vlnk_adaptor FROM velink.VLNK_ADAPMOD,velink.vlnk_adaptor where pk_vlnk_adaptor = fk_vlnk_adaptor and pk_vlnk_adapmod = ".$pk_vlnk_adapmod;
$results = executeOCIQuery($query_sql,$ds_conn);
$v_account = $results["FK_ACCOUNT"][0];
$v_pk_vlnk_adaptor = $results["PK_VLNK_ADAPTOR"][0];

echo '<table width="100%"><tr><td width="20%"><a href="./mig_modules.php?pk_vlnk_adaptor='.$v_pk_vlnk_adaptor.'">Back to Migration Page</a></td><td align=center>';
echo "<input ".(($v_mode == 'NM')? "checked" : "")." type='radio' name='display' value='NM' onclick='window.location=\"mig_datamapping.php?pk_vlnk_adapmod=".$pk_vlnk_adapmod."&refresh=0&mode=NM\"' /> Not Mapped";
echo "<input ".(($v_mode == 'M')? "checked" : "")." type='radio' name='display' value='M' onclick='window.location=\"mig_datamapping.php?pk_vlnk_adapmod=".$pk_vlnk_adapmod."&refresh=0&mode=M\"' /> Mapped";
echo "<input ".(($v_mode == 'ALL')? "checked" : "")." type='radio' name='display' value='ALL' onclick='window.location=\"mig_datamapping.php?pk_vlnk_adapmod=".$pk_vlnk_adapmod."&refresh=0&mode=ALL\"' /> All";
echo "</td></tr></table>";


//$mapcolname = "select map_colname from velink.vlnk_modcol where pk_vlnk_modcol in (select fk_vlnk_modcol from velink.vlnk_pipemap where fk_vlnk_adapmod = ".$pk_vlnk_adapmod.")";
//$mapcolnamers = executeOCIQuery($mapcolname, $ds_conn);
//
//$mapcolnameFlag = 0;
//for($e=0; $e<$results_nrows; $e++){
//    //echo $mapcolnamers['MAP_COLNAME'][$e]."<br>";
//    if($mapcolnamers['MAP_COLNAME'][$e] == 'PATSTUDYSTAT_REASON'){
//        $mapcolnameFlag = 1;
//    }
//}
//
//if($mapcolnameFlag == 1){    
//    $readonsubtype = "select distinct codelst_subtyp from er_codelst where codelst_subtyp in (select codelst_subtyp from er_codelst where codelst_type = 'patStatus')";
//    $readonsubtypers = executeOCIQuery($readonsubtype,$ds_conn);
//    
//    $mapsubtype = '';
//    for($q=0; $q<$results_nrows; $q++){
//        if($q == $results_nrows-1){
//            $mapsubtype .= "'".strtoupper($readonsubtypers['CODELST_SUBTYP'][$q])."'";
//        }else{
//            $mapsubtype .= "'".strtoupper($readonsubtypers['CODELST_SUBTYP'][$q])."',";
//        }
//        
//    }
//}
$query_sql = "SELECT pk_vlnk_pipemap,column_name,source_value,substr(target_value,instr(target_value,':')+1) as target_value,target_id,fk_vlnk_modcol,map_type,map_subtyp,is_required 
FROM velink.VLNK_PIPEMAP, velink.VLNK_MODCOL 
WHERE pk_vlnk_modcol = fk_vlnk_modcol AND fk_vlnk_adapmod = ".$pk_vlnk_adapmod;    
switch ($v_mode) {
    case "NM":
        $query_sql .= ' and target_id = 0 ';
        break;
    case "M":
        $query_sql .= ' and target_id <> 0 ';
        break;
    }
$query_sql .= " order by column_name";

$results = executeOCIQuery($query_sql,$ds_conn);
$tot_rows = $results_nrows;

$v_lasttype = "";

echo '<Table border="1" width="100%"><TR>';
echo '<TH>Column Name</TH>';
echo '<TH>Source Value</TH>';
echo '<TH>Map to</TH>';
echo '</TR>';

for ($rec=0; $rec<$tot_rows; $rec++){
    echo '<TR onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';">';
    echo "<TD>".$results["COLUMN_NAME"][$rec];
    $v_mandatory = "";
    if ($results["IS_REQUIRED"][$rec] == 1) {
            $v_mandatory = " class='required' ";
            echo "<font color='red'><b><font size='3'>*</font></b></font>";
    }
    echo "</td><TD>".$results["SOURCE_VALUE"][$rec]."</TD>";

    if ($results["TARGET_ID"][$rec] == 0){
            if ($results["MAP_TYPE"][$rec] == "CODELIST" || $results["MAP_TYPE"][$rec] == "SCODELIST"){
                    //echo $results["MAP_SUBTYP"][$rec]."<br>";
//                    if($mapcolnameFlag == 1){
//                        $mapsubtype = $mapsubtype;
//                    }else{
//                        $mapsubtype = $results["MAP_SUBTYP"][$rec];
//                    }
                    //echo $mapsubtype."<br>";
                        if ($v_lasttype != $results["MAP_SUBTYP"][$rec]){  
                        //if ($v_lasttype != $mapsubtype){  
                            //--- fix for the bug 16396
                            $reasonQry = "select distinct codelst_type from er_codelst where codelst_type in (select codelst_subtyp from er_codelst where codelst_type = 'patStatus')";
                            $reasonresults = executeOCIQuery($reasonQry,$ds_conn);
                            $reaVal = '';
                            for ($rr=0; $rr < $results_nrows; $rr++){
                                    if($rr == $results_nrows-1){
                                            $reaVal = $reaVal."'".strtoupper($reasonresults["CODELST_TYPE"][$rr])."'";	
                                    }else{
                                            $reaVal = $reaVal."'".strtoupper($reasonresults["CODELST_TYPE"][$rr])."',";
                                    }
                            }    
                        
                            //--- fix for the bug 16396		
                            //$v_sql = "select codelst_type || ':' || codelst_desc as codelst_desc1,codelst_desc as codelst_desc from ".($results["MAP_TYPE"][$rec]=="CODELIST"?"ERES.ER_CODELST":"ESCH.SCH_CODELST")." where upper(codelst_type) IN (".$results["MAP_SUBTYP"][$rec].") order by codelst_desc";

                            // made changes for the form migration issue on 28/8/2014
                            /*if($results["MAP_TYPE"][$rec] == "CODELIST"){
                                    $v_sql = "select codelst_type || ':' || codelst_desc as codelst_desc1,codelst_desc as codelst_desc from ".($results["MAP_TYPE"][$rec]=="CODELIST"?"ERES.ER_CODELST":"ESCH.SCH_CODELST")." where upper(codelst_type) IN (".$reaVal.") order by codelst_desc";
                            }else{
                                    $v_sql = "select codelst_type || ':' || codelst_desc as codelst_desc1,codelst_desc as codelst_desc from ".($results["MAP_TYPE"][$rec]=="CODELIST"?"ERES.ER_CODELST":"ESCH.SCH_CODELST")." where upper(codelst_type) IN (".$results["MAP_SUBTYP"][$rec].") order by codelst_desc";
                            }*/


                           // above fix didnt allow the tarea so made changes - 26-10-15                                                       
                            $statreason = "select MAP_TYPE, MAP_COLNAME from  velink.vlnk_modcol where pk_vlnk_modcol= ".$results["FK_VLNK_MODCOL"][$rec];
                            $statreasonrs = executeOCIQuery($statreason, $ds_conn);
                            //if($results["MAP_SUBTYP"][$rec] != 'patStatus'){    
                            if($statreasonrs['MAP_COLNAME'][0] != 'PATSTUDYSTAT_REASON'){                                    
                                $v_sql = "select codelst_type || ':' || codelst_desc as codelst_desc1,codelst_desc as codelst_desc from ".($results["MAP_TYPE"][$rec]=="CODELIST"?"ERES.ER_CODELST":"ESCH.SCH_CODELST")." where upper(codelst_type) IN (".$results["MAP_SUBTYP"][$rec].") order by codelst_desc";                            
                            }else{
                                $v_sql = "select codelst_type || ':' || codelst_desc as codelst_desc1,codelst_desc as codelst_desc from ".($results["MAP_TYPE"][$rec]=="CODELIST"?"ERES.ER_CODELST":"ESCH.SCH_CODELST")." where upper(codelst_type) IN (".$reaVal.") order by codelst_desc";
                            }

                            $res_codelist = executeOCIQuery($v_sql,$ds_conn);
                    }
                    $dd_codelist = '<option value=""></option>';
                    for ($rec_cod = 0; $rec_cod < $results_nrows; $rec_cod++){
                        $dd_codelist .= "<option value=\"".urlencode($res_codelist["CODELST_DESC1"][$rec_cod])."\">".$res_codelist["CODELST_DESC"][$rec_cod]."</option>";
                    }
                    echo "<TD><select $v_mandatory name=\"target_value[$rec]\">".$dd_codelist."</select></TD>";
                    $v_lasttype = $results["MAP_SUBTYP"][$rec];
            } 
            if ($results["MAP_TYPE"][$rec] == "USERLIST"){
                    if ($v_lasttype != $results["MAP_TYPE"][$rec]){
                            $v_sql = "select usr_firstname || ' ' || usr_lastname as user_name from er_user where fk_account = ".$v_account." order by 1";
                            $res_codelist = executeOCIQuery($v_sql,$ds_conn);
                    }
                    $dd_codelist = '<option value=""></option>';
                    for ($rec_cod = 0; $rec_cod < $results_nrows; $rec_cod++){
                               $dd_codelist .= "<option value=\"".$res_codelist["USER_NAME"][$rec_cod]."\">".$res_codelist["USER_NAME"][$rec_cod]."</option>";
                    }
                    echo "<TD><select $v_mandatory name=\"target_value[$rec]\">".$dd_codelist."</select></TD>";
                    $v_lasttype = $results["MAP_TYPE"][$rec];
            } 
            if ($results["MAP_TYPE"][$rec] == "SITELIST"){
                    if ($v_lasttype != $results["MAP_TYPE"][$rec]){
                            $v_sql = "select site_name from er_site where fk_account = ".$v_account." order by 1";
                            $res_codelist = executeOCIQuery($v_sql,$ds_conn);
                    }
                    $dd_codelist = '<option value=""></option>';
                    for ($rec_cod = 0; $rec_cod < $results_nrows; $rec_cod++){
                               $dd_codelist .= "<option value=\"".$res_codelist["SITE_NAME"][$rec_cod]."\">".$res_codelist["SITE_NAME"][$rec_cod]."</option>";
                    }
                    echo "<TD><select $v_mandatory name=\"target_value[$rec]\">".$dd_codelist."</select></TD>";
                    $v_lasttype = $results["MAP_TYPE"][$rec];
            } 

            if ($results["MAP_TYPE"][$rec] == "GROUP"){
                    if ($v_lasttype != $results["MAP_TYPE"][$rec]){
                            $v_sql = "select grp_name from er_grps where fk_account = ".$v_account." order by 1";
                            $res_codelist = executeOCIQuery($v_sql,$ds_conn);
                    }
                    $dd_codelist = '<option value=""></option>';
                    for ($rec_cod = 0; $rec_cod < $results_nrows; $rec_cod++){
                               $dd_codelist .= "<option value=\"".$res_codelist["GRP_NAME"][$rec_cod]."\">".$res_codelist["GRP_NAME"][$rec_cod]."</option>";
                    }
                    echo "<TD><select $v_mandatory name=\"target_value[$rec]\">".$dd_codelist."</select></TD>";
                    $v_lasttype = $results["MAP_TYPE"][$rec];
            } 

            if ($results["MAP_TYPE"][$rec] == "TIMEZONE"){
                    if ($v_lasttype != $results["MAP_TYPE"][$rec]){
                            $v_sql = "select tz_name from esch.sch_timezones order by 1";
                            $res_codelist = executeOCIQuery($v_sql,$ds_conn);
                    }
                    $dd_codelist = '<option value=""></option>';
                    for ($rec_cod = 0; $rec_cod < $results_nrows; $rec_cod++){
                               $dd_codelist .= "<option value=\"".$res_codelist["TZ_NAME"][$rec_cod]."\">".$res_codelist["TZ_NAME"][$rec_cod]."</option>";
                    }
                    echo "<TD><select $v_mandatory name=\"target_value[$rec]\">".$dd_codelist."</select></TD>";
                    $v_lasttype = $results["MAP_TYPE"][$rec];
            } 

    } else {
            echo "<TD><INPUT size=\"75\" type=\"text\" name=\"target_value[$rec]\" value=\"".$results["TARGET_VALUE"][$rec]."\"></input></TD>";
    }	
            echo '<INPUT type="hidden" name="target_id['.$rec.']" value="'.$results["TARGET_ID"][$rec].'"></input>';

    ?>

    <!--	<TD><?php echo $results["TARGET_ID"][$rec]; ?></TD> -->
            <?PHP echo "<INPUT type=\"hidden\" name=\"pk_vlnk_pipemap[$rec]\" value=\"".$results["PK_VLNK_PIPEMAP"][$rec]."\"></input>" ?>
    <?php	
            echo "</TR>";
}
?>
</TABLE>
<input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onmouseover="this.src='./img/submit_m.png';" onmouseout="this.src='./img/submit.png';" /> 

</form>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
