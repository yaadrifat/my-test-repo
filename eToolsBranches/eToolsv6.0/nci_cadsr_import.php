<?php
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velos eTools -> caDSR Integration</title>
<script>
function getCDEData() {
 window.open ("http://cdebrowser.nci.nih.gov/CDEBrowser/","mywindow","status=1,toolbar=1");
}
</script>
</head>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
<body>
<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	
<?php

if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 

	$v_sql = "select pk_catlib || '|' || fk_account as pk_catlib,ac_name || ' - ' || catlib_name as catlib_name from er_catlib,er_account WHERE pk_account = fk_account and catlib_type = 'T' order by ac_name || ' - ' || lower(catlib_name)";
	$results = executeOCIQuery($v_sql,$ds_conn);
	$cat_dropdown = '';
	for ($rec = 0; $rec < $results_nrows; $rec++){
		$cat_dropdown .= "<option value=".$results["PK_CATLIB"][$rec].">".$results["CATLIB_NAME"][$rec]."</option>";
	}

?>

<div class="navigate">caDSR Integration:</div>
 

<form enctype="multipart/form-data" action="nci_cadsr_import.php" method="POST">
<table>
<tr><td>STEP 1:</td><td><a href="#" onclick="getCDEData();">Get data from CDE Browser</a></td></tr>
<tr><td> <br></td></tr>
<tr><td>STEP 2:</td><td>Upload File:</td></tr>
<tr><td> </td><td><input type="file" name="uploadedfile" size="75"/></td></tr>
<tr><td> <br></td></tr>
<tr><td>STEP 3:</td><td>Import</td></tr>
<TR><TD>
&nbsp;</TD><TD>Form Category: <select name="category"><?PHP echo $cat_dropdown; ?></select></TD></TR>
<tr><td> </td><td>Import to Form Library:<INPUT checked TYPE="CHECKBOX" name="expForm"/> Specify Form Name: <input type="text" name="formname" size="50"/></td></tr>
<tr><td> </td><td>Import to Field Library:<INPUT checked TYPE="CHECKBOX" name="expField"/></td></tr>

<tr><td><input type="submit" name="submit" value="Submit"/></td></tr>
</table>
</form>
<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Note">
<tr>
<th align="left">Note</th>
</tr>
<tr><td align="left" valign="top">
<p>STEP 1: Download data elements in Excel format by clicking on the link "Get data from CDE Browser" and save it to your local folder.</p>
<p>STEP 2: Select the downloaded Excel file.</p>
<p>STEP 3: Import to Form and/or Field Library by selecting the appropriate name. Remember to specify a Form Name if importing into Form Library.</p>
</td></tr>
</table></div>

<?php } else {

echo "CDE import in progress...<BR><BR>";
flush();
	
  $curDir = getcwd();
  $target_path = $curDir."/upload/";

  $target_path = $target_path.basename( $_FILES['uploadedfile']['name']);

	$v_expForm = 0;
	$v_expField = 0;
  if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {

	if (isset($_POST["expForm"])) $v_expForm = 1;
	if (isset($_POST["expField"])) $v_expField = 1;

	list($v_category,$v_account) = explode("|",$_POST["category"]);

//	$v_category = 147;
//	$v_account = 51;


	if ($v_expForm == 1) {
		$v_formname = $_POST["formname"];
		$v_formname = substr($v_formname,0,49);
		$results = executeOCIQuery("SELECT pk_codelst from er_codelst where codelst_type='frmlibstat' and codelst_subtyp='W'",$ds_conn);
		$v_form_status = $results["PK_CODELST"][0];
		$results = executeOCIQuery("SELECT seq_er_formlib.NEXTVAL as pk_formlib FROM dual",$ds_conn);
		$v_pk_formlib = $results["PK_FORMLIB"][0];
		$v_sql = "insert into er_formlib (pk_formlib,fk_catlib,fk_account,form_name,form_desc,form_sharedwith,form_status,form_linkto,form_xslrefresh,record_type) values ($v_pk_formlib,$v_category,$v_account,'$v_formname','$v_formname','A',$v_form_status,'L',1,'N')";
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);

		// Insert records in er_objectshare
		$results = executeOCIQuery("SELECT seq_er_objectshare.NEXTVAL as pk_objectshare FROM dual",$ds_conn);
		$v_pk_objectshare = $results["PK_OBJECTSHARE"][0];
		$v_sql = "insert into er_objectshare (pk_objectshare,object_number,fk_object,fk_objectshare_id,objectshare_type,record_type) values ($v_pk_objectshare,1,$v_pk_formlib,$v_account,'A','N')";
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);

		$v_sql = "insert into er_objectshare (pk_objectshare,object_number,fk_object,fk_objectshare_id,objectshare_type,record_type,fk_objectshare) select seq_er_objectshare.NEXTVAL,1,$v_pk_formlib,pk_user,'U','N',$v_pk_objectshare from er_user where fk_account = $v_account and usr_type <> 'X'";
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);

		// Form section
		$results = executeOCIQuery("SELECT seq_er_formsec.NEXTVAL as pk_formsec FROM dual",$ds_conn);
		$v_pk_formsec = $results["PK_FORMSEC"][0];
		$v_sql = "insert into er_formsec (pk_formsec,fk_formlib,formsec_name,formsec_seq,formsec_fmt,formsec_repno,record_type) values ($v_pk_formsec,$v_pk_formlib,' ','1','N',0,'N')";
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);

		// Insert record in ER_FLDLIB (data entry date)
		$results = executeOCIQuery("SELECT seq_er_fldlib.NEXTVAL as pk_field FROM dual",$ds_conn);
		$v_pk_field = $results["PK_FIELD"][0];
		$v_sql = "insert into er_fldlib (pk_field,fk_account,fld_libflag,fld_name,fld_desc,fld_uniqueid,fld_systemid,fld_type,fld_datatype,record_type,fld_align,fld_display_width,fld_bold) values ($v_pk_field,$v_account,'F','Data Entry Date','','er_def_date_01','er_def_date_01','E','ED','N','left',35,1)";
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);

		$v_sql = "insert into er_formfld (pk_formfld,fk_formsec,fk_field,formfld_seq,formfld_mandatory,formfld_browserflg,record_type) values (seq_er_formfld.nextval,$v_pk_formsec,$v_pk_field,0,1,1,'N')";
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);
	}


	require_once './excelread/Excel/reader.php';

	//require_once 'fileupload.php';


	// ExcelFile($filename, $encoding);
	$data = new Spreadsheet_Excel_Reader();


	// Set output Encoding.
	$data->setOutputEncoding('CP1251');
	$filename = $target_path;
//	$v_tablename = $_GET["tabname"];

	
	$data->read($filename);
	error_reporting(E_ALL ^ E_NOTICE);


$v_sql = "";
$v_fldseq = 0;

for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) {
	for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
		if ($i == 1){
			switch ($data->sheets[0]['cells'][$i][$j] ){
			case 'Data Element Short Name':
				$v_fldid_pos = $j;
				break;
			case 'Data Element Long Name':
				$v_fldname_pos = $j;
				break;
			case 'Data Element Preferred Definition':
				$v_flddesc_pos = $j;
				break;
			case 'Data Element Public ID':
				$v_fldkwd_pos = $j;
				break;
			case 'Data Element Concept Context Name':
				$v_fldcat_pos = $j;
				break;
			case 'Value Domain Type':
				$v_fldtype_pos = $j;
				break;
			case 'Value Domain Datatype':
				$v_flddatatype_pos = $j;
				break;
			case 'Value Domain Max Length':
				$v_fldmaxlen_pos = $j;
				break;
			case 'Value Domain Min Value':
				$v_fldminval_pos = $j;
				break;
			case 'Value Domain Max Value':
				$v_fldmaxval_pos = $j;
				break;
			case 'Valid Values':
				$v_flddispval_pos = $j;
				break;
			case 'Value Meaning Name':
				$v_flddataval_pos = $j;
				break;
			case 'Data Element Version':
				$v_fldver_pos = $j;
				break;
			}
		}
	}

	if (strlen($data->sheets[0]['cells'][$i][$v_fldname_pos]) > 0 && $i > 1) {
//echo "Field name($i): ".$data->sheets[0]['cells'][$i][$v_fldname_pos]."<BR>";
		$v_fieldName = str_replace("'","''",$data->sheets[0]['cells'][$i][$v_fldname_pos]);
		$v_fieldId = str_replace("'","''",$data->sheets[0]['cells'][$i][$v_fldid_pos]);
		$v_fieldVersion = $data->sheets[0]['cells'][$i][$v_fldver_pos];
		$v_fieldDesc = str_replace("'","''",$data->sheets[0]['cells'][$i][$v_flddesc_pos])." (Version:".$v_fieldVersion.")";
		$v_fieldKeyword = $data->sheets[0]['cells'][$i][$v_fldkwd_pos];
		$v_fieldCategory = str_replace("'","''",$data->sheets[0]['cells'][$i][$v_fldcat_pos]);
		$v_fieldLength = $data->sheets[0]['cells'][$i][$v_fldmaxlen_pos];
		if (strlen($v_fieldLength) == 0) $v_fieldLength = 'NULL';
		$v_respSeq = 1;
		$v_respSeq_fldlib = 1;
	
		$v_fldType = "";
		$v_fldDatatype = "";
		$v_fldcolcount = "null";
		if ($data->sheets[0]['cells'][$i][$v_fldtype_pos] == "Non Enumerated"){
			switch ($data->sheets[0]['cells'][$i][$v_flddatatype_pos]){
			case "CHARACTER":
				$v_fldType = "E";
				$v_fldDatatype = 'ET';
				break;
			case "DATE":
				$v_fldType = "E";
				$v_fldDatatype = 'ED';
				break;
			case "NUMBER":
				$v_fldType = "E";
				$v_fldDatatype = 'EN';
				break;
			case "COMMENT":
				$v_fldType = "C";
				$v_fldDatatype = '';
				$v_fldinstructions = stripslashes(str_replace("'","''",$v_fieldName[$i]));
				break;
			}
		} else {
			$v_fldType = "M";
			$v_fldDatatype = 'MD';
			$v_fldcolcount = "1";
		}

		if 	($v_expForm == 1) {
			$results = executeOCIQuery("SELECT seq_er_fldlib.NEXTVAL as pk_field FROM dual",$ds_conn);
			$v_pk_field = $results["PK_FIELD"][0];
			$v_sql = "insert into er_fldlib (pk_field,fk_account,fld_libflag,fld_name,fld_desc,fld_uniqueid,fld_type,fld_datatype,record_type,fld_align,fld_keyword,fld_charsno,fld_display_width,fld_bold,fld_colcount) values 
			($v_pk_field,$v_account,'F','$v_fieldName','$v_fieldDesc','$v_fieldId','$v_fldType','$v_fldDatatype','N','left','$v_fieldKeyword',$v_fieldLength,35,1,$v_fldcolcount)";
			$results = executeOCIUpdateQuery($v_sql,$ds_conn);
			$v_sql = "insert into er_fldvalidate (pk_fldvalidate,fk_fldlib,record_type) values (seq_er_fldvalidate.nextval,$v_pk_field,'N')";
			$results = executeOCIUpdateQuery($v_sql,$ds_conn);
			$v_fldseq = $v_fldseq + 1;
			$v_sql = "insert into er_formfld (pk_formfld,fk_formsec,fk_field,formfld_seq,formfld_browserflg,record_type) values 
			(seq_er_formfld.nextval,$v_pk_formsec,$v_pk_field,$v_fldseq,0,'N')";
			$results = executeOCIUpdateQuery($v_sql,$ds_conn);
		}

		if 	($v_expField == 1) {
			$results = executeOCIQuery("SELECT PK_CATLIB FROM er_catlib where fk_account = $v_account and catlib_type = 'C' and record_type <> 'D' and upper(catlib_name) = '".strtoupper($v_fieldCategory)."'",$ds_conn);
			if (isset($results["PK_CATLIB"][0])){
				$v_pk_fieldcat = $results["PK_CATLIB"][0];
			} else {
				$results = executeOCIQuery("SELECT seq_er_catlib.NEXTVAL as pk_fieldcat FROM dual",$ds_conn);
				$v_pk_fieldcat = $results["PK_FIELDCAT"][0];
				$v_sql = "insert into er_catlib (pk_catlib,fk_account,catlib_type,record_type,catlib_name) values 
				($v_pk_fieldcat,$v_account,'C','N','$v_fieldCategory')";
				$results = executeOCIUpdateQuery($v_sql,$ds_conn);
				
			}
			
			$results = executeOCIQuery("SELECT PK_field FROM er_fldlib where fk_account = $v_account and fld_libflag = 'L' and record_type <> 'D' and upper(fld_name) = '".strtoupper($v_fieldName)."'",$ds_conn);
			if (isset($results["PK_FIELD"][0])){
				$v_pk_fieldlib = $results["PK_FIELD"][0];
				$v_newfield = 0;
			} else {
				$v_newfield = 1;
				$results = executeOCIQuery("SELECT seq_er_fldlib.NEXTVAL as pk_field FROM dual",$ds_conn);
				$v_pk_fieldlib = $results["PK_FIELD"][0];
				$v_sql = "insert into er_fldlib (pk_field,fk_account,fld_libflag,fld_name,fld_desc,fld_uniqueid,fld_type,fld_datatype,record_type,fld_align,fld_keyword,fld_charsno,fld_display_width,fld_bold,fk_catlib,fld_colcount) values 
				($v_pk_fieldlib,$v_account,'L','$v_fieldName','$v_fieldDesc','$v_fieldId','$v_fldType','$v_fldDatatype','N','left','$v_fieldKeyword',$v_fieldLength,35,1,$v_pk_fieldcat,$v_fldcolcount)";
				$results = executeOCIUpdateQuery($v_sql,$ds_conn);
				$v_sql = "insert into er_fldvalidate (pk_fldvalidate,fk_fldlib,record_type) values (seq_er_fldvalidate.nextval,$v_pk_fieldlib,'N')";
				$results = executeOCIUpdateQuery($v_sql,$ds_conn);
			}
		}

		}

	if ($v_fldType == "M" && strlen($data->sheets[0]['cells'][$i][$v_flddispval_pos]) > 0){
		$v_mvalues_disp = str_replace("'","''",$data->sheets[0]['cells'][$i][$v_flddispval_pos]);
		$v_mvalues_data = str_replace("'","''",$data->sheets[0]['cells'][$i][$v_flddataval_pos]);
		
		if 	($v_expForm == 1) {
			$v_sql = "insert into er_fldresp (pk_fldresp,fk_field,fldresp_seq,fldresp_dispval,fldresp_dataval,fldresp_score,record_type) values 
			(seq_er_fldresp.nextval,$v_pk_field,$v_respSeq,'$v_mvalues_disp','$v_mvalues_data',0,'N')";
			$results = executeOCIUpdateQuery($v_sql,$ds_conn);
			$v_respSeq = $v_respSeq + 1;
		}
		if 	($v_expField == 1 && $v_newfield == 1) {
			$v_sql = "insert into er_fldresp (pk_fldresp,fk_field,fldresp_seq,fldresp_dispval,fldresp_dataval,fldresp_score,record_type) values 
			(seq_er_fldresp.nextval,$v_pk_fieldlib,$v_respSeq_fldlib,'$v_mvalues_disp','$v_mvalues_data',0,'N')";
			$results = executeOCIUpdateQuery($v_sql,$ds_conn);
			$v_respSeq_fldlib = $v_respSeq_fldlib + 1;
		}
		
		
	}

}

if 	($v_expField == 1) echo "CDE's imported.<BR><BR>";


//echo "Data Imported to table: ".$v_tablename;
unlink($filename);
OCICommit($ds_conn);
OCILogoff($ds_conn);
if ($v_expForm == 1) {
echo "<font color=red><b>Do not close the popup window. Popup window will close automatically when processing is complete.</b></font>"
?>
<SCRIPT LANGUAGE="javascript">
window.open ('<?PHP echo $_SESSION["URL"]; ?>/eres/jsp/refreshFormFields.jsp?formIdList=<?PHP echo $v_pk_formlib; ?>', 'newwindow', config='height=100,width=400, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no')
</SCRIPT>
<?PHP
}
  } else{
      echo "There was an error uploading the file, please try again!"."<BR>";
  }





}

?>

	
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
