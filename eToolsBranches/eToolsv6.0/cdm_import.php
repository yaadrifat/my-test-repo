<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velos eTools -> Charge Description Master Import</title>
<script>
function validate(form){


	if (form.uploadedfile.value == ""){
		alert("Choose a file to upload.");
		form.uploadedfile.focus();
		return false;
	}

	if (form.delimiter.value == ""){
		alert("Delimiter cannot be blank.");
		form.delimiter.focus();
		return false;
	}
	
	return true;
}
</script>

</head>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body>
<div id="fedora-content">	
<div class="navigate">Charge Description Master Import</div>
<br>
<?PHP
if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
	$v_sql = "SELECT pk_user,usr_firstname || ' ' || usr_lastname AS username FROM ER_USER WHERE USR_STAT = 'A' ORDER BY lower(usr_firstname || ' ' || usr_lastname)";
	$results = executeOCIQuery($v_sql,$ds_conn);
	$v_users = "";
	for ($rec = 0; $rec < $results_nrows; $rec++){
		$results["USERNAME"][$rec] = str_replace(array("<script>","</script>"),array("",""),$results["USERNAME"][$rec]);
		$v_users .= "<option value=".$results["PK_USER"][$rec]." >".$results["USERNAME"][$rec]."</option>";
	}

	$v_sql = "select pk_codelst,codelst_desc from sch_codelst where codelst_type = 'currency'";
	$results = executeOCIQuery($v_sql,$ds_conn);
	$v_currency = "";
	for ($rec = 0; $rec < $results_nrows; $rec++){
		$v_currency .= "<option value=".$results["PK_CODELST"][$rec]." >".$results["CODELST_DESC"][$rec]."</option>";
	}
	
?>
<form name="fimport" enctype="multipart/form-data" action="cdm_import.php" method="POST" onSubmit="if (validate(document.fimport) == false) return false;">
<table>
<tr><td>Delimited CDM File name:</td><td><input class="required" type="file" name="uploadedfile" size="75"/></td></tr> 
<tr><td>Delimiter:</td><td><input class="required" type="text" name="delimiter" size="2" value=","/></td></tr>
<TR><TD>Audit Trail User:</TD><TD><SELECT class="required" NAME="user"><?PHP echo $v_users; ?></SELECT></TD></TR>
<TR><TD>Currency:</TD><TD><SELECT class="required" NAME="currency"><?PHP echo $v_currency; ?></SELECT></TD></TR>
<tr><td><input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" /> </td><td align="right">Sample template: <a href="template/chargemaster_template.csv">chargemaster_template.csv</a></td>
</tr>
</table>
</form>



<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Note">
<tr>
<th align="left">Note</th>
</tr>
<tr><td align="left" valign="top">
<p>Supported File Types: Any delimited file (delimiters such as comma, semicolon, pipe etc. are commonly used)</p>
<p>First row of the file should contain Column Name, no special characters other than '_' (underscore), should be one continuous string of characters (no space) and delimited with same delimiter as the actual data rows.</p>
<p>Data row shouldn't contain special characters " or ' </p>
</td></tr>
</table></div>


<?php } else { 


$delimiter = $_POST["delimiter"];

$curDir = getcwd();
$target_path = $curDir."/upload/";

$target_path = $target_path.basename( $_FILES['uploadedfile']['name']);



if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
//  echo "The file ".  basename( $_FILES['uploadedfile']['name']).
  " has been uploaded"."<BR>";

	$handle = fopen($target_path, "r");
	$data = fgetcsv($handle, 10000, $delimiter);
//	array_push($data, "BUDGET CATEGORY");
//	sort($data);
	$num = count($data);
	$dd_columns = '<option value=""></option>';
	for ($c=0; $c < $num; $c++) {
		$dd_columns .= "<option value=".$c.">".$data[$c]."</option>";
	}	
	fclose($handle);
	
	$bc_query = "SELECT PK_CODELST, CODELST_DESC FROM SCH_CODELST WHERE CODELST_TYPE='category'";
	$bcresults = executeOCIQuery($bc_query,$ds_conn);	
	for ($j=0; $j < $results_nrows; $j++) {
		$bc_columns .= "<option value=".$bcresults["PK_CODELST"][$j].">".strtoupper($bcresults["CODELST_DESC"][$j])."</option>";
	}	



	echo '<form name="cdm" method="post" action="cdm_import_save.php">';
	echo '<table width="50%" border="1">';
	echo '<tr><td>Eventy Library Type</td><td><select class="required" name="libtype">'.$dd_columns.'</select></td></tr>';	
	echo '<tr><td>Category</td><td><select class="required" name="category">'.$dd_columns.'</select></td></tr>';
	echo '<tr><td>Event Name</td><td><select class="required" name="event">'.$dd_columns.'</select></td></tr>';
	echo '<tr><td>CPT Code</td><td><select name="cpt">'.$dd_columns.'</select></td></tr>';
	echo '<tr><td>Description</td><td><select name="desc">'.$dd_columns.'</select></td></tr>';
	echo '<tr><td>Notes</td><td><select name="notes">'.$dd_columns.'</select></td></tr>';
	echo '<tr><td>Budget Categories</td><td><select name="budgetcat">'.$bc_columns.'</select></td></tr>';
	echo '<tr><td>Site of Service</td><td><select name="site_of_service">'.$dd_columns.'</select></td></tr>';
	echo '<tr><td>Facility</td><td><select name="facility">'.$dd_columns.'</select></td></tr>';
	echo '<tr><td>Coverage Type</td><td><select name="coverage_type">'.$dd_columns.'</select></td></tr>';
	echo '<tr><td>Coverage Type Notes</td><td><select name="coverage_type_notes">'.$dd_columns.'</select></td></tr>';
	
	$codeQ = "select CODELST_DESC from er_codelst where codelst_type='evtaddlcode' and CODELST_CUSTOM_COL is null order by CODELST_SEQ";
	$results = executeOCIQuery($codeQ,$ds_conn);
	for($j=0; $j<$results_nrows; $j++){		
		echo '<tr><td>Additional Code List - '.$results["CODELST_DESC"][$j].'</td><td><select name="addcodelst['.$j.']">'.$dd_columns.'</select></td></tr>';
		echo '<input type="hidden" name="addcodelst_pk['.$j.']" value="'.$j.'">';		
	} 

	
/*	for($j=1; $j<6; $j++){		
		echo '<tr><td>Additional Code List '.$j.'</td><td><select name="addcodelst['.$j.']">'.$dd_columns.'</select></td></tr>';
		echo '<input type="hidden" name="addcodelst_pk['.$j.']" value="'.$j.'">';		
	} */
	$query_sql = "select pk_codelst,codelst_desc from sch_codelst where codelst_type = 'cost_desc' order by CODELST_DESC";
	$results = executeOCIQuery($query_sql,$ds_conn);
	for ($rec = 0; $rec < $results_nrows; $rec++){
		echo '<tr><td>Cost - '.$results["CODELST_DESC"][$rec].'</td><td><select name="cost['.$rec.']">'.$dd_columns.'</select></td></tr>';
		echo '<input type="hidden" name="cost_pk['.$rec.']" value="'.$results["PK_CODELST"][$rec].'">';
	}
	
	echo '</table>';
	echo '<BR><input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onmouseover="this.src=\'./img/submit_m.png\';" onmouseout="this.src=\'./img/submit.png\';" /> ';
	echo '<INPUT TYPE="hidden" name="filepath" value="'.$target_path.'">';
	echo '<INPUT TYPE="hidden" name="delimiter" value="'.$delimiter.'">';
	echo '<INPUT TYPE="hidden" name="user" value="'.$_POST["user"].'">';
	echo '<INPUT TYPE="hidden" name="currency" value="'.$_POST["currency"].'">';
	echo '</form>';

//	echo '<select name="column_name">'.$dd_columns."</select>";
} else {
echo "Error uploading file.";
}

} ?>

	
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
