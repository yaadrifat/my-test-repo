<?php
session_start();	// Maintain session state
//require_once "./includes/session.php";
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){

include("./includes/oci_functions.php");
require_once('audit_queries.php');
include ("./txt-db-api.php");
include("db_config.php");
$db = new Database("etools");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

//$grpName = $db->executeQuery("SELECT GROUP_NAME from ET_GROUPS WHERE PK_GROUPS =".$_SESSION["FK_GROUPS"]);
//$grpName->next();
//$grpNameVal = $grpName->getCurrentValuesAsHash();
$grpName = mysql_query("SELECT group_name from et_groups WHERE pk_groups =".$_SESSION["FK_GROUPS"]);	
$grpNameVal = mysql_fetch_array($grpName);



if (isset($_GET['action'])){
	if ($_GET['action'] == 'CLIPBOARD' && $_GET['table'] != 'undefined') {
		$v_tablestr = strtoupper($_GET['table']);
		list($v_owner,$v_table) = explode(".",$v_tablestr);
		$query="select column_name,data_type,data_length from dba_tab_cols where owner = '$v_owner' and table_name = '$v_table' order by column_id";
		$results = executeOCIQuery($query,$ds_conn);
		$v_html = "";
		for ($rec = 0; $rec < $results_nrows; $rec++){
			$v_html .= $results["COLUMN_NAME"][$rec].", ";
		}
		$v_html = (strlen($v_html) > 0 ? substr($v_html,0,strlen($v_html)-2)  : "");
		echo trim($v_html);

		/*--- AUDIT ---*/
		$colArray = array('CLIPBOARD');
		$colvalarray = array($v_html);
		$tblname = '';
		colQueries($colArray, $colvalarray, $tblname);
		/*--- END ---*/		
	}
}

if (isset($_GET['table']) and $_GET['action'] != 'CLIPBOARD'){
	$v_tablestr = strtoupper($_GET['table']);
	list($v_owner,$v_table) = explode(".",$v_tablestr);
	$query="select column_name,data_type,data_length from dba_tab_cols where owner = '$v_owner' and table_name = '$v_table' order by column_id";
	$results = executeOCIQuery($query,$ds_conn);
	//$v_html = "Table Name: $v_table<table border='1'><th>COLUMN NAME</th><th>TYPE</th><th>SIZE</th>";	
	$v_html = "";
	for ($rec = 0; $rec < $results_nrows; $rec++){
		$v_html .= '<table border="1" width="100%"><tr onMouseOver="bgColor=\'#a4bef1\'" onMouseOut="bgColor=\'#FFFFFF\'">'."<td onclick='document.getElementById(\"clipboard\").value = document.getElementById(\"clipboard\").value + this.innerHTML + \", \"'  width='50%'>".$results["COLUMN_NAME"][$rec]."</td><td  width='30%'>".$results["DATA_TYPE"][$rec]."</td><td  width='15%'>".$results["DATA_LENGTH"][$rec]."</td></tr></table>";
	}
	if ($results_nrows == 0) $v_html .= "No tables found";
	$v_html .= "</table>";
	echo $v_html;

}

OCICommit($ds_conn);
OCILogoff($ds_conn);
}
else header("location: index.php?fail=1");
?>