<?php
session_start();
header("Cache-control: private");
if(@$_SESSION['user']) header("location: login.php");
else{
?>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html"; charset="utf-8">
    <link rel="stylesheet" type="text/css" media="print" href="css/print.css">
    <style type="text/css" media="screen">
            @import url("css/layout.css");
            @import url("css/content.css");
            @import url("css/docbook.css");
    </style>
    <meta name="MSSmartTagsPreventParsing" content="TRUE">	
<?PHP
    $file = file("register.php") or die("Authorization required!");
    $totalLines = sizeof($file);
    $line = 0;
    $match = 0;
    do{
        if("//" != substr($file[$line], 0, 2)){
            @list($ip_address, $user_name) = explode("|", $file[$line]);
            if($ip_address == $_SERVER["REMOTE_ADDR"]) $match = 1;
            else $match = 0;
        }
        if($match) break;
        $line++;
    } while($line < $totalLines);
?>
<title>Velos eTools</title>
</head>
<body onload = "document.getElementById('user').focus();">
<?php 
include("db_config.php");
$ver_result = mysql_query("SELECT * from et_version");
while($row = mysql_fetch_array($ver_result)) {
    $version_num = $row["et_version_num"];
    $build_num = $row["et_build_num"];
    $eres_ver = $row["eres_ver"];       
}
?>
<div id="fedora-header">
    <div id="fedora-header-logo" >
        <a href="http://www.veloseresearch.com" id="link-internal"><img src="img/velos_etools_v3.png" alt="Velos eTools"></a><br>
        <!-- <span>Version <b style="color:red;"><?php echo $version_num ?></b> Build#<b style="color:red;"><?php //echo $build_num ?></b></span> -->
        <span>Version <b style="color:red;"><?php echo $version_num ?></b> Build#<b style="color:red;"><?php echo $build_num ?></b> <i style="font-size:11px; color:gray;"> for <B><?php echo $eres_ver ?></b></i></span><br>
    </div>
</div>
<div id="fedora-nav"></div>
<div>
<form method = "post" action = "login.php">				
<BR><BR><BR>
<table border = "1" cellspacing = "0" cellpadding = "0" width = "300" height="200" align="center" valign = "top" bgcolor="ccccccc" >
<tr><td><table width="263" border="0" align="center">
<tr><td><img src="img/security.png" alt="Velos eTools"></td>
    <td align="left" height = "5%">
    <font size="5"><B>Login</B></font>
    </td>
    </tr>
    <tr>
        <td width = "94" height = "10" valign = "bottom">
            <BR>
            <BR><B>User Name:</B>
        </td>
        <td width = "159" height = "10" valign = "bottom">
            <input type = "text" id = "user" name = "user" style = "width: 150" class = "text" tabindex = "1">
        </td>
    </tr>
    <tr>
        <td width = "94" height = "10" valign = "bottom">
            <B>Password:</B>		</td>
        <td width = "159" height = "10" valign = "bottom">
            <input type = "password" name = "pass" style = "width: 150" class = "text" tabindex = "2">
        </td>
    </tr>
    <tr>
        <td  colspan = "2" align = "center">
            <BR><input type = "image" src = "./img/loginbutton.png" width  = "128" height = "23" name = "submit" alt = "login" tabindex = "3">
        </td>
    </tr>
</table>
</td></tr></table>
<table width="100%" border="0"><tr><td align="center">
<?php
    if(@$_GET["fail"]) echo "<font size=3 color=red>Incorrect username or password</font>";
    elseif(@$_GET["logout"]) echo " <font size=3 color=green><span class = 'alert'>Successfully logged out</span></font>";
    //			elseif(@$_GET["new"]) echo " <span class = 'alert'>Successfully registered</span>";
?>
&nbsp;</td></tr></table>
<table><tr><td><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>&nbsp;</td></tr></table>
</form>
</div>
</body>
</html>
<?php
}
?>