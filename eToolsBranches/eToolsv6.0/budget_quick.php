<?php
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velos eTools -> Budget from File</title>
<script>
	var studies = new Array();
	var studiesTrim = new Array();
		function usrChng(str){
			if (window.XMLHttpRequest){
			  xmlhttp=new XMLHttpRequest();
			  }else{
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function(){
			  if (xmlhttp.readyState==4 && xmlhttp.status==200){
					tmpRs = xmlhttp.responseText;
					studies = tmpRs.split('-#$^');
				}
				for(i=0; i<studies.length; i++){
					if(i==0){
						studiesTrim[i] = studies[i].substring(6,studies[i].length);
					}else{
						studiesTrim[i] = studies[i];
					}
				}
			  }
			xmlhttp.open("GET","bgt_std.php?q="+str, true);
			xmlhttp.send();			
		}


function validate(form){
	if (form.bname.value == ""){
		alert("Budget name cannot be blank.");
		form.bname.focus();
		return false;
	}

	if (form.snum.value == ""){
		alert("Study number cannot be blank.");
		form.snum.focus();
		return false;
	}

	if (form.uploadedfile.value == ""){
		alert("Choose a file to upload.");
		form.uploadedfile.focus();
		return false;
	}

	if (form.delimiter.value == ""){
		alert("Delimiter cannot be blank.");
		form.delimiter.focus();
		return false;
	}
	
	var count = studies.length;
	var found = false;
	studynumber = form.snum.value.toUpperCase();
	for (var i=0; i<count; i++){
		if (studynumber == studiesTrim[i]){
			found = true;
			break;
		}
	}
	if (!studynumber) {
		alert("Study number does not exist.");
		form.snum.focus();
		return false;
	}
	return true;
}


function refreshlist(studynum){
	studynumber=studynum.toUpperCase();	
	
	var length=studynum.length;
	var count = studies.length;
		
	var slist = "";
		for (var i=0; i<count; i++){
				if (studynumber == studiesTrim[i].substring(0,length)){
					slist = slist + '<tr onMouseOver="bgColor=\'#a4bef1\'" onMouseOut="bgColor=\'#FFFFFF\'">'+"<td ondblclick='document.budgetimport.snum.value = this.innerHTML'>" + studiesTrim[i] + "</td></tr>";
				}			
			if (length == 0 && i==101) break;
			
		}
	slist = (slist.length == 0) ? slist="No studies found":"<i>Double click on study number below to populate 'Study Number'<i><br><table class=ttip width='50%'>"+slist+"</table>";
	document.getElementById('studies').innerHTML = slist;
}

function clearlist() {
	document.getElementById('studies').innerHTML = "";
}


</script>
</head>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 



?>
<body>
<div id="fedora-content">	
<?PHP
if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 	
		$v_sql = "SELECT pk_user,usr_firstname || ' ' || usr_lastname AS username FROM ER_USER WHERE USR_STAT = 'A' ORDER BY lower(usr_firstname || ' ' || usr_lastname)";
		$results = executeOCIQuery($v_sql,$ds_conn);
		$v_users = "";
		for ($rec = 0; $rec < $results_nrows; $rec++){
			$results["USERNAME"][$rec] = str_replace(array("<script>","</script>"),array("",""),$results["USERNAME"][$rec]);
			$v_users .= "<option value=".$results["PK_USER"][$rec]." >".$results["USERNAME"][$rec]."</option>";
		}

?>

<div class="navigate">Budget from File:</div>
	<form name="budgetimport" enctype="multipart/form-data" action="budget_quick.php" method="POST" onSubmit="if (validate(document.budgetimport) == false) return false;">
	<table width="100%">
	<tr><td>Budget Name:</td><td><input class="required" type="text" name="bname" size="75" maxlength="100" onFocus="clearlist();" /></td></tr>
	<TR><TD>Audit Trail User:</TD><TD><SELECT class="required" NAME="user" onFocus="clearlist();" onChange="usrChng(document.budgetimport.user.value);" ><?PHP echo $v_users; ?></SELECT></TD></TR>	
	<tr><td>Study Number:</td><td><input class="required" type="text" name="snum" size="75"  maxlength="100" onFocus="refreshlist(document.budgetimport.snum.value);" onKeyUp="refreshlist(document.budgetimport.snum.value);" /></td></tr>
	<tr><td>Delimited File name:</td><td><input class="required" type="file" name="uploadedfile" size="75" onFocus="clearlist();"/></td></tr>
	<tr><td>Delimiter:</td><td><input class="required" type="text" name="delimiter" size="2" value="," onFocus="clearlist();"/></td></tr>
	<tr><td><input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" /></td><td align="right">Sample template: <a href="./template/budget_template.csv">budget_template.csv</a></td></tr>
	</table>
	</form>
	</table>	
	<style>
            DIV.studies {
                width:50%;
                height:100px;
                overflow: auto;
                align:left;
                margin: 0 0 0 17px;
            }
	</style>	
	<!--<span><div class="studies" id="studies"></div></span>-->
     <span><div class="studies"><table><tr><td width="215">&nbsp;</td><td id="studies" width="400"></td></tr></table></div></span>
</div>

<script> budgetimport.bname.focus(); </script>

<?php } else {
$v_delimiter = $_POST["delimiter"];

$v_creator = $_POST["user"];
$v_query = "SELECT fk_account FROM er_user WHERE pk_user = $v_creator";

$results = executeOCIQuery($v_query,$ds_conn);
$v_account = $results["FK_ACCOUNT"][0];

  $curDir = getcwd();
  $target_path = $curDir."/upload/";

  $target_path = $target_path.basename( $_FILES['uploadedfile']['name']);

  if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {

	$handle = fopen($target_path, "r");
	$v_secname[0] = '';
	$v_category[0] = '';
	$v_sec_counter = 0;
	$v_cat_counter = 0;
	$i = 0;
	$v_counter = 0;
	while (($data = fgetcsv($handle, 10000, $v_delimiter)) !== FALSE) {
		if ($v_counter > 0) {
			$v_match = 0;
			for($j=0;$j<count($v_secname);$j++){
				if ($v_secname[$j] == trim($data[0])) $v_match = 1;
			}
			if ($v_match == 0) {
				$v_secname[$v_sec_counter] = trim($data[0]);
				$v_sec_counter++;
			}
			$v_match = 0;
			for($j=0;$j<count($v_category);$j++){
				if ($v_category[$j] == trim($data[1])) $v_match = 1;
			}
			if ($v_match == 0) {
				$v_category[$v_cat_counter] = trim($data[1]);
				$v_query = "SELECT pk_codelst FROM sch_codelst WHERE trim(codelst_type) = 'category' and lower(codelst_desc) = '".strtolower(trim($data[1]))."'";
				$results = executeOCIQuery($v_query,$ds_conn);
				if ($results_nrows == 1) {
					$v_categorypk[$v_cat_counter] = $results["PK_CODELST"][0];
				} else {
					$v_categorypk[$v_cat_counter] = 'NULL';
				}
				$v_cat_counter++;
			}
			$v_budsec[$i] = trim($data[0]);
			$v_budcat[$i] = trim($data[1]);
			$v_lineitem[$i] = trim($data[2]);
			$v_unitcost[$i] = trim($data[3]);
			$v_units[$i] = trim($data[4]);
		$i++;
		}
	$v_counter++;
	}

fclose($handle);

// Validations
// Check if Study exists

$v_bname = trim($_POST["bname"]);
$v_snum = $_POST["snum"];

$v_query = "SELECT count(*) as count FROM ER_STUDY WHERE fk_account = $v_account and upper(study_number) = upper('$v_snum')";
$results = executeOCIQuery($v_query,$ds_conn);
$v_stdexst= $results["COUNT"][0];
if ($v_stdexst == 1){
	$v_query = "SELECT pk_study FROM ER_STUDY WHERE fk_account = $v_account and upper(study_number) = upper('$v_snum')";
	$results = executeOCIQuery($v_query,$ds_conn);
	$v_stdpk= $results["PK_STUDY"][0];
}
// Check if Budget exists
//$v_query = "SELECT count(*) as count FROM sch_budget WHERE fk_account = $v_account and budget_name = '$v_bname' and budget_delflag <> 'Y'";
$v_query = "SELECT * FROM sch_budget WHERE fk_account = $v_account and budget_name = '$v_bname'";
$results = executeOCIQuery($v_query,$ds_conn);
$v_budexstcount = $results_nrows;

	if($v_budexstcount == 1){
		if($results["BUDGET_DELFLAG"][0] == 'N' || $results["BUDGET_DELFLAG"][0] == null){
			$v_budexst = 1;
		}else{
			$v_budexst = 0;
		}
	}

	if ($v_stdexst == 1 && $v_budexst == 0){
		$v_query = "SELECT seq_sch_budget.nextval as pk_budget from dual";
		$results = executeOCIQuery($v_query,$ds_conn);
		$v_budpk = $results["PK_BUDGET"][0];
	
		$v_query = "insert into sch_budget (PK_BUDGET,BUDGET_NAME,BUDGET_CREATOR,BUDGET_RIGHTSCOPE,BUDGET_TYPE,BUDGET_STATUS,BUDGET_CURRENCY,BUDGET_SITEFLAG,
		FK_STUDY,FK_ACCOUNT,BUDGET_RIGHTS,CREATOR,CREATED_ON,BUDGET_TEMPLATE,FK_CODELST_STATUS) values ($v_budpk,'$v_bname',$v_creator,'S',
		'P','W',(select pk_codelst from sch_codelst where trim(codelst_type) = 'currency' and trim(codelst_subtyp) = '$'),
		'0',$v_stdpk,$v_account,'777',$v_creator,sysdate,(select pk_codelst from sch_codelst where trim(codelst_type) = 'budget_template' and trim(codelst_subtyp) = 'patient_bgt'),
		(select pk_codelst from sch_codelst where codelst_type = 'budget_stat' and codelst_subtyp = 'W'))";	
	
		executeOCIUpdateQuery($v_query,$ds_conn);
	
		$v_query = "SELECT seq_sch_bgtcal.nextval as pk_bgtcal from dual";
		$results = executeOCIQuery($v_query,$ds_conn);
		$v_budcalpk = $results["PK_BGTCAL"][0];
	
		$v_query = "insert into sch_bgtcal (PK_BGTCAL,FK_BUDGET,BGTCAL_DELFLAG,BGTCAL_INDIRECTCOST,CREATOR,CREATED_ON,BGTCAL_FRGFLAG,
		BGTCAL_DISCOUNTFLAG,BGTCAL_EXCLDSOCFLAG) values ($v_budcalpk,$v_budpk,'N',0,$v_creator,sysdate,0,0,0)";	
		executeOCIUpdateQuery($v_query,$ds_conn);

		$v_budsecseq = 10;
		$ipAdd = $_SERVER['REMOTE_ADDR'];
		$systemDate = date("d-M-y");

		$v_query = "SELECT seq_sch_bgtsection.nextval as pk_bgtsec from dual";
		$results = executeOCIQuery($v_query,$ds_conn);
		$nxtVal = $results["PK_BGTSEC"][0];		
		$secQuery = "insert into sch_bgtsection (PK_BUDGETSEC,FK_BGTCAL,BGTSECTION_NAME,BGTSECTION_VISIT,BGTSECTION_DELFLAG,BGTSECTION_SEQUENCE,CREATOR,CREATED_ON,IP_ADD,BGTSECTION_TYPE,BGTSECTION_PERSONLFLAG) values ($nxtVal,$v_budcalpk,'Personnel Cost',null,null,$v_budsecseq,$v_creator,sysdate,'$ipAdd','P',1)";					
		executeOCIUpdateQuery($secQuery,$ds_conn);		

		$v_query = "SELECT seq_sch_bgtsection.nextval as pk_bgtsec from dual";
		$results = executeOCIQuery($v_query,$ds_conn);
		$nxtVal = $results["PK_BGTSEC"][0];
		$v_budsecseq = $v_budsecseq + 10;
		$secQuery = "insert into sch_bgtsection (PK_BUDGETSEC,FK_BGTCAL,BGTSECTION_NAME,BGTSECTION_VISIT,BGTSECTION_DELFLAG,BGTSECTION_SEQUENCE,CREATOR,CREATED_ON,IP_ADD,BGTSECTION_TYPE,BGTSECTION_PERSONLFLAG) values ($nxtVal,$v_budcalpk,'Miscellaneous','Miscellaneous','N',$v_budsecseq,$v_creator,'$systemDate','$ipAdd','P',0)";	
		executeOCIUpdateQuery($secQuery,$ds_conn);

		$v_query = "SELECT seq_sch_bgtsection.nextval as pk_bgtsec from dual";
		$results = executeOCIQuery($v_query,$ds_conn);
		$nxtVal = $results["PK_BGTSEC"][0];		
		$secQuery = "insert into sch_bgtsection (PK_BUDGETSEC,FK_BGTCAL,BGTSECTION_NAME,BGTSECTION_VISIT,BGTSECTION_DELFLAG,BGTSECTION_SEQUENCE,CREATOR,CREATED_ON,IP_ADD,BGTSECTION_TYPE,BGTSECTION_PERSONLFLAG) values ($nxtVal,$v_budcalpk,'Default section',null,'P',null,$v_creator,'$systemDate','$ipAdd',null,0)";	
		executeOCIUpdateQuery($secQuery,$ds_conn);

		$v_query = "SELECT seq_sch_bgtsection.nextval as pk_bgtsec from dual";
		$results = executeOCIQuery($v_query,$ds_conn);
		$nxtVal = $results["PK_BGTSEC"][0];		
		$secQuery = "insert into sch_bgtsection (PK_BUDGETSEC,FK_BGTCAL,BGTSECTION_NAME,BGTSECTION_VISIT,BGTSECTION_DELFLAG,BGTSECTION_SEQUENCE,CREATOR,CREATED_ON,IP_ADD,BGTSECTION_TYPE,BGTSECTION_PERSONLFLAG) values ($nxtVal,$v_budcalpk,'Repeat Default section',null,'R',null,$v_creator,'$systemDate','$ipAdd',null,0)";	
		executeOCIUpdateQuery($secQuery,$ds_conn);
		
		for($i=0;$i<count($v_secname);$i++){
			$v_query = "SELECT seq_sch_bgtsection.nextval as pk_bgtsec from dual";
			$results = executeOCIQuery($v_query,$ds_conn);
			$v_budsecpk[$i] = $results["PK_BGTSEC"][0];
			$v_query = "insert into sch_bgtsection (PK_BUDGETSEC,FK_BGTCAL,BGTSECTION_NAME,BGTSECTION_DELFLAG,BGTSECTION_SEQUENCE,CREATOR,CREATED_ON,
			BGTSECTION_TYPE,BGTSECTION_PERSONLFLAG) values ($v_budsecpk[$i],$v_budcalpk,'$v_secname[$i]','N',$v_budsecseq,$v_creator,sysdate,'P',0)";
			$v_budsecseq = $v_budsecseq + 10;
			executeOCIUpdateQuery($v_query,$ds_conn);
		}
			
			$pk_sec = "select PK_BUDGETSEC from sch_bgtsection where BGTSECTION_NAME='Miscellaneous' and FK_BGTCAL=".$v_budcalpk;
			$results = executeOCIQuery($pk_sec,$ds_conn);
			$pk_secNo = $results["PK_BUDGETSEC"][0];
			
			$codCat = "select PK_CODELST from sch_codelst where CODELST_DESC='Patient Care' and CODELST_SUBTYP='ctgry_patcare' and CODELST_TYPE='category'";
			$results = executeOCIQuery($codCat,$ds_conn);
			$codCat_pk = $results["PK_CODELST"][0];
			
			
			$v_query = "insert into sch_lineitem (PK_LINEITEM,FK_BGTSECTION,LINEITEM_SPONSORUNIT,LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,CREATOR,CREATED_ON,
			LINEITEM_NAME,LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,LINEITEM_INCOSTDISC,LINEITEM_APPLYINFUTURE,FK_CODELST_CATEGORY,
			LINEITEM_APPLYINDIRECTS, SUBCOST_ITEM_FLAG) values (seq_sch_lineitem.nextval,$pk_secNo,'0.0','1.0','0',$v_creator,sysdate,'Misc',null,null,null,null,null,$codCat_pk,'1','0')"; 
			executeOCIUpdateQuery($v_query,$ds_conn);
			
				
		for($i=0;$i<count($v_lineitem);$i++){
			for($j=0;$j<count($v_secname);$j++){
				if ($v_budsec[$i] == $v_secname[$j]){
					break;
				}
			}
			for($k=0;$k<count($v_category);$k++){
				if ($v_budcat[$i] == $v_category[$k]){
					break;
				}
			}
			
			$unitcost = (empty($v_unitcost[$i]))?'null':$v_unitcost[$i];
			$unit = (empty($v_units[$i]))?'null':$v_units[$i];
			
			if($unitcost!="" && $unit!=""){
				$linOthercost = $unitcost * $unit;
			}
			
	/*		$v_query = "insert into sch_lineitem (PK_LINEITEM,FK_BGTSECTION,LINEITEM_SPONSORUNIT,LINEITEM_CLINICNOFUNIT,CREATOR,CREATED_ON,
			LINEITEM_NAME,LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,LINEITEM_INCOSTDISC,LINEITEM_APPLYINFUTURE,FK_CODELST_CATEGORY,
			LINEITEM_APPLYINDIRECTS) values (seq_sch_lineitem.nextval,$v_budsecpk[$j],$unitcost,$unit,$v_creator,sysdate,
			'$v_lineitem[$i]',0,null,'0',0,0,$v_categorypk[$k],'1')";  */
	
			$v_query = "insert into sch_lineitem (PK_LINEITEM,FK_BGTSECTION,LINEITEM_SPONSORUNIT,LINEITEM_CLINICNOFUNIT,LINEITEM_OTHERCOST,CREATOR,CREATED_ON,
			LINEITEM_NAME,LINEITEM_INPERSEC,LINEITEM_STDCARECOST,LINEITEM_RESCOST,LINEITEM_INCOSTDISC,LINEITEM_APPLYINFUTURE,FK_CODELST_CATEGORY,
			LINEITEM_APPLYINDIRECTS, SUBCOST_ITEM_FLAG) values (seq_sch_lineitem.nextval,$v_budsecpk[$j],$unitcost,$unit,$linOthercost,$v_creator,sysdate,
			'$v_lineitem[$i]',0,null,'0',0,0,$v_categorypk[$k],'1', '0')"; 
			executeOCIUpdateQuery($v_query,$ds_conn);
		}
		
		$chkQ = "select count(*) as count from sch_bgtusers where FK_BUDGET=$v_budpk and BGTUSERS_TYPE='I'";
		$results = executeOCIQuery($chkQ,$ds_conn);		
		
		if($results["COUNT"][0]==0){
			$v_query = "insert into sch_bgtusers(PK_BGTUSERS,FK_BUDGET,FK_USER,BGTUSERS_RIGHTS,BGTUSERS_TYPE,CREATOR,CREATED_ON) values (
			seq_sch_bgtusers.nextval,$v_budpk,$v_creator,'777','I',$v_creator,sysdate)";
			executeOCIUpdateQuery($v_query,$ds_conn);		
		}	
	
		$v_query = "insert into sch_bgtusers(PK_BGTUSERS,FK_BUDGET,FK_USER,BGTUSERS_RIGHTS,BGTUSERS_TYPE,CREATOR,CREATED_ON)
		select seq_sch_bgtusers.nextval,$v_budpk,fk_user,'777','G',$v_creator,sysdate from (select fk_user from er_studyteam where 
		fk_study = $v_stdpk)";
		executeOCIUpdateQuery($v_query,$ds_conn);
		echo "<span style='color:green;'>Budget <b>".$v_bname."</b> imported successfully.</span><BR>";
		echo '<meta http-equiv="refresh" content="2; url=./budget_quick.php">';
	} else{
	  echo "<span style='color:red;'>Budget <b>".$v_bname."</b> already exists.</span><BR>";
	  //echo '<meta http-equiv="refresh" content="2; url=./budget_quick.php">';
	}
  } else{
     echo "There was an error uploading the file, please try again!"."<BR>";
  }
}

?>

	
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
