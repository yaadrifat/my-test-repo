<?php
session_start();
header("Cache-control: private");
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> New Datasource</title>
	<link href="css/enhancement.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript">
	
		function VerfiyValidation() {
			if(document.getElementById("dsname").value == ''){
				alert("DS Name should not be empty");
				document.getElementById("dsname").focus();
				return false;
			}
			if(document.getElementById("dshost").value == ''){
				alert("DS Host should not be empty");
				document.getElementById("dshost").focus();
				return false;			
			}
			if(document.getElementById("dsport").value == ''){
				alert("DS Port should not be empty");
				document.getElementById("dsport").focus();
				return false;
			} else {
				var numericExpression = /^[0-9]+$/;
				if(document.getElementById("dsport").value.match(numericExpression)){
					document.getElementById("dssid").focus();
				} else {
				alert("DS Port should be numeric");
					document.getElementById("dsport").focus();
					return false;
				}
			}
			if(document.getElementById("dssid").value == ''){
				alert("DS SID should not be empty");
				document.getElementById("dssid").focus();
				return false;
			}
			if(document.getElementById("pass1").value == ''){
				alert("DS Password should not be empty");
				document.getElementById("pass1").focus();
				return false;
			}
			if(document.getElementById("pass2").value == ''){
				alert("DS RePassword should not be empty");
				document.getElementById("pass2").focus();
				return false;
			}
			if(document.getElementById("pass1").value != document.getElementById("pass2").value){
				alert("DS Password and DS RePassword should be same");
				document.getElementById("pass1").focus();
				return false;
			}
		}
	</script>
<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");
require_once('audit_queries.php');
?>
</head>
<body>

<div id="fedora-content">	

<?PHP
if (isset($_GET["mode"])) $v_mode = $_GET["mode"];
if (isset($_POST["mode"])) $v_mode = $_POST["mode"];

if (!($_SERVER['REQUEST_METHOD'] == 'POST')) {
	if ($v_mode == 'n'){
		$v_dsname = "";
		$v_dshost = "";
		$v_dsport = "";
		$v_dssid = "";
	}elseif($v_mode == 'm'){
		$refNum = $_GET["pk_ds"];
                $rs = mysql_query("SELECT pk_ds, ds_name, ds_host, ds_port,ds_sid,ds_pass FROM et_ds where pk_ds=".$refNum);
		$rs_row = mysql_fetch_array($rs);
		$v_pkds = $rs_row["pk_ds"];
		$v_dsname = $rs_row["ds_name"];
		$v_dshost = $rs_row["ds_host"];
		$v_dsport = $rs_row["ds_port"];
		$v_dssid = $rs_row["ds_sid"];	
		$v_pass = $rs_row["ds_pass"];
		?>
		<script type="text/javascript">
		function VerfiyValidation() {
		if(document.getElementById("dsname").value == ''){
				alert("DS Name should not be empty");
				document.getElementById("dsname").focus();
				return false;
			}
			if(document.getElementById("dsport").value == ''){
				alert("DS Port should not be empty");
				document.getElementById("dsport").focus();
				return false;
			} else {
				var numericExpression = /^[0-9]+$/;
				if(document.getElementById("dsport").value.match(numericExpression)){
					document.getElementById("dssid").focus();
				} else {
				alert("DS Port should be numeric");
					document.getElementById("dsport").focus();
					return false;
				}
			}
			if(document.getElementById("dssid").value == ''){
				alert("DS SID should not be empty");
				document.getElementById("dssid").focus();
				return false;
			}
			if(document.getElementById("pass1").value == ''){
				alert("DS Password should not be empty");
				document.getElementById("pass1").focus();
				return false;
			}
			if(document.getElementById("pass2").value == ''){
				alert("DS RePassword should not be empty");
				document.getElementById("pass2").focus();
				return false;
			}
			if(document.getElementById("pass1").value != document.getElementById("pass2").value){
				alert("DS Password and DS RePassword should be same");
				document.getElementById("pass1").focus();
				return false;
			}
			if(document.getElementById("pass1").value != document.getElementById("v_pass").value){
				alert("Password is wrong, Please enter a valid DB password");
				document.getElementById("pass1").focus();
				return false;
			}
		}
		</script>
		<?php		
	}else{ //this is for change password key is 'c'
            $refNum = $_GET["pk_ds"];
            $rs = mysql_query("SELECT pk_ds, ds_name, ds_host, ds_port,ds_sid,ds_pass FROM et_ds where pk_ds=".$refNum);
            $rs_row = mysql_fetch_array($rs);		
            $v_pkds = $rs_row["pk_ds"];
            $v_dsname = $rs_row["ds_name"];
            $v_dshost = $rs_row["ds_host"];
            $v_dsport = $rs_row["ds_port"];
            $v_dssid = $rs_row["ds_sid"];	
            $v_pass = $rs_row["ds_pass"];                
	}
	
	$records = mysql_query("SELECT ds_rights FROM et_groups where pk_groups=".$_SESSION["FK_GROUPS"]);
	if(mysql_num_rows($records) >0){
            $tempVal = 0;
                while($records_row = mysql_fetch_array($records)){
                    $dsRights = explode("|",$records_row['ds_rights']);	
                    $tempVal++;
                }
        }
	for($di=0; $di<count($dsRights); $di++){
            list($pk_ds, $tmp)=explode(":",$dsRights[$di]);
            if($pk_ds==$v_pkds){
                $ds=$tmp;
            }
	}
        
	if ($v_mode == 'n'){
            echo '<div class="navigate">Datasource - New</div>';
	}elseif($v_mode == 'm') {
            echo '<div class="navigate">Datasource - Edit</div>';
	}else{
            echo '<div class="navigate">Datasource - Change Password</div>';	
	}
	
	// this is for change password
	if($v_mode=='c'){ ?>
		<script type="text/javascript">
		function VerfiyValidation() {
			if(document.getElementById("chp_1").value != document.getElementById("v_pass").value){
				alert("Password is wrong, Please enter a valid DB password");
				document.getElementById("chp_1").focus()
				return false;
			}
			if(document.getElementById("chp_2").value == ''){
				alert("DS Password should not be empty");
				document.getElementById("chp_2").focus();
				return false;
			}
			if(document.getElementById("chp_3").value == ''){
				alert("DS RePassword should not be empty");
				document.getElementById("chp_3").focus();
				return false;
			}
			if(document.getElementById("chp_2").value != document.getElementById("chp_3").value){
				alert("DS Password and DS RePassword should be same");
				document.getElementById("chp_2").focus();
				return false;
			}
		}
		</script>
		<form id="changepass" name="edituser" action="datasource_edit.php" method="POST" onSubmit="return VerfiyValidation();">
			<input type='hidden' name='mode' value=<?PHP echo $v_mode; ?>>
			<input type='hidden' name='pk_ds' value=<?PHP echo $refNum; ?>>
			<input type='hidden' name='v_pass' value=<?PHP echo $v_pass; ?>>
			<BR><table width="700">
			<tr>			
			  <td width="122">DS Name: </td>
			  <td width="555" colspan="2"><input class="textdisblae" id="dsname" type = "text" size="40" maxlength="25" name="dsname" value="<?PHP echo trim($v_dsname); ?>" disabled="disabled"></td></tr>
			<tr>
			  <td>DS Host : </td>
			  <td colspan="2"><input class="textdisblae" type = "text" size="40" maxlength="15" name="dshost" value="<?PHP echo trim($v_dshost); ?>"  disabled="disabled"></td></tr>
			<tr>
			  <td>DS Port : </td>
			  <td colspan="2"><input class="textdisblae" type = "text" size="40" name="dsport" value="<?PHP echo trim($v_dsport); ?>"  disabled="disabled"></td></tr>
			<tr>
			  <td>DS SID : </td>
			  <td colspan="2"><input class="textdisblae" type = "text" size="40" name="dssid" value="<?PHP echo trim($v_dssid); ?>"  disabled="disabled"></td></tr>
			
			<tr><?php 
				if($ds==0){?>
					  <td>DS Status : </td>
					  <td colspan="2">
					  <input class="textdisblae" name="dsstat" type="text" value="Disabled" disabled="disabled">
					  </td>
				<?php }else{ ?>
					  <td>DS Status : </td>
					  <td colspan="2">
					  <input class="textdisblae" name="dsstat" type="text" value="Enabled" disabled="disabled"></td>										
				<?php }
			?>
			  </tr>
			<tr>
			  <td>DS Existing Password : </td>
			  <td colspan="2"><input name="chp_1" type="password" size="30" <?php echo (($v_mode == 'c') ? " class = 'required'" : "")?>type="password" class="required"/></td>
			</tr>

			<tr>
			  <td>DS New Password: </td>
			  <td colspan="2"><input name="chp_2" type="password" size="30" <?php echo (($v_mode == 'c') ? " class = 'required'" : "")?>type="password" class="required"/></td>
			</tr>

			<tr>
			  <td>DS Reenter Password: </td>
			  <td colspan="2"><input name="chp_3" type="password" size="30" <?php echo (($v_mode == 'c') ? " class = 'required'" : "")?>type="password" class="required"/></td>
			</tr>			
			<tr>
			  <td>&nbsp;</td>
			  <td colspan="2"><?php if(@$_GET["blank"]) echo "<font size=2 color=red>Fields cannot be empty</font>"; ?><?php if(@$_GET["pass"]) echo "<font size=2 color=red>Password mismatch</font>"; ?><?php if(@$_GET["mm"]) echo "<font size=2 color=red>Given Password is not matching with existing password</font>"; ?></td>
			  </tr>
			</table>
				<input type="image" src="./img/submit.png" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';"></form>
<?php	}else{
	?>
		<form id="edituser" name="edituser" action="datasource_edit.php" method="POST" onSubmit="return VerfiyValidation();">
			<input type='hidden' name='mode' value=<?PHP echo $v_mode; ?>>
			<input type='hidden' name='pk_ds' value=<?PHP echo $refNum; ?>>
			<input type='hidden' name='v_pass' value=<?PHP echo $v_pass; ?>>
			<BR><table width="700">

			<tr>
			  <td width="122">DS Name: </td>
			  <td width="555" colspan="2"><input name="dsname" type = "text" id="dsname" value="<?PHP echo $v_dsname; ?>" size="25" maxlength="25">
			  <span style="font-size:11px; font-style:italic; color:#999999;"> (Maximum Length: 25 Characters)</span></td></tr>
			<tr>
			  <td>DS Host : </td>
			  <td colspan="2"><input type = "text" size="40" maxlength="15" name="dshost" id="dshost" value="<?PHP echo $v_dshost; ?>"></td></tr>
			<tr>
			  <td>DS Port : </td>
			  <td colspan="2"><input type = "text" size="40" name="dsport" id="dsport" value="<?PHP echo $v_dsport; ?>"></td></tr>
			<tr>
			  <td>DS SID : </td>
			  <td colspan="2"><input type = "text" size="40" name="dssid" id="dssid" value="<?PHP echo $v_dssid; ?>"></td></tr>
			
			<tr><?php 
                            if($v_mode!='n'){
				if($ds==0){?>
					  <td>DS Status : </td>
					  <td colspan="2">
						<input  name="dsstat" type="radio" value="1">
					  Enable 
					  <input name="dsstat" type="radio" value="0" checked="checked">
					  Disable</td>					
				<?php }else{ ?>
					  <td>DS Status : </td>
					  <td colspan="2">
						<input name="dsstat" type="radio" value="1" checked="checked">
					  Enable 
					  <input name="dsstat" type="radio" value="0">
					  Disable</td>					
					
				<?php }
                            }else{ 
                                ?>
                                <td>DS Status : </td>
                                <td colspan="2">
                                      <input  name="dsstat" type="radio" value="1" checked="checked">
                                Enable 
                                <input name="dsstat" type="radio" value="0" >
                                Disable</td>					
                                
                            <?php }
			?>
			  </tr>
			<tr>
			  <td>DS Password : </td>
			  <td colspan="2"><input name="pass1" type="password" id="pass1" size="20" <?php echo (($v_mode == 'n') ? " class = 'required'" : "")?>type="password" class="required"/></td>
			</tr>

			<tr>
			  <td>DS Reenter Password: </td>
			  <td colspan="2"><input name="pass2" type="password" id="pass2" size="20" <?php echo (($v_mode == 'n') ? " class = 'required'" : "")?>type="password" class="required"/></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td colspan="2"><?php if(@$_GET["blank"]) echo "<font size=2 color=red>Fields cannot be empty</font>"; ?><?php if(@$_GET["pass"]) echo "<font size=2 color=red>Password mismatch</font>"; ?><?php if(@$_GET["mm"]) echo "<font size=2 color=red>Given Password is not matching with existing password</font>"; ?><?php if(@$_GET["pempty"]) echo "<font size=2 color=red> Password fields cannot be left blank</font>"; ?></td>
			  </tr>
			</table>
				<input type="image" src="./img/submit.png" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';">
				</form>
				
		
	<?PHP
	}
} else {
	$v_pass = $_POST["pass1"];
	$v_pass2 = $_POST["pass2"];
	$v_mode = $_POST["mode"];

	$v_ch_pe = $_POST['chp_1'];
	$v_ch_pn = $_POST['chp_2'];
	$v_ch_pr = $_POST['chp_3'];	
		
	$v_pkds = $_POST["pk_ds"];
	$v_dsname = $_POST["dsname"];
	$v_dshost = $_POST["dshost"];
	$v_dsport = $_POST["dsport"];
	$v_dssid = $_POST["dssid"];
	$v_dssta = $_POST["dsstat"];
	
	/*--- AUDIT ---*/
	$grpName = mysql_query("SELECT group_name from et_groups WHERE pk_groups =".$_SESSION["FK_GROUPS"]);	
	$grpNameVal = mysql_fetch_array($grpName);        
	/*--- END ---*/
        
	switch($v_mode){
		case "n":
                    if($v_dsname=="" || $v_dshost=="" || $v_dsport=="" || $v_dssid=="" || $v_pass=="" || $v_pass2==""){
                        header("location:datasource_edit.php?mode=n&blank=1");	
                    }else{
                        if($v_pass == $v_pass2){
                            $rightsResult = mysql_query("SELECT ds_rights from et_groups where pk_groups=1");
                            $rightsResult_row = mysql_fetch_array($rightsResult);
                            $maxNum = mysql_query("SELECT MAX(pk_ds) as pk_ds from et_ds");                            
                            $maxNum_row = mysql_fetch_array($maxNum);
							
                            if($maxNum_row["pk_ds"] == NULL){
                                $maxNum = $maxNum_row["pk_ds"]+1;
                                $dsright = "|".$maxNum.":".$v_dssta;
                                $result = mysql_query("SELECT ds_name from et_ds where ds_name='".$v_dsname."'");
                                if (mysql_num_rows($result) > 0) {							
                                    echo "Datasource is already exists";
                                    echo '<meta http-equiv="refresh" content="3; url=./datasource_dash.php">';							
                                }else{						
                                    $rs = mysql_query("INSERT INTO et_ds (ds_name,ds_host,ds_port,ds_sid,ds_pass) VALUES ('".$v_dsname."','".$v_dshost."','".$v_dsport."','".$v_dssid."','".$v_pass."')");
                                    $dsrights = mysql_query("UPDATE et_groups set ds_rights ='".$dsright."' where pk_groups = 1");
                                    echo "Data Saved.";
                                    echo '<meta http-equiv="refresh" content="0; url=./datasource.php">';
                                }
                            }else{
                                $etdsnextval = mysql_query("SELECT AUTO_INCREMENT as ac FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'etoolsdb' AND TABLE_NAME ='et_ds';");
                                $etdsnextval_rs = mysql_fetch_array($etdsnextval);

                                $adminGRP = mysql_query("SELECT ds_rights from et_groups where pk_groups = 1");
                                $adminGRP_rs = mysql_fetch_array($adminGRP);
                                $adminGRP_ds = $adminGRP_rs["ds_rights"]."|".$etdsnextval_rs["ac"].":".$v_dssta;
								
                                $result = mysql_query("SELECT ds_name from et_ds where ds_name='".$v_dsname."'");				
                                if (mysql_num_rows($result) > 0) {
                                    echo "Datasource is already exists";
                                    echo '<meta http-equiv="refresh" content="3; url=./datasource_dash.php">';
                                }else{						
                                    $rs = mysql_query("INSERT INTO et_ds (ds_name,ds_host,ds_port,ds_sid,ds_pass) VALUES ('".$v_dsname."','".$v_dshost."','".$v_dsport."','".$v_dssid."','".$v_pass."')");

                                    /*--- AUDIT ---*/								
                                    $rowInfo = array(2,$_SESSION["user"],$grpNameVal['group_name'], "SYSDATE", "SYSDATE", "I");
                                    auditQuery(2,$rowInfo);

                                    $colArray = array('DS_NAME', 'DS_HOST', 'DS_PORT', 'DS_SID');
                                    $colvalarray = array($v_dsname,$v_dshost,$v_dsport,$v_dssid);
                                    $tblname = 'et_ds';
                                    colQueries($colArray, $colvalarray, $tblname);
                                    /*--------------*/
                                    
                                    if (substr($adminGRP_ds,0,1)!="|"){
                                        $adminGRP_ds = "|".$adminGRP_ds;			
                                    }
                                    $adminUpdate = mysql_query("UPDATE et_groups set ds_rights ='".$adminGRP_ds."' where fk_parentgroup IS NULL ");

                                    /*--- AUDIT ---*/
                                    $rowInfo = array(2,$_SESSION["user"],$grpNameVal['group_name'], "SYSDATE", "SYSDATE", "U");
                                    auditQuery(2,$rowInfo);
                                    
                                    //$colArray = array('ET_GROUPS');
                                    $colArray = array('DS_RIGHTS');
                                    $colvalarray = array($adminGRP_ds);
                                    $tblname = 'et_groups';
                                    colQueries($colArray, $colvalarray, $tblname);
                                    /*------------*/
                                    
                                    $otrList = $_SESSION["FK_GROUPS"];
                                    $nullVar = '';
                                    $otrPKlist = '';
                                    while($nullVar != 'NULL'){
                                        $otrParent = mysql_query("SELECT fk_parentgroup from et_groups where pk_groups = ".$otrList);
                                        $otrParent_rs = mysql_fetch_array($otrParent);
                                        if($otrParent_rs["fk_parentgroup"] != NULL){                                  
                                            if($otrParent_rs["fk_parentgroup"] != 1){
                                                if($otrPKlist == ''){
                                                    $otrPKlist = $otrList.','.$otrParent_rs["fk_parentgroup"];
                                                }else{                                                
                                                    $otrPKlist = $otrPKlist.','.$otrParent_rs["fk_parentgroup"];
                                                }                                        
                                            }else{
                                                $currentGRP = mysql_query("SELECT ds_rights from et_groups where pk_groups=".$otrList);
                                                $currentGRP_rs = mysql_fetch_array($currentGRP);
                                                $currentGRP_ds = $currentGRP_rs["ds_rights"]."|".$etdsnextval_rs["ac"].":".$v_dssta;

                                                if(substr($currentGRP_ds,0,1)!="|"){
                                                    $currentGRP_ds = "|".$currentGRP_ds;			
                                                }                                                
                                                $currentGRP_qry = mysql_query("UPDATE et_groups set ds_rights ='".$currentGRP_ds."' where pk_groups=".$otrList);

                                                    /*--- AUDIT ---*/
                                                    $rowInfo = array(2,$_SESSION["user"],$grpNameVal['group_name'], "SYSDATE", "SYSDATE", "U");
                                                    auditQuery(2,$rowInfo);

                                                    //$colArray = array('ET_GROUPS');
                                                    $colArray = array('DS_RIGHTS');
                                                    $colvalarray = array($currentGRP_ds);
                                                    $tblname = 'et_groups';
                                                    colQueries($colArray, $colvalarray, $tblname);
                                                    /*------------*/
                                                
                                            }
                                            $otrList = $otrParent_rs["pk_groups"];
                                        }else{
                                            $nullVar = 'NULL';
                                        }
                                    }
                                    $otrPKlistarr = explode(",",$otrPKlist);
                                    
                                    for($i=0; $i<sizeof($otrPKlistarr); $i++){
                                        $currentGRP = mysql_query("SELECT ds_rights from et_groups where pk_groups=".$otrPKlistarr[$i]);
                                        $currentGRP_rs = mysql_fetch_array($currentGRP);
                                        $currentGRP_ds = $currentGRP_rs["ds_rights"]."|".$etdsnextval_rs["ac"].":".$v_dssta;
                                        $currentGRP_qry = mysql_query("UPDATE et_groups set ds_rights ='".$currentGRP_ds."' where pk_groups=".$otrPKlistarr[$i]);
                                    }
                                    /*--- END ---*/
                                    echo "Data Saved.";
                                    echo '<meta http-equiv="refresh" content="0; url=./datasource_dash.php">';
                                }	
                            }				
                        }else{
                            header("location:datasource_edit.php?mode=n&pass=1&pk_ds=".$v_pkds);
                        }		
                    }										
		break;
		
		case "m":
			if($v_dsname=="" || $v_dshost=="" || $v_dsport=="" || $v_dssid=="" || $v_pass=="" || $v_pass2==""){
				header("location:datasource_edit.php?mode=m&blank=1&pk_ds=".$v_pkds);				
			}else{
				if ($v_pass == $v_pass2){ //password confirmation
                                        $rs = mysql_query("SELECT ds_pass from et_ds where pk_ds='$v_pkds'");
                                        $dspass_row = mysql_fetch_array($rs);
					if($dspass_row["ds_pass"] == $v_pass){ //password check with db
						/*--- AUDIT ---*/
						$diffArray[] ='';
						$diffArrayname[] ='';
                                                $cnt = 0;

                                                if($v_dsname != $dspass['ds_name']){								
                                                    $diffArray[$cnt] = $v_dsname;
                                                    $diffArrayname[$cnt] = "ds_name";
                                                    $cnt++;
                                                }
                                                if($v_dshost != $dspass['ds_host']){								
                                                    $diffArray[$cnt] = $v_dshost;
                                                    $diffArrayname[$cnt] = "ds_host";
                                                    $cnt++;
                                                }
                                                if($v_dsport != $dspass['ds_port']){							
                                                    $diffArray[$cnt] = $v_dsport;
                                                    $diffArrayname[$cnt] = "ds_port";
                                                    $cnt++;
                                                }
                                                if($v_dssid != $dspass['ds_sid']){								
                                                    $diffArray[$cnt] = $v_dssid;
                                                    $diffArrayname[$cnt] = "ds_sid";
                                                    $cnt++;
                                                }
						
						$oldvaQuery = "SELECT ";
						for($oq=0; $oq<=sizeof($diffArrayname); $oq++){
							$oldvaQuery .= $diffArrayname[$oq]." ,";
						}
						$oldvaQuery = substr($oldvaQuery, 0, strlen(trim($oldvaQuery))-3);                                                
						$oldvaQuery .= "from et_ds where pk_ds='$v_pkds'";
						$oldValueRsr = mysql_query($oldvaQuery);
						$oldValueRs = mysql_fetch_array($oldValueRsr);
						/*--- END ---*/
						
						$rs = mysql_query("UPDATE et_ds set ds_pass = '".$v_pass."' where pk_ds = ".$v_pkds);
						$rs = mysql_query("UPDATE et_ds set ds_name = '".$v_dsname."',ds_host = '".$v_dshost."',ds_port = '".$v_dsport."',ds_sid = '".$v_dssid."' where pk_ds = ".$v_pkds);

						/*--- AUDIT ---*/
						$rowInfo = array(2,$_SESSION["user"],$grpNameVal['group_name'], "SYSDATE", "SYSDATE", "U");
						auditQuery(2,$rowInfo);

						$tblname = 'et_ds';
						colQueries(array_diff($diffArrayname, array('')), array_diff($diffArray, array('')), $tblname, array_diff($oldValueRs, array('')));
						/*--- END ---*/

                                                
                                                $noDs = mysql_query("SELECT pk_groups from et_groups");
						if(mysql_num_rows($noDs) >0){
							while($noDs_row = mysql_fetch_array($noDs)){
                                                                $dsrs = mysql_query("SELECT ds_rights FROM et_groups where pk_groups=".$noDs_row["pk_groups"]);
								if(mysql_num_rows($dsrs)>0){
									while($dsrs_row = mysql_fetch_array($dsrs)){
										$dsRi = explode("|",$dsrs_row['ds_rights']);
									}
								}
								$finalDs = "";
                                                               
								for($i=0; $i<count($dsRi)+1; $i++){
									$tmpStr = explode(":",$dsRi[$i]);
									if($v_pkds==$tmpStr[0]){
										$finalDs.=$v_pkds.":".$v_dssta."|";                                                                                
									}else if($tmpStr[0]>0){
										$finalDs.=$dsRi[$i]."|";
									}
								}			
								
								if (substr($finalDs,strlen($finalDs)-1,strlen($finalDs))=="|"){
									$finalDs = substr($finalDs,0,strlen($finalDs)-1);			
								}
								/*--- AUDIT ---*/
                                                                    //$oldValue = $db->executeQuery("SELECT DS_RIGHTS FROM ET_GROUPS where PK_GROUPS=".$noData["PK_GROUPS"]);
                                                                    //$oldValue->next();
                                                                    //$oldValueRs = $oldValue->getCurrentValuesAsHash();
                                                                    $oldValue = mysql_query("SELECT ds_rights FROM et_groups where pk_groups=".$noDs_row["pk_groups"]);                                                                    
                                                                    $oldValueRs = mysql_fetch_array($oldValue);

                                                                    $finalDs = str_replace("||","|",$finalDs);			
                                                                    
                                                                    if(substr($finalDs,0,1)!="|"){
                                                                        $finalDs = "|".$finalDs;
                                                                    }
                                                                    //$rs = $db->executeQuery("UPDATE ET_GROUPS set DS_RIGHTS ='".$finalDs."' where PK_GROUPS=".$noData["PK_GROUPS"]);
                                                                    $rs = mysql_query("UPDATE et_groups set ds_rights ='".$finalDs."' where pk_groups=".$noDs_row["pk_groups"]);
                                                                    
                                                                    $diffName = array('ds_rights');
                                                                    $diffValue = array($finalDs);
                                                                    $tblname = 'et_groups';
                                                                    colQueries(array_diff($diffName, array('')), array_diff($diffValue, array('')), $tblname, array_diff($oldValueRs, array('')));
								/*--- END ---*/
							}							
						}
						echo "Data Saved.";
						echo '<meta http-equiv="refresh" content="0; url=./datasource_dash.php">';
					}else{
						echo "<meta http-equiv='refresh' content='0; url=./datasource_edit.php?mode=m&pk_ds=".$v_pkds."'>";						
					}
				}else{
					header("location:datasource_edit.php?mode=m&pass=1&pk_ds=".$v_pkds);
				}
			}
		break;
		
		case "c":
			if ($v_ch_pe=="" || $v_ch_pn=="" || $v_ch_pr==""){
				header("location:datasource_edit.php?mode=c&blank=1&pk_ds=".$v_pkds);				
			}else{
				if($v_ch_pn!=$v_ch_pr){
					header("location:datasource_edit.php?mode=c&pass=1&pk_ds=".$v_pkds);
				}else{
                                        $passQ = mysql_query("SELECT ds_pass from et_ds where pk_ds='$v_pkds'");
                                        $passrs_row = mysql_fetch_array($passQ);				
					if($passrs_row["ds_pass"]!=$v_ch_pe){
						header("location:datasource_edit.php?mode=c&mm=1&pk_ds=".$v_pkds);
					}else{
						
						/*--- AUDIT ---*/
						//$oldValue = $db->executeQuery("SELECT DS_PASS FROM ET_DS where PK_DS=".$v_pkds);
						//$oldValue->next();
						//$oldValueRs = $oldValue->getCurrentValuesAsHash();								
						$oldValue = mysql_query("SELECT ds_pass FROM et_ds where pk_ds=".$v_pkds);						
						$oldValueRs = mysql_fetch_array($oldValue);
                                                
						/*--- END ---*/
						
                                                $newpass = mysql_query("UPDATE et_ds set ds_pass ='".$v_ch_pn."' where pk_ds=".$v_pkds);						
						
						/*--- AUDIT ---*/
						$rowInfo = array(2,$_SESSION["user"],$grpNameVal['group_name'], "SYSDATE", "SYSDATE", "U");
						auditQuery(2,$rowInfo);
						
						$diffName = array('DS_PASS');
						$diffValue = array($v_ch_pn);
						$tblname = 'et_ds';
						colQueries(array_diff($diffName, array('')), array_diff($diffValue, array('')), $tblname, array_diff($oldValueRs, array('')));
						/*--- END ---*/						
						
						echo "Data Saved.";
						echo '<meta http-equiv="refresh" content="0; url=./datasource_dash.php">';					
					}
				}
			}	
		break;
	}	
}
?>
</div>

</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
