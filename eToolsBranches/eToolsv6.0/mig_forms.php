<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Migrate Form Data</title>
	
	
      <script language="javascript" type="text/javascript">
      function getHTTPObject(){
      if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
      else if (window.XMLHttpRequest) return new XMLHttpRequest();
      else {
      alert("Your browser does not support AJAX.");
      return null;
      }
      }

      function startImport(adaptor,adapmod){
document.getElementById('importdata').innerHTML = '<font color="#d1cebf">Importing...</font>';
      alert("Click OK to start import");
	  httpObject = getHTTPObject();
      if (httpObject != null) {
      httpObject.open("GET", "mig_formflds_migrate_step2.php?pk_vlnk_adaptor="+adaptor+"&pk_vlnk_adapmod="+adapmod, false);
	  httpObject.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
	  httpObject.setRequestHeader("Cache-Control", "no-cache");
      httpObject.send(null);
	  if(httpObject.status != 200){document.write("404 not found");}
		var finalString = httpObject.responseText;
alert("Data imported into staging");
window.location="mig_forms.php?pk_vlnk_adaptor="+adaptor;
			document.getElementById('importdata').innerHTML = '<A HREF="javascript:void(0)" onClick="startImport('+adaptor+','+adapmod+')">Import</a>';
		}

      }
      
      


      var httpObject = null;
      </script>

	  
<script>
function formPreview(formPk){
	var win = "form_preview.php?formPk="+formPk;
	window.open(win,'mywin',"toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=825,height=600,top=90,left=150");
}


</script>
<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

$pk_vlnk_adaptor = $_GET["pk_vlnk_adaptor"];

$v_flag = $_GET["flag"];
?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Migrate Data - Forms</div>
<?php
echo "<a href=migform_new.php?pk_vlnk_adaptor=".$pk_vlnk_adaptor.">New migration</a>";
?>
<Table border="1" width="100%"><TR>
<TH>NAME</TH>
<TH>INFO</TH>
<TH>FORM NAME</TH>
<TH>TABLE NAME</TH>
<TH>MAP FIELDS</TH>
<TH>MAP RESPONSES</TH>
<TH>STAGING AREA</TH>
<TH>IMPORT INTO APPLICATION</TH>
<TH></TH>
</TR>
<?php

$query_sql = "SELECT pk_vlnk_adapmod,fk_form,table_name,lf_displaytype,adapmod_name,
decode(trim(lf_displaytype),'C','C',
'S','S',
'SP','P',
'PS','P',
'PR','P',
'A','A',
'SA','S',
'PA','P') as form_linkto,
(select count(*) from velink.vlnk_log where status = -1 and fk_vlnk_adapmod = pk_vlnk_adapmod) as err_count,
(select count(*)  from velink.vlnk_imp_formfld where fk_vlnk_adapmod = pk_vlnk_adapmod) as map_count,
(select count(*)  from velink.vlnk_imp_formfld where fk_vlnk_adapmod = pk_vlnk_adapmod and fld_mandatory = 1 and table_colname is null) as map_count_man,
(select count(*) from velink.vlnk_imp_formresp where fldresp is null and exists (select *  from velink.vlnk_imp_formfld where fk_vlnk_adapmod = pk_vlnk_adapmod and pk_imp_formfld = fk_imp_formfld)) as map_count_resp,
(select count(*) from velink.vlnk_imp_formresp where exists (select *  from velink.vlnk_imp_formfld where fk_vlnk_adapmod = pk_vlnk_adapmod and pk_imp_formfld = fk_imp_formfld)) as map_count_resp_exists,
pk_formlib,form_name, form_desc,decode(trim(lf_displaytype),'C','CRF','S','Study','SP','Patient (Specific Study)','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','SA','All Studies','PA','All Patients') as linked_to,DECODE(fk_study,NULL,'',(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study)) AS study_number,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = form_status) as status, er_formlib.record_type as record_type 
FROM velink.VLNK_ADAPMOD, ER_FORMLIB, ER_LINKEDFORMS 
WHERE  pk_formlib = fk_form AND pk_formlib = fk_formlib AND fk_form IS NOT NULL AND fk_vlnk_adaptor = ".$pk_vlnk_adaptor." order by 1 desc"; 
$results = executeOCIQuery($query_sql,$ds_conn);
$total_rows = $results_nrows;
for ($rec = 0; $rec < $total_rows; $rec++){
$results1 = executeOCIQuery("select count(*) as count from dba_tables where owner = 'ERES' and table_name = '".strtoupper($results["TABLE_NAME"][$rec])."'",$ds_conn);
?>
	<TR onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
	<TD><?php echo $results["ADAPMOD_NAME"][$rec] . "&nbsp;"; ?></TD>
	<TD><a href="#" class="tip"><img src="./img/info_1.png">
	<span>
	<table class="ttip" width="100%">
	<tr><td width="25%" valign="top"><b>Primary Key</b></td><td width="75%"><?PHP echo $results["PK_FORMLIB"][$rec]; ?></td><tr>
	<tr><td valign="top"><b>Form Name</b></td><td><?PHP echo $results["FORM_NAME"][$rec]; ?></td><tr>
	<tr><td valign="top"><b>Description</b></td><td><?PHP echo $results["FORM_DESC"][$rec]; ?></td><tr>
	<tr><td valign="top"><b>Status</b></td><td><?PHP echo $results["STATUS"][$rec]; ?></td><tr>
	<tr><td valign="top"><b>Linked to</b></td><td><?PHP echo $results["LINKED_TO"][$rec]; ?></td><tr>
	<tr><td valign="top"><b>Study Number</b></td><td><?PHP echo $results["STUDY_NUMBER"][$rec]; ?></td><tr>
	</table>
	</span></a></TD>

	<TD>
	<?php 
	if ($results["RECORD_TYPE"][$rec] == 'D') {
		echo '<a style="color:red" href=# onclick="formPreview('.$results["FK_FORM"][$rec].')">'.$results["FORM_NAME"][$rec] . '</a>'; 
	} else {
		echo '<a href=# onclick="formPreview('.$results["FK_FORM"][$rec].')">'.$results["FORM_NAME"][$rec] . '</a>'; 
	}
	?></TD>
	<?php 
	

	if ($results1["COUNT"][0] == 0) {
		echo "<TD>".$results["TABLE_NAME"][$rec]."</TD>"; 
	} else {
		$results1 = executeOCIQuery('select count(*) as count from '.$results["TABLE_NAME"][$rec],$ds_conn);
		echo "<TD><a href='mig_table_view.php?table=".$results["TABLE_NAME"][$rec]."&pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."'>".$results["TABLE_NAME"][$rec].' ('.(($results1["COUNT"][0])? $results1["COUNT"][0]:"").")</a></TD>"; 
	}
	echo "<td>";

	$v_fieldMapDefined = true;
	if ($results["MAP_COUNT"][$rec] == 0 && $v_flag == '') {
		echo "<img src='./img/red.png' />";
		$v_fieldMapDefined = false;
	} elseif ($results["MAP_COUNT_MAN"][$rec] > 0) {
		echo "<img src='./img/yellow.png' />";
	} else {
		echo "<img src='./img/green.png' />";
	}

	if ($results["MAP_COUNT"][$rec] == 0) {
		echo "<a href=mig_forms_mapflds.php?pk_vlnk_adaptor=".$pk_vlnk_adaptor."&pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."&tablename=".$results["TABLE_NAME"][$rec]."&pk_formlib=".$results["FK_FORM"][$rec]."&formname=".urlencode($results["FORM_NAME"][$rec])."&linkedto=".$results["LF_DISPLAYTYPE"][$rec].">Edit</a>";
	} else {
		echo "<a href=mig_forms_mapflds_edit.php?pk_vlnk_adaptor=".$pk_vlnk_adaptor."&pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."&tablename=".$results["TABLE_NAME"][$rec]."&pk_formlib=".$results["FK_FORM"][$rec]."&formname=".urlencode($results["FORM_NAME"][$rec])."&linkedto=".$results["LF_DISPLAYTYPE"][$rec].">Edit</a>";
	}
	echo "</td>";
	
	
//	echo "<td><a href=mig_forms_mapflds_edit.php?pk_vlnk_adaptor=".$pk_vlnk_adaptor."&pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."&tablename=".$results["TABLE_NAME"][$rec]."&pk_formlib=".$results["FK_FORM"][$rec]."&formname=".urlencode($results["FORM_NAME"][$rec])."&linkedto=".$results["LF_DISPLAYTYPE"][$rec].">Edit Mapping</a></td>";

	echo "<td>";
	if ($v_fieldMapDefined) {
		if ($results["MAP_COUNT_RESP"][$rec] == 0 && $results["MAP_COUNT_RESP_EXISTS"][$rec] > 0) {
			echo "<img src='./img/green.png' />";
		} else {
			echo "<img src='./img/yellow.png' />";
		} 
		echo "<a href=mig_forms_mapfldresp.php?pk_vlnk_adaptor=".$pk_vlnk_adaptor."&pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."&tablename=".$results["TABLE_NAME"][$rec]."&pk_formlib=".$results["FK_FORM"][$rec]."&formname=".urlencode($results["FORM_NAME"][$rec])."&linkedto=".$results["LF_DISPLAYTYPE"][$rec].">Edit</a>";
	} else {
		echo "<img src='./img/red.png' />";
		echo "<font color='#d1cebf'>Edit</font>";
	}
	echo "</td>";
	
//	echo "<td><a href=mig_formflds_valdate.php?pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec].">Validate Date</a></td>";


	echo "<td>";
	if ($v_fieldMapDefined) {
		switch ($results["FORM_LINKTO"][$rec]) {
		case "A":
			$v_table = "ER_IMPACCTFORM";
			break;
		case "P":
			$v_table = "ER_IMPPATFORM";
			break;
		case "S":
			$v_table = "ER_IMPSTUDYFORM";
			break;
		}
		$v_sql = "select count(*) as count from $v_table where fk_form = ".$results["PK_FORMLIB"][$rec]." and custom_col = '".$results["PK_VLNK_ADAPMOD"][$rec]."'";
		$results1 = executeOCIQuery($v_sql,$ds_conn);

		if ($results1["COUNT"][0] == 0) {
			echo "<img src='./img/yellow.png' />";
		} else {
			echo "<img src='./img/green.png' />";
		} 
		echo "<a href=mig_formflds_migrate.php?pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."&flag=".$_REQUEST["flag"].""."&pk_vlnk_adaptor=".$pk_vlnk_adaptor."&rows=".$results1["COUNT"][0]."&form_linkto=".$results["FORM_LINKTO"][$rec]."&mode=import>Import</a>";

//		echo '<INPUT TYPE="BUTTON" NAME="STARTIMPORT" VALUE="Import" onClick="startImport('.$pk_vlnk_adaptor.','.$results["PK_VLNK_ADAPMOD"][$rec].')"/>';
//echo '<div id="importdata">';
//		echo '<A HREF="javascript:void(0)" onClick="startImport('.$pk_vlnk_adaptor.','.$results["PK_VLNK_ADAPMOD"][$rec].')">Import</a>';
//echo '</div>';
 

		echo "&nbsp;&nbsp;&nbsp;&nbsp;".($results1["COUNT"][0] == 0?"&nbsp;&nbsp;&nbsp;&nbsp;<font color='#d1cebf'>View</font>":"<a href=mig_form_view.php?pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."&form_linkto=".$results["FORM_LINKTO"][$rec]."&fk_form=".$results["PK_FORMLIB"][$rec].">View (".$results1["COUNT"][0].")</a>");
		echo "&nbsp;&nbsp;&nbsp;&nbsp;".($results1["COUNT"][0] == 0?"&nbsp;&nbsp;&nbsp;&nbsp;<font color='#d1cebf'>Empty</font>":"<a href=mig_formflds_migrate.php?pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."&pk_vlnk_adaptor=".$pk_vlnk_adaptor."&rows=".$results1["COUNT"][0]."&form_linkto=".$results["FORM_LINKTO"][$rec]."&mode=empty>Empty</a>");
		if ($results["ERR_COUNT"][$rec] > 0) {
			$v_sql = "select a.message as ERROR_MESSAGE,b.* from velink.vlnk_log a,".$results["TABLE_NAME"][$rec]." b where status = -1 and b.rowid = a.rid and fk_vlnk_adapmod = ".$results["PK_VLNK_ADAPMOD"][$rec];
			echo "&nbsp;&nbsp;&nbsp;&nbsp;<a style='color:red' href=mig_table_view.php?pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."&sql=".urlencode($v_sql).">Error (".$results["ERR_COUNT"][$rec].")</a>";
		}
	} else {
		echo "<img src='./img/red.png' />";
		echo "<font color='#d1cebf'>Import";
		echo "&nbsp;&nbsp;&nbsp;&nbsp;View";
		echo "&nbsp;&nbsp;&nbsp;&nbsp;Empty</font>";
	}
	echo "</td>";


	if ($results1["COUNT"][0] == 0) {
		echo "<td><font color='#d1cebf'>Import&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbspView log</font></td>";
	} else {
		echo "<td><a href=mig_forms_migrate.php?pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."&form=".$results["FK_FORM"][$rec]."&form_linkto=".$results["FORM_LINKTO"][$rec]."&pk_vlnk_adaptor=$pk_vlnk_adaptor>Import</a>";
		echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=mig_form_log.php?pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."&linkedto=".$results["FORM_LINKTO"][$rec]."&fk_form=".$results["PK_FORMLIB"][$rec].">View log</a></td>";
	} 

	echo "<td><a href=mig_form_validate.php?pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."&form_linkto=".$results["FORM_LINKTO"][$rec]."&tablename=".$results["TABLE_NAME"][$rec]."&pk_vlnk_adaptor=".$pk_vlnk_adaptor.">Validate</a></td>";
	echo "</TR>";
}
?>
</TABLE>
<BR><BR>
<Table class="ttip" border="0" width="100%"><TR>
<TH>MAP FIELDS</TH>
<TH>MAP RESPONSES</TH>
<TH>STAGING AREA</TH>
<TH>IMPORT INTO APPLICATION</TH>
</TR>
<tr>
<td>
<img src='./img/yellow.png' /> - Mandatory Fields not mapped
<br><img src='./img/green.png' /> - All Mandatory Fields  mapped
</td>
<td>
<img src='./img/yellow.png' /> - Not all responses mapped
<br><img src='./img/green.png' /> - Response mapping exists
</td>
<td>
<img src='./img/yellow.png' /> - No data in staging area
<br><img src='./img/green.png' /> - Data exists in staging area
</td>
<td>
<img src='./img/yellow.png' /> - Error migrating some records
<br><img src='./img/green.png' /> - Data migrated (check log)
</td>
</tr>
</table>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
