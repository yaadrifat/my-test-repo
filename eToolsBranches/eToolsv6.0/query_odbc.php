<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Import ODBC DB</title>

<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Import ODBC DB</div>
<br>
	
<a href="query_odbc_new.php">Add ODBC DB Connection Profile</a>
<Table border="1"><TR>
<TH>NAME</TH>
<TH>DSN</TH>
<TH>&nbsp;</TH>
</TR>
<?php

$query_sql = "SELECT pk_vlnk_odbc, odbc_name, odbc_dsn from velink.vlnk_odbc";

$results = executeOCIQuery($query_sql,$ds_conn);

for ($rec = 0; $rec < $results_nrows; $rec++){
	if ($rec % 2) {
		echo '<TR class="even">';
	} else {
		echo '<TR>';
	}
?>
	<TD><?php echo $results["ODBC_NAME"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["ODBC_DSN"][$rec] . "&nbsp;"; ?></TD>
<?php
	echo "<td><a href=queryaccessdb.php?pk_vlnk_odbc=".$results["PK_VLNK_ODBC"][$rec].">Select</a></td>";
	echo "</TR>";
}
?>
</TABLE>
      </div>


</body>
</html>

<?php
}
else header("location: ./index.php?fail=1");
?>
