<?php
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Edit Code List</title>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");
include ("./txt-db-api.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
</head>
<body>

<div id="fedora-content">

<?php
// phpinfo();
if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 

$v_pk_codelst = $_GET['pk_codelst'];
$v_tablename = $_GET['tablename'];
$v_pk_codelst_maint = $_GET['pk_codelst_maint'];
if ($v_tablename == "ER_CODELST") {
	$query="select codelst_type,codelst_subtyp,codelst_desc,codelst_custom_col,codelst_seq,codelst_custom_col,codelst_custom_col1,codelst_hide from er_codelst where pk_codelst = '".$v_pk_codelst."'";
} 

$results = executeOCIQuery($query,$ds_conn);
$total_rows = $results_nrows;

?>

<div class="navigate">Code List - Edit	</div>
<!--<FORM name="stdRights" action="codelist_edit.php" method=post onSubmit="if (setRights(document.stdRights) == false) return false; if (validate(document.stdRights) == false) return false;">  -->
<FORM name="stdRights" action="codelist_dca.php" method=post>
<BR>
<?php
	$codelst_Label = array("fillformstat","studystat_type","studystat","query_status","milestone_stat","budget_stat","calStatStd","cost_desc","coverage_type");
	$codelst_type = array("fillformstat" => "Form Response Status","studystat_type" => "Study Status Type","studystat" => "Study Status","query_status" => "Form Query Status","milestone_stat" => "Milestone Status","milestone_stat" => "Milestone Status","budget_stat" => "Budget Status", "calStatStd" => "Calendar Study Status", "cost_desc" => "Event Cost Type", "coverage_type" => "Event Coverage Type");
	
	$csrQ = "SELECT CODELST_DESC from ER_CODELST where pk_codelst=".$v_pk_codelst;	
	$csrrs = executeOCIQuery($csrQ, $ds_conn); 
	
	echo '<table width="100%">';	
	echo '<tr><span style="padding-left:170px;">Study Team Role</span> : <b>'.$csrrs["CODELST_DESC"][0].'</b></tr></br></br>';
	$count = 0;
	for($i=0; $i<count($codelst_Label); $i++){
		echo '<tr><td width="15%">';	
		echo '<b>'.$codelst_type[$codelst_Label[$i]].'</b>';
		echo '</td>';
		echo '<td width="85%">';
		echo '<table width="100%"><tr>';		
		
		if($codelst_Label[$i] == "budget_stat" || $codelst_Label[$i] == "calStatStd" || $codelst_Label[$i] == "cost_desc" || $codelst_Label[$i] == "coverage_type"){		
			$csrQ = "SELECT CODELST_SUBTYP from ER_CODELST where pk_codelst=".$v_pk_codelst;	
			$csrrs = executeOCIQuery($csrQ, $ds_conn);
			$coditemsQ = "SELECT * from SCH_CODELST where codelst_type='".$codelst_Label[$i]."'";
			$coditems = executeOCIQuery($coditemsQ, $ds_conn);
			$codenrow = $results_nrows;					
		}else{
			$csrQ = "SELECT CODELST_SUBTYP from ER_CODELST where pk_codelst=".$v_pk_codelst;	
			$csrrs = executeOCIQuery($csrQ, $ds_conn);
			$coditemsQ = "SELECT * from ER_CODELST where codelst_type='".$codelst_Label[$i]."'";
			$coditems = executeOCIQuery($coditemsQ, $ds_conn);
			$codenrow = $results_nrows;			
		}		
		if($codenrow!=0){			
			for($j=0; $j<$codenrow; $j++){
				$csr = explode(',', $coditems["CODELST_STUDY_ROLE"][$j]);			
				echo '<td width="20%">';
				
				if($codelst_Label[$i] != "budget_stat" && $codelst_Label[$i] != "calStatStd" && $codelst_Label[$i] != "cost_desc" && $codelst_Label[$i] != "coverage_type"){
					if(in_array($csrrs["CODELST_SUBTYP"][0],$csr)){
						echo '<b><font color="#009900;"><input type=checkbox name=configu['.$count.'] checked value=ER'.$coditems["PK_CODELST"][$j].'>'.$coditems["CODELST_DESC"][$j].'</font></b>';	
						echo '<input type=hidden name=pre_conf['.$count.'] value=ER'.$coditems["PK_CODELST"][$j].'>';																		
					}else{
						echo '<input type=checkbox name=configu['.$count.'] value=ER'.$coditems["PK_CODELST"][$j].'>'.$coditems["CODELST_DESC"][$j];
					}				
				}else{
					if(in_array($csrrs["CODELST_SUBTYP"][0],$csr)){
						echo '<b><font color="#009900;"><input type=checkbox name=configu['.$count.'] checked value=ES'.$coditems["PK_CODELST"][$j].'>'.$coditems["CODELST_DESC"][$j].'</font></b>';						
						echo '<input type=hidden name=pre_conf['.$count.'] value=ES'.$coditems["PK_CODELST"][$j].'>';																		
					}else{
						echo '<input type=checkbox name=configu['.$count.'] value=ES'.$coditems["PK_CODELST"][$j].'>'.$coditems["CODELST_DESC"][$j];
					}
				}

				echo '</td>';
				if (fmod($j+1,3) == 0) echo '</tr><tr>';
				$count++;
			}			
		}else{
			echo '<i><font color=red>This Code List doesn\'t have any items</font></i>';
		}
		echo '</tr></table>';							
		echo '</td></tr>';
		if($i!=count($codelst_Label)-1){
			echo '<tr><td>&nbsp;</td><td ><hr size=1/></td></tr>'; 
		}		
	}
?>
	<tr>
	<td width="94">
	<BR>
	<input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" /> 
	<input type="hidden" name="rightstr" id="rightstr" value="0" />
<input type = "hidden" name="h_pk_codelst" id="h_pk_codelst" value="<?php echo $v_pk_codelst ?>">
<input type = "hidden" name="h_tablename" id="h_tablename" value="<?php echo $v_tablename ?>">
<input type = "hidden" name="h_pk_codelst_maint" id="h_pk_codelst_maint" value="<?php echo $v_pk_codelst_maint ?>">
	</td>
		<td width="594">&nbsp;</td>
	</tr>
</table>
</form>
<?php 
} else { 

$vh_pk_codelist = $_POST['h_pk_codelst'];
$vh_tablename= $_POST['h_tablename'];
$vh_codelst_maint = $_POST['h_pk_codelst_maint'];

$roleQ = "SELECT CODELST_SUBTYP from ER_CODELST where pk_codelst=".$vh_pk_codelist;
$roleRS = executeOCIQuery($roleQ,$ds_conn);
$roleDes = $roleRS["CODELST_SUBTYP"][0];

if(isset($_POST['configu'])){
	$allChkd = $_POST['configu'];
	$preChkd = isset($_POST['pre_conf'])?$_POST['pre_conf']:array();
	$uncChkd = array_diff($preChkd,$allChkd);

	//when something is checked
	foreach($allChkd as $conf_pko){
		$prefix = substr($conf_pko,0,2);
		$conf_pk = substr($conf_pko,2,strlen($conf_pko));		
		
		if($prefix=="ER"){
			$confQ = "SELECT * from ER_CODELST where pk_codelst=".$conf_pk;
			$confQRS = executeOCIQuery($confQ,$ds_conn);		
			$codSRole = $confQRS["CODELST_STUDY_ROLE"][0];
			if(strlen($codSRole)!=0){		
				if(substr_count($codSRole,',')!=0){
					$val_csr = explode(',',$codSRole);
					if(in_array($roleDes,$val_csr)==false){
						$finStr = $codSRole.','.$roleDes;
						$insQ = "UPDATE ER_CODELST set CODELST_STUDY_ROLE='".$finStr."' where pk_codelst=".$conf_pk;
						$insRS = executeOCIUpdateQuery($insQ,$ds_conn);
					}
				}elseif($codSRole!=$roleDes){
						$finStr = $codSRole.','.$roleDes;						
						$insQ = "UPDATE ER_CODELST set CODELST_STUDY_ROLE= '".$finStr."' where pk_codelst=".$conf_pk;
						$insRS = executeOCIUpdateQuery($insQ,$ds_conn);				
				} 
			}else{
				$insQ = "UPDATE ER_CODELST set CODELST_STUDY_ROLE='".$roleDes."' where pk_codelst=".$conf_pk;			
				$insRS = executeOCIUpdateQuery($insQ,$ds_conn);	
			}
		}else{
			$confQ = "SELECT * from SCH_CODELST where pk_codelst=".$conf_pk;
			$confQRS = executeOCIQuery($confQ,$ds_conn);
			$codSRole = $confQRS["CODELST_STUDY_ROLE"][0];
			if(strlen($codSRole)!=0){		
				if(substr_count($codSRole,',')!=0){
					$val_csr = explode(',',$codSRole);
					if(in_array($roleDes,$val_csr)==false){
						$finStr = $codSRole.','.$roleDes;
						$insQ = "UPDATE SCH_CODELST set CODELST_STUDY_ROLE='".$finStr."' where pk_codelst=".$conf_pk;
						$insRS = executeOCIUpdateQuery($insQ,$ds_conn);
					}
				}elseif($codSRole!=$roleDes){
						$finStr = $codSRole.','.$roleDes;
						$insQ = "UPDATE SCH_CODELST set CODELST_STUDY_ROLE= '".$finStr."' where pk_codelst=".$conf_pk;
						$insRS = executeOCIUpdateQuery($insQ,$ds_conn);				
				} 
			}else{
				$insQ = "UPDATE SCH_CODELST set CODELST_STUDY_ROLE='".$roleDes."' where pk_codelst=".$conf_pk;
				$insRS = executeOCIUpdateQuery($insQ,$ds_conn);	
			}		
		}		
	}
	// when uncheck happens
	foreach($uncChkd as $unchk_pk){
		$prefix = substr($unchk_pk,0,2);
		$unchk_pk = substr($unchk_pk,2,strlen($unchk_pk));
		
		
		if($prefix=="ER"){
		
			$unconfQ = "SELECT * from ER_CODELST where pk_codelst=".$unchk_pk;
			$unconfQRS = executeOCIQuery($unconfQ,$ds_conn);		
			$uncodSRole = $unconfQRS["CODELST_STUDY_ROLE"][0];
	
			if(strlen($uncodSRole)!=0){
				if(substr_count($uncodSRole,',')!=0){
					$unval_csr = explode(',',$uncodSRole);
					if(in_array($roleDes, $unval_csr)){						
						$finStr = "";
						for($i=0; $i<count($unval_csr); $i++){
							if($unval_csr[$i]!=$roleDes){							
								$finStr.= $unval_csr[$i].",";
							}
						}
						if(substr($finStr,strlen($finStr)-1,strlen($finStr))==","){
							$finStr = substr($finStr,0,strlen($finStr)-1);
						}
						$insQ = "UPDATE ER_CODELST set CODELST_STUDY_ROLE='".$finStr."' where pk_codelst=".$unchk_pk;
						$insRS = executeOCIUpdateQuery($insQ,$ds_conn);	
					}				
				}elseif($uncodSRole==$roleDes){
						$insQ = "UPDATE ER_CODELST set CODELST_STUDY_ROLE='' where pk_codelst=".$unchk_pk;
						$insRS = executeOCIUpdateQuery($insQ,$ds_conn);									
				}
			}
		}else{
			$unconfQ = "SELECT * from SCH_CODELST where pk_codelst=".$unchk_pk;
			$unconfQRS = executeOCIQuery($unconfQ,$ds_conn);		
			$uncodSRole = $unconfQRS["CODELST_STUDY_ROLE"][0];
	
			if(strlen($uncodSRole)!=0){
				if(substr_count($uncodSRole,',')!=0){
					$unval_csr = explode(',',$uncodSRole);
					if(in_array($roleDes, $unval_csr)){						
						$finStr = "";
						for($i=0; $i<count($unval_csr); $i++){
							if($unval_csr[$i]!=$roleDes){							
								$finStr.= $unval_csr[$i].",";
							}
						}
						if(substr($finStr,strlen($finStr)-1,strlen($finStr))==","){
							$finStr = substr($finStr,0,strlen($finStr)-1);
						}
						$insQ = "UPDATE SCH_CODELST set CODELST_STUDY_ROLE='".$finStr."' where pk_codelst=".$unchk_pk;
						$insRS = executeOCIUpdateQuery($insQ,$ds_conn);	
					}				
				}elseif($uncodSRole==$roleDes){
						$insQ = "UPDATE SCH_CODELST set CODELST_STUDY_ROLE='' where pk_codelst=".$unchk_pk;
						$insRS = executeOCIUpdateQuery($insQ,$ds_conn);									
				}
			}		
		}
		
		
	}	
}else{	
	// when nothing is checked
	
	$preChkd = $_POST['pre_conf'];
	foreach($preChkd as $conf_pk){ 
		if(substr($conf_pk,0,2)=="ER"){
			$conf_pk = str_replace(array("ER","ES"),"",$conf_pk);
			$confQ = "SELECT * from ER_CODELST where pk_codelst=".$conf_pk;
			$confQRS = executeOCIQuery($confQ,$ds_conn);
			$codSRole = $confQRS["CODELST_STUDY_ROLE"][0];		
			$preCode = 'ER_CODELST';
		}else{
			$conf_pk = str_replace(array("ER","ES"),"",$conf_pk);		
			$confQ = "SELECT * from SCH_CODELST where pk_codelst=".$conf_pk;
			$confQRS = executeOCIQuery($confQ,$ds_conn);
			$codSRole = $confQRS["CODELST_STUDY_ROLE"][0];				
			$preCode = 'SCH_CODELST';
		}

		$insQ = "UPDATE SCH_CODELST set CODELST_STUDY_ROLE= '' where pk_codelst=".$conf_pk;
		$insRS = executeOCIUpdateQuery($insQ,$ds_conn);

		if(strlen($codSRole)!=0){
			if(substr_count($codSRole,',')!=0){
				$val_csr = explode(',',$codSRole);
				if(in_array($roleDes,$val_csr)){
					$finStr = "";
					for($i=0; $i<count($val_csr); $i++){
						if($val_csr[$i]!=$roleDes){	
							$finStr.= $val_csr[$i].",";					
						}
					}					
					if(substr($finStr,strlen($finStr)-1,strlen($finStr))==","){
						$finStr = substr($finStr,0,strlen($finStr)-1);						
					}
					$insQ = "UPDATE ".$preCode." set CODELST_STUDY_ROLE ='".$finStr."' where pk_codelst=".$conf_pk;
					$insRS = executeOCIUpdateQuery($insQ,$ds_conn);
				}
			}elseif($confQRS["CODELST_STUDY_ROLE"][0]==$roleRS["CODELST_SUBTYP"][0]){
					$insQ = "UPDATE ".$preCode." set CODELST_STUDY_ROLE='' where pk_codelst=".$conf_pk;
					$insRS = executeOCIUpdateQuery($insQ,$ds_conn);
			}
		} else {
			$insQ = "UPDATE ".$preCode." SET CODELST_STUDY_ROLE = '' where pk_codelst=".$conf_pk;
			$insRS = executeOCIUpdateQuery($insQ,$ds_conn);
		}
	}
} 

OCICommit($ds_conn);
OCILogoff($ds_conn);
echo "Data Saved Successfully !!!";
echo '<meta http-equiv="refresh" content="0; url=./codelist.php">';
}

?>

</div>
</body>
</html>

<?php
}
else header("location: index.php?fail=1");
?>
