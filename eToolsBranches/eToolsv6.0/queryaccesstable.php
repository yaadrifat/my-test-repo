<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Query Database</title>
</head>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

if (isset($_GET["pk_vlnk_odbc"])) $v_pk_vlnk_odbc = $_GET["pk_vlnk_odbc"];
if (isset($_POST["pk_vlnk_odbc"])) $v_pk_vlnk_odbc = $_POST["pk_vlnk_odbc"];

$query = "select odbc_name,odbc_dsn,odbc_user,odbc_pass from velink.vlnk_odbc where pk_vlnk_odbc = ".$v_pk_vlnk_odbc;
$results = executeOCIQuery($query,$ds_conn);
$connaccess=odbc_connect($results["ODBC_DSN"][0],$results["ODBC_USER"][0],$results["ODBC_PASS"][0]);

?>
<body>
<div id="fedora-content">	


<FORM action="queryaccesstable.php" method=post>
<INPUT type="hidden" name="pk_vlnk_odbc" value="<?PHP echo $v_pk_vlnk_odbc; ?>"/>
<?php


if (isset($_POST['query']) ) {
	$v_query =  $_POST['query'];
	} else {
		if (!empty($_SESSION['query']) ) {
			$v_query =  $_SESSION['query'];
		} else {
			if (isset($_GET["table_name"])){
				$v_query = 'select * from "'.$_GET["table_name"].'"';
			} else {
				if (isset($_GET["distinctquery"])){
					$v_query = $_GET["distinctquery"];
				} else {
					$v_query = "";
				}
			}
		}
	}
?>
<div class="navigate">Import Access DB - View</div>
<table width="100%" border="1">
<tr>
<td><Textarea name="query" type="text" cols=110 maxlength=10000 rows=5><?PHP echo $v_query; ?></textarea>
</td>
</tr><tr>
<td><input type=submit name=submit></input></td>
</tr>
</table>


</form>

<?php
//if (isset($_POST['submit'])) {	

?>

<?php
if (isset($_POST['query'])) {
	$query=$_POST['query'];
} else {
	$query=$v_query;
}

$sql="SELECT count(*) as cnt from (".$query.")"; 
$rs_cnt=odbc_exec($connaccess,$sql);

$rs=odbc_exec($connaccess,$query);
echo "<B>Total Rows: ".odbc_result($rs_cnt,1)."</B>";


echo '<table border="1">';
echo "<tr>";
	for ($i=1;$i<=odbc_num_fields($rs);$i++){
  		echo "<th>".odbc_field_name($rs,$i)."[".odbc_field_type($rs,$i)." (".odbc_field_precision($rs,$i).")]"."</th>";
//		if (odbc_field_type($rs,$i) == 'VARCHAR'){
//  			echo "<th><a href='queryaccesstable.php?distinctquery=select distinct ".odbc_field_name($rs,$i).' from ('.$query.")'>".odbc_field_name($rs,$i).'</a></th>';
//  		} else {
//  			echo "<th>".odbc_field_name($rs,$i)."</th>";
//  		}
      }
echo "</tr>";

$rows = 0;
while (odbc_fetch_row($rs))
{
echo "<tr>";
	for ($i=1;$i<=odbc_num_fields($rs);$i++){
  		echo "<td>".odbc_result($rs,$i)."</td>";
      }
echo "</tr>";
$rows = $rows + 1;
if ($rows > 200){exit;}

}
odbc_close($connaccess);
/*
$stmt = OCIParse($ds_conn,$query);
OCIExecute($stmt);
$ncols = OCINumCols($stmt);
$results = executeOCIQuery($query,$ds_conn);
echo "Total Rows: ".$results_nrows;
echo " &nbsp;&nbsp;|&nbsp;&nbsp; Total Columns: ".$ncols;
echo " &nbsp;&nbsp;|&nbsp;&nbsp; 100 Records displayed. ";
echo "<Table border=1>";
echo "<TR>";

echo "<TH>ROW</TH>";
for ($i=1; $i <= $ncols; $i++){
	echo "<TH>".OCIColumnName($stmt,$i)."</TH>";
}
echo "</TR>";


$rownum = 0;
$rowcount = 100;
if ($results_nrows < 100){
$rowcount = $results_nrows;
}
for ($rec=0; $rec < $rowcount; $rec++){
$rownum = $rec + 1;
echo "<TR><TD>".$rownum."</TD>";
	
	for ($i=1; $i <= $ncols; $i++){
		echo "<TD>".$results[OCIColumnName($stmt,$i)][$rec]."&nbsp;"."</TD>";
	}
echo "</TR>";
}	
*/

?>
</TABLE>
</div>


</body>
</html>
<?php
OCICommit($ds_conn);
OCILogoff($ds_conn);
//}
}
else header("location: index.php?fail=1");
?>
