<?php
session_start();
header("Cache-control: private");
if(@$_SESSION["user"]){
?>

<html>
    <head>
        <title>Velos eTools -&gt; Data Source</title>
        <?php
        include("db_config.php");
        include("./includes/oci_functions.php");        
        include("./includes/header.php");
        require_once('audit_queries.php');
?>
    </head>
<body>
    <div id="fedora-content">	
    <div class="navigate">Data Sources</div>
    <?PHP        
        echo "</br>";
        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        echo "<a href=datasource_edit.php?mode=n>Add Data Source</a>";
        echo "</br>";
        echo "&nbsp;"; ?>
        <Table border="1" width="100%"><TR>
            <TH width="3%">SNO</TH>
            <TH width="24%">DS Name </TH>
            <TH width="12%">DS Host </TH>
            <TH width="8%">DS Port </TH>
            <TH width="15%">DS SID </TH>
            <TH width="10%">DS Status </TH>		
            <TH width="6%">&nbsp;</TH>
            <TH width="10%">&nbsp;</TH>		
            <TH width="6%">&nbsp;</TH>
        </TR>		
    <?php        
        $dsRights = '';
        if($_SESSION["FK_GROUPS"] == 1){            
            $records = mysql_query("SELECT ds_rights FROM et_groups where pk_groups=1");
            if(mysql_num_rows($records)>0){
                while($row = mysql_fetch_array($records)){
                    if(substr($row['ds_rights'],0,1) == "|"){
                        $dsString = substr($row['ds_rights'],1,strlen($row['ds_rights']));
                    }else{
                        $dsString = $row['ds_rights'];
                    }
                    #$dsRights = explode("|",$row['ds_rights']);
                    $dsRights = explode("|",$dsString);
                }
            }            
        }else{
            $recordsot = mysql_query("SELECT ds_rights FROM et_groups where pk_groups = ".$_SESSION["FK_GROUPS"]);
            if(mysql_num_rows($recordsot) > 0){
                while($rowot = mysql_fetch_array($recordsot)){                                        
                    if(substr($rowot['ds_rights'],0,1) == "|"){                        
                        $dsString = substr($rowot['ds_rights'],1,strlen($rowot['ds_rights']));
                    }
                    $dsRights = explode("|",$rowot['ds_rights']);                                        
                }
            }
        }
    $v_counter = 0;    
    $dsda_rs = mysql_query("SELECT * FROM et_groups where pk_groups = ".$_SESSION["FK_GROUPS"]);
    $v_sno = mysql_num_rows($dsda_rs);
    $v_adsno =1;
    $v_usno =1;

    for($i=0; $i<sizeof($dsRights); $i++){
        list($pk_ds, $ds)= explode(":",$dsRights[$v_counter]);        
        $dsQry = mysql_query("SELECT * from et_ds where pk_ds = ".$pk_ds);
        $dsQry_rs = mysql_fetch_array($dsQry);
        
        if($_SESSION["PK_USERS"] == 1){ ?> 
            <TR bgcolor="#FFFFFF" onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
            <TD><?php echo $v_adsno ?></TD>
            <TD><?php echo $dsQry_rs["ds_name"] ?></TD>
            <TD><?php echo $dsQry_rs["ds_host"]?></TD>
            <TD><?php echo $dsQry_rs["ds_port"] ?></TD>
            <TD><?php echo $dsQry_rs["ds_sid"] ?></TD>
            <TD><?php 
                if($ds == 1){
                    echo "Enabled";                     
                }else{ 
                    echo "Disabled";                    
                }?></TD>			
            <TD><?PHP echo '<a href=datasource_edit.php?mode=m&pk_ds='.$dsQry_rs["pk_ds"].'>Edit</a>'; ?></TD>
            <TD><?PHP echo '<a href=datasource_edit.php?mode=c&pk_ds='.$dsQry_rs["pk_ds"].'>Change Password</a>'; ?></TD>
            <TD><?PHP echo '<a href=datasource_dash.php?mode=d&pk_ds='.$dsQry_rs["pk_ds"].'>Delete</a>'; ?></TD>			
            <input type='hidden' name='dsst' value=<?PHP echo $ds; ?>>			
            </TR>			
            <?php            
            $v_adsno++;
        }else{
            if($ds == 1){
            ?>
            <TR bgcolor="#FFFFFF" onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
            <TD><?php echo $v_usno ?></TD>
            <TD><?php echo $dsQry_rs["ds_name"] ?></TD>
            <TD><?php echo $dsQry_rs["ds_host"]?></TD>
            <TD><?php echo $dsQry_rs["ds_port"] ?></TD>
            <TD><?php echo $dsQry_rs["ds_sid"] ?></TD>
            <TD><?php 
                if($ds == 1){
                    echo "Enabled";                     
                }else{ 
                    echo "Disabled";                    
                }?></TD>
            <TD><?PHP echo '<a href=datasource_edit.php?mode=m&pk_ds='.$dsQry_rs["pk_ds"].'>Edit</a>'; ?></TD>
            <TD><?PHP echo '<a href=datasource_edit.php?mode=c&pk_ds='.$dsQry_rs["pk_ds"].'>Change Password</a>'; ?></TD>
            <TD><?PHP echo '<a href=datasource_dash.php?mode=d&pk_ds='.$dsQry_rs["pk_ds"].'>Delete</a>'; ?></TD>			
            <input type='hidden' name='dsst' value=<?PHP echo $ds; ?>>			
            </TR>                        
            <?php  $v_usno++;
            }            
        }        
        $v_counter++;
    }
    ?>
    <?php
        if (!($_SERVER['REQUEST_METHOD'] == 'POST')) {
            $v_mode = $_GET["mode"];
            $pk_num = $_GET["pk_ds"];

            /*--- AUDIT ---*/
            $grpName = mysql_query("SELECT group_name from et_groups WHERE pk_groups =".$_SESSION["FK_GROUPS"]);
            $grpNameVal = mysql_fetch_array($grpName);
            /*--- END ---*/

            if($v_mode=="d"){                
                    $noDs = mysql_query("SELECT pk_groups from et_groups");
                    if(mysql_num_rows($noDs)>0){
                        while($noDs_row = mysql_fetch_array($noDs)){                            
                            $rightsResult = mysql_query("SELECT ds_rights from et_groups where pk_groups=".$noDs_row["pk_groups"]);
                            if(mysql_num_rows($rightsResult) >0){
                                while($rightsResult_row = mysql_fetch_array($rightsResult)){
                                    $ds_rights = explode("|",$rightsResult_row['ds_rights']);
                                }
                            }

                            for($i=0; $i<count($ds_rights)+1;$i++){
                                $tmpStr = explode(":",$ds_rights[$i]);
                                if($tmpStr[0]==$pk_num){
                                    unset($ds_rights[$i]);
                                }
                            }		
                            $rightsStr = "";		
                            for($i=0; $i<count($ds_rights)+1;$i++){	
                                if($i==count($ds_rights)){
                                    $rightsStr.= $ds_rights[$i];
                                }else{
                                    $rightsStr.= $ds_rights[$i]."|";									
                                }
                            }

                            $tmpStr = substr($rightsStr,0,1);
                            if ($tmpStr!="|"){
                                $rightsStr = substr($rightsStr,1,strlen($rightsStr)-1);			
                            }

                            $lastStr = substr($rightsStr,strlen($rightsStr)-1,strlen($rightsStr));
                            if ($lastStr=="|"){
                                $rightsStr = substr($rightsStr,0,strlen($rightsStr)-1);			
                            }

                            $rightsStr = str_replace("||","|",$rightsStr);	
                            
                            $updateRs = mysql_query("UPDATE et_groups set ds_rights='".$rightsStr."' where pk_groups=".$noDs_row["pk_groups"]);

                            /*--- AUDIT ---*/
                            $rowInfo = array(2,$_SESSION["user"],$grpNameVal['GROUP_NAME'], "SYSDATE", "SYSDATE", "U");
                            auditQuery(2,$rowInfo);
                            /*--- END ---*/

                            /*--- AUDIT ---*/
                            $diffName = array('DS_RIGHTS');
                            $diffValue = array($rightsStr);
                            $tblname = 'et_groups';
                            colQueries(array_diff($diffName, array('')), array_diff($diffValue, array('')), $tblname, array_diff($data, array('')));
                            /*--- END ---*/
                        }
                    }

                    /*--- AUDIT ---*/
                    $oldValue = mysql_query("SELECT ds_name,ds_host, ds_port, ds_sid from et_ds WHERE pk_ds=".$pk_num);		
                    $oldValueRs = mysql_fetch_array($oldValue); 

                    $deleteRs = mysql_query("DELETE from et_ds where pk_ds=".$pk_num);

                    /*--- AUDIT ---*/
                    $rowInfo = array(2,$_SESSION["user"],$grpNameVal['GROUP_NAME'], "SYSDATE", "SYSDATE", "D");
                    auditQuery(2,$rowInfo);
                    /*--- END ---*/

                    $diffName = array('DS_NAME', 'DS_HOST', 'DS_PORT', 'DS_SID');
                    $diffValue = array('','','','');
                    $tblname = 'et_ds';
                    colQueries(array_diff($diffName, array('')), array_diff($diffValue, array('')), $tblname, array_diff($oldValueRs, array('')));
                    /*--- END ---*/

                    echo '<meta http-equiv="refresh" content="0; url=./datasource_dash.php">';
            }
        }
    ?>
    </TABLE>
    </div>
</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
