<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
<title>Velos eTools -> Migrate Form Data</title>

<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

if (isset($_GET["mode"])) {
	$v_mode = $_GET["mode"];
} else {
//	$v_mode = "M";
	$v_mode = "NM";	
}


?>
</head>
<body>
<div id="fedora-content">	
<div class="navigate">Migrate Data - Forms - Map Responses</div><br>

<?php
echo '<table width="100%"><tr><td width="20%"><a href="./mig_forms.php?pk_vlnk_adaptor='.$_REQUEST["pk_vlnk_adaptor"].'">Back to Migration Page</a></td><td align=center>';

//echo "<input ".(($v_mode == 'NM')? "checked" : "")." type='radio' name='display' value='NM' onclick='window.location=\"mig_forms_mapfldresp.php?tablename=".$_REQUEST["tablename"]."&pk_vlnk_adapmod=".$_REQUEST["pk_vlnk_adapmod"]."&pk_vlnk_adaptor=".$_REQUEST["pk_vlnk_adaptor"]."&pk_formlib=".$_REQUEST["pk_formlib"]."&refresh=0&mode=NM\"' /> Not Mapped";

echo "<input ".(($v_mode == 'NM')? "checked" : "")." type='radio' name='display' value='NM' onclick='window.location=\"mig_forms_mapfldresp.php?tablename=".$_REQUEST["tablename"]."&pk_vlnk_adapmod=".$_REQUEST["pk_vlnk_adapmod"]."&pk_formlib=".$_REQUEST["pk_formlib"]."&pk_vlnk_adaptor=".$_REQUEST["pk_vlnk_adaptor"]."&refresh=0&mode=NM\"' />Not Mapped";

echo "<input ".(($v_mode == 'M')? "checked" : "")." type='radio' name='display' value='M' onclick='window.location=\"mig_forms_mapfldresp.php?tablename=".$_REQUEST["tablename"]."&pk_vlnk_adapmod=".$_REQUEST["pk_vlnk_adapmod"]."&pk_formlib=".$_REQUEST["pk_formlib"]."&pk_vlnk_adaptor=".$_REQUEST["pk_vlnk_adaptor"]."&refresh=0&mode=M\"' /> Mapped";

echo "<input ".(($v_mode == 'ALL')? "checked" : "")." type='radio' name='display' value='ALL' onclick='window.location=\"mig_forms_mapfldresp.php?tablename=".$_REQUEST["tablename"]."&pk_vlnk_adapmod=".$_REQUEST["pk_vlnk_adapmod"]."&pk_formlib=".$_REQUEST["pk_formlib"]."&pk_vlnk_adaptor=".$_REQUEST["pk_vlnk_adaptor"]."&refresh=0&mode=ALL\"' /> All";

echo "</td></tr></table>";

if ($_SERVER['REQUEST_METHOD'] != "POST"){
?>

<form name="map" action="mig_forms_mapfldresp.php" method="post">
<input type='hidden' name='pk_vlnk_adaptor' value="<?PHP echo $_GET['pk_vlnk_adaptor']; ?>" />
<input type='hidden' name='pk_formlib' value="<?PHP echo $_GET['pk_formlib']; ?>" />
<input type='hidden' name='tabname' value="<?PHP echo $_GET['tablename']; ?>" />
<input type='hidden' name='linkedto' value="<?PHP echo $_GET['linkedto']; ?>" />
<input type='hidden' name='formname' value="<?PHP echo $_GET['formname']; ?>" />
<input type='hidden' name='pk_vlnk_adapmod' value="<?PHP echo $_GET['pk_vlnk_adapmod']; ?>" />
<?PHP

$select_sql = "SELECT pk_imp_formfld,fk_field,table_colname,map_displayval, fld_name FROM velink.VLNK_IMP_FORMFLD, eres.er_fldlib WHERE pk_field = fk_field AND form_colname IS NOT NULL AND map_displayval is not null and table_colname is not null and fk_vlnk_adapmod = ".$_GET['pk_vlnk_adapmod'];

switch ($v_mode) {
case "NM":
	$select_sql .= ' and FK_MAP_RESP = 0 ';
	break;
case "M":
	$select_sql .= ' and FK_MAP_RESP > 0 ';
	break;
}
$select_sql .= "ORDER BY pk_imp_formfld";

$col_results = executeOCIQuery($select_sql,$ds_conn);
$col_count = $results_nrows;	

echo "<table border=1 width='100%'>";
echo "<TH>Field Name</TH>";
echo "<TH>Column Name</TH>";
echo "<TH>Responses</TH>";

	for ($k = 0; $k < $col_count; $k++){
		echo "<INPUT type=\"hidden\" name=\"pk_imp_formfld[$k]\" value=".$col_results["PK_IMP_FORMFLD"][$k]."></input>";
		echo "<tr>";
		echo "<td>".$col_results["FLD_NAME"][$k]."</TD>";
		echo "<td>".$col_results["TABLE_COLNAME"][$k]."</TD>";
	
		if ($col_results["MAP_DISPLAYVAL"][$k] != ""){
			echo "<td>";			
			$select_sql = "select pk_fldresp,fldresp_dispval,fldresp_dataval from eres.er_fldresp where fk_field = ".$col_results["FK_FIELD"][$k]." order by fldresp_seq";
			$results = executeOCIQuery($select_sql,$ds_conn);
			$total_rows = $results_nrows;
	
	
			if ($col_results["TABLE_COLNAME"][$k] != "") {
				$select_sql = "select distinct ".$col_results["TABLE_COLNAME"][$k]." as mapvals from ".$_GET['tablename'];
				$table_results = executeOCIQuery($select_sql,$ds_conn);
				$sub_rows = $results_nrows;
			} else {$sub_rows =0;}
	
	
			if ($col_results["MAP_DISPLAYVAL"][$k] == "DISP") {
				$dispdata = "FLDRESP_DISPVAL";
			} else {
				$dispdata = "FLDRESP_DATAVAL";
			}
	
			echo "<table>";
			for ($mrec = 0; $mrec < $sub_rows; $mrec++){
				$select_sql = "select fldresp,map_to from velink.vlnk_imp_formresp where fk_imp_formfld = ".$col_results["PK_IMP_FORMFLD"][$k]." and map_to = '".str_replace("'","''",$table_results["MAPVALS"][$mrec])."'";
				$resultsMap = executeOCIQuery($select_sql,$ds_conn);
				if ($results_nrows > 0 ) {
					$v_mappedVal = $resultsMap["FLDRESP"][0];
					$v_mappedTo = $resultsMap["MAP_TO"][0];
				} else {
					$v_mappedVal = "";
					$v_mappedTo = "";
				}
	
				echo "<tr>";
					echo "<td><INPUT style='border-right: 0px solid #f5f5f5;border-left: 0px solid #f5f5f5;border-top: 0px solid #f5f5f5; border-bottom: 1px solid #bbbbbb;' readonly name=\"tab_resp1[$k][$mrec]\" value=\"".$table_results["MAPVALS"][$mrec]."\">";
					echo "<INPUT type=hidden name=\"tab_resp[$k][$mrec]\" value=\"".urlencode($table_results["MAPVALS"][$mrec])."\">";
					echo "<TD><select name=\"fld_resp[$k][$mrec]\">";
					echo "<OPTION VALUE=''></OPTION>";
					$v_sflag = false;
					for ($srec = 0; $srec < $total_rows; $srec++){
						if ($table_results["MAPVALS"][$mrec] == $v_mappedTo && $results["FLDRESP_DISPVAL"][$srec] == $v_mappedVal) {
							echo "<OPTION VALUE=\"".urlencode($results["FLDRESP_DISPVAL"][$srec]."[VELSEP1]".$results["FLDRESP_DATAVAL"][$srec])."\" SELECTED>".$results[$dispdata][$srec]."</OPTION>";
						} else {
						if (($results[$dispdata][$srec] == $table_results["MAPVALS"][$mrec]) and $results_nrows == 0) {
							echo "<OPTION VALUE=\"".urlencode($results["FLDRESP_DISPVAL"][$srec]."[VELSEP1]".$results["FLDRESP_DATAVAL"][$srec])."\" SELECTED>".$results[$dispdata][$srec]."</OPTION>";
						} else {
							echo "<OPTION VALUE=\"".urlencode($results["FLDRESP_DISPVAL"][$srec]."[VELSEP1]".$results["FLDRESP_DATAVAL"][$srec])."\" >".$results[$dispdata][$srec]."</OPTION>";
						}
						}
						//echo "<input type='text' value=".$results["FLDRESP_DISPVAL"][$srec]."|".$v_mappedVal." />";	
					}
					echo "</select></TD>";
					
	
				echo "</tr>";
			}
			echo "</table>";
			echo "</TD>";
		} else {
			echo "<td>&nbsp;</TD>";
			echo "<input type=hidden name=\"tab_resp[$k][0]\" value=\"\"></input>";
			echo "<input type=hidden name=\"fld_resp[$k][0]\" value=\"\"></input>";
			//echo "<input type=hidden name=\"mapVals[$k][0]\" value=\"\"></input>";
		}
		echo "</tr>";
	}

echo "</table> <br>";
echo '<input type="image" name="submit" value="SUBMIT" src="./img/submit.png"  align="absmiddle" border="0" onmouseover="this.src=\'./img/submit_m.png\';" onmouseout="this.src=\'./img/submit.png\';" />';

} else {
$pk_imp_formfld = $_POST["pk_imp_formfld"];
$fld_resp = $_POST["fld_resp"];
$tab_resp = $_POST["tab_resp"];
$v_form = $_POST["pk_formlib"];
$encodingList[] = "ASCII";
$encodingList[] = "EUC-KR";
for ($i=0;$i<count($pk_imp_formfld);$i++){
//	echo $pk_imp_formfld[$i];
	$v_sql = "delete from velink.vlnk_imp_formresp where fk_imp_formfld = ".$pk_imp_formfld[$i];
	$results = executeOCIUpdateQuery($v_sql,$ds_conn);
	for ($j=0; $j<count($tab_resp[$i]); $j++){

	/*	echo $tab_resp[$i][$j];
		echo " | ";
		echo $fld_resp[$i][$j];
		echo "<BR>"; */

		if ($tab_resp[$i][$j] != ""){
			
			$edata = urldecode($fld_resp[$i][$j]);
			$tresp = urldecode($tab_resp[$i][$j]);
			/*if (mb_detect_encoding($edata) != 'ASCII') {
				$encoding = mb_detect_encoding($edata ,$encodingList);
				$edata = htmlspecialchars(mb_convert_encoding($edata , "HTML-ENTITIES", $encoding),ENT_NOQUOTES,'iso-8859-1',false);
			}
			if (mb_detect_encoding($tresp) != 'ASCII') {
				$encoding = mb_detect_encoding($tresp ,$encodingList);
				$tresp = htmlspecialchars(mb_convert_encoding($tresp , "HTML-ENTITIES", $encoding),ENT_NOQUOTES,'iso-8859-1',false);
			}*/

			$v_response = explode("[VELSEP1]",$edata);
			$nxtVal = 
			$v_sql = "insert into velink.vlnk_imp_formresp values (velink.seq_vlnk_imp_formresp.nextval,".$pk_imp_formfld[$i].",'".str_replace("'","''",$v_response[0])."','".str_replace("'","''",$tresp)."','".str_replace("'","''",$edata)."')";
			$results = executeOCIUpdateQuery($v_sql,$ds_conn);

			$frmfldQ = "UPDATE velink.vlnk_imp_formfld set FK_MAP_RESP= velink.seq_vlnk_imp_formresp.currval where pk_imp_formfld = ".$pk_imp_formfld[$i];
			$results = executeOCIUpdateQuery($frmfldQ,$ds_conn);
		}
	}
}

$select_sql = "select fk_field,table_colname,pk_imp_formfld 
from er_mapform, velink.vlnk_imp_formfld
where fk_form = $v_form and 
mp_pkfld = fk_field and 
mp_flddatatype = 'MC' and
table_colname is not null and 
fk_vlnk_adapmod = ".$_POST["pk_vlnk_adapmod"];
$results = executeOCIQuery($select_sql,$ds_conn);
$v_trows = $results_nrows;
for ($rec = 0; $rec < $v_trows; $rec++){
	$v_sql = "delete from velink.vlnk_imp_formresp where fk_imp_formfld = ".$results["PK_IMP_FORMFLD"][$rec];
	$results_1 = executeOCIUpdateQuery($v_sql,$ds_conn);
	$v_sql = "select distinct ".$results["TABLE_COLNAME"][$rec]." as cbdata from ".$_POST["tabname"]." where ".$results["TABLE_COLNAME"][$rec]." is not null";
	$results_1 = executeOCIQuery($v_sql,$ds_conn);
	$v_trows_1 = $results_nrows;
	for ($rec_1 = 0; $rec_1 < $v_trows_1; $rec_1++){
		$v_cbdata = $results_1["CBDATA"][$rec_1];
		$v_cbdata = str_replace("'","''",$v_cbdata);
		$v_cbdata = str_replace("[","'",$v_cbdata);
		$v_cbdata = trim(strtolower(str_replace("]","'",$v_cbdata)));
		if (substr($v_cbdata,0,1) != "'") $v_cbdata = "'".$v_cbdata;
		if (substr($v_cbdata,strlen($v_cbdata)-1,1) != "'") $v_cbdata = $v_cbdata."'";
		
		$v_sql = "select fldresp_dispval,fldresp_dataval from er_fldresp where fk_field = ".$results["FK_FIELD"][$rec]." and lower(fldresp_dispval) in (".$v_cbdata.")";
		$results_2 = executeOCIQuery($v_sql,$ds_conn);
		$v_trows_2 = $results_nrows;
		$v_dispval = "";
		$v_dataval = "";
		for ($rec_2 = 0; $rec_2 < $v_trows_2; $rec_2++){
			if ($rec_2 == 0) {
				$v_dispval .= '['.$results_2["FLDRESP_DISPVAL"][$rec_2].']';
				$v_dataval .= '['.$results_2["FLDRESP_DATAVAL"][$rec_2].']';
			} else {
				$v_dispval .= ',['.$results_2["FLDRESP_DISPVAL"][$rec_2].']';
				$v_dataval .= '[VELCOMMA]['.$results_2["FLDRESP_DATAVAL"][$rec_2].']';
			}
		}
		if (!empty($v_dispval) && !empty($v_dataval)) {
			$v_sql = "insert into velink.vlnk_imp_formresp values (velink.seq_vlnk_imp_formresp.nextval,".$results["PK_IMP_FORMFLD"][$rec].",'".str_replace("'","''",$results_1["CBDATA"][$rec_1])."','".str_replace("'","''",$results_1["CBDATA"][$rec_1])."','".str_replace("'","''",$v_dispval."[VELSEP1]".$v_dataval)."')";
			$results_3 = executeOCIUpdateQuery($v_sql,$ds_conn);
		}

	}
}


echo "Data Saved...";

$url = "mig_forms.php?pk_vlnk_adaptor=".$_POST["pk_vlnk_adaptor"];
echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";

OCICommit($ds_conn);
OCILogoff($ds_conn);
}
?>
</form>
	</div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
